﻿namespace RSG.Model.Report2.Reports.Map
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using RSG.Base.ConfigParser;
    using RSG.Base.Tasks;
    using RSG.Model.Common;
    using RSG.Model.Common.Map;
    using RSG.Model.Report;
    using RSG.ManagedRage;
    using RSG.Platform;

    /// <summary>
    /// A report that lists 
    /// </summary>
    public class MapSizesReport : CSVReport, IDynamicLevelReport
    {
        #region Fields
        /// <summary>
        /// The name of this report.
        /// </summary>
        private const string Name = "Map Sizes";

        /// <summary>
        /// The description for this report.
        /// </summary>
        private const string Description =
            "Exports a csv report that lists all of the map sections with the archetype " +
            "and entity counts.";

        /// <summary>
        /// The private field used for the <see cref="GenerationTask"/> property.
        /// </summary>
        private ITask _generationTask;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MapSizesReport"/> class.
        /// </summary>
        public MapSizesReport()
            : base(Name, Description)
        {
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the task that represents the work needing to be done to generate this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (this._generationTask == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", this.EnsureDataLoaded));
                    task.AddSubTask(new ActionTask("Generating report", this.GenerateReport));
                    this._generationTask = task;
                }

                return this._generationTask;
            }
        }

        /// <summary>
        /// Gets a iterator around the stats that are required to generate this report. Always
        /// returns null.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Ensures that the data needed for this report is loaded.
        /// </summary>
        /// <param name="context">
        /// The task context for this action.
        /// </param>
        /// <param name="progress">
        /// The object used to notify a listener of the progress.
        /// </param>
        private void EnsureDataLoaded(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext reportContext = context as DynamicLevelReportContext;
            Debug.Assert(reportContext != null, "Unable to load date for report");
            if (reportContext == null)
            {
                return;
            }

            IMapHierarchy hierarchy = reportContext.Level.MapHierarchy;
            Debug.Assert(hierarchy != null, "Unable to load date for report");
            if (hierarchy == null || hierarchy.AllSections == null)
            {
                return;
            }

            List<IMapSection> mapSections = hierarchy.AllSections.ToList();
            float increment = 1.0f / mapSections.Count;
            string fmt = "Loading {0}";
            int index = 1;
            foreach (IMapSection section in mapSections)
            {
                Debug.WriteLine("{0} - {1} of {2}", section.Name, index++, mapSections.Count);
                context.Token.ThrowIfCancellationRequested();
                string message = String.Format(fmt, section.Name);
                progress.Report(new TaskProgress(increment, true, message));

                section.LoadStats(new List<StreamableStat>() { StreamableMapSectionStat.Archetypes, StreamableMapSectionStat.Entities });
            }
        }

        /// <summary>
        /// Generates the report and saves the file associated with it.
        /// </summary>
        /// <param name="context">
        /// The task context for this action.
        /// </param>
        /// <param name="progress">
        /// The object used to notify a listener of the progress.
        /// </param>
        private void GenerateReport(ITaskContext context, IProgress<TaskProgress> progress)
        {
            DynamicLevelReportContext reportContext = context as DynamicLevelReportContext;
            Debug.Assert(reportContext != null, "Unable to generate report");
            if (reportContext == null)
            {
                return;
            }

            IMapHierarchy hierarchy = reportContext.Level.MapHierarchy;
            Debug.Assert(hierarchy != null, "Unable to generate report");
            if (hierarchy == null || hierarchy.AllSections == null)
            {
                return;
            }

            Dictionary<string, Dictionary<string, string>> previousdata = this.PreviousData();
            Dictionary<string, Dictionary<string, string>> data = new Dictionary<string, Dictionary<string, string>>();
               
            List<IMapSection> mapSections = hierarchy.AllSections.ToList();
            float increment = 1.0f / mapSections.Count;
            string fmt = "Processing {0}";
            foreach (IMapSection section in mapSections)
            {
                context.Token.ThrowIfCancellationRequested();
                string message = String.Format(fmt, section.Name);
                progress.Report(new TaskProgress(increment, true, message));
                if (section == null)
                {
                    continue;
                }

                IMapArea parentArea = section.ParentArea;
                IMapArea grandParentArea = (parentArea != null ? parentArea.ParentArea : null);

                Dictionary<string, string> values = new Dictionary<string, string>();
                values.Add("Parent Area", parentArea != null ? parentArea.Name : "n/a");
                values.Add("Grandparent Area", grandParentArea != null ? grandParentArea.Name : "n/a");
                values.Add("Archetype Count", section.Archetypes != null ? section.Archetypes.Count.ToString() : "0");
                values.Add("Entity Count", section.ChildEntities != null ? section.ChildEntities.Count.ToString() : "0");
                data.Add(section.Name, values);
            }

            using (StreamWriter writer = new StreamWriter(this.Filename))
            {
                writer.WriteLine(
                    "Section Name," +
                    "Parent Area," +
                    "Grandparent Area," +
                    "Archetype Count," +
                    "Entity Count");

                foreach (KeyValuePair<string, Dictionary<string, string>> kvp in data)
                {
                    string csvRecord = String.Format("{0},{1},{2},", kvp.Key, kvp.Value["Parent Area"], kvp.Value["Grandparent Area"]);
                    csvRecord += String.Format("{0},{1}", kvp.Value["Archetype Count"], kvp.Value["Entity Count"]);

                    writer.WriteLine(csvRecord);
                }
            }

            string filename = Path.Combine(Path.GetDirectoryName(this.Filename), Path.GetFileNameWithoutExtension(this.Filename) + "_delta" + Path.GetExtension(this.Filename));
            using (StreamWriter writer = new StreamWriter(filename))
            {
                writer.WriteLine(
                    "Section Name," +
                    "Parent Area," +
                    "Grandparent Area," +
                    "Archetype Count," +
                    "Entity Count");

                Dictionary<string, Dictionary<string, string>> comparison = new Dictionary<string, Dictionary<string, string>>();
                foreach (KeyValuePair<string, Dictionary<string, string>> record in data)
                {
                    comparison.Add(record.Key, new Dictionary<string, string>());
                    Dictionary<string, string> previous;
                    if (!previousdata.TryGetValue(record.Key, out previous))
                    {
                        foreach (KeyValuePair<string, string> entry in record.Value)
                        {
                            comparison[record.Key].Add(entry.Key, entry.Value);
                        }
                    }
                    else
                    {
                        foreach (KeyValuePair<string, string> entry in record.Value)
                        {
                            if (entry.Key == "Parent Area" || entry.Key == "Grandparent Area")
                            {
                                comparison[record.Key].Add(entry.Key, entry.Value);
                            }
                            else
                            {
                                string previousValue;
                                if (!previous.TryGetValue(entry.Key, out previousValue))
                                {
                                    comparison[record.Key].Add(entry.Key, entry.Value);
                                }
                                else
                                {
                                    int delta = int.Parse(entry.Value) - int.Parse(previousValue);
                                    comparison[record.Key].Add(entry.Key, delta.ToString());
                                }           
                            }
                        }
                    }
                }

                foreach (KeyValuePair<string, Dictionary<string, string>> record in previousdata)
                {
                    if (!comparison.ContainsKey(record.Key))
                    {
                        comparison.Add(record.Key, new Dictionary<string, string>());
                        foreach (KeyValuePair<string, string> entry in record.Value)
                        {
                            if (entry.Key == "Parent Area" || entry.Key == "Grandparent Area")
                            {
                                comparison[record.Key].Add(entry.Key, entry.Value);
                            }
                            else
                            {
                                comparison[record.Key].Add(entry.Key, (-int.Parse(entry.Value)).ToString());
                            }
                        }
                    }
                }

                foreach (KeyValuePair<string, Dictionary<string, string>> kvp in comparison)
                {
                    string csvRecord = String.Format("{0},{1},{2},", kvp.Key, kvp.Value["Parent Area"], kvp.Value["Grandparent Area"]);
                    csvRecord += String.Format("{0},{1}", kvp.Value["Archetype Count"], kvp.Value["Entity Count"]);

                    writer.WriteLine(csvRecord);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, Dictionary<string, string>> PreviousData()
        {
            Dictionary<string, Dictionary<string, string>> data = new Dictionary<string, Dictionary<string, string>>();
            if (!File.Exists(this.Filename))
            {
                return data;
            }

            Dictionary<int, string> headerMap = new Dictionary<int, string>();
            int lineNumber = 0;
            using (StreamReader file = new StreamReader(this.Filename))
            {
                string line;
                while ((line = file.ReadLine()) != null)
                {
                    if (lineNumber == 0)
                    {
                        string[] headers = line.Split(',');
                        for (int i = 0; i < headers.Length; i++)
                        {
                            headerMap.Add(i, headers[i]);
                        }
                    }
                    else
                    {
                        string[] values = line.Split(',');
                        string sectionName = null;
                        for (int i = 0; i < values.Length; i++)
                        {
                            string header = headerMap[i];
                            if (header == "Section Name")
                            {
                                sectionName = values[i];
                                data.Add(values[i], new Dictionary<string, string>());
                                break;
                            }
                        }

                        if (sectionName != null)
                        {
                            for (int i = 0; i < values.Length; i++)
                            {
                                string header = headerMap[i];
                                if (header != "Section Name")
                                {
                                    data[sectionName].Add(header, values[i]);
                                }
                            }
                        }
                    }

                    lineNumber++;
                }
            }

            return data;
        }
        #endregion Methods
    }
}
