﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Ionic.Zip;
using RSG.Base.Extensions;
using RSG.Base.Logging;

namespace RSG.Model.GlobalTXD
{
    /// <summary>
    /// Set of validation methods
    /// </summary>
    public static class Validation
    {
        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="modelRoot"></param>
        /// <param name="zipFilepath"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        public static bool ValidateModelAgainstExportedZip(GlobalRoot modelRoot, String zipFilepath, ILog log)
        {
            // Get a list of global texture dictionary model objects that contain global textures.
            IList<GlobalTextureDictionary> validTxdModels = GetGlobalTxdsWithGlobalTextures(modelRoot);

            // Create a lookup for quickly retrieving gtxds by name.
            IDictionary<String, GlobalTextureDictionary> gtxdLookup = validTxdModels.ToDictionary(item => item.Name.ToLower());
            IList<String> encounteredTxds = new List<String>();

            try
            {
                using (ZipFile gtxdZipFile = ZipFile.Read(zipFilepath))
                {
                    // Go over all the entries in the gtxd file that are zip files.
                    foreach (ZipEntry entry in gtxdZipFile.Entries.Where(item => item.FileName.EndsWith(".zip")))
                    {
                        String txdName = StripAllExtensions(entry.FileName);

                        // Make sure there is a global txd for this entry.
                        if (gtxdLookup.ContainsKey(txdName))
                        {
                            // Extract the entry to a memory stream and then open it up to get at the tcl file names.
                            using (MemoryStream txdZipStream = new MemoryStream())
                            {
                                entry.Extract(txdZipStream);
                                txdZipStream.Position = 0;

                                ValidateGlobalTxdAgainstZipFile(gtxdLookup[txdName], txdZipStream, zipFilepath, log);
                            }

                            // Keep track of this txd.
                            encounteredTxds.Add(txdName);
                        }
                        else
                        {
                            log.Error("GlobalTXD: Parent txd model doesn't contain the '{0}' global txd.", txdName);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                log.Exception(ex, "GlobalTXD: Exception while parsing '{0}' zip file.", zipFilepath);
                return false;
            }

            // Let the user know about any textures that exist in the model but not in the zip file.
            IEnumerable<String> missingTxds = gtxdLookup.Keys.Except(encounteredTxds);
            foreach (String missingTxd in missingTxds)
            {
                log.ErrorCtx("GlobalTXD: Parent txd zip file '{0}' doesn't contain the '{1}' global txd.",
                    zipFilepath, missingTxd);
            }

            return true;
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="root"></param>
        /// <returns></returns>
        private static IList<GlobalTextureDictionary> GetGlobalTxdsWithGlobalTextures(GlobalRoot root)
        {
            IList<GlobalTextureDictionary> globalTxds = new List<GlobalTextureDictionary>();
            foreach (GlobalTextureDictionary globalTxd in root)
            {
                globalTxds.AddRange(GetGlobalTxdsWithGlobalTextures(globalTxd));
            }
            return globalTxds;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dictContainer"></param>
        /// <returns></returns>
        private static IEnumerable<GlobalTextureDictionary> GetGlobalTxdsWithGlobalTextures(GlobalTextureDictionary globalTxd)
        {
            // If this global txd has global textures then it's valid.
            if (globalTxd.Textures != null && globalTxd.Textures.Any())
            {
                yield return globalTxd;
            }

            // Make sure there are some child global txds.
            if (globalTxd.GlobalTextureDictionaries != null)
            {
                foreach (GlobalTextureDictionary childTxd in globalTxd.GlobalTextureDictionaries.Values)
                {
                    foreach (GlobalTextureDictionary validTxd in GetGlobalTxdsWithGlobalTextures(childTxd))
                    {
                        yield return validTxd;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private static String StripAllExtensions(String filename)
        {
            // Strip the off all extensionts from the entries file name.
            filename = filename.ToLower();
            while (!String.IsNullOrEmpty(Path.GetExtension(filename)))
            {
                filename = Path.GetFileNameWithoutExtension(filename);
            }
            return filename;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalTxd"></param>
        /// <param name="txdZipStream"></param>
        /// <param name="log"></param>
        private static void ValidateGlobalTxdAgainstZipFile(GlobalTextureDictionary globalTxd, Stream txdZipStream, String zipFilepath, ILog log)
        {
            IList<String> globalTextureNames =
                globalTxd.Textures.Keys.Select(item => Path.GetFileNameWithoutExtension(item)).ToList();
            IList<String> encounteredTextures = new List<String>();

            using (ZipFile txdZipFile = ZipFile.Read(txdZipStream))
            {
                foreach (ZipEntry entry in txdZipFile.Entries.Where(item => item.FileName.EndsWith(".tcl")))
                {
                    String textureName = StripAllExtensions(entry.FileName);

                    if (globalTextureNames.Contains(textureName))
                    {
                        encounteredTextures.Add(textureName);
                    }
                    else
                    {
                        log.Error("GlobalTXD: Parent txd model doesn't contain the '{0}' texture in the '{1}' global txd.",
                            textureName, globalTxd.Name);
                    }
                }
            }

            // Let the user know about any textures that exist in the model but not in the zip file.
            IEnumerable<String> missingTextures = globalTextureNames.Except(encounteredTextures);
            foreach (String missingTexture in missingTextures)
            {
                log.Error("GlobalTXD: Parent txd zip file '{0}' doesn't contain the '{1}' texture in the '{2}' txd.",
                    zipFilepath, missingTexture, globalTxd.Name);
            }
        }
        #endregion // Private Methods
    }
}
