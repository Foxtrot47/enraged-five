﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using System.Diagnostics;
using RSG.Base.Logging;
using System.IO;
using System.Text.RegularExpressions;
using System.Web.UI;

namespace RSG.Model.Report.Reports.ScriptReports
{
    /// <summary>
    /// Generates a report that shows information about the script model enums including
    /// which ones aren't being used
    /// </summary>
    public class ScriptStatEnumsReport : ScriptReportBase
    {
        #region Constants
        private const String NAME = "Script Stat Enums Report";
        private const String DESC = "Generates a report that shows information about the script stat enums including which ones aren't being used.";

        private const String STAT_ENUM_FILEPATH = @"script:\core\game\data\stats_enums.sch";

        /// <summary>
        /// Regex to use to find enum values that we'll be looking for.
        /// Matches things like:
        ///     MP_STAT_WHATEVER
        ///     MP_AWARD_BLAH,
        /// </summary>
        protected static readonly Regex LINE_MATCH_REGEX = new Regex(@"^\s+(?<name>(MP_STAT_|MP_AWARD_)\w*)");
        #endregion // Constants

        #region Properties
        /// <summary>
        /// The file that we wish to search for enum values in
        /// </summary>
        public override string EnumFile
        {
            get
            {
                return STAT_ENUM_FILEPATH;
            }
        }

        /// <summary>
        /// The regex to use when searching through the enum file
        /// </summary>
        public override Regex EnumValueRegex
        {
            get
            {
                return LINE_MATCH_REGEX;
            }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ScriptStatEnumsReport()
            : base(NAME, DESC)
        {
        }
        #endregion // Constructor(s)
    } // ScriptStatEnumsReport
}
