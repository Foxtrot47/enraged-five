﻿//---------------------------------------------------------------------------------------------
// <copyright file="MemberOrderInfo.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions
{
    using RSG.Metadata.Model.Definitions.Members;

    /// <summary>
    /// A structure that contains the order positional information for a single member object.
    /// </summary>
    public struct MemberOrderInfo
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Member"/> property.
        /// </summary>
        private IMember _member;

        /// <summary>
        /// The private field used for the <see cref="Position"/> property.
        /// </summary>
        private int _position;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MemberOrderInfo"/> struct.
        /// </summary>
        /// <param name="member">
        /// The member the information is attached to.
        /// </param>
        /// <param name="position">
        /// The order position of the associated member.
        /// </param>
        public MemberOrderInfo(IMember member, int position)
        {
            this._member = member;
            this._position = position;
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the member whose positional information is contained within this structure.
        /// </summary>
        public IMember Member
        {
            get { return this._member; }
        }

        /// <summary>
        /// Gets the integer position of this member within its structure parent.
        /// </summary>
        public int Position
        {
            get { return this._position; }
        }
        #endregion Properties
    } // RSG.Metadata.Model.Definitions.MemberOrderInfo {Structure}
} // RSG.Metadata.Model.Definitions {Namespace}
