﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using RSG.Base.Extensions;
using RSG.ManagedRage;
using RSG.Model.Asset;
using RSG.Model.Common;
using RSG.Platform;
using RSG.SceneXml;
using RSG.SceneXml.Material;
using RSG.Statistics.Common.Dto.GameAssets.Bundles;

namespace RSG.Model.Character
{
    /// <summary>
    /// Character asset.
    /// </summary>
    public abstract class Character : StreamableAssetContainerBase, ICharacter
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// The collection this character is a part of
        /// </summary>
        [Browsable(false)]
        public ICharacterCollection ParentCollection
        {
            get;
            protected set;
        }

        /// <summary>
        /// Character category classification.
        /// </summary>
        [StreamableStat(StreamableCharacterStat.CategoryString)]
        public CharacterCategory Category
        {
            get
            {
                CheckStatLoaded(StreamableCharacterStat.Category);
                return m_category;
            }
            protected set
            {
                SetPropertyValue(ref m_category, value, "Category");
                SetStatLodaded(StreamableCharacterStat.Category, true);
            }
        }
        private CharacterCategory m_category;

        /// <summary>
        /// 
        /// </summary>
        [Browsable(false)]
        public ITexture[] Textures
        {
            get
            {
                return m_textures;
            }
            private set
            {
                SetPropertyValue(ref m_textures, value, "Textures", "TextureCount");
            }
        }
        private ITexture[] m_textures;

        /// <summary>
        /// Texture count (exposed for the property grid)
        /// </summary>
        public int TextureCount
        {
            get
            {
                return (Textures != null ? Textures.Count() : 0);
            }
        }

        /// <summary>
        /// Array of character shaders.
        /// </summary>
        [Browsable(false)]
        [StreamableStat(StreamableCharacterStat.ShadersString)]
        public IShader[] Shaders
        {
            get
            {
                CheckStatLoaded(StreamableCharacterStat.Shaders);
                return m_shaders;
            }
            protected set
            {
                if (SetPropertyValue(ref m_shaders, value, "Shaders", "ShaderCount"))
                {
                    if (m_shaders != null)
                        Textures = m_shaders.SelectMany(shader => shader.Textures).Distinct().OrderBy(item => item.Name).ToArray();
                    else
                        Textures = null;
                }
                SetStatLodaded(StreamableCharacterStat.Shaders, true);
            }
        }
        private IShader[] m_shaders;

        /// <summary>
        /// Shader count (exposed for the property grid)
        /// </summary>
        public int ShaderCount
        {
            get
            {
                return (Shaders != null ? Shaders.Count() : 0);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableCharacterStat.PolyCountString)]
        public int PolyCount
        {
            get
            {
                CheckStatLoaded(StreamableCharacterStat.PolyCount);
                return m_polyCount;
            }
            protected set
            {
                SetPropertyValue(ref m_polyCount, value, "PolyCount");
                SetStatLodaded(StreamableCharacterStat.PolyCount, true);
            }
        }
        private int m_polyCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableCharacterStat.CollisionCountString)]
        public int CollisionCount
        {
            get
            {
                CheckStatLoaded(StreamableCharacterStat.CollisionCount);
                return m_collisionCount;
            }
            protected set
            {
                SetPropertyValue(ref m_collisionCount, value, "CollisionCount");
                SetStatLodaded(StreamableCharacterStat.CollisionCount, true);
            }
        }
        private int m_collisionCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableCharacterStat.BoneCountString)]
        public int BoneCount
        {
            get
            {
                CheckStatLoaded(StreamableCharacterStat.BoneCount);
                return m_boneCount;
            }
            protected set
            {
                SetPropertyValue(ref m_boneCount, value, "BoneCount");
                SetStatLodaded(StreamableCharacterStat.BoneCount, true);
            }
        }
        private int m_boneCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableCharacterStat.ClothCountString)]
        public int ClothCount
        {
            get
            {
                CheckStatLoaded(StreamableCharacterStat.ClothCount);
                return m_clothCount;
            }
            protected set
            {
                SetPropertyValue(ref m_clothCount, value, "ClothCount");
                SetStatLodaded(StreamableCharacterStat.ClothCount, true);
            }
        }
        private int m_clothCount;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableCharacterStat.ClothPolyCountString)]
        public int ClothPolyCount
        {
            get
            {
                CheckStatLoaded(StreamableCharacterStat.ClothPolyCount);
                return m_clothPolycount;
            }
            protected set
            {
                SetPropertyValue(ref m_clothPolycount, value, "ClothPolyCount");
                SetStatLodaded(StreamableCharacterStat.ClothPolyCount, true);
            }
        }
        private int m_clothPolycount;

        /// <summary>
        /// Dictionary mapping platforms to platform stats.
        /// </summary>
        [Browsable(false)]
        [StreamableStat(StreamableCharacterStat.PlatformStatsString)]
        public IDictionary<RSG.Platform.Platform, ICharacterPlatformStat> PlatformStats
        {
            get
            {
                CheckStatLoaded(StreamableCharacterStat.PlatformStats);
                return m_platformStats;
            }
            protected set
            {
                SetPropertyValue(ref m_platformStats, value, "PlatformStats");
                SetStatLodaded(StreamableCharacterStat.PlatformStats, true);
            }
        }
        private IDictionary<RSG.Platform.Platform, ICharacterPlatformStat> m_platformStats;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="sceneXml"></param>
        public Character(String name, CharacterCategory category, ICharacterCollection parentCollection)
            : base(name)
        {
            ParentCollection = parentCollection;
            Category = category;
        }
        #endregion // Constructor(s)

        #region IComparable<ICharacter> Interface
        /// <summary>
        /// Compare this weapon to another.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(ICharacter other)
        {
            if (other == null)
            {
                return 1;
            }

            return (Name.CompareTo(other.Name));
        }
        #endregion // IComparable<ICharacter> Interface

        #region IEquatable<ICharacter> Interface
        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <param name="other">An object to compare with this object.</param>
        /// <returns>true if the current object is equal to the other parameter; otherwise, false.</returns>
        public bool Equals(ICharacter other)
        {
            if (other == null)
            {
                return false;
            }

            return (Name == other.Name);
        }
        #endregion // IEquatable<ICharacter> Interface

        #region Object Overrides
        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (obj == null)
            {
                return base.Equals(obj);
            }

            return ((obj is ICharacter) && Equals(obj as ICharacter));
        }

        /// <summary>
        /// Return String representation of the character.
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return Name;
        }

        /// <summary>
        /// Return hash code of character (based on Name).
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return (this.Name.GetHashCode());
        }
        #endregion // Object Overrides
    } // Character
} // RSG.Model.Character
