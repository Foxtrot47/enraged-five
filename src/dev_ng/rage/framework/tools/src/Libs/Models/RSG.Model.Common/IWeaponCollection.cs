﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;

namespace RSG.Model.Common
{
    /// <summary>
    /// 
    /// </summary>
    public interface IWeaponCollection :
        IAsset,
        IEnumerable<IWeapon>,
        ICollection<IWeapon>,
        IDisposable
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        ObservableCollection<IWeapon> Weapons { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        void LoadAllStats();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        void LoadStats(IEnumerable<StreamableStat> statsToLoad);
        #endregion // Methods
    } // IWeaponCollection
} // RSG.Model.Common
