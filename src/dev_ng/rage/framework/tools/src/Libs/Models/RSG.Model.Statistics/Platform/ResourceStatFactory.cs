﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Model.Statistics.Platform;

/// <summary>
/// Resource stat factory
/// </summary>
public static class ResourceStatFactory
{
    #region Constructor(s)
    /// <summary>
    /// Static constructor.
    /// </summary>
    static ResourceStatFactory()
    {
    }
    #endregion // Constructor(s)

    #region Static Controller Methods

    /// <summary>
    /// Create a list of resource stats from a filesystem wildcard
    /// </summary>
    /// <param name="wildcard"></param>
    /// <returns></returns>
    public static List<ResourceStat> CreateResourceStats(string wildcard)
    {
            string searchDir = System.IO.Path.GetDirectoryName(wildcard);
            string searchPattern = System.IO.Path.GetFileName(wildcard);

            // Discover all these file names. (resource_statistics.xml)
            string[] allStatFiles = Directory.GetFiles(searchDir, searchPattern, SearchOption.AllDirectories);

            return CreateResourceStats(allStatFiles);
    }

    /// <summary>
    /// Create a list of resource stats from a list of xml filenames
    /// </summary>
    /// <param name="statFilenames"></param>
    /// <returns></returns>
    public static List<ResourceStat> CreateResourceStats(string[] statFilenames)
    {
            ICollection<ResourceBucketStat> resBucketStats = new List<ResourceBucketStat>();

            // Read all bucket stats
            foreach (string filename in statFilenames)
            {
                Log.Log__Message("Populating resourcing bucket stat from file {0}", filename);
                CreateResourceStats(filename, resBucketStats);
            }

            Log.Log__Message("Populated {0} resourcing bucket stats", resBucketStats.Count());
            List<ResourceStat> resStats = ResourceStatFactory.CreateResourceStats(resBucketStats);

            return resStats;
    }

    #endregion // Static Controller Methods

    #region Private Methods

    /// <summary>
    /// Create a list of resource stats from a single xml filename
    /// </summary>
    /// <param name="filename"></param>
    /// <param name="resBucketStats"></param>
    private static void CreateResourceStats(string filename, ICollection<ResourceBucketStat> resBucketStats)
    {
        try
        {
            //
            // Use XmlReader - these files could be rather big.                    
            using (XmlReader reader = XmlReader.Create(filename))
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        if (reader.Name == "Bucket")
                        {
                            XElement xmlBucketElem = XNode.ReadFrom(reader) as XElement;
                            ResourceBucketStat resBucketStat = CreateResourceStat(xmlBucketElem);
                            if (null != resBucketStat)
                                resBucketStats.Add(resBucketStat);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            String message = String.Format("Exception Populating resourcing bucket stat from file {0} See InnerException.", filename);
            throw new Exception(message, ex);
        }
    }

    /// <summary>
    /// Create resource stats from an XmlElement
    /// </summary>
    /// <param name="xmlBucketElem"></param>
    /// <returns></returns>
    private static ResourceBucketStat CreateResourceStat(XElement xmlBucketElem)
    {
        if (null != xmlBucketElem && ResourceBucketStat.CanBeConstructed(xmlBucketElem))
        {
            ResourceBucketStat resBucketStat = new ResourceBucketStat(xmlBucketElem);
            return resBucketStat;
        }

        return null;
    }

    /// <summary>
    /// Create a new list of ResourceStats from the enumerable of ResourceBucketStats
    /// - then returns a list of Resource Stats ( needs to be a list )
    /// </summary>
    /// <param name="bucketStat"></param>
    /// <returns></returns>
    private static List<ResourceStat> CreateResourceStats(IEnumerable<ResourceBucketStat> bucketStats)
    {
        IDictionary<ResourceStatKey, ICollection<ResourceBucket>> KeyedBucketStats = KeyBucketStats(bucketStats);

        // Now that we have built the keyed data, transform that into a flat array of ResourceStat objects
        List<ResourceStat> resourceStats = new List<ResourceStat>();

        foreach (KeyValuePair<ResourceStatKey, ICollection<ResourceBucket>> kvp in KeyedBucketStats)
        {
            ResourceStatKey key = kvp.Key;
            ICollection<ResourceBucket> bucketList = kvp.Value;

            // Get the keys
            RSG.Platform.Platform platform = key.Platform;
            RSG.Platform.FileType fileType = key.FileType;
            String resourceName = key.ResourceName;
            String sourceFilename = key.SourceFilename;
            String destFilename = key.DestinationFilename;

            if (resourceName == null)
            {
                Log.Log__Warning("Resource stat has NULL resource name");
            }

            if (null != bucketList && resourceName != null && 
                null != sourceFilename && null != destFilename)
            {
                ResourceStat newResourceStat = new ResourceStat(resourceName, 
                    sourceFilename, destFilename, fileType, platform, bucketList);
                resourceStats.Add(newResourceStat);
                Log.Log__Debug("ResourceStatFactory {0}:{1}:{2} {3} buckets", platform, fileType, resourceName, bucketList.Count());
            }
        }

        Log.Log__Message("ResourceStatFactory created {0} items", resourceStats.Count());

        return (resourceStats);
    }

    /// <summary>
    /// Create a keyed dictionary of the bucket stats
    /// - facilitated with a Tuple key
    /// - dictionary allows the keyed data to grow as we encounter new keys & buckets.
    /// </summary>
    /// <param name="bucketStats"></param>
    /// <returns></returns>
    private static IDictionary<ResourceStatKey, ICollection<ResourceBucket>> KeyBucketStats(IEnumerable<ResourceBucketStat> bucketStats)
    {
        IDictionary<ResourceStatKey, ICollection<ResourceBucket>> dict = new Dictionary<ResourceStatKey, ICollection<ResourceBucket>>();
        
        foreach (ResourceBucketStat bucketStat in bucketStats)
        {
            // Create bucket
            ResourceBucket bucket = new ResourceBucket(bucketStat.Type, bucketStat.ID, bucketStat.Size, bucketStat.Capacity);

            // Create key
            ResourceStatKey resourceStatsKey = new ResourceStatKey(bucketStat.Platform, 
                bucketStat.FileType, bucketStat.ResourceName, bucketStat.SourceFilename,
                bucketStat.DestinationFilename);

            // Insert into dictionary
            if (!dict.ContainsKey(resourceStatsKey))
            {
                ICollection<ResourceBucket> buckets = new List<ResourceBucket>() { bucket };
                dict.Add(resourceStatsKey, buckets);
            }
            else
            {
                dict[resourceStatsKey].Add(bucket);
            }
        }

        return dict;
    }

    #endregion // Private Methods

    #region Private Classes
    /// <summary>
    /// Private class to wrap up Tuple keys for dictionary
    /// </summary>
    private class ResourceStatKey 
        : Tuple<RSG.Platform.Platform, RSG.Platform.FileType, String, String, String>
    {
        #region Properties
        /// <summary>
        /// Gets the Platform from the Tuple key
        /// </summary>
        public RSG.Platform.Platform Platform { get { return base.Item1; } }

        /// <summary>
        /// Gets the Filetype from the Tuple key
        /// </summary>
        public RSG.Platform.FileType FileType { get { return base.Item2; } }

        /// <summary>
        /// Gets the ResourceName from the Tuple key
        /// </summary>
        public String ResourceName { get { return base.Item3; } }

        /// <summary>
        /// Get the source filename from the Tuple key.
        /// </summary>
        public String SourceFilename { get { return base.Item4; } }

        /// <summary>
        /// Get the destination filename from the Tuple key.
        /// </summary>
        public String DestinationFilename { get { return base.Item5; } }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="platform"></param>
        /// <param name="fileType"></param>
        /// <param name="resourceName"></param>
        /// <param name="sourceFilename"></param>
        /// <param name="destFilename"></param>
        public ResourceStatKey(RSG.Platform.Platform platform, RSG.Platform.FileType fileType, 
            String resourceName, String sourceFilename, String destFilename)
            : base(platform, fileType, resourceName, sourceFilename, destFilename)
        {
        }
        #endregion // Constructors
    }
    #endregion // Private Classes

}