﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RSG.Base.ConfigParser;
using RSG.Base.Logging;
using RSG.ManagedRage;
using RSG.Model.Common;
using System.Text;
using RSG.Base.Threading;
using RSG.Base.Tasks;

namespace RSG.Model.Report.Reports.Characters
{
    /// <summary>
    /// Generates a CSV report containing character information
    /// </summary>
    public class CharacterStatsReport : CSVReport, IDynamicLevelReport
    {
        #region Constants
        private const String c_name = "Character Stats Report";
        private const String c_description = "Exports the selected level characters into a .csv file"; 
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicLevelReportContext)context, progress)), 10);
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public CharacterStatsReport()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)

        #region Task Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void EnsureDataLoaded(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            ICharacterCollection characters = context.Level.Characters;

            if (characters != null)
            {
                characters.LoadAllStats();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            ExportCharacterCSVReport(context.Level, context.GameView, context.Platforms);
        }
        #endregion // Task Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectFilename"></param>
        /// <param name="interiorFilename"></param>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        private void ExportCharacterCSVReport(ILevel level, RSG.Base.ConfigParser.ConfigGameView gv, IPlatformCollection platforms)
        {
            try
            {
                using (StreamWriter sw = new StreamWriter(Filename))
                {
                    WriteCsvHeader(sw, platforms);

                    ICharacterCollection characters = level.Characters;
                    if (characters != null)
                    {
                        foreach (ICharacter character in characters.AllCharacters)
                        {
                            string scvRecord = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}",
                                character.Name, character.Category.ToString(), character.PolyCount, character.CollisionCount, character.ClothPolyCount, character.ClothCount,
                                character.Textures.Count(), character.Shaders.Count(),
                                character.BoneCount,
                                GenerateMemoryDetails(character, platforms));

                            sw.WriteLine(scvRecord);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                RSG.Base.Logging.Log.Log__Exception(ex, "Error creating csv file.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        /// <param name="milo"></param>
        private void WriteCsvHeader(StreamWriter sw, IPlatformCollection platforms)
        {
            string header = "Name,Category,Poly Count,Collision Count,Cloth Poly Count,Cloth Count,Texture Count,Shader Count,Bone Count";

            foreach (RSG.Platform.Platform platform in platforms)
            {
                header += String.Format(",{0} Physical Size,{0} Virtual Size,{0} Total Size", platform);
            }

            sw.WriteLine(header);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="character"></param>
        /// <param name="platforms"></param>
        /// <returns></returns>
        private string GenerateMemoryDetails(ICharacter character, IPlatformCollection platforms)
        {
            StringBuilder details = new StringBuilder("");

            foreach (RSG.Platform.Platform platform in platforms)
            {
                if (character.PlatformStats.ContainsKey(platform))
                {
                    ICharacterPlatformStat stat = character.PlatformStats[platform];
                    details.Append(String.Format("{0},{1},{2},", stat.PhysicalSize, stat.VirtualSize, stat.PhysicalSize + stat.VirtualSize));
                }
                else
                {
                    details.Append("n/a,n/a,n/a,");
                }
            }

            return details.ToString().TrimEnd(new char[] { ',' });
        }
        #endregion // Private Methods
    } // CharacterStatsReport
}
