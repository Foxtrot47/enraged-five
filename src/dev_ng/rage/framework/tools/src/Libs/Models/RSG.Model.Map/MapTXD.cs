﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.SceneXml;
using RSG.SceneXml.Material;
using System.Xml.XPath;
using System.Windows.Media.Imaging;
using System.Xml;
using System.Collections.ObjectModel;

namespace RSG.Model.Map
{
    public class MapTXD
    {

        #region Properties & Member Data

        public String Name
        {
            get { return m_sName; }
            set { m_sName = value; }
        }
        private String m_sName;

        public UserTXD ParentUserTXD
        {
            get { return m_parentUserTXD; }
            set { m_parentUserTXD = value; }
        }
        private UserTXD m_parentUserTXD;

        public Object ParentUserData
        {
            get { return m_ParentUserData; }
            set { m_ParentUserData = value; }
        }
        private Object m_ParentUserData;

        public TXDHierarchy.TXDHierarchy ParentHierarchy
        {
            get { return m_parentHierarchy; }
            set { m_parentHierarchy = value; }
        }
        private TXDHierarchy.TXDHierarchy m_parentHierarchy;

        public int TextureCount
        {
            get { return m_Textures != null ? m_Textures.Count : 0; }
        }
        public ObservableDictionary<String, Texture> Textures
        {
            get { return m_Textures; }
        }
        private ObservableDictionary<String, Texture> m_Textures;   // <filename, Object>

        public RSG.Base.Collections.ObservableCollection<Object> BindingObjects
        {
            get { return m_BindingObjects; }
        }
        private RSG.Base.Collections.ObservableCollection<Object> m_BindingObjects;

        public float Size
        {
            get { return m_size; }
            set { m_size = value; }
        }
        private float m_size;

        #endregion

        #region Constructor(s)

        /// <summary>
        /// Creates a empty map TXD dynamically on the users request and fills the textures in with the given object.
        /// </summary>
        public MapTXD(ObjectDef def, Scene scene, TXDHierarchy.TXDHierarchy hierarchyParent, Object userDataParent)
        {
            this.Size = 0.0f;

            m_parentUserTXD = null;
            m_parentHierarchy = hierarchyParent;
            m_ParentUserData = userDataParent;

            m_Textures = new ObservableDictionary<String, Texture>();
            m_BindingObjects = new RSG.Base.Collections.ObservableCollection<Object>();

            this.Name = def.GetAttribute(AttrNames.OBJ_TXD, AttrDefaults.OBJ_TXD);
            if (scene.MaterialLookup.ContainsKey(def.Material))
            {
                MaterialDef rootMaterial = scene.MaterialLookup[def.Material];
                RecurseCollectTexturesFromMaterial(rootMaterial);
            }
        }

        /// <summary>
        /// Creates a empty map TXD dynamically on the users request and fills the textures in with the given object.
        /// </summary>
        public MapTXD(ObjectDef def, Scene scene, Object userDataParent)
        {
            this.Size = 0.0f;

            m_parentUserTXD = null;
            m_parentHierarchy = null;
            m_ParentUserData = userDataParent;

            m_Textures = new ObservableDictionary<String, Texture>();
            m_BindingObjects = new RSG.Base.Collections.ObservableCollection<Object>();

            this.Name = def.GetAttribute(AttrNames.OBJ_TXD, AttrDefaults.OBJ_TXD);
            if (scene.MaterialLookup.ContainsKey(def.Material))
            {
                MaterialDef rootMaterial = scene.MaterialLookup[def.Material];
                RecurseCollectTexturesFromMaterial(rootMaterial);
            }
        }

        /// <summary>
        /// Creates a map TXD dynamically on the users request and copies the properties from the 
        /// given map dictionary including creating new textures. It also uses the given
        /// user dictionary parent to set the parent data.
        /// </summary>
        public MapTXD(MapTXD mapTXD, UserTXD parent)
        {
            this.Size = 0.0f;

            m_parentUserTXD = parent;
            m_parentHierarchy = parent.ParentHierarchy;

            m_Textures = new ObservableDictionary<String, Texture>();
            m_BindingObjects = new RSG.Base.Collections.ObservableCollection<Object>();

            this.Name = mapTXD.Name;
            this.Size = mapTXD.Size;
            foreach (Texture texture in mapTXD.Textures.Values)
            {
                Texture newTexture = new Texture(texture, this);
                m_Textures.Add(newTexture.StreamName, newTexture);
                m_BindingObjects.Add(newTexture);
            }
        }

        /// <summary>
        /// Creates a map dictionary from the navigator from the xml file
        /// </summary>
        public MapTXD(XPathNavigator navigator, UserTXD parent)
        {
            this.Size = 0.0f;

            m_parentUserTXD = parent;
            m_parentHierarchy = parent.ParentHierarchy;

            m_Textures = new ObservableDictionary<String, Texture>();
            m_BindingObjects = new RSG.Base.Collections.ObservableCollection<Object>();

            Deserialise(navigator);
        }

        #endregion

        #region Public Function(s)

        public void AddObject(ObjectDef objectDef, Scene scene)
        {
            // make sure that the txd names are the same
            String txdname = objectDef.GetAttribute(AttrNames.OBJ_TXD, AttrDefaults.OBJ_TXD);
            if (this.Name != txdname)
                return;

            if (scene.MaterialLookup.ContainsKey(objectDef.Material))
            {
                MaterialDef rootMaterial = scene.MaterialLookup[objectDef.Material];
                RecurseCollectTexturesFromMaterial(rootMaterial);
            }
        }

        public Boolean IsARelative(UserTXD userTXD)
        {
            UserTXD parent = this.ParentUserTXD;
            while (parent != null)
            {
                if (parent == userTXD)
                {
                    return true;
                }
                parent = parent.ParentUserTXD;
            }
            return false;
        }

        /// <summary>
        /// Serialises this map dictionary out to the given xml document and appends the
        /// data onto the given parent node from the document
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="parentNode"></param>
        public void Serialise(XmlDocument xmlDoc, XmlElement parentNode)
        {
            XmlElement dictionaryNode = xmlDoc.CreateElement("MapDictionary");

            // set attributes
            dictionaryNode.SetAttribute("Name", this.Name);
            dictionaryNode.SetAttribute("TextureCount", this.TextureCount.ToString());

            // Create the texture root node and add the child in for this dictionary
            XmlElement TextureRootNode = xmlDoc.CreateElement("Textures");
            foreach (Texture texture in this.Textures.Values)
            {
                texture.Serialise(xmlDoc, TextureRootNode);
            }
            dictionaryNode.AppendChild(TextureRootNode);

            parentNode.AppendChild(dictionaryNode);
        }

        /// <summary>
        /// Resets the binding so that it equals the texture list, this is 
        /// used when a map dictionary moves to a different location and therefore needs
        /// a clean slate
        /// </summary>
        public void ResetBindingObjects()
        {
            m_BindingObjects.Clear();
            foreach (Texture texture in m_Textures.Values)
            {
                texture.UserParent = null;
                texture.MapParent = this;
                m_BindingObjects.Add(texture);
            }
        }

        /// <summary>
        /// Determines if the given streamname is located in the texture list
        /// </summary>
        /// <param name="streamname"></param>
        /// <returns></returns>
        public Boolean ContainsTexture(String streamName)
        {
            return m_Textures.ContainsKey(streamName);
        }

        /// <summary>
        /// Determines if the given texture is located in the texture list
        /// </summary>
        /// <param name="streamname"></param>
        /// <returns></returns>
        public Boolean ContainsTexture(Texture texture)
        {
            return m_Textures.ContainsKey(texture.StreamName);
        }

        /// <summary>
        /// Determines if the given streamname is located in the texture list
        /// </summary>
        /// <param name="streamname"></param>
        /// <returns></returns>
        public Boolean ContainsBindingTexture(String streamName)
        {
            foreach (Texture texture in m_BindingObjects)
            {
                if (texture.StreamName == streamName)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Determines if the given texture is located in the texture list
        /// </summary>
        /// <param name="streamname"></param>
        /// <returns></returns>
        public Boolean ContainsBindingTexture(Texture texture)
        {
            foreach (Texture boundTexture in m_BindingObjects)
            {
                if (boundTexture.StreamName == texture.StreamName)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Re-adds the given texture back into the binding objects only if it's present in the texture list.
        /// This is used to add a texture back in after it has been promoted. This function also walks up the parents
        /// and removes any occurances of this texture in their promoted textures.
        /// </summary>
        /// <param name="texture"></param>
        /// <returns></returns>
        public Boolean ReAddTexture(Texture texture)
        {
            Texture readdedTexture = this.Textures[texture.StreamName];

            if (!m_Textures.ContainsKey(readdedTexture.StreamName)) return false;
            if (m_BindingObjects.Contains(readdedTexture)) return false;

            readdedTexture.MapParent = this;
            readdedTexture.UserParent = null;
            readdedTexture.ParentHierarchy = this.ParentHierarchy;
            m_BindingObjects.Add(readdedTexture);

            // Need to make sure that the texture presence count is updated
            texture.UpdatePresenceCountFromSiblings();
            return true;
        }

        /// <summary>
        /// Goes through the binding objects and removes the texture with the given streamname if found.
        /// </summary>
        /// <param name="streamname"></param>
        public void RemoveBindingTextureIfPresent(String streamName)
        {
            foreach (Texture texture in m_BindingObjects)
            {
                if (texture.StreamName == streamName)
                {
                    m_BindingObjects.Remove(texture);
                    return;
                }
            }
        }

        /// <summary>
        /// Goes through this map and any other map that belongs to the parent (sibling) and updates the
        /// presence count for each texture
        /// </summary>
        /// <param name="mapTXD"></param>
        /// <returns></returns>
        public void UpdatePresenceCount()
        {
            if (this.ParentUserTXD == null) return;
            // Reset the presence count in all of the textures
            foreach (Texture texture in Textures.Values) texture.ResetPresenceCount();

            // Loop through the sibling map dictionaries and increase the presence count for common textures
            foreach (MapTXD sibling in this.ParentUserTXD.ChildrenMapTXD.Values)
            {
                if (sibling == this) continue;

                foreach (Texture siblingTexture in sibling.Textures.Values)
                {
                    if (this.ContainsTexture(siblingTexture)) this.Textures[siblingTexture.StreamName].IncreasePresenceCount();
                }
            }
        }

        /// <summary>
        /// Searches through this object and it's children to see if the given objname is present in the binding objects
        /// </summary>
        /// <param name="objName">The name of the object to find</param>
        /// <param name="parentsToObjectList"></param>
        /// <returns></returns>
        public Object SearchHierarchyForObjectFromString(String objName, List<Object> parentsToObjectList)
        {
            foreach (Object obj in m_BindingObjects)
            {
                if (obj is Texture)
                {
                    if (((Texture)obj).StreamName == objName)
                    {
                        parentsToObjectList.Add(this);
                        return obj;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Finds the first occurance to the given obj name
        /// </summary>
        /// <param name="objName"></param>
        /// <param name="parentsToObject">Lists the parents it has to go through to get to that object</param>
        /// <returns>The object it found</returns>
        public Object FindFirst(String objName, List<Object> parentsToObject, Boolean matchWhole, Boolean caseSensitive)
        {
            Object found = DoesObjectBelongToBindingObjects(objName, matchWhole, caseSensitive);
            if (found != null)
            {
                parentsToObject.Add(this);
                UserTXD parent = this.ParentUserTXD;
                while (parent != null)
                {
                    parentsToObject.Add(parent);
                    parent = parent.ParentUserTXD;
                }
                parentsToObject.Reverse();
                return found;
            }

            return null;
        }

        /// <summary>
        /// Finds the last occurance to the given obj name
        /// </summary>
        /// <param name="objName"></param>
        /// <param name="parentsToObject">Lists the parents it has to go through to get to that object</param>
        /// <returns>The object it found</returns>
        public Object FindLast(String objName, List<Object> parentsToObject, Boolean matchWhole, Boolean caseSensitive)
        {
            Object found = null;
            foreach (Object obj in m_BindingObjects.Reverse<Object>())
            {
                if (obj is Texture)
                {
                    if (matchWhole == true)
                    {
                        if (String.Compare(((Texture)obj).Name, objName, !caseSensitive) == 0)
                        {
                            found = obj;
                            break;
                        }
                    }
                    else
                    {
                        if (((Texture)obj).Name.StartsWith(objName, !caseSensitive, null))
                        {
                            found = obj;
                            break;
                        }
                    }
                }
            }

            if (found != null)
            {
                parentsToObject.Add(this);
                UserTXD parent = this.ParentUserTXD;
                while (parent != null)
                {
                    parentsToObject.Add(parent);
                    parent = parent.ParentUserTXD;
                }
                parentsToObject.Reverse();
                return found;
            }

            if (matchWhole == true)
            {
                if (String.Compare(this.Name, objName, !caseSensitive) == 0)
                {
                    found = this;
                }
            }
            else
            {
                if (this.Name.StartsWith(objName, !caseSensitive, null))
                {
                    found = this;
                }
            }
            if (found != null)
            {
                UserTXD parent = this.ParentUserTXD;
                while (parent != null)
                {
                    parentsToObject.Add(parent);
                    parent = parent.ParentUserTXD;
                }
                parentsToObject.Reverse();
                return found;
            }

            return null;
        }

        public void FindAll(String objName, Collection<List<Object>> parentsToObjects, Collection<Object> results, Boolean matchWhole, Boolean caseSensitive)
        {
            // Detemine if this map is valid for the search
            Object found = null;
            if (matchWhole == true)
            {
                if (String.Compare(this.Name, objName, !caseSensitive) == 0)
                {
                    found = this;
                }
            }
            else
            {
                if (this.Name.StartsWith(objName, !caseSensitive, null))
                {
                    found = this;
                }
            }
            if (found != null)
            {
                List<Object> newParentList = new List<Object>();
                UserTXD parent = this.ParentUserTXD;
                while (parent != null)
                {
                    newParentList.Add(parent);
                    parent = parent.ParentUserTXD;
                }
                newParentList.Reverse();
                parentsToObjects.Add(newParentList);
                results.Add(this);
            }

            // Go through all the textures and determine if any of them are valid for the search
            foreach (Object childTexture in BindingObjects)
            {
                Texture foundTexture = null;
                if (matchWhole == true)
                {
                    if (String.Compare(((Texture)childTexture).Name, objName, !caseSensitive) == 0)
                    {
                        foundTexture = (Texture)childTexture;
                    }
                }
                else
                {
                    if (((Texture)childTexture).Name.StartsWith(objName, !caseSensitive, null))
                    {
                        foundTexture = (Texture)childTexture;
                    }
                }
                if (foundTexture != null)
                {
                    List<Object> newParentList = new List<Object>();
                    newParentList.Add(this);
                    UserTXD parent = this.ParentUserTXD;
                    while (parent != null)
                    {
                        newParentList.Add(parent);
                        parent = parent.ParentUserTXD;
                    }
                    newParentList.Reverse();
                    parentsToObjects.Add(newParentList);
                    results.Add(foundTexture);
                }
            }
        }

        /// <summary>
        /// Determines the dictionary size by using the file info from the indivual textures in the stream
        /// </summary>
        public void DetermineSize()
        {
            if (this.Size == 0.0f)
            {
                foreach (Texture texture in this.Textures.Values)
                {
                    Boolean fileFound = false;
                    String completePath = String.Empty;
                    String location = System.IO.Path.Combine("K:\\", "texturesGTA5", texture.StreamName);
                    if (System.IO.File.Exists(location + ".dds"))
                    {
                        fileFound = true;
                        completePath = location + ".dds";
                    }
                    else
                    {
                        location = System.IO.Path.Combine("K:\\", "texturesGTA5lod", texture.StreamName);
                        if (System.IO.File.Exists(location + ".dds"))
                        {
                            fileFound = true;
                            completePath = location + ".dds";
                        }
                    }

                    if (fileFound)
                    {
                        System.IO.FileInfo fileInfo = new System.IO.FileInfo(completePath);
                        float textureSize = (float)fileInfo.Length / 1024.0f;
                        textureSize = ((float)((int)(textureSize * 10.0f))) / 10.0f;

                        this.Size += textureSize;
                    }
                }
            }
        }

        #endregion

        #region Private Function(s)

        private void RecurseCollectTexturesFromMaterial(MaterialDef rootMaterial)
        {
            if (rootMaterial.HasTextures)
            {
                bool bSearchForDiffuseAlpha = false;
                bool bSearchForSpecularAlpha = false;
                bool bSearchForBumpAlpha = false;
                Texture previousTexture = null;
                String previousTextureName = String.Empty;

                foreach (TextureDef texture in rootMaterial.Textures)
                {
                    String textureName = System.IO.Path.GetFileNameWithoutExtension(texture.FilePath);

                    // Need to have the texture type to merge textures at this stage so that the diffuse/normal/specular alpha textures don't show.
                    if (!m_Textures.ContainsKey(textureName))
                    {
                        if (texture.Type != TextureTypes.None)
                        {
                            bool alphaTextureFound = false;
                            if (bSearchForDiffuseAlpha == true)
                            {
                                if (texture.Type == TextureTypes.DiffuseAlpha)
                                {
                                    alphaTextureFound = true;
                                }
                                bSearchForDiffuseAlpha = false;
                            }
                            else if (bSearchForSpecularAlpha == true)
                            {
                                if (texture.Type == TextureTypes.DiffuseAlpha)
                                {
                                    alphaTextureFound = true;
                                }
                                bSearchForSpecularAlpha = false;
                            }
                            else if (bSearchForBumpAlpha == true)
                            {
                                if (texture.Type == TextureTypes.DiffuseAlpha)
                                {
                                    alphaTextureFound = true;
                                }
                                bSearchForBumpAlpha = false;
                            }

                            if (alphaTextureFound)
                            {
                                previousTexture.AlphaFilepath = texture.FilePath;
                                previousTexture.Name += (" with alpha " + textureName);
                                previousTexture.StreamName = previousTextureName + textureName;

                                if (m_Textures.ContainsKey(previousTextureName + textureName))
                                {
                                    // The diffuse texture will be in binding objects so remove it
                                    m_BindingObjects.RemoveAt(m_BindingObjects.Count - 1);
                                }
                                m_Textures.Remove(previousTextureName);
                                if (!m_Textures.ContainsKey(previousTextureName + textureName)) m_Textures.Add(previousTextureName + textureName, previousTexture);
                                previousTexture = null;
                                continue;
                            }

                            // If this is a diffuse/specular/bump texture then the next one could well be a diffuse/specular/bump alpha texture
                            if (texture.Type == TextureTypes.DiffuseMap)
                            {
                                bSearchForDiffuseAlpha = true;
                            }
                            else if (texture.Type == TextureTypes.SpecularMap)
                            {
                                bSearchForSpecularAlpha = true;
                            }
                            else if (texture.Type == TextureTypes.BumpMap)
                            {
                                bSearchForBumpAlpha = true;
                            }

                            // Shouldn't add any alpha textures as indiviual textures
                            if (texture.Type == TextureTypes.DiffuseAlpha || texture.Type == TextureTypes.BumpAlpha || texture.Type == TextureTypes.SpecularAlpha)
                            {
                                continue;
                            }

                            Texture newTexture = new Texture(texture.FilePath, this, texture.Type);

                            m_Textures.Add(textureName, newTexture);
                            m_BindingObjects.Add(newTexture);
                            previousTexture = newTexture;
                            previousTextureName = textureName;
                        }
                    }
                }
            }

            if (rootMaterial.HasSubMaterials)
            {
                foreach (MaterialDef childMaterial in rootMaterial.SubMaterials)
                {
                    RecurseCollectTexturesFromMaterial(childMaterial);
                }
            }
        }

        private void Deserialise(XPathNavigator navigator)
        {
            XPathNodeIterator dictionaryAttrIt = navigator.Select(XPathExpression.Compile("@*"));
            while (dictionaryAttrIt.MoveNext())
            {
                if (typeof(String) != dictionaryAttrIt.Current.ValueType)
                    continue;
                String value = (dictionaryAttrIt.Current.TypedValue as String);

                if (dictionaryAttrIt.Current.Name == "Name")
                    this.m_sName = value;
            }

            // Load any textures this mapTXD has
            XPathNodeIterator textureIt = navigator.Select(XPathExpression.Compile("Textures/Texture"));
            while (textureIt.MoveNext())
            {
                XPathNavigator userNavigator = textureIt.Current;
                Texture newTexture = new Texture(userNavigator, this);

                this.m_Textures.Add(newTexture.StreamName, newTexture);
                this.m_BindingObjects.Add(newTexture);
            }
        }

        /// <summary>
        /// Determines whether the given obj name has a object in the binding list and if so returns the
        /// object it found.
        /// </summary>
        /// <param name="objName"></param>
        /// <param name="matchWhole"></param>
        /// <param name="caseSensitive"></param>
        /// <returns></returns>
        private Object DoesObjectBelongToBindingObjects(String objName, Boolean matchWhole, Boolean caseSensitive)
        {
            foreach (Object obj in m_BindingObjects)
            {
                if (obj is Texture)
                {
                    if (matchWhole == true)
                    {
                        if (String.Compare(((Texture)obj).Name, objName, !caseSensitive) == 0)
                        {
                            return obj;
                        }
                    }
                    else
                    {
                        if (((Texture)obj).Name.StartsWith(objName, !caseSensitive, null))
                        {
                            return obj;
                        }
                    }
                }
            }

            return null;
        }

        #endregion

    }
}
