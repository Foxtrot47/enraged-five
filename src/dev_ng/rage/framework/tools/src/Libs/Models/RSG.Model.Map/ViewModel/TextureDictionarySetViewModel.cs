﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Editor;

namespace RSG.Model.Map.ViewModel
{
    public class TextureDictionarySetViewModel
        : UserDataViewModelBase
    {
        #region Properties

        /// <summary>
        /// The model reference that this view model represents.
        /// </summary>
        public TextureDictionarySet Model
        {
            get { return m_model; }
            set { m_model = value; }
        }
        private TextureDictionarySet m_model;

        #endregion // Properties

        #region Constructor

        /// <summary>
        /// Default Constructor
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="model"></param>
        public TextureDictionarySetViewModel(IViewModel parent, TextureDictionarySet model)
        {
            this.Parent = parent;
            this.Model = model;

            foreach (TextureDictionary dictionary in model)
            {
                TextureDictionaryViewModel newDictionary = new TextureDictionaryViewModel(this, dictionary);
                this.Children.Add(newDictionary);
            }
        }

        #endregion // Constructor

        public override IViewModel GetChildWithString(String name)
        {
            foreach (TextureDictionaryViewModel child in this.Children)
            {
                if (String.Compare(child.Model.Name, name, true) == 0)
                {
                    return child as IViewModel;
                }
            }

            return null;
        }

    } // TextureDictionarySetViewModel
} // RSG.Model.Map.ViewModel
