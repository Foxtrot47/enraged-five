﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.SceneXml;
using RSG.SceneXml.Statistics;
using RSG.Base.Math;

namespace RSG.Model.Map.Statistics
{
    #region Enum

    public enum DrawableType
    {
        UNKNOWN,
        HIGH_DETAIL,
        LOD,
        SUPER_LOD
    }

    public enum StatisticLevel
    {
        HIGH_DETAIL,
        LOD,
        SUPER_LOD,
        TOTAL
    }

    #endregion // Enum

    /// <summary>
    /// The statistics object that is attached to every map secion,
    /// the statistics are got from both the scene xml statistic object and
    /// a port process set to get higher level stats like neighbour and prop
    /// statistics
    /// </summary>
    public class MapContainer
        : Base
    {
        #region Properties

        public Dictionary<String, LevelReferenceStatistics> ReferenceStats
        {
            get;
            set;
        }

        public Dictionary<String, int> ReferenceCounts
        {
            get;
            set;
        }

        /// <summary>
        /// Represents whether this container contains any valid
        /// statistics
        /// </summary>
        public Boolean ValidStatistics
        {
            get { return m_validStatistics; }
            private set { m_validStatistics = value; }
        }
        private Boolean m_validStatistics = false;

        /// <summary>
        /// Represents whether this container contains valid geometry
        /// statistics
        /// </summary>
        public Boolean ValidGeometryStatistics
        {
            get { return m_validGeometryStatistics; }
            private set { m_validGeometryStatistics = value; }
        }
        private Boolean m_validGeometryStatistics = false;

        /// <summary>
        /// Represents whether this container contains valid collision
        /// statistics
        /// </summary>
        public Boolean ValidCollisionStatistics
        {
            get { return m_validCollisionStatistics; }
            private set { m_validCollisionStatistics = value; }
        }
        private Boolean m_validCollisionStatistics = false;

        /// <summary>
        /// Represents whether this container contains valid txd
        /// statistics
        /// </summary>
        public Boolean ValidTxdStatistics
        {
            get { return m_validTxdStatistics; }
            private set { m_validTxdStatistics = value; }
        }
        private Boolean m_validTxdStatistics = false;

        /// <summary>
        /// Contains the reference statistics, indexed first by source filename, then by reference ref name, and contains
        /// both the instance count and the prop statistics
        /// </summary>
        public SectionReferences References
        {
            get { return m_references; }
            set { m_references = value; }
        }
        private SectionReferences m_references;

        /// <summary>
        /// Represents whether this container has a prop group attached to
        /// it.
        /// </summary>
        public Boolean HasPropGroup
        {
            get { return m_hasPropGroup; }
            set { m_hasPropGroup = value; }
        }
        private Boolean m_hasPropGroup;

        #region Polycount

        /// <summary>
        /// The total number of polygons in the high detailed objects
        /// </summary>
        private uint HighDetailPolycount
        {
            get;
            set;
        }

        /// <summary>
        /// The total number of polygons in the lod objects
        /// </summary>
        private uint LodPolycount
        {
            get;
            set;
        }

        /// <summary>
        /// The total number of polygons in the super lod objects
        /// </summary>
        private uint SuperLodPolycount
        {
            get;
            set;
        }

        #endregion // Polycount

        #region Polydensity

        /// <summary>
        /// The area of this map section so that we can use this and the
        /// polygon counts to calculate the density
        /// </summary>
        private float InverseArea
        {
            get;
            set;
        }

        #endregion // Polydensity

        #region GeometrySize

        /// <summary>
        /// The total geometry size of the high detailed objects
        /// </summary>
        private uint HighDetailSize
        {
            get;
            set;
        }

        /// <summary>
        /// The total geometry size of the lod objects
        /// </summary>
        private uint LodSize
        {
            get;
            set;
        }

        /// <summary>
        /// The total geometry size of the super lod objects
        /// </summary>
        private uint SuperLodSize
        {
            get;
            set;
        }

        #endregion // GeometrySize

        #region ObjectCount

        /// <summary>
        /// The total number of high detailed objects
        /// </summary>
        private uint HighDetailCount
        {
            get;
            set;
        }

        /// <summary>
        /// The total number of lod objects
        /// </summary>
        private uint LodCount
        {
            get;
            set;
        }

        /// <summary>
        /// The total number of super lod objects
        /// </summary>
        private uint SuperLodCount
        {
            get;
            set;
        }

        #endregion // ObjectCount

        #region TextureDictionary

        /// <summary>
        /// A dictionary that contains the txds that are on the high detailed objects
        /// </summary>
        private Dictionary<String, uint> HighTxds
        {
            get;
            set;
        }

        /// <summary>
        /// A dictionary that contains the txds that are on the lod objects
        /// </summary>
        private Dictionary<String, uint> LodTxds
        {
            get;
            set;
        }

        /// <summary>
        /// A dictionary that contains the txds that are on the super lod objects
        /// </summary>
        private Dictionary<String, uint> SuperTxds
        {
            get;
            set;
        }

        #endregion TextureDictionary

        #region DrawDistance

        /// <summary>
        /// The maximum lod distance for the high detail objects
        /// </summary>
        public float MaxHighDetailDistance
        {
            get;
            private set;
        }

        /// <summary>
        /// The maximum lod distance for the lod objects
        /// </summary>
        public float MaxLodDistance
        {
            get;
            private set;
        }

        /// <summary>
        /// The maximum lod distance for the super lod objects
        /// </summary>
        public float MaxSuperLodDistance
        {
            get;
            set;
        }

        /// <summary>
        /// The maximum draw distance for all the references
        /// </summary>
        public float MaxReferenceDistance
        {
            get;
            set;
        }

        #endregion // DrawDistance

        #region Collision

        /// <summary>
        /// The collision polycount for the high detail objects
        /// </summary>
        private uint HighDetailCollisionPolycount
        {
            get;
            set;
        }

        /// <summary>
        /// The collision polycount for the lod objects
        /// </summary>
        private uint LodCollisionPolycount
        {
            get;
            set;
        }

        /// <summary>
        /// The collision polycount for the super lod objects
        /// </summary>
        private uint SuperLodCollisionPolycount
        {
            get;
            set;
        }

        /// <summary>
        /// The collision count for the high detail objects
        /// </summary>
        private uint HighDetailCollisionCount
        {
            get;
            set;
        }

        /// <summary>
        /// The collision count for the lod objects
        /// </summary>
        private uint LodCollisionCount
        {
            get;
            set;
        }

        /// <summary>
        /// The collision count for the super lod objects
        /// </summary>
        private uint SuperLodCollisionCount
        {
            get;
            set;
        }

        #endregion // Collision

        #region Progress

        public Boolean FirstPassInProgress
        {
            get;
            set;
        }

        public Boolean SecondPassInProgress
        {
            get;
            set;
        }

        public Boolean SecondPassComplete
        {
            get;
            set;
        }

        public Boolean ThirdPassComplete
        {
            get;
            set;
        }

        public Boolean Completed
        {
            get;
            set;
        }

        #endregion // Progress

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public MapContainer(MapSection section, LevelReferences references)
        {
            this.HighDetailPolycount = 0;
            this.LodPolycount = 0;
            this.SuperLodPolycount = 0;

            this.HighDetailSize = 0;
            this.LodSize = 0;
            this.SuperLodSize = 0;

            this.HighDetailCount = 0;
            this.LodCount = 0;
            this.SuperLodCount = 0;

            this.HighTxds = new Dictionary<String, uint>();
            this.LodTxds = new Dictionary<String, uint>();
            this.SuperTxds = new Dictionary<String, uint>();

            this.MaxHighDetailDistance = 0.0f;
            this.MaxLodDistance = 0.0f;
            this.MaxSuperLodDistance = 0.0f;
            this.MaxReferenceDistance = 0.0f;

            this.HighDetailCollisionPolycount = 0;
            this.LodCollisionPolycount = 0;
            this.SuperLodCollisionPolycount = 0;

            this.HighDetailCollisionCount = 0;
            this.LodCollisionCount = 0;
            this.SuperLodCollisionCount = 0;

            this.InverseArea = 0.0f;

            this.References = new SectionReferences();
            this.ReferenceStats = new Dictionary<String, LevelReferenceStatistics>();
            this.ReferenceCounts = new Dictionary<String, int>();
            this.HasPropGroup = false;

            this.FirstPassInProgress = false;
            this.SecondPassInProgress = false;
            this.SecondPassComplete = false;
            this.ThirdPassComplete = false;
            this.Completed = false;

            Scene scene = SceneManager.GetScene(section.SceneXmlFilename);
            if (scene != null)
            {
                // Get the container attributes
                foreach (TargetObjectDef sceneObject in scene.Objects)
                {
                    if (sceneObject.IsContainer())
                    {
                        this.FirstPassInProgress = sceneObject.GetAttribute(AttrNames.BLOCK_FIRST_PASS, AttrDefaults.BLOCK_FIRST_PASS);

                        if (sceneObject.GetAttribute(AttrNames.BLOCK_SECOND_PASS_STARTED, AttrDefaults.BLOCK_SECOND_PASS_STARTED) &&
                            !sceneObject.GetAttribute(AttrNames.BLOCK_SECOND_PASS_COMPLETE, AttrDefaults.BLOCK_SECOND_PASS_COMPLETE))
                        {
                            this.SecondPassInProgress = true;
                        }

                        this.SecondPassComplete = sceneObject.GetAttribute(AttrNames.BLOCK_SECOND_PASS_COMPLETE, AttrDefaults.BLOCK_SECOND_PASS_COMPLETE);
                        this.ThirdPassComplete = sceneObject.GetAttribute(AttrNames.BLOCK_THIRD_PASS_COMPLETE, AttrDefaults.BLOCK_THIRD_PASS_COMPLETE);
                        this.Completed = sceneObject.GetAttribute(AttrNames.BLOCK_COMPLETE, AttrDefaults.BLOCK_COMPLETE);
                        break;
                    }
                }

                Stats sceneStatistics = scene.Statistics;
                if (sceneStatistics != null)
                {
                    this.ValidStatistics = sceneStatistics.ValidStatistics;
                    this.ValidGeometryStatistics = sceneStatistics.GeometryStats.Count > 0 ? true : false;
                    this.ValidCollisionStatistics = sceneStatistics.CollisionStats.Count > 0 ? true : false;
                    this.ValidTxdStatistics = sceneStatistics.TxdStats.Count > 0 ? true : false;

                    if (this.ValidGeometryStatistics == true)
                    {
                        GenerateGeometryStatistics(scene, references);
                    }

                    if (this.ValidCollisionStatistics == true)
                    {
                        GenerateCollisionStatistics(scene);
                    }

                    if (this.ValidTxdStatistics == true)
                    {
                        GenerateTxdStatistics(scene);
                    }
                }
            }
        }

        #endregion // Constructor(s)

        #region Statistics Creation

        /// <summary>
        /// Generates all the statistics for this container using the geometry statistics
        /// </summary>
        private void GenerateGeometryStatistics(Scene scene, LevelReferences references)
        {
            IDictionary<Guid, DrawableStats> statistics = scene.Statistics.GeometryStats;

            BoundingBox3f sectionBoundingBox = new BoundingBox3f();
            foreach (DrawableStats drawableStat in statistics.Values)
            {
                TargetObjectDef drawable = scene.FindObject(drawableStat.Guid);
                if (drawable != null && drawable.DontExport() == false && drawable.DontExportIDE() == false && drawable.DontExportIPL() == false)
                {
                    if (drawable.IsXRef() || drawable.IsInternalRef() || drawable.IsRefObject())
                    {
                        this.GenerateReference(drawable, references);
                    }
                    else
                    {
                        sectionBoundingBox.Expand(drawable.WorldBoundingBox);
                        DrawableType drawableType = GetDrawableType(drawable);
                        switch (drawableType)
                        {
                            case DrawableType.HIGH_DETAIL:
                                this.HighDetailPolycount += drawableStat.PolyCount;
                                this.HighDetailSize += drawableStat.Size;
                                this.HighDetailCount++;
                                if (drawable.GetLODDistance() > this.MaxHighDetailDistance)
                                {
                                    this.MaxHighDetailDistance = drawable.GetLODDistance();
                                }
                                break;
                            case DrawableType.LOD:
                                this.LodPolycount += drawableStat.PolyCount;
                                this.LodSize += drawableStat.Size;
                                this.LodCount++;
                                if (drawable.GetLODDistance() > this.MaxLodDistance)
                                {
                                    this.MaxLodDistance = drawable.GetLODDistance();
                                }
                                break;
                            case DrawableType.SUPER_LOD:
                                this.SuperLodPolycount += drawableStat.PolyCount;
                                this.SuperLodSize += drawableStat.Size;
                                this.SuperLodCount++;
                                if (drawable.GetLODDistance() > this.MaxSuperLodDistance)
                                {
                                    this.MaxSuperLodDistance = drawable.GetLODDistance();
                                }
                                break;
                            case DrawableType.UNKNOWN:
                                break;
                        }
                    }
                }
            }

            float sectionWidth = sectionBoundingBox.Max.X - sectionBoundingBox.Min.X;
            float sectionLength = sectionBoundingBox.Max.Y - sectionBoundingBox.Min.Y;
            float sectionHeight = sectionBoundingBox.Max.Z - sectionBoundingBox.Min.Z;
            if (sectionWidth > 0.1f && sectionLength > 0.1f && sectionHeight > 0.1f)
            {
                this.InverseArea = 1.0f / (sectionWidth * sectionLength * sectionHeight);
            }
        }

        /// <summary>
        /// Generates that statistics for the container that are to do with the collision statistics
        /// </summary>
        /// <param name="scene"></param>
        private void GenerateCollisionStatistics(Scene scene)
        {
            IDictionary<Guid, DrawableStats> statistics = scene.Statistics.CollisionStats;

            foreach (DrawableStats drawableStat in statistics.Values)
            {
                TargetObjectDef drawable = scene.FindObject(drawableStat.Guid);
                if (drawable != null)
                {
                    DrawableType drawableType = GetDrawableType(drawable);
                    switch (drawableType)
                    {
                        case DrawableType.HIGH_DETAIL:
                            this.HighDetailCollisionCount++;
                            this.HighDetailCollisionPolycount += drawableStat.PolyCount;
                            break;
                        case DrawableType.LOD:
                            this.LodCollisionCount++;
                            this.LodCollisionPolycount += drawableStat.PolyCount;
                            break;
                        case DrawableType.SUPER_LOD:
                            this.SuperLodCollisionCount++;
                            this.SuperLodCollisionPolycount += drawableStat.PolyCount;
                            break;
                        case DrawableType.UNKNOWN:
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Generates that statistics for the container that are to do with the texture dictionaries
        /// </summary>
        /// <param name="scene"></param>
        private void GenerateTxdStatistics(Scene scene)
        {
            IDictionary<String, TxdStats> statistics = scene.Statistics.TxdStats;

            foreach (TargetObjectDef sceneObject in scene.Objects)
            {
                if (sceneObject.IsXRef())
                    continue;
                if (sceneObject.IsRefObject())
                    continue;
                if (sceneObject.IsInternalRef())
                    continue;
                if (sceneObject.DontExport())
                    continue;
                if (sceneObject.DontExportIDE())
                    continue;
                if (sceneObject.DontExportIPL())
                    continue;

                if (sceneObject.IsObject())
                {
                    this.GenerateTxd(sceneObject, statistics);
                }

                if (sceneObject.IsContainer())
                {
                    foreach (TargetObjectDef child in sceneObject.Children)
                    {
                        this.GenerateTxd(child, statistics);
                    }
                }
            }
        }

        /// <summary>
        /// Determine if a object def is a high detail drawable, a lod, or a super lod,
        /// by using the lod hierarchy
        /// </summary>
        /// <returns></returns>
        private DrawableType GetDrawableType(TargetObjectDef drawable)
        {
            try
            {
                if (drawable != null)
                {
                    if (!drawable.HasLODChildren())
                    {
                        return DrawableType.HIGH_DETAIL;
                    }
                    else if (drawable.HasLODParent())
                    {
                        return DrawableType.LOD;
                    }
                    else if (drawable.LOD.Children[0] != null)
                    {
                        if (drawable.LOD.Children[0].HasLODChildren())
                        {
                            return DrawableType.SUPER_LOD;
                        }
                        else if (!drawable.LOD.Children[0].HasLODChildren())
                        {
                            return DrawableType.LOD;
                        }
                    }
                    else if (drawable.LOD.Children[0] == null)
                    {
                        foreach (TargetObjectDef child in drawable.LOD.Children)
                        {
                            if (child != null)
                            {
                                if (drawable.LOD.Children[0].HasLODChildren())
                                {
                                    return DrawableType.SUPER_LOD;
                                }
                                else
                                {
                                    return DrawableType.LOD;
                                }
                            }
                        }
                        // Cannot find any of the children in the scene xml guess at lod
                        return DrawableType.LOD;
                    }
                }

                return DrawableType.UNKNOWN;
            }
            catch
            {
                return DrawableType.UNKNOWN;
            }
        }

        /// <summary>
        /// Creates a dictionary of references that belong to this map section so that
        /// the statistics can be generated later in a second pass
        /// </summary>
        /// <param name="scene"></param>
        private void GenerateReference(TargetObjectDef reference, LevelReferences references)
        {
            LevelReferenceStatistics resolvedReference = references.GetReferenceStatistics(reference.RefFile, reference.RefName);
            if (resolvedReference != null)
            {
                UniqueLevelReference levelReference = references.ReferenceFiles[reference.RefFile][reference.RefName];
                levelReference.AppearenceCount++;
                levelReference.AppearenceLocations.Add(reference.WorldBoundingBox.Centre());

                if (this.ReferenceStats.ContainsKey(reference.RefName))
                {
                    this.ReferenceCounts[reference.RefName]++;
                }
                else
                {
                    this.ReferenceCounts.Add(reference.RefName, 1);
                    this.ReferenceStats.Add(reference.RefName, resolvedReference);
                }
            }
            this.References.AddReference(reference);
            float lodDistance = reference.GetLODDistance();
            if (lodDistance > this.MaxReferenceDistance)
            {
                this.MaxReferenceDistance = lodDistance;
            }
        }

        /// <summary>
        /// Generates a txd statistic from a scene object and the txd stats
        /// </summary>
        /// <param name="sceneObject"></param>
        /// <param name="statistics"></param>
        private void GenerateTxd(TargetObjectDef sceneObject, IDictionary<String, TxdStats> statistics)
        {
            String txdName = sceneObject.GetAttribute(AttrNames.OBJ_TXD, AttrDefaults.OBJ_TXD);
            DrawableType drawableType = GetDrawableType(sceneObject);

            switch (drawableType)
            {
                case DrawableType.HIGH_DETAIL:
                    if (!this.HighTxds.ContainsKey(txdName))
                    {
                        TxdStats statistic = null;
                        if (statistics.TryGetValue(txdName, out statistic))
                        {
                            this.HighTxds.Add(txdName, statistic.Size);
                        }
                    }
                    break;
                case DrawableType.LOD:
                    if (!this.LodTxds.ContainsKey(txdName))
                    {
                        TxdStats statistic = null;
                        if (statistics.TryGetValue(txdName, out statistic))
                        {
                            this.LodTxds.Add(txdName, statistic.Size);
                        }
                    }
                    break;
                case DrawableType.SUPER_LOD:
                    if (!this.SuperTxds.ContainsKey(txdName))
                    {
                        TxdStats statistic = null;
                        if (statistics.TryGetValue(txdName, out statistic))
                        {
                            this.SuperTxds.Add(txdName, statistic.Size);
                        }
                    }
                    break;
                case DrawableType.UNKNOWN:
                    break;
            }

        }

        /// <summary>
        /// Adds all the references from a prop group map section into this map section
        /// </summary>
        /// <param name="stats"></param>
        public void AddPropGroupStatistics(MapSection propGroup)
        {
            PropGroup stats = propGroup.SectionStatistics as PropGroup;
            if (stats != null)
            {
                foreach (KeyValuePair<String, LevelReferenceStatistics> stat in stats.ReferenceStats)
                {
                    if (this.ReferenceStats.ContainsKey(stat.Key))
                    {
                        this.ReferenceCounts[stat.Key] += stats.ReferenceCounts[stat.Key];
                    }
                    else
                    {
                        this.ReferenceCounts.Add(stat.Key, stats.ReferenceCounts[stat.Key]);
                        this.ReferenceStats.Add(stat.Key, stat.Value);
                    }
                }
            }
            float lodDistance = stats.MaxReferenceDistance;
            if (lodDistance > this.MaxReferenceDistance)
            {
                this.MaxReferenceDistance = lodDistance;
            }
        }

        #endregion // Statistics Creation

        #region Statistics Collection

        public int GetTotalSize(Boolean highDetail, Boolean lod, Boolean superLod, Boolean referenced, Boolean justUnique)
        {
            return GetGeometrySize(highDetail, lod, superLod, referenced, justUnique) + GetTXDSize(highDetail, lod, superLod, referenced);
        }

        public int GetGeometrySize(Boolean highDetail, Boolean lod, Boolean superLod, Boolean referenced, Boolean justUnique)
        {
            int size = 0;

            if (highDetail == true)
            {
                size += (int)this.HighDetailSize;
            }
            if (lod == true)
            {
                size += (int)this.LodSize;
            }
            if (superLod == true)
            {
                size += (int)this.SuperLodSize;
            }
            if (referenced == true)
            {
                foreach (var reference in this.ReferenceStats)
                {
                    if (justUnique == true)
                    {
                        size += ((int)reference.Value.GeometrySize * this.ReferenceCounts[reference.Key]);
                    }
                    else
                    {
                        size += (int)reference.Value.GeometrySize;
                    }
                }
            }

            return size;
        }

        public int GetTXDSize(Boolean highDetail, Boolean lod, Boolean superLod, Boolean referenced)
        {
            List<String> uniqueTxds = new List<String>();
            int size = 0;
            if (highDetail == true)
            {
                foreach (KeyValuePair<String, uint> txd in this.HighTxds)
                {
                    if (!uniqueTxds.Contains(txd.Key))
                    {
                        uniqueTxds.Add(txd.Key);
                        size += (int)txd.Value;
                    }
                }
            }
            if (lod == true)
            {
                foreach (KeyValuePair<String, uint> txd in this.LodTxds)
                {
                    if (!uniqueTxds.Contains(txd.Key))
                    {
                        uniqueTxds.Add(txd.Key);
                        size += (int)txd.Value;
                    }
                }
            }
            if (superLod == true)
            {
                foreach (KeyValuePair<String, uint> txd in this.SuperTxds)
                {
                    if (!uniqueTxds.Contains(txd.Key))
                    {
                        uniqueTxds.Add(txd.Key);
                        size += (int)txd.Value;
                    }
                }
            }
            if (referenced == true)
            {
                foreach (var reference in this.ReferenceStats)
                {
                    foreach (var txd in reference.Value.Txds)
                    {
                        if (!uniqueTxds.Contains(txd.Key))
                        {
                            uniqueTxds.Add(txd.Key);
                            size += (int)txd.Value;
                        }
                    }
                }
            }

            return size;
        }

        public int GetGeometryCount(Boolean highDetail, Boolean lod, Boolean superLod, Boolean referenced, Boolean justUnique)
        {
            int count = 0;

            if (highDetail == true)
            {
                count += (int)this.HighDetailCount;
            }
            if (lod == true)
            {
                count += (int)this.LodCount;
            }
            if (superLod == true)
            {
                count += (int)this.SuperLodCount;
            }
            if (referenced == true)
            {
                foreach (var reference in this.ReferenceCounts)
                {
                    if (justUnique == true)
                    {
                        count += (int)reference.Value;
                    }
                    else
                    {
                        count++;
                    }
                }
            }

            return count;
        }

        public int GetTXDCount(Boolean highDetail, Boolean lod, Boolean superLod, Boolean referenced)
        {
            int count = 0;

            if (highDetail == true)
            {
                count += (int)this.HighDetailCount;
            }
            if (lod == true)
            {
                count += (int)this.LodCount;
            }
            if (superLod == true)
            {
                count += (int)this.SuperLodCount;
            }
            if (referenced == true)
            {
                foreach (var reference in this.ReferenceStats)
                {
                    count += (int)reference.Value.Txds.Count;
                }
            }

            return count;
        }

        public int GetPolygonCount(Boolean highDetail, Boolean lod, Boolean superLod, Boolean referenced, Boolean justUnique)
        {
            int count = 0;

            if (highDetail == true)
            {
                count += (int)this.HighDetailPolycount;
            }
            if (lod == true)
            {
                count += (int)this.LodPolycount;
            }
            if (superLod == true)
            {
                count += (int)this.SuperLodPolycount;
            }
            if (referenced == true)
            {
                foreach (var reference in this.ReferenceStats)
                {
                    if (justUnique == true)
                    {
                        count += ((int)reference.Value.Polycount * this.ReferenceCounts[reference.Key]);
                    }
                    else
                    {
                        count += (int)reference.Value.Polycount;
                    }
                }
            }

            return count;
        }

        public float GetPolygonDensity(Boolean highDetail, Boolean lod, Boolean superLod, Boolean referenced, Boolean justUnique)
        {
            int count = 0;

            if (highDetail == true)
            {
                count += (int)this.HighDetailPolycount;
            }
            if (lod == true)
            {
                count += (int)this.LodPolycount;
            }
            if (superLod == true)
            {
                count += (int)this.SuperLodPolycount;
            }
            if (referenced == true)
            {
                foreach (var reference in this.ReferenceStats)
                {
                    if (justUnique == true)
                    {
                        count += ((int)reference.Value.Polycount * this.ReferenceCounts[reference.Key]);
                    }
                    else
                    {
                        count += (int)reference.Value.Polycount;
                    }
                }
            }

            return count * this.InverseArea;
        }

        public int GetCollisionPolygonCount(Boolean highDetail, Boolean referenced, Boolean justUnique)
        {
            int count = 0;
            if (highDetail == true)
            {
                count += this.HighDetailCollisionCount >= 0 ? (int)this.HighDetailCollisionCount : 0;
            }
            if (referenced == true)
            {
                foreach (var reference in this.ReferenceStats)
                {
                    if (justUnique == true)
                    {
                        int referenceCount = reference.Value.CollisionPolycount >= 0 ? (int)reference.Value.CollisionPolycount : 0;
                        if (referenceCount < 1000)
                        {
                            count += (referenceCount * this.ReferenceCounts[reference.Key]);
                        }
                    }
                    else
                    {
                        count += reference.Value.CollisionPolycount >= 0 && reference.Value.CollisionPolycount < 1000 ? (int)reference.Value.CollisionPolycount : 0;
                    }
                }
            }

            return count;
        }

        public float GetCollisionPolygonDensity(Boolean highDetail, Boolean referenced, Boolean justUnique)
        {
            int count = 0;

            if (highDetail == true)
            {
                count += this.HighDetailCollisionCount >= 0 ? (int)this.HighDetailCollisionCount : 0;
            }
            if (referenced == true)
            {
                foreach (var reference in this.ReferenceStats)
                {
                    if (justUnique == true)
                    {
                        int referenceCount = reference.Value.CollisionPolycount >= 0 ? (int)reference.Value.CollisionPolycount : 0;
                        if (referenceCount < 1000)
                        {
                            count += (referenceCount * this.ReferenceCounts[reference.Key]);
                        }
                    }
                    else
                    {
                        count += reference.Value.CollisionPolycount >= 0 && reference.Value.CollisionPolycount < 1000 ? (int)reference.Value.CollisionPolycount : 0;
                    }
                }
            }

            return count * this.InverseArea;
        }

        public float GetCollisionPolygonRatio(Boolean highDetail, Boolean referenced, Boolean justUnique)
        {
            int collisionCount = 0;
            int polycount = 0;

            if (highDetail == true)
            {
                collisionCount += this.HighDetailCollisionCount >= 0 ? (int)this.HighDetailCollisionCount : 0;
                polycount += (int)this.HighDetailPolycount;
            }
            if (referenced == true)
            {
                foreach (var reference in this.ReferenceStats)
                {
                    if (justUnique == true)
                    {
                        int referenceCount = reference.Value.CollisionPolycount >= 0 ? (int)reference.Value.CollisionPolycount : 0;
                        if (referenceCount < 1000)
                        {
                            collisionCount += (referenceCount * this.ReferenceCounts[reference.Key]);
                        }
                        polycount += ((int)reference.Value.Polycount * this.ReferenceCounts[reference.Key]);
                    }
                    else
                    {
                        collisionCount += reference.Value.CollisionPolycount >= 0 && reference.Value.CollisionPolycount < 1000 ? (int)reference.Value.CollisionPolycount : 0;
                        polycount += (int)reference.Value.Polycount;
                    }
                }
            }

            return ((float)collisionCount / (float)polycount);
        }
        
        #endregion // Statistics Collection

        #region Override Functions

        #region Total Cost Statistics

        public override int GetHighTotalCost()
        {
            return GetHighGeometryCost() + GetHighTXDCost();
        }

        public override int GetLodTotalCost()
        {
            return GetLodGeometryCost() + GetLodTXDCost();
        }

        public override int GetSLodTotalCost()
        {
            return GetSLodGeometryCost() + GetSLodTXDCost();
        }

        #endregion // Total Cost Statistics

        #region Geometry Cost Statistics

        public override int GetHighGeometryCost()
        {
            return (int)this.HighDetailSize;
        }

        public override int GetLodGeometryCost()
        {
            return (int)this.LodSize;
        }

        public override int GetSLodGeometryCost()
        {
            return (int)this.SuperLodSize;
        }

        #endregion // Geometry Cost Statistics

        #region TXD Cost Statistics

        public override int GetHighTXDCost()
        {
            int size = 0;
            foreach (uint txd in this.HighTxds.Values)
            {
                size += (int)txd;
            }
            return size;
        }

        public override int GetLodTXDCost()
        {
            int size = 0;
            foreach (uint txd in this.LodTxds.Values)
            {
                size += (int)txd;
            }
            return size;
        }

        public override int GetSLodTXDCost()
        {
            int size = 0;
            foreach (uint txd in this.SuperTxds.Values)
            {
                size += (int)txd;
            }
            return size;
        }

        #endregion // TXD Cost Statistics

        #region Geometry Count Statistics

        public override int GetHighGeometryCount()
        {
            return (int)this.HighDetailCount;
        }

        public override int GetLodGeometryCount()
        {
            return (int)this.LodCount;
        }

        public override int GetSLodGeometryCount()
        {
            return (int)this.SuperLodCount;
        }

        #endregion // Geometry Count Statistics

        #region TXD Count Statistics

        public override int GetHighTXDCount()
        {
            return this.HighTxds.Count;
        }

        public override int GetLodTXDCount()
        {
            return this.LodTxds.Count;
        }

        public override int GetSLodTXDCount()
        {
            return this.SuperTxds.Count;
        }

        #endregion // TXD Count Statistics

        #region Polygon Count Statistics

        public override int GetHighPolygonCount()
        {
            return (int)this.HighDetailPolycount;
        }

        public override int GetLodPolygonCount()
        {
            return (int)this.LodPolycount;
        }

        public override int GetSLodPolygonCount()
        {
            return (int)this.SuperLodPolycount;
        }

        #endregion // Polygon Count Statistics

        #region Polygon Density Statistics

        public override float GetHighPolygonDensity()
        {
            return this.HighDetailPolycount * this.InverseArea;
        }

        public override float GetLodPolygonDensity()
        {
            return this.LodPolycount * this.InverseArea;
        }

        public override float GetSLodPolygonDensity()
        {
            return this.SuperLodPolycount * this.InverseArea;
        }

        #endregion // Polygon Density Statistics

        #region Collision Statistics

        public override int GetCollisionPolygonCount()
        {
            return (int)this.HighDetailCollisionPolycount;
        }

        public override float GetCollisionPolygonDensity()
        {
            return this.HighDetailCollisionPolycount * this.InverseArea;
        }

        public override float GetCollisionPolygonRatio()
        {
            return ((float)this.HighDetailCollisionPolycount / (float)this.HighDetailPolycount);
        }

        #endregion // Collision Statistics

        #region LOD Distance Statistics

        public override int GetHighLodDistance()
        {
            return (int)this.MaxHighDetailDistance;
        }

        public override int GetLodLodDistance()
        {
            return (int)this.MaxLodDistance;
        }

        public override int GetSLodLodDistance()
        {
            return (int)this.MaxSuperLodDistance;
        }

        #endregion // LOD Distance Statistics

        #region Reference Total Cost Statistics

        public override int GetAllRefTotalCost()
        {
            int geoemtryCost = 0;
            int txdCost = 0;
            List<String> uniqueTxds = new List<String>();
            foreach (var reference in this.ReferenceStats)
            {
                int referenceCount = 0;
                this.ReferenceCounts.TryGetValue(reference.Key, out referenceCount);
                if (referenceCount <= 0)
                    referenceCount = 1;

                geoemtryCost += (int)(reference.Value.GeometrySize * referenceCount);
                foreach (var txd in reference.Value.Txds)
                {
                    if (!uniqueTxds.Contains(txd.Key))
                    {
                        uniqueTxds.Add(txd.Key);
                        txdCost += (int)txd.Value;
                    }
                }
            }
            return geoemtryCost + txdCost;
        }

        public override int GetUniqueRefTotalCost()
        {
            int geoemtryCost = 0;
            int txdCost = 0;
            List<String> uniqueTxds = new List<String>();
            foreach (var reference in this.ReferenceStats)
            {
                geoemtryCost += (int)reference.Value.GeometrySize;
                foreach (var txd in reference.Value.Txds)
                {
                    if (!uniqueTxds.Contains(txd.Key))
                    {
                        uniqueTxds.Add(txd.Key);
                        txdCost += (int)txd.Value;
                    }
                }
            }

            return geoemtryCost + txdCost;
        }

        #endregion // Reference Cost Statistics

        #region Reference Geometry Cost Statistics

        public override int GetAllRefGeometryCost()
        {
            int size = 0;
            foreach (var reference in this.ReferenceStats)
            {
                int referenceCount = 0;
                this.ReferenceCounts.TryGetValue(reference.Key, out referenceCount);
                if (referenceCount <= 0)
                    referenceCount = 1;

                size += (int)(reference.Value.GeometrySize * referenceCount);
            }
            return size;
        }

        public override int GetUniqueRefGeometryCost()
        {
            int size = 0;
            foreach (var reference in this.ReferenceStats.Values)
            {
                size += (int)reference.GeometrySize;
            }
            return size;
        }

        #endregion // Reference Geometry Cost Statistics

        #region Reference TXD Statistics

        public override int GetAllRefTXDCost()
        {
            int size = 0;
            List<String> uniqueTxds = new List<String>();
            foreach (var reference in this.ReferenceStats)
            {
                foreach (var txd in reference.Value.Txds)
                {
                    if (!uniqueTxds.Contains(txd.Key))
                    {
                        uniqueTxds.Add(txd.Key);
                        size += (int)txd.Value;
                    }
                }
            }
            return size;
        }

        public override int GetAllRefTXDCount()
        {
            List<String> uniqueTxds = new List<String>();
            foreach (var reference in this.ReferenceStats)
            {
                foreach (var txd in reference.Value.Txds)
                {
                    if (!uniqueTxds.Contains(txd.Key))
                    {
                        uniqueTxds.Add(txd.Key);
                    }
                }
            }
            return uniqueTxds.Count;
        }

        #endregion // Reference TXD Statistics

        #region Reference Count Statistics

        public override int GetAllRefCount()
        {
            int count = 0;
            foreach (var reference in this.ReferenceCounts.Values)
            {
                count += (int)reference;
            }
            return count;
        }

        public override int GetUniqueRefCount()
        {
            return this.ReferenceCounts.Count;
        }

        #endregion // Reference Count Statistics

        #region Reference Polygon Count Statistics

        public override int GetReferencePolygonCountAll()
        {
            int count = 0;
            foreach (var reference in this.ReferenceStats)
            {
                int referenceCount = 0;
                this.ReferenceCounts.TryGetValue(reference.Key, out referenceCount);
                if (referenceCount <= 0)
                    referenceCount = 1;

                count += (int)(reference.Value.Polycount * referenceCount);
            }
            return count;
        }

        public override int GetReferencePolygonCountUnique()
        {
            int count = 0;
            foreach (var reference in this.ReferenceStats.Values)
            {
                count += (int)reference.Polycount;
            }
            return count;
        }

        #endregion // Reference Polygon Count Statistics

        #region Reference Polygon Density Statistics

        public override float GetReferencePolygonDensityAll()
        {
            int count = 0;
            foreach (var reference in this.ReferenceStats)
            {
                int referenceCount = 0;
                this.ReferenceCounts.TryGetValue(reference.Key, out referenceCount);
                if (referenceCount <= 0)
                    referenceCount = 1;

                count += (int)(reference.Value.Polycount * referenceCount);
            }
            return count * this.InverseArea;
        }

        public override float GetReferencePolygonDensityUnique()
        {
            int count = 0;
            foreach (var reference in this.ReferenceStats.Values)
            {
                count += (int)reference.Polycount;
            }
            return count * this.InverseArea;
        }

        #endregion // Reference Polygon Density Statistics

        #region Reference Collision Count Statistics

        public override int GetReferenceCollisionPolygonCountAll()
        {
            int count = 0;
            foreach (var reference in this.ReferenceStats)
            {
                int referenceCount = 0;
                this.ReferenceCounts.TryGetValue(reference.Key, out referenceCount);
                if (referenceCount <= 0)
                    referenceCount = 1;

                int collisionCount = reference.Value.CollisionPolycount > 0 && reference.Value.CollisionPolycount < 1000 ? (int)reference.Value.CollisionPolycount : 0;
                count += (collisionCount * referenceCount);
            }
            return count;
        }

        public override int GetReferenceCollisionPolygonCountUnique()
        {
            int count = 0;
            foreach (var reference in this.ReferenceStats)
            {
                int collisionCount = reference.Value.CollisionPolycount > 0 && reference.Value.CollisionPolycount < 1000 ? (int)reference.Value.CollisionPolycount : 0;
                count += collisionCount;
            }
            return count;
        }

        #endregion // Reference Collision Count Statistics

        #region Reference Collision Density Statistics

        public override float GetReferenceCollisionPolygonDensityAll()
        {
            int count = 0;
            foreach (var reference in this.ReferenceStats)
            {
                int referenceCount = 0;
                this.ReferenceCounts.TryGetValue(reference.Key, out referenceCount);
                if (referenceCount <= 0)
                    referenceCount = 1;

                int collisionCount = reference.Value.CollisionPolycount > 0 && reference.Value.CollisionPolycount < 1000 ? (int)reference.Value.CollisionPolycount : 0;
                count += (collisionCount * referenceCount);
            }
            return count * this.InverseArea;
        }

        public override float GetReferenceCollisionPolygonDensityUnique()
        {
            int count = 0;
            foreach (var reference in this.ReferenceStats)
            {
                int collisionCount = reference.Value.CollisionPolycount > 0 && reference.Value.CollisionPolycount < 1000 ? (int)reference.Value.CollisionPolycount : 0;
                count += collisionCount;
            }
            return count * this.InverseArea;
        }

        #endregion // Reference Collision Density Statistics

        #region Reference Collision Ratio Statistics

        public override float GetReferenceCollisionPolygonRatioAll()
        {
            return ((float)this.GetReferenceCollisionPolygonCountAll() / (float)this.GetReferencePolygonCountAll());
        }

        public override float GetReferenceCollisionPolygonRatioUnique()
        {
            return ((float)this.GetReferenceCollisionPolygonCountUnique() / (float)this.GetReferencePolygonCountUnique());
        }

        #endregion // Reference Collision Ratio Statistics

        #region Reference LOD Distance Statistics

        public override float GetReferenceLodDistance()
        {
            return this.MaxReferenceDistance;
        }

        #endregion Reference // LOD Distance Statistics

        #endregion // Override Functions
    }
}
