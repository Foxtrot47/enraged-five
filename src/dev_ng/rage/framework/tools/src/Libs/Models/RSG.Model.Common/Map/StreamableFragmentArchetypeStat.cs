﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// Enumeration of all streamable stat a fragment object can have.
    /// </summary>
    [DependentStat(StreamableMapSectionStat.ArchetypesString)]
    public static class StreamableFragmentArchetypeStat
    {
        public const String FragmentCountString = "2AD10412-AA32-49F2-AB60-7E6DC8E6475B";
        public const String IsClothString = "4B009D42-BDD3-49B7-8C46-4A766162FFE9";

        public static readonly StreamableStat FragmentCount = new StreamableStat(FragmentCountString, true);
        public static readonly StreamableStat IsCloth = new StreamableStat(IsClothString, true);
    } // StreamableFragmentArchetypeStat
}
