﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.SceneXml.Material;

namespace RSG.Model.Common
{
    /// <summary>
    /// 
    /// </summary>
    public interface ITexture : IFileAsset, IEquatable<ITexture>, IComparable<ITexture>
    {
        /// <summary>
        /// 
        /// </summary>
        string AlphaFilename
        {
            get;
        }

        /// <summary>
        /// 
        /// </summary>
        TextureTypes TextureType
        {
            get;
        }

        /// <summary>
        /// Parent object
        /// </summary>
        IHasTextureChildren Parent
        {
            get;
        }

        IEnumerable<string> SourceFilenames
        {
            get;
        }
    }
}
