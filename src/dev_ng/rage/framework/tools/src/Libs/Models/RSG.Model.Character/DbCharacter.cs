﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Model.Asset;
using RSG.Model.Common;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common.Dto.GameAssets.Bundles;

namespace RSG.Model.Character
{
    /// <summary>
    /// 
    /// </summary>
    public class DbCharacter : Character
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor from a SceneXml file.
        /// </summary>
        public DbCharacter(String name, CharacterCategory category, ICharacterCollection parentCollection)
            : base(name, category, parentCollection)
        {
        }
        #endregion // Constructor(s)

        #region StreamableAssetContainerBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        protected override void LoadStatsInternal(IEnumerable<StreamableStat> statsToLoad)
        {
            Debug.Assert(ParentCollection is SingleBuildCharacterCollection, "Parent collection isn't an SingleBuildCharacterCollection.");
            SingleBuildCharacterCollection dbCollection = (SingleBuildCharacterCollection)ParentCollection;

            if (!AreStatsLoaded(statsToLoad))
            {
                try
                {
                    using (BuildClient client = new BuildClient())
                    {
                        CharacterStatBundleDto bundle = client.GetCharacterStat(dbCollection.BuildIdentifier, Hash.ToString());
                        LoadStatsFromDatabase(bundle);
                    }
                }
                catch (Exception e)
                {
                    Log.Log__Exception(e, "Unhandled exception while retrieving weapon stats.");
                }
            }
        }
        #endregion // StreamableAssetContainerBase Overrides
        
        #region Internal Methods
        /// <summary>
        /// Load all the statistic information from a database object
        /// </summary>
        /// <param name="stats"></param>
        internal void LoadStatsFromDatabase(CharacterStatBundleDto bundle)
        {
            // Load in the basic data
            PolyCount = bundle.PolyCount;
            CollisionCount = bundle.CollisionCount;
            BoneCount = bundle.BoneCount;
            ClothCount = bundle.ClothCount;
            ClothPolyCount = bundle.ClothPolyCount;

            // Load in the shaders/textures
            List<IShader> shaders = new List<IShader>();
            foreach (ShaderStatBundleDto shaderBundle in bundle.ShaderStats)
            {
                shaders.Add(new Shader(shaderBundle));
            }
            shaders.Sort();
            this.Shaders = shaders.ToArray();

            this.AssetChildren.Clear();
            this.AssetChildren.AddRange(Textures);

            // Next load in the platform stats
            PlatformStats = new Dictionary<RSG.Platform.Platform, ICharacterPlatformStat>();
            foreach (CharacterPlatformStatBundleDto platformStatDto in bundle.CharacterPlatformStats)
            {
                RSG.Platform.Platform platform = platformStatDto.Platform.Value;

                ICharacterPlatformStat platformStat = new CharacterPlatformStat(platform, platformStatDto.PhysicalSize, platformStatDto.VirtualSize);
                PlatformStats.Add(platform, platformStat);
            }
        }
        #endregion // Internal Methods

    } // DbCharacter
}
