﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using RSG.Base.Extensions;
using RSG.Model.Statistics.Captures;
using System.IO;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Data;
using System.Windows;

namespace RSG.Model.Report.Reports.GameStatsReports.Tests
{
    /// <summary>
    /// 
    /// </summary>
    public class StreamingMemorySmokeTest : SmokeTestBase
    {
        #region Constants
        private const string NAME = "Streaming Memory";
        #endregion // Constants

        #region Private Classes
        #endregion // Private Methods

        #region Properties
        /// <summary>
        /// Test name -> list of streaming memory results for the latest build
        /// </summary>
        public IDictionary<string, IList<StreamingMemoryResult>> LatestStats
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public StreamingMemorySmokeTest()
            : base(NAME)
        {
        }
        #endregion // Constructor(s)

        #region SmokeTestBase Methods
        /// <summary>
        /// Executes the particular test
        /// </summary>
        /// <param name="testSessions">List of test sessions</param>
        public override void RunTest(IList<TestSession> testSessions)
        {
            base.RunTest(testSessions);

            LatestStats = GatherLatestBuildStats(testSessions.FirstOrDefault());
        }

        /// <summary>
        /// Generates test specific graphs for this smoke test
        /// </summary>
        /// <param name="test"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns>Null if the smoke test doesn't require a graph</returns>
        public override IList<GraphImage> GenerateGraphs(DateTime start, DateTime end)
        {
            IList<GraphImage> allGraphs = new List<GraphImage>();

            // Generate a couple of graphs for each  test
            foreach (KeyValuePair<string, IList<StreamingMemoryResult>> pair in LatestStats)
            {
                string testName = pair.Key;

                // For all the unique category names
                foreach (string categoryName in pair.Value.SelectMany(result => result.Categories.Keys).Distinct())
                {
                    // Create the pie graph
                    Dictionary<string, StreamingMemoryStat> stats = new Dictionary<string, StreamingMemoryStat>();
                    foreach (StreamingMemoryResult result in pair.Value)
                    {
                        if (result.Categories.ContainsKey(categoryName))
                        {
                            StreamingMemoryStat stat = result.Categories[categoryName];

                            if ((stat.Physical != 0 || stat.Virtual != 0) && !stats.ContainsKey(result.ModuleName))
                            {
                                stats.Add(result.ModuleName, stat);
                            }
                        }
                    }

                    if (stats.Count > 0)
                    {
                        Stream imageStream = GenerateColumnGraph(testName, categoryName, stats);
                        string graphName = String.Format("streaming_column_{0}_for_{1}", categoryName, testName.Replace("(", "").Replace(")", "").Replace(" ", ""));
                        GraphImage image = new GraphImage(graphName, imageStream);

                        if (!PerTestGraphs.ContainsKey(testName))
                        {
                            PerTestGraphs.Add(testName, new List<GraphImage>());
                        }

                        PerTestGraphs[testName].Add(image);
                        allGraphs.Add(image);
                    }
                }
            }

            return allGraphs;
        }

        /// <summary>
        /// Write this test's portion of the report
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="test">The name of the test for which we wish to write the report</param>
        /// <param name="includeHeader">Whether we should output a header for this report</param>
        /// <param name="tableResults"></param>
        public override void WriteReport(HtmlTextWriter writer, string test, bool includeHeader, uint tableResults, bool email)
        {
            base.WriteReport(writer, test, includeHeader, tableResults, email);
        }
        #endregion // SmokeTestBase Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <returns></returns>
        private IDictionary<string, IList<StreamingMemoryResult>> GatherLatestBuildStats(TestSession session)
        {
            IDictionary<string, IList<StreamingMemoryResult>> latestStats = new Dictionary<string, IList<StreamingMemoryResult>>();

            if (session != null)
            {
                foreach (TestResults test in session.TestResults)
                {
                    if (!latestStats.ContainsKey(test.Name))
                    {
                        latestStats.Add(test.Name, new List<StreamingMemoryResult>());
                    }
                    latestStats[test.Name].AddRange(test.StreamingMemoryResults);
                }
            }

            return latestStats;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="testName"></param>
        /// <param name="setName"></param>
        /// <param name="stats"></param>
        /// <returns></returns>
        private Stream GenerateColumnGraph(string testName, string categoryName, IEnumerable<KeyValuePair<string, StreamingMemoryStat>> stats)
        {
            Chart chart = new Chart();
            chart.BeginInit();
            chart.Width = 1280;
            chart.Height = 600;
            chart.Title = String.Format("Streaming Memory Stats Breakdown for '{0}' ({1} Category)", testName, categoryName);
            chart.LegendTitle = "Memory Type";
            chart.EndInit();

            RenderTargetBitmap renderTarget = new RenderTargetBitmap((int)chart.Width, (int)chart.Height, 96d, 96d, PixelFormats.Default);

            ColumnSeries gvSeries = new ColumnSeries
            {
                Title = "Virtual",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Key"),
                DependentValueBinding = new Binding("Value.Virtual")
            };
            chart.Series.Add(gvSeries);

            ColumnSeries gpSeries = new ColumnSeries
            {
                Title = "Physical",
                ItemsSource = stats,
                IndependentValueBinding = new Binding("Key"),
                DependentValueBinding = new Binding("Value.Physical")
            };
            chart.Series.Add(gpSeries);

            CategoryAxis xAxis = new CategoryAxis
            {
                Orientation = AxisOrientation.X,
                Title = "Category Name"
            };
            chart.Axes.Add(xAxis);

            LinearAxis yAxis = new LinearAxis
            {
                Orientation = AxisOrientation.Y,
                Title = "Kilobytes"
            };
            chart.Axes.Add(yAxis);

            chart.Measure(new Size(chart.Width, chart.Height));
            chart.Arrange(new Rect(new Size(chart.Width, chart.Height)));
            chart.UpdateLayout();

            renderTarget.Render(chart);

            // Save out the chart to a png file
            PngBitmapEncoder png = new PngBitmapEncoder();
            png.Frames.Add(BitmapFrame.Create(renderTarget));
            MemoryStream stream = new MemoryStream();
            png.Save(stream);
            stream.Position = 0;
            return stream;
        }
        #endregion // Private Methods
    } // StreamingMemorySmokeTest
}
