﻿// --------------------------------------------------------------------------------------------
// <copyright file="IHasPrecision.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
// --------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Definitions.Members
{
    /// <summary>
    /// When implemented represents a member that has a boolean property indicating whether the
    /// high precision flag on it is set or not.
    /// </summary>
    public interface IHasPrecision : IMember
    {
        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether this member has high precision on or off.
        /// </summary>
        bool HighPrecision { get; set; }
        #endregion Properties
    } // RSG.Metadata.Model.Definitions.Members.IHasPrecision {Interface}
} // RSG.Metadata.Model.Definitions.Members {Namespace}
