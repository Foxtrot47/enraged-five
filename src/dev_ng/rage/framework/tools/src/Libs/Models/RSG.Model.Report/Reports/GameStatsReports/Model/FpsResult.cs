﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Model.Report.Reports.GameStatsReports.Model
{
    /// <summary>
    /// 
    /// </summary>
    public class FpsResult
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public float Min
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public float Max
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public float Average
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public FpsResult(XElement element)
        {
            // Extract the various stats
            XElement minElement = element.Element("min");
            if (minElement != null && minElement.Attribute("value") != null)
            {
                Min = Single.Parse(minElement.Attribute("value").Value);
            }

            XElement maxElement = element.Element("max");
            if (maxElement != null && maxElement.Attribute("value") != null)
            {
                Max = Single.Parse(maxElement.Attribute("value").Value);
            }

            XElement avgElement = element.Element("average");
            if (avgElement != null && avgElement.Attribute("value") != null)
            {
                Average = Single.Parse(avgElement.Attribute("value").Value);
            }
        }
        #endregion // Constructor(s)
    } // FpsResult
}
