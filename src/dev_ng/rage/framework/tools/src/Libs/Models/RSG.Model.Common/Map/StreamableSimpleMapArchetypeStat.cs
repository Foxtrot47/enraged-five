﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// Base class for streamable simple map archetype stat enumerations.
    /// </summary>
    [DependentStat(StreamableMapSectionStat.ArchetypesString)]
    public static class StreamableSimpleMapArchetypeStat
    {
        public const String IsDynamicString = "48871690-903A-499C-BFE4-7C022864D141";
        public const String IsFixedString = "63EEE483-B521-41C8-8D92-606D67C02ED0";
        public const String CollisionGroupString = "C625FB20-E5B9-4BF2-A478-141DBBD29DA4";
        public const String ForceBakeCollisionString = "4C234941-1217-4850-B7A7-87FFEB4BF123";
        public const String NeverBakeCollisionString = "B80EE81E-F3A6-494C-BF72-5BD4674B6936";
        public const String SpawnPointsString = "905F9328-03B0-48A3-918B-D88A1C3E7DF4";
        public const String DrawableLODsString = "2E96EB8B-18A4-4322-B4E1-4C432BF27357";

        public static readonly StreamableStat IsDynamic = new StreamableStat(IsDynamicString, true);
        public static readonly StreamableStat IsFixed = new StreamableStat(IsFixedString, false);
        public static readonly StreamableStat CollisionGroup = new StreamableStat(CollisionGroupString, true);
        public static readonly StreamableStat ForceBakeCollision = new StreamableStat(ForceBakeCollisionString, true);
        public static readonly StreamableStat NeverBakeCollision = new StreamableStat(NeverBakeCollisionString, true);
        public static readonly StreamableStat SpawnPoints = new StreamableStat(SpawnPointsString, true, true);
        public static readonly StreamableStat DrawableLODs = new StreamableStat(DrawableLODsString, true, true);
    } // StreamableSimpleMapArchetypeStat
}
