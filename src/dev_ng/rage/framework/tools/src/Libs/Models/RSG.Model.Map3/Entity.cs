﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using RSG.Model.Common;
using System.ComponentModel;
using RSG.Base.Math;
using System.Diagnostics;
using RSG.SceneXml;
using System.Collections.ObjectModel;
using RSG.Statistics.Common.Dto.GameAssetStats;
using RSG.Base.Logging;
using System.Reflection;
using RSG.Model.Asset;
using rage;

namespace RSG.Model.Map3
{
    /// <summary>
    /// 
    /// </summary>
    public class Entity : StreamableAssetBase, IEntity
    {
        #region Properties
        /// <summary>
        /// The parent of this entity (can be either a section or a room)
        /// </summary>
        public IHasEntityChildren Parent
        {
            get;
            protected set;
        }

        /// <summary>
        /// 
        /// </summary>
        public IArchetype ReferencedArchetype
        {
            get
            {
                return m_referencedArchetype;
            }
            private set
            {
                SetPropertyValue(ref m_referencedArchetype, value, "ReferencedArchetype");
            }
        }
        private IArchetype m_referencedArchetype;

        /// <summary>
        /// Section that this entity appears in.
        /// </summary>
        public IMapSection ContainingSection
        {
            get
            {
                if (Parent is IMapSection)
                {
                    return (Parent as IMapSection);
                }
                else if (Parent is IInteriorArchetype)
                {
                    return (Parent as IInteriorArchetype).ParentSection;
                }
                else if (Parent is IRoom)
                {
                    return (Parent as IRoom).ParentArchetype.ParentSection;
                }
                else
                {
                    Debug.Assert(false, "Entity has an invalid parent type.  Unable to request stats for it.");
                    throw new NotSupportedException("Entity has an invalid parent type.  Unable to request stats for it.");
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableEntityStat.PositionString)]
        public Vector3f Position
        {
            get
            {
                CheckStatLoaded(StreamableEntityStat.Position);
                return m_position;
            }
            private set
            {
                SetPropertyValue(ref m_position, value, "Position");
                SetStatLodaded(StreamableEntityStat.Position, true);
            }
        }
        private Vector3f m_position;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableEntityStat.RotationString)]
        public Quaternionf Rotation
        {
            get
            {
                CheckStatLoaded(StreamableEntityStat.Rotation);
                return m_Rotation;
            }
            private set
            {
                SetPropertyValue(ref m_Rotation, value, "Rotation");
                SetStatLodaded(StreamableEntityStat.Rotation, true);
            }
        }
        private Quaternionf m_Rotation;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableEntityStat.TransformString)]
        public Matrix34f Transform
        {
            get
            {
                CheckStatLoaded(StreamableEntityStat.Transform);
                return m_Transform;
            }
            private set
            {
                SetPropertyValue(ref m_Transform, value, "Transform");
                SetStatLodaded(StreamableEntityStat.Transform, true);
            }
        }
        private Matrix34f m_Transform;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableEntityStat.BoundingBoxString)]
        public BoundingBox3f BoundingBox
        {
            get
            {
                CheckStatLoaded(StreamableEntityStat.BoundingBox);
                return m_boundingBox;
            }
            private set
            {
                SetPropertyValue(ref m_boundingBox, value, "BoundingBox");
                SetStatLodaded(StreamableEntityStat.BoundingBox, true);
            }
        }
        private BoundingBox3f m_boundingBox;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableEntityStat.PriorityLevelString)]
        public int PriorityLevel
        {
            get
            {
                CheckStatLoaded(StreamableEntityStat.PriorityLevel);
                return m_priorityLevel;
            }
            private set
            {
                SetPropertyValue(ref m_priorityLevel, value, "PriorityLevel");
                SetStatLodaded(StreamableEntityStat.PriorityLevel, true);
            }
        }
        private int m_priorityLevel;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableEntityStat.IsLowPriorityString)]
        public bool IsLowPriority
        {
            get
            {
                CheckStatLoaded(StreamableEntityStat.IsLowPriority);
                return m_isLowPriority;
            }
            private set
            {
                SetPropertyValue(ref m_isLowPriority, value, "IsLowPriority");
                SetStatLodaded(StreamableEntityStat.IsLowPriority, true);
            }
        }
        private bool m_isLowPriority;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableEntityStat.IsReferenceString)]
        public bool IsReference
        {
            get
            {
                CheckStatLoaded(StreamableEntityStat.IsReference);
                return m_isReference;
            }
            private set
            {
                SetPropertyValue(ref m_isReference, value, "IsReference");
                SetStatLodaded(StreamableEntityStat.IsReference, true);
            }
        }
        private bool m_isReference;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableEntityStat.IsInteriorReferenceString)]
        public bool IsInteriorReference
        {
            get
            {
                CheckStatLoaded(StreamableEntityStat.IsInteriorReference);
                return m_isInteriorReference;
            }
            private set
            {
                SetPropertyValue(ref m_isInteriorReference, value, "IsInteriorReference");
                SetStatLodaded(StreamableEntityStat.IsInteriorReference, true);
            }
        }
        private bool m_isInteriorReference;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableEntityStat.LodLevelString)]
        public LodLevel LodLevel
        {
            get
            {
                CheckStatLoaded(StreamableEntityStat.LodLevel);
                return m_lodLevel;
            }
            private set
            {
                SetPropertyValue(ref m_lodLevel, value, "LodLevel");
                SetStatLodaded(StreamableEntityStat.LodLevel, true);
            }
        }
        private LodLevel m_lodLevel;

        /// <summary>
        /// 
        /// </summary>
        public float LodDistance
        {
            get
            {
                if (this.LodParent != null && this.LodParent.LodDistanceForChildren.HasValue)
                {
                    return this.LodParent.LodDistanceForChildren.Value;
                }

                CheckStatLoaded(StreamableEntityStat.LodDistanceOverride);
                if (LodDistanceOverride.HasValue)
                {
                    return LodDistanceOverride.Value;
                }
                else
                {
                    return (ReferencedArchetype != null ? (ReferencedArchetype as IMapArchetype).LodDistance : 0.0f);
                }
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableEntityStat.LodDistanceOverrideString)]
        public float? LodDistanceOverride
        {
            get
            {
                CheckStatLoaded(StreamableEntityStat.LodDistanceOverride);
                return m_lodDistanceOverride;
            }
            private set
            {
                SetPropertyValue(ref m_lodDistanceOverride, value, "LodDistanceOverride", "LodDistance");
                SetStatLodaded(StreamableEntityStat.LodDistanceOverride, true);
            }
        }
        private float? m_lodDistanceOverride;


        /// <summary>
        /// Lod distance automatically assigned to all of its children.
        /// </summary>
        [StreamableStat(StreamableEntityStat.LodDistanceForChildrenString)]
        public float? LodDistanceForChildren
        {
            get
            {
                CheckStatLoaded(StreamableEntityStat.LodDistanceForChildren);
                return m_lodDistanceForChildren;
            }
            private set
            {
                SetPropertyValue(ref m_lodDistanceForChildren, value, "LodDistanceForChildren", "LodDistance");
                SetStatLodaded(StreamableEntityStat.LodDistanceForChildren, true);
            }
        }
        private float? m_lodDistanceForChildren;

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableEntityStat.LodParentString)]
        public IEntity LodParent
        {
            get
            {
                CheckStatLoaded(StreamableEntityStat.LodParent);
                return m_lodParent;
            }
            set
            {
                SetPropertyValue(ref m_lodParent, value, "LodParent");
                SetStatLodaded(StreamableEntityStat.LodParent, true);
            }
        }
        private IEntity m_lodParent;

        /// <summary>
        /// List of lod child. (Non streamable, request the lod parent to have this populated.)
        /// </summary>
        public ICollection<IEntity> LodChildren
        {
            get
            {
                return m_lodChildren;
            }
            private set
            {
                SetPropertyValue(ref m_lodChildren, value, "LodChildren");
            }
        }
        private ICollection<IEntity> m_lodChildren;

        /// <summary>
        /// AttrNames.OBJ_FORCE_BAKE_COLLISION attribute
        /// </summary>
        [StreamableStat(StreamableEntityStat.ForceBakeCollisionString)]
        public bool ForceBakeCollision
        {
            get
            {
                CheckStatLoaded(StreamableEntityStat.ForceBakeCollision);
                return m_forceBakeCollision;
            }
            private set
            {
                SetPropertyValue(ref m_forceBakeCollision, value, "ForceBakeCollision");
                SetStatLodaded(StreamableEntityStat.ForceBakeCollision, true);
            }
        }
        private bool m_forceBakeCollision;

        /// <summary>
        /// List of spawn points associated with this entity.
        /// </summary>
        [StreamableStatAttribute(StreamableEntityStat.SpawnPointsString)]
        public ICollection<ISpawnPoint> SpawnPoints
        {
            get
            {
                CheckStatLoaded(StreamableEntityStat.SpawnPoints);
                return m_spawnPoints;
            }
            protected set
            {
                SetPropertyValue(ref m_spawnPoints, value, "SpawnPoints");
                SetStatLodaded(StreamableEntityStat.SpawnPoints, true);
            }
        }
        private ICollection<ISpawnPoint> m_spawnPoints;

        /// <summary>
        /// Gets the guid set on this entities attributes.
        /// </summary>
        [StreamableStatAttribute(StreamableEntityStat.AttributeGuidString)]
        public Guid AttributeGuid
        {
            get
            {
                CheckStatLoaded(StreamableEntityStat.AttributeGuid);
                return m_attributeGuid;
            }
            private set
            {
                SetPropertyValue(ref m_attributeGuid, value, "AttributeGuid");
                SetStatLodaded(StreamableEntityStat.AttributeGuid, true);
            }
        }
        private Guid m_attributeGuid;

        /// <summary>
        /// Gets a value indicating whether this entity has had scaling applied to it.
        /// </summary>
        [StreamableStatAttribute(StreamableEntityStat.HasScalingString)]
        public bool HasScaling
        {
            get
            {
                CheckStatLoaded(StreamableEntityStat.HasScaling);
                return m_hasScaling;
            }
            private set
            {
                SetPropertyValue(ref m_hasScaling, value, "HasScaling");
                SetStatLodaded(StreamableEntityStat.HasScaling, true);
            }
        }
        private bool m_hasScaling;

        /// <summary>
        /// Gets a value indicating whether this entity has a link (via proxy) to SLOD2.
        /// </summary>
        [StreamableStatAttribute(StreamableEntityStat.HasSLOD2LinkString)]
        public bool HasSLOD2Link
        {
            get
            {
                CheckStatLoaded(StreamableEntityStat.HasSLOD2Link);
                return m_hasSLOD2Link;
            }
            set
            {
                SetPropertyValue(ref m_hasSLOD2Link, value, "HasSLOD2Link");
                SetStatLodaded(StreamableEntityStat.HasSLOD2Link, true);
            }
        }
        private bool m_hasSLOD2Link;

        /// <summary>
        /// Gets the number of Rage light instances this entity has underneath it.
        /// </summary>
        [StreamableStatAttribute(StreamableEntityStat.RageLightInstanceCountString)]
        public int RageLightInstanceCount
        {
            get
            {
                CheckStatLoaded(StreamableEntityStat.RageLightInstanceCount);
                return _rageLightInstanceCount;
            }
            set
            {
                SetPropertyValue(ref _rageLightInstanceCount, value, "RageLightInstanceCount");
                SetStatLodaded(StreamableEntityStat.RageLightInstanceCount, true);
            }
        }
        private int _rageLightInstanceCount;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="archetype"></param>
        /// <param name="parentSection"></param>
        public Entity(string name, IArchetype archetype, IHasEntityChildren parent)
            : base(name)
        {
            //Debug.Assert(archetype != null, "Attempting to create entity without a valid archetype reference.");
            Debug.Assert(parent != null, "An entity must have a parent.");

            ReferencedArchetype = archetype;
            if (archetype != null)
            {
                ReferencedArchetype.Entities.Add(this);
            }

            Parent = parent;

            lock (Parent.ChildEntities)
            {
                Parent.ChildEntities.Add(this);
            }

            // Set by default as this isn't streamable.
            LodChildren = new Collection<IEntity>();
        }
        #endregion // Constructor(s)

        #region IEntity Implementation
        /// <summary>
        /// Load specific statistics for this entity.
        /// </summary>
        public override void RequestStatistics(IEnumerable<StreamableStat> statsToLoad, bool async)
        {
            ContainingSection.MapHierarchy.RequestStatisticsForEntity(this, statsToLoad, async);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        public void LoadStatsFromExportData(TargetObjectDef sceneObject)
        {
            Transform = sceneObject.NodeTransform;
            Position = sceneObject.NodeTransform.Translation;
            Rotation = new Quaternionf(sceneObject.NodeTransform);

            BoundingBox = sceneObject.WorldBoundingBox;
            AttributeGuid = sceneObject.AttributeGuid;
            if (sceneObject.ObjectScale != null)
            {
                if (sceneObject.ObjectScale.X != 1.0f)
                {
                    HasScaling = true;
                }
                else if (sceneObject.ObjectScale.Y != 1.0f)
                {
                    HasScaling = true;
                }
                else if (sceneObject.ObjectScale.Z != 1.0f)
                {
                    HasScaling = true;
                }
            }

            foreach (TargetObjectDef childDef in sceneObject.Children)
            {
                if (childDef.IsLightInstance())
                {
                    this.RageLightInstanceCount++;
                }
            }

            if (sceneObject.Attributes.ContainsKey(AttrNames.OBJ_PRIORITY))
            {
                PriorityLevel = (int)sceneObject.Attributes[AttrNames.OBJ_PRIORITY];
            }
            else
            {
                PriorityLevel = 0;
            }
            IsLowPriority = sceneObject.GetAttribute(AttrNames.OBJ_LOW_PRIORITY, AttrDefaults.OBJ_LOW_PRIORITY);

            IsReference = (sceneObject.IsXRef() || sceneObject.IsRefInternalObject() || sceneObject.IsRefObject() || sceneObject.IsInternalRef());
            IsInteriorReference = ReferencedArchetype is InteriorArchetype;

            LodLevel = ConvertLodLevel(sceneObject.LODLevel);
            LodParent = null;

            if (sceneObject.Attributes.ContainsKey(AttrNames.OBJ_INSTANCE_LOD_DISTANCE))
            {
                LodDistanceOverride = sceneObject.GetLODDistance();
            }
            else
            {
                LodDistanceOverride = null;
            }

            if (sceneObject.Attributes.ContainsKey(AttrNames.OBJ_CHILD_LOD_DISTANCE))
            {
                LodDistanceForChildren = sceneObject.GetAttribute(AttrNames.OBJ_CHILD_LOD_DISTANCE, AttrDefaults.OBJ_CHILD_LOD_DISTANCE);
            }
            else
            {
                LodDistanceForChildren = null;
            }

            ForceBakeCollision = sceneObject.HasForceBakeCollisionFlag();
            SpawnPoints = RetrieveChildSpawnPoints(sceneObject);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        private ICollection<ISpawnPoint> RetrieveChildSpawnPoints(TargetObjectDef sceneObject)
        {
            IList<ISpawnPoint> spawnPoints = new List<ISpawnPoint>();
            foreach (TargetObjectDef child in sceneObject.Children.Where(item => item.Is2dfxSpawnPoint()))
            {
                spawnPoints.Add(new SpawnPoint(child, null));
            }
            return spawnPoints;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="requestedStats"></param>
        public void LoadStatsFromDatabase(EntityStatDto dto, IList<StreamableStat> requestedStats)
        {
            IDictionary<StreamableStat, PropertyInfo> modelLookup = StreamableStatUtils.GetPropertyInfoLookup(GetType());
            IDictionary<StreamableStat, PropertyInfo> dtoLookup = StreamableStatUtils.GetPropertyInfoLookup(dto.GetType());

            // Map the properties we are after.
            foreach (StreamableStat stat in requestedStats.Where(item => !IsStatLoaded(item)))
            {
                if (dtoLookup.ContainsKey(stat) && modelLookup.ContainsKey(stat))
                {
                    // Is this a complex item?
                    if (stat.DtoToModelRequiresProcessing)
                    {
                        ProcessComplexEntityStat(dto, stat);
                    }
                    else
                    {
                        object value = dtoLookup[stat].GetValue(dto, null);
                        modelLookup[stat].SetValue(this, value, null);
                    }
                }
                else
                {
#warning Readd assert once everything is marked up.
                    //Debug.Assert(false, String.Format("One of the MapSectionStat dto class or the model class doesn't have the '{0}' stat marked up.", stat));
                    Log.Log__Error(String.Format("One of the EntityStat dto class or the model class doesn't have the '{0}' stat marked up.", stat));
                }
            }
        }
        #endregion // IEntity Implementation

        #region Public Methods
        /// <summary>
        /// Return the streaming extents of this Entity object.
        /// </summary>
        /// The streaming extents is defined by the LOD distance of this 
        /// instance; and depends on whether its in a LOD hierarchy or not.
        /// If we have a LOD parent then its relative to the parent's pivot.
        /// <returns></returns>
        public BoundingBox3f GetStreamingExtents()
        {
            // Note that this doesn't take streaming extent overrides into account :/

            BoundingBox3f streamingExtents = new BoundingBox3f();
            float lod = (float)LodDistance;
            Vector3f vlod = new Vector3f(lod, lod, lod);

            if (this.LodParent != null)
            {
                streamingExtents.Expand(LodParent.Position + vlod);
                streamingExtents.Expand(LodParent.Position - vlod);
            }
            else
            {
                streamingExtents.Expand(Position + vlod);
                streamingExtents.Expand(Position - vlod);
            }

            return streamingExtents;
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        private LodLevel ConvertLodLevel(ObjectDef.LodLevel level)
        {
            switch (level)
            {
                case ObjectDef.LodLevel.HD:
                    return RSG.Model.Common.Map.LodLevel.Hd;

                case ObjectDef.LodLevel.ORPHANHD:
                    return RSG.Model.Common.Map.LodLevel.OrphanHd;

                case ObjectDef.LodLevel.LOD:
                    return RSG.Model.Common.Map.LodLevel.Lod;

                case ObjectDef.LodLevel.SLOD1:
                    return RSG.Model.Common.Map.LodLevel.SLod1;

                case ObjectDef.LodLevel.SLOD2:
                    return RSG.Model.Common.Map.LodLevel.SLod2;

                case ObjectDef.LodLevel.SLOD3:
                    return RSG.Model.Common.Map.LodLevel.SLod3;

                case ObjectDef.LodLevel.SLOD4:
                    return RSG.Model.Common.Map.LodLevel.SLod4;

                default:
                    return RSG.Model.Common.Map.LodLevel.Unknown;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="stat"></param>
        private void ProcessComplexEntityStat(EntityStatDto dto, Guid stat)
        {
            if (stat == StreamableEntityStat.LodParent)
            {
                if (dto.LodParent != null)
                {
                    IEntity parentEntity = Parent.ChildEntities.FirstOrDefault(item => item.Name == dto.LodParent.Name);
                    if (parentEntity != null)
                    {
                        LodParent = parentEntity;
                    }
                    else
                    {
                        Log.Log__Warning("Unable to resolve the lod parent entity for entity '{0}' and parent '{1}'.", Name, dto.LodParent.Name);
                    }
                }
                else
                {
                    LodParent = null;
                }
            }
        }
        #endregion // Private Methods

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return (String.Format("Entity: [{0}]{1}", this.Name, this.Hash));
        }
        #endregion // Object Overrides
    } // Entity
}
