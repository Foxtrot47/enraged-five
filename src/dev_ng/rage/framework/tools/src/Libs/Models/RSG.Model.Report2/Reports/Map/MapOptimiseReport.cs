﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using RSG.Base.ConfigParser;
using RSG.Model.Common;
using RSG.Base.Tasks;
using RSG.Model.Common.Map;

namespace RSG.Model.Report.Reports.Map
{
    /// <summary>
    /// 
    /// </summary>
    public class MapOptimiseReport : HTMLReport, IDynamicLevelReport
    {
        #region Constants
        private const String c_name = "Map Optimise Report";
        private const String c_description = "Generate and display a map optimisation report.";
        private const int    c_numSectionsToDisplay = 20;
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    CompositeTask task = new CompositeTask("Generate Report");
                    task.AddSubTask(new ActionTask("Loading data", (context, progress) => EnsureDataLoaded((DynamicLevelReportContext)context, progress)), 50);
                    task.AddSubTask(new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress)));
                    m_task = task;
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public MapOptimiseReport()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)

        #region Task Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void EnsureDataLoaded(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                float increment = 1.0f / hierarchy.AllSections.Count();

                // Load all the map section data
                foreach (IMapSection section in hierarchy.AllSections)
                {
                    context.Token.ThrowIfCancellationRequested();

                    // Update the progress information
                    string message = String.Format("Loading {0}", section.Name);
                    progress.Report(new TaskProgress(increment, true, message));

                    section.RequestAllStatistics(false);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            IMapHierarchy hierarchy = context.Level.MapHierarchy;

            if (hierarchy != null)
            {
                IDictionary<IMapSection, float> shaderDensities = new Dictionary<IMapSection, float>();
                IDictionary<IMapSection, float> polygonDensities = new Dictionary<IMapSection, float>();
                IDictionary<IMapSection, float> drawableInstanceDensities = new Dictionary<IMapSection, float>();
                IDictionary<IMapSection, float> nonDrawableDensities = new Dictionary<IMapSection, float>();
                IDictionary<IMapSection, float> uniqueNonDrawableDensities = new Dictionary<IMapSection, float>();
                GetSortedStatistics(hierarchy, ref shaderDensities, ref polygonDensities, ref drawableInstanceDensities, ref nonDrawableDensities, ref uniqueNonDrawableDensities);

                MemoryStream stream = new MemoryStream();
                StreamWriter reportFile = new StreamWriter(stream);

                HtmlTextWriter writer = new HtmlTextWriter(reportFile);
                writer.RenderBeginTag(HtmlTextWriterTag.Html);
                writer.RenderBeginTag(HtmlTextWriterTag.Head);
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Body);

                writer.RenderBeginTag(HtmlTextWriterTag.H1);
                writer.Write("Map Optimisation Report");
                writer.RenderEndTag();

                RenderStatTable(shaderDensities, writer, String.Format("The {0} Worst Sections For Shader Density", c_numSectionsToDisplay));
                RenderStatTable(polygonDensities, writer, String.Format("The {0} Worst Sections For Polygon Density", c_numSectionsToDisplay));
                RenderStatTable(drawableInstanceDensities, writer, String.Format("The {0} Worst Sections For Object Density", c_numSectionsToDisplay));
                RenderStatTable(nonDrawableDensities, writer, String.Format("The {0} Worst Sections For Reference Density", c_numSectionsToDisplay));
                RenderStatTable(uniqueNonDrawableDensities, writer, String.Format("The {0} Worst Sections For Unique Reference Density", c_numSectionsToDisplay));

                writer.RenderEndTag();
                writer.RenderEndTag();
                writer.Flush();

                stream.Position = 0;
                Stream = stream;
            }
        }
        #endregion // Task Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="shaderDensities"></param>
        /// <param name="polygonDensities"></param>
        /// <param name="drawableInstanceDensities"></param>
        /// <param name="nonDrawableDensities"></param>
        /// <param name="uniqueNonDrawableDensities"></param>
        private void GetSortedStatistics(
            IMapHierarchy hierarchy,
            ref IDictionary<IMapSection, float> shaderDensities,
            ref IDictionary<IMapSection, float> polygonDensities,
            ref IDictionary<IMapSection, float> drawableInstanceDensities,
            ref IDictionary<IMapSection, float> nonDrawableDensities,
            ref IDictionary<IMapSection, float> uniqueNonDrawableDensities)
        {
            foreach (IMapSection section in hierarchy.AllSections.Where(item => item.Area != 0.0f))
            {
                IMapSectionStatistic stats = section.AggregatedStats[new StatKey(StatGroup.AllNonInteriorEntities, StatLodLevel.Total, StatEntityType.Total)];
                shaderDensities[section] = stats.ShaderCount / section.Area;
                polygonDensities[section] = stats.PolygonCount / section.Area;

                stats = section.AggregatedStats[new StatKey(StatGroup.DrawableEntities, StatLodLevel.Total, StatEntityType.Total)];
                drawableInstanceDensities[section] = stats.TotalCount / section.Area;

                stats = section.AggregatedStats[new StatKey(StatGroup.NonDrawableEntities, StatLodLevel.Total, StatEntityType.Total)];
                nonDrawableDensities[section] = stats.TotalCount / section.Area;
                uniqueNonDrawableDensities[section] = stats.UniqueCount / section.Area;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stats"></param>
        /// <param name="writer"></param>
        /// <param name="header"></param>
        private void RenderStatTable(IDictionary<IMapSection, float> stats, HtmlTextWriter writer, string header)
        {
            writer.RenderBeginTag(HtmlTextWriterTag.H1);
            writer.Write(header);
            writer.RenderEndTag();

            // <table border="1" width="40%">
            // <tr><th>Section</th><th>Density Value</th></tr>
            writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "40%");
            writer.RenderBeginTag(HtmlTextWriterTag.Table);
            writer.RenderBeginTag(HtmlTextWriterTag.Tr);
            writer.RenderBeginTag(HtmlTextWriterTag.Th);
            writer.Write("Section");
            writer.RenderEndTag();
            writer.RenderBeginTag(HtmlTextWriterTag.Th);
            writer.Write("Density Value");
            writer.RenderEndTag();
            writer.RenderEndTag();

            foreach (KeyValuePair<IMapSection, float> pair in stats.OrderBy(item => item.Value).Reverse().Take(c_numSectionsToDisplay))
            {
                // <tr><td>Section Name</td><td>density</td></tr>
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(String.Format("{0}    ", pair.Key.Name));
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(String.Format("{0}", pair.Value));
                writer.RenderEndTag();
                writer.RenderEndTag();
            }

            // </table>
            writer.RenderEndTag();
        }
        #endregion // Private Methods
    } // MapOptimiseReport
}
