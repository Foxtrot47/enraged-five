﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Common.Animation
{
    /// <summary>
    /// IClipDictionary interface
    /// </summary>
    public interface IClipDictionary : IAsset, IEnumerable<IClip>, ICollection<IClip>
    {
        /// <summary>
        /// Type of clip dictionary this is.
        /// </summary>
        ClipDictionaryCategory Category { get; set; }

        /// <summary>
        /// Enumerable clips
        /// </summary>
        ICollection<IClip> Clips { get; }
    } // IClipDictionary
}
