﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.Tasks;
using RSG.Model.Common;
using RSG.Model.GlobalTXD;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Build;
using RSG.Pipeline.Engine;
using RSG.Pipeline.Services;

namespace RSG.Model.Report.Reports.Pipeline
{
    /// <summary>
    /// 
    /// </summary>
    /// DHM FIX ME: this should not be using RSG.Pipeline.Processor assemblies!  Invoke the 
    /// engine by invoking the conversion executable.  This is too tightly coupled to the
    /// pipeline (changes will break the Workbench).
    public class ParentTxdTextureReport : Report, IReportFileProvider, IDynamicLevelReport
    {
        #region Constants
        private const String c_name = "Parent Txd Texture Report";
        private const String c_description = "Does a prebuild of all sections looking for a mismatch between parent txd xml/zip and for textures in both parent txd file and exported map section files.";

        private const String ProcessorParentTXD = "RSG.Pipeline.Processor.Map.ParentTextureDictionary";
        private const String ProcessorTexture = "RSG.Pipeline.Processor.Map.Texture";
        private const String ProcessorMapPreProcess = "RSG.Pipeline.Processor.Map.PreProcess";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Filter used for the save dialog
        /// </summary>
        public string Filter
        {
            get
            {
                return "ULOG (Universal Log)|*.ulog";
            }
        }

        /// <summary>
        /// Extensions that this report support
        /// </summary>
        public string SupportedExtension
        {
            get
            {
                return ".ulog";
            }
        }

        /// <summary>
        /// Filename specifying where a report should be output
        /// </summary>
        public string Filename { get; set; }

        /// <summary>
        /// Task to run for generating this report.
        /// </summary>
        public ITask GenerationTask
        {
            get
            {
                if (m_task == null)
                {
                    m_task = new ActionTask("Generating report", (context, progress) => GenerateReport((DynamicLevelReportContext)context, progress));
                }
                return m_task;
            }
        }
        private ITask m_task;

        /// <summary>
        /// List of stats that are required to generate this report.
        /// </summary>
        public IEnumerable<StreamableStat> RequiredStats
        {
            get { return null; }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public ParentTxdTextureReport()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)

        #region Task Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// DHM FIX ME: this should not be using RSG.Pipeline.Processor assemblies!  Invoke the 
        /// engine by invoking the conversion executable.  This is too tightly coupled to the
        /// pipeline (changes will break the Workbench).
        private void GenerateReport(DynamicLevelReportContext context, IProgress<TaskProgress> progress)
        {
            // Two things to do.
            // 1) Check that the parent txd xml file matches the parent txd exported zip file.
            // 2) Check the txds in all sections.

            // Create a log for the output from this report.
            IUniversalLog log = LogFactory.ApplicationLog;
            UniversalLogFile logFile = new UniversalLogFile(Filename, FileMode.CreateNew, FlushMode.Error, log);

            // We need to construct a pipeline process for this.
            CommandOptions options = new CommandOptions();
            IEngine engine = new Engine(options, options.Config, options.Project, options.Branch, EngineFlags.Default);
            IInput buildInput = new BuildInput(context.Branch);
            IContentTree tree = RSG.Pipeline.Content.Factory.CreateTree(context.Branch);

            IProcess gtxdProcess = tree.Processes.FirstOrDefault(p => p.ProcessorClassName.Equals(ProcessorParentTXD, StringComparison.OrdinalIgnoreCase));
            Debug.Assert(gtxdProcess != null, "Unable to find the parent texture dictionary process.");

            // Get the parent txd xml/zip paths based on the processor.
            IFilesystemNode gtxdInput = gtxdProcess.Inputs.FirstOrDefault() as IFilesystemNode;
            IFilesystemNode gtxdOutput = gtxdProcess.Outputs.FirstOrDefault() as IFilesystemNode;
            Debug.Assert(gtxdInput != null, "Unable to find the input to the parent texture dictionary process or it isn't a file node.");
            Debug.Assert(gtxdOutput != null, "Unable to find the output to the parent texture dictionary process or it isn't a file node.");

            log.Profile("GlobalTXD: Creating global txd model");
            GlobalRoot txdModel = new GlobalRoot(gtxdInput.AbsolutePath, Path.Combine(context.Branch.Assets, "maps", "Textures"), ValidationOptions.DontValidate, true);
            gtxdInput.Parameters.Add(RSG.Pipeline.Processor.Map.Consts.Textures_ParentTextureDictionaryModel, txdModel);
            log.ProfileEnd();

            // Validation part 1.
            GlobalTXD.Validation.ValidateModelAgainstExportedZip(txdModel, gtxdOutput.AbsolutePath, log);

            // Validation part 2.
            // Get a list of all the inputs to the map PreProcess processor.
            ISet<IContentNode> inputNodes = new HashSet<IContentNode>();

            //TODO: Do this per node
            //inputNodes.Add(tree.CreateFile(@"X:\gta5\assets\export\levels\gta5\_citye\downtown_01\fwy_01.zip"));

            IEnumerable<IProcess> mapPreProcessProcesses = tree.Processes.Where(p => p.ProcessorClassName.Equals(ProcessorMapPreProcess, StringComparison.OrdinalIgnoreCase));
            inputNodes.AddRange(mapPreProcessProcesses.SelectMany(item => item.Inputs));

            foreach (IContentNode inputNode in inputNodes)
            {
                log.Message("GlobalTXD: Section {0} starting prebuild", inputNode.Name);

                ProcessBuilder texturesProcessBuilder = new ProcessBuilder(ProcessorTexture, tree);
                texturesProcessBuilder.Inputs.Add(inputNode);
                texturesProcessBuilder.Inputs.Add(gtxdInput);
                texturesProcessBuilder.Inputs.Add(gtxdOutput);

                IProcess process = texturesProcessBuilder.ToProcess();
                buildInput.RawProcesses.Add(process);

                // Finally run the prebuild
                IEnumerable<IOutput> outputs;
                TimeSpan time;
                if (engine.Prebuild(ref buildInput, out outputs, out time))
                {
                    log.Message("GlobalTXD: Section {0} prebuild succeeded.", inputNode.Name);
                }
                else
                {
                    log.Message("GlobalTXD: Section {0} prebuild failed.", inputNode.Name);
                }
            }

            logFile.Flush();
            logFile.Disconnect(log);
        }
        #endregion // Task Methods
    } // ParentTxdTextureReport
}
