﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;

namespace RSG.Model.Common.RadioStation
{
    /// <summary>
    /// Interface for a collection of radio station.
    /// </summary>
    public interface IRadioStationCollection :
        IAsset,
        IEnumerable<IRadioStation>,
        ICollection<IRadioStation>
    {
        /// <summary>
        /// Radio stations that this collection contains.
        /// </summary>
        ObservableCollection<IRadioStation> RadioStations { get; }
    } // IRadioStationCollection
}
