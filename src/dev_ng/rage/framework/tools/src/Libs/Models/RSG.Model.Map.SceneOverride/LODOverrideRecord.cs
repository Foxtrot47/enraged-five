﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Map.SceneOverride
{
    /// <summary>
    /// A full LOD override record with foreign keys to the processed content and user 'tables'
    /// </summary>
    internal class LODOverrideRecord
    {
        internal LODOverrideRecord(int contentId, uint hash, uint guid,
                                   String modelName, float posX, float posY, float posZ, 
                                   int userId, float distance, float childDistance, long utcFileTimeSubmitted)
        {
            ContentId = contentId;
            Hash = hash;
            Guid = guid;
            ModelName = modelName;
            PosX = posX;
            PosY = posY;
            PosZ = posZ;
            Distance = distance;
            ChildDistance = childDistance;
            UserId = userId;
            UtcFileTimeSubmitted = utcFileTimeSubmitted;
        }

        internal int ContentId { get; private set; }
        internal String ModelName { get; private set; }
        internal uint Hash { get; private set; }
        internal uint Guid { get; private set; }
        internal float PosX { get; set; }
        internal float PosY { get; set; }
        internal float PosZ { get; set; }
        internal float Distance { get; set; }
        internal float ChildDistance { get; set; }
        internal int UserId { get; set; }
        internal long UtcFileTimeSubmitted { get; set; }

        internal bool HasDistance { get { return Distance != invalidDistance_; } }
        internal bool HasChildDistance { get { return ChildDistance != invalidDistance_; } }

        private static float invalidDistance_ = -1.0f;
    }
}
