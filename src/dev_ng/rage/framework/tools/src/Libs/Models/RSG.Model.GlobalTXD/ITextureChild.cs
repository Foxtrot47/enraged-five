﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.GlobalTXD
{
    public interface ITextureChild
    {
        /// <summary>
        /// The parent container that conatins this object
        /// </summary>
        ITextureContainer Parent { get; }

        /// <summary>
        /// The root object for this object
        /// </summary>
        GlobalRoot Root { get; }

        /// <summary>
        /// The stream name for this texture
        /// </summary>
        String StreamName { get; }

        /// <summary>
        /// Represents the number of times the texture appears in the sibling
        /// texture containers
        /// </summary>
        int InstanceCount { get; set; }

        /// <summary>
        /// Represents the number of times the texture appears in the sibling
        /// texture containers if promoted
        /// </summary>
        int InstanceCountIfPromoted { get; set; }
    }
}
