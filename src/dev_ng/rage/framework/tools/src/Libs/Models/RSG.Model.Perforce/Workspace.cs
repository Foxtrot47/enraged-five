﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Model.Common;
using RSG.SourceControl.Perforce;
using P4API;

namespace RSG.Model.Perforce
{

    /// <summary>
    /// Perforce workspace object.
    /// </summary>
    public class Workspace :
        AssetContainerBase,
        IPerforceObject
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public User User 
        {
            get { return (m_User); }
            set
            {
                SetPropertyValue(ref m_User, value, "User");
            }
        }
        private User m_User;
        
        /// <summary>
        /// Root path.
        /// </summary>
        public String Root
        {
            get { return (m_sRoot); }
            set
            {
                SetPropertyValue(ref m_sRoot, value, "Root");
            }
        }
        private String m_sRoot;

        /// <summary>
        /// Workspace description.
        /// </summary>
        public String Description
        {
            get { return (m_sDescription); }
            set
            {
                SetPropertyValue(ref m_sDescription, value, "Description");
            }
        }
        private String m_sDescription;

        /// <summary>
        /// Last access time.
        /// </summary>
        public DateTime AccessTime
        {
            get { return (m_AccessTime); }
            set
            {
                SetPropertyValue(ref m_AccessTime, value, "AccessTime");
            }
        }
        private DateTime m_AccessTime;

        /// <summary>
        /// Last modified time.
        /// </summary>
        public DateTime UpdateTime
        {
            get { return (m_UpdateTime); }
            set
            {
                SetPropertyValue(ref m_UpdateTime, value, "UpdateTime");
            }
        }
        private DateTime m_UpdateTime;

        /// <summary>
        /// 
        /// </summary>
        public Depot[] Depots
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region IPerforceObject Interface
        /// <summary>
        /// Perforce Connection object.
        /// </summary>
        public P4 Connection
        {
            get { return (m_Connection); }
            set
            {
                SetPropertyValue(ref m_Connection, value, "Connection");
            }
        }
        private P4 m_Connection;

        /// <summary>
        /// 
        /// </summary>
        public void Refresh()
        {
            this.AssetChildren.Clear();

            Init();
            this.Depots = Depot.GetDepots(this.Connection);
            this.AssetChildren.AddRange(this.Depots);
        }
        #endregion // IPerforace Object Interface

        #region Constructor(s)
        /// <summary>
        /// Constructor, from a P4 object.
        /// </summary>
        /// <param name="p4"></param>
        public Workspace(P4 p4)
        {
            this.Connection = p4;
            if (!this.Connection.IsValidConnection(true, true))
                this.Connection.Connect();
            Refresh();
        }
        #endregion // Constructor(s)

        private void Init()
        {
            this.Name = this.Connection.Client;
            this.User = new User(this.Connection);
            P4RecordSet set = this.Connection.Run(Commands.P4_CLIENT, "-o", this.Connection.Client);
            Debug.Assert(1 == set.Records.Length);

            P4Record record = set[0];
            this.Root = (record["Root"].Clone() as String);
            this.AccessTime = (DateTime.Parse(record["Access"]));
            this.UpdateTime = (DateTime.Parse(record["Update"]));
            this.Description = (record["Description"].Clone() as String);
        }
    }

} // RSG.Model.Perforce namespace
