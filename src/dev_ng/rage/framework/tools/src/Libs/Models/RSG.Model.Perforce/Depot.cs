﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using P4API;
using RSG.SourceControl.Perforce;
using RSG.Model.Common;
using RSG.Base.Editor.Command;

namespace RSG.Model.Perforce
{

    /// <summary>
    /// Perforce depot object.
    /// </summary>
    public class Depot : 
        AssetContainerBase,
        IPerforceObject,
        IComparable<Depot>
    {
        #region Constants
        /// <summary>
        /// Server-command to get information about a depot.
        /// </summary>
        protected static readonly String CMD_DEPOT = "depot";

        /// <summary>
        /// Server-command to get information about all depots.
        /// </summary>
        protected static readonly String CMD_DEPOTS = "depots";
        #endregion // Constants

        #region Enumerations
        /// <summary>
        /// 
        /// </summary>
        public enum DepotCategory
        {
            Local,  // (default) locally managed depot by the server.
            Remote, // remotely managed depot
            Spec,
            Archive,
        }
        #endregion // Enumerations

        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public DepotCategory Category
        {
            get;
            protected set;
        }

        /// <summary>
        /// Depot description information.
        /// </summary>
        public String Description
        {
            get;
            protected set;
        }

        /// <summary>
        /// Depot creation date/time.
        /// </summary>
        public DateTime CreationDate
        {
            get;
            protected set;
        }

        /// <summary>
        /// Depot address (for Remote depots only) specifying connection address.
        /// </summary>
        public String Address
        {
            get;
            protected set;
        }

        /// <summary>
        /// Depot optional suffix (for Spec depots only).
        /// </summary>
        public String Suffix
        {
            get;
            protected set;
        }

        /// <summary>
        /// Depot server mapping (where files are on the server).
        /// </summary>
        public String Map
        {
            get;
            protected set;
        }
        #endregion // Properties and Associated Member Data
        
        #region IPerforceObject Interface
        /// <summary>
        /// Perforce Connection object.
        /// </summary>
        public P4 Connection
        {
            get { return (m_Connection); }
            set
            {
                SetPropertyValue(ref m_Connection, value, "Connection");
            }
        }
        private P4 m_Connection;

        /// <summary>
        /// 
        /// </summary>
        public void Refresh()
        {
        }
        #endregion // IPerforace Object Interface

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="r"></param>
        public Depot(P4 p4, P4Record record)
        {
            this.Connection = p4;
            if (!this.Connection.IsValidConnection(true, true))
                this.Connection.Connect();
            Init(record);
        }
        #endregion // Constructor(s)

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            return ((obj is Depot) && (obj as Depot).Name.Equals(this.Name));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return (String.Format("//{0}", this.Name));
        }
        #endregion // Object Overrides

        #region IComparable<Depot> Interface
        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(Depot other)
        {
            return (this.Name.CompareTo(other.Name));
        }
        #endregion // IComparable<Depot> Interface

        #region Static Controller Methods
        /// <summary>
        /// Return array of all depots on server.
        /// </summary>
        /// <param name="p4"></param>
        /// <returns></returns>
        public static Depot[] GetDepots(P4 p4)
        {
            List<Depot> depots = new List<Depot>();
            P4RecordSet result = p4.Run(Commands.P4_DEPOTS);
            foreach (P4Record record in result)
            {
                depots.Add(new Depot(p4, record));
            }
            return (depots.ToArray());
        }
        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// Init a Depot object from a P4Record result of 'depot' command.
        /// </summary>
        /// <param name="r"></param>
        private void Init(P4Record record)
        {
            this.Name = record["name"];
            switch (record["type"])
            {
                case "local":
                    this.Category = DepotCategory.Local;
                    break;
                case "remote":
                    this.Category = DepotCategory.Remote;
                    this.Address = record["address"];
                    break;
                case "archive":
                    this.Category = DepotCategory.Archive;
                    break;
                case "spec":
                    this.Category = DepotCategory.Spec;
                    break;
            }
            this.Description = record["desc"];
            this.Map = record["map"];
            this.CreationDate = DateTime.Now; //DateTime.Parse(record["time"]);
        }
        #endregion // Private Methods
    }

} // RSG.Model.Perforce namespace
