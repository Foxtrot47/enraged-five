﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using RSG.Base.Tasks;
using System.Threading;
using RSG.Base.ConfigParser;
using RSG.Base.Configuration;

namespace RSG.Model.Report
{
    /// <summary>
    /// Context used when creating a dynamic section report task.
    /// Might need to change this to inherit from DynamicLevelReportContext at some point.
    /// For now the level is accessible from a map section and the platforms aren't needed.
    /// </summary>
    public class DynamicSectionReportContext : DynamicReportContext
    {
        #region Properties
        /// <summary>
        /// List of sections the report should use when being generated.
        /// </summary>
        public IList<IMapSection> Sections { get; private set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public DynamicSectionReportContext(ConfigGameView gv, IConfig config, IList<IMapSection> sections)
            : this(CancellationToken.None, gv, config, sections)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        public DynamicSectionReportContext(CancellationToken token, ConfigGameView gv, IConfig config, IList<IMapSection> sections)
            : base(token, gv, config)
        {
            Sections = sections;
        }
        #endregion // Constructor(s)
    } // DynamicSectionReportContext
}
