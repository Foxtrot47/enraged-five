﻿namespace RSG.Model.Character.Game
{
    using System.Collections.Generic;
    using System.IO;
    using RSG.Base.Configuration;
    using RSG.Metadata.Data;
    using RSG.Metadata.Model;
    using RSG.Metadata.Parser;
    using System.Linq;

    /// <summary>
    /// Represents all of the vehicles that are being mounted by the game through the peds.meta
    /// data file and its associated meta files.
    /// </summary>
    public class GameCharacters
    {
        #region Fields
        /// <summary>
        /// The name of the character metadata file that contains the data mounted by the game
        /// that defines all of the characters used in the game.
        /// </summary>
        private const string characterFilename = @"data\peds.pso.meta";

        /// <summary>
        /// The private field used for the <see cref="Characters"/> property.
        /// </summary>
        List<GameCharacter> _characters;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="GameWeapons"/> class using the
        /// specified game branch and level to locate the data files used during loading.
        /// </summary>
        /// <param name="branch">
        /// The branch that the data should be loaded from.
        /// </param>
        /// <param name="structures">
        /// The structure dictionary containing the metadata definitions to use to parse the
        /// metadata files.
        /// </param>
        public GameCharacters(IBranch branch, StructureDictionary structures)
        {
            this._characters = new List<GameCharacter>();
            this.ParseCharacterMetadata(branch, structures);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets a iterator around the characters that have been defined in the game data.
        /// </summary>
        public IEnumerable<GameCharacter> Characters
        {
            get
            {
                foreach (GameCharacter character in this._characters)
                {
                    yield return character;
                }
            }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Parses the character metadata file.
        /// </summary>
        /// <param name="filename">
        /// The name of the file to parse.
        /// </param>
        /// <param name="structures">
        /// The structure dictionary containing the metadata definitions to use to parse the
        /// specified file.
        /// </param>
        private void ParseCharacterMetadata(IBranch branch, StructureDictionary structures)
        {
            string path = Path.Combine(branch.Export, characterFilename);
            if (!File.Exists(path))
            {
                return;
            }

            MetaFile metadata = new MetaFile(path, structures, branch);

            Structure characterStructure = structures["CPedModelInfo::InitData"];
            List<StructureTunable> characters = new List<StructureTunable>();
            foreach (StructureTunable t in metadata.AllMembers.OfType<StructureTunable>())
            {
                StructMember m = t.Definition as StructMember;
                if (m == null || m.Definition == null)
                {
                    continue;
                }

                if (!m.Definition.IsA(characterStructure))
                {
                    continue;
                }

                characters.Add(t);
            }

            this._characters = new List<GameCharacter>(characters.Count);
            foreach (StructureTunable t in characters)
            {
                GameCharacter character = new GameCharacter();
                foreach (StringTunable child in t.Values.OfType<StringTunable>())
                {
                    if (string.Compare(child.Name, "Name", true) == 0)
                    {
                        character.Name = child.Value.ToLower();
                    }
                    else if (string.Compare(child.Name, "PropsName", true) == 0)
                    {
                        if (child.Value.ToLower() != "null")
                        {
                            character.PropsName = child.Value.ToLower();
                        }
                    }
                }

                this._characters.Add(character);
            }
        }
        #endregion Methods
    }
}
