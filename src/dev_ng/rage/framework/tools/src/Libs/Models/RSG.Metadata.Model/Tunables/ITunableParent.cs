﻿//---------------------------------------------------------------------------------------------
// <copyright file="ITunableParent.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Metadata.Model.Tunables
{
    using System;
    using System.Xml;
    using RSG.Editor.Model;
    using RSG.Metadata.Model.Definitions.Members;

    /// <summary>
    /// When implemented represents a instance of a specific parCodeGen member object that
    /// can contain additional instances.
    /// </summary>
    public interface ITunableParent : IModel
    {
        #region Properties
        /// <summary>
        /// Gets the base System.Uri for the file this instance is defined in.
        /// </summary>
        string BasePath { get; }

        /// <summary>
        /// Gets a collection of the tunables that are defined for this parent tunable.
        /// </summary>
        ReadOnlyModelCollection<ITunable> Tunables { get; }

        /// <summary>
        /// Gets the tunable structure this instance belongs to.
        /// </summary>
        TunableStructure TunableStructure { get; }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Updates this tunables HasDefaultValue property.
        /// </summary>
        void UpdateHasDefaultValue();
        #endregion Methods
    } // RSG.Metadata.Model.Tunables.ITunableParent {Interface}
} // RSG.Metadata.Model.Tunables {Namespace}
