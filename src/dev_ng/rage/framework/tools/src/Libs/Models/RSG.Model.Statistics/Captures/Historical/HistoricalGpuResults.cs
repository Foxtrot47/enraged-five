﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace RSG.Model.Statistics.Captures.Historical
{
    /// <summary>
    /// Wrapper for historical gpu based results.
    /// </summary>
    [DataContract]
    [Serializable]
    public class HistoricalGpuResults
    {
        #region Properties
        /// <summary>
        /// Mapping of drawlist names to historical results.
        /// </summary>
        [DataMember]
        public IDictionary<String, HistoricalResults> PerDrawListResults { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public HistoricalGpuResults()
        {
            PerDrawListResults = new SortedDictionary<String, HistoricalResults>();
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="result"></param>
        public void AddResult(Object context, GpuResult result)
        {
            if (!PerDrawListResults.ContainsKey(result.Name))
            {
                PerDrawListResults[result.Name] = new HistoricalResults();
            }

            PerDrawListResults[result.Name].AddResult(context, result.Time, 0.0);
        }
        #endregion // Public Methods
    } // HistoricalGpuResults
}
