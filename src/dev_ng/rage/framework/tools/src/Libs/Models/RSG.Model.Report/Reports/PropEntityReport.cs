﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Base.ConfigParser;
using System.IO;
using RSG.Model.Map;

namespace RSG.Model.Report.Reports
{
    /// <summary>
    /// 
    /// </summary>
    public class PropEntityReport : CSVReport, IDynamicMapReport
    {
        #region Constants
        private const String c_name = "Prop Entity CSV Report";
        private const String c_description = "Exports the prop instance information to a CSV file.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Determines whether the map data should be loaded
        /// </summary>
        public bool LoadMapData
        {
            get { return true; }
        }

        /// <summary>
        /// Determines whether the car generators should be loaded
        /// 
        /// </summary>
        public bool LoadCarGenarators
        {
            get { return false; }
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public PropEntityReport()
            : base(c_name, c_description)
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Generate the report dynamically.
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public void Generate(ILevel level, ConfigGameView gv, IPlatformCollection platforms)
        {
            RSG.Model.Map.Util.Creation.InitialiseLevelStatistics(level, gv);

            // Write out the report
            using (StreamWriter writer = new StreamWriter(Filename))
            {
                WriteCsvHeader(writer);

                foreach (MapInstance instance in GatherLodInstances(level))
                {
                    WriteCsvInstance(writer, instance);
                }
            }
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        private IEnumerable<MapInstance> GatherLodInstances(ILevel level)
        {
            ISet<MapInstance> encounteredInstances = new HashSet<MapInstance>();

            // Loop over all sections
            foreach (MapSection section in RSG.Model.Map.Util.Enumerations.GetAllMapSections(level, null, true))
            {
                // Loop over all instances in the section
                foreach (MapInstance instance in section.AssetChildren.OfType<MapInstance>())
                {
                    // Is this a prop (i.e. the definition is referenced in an external file)
                    if (instance.Container != instance.ReferencedDefinition.Container)
                    {
                        yield return instance;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sw"></param>
        private void WriteCsvHeader(StreamWriter writer)
        {
            string header = "Entity Name,Section,Parent Area,Grandparent Area,X,Y,Z,Archetype Name,Tri Count,Bounding Box Volume (m^3),Tri Density (tris/m^3),";
            header += "Shader Count,Texture Count,TXD Name,Lod Distance,";
            header += "Mover Collision,Weapons Collision,Camera Collision,River Collision";
            writer.WriteLine(header);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="instance"></param>
        private void WriteCsvInstance(StreamWriter writer, MapInstance instance)
        {
            if (instance.IsInteriorReference)
            {
                return;
            }

            IMapContainer section = instance.Container;
            IMapContainer parent = section.Container;
            IMapContainer grandParent = (parent is MapArea ? (parent as MapArea).Container : null);

            MapDefinition definition = instance.ReferencedDefinition;

            float triDensity = 0;
            if (definition.BoundingBox.Volume() != 0)
            {
                triDensity = definition.PolygonCount / definition.BoundingBox.Volume();
            }

            // Generate the csv line
            string csvRecord = String.Format("{0},{1},{2},{3},", instance.Name, section.Name, parent != null ? parent.Name : "n/a", grandParent != null ? grandParent.Name : "n/a");
            csvRecord += String.Format("{0},{1},{2},", instance.Position.X, instance.Position.Y, instance.Position.Z);
            csvRecord += String.Format("{0},{1},{2},{3},", definition.Name, definition.PolygonCount, definition.BoundingBox.Volume(), triDensity);
            csvRecord += String.Format("{0},{1},{2},", definition.Shaders.Count, definition.Textures.Count(), definition.TextureDictionaryName);
            csvRecord += String.Format("{0},", instance.LodDistance);
            csvRecord += String.Format("{0},{1},{2},{3}", definition.MoverPolygonCount, definition.WeaponsPolygonCount, definition.CameraPolygonCount, definition.RiverPolygonCount);
            writer.WriteLine(csvRecord);
        }
        #endregion // Private Methods
    } // PropEntityReport
}
