﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace RSG.Model.Report.Reports.GameStatsReports
{
    /// <summary>
    /// 
    /// </summary>
    public class GraphImage : IDisposable
    {
        /// <summary>
        /// Name of the image
        /// </summary>
        public string Name
        {
            get;
            protected set;
        }

        /// <summary>
        /// 
        /// </summary>
        public string Filepath
        {
            get;
            set;
        }

        /// <summary>
        /// Stream containing the image data
        /// </summary>
        public Stream DataStream
        {
            get;
            protected set;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="data"></param>
        public GraphImage(string name, Stream data)
        {
            Name = name;
            DataStream = data;
        }

        public void Dispose()
        {
            DataStream.Dispose();
        }
    } // GraphImage
}
