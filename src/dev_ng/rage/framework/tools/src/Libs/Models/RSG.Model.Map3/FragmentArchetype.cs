﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;
using System.ComponentModel;
using RSG.Base.Math;
using RSG.Model.Common;
using RSG.SceneXml;
using System.Diagnostics;

namespace RSG.Model.Map3
{
    /// <summary>
    /// 
    /// </summary>
    public class FragmentArchetype : SimpleMapArchetypeBase, IFragmentArchetype
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public uint FragmentCount
        {
            get
            {
                CheckStatLoaded(StreamableFragmentArchetypeStat.FragmentCount);
                return m_fragmentCount;
            }
            private set
            {
                SetPropertyValue(ref m_fragmentCount, value, "FragmentCount");
                SetStatLodaded(StreamableFragmentArchetypeStat.FragmentCount, true);
            }
        }
        private uint m_fragmentCount;

        /// <summary>
        /// 
        /// </summary>
        public bool IsCloth
        {
            get
            {
                CheckStatLoaded(StreamableFragmentArchetypeStat.IsCloth);
                return m_isCloth;
            }
            private set
            {
                SetPropertyValue(ref m_isCloth, value, "IsCloth");
                SetStatLodaded(StreamableFragmentArchetypeStat.IsCloth, true);
            }
        }
        private bool m_isCloth;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public FragmentArchetype(string name, IMapSection parent)
            : base(name, parent)
        {
        }
        #endregion // Constructor(s)

        #region SimpleMapArchetypeBase Overrides
        /// <summary>
        /// Load basic stats from the scene xml object def
        /// </summary>
        /// <param name="sceneObject"></param>
        public void LoadStatsFromExportData(TargetObjectDef sceneObject, TargetObjectDef root, Scene scene)
        {
            base.LoadStatsFromExportData(sceneObject, scene);
            foreach (ISpawnPoint spawnPoint in this.RetrieveChildSpawnPoints(root))
            {
                this.SpawnPoints.Add(spawnPoint);
            }
        }

        /// <summary>
        /// Load specific statistics for this fragment
        /// </summary>
        /// <param name="statsToLoad"></param>
        /// <param name="async"></param>
        public override void RequestStatistics(IEnumerable<StreamableStat> statsToLoad, bool async)
        {
            ParentSection.MapHierarchy.RequestStatisticsForFragment(this, statsToLoad, async);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneObject"></param>
        protected override void LoadStatsFromDrawableObject(TargetObjectDef sceneObject, Scene scene)
        {
            base.LoadStatsFromDrawableObject(sceneObject, scene);

            FragmentCount = 0;
            IsCloth = sceneObject.GetAttribute(AttrNames.OBJ_IS_CLOTH, AttrDefaults.OBJ_IS_CLOTH);
        }
        #endregion // IStreamableObject<T> Implementation

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return (String.Format("Fragment Archetype: [{0}]{1}", this.Name, this.Hash));
        }
        #endregion // Object Overrides
    } // FragmentArchetype
}
