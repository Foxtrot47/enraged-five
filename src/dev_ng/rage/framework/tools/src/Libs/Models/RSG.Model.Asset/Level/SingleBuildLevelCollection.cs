﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Model.Common;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common.Dto.GameAssetStats;

namespace RSG.Model.Asset.Level
{
    /// <summary>
    /// 
    /// </summary>
    public class SingleBuildLevelCollection : LevelCollectionBase
    {
        #region Properties
        /// <summary>
        /// The build this vehicle collection is for.
        /// </summary>
        public string BuildIdentifier { get; protected set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildIdentifier"></param>
        public SingleBuildLevelCollection(string buildIdentifier)
            : base()
        {
            BuildIdentifier = buildIdentifier;
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        public override void LoadStats(IEnumerable<StreamableStat> statsToLoad)
        {
            try
            {
                using (BuildClient client = new BuildClient())
                {
                    // Construct the request object and query the database for the data.
                    StreamableStatFetchDto fetchDto = new StreamableStatFetchDto(Levels.Select(item => item.Hash).ToList(), statsToLoad.ToList());
                    IList<LevelStatDto> levelDtos = client.GetMultipleLevelStats(BuildIdentifier, fetchDto);

                    // Match up the retrieved data with the sections that were requested to allow them to read in the relevant data.
                    IDictionary<uint, DbLevel> requestedLevelLookup = Levels.ToDictionary(item => item.Hash, item => item as DbLevel);

                    foreach (LevelStatDto levelDto in levelDtos)
                    {
                        if (requestedLevelLookup.ContainsKey(levelDto.Identifier))
                        {
                            DbLevel level = requestedLevelLookup[levelDto.Identifier];
                            level.LoadStatsFromDatabase(levelDto, statsToLoad);
                        }
                        else
                        {
                            Debug.Assert(false, "Received section stat information for a section that wasn't requested.");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Log.Log__Exception(e, "Unhandled exception while retrieving stats from the database.");
            }
        }
        #endregion // Overrides
    } // SingleBuildLevelCollection
}
