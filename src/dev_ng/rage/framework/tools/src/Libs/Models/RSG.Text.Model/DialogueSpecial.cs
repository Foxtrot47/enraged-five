﻿//---------------------------------------------------------------------------------------------
// <copyright file="DialogueSpecial.cs" company="Rockstar">
//     Copyright © Rockstar Games 2013. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Text.Model
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Globalization;
    using System.Text;
    using System.Xml;
    using RSG.Editor;
    using RSG.Editor.Model;

    /// <summary>
    /// Represents a single special field that can be assigned to a audio line.
    /// </summary>
    [DebuggerDisplay("{_name}")]
    public sealed class DialogueSpecial : ModelBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Flags"/> property.
        /// </summary>
        private DialogueSpecialFlags _flags;

        /// <summary>
        /// The private field used for the <see cref="Id"/> property.
        /// </summary>
        private Guid _id;

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueSpecial"/> class.
        /// </summary>
        /// <param name="id">
        /// The static id for this special that cannot be changed.
        /// </param>
        /// <param name="name">
        /// The name for this special.
        /// </param>
        public DialogueSpecial(Guid id, string name)
        {
            this._id = id;
            this._name = name;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="DialogueSpecial"/> class.
        /// </summary>
        /// <param name="reader">
        /// Contains data used to initialise this instance.
        /// </param>
        public DialogueSpecial(XmlReader reader)
        {
            if (reader == null)
            {
                throw new SmartArgumentNullException(() => reader);
            }

            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the flags that have been associated with this dialogue special.
        /// </summary>
        public DialogueSpecialFlags Flags
        {
            get { return this._flags; }
            set { this.SetProperty(ref this._flags, value); }
        }

        /// <summary>
        /// Gets the id used by this special. This cannot be changed once setup and is how
        /// the special can be renamed without any data being lost or manipulated.
        /// </summary>
        public Guid Id
        {
            get { return this._id; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this special has been associated with a
        /// headset special flag.
        /// </summary>
        public bool HasHeadsetFlag
        {
            get
            {
                return this._flags.HasFlag(DialogueSpecialFlags.Headset);
            }

            set
            {
                if (value)
                {
                    this._flags |= DialogueSpecialFlags.Headset;
                }
                else
                {
                    this._flags &= ~DialogueSpecialFlags.Headset;
                }
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this special has been associated with a pad
        /// speaker special flag.
        /// </summary>
        public bool HasPadSpeakerFlag
        {
            get
            {
                return this._flags.HasFlag(DialogueSpecialFlags.PadSpeaker);
            }

            set
            {
                if (value)
                {
                    this._flags |= DialogueSpecialFlags.PadSpeaker;
                }
                else
                {
                    this._flags &= ~DialogueSpecialFlags.PadSpeaker;
                }
            }
        }

        /// <summary>
        /// Gets or sets the name for this dialogue special.
        /// </summary>
        public string Name
        {
            get { return this._name; }
            set { this.SetProperty(ref this._name, value); }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Creates a deep copy of the current instance.
        /// </summary>
        /// <returns>
        /// A deep copy of the current System.Object.
        /// </returns>
        public override object DeepClone()
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Serialises the data contained within this instance onto the specified xml writer.
        /// </summary>
        /// <param name="writer">
        /// The xml writer this instance will be serialised to.
        /// </param>
        public void Serialise(XmlWriter writer)
        {
            if (writer == null)
            {
                Debug.Assert(writer != null, "Unable to serialise into a null writer.");
                return;
            }

            writer.WriteElementString("Name", this._name);
            writer.WriteElementString("Id", this._id.ToString("D"));

            List<string> flags = new List<string>();
            foreach (DialogueSpecialFlags flag in Enum.GetValues(typeof(DialogueSpecialFlags)))
            {
                if ((this._flags & flag) == flag)
                {
                    flags.Add(flag.ToString());
                }
            }

            if (flags.Count > 0)
            {
                writer.WriteElementString("Flags", String.Join("|", flags));
            }
        }

        /// <summary>
        /// Initialises this instance using the data contained within the specified xml reader.
        /// </summary>
        /// <param name="reader">
        /// The xml reader that contains the data used to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() == XmlNodeType.Element)
            {
                if (String.Equals("Name", reader.Name))
                {
                    this._name = reader.ReadElementContentAsString();
                }
                else if (String.Equals("Id", reader.Name))
                {
                    string value = reader.ReadElementContentAsString();
                    if (!Guid.TryParseExact(value, "D", out this._id))
                    {
                        this._id = Guid.Empty;
                    }
                }
                else if (String.Equals("Flags", reader.Name))
                {
                    string value = reader.ReadElementContentAsString();
                    char[] sep = new char[] { '|' };
                    string[] parts = value.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                    foreach (string part in parts)
                    {
                        DialogueSpecialFlags flag;
                        if (Enum.TryParse<DialogueSpecialFlags>(part, out flag))
                        {
                            this._flags |= flag;
                        }
                    }
                }

                reader.Skip();
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }
        #endregion Methods
    } // RSG.Text.Model.DialogueSpecial {Class}
} // RSG.Text.Model {Namespace}
