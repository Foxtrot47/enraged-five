﻿using System;
using RSG.Model.Common;
using RSG.Base.Configuration.Reports;
using System.Collections.Generic;

namespace RSG.Model.Report
{
    /// <summary>
    /// 
    /// </summary>
    public class DataSourceCollection
    {
        ///
        public bool this[DataSource index]
        {
            get
            {
                return m_dataSources[(int)index];
            }
            set
            {
                m_dataSources[(int)index] = value;
            }
        }

        public DataSourceCollection()
        {
            m_dataSources = new bool[Enum.GetValues(typeof(DataSource)).Length];
        }

        private bool[] m_dataSources;
    }

    /// <summary>
    /// Report Model Interface
    /// </summary>
    public interface IReport : IReportItem
    {
        #region Properties
        /// <summary>
        /// Which data source modes this report can be used in.
        /// </summary>
        DataSourceCollection DataSourceModes { get; }
        #endregion // Properties
    } // IReport
} // RSG.Model.Report
