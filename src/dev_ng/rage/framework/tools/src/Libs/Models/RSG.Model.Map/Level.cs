﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Diagnostics;
using RSG.Base.ConfigParser;
using RSG.Base.Collections;
using RSG.Model.Map.Utilities;

namespace RSG.Model.Map
{

    /// <summary>
    /// Game level object model; containing a set of MapSection objects and
    /// optional user-defined data.
    /// </summary>
    public class Level : IMapContainer, ISearchable
    {
        #region Published Events
        /// <summary>
        /// Event raised when attached user-data object is changed.
        /// </summary>
        public event EventHandler UserDataChanged;
        #endregion // Published Events

        #region Properties and Associated Member ExportData
        /// <summary>
        /// 
        /// </summary>
        public String Name
        {
            get;
            private set;
        }
        
        /// <summary>
        /// Dictionary of MapSection objects for this level.
        /// </summary>
        public Dictionary<String, IMapComponent> MapComponents
        {
            get;
            protected set;
        }

        /// <summary>
        /// Indexer.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public IMapComponent this[String sectionName]
        {
            get
            {
                Debug.Assert(this.ContainsMapComponent(sectionName),
                    "Map section does not exist.");
                return (this.MapComponents[sectionName.ToLower()]);
            }
        }

        /// <summary>
        /// Reference to user-defined data (application specific level data).
        /// </summary>
        public Object UserData
        {
            get { return m_UserData; }
            set
            {
                m_UserData = value;
                if (null != UserDataChanged)
                    UserDataChanged(this, EventArgs.Empty);
            }
        }
        private Object m_UserData;

        /// <summary>
        /// The model user data for this map section
        /// </summary>
        public UserData ModelUserData
        {
            get { return m_modelUserData; }
            set { m_modelUserData = value; }
        }
        private UserData m_modelUserData = new UserData();

        /// <summary>
        /// Level map image filename (absolute path).
        /// </summary>
        public String ImageFilename
        {
            get;
            private set;
        }
        
        /// <summary>
        /// Level map image bounding box (game world-space).
        /// </summary>
        public RSG.Base.Math.BoundingBox2f ImageBounds
        {
            get;
            private set;
        }

        public IEnumerable<IMapComponent> ComponentChildren 
        {
            get
            {
                foreach (IMapComponent component in this.MapComponents.Values)
                {
                    if ((component is IMapComponent == true) && (component is IMapContainer == false))
                    {
                        yield return component;
                    }
                }
            }
        }

        public IEnumerable<IMapContainer> ContainerChildren 
        {
            get
            {
                foreach (IMapComponent container in this.MapComponents.Values)
                {
                    if ((container is IMapContainer == true))
                    {
                        yield return container as IMapContainer;
                    }
                }
            }
        }

        /// <summary>
        /// The parent of this searchable object
        /// </summary>
        public ISearchable SearchableParent
        {
            get;
            private set;
        }

        #endregion // Properties and Associated Member ExportData

        #region Member ExportData
        #endregion // Member ExportData

        #region Constructor(s)

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="cm"></param>
        public Level(String name, ContentModel cm)
        {
            Init(name, cm, Filters.MapFilters.All, Filters.AreaFilters.All);
            this.SearchableParent = null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="cm"></param>
        public Level(String name, ContentNodeLevel group)
        {
            this.ImageFilename = String.Empty;
            this.Name = name;
            this.MapComponents = new Dictionary<String, IMapComponent>();
            this.SearchableParent = null;

            foreach (ContentNode node in group.Children)
            {
                if (node is ContentNodeLevelImage)
                {
                    Debug.Assert((String.Empty == this.ImageFilename) && (this.ImageBounds == null),
                        "Found two level_image nodes.  Unusual content tree.");
                    ContentNodeLevelImage image = (node as ContentNodeLevelImage);

                    this.ImageBounds = new RSG.Base.Math.BoundingBox2f();
                    this.ImageBounds.Min.X = image.MinX;
                    this.ImageBounds.Min.Y = image.MinY;
                    this.ImageBounds.Max.X = image.MaxX;
                    this.ImageBounds.Max.Y = image.MaxY;
                    this.ImageFilename = image.Filename;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="cm"></param>
        /// <param name="mapFilter"></param>
        /// <param name="areaFilter"></param>
        public Level(String name, ContentModel cm, LevelDictionary.FilterMapAreaDelegate areaFilter, LevelDictionary.FilterMapSectionDelegate mapFilter)
        {
            Init(name, cm, mapFilter, areaFilter);
            this.SearchableParent = null;
        }

        #endregion // Constructor(s)

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return (this.Name);
        }
        #endregion // Object Overrides

        #region IEnumerable<IMapContainer>
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator<IMapComponent> IEnumerable<IMapComponent>.GetEnumerator()
        {
            foreach (IMapComponent l in this.MapComponents.Values)
                yield return l;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            foreach (IMapComponent l in this.MapComponents.Values)
                yield return l;
        }
        #endregion // IEnumerable<IMapContainer>

        #region Controller Methods
        /// <summary>
        /// Return true iff level contains a MapComponent with specified name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool ContainsMapComponent(String name)
        {
            return (this.MapComponents.ContainsKey(name.ToLower()));
        }

        #endregion // Controller Methods

        #region Protected Methods

        /// <summary>
        /// Initialises the level and creates the map sections using the map filter delegate given
        /// </summary>
        /// <param name="name">The name of the level</param>
        /// <param name="cm">The content model from the game view</param>
        /// <param name="mapFilter">The delegate function to use when filtering map sections, if this function returns false the map section doesn't get loaded</param>
        protected virtual void Init(String name, ContentModel cm, LevelDictionary.FilterMapSectionDelegate mapFilter, LevelDictionary.FilterMapAreaDelegate areaFilter)
        {
            this.ImageFilename = String.Empty;
            this.Name = name;
            this.MapComponents = new Dictionary<String, IMapComponent>();
            ParseLevelContentModel(cm, mapFilter, areaFilter);
        }

        /// <summary>
        /// Parse the ContentModel for Map Sections within this Level.
        /// </summary>
        /// <param name="cm">The content model to parse</param>
        /// <param name="mapFilter">The delegate function to use when filtering map sections, if this function returns false the map section doesn't get loaded</param>
        /// <param name="areaFilter"></param>
        protected virtual void ParseLevelContentModel(ContentModel cm, LevelDictionary.FilterMapSectionDelegate mapFilter, LevelDictionary.FilterMapAreaDelegate areaFilter)
        {
            ContentNodeGroup root = cm.Root;
            ContentNode node = root.FindFirst(this.Name, "level");
            Debug.Assert(node is ContentNodeLevel, "Invalid level object found.");

            // Recurse through level's children.
            ParseLevelContentGroup(this, node as ContentNodeLevel, mapFilter, areaFilter);
        }

        /// <summary>
        /// Parses an content node group and decides to load a map section based on the given delegate function
        /// </summary>
        /// <param name="container">Map</param>
        /// <param name="group">The group to parse</param>
        /// <param name="mapFilter">The delegate to use to filter the map sections if this function returns false the map section doesn't get loaded</param>
        /// <param name="areaFilter"></param>
        protected virtual void ParseLevelContentGroup(IMapContainer container, ContentNodeGroup group, LevelDictionary.FilterMapSectionDelegate mapFilter, LevelDictionary.FilterMapAreaDelegate areaFilter)
        {
            foreach (ContentNode node in group.Children)
            {
                // Map Section
                if (node is ContentNodeMapZip)
                {
                    ContentNodeMapZip mapzip = (node as ContentNodeMapZip);
                    if (0 == node.Inputs.Length)
                        continue;

                    ContentNodeMap map = (node.Inputs[0] as ContentNodeMap);
                    if (container.MapComponents.ContainsKey(map.Name.ToLower()))
                        continue;

                    Boolean loadMap = true;

                    if (mapFilter != null)
                        loadMap = mapFilter(map);

                    if (loadMap)
                    {
                        MapSection mapSection = new MapSection(this, map, mapzip);
                        container.MapComponents.Add(map.Name.ToLower(), mapSection);
                    }
                }
                // Map Area 
                else if (node is ContentNodeGroup)
                {
                    ContentNodeGroup area = (node as ContentNodeGroup);
                    if (0 == String.Compare(area.RawRelativePath, String.Empty))
                        continue;
                    if (container.MapComponents.ContainsKey(area.RawRelativePath.ToLower()))
                        continue;

                    Boolean loadArea = true;
                    if (null != areaFilter)
                        loadArea = areaFilter(area);

                    if (loadArea)
                    {
                        MapArea mapArea = new MapArea(this, area, container);
                        container.MapComponents.Add(area.RawRelativePath.ToLower(), mapArea);
                        ParseLevelContentGroup(mapArea, node as ContentNodeGroup, mapFilter, areaFilter);
                    }
                }
                else if (node is ContentNodeLevelImage)
                {
                    Debug.Assert((String.Empty == this.ImageFilename) && (this.ImageBounds == null),
                        "Found two level_image nodes.  Unusual content tree.");
                    ContentNodeLevelImage image = (node as ContentNodeLevelImage);

                    this.ImageBounds = new RSG.Base.Math.BoundingBox2f();
                    this.ImageBounds.Min.X = image.MinX;
                    this.ImageBounds.Min.Y = image.MinY;
                    this.ImageBounds.Max.X = image.MaxX;
                    this.ImageBounds.Max.Y = image.MaxY;
                    this.ImageFilename = image.Filename;
                }
            }
        }

        #endregion // Protected Methods

        #region Walk Properties

        /// <summary>
        /// Walks through the children of ISearchables, going
        /// depth first
        /// </summary>
        public IEnumerable<ISearchable> WalkSearchDepth 
        {
            get
            {
                yield return this;
                foreach (ISearchable searchable in this.MapComponents.Values.Where(c => c is ISearchable == true))
                {
                    foreach (ISearchable searchableChld in searchable.WalkSearchDepth)
                    {
                        yield return searchableChld;
                    }
                }
            }
        }

        #endregion // Walk Properties
    }

    /// <summary>
    /// 
    /// </summary>
    public class LevelEventArgs : EventArgs
    {
        #region Properties and Associated Member ExportData
        /// <summary>
        /// 
        /// </summary>
        public Level AssociatedLevel
        {
            get { return m_AssociatedLevel; }
            private set { m_AssociatedLevel = value; }
        }
        private Level m_AssociatedLevel;
        #endregion // Properties and Associated Member ExportData

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="prop"></param>
        public LevelEventArgs(Level level)
        {
            this.AssociatedLevel = level;
        }
        #endregion // Constructor(s)
    }

} // RSG.Model.Map namespace
