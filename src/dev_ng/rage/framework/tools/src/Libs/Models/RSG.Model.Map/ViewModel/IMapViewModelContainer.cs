﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Model.Map.ViewModel
{
    public interface IMapViewModelContainer
    {
        IEnumerable<IMapViewModelComponent> ComponentChildren { get; }

        IEnumerable<IMapViewModelContainer> ContainerChildren { get; }

        IEnumerable<IMapSectionViewModel> SectionChildren { get; }

        String Name { get; }

        Boolean ContainsNonGenericWithDefinitionChildren(Boolean recusive);

        Boolean InitialisedChildren { get; }

        Boolean InitialisingChildren { get; }

        RSG.Base.Editor.IViewModel Parent { get; }
    }
}
