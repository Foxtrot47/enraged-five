﻿using System;
using System.Collections.Generic;
using System.Windows.Media.Imaging;

namespace RSG.Model.Asset.Util
{
    /// <summary>
    /// A static class that is used to manage the loading of the thumbnails,
    /// using this class will make sure that any single thumbnail for a filename
    /// will only ever be loaded once.
    /// </summary>
    public static class ThumbnailManager
    {
        #region Constants 
        /// <summary>
        /// 
        /// </summary>
        private static int DECODE_PIXEL_WIDTH = 16;
        #endregion // Constants

        #region Properties
        /// <summary>
        /// The main thumbnail dictionary that references the bitmap image objects by
        /// the filename
        /// </summary>
        private static Dictionary<String, BitmapImage> Thumbnails
        {
            get;
            set;
        }       
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor that creates the thumbnail dictionary
        /// </summary>
        static ThumbnailManager()
        {
            Thumbnails = new Dictionary<String, BitmapImage>();
        }
        #endregion // Constructor(s)

        #region Public Static Function(s)
        /// <summary>
        /// This is the main function to retrive the thumbnail objects. This
        /// function either returns a pre existing thumbnail object or creates it
        /// and then returns it.
        /// </summary>
        public static BitmapImage GetThumbnail(String filename)
        {
            if (Thumbnails.ContainsKey(filename))
            {
                return Thumbnails[filename];
            }
            else
            {
                try
                {
                    if (System.IO.File.Exists(filename))
                    {
                        Uri uriSource = new Uri(filename, UriKind.RelativeOrAbsolute);

                        Thumbnails[filename] = new BitmapImage();
                        Thumbnails[filename].BeginInit();
                        Thumbnails[filename].DecodePixelWidth = DECODE_PIXEL_WIDTH;
                        Thumbnails[filename].CreateOptions = BitmapCreateOptions.None;
                        Thumbnails[filename].CacheOption = BitmapCacheOption.OnDemand;
                        Thumbnails[filename].UriSource = uriSource;
                        Thumbnails[filename].EndInit();

                        Thumbnails[filename].Freeze();
                    }
                    else
                    {
                        Thumbnails[filename] = null;
                    }
                }
                catch
                {
                    // Remove it so that we can try again
                    Thumbnails.Remove(filename);
                }

                if (Thumbnails.ContainsKey(filename))
                {
                    return Thumbnails[filename];
                }
            }
            return null;
        }

        /// <summary>
        /// Enumerable that loops through all the currently existing 
        /// thumbnail objects.
        /// </summary>
        public static IEnumerable<BitmapImage> GetThumbnails()
        {
            foreach (BitmapImage thumbnail in Thumbnails.Values)
            {
                yield return thumbnail;
            }
        }

        /// <summary>
        /// Clears the memory for all the thumbnails currently created
        /// </summary>
        public static void Reset()
        {
            Thumbnails = null;
            Thumbnails = new Dictionary<String, BitmapImage>();
        }
        #endregion // Public Static Function(s)
    }

} // RSG.Model.Asset.Util
