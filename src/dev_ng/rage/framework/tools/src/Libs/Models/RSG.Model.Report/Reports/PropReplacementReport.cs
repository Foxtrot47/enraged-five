﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web.UI;
using RSG.Base.ConfigParser;
using RSG.Base.Logging;
using RSG.Model.Map;
using RSG.Model.Common;

namespace RSG.Model.Report.Reports
{
    /// <summary>
    /// Report that processes the prop replacement list and looks for any instances of those
    /// props that are still present in the game
    /// </summary>
    public class PropReplacementReport : HTMLReport, IDynamicMapReport
    {
        #region Constants
        private const String NAME = "Prop Replacements Report";
        private const String DESC = "Generate and display a report of where props in the prop replacement file are still in use.";
        private const String PROPREPLACEMENTFILE = @"x:\gta5\assets\maps\PropSwap.txt";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Determines whether the map data should be loaded
        /// </summary>
        public bool LoadMapData
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Determines whether the car generators should be loaded
        /// 
        /// </summary>
        public bool LoadCarGenarators
        {
            get { return false; }
        }

        /// <summary>
        /// List of prop names that we are looking for
        /// </summary>
        private HashSet<string> PropNames
        {
            get;
            set;
        }

        /// <summary>
        /// List of props that were found in the game
        /// </summary>
        private HashSet<MapDefinition> PropsEncountered
        {
            get;
            set;
        }
        #endregion
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public PropReplacementReport()
            : base(NAME, DESC)
        {
            PropNames = new HashSet<string>();
            PropsEncountered = new HashSet<MapDefinition>();
        }
        #endregion // Constructor(s)

        #region IDynamicMapReport Implementation
        /// <summary>
        /// Generate the report for the data associated with the supplied level
        /// </summary>
        /// <param name="level"></param>
        /// <param name="gv"></param>
        public void Generate(ILevel level, ConfigGameView gv, IPlatformCollection platforms)
        {
            DateTime start = DateTime.Now;

            // Process the prop replacement file to determine which props we should be looking for
            ProcessPropReplacementFile();

            // Process the level data looking for the props we just identified
            LookForPropsInLevel(level);

            // Generate the resulting report
            Stream = WriteReport();
            
            Log.Log__Message("Prop replacement CSV report generation complete. (report took {0} milliseconds to create.)", (DateTime.Now - start).TotalMilliseconds);
        }
        #endregion // IDynamicMapReport Implementation

        #region Private Methods
        /// <summary>
        /// Processes the prop replacement file extracting the list of prop names that should be replaced
        /// </summary>
        private void ProcessPropReplacementFile()
        {
            PropNames.Clear();

            // Read through the file line by line
            using (StreamReader reader = new StreamReader(PROPREPLACEMENTFILE))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    line = line.Trim();

                    if (line.Length > 0 && line[0] != '#')
                    {
                        int colonIdx = line.IndexOf(':');
                        if (colonIdx != -1)
                        {
                            string propName = line.Substring(0, colonIdx);
                            PropNames.Add(propName);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Iterates over the level's map sections looking for instances of props in the prop name list
        /// </summary>
        /// <param name="level"></param>
        private void LookForPropsInLevel(ILevel level)
        {
            PropsEncountered.Clear();

            foreach (MapSection section in RSG.Model.Map.Util.Enumerations.GetAllMapSections(level, null, true))
            {
                foreach (IAsset asset in section.AssetChildren)
                {
                    MapDefinition definition = asset as MapDefinition;

                    if (definition != null && definition.Instances.Count > 0 && PropNames.Contains(definition.Name))
                    {
                        PropsEncountered.Add(definition);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private MemoryStream WriteReport()
        {
            // Generate the stream for the report
            MemoryStream stream = new MemoryStream();
            StreamWriter reportFile = new StreamWriter(stream);

            List<MapDefinition> sortedProps = new List<MapDefinition>(PropsEncountered.Count);
            sortedProps.AddRange(PropsEncountered);
            sortedProps.Sort();

            // Start writing out the report
            HtmlTextWriter writer = new HtmlTextWriter(reportFile);
            writer.RenderBeginTag(HtmlTextWriterTag.Html);
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Head);
                writer.RenderEndTag();

                writer.RenderBeginTag(HtmlTextWriterTag.Body);
                {
                    WriteHeader(writer, "Prop Replacement Report");
                    
                    WriteSubHeader(writer, "Summary");
                    OutputSummary(writer, sortedProps);

                    if (sortedProps.Count > 0)
                    {
                        WriteSubHeader(writer, "Detailed Information");

                        foreach (MapDefinition propDef in PropsEncountered)
                        {
                            OutputPropInformation(writer, propDef);
                        }
                    }
                }
                writer.RenderEndTag();

            }
            writer.RenderEndTag();
            writer.Flush();

            // Reset the stream before returning it
            stream.Position = 0;
            return stream;
        }

        /// <summary>
        /// Outputs a summary of the information we found
        /// </summary>
        /// <param name="writer"></param>
        private void OutputSummary(HtmlTextWriter writer, List<MapDefinition> props)
        {
            // Determine how many instances there are of all the props
            int instanceCount = 0;
            props.ForEach(item => instanceCount += item.Instances.Count);

            // Generate the summary text
            if (instanceCount == 0)
            {
                WriteParagraph(writer, "Congratulations, none of the props in the prop replacement file where found in the map data.");
            }
            else
            {
                string summaryText = String.Format("{0} of the props in the prop replacement file are still in use.  They are being used a total of {1} times in the map data.  " +
                                                   "Click on one of the links below to see more information about where it is in use.",
                                                   props.Count, instanceCount);
                WriteParagraph(writer, summaryText);

                // Output each of the props we found and and link to the detailed information
                foreach (MapDefinition prop in props)
                {
                    writer.AddAttribute(HtmlTextWriterAttribute.Href, String.Format("#{0}", prop.Name));
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write(String.Format("{0} ({1} instances)", prop.Name, prop.Instances.Count));
                    writer.RenderEndTag();

                    writer.WriteBreak();
                }
            }

        }

        /// <summary>
        /// Outputs information regarding a single prop's occurrences
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="propDef"></param>
        private void OutputPropInformation(HtmlTextWriter writer, MapDefinition prop)
        {
            // <a name="propName"><b>Prop Name</b></a>
            writer.AddAttribute(HtmlTextWriterAttribute.Name, prop.Name);
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.RenderBeginTag(HtmlTextWriterTag.B);
            writer.Write(prop.Name);
            writer.RenderEndTag();
            writer.RenderEndTag();

            // <table>
            //   <tr><td>Section Name</td><td>Position: (X, Y)</td></tr>
            //   ...
            // </table>
            writer.RenderBeginTag(HtmlTextWriterTag.Table);

            foreach (MapInstance instance in prop.Instances)
            {
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(instance.Container.Name);
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(String.Format("Position: {0}", instance.Position));
                writer.RenderEndTag();
                writer.RenderEndTag();
            }

            writer.RenderEndTag();
            writer.WriteBreak();
        }
        #endregion // Private Methods
    } // PropReplacementReport
} // RSG.Model.Report.Reports
