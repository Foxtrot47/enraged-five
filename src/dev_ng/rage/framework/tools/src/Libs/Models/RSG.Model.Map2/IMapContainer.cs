﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;

namespace RSG.Model.Map
{
    /// <summary>
    /// Map container interface; map component which has a set of child
    /// map components.
    /// </summary>
    public interface IMapContainer : IMapComponent, IHasAssetChildren, IEnumerable<IMapComponent>
    {
        ILevel Level { get; }
    } // IMapContainer
} // RSG.Model.Map namespace
