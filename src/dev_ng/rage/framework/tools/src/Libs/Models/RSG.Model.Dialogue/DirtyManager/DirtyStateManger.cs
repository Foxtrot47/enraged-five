﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using RSG.Model.Dialogue.UndoInterface;
using System.ComponentModel;

namespace RSG.Model.Dialogue.DirtyManager
{
    /// <summary>
    /// Keeps the dirty state of a set of view models
    /// up to date throught he use of events inside the undo redo base
    /// </summary>
    public class DirtyStateManager : INotifyPropertyChanged
    {
        #region Events

        /// <summary>
        /// Event raised when a property in the ViewModel has changed.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (null != handler)
                handler(this, e);
        }

        /// <summary>
        /// Method to allow easy raising of the OnPropertyChanged event.
        /// </summary>
        /// <param name="propertyName"></param>
        public void OnPropertyChanged(String propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        #endregion // Events

        #region Properties

        /// <summary>
        /// The dirty state of all of the registered view models
        /// </summary>
        public Boolean Dirty
        {
            get { return m_dirty; }
            set { m_dirty = value; OnPropertyChanged("Dirty"); }
        }
        private Boolean m_dirty;

        /// <summary>
        /// A collection of UndoRedoBase objects that this dirty state manager is listening to.
        /// </summary>
        private Collection<UndoRedoBase> m_registeredViewModels;

        #endregion

        #region Constructor(s)

        /// <summary>
        /// Default constructor, just initialises the properties.
        /// </summary>
        public DirtyStateManager()
        {
            m_registeredViewModels = new Collection<UndoRedoBase>();
            m_dirty = false;
        }

        #endregion // Constructor(s)

        #region Private Function(s)

        /// <summary>
        /// Register a view model with this undo manager, meaning that 
        /// any undo property change will be managed here.
        /// </summary>
        /// <param name="viewModel"></param>
        public void RegisterViewModel(Object viewModel)
        {
            if (viewModel is UndoRedoBase)
            {
                m_registeredViewModels.Add((UndoRedoBase)viewModel);
                (viewModel as UndoRedoBase).DirtyPropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(DirtyStateManger_DirtyPropertyChanged);
            }
        }

        /// <summary>
        /// Unregisters the given view model with this undo manager so that 
        /// the undo events of the view model will no longer be managed here.
        /// </summary>
        /// <param name="viewModel"></param>
        public void UnRegisterViewModel(Object viewModel)
        {
            if (viewModel is UndoRedoBase)
            {
                m_registeredViewModels.Remove((UndoRedoBase)viewModel);
                (viewModel as UndoRedoBase).DirtyPropertyChanged -= this.DirtyStateManger_DirtyPropertyChanged;
            }
        }

        /// <summary>
        /// Gets called if any of the registered view model dirty state are changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void DirtyStateManger_DirtyPropertyChanged(Object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            this.Dirty = true;
        }

        #endregion // Private Function(s)

    } // DirtyManger
} // RSG.Model.Dialogue.DirtyStateManager
