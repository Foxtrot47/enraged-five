﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Runtime.Serialization;

namespace RSG.Model.Statistics.Captures
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class StreamingMemoryResult
    {
        #region Properties
        /// <summary>
        /// Module name for these streaming memory results.
        /// </summary>
        [DataMember]
        public String ModuleName { get; set; }

        /// <summary>
        /// Mapping of category names to memory readings.
        /// </summary>
        [DataMember]
        public IDictionary<String, StreamingMemoryStat> Categories { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public StreamingMemoryResult()
        {
            Categories = new Dictionary<String, StreamingMemoryStat>();
        }

        /// <summary>
        /// 
        /// </summary>
        public StreamingMemoryResult(XElement element)
        {
            // Extract the various stats
            XElement nameElement = element.Element("moduleName");
            if (nameElement != null)
            {
                ModuleName = nameElement.Value;
            }

            // Extract all the categorised stats
            Categories = new Dictionary<String, StreamingMemoryStat>();

            if (element.Element("categories") != null)
            {
                foreach (XElement item in element.Element("categories").Elements("Item"))
                {
                    nameElement = item.Element("categoryName");
                    if (nameElement != null)
                    {
                        string categoryName = nameElement.Value;
                        uint virtualMemory = 0;
                        uint physicalMemory = 0;

                        XElement virtualElement = item.Element("virtualMemory");
                        if (virtualElement != null && virtualElement.Attribute("value") != null)
                        {
                            virtualMemory = UInt32.Parse(virtualElement.Attribute("value").Value);
                        }

                        XElement physicalElement = item.Element("physicalMemory");
                        if (physicalElement != null && physicalElement.Attribute("value") != null)
                        {
                            physicalMemory = UInt32.Parse(physicalElement.Attribute("value").Value);
                        }

                        Categories.Add(categoryName, new StreamingMemoryStat(categoryName, virtualMemory, physicalMemory));
                    }
                }
            }
        }
        #endregion // Constructor(s)
    } // StreamingMemoryResult
}
