﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.ConfigParser;
using RSG.Base.Configuration;
using RSG.ManagedRage;
using RSG.Model.Asset.Util;
using RSG.Model.Common;
using RSG.Model.Common.Util;

namespace RSG.Model.Character
{
    /// <summary>
    /// Collection of characters that come from the local export data
    /// </summary>
    public class LocalCharacterCollection : CharacterCollectionBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public ConfigGameView GameView
        {
            get
            {
                if (m_gv == null)
                {
                    m_gv = new ConfigGameView();
                }
                return m_gv;
            }
        }
        private ConfigGameView m_gv;

        /// <summary>
        /// 
        /// </summary>
        public IConfig Config
        {
            get
            {
                if (m_config == null)
                {
                    m_config = ConfigFactory.CreateConfig();
                }
                return m_config;
            }
        }
        private IConfig m_config;

        /// <summary>
        /// The source folder for the weapon data
        /// </summary>
        public string ExportDataPath
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="streamingCoordinator"></param>
        /// <param name="exportDataPath"></param>
        public LocalCharacterCollection(string exportDataPath)
            : base()
        {
            ExportDataPath = exportDataPath;
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        public override void LoadStats(IEnumerable<StreamableStat> statsToLoad)
        {
            // Two different load paths.
            // 1) Loading "basic" stat from scenexml data.
            // 2) Loading platform stats data from rpf's.

            // Basic stats
            IEnumerable<StreamableStat> nonPlatformStats = statsToLoad.Where(item => item != StreamableCharacterStat.PlatformStats);

            // Load each character individually.
            foreach (ICharacter character in AllCharacters)
            {
                if (!character.AreStatsLoaded(nonPlatformStats))
                {
                    character.LoadStats(nonPlatformStats);
                }
            }

            // Platform stats.
            if (statsToLoad.Contains(StreamableCharacterStat.PlatformStats) && !ArePlatformStatsLoaded())
            {
                LoadPlatformStatsFromExportData();
            }
        }
        #endregion // Overrides

        #region Private Methods
        /// <summary>
        /// Helper function that checks whether all the weapons in a collection have their platform stats loaded
        /// </summary>
        /// <param name="weapons"></param>
        /// <returns></returns>
        private bool ArePlatformStatsLoaded()
        {
            bool allLoaded = true;

            foreach (ICharacter character in AllCharacters)
            {
                if (!character.IsStatLoaded(StreamableCharacterStat.PlatformStats))
                {
                    allLoaded = false;
                    break;
                }
            }

            return allLoaded;
        }

        /// <summary>
        /// 
        /// </summary>
        private void LoadPlatformStatsFromExportData()
        {
            // Get the streamed peds pack files
            List<ContentNode> streamedNodes = GameView.Content.Root.FindAll("streamedpeds", "directory");
            string streamedIndependentPath = LocalLoadUtil.GeneratePathForNodes(streamedNodes);

            IDictionary<RSG.Platform.Platform, Packfile> streamedPackFiles;
            if (!String.IsNullOrEmpty(streamedIndependentPath))
            {
                streamedPackFiles = LocalLoadUtil.OpenPackFiles(streamedIndependentPath, GameView, Config);
            }
            else
            {
                streamedPackFiles = new Dictionary<RSG.Platform.Platform, Packfile>();
            }

            // Get the component ped pack files
            List<ContentNode> componentNodes = GameView.Content.Root.FindAll("componentpeds", "directory");
            string componentIndependentPath = LocalLoadUtil.GeneratePathForNodes(componentNodes);

            IDictionary<RSG.Platform.Platform, Packfile> componentPackFiles;
            if (!String.IsNullOrEmpty(componentIndependentPath))
            {
                componentPackFiles = LocalLoadUtil.OpenPackFiles(componentIndependentPath, GameView, Config);
            }
            else
            {
                componentPackFiles = new Dictionary<RSG.Platform.Platform, Packfile>();
            }

            // Get the cutscene pack files
            List<ContentNode> cutsceneNodes = GameView.Content.Root.FindAll("cutspeds", "directory");
            string cutsceneIndependentPath = LocalLoadUtil.GeneratePathForNodes(cutsceneNodes);

            IDictionary<RSG.Platform.Platform, Packfile> cutscenePackFiles;
            if (!String.IsNullOrEmpty(cutsceneIndependentPath))
            {
                cutscenePackFiles = LocalLoadUtil.OpenPackFiles(cutsceneIndependentPath, GameView, Config);
            }
            else
            {
                cutscenePackFiles = new Dictionary<RSG.Platform.Platform, Packfile>();
            }

            // Go over all the weapons telling them to load the platform data
            foreach (ICharacter character in AllCharacters)
            {
                (character as LocalCharacter).LoadPlatformStatsFromExportData(componentPackFiles, cutscenePackFiles, streamedPackFiles);
            }

            // Close all the pack files we opened
            LocalLoadUtil.ClosePackFiles(streamedPackFiles);
            LocalLoadUtil.ClosePackFiles(componentPackFiles);
            LocalLoadUtil.ClosePackFiles(cutscenePackFiles);
        }
        #endregion // Private Methods

        #region IDisposable Methods
        /// <summary>
        /// 
        /// </summary>
        public override void Dispose()
        {
            base.Dispose();

            if (m_gv != null)
            {
                m_gv.Dispose();
            }
        }
        #endregion // IDisposable Methods
    } // ExportDataCharacterCollection
}
