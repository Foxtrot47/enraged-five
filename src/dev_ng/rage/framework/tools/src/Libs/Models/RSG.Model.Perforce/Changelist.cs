﻿using System;
using RSG.Model.Common;
using RSG.SourceControl.Perforce;

namespace RSG.Model.Perforce
{

    /// <summary>
    /// Perforce changelist object.
    /// </summary>
    public class Changelist : 
        AssetContainerBase
    {
        #region Constants
        /// <summary>
        /// Default number of maximum results to return.
        /// </summary>
        protected static readonly UInt32 DEFAULT_MAXIMUM_RESULTS = 100;
        #endregion // Constants

        #region Enumerations
        /// <summary>
        /// 
        /// </summary>
        public enum ChangelistState
        {
            Pending,
            Submitted,
            Shelved
        }
        #endregion // Enumerations

        #region Static Properties
        /// <summary>
        /// Default maximum number of results to return.
        /// </summary>
        public static UInt32 MaximumResults
        {
            get;
            set;
        }
        #endregion // Static Properties

        #region Properties and Associated Member Data

        /// <summary>
        /// Changelist numeric identifier.
        /// </summary>
        public UInt64 ID
        {
            get;
            protected set;
        }

        /// <summary>
        /// Changelist owner.
        /// </summary>
        public User Owner
        {
            get;
            protected set;
        }

        /// <summary>
        /// Changelist state.
        /// </summary>
        public ChangelistState State
        {
            get;
            protected set;
        }

        /// <summary>
        /// Changelist submitted date/time stamp.
        /// </summary>
        public DateTime SubmittedDate
        {
            get;
            protected set;
        }

        /// <summary>
        /// Changelist description.
        /// </summary>
        public String Description
        {
            get;
            protected set;
        }

        /// <summary>
        /// Changelist file list.
        /// </summary>
        public File[] Files
        {
            get;
            protected set;
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Static default constructor.
        /// </summary>
        static Changelist()
        {
            Changelist.MaximumResults = DEFAULT_MAXIMUM_RESULTS;
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p4"></param>
        /// <returns></returns>
        public static Changelist[] GetChangelists(P4 p4)
        {
            return (GetChangelists(p4, MaximumResults));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static Changelist[] GetChangelists(P4 p4, UInt32 max)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="username"></param>
        /// <returns></returns>
        public static Changelist[] GetChangelists(P4 p4, String username)
        {
            return (GetChangelists(p4, username, MaximumResults));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="username"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static Changelist[] GetChangelists(P4 p4, String username, UInt32 max)
        {
            User user = User.GetUser(username);
            return (GetChangelists(p4, user, max));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public static Changelist[] GetChangelists(P4 p4, User user)
        {
            return (GetChangelists(p4, user, MaximumResults));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="user"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static Changelist[] GetChangelists(P4 p4, User user, UInt32 max)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public static Changelist[] GetChangelists(P4 p4, ChangelistState state)
        {
            return (GetChangelists(p4, state, MaximumResults));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="state"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static Changelist[] GetChangelists(P4 p4, ChangelistState state, UInt32 max)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="username"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public static Changelist[] GetChangelists(P4 p4, String username, ChangelistState state)
        {
            User user = User.GetUser(username);
            return (GetChangelists(p4, user, state));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="user"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public static Changelist[] GetChangelists(P4 p4, User user, ChangelistState state)
        {
            throw new NotImplementedException();
        }
        #endregion // Static Controller Methods
    }

} // RSG.Model.Perforce namespace
