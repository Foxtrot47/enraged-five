﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Bounds;

namespace RSG.Model.Common.Map
{
    /// <summary>
    /// Primitive types 
    /// </summary>
    public enum CollisionPrimitiveType
    {
        Triangle,
        Box,
        Sphere,
        Cylinder,
        Capsule,
        Quad
    } // CollisionPrimitiveType

    /// <summary>
    /// 
    /// </summary>
    public class CollisionInfo
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public BNDFile.CollisionType CollisionFlags { get; set; }

        /// <summary>
        /// Type of primitve this is for.
        /// </summary>
        public CollisionPrimitiveType? PrimitiveType { get; set; }

        /// <summary>
        /// Number of 
        /// </summary>
        public uint Count { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CollisionInfo(BNDFile.CollisionType colFlags, CollisionPrimitiveType? primType)
        {
            CollisionFlags = colFlags;
            PrimitiveType = primType;
        }
        #endregion // Constructor(s)
    } // CollisionInfo
}
