﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Math;
using RSG.SceneXml;
using RSG.Model.Map.Utilities;

namespace RSG.Model.Map
{
    using AttributeContainer = Dictionary<String, Object>;

    public interface IMapInstance
    {
    }

    public enum InstanceType
    {
        Unknown = 0,
        HD = 1,
        LOD = 2,
        SLOD = 3,
        REF = 4,
    }

    /// <summary>
    /// A high level object that represents a map instance. A map instance
    /// is defined as a object that gets serialised out to the IPL file for the map
    /// section and can be thought of as the actual objects the game draws. Any
    /// instance uses a definition to resolve itself.
    /// </summary>
    public class MapInstance : IMapInstance, ISearchable
    {
        #region Properties

        /// <summary>
        /// The object name for this instance
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Instance's position in world-space.
        /// </summary>
        public Vector3f Position
        {
            get;
            private set;
        }

        /// <summary>
        /// A reference to the definition that this
        /// instance is instancing. The statistics and 
        /// the textures can be got from this
        /// </summary>
        public IMapDefinition ResolvedDefinition
        {
            get;
            private set;
        }
        
        /// <summary>
        /// The local bounding box for the definition,
        /// used to create areas that is used for statistics
        /// etc. This is a cc of the bounding box on a ObjectDef
        /// </summary>
        public BoundingBox3f WorldBoundingBox
        {
            get;
            private set;
        }

        /// <summary>
        /// The Lod parent for this map instance
        /// </summary>
        public MapInstance LodParent
        {
            get;
            private set;
        }

        /// <summary>
        /// The child instances that this instance
        /// has in the lod hierarchy
        /// </summary>
        public List<MapInstance> LodChildren
        {
            get;
            private set;
        }

        /// <summary>
        /// The type of instance this is. This is got
        /// from the lod hierarchy
        /// </summary>
        public InstanceType InstanceType
        {
            get;
            private set;
        }

        /// <summary>
        /// The lod distance for this particular
        /// instance. If the OBJ_INSTANCE_LOD_DISTANCE
        /// attrribute is set this comes from the instance
        /// else it comes from the definition
        /// </summary>
        public float LodDistance
        {
            get;
            private set;
        }

        /// <summary>
        /// The parent of this searchable object
        /// </summary>
        public ISearchable SearchableParent
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructors

        /// <summary>
        /// Default constructor
        /// </summary>
        public MapInstance(TargetObjectDef sceneObject, Scene scene, DefinitionList allDefinitions)
        {
            this.Position = sceneObject.NodeTransform.Translation;
            this.GetNameFromObject(sceneObject);
            this.GetDefinition(sceneObject, allDefinitions, scene);
            this.GetAttributesFromObject(sceneObject);
            this.GetBoundingBoxFromObject(sceneObject);
            this.DetermineInstanceType(sceneObject);
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public MapInstance(TargetObjectDef sceneObject, Scene scene, IMapDefinition definition)
        {
            this.Position = sceneObject.NodeTransform.Translation;
            this.GetNameFromObject(sceneObject);
            this.GetDefinition(sceneObject, definition);
            this.GetAttributesFromObject(sceneObject);
            this.GetBoundingBoxFromObject(sceneObject);
            this.DetermineInstanceType(sceneObject);
        }

        #endregion // Constructors

        #region Public Methods

        /// <summary>
        /// Creates the lod hierarchy
        /// </summary>
        public void ResolveLodHierarchy(TargetObjectDef sceneObject, Scene scene, List<MapInstance> siblingInstances)
        {
            this.LodChildren = new List<MapInstance>();
            this.LodParent = null;

            if (sceneObject.LOD != null)
            {
                if (sceneObject.LOD.Parent != null)
                {
                    foreach (MapInstance instance in siblingInstances)
                    {
                        if (instance.Name == sceneObject.LOD.Parent.Name)
                        {
                            this.LodParent = instance;
                            break;
                        }
                    }
                }
                if (sceneObject.LOD.Children != null && sceneObject.LOD.Children.Length > 0)
                {
                    List<String> lodChildren = new List<String>();
                    foreach (TargetObjectDef child in sceneObject.LOD.Children)
                    {
                        lodChildren.Add(child.Name);
                    }
                    foreach (MapInstance instance in siblingInstances)
                    {
                        if (lodChildren.Contains(instance.Name))
                        {
                            this.LodChildren.Add(instance);
                            if (this.LodChildren.Count == sceneObject.LOD.Children.Length)
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }

        #endregion // Public Methods

        #region Private Methods

        /// <summary>
        /// Gets the name from the given object
        /// </summary>
        private void GetNameFromObject(TargetObjectDef sceneObject)
        {
            this.Name = String.Copy(sceneObject.Name);
        }

        /// <summary>
        /// Gets the attributes from the given object
        /// </summary>
        private void GetAttributesFromObject(TargetObjectDef sceneObject)
        {
            if (sceneObject.Attributes.ContainsKey(AttrNames.OBJ_INSTANCE_LOD_DISTANCE))
            {
                if (sceneObject.Attributes.ContainsKey(AttrNames.OBJ_LOD_DISTANCE))
                {
                    this.LodDistance = (float)sceneObject.Attributes[AttrNames.OBJ_LOD_DISTANCE];
                }
            }
        }

        /// <summary>
        /// Gets the local bounding box for the given scene object and
        /// returns the inverse area for it. (Default return is 0.0f)
        /// </summary>
        private void GetBoundingBoxFromObject(TargetObjectDef sceneObject)
        {
            if (sceneObject.WorldBoundingBox != null)
            {
                this.WorldBoundingBox = new BoundingBox3f(sceneObject.WorldBoundingBox);
            }
            else
            {
                this.WorldBoundingBox = null;
            }
        }

        /// <summary>
        /// Resolves the definition for this instance.
        /// </summary>
        private void GetDefinition(TargetObjectDef sceneObject, DefinitionList allDefinitions, Scene scene)
        {
            this.ResolvedDefinition = allDefinitions.GetDefinition(sceneObject.RefFile, sceneObject.RefName, sceneObject.Name, scene.Filename);

            if (this.ResolvedDefinition != null)
            {
                if (this.ResolvedDefinition.Attributes.ContainsKey(AttrNames.OBJ_LOD_DISTANCE))
                {
                    this.LodDistance = (float)this.ResolvedDefinition.Attributes[AttrNames.OBJ_LOD_DISTANCE];
                }
            }
        }

        /// <summary>
        /// Resolves the definition for this instance.
        /// </summary>
        private void GetDefinition(TargetObjectDef sceneObject, IMapDefinition definition)
        {
            this.ResolvedDefinition = definition;
            if (this.ResolvedDefinition != null)
            {
                if (this.ResolvedDefinition.Attributes.ContainsKey(AttrNames.OBJ_LOD_DISTANCE))
                {
                    this.LodDistance = (float)this.ResolvedDefinition.Attributes[AttrNames.OBJ_LOD_DISTANCE];
                }
            }
        }

        /// <summary>
        /// Determines the type of lod that this instance represents
        /// </summary>
        private void DetermineInstanceType(TargetObjectDef sceneObject)
        {
            if (sceneObject.LOD != null)
            {
                if (!sceneObject.HasLODChildren())
                {
                    this.InstanceType = Map.InstanceType.HD;
                }
                else if (sceneObject.HasLODParent())
                {
                    this.InstanceType = Map.InstanceType.LOD;
                }
                else if (sceneObject.LOD.Children[0] != null)
                {
                    if (sceneObject.LOD.Children[0].HasLODChildren())
                    {
                        this.InstanceType = Map.InstanceType.SLOD;
                    }
                    else if (!sceneObject.LOD.Children[0].HasLODChildren())
                    {
                        this.InstanceType = Map.InstanceType.LOD;
                    }
                }
                else if (sceneObject.LOD.Children[0] == null)
                {
                    foreach (TargetObjectDef child in sceneObject.LOD.Children)
                    {
                        if (child != null)
                        {
                            if (sceneObject.LOD.Children[0].HasLODChildren())
                            {
                                this.InstanceType = Map.InstanceType.SLOD;
                            }
                            else
                            {
                                this.InstanceType = Map.InstanceType.LOD;
                            }
                        }
                    }
                    // Cannot find any of the children in the scene xml guess at lod
                    this.InstanceType = Map.InstanceType.Unknown;
                }
            }
            else
            {
                this.InstanceType = Map.InstanceType.HD;
            }
        }

        #endregion // Private Methods

        #region Walk Properties

        /// <summary>
        /// Walks through the children of ISearchables, going
        /// depth first
        /// </summary>
        public IEnumerable<ISearchable> WalkSearchDepth
        {
            get
            {
                yield return this;
                if (this.ResolvedDefinition != null && this.ResolvedDefinition.Textures != null)
                {
                    List<Texture> textures = new List<Texture>(this.ResolvedDefinition.Textures.Values);
                    foreach (ISearchable searchable in textures.Where(t => t is ISearchable == true))
                    {
                        foreach (ISearchable searchableChld in searchable.WalkSearchDepth)
                        {
                            yield return searchableChld;
                        }
                    }
                }
            }
        }

        #endregion // Walk Properties

    } // MapInstance
} // RSG.Model.Map
