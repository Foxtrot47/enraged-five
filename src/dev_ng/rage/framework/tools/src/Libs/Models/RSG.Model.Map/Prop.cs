﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.SceneXml;

namespace RSG.Model.Map
{

    #region Enumerations
    /// <summary>
    /// Flags to construct filters when searching for Props.
    /// </summary>
    [Flags]
    public enum PropFilter
    {
        None = 0x00000000,
        HasTunefile = 0x00000001,
        IsDynamic,
        IsDoor,
        IsUsed,
        IsFragment,
        HasVfx,
        All = 0x7FFFFFFF,
    }
    #endregion // Enumerations

    /// <summary>
    /// Lowest level model Prop object.
    /// </summary>
    /// This is the meat of the viewer.  
    public class Prop : IComparable<Prop>
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private static readonly String IMAGE_EXTENSION = ".jpg";
        private static readonly String JAXX_EXTENSION = "_Jaxx.jpg";
        #endregion // Constants

        #region Properties and Associated Member Data
        /// <summary>
        /// Unique name.
        /// </summary>
        public String Name
        {
            get { return m_sName; }
            private set { m_sName = value; }
        }
        private String m_sName;

        /// <summary>
        /// 
        /// </summary>
        public ObjectDef ObjectDef
        {
            get { return m_ObjectDef; }
            private set { m_ObjectDef = value; }
        }
        private ObjectDef m_ObjectDef;

        /// <summary>
        /// 
        /// </summary>
        public MapSection Map
        {
            get { return m_Map; }
            private set { m_Map = value; }
        }
        private MapSection m_Map;

        /// <summary>
        /// 
        /// </summary>
        public PropFilter CurrentFilter
        {
            get
            {
                PropFilter filter = PropFilter.None;
                if (this.HasTuneFile)
                    filter |= PropFilter.HasTunefile;
                if (this.IsDynamic)
                    filter |= PropFilter.IsDynamic;
                if (this.IsDoor)
                    filter |= PropFilter.IsDoor;
                if (this.IsUsed)
                    filter |= PropFilter.IsUsed;
                if (this.IsFragment)
                    filter |= PropFilter.IsFragment;
                if (this.HasVfx)
                    filter |= PropFilter.HasVfx;

                return (filter);
            }
        }

        /// <summary>
        /// Current configured LOD distance.
        /// </summary>
        public float LODDistance
        {
            get { return this.ObjectDef.GetAttribute(AttrNames.OBJ_LOD_DISTANCE, AttrDefaults.OBJ_LOD_DISTANCE); }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsDynamic
        {
            get { return this.ObjectDef.GetAttribute(AttrNames.OBJ_IS_DYNAMIC, AttrDefaults.OBJ_IS_DYNAMIC); }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsDoor
        {
            get { return this.ObjectDef.GetAttribute(AttrNames.OBJ_HAS_DOOR_PHYSICS, AttrDefaults.OBJ_HAS_DOOR_PHYSICS); }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsUsed
        {
            get { return false; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsFragment
        {
            get { return this.ObjectDef.GetAttribute(AttrNames.OBJ_IS_FRAGMENT, AttrDefaults.OBJ_IS_FRAGMENT); }
        }

        /// <summary>
        /// 
        /// </summary>
        public BoundingBox3f BoundingBox
        {
            get { return this.ObjectDef.LocalBoundingBox; }
        }

        /// <summary>
        /// Texture dictionary name.
        /// </summary>
        public String TextureDictionary
        {
            get { return this.ObjectDef.GetAttribute(AttrNames.OBJ_REAL_TXD, AttrDefaults.OBJ_REAL_TXD); }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool HasTuneFile
        {
            get { return m_bHasTuneFile; }
            private set { m_bHasTuneFile = value; }
        }
        private bool m_bHasTuneFile;

        /// <summary>
        /// 
        /// </summary>
        public bool HasVfx
        {
            get { return false; }
        }

        /// <summary>
        /// Image preview prop render.
        /// </summary>
        public String ImagePreviewFilename
        {
            get { return m_sImagePreviewFilename; }
            private set { m_sImagePreviewFilename = value; }
        }
        private String m_sImagePreviewFilename;

        /// <summary>
        /// Image preview prop render with the Jaxx.
        /// </summary>
        public String JaxxImagePreviewFilename
        {
            get { return m_sJaxxImagePreviewFilename; }
            private set { m_sJaxxImagePreviewFilename = value; }
        }
        private String m_sJaxxImagePreviewFilename;

        /// <summary>
        /// Represents whether the image can be found
        /// </summary>
        public Boolean ValidImages
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool HasAnim
        {
            get { return this.ObjectDef.GetAttribute(AttrNames.OBJ_HAS_ANIM, AttrDefaults.OBJ_HAS_ANIM); }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool HasUvAnim
        {
            get { return this.ObjectDef.GetAttribute(AttrNames.OBJ_HAS_UV_ANIM, AttrDefaults.OBJ_HAS_UV_ANIM); }
        }

        /// <summary>
        /// Drawable/fragment data size (in bytes).
        /// </summary>
        public int DrawableDataSize
        {
            get { return m_DrawableDataSize; }
            set { m_DrawableDataSize = value; }
        }
        private int m_DrawableDataSize;

        /// <summary>
        /// Texture data size (in bytes).
        /// </summary>
        public int TextureDataSize
        {
            get { return m_TextureDataSize; }
            set { m_TextureDataSize = value; }
        }
        private int m_TextureDataSize;

        /// <summary>
        /// Return poly count for object.
        /// </summary>
        public int PolyCount
        {
            get { return this.ObjectDef.PolyCount; }
        }

        /// <summary>
        /// Return poly count for all child collision.
        /// </summary>
        public int CollisionPolyCount
        {
            get
            {
                int polycount = 0;
                foreach (ObjectDef child in this.ObjectDef.Children)
                {
                    if (child.IsCollision())
                        polycount += child.PolyCount;
                }
                return (polycount);
            }
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor from SceneXml object definition.
        /// </summary>
        /// <param name="def"></param>
        /// <param name="parent"></param>
        /// <param name="thumbPrefixFolder"></param>
        public Prop(ObjectDef def, MapSection map, String streamPath)
        {
            this.Map = map;
            this.ObjectDef = def;
            this.Name = def.GetObjectName();

            String imagePath = Path.Combine(streamPath, this.Map.Level.Name, this.Map.Name);
            imagePath = Path.Combine(imagePath, this.Name);

            this.ImagePreviewFilename = imagePath + IMAGE_EXTENSION;
            this.JaxxImagePreviewFilename = imagePath + JAXX_EXTENSION;

            this.ValidImages = true;
            if (!System.IO.File.Exists(this.ImagePreviewFilename) || !System.IO.File.Exists(this.JaxxImagePreviewFilename))
            {
                this.ValidImages = false;
            }
        }
        #endregion // Constructor(s)

        #region IComparable<Prop> Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(Prop other)
        {
            return (this.Name.CompareTo(other.Name));
        }
        #endregion // IComparable<Prop> Methods
    }

    #region EventArg Classes
    /// <summary>
    /// EventArgs subclass with an associated Prop object.
    /// </summary>
    public class PropEventArgs : EventArgs
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Associated Prop object.
        /// </summary>
        public Prop AssociatedProp
        {
            get { return m_AssociatedProp; }
            private set { m_AssociatedProp = value; }
        }
        private Prop m_AssociatedProp;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="prop"></param>
        public PropEventArgs(Prop prop)
        {
            this.AssociatedProp = prop;
        }
        #endregion // Constructor(s)
    }

    /// <summary>
    /// EventArgs subclass with an associated collection of Prop objects.
    /// </summary>
    public class PropListEventArgs : EventArgs
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Associated collection of Prop objects.
        /// </summary>
        public List<Prop> AssociatedProps
        {
            get { return m_AssociatedProps; }
            private set { m_AssociatedProps = value; }
        }
        private List<Prop> m_AssociatedProps;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="props"></param>
        public PropListEventArgs(List<Prop> props)
        {
            this.AssociatedProps = new List<Prop>();
            this.AssociatedProps.AddRange(props);
        }

        #endregion // Constructor(s)
    }
    #endregion // EventArg Classes

} // RSG.Model.Map namespace
