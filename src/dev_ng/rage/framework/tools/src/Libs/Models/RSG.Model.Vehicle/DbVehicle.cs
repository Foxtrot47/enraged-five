﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Model.Asset;
using RSG.Model.Common;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Common.Dto.GameAssets.Bundles;

namespace RSG.Model.Vehicle
{
    /// <summary>
    /// 
    /// </summary>
    public class DbVehicle : Vehicle
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public DbVehicle(VehicleDto dto, IVehicleCollection parentCollection)
            : base(dto.Name, dto.FriendlyName, dto.GameName, (VehicleCategory)Enum.Parse(typeof(VehicleCategory), dto.Category), parentCollection)
        {
        }
        #endregion // Constructor(s)

        #region StreamableAssetContainerBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="statsToLoad"></param>
        protected override void LoadStatsInternal(IEnumerable<StreamableStat> statsToLoad)
        {
            Debug.Assert(ParentCollection is SingleBuildVehicleCollection, "Parent collection isn't an SingleBuildVehicleCollection.");
            SingleBuildVehicleCollection dbCollection = (SingleBuildVehicleCollection)ParentCollection;

            if (!AreStatsLoaded(statsToLoad))
            {
                try
                {
                    using (BuildClient client = new BuildClient())
                    {
                        VehicleStatBundleDto bundle = client.GetVehicleStat(dbCollection.BuildIdentifier, Hash.ToString());
                        LoadStatsFromDatabase(bundle);
                    }
                }
                catch (Exception e)
                {
                    Log.Log__Exception(e, "Unhandled exception while retrieving weapon stats.");
                }
            }
        }
        #endregion // StreamableAssetContainerBase Overrides

        #region Internal Methods
        /// <summary>
        /// Loads data into this vehicle for a particular build
        /// </summary>
        internal void LoadStatsFromDatabase(VehicleStatBundleDto bundle)
        {
            this.PolyCount = bundle.PolyCount;
            this.CollisionCount = bundle.CollisionCount;
            this.BoneCount = bundle.BoneCount;
            this.HasLod1 = bundle.HasLod1;
            this.HasLod2 = bundle.HasLod2;
            this.HasLod3 = bundle.HasLod3;
            this.WheelCount = bundle.WheelCount;
            this.SeatCount = bundle.SeatCount;
            this.DoorCount = bundle.DoorCount;
            this.LightCount = bundle.LightCount;
            this.ExtraCount = bundle.ExtraCount;
            this.RudderCount = bundle.RudderCount;
            this.RotorCount = bundle.RotorCount;
            this.PropellerCount = bundle.PropellerCount;
            this.ElevatorCount = bundle.ElevatorCount;
            this.HandlebarCount = bundle.HandlebarCount;

            // Process the shaders next
            List<Shader> shaders = new List<Shader>();
            foreach (ShaderStatBundleDto shaderBundle in bundle.ShaderStats)
            {
                shaders.Add(new Shader(shaderBundle));
            }
            shaders.Sort();
            this.Shaders = shaders.ToArray();

            this.AssetChildren.Clear();
            this.AssetChildren.AddRange(Textures);

            // Next load in the platform stats
            PlatformStats = new Dictionary<RSG.Platform.Platform, IVehiclePlatformStat>();

            foreach (VehiclePlatformStatBundleDto platformStatDto in bundle.VehiclePlatformStats)
            {
                RSG.Platform.Platform platform = platformStatDto.Platform.Value;
                IVehiclePlatformStat platformStat = new VehiclePlatformStat(platform, platformStatDto.PhysicalSize, platformStatDto.VirtualSize, platformStatDto.HiPhysicalSize, platformStatDto.HiVirtualSize);
                PlatformStats.Add(platform, platformStat);
            }
        }
        #endregion // Internal Methods
    } // DbVehicle
}
