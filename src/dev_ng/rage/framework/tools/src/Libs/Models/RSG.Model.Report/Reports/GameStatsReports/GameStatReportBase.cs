﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web.UI;
using System.Windows;
using System.Windows.Controls.DataVisualization.Charting;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.ConfigParser;
using RSG.Model.Report.Reports.GameStatsReports.Model;
using RSG.SourceControl.Perforce;
using RSG.Model.Report.Reports.GameStatsReports.Tests;
using RSG.Base.Configuration.Reports;
using RSG.Base.Configuration;
using RSG.Base.Logging;

namespace RSG.Model.Report.Reports.GameStatsReports
{
    /// <summary>
    /// 
    /// </summary>
    public enum GameStatReportMode
    {
        PerBuild,
        PerChangelist
    } // GameStatReportMode

    /// <summary>
    /// 
    /// </summary>
    public abstract class GameStatReportBase : Report, IDynamicReport, IDisposable
    {
        #region Constants
        // Default values for the customizable parameters
        private const int    c_maxGraphDays = 7;
        private const int    c_maxTableResults = 10;
        private const string c_perBuildCacheDir = @"cache:\Reports\Automated\PerBuild";
        private const string c_perChangelistCacheDir = @"cache:\Reports\Automated\PerChangelist";

        // Constants for use in the html reports
        protected const string BGCOL_DEFAULT = "#999";
        protected const string FGCOL_DEFAULT = "#FFF";
        protected const string BGCOL_ERROR   = "#F00";
        protected const string BGCOL_WARNING = "#F60";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Which mode we should be running in
        /// </summary>
        public GameStatReportMode Mode
        {
            get;
            set;
        }

        /// <summary>
        /// Location of the stats file to use in perforce
        /// </summary>
        private string StatsFile
        {
            get
            {
                if (Mode == GameStatReportMode.PerBuild)
                {
                    return ReportsConfig.AutomatedReports.PerBuildStatsFile;
                }
                else if (Mode == GameStatReportMode.PerChangelist)
                {
                    return ReportsConfig.AutomatedReports.PerChangelistStatsFile;
                }
                else
                {
                    throw new ArgumentException(String.Format("Unknown report mode detected: {0}", Mode));
                }
            }
        }

        /// <summary>
        /// The cache directory to store any temporary files in
        /// </summary>
        private string RelativeCacheDir
        {
            get
            {
                if (Mode == GameStatReportMode.PerBuild)
                {
                    return c_perBuildCacheDir;
                }
                else if (Mode == GameStatReportMode.PerChangelist)
                {
                    return c_perChangelistCacheDir;
                }
                else
                {
                    throw new ArgumentException(String.Format("Unknown report mode detected: {0}", Mode));
                }
            }
        }

        /// <summary>
        /// The starting date from where to generate the report
        /// </summary>
        public DateTime? StartDate
        {
            get;
            set;
        }

        /// <summary>
        /// The end date to which to generate the report
        /// </summary>
        public DateTime? EndDate
        {
            get;
            set;
        }

        /// <summary>
        /// Number of days the graph will show
        /// </summary>
        public uint MaxGraphDays
        {
            get;
            set;
        }

        /// <summary>
        /// Number of results to show in the tables
        /// </summary>
        public uint MaxTableResults
        {
            get;
            set;
        }

        /// <summary>
        /// The perforce connection object to use for p4 queries
        /// </summary>
        protected P4 PerforceConnection
        {
            get
            {
                if (m_perforceConnection == null)
                {
                    m_perforceConnection = new P4();
                    m_perforceConnection.Connect();
                    m_createdConnection = true;
                }
                return m_perforceConnection;
            }
            set
            {
                m_perforceConnection = value;
            }
        }
        private P4 m_perforceConnection;
        private bool m_createdConnection = false;

        /// <summary>
        /// Report config
        /// </summary>
        protected IReportsConfig ReportsConfig
        {
            get
            {
                if (m_config == null)
                {
                    m_config = ConfigFactory.CreateReportConfig();
                }
                return m_config;
            }
            set
            {
                m_config = value;
            }
        }
        private IReportsConfig m_config;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="desc"></param>
        public GameStatReportBase(string name, string desc)
            : this(name, desc, null, null)
        {
        }

        /// <summary>
        /// Constructor that takes a p4 object
        /// </summary>
        /// <param name="perforceObject"></param>
        public GameStatReportBase(string name, string desc, P4 perforceObject, IReportsConfig config)
            : base(name, desc)
        {
            PerforceConnection = perforceObject;
            ReportsConfig = config;

            Mode = GameStatReportMode.PerBuild;
            MaxGraphDays = c_maxGraphDays;
            MaxTableResults = c_maxTableResults;
        }
        #endregion // Constructor(s)
        
        #region Controller Methods
        /// <summary>
        /// Generate the report dynamically.
        /// </summary>
        public virtual void Generate(ConfigGameView gv)
        {
            // Get the revision information for the stats file
            DateTime start = (StartDate == null ? DateTime.Now.AddDays(-MaxGraphDays) : (DateTime)StartDate);
            DateTime end = (EndDate == null ? start.AddDays(MaxGraphDays) : (DateTime)EndDate);

            P4API.P4RecordSet recordSet = GetStatsFileModifications(start, end);

            // Retrieve the contents of the files and cache their contents
            string cacheDir = RelativeCacheDir.Replace("cache:", gv.ToolsCacheDir);
            EnsureStatsFilesAreSynced(recordSet, cacheDir);

            // Extract the information that we are interested in
            IList<TestSession> testSessions = ProcessHistoricalStatsFiles(recordSet, cacheDir);

            // Get the list of unique test names
            IList<string> uniqueTestNames = testSessions.SelectMany(session => session.TestResults).Select(test => test.Name).Distinct().ToList();
            
            // Write out the report
            GenerateReport(testSessions, uniqueTestNames, start, end, cacheDir);
        }
        #endregion // Controller Methods

        #region Abstract Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="testSessions"></param>
        /// <param name="uniqueTestNames"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <param name="cacheDir"></param>
        protected abstract void GenerateReport(IList<TestSession> testSessions, IEnumerable<string> uniqueTestNames, DateTime start, DateTime end, string cacheDir);
        #endregion // Abstract Methods

        #region Private Methods
        #region Gathering Data
        /// <summary>
        /// 
        /// </summary>
        /// <param name="start"></param>
        /// <returns></returns>
        protected P4API.P4RecordSet GetStatsFileModifications(DateTime start, DateTime end)
        {
            // Get the history information for the stats file
            string startDateString = String.Format("{0:d4}/{1:d2}/{2:d2}:{3:d2}:{4:d2}", start.Year, start.Month, start.Day, start.Hour, start.Minute);
            string endDateString = String.Format("{0:d4}/{1:d2}/{2:d2}:{3:d2}:{4:d2}", end.Year, end.Month, end.Day, end.Hour, end.Minute);
            return PerforceConnection.Run("changes", String.Format("{0}@{1},{2}", StatsFile, startDateString, endDateString));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="recordSet"></param>
        /// <param name="reportsCacheDir"></param>
        protected void EnsureStatsFilesAreSynced(P4API.P4RecordSet recordSet, string reportsCacheDir)
        {
            // Cache the contents of the files to the tools cache directory
            if (!Directory.Exists(reportsCacheDir))
            {
                Directory.CreateDirectory(reportsCacheDir);
            }

            // If we don't have a particular revision of the stats file retrieve and cache it
            foreach (P4API.P4Record record in recordSet)
            {
                string tempFilename = String.Format("{0}@{1}", Path.Combine(reportsCacheDir, Path.GetFileName(StatsFile)), record["change"]);
                DateTime timestamp;

                // If the file doesn't exist get the contents of the file from p4
                if (!File.Exists(tempFilename))
                {
                    P4API.P4RecordSet printRecordSet = PerforceConnection.Run("print", String.Format("{0}@{1}", StatsFile, record["change"]));

                    using (FileStream stream = File.Create(tempFilename))
                    {
                        using (TextWriter writer = new StreamWriter(stream))
                        {
                            foreach (string message in printRecordSet.Messages)
                            {
                                writer.Write(message);
                            }
                            writer.Flush();
                        }
                    }

                    // Update the creation time to the time the file was submitted to p4
                    timestamp = new DateTime(1970, 1, 1);
                    timestamp = timestamp.AddSeconds(UInt32.Parse(record["time"]));
                    File.SetCreationTime(tempFilename, timestamp);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="recordSet"></param>
        /// <param name="reportsCacheDir"></param>
        /// <returns></returns>
        protected IList<TestSession> ProcessHistoricalStatsFiles(P4API.P4RecordSet recordSet, string reportsCacheDir)
        {
            List<TestSession> testSessions = new List<TestSession>();

            foreach (P4API.P4Record record in recordSet)
            {
                string tempFilename = String.Format("{0}@{1}", Path.Combine(reportsCacheDir, Path.GetFileName(StatsFile)), record["change"]);

                // The file should really exist
                if (File.Exists(tempFilename))
                {
                    DateTime timestamp = File.GetCreationTime(tempFilename);

                    // Open up the file and add it to our list
                    using (Stream stream = File.OpenRead(tempFilename))
                    {
                        try
                        {
                            testSessions.Add(new TestSession(stream, timestamp));
                        }
                        catch (System.Exception ex)
                        {
                            Log.Log__Error("Unable to parse test session data in {0}.", tempFilename);
                        }
                    }
                }
            }

            return testSessions;
        }
        #endregion // Gathering Data

        #region Generating Report
        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        protected void WriteReportHeader(HtmlTextWriter writer, IList<ModificationInfo> modifications, IEnumerable<string> errors, IEnumerable<string> warnings, IEnumerable<string> messages)
        {
            string defaultStyle = String.Format("background-color: {0}; color: {1}", BGCOL_DEFAULT, FGCOL_DEFAULT);
            string errorStyle = String.Format("background-color: {0}; color: {1}", BGCOL_ERROR, FGCOL_DEFAULT);
            string warningStyle = String.Format("background-color: {0}; color: {1}", BGCOL_WARNING, FGCOL_DEFAULT);

            writer.AddAttribute(HtmlTextWriterAttribute.Border, "1");
            writer.AddAttribute(HtmlTextWriterAttribute.Width, "100%");
            writer.RenderBeginTag(HtmlTextWriterTag.Table);
            {
                // Header row
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.AddAttribute(HtmlTextWriterAttribute.Colspan, "2");
                writer.AddAttribute(HtmlTextWriterAttribute.Align, "center");
                writer.AddAttribute(HtmlTextWriterAttribute.Style, String.Format("background-color: {0}; color: {1}; font-size: 16pt", BGCOL_DEFAULT, FGCOL_DEFAULT));
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(errors.Any() ? "Failure" : "Success");
                writer.RenderEndTag();
                writer.RenderEndTag();

                // Project Row
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.AddAttribute(HtmlTextWriterAttribute.Style, defaultStyle);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write("Project");
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
#warning TODO: Source of project name
                writer.Write("gta5");
                writer.RenderEndTag();
                writer.RenderEndTag();

                // Build Time Row
                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.AddAttribute(HtmlTextWriterAttribute.Style, defaultStyle);
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write("Build Time");
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                writer.Write(DateTime.Now);
                writer.RenderEndTag();
                writer.RenderEndTag();

                // Max Changelist Row
                if (modifications != null && modifications.Count > 0)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                    writer.AddAttribute(HtmlTextWriterAttribute.Style, defaultStyle);
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write("Max Changelist");
                    writer.RenderEndTag();
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(modifications.First().Changelist);
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                }

                // Messages Row(s)
                foreach (string error in errors)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                    writer.AddAttribute(HtmlTextWriterAttribute.Style, errorStyle);
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write("Error");
                    writer.RenderEndTag();
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(error);
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                }

                foreach (string warning in warnings)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                    writer.AddAttribute(HtmlTextWriterAttribute.Style, warningStyle);
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write("Warning");
                    writer.RenderEndTag();
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(warning);
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                }

                foreach (string message in messages)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                    writer.AddAttribute(HtmlTextWriterAttribute.Style, defaultStyle);
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write("Message");
                    writer.RenderEndTag();
                    writer.RenderBeginTag(HtmlTextWriterTag.Td);
                    writer.Write(message);
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                }
            }
            writer.RenderEndTag();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="imageStreams"></param>
        /// <param name="email"></param>
        protected void WriteReportCommonGraphs(HtmlTextWriter writer, IEnumerable<GraphImage> commonImages, bool includeSubHeaders, bool email)
        {
            // Write the graphs header
            writer.RenderBeginTag(HtmlTextWriterTag.H2);
            writer.AddAttribute(HtmlTextWriterAttribute.Name, "Common Graphs");
            writer.RenderBeginTag(HtmlTextWriterTag.A);
            writer.Write("Common Graphs");
            writer.RenderEndTag();
            writer.RenderEndTag();

            // Output each graph individually
            foreach (GraphImage image in commonImages)
            {
                string imageSource = image.Filepath;
                if (email)
                {
                    imageSource = String.Format("cid:{0}", image.Name);
                }

                // Write the smoke test name
                if (includeSubHeaders)
                {
                    writer.RenderBeginTag(HtmlTextWriterTag.H3);
                    writer.AddAttribute(HtmlTextWriterAttribute.Name, image.Name);
                    writer.RenderBeginTag(HtmlTextWriterTag.A);
                    writer.Write(image.Name);
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                }

                // Write out the image itself
                writer.AddAttribute(HtmlTextWriterAttribute.Src, imageSource);
                writer.RenderBeginTag(HtmlTextWriterTag.Img);
                writer.RenderEndTag();
                //writer.RenderBeginTag(HtmlTextWriterTag.Br);
                //writer.RenderEndTag();
            }
        }
        #endregion // Generating Report
        #endregion // Private Methods

        #region IDisposable Implementation
        /// <summary>
        /// Perform cleanup
        /// </summary>
        public virtual void Dispose()
        {
            // Disconnect from perforce (if we created the p4 object)
            if (m_createdConnection)
            {
                PerforceConnection.Disconnect();
            }
        }
        #endregion // IDisposable Implementation
    } // AutomatedReportBase
}
