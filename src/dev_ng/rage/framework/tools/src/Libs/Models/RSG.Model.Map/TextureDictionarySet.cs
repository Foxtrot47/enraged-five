﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.SceneXml;
using RSG.SceneXml.Material;
using RSG.Base.ConfigParser;
using RSG.Model.Map.Utilities;

namespace RSG.Model.Map
{
    /// <summary>
    /// Represents a dictionary of unique texture dictionaries,
    /// the way that the dictionaries are compared are by name
    /// </summary>
    public class TextureDictionarySet : IEnumerable<TextureDictionary>, ISearchable
    {
        #region Properties

        /// <summary>
        /// The object name for this instance
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// The collection of texture dictionaries in this set, indexed by texture name
        /// so has to have unique texture dictionary names
        /// </summary>
        private Dictionary<String, TextureDictionary> TextureDictionaries
        {
            get;
            set;
        }

        /// <summary>
        /// Returns the texture dictionary in this set with the given name or null
        /// if one doesn't exist
        /// </summary>
        public TextureDictionary this[String name]
        {
            get
            {
                TextureDictionary textureDictionary = null;
                this.TextureDictionaries.TryGetValue(name, out textureDictionary);
                return textureDictionary;
            }
        }

        /// <summary>
        /// The parent of this searchable object
        /// </summary>
        public ISearchable SearchableParent
        {
            get;
            set;
        }

        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor, creates a empty texture dictionary set
        /// </summary>
        public TextureDictionarySet()
        {
            this.TextureDictionaries = new Dictionary<String,TextureDictionary>();
            this.Name = String.Empty;
        }

        /// <summary>
        /// Creates a texture dictionary set and fills it with the given texture dictionaries
        /// </summary>
        public TextureDictionarySet(IList<TextureDictionary> textureDictionaries)
        {
            this.TextureDictionaries = new Dictionary<String, TextureDictionary>();
            this.Name = String.Empty;
            foreach (TextureDictionary textureDictionary in textureDictionaries)
            {
                this.AddTextureDictionary(textureDictionary);
            }
        }

        /// <summary>
        /// Creates a texture dictionary set using a list of scene objects
        /// </summary>
        public TextureDictionarySet(IList<ObjectDef> sceneObjects, Scene scene)
        {
            this.TextureDictionaries = new Dictionary<String, TextureDictionary>();
            this.Name = String.Empty;
            foreach (ObjectDef sceneObject in sceneObjects)
            {
                String txdName = sceneObject.GetAttribute(AttrNames.OBJ_TXD, AttrDefaults.OBJ_TXD);
                if (!String.IsNullOrEmpty(txdName) && txdName != "CHANGEME")
                {
                    if (!this.ContainsTextureDictionary(txdName))
                    {
                        this.AddTextureDictionary(new TextureDictionary(txdName));
                    }

                    IList<Texture> textures = new List<Texture>();
                    GetTexturesFromObject(sceneObject, ref textures, scene);
                    this[txdName].AddTextures(textures);
                }
            }
        }

        /// <summary>
        /// Creates a texture dictionary set using a list of map definitions
        /// </summary>
        public TextureDictionarySet(IList<IMapDefinition> definitions, MapSection section)
        {
            this.TextureDictionaries = new Dictionary<String, TextureDictionary>();
            this.SearchableParent = section;
            this.Name = String.Empty;
            foreach (IMapDefinition definition in definitions)
            {
                if (definition.Attributes.ContainsKey(AttrNames.OBJ_TXD))
                {
                    String txdName = definition.Attributes[AttrNames.OBJ_TXD] as String;
                    if (!String.IsNullOrEmpty(txdName) && txdName != AttrDefaults.OBJ_TXD)
                    {
                        txdName = txdName.ToLower();
                        if (!this.ContainsTextureDictionary(txdName))
                        {
                            TextureDictionary newDictionary = new TextureDictionary(txdName);
                            newDictionary.SearchableParent = this;
                            this.AddTextureDictionary(newDictionary);
                        }
                        IList<Texture> textures = new List<Texture>();
                        foreach (Texture texture in definition.Textures.Values)
                        {
                            Texture newTexture = new Texture(texture.Filename, texture.AlphaFilepath, texture.Type);
                            newTexture.SearchableParent = this[txdName];
                            textures.Add(newTexture);
                        }
                        this[txdName].AddTextures(textures);
                    }
                }
            }
        }

        #endregion //Constructor(s)

        #region Private Function(s)

        /// <summary>
        /// Gets the textures that are on the given object and adds them to the referenced list
        /// </summary>
        private void GetTexturesFromObject(ObjectDef sceneObject, ref IList<Texture> textures, Scene scene)
        {
            MaterialDef material = null;
            if (scene.MaterialLookup.TryGetValue(sceneObject.Material, out material))
            {
                this.GetTexturesFromMaterial(material, ref textures, true);
            }
        }

        /// <summary>
        /// Goes through the material def and adds all the textures into the referenced textures list, if the
        /// parameter recursive is true it will also go through all the sub materials if any exist
        /// </summary>
        private void GetTexturesFromMaterial(MaterialDef material, ref IList<Texture> textures, Boolean recursive)
        {
            if (material.HasTextures)
            {
                for (int i = 0; i < material.Textures.Length; i++)
                {
                    TextureDef texture = material.Textures[i];

                    String filename = texture.FilePath;
                    String name = System.IO.Path.GetFileNameWithoutExtension(texture.FilePath);
                    TextureTypes type = texture.Type;

                    if (type != TextureTypes.None)
                    {
                        Texture newTexture = null;
                        if (type == TextureTypes.DiffuseMap || type == TextureTypes.SpecularMap || type == TextureTypes.BumpMap)
                        {
                            // We need to determine if the next texture is an appropriate alpha texture, if a next texture exists
                            int nextIndex = i + 1;
                            if (nextIndex < material.Textures.Length)
                            {
                                TextureDef nextTexture = material.Textures[i + 1];
                                if (type == TextureTypes.DiffuseMap && nextTexture.Type == TextureTypes.DiffuseAlpha)
                                {
                                    newTexture = new Texture(filename, nextTexture.FilePath, type);
                                    textures.Add(newTexture);
                                    i++;
                                    continue;
                                }
                                else if (type == TextureTypes.SpecularMap && nextTexture.Type == TextureTypes.SpecularAlpha)
                                {
                                    newTexture = new Texture(filename, nextTexture.FilePath, type);
                                    textures.Add(newTexture);
                                    i++;
                                    continue;
                                }
                                else if (type == TextureTypes.BumpMap && nextTexture.Type == TextureTypes.BumpAlpha)
                                {
                                    newTexture = new Texture(filename, nextTexture.FilePath, type);
                                    textures.Add(newTexture);
                                    i++;
                                    continue;
                                }
                            }
                        }

                        // Shouldn't add any alpha textures as a indiviual texture, alpha textures need to go in pairs with another texture
                        if (type == TextureTypes.DiffuseAlpha || type == TextureTypes.BumpAlpha || type == TextureTypes.SpecularAlpha)
                        {
                            continue;
                        }

                        newTexture = new Texture(filename, type);
                        textures.Add(newTexture);
                    }

                }
            }

            if (material.HasSubMaterials)
            {
                foreach (MaterialDef child in material.SubMaterials)
                {
                    GetTexturesFromMaterial(child, ref textures, true);
                }
            }
        }

        #endregion // Private Function(s)

        #region Dictionary Manipulation

        /// <summary>
        /// Adds a texture dictionary to this set iff it isn't already
        /// </summary>
        /// <param name="textureDictionary">The texture dictionary to add</param>
        public void AddTextureDictionary(TextureDictionary textureDictionary)
        {
            if (!this.TextureDictionaries.ContainsKey(textureDictionary.Name))
            {
                this.TextureDictionaries.Add(textureDictionary.Name, textureDictionary);
            }
        }

        /// <summary>
        /// Removes a texture dictionary from this set iff it is currently a member of this set
        /// </summary>
        /// <param name="textureDictionary">The texture dictionary to remove</param>
        public void RemoveTextureDictionary(TextureDictionary textureDictionary)
        {
            if (this.TextureDictionaries.ContainsKey(textureDictionary.Name))
            {
                this.TextureDictionaries.Remove(textureDictionary.Name);
            }
        }

        /// <summary>
        /// Clears the texture dictionaries making this set empty
        /// </summary>
        public void ClearTextureDictionaries()
        {
            this.TextureDictionaries.Clear();
        }

        /// <summary>
        /// Returns true iff the texture dictionary set conatins a texture dictionary with
        /// the given name
        /// </summary>
        public Boolean ContainsTextureDictionary(String name)
        {
            return this.TextureDictionaries.ContainsKey(name);
        }

        #endregion // Dictionary Manipulation

        #region IEnumerable<TextureDictionary>

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator<TextureDictionary> IEnumerable<TextureDictionary>.GetEnumerator()
        {
            foreach (TextureDictionary d in this.TextureDictionaries.Values)
            {
                yield return d;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerator)this);
        }

        #endregion // IEnumerable<TextureDictionary>

        #region Walk Properties

        /// <summary>
        /// Walks through the children of ISearchables, going
        /// depth first
        /// </summary>
        public IEnumerable<ISearchable> WalkSearchDepth
        {
            get
            {
                yield return this;
                List<TextureDictionary> textureDictionaries = new List<TextureDictionary>(this.TextureDictionaries.Values);
                foreach (ISearchable searchable in textureDictionaries.Where(d => d is ISearchable == true))
                {
                    foreach (ISearchable searchableChld in searchable.WalkSearchDepth)
                    {
                        yield return searchableChld;
                    }
                }
            }
        }

        #endregion // Walk Properties

    } // TextureDictionarySet
} // RSG.Model.Map
