﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;

namespace RSG.Model.Common
{
    /// <summary>
    /// 
    /// </summary>
    public interface ICharacterCategoryCollection : IAsset, ICollection<ICharacter>, IEnumerable<ICharacter>
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        ObservableCollection<ICharacter> Characters { get; }

        /// <summary>
        /// Category this collection is for.
        /// </summary>
        CharacterCategory Category { get; }
        #endregion // Properties
    } // ICharacterCategoryCollection
}
