﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace RSG.Model.Common
{
    /// <summary>
    /// Character type enumeration.
    /// </summary>
    /// <remarks>The description attribute is the name of the folder that corresponds to the enum value.</remarks>
    public enum CharacterCategory
    {
        [Description("Unknown")]
        Unknown,

        [Description("streamedpeds")]
        Streamed,

        [Description("componentpeds")]
        Component,

        [Description("cutspeds")]
        Cutscene
    }

    /// <summary>
    /// Comparer for comparing by enum names
    /// </summary>
    public class CharacterCategoryComparer : IComparer<CharacterCategory>
    {
        public int Compare(CharacterCategory x, CharacterCategory y)
        {
            return x.ToString().CompareTo(y.ToString());
        }
    }
}
