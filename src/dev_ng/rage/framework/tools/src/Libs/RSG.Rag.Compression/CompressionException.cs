﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Compression
{
    /// <summary>
    /// Custom exception for encoding/decoding errors.
    /// </summary>
    public class CompressionException : Exception
    {
        /// <summary>
        /// Creates a new instance of the <see cref="CompressionException"/> class.
        /// </summary>
        public CompressionException()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CompressionException"/> class with
        /// a specified error message.
        /// </summary>
        /// <param name="message">The message that describes the error.</param>
        public CompressionException(string message)
            : base(message)
        {
        }
    } //CompressionException
}
