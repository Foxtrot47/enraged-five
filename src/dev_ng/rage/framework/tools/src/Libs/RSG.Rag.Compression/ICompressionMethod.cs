﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Compression
{
    /// <summary>
    /// Method of compressing/decompressing data.
    /// </summary>
    internal interface ICompressionMethod
    {
        /// <summary>
        /// Retrieves the encoded version of the passed in data.
        /// </summary>
        /// <param name="data">Data to be encoded.</param>
        /// <returns>Encoded data.</returns>
        byte[] Encode(byte[] data);

        /// <summary>
        /// Retrieves the decoded version of the passed in data.
        /// </summary>
        /// <param name="data">Data to be decoded.</param>
        /// <param name="decodedDataLength">Length of the decoded data.</param>
        /// <returns>Deocded data.</returns>
        byte[] Decode(byte[] data, int decodedDataLength);
    } // IcompressionMethod
}
