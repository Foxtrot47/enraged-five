﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Compression
{
    /// <summary>
    /// 
    /// </summary>
    public class CompressionFactory
    {
        #region Member Data
        /// <summary>
        /// Collection of known compression packet types.
        /// </summary>
        private IDictionary<String, Type> _compressionPacketTypes =
            new Dictionary<String, Type>();
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CompressionFactory()
        {
            RegisterCompressionPacketType<DatCompressionPacket>(DatCompressionPacket.StaticHeaderPrefix);
            RegisterCompressionPacketType<FiCompressionPacket>(FiCompressionPacket.StaticHeaderPrefix);
            RegisterCompressionPacketType<NoCompressionPacket>(NoCompressionPacket.StaticHeaderPrefix);
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Compresses the provided data with the specified compressionMethod.
        /// </summary>
        /// <param name="data"></param>
        /// <param name="compressionMethod"></param>
        /// <returns>Compression packet that is ready to send across the network.</returns>
        public CompressionPacket Compress(byte[] data, String compressionMethod)
        {
            Type compressionPacketType;
            if (!_compressionPacketTypes.TryGetValue(compressionMethod, out compressionPacketType))
            {
                throw new DataException(String.Format("Unknown compression type '{0}'.", compressionMethod));
            }

            throw new NotImplementedException();
        }

        /// <summary>
        /// Converts raw byte data into compression packets ready for decompression.
        /// </summary>
        /// <param name="buffer">Raw data buffer.</param>
        /// <param name="amountRead">Amount of data that we read from the buffer.</param>
        /// <returns>List of compression packets that we managed to extract from the buffer.</returns>
        public IList<CompressionPacket> Read(byte[] buffer, out int amountRead)
        {
            IList<CompressionPacket> packets = new List<CompressionPacket>();
            int packetStart = 0;

            // Read data until there isn't enough left.
            while (packetStart < buffer.Length)
            {
                // Try and read in the compression header to determine the type of compression data we should create.
                String headerStr = null;
                if (!ReadHeaderPrefix(buffer, packetStart, out headerStr))
                {
                    break;
                }

                Type compressionPacketType;
                if (!_compressionPacketTypes.TryGetValue(headerStr, out compressionPacketType))
                {
                    throw new DataException(String.Format("Unknown compression type '{0}'.", headerStr));
                }

                // Create an instance of the compression data.
                CompressionPacket packet = (CompressionPacket)Activator.CreateInstance(compressionPacketType);

                // Attempt to parse the header data.
                if (!packet.ParseHeader(buffer, packetStart))
                {
                    // Not enough data.
                    break;
                }
                else
                {
                    if (packet.ParseData(buffer, packetStart + packet.HeaderLength))
                    {
                        packets.Add(packet);
                        packetStart += packet.HeaderLength + packet.CompressedDataLength;
                    }
                    else
                    {
                        break;
                    }
                }
            }

            // Set the amount of data we read and return the results.
            amountRead = packetStart;
            return packets;
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="header"></param>
        private void RegisterCompressionPacketType<T>(String header) where T : CompressionPacket
        {
            _compressionPacketTypes.Add(header, typeof(T));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="start"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        private bool ReadHeaderPrefix(byte[] data, int start, out string prefix)
        {
            // The first 4 characters should contain the type of compression used in this header
            const int PREFIX_LENGTH = 4;

            if (start + PREFIX_LENGTH > data.Length)
            {
                // not enough data, incomplete header!
                prefix = null;
                return false;
            }

            char[] headerBytes = new char[PREFIX_LENGTH];

            int i = 0;
            for (i = 0; i < PREFIX_LENGTH; i++)
            {
                headerBytes[i] = (char)data[i + start];
                if (headerBytes[i] == ':') // all headers MUST end with a ':'
                {
                    break;
                }
            }

            if (i == PREFIX_LENGTH)
            {
                string msg = "Received badly formatted data at start=" + start + Environment.NewLine + BitConverter.ToString(data);
                throw new DataException(msg);
            }

            prefix = new string(headerBytes, 0, i);
            return true;
        }
        #endregion // Private Methods
    } // CompressionFactory
}
