﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Compression
{
    /// <summary>
    /// Compression method where the coded version of a piece of data is the same as the unencoded data.
    /// </summary>
    internal class NoCompressionMethod : ICompressionMethod
    {
        #region Methods
        /// <summary>
        /// Retrieves the encoded version of the passed in data.
        /// </summary>
        /// <param name="data">Data to be encoded.</param>
        /// <returns>Encoded data.</returns>
        public byte[] Encode(byte[] data)
        {
            return data;
        }

        /// <summary>
        /// Retrieves the decoded version of the passed in data.
        /// </summary>
        /// <param name="data">Data to be decoded.</param>
        /// <param name="decodedDataLength">Length of the decoded data.</param>
        /// <returns>Deocded data.</returns>
        public byte[] Decode(byte[] data, int decodedDataLength)
        {
            return data;
        }
        #endregion // Methods
    } // NoCompressionMethod
}
