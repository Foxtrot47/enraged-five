﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Compression
{
    /// <summary>
    /// Class for compressing and decompressing with the fiCompress and fiDecompress methods.
    /// Mirrored in C++ in rage/base/samples/sample_ragcompression.
    /// </summary>
    internal class FiCompressionMethod : ICompressionMethod
    {
        #region Constants
        /// <summary>
        /// Initial width currently has to be two so that initial bias is correct when invoked from encode_length.
        /// </summary>
        private const int INITIAL_WIDTH = 2;

        /// <summary>
        /// 
        /// </summary>
        private const int WIDTH_STEP = 2;

        /// <summary>
        /// Magic number (to allow for future configurability).
        /// </summary>
        private const byte MAGIC_1 = 0xDC;

        /// <summary>
        /// Magic number (to allow for future configurability).
        /// </summary>
        private const byte MAGIC_2 = 0xE0;
        #endregion

        #region Member Data
        /// <summary>
        /// Maximum lookback (log2).  15 or 16 is pretty fast and still yields very good results.
        /// </summary>
        private readonly uint _maxLookback;

        /// <summary>
        /// If true, does more exhaustive table update which is substantially more expensive but 
        /// improves compression by about 10% or so.  Varies by data.
        /// </summary>
        private readonly bool _fullUpdate;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="FiCompressionMethod"/> class.
        /// </summary>
        /// <param name="maxLookback">Maximum lookback to use (log2).</param>
        /// <param name="fullUpdate">Whether to do full table updates.</param>
        internal FiCompressionMethod(uint maxLookback, bool fullUpdate)
        {
            _maxLookback = maxLookback;
            _fullUpdate = fullUpdate;
        }
        #endregion // Constructor(s)

        #region Methods
        /// <summary>
        /// Retrieves the encoded version of the passed in data.
        /// </summary>
        /// <param name="data">Data to be encoded.</param>
        /// <returns>Encoded data.</returns>
        public byte[] Encode(byte[] data)
        {
            Debug.Assert(data != null || data.Length == 0, "No data provided to encode.");
            if (data == null || data.Length == 0)
            {
                throw new CompressionException("No data provided to encode.");
            }

            // We need at least 4 bytes of data to perform any compression.
            if (data.Length < 4)
            {
                return data;
            }

            IList<byte> encodedData = new List<byte>(data.Length);

            UInt32 maxLookback = (UInt32)(1 << (Int32)_maxLookback);

            const int lookup_size = 65536;

            Node[] lookup = new Node[lookup_size];
            UInt32 poolSize = (uint)data.Length + 1024;
            Node[] pool = new Node[poolSize];
            UInt32 poolCount = 0;

            UInt32 literalCount = 0, windowCount = 0;
            UInt32 literalRun = 0;
            UInt64 estimate = 0;

            EncoderState state = new EncoderState();
            state.m_Accum = 1;
            state.m_Offset = 2;
            state.m_NextAccum = 2;
            state.m_Output = encodedData;

            encodedData[0] = MAGIC_1;
            encodedData[1] = MAGIC_2;

            UInt32 offset = 0;
            while (offset < data.Length - 2)
            {
                int key = data[offset] | (data[offset + 1] << 8);
                int piKey = key;
                bool piIsNext = false;
                Node pi = lookup[piKey];
                Node i = lookup[piKey];
                UInt32 bestRun = 0;
                UInt32 bestOffset = 0;
                while (i != null)
                {
                    // Runs are stored with closest ones first in the list, so earlier runs will always be cheaper.
                    // For short runs, do a sanity check to make sure that storing it as a literal run wouldn't be cheaper.
                    UInt32 thisRun = LeadingMatch(data, i.Offset, data, offset, (uint)data.Length - offset);
                    if (thisRun > bestRun &&
                        ((thisRun == 2 && 2 + CostForOffset(offset - i.Offset) < (2 + 2 * 8) /* size of a 2-literal run */)
                        || (thisRun == 3 && 4 + CostForOffset(offset - i.Offset) < (4 + 3 * 8) /* size of a 3-literal run */)
                        || (thisRun == 4 && 4 + CostForOffset(offset - i.Offset) < (4 + 4 * 8) /* size of a 4-literal run */)
                        || (thisRun > 4)
                        ))
                    {
                        bestRun = thisRun;
                        bestOffset = i.Offset;
                    }
                    else if (offset - i.Offset > maxLookback)
                    {
                        // Delete nodes that are too far back.
                        if (piKey != -1)
                        {
                            pi = lookup[piKey] = i;//pi = i.next;    //*pi = i->next;
                            piKey = -1;
                            piIsNext = true;
                        }
                        else if (piIsNext)
                        {
                            pi.Next = i.Next;
                        }
                        else
                        {
                            pi = i.Next;
                        }

                        i = i.Next; //i = i->next;
                        continue;
                    }

                    pi = i; //pi = i.next;    //pi = &i->next;
                    piKey = -1;
                    piIsNext = true;
                    i = i.Next; //i = pi; //i = *pi;
                }
                Debug.Assert(poolCount < poolSize);
                Node nn = new Node();
                pool[poolCount++] = nn;
                nn.Offset = offset;
                nn.Next = lookup[key];
                lookup[key] = nn;

                if (bestRun > 1)
                {
                    FlushRun(ref literalRun, ref state, estimate, data, offset);

                    estimate += EncodeLength(ref state, bestRun - 1);
                    PutBits(ref state, 1, 1);
                    estimate += 1 + EncodeOffset(ref state, offset - bestOffset);

                    ++windowCount;

                    // Add "closer" copy of the windowed data to the work queue.
                    // (the initial two bytes of the window run were already added above)
                    while (--bestRun != 0)
                    {
                        ++offset;
                        // This produces slightly better results but is never practical except for lookbacks 64k or less
                        if (_fullUpdate)
                        {
                            int key1 = data[offset] | (data[offset + 1] << 8);
                            Debug.Assert(poolCount < poolSize);
                            Node nn1 = new Node();
                            pool[poolCount++] = nn1;
                            nn1.Offset = offset;
                            nn1.Next = lookup[key1];
                            lookup[key1] = nn1;
                        }
                    }
                    ++offset;
                }
                else
                {
                    ++literalRun;
                    ++literalCount;
                    ++offset;
                }
            }

            literalRun += ((uint)data.Length - offset);
            offset = (uint)data.Length;
            FlushRun(ref literalRun, ref state, estimate, data, offset);

            // Flush the last control byte if necessary.
            if (state.m_Accum > 1)
            {
                PutBits(ref state, 0, 8);
            }

            // clean up allocations
            pool = null;
            lookup = null;

            return encodedData.ToArray();
        }


        /// <summary>
        /// Retrieves the decoded version of the passed in data.
        /// </summary>
        /// <param name="data">Data to be decoded.</param>
        /// <param name="decodedDataLength">Length of the decoded data.</param>
        /// <returns>Deocded data.</returns>
        public byte[] Decode(byte[] data, int decodedDataLength)
        {
            Debug.Assert(data == null || data.Length == 0, "No data provided to decode.");
            if (data == null || data.Length == 0)
            {
                throw new CompressionException("No data provided to decode.");
            }

            Int32 accum = 0x10000;
            UInt32 offset = 0;

            UInt32 indexSrc = 0;
            if (data[0] != MAGIC_1 || data[1] != MAGIC_2)
            {
                throw new CompressionException("Magic number mismatch.");
            }
            indexSrc += 2;

            byte[] decodedData = new byte[decodedDataLength];

            while (offset < decodedDataLength)
            {
                // Decode length (either literal count or window length minus one)
                UInt32 length = 0;
                if (GetBits(ref accum, data, ref indexSrc, 1) != 0)
                {
                    length = 1;
                }
                else if (GetBits(ref accum, data, ref indexSrc, 1) != 0)
                {
                    // 4 or 5-20
                    if (GetBits(ref accum, data, ref indexSrc, 1) != 0)
                    {
                        // 5-20+
                        UInt32 width = INITIAL_WIDTH + WIDTH_STEP;
                        UInt32 bias = 1 + (1 << INITIAL_WIDTH);
                        while (GetBits(ref accum, data, ref indexSrc, 1) != 0)
                        {
                            bias += (UInt32)(1 << (Int32)width);
                            width += WIDTH_STEP;
                        }
                        length = bias + GetBits(ref accum, data, ref indexSrc, (Int32)width);
                    }
                    else
                    {
                        length = 4;
                    }
                }
                else if (GetBits(ref accum, data, ref indexSrc, 1) != 0)
                {
                    length = 3;
                }
                else
                {
                    length = 2;
                }

                // Frame type:
                if (GetBits(ref accum, data, ref indexSrc, 1) != 0)
                {
                    // window
                    ++length;			// min window length is two
                    UInt32 width = INITIAL_WIDTH;
                    UInt32 bias = 1;
                    while (GetBits(ref accum, data, ref indexSrc, 1) != 0)
                    {
                        bias += (UInt32)(1 << (Int32)width);
                        width += WIDTH_STEP;
                    }
                    UInt32 backup = bias + GetBits(ref accum, data, ref indexSrc, (Int32)width);
                    if (backup > offset)
                    {
                        throw new CompressionException("Unexpected end to data.");
                    }

                    do
                    {
                        if (offset >= decodedDataLength)
                        {
                            throw new CompressionException("Uncompressed too much data.");
                        }

                        decodedData[(int)offset] = decodedData[(int)(offset - backup)];
                        ++offset;
                    } while (--length != 0);
                }
                else
                {
                    // literals
                    do
                    {
                        if (offset >= decodedDataLength)
                        {
                            throw new CompressionException("Uncompressed too much data.");
                        }

                        decodedData[(int)offset] = data[indexSrc];
                        ++offset;
                        ++indexSrc;
                    } while (--length != 0);
                }
            }

            Debug.Assert(offset == decodedDataLength, "Didn't decompress the expected amount of data.");
            return decodedData.ToArray();
        }
        #endregion // Methods

        #region Helper Structures
        /// <summary>
        /// 
        /// </summary>
        private class Node
        {
            public UInt32 Offset;		// offset of the two leader bytes
            public Node Next;		 // pointer to next node
        }

        /// <summary>
        /// 
        /// </summary>
        private class EncoderState
        {
            public uint m_Accum;
            public uint m_NextAccum;
            public uint m_Offset;
            public IList<byte> m_Output;
        }
        #endregion // Helper Structures

        #region Helper Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="literalRun"></param>
        /// <param name="state"></param>
        /// <param name="estimate"></param>
        /// <param name="src"></param>
        /// <param name="offset"></param>
        private void FlushRun(ref UInt32 literalRun, ref EncoderState state, UInt64 estimate, byte[] src, UInt32 offset)
        {
            if (literalRun != 0)
            {
                estimate += 1 + EncodeLength(ref state, literalRun);
                PutBits(ref state, 0, 1);
                for (UInt32 i = 0; i < literalRun; i++)
                {
                    estimate += 8;
                    state.m_Output[(int)(state.m_Offset++)] = src[offset - literalRun + i];
                }

                literalRun = 0;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="indexA"></param>
        /// <param name="b"></param>
        /// <param name="indexB"></param>
        /// <param name="maxLen"></param>
        /// <returns></returns>
        private UInt32 LeadingMatch(byte[] a, UInt32 indexA, byte[] b, UInt32 indexB, UInt32 maxLen)
        {
            UInt32 count = 0;
            while ((count < maxLen) && (a[indexA] == b[indexB]))
            {
                ++count;
                ++indexA;
                ++indexB;
            }
            return count;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="state"></param>
        /// <param name="value"></param>
        /// <param name="width"></param>
        private void PutBits(ref EncoderState state, UInt32 value, UInt32 width)
        {
            Debug.Assert(width < 32);
            Debug.Assert(value < (UInt32)(1 << (Int32)width));

            while (width != 0)
            {
                if (state.m_Accum == 1)
                {
                    state.m_NextAccum = state.m_Offset++;
                }
                state.m_Accum = (state.m_Accum << 1) | ((UInt32)((Int32)value >> (Int32)(--width)) & 1);
                if ((state.m_Accum & 0x100) != 0)
                {
                    state.m_Output[(int)state.m_NextAccum] = (byte)state.m_Accum;
                    state.m_Accum = 1;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="state"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        private UInt32 EncodeOffset(ref EncoderState state, UInt32 len)
        {
            // 0xx = 1,2,3,4 (when c_InitialWidth=2)
            // 10xxxxxx = 5,6,7...
            // 110xxxxxxxxxx
            // Every leading one adds c_WidthStep more bits (and builds on the previous range)
            Debug.Assert(len != 0);
            UInt32 leader = 1;
            UInt32 width = INITIAL_WIDTH;
            UInt32 mini = 1;
            UInt32 maxi = 1 << INITIAL_WIDTH;
            while (len > maxi)
            {
                PutBits(ref state, 1, 1);
                mini = maxi + 1;
                width += WIDTH_STEP;
                maxi += (UInt32)(1 << (Int32)width);
                ++leader;
            }
            PutBits(ref state, 0, 1);
            Debug.Assert(len >= mini && len <= maxi);
            PutBits(ref state, len - mini, width);
            return leader + width;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="len"></param>
        /// <returns></returns>
        private UInt32 CostForOffset(UInt32 len)
        {
            UInt32 leader = 1;
            UInt32 width = INITIAL_WIDTH;
            UInt32 mini = 1;
            UInt32 maxi = 1 << INITIAL_WIDTH;
            while (len > maxi)
            {
                mini = maxi + 1;
                width += WIDTH_STEP;
                maxi += (UInt32)(1 << (Int32)width);
                ++leader;
            }
            return leader + width;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="state"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        private UInt32 EncodeLength(ref EncoderState state, UInt32 len)
        {
            // 1   -> 1
            // 000 -> 2
            // 001 -> 3
            // 010 -> 4
            // 011xxx -> as per encode_offset
            if (len == 1)
            {
                PutBits(ref state, 1, 1);
                return 1;
            }
            else if (len == 2 || len == 3 || len == 4)
            {
                PutBits(ref state, len - 2, 3);
                return 3;
            }
            else
            {
                // fall through to common encoding (albeit with longer prefix)
                PutBits(ref state, 1, 2);
                return 2 + EncodeOffset(ref state, len);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="accum"></param>
        /// <param name="src"></param>
        /// <param name="indexSrc"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        private UInt32 GetBits(ref Int32 accum, byte[] src, ref UInt32 indexSrc, Int32 count)
        {
            UInt32 result = 0;
            Debug.Assert(count != 0);
            do
            {
                if ((accum & 0x10000) != 0)
                {
                    // FIXME: indexSrc might need to be ref'd back to calling function
                    accum = 0x100 | src[indexSrc];
                    ++indexSrc;
                }

                result = (result << 1) | (UInt32)((accum & 0x80) >> 7);
                accum <<= 1;
            } while (--count != 0);

            return result;
        }
        #endregion // Helper Methods
    } // FiCompressionMethod
}
