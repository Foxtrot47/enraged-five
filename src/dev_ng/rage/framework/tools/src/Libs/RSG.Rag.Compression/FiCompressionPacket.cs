﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Compression
{
    /// <summary>
    /// Packet that contains Fi compressed data.
    /// </summary>
    public class FiCompressionPacket : CompressionPacket
    {
        #region Member Data
        /// <summary>
        /// Private field for the <see cref="CompressedDataLength"/> property.
        /// </summary>
        private int _compressedDataLength;

        /// <summary>
        /// Private field for the <see cref="DecompressedDataLength"/> property.
        /// </summary>
        private int _decompressedDataLength;

        /// <summary>
        /// Private field for the <see cref="CompressionMethod"/> property.
        /// </summary>
        private readonly ICompressionMethod _compressionMethod = new FiCompressionMethod(16, false);
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="FiCompressionPacket"/> class.
        /// </summary>
        public FiCompressionPacket()
            : base()
        {
        }
        
        /// <summary>
        /// Creates a new instance of the <see cref="FiCompressionPacket"/> class based off an
        /// uncompressed data buffer.
        /// </summary>
        /// <param name="buffer"></param>
        public FiCompressionPacket(byte[] buffer)
            : base()
        {
            CompressedData = CompressionMethod.Encode(buffer);
            _compressedDataLength = CompressedData.Length;
            _decompressedDataLength = buffer.Length;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Static header prefix.
        /// </summary>
        internal static string StaticHeaderPrefix
        {
            get { return "CHF"; }
        }

        /// <summary>
        /// Prefix that the data will start with for data compressed using this method.
        /// </summary>
        internal override string HeaderPrefix
        {
            get { return StaticHeaderPrefix; }
        }

        /// <summary>
        /// Length in bytes of this packets header.
        /// header: CHD:0000:0000: where the 0000 is a u32 and everything else is a character.
        /// </summary>
        internal override int HeaderLength
        {
            get { return 14; }
        }

        /// <summary>
        /// How long the compressed data should be based on the packet's header.
        /// </summary>
        internal override int CompressedDataLength
        {
            get { return _compressedDataLength; }
        }

        /// <summary>
        /// Length in bytes that the decompressed data will be.
        /// </summary>
        internal override int DecompressedDataLength
        {
            get { return _decompressedDataLength; }
        }

        /// <summary>
        /// The method to use for decompressing the data contained within this packet.
        /// </summary>
        internal override ICompressionMethod CompressionMethod
        {
            get { return _compressionMethod; }
        }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Parses the header from the supplied buffer.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        internal override bool ParseHeader(byte[] buffer, int offset)
        {
            // Make sure there is enough data.
            if (buffer.Length < offset + HeaderLength)
            {
                return false;
            }

            // Start parsing the header.
            for (int i = 0; i < HeaderPrefix.Length; ++i)
            {
                if (buffer[offset + i] != (byte)HeaderPrefix[i])
                {
                    throw new DataException("Bad header data.");
                }
            }

            int index = offset + HeaderPrefix.Length;
            if (buffer[index++] != (byte)':')
            {
                throw new DataException("Bad header data.");
            }

            UInt32 cSize = (UInt32)(buffer[index++]);
            cSize |= (UInt32)(buffer[index++] << 8);
            cSize |= (UInt32)(buffer[index++] << 16);
            cSize |= (UInt32)(buffer[index++] << 24);

            if (buffer[index++] != (byte)':')
            {
                throw new DataException("Bad header data.");
            }

            UInt32 dSize = (UInt32)(buffer[index++]);
            dSize |= (UInt32)(buffer[index++] << 8);
            dSize |= (UInt32)(buffer[index++] << 16);
            dSize |= (UInt32)(buffer[index++] << 24);

            if (buffer[index++] != (byte)':')
            {
                throw new DataException("Bad header data.");
            }

            _compressedDataLength = (int)cSize;
            _decompressedDataLength = (int)dSize;
            return true;
        }

        /// <summary>
        /// Writes the header to the start of the specified buffer.
        /// </summary>
        /// <param name="buffer"></param>
        protected override void WriteHeader(ref byte[] buffer)
        {
            for (int i = 0; i < HeaderPrefix.Length; ++i)
            {
                buffer[i] = (byte)HeaderPrefix[i];
            }

            int index = HeaderPrefix.Length;
            uint compressedSize = (uint)CompressedDataLength;
            buffer[index++] = (byte)(compressedSize);
            buffer[index++] = (byte)(compressedSize >> 8);
            buffer[index++] = (byte)(compressedSize >> 16);
            buffer[index++] = (byte)(compressedSize >> 24);
            buffer[index++] = (byte)':';

            UInt32 decompressedSize = (uint)_decompressedDataLength;
            buffer[index++] = (byte)(decompressedSize);
            buffer[index++] = (byte)(decompressedSize >> 8);
            buffer[index++] = (byte)(decompressedSize >> 16);
            buffer[index++] = (byte)(decompressedSize >> 24);
            buffer[index++] = (byte)':';
        }
        #endregion // Methods
    } // FiCompressionPacket
}
