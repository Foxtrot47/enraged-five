﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Rag.Compression
{
    /// <summary>
    /// Class for compressing and decompressing with the datCompress and datDecompress methods.
    /// Mirrored in C++ in rage/base/src/data.
    /// </summary>
    internal class DatCompressionMethod : ICompressionMethod
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private const Int32 K_WIDTH = 4;

        /// <summary>
        /// Minimum number of bytes in a window.
        /// </summary>
        private const Int32 MIN_K = 3;

        /// <summary>
        /// Determines max number of bytes in a window.
        /// </summary>
        private const Int32 MAX_K = (MIN_K + (1 << K_WIDTH) - 1);

        /// <summary>
        /// Max window distance backward.
        /// </summary>
        private const int COMP_SLIDE_SIZE = 1 << (16 - K_WIDTH);

        /// <summary>
        /// Header size.
        /// </summary>
        private const Int32 HEADER_SIZE = 2;
        #endregion

        #region Methods
        /// <summary>
        /// Retrieves the encoded version of the passed in data.
        /// </summary>
        /// <param name="data">Data to be encoded.</param>
        /// <returns>Encoded data.</returns>
        public byte[] Encode(byte[] data)
        {
            Debug.Assert(data != null || data.Length == 0, "No data provided to encode.");
            if (data == null || data.Length == 0)
            {
                throw new CompressionException("No data provided to encode.");
            }

            IList<byte> encodedData = new List<byte>(data.Length + HEADER_SIZE);

            // Set all Oldest ptrs to -1
            QueueEntry[] queue = new QueueEntry[256];
            for (int x = 0; x < queue.Length; ++x)
            {
                queue[x] = new QueueEntry();
                queue[x].Newest = queue[x].Oldest = -1;
            }

            // Initialize heap
            HeapEntry[] heap = new HeapEntry[COMP_SLIDE_SIZE];
            for (int x = 0; x < heap.Length; ++x)
            {
                heap[x] = new HeapEntry();
                heap[x].Follow = 0;
                heap[x].Next = 0;
                heap[x].RestIndex = 0;
            }

            int freePtr = 0;
            int outputIndex = HEADER_SIZE;
            int control = 1;
            int controlIndex = outputIndex++;

            uint here = data[0];
            uint follow = (uint)(data[1] | (data[2] << 8));

            uint i = 0;
            while (i < data.Length)
            {
                uint best_k = 1;
                uint best_j = 0;
                int search = queue[here].Oldest;

                if (i < data.Length - 2)
                {
                    while (search != -1)
                    {
                        // definitely compressible!
                        if (follow == heap[search].Follow)
                        {
                            uint k = (uint)MIN_K;
                            while ((i + k < data.Length) && (k < MAX_K)
                                && (data[i + k] == data[heap[search].RestIndex + k]))
                            {
                                k++;
                            }

                            // If this is as good, but closer, use it
                            // instead, hoping that cache is fresher.
                            if (k > best_k)
                            {
                                best_k = k;
                                best_j = heap[search].RestIndex;
                            }
                        }

                        search = heap[search].Next;
                    }
                }

                // make room for incoming control bit
                control <<= 1;

                if (best_k != 1)
                {
                    uint offset = i - best_j - 1;
                    uint count = (uint)(best_k - MIN_K);
                    encodedData[outputIndex++] = (byte)(count | (offset << K_WIDTH));
                    encodedData[outputIndex++] = (byte)(offset >> (8 - K_WIDTH));
                }
                else
                {
                    control |= 1;
                    encodedData[outputIndex++] = (byte)here;
                }

                if ((control & 256) != 0)
                {
                    // it's full, so dump it
                    encodedData[controlIndex] = (byte)control;
                    controlIndex = outputIndex++;
                    control = 1;
                }

                // update heap for however many we compressed.
                do
                {
                    if (i >= COMP_SLIDE_SIZE)
                    {
                        int oldest = queue[data[i - COMP_SLIDE_SIZE]].Oldest;
                        Debug.Assert(oldest == (int)freePtr);
                        queue[data[i - COMP_SLIDE_SIZE]].Oldest = heap[oldest].Next;
                    }

                    if (queue[here].Oldest == -1)
                    {
                        // Nothing in its queue, no chance of compression.
                        queue[here].Oldest = queue[here].Newest = freePtr;
                    }
                    else
                    {
                        // Link old Newest to this new entry.
                        heap[queue[here].Newest].Next = (Int16)freePtr;
                        // Update Newest to here.
                        queue[here].Newest = freePtr;
                    }
                    heap[freePtr].Follow = (UInt16)follow;
                    heap[freePtr].Next = -1;
                    heap[freePtr].RestIndex = i;
                    freePtr = (freePtr + 1) & (COMP_SLIDE_SIZE - 1);
                    i++;

                    here = follow & 255;
                    follow >>= 8;

                    if (i + 2 < data.Length)
                    {
                        follow |= (uint)(data[i + 2] << 8);
                    }
                }
                while ((--best_k) != 0);
            }

            // Make sure last control byte is flushed.
            while ((control & 256) == 0)
            {
                control <<= 1;
            }

            encodedData[controlIndex] = (byte)control;

            int length = outputIndex - HEADER_SIZE;

            // Encode length at start of compressed stream.
            Debug.Assert(length <= 65535);
            encodedData[0] = (byte)length;
            encodedData[1] = (byte)(length >> 8);

            return encodedData.ToArray();
        }

        /// <summary>
        /// Retrieves the decoded version of the passed in data.
        /// </summary>
        /// <param name="data">Data to be decoded.</param>
        /// <param name="decodedDataLength">Length of the decoded data.</param>
        /// <returns>Deocded data.</returns>
        public byte[] Decode(byte[] data, int decodedDataLength)
        {
            Debug.Assert(data == null || data.Length == 0, "No data provided to decode.");
            if (data == null || data.Length == 0)
            {
                throw new CompressionException("No data provided to decode.");
            }

            byte[] decodedData = new byte[decodedDataLength];

            int srcIndex = 0, destIndex = 0;
            int length = data[srcIndex++];
            length += (data[srcIndex++] << 8);

            do
            {
                byte control = data[srcIndex++];
                --length;
                for (int i = 0; length != 0 && i < 8; i++, control <<= 1)
                {
                    if ((control & 0x80) != 0)
                    {
                        decodedData[destIndex++] = data[srcIndex++];
                        --length;
                    }
                    else
                    {
                        byte count = data[srcIndex];
                        int offset = (data[srcIndex + 1] << 4) | (count >> 4);
                        int ptrIndex = destIndex - offset - 1;

                        srcIndex += 2;
                        length -= 2;
                        count = (byte)((count & 15) + MIN_K);
                        do
                        {
                            decodedData[destIndex++] = decodedData[ptrIndex++];
                        }
                        while ((--count) != 0);
                    }
                }
            } while (length != 0);

            return decodedData.ToArray();
        }
        #endregion // Methods

        #region Helper Structures
        /// <summary>
        /// 
        /// </summary>
        private struct HeapEntry
        {
            public UInt16 Follow;
            public Int16 Next;
            public UInt32 RestIndex;
        }

        /// <summary>
        /// 
        /// </summary>
        private struct QueueEntry
        {
            public int Oldest;
            public int Newest;
        }
        #endregion
    } // DatCompressionMethod
}
