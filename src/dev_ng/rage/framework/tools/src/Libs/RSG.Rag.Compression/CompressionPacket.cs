﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace RSG.Rag.Compression
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class CompressionPacket
    {
        #region Member Data
        /// <summary>
        /// Compressed data that this packet contains.
        /// </summary>
        private byte[] _compressedData;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="CompressionPacket"/> class.
        /// </summary>
        protected CompressionPacket()
        {
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Prefix that the data will start with for data compressed using this method.
        /// </summary>
        internal abstract String HeaderPrefix { get; }

        /// <summary>
        /// Length in bytes of this packets header.
        /// </summary>
        internal abstract int HeaderLength { get; }

        /// <summary>
        /// Compressed version of data.
        /// </summary>
        protected byte[] CompressedData
        {
            get { return _compressedData; }
            set { _compressedData = value; }
        }

        /// <summary>
        /// How long the compressed data should be based on the packet's header.
        /// </summary>
        internal abstract int CompressedDataLength { get; }

        /// <summary>
        /// Length in bytes that the decompressed data will be.
        /// </summary>
        internal abstract int DecompressedDataLength { get; }

        /// <summary>
        /// The method to use for decompressing the data contained within this packet.
        /// </summary>
        internal abstract ICompressionMethod CompressionMethod { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Parses the header from the supplied buffer.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <returns>Whether the buffer contained enough data.</returns>
        internal abstract bool ParseHeader(byte[] buffer, int offset);

        /// <summary>
        /// Parses the compressed data from the supplied buffer.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <returns>Whether the buffer contained enough data.</returns>
        internal bool ParseData(byte[] buffer, int offset)
        {
            if (buffer.Length >= offset + CompressedDataLength)
            {
                _compressedData = new byte[CompressedDataLength];
                Array.ConstrainedCopy(buffer, offset, _compressedData, 0, CompressedDataLength);
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Retrieves the decompressed version of the data.
        /// </summary>
        /// <returns></returns>
        public byte[] DecompressData()
        {
            return CompressionMethod.Decode(CompressedData, DecompressedDataLength);
        }

        /// <summary>
        /// Converts the compression packet to a raw byte stream that can be sent across the network.
        /// </summary>
        /// <returns></returns>
        public byte[] ToBuffer()
        {
            byte[] buffer = new byte[HeaderLength + CompressedDataLength];
            WriteHeader(ref buffer);
            WriteData(ref buffer, HeaderLength);
            return buffer;
        }

        /// <summary>
        /// Writes the header to the start of the specified buffer.
        /// </summary>
        /// <param name="buffer"></param>
        protected abstract void WriteHeader(ref byte[] buffer);

        /// <summary>
        /// Writes the compressed data to the buffer starting at the specified offset.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        private void WriteData(ref byte[] buffer, int offset)
        {
            Array.ConstrainedCopy(CompressedData, 0, buffer, offset, CompressedDataLength);
        }
        #endregion // Methods
    } // CompressionPacket
}
