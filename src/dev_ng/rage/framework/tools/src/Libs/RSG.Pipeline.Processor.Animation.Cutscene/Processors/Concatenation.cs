﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Content;
using RSG.Pipeline.Services;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.Pipeline.Processor.Animation.Common;

namespace RSG.Pipeline.Processor.Animation.Cutscene
{

    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    public class Concatenation :
        BaseAnimationProcessor
    {
        #region Constants
        /// <summary>
        /// Processor description string.
        /// </summary>
        private static readonly String DESCRIPTION = "Concatenation Processor";
        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly String LOG_CTX = "Cutscene:Concatenation";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public Concatenation()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="syncDependencies"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            List<IProcess> processes = new List<IProcess>();

            process.State = ProcessState.Prebuilt;
            processes.Add(process);
            resultantProcesses = processes;
            syncDependencies = new List<IContentNode>();

            return (true);
        }
        #endregion // IProcessor Interface Methods

        #region IProcessorXGE Interface Methods
        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            // Assign method output.
            List<XGE.ITool> prepTools = new List<XGE.ITool>();
            List<XGE.ITask> prepTasks = new List<XGE.ITask>();

            // Run the processer exe that calls the rage exe

            XGE.ITool tool = XGEFactory.GetAnimationProcessorTool(param.Log, param.Branch.Project.Config, XGEFactory.AnimationToolType.CutsceneConcatenation, "Cutscene Concatenation");
            prepTools.Add(tool);

            foreach (IProcess process in processes)
            {
                XGEUtil.CreateCacheOutputDirectory(process);

                XGE.ITask task = XGEUtil.GetCutsceneConcatenationTask(param.Log, param.Branch.Environment, process, tool);
                prepTasks.Add(task);
            }

            tools = prepTools;
            tasks = prepTasks;

            return (true);
        }

        /// <summary>
        /// Parse log information;
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="taskLogData">Log string data.</param>
        /// <returns>true for successful process; false for error/failure.</returns>
        public override bool ParseLog(IEngineParameters param, IEnumerable<String> taskLogData)
        {
            bool hasErrors = false;
            XGEUtil.ParseCutsceneProcessorToolOutput(param.Log, taskLogData, LOG_CTX, out hasErrors);
            return (!hasErrors);
        }
        #endregion // IPRocessorXGE Interface Methods
    }

} // RSG.Pipeline.Processor.Animation.Cutscene namespace
