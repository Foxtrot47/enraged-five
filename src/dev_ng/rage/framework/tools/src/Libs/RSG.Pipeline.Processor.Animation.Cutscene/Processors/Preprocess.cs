﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using SIO = System.IO;
using System.Linq;
using System.Diagnostics;
using RSG.Base.Extensions;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Content;
using RSG.Pipeline.Processor.Animation.Common;
using RSG.Pipeline.Services.Animation;
using RSG.Pipeline.Services;
using RSG.Platform;
using XGE = RSG.Interop.Incredibuild.XGE;
using System.Xml;
using System.Text;
using RSG.Base.Configuration;
using RSG.Pipeline.Services.AssetPack;
using RSG.Pipeline.Services.Platform;
using Ionic.Zip;
using System.Xml.Linq;

namespace RSG.Pipeline.Processor.Animation.Cutscene
{

    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    public class PreProcess :
        ProcessorBase,
        IProcessor
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        private static readonly String DESCRIPTION = "Cutscene Animation Pre Processor";
        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly String LOG_CTX = "Cutscene:PreProcess";
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public PreProcess()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)

        #region IProcessor Interface Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies,
            out IEnumerable<IProcess> resultantProcesses)
        {
            List<IProcess> processes = new List<IProcess>();
            List<IContentNode> syncDepends = new List<IContentNode>();

            bool result = true;

            // Sync expressions
            String assetExpressions = SIO.Path.Combine(param.Branch.Assets, "anim", "expressions");
            IContentNode assetExpressionsContent = owner.CreateDirectory(assetExpressions);

            // Sync the concat cutlist folder
            String cutListFolder = SIO.Path.Combine(param.Branch.Assets, "cuts", "!!Cutlists");
            IContentNode cutListContent = owner.CreateDirectory(cutListFolder);
            String mergeListFolder = SIO.Path.Combine(param.Branch.Assets, "cuts", "!!Mergelists");
            IContentNode mergeListContent = owner.CreateDirectory(mergeListFolder);
            String cutSubFolder = SIO.Path.Combine(param.Branch.Assets, "cuts", "!!cutsubs");
            IContentNode cutSubContent = owner.CreateDirectory(cutSubFolder);
            String cutGameFolder = SIO.Path.Combine(param.Branch.Assets, "cuts", "!!game_data");
            IContentNode cutGameContent = owner.CreateDirectory(cutGameFolder);
            String cutReplayFolder = SIO.Path.Combine(param.Branch.Assets, "metadata", "animation", "replayedits");
            IContentNode cutReplayContent = owner.CreateDirectory(cutReplayFolder);
            String cutMissionOverload = SIO.Path.Combine(param.Branch.Assets, "metadata", "animation", "cutscenes", "config", "missionoverload.xml");
            IContentNode cutMissionOverloadContent = owner.CreateFile(cutMissionOverload);
            String cutMissionExclude = SIO.Path.Combine(param.Branch.Assets, "metadata", "animation", "cutscenes", "config", "excludedscenes.txt");
            IContentNode cutMissionExcludeContent = owner.CreateFile(cutMissionExclude);
            String pscFolder = SIO.Path.Combine(param.Branch.Assets, "metadata", "definitions");
            IContentNode pscContent = owner.CreateDirectory(pscFolder);
            String decalDefinitions = SIO.Path.Combine(param.Branch.Build, "common", "data", "effects", "decals_cs.dat");
            IContentNode decalDefinitionsContent = owner.CreateFile(decalDefinitions);
            String propSceneXmlFolder = SIO.Path.Combine(param.Branch.Export, "levels", "gta5", "props");
            IContentNode propSceneXmlContent = owner.CreateDirectory(propSceneXmlFolder);

            if (process.State == ProcessState.Initialised)
            {
                syncDepends.Add(pscContent);
                syncDepends.Add(cutListContent);
                syncDepends.Add(mergeListContent);
                syncDepends.Add(cutSubContent);
                syncDepends.Add(cutGameContent);
                syncDepends.Add(cutReplayContent);
                syncDepends.Add(cutMissionOverloadContent);
                syncDepends.Add(cutMissionExcludeContent);
                syncDepends.Add(decalDefinitionsContent);
                syncDepends.Add(assetExpressionsContent);
                syncDepends.Add(propSceneXmlContent);
                syncDependencies = syncDepends;
                resultantProcesses = processes;
                process.State = ProcessState.Prebuilding;
                return result;
            }

            String cache = PreProcess.GetCacheDir(param);

            ConcatListManager.Instance.InitWith(param, cutListFolder, mergeListFolder, cutMissionOverload, cutMissionExclude, param.Log);

            // Collect all the independent dictionaries and build anim dictionary entries for merging with out clip dictionary defs
            IEnumerable<IContentNode> inputContentMap;
            if (!GenerateInputContentMap(param, owner, process.Inputs, out inputContentMap))
            {
                param.Log.ErrorCtx(LOG_CTX, "Error generating input map.");
                syncDependencies = new List<IContentNode>();
                resultantProcesses = processes;
                return (false);
            }

            // Loop through all the dictionaries and create our process/content chain
            List<IContentNode> outputOptimisedContent = new List<IContentNode>();
            List<IContentNode> outputFullFatContent = new List<IContentNode>();
            foreach (IContentNode content in inputContentMap)
            {
                RSG.Pipeline.Content.Directory directoryContent = content as RSG.Pipeline.Content.Directory;
                IEnumerable<IContentNode> dictionaryInputs = directoryContent.EvaluateInputs();

                this.LoadParameters(param);
                object automaticallyBuildConcat;
                if (!this.Parameters.TryGetValue("AutomaticallyBuildConcat", out automaticallyBuildConcat))
                {
                    param.Log.ErrorCtx(LOG_CTX, "Missing Paramater 'AutomaticallyBuildConcat'");
                    syncDependencies = new List<IContentNode>();
                    resultantProcesses = processes;
                    return (false);
                }

                object automaticallyMerge;
                if (!this.Parameters.TryGetValue("AutomaticallyMerge", out automaticallyMerge))
                {
                    param.Log.ErrorCtx(LOG_CTX, "Missing Paramater 'AutomaticallyMerge'");
                    syncDependencies = new List<IContentNode>();
                    resultantProcesses = processes;
                    return (false);
                }

                object useExcludeList;
                if (!this.Parameters.TryGetValue("UseExcludeList", out useExcludeList))
                {
                    param.Log.ErrorCtx(LOG_CTX, "Missing Paramater 'UseExcludeList'");
                    syncDependencies = new List<IContentNode>();
                    resultantProcesses = processes;
                    return (false);
                }

                object mergeLighting = true;
                if (!this.Parameters.TryGetValue("MergeLighting", out mergeLighting))
                {
                    mergeLighting = true;         
                }

                object useClipXml = false;
                if (!this.Parameters.TryGetValue("UseClipXml", out useClipXml))
                {
                    useClipXml = false;
                }

                object rebuildScene = false;
                if (!this.Parameters.TryGetValue("RebuildScene", out rebuildScene))
                {
                    rebuildScene = false;
                }

                Dictionary<string, string> dictPlatformCompressions = ProcessPlatformCompressionSettings(param);

                Dictionary<String, IContentNode> mergeInputContent = new Dictionary<string, IContentNode>();
                if ((bool)automaticallyMerge == true)
                {
                    GenerateMergeProcesses(param, cache, owner, processors, processes, dictionaryInputs, mergeInputContent, SIO.Path.GetFileName(directoryContent.AbsolutePath));
                }

                Dictionary<String, IContentNode> concatInputContent = new Dictionary<string, IContentNode>();
                if ((bool)automaticallyBuildConcat == true)
                {
                    GenerateConcatenationProcesses(param, cache, owner, processors, processes, dictionaryInputs, concatInputContent, mergeInputContent, SIO.Path.GetFileName(directoryContent.AbsolutePath), (bool)useClipXml);
                }

                RSG.Pipeline.Content.StaticDirectory staticDirectoryOptimisedProcessedContent = owner.CreateStaticDirectory((content as Directory).AbsolutePath) as StaticDirectory;
                RSG.Pipeline.Content.StaticDirectory staticDirectoryFullFatProcessedContent = owner.CreateStaticDirectory((content as Directory).AbsolutePath) as StaticDirectory;
                foreach (IContentNode dictionary in dictionaryInputs)
                {
                    String dictionaryName = Filename.GetBasename(SIO.Path.GetFileName((dictionary as RSG.Pipeline.Content.File).AbsolutePath));
                    String missionName = SIO.Path.GetFileName(SIO.Path.GetDirectoryName((dictionary as RSG.Pipeline.Content.File).AbsolutePath));

                    String processedDirectory = SIO.Path.Combine(param.Branch.Processed, "anim", "cutscene", String.Format("cuts_{0}\\{1}\\", directoryContent.Name, dictionaryName));
                    IContentNode directoryProcessedContent = owner.CreateDirectory(processedDirectory, "*.*");

                    String extractionDirectory = SIO.Path.Combine(cache, dictionaryName, "extraction");
                    String rebuildDirectory = SIO.Path.Combine(cache, dictionaryName, "rebuild");
                    String lightMergeDirectory = SIO.Path.Combine(cache, dictionaryName, "lightmerge");
                    String facialMergeDirectory = SIO.Path.Combine(cache, dictionaryName, "facialmerge");
                    String sectionDirectory = SIO.Path.Combine(cache, dictionaryName, "section");
                    String finaliseDirectory = SIO.Path.Combine(cache, dictionaryName, "finalise");
                    String groupingDirectory = SIO.Path.Combine(cache, dictionaryName, "grouping");
                    String compressionDirectory = SIO.Path.Combine(cache, dictionaryName, "compression");
                    String subtitleDirectory = SIO.Path.Combine(cache, dictionaryName, "subtitle");
                    String injectDofDirectory = SIO.Path.Combine(cache, dictionaryName, "injectdof");
                    String injectPrefixDirectory = SIO.Path.Combine(cache, dictionaryName, "injectprefix");
                    String externalMergeDirectory = SIO.Path.Combine(cache, dictionaryName, "externalmerge");
                    String clipDictionaryDirectory = SIO.Path.Combine(cache, dictionaryName, "clipdictionary");
                    String compressionTemplateDirectory = SIO.Path.Combine(param.Branch.Project.Config.ToolsConfig, "config", "anim", "compression_templates");

                    IContentNode extractionDirNode = owner.CreateDirectory(extractionDirectory, "*.*");
                    ProcessBuilder clipDictionary = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Common.ClipDictionaryProcessor", processors, owner);
                    clipDictionary.Inputs.Add((dictionary as RSG.Pipeline.Content.File));
                    clipDictionary.Outputs.Add(extractionDirNode);

                    IContentNode rebuildDirNode = null;
                    ProcessBuilder rebuildData = null;
                    if ((bool)rebuildScene == true && !ConcatListManager.Instance.ConcatNames.Contains(dictionaryName))
                    {
                        // We regen the data here
                        rebuildDirNode = owner.CreateDirectory(rebuildDirectory, "*.*");
                        rebuildData = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Cutscene.SliceDice", processors, owner);
                        rebuildData.Inputs.Add(extractionDirNode);
                        rebuildData.Inputs.Add((dictionary as RSG.Pipeline.Content.File));
                        rebuildData.Outputs.Add(rebuildDirNode);

                        // If we come across the concat then we push the input node in, this means its build after generation
                        if (ConcatListManager.Instance.ConcatNames.Contains(dictionaryName))
                        {
                            if (concatInputContent.ContainsKey(dictionaryName))
                            {
                                rebuildData.Inputs.Add(concatInputContent[dictionaryName]);
                            }
                        }
                    }
                    else
                    {
                        rebuildDirNode = extractionDirNode;
                        
                        // If we come across the concat then we push the input node in, this means its build after generation
                        if (ConcatListManager.Instance.ConcatNames.Contains(dictionaryName))
                        {
                            if (concatInputContent.ContainsKey(dictionaryName))
                            {
                                clipDictionary.Inputs.Add(concatInputContent[dictionaryName]);
                            }
                        }

                        // If we come across the concat then we push the input node in, this means its build after generation
                        if (ConcatListManager.Instance.MergeNames.Contains(dictionaryName))
                        {
                            if (mergeInputContent.ContainsKey(dictionaryName))
                            {
                                clipDictionary.Inputs.Add(mergeInputContent[dictionaryName]);
                            }
                        }
                    }

                    // Processor for merging subtitles 
                    IContentNode externalMergeDirNode = owner.CreateDirectory(externalMergeDirectory, "*.*");
                    ProcessBuilder externalMerge = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Cutscene.MergeExternal", processors, owner);
                    externalMerge.Inputs.Add(rebuildDirNode);
                    externalMerge.Inputs.Add((dictionary as RSG.Pipeline.Content.File));
                    externalMerge.Outputs.Add(externalMergeDirNode);

                    // Processor for merging subtitles 
                    IContentNode subtitleDirNode = owner.CreateDirectory(subtitleDirectory, "*.*");
                    ProcessBuilder subtitle = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Cutscene.MergeSubtitles", processors, owner);
                    subtitle.Inputs.Add(externalMergeDirNode);
                    subtitle.Inputs.Add((dictionary as RSG.Pipeline.Content.File));
                    subtitle.Outputs.Add(subtitleDirNode);

                    // Processor for merging light data
                    IContentNode lightMergeDirNode = owner.CreateDirectory(lightMergeDirectory, "*.*");
                    ProcessBuilder lightMerge = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Cutscene.MergeLights", processors, owner);
                    lightMerge.Inputs.Add(subtitleDirNode);
                    lightMerge.Inputs.Add(rebuildDirNode);
                    lightMerge.Outputs.Add(lightMergeDirNode);

                    // Processor for merging facial data with body
                    IContentNode facialMergeDirNode = owner.CreateDirectory(facialMergeDirectory, "*.*");
                    ProcessBuilder facialMerge = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Cutscene.MergeFacial", processors, owner);
                    facialMerge.Inputs.Add(rebuildDirNode);
                    facialMerge.Outputs.Add(facialMergeDirNode);

                    // Processor for injecting dof into animation
                    IContentNode injectDofDirNode = owner.CreateDirectory(injectDofDirectory, "*.*");
                    ProcessBuilder injectDof = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Cutscene.InjectDof", processors, owner);
                    injectDof.Inputs.Add(facialMergeDirNode);
                    injectDof.Outputs.Add(injectDofDirNode);

                    // Processor for injecting dlc prefix's into animation
                    IContentNode injectPrefixDirNode = owner.CreateDirectory(injectPrefixDirectory, "*.*");
                    ProcessBuilder injectPrefix = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Cutscene.InjectPrefix", processors, owner);
                    injectPrefix.Inputs.Add(injectDofDirNode);
                    if ((bool)mergeLighting == true) injectPrefix.Inputs.Add(lightMergeDirNode); else injectPrefix.Inputs.Add(subtitleDirNode);
                    injectPrefix.Outputs.Add(injectPrefixDirNode);
                    if (param.Branch.Project.IsDLC)
                        injectPrefix.Parameters.Add("DLCName", param.Branch.Project.Name);

                     // Processor for sectioning animations/clips (clip-0, -1) etc
                    IContentNode sectionDirNode = owner.CreateDirectory(sectionDirectory, "*.*");
                    ProcessBuilder section = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Cutscene.Section", processors, owner);
                    section.Inputs.Add(rebuildDirNode);
                    section.Inputs.Add(injectPrefixDirNode);
                    section.Outputs.Add(sectionDirNode);

                    //Processor for finalising animation, creating binary cutfile
                    IContentNode finaliseDirNode = owner.CreateDirectory(finaliseDirectory, "*.*");
                    ProcessBuilder finalise = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Cutscene.Finalise", processors, owner);
                    finalise.Inputs.Add(sectionDirNode);
                    finalise.Inputs.Add((dictionary as RSG.Pipeline.Content.File));
                    finalise.Inputs.Add(rebuildDirNode);
                    finalise.Outputs.Add(finaliseDirNode);

                    IContentNode groupDirNode = owner.CreateDirectory(groupingDirectory, "*.*");
                    ProcessBuilder group = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Common.ClipDictionaryProcessor", processors, owner);
                    group.Inputs.Add(sectionDirNode);
                    group.Outputs.Add(groupDirNode);
                    group.Parameters.Add("Grouping", true);

                    object defaultSkel;
                    if (!this.Parameters.TryGetValue("DefaultSkel", out defaultSkel))
                    {
                        param.Log.ErrorCtx(LOG_CTX, "Missing Paramater 'DefaultSkel'");
                        syncDependencies = new List<IContentNode>();
                        resultantProcesses = processes;
                        return (false);
                    }

                    ICollection<IContentNode> platformProcessedDirNodes = PerformPlatformCompression(dictPlatformCompressions, owner, param, processors, groupDirNode, dictionary, finaliseDirNode, processes, cache, dictionaryName, processedDirectory, defaultSkel);

                    IProcess clipDictionaryProcess = clipDictionary.ToProcess();
                    processes.Add(clipDictionaryProcess);

                    if ((bool)rebuildScene == true && !ConcatListManager.Instance.ConcatNames.Contains(dictionaryName))
                    {
                        IProcess rebuildProcess = rebuildData.ToProcess();
                        processes.Add(rebuildProcess);
                    }

                    IProcess externalMergeProcess = externalMerge.ToProcess();
                    processes.Add(externalMergeProcess);

                    IProcess sectionProcess = section.ToProcess();
                    processes.Add(sectionProcess);

                    IProcess facialMergeProcess = facialMerge.ToProcess();
                    processes.Add(facialMergeProcess);

                    IProcess injectDofProcess = injectDof.ToProcess();
                    processes.Add(injectDofProcess);

                    IProcess injectPrefixProcess = injectPrefix.ToProcess();
                    processes.Add(injectPrefixProcess);

                    if ((bool)mergeLighting == true)
                    {
                        IProcess lightMergeProcess = lightMerge.ToProcess();
                        processes.Add(lightMergeProcess);
                    }

                    IProcess subtitleMergeProcess = subtitle.ToProcess();
                    processes.Add(subtitleMergeProcess);

                    IProcess finaliseProcess = finalise.ToProcess();
                    processes.Add(finaliseProcess);

                    IProcess grouping = group.ToProcess();
                    processes.Add(grouping);

                    staticDirectoryFullFatProcessedContent.StaticFiles.AddRange(platformProcessedDirNodes);

                    // Exclude scenes from the final rpf, this stops processed data being built. Used to filter out part scenes of a concat.
                    if (ConcatListManager.Instance.ExcludedNames.Contains(dictionaryName) && ((bool)useExcludeList == true))
                    {
                        param.Log.WarningCtx(LOG_CTX, String.Format("Scene '{0}' is specified to be excluded from prebuild", dictionaryName));
                        continue;
                    }

                    staticDirectoryOptimisedProcessedContent.StaticFiles.AddRange(platformProcessedDirNodes);
                }

                outputOptimisedContent.Add(staticDirectoryOptimisedProcessedContent);
                outputFullFatContent.Add(staticDirectoryFullFatProcessedContent);
            }

            foreach (IContentNode content in outputFullFatContent)
            {
                String rpfName = SIO.Path.GetFileName((content as RSG.Pipeline.Content.Directory).AbsolutePath);

                // Build the full fat content, this is every scene
                List<IProcess> rageProcesses = new List<IProcess>();
                StaticDirectory sd = content as StaticDirectory;
                foreach (IContentNode dir in sd.StaticFiles)
                {
                    Content.Directory directory = (dir as Content.Directory);

                    IEnumerable<ProcessBuilder> directoryConversionPb = PlatformProcessBuilder.CreateSimpleResourcesInCacheForDirectory(param, processors, owner, directory);

                    foreach (ProcessBuilder pb in directoryConversionPb)
                    {
                        // FILTER OUT THE PLATFORM WE DONT NEED
                        if (pb.Parameters.Count > 0)
                        {
                            if (pb.Parameters.ContainsKey("platform") && !directory.AbsolutePath.ToLower().Contains(pb.Parameters["platform"].ToString().ToLower()))
                            {
                                continue;
                            }
                        }
                        // FILTER OUT THE PLATFORM WE DONT NEED

                        // Redirect the output th thto the preview folder for the platform data
                        if (param.Flags.HasFlag(EngineFlags.Preview))
                        {
                            pb.Outputs.Clear();
                            pb.Outputs.Add((IContentNode)owner.CreateDirectory(param.Branch.Project.Config.CoreProject.Branches[param.Branch.Name].Preview, "*.*"));
                        }

                        IProcess rageProcess = pb.ToProcess();
                        rageProcess.State = ProcessState.Prebuilt;
                        rageProcesses.Add(rageProcess);
                    }
                }

                processes.AddRange(rageProcesses);

                // Do not process rpfs if we are in preview
                if (!param.Flags.HasFlag(EngineFlags.Preview))
                {
                    // For every platform create a new rpf, just pull in the rage processes for that platform and the inputs for that specific rpf
                    IEnumerable<ITarget> enabledTargets = PlatformProcessBuilder.GetEnabledTargets(param);
                    foreach (ITarget target in enabledTargets)
                    {
                        String outputFullFatFile = target.Environment.Subst(SIO.Path.Combine("$(assets)", "non_final", "build", param.Branch.Name, SIO.Path.GetFileName(target.ResourcePath), "anim", "cutscene", String.Format("cuts_{0}.rpf", rpfName)));
                        String outputOptimisedFile = target.Environment.Subst(SIO.Path.Combine("$(target)", "anim", "cutscene", String.Format("cuts_{0}.rpf", rpfName)));
                        IContentNode outputFullFatFileNode = owner.CreateAsset(outputFullFatFile, target.Platform);
                        IContentNode outputOptimisedFileNode = owner.CreateAsset(outputOptimisedFile, target.Platform);

                        ProcessBuilder rpfFullFatPb = new ProcessBuilder("RSG.Pipeline.Processor.Platform.RpfCreateProcessor", processors, owner);
                        ProcessBuilder rpfOptimisedPb = new ProcessBuilder("RSG.Pipeline.Processor.Platform.RpfCreateProcessor", processors, owner);

                        foreach (IProcess rage in rageProcesses)
                        {
                            if (rage.Parameters.ContainsKey("platform") && (RSG.Platform.Platform)rage.Parameters["platform"] == target.Platform)
                            {
                                StaticDirectory sdFat = outputFullFatContent.First() as StaticDirectory;
                                foreach (IContentNode dir in sdFat.StaticFiles)
                                {
                                    if ((rage.Inputs.First() as Directory).AbsolutePath == (dir as Directory).AbsolutePath)
                                    {
                                        rpfFullFatPb.Inputs.Add(rage.Outputs.FirstOrDefault());
                                    }
                                }

                                StaticDirectory sdOptimised = outputOptimisedContent.First() as StaticDirectory;
                                foreach (IContentNode dir in sdOptimised.StaticFiles)
                                {
                                    if ((rage.Inputs.First() as Directory).AbsolutePath == (dir as Directory).AbsolutePath)
                                    {
                                        rpfOptimisedPb.Inputs.Add(rage.Outputs.FirstOrDefault());
                                    }
                                }
                            }
                        }

                        if (rpfFullFatPb.Inputs.Count() == 0)
                        {
                            param.Log.WarningCtx(LOG_CTX, "No inputs for rage process targetting {0}.  Assuming all content has been excluded via {1}", (outputFullFatFileNode as File).AbsolutePath, cutMissionExclude);
                        }
                        else
                        {
                            rpfFullFatPb.Outputs.Add(outputFullFatFileNode);
                            IProcess rpfFullFatProcess = rpfFullFatPb.ToProcess();
                            rpfFullFatProcess.State = ProcessState.Prebuilt;
                            processes.Add(rpfFullFatProcess);
                        }

                        if (rpfOptimisedPb.Inputs.Count() == 0)
                        {
                            param.Log.WarningCtx(LOG_CTX, "No inputs for rage process targetting {0}.  Assuming all content has been excluded via {1}", (outputOptimisedFileNode as File).AbsolutePath, cutMissionExclude);
                        }
                        else
                        {
                            rpfOptimisedPb.Outputs.Add(outputOptimisedFileNode);
                            IProcess rpfOptimisedProcess = rpfOptimisedPb.ToProcess();
                            rpfOptimisedProcess.State = ProcessState.Prebuilt;
                            processes.Add(rpfOptimisedProcess);
                        }
                    }
                }
            }

            process.State = ProcessState.Discard;
            resultantProcesses = processes;
            syncDependencies = syncDepends;

            return (result);
        }

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            // Assign method output.
            tools = new List<XGE.ITool>();
            tasks = new List<XGE.ITask>();

            return (false);
        }
        #endregion // IProcessor Interface Methods

        #region Private Methods

        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        private Dictionary<string, string> ProcessPlatformCompressionSettings(IEngineParameters param)
        {
            Dictionary<string, string> dictPlatforms = new Dictionary<string,string>();

            if (this.Parameters.ContainsKey("CompressionFile"))
            {
                string compressionFile = (string)this.Parameters["CompressionFile"];
                compressionFile = param.Branch.Environment.Subst(compressionFile);
                compressionFile = compressionFile.Replace(SIO.Path.DirectorySeparatorChar, SIO.Path.AltDirectorySeparatorChar);

                if (SIO.File.Exists(compressionFile))
                {
                    var xmlDoc = new XmlDocument();
                    xmlDoc.Load(compressionFile);

                    XmlNodeList platformNodes = xmlDoc.SelectNodes("/compression/platform");
                    foreach (XmlNode node in platformNodes)
                    {
                        string compressionSettingsDir = node.Attributes["settings"].Value;
                        compressionSettingsDir = param.Branch.Environment.Subst(compressionSettingsDir);
                        compressionSettingsDir = compressionSettingsDir.Replace(SIO.Path.DirectorySeparatorChar, SIO.Path.AltDirectorySeparatorChar);

                        dictPlatforms.Add(node.Attributes["name"].Value.ToLower(), compressionSettingsDir);
                    }
                }
            }

            return dictPlatforms;
        }

        /// <summary>
        /// Prepare the compression/post process processes, calculate which platform uses what.
        /// </summary>
        /// <param name="platformCompression"></param>
        /// <param name="owner"></param>
        /// <param name="param"></param>
        /// <param name="processors"></param>
        /// <param name="groupDirNode"></param>
        /// <param name="dictionary"></param>
        /// <param name="finaliseDirNode"></param>
        /// <param name="processes"></param>
        /// <param name="cache"></param>
        /// <param name="dictionaryName"></param>
        /// <param name="processedDirectory"></param>
        /// <param name="defaultSkel"></param>
        /// <returns></returns>
        private ICollection<IContentNode> PerformPlatformCompression(Dictionary<string, string> platformCompression, IContentTree owner, IEngineParameters param, IProcessorCollection processors, IContentNode groupDirNode, IContentNode dictionary, IContentNode finaliseDirNode, List<IProcess> processes, String cache, String dictionaryName, String processedDirectory, object defaultSkel)
        {
            List<IContentNode> contentNodes = new List<IContentNode>();
            IEnumerable<ITarget> enabledTargets = PlatformProcessBuilder.GetEnabledTargets(param);

            Dictionary<string, IContentNode> platformToDir = new Dictionary<string, IContentNode>();

            foreach (ITarget target in enabledTargets)
            {
                string compressionSettingsFolder = String.Empty;

                if (!platformCompression.ContainsKey(target.Platform.ToString().ToLower()))
                {
                    param.Log.WarningCtx(LOG_CTX, String.Format("Platform '{0}' is not specified in the compression config, reverting to default.", target.Platform.ToString().ToLower()));
                    compressionSettingsFolder = (string)platformCompression["default"];
                }
                else
                    compressionSettingsFolder = (string)platformCompression[target.Platform.ToString().ToLower()];

                String compressionDirectory = SIO.Path.Combine(cache, dictionaryName, "compression", SIO.Path.GetFileName(compressionSettingsFolder));
               
                // Processor for compressing animation
                IContentNode compressionDirNode = owner.CreateDirectory(compressionDirectory, "*.*");
                ProcessBuilder compression = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Common.Compression", processors, owner);
                compression.Inputs.Add(groupDirNode);
                compression.AdditionalDependentContent.Add(owner.CreateFile((string)defaultSkel));
                compression.AdditionalDependentContent.Add(owner.CreateDirectory(compressionSettingsFolder, "*.*")); // Compression folder
                compression.Outputs.Add(compressionDirNode);

                // If any platforms share the same compression dir then we use the existing node that has being created (if any)
                if (!platformToDir.ContainsValue(compressionDirNode))
                {
                    IProcess compressionProcess = compression.ToProcess();
                    processes.Add(compressionProcess);

                    platformToDir.Add(target.Platform.ToString().ToLower(), compressionDirNode);
                }
                else
                {
                    foreach (IContentNode node in platformToDir.Values)
                    {
                        if (node == compressionDirNode)
                        {
                            platformToDir.Add(target.Platform.ToString().ToLower(), node);
                            break;
                        }
                    }
                }
            }

            // We have a per platform set of zips, but re-use the compression folders
            foreach (ITarget target in enabledTargets)
            {
                String platformProcessedDirectory = SIO.Path.Combine(processedDirectory, target.Platform.ToString().ToLower());
                IContentNode directoryProcessedContent = owner.CreateDirectory(platformProcessedDirectory, "*.*");

                IContentNode compressionDirNode = platformToDir[target.Platform.ToString().ToLower()];

                ProcessBuilder postProcess = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Cutscene.PostProcess", processors, owner);
                postProcess.Inputs.Add(compressionDirNode);
                postProcess.Inputs.Add((dictionary as RSG.Pipeline.Content.File));
                postProcess.Inputs.Add(finaliseDirNode);
                postProcess.Outputs.Add(directoryProcessedContent);

                IProcess postProcessProcess = postProcess.ToProcess();
                processes.Add(postProcessProcess);

                contentNodes.Add(directoryProcessedContent);
            }

            return contentNodes;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="owner"></param>
        /// <param name="inputs"></param>
        /// <returns></returns>
        private bool ValidateConcatentationEntries(IEngineParameters param, IContentNode dictionary, ConcatListEntry entry, String mission)
        {
            object processConcatLighting;
            if (!this.Parameters.TryGetValue("ProcessConcatLighting", out processConcatLighting))
            {
                processConcatLighting = false;
            }

            //if (entry.SceneFolders.Count < 2)
            //{
            //    param.Log.Error("Cutlist '{0}' is invalid; it contains only '{1}' input. Skipping.", entry.Filename, entry.SceneFolders.Count.ToString());
            //    return false;
            //}

            // We have cross mission concats, we do not want these to end up in the wrong rpf and bounce around so we ignore them if they are not sourced
            // from the correct source
            if(ConcatListManager.Instance.ConcatRPFOverloads.ContainsKey(SIO.Path.GetFileName(entry.OutputPath).ToLower()))
            {
                if (ConcatListManager.Instance.ConcatRPFOverloads[SIO.Path.GetFileName(entry.OutputPath).ToLower()] != mission)
                {
                    param.Log.Warning("Concat '{0}' was destined for RPF '{1}' but has being overloaded to RPF '{2}'.", SIO.Path.GetFileName(entry.OutputPath).ToLower(), mission, ConcatListManager.Instance.ConcatRPFOverloads[SIO.Path.GetFileName(entry.OutputPath).ToLower()]);
                    return false;
                }
            }

            Dictionary<string, List<string>> lightSceneToNames = new Dictionary<string, List<string>>();
            List<string> zipProcessed = new List<string>();
            bool bLightsPassed = true;

            foreach (string folder in entry.SceneFolders)
            {
                string sceneName = SIO.Path.GetFileName(SIO.Path.GetDirectoryName(folder)).ToLower();

                if (sceneName == SIO.Path.GetFileName(entry.OutputPath).ToLower())
                {
                    param.Log.Error("Cutlist '{0}' has an input which is the same as the output '{1}' / '{2}'. Skipping.", entry.Filename, folder, entry.OutputPath);
                    return false;
                }

                if (ConcatListManager.Instance.MergeNames.Contains(sceneName))
                {
                    param.Log.Warning("Concat '{0}' contains an entry which is a merged scene '{1}', unable to validate up front", SIO.Path.GetFileName(entry.OutputPath), sceneName);
                    continue;
                }

                // Build the zip location we want to extract for this scene, we assume it comes under the same mission
                // We need to check if our concat part has its mission overloaded
                string missionDirectory = SIO.Path.GetDirectoryName((dictionary as RSG.Pipeline.Content.File).AbsolutePath);
                if (ConcatListManager.Instance.ConcatMissionOverloads.ContainsKey(sceneName.ToLower()))
                {
                    missionDirectory = SIO.Path.Combine(param.Branch.Assets, "export", "anim", "cutscene", ConcatListManager.Instance.ConcatMissionOverloads[sceneName.ToLower()]);
                }

                string zip = String.Format("{0}\\{1}.icd.zip", missionDirectory, sceneName);

                String dictionaryName = Filename.GetBasename(SIO.Path.GetFileName((dictionary as RSG.Pipeline.Content.File).AbsolutePath));

                if (!SIO.File.Exists(zip))
                {
                    param.Log.Error("Zip '{0}' does not exist. Specified in cutlist '{1}' for concat '{2}'. (Initiated by: '{3}'). Skipping.", zip, entry.Filename, SIO.Path.GetFileName(entry.OutputPath), dictionaryName);
                    return false;
                }
                else
                {
                    // Since we cannot get the folders from a zip we should always have the source file ie. Shot_(X).cutxml, look for this.
                    string validationFile = SIO.Path.GetFileName(folder) + ".cutxml";

                    System.Collections.Generic.IEnumerable<string> zipContentFiles = new List<string>();
                    if (RSG.Pipeline.Services.Zip.GetFileList(zip, out zipContentFiles))
                    {
                        bool bFound = false;

                        foreach (string file in zipContentFiles)
                        {
                            if (validationFile.ToLower().Equals(file.ToLower()))
                            {
                                bFound = true;
                                break;
                            }
                        }

                        if (!bFound)
                        {
                            param.Log.Error("Shot '{0}' does not exist in zip '{1}'. Specified in cutlist '{2}' for concat '{3}'. (Initiated by: '{3}'). Skipping.", SIO.Path.GetFileName(folder), zip, entry.Filename, SIO.Path.GetFileName(entry.OutputPath), dictionaryName);
                            return false;
                        }
                    }
                }

                if ((bool)processConcatLighting == true)
                {
                    if (!zipProcessed.Contains(zip))
                    {
                        try
                        {
                            using (ZipFile zf = ZipFile.Read(zip))
                            {
                                foreach (ZipEntry ze in zf.Entries.Where(p => p.FileName == "data.lightxml"))
                                {
                                    using (SIO.MemoryStream ms = new SIO.MemoryStream())
                                    {
                                        ze.Extract(ms);
                                        ms.Seek(0, SIO.SeekOrigin.Begin);

                                        XDocument xd = XDocument.Load(ms);
                                        string[] lightObjectNames = xd.Root.Element("pCutsceneObjects").Elements("Item").Select(elem => elem.Element("cName").Value).ToArray();

                                        // Check that a light file does not have dupes within itself, this is bad setup
                                        var duplicates = lightObjectNames.GroupBy(i => i).Where(g => g.Count() > 1).Select(g => g.Key);

                                        foreach (string light in duplicates)
                                        {
                                            param.Log.Error("Dupe light objects exist in '{0}' - '{1}' for concat '{2}'. (Initiated by: '{3}'). Skipping.", sceneName, light, SIO.Path.GetFileName(entry.OutputPath), dictionaryName);
                                            bLightsPassed = false;
                                        }

                                        // Check that a light file does not clash with another light file in the concat, since all entities within a concat have to be
                                        // unique.
                                        foreach (KeyValuePair<string, List<string>> pair in lightSceneToNames)
                                        {
                                            string[] dupeLights = pair.Value.Intersect(lightObjectNames.ToList()).ToArray();

                                            if (dupeLights.Length > 0)
                                            {
                                                param.Log.Error("Dupe light objects exist in '{0} / {1}' - '{2}' for concat '{3}'. (Initiated by: '{4}'). Skipping.", sceneName, pair.Key, String.Join(" , ", dupeLights), SIO.Path.GetFileName(entry.OutputPath), dictionaryName);
                                                bLightsPassed = false;
                                            }
                                        }

                                        lightSceneToNames.Add(sceneName, lightObjectNames.ToList());
                                        zipProcessed.Add(zip);
                                    }
                                }
                            }
                        }
                        catch (Exception /*e*/)
                        {
                            param.Log.Error("Error reading light data from '{0}'", zip);
                        }
                    }
                }
            }

            if (!bLightsPassed)
                return false;

            return true;
        }

        private void GenerateMergeProcesses(IEngineParameters param, String cache, IContentTree owner, IProcessorCollection processors, List<IProcess> processes, IEnumerable<IContentNode> dictionaryInputs, Dictionary<String, IContentNode> mergeInputContent, String directoryContentMission)
        {
            // Lookup if the dictionary is part of a merge
            List<IContentNode> lstDictionaryInputs = dictionaryInputs as List<IContentNode>;

            for (int i = 0; i < lstDictionaryInputs.Count; ++i)
            {
                IContentNode dictionary = lstDictionaryInputs[i];

                String dictionaryName = Filename.GetBasename(SIO.Path.GetFileName((dictionary as RSG.Pipeline.Content.File).AbsolutePath));

                // Do not process the merge icd zip, skip this bad boi
                if (ConcatListManager.Instance.MergeNames.Contains(dictionaryName)) continue;

                if (ConcatListManager.Instance.MergeEntries.ContainsKey(SIO.Path.GetFileNameWithoutExtension(dictionary.Name.ToLower())))
                {
                    List<MergeEntry> entries = ConcatListManager.Instance.MergeEntries[SIO.Path.GetFileNameWithoutExtension(dictionary.Name.ToLower())];

                    foreach (MergeEntry entry in entries)
                    {
                        // We loop every entry, so multiples of the same concat, this makes sure we only add the concat once.
                        if (mergeInputContent.ContainsKey(SIO.Path.GetFileName(entry.OutputPath).ToLower())) continue;

                        String rootDirectory = SIO.Path.Combine(cache, SIO.Path.GetFileName(entry.OutputPath));
                        String mergeDirectory = SIO.Path.Combine(rootDirectory, "merge");

                        IContentNode mergeDirNode = owner.CreateDirectory(mergeDirectory, "*.*", true);
                        ProcessBuilder merge = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Cutscene.Merge",
                            processors, owner);

                        List<String> lstZipFiles = new List<string>();
                        List<String> lstDependantZips = new List<string>();

                        // We need to find the icd.zip files from the entry.SceneFolders - filter these out to be unique
                        foreach (string folder in entry.SceneFolders)
                        {
                            // Folder will be for example: X:\gta5\assets\cuts\Armenian_2_MCS_4
                            // Get the sceneName which is Armenian_2_MCS_4
                            string sceneName = SIO.Path.GetFileName(folder).ToLower();

                            // Build the zip location we want to extract for this scene, we assume it comes under the same mission
                            string missionDirectory = SIO.Path.GetDirectoryName((dictionary as RSG.Pipeline.Content.File).AbsolutePath);

                            string zip = String.Format("{0}\\{1}.icd.zip", missionDirectory, sceneName);

                            String mergeExtractionDirectory = SIO.Path.Combine(rootDirectory, "merge_extraction", sceneName);

                            IContentNode sceneExtractionDirNode = owner.CreateDirectory(mergeExtractionDirectory, "*.*");
                            ProcessBuilder sceneExtraction = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Common.Extract", processors, owner);
                            sceneExtraction.Inputs.Add(owner.CreateFile(zip));
                            sceneExtraction.Outputs.Add(sceneExtractionDirNode);

                            if (!lstDependantZips.Contains(zip.ToLower()))
                                lstDependantZips.Add(zip.ToLower());

                            // We need the actual dir the data is within, so Shot_1 etc
                            string partPath = ((Content.IFilesystemNode)sceneExtractionDirNode).AbsolutePath;

                            IContentNode partDir = owner.CreateDirectory(partPath, "*.*");
                            sceneExtraction.Outputs.Add(partDir); // add as a output to keep sync

                            // We could potentially send multiple extracts of the same zip, we only want to extract the zip once.
                            // i.e if we have car_2_mcs_1p1\shot1 and car_2_mcs_1p1\shot2, both of these dirs will be extracted from the one zip.
                            if (!lstZipFiles.Contains(((Content.IFilesystemNode)sceneExtractionDirNode).AbsolutePath))
                            {
                                IProcess sceneExtractionProcess = sceneExtraction.ToProcess();
                                processes.Add(sceneExtractionProcess);
                                lstZipFiles.Add(((Content.IFilesystemNode)sceneExtractionDirNode).AbsolutePath);
                            }

                            merge.Inputs.Add(partDir);
                        }

                        merge.Outputs.Add(mergeDirNode);

                        IProcess mergeProcess = merge.ToProcess();
                        processes.Add(mergeProcess);

                        string missionName = SIO.Path.GetFileName(SIO.Path.GetDirectoryName((dictionary as RSG.Pipeline.Content.File).AbsolutePath));

                        // Pack the resulting data into an icd.zip for the concat
                        string mergeZip = SIO.Path.Combine(param.Branch.Assets, "export", "anim", "cutscene_merge", missionName, SIO.Path.GetFileName(entry.OutputPath) + ".icd.zip");

                        if (!SIO.Directory.Exists(SIO.Path.Combine(param.Branch.Assets, "export", "anim", "cutscene_merge", missionName)))
                            SIO.Directory.CreateDirectory(SIO.Path.Combine(param.Branch.Assets, "export", "anim", "cutscene_merge", missionName));

                        AssetPackScheduleBuilder assetPackScheduleBuilder = new AssetPackScheduleBuilder();
                        AssetPackZipArchive outputZipArchive = assetPackScheduleBuilder.AddZipArchive(mergeZip);

                        outputZipArchive.AddDirectory((mergeDirNode as RSG.Pipeline.Content.Directory).AbsolutePath, null, null, true);
                        //outputZipArchive.AddDirectory((mergeDirNode as RSG.Pipeline.Content.Directory).AbsolutePath);
                        //outputZipArchive.AddDirectory(SIO.Path.Combine((mergeDirNode as RSG.Pipeline.Content.Directory).AbsolutePath, "faces"), "faces");

                        SIO.Directory.CreateDirectory(rootDirectory);

                        String assetPackSchedulePathname = System.IO.Path.Combine(rootDirectory, "asset_pack_schedule.xml");
                        assetPackScheduleBuilder.Save(assetPackSchedulePathname);

                        IContentNode assetPackScheduleNode = owner.CreateFile(assetPackSchedulePathname);

                        ProcessBuilder assetPacker = new ProcessBuilder("RSG.Pipeline.Processor.Common.AssetPackProcessor", processors, owner);
                        assetPacker.Inputs.Add(assetPackScheduleNode);
                        foreach (String zip in lstDependantZips)
                            assetPacker.AdditionalDependentContent.Add(owner.CreateFile(zip));

                        IContentNode mergeOutputFile = owner.CreateFile(mergeZip);

                        IProcess assetPackerProcess = assetPacker.ToProcess();
                        processes.Add(assetPackerProcess);

                        mergeInputContent.Add(SIO.Path.GetFileName(entry.OutputPath).ToLower(), mergeOutputFile);

                        param.Log.MessageCtx(LOG_CTX, String.Format("Automatically generating merge '{0}' from part '{1}' with '{2}'", SIO.Path.GetFileName(entry.OutputPath), dictionary.Name.ToLower(), entry.Filename));

                        // Inject the concat content node if it doesnt exist
                        IContentNode dictFile = owner.CreateFile(mergeZip);
                        lstDictionaryInputs.Add(dictFile);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="owner"></param>
        /// <param name="inputs"></param>
        /// <returns></returns>
        private void GenerateConcatenationProcesses(IEngineParameters param, String cache, IContentTree owner, IProcessorCollection processors, List<IProcess> processes, IEnumerable<IContentNode> dictionaryInputs, Dictionary<String, IContentNode> concatInputContent, Dictionary<String, IContentNode> mergeInputContent, String directoryContentMission, bool useClipXml)
        {
            List<IContentNode> lstDictionaryInputs = dictionaryInputs as List<IContentNode>;

            object processConcatLighting;
            if (!this.Parameters.TryGetValue("ProcessConcatLighting", out processConcatLighting))
            {
                processConcatLighting = false;         
            }

            object rebuildScene = false;
            if (!this.Parameters.TryGetValue("RebuildScene", out rebuildScene))
            {
                rebuildScene = false;
            }

            for(int i=0; i < lstDictionaryInputs.Count; ++i)
            {
                IContentNode dictionary = lstDictionaryInputs[i];

                String dictionaryName = Filename.GetBasename(SIO.Path.GetFileName((dictionary as RSG.Pipeline.Content.File).AbsolutePath));

                // Do not process the concat icd zip, skip this bad boi
                if (ConcatListManager.Instance.ConcatNames.Contains(dictionaryName)) continue;

                if (ConcatListManager.Instance.ConcatEntries.ContainsKey(SIO.Path.GetFileNameWithoutExtension(dictionary.Name.ToLower())))
                {
                    List<ConcatListEntry> entries = ConcatListManager.Instance.ConcatEntries[SIO.Path.GetFileNameWithoutExtension(dictionary.Name.ToLower())];

                    foreach(ConcatListEntry entry in entries)
                    {
                        // We loop every entry, so multiples of the same concat, this makes sure we only add the concat once.
                        if(concatInputContent.ContainsKey(SIO.Path.GetFileName(entry.OutputPath).ToLower())) continue;

                        if (!ValidateConcatentationEntries(param, dictionary, entry, directoryContentMission)) continue;

                        String rootDirectory = SIO.Path.Combine(cache, SIO.Path.GetFileName(entry.OutputPath));
                        String concatDirectory = SIO.Path.Combine(rootDirectory, "concat");

                        IContentNode concatDirNode = owner.CreateDirectory(concatDirectory, "*.*");
                        ProcessBuilder concat = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Cutscene.Concatenation",
                            processors, owner);

                        ProcessBuilder concatLighting = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Cutscene.ConcatenationLighting",
                                processors, owner);

                        List<String> lstZipFiles = new List<string>();
                        List<String> lstDependantZips = new List<string>();

                        // We need to find the icd.zip files from the entry.SceneFolders - filter these out to be unique
                        foreach (string folder in entry.SceneFolders)
                        {
                            // Folder will be for example: X:\gta5\assets\cuts\Armenian_2_MCS_4\Shot_1
                            // Get the sceneName which is Armenian_2_MCS_4
                            string sceneName = SIO.Path.GetFileName(SIO.Path.GetDirectoryName(folder)).ToLower();

                            // Build the zip location we want to extract for this scene, we assume it comes under the same mission
                            // We need to check if our concat part has its mission overloaded
                            string missionDirectory = SIO.Path.GetDirectoryName((dictionary as RSG.Pipeline.Content.File).AbsolutePath);
                            if (ConcatListManager.Instance.ConcatMissionOverloads.ContainsKey(sceneName.ToLower()))
                            {
                                missionDirectory = SIO.Path.Combine(param.Branch.Assets, "export", "anim", "cutscene", ConcatListManager.Instance.ConcatMissionOverloads[sceneName.ToLower()]);
                            }

                            bool isMerged = ConcatListManager.Instance.MergeNames.Contains(sceneName);

                            string zip = String.Format("{0}\\{1}.icd.zip", missionDirectory, sceneName);

                            String clipDictionaryDirectory = SIO.Path.Combine(rootDirectory, "concat_clipdictionary", sceneName);

                            ProcessBuilder clipDictionary = null;
                            if ((bool)useClipXml == true)
                            {
                                IContentNode clipDictionaryDirNode = owner.CreateDirectory(clipDictionaryDirectory, "*.*");
                                clipDictionary = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Common.ClipDictionaryProcessor", processors, owner);
                                clipDictionary.Inputs.Add(owner.CreateFile(zip));
                                clipDictionary.Outputs.Add(clipDictionaryDirNode);
                                clipDictionary.Outputs.Add(owner.CreateFile(SIO.Path.Combine(clipDictionaryDirectory, sceneName + ".icd.zip")));
                            }

                            String concatExtractionDirectory = SIO.Path.Combine(rootDirectory, "concat_extraction", sceneName);

                            IContentNode sceneExtractionDirNode = owner.CreateDirectory(concatExtractionDirectory, "*.*");
                            ProcessBuilder sceneExtraction = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Common.Extract", processors, owner);
                            if ((bool)useClipXml == true)
                                sceneExtraction.Inputs.Add(owner.CreateFile(SIO.Path.Combine(clipDictionaryDirectory, sceneName + ".icd.zip")));
                            else
                            {
                                if (isMerged)
                                    sceneExtraction.Inputs.Add(mergeInputContent[sceneName]);
                                else
                                    sceneExtraction.Inputs.Add(owner.CreateFile(zip));
                            }
                            sceneExtraction.Outputs.Add(sceneExtractionDirNode);

                            //if ((bool)rebuildScene == true)
                            //{
                                String concatRebuildDirectory = SIO.Path.Combine(rootDirectory, "concat_rebuild", sceneName);

                                IContentNode rebuildDirNode = owner.CreateDirectory(concatRebuildDirectory, "*.*");
                                ProcessBuilder rebuildData = new ProcessBuilder("RSG.Pipeline.Processor.Animation.Cutscene.SliceDice", processors, owner);
                                rebuildData.Inputs.Add(sceneExtractionDirNode);
                                rebuildData.Inputs.Add(owner.CreateFile(zip));
                                rebuildData.Outputs.Add(rebuildDirNode);
                            //}

                            if(!lstDependantZips.Contains(zip.ToLower()))
                                lstDependantZips.Add(zip.ToLower());

                            // We need the actual dir the data is within, so Shot_1 etc
                            string partPath = ((bool)rebuildScene == true) ? SIO.Path.Combine(((Content.IFilesystemNode)rebuildDirNode).AbsolutePath, SIO.Path.GetFileName(folder)) : SIO.Path.Combine(((Content.IFilesystemNode)sceneExtractionDirNode).AbsolutePath, SIO.Path.GetFileName(folder));

                            IContentNode partDir = owner.CreateDirectory(partPath, "*.*");
                            if ((bool)rebuildScene == true)
                                rebuildData.Outputs.Add(partDir); // add as a output to keep sync
                            else
                                sceneExtraction.Outputs.Add(partDir); // add as a output to keep sync
                            
                            // We could potentially send multiple extracts of the same zip, we only want to extract the zip once.
                            // i.e if we have car_2_mcs_1p1\shot1 and car_2_mcs_1p1\shot2, both of these dirs will be extracted from the one zip.
                            if (!lstZipFiles.Contains( ((bool)rebuildScene == true) ? ((Content.IFilesystemNode)rebuildDirNode).AbsolutePath : ((Content.IFilesystemNode)sceneExtractionDirNode).AbsolutePath))
                            {
                                if ((bool)useClipXml == true)
                                {
                                    IProcess clipDictionaryProcess = clipDictionary.ToProcess();
                                    processes.Add(clipDictionaryProcess);
                                }

                                if ((bool)rebuildScene == true)
                                {
                                    IProcess rebuildProcess = rebuildData.ToProcess();
                                    processes.Add(rebuildProcess);
                                }
                                
                                IProcess sceneExtractionProcess = sceneExtraction.ToProcess();
                                processes.Add(sceneExtractionProcess);
                                if ((bool)rebuildScene == true)
                                    lstZipFiles.Add(((Content.IFilesystemNode)rebuildDirNode).AbsolutePath);
                                else
                                    lstZipFiles.Add(((Content.IFilesystemNode)sceneExtractionDirNode).AbsolutePath);
                            }

                            concat.Inputs.Add(partDir);
                            concatLighting.Inputs.Add(partDir);
                        }

                        // If the concat list changes then we want to rebuild the concat
                        IContentNode concatListFileNode = owner.CreateFile(entry.Filename);
                        concat.AdditionalDependentContent.Add(concatListFileNode);

                        concat.Outputs.Add(concatDirNode);

                        concatLighting.Outputs.Add(concatDirNode);

                        concat.Parameters.Add(RSG.Pipeline.Core.Constants.Cutscene_Concat_SectionMethod, entry.SectionMethod);
                        concat.Parameters.Add(RSG.Pipeline.Core.Constants.Cutscene_Concat_SectionDuration, entry.SectionDuration);
                        concat.Parameters.Add(RSG.Pipeline.Core.Constants.Cutscene_Concat_Audio, entry.AudioName);

                        IProcess concatProcess = concat.ToProcess();
                        processes.Add(concatProcess);

                        if ((bool)processConcatLighting == true)
                        {
                            IProcess concatLightingProcess = concatLighting.ToProcess();
                            processes.Add(concatLightingProcess);
                        }

                        string missionName = SIO.Path.GetFileName(SIO.Path.GetDirectoryName((dictionary as RSG.Pipeline.Content.File).AbsolutePath));

                        // Pack the resulting data into an icd.zip for the concat
                        string concatZip = SIO.Path.Combine(param.Branch.Assets, "export", "anim", "cutscene_concat", missionName, SIO.Path.GetFileName(entry.OutputPath) + ".icd.zip");

                        if (SIO.Directory.Exists(SIO.Path.Combine(param.Branch.Assets, "export", "anim", "cutscene_concat", missionName)))
                            SIO.Directory.CreateDirectory(SIO.Path.Combine(param.Branch.Assets, "export", "anim", "cutscene_concat", missionName));

                        AssetPackScheduleBuilder assetPackScheduleBuilder = new AssetPackScheduleBuilder();
                        AssetPackZipArchive outputZipArchive = assetPackScheduleBuilder.AddZipArchive(concatZip);

                        outputZipArchive.AddDirectory((concatDirNode as RSG.Pipeline.Content.Directory).AbsolutePath);
                        outputZipArchive.AddDirectory(SIO.Path.Combine((concatDirNode as RSG.Pipeline.Content.Directory).AbsolutePath, "faces"), "faces");

                        SIO.Directory.CreateDirectory(rootDirectory);

                        String assetPackSchedulePathname = System.IO.Path.Combine(rootDirectory, "asset_pack_schedule.xml");
                        assetPackScheduleBuilder.Save(assetPackSchedulePathname);

                        IContentNode assetPackScheduleNode = owner.CreateFile(assetPackSchedulePathname);

                        ProcessBuilder assetPacker = new ProcessBuilder("RSG.Pipeline.Processor.Common.AssetPackProcessor", processors, owner);
                        assetPacker.Inputs.Add(assetPackScheduleNode);
                        foreach (String zip in lstDependantZips)
                            assetPacker.AdditionalDependentContent.Add(owner.CreateFile(zip));

                        IContentNode concatOutputFile = owner.CreateFile(concatZip);

                        IProcess assetPackerProcess = assetPacker.ToProcess();
                        processes.Add(assetPackerProcess);

                        concatInputContent.Add(SIO.Path.GetFileName(entry.OutputPath).ToLower(), concatOutputFile);

                        param.Log.MessageCtx(LOG_CTX, String.Format("Automatically generating concat '{0}' from part '{1}' with '{2}'", SIO.Path.GetFileName(entry.OutputPath), dictionary.Name.ToLower(), entry.Filename));

                        // Inject the concat content node if it doesnt exist
                        IContentNode dictFile = owner.CreateFile(concatZip);
                        lstDictionaryInputs.Add(dictFile);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="param"></param>
        /// <param name="owner"></param>
        /// <param name="inputs"></param>
        /// <returns></returns>
        private bool GenerateInputContentMap(IEngineParameters param, IContentTree owner, IEnumerable<IContentNode> inputs, out IEnumerable<IContentNode> realInputs)
        {
            bool result = true;
            realInputs = new List<IContentNode>();

            List<String> excludedDictionaries = DictionaryModManager.Instance.ExcludedDictionaries as List<String>;
            // Loop through our input directories and create a dictionary for the directory node and the contents of the directory, minus any excluded directories
            foreach (IContentNode contentnode in inputs)
            {
                if (contentnode is RSG.Pipeline.Content.Directory)
                {
                    IEnumerable<IContentNode> dictionaryList = (contentnode as Directory).EvaluateInputs();
                    if (dictionaryList.Count() == 0)
                        continue;
                    List<String> dictionaryListLower = new List<String>();
                    foreach (IContentNode dictionary in dictionaryList)
                    {
                        dictionaryListLower.Add((dictionary as RSG.Pipeline.Content.File).AbsolutePath.ToLower());
                    }

                    if (excludedDictionaries.Contains(contentnode.Name.ToLower()))
                    {
                        param.Log.MessageCtx(LOG_CTX, String.Format("Ignoring dictionary {0}.  It is listed as an excluded clip dictionary.", contentnode.Name));
                    }
                    else
                    {
                        (realInputs as List<IContentNode>).Add(contentnode);
                    }
                }
                else if (contentnode is RSG.Pipeline.Content.File)
                {
                    String filePath = SIO.Path.GetFullPath((contentnode as RSG.Pipeline.Content.File).AbsolutePath);
                    String message = String.Format("File content has made it into animation Preprocessor.  This should never happen: {0}.", filePath);
                    param.Log.ErrorCtx(LOG_CTX, message);
                    result = false;
                }
            }
            return result;
        }

        #endregion

        #region Private Static Methods
        /// <summary>
        /// Return cache dir for the branch.  Could be a property if we kept the params as a member?
        /// </summary>
        /// <returns></returns>
        private static String GetCacheDir(IEngineParameters param)
        {
            return (SIO.Path.Combine(param.Branch.Project.Cache, "convert", param.Branch.Name, "anim", "cutscene"));
        }
        #endregion // Private Static Methods
    }

} // RSG.Pipeline.Processor.Animation namespace
