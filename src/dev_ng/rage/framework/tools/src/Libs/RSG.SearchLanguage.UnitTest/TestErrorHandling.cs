﻿//---------------------------------------------------------------------------------------------
// <copyright file="TestSearchGrammar.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage.UnitTest
{
    using System;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using RSG.Base.Language;
    using RSG.Base.Logging;
    using RSG.SearchLanguage;

    [TestClass]
    public class TestErrorHandling
    {
        #region Test Data
        private const String errorLiteral1 = "\"prop_barrel_1";
        #endregion // Test Data

        #region Member Data
        /// <summary>
        /// Parser object.
        /// </summary>
        private SearchLanguage _parser;
        #endregion // Member Data

        [TestInitialize]
        public void TestInitialise()
        {
            LogFactory.Initialize();
            _parser = new SearchLanguage();
        }

        [TestMethod]
        public void TestErrorLiteral1()
        {
            SearchParserResult result = _parser.Parse(errorLiteral1, LogFactory.ApplicationLog);
            Assert.IsTrue(result.SyntaxErrors.Any());
        }
    }

} // RSG.SearchLanguage.UnitTest namespace
