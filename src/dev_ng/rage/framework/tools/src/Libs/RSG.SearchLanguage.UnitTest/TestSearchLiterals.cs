﻿//---------------------------------------------------------------------------------------------
// <copyright file="TestSearchLiterals.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage.UnitTest
{
    using System;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using RSG.Base.Extensions;
    using RSG.Base.Language;
    using RSG.Base.Logging;
    using RSG.SearchLanguage;
    using RSG.SearchLanguage.AST;

    /// <summary>
    /// 
    /// </summary>
    [TestClass]
    public class TestSearchLiterals
    {
        #region Test Data
        private const String testQueryLiteral1 = "\"prop_barrel_1\"";
        private const String testQueryLiteral2 = "\"carbine\"";
        private const String testQueryLiteral3 = "\"prop_barrel_1\" \"carbine\""; // same as below
        private const String testQueryLiteral4 = "\"prop_barrel_1\" AND \"carbine\"";
        private const String testQueryLiteral5 = "\"prop_barrel_1\" OR \"carbine\"";
        #endregion // Test Data

        #region Member Data
        /// <summary>
        /// Parser object.
        /// </summary>
        private SearchLanguage parser;
        #endregion // Member Data

        [TestInitialize]
        public void TestInitialise()
        {
            LogFactory.Initialize();
            parser = new SearchLanguage();
        }

        [TestMethod]
        public void TestParseLiterals()
        {
            SearchParserResult result = parser.Parse(testQueryLiteral1);
            Assert.IsTrue(result.SyntaxErrors.None());
            IAstNode top = result.Query;
            Assert.IsInstanceOfType(top, typeof(SearchStringLiteral));
            RSG.SearchLanguage.AST.SearchStringLiteral literal = (SearchStringLiteral)top;
            Assert.IsInstanceOfType(literal.Expression, typeof(LiteralValue<String>));
            RSG.SearchLanguage.AST.LiteralValue<String> expr = (LiteralValue<String>)literal.Expression;
            Assert.AreEqual("prop_barrel_1", expr.Value);

            result = parser.Parse(testQueryLiteral2);
            Assert.IsTrue(result.SyntaxErrors.None());
            top = result.Query;
            Assert.IsInstanceOfType(top, typeof(SearchStringLiteral));
            literal = (SearchStringLiteral)top;
            Assert.IsInstanceOfType(literal.Expression, typeof(LiteralValue<String>));
            expr = (LiteralValue<String>)literal.Expression;
            Assert.AreEqual("carbine", expr.Value);

            result = parser.Parse(testQueryLiteral3);
            Assert.IsTrue(result.SyntaxErrors.None());
            top = result.Query;
            Assert.IsInstanceOfType(top, typeof(RSG.SearchLanguage.AST.SearchStringLiteral));
            literal = (RSG.SearchLanguage.AST.SearchStringLiteral)top;
            Assert.IsInstanceOfType(literal.Expression, typeof(RSG.SearchLanguage.AST.StringLiteralExpression));
            RSG.SearchLanguage.AST.StringLiteralExpression exp = (RSG.SearchLanguage.AST.StringLiteralExpression)literal.Expression;
            Assert.IsInstanceOfType(exp.Literal, typeof(LiteralValue<String>));
            Assert.AreEqual(RSG.SearchLanguage.AST.LogicalOperatorType.And, exp.Operator);
            Assert.IsInstanceOfType(exp.Expression, typeof(LiteralValue<String>));
            Assert.AreEqual("prop_barrel_1", exp.Literal.Value);
            Assert.AreEqual("carbine", ((LiteralValue<String>)exp.Expression).Value);

            result = parser.Parse(testQueryLiteral4);
            Assert.IsTrue(result.SyntaxErrors.None());
            top = result.Query;
            Assert.IsInstanceOfType(top, typeof(RSG.SearchLanguage.AST.SearchStringLiteral));
            literal = (RSG.SearchLanguage.AST.SearchStringLiteral)top;
            Assert.IsInstanceOfType(literal.Expression, typeof(RSG.SearchLanguage.AST.StringLiteralExpression));
            exp = (RSG.SearchLanguage.AST.StringLiteralExpression)literal.Expression;
            Assert.IsInstanceOfType(exp.Literal, typeof(LiteralValue<String>));
            Assert.AreEqual(RSG.SearchLanguage.AST.LogicalOperatorType.And, exp.Operator);
            Assert.IsInstanceOfType(exp.Expression, typeof(LiteralValue<String>));
            Assert.AreEqual("prop_barrel_1", exp.Literal.Value);
            Assert.AreEqual("carbine", ((LiteralValue<String>)exp.Expression).Value);

            result = parser.Parse(testQueryLiteral5);
            Assert.IsTrue(result.SyntaxErrors.None());
            top = result.Query;
            Assert.IsInstanceOfType(top, typeof(RSG.SearchLanguage.AST.SearchStringLiteral));
            literal = (RSG.SearchLanguage.AST.SearchStringLiteral)top;
            Assert.IsInstanceOfType(literal.Expression, typeof(RSG.SearchLanguage.AST.StringLiteralExpression));
            exp = (RSG.SearchLanguage.AST.StringLiteralExpression)literal.Expression;
            Assert.IsInstanceOfType(exp.Literal, typeof(LiteralValue<String>));
            Assert.AreEqual(RSG.SearchLanguage.AST.LogicalOperatorType.Or, exp.Operator);
            Assert.IsInstanceOfType(exp.Expression, typeof(LiteralValue<String>));
            Assert.AreEqual("prop_barrel_1", exp.Literal.Value);
            Assert.AreEqual("carbine", ((LiteralValue<String>)exp.Expression).Value);
        }
    }

} // RSG.Search.UnitTest namespace
