﻿//---------------------------------------------------------------------------------------------
// <copyright file="TestSearchGrammar.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SearchLanguage.UnitTest
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using RSG.Base.Extensions;
    using RSG.Base.Language;
    using RSG.Base.Logging;
    using RSG.SearchLanguage;
    using RSG.SearchLanguage.AST;

    /// <summary>
    /// 
    /// </summary>
    [TestClass]
    public class TestSearchGrammar
    {
        #region Test Data
        // No condition searches.
        private const String testQueryNoCondition1 = "FIND mapobject FROM mapobjects";
        private const String testQueryNoCondition2 = "FIND o FROM mapobjects, props";

        // Boolean-literal expressions
        private const String testQuery1 = "FIND mapobject FROM mapobjects WHERE mapobject.IsFragment";
        private const String testQuery2 = "FIND mapobject FROM mapobjects WHERE NOT mapobject.IsFragment";
        private const String testQuery3 = "FIND mapobject FROM mapobjects WHERE mapobject.SubObject.IsFragment";
        private const String testQuery4 = "FIND mapobject FROM mapobjects WHERE mapobject.LODDistance > 5.0";
        private const String testQuery5 = "FIND weapon FROM weapons, props WHERE weapon.Name == \"carbine\"";       
        
        // Boolean-composite expressions
        private const String testQuery6 = "FIND mapobject FROM mapobjects WHERE mapobject.LODDistance > 5.0 AND mapobject.LODDistance <= 1000";
        #endregion // Test Data

        #region Member Data
        /// <summary>
        /// Parser object.
        /// </summary>
        private SearchLanguage parser;
        #endregion // Member Data

        [TestInitialize]
        public void TestInitialise()
        {
            LogFactory.Initialize();
            parser = new SearchLanguage();
        }
        
        [TestMethod]
        public void TestParseQueriesNoCondition() 
        {
            SearchParserResult result = parser.Parse(testQueryNoCondition1);
            Assert.IsTrue(result.SyntaxErrors.None());
            AssertQueryNoCondition(result.Query, "mapobject", new String[] { "mapobjects" });

            result = parser.Parse(testQueryNoCondition2);
            Assert.IsTrue(result.SyntaxErrors.None());
            AssertQueryNoCondition(result.Query, "o", new String[] { "mapobjects", "props" });
        }

        [TestMethod]
        public void TestParseQueries()
        {
            SearchParserResult result = parser.Parse(testQuery1);
            Assert.IsTrue(result.SyntaxErrors.None());
            AssertQueryCondition(result.Query, "mapobject", new String[] { "mapobjects" });
            SearchQuery query = (SearchQuery)result.Query;
            Assert.IsInstanceOfType(query.WhereExpression, typeof(BooleanTerm));
            BooleanTerm term = (BooleanTerm)query.WhereExpression;
            Assert.AreEqual(false, term.Invert);
            Assert.IsInstanceOfType(term.Literal, typeof(LiteralValue<bool>));
            Assert.AreEqual(true, ((LiteralValue<bool>)term.Literal).Value);

            result = parser.Parse(testQuery2);
            Assert.IsTrue(result.SyntaxErrors.None());
            AssertQueryCondition(result.Query, "mapobject", new String[] { "mapobjects" });
            query = (SearchQuery)result.Query;
            Assert.IsInstanceOfType(query.WhereExpression, typeof(BooleanTerm));
            term = (BooleanTerm)query.WhereExpression; 
            Assert.AreEqual(false, term.Invert);
            Assert.IsInstanceOfType(term.Literal, typeof(LiteralValue<bool>));
            Assert.AreEqual(false, ((LiteralValue<bool>)term.Literal).Value);

            result = parser.Parse(testQuery3);
            Assert.IsTrue(result.SyntaxErrors.None());
            AssertQueryCondition(result.Query, "mapobject", new String[] { "mapobjects" });
            query = (SearchQuery)result.Query;
            Assert.IsInstanceOfType(query.WhereExpression, typeof(BooleanTerm));
            Assert.AreEqual(false, ((BooleanTerm)query.WhereExpression).Invert);

            result = parser.Parse(testQuery4);
            Assert.IsTrue(result.SyntaxErrors.None());
            AssertQueryCondition(result.Query, "mapobject", new String[] { "mapobjects" });
            query = (SearchQuery)result.Query;
            Assert.IsInstanceOfType(query.WhereExpression, typeof(BooleanTerm));
            Assert.AreEqual(false, ((BooleanTerm)query.WhereExpression).Invert);

            result = parser.Parse(testQuery5);
            Assert.IsTrue(result.SyntaxErrors.None());
            AssertQueryCondition(result.Query, "weapon", new String[] { "weapons", "props" }); 
            query = (SearchQuery)result.Query;
            Assert.IsInstanceOfType(query.WhereExpression, typeof(BooleanTerm));
            Assert.AreEqual(false, ((BooleanTerm)query.WhereExpression).Invert);

            result = parser.Parse(testQuery6);
            Assert.IsTrue(result.SyntaxErrors.None());
            AssertQueryCondition(result.Query, "mapobject", new String[] { "mapobjects" });
            query = (SearchQuery)result.Query;
            Assert.IsInstanceOfType(query.WhereExpression, typeof(BooleanCompositeExpression));
            BooleanCompositeExpression composite = (BooleanCompositeExpression)query.WhereExpression;
            Assert.IsInstanceOfType(composite.Expression, typeof(BooleanTerm));
        }

        #region Private Methods
        /// <summary>
        /// Assert tests for no condition queries (e.g. SearchAll).
        /// </summary>
        /// <param name="query"></param>
        /// <param name="identifier"></param>
        /// <param name="spaces"></param>
        private void AssertQueryNoCondition(IAstNode query, String identifier,
            IEnumerable<String> spaces)
        {
            Assert.IsInstanceOfType(query, typeof(RSG.SearchLanguage.AST.SearchAll));
            SearchAll searchQuery = (SearchAll)query;
            Assert.AreEqual(identifier, searchQuery.ObjectIdentifier.Symbol.Name);
            Assert.AreEqual(SearchSymbolType.Object, searchQuery.ObjectIdentifier.Symbol.Type);
            Assert.AreEqual(spaces.Count(), searchQuery.SearchSpaces.Count());
            for (int n = 0; n < spaces.Count(); ++n)
            {
                String expectedSearchSpace = spaces.ElementAt(n);
                Identifier actualSearchSpace = searchQuery.SearchSpaces.ElementAt(n);
                Assert.AreEqual(expectedSearchSpace, actualSearchSpace.Symbol.Name);
                Assert.AreEqual(SearchSymbolType.SearchSpace, actualSearchSpace.Symbol.Type);
            }
        }

        /// <summary>
        /// Assert tests for conditional queries (e.g. SearchQuery).
        /// </summary>
        /// <param name="query"></param>
        /// <param name="identifier"></param>
        /// <param name="spaces"></param>
        private void AssertQueryCondition(IAstNode query, String identifier, 
            IEnumerable<String> spaces)
        {
            Assert.IsInstanceOfType(query, typeof(RSG.SearchLanguage.AST.SearchQuery));
            SearchQuery searchQuery = (SearchQuery)query;
            Assert.AreEqual(identifier, searchQuery.ObjectIdentifier.Symbol.Name);
            Assert.AreEqual(SearchSymbolType.Object, searchQuery.ObjectIdentifier.Symbol.Type);
            Assert.AreEqual(spaces.Count(), searchQuery.SearchSpaces.Count());
            for (int n = 0; n < spaces.Count(); ++n)
            {
                String expectedSearchSpace = spaces.ElementAt(n);
                Identifier actualSearchSpace = searchQuery.SearchSpaces.ElementAt(n);
                Assert.AreEqual(expectedSearchSpace, actualSearchSpace.Symbol.Name);
                Assert.AreEqual(SearchSymbolType.SearchSpace, actualSearchSpace.Symbol.Type);
            }
        }
        #endregion // Private Methods
    }

} // RSG.Search.UnitTest namespace
