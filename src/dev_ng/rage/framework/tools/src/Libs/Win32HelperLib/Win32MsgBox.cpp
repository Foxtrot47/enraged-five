//
// filename:	Win32MsgBox.cpp
// author:		David Muir
// date:		25 April 2007
// description:	Friendly wrapper around Win32's MessageBox function
//

// --- Include Files ------------------------------------------------------------

// Local headers
#include "Win32MsgBox.h"

// STL headers
#include <cstdarg>
#include <cstdio>

// Win32 headers
#include <Windows.h>

// --- Constants ----------------------------------------------------------------

const int sC_s_cnt_BufferSize = 1024;

// --- Code ---------------------------------------------------------------------

void 
DisplayMessage( const char* sTitle, const char* sFormat, ... )
{
	va_list args;
	char sBuffer[sC_s_cnt_BufferSize];

	va_start( args, sFormat );
	vsprintf_s( sBuffer, sC_s_cnt_BufferSize, sFormat, args );
	va_end( args );

	MessageBox( 0, sBuffer, sTitle, MB_OK );
}

void 
DisplayError( const char* sTitle, const char* sFormat, ... )
{
	va_list args;
	char sBuffer[sC_s_cnt_BufferSize];

	va_start( args, sFormat );
	vsprintf_s( sBuffer, sC_s_cnt_BufferSize, sFormat, args );
	va_end( args );

	MessageBox( 0, sBuffer, sTitle, MB_OK | MB_ICONERROR );
}

void 
DisplayWarning( const char* sTitle, const char* sFormat, ... )
{
	va_list args;
	char sBuffer[sC_s_cnt_BufferSize];

	va_start( args, sFormat );
	vsprintf_s( sBuffer, sC_s_cnt_BufferSize, sFormat, args );
	va_end( args );

	MessageBox( 0, sBuffer, sTitle, MB_OK | MB_ICONWARNING );
}

// End of file
