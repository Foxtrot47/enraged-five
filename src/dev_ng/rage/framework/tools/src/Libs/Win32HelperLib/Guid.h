//
// filename:	Guid.h
// description:	
//
// references: http://www.codeproject.com/KB/COM/GUID_wrapper_class.aspx
//

#ifndef INC_GUID_H_
#define INC_GUID_H_

// --- Include Files ------------------------------------------------------------

// STL headers
#include <string>

// Windows headers
#include <Windows.h>

// --- Defines ------------------------------------------------------------------
#define GUID_STRING_LEN 40


// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

/**
 * @brief Friendly wrapper for GUID objects.
 */
class cGuid
{
public:
	cGuid( );
	cGuid( const char* guid );
	cGuid( const std::string& guid );
	cGuid( const cGuid& guid );
	cGuid( const GUID& guid );

	virtual ~cGuid( );

	cGuid& operator=( const GUID& g );
	cGuid& operator=( const cGuid& g );
	cGuid& operator=( const char* g );
	cGuid& operator=( const std::string& g );
	bool operator==(const GUID& g);
	bool operator==(const cGuid& g);
	bool operator!=(const GUID& g);
	bool operator!=(const cGuid& g);

	operator GUID&();
	operator GUID*();

	bool IsEmpty( ) const;
	std::string ToString( ) const;

	static cGuid Empty( );
	static cGuid Create( );
	static bool Create( GUID& guid );
	static bool Create( const char* str );
	static bool Create( const std::string& str );

protected:
	GUID m_guid;
	void copy(const cGuid& g); 
	void copy(const GUID& g);
};

// --- Globals ------------------------------------------------------------------

#endif // !INC_GUID_H_
