//
// filename:	Win32MsgBox.h
// author:		David Muir
// date:		25 April 2007
// description:	Friendly wrapper around Win32's MessageBox function
//

#ifndef INC_WIN32MSGBOX_H_
#define INC_WIN32MSGBOX_H_

// --- Include Files ------------------------------------------------------------

// STL headers
#include <string>

// --- Globals ------------------------------------------------------------------

void DisplayMessage( const char* sTitle, const char* sFormat, ... );
void DisplayError( const char* sTitle, const char* sFormat, ... );
void DisplayWarning( const char* sTitle, const char* sFormat, ... );

#endif // !INC_WIN32MSGBOX_H_

// End of file
