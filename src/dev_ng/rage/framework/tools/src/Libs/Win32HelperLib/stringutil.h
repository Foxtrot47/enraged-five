//
// filename:	stringutil.h
// description:	Some STL string and C string functions.
//

#ifndef INC_STRINGUTIL_H_
#define INC_STRINGUTIL_H_

// --- Include Files ------------------------------------------------------------

// STL headers
#include <string>
#include <cstring>

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------

inline void 
trim_right( std::string& source, const std::string& t ) 
{
	source.erase(source.find_last_not_of(t)+1);
}

inline void 
trim_left( std::string& source, const std::string& t ) 
{
	source.erase(0, source.find_first_not_of(t));
}

inline void
trim( std::string& source, const std::string& t )
{
	trim_left( source, t );
	trim_right( source, t );
}

inline bool
is_printable( const std::string& source )
{
	if ( 0 == source.length() )
		return ( false );

	for ( size_t n = 0; n < source.length(); ++n )
	{
		if ( !isprint( source[n] ) )
			return ( false );
	}
	return ( true );
}

#endif // !INC_STRINGUTIL_H_
