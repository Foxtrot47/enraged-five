//
// filename:	Win32Assert.cpp
// author:		David Muir
// date:		2 June 2008
// description:	Win32 Assertion Message Macros
//

#include "Win32Assert.h"

bool 
AssertQuery( const char* exp, const char* file, unsigned line, char* ignored ) 
{
	if ( ignored && *ignored )
		return false;
	char text[ASSERTMSG_BUFFER_SIZE];

	sprintf_s( text, ASSERTMSG_BUFFER_SIZE,
		"%s\nAssertion %s failed on line %u of file %s.\n\n"
		"Select Abort to abort now.\n"
		"Select Retry to pass assertion just this once.\n"
		"Select Ignore to pass assertion from now on.\n\n"
		"Please don't report any crash bugs associated with assertions being ignored.",
		g_AssertText, exp, line, file );

	return true;
}

int 
AssertFailed( const char* exp, const char* file, int line, char* ignored ) 
{
	if ( ignored && *ignored )
		return 1;
	bool firstTime = ignored && !*ignored;

	// Display the assertion text on first ignore so that we have a better clue if something crashes later.
	if ( firstTime || !ignored )
	{
		DisplayError( "Assert", "Assert(%s) failed in [%s, line %d].", exp, file, line );
	}

	if ( AssertQuery( exp, file, line, ignored ) )
	{
		return 0;
	}
	else
	{
		if ( firstTime )
		{
			DisplayError( "Assert", "IGNORED %s", *ignored ? "FOREVER":"ONCE" );
		}
		return 1;
	}
}

int 
AssertText(const char * fmt,...) 
{
	va_list args;
	va_start( args, fmt );
	if ( fmt )
		vsprintf_s( g_AssertText, ASSERTMSG_BUFFER_SIZE, fmt, args );
	else
		g_AssertText[0] = 0;
	va_end(args);
	return 0;
}
