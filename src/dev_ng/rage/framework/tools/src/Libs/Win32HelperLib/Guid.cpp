//
// filename:	Guid.cpp
// description:	
//

// --- Include Files ------------------------------------------------------------

// Win32HelperLib headers
#include "Guid.h"
#include "stringutil.h"

// Windows headers
#include <Rpc.h>

// STL headers
#include <cassert>
#include <cstdlib>

// --- Libs Files ---------------------------------------------------------------
#pragma comment( lib, "Rpcrt4.lib" )

// --- Defines ------------------------------------------------------------------

// --- Constants ----------------------------------------------------------------

// --- Structure/Class Definitions ----------------------------------------------

// --- Globals ------------------------------------------------------------------

// --- Static Globals -----------------------------------------------------------

// --- Static class members -----------------------------------------------------

// --- Code ---------------------------------------------------------------------

cGuid::cGuid( )
{
	m_guid = GUID_NULL;
}

cGuid::~cGuid()
{
}

cGuid::cGuid( const cGuid& guid )
{
	m_guid = guid.m_guid; 
}

cGuid::cGuid( const GUID& guid )
{
	m_guid = guid;
}

bool cGuid::operator==(const GUID& g)
{
	return !::memcmp(&m_guid, &g, sizeof(GUID));
}

bool cGuid::operator==(const cGuid& g)
{ 
	return operator==( g.m_guid );
}

cGuid::operator GUID&( )
{ 
	return ( m_guid );
}

cGuid::operator GUID*( )
{ 
	return ( &m_guid ); 
}

bool cGuid::operator!=( const GUID& g )
{
	return ::memcmp(&m_guid, &g, sizeof(GUID)) != 0;
}

bool cGuid::operator!=( const cGuid& g )
{
	return operator!=(g.m_guid);
}

cGuid& cGuid::operator=( const GUID& g )
{
	if( ::memcmp(&m_guid, &g, sizeof(GUID)) != 0 )
	{
		copy(g);
	}
	return *this;
}

cGuid& cGuid::operator=(const cGuid& g)
{
	if ( this != &g )
	{
		copy(g.m_guid);
	}

	return *this;
}

inline void cGuid::copy(const cGuid& g)
{
	copy(g.m_guid);
}
inline void cGuid::copy(const GUID& g)
{
	::memcpy(&m_guid, (void*)&g, sizeof(GUID));
}

bool 
cGuid::IsEmpty( ) const
{
	return ( cGuid::Empty() == *this );
}

std::string 
cGuid::ToString( ) const
{
	OLECHAR szGuid[GUID_STRING_LEN]={0};
	char sGuid[GUID_STRING_LEN]={0};
	::StringFromGUID2( m_guid, szGuid, GUID_STRING_LEN );

	// String conversion
	wcstombs( sGuid, szGuid, GUID_STRING_LEN );
	std::string guid( sGuid );
	trim_left( guid, "{" );
	trim_right( guid, "}" );

	return ( guid );
}

cGuid 
cGuid::Empty( )
{
	return ( cGuid( GUID_NULL ) );
}

cGuid 
cGuid::Create( )
{
	GUID guid;
	::CoCreateGuid( &guid );

	return ( cGuid( guid ) );
}

bool 
cGuid::Create( GUID& guid )
{
	return ::UuidCreate(&guid) == RPC_S_OK;
}

