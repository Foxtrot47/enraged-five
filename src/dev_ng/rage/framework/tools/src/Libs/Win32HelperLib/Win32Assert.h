//
// filename:	Win32Assert.h
// author:		David Muir
// date:		23 April 2008
// description:	Win32 Assertion Message Macros
//

#ifndef INC_WIN32ASSERT_H_
#define INC_WIN32ASSERT_H_

// --- Include Files ------------------------------------------------------------

// Local Header Files
#include "Win32MsgBox.h"

#include <Windows.h>

#include <cstdarg>
#include <cstdlib>

// --- Defines ------------------------------------------------------------------

#define ASSERTMSG_BUFFER_SIZE (4096L)

#define AssertMsgf(x,args)	do { static char ignored; (void)((x) || AssertText args || AssertFailed(#x,__FILE__,__LINE__,&ignored) || (__debugbreak(),0)); } while (0)
#define AssertMsg(x,msg)	AssertMsgf(x,("%s",msg))

// --- Globals ------------------------------------------------------------------

static char g_AssertText[ASSERTMSG_BUFFER_SIZE];

bool AssertQuery( const char* exp, const char* file, unsigned line, char *ignored );
int AssertFailed( const char* exp, const char* file, int line, char* ignored );
int AssertText( const char* fmt, ... );

#endif // !INC_WIN32ASSERT_H_

// End of file
