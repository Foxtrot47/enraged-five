﻿using System;
using System.Diagnostics;
using System.Xml;
using RSG.Base.Configuration;
using RSG.Base.Logging;

namespace RSG.SceneXml.MapExport.AssetCombine
{

    /// <summary>
    /// 
    /// </summary>
    public class DrawableDictionary : AssetOutput
    {
        #region Enumerations
        /// <summary>
        /// LOD level.
        /// </summary>
        public enum LodLevel
        {
            HD = 0,
            LOD = 1,
            SLOD1 = 2,
            SLOD2 = 3,
            SLOD3 = 4,
            SLOD4 = 5   // Hmm
        }
        #endregion // Enumerations

        #region Properties and Associated Member Data
        /// <summary>
        /// Associated TXD name.
        /// </summary>
        public String TXD
        {
            get;
            set;
        }

        /// <summary>
        /// The LOD Level that this Drawable Dictionary is for. 
        /// </summary>
        public LodLevel LOD
        {
            get;
            set;
        }

        public bool IsNewDlc { get; private set; }
        #endregion // Properties and Associated Member Data

        #region Construtor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public DrawableDictionary(String name, bool isNewDlc = false)
            : this(name, String.Empty, LodLevel.HD, isNewDlc)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public DrawableDictionary(String name, String txd, LodLevel lod, bool isNewDlc = false)
            : base(name)
        {
            this.TXD = txd;
            this.LOD = lod;
            IsNewDlc = isNewDlc;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="projectIsDlc"></param>
        /// <returns></returns>
        public override XmlElement ToXml(XmlDocument xmlDoc, bool projectIsDlc)
        {
            XmlElement xmlAsset = ToXml(xmlDoc, "DrawableDictionary", projectIsDlc);
            xmlAsset.SetAttribute("txd", this.TXD);
            xmlAsset.SetAttribute("lod", this.LOD.ToString());
            xmlAsset.SetAttribute("isNewDlc", IsNewDlc.ToString());
            return (xmlAsset);
        }

        protected override XmlElement ToXml(XmlDocument xmlDoc, string name, bool projectIsDlc)
        {
            XmlElement xmlElem = xmlDoc.CreateElement(name);
            xmlElem.SetAttribute("name", this.Name);
            XmlElement xmlInputs = xmlDoc.CreateElement("Inputs");
            foreach (AssetInput input in this.Inputs)
            {
                XmlElement xmlInput = input.ToXml(xmlDoc);
                xmlInputs.AppendChild(xmlInput);
                if (projectIsDlc && !input.IsNewDLC)
                {
                    XmlComment comment = xmlDoc.CreateComment(xmlInput.OuterXml);
                    xmlInputs.ReplaceChild(comment, xmlInput);
                    //xmlInputs.AppendChild(comment);
                }
            }
            xmlElem.AppendChild(xmlInputs);

            return (xmlElem);
        }
        #endregion // Controller Methods

        #region Static Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="xmlElem"></param>
        /// <returns></returns>
        public static DrawableDictionary Create(IBranch branch, XmlElement xmlElem)
        {
            Debug.Assert(xmlElem.Name.Equals("DrawableDictionary"), "Invalid XML.");
            if (!xmlElem.Name.Equals("DrawableDictionary"))
            {
                Log.Log__Error("Invalid XML constructing DrawableDictionary ({0}).", xmlElem.Name);
                return (null);
            }

            String name = xmlElem.GetAttribute("name");
            String txd = xmlElem.GetAttribute("txd");
            String lod = xmlElem.GetAttribute("lod");
            
            bool isNewDlc;
            bool.TryParse(xmlElem.GetAttribute("isNewDlc"), out isNewDlc);

            LodLevel lodlevel;
            Enum.TryParse(lod, out lodlevel);

            XmlNodeList xmlInputsList = xmlElem.SelectNodes("Inputs/Input");
            DrawableDictionary drawableDict = new DrawableDictionary(name, txd, lodlevel, isNewDlc);
            foreach (XmlNode xmlInput in xmlInputsList)
            {
                AssetInput input = AssetInput.Create(branch, xmlInput as XmlElement);
                drawableDict.AddInput(input);
            }

            return (drawableDict);
        }

        /// <summary>
        /// Convert ObjectDef.LodLevel into DrawableDictionary.LodLevel :)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static LodLevel GetLodLevel(TargetObjectDef obj)
        {
            switch (obj.LODLevel)
            {
                case ObjectDef.LodLevel.LOD:
                    return (LodLevel.LOD);
                case ObjectDef.LodLevel.SLOD1:
                    return (LodLevel.SLOD1);
                case ObjectDef.LodLevel.SLOD2:
                    return (LodLevel.SLOD2);
                case ObjectDef.LodLevel.SLOD3:
                    return (LodLevel.SLOD3);
                case ObjectDef.LodLevel.HD:
                case ObjectDef.LodLevel.ORPHANHD:
                default:
                    return (LodLevel.HD);
            }
        }
        #endregion // Static Controller Methods
    }

} // RSG.SceneXml.MapExport.AssetCombine namespace
