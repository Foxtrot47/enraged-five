﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.SceneXml.MapExport
{
    
    /// <summary>
    /// Extension represents a map object extension (e.g. 2dfx types).
    /// </summary>
    public class Extension :
        IComparable,
        IComparable<Extension>
    {        
        #region Properties and Associated Member Data
        /// <summary>
        /// Associated SceneXml ObjectDef object.
        /// </summary>
        public TargetObjectDef Object
        {
            get;
            protected set;
        }

        /// <summary>
        /// Parent.
        /// </summary>
        public TargetObjectDef Parent
        {
            get;
            protected set;
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="obj"></param>
        public Extension(TargetObjectDef obj, TargetObjectDef parent)
        {
            this.Object = obj;
            this.Parent = parent;
        }
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="obj"></param>
        public Extension(TargetObjectDef obj)
        {
            this.Object = obj;
            this.Parent = obj.Parent;
        }
        #endregion // Constructor(s)

        #region IComparable and IComparable<Archetype> Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        int IComparable.CompareTo(Object obj)
        {
            if (obj is Extension)
                return (CompareTo(obj as Extension));
            throw new ArgumentException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(Extension obj)
        {
            return (this.Object.Name.CompareTo(obj.Object.Name));
        }
        #endregion // IComparable and IComparable<Archetype> Methods
    }

} // RSG.SceneXml.MapExport namespace
