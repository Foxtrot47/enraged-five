﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using RSG.SceneXml.MapExport.Framework;
using RSG.SceneXml.MapExport.Project.GTA5.Collections;

namespace RSG.SceneXml.MapExport.Project.GTA5.Algorithm
{

    /// <summary>
    /// Basic spliting utility functions.
    /// </summary>
    internal static class SplitUtils
    {

        /// <summary>
        /// Repackage an IMAPContainer's entities into a list of ObjectDef's.
        /// </summary>
        /// <param name="inputContainer"></param>
        /// <param name="outputContainer"></param>
        internal static void RepackageIMAPContainerEntities(
            IMAPContainer inputContainer,
            out List<TargetObjectDef> outputContainer)
        {
            outputContainer = new List<TargetObjectDef>();
            foreach (Entity entity in inputContainer.Entities)
                outputContainer.Add(entity.Object);
        }

        /// <summary>
        /// Repackage an IMAP container to be based on the SLOD/LOD/Orphan HD
        /// entities as we often will use those for categories.  
        /// 
        /// The output container will include LOD entities, and orphan HD 
        /// entities only.
        /// 
        /// This will ensure that any split algorithm keeps LOD tree siblings
        /// together; when they move aorund the LODs etc will.
        /// </summary>
        /// <param name="inputContainer"></param>
        /// <param name="outputContainer"></param>
        internal static void RepackageContainer(
            IMAPContainer inputContainer, 
            out IMAPContainer outputContainer)
        {
            outputContainer = new IMAPContainer();
            foreach (Entity e in inputContainer.Entities)
            {
                fwEntityDef_eLodTypes lodType = LodLevel.Get(e);
                switch (lodType)
                {
                    case fwEntityDef_eLodTypes.LODTYPES_DEPTH_HD:
                        Debug.Assert(null != e.Parent,
                            String.Format("Internal error: HD object {0} has no parent entity.",
                            e.Object.Name));
                        if (null != e.Parent)
                        {
                            if(!outputContainer.ContainsEntity(e.Parent))
                            {
                                outputContainer.AddEntity(e.Parent);
                            }
                        }
                        else if (!outputContainer.ContainsEntity(e))
                            outputContainer.AddEntity(e);
                        break;
                    case fwEntityDef_eLodTypes.LODTYPES_DEPTH_ORPHANHD:
                        outputContainer.AddEntity(e);
                        break;
                    default:
                        // Do not include.
                        break;
                }
            }
        }
    }

} // RSG.SceneXml.MapExport.Project.GTA.Algorithm
