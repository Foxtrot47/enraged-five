﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.SceneXml.MapExport
{
    public class InstancedEntityData
    {
        #region Properties
        public String IMAPLink
        {
            get;
            set;
        }

        /// <summary>
        /// Number of ordered splits on next-gen consoles.
        /// </summary>
        public int NextGenSplitSize
        {
            get;
            set;
        }

        /// <summary>
        /// List of platform strings we want to re-order the instance lists for.
        /// </summary>
        public List<String> NextGenPlatformSplits
        {
            get;
            set;
        }

        /// <summary>
        /// Grass entities.
        /// </summary>
        public Dictionary<String, List<InstancedEntityList>> GrassEntitiesList
        {
            get;
            protected set;
        }

        /// <summary>
        /// Prop entities.
        /// </summary>
        public Dictionary<String, List<InstancedEntityList>> TreeEntitiesList
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="obj"></param>
        public InstancedEntityData()
        {
            this.NextGenSplitSize = 8;
            this.NextGenPlatformSplits = new List<String>();
            this.GrassEntitiesList = new Dictionary<String, List<InstancedEntityList>>();
            this.TreeEntitiesList = new Dictionary<String, List<InstancedEntityList>>();
        }
        #endregion // Constructor(s)
    }
}
