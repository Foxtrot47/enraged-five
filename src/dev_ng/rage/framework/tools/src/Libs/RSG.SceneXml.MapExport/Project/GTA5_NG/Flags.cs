﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Linq;
using RSG.Metadata.Util;
using RSG.SceneXml.MapExport.AssetCombine;

namespace RSG.SceneXml.MapExport.Project.GTA5_NG
{
    
    #region CBaseArchetype Flags
    /// <summary>
    /// CBaseArchetype_Flags Flags Enumeration
    /// </summary>
    [Flags]
    public enum CBaseArchetype_Flags
    {
        F_NONE = 0,
        F_WET_ROAD_REFLECTION = (1 << 0),
        F_DONT_FADE = (1 << 1),
        F_DRAW_LAST = (1 << 2),
        F_CLIMBABLE_BY_AI = (1 << 3),
        F_SUPPRESS_HD_TXDS = (1 << 4),
        F_IS_FIXED = (1 << 5),
        F_DONT_WRITE_ZBUFFER = (1 << 6),
        F_TOUGHFORBULLETS = (1 << 7),
        F_IS_GENERIC = (1 << 8),
        F_HAS_ANIM = (1 << 9),
        F_HAS_UVANIM = (1 << 10),
        F_SHADOW_ONLY = (1 << 11),
        F_DAMAGE_MODEL = (1 << 12),
        F_DONT_CAST_SHADOWS = (1 << 13),
        F_CAST_TEXTURE_SHADOWS = (1 << 14),
        F_DONT_COLLIDE_WITH_FLYER = (1 << 15),
        F_IS_TREE = (1 << 16),
        F_IS_TYPE_OBJECT = (1 << 17),
        F_OVERRIDE_PHYSICS_BOUNDS = (1 << 18),
        F_AUTOSTART_ANIM = (1 << 19),
        F_HAS_PRE_REFLECTED_WATER_PROXY = (1 << 20),
        F_HAS_DRAWABLE_PROXY_FOR_WATER_REFLECTIONS = (1 << 21),
        F_DOES_NOT_PROVIDE_COVER = (1 << 22),
        F_DOES_NOT_PROVIDE_PLAYER_COVER = (1 << 23),
        F_IS_LADDER = (1 << 24),
        F_HAS_CLOTH = (1 << 25),
        F_DOOR_PHYSICS = (1 << 26),
        F_IS_FIXED_FOR_NAVIGATION = (1 << 27),
        F_DONT_AVOID_BY_PEDS = (1 << 28),
        F_USE_AMBIENT_SCALE = (1 << 29),
        F_IS_DEBUG = (1 << 30),
        F_HAS_ALPHA_SHADOW = (1 << 31)
    }

    /// <summary>
    /// Archetype flags methods.
    /// </summary>
    internal static class ArchetypeFlags
    {
        /// <summary>
        /// Initialise fwArchetype_Flags from Archetype object.
        /// </summary>
        /// <param name="flags"></param>
        /// <param name="archetype"></param>
        internal static CBaseArchetype_Flags Get(Archetype archetype)
        {
            CBaseArchetype_Flags flags = CBaseArchetype_Flags.F_NONE;
            
            if (archetype.Object.GetAttribute(AttrNames.OBJ_LIGHT_REFLECTION, AttrDefaults.OBJ_LIGHT_REFLECTION))
                flags |= CBaseArchetype_Flags.F_WET_ROAD_REFLECTION;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_DO_NOT_FADE, AttrDefaults.OBJ_DO_NOT_FADE))
                flags |= CBaseArchetype_Flags.F_DONT_FADE;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_DRAW_LAST, AttrDefaults.OBJ_DRAW_LAST))
                flags |= CBaseArchetype_Flags.F_DRAW_LAST;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_CLIMBABLE_BY_AI, AttrDefaults.OBJ_CLIMBABLE_BY_AI))
                flags |= CBaseArchetype_Flags.F_CLIMBABLE_BY_AI;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_DONT_WRITE_ZBUFFER, AttrDefaults.OBJ_DONT_WRITE_ZBUFFER))
                flags |= CBaseArchetype_Flags.F_DONT_WRITE_ZBUFFER;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_TOUGH_FOR_BULLETS, AttrDefaults.OBJ_TOUGH_FOR_BULLETS))
                flags |= CBaseArchetype_Flags.F_TOUGHFORBULLETS;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_HAS_ANIM, AttrDefaults.OBJ_HAS_ANIM))
                flags |= CBaseArchetype_Flags.F_HAS_ANIM;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_HAS_UV_ANIM, AttrDefaults.OBJ_HAS_UV_ANIM))
                flags |= CBaseArchetype_Flags.F_HAS_UVANIM;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_SHADOW_ONLY, AttrDefaults.OBJ_SHADOW_ONLY))
                flags |= CBaseArchetype_Flags.F_SHADOW_ONLY;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_DONT_CAST_SHADOWS, AttrDefaults.OBJ_DONT_CAST_SHADOWS))
                flags |= CBaseArchetype_Flags.F_DONT_CAST_SHADOWS;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_CAST_TEXTURE_SHADOWS, AttrDefaults.OBJ_CAST_TEXTURE_SHADOWS))
                flags |= CBaseArchetype_Flags.F_CAST_TEXTURE_SHADOWS;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_DONT_COLLIDE_WITH_FLYER, AttrDefaults.OBJ_DONT_COLLIDE_WITH_FLYER))
                flags |= CBaseArchetype_Flags.F_DONT_COLLIDE_WITH_FLYER;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_IS_TREE, AttrDefaults.OBJ_IS_TREE))
                flags |= CBaseArchetype_Flags.F_IS_TREE;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_IS_DYNAMIC, AttrDefaults.OBJ_IS_DYNAMIC))
                flags |= CBaseArchetype_Flags.F_IS_TYPE_OBJECT;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_IS_FIXED, AttrDefaults.OBJ_IS_FIXED))
                flags |= CBaseArchetype_Flags.F_IS_FIXED;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_DOES_NOT_PROVIDE_AI_COVER, AttrDefaults.OBJ_DOES_NOT_PROVIDE_AI_COVER))
                flags |= CBaseArchetype_Flags.F_DOES_NOT_PROVIDE_COVER;
            if (archetype.SuppressHDTextures)
                flags |= CBaseArchetype_Flags.F_SUPPRESS_HD_TXDS;

            // Jimmy bug #7865, force set "Is Dynamic" automatically for "Bendy Plants".
            if (ObjAttributeValues.Bendy_Plant == (ObjAttributeValues)archetype.Object.GetAttribute(AttrNames.OBJ_ATTRIBUTE, AttrDefaults.OBJ_ATTRIBUTE))
                flags |= CBaseArchetype_Flags.F_IS_TYPE_OBJECT;

            bool reverse = archetype.Object.GetAttribute(AttrNames.OBJ_REVERSE_COVER_FOR_PLAYER, AttrDefaults.OBJ_REVERSE_COVER_FOR_PLAYER);
            bool cover = flags.HasFlag(CBaseArchetype_Flags.F_DOES_NOT_PROVIDE_COVER);
            cover = reverse ? !cover : cover;
            if (cover)
                flags |= CBaseArchetype_Flags.F_DOES_NOT_PROVIDE_PLAYER_COVER;

            if (archetype.Object.GetAttribute(AttrNames.OBJ_IS_LADDER, AttrDefaults.OBJ_IS_LADDER))
                flags |= CBaseArchetype_Flags.F_IS_LADDER;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_HAS_DOOR_PHYSICS, AttrDefaults.OBJ_HAS_DOOR_PHYSICS))
                flags |= CBaseArchetype_Flags.F_DOOR_PHYSICS;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_IS_FIXED_FOR_NAVIGATION, AttrDefaults.OBJ_IS_FIXED_FOR_NAVIGATION))
                flags |= CBaseArchetype_Flags.F_IS_FIXED_FOR_NAVIGATION;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_DONT_AVOID_BY_PEDS, AttrDefaults.OBJ_DONT_AVOID_BY_PEDS))
                flags |= CBaseArchetype_Flags.F_DONT_AVOID_BY_PEDS;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_ENABLE_AMBIENT_MULTIPLIER, AttrDefaults.OBJ_ENABLE_AMBIENT_MULTIPLIER))
                flags |= CBaseArchetype_Flags.F_USE_AMBIENT_SCALE;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_IS_DEBUG, AttrDefaults.OBJ_IS_DEBUG))
                flags |= CBaseArchetype_Flags.F_IS_DEBUG;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_OVERRIDE_PHYSICS_BOUNDS, AttrDefaults.OBJ_OVERRIDE_PHYSICS_BOUNDS))
                flags |= CBaseArchetype_Flags.F_OVERRIDE_PHYSICS_BOUNDS;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_AUTOSTART_ANIM, AttrDefaults.OBJ_AUTOSTART_ANIM))
                flags |= CBaseArchetype_Flags.F_AUTOSTART_ANIM;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_HAS_PRE_REFLECTED_WATER_PROXY, AttrDefaults.OBJ_HAS_PRE_REFLECTED_WATER_PROXY))
                flags |= CBaseArchetype_Flags.F_HAS_PRE_REFLECTED_WATER_PROXY;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_HAS_DRAWABLE_PROXY_FOR_WATER_REFLECTIONS, AttrDefaults.OBJ_HAS_DRAWABLE_PROXY_FOR_WATER_REFLECTIONS))
                flags |= CBaseArchetype_Flags.F_HAS_DRAWABLE_PROXY_FOR_WATER_REFLECTIONS;
            if (archetype.Object.GetAttribute(AttrNames.OBJ_HAS_ALPHA_SHADOW, AttrDefaults.OBJ_HAS_ALPHA_SHADOW))
                flags |= CBaseArchetype_Flags.F_HAS_ALPHA_SHADOW;
            if (archetype.HasCloth)
                flags |= CBaseArchetype_Flags.F_HAS_CLOTH;

            return (flags);
        }

        /// <summary>
        /// Serialise ::rage::fwArchetype flags to XML.
        /// </summary>
        /// <param name="flags"></param>
        /// <param name="doc"></param>
        /// <returns></returns>
        internal static XElement AsXml(this CBaseArchetype_Flags flags)
        {
            XElement xmlItem = Xml.CreateElementWithValueAttribute(null, "flags", flags.ToString("d"));
            return (xmlItem);
        }
    }
    #endregion // CBaseArchetype Flags

    #region SpawnPoint Availability
    /// <summary>
    /// Matches the strings in CSpawnPoint::AvailabilityMpSp enum and the ints
    /// defined in the attribute system.
    /// </summary>
    public enum SpawnPointAvailability
    {
        kBoth = 0,
        kOnlySp = 1,
        kOnlyMp = 2,
    }
    #endregion // SpawnPoint Availability

    #region SpawnPoint Flags
    /// <summary>
    /// Matches the CScenarioPointFlags runtime flags.
    /// </summary>
    [Flags]
    public enum CScenarioPointFlags
    {
        None = 0,
        StationaryReactions = (1 << 0),
        NoSpawn = (1 << 1),
    }

    /// <summary>
    /// Spawn point flag methods.
    /// </summary>
    internal static class SpawnPointFlags
    {
        /// <summary>
        /// Return CScenarioPointFlags for an object.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        internal static CScenarioPointFlags Get(TargetObjectDef obj)
        {
            CScenarioPointFlags flags = CScenarioPointFlags.None;

            if (obj.GetAttribute(AttrNames.SPAWNPOINTFLAGS_STATIONARY_REACTIONS, AttrDefaults.SPAWNPOINTFLAGS_STATIONARY_REACTIONS))
                flags |= CScenarioPointFlags.StationaryReactions;
            if (obj.GetAttribute(AttrNames.SPAWNPOINTFLAGS_NO_SPAWN, AttrDefaults.SPAWNPOINTFLAGS_NO_SPAWN))
                flags |= CScenarioPointFlags.NoSpawn;

            return (flags);
        }

        /// <summary>
        /// Return string representation for Metadata.
        /// </summary>
        /// <param name="flags"></param>
        /// <returns></returns>
        internal static String GetEnumString(TargetObjectDef obj)
        {
            List<String> flags = new List<String>();

            if (obj.GetAttribute(AttrNames.SPAWNPOINTFLAGS_STATIONARY_REACTIONS, AttrDefaults.SPAWNPOINTFLAGS_STATIONARY_REACTIONS))
                flags.Add(CScenarioPointFlags.StationaryReactions.ToString());
            if (obj.GetAttribute(AttrNames.SPAWNPOINTFLAGS_NO_SPAWN, AttrDefaults.SPAWNPOINTFLAGS_NO_SPAWN))
                flags.Add(CScenarioPointFlags.NoSpawn.ToString());

            return (String.Join(" | ", flags));
        }
    }
    #endregion // SpawnPoint Flags

    #region ArchetypeDef eAssetType
    public enum fwArchetypeDef_eAssetType
    {
        ASSET_TYPE_UNINITIALIZED      = 0,
        ASSET_TYPE_FRAGMENT           = 1,
        ASSET_TYPE_DRAWABLE           = 2,
        ASSET_TYPE_DRAWABLEDICTIONARY = 3,
        ASSET_TYPE_ASSETLESS          = 4
    }

    internal static class AssetType
    {
        internal static fwArchetypeDef_eAssetType GetAssetType(this Archetype archetype, AssetFile assetFile, ArchetypeLite coreArchetype)
        {
            if(archetype.Object.IsFragment())
                return fwArchetypeDef_eAssetType.ASSET_TYPE_FRAGMENT;

            bool isNewDlcObject = Archetype.Branch.Project.ForceFlags.AllNewContent || archetype.Object.IsNewDLCObject();
            if (Archetype.Branch.Project.IsDLC && !isNewDlcObject)
            {
                if (null != coreArchetype)
                {
                    // Pull data from core archetype.
                    if (String.IsNullOrEmpty(coreArchetype.ModelDictionary))
                        return (fwArchetypeDef_eAssetType.ASSET_TYPE_DRAWABLE);
                    else
                        return (fwArchetypeDef_eAssetType.ASSET_TYPE_DRAWABLEDICTIONARY);
                }
                else
                {
                    var sceneType = archetype.Object.MyScene.SceneType;
                    if (sceneType == SceneType.EnvironmentInterior || sceneType == SceneType.EnvironmentProps)
                    {
                        // special case here. We dont have core metadata for interiors or props yet;
                        // we consider them as "need to be fully serialised as they are", even if we cant find a core reference
                        // ASSET_TYPE_FRAGMENT is already covered as the top level case, so it has to be a DRAWABLE
                        return fwArchetypeDef_eAssetType.ASSET_TYPE_DRAWABLE;
                    }
                    
                    // else, it's a genuine "not found in core" case, so let's warn even if no one reads warnings.
                    RSG.Base.Logging.Log.StaticLog.WarningCtx(archetype.Name,
                        "Archetype '{0}' found and not flagged as 'New DLC' but it doesn't have a core game archetype.  Should it be flagged as 'New DLC'?",
                        archetype.Name);
                }
            }
            else
            {
                switch (archetype.Object.LODLevel)
                {
                    case ObjectDef.LodLevel.HD:
                    case ObjectDef.LodLevel.ORPHANHD:
                    case ObjectDef.LodLevel.UNKNOWN:
                        return fwArchetypeDef_eAssetType.ASSET_TYPE_DRAWABLE;

                    case ObjectDef.LodLevel.SLOD4:
                    case ObjectDef.LodLevel.SLOD3:
                    case ObjectDef.LodLevel.SLOD2:
                    case ObjectDef.LodLevel.SLOD1:
                    case ObjectDef.LodLevel.LOD:
                        if (assetFile != null &&
                            assetFile.InputDrawableDictionaries != null &&
                            assetFile.InputDrawableDictionaries.ContainsKey(archetype.RawName))
                        {
                            return fwArchetypeDef_eAssetType.ASSET_TYPE_DRAWABLEDICTIONARY;
                        }
                        return fwArchetypeDef_eAssetType.ASSET_TYPE_DRAWABLE;
                }
            }
            return fwArchetypeDef_eAssetType.ASSET_TYPE_ASSETLESS;
        }

        internal static XElement AsXml(this fwArchetypeDef_eAssetType assetType)
        {
            return new XElement("assetType", assetType.ToString());
        }
    }
    #endregion
} 
