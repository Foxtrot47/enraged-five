﻿using System;
using System.Collections.Generic;
using RSG.SceneXml.MapExport.Algorithm;
using RSG.SceneXml.MapExport.Project.GTA5.Collections;

namespace RSG.SceneXml.MapExport.Project.GTA5.Algorithm
{

    /// <summary>
    /// Naive entity cutting algorithm to get us started.
    /// </summary>
    public class IMAPSplitNaive : IIMAPSplitAlgorithm<IMAPContainer>
    {
        #region Constants
        const int MAXIMUM_ENTITIES_PER_FILE = 450;
        const int MINIMUM_ENTITIES_PER_FILE = 50;
        #endregion // Constants

        #region IIMAPSplitAlgorithm Methods
        /// <summary>
        /// Split the larger container into a series of smaller containers.
        /// </summary>
        /// <param name="container"></param>
        /// <param name="containers"></param>
        public void Split(String prefix, IMAPContainer container, out List<IMAPContainer> containers, bool warnMissingCarGens, Entity.StreamModeType streamMode)
        {
            containers = new List<IMAPContainer>();
            if (0 == container.Entities.Count)
                return;

            containers.Add(container);
        }
        #endregion // IIMAPSplitAlgorithm Methods
    }

} // RSG.SceneXml.MapExport.Project.GTA/Algorithm namespace
