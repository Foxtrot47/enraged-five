﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace RSG.SceneXml.MapExport.AssetCombine
{

    /// <summary>
    /// Abstraction of Asset Combine Processor output data.
    /// </summary>
    public abstract class AssetOutput
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Output name.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Output directory.
        /// </summary>
        /// TODO FLo : NO. Creating an array each time we call a getter is bad. No Twinkie.
        public AssetInput[] Inputs
        {
            get { return (m_Inputs.ToArray()); }
        }
        private List<AssetInput> m_Inputs;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        public AssetOutput(String name)
        {
            this.Name = name;
            this.m_Inputs = new List<AssetInput>();
        }

        /// <summary>
        /// Constructor from series of inputs.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="inputs"></param>
        public AssetOutput(String name,  AssetInput[] inputs)
        {
            this.Name = name;
            this.m_Inputs = new List<AssetInput>();
            AddInputRange(inputs);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Add a single input object.
        /// </summary>
        /// <param name="input"></param>
        public void AddInput(AssetInput input)
        {
            // Any validation required?
            this.m_Inputs.Add(input);
        }

        /// <summary>
        /// Add a range of input objects.
        /// </summary>
        /// <param name="inputs"></param>
        public void AddInputRange(IEnumerable<AssetInput> inputs)
        {
            foreach (AssetInput input in inputs)
                AddInput(input);
        }

        /// <summary>
        /// Determine whether we already have this input defined.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public bool HasInput(String name)
        {
            return m_Inputs.Any(input => String.Equals(input.Name, name, StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// Get a named input (assuming it is defined)
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public AssetInput GetInput(String name)
        {
            return m_Inputs.First(input => String.Equals(input.Name, name, StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="projectIsDlc"></param>
        /// <returns></returns>
        public abstract XmlElement ToXml(XmlDocument xmlDoc, bool projectIsDlc);
        #endregion // Controller Methods

        #region Protected Methods
        /// <summary>
        /// Return an XmlElement representing this output; for serialisation.
        /// </summary>
        /// <returns></returns>
        protected abstract XmlElement ToXml(XmlDocument xmlDoc, String name, bool projectIsDlc);
        #endregion // Protected Methods
    }

} // RSG.SceneXml.MapExport.AssetCombine namespace
