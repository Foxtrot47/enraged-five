//
// File: Exceptions.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of map export exception classes.
//

using System;
using System.Runtime.Serialization;

namespace RSG.SceneXml.MapExport
{

    /// <summary>
    /// Fatal IDE file serialisation error.
    /// </summary>
    public class IDESerialiserFatalError : Exception
    {
        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public IDESerialiserFatalError()
            : base( )
        {
        }
            
        public IDESerialiserFatalError(String sMessage)
            : base(sMessage)
        {
        }
            
        public IDESerialiserFatalError(String sMessage, Exception InnerException)
            : base(sMessage, InnerException)
        {
        }
            
        public IDESerialiserFatalError(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
        #endregion // Constructors
    }
    
    /// <summary>
    /// Fatal IPL file serialisation error.
    /// </summary>
    public class IPLSerialiserFatalError : Exception
    {
        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public IPLSerialiserFatalError()
            : base( )
        {
        }
            
        public IPLSerialiserFatalError(String sMessage)
            : base(sMessage)
        {
        }
            
        public IPLSerialiserFatalError(String sMessage, Exception InnerException)
            : base(sMessage, InnerException)
        {
        }

        public IPLSerialiserFatalError(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
        #endregion // Constructors
    }


    /// <summary>
    /// Fatal IMD file serialisation error.
    /// </summary>
    public class IMDSerialiserFatalError : Exception
    {
        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public IMDSerialiserFatalError()
            : base()
        {
        }

        public IMDSerialiserFatalError(String sMessage)
            : base(sMessage)
        {
        }

        public IMDSerialiserFatalError(String sMessage, Exception InnerException)
            : base(sMessage, InnerException)
        {
        }

        public IMDSerialiserFatalError(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
        #endregion // Constructors
    }

    /// <summary>
    /// Fatal IPL file serialisation error.
    /// </summary>
    public class SerialiserNotReadableError : Exception
    {
        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public SerialiserNotReadableError()
            : base()
        {
        }

        public SerialiserNotReadableError(String sMessage)
            : base(sMessage)
        {
        }

        public SerialiserNotReadableError(String sMessage, Exception InnerException)
            : base(sMessage, InnerException)
        {
        }

        public SerialiserNotReadableError(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
        #endregion // Constructors
    }
} // RSG.SceneXml.MapExport namespace

// Exceptions.cs
