﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.SceneXml;

namespace RSG.SceneXml.MapExport.AssetCombine
{
    
    /// <summary>
    /// Abstraction of Asset Combine Processor input data.
    /// </summary>
    public class AssetInput
    {
        #region Enumerations
        /// <summary>
        /// 
        /// </summary>
        public enum InputType
        {
            Drawable,
            TextureDictionary,
        }
        #endregion // Enumerations

        #region Properties and Associated Member Data
        /// <summary>
        /// Input name.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Input type.
        /// </summary>
        public InputType Type
        {
            get;
            private set;
        }

        /// <summary>
        /// Cache directory (absolute path) to get assets from.
        /// </summary>
        public String CacheDir
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String ZipPathname
        {
            get;
            private set;
        }

        /// <summary>
        /// Whether this input is newly defined in DLC (or not).  This may
        /// cause name remapping.
        /// </summary>
        public bool IsNewDLC
        {
            get; 
            private set;
        }

        /// <summary>
        /// We wont serialise out a renamed txd, but we need to pass down that this input requires a prefixed TXD
        /// </summary>
        public bool IsNewTXD
        {
            get;
            private set;
        }

        /// <summary>
        /// Items from within the input that are required.  If this is empty then all items are required.
        /// </summary>
        public IEnumerable<String> RequiredItems
        {
            get { return requiredItems_; }
        }
        private HashSet<String> requiredItems_;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="cacheDir"></param>
        /// <param name="zipPathname"></param>
        /// <param name="type"></param>
        /// <param name="isNewDLC"></param>
        /// <param name="isNewTxd"></param>
        public AssetInput(string name, string cacheDir, string zipPathname, InputType type, bool isNewDLC, bool isNewTxd)
        {
            this.Name = name;
            this.CacheDir = cacheDir;
            this.ZipPathname = zipPathname;
            this.Type = type;
            this.IsNewDLC = isNewDLC;
            this.IsNewTXD = isNewTxd;
            requiredItems_ = new HashSet<String>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return an XmlElement representing this output; for serialisation.
        /// </summary>
        /// <returns></returns>
        public XmlElement ToXml(XmlDocument xmlDoc)
        {
            XmlElement inputElement = xmlDoc.CreateElement("Input");
            inputElement.SetAttribute("name", this.Name);
            inputElement.SetAttribute("type", this.Type.ToString());
            inputElement.SetAttribute("cachedir", this.CacheDir);
            inputElement.SetAttribute("source", this.ZipPathname);
            inputElement.SetAttribute("is_new_dlc", this.IsNewDLC.ToString());
            inputElement.SetAttribute("is_new_txd", IsNewTXD.ToString());
            if (requiredItems_.Count > 0)
            {
                XmlElement requiredItemsElement = xmlDoc.CreateElement("RequiredItems");
                foreach (String requiredItem in requiredItems_)
                {
                    XmlElement itemElement = xmlDoc.CreateElement("Item");
                    itemElement.InnerText = requiredItem;
                    requiredItemsElement.AppendChild(itemElement);
                }

                inputElement.AppendChild(requiredItemsElement);
            }

            return inputElement;
        }

        /// <summary>
        /// Mark a file within the source as being required
        /// </summary>
        public void AddRequiredItem(String requiredItem)
        {
            requiredItems_.Add(requiredItem);
        }
        #endregion // Controller Methods

        #region Static Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlElem"></param>
        /// <returns></returns>
        public static AssetInput Create(IBranch branch, XmlElement xmlElem)
        {
            Debug.Assert(xmlElem.Name.Equals("Input"), "Invalid XML.");
            if (!xmlElem.Name.Equals("Input"))
            {
                Log.Log__Error("Invalid XML constructing Input ({0}).", xmlElem.Name);
                return (null);
            }

            String name = xmlElem.GetAttribute("name");
            String typeStr = xmlElem.GetAttribute("type");
            InputType type;
            Enum.TryParse(typeStr, out type);
            String cacheDir = xmlElem.GetAttribute("cachedir");
            String sourceFileName = Path.GetFileNameWithoutExtension(xmlElem.GetAttribute("source"));
            bool isNewDlc;
            if (!bool.TryParse(xmlElem.GetAttribute("is_new_dlc"), out isNewDlc))
                isNewDlc = false;

            bool isNewTxd;
            if (!bool.TryParse(xmlElem.GetAttribute("is_new_txd"), out isNewTxd))
                isNewTxd = false;


            AssetInput input = new AssetInput(name, cacheDir, sourceFileName, type, isNewDlc, isNewTxd);
            return (input);
        }
        #endregion // Static Controller Methods
    }

} // RSG.SceneXml.MapExport.AssetCombine namespace
