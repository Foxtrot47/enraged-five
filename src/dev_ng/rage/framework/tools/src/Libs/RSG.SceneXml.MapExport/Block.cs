﻿using System;

namespace RSG.SceneXml.MapExport
{

    /// <summary>
    /// 
    /// </summary>
    public class Block
    {
        #region Properties
        /// <summary>
        /// Associated Scene.
        /// </summary>
        public Scene Scene
        {
            get;
            protected set;
        }

        /// <summary>
        /// Block owner attribute.
        /// </summary>
        public String Owner
        {
            get;
            protected set;
        }

        /// <summary>
        /// Block flags.
        /// </summary>
        public UInt32 Flags
        {
            get;
            protected set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor from SceneXml Scene.
        /// </summary>
        /// <param name="scene"></param>
        public Block(Scene scene)
        {
            this.Scene = scene;

            // Initialise flags from Container object if present.
            this.Flags = 0x00000000;
            foreach (TargetObjectDef o in this.Scene.Objects)
            {
                if (!o.IsContainer())
                    continue;

                this.Flags = RSG.SceneXml.MapExport.Flags.GetBlockFlags(o);
                String owner = o.GetAttribute(AttrNames.BLOCK_OWNER, AttrDefaults.BLOCK_OWNER);
                owner = owner.Replace(" ", "_");
                break;
            }
        }
        #endregion // Constructor(s)
    }

} // RSG.SceneXml.MapExport namespace
