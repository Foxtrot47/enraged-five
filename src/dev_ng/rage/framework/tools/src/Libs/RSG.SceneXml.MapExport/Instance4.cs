//
// File: Instance4.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Instance4 class
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using RSG.Base.Math;
using RSG.SceneXml;
using RSG.ManagedRage;
using SMath = System.Math;

namespace RSG.SceneXml.MapExport
{

	/// <summary>
	/// SceneXml Instance with IPL Version 4 serialisation.
	/// </summary>
	/// As used in GTA4, E1, E2 and for the start of Jimmy; although the 
	/// original GTA3 3dsmax exporter was used for those projects.
	/// 
	public class Instance4 : InstanceBase
	{
		#region Constants
		protected static readonly float sC_s_m_StreamedLongLODDistance = 150.0f;
		protected static readonly float sC_s_m_NonStreamedLODDistance = 300.0f;
		protected static readonly String sC_s_str_BlockDateTimeFormat = "yyyy:MM:dd:hh:mm:ss";
		#endregion // Constants

		#region Constructor(s)
		/// <summary>
		/// Constructor, no associated ObjectDef (e.g. used for anonymous block).
		/// </summary>
		/// <param name="scene">SceneXml.Scene containing instance</param>
		public Instance4(Scene scene)
			: base(scene)
		{
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="scene">SceneXml.Scene containing instance</param>
		/// <param name="obj">SceneXml.ObjectDef for instance</param>
		/// <param name="reader"></param>
		public Instance4(Scene scene, TargetObjectDef obj, Util.IDEReaderBasic reader)
			: base(scene, obj, reader)
		{
			DetermineStreamMode(reader);
		}
		#endregion // Constructor(s)

		#region Object Overrides
		/// <summary>
		/// 
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		public override bool Equals(Object obj)
		{
			if (obj is InstanceBase)
			{
				InstanceBase inst = (obj as InstanceBase);
				return (this.Object.Equals(inst.Object));
			}

			return (false);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			return (String.Format("Instance4: [{0}]{1}", this.Object.Name, this.Object.Guid));
		}
		#endregion // Object Overrides

		#region Controller Methods
		/// <summary>
		/// Return IPL String representation for a "Gta Object" ObjectDef.
		/// Just a regular object instance.
		/// </summary>
		/// <param name="blocks"></param>
		/// <returns></returns>
		/// Format of "Gta Object" line is:
		///   name, flags, tx, ty, tz, qx, qy, qz, qw, parent_idx, block_idx, lod_dist
		///   
		public override String ToIPLInst(String[] blocks)
		{
			Vector3f pos = this.Object.NodeTransform.Translation;
			Quaternionf rot = new Quaternionf(this.Object.NodeTransform);
			bool instloddist = this.Object.GetAttribute(AttrNames.OBJ_INSTANCE_LOD_DISTANCE, AttrDefaults.OBJ_INSTANCE_LOD_DISTANCE);
			float loddist = -1.0f;
			if (instloddist)
				loddist = this.Object.GetLODDistance();
			String block = this.Object.GetAttribute(AttrNames.OBJ_BLOCK_ID, AttrDefaults.OBJ_BLOCK_ID);
			int block_idx = Array.IndexOf(blocks, block) + 1;
			
			String ipl_line = String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}",
				this.Object.GetObjectName(), Flags.GetInstanceFlags(this.Object),
				pos.X, pos.Y, pos.Z, rot.X, rot.Y, rot.Z, rot.W,
				this.LODParentIndex, block_idx, loddist);
			return (ipl_line);
		}

		/// <summary>
		/// Return IPL String representation for a "Gta Block" ObjectDef.
		/// </summary>
		/// <returns></returns>
		/// Format of "Gta Block" line is:
		///   name, user, datetime, flags, #points, x0, y0, ..., xn, yn, owner 
		///   
		/// Note: strange datetime format: "YYYY:MM:dd:hh:mm:ss".
		public override String ToIPLBlock()
		{
			String ipl_line = String.Empty;
			BoundingBox3f bound = this.Object.WorldBoundingBox;
			if (null == bound)
				bound = new BoundingBox3f(new Vector3f(0.0f, 0.0f, 0.0f), new Vector3f(0.0f, 0.0f, 0.0f));
			Vector3f min = bound.Min;
			Vector3f max = bound.Max;

			String owner = this.Object.GetAttribute(AttrNames.BLOCK_OWNER, AttrDefaults.BLOCK_OWNER);

			owner = owner.Replace(" ", "_");

			// Regular named blocks
			ipl_line =
				String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}",
					this.Object.GetObjectName(), 
					this.m_Scene.ExportUser,
					this.m_Scene.ExportTimestamp.ToString(sC_s_str_BlockDateTimeFormat),
					Flags.GetBlockFlags(this.Object), 
					4, 
					min.X, max.Y, max.X, max.Y, max.X, min.Y, min.X, min.Y,
					owner);

			return (ipl_line);
		}

		/// <summary>
		/// Return IPL String representation for a "Gta CarGen" ObjectDef.
		/// </summary>
		/// <returns></returns>
		/// Format of "Gta CarGen" line is:
		///   x, y, z, vec1x, vec1y, vec2length, modelid, colour1, colour2, colour3, colour4, flags, alarmChance, lockedChance
		///
		public override String ToIPLCarGen()
		{
			Vector3f pos = this.Object.NodeTransform.Translation;
			String model = this.Object.GetAttribute(AttrNames.CARGEN_MODEL, AttrDefaults.CARGEN_MODEL);
			uint modelid = (uint)StringHashUtil.atStringHash(model, 0);
			int col1 = this.Object.GetAttribute(AttrNames.CARGEN_COLOUR1, AttrDefaults.CARGEN_COLOUR1);
			int col2 = this.Object.GetAttribute(AttrNames.CARGEN_COLOUR2, AttrDefaults.CARGEN_COLOUR2);
			int col3 = this.Object.GetAttribute(AttrNames.CARGEN_COLOUR3, AttrDefaults.CARGEN_COLOUR3);
			int col4 = this.Object.GetAttribute(AttrNames.CARGEN_COLOUR4, AttrDefaults.CARGEN_COLOUR4);
			int alarmChance = this.Object.GetAttribute(AttrNames.CARGEN_ALARM_CHANCE, AttrDefaults.CARGEN_ALARM_CHANCE);
			int lockChance = this.Object.GetAttribute(AttrNames.CARGEN_LOCKED_CHANCE, AttrDefaults.CARGEN_LOCKED_CHANCE);
			UInt32 flags = Flags.GetCarGenFlags(this.Object);
			float width = this.Object.GetParameter(ParamNames.CARGEN_WIDTH, ParamDefaults.CARGEN_WIDTH);
			float length = this.Object.GetParameter(ParamNames.CARGEN_DEPTH, ParamDefaults.CARGEN_DEPTH);

			Matrix34f mat = this.Object.NodeTransform.Inverse();
			mat.Translation = new Vector3f();
			Vector3f forward = new Vector3f(0.0f, length, 0.0f);
			Vector3f right = new Vector3f(width, 0.0f, 0.0f);
			forward = mat * forward;
			right = mat * right;

			String ipl_line = String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}",
				pos.X, pos.Y, pos.Z, forward.X, forward.Y, right.Magnitude(),
				modelid, col1, col2, col3, col4, flags, alarmChance, lockChance);

			return (ipl_line);
		}

		/// <summary>
		/// Return IPL String representation for a 2dfx ObjectDef.
		/// </summary>
		/// <returns></returns>
		/// We cheat here a little because the IPL 2dfx format is currently the
		/// same as the IDE 2dfx format, so we create a wrapper Definition
		/// object and call its serialisation method.
		///
		public override String ToIPL2dfx()
		{
			Definition def = new Definition(this.m_Scene, this.Object);
			return (def.ToIDE2dfx());
		}

		/// <summary>
		/// Return IPL String representation for a MiloPlus ObjectDef.
		/// </summary>
		/// <returns></returns>
		/// Format for a MiloPlus object:
		///   instname, flags, miloID, miloRoomID, tx, ty, tz, qx, qy, qz, qw, lodparent, blockindex
		///
		/// Note: MILO Tri rotations are reversed for some reason.
		/// 
		public override String ToIPLMiloPlus(String[] blocks)
		{
			Vector3f pos = this.Object.NodeTransform.Translation;
			Quaternionf rot = new Quaternionf(this.Object.NodeTransform);
			String block = this.Object.GetAttribute(AttrNames.OBJ_BLOCK_ID, AttrDefaults.OBJ_BLOCK_ID);
			int block_idx = Array.IndexOf(blocks, block) + 1;

			String ipl_line = String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}",
				this.Object.GetObjectName(), Flags.GetInstanceFlags(this.Object),
				0, -1,
				pos.X, pos.Y, pos.Z, rot.X, rot.Y, rot.Z, -rot.W,
				this.LODParentIndex, block_idx);
			return (ipl_line);
		}

		/// <summary>
		/// Return IPL String representation for a "VehicleNode" ObjectDef.
		/// </summary>
		/// <returns></returns>
		/// Format of "VehicleNode" line is:
		///   x, y, z, disabled, water, roadblock, carspeed, special, density, streetname, highway, nogps, tunnel, openspace
		///
		public override String ToIPLVehicleNode()
		{
			Vector3f trans = this.Object.NodeTransform.Translation;
			bool disabled = this.Object.GetAttribute(AttrNames.VEHNOD_DISABLED, AttrDefaults.VEHNOD_DISABLED);
			bool water = this.Object.GetAttribute(AttrNames.VEHNOD_WATER, AttrDefaults.VEHNOD_WATER);
			bool roadblock = this.Object.GetAttribute(AttrNames.VEHNOD_ROAD_BLOCK, AttrDefaults.VEHNOD_ROAD_BLOCK);
			bool lowbridge = this.Object.GetAttribute(AttrNames.VEHNOD_LOW_BRIDGE, AttrDefaults.VEHNOD_LOW_BRIDGE);
			bool highway = this.Object.GetAttribute(AttrNames.VEHNOD_HIGHWAY, AttrDefaults.VEHNOD_HIGHWAY);
			bool nogps = this.Object.GetAttribute(AttrNames.VEHNOD_NO_GPS, AttrDefaults.VEHNOD_NO_GPS);
			bool tunnel = this.Object.GetAttribute(AttrNames.VEHNOD_TUNNEL, AttrDefaults.VEHNOD_TUNNEL);
			bool openspace = this.Object.GetAttribute(AttrNames.VEHNOD_OPENSPACE, AttrDefaults.VEHNOD_OPENSPACE);
			// AJM: They wanted it changed so they select value between 0 and 15, default changed to 15 (it was 1.0f before)
            int densityInt = this.Object.GetAttribute(AttrNames.VEHNOD_DENSITY, AttrDefaults.VEHNOD_DENSITY);
            float density = (densityInt / 15.0f);
			int speed = this.Object.GetAttribute(AttrNames.VEHNOD_SPEED, AttrDefaults.VEHNOD_SPEED);
			int special = this.Object.GetAttribute(AttrNames.VEHNOD_SPECIAL, AttrDefaults.VEHNOD_SPECIAL);
			String street = this.Object.GetAttribute(AttrNames.VEHNOD_STREETNAME, AttrDefaults.VEHNOD_STREETNAME);
			uint hstreet = (uint)StringHashUtil.atStringHash(street, 0);
            bool cannotGoLeft = this.Object.GetAttribute(AttrNames.VEHNOD_CANNOTGOLEFT, AttrDefaults.VEHNOD_CANNOTGOLEFT);
            bool leftTurnsOnly = this.Object.GetAttribute(AttrNames.VEHNOD_LEFTTURNSONLY, AttrDefaults.VEHNOD_LEFTTURNSONLY);
            bool offRoad = this.Object.GetAttribute(AttrNames.VEHNOD_OFF_ROAD, AttrDefaults.VEHNOD_OFF_ROAD);
            bool cannotGoRight = this.Object.GetAttribute(AttrNames.VEHNOD_CANNOT_GO_RIGHT, AttrDefaults.VEHNOD_CANNOT_GO_RIGHT);
            bool noBigVehicles = this.Object.GetAttribute(AttrNames.VEHNOD_NO_BIG_VEHICLES, AttrDefaults.VEHNOD_NO_BIG_VEHICLES);
            bool indicateKeepLeft = this.Object.GetAttribute(AttrNames.VEHNOD_INDICATE_KEEP_LEFT, AttrDefaults.VEHNOD_INDICATE_KEEP_LEFT);
            bool indicateKeepRight = this.Object.GetAttribute(AttrNames.VEHNOD_INDICATE_KEEP_RIGHT, AttrDefaults.VEHNOD_INDICATE_KEEP_RIGHT);
            bool slipLane = this.Object.GetAttribute(AttrNames.VEHNOD_SLIP_LANE, AttrDefaults.VEHNOD_SLIP_LANE);

            String ipl_line = String.Format("\t{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19}, {20}, {21}",
                trans.X, trans.Y, trans.Z,
                disabled ? "1" : "0",
                water ? "1" : "0",
                roadblock ? "1" : "0",
                speed, special, density, hstreet,
                highway ? "1" : "0",
                nogps ? "1" : "0",
                tunnel ? "1" : "0",
                openspace ? "1" : "0",
                cannotGoLeft ? "1" : "0",
                leftTurnsOnly ? "1" : "0",
                offRoad ? "1" : "0",
                cannotGoRight ? "1" : "0",
                noBigVehicles ? "1" : "0",
                indicateKeepLeft ? "1" : "0",
                indicateKeepRight ? "1" : "0",
                slipLane ? "1" : "0");

			return (ipl_line);
		}

		/// <summary>
		/// Return IPL String representation for a "VehicleLink" ObjectDef.
		/// </summary>
		/// <returns></returns>
		/// Format of "VehicleLink" line is:
		///   node1id, node2id, width, #lanes12, #lanes21, flags
		///
		public override String ToIPLVehicleLink(List<Guid> nodes)
		{
			Debug.Assert(2 == this.Object.GenericReferenceGuids.Length,
				String.Format("Vehicle link {0} does not have two nodes.", this.Object.Name));
			if (2 != this.Object.GenericReferenceGuids.Length)
				return "";

			System.Collections.Generic.List<Guid> nodelist = new System.Collections.Generic.List<Guid>(nodes);
			Guid node1 = this.Object.GenericReferenceGuids[0];
			Guid node2 = this.Object.GenericReferenceGuids[1];
			int node1id = nodelist.IndexOf(node1);
			int node2id = nodelist.IndexOf(node2);
			int width = this.Object.GetAttribute(AttrNames.VEHLNK_WIDTH, AttrDefaults.VEHLNK_WIDTH);
			int lanesin = this.Object.GetAttribute(AttrNames.VEHLNK_LANES_IN, AttrDefaults.VEHLNK_LANES_IN);
			int lanesout = this.Object.GetAttribute(AttrNames.VEHLNK_LANES_OUT, AttrDefaults.VEHLNK_LANES_OUT);
            UInt32 flags = Flags.GetVehicleLinkFlags(this.Object);

			String ipl_line = String.Format("\t{0}, {1}, {2}, {3}, {4}, {5}",
				node1id, node2id, width, lanesin, lanesout, flags);

			return (ipl_line);
		}

		/// <summary>
		/// Return IPL String representation for a "PatrolNode" ObjectDef.
		/// </summary>
		/// <returns></returns>
		/// Format of "PatrolNode" line is:
		///   x, y, z, headx, heady, headz, duration, type, associated_base, route_name
		///   
		public override String ToIPLPatrolNode()
		{
			Vector3f trans = this.Object.NodeTransform.Translation;
			Vector3f head = new Vector3f();
			int duration = this.Object.GetAttribute(AttrNames.PATNOD_DURATION, AttrDefaults.PATNOD_DURATION);
			String type = this.Object.GetAttribute(AttrNames.PATNOD_PATROL_TYPE, AttrDefaults.PATNOD_PATROL_TYPE);
			String assocbase = this.Object.GetAttribute(AttrNames.PATNOD_ASSOCIATED_BASE, AttrDefaults.PATNOD_ASSOCIATED_BASE);
			String name = this.Object.GetAttribute(AttrNames.PATNOD_ROUTE_NAME, AttrDefaults.PATNOD_ROUTE_NAME);

			head.X = this.Object.GetAttribute(AttrNames.PATNOD_HEADING_X, AttrDefaults.PATNOD_HEADING_X);
			head.Y = this.Object.GetAttribute(AttrNames.PATNOD_HEADING_Y, AttrDefaults.PATNOD_HEADING_Y);
			head.Z = this.Object.GetAttribute(AttrNames.PATNOD_HEADING_Z, AttrDefaults.PATNOD_HEADING_Z);
			uint htype = (uint)StringHashUtil.atStringHash(type, 0);
			uint hassocbase = (uint)StringHashUtil.atStringHash(assocbase, 0);
			uint hname = (uint)StringHashUtil.atStringHash(name, 0);

			String ipl_line = String.Format("\t{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}",
				trans.X, trans.Y, trans.Z, head.X, head.Y, head.Z, duration,
				htype, hassocbase, hname);

			return (ipl_line);
		}

		/// <summary>
		/// Return IPL String representation for a "PatrolLink" ObjectDef.
		/// </summary>
		/// <returns></returns>
		/// Format of "PatrolLink" line is:
		///   node1id, node2id
		///   
		public override String ToIPLPatrolLink(List<Guid> nodes)
		{
			Debug.Assert(2 == this.Object.GenericReferenceGuids.Length,
				String.Format("Patrol link {0} does not have two nodes.", this.Object.Name));
			if (2 != this.Object.GenericReferenceGuids.Length)
				return "";

			System.Collections.Generic.List<Guid> nodelist = new System.Collections.Generic.List<Guid>(nodes);
			Guid node1 = this.Object.GenericReferenceGuids[0];
			Guid node2 = this.Object.GenericReferenceGuids[1];
			int node1id = nodelist.IndexOf(node1);
			int node2id = nodelist.IndexOf(node2);

			String ipl_line = String.Format("\t{0}, {1}", node1id, node2id);
			return (ipl_line);
		}

		/// <summary>
		/// Return IPL String representation for a "Gta TimeCycle" ObjectDef.
		/// </summary>
		/// <returns></returns>
		/// Format of "Gta TimeCycle" line is:
		///   bbmin.x, bbmin.y, bbmin.z, bbmax.x, bbmax.y, bbmax.z, range, percentage, starth, endh, id
		///   
		public override String ToIPLTimeCycle()
		{
            float percentage = (float)this.Object.GetAttribute(AttrNames.TWODFX_TCYC_BOX_PERCENTAGE, AttrDefaults.TWODFX_TCYC_BOX_PERCENTAGE);
            int start_hour = (int)this.Object.GetAttribute(AttrNames.TWODFX_TCYC_BOX_START_HOUR, AttrDefaults.TWODFX_TCYC_BOX_START_HOUR);
            int end_hour = (int)this.Object.GetAttribute(AttrNames.TWODFX_TCYC_BOX_END_HOUR, AttrDefaults.TWODFX_TCYC_BOX_END_HOUR);
            String id_str = (String)this.Object.GetAttribute(AttrNames.TWODFX_TCYC_BOX_IDSTRING, AttrDefaults.TWODFX_TCYC_BOX_IDSTRING);
            float range = (float)this.Object.GetAttribute(AttrNames.TWODFX_TCYC_BOX_RANGE, AttrDefaults.TWODFX_TCYC_BOX_RANGE);
            Vector3f bbmin = this.Object.WorldBoundingBox.Min;
            Vector3f bbmax = this.Object.WorldBoundingBox.Max;
            Vector3f min = new Vector3f(SMath.Min(bbmin.X, bbmax.X), SMath.Min(bbmin.Y, bbmax.Y), SMath.Min(bbmin.Z, bbmax.Z));
            Vector3f max = new Vector3f(SMath.Max(bbmin.X, bbmax.X), SMath.Max(bbmin.Y, bbmax.Y), SMath.Max(bbmin.Z, bbmax.Z));

			String ipl_line = String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}",
				min.X, min.Y, min.Z, max.X, max.Y, max.Z,
				range, percentage, start_hour, end_hour, id_str);
			return (ipl_line);
		}
		#endregion // Controller Methods

		#region Protected Methods
		/// <summary>
		/// Determine the current instance's StreamMode (from ObjectDef attributes).
		/// </summary>
		/// <param name="reader"></param>
		/// Criteria for streaming mode selection:
		///   Streamed: (default) 
		///   StreamedBig:
		///   Non-Streamed:
		protected virtual void DetermineStreamMode(Util.IDEReaderBasic reader)
		{
			bool alwaysloaded = this.Object.GetAttribute(AttrNames.OBJ_ALWAYS_LOADED, AttrDefaults.OBJ_ALWAYS_LOADED);
			bool instloddist = this.Object.GetAttribute(AttrNames.OBJ_INSTANCE_LOD_DISTANCE, AttrDefaults.OBJ_INSTANCE_LOD_DISTANCE);
			bool lodchildren = false;
            bool isRef = (this.Object.IsXRef() || this.Object.IsInternalRef() || this.Object.IsRefObject() || this.Object.IsRefInternalObject());
			this.LODDistance = 0.0f;

			if (isRef)
			{
				if (instloddist)
					this.LODDistance = this.Object.GetLODDistance();
				else if (this.Object.IsXRef() && !this.Object.IsMiloTri())
				{
					String refname = this.Object.GetObjectName();
					this.LODDistance = reader.GetLODDistance(refname);
				}
                else if (this.Object.IsInternalRef() || this.Object.IsRefInternalObject())
					this.LODDistance = this.m_Scene.FindObject(this.Object.InternalRefGuid).GetLODDistance();
			}
			else
			{
				this.LODDistance = this.Object.GetLODDistance();
			}

			if ((null != this.Object.LOD) && (this.Object.LOD.Children.Length > 0))
				lodchildren = true;

			if (this.Object.IsMiloTri())
				this.StreamMode = StreamModeType.NotStreamed;
			else if (alwaysloaded || (this.LODDistance > sC_s_m_NonStreamedLODDistance) || lodchildren)
				this.StreamMode = StreamModeType.NotStreamed;
			else if ((this.LODDistance > sC_s_m_StreamedLongLODDistance) || this.Object.IsMiloTri())
				this.StreamMode = StreamModeType.StreamedBig;
		}
		#endregion // Protected Methods
	}

} // RSG.SceneXml.MapExport namespace

// Instance4.cs
