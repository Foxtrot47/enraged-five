﻿using System.Collections.Generic;

namespace RSG.SceneXml.MapExport.Project.GTA5_NG
{
    public class InteriorContainerNameMap : Framework.InteriorContainerNameMap
    {
        private Dictionary<TargetObjectDef, string> m_objectNameMap;
        
        // keep tracks of interiors "Unique" names, for static collision serialisation
        // this prevents returning the same name for different objects, which will have their own imap
        private Dictionary<string, int> m_interiorNameCounter;

        public InteriorContainerNameMap()
        {
            m_objectNameMap = new Dictionary<TargetObjectDef, string>();
            m_interiorNameCounter = new Dictionary<string, int>();
        }

        public override string GetContainerNameFromObject(TargetObjectDef objectDef)
        {
            if (m_objectNameMap.ContainsKey(objectDef))
            {
                return m_objectNameMap[objectDef];
            }

            string containerName = "";
            if (objectDef.IsMilo())// in-situ interior
            {
                containerName = string.Format("interior_{0}", objectDef.Name).ToLower();
            }
            else if (objectDef.IsMiloTri())// ref interior
            {
                containerName = string.Format("interior_{0}", objectDef.RefName).ToLower();
            }

            if (m_interiorNameCounter.ContainsKey(containerName))
            {
                m_interiorNameCounter[containerName] += 1;
                containerName += "_" + m_interiorNameCounter[containerName];
            }
            else
            {
                m_interiorNameCounter.Add(containerName, 0);
            }

            m_objectNameMap.Add(objectDef, containerName);

            return containerName;
        }
    }
}
