﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;
using RSG.Base.Configuration;
using RSG.Base.Logging;

namespace RSG.SceneXml.MapExport.AssetCombine
{

    /// <summary>
    /// Representation of an Asset Collection for a particular map; produces a 
    /// Combined Map Zip.
    /// </summary>
    public class MapAssetCollection : IAssetCollection
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Name.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Cache directory.
        /// </summary>
        public String CacheDir
        {
            get;
            private set;
        }

        /// <summary>
        /// Output contents into the combined data.
        /// </summary>
        public AssetOutput[] Contents
        {
            get { return (m_Contents.ToArray()); }
        }
        private List<AssetOutput> m_Contents;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="cachrDir"></param>
        public MapAssetCollection(String name, String cacheDir)
        {
            this.Name = name;
            this.CacheDir = cacheDir;
            this.m_Contents = new List<AssetOutput>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Add a single output.
        /// </summary>
        /// <param name="output"></param>
        public void AddContent(AssetOutput output)
        {
            this.m_Contents.Add(output);
        }

        /// <summary>
        /// Add a range of outputs.
        /// </summary>
        /// <param name="outputs"></param>
        public void AddContentRange(IEnumerable<AssetOutput> outputs)
        {
            foreach (AssetOutput output in outputs)
                AddContent(output);
        }

        /// <summary>
        /// Serialise this collection to an XmlElement.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="projectIsDLC"></param>
        /// <returns></returns>
        public XmlElement ToXml(XmlDocument xmlDoc, bool projectIsDLC)
        {
            XmlElement xmlCombineMap = xmlDoc.CreateElement("Map");
            xmlCombineMap.SetAttribute("name", this.Name);
            xmlCombineMap.SetAttribute("cachedir", this.CacheDir);

            foreach (AssetOutput o in this.Contents)
            {
                XmlElement xmlAsset = o.ToXml(xmlDoc, projectIsDLC);
                xmlCombineMap.AppendChild(xmlAsset);
            }
            return (xmlCombineMap);
        }
        #endregion // Controller Methods

        #region Static Controller Methods
        /// <summary>
        /// Deserialise a MapAssetCollection from XmlElement.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="xmlElem"></param>
        /// <returns></returns>
        public static MapAssetCollection Create(IBranch branch, XmlElement xmlElem)
        {
            Debug.Assert(xmlElem.Name.Equals("Map"), "Invalid XML element.");
            if (!xmlElem.Name.Equals("Map"))
            {
                Log.Log__Error("Invalid XML constructing MapAssetCollection ({0}).", xmlElem.Name);
                return (null);
            }

            String name = xmlElem.GetAttribute("name");
            String cacheDir = xmlElem.GetAttribute("cachedir");
            MapAssetCollection map = new MapAssetCollection(name, cacheDir);
            XmlNodeList xmlDrawableDicts = xmlElem.SelectNodes("DrawableDictionary");
            foreach (XmlNode xmlDrawableDictNode in xmlDrawableDicts)
            {
                XmlElement xmlDrawableDict = (xmlDrawableDictNode as XmlElement);
                map.AddContent(DrawableDictionary.Create(branch, xmlDrawableDict));
            }
            XmlNodeList xmlTextureDicts = xmlElem.SelectNodes("TextureDictionary");
            foreach (XmlNode xmlTextureDictNode in xmlTextureDicts)
            {
                XmlElement xmlTextureDict = (xmlTextureDictNode as XmlElement);
                map.AddContent(TextureDictionary.Create(branch, xmlTextureDict));
            }

            return (map);
        }
        #endregion // Static Controller Methods
    }

} // RSG.SceneXml.MapExport.AssetCombine namespace
