﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Logging;

namespace RSG.SceneXml.MapExport.AssetCombine
{

    /// <summary>
    /// Centralised class to support the loading and saving of a collection of 
    /// MapAssetCollection objects; this data is used by the AssetCombineProcessor
    /// and the Map Metadata serialiser so we need to keep this code in a common
    /// place.
    /// </summary>
    ///
    public class AssetFile
    {
        #region Static Constants
        /// <summary>
        /// XML document root node's name.
        /// </summary>
        private static readonly String XML_DOC_ROOT = "AssetCombineProcessor";

        /// <summary>
        /// XML declaration version string.
        /// </summary>
        private static readonly String XML_DECL_VER = "1.0";

        /// <summary>
        /// XML declaration encoding string.
        /// </summary>
        private static readonly String XML_DECL_ENCODING = "utf-8";

        /// <summary>
        /// Help URL.
        /// </summary>
        private static readonly String URL_MULTIPLE_LOD_LINKS = 
            "https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#Multiple_LOD_Links_to_Asset";
        #endregion // Static COnstants

        #region Properties and Associated Member Data
        /// <summary>
        /// Collection of MapAssetCollection objects.
        /// </summary>
        public List<MapAssetCollection> Maps
        {
            get;
            private set;
        }

        /// <summary>
        /// Public access to our dictionary storage; for random access.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public MapAssetCollection this[String name]
        {
            get { return (this.MapDictionary[name]); }
        }

        /// <summary>
        /// Public accessible dictionary based on Input name; returning the
        /// input's Drawable Dictionary name.
        /// </summary>
        public Dictionary<String, String> InputDrawableDictionaries
        {
            get;
            private set;
        }

        /// <summary>
        /// Public accessible dictionary based on Input name; returning the
        /// InputDrawableDictionaries's Texture Dictionary name.
        /// </summary>
        public Dictionary<String, String> InputTextureDictionaries
        {
            get;
            private set;
        }
        #endregion // Properties and Associated Member Data

        #region Member Data
        /// <summary>
        /// Dictionary storage.
        /// </summary>
        private Dictionary<String, MapAssetCollection> MapDictionary;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="branch"></param>
        public AssetFile(IBranch branch)
        {
            this.Maps = new List<MapAssetCollection>();
            InitMapDictionary();
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="maps"></param>
        public AssetFile(IBranch branch, MapAssetCollection[] maps)
        {
            this.Maps = new List<MapAssetCollection>();
            this.Maps.AddRange(maps);
            InitMapDictionary();
        }

        /// <summary>
        /// Constructor from an XML file with custom ConfigGameView.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="filename"></param>
        public AssetFile(IBranch branch, String filename)
        {
            this.Maps = new List<MapAssetCollection>();
            MapAssetCollection[] maps = this.Load(branch, filename);
            this.Maps.AddRange(maps);
            InitMapDictionary();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Save out the data as XML.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="maps"></param>
        /// <param name="projectIsDLC"></param>
        public void Save(String filename, bool projectIsDLC)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.AppendChild(xmlDoc.CreateXmlDeclaration(XML_DECL_VER, XML_DECL_ENCODING, String.Empty));
            XmlElement xmlRoot = xmlDoc.CreateElement(XML_DOC_ROOT);
            xmlRoot.SetAttribute("timestamp", DateTime.Now.ToString());
            xmlDoc.AppendChild(xmlRoot);

            foreach (MapAssetCollection map in this.Maps)
            {
                XmlElement xmlMap = map.ToXml(xmlDoc, projectIsDLC);
                xmlRoot.AppendChild(xmlMap);
            }

            xmlDoc.Save(filename);
        }
        #endregion // Controller Methods

        #region Private Helper Methods
        /// <summary>
        /// Initialise the helper map dictionary.
        /// </summary>
        private void InitMapDictionary()
        {
            this.MapDictionary = new Dictionary<String, MapAssetCollection>();
            foreach (MapAssetCollection map in this.Maps)
                this.MapDictionary.Add(map.Name, map);

            this.InputDrawableDictionaries = new Dictionary<String, String>();
            this.InputTextureDictionaries = new Dictionary<String, String>();
            foreach (KeyValuePair<String, MapAssetCollection> kvp in this.MapDictionary)
            {
                foreach (AssetOutput output in kvp.Value.Contents)
                {
                    if (!(output is DrawableDictionary))
                        continue;

                    DrawableDictionary dict = (output as DrawableDictionary);
                    foreach (AssetInput input in dict.Inputs)
                    {
                        if (this.InputDrawableDictionaries.ContainsKey(input.Name) ||
                            this.InputTextureDictionaries.ContainsKey(input.Name))
                        {
                            Log.Log__ErrorCtx(input.Name, 
                                "Object '{0}' already processed; multiple LOD links to same asset?  Current output: '{1}'.  See {2} for more information.",
                                input.Name, output.Name, URL_MULTIPLE_LOD_LINKS);
                            continue;
                        }
                        this.InputDrawableDictionaries.Add(input.Name, output.Name);
                        this.InputTextureDictionaries.Add(input.Name, dict.TXD);    
                    }                    
                }
            }
        }

        /// <summary>
        /// Load XML data.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
        private MapAssetCollection[] Load(IBranch branch, String filename)
        {
            List<MapAssetCollection> maps = new List<MapAssetCollection>();
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(filename);

            XmlNodeList xmlNodes = xmlDoc.SelectNodes("/AssetCombineProcessor/Map");
            foreach (XmlNode xmlNode in xmlNodes)
            {
                Debug.Assert(xmlNode is XmlElement, "Invalid XML; not an XmlElement?");
                if (!(xmlNode is XmlElement))
                    continue;
                XmlElement xmlElem = (xmlNode as XmlElement);
                MapAssetCollection map = MapAssetCollection.Create(branch, xmlElem);
                Debug.Assert(map != null, "MapAssetCollection is invalid.");
                if (map != null)
                    maps.Add(map);
            }
            return (maps.ToArray());
        }
        #endregion // Static Controller Methods
    }

} // RSG.SceneXml.MapExport.AssetCombine
