﻿using System.Xml.Linq;

using RSG.Base.Math;

namespace RSG.SceneXml.MapExport.Occlusion
{
    /// <summary>
    /// This class represents any occlusion model that can be split among Occlusion.Node items
    /// </summary>
    public abstract class Model
    {
        public abstract BoundingBox3f WorldBoundingBox { get; }
        public abstract int DataSize { get; }
        public abstract void AddToXmlDocument(XElement parentElement);
    }
}
