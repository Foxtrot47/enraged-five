﻿using System;
using System.Xml;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Metadata.Util;

namespace RSG.SceneXml.MapExport.Framework
{
    
    // Note: all archetype flags are gameside; there are none associated with
    //       ::rage::fwArchetype.

    #region ::rage::fwEntityDef Flags
    /// <summary>
    /// ::rage::fwEntityDef Flags Enumeration
    /// </summary>
[Flags]
public enum fwEntityDef_Flags
{
    FLAG_NONE = 0,

    // Framework
    FLAG_FULLMATRIX = (1 << 0),
    FLAG_STREAM_LOW_PRIORITY = (1 << 1),
    FLAG_DONT_INSTANCE_COLLISION = (1 << 2),
    FLAG_ATTACH_LOD_IS_IN_PARENT_MAPDATA = (1 << 3),
    FLAG_ADOPTME = (1 << 4),
    FLAG_IS_FIXED = (1 << 5),
    FLAG_IS_INTERIOR_LOD = (1 << 6),
    
    // Game (will be moved out to GTA IMAP serialiser).
    FLAG_DRAWABLELODUSEALTFADE = (1 << 15),
    FLAG_UNDERWATER = (1<<16),
    FLAG_DOESNOTTOUCHWATER = (1<<17),
    FLAG_DOESNOTSPAWNPEDS = (1<<18),
    FLAG_LIGHTS_CAST_STATIC_SHADOWS = (1<<19),
    FLAG_LIGHTS_CAST_DYNAMIC_SHADOWS = (1<<20),
    FLAG_LIGHTS_IGNORE_DAY_NIGHT_SETTINGS = (1<<21),
    FLAG_DONT_RENDER_IN_SHADOWS = (1<<22),
    FLAG_ONLY_RENDER_IN_SHADOWS = (1<<23),
    FLAG_DONT_RENDER_IN_REFLECTIONS = (1<<24),
    FLAG_ONLY_RENDER_IN_REFLECTIONS = (1<<25),
    FLAG_DONT_RENDER_IN_WATER_REFLECTIONS = (1<<26),
    FLAG_ONLY_RENDER_IN_WATER_REFLECTIONS = (1<<27),
    FLAG_DONT_RENDER_IN_MIRROR_REFLECTIONS = (1<<28),
    FLAG_ONLY_RENDER_IN_MIRROR_REFLECTIONS = (1<<29)
}

    /// <summary>
    /// fwEntityDef_Flags extension methods.
    /// </summary>
    internal static class EntityDefFlags
    {
        /// <summary>
        /// Initialise fwEntityDef_Flags from an Entity object.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        internal static fwEntityDef_Flags Get(Entity entity)
        {
            fwEntityDef_Flags flags = fwEntityDef_Flags.FLAG_NONE;

            // Framework Flags
            if (entity.Object.GetAttribute(AttrNames.OBJ_USE_FULL_MATRIX, AttrDefaults.OBJ_USE_FULL_MATRIX))
                flags |= fwEntityDef_Flags.FLAG_FULLMATRIX;
            if (entity.Object.GetAttribute(AttrNames.OBJ_LOW_PRIORITY, AttrDefaults.OBJ_LOW_PRIORITY))
                flags |= fwEntityDef_Flags.FLAG_STREAM_LOW_PRIORITY;
            if (entity.Object.GetAttribute(AttrNames.OBJ_ADOPT_ME, AttrDefaults.OBJ_ADOPT_ME))
                flags |= fwEntityDef_Flags.FLAG_ADOPTME;
            if (entity.Object.WillHaveCollisionBaked())
                flags |= fwEntityDef_Flags.FLAG_DONT_INSTANCE_COLLISION;
            if (entity.Object.GetAttribute(AttrNames.OBJ_IS_FIXED, AttrDefaults.OBJ_IS_FIXED))
                flags |= fwEntityDef_Flags.FLAG_IS_FIXED;

            // SLOD2 and LOD have parent
            fwEntityDef_eLodTypes lodLevel = LodLevel.Get(entity);
            switch (lodLevel)
            {
                case fwEntityDef_eLodTypes.LODTYPES_DEPTH_HD:
                    flags |= fwEntityDef_Flags.FLAG_ATTACH_LOD_IS_IN_PARENT_MAPDATA;
                    break;
                case fwEntityDef_eLodTypes.LODTYPES_DEPTH_SLOD1:
                    if (entity.Object.LOD.Parent != null && entity.Object.LOD.Parent.IsContainerLODRefObject())
                        flags |= fwEntityDef_Flags.FLAG_ATTACH_LOD_IS_IN_PARENT_MAPDATA;
                    break;                
            }

            // FLAG_IS_INTERIOR_LOD 
            if (entity.Object.HasLODChildren() && entity.Object.LOD.Children.Length == 1)
            {
                TargetObjectDef childObj = entity.Object.LOD.Children[0];
                if (childObj.IsMilo() || // In-situ interiors
                    (childObj.IsRefObject() && childObj.RefObject != null && childObj.RefObject.IsMiloTri())) // Interior refs
                {
                    flags |= fwEntityDef_Flags.FLAG_IS_INTERIOR_LOD;
                }
            }

            // Game Flags
            if (entity.Object.GetAttribute(AttrNames.OBJ_USE_ALTERNATE_FADE_DISTANCE, AttrDefaults.OBJ_USE_ALTERNATE_FADE_DISTANCE))
                flags |= fwEntityDef_Flags.FLAG_DRAWABLELODUSEALTFADE;
            if (entity.Object.GetAttribute(AttrNames.OBJ_IS_UNDERWATER, AttrDefaults.OBJ_IS_UNDERWATER))
                flags |= fwEntityDef_Flags.FLAG_UNDERWATER;
            if (entity.Object.GetAttribute(AttrNames.OBJ_DOES_NOT_TOUCH_WATER, AttrDefaults.OBJ_DOES_NOT_TOUCH_WATER))
                flags |= fwEntityDef_Flags.FLAG_DOESNOTTOUCHWATER;
            if (entity.Object.GetAttribute(AttrNames.OBJ_DISABLE_PED_SPAWNING, AttrDefaults.OBJ_DISABLE_PED_SPAWNING))
                flags |= fwEntityDef_Flags.FLAG_DOESNOTSPAWNPEDS;
            if (entity.Object.GetAttribute(AttrNames.OBJ_STATIC_SHADOWS, AttrDefaults.OBJ_STATIC_SHADOWS))
                flags |= fwEntityDef_Flags.FLAG_LIGHTS_CAST_STATIC_SHADOWS;
            if (entity.Object.GetAttribute(AttrNames.OBJ_DYNAMIC_SHADOWS, AttrDefaults.OBJ_DYNAMIC_SHADOWS))
                flags |= fwEntityDef_Flags.FLAG_LIGHTS_CAST_DYNAMIC_SHADOWS;
            if (entity.Object.GetAttribute(AttrNames.OBJ_ON_ALL_DAY, AttrDefaults.OBJ_ON_ALL_DAY))
                flags |= fwEntityDef_Flags.FLAG_LIGHTS_IGNORE_DAY_NIGHT_SETTINGS;
            if (entity.Object.GetAttribute(AttrNames.OBJ_DONT_RENDER_IN_SHADOWS, AttrDefaults.OBJ_DONT_RENDER_IN_SHADOWS))
                flags |= fwEntityDef_Flags.FLAG_DONT_RENDER_IN_SHADOWS;
            if (entity.Object.GetAttribute(AttrNames.OBJ_ONLY_RENDER_IN_SHADOWS, AttrDefaults.OBJ_ONLY_RENDER_IN_SHADOWS))
                flags |= fwEntityDef_Flags.FLAG_ONLY_RENDER_IN_SHADOWS;
            if (entity.Object.GetAttribute(AttrNames.OBJ_DONT_RENDER_IN_REFLECTIONS, AttrDefaults.OBJ_DONT_RENDER_IN_REFLECTIONS))
                flags |= fwEntityDef_Flags.FLAG_DONT_RENDER_IN_REFLECTIONS;
            if (entity.Object.GetAttribute(AttrNames.OBJ_ONLY_RENDER_IN_REFLECTIONS, AttrDefaults.OBJ_ONLY_RENDER_IN_REFLECTIONS))
                flags |= fwEntityDef_Flags.FLAG_ONLY_RENDER_IN_REFLECTIONS;
            if (entity.Object.GetAttribute(AttrNames.OBJ_DONT_RENDER_IN_WATER_REFLECTIONS, AttrDefaults.OBJ_DONT_RENDER_IN_WATER_REFLECTIONS))
                flags |= fwEntityDef_Flags.FLAG_DONT_RENDER_IN_WATER_REFLECTIONS;
            if (entity.Object.GetAttribute(AttrNames.OBJ_ONLY_RENDER_IN_WATER_REFLECTIONS, AttrDefaults.OBJ_ONLY_RENDER_IN_WATER_REFLECTIONS))
                flags |= fwEntityDef_Flags.FLAG_ONLY_RENDER_IN_WATER_REFLECTIONS;
            if (entity.Object.GetAttribute(AttrNames.OBJ_DONT_RENDER_IN_MIRROR_REFLECTIONS, AttrDefaults.OBJ_DONT_RENDER_IN_MIRROR_REFLECTIONS))
                flags |= fwEntityDef_Flags.FLAG_DONT_RENDER_IN_MIRROR_REFLECTIONS;
            if (entity.Object.GetAttribute(AttrNames.OBJ_ONLY_RENDER_IN_MIRROR_REFLECTIONS, AttrDefaults.OBJ_ONLY_RENDER_IN_MIRROR_REFLECTIONS))
                flags |= fwEntityDef_Flags.FLAG_ONLY_RENDER_IN_MIRROR_REFLECTIONS;

            // This is a mess, DONT_RENDER_IN_MIRROR is setting WATER_REFLECTIONS. This attribute has been hidden from Max and no object
            // should use it but we'll set the appropriate flags if for some crazy reason the attribute gets through.
            if (entity.Object.GetAttribute(AttrNames.OBJ_DONT_RENDER_IN_MIRROR, AttrDefaults.OBJ_DONT_RENDER_IN_MIRROR))
                flags |= fwEntityDef_Flags.FLAG_DONT_RENDER_IN_MIRROR_REFLECTIONS;
            if (entity.Object.GetAttribute(AttrNames.OBJ_ONLY_RENDER_IN_MIRROR, AttrDefaults.OBJ_ONLY_RENDER_IN_MIRROR))
                flags |= fwEntityDef_Flags.FLAG_ONLY_RENDER_IN_MIRROR_REFLECTIONS;

            return (flags);
        }

        /// <summary>
        /// Serialise fwEntity_Flags as XML.
        /// </summary>
        /// <param name="flags"></param>
        /// <param name="doc"></param>
        /// <returns></returns>
        internal static XmlElement AsXml(this fwEntityDef_Flags flags, XmlDocument doc)
        {
            XmlElement xmlItem = Xml.CreateElementWithValueAttribute(doc, null, "flags", flags.ToString("d"));
#if DEBUG
            Xml.CreateComment(doc, xmlItem, flags.ToString());
#endif // DEBUG
            return (xmlItem);
        }
    }

    /// <summary>
    /// ::rage::ePriorityLevel Enumeration
    /// </summary>
    public enum fwEntityDef_ePriorityLevel
    {
        PRI_REQUIRED = 0,
        PRI_OPTIONAL_HIGH = 1,
        PRI_OPTIONAL_MEDIUM = 2,
        PRI_OPTIONAL_LOW = 3,
    }

    /// <summary>
    /// fwEntityDef_ePriorityLevel extension methods.
    /// </summary>
    internal static class PriorityLevel
    {
        /// <summary>
        /// Initialise fwEntityDef_ePriorityLevel from an Entity object.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        internal static fwEntityDef_ePriorityLevel Get(Entity entity)
        {
            fwEntityDef_ePriorityLevel priority = fwEntityDef_ePriorityLevel.PRI_REQUIRED;

            if (entity.Object.HasPriority())
                priority = (fwEntityDef_ePriorityLevel)entity.Object.GetPriority();
            return (priority);
        }

        /// <summary>
        /// Serialise fwEntityDef_ePriorityLevel as XML.
        /// </summary>
        /// <param name="priority"></param>
        /// <param name="doc"></param>
        /// <returns></returns>
        internal static XElement AsXml(this fwEntityDef_ePriorityLevel priority)
        {
            XElement xmlItem = null;
            switch (priority)
            {
                case fwEntityDef_ePriorityLevel.PRI_REQUIRED:
                    xmlItem = Xml.CreateElementWithText(null, "priorityLevel", "PRI_REQUIRED");
                    break;
                case fwEntityDef_ePriorityLevel.PRI_OPTIONAL_HIGH:
                    xmlItem = Xml.CreateElementWithText(null, "priorityLevel", "PRI_OPTIONAL_HIGH");
                    break;
                case fwEntityDef_ePriorityLevel.PRI_OPTIONAL_MEDIUM:
                    xmlItem = Xml.CreateElementWithText(null, "priorityLevel", "PRI_OPTIONAL_MEDIUM");
                    break;
                case fwEntityDef_ePriorityLevel.PRI_OPTIONAL_LOW:
                    xmlItem = Xml.CreateElementWithText(null, "priorityLevel", "PRI_OPTIONAL_LOW");
                    break;
                default:
                    Log.Log__ErrorCtx("Invalid priority flag {0} on object.", priority.ToString("d"));
                    break;
            }
            return (xmlItem);
        }
    }

    /// <summary>
    /// ::rage::fwEntityDef Lod Level Enumeration
    /// </summary>
    public enum fwEntityDef_eLodTypes
    {
        LODTYPES_DEPTH_HD = 0,
        LODTYPES_DEPTH_LOD = 1,
        LODTYPES_DEPTH_SLOD1 = 2,
        LODTYPES_DEPTH_SLOD2 = 3,
        LODTYPES_DEPTH_SLOD3 = 4,
        LODTYPES_DEPTH_ORPHANHD = 5,
        LODTYPES_DEPTH_SLOD4 = 6,
    }

    /// <summary>
    /// fwEntityDef_eLodTypes extension methods.
    /// </summary>
    internal static class LodLevel
    {
        /// <summary>
        /// Initialise fwEntityDef_eLodTypes from an Entity object.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        internal static fwEntityDef_eLodTypes Get(Entity entity)
        {
            switch (entity.Object.LODLevel)
            {
                case ObjectDef.LodLevel.HD:
                    return (fwEntityDef_eLodTypes.LODTYPES_DEPTH_HD);
                case ObjectDef.LodLevel.LOD:
                    return (fwEntityDef_eLodTypes.LODTYPES_DEPTH_LOD);
                case ObjectDef.LodLevel.SLOD1:
                    return (fwEntityDef_eLodTypes.LODTYPES_DEPTH_SLOD1);
                case ObjectDef.LodLevel.SLOD2:
                    return (fwEntityDef_eLodTypes.LODTYPES_DEPTH_SLOD2);
                case ObjectDef.LodLevel.SLOD3:
                    return (fwEntityDef_eLodTypes.LODTYPES_DEPTH_SLOD3);
                case ObjectDef.LodLevel.SLOD4:
                    return (fwEntityDef_eLodTypes.LODTYPES_DEPTH_SLOD4);
                case ObjectDef.LodLevel.ORPHANHD:
                default:
                    return (fwEntityDef_eLodTypes.LODTYPES_DEPTH_ORPHANHD);
            }       
        }


        /// <summary>
        /// Serialise fwEntityDef_eLodTypes as XML.
        /// </summary>
        /// <param name="lodlevel"></param>
        /// <param name="doc"></param>
        /// <returns></returns>
        internal static XElement AsXml(this fwEntityDef_eLodTypes lodlevel)
        {
            XElement xmlItem = Xml.CreateElementWithText(null, "lodLevel", lodlevel.ToString());
            return (xmlItem);
        }
    }
    #endregion // ::rage::fwEntityDef Flags

    #region ::rage::fwMapData Flags and ContentFlags
    /// <summary>
    /// ::rage::fwMapData Flags enumeration.
    /// </summary>
    /// This enumeration specified the type of IMAp file, e.g. how it should
    /// be streamed by the game.
    [Flags]
    public enum IMAP_eFlags
    {
        F_MANUAL_STREAM_ONLY = 0x00000001,
        F_IS_PARENT = 0x00000002,
    }

    /// <summary>
    /// IMAP_eFlags extension methods.
    /// </summary>
    internal static class IMAP_Flags
    {
        internal static XmlElement AsXml(this IMAP_eFlags flags, XmlDocument doc)
        {
            XmlElement xmlItem = Xml.CreateElementWithValueAttribute(doc, null, "flags", flags.ToString());
            return (xmlItem);
        }
    }

    /// <summary>
    /// ::rage::fwMapData Content Flags enumeration.
    /// </summary>
    /// This enumeration specifies the types of entities that are contained
    /// in the IMAP file.
    [Flags]
    public enum IMAP_eContentFlags
    {
        CF_HIGHDETAIL = 0x00000001,
        CF_LOD = 0x00000002,
        CF_CONTAINERLOD = 0x00000004,
        CF_MLO = 0x00000008,
        CF_BLOCKINFO = 0x00000010,      // Dirty!  Should be in game-ContentFlags.
        CF_OCCLUDER = 0x00000020,    
        CF_ENTITIES_USING_PHYSICS = 0x00000040,
        CF_LOD_LIGHTS = 0x00000080,
        CF_DSITANT_LOD_LIGHTS = 0x00000100,
        CF_ENTITIES_CRITICAL = 0x00000200,
		CF_INSTANCED_ENTITIES = 0x00000400
    }

    /// <summary>
    /// IMAP_eContentFlags extension methods.
    /// </summary>
    internal static class IMAP_ContentFlags
    {
        internal static XmlElement AsXml(this IMAP_eContentFlags flags, XmlDocument doc)
        {
            XmlElement xmlItem = Xml.CreateElementWithValueAttribute(doc, null, "contentFlags", flags.ToString());
            return (xmlItem);
        }
    }
    #endregion // ::rage::fwMapData Flags and ContentFlags

} // RSG.SceneXml.MapExport.Framework namespace
