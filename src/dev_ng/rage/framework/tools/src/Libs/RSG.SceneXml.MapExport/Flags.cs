//
// File: IDEFlags.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of IDEFlags class
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using RSG.Base.Logging;

namespace RSG.SceneXml.MapExport
{
	/// <summary>
	/// IDEFlags class contains all of the flag constants in useful enumerations
	/// and static utility methods.
	/// </summary>
	/// <seealso cref="ObjectDef"/>
	public static class Flags
	{
		#region Flag Constants
		/// <summary>
		/// "Gta Object" Flags
		/// </summary>
		[Flags]
		public enum ObjectFlags
		{
			OBJ_LIGHT_REFLECTION		= ( 1 << 0 ),
			OBJ_DO_NOT_FADE				= ( 1 << 1 ),
			OBJ_DRAW_LAST				= ( 1 << 2 ),
			OBJ_INSTANCE				= ( 1 << 3 ),
			// OBJ_IS_SUBWAY			= ( 1 << 4 ), // Don't reuse
			OBJ_IS_FIXED				= ( 1 << 5 ),
			OBJ_NO_ZBUFFER_WRITE		= ( 1 << 6 ),
			OBJ_TOUGH_FOR_BULLETS		= ( 1 << 7 ),
			// OBJ_IS_GENERIC			= ( 1 << 8 ), // Don't reuse
			OBJ_HAS_ANIM				= ( 1 << 9 ),
			OBJ_HAS_UV_ANIM				= ( 1 << 10 ),
			OBJ_SHADOW_ONLY				= ( 1 << 11 ),
			// OBJ_IS_DAMAGEABLE		= ( 1 << 12 ), // Not attribute, implied by prescence of @name_dam model.
			OBJ_DONT_CAST_SHADOWS		= ( 1 << 13 ),
			OBJ_CAST_TEXTURE_SHADOWS	= ( 1 << 14 ),
			OBJ_DONT_COLLIDE_WITH_FLYER	= ( 1 << 15 ),
			// OBJ_EXPLODESWHENSHOT		= ( 1 << 16 ), // Don't reuse
			OBJ_IS_DYNAMIC				= ( 1 << 17 ),
			// OBJ_ISCRANE				= ( 1 << 18 ), // Don't reuse
			// OBJ_DRAWWETONLY			= ( 1 << 19 ), // Don't reuse
			// OBJ_ISTAGGABLE			= ( 1 << 20 ), // Don't reuse
			OBJ_BACKFACE_CULL			= ( 1 << 21 ),
			OBJ_DOES_NOT_PROVIDE_AI_COVER	= ( 1 << 22 ),
			// OBJ_DOES_NOT_PROVIDE_COVER	= ( 1 << 23 ), // Not attribute, implied by OBJ_DOES_NOT_PROVIDE_AI_COVER, OBJ_REVERSE_COVER_FOR_PLAYER
			OBJ_IS_LADDER				= ( 1 << 24 ),
			OBJ_IS_FIRE_ESCAPE			= ( 1 << 25 ),
			OBJ_HAS_DOOR_PHYSICS		= ( 1 << 26 ),
			OBJ_IS_FIXED_FOR_NAVIGATION	= ( 1 << 27 ),		
			OBJ_DONT_AVOID_BY_PEDS		= ( 1 << 28 ),		
			OBJ_ENABLE_AMBIENT_MULTIPLIER	= ( 1 << 29 ),
			OBJ_IS_DEBUG                = (1 << 30)
		};
		internal const UInt32 ATTR_BIT_OBJ_DOES_NOT_PROVIDE_PLAYER_COVER = (1 << 23);

		/// <summary>
		/// "Gta Object" per-instance (IPL) flags
		/// </summary>
		[Flags]
		public enum InstanceFlags
		{
			OBJ_LOW_PRIORITY            = ( 1 << 0 ),
			OBJ_USE_FULL_MATRIX         = ( 1 << 1 ),
			OBJ_IS_UNDERWATER           = ( 1 << 2 ),
			OBJ_IS_TUNNEL               = ( 1 << 3 ),
			OBJ_IS_TUNNEL_TRANSITION    = ( 1 << 4 ),
			OBJ_DOES_NOT_TOUCH_WATER    = ( 1 << 5 ),
			OBJ_DISABLE_PED_SPAWNING    = ( 1 << 6 ),
			OBJ_STATIC_SHADOWS          = ( 1 << 7 ),
			OBJ_DYNAMIC_SHADOWS         = ( 1 << 8 ),
			OBJ_ON_ALL_DAY              = ( 1 << 9 ),
			OBJ_ADOPT_ME                = ( 1 << 12 )         // 10 and 11 are for FLAG_LIGHTS_PRIORITYA and FLAG_LIGHTS_PRIORITYB
		};

        /// <summary>
        /// MLO Portal Mirror Type
        /// </summary>
        public enum MLOPortalMirrorType
        {
            NONE,
            MIRROR,
            MIRROR_FLOOR,
            MIRROR_WATER_SURFACE,
            MIRROR_WATER_SURFACE_EXTEND_TO_HORIZON,
            COUNT
        }

		/// <summary>
		/// "Gta Object", "Is Time Object" enabled Flags.
		/// </summary>
		[Flags]
		internal enum TimeObjectFlags
		{
			TOBJ_TIME_1					    = ( 1 << 1 ),
			TOBJ_TIME_2					    = ( 1 << 2 ),
			TOBJ_TIME_3					    = ( 1 << 3 ),
			TOBJ_TIME_4					    = ( 1 << 4 ),
			TOBJ_TIME_5					    = ( 1 << 5 ),
			TOBJ_TIME_6					    = ( 1 << 6 ),
			TOBJ_TIME_7					    = ( 1 << 7 ),
			TOBJ_TIME_8					    = ( 1 << 8 ),
			TOBJ_TIME_9					    = ( 1 << 9 ),
			TOBJ_TIME_10					= ( 1 << 10 ),
			TOBJ_TIME_11					= ( 1 << 11 ),
			TOBJ_TIME_12					= ( 1 << 12 ),
			TOBJ_TIME_13					= ( 1 << 13 ),
			TOBJ_TIME_14					= ( 1 << 14 ),
			TOBJ_TIME_15					= ( 1 << 15 ),
			TOBJ_TIME_16					= ( 1 << 16 ),
			TOBJ_TIME_17					= ( 1 << 17 ),
			TOBJ_TIME_18					= ( 1 << 18 ),
			TOBJ_TIME_19					= ( 1 << 19 ),
			TOBJ_TIME_20					= ( 1 << 20 ),
			TOBJ_TIME_21					= ( 1 << 21 ),
			TOBJ_TIME_22					= ( 1 << 22 ),
			TOBJ_TIME_23					= ( 1 << 23 ),
			TOBJ_TIME_24					= ( 1 << 0 ),
			TOBJ_ALLOW_VANISH_WHILST_VIEWED = ( 1 << 24 )
		};

		/// <summary>
		/// "Gta Block" Flags
		/// </summary>
		[Flags]
		internal enum BlockFlags
		{
			BLOCK_FIRST_PASS                = ( 1 << 0 ),
			BLOCK_SECOND_PASS_STARTED       = ( 1 << 1 ),
			BLOCK_SECOND_PASS_OUTSOURCED    = ( 1 << 2 ),
			BLOCK_SECOND_PASS_COMPLETE      = ( 1 << 3 ),
			BLOCK_THIRD_PASS_COMPLETE       = ( 1 << 4 ),
			BLOCK_COMPLETE                  = ( 1 << 5 ),
			BLOCK_UNUSED                    = ( 1 << 6 )
		};

		/// <summary>
		/// "Gta CarGen" Flags
		/// </summary>
		[Flags]
		internal enum CarGenFlags
		{
			CARGEN_HIGH_PRIORITY            = ( 1 << 0 ),
			CARGEN_IGNORE_POPULATION_LIMIT  = ( 1 << 1 ),
			CARGEN_POLICE                   = ( 1 << 2 ),
			CARGEN_FIRETRUCK                = ( 1 << 3 ),
			CARGEN_AMBULANCE                = ( 1 << 4 ),
			CARGEN_ACTIVE_DAY               = ( 1 << 5 ),
			CARGEN_ACTIVE_NIGHT             = ( 1 << 6 ),
			CARGEN_ALIGN_LEFT               = ( 1 << 7 ),
			CARGEN_ALIGN_RIGHT              = ( 1 << 8 ),
			CARGEN_SINGLE_PLAYER            = ( 1 << 9 ),
			CARGEN_NETWORK                  = ( 1 << 10 ),
            CARGEN_LOW_PRIORITY             = ( 1 << 11 )
		};

		/// <summary>
		/// "Gta Milo" Flags.
		/// </summary>
		[Flags]
		internal enum MiloFlags
		{
			MILO_PROPOGATE_INSTANCING		= ( 1 << 8 ),
			MILO_OFFICE_POPULATION			= ( 1 << 9 ),
			MILO_ALLOW_SPRINTING			= ( 1 << 10 ),
			MILO_CUTSCENE_ONLY				= ( 1 << 11 ),
			MILO_LOD_WHEN_LOCKED			= ( 1 << 12 ),
			MILO_NO_WATER_REFLECTION		= ( 1 << 13 ),
			MILO_DONT_DRAW_IF_LOCKED		= ( 1 << 14 )
		};

        /// <summary>
        /// "Gta Milo" Flags.
        /// </summary>
        [Flags]
        internal enum MiloInstanceFlags
        {
            MILO_INSTANCE_TURN_ON_GPS = (1 << 0),
            MILO_INSTANCE_CAP_ALPHA = (1 << 1),
            MILO_INSTANCE_SHORT_FADE_DISTANCE = (1 << 2),
        };

		/// <summary>
		/// Milo Entry Flags
		/// </summary>
		[Flags]
		internal enum MiloEntryFlags
		{
			MILOENTRY_IS_LOD                = ( 1 << 0 ),   // No attribute
			MILOENTRY_IS_SLOD               = ( 1 << 1 ),   // No attribute
			OBJ_HOLD_FOR_LOD				= ( 1 << 2 )
		};

		/// <summary>
		/// "Gta MloRoom" Flags
		/// </summary>
		[Flags]
		internal enum MloRoomFlags
		{
			MLOROOM_FREEZE_CARS			        = ( 1 << 0 ),
			MLOROOM_FREEZE_PEDS			        = ( 1 << 1 ),
			MLOROOM_NO_DIRECTIONAL_LIGHT        = ( 1 << 2 ),
			MLOROOM_NO_EXTERIOR_LIGHT	        = ( 1 << 3 ),
			MLOROOM_FORCE_FREEZE		        = ( 1 << 4 ), 
			MLOROOM_REDUCE_CARS			        = ( 1 << 5 ),
			MLOROOM_REDUCE_PEDS			        = ( 1 << 6 ),
            MLOROOM_FORCE_DIRECTIONAL_LIGHT     = ( 1 << 7 ),
            MLOROOM_DONT_RENDER_EXTERIOR        = ( 1 << 8 ),
            MLOROOM_MIRROR_POTENTIALLY_VISIBLE  = ( 1 << 9 ),
		}
		
		/// <summary>
		/// "Gta MloPortal" Flags
		/// </summary>
		[Flags]
		internal enum MloPortalFlags
		{
			MLOPORTAL_ONE_WAY			                        = ( 1 << 0 ),
			MLOPORTAL_MLO_LINK			                        = ( 1 << 1 ),
			MLOPORTAL_MIRROR			                        = ( 1 << 2 ),
			MLOPORTAL_IGNORE_BY_MODIFIER                        = ( 1 << 3 ),
			MLOPORTAL_EXPENSIVE_SHADERS	                        = ( 1 << 4 ),
			MLOPORTAL_DRAW_LOW_LOD_ONLY	                        = ( 1 << 5 ),
			MLOPORTAL_ALLOW_CLOSING		                        = ( 1 << 6 ),
            MLOPORTAL_MIRROR_CAN_SEE_DIRECTIONAL_LIGHT          = ( 1 << 7 ),
            MLOPORTAL_USE_PORTAL_TRAVERSAL                      = ( 1 << 8 ),
            MLOPORTAL_MIRROR_FLOOR                              = ( 1 << 9 ),
            MLOPORTAL_MIRROR_CAN_SEE_EXTERIOR_VIEW              = ( 1 << 10 ),
            MLOPORTAL_MIRROR_WATER_SURFACE                      = ( 1 << 11 ),
            MLOPORTAL_MIRROR_WATER_SURFACE_EXTEND_TO_HORIZON    = ( 1 << 12 ),
            MLOPORTAL_USE_LIGHT_BLEED                           = ( 1 << 13 )
		}
		
		/// <summary>
		/// "RAGE Particle" Flags
		/// </summary>
		[Flags]
		internal enum TwodfxParticleFlags 
		{
			TWODFX_PARTICLE_HAS_TINT			= ( 1 << 0 ),
			TWODFX_PARTICLE_IGNORE_DAMAGE_MODEL	= ( 1 << 1 ),
			TWODFX_PARTICLE_PLAY_ON_PARENT = (1 << 2),
			TWODFX_PARTICLE_ONLY_ON_DAMAGE_MODEL = (1 << 3),
            TWODFX_PARTICLE_ALLOW_RUBBER_BULLET_SHOT_FX = (1 << 4),
            TWODFX_PARTICLE_ALLOW_ELECTRIC_BULLET_SHOT_FX = (1 << 5)
		}

        /// <summary>
        /// "RsDecal" Flags
        /// </summary>
        [Flags]
        internal enum TwodfxDecalFlags
        {
            TWODFX_DECAL_UNUSED                         = (1 << 0),
            TWODFX_DECAL_IGNORE_DAMAGE_MODEL            = (1 << 1),
            TWODFX_DECAL_PLAY_ON_PARENT                 = (1 << 2),
            TWODFX_DECAL_ONLY_ON_DAMAGE_MODEL           = (1 << 3),
            TWODFX_DECAL_ALLOW_RUBBER_BULLET_SHOT_FX    = (1 << 4),
            TWODFX_DECAL_ALLOW_ELECTRIC_BULLET_SHOT_FX  = (1 << 5)
        }

		/// <summary>
		/// "Gta Explosion" Flags
		/// </summary>
		[Flags]
		internal enum TwodfxExplosionFlags
		{
			TWODFX_EXPLOSION_UNUSED                 = ( 1 << 0 ),
			TWODFX_EXPLOSION_IGNORE_DAMAGE_MODEL    = ( 1 << 1 ),
			TWODFX_EXPLOSION_PLAY_ON_PARENT         = ( 1 << 2 ),
			TWODFX_EXPLOSION_ONLY_ON_DAMAGE_MODEL   = ( 1 << 3 ),
            TWODFX_EXPLOSION_ALLOW_RUBBER_BULLET_SHOT_FX = (1 << 4),
            TWODFX_EXPLOSION_ALLOW_ELECTRIC_BULLET_SHOT_FX = (1 << 5)
		}

		/// <summary>
		/// "Gta Light" / "Gta PhotoLight" Flags
		/// </summary>
		[Flags]
		internal enum TwodfxLightFlags
		{
            TWODFX_LIGHT_INTERIOR_ONLY = (1<<0),
            TWODFX_LIGHT_EXTERIOR_ONLY = (1<<1),
            TWODFX_LIGHT_DISABLE_IN_CUTSCENE = (1 << 2),
            TWODFX_LIGHT_VEHICLE = (1<<3),
            TWODFX_LIGHT_FX = (1<<4),
            TWODFX_LIGHT_TEXTURE_PROJECTION = (1<<5),
            TWODFX_LIGHT_CAST_SHADOWS = (1<<6),
            TWODFX_LIGHT_CAST_STATIC_SHADOWS = (1 << 7),
            TWODFX_LIGHT_CAST_DYNAMIC_SHADOWS = (1 << 8),
            TWODFX_LIGHT_CALC_FROM_SUNLIGHT = (1 << 9),
            TWODFX_LIGHT_ENABLE_BUZZING = (1<<10),
            TWODFX_LIGHT_FORCE_BUZZING = (1<<11),
            TWODFX_LIGHT_VOLUME_DRAWING = (1 << 12),
            TWODFX_LIGHT_NO_SPECULAR = (1<<13),
            TWODFX_LIGHT_INT_AND_EXT = (1 << 14),
            TWODFX_LIGHT_ONLY_CORONA = (1 << 15),
            TWODFX_LIGHT_DONT_RENDER_IN_REFLECTION = (1 << 16),
            TWODFX_LIGHT_ONLY_RENDER_IN_REFLECTION = (1 << 17),
            TWODFX_LIGHT_USE_CULLPLANE = (1 << 18),
            TWODFX_LIGHT_USE_VOL_OUTER_COLOUR = (1 << 19),
            TWODFX_LIGHT_CAST_HIGHER_RES_SHADOW = (1<<20), // bump the shadow cache 
            TWODFX_LIGHT_CAST_ONLY_LOWRES_SHADOWS = (1<<21), // never use hires shadow        
            TWODFX_LIGHT_ADD_TO_VIP_LOD_LIGHTS       = ( 1 << 22 ),
            TWODFX_LIGHT_DONT_LIGHT_ALPHA            = ( 1 << 23 ),
            TWODFX_LIGHT_SHADOWS_IF_POSSIBLE         = ( 1 << 24 ),
            TWODFX_LIGHT_CUTSCENE                    = (1<<25),
            TWODFX_LIGHT_MOVING_LIGHT_SOURCE         = (1<<26),               
            TWODFX_LIGHT_USE_VEHICLE_TWIN            = (1<<27),
            TWODFX_LIGHT_FORCE_MEDIUM_LOD_LIGHT      = (1<<28),
            TWODFX_LIGHT_CORONA_ONLY_LOD_LIGHT       = (1<<29)

        }
        internal enum TwodfxLightTypes
        {
            TWODFX_LIGHT_STANDARD   = (1 << 0),
            TWODFX_LIGHT_SPOT       = (1 << 1),
            TWODFX_LIGHT_CAPSULE    = (1 << 2)
        }

		/// <summary>
		/// "Gta LightShaft" Flags
		/// </summary>
		[Flags]
		internal enum TwodfxLightShaftFlags
		{
			TWODFX_LIGHTSHAFT_USE_SUN_LIGHT_DIRECTION   = ( 1 << 0 ),
			TWODFX_LIGHTSHAFT_USE_SUN_LIGHT_COLOUR      = ( 1 << 1 ),
// 			TWODFX_LIGHTSHAFT_SHADOWED                  = ( 1 << 2 ),
// 			TWODFX_LIGHTSHAFT_PER_PIXEL_SHADOWED        = ( 1 << 3 )
            TWODFX_LIGHTSHAFT_UNUSED_2				    = ( 1<<2), // LIGHTSHAFTFLAG_SHADOWED
            TWODFX_LIGHTSHAFT_UNUSED_3				    = ( 1<<3), // LIGHTSHAFTFLAG_PER_PIXEL_SHADOWED
            TWODFX_LIGHTSHAFT_SCALE_BY_SUN_COLOUR	    = ( 1<<4),
            TWODFX_LIGHTSHAFT_SCALE_BY_SUN_INTENSITY    = ( 1<<5),
            TWODFX_LIGHTSHAFT_DRAW_IN_FRONT_AND_BEHIND  = ( 1<<6),
            TWODFX_LIGHTSHAFT_SQUARED_OFF_ENDCAP        = ( 1<<7)
		}

		/// <summary>
		/// "Gta ProcObject" Flags
		/// </summary>
		[Flags]
		internal enum TwodfxProcFlags
		{
			TWODFX_PROC_ALIGNED = ( 1 << 0 )
		}

		/// <summary>
		/// "VehicleLink" Flags
		/// </summary>
        [Flags]
        internal enum VehicleLinkFlags
        {
            VEHLNK_NARROW_ROAD      = (1 << 0),
            VEHLNK_GPS_BOTH_WAYS    = (1 << 1),
            VEHLNK_BLOCK_IF_NO_LANES= (1 << 2),
            VEHLNK_SHORTCUT         = (1 << 3),
            VEHLNK_NONAVIGATION     = (1 << 4)
        }

		/// <summary>
		/// "Gta LodModifier" Flags
		/// </summary>
		[Flags]
		internal enum LodModifierFlags
		{
			LODMODIFIER_CULLWATER
		}

        /// <summary>
        /// "RsWindDisturbance" Flags
        /// </summary>
        [Flags]
        internal enum TwodfxWindDisturbanceFlags
        {
            TWODFX_WIND_STRENGTH_MULTIPLIES_GLOBAL_WIND = (1 << 0)
        }

        /// <summary>
        /// "RsWindDisturbance" Flags
        /// </summary>
        [Flags]
        internal enum OcclusionMeshFlags
        {
            OCCLUSION_MESH_WATER_ONLY = (1 << 0)
        }
		#endregion // Flag Constants

		#region Static Methods
		/// <summary>
		/// Fetch "Gta Object" definition flags (for IDE file).
		/// </summary>
		/// <param name="obj">Object to fetch flags for</param>
		/// <returns>UInt32 bitmask for "Gta Object" definition flags.</returns>
		internal static UInt32 GetFlags(TargetObjectDef obj)
		{
			UInt32 flags = 0x00000000;
			SetAttributeFlag(obj, AttrNames.OBJ_LIGHT_REFLECTION, AttrDefaults.OBJ_LIGHT_REFLECTION, (uint)ObjectFlags.OBJ_LIGHT_REFLECTION, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_DO_NOT_FADE, AttrDefaults.OBJ_DO_NOT_FADE, (uint)ObjectFlags.OBJ_DO_NOT_FADE, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_DRAW_LAST, AttrDefaults.OBJ_DRAW_LAST, (uint)ObjectFlags.OBJ_DRAW_LAST, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_INSTANCE, AttrDefaults.OBJ_INSTANCE, (uint)ObjectFlags.OBJ_INSTANCE, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_NO_ZBUFFER_WRITE, AttrDefaults.OBJ_NO_ZBUFFER_WRITE, (uint)ObjectFlags.OBJ_NO_ZBUFFER_WRITE, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_TOUGH_FOR_BULLETS, AttrDefaults.OBJ_TOUGH_FOR_BULLETS, (uint)ObjectFlags.OBJ_TOUGH_FOR_BULLETS, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_HAS_ANIM, AttrDefaults.OBJ_HAS_ANIM, (uint)ObjectFlags.OBJ_HAS_ANIM, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_HAS_UV_ANIM, AttrDefaults.OBJ_HAS_UV_ANIM, (uint)ObjectFlags.OBJ_HAS_UV_ANIM, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_SHADOW_ONLY, AttrDefaults.OBJ_SHADOW_ONLY, (uint)ObjectFlags.OBJ_SHADOW_ONLY, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_DONT_CAST_SHADOWS, AttrDefaults.OBJ_DONT_CAST_SHADOWS, (uint)ObjectFlags.OBJ_DONT_CAST_SHADOWS, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_CAST_TEXTURE_SHADOWS, AttrDefaults.OBJ_CAST_TEXTURE_SHADOWS, (uint)ObjectFlags.OBJ_CAST_TEXTURE_SHADOWS, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_DONT_COLLIDE_WITH_FLYER, AttrDefaults.OBJ_DONT_COLLIDE_WITH_FLYER, (uint)ObjectFlags.OBJ_DONT_COLLIDE_WITH_FLYER, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_IS_DYNAMIC, AttrDefaults.OBJ_IS_DYNAMIC, (uint)ObjectFlags.OBJ_IS_DYNAMIC, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_IS_FIXED, AttrDefaults.OBJ_IS_FIXED, (uint)ObjectFlags.OBJ_IS_FIXED, ref flags);
			SetAttributeFlagNegative(obj, AttrNames.OBJ_BACKFACE_CULL, AttrDefaults.OBJ_BACKFACE_CULL, (uint)ObjectFlags.OBJ_BACKFACE_CULL, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_DOES_NOT_PROVIDE_AI_COVER, AttrDefaults.OBJ_DOES_NOT_PROVIDE_AI_COVER, (uint)ObjectFlags.OBJ_DOES_NOT_PROVIDE_AI_COVER, ref flags);

			// Jimmy bug #7865, force set "Is Dynamic" automatically for "Bendy Plants".
			if (ObjAttributeValues.Bendy_Plant == (ObjAttributeValues)obj.GetAttribute(AttrNames.OBJ_ATTRIBUTE, AttrDefaults.OBJ_ATTRIBUTE))
				flags |= (uint)ObjectFlags.OBJ_IS_DYNAMIC;

			bool reverse = obj.GetAttribute(AttrNames.OBJ_REVERSE_COVER_FOR_PLAYER, AttrDefaults.OBJ_REVERSE_COVER_FOR_PLAYER);
			bool cover = (flags & (uint)ObjectFlags.OBJ_DOES_NOT_PROVIDE_AI_COVER) > 0;
			cover = reverse ? !cover : cover;
			if (cover)
				flags |= ATTR_BIT_OBJ_DOES_NOT_PROVIDE_PLAYER_COVER;
			
			SetAttributeFlag(obj, AttrNames.OBJ_IS_LADDER, AttrDefaults.OBJ_IS_LADDER, (uint)ObjectFlags.OBJ_IS_LADDER, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_HAS_DOOR_PHYSICS, AttrDefaults.OBJ_HAS_DOOR_PHYSICS, (uint)ObjectFlags.OBJ_HAS_DOOR_PHYSICS, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_IS_FIXED_FOR_NAVIGATION, AttrDefaults.OBJ_IS_FIXED_FOR_NAVIGATION, (uint)ObjectFlags.OBJ_IS_FIXED_FOR_NAVIGATION, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_DONT_AVOID_BY_PEDS, AttrDefaults.OBJ_DONT_AVOID_BY_PEDS, (uint)ObjectFlags.OBJ_DONT_AVOID_BY_PEDS, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_ENABLE_AMBIENT_MULTIPLIER, AttrDefaults.OBJ_ENABLE_AMBIENT_MULTIPLIER, (uint)ObjectFlags.OBJ_ENABLE_AMBIENT_MULTIPLIER, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_IS_DEBUG, AttrDefaults.OBJ_IS_DEBUG, (uint)ObjectFlags.OBJ_IS_DEBUG, ref flags);

			return (flags);
		}

		/// <summary>
		/// Fetch "Gta Object" definition time flags (for IDE file).
		/// </summary>
		/// <param name="obj">Object to fetch flags for</param>
		/// <returns></returns>
		public static UInt32 GetTimeFlags(TargetObjectDef obj, UInt32 flags = 0x00000000)
		{
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_1, AttrDefaults.TOBJ_TIME_1, (uint)TimeObjectFlags.TOBJ_TIME_1, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_2, AttrDefaults.TOBJ_TIME_2, (uint)TimeObjectFlags.TOBJ_TIME_2, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_3, AttrDefaults.TOBJ_TIME_3, (uint)TimeObjectFlags.TOBJ_TIME_3, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_4, AttrDefaults.TOBJ_TIME_4, (uint)TimeObjectFlags.TOBJ_TIME_4, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_5, AttrDefaults.TOBJ_TIME_5, (uint)TimeObjectFlags.TOBJ_TIME_5, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_6, AttrDefaults.TOBJ_TIME_6, (uint)TimeObjectFlags.TOBJ_TIME_6, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_7, AttrDefaults.TOBJ_TIME_7, (uint)TimeObjectFlags.TOBJ_TIME_7, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_8, AttrDefaults.TOBJ_TIME_8, (uint)TimeObjectFlags.TOBJ_TIME_8, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_9, AttrDefaults.TOBJ_TIME_9, (uint)TimeObjectFlags.TOBJ_TIME_9, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_10, AttrDefaults.TOBJ_TIME_10, (uint)TimeObjectFlags.TOBJ_TIME_10, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_11, AttrDefaults.TOBJ_TIME_11, (uint)TimeObjectFlags.TOBJ_TIME_11, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_12, AttrDefaults.TOBJ_TIME_12, (uint)TimeObjectFlags.TOBJ_TIME_12, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_13, AttrDefaults.TOBJ_TIME_13, (uint)TimeObjectFlags.TOBJ_TIME_13, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_14, AttrDefaults.TOBJ_TIME_14, (uint)TimeObjectFlags.TOBJ_TIME_14, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_15, AttrDefaults.TOBJ_TIME_15, (uint)TimeObjectFlags.TOBJ_TIME_15, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_16, AttrDefaults.TOBJ_TIME_16, (uint)TimeObjectFlags.TOBJ_TIME_16, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_17, AttrDefaults.TOBJ_TIME_17, (uint)TimeObjectFlags.TOBJ_TIME_17, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_18, AttrDefaults.TOBJ_TIME_18, (uint)TimeObjectFlags.TOBJ_TIME_18, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_19, AttrDefaults.TOBJ_TIME_19, (uint)TimeObjectFlags.TOBJ_TIME_19, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_20, AttrDefaults.TOBJ_TIME_20, (uint)TimeObjectFlags.TOBJ_TIME_20, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_21, AttrDefaults.TOBJ_TIME_21, (uint)TimeObjectFlags.TOBJ_TIME_21, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_22, AttrDefaults.TOBJ_TIME_22, (uint)TimeObjectFlags.TOBJ_TIME_22, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_23, AttrDefaults.TOBJ_TIME_23, (uint)TimeObjectFlags.TOBJ_TIME_23, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_TIME_24, AttrDefaults.TOBJ_TIME_24, (uint)TimeObjectFlags.TOBJ_TIME_24, ref flags);
			SetAttributeFlag(obj, AttrNames.TOBJ_ALLOW_VANISH_WHILST_VIEWED, AttrDefaults.TOBJ_ALLOW_VANISH_WHILST_VIEWED, (uint)TimeObjectFlags.TOBJ_ALLOW_VANISH_WHILST_VIEWED, ref flags);

			return (flags);
		}

		internal delegate TargetObjectDef ObjectDelegate(string propertyValArrayName, string attrName, TargetObjectDef obj);
		/// <summary>
		/// Fetch "Gta Object" definition time flags (for IDE file).
		/// </summary>
		/// <param name="obj">Object to fetch flags for</param>
		/// <returns></returns>
		internal static UInt32 GetTimeFlags(TargetObjectDef obj, ObjectDelegate objRef, string propArrayName, UInt32 flags = 0x00000000)
		{
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_1, obj), AttrNames.TOBJ_TIME_1, AttrDefaults.TOBJ_TIME_1, (uint)TimeObjectFlags.TOBJ_TIME_1, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_2, obj), AttrNames.TOBJ_TIME_2, AttrDefaults.TOBJ_TIME_2, (uint)TimeObjectFlags.TOBJ_TIME_2, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_3, obj), AttrNames.TOBJ_TIME_3, AttrDefaults.TOBJ_TIME_3, (uint)TimeObjectFlags.TOBJ_TIME_3, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_4, obj), AttrNames.TOBJ_TIME_4, AttrDefaults.TOBJ_TIME_4, (uint)TimeObjectFlags.TOBJ_TIME_4, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_5, obj), AttrNames.TOBJ_TIME_5, AttrDefaults.TOBJ_TIME_5, (uint)TimeObjectFlags.TOBJ_TIME_5, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_6, obj), AttrNames.TOBJ_TIME_6, AttrDefaults.TOBJ_TIME_6, (uint)TimeObjectFlags.TOBJ_TIME_6, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_7, obj), AttrNames.TOBJ_TIME_7, AttrDefaults.TOBJ_TIME_7, (uint)TimeObjectFlags.TOBJ_TIME_7, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_8, obj), AttrNames.TOBJ_TIME_8, AttrDefaults.TOBJ_TIME_8, (uint)TimeObjectFlags.TOBJ_TIME_8, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_9, obj), AttrNames.TOBJ_TIME_9, AttrDefaults.TOBJ_TIME_9, (uint)TimeObjectFlags.TOBJ_TIME_9, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_10, obj), AttrNames.TOBJ_TIME_10, AttrDefaults.TOBJ_TIME_10, (uint)TimeObjectFlags.TOBJ_TIME_10, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_11, obj), AttrNames.TOBJ_TIME_11, AttrDefaults.TOBJ_TIME_11, (uint)TimeObjectFlags.TOBJ_TIME_11, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_12, obj), AttrNames.TOBJ_TIME_12, AttrDefaults.TOBJ_TIME_12, (uint)TimeObjectFlags.TOBJ_TIME_12, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_13, obj), AttrNames.TOBJ_TIME_13, AttrDefaults.TOBJ_TIME_13, (uint)TimeObjectFlags.TOBJ_TIME_13, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_14, obj), AttrNames.TOBJ_TIME_14, AttrDefaults.TOBJ_TIME_14, (uint)TimeObjectFlags.TOBJ_TIME_14, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_15, obj), AttrNames.TOBJ_TIME_15, AttrDefaults.TOBJ_TIME_15, (uint)TimeObjectFlags.TOBJ_TIME_15, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_16, obj), AttrNames.TOBJ_TIME_16, AttrDefaults.TOBJ_TIME_16, (uint)TimeObjectFlags.TOBJ_TIME_16, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_17, obj), AttrNames.TOBJ_TIME_17, AttrDefaults.TOBJ_TIME_17, (uint)TimeObjectFlags.TOBJ_TIME_17, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_18, obj), AttrNames.TOBJ_TIME_18, AttrDefaults.TOBJ_TIME_18, (uint)TimeObjectFlags.TOBJ_TIME_18, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_19, obj), AttrNames.TOBJ_TIME_19, AttrDefaults.TOBJ_TIME_19, (uint)TimeObjectFlags.TOBJ_TIME_19, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_20, obj), AttrNames.TOBJ_TIME_20, AttrDefaults.TOBJ_TIME_20, (uint)TimeObjectFlags.TOBJ_TIME_20, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_21, obj), AttrNames.TOBJ_TIME_21, AttrDefaults.TOBJ_TIME_21, (uint)TimeObjectFlags.TOBJ_TIME_21, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_22, obj), AttrNames.TOBJ_TIME_22, AttrDefaults.TOBJ_TIME_22, (uint)TimeObjectFlags.TOBJ_TIME_22, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_23, obj), AttrNames.TOBJ_TIME_23, AttrDefaults.TOBJ_TIME_23, (uint)TimeObjectFlags.TOBJ_TIME_23, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_TIME_24, obj), AttrNames.TOBJ_TIME_24, AttrDefaults.TOBJ_TIME_24, (uint)TimeObjectFlags.TOBJ_TIME_24, ref flags);
			SetAttributeFlag(objRef(propArrayName, AttrNames.TOBJ_ALLOW_VANISH_WHILST_VIEWED, obj), AttrNames.TOBJ_ALLOW_VANISH_WHILST_VIEWED, AttrDefaults.TOBJ_ALLOW_VANISH_WHILST_VIEWED, (uint)TimeObjectFlags.TOBJ_ALLOW_VANISH_WHILST_VIEWED, ref flags);

			return (flags);
		}

		/// <summary>
		/// Fetch "Gta Object" instance flags (for IPL files).
		/// </summary>
		/// <param name="obj">Object to fetch flags for</param>
		/// <returns></returns>
		internal static UInt32 GetInstanceFlags(TargetObjectDef obj)
		{
			UInt32 flags = 0x00000000;
			SetAttributeFlag(obj, AttrNames.OBJ_LOW_PRIORITY, AttrDefaults.OBJ_LOW_PRIORITY, (uint)InstanceFlags.OBJ_LOW_PRIORITY, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_USE_FULL_MATRIX, AttrDefaults.OBJ_USE_FULL_MATRIX, (uint)InstanceFlags.OBJ_USE_FULL_MATRIX, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_IS_UNDERWATER, AttrDefaults.OBJ_IS_UNDERWATER, (uint)InstanceFlags.OBJ_IS_UNDERWATER, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_IS_TUNNEL, AttrDefaults.OBJ_IS_TUNNEL, (uint)InstanceFlags.OBJ_IS_TUNNEL, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_IS_TUNNEL_TRANSITION, AttrDefaults.OBJ_IS_TUNNEL_TRANSITION, (uint)InstanceFlags.OBJ_IS_TUNNEL_TRANSITION, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_DOES_NOT_TOUCH_WATER, AttrDefaults.OBJ_DOES_NOT_TOUCH_WATER, (uint)InstanceFlags.OBJ_DOES_NOT_TOUCH_WATER, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_DISABLE_PED_SPAWNING, AttrDefaults.OBJ_DISABLE_PED_SPAWNING, (uint)InstanceFlags.OBJ_DISABLE_PED_SPAWNING, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_STATIC_SHADOWS, AttrDefaults.OBJ_STATIC_SHADOWS, (uint)InstanceFlags.OBJ_STATIC_SHADOWS, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_DYNAMIC_SHADOWS, AttrDefaults.OBJ_DYNAMIC_SHADOWS, (uint)InstanceFlags.OBJ_DYNAMIC_SHADOWS, ref flags);
			SetAttributeFlag(obj, AttrNames.OBJ_ON_ALL_DAY, AttrDefaults.OBJ_ON_ALL_DAY, (uint)InstanceFlags.OBJ_ON_ALL_DAY, ref flags);

			// Set instance priority flags
			int priority = obj.GetPriority();
			flags |= ((uint)priority << 10);

			SetAttributeFlag(obj, AttrNames.OBJ_ADOPT_ME, AttrDefaults.OBJ_ADOPT_ME, (uint)InstanceFlags.OBJ_ADOPT_ME, ref flags);

			return (flags);
		}

		/// <summary>
		/// Fetch "Gta Object" extra instance flags (for IPL files).
		/// </summary>
		/// <param name="obj">Object to fetch extra flags for</param>
		/// <returns></returns>
		[Obsolete]
		internal static UInt32 GetInstanceExtraFlags(TargetObjectDef obj)
		{
			UInt32 flags = 0x00000000;
			int lightGroup = obj.GetAttribute(AttrNames.OBJ_LIGHT_GROUP, AttrDefaults.OBJ_LIGHT_GROUP);
			flags |= (((uint)lightGroup) & 0x0000000F); // 4-bits for light group (0-15).

			return (flags);
		}

		/// <summary>
		/// Fetch "Gta Block" flags (for IPL files).
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		internal static UInt32 GetBlockFlags(TargetObjectDef obj)
		{
			UInt32 flags = 0x00000000;
			if (null == obj)
			{
				flags |= (uint)BlockFlags.BLOCK_UNUSED;
			}
			else
			{
				SetAttributeFlag(obj, AttrNames.BLOCK_FIRST_PASS, AttrDefaults.BLOCK_FIRST_PASS, (uint)BlockFlags.BLOCK_FIRST_PASS, ref flags);
				SetAttributeFlag(obj, AttrNames.BLOCK_SECOND_PASS_STARTED, AttrDefaults.BLOCK_SECOND_PASS_STARTED, (uint)BlockFlags.BLOCK_SECOND_PASS_STARTED, ref flags);
				SetAttributeFlag(obj, AttrNames.BLOCK_SECOND_PASS_OUTSOURCED, AttrDefaults.BLOCK_SECOND_PASS_OUTSOURCED, (uint)BlockFlags.BLOCK_SECOND_PASS_OUTSOURCED, ref flags);
				SetAttributeFlag(obj, AttrNames.BLOCK_SECOND_PASS_COMPLETE, AttrDefaults.BLOCK_SECOND_PASS_COMPLETE, (uint)BlockFlags.BLOCK_SECOND_PASS_COMPLETE, ref flags);
				SetAttributeFlag(obj, AttrNames.BLOCK_THIRD_PASS_COMPLETE, AttrDefaults.BLOCK_THIRD_PASS_COMPLETE, (uint)BlockFlags.BLOCK_THIRD_PASS_COMPLETE, ref flags);
				SetAttributeFlag(obj, AttrNames.BLOCK_COMPLETE, AttrDefaults.BLOCK_COMPLETE, (uint)BlockFlags.BLOCK_COMPLETE, ref flags);
			}
			return (flags);
		}

		/// <summary>
		/// Fetch "Gta CarGen" flags (for IPL files).
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		internal static UInt32 GetCarGenFlags(TargetObjectDef obj)
		{
			UInt32 flags = 0x00000000;
			SetAttributeFlag(obj, AttrNames.CARGEN_HIGH_PRIORITY, AttrDefaults.CARGEN_HIGH_PRIORITY, (uint)CarGenFlags.CARGEN_HIGH_PRIORITY, ref flags);
			SetAttributeFlag(obj, AttrNames.CARGEN_IGNORE_POPULATION_LIMIT, AttrDefaults.CARGEN_IGNORE_POPULATION_LIMIT, (uint)CarGenFlags.CARGEN_IGNORE_POPULATION_LIMIT, ref flags);
			SetAttributeFlag(obj, AttrNames.CARGEN_POLICE, AttrDefaults.CARGEN_POLICE, (uint)CarGenFlags.CARGEN_POLICE, ref flags);
			SetAttributeFlag(obj, AttrNames.CARGEN_FIRETRUCK, AttrDefaults.CARGEN_FIRETRUCK, (uint)CarGenFlags.CARGEN_FIRETRUCK, ref flags);
			SetAttributeFlag(obj, AttrNames.CARGEN_AMBULANCE, AttrDefaults.CARGEN_AMBULANCE, (uint)CarGenFlags.CARGEN_AMBULANCE, ref flags);
			SetAttributeFlag(obj, AttrNames.CARGEN_ACTIVE_DAY, AttrDefaults.CARGEN_ACTIVE_DAY, (uint)CarGenFlags.CARGEN_ACTIVE_DAY, ref flags);
			SetAttributeFlag(obj, AttrNames.CARGEN_ACTIVE_NIGHT, AttrDefaults.CARGEN_ACTIVE_NIGHT, (uint)CarGenFlags.CARGEN_ACTIVE_NIGHT, ref flags);
			SetAttributeFlag(obj, AttrNames.CARGEN_ALIGN_LEFT, AttrDefaults.CARGEN_ALIGN_LEFT, (uint)CarGenFlags.CARGEN_ALIGN_LEFT, ref flags);
			SetAttributeFlag(obj, AttrNames.CARGEN_ALIGN_RIGHT, AttrDefaults.CARGEN_ALIGN_RIGHT, (uint)CarGenFlags.CARGEN_ALIGN_RIGHT, ref flags);
			SetAttributeFlag(obj, AttrNames.CARGEN_SINGLE_PLAYER, AttrDefaults.CARGEN_SINGLE_PLAYER, (uint)CarGenFlags.CARGEN_SINGLE_PLAYER, ref flags);
			SetAttributeFlag(obj, AttrNames.CARGEN_NETWORK, AttrDefaults.CARGEN_NETWORK, (uint)CarGenFlags.CARGEN_NETWORK, ref flags);
            SetAttributeFlag(obj, AttrNames.CARGEN_LOW_PRIORITY, AttrDefaults.CARGEN_LOW_PRIORITY, (uint)CarGenFlags.CARGEN_LOW_PRIORITY, ref flags);

			// Creation Rule and Scenario are in MSB of CarGen flags
			uint creationRule = (uint)obj.GetAttribute(AttrNames.CARGEN_CREATION_RULE, AttrDefaults.CARGEN_CREATION_RULE);
			uint scenario = (uint)obj.GetAttribute(AttrNames.CARGEN_SCENARIO, AttrDefaults.CARGEN_SCENARIO);
			flags |= ( 0x0000000F & creationRule ) << 28;
			flags |= ( 0x0000000F & scenario ) << 24;

			return (flags);
		}

        /// <summary>
        /// Fetch "RsDecal" flags.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        internal static UInt32 GetDecalFlags(TargetObjectDef obj)
        {
            UInt32 flags = 0x00000000;
            SetAttributeFlag(obj, AttrNames.TWODFX_DECAL_IGNORE_DAMAGE_MODEL, AttrDefaults.TWODFX_DECAL_IGNORE_DAMAGE_MODEL, (uint)TwodfxDecalFlags.TWODFX_DECAL_IGNORE_DAMAGE_MODEL, ref flags);
            SetAttributeFlag(obj, AttrNames.TWODFX_DECAL_PLAY_ON_PARENT, AttrDefaults.TWODFX_DECAL_PLAY_ON_PARENT, (uint)TwodfxDecalFlags.TWODFX_DECAL_PLAY_ON_PARENT, ref flags);
            SetAttributeFlag(obj, AttrNames.TWODFX_DECAL_ONLY_ON_DAMAGE_MODEL, AttrDefaults.TWODFX_DECAL_ONLY_ON_DAMAGE_MODEL, (uint)TwodfxDecalFlags.TWODFX_DECAL_ONLY_ON_DAMAGE_MODEL, ref flags);
            SetAttributeFlag(obj, AttrNames.TWODFX_DECAL_ALLOW_RUBBER_BULLET_SHOT_FX, AttrDefaults.TWODFX_DECAL_ALLOW_RUBBER_BULLET_SHOT_FX, (uint)TwodfxDecalFlags.TWODFX_DECAL_ALLOW_RUBBER_BULLET_SHOT_FX, ref flags);
            SetAttributeFlag(obj, AttrNames.TWODFX_DECAL_ALLOW_ELECTRIC_BULLET_SHOT_FX, AttrDefaults.TWODFX_DECAL_ALLOW_ELECTRIC_BULLET_SHOT_FX, (uint)TwodfxDecalFlags.TWODFX_DECAL_ALLOW_ELECTRIC_BULLET_SHOT_FX, ref flags);
            
            return (flags);
        }

		/// <summary>
		/// Fetch "Gta Explosion" flags (for IDE files).
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		internal static UInt32 GetExplosionFlags(TargetObjectDef obj)
		{
			UInt32 flags = 0x00000000;
			SetAttributeFlag(obj, AttrNames.TWODFX_EXPLOSION_IGNORE_DAMAGE_MODEL, AttrDefaults.TWODFX_EXPLOSION_IGNORE_DAMAGE_MODEL, (uint)TwodfxExplosionFlags.TWODFX_EXPLOSION_IGNORE_DAMAGE_MODEL, ref flags);
			SetAttributeFlag(obj, AttrNames.TWODFX_EXPLOSION_PLAY_ON_PARENT, AttrDefaults.TWODFX_EXPLOSION_PLAY_ON_PARENT, (uint)TwodfxExplosionFlags.TWODFX_EXPLOSION_PLAY_ON_PARENT, ref flags);
			SetAttributeFlag(obj, AttrNames.TWODFX_EXPLOSION_ONLY_ON_DAMAGE_MODEL, AttrDefaults.TWODFX_EXPLOSION_ONLY_ON_DAMAGE_MODEL, (uint)TwodfxExplosionFlags.TWODFX_EXPLOSION_ONLY_ON_DAMAGE_MODEL, ref flags);
            SetAttributeFlag(obj, AttrNames.TWODFX_EXPLOSION_ALLOW_RUBBER_BULLET_SHOT_FX, AttrDefaults.TWODFX_EXPLOSION_ALLOW_RUBBER_BULLET_SHOT_FX, (uint)TwodfxExplosionFlags.TWODFX_EXPLOSION_ALLOW_RUBBER_BULLET_SHOT_FX, ref flags);
            SetAttributeFlag(obj, AttrNames.TWODFX_EXPLOSION_ALLOW_ELECTRIC_BULLET_SHOT_FX, AttrDefaults.TWODFX_EXPLOSION_ALLOW_ELECTRIC_BULLET_SHOT_FX, (uint)TwodfxExplosionFlags.TWODFX_EXPLOSION_ALLOW_ELECTRIC_BULLET_SHOT_FX, ref flags);
			
            return (flags);
		}

		/// <summary>
		/// Fetch "RAGE Particle" flags (for IPL files).
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		internal static UInt32 GetParticleFlags(TargetObjectDef obj)
		{
			UInt32 flags = 0x00000000;
			SetAttributeFlag(obj, AttrNames.TWODFX_PARTICLE_HAS_TINT, AttrDefaults.TWODFX_PARTICLE_HAS_TINT, (uint)TwodfxParticleFlags.TWODFX_PARTICLE_HAS_TINT, ref flags);
			SetAttributeFlag(obj, AttrNames.TWODFX_PARTICLE_IGNORE_DAMAGE_MODEL, AttrDefaults.TWODFX_PARTICLE_IGNORE_DAMAGE_MODEL, (uint)TwodfxParticleFlags.TWODFX_PARTICLE_IGNORE_DAMAGE_MODEL, ref flags);
			SetAttributeFlag(obj, AttrNames.TWODFX_PARTICLE_PLAY_ON_PARENT, AttrDefaults.TWODFX_PARTICLE_PLAY_ON_PARENT, (uint)TwodfxParticleFlags.TWODFX_PARTICLE_PLAY_ON_PARENT, ref flags);
			SetAttributeFlag(obj, AttrNames.TWODFX_PARTICLE_ONLY_ON_DAMAGE_MODEL, AttrDefaults.TWODFX_PARTICLE_ONLY_ON_DAMAGE_MODEL, (uint)TwodfxParticleFlags.TWODFX_PARTICLE_ONLY_ON_DAMAGE_MODEL, ref flags);
            SetAttributeFlag(obj, AttrNames.TWODFX_PARTICLE_ALLOW_RUBBER_BULLET_SHOT_FX, AttrDefaults.TWODFX_PARTICLE_ALLOW_RUBBER_BULLET_SHOT_FX, (uint)TwodfxParticleFlags.TWODFX_PARTICLE_ALLOW_RUBBER_BULLET_SHOT_FX, ref flags);
            SetAttributeFlag(obj, AttrNames.TWODFX_PARTICLE_ALLOW_ELECTRIC_BULLET_SHOT_FX, AttrDefaults.TWODFX_PARTICLE_ALLOW_ELECTRIC_BULLET_SHOT_FX, (uint)TwodfxParticleFlags.TWODFX_PARTICLE_ALLOW_ELECTRIC_BULLET_SHOT_FX, ref flags);
	
			return (flags);
		}

		/// <summary>
		/// Fetch "Gta Light" / "Gta LightPhoto" flags (2dfx, for IDE/IPL files).
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
        internal static UInt32 GetLightFlags(TargetObjectDef obj)
		{
			UInt32 flags = 0x00000000;
            SetAttributeFlag(obj, AttrNames.TWODFX_LIGHT_CAST_STATIC_SHADOWS, AttrDefaults.TWODFX_LIGHT_CAST_STATIC_SHADOWS, (uint)TwodfxLightFlags.TWODFX_LIGHT_CAST_STATIC_SHADOWS, ref flags);
            SetAttributeFlag(obj, AttrNames.TWODFX_LIGHT_CAST_DYNAMIC_SHADOWS, AttrDefaults.TWODFX_LIGHT_CAST_DYNAMIC_SHADOWS, (uint)TwodfxLightFlags.TWODFX_LIGHT_CAST_DYNAMIC_SHADOWS, ref flags);
            SetAttributeFlag(obj, AttrNames.TWODFX_LIGHT_CALC_FROM_SUNLIGHT, AttrDefaults.TWODFX_LIGHT_CALC_FROM_SUNLIGHT, (uint)TwodfxLightFlags.TWODFX_LIGHT_CALC_FROM_SUNLIGHT, ref flags);
            SetAttributeFlag(obj, AttrNames.TWODFX_LIGHT_ENABLE_BUZZING, AttrDefaults.TWODFX_LIGHT_ENABLE_BUZZING, (uint)TwodfxLightFlags.TWODFX_LIGHT_ENABLE_BUZZING, ref flags);
            SetAttributeFlag(obj, AttrNames.TWODFX_LIGHT_FORCE_BUZZING, AttrDefaults.TWODFX_LIGHT_FORCE_BUZZING, (uint)TwodfxLightFlags.TWODFX_LIGHT_FORCE_BUZZING, ref flags);
            SetAttributeFlag(obj, AttrNames.TWODFX_LIGHT_VOLUME_DRAWING, AttrDefaults.TWODFX_LIGHT_VOLUME_DRAWING, (uint)TwodfxLightFlags.TWODFX_LIGHT_VOLUME_DRAWING, ref flags);
            SetAttributeFlag(obj, AttrNames.TWODFX_LIGHT_NO_SPECULAR, AttrDefaults.TWODFX_LIGHT_NO_SPECULAR, (uint)TwodfxLightFlags.TWODFX_LIGHT_NO_SPECULAR, ref flags);
            SetAttributeFlag(obj, AttrNames.TWODFX_LIGHT_INT_AND_EXT, AttrDefaults.TWODFX_LIGHT_INT_AND_EXT, (uint)TwodfxLightFlags.TWODFX_LIGHT_INT_AND_EXT, ref flags);
            SetAttributeFlag(obj, AttrNames.TWODFX_LIGHT_CORONA_ONLY, AttrDefaults.TWODFX_LIGHT_CORONA_ONLY, (uint)TwodfxLightFlags.TWODFX_LIGHT_ONLY_CORONA, ref flags);

            SetAttributeFlag(obj, AttrNames.TWODFX_LIGHT_CAST_HIGHER_RES_SHADOW, AttrDefaults.TWODFX_LIGHT_CAST_HIGHER_RES_SHADOW, (uint)TwodfxLightFlags.TWODFX_LIGHT_CAST_HIGHER_RES_SHADOW, ref flags);
            SetAttributeFlag(obj, AttrNames.TWODFX_LIGHT_CAST_ONLY_LOWRES_SHADOWS, AttrDefaults.TWODFX_LIGHT_CAST_ONLY_LOWRES_SHADOWS, (uint)TwodfxLightFlags.TWODFX_LIGHT_CAST_ONLY_LOWRES_SHADOWS, ref flags);

            SetAttributeFlag(obj, AttrNames.TWODFX_LIGHT_FORCE_MEDIUM_LOD_LIGHT, AttrDefaults.TWODFX_LIGHT_FORCE_MEDIUM_LOD_LIGHT, (uint)TwodfxLightFlags.TWODFX_LIGHT_FORCE_MEDIUM_LOD_LIGHT, ref flags);
            SetAttributeFlag(obj, AttrNames.TWODFX_LIGHT_CORONA_ONLY_LOD_LIGHT, AttrDefaults.TWODFX_LIGHT_CORONA_ONLY_LOD_LIGHT, (uint)TwodfxLightFlags.TWODFX_LIGHT_CORONA_ONLY_LOD_LIGHT, ref flags);

            // Attribute name was changed, TWODFX_LIGHT_ADD_TO_VIP_LOD_LIGHTS is the new attribute
            if (obj.GetAttribute(AttrNames.TWODFX_LIGHT_ADD_TO_LOD_LIGHTS, AttrDefaults.TWODFX_LIGHT_ADD_TO_LOD_LIGHTS) == false)
            {
                SetAttributeFlag(obj, AttrNames.TWODFX_LIGHT_ADD_TO_VIP_LOD_LIGHTS, AttrDefaults.TWODFX_LIGHT_ADD_TO_VIP_LOD_LIGHTS, (uint)TwodfxLightFlags.TWODFX_LIGHT_ADD_TO_VIP_LOD_LIGHTS, ref flags);
            }
            else
            {
                // AJM: Delete this once the old attribute 'Add to LOD lights' is flushed
                SetAttributeFlag(obj, AttrNames.TWODFX_LIGHT_ADD_TO_LOD_LIGHTS, AttrDefaults.TWODFX_LIGHT_ADD_TO_LOD_LIGHTS, (uint)TwodfxLightFlags.TWODFX_LIGHT_ADD_TO_VIP_LOD_LIGHTS, ref flags);
            }
            SetAttributeFlag(obj, AttrNames.TWODFX_LIGHT_DONT_LIGHT_ALPHA, AttrDefaults.TWODFX_LIGHT_DONT_LIGHT_ALPHA, (uint)TwodfxLightFlags.TWODFX_LIGHT_DONT_LIGHT_ALPHA, ref flags);
            SetAttributeFlag(obj, AttrNames.TWODFX_LIGHT_SHADOWS_IF_POSSIBLE, AttrDefaults.TWODFX_LIGHT_SHADOWS_IF_POSSIBLE, (uint)TwodfxLightFlags.TWODFX_LIGHT_SHADOWS_IF_POSSIBLE, ref flags);

            int renderInReflection = obj.GetAttribute<int>(AttrNames.TWODFX_LIGHT_RENDER_IN_REFLECTION, AttrDefaults.TWODFX_LIGHT_RENDER_IN_REFLECTION);
            if (renderInReflection == 1)
                GenericAddFlag(true, ref flags, (uint)TwodfxLightFlags.TWODFX_LIGHT_DONT_RENDER_IN_REFLECTION);
            else if (renderInReflection == 2)
                GenericAddFlag(true, ref flags, (uint)TwodfxLightFlags.TWODFX_LIGHT_ONLY_RENDER_IN_REFLECTION);

            bool disableInCutscene = obj.GetAttribute(AttrNames.TWODFX_LIGHT_DISABLE_IN_CUTSCENE, AttrDefaults.TWODFX_LIGHT_DISABLE_IN_CUTSCENE);
            if (disableInCutscene == true)
                GenericAddFlag(true, ref flags, (uint)TwodfxLightFlags.TWODFX_LIGHT_DISABLE_IN_CUTSCENE);

            ParamHelpers.SetLightParamFlag(obj, ParamNames.TWODFX_LIGHT_USE_CULLPLANE, ParamDefaults.TWODFX_LIGHT_USE_CULLPLANE, (uint)TwodfxLightFlags.TWODFX_LIGHT_USE_CULLPLANE, ref flags);
            ParamHelpers.SetLightParamFlag(obj, ParamNames.TWODFX_LIGHT_USE_VOL_OUTER_COLOUR, ParamDefaults.TWODFX_LIGHT_USE_VOL_OUTER_COLOUR, (uint)TwodfxLightFlags.TWODFX_LIGHT_USE_VOL_OUTER_COLOUR, ref flags);
            return (flags);
		}

		/// <summary>
		/// Fetch "Gta Object" definition time flags (for IDE file).
		/// </summary>
		/// <param name="obj">Object to fetch flags for</param>
		/// <returns></returns>
        internal static UInt32 GetLightFlags(TargetObjectDef obj, ObjectDelegate objAttrRef, string attrArrayName, ObjectDelegate objPropRef, string propArrayName)
		{
			UInt32 flags = 0x00000000;
			SetAttributeFlag(objAttrRef(attrArrayName, AttrNames.TWODFX_LIGHT_CAST_STATIC_SHADOWS, obj), AttrNames.TWODFX_LIGHT_CAST_STATIC_SHADOWS, AttrDefaults.TWODFX_LIGHT_CAST_STATIC_SHADOWS, (uint)TwodfxLightFlags.TWODFX_LIGHT_CAST_STATIC_SHADOWS, ref flags);
			SetAttributeFlag(objAttrRef(attrArrayName, AttrNames.TWODFX_LIGHT_CAST_DYNAMIC_SHADOWS, obj), AttrNames.TWODFX_LIGHT_CAST_DYNAMIC_SHADOWS, AttrDefaults.TWODFX_LIGHT_CAST_DYNAMIC_SHADOWS, (uint)TwodfxLightFlags.TWODFX_LIGHT_CAST_DYNAMIC_SHADOWS, ref flags);
			SetAttributeFlag(objAttrRef(attrArrayName, AttrNames.TWODFX_LIGHT_CALC_FROM_SUNLIGHT, obj), AttrNames.TWODFX_LIGHT_CALC_FROM_SUNLIGHT, AttrDefaults.TWODFX_LIGHT_CALC_FROM_SUNLIGHT, (uint)TwodfxLightFlags.TWODFX_LIGHT_CALC_FROM_SUNLIGHT, ref flags);
			SetAttributeFlag(objAttrRef(attrArrayName, AttrNames.TWODFX_LIGHT_ENABLE_BUZZING, obj), AttrNames.TWODFX_LIGHT_ENABLE_BUZZING, AttrDefaults.TWODFX_LIGHT_ENABLE_BUZZING, (uint)TwodfxLightFlags.TWODFX_LIGHT_ENABLE_BUZZING, ref flags);
			SetAttributeFlag(objAttrRef(attrArrayName, AttrNames.TWODFX_LIGHT_FORCE_BUZZING, obj), AttrNames.TWODFX_LIGHT_FORCE_BUZZING, AttrDefaults.TWODFX_LIGHT_FORCE_BUZZING, (uint)TwodfxLightFlags.TWODFX_LIGHT_FORCE_BUZZING, ref flags);
			SetAttributeFlag(objAttrRef(attrArrayName, AttrNames.TWODFX_LIGHT_VOLUME_DRAWING, obj), AttrNames.TWODFX_LIGHT_VOLUME_DRAWING, AttrDefaults.TWODFX_LIGHT_VOLUME_DRAWING, (uint)TwodfxLightFlags.TWODFX_LIGHT_VOLUME_DRAWING, ref flags);
			SetAttributeFlag(objAttrRef(attrArrayName, AttrNames.TWODFX_LIGHT_NO_SPECULAR, obj), AttrNames.TWODFX_LIGHT_NO_SPECULAR, AttrDefaults.TWODFX_LIGHT_NO_SPECULAR, (uint)TwodfxLightFlags.TWODFX_LIGHT_NO_SPECULAR, ref flags);
            SetAttributeFlag(objAttrRef(attrArrayName, AttrNames.TWODFX_LIGHT_INT_AND_EXT, obj), AttrNames.TWODFX_LIGHT_INT_AND_EXT, AttrDefaults.TWODFX_LIGHT_INT_AND_EXT, (uint)TwodfxLightFlags.TWODFX_LIGHT_INT_AND_EXT, ref flags);

            SetAttributeFlag(objAttrRef(attrArrayName, AttrNames.TWODFX_LIGHT_CORONA_ONLY, obj), AttrNames.TWODFX_LIGHT_CORONA_ONLY, AttrDefaults.TWODFX_LIGHT_CORONA_ONLY, (uint)TwodfxLightFlags.TWODFX_LIGHT_ONLY_CORONA, ref flags);

            SetAttributeFlag(objAttrRef(attrArrayName, AttrNames.TWODFX_LIGHT_CAST_HIGHER_RES_SHADOW, obj), AttrNames.TWODFX_LIGHT_CAST_HIGHER_RES_SHADOW, AttrDefaults.TWODFX_LIGHT_CAST_HIGHER_RES_SHADOW, (uint)TwodfxLightFlags.TWODFX_LIGHT_CAST_HIGHER_RES_SHADOW, ref flags);
            SetAttributeFlag(objAttrRef(attrArrayName, AttrNames.TWODFX_LIGHT_CAST_ONLY_LOWRES_SHADOWS, obj), AttrNames.TWODFX_LIGHT_CAST_ONLY_LOWRES_SHADOWS, AttrDefaults.TWODFX_LIGHT_CAST_ONLY_LOWRES_SHADOWS, (uint)TwodfxLightFlags.TWODFX_LIGHT_CAST_ONLY_LOWRES_SHADOWS, ref flags);

            SetAttributeFlag(objAttrRef(attrArrayName, AttrNames.TWODFX_LIGHT_FORCE_MEDIUM_LOD_LIGHT, obj), AttrNames.TWODFX_LIGHT_FORCE_MEDIUM_LOD_LIGHT, AttrDefaults.TWODFX_LIGHT_FORCE_MEDIUM_LOD_LIGHT, (uint)TwodfxLightFlags.TWODFX_LIGHT_FORCE_MEDIUM_LOD_LIGHT, ref flags);
            SetAttributeFlag(objAttrRef(attrArrayName, AttrNames.TWODFX_LIGHT_CORONA_ONLY_LOD_LIGHT, obj), AttrNames.TWODFX_LIGHT_CORONA_ONLY_LOD_LIGHT, AttrDefaults.TWODFX_LIGHT_CORONA_ONLY_LOD_LIGHT, (uint)TwodfxLightFlags.TWODFX_LIGHT_CORONA_ONLY_LOD_LIGHT, ref flags);

            int renderInReflection = objAttrRef(attrArrayName, AttrNames.TWODFX_LIGHT_RENDER_IN_REFLECTION, obj).GetProperty<int>(AttrNames.TWODFX_LIGHT_RENDER_IN_REFLECTION, AttrDefaults.TWODFX_LIGHT_RENDER_IN_REFLECTION);
            if (renderInReflection == 1)
                GenericAddFlag(true, ref flags, (uint)TwodfxLightFlags.TWODFX_LIGHT_DONT_RENDER_IN_REFLECTION);
            else if (renderInReflection == 2)
                GenericAddFlag(true, ref flags, (uint)TwodfxLightFlags.TWODFX_LIGHT_ONLY_RENDER_IN_REFLECTION);

            bool disableInCutscene = obj.GetAttribute(AttrNames.TWODFX_LIGHT_DISABLE_IN_CUTSCENE, AttrDefaults.TWODFX_LIGHT_DISABLE_IN_CUTSCENE);
            if (disableInCutscene == true)
                GenericAddFlag(true, ref flags, (uint)TwodfxLightFlags.TWODFX_LIGHT_DISABLE_IN_CUTSCENE);

            // These only ever exist on Ragelights.
            TargetObjectDef instObj = objPropRef(propArrayName, ParamNames.TWODFX_LIGHT_USE_CULLPLANE[0], obj);
            ParamHelpers.SetLightParamFlag(instObj, ParamNames.TWODFX_LIGHT_USE_CULLPLANE, ParamDefaults.TWODFX_LIGHT_USE_CULLPLANE, (uint)TwodfxLightFlags.TWODFX_LIGHT_USE_CULLPLANE, ref flags);
            instObj = objPropRef(propArrayName, ParamNames.TWODFX_LIGHT_USE_VOL_OUTER_COLOUR[0], obj);
            ParamHelpers.SetLightParamFlag(instObj, ParamNames.TWODFX_LIGHT_USE_VOL_OUTER_COLOUR, ParamDefaults.TWODFX_LIGHT_USE_VOL_OUTER_COLOUR, (uint)TwodfxLightFlags.TWODFX_LIGHT_USE_VOL_OUTER_COLOUR, ref flags);

			return (flags);
		}

		   /// <summary>
		/// fetch "Gta LightShaft" flags (2dfx, for IDE/IPL files)
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
        internal static UInt32 GetLightShaftFlags(TargetObjectDef obj)
		{
			UInt32 flags = 0x00000000;
			SetAttributeFlag(obj, AttrNames.TWODFX_LIGHTSHAFT_USE_SUN_LIGHT_DIRECTION, AttrDefaults.TWODFX_LIGHTSHAFT_USE_SUN_LIGHT_DIRECTION, (uint)TwodfxLightShaftFlags.TWODFX_LIGHTSHAFT_USE_SUN_LIGHT_DIRECTION, ref flags);
			SetAttributeFlag(obj, AttrNames.TWODFX_LIGHTSHAFT_USE_SUN_LIGHT_COLOUR, AttrDefaults.TWODFX_LIGHTSHAFT_USE_SUN_LIGHT_COLOUR, (uint)TwodfxLightShaftFlags.TWODFX_LIGHTSHAFT_USE_SUN_LIGHT_COLOUR, ref flags);
// 			SetAttributeFlag(obj, AttrNames.TWODFX_LIGHTSHAFT_SHADOWED, AttrDefaults.TWODFX_LIGHTSHAFT_SHADOWED, (uint)TwodfxLightShaftFlags.TWODFX_LIGHTSHAFT_SHADOWED, ref flags);
// 			SetAttributeFlag(obj, AttrNames.TWODFX_LIGHTSHAFT_PER_PIXEL_SHADOWED, AttrDefaults.TWODFX_LIGHTSHAFT_PER_PIXEL_SHADOWED, (uint)TwodfxLightShaftFlags.TWODFX_LIGHTSHAFT_PER_PIXEL_SHADOWED, ref flags);
            SetAttributeFlag(obj, AttrNames.TWODFX_LIGHTSHAFT_SCALE_BY_SUN_COLOUR, AttrDefaults.TWODFX_LIGHTSHAFT_SCALE_BY_SUN_COLOUR, (uint)TwodfxLightShaftFlags.TWODFX_LIGHTSHAFT_SCALE_BY_SUN_COLOUR, ref flags);
            SetAttributeFlag(obj, AttrNames.TWODFX_LIGHTSHAFT_SCALE_BY_SUN_INTENSITY, AttrDefaults.TWODFX_LIGHTSHAFT_SCALE_BY_SUN_INTENSITY, (uint)TwodfxLightShaftFlags.TWODFX_LIGHTSHAFT_SCALE_BY_SUN_INTENSITY, ref flags);
            SetAttributeFlag(obj, AttrNames.TWODFX_LIGHTSHAFT_DRAW_IN_FRONT_AND_BEHIND, AttrDefaults.TWODFX_LIGHTSHAFT_DRAW_IN_FRONT_AND_BEHIND, (uint)TwodfxLightShaftFlags.TWODFX_LIGHTSHAFT_DRAW_IN_FRONT_AND_BEHIND, ref flags);
            SetAttributeFlag(obj, AttrNames.TWODFX_LIGHTSHAFT_SQUARED_OFF_ENDCAP, AttrDefaults.TWODFX_LIGHTSHAFT_SQUARED_OFF_ENDCAP, (uint)TwodfxLightShaftFlags.TWODFX_LIGHTSHAFT_SQUARED_OFF_ENDCAP, ref flags);
			return (flags);
		}

		/// <summary>
		/// Fetch "Gta Milo" flags (for IDE file).
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
        internal static UInt32 GetMiloFlags(TargetObjectDef obj)
		{
			UInt32 flags = GetBlockFlags(obj);
			SetAttributeFlag(obj, AttrNames.MILO_PROPOGATE_INSTANCING, AttrDefaults.MILO_PROPOGATE_INSTANCING, (uint)MiloFlags.MILO_PROPOGATE_INSTANCING, ref flags);
			SetAttributeFlag(obj, AttrNames.MILO_ALLOW_SPRINTING, AttrDefaults.MILO_ALLOW_SPRINTING, (uint)MiloFlags.MILO_ALLOW_SPRINTING, ref flags);
			SetAttributeFlag(obj, AttrNames.MILO_CUTSCENE_ONLY, AttrDefaults.MILO_CUTSCENE_ONLY, (uint)MiloFlags.MILO_CUTSCENE_ONLY, ref flags);
			SetAttributeFlag(obj, AttrNames.MILO_LOD_WHEN_LOCKED, AttrDefaults.MILO_LOD_WHEN_LOCKED, (uint)MiloFlags.MILO_LOD_WHEN_LOCKED, ref flags);
			SetAttributeFlag(obj, AttrNames.MILO_NO_WATER_REFLECTION, AttrDefaults.MILO_NO_WATER_REFLECTION, (uint)MiloFlags.MILO_NO_WATER_REFLECTION, ref flags);
			SetAttributeFlag(obj, AttrNames.MILO_DONT_DRAW_IF_LOCKED, AttrDefaults.MILO_DONT_DRAW_IF_LOCKED, (uint)MiloFlags.MILO_DONT_DRAW_IF_LOCKED, ref flags);

			return (flags);
		}

        internal static UInt32 GetMiloInstanceFlags(TargetObjectDef obj)
        {
            UInt32 flags = 0;

            // MILO instance flags may come from a milo (in-situ interiors) or an milo-tri (referenced instances)
            if (obj.IsMilo())
            {
                SetAttributeFlag(obj, AttrNames.MILO_INSTANCE_TURN_ON_GPS, AttrDefaults.MILO_INSTANCE_TURN_ON_GPS, 
                    (UInt32)MiloInstanceFlags.MILO_INSTANCE_TURN_ON_GPS, ref flags);
                SetAttributeFlag(obj, AttrNames.MILO_INSTANCE_CAP_ALPHA, AttrDefaults.MILO_INSTANCE_CAP_ALPHA,
                    (UInt32)MiloInstanceFlags.MILO_INSTANCE_CAP_ALPHA, ref flags);
                SetAttributeFlag(obj, AttrNames.MILO_INSTANCE_SHORT_FADE_DISTANCE, AttrDefaults.MILO_INSTANCE_SHORT_FADE_DISTANCE,
                    (UInt32)MiloInstanceFlags.MILO_INSTANCE_SHORT_FADE_DISTANCE, ref flags);
            }
            else if (obj.IsRefObject())
            {
                SetAttributeFlag(obj, AttrNames.OBJ_MILO_INSTANCE_TURN_ON_GPS, AttrDefaults.OBJ_MILO_INSTANCE_TURN_ON_GPS,
                    (UInt32)MiloInstanceFlags.MILO_INSTANCE_TURN_ON_GPS, ref flags);
                SetAttributeFlag(obj, AttrNames.OBJ_MILO_INSTANCE_CAP_ALPHA, AttrDefaults.OBJ_MILO_INSTANCE_CAP_ALPHA,
                    (UInt32)MiloInstanceFlags.MILO_INSTANCE_CAP_ALPHA, ref flags);
                SetAttributeFlag(obj, AttrNames.OBJ_MILO_INSTANCE_SHORT_FADE_DISTANCE, AttrDefaults.OBJ_MILO_INSTANCE_SHORT_FADE_DISTANCE,
                    (UInt32)MiloInstanceFlags.MILO_INSTANCE_SHORT_FADE_DISTANCE, ref flags);
            }

            return flags;
        }

		/// <summary>
		/// Fetch "Gta MloPortal" flags (for IDE file).
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
        internal static UInt32 GetMloPortalFlags(TargetObjectDef obj)
        {
            UInt32 flags = 0x00000000;
			SetAttributeFlag(obj, AttrNames.MLOPORTAL_ONE_WAY, AttrDefaults.MLOPORTAL_ONE_WAY, (uint)MloPortalFlags.MLOPORTAL_ONE_WAY, ref flags);
			SetAttributeFlag(obj, AttrNames.MLOPORTAL_MLO_LINK, AttrDefaults.MLOPORTAL_MLO_LINK, (uint)MloPortalFlags.MLOPORTAL_MLO_LINK, ref flags);
			SetAttributeFlag(obj, AttrNames.MLOPORTAL_IGNORE_BY_MODIFIER, AttrDefaults.MLOPORTAL_IGNORE_BY_MODIFIER, (uint)MloPortalFlags.MLOPORTAL_IGNORE_BY_MODIFIER, ref flags);
			SetAttributeFlag(obj, AttrNames.MLOPORTAL_EXPENSIVE_SHADERS, AttrDefaults.MLOPORTAL_EXPENSIVE_SHADERS, (uint)MloPortalFlags.MLOPORTAL_EXPENSIVE_SHADERS, ref flags);
			SetAttributeFlag(obj, AttrNames.MLOPORTAL_DRAW_LOW_LOD_ONLY, AttrDefaults.MLOPORTAL_DRAW_LOW_LOD_ONLY, (uint)MloPortalFlags.MLOPORTAL_DRAW_LOW_LOD_ONLY, ref flags);
			SetAttributeFlag(obj, AttrNames.MLOPORTAL_ALLOW_CLOSING, AttrDefaults.MLOPORTAL_ALLOW_CLOSING, (uint)MloPortalFlags.MLOPORTAL_ALLOW_CLOSING, ref flags);
            SetAttributeFlag(obj, AttrNames.MLOPORTAL_MIRROR_USES_DIRECTIONAL_LIGHT, AttrDefaults.MLOPORTAL_MIRROR_USES_DIRECTIONAL_LIGHT, (uint)MloPortalFlags.MLOPORTAL_MIRROR_CAN_SEE_DIRECTIONAL_LIGHT, ref flags);
            SetAttributeFlag(obj, AttrNames.MLOPORTAL_MIRROR_PORTAL_TRAVERSAL, AttrDefaults.MLOPORTAL_MIRROR_PORTAL_TRAVERSAL, (uint)MloPortalFlags.MLOPORTAL_USE_PORTAL_TRAVERSAL, ref flags);
            SetAttributeFlag(obj, AttrNames.MLOPORTAL_MIRROR_CAN_SEE_EXTERIOR_VIEW, AttrDefaults.MLOPORTAL_MIRROR_CAN_SEE_EXTERIOR_VIEW, (uint)MloPortalFlags.MLOPORTAL_MIRROR_CAN_SEE_EXTERIOR_VIEW, ref flags);
            SetAttributeFlag(obj, AttrNames.MLOPORTAL_USE_LIGHT_BLEED, AttrDefaults.MLOPORTAL_USE_LIGHT_BLEED, (uint)MloPortalFlags.MLOPORTAL_USE_LIGHT_BLEED, ref flags);

            // AJM: Need this to be backwards compatible with old mirror data, which still has these attributes in the scene xml
            // The newer changes only have the 'Mirror Type' attribute set which is used below.
            if (obj.GetAttribute(AttrNames.MLOPORTAL_MIRROR, AttrDefaults.MLOPORTAL_MIRROR))
                SetAttributeFlag(obj, AttrNames.MLOPORTAL_MIRROR, AttrDefaults.MLOPORTAL_MIRROR, (uint)MloPortalFlags.MLOPORTAL_MIRROR, ref flags);
            if (obj.GetAttribute(AttrNames.MLOPORTAL_MIRROR_FLOOR, AttrDefaults.MLOPORTAL_MIRROR_FLOOR))
                SetAttributeFlag(obj, AttrNames.MLOPORTAL_MIRROR_FLOOR, AttrDefaults.MLOPORTAL_MIRROR_FLOOR, (uint)MloPortalFlags.MLOPORTAL_MIRROR_FLOOR, ref flags);

            // Mirror type flags following logic from bug 736701:
            // 0 = none
            // 1 = MIRROR
            // 2 = MIRROR+MIRROR_FLOOR
            // 3 = MIRROR+MIRROR_FLOOR+WATER_SURFACE
            // 4 = MIRROR+MIRROR_FLOOR+WATER_SURFACE_EXTEND_TO_HORIZON
            int portalMirrorTypeValue = obj.GetAttribute(AttrNames.MLOPORTAL_MIRROR_TYPE, AttrDefaults.MLOPORTAL_MIRROR_TYPE);
            MLOPortalMirrorType portalMirrorType = (MLOPortalMirrorType)portalMirrorTypeValue;

            Debug.Assert(portalMirrorType >= MLOPortalMirrorType.NONE && portalMirrorType < MLOPortalMirrorType.COUNT, "Invalid MLO Portal Mirror Type Enum Value!");
            switch (portalMirrorType)
            {
                case MLOPortalMirrorType.MIRROR:
                    flags |= (uint)MloPortalFlags.MLOPORTAL_MIRROR;
                    break;

                case MLOPortalMirrorType.MIRROR_FLOOR:
                    flags |= (uint)MloPortalFlags.MLOPORTAL_MIRROR;
                    flags |= (uint)MloPortalFlags.MLOPORTAL_MIRROR_FLOOR;                    
                    break;

                case MLOPortalMirrorType.MIRROR_WATER_SURFACE:
                    flags |= (uint)MloPortalFlags.MLOPORTAL_MIRROR;
                    flags |= (uint)MloPortalFlags.MLOPORTAL_MIRROR_FLOOR;
                    flags |= (uint)MloPortalFlags.MLOPORTAL_MIRROR_WATER_SURFACE; 
                    break;

                case MLOPortalMirrorType.MIRROR_WATER_SURFACE_EXTEND_TO_HORIZON:
                    flags |= (uint)MloPortalFlags.MLOPORTAL_MIRROR;
                    flags |= (uint)MloPortalFlags.MLOPORTAL_MIRROR_FLOOR;
                    flags |= (uint)MloPortalFlags.MLOPORTAL_MIRROR_WATER_SURFACE_EXTEND_TO_HORIZON;
                    break;

                case MLOPortalMirrorType.NONE:
                default:                       
                    break;
            }
            return (flags);
		}

		/// <summary>
		/// Fetch "Gta MloRoom" Flags (for IDE file).
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
        internal static UInt32 GetMloRoomFlags(TargetObjectDef obj)
		{
			UInt32 flags = 0x00000000;
			SetAttributeFlag(obj, AttrNames.MLOROOM_FREEZE_CARS, AttrDefaults.MLOROOM_FREEZE_CARS, (uint)MloRoomFlags.MLOROOM_FREEZE_CARS, ref flags);
			SetAttributeFlag(obj, AttrNames.MLOROOM_FREEZE_PEDS, AttrDefaults.MLOROOM_FREEZE_PEDS, (uint)MloRoomFlags.MLOROOM_FREEZE_PEDS, ref flags);
			SetAttributeFlag(obj, AttrNames.MLOROOM_NO_DIRECTIONAL_LIGHT, AttrDefaults.MLOROOM_NO_DIRECTIONAL_LIGHT, (uint)MloRoomFlags.MLOROOM_NO_DIRECTIONAL_LIGHT, ref flags);
			SetAttributeFlag(obj, AttrNames.MLOROOM_NO_EXTERIOR_LIGHT, AttrDefaults.MLOROOM_NO_EXTERIOR_LIGHT, (uint)MloRoomFlags.MLOROOM_NO_EXTERIOR_LIGHT, ref flags);
			SetAttributeFlag(obj, AttrNames.MLOROOM_FORCE_FREEZE, AttrDefaults.MLOROOM_FORCE_FREEZE, (uint)MloRoomFlags.MLOROOM_FORCE_FREEZE, ref flags);
			SetAttributeFlag(obj, AttrNames.MLOROOM_REDUCE_CARS, AttrDefaults.MLOROOM_REDUCE_CARS, (uint)MloRoomFlags.MLOROOM_REDUCE_CARS, ref flags);
			SetAttributeFlag(obj, AttrNames.MLOROOM_REDUCE_PEDS, AttrDefaults.MLOROOM_REDUCE_PEDS, (uint)MloRoomFlags.MLOROOM_REDUCE_PEDS, ref flags);
            SetAttributeFlag(obj, AttrNames.MLOROOM_FORCE_DIRECTIONAL_LIGHT, AttrDefaults.MLOROOM_FORCE_DIRECTIONAL_LIGHT, (uint)MloRoomFlags.MLOROOM_FORCE_DIRECTIONAL_LIGHT, ref flags);
            SetAttributeFlag(obj, AttrNames.MLOROOM_DONT_RENDER_EXTERIOR, AttrDefaults.MLOROOM_DONT_RENDER_EXTERIOR, (uint)MloRoomFlags.MLOROOM_DONT_RENDER_EXTERIOR, ref flags);
            SetAttributeFlag(obj, AttrNames.MLOROOM_MIRROR_POTENTIALLY_VISIBLE, AttrDefaults.MLOROOM_MIRROR_POTENTIALLY_VISIBLE, (uint)MloRoomFlags.MLOROOM_MIRROR_POTENTIALLY_VISIBLE, ref flags);

			return (flags);
		}

		/// <summary>
		/// Fetch Milo Entry flags (for IDE file).
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
        internal static UInt32 GetMiloEntryFlags(TargetObjectDef obj)
		{
			UInt32 flags = 0x00000000;
			SetAttributeFlag(obj, AttrNames.OBJ_HOLD_FOR_LOD, AttrDefaults.OBJ_HOLD_FOR_LOD, (uint)MiloEntryFlags.OBJ_HOLD_FOR_LOD, ref flags);

			return (flags);
		}

		/// <summary>
		/// Fetch "Gta ProcObject" Flags (for IDE file).
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
        internal static UInt32 GetProcObjectFlags(TargetObjectDef obj)
		{
			UInt32 flags = 0x00000000;
			SetAttributeFlag(obj, AttrNames.TWODFX_PROC_IS_ALIGNED, AttrDefaults.TWODFX_PROC_IS_ALIGNED, (uint)TwodfxProcFlags.TWODFX_PROC_ALIGNED, ref flags);
			
			return (flags);
		}

		/// <summary>
		/// Fetch "VehicleLink" Flags (for IDE file).
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
        internal static UInt32 GetVehicleLinkFlags(TargetObjectDef obj)
		{
			UInt32 flags = 0x00000000;
			SetAttributeFlag(obj, AttrNames.VEHLNK_NARROW_ROAD, AttrDefaults.VEHLNK_NARROW_ROAD, (uint)VehicleLinkFlags.VEHLNK_NARROW_ROAD, ref flags);
			SetAttributeFlag(obj, AttrNames.VEHLNK_GPS_BOTH_WAYS, AttrDefaults.VEHLNK_GPS_BOTH_WAYS, (uint)VehicleLinkFlags.VEHLNK_GPS_BOTH_WAYS, ref flags);
            SetAttributeFlag(obj, AttrNames.VEHLNK_BLOCK_IF_NO_LANES, AttrDefaults.VEHLNK_BLOCK_IF_NO_LANES, (uint)VehicleLinkFlags.VEHLNK_BLOCK_IF_NO_LANES, ref flags);
            SetAttributeFlag(obj, AttrNames.VEHLNK_SHORTCUT, AttrDefaults.VEHLNK_SHORTCUT, (uint)VehicleLinkFlags.VEHLNK_SHORTCUT, ref flags);
            SetAttributeFlag(obj, AttrNames.VEHLNK_NONAVIGATION, AttrDefaults.VEHLNK_NONAVIGATION, (uint)VehicleLinkFlags.VEHLNK_NONAVIGATION, ref flags);
			return (flags);
		}

        /// <summary>
        /// Fetch "RsWindDisturbance" flags.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        internal static UInt32 GetWindDisturbanceFlags(TargetObjectDef obj)
        {
            UInt32 flags = 0x00000000;
            SetAttributeFlag(obj, AttrNames.TWODFX_WIND_STRENGTH_MULTIPLIES_GLOBAL_WIND, AttrDefaults.TWODFX_WIND_STRENGTH_MULTIPLIES_GLOBAL_WIND,
                (uint)TwodfxWindDisturbanceFlags.TWODFX_WIND_STRENGTH_MULTIPLIES_GLOBAL_WIND, ref flags);
            return (flags);
        }

		/// <summary>
		/// Fetch "Gta LodModifier" Flags (for IDE file).
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
        internal static UInt32 GetLodModifierFlags(TargetObjectDef obj)
		{
			UInt32 flags = 0x00000000;
			SetAttributeFlag(obj, AttrNames.LODMODIFIER_CULL_WATER, AttrDefaults.LODMODIFIER_CULL_WATER, (uint)LodModifierFlags.LODMODIFIER_CULLWATER, ref flags);

			return (flags);
		}


        /// <summary>
		/// Generic Set flag if given boolean is true.
		/// </summary>
		/// <param name="doIt"></param>
		/// <param name="flags"></param>
		/// <param name="bitOffset"></param>
        internal static void GenericAddFlag(bool doIt, ref UInt32 flags, uint bitOffset)
        {
			if (doIt)
				flags |= bitOffset;
			else
				flags &= ~bitOffset;
        }

        /// <summary>
		/// Set attribute flag if attribute is true.
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="attr"></param>
		/// <param name="defVal"></param>
		/// <param name="flag"></param>
		/// <param name="flags"></param>
        internal static void SetAttributeFlag(SceneXml.TargetObjectDef obj, String attr, bool defVal, UInt32 flag, ref UInt32 flags)
		{
			bool attrVal = obj.GetAttribute<bool>(attr, defVal);
			if (attrVal)
				flags |= flag;
			else
				flags &= ~flag;
		}

        /// <summary>
        /// Set flag if property is true.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="attr"></param>
        /// <param name="defVal"></param>
        /// <param name="flag"></param>
        /// <param name="flags"></param>
        internal static void SetPropertyFlag(SceneXml.TargetObjectDef obj, String prop, bool defVal, UInt32 flag, ref UInt32 flags)
        {
            bool attrVal = defVal;
            object propType = obj.GetProperty(prop);
            if (null != propType)
            {
                if (propType.GetType() == typeof(bool))
                {
                    attrVal = obj.GetProperty<bool>(prop, defVal);
                }
                else if (propType.GetType() == typeof(int))
                {
                    attrVal = obj.GetProperty<int>(prop, defVal ? 1 : 0) != 0;
                }
                else
                {
                    Log.Log__ErrorCtx(prop, "Trying to parse property with name {0} as bool or int, but found value of type {1}!", prop, propType.ToString());
                    return;
                }
            }
            if (attrVal)
                flags |= flag;
            else
                flags &= ~flag;
        }

        /// <summary>
        /// Set flag if property is true.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="attr"></param>
        /// <param name="defVal"></param>
        /// <param name="flag"></param>
        /// <param name="flags"></param>
        internal static void SetParamFlag(SceneXml.TargetObjectDef obj, String prop, bool defVal, UInt32 flag, ref UInt32 flags)
        {
            bool attrVal = obj.GetParameter(prop, defVal);
            if (attrVal)
                flags |= flag;
            else
                flags &= ~flag;
        }

        /// <summary>
		/// Set attribute flag if attribute if false.
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="attr"></param>
		/// <param name="defVal"></param>
		/// <param name="flag"></param>
		/// <param name="flags"></param>
        internal static void SetAttributeFlagNegative(SceneXml.TargetObjectDef obj, String attr, bool defVal, UInt32 flag, ref UInt32 flags)
		{
			bool attrVal = obj.GetAttribute<bool>(attr, defVal);
			if (!attrVal)
				flags |= flag;
		}
		#endregion // Static Methods
	}

} // RSG.SceneXml.MapExport namespace

// IDEFlags
