//
// File: Definition.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Definition class
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Base.Math;
// ObjectContainer alias used when pre-passing MILO ObjectDefs.
using MiloPortalContainer = System.Collections.Generic.Dictionary<RSG.SceneXml.TargetObjectDef, RSG.SceneXml.MapExport.MiloPortal>;
using MiloRoomContainer = System.Collections.Generic.Dictionary<RSG.SceneXml.TargetObjectDef, RSG.SceneXml.MapExport.MiloRoom>;
using SMath = System.Math;

namespace RSG.SceneXml.MapExport
{

    #region IDE File 2dfx Type Integers
    /// <summary>
    /// 
    /// </summary>
    internal static class IDE2dfxTypes
    {
        public static readonly int TWODFX_LIGHT = 0;
        public static readonly int TWODFX_LIGHT_PHOTO = 0;
        public static readonly int TWODFX_PARTICLE_EFFECT = 1;
        public static readonly int TWODFX_EXPLOSION_EFFECT = 2;
        // Types 3 .. 11 inclusive are old and no longer used.
        public static readonly int TWODFX_PROC_OBJECT = 12;
        public static readonly int TWODFX_SCRIPT = 13;
        public static readonly int TWODFX_LADDER = 14;
        public static readonly int TWODFX_AUDIO_EMITTER = 15;
        public static readonly int TWODFX_SPAWN_POINT = 17;
        public static readonly int TWODFX_LIGHT_SHAFT = 18;
        public static readonly int TWODFX_SCROLLBARS = 19;
        // Type 20 is no longer used
        public static readonly int TWODFX_SWAYABLE_EFFECT = 21;
        public static readonly int TWODFX_BUOYANCY = 22;
        public static readonly int TWODFX_WALK_DONT_WALK = 23; 
        public static readonly int TWODFX_LEDGE = 24;
    }

    internal static class META2dfxTypes
    {
        public static readonly int TWODFX_PARTICLE_EFFECT = 0;
        //public static readonly int TWODFX_LIGHT = 1;
        //public static readonly int TWODFX_LIGHT_PHOTO = 1;
        //public static readonly int TWODFX_EXPLOSION_EFFECT = 2;
        //// Types 3 .. 11 inclusive are old and no longer used.
        //public static readonly int TWODFX_PROC_OBJECT = 12;
        //public static readonly int TWODFX_SCRIPT = 13;
        //public static readonly int TWODFX_LADDER = 14;
        //public static readonly int TWODFX_AUDIO_EMITTER = 15;
        //public static readonly int TWODFX_SPAWN_POINT = 17;
        //public static readonly int TWODFX_LIGHT_SHAFT = 18;
        //public static readonly int TWODFX_SCROLLBARS = 19;
        //// Type 20 is no longer used
        //public static readonly int TWODFX_SWAYABLE_EFFECT = 21;
        //public static readonly int TWODFX_BUOYANCY = 22;
        //public static readonly int TWODFX_WALK_DONT_WALK = 23; 
        //public static readonly int TWODFX_LEDGE = 24;
    }

    #endregion // IDE File 2dfx Type Integers

    /// <summary>
    /// Object Definition
    /// </summary>
    internal class Definition
    {
        #region Constants
        private static readonly String LIMBO = "limbo";
        #endregion // Constants

        #region Properties and Associated Member Data
        /// <summary>
        /// Instance4 object definition within SceneXml Scene.
        /// </summary>
        public TargetObjectDef Object
        {
            get { return m_Object;}
        }
        private TargetObjectDef m_Object;
        #endregion // Properties and Associated Member Data

        #region Member Data
        /// <summary>
        /// Reference to our SceneXml Scene object.
        /// </summary>
        private Scene m_Scene;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="scene">SceneXml.Scene containing definition</param>
        /// <param name="obj">SceneXml.ObjectDef for instance</param>
        public Definition(Scene scene, TargetObjectDef obj)
        {
            this.m_Scene = scene;
            this.m_Object = obj;
        }
        #endregion // Constructor(s)

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (obj is Definition)
            {
                Definition def = (obj as Definition);
                return (this.Object.Equals(def.Object));
            }

            return (false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return (String.Format("Definition: [{0}]{1}", this.Object.Name, this.Object.Guid));
        }
        #endregion // Object Overrides

        #region Controller Methods
        /// <summary>
        /// Return IDE String representation for a regular "Gta Object".
        /// </summary>
        /// <returns>String representing normal "Gta Object"</returns>
        /// <seealso cref="ToIDETimeObject"/>
        /// <seealso cref="ToIDEAnimObject"/>
        /// <seealso cref="ToIDETimeAnimObject"/>
        /// 
        /// These objects are non-animated, non-time objects as defined by 
        /// their attributes.  Animated, time and animated time objects
        /// have their own section in the IDE file.
        /// 
        /// Format of "Gta Object" line is:
        ///   name, txd, loddist, flags, attribute, bbmin.x, bbmin.y, bbmin.z, bbmax.x, bbmax.y, bbmax.z, bsph.x, bsph.y, bsph.z, bsph.radius, modelgroup
        ///   
        public String ToIDEObject()
        {
            Debug.Assert(this.m_Object.Is2dfxLightEffect() || null != this.m_Object.LocalBoundingBox,
                "Object for definition has no Bounding Box!");
            BoundingBox3f bbox = this.Object.GetLocalBoundingBox();
            BoundingSpheref bsphere = null != bbox ? new BoundingSpheref(bbox) : new BoundingSpheref();
            String txdname = this.Object.GetAttribute(AttrNames.OBJ_REAL_TXD, AttrDefaults.OBJ_REAL_TXD);
            if (txdname == "CHANGEME")
                Log.Log__Error("Object {0} has TXD set to CHANGEME.", this);

            String ide_line = String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}",
                this.Object.GetObjectName(),
                txdname,
                this.Object.GetLODDistance(),
                Flags.GetFlags(this.m_Object),
                this.Object.GetAttribute(AttrNames.OBJ_ATTRIBUTE, AttrDefaults.OBJ_ATTRIBUTE),
                bbox.Min.X, bbox.Min.Y, bbox.Min.Z, bbox.Max.X, bbox.Max.Y, bbox.Max.Z, 
                bsphere.Centre.X, bsphere.Centre.Y, bsphere.Centre.Z, bsphere.Radius,
                this.Object.GetAttribute(AttrNames.OBJ_MODEL_GROUP, AttrDefaults.OBJ_MODEL_GROUP));

            return (ide_line);
        }

        /// <summary>
        /// Return IDE String representation for a time "Gta Object".
        /// </summary>
        /// <returns>String representing time "Gta Object"</returns>
        /// <seealso cref="ToIDEObject"/>
        /// <seealso cref="ToIDEAnimObject"/>
        /// <seealso cref="ToIDETimeAnimObject"/>
        ///  
        /// Format of time "Gta Object" line is:
        ///   *regular object fields*, timeflags
        ///   
        public String ToIDETimeObject()
        {
            String ide_line = ToIDEObject();
            ide_line += String.Format(", {0}", Flags.GetTimeFlags(this.Object));

            return (ide_line);
        }

        /// <summary>
        /// Return IDE String representation for an anim "Gta Object".
        /// </summary>
        /// <returns>String representing anim "Gta Object"</returns>
        /// <seealso cref="ToIDEObject"/>
        /// <seealso cref="ToIDETimeObject"/>
        /// <seealso cref="ToIDETimeAnimObject"/>
        /// 
        /// Format of anim "Gta Object" line is:
        ///   name, txd, iad, loddist, flags, attribute, bbmin.x, bbmin.y, bbmin.z, bbmax.x, bbmax.y, bbmax.z, bsph.x, bsph.y, bsph.z, bsph.radius, modelgroup
        ///   
        public String ToIDEAnimObject()
        {
            Debug.Assert(null != this.m_Object.LocalBoundingBox,
                "Object for definition has no Bounding Box!");
            BoundingBox3f bbox = this.Object.GetLocalBoundingBox();
            BoundingSpheref bsphere = new BoundingSpheref(bbox);

            String txdname = String.Empty;
            // No longer outputting empty texture dictionaries
            if (this.Object.GetAttribute(AttrNames.OBJ_NO_TXD, AttrDefaults.OBJ_NO_TXD))
            {
                txdname = AttrDefaults.OBJ_REAL_TXD;
            }
            else
            {
                txdname = this.Object.GetAttribute(AttrNames.OBJ_REAL_TXD, AttrDefaults.OBJ_REAL_TXD);
            }

            String ide_line = String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}",
                this.Object.GetObjectName(),
                txdname,
                this.Object.GetAttribute(AttrNames.OBJ_ANIM, AttrDefaults.OBJ_ANIM),
                this.Object.GetLODDistance(), 
                Flags.GetFlags(this.Object), 
                this.m_Object.GetAttribute(AttrNames.OBJ_ATTRIBUTE, AttrDefaults.OBJ_ATTRIBUTE),
                bbox.Min.X, bbox.Min.Y, bbox.Min.Z, bbox.Max.X, bbox.Max.Y, bbox.Max.Z,
                bsphere.Centre.X, bsphere.Centre.Y, bsphere.Centre.Z, bsphere.Radius,
                this.Object.GetAttribute(AttrNames.OBJ_MODEL_GROUP, AttrDefaults.OBJ_MODEL_GROUP));

            return (ide_line);
        }

        /// <summary>
        /// Return IDE String representation for a time anim "Gta Object".
        /// </summary>
        /// <returns>String representing time anim "Gta Object"</returns>
        /// <seealso cref="ToIDEObject"/>
        /// <seealso cref="ToIDETimeObject"/>
        /// <seealso cref="ToIDETimeAnimObject"/>
        /// 
        /// Format of time anim "Gta Object" line is:
        ///   *regular anim object*, timeflags
        ///   
        public String ToIDETimeAnimObject()
        {
            String ide_line = ToIDEAnimObject();
            ide_line += String.Format(", {0}", Flags.GetTimeFlags(this.Object));

            return (ide_line);
        }

        /// <summary>
        /// Return IDE String representation for a "Gta Milo" object, and its
        /// children.
        /// </summary>
        /// <returns></returns>
        /// The scene is required to resolve object references.
        /// 
        /// Format of a "Gta Milo" line is:
        ///   name, flags, #rooms, #portals, #objects, loddist, loddist2, detail, 
        ///     *entries*
        ///   mloroomstart
        ///     limbo, ...
        ///     ...
        ///   mloportalstart
        ///     ...
        ///   mloend
        /// 
        public String ToIDEMilo(Scene scene)
        {
            String[] room_list;
            List<Definition> entries;
            List<TargetObjectDef> entryObjs = new List<TargetObjectDef>();
            MiloRoomContainer rooms;
            MiloPortalContainer portals;
            TargetObjectDef limbo;
            MiloPreParseRooms(scene, out rooms, out room_list, out limbo, out entries);
            MiloPreParsePortals(scene, limbo, ref rooms, out portals);
            MiloPortParseLimbo(scene, rooms[limbo], portals);

            // Some sanity checking.
            // GTA5 Bug #1994.
            if (rooms.Count > 31)
            {
                Log.Log__Error("Interior {0}: maximum number of rooms exceeded {1}.  Reduce the number of rooms in this interior.",
                    System.IO.Path.GetFileNameWithoutExtension(scene.Filename), rooms.Count);
            }
            if (portals.Count > 255)
            {
                Log.Log__Error("Interior {0}: maximum number of portals exceeded {1}.  Reduce the number of portals in this interior.",
                   System.IO.Path.GetFileNameWithoutExtension(scene.Filename), portals.Count);
            }

            UInt32 flags = Flags.GetMiloFlags(this.Object);
            float detail = this.Object.GetAttribute(AttrNames.MILO_DETAIL, AttrDefaults.MILO_DETAIL);
            float loddist = -1.0f;
            float loddist2 = -1.0f;
            if (this.Object.HasLODParent())
            {
                loddist = this.Object.LOD.Parent.GetLODDistance();
                entries.Add(new Definition(scene, this.Object.LOD.Parent));
                if (this.Object.LOD.Parent.HasLODParent())
                {
                    loddist2 = this.Object.LOD.Parent.LOD.Parent.GetLODDistance();
                    entries.Add(new Definition(scene, this.Object.LOD.Parent.LOD.Parent));
                }
            }
            int num_entries = entries.Count;
            foreach (Definition def in entries)
                entryObjs.Add(def.Object);

            String ide_line = String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}{8}",
                this.Object.GetObjectName(), flags, rooms.Count, portals.Count,
                num_entries, loddist, loddist2, detail, Environment.NewLine);
            foreach (Definition entryDef in entries)
            {
                ide_line += String.Format("\t{0}{1}", 
                    entryDef.ToIDEMiloEntry(this.Object), Environment.NewLine);
            }
            ide_line += String.Format("mloroomstart{0}", Environment.NewLine);
            foreach (KeyValuePair<TargetObjectDef, MiloRoom> room in rooms)
            {
                ide_line += String.Format("\t{0}{1}\troomend{1}", room.Value.ToIDERoom(entryObjs), 
                    Environment.NewLine);
            }
            ide_line += "mloportalstart";
            ide_line += Environment.NewLine;
            foreach (KeyValuePair<TargetObjectDef, MiloPortal> portal in portals)
            {
                ide_line += String.Format("\t{0}{1}", portal.Value.ToIDEPortal(scene, room_list, entryObjs),
                    Environment.NewLine);
            }
            ide_line += "mloend";

            return (ide_line);
        }

        /// <summary>
        /// Return IDE String representation for a 2dfx object.
        /// </summary>
        /// <returns></returns>
        /// The format varies depending on the 2dfx type, although most have a 
        /// common prefix which is specified in ToIDE2dfxCommon().
        public String ToIDE2dfx()
        {
            if (this.Object.Is2dfxAudioEmitter())
                return (this.ToIDE2dfxAudioEmitter());
            else if (this.Object.Is2dfxBuoyancy())
                return (this.ToIDE2dfxBuoyancy());
            else if (this.Object.Is2dfxDecal())
            {
                Log.Log__WarningCtx(this.Object.Name, "RsDecals are not supported in IDE files.");
                return (String.Empty);
            }
            else if (this.Object.Is2dfxExplosionEffect())
                return (this.ToIDE2dfxExplosionEffect());
            else if (this.Object.Is2dfxLadderFx())
                return (this.ToIDE2dfxLadder());
            else if (this.Object.Is2dfxRSLadder())
            {
                Log.Log__WarningCtx(this.Object.Name, "RsLadders are not supported in IDE files.");
                return (String.Empty);
            }
            else if (this.Object.Is2dfxLightEffect())
                return (this.ToIDE2dfxLightEffect());
            else if (this.Object.Is2dfxLightShaft())
                return (this.ToIDE2dfxLightShaft());
            else if (this.Object.Is2dfxProcObject())
                return (this.ToIDE2dfxProcObject());
            else if (this.Object.Is2dfxScript())
                return (this.ToIDE2dfxScript());
            else if (this.Object.Is2dfxParticleEffect())
                return (this.ToIDE2dfxParticleEffect());
            else if (this.Object.Is2dfxScrollbar())
                return (this.ToIDE2dfxScrollbars());
            else if (this.Object.Is2dfxSpawnPoint())
                return (this.ToIDE2dfxSpawnPoint());
            else if (this.Object.Is2dfxSwayableEffect())
                return (this.ToIDE2dfxSwayableEffect());
            else if (this.Object.Is2dfxWalkDontWalk())
                return (this.ToIDE2dfxWalkDontWalk());
            else if (this.Object.Is2dfxRsWindDisturbance())
            {
                Log.Log__WarningCtx(this.Object.Name, "RsWindDisturbances are not supported in IDE files.");
                return (String.Empty);
            }
            else if (this.Object.Is2dfxDoor())
            {
                Log.Log__WarningCtx(this.Object.Name, "RS Doors are not supported in IDE files.");
                return (String.Empty);
            }
            else if (this.Object.Is2dfxExpression())
            {
                Log.Log__WarningCtx(this.Object.Name, "RS Expressions are not supported in IDE files.");
                return (String.Empty);
            }
// GunnarD: url:bugstar:94411 retiring ledge data
//             else if (this.Object.Is2dfxLedge())
//                 return (this.ToIDE2dfxLedge());

            Debug.Assert(false, "Unhandled 2dfx type.");
            Log.Log__ErrorCtx(this.Object.Name, "Object is an unhandled 2dfx type: {0}.", this.Object.Class);
            return (String.Empty);
        }

        /// <summary>
        /// Return IDE String representation for audio material tagged "Gta Collision".
        /// </summary>
        /// <returns>String representing for an audio material tagged "Gta Collision"</returns>
        ///
        /// Format of an audio tagged "Gta Collision" line is:
        ///   parent_name, name_hash, material_hash
        ///   
        public String ToIDEAudioMaterial()
        {
            Debug.Assert(this.Object.HasParent(), "Audio material tagged Collision has no parent!");
            UInt32 matHash = (UInt32)RSG.ManagedRage.StringUtil.atHash(this.Object.GetAttribute(AttrNames.COLL_AUDIO_MATERIAL, AttrDefaults.COLL_AUDIO_MATERIAL));
            UInt16 objHash = RSG.ManagedRage.StringUtil.atHash16U(this.Object.LOD.Parent.Name);

            String ide_line = String.Format("{0}, {1}, {2}",
                this.Object.LOD.Parent.Name, matHash, objHash);

            return (ide_line);
        }
        #endregion // Controller Methods

        #region Private Methods
        #region Milo Helper Methods
        /// <summary>
        /// Pre-pass on the MloRoom objects.
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="rooms"></param>
        /// <param name="room_list"></param>
        /// <param name="limbo"></param>
        /// <param name="entries"></param>
        private void MiloPreParseRooms(Scene scene, out MiloRoomContainer rooms, 
            out String[] room_list, out TargetObjectDef limbo, out List<Definition> entries)
        {
            List<String> room_list_temp = new List<String>();
            rooms = new MiloRoomContainer();
            entries = new List<Definition>();
            
            // Create limbo room (we always have a limbo room as a container 
            // for objects not parented to a room helper or attached to a portal helper).
            limbo = new TargetObjectDef(LIMBO, "", AttrClass.MLOROOM, scene);
            limbo.NodeTransform = new Matrix34f(this.Object.NodeTransform);
            rooms.Add(limbo, new MiloRoom(limbo));

            foreach (TargetObjectDef room in this.Object.Children)
            {
                if (room.DontExport())
                    continue;

                if (room.IsMloRoom())
                {
                    // Create room ObjectContainer for its children entries.
                    room_list_temp.Add(room.Name);
                    rooms.Add(room, new MiloRoom(room));

                    // Populate entries from child objects.
                    foreach (TargetObjectDef child in room.Children)
                    {
                        if (!(child.IsXRef() || child.IsObject()))
                            continue; // Skip
                        if (child.DontExport() || child.DontExportIDE() || child.DontExportIPL())
                            continue; // Skip
                        rooms[room].Children.Add(child);
                        entries.Add(new Definition(scene, child));
                    }
                }
                else if (room.IsXRef() || room.IsObject())
                {
                    if (room.DontExportIDE() || room.DontExportIPL())
                        continue;
                    if (room.HasLODChildren())
                        continue;

                    // Non-room object as child of Gta Milo, so it goes in limbo.
                    rooms[limbo].Children.Add(room);
                    entries.Add(new Definition(scene, room));
                }
            }
            room_list = room_list_temp.ToArray();
        }

        /// <summary>
        /// Pre-pass on the MloPortal objects.
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="limbo"></param>
        /// <param name="rooms"></param>
        /// <param name="portals"></param>
        private void MiloPreParsePortals(Scene scene, TargetObjectDef limbo, ref MiloRoomContainer rooms, out MiloPortalContainer portals)
        {
            portals = new MiloPortalContainer();

            foreach (TargetObjectDef portal in this.Object.Children)
            {
                if (!portal.IsMloPortal())
                    continue; // Skip non-portals

                portals.Add(portal, new MiloPortal(portal));

                Guid roomAguid = portal.GetParameter(ParamNames.MLOPORTAL_FIRST_ROOM,
                    ParamDefaults.MLOPORTAL_FIRST_ROOM);
                Guid roomBguid = portal.GetParameter(ParamNames.MLOPORTAL_SECOND_ROOM,
                    ParamDefaults.MLOPORTAL_SECOND_ROOM);
                if (roomAguid != Guid.Empty)
                {
                    TargetObjectDef roomA = scene.FindObject(roomAguid);
                    Debug.Assert(null != roomA, String.Format("Could not find Room A reference for portal ({0}).", roomAguid));
                    portals[portal].RoomA = roomA;
                    rooms[roomA].Portals.Add(portal);
                }
                else
                {
                    rooms[limbo].Portals.Add(portal);
                }

                if (roomBguid != Guid.Empty)
                {
                    TargetObjectDef roomB = scene.FindObject(roomBguid);
                    Debug.Assert(null != roomB, String.Format("Could not find Room B reference for portal ({0}).", roomBguid));
                    portals[portal].RoomB = roomB;
                    rooms[roomB].Portals.Add(portal);
                }
                else
                {
                    rooms[limbo].Portals.Add(portal);
                }

                // Portal children
                foreach (TargetObjectDef child in portal.Children)
                    portals[portal].Children.Add(child);
            }
        }

        /// <summary>
        /// Post-pass on the Limbo room.
        /// </summary>
        /// This post-pass currently only removes objects previously added to 
        /// Limbo that are attached to Portals.
        /// <param name="scene"></param>
        /// <param name="limbo"></param>
        /// <param name="portals"></param>
        private void MiloPortParseLimbo(Scene scene, MiloRoom limbo, MiloPortalContainer portals)
        {
            foreach (KeyValuePair<TargetObjectDef, MiloPortal> portal in portals)
            {
                // Find attached objects.
                Object[] attachedGuids = portal.Value.Portal.GetParameter(ParamNames.MLOPORTAL_ATTACHED_OBJECTS, 
                    ParamDefaults.MLOPORTAL_ATTACHED_OBJECTS);
                if ( 0 == attachedGuids.Length)
                    continue; // Nothing to process here.

                ObjectDef[] attached = new ObjectDef[attachedGuids.Length];
                foreach (Object o in attachedGuids)
                {
                    if (!(o is Guid))
                        continue;

                    TargetObjectDef limboObj = scene.FindObject((Guid)o);
                    limbo.Children.Remove(limboObj);
                }
            }
        }

        /// <summary>
        /// Return String representation for a Milo Entry.
        /// </summary>
        /// <returns></returns>
        /// Format for a Milo Entry line is:
        ///   name, x, y, z, qx, qy, qz, qw, entryflags, instflags
        ///   
        /// Entry transform is in local-space; relative to the parent milo helper.
        /// 
        private String ToIDEMiloEntry(TargetObjectDef milo)
        {
            Matrix34f mtxParent = milo.NodeTransform.Inverse();
            Vector3f pos = mtxParent * this.Object.NodeTransform.Translation;
            Quaternionf rot = new Quaternionf(mtxParent * this.Object.NodeTransform);
            // For some reason MILO entry rotations are inverted.
            rot.Invert();
            
            UInt32 entryflags = Flags.GetMiloEntryFlags(this.Object);
            UInt32 instflags = Flags.GetInstanceFlags(this.Object);
            ObjectDef.InstanceType instType = this.Object.GetInstanceType();
            switch (instType)
            {
                case ObjectDef.InstanceType.LOD:
                    entryflags |= (uint)Flags.MiloEntryFlags.MILOENTRY_IS_LOD;
                    break;
                case ObjectDef.InstanceType.SLOD:
                    entryflags |= (uint)Flags.MiloEntryFlags.MILOENTRY_IS_SLOD;
                    break;
            }

            String ide_line = String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}",
                this.Object.GetObjectName(), pos.X, pos.Y, pos.Z, rot.X, rot.Y, rot.Z, rot.W,
                entryflags, instflags);
            
            return (ide_line);
        }
        #endregion // Milo Helper Methods

        #region 2dfx Helper Methods
        /// <summary>
        /// Return common IDE String prefix representation for most 2dfx objects.
        /// </summary>
        /// <returns></returns>
        /// Format of the common prefix is:
        ///   name, x, y, z, type_id, qx, qy, qz, -qw, **2dfx-specific**
        ///
        private String ToIDE2dfxCommon(int type)
        {
            Matrix34f mtxParent = new Matrix34f();
            if (this.Object.HasParent())
                mtxParent = this.Object.Parent.NodeTransform.Inverse();
            Matrix34f mtxLocal = this.Object.NodeTransform * mtxParent;
            Vector3f pos = mtxLocal.Translation;
            Quaternionf rot = new Quaternionf(mtxLocal);

            TargetObjectDef attachedObj = this.Object;
            TargetObjectDef parent = this.Object.Parent;
            while (null != parent)
            {
                if (null != parent.SkinObj)
                {
                    attachedObj = parent.SkinObj;
                    break;
                }
                parent = parent.Parent;
            }

            String ide_line = String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}",
                attachedObj.GetObjectName(), 
                pos.X, pos.Y, pos.Z, 
                type, 
                rot.X, rot.Y, rot.Z, -rot.W);
            return (ide_line);
        }

        /// <summary>
        /// Return String representation of a Audio Emitter object.
        /// </summary>
        /// <returns></returns>
        /// Format of a audio emitter object is:
        ///   *common*, hash
        ///   
        private String ToIDE2dfxAudioEmitter()
        {
            String effect = this.Object.GetAttribute(AttrNames.TWODFX_AUDIO_EMITTER_EFFECT, AttrDefaults.TWODFX_AUDIO_EMITTER_EFFECT);
            uint effecthash = (uint)RSG.ManagedRage.StringHashUtil.atStringHash(effect, 0);

            String ide_line = String.Format("{0}, {1}",
                ToIDE2dfxCommon(IDE2dfxTypes.TWODFX_AUDIO_EMITTER), effecthash);
            return (ide_line);
        }

        /// <summary>
        /// Return String representation of a Buoyancy object.
        /// </summary>
        /// <returns></returns>
        /// Format of a buoyancy object is:
        ///   *common*
        /// 
        /// No additional information is required.
        /// 
        private String ToIDE2dfxBuoyancy()
        {
            String ide_line = String.Format("{0}", 
                ToIDE2dfxCommon(IDE2dfxTypes.TWODFX_BUOYANCY));
            return (ide_line);
        }

        /// <summary>
        /// Return String representation of an "Gta Explosion" 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Format of a "Gta Explosion" effect is:
        ///   *common*, boneid, tag, trigger, flags 
        ///
        private String ToIDE2dfxExplosionEffect()
        {
            int trigger = this.Object.GetAttribute(AttrNames.TWODFX_EXPLOSION_TRIGGER, AttrDefaults.TWODFX_EXPLOSION_TRIGGER);
            bool attach_all = this.Object.GetAttribute(AttrNames.TWODFX_EXPLOSION_ATTACH_TO_ALL, AttrDefaults.TWODFX_EXPLOSION_ATTACH_TO_ALL);
            // AJM: Tag has been removed and replaced with a string name, setting this to 0 although this IDE file stuff shouldn't
            // be getting called anymore anyway
            int tag = 0;
            int boneid;
            if (attach_all)
                boneid = -1;
            else
                boneid = this.Object.GetAttribute(AttrNames.TWODFX_EXPLOSION_ATTACH, AttrDefaults.TWODFX_EXPLOSION_ATTACH);
            UInt32 flags = Flags.GetExplosionFlags(this.Object);

            String ide_line = String.Format("{0}, {1}, {2}, {3}, {4}",
                ToIDE2dfxCommon(IDE2dfxTypes.TWODFX_EXPLOSION_EFFECT),
                boneid, tag, trigger, flags);

            return (ide_line);
        }

        /// <summary>
        /// Return String representation of a "Gta Light" or "Gta LightPhoto" 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Format of a "Gta Light" or "Gta LightPhoto" is: (all one line)
        ///   name, x, y, z, 0, qx, qy, qz, qw, type, r, g, b, a, corona_name, 
        ///   shadowmap, loddist, size, corona_size, 0.0, 0, flashiness, 
        ///   0.0, 0.0, flags, cone_inner, cone_outer, intensity,
        ///   volume_intensity, volume_size, atten_start, atten_end,
        ///   dirx, diry, dirz, upx, upy, upz, attach, fadedist, 
        ///   volume_fadedist, corona_hdr_multi
        ///   
        private String ToIDE2dfxLightEffect()
        {
            bool dontExport = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_DONT_EXPORT, AttrDefaults.TWODFX_LIGHT_DONT_EXPORT);
            if (dontExport)
                return (String.Empty);

            // Regular attributes
            int flashiness = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_FLASHINESS, AttrDefaults.TWODFX_LIGHT_FLASHINESS);
            float volinten = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_VOLUME_INTENSITY, AttrDefaults.TWODFX_LIGHT_VOLUME_INTENSITY);
            float volsize = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_VOLUME_SIZE, AttrDefaults.TWODFX_LIGHT_VOLUME_SIZE);
            int attach = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_ATTACH, AttrDefaults.TWODFX_LIGHT_ATTACH);
            float corona_size = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_CORONA_SIZE, AttrDefaults.TWODFX_LIGHT_CORONA_SIZE);
            float corona_intensity = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_CORONA_INTENSITY, AttrDefaults.TWODFX_LIGHT_CORONA_INTENSITY);
            float corona_zBias = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_CORONA_ZBIAS, AttrDefaults.TWODFX_LIGHT_CORONA_ZBIAS);
            float loddist = this.Object.GetLODDistance();

            // Light Properties
            Vector3f colour = ParamHelpers.GetLightParam(this.Object, ParamNames.TWODFX_LIGHT_COLOUR, ParamDefaults.TWODFX_LIGHT_COLOUR);
 
            String shadowmap = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_SHADOW_NAME, AttrDefaults.TWODFX_LIGHTPHOTO_SHADOW_NAME);
            string shadowmapPropValue = ParamHelpers.GetLightParam(this.Object, ParamNames.TWODFX_LIGHT_SHADOW_MAP, ParamDefaults.TWODFX_LIGHT_SHADOW_MAP);
            if (null != shadowmapPropValue && !System.IO.File.Exists(shadowmapPropValue))
            {
                Log.Log__WarningCtx(this.Object.Name, "The light's ({0}) projection texture {1} does not exist on your machine. Won't be considered.", this.Object.Name, shadowmapPropValue);
            }
            if (shadowmap == AttrDefaults.TWODFX_LIGHTPHOTO_SHADOW_NAME && null!=shadowmapPropValue)
            {
                //if shadowmap texture name is set to default for a spot light, allow use of the 'Property' value
                shadowmap = shadowmapPropValue;
            }
            else if(shadowmapPropValue!=null)
            {
                Log.Log__WarningCtx(this.Object.Name, "Shadow map ATTRIBUTE on light {0} is set to non-default {1}. Overriding the artist's defined Projection map {2}!\n", this.Object.Name, shadowmap, shadowmapPropValue);
            }

            float capsuleWidth = ParamHelpers.GetLightParam(this.Object, ParamNames.TWODFX_LIGHT_CAPSULE_WIDTH, ParamDefaults.TWODFX_LIGHT_CAPSULE_WIDTH);

            float cone_inner = ParamHelpers.GetLightParam(this.Object, ParamNames.TWODFX_LIGHT_HOTSPOT, ParamDefaults.TWODFX_LIGHT_HOTSPOT);
            float cone_outer = ParamHelpers.GetLightParam(this.Object, ParamNames.TWODFX_LIGHT_FALLSIZE, ParamDefaults.TWODFX_LIGHT_FALLSIZE);
            float intensity = ParamHelpers.GetLightParam(this.Object, ParamNames.TWODFX_LIGHT_INTENSITY, ParamDefaults.TWODFX_LIGHT_INTENSITY);
            float atten_end = ParamHelpers.GetLightParam(this.Object, ParamNames.TWODFX_LIGHT_ATTENUATION_END, ParamDefaults.TWODFX_LIGHT_ATTENUATION_END);
            cone_inner /= 2.0f;
            cone_outer /= 2.0f;
            uint lightFlags = Flags.GetLightFlags(this.Object);
            int actualLightType = ParamHelpers.GetLightParam(this.Object, ParamNames.TWODFX_LIGHT_TYPE, ParamDefaults.TWODFX_LIGHT_TYPE);
            Flags.TwodfxLightTypes lighttype = Flags.TwodfxLightTypes.TWODFX_LIGHT_STANDARD;
            if (actualLightType > (int)ParamNames.TWODFX_SCENEXML_LIGHTTYPES.TWODFX_SCENEXML_LIGHTTYPE_OMNI)
            {
                if (actualLightType == (int)ParamNames.TWODFX_SCENEXML_LIGHTTYPES.TWODFX_SCENEXML_LIGHTTYPE_CAPSULE)
                    lighttype = Flags.TwodfxLightTypes.TWODFX_LIGHT_CAPSULE;
                else
                    lighttype = Flags.TwodfxLightTypes.TWODFX_LIGHT_SPOT;
            }

            float expFalloff = ParamHelpers.GetLightParam(this.Object, ParamNames.TWODFX_LIGHT_EXP_FALLOFF, ParamDefaults.TWODFX_LIGHT_EXP_FALLOFF);

            Matrix34f localToWorldTransform = this.Object.NodeTransform.Transpose();// HACK - Transpose to work around data issue

            Matrix34f parentToWorldTransform = new Matrix34f();
            Matrix34f worldToParentTransform = new Matrix34f();
            if (this.Object.HasParent())
            {
                parentToWorldTransform = this.Object.Parent.NodeTransform.Transpose();// HACK - Transpose to work around data issue
                worldToParentTransform = parentToWorldTransform.Inverse();
            }

            Matrix34f localToParentTransform = localToWorldTransform * worldToParentTransform;
            Quaternionf parentSpaceOrientation = new Quaternionf(localToParentTransform);
            Vector3f direction = -localToParentTransform.C;
            direction.Normalise();
            Vector3f tangent = -localToParentTransform.A;
            tangent.Normalise();

            Vector4f cullPlane = ParamHelpers.GetLightParam(this.Object, ParamNames.TWODFX_LIGHT_CULLPLANE, ParamDefaults.TWODFX_LIGHT_CULLPLANE);
            Vector3f cullPlaneRotPart = new Vector3f(cullPlane.X, cullPlane.Y, cullPlane.Z);
            // Calculating the cullplane orientation realtiv to the PARENT object
            Matrix34f parentToLocalTransform = this.Object.NodeTransform * this.Object.Parent.NodeTransform;
            parentToLocalTransform.Translation = new Vector3f();
            parentToLocalTransform = parentToLocalTransform.Inverse();
            parentToLocalTransform.Transform(cullPlaneRotPart);
            cullPlaneRotPart.Normalise();
            cullPlane.X = cullPlaneRotPart.X;
            cullPlane.Y = cullPlaneRotPart.Y;
            cullPlane.Z = cullPlaneRotPart.Z;
            if (!ParamHelpers.GetLightParam(this.Object, ParamNames.TWODFX_LIGHT_USE_CULLPLANE, AttrDefaults.TWODFX_LIGHT_USE_CULLPLANE))
                cullPlane.W = atten_end;
            Vector3f outerCol = ParamHelpers.GetLightParam(this.Object, ParamNames.TWODFX_LIGHT_VOL_OUTER_COLOUR, ParamDefaults.TWODFX_LIGHT_VOL_OUTER_COLOUR);
            float outerColIntensity = ParamHelpers.GetLightParam(this.Object, ParamNames.TWODFX_LIGHT_VOL_OUTER_COLOUR_INTEN, ParamDefaults.TWODFX_LIGHT_VOL_OUTER_COLOUR_INTEN);
            float outerColExponent = ParamHelpers.GetLightParam(this.Object, ParamNames.TWODFX_LIGHT_VOL_OUTER_COLOUR_EXP, ParamDefaults.TWODFX_LIGHT_VOL_OUTER_COLOUR_EXP);
            
            // write time flags. if not a time object write number bugger than possible daytime
            // defaulting to binary 111111100000000000000111
            // NOTE: TIME FLAGS ARE WRITTEN OUT WITH THE HOUR 24 BEING BIT 0!
            int theFlag = (int)Flags.GetTimeFlags(this.Object, 0xFE0007);
            if (theFlag == 0)
                Log.Log__WarningCtx(this.m_Object.Name, "Light object {0} has no time set. Won't be visible.", this.m_Object.Name);

            int fadedist = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_LIGHT_FADE_DISTANCE, AttrDefaults.TWODFX_LIGHT_LIGHT_FADE_DISTANCE);
            int volfadedist = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_VOLUME_FADE_DISTANCE, AttrDefaults.TWODFX_LIGHT_VOLUME_FADE_DISTANCE);
            int shadowfadedist = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_SHADOW_FADE_DISTANCE, AttrDefaults.TWODFX_LIGHT_SHADOW_FADE_DISTANCE);
            int specularfadedist = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_SPECULAR_FADE_DISTANCE, AttrDefaults.TWODFX_LIGHT_SPECULAR_FADE_DISTANCE);

            int shadowBlur = (int)(255.0f * this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_SHADOW_BLUR, AttrDefaults.TWODFX_LIGHT_SHADOW_BLUR));

            float shadowNearClip = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_SHADOW_NEAR_CLIP, AttrDefaults.TWODFX_LIGHT_SHADOW_NEAR_CLIP);

            byte lightIstanceHash = this.Object.AttributeGuid.ToByteArray()[0];

            String ide_line = String.Format(
                "{0}, "+
                "{1}, {2}, {3}, "+
                "{4}, "+
                "{5}, {6}, {7}, {8}, "+
                "{9}, "+
                "{10}, {11}, {12}, {13}, "+
                "\"{14}\", \"{15}\", {16}, "+
                "{17}, "+
                "{18}, {19}, {20}, {21}, "+
                "{22}, {23}, {24}, {25}, "+
                "{26}, {27}, {28}, {29}, "+
                "{30}, {31}, "+
                "{32}, {33}, {34}, "+
                "{35}, {36}, {37}, "+
                "{38}, "+
                "{39}, {40}, "+
                "{41}, {42}, {43}, "+
                "{44}, {45}, {46}, {47}, "+
                "{48}, {49}, {50}, {51}, "+
                "{52}, "+
                "{53}, {54}, {55}, {56}, "+
                "{57}",
                this.Object.GetObjectName(),
                localToParentTransform.Translation.X, localToParentTransform.Translation.Y, localToParentTransform.Translation.Z,
                0, //retired
                parentSpaceOrientation.X, parentSpaceOrientation.Y, parentSpaceOrientation.Z, parentSpaceOrientation.W,
                (uint)lighttype, 
                SMath.Round(colour.X * 255), SMath.Round(colour.Y * 255), SMath.Round(colour.Z * 255), 255,
                "_retired_", System.IO.Path.GetFileNameWithoutExtension(shadowmap), loddist, 
                0, //retired
                corona_size, capsuleWidth, lightIstanceHash, flashiness,
                shadowNearClip, outerColExponent, 0/*old light flags*/, Convert.ToInt32(cone_inner), 
                Convert.ToInt32(cone_outer), intensity, volinten, volsize,
                shadowBlur, atten_end, 
                direction.X, direction.Y, direction.Z,
                tangent.X, tangent.Y, tangent.Z,
                attach, 
                0, 0, //retired
                corona_intensity, theFlag, expFalloff,
                cullPlane.X,cullPlane.Y,cullPlane.Z,cullPlane.W,
                SMath.Round(outerCol.X * 255), SMath.Round(outerCol.Y * 255), SMath.Round(outerCol.Z * 255), outerColIntensity,
                corona_zBias,
                fadedist, shadowfadedist, specularfadedist, volfadedist,
                lightFlags);

            return (ide_line);  
        }

        /// <summary>
        /// Return String representation of an "RAGE Particle" 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Format of a "RAGE Particle" effect is:
        ///   *common*, tag, trigger, boneid, scale, prob, flags, r, g, b
        ///   
        private String ToIDE2dfxParticleEffect()
        {
            String tag = this.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_NAME,
                AttrDefaults.TWODFX_PARTICLE_NAME);
            if ("" == tag)
                tag = AttrDefaults.TWODFX_PARTICLE_NAME;
            int trigger = this.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_TRIGGER,
                AttrDefaults.TWODFX_PARTICLE_TRIGGER);
            int boneid = this.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_ATTACH,
                AttrDefaults.TWODFX_PARTICLE_ATTACH);
            float prob = this.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_PROBABILITY,
                AttrDefaults.TWODFX_PARTICLE_PROBABILITY);
            float scale = this.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_SCALE,
                AttrDefaults.TWODFX_PARTICLE_SCALE);
            UInt32 flags = Flags.GetParticleFlags(this.Object);
            int r = this.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_COLOUR_R,
                AttrDefaults.TWODFX_PARTICLE_COLOUR_R);
            int g = this.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_COLOUR_G,
                AttrDefaults.TWODFX_PARTICLE_COLOUR_G);
            int b = this.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_COLOUR_B,
                AttrDefaults.TWODFX_PARTICLE_COLOUR_B);
            float size_x = this.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_SIZE_X,
                AttrDefaults.TWODFX_PARTICLE_SIZE_X);
            float size_y = this.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_SIZE_Y,
                AttrDefaults.TWODFX_PARTICLE_SIZE_Y);
            float size_z = this.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_SIZE_Z,
                AttrDefaults.TWODFX_PARTICLE_SIZE_Z);

            String ide_line = String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}",
                ToIDE2dfxCommon(IDE2dfxTypes.TWODFX_PARTICLE_EFFECT),
                tag, trigger, boneid, scale, prob, flags, r, g, b, size_x, size_y, size_z);
            return (ide_line);
        }

        /// <summary>
        /// Return String representation for a "Gta Scrollbar" 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Format of a "Gta Scrollbar" 2dfx object is:
        ///   *common*, height, type, numpoints-1, x1, y1, z1, ..., xn, yn, zn
        ///
        private String ToIDE2dfxScrollbars()
        {
            float height = this.Object.GetParameter(ParamNames.TWODFX_SCROLLBARS_HEIGHT, 
                ParamDefaults.TWODFX_SCROLLBARS_HEIGHT);
            int type = this.Object.GetParameter(ParamNames.TWODFX_SCROLLBARS_TYPE, 
                ParamDefaults.TWODFX_SCROLLBARS_TYPE);
            int numpoints = this.Object.GetParameter(ParamNames.TWODFX_SCROLLBARS_NUMPOINTS, 
                ParamDefaults.TWODFX_SCROLLBARS_NUMPOINTS);

            String ide_line = String.Format("{0}, {1}, {2}, {3}",
                ToIDE2dfxCommon(IDE2dfxTypes.TWODFX_SCROLLBARS),
                height, type, numpoints-1);
            if ((numpoints-1) > 0)
            {
                Matrix34f mtxObject = this.Object.NodeTransform.Inverse();
                ide_line += ", ";
                int n = 0;
                foreach (TargetObjectDef o in this.Object.SubObjects)
                {
                    if (0 != n)
                    {
                        // Calculate point positions in local-space.
                        Matrix34f mtxLocal = mtxObject * o.NodeTransform;
                        Vector3f pos = mtxLocal.Translation;
                        if (n == (this.Object.SubObjects.Length-1))
                            ide_line += String.Format("{0}, {1}, {2}", pos.X, pos.Y, pos.Z);
                        else
                            ide_line += String.Format("{0}, {1}, {2}, ", pos.X, pos.Y, pos.Z);
                    }
                    ++n;
                }
            }
            return (ide_line);
        }
        
        /// <summary>
        /// Return String representation of a SpawnPoint 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Fomrat of a SpawnPoint 2dfx is:
        ///   *common*, spawntype, pedtype, start, end
        /// 
        private String ToIDE2dfxSpawnPoint()
        {
            String spawnType = this.Object.GetParameter(ParamNames.TWODFX_SPAWNPOINT_SPAWNTYPE,
                ParamDefaults.TWODFX_SPAWNPOINT_SPAWNTYPE);
            uint hSpawnType = (uint)RSG.ManagedRage.StringHashUtil.atStringHash(spawnType, 0);
            String pedType = this.Object.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_PED_TYPE,
                AttrDefaults.TWODFX_SPAWNPOINT_PED_TYPE);
            uint hPedType = (uint)RSG.ManagedRage.StringHashUtil.atStringHash(pedType, 0);
            int start = this.Object.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_START_TIME,
                AttrDefaults.TWODFX_SPAWNPOINT_START_TIME);
            int end = this.Object.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_END_TIME,
                AttrDefaults.TWODFX_SPAWNPOINT_END_TIME);

            String ide_line = String.Format("{0}, {1}, {2}, {3}, {4}",
                ToIDE2dfxCommon(IDE2dfxTypes.TWODFX_SPAWN_POINT),
                hSpawnType, hPedType, start, end);
            return (ide_line);
        }

        /// <summary>
        /// Return String representation of a Swayable 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Format of a Swayable 2dfx is:
        ///   *common*, boneid, lowwindspeed, lowwindampl, highwindspeed, highwindampl
        ///   
        private String ToIDE2dfxSwayableEffect()
        {
            int boneid = this.Object.GetAttribute(AttrNames.TWODFX_SWAYABLE_BONEID,
                AttrDefaults.TWODFX_SWAYABLE_BONEID);
            float lwspeed = this.Object.GetAttribute(AttrNames.TWODFX_SWAYABLE_LOW_WIND_SPEED,
                AttrDefaults.TWODFX_SWAYABLE_LOW_WIND_SPEED);
            float lwampl = this.Object.GetAttribute(AttrNames.TWODFX_SWAYABLE_LOW_WIND_AMPLITUDE,
                AttrDefaults.TWODFX_SWAYABLE_LOW_WIND_AMPLITUDE);
            float hwspeed = this.Object.GetAttribute(AttrNames.TWODFX_SWAYABLE_HIGH_WIND_SPEED,
                AttrDefaults.TWODFX_SWAYABLE_HIGH_WIND_SPEED);
            float hwampl = this.Object.GetAttribute(AttrNames.TWODFX_SWAYABLE_HIGH_WIND_AMPLITUDE,
                AttrDefaults.TWODFX_SWAYABLE_HIGH_WIND_AMPLITUDE);

            String ide_line = String.Format("{0}, {1}, {2}, {3}, {4}, {5}",
                ToIDE2dfxCommon(IDE2dfxTypes.TWODFX_SWAYABLE_EFFECT),
                boneid, lwspeed, lwampl, hwspeed, lwampl);
            return (ide_line);
        }

        /// <summary>
        /// Return String representation of a Light Shaft 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Format of a LightShaft 2dfx is:
        ///   *common*, a.x, a.y, a.z, b.x, b.y, b.z, c.x, c.y, c.z, d.x, d.y, d.z, length, density, intensity
        ///
        private String ToIDE2dfxLightShaft()
        {
            return String.Empty; //#deprecated
 
            Matrix34f matParent = this.Object.Parent.NodeTransform.Inverse();
            Quaternionf nodeRot = new Quaternionf(this.Object.NodeTransform);
            Matrix34f nodeMtx = new Matrix34f(nodeRot);
            nodeMtx.Translation = this.Object.NodeTransform.Translation;
            nodeMtx *= matParent;

            float lengthdiv2 = this.Object.GetParameter(ParamNames.TWODFX_LIGHTSHAFT_LENGTH, ParamDefaults.TWODFX_LIGHTSHAFT_LENGTH) / 2.0f;
            float widthdiv2 = this.Object.GetParameter(ParamNames.TWODFX_LIGHTSHAFT_WIDTH, ParamDefaults.TWODFX_LIGHTSHAFT_WIDTH) / 2.0f;
            Vector3f a = nodeMtx * (new Vector3f(widthdiv2, -lengthdiv2, 0.0f));
            Vector3f b = nodeMtx * (new Vector3f(widthdiv2, lengthdiv2, 0.0f));
            Vector3f c = nodeMtx * (new Vector3f(-widthdiv2, lengthdiv2, 0.0f));
            Vector3f d = nodeMtx * (new Vector3f(-widthdiv2, -lengthdiv2, 0.0f));

            float length = this.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_LENGTH, AttrDefaults.TWODFX_LIGHTSHAFT_LENGTH);
            float density = 0;
            float intensity = this.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_INTENSITY, AttrDefaults.TWODFX_LIGHTSHAFT_INTENSITY);
            UInt32 flags = Flags.GetLightShaftFlags(this.Object);
            float directionX = this.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_DIRECTION_X, AttrDefaults.TWODFX_LIGHTSHAFT_DIRECTION_X);
            float directionY = this.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_DIRECTION_Y, AttrDefaults.TWODFX_LIGHTSHAFT_DIRECTION_Y);
            float directionZ = this.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_DIRECTION_Z, AttrDefaults.TWODFX_LIGHTSHAFT_DIRECTION_Z);
            Vector3f direction = new Vector3f(directionX, directionY, directionZ);
            direction.Normalise();
            int colourR = this.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_COLOUR_R, AttrDefaults.TWODFX_LIGHTSHAFT_COLOUR_R);
            int colourG = this.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_COLOUR_G, AttrDefaults.TWODFX_LIGHTSHAFT_COLOUR_G);
            int colourB = this.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_COLOUR_B, AttrDefaults.TWODFX_LIGHTSHAFT_COLOUR_B);
            
            String ide_line = String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19}, {20}, {21}, {22}", 
                ToIDE2dfxCommon(IDE2dfxTypes.TWODFX_LIGHT_SHAFT),
                a.X, a.Y, a.Z, b.X, b.Y, b.Z,
                c.X, c.Y, c.Z, d.X, d.Y, d.Z,
                length, density, intensity,
                flags,
                direction.X, direction.Y, direction.Z,
                colourR, colourG, colourB);
            return (ide_line);
        }

        /// <summary>
        /// Return String representation of a Proc Object 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Format of a Proc 2dfx is:
        ///   *common*, radinner, radouter, id, spacing, minscale, maxscale, minscaleZ, maxscaleZ, zoffsetMin, zoffsetMax, flags
        ///
        private String ToIDE2dfxProcObject()
        {
            String id = this.Object.GetAttribute(AttrNames.TWODFX_PROC_OBJECT, AttrDefaults.TWODFX_PROC_OBJECT);
            UInt32 objid = (uint)RSG.ManagedRage.StringHashUtil.atStringHash(id, 0);
            float radius_inner = 0.0f;
            float radius_outer = 0.0f;
            float spacing = this.Object.GetAttribute(AttrNames.TWODFX_PROC_SPACING, AttrDefaults.TWODFX_PROC_SPACING);
            float minscale = this.Object.GetAttribute(AttrNames.TWODFX_PROC_MIN_SCALE, AttrDefaults.TWODFX_PROC_MIN_SCALE);
            float maxscale = this.Object.GetAttribute(AttrNames.TWODFX_PROC_MAX_SCALE, AttrDefaults.TWODFX_PROC_MAX_SCALE);
            float minscalez = this.Object.GetAttribute(AttrNames.TWODFX_PROC_MIN_SCALE_Z, AttrDefaults.TWODFX_PROC_MIN_SCALE_Z);
            float maxscalez = this.Object.GetAttribute(AttrNames.TWODFX_PROC_MAX_SCALE_Z, AttrDefaults.TWODFX_PROC_MAX_SCALE_Z);
            float minzoffset = this.Object.GetAttribute(AttrNames.TWODFX_PROC_MIN_Z_OFFSET, AttrDefaults.TWODFX_PROC_MIN_Z_OFFSET);
            float maxzoffset = this.Object.GetAttribute(AttrNames.TWODFX_PROC_MAX_Z_OFFSET, AttrDefaults.TWODFX_PROC_MAX_Z_OFFSET);
            uint flags = Flags.GetProcObjectFlags(this.Object);

            String ide_line = String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}", 
                ToIDE2dfxCommon(IDE2dfxTypes.TWODFX_PROC_OBJECT),
                radius_inner, radius_outer,
                objid,
                spacing, minscale, maxscale, minscalez, maxscalez,
                minzoffset, maxzoffset, flags);
            return (ide_line);
        }

        /// <summary>
        /// Return String representation of a WalkDontWalk 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Format of a walk dont walk 2dfx is:
        ///   *common*
        ///   
        private String ToIDE2dfxWalkDontWalk()
        {
            String ide_line = String.Format("{0}", ToIDE2dfxCommon(IDE2dfxTypes.TWODFX_WALK_DONT_WALK));
            return (ide_line);
        }

        /// <summary>
        /// Return String representation of a Script 2dfx.
        /// </summary>
        /// <returns></returns>
        private String ToIDE2dfxScript()
        {
            List<TargetObjectDef> scriptChildren = new List<TargetObjectDef>();
            foreach (TargetObjectDef obj in this.Object.Children)
            {
                if (obj.Is2dfxScript())
                    scriptChildren.Add(obj);
            }

            String ide_line = String.Format("{0}, {1}, {2}", 
                ToIDE2dfxCommon(IDE2dfxTypes.TWODFX_SCRIPT), 
                this.Object.GetAttribute(AttrNames.SCRIPT_NAME, AttrDefaults.SCRIPT_NAME),
                scriptChildren.Count);
            foreach (TargetObjectDef obj in scriptChildren)
            {
                Matrix34f mtxParent = this.Object.NodeTransform.Inverse();
                Matrix34f mtxLocal = this.Object.NodeTransform * mtxParent;
                Vector3f eulers = mtxLocal.ToEulers();

                ide_line += String.Format(", {0}, {1}, {2}, {3}",
                    mtxLocal.Translation.X,
                    mtxLocal.Translation.Y,
                    mtxLocal.Translation.Z, 
                    eulers.Z);
            }

            return (ide_line);
        }

        /// <summary>
        /// Return String representation of a GtaLadderFX 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Format of a ladder 2dfx is:
        ///   name, 0, 0, 0, type_id, 0, 0, 0, -1, bottom.x, bottom.y, bottom.z, top.x, top.y, top.z, normal.x, normal.y, normal.z, canGetOffAtTop
        ///   
        /// Note: position information is always zero.
        /// Also: Normal rotations don't get the same world inverse for a local interpretation. 
        ///       Hence we keep use the parent transition for rotation and the inverse for the translation part.
        /// 
        private String ToIDE2dfxLadder()
        {
            Matrix34f mtxParent = new Matrix34f();
            Vector3f parentTranslation = new Vector3f();
            if (this.Object.HasParent())
            {
                mtxParent = new Matrix34f(this.Object.Parent.NodeTransform);
                parentTranslation = new Vector3f(mtxParent.Translation);
                mtxParent.Translation = new Vector3f();
            }

            bool canGetOff = this.Object.GetAttribute(AttrNames.TWODFX_LADDER_CAN_GET_OFF_AT_TOP, AttrDefaults.TWODFX_LADDER_CAN_GET_OFF_AT_TOP);
            Vector3f bottom = new Vector3f(this.Object.SubObjects[Subobjects.TWODFX_LADDER_BOTTOM].NodeTransform.Translation);
            Vector3f top = new Vector3f(this.Object.SubObjects[Subobjects.TWODFX_LADDER_TOP].NodeTransform.Translation);
            Vector3f normal = new Vector3f(this.Object.SubObjects[Subobjects.TWODFX_LADDER_NORMAL].NodeTransform.Translation);
            bottom -= parentTranslation;
            top -= parentTranslation;
            normal -= parentTranslation;
            mtxParent.Transform(bottom);
            mtxParent.Transform(top);
            mtxParent.Transform(normal);
            normal -= bottom;
            normal.Normalise();

            String ide_line = String.Format("{0}, 0, 0, 0, {1}, 0, 0, 0, -1, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}",
                this.Object.GetObjectName(),
                IDE2dfxTypes.TWODFX_LADDER,
                bottom.X, bottom.Y, bottom.Z,
                top.X, top.Y, top.Z,
                normal.X, normal.Y, normal.Z,
                canGetOff ? "1" : "0");
            return (ide_line);
        }

        /// <summary>
        /// Return String representation of a "RSLedge" 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Format of a ledge object is:
        ///   name, 0, 0, 0, type_id, 0, 0, 0, -1, left.x, left.y, left.z, right.x, right.y, right.z, normal.x, normal.y, normal.z
        ///   
        /// Note: position information is always zero.
        /// 
        private String ToIDE2dfxLedge()
        {
            if (null == this.Object.Parent)
            {
                String message = String.Format("RSLedge {0} has no parent object.", this);
                Debug.Assert(null != this.Object.Parent, message);
                Log.Log__Error(message);
                return (String.Empty);
            }

            // DHM 8 June 2009
            // HACK as SceneXml is outputting some custom (*HACK*) attributes for
            // ledge objects.  We just write those out.
            Vector3f left = this.Object.CustomAttributes["ledge_left"] as Vector3f;
            Vector3f right = this.Object.CustomAttributes["ledge_right"] as Vector3f;
            Vector3f normal = this.Object.CustomAttributes["ledge_normal"] as Vector3f;

            String ide_line = String.Format("{0}, 0, 0, 0, {1}, 0, 0, 0, -1, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}",
                this.Object.GetObjectName(),
                IDE2dfxTypes.TWODFX_LEDGE,
                left.X, left.Y, left.Z,
                right.X, right.Y, right.Z,
                normal.X, normal.Y, normal.Z);
            return (ide_line);
        }
        #endregion // 2dfx Helper Methods
        #endregion // Private Methods

        #region XML export methods
        #region base object methods
        /// <summary>
        /// Return XNode representation for a "StatedAnim/Composite Entity Object".
        /// </summary>
        /// <returns>XNode representing a "StatedAnim Object"</returns>
        /// 
        /// Format of "Gta Object" XNode is:
        ///<compositeEntityTypes>
        ///   <Item>
        ///     <Name>DES_Smash</Name>
        ///     <lodDist value="100.0" />
        ///     <flags value="29" />
        ///     <specialAttribute value="0" />
        ///     <bbMin x="0.0" y="-6.04662" z="0.0" />
        ///     <bbMax x="372.309" y="78.0255" z="989.174" />
        ///     <bsCentre x="186.154" y="35.9894" z="494.587" />
        ///     <bsRadius value="530.129" />
        ///     <AnimDict>DES_SetPiece</AnimDict>
        ///     <AnimName>rig_root</AnimName>
        ///     <StartModel>DES_smash_strt</StartModel>
        ///     <AnimatedModel>DES_smash_root</AnimatedModel>
        ///     <EndModel>DES_smash_end</EndModel>
        ///     <effectsData>
        ///     ...
        ///     </effectsData>
        ///   </Item>
        /// </compositeEntityTypes>
        ///   
        public XNode ToMETAStatedAnimObject(List<Definition> statedAnimObjects, Scene scene, TargetObjectDef rootObject)
        {
            Dictionary<String, int> animStateMap = new Dictionary<String, int>();
            animStateMap.Add("Start", 0);
            animStateMap.Add("Animation", 1);
            animStateMap.Add("End", 2);

            TargetObjectDef startObj = null;
            TargetObjectDef endObj = null;
            List<TargetObjectDef> currAnimObjects = new List<TargetObjectDef>();
            String myGroupName = this.Object.GetAttribute(AttrNames.OBJ_GROUP_NAME, AttrDefaults.OBJ_GROUP_NAME);
            foreach (Definition def in statedAnimObjects)
            {
                String animState = def.Object.GetAttribute(AttrNames.OBJ_ANIM_STATE, AttrDefaults.OBJ_ANIM_STATE);
                if (myGroupName == def.Object.GetAttribute(AttrNames.OBJ_GROUP_NAME, AttrDefaults.OBJ_GROUP_NAME))
                {
                    switch (animState)
                    {
                        case "Start": startObj = def.Object; break;
                        case "End": endObj = def.Object; break;
                        default: currAnimObjects.Add(def.Object); break;
                    }

                }
            }
            if (null == startObj || null == endObj || currAnimObjects.Count<1)
            {
                String message = String.Format("StatedAnim {0} has not enough objects in scene.", myGroupName);
                Debug.Assert(false, message);
                Log.Log__Error(message);
                return (null);
            }

            XElement rootNode  = new XElement("Item");

            BoundingBox3f bbox = new BoundingBox3f();

            XElement nameNode  = new XElement("Name");
            XText nametext = new XText(myGroupName);
            nameNode.Add(nametext);
            rootNode.Add(nameNode);
            XElement lodNode  = new XElement("lodDist");
            lodNode.Add(new XAttribute("value", this.Object.GetLODDistance().ToString()));
            rootNode.Add(lodNode);
            XElement flagNode = new XElement("flags");
            UInt32 flags = Flags.GetFlags(this.Object);
            flagNode.Add(new XAttribute("value", flags.ToString()));
            rootNode.Add(flagNode);
            XElement attrNode = new XElement("specialAttribute");
            attrNode.Add(new XAttribute("value", this.m_Object.GetAttribute(AttrNames.OBJ_ATTRIBUTE, AttrDefaults.OBJ_ATTRIBUTE).ToString()));
            rootNode.Add(attrNode);
            XElement bbminNode = new XElement("bbMin");
            rootNode.Add(bbminNode);
            XElement bbmaxNode = new XElement("bbMax");
            rootNode.Add(bbmaxNode);
            XElement centreNode = new XElement("bsCentre");
            rootNode.Add(centreNode);
            XElement radiusNode = new XElement("bsRadius");
            rootNode.Add(radiusNode);

            XElement startNode = new XElement("StartModel");
            string startId = startObj.GetObjectName();
            if (startObj.IsIMAPGroupDummy())
            {
                startId = startObj.GetParameter("imapName", PropDefaults.IMAP_GROUP_DUMMY_NAME);
                if (null == startId)
                    Log.Log__ErrorCtx(startObj.Name, "StatedAnim state object {0} is an imap group, but name property is not set!", startObj.Name);
            }
            else
                bbox.Expand(startObj.LocalBoundingBox);
            XText starttext = new XText(startId);
            startNode.Add(starttext);
            rootNode.Add(startNode);

            // Subscope for split animated obejcts per 128 bones
            XElement animsNode = new XElement("Animations");
            rootNode.Add(animsNode);
            foreach (TargetObjectDef def in currAnimObjects)
            {
                XElement animItemNode = new XElement("Item");
                animsNode.Add(animItemNode);

                XElement animatedNode = new XElement("AnimatedModel");
                XText animatedtext = new XText(def.GetObjectName());
                animatedNode.Add(animatedtext);
                animItemNode.Add(animatedNode);
                XElement animDictNode = new XElement("AnimDict");
                XText animDicttext = new XText(def.GetAttribute(AttrNames.OBJ_ANIM, AttrDefaults.OBJ_ANIM));
                animDictNode.Add(animDicttext);
                animItemNode.Add(animDictNode);
                XElement animNameNode = new XElement("AnimName");
                XText animNametext = new XText(def.GetObjectName());
                animNameNode.Add(animNametext);
                animItemNode.Add(animNameNode);

                bbox.Expand(def.LocalBoundingBox);

                // attaching effects
                List<Definition> l2dfx = new List<Definition>();
                if (def.SkeletonRootGuid != Guid.Empty)
                {
                    TargetObjectDef skelObj = scene.FindObject(def.SkeletonRootGuid);
                    if (null != skelObj)
                        ProcessScene2DFX(scene, l2dfx, skelObj);
                }
                else
                    ProcessScene2DFX(scene, l2dfx, def);
                XElement effectNode = new XElement("effectsData");
                animItemNode.Add(effectNode);
                foreach (Definition fxdef in l2dfx)
                {
                    XNode node = fxdef.ToMETA2dfx(def);
                    if (null != node)
                        effectNode.Add(node);
                }
            }

            XElement endNode = new XElement("EndModel");
            string endId = endObj.GetObjectName();
            if (endObj.IsIMAPGroupDummy())
            {
                endId = endObj.GetParameter("imapName", PropDefaults.IMAP_GROUP_DUMMY_NAME);
                if (null == endId)
                    Log.Log__ErrorCtx(endObj.Name, "StatedAnim state object {0} is an imap group, but name property is not set!", endObj.Name);
            }
            else
                bbox.Expand(startObj.LocalBoundingBox);
            XText endtext = new XText(endId);

            BoundingSpheref bsphere = new BoundingSpheref(bbox);
            bbminNode.Add(new XAttribute("x", bbox.Min.X.ToString()));
            bbminNode.Add(new XAttribute("y", bbox.Min.Y.ToString()));
            bbminNode.Add(new XAttribute("z", bbox.Min.Z.ToString()));
            bbmaxNode.Add(new XAttribute("x", bbox.Max.X.ToString()));
            bbmaxNode.Add(new XAttribute("y", bbox.Max.Y.ToString()));
            bbmaxNode.Add(new XAttribute("z", bbox.Max.Z.ToString()));
            centreNode.Add(new XAttribute("x", bsphere.Centre.X.ToString()));
            centreNode.Add(new XAttribute("y", bsphere.Centre.Y.ToString()));
            centreNode.Add(new XAttribute("z", bsphere.Centre.Z.ToString()));
            radiusNode.Add(new XAttribute("value", bsphere.Radius.ToString()));

            endNode.Add(endtext);
            rootNode.Add(endNode);

            return rootNode;
        }
        #endregion // base object methods

        #region nested effect methods
        /// <summary>
        /// Return IDE String representation for a 2dfx object.
        /// </summary>
        /// <returns></returns>
        /// The format varies depending on the 2dfx type, although most have a 
        /// common prefix which is specified in ToIDE2dfxCommon().
        public XNode ToMETA2dfx(TargetObjectDef rootObject)
        {
            if (this.Object.Is2dfxAudioEmitter())
                return (this.ToMETA2dfxAudioEmitter(rootObject));
            else if (this.Object.Is2dfxBuoyancy())
                return (this.ToMETA2dfxBuoyancy(rootObject));
            else if (this.Object.Is2dfxDecal())
                return (this.ToMETA2dfxDecal(rootObject));
            else if (this.Object.Is2dfxExplosionEffect())
                return (this.ToMETA2dfxExplosionEffect(rootObject));
            else if (this.Object.Is2dfxLadderFx() || this.Object.Is2dfxRSLadder())
                return (this.ToMETA2dfxLadder(rootObject));
            else if (this.Object.Is2dfxLightEffect())
                return (this.ToMETA2dfxLightEffect(rootObject));
            else if (this.Object.Is2dfxLightShaft())
                return (this.ToMETA2dfxLightShaft(rootObject));
            //else if (this.Object.Is2dfxProcObject())
            //    return (this.ToMETA2dfxProcObjectdoc, rootObject(rootObject));
            else if (this.Object.Is2dfxScript())
                return (this.ToMETA2dfxScript(rootObject));
            else if (this.Object.Is2dfxParticleEffect())
                return (this.ToMETA2dfxParticleEffect(rootObject));
            else if (this.Object.Is2dfxScrollbar())
                return (this.ToMETA2dfxScrollbars(rootObject));
            else if (this.Object.Is2dfxSpawnPoint())
                return (this.ToMETA2dfxSpawnPoint(rootObject));
            else if (this.Object.Is2dfxSwayableEffect())
                return (this.ToMETA2dfxSwayableEffect(rootObject));
            else if (this.Object.Is2dfxWalkDontWalk())
                return (this.ToMETA2dfxWalkDontWalk(rootObject));
            else if (this.Object.Is2dfxRsWindDisturbance())
                return (this.ToMETA2dfxRsWindDisturbance(rootObject));
            else if (this.Object.Is2dfxLedge())
                return (this.ToMETA2dfxLedge(rootObject));

            Debug.Assert(false, "Unhandled 2dfx type.");
            Log.Log__Error("Unhandled 2dfx type: {0}.", this.Object.Name);
            return null;
        }

        /// <summary>
        /// Return META String representation for audio material tagged "Gta Collision".
        /// </summary>
        /// <returns>String representing for an audio material tagged "Gta Collision"</returns>
        ///
        /// Format of an audio tagged "Gta Collision" line is:
        ///   parent_name, name_hash, material_hash
        ///   
        public XNode ToMETAAudioMaterial(ObjectDef rootObject)
        {
            //Debug.Assert(this.Object.HasParent(), "Audio material tagged Collision has no parent!");
            //UInt32 matHash = (UInt32)RSG.ManagedRage.atString.atHash(this.Object.GetAttribute(AttrNames.COLL_AUDIO_MATERIAL, AttrDefaults.COLL_AUDIO_MATERIAL));
            //UInt16 objHash = RSG.ManagedRage.atString.atHash16U(this.Object.LOD.Parent.Name);

            //String ide_line = String.Format("{0}, {1}, {2}",
            //    this.Object.LOD.Parent.Name, matHash, objHash);

            return null;
        }
        /// <summary>
        /// Return common META String prefix representation for most 2dfx objects.
        /// </summary>
        /// <returns></returns>
        /// Format of the common prefix is:
        ///   name, x, y, z, type_id, qx, qy, qz, -qw, **2dfx-specific**
        ///         <fxName>RsParticleHelper001</fxName>
        ///         <fxOffsetPos x="0.0" y="0.0" z="0.0"/>
        ///         <fxType value = "0" />
        ///         <fxOffsetRot x="0.0" y="0.0" z="0.0" w="1.0"/>
        ///
        private void ToMETA2dfxCommon(int type, XElement attachTo)
        {
            Matrix34f localToWorldTransform = this.Object.NodeTransform.Transpose();// HACK - Transpose to work around data issue

            Matrix34f parentToWorldTransform = new Matrix34f();
            Matrix34f worldToParentTransform = new Matrix34f();
            if (this.Object.HasParent())
            {
                parentToWorldTransform = this.Object.Parent.NodeTransform.Transpose();// HACK - Transpose to work around data issue
                worldToParentTransform = parentToWorldTransform.Inverse();
            }

            Matrix34f localToParentTransform = localToWorldTransform * worldToParentTransform;

            Quaternionf parentSpaceOrientation = new Quaternionf(localToParentTransform);

            XElement nameNode = new XElement("fxObjName");
            XText nametext = new XText(this.Object.Parent.Name);
            nameNode.Add(nametext);
            attachTo.Add(nameNode);
            XElement posNode = new XElement("fxOffsetPos");
            posNode.Add(new XAttribute("x", localToParentTransform.D.X.ToString()));
            posNode.Add(new XAttribute("y", localToParentTransform.D.Y.ToString()));
            posNode.Add(new XAttribute("z", localToParentTransform.D.Z.ToString()));
            attachTo.Add(posNode);
            XElement typeNode = new XElement("fxType");
            typeNode.Add("value", type.ToString());
            attachTo.Add(typeNode);
            XElement rotNode = new XElement("fxOffsetRot");
            rotNode.Add(new XAttribute("x", parentSpaceOrientation.X.ToString()));
            rotNode.Add(new XAttribute("y", parentSpaceOrientation.Y.ToString()));
            rotNode.Add(new XAttribute("z", parentSpaceOrientation.Z.ToString()));
            rotNode.Add(new XAttribute("w", parentSpaceOrientation.W.ToString()));
            attachTo.Add(rotNode);
        }

        /// <summary>
        /// Return String representation of a Audio Emitter object.
        /// </summary>
        /// <returns></returns>
        /// Format of a audio emitter object is:
        ///   *common*, hash
        ///   
        private XNode ToMETA2dfxAudioEmitter(TargetObjectDef rootObject)
        {
            //String effect = this.Object.GetAttribute(AttrNames.TWODFX_AUDIO_EMITTER_EFFECT, AttrDefaults.TWODFX_AUDIO_EMITTER_EFFECT);
            //uint effecthash = (uint)RSG.ManagedRage.StringHashUtil.atStringHash(effect, 0);

            //String ide_line = String.Format("{0}, {1}",
            //    ToMETA2dfxCommon(META2dfxTypes.TWODFX_AUDIO_EMITTER), effecthash);
            //return (ide_line);
            return null;
        }

        /// <summary>
        /// Return String representation of a Buoyancy object.
        /// </summary>
        /// <returns></returns>
        /// Format of a buoyancy object is:
        ///   *common*
        /// 
        /// No additional information is required.
        /// 
        private XNode ToMETA2dfxBuoyancy(TargetObjectDef rootObject)
        {
            //String ide_line = String.Format("{0}",
            //    ToMETA2dfxCommon(META2dfxTypes.TWODFX_BUOYANCY));
            //return (ide_line);
            return null;
        }

        /// <summary>
        /// Return String representation of a Decal object.
        /// </summary>
        /// <returns></returns>
        /// Format of a decal object is:
        ///   *common*
        /// 
        /// No additional information is required.
        /// 
        private XNode ToMETA2dfxDecal(TargetObjectDef rootObject)
        {
            return null;
        }
        

        /// <summary>
        /// Return String representation of an "Gta Explosion" 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Format of a "Gta Explosion" effect is:
        ///   *common*, boneid, tag, trigger, flags 
        ///
        private XNode ToMETA2dfxExplosionEffect(TargetObjectDef rootObject)
        {
            //int trigger = this.Object.GetAttribute(AttrNames.TWODFX_EXPLOSION_TRIGGER, AttrDefaults.TWODFX_EXPLOSION_TRIGGER);
            //bool attach_all = this.Object.GetAttribute(AttrNames.TWODFX_EXPLOSION_ATTACH_TO_ALL, AttrDefaults.TWODFX_EXPLOSION_ATTACH_TO_ALL);
            //int tag = this.Object.GetAttribute(AttrNames.TWODFX_EXPLOSION_TAG, AttrDefaults.TWODFX_EXPLOSION_TAG);
            //int boneid;
            //if (attach_all)
            //    boneid = -1;
            //else
            //    boneid = this.Object.GetAttribute(AttrNames.TWODFX_EXPLOSION_ATTACH, AttrDefaults.TWODFX_EXPLOSION_ATTACH);
            //UInt32 flags = Flags.GetExplosionFlags(this.Object);

            //String ide_line = String.Format("{0}, {1}, {2}, {3}, {4}",
            //    ToMETA2dfxCommon(META2dfxTypes.TWODFX_EXPLOSION_EFFECT),
            //    boneid, tag, trigger, flags);

            //return (ide_line);
            return null;
        }

        /// <summary>
        /// Return String representation of a "Gta Light" or "Gta LightPhoto" 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Format of a "Gta Light" or "Gta LightPhoto" is: (all one line)
        ///   name, x, y, z, 0, qx, qy, qz, qw, type, r, g, b, a, corona_name, 
        ///   shadowmap, loddist, size, corona_size, 0.0, 0, flashiness, 
        ///   0.0, 0.0, flags, cone_inner, cone_outer, intensity,
        ///   volume_intensity, volume_size, atten_start, atten_end,
        ///   dirx, diry, dirz, upx, upy, upz, attach, fadedist, 
        ///   volume_fadedist, corona_hdr_multi
        ///   
        private XNode ToMETA2dfxLightEffect(TargetObjectDef rootObject)
        {
            //bool enabled = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_ENABLED, AttrDefaults.TWODFX_LIGHT_ENABLED);
            //if (!enabled)
            //    return (String.Empty);

            //// Regular attributes
            //bool night = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_NIGHT, AttrDefaults.TWODFX_LIGHT_NIGHT);
            //float size = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_SIZE, AttrDefaults.TWODFX_LIGHT_SIZE);
            //float flashiness = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_FLASHINESS, AttrDefaults.TWODFX_LIGHT_FLASHINESS);
            //float volinten = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_VOLUME_INTENSITY, AttrDefaults.TWODFX_LIGHT_VOLUME_INTENSITY);
            //float volsize = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_VOLUME_SIZE, AttrDefaults.TWODFX_LIGHT_VOLUME_SIZE);
            //int attach = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_ATTACH, AttrDefaults.TWODFX_LIGHT_ATTACH);
            //float fadedist = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_LIGHT_FADE_DISTANCE, AttrDefaults.TWODFX_LIGHT_LIGHT_FADE_DISTANCE);
            //float volfadedist = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_VOLUME_FADE_DISTANCE, AttrDefaults.TWODFX_LIGHT_VOLUME_FADE_DISTANCE);
            //float corona_size = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_CORONA_SIZE, AttrDefaults.TWODFX_LIGHT_CORONA_SIZE);
            //String corona_name = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_CORONA_NAME, AttrDefaults.TWODFX_LIGHT_CORONA_NAME);
            //float corona_hdr = this.Object.GetAttribute(AttrNames.TWODFX_LIGHT_CORONA_HDR_MULTIPLIER, AttrDefaults.TWODFX_LIGHT_CORONA_HDR_MULTIPLIER);
            //float loddist = this.Object.GetLODDistance();

            //// Light Properties
            //Vector4f colour = this.Object.GetProperty(PropNames.TWODFX_LIGHT_COLOUR, PropDefaults.TWODFX_LIGHT_COLOUR);
            //bool is_spot = this.Object.GetProperty(PropNames.TWODFX_LIGHT_IS_SPOT, PropDefaults.TWODFX_LIGHT_IS_SPOT);
            //String shadowmap = this.Object.GetProperty(PropNames.TWODFX_LIGHT_SHADOW_MAP, PropDefaults.TWODFX_LIGHT_SHADOW_MAP);
            //float cone_inner = this.Object.GetProperty(PropNames.TWODFX_LIGHT_HOTSPOT, PropDefaults.TWODFX_LIGHT_HOTSPOT);
            //float cone_outer = this.Object.GetProperty(PropNames.TWODFX_LIGHT_FALLSIZE, PropDefaults.TWODFX_LIGHT_FALLSIZE);
            //float intensity = this.Object.GetProperty(PropNames.TWODFX_LIGHT_INTENSITY, PropDefaults.TWODFX_LIGHT_INTENSITY);
            //float atten_start = this.Object.GetProperty(PropNames.TWODFX_LIGHT_ATTENUTAION_START, PropDefaults.TWODFX_LIGHT_ATTENUTAION_START);
            //float atten_end = this.Object.GetProperty(PropNames.TWODFX_LIGHT_ATTENUATION_END, PropDefaults.TWODFX_LIGHT_ATTENUATION_END);
            //cone_inner /= 2.0f;
            //cone_outer /= 2.0f;
            //uint flags = Flags.GetLightFlags(this.Object);
            //uint lighttype = is_spot ? Flags.TWODFX_LIGHT_SPOT : Flags.TWODFX_LIGHT_STANDARD;

            //Matrix34f matlocal = new Matrix34f(this.Object.NodeTransform);
            //Matrix34f matparent = null;
            //if (this.Object.HasParent())
            //    matparent = new Matrix34f(this.Object.Parent.NodeTransform);
            //else
            //    matparent = new Matrix34f();
            //// Get light's relative transform from parent
            //matlocal = matlocal * matparent.Inverse();
            //Matrix34f matrot = new Matrix34f(matlocal);
            //matrot.Translation = new Vector3f();
            //Vector3f direction = new Vector3f(0.0f, 0.0f, -1.0f);
            //Vector3f up = new Vector3f(1.0f, 0.0f, 0.0f);

            //Vector3f relPos = matlocal.Translation;
            //Quaternionf relRot = new Quaternionf(matlocal);
            //Quaternionf relRotInv = new Quaternionf(relRot);
            //relRotInv.Invert();
            //direction = (new Matrix34f(relRotInv) * direction);
            //direction.Normalise();

            //// See lights.cpp: ChangeLightDataInEffect()
            //Vector3f tmp = Vector3f.Cross(new Vector3f(0.0f, 1.0f, 0.0f), direction);
            //up = Vector3f.Cross(direction, tmp);
            //up.Normalise();

            //String ide_line = String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, \"{14}\", \"{15}\", {16}, {17}, {18}, {19}, {20}, {21}, {22}, {23}, {24}, {25}, {26}, {27}, {28}, {29}, {30}, {31}, {32}, {33}, {34}, {35}, {36}, {37}, {38}, {39}, {40}, {41}, {42}",
            //    this.Object.GetObjectName(),
            //    relPos.X, relPos.Y, relPos.Z,
            //    META2dfxTypes.TWODFX_LIGHT,
            //    relRot.X, relRot.Y, relRot.Z, relRot.W,
            //    lighttype,
            //    SMath.Round(colour.X * 255), SMath.Round(colour.Y * 255), SMath.Round(colour.Z * 255), 255,
            //    corona_name, System.IO.Path.GetFileNameWithoutExtension(shadowmap), loddist, size,
            //    corona_size, 0.0f, 0, flashiness,
            //    0.0f, 0.0f, flags, Convert.ToInt32(cone_inner),
            //    Convert.ToInt32(cone_outer), intensity, volinten, volsize,
            //    atten_start, atten_end,
            //    direction.X, direction.Y, direction.Z,
            //    up.X, up.Y, up.Z,
            //    attach, fadedist, volfadedist, corona_hdr, Flags.GetTimeFlags(this.Object));
            //return (ide_line);
            return null;
        }

        /// <summary>
        /// Return XNode representation of an "RAGE Particle" 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Format of a "RAGE Particle" effect is:
//        ///   *common*, tag, trigger, boneid, scale, prob, flags, r, g, b
        ///       <Item>
         ///         ... (common values)
        ///         <isTriggered value = "true" />
        ///         <boneTag value ="55988" />
        ///         <startPhase value = "0.0" />
        ///         <endPhase value = "0.0" />
        ///         <ptFxTag>RAY_TEST_TRIGGER</ptFxTag>
        ///         <ptFxScale value ="1.0"/>
        ///         <ptFxProbability value ="100.0"/>
        ///         <ptFxHasTint value ="0"/>
        ///         <ptFxTintR value ="0"/>
        ///         <ptFxTintG value ="0"/>
        ///         <ptFxTintB value ="0"/>
        ///         <sizeX value ="0"/>
        ///         <sizeY value ="0"/>
        ///         <sizeZ value ="0"/>
        ///       </Item>
        ///   
        private XNode ToMETA2dfxParticleEffect(TargetObjectDef rootObject)
        {
           XElement rootNode = new XElement("Item");

           String tag = this.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_NAME,
                AttrDefaults.TWODFX_PARTICLE_NAME);
            bool isTriggered = this.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_IS_TRIGGERED,
                AttrDefaults.TWODFX_PARTICLE_IS_TRIGGERED);
            int boneid = this.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_ATTACH,
                AttrDefaults.TWODFX_PARTICLE_ATTACH);
            float prob = this.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_PROBABILITY,
                AttrDefaults.TWODFX_PARTICLE_PROBABILITY);
            float scale = this.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_SCALE,
                AttrDefaults.TWODFX_PARTICLE_SCALE);
            UInt32 flags = Flags.GetParticleFlags(this.Object);
            int r = this.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_COLOUR_R,
                AttrDefaults.TWODFX_PARTICLE_COLOUR_R);
            int g = this.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_COLOUR_G,
                AttrDefaults.TWODFX_PARTICLE_COLOUR_G);
            int b = this.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_COLOUR_B,
                AttrDefaults.TWODFX_PARTICLE_COLOUR_B);
            float size_x = this.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_SIZE_X,
                AttrDefaults.TWODFX_PARTICLE_SIZE_X);
            float size_y = this.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_SIZE_Y,
                AttrDefaults.TWODFX_PARTICLE_SIZE_Y);
            float size_z = this.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_SIZE_Z,
                AttrDefaults.TWODFX_PARTICLE_SIZE_Z);

            float relativeStartTime = 0.0f;
            float relativeStopTime = 1.0f;
            List<RSG.SceneXml.sKeyFrame> ActiveKeys;
            if (this.Object.KeyedProperties.TryGetValue("active", out ActiveKeys))
            {
                if(ActiveKeys.Count>0)
                {
                    int objectAnimLength = rootObject.AnimLength;
                    if (objectAnimLength > 0)
                    {
                        int firstStartTime = 999999;
                        int lastStopTime = 0;
                        foreach (sKeyFrame keyframe in ActiveKeys)
                        {
                            if (keyframe.m_nState == 1 && keyframe.m_nTime < firstStartTime)
                                firstStartTime = keyframe.m_nTime;
                            if (keyframe.m_nState == 0 && keyframe.m_nTime > lastStopTime)
                                lastStopTime = keyframe.m_nTime;
                        }
                        relativeStartTime = ((float)firstStartTime) / ((float)objectAnimLength);
                        relativeStopTime = ((float)lastStopTime) / ((float)objectAnimLength);
                        if ((relativeStopTime > 1.0f) ||
                            (relativeStopTime < relativeStartTime))
                        {
                            relativeStopTime = 1.0f;
                        }
                    }
                    else
                    {
                        Log.Log__DebugCtx(rootObject.Name, "Object {0} has zero animation length!", rootObject.Name);
                    }
                }
            }
            else
            {
                Log.Log__MessageCtx(this.Object.Name, "Object {0} doesn't have \"active\" keyframes.", this.Object.Name);
            }

            if (isTriggered)
                relativeStopTime = relativeStartTime;

            // output common data
            ToMETA2dfxCommon(META2dfxTypes.TWODFX_PARTICLE_EFFECT, rootNode);

            XElement boneNode = new XElement("boneTag");
            boneNode.Add(new XAttribute("value", boneid.ToString()));
            rootNode.Add(boneNode);

            XElement startNode = new XElement("startPhase");
            startNode.Add(new XAttribute("value", relativeStartTime.ToString()));
            rootNode.Add(startNode);
            XElement endNode = new XElement("endPhase");
            endNode.Add(new XAttribute("value", relativeStopTime.ToString()));
            rootNode.Add(endNode);

            XElement triggerNode = new XElement("ptFxIsTriggered");
            triggerNode.Add(new XAttribute("value", isTriggered.ToString()));
            rootNode.Add(triggerNode);

            XElement ptfxNode = new XElement("ptFxTag");
            XText ptfxtext = new XText(tag.ToString());
            ptfxNode.Add(ptfxtext);
            rootNode.Add(ptfxNode);

            XElement scaleNode = new XElement("ptFxScale");
            scaleNode.Add(new XAttribute("value", scale.ToString()));
            rootNode.Add(scaleNode);

            XElement probNode = new XElement("ptFxProbability");
            probNode.Add(new XAttribute("value", prob.ToString()));
            rootNode.Add(probNode);

            XElement tintFlagNode = new XElement("ptFxHasTint");
            bool hasTint = (r != AttrDefaults.TWODFX_PARTICLE_COLOUR_R) || (g != AttrDefaults.TWODFX_PARTICLE_COLOUR_G) || (b != AttrDefaults.TWODFX_PARTICLE_COLOUR_B);
            tintFlagNode.Add(new XAttribute("value", hasTint.ToString()));
            rootNode.Add(tintFlagNode);

            XElement tintNodeR = new XElement("ptFxTintR");
            tintNodeR.Add(new XAttribute("value", r.ToString()));
            rootNode.Add(tintNodeR);
            XElement tintNodeG = new XElement("ptFxTintG");
            tintNodeG.Add(new XAttribute("value", g.ToString()));
            rootNode.Add(tintNodeG);
            XElement tintNodeB = new XElement("ptFxTintB");
            tintNodeB.Add(new XAttribute("value", b.ToString()));
            rootNode.Add(tintNodeB);

            XElement sizeNode = new XElement("ptFxSize");
            sizeNode.Add(new XAttribute("x", size_x.ToString()));
            sizeNode.Add(new XAttribute("y", size_y.ToString()));
            sizeNode.Add(new XAttribute("z", size_z.ToString()));
            rootNode.Add(sizeNode);

            return rootNode;
        }

        /// <summary>
        /// Return String representation for a "Gta Scrollbar" 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Format of a "Gta Scrollbar" 2dfx object is:
        ///   *common*, height, type, numpoints-1, x1, y1, z1, ..., xn, yn, zn
        ///
        private XNode ToMETA2dfxScrollbars(TargetObjectDef rootObject)
        {
            //float height = this.Object.GetParameter(ParamNames.TWODFX_SCROLLBARS_HEIGHT,
            //    ParamDefaults.TWODFX_SCROLLBARS_HEIGHT);
            //int type = this.Object.GetParameter(ParamNames.TWODFX_SCROLLBARS_TYPE,
            //    ParamDefaults.TWODFX_SCROLLBARS_TYPE);
            //int numpoints = this.Object.GetParameter(ParamNames.TWODFX_SCROLLBARS_NUMPOINTS,
            //    ParamDefaults.TWODFX_SCROLLBARS_NUMPOINTS);

            //String ide_line = String.Format("{0}, {1}, {2}, {3}",
            //    ToMETA2dfxCommon(META2dfxTypes.TWODFX_SCROLLBARS),
            //    height, type, numpoints - 1);
            //if ((numpoints - 1) > 0)
            //{
            //    Matrix34f mtxObject = this.Object.NodeTransform.Inverse();
            //    ide_line += ", ";
            //    int n = 0;
            //    foreach (ObjectDef o in this.Object.SubObjects)
            //    {
            //        if (0 != n)
            //        {
            //            // Calculate point positions in local-space.
            //            Matrix34f mtxLocal = mtxObject * o.NodeTransform;
            //            Vector3f pos = mtxLocal.Translation;
            //            if (n == (this.Object.SubObjects.Length - 1))
            //                ide_line += String.Format("{0}, {1}, {2}", pos.X, pos.Y, pos.Z);
            //            else
            //                ide_line += String.Format("{0}, {1}, {2}, ", pos.X, pos.Y, pos.Z);
            //        }
            //        ++n;
            //    }
            //}
            //return (ide_line);
            return null;
        }

        /// <summary>
        /// Return String representation of a SpawnPoint 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Fomrat of a SpawnPoint 2dfx is:
        ///   *common*, spawntype, pedtype, start, end
        /// 
        private XNode ToMETA2dfxSpawnPoint(TargetObjectDef rootObject)
        {
            //String spawnType = this.Object.GetParameter(ParamNames.TWODFX_SPAWNPOINT_SPAWNTYPE,
            //    ParamDefaults.TWODFX_SPAWNPOINT_SPAWNTYPE);
            //uint hSpawnType = (uint)RSG.ManagedRage.StringHashUtil.atStringHash(spawnType, 0);
            //String pedType = this.Object.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_PED_TYPE,
            //    AttrDefaults.TWODFX_SPAWNPOINT_PED_TYPE);
            //uint hPedType = (uint)RSG.ManagedRage.StringHashUtil.atStringHash(pedType, 0);
            //int start = this.Object.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_START_TIME,
            //    AttrDefaults.TWODFX_SPAWNPOINT_START_TIME);
            //int end = this.Object.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_END_TIME,
            //    AttrDefaults.TWODFX_SPAWNPOINT_END_TIME);

            //String ide_line = String.Format("{0}, {1}, {2}, {3}, {4}",
            //    ToMETA2dfxCommon(META2dfxTypes.TWODFX_SPAWN_POINT),
            //    hSpawnType, hPedType, start, end);
            //return (ide_line);
            return null;
        }

        /// <summary>
        /// Return String representation of a Swayable 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Format of a Swayable 2dfx is:
        ///   *common*, boneid, lowwindspeed, lowwindampl, highwindspeed, highwindampl
        ///   
        private XNode ToMETA2dfxSwayableEffect(TargetObjectDef rootObject)
        {
            //int boneid = this.Object.GetAttribute(AttrNames.TWODFX_SWAYABLE_BONEID,
            //    AttrDefaults.TWODFX_SWAYABLE_BONEID);
            //float lwspeed = this.Object.GetAttribute(AttrNames.TWODFX_SWAYABLE_LOW_WIND_SPEED,
            //    AttrDefaults.TWODFX_SWAYABLE_LOW_WIND_SPEED);
            //float lwampl = this.Object.GetAttribute(AttrNames.TWODFX_SWAYABLE_LOW_WIND_AMPLITUDE,
            //    AttrDefaults.TWODFX_SWAYABLE_LOW_WIND_AMPLITUDE);
            //float hwspeed = this.Object.GetAttribute(AttrNames.TWODFX_SWAYABLE_HIGH_WIND_SPEED,
            //    AttrDefaults.TWODFX_SWAYABLE_HIGH_WIND_SPEED);
            //float hwampl = this.Object.GetAttribute(AttrNames.TWODFX_SWAYABLE_HIGH_WIND_AMPLITUDE,
            //    AttrDefaults.TWODFX_SWAYABLE_HIGH_WIND_AMPLITUDE);

            //String ide_line = String.Format("{0}, {1}, {2}, {3}, {4}, {5}",
            //    ToMETA2dfxCommon(META2dfxTypes.TWODFX_SWAYABLE_EFFECT),
            //    boneid, lwspeed, lwampl, hwspeed, lwampl);
            //return (ide_line);
            return null;
        }

        /// <summary>
        /// Return String representation of a Light Shaft 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Format of a LightShaft 2dfx is:
        ///   *common*, a.x, a.y, a.z, b.x, b.y, b.z, c.x, c.y, c.z, d.x, d.y, d.z, length, density, intensity
        ///
        private XNode ToMETA2dfxLightShaft(TargetObjectDef rootObject)
        {
            //Matrix34f matParent = this.Object.Parent.NodeTransform.Inverse();
            //Quaternionf nodeRot = new Quaternionf(this.Object.NodeTransform);
            //Matrix34f nodeMtx = new Matrix34f(nodeRot);
            //nodeMtx.Translation = this.Object.NodeTransform.Translation;
            //nodeMtx *= matParent;

            //float lengthdiv2 = this.Object.GetParameter(ParamNames.TWODFX_LIGHTSHAFT_LENGTH, ParamDefaults.TWODFX_LIGHTSHAFT_LENGTH) / 2.0f;
            //float widthdiv2 = this.Object.GetParameter(ParamNames.TWODFX_LIGHTSHAFT_WIDTH, ParamDefaults.TWODFX_LIGHTSHAFT_WIDTH) / 2.0f;
            //Vector3f a = nodeMtx * (new Vector3f(widthdiv2, -lengthdiv2, 0.0f));
            //Vector3f b = nodeMtx * (new Vector3f(widthdiv2, lengthdiv2, 0.0f));
            //Vector3f c = nodeMtx * (new Vector3f(-widthdiv2, lengthdiv2, 0.0f));
            //Vector3f d = nodeMtx * (new Vector3f(-widthdiv2, -lengthdiv2, 0.0f));

            //float length = this.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_LENGTH, AttrDefaults.TWODFX_LIGHTSHAFT_LENGTH);
            //float density = this.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_DENSITY, AttrDefaults.TWODFX_LIGHTSHAFT_DENSITY);
            //float intensity = this.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_INTENSITY, AttrDefaults.TWODFX_LIGHTSHAFT_INTENSITY);
            //UInt32 flags = Flags.GetLightShaftFlags(this.Object);
            //float directionX = this.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_DIRECTION_X, AttrDefaults.TWODFX_LIGHTSHAFT_DIRECTION_X);
            //float directionY = this.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_DIRECTION_Y, AttrDefaults.TWODFX_LIGHTSHAFT_DIRECTION_Y);
            //float directionZ = this.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_DIRECTION_Z, AttrDefaults.TWODFX_LIGHTSHAFT_DIRECTION_Z);
            //Vector3f direction = new Vector3f(directionX, directionY, directionZ);
            //direction.Normalise();
            //int colourR = this.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_COLOUR_R, AttrDefaults.TWODFX_LIGHTSHAFT_COLOUR_R);
            //int colourG = this.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_COLOUR_G, AttrDefaults.TWODFX_LIGHTSHAFT_COLOUR_G);
            //int colourB = this.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_COLOUR_B, AttrDefaults.TWODFX_LIGHTSHAFT_COLOUR_B);

            //String ide_line = String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19}, {20}, {21}, {22}",
            //    ToMETA2dfxCommon(META2dfxTypes.TWODFX_LIGHT_SHAFT),
            //    a.X, a.Y, a.Z, b.X, b.Y, b.Z,
            //    c.X, c.Y, c.Z, d.X, d.Y, d.Z,
            //    length, density, intensity,
            //    flags,
            //    direction.X, direction.Y, direction.Z,
            //    colourR, colourG, colourB);
            //return (ide_line);
            return null;
        }

        /// <summary>
        /// Return String representation of a Proc Object 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Format of a Proc 2dfx is:
        ///   *common*, radinner, radouter, id, spacing, minscale, maxscale, minscaleZ, maxscaleZ, zoffsetMin, zoffsetMax, flags
        ///
        private XNode ToMETA2dfxProcObject(TargetObjectDef rootObject)
        {
            //String id = this.Object.GetAttribute(AttrNames.TWODFX_PROC_OBJECT, AttrDefaults.TWODFX_PROC_OBJECT);
            //UInt32 objid = (uint)RSG.ManagedRage.StringHashUtil.atStringHash(id, 0);
            //float radius_inner = 0.0f;
            //float radius_outer = 0.0f;
            //float spacing = this.Object.GetAttribute(AttrNames.TWODFX_PROC_SPACING, AttrDefaults.TWODFX_PROC_SPACING);
            //float minscale = this.Object.GetAttribute(AttrNames.TWODFX_PROC_MIN_SCALE, AttrDefaults.TWODFX_PROC_MIN_SCALE);
            //float maxscale = this.Object.GetAttribute(AttrNames.TWODFX_PROC_MAX_SCALE, AttrDefaults.TWODFX_PROC_MAX_SCALE);
            //float minscalez = this.Object.GetAttribute(AttrNames.TWODFX_PROC_MIN_SCALE_Z, AttrDefaults.TWODFX_PROC_MIN_SCALE_Z);
            //float maxscalez = this.Object.GetAttribute(AttrNames.TWODFX_PROC_MAX_SCALE_Z, AttrDefaults.TWODFX_PROC_MAX_SCALE_Z);
            //float minzoffset = this.Object.GetAttribute(AttrNames.TWODFX_PROC_MIN_Z_OFFSET, AttrDefaults.TWODFX_PROC_MIN_Z_OFFSET);
            //float maxzoffset = this.Object.GetAttribute(AttrNames.TWODFX_PROC_MAX_Z_OFFSET, AttrDefaults.TWODFX_PROC_MAX_Z_OFFSET);
            //uint flags = Flags.GetProcObjectFlags(this.Object);

            //String ide_line = String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}",
            //    ToMETA2dfxCommon(META2dfxTypes.TWODFX_PROC_OBJECT),
            //    radius_inner, radius_outer,
            //    objid,
            //    spacing, minscale, maxscale, minscalez, maxscalez,
            //    minzoffset, maxzoffset, flags);
            //return (ide_line);
            return null;
        }

        /// <summary>
        /// Return String representation of a WalkDontWalk 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Format of a walk dont walk 2dfx is:
        ///   *common*
        ///   
        private XNode ToMETA2dfxWalkDontWalk(TargetObjectDef rootObject)
        {
            //String ide_line = String.Format("{0}", ToMETA2dfxCommon(META2dfxTypes.TWODFX_WALK_DONT_WALK));
            //return (ide_line);
            return null;
        }

        /// <summary>
        /// Return String representation of a RsWindDisturbance 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Format of a RsWindDisturbance 2dfx is:
        ///   
        ///   
        private XNode ToMETA2dfxRsWindDisturbance(TargetObjectDef rootObject)
        {
            return null;
        }

        /// <summary>
        /// Return String representation of a Script 2dfx.
        /// </summary>
        /// <returns></returns>
        private XNode ToMETA2dfxScript(TargetObjectDef rootObject)
        {
            //List<ObjectDef> scriptChildren = new List<ObjectDef>();
            //foreach (ObjectDef obj in this.Object.Children)
            //{
            //    if (obj.Is2dfxScript())
            //        scriptChildren.Add(obj);
            //}

            //String ide_line = String.Format("{0}, {1}, {2}",
            //    ToIDE2dfxCommon(META2dfxTypes.TWODFX_SCRIPT),
            //    this.Object.GetAttribute(AttrNames.SCRIPT_NAME, AttrDefaults.SCRIPT_NAME),
            //    scriptChildren.Count);
            //foreach (ObjectDef obj in scriptChildren)
            //{
            //    Matrix34f mtxParent = this.Object.NodeTransform.Inverse();
            //    Matrix34f mtxLocal = this.Object.NodeTransform * mtxParent;
            //    Vector3f eulers = mtxLocal.ToEulers();

            //    ide_line += String.Format(", {0}, {1}, {2}, {3}",
            //        mtxLocal.Translation.X,
            //        mtxLocal.Translation.Y,
            //        mtxLocal.Translation.Z,
            //        eulers.Z);
            //}

            //return (ide_line);
            return null;
        }

        /// <summary>
        /// Return String representation of a GtaLadderFX 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Format of a ladder 2dfx is:
        ///   name, 0, 0, 0, type_id, 0, 0, 0, -1, bottom.x, bottom.y, bottom.z, top.x, top.y, top.z, normal.x, normal.y, normal.z, canGetOffAtTop
        ///   
        /// Note: position information is always zero.
        /// 
        private XNode ToMETA2dfxLadder(TargetObjectDef rootObject)
        {
            //Matrix34f mtxParent = new Matrix34f();
            //if (this.Object.HasParent())
            //    mtxParent = this.Object.Parent.NodeTransform.Inverse();
            //bool canGetOff = this.Object.GetAttribute(AttrNames.TWODFX_LADDER_CAN_GET_OFF_AT_TOP, AttrDefaults.TWODFX_LADDER_CAN_GET_OFF_AT_TOP);
            //Vector3f bottom = mtxParent * this.Object.SubObjects[Subobjects.TWODFX_LADDER_BOTTOM].NodeTransform.Translation;
            //Vector3f top = mtxParent * this.Object.SubObjects[Subobjects.TWODFX_LADDER_TOP].NodeTransform.Translation;
            //Vector3f normal = mtxParent * this.Object.SubObjects[Subobjects.TWODFX_LADDER_NORMAL].NodeTransform.Translation;
            //normal -= bottom;
            //normal.Normalise();

            //String ide_line = String.Format("{0}, 0, 0, 0, {1}, 0, 0, 0, -1, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}",
            //    this.Object.GetObjectName(),
            //    META2dfxTypes.TWODFX_LADDER,
            //    bottom.X, bottom.Y, bottom.Z,
            //    top.X, top.Y, top.Z,
            //    normal.X, normal.Y, normal.Z,
            //    canGetOff ? "1" : "0");
            //return (ide_line);
            return null;
        }

        /// <summary>
        /// Return String representation of a "RSLedge" 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Format of a ledge object is:
        ///   name, 0, 0, 0, type_id, 0, 0, 0, -1, left.x, left.y, left.z, right.x, right.y, right.z, normal.x, normal.y, normal.z
        ///   
        /// Note: position information is always zero.
        /// 
        private XNode ToMETA2dfxLedge(TargetObjectDef rootObject)
        {
            //if (null == this.Object.Parent)
            //{
            //    String message = String.Format("RSLedge {0} has no parent object.", this);
            //    Debug.Assert(null != this.Object.Parent, message);
            //    Log.Log__Error(message);
            //    return (String.Empty);
            //}

            //// DHM 8 June 2009
            //// HACK as SceneXml is outputting some custom (*HACK*) attributes for
            //// ledge objects.  We just write those out.
            //Vector3f left = this.Object.CustomAttributes["ledge_left"] as Vector3f;
            //Vector3f right = this.Object.CustomAttributes["ledge_right"] as Vector3f;
            //Vector3f normal = this.Object.CustomAttributes["ledge_normal"] as Vector3f;

            //String ide_line = String.Format("{0}, 0, 0, 0, {1}, 0, 0, 0, -1, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}",
            //    this.Object.GetObjectName(),
            //    META2dfxTypes.TWODFX_LEDGE,
            //    left.X, left.Y, left.Z,
            //    right.X, right.Y, right.Z,
            //    normal.X, normal.Y, normal.Z);
            //return (ide_line);
            return null;
        }
        #endregion // nested effect methods

        #region Helper methods

        /// <summary>
        /// Process a single SceneXml ObjectDef for 2DFX objects.
        /// </summary>
        /// <param name="obj"></param>
        private void ProcessScene2DFX(Scene scene, List<Definition> l2dfx, TargetObjectDef obj)
        {
            if (obj.DontExport() || obj.DontExportIDE())
                return;

            if (obj.Is2dfx() && obj.HasParent())
                l2dfx.Add(new Definition(scene, obj));

            // Children (for 2DFX entries)
            foreach (TargetObjectDef o in obj.Children)
                ProcessScene2DFX(scene, l2dfx, o);
        }

        #endregion // Helper Methods

        #endregion //XML export methods
    }

    #region DefinitionIDEComparer Class
    /// <summary>
    /// Definition Objects comparison interface.
    /// </summary>
    /// We compare two Definition objects by comparing their contained object's
    /// name (String) to produce a list in alphabetical (ASCII) order.
    /// 
    internal class DefinitionIDEComparer : System.Collections.Generic.IComparer<Definition>
    {
        /// <summary>
        /// Compare two Definition objects.
        /// </summary>
        /// <param name="x">First Definition</param>
        /// <param name="y">Second Definition</param>
        /// <returns></returns>
        public int Compare(Definition x, Definition y)
        {
            return (String.Compare(x.Object.GetObjectName(), y.Object.GetObjectName(), StringComparison.OrdinalIgnoreCase));
        }
    }
    #endregion // DefinitionIDEComparer Class

} // RSG.SceneXml.MapExport

// Definition.cs
