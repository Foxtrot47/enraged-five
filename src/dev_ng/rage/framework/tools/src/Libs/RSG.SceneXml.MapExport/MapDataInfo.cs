﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;

namespace RSG.SceneXml.MapExport
{
    public class MapDataInfo
    {
        #region Variables
        public String Name;
        public BoundingBox3f EntityBoundingBox;
        #endregion

        #region Constructors
        public MapDataInfo()
        {
            Name = null;
            EntityBoundingBox = new BoundingBox3f();
        }

        public MapDataInfo(String name)
        {
            Name = name;
            EntityBoundingBox = new BoundingBox3f();
        }
        #endregion

        #region Static Functions
        /// <summary>
        /// Checks a map container for all intersecting containers and returns a list of them.
        /// </summary>
        /// <param name="mapContainer"></param>
        /// <param name="mapContainer"></param>
        /// <returns></returns>
        public static bool CheckMapIntersections(String mapContainer, ref List<MapDataInfo> mapDataInfoList, ref List<MapDataInfo> intersectingList)
        {
            if (mapDataInfoList.Count == 0)
                return false;

            MapDataInfo currentMap = mapDataInfoList.Where(map => map.Name.ToLower() == mapContainer.ToLower()).FirstOrDefault();

            if (currentMap == null)
                return false;

            // Check all the other containers and see if there are any bounding boxes that intersect.
            foreach (MapDataInfo testMap in mapDataInfoList)
            {
                if (testMap.Name == currentMap.Name)
                    continue;

                if (testMap.EntityBoundingBox.Intersects(currentMap.EntityBoundingBox))
                    intersectingList.Add(testMap);
            }

            return true;
        }
        #endregion
    }
}
