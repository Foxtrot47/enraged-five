﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging;

namespace RSG.SceneXml.MapExport
{
    /// <summary>
    /// 
    /// </summary>
    public class EffectCollection
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<String, String> ms_PtfxAssetNames = new Dictionary<String, String>();
        private Dictionary<String, PtfxTriggerType> ms_PtfxTriggerTypes = new Dictionary<string, PtfxTriggerType>();

        /// <summary>
        /// 
        /// </summary>
        public enum PtfxTriggerType
        {
            Unknown = -1,

            AMBIENT = 0,
            COLLISION,
            SHOT,
            BREAK,
            DESTROY,
            ANIM,
            RAYFIRE,
            INWATER
        };

        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Static constructor.
        /// </summary>
        public EffectCollection(IBranch branch = null)
        {
            if (null == branch)
            {
                IConfig config = ConfigFactory.CreateConfig();
                branch = config.Project.DefaultBranch;
            }
            // Titleupdate first, then fall back to core if it doesn't exist.
            String entityFXFilename = Path.Combine(branch.Project.Config.CoreProject.Branches[branch.Name].Project.Root, "titleupdate", branch.Name, "common", "data", "effects", "entityfx.dat");
            if (!File.Exists(entityFXFilename))
            {
                if (branch.Project.Config.CoreProject.Branches.ContainsKey(branch.Name))
                    entityFXFilename = Path.Combine(branch.Project.Config.CoreProject.Branches[branch.Name].Common, "data", "effects", "entityfx.dat");
            }
            Reload(entityFXFilename);
        }
        #endregion // Constructor(s)

        #region Controller Methods

        /// <summary>
        /// Return the asset name for a given effect name.
        /// </summary>
        /// <param name="effect"></param>
        /// <returns></returns>
        public String GetAssetNameForEffect(String effect)
        {
            effect = effect.ToUpper();
            if (ms_PtfxAssetNames.ContainsKey(effect))
                return (ms_PtfxAssetNames[effect]);
            return (String.Empty);
        }

        /// <summary>
        /// Return the (hard-coded) trigger index for a given effect name
        /// </summary>
        /// AJM: This is nasty and hard-coded values but it'll get changed to a hash at some point in
        /// future to make it less evil.
        /// <param name="effect"></param>
        /// <returns></returns>
        public PtfxTriggerType GetAssetTriggerTypeForEffect(String effect, String archetypeName, String sceneName)
        {
            if (ms_PtfxTriggerTypes.ContainsKey(effect.ToUpper()))
                return (ms_PtfxTriggerTypes[effect.ToUpper()]);
            else
            {
                Log.StaticLog.Error("PTFX Trigger for '{0}' attached to object '{1}' in '{2}' was not found.", effect, archetypeName, sceneName);
                return PtfxTriggerType.Unknown;
            }
        }

        /// <summary>
        /// Return an Array of effect names for a given asset name.
        /// </summary>
        /// <param name="assetName"></param>
        /// <returns></returns>
        public String[] GetEffectsForAssetName(String assetName)
        {
            return ms_PtfxAssetNames.Where(kvp => kvp.Value == assetName).Select(kvp => kvp.Key).ToArray();
        }

        /// <summary>
        /// Gets the trigger idx for a given effect name (based on prefix currently)
        /// </summary>
        /// AJM: This is dirty and just uses the naming convention for particles in the entityfx.dat file
        /// to find the trigger idx.  
        /// <param name="effect"></param>
        /// <returns></returns>
        public PtfxTriggerType GetTriggerForAssetName(String effectName)
        {
            if (effectName.StartsWith("AMB"))
            {
                return PtfxTriggerType.AMBIENT;
            }
            else if (effectName.StartsWith("COL"))
            {
                return PtfxTriggerType.COLLISION;
            }
            else if (effectName.StartsWith("SHT"))
            {
                return PtfxTriggerType.SHOT;
            }
            else if (effectName.StartsWith("BRK"))
            {
                return PtfxTriggerType.BREAK;
            }
            else if (effectName.StartsWith("DST"))
            {
                return PtfxTriggerType.DESTROY;
            }
            else if (effectName.StartsWith("ANM"))
            {
                return PtfxTriggerType.ANIM;
            }
            else if (effectName.StartsWith("RAY"))
            {
                return PtfxTriggerType.RAYFIRE;
            }
            else if (effectName.StartsWith("WTR"))
            {
                return PtfxTriggerType.INWATER;
            }
            else
            {
                // Some decal information stored in entityfx too so they will hit this
                Log.Log__Warning("PTFX Trigger Index for '{0}' could not be set.", effectName);
                return PtfxTriggerType.Unknown;
            }
        }

        /// <summary>
        /// Reload effect data.
        /// </summary>
        /// <param name="filename"></param>
        public void Reload(String filename)
        {
            ms_PtfxAssetNames.Clear();
            ms_PtfxTriggerTypes.Clear();

            if (!File.Exists(filename))
            {
                Log.Log__Warning("Material collection file '{0}' does not exist, no PTFX Asset Names or trigger types could be loaded.",
                    filename);
            }
            else
            {
                using (TextReader tr = new StreamReader(filename))
                {
                    String line = String.Empty;
                    while (null != (line = tr.ReadLine()))
                    {
                        if (String.IsNullOrEmpty(line))
                            continue;

                        // Ignore comments in the file
                        if (!line.StartsWith("#"))
                        {
                            // Only use lines that have more than one token
                            char[] delims = new char[] { ' ', '\t' };
                            string[] tokens = line.Split(delims, 3, StringSplitOptions.RemoveEmptyEntries);
                            if (tokens.Length > 1)
                            {
                                string tagToken = tokens[0].ToUpper();
                                if (ms_PtfxAssetNames.ContainsKey(tagToken))
                                {
                                    Log.Log__Error("{0} contains double entry for \"{1}\"!", filename, tagToken);
                                    continue;
                                }
                                ms_PtfxAssetNames.Add(tagToken, tokens[1]);
                                PtfxTriggerType triggerType = GetTriggerForAssetName(tagToken);

                                // There are some other types in the entityfx file e.g. ENTITYFX_COLLISION_DECAL_START
                                // so we want to skip adding the types in those sections to the dictionary
                                if (triggerType != PtfxTriggerType.Unknown)
                                    ms_PtfxTriggerTypes.Add(tagToken, triggerType);
                            }
                        }
                    }
                }
            }
        }
        #endregion // Controller Methods
    }

} // RSG.SceneXml.MapExport namespace
