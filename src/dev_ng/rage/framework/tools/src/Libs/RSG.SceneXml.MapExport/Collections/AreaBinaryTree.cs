//
// File: AreaBinaryTree.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of AreaBinaryTree class
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using RSG.Base.Collections;
using RSG.Base.Math;

namespace RSG.SceneXml.MapExport.Collections
{

    /// <summary>
    /// 
    /// </summary>
    public class AreaBinaryTree<T> : BinaryTree<T>
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public AreaBinaryTree()
            : base()
        {
        }
        #endregion // Constructor(s)
    }

    /// <summary>
    /// 
    /// </summary>
    public class AreaBinaryTreeNode<T> : BinaryTreeNode<T>
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Area defined by this node.
        /// </summary>
        public BoundingBox2f Area
        {
            get { return m_Area; }
        }
        private BoundingBox2f m_Area;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="area"></param>
        public AreaBinaryTreeNode(BoundingBox2f area)
            : base()
        {
            this.m_Area = area;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="area"></param>
        /// <param name="container"></param>
        public AreaBinaryTreeNode(BoundingBox2f area, T container)
            : base(container)
        {
            this.m_Area = area;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="area"></param>
        /// <param name="container"></param>
        /// <param name="left"></param>
        /// <param name="right"></param>
        public AreaBinaryTreeNode(BoundingBox2f area, T container, AreaBinaryTreeNode<T> left, AreaBinaryTreeNode<T> right)
            : base(container, left, right)
        {
            this.m_Area = area;
        }
        #endregion // Constructor(s)
    }

} // RSG.SceneXml.MapExport.Collections namespace


// AreaBinaryTree.cs
