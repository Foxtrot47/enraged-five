﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.SceneXml.MapExport.Project.GTA5_NG
{

    /// <summary>
    /// Class deserialises ITYP files; we read core game ITYPs when serialising 
    /// out DLC ITYP and IMAP files to ensure the dependencies are accurate.
    /// </summary>
    public class SceneDeserialiserITYP
    {
        #region Properties
        /// <summary>
        /// ITYP basename.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Contained archetype lite objects.
        /// </summary>
        public IEnumerable<ArchetypeLite> ArchetypeObjects
        {
            get { return (m_lArchetypeObjects); }
        }
        private List<ArchetypeLite> m_lArchetypeObjects;

        /// <summary>
        /// Contained base archetypes. Archetypes with type 'CBaseArchetypeDef'.
        /// </summary>
        public IEnumerable<String> Archetypes 
        {
            get { return (m_lArchetypes); }
        }
        private List<String> m_lArchetypes;

        /// <summary>
        /// Contained interior archetypes. Archetypes with type 'CMloArchetypeDef'.
        /// </summary>
        public IEnumerable<String> InteriorArchetypes
        {
            get { return (m_lInteriorArchetypes); }
        }
        private List<String> m_lInteriorArchetypes;

        /// <summary>
        /// Contained time archetypes. Archetypes with type 'CTimeArchetypeDef'.
        /// </summary>
        public IEnumerable<String> TimeArchetypes
        {
            get { return (m_lTimeArchetypes); }
        }
        private List<String> m_lTimeArchetypes;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; from Stream.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="stream"></param>
        public SceneDeserialiserITYP(Stream stream)
        {
            Load(stream);
        }

        /// <summary>
        /// Constructor; from filename.
        /// </summary>
        /// <param name="filename"></param>
        public SceneDeserialiserITYP(String filename)
        {
            using (FileStream stream = File.Create(filename))
                Load(stream);
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// Load archetype information.
        /// </summary>
        /// <param name="stream"></param>
        private void Load(Stream stream)
        {
            XDocument xmlDoc = XDocument.Load(stream);
            XElement xmlNameElem = xmlDoc.XPathSelectElement("/CMapTypes/name");
            if (null != xmlNameElem)
                this.Name = xmlNameElem.Value;

            this.m_lArchetypeObjects = new List<ArchetypeLite>();
            IEnumerable<XElement> xmlArchetypeObjectElems = xmlDoc.XPathSelectElements("/CMapTypes/archetypes/Item");
            this.m_lArchetypeObjects.AddRange(xmlArchetypeObjectElems.Select(xmlElem => new ArchetypeLite(xmlElem)));


            // Old data; migrate to use ArchetypeLite (all the data is there).
            IEnumerable<XElement> xmlBaseArchetypeElems =
                xmlDoc.XPathSelectElements(
                "/CMapTypes/archetypes/Item").Where(
                x => x.Attribute("type").Value == "CBaseArchetypeDef").Select(xmlElem => xmlElem.XPathSelectElement("name"));
            m_lArchetypes = new List<String>();
            this.m_lArchetypes.AddRange(xmlBaseArchetypeElems.Select(xmlElem => xmlElem.Value));
            
            IEnumerable<XElement> xmlTimeArchetypeElems =
                xmlDoc.XPathSelectElements("/CMapTypes/archetypes/Item").Where(
                x => x.Attribute("type").Value == "CTimeArchetypeDef").Select(xmlElem => xmlElem.XPathSelectElement("name"));
            m_lTimeArchetypes = new List<String>();
            this.m_lTimeArchetypes.AddRange(xmlTimeArchetypeElems.Select(xmlElem => xmlElem.Value));
            
            IEnumerable<XElement> xmlInteriorArchetypeElems =
                xmlDoc.XPathSelectElements("/CMapTypes/archetypes/Item").Where(
                x => x.Attribute("type").Value == "CMloArchetypeDef").Select(xmlElem => xmlElem.XPathSelectElement("name"));
            m_lInteriorArchetypes = new List<String>();
            this.m_lInteriorArchetypes.AddRange(xmlInteriorArchetypeElems.Select(xmlElem => xmlElem.Value));
            
        }
        #endregion // Private Methods
    }

} // RSG.SceneXml.MapExport.Project.GTA namespace

