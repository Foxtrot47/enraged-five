//
// File: MiloRoom.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of MiloRoom.cs class
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using RSG.Base.Math;

namespace RSG.SceneXml.MapExport
{

    /// <summary>
    /// Abstraction for a Milo Room.
    /// </summary>
    /// A Milo Room has a list of child objects and a list of portals.
    internal class MiloRoom
    {
        #region Constants
        /// Number of entry indices per line.
        private static readonly int MLOROOM_NUM_ENTRY = 16;
        #endregion // Constants

        #region Properties
        public TargetObjectDef Room;
        public List<TargetObjectDef> Children;
        public List<TargetObjectDef> Portals;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public MiloRoom(TargetObjectDef room)
        {
            this.Room = room;
            this.Children = new List<TargetObjectDef>();
            this.Portals = new List<TargetObjectDef>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return String representation of MloRoom object (for IDE file).
        /// </summary>
        /// <returns></returns>
        /// Format of MloRoom line is:
        ///   name, #entries, #portals, maxx, maxy, maxz, minx, miny, minz, blend, timecycle, flags
        ///   
        public String ToIDERoom(List<TargetObjectDef> entries)
        {
            BoundingBox3f bbox = CalcRoomBounds();
            float blend = this.Room.GetAttribute(AttrNames.MLOROOM_BLEND, AttrDefaults.MLOROOM_BLEND);
            String timecycle = this.Room.GetAttribute(AttrNames.MLOROOM_TIMECYCLE, AttrDefaults.MLOROOM_TIMECYCLE);
            uint hTimecycle = 0;
            UInt32 flags = Flags.GetMloRoomFlags(this.Room);
            if ( 0 != timecycle.CompareTo(AttrDefaults.MLOROOM_TIMECYCLE) )
                hTimecycle = (uint)RSG.ManagedRage.StringHashUtil.atStringHash(timecycle, 0);

            String ide_line = String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}{12}", 
                this.Room.GetObjectName(), this.Children.Count, this.Portals.Count,
                bbox.Max.X, bbox.Max.Y, bbox.Max.Z, bbox.Min.X, bbox.Min.Y, bbox.Min.Z,
                blend, hTimecycle, flags, Environment.NewLine);
            
            // Loop through our children outputting each entry index.
            // Pretty formatting with MLOROOM_NUM_ENTRY per line.
            int n = 0;
            ide_line += "\t";
            foreach (TargetObjectDef child in this.Children)
            {
                if ((0 != n) && 0 == (n % MLOROOM_NUM_ENTRY))
                    ide_line += String.Format("{0}\t", Environment.NewLine);

                int index = entries.IndexOf(child);
                ide_line += String.Format("{0}, ", index);
                ++n;
            }
            // Round to nearest MLOROOM_NUM_ENTRY indices for last line (pad with -1).
            if (0 != (n % MLOROOM_NUM_ENTRY))
            {
                while (0 != (n % (MLOROOM_NUM_ENTRY-1)))
                {
                    ide_line += "-1, ";
                    ++n;
                }
                ide_line += "-1";
            }

            return (ide_line);
        }

        /// <summary>
        /// Calculate a MiloRoom's bounding box.
        /// </summary>
        /// <returns></returns>
        public BoundingBox3f CalcRoomBounds()
        {
            BoundingBox3f bbox = new BoundingBox3f();
            Debug.Assert(this.Room != null, "Null Room!");
            foreach (TargetObjectDef obj in this.Children)
            {
                // Skip LOD objects (for Limbo room)
                ObjectDef.InstanceType instType = obj.GetInstanceType();
                if ((ObjectDef.InstanceType.SLOD == instType) ||
                    (ObjectDef.InstanceType.LOD == instType))
                    continue;

                if ("limbo" != this.Room.Name)
                {
                    Debug.Assert(obj.Parent != null, "Object has NULL parent.");
                    Debug.Assert(obj.Parent.Equals(this.Room), String.Format("Object {0} not child of this room {1}.", obj.Name, this.Room.Name));
                }
                
                if (obj.IsXRef())
                {
                    bbox.Expand(obj.Parent.NodeTransform.Translation - obj.ObjectTransform.Translation);
                }
                else
                {
                    BoundingBox3f localbb = new BoundingBox3f(obj.LocalBoundingBox);
                    Matrix34f parentmtxinv = new Matrix34f();
                    if (null != obj.Parent)
                        parentmtxinv = obj.Parent.NodeTransform.Inverse();
                    Matrix34f nodemtx = obj.NodeTransform * parentmtxinv;

                    localbb.Min = nodemtx * localbb.Min;
                    localbb.Max = nodemtx * localbb.Max;
                    bbox.Expand(localbb);
                }
            }
            return (bbox);
        }
        #endregion // Controller Methods
    }

} // RSG.SceneXml.MapExport namespace

// MiloRoom.cs
