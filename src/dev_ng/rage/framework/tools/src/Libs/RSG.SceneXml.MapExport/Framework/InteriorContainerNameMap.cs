﻿namespace RSG.SceneXml.MapExport.Framework
{
    /// <summary>
    /// Class responsible for generating and storing unique interior IMAP and ITYP names for interiors of any flavour
    /// </summary>
    public abstract class InteriorContainerNameMap
    {
        public abstract string GetContainerNameFromObject(TargetObjectDef objectDef);
    }
}
