﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using RSG.Base.Collections;
using RSG.Base.Math;
using RSG.ManagedRage;

namespace RSG.SceneXml.MapExport.Framework.Collections
{

    /// <summary>
    /// ITYP Container class; used by the ITYP serialiser(s).
    /// </summary>
    public class ITYPContainer
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Container name (can only be set once).
        /// </summary>
        public String Name
        {
            get { return m_sName; }
            set
            {
                if (String.IsNullOrEmpty(m_sName))
                    m_sName = value;
                else
                    throw new NotSupportedException();
            }
        }
        protected String m_sName;

        /// <summary>
        /// DLC Container name (can only be set once).
        /// </summary>
        public String DLCName
        {
            get { return m_sDLCName; }
            set
            {
                if (String.IsNullOrEmpty(m_sDLCName))
                    m_sDLCName = value;
                else
                    throw new NotSupportedException();
            }
        }
        protected String m_sDLCName;

        /// <summary>
        /// Dependencies.
        /// </summary>
        public String[] Dependencies
        {
            get { return (m_Dependencies.ToArray()); }
        }
        protected List<String> m_Dependencies;

        /// <summary>
        /// Archetypes within this container.
        /// </summary>
        public Archetype[] Archetypes
        {
            get { return (m_Archetypes.ToArray()); }
        }
        protected List<Archetype> m_Archetypes;

        /// <summary>
        /// Number of archetypes within this container.
        /// </summary>
        public int ArchetypeCount
        {
            get { return m_Archetypes.Count; }
        }
        #endregion // Properties and Associated Member Data
        
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public ITYPContainer()
        {
            Reset();
        }

        /// <summary>
        /// Constructor (with name).
        /// </summary>
        public ITYPContainer(String name)
        {
            this.Name = name;
            Reset();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Reset internal data.
        /// </summary>
        public virtual void Reset()
        {
            this.m_Dependencies = new List<String>();
            this.m_Archetypes = new List<Archetype>();
        }

        public virtual void MergeWith(ITYPContainer sourceContainer)
        {
            this.AddArchetypeRange(sourceContainer.Archetypes);
            this.AddDependencyRange(sourceContainer.Dependencies);
        }

        #region Dependency Collection Methods
        /// <summary>
        /// Add IMD Container dependency.
        /// </summary>
        /// <param name="name"></param>
        public void AddDependency(ITYPContainer container)
        {
            this.AddDependency(container.Name);
        }

        /// <summary>
        /// Add named container dependency (e.g. for a props file we aren't serialising).
        /// </summary>
        /// <param name="name"></param>
        public void AddDependency(String name)
        {
            Debug.Assert(name != this.Name,
                "Internal error: cannot add self as a dependency.",
                "IMD Container {0} being added as own dependency.", this.Name);
            if (name == this.Name)
                return;

            Debug.Assert(!String.IsNullOrWhiteSpace(name),
                "Internal error: cannot add a null or whitespace container dependency.",
                "IMD container {0} cannot accept an empty IMD container dependency.",
                this.Name);
            // DHM Lets not warn about dupe dependencies because it will complicate
            //     calling code.
            //Debug.Assert(!m_Dependencies.Contains(container.Name),
            //    "Internal error: dependency list already this IMD Container.",
            //    "IMD container {0} is already marked as a dependency.", container.Name);
            if (m_Dependencies.Contains(name))
                return;
            this.m_Dependencies.Add(name);
        }

        /// <summary>
        /// Add a set of containers as dependencies.
        /// </summary>
        /// <param name="container"></param>
        public void AddDependencyRange(IEnumerable<ITYPContainer> containers)
        {
            foreach (ITYPContainer dependency in containers)
                AddDependency(dependency);
        }

        /// <summary>
        /// Add a set of containers as dependencies.
        /// </summary>
        /// <param name="containers"></param>
        public void AddDependencyRange(IEnumerable<String> containers)
        {
            foreach (String name in containers)
                AddDependency(name);
        }
        #endregion // Dependency Collection Methods

        #region Archetype Collection Methods
        /// <summary>
        /// Add archtype to the container.
        /// </summary>
        /// <param name="archetype"></param>
        public virtual void AddArchetype(Archetype archetype)
        {
#warning Flo: Seriously ? Sort after each add?
            Debug.Assert(!m_Archetypes.Contains(archetype),
                "Internal error: archetype already in ITYP Container.",
                "Archetype {0} is already in the ITYP Container.", archetype.Object.Name);
            if (m_Archetypes.Contains(archetype))
                return;

            this.m_Archetypes.Add(archetype);
            this.m_Archetypes.Sort();
        }

        /// <summary>
        /// Add range of archetypes to the container.
        /// </summary>
        /// <param name="archetypes"></param>
        public virtual void AddArchetypeRange(IEnumerable<Archetype> archetypes)
        {
            foreach (Archetype archetype in archetypes)
                this.AddArchetype(archetype);
        }

        /// <summary>
        /// Determine whether this container stores specified Archetype.
        /// </summary>
        /// <param name="archetype"></param>
        /// <returns></returns>
        public virtual bool ContainsArchetype(Archetype archetype)
        {
            return (this.m_Archetypes.Contains(archetype));
        }

        /// <summary>
        /// Find an ObjectDef; returning its Entity.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Archetype FindArchetype(ObjectDef obj)
        {
            foreach (Archetype archetype in this.m_Archetypes)
            {
                if (archetype.Object.Equals(obj))
                    return (archetype);
            }
            return (null);
        }
        #endregion // Archetype Collection Methods
        #endregion // Controller Methods
    }

} // RSG.SceneXml.MapExport.Framework.Collections namespace
