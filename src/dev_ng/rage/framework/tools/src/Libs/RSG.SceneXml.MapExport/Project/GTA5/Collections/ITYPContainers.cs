﻿using System;
using System.Collections.Generic;

namespace RSG.SceneXml.MapExport.Project.GTA5.Collections
{

    /// <summary>
    /// 
    /// </summary>
    public class ITYPContainer : 
        RSG.SceneXml.MapExport.Framework.Collections.ITYPContainer
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Collection of Composite Entities (e.g. stated anim objects).
        /// Key is the group name.
        /// </summary>
        public Dictionary<String, CompositeEntityType> CompositeEntityTypes
        {
            get;
            protected set;
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public ITYPContainer()
            : base()
        {
            Reset();
        }

        /// <summary>
        /// Constructor (with name).
        /// </summary>
        public ITYPContainer(String name)
            : base(name)
        {
            Reset();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Reset internal data.
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            this.CompositeEntityTypes = new Dictionary<String, CompositeEntityType>();
        }

        /// <summary>
        /// Add Composite Entity Type to the container.
        /// </summary>
        /// <param name="archetype"></param>
        public void AddCompositeEntityType(CompositeEntityType compType)
        {
            this.CompositeEntityTypes.Add(compType.Name, compType);
        }

        /// <summary>
        /// Add Composite Entity Types to the container.
        /// </summary>
        /// <param name="compTypes"></param>
        public void AddCompositeEntityTypeRange(IEnumerable<CompositeEntityType> compTypes)
        {
            foreach (CompositeEntityType compType in compTypes)
                this.AddCompositeEntityType(compType);
        }

        public override void MergeWith(RSG.SceneXml.MapExport.Framework.Collections.ITYPContainer sourceContainer)
        {
            base.MergeWith(sourceContainer);

            ITYPContainer derivedSourceContiainer = (ITYPContainer)sourceContainer;
            this.AddCompositeEntityTypeRange(derivedSourceContiainer.CompositeEntityTypes.Values);
        }
        #endregion // Controller Methods
    }

} // RSG.SceneXml.MapExport.Project.GTA.Collections namespace
