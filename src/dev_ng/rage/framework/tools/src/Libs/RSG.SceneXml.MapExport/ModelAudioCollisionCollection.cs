﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using RSG.Base.Configuration;
using RSG.Base.Logging;

namespace RSG.SceneXml.MapExport
{
    /// <summary>
    /// Collection of Game Name, RAVE Name pairs
    /// </summary>
    public class ModelAudioCollisionCollection
    {
        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<String, String> m_AudioCollisionNames;
        private IBranch m_Branch;

        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public ModelAudioCollisionCollection(IBranch branch, String audioCollisionFilename)
        {
            m_Branch = branch;
            m_AudioCollisionNames = new Dictionary<String, String>();

            if (String.IsNullOrEmpty(audioCollisionFilename))
            {
                audioCollisionFilename = this.m_Branch.Environment.Subst(
                    Path.Combine("$(assets)", "maps", "ModelAudioCollisionList.csv"));
            }

            Reload(audioCollisionFilename);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return the RAVE name for a given game name.
        /// </summary>
        /// <param name="effect"></param>
        /// <returns></returns>
        public String GetRAVEName(String modelGameName)
        {
            String modelGameNameUpper = modelGameName.ToUpper();
            if (m_AudioCollisionNames.ContainsKey(modelGameNameUpper))
                return (m_AudioCollisionNames[modelGameNameUpper]);
            return (String.Empty);
        }

        /// <summary>
        /// Reload model audio collision data.
        /// </summary>
        /// <param name="filename"></param>
        public void Reload(String filename)
        {
            m_AudioCollisionNames.Clear();

            if (!File.Exists(filename))
            {
                Log.Log__Warning("Model Audio Collision file '{0}' does not exist, no Game/RAVE names could be loaded.",
                    filename);
            }
            else
            {
                SortedSet<String> duplicateKeys = new SortedSet<String>();
                using (TextReader tr = new StreamReader(filename))
                {
                    int lineIndex = 0;
                    String line = String.Empty;
                    while (null != (line = tr.ReadLine()))
                    {
                        ++lineIndex;

                        if (String.IsNullOrEmpty(line))
                            continue;

                        // Ignore first line
                        if (!line.StartsWith("Game Name,RAVE Name"))
                        {
                            // Only use lines that have more than one token
                            string[] tokens = line.Split(audioCSVSeparators_, 3, StringSplitOptions.RemoveEmptyEntries);
                            if (tokens.Length >= 2)
                            {
                                string gameName = tokens[0].ToUpper();
                                string raveName = tokens[1].ToUpper();
                                if (!m_AudioCollisionNames.ContainsKey(gameName))
                                {
                                    m_AudioCollisionNames.Add(gameName, raveName);
                                }
                                else
                                {
                                    duplicateKeys.Add(gameName);
                                }
                            }
                            else
                            {
                                Log.Log__Warning("Audio collision mapping file at '{0}' has invalid entry at line {1}.", filename, lineIndex);
                            }
                        }
                    }
                }

                foreach (String duplicateKey in duplicateKeys)
                {
                    Log.Log__Warning("Audio collision mapping file at '{0}' contains duplicate entries for '{1}'.", filename, duplicateKey);
                }
            }
        }
        #endregion // Controller Methods

        private static readonly char[] audioCSVSeparators_ = new char[] { ',' };
    }
}
