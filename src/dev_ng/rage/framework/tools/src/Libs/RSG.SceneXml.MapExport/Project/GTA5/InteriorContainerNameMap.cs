﻿using System;
using System.Collections.Generic;
using RSG.Base.Math;


namespace RSG.SceneXml.MapExport.Project.GTA5
{
    /// <summary>
    /// Class responsible for generating and storing unique interior IMAP and ITYP names for interiors of any flavour
    /// </summary>
    public class InteriorContainerNameMap : Framework.InteriorContainerNameMap
    {
        /// <summary>
        /// Used as a key to match DLC interiors to their Core counterpart
        /// based on both name and position (name is not sufficient to match that, in case of apartments or VGuard tower in the prison vicinity)
        /// </summary>
        public sealed class InteriorKey
        {
            private const float PositionTolerance = 0.1f;

            private readonly string Name;
            private readonly float X;
            private readonly float Y;
            private readonly float Z;

            public InteriorKey(string name, float x, float y, float z)
            {
                Name = name;
                X = x;
                Y = y;
                Z = z;
            }

            public override bool Equals(object obj)
            {
                InteriorKey target = obj as InteriorKey;
                if (target == null)
                    return false;

                bool nameEquals = Name.Equals(target.Name, StringComparison.OrdinalIgnoreCase);
                bool coordX = Math.Abs(X - target.X) < PositionTolerance;
                bool coordY = Math.Abs(Y - target.Y) < PositionTolerance;
                bool coordZ = Math.Abs(Z - target.Z) < PositionTolerance;

                return nameEquals && coordX && coordY && coordZ;
            }

            public override int GetHashCode()
            {
                // keeping .1f significance
                string xString = X.ToString("F1");
                string yString = Y.ToString("F1");
                string zString = Z.ToString("F1");

                return (Name + xString + yString + zString).GetHashCode();
            }
        }

        private Dictionary<TargetObjectDef, string> m_objectNameMap;
        private Dictionary<InteriorKey, string> m_coregameMetadataNames;

        public InteriorContainerNameMap(Dictionary<InteriorKey, string> coregameMetadataNames = null)
        {
            m_objectNameMap = new Dictionary<TargetObjectDef, string>();
            m_coregameMetadataNames = coregameMetadataNames ?? new Dictionary<InteriorKey, string>();
        }

        public override string GetContainerNameFromObject(TargetObjectDef objectDef)
        {
            if (m_objectNameMap.ContainsKey(objectDef))
            {
                return m_objectNameMap[objectDef];
            }
            string containerName = "";

            Vector3f position = objectDef.ObjectTransform.Translation;
            
            if (objectDef.IsMilo()) // in-situ interior
            {
                InteriorKey objectKey = new InteriorKey(objectDef.Name.ToLower(), position.X, position.Y, position.Z);

                if (m_coregameMetadataNames.TryGetValue(objectKey, out containerName))
                {
                    m_objectNameMap.Add(objectDef, containerName);
                    return containerName;
                }

                containerName = string.Format("interior_{0}_{1}", m_objectNameMap.Count, objectDef.Name).ToLower();
            }
            else if (objectDef.IsMiloTri()) // ref interior
            {
                InteriorKey objectKey = new InteriorKey(objectDef.RefName.ToLower(), position.X, position.Y, position.Z);

                if (m_coregameMetadataNames.TryGetValue(objectKey, out containerName))
                {
                    m_objectNameMap.Add(objectDef, containerName);
                    return containerName;
                }

                containerName = string.Format("interior_{0}_{1}", m_objectNameMap.Count, objectDef.RefName).ToLower();
            }

            m_objectNameMap.Add(objectDef, containerName);
            return containerName;
        }
    }
}
