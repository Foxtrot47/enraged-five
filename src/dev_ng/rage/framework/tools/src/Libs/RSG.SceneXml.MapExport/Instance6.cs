﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using SMath = System.Math;
using System.Text;
using RSG.Base.Logging;
using RSG.Base.Math;

namespace RSG.SceneXml.MapExport
{

    /// <summary>
    /// SceneXml Instance with IPL Version 6 serialisation.
    /// </summary>
    public sealed class Instance6 : Instance5
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public int LODLevel
        {
            get;
            private set;
        }

        /// <summary>
        /// Index of self in IPL file.
        /// </summary>
        public int Index
        {
            get;
            internal set;
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="scene"></param>
        public Instance6(Scene scene)
            : base(scene)
        {
        }        
        
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="scene">SceneXml.Scene containing instance</param>
        /// <param name="obj">SceneXml.ObjectDef for instance</param>
        /// <param name="reader"></param>
        public Instance6(Scene scene, TargetObjectDef obj, Util.IDEReaderBasic reader)
            : base(scene, obj, reader)
        {
            this.LODLevel = DetermineLodLevel();
            this.InstType = this.Object.GetInstanceType();
            this.LODDistance = this.GetInstanceLODDistance(reader);
            // LOD Distance sanity checks
            switch (this.InstType)
            {
                case ObjectDef.InstanceType.LOD:
                    if (this.LODDistance > LOD_MAX_LOD_DISTANCE)
                        Log.Log__Warning("LOD Object {0} has a high LOD distance, > {1}.", this, LOD_MAX_LOD_DISTANCE);
                    break;
            }
            DetermineStreamMode(reader); // Since updated the LOD Distance
        }
        #endregion // Constructor(s)

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return (String.Format("Instance6: [{0}]{1}", this.Object.Name, this.Object.Guid));
        }
        #endregion // Object Overrides

        #region Controller Methods
        #region IPL File Export Methods
        /// <summary>
        /// Return IPL5 instance string representation.
        /// </summary>
        /// <param name="blocks"></param>
        /// <returns></returns>
        /// Format of instance IPL line is:
        ///   *inst_v4*, lod_level, #children
        /// 
        /// When we output LOD distance we output the actual LOD distance of 
        /// the object rather than -1 unless its per-instance based.
        ///   
        public override String ToIPLInst(String[] blocks)
        {
            return (ToIPLInst(blocks, false));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="blocks"></param>
        /// <param name="includeScale"></param>
        /// <returns></returns>
        public String ToIPLInst(String[] blocks, bool includeScale)
        {
            Debug.Assert(!this.InstType.Equals(ObjectDef.InstanceType.None), "No instance type determined.  Contact tools.");

            Vector3f pos = this.Object.NodeTransform.Translation;
            Quaternionf rot = new Quaternionf(this.Object.NodeTransform);
            String block = this.Object.GetAttribute(AttrNames.OBJ_BLOCK_ID, AttrDefaults.OBJ_BLOCK_ID);
            int aoMult = this.Object.GetAttribute(AttrNames.OBJ_AMBIENT_OCCLUSION_MULTIPLIER, AttrDefaults.OBJ_AMBIENT_OCCLUSION_MULTIPLIER);
            int children = 0;
            if (this.Object.HasLODChildren())
                children = this.Object.LOD.Children.Length;

            // Milo's use an inverted rotation for some reason.
            if (this.Object.IsMiloTri())
                rot.Invert();

            if (this.Object.HasLODParent())
                Debug.Assert(-1 != this.LODParentIndex, String.Format("Invalid LOD Parent Index for {0}.  Contact tools.", this.Object));

            String ipl_line = String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}",
                this.Object.GetObjectName(),
                Flags.GetInstanceFlags(this.Object),
                pos.X, pos.Y, pos.Z, rot.X, rot.Y, rot.Z, rot.W,
                this.LODParentIndex,
                Flags.GetInstanceExtraFlags(this.Object),  // replaces block index
                this.LODDistance,
                (int)this.LODLevel, children, aoMult);

            if (includeScale)
            {
                ipl_line += String.Format(", {0}, {1}", this.Object.NodeScale.X, 
                    this.Object.NodeScale.Z);
            }

            return (ipl_line);
        }

        /// <summary>
        /// Return IPL6 MILO instance string representation.
        /// </summary>
        /// <param name="blocks"></param>
        /// <returns></returns>
        /// Format of instance IPL line is:
        ///   *inst_v5*, groupid
        ///   
        public String ToIPLMiloInst(String[] blocks)
        {
            int groupid = this.Object.GetAttribute(AttrNames.MILOTRI_GROUPID, AttrDefaults.MILOTRI_GROUPID);
            String ipl_line = ToIPLInst(blocks, false);
            ipl_line += String.Format(", {0}", groupid);

            return (ipl_line);
        }

        public static ObjectDef GetObjectToQueryForValArray(string propertyValArrayName, string attrName, ObjectDef obj)
        {
            if(null==obj.RefObject)
                return obj;
            string[] def = {};
            Object[] overrideMask = obj.GetParameter<Object[]>(propertyValArrayName, def);
            if(null==overrideMask)
                return obj;
            foreach(string val in overrideMask)
            {
                if(val.ToLower()==attrName)
                    return obj.RefObject;
            }
            return obj;
        }

        /// <summary>
        /// 
        /// The instances have 
        /// a) arrays with all original object property and attribute values and 
        /// b) arrays with names of attributes/properties they override.
        /// 
        /// So we go through the instance name array 
        /// 
        /// </summary>
        /// <returns></returns>
        public String ToIPLLightInst()
        {
            // Not used anymore
            return (String.Empty);
        }
        #endregion // IPL File Export Methods
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Return number of ancestor parents for Lod Level.
        /// </summary>
        /// <returns></returns>
        public int DetermineLodLevel()
        {
            int level = 0;
            TargetObjectDef parent = null;
            if (null != this.Object.LOD)
                parent = this.Object.LOD.Parent;
            while (null != parent)
            {
                if (null != parent.LOD)
                    parent = parent.LOD.Parent;
                else
                    parent = null;
                ++level;
            }
            return (level);
        }
        #endregion // Private Methods
    }

} // RSG.SceneXml.MapExport namespace
