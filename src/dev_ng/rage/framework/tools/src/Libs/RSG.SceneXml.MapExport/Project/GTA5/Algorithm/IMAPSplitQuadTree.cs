﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using RSG.Base.Collections;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.SceneXml.MapExport.Algorithm;
using RSG.SceneXml.MapExport.Project.GTA5.Collections;

namespace RSG.SceneXml.MapExport.Project.GTA5.Algorithm
{
    /// <summary>
    /// QuadTree entity cutting algorithm.
    /// </summary>
    public class IMAPSplitQuadTree : 
        IIMAPSplitAlgorithm<IMAPContainer>
    {
        #region IIMAPSplitAlgorithm Methods
        /// <summary>
        /// Split the larger container into a series of smaller containers.
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="container"></param>
        /// <param name="containers"></param>
        /// <param name="warnMissingCarGens"></param>
        /// <param name="streamMode">Stream mode to include for HD objects.</param>
        public void Split(String prefix, IMAPContainer container, out List<IMAPContainer> containers, bool warnMissingCarGens, Entity.StreamModeType streamMode)
        {
            int minStreamFiles = (int)Math.Ceiling((float)container.Entities.Count / (float)targetImapEntityCountUpperLimit_);
            List<IMAPContainer> streamableContainers = new List<IMAPContainer>();
            IMAPContainer lodObjects;
            SplitUtils.RepackageContainer(container, out lodObjects);

            // lodObjects now contains a list of LOD parent and orphan HD objects
            // which we use to package.  This ensures that LOD children stay 
            // together during the QuadTree splitting.

            // Setup and populate our QuadTree.
            QuadTree<EntityNode> qt = new QuadTree<EntityNode>(
                new BoundingBox2f(lodObjects.EntityBoundingBox), this.SplitCallback);
            foreach (Entity e in lodObjects.Entities)
            {
                switch (e.Object.LODLevel)
                {
                    case ObjectDef.LodLevel.ORPHANHD:
                    case ObjectDef.LodLevel.HD:
                    case ObjectDef.LodLevel.LOD:
                    case ObjectDef.LodLevel.SLOD1:
                        qt.Insert(new EntityNode(e), QuadTree<EntityNode>.SplitBehaviour.Split);
                        break;
                    default:
                        Log.Log__Error("Internal error; entity {0} (lodlevel: {1}) should not be in lodObjects.",
                            e.Object.Name, e.Object.LODLevel);
                        break;
                }
            }

            // Merge quad tree nodes of low value with adjacent nodes
            qt.MergeLowValueNodes(this.GetNodeEntityCountRecursive, targetImapEntityCountLowerLimit_, targetImapEntityCountUpperLimit_);

            // Loop through our QuadTree and produce IMAP containers.
            int n = 0;
            qt.ForEach(delegate(QuadTreeNode<EntityNode> node)
            {
                if (0 != node.Contents.Count)
                {
                    Log.Log__Message("Stream entity container: {0}_{1} containing {2} entities.",
                        prefix, n, node.Contents.Count);

                    IMAPContainer streamableContainer = new IMAPContainer(
                        String.Format("{0}_{1}", prefix, n));

                    foreach (EntityNode entityNode in node.Contents)
                    {
                        // Ensure its the HD children that get added, as this
                        // container has LODs in it.
                        switch (entityNode.Entity.Object.LODLevel)
                        {
                            case ObjectDef.LodLevel.SLOD1:
                                foreach (Entity e in entityNode.Entity.Children)
                                {
                                    if (e.Children.Count > 0)
                                    {
                                        foreach (Entity ec in e.Children)
                                            streamableContainer.AddEntity(ec);
                                    }
                                    else if (ObjectDef.LodLevel.HD == e.Object.LODLevel)
                                    {
                                        if (streamMode == e.StreamMode)
                                            streamableContainer.AddEntity(e);
                                    }
                                }
                                break;
                            case ObjectDef.LodLevel.SLOD4:
                            case ObjectDef.LodLevel.SLOD3:
                            case ObjectDef.LodLevel.LOD:
                                foreach (Entity e in entityNode.Entity.Children)
                                {
                                    if (streamMode == e.StreamMode)
                                        streamableContainer.AddEntity(e);
                                }
                                break;
                            case ObjectDef.LodLevel.SLOD2:
                            case ObjectDef.LodLevel.HD:
                                // Bug fix: this leads to duplicate entities.
                                break;
                            case ObjectDef.LodLevel.ORPHANHD:
                                if (streamMode == entityNode.Entity.StreamMode)
                                    streamableContainer.AddEntity(entityNode.Entity);
                                break;
                            default:
                                Debug.Assert(false);
                                break;
                        }
                    }
                    streamableContainers.Add(streamableContainer);

                    ++n;
                }    
            });

            // Once we have the IMAP containers; loop through our CarGens
            // and put them in their containing bucket.
            // GTA5 Bug 217920; also consider the stream long IMAP containers.
            List<CarGen> carGens = new List<CarGen>(container.CarGens);
            int carGensCount = 0;
            foreach (CarGen cg in carGens)
            {
                // Stream containers.
                foreach (IMAPContainer scont in streamableContainers)
                {
                    // Check streaming bounds rather than entity bounds.
                    if (scont.StreamingBoundingBox.Contains(cg.Object.NodeTransform.Translation))
                    {
                        scont.AddCarGen(cg);
                        cg.HasContainer = true;
                        ++carGensCount;
                        break;
                    }
                }
                if (!cg.HasContainer && warnMissingCarGens)
                {
                    Log.Log__WarningCtx(cg.Object.Name, "CarGen {0} at {1} does not have a valid IMAP container.  Is it near any existing instances?  See {0} for more information.",
                        cg.Object.Name, cg.Object.NodeTransform.Translation, invalidCarGenErrorDevstarLink_);
                }
            }
            // Verify entity counts.
            int count = 0;
            foreach (IMAPContainer cont in streamableContainers)
            {
                count += cont.Entities.Count;
            }
            Debug.Assert(count == container.Entities.Count,
                "Entity inconsistency.");

            // Verify CarGens got scooped up.
            Debug.Assert(carGensCount == container.CarGens.Length,
                "CarGen inconsistency.");

            containers = streamableContainers;
        }
        #endregion // IIMAPSplitAlgorithm Methods

        #region Private Methods
        /// <summary>
        /// QuadTree Split Callback Method
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private QuadTree<EntityNode>.Operation SplitCallback(QuadTreeNode<EntityNode> node)
        {
            // Split if we have exceeded our maximum number of entities.
            int count = GetNodeEntityCount(node);

            if (count > targetImapEntityCountUpperLimit_)
            {
                return (QuadTree<EntityNode>.Operation.Split);
            }

            return (QuadTree<EntityNode>.Operation.None);
        }

        internal int GetNodeEntityCount(QuadTreeNode<EntityNode> node)
        {
            int count = 0;

            foreach (EntityNode entityNode in node.Contents)
            {
                if (entityNode.Entity.Object.HasLODChildren())
                    count += entityNode.Entity.Object.LOD.Children.Length;
                else
                    count += 1;
            }

            return count;
        }

        internal int GetNodeEntityCountRecursive(QuadTreeNode<EntityNode> node)
        {
            int count = 0;

            foreach (EntityNode entityNode in node.SubTreeContents)
            {
                if (entityNode.Entity.Object.HasLODChildren())
                    count += entityNode.Entity.Object.LOD.Children.Length;
                else
                    count += 1;
            }

            return count;
        }
        #endregion // Private Methods

        #region Constant 
        // Our aim is 400 ± 120
        static readonly int targetImapEntityCountLowerLimit_ = 280;// We attempt to merge if there are less than this number of entities
        static readonly int targetImapEntityCountUpperLimit_ = 520;// This is our upper limit (before merging)
        static readonly String invalidCarGenErrorDevstarLink_ = "https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#Car_Generator_invalid_IMAP_Container";
        #endregion Constant Data
    }
        
    /// <summary>
    /// 
    /// </summary>
    class EntityNode : IHasBoundingBox2f
    {
        public Entity Entity { get; set; }
        public BoundingBox2f Bound { get { return new BoundingBox2f(this.Entity.WorldBoundingBox); } }

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="o"></param>
        public EntityNode(Entity e)
        {
            this.Entity = e;
        }
        #endregion // Constructor(s)
    }

} // RSG.SceneXml.MapExport.Project.GTA.Algorithm namespace
