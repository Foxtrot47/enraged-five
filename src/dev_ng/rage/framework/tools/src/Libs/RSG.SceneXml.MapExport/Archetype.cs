using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using SMath = System.Math;
using System.Xml;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.Metadata.Util;
using RSG.SceneXml;
using RSG.SceneXml.MapExport.Util;
using RSG.Base.Configuration;

namespace RSG.SceneXml.MapExport
{
    
    /// <summary>
    /// Object Definition
    /// </summary>
    public sealed class Archetype :
        ExtensionContainer,
        IComparable,
        IComparable<Archetype>
    {
        #region Enumerations
        /// <summary>
        /// StreamMode enumeration.
        /// </summary>
        public enum StreamModeType
        {
            /// <summary>
            /// Archetype is streamed (default)
            /// </summary>
            Streamed,
            /// <summary>
            /// Archetype is not-streamed (always loaded, "Static" ITYP)
            /// </summary>
            NotStreamed,
            /// <summary>
            /// Entity stream mode unknown.
            /// </summary>
            Unknown,
        }
        #endregion // Enumerations

        #region Properties
        /// <summary>
        /// Archetype name.
        /// </summary>
        public String Name
        {
            get
            {
                Debug.Assert(null != Archetype.Branch, "Branch not set.  Internal error.");
                if (null == Archetype.Branch)
                    throw (new ArgumentNullException("Archetype.Branch not set.  Internal error."));

                // We only append the archetype name if the archetype is coming from
                // the DLC data; otherwise we prefix with Core Project.
                bool isNewDlcObject = Branch.Project.ForceFlags.AllNewContent || this.Object.IsNewDLCObject();
                IProject project = (Branch.Project.IsDLC && isNewDlcObject)
                    ? Branch.Project                    
                    : Branch.Project.Config.CoreProject;
                String objectName = MapAsset.AppendMapPrefixIfRequired(project, this.Object.GetObjectName());
                return (objectName);
            }
        }

        /// <summary>
        /// Archetype raw name (no prefix).
        /// </summary>
        public String RawName
        {
            get { return (this.Object.GetObjectName()); }
        }

        /// <summary>
        /// Archetype LOD distance (in metres).
        /// </summary>
        public float LODDistance
        {
            get
            {
                return SceneOverrideIntegrator.Instance.GetObjectLODDistance(this.Object);
            }
        }

        /// <summary>
        /// Does this archetype have a HD Texture distance specified that is different to the default?
        /// </summary>
        public bool HasHDTextureDistance
        {
            get
            {
                return (HDTextureDistance != AttrDefaults.OBJ_HD_TEXTURES_DISTANCE);
            }
        }

        /// <summary>
        /// Gets the HD Texture Distance for this Archetype
        /// </summary>
        public float HDTextureDistance
        {
            get
            {
                return (this.Object.GetAttribute(AttrNames.OBJ_HD_TEXTURES_DISTANCE, AttrDefaults.OBJ_HD_TEXTURES_DISTANCE));
            }
        }

        public bool SuppressHDTextures
        {
            get
            {
                return (this.Object.GetAttribute(AttrNames.OBJ_SUPPRESS_HD_TEXTURES, AttrDefaults.OBJ_SUPPRESS_HD_TEXTURES));
            }
        }

        public bool HasCloth
        {
            get
            {
                bool hasCloth = this.Object.IsCloth();
                TargetObjectDef[] allChildObjectDefs = this.Object.GetChildrenRecursive();
                foreach (TargetObjectDef childObjectDef in allChildObjectDefs)
                    hasCloth |= childObjectDef.IsCloth();

                return hasCloth;
            }
        }

        /// <summary>
        /// Archetype special attribute.
        /// </summary>
        public int SpecialAttribute
        {
            get
            {
                return (this.Object.GetAttribute(AttrNames.OBJ_ATTRIBUTE, AttrDefaults.OBJ_ATTRIBUTE));
            }
        }

        /// <summary>
        /// Texture dictionary name or empty string.
        /// </summary>
        public String TextureDictionary
        {
            get
            {
                Debug.Assert(null != Archetype.Branch, "Branch not set.  Internal error.");
                if (null == Archetype.Branch)
                    throw (new ArgumentNullException("", "Archetype.Branch not set.  Internal error."));

                String rawTxd = this.Object.GetAttribute(AttrNames.OBJ_REAL_TXD, AttrDefaults.OBJ_REAL_TXD);
                bool shadowOnly = this.Object.GetAttribute(AttrNames.OBJ_SHADOW_ONLY, AttrDefaults.OBJ_SHADOW_ONLY);

                if (shadowOnly || String.Equals(rawTxd, AttrDefaults.OBJ_REAL_TXD))
                    return string.Empty;

                // B*2099422 Black textures all over the place
                // prefix only if we override the txd
                bool isNewTxd = this.Object.GetAttribute(AttrNames.OBJ_TXD_DLC, AttrDefaults.OBJ_NEW_TXD);
                if (!isNewTxd)
                    return rawTxd;

                // We only append the archetype name if the archetype is coming from
                // the DLC data; otherwise we prefix with Core Project.
                bool isNewDlcObject = Branch.Project.ForceFlags.AllNewContent || this.Object.IsNewDLCObject();
                IProject project = (Branch.Project.IsDLC && isNewDlcObject)
                    ? Branch.Project
                    : Branch.Project.Config.CoreProject;

                return MapAsset.AppendMapPrefixIfRequired(project, rawTxd);
            }
        }

        /// <summary>
        /// Model dictionary name or empty string.
        /// </summary>
        public String ModelDictionary
        {
            get
            {
                String idd = this.Object.GetAttribute(AttrNames.OBJ_MODEL_GROUP, AttrDefaults.OBJ_MODEL_GROUP);
                if (String.Equals(idd, AttrDefaults.OBJ_MODEL_GROUP))
                    return (String.Empty);
                else
                    return (idd);
            }
        }

        /// <summary>
        /// Physics dictionary name or empty string.
        /// </summary>
        public String PhysicsDictionary { get; private set; }

        /// <summary>
        /// Clip (Animation) dictionary name or empty string.
        /// </summary>
        public String ClipDictionary
        {
            get
            {
                Debug.Assert(null != Archetype.Branch, "Branch not set.  Internal error.");
                if (null == Archetype.Branch)
                    throw (new ArgumentNullException("Archetype.Branch not set.  Internal error."));

                String clip = this.Object.GetAttribute(AttrNames.OBJ_ANIM, AttrDefaults.OBJ_ANIM);
                if (String.Equals(clip, AttrDefaults.OBJ_ANIM))
                    return (String.Empty);

                String mapPrefix = Archetype.Branch.Project.AssetPrefix.MapPrefix;
                return (String.Format("{0}{1}", mapPrefix, clip));
            }
        }

        /// <summary>
        /// Determines which ITYP file the archetype will end up in.
        /// </summary>
        public StreamModeType StreamMode
        {
            get;
            private set;
        }

        /// <summary>
        /// Object within SceneXml Scene.
        /// </summary>
        public TargetObjectDef Object
        {
            get;
            private set;
        }

        public ITarget Target
        {
            get { return Object.Target; }
        }
        #endregion // Properties

        #region Static Properties
        /// <summary>
        /// Branch for DLC name determination.  TargetObjectDef.Target isn't always set
        /// and need to get the project for its prefix.
        /// </summary>
#warning DHM FIX ME: hack to get DLC working on GTA5.
        public static IBranch Branch
        {
            get;
            set;
        }
        #endregion // Static Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="scene">SceneXml.Scene containing Archetype</param>
        public Archetype(TargetObjectDef obj)
        {
            this.Object = obj;

            // Derive data from the obj passed in
            PhysicsDictionary = Archetype.DerivePhysicsDictionaryName(this.Object);

            DetermineStreamMode();
        }
        #endregion // Constructor(s)

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            if (obj is Archetype)
            {
                Archetype def = (obj as Archetype);
                return (this.Object.Equals(def.Object));
            }

            return (false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return (base.GetHashCode());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return (String.Format("Archetype: [{0}]{1}", this.Object.Name, this.Object.Guid));
        }
        #endregion // Object Overrides

        #region IComparable and IComparable<Archetype> Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        int IComparable.CompareTo(Object obj)
        {
            if (obj is Archetype)
                return (CompareTo(obj as Archetype));
            throw new ArgumentException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(Archetype obj)
        {
            if (this.Object.IsMilo() && !obj.Object.IsMilo())
                return (1);
            else if (obj.Object.IsMilo() && !this.Object.IsMilo())
                return (-1);
            return (this.Object.Name.CompareTo(obj.Object.Name));
        }
        #endregion // IComparable and IComparable<Archetype> Methods

        #region Private methods
        private static string DerivePhysicsDictionaryName(TargetObjectDef objectDef)
        {
            if (!objectDef.HasCollision())
            {
                return "";// no collision, no physics dictionary
            }

            if (objectDef.MyScene.SceneType == SceneType.EnvironmentContainer)
            {
                if (objectDef.IsDynObject() && !objectDef.IsFragment())
                {
                    String collisionGroup = objectDef.GetAttribute(AttrNames.OBJ_COLLISION_GROUP, AttrDefaults.OBJ_COLLISION_GROUP);
                    if (!String.IsNullOrWhiteSpace(collisionGroup) && 0 != String.Compare(collisionGroup, AttrDefaults.OBJ_COLLISION_GROUP, true))
                    {
                        return collisionGroup;// objects with a collision group are always in <collision_group_name>.ibd.zip
                    }
                    else
                    {
                        string contentName = System.IO.Path.GetFileNameWithoutExtension(objectDef.MyScene.Filename);
                        return contentName;// objects with no collision group are always in <content_name>.ibd.zip
                    }
                }
            }
            else if (objectDef.MyScene.SceneType == SceneType.EnvironmentInterior)
            {
                if ((objectDef.IsDynObject() && !objectDef.IsFragment()) || objectDef.IsPartOfAnEntitySet())
                {
                    TargetObjectDef parentObjectDef = objectDef.Parent;
                    while (parentObjectDef != null)
                    {
                        if (parentObjectDef.IsMilo())
                        {
                            return parentObjectDef.Name;// dymanic milo objects are always in <milo_name>.ibd.zip
                        }
                        else
                        {
                            parentObjectDef = parentObjectDef.Parent;
                        }
                    }
                }
            }
            else if (objectDef.MyScene.SceneType == SceneType.EnvironmentProps)
            {
                if (!objectDef.IsFragment())
                {
                    String collisionGroup = objectDef.GetAttribute(AttrNames.OBJ_COLLISION_GROUP, AttrDefaults.OBJ_COLLISION_GROUP);
                    if (!String.IsNullOrWhiteSpace(collisionGroup) && 0 != String.Compare(collisionGroup, AttrDefaults.OBJ_COLLISION_GROUP, true))
                    {
                        return collisionGroup;// prop objects with a collision group are always in <collision_group_name>.ibd.zip
                    }
                    else
                    {
                        string contentName = System.IO.Path.GetFileNameWithoutExtension(objectDef.MyScene.Filename);
                        return contentName;// prop objects with no collision group are always in <content_name>.ibd.zip
                    }
                }
            }

            return "";
        }

        /// <summary>
        /// Determine the archetype streaming mode (based on LOD hierarchy and LOD distance).
        /// </summary>
        private void DetermineStreamMode()
        {
            switch (this.Object.LODLevel)
            {
                case ObjectDef.LodLevel.SLOD4:
                case ObjectDef.LodLevel.SLOD3:
                case ObjectDef.LodLevel.SLOD2:
                case ObjectDef.LodLevel.SLOD1:
                case ObjectDef.LodLevel.LOD:
                    this.StreamMode = StreamModeType.NotStreamed;
                    break;
                case ObjectDef.LodLevel.HD:
                case ObjectDef.LodLevel.ORPHANHD:
                    if (this.Object.IsMiloTri())
                        this.StreamMode = StreamModeType.NotStreamed;
                    else
                        this.StreamMode = StreamModeType.Streamed;
                    break;
                default:
                    this.StreamMode = StreamModeType.Unknown;
                    break;
            }
        }
        #endregion
    }

} // RSG.SceneXml.MapExport
