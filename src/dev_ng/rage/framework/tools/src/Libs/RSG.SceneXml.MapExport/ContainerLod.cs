﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.SceneXml.MapExport
{

    /// <summary>
    /// 
    /// </summary>
    public class ContainerLod : 
        IComparable,
        IComparable<ContainerLod>
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String Name
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public uint Index
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="index"></param>
        public ContainerLod(String name, uint index)
        {
            this.Name = name;
            this.Index = index;
        }
        #endregion // Constructor(s)

        #region Object Overrides
        /// <summary>
        /// EQuality between two ContainerLod objects.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            return (0 == this.CompareTo(obj));
        }
        #endregion // Object Overrides

        #region IComparable Interface Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(Object obj)
        {
            if (!(obj is ContainerLod))
                return (-1);
            ContainerLod o = (obj as ContainerLod);
            return (this.CompareTo(o));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(ContainerLod obj)
        {
            if (this.Index == obj.Index &&
                (0 == String.Compare(this.Name, obj.Name)))
                return (0);
            else
                return (String.Compare(this.Name, obj.Name));
        }
        #endregion // IComparable Interface Methods
    }

} // RSG.SceneXml.MapExport namespace
