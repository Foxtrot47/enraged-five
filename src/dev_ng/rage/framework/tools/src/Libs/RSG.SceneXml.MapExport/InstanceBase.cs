//
// File: Instance4.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Instance4 class
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using RSG.Base.Math;
using RSG.SceneXml;
using RSG.ManagedRage;

namespace RSG.SceneXml.MapExport
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class InstanceBase
    {
        #region Enumerations
        /// <summary>
        /// Instance4 StreamMode enumeration.
        /// </summary>
        public enum StreamModeType
        {
            /// <summary>
            /// Instance4 is streamed (default)
            /// </summary>
            Streamed,
            /// <summary>
            /// Instance4 has large LOD, strbig.
            /// </summary>
            StreamedBig,
            /// <summary>
            /// Instance4 is not-streamed (always loaded, Static IPL)
            /// </summary>
            NotStreamed,
        }
        #endregion // Enumerations

        #region Properties and Associated Member Data
        /// <summary>
        /// Instance LOD distance.
        /// </summary>
        public float LODDistance
        {
            get { return m_fLODDistance; }
            internal set { m_fLODDistance = value; }
        }
        private float m_fLODDistance;
        
        /// <summary>
        /// Instance4 stream mode.  Ultimately determines which IPL
        /// file the instance will end up in (streamed, non-streamed or strbig).
        /// </summary>
        public StreamModeType StreamMode
        {
            get { return m_eStreamMode; }
            protected set { m_eStreamMode = value; }
        }
        private StreamModeType m_eStreamMode;

        /// <summary>
        /// Instance LOD parent index as required in IPL file.
        /// </summary>
        public int LODParentIndex
        {
            get { return m_idxLODParentIndex; }
            set { m_idxLODParentIndex = value; }
        }
        private int m_idxLODParentIndex;

        /// <summary>
        /// Instance object definition within SceneXml Scene.
        /// </summary>
        public TargetObjectDef Object
        {
            get { return m_Object; }
            protected set { m_Object = value; }
        }
        private TargetObjectDef m_Object;
        #endregion // Properties and Associated Member Data

        #region Member Data
        /// <summary>
        /// Reference to our SceneXml Scene object.
        /// </summary>
        protected Scene m_Scene;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor, no associated ObjectDef (e.g. used for anonymous block).
        /// </summary>
        /// <param name="scene">SceneXml.Scene containing instance</param>
        public InstanceBase(Scene scene)
        {
            this.m_eStreamMode = StreamModeType.NotStreamed;
            this.m_Scene = scene;
            this.m_Object = null;
            this.m_idxLODParentIndex = -1;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="scene">SceneXml.Scene containing instance</param>
        /// <param name="obj">SceneXml.ObjectDef for instance</param>
        /// <param name="reader"></param>
        public InstanceBase(Scene scene, TargetObjectDef obj, Util.IDEReaderBasic reader)
        {
            this.m_eStreamMode = StreamModeType.Streamed;
            this.m_Scene = scene;
            this.m_Object = obj;
            this.m_idxLODParentIndex = -1;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return IPL String representation for a "Gta Object" ObjectDef.
        /// Just a regular object instance.
        /// </summary>
        public abstract String ToIPLInst(String[] blocks);

        /// <summary>
        /// Return IPL String representation for a "Gta Block" ObjectDef.
        /// </summary>
        public abstract String ToIPLBlock();

        /// <summary>
        /// Return IPL String representation for a "Gta CarGen" ObjectDef.
        /// </summary>
        public abstract String ToIPLCarGen();

        /// <summary>
        /// Return IPL String representation for a 2dfx ObjectDef.
        /// </summary>
        public abstract String ToIPL2dfx();

        /// <summary>
        /// Return IPL String representation for a MiloPlus ObjectDef.
        /// </summary>
        public abstract String ToIPLMiloPlus(String[] blocks);

        /// <summary>
        /// Return IPL String representation for a "VehicleNode" ObjectDef.
        /// </summary>
        public abstract String ToIPLVehicleNode();

        /// <summary>
        /// Return IPL String representation for a "VehicleLink" ObjectDef.
        /// </summary>
        public abstract String ToIPLVehicleLink(List<Guid> nodes);

        /// <summary>
        /// Return IPL String representation for a "PatrolNode" ObjectDef.
        /// </summary>
        public abstract String ToIPLPatrolNode();

        /// <summary>
        /// Return IPL String representation for a "PatrolLink" ObjectDef.
        /// </summary>
        public abstract String ToIPLPatrolLink(List<Guid> nodes);

        /// <summary>
        /// Return IPL String representation for a "Gta TimeCycle" ObjectDef.
        /// </summary>
        public abstract String ToIPLTimeCycle();
        #endregion // Controller Methods
    }

} // RSG.SceneXml.MapExport namespace

// Instance4.cs
