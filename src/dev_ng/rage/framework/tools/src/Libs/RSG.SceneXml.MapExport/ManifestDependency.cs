﻿using System;   
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.SceneXml.MapExport
{
    public class ManifestDependency
    {
        #region Properties
        public String IMAPFile
        {
            get;
            private set;
        }

        public String[] ITYPDependencies
        {
            get;
            set;
        }

        public bool IsInterior
        {
            get;
            private set;
        }
        #endregion

        #region Constructors
        public ManifestDependency(String imapFile, String[] itypDependencies, bool isInterior)
        {
            IMAPFile = imapFile;
            ITYPDependencies = itypDependencies;
            IsInterior = isInterior;
        }

        #endregion
    }
}
