﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.Metadata.Util;
using RSG.SceneXml.MapExport.AssetCombine;
using RSG.SceneXml.MapExport.Project.GTA5_NG.Collections;
using RSG.SceneXml.MapExport.Util;

namespace RSG.SceneXml.MapExport.Project.GTA5_NG
{
    /// <summary>
    /// GTA5 ITYP Map Data Serialiser.
    /// </summary>
    /// Since we share a lot of game-level code now this should provide new 
    /// projects with a good starting point.  Once they need to customise data
    /// a copy or subclass of this should be made to add or change data.
    ///
    public sealed class SceneSerialiserITYP : Framework.SceneSerialiserITYP<ITYPContainer>
    {
        #region Constants
        /// <summary>
        /// Serialiser version.
        /// </summary>
        protected const int ITYP_VERSION = 2;

        private const String URL_EMPTY_ITYP = "https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#Empty_ITYP_File";

        /// <summary>
        /// MLo LIMBO room.
        /// </summary>
        private const String LIMBO = "limbo";

        /// <summary>
        /// Maximum number of MILO rooms.
        /// </summary>
        private const int MAX_MILO_ROOMS = 31;

        /// <summary>
        /// Maximum number of MILO portals.
        /// </summary>
        private const int MAX_MILO_PORTALS = 255;
        #endregion // Constants

        #region Member Data

        /// <summary>
        /// Dictionary of game name -> RAVE name pairs
        /// </summary>
        public ModelAudioCollisionCollection ModelAudioCollision;
        private EffectCollection effectCollection;

        #endregion

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="scenes"></param>
        /// <param name="splitITYP"></param>
        /// <param name="af"></param>
        /// <param name="interiorNameMap"></param>
        /// <param name="audioCollisionFilename"></param>
        /// <param name="coreArchetypeObjects"></param>
        public SceneSerialiserITYP(IBranch branch, Scene[] scenes, bool splitITYP, 
            AssetFile af, InteriorContainerNameMap interiorNameMap, String audioCollisionFilename,
            IDictionary<String, IEnumerable<ArchetypeLite>> coreArchetypeObjects)
            : base(branch, scenes, af, interiorNameMap, audioCollisionFilename, coreArchetypeObjects)
        {
            splitITYP_ = splitITYP;
            effectCollection = new EffectCollection(branch);

            Reset();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        public override bool Process(String pathname)
        {
            Log.Log__Message("Starting ITYP processing:");
            Log.Log__Message("\tScenes ({0}):", this.Scenes.Length);
            foreach (Scene scene in this.Scenes)
                Log.Log__Message("\t\t{0}", scene.Filename);

            Reset();
            ProcessScenes(this.Scenes);

            // Initialise container names
            this.Container.Name = Path.GetFileNameWithoutExtension(pathname);
            if (splitITYP_)
            {
                this.StreamedContainer.Name = this.Container.Name + "_strm";

                foreach (KeyValuePair<string, ITYPContainer> interiorITYPPair in this.InteriorContainerMap)
                    interiorITYPPair.Value.Name = String.Format("{0}_{1}", this.Container.Name, interiorITYPPair.Key);
            }

            return true;
        }

        public override bool Serialise(String pathname)
        {
            Log.Log__Message("Starting ITYP serialisation:");

            // Raise an error if the ITYP file is empty.
            if (((null != this.Container) && (0 == this.Container.ArchetypeCount)) && 
                ((null != this.StreamedContainer) && (0 == this.StreamedContainer.ArchetypeCount)))
            {
                Log.Log__Warning("Container {0} has an empty ITYP file.  This should be disabled in the content tree and game metadata files as it causes seeks.  See {1} for more information.",
                    this.Container.Name, URL_EMPTY_ITYP);
            }

            // Output main ITYP container
            string outputDirectory = Path.GetDirectoryName(pathname);
            string containerPathname = Path.Combine(outputDirectory, this.Container.Name + ".ityp");
            string permanentContainerFilename = Path.Combine(outputDirectory, 
                String.Format("{0}_permanent.ityp", this.Container.Name));
            WriteContainerToDisc(this.Container, containerPathname);
            WriteContainerToDisc(this.PermanentContainer, permanentContainerFilename);

            if (splitITYP_)
            {
                // Output ITYP container for streamed archetypes
                string streamedContainerPathname = Path.Combine(outputDirectory, this.StreamedContainer.Name + ".ityp");
                WriteContainerToDisc(this.StreamedContainer, streamedContainerPathname);

                foreach (KeyValuePair<string, ITYPContainer> interiorITYPPair in this.InteriorContainerMap)
                {
                    string interiorITYPPathname = Path.Combine(outputDirectory, interiorITYPPair.Value.Name + ".ityp");
                    WriteContainerToDisc(interiorITYPPair.Value, interiorITYPPathname);
                }
            }

            return true;
        }

        /// <summary>
        /// Write manifest data to XDocument.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        public override bool WriteManifestData(XDocument xmlDoc)
        {
            // ITYP to ITYP dependencies
            XElement itypDep2 = AsManifestITYPDependency2(this.Container);
            if (null != itypDep2)
                xmlDoc.Root.Add(itypDep2);

            if (splitITYP_)
            {
                foreach (KeyValuePair<String, ITYPContainer> interiorContainerPair in InteriorContainerMap)
                {
                    itypDep2 = AsManifestITYPDependency2(interiorContainerPair.Value);
                    if (null != itypDep2)
                        xmlDoc.Root.Add(itypDep2);
                }
            }

            return (true);
        }

        public bool WriteManifestData(XDocument xmlDoc, IEnumerable<string> forceDependency)
        {
            if (forceDependency.Any())
            {
                // ITYP to ITYP dependencies
                XElement itypDep2 = AsManifestITYPDependency2(this.Container, forceDependency);
                if (null != itypDep2)
                    xmlDoc.Root.Add(itypDep2);

                if (splitITYP_)
                {
                    foreach (KeyValuePair<String, ITYPContainer> interiorContainerPair in InteriorContainerMap)
                    {
                        itypDep2 = AsManifestITYPDependency2(interiorContainerPair.Value);
                        if (null != itypDep2)
                            xmlDoc.Root.Add(itypDep2);
                    }
                }

                return true;
            }

            return WriteManifestData(xmlDoc);
        }

        public void MergeWith(SceneSerialiserITYP sourceSerialiser)
        {
            if (this.Container != null && sourceSerialiser.Container != null)
                this.Container.MergeWith(sourceSerialiser.Container);

            if (this.StreamedContainer != null && sourceSerialiser.StreamedContainer != null)
                this.StreamedContainer.MergeWith(sourceSerialiser.StreamedContainer);
        }

        public static void WriteContainerToDisc(ITYPContainer container, String pathname, Scene[] scenes, 
            IBranch branch, AssetFile assetFile, ModelAudioCollisionCollection modelAudioCollisionCollection,
            IDictionary<String, IEnumerable<ArchetypeLite>> coreArchetypeObjects)
        {
            if (container.ArchetypeCount == 0)
                return;

            XDocument document = new XDocument(new XDeclaration("1.0", "utf-8", null));
            {
                StringBuilder description = new StringBuilder();
                description.Append("ITYP file generated from SceneXml files:\n");
                foreach (Scene scene in scenes)
                    description.AppendFormat("\t\t{0}{1}", scene.Filename, Environment.NewLine);
                XComment xmlDescription = new XComment(description.ToString());
                document.Add(xmlDescription);

                XElement root = AsCMapTypes("CMapTypes", container, branch, assetFile, modelAudioCollisionCollection,
                    coreArchetypeObjects);
                document.Add(root);
                document.Save(pathname);
            }
        }
        #endregion // Controller Methods

        private void WriteContainerToDisc(ITYPContainer container, String pathname)
        {
            WriteContainerToDisc(container, pathname, this.Scenes, this.Branch, this.AssetFile, this.ModelAudioCollision,
                this.CoreArchetyopeObjects);
        }

        /// <summary>
        /// Clear all internal state.
        /// </summary>
        protected override void Reset()
        {
            this.Container = new ITYPContainer();
            this.PermanentContainer = new ITYPContainer();
            if(this.ModelAudioCollision == null)
                this.ModelAudioCollision = new ModelAudioCollisionCollection(this.Branch, this.AudioCollisionFilename);

            if (splitITYP_)
            {
                this.StreamedContainer = new ITYPContainer();
                this.InteriorContainerMap = new Dictionary<string, ITYPContainer>();
            }
        }

        protected override ITYPContainer CreateITYPContainer()
        {
            return new ITYPContainer();
        }

        #region Protected Object-Serialisation Methods
        /// <summary>
        /// Serialise a CMapData object to an XmlDocument.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="container"></param>
        /// <returns></returns>
        protected static XElement AsCMapTypes(String name, ITYPContainer container,
            IBranch branch, AssetFile assetFile, ModelAudioCollisionCollection modelAudioCollisionCollection,
            IDictionary<String, IEnumerable<ArchetypeLite>> coreArchetypeObjects)
        {
            XElement root = AsFwMapTypes(name, container);
            Xml.CreateElementWithValueAttribute(root, "serialiserVersion", ITYP_VERSION);

            EffectCollection effectCollection = new EffectCollection(branch);

            XElement archetypesNode = root.Elements().First(element => element.Name.LocalName == "archetypes");
            foreach (Archetype def in container.Archetypes)
            {
                XElement archetypeElement = null;
                if (def.Object.IsTimeObject())
                    archetypeElement = AsCTimeArchetypeDef(def, assetFile, modelAudioCollisionCollection, effectCollection,
                        coreArchetypeObjects);
                else if (def.Object.IsMilo())
                    archetypeElement = AsCMloArchetypeDef(def, container, branch, coreArchetypeObjects);
                else
                    archetypeElement = AsCBaseArchetypeDef(def, assetFile, modelAudioCollisionCollection, effectCollection,
                        coreArchetypeObjects);

                if (null != archetypeElement)
                    archetypesNode.Add(archetypeElement);
            }
            XElement xmlTxdRelationships = new XElement("txdRelationships");
            root.Add(xmlTxdRelationships);
            XElement xmlCompositeEntityTypes = new XElement("compositeEntityTypes");
            foreach (KeyValuePair<String, CompositeEntityType> compositeType in container.CompositeEntityTypes)
            {
                XElement xmlItem = AsCCompositeEntityType(compositeType.Value);
                xmlCompositeEntityTypes.Add(xmlItem);
            }
            root.Add(xmlCompositeEntityTypes);

            return (root);
        }

        #region Archetype (Definition) Serialisation Methods
        /// <summary>
        /// Serialise a CBaseArchetypeDef object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="archetype"></param>
        /// <returns></returns>
        protected static XElement AsCBaseArchetypeDef(Archetype archetype, AssetFile assetFile, 
            ModelAudioCollisionCollection modelAudioCollisionCollection, EffectCollection effectCollection,
            IDictionary<String, IEnumerable<ArchetypeLite>> coreArchetypeObjects)
        {
            // Find our core archetype as we may need data from it; we only need data from it when
            // we're in DLC but its a core game archetype (not flagged as new).
            ArchetypeLite coreArchetype = null;
            bool isNewDlcObject = Archetype.Branch.Project.ForceFlags.AllNewContent || archetype.Object.IsNewDLCObject();
            if (Archetype.Branch.Project.IsDLC && !isNewDlcObject)
            {
                coreArchetype = coreArchetypeObjects.Values.SelectMany(l => l.Where(a => a.Name.Equals(archetype.Name, StringComparison.OrdinalIgnoreCase))).FirstOrDefault();
            }

            XElement xmlItem = AsFwArchetypeDef(archetype, assetFile, coreArchetype);
            xmlItem.SetAttributeValue("type", "CBaseArchetypeDef");

            fwArchetypeDef_eAssetType assetType = archetype.GetAssetType(assetFile, coreArchetype);
            Xml.CreateElementWithText(xmlItem, "assetType", assetType.ToString());

            CBaseArchetype_Flags flags = ArchetypeFlags.Get(archetype);
            XElement xmlFlags = flags.AsXml();
            xmlItem.Add(xmlFlags);

            // Extensions
            XElement xmlExtensions = new XElement("extensions");
            xmlItem.Add(xmlExtensions);
            foreach (Extension extension in archetype.Extensions)
            {
                XElement xmlExtension = AsCExtensionDef(extension, effectCollection);
                if (null != xmlExtension)
                    xmlExtensions.Add(xmlExtension);
            }

            String modelAudioCollisionRAVEName = modelAudioCollisionCollection.GetRAVEName(archetype.Name);
            if (modelAudioCollisionRAVEName != String.Empty)
            {
                XElement xmlExtension = CExtensionDefAudioCollisionSettings(archetype, modelAudioCollisionRAVEName);
                if (null != xmlExtension)
                    xmlExtensions.Add(xmlExtension);
            }

            return (xmlItem);
        }

        /// <summary>
        /// Serialise a CTimeArchetypeDef object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="parent"></param>
        /// <param name="archetype"></param>
        /// <param name="blocks"></param>
        /// <returns></returns>
        protected static XElement AsCTimeArchetypeDef(Archetype archetype, 
            AssetFile assetFile, ModelAudioCollisionCollection modelAudioCollisionCollection, EffectCollection effectCollection,
            IDictionary<String, IEnumerable<ArchetypeLite>> coreArchetypeObjects)
        {
            Debug.Assert(archetype.Object.IsTimeObject(),
                "Internal error: non-Time Object being serialised as one!");
            uint timeFlags = Flags.GetTimeFlags(archetype.Object);

            XElement xmlItem = AsCBaseArchetypeDef(archetype, assetFile, modelAudioCollisionCollection, effectCollection,
                coreArchetypeObjects);
            xmlItem.SetAttributeValue("type", "CTimeArchetypeDef");

            Xml.CreateElementWithValueAttribute(xmlItem, "timeFlags", timeFlags);

            return (xmlItem);
        }

        /// <summary>
        /// Serialise a CMloArchetypeDef object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="archetype"></param>
        /// <param name="container"></param>
        /// <returns></returns>
        protected static XElement AsCMloArchetypeDef(Archetype archetype, ITYPContainer container, IBranch branch,
            IDictionary<String, IEnumerable<ArchetypeLite>> coreArchetypeObjects)
        {
            String[] room_list;
            List<Archetype> entries;
            Dictionary<TargetObjectDef, MiloRoom> rooms;
            Dictionary<TargetObjectDef, MiloPortal> portals;
            TargetObjectDef limbo;
            Scene scene = archetype.Object.MyScene;
            Dictionary<string, List<TargetObjectDef>> entrySets;
            MiloPreParseRooms(archetype, out rooms, out room_list, out limbo, out entries);
            MiloPreParsePortals(archetype, limbo, ref rooms, out portals);
            MiloPortParseLimbo(archetype, rooms[limbo], portals);
            // 
            MiloPortParseEntrySets(archetype, out entrySets, ref entries);

            // Some sanity checking.
            // GTA5 Bug #1994.
            if (rooms.Count > MAX_MILO_ROOMS)
            {
                Log.Log__Error("Interior {0}: maximum number of rooms exceeded {1}.  Reduce the number of rooms in this interior.",
                    System.IO.Path.GetFileNameWithoutExtension(scene.Filename), rooms.Count);
            }
            if (portals.Count > MAX_MILO_PORTALS)
            {
                Log.Log__Error("Interior {0}: maximum number of portals exceeded {1}.  Reduce the number of portals in this interior.",
                   System.IO.Path.GetFileNameWithoutExtension(scene.Filename), portals.Count);
            }

            UInt32 flags = Flags.GetMiloFlags(archetype.Object);
            float detailDist = archetype.Object.GetAttribute(AttrNames.MILO_DETAIL, AttrDefaults.MILO_DETAIL);

            XElement xmlItem = new XElement("Item");
            xmlItem.Add(new XAttribute("type", "CMloArchetypeDef"));
            Xml.CreateElementWithText(xmlItem, "name", archetype.Name);
            
            // TODO Flo: assetname for milos, as they are not CBaseArchetype
            Xml.CreateElementWithText(xmlItem, "assetName", archetype.Name);
            // TODO Flo: All milos are assetless by default
            Xml.CreateElementWithText(xmlItem, "assetType", fwArchetypeDef_eAssetType.ASSET_TYPE_ASSETLESS.ToString());

            Xml.CreateElementWithValueAttribute(xmlItem, "mloFlags", flags);
            Xml.CreateElementWithValueAttribute(xmlItem, "lodDist", detailDist);
            // JWR - although this is just the object name for now, it may be more complex in future
            Xml.CreateElementWithText(xmlItem, "physicsDictionary", archetype.Object.GetObjectName());

            // B*1304011 - we need to remove objects from rooms after we've processed all entity sets, not while we process them
            Dictionary<MiloRoom, List<TargetObjectDef>> roomObjectRemovalMap = new Dictionary<MiloRoom, List<TargetObjectDef>>();
            Dictionary<MiloPortal, List<TargetObjectDef>> portalObjectRemovalMap = new Dictionary<MiloPortal, List<TargetObjectDef>>();

            // GunnarD: has to happen first as it needs to remove other references from room and portal lists. Added to xml further below.
            XElement entrySetsNode = new XElement("entitySets");
            foreach (KeyValuePair<String, List<TargetObjectDef>> entrySet in entrySets)
            {
                XElement entrySetNode = Xml.CreateElementWithAttribute(entrySetsNode, "Item", "type", "CMloEntitySet");
                Xml.CreateElementWithText(entrySetNode, "name", entrySet.Key);
                XElement entitySetEntriesNode = new XElement("entities");
                List<int> locationFlagArray = new List<int>();
                foreach (TargetObjectDef obj in entrySet.Value)
                {
                    int locationFlags = 0; // Default to room
                    int counter = 0;
                    foreach (MiloRoom room in rooms.Values)
                    {
                        if (room.Children.Contains(obj))
                        {
                            if (!roomObjectRemovalMap.ContainsKey(room))
                                roomObjectRemovalMap.Add(room, new List<TargetObjectDef>());
                            roomObjectRemovalMap[room].Add(obj);
                            
                            locationFlags = counter;
                            break;
                        }
                        counter++;
                    }
                    counter = 0;
                    foreach (MiloPortal portal in portals.Values)
                    {
                        // B* 1044004 - we also want to support objects that are attached to portals and part of an interior group/entity set
                        Guid[] attachedObjectGuids = portal.Portal.GetParameter(
                            ParamNames.MLOPORTAL_ATTACHED_OBJECTS,
                            ParamDefaults.MLOPORTAL_ATTACHED_OBJECTS).OfType<Guid>().ToArray();
                        TargetObjectDef[] attachedObjects = attachedObjectGuids.Select(attachedObjectGuid => obj.MyScene.FindObject(attachedObjectGuid))
                                                                         .ToArray();

                        if (portal.Children.Contains(obj))
                        {
                            if (!portalObjectRemovalMap.ContainsKey(portal))
                                portalObjectRemovalMap.Add(portal, new List<TargetObjectDef>());
                            portalObjectRemovalMap[portal].Add(obj);

                            locationFlags = counter;
                            locationFlags |= 1 << 31;
                            break;
                        }
                        else if (attachedObjects.Contains(obj))
                        {
                            locationFlags = counter;
                            locationFlags |= 1 << 31;
                            break;
                        }
                        counter++;
                    }

                    if (locationFlags == 0)
                    {
                        Log.Log__ErrorCtx(obj.Name, "The interior group {0} has object {1} listed that is neither in a room nor attached to a portal!", entrySet.Key, obj.Name);
                    }

                    locationFlagArray.Add(locationFlags);
                    entitySetEntriesNode.Add(
                        SceneSerialiserIMAP._AsCEntityDef(
                            branch,
                            new Entity(obj),
                            new Entity(archetype.Object))
                            );

                    if (obj.IsRefObject() && !String.IsNullOrEmpty(obj.RefFile))
                    {
                        String ityp_name = Path.GetFileNameWithoutExtension(obj.RefFile);
                        if (Entity.Branch.Project.IsDLC)
                        {
                            if(obj.RefObject.IsNewDLCObject())
                                ityp_name = MapAsset.AppendMapPrefixIfRequired(Entity.Branch.Project, ityp_name);
                        }
                        // JWR - We should be adding dependencies during processing, not serialisation
                        container.AddDependency(ityp_name);
                    }
                }
                String locationString = String.Join("\n", locationFlagArray);
                locationString = "\n" + locationString + "\n";
                XElement locationNode = Xml.CreateElementWithText(entrySetNode, "locations", locationString);
                XAttribute xmlAttrContent = new XAttribute("content", "int_array");
                locationNode.Add(xmlAttrContent);
                entrySetNode.Add(locationNode);

                entrySetNode.Add(entitySetEntriesNode);
            }

            // B*1304011 - finally we remove the entity set objects
            foreach (KeyValuePair<MiloRoom, List<TargetObjectDef>> roomObjectRemovalPair in roomObjectRemovalMap)
            {
                foreach (TargetObjectDef obj in roomObjectRemovalPair.Value)
                    roomObjectRemovalPair.Key.Children.Remove(obj);
            }

            foreach (KeyValuePair<MiloPortal, List<TargetObjectDef>> portalObjectRemovalPair in portalObjectRemovalMap)
            {
                foreach (TargetObjectDef obj in portalObjectRemovalPair.Value)
                    portalObjectRemovalPair.Key.Children.Remove(obj);
            }

            XElement xmlObjects = new XElement("entities");
            int entityIndex = 0;
            foreach (Archetype entryDef in entries)
            {
                if (!entryDef.Object.IsPartOfAnEntitySet())
                {
                    XElement xmlObject = SceneSerialiserIMAP._AsCEntityDef(
                        branch,
                        new Entity(entryDef.Object),
                        new Entity(archetype.Object),
                        entityIndex++);
                    xmlObjects.Add(xmlObject);
                }

                if (entryDef.Object.IsRefObject() && !String.IsNullOrEmpty(entryDef.Object.RefFile))
                {
                    String ityp_name = Path.GetFileNameWithoutExtension(entryDef.Object.RefFile);
                    if (Entity.Branch.Project.IsDLC)
                    {
                        //TargetObjectDef refObject = entryDef.Object.RefObject;
                        //// testing if we resolved that object against the RsRef system
                        //if(refObject != null && refObject.IsNewDLCObject())
                        if (entryDef.Object.RefObject.IsNewDLCObject())
                            ityp_name = MapAsset.AppendMapPrefixIfRequired(Entity.Branch.Project, ityp_name);
                    }
                    // JWR - We should be adding dependencies during processing, not serialisation
                    container.AddDependency(ityp_name);
                }
            }
            xmlItem.Add(xmlObjects);

            List<TargetObjectDef> entityObjectDefs = entries.Where(entry => !entry.Object.IsPartOfAnEntitySet())
                                                            .Select(entry => entry.Object)
                                                            .ToList();

            XElement xmlRooms = new XElement("rooms");
            foreach (KeyValuePair<TargetObjectDef, MiloRoom> room in rooms)
            {
                XElement xmlRoom = AsCMloRoomDef(xmlRooms, room.Value, entityObjectDefs);
                xmlRooms.Add(xmlRoom);
            }
            xmlItem.Add(xmlRooms);

            XElement xmlPortals = new XElement("portals");
            foreach (KeyValuePair<TargetObjectDef, MiloPortal> portal in portals)
            {
                XElement xmlPortal = AsCMloPortalDef(xmlPortals, portal.Value, room_list, entityObjectDefs);
                xmlPortals.Add(xmlPortal);
            }
            xmlItem.Add(xmlPortals);

            if (entrySetsNode.Elements().Any())
                xmlItem.Add(entrySetsNode);

            // B* 971305 - timecycle spheres
            SerialiseTimecycleSpheres(archetype.Object, xmlItem);

            return (xmlItem);
        }

        /// <summary>
        /// Serialise a CCompositeEntityType/StatedAnim object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="parent"></param>
        /// <param name="archetype"></param>
        /// <returns></returns>
        protected static XElement AsCCompositeEntityType(CompositeEntityType compositeEntityType)
        {
            XElement xmlCompositeEntityType = new XElement("Item");
            BoundingBox3f bbox = new BoundingBox3f();

            Scene scene = compositeEntityType.StartObject.MyScene;
            float lodDist = SceneOverrideIntegrator.Instance.GetObjectLODDistance(compositeEntityType.StartObject);
            UInt32 flags = Flags.GetFlags(compositeEntityType.StartObject);
            int specialAttr = compositeEntityType.StartObject.GetAttribute(AttrNames.OBJ_ATTRIBUTE, AttrDefaults.OBJ_ATTRIBUTE);

            // GTA5 Bug #207448; ensure that the FLAG_IS_TYPE_OBJECT bit is
            // not set for composite types; since we're currently using the
            // start states flags this could happen.
            flags &= ~((uint)CBaseArchetype_Flags.F_IS_TYPE_OBJECT);

            Xml.CreateElementWithText(xmlCompositeEntityType, "Name", compositeEntityType.Name);
            Xml.CreateElementWithValueAttribute(xmlCompositeEntityType, "lodDist", lodDist.ToString());
            Xml.CreateElementWithValueAttribute(xmlCompositeEntityType, "flags", flags.ToString("d"));
            Xml.CreateElementWithValueAttribute(xmlCompositeEntityType, "specialAttribute", specialAttr);
            string startId = compositeEntityType.StartObject.GetObjectName();
            if (compositeEntityType.StartObject.IsIMAPGroupDummy())
            {
                startId = compositeEntityType.StartObject.GetParameter("imapName", PropDefaults.IMAP_GROUP_DUMMY_NAME);
                if (null == startId)
                    Log.Log__ErrorCtx(compositeEntityType.StartObject.Name, "StatedAnim state object {0} is an imap group, but name property is not set!", compositeEntityType.StartObject.Name);
                Xml.CreateElementWithText(xmlCompositeEntityType, "StartImapFile", startId);
            }
            else
            {
                bbox.Expand(compositeEntityType.StartObject.GetLocalBoundingBox());
                Xml.CreateElementWithText(xmlCompositeEntityType, "StartModel", startId);
            }

            string endId = compositeEntityType.EndObject.GetObjectName();
            if (compositeEntityType.EndObject.IsIMAPGroupDummy())
            {
                endId = compositeEntityType.EndObject.GetParameter("imapName", PropDefaults.IMAP_GROUP_DUMMY_NAME);
                if (null == endId)
                    Log.Log__ErrorCtx(compositeEntityType.EndObject.Name, "StatedAnim state object {0} is an imap group, but name property is not set!", compositeEntityType.EndObject.Name);
                Xml.CreateElementWithText(xmlCompositeEntityType, "EndImapFile", endId);
            }
            else
            {
                bbox.Expand(compositeEntityType.EndObject.GetLocalBoundingBox());
                Xml.CreateElementWithText(xmlCompositeEntityType, "EndModel", endId);
            }

            // PTFX Asset Name
            Xml.CreateElementWithText(xmlCompositeEntityType, "PtFxAssetName", compositeEntityType.PtfxAssetName);

            // Subscope for split animated objects per 128 bones
            XElement animsNode = new XElement("Animations");
            foreach (TargetObjectDef def in compositeEntityType.AnimationObjects)
            {
                XElement animItemNode = new XElement("Item");
                animsNode.Add(animItemNode);

                Xml.CreateElementWithText(animItemNode, "AnimatedModel", def.GetObjectName());
                Xml.CreateElementWithText(animItemNode, "AnimDict", 
                    def.GetAttribute(AttrNames.OBJ_ANIM, AttrDefaults.OBJ_ANIM));
                Xml.CreateElementWithText(animItemNode, "AnimName", def.GetObjectName());

                float relativeStartTime = 0.0f;
                float relativeStopTime = 1.0f;
                float animationStart = compositeEntityType.ProxyObject.GetAttribute(AttrNames.STATEDANIM_ANIM_START, AttrDefaults.STATEDANIM_ANIM_START);
                float animationEnd = compositeEntityType.ProxyObject.GetAttribute(AttrNames.STATEDANIM_ANIM_END, AttrDefaults.STATEDANIM_ANIM_END);
                RSG.SceneXml.Utils.AnimationUtil.CalculateRelativeAnimRange(def, "visibility", animationStart, animationEnd, out relativeStartTime, out relativeStopTime);
                Xml.CreateElementWithValueAttribute(animItemNode, "punchInPhase", relativeStartTime);
                Xml.CreateElementWithValueAttribute(animItemNode, "punchOutPhase", relativeStopTime);

                // attaching effects
                List<Definition> l2dfx = new List<Definition>();
                if (def.SkeletonRootGuid != Guid.Empty)
                {
                    TargetObjectDef skelObj = scene.FindObject(def.SkeletonRootGuid);
                    if (null != skelObj)
                        ProcessScene2DFX(scene, l2dfx, skelObj, true);
                }
                else
                    ProcessScene2DFX(scene, l2dfx, def);
                XElement effectNode = new XElement("effectsData");
                animItemNode.Add(effectNode);
                foreach (Definition fxdef in l2dfx)
                {
                    XNode node = fxdef.ToMETA2dfx(def);
                    if (null != node)
                        effectNode.Add(node);
                }

                bbox.Expand(def.GetLocalBoundingBox());
            }
            xmlCompositeEntityType.Add(animsNode);

            // Now that our boundingbox has been expanded for all of the child
            // nodes we can serialise it out to XML elements.

            // Bounding Box
//             Matrix34f transMatrix = Matrix34f.Identity;
//             transMatrix.Translation = -compositeEntityType.StartObject.NodeTransform.Translation;
//             bbox.Transform(transMatrix);
            Xml.CreateElementWithVectorAttributes(xmlCompositeEntityType, "bbMin", bbox.Min);
            Xml.CreateElementWithVectorAttributes(xmlCompositeEntityType, "bbMax", bbox.Max);

            // Bounding Sphere
            BoundingSpheref bsphere = new BoundingSpheref(bbox);
            Xml.CreateElementWithVectorAttributes(xmlCompositeEntityType, "bsCentre", bsphere.Centre);
            Xml.CreateElementWithValueAttribute(xmlCompositeEntityType, "bsRadius", bsphere.Radius);

            return (xmlCompositeEntityType);
        }
        #endregion // Archetype Serialisation Methods

        #region Extension Serialisation Methods
        /// <summary>
        /// Serialise a CExtensionDef object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        protected static XElement AsCExtensionDef(Extension extension, EffectCollection effectCollection)
        {
            if (extension.Object.Is2dfx())
            {
                if (extension.Object.Is2dfxAudioEmitter())
                    return (AsCExtensionDefAudioEmitter(extension));
                else if (extension.Object.Is2dfxBuoyancy())
                    return (AsCExtensionDefBuoyancy(extension));
                else if (extension.Object.Is2dfxDecal())
                    return (AsCExtensionDefDecal(extension));
                else if (extension.Object.Is2dfxExplosionEffect())
                    return (AsCExtensionDefExplosionEffect(extension));
                else if (extension.Object.Is2dfxLadderFx() || extension.Object.Is2dfxRSLadder())
                    return (AsCExtensionDefLadder(extension));
                // Runtime request; do not serialise Light Effects into the
                // ITYP data (they are resourced with drawable).  We are
                // keeping the light effect structures around for light instancing.
                //
                // The decision to not serialise light extensions is hard-wired into the ITYPCostEstimator.
                // Re-enabling this should be reflected there also.
                // (//RAGE/gta5/dev/rage/framework/tools/src/cli/RSG.Pipeline.MapExportMetadata/MergeSplit/ITYPMergeSplitController.cs)
                else if (extension.Object.Is2dfxLightEffect())
                    return (null);
                //return (AsCExtensionDefLightEffect(extension));
                else if (extension.Object.Is2dfxLightShaft())
                    return (AsCExtensionDefLightShaft(extension));
                else if (extension.Object.Is2dfxProcObject())
                    return (AsCExtensionDefProcObject(extension));
                else if (extension.Object.Is2dfxScript())
                    return (AsCExtensionDefScript(extension));
                else if (extension.Object.Is2dfxParticleEffect())
                    return (AsCExtensionDefParticleEffect(extension, effectCollection));
                else if (extension.Object.Is2dfxScrollbar())
                    return (AsCExtensionDefScrollbars(extension));
                else if (extension.Object.Is2dfxSpawnPoint())
                    return (AsCExtensionDefSpawnPoint(extension));
                else if (extension.Object.Is2dfxSwayableEffect())
                {
                    Log.Log__WarningCtx(extension.Parent.Name, "Swayable helpers are currently not supported.");
                    return (null); // Currently not supported at runtime.
                    //return (AsCExtensionDefSwayableEffect(doc, extension));
                }
                else if (extension.Object.Is2dfxWalkDontWalk())
                {
                    Log.Log__WarningCtx(extension.Parent.Name, "WalkDontWalk helpers are currently not supported.");
                    return (null); // Currently not supported at runtime.
                    // return (AsCExtensionDefWalkDontWalk(doc, extension));
                }
                else if (extension.Object.Is2dfxRsWindDisturbance())
                    return (AsCExtensionDefWindDisturbance(extension));
                else if (extension.Object.Is2dfxDoor())
                    return (AsCExtensionDefDoor(extension));
                else if (extension.Object.Is2dfxExpression())
                    return (AsCExtensionDefExpression(extension));
                
                String message = String.Format("Unrecognised 2dfx type: the serialiser doesn't know about the CExtensionDef required for object {0}.",
                    extension.Object.Name);
                Debug.Assert(false, "Unhandled 2dfx type; CExtensionDef required!",
                    message);
                Log.Log__ErrorCtx(extension.Object.Name, message);
                return (null);
            }

            Debug.Assert(false, "Internal error: unhandled extension type.",
                "Object {0} is of unrecognised type by the IMD serialiser.",
                extension.Object.Name);
            return (null);
        }

        /// <summary>
        /// Serialise a CExtensionDef object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        protected static XElement AsCExtensionDefCommon(Extension extension, bool buildOffsetPosition = true)
        {
            XElement xmlItem = AsFwExtensionDef(extension);
            xmlItem.SetAttributeValue("type", "CExtensionDef");

            if (buildOffsetPosition)
            {
                // JWR
                // This calculation is incorrect but it is used throughout the serialiser.
                // The game may be working around this in a number of places so changing this would be dangerous

                Matrix34f mtxParent = new Matrix34f();
                if (extension.Object.HasParent())
                    mtxParent = extension.Object.Parent.NodeTransform.Inverse();
                Matrix34f mtxLocal = extension.Object.NodeTransform * mtxParent;
                Vector3f pos = mtxLocal.Translation;
                Xml.CreateElementWithVectorAttributes(xmlItem, "offsetPosition", pos);
            }

            return (xmlItem);
        }

        /// <summary>
        /// Serialise a AsCExtensionDefAudioEmitter object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        protected static XElement AsCExtensionDefAudioEmitter(Extension extension)
        {
            XElement xmlItem = AsCExtensionDefCommon(extension);
            xmlItem.SetAttributeValue("type", "CExtensionDefAudioEmitter");

            Matrix34f localToWorldTransform = extension.Object.NodeTransform.Transpose();// HACK - Transpose to work around data issue

            Matrix34f parentToWorldTransform = new Matrix34f();
            Matrix34f worldToParentTransform = new Matrix34f();
            if (extension.Object.HasParent())
            {
                parentToWorldTransform = extension.Object.Parent.NodeTransform.Transpose();// HACK - Transpose to work around data issue
                worldToParentTransform = parentToWorldTransform.Inverse();
            }

            Matrix34f localToParentTransform = localToWorldTransform * worldToParentTransform;

            Quaternionf parentSpaceOrientation = new Quaternionf(localToParentTransform);

            Xml.CreateElementWithVectorAttributes(xmlItem, "offsetRotation", parentSpaceOrientation);

            String effect = extension.Object.GetAttribute(AttrNames.TWODFX_AUDIO_EMITTER_EFFECT, AttrDefaults.TWODFX_AUDIO_EMITTER_EFFECT);
            UInt32 effecthash = RSG.ManagedRage.StringHashUtil.atStringHash(effect, 0);
            Xml.CreateElementWithValueAttribute(xmlItem, "effectHash", effecthash);

            return (xmlItem);
        }

        /// <summary>
        /// Serialise a CExtensionDefAudioCollisionSettings object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        protected static XElement CExtensionDefAudioCollisionSettings(Archetype archtype, String modelAudioCollisionRAVEName)
        {
            XElement xmlItem = Xml.CreateElementWithAttribute(null, "Item", "type", "CExtensionDefAudioCollisionSettings");

            Xml.CreateElementWithText(xmlItem, "name", archtype.Name);

            // No offset - no helper used for this extension
            Xml.CreateElementWithVectorAttributes(xmlItem, "offsetPosition", new Vector3f(0.0f, 0.0f, 0.0f));
            Xml.CreateElementWithText(xmlItem, "settings", modelAudioCollisionRAVEName);

            return (xmlItem);
        }

        /// <summary>
        /// Serialise a CExtensionDefBuoyancy object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        protected static XElement AsCExtensionDefBuoyancy(Extension extension)
        {
            XElement xmlItem = AsCExtensionDefCommon(extension);
            xmlItem.SetAttributeValue("type", "CExtensionDefBuoyancy");

            return (xmlItem);
        }

        /// <summary>
        /// Serialise a AsCExtensionDefDecal object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        protected static XElement AsCExtensionDefDecal(Extension extension)
        {
            XElement xmlItem = AsCExtensionDefCommon(extension);
            xmlItem.SetAttributeValue("type", "CExtensionDefDecal");

            Matrix34f mtxParent = new Matrix34f();
            if (extension.Object.HasParent())
                mtxParent = extension.Object.Parent.NodeTransform.Inverse();
            Matrix34f mtxLocal = extension.Object.NodeTransform * mtxParent;
            Quaternionf rot = new Quaternionf(mtxLocal);
            Xml.CreateElementWithVectorAttributes(xmlItem, "offsetRotation", rot);

            String tag = extension.Object.GetAttribute(AttrNames.TWODFX_DECAL_NAME,
                AttrDefaults.TWODFX_DECAL_NAME);
            if ("" == tag)
                tag = AttrDefaults.TWODFX_DECAL_NAME;
            Xml.CreateElementWithText(xmlItem, "decalName", tag);

            int trigger = extension.Object.GetAttribute(AttrNames.TWODFX_DECAL_TRIGGER,
                AttrDefaults.TWODFX_DECAL_TRIGGER);
            Xml.CreateElementWithValueAttribute(xmlItem, "decalType", trigger);

            int boneid;
            bool attach_all = extension.Object.GetAttribute(AttrNames.TWODFX_DECAL_ATTACH_TO_ALL, AttrDefaults.TWODFX_DECAL_ATTACH_TO_ALL);
            if (attach_all)
                boneid = -1;
            else
                boneid = extension.Object.GetAttribute(AttrNames.TWODFX_DECAL_ATTACH, AttrDefaults.TWODFX_DECAL_ATTACH);
            Xml.CreateElementWithValueAttribute(xmlItem, "boneTag", boneid);

            float scale = extension.Object.GetAttribute(AttrNames.TWODFX_DECAL_SCALE,
                AttrDefaults.TWODFX_DECAL_SCALE);
            Xml.CreateElementWithValueAttribute(xmlItem, "scale", scale);

            float prob = extension.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_PROBABILITY,
                AttrDefaults.TWODFX_PARTICLE_PROBABILITY);
            Xml.CreateElementWithValueAttribute(xmlItem, "probability", prob);

            UInt32 flags = Flags.GetDecalFlags(extension.Object);
            Xml.CreateElementWithValueAttribute(xmlItem, "flags", flags);

            return (xmlItem);
        }        

        /// <summary>
        /// Serialise a CExtensionDefExplosionEffect object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        protected static XElement AsCExtensionDefExplosionEffect(Extension extension)
        {
            XElement xmlItem = AsCExtensionDefCommon(extension);
            xmlItem.SetAttributeValue("type", "CExtensionDefExplosionEffect");

            Matrix34f mtxParent = new Matrix34f();
            if (extension.Object.HasParent())
                mtxParent = extension.Object.Parent.NodeTransform.Inverse();
            Matrix34f mtxLocal = extension.Object.NodeTransform * mtxParent;
            Quaternionf rot = new Quaternionf(mtxLocal);
            Xml.CreateElementWithVectorAttributes(xmlItem, "offsetRotation", rot);

            int trigger = extension.Object.GetAttribute(AttrNames.TWODFX_EXPLOSION_TRIGGER, AttrDefaults.TWODFX_EXPLOSION_TRIGGER);
            bool attach_all = extension.Object.GetAttribute(AttrNames.TWODFX_EXPLOSION_ATTACH_TO_ALL, AttrDefaults.TWODFX_EXPLOSION_ATTACH_TO_ALL);
            int boneid;
            if (attach_all)
                boneid = -1;
            else
                boneid = extension.Object.GetAttribute(AttrNames.TWODFX_EXPLOSION_ATTACH, AttrDefaults.TWODFX_EXPLOSION_ATTACH);
            UInt32 flags = Flags.GetExplosionFlags(extension.Object);

            String explosionName = extension.Object.GetAttribute(AttrNames.TWODFX_EXPLOSION_NAME, AttrDefaults.TWODFX_EXPLOSION_NAME);
            if ("" == explosionName)
                explosionName = AttrDefaults.TWODFX_EXPLOSION_NAME;

            Xml.CreateElementWithText(xmlItem, "explosionName", explosionName);
            Xml.CreateElementWithValueAttribute(xmlItem, "boneTag", boneid);
            Xml.CreateElementWithValueAttribute(xmlItem, "explosionType", trigger);
            Xml.CreateElementWithValueAttribute(xmlItem, "flags", flags);

            return (xmlItem);
        }

        /// <summary>
        /// Serialise a CExtensionDefParticleEffect object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        protected static XElement AsCExtensionDefParticleEffect(Extension extension, EffectCollection effectCollection)
        {
            if (extension.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_DONTEXPORT, AttrDefaults.TWODFX_PARTICLE_DONTEXPORT))
                return null;

            // Platform specific check to see if the particle is available for this platform.
            if (!extension.Object.IsActive())
                return null;

            XElement xmlItem = AsCExtensionDefCommon(extension);
            xmlItem.SetAttributeValue("type", "CExtensionDefParticleEffect");

            Matrix34f localToWorldTransform = extension.Object.NodeTransform.Transpose();// HACK - Transpose to work around data issue

            Matrix34f parentToWorldTransform = new Matrix34f();
            Matrix34f worldToParentTransform = new Matrix34f();
            if (extension.Object.HasParent())
            {
                parentToWorldTransform = extension.Object.Parent.NodeTransform.Transpose();// HACK - Transpose to work around data issue
                worldToParentTransform = parentToWorldTransform.Inverse();
            }

            Matrix34f localToParentTransform = localToWorldTransform * worldToParentTransform;

            Quaternionf parentSpaceOrientation = new Quaternionf(localToParentTransform);

            Xml.CreateElementWithVectorAttributes(xmlItem, "offsetRotation", parentSpaceOrientation);

            String tag = extension.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_NAME,
                AttrDefaults.TWODFX_PARTICLE_NAME);
            if ("" == tag)
                tag = AttrDefaults.TWODFX_PARTICLE_NAME;
            Xml.CreateElementWithText(xmlItem, "fxName", tag);

            String sceneName = Path.GetFileNameWithoutExtension(extension.Object.MyScene.ExportFilename);
            EffectCollection.PtfxTriggerType trigger = effectCollection.GetAssetTriggerTypeForEffect(tag, extension.Object.Parent.Name, sceneName);
            Xml.CreateElementWithValueAttribute(xmlItem, "fxType", (int)trigger);

            int boneid = extension.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_ATTACH,
                AttrDefaults.TWODFX_PARTICLE_ATTACH);
            bool attachToAll = extension.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_ATTACH_TO_ALL, 
                AttrDefaults.TWODFX_PARTICLE_ATTACH_TO_ALL);
            if (attachToAll)
                boneid = -1;
            Xml.CreateElementWithValueAttribute(xmlItem, "boneTag", boneid);

            float scale = extension.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_SCALE,
                AttrDefaults.TWODFX_PARTICLE_SCALE);
            Xml.CreateElementWithValueAttribute(xmlItem, "scale", scale);

            float prob = extension.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_PROBABILITY,
                AttrDefaults.TWODFX_PARTICLE_PROBABILITY);
            Xml.CreateElementWithValueAttribute(xmlItem, "probability", prob);

            UInt32 flags = Flags.GetParticleFlags(extension.Object);
            Xml.CreateElementWithValueAttribute(xmlItem, "flags", flags);

            int r = extension.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_COLOUR_R,
                AttrDefaults.TWODFX_PARTICLE_COLOUR_R);
            int g = extension.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_COLOUR_G,
                AttrDefaults.TWODFX_PARTICLE_COLOUR_G);
            int b = extension.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_COLOUR_B,
                AttrDefaults.TWODFX_PARTICLE_COLOUR_B);
            int a = extension.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_COLOUR_A,
                AttrDefaults.TWODFX_PARTICLE_COLOUR_A);
            Xml.CreateColor32Element(xmlItem, "color", new Vector4f(r, g, b, a), false);


            return (xmlItem);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="rootObject"></param>
        /// <returns></returns>
        private XElement AsCCompositeEntityParticleEffect(Extension extension, ObjectDef rootObject, CompositeEntityType compositeEntity)
        {
            if (extension.Object.GetAttribute(AttrNames.TWODFX_PARTICLE_DONTEXPORT, AttrDefaults.TWODFX_PARTICLE_DONTEXPORT))
                return null;

            XElement rootNode = new XElement("Item");
            return rootNode;
        }

        /// <summary>
        /// Serialise a CExtensionDefLightEffect object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        protected static XElement AsCExtensionDefLightEffect(Extension extension)
        {
            bool dontExport = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHT_DONT_EXPORT, AttrDefaults.TWODFX_LIGHT_DONT_EXPORT);
            if (dontExport)
                return (null);

            // Regular attributes
            int flashiness = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHT_FLASHINESS, AttrDefaults.TWODFX_LIGHT_FLASHINESS);
            float volinten = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHT_VOLUME_INTENSITY, AttrDefaults.TWODFX_LIGHT_VOLUME_INTENSITY);
            float volsize = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHT_VOLUME_SIZE, AttrDefaults.TWODFX_LIGHT_VOLUME_SIZE);
            int attach = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHT_ATTACH, AttrDefaults.TWODFX_LIGHT_ATTACH);
            float corona_size = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHT_CORONA_SIZE, AttrDefaults.TWODFX_LIGHT_CORONA_SIZE);
            float corona_inten = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHT_CORONA_INTENSITY, AttrDefaults.TWODFX_LIGHT_CORONA_INTENSITY);
            float corona_zBias = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHT_CORONA_ZBIAS, AttrDefaults.TWODFX_LIGHT_CORONA_ZBIAS);
            String shadowmap = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHT_SHADOW_NAME, AttrDefaults.TWODFX_LIGHTPHOTO_SHADOW_NAME);

            // Light Properties
            Vector3f colour = ParamHelpers.GetLightParam(extension.Object, ParamNames.TWODFX_LIGHT_COLOUR, ParamDefaults.TWODFX_LIGHT_COLOUR);
            Vector3i intColour = (colour * 255).ToVector3i();
            String shadowmapPropValue = ParamHelpers.GetLightParam(extension.Object, ParamNames.TWODFX_LIGHT_SHADOW_MAP, ParamDefaults.TWODFX_LIGHT_SHADOW_MAP);
            float cone_inner = ParamHelpers.GetLightParam(extension.Object, ParamNames.TWODFX_LIGHT_HOTSPOT, ParamDefaults.TWODFX_LIGHT_HOTSPOT);
            float cone_outer = ParamHelpers.GetLightParam(extension.Object, ParamNames.TWODFX_LIGHT_FALLSIZE, ParamDefaults.TWODFX_LIGHT_FALLSIZE);
            cone_inner /= 2.0f;
            cone_outer /= 2.0f;
            float intensity = ParamHelpers.GetLightParam(extension.Object, ParamNames.TWODFX_LIGHT_INTENSITY, ParamDefaults.TWODFX_LIGHT_INTENSITY);
            float atten_end = ParamHelpers.GetLightParam(extension.Object, ParamNames.TWODFX_LIGHT_ATTENUATION_END, ParamDefaults.TWODFX_LIGHT_ATTENUATION_END);
            Vector3f outerCol = ParamHelpers.GetLightParam(extension.Object, ParamNames.TWODFX_LIGHT_VOL_OUTER_COLOUR, ParamDefaults.TWODFX_LIGHT_VOL_OUTER_COLOUR);
            Vector3i intOuterColour = (outerCol * 255).ToVector3i();
            float outerColIntensity = ParamHelpers.GetLightParam(extension.Object, ParamNames.TWODFX_LIGHT_VOL_OUTER_COLOUR_INTEN, ParamDefaults.TWODFX_LIGHT_VOL_OUTER_COLOUR_INTEN);
            float outerColExponent = ParamHelpers.GetLightParam(extension.Object, ParamNames.TWODFX_LIGHT_VOL_OUTER_COLOUR_EXP, ParamDefaults.TWODFX_LIGHT_VOL_OUTER_COLOUR_EXP);
            uint flags = Flags.GetLightFlags(extension.Object);
            // write time flags. if not a time object write number bugger than possible daytime
            // defaultin to binary 111100000000000000111111
            uint timeFlags = Flags.GetTimeFlags(extension.Object, 0xF0003F);
            if (timeFlags == 0)
                Log.Log__WarningCtx(extension.Object.Name, "Light object {0} has no time set. The light won't be visible.", extension.Object.Name);
            int actualLightType = ParamHelpers.GetLightParam(extension.Object, ParamNames.TWODFX_LIGHT_TYPE, ParamDefaults.TWODFX_LIGHT_TYPE);
            Flags.TwodfxLightTypes lighttype = Flags.TwodfxLightTypes.TWODFX_LIGHT_STANDARD;
            if (actualLightType > (int)ParamNames.TWODFX_SCENEXML_LIGHTTYPES.TWODFX_SCENEXML_LIGHTTYPE_OMNI)
            {
                if (actualLightType == (int)ParamNames.TWODFX_SCENEXML_LIGHTTYPES.TWODFX_SCENEXML_LIGHTTYPE_CAPSULE)
                    lighttype = Flags.TwodfxLightTypes.TWODFX_LIGHT_CAPSULE;
                else
                    lighttype = Flags.TwodfxLightTypes.TWODFX_LIGHT_SPOT;
            }

            float expFalloff = ParamHelpers.GetLightParam(extension.Object, ParamNames.TWODFX_LIGHT_EXP_FALLOFF, ParamDefaults.TWODFX_LIGHT_EXP_FALLOFF);

            // Light Properties
            if (null != shadowmapPropValue && !System.IO.File.Exists(shadowmapPropValue))
            {
                Log.Log__WarningCtx(extension.Object.Name, "The light's ({0}) projection texture {1} does not exist on your machine. Won't be considered.", extension.Object.Name, shadowmapPropValue);
                shadowmapPropValue = null;
            }
            if (shadowmap == AttrDefaults.TWODFX_LIGHTPHOTO_SHADOW_NAME && null != shadowmapPropValue)
            {
                //if shadowmap texture name is set to default for a spot light, allow use of the 'Property' value
                shadowmap = shadowmapPropValue;
            }
            else if (shadowmapPropValue != null)
            {
                Log.Log__WarningCtx(extension.Object.Name, "Shadow map ATTRIBUTE on light {0} is set to non-default {1}. Overriding the artist's defined Projection map {2}!\n", extension.Object.Name, shadowmap, shadowmapPropValue);
            }
            shadowmap = System.IO.Path.GetFileNameWithoutExtension(shadowmap);
            uint shadowMapHash = (uint)RSG.ManagedRage.StringHashUtil.atStringHash(shadowmap, 0);

            float capsuleWidth = ParamHelpers.GetLightParam(extension.Object, ParamNames.TWODFX_LIGHT_CAPSULE_WIDTH, ParamDefaults.TWODFX_LIGHT_CAPSULE_WIDTH);

            XElement xmlItem = AsCExtensionDefCommon(extension);
            xmlItem.SetAttributeValue("type", "CExtensionDefLightEffect");

            Matrix34f mtxParent = new Matrix34f();
            if (extension.Object.HasParent())
                mtxParent = extension.Object.Parent.NodeTransform.Inverse();
            Matrix34f mtxLocal = extension.Object.NodeTransform * mtxParent;
            Quaternionf rot = new Quaternionf(mtxLocal);
            Xml.CreateElementWithVectorAttributes(xmlItem, "offsetRotation", rot);


            Matrix34f localToWorldTransform = extension.Object.NodeTransform.Transpose();// HACK - Transpose to work around data issue

            Matrix34f parentToWorldTransform = new Matrix34f();
            Matrix34f worldToParentTransform = new Matrix34f();
            if (extension.Object.HasParent())
            {
                parentToWorldTransform = extension.Object.Parent.NodeTransform.Transpose();// HACK - Transpose to work around data issue
                worldToParentTransform = parentToWorldTransform.Inverse();
            }

            Matrix34f localToParentTransform = localToWorldTransform * worldToParentTransform;
            Quaternionf parentSpaceOrientation = new Quaternionf(localToParentTransform);
            Vector3f direction = -localToParentTransform.C;
            direction.Normalise();
            Vector3f tangent = -localToParentTransform.A;
            tangent.Normalise();

            Vector4f cullPlane = ParamHelpers.GetLightParam(extension.Object, ParamNames.TWODFX_LIGHT_CULLPLANE, ParamDefaults.TWODFX_LIGHT_CULLPLANE);
            Vector3f cullPlaneRotPart = new Vector3f(cullPlane.X, cullPlane.Y, cullPlane.Z);
            // Calculating the cullplane orientation realtiv to the PARENT object
            Matrix34f parentToLocalTransform = new Matrix34f(localToParentTransform);
            parentToLocalTransform.Translation = new Vector3f();
            parentToLocalTransform = parentToLocalTransform.Inverse();
            parentToLocalTransform.Transform(cullPlaneRotPart);
            cullPlaneRotPart.Normalise();
            cullPlane.X = cullPlaneRotPart.X;
            cullPlane.Y = cullPlaneRotPart.Y;
            cullPlane.Z = cullPlaneRotPart.Z;
            if (!ParamHelpers.GetLightParam(extension.Object, ParamNames.TWODFX_LIGHT_USE_CULLPLANE, AttrDefaults.TWODFX_LIGHT_USE_CULLPLANE))
                cullPlane.W = atten_end;

            byte lightIstanceHash = extension.Object.AttributeGuid.ToByteArray()[0];

            //new ValueType defaults from //rage/gta5/dev/rage/framework/tools/src/cli/ragebuilder/gta_res.h

            XElement posElem = new XElement("posn");
            Xml.CreateElementWithValueAttribute(posElem, "x", localToParentTransform.Translation.X);
            Xml.CreateElementWithValueAttribute(posElem, "y", localToParentTransform.Translation.Y);
            Xml.CreateElementWithValueAttribute(posElem, "z", localToParentTransform.Translation.Z);
            xmlItem.Add(posElem);
            XElement colourElem = new XElement("colour");
            Xml.CreateElementWithValueAttribute(colourElem, "r", intColour.X);
            Xml.CreateElementWithValueAttribute(colourElem, "g", intColour.Y);
            Xml.CreateElementWithValueAttribute(colourElem, "b", intColour.Z);
            xmlItem.Add(colourElem);
            Xml.CreateElementWithValueAttribute(xmlItem, "intensity", intensity);
            Xml.CreateElementWithValueAttribute(xmlItem, "flashiness", flashiness);
            Xml.CreateElementWithValueAttribute(xmlItem, "flags", flags);
            Xml.CreateElementWithValueAttribute(xmlItem, "boneTag", attach);
            Xml.CreateElementWithValueAttribute(xmlItem, "lightType", (uint)lighttype);
            Xml.CreateElementWithValueAttribute(xmlItem, "groupId", 0);                                // dummy value, set by runtime
            Xml.CreateElementWithValueAttribute(xmlItem, "timeFlags", timeFlags);
            XElement cullPlaneElem = new XElement("cullingPlane");
            Xml.CreateElementWithValueAttribute(cullPlaneElem, "x", cullPlane.X);
            Xml.CreateElementWithValueAttribute(cullPlaneElem, "y", cullPlane.Y);
            Xml.CreateElementWithValueAttribute(cullPlaneElem, "z", cullPlane.Z);
            Xml.CreateElementWithValueAttribute(cullPlaneElem, "w", cullPlane.W);
            xmlItem.Add(cullPlaneElem);
            Xml.CreateElementWithValueAttribute(xmlItem, "falloff", atten_end);
            Xml.CreateElementWithValueAttribute(xmlItem, "falloffExponent", expFalloff);
            Xml.CreateElementWithValueAttribute(xmlItem, "volIntensity", volinten);
            Xml.CreateElementWithValueAttribute(xmlItem, "volSizeScale", volsize);
            XElement volOuterColourElem = new XElement("volOuterColour");
            Xml.CreateElementWithValueAttribute(volOuterColourElem, "r", intOuterColour.X);
            Xml.CreateElementWithValueAttribute(volOuterColourElem, "g", intOuterColour.Y);
            Xml.CreateElementWithValueAttribute(volOuterColourElem, "b", intOuterColour.Z);
            xmlItem.Add(volOuterColourElem);
            Xml.CreateElementWithValueAttribute(xmlItem, "volOuterColourIntensity", outerColIntensity);
            Xml.CreateElementWithValueAttribute(xmlItem, "volOuterExponent", outerColExponent);
            Xml.CreateElementWithValueAttribute(xmlItem, "lightHash", lightIstanceHash);
            Xml.CreateElementWithValueAttribute(xmlItem, "volOuterIntensity", 0);                      // dummy, to be replaced
            Xml.CreateElementWithValueAttribute(xmlItem, "coronaSize", corona_size);
            Xml.CreateElementWithValueAttribute(xmlItem, "coronaSizeAdjustNear", 0.0f);              // dummy, to be replaced
            Xml.CreateElementWithValueAttribute(xmlItem, "coronaSizeAdjustFar", 0.0f);               // dummy, to be replaced
            Xml.CreateElementWithValueAttribute(xmlItem, "coronaSizeAdjustDistance", 0.0f);          // dummy, to be replaced
            Xml.CreateElementWithValueAttribute(xmlItem, "coronaIntensity", corona_inten);
            Xml.CreateElementWithValueAttribute(xmlItem, "coronaZBias", corona_zBias);
            Xml.CreateElementWithValueAttribute(xmlItem, "projectedTextureKey", shadowMapHash);
            XElement dirElem = new XElement("direction");
            Xml.CreateElementWithValueAttribute(dirElem, "x", direction.X);
            Xml.CreateElementWithValueAttribute(dirElem, "y", direction.Y);
            Xml.CreateElementWithValueAttribute(dirElem, "z", direction.Z);
            xmlItem.Add(dirElem);
            Xml.CreateElementWithValueAttribute(xmlItem, "coneInnerAngle", cone_inner);
            XElement tangentElem = new XElement("tangent");
            Xml.CreateElementWithValueAttribute(tangentElem, "x", tangent.X);
            Xml.CreateElementWithValueAttribute(tangentElem, "y", tangent.Y);
            Xml.CreateElementWithValueAttribute(tangentElem, "z", tangent.Z);
            xmlItem.Add(tangentElem);
            Xml.CreateElementWithValueAttribute(xmlItem, "coneOuterAngle", cone_outer);
            XElement extentsElem = new XElement("extents");                                          // dummy, to be replaced
            Xml.CreateElementWithValueAttribute(extentsElem, "x", capsuleWidth);                   // dummy, to be replaced
            Xml.CreateElementWithValueAttribute(extentsElem, "y", 0.0f);                   // dummy, to be replaced
            Xml.CreateElementWithValueAttribute(extentsElem, "z", 0.0f);                   // dummy, to be replaced
            xmlItem.Add(extentsElem);                                                               // dummy, to be replaced

            return (xmlItem);
        }

        /// <summary>
        /// Serialise a CExtensionDefLightShaft object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        protected static XElement AsCExtensionDefLightShaft(Extension extension)
        {
            XElement xmlItem = AsCExtensionDefCommon(extension);
            xmlItem.SetAttributeValue("type", "CExtensionDefLightShaft");

            Matrix34f localToWorldTransform = extension.Object.NodeTransform.Transpose();// HACK - Transpose to work around data issue

            Matrix34f parentToWorldTransform = new Matrix34f();
            Matrix34f worldToParentTransform = new Matrix34f();
            if (extension.Object.HasParent())
            {
                parentToWorldTransform = extension.Object.Parent.NodeTransform.Transpose();// HACK - Transpose to work around data issue
                worldToParentTransform = parentToWorldTransform.Inverse();
            }

            Matrix34f worldToLocalTransform = localToWorldTransform.Inverse();
            Matrix34f localToParentTransform = localToWorldTransform * worldToParentTransform;
            Matrix34f parentToLocalTransform = new Matrix34f(localToParentTransform);
            parentToLocalTransform.Translation = new Vector3f();
            parentToLocalTransform = parentToLocalTransform.Inverse();

            Matrix34f localToWorldRot = new Matrix34f(localToWorldTransform);
            localToWorldRot.Translation = new Vector3f(0, 0, 0);

            Quaternionf parentSpaceOrientation = new Quaternionf(localToParentTransform);
            Xml.CreateElementWithVectorAttributes(xmlItem, "offsetRotation", parentSpaceOrientation);

            float lengthdiv2 = extension.Object.GetParameter(ParamNames.TWODFX_LIGHTSHAFT_LENGTH, ParamDefaults.TWODFX_LIGHTSHAFT_LENGTH) / 2.0f;
            float widthdiv2 = extension.Object.GetParameter(ParamNames.TWODFX_LIGHTSHAFT_WIDTH, ParamDefaults.TWODFX_LIGHTSHAFT_WIDTH) / 2.0f;
            Vector3f[] points = {
                new Vector3f(widthdiv2, -lengthdiv2, 0.0f),
                new Vector3f(widthdiv2, lengthdiv2, 0.0f),
                new Vector3f(-widthdiv2, lengthdiv2, 0.0f),
                new Vector3f(-widthdiv2, -lengthdiv2, 0.0f)
            };

            Vector3f a = localToParentTransform * points[3];
            Vector3f b = localToParentTransform * points[2];
            Vector3f c = localToParentTransform * points[1];
            Vector3f d = localToParentTransform * points[0];

            float length = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_LENGTH, AttrDefaults.TWODFX_LIGHTSHAFT_LENGTH);
            float intensity = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_INTENSITY, AttrDefaults.TWODFX_LIGHTSHAFT_INTENSITY);
            UInt32 flags = Flags.GetLightShaftFlags(extension.Object);
            float directionX = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_DIRECTION_X, AttrDefaults.TWODFX_LIGHTSHAFT_DIRECTION_X);
            float directionY = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_DIRECTION_Y, AttrDefaults.TWODFX_LIGHTSHAFT_DIRECTION_Y);
            float directionZ = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_DIRECTION_Z, AttrDefaults.TWODFX_LIGHTSHAFT_DIRECTION_Z);
            Vector3f direction = new Vector3f(directionX, directionY, directionZ);
            localToWorldRot.Transform(direction);
            direction.Normalise();
            int colourR = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_COLOUR_R, AttrDefaults.TWODFX_LIGHTSHAFT_COLOUR_R);
            int colourG = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_COLOUR_G, AttrDefaults.TWODFX_LIGHTSHAFT_COLOUR_G);
            int colourB = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_COLOUR_B, AttrDefaults.TWODFX_LIGHTSHAFT_COLOUR_B);

            float directionAmount = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_DIRECTION_AMOUNT, AttrDefaults.TWODFX_LIGHTSHAFT_DIRECTION_AMOUNT);

            float softness = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_SOFTNESS, AttrDefaults.TWODFX_LIGHTSHAFT_SOFTNESS);
            int densityType = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_DENSITY_TYPE, AttrDefaults.TWODFX_LIGHTSHAFT_DENSITY_TYPE);
            int volumeType = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_VOLUME_TYPE, AttrDefaults.TWODFX_LIGHTSHAFT_VOLUME_TYPE);

            float fadeInTimeStart = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_FADE_IN_TIME_START, AttrDefaults.TWODFX_LIGHTSHAFT_FADE_IN_TIME_START);
            float fadeInTimeEnd = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_FADE_IN_TIME_END, AttrDefaults.TWODFX_LIGHTSHAFT_FADE_IN_TIME_END);
            float fadeOutTimeStart = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_FADE_OUT_TIME_START, AttrDefaults.TWODFX_LIGHTSHAFT_FADE_OUT_TIME_START);
            float fadeOutTimeEnd = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_FADE_OUT_TIME_END, AttrDefaults.TWODFX_LIGHTSHAFT_FADE_OUT_TIME_END);
            float fadeDistanceStart = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_FADE_DISTANCE_START, AttrDefaults.TWODFX_LIGHTSHAFT_FADE_DISTANCE_START);
            float fadeDistanceEnd = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_FADE_DISTANCE_END, AttrDefaults.TWODFX_LIGHTSHAFT_FADE_DISTANCE_END);
            int flashiness = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHTSHAFT_FLASHINESS, AttrDefaults.TWODFX_LIGHTSHAFT_FLASHINESS);

            Xml.CreateElementWithVectorAttributes(xmlItem, "cornerA", a);
            Xml.CreateElementWithVectorAttributes(xmlItem, "cornerB", b);
            Xml.CreateElementWithVectorAttributes(xmlItem, "cornerC", c);
            Xml.CreateElementWithVectorAttributes(xmlItem, "cornerD", d);
            Xml.CreateElementWithVectorAttributes(xmlItem, "direction", direction);
            Xml.CreateElementWithValueAttribute(xmlItem, "length", length);
            Xml.CreateElementWithValueAttribute(xmlItem, "intensity", intensity);
            Xml.CreateElementWithValueAttribute(xmlItem, "flags", flags);
            Xml.CreateElementWithValueAttribute(xmlItem, "softness", softness);
            Xml.CreateElementWithValueAttribute(xmlItem, "directionAmount", directionAmount);
            Xml.CreateElementWithValueAttribute(xmlItem, "densityType", densityType);
            Xml.CreateElementWithValueAttribute(xmlItem, "volumeType", volumeType);
            Xml.CreateColor32Element(xmlItem, "color", new Vector3f(colourR / 255.0f, colourG / 255.0f, colourB / 255.0f));
            Xml.CreateElementWithValueAttribute(xmlItem, "fadeInTimeStart", fadeInTimeStart);
            Xml.CreateElementWithValueAttribute(xmlItem, "fadeInTimeEnd", fadeInTimeEnd);
            Xml.CreateElementWithValueAttribute(xmlItem, "fadeOutTimeStart", fadeOutTimeStart);
            Xml.CreateElementWithValueAttribute(xmlItem, "fadeOutTimeEnd", fadeOutTimeEnd);
            Xml.CreateElementWithValueAttribute(xmlItem, "fadeDistanceStart", fadeDistanceStart);
            Xml.CreateElementWithValueAttribute(xmlItem, "fadeDistanceEnd", fadeDistanceEnd);
            Xml.CreateElementWithValueAttribute(xmlItem, "flashiness", flashiness);

            return (xmlItem);
        }

        /// <summary>
        /// Serialise a CExtensionDefScrollbars object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        protected static XElement AsCExtensionDefScrollbars(Extension extension)
        {
            XElement xmlItem = AsCExtensionDefCommon(extension);
            xmlItem.SetAttributeValue("type", "CExtensionDefScrollbars");

            float height = extension.Object.GetParameter(ParamNames.TWODFX_SCROLLBARS_HEIGHT,
                ParamDefaults.TWODFX_SCROLLBARS_HEIGHT);
            int type = extension.Object.GetParameter(ParamNames.TWODFX_SCROLLBARS_TYPE,
                ParamDefaults.TWODFX_SCROLLBARS_TYPE);
            int numpoints = extension.Object.GetParameter(ParamNames.TWODFX_SCROLLBARS_NUMPOINTS,
                ParamDefaults.TWODFX_SCROLLBARS_NUMPOINTS);

            Xml.CreateElementWithValueAttribute(xmlItem, "height", height);
            Xml.CreateElementWithValueAttribute(xmlItem, "scrollbarsType", type);

            XElement xmlPoints = new XElement("points");
            xmlItem.Add(xmlPoints);
            if ((numpoints - 1) > 0)
            {
                Matrix34f mtxObject = extension.Object.NodeTransform.Inverse();
                int n = 0;
                foreach (TargetObjectDef o in extension.Object.SubObjects)
                {
                    if (0 != n)
                    {
                        // Calculate point positions in local-space.
                        Matrix34f mtxLocal = mtxObject * o.NodeTransform;
                        Vector3f pos = mtxLocal.Translation;
                        Xml.CreateElementWithVectorAttributes(xmlPoints, "Item", pos);
                    }
                }
            }

            return (xmlItem);
        }

        /// <summary>
        /// Serialise a CExtensionDefSpawnPoint object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="parent"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        protected static XElement AsCExtensionDefSpawnPoint(Extension extension)
        {
            XElement xmlItem = AsCExtensionDefCommon( extension);
            xmlItem.SetAttributeValue("type", "CExtensionDefSpawnPoint");

            Matrix34f localToWorldTransform = extension.Object.NodeTransform.Transpose();// HACK - Transpose to work around data issue

            Matrix34f parentToWorldTransform = new Matrix34f();
            Matrix34f worldToParentTransform = new Matrix34f();
            if (extension.Object.HasParent())
            {
                parentToWorldTransform = extension.Object.Parent.NodeTransform.Transpose();// HACK - Transpose to work around data issue
                worldToParentTransform = parentToWorldTransform.Inverse();
            }

            Matrix34f localToParentTransform = localToWorldTransform * worldToParentTransform;

            Quaternionf parentSpaceOrientation = new Quaternionf(localToParentTransform);
            Xml.CreateElementWithVectorAttributes(xmlItem, "offsetRotation", parentSpaceOrientation);

            String spawnType = extension.Object.GetParameter(ParamNames.TWODFX_SPAWNPOINT_SPAWNTYPE,
                ParamDefaults.TWODFX_SPAWNPOINT_SPAWNTYPE);
            String groupType = extension.Object.GetParameter(ParamNames.TWODFX_SPAWNPOINT_GROUPTYPE,
                ParamDefaults.TWODFX_SPAWNPOINT_GROUPTYPE);
            String modelSet = extension.Object.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_MODEL_SET,
                AttrDefaults.TWODFX_SPAWNPOINT_MODEL_SET);
            int start = extension.Object.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_START_TIME,
                AttrDefaults.TWODFX_SPAWNPOINT_START_TIME);
            int end = extension.Object.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_END_TIME,
                AttrDefaults.TWODFX_SPAWNPOINT_END_TIME);
            int availabilityInt = extension.Object.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_AVAILABILITY,
                AttrDefaults.TWODFX_SPAWNPOINT_AVAILABILITY);
            SpawnPointAvailability availability = (SpawnPointAvailability)availabilityInt;
            float timeTillPedLeaves = extension.Object.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_TIME_TILL_PED_LEAVES,
                AttrDefaults.TWODFX_SPAWNPOINT_TIME_TILL_PED_LEAVES);
            float radius = extension.Object.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_RADIUS,
                AttrDefaults.TWODFX_SPAWNPOINT_RADIUS);

            Xml.CreateElementWithText(xmlItem, "spawnType", spawnType);
            Xml.CreateElementWithText(xmlItem, "pedType", modelSet);
            Xml.CreateElementWithText(xmlItem, "groupType", groupType);
            Xml.CreateElementWithValueAttribute(xmlItem, "start", start);
            Xml.CreateElementWithValueAttribute(xmlItem, "end", end);

            // No "Model Set" for the definition???
            Xml.CreateElementWithText(xmlItem, "AvailabilityInMpSp", availability.ToString());
            Xml.CreateElementWithValueAttribute(xmlItem, "TimeTillPedLeaves", timeTillPedLeaves);
            Xml.CreateElementWithValueAttribute(xmlItem, "Radius", radius);
            Xml.CreateElementWithText(xmlItem, "flags", SpawnPointFlags.GetEnumString(extension.Object));
            
            return (xmlItem);
        }

        /// <summary>
        /// Serialise a CExtensionDefSwayableEffect object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="parent"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        protected static XElement AsCExtensionDefSwayableEffect(Extension extension)
        {
            XElement xmlItem = AsCExtensionDefCommon(extension);
            xmlItem.SetAttributeValue("type", "CExtensionDefSwayableEffect");

            Matrix34f mtxParent = new Matrix34f();
            if (extension.Object.HasParent())
                mtxParent = extension.Object.Parent.NodeTransform.Inverse();
            Matrix34f mtxLocal = extension.Object.NodeTransform * mtxParent;
            Quaternionf rot = new Quaternionf(mtxLocal);
            Xml.CreateElementWithVectorAttributes(xmlItem, "offsetRotation", rot);

            int boneid = extension.Object.GetAttribute(AttrNames.TWODFX_SWAYABLE_BONEID,
                AttrDefaults.TWODFX_SWAYABLE_BONEID);
            float lwspeed = extension.Object.GetAttribute(AttrNames.TWODFX_SWAYABLE_LOW_WIND_SPEED,
                AttrDefaults.TWODFX_SWAYABLE_LOW_WIND_SPEED);
            float lwampl = extension.Object.GetAttribute(AttrNames.TWODFX_SWAYABLE_LOW_WIND_AMPLITUDE,
                AttrDefaults.TWODFX_SWAYABLE_LOW_WIND_AMPLITUDE);
            float hwspeed = extension.Object.GetAttribute(AttrNames.TWODFX_SWAYABLE_HIGH_WIND_SPEED,
                AttrDefaults.TWODFX_SWAYABLE_HIGH_WIND_SPEED);
            float hwampl = extension.Object.GetAttribute(AttrNames.TWODFX_SWAYABLE_HIGH_WIND_AMPLITUDE,
                AttrDefaults.TWODFX_SWAYABLE_HIGH_WIND_AMPLITUDE);

            Xml.CreateElementWithValueAttribute(xmlItem, "boneTag", boneid);
            Xml.CreateElementWithValueAttribute(xmlItem, "lowWindSpeed", lwspeed);
            Xml.CreateElementWithValueAttribute(xmlItem, "lowWindAmplitude", lwampl);
            Xml.CreateElementWithValueAttribute(xmlItem, "highWindSpeed", hwspeed);
            Xml.CreateElementWithValueAttribute(xmlItem, "highWindAmplitude", hwampl);

            return (xmlItem);
        }

        /// <summary>
        /// Serialise a CExtensionDefProcObject object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        protected static XElement AsCExtensionDefProcObject(Extension extension)
        {
            XElement xmlItem = AsCExtensionDefCommon(extension);
            xmlItem.SetAttributeValue("type", "CExtensionDefProcObject");

            Matrix34f mtxParent = new Matrix34f();
            if (extension.Object.HasParent())
                mtxParent = extension.Object.Parent.NodeTransform.Inverse();
            Matrix34f mtxLocal = extension.Object.NodeTransform * mtxParent;
            Quaternionf rot = new Quaternionf(mtxLocal);
            Xml.CreateElementWithVectorAttributes(xmlItem, "offsetRotation", rot);

            String id = extension.Object.GetAttribute(AttrNames.TWODFX_PROC_OBJECT, AttrDefaults.TWODFX_PROC_OBJECT);
            UInt32 objid = RSG.ManagedRage.StringHashUtil.atStringHash(id, 0);
            float radius_inner = extension.Object.GetParameter("radiusInner", 0.0f);
            float radius_outer = extension.Object.GetParameter("radiusOuter", 0.0f);
            
            float spacing = extension.Object.GetAttribute(AttrNames.TWODFX_PROC_SPACING, AttrDefaults.TWODFX_PROC_SPACING);
            float minscale = extension.Object.GetAttribute(AttrNames.TWODFX_PROC_MIN_SCALE, AttrDefaults.TWODFX_PROC_MIN_SCALE);
            float maxscale = extension.Object.GetAttribute(AttrNames.TWODFX_PROC_MAX_SCALE, AttrDefaults.TWODFX_PROC_MAX_SCALE);
            float minscalez = extension.Object.GetAttribute(AttrNames.TWODFX_PROC_MIN_SCALE_Z, AttrDefaults.TWODFX_PROC_MIN_SCALE_Z);
            float maxscalez = extension.Object.GetAttribute(AttrNames.TWODFX_PROC_MAX_SCALE_Z, AttrDefaults.TWODFX_PROC_MAX_SCALE_Z);
            float minzoffset = extension.Object.GetAttribute(AttrNames.TWODFX_PROC_MIN_Z_OFFSET, AttrDefaults.TWODFX_PROC_MIN_Z_OFFSET);
            float maxzoffset = extension.Object.GetAttribute(AttrNames.TWODFX_PROC_MAX_Z_OFFSET, AttrDefaults.TWODFX_PROC_MAX_Z_OFFSET);
            uint flags = Flags.GetProcObjectFlags(extension.Object);

            Xml.CreateElementWithValueAttribute(xmlItem, "radiusInner", radius_inner);
            Xml.CreateElementWithValueAttribute(xmlItem, "radiusOuter", radius_outer);
            Xml.CreateElementWithValueAttribute(xmlItem, "spacing", spacing);
            Xml.CreateElementWithValueAttribute(xmlItem, "minScale", minscale);
            Xml.CreateElementWithValueAttribute(xmlItem, "maxScale", maxscale);
            Xml.CreateElementWithValueAttribute(xmlItem, "minScaleZ", minscalez);
            Xml.CreateElementWithValueAttribute(xmlItem, "maxScaleZ", maxscalez);
            Xml.CreateElementWithValueAttribute(xmlItem, "minZOffset", minzoffset);
            Xml.CreateElementWithValueAttribute(xmlItem, "maxZOffset", maxzoffset);
            Xml.CreateElementWithValueAttribute(xmlItem, "objectHash", objid);
            Xml.CreateElementWithValueAttribute(xmlItem, "flags", flags);

            return (xmlItem);
        }

        /// <summary>
        /// Serialise a CExtensionDefWalkDontWalk object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        protected static XElement AsCExtensionDefWalkDontWalk(Extension extension)
        {
            XElement xmlItem = AsCExtensionDefCommon(extension);
            xmlItem.SetAttributeValue("type", "CExtensionDefWalkDontWalk");

            return (xmlItem);
        }

        /// <summary>
        /// Serialise a CExtensionDefScriptChild object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        protected static XElement AsCExtensionDefScriptChild(Extension extension)
        {
            XElement xmlItem = new XElement("Item");
            xmlItem.Add(new XAttribute("type", "CExtensionDefScriptChild"));

            Matrix34f mtxParent = extension.Object.Parent.NodeTransform.Inverse();
            Matrix34f mtxLocal = extension.Object.NodeTransform * mtxParent;
            Vector3f eulers = mtxLocal.ToEulers();

            Xml.CreateElementWithVectorAttributes(xmlItem, "position", mtxLocal.Translation);
            Xml.CreateElementWithValueAttribute(xmlItem, "rotationZ", eulers.Z);

            return (xmlItem);
        }

        /// <summary>
        /// Serialise a CExtensionDefScript object for XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        protected static XElement AsCExtensionDefScript(Extension extension)
        {
            XElement xmlItem = AsCExtensionDefCommon(extension);
            xmlItem.SetAttributeValue("type", "CExtensionDefScript");

            String scriptName = extension.Object.GetAttribute(AttrNames.SCRIPT_NAME, AttrDefaults.SCRIPT_NAME);
            Xml.CreateElementWithText(xmlItem, "scriptName", scriptName);

            XElement xmlChildren = new XElement("children");
            xmlItem.Add(xmlChildren);
            List<TargetObjectDef> scriptChildren = new List<TargetObjectDef>();
            foreach (TargetObjectDef obj in extension.Object.Children)
            {
                if (!obj.Is2dfxScript())
                    continue;

                XElement xmlChild = AsCExtensionDefScriptChild(new Extension(obj, extension.Object));
                xmlChildren.Add(xmlChild);
            }

            return (xmlItem);
        }

        /// <summary>
        /// Serialise a CExtensionDefLadder object for XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        protected static XElement AsCExtensionDefLadder(Extension extension)
        {
            XElement xmlItem = AsCExtensionDefCommon(extension);
            xmlItem.SetAttributeValue("type", "CExtensionDefLadder");

            if (extension.Object.Is2dfxRSLadder())
            {
                float ladderHeight = extension.Object.GetParameter(ParamNames.TWODFX_RSLADDER_HEIGHT, ParamDefaults.TWODFX_RSLADDER_HEIGHT);
                
                Matrix34f localToWorldTransform = extension.Object.NodeTransform.Transpose(); // HACK - Transpose to work around data issue
                Matrix34f parentToWorldTransform = new Matrix34f();
                Matrix34f worldToParentTransform = new Matrix34f();
                if (extension.Object.HasParent())
                {
                    parentToWorldTransform = extension.Object.Parent.NodeTransform.Transpose(); // HACK - Transpose to work around data issue
                    worldToParentTransform = parentToWorldTransform.Inverse();
                }
                Matrix34f localToParentTransform = localToWorldTransform * worldToParentTransform;
                Quaternionf parentSpaceOrientation = new Quaternionf(localToParentTransform);

                // Ladder pivot/pos is the top point of the RSLadder
                Vector3f nodeTranslation = new Vector3f(extension.Object.NodeTransform.Translation);                
                Vector3f bottomPos = new Vector3f(nodeTranslation);
                bottomPos.Z += extension.Object.GetParameter(ParamNames.TWODFX_RSLADDER_BOTTOM_NODE_Z_OFFSET, ParamDefaults.TWODFX_RSLADDER_BOTTOM_NODE_Z_OFFSET);
                Vector3f top = worldToParentTransform * nodeTranslation;
                Vector3f bottom = worldToParentTransform * bottomPos;

                Vector3f normal = new Vector3f(0.0f, -1.0f, 0.0f);
                Matrix34f mtxOrientation = new Matrix34f(parentSpaceOrientation);
                mtxOrientation.Transform(normal);
                normal.Normalise();

                String template = extension.Object.GetParameter(ParamNames.TWODFX_RSLADDER_TEMPLATE, ParamDefaults.TWODFX_RSLADDER_TEMPLATE);
                bool canGetOffBottom = extension.Object.GetAttribute(AttrNames.TWODFX_RSLADDER_CAN_GET_OFF_AT_BOTTOM, AttrDefaults.TWODFX_RSLADDER_CAN_GET_OFF_AT_BOTTOM);
                bool canGetOffTop = extension.Object.GetAttribute(AttrNames.TWODFX_RSLADDER_CAN_GET_OFF_AT_TOP, AttrDefaults.TWODFX_RSLADDER_CAN_GET_OFF_AT_TOP);

                // Ladder material type
                int materialTypeInt = extension.Object.GetParameter(ParamNames.TWODFX_RSLADDER_MATERIAL_TYPE, ParamDefaults.TWODFX_RSLADDER_MATERIAL_TYPE);
                LadderMaterialTypes materialType = (LadderMaterialTypes)materialTypeInt;

                Xml.CreateElementWithVectorAttributes(xmlItem, "bottom", bottom);
                Xml.CreateElementWithVectorAttributes(xmlItem, "top", top);
                Xml.CreateElementWithVectorAttributes(xmlItem, "normal", normal);
                Xml.CreateElementWithText(xmlItem, "template", template);
                Xml.CreateElementWithValueAttribute(xmlItem, "canGetOffAtTop", canGetOffTop);
                Xml.CreateElementWithValueAttribute(xmlItem, "canGetOffAtBottom", canGetOffBottom);
                Xml.CreateElementWithText(xmlItem, "materialType", materialType.ToString());             
            }
            // Old ladder helper
            else
            {
                Matrix34f mtxParent = new Matrix34f();
                Vector3f parentTranslation = new Vector3f();
                if (extension.Object.HasParent())
                {
                    mtxParent = new Matrix34f(extension.Object.Parent.NodeTransform);
                    parentTranslation = new Vector3f(mtxParent.Translation);
                    mtxParent.Translation = new Vector3f();
                }

                bool canGetOffTop = extension.Object.GetAttribute(AttrNames.TWODFX_LADDER_CAN_GET_OFF_AT_TOP, AttrDefaults.TWODFX_LADDER_CAN_GET_OFF_AT_TOP);
                Vector3f bottom = new Vector3f(extension.Object.SubObjects[Subobjects.TWODFX_LADDER_BOTTOM].NodeTransform.Translation);
                Vector3f top = new Vector3f(extension.Object.SubObjects[Subobjects.TWODFX_LADDER_TOP].NodeTransform.Translation);
                Vector3f normal = new Vector3f(extension.Object.SubObjects[Subobjects.TWODFX_LADDER_NORMAL].NodeTransform.Translation);
                bottom -= parentTranslation;
                top -= parentTranslation;
                normal -= parentTranslation;
                mtxParent.Transform(bottom);
                mtxParent.Transform(top);
                mtxParent.Transform(normal);
                normal -= bottom;
                normal.Normalise();

                Xml.CreateElementWithVectorAttributes(xmlItem, "bottom", bottom);
                Xml.CreateElementWithVectorAttributes(xmlItem, "top", top);
                Xml.CreateElementWithVectorAttributes(xmlItem, "normal", normal);
                Xml.CreateElementWithValueAttribute(xmlItem, "canGetOffAtTop", canGetOffTop);
            }
            return (xmlItem);
        }

        /// <summary>
        /// Serialise a CExtensionDefWindDisturbance object for XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        protected static XElement AsCExtensionDefWindDisturbance(Extension extension)
        {
            XElement xmlItem = AsCExtensionDefCommon(extension, false);
            xmlItem.SetAttributeValue("type", "CExtensionDefWindDisturbance");

            Matrix34f localToWorldTransform = extension.Object.NodeTransform.Transpose();// HACK - Transpose to work around data issue

            Matrix34f parentToWorldTransform = new Matrix34f();
            Matrix34f worldToParentTransform = new Matrix34f();
            if (extension.Object.HasParent())
            {
                parentToWorldTransform = extension.Object.Parent.NodeTransform.Transpose();// HACK - Transpose to work around data issue
                worldToParentTransform = parentToWorldTransform.Inverse();
            }

            Matrix34f localToParentTransform = localToWorldTransform * worldToParentTransform;

            Quaternionf parentSpaceOrientation = new Quaternionf(localToParentTransform);
            Xml.CreateElementWithVectorAttributes(xmlItem, "offsetPosition", localToParentTransform.Translation);
            Xml.CreateElementWithVectorAttributes(xmlItem, "offsetRotation", parentSpaceOrientation);

            int windType = extension.Object.GetAttribute(AttrNames.TWODFX_WIND_TYPE, AttrDefaults.TWODFX_WIND_TYPE);
            Xml.CreateElementWithValueAttribute(xmlItem, "disturbanceType", windType);

            int boneid;
            bool attach_all = extension.Object.GetAttribute(AttrNames.TWODFX_WIND_ATTACH_TO_ALL, AttrDefaults.TWODFX_WIND_ATTACH_TO_ALL);
            if (attach_all)
                boneid = -1;
            else
                boneid = extension.Object.GetAttribute(AttrNames.TWODFX_WIND_ATTACH, AttrDefaults.TWODFX_WIND_ATTACH);
            Xml.CreateElementWithValueAttribute(xmlItem, "boneTag", boneid);

            Vector4f size = new Vector4f();
            if ( windType == (int)RsWindDisturbance.Hemisphere ) 
            {
                size.X = extension.Object.GetParameter(ParamNames.TWODFX_WIND_SIZE_RADIUS, ParamDefaults.TWODFX_WIND_SIZE_RADIUS);
            }
            Xml.CreateElementWithVectorAttributes(xmlItem, "size", size);

            float strength = extension.Object.GetAttribute(AttrNames.TWODFX_WIND_STRENGTH, AttrDefaults.TWODFX_WIND_STRENGTH);
            Xml.CreateElementWithValueAttribute(xmlItem, "strength", strength);

            UInt32 flags = Flags.GetWindDisturbanceFlags(extension.Object);
            Xml.CreateElementWithValueAttribute(xmlItem, "flags", flags);

            return (xmlItem);
        }

        /// <summary>
        /// Serialise a CExtensionDefDoor object for XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        protected static XElement AsCExtensionDefDoor(Extension extension)
        {
            XElement xmlItem = AsCExtensionDefCommon(extension);
            xmlItem.SetAttributeValue("type", "CExtensionDefDoor");

            Matrix34f mtxParent = new Matrix34f();
            if (extension.Object.HasParent())
                mtxParent = extension.Object.Parent.NodeTransform.Inverse();
            Matrix34f mtxLocal = extension.Object.NodeTransform * mtxParent;
            Quaternionf rot = new Quaternionf(mtxLocal);
            Xml.CreateElementWithVectorAttributes(xmlItem, "offsetRotation", rot);

            bool enableLimitAngle = extension.Object.GetAttribute(AttrNames.TWODFX_DOOR_ENABLE_LIMIT_ANGLE, AttrDefaults.TWODFX_DOOR_ENABLE_LIMIT_ANGLE);
            bool startsLocked = extension.Object.GetAttribute(AttrNames.TWODFX_DOOR_STARTS_LOCKED, AttrDefaults.TWODFX_DOOR_STARTS_LOCKED);
            bool canBreak = extension.Object.GetAttribute(AttrNames.TWODFX_DOOR_CAN_BREAK, AttrDefaults.TWODFX_DOOR_CAN_BREAK);
            float limitAngle = extension.Object.GetAttribute(AttrNames.TWODFX_DOOR_LIMIT_ANGLE, AttrDefaults.TWODFX_DOOR_LIMIT_ANGLE);
            float doorTargetRatio = extension.Object.GetAttribute(AttrNames.TWODFX_DOOR_DOOR_TARGET_RATIO, AttrDefaults.TWODFX_DOOR_DOOR_TARGET_RATIO);

            float radiansPerDegree = (float)(1.0d / (180.0d / Math.PI));
            Xml.CreateElementWithValueAttribute(xmlItem, "enableLimitAngle", enableLimitAngle);
            Xml.CreateElementWithValueAttribute(xmlItem, "startsLocked", startsLocked);
            Xml.CreateElementWithValueAttribute(xmlItem, "canBreak", canBreak);
            Xml.CreateElementWithValueAttribute(xmlItem, "limitAngle", limitAngle * radiansPerDegree);
            Xml.CreateElementWithValueAttribute(xmlItem, "doorTargetRatio", doorTargetRatio * radiansPerDegree);

            return xmlItem;
        }

        //AsCExtensionDefExpression

        /// <summary>
        /// Serialise a CExtensionDefExpression object for XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="extension"></param>
        /// <returns></returns>
        protected static XElement AsCExtensionDefExpression(Extension extension)
        {
            XElement xmlItem = AsCExtensionDefCommon(extension);
            xmlItem.SetAttributeValue("type", "CExtensionDefExpression");

            Matrix34f mtxParent = new Matrix34f();
            if (extension.Object.HasParent())
                mtxParent = extension.Object.Parent.NodeTransform.Inverse();
            Matrix34f mtxLocal = extension.Object.NodeTransform * mtxParent;
            Quaternionf rot = new Quaternionf(mtxLocal);
            Xml.CreateElementWithVectorAttributes(xmlItem, "offsetRotation", rot);

            string expressionDictionaryName = extension.Object.GetAttribute(AttrNames.TWODFX_EXPRESSION_DICTIONARY_NAME, AttrDefaults.TWODFX_EXPRESSION_DICTIONARY_NAME);
            string expressionName = extension.Object.GetAttribute(AttrNames.TWODFX_EXPRESSION_NAME, AttrDefaults.TWODFX_EXPRESSION_NAME);
            string creatureMetadataName = extension.Object.GetAttribute(AttrNames.TWODFX_EXPRESSION_CREATURE_METADATA_NAME, AttrDefaults.TWODFX_EXPRESSION_CREATURE_METADATA_NAME);
            bool initialiseOnCollision = extension.Object.GetAttribute(AttrNames.TWODFX_EXPRESSION_INIT_ON_COLLISION, AttrDefaults.TWODFX_EXPRESSION_INIT_ON_COLLISION);

            Xml.CreateElementWithText(xmlItem, "expressionDictionaryName", expressionDictionaryName);
            Xml.CreateElementWithText(xmlItem, "expressionName", expressionName);
            Xml.CreateElementWithText(xmlItem, "creatureMetadataName", creatureMetadataName);
            Xml.CreateElementWithValueAttribute(xmlItem, "initialiseOnCollision", initialiseOnCollision);
            return xmlItem;
        }

        #endregion // Extension Serialisation Methods

        #region MILO/Interior Serialisation Methods
        /// <summary>
        /// Serialise a CMloPortalDef object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="parent"></param>
        /// <param name="portal"></param>
        /// <returns></returns>
        internal static XElement AsCMloPortalDef(XElement parent, MiloPortal portal, String[] room_list, List<TargetObjectDef> entries)
        {
            Scene scene = portal.Portal.MyScene;
            List<String> roomlist = new List<String>(room_list);
            int roomAidx = 0;
            int roomBidx = 0;
            if (null != portal.RoomA)
                roomAidx = roomlist.IndexOf(portal.RoomA.Name) + 1; // +1 for limbo
            if (null != portal.RoomB)
                roomBidx = roomlist.IndexOf(portal.RoomB.Name) + 1; // +1 for limbo

            int portalMirrorTypeValue = portal.Portal.GetAttribute(AttrNames.MLOPORTAL_MIRROR_TYPE, AttrDefaults.MLOPORTAL_MIRROR_TYPE);
            int portalMirrorPriority = portal.Portal.GetAttribute(AttrNames.MLOPORTAL_MIRROR_PRIORITY, AttrDefaults.MLOPORTAL_MIRROR_PRIORITY);
            Flags.MLOPortalMirrorType portalMirrorType = (Flags.MLOPortalMirrorType)portalMirrorTypeValue;
            Debug.Assert(portalMirrorType >= Flags.MLOPortalMirrorType.NONE && portalMirrorType < Flags.MLOPortalMirrorType.COUNT);
            
            UInt32 flags = Flags.GetMloPortalFlags(portal.Portal);
            Object[] attachedGuids = portal.Portal.GetParameter(ParamNames.MLOPORTAL_ATTACHED_OBJECTS,
                ParamDefaults.MLOPORTAL_ATTACHED_OBJECTS);
            TargetObjectDef[] attached = new TargetObjectDef[attachedGuids.Length];
            String audioOcclusion = portal.Portal.GetParameter(ParamNames.MLOPORTAL_AUDIO_OCCL_HASH, ParamDefaults.MLOPORTAL_AUDIO_OCCL_HASH);
            uint audioOcclusionHash = (uint)RSG.ManagedRage.StringHashUtil.atStringHash(audioOcclusion, 0);
            int n = 0;
            foreach (Object o in attachedGuids)
            {
                if (o is Guid)
                    attached[n] = scene.FindObject((Guid)o);
                ++n;
            }

            // Portal NodeTransform (mirrors have a 10cm offset unless they are mirrors).
            float offset = 0.1f;
            if (portalMirrorType != Flags.MLOPortalMirrorType.NONE)
                offset = 0.0f;

            float lengthdiv2 = portal.Portal.GetParameter(ParamNames.MLOPORTAL_LENGTH, ParamDefaults.MLOPORTAL_LENGTH) / 2.0f;
            float widthdiv2 = portal.Portal.GetParameter(ParamNames.MLOPORTAL_WIDTH, ParamDefaults.MLOPORTAL_WIDTH) / 2.0f;
            Matrix34f parentMtx = portal.Portal.Parent.NodeTransform.Inverse();
            Quaternionf nodeRot = new Quaternionf(portal.Portal.NodeTransform);
            // DHM 2009/04/29 -- Milo entry and portal rotations are inverted for some reason.
            nodeRot.Invert();

            Matrix34f nodeMtx = new Matrix34f(nodeRot);
            nodeMtx.Translation = portal.Portal.NodeTransform.Translation;
            nodeMtx *= parentMtx;
            Vector3f a = nodeMtx * (new Vector3f(widthdiv2 + offset, -lengthdiv2 - offset, 0.0f));
            Vector3f b = nodeMtx * (new Vector3f(widthdiv2 + offset, lengthdiv2 + offset, 0.0f));
            Vector3f c = nodeMtx * (new Vector3f(-widthdiv2 - offset, lengthdiv2 + offset, 0.0f));
            Vector3f d = nodeMtx * (new Vector3f(-widthdiv2 - offset, -lengthdiv2 - offset, 0.0f));

            float opacityPercentage = portal.Portal.GetAttribute(AttrNames.MLOPORTAL_OPACITY, AttrDefaults.MLOPORTAL_OPACITY);
            byte opacityAsByte = (byte)(opacityPercentage * 255.0f / 100.0f);

            XElement xmlItem = new XElement("Item");
            xmlItem.Add(new XAttribute("type", "CMloPortalDef"));

            Xml.CreateElementWithValueAttribute(xmlItem, "flags", flags.ToString("d"));
            Xml.CreateElementWithValueAttribute(xmlItem, "roomFrom", roomAidx);
            Xml.CreateElementWithValueAttribute(xmlItem, "roomTo", roomBidx);
            Xml.CreateElementWithValueAttribute(xmlItem, "opacity", opacityAsByte);
            Xml.CreateElementWithValueAttribute(xmlItem, "audioOcclusion", audioOcclusionHash);
            Xml.CreateElementWithValueAttribute(xmlItem, "mirrorPriority", portalMirrorPriority);
            XElement xmlCorners = new XElement("corners");
            xmlItem.Add(xmlCorners);
            Xml.CreateElementWithVectorAttributes(xmlCorners, "Item", a);
            Xml.CreateElementWithVectorAttributes(xmlCorners, "Item", b);
            Xml.CreateElementWithVectorAttributes(xmlCorners, "Item", c);
            Xml.CreateElementWithVectorAttributes(xmlCorners, "Item", d);

            // Attached objects (4)
            System.Text.StringBuilder attached_builder = new System.Text.StringBuilder();
            for (n = 0; n < attached.Length; ++n)
            {
                int index = entries.IndexOf(attached[n]);
                if (index > -1)
                    attached_builder.AppendFormat("{0} ", index);
                // B* 1044004 - we used to error here, but that caused invalid errors for portal objects in entity sets
            }
            XElement xmlAttachedObjects = Xml.CreateElementWithText(xmlItem, "attachedObjects", attached_builder.ToString());
            xmlAttachedObjects.Add(new XAttribute("content", "int_array"));

            return (xmlItem);
        }

        /// <summary>
        /// Serialise a CMloRoomDef object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="parent"></param>
        /// <param name="room"></param>
        /// <returns></returns>
        internal static XElement AsCMloRoomDef(XElement parent, MiloRoom room, List<TargetObjectDef> entries)
        {
            BoundingBox3f bbox = room.CalcRoomBounds();
            float blend = room.Room.GetAttribute(AttrNames.MLOROOM_BLEND, AttrDefaults.MLOROOM_BLEND);
            String timecycle = room.Room.GetAttribute(AttrNames.MLOROOM_TIMECYCLE, AttrDefaults.MLOROOM_TIMECYCLE);
            String secondaryTimecycle = room.Room.GetAttribute(AttrNames.MLOROOM_SECONDARY_TIMECYCLE, AttrDefaults.MLOROOM_SECONDARY_TIMECYCLE);
            uint hTimecycle = 0;
            UInt32 flags = Flags.GetMloRoomFlags(room.Room);
            if (0 != timecycle.CompareTo(AttrDefaults.MLOROOM_TIMECYCLE))
                hTimecycle = (uint)RSG.ManagedRage.StringHashUtil.atStringHash(timecycle, 0);
            int floorId = room.Room.GetAttribute(AttrNames.MLOROOM_ROOM_FLOOR, AttrDefaults.MLOROOM_ROOM_FLOOR);
            int depth = room.Room.GetAttribute(AttrNames.MLOROOM_DEPTH, AttrDefaults.MLOROOM_DEPTH);

            XElement xmlItem = new XElement("Item");
            xmlItem.Add(new XAttribute("type", "CMloRoomDef"));

            Xml.CreateElementWithText(xmlItem, "name", room.Room.GetObjectName());
            Xml.CreateElementWithVectorAttributes(xmlItem, "bbMin", bbox.Min);
            Xml.CreateElementWithVectorAttributes(xmlItem, "bbMax", bbox.Max);
            Xml.CreateElementWithValueAttribute(xmlItem, "blend", blend);
            Xml.CreateElementWithValueAttribute(xmlItem, "timecycle", hTimecycle);
            Xml.CreateElementWithText(xmlItem, "timecycleName", timecycle);
            Xml.CreateElementWithText(xmlItem, "secondaryTimecycleName", secondaryTimecycle);
            Xml.CreateElementWithValueAttribute(xmlItem, "flags", flags);
            Xml.CreateElementWithValueAttribute(xmlItem, "portalCount", room.Portals.Count);
            Xml.CreateElementWithValueAttribute(xmlItem, "floorId", floorId);
            Xml.CreateElementWithValueAttribute(xmlItem, "depth", depth);

            // Loop through our children outputting each entry index.
            // Pretty formatting with MLOROOM_NUM_ENTRY per line.
            System.Text.StringBuilder entries_builder = new System.Text.StringBuilder();
            foreach (TargetObjectDef child in room.Children)
            {
                entries_builder.AppendFormat("{0}{1}",
                    entries.IndexOf(child), Environment.NewLine);
            }
            XElement xmlAttachedObjects = Xml.CreateElementWithText(xmlItem, "attachedObjects", entries_builder.ToString());
            xmlAttachedObjects.Add(new XAttribute("content", "int_array"));

            return (xmlItem);
        }
        #endregion // MILO/Interior Serialisation Methods

        #region StatedAnim Serialisation Methods
        #region Nested Effect Methods
        /// <summary>
        /// Return IDE String representation for a 2dfx object.
        /// </summary>
        /// <returns></returns>
        /// The format varies depending on the 2dfx type, although most have a 
        /// common prefix which is specified in ToIDE2dfxCommon().
        public XElement ToMETA2dfx(XElement parent, TargetObjectDef obj, TargetObjectDef rootObject)
        {
            if (obj.Is2dfxParticleEffect())
                return (ToMETA2dfxParticleEffect(parent, obj, rootObject));
            
            Debug.Assert(false, "Unhandled 2dfx type.");
            Log.Log__Error("Unhandled 2dfx type: {0}.", obj.Name);
            return (null);
        }

        /// <summary>
        /// Return common META String prefix representation for most 2dfx objects.
        /// </summary>
        /// <returns></returns>
        /// Format of the common prefix is:
        ///   name, x, y, z, type_id, qx, qy, qz, -qw, **2dfx-specific**
        ///         <fxName>RsParticleHelper001</fxName>
        ///         <fxOffsetPos x="0.0" y="0.0" z="0.0"/>
        ///         <fxType value = "0" />
        ///         <fxOffsetRot x="0.0" y="0.0" z="0.0" w="1.0"/>
        ///
        private XElement ToMETA2dfxCommon(TargetObjectDef obj, int type)
        {
            Debug.Assert(obj.Is2dfx(),
                String.Format("Internal error: object {0} is not a 2dfx.", obj.Name));

            Matrix34f localToWorldTransform = obj.NodeTransform.Transpose();// HACK - Transpose to work around data issue

            Matrix34f parentToWorldTransform = new Matrix34f();
            Matrix34f worldToParentTransform = new Matrix34f();
            if (obj.HasParent())
            {
                parentToWorldTransform = obj.Parent.NodeTransform.Transpose();// HACK - Transpose to work around data issue
                worldToParentTransform = parentToWorldTransform.Inverse();
            }

            Matrix34f localToParentTransform = localToWorldTransform * worldToParentTransform;

            Quaternionf parentSpaceOrientation = new Quaternionf(localToParentTransform);

            XElement xmlElem = new XElement("Item");
            Xml.CreateElementWithText(xmlElem, "fxObjName", obj.Parent.Name);
            Xml.CreateElementWithVectorAttributes(xmlElem, "fxOffsetPos", localToParentTransform.D);
            Xml.CreateElementWithValueAttribute(xmlElem, "fxType", type);
            Xml.CreateElementWithVectorAttributes(xmlElem, "fxOffsetRot", parentSpaceOrientation);

            return (xmlElem);
        }

        /// <summary>
        /// Return XmlNode representation of an "RAGE Particle" 2dfx.
        /// </summary>
        /// <returns></returns>
        /// Format of a "RAGE Particle" effect is:
        ///       <Item>
        ///         ... (common values)
        ///         <isTriggered value = "true" />
        ///         <boneTag value ="55988" />
        ///         <startPhase value = "0.0" />
        ///         <endPhase value = "0.0" />
        ///         <ptFxTag>RAY_TEST_TRIGGER</ptFxTag>
        ///         <ptFxScale value ="1.0"/>
        ///         <ptFxProbability value ="100.0"/>
        ///         <ptFxHasTint value ="0"/>
        ///         <ptFxTintR value ="0"/>
        ///         <ptFxTintG value ="0"/>
        ///         <ptFxTintB value ="0"/>
        ///         <sizeX value ="0"/>
        ///         <sizeY value ="0"/>
        ///         <sizeZ value ="0"/>
        ///       </Item>
        ///   
        private XElement ToMETA2dfxParticleEffect(XElement parent, TargetObjectDef obj, TargetObjectDef rootObj)
        {
            if (obj.GetAttribute(AttrNames.TWODFX_PARTICLE_DONTEXPORT, AttrDefaults.TWODFX_PARTICLE_DONTEXPORT))
                return null;

            String tag = obj.GetAttribute(AttrNames.TWODFX_PARTICLE_NAME,
                 AttrDefaults.TWODFX_PARTICLE_NAME);
            bool isTriggered = obj.GetAttribute(AttrNames.TWODFX_PARTICLE_IS_TRIGGERED,
                AttrDefaults.TWODFX_PARTICLE_IS_TRIGGERED);
            int boneid = obj.GetAttribute(AttrNames.TWODFX_PARTICLE_ATTACH,
                AttrDefaults.TWODFX_PARTICLE_ATTACH);
            float prob = obj.GetAttribute(AttrNames.TWODFX_PARTICLE_PROBABILITY,
                AttrDefaults.TWODFX_PARTICLE_PROBABILITY);
            float scale = obj.GetAttribute(AttrNames.TWODFX_PARTICLE_SCALE,
                AttrDefaults.TWODFX_PARTICLE_SCALE);
            UInt32 flags = Flags.GetParticleFlags(obj);
            int r = obj.GetAttribute(AttrNames.TWODFX_PARTICLE_COLOUR_R,
                AttrDefaults.TWODFX_PARTICLE_COLOUR_R);
            int g = obj.GetAttribute(AttrNames.TWODFX_PARTICLE_COLOUR_G,
                AttrDefaults.TWODFX_PARTICLE_COLOUR_G);
            int b = obj.GetAttribute(AttrNames.TWODFX_PARTICLE_COLOUR_B,
                AttrDefaults.TWODFX_PARTICLE_COLOUR_B);
            float size_x = obj.GetAttribute(AttrNames.TWODFX_PARTICLE_SIZE_X,
                AttrDefaults.TWODFX_PARTICLE_SIZE_X);
            float size_y = obj.GetAttribute(AttrNames.TWODFX_PARTICLE_SIZE_Y,
                AttrDefaults.TWODFX_PARTICLE_SIZE_Y);
            float size_z = obj.GetAttribute(AttrNames.TWODFX_PARTICLE_SIZE_Z,
                AttrDefaults.TWODFX_PARTICLE_SIZE_Z);
            bool hasTint = (r != AttrDefaults.TWODFX_PARTICLE_COLOUR_R) || 
                (g != AttrDefaults.TWODFX_PARTICLE_COLOUR_G) || 
                (b != AttrDefaults.TWODFX_PARTICLE_COLOUR_B);


            float relativeStartTime = 0.0f;
            float relativeStopTime = 1.0f;
            List<RSG.SceneXml.sKeyFrame> ActiveKeys;
            if (obj.KeyedProperties.TryGetValue("active", out ActiveKeys))
            {
                if (ActiveKeys.Count > 0)
                {
                    int objectAnimLength = rootObj.AnimLength;
                    int firstStartTime = 999999;
                    int lastStopTime = 0;
                    foreach (sKeyFrame keyframe in ActiveKeys)
                    {
                        if (keyframe.m_nState == 1 && keyframe.m_nTime < firstStartTime)
                            firstStartTime = keyframe.m_nTime;
                        if (keyframe.m_nState == 0 && keyframe.m_nTime > lastStopTime)
                            lastStopTime = keyframe.m_nTime;
                    }
                    relativeStartTime = ((float)firstStartTime) / ((float)objectAnimLength);
                    relativeStopTime = ((float)lastStopTime) / ((float)objectAnimLength);
                    if ((relativeStopTime > 1.0f) ||
                        (relativeStopTime < relativeStartTime))
                    {
                        relativeStopTime = 1.0f;
                    }
                }
            }
            else
            {
                Log.Log__MessageCtx(obj.Name, "Object {0} doesn't have \"active\" keyframes.", obj.Name);
            }

            // output common data
            XElement rootNode = ToMETA2dfxCommon(obj, META2dfxTypes.TWODFX_PARTICLE_EFFECT);

            Xml.CreateElementWithValueAttribute(rootNode, "boneTag", boneid);
            Xml.CreateElementWithValueAttribute(rootNode, "startPhase", relativeStartTime);
            Xml.CreateElementWithValueAttribute(rootNode, "endPhase", relativeStopTime);
            Xml.CreateElementWithValueAttribute(rootNode, "ptFxIsTriggered", isTriggered);
            Xml.CreateElementWithText(rootNode, "ptFxTag", tag);
            Xml.CreateElementWithValueAttribute(rootNode, "ptFxScale", scale);
            Xml.CreateElementWithValueAttribute(rootNode, "ptFxProbability", prob);
            Xml.CreateElementWithValueAttribute(rootNode, "ptFxHasTint", hasTint);
            Xml.CreateElementWithValueAttribute(rootNode, "ptFxTintR", r);
            Xml.CreateElementWithValueAttribute(rootNode, "ptFxTintG", g);
            Xml.CreateElementWithValueAttribute(rootNode, "ptFxTintB", b);
            Xml.CreateElementWithVectorAttributes(rootNode, "ptFxSize",
                new Vector3f(size_x, size_y, size_z));

            return rootNode;
        }
        #endregion // Nested Effect Methods

        #region Helper methods
        /// <summary>
        /// Process a single SceneXml ObjectDef for 2DFX objects.
        /// </summary>
        /// <param name="obj"></param>
        private static void ProcessScene2DFX(Scene scene, List<Definition> l2dfx, TargetObjectDef obj, bool isSkeleton = false)
        {
            if (!isSkeleton && (obj.DontExport() || obj.DontExportIDE() || !obj.IsActive()))
                return;

            if (obj.Is2dfx() && obj.HasParent())
                l2dfx.Add(new Definition(scene, obj));

            // Children (for 2DFX entries)
            foreach (TargetObjectDef o in obj.Children)
                ProcessScene2DFX(scene, l2dfx, o, isSkeleton);
        }
        #endregion // Helper Methods
        #endregion // StatedAnim Serialisation Methods

        #region TXD Relationship Serialisation Methods
        /// <summary>
        /// Serialise a CTxdRelationship object to XML.
        /// </summary>
        /// <param name="relationship"></param>
        /// <returns></returns>
        protected XElement AsCTxdRelationship(KeyValuePair<String, String> relationship)
        {
            XElement relationshipElem = new XElement("Item");

            XElement parentElem = new XElement("parent");
            parentElem.Add(new XText(relationship.Value));

            XElement childElem = new XElement("child");
            childElem.Add(new XText(relationship.Key));

            relationshipElem.Add(parentElem);
            relationshipElem.Add(childElem);

            return (relationshipElem);
        }
        #endregion // TXD Relationship Serialisation Methods

        #region Manifest Serialisation Methods
        /// <summary>
        /// Serialise ITYP dependencies.
        /// </summary>
        /// <param name="container"></param>
        /// <param name="forceDependency">For props merging, allow to filter out old props reference and force the merged one instead.</param>
        public static XElement AsManifestITYPDependency2(ITYPContainer container, IEnumerable<string> forceDependency = null)
        {
            bool isInterior = (container.Archetypes.Any(archetype => archetype.Object.IsMilo()));
            bool hasDependencies = (container.Dependencies.Length > 0);

            if (isInterior || hasDependencies)
            {
                XElement xmlDep2 = new XElement("ITYPDependency2");

                xmlDep2.Add(new XAttribute("isInterior", isInterior.ToString()));
                xmlDep2.Add(new XAttribute("itypName", container.Name));

                if (hasDependencies)
                {
                    List<String> sortedDependencies = container.Dependencies.Where(dependency => !string.IsNullOrWhiteSpace(dependency)).ToList();

                    // adding the merged prop dependency in the dependency list
                    if (forceDependency != null && forceDependency.Any())
                    {
                        sortedDependencies.AddRange(forceDependency);
                    }

                    sortedDependencies.Sort();
                    xmlDep2.Add(new XAttribute("itypNames", String.Join(";", sortedDependencies)));
                }
                else
                {
                    xmlDep2.Add(new XAttribute("itypNames", ""));
                }
                
                return (xmlDep2);
            }
            else
            {
                return (null);
            }
        }

        #endregion // Manifest Serialisation Methods
        #endregion // #region Protected Object-Serialisation Methods

        #region Scene and Object Processing Methods
        /// <summary>
        /// Process a single Scene object.
        /// </summary>
        /// <param name="scene"></param>
        protected override void ProcessScene(Scene scene)
        {
            base.ProcessScene(scene);

            // Process for CompositeEntityTypes.
            CompositeEntityType[] compEntityTypes =
                CompositeEntityType.CreateAll(scene, effectCollection);
            this.Container.AddCompositeEntityTypeRange(compEntityTypes);
        }

        protected override void ProcessSceneObject(TargetObjectDef obj)
        {
            // Check to see if we're in a DLC project. If so, only serialize the DLC specific objects.
            // Children check is to ensure we iterate into MLO, MLO Rooms, MLO Portals etc.
            // TODO Flo DLC Markup
            //bool isNewDlcObject = this.Branch.Project.ForceFlags.AllNewContent || obj.IsNewDLCObject();
            //if (this.Branch.Project.IsDLC && !obj.IsContainer() && !isNewDlcObject && (0 == obj.Children.Length))
            //    return;

            if (obj.DontExport() || obj.DontExportIDE() || SceneOverrideIntegrator.Instance.IsObjectMarkedForDeletion(obj))
                return;
            // GunnarD: url:bugstar:99343 Avoid container children getting processed, too.
            // Shadow meshes need same treatment
            if (obj.HasDrawableLODChildren() || obj.HasRenderSimParent() || obj.HasShadowMeshLink())
                return;

            if (obj.IsMiloTri())
            {
                foreach (TargetObjectDef child in obj.Children)
                    ProcessSceneObject(child);
            }
            else if (obj.IsXRef())
                return; // Skipped
            else if (obj.IsRefObject())
                return; // Skipped
            else if (obj.IsInternalRef())
                return; // Skipped
            else if (obj.IsContainerLODRefObject())
                return; // Skipped
            else if (obj.IsRefInternalObject())
                return; // Skipped
            else if (obj.IsBlock())
                return; // Skipped
            else if (obj.Is2dfx())
                return; // Skipped
            else if (obj.IsCarGen())
                return; // Skipped
            else if (obj.IsCollision())
                return; // Skipped
            else if (obj.IsAnimProxy())
                return; // Skipped
            else if (obj.IsIMAPGroupDefinition())
            {
                // Process the linked meshes as if they were root objects.
                foreach (TargetObjectDef child in obj.Children)
                    ProcessSceneObject(child);
            }
            else if (obj.IsObject())
            {
                //// TODO Flo DLC Markup
                //if (this.Branch.Project.IsDLC && !isNewDlcObject)
                //    return; // Ignore non-DLC objects in DLC.

                Archetype archetype = new Archetype(obj);
                AddArchetype(archetype);
                ProcessSceneExtensions(obj, archetype, obj);
            }
            else if (obj.IsMilo())
            {
                //// TODO Flo DLC Markup
                //if (this.Branch.Project.IsDLC && !isNewDlcObject)
                //    return; // Ignore non-DLC objects in DLC.

                if (InteriorContainerMap == null)
                {
                    this.Container.AddArchetype(new Archetype(obj));
                    foreach (TargetObjectDef child in obj.Children)
                        ProcessSceneObject(child);
                }
                else
                {
                    // This is an in-situ interior
                    String containerName = InteriorNameMap.GetContainerNameFromObject(obj);
                    InteriorContainerMap.Add(containerName, CreateITYPContainer());
                    InteriorContainerMap[containerName].AddArchetype(new Archetype(obj));
                    CurrentInteriorITYPContainer = InteriorContainerMap[containerName];
                    foreach (TargetObjectDef child in obj.Children)
                        ProcessSceneObject(child);
                    CurrentInteriorITYPContainer = null;
                }
            }
            else if (obj.IsMloPortal() || obj.IsMloRoom())
            {
                foreach (TargetObjectDef child in obj.Children)
                    ProcessSceneObject(child);
            }
            else if (obj.IsContainer())
            {
                foreach (TargetObjectDef child in obj.Children)
                    ProcessSceneObject(child);
            }
            else if (obj.IsPropGroup())
            {
                foreach (TargetObjectDef child in obj.Children)
                    ProcessSceneObject(child);
            }
            else if (obj.IsIMAPGroupDummy())
                return; // Skipped
            else if (obj.IsDummy())
            {
                Log.Log__Warning("Ignoring Dummy {0} as this is how 3dsmax represents Groups.", obj);
            }
            else if (obj.IsTimeCycleBox() || obj.IsTimeCycleSphere())
                return; // Skipped
            else if (obj.IsEditableSpline() || obj.IsText() || obj.IsLine())
                return; // Skipped
            else if (obj.IsStreamingExtentsOverrideBox())
                return; // Skipped
            else if (obj.IsPointHelper())
                return; // Skipped
            else if (obj.IsCableProxy())
                return; // Skipped
            else
            {
                Log.Log__WarningCtx(obj.Name, "Ignoring {0} {1} as type not recognised by ITYP serialiser.", obj.Name, obj.Class);
            }
        }
        #endregion // Scene and Object Processing Methods

        #region Private Helper Methods
        #region Milo Helper Methods
        /// <summary>
        /// Pre-pass on the MloRoom objects.
        /// </summary>
        /// <param name="archetype"></param>
        /// <param name="rooms"></param>
        /// <param name="room_list"></param>
        /// <param name="limbo"></param>
        /// <param name="entries"></param>
        internal static void MiloPreParseRooms(Archetype archetype, out Dictionary<TargetObjectDef, MiloRoom> rooms,
            out String[] room_list, out TargetObjectDef limbo, out List<Archetype> entries)
        {
            Scene scene = archetype.Object.MyScene;
            List<String> room_list_temp = new List<String>();
            rooms = new Dictionary<TargetObjectDef, MiloRoom>();
            entries = new List<Archetype>();

            // Create limbo room (we always have a limbo room as a container 
            // for objects not parented to a room helper or attached to a portal helper).
            limbo = new TargetObjectDef(LIMBO, "", AttrClass.MLOROOM, scene);
            limbo.NodeTransform = new Matrix34f(archetype.Object.NodeTransform);
            rooms.Add(limbo, new MiloRoom(limbo));

            foreach (TargetObjectDef room in archetype.Object.Children)
            {
                if (room.DontExport())
                    continue;

                if (room.IsMloRoom())
                {
                    // Create room ObjectContainer for its children entries.
                    room_list_temp.Add(room.Name);
                    rooms.Add(room, new MiloRoom(room));

                    // Populate entries from child objects.
                    foreach (TargetObjectDef child in room.Children)
                    {
                        if (!(child.IsXRef() || child.IsObject()))
                            continue; // Skip
                        if (child.DontExport() || child.DontExportIDE() || child.DontExportIPL() || child.HasShadowMeshLink())
                            continue; // Skip

                        rooms[room].Children.Add(child);
                        entries.Add(new Archetype(child));
                    }
                }
                else if (room.IsXRef() || room.IsObject())
                {
                    if (room.DontExportIDE() || room.DontExportIPL() || room.HasShadowMeshLink())
                        continue;
                    if (room.HasLODChildren())
                        continue;

                    // Non-room object as child of Gta Milo, so it goes in limbo.
                    rooms[limbo].Children.Add(room);
                    entries.Add(new Archetype(room));
                }
            }
            room_list = room_list_temp.ToArray();
        }

        /// <summary>
        /// Pre-pass on the MloPortal objects.
        /// </summary>
        /// <param name="archetype"></param>
        /// <param name="limbo"></param>
        /// <param name="rooms"></param>
        /// <param name="portals"></param>
        internal static void MiloPreParsePortals(Archetype archetype, TargetObjectDef limbo, ref Dictionary<TargetObjectDef, MiloRoom> rooms, out Dictionary<TargetObjectDef, MiloPortal> portals)
        {
            Scene scene = archetype.Object.MyScene;
            portals = new Dictionary<TargetObjectDef, MiloPortal>();

            foreach (TargetObjectDef portal in archetype.Object.Children)
            {
                if (!portal.IsMloPortal())
                    continue; // Skip non-portals

                portals.Add(portal, new MiloPortal(portal));

                Guid roomAguid = portal.GetParameter(ParamNames.MLOPORTAL_FIRST_ROOM,
                    ParamDefaults.MLOPORTAL_FIRST_ROOM);
                Guid roomBguid = portal.GetParameter(ParamNames.MLOPORTAL_SECOND_ROOM,
                    ParamDefaults.MLOPORTAL_SECOND_ROOM);
                if (roomAguid != Guid.Empty)
                {
                    TargetObjectDef roomA = scene.FindObject(roomAguid);
                    Debug.Assert(null != roomA, String.Format("Could not find Room A reference for portal ({0}).", roomAguid));
                    portals[portal].RoomA = roomA;
                    rooms[roomA].Portals.Add(portal);
                }
                else
                {
                    rooms[limbo].Portals.Add(portal);
                }

                if (roomBguid != Guid.Empty)
                {
                    TargetObjectDef roomB = scene.FindObject(roomBguid);
                    Debug.Assert(null != roomB, String.Format("Could not find Room B reference for portal ({0}).", roomBguid));
                    portals[portal].RoomB = roomB;
                    rooms[roomB].Portals.Add(portal);
                }
                else
                {
                    rooms[limbo].Portals.Add(portal);
                }

                // Portal children
                foreach (TargetObjectDef child in portal.Children)
                {
                    if (child.DontExport() || child.DontExportIDE() || child.HasShadowMeshLink())
                        continue;

                    portals[portal].Children.Add(child);
                }
            }
        }

        /// <summary>
        /// Post-pass on the Limbo room.
        /// </summary>
        /// This post-pass currently only removes objects previously added to 
        /// Limbo that are attached to Portals.
        /// <param name="archetype"></param>
        /// <param name="limbo"></param>
        /// <param name="portals"></param>
        internal static void MiloPortParseLimbo(Archetype archetype, MiloRoom limbo, Dictionary<TargetObjectDef, MiloPortal> portals)
        {
            Scene scene = archetype.Object.MyScene;
            foreach (KeyValuePair<TargetObjectDef, MiloPortal> portal in portals)
            {
                // Find attached objects.
                Object[] attachedGuids = portal.Value.Portal.GetParameter(ParamNames.MLOPORTAL_ATTACHED_OBJECTS,
                    ParamDefaults.MLOPORTAL_ATTACHED_OBJECTS);
                if (0 == attachedGuids.Length)
                    continue; // Nothing to process here.

                TargetObjectDef[] attached = new TargetObjectDef[attachedGuids.Length];
                foreach (Object o in attachedGuids)
                {
                    if (!(o is Guid))
                        continue;

                    TargetObjectDef limboObj = scene.FindObject((Guid)o);
                    limbo.Children.Remove(limboObj);
                }
            }
        }

        internal static void MiloPortParseEntrySets(Archetype archetype, out Dictionary<string, List<TargetObjectDef>> entrySets, ref List<Archetype> entries)
        {
            Scene scene = archetype.Object.MyScene;
            entrySets = new Dictionary<string, List<TargetObjectDef>>();
            foreach (TargetObjectDef obj in archetype.Object.Children)
            {
                if (obj.IsInteriorGroup())
                {
                    String groupName = obj.GetParameter(ParamNames.OBJECT_GROUP_NAME, ParamDefaults.OBJECT_GROUP_NAME);
                    Object[] groupObjGuids = obj.GetParameter(ParamNames.INTERIOR_GROUP_OBJECTS, ParamDefaults.INTERIOR_GROUP_OBJECTS);
                    foreach (Guid childGuid in groupObjGuids)
                    {
                        TargetObjectDef child = archetype.Object.MyScene.FindObject(childGuid);
                        if(null==child)
                            continue;
                        if (!entrySets.ContainsKey(groupName))
                            entrySets[groupName] = new List<TargetObjectDef>();
                        entrySets[groupName].Add(child);

                        entries.RemoveAll(at => at.Object == child);
                    }
                }
            }
        }

        #endregion // Milo Helper Methods

        private static void SerialiseTimecycleSpheres(TargetObjectDef mloArchetypeObjectDef, XElement parentElement)
        {
            List<TargetObjectDef> timecycleSphereObjectDefs = new List<TargetObjectDef>();
            GetTimecycleSpheresRecursive(mloArchetypeObjectDef, timecycleSphereObjectDefs);

            XElement timeCycleModifiersElement = new XElement("timeCycleModifiers");
            foreach (TargetObjectDef timecycleSphereObjectDef in timecycleSphereObjectDefs)
            {
                XElement xmlItem = new XElement("Item");
                xmlItem.Add(new XAttribute("type", "CMloTimeCycleModifier"));

                String name = (String)timecycleSphereObjectDef.GetAttribute(AttrNames.TWODFX_TCYC_SPHERE_NAME, AttrDefaults.TWODFX_TCYC_SPHERE_NAME);
                float fallof = (float)timecycleSphereObjectDef.GetAttribute(AttrNames.TWODFX_TCYC_SPHERE_RANGE, AttrDefaults.TWODFX_TCYC_SPHERE_RANGE);
                float strength = (float)timecycleSphereObjectDef.GetAttribute(AttrNames.TWODFX_TCYC_SPHERE_PERCENTAGE, AttrDefaults.TWODFX_TCYC_SPHERE_PERCENTAGE);
                int start_hour = (int)timecycleSphereObjectDef.GetAttribute(AttrNames.TWODFX_TCYC_SPHERE_START_HOUR, AttrDefaults.TWODFX_TCYC_SPHERE_START_HOUR);
                int end_hour = (int)timecycleSphereObjectDef.GetAttribute(AttrNames.TWODFX_TCYC_SPHERE_END_HOUR, AttrDefaults.TWODFX_TCYC_SPHERE_END_HOUR);

                Matrix34f miloToWorldTransform = mloArchetypeObjectDef.NodeTransform.Transpose();// HACK - Transpose to work around data issue
                Matrix34f worldToMiloTransform = miloToWorldTransform.Inverse();
                Matrix34f timecycleSphereToWorldTransform = timecycleSphereObjectDef.NodeTransform.Transpose();// HACK - Transpose to work around data issue
                Matrix34f timecycleSphereToMiloTransform = timecycleSphereToWorldTransform * worldToMiloTransform;
                float radius = 1.0f;
                if (null != timecycleSphereObjectDef.ParamBlocks)
                    radius = timecycleSphereObjectDef.GetParameter("radius", 1.0f);

                Xml.CreateElementWithText(xmlItem, "name", name);
                Vector4f sphere = new Vector4f(
                    timecycleSphereToMiloTransform.D.X,
                    timecycleSphereToMiloTransform.D.Y,
                    timecycleSphereToMiloTransform.D.Z,
                    radius);
                Xml.CreateElementWithVectorAttributes(xmlItem, "sphere", sphere);
                Xml.CreateElementWithValueAttribute(xmlItem, "range", fallof);
                Xml.CreateElementWithValueAttribute(xmlItem, "percentage", strength);
                Xml.CreateElementWithValueAttribute(xmlItem, "start_hour", start_hour);
                Xml.CreateElementWithValueAttribute(xmlItem, "end_hour", end_hour);

                timeCycleModifiersElement.Add(xmlItem);
            }
            parentElement.Add(timeCycleModifiersElement);
        }

        private static void GetTimecycleSpheresRecursive(TargetObjectDef objectDef, ICollection<TargetObjectDef> timecycleSphereObjectDefs)
        {
            if (objectDef.IsTimeCycleSphere())
            {
                timecycleSphereObjectDefs.Add(objectDef);
            }

            foreach (TargetObjectDef childObjectDef in objectDef.Children)
            {
                GetTimecycleSpheresRecursive(childObjectDef, timecycleSphereObjectDefs);
            }
        }

        #endregion // Private Helper Methods

        #region Private data
        private bool splitITYP_;
        #endregion
    }

} // RSG.SceneXml.MapExport.Project.GTA namespace
