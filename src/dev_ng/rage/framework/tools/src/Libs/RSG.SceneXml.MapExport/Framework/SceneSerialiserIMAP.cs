using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.Metadata.Util;
using RSG.SceneXml.MapExport.Framework.Collections;
using RSG.SceneXml.MapExport.Util;

namespace RSG.SceneXml.MapExport.Framework
{

	/// <summary>
	/// Framework IMAP Map Data Serialiser (abstract class)
	/// </summary>
	/// This will eventually replace the IDE serialiser completely.
	/// This should be used as a base class for project-specific serialisers.
	/// 
	/// This serialises ::rage::fwMapData structures.
	/// 
	public abstract class SceneSerialiserIMAP<ContainerClass> :
        SceneSerialiserBase where ContainerClass : IMAPContainer, new()
	{
		#region Member Data
        /// <summary>
        /// RSG.SceneXml Scene to be serialised.
        /// </summary>
        public Scene[] Scenes
        {
            get;
            protected set;
        }

        public BoundingBox2f BoundsXY
        {
            get
            {
                // Gather all bounding boxes
                List<BoundingBox3f> containerBoundingBoxes = new List<BoundingBox3f>();
                containerBoundingBoxes.Add(StaticContainer.EntityBoundingBox);
                containerBoundingBoxes.Add(SceneStreamedContainer.EntityBoundingBox);
                containerBoundingBoxes.Add(SceneStreamedLongContainer.EntityBoundingBox);
                containerBoundingBoxes.Add(SceneStreamedCriticalContainer.EntityBoundingBox);
                containerBoundingBoxes.AddRange(StreamedContainers.Select(container => container.EntityBoundingBox));
                containerBoundingBoxes.AddRange(StreamedLongContainers.Select(container => container.EntityBoundingBox));
                containerBoundingBoxes.AddRange(StreamedCriticalContainers.Select(container => container.EntityBoundingBox));

                // Convert to XY
                BoundingBox2f boundsXY = new BoundingBox2f();
                foreach (BoundingBox3f containerBoundingBox in containerBoundingBoxes)
                {
                    boundsXY.Expand(new Vector2f(containerBoundingBox.Min.X, containerBoundingBox.Min.Y));
                    boundsXY.Expand(new Vector2f(containerBoundingBox.Max.X, containerBoundingBox.Max.Y));
                }

                return boundsXY;
            }
        }

        public BoundingBox3f BoundsXYZ
        {
            get
            {
                BoundingBox3f bounds = new BoundingBox3f();

                // Gather all bounding boxes
                List<BoundingBox3f> containerBoundingBoxes = new List<BoundingBox3f>();
                containerBoundingBoxes.Add(StaticContainer.EntityBoundingBox);
                containerBoundingBoxes.Add(SceneStreamedContainer.EntityBoundingBox);
                containerBoundingBoxes.Add(SceneStreamedLongContainer.EntityBoundingBox);
                containerBoundingBoxes.Add(SceneStreamedCriticalContainer.EntityBoundingBox);
                containerBoundingBoxes.AddRange(StreamedContainers.Select(container => container.EntityBoundingBox));
                containerBoundingBoxes.AddRange(StreamedLongContainers.Select(container => container.EntityBoundingBox));
                containerBoundingBoxes.AddRange(StreamedCriticalContainers.Select(container => container.EntityBoundingBox));

                foreach (BoundingBox3f containerBoundingBox in containerBoundingBoxes)
                {
                    if (!containerBoundingBox.IsEmpty)
                        bounds.Expand(containerBoundingBox);
                }

                return bounds;
            }
        }

		/// <summary>
		/// IMAP Output Directory.
		/// </summary>
		protected String IMAPStreamDirectory;

		/// <summary>
		/// Static IMAP (static entities) container.
		/// </summary>
		protected ContainerClass StaticContainer;

		/// <summary>
		/// Scene Streamed Container Structures (all entities in scenes)
		/// </summary>
		protected ContainerClass SceneStreamedContainer;

		/// <summary>
		/// Scene Streamed Long Container Structures (all entities in scenes)
		/// </summary>
		protected ContainerClass SceneStreamedLongContainer;

        /// <summary>
        /// Scene Streamed Critical Container Structures (all entities in scenes)
        /// </summary>
        protected ContainerClass SceneStreamedCriticalContainer;

		/// <summary>
		/// Streamed Container Structures
		/// </summary>
		protected List<ContainerClass> StreamedContainers;

		/// <summary>
		/// Streamed Long Container Structures
		/// </summary>
		protected List<ContainerClass> StreamedLongContainers;

        /// <summary>
        /// Streamed Critical Container Structures
        /// </summary>
        protected List<ContainerClass> StreamedCriticalContainers;

        /// <summary>
        /// Container for occlusion data.
        /// </summary>
        protected Occlusion.Container OcclusionContainer;

		/// <summary>
        /// Container for instanced map data.
        /// </summary>
        protected ContainerClass InstanceMapDataContainer;

		/// <summary>
        /// Interior name map.
        /// </summary>
        protected InteriorContainerNameMap InteriorNameMap;

        /// <summary>
        /// Interior name map.
        /// </summary>
        protected String[] NonSerialisedTimeCycleTypes;

        /// <summary>
        /// This is the LOD container, the parent for the static container and is set during scene processing (it's never a real serialisable container)
        /// </summary>
        protected string lodContainerName_;
		#endregion // Member Data

		#region Constructor(s)
		/// <summary>
		/// Map serialiser constructor, multiple Scenes.
		/// </summary>
        /// <param name="branch"></param>
		/// <param name="scenes"></param>
		/// <param name="imapStreamDir"></param>
		/// <param name="builddir"></param>
        /// <param name="interiorNameMap"></param>
        /// <param name="interiorNameMap"></param>
        public SceneSerialiserIMAP(
            IBranch branch, 
            Scene[] scenes, 
            String imapStreamDir, 
            String builddir, 
            InteriorContainerNameMap interiorNameMap, 
            String[] nonSerialisedTimeCycleTypes)
            : base(branch)
		{
            this.Scenes = scenes;
			this.IMAPStreamDirectory = (imapStreamDir.Clone() as String);// what is this?  Surely this is unnecessary?
            this.InteriorNameMap = interiorNameMap;
            this.NonSerialisedTimeCycleTypes = nonSerialisedTimeCycleTypes;
			Reset();
		}
		#endregion // Constructor(s)

        #region Protected Object-Serialisation Methods
        /// <summary>
        /// Serialise a ::rage::fwMapData object to XML.
        /// </summary>
        /// This is the top-level Framework Map Data object; that acts as a
        /// container for all the other map data.
        /// <param name="name"></param>
        /// <param name="container"></param>
        /// <returns>XmlElement representing ::rage::fwMapData</returns>
        protected XElement AsFwMapData(String name, IMAPContainer container)
        {
            XElement node = new XElement(name);

            Xml.CreateElementWithText(node, "name", container.Name);

            // Determine whether we have a ref to a SLOD2/3/4 entity; and only include
            // the parent under certain circumstances (for IPL Groups SLOD2/3/4 support).
            // This is a hack and when we consider all SceneXml files together rather than
            // in Input Groups this can go.
            bool hasSLOD2EntityRef = container.Entities.Any(e =>
                (null != e.Object && null != e.Object.LOD && null != e.Object.LOD.Parent) &&
                (e.Object.LOD.Parent.IsContainerLODRefObject()));

            // Parent Hash
            // If we've overidden the lod Parent due to it spanning a SLOD1 - > SLOD2 link
            // inside an IMAP Group  then use that name here. url:bugstar:4752720.
            if (!String.IsNullOrEmpty(container.ParentImapOverride))
            {
                Xml.CreateElementWithText(node, "parent", container.ParentImapOverride);
            }
            else if ((null != container.Parent) && (container.Parent != container))
            {
                Xml.CreateElementWithText(node, "parent", container.Parent.Name);
            }
            else if (!String.IsNullOrEmpty(lodContainerName_) &&
                ((container == StaticContainer && hasSLOD2EntityRef) || // Normal map case
                (container.Flags.HasFlag(IMAP_eFlags.F_MANUAL_STREAM_ONLY) && hasSLOD2EntityRef))) // IMAP Group case
            {
                String parentName = MapAsset.AppendMapPrefixIfRequired(this.Project, lodContainerName_);
                Xml.CreateElementWithText(node, "parent", parentName);
            }

            // Flags
            Xml.CreateElementWithValueAttribute(node, "flags", container.Flags.ToString("d"));

            // Content Flags
            Xml.CreateElementWithValueAttribute(node, "contentFlags", container.ContentFlags.ToString("d"));

            // Dependencies
#warning DHM TODO; once defined.

            // Physics Dictionaries
            XElement xmlPhysicsDictionaries = new XElement("physicsDictionaries");
            foreach (String physicsDictionary in container.PhysicsDictionaries)
            {
                Xml.CreateElementWithText(xmlPhysicsDictionaries, "Item", physicsDictionary);
            }
            node.Add(xmlPhysicsDictionaries);

            // Entity Bounding Box
            if (!container.EntityBoundingBox.IsEmpty)
            {
                Xml.CreateElementWithVectorAttributes(node, "entitiesExtentsMax",
                    container.EntityBoundingBox.Max);
                Xml.CreateElementWithVectorAttributes(node, "entitiesExtentsMin",
                    container.EntityBoundingBox.Min);
            }

            // Streaming Bounding Box
            if (!container.StreamingBoundingBox.IsEmpty)
            {
                Xml.CreateElementWithVectorAttributes(node, "streamingExtentsMax",
                    container.StreamingBoundingBox.Max);
                Xml.CreateElementWithVectorAttributes(node, "streamingExtentsMin",
                    container.StreamingBoundingBox.Min);
            }

            // Core entities node (fetched by a higher-level serialiser.
            node.Add(new XElement("entities"));

            // ContainerLODs written in project-specific area; as they 
            // require serialisation information from each entity.

            // Occlusion Models
            if (container.OcclusionNode != null)
                container.OcclusionNode.AddOcclusionModelsToXmlDocument(node);

            return (node);
        }

        /// <summary>
        /// Serialise a ::rage::fwEntityDef object to XML (transform relative to parent).
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="entity"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        protected virtual XElement AsFwEntityDef(Entity entity, Entity parent = null, int entityIndex = -1)
        {
            Matrix34f mtxParent = Matrix34f.Identity;
            if (null != parent)
                mtxParent = parent.Object.NodeTransform.Inverse();

            Vector3f pos = mtxParent * entity.Object.NodeTransform.Translation;
            Quaternionf rot = new Quaternionf(mtxParent * entity.Object.NodeTransform);
            Vector3f scale = entity.Object.NodeScale;
            if (entity.WorldTransformOverride != null)
            {
                pos = entity.WorldTransformOverride.Translation;
                rot = new Quaternionf(mtxParent * entity.WorldTransformOverride);
                scale = entity.ScaleOverride;
            }

            String block = entity.Object.GetAttribute(AttrNames.OBJ_BLOCK_ID, AttrDefaults.OBJ_BLOCK_ID);
            int aoMult = entity.Object.GetAttribute(AttrNames.OBJ_AMBIENT_OCCLUSION_MULTIPLIER, AttrDefaults.OBJ_AMBIENT_OCCLUSION_MULTIPLIER);

            // Milo's use an inverted rotation for some reason.
            if (entity.Object.IsMiloTri())
                rot.Invert();

            if (entity.Object.HasLODParent())
                Debug.Assert(-1 != entity.LODParentIndex, String.Format("Invalid LOD Parent Index for {0}.  Contact tools.", entity.Object));

            XElement xmlItem = Xml.CreateElementWithAttribute(null, "Item", "type", "::rage::fwEntityDef");
            if (entityIndex != -1)
            {
                Xml.CreateComment(xmlItem, String.Format("<entityIndex>{0}</entityIndex>", entityIndex));
            }
            Xml.CreateElementWithText(xmlItem, "archetypeName", entity.Name);
            Xml.CreateElementWithValueAttribute(xmlItem, "flags", EntityDefFlags.Get(entity).ToString("d"));
            Xml.CreateElementWithValueAttribute(xmlItem, "guid", entity.Hash);
            Xml.CreateElementWithVectorAttributes(xmlItem, "position", pos);
            Xml.CreateElementWithVectorAttributes(xmlItem, "rotation", rot);
            Xml.CreateElementWithValueAttribute(xmlItem, "scaleXY", entity.Object.NodeScale.X);
            Xml.CreateElementWithValueAttribute(xmlItem, "scaleZ", entity.Object.NodeScale.Z);
            fwEntityDef_ePriorityLevel priority = PriorityLevel.Get(entity);
            xmlItem.Add(priority.AsXml());

            // passed to fwLodNode
            if (entity.LODParentIndex != -1)
            {
                if (!entity.Object.LOD.Parent.IsContainerLODRefObject())
                {
#if DEBUG
				    Debug.Assert(null != entity.Parent);
				    // Validate that the parent has this entity as a child.
				    bool found = false;
				    foreach (TargetObjectDef child in entity.Parent.Object.LOD.Children)
				    {
					    if (child.Equals(entity.Object))
					    {
						    found = true;
						    break;
					    }
				    }
				    Debug.Assert(found);
#endif // DEBUG
                    Xml.CreateComment(xmlItem, String.Format("ParentObject set to {0}", entity.Parent.Object.Name));
                }
                else
                {
                    Xml.CreateComment(xmlItem, String.Format("ParentObject set to {0} in LOD container", entity.Object.LOD.Parent.RefName));
                }
            }
            Xml.CreateElementWithValueAttribute(xmlItem, "parentIndex", entity.LODParentIndex);

            if (entity.Object.IsRefObject() || entity.Object.IsXRef())
            {
                bool refdObjIsAnimDummy = null != entity.Object.RefObject && entity.Object.RefObject.IsAnimProxy();
                if (entity.InstanceLODDistance || refdObjIsAnimDummy)
                    Xml.CreateElementWithValueAttribute(xmlItem, "lodDist", entity.LODDistance);
                else
                    Xml.CreateElementWithValueAttribute(xmlItem, "lodDist", -1.0f);
            }
            else if (entity.IsInstancedObject)
            {
                Xml.CreateElementWithValueAttribute(xmlItem, "lodDist", entity.LODDistance);
            }
            else
            {
                // B* 1032979 - entity objects contain LOD distances that have onsidered scene overrides.
                // The change to make this code rely on Object.GetLODDistance() broke the scene override system.
                if (!entity.Object.HasDrawableLODParent())
                {
                    Xml.CreateElementWithValueAttribute(xmlItem, "lodDist", entity.LODDistance);
                }
                else
                {
                    TargetObjectDef lowestLod = entity.Object;
                    while (lowestLod.HasDrawableLODParent())
                        lowestLod = lowestLod.DrawableLOD.Parent;

                    Xml.CreateElementWithValueAttribute(xmlItem, "lodDist", lowestLod.GetLODDistance());
                }

                if (entity.ChildLODDistance > 0.0f)
                    Xml.CreateElementWithValueAttribute(xmlItem, "childLodDist", entity.ChildLODDistance);
            }

            fwEntityDef_eLodTypes lodlevel = LodLevel.Get(entity);
            xmlItem.Add(lodlevel.AsXml());

            int children = 0;
            if (entity.Object.HasLODChildren())
            {
                children = entity.Object.LOD.Children.Where(child => (child.DontExportIPL() == false)).Count();
            }
            else if (lodlevel == fwEntityDef_eLodTypes.LODTYPES_DEPTH_SLOD2)
            {
                children = InterContainerLODManager.Instance.GetSLOD2ObjectChildCount(entity.Object);
            }
            Xml.CreateElementWithValueAttribute(xmlItem, "numChildren", children);

            return (xmlItem);
        }

		/// <summary>
		/// Serialise a ::rage::fwContainerLodDef object to XML.
		/// </summary>
		/// <param name="doc">XmlDocument container</param>
		/// <param name="entity"></param>
		/// <returns>XmlElement representing ::rage::fwContainerLodDef</returns>
		protected virtual XmlElement AsFwContainerLodDef(XmlDocument doc, ContainerLod clod)
		{
			XmlElement xmlItem = doc.CreateElement("Item");
			Xml.CreateElementWithText(doc, xmlItem, "name", clod.Name);
			Xml.CreateElementWithValueAttribute(doc, xmlItem, "parentIndex", clod.Index);

			return (xmlItem);
		}

        /// <summary>
        /// Serialise a ::rage::fwContainerLodDef object to XML.
        /// </summary>
        /// <param name="doc">XmlDocument container</param>
        /// <param name="entity"></param>
        /// <returns>XmlElement representing ::rage::fwContainerLodDef</returns>
        protected virtual XElement AsFwContainerLodDef(ContainerLod clod)
        {
            XElement xmlItem = new XElement("Item");
            Xml.CreateElementWithText(xmlItem, "name", clod.Name);
            Xml.CreateElementWithValueAttribute(xmlItem, "parentIndex", clod.Index);

            return (xmlItem);
        }

		/// <summary>
		/// Serialise a ::rage::fwExtension object to XML.
		/// </summary>
		/// <param name="doc"></param>
		/// <param name="instance"></param>
		/// <returns></returns>
		protected XmlElement AsFwExtensionDef(XmlDocument doc, Extension extension)
		{
			XmlElement xmlItem = Xml.CreateElementWithAttribute(doc, null, "Item", "type", "::rage::fwExtensionDef");
			Xml.CreateElementWithText(doc, xmlItem, "name", extension.Parent.GetObjectName());
			return (xmlItem);
		}

        /// <summary>
        /// Serialise a ::rage::fwExtension object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="instance"></param>
        /// <returns></returns>
        protected XElement AsFwExtensionDef(Extension extension)
        {
            XElement xmlItem = Xml.CreateElementWithAttribute(null, "Item", "type", "::rage::fwExtensionDef");
            Xml.CreateElementWithText(xmlItem, "name", extension.Parent.GetObjectName());
            return (xmlItem);
        }

        /// <summary>
        /// Serialise a ::rage::fwExtension object to XML.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="instance"></param>
        /// <returns></returns>
        protected XElement AsFwClothBoundsExtensionDef(Extension extension)
        {
            if (!extension.Object.IsMaxClass(ClassConsts.MAX_COLLISION_CAPSULE) &&
                !extension.Object.IsMaxClass(ClassConsts.MAX_COLLISION_PLANE))
                return null;

            // Changing to world space again...
             Matrix34f localToWorldTransform = extension.Object.NodeTransform.Transpose();// HACK - Transpose to work around data issue
// 
//             Matrix34f parentToWorldTransform = extension.Parent.NodeTransform.Transpose();// HACK - Transpose to work around data issue
//             Matrix34f worldToParentTransform = parentToWorldTransform.Inverse();
// 
//             Matrix34f localToParentTransform = localToWorldTransform * worldToParentTransform;

            XElement xmlItem = new XElement("Item");
            Xml.CreateElementWithText(xmlItem, "OwnerName", extension.Parent.Name);
            Quaternionf rot = new Quaternionf(localToWorldTransform);
            Xml.CreateElementWithVectorAttributes(xmlItem, "Rotation", rot);
            Xml.CreateElementWithVectorAttributes(xmlItem, "Position", extension.Object.NodeTransform.D);

            if (extension.Object.IsMaxClass(ClassConsts.MAX_COLLISION_PLANE))
            {
                Vector3f planeNormal = new Vector3f(0, 0, 1);
                switch (extension.Object.GetParameter("Orientation", 0))
                {
                    case 1:
                        planeNormal = new Vector3f(0, 1, 0);
                        break;
                    case 2:
                        planeNormal = new Vector3f(1, 0, 0);
                        break;
                }
                new Matrix34f(rot).Transform(planeNormal);
                planeNormal.Normalise();
                Xml.CreateElementWithVectorAttributes(xmlItem, "Normal", planeNormal);
            }
            else
            {
                Xml.CreateElementWithVectorAttributes(xmlItem, "Normal", new Vector3f(0.0f, 0.0f, 0.0f));
            }

            Xml.CreateElementWithValueAttribute(xmlItem, "CapsuleRadius", extension.Object.GetParameter("radius", 0.0f));
            Xml.CreateElementWithValueAttribute(xmlItem, "CapsuleLen", extension.Object.GetParameter("length", 0.0f));

            Xml.CreateElementWithValueAttribute(xmlItem, "CapsuleHalfHeight", 0.0f);
            Xml.CreateElementWithValueAttribute(xmlItem, "CapsuleHalfWidth", 0.0f);
            Xml.CreateElementWithValueAttribute(xmlItem, "Flags", 0);
            return (xmlItem);
        }
        #endregion // Protected Object-Serialisation Methods

		#region Scene and Object Processing Methods
        /// <summary>
        /// Process all scenes into internal structures ready for serialisation
        /// </summary>
        public abstract bool Process(String pathname);

        /// <summary>
        /// Process internal structures to ITYP file(s)
        /// </summary>
        public abstract bool Serialise(String pathname);

        /// <summary>
        /// Write manifest data to XmlDocument.
        /// </summary>
        public abstract bool WriteManifestData(XDocument xmlDoc, IEnumerable<ManifestDependency> manifestDependencies);

		/// <summary>
		/// Clear all internal state.
		/// </summary>
		protected virtual void Reset()
		{
			StaticContainer = new ContainerClass();
			SceneStreamedContainer = new ContainerClass();
			SceneStreamedLongContainer = new ContainerClass();
            SceneStreamedCriticalContainer = new ContainerClass();
			StreamedContainers = new List<ContainerClass>();
			StreamedLongContainers = new List<ContainerClass>();
            StreamedCriticalContainers = new List<ContainerClass>();
            OcclusionContainer = new Occlusion.Container();
            InstanceMapDataContainer = new ContainerClass();
		}

		/// <summary>
		/// Process a set of Scene objects.
		/// </summary>
		protected virtual void ProcessScenes(IEnumerable<Scene> scenes)
		{
			foreach (Scene scene in scenes)
				ProcessScene(scene);

			// Post-process to complete Entity 'Parent' property.
			foreach (Entity e in this.SceneStreamedContainer.Entities)
				SetParentEntity(e, this.StaticContainer);
			foreach (Entity e in this.SceneStreamedLongContainer.Entities)
				SetParentEntity(e, this.StaticContainer);
            foreach (Entity e in this.SceneStreamedCriticalContainer.Entities)
                SetParentEntity(e, this.StaticContainer);
			// Post-process to complete Entity 'Children' property.
			foreach (Entity e in this.StaticContainer.Entities)
			{
				SetParentEntity(e, this.StaticContainer);
				SetChildEntities(e, this.StaticContainer, this.SceneStreamedContainer, this.SceneStreamedLongContainer, this.SceneStreamedCriticalContainer);
			}
		}

		/// <summary>
		/// Process a single Scene object.
		/// </summary>
		/// <param name="scene">Scene object to process.</param>
		protected virtual void ProcessScene(Scene scene)
		{
			// Loop through scene objects.
			foreach (TargetObjectDef obj in scene.Objects)
			{
				ProcessObject(obj);
			}
		}

		/// <summary>
		/// Process a single ObjectDef object.
		/// </summary>
		/// <param name="o">ObjectDef object to process.</param>
		protected virtual void ProcessObject(TargetObjectDef o)
		{
			if (o.DontExport() || o.DontExportIPL() || SceneOverrideIntegrator.Instance.IsObjectMarkedForDeletion(o))
				return;

			// GD: Drawable LOD states don't ever need to go into the ITYP.
            // Shadow meshes need same treatment
            if (o.HasDrawableLODChildren() || o.HasRenderSimParent() || o.HasShadowMeshLink())
				return;

			ProcessSceneObject(o);
		}

		/// <summary>
		/// Process a single ObjectDef object.
		/// </summary>
		/// <param name="o"></param>
		protected virtual void ProcessSceneObject(TargetObjectDef o)
		{
            if (o.DontExport() || o.DontExportIPL() || SceneOverrideIntegrator.Instance.IsObjectMarkedForDeletion(o))
				return;

            if (IsExcluded(o))
                return;

			if (o.IsContainer())
			{
                foreach (TargetObjectDef obj in o.Children)
					ProcessSceneObject(obj);
				return;
			}
			else if (o.IsOcclusion())
			{
				if (o.IsOcclusionBox())
				{
                    this.OcclusionContainer.AddBoxPlaceholder(o);
				}
				else if (o.IsOcclusionMesh())
				{
                    this.OcclusionContainer.AddMeshPlaceholder(o);
				}
				else
				{
					Log.Log__ErrorCtx(o.Name, "Object has unknown occlusion type.");
				}
			}
            else if (o.IsStreamingExtentsOverrideBox())
            {
                this.OcclusionContainer.StreamingExtentsOverride = o.WorldBoundingBox;
            }
			else if (o.IsObject())
			{
				Entity entity = new Entity(o);
				switch (entity.StreamMode)
				{
					case Entity.StreamModeType.Streamed:
						this.SceneStreamedContainer.AddEntity(entity);
						break;
					case Entity.StreamModeType.StreamedLong:
                        this.SceneStreamedLongContainer.AddEntity(entity);
						break;
                    case Entity.StreamModeType.StreamedCritical:
                        this.SceneStreamedCriticalContainer.AddEntity(entity);
                        break;
					case Entity.StreamModeType.NotStreamed:
                        this.StaticContainer.AddEntity(entity);
						break;
					default:
						String message = String.Format("Internal error: invalid streaming mode for {0}.",
							entity.Object.Name);
						Debug.Assert(false, message);
						Log.Log__ErrorCtx(o.Name, message);
						break;
				}
			}
			// If you need project-specific entries here you should
			// be using a project-specific IMAPContainer and a custom
			// serialiser (see GTA namespace for an example).
		}
		#endregion // Scene and Object Processing Methods

        #region Protected Object-Serialisation Methods
        /// <summary>
        /// Serialise a ::rage::fwPropInstanceListDef::InstanceData object to XML (transform relative to parent).
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="entity"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        protected virtual XElement AsFwInstancedPropEntityDef(InstancedPropEntity entity)
        {
            XElement xmlItem = new XElement("Item");

            XElement xmlInstanceMatrix = new XElement("InstMat");
            XAttribute contentAttribute = new XAttribute("content", "vector4_array");
            xmlInstanceMatrix.Add(contentAttribute);
            xmlItem.Add(xmlInstanceMatrix);

            Matrix34f transform = new Matrix34f();
            transform.Translation = entity.Position;

            Matrix34f rotationMatrix = new Matrix34f(entity.Rotation);
            transform *= rotationMatrix;

            transform.Scale(entity.ScaleXY, entity.ScaleXY, entity.ScaleZ);

            String vector4ArrayString = String.Format("{0}\t{1}\t{2}\t{3}\t", transform.A.X, transform.B.X, transform.C.X, transform.D.X);
            vector4ArrayString += String.Format("{0}\t{1}\t{2}\t{3}\t", transform.A.Y, transform.B.Y, transform.C.Y, transform.D.Y);
            vector4ArrayString += String.Format("{0}\t{1}\t{2}\t{3}", transform.A.Z, transform.B.Z, transform.C.Z, transform.D.Z);

            XText xmlText = new XText(vector4ArrayString);
            xmlInstanceMatrix.Add(xmlText);

            Vector3f color = new Vector3f();

            if (entity.Index != -1)
            {
                Xml.CreateElementWithValueAttribute(xmlItem, "Index", entity.Index);
            }

            Xml.CreateElementWithValueAttribute(xmlItem, "ScaleXY", entity.ScaleXY);
            Xml.CreateElementWithValueAttribute(xmlItem, "ScaleZ", entity.ScaleZ);
            Xml.CreateColor32Element(xmlItem, "Tint", color);
            return (xmlItem);
        }

        /// <summary>
        /// Returns a string defining an instanced grass entity.
        /// The parent is responsible for handling this data.
        /// </summary>
        /// <param name="vertexBufferElement"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        protected virtual XElement AsFwInstancedGrassEntityDef(InstancedGrassEntity entity, BoundingBox3f boundingBox)
        {
            XElement xmlItem = new XElement("Item");

            Matrix34f transform = new Matrix34f();
            transform.Translation = entity.Position;

            Matrix34f rotationMatrix = new Matrix34f(entity.Rotation);
            transform *= rotationMatrix;

            transform.Scale(entity.ScaleXY, entity.ScaleXY, entity.ScaleZ);

            XElement xmlPosition = new XElement("Position");
            XAttribute contentAttribute = new XAttribute("content", "short_array");
            xmlPosition.Add(contentAttribute);

            uint x = entity.CompressedX;
            uint y = entity.CompressedY;
            uint z = entity.CompressedZ;

            Vector3f calculatedNormal = ((entity.GroundNormal + new Vector3f(1, 1, 1)) / 2) * 255;

            string positionText = String.Format("{0}\t{1}\t{2}", x, y, z);
            XText xmlText = new XText(positionText);
            xmlPosition.Add(xmlText);
            xmlItem.Add(xmlPosition);

            int normalX = Convert.ToByte((int)calculatedNormal.X);
            Xml.CreateElementWithValueAttribute(xmlItem, "NormalX", normalX);

            uint r = Convert.ToByte((uint)entity.TintColor.X);
            uint g = Convert.ToByte((uint)entity.TintColor.Y);
            uint b = Convert.ToByte((uint)entity.TintColor.Z);

            string colorText = String.Format("{0}\t{1}\t{2}", r, g, b);
            XElement xmlColor = new XElement("Color");
            XAttribute colorContentAttribute = new XAttribute("content", "char_array");
            xmlColor.Add(colorContentAttribute);

            xmlText = new XText(colorText);
            xmlColor.Add(xmlText);
            xmlItem.Add(xmlColor);

            int normalY = Convert.ToByte((int)calculatedNormal.Y);
            Xml.CreateElementWithValueAttribute(xmlItem, "NormalY", normalY);

            Xml.CreateElementWithValueAttribute(xmlItem, "Scale", (int)entity.ScaleXY);

            Xml.CreateElementWithValueAttribute(xmlItem, "Ao", (int)entity.AO);

            return (xmlItem);
        }
        #endregion // Protected Object-Serialisation Methods

		#region Misc. Helper Methods
		/// <summary>
		/// Sets the Entity's Parent property from a container (or its parent).
		/// </summary>
        /// <param name="e"></param>
        /// <param name="container"></param>
        /// *** DHM TODO: probably make this obsolete and use the one below ***
        /// 
		protected void SetParentEntity(Entity e, IMAPContainer container)
		{
            if (!e.Object.HasLODParent())
                return;

            Entity parent = null;
            if (null == container.Parent)
                parent = container.FindEntity(e.Object.LOD.Parent);
            else
                parent = container.Parent.FindEntity(e.Object.LOD.Parent);
            Debug.Assert(null != parent,
                 String.Format("Internal error: entity {0} parent not found.",
                 e.Object.LOD.Parent.Name));
            e.Parent = parent;
        }

        /// <summary>
        /// Sets the Entity's Parent property from a specific container.
        /// </summary>
        /// <param name="e"></param>
        /// <param name="container"></param>
        protected void SetParentEntityFromContainer(Entity e, IMAPContainer container)
        {
            if (!e.Object.HasLODParent())
                return;

            Entity parent = container.FindEntity(e.Object.LOD.Parent);
            e.Parent = parent;
        }

		/// <summary>
		/// Sets the Entity's Children property from the stream containers.
		/// </summary>
		/// <param name="e"></param>
		/// <param name="containers"></param>
		protected void SetChildEntities(Entity e, params IMAPContainer[] containers)
		{
			switch (e.Object.LODLevel)
			{
                case ObjectDef.LodLevel.SLOD4:
				case ObjectDef.LodLevel.SLOD3:
				case ObjectDef.LodLevel.SLOD1:
				case ObjectDef.LodLevel.LOD:
					Debug.Assert(e.Object.HasLODChildren(),
						String.Format("Internal error: entity {0} has no LOD children, but should have!",
						e.Object.Name));
                    foreach (TargetObjectDef o in e.Object.LOD.Children)
					{
						// Ignore children that won't be exported.
						if (o.DontExport() || o.DontExportIPL())
							continue;

						Entity child = null;
						foreach (IMAPContainer container in containers)
						{
							child = container.FindEntity(o);
							if (null != child)
								break;
						}

                        // We never want to add in-situ interiors as children
                        bool isMilo = o.IsMilo();
                        bool isRefToMilo = (o.IsRefObject() && (null != o.RefObject) && o.RefObject.IsMiloTri());
                        if (!(isMilo || isRefToMilo))
                        {
                            if (child == null)
                            {
                                Log.Log__WarningCtx(o.Name,
                                    "Unable to lookup entity for '{0}' when building child entity container for '{1}'.  Other errors may follow.", 
                                    o.Name, e.Object.Name);
                            }

                            e.Children.Add(child);
                        }
					}
					break;
			}
		}

        /// <summary>
        /// Returns whether this entity should be excluded from processing.
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        protected bool IsExcluded(TargetObjectDef o)
        {
            foreach (Scene scene in this.Scenes)
            {
                if (scene.ExcludedObjects.Contains(o.AttributeGuid))
                    return true;
            }

            return false;
        }
		#endregion // Misc. Helper Methods
	}

} // RSG.SceneXml.MapExport.Framework namespace
