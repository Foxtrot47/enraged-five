//
// File: MiloPortal.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of MiloPortal class
//

using System;
using System.Collections.Generic;
using RSG.Base.Math;

namespace RSG.SceneXml.MapExport
{

    /// <summary>
    /// Abstraction for a Milo Portal.
    /// </summary>
    /// A Milo Portal has references to the two room helpers on either side 
    /// of the portal.
    internal class MiloPortal
    {
        #region Constants
        /// Number of attached objects per portal
        private static readonly int MLOPORTAL_NUM_ATTACHED = 4;
        #endregion // Constants

        #region Properties
        public TargetObjectDef Portal;
        public TargetObjectDef RoomA;
        public TargetObjectDef RoomB;
        public List<TargetObjectDef> Children;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MiloPortal(TargetObjectDef portal)
        {
            this.Portal = portal;
            this.Children = new List<TargetObjectDef>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return String representation of this Portal object.
        /// </summary>
        /// <returns></returns>
        /// Format of a Milo Portal line is:
        ///   roomAidx, roomBidx, ax, ay, az, bx, by, bz, cx, cy, cz, dx, dy, dz, attach0, ..., attach3, flags, rgb, fade
        ///   
        public String ToIDEPortal(Scene scene, String[] room_list, List<TargetObjectDef> entries)
        {
            List<String> roomlist = new List<String>(room_list);
            int roomAidx = 0;
            int roomBidx = 0;
            if (null != this.RoomA)
                roomAidx = roomlist.IndexOf(this.RoomA.Name) + 1; // +1 for limbo
            if (null != this.RoomB)
                roomBidx = roomlist.IndexOf(this.RoomB.Name) + 1; // +1 for limbo

            bool mirror = this.Portal.GetAttribute(AttrNames.MLOPORTAL_MIRROR, AttrDefaults.MLOPORTAL_MIRROR);
            UInt32 flags = Flags.GetMloPortalFlags(this.Portal);
            int red = this.Portal.GetAttribute(AttrNames.MLOPORTAL_RED, AttrDefaults.MLOPORTAL_RED);
            int green = this.Portal.GetAttribute(AttrNames.MLOPORTAL_GREEN, AttrDefaults.MLOPORTAL_GREEN);
            int blue = this.Portal.GetAttribute(AttrNames.MLOPORTAL_BLUE, AttrDefaults.MLOPORTAL_BLUE);
            int rgb = (int)((red & 0xFF) << 0) | ((green & 0xFF) << 8) | ((blue & 0xFF) << 16);
            float fade = this.Portal.GetAttribute(AttrNames.MLOPORTAL_FADE_DISTANCE, AttrDefaults.MLOPORTAL_FADE_DISTANCE);
            Object[] attachedGuids = this.Portal.GetParameter(ParamNames.MLOPORTAL_ATTACHED_OBJECTS, 
                ParamDefaults.MLOPORTAL_ATTACHED_OBJECTS);
            TargetObjectDef[] attached = new TargetObjectDef[attachedGuids.Length];
            int n = 0;
            foreach (Object o in attachedGuids)
            {
                if (o is Guid)
                    attached[n] = scene.FindObject((Guid)o);
                ++n;
            }

            // Portal NodeTransform (mirrors have a 10cm offset unless they are mirrors).
            float offset = 0.1f;
            if (mirror)
                offset = 0.0f;
            float lengthdiv2 = this.Portal.GetParameter(ParamNames.MLOPORTAL_LENGTH, ParamDefaults.MLOPORTAL_LENGTH) / 2.0f;
            float widthdiv2 = this.Portal.GetParameter(ParamNames.MLOPORTAL_WIDTH, ParamDefaults.MLOPORTAL_WIDTH) / 2.0f;
            Matrix34f parentMtx = this.Portal.Parent.NodeTransform.Inverse(); 
            Quaternionf nodeRot = new Quaternionf(this.Portal.NodeTransform);
            // DHM 2009/04/29 -- Milo entry and portal rotations are inverted for some reason.
            nodeRot.Invert();

            Matrix34f nodeMtx = new Matrix34f(nodeRot);
            nodeMtx.Translation = this.Portal.NodeTransform.Translation;
            nodeMtx *= parentMtx;
            Vector3f a = nodeMtx * (new Vector3f(widthdiv2 + offset, -lengthdiv2 - offset, 0.0f));
            Vector3f b = nodeMtx * (new Vector3f(widthdiv2 + offset, lengthdiv2 + offset, 0.0f));
            Vector3f c = nodeMtx * (new Vector3f(-widthdiv2 - offset, lengthdiv2 + offset, 0.0f));
            Vector3f d = nodeMtx * (new Vector3f(-widthdiv2 - offset, -lengthdiv2 - offset, 0.0f));

            String ide_line = String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, ",
                roomAidx, roomBidx, a.X, a.Y, a.Z, b.X, b.Y, b.Z, c.X, c.Y, c.Z, d.X, d.Y, d.Z);
            // Attached objects (4)
            for (n = 0; n < MLOPORTAL_NUM_ATTACHED; ++n)
            {
                if ((n > (attached.Length-1)) || (null == attached[n]))
                    ide_line += String.Format("-1, ");
                else
                    ide_line += String.Format("{0}, ", entries.IndexOf(attached[n]));
            }
            ide_line += String.Format("{0}, {1}, {2}", flags, rgb, fade);

            return (ide_line);
        }
        #endregion // Controller Methods
    }

} // RSG.SceneXml.MapExport namespace

// MiloPortal.cs
