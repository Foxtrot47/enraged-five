//
// File: Instance5.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Instance5.cs class
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using RSG.Base.Logging;
using RSG.Base.Math;

namespace RSG.SceneXml.MapExport
{

    /// <summary>
    /// SceneXml Instance with IPL Version 5 serialisation.
    /// </summary>
    /// See also Jimmy Bug #1123 for some details.
    /// 
    /// We expand on version 4 Instance4 class by writing out some additional
    /// information that normally the game determines at runtime.  These
    /// are summarised below:
    ///   1. Extra field for instances showing whether SLOD, LOD, HD
    ///   2. Extra field for instances showing number of children.
    ///   
    public class Instance5 : Instance4
    {
        #region Constants
        /// <summary>
        /// Maximum LOD object LOD distance.
        /// </summary>
        protected static float LOD_MAX_LOD_DISTANCE = 65000.0f;
        #endregion // Constants

        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public ObjectDef.InstanceType InstType
        {
            get { return m_eInstType; }
            protected set { m_eInstType = value; }
        }
        private ObjectDef.InstanceType m_eInstType;

        /// <summary>
        /// Indentation for this Instance in IPL5 files.
        /// </summary>
        public int Indent
        {
            get { return m_nIndent; }
            set { m_nIndent = value; }
        }
        private int m_nIndent;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor, no associated ObjectDef (e.g. used for anonymous block).
        /// </summary>
        /// <param name="scene">SceneXml.Scene containing instance</param>
        public Instance5(Scene scene)
            : base(scene)
        {
            this.InstType = ObjectDef.InstanceType.HD;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="scene">SceneXml.Scene containing instance</param>
        /// <param name="obj">SceneXml.ObjectDef for instance</param>
        /// <param name="reader"></param>
        public Instance5(Scene scene, TargetObjectDef obj, Util.IDEReaderBasic reader)
            : base(scene, obj, reader)
        {
            this.InstType = this.Object.GetInstanceType();
            this.LODDistance = this.GetInstanceLODDistance(reader);
            // LOD Distance sanity checks
            switch (this.InstType)
            {
                case ObjectDef.InstanceType.LOD:
                    if (this.LODDistance > LOD_MAX_LOD_DISTANCE)
                        Log.Log__Warning("LOD Object {0} has a high LOD distance, > {1}.", this, LOD_MAX_LOD_DISTANCE);
                    break;
            }
            DetermineStreamMode(reader); // Since updated the LOD Distance
        }
        #endregion // Constructor(s)

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return (String.Format("Instance5: [{0}]{1}", this.Object.Name, this.Object.Guid));
        }
        #endregion // Object Overrides

        #region Controller Methods
        /// <summary>
        /// Return IPL5 instance string representation.
        /// </summary>
        /// <param name="blocks"></param>
        /// <returns></returns>
        /// Format of instance IPL line is:
        ///   *inst_v4*, objtypeflag, #children
        /// 
        /// When we output LOD distance we output the actual LOD distance of 
        /// the object rather than -1 unless its per-instance based.
        ///   
        public override String ToIPLInst(String[] blocks)
        {
            Debug.Assert(!this.InstType.Equals(ObjectDef.InstanceType.None), "No instance type determined.  Contact tools.");
            
            Vector3f pos = this.Object.NodeTransform.Translation;
            Quaternionf rot = new Quaternionf(this.Object.NodeTransform);
            String block = this.Object.GetAttribute(AttrNames.OBJ_BLOCK_ID, AttrDefaults.OBJ_BLOCK_ID);
            int aoMult = this.Object.GetAttribute(AttrNames.OBJ_AMBIENT_OCCLUSION_MULTIPLIER, AttrDefaults.OBJ_AMBIENT_OCCLUSION_MULTIPLIER);
            int block_idx = 0;// Array.IndexOf(blocks, block) + 1;
            int children = 0;
            if (this.Object.HasLODChildren())
                children = this.Object.LOD.Children.Length;

            // Milo's use an inverted rotation for some reason.
            if (this.Object.IsMiloTri())
                rot.Invert();

            if (this.Object.HasLODParent())
                Debug.Assert(-1 != this.LODParentIndex, String.Format("Invalid LOD Parent Index for {0}.  Contact tools.", this.Object));

            String ipl_line = String.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}",
                this.Object.GetObjectName(), Flags.GetInstanceFlags(this.Object),
                pos.X, pos.Y, pos.Z, rot.X, rot.Y, rot.Z, rot.W,
                this.LODParentIndex, block_idx, this.LODDistance,
                (int)this.InstType, children, aoMult);
            return (ipl_line);
        }
        #endregion // Controller Methods

        #region Protected Methods
        /// <summary>
        /// Return instance LOD distance.
        /// </summary>
        /// <returns></returns>
        protected virtual float GetInstanceLODDistance(Util.IDEReaderBasic reader)
        {
            bool instDist = this.Object.GetAttribute(AttrNames.OBJ_INSTANCE_LOD_DISTANCE, AttrDefaults.OBJ_INSTANCE_LOD_DISTANCE);
            float currDist = float.MinValue;
            if (instDist && (this.Object.IsXRef() || this.Object.IsRefObject()))
            {
                currDist = (this.Object.GetLODDistance());
            }
            else if (this.Object.IsXRef() || this.Object.IsRefObject() ||
                     this.Object.IsInternalRef() || this.Object.IsRefInternalObject())
            {
                // XRefs without "Instance LOD distance" attribute have their LOD
                // distance set to -1 so it comes from the prop ModelInfo.
                currDist = -1.0f;
            }
            else if (this.Object.IsObject())
            {
                currDist = (this.Object.GetLODDistance());
            }

            // Now we clamp the LOD distance if the object is within a Scene 
            // LOD hierarchy.  In this case we return the highest LOD distance
            // of all our siblings.
            if (this.Object.HasLODParent())
            {
                // Loop through siblings finding highest LOD distance.
                foreach (TargetObjectDef sibling in this.Object.LOD.Parent.LOD.Children)
                {
                    if (this.Object.Equals(sibling))
                        continue; // Skip self.

                    float sibdist = sibling.GetLODDistance();
                    if (sibdist > currDist)
                        currDist = sibdist;
                }
            }
            return (currDist);
        }

        /// <summary>
        /// Determine the current instance's StreamMode (from ObjectDef attributes).
        /// </summary>
        /// <param name="reader"></param>
        /// Criteria for streaming mode selection:
        ///   Streamed: (default) 
        ///   StreamedBig:
        ///   Non-Streamed:
        protected override void DetermineStreamMode(Util.IDEReaderBasic reader)
        {
            bool alwaysloaded = this.Object.GetAttribute(AttrNames.OBJ_ALWAYS_LOADED, AttrDefaults.OBJ_ALWAYS_LOADED);
            bool instloddist = this.Object.GetAttribute(AttrNames.OBJ_INSTANCE_LOD_DISTANCE, AttrDefaults.OBJ_INSTANCE_LOD_DISTANCE);
            bool lodchildren = false;
            bool isRef = (this.Object.IsXRef() || this.Object.IsInternalRef() || this.Object.IsRefObject() || this.Object.IsRefInternalObject());

            if ((null != this.Object.LOD) && (this.Object.LOD.Children.Length > 0))
                lodchildren = true;

            if (this.Object.IsMiloTri())
                this.StreamMode = StreamModeType.NotStreamed;
            else if (this.Object.IsAnimProxy())
                this.StreamMode = StreamModeType.NotStreamed;
            else if (alwaysloaded || (this.LODDistance > sC_s_m_NonStreamedLODDistance) || lodchildren)
                this.StreamMode = StreamModeType.NotStreamed;
            else if ((this.LODDistance > sC_s_m_StreamedLongLODDistance) || this.Object.IsMiloTri())
                this.StreamMode = StreamModeType.StreamedBig;
        }
        #endregion // Protected Methods
    }

} // RSG.SceneXml.MapExport namespace

// Instance5.cs
