﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.Metadata.Util;
using RSG.ObjectLinks;
using RSG.SceneXml.MapExport.Algorithm;
using RSG.SceneXml.MapExport.Project.GTA5.Algorithm;
using RSG.SceneXml.MapExport.Project.GTA5.Collections;
using RSG.SceneXml.MapExport.Util;

namespace RSG.SceneXml.MapExport.Project.GTA5
{
	/// <summary>
	/// GTA IMAP Serialiser.
	/// </summary>
    public class SceneSerialiserIMAP : Framework.SceneSerialiserIMAP<IMAPContainer>
	{
		#region Constants
		protected const int IMAP_VERSION = 2;

		/// <summary>
		/// DateTime object ToString formatter.
		/// </summary>
		protected const String DATETIME_FORMAT = "f";

        protected const String HREF_DUPE_LOD_HIERARCHIES =
			"https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#Object_has_LOD_Children_and_Container_LODs_Defined";

        protected const String HREF_INVALID_CONTAINER =
			"https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#Invalid_Container_LOD";

        protected const String HREF_INVALID_INTERIOR_ENTITY =
			"https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#Invalid_Interior_Entity";

        protected const String HREF_EMPTY_PARENT_ENTITY =
			"https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#IMAP_Empty_Parent_Entity";

		private const string HREF_INTERIOR_NOT_FOUND_ENTITY =
			"https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#Interior_File_.27name.27_could_not_be_found";

		protected const int MAX_ENTITIES_PER_IMAP_GROUP = 1000;

		protected const String EXTENSION_DEF_DOOR_PREFIX = "D_";
		#endregion // Constants

		#region Member Data
		/// <summary>
		/// "IPL Group" Container Structures (named)
		/// </summary>
		protected Dictionary<String, IMAPGroupContainer> GroupContainers;

		/// <summary>
		/// Named interior instances container map
		/// </summary>
		protected Dictionary<String, IMAPContainer> InteriorContainerMap;

		private IIMAPSplitAlgorithm<IMAPContainer> m_splitAlgorithm;
		private ITYPContainer itypContainer_;
		private ITYPContainer itypStreamedContainer_;
		private static readonly int maxOcclusionNodeDataSize_ = 32 * 1024;

		private List<IMAPContainer> manualStreamInteriorIMAPs_ = new List<IMAPContainer>();
		#endregion // Member Data

		#region Static Member Data
		/// <summary>
		/// Used for static serialisation helpers (created on first use).
		/// </summary>
		private static SceneSerialiserIMAP ms_ImapSerialiser;
		public static bool Parallelise = false;
		#endregion // Static Member Data

		#region Constructor(s)
		/// <summary>
		/// 
		/// </summary>
		/// <param name="branch"></param>
		/// <param name="scenes"></param>
		/// <param name="imapStreamDir"></param>
		/// <param name="builddir"></param>
		public SceneSerialiserIMAP(
			IBranch branch,
			Scene[] scenes, 
			ITYPContainer itypContainer, 
			ITYPContainer itypStreamedContainer, 
			String imapStreamDir, 
			String builddir, 
			Framework.InteriorContainerNameMap interiorNameMap,
			String[] nonSerialisedTimeCycleTypes)
			: base(branch, scenes, imapStreamDir, builddir, interiorNameMap, nonSerialisedTimeCycleTypes)
		{
			// The ityp container members will be null whenever we haven't build ityp data and this must be handled throughout - JWR
			itypContainer_ = itypContainer;
			itypStreamedContainer_ = itypStreamedContainer;
			Reset();
		}

		/// <summary>
		/// Private constructor for use by static serialisation helpers.
		/// </summary>
		private SceneSerialiserIMAP(IBranch branch)
			: base(branch, new Scene[]{}, String.Empty, String.Empty, new InteriorContainerNameMap(), new String[0])
		{
			Reset();
		}
		#endregion // Constructor(s)

		#region Controller Methods
		public override bool Process(String pathname)
		{
			Log.Log__Message("Starting IMAP processing:");
			Log.Log__Message("\tScenes ({0}):", this.Scenes.Length);
			foreach (Scene scene in this.Scenes)
				Log.Log__Message("\t\t{0}", scene.Filename);

			String basename = Path.GetFileNameWithoutExtension(pathname);
            this.StaticContainer.Name = MapAsset.AppendMapPrefixIfRequired(this.Branch.Project, basename);
		    this.StaticContainer.Parent = this.StaticContainer;
			ProcessScenes(this.Scenes);
			this.StaticContainer.Entities.Sort();

			if (this.OcclusionContainer.HasData)
			{
				Log.Log__Message("Processed occlusion data. Skipping any other data.");
				return true;
			}

			// Set prefix for "streamed" IMAP.
		    String prefix = MapAsset.AppendMapPrefixIfRequired(this.Branch.Project, string.Format("{0}_strm", basename));
			m_splitAlgorithm = new IMAPSplitQuadTree();
			m_splitAlgorithm.Split(prefix, this.SceneStreamedContainer, out this.StreamedContainers, false, Entity.StreamModeType.Streamed);

			// Set prefix for "streamed-long" IMAP.
            prefix = MapAsset.AppendMapPrefixIfRequired(this.Branch.Project, string.Format("{0}_long", basename));

			// GTA5 Bug #217920; in order to check the stream longs too...
			// Now we migrate any unallocated car generators into the stream long pile.
			foreach (CarGen cg in this.SceneStreamedContainer.CarGens)
			{
				if (cg.HasContainer)
					continue;
				this.SceneStreamedLongContainer.AddCarGen(cg);
			}
			m_splitAlgorithm.Split(prefix, this.SceneStreamedLongContainer, out this.StreamedLongContainers, true, Entity.StreamModeType.StreamedLong);

			// Set prefix for "critical" IMAP.
            prefix = MapAsset.AppendMapPrefixIfRequired(this.Branch.Project, string.Format("{0}_critical", basename));
			m_splitAlgorithm.Split(prefix, this.SceneStreamedCriticalContainer, out this.StreamedCriticalContainers, false, Entity.StreamModeType.StreamedCritical);

			// Setup Parents; before parent is serialised otherwise it doesn't
			// get the Is Parent flag.
			foreach (IMAPContainer container in this.StreamedContainers)
				container.Parent = this.StaticContainer;
			foreach (IMAPContainer container in this.StreamedLongContainers)
				container.Parent = this.StaticContainer;
			foreach (IMAPContainer container in this.StreamedCriticalContainers)
				container.Parent = this.StaticContainer;

			// Setup ITYP container links
			if ((itypContainer_ != null) && (itypContainer_.ArchetypeCount > 0))
				AddContainerDependency(this.StaticContainer, itypContainer_);

			if ((itypStreamedContainer_ != null) && (itypStreamedContainer_.ArchetypeCount > 0))
			{
				foreach (IMAPContainer container in this.StreamedContainers)
				{
					bool imapHasLocalEntities = container.Entities.Any(e => !e.Object.IsRefObject());
					if (imapHasLocalEntities)
						AddContainerDependency(container, itypStreamedContainer_);                    
				}
				foreach (IMAPContainer container in this.StreamedLongContainers)
				{
					bool imapHasLocalEntities = container.Entities.Any(e => !e.Object.IsRefObject());
					if (imapHasLocalEntities)
						AddContainerDependency(container, itypStreamedContainer_);
				}
				foreach (IMAPContainer container in this.StreamedCriticalContainers)
				{
					bool imapHasLocalEntities = container.Entities.Any(e => !e.Object.IsRefObject());
					if (imapHasLocalEntities)
						AddContainerDependency(container, itypStreamedContainer_);
				}
			}

			foreach (IMAPGroupContainer container in this.GroupContainers.Values)
			{
                bool imapGroupStaticSLODHasLocalEntities = container.StaticSLOD.Entities.Any(e => !e.Object.IsRefObject());
                bool imapGroupStaticHasLocalEntities = container.Static.Entities.Any(e => !e.Object.IsRefObject());
				bool imapGroupStreamHasLocalEntities = container.Streamed.Entities.Any(e => !e.Object.IsRefObject());

                // Setup ITYP container dependencies.
                if (imapGroupStaticSLODHasLocalEntities &&
                    (itypContainer_ != null) && (itypContainer_.ArchetypeCount > 0))
                    AddContainerDependency(container.StaticSLOD, itypContainer_);
				if (imapGroupStaticHasLocalEntities &&
					(itypContainer_ != null) && (itypContainer_.ArchetypeCount > 0))
					AddContainerDependency(container.Static, itypContainer_);
				if (imapGroupStreamHasLocalEntities &&
					(itypStreamedContainer_ != null) && (itypStreamedContainer_.ArchetypeCount > 0))
					AddContainerDependency(container.Streamed, itypStreamedContainer_);
			}

			return true;
		}

		/// <summary>
		/// Add IMAP container ITYP dependency (incl. support for core/DLC differences).
		/// </summary>
		/// <param name="container"></param>
		/// <param name="itypContainer"></param>
		private void AddContainerDependency(IMAPContainer container, ITYPContainer itypContainer)
		{
			AddContainerDependency(container, itypContainer.Name);
		}

		/// <summary>
		/// Add IMAP container ITYP dependency (incl. support for core/DLC differences).
		/// </summary>
		/// <param name="container"></param>
		/// <param name="itypName"></param>
		private void AddContainerDependency(IMAPContainer container, String itypName)
		{
			// Add the generated streamed ITYP dependency.
			container.AddDependency(itypName);
		}

		/// <summary>
		/// Process internal structures to ITYP file(s)
		/// </summary>
		public override bool Serialise(String pathname)
		{
			Log.Log__Message("Starting IMAP serialisation:");

			// Process occlusion
			if (this.OcclusionContainer.HasData)
			{
				this.OcclusionContainer.TranslatePlaceholders(this.Scenes);
				if (this.OcclusionContainer.DataSize == 0)
				{
					Log.Log__ErrorCtx(this.StaticContainer.Name, "Occluders within {0} does not have any data within it.  Occluders may not have exported correctly.", this.StaticContainer.Name);
					return false;
				}

				Occlusion.Node[] splitNodes = this.OcclusionContainer.Split(maxOcclusionNodeDataSize_);
				// Build a new IMAP container for each occlusion node
				int occlIndex = 0;
				foreach (Occlusion.Node splitNode in splitNodes)
				{
					String occlContainerName = String.Format("{0}_{1}", this.StaticContainer.Name, occlIndex.ToString("00"));
					IMAPContainer occlContainer = new IMAPContainer(occlContainerName);
					occlContainer.OcclusionNode = splitNode;
					occlContainer.EntityBoundingBox.Expand(splitNode.WorldBoundingBox);
					occlContainer.StreamingBoundingBox.Expand(splitNode.WorldBoundingBox);
					if (this.OcclusionContainer.StreamingExtentsOverride != null)
						occlContainer.StreamingBoundingBox.Expand(this.OcclusionContainer.StreamingExtentsOverride);
					String imapPathname = Path.Combine(this.IMAPStreamDirectory, occlContainerName + ".imap");
					Write(imapPathname, occlContainer);
					++occlIndex;
				}

				Tuple<String, Occlusion.Node>[] imapGroupNodePairs = this.OcclusionContainer.GetIMAPGroupNodes();
				foreach (Tuple<String, Occlusion.Node> imapGroupNodePair in imapGroupNodePairs)
				{
					String occlContainerName = imapGroupNodePair.Item1;
					IMAPContainer occlContainer = new IMAPContainer(occlContainerName);
                    occlContainer.Flags = Framework.IMAP_eFlags.F_MANUAL_STREAM_ONLY;
					occlContainer.OcclusionNode = imapGroupNodePair.Item2;
					occlContainer.EntityBoundingBox.Expand(imapGroupNodePair.Item2.WorldBoundingBox);
					occlContainer.StreamingBoundingBox.Expand(imapGroupNodePair.Item2.WorldBoundingBox);
					if (this.OcclusionContainer.StreamingExtentsOverride != null)
						occlContainer.StreamingBoundingBox.Expand(this.OcclusionContainer.StreamingExtentsOverride);
					String imapPathname = Path.Combine(this.IMAPStreamDirectory, occlContainerName + ".imap");
					Write(imapPathname, occlContainer);
				}

				return true;
			}

			// Write Static IMAP
			Write(pathname, this.StaticContainer);

			// Write Stream IMAPs
			int n = 0;
			foreach (IMAPContainer container in this.StreamedContainers)
			{
				String streamFilename = Path.Combine(this.IMAPStreamDirectory,
					Path.GetFileNameWithoutExtension(pathname)) + String.Format("_strm_{0}.imap", n++);
				Write(streamFilename, container);
			}

			// Write Stream Long IMAPs
			n = 0;
			foreach (IMAPContainer container in this.StreamedLongContainers)
			{
				String streamFilename = Path.Combine(this.IMAPStreamDirectory,
					Path.GetFileNameWithoutExtension(pathname)) + String.Format("_long_{0}.imap", n++);
				Write(streamFilename, container);
			}

			// Write Stream Critical IMAPs
			n = 0;
			foreach (IMAPContainer container in this.StreamedCriticalContainers)
			{
				String streamFilename = Path.Combine(this.IMAPStreamDirectory,
					Path.GetFileNameWithoutExtension(pathname)) + String.Format("_critical_{0}.imap", n++);
				Write(streamFilename, container);
			}

			// Write Interior IMAPs (if we have any)
			foreach (KeyValuePair<String, IMAPContainer> interiorContainerPair in InteriorContainerMap)
			{
				string interiorIMAPFilename = null;
				if (IsManualStreamInteriorIMAP(interiorContainerPair.Value))
					interiorIMAPFilename = interiorContainerPair.Key + ".imap";
				else
					interiorIMAPFilename = Path.GetFileNameWithoutExtension(pathname) + "_" + interiorContainerPair.Key + ".imap";

				string interiorIMAPPathname = Path.Combine(this.IMAPStreamDirectory, interiorIMAPFilename);
				Write(interiorIMAPPathname, interiorContainerPair.Value);
			}

			// Write "IPL Group" IMAPs
			foreach (IMAPGroupContainer container in this.GroupContainers.Values)
			{
                // We only export "IPL Groups" for DLC when they are new for DLC.
                bool hasNewDLCSLODStaticEntity = container.StaticSLOD.Entities.Any(e => e.Object.IsNewDLCObject());
                bool hasNewDLCStaticEntity = container.Static.Entities.Any(e => e.Object.IsNewDLCObject());
                bool hasNewDLCStreamedEntity = container.Streamed.Entities.Any(e => e.Object.IsNewDLCObject());
                bool hasNewDLCInteriorEntity = container.InteriorContainerMap.Any(c => c.Value.Entities.Any(e => e.Object.IsNewDLCObject()));
                if (this.IsDLC && !hasNewDLCSLODStaticEntity && !hasNewDLCStaticEntity && !hasNewDLCStreamedEntity && !hasNewDLCInteriorEntity)
                    continue; // Skip.

                // Determine prefixed group name.
                String groupName = MapAsset.AppendMapPrefixIfRequired(this.Branch.Project, 
                    container.Name);

                // Prevent empty SLOD IMAP from being created.
                if (container.StaticSLOD.Entities.Count > 0)
                {
                    String groupLodFilename = Path.Combine(this.IMAPStreamDirectory,
                        String.Format("{0}_slod.imap", groupName));
                    container.StaticSLOD.Entities.Sort();
                    Write(groupLodFilename, container.StaticSLOD);
                }
                else
                {
                    container.Static.Parent = container.Static;
                }

				// B* 283080 - Prevent empty ipl lod IMAPs from being created
				if (container.Static.Entities.Count > 0)
				{
					String groupLodFilename = Path.Combine(this.IMAPStreamDirectory,
                        String.Format("{0}_lod.imap", groupName));
					container.Static.Entities.Sort();
					Write(groupLodFilename, container.Static);
				}
				else
				{
					// B* 653501 - prevent invalid IMAP parent references
					container.Streamed.Parent = container.Streamed;
				}

				if (container.NumEntities() == 0)
				{
					Log.Log__WarningCtx(container.Name, "IMAP Group definition {0} has no objects attached. Misspelled group name?", container.Name);
				}
				else if (container.NumEntities() > MAX_ENTITIES_PER_IMAP_GROUP)
				{
					Log.Log__ErrorCtx(
						container.Name,
						"IMAP Group {0} has {1} entities attached.  There should be no more than {2}. Please split up.",
						container.Name,
						container.NumEntities(),
						MAX_ENTITIES_PER_IMAP_GROUP);
				}

                // Don't write out top-level unless there are entities; this is to get round a problem
                // where different Input Groups are specified for the dt1_carrmp and dt1_carrmp_lod
                // - the map pipeline should change such that all SceneXml files are processed together!
                if (container.Streamed.Entities.Any())
                {
                    String groupFilename = Path.Combine(this.IMAPStreamDirectory,
                        MapAsset.AppendMapPrefixIfRequired(this.Project, string.Format("{0}.imap", groupName)));
                    Write(groupFilename, container.Streamed);
                }

				// Interior IMAPs for "IPL Group".
				foreach (KeyValuePair<String, IMAPContainer> interiorContainerPair in container.InteriorContainerMap)
				{
                    String interiorGroupName = MapAsset.AppendMapPrefixIfRequired(this.Branch.Project,
                        interiorContainerPair.Key);
					String interiorIMAPFilename = MapAsset.AppendMapPrefixIfRequired(this.Project, string.Format("{0}.imap", interiorGroupName));
					String interiorIMAPPathname = Path.Combine(this.IMAPStreamDirectory, interiorIMAPFilename);
					Write(interiorIMAPPathname, interiorContainerPair.Value);
				}
			}
		   
			Log.Log__Message("Done.");

			return true;
		}

		/// <summary>
		/// A specialized function to write the instance map data container.
		/// Since this container is generated procedurally, this is called after populating
		/// the IMAPContainer in the generator processor itself.
		/// </summary>
		/// <param name="instanceContainer"></param>
		/// <param name="pathname"></param>
		public void WriteInstanceMapDataContainer(IMAPContainer instanceContainer, String pathname)
		{
			// Write Instance Map Data Container
			Parallelise = true;
			this.InstanceMapDataContainer = instanceContainer;
			Write(pathname, this.InstanceMapDataContainer);
			Parallelise = false;
		}
		
		/// <summary>
		/// Writes an empty, valid IMAP definition to the specified pathname
		/// </summary>
		/// <param name="pathname"></param>
		public void WriteEmptyIMAPFile(String pathname)
		{
			Write(pathname, new IMAPContainer());
		}

		/// <summary>
		/// A specialized function to write the instance map data container.
		/// Since this container is generated procedurally, this is called after populating
		/// the IMAPContainer in the generator processor itself.
		/// </summary>
		public MapDataInfo GetMapDataInfo()
		{
			MapDataInfo mapInfo = new MapDataInfo(this.StaticContainer.Name);
			mapInfo.EntityBoundingBox = this.BoundsXYZ;

			return mapInfo;
		}

		/// <summary>
		/// Add a "static" IMAP dependency (typically on ITYP data).
		/// </summary>
		/// <param name="depName"></param>
		public void AddStaticDependency(String depName)
		{
			this.StaticContainer.AddDependency(depName);
		}

		/// <summary>
		/// Replace an IMAP dependency (typically on ITYP data) across all IMAP
		/// containers.
		/// </summary>
		/// <param name="sourceName"></param>
		/// <param name="targetName"></param>
		public void ReplaceDependency(String sourceName, String targetName)
		{
			this.StaticContainer.ReplaceDependency(sourceName, targetName);

			foreach (IMAPContainer container in this.StreamedContainers)
				container.ReplaceDependency(sourceName, targetName);

			foreach (IMAPContainer container in this.StreamedLongContainers)
				container.ReplaceDependency(sourceName, targetName);

			foreach (IMAPContainer container in this.StreamedCriticalContainers)
				container.ReplaceDependency(sourceName, targetName);

			foreach (IMAPGroupContainer container in this.GroupContainers.Values)
				container.ReplaceDependency(sourceName, targetName);
		}

		/// <summary>
		/// Replace a "static" IMAP dependency (typically on ITYP data).
		/// </summary>
		/// <param name="sourceName"></param>
		/// <param name="targetNames"></param>
		public void ReplaceStaticDependency(String sourceName, IEnumerable<String> targetNames)
		{
			this.StaticContainer.ReplaceDependency(sourceName, targetNames);

		    foreach (IMAPGroupContainer container in this.GroupContainers.Values)
		    {
		        container.ReplaceDependencies(sourceName, targetNames);
		    }
		}

		/// <summary>
		/// Add any core-game dependencies into IMAP Containers.  Archetype cache
		/// map is passed in.
		/// </summary>
		/// <param name="archetypeToITYPMap"></param>
		public void AddDependenciesForCoreGame(IDictionary<String, String> archetypeToITYPMap)
		{
			AddDependenciesForCoreGame(this.StaticContainer, archetypeToITYPMap);
			
			foreach (IMAPContainer container in this.StreamedContainers)
				AddDependenciesForCoreGame(container, archetypeToITYPMap);

			foreach (IMAPContainer container in this.StreamedLongContainers)
				AddDependenciesForCoreGame(container, archetypeToITYPMap);

			foreach (IMAPContainer container in this.StreamedCriticalContainers)
				AddDependenciesForCoreGame(container, archetypeToITYPMap);

            // Interiors
            foreach (KeyValuePair<String, IMAPContainer> containerKvp in this.InteriorContainerMap)
            {
                AddDependenciesForCoreGame(containerKvp.Value, archetypeToITYPMap);
            }

            // IPL Groups
			foreach (IMAPGroupContainer container in this.GroupContainers.Values)
			{
                AddDependenciesForCoreGame(container.StaticSLOD, archetypeToITYPMap);
				AddDependenciesForCoreGame(container.Static, archetypeToITYPMap);
				AddDependenciesForCoreGame(container.Streamed, archetypeToITYPMap);
			}
		}

		/// <summary>
		/// Write manifest data to XmlDocument.
		/// </summary>
		/// <param name="xmlDoc"></param>
		/// <returns></returns>
		public override bool WriteManifestData(XDocument xmlDoc, IEnumerable<ManifestDependency> additionalDependencies)
		{
			// IMAP Group Containers
			foreach (IMAPGroupContainer imapCont in GroupContainers.Values)
			{
				XElement imapGroupElem = AsManifestIMAPGroup(imapCont);
				if (null != imapGroupElem)
					xmlDoc.Root.Add(imapGroupElem);

				foreach (KeyValuePair<String, IMAPContainer> kvp in imapCont.InteriorContainerMap)
				{
					String name = String.Format("{0}_{1}", this.StaticContainer.Name, kvp.Key);
					XElement imapDependency = AsManifestIMAPDependency(name, kvp.Value);
					if (null != imapDependency)
						xmlDoc.Root.Add(imapDependency);
				}
			}
			// OLD: Interior IMAP Dependencies
			foreach (KeyValuePair<String, IMAPContainer> kvp in this.InteriorContainerMap)
			{
				String name;
				if (IsManualStreamInteriorIMAP(kvp.Value))
					name = kvp.Key;
				else
					name = String.Format("{0}_{1}", this.StaticContainer.Name, kvp.Key);

				XElement imapDependency = AsManifestIMAPDependency(name, kvp.Value);
				if (null != imapDependency)
					xmlDoc.Root.Add(imapDependency);
			}

			// IMAP to ITYP dependencies
			WriteIMAPDependencyManifestData(xmlDoc);            

			// Write additional IMAP files.
			if (additionalDependencies != null)
			{
				foreach (ManifestDependency additionalDependency in additionalDependencies)
				{
					XElement imapDependency2 = AsManifestIMAPDependency2(additionalDependency);
					if (null != imapDependency2)
						xmlDoc.Root.Add(imapDependency2);
				}
			}

			return (true);
		}
		#endregion // Controller Methods

		#region Static Controller Methods
		/// <summary>
		/// Static method to get CEntityDef representation.
		/// </summary>
		/// <param name="branch"></param>
		/// <param name="entity"></param>
		/// <param name="parent"></param>
		/// <param name="entityIndex"></param>
		/// <returns></returns>
		public static XElement _AsCEntityDef(IBranch branch, Entity entity, Entity parent = null, int entityIndex = -1)
		{
			if (null == ms_ImapSerialiser)
				ms_ImapSerialiser = new SceneSerialiserIMAP(branch);
			return (ms_ImapSerialiser.AsCEntityDef(entity, parent, entityIndex));
		}
		#endregion // Static Controller Methods

		#region Protected Object-Serialisation Methods
		/// <summary>
		/// Write the IMAPContainer to disk file.
		/// </summary>
		/// <param name="filename"></param>
		/// <param name="container"></param>
		/// <returns></returns>
		public bool Write(String filename, IMAPContainer container)
		{
			// Write out IMAP file.
			container.AddPhysicsDictionariesForEntities();
			container.CheckExtentsConsistency();

			// B*1493947 - consider TC boxes if bounds are otherwise empty
			if (container.StreamingBoundingBox.IsEmpty && container.TimeCycleModifierBoxes.Length > 0)
				container.IncorporateTimeCycleBoxExtents();

			XDocument document = new XDocument(new XDeclaration("1.0", "utf-8", null));
			{
				StringBuilder description = new StringBuilder();
				description.Append("IMAP file generated from SceneXml files:\n");
			    foreach (Scene scene in this.Scenes)
			    {
			        description.AppendFormat("\t\t{0}{1}", scene.Filename, Environment.NewLine);
			    }
				description.AppendFormat("{0} entities, {1} cargens.", container.Entities.Count, container.CarGens.Length);
				XComment xmlDescription = new XComment(description.ToString());
				document.Add(xmlDescription);

				XElement node = AsCMapData("CMapData", container);
				Xml.CreateElementWithValueAttribute(node, "serialiserVersion", IMAP_VERSION);
				document.Add(node);

				document.Save(filename);
			}

			return (true);
		}


		/// <summary>
		/// Serialise a CMapData object to XML.
		/// </summary>
		/// <param name="doc"</param>
		/// <param name="container"></param>
		/// <returns></returns>
		protected XElement AsCMapData(String name, IMAPContainer container)
		{
			XElement xmlRoot = AsFwMapData(name, container);

			#region Entities
			XElement entitiesNode = xmlRoot.Elements().First(element => element.Name.LocalName == "entities");
			int n = 0;
			object counterLock = new object();

			ParallelOptions options = new ParallelOptions();
			options.MaxDegreeOfParallelism = Parallelise ? -1 : 1; //Set to -1 for maximum threads; set to 1 to debug a single thread at a time.
			Parallel.ForEach<Entity>(container.Entities, options, entity =>
			{
				// Set our LOD Parent Index.
				if (entity.Object.HasLODParent())
				{
					Debug.Assert(null != container.Parent,
						String.Format("Container {0} does not have parent set.", container.Name));

					if (entity.Object.LODLevel != ObjectDef.LodLevel.SLOD1)
					{
						if (null != entity.Parent)
						{
							entity.LODParentIndex = container.Parent.Entities.IndexOf(entity.Parent);
						}
						else
						{
							String message = String.Format("[art] Entity {0} has empty parent entity; is it set to Don't Export or Don't Add to IPL? (We need its IMAP index).  For more information see {1}.",
								entity.Object.Name, HREF_EMPTY_PARENT_ENTITY);
							Debug.Assert(null != entity.Parent, message);
							Log.Log__ErrorCtx(entity.Object.Name, message);
						}
					}
					else
					{
                        entity.LODParentIndex = Framework.InterContainerLODManager.Instance.GetSLOD2ParentIndex(entity.Object);
					}
				}


				XElement entityElement = null;
				if (entity is InteriorEntity)
				{
					entityElement = AsCMloInstanceDef(entity as InteriorEntity, null);
				}
				else
				{
					entityElement = AsCEntityDef(entity);
				}

				lock (counterLock)
				{
					entity.FileIndex = (uint)n;
					entitiesNode.Add(entityElement);

					++n;
				}
			});

			#endregion // Entities

			#region CarGens
			// Serialise CarGens
			XElement xmlCarGens = new XElement("carGenerators");
			foreach (CarGen cargen in container.CarGens)
			{
				XElement xmlCarGen = AsCCarGen(cargen);
				xmlCarGens.Add(xmlCarGen);
			}
			xmlRoot.Add(xmlCarGens);
			#endregion // CarGens

			#region TimeCycleModifiers
			// Serialise TimeCycle Modifiers
			XElement xmlTimeCycleMods = new XElement("timeCycleModifiers");
			foreach (TargetObjectDef obj in container.TimeCycleModifierBoxes)
			{
				XElement xmlTimeCycleMod = AsCTimeCycleModifierBox(obj);
				xmlTimeCycleMods.Add(xmlTimeCycleMod);
			}
			xmlRoot.Add(xmlTimeCycleMods);
			#endregion // TimeCycleModifiers

			#region Block
			// Serialise Block
			if (container.Blocks.Length > 0)
			{
				XElement xmlBlock = AsCBlockDesc(container.Blocks[0], container.Name);
				xmlRoot.Add(xmlBlock);
			}
			#endregion // Block

			#region Instance Entities
			if (container.InstanceEntityData != null)
			{
				XElement xmlInstanceMapData = AsCInstancedMapData(container.InstanceEntityData);
				xmlRoot.Add(xmlInstanceMapData);
			}
			#endregion // Instance Entities

			return (xmlRoot);
		}

		/// <summary>
		/// Serialise a CEntityDef object to XML (relative to parent transform).
		/// </summary>
		/// <param name="entity"></param>
		/// <param name="parent"></param>
		/// <param name="entityIndex"></param>
		/// <returns></returns>
		protected XElement AsCEntityDef(Entity entity, Entity parent = null, int entityIndex = -1)
		{
			int aoMult = entity.Object.GetAttribute(AttrNames.OBJ_AMBIENT_OCCLUSION_MULTIPLIER, AttrDefaults.OBJ_AMBIENT_OCCLUSION_MULTIPLIER);
			int aaoMult = entity.Object.GetAttribute(AttrNames.OBJ_ARTIFICIAL_AMBIENT_OCCLUSION_MULTIPLIER, AttrDefaults.OBJ_ARTIFICIAL_AMBIENT_OCCLUSION_MULTIPLIER);
			int tint = entity.Object.GetAttribute(AttrNames.OBJ_TINT, AttrDefaults.OBJ_TINT);
			int lightGroup = entity.Object.GetAttribute(AttrNames.OBJ_LIGHT_GROUP, AttrDefaults.OBJ_LIGHT_GROUP);

			XElement xmlItem = AsFwEntityDef(entity, parent, entityIndex);
			xmlItem.SetAttributeValue("type", "CEntityDef");

			Xml.CreateElementWithValueAttribute(xmlItem, "ambientOcclusionMultiplier", aoMult);
			Xml.CreateElementWithValueAttribute(xmlItem, "artificialAmbientOcclusion", aaoMult);
			Xml.CreateElementWithValueAttribute(xmlItem, "tintValue", tint);
			Xml.CreateElementWithValueAttribute(xmlItem, "lightGroup", lightGroup);

			// GunnarD: Map extension serialisation.
			XElement xmlExtensions = new XElement("extensions");

			WalkChildren4Extensions(xmlExtensions, entity.Object);

			if (!xmlExtensions.IsEmpty)
				xmlItem.Add(xmlExtensions);
			
			return (xmlItem);
		}

		/// <summary>
		/// Serialise a CMloInstanceDef object to XML (instance of a MILO interior).
		/// </summary>
		/// <param name="doc"></param>
		/// <param name="instance"></param>
		/// <returns></returns>
		protected XElement AsCMloInstanceDef(InteriorEntity instance, ITarget target)
		{
			// For gathering information about the interior archetype.
			Archetype interiorArchetype = null;
			
			int groupid = AttrDefaults.MILOTRI_GROUPID;
			int floorid = AttrDefaults.MILOTRI_FLOORID;
			if (instance.Object.IsAttributeClass(AttrClass.MILOTRI))// instance.Object is a MiloTri
			{
				interiorArchetype = new Archetype(instance.Object);
				groupid = instance.Object.GetAttribute(AttrNames.MILOTRI_GROUPID, AttrDefaults.MILOTRI_GROUPID);
				floorid = instance.Object.GetAttribute(AttrNames.MILOTRI_FLOORID, AttrDefaults.MILOTRI_FLOORID);
			}
			else if (instance.Object.RefObject != null && instance.Object.RefObject.IsAttributeClass(AttrClass.MILOTRI))// instance.Object references a MiloTri
			{
				string miloName = instance.Object.RefObject.GetObjectName();
				TargetObjectDef miloObject = instance.Object.RefObject.MyScene.FindObject(miloName);
				interiorArchetype = new Archetype(miloObject);
				groupid = instance.Object.GetAttribute(AttrNames.OBJ_GROUPID, AttrDefaults.OBJ_GROUPID);// B* 567746 - support for per instance group id - JWR
				floorid = instance.Object.RefObject.GetAttribute(AttrNames.MILOTRI_FLOORID, AttrDefaults.MILOTRI_FLOORID);
			}
			else if (instance.Object.IsMilo())
			{
				interiorArchetype = new Archetype(instance.Object);
				groupid = instance.Object.GetAttribute(AttrNames.MILO_GROUPID, AttrDefaults.MILO_GROUPID);
				floorid = AttrDefaults.MILOTRI_FLOORID;
			}
			else
			{
				Log.Log__WarningCtx(instance.Object.Name, "Adding CMloInstanceDef for object '{0}' that is neither a Milo, MiloTri nor a reference to a MiloTri.", instance.Object.Name);
			}

			// Get the default entity sets
			IEnumerable<string> defaultEntitySets;
			if (instance.Object.IsEntity())
			{
				string defaultEntitySetsText = instance.Object.GetAttribute(AttrNames.OBJ_DEFAULT_ENTITY_SETS, AttrDefaults.OBJ_DEFAULT_ENTITY_SETS);
				defaultEntitySets = defaultEntitySetsText.Split(';').Where(token => !String.IsNullOrWhiteSpace(token));
			}
			else
			{
				defaultEntitySets = new string[0];
			}

			// B* 724578 - calculate numExitPortals
			int numExitPortals = 0;
			if (interiorArchetype != null)
			{
				Dictionary<TargetObjectDef, MiloRoom> rooms;
				String[] roomList;
				TargetObjectDef limbo;
				List<Archetype> entries;
				SceneSerialiserITYP.MiloPreParseRooms(interiorArchetype, out rooms, out roomList, out limbo, out entries);

				Dictionary<TargetObjectDef, MiloPortal> portals;
				SceneSerialiserITYP.MiloPreParsePortals(interiorArchetype, limbo, ref rooms, out portals);

				numExitPortals = portals.Values.Count(portal => (false == portal.Portal.GetAttribute(AttrNames.MLOPORTAL_MLO_LINK, AttrDefaults.MLOPORTAL_MLO_LINK) &&
																(portal.RoomA == null || portal.RoomB == null)));
			}

			XElement xmlItem = AsCEntityDef(instance);
			xmlItem.SetAttributeValue("type", "CMloInstanceDef");
			Xml.CreateElementWithValueAttribute(xmlItem, "groupId", groupid);
			Xml.CreateElementWithValueAttribute(xmlItem, "floorId", floorid);
			if (defaultEntitySets.Any())
			{
				XElement defaultEntitySetsElement = new XElement("defaultEntitySets");
				foreach (string defaultEntitySet in defaultEntitySets)
					Xml.CreateElementWithText(defaultEntitySetsElement, "Item", defaultEntitySet);
				xmlItem.Add(defaultEntitySetsElement);
			}

			Xml.CreateElementWithValueAttribute(xmlItem, "numExitPortals", numExitPortals);

			// B*1390145 - MLO isntance specific flags
			UInt32 flags = Flags.GetMiloInstanceFlags(instance.Object);
			Xml.CreateElementWithValueAttribute(xmlItem, "MLOInstflags", flags);
			
			return (xmlItem);
		}

		/// <summary>
		/// Serialise a CCarGen object to XML.
		/// </summary>
		/// <param name="doc"></param>
		/// <param name="cargen"></param>
		/// <returns></returns>
		protected XElement AsCCarGen(CarGen cargen)
		{
			Vector3f pos = cargen.Object.NodeTransform.Translation;
			String model = cargen.Object.GetAttribute(AttrNames.CARGEN_MODEL, AttrDefaults.CARGEN_MODEL);
			int col1 = cargen.Object.GetAttribute(AttrNames.CARGEN_COLOUR1, AttrDefaults.CARGEN_COLOUR1);
			int col2 = cargen.Object.GetAttribute(AttrNames.CARGEN_COLOUR2, AttrDefaults.CARGEN_COLOUR2);
			int col3 = cargen.Object.GetAttribute(AttrNames.CARGEN_COLOUR3, AttrDefaults.CARGEN_COLOUR3);
			int col4 = cargen.Object.GetAttribute(AttrNames.CARGEN_COLOUR4, AttrDefaults.CARGEN_COLOUR4);
			int alarmChance = cargen.Object.GetAttribute(AttrNames.CARGEN_ALARM_CHANCE, AttrDefaults.CARGEN_ALARM_CHANCE);
			int lockChance = cargen.Object.GetAttribute(AttrNames.CARGEN_LOCKED_CHANCE, AttrDefaults.CARGEN_LOCKED_CHANCE);
			UInt32 flags = Flags.GetCarGenFlags(cargen.Object);
			float width = cargen.Object.GetParameter(ParamNames.CARGEN_WIDTH, ParamDefaults.CARGEN_WIDTH);
			float length = cargen.Object.GetParameter(ParamNames.CARGEN_DEPTH, ParamDefaults.CARGEN_DEPTH);
			String modelSet = cargen.Object.GetAttribute(AttrNames.CARGEN_MODEL_SET, AttrDefaults.CARGEN_MODEL_SET);

			Matrix34f mat = cargen.Object.NodeTransform.Inverse();
			mat.Translation = new Vector3f();
			Vector3f forward = new Vector3f(0.0f, length, 0.0f);
			Vector3f right = new Vector3f(width, 0.0f, 0.0f);
			forward = mat * forward;
			right = mat * right;

			XElement xmlCarGen = new XElement("Item");
			Xml.CreateElementWithVectorAttributes(xmlCarGen, "position", pos);
			Xml.CreateElementWithValueAttribute(xmlCarGen, "orientX", forward.X);
			Xml.CreateElementWithValueAttribute(xmlCarGen, "orientY", forward.Y);
			Xml.CreateElementWithValueAttribute(xmlCarGen, "perpendicularLength", right.Magnitude());
			Xml.CreateElementWithText(xmlCarGen, "carModel", model);
			Xml.CreateElementWithValueAttribute(xmlCarGen, "flags", flags);
			Xml.CreateElementWithValueAttribute(xmlCarGen, "alarmChance", alarmChance);
			Xml.CreateElementWithValueAttribute(xmlCarGen, "lockedChance", lockChance);
			Xml.CreateElementWithValueAttribute(xmlCarGen, "bodyColorRemap1", col1);
			Xml.CreateElementWithValueAttribute(xmlCarGen, "bodyColorRemap2", col2);
			Xml.CreateElementWithValueAttribute(xmlCarGen, "bodyColorRemap3", col3);
			Xml.CreateElementWithValueAttribute(xmlCarGen, "bodyColorRemap4", col4);

			// This is called Model Set in the vehiclemodelsets.meta file, the psc has it as popGroup:
			// <string name="m_popGroup" type="atHashString"/>
			Xml.CreateElementWithText(xmlCarGen, "popGroup", modelSet);

			return (xmlCarGen);
		}

		/// <summary>
		/// Serialise a CTimeCycleModifier object to XML.
		/// </summary>
		/// <param name="doc"></param>
		/// <param name="obj"></param>
		/// <returns></returns>
		protected XElement AsCTimeCycleModifierBox(TargetObjectDef obj)
		{
			float percentage = (float)obj.GetAttribute(AttrNames.TWODFX_TCYC_BOX_PERCENTAGE, AttrDefaults.TWODFX_TCYC_BOX_PERCENTAGE);
			int start_hour = (int)obj.GetAttribute(AttrNames.TWODFX_TCYC_BOX_START_HOUR, AttrDefaults.TWODFX_TCYC_BOX_START_HOUR);
			int end_hour = (int)obj.GetAttribute(AttrNames.TWODFX_TCYC_BOX_END_HOUR, AttrDefaults.TWODFX_TCYC_BOX_END_HOUR);
			String id_str = (String)obj.GetAttribute(AttrNames.TWODFX_TCYC_BOX_IDSTRING, AttrDefaults.TWODFX_TCYC_BOX_IDSTRING);
			float range = (float)obj.GetAttribute(AttrNames.TWODFX_TCYC_BOX_RANGE, AttrDefaults.TWODFX_TCYC_BOX_RANGE);
			Vector3f bbmin = obj.WorldBoundingBox.Min;
			Vector3f bbmax = obj.WorldBoundingBox.Max;
			Vector3f min = new Vector3f(Math.Min(bbmin.X, bbmax.X), Math.Min(bbmin.Y, bbmax.Y), Math.Min(bbmin.Z, bbmax.Z));
			Vector3f max = new Vector3f(Math.Max(bbmin.X, bbmax.X), Math.Max(bbmin.Y, bbmax.Y), Math.Max(bbmin.Z, bbmax.Z));

			XElement xmlTimeCycleMod = new XElement("Item");
			Xml.CreateElementWithText(xmlTimeCycleMod, "name", id_str);
			Xml.CreateElementWithVectorAttributes(xmlTimeCycleMod, "minExtents", min);
			Xml.CreateElementWithVectorAttributes(xmlTimeCycleMod, "maxExtents", max);
			Xml.CreateElementWithValueAttribute(xmlTimeCycleMod, "percentage", percentage);
			Xml.CreateElementWithValueAttribute(xmlTimeCycleMod, "range", range);
			Xml.CreateElementWithValueAttribute(xmlTimeCycleMod, "startHour", start_hour);
			Xml.CreateElementWithValueAttribute(xmlTimeCycleMod, "endHour", end_hour);

			return (xmlTimeCycleMod);
		}

		/// <summary>
		/// Serialise a CBlockDesc object to XML.
		/// </summary> 
		/// <param name="doc"></param>
		/// <param name="block"></param>
		/// <returns></returns>
		protected XElement AsCBlockDesc(Block block, String blockName)
		{
			XElement xmlBlock = new XElement("block");
			Xml.CreateElementWithValueAttribute(xmlBlock, "version", 0);
			Xml.CreateElementWithValueAttribute(xmlBlock, "flags", block.Flags.ToString("d"));
			Xml.CreateElementWithText(xmlBlock, "name", blockName);
			Xml.CreateElementWithText(xmlBlock, "exportedBy", block.Scene.ExportUser);
			Xml.CreateElementWithText(xmlBlock, "owner", block.Owner);
			Xml.CreateElementWithText(xmlBlock, "time", block.Scene.ExportTimestamp.ToString(DATETIME_FORMAT));

			return (xmlBlock);
		}

		/// <summary>
		/// Serialise a CExtensionDef object to XML.
		/// </summary>
		/// <param name="extension"></param>
		/// <returns></returns>
		protected XElement AsCExtensionDefCommon(Extension extension)
		{
			XElement xmlItem = AsFwExtensionDef(extension);
			xmlItem.SetAttributeValue("type", "CExtensionDef");

			Matrix34f mtxParent = new Matrix34f();
			if (extension.Object.HasParent())
				mtxParent = extension.Object.Parent.NodeTransform.Inverse();
			Matrix34f mtxLocal = extension.Object.NodeTransform * mtxParent;
			Vector3f pos = mtxLocal.Translation;
			Xml.CreateElementWithVectorAttributes(xmlItem, "offsetPosition", pos);

			return (xmlItem);
		}

		/// <summary>
		/// Serialise a CExtensionDefLightEffect object to XML.
		/// </summary>
		/// <param name="doc"></param>
		/// <param name="extension"></param>
		/// <returns></returns>
		protected XElement AsCExtensionDefLightEffect(Extension extension)
		{
			bool dontExport = extension.Object.GetAttribute(AttrNames.TWODFX_LIGHT_DONT_EXPORT, AttrDefaults.TWODFX_LIGHT_DONT_EXPORT);
			if (dontExport)
				return (null);

			if (null == extension.Object.RefObject)
			{
				Log.Log__WarningCtx(extension.Object.Name,
					"LightInstance override for '{0}' is not resolved.  Ignoring.", extension.Parent.Name);
				return null;
			}

			// fancy instance updating of inherited properties/attributes
			Dictionary<string, Object> firstPBlock = extension.Object.ParamBlocks[0];
			Object[] instProps = firstPBlock["instancePropNameArray".ToLower()] as Object[];
			List<string> instPropStrings = new List<string>();
			if (null != instProps)
			{
				for (int propIndex = 0; propIndex < instProps.Length; propIndex++)
					instPropStrings.Add(((string)instProps[propIndex]).ToLower());
			}
			Object[] instAttrs = extension.Object.ParamBlocks[0]["instanceAttributeNameArray".ToLower()] as Object[];
			string[] instAttrStrings = new string[0];
			if (null != instAttrs)
			{
				instAttrStrings = new string[instAttrs.Length];
				for (int attrIndex = 0; attrIndex < instAttrs.Length; attrIndex++)
					instAttrStrings[attrIndex] = ((string)instAttrs[attrIndex]).ToLower();
			}
			if (!instPropStrings.Any() && !instAttrStrings.Any())
				return null;

			// Regular attributes
			int flashiness = GetLightInstanceAttribute(extension.Object, AttrNames.TWODFX_LIGHT_FLASHINESS, AttrDefaults.TWODFX_LIGHT_FLASHINESS, instAttrStrings);
			float volinten = GetLightInstanceAttribute(extension.Object, AttrNames.TWODFX_LIGHT_VOLUME_INTENSITY, AttrDefaults.TWODFX_LIGHT_VOLUME_INTENSITY, instAttrStrings);
			float volsize = GetLightInstanceAttribute(extension.Object, AttrNames.TWODFX_LIGHT_VOLUME_SIZE, AttrDefaults.TWODFX_LIGHT_VOLUME_SIZE, instAttrStrings);
			int attach = GetLightInstanceAttribute(extension.Object, AttrNames.TWODFX_LIGHT_ATTACH, AttrDefaults.TWODFX_LIGHT_ATTACH, instAttrStrings);
			float corona_size = GetLightInstanceAttribute(extension.Object, AttrNames.TWODFX_LIGHT_CORONA_SIZE, AttrDefaults.TWODFX_LIGHT_CORONA_SIZE, instAttrStrings);
			float corona_inten = GetLightInstanceAttribute(extension.Object, AttrNames.TWODFX_LIGHT_CORONA_INTENSITY, AttrDefaults.TWODFX_LIGHT_CORONA_INTENSITY, instAttrStrings);
			float corona_zBias = GetLightInstanceAttribute(extension.Object, AttrNames.TWODFX_LIGHT_CORONA_ZBIAS, AttrDefaults.TWODFX_LIGHT_CORONA_ZBIAS, instAttrStrings);
			float loddist = GetLightInstanceAttribute(extension.Object, AttrNames.OBJ_LOD_DISTANCE, AttrDefaults.OBJ_LOD_DISTANCE, instAttrStrings);
			String shadowmap = GetLightInstanceAttribute(extension.Object, AttrNames.TWODFX_LIGHT_SHADOW_NAME, AttrDefaults.TWODFX_LIGHTPHOTO_SHADOW_NAME, instAttrStrings);
			
			int lightFadeDistance = GetLightInstanceAttribute(extension.Object, AttrNames.TWODFX_LIGHT_LIGHT_FADE_DISTANCE, AttrDefaults.TWODFX_LIGHT_LIGHT_FADE_DISTANCE, instAttrStrings);
			int volumeFadeDistance = GetLightInstanceAttribute(extension.Object, AttrNames.TWODFX_LIGHT_VOLUME_FADE_DISTANCE, AttrDefaults.TWODFX_LIGHT_VOLUME_FADE_DISTANCE, instAttrStrings);
			int shadowFadeDistance = GetLightInstanceAttribute(extension.Object, AttrNames.TWODFX_LIGHT_SHADOW_FADE_DISTANCE, AttrDefaults.TWODFX_LIGHT_SHADOW_FADE_DISTANCE, instAttrStrings);
			int specularFadeDistance = GetLightInstanceAttribute(extension.Object, AttrNames.TWODFX_LIGHT_SPECULAR_FADE_DISTANCE, AttrDefaults.TWODFX_LIGHT_SPECULAR_FADE_DISTANCE, instAttrStrings);

			float shadowNearClip = GetLightInstanceAttribute(extension.Object, AttrNames.TWODFX_LIGHT_SHADOW_NEAR_CLIP, AttrDefaults.TWODFX_LIGHT_SHADOW_NEAR_CLIP, instAttrStrings);

			int shadowBlur = (int)(255.0f * GetLightInstanceAttribute(extension.Object, AttrNames.TWODFX_LIGHT_SHADOW_BLUR, AttrDefaults.TWODFX_LIGHT_SHADOW_BLUR, instAttrStrings)); 
		   
			// clamp u8 values
			lightFadeDistance = Math.Max(lightFadeDistance, Byte.MinValue);
			lightFadeDistance = Math.Min(lightFadeDistance, Byte.MaxValue);
			volumeFadeDistance = Math.Max(volumeFadeDistance, Byte.MinValue);
			volumeFadeDistance = Math.Min(volumeFadeDistance, Byte.MaxValue);
			shadowFadeDistance = Math.Max(shadowFadeDistance, Byte.MinValue);
			shadowFadeDistance = Math.Min(shadowFadeDistance, Byte.MaxValue);
			specularFadeDistance = Math.Max(specularFadeDistance, Byte.MinValue);
			specularFadeDistance = Math.Min(specularFadeDistance, Byte.MaxValue);
			shadowBlur = Math.Min(shadowBlur, Byte.MaxValue);

			// Light Properties
			string shadowmapPropValue = ParamHelpers.GetLightInstanceParameter(extension.Object, ParamNames.TWODFX_LIGHT_SHADOW_MAP, ParamDefaults.TWODFX_LIGHT_SHADOW_MAP, instPropStrings);
			if (null != shadowmapPropValue && !System.IO.File.Exists(shadowmapPropValue))
			{
				Log.Log__WarningCtx(extension.Object.Name, "The light's ({0}) projection texture {1} does not exist on your machine. Won't be considered.", extension.Object.Name, shadowmapPropValue);
			}
			if (shadowmap == AttrDefaults.TWODFX_LIGHTPHOTO_SHADOW_NAME && null != shadowmapPropValue)
			{
				//if shadowmap texture name is set to default for a spot light, allow use of the 'Property' value
				shadowmap = shadowmapPropValue;
			}
			else if (shadowmapPropValue != null)
			{
				Log.Log__WarningCtx(extension.Object.Name, "Shadow map ATTRIBUTE on light {0} is set to non-default {1}. Overriding the artist's defined Projection map {2}!\n", extension.Object.Name, shadowmap, shadowmapPropValue);
			}
			shadowmap = System.IO.Path.GetFileNameWithoutExtension(shadowmap);

			uint shadowMapHash = 0;
			if (shadowmap != AttrDefaults.TWODFX_LIGHTPHOTO_SHADOW_NAME)
				shadowMapHash = (uint)RSG.ManagedRage.StringHashUtil.atStringHash(shadowmap, 0);

			float capsuleWidth = ParamHelpers.GetLightInstanceParameter(extension.Object, ParamNames.TWODFX_LIGHT_CAPSULE_WIDTH, ParamDefaults.TWODFX_LIGHT_CAPSULE_WIDTH, instPropStrings);

			Vector3f colour = ParamHelpers.GetLightInstanceParameter(extension.Object, ParamNames.TWODFX_LIGHT_COLOUR, ParamDefaults.TWODFX_LIGHT_COLOUR, instPropStrings);
			Vector3i intColour = (colour * 255).ToVector3i();
			float cone_inner = ParamHelpers.GetLightInstanceParameter(extension.Object, ParamNames.TWODFX_LIGHT_HOTSPOT, ParamDefaults.TWODFX_LIGHT_HOTSPOT, instPropStrings);
			float cone_outer = ParamHelpers.GetLightInstanceParameter(extension.Object, ParamNames.TWODFX_LIGHT_FALLSIZE, ParamDefaults.TWODFX_LIGHT_FALLSIZE, instPropStrings);
			float intensity = ParamHelpers.GetLightInstanceParameter(extension.Object, ParamNames.TWODFX_LIGHT_INTENSITY, ParamDefaults.TWODFX_LIGHT_INTENSITY, instPropStrings);
			float atten_end = ParamHelpers.GetLightInstanceParameter(extension.Object, ParamNames.TWODFX_LIGHT_ATTENUATION_END, ParamDefaults.TWODFX_LIGHT_ATTENUATION_END, instPropStrings);
			Vector4f cullPlane = ParamHelpers.GetLightInstanceParameter(extension.Object, ParamNames.TWODFX_LIGHT_CULLPLANE, ParamDefaults.TWODFX_LIGHT_CULLPLANE, instPropStrings);
			bool useCullPlane = ParamHelpers.GetLightInstanceParameter(extension.Object, ParamNames.TWODFX_LIGHT_USE_CULLPLANE, AttrDefaults.TWODFX_LIGHT_USE_CULLPLANE, instPropStrings);
			if (!useCullPlane)
				cullPlane.W = atten_end;
			Vector3f outerCol = ParamHelpers.GetLightInstanceParameter(extension.Object, ParamNames.TWODFX_LIGHT_VOL_OUTER_COLOUR, ParamDefaults.TWODFX_LIGHT_VOL_OUTER_COLOUR, instPropStrings);
			Vector3i intOuterColour = (outerCol * 255).ToVector3i();
			float outerColIntensity = ParamHelpers.GetLightInstanceParameter(extension.Object, ParamNames.TWODFX_LIGHT_VOL_OUTER_COLOUR_INTEN, ParamDefaults.TWODFX_LIGHT_VOL_OUTER_COLOUR_INTEN, instPropStrings);
			float outerColExponent = ParamHelpers.GetLightInstanceParameter(extension.Object, ParamNames.TWODFX_LIGHT_VOL_OUTER_COLOUR_EXP, ParamDefaults.TWODFX_LIGHT_VOL_OUTER_COLOUR_EXP, instPropStrings);

			cone_inner /= 2.0f;
			cone_outer /= 2.0f;
			uint flags = Flags.GetLightFlags(extension.Object);
			// write time flags. if not a time object write number bugger than possible daytime
			// defaulting to binary 111111100000000000000111
			// NOTE: TIME FLAGS ARE WRITTEN OUT WITH THE HOUR 24 BEING BIT 0!
			int timeFlags = (int)Flags.GetTimeFlags(extension.Object, 0xFE0007);
			if (timeFlags == 0)
				Log.Log__WarningCtx(extension.Object.Name, "Lightinstance {0} reference object {1} has no time set. The light won't be visible.", extension.Object.RefObject.Name, extension.Object.Name);

			int actualLightType = ParamHelpers.GetLightParam(extension.Object, ParamNames.TWODFX_LIGHT_TYPE, ParamDefaults.TWODFX_LIGHT_TYPE);
			Flags.TwodfxLightTypes lighttype = Flags.TwodfxLightTypes.TWODFX_LIGHT_STANDARD;
			if (actualLightType > (int)ParamNames.TWODFX_SCENEXML_LIGHTTYPES.TWODFX_SCENEXML_LIGHTTYPE_OMNI)
			{
				if (actualLightType == (int)ParamNames.TWODFX_SCENEXML_LIGHTTYPES.TWODFX_SCENEXML_LIGHTTYPE_CAPSULE)
					lighttype = Flags.TwodfxLightTypes.TWODFX_LIGHT_CAPSULE;
				else
					lighttype = Flags.TwodfxLightTypes.TWODFX_LIGHT_SPOT;
			}

			float expFalloff = ParamHelpers.GetLightInstanceParameter(extension.Object, ParamNames.TWODFX_LIGHT_EXP_FALLOFF, ParamDefaults.TWODFX_LIGHT_EXP_FALLOFF, instPropStrings);

			XElement xmlItem = AsFwExtensionDef(extension);
			xmlItem.SetAttributeValue("type", "CLightAttr");

			TargetObjectDef lightObj = extension.Object;

			//             Matrix34f matlocal = new Matrix34f(lightObj.NodeTransform);
			//             Matrix34f matparent = null;
			//             if (lightObj.HasParent())
			//                 matparent = new Matrix34f(lightObj.Parent.NodeTransform);
			//             else
			//                 matparent = new Matrix34f();
			// Get light's relative transform from parent
			Matrix34f localToWorldTransform = lightObj.NodeTransform.Transpose();// HACK - Transpose to work around data issue

			Matrix34f parentToWorldTransform = new Matrix34f();
			Matrix34f worldToParentTransform = new Matrix34f();
			if (lightObj.HasParent())
			{
				parentToWorldTransform = lightObj.Parent.NodeTransform.Transpose();// HACK - Transpose to work around data issue
				worldToParentTransform = parentToWorldTransform.Inverse();
			}

			Matrix34f localToParentTransform = localToWorldTransform * worldToParentTransform;
			Quaternionf parentSpaceOrientation = new Quaternionf(localToParentTransform);
			Vector3f direction = -localToParentTransform.C;
			Vector3f tangent = -localToParentTransform.A;

			//////////////////////////////////////////////////////////////////////////
			// Light imap spcial cases
			//////////////////////////////////////////////////////////////////////////

			bool isEnabled = ParamHelpers.GetLightInstanceParameter(extension.Object, ParamNames.TWODFX_LIGHT_ENABLED, ParamDefaults.TWODFX_LIGHT_ENABLED, instPropStrings);
			if (!isEnabled)
			{
				intensity = 0.0f;
				Log.Log__WarningCtx(extension.Object.Name, "The light {0} is set to not be enabled, which will still create extra overhead for the light override. Please make sure you know what you're doing.", extension.Object.Name);
			}

			Vector3f relPos = localToParentTransform.Translation;

			byte lightIstanceHash = extension.Object.RefObject.AttributeGuid.ToByteArray()[0];

			Vector3f cullPlaneRotPart = new Vector3f(cullPlane.X, cullPlane.Y, cullPlane.Z);
			// Calculating the cullplane orientation realtiv to the PARENT object
			Matrix34f inverseParentTranslation = extension.Object.Parent.NodeTransform.Inverse();
			Matrix34f nodeTranslation = extension.Object.NodeTransform;
			inverseParentTranslation = inverseParentTranslation.Transpose();
			nodeTranslation = nodeTranslation.Transpose();
			Matrix34f parentToLocalTransform = nodeTranslation * inverseParentTranslation;
			parentToLocalTransform.Translation = new Vector3f();
//            parentToLocalTransform = parentToLocalTransform.Inverse();
			parentToLocalTransform.Transform(cullPlaneRotPart);
			cullPlaneRotPart.Normalise();
			cullPlane.X = cullPlaneRotPart.X;
			cullPlane.Y = cullPlaneRotPart.Y;
			cullPlane.Z = cullPlaneRotPart.Z;

			//new ValueType defaults from //rage/gta5/dev/rage/framework/tools/src/cli/ragebuilder/gta_res.h

			XElement posElem = new XElement("posn");
			{
				Xml.CreateElementWithValueAttribute(posElem, "x", relPos.X);
				Xml.CreateElementWithValueAttribute(posElem, "y", relPos.Y);
				Xml.CreateElementWithValueAttribute(posElem, "z", relPos.Z);
			}
			xmlItem.Add(posElem);

			XElement colourElem = new XElement("colour");
			{
				Xml.CreateElementWithValueAttribute(colourElem, "r", intColour.X);
				Xml.CreateElementWithValueAttribute(colourElem, "g", intColour.Y);
				Xml.CreateElementWithValueAttribute(colourElem, "b", intColour.Z);
			}
			xmlItem.Add(colourElem);

			Xml.CreateElementWithValueAttribute(xmlItem, "intensity", intensity);
			Xml.CreateElementWithValueAttribute(xmlItem, "flashiness", flashiness);
			Xml.CreateElementWithValueAttribute(xmlItem, "flags", flags);
			Xml.CreateElementWithValueAttribute(xmlItem, "boneTag", attach);
			Xml.CreateElementWithValueAttribute(xmlItem, "lightType", (uint)lighttype);
			Xml.CreateElementWithValueAttribute(xmlItem, "groupId", 0);                                // dummy value, set by runtime
			Xml.CreateElementWithValueAttribute(xmlItem, "timeFlags", timeFlags);  
			
			XElement cullPlaneElem = new XElement("cullingPlane");
			{
				Xml.CreateElementWithValueAttribute(cullPlaneElem, "x", cullPlane.X);
				Xml.CreateElementWithValueAttribute(cullPlaneElem, "y", cullPlane.Y);
				Xml.CreateElementWithValueAttribute(cullPlaneElem, "z", cullPlane.Z);
				Xml.CreateElementWithValueAttribute(cullPlaneElem, "w", cullPlane.W);
			}
			xmlItem.Add(cullPlaneElem);

			Xml.CreateElementWithValueAttribute(xmlItem, "shadowBlur", shadowBlur);

            Xml.CreateElementWithValueAttribute(xmlItem, "falloff", atten_end);
            Xml.CreateElementWithValueAttribute(xmlItem, "falloffExponent", expFalloff);
            Xml.CreateElementWithValueAttribute(xmlItem, "volIntensity", volinten);
            Xml.CreateElementWithValueAttribute(xmlItem, "volSizeScale", volsize);
            
            XElement volOuterColourElem = new XElement("volOuterColour");
            {
                Xml.CreateElementWithValueAttribute(volOuterColourElem, "r", intOuterColour.X);
                Xml.CreateElementWithValueAttribute(volOuterColourElem, "g", intOuterColour.Y);
                Xml.CreateElementWithValueAttribute(volOuterColourElem, "b", intOuterColour.Z);
            }
            xmlItem.Add(volOuterColourElem);
           
            Xml.CreateElementWithValueAttribute(xmlItem, "volOuterColourIntensity", outerColIntensity);
            Xml.CreateElementWithValueAttribute(xmlItem, "volOuterExponent", outerColExponent);
            Xml.CreateElementWithValueAttribute(xmlItem, "lightHash", lightIstanceHash);
            Xml.CreateElementWithValueAttribute(xmlItem, "volOuterIntensity", 0);                      // dummy, to be replaced
            Xml.CreateElementWithValueAttribute(xmlItem, "lightFadeDistance", lightFadeDistance);
            Xml.CreateElementWithValueAttribute(xmlItem, "volumetricFadeDistance", volumeFadeDistance);
            Xml.CreateElementWithValueAttribute(xmlItem, "shadowNearClip", shadowNearClip);
            Xml.CreateElementWithValueAttribute(xmlItem, "shadowFadeDistance", shadowFadeDistance);
            Xml.CreateElementWithValueAttribute(xmlItem, "specularFadeDistance", specularFadeDistance);
            Xml.CreateElementWithValueAttribute(xmlItem, "coronaSize", corona_size);
            Xml.CreateElementWithValueAttribute(xmlItem, "coronaSizeAdjustNear", 0.0f);              // dummy, to be replaced
            Xml.CreateElementWithValueAttribute(xmlItem, "coronaSizeAdjustFar", 0.0f);               // dummy, to be replaced
            Xml.CreateElementWithValueAttribute(xmlItem, "coronaSizeAdjustDistance", 0.0f);          // dummy, to be replaced
            Xml.CreateElementWithValueAttribute(xmlItem, "coronaIntensity", corona_inten);
            Xml.CreateElementWithValueAttribute(xmlItem, "coronaZBias", corona_zBias);
            Xml.CreateElementWithValueAttribute(xmlItem, "projectedTextureKey", shadowMapHash);
            
            XElement dirElem = new XElement("direction");
            {
                Xml.CreateElementWithValueAttribute(dirElem, "x", direction.X);
                Xml.CreateElementWithValueAttribute(dirElem, "y", direction.Y);
                Xml.CreateElementWithValueAttribute(dirElem, "z", direction.Z);
            }
            xmlItem.Add(dirElem);
            
            Xml.CreateElementWithValueAttribute(xmlItem, "coneInnerAngle", cone_inner);
            
            XElement tangentElem = new XElement("tangent");
            {
                Xml.CreateElementWithValueAttribute(tangentElem, "x", tangent.X);
                Xml.CreateElementWithValueAttribute(tangentElem, "y", tangent.Y);
                Xml.CreateElementWithValueAttribute(tangentElem, "z", tangent.Z);
            }
            xmlItem.Add(tangentElem);
            
            Xml.CreateElementWithValueAttribute(xmlItem, "coneOuterAngle", cone_outer);
            
            XElement extentsElem = new XElement("extents");                                          // dummy, to be replaced
            {
                Xml.CreateElementWithValueAttribute(extentsElem, "x", capsuleWidth); // dummy, to be replaced
                Xml.CreateElementWithValueAttribute(extentsElem, "y", 0.0f); // dummy, to be replaced
                Xml.CreateElementWithValueAttribute(extentsElem, "z", 0.0f); // dummy, to be replaced
            }
            xmlItem.Add(extentsElem);                                                               // dummy, to be replaced

			return (xmlItem);
		}

		/// <summary>
		/// Serialise a AsCExtensionDefSpawnpointOverride object to XML.
		/// </summary>
		/// <param name="doc"></param>
		/// <param name="extension"></param>
		/// <returns></returns>
		protected XElement AsCExtensionDefSpawnpointOverride(Extension extension)
		{
			if (null == extension.Object.RefObject)
			{
				Log.Log__WarningCtx(extension.Object.Name,
					"SpawnPoint override for '{0}' is not resolved.  Ignoring.", extension.Parent.Name);
				return (null);
			}

			XElement xmlItem = AsFwExtensionDef(extension);
			xmlItem.SetAttributeValue("type", "CExtensionDefSpawnPointOverride");

			// Flo 13.02.2014: To avoid writing out non overriden props as overriden, we pass the "original" value as Default for instance's GetAttribute()
			// in that case, if no value is found, that means that it was not locally overriden, so we fall back to the original one, and dont write an extension in the .imap

			// Not sure if pedType will ever become required by the game, psc didn't mention it yet

			// Parameters
			String origSpawnTypeParam = extension.Object.RefObject.GetParameter(ParamNames.TWODFX_SPAWNPOINT_SPAWNTYPE, ParamDefaults.TWODFX_SPAWNPOINT_SPAWNTYPE);
			String spawnTypeParam = extension.Object.GetParameter(ParamNames.TWODFX_SPAWNPOINT_SPAWNTYPE, origSpawnTypeParam);
			if (!origSpawnTypeParam.Equals(spawnTypeParam, StringComparison.OrdinalIgnoreCase))
			{
				Xml.CreateElementWithText(xmlItem, "ScenarioType", spawnTypeParam);
			}

			String origGroupTypeParam = extension.Object.RefObject.GetParameter(ParamNames.TWODFX_SPAWNPOINT_GROUPTYPE, ParamDefaults.TWODFX_SPAWNPOINT_GROUPTYPE);
			String groupTypeParam = extension.Object.GetParameter(ParamNames.TWODFX_SPAWNPOINT_GROUPTYPE, origGroupTypeParam);
			if (!origGroupTypeParam.Equals(groupTypeParam, StringComparison.OrdinalIgnoreCase))
			{
				Xml.CreateElementWithText(xmlItem, "Group", groupTypeParam);
			}

			// Attributes 
			int origtimeStartAttr = extension.Object.RefObject.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_START_TIME, AttrDefaults.TWODFX_SPAWNPOINT_START_TIME);
			int timeStartAttr = extension.Object.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_START_TIME, origtimeStartAttr);
			if (origtimeStartAttr != timeStartAttr)
			{
				Xml.CreateElementWithValueAttribute(xmlItem, "iTimeStartOverride", timeStartAttr);
			}

			int origtimeEndAttr = extension.Object.RefObject.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_END_TIME, AttrDefaults.TWODFX_SPAWNPOINT_END_TIME);
			int timeEndAttr = extension.Object.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_END_TIME, origtimeEndAttr);
			if (origtimeEndAttr != timeEndAttr)
			{
				Xml.CreateElementWithValueAttribute(xmlItem, "iTimeEndOverride", timeEndAttr);
			}

			// Model Set
			String origModelSet = extension.Object.RefObject.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_MODEL_SET, AttrDefaults.TWODFX_SPAWNPOINT_MODEL_SET);
			String modelSet = extension.Object.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_MODEL_SET, origModelSet);
			if (!origModelSet.Equals(modelSet, StringComparison.OrdinalIgnoreCase))
			{
				Xml.CreateElementWithText(xmlItem, "ModelSet", modelSet);
			}

			// AvailabilityInMpSp 
			int origAvailabilityInt = extension.Object.RefObject.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_AVAILABILITY, AttrDefaults.TWODFX_SPAWNPOINT_AVAILABILITY);
			int availabilityInt = extension.Object.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_AVAILABILITY, origAvailabilityInt);
			if (origAvailabilityInt != availabilityInt)
			{
				SpawnPointAvailability availability = (SpawnPointAvailability)availabilityInt;
				Xml.CreateElementWithText(xmlItem, "AvailabilityInMpSp", availability.ToString());
			}

			// TimeTillPedLeaves
			float origTimeTillPedLeaves = extension.Object.RefObject.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_TIME_TILL_PED_LEAVES, AttrDefaults.TWODFX_SPAWNPOINT_TIME_TILL_PED_LEAVES);
			float timeTillPedLeaves = extension.Object.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_TIME_TILL_PED_LEAVES, origTimeTillPedLeaves);
			if (Math.Abs(origTimeTillPedLeaves - timeTillPedLeaves) > Single.Epsilon)
			{
				Xml.CreateElementWithValueAttribute(xmlItem, "TimeTillPedLeaves", timeTillPedLeaves);
			}

			// Radius
			float origRadius = extension.Object.RefObject.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_RADIUS, AttrDefaults.TWODFX_SPAWNPOINT_RADIUS);
			float radius = extension.Object.GetAttribute(AttrNames.TWODFX_SPAWNPOINT_RADIUS, origRadius);
			if (Math.Abs(origRadius - radius) > Single.Epsilon)
			{
				Xml.CreateElementWithValueAttribute(xmlItem, "Radius", radius);
			}

			// Flags
			CScenarioPointFlags origFlags = SpawnPointFlags.Get(extension.Object.RefObject);
			CScenarioPointFlags flags = SpawnPointFlags.Get(extension.Object);
			if (origFlags != flags)
			{
				Xml.CreateElementWithText(xmlItem, "Flags", SpawnPointFlags.GetEnumString(extension.Object));
			}

			// The Name Element is always added to xmlItem, so check here to make sure at least one
			// other thing has been overridden, or return null.
			if (xmlItem.Elements().Count()> 1)
				return (xmlItem);
			return null;
		}

		/// <summary>
		/// Serialise a CExtensionDefDoor object for XML.
		/// </summary>
		/// <param name="doc"></param>
		/// <param name="extension"></param>
		/// <returns></returns>
		protected XElement AsCExtensionDefDoor(Extension extension)
		{
			XElement xmlItem = AsCExtensionDefCommon(extension);
			xmlItem.SetAttributeValue("type", "CExtensionDefDoor");

			Matrix34f mtxParent = new Matrix34f();
			if (extension.Object.HasParent())
				mtxParent = extension.Object.Parent.NodeTransform.Inverse();
			Matrix34f mtxLocal = extension.Object.NodeTransform * mtxParent;
			Quaternionf rot = new Quaternionf(mtxLocal);
			Xml.CreateElementWithVectorAttributes(xmlItem, "offsetRotation", rot);

			bool enableLimitAngle = extension.Object.GetAttribute(AttrNames.TWODFX_DOOR_ENABLE_LIMIT_ANGLE, AttrDefaults.TWODFX_DOOR_ENABLE_LIMIT_ANGLE);
			bool startsLocked = extension.Object.GetAttribute(AttrNames.TWODFX_DOOR_STARTS_LOCKED, AttrDefaults.TWODFX_DOOR_STARTS_LOCKED);
			bool canBreak = extension.Object.GetAttribute(AttrNames.TWODFX_DOOR_CAN_BREAK, AttrDefaults.TWODFX_DOOR_CAN_BREAK);
			float limitAngle = extension.Object.GetAttribute(AttrNames.TWODFX_DOOR_LIMIT_ANGLE, AttrDefaults.TWODFX_DOOR_LIMIT_ANGLE);
			float doorTargetRatio = extension.Object.GetAttribute(AttrNames.TWODFX_DOOR_DOOR_TARGET_RATIO, AttrDefaults.TWODFX_DOOR_DOOR_TARGET_RATIO);
			string doorAudioName = string.Format("{0}{1}", EXTENSION_DEF_DOOR_PREFIX, extension.Parent.GetObjectName());

			float radiansPerDegree = (float)(1.0d / (180.0d / Math.PI));
			Xml.CreateElementWithValueAttribute(xmlItem, "enableLimitAngle", enableLimitAngle);
			Xml.CreateElementWithValueAttribute(xmlItem, "startsLocked", startsLocked);
			Xml.CreateElementWithValueAttribute(xmlItem, "canBreak", canBreak);
			Xml.CreateElementWithValueAttribute(xmlItem, "limitAngle", limitAngle * radiansPerDegree);
			Xml.CreateElementWithValueAttribute(xmlItem, "doorTargetRatio", doorTargetRatio);
			Xml.CreateElementWithText(xmlItem, "audioHash", doorAudioName);

			return xmlItem;
		}

		/// <summary>
		/// Serialise a CInstancedMapData object to XML.
		/// </summary>
		/// <returns></returns>
		protected XElement AsCInstancedMapData(InstancedEntityData instancedEntityData)
		{
			XElement xmlRoot = new XElement("instancedData");

			if (instancedEntityData.IMAPLink != null)
				Xml.CreateElementWithText(xmlRoot, "ImapLink", instancedEntityData.IMAPLink);

			#region Optimised Entities List

			XElement listsNode = null;

			foreach (String platform in instancedEntityData.TreeEntitiesList.Keys)
			{
				listsNode = new XElement("PropInstanceList");
				listsNode.SetAttributeValue("platform", platform);

				xmlRoot.Add(listsNode);

				List<InstancedEntityList> entityLists = instancedEntityData.TreeEntitiesList[platform];

				foreach (InstancedEntityList entityList in entityLists)
				{
					XElement listNode = new XElement("Item");

					XElement entitiesListNode = new XElement("InstanceList");

					foreach (InstancedPropEntity entity in entityList)
					{
						XElement entityElement = AsFwInstancedPropEntityDef(entity);
						entitiesListNode.Add(entityElement);
					}

					listNode.Add(entitiesListNode);
					listsNode.Add(listNode);

					Xml.CreateElementWithText(listNode, "archetypeName", entityList[0].ArchetypeName);
					Xml.CreateElementWithText(listNode, "hdArchetypeName", entityList[0].HighDetailArchetypeName);
					Xml.CreateElementWithValueAttribute(listNode, "lodDist", entityList.LODDistance);
					Xml.CreateAABBElementForBoundingBox(listNode, "BatchAABB", entityList.BoundingBox);
				}
			}

			foreach (String platform in instancedEntityData.GrassEntitiesList.Keys)
			{
				listsNode = new XElement("GrassInstanceList");
				listsNode.SetAttributeValue("platform", platform);

				xmlRoot.Add(listsNode);

				List<InstancedEntityList> entityLists = instancedEntityData.GrassEntitiesList[platform];

				// Reorder the list for next-gen platforms before serializing.
				if (instancedEntityData.NextGenPlatformSplits.Contains(platform))
					entityLists = ReorderInstanceLists(instancedEntityData.NextGenSplitSize, entityLists);

				foreach (InstancedEntityList entityList in entityLists)
				{
					XElement listNode = new XElement("Item");

					Xml.CreateElementWithText(listNode, "archetypeName", entityList[0].ArchetypeName);
					Xml.CreateElementWithValueAttribute(listNode, "lodDist", entityList.LODDistance);
					Xml.CreateAABBElementForBoundingBox(listNode, "BatchAABB", entityList.BoundingBox);
					Xml.CreateElementWithVectorAttributes(listNode, "ScaleRange", new Vector3f(entityList.MinScale, entityList.MaxScale, entityList.RandomScale));
					Xml.CreateElementWithValueAttribute(listNode, "LodFadeStartDist", entityList.LODFadeStartDistance);
					Xml.CreateElementWithValueAttribute(listNode, "LodInstFadeRange", entityList.LODInstFadeRange);
					Xml.CreateElementWithValueAttribute(listNode, "OrientToTerrain", entityList.OrientToTerrain);

					XElement entitiesListNode = new XElement("InstanceList");

					foreach (InstancedGrassEntity entity in entityList)
					{
						XElement entityElement = AsFwInstancedGrassEntityDef(entity, entityList.BoundingBox);
						entitiesListNode.Add(entityElement);
					}

					listNode.Add(entitiesListNode);
					listsNode.Add(listNode);
				}
			}
			#endregion

			return (xmlRoot);
		}

		#region Extension Serialisation Methods
		/// <summary>
		/// Serialise a CExtensionDef object to XML.
		/// Runtime wants only one Extension type per entity with an Array of instances inside.
		/// </summary>
		/// <param name="extension"></param>
		/// <param name="extensionXmlNode"></param>
		/// <returns></returns>
        protected void AsCExtensionDef(Extension extension, XElement extensionXmlNode)
		{
			Debug.Assert( 
				extension.Object.Is2dfx() || extension.Parent.IsCloth(), 
				"Internal error: unhandled extension type.",
				"Object {0} is of unrecognised type by the IMAP serialiser.",
				extension.Object.Name);

			// testing the extension child
			if (extension.Object.Is2dfx())
			{
				if (extension.Object.IsLightInstance())
				{
					XElement lightExtInstance = AsCExtensionDefLightEffect(extension);
					if (null != lightExtInstance)
					{
						XElement lighthExtNode = Xml.FindChildNodeWithAttr(extensionXmlNode, "type", "CExtensionDefLightEffect");
						if (null == lighthExtNode)
						{
							lighthExtNode = Xml.CreateElementWithAttribute(extensionXmlNode, "Item", "type", "CExtensionDefLightEffect");
							lighthExtNode.Add(new XElement("instances"));
						}
						lighthExtNode = lighthExtNode.Elements().First();
						lighthExtNode.Add(lightExtInstance);
					}
				}
				else if (extension.Object.IsSpawnPointInstance())
				{
					XElement spawnPointExtInstance = AsCExtensionDefSpawnpointOverride(extension);
					if (null != spawnPointExtInstance)
					{
						extensionXmlNode.Add(spawnPointExtInstance);
					}
				}
				else if (extension.Object.Is2dfxDoor())
				{
					extensionXmlNode.Add(AsCExtensionDefDoor(extension));
				}
			}
			
			// testing the parent
            // GD: url:bugstar:1875093 Only writing out cloth extension for NG platforms. Determining by presence of PS4, approved by Mr. Muir.
            if (Entity.Branch.Targets.ContainsKey(Platform.Platform.PS4) && extension.Parent.IsCloth())
			{
				XElement xmlCollData = extensionXmlNode.Descendants("CollisionData").FirstOrDefault();
				XElement xmlCustBounds = null;
				if (null == xmlCollData)
				{
					xmlCustBounds = Xml.CreateElementWithAttribute(null, "Item", "type", "rage__phVerletClothCustomBounds");
					xmlCustBounds.Add(xmlCollData = new XElement("CollisionData"));
				}                
				XElement clothCollisionInstance = AsFwClothBoundsExtensionDef(extension);
				if (null != clothCollisionInstance)
				{
					xmlCollData.Add(clothCollisionInstance);
				}
				if (null!=xmlCustBounds && xmlCollData.Elements().Any())
				{
					extensionXmlNode.Add(xmlCustBounds);
				}
			}

		}

		private void WalkChildren4Extensions(XElement extensionXmlNode, TargetObjectDef entityObj)
		{
			foreach (TargetObjectDef child in entityObj.Children)
			{
				if (child.Is2dfx())
				{
                    AsCExtensionDef(new Extension(child, entityObj), extensionXmlNode);
				}
                WalkChildren4Extensions(extensionXmlNode, child);
			}

			if (entityObj.IsCloth() && entityObj.SceneLinks.ContainsKey(LinkChannel.LinkChannelClothCollision))
			{
                foreach (TargetObjectDef child in entityObj.SceneLinks[LinkChannel.LinkChannelClothCollision].Children)
				{
					if (child == null)
						Log.StaticLog.WarningCtx(entityObj.Name, "The Cloth_Collision hierarchy of {0} contains a null child", entityObj.Name);
					else
                        AsCExtensionDef(new Extension(child, entityObj), extensionXmlNode);
				}
			}
		}


		#endregion // Extension Serialisation Methods

		#region Manifest Serialisation Methods
		/// <summary>
		/// Serialise IMAP Group data into SceneXml manifest Additional XML.
		/// </summary>
		/// <param name="doc"></param>
		/// <param name="imapGroup"></param>
		/// <returns>XmlElement representing the IMAP group.</returns>
		/// 
		protected XElement AsManifestIMAPGroup(IMAPGroupContainer imapGroup)
		{
			XElement imapElem = Xml.CreateElementWithAttribute(null, "IMAPGroup", "name", imapGroup.Name.ToLower());
			if (null != imapGroup.ActivationTypes && imapGroup.ActivationTypes.Any())
			{
				foreach (String activationType in imapGroup.ActivationTypes)
					Xml.CreateElementWithAttribute(imapElem, "ActivationType", "value", activationType);
			}
			if (null != imapGroup.ActiveWeather && imapGroup.ActiveWeather.Any())
			{
				foreach (String weatherType in imapGroup.ActiveWeather)
					Xml.CreateElementWithAttribute(imapElem, "ActiveWeatherType", "value", weatherType);
			}
			if (imapGroup.ActiveHours != 0)
				Xml.CreateElementWithAttribute(imapElem, "ActiveHours", "value", imapGroup.ActiveHours);
			if (imapElem.HasElements)
				return imapElem;
			else
				return null;
		}

		/// <summary>
		/// Serialise IMAP Dependency data into SceneXml manifest additional XML.
		/// </summary>
		/// <param name="kvp">String, IMAP Container Key Value Pair.</param>
		/// <returns>XmlElement representing the dependency.</returns>
		protected XElement AsManifestIMAPDependency(KeyValuePair<String, IMAPContainer> kvp)
		{
			String name = kvp.Key;
			IMAPContainer container = kvp.Value;

			return (AsManifestIMAPDependency(name, container));
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="doc"></param>
		/// <param name="name"></param>
		/// <param name="container"></param>
		/// <returns></returns>
		protected XElement AsManifestIMAPDependency(String name, IMAPContainer container)
		{
			// Determine if we definitely have an interior.
			// B* 660582 - include IsMilo() check to make sure we process in-situ interiors - JWR
			if ((container.Entities.Count > 0) && (container.Entities[0].Object.IsMiloTri() || container.Entities[0].Object.IsMilo()))
			{
				String interiorFilename;

				if (container.Entities[0].Object.IsMilo() || container.Entities[0].Object.IsInternalRef())
				{
					interiorFilename = container.Name;
				}
				else if (container.Entities[0].Object.IsRefObject())
				{
					interiorFilename = Path.GetFileNameWithoutExtension(container.Entities[0].Object.RefFile);
				}
				else
				{
					Log.StaticLog.WarningCtx(container.Entities[0].Object.Name,
						"Invalid interior entity {0} ({1}).  See {2} for more information.",
						container.Entities[0].Object.Name, container.Entities[0].Object.Class, HREF_INVALID_INTERIOR_ENTITY);
					return (null);
				}

				// if interiorFilename is null, we dont want to write that dependency. It's probably related to a missing file,
				// or a deleted interior, or a not-exported-yet dependency. See B*1714853 or B*1666098. Or B*1686068
				if (string.IsNullOrEmpty(interiorFilename))
				{
					Log.StaticLog.WarningCtx(container.Entities[0].Object.Name, "Interior file '{0}' could not be found. See {1} for more information.",
						container.Entities[0].Object.Name, HREF_INTERIOR_NOT_FOUND_ENTITY);

					return null;
				}

                if (IsDLC)
                {
                    if (container.Entities[0].Object.IsNewDLCObject())
                    {
                        interiorFilename = MapAsset.AppendMapPrefixIfRequired(this.Project, interiorFilename);
                    }
                    // B*1916956
                    // if we're in a DLC project, but this is "on disc" data, just strip out the prefix - Flo
                    else
                    {
                        string prefix = MapAsset.GetMapProjectPrefix(this.Branch.Project);
                        interiorFilename = StringExtensions.Replace(interiorFilename, prefix, "", StringComparison.OrdinalIgnoreCase);
                    }
                }

				XElement xmlDependency = new XElement("IMAPDependency");
				xmlDependency.Add(new XAttribute("interiorFilename", interiorFilename));
				xmlDependency.Add(new XAttribute("imapName", name));
				return (xmlDependency);
			}
			else
			{
				return (null);
			}
		}

		/// <summary>
		/// Serialise IMAP dependencies.
		/// </summary>
		/// <returns></returns>
		protected XElement AsManifestIMAPDependency2(ManifestDependency dependency)
		{
			bool isInterior = dependency.IsInterior;
			bool hasDependencies = dependency.ITYPDependencies.Length > 0;

			if (isInterior || hasDependencies)
			{
				String containerName = Path.GetFileNameWithoutExtension(dependency.IMAPFile);
				XElement xmlDep2 = AsManifestIMAPDependency2(isInterior, containerName, dependency.ITYPDependencies.ToArray());
				return (xmlDep2);
			}
			else
			{
				return (null);
			}
		}

		/// <summary>
		/// Serialise IMAP dependencies.
		/// </summary>
		/// <param name="container"></param>
		/// <returns></returns>
		protected XElement AsManifestIMAPDependency2(IMAPContainer container)
		{
			bool isInterior = (container.Entities.Count == 1 && container.Entities.Count(entity => entity is InteriorEntity) == 1);
			bool hasDependencies = (container.Dependencies.Length > 0);

			if (isInterior || hasDependencies)
			{
				XElement xmlDep2 = AsManifestIMAPDependency2(isInterior, container.Name, container.Dependencies);
				return (xmlDep2);
			}
			else
			{
				return (null);
			}
		}

		/// <summary>
		/// Serialise a map dependency.
		/// </summary>
		/// <param name="doc"></param>
		/// <param name="isInterior"></param>
		/// <param name="containerName"></param>
		/// <param name="dependencies"></param>
		/// <returns></returns>
		protected XElement AsManifestIMAPDependency2(bool isInterior, String containerName, String[] dependencies)
		{
			bool hasDependencies = (dependencies != null && dependencies.Length > 0);

			XElement xmlDep2 = new XElement("IMAPDependency2");

			xmlDep2.Add(new XAttribute("isInterior", isInterior.ToString()));
			xmlDep2.Add(new XAttribute("imapName", containerName));

			if (hasDependencies)
			{
                // remove empty entries from the dependency list
				List<String> sortedDependencies = dependencies.Where(dependency => !string.IsNullOrWhiteSpace(dependency)).ToList();

			    sortedDependencies.Sort();
				xmlDep2.Add(new XAttribute("itypNames", String.Join(";", sortedDependencies)));
			}
			else
			{
				xmlDep2.Add(new XAttribute("itypNames", ""));
			}

			return xmlDep2;
		}

		#endregion // Manifest Serialisation Methods
		#endregion // Protected Object-Serialisation Methods

		#region Scene and Object Processing Methods
		/// <summary>
		/// Clear all internal state.
		/// </summary>
		protected override void Reset()
		{
			base.Reset();
			this.GroupContainers = new Dictionary<string, IMAPGroupContainer>();
			this.InteriorContainerMap = new Dictionary<string, IMAPContainer>();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="scenes"></param>
		protected override void ProcessScenes(IEnumerable<Scene> scenes)
		{
			base.ProcessScenes(scenes);
			
			// Post-process interior entities.
			foreach (KeyValuePair<String, IMAPContainer> interiorContainerPair in InteriorContainerMap)
			{
				if (IsManualStreamInteriorIMAP(interiorContainerPair.Value))
				{
					interiorContainerPair.Value.Name = interiorContainerPair.Key;
					interiorContainerPair.Value.Flags = Framework.IMAP_eFlags.F_MANUAL_STREAM_ONLY;
				}
				else
				{
					interiorContainerPair.Value.Parent = this.StaticContainer;
					interiorContainerPair.Value.Name = String.Format("{0}_{1}", this.StaticContainer.Name, interiorContainerPair.Key);

					foreach (Entity e in interiorContainerPair.Value.Entities)
						SetParentEntity(e, this.StaticContainer);
				}
			}

			// Post-process "IPL Group" entities.
			foreach (IMAPGroupContainer groupContainer in this.GroupContainers.Values)
			{
				foreach (Entity e in groupContainer.Streamed.Entities)
					SetParentEntity(e, groupContainer.Streamed);
				foreach (Entity e in groupContainer.Static.Entities)
				{
					switch (e.Object.LODLevel)
                    {
                        // Parent in StaticLOD container.
                        case ObjectDef.LodLevel.SLOD1:
                            SetChildEntities(e, groupContainer.Static, groupContainer.Streamed);
                            SetParentEntityFromContainer(e, groupContainer.StaticSLOD);
                            Debug.Assert(e.Parent != null);
                            break;
                        // Parent in Static container.
                        case ObjectDef.LodLevel.LOD:
                            SetChildEntities(e, groupContainer.Static, groupContainer.Streamed);
                            SetParentEntityFromContainer(e, groupContainer.Static);
                            Debug.Assert(e.Parent != null);
                            break;
                        // Error cases.
                        case ObjectDef.LodLevel.ORPHANHD:
                        case ObjectDef.LodLevel.HD:
                        default:
                            Debug.Fail("Unexpected object type.  Contact tools.");
                            break;
                    }
                }
                foreach (Entity e in groupContainer.StaticSLOD.Entities)
                {
                    SetChildEntities(e, groupContainer.StaticSLOD, groupContainer.Static);
                    SetParentEntityFromContainer(e, groupContainer.StaticSLOD);
                }
			}
		}

		/// <summary>
		/// Process a single Scene object.
		/// </summary>
		/// <param name="scene">Scene object to process.</param>
		protected override void ProcessScene(Scene scene)
		{
			// Add block for this Scene.
			this.StaticContainer.AddBlock(new Block(scene));

			base.ProcessScene(scene);
		}

		/// <summary>
		/// Process a single ObjectDef object.
		/// </summary>
		/// <param name="o"></param>
		protected override void ProcessSceneObject(TargetObjectDef o)
		{
            // DHM FIX ME: investigate this.  Does take a while and its per-entity!!
			if (IsExcluded(o))
				return;

			if (o.DontExport() || o.DontExportIPL() || SceneOverrideIntegrator.Instance.IsObjectMarkedForDeletion(o))
				return;

			// GD: Drawable LOD states don't ever need to go into the ITYP.
			// Shadow meshes need same treatment
			if (o.HasDrawableLODChildren() || o.HasRenderSimParent() || o.HasShadowMeshLink())
				return;

			if (o.IsContainer())
			{
				foreach (TargetObjectDef obj in o.Children)
					ProcessSceneObject(obj);
			}
			else if (o.IsTimeCycleBox())
			{
				// B*1121239 - we don't serialise some timecycle box types as specified by the pipeline configuration
				String id_str = (String)o.GetAttribute(AttrNames.TWODFX_TCYC_BOX_IDSTRING, AttrDefaults.TWODFX_TCYC_BOX_IDSTRING);
				if (!NonSerialisedTimeCycleTypes.Any(type => type.Equals(id_str, StringComparison.OrdinalIgnoreCase)))
					this.StaticContainer.AddTimeCycleModifierBox(o);
			}
			else if (o.IsMiloTri() ||
					 (o.IsMilo() && (SceneType.EnvironmentContainer == o.MyScene.SceneType)))
			{
				// If we are a regular milo reference (IsMiloTri())
				// Or we are an in-situ milo within a normal map container (B* 370617) - JWR
				
				// DHM: to support url:bugstar:1468011 we check the IPL/IMAP Group
				// first to determine the container.  We treat IPL Groups the same
				// for interiors and regular entities.  Validator ensures that
				// we don't mix them up in a single group however.
				String iplGroup = o.GetAttribute(AttrNames.OBJ_IPL_GROUP, AttrDefaults.OBJ_IPL_GROUP).ToLower();
			    bool hasIplGroup = !String.IsNullOrEmpty(iplGroup) && (!iplGroup.Equals(AttrDefaults.OBJ_IPL_GROUP, StringComparison.OrdinalIgnoreCase));

				if (hasIplGroup)
				{
                    bool isNewDlcObject = this.Branch.Project.ForceFlags.AllNewContent || o.IsNewDLCObject();
				    if (this.IsDLC && !isNewDlcObject)
				        return; // Don't serialise out non-DLC IPL Groups.
                    
                    // Ensure group name is prefixed if required.
                    iplGroup = MapAsset.AppendMapPrefixIfRequired(this.Branch.Project, iplGroup);
					
                    IMAPGroupContainer groupContainer = null;
					if (this.GroupContainers.ContainsKey(iplGroup))
					{
						groupContainer = this.GroupContainers[iplGroup];
					}
					else
					{
						groupContainer = new IMAPGroupContainer(iplGroup);
						this.GroupContainers.Add(iplGroup, groupContainer);
					}
					InteriorEntity interiorEntity = new InteriorEntity(o);

					if (o.IsMiloTri())
					{
						// Add attached child entities and CarGens.
						foreach (TargetObjectDef childObjectDef in o.Children)
						{
							if (childObjectDef.IsStreamingExtentsOverrideBox())
							{
								interiorEntity.StreamingExtentsOverride = childObjectDef.WorldBoundingBox;
							}
							else if (childObjectDef.IsObject())
							{
								interiorEntity.AddEntity(new Entity(childObjectDef));
							}
							else
							{
								Log.Log__WarningCtx(o.Name, "Interior with IPL Group '{0}' has unsupported sub-object '{1}'.  Ignoring.",
									o.Name, childObjectDef.Name);
							}
						}
					}
					else if (o.IsMilo())
					{
						// In-situ interior; handle CarGen helper children.
						foreach (TargetObjectDef childObjectDef in o.Children)
						{
							Log.Log__WarningCtx(o.Name, "Interior with IPL Group '{0}' has unsupported sub-object '{1}'.  Ignoring.",
								o.Name, childObjectDef.Name);
						}
					}

					groupContainer.Streamed.AddEntity(interiorEntity);

					// Create the unique interior name; the unique name is used 
					// later for pathname and container name generation (this 
					// ensures we only do that once)
					// Default interior IMAP name: "interior_<count>_<name>".
					String uniqueInteriorName = InteriorNameMap.GetContainerNameFromObject(interiorEntity.Object);

					if (o.IsMilo())
                    {
                        // B* 660582 - in-situ interiors have identically named ITYP and IMAP files - JWR
                        if ( (this.IsDLC == o.IsNewDLCObject()) 
                            || Branch.Project.ForceFlags.AllNewContent)
                        {
                            string itypName = string.Format("{0}_{1}", StaticContainer.Name, uniqueInteriorName);
                            AddContainerDependency(groupContainer.Streamed, itypName);
                        }
                        // Apart from when processing DLC damnit!  - DHM
                        else
                        {
                            // If our container-name has the prefix; cull the prefix.
                            String prefix = MapAsset.GetMapProjectPrefix(this.Branch.Project);
                            string containerName = StaticContainer.Name.Replace(prefix, "");

                            String itypName = String.Format("{0}_{1}", containerName, uniqueInteriorName);
                            AddContainerDependency(groupContainer.Streamed, itypName);
                        }
					}

#warning DHM FIX ME: InteriorContainerMap?
					// Add it to the regular interior map
					// groupContainer.InteriorContainerMap.Add(uniqueInteriorName, groupContainer.Streamed);
				}
				else
				{
					IMAPContainer newIMAPContainer = new IMAPContainer();
					InteriorEntity interiorEntity = new InteriorEntity(o);

					if (o.IsMiloTri())
					{
						// Add attached child entities and CarGens.
						foreach (TargetObjectDef childObjectDef in o.Children)
						{
							if (childObjectDef.IsStreamingExtentsOverrideBox())
							{
								interiorEntity.StreamingExtentsOverride = childObjectDef.WorldBoundingBox;
							}
							else if (childObjectDef.IsObject())
							{
								interiorEntity.AddEntity(new Entity(childObjectDef));
							}
							else if (childObjectDef.IsCarGen())
							{
								newIMAPContainer.AddCarGen(new CarGen(childObjectDef));
							}
						}
					}
					else if (o.IsMilo())
					{
						// In-situ interior; handle CarGen helper children.
						foreach (TargetObjectDef childObjectDef in o.Children)
						{
							if (!childObjectDef.IsCarGen())
								continue;

							newIMAPContainer.AddCarGen(new CarGen(childObjectDef));
						}
					}

					newIMAPContainer.AddEntity(interiorEntity);

					// Create the unique interior name; the unique name is used 
					// later for pathname and container name generation (this 
					// ensures we only do that once)
					// Default interior IMAP name: "interior_<count>_<name>".
					String uniqueInteriorName = InteriorNameMap.GetContainerNameFromObject(interiorEntity.Object);

					if (o.IsMilo())
					{
                        // B* 660582 - in-situ interiors have identically named ITYP and IMAP files - JWR
                        if ((this.IsDLC == o.IsNewDLCObject())
                            || Branch.Project.ForceFlags.AllNewContent)
                        {
                            string itypName = string.Format("{0}_{1}", StaticContainer.Name, uniqueInteriorName);
                            AddContainerDependency(newIMAPContainer, itypName);
                        }
                        // Apart from when processing DLC damnit!  - DHM
                        else
                        {
                            // If our container-name has the prefix; cull the prefix.
                            String prefix = MapAsset.GetMapProjectPrefix(this.Branch.Project);
                            string containerName = StaticContainer.Name.Replace(prefix, "");

                            String itypName = String.Format("{0}_{1}", containerName, uniqueInteriorName);
                            AddContainerDependency(newIMAPContainer, itypName);
                        }
					}

					// Add it to the regular interior map
					InteriorContainerMap.Add(uniqueInteriorName, newIMAPContainer);
				}
			}
			else if (o.IsCarGen())
			{
				CarGen cargen = new CarGen(o);

                String iplGroup = o.GetAttribute(AttrNames.OBJ_IPL_GROUP, AttrDefaults.OBJ_IPL_GROUP).ToLower();
                bool hasIplGroup = !String.IsNullOrEmpty(iplGroup) && !iplGroup.Equals(AttrDefaults.OBJ_IPL_GROUP, StringComparison.OrdinalIgnoreCase);

                // Ensure group name is prefixed if required.
                iplGroup = MapAsset.AppendMapPrefixIfRequired(this.Branch.Project, iplGroup);
                IMAPGroupContainer groupContainer = null;
                // Ensure the container has been constructed to save us handling
                // it in each case below.
                if (hasIplGroup)
                {
                    if (!this.GroupContainers.ContainsKey(iplGroup))
                    {
                        groupContainer = new IMAPGroupContainer(iplGroup);
                        this.GroupContainers.Add(iplGroup, groupContainer);
                    }

                    groupContainer = this.GroupContainers[iplGroup];
                    groupContainer.Streamed.AddCarGen(cargen);
                }
                else
                {
                    this.SceneStreamedContainer.AddCarGen(cargen);
                }
			}
			else if (o.IsOcclusion())
			{
				if (o.IsOcclusionBox())
				{
					this.OcclusionContainer.AddBoxPlaceholder(o);
				}
				else if (o.IsOcclusionMesh())
				{
					this.OcclusionContainer.AddMeshPlaceholder(o);
				}
				else
				{
					Log.Log__ErrorCtx(o.Name, "Object has unknown occlusion type.");
				}
			}
			else if (o.IsStreamingExtentsOverrideBox())
			{
				this.OcclusionContainer.StreamingExtentsOverride = o.WorldBoundingBox;
			}
			else if (o.IsIMAPGroupDefinition())
			{
                bool isNewDlcObject = this.Branch.Project.ForceFlags.AllNewContent || o.IsNewDLCObject();
                if (this.Branch.Project.IsDLC && !isNewDlcObject && (o.Children.Length == 0))
                    return;
                
				// Ensure the container has been constructed to save us handling
				// it in each case below.
				IMAPGroupContainer groupContainer = null;
                String iplGroup = o.GetParameter(ParamNames.OBJECT_GROUP_NAME, ParamDefaults.OBJECT_GROUP_NAME).ToLower();
                // Ensure group name is prefixed if required.
                iplGroup = MapAsset.AppendMapPrefixIfRequired(this.Branch.Project, iplGroup);
				if (!this.GroupContainers.ContainsKey(iplGroup))
				{
					groupContainer = new IMAPGroupContainer(iplGroup);
					this.GroupContainers.Add(iplGroup, groupContainer);
				}
				else
				{
					groupContainer = this.GroupContainers[iplGroup];
				}

				Object[] activaTypeObjectArray = o.GetParameter(ParamNames.IMAP_GROUP_TYPES, ParamDefaults.IMAP_GROUP_TYPES);
				String[] actTypeNameArray = new String[activaTypeObjectArray.Length];
				for (int i = 0; i < activaTypeObjectArray.Length; i++)
					actTypeNameArray[i] = activaTypeObjectArray[i] as String;
				groupContainer.ActivationTypes = actTypeNameArray;

				Object[] typeObjectArray = o.GetParameter(ParamNames.IMAP_GROUP_WEATHER_TYPES, ParamDefaults.IMAP_GROUP_WEATHER_TYPES);
				String[] typeNameArray = new String[typeObjectArray.Length];
				for (int i = 0; i < typeObjectArray.Length; i++)
					typeNameArray[i] = typeObjectArray[i] as String;
				groupContainer.ActiveWeather = typeNameArray;

				groupContainer.ActiveHours = o.GetParameter(ParamNames.IMAP_GROUP_TIME, ParamDefaults.IMAP_GROUP_TIME);

				// Process the linked meshes as if they were root objects.
				foreach (TargetObjectDef obj in o.Children)
					ProcessSceneObject(obj);
			}
			else if (o.IsObject())
			{
                String iplGroup = o.GetAttribute(AttrNames.OBJ_IPL_GROUP, AttrDefaults.OBJ_IPL_GROUP).ToLower();
                bool hasIplGroup = !String.IsNullOrEmpty(iplGroup) && !iplGroup.Equals(AttrDefaults.OBJ_IPL_GROUP, StringComparison.OrdinalIgnoreCase);

                bool isNewDlcObject = this.Branch.Project.ForceFlags.AllNewContent || o.IsNewDLCObject();
			    if (this.Branch.Project.IsDLC && hasIplGroup && !isNewDlcObject)
			        return;
                
                // Ensure group name is prefixed if required.
			    iplGroup = MapAsset.AppendMapPrefixIfRequired(this.Branch.Project, iplGroup);
			    
			    IMAPGroupContainer groupContainer = null;
				// Ensure the container has been constructed to save us handling
				// it in each case below.
				if (hasIplGroup && !this.GroupContainers.ContainsKey(iplGroup))
				{
					groupContainer = new IMAPGroupContainer(iplGroup);
					this.GroupContainers.Add(iplGroup, groupContainer);
				}
				else if (hasIplGroup)
				{
					groupContainer = this.GroupContainers[iplGroup];
				}

				Entity entity = new Entity(o);
				switch (o.LODLevel)
				{
					case ObjectDef.LodLevel.SLOD4:
					case ObjectDef.LodLevel.SLOD3:
					case ObjectDef.LodLevel.SLOD2:
						if (hasIplGroup)
						{
                            // We now allow SLOD2/SLOD3/SLOD4 entities in IPL Groups!
                            // This was introduced for the aircraft carrier.
                            groupContainer.StaticSLOD.AddEntity(entity);
						}
                        else
                        {
						    this.StaticContainer.AddEntity(entity);
                        }
						break;
					case ObjectDef.LodLevel.SLOD1:
					case ObjectDef.LodLevel.LOD:
						if (hasIplGroup)
						{
							// We allow LOD hierarchies now in IPL Groups.
							groupContainer.Static.AddEntity(entity);
						}
						else
						{
							this.StaticContainer.AddEntity(entity);
						}
						break;
					case ObjectDef.LodLevel.HD:
					case ObjectDef.LodLevel.ORPHANHD:
						switch (entity.StreamMode)
						{
							case Entity.StreamModeType.Streamed:
								if (hasIplGroup)
									groupContainer.Streamed.AddEntity(entity);
								else
									this.SceneStreamedContainer.AddEntity(entity);
								break;
							case Entity.StreamModeType.StreamedLong:
								if (hasIplGroup)
									groupContainer.Streamed.AddEntity(entity);
								else
									this.SceneStreamedLongContainer.AddEntity(entity);
								break;
							case Entity.StreamModeType.StreamedCritical:
								if (hasIplGroup)
									groupContainer.Streamed.AddEntity(entity);
								else
									this.SceneStreamedCriticalContainer.AddEntity(entity);
								break;
							case Entity.StreamModeType.NotStreamed:
								if (hasIplGroup)
									groupContainer.Static.AddEntity(entity);
								else
									this.StaticContainer.AddEntity(entity);
								break;
						}
						break;
				}
			}
			else if (o.IsCarGen())
			{
				CarGen cargen = new CarGen(o);
				this.SceneStreamedContainer.AddCarGen(cargen);
			}
			else if (o.IsContainerLODRefObject())
			{
				if (o.LOD != null && o.LOD.Children.Any())
				{
					// We have found a suitable container LOD parent
                    TargetObjectDef slod2ObjectDef = Framework.InterContainerLODManager.Instance.GetSourceSLOD2ObjectDef(o);
                    if (slod2ObjectDef != null)
					{
                        // Default SLOD2 IMAP is the <container>_lod static IMAP.
                        String containerLodName = Path.GetFileNameWithoutExtension(slod2ObjectDef.MyScene.Filename);
                        // We also now support SLOD2/3/4 in IMAP Groups; so here we need to check whether the SLOD2
                        // is defined in an IMAP group and adjust the name accordingly.
                        if (slod2ObjectDef.Attributes.ContainsKey(AttrNames.OBJ_IPL_GROUP))
                        {
                            String iplGroup = slod2ObjectDef.GetAttribute(AttrNames.OBJ_IPL_GROUP, AttrDefaults.OBJ_IPL_GROUP).ToLower();
                            if (this.IsDLC)
                            {
                                // Ensure group name is prefixed if required.
                                iplGroup = MapAsset.AppendMapPrefixIfRequired(this.Project, iplGroup);
                            }

                            IMAPGroupContainer groupContainer = null;
                            // Ensure the container has been constructed as there is a small chance that this will 
                            // be undefined if this is the first sceneXml and the first object is an RsRef.
                            if (!this.GroupContainers.TryGetValue(iplGroup, out groupContainer))
                            {
                                groupContainer = new IMAPGroupContainer(iplGroup);
                                this.GroupContainers.Add(iplGroup, groupContainer);
                            }

                            // _slod IMAP Group filename (see IMAPContainer.cs)
                            containerLodName = String.Format("{0}_slod", iplGroup);
                            groupContainer.Static.ParentImapOverride = containerLodName;
                        }
                        
                        if (String.IsNullOrEmpty(lodContainerName_))
						{
							lodContainerName_ = containerLodName;
						}
						else if (containerLodName != lodContainerName_)
						{
							Log.Log__Warning("Found a second LOD container named {0} when another named {1} had already been found.", 
								containerLodName, lodContainerName_);
						}
					}
					else
					{
						Log.Log__ErrorCtx(o.Name, "Failed to look up the source SLOD2 object for {0}, in {1}.", o.Name, o.Parent.Name);
					}
				}
			}
		}
		#endregion // Scene and Object Processing Methods

		#region Misc. Helper Methods
		/// <summary>
		/// Add any core-game dependencies into IMAP Containers.  Archetype cache
		/// map is passed in (implementation method).
		/// </summary>
		/// <param name="container"></param>
		/// <param name="archetypeToITYPMap"></param>
		private void AddDependenciesForCoreGame(IMAPContainer container, IDictionary<String, String> archetypeToITYPMap)
		{
			foreach (Entity entity in container.Entities)
            {
                if (entity.Object.IsRefObject())
                    continue; // Skip.
				if( entity.Object.IsMilo())
                    continue;

                // Find entity's archetype; add dependency if non-DLC object.
                bool isNewDlcObject = this.Branch.Project.ForceFlags.AllNewContent || entity.Object.IsNewDLCObject();
                if (isNewDlcObject)
					continue; // Skip.

				String archetypeName = entity.Object.GetObjectName().ToLower();

                string itypMapName;
                archetypeToITYPMap.TryGetValue(archetypeName, out itypMapName);

				if (itypMapName != null)
					container.AddDependency(itypMapName);
				else
					Log.Log__Error("Failed to find archetype ITYP for '{0}'.", archetypeName);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="xmlDoc"></param>
		private void WriteIMAPDependencyManifestData(XDocument xmlDoc)
		{
			XElement staticImapDependency2 = AsManifestIMAPDependency2(this.StaticContainer);
			if ((null != staticImapDependency2) && (this.StaticContainer.Entities.Count > 0))
				xmlDoc.Root.Add(staticImapDependency2);

			foreach (IMAPContainer container in this.StreamedContainers)
			{
				if (0 == container.Entities.Count)
					continue;

				XElement imapDependency2 = AsManifestIMAPDependency2(container);
				if (null != imapDependency2)
					xmlDoc.Root.Add(imapDependency2);
			}
			foreach (IMAPContainer container in this.StreamedLongContainers)
			{
				if (0 == container.Entities.Count)
					continue;

				XElement imapDependency2 = AsManifestIMAPDependency2(container);
				if (null != imapDependency2)
					xmlDoc.Root.Add(imapDependency2);
			}
			foreach (IMAPContainer container in this.StreamedCriticalContainers)
			{
				if (0 == container.Entities.Count)
					continue;

				XElement imapDependency2 = AsManifestIMAPDependency2(container);
				if (null != imapDependency2)
					xmlDoc.Root.Add(imapDependency2);
			}
			foreach (KeyValuePair<String, IMAPGroupContainer> kvp in this.GroupContainers)
            {
                XElement groupStaticSLODImapDependency2 = AsManifestIMAPDependency2(kvp.Value.StaticSLOD);
                if ((null != groupStaticSLODImapDependency2) && (kvp.Value.StaticSLOD.Entities.Count > 0))
                    xmlDoc.Root.Add(groupStaticSLODImapDependency2);
				XElement groupStaticImapDependency2 = AsManifestIMAPDependency2(kvp.Value.Static);
				if ((null != groupStaticImapDependency2) && (kvp.Value.Static.Entities.Count > 0))
					xmlDoc.Root.Add(groupStaticImapDependency2);
				XElement groupStreamedImapDependency2 = AsManifestIMAPDependency2(kvp.Value.Streamed);
				if ((null != groupStreamedImapDependency2) && (kvp.Value.Streamed.Entities.Count > 0))
					xmlDoc.Root.Add(groupStreamedImapDependency2);
			}
			foreach (KeyValuePair<String, IMAPContainer> kvp in this.InteriorContainerMap)
			{
				if (0 == kvp.Value.Entities.Count)
					continue;

				XElement imapDependency2 = AsManifestIMAPDependency2(kvp.Value);
				if (null != imapDependency2)
					xmlDoc.Root.Add(imapDependency2);
			}
		}

		/// <summary>
		/// Does a lookup in the array of overridden property names and gives back value from according object
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="instObj">instanced light object</param>
		/// <param name="propKey">name of property to look up</param>
		/// <param name="defaultVal">default value to give back if property not on any object</param>
		/// <param name="instProps">array of overridden properties</param>
		/// <returns></returns>
		private T GetLightInstanceProperty<T>(TargetObjectDef instObj, string propKey, T defaultVal, string[] instProps)
		{
			// is this an instance overridden prop?
			if (instProps.Contains(propKey))
				return (T)instObj.GetProperty(propKey, defaultVal);
			//otherwise get the prop of the referenced object
			return instObj.RefObject.GetProperty(propKey, defaultVal);
		}

		/// <summary>
		/// Does a lookup in the array of overridden attribute names and gives back value from according object
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="instObj">instanced light object</param>
		/// <param name="attrKey">name of attribute to look up</param>
		/// <param name="defaultVal">default value to give back if attribute not on any object</param>
		/// <param name="instAttrs">array of overridden attributes</param>
		/// <returns></returns>
		private T GetLightInstanceAttribute<T>(TargetObjectDef instObj, string attrKey, T defaultVal, string[] instAttrs)
		{
			// is this an instance overridden prop?
			if (instAttrs.Contains(attrKey))
				return (T)instObj.GetAttribute(attrKey, defaultVal);
			//otherwise get the prop of the referenced object
			return instObj.RefObject.GetAttribute(attrKey, defaultVal);
		}

		// Interiors with an IMAP group set are special.  They form new manual stream IMAPs with a single interior.
		private bool IsManualStreamInteriorIMAP(IMAPContainer imapContainer)
		{
			return manualStreamInteriorIMAPs_.Contains(imapContainer);
		}

		/// <summary>
		/// Reorders the instance list array so they are split up based on the splitSize.
		/// This is required for next-gen instance lists.
		/// </summary>
		/// <param name="splitSize"></param>
		/// <param name="entityLists"></param>
		/// <returns></returns>
		private List<InstancedEntityList> ReorderInstanceLists(int splitSize, List<InstancedEntityList> entityLists)
		{
			List<InstancedEntityList> newEntityList = new List<InstancedEntityList>();

			foreach (InstancedEntityList list in entityLists)
			{
				InstancedEntityList newList = new InstancedEntityList();

				int numIterations = list.Count / splitSize;
				int remainder = list.Count % splitSize;

				for (int splitCount = 0; splitCount < splitSize; splitCount++)
				{
					for (int i = 0; i < numIterations; i++)
					{
						int index = i * splitSize + splitCount;
						InstancedEntity entity = list[index];
						newList.Add(entity);
					}
				}

				for (int i = list.Count - remainder; i < list.Count; i++)
				{
					InstancedEntity entity = list[i];
					newList.Add(entity);
				}

				newList.Process(list.ArchetypeBoundingBox, list.LODDistance, list.LODFadeStartDistance, list.LODInstFadeRange, list.OrientToTerrain,
											new Vector3f(list.MinScale, list.MaxScale, list.RandomScale), list.Brightness);

				newEntityList.Add(newList);
			}

			return newEntityList;
		}
		#endregion // Misc. Helper Methods
	}

} // RSG.SceneXml.MapExport.Project.GTA namespace
