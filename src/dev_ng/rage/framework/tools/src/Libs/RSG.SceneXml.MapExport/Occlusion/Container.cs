﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using Ionic.Zip;

using RSG.Base.Logging;
using RSG.Base.Math;

namespace RSG.SceneXml.MapExport.Occlusion
{
    /// <summary>
    /// This class is responsible for caching occlusion model placeholders and then managing the
    /// translation to proper occlusion models and the split into streamable chunks
    /// </summary>
    public class Container
    {
        #region Constructor

        public Container()
        {
            StreamingExtentsOverride = null;

            boxPlaceholders_ = new List<TargetObjectDef>();
            meshPlaceholders_ = new List<TargetObjectDef>();
        }

        #endregion Constructor

        #region Public Properties

        public bool HasData
        {
            get
            {
                return boxPlaceholders_.Any() || meshPlaceholders_.Any();
            }
        }

        public int DataSize
        {
            get
            {
                return transformedModels_.Sum(model => model.DataSize);
            }
        }

        public BoundingBox3f StreamingExtentsOverride { get; set; }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Marks an object def to be processed as an Occlusion.Box
        /// </summary>
        public void AddBoxPlaceholder(TargetObjectDef boxPlaceholder)
        {
            boxPlaceholders_.Add(boxPlaceholder);
        }

        /// <summary>
        /// Marks an object def to be processed as an Occlusion.Mesh
        /// </summary>
        public void AddMeshPlaceholder(TargetObjectDef meshPlaceholder)
        {
            meshPlaceholders_.Add(meshPlaceholder);
        }

        /// <summary>
        /// Translated placeholders to Occlusion.Model instances.
        /// The scenes collection is used to find XML mesh data built bu Ragebuilder.
        /// </summary>
        public void TranslatePlaceholders(IEnumerable<Scene> scenes)
        {
            transformedModels_ = new List<Model>();
            transformedIMAPGroupModels_ = new Dictionary<string, List<Model>>();
            TranslateBoxPlaceholders();
            TranslateMeshPlaceholders(scenes);
        }

        /// <summary>
        /// Splits transformed models into a flattened collection of nodes.
        /// </summary>
        public Node[] Split(int maxNodeDataSize)
        {
            Node rootNode = new Node(transformedModels_);
            rootNode.Split(maxNodeDataSize);
            return rootNode.GetLeafNodesRecursive();
        }

        public Tuple<String, Node>[] GetIMAPGroupNodes()
        {
            return transformedIMAPGroupModels_.Select(kvp => new Tuple<String, Node>(kvp.Key, new Node(kvp.Value))).ToArray();
        }

        #endregion Public Methods

        #region Private Methods

        private static IEnumerable<Mesh> GetOcclusionMeshesFromXmlDocument(XDocument xmlDocument, TargetObjectDef occlMeshObjectDef)
        {
            List<Mesh> meshes = new List<Mesh>();

            bool isWaterOnlyOcclusion = occlMeshObjectDef.GetAttribute(AttrNames.OBJ_IS_WATER_ONLY_OCCLUSION, AttrDefaults.OBJ_IS_WATER_ONLY_OCCLUSION);

            XElement occlusionMeshesElement = xmlDocument.Root.Element("occludeModels");
            foreach (XElement occlusionMeshElement in occlusionMeshesElement.Elements("Item"))
            {
                meshes.Add(Mesh.FromXElementAndObjectDef(occlusionMeshElement, occlMeshObjectDef));
            }

            return meshes;
        }

        private void TranslateBoxPlaceholders()
        {
            if (boxPlaceholders_.Count > 0)
                transformedModels_.AddRange(boxPlaceholders_.Select(boxPlaceholder => Occlusion.Box.FromObjectDef(boxPlaceholder)));
            
            boxPlaceholders_ = null;
        }

        private void TranslateMeshPlaceholders(IEnumerable<Scene> scenes)
        {
            foreach (Scene scene in scenes)
            {
                String sceneXmlPathname = scene.Filename;
                String mapname = Path.GetFileNameWithoutExtension(sceneXmlPathname);
                String exportZipPathname = Path.ChangeExtension(sceneXmlPathname, "zip");
                if (!File.Exists(exportZipPathname))
                    continue; // Skip; scene has no occlusion, others *should*.

                // Open up our zip
                using (ZipFile exportZipFile = ZipFile.Read(exportZipPathname))
                {
                    // Look for the occlusion zip data package in the exported map zip file.
                    String occlDataZipFilename = String.Format("{0}.occl.zip", mapname);
                    Debug.Assert(exportZipFile.ContainsEntry(occlDataZipFilename),
                        String.Format("Export zip ({0}) does not include occlusion mesh data ({1}).", exportZipPathname, occlDataZipFilename));
                    if (!exportZipFile.ContainsEntry(occlDataZipFilename))
                    {
                        Log.Log__Warning("Export zip ({0}) does not include occlusion mesh data ({1}).", exportZipPathname, occlDataZipFilename);
                        continue;
                    }
                    ZipEntry occlDataZipEntry = exportZipFile[occlDataZipFilename];
                    using (MemoryStream occlDataZipMemoryStream = new MemoryStream((int)occlDataZipEntry.UncompressedSize))
                    {
                        occlDataZipEntry.Extract(occlDataZipMemoryStream);
                        occlDataZipMemoryStream.Seek(0, SeekOrigin.Begin);

                        // Open up our occlusion zip file; parsing all XML and constructing OccludeModelCollection
                        // objects for each entry.
                        using (ZipFile occlZipFile = ZipFile.Read(occlDataZipMemoryStream))
                        {
                            foreach (ZipEntry occlEntry in occlZipFile.Entries.Where(entry => entry.FileName.EndsWith(".xml")))
                            {
                                String objectName = Path.GetFileNameWithoutExtension(occlEntry.FileName);

                                TargetObjectDef occlMeshObjectDef = meshPlaceholders_.FirstOrDefault(obj => String.Compare(obj.Name, objectName, true) == 0);
                                if (occlMeshObjectDef == null)
                                    continue;

                                using (MemoryStream occlEntryMemoryStream = new MemoryStream((int)occlEntry.UncompressedSize))
                                {
                                    occlEntry.Extract(occlEntryMemoryStream);
                                    occlEntryMemoryStream.Seek(0, SeekOrigin.Begin);
                                    XDocument occlXmlDocument = XDocument.Load(occlEntryMemoryStream);

                                    String imapGroupName = occlMeshObjectDef.GetAttribute(AttrNames.OBJ_IPL_GROUP, AttrDefaults.OBJ_IPL_GROUP);
                                    if (String.IsNullOrEmpty(imapGroupName) || (String.Compare(imapGroupName, AttrDefaults.OBJ_IPL_GROUP, true) == 0))
                                    {
                                        transformedModels_.AddRange(GetOcclusionMeshesFromXmlDocument(occlXmlDocument, occlMeshObjectDef));
                                    }
                                    else
                                    {
                                        if (!transformedIMAPGroupModels_.ContainsKey(imapGroupName))
                                            transformedIMAPGroupModels_.Add(imapGroupName, new List<Model>());
                                        transformedIMAPGroupModels_[imapGroupName].AddRange(GetOcclusionMeshesFromXmlDocument(occlXmlDocument, occlMeshObjectDef));
                                    }
                                }
                            }
                        }
                    }
                }
            }

            meshPlaceholders_ = null;
        }

        #endregion Private Methods

        #region Private Data

        // Placeholder data
        private List<TargetObjectDef> boxPlaceholders_;
        private List<TargetObjectDef> meshPlaceholders_;

        // Translated occlusion data
        private List<Model> transformedModels_;
        private Dictionary<String, List<Model>> transformedIMAPGroupModels_;

        #endregion Private Data
    }
}
