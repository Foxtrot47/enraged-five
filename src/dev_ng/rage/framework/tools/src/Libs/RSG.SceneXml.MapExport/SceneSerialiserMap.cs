using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.SceneXml;

namespace RSG.SceneXml.MapExport
{

    /// <summary>
    /// Base Map File Serialiser from RSG.SceneXml.
    /// </summary>
    /// This includes common functionality used by both the IDE and IPL
    /// serialiser classes.
    public abstract class SceneSerialiserMap : ISceneSerialiser
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Branch object.
        /// </summary>
        public IBranch Branch
        {
            get;
            private set;
        }

        /// <summary>
        /// RSG.SceneXml Scene to be serialised.
        /// </summary>
        public Scene Scene
        {
            get;
            protected set;
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Map serialiser constructor, single Scene.
        /// </summary>
        /// <param name="branch">Branch</param>
        /// <param name="scene">Scene being serialised</param>
        public SceneSerialiserMap(IBranch branch, Scene scene)
        {
            this.Scene = scene;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Write the serialised scene to a disk location (default options).
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public abstract bool Write(String filename);

        /// <summary>
        /// Write the serialised scene to a disk location (with options).
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public abstract bool Write(String filename, Dictionary<String, Object> options);
        #endregion // Controller Methods
    }

    /// <summary>
    /// 
    /// </summary>
    public abstract class MultipleSceneSerialiserMap : IMultipleSceneSerialiser
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Branch object.
        /// </summary>
        public IBranch Branch
        {
            get;
            private set;
        }

        /// <summary>
        /// RSG.SceneXml Scene to be serialised.
        /// </summary>
        public Scene[] Scenes
        {
            get;
            protected set;
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Map serialiser constructor, multiple Scenes.
        /// </summary>
        /// <param name="branch">Branch</param>
        /// <param name="scenes">Scenes being serialised</param>
        public MultipleSceneSerialiserMap(IBranch branch, Scene[] scenes)
        {
            this.Branch = branch;
            this.Scenes = new Scene[scenes.Length];
            scenes.CopyTo(this.Scenes, 0);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Write the serialised scene to a disk location (default options).
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public abstract bool Write(String filename);

        /// <summary>
        /// Write the serialised scene to a disk location (with options).
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public abstract bool Write(String filename, Dictionary<String, Object> options);

        /// <summary>
        /// Write manifest data to XmlDocument.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        public abstract bool WriteManifestData(XDocument xmlDoc);
        #endregion // Controller Methods
    }

} // RSG.SceneXml.MapExport namespace

// SceneSerialiserMap.cs
