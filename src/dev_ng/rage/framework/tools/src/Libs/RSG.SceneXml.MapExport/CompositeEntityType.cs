﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using RSG.Base.Logging;

namespace RSG.SceneXml.MapExport
{

    /// <summary>
    /// Composite entity type (Archetype).
    /// </summary>
    public class CompositeEntityType
    {
        #region Constants
        private const String STATE_START = "start";
        private const String STATE_END = "end";
        private const String HREF_STATED_ANIM_GROUP_NO_STATE =
            "https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#StatedAnim_group_does_not_define_a_start.7Cend_state";
        private const String HREF_EFFECT_MULTIPLE_NAMES =
            "https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#Particle_Effects:_Multiple_PTFX_Asset_Sources";
        #endregion // Constants

        #region Properties and Associated Member Data
        /// <summary>
        /// Composite entity type name.
        /// </summary>
        public String Name
        {
            get;
            protected set;
        }

        /// <summary>
        /// Start object.
        /// </summary>
        public TargetObjectDef StartObject
        {
            get;
            protected set;
        }

        /// <summary>
        /// Animated objects.
        /// </summary>
        public TargetObjectDef[] AnimationObjects
        {
            get;
            protected set;
        }

        /// <summary>
        /// End object.
        /// </summary>
        public TargetObjectDef EndObject
        {
            get;
            protected set;
        }

        /// <summary>
        /// End object.
        /// </summary>
        public TargetObjectDef ProxyObject
        {
            get;
            protected set;
        }

        /// <summary>
        /// VFX asset name dependency.
        /// </summary>
        public String PtfxAssetName
        {
            get;
            protected set;
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="o"></param>
        public CompositeEntityType(TargetObjectDef start, TargetObjectDef end, TargetObjectDef[] anim, TargetObjectDef proxy, EffectCollection effectCollection)
        {
            this.Name = start.GetAttribute(AttrNames.OBJ_GROUP_NAME, AttrDefaults.OBJ_GROUP_NAME);
            this.StartObject = start;
            this.EndObject = end;
            this.AnimationObjects = anim;
            this.ProxyObject = proxy;

            DeterminePtfxAssetName(effectCollection);
        }
        #endregion // Constructor(s)

        #region Protected Methods
        /// <summary>
        /// Determine, and set, the PtfxAssetName property based on child
        /// Effect helpers.
        /// </summary>
        protected void DeterminePtfxAssetName(EffectCollection effectCollection)
        {
            // We collate effect tags first then process asset names so we
            // can give more information regarding which asset names apply
            // to which effect names.
            List<String> effectTags = new List<String>();
            if (null != this.StartObject)
                effectTags.AddRange(this.StartObject.GetPtfxEffectTags());
            foreach (TargetObjectDef animObj in this.AnimationObjects)
            {
                effectTags.AddRange(animObj.GetPtfxEffectTags());
                if (null != animObj.SkeletonRoot)
                    effectTags.AddRange(animObj.SkeletonRoot.GetPtfxEffectTags());
            }
            if (null != this.EndObject)
                effectTags.AddRange(this.EndObject.GetPtfxEffectTags());

            List<String> assetNames = new List<String>();
            foreach (String effectTag in effectTags)
            {
                String assetName = effectCollection.GetAssetNameForEffect(effectTag);
                if (!assetNames.Contains(assetName))
                    assetNames.Add(assetName);
            }

            Debug.Assert(assetNames.Count <= 1, 
                String.Format("Composite Entity references effects from multiple ({0}) PTFX assets.",
                assetNames.Count));
            if (assetNames.Count == 0)
            {
                this.PtfxAssetName = "";
            }
            else if (assetNames.Count > 1)
            {
                Log.Log__Error("Composite entity '{0}' has particle effects from multiple assets (Assets: {1}, Particles: {2}).  All effects must come from the same asset name.  See {3} for more information.",
                    this.Name, assetNames.Count, effectTags.Count, HREF_EFFECT_MULTIPLE_NAMES);
                foreach (String assetName in assetNames)
                    Log.Log__Warning("Composite entity '{0}' uses PTFX Asset: {1}.",
                        this.Name, assetName);
            }
            else
            {
                this.PtfxAssetName = assetNames[0];
            }
        }

        #endregion // Protected Methods
 
        #region Static Controller Methods
        /// <summary>
        /// Create all CompositeEntityType from single Scene.
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static CompositeEntityType[] CreateAll(Scene scene, EffectCollection effectCollection)
        {
            Dictionary<String, List<TargetObjectDef>> groups =
                new Dictionary<String, List<TargetObjectDef>>();
            foreach (TargetObjectDef o in scene.Walk(Scene.WalkMode.DepthFirst))
            {
                String group = "";
                if (o.IsStatedAnim())
                {
                    group = o.GetStatedAnimName();
                }
                if (o.IsAnimProxy())
                {
                    group = o.Name.Substring(0, o.Name.IndexOf("_anim"));
                }
                group = group.ToLower();

                // Hierarchies are not allowed to add all children.
                String parentGroup = null;
                if (null != o.Parent && o.Parent.IsStatedAnim())
                    parentGroup = o.Parent.GetStatedAnimName();

                if ("" != group && null == parentGroup)
                {
                    if (groups.ContainsKey(group))
                    {
                        groups[group].Add(o);
                    }
                    else
                    {
                        groups[group] = new List<TargetObjectDef>();
                        groups[group].Add(o);
                    }
                }
            }

            List<CompositeEntityType> compEntityTypes = new List<CompositeEntityType>();
            foreach (KeyValuePair<String, List<TargetObjectDef>> group in groups)
            {
                // Find Start and End Objects.
                TargetObjectDef start = null;
                TargetObjectDef[] anims = null;
                TargetObjectDef end = null;
                TargetObjectDef proxy = null;

                DivideGroup(group.Key, group.Value, out start, out anims, out end, out proxy);
                if (null != start && null != end && anims.Length > 0)
                    compEntityTypes.Add(new CompositeEntityType(start, end, anims, proxy, effectCollection));
            }

            return (compEntityTypes.ToArray());
        }
        #endregion // Static Controller Methods

        #region Private Static Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="group"></param>
        /// <param name="start"></param>
        /// <param name="anims"></param>
        /// <param name="end"></param>
        private static void DivideGroup(String name, IEnumerable<TargetObjectDef> group, out TargetObjectDef start, out TargetObjectDef[] anims, out TargetObjectDef end, out TargetObjectDef proxy)
        {
            start = null;
            end = null;
            proxy = null;
            List<TargetObjectDef> animObjects = new List<TargetObjectDef>();
            foreach (TargetObjectDef o in group)
            {
                if (o.IsAnimProxy())
                {
                    proxy = o;
                    continue;
                }

                String animState = o.GetAttribute(AttrNames.OBJ_ANIM_STATE, AttrDefaults.OBJ_ANIM_STATE);
                animState = animState.ToLower();
                switch (animState)
                {
                    case STATE_START:
                        Debug.Assert(null == start, "Start already defined.");
                        start = o;
                        break;
                    case STATE_END:
                        Debug.Assert(null == end, "End already defined.");
                        end = o;
                        break;
                    default:
                        animObjects.Add(o);
                        break;
                }
            }
            // Validation.
            if (null == start || null == end || 0 == animObjects.Count || null == proxy)
            {
                StringBuilder objectList = new StringBuilder();
                foreach (TargetObjectDef o in animObjects)
                    objectList.AppendFormat("{0}, ", o.Name);
            
                if (null == start)
                {
                    Log.Log__ErrorCtx(name, "StatedAnim group \"{0}\" does not define a start state (objects: {1}). See {2} for more information.",
                        name, objectList.ToString(), HREF_STATED_ANIM_GROUP_NO_STATE);
                }
                if (null == end)
                {
                    Log.Log__ErrorCtx(name, "StatedAnim group \"{0}\" does not define an end state (objects: {1}).  See {2} for more information.",
                        name, objectList.ToString(), HREF_STATED_ANIM_GROUP_NO_STATE);
                }
                if (0 == animObjects.Count)
                {
                    Log.Log__ErrorCtx(name, "StatedAnim group \"{0}\" does not have any animated objects defined (objects: {1}).",
                        name, objectList.ToString());
                }
                if (null == end)
                {
                    Log.Log__ErrorCtx(name, "StatedAnim group \"{0}\" does not define a proxy state (objects: {1}).  See {2} for more information.",
                        name, objectList.ToString(), HREF_STATED_ANIM_GROUP_NO_STATE);
                }
            }
            anims = animObjects.ToArray();
        }
        #endregion // Private Static Methods
    }

} // RSG.SceneXml.MapExport namespace
