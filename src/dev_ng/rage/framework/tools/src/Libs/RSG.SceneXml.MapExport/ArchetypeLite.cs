﻿//---------------------------------------------------------------------------------------------
// <copyright file="ArchetypeLite.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.SceneXml.MapExport
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;

    /// <summary>
    /// Archetype-lite.  Constructed after reading out the core game ITYP data
    /// to ensure we can reconstruct the on-disc ITYP entries for when they
    /// are dropped.
    /// </summary>
    public class ArchetypeLite
    {
        #region Properties
        public String Name
        {
            get;
            private set;
        }

        public String TXD
        {
            get;
            private set;
        }

        public String ModelDictionary
        {
            get;
            private set;
        }

        public string PhysicsDictionary { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="xmlArchetypeElem"></param>
        public ArchetypeLite(XElement xmlArchetypeElem)
        {
            XElement xmlNameElem = xmlArchetypeElem.Element("name");
            if (null != xmlNameElem)
                this.Name = xmlNameElem.Value.ToLower();
            else
                this.Name = String.Empty;
            
            XElement xmlTextureDictionaryElem = xmlArchetypeElem.Element("textureDictionary");
            if (null != xmlTextureDictionaryElem)
                this.TXD = xmlTextureDictionaryElem.Value.ToLower();
            else
                this.TXD = String.Empty;
            
            XElement xmlModelDictionaryElem = xmlArchetypeElem.Element("drawableDictionary");
            if ((null != xmlModelDictionaryElem) && !String.IsNullOrEmpty(xmlModelDictionaryElem.Value))
                this.ModelDictionary = xmlModelDictionaryElem.Value.ToLower();
            else
                this.ModelDictionary = String.Empty;

            XElement physicsDictionary = xmlArchetypeElem.Element("physicsDictionary");
            if (physicsDictionary != null)
                PhysicsDictionary = physicsDictionary.Value.ToLower();
            else
                PhysicsDictionary = string.Empty;
        }
        #endregion // Constructor(s)

        #region Object Overridden Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String ToString()
        {
            return (this.Name);
        }
        #endregion // Object Overridden Methods
    }

} // RSG.SceneXml.MapExport namespace
