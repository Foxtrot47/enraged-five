﻿using System;

namespace RSG.SceneXml.MapExport
{

    /// <summary>
    /// 
    /// </summary>
    public class CarGen
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public TargetObjectDef Object
        {
            get;
            private set;
        }

        /// <summary>
        /// Whether this CarGen has found a suitable IMAP container.
        /// </summary>
        public bool HasContainer
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="obj"></param>
        public CarGen(TargetObjectDef obj)
        {
            this.Object = obj;
            this.HasContainer = false;
        }
        #endregion // Constructor(s)
    }

} // RSG.SceneXml.MapExport namespace
