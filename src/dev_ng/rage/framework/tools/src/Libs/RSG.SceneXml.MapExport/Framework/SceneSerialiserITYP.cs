﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.Metadata.Util;
using RSG.SceneXml.MapExport.AssetCombine;
using RSG.SceneXml.MapExport.Framework.Collections;
using RSG.SceneXml.MapExport.Util;

namespace RSG.SceneXml.MapExport.Framework
{

	/// <summary>
	/// Framework ITYP Map Data Serialiser (abstract class)
	/// </summary>
	/// This will eventually replace the IDE serialiser completely.
	/// This should be used as a base class for project-specific serialisers.
	/// 
	/// This serialises ::rage::fwMapTypes structures.
	/// 
	public abstract class SceneSerialiserITYP<ContainerClass> :
        SceneSerialiserBase where ContainerClass : ITYPContainer
	{
		#region Member Data
        /// <summary>
        /// RSG.SceneXml Scene to be serialised.
        /// </summary>
        public Scene[] Scenes
        {
            get;
            protected set;
        }

		/// <summary>
		/// ITYP (archetype) container.
		/// </summary>
		public ContainerClass Container;

        /// <summary>
        /// ITYP (archetype) container for 'HD' archetypes (if we are splitting them up)
        /// </summary>
        public ContainerClass StreamedContainer;

        /// <summary>
        /// ITYP (archetype) containers for in-situ interiors
        /// </summary>
        public Dictionary<String, ContainerClass> InteriorContainerMap;

        /// <summary>
        /// ITYP (archetype) container for permanently flagged archetypes.
        /// </summary>
        public ContainerClass PermanentContainer;

        /// <summary>
        /// Asset combine data.
        /// </summary>
        public AssetFile AssetFile;

        /// <summary>
        /// Interior name map.
        /// </summary>
        protected InteriorContainerNameMap InteriorNameMap;

        protected String AudioCollisionFilename;

        /// <summary>
        /// A cache of the current in-situ interior that is used when we are adding archetypes hierarchically
        /// </summary>
        protected ITYPContainer CurrentInteriorITYPContainer;

        /// <summary>
        /// Dictionary of core archetype objects (by ITYP).
        /// </summary>
        protected IDictionary<String, IEnumerable<ArchetypeLite>> CoreArchetyopeObjects;
		#endregion // Member Data

		#region Constructor(s)
		/// <summary>
		/// IDE serialiser constructor, multiple Scene's.
		/// </summary>
        /// <param name="branch"></param>
		/// <param name="scenes"></param>
        /// <param name="af"></param>
        /// <param name="interiorNameMap"></param>
        /// <param name="audioCollisionFilename"></param>
        /// <param name="coreArchetypeObjects"></param>
        public SceneSerialiserITYP(IBranch branch, Scene[] scenes, AssetFile af, 
            InteriorContainerNameMap interiorNameMap, String audioCollisionFilename,
            IDictionary<String, IEnumerable<ArchetypeLite>> coreArchetypeObjects)
		    : base(branch)
        {
            this.Scenes = scenes;
            this.AssetFile = af;
            this.InteriorNameMap = interiorNameMap;
            this.AudioCollisionFilename = audioCollisionFilename;
            this.CoreArchetyopeObjects = coreArchetypeObjects;

			Reset();
		}
		#endregion // Constructor(s)
		
		#region Protected Object-Serialisation Methods
	    /// <summary>
	    /// Serialise a ::rage::fwMapData object to XML.
	    /// </summary>
	    /// This is the top-level Framework Map Data object; that acts as a
	    /// container for all the other map data.
	    /// <param name="name"></param>
	    /// <param name="container"></param>
	    /// <returns></returns>
	    protected static XElement AsFwMapTypes(String name, ITYPContainer container)
        {
            XElement node = new XElement(name);

            Xml.CreateElementWithText(node, "name", container.Name);

            node.Add(new XElement("archetypes"));
            node.Add(new XElement("extensions"));

			return (node);
		}

		/// <summary>
		/// Serialise a ::rage::fwArchetypeDef object to XML.
		/// </summary>
		/// <param name="archetype"></param>
        /// <param name="assetFile"></param>
        /// <param name="coreArchetype"></param>
		/// <returns></returns>
		protected static XElement AsFwArchetypeDef(Archetype archetype, AssetFile assetFile, ArchetypeLite coreArchetype)
        {
            Debug.Assert(null != archetype.Object.LocalBoundingBox,
                "Object for Archetype has no Bounding Box!");
            BoundingBox3f bbox = archetype.Object.GetLocalBoundingBox();
            BoundingSpheref bsphere = new BoundingSpheref(bbox);
			
			XElement xmlItem = Xml.CreateElementWithAttribute(null, "Item", "type", "::rage::fwArchetypeDef");

            // Todo Flo: for the time being, we set the name and assetName the same.
            // Todo Flo: This will allow us later on to specify different ones, for DLC for example.
            Xml.CreateElementWithText(xmlItem, "name", archetype.Name);
		    Xml.CreateElementWithText(xmlItem, "assetName", archetype.Name);

            // Flags are defined per-project.
            Xml.CreateElementWithValueAttribute(xmlItem, "lodDist", archetype.LODDistance);
            Xml.CreateElementWithVectorAttributes(xmlItem, "bbMin", bbox.Min);
            Xml.CreateElementWithVectorAttributes(xmlItem, "bbMax", bbox.Max);
            Xml.CreateElementWithVectorAttributes(xmlItem, "bsCentre", bsphere.Centre);
            Xml.CreateElementWithValueAttribute(xmlItem, "bsRadius", bsphere.Radius);
            if (archetype.HasHDTextureDistance)
                Xml.CreateElementWithValueAttribute(xmlItem, "hdTextureDist", archetype.HDTextureDistance);
            Xml.CreateElementWithValueAttribute(xmlItem, "specialAttribute", archetype.SpecialAttribute);

            bool isNewDlcObject = Archetype.Branch.Project.ForceFlags.AllNewContent || archetype.Object.IsNewDLCObject();
            if (Archetype.Branch.Project.IsDLC && !isNewDlcObject)
            {
                if (null != coreArchetype)
                {
                    // Pull data from core archetype.
                    Xml.CreateElementWithText(xmlItem, "textureDictionary", coreArchetype.TXD);
                    Xml.CreateElementWithText(xmlItem, "drawableDictionary", coreArchetype.ModelDictionary);
                    if (!string.IsNullOrEmpty(coreArchetype.PhysicsDictionary))
                    {
                        Xml.CreateElementWithText(xmlItem, "physicsDictionary", coreArchetype.PhysicsDictionary);
                    }
                }
                else
                {
                    // special case here. We dont have core metadata for interiors or props yet;
                    // we consider them as "need to be fully serialised as they are", even if we cant find a core reference
                    // we write out their txd and dd and pd if we have them
                    SceneType sceneType = archetype.Object.MyScene.SceneType;
                    if (sceneType == SceneType.EnvironmentInterior || sceneType == SceneType.EnvironmentProps)
                    {
                        Xml.CreateElementWithText(xmlItem, "textureDictionary", archetype.TextureDictionary);
                        Xml.CreateElementWithText(xmlItem, "drawableDictionary", archetype.ModelDictionary);
                        if (!string.IsNullOrEmpty(archetype.PhysicsDictionary))
                        {
                            Xml.CreateElementWithText(xmlItem, "physicsDictionary", archetype.PhysicsDictionary);
                        }
                    }
                    else
                    {
                        Log.Log__WarningCtx(archetype.Name, "Archetype found and not flagged as 'New DLC' but it doesn't have a core game archetype.  Should it be flagged as 'New DLC'?");
                    }
                }
            }
            else
            {
                switch (archetype.Object.LODLevel)
                {
                    case ObjectDef.LodLevel.SLOD4:
                    case ObjectDef.LodLevel.SLOD3:
                    case ObjectDef.LodLevel.SLOD2:
                    case ObjectDef.LodLevel.SLOD1:
                    case ObjectDef.LodLevel.LOD:
                        if ((null != assetFile) &&
                            assetFile.InputDrawableDictionaries.ContainsKey(archetype.RawName))
                        {
                            String txd = assetFile.InputTextureDictionaries[archetype.RawName];
                            String modelGroup = assetFile.InputDrawableDictionaries[archetype.RawName];
                            Xml.CreateElementWithText(xmlItem, "textureDictionary", txd);
                            Xml.CreateElementWithText(xmlItem, "drawableDictionary", modelGroup);
                        }
                        else if (null != assetFile)
                        {
                            // Fallback; we have an Asset File specified but it doesn't contain an entry.
                            Xml.CreateElementWithText(xmlItem, "textureDictionary", archetype.TextureDictionary);
                            Xml.CreateElementWithText(xmlItem, "drawableDictionary", String.Empty);
                        }
                        else
                        {
                            // Fallback.
                            Xml.CreateElementWithText(xmlItem, "textureDictionary", archetype.TextureDictionary);
                            Xml.CreateElementWithText(xmlItem, "drawableDictionary", archetype.ModelDictionary);
                        }
                        break;
                    case ObjectDef.LodLevel.HD:
                    case ObjectDef.LodLevel.ORPHANHD:
                    case ObjectDef.LodLevel.UNKNOWN:
                    default:
                        Xml.CreateElementWithText(xmlItem, "textureDictionary", archetype.TextureDictionary);
                        Xml.CreateElementWithText(xmlItem, "drawableDictionary", String.Empty);
                        if (!string.IsNullOrEmpty(archetype.PhysicsDictionary))
                        {
                            Xml.CreateElementWithText(xmlItem, "physicsDictionary", archetype.PhysicsDictionary);
                        }
                        break;
                }
            }

			// Clip Dictionary (if applicable)
			if (archetype.Object.IsAnimObject())
				Xml.CreateElementWithText(xmlItem, "clipDictionary", archetype.ClipDictionary);

			return (xmlItem);
		}
		
		/// <summary>
		/// Serialise a ::rage::fwExtension object to XML.
		/// </summary>
		/// <param name="doc"></param>
		/// <param name="instance"></param>
		/// <returns></returns>
		protected static XElement AsFwExtensionDef(Extension extension)
        {
            XElement xmlItem = Xml.CreateElementWithAttribute(null, "Item", "type", "::rage::fwExtensionDef");
            Xml.CreateElementWithText(xmlItem, "name", extension.Parent.GetObjectName());
            return (xmlItem);
		}
		#endregion // Protected Object-Serialisation Methods
		
		#region Scene and Object Processing Methods
        /// <summary>
        /// Process all scenes into internal structures ready for serialisation
        /// </summary>
        public abstract bool Process(String pathname);

        /// <summary>
        /// Process internal structures to ITYP file(s)
        /// </summary>
        public abstract bool Serialise(String pathname);

        /// <summary>
        /// Write manifest data to XmlDocument.
        /// </summary>
        public abstract bool WriteManifestData(XDocument xmlDoc);

		/// <summary>
		/// Clear all internal state.
		/// </summary>
		protected abstract void Reset();

		/// <summary>
		/// Process a set of Scene objects.
		/// </summary>
		protected virtual void ProcessScenes(IEnumerable<Scene> scenes)
		{
			foreach (Scene scene in scenes)
				ProcessScene(scene);
		}

		/// <summary>
		/// Process a single Scene object.
		/// </summary>
		/// <param name="scene">Scene object to process.</param>
		protected virtual void ProcessScene(Scene scene)
		{
            foreach (TargetObjectDef o in scene.Objects)
            {
				ProcessObject(o);
			}
		}

		/// <summary>
		/// Process a single ObjectDef object.
		/// </summary>
		/// <param name="o">ObjectDef object to process.</param>
        protected virtual void ProcessObject(TargetObjectDef o)
        {
            if (o.DontExport() || o.DontExportIDE() || SceneOverrideIntegrator.Instance.IsObjectMarkedForDeletion(o))
                return;

            if (o.Is2dfxLightEffect() && o.DontExportLightEffect())
                return;

			// GD: Drawable LOD states don't ever need to go into the ITYP.
            // Shadow meshes need same treatment
           if (o.HasDrawableLODChildren() || o.HasRenderSimParent() || o.HasShadowMeshLink())
				return;

            ProcessSceneObject(o);
		}

		/// <summary>
		/// Process a single top-level SceneXml ObjectDef.  This recurses into
		/// its children if required.
		/// </summary>
		/// <param name="obj">ObjectDef to process</param>
        protected virtual void ProcessSceneObject(TargetObjectDef obj)
        {
            // Check to see if we're in a DLC project. If so, only serialize the DLC specific objects.
            // Children check is to ensure we iterate into MLO, MLO Rooms, MLO Portals etc.
            // TODO Flo DLC Markup
            //bool isNewDlcObject = this.Branch.Project.ForceFlags.AllNewContent || obj.IsNewDLCObject();
            //if (this.Branch.Project.IsDLC && !obj.IsContainer() && !isNewDlcObject && (0 == obj.Children.Length))
            //    return;

            if (obj.DontExport() || obj.DontExportIDE() || SceneOverrideIntegrator.Instance.IsObjectMarkedForDeletion(obj))
				return;
			// GunnarD: url:bugstar:99343 Avoid container children getting processed, too.
            // Shadow meshes need same treatment
            if (obj.HasDrawableLODChildren() || obj.HasRenderSimParent() || obj.HasShadowMeshLink())
				return;

            if (obj.IsMiloTri())
            {
                foreach (TargetObjectDef child in obj.Children)
                    ProcessSceneObject(child);
            }
            else if (obj.IsXRef())
                return; // Skipped
            else if (obj.IsRefObject())
                return; // Skipped
            else if (obj.IsInternalRef())
                return; // Skipped
            else if (obj.IsContainerLODRefObject())
                return; // Skipped
            else if (obj.IsRefInternalObject())
                return; // Skipped
            else if (obj.IsBlock())
                return; // Skipped
            else if (obj.Is2dfx())
                return; // Skipped
            else if (obj.IsCarGen())
                return; // Skipped
            else if (obj.IsCollision())
                return; // Skipped
            else if (obj.IsAnimProxy())
                return; // Skipped
            else if (obj.IsIMAPGroupDefinition())
            {
                // Process the linked meshes as if they were root objects.
                foreach (TargetObjectDef child in obj.Children)
                    ProcessSceneObject(child);
            }
            else if (obj.IsObject())
            {
                //// TODO Flo DLC Markup
                //if (this.Branch.Project.IsDLC && !isNewDlcObject)
                //    return; // Ignore non-DLC objects in DLC.

                Archetype archetype = new Archetype(obj);
                AddArchetype(archetype);
                ProcessSceneExtensions(obj, archetype, obj);
            }
            else if (obj.IsMilo())
            {
                //// TODO Flo DLC Markup
                //if (this.Branch.Project.IsDLC && !isNewDlcObject)
                //    return; // Ignore non-DLC objects in DLC.

                if (InteriorContainerMap == null)
                {
                    this.Container.AddArchetype(new Archetype(obj));
                    foreach (TargetObjectDef child in obj.Children)
                        ProcessSceneObject(child);
                }
                else
                {
                    // This is an in-situ interior
                    String containerName = InteriorNameMap.GetContainerNameFromObject(obj);
                    InteriorContainerMap.Add(containerName, CreateITYPContainer());
                    InteriorContainerMap[containerName].AddArchetype(new Archetype(obj));
                    CurrentInteriorITYPContainer = InteriorContainerMap[containerName];
                    foreach (TargetObjectDef child in obj.Children)
                        ProcessSceneObject(child);
                    CurrentInteriorITYPContainer = null;
                }
            }
            else if (obj.IsMloPortal() || obj.IsMloRoom())
            {
                foreach (TargetObjectDef child in obj.Children)
                    ProcessSceneObject(child);
            }
            else if (obj.IsContainer())
            {
                foreach (TargetObjectDef child in obj.Children)
                    ProcessSceneObject(child);
            }
            else if (obj.IsPropGroup())
            {
                foreach (TargetObjectDef child in obj.Children)
                    ProcessSceneObject(child);
            }
            else if (obj.IsIMAPGroupDummy())
                return; // Skipped
            else if (obj.IsDummy())
            {
                Log.Log__Warning("Ignoring Dummy {0} as this is how 3dsmax represents Groups.", obj);
            }
            else if (obj.IsTimeCycleBox() || obj.IsTimeCycleSphere())
                return; // Skipped
            else if (obj.IsEditableSpline() || obj.IsText() || obj.IsLine())
                return; // Skipped
            else if (obj.IsStreamingExtentsOverrideBox())
                return; // Skipped
            else if (obj.IsPointHelper())
                return; // Skipped
            else if (obj.IsCableProxy())
                return; // Skipped
            else
            {
                Log.Log__WarningCtx(obj.Name, "Ignoring {0} {1} as type not recognised by ITYP serialiser.", obj.Name, obj.Class);
            }
		}

		/// <summary>
		/// Process a single SceneXml ObjectDef for 2DFX objects.
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="archetype"></param>
		/// <param name="obj"></param>
        protected virtual void ProcessSceneExtensions(TargetObjectDef parent, Archetype archetype, TargetObjectDef obj)
        {
            if (archetype.Object.DontExport() || archetype.Object.DontExportIDE() || SceneOverrideIntegrator.Instance.IsObjectMarkedForDeletion(archetype.Object))
                return;

            if (obj.Is2dfx())
                archetype.AddExtension(new Extension(obj, parent));

            // Children (for 2DFX entries)
            foreach (TargetObjectDef o in obj.Children)
                ProcessSceneExtensions(parent, archetype, o);
            
            // Skeleton Children (for 2DFX entries, stated anim)
            if (obj.SkeletonRootGuid != Guid.Empty)
            {
                foreach (TargetObjectDef o in obj.SkeletonRoot.Children)
                    ProcessSceneExtensions(parent, archetype, o);
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        protected abstract ContainerClass CreateITYPContainer();

        /// <summary>
        /// Add archetype to the relevant archetype container.
        /// </summary>
        /// <param name="archetype"></param>
        protected void AddArchetype(Archetype archetype)
        {
            bool isPermanent = archetype.Object.GetAttribute(AttrNames.OBJ_PERMANENT_ARCHTYPE,
                AttrDefaults.OBJ_PERMANENT_ARCHTYPE);

            if (isPermanent && null != this.PermanentContainer)
            {
                this.PermanentContainer.AddArchetype(archetype);
            }
            else if (CurrentInteriorITYPContainer != null)
            {
                CurrentInteriorITYPContainer.AddArchetype(archetype);
            }
            else if (this.StreamedContainer == null)
            {
                this.Container.AddArchetype(archetype);
            }
            else
            {
                if (archetype.StreamMode == Archetype.StreamModeType.Streamed)
                    this.StreamedContainer.AddArchetype(archetype);
                else
                    this.Container.AddArchetype(archetype);
            }
        }
		#endregion // Scene and Object Processing Methods
	}

} // RSG.SceneXml.MapExport.Framework namespace
