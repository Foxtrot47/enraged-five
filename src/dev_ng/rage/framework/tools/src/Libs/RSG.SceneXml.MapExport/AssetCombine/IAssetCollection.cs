﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace RSG.SceneXml.MapExport.AssetCombine
{

    /// <summary>
    /// Asset collection interface.
    /// </summary>
    public interface IAssetCollection
    {
        #region Properties 
        /// <summary>
        /// Associated container name.
        /// </summary>
        String Name { get;  }

        /// <summary>
        /// Cache directory.
        /// </summary>
        String CacheDir { get;  }

        /// <summary>
        /// Output contents into the combined data.
        /// </summary>
        AssetOutput[] Contents { get; }
        #endregion // Properties

        #region Methods
        /// <summary>
        /// Add a single output.
        /// </summary>
        /// <param name="output"></param>
        void AddContent(AssetOutput output);

        /// <summary>
        /// Add a range of outputs.
        /// </summary>
        /// <param name="outputs"></param>
        void AddContentRange(IEnumerable<AssetOutput> outputs);

        /// <summary>
        /// Serialise this collection to an XmlElement.
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="projectIsDLC"></param>
        /// <returns></returns>
        XmlElement ToXml(XmlDocument xmlDoc, bool projectIsDLC);
        #endregion // Methods
    }

} // RSG.SceneXml.MapExport.AssetCombine namespace
