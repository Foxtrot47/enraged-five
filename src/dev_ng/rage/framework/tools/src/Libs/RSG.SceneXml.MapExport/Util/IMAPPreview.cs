﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.ManagedRage;
using RSG.SceneXml.MapExport.Project.GTA5;

namespace RSG.SceneXml.MapExport.Util
{

    /// <summary>
    /// 
    /// </summary>
    /// TODO Flo: rework main algorithm. The Count() calls leads to multiple enumerations and makes it slower and less efficient than it should be
    public static class IMAPPreview
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="workingDirectory"></param>
        /// <param name="tree"></param>
        /// <param name="scene"></param>
        /// <param name="imapSerialiser"></param>
        public static void GenerateIMAPsForPreview_GTA5(IBranch branch, String workingDirectory, IContentTree tree, Scene scene, SceneSerialiserIMAP imapSerialiser, bool executeProcess = true)
        {
            // Count the number of IMAP files generated
            string[] generatedImapPathnames = System.IO.Directory.GetFiles(workingDirectory, "*.imap", SearchOption.TopDirectoryOnly);
            int numGeneratedLongImaps = generatedImapPathnames.Where(generatedImap => generatedImap.Contains("_long_")).Count();
            int numGeneratedStrmImaps = generatedImapPathnames.Where(generatedImap => generatedImap.Contains("_strm_")).Count();
            int numGeneratedCriticalImaps = generatedImapPathnames.Where(generatedImap => generatedImap.Contains("_critical_")).Count();

            // Count the number of IMAP files required
            string rpfPathname = GetTargetRPFFilename(branch, tree, scene);
            string[] requiredImapFilenames = GetImapFilenamesFromRPF(rpfPathname);
            int numRequiredLongImaps = requiredImapFilenames.Where(requiredImap => requiredImap.Contains("_long_")).Count();
            int numRequiredStrmImaps = requiredImapFilenames.Where(requiredImap => requiredImap.Contains("_strm_")).Count();
            int numRequiredCriticalImaps = requiredImapFilenames.Where(requiredImap => requiredImap.Contains("_critical_")).Count();

            string contentName = Path.GetFileNameWithoutExtension(rpfPathname);

            // Generate empty _long_ IMAP files as required by the game
            for (int longImapIndex = numGeneratedLongImaps; longImapIndex < numRequiredLongImaps; ++longImapIndex)
            {
                string emptyLongImapFilename = string.Format("{0}_long_{1}.imap", contentName, longImapIndex);
                string emptyLongImapPathname = Path.Combine(workingDirectory, emptyLongImapFilename);
                imapSerialiser.WriteEmptyIMAPFile(emptyLongImapPathname);
            }

            // Generate empty _strm_ IMAP files as required by the game
            for (int strmImapIndex = numGeneratedStrmImaps; strmImapIndex < numRequiredStrmImaps; ++strmImapIndex)
            {
                string emptyStrmImapFilename = string.Format("{0}_strm_{1}.imap", contentName, strmImapIndex);
                string emptyStrmImapPathname = Path.Combine(workingDirectory, emptyStrmImapFilename);
                imapSerialiser.WriteEmptyIMAPFile(emptyStrmImapPathname);
            }

            // Generate empty _strm_ IMAP files as required by the game
            for (int criticalImapIndex = numGeneratedCriticalImaps; criticalImapIndex < numRequiredCriticalImaps; ++criticalImapIndex)
            {
                string emptyCriticalImapFilename = string.Format("{0}_critical_{1}.imap", contentName, criticalImapIndex);
                string emptyCriticalImapPathname = Path.Combine(workingDirectory, emptyCriticalImapFilename);
                imapSerialiser.WriteEmptyIMAPFile(emptyCriticalImapPathname);
            }

            // The platform conversion now requires input files to live within the export or processed directories
            // Here we ensure that we have a clean directory at assets/export/temp/imap_preview/
            string imapPreviewBaseDirectory = Path.Combine(branch.Export, "temp", "imap_preview");
            if (System.IO.Directory.Exists(imapPreviewBaseDirectory))
                System.IO.Directory.Delete(imapPreviewBaseDirectory, true);
            System.IO.Directory.CreateDirectory(imapPreviewBaseDirectory);

            // Gather a space separated list of IMAP pathnames for the ruby script trailing arguments
            StringBuilder imapPathnamesBuilder = new StringBuilder();
            foreach (string imapFileCreated in System.IO.Directory.EnumerateFiles(workingDirectory, "*.imap", SearchOption.TopDirectoryOnly))
            {
                // Copy each file to imapPreviewBaseDirectory so that it can be converted
                string conversionSourcePathname = Path.Combine(imapPreviewBaseDirectory, Path.GetFileName(imapFileCreated));
                System.IO.File.Copy(imapFileCreated, conversionSourcePathname);

                imapPathnamesBuilder.Append(conversionSourcePathname);
                imapPathnamesBuilder.Append(" ");
            }

            if (executeProcess)
            {
#warning DHM FIX ME: we shouldn't kick this off in here.
                System.Diagnostics.Process convertProcess = new System.Diagnostics.Process();
                convertProcess.StartInfo.FileName = Environment.ExpandEnvironmentVariables(RUBY_EXE_PATHNAME);
                string scriptWithArgs = RUBY_DATA_CONVERT_FILE_PATHNAME + " " + imapPathnamesBuilder.ToString();
                convertProcess.StartInfo.Arguments = Environment.ExpandEnvironmentVariables(scriptWithArgs);
                convertProcess.StartInfo.UseShellExecute = false;
                convertProcess.StartInfo.CreateNoWindow = false;

                convertProcess.Start();
                convertProcess.WaitForExit();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="workingDirectory"></param>
        /// <param name="tree"></param>
        /// <param name="scene"></param>
        /// <param name="imapSerialiser"></param>
        public static void GenerateIMAPsForPreview_GTA5_NG(IBranch branch, String workingDirectory, IContentTree tree, Scene scene, Project.GTA5_NG.SceneSerialiserIMAP imapSerialiser, bool executeProcess = true)
        {
            // Count the number of IMAP files generated
            string[] generatedImapPathnames = System.IO.Directory.GetFiles(workingDirectory, "*.imap", SearchOption.TopDirectoryOnly);
            int numGeneratedLongImaps = generatedImapPathnames.Where(generatedImap => generatedImap.Contains("_long_")).Count();
            int numGeneratedStrmImaps = generatedImapPathnames.Where(generatedImap => generatedImap.Contains("_strm_")).Count();
            int numGeneratedCriticalImaps = generatedImapPathnames.Where(generatedImap => generatedImap.Contains("_critical_")).Count();

            // Count the number of IMAP files required
            string rpfPathname = GetTargetRPFFilename(branch, tree, scene);
            string[] requiredImapFilenames = GetImapFilenamesFromRPF(rpfPathname);
            int numRequiredLongImaps = requiredImapFilenames.Where(requiredImap => requiredImap.Contains("_long_")).Count();
            int numRequiredStrmImaps = requiredImapFilenames.Where(requiredImap => requiredImap.Contains("_strm_")).Count();
            int numRequiredCriticalImaps = requiredImapFilenames.Where(requiredImap => requiredImap.Contains("_critical_")).Count();

            string contentName = Path.GetFileNameWithoutExtension(rpfPathname);

            // Generate empty _long_ IMAP files as required by the game
            for (int longImapIndex = numGeneratedLongImaps; longImapIndex < numRequiredLongImaps; ++longImapIndex)
            {
                string emptyLongImapFilename = string.Format("{0}_long_{1}.imap", contentName, longImapIndex);
                string emptyLongImapPathname = Path.Combine(workingDirectory, emptyLongImapFilename);
                imapSerialiser.WriteEmptyIMAPFile(emptyLongImapPathname);
            }

            // Generate empty _strm_ IMAP files as required by the game
            for (int strmImapIndex = numGeneratedStrmImaps; strmImapIndex < numRequiredStrmImaps; ++strmImapIndex)
            {
                string emptyStrmImapFilename = string.Format("{0}_strm_{1}.imap", contentName, strmImapIndex);
                string emptyStrmImapPathname = Path.Combine(workingDirectory, emptyStrmImapFilename);
                imapSerialiser.WriteEmptyIMAPFile(emptyStrmImapPathname);
            }

            // Generate empty _strm_ IMAP files as required by the game
            for (int criticalImapIndex = numGeneratedCriticalImaps; criticalImapIndex < numRequiredCriticalImaps; ++criticalImapIndex)
            {
                string emptyCriticalImapFilename = string.Format("{0}_critical_{1}.imap", contentName, criticalImapIndex);
                string emptyCriticalImapPathname = Path.Combine(workingDirectory, emptyCriticalImapFilename);
                imapSerialiser.WriteEmptyIMAPFile(emptyCriticalImapPathname);
            }

            // The platform conversion now requires input files to live within the export or processed directories
            // Here we ensure that we have a clean directory at assets/export/temp/imap_preview/
            string imapPreviewBaseDirectory = Path.Combine(branch.Export, "temp", "imap_preview");
            if (System.IO.Directory.Exists(imapPreviewBaseDirectory))
                System.IO.Directory.Delete(imapPreviewBaseDirectory, true);
            System.IO.Directory.CreateDirectory(imapPreviewBaseDirectory);

            // Gather a space separated list of IMAP pathnames for the ruby script trailing arguments
            StringBuilder imapPathnamesBuilder = new StringBuilder();
            foreach (string imapFileCreated in System.IO.Directory.EnumerateFiles(workingDirectory, "*.imap", SearchOption.TopDirectoryOnly))
            {
                // Copy each file to imapPreviewBaseDirectory so that it can be converted
                string conversionSourcePathname = Path.Combine(imapPreviewBaseDirectory, Path.GetFileName(imapFileCreated));
                System.IO.File.Copy(imapFileCreated, conversionSourcePathname);

                imapPathnamesBuilder.Append(conversionSourcePathname);
                imapPathnamesBuilder.Append(" ");
            }

            if (executeProcess)
            {
#warning DHM FIX ME: we shouldn't kick this off in here.
                System.Diagnostics.Process convertProcess = new System.Diagnostics.Process();
                convertProcess.StartInfo.FileName = Environment.ExpandEnvironmentVariables(RUBY_EXE_PATHNAME);
                string scriptWithArgs = RUBY_DATA_CONVERT_FILE_PATHNAME + " " + imapPathnamesBuilder.ToString();
                convertProcess.StartInfo.Arguments = Environment.ExpandEnvironmentVariables(scriptWithArgs);
                convertProcess.StartInfo.UseShellExecute = false;
                convertProcess.StartInfo.CreateNoWindow = false;

                convertProcess.Start();
                convertProcess.WaitForExit();
            }
        }



        /// <summary>
        /// Derives the target RPF file from a given Scene object
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="contentTree"></param>
        /// <param name="firstInputScene"></param>
        /// <returns></returns>
        private static string GetTargetRPFFilename(IBranch branch, IContentTree contentTree, Scene firstInputScene)
        {
            // TODO Flo: this is dead code; it does nothing. Returns nothing. Rework or remove.
            ProcessorCollection processors = new ProcessorCollection(branch.Project.Config);
            ContentTreeHelper contentHelper = new ContentTreeHelper(contentTree);

            IContentNode zipContentNode = contentTree.CreateFile(Path.ChangeExtension(firstInputScene.Filename, ".zip"));
            HashSet<IContentNode> outputNodes = contentHelper.GetAllOutputsFromNode(zipContentNode);

            // Find the end of the content tree for this scene
            /*string contentName = Path.GetFileNameWithoutExtension(firstInputScene.Filename);
            ContentNodeMap contentMapNode = (ContentNodeMap)configGameView.Content.Root.FindFirst(contentName, "map");
            ContentNodeMapZip contentMapZipNode = contentMapNode.Outputs.OfType<ContentNodeMapZip>().First();// mapzip
            string outputFilename = contentMapZipNode.Filename.Replace('\\', '/');// e.g. X:/gta5/assets/export/levels/tools_test/tools_test.zip
            string exportDataBaseDirectory = configGameView.ExportDir.Replace('\\', '/');// e.g. X:/gta5/assets/export/
            if (contentMapZipNode.Outputs.OfType<ContentNodeMapProcessedZip>().Count() == 1)// some levels don't get processed
            {
                ContentNodeMapProcessedZip contentProcessedMapZipNode = contentMapZipNode.Outputs.OfType<ContentNodeMapProcessedZip>().First();// processedzip (if available)
                outputFilename = contentProcessedMapZipNode.Filename.Replace('\\', '/');// e.g. X:/gta5/assets/processed/levels/tools_test/tools_test.zip
                exportDataBaseDirectory = configGameView.ProcessedDir.Replace('\\', '/');// e.g. X:/gta5/assets/processed/
            }

            // Now we derive the rpf pathname from the output filename
            string firstEnabledTargetBuildDirectory = configGameView.Targets(null).Where(target => target.Enabled).Select(target => target.Directory).First().Replace('\\', '/');
             */

            string rpfPathname = ""; // Path.ChangeExtension(outputFilename, "rpf").Replace(exportDataBaseDirectory, firstEnabledTargetBuildDirectory);

            return rpfPathname;
        }

        /// <summary>
        /// Loads an RPF and returns the independant equivalent (IMAP) instance data entries
        /// </summary>
        /// <param name="rpfPathname"></param>
        /// <returns></returns>
        private static string[] GetImapFilenamesFromRPF(string rpfPathname)
        {
            // Finally (phew) we load the rpf and find our imap equivalent files
            Packfile rpfFile = new Packfile();
            if (!rpfFile.Load(rpfPathname))
                return new string[0];

            // return the renamed files
            return rpfFile.Entries.Where(entry => entry.Name.EndsWith("map")).Select(entry => Path.ChangeExtension(entry.Name, "imap")).ToArray();
        }

        // Ruby interop constants
        private static readonly String RUBY_EXE_PATHNAME = @"%RS_TOOLSROOT%\ironlib\prompt.bat";
        private static readonly String RUBY_DATA_CONVERT_FILE_PATHNAME = @"%RS_TOOLSROOT%\bin\ironruby\bin\ir.exe %RS_TOOLSROOT%\ironlib\util\data_convert_file.rb --preview";
    }

} // RSG.SceneXml.MapExport.Util namespace
