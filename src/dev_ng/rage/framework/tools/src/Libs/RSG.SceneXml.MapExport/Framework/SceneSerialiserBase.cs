﻿using System;
using RSG.Base.Configuration;

namespace RSG.SceneXml.MapExport.Framework
{

    /// <summary>
    /// Abstract base class for Scene Serialiser classes.
    /// </summary>
    public abstract class SceneSerialiserBase
    {
        #region Properties
        /// <summary>
        /// Branch object.
        /// </summary>
        public IBranch Branch
        {
            get;
            private set;
        }

        /// <summary>
        /// Project object.
        /// </summary>
        public IProject Project
        {
            get { return (this.Branch.Project); }
        }

        /// <summary>
        /// Whether the project is DLC.
        /// </summary>
        public bool IsDLC
        {
            get { return (this.Project.IsDLC); }
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="branch"></param>
        public SceneSerialiserBase(IBranch branch)
        {
            if (null == branch)
                throw (new ArgumentNullException("branch"));

            this.Branch = branch;
        }
        #endregion // Constructor(s)
    }

} // RSG.SceneXml.MapExport.Framework namespace
