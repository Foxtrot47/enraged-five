﻿using System;
using System.Collections.Generic;

using RSG.Base.Logging;
using RSG.Base.Math;

namespace RSG.SceneXml.MapExport
{

    /// <summary>
    /// 
    /// </summary>
    public sealed class InteriorEntity : Entity
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Appended entities (MILO+ objects).
        /// </summary>
        public Entity[] AppendEntities
        {
            get { return m_AppendEntities.ToArray(); }
        }
        private List<Entity> m_AppendEntities;

        /// <summary>
        /// Property to encapsulate the WorldBoundingBox of the underlying object.  This is overridden by InteriorEntity, which calculates the BB for in-situ Milos
        /// </summary>
        public override BoundingBox3f WorldBoundingBox
        {
            get 
            {
                if (this.Object.WorldBoundingBox != null)
                {
                    return this.Object.WorldBoundingBox; 
                }

                if (calculatedWorldBoundingBox_ == null)
                    CalculateWorldBoundingBox();

                return calculatedWorldBoundingBox_;
            }
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor, from ObjectDef.
        /// </summary>
        /// <param name="obj"></param>
        public InteriorEntity(TargetObjectDef obj)
            : base(obj)
        {
            Init();
        }

        /// <summary>
        /// Constructor, from ObjectDef.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="parent">LOD parent Entity (or null)</param>
        public InteriorEntity(TargetObjectDef obj, Entity parent)
            : base(obj, parent)
        {
            Init();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        public void AddEntity(Entity e)
        {
            this.m_AppendEntities.Add(e);
        }
        #endregion // Controller Methods
        
        #region Private Methods
        /// <summary>
        /// Initialise self.
        /// </summary>
        private void Init()
        {
            this.m_AppendEntities = new List<Entity>();
            this.InstanceLODDistance = true;

            bool instloddist = this.Object.GetAttribute(AttrNames.OBJ_INSTANCE_LOD_DISTANCE, AttrDefaults.OBJ_INSTANCE_LOD_DISTANCE);

            // B* 479586 - support for per instance LOD distance overrides - JWR
            if (instloddist)
            {
                this.LODDistance = this.Object.GetAttribute(AttrNames.OBJ_LOD_DISTANCE, AttrDefaults.OBJ_LOD_DISTANCE);
            }
            else
            {
                if (this.Object.IsMilo())// in-situ interior entity
                {
                    this.LODDistance = this.Object.GetAttribute(AttrNames.MILO_DETAIL, AttrDefaults.MILO_DETAIL);
                }
                else if (null != this.Object.RefObject && this.Object.RefObject.IsMiloTri())// RsRef interior entity
                {
                    String miloObjectName = this.Object.RefObject.GetObjectName();
                    TargetObjectDef miloObjectDef = this.Object.RefObject.MyScene.FindObject(miloObjectName);
                    if (null != miloObjectDef)
                    {
                        this.LODDistance = miloObjectDef.GetAttribute(AttrNames.MILO_DETAIL, AttrDefaults.MILO_DETAIL);
                    }
                    else
                    {
                        Log.Log__WarningCtx(this.Object.Name, 
                            "[art] Entity '{0}' is unresolved interior reference.  " + 
                            "The milo proxy was found, but not the milo itself.  " + 
                            "Has the interior '{1}' been deleted?  Could not find for LOD distance.", 
                            this.Object.Name, this.Object.RefName);
                        this.LODDistance = AttrDefaults.MILO_DETAIL;
                    }
                }
                else
                {
                    Log.Log__WarningCtx(this.Object.Name, 
                        "[art] Entity '{0}' is unresolved interior reference.  Has the interior '{1}' been deleted?  Could not find for LOD distance.",
                        this.Object.Name, this.Object.RefName);
                    this.LODDistance = AttrDefaults.MILO_DETAIL;
                }
            }
        }

        private void CalculateWorldBoundingBox()
        {
            calculatedWorldBoundingBox_ = new BoundingBox3f();

            CalculateWorldBoundingBoxRecursive(this.Object, calculatedWorldBoundingBox_);
        }

        private static void CalculateWorldBoundingBoxRecursive(TargetObjectDef objectDef, BoundingBox3f boundingBox)
        {
            if(objectDef.WorldBoundingBox != null)
                boundingBox.Expand(objectDef.WorldBoundingBox);

            foreach(TargetObjectDef childObjectDef in objectDef.Children)
                CalculateWorldBoundingBoxRecursive(childObjectDef, boundingBox);
        }
        #endregion // Private Methods

        private BoundingBox3f calculatedWorldBoundingBox_;
    }

} // RSG.SceneXml.MapExport namespace
