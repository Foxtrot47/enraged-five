﻿using System;
using System.Collections.Generic;
using RSG.Base.Math;
using RSG.SceneXml.MapExport.Framework;

namespace RSG.SceneXml.MapExport.Project.GTA5_NG.Collections
{

    /// <summary>
    /// IMAP Container class; used by the IMAP serialiser(s).
    /// </summary>
    public class IMAPContainer : Framework.Collections.IMAPContainer
    {
        #region Properties
        /// <summary>
        /// Blocks within this container..
        /// </summary>
        public Block[] Blocks
        {
            get { return (m_Blocks.ToArray()); }
        }
        protected List<Block> m_Blocks;

        /// <summary>
        /// Car Generator helpers within this container.
        /// </summary>
        public CarGen[] CarGens
        {
            get { return (m_CarGens.ToArray()); }
        }
        protected List<CarGen> m_CarGens;

        /// <summary>
        /// TimeCycle Modifier boxes within this container.
        /// </summary>
        public TargetObjectDef[] TimeCycleModifierBoxes
        {
            get { return (m_TimeCycleModifierBoxes.ToArray()); }
        }
        protected List<TargetObjectDef> m_TimeCycleModifierBoxes;

        /// <summary>
        /// Lists of instanced entity data.
        /// </summary>
        public InstancedEntityData InstanceEntityData
        {
            get { return m_InstancedEntityData; }
        }
        protected InstancedEntityData m_InstancedEntityData;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public IMAPContainer()
            : base()
        {
            Reset();
        }

        /// <summary>
        /// Constructor, with optional name.
        /// </summary>
        /// <param name="name"></param>
        public IMAPContainer(String name)
            : base(name)
        {
            Reset();
        }

        /// <summary>
        /// Constructor, specifying the name and bounding boxes.
        /// </summary>
        /// <param name="name"></param>
        public IMAPContainer(String name, BoundingBox3f streamingExtents, BoundingBox3f entityExtents)
            : base(name)
        {
            Reset();

            StreamingBoundingBox = streamingExtents;
            EntityBoundingBox = entityExtents;
        }
        #endregion // Constructor(s)
        
        #region Controller Methods
        /// <summary>
        /// Reset internal data.
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            this.m_Blocks = new List<Block>();
            this.m_CarGens = new List<CarGen>();
            this.m_TimeCycleModifierBoxes = new List<TargetObjectDef>();
            this.m_InstancedEntityData = new InstancedEntityData();
        }

        #region Block Collection Methods
        /// <summary>
        /// Add Block object.
        /// </summary>
        /// <param name="block"></param>
        public void AddBlock(Block block)
        {
            this.m_Blocks.Add(block);
            this.ContentFlags |= IMAP_eContentFlags.CF_BLOCKINFO;
        }

        /// <summary>
        /// Add set of Block objects.
        /// </summary>
        /// <param name="blocks"></param>
        public void AddBlockRange(IEnumerable<Block> blocks)
        {
            this.m_Blocks.AddRange(blocks);
        }
        #endregion // Block Collection Methods

        #region CarGen Collection Methods
        /// <summary>
        /// Add CarGen object.
        /// </summary>
        /// <param name="cargen"></param>
        public void AddCarGen(CarGen cargen)
        {
            this.m_CarGens.Add(cargen);
        }

        /// <summary>
        /// Add set of CarGen objects.
        /// </summary>
        /// <param name="cargens"></param>
        public void AddCarGenRange(IEnumerable<CarGen> cargens)
        {
            foreach (CarGen cargen in cargens)
                AddCarGen(cargen);
        }
        #endregion // CarGen Collection Methods

        #region TimeCycle Modifiers Collection Methods
        /// <summary>
        /// Add TimeCycle Modifier Box object.
        /// </summary>
        /// <param name="cargen"></param>
        public void AddTimeCycleModifierBox(TargetObjectDef o)
        {
            this.m_TimeCycleModifierBoxes.Add(o);
        }
        #endregion // TimeCycle Modifiers Collection Methods

        public void IncorporateTimeCycleBoxExtents()
        {
            foreach (TargetObjectDef objectDef in m_TimeCycleModifierBoxes)
            {
                EntityBoundingBox.Expand(objectDef.WorldBoundingBox);
                StreamingBoundingBox.Expand(objectDef.WorldBoundingBox);
            }
        }

        #region Instanced Entity Data Methods 
        /// <summary>
        /// Sets the link between the entity lists and the high-definition entity IMAPs.
        /// </summary>
        /// <param name="imapLink"></param>
        public void SetInstancedDataIMAPLink(String imapLink)
        {
            this.InstanceEntityData.IMAPLink = imapLink;
        }

        /// <summary>
        /// Add set of Instanced Grass Entity objects.
        /// </summary>
        /// <param name="blocks"></param>
        public void AddGrassEntities(String platformString, InstancedEntityList grassEntities)
        {
            this.ContentFlags |= IMAP_eContentFlags.CF_INSTANCED_ENTITIES;
            if (!this.InstanceEntityData.GrassEntitiesList.ContainsKey(platformString))
                this.InstanceEntityData.GrassEntitiesList.Add(platformString, new List<InstancedEntityList>());
            this.InstanceEntityData.GrassEntitiesList[platformString].Add(grassEntities);
        }

        /// <summary>
        /// Add set of Instanced Prop Entity objects specifically as trees.
        /// </summary>
        /// <param name="blocks"></param>
        public void AddTreeEntities(String platformString, InstancedEntityList treeEntities)
        {
            this.ContentFlags |= IMAP_eContentFlags.CF_INSTANCED_ENTITIES;
            if (!this.InstanceEntityData.TreeEntitiesList.ContainsKey(platformString))
                this.InstanceEntityData.TreeEntitiesList.Add(platformString, new List<InstancedEntityList>());
            this.InstanceEntityData.TreeEntitiesList[platformString].Add(treeEntities);
        }

        /// <summary>
        /// Sets the split size for next-gen when ordering instance lists.
        /// </summary>
        /// <param name="splitSize"></param>
        public void SetNextGenInstanceSplitSize(int splitSize)
        {
            this.InstanceEntityData.NextGenSplitSize = splitSize;
        }

        /// <summary>
        /// Add the platforms to order the instance list for next-gen.
        /// </summary>
        /// <param name="splitSize"></param>
        public void AddNextGenSplitPlatform(String platformString)
        {
            this.InstanceEntityData.NextGenPlatformSplits.Add(platformString);
        }
        #endregion
        #endregion // Controller Methods
    }


    /// <summary>
    /// Named IMAP Group/IPL Group for streaming by script.
    /// </summary>
    public class IMAPGroupContainer
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Group container name.
        /// </summary>
        public String Name
        {
            get;
            set;
        }

        /// <summary>
        /// IPL Group LOD and SLOD1 instances.
        /// </summary>
        public IMAPContainer Static
        {
            get;
            protected set;
        }

        /// <summary>
        /// IPL Group HD instances (default).
        /// </summary>
        public IMAPContainer Streamed
        {
            get;
            protected set;
        }

        /// <summary>
        /// IPL Group SLOD2/SLOD3 instances.
        /// </summary>
        public IMAPContainer StaticSLOD
        {
            get;
            protected set;
        }

        /// <summary>
        /// IPL Group Interior instances; IMAP per interior.
        /// </summary>
        public Dictionary<String, IMAPContainer> InteriorContainerMap
        {
            get;
            protected set;
        }

        /// <summary>
        /// Type of artist chosen activation
        /// </summary>
        public string[] ActivationTypes
        {
            get;
            set;
        }

        /// <summary>
        /// Weather activation
        /// </summary>
        public string[] ActiveWeather
        {
            get;
            set;
        }

        /// <summary>
        /// Hourly activation
        /// </summary>
        public int ActiveHours
        {
            get;
            set;
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public IMAPGroupContainer()
            : base()
        {
            this.StaticSLOD = new IMAPContainer();
            this.StaticSLOD.Flags = IMAP_eFlags.F_MANUAL_STREAM_ONLY;
            this.StaticSLOD.Parent = this.StaticSLOD;
            this.Static = new IMAPContainer();
            this.Static.Flags = Framework.IMAP_eFlags.F_MANUAL_STREAM_ONLY; 
            this.Static.Parent = this.StaticSLOD;
            this.Streamed = new IMAPContainer();
            this.Streamed.Flags = Framework.IMAP_eFlags.F_MANUAL_STREAM_ONLY;
            this.Streamed.Parent = this.Static;
            this.InteriorContainerMap = new Dictionary<String, IMAPContainer>();
        }

        /// <summary>
        /// Constructor, simply initialises our containers.
        /// </summary>
        public IMAPGroupContainer(String name)
            : base()
        {
            this.Name = name;
            this.StaticSLOD = new IMAPContainer();
            this.StaticSLOD.Name = String.Format("{0}_slod", this.Name);
            this.StaticSLOD.Flags = IMAP_eFlags.F_MANUAL_STREAM_ONLY;
            this.StaticSLOD.Parent = this.StaticSLOD;
            this.Static = new IMAPContainer();
            this.Static.Name = String.Format("{0}_lod", this.Name);
            this.Static.Flags = Framework.IMAP_eFlags.F_MANUAL_STREAM_ONLY;
            this.Static.Parent = this.StaticSLOD;
            this.Streamed = new IMAPContainer();
            this.Streamed.Name = this.Name;
            this.Streamed.Flags = Framework.IMAP_eFlags.F_MANUAL_STREAM_ONLY;
            this.Streamed.Parent = this.Static;            
            this.InteriorContainerMap = new Dictionary<string,IMAPContainer>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Clear all sub-containers.
        /// </summary>
        public void Clear()
        {
            this.StaticSLOD.Reset();
            this.Static.Reset();
            this.Streamed.Reset();
            this.InteriorContainerMap.Clear();
        }

        /// <summary>
        /// Find the Entity for an object (searches all IMAP containers).
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Entity Find(ObjectDef obj)
        {
            Entity find = FindStaticSLOD2or3(obj);
            if (null == find)
                find = FindStatic(obj);
            if (null == find)
                find = FindStreamed(obj);
            return (find);
        }

        /// <summary>
        /// Find the Entity for an object in the static container.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Entity FindStatic(ObjectDef obj)
        {
            return (Find(obj, this.Static.Entities));
        }

        /// <summary>
        /// Find the Entity for an object in the static SLOD2/3 container.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Entity FindStaticSLOD2or3(ObjectDef obj)
        {
            return (Find(obj, this.StaticSLOD.Entities));
        }

        /// <summary>
        /// Find the Entity for an object in the streamed (HD) container.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Entity FindStreamed(ObjectDef obj)
        {
            return (Find(obj, this.Streamed.Entities));
        }

        /// <summary>
        /// Return the number of combined entities.
        /// </summary>
        /// <returns></returns>
        public int NumEntities()
        {
            return (StaticSLOD.Entities.Count + Static.Entities.Count + Streamed.Entities.Count);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="depName"></param>
        public void AddDependency(String depName)
        {
            this.StaticSLOD.AddDependency(depName);
            this.Static.AddDependency(depName);
            this.Streamed.AddDependency(depName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sourceName"></param>
        /// <param name="targetName"></param>
        public void ReplaceDependency(String sourceName, String targetName)
        {
            this.StaticSLOD.ReplaceDependency(sourceName, targetName);
            this.Static.ReplaceDependency(sourceName, targetName);
            this.Streamed.ReplaceDependency(sourceName, targetName);
        }

        /// <summary>
        /// Replace one dependency with multiple dependencies in case of split
        /// </summary>
        /// <param name="sourceName"></param>
        /// <param name="targetNames"></param>
        public void ReplaceDependencies(string sourceName, IEnumerable<string> targetNames)
        {
            StaticSLOD.ReplaceDependency(sourceName, targetNames);
            Static.ReplaceDependency(sourceName, targetNames);
            Streamed.ReplaceDependency(sourceName, targetNames);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Find an Instance6 for an ObjectDef. 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private Entity Find(ObjectDef obj, IEnumerable<Entity> instances)
        {
            foreach (Entity inst in instances)
            {
                if (inst.Object.Equals(obj))
                    return (inst);
            }
            return (null);
        }
        #endregion // Private Methods
    }

} // RSG.SceneXml.MapExport.Project.GTA.Collections namespace
