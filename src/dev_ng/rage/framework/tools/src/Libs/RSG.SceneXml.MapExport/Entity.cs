﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.SceneXml.MapExport.Framework;
using RSG.SceneXml.MapExport.Util;

namespace RSG.SceneXml.MapExport
{

    /// <summary>
    /// Entity represents a map object instance.
    /// </summary>
    public class Entity :
        ExtensionContainer,
        IComparable,
        IComparable<Entity>
    {
        #region Constants
        private static readonly float LOD_DISTANCE_STREAMED_LONG = 150.0f;
        #endregion // Constants

        #region Enumerations
        /// <summary>
        /// StreamMode enumeration.
        /// </summary>
        public enum StreamModeType
        {
            /// <summary>
            /// Entity is streamed (default)
            /// </summary>
            Streamed,
            /// <summary>
            /// Entity has large LOD distance; place in streamed long files..
            /// </summary>
            StreamedLong,
            /// <summary>
            /// Entity is deemed critical
            /// </summary>
            StreamedCritical,
            /// <summary>
            /// Entity is not-streamed (always loaded, "Static" IMAP)
            /// </summary>
            NotStreamed,

            /// <summary>
            /// Entity stream mode unknown.
            /// </summary>
            Unknown,
        }
        #endregion // Enumerations

        #region Properties and Associated Member Data
        /// <summary>
        /// Archetype name.
        /// </summary>
        public String Name
        {
            get
            {
                // Todo Flo: Please fix me one day. 
                bool isNewDlcObject = Branch.Project.ForceFlags.AllNewContent || this.Object.IsNewDLCObject();

                // If we are a reference to a DLC object then we add DLC prefix; otherwise we add core prefix.
                if (null != Entity.Branch && Entity.Branch.Project.IsDLC && this.Object.IsResolvedRefObject() && 
                    (this.Object.RefFile.StartsWith(Entity.Branch.Project.Root, StringComparison.OrdinalIgnoreCase)))
                {
                    // RefFile case above for Interiors pointing to the interior proxy.
                    String mapDLCPrefix = MapAsset.GetMapProjectPrefix(Entity.Branch.Project);
                    return (String.Format("{0}{1}", mapDLCPrefix, this.Object.GetObjectName()));
                }
                // Otherwise if we are a core-project RefObject we don't prefix.
                else if (null != Entity.Branch && Entity.Branch.Project.IsDLC && this.Object.IsResolvedRefObject() &&
                         (this.Object.RefFile.StartsWith(Entity.Branch.Project.Config.CoreProject.Root + "\\", StringComparison.OrdinalIgnoreCase)))
                {
                    return (this.Object.GetObjectName());
                }
                // Otherwise if we are defined in the current map; not a reference.
                else if (null != Entity.Branch && Entity.Branch.Project.IsDLC && !this.Object.IsRefObject() &&
                    isNewDlcObject)
                {
                    String mapDLCPrefix = MapAsset.GetMapProjectPrefix(Entity.Branch.Project);
                    return (String.Format("{0}{1}", mapDLCPrefix, this.Object.GetObjectName()));
                }
                else
                {
                    IProject project = (Branch.Project.IsDLC && isNewDlcObject)
                       ? Branch.Project
                       : Branch.Project.Config.CoreProject;
                    String mapCorePrefix = MapAsset.GetMapProjectPrefix(project);
                    return (String.Format("{0}{1}", mapCorePrefix, this.Object.GetObjectName()));
                }
            }
        }

        /// <summary>
        /// Streamable object streaming mode.  Ultimately determines which IMD
        /// file the instance will end up in (streamed, non-streamed or strbig).
        /// </summary>
        public StreamModeType StreamMode
        {
            get;
            private set;
        }

        /// <summary>
        /// Object within SceneXml Scene.
        /// </summary>
        public TargetObjectDef Object
        {
            get;
            private set;
        }

        /// <summary>
        /// LOD Parent entity object.
        /// </summary>
        public Entity Parent
        {
            get;
            internal set;
        }

        /// <summary>
        /// LOD Children entity objects.
        /// </summary>
        public List<Entity> Children
        {
            get;
            set;
        }

        /// <summary>
        /// LOD distance.
        /// </summary>
        public float LODDistance
        {
            get;
            set;
        }

        /// <summary>
        /// Child LOD distance.
        /// </summary>
        public float ChildLODDistance
        {
            get;
            protected set;
        }

        /// <summary>
        /// Flag to determine whether -1 get written for LOD distance or we
        /// actually serialise LODDistance (above) for reference objects.
        /// </summary>
        public bool InstanceLODDistance
        {
            get;
            protected set;
        }

        /// <summary>
        /// Instance LOD xmlParent index as required in IPL file.
        /// </summary>
        public int LODParentIndex
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public uint FileIndex
        {
            get;
            internal set;
        }

        /// <summary>
        /// Property to encapsulate the WorldBoundingBox of the underlying object.  This is overridden by InteriorEntity, which calculates the BB for in-situ Milos
        /// </summary>
        public virtual BoundingBox3f WorldBoundingBox
        {
            get { return this.Object.WorldBoundingBox; }
        }

        /// <summary>
        /// An optional override for the streaming extents for this entity.
        /// This is null by default, which means the extents are calculated via GetStreamingExtents()
        /// </summary>
        public BoundingBox3f StreamingExtentsOverride
        {
            get;
            set;
        }

        /// <summary>
        /// Unique entity identifier; per container per archetype.
        /// </summary>
        public UInt32 Hash
        {
            get;
            protected set;
        }

        /// <summary>
        /// An override variable for an entity's world transform.
        /// </summary>
        public Matrix34f WorldTransformOverride
        {
            get;
            set;
        }

        /// <summary>
        /// An override variable for an entity's scale.  
        /// </summary>
        public Vector3f ScaleOverride
        {
            get;
            set;
        }
		
        public bool IsCritical
        {
            get;
            private set;
        }

        /// <summary>
        /// An override to specify if this entity was created by the instance placement processor.
        /// </summary>
        public bool IsInstancedObject
        {
            get;
            set;
        }
        #endregion // Properties and Associated Member Data

        #region Static Properties
        /// <summary>
        /// Branch for DLC name determination.  TargetObjectDef.Target isn't always set
        /// and need to get the project for its prefix.
        /// </summary>
#warning DHM FIX ME: hack to get DLC working on GTA5.
        public static IBranch Branch
        {
            get;
            set;
        }
        #endregion // Static Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor, from ObjectDef.
        /// </summary>
        /// <param name="obj"></param>
        public Entity(TargetObjectDef obj)
        {
            this.Object = obj;
            Init();
        }

        /// <summary>
        /// Constructor, from ObjectDef.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="parent">LOD parent Entity (or null)</param>
        public Entity(TargetObjectDef obj, Entity parent)
        {
            this.Object = obj;
            this.Parent = parent;
            Init();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return the streaming extents of this Entity object.
        /// </summary>
        /// The streaming extents is defined by the LOD distance of this 
        /// instance; and depends on whether its in a LOD hierarchy or not.
        /// If we have a LOD parent then its relative to the parent's pivot.
        /// <returns></returns>
        public BoundingBox3f GetStreamingExtents()
        {
            if (StreamingExtentsOverride != null)
                return StreamingExtentsOverride;// JWR - support for artist overridden streaming extents

            BoundingBox3f streamingExtents = new BoundingBox3f();
            float lod = this.LODDistance;
            if (this.Object.IsRefInternalObject() || this.Object.IsInternalRef())
            {
                if (null != this.Object.InternalRef)
                {
                    lod = SceneOverrideIntegrator.Instance.GetObjectLODDistance(this.Object.InternalRef);
                }
                else
                {
                    lod = -1.0f;
                }
            }

            Vector3f vlod = new Vector3f(lod, lod, lod);
            if (this.Object.HasLODParent())
            {
                TargetObjectDef streamingExtentsCentreObject = this.Object.LOD.Parent;
                if (streamingExtentsCentreObject.IsContainerLODRefObject())
                {
                    streamingExtentsCentreObject = InterContainerLODManager.Instance.GetSourceSLOD2ObjectDef(streamingExtentsCentreObject);

                    if (streamingExtentsCentreObject != null)// fall back to the SLOD2 object if we can't look up the source parent
                    {
                        // B*1291104 - we need to use the parent's child LOD distance for streaming extents
                        float lodDistance = streamingExtentsCentreObject.GetAttribute(AttrNames.OBJ_CHILD_LOD_DISTANCE, AttrDefaults.OBJ_CHILD_LOD_DISTANCE);
                        vlod = new Vector3f(lodDistance, lodDistance, lodDistance);
                    }
                    else
                    {
                        streamingExtentsCentreObject = this.Object;
                    }
                }
                streamingExtents.Expand(streamingExtentsCentreObject.NodeTransform.Translation + vlod);
                streamingExtents.Expand(streamingExtentsCentreObject.NodeTransform.Translation - vlod);
            }
            else
            {
                if (this.WorldTransformOverride == null)
                {
                    streamingExtents.Expand(this.Object.NodeTransform.Translation + vlod);
                    streamingExtents.Expand(this.Object.NodeTransform.Translation - vlod);
                }
                else
                {
                    streamingExtents.Expand(this.WorldTransformOverride.Translation + vlod);
                    streamingExtents.Expand(this.WorldTransformOverride.Translation - vlod);
                }
            }
            return (streamingExtents);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Initialise self.
        /// </summary>
        private void Init()
        {
            this.Children = new List<Entity>();
            this.LODParentIndex = -1;
            this.InstanceLODDistance = false;
            this.ChildLODDistance = 0;
            this.WorldTransformOverride = null;
            this.ScaleOverride = null;
            this.IsInstancedObject = false;

            if (this.Object.IsRefObject() || this.Object.IsXRef() ||
                this.Object.IsRefInternalObject() || this.Object.IsInternalRef())
            {
                bool instloddist = this.Object.GetAttribute(AttrNames.OBJ_INSTANCE_LOD_DISTANCE,
                    AttrDefaults.OBJ_INSTANCE_LOD_DISTANCE);
                if (instloddist)
                {
                    this.InstanceLODDistance = true;
                    this.LODDistance = SceneOverrideIntegrator.Instance.GetObjectLODDistance(this.Object);
                }
                else
                {
                    if (this.Object.IsRefObject() || this.Object.IsXRef())
                    {
                        if (null == this.Object.RefObject)
                        {
                            Log.Log__WarningCtx(this.Object.Name, "[art] Entity {0} is unresolved external reference.  Has the prop {1} been deleted?  Could not find for LOD distance.",
                                this.Object.Name, this.Object.RefName);
                        }
                        else
                            this.LODDistance = SceneOverrideIntegrator.Instance.GetObjectLODDistance(this.Object.RefObject);
                    }
                    else if (this.Object.IsRefInternalObject() || this.Object.IsInternalRef())
                    {
                        this.LODDistance = -1.0f;
                    }
                }
            }
            else
            {
                this.LODDistance = SceneOverrideIntegrator.Instance.GetObjectLODDistance(this.Object);

                this.ChildLODDistance = SceneOverrideIntegrator.Instance.GetObjectChildLODDistance(this.Object);
            }

            DetermineIsCritical();// B* 1186193 - is this entity 'critical'?
            DetermineStreamMode();
            SetUniqueIdentifier();
        }

        /// <summary>
        /// Dertermine the entity streaming mode (based on LOD hierarchy and
        /// LOD distance).
        /// </summary>
        private void DetermineStreamMode()
        {
            switch (this.Object.LODLevel)
            {
                case ObjectDef.LodLevel.SLOD4:
                case ObjectDef.LodLevel.SLOD3:
                case ObjectDef.LodLevel.SLOD2:
                case ObjectDef.LodLevel.SLOD1:
                case ObjectDef.LodLevel.LOD:
                    this.StreamMode = StreamModeType.NotStreamed;
                    break;
                case ObjectDef.LodLevel.HD:
                case ObjectDef.LodLevel.ORPHANHD:
                    if (!this.Object.IsMiloTri())
                    {
                        // B* 971299 - JWR - this isn't ideal but prevents having to make a bigger change
                        float effectiveLODDistance = this.LODDistance;
                        if (this.Object.HasLODParent())
                        {
                            float parentChildLODDistance = this.Object.LOD.Parent.GetAttribute(
                                AttrNames.OBJ_CHILD_LOD_DISTANCE, AttrDefaults.OBJ_CHILD_LOD_DISTANCE);
                            if (parentChildLODDistance != AttrDefaults.OBJ_CHILD_LOD_DISTANCE && 
                                parentChildLODDistance != effectiveLODDistance)
                                effectiveLODDistance = parentChildLODDistance;
                        }

                        if (this.IsCritical)
                            this.StreamMode = StreamModeType.StreamedCritical;
                        else if (effectiveLODDistance > LOD_DISTANCE_STREAMED_LONG)
                            this.StreamMode = StreamModeType.StreamedLong;
                        else
                            this.StreamMode = StreamModeType.Streamed;
                    }
                    else
                    {
                        this.StreamMode = StreamModeType.NotStreamed;
                    }
                    break;
                default:
                    this.StreamMode = StreamModeType.Unknown;
                    break;
            }
        }
        
        /// <summary>
        /// Set our unique identifier.
        /// </summary>
        private void SetUniqueIdentifier()
        {
            UInt32 containerHash = this.Object.MyScene.Hash32;
            UInt32 hash = RSG.ManagedRage.StringHashUtil.atStringHash(
                this.Object.AttributeGuid.ToString(), containerHash);
            this.Hash = hash;                    
        }

        private void DetermineIsCritical()
        {
            this.IsCritical = false;
            List<TargetObjectDef> lodSiblings = new List<TargetObjectDef>();
            lodSiblings.Add(this.Object);
            if (this.Object.LOD != null && this.Object.LOD.Siblings != null)
                lodSiblings.AddRange(this.Object.LOD.Siblings);

            foreach (TargetObjectDef lodSibling in lodSiblings)
            {
                if (lodSibling.IsRefObject() &&
                    lodSibling.RefObject != null &&
                    lodSibling.RefObject.GetAttribute(AttrNames.OBJ_CRITICAL, AttrDefaults.OBJ_CRITICAL))
                {
                    this.IsCritical = true;
                    break;
                }
                else if (lodSibling.IsObject() &&
                    lodSibling.GetAttribute(AttrNames.OBJ_CRITICAL, AttrDefaults.OBJ_CRITICAL))
                {
                    this.IsCritical = true;
                    break;
                }
            }
        }

        #endregion // Protected Methods

        #region IComparable and IComparable<Entity> Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        int IComparable.CompareTo(Object entity)
        {
            if (entity is Entity)
                return (CompareTo(entity as Entity));
            throw new ArgumentException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(Entity entity)
        {
            // This algorithm is duplicated in InterContainerLODManager.CompareObjectDefsOnLODData(), so don't change it

            if (TargetObjectDef.IsLODParentOf(this.Object, entity.Object))
                return (1); // Child (this) follows entity.
            else if (TargetObjectDef.IsLODChildOf(this.Object, entity.Object))
                return (-1); // Parent (this) precedes entity.
            else
            {
                fwEntityDef_eLodTypes lodLevel = LodLevel.Get(this);
                fwEntityDef_eLodTypes lodLevel2 = LodLevel.Get(entity);

                if ((int)lodLevel > (int)lodLevel2)
                    return (-1);
                else if ((int)lodLevel < (int)lodLevel2)
                    return (1);
                return (0);
            }                
        }
        #endregion // IComparable and IComparable<Archetype> Methods

        #region Static Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static int CompareEntities(Entity a, Entity b)
        {
            return (a.CompareTo(b));
        }
        #endregion // Static Controller Methods
    }

} // RSG.SceneXml.MapExport namespace
