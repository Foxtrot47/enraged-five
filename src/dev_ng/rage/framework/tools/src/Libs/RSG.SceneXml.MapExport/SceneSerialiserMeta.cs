﻿//
// File: SceneSerialiserMETA.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of SceneSerialiserMETA class
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging;

namespace RSG.SceneXml.MapExport
{

    /// <summary>
    /// Map IDE File Serialiser from RSG.SceneXml.
    /// </summary>
    [Obsolete]
    public class SceneSerialiserMETA : MultipleSceneSerialiserMap
    {
        #region Member Data
        List<Definition> m_lObjects;
        List<Definition> m_lTimeObjects;
        List<Definition> m_lAnimObjects;
        List<Definition> m_lTimeAnimObjects;
        List<Definition> m_lMilos;
        List<Definition> m_l2dfx;
        List<Definition> m_lAudioMaterials;
        List<Definition> m_statedAnimObjects;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Serialiser constructor.
        /// </summary>
        /// <param name="scene">RSG.SceneXml Scene</param>
        public SceneSerialiserMETA(IBranch branch, Scene[] scenes)
            : base(branch, scenes)
        {
            m_lObjects = new List<Definition>();
            m_lTimeObjects = new List<Definition>();
            m_lAnimObjects = new List<Definition>();
            m_lTimeAnimObjects = new List<Definition>();
            m_lMilos = new List<Definition>();
            m_l2dfx = new List<Definition>();
            m_lAudioMaterials = new List<Definition>();
            m_statedAnimObjects = new List<Definition>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Write the serialised scene to a disk location (default options).
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public override bool Write(String filename)
        {
            return (Write(filename, new Dictionary<String, Object>()));
        }

        /// <summary>
        /// Write the serialised scene to a disk location (default options).
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public override bool Write(String filename, Dictionary<String, Object> options)
        {
            Log.Log__Message("Starting META serialisation:");
            Log.Log__Message("\tDestination: {0}", filename);
            Log.Log__Message("\tScenes ({0}):", this.Scenes.Length);
            foreach (Scene scene in this.Scenes)
                Log.Log__Message("\t\t{0}", scene.Filename);

            Reset();
            foreach (Scene scene in this.Scenes)
            {
                foreach (TargetObjectDef o in scene.Objects)
                {
                    if (o.DontExport())
                        continue;
                    if (o.DontExportIDE())
                        continue;

                    ProcessSceneObject(o);
                }
            }
            SortLists();

            // Write IDE file
            Log.Log__Message("Writing {0}...", filename);
            XDocument xmlDoc = new XDocument(new XDeclaration("1.0", "utf-8", null));

            // Create the root element
            XElement rootNode = new XElement("CMapTypes");
            xmlDoc.Add(rootNode);

            // Create the Compundentity/statedAnim element
            XElement statedAnimNode = new XElement("compositeEntityTypes");
            foreach (Definition def in this.m_statedAnimObjects)
            {
                // only consider the animation state as the animation name has to be defined and only one is needed anyway.
                String myAnimState = def.Object.GetAttribute(AttrNames.OBJ_ANIM_STATE, AttrDefaults.OBJ_ANIM_STATE);
                if (myAnimState == "Start")
                {
                    XNode statedAnimChild = def.ToMETAStatedAnimObject(this.m_statedAnimObjects, def.Object.MyScene, def.Object);
                    if (null != statedAnimChild)
                        statedAnimNode.Add(statedAnimChild);
                }
            }
            if (statedAnimNode.Elements().Any())
                rootNode.Add(statedAnimNode);
            else if (this.m_statedAnimObjects.Count > 0)
            {
                Log.Log__Error("Scene outputs no stated anims, but groups were found. Are animated objects dynamic?");
            }
            
            // Create further elements

            // Save to the XML file
            if(rootNode.Elements().Any())
                xmlDoc.Save(filename);

            Log.Log__Message("XML meta file created.");

            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        public override bool WriteManifestData(XDocument xmlDoc)
        {
            return (true);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Clear all internal state.
        /// </summary>
        private void Reset()
        {
            m_lObjects.Clear();
            m_lTimeObjects.Clear();
            m_lAnimObjects.Clear();
            m_lTimeAnimObjects.Clear();
            m_lMilos.Clear();
            m_l2dfx.Clear();
            m_lAudioMaterials.Clear();
            m_statedAnimObjects.Clear();
        }

        /// <summary>
        /// Process a single top-level SceneXml ObjectDef.  This recurses into
        /// its children if required.
        /// </summary>
        /// <param name="obj">ObjectDef to process</param>
        private void ProcessSceneObject(TargetObjectDef obj)
        {
            if (obj.DontExport() || obj.DontExportIDE())
                return;

            if (obj.IsXRef())
                return; // Skipped
            else if (obj.IsInternalRef())
                return; // Skipped
            else if (obj.IsRefInternalObject())
                return; // Skipped
            else if (obj.IsBlock())
                return; // Skipped
            else if (obj.Is2dfx())
                return; // Skipped
            else if (obj.IsCarGen())
                return; // Skipped
            else if (obj.IsCollision())
                return; // Skipped
            else if (obj.IsAnimProxy())
                return; // Skipped
            else if (obj.IsStatedAnim())
            {
                m_statedAnimObjects.Add(new Definition(obj.MyScene, obj));
            }
            else if (obj.IsIMAPGroupDummy())
                return; // Skipped
            else if (obj.IsTimeObject() && obj.IsAnimObject())
                m_lTimeAnimObjects.Add(new Definition(obj.MyScene, obj));
            else if (obj.IsTimeObject())
                m_lTimeObjects.Add(new Definition(obj.MyScene, obj));
            else if (obj.IsAnimObject())
                m_lAnimObjects.Add(new Definition(obj.MyScene, obj));
            else if (obj.IsObject())
                m_lObjects.Add(new Definition(obj.MyScene, obj));
            else if (obj.IsMilo())
            {
                m_lMilos.Add(new Definition(obj.MyScene, obj));
                foreach (TargetObjectDef child in obj.Children)
                    ProcessSceneObject(child);
            }
            else if (obj.IsMloPortal() || obj.IsMloRoom())
            {
                foreach (TargetObjectDef child in obj.Children)
                    ProcessSceneObject(child);
            }
            else if (obj.HasAudioMaterial())
            {
                m_lAudioMaterials.Add(new Definition(obj.MyScene, obj));
            }
            else if (obj.IsContainer())
            {
                foreach (TargetObjectDef child in obj.Children)
                    ProcessSceneObject(child);
            }
            else if (obj.IsPropGroup())
            {
                foreach (TargetObjectDef child in obj.Children)
                    ProcessSceneObject(child);
            }
            else if (obj.IsDummy())
            {
                Log.Log__Warning("Ignoring Dummy {0} as this is how 3dsmax represents Groups.", obj);
            }
            else if (obj.IsEditableSpline() || obj.IsText() || obj.IsLine())
            {
                // Ignore.
            }
            else
            {
                Log.Log__Warning("Ignoring {0} {1} as type not recognised by IDE serialiser.",
                    obj.Name, obj.Class);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void SortLists()
        {
            IComparer<Definition> comparer = new DefinitionIDEComparer();
            m_lObjects.Sort(comparer);
            m_lTimeObjects.Sort(comparer);
            m_lAnimObjects.Sort(comparer);
            m_lTimeAnimObjects.Sort(comparer);
            m_lMilos.Sort(comparer);
            //m_l2dfx.Sort(comparer);
            m_lAudioMaterials.Sort(comparer);
        }
        #endregion // Private Methods
    }

} // RSG.SceneXml.MapExport namespace

// SceneSerialiserMETA.cs
