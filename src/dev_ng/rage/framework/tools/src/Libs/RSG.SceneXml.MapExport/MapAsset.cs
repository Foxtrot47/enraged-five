﻿using System;
using System.Diagnostics;
using RSG.Base.Configuration;

namespace RSG.SceneXml.MapExport
{

    /// <summary>
    /// Helper methods for naming map assets.
    /// </summary>
    public static class MapAsset
    {
        #region Static Controller Methods
        /// <summary>
        /// Return project asset prefix string; defaults to DLC pack name but
        /// can be overridden.
        /// </summary>
        /// <param name="branch"></param>
        /// <returns></returns>
        public static String GetMapProjectPrefix(IProject project)
        {
            Debug.Assert(null != project, "Project reference is null.  Internal error.");
            if (null == project)
                throw (new ArgumentNullException("project", "Project reference is null.  Internal error."));

            String projectPrefix = project.AssetPrefix.MapPrefix;
            return (projectPrefix);
        }

        /// <summary>
        /// Append map prefix to passed in asset name.
        /// </summary>
        /// <param name="project"></param>
        /// <param name="basename"></param>
        /// <returns></returns>
        /// <remarks>Prefere the AppendMapPrefixIfRequired(IProject project, ObjectDef objectDef) to this method.</remarks>
        public static String AppendMapPrefixIfRequired(IProject project, String basename)
        {
            Debug.Assert(null != project, "Project reference is null.  Internal error.");
            if (null == project)
                throw (new ArgumentNullException("project", "Project reference is null.  Internal error."));

            String mapPrefix = GetMapProjectPrefix(project);
            if (project.IsDLC && !basename.StartsWith(mapPrefix, StringComparison.OrdinalIgnoreCase))
               return string.Format("{0}{1}", mapPrefix, basename);
            
            return basename;
        }

        /// <summary>
        /// Append map prefix to passed in asset.
        /// </summary>
        /// <param name="project">Current's IProject.</param>
        /// <param name="targetObjectDef">source object</param>
        /// <returns>A map's prefixed name.</returns>
        public static string AppendMapPrefixIfRequired(IProject project, TargetObjectDef targetObjectDef)
        {
            Debug.Assert(project != null, "Project reference is null.  Internal error.");
            if(project == null)
                throw new ArgumentNullException("project", "Project reference is null.  Internal error.");
            Debug.Assert(targetObjectDef != null, "targetObjectDef reference is null. Internal error.");
            if(targetObjectDef == null)
                throw new ArgumentNullException("targetObjectDef", "objectDef reference is null. Internal error.");

            if (project.IsDLC && targetObjectDef.IsNewDLCObject())
            {
                string mapPrefix = GetMapProjectPrefix(project);
                string name = targetObjectDef.Name;
                
                // We shouldn't be changing the TargetObjectDef name, BUT JUST IN CASE WE HAPPEN TO DO THAT ONE DAY.
                if (name.StartsWith(mapPrefix, StringComparison.OrdinalIgnoreCase))
                {
                    Debug.Fail("Object name already prefixed. This should not happen.",
                        "This object appears to already have a prefix in its name. Either way, we changed it via the code - we should not -, or it has been named like that by art - which is fine.");
                    return name;
                }

                // not prefixed and elligible to prefixing, yay!
                return mapPrefix + name;
            }

            return targetObjectDef.Name;
        }
        #endregion // Static Controller Methods
    }

} // RSG.SceneXml.MapExport namespace
