﻿using System;
using System.Xml.Linq;

using RSG.Base.Math;
using RSG.Metadata.Util;

namespace RSG.SceneXml.MapExport.Occlusion
{
    /// <summary>
    /// This class represents an occlusion mesh
    /// This class includes methods to deserialise from an <XElement,ObjectDef> pair and to serialise to XML
    /// This class also includes properties that allow the split algorithm in Occlusion.Node to function
    /// </summary>
    public class Mesh : Model
    {
        #region Constructor

        public Mesh(BoundingBox3f boundingBox, int dataSize, XElement vertsElement, int numVertsInBytes, int numTris, int flags)
        {
            worldBoundingBox_ = boundingBox;
            numVertsInElement_ = dataSize;
            vertsElement_ = vertsElement;
            numVertsInBytes_ = numVertsInBytes;
            numTris_ = numTris;
            flags_ = flags;
        }

        #endregion Constructor

        #region Public Properties

        public override BoundingBox3f WorldBoundingBox { get { return worldBoundingBox_; } }
        public override int DataSize { get { return fixedMeshDataSize_ + numVertsInElement_; } }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Factory method to construct a Box from an <XElement,ObjectDef> pair
        /// </summary>
        public static Mesh FromXElementAndObjectDef(XElement occlusionMeshElement, TargetObjectDef occlMeshObjectDef)
        {
            // Get xml data
            XElement boundingBoxMinElement = occlusionMeshElement.Element("bmin");
            Vector3f boundingBoxMin = new Vector3f(
                Single.Parse(boundingBoxMinElement.Attribute("x").Value),
                Single.Parse(boundingBoxMinElement.Attribute("y").Value),
                Single.Parse(boundingBoxMinElement.Attribute("z").Value));
            XElement boundingBoxMaxElement = occlusionMeshElement.Element("bmax");
            Vector3f boundingBoxMax = new Vector3f(
                Single.Parse(boundingBoxMaxElement.Attribute("x").Value),
                Single.Parse(boundingBoxMaxElement.Attribute("y").Value),
                Single.Parse(boundingBoxMaxElement.Attribute("z").Value));
            BoundingBox3f boundingBox = new BoundingBox3f(boundingBoxMin, boundingBoxMax);
            int numVertsInElement_ = Int32.Parse(occlusionMeshElement.Element("dataSize").Attribute("value").Value);
            XElement vertsElement = occlusionMeshElement.Element("verts");
            int numVertsInBytes = Int32.Parse(occlusionMeshElement.Element("numVertsInBytes").Attribute("value").Value);
            int numTris = Int32.Parse(occlusionMeshElement.Element("numTris").Attribute("value").Value);
            int xmlFlagsValue = Int32.Parse(occlusionMeshElement.Element("flags").Attribute("value").Value);

            // Get object def data
            bool isWaterOnlyOcclusion =
                occlMeshObjectDef.GetAttribute(AttrNames.OBJ_IS_WATER_ONLY_OCCLUSION, AttrDefaults.OBJ_IS_WATER_ONLY_OCCLUSION);
            int objectDefFlagsValue = 0;
            if (isWaterOnlyOcclusion)
                objectDefFlagsValue |= (int)Flags.OcclusionMeshFlags.OCCLUSION_MESH_WATER_ONLY;

            // Merge flags value
            int flagsValue = xmlFlagsValue | objectDefFlagsValue;

            // Create mesh
            return new Mesh(boundingBox, numVertsInElement_, vertsElement, numVertsInBytes, numTris, flagsValue);
        }

        /// <summary>
        /// Overriden method to serialise a box to xml
        /// </summary>
        public override void AddToXmlDocument(XElement parentElement)
        {
            XElement itemElement = new XElement("Item");
            parentElement.Add(itemElement);
            Xml.CreateElementWithVectorAttributes(itemElement, "bmin", worldBoundingBox_.Min);
            Xml.CreateElementWithVectorAttributes(itemElement, "bmax", worldBoundingBox_.Max);
            Xml.CreateElementWithValueAttribute(itemElement, "dataSize", numVertsInElement_);
            XElement vertsElement = new XElement("verts", vertsElement_.Value);
            XAttribute vertsContentAttribute = new XAttribute("content", "char_array");
            vertsElement.Add(vertsContentAttribute);
            vertsElement.Value = vertsElement_.Value;
            itemElement.Add(vertsElement);
            Xml.CreateElementWithValueAttribute(itemElement, "numVertsInBytes", numVertsInBytes_);
            Xml.CreateElementWithValueAttribute(itemElement, "numTris", numTris_);
            Xml.CreateElementWithValueAttribute(itemElement, "flags", flags_);
        }

        #endregion Public Methods

        #region Private Data

        private readonly static int fixedMeshDataSize_ = 40;

        private BoundingBox3f worldBoundingBox_;
        private int numVertsInElement_;
        private XElement vertsElement_;
        private int numVertsInBytes_;
        private int numTris_;
        private int flags_;

        #endregion Private Data
    }
}
