//
// File: StaticIPL6Container.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of StaticIPL6Container and StreamedIPL6Container classes
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using RSG.Base.Collections;
using RSG.Base.Math;

namespace RSG.SceneXml.MapExport.Collections
{

    /// <summary>
    /// IPL Container for Static IPL Files
    /// </summary>
    public class StaticIPL6Container : IPL6ContainerBase
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// IPL regular instance objects.
        /// </summary>
        public List<Tree<Instance6>> Instances
        {
            get { return m_lInstances; }
        }
        private List<Tree<Instance6>> m_lInstances;

        /// <summary>
        /// IPL MILO instance objects.
        /// </summary>
        public List<Tree<Instance6>> MiloInstances
        {
            get { return m_lMiloInstances; }
        }
        private List<Tree<Instance6>> m_lMiloInstances;

        /// <summary>
        /// IPL Milo Plus objects.
        /// </summary>
        public List<Instance6> MiloPlus
        {
            get { return m_lMiloPlus; }
        }
        private List<Instance6> m_lMiloPlus;
        
        /// <summary>
        /// IPL Cull zone objects.
        /// </summary>
        public List<Instance6> CullZones
        {
            get { return m_lCullZones; }
            private set { m_lCullZones = value; }
        }
        private List<Instance6> m_lCullZones;

        /// <summary>
        /// IPL Path objects.
        /// </summary>
        public List<Instance6> Paths
        {
            get { return m_lPaths; }
            private set { m_lPaths = value; }
        }
        private List<Instance6> m_lPaths;

        /// <summary>
        /// IPL GarageArea objects.
        /// </summary>
        public List<Instance6> GarageAreas
        {
            get { return m_lGarageAreas; }
            private set { m_lGarageAreas = value; }
        }
        private List<Instance6> m_lGarageAreas;

        /// <summary>
        /// IPL 2dfx Objects
        /// </summary>
        public List<Instance6> Twodfx
        {
            get { return m_lTwodfx; }
            private set { m_lTwodfx = value; }
        }
        private List<Instance6> m_lTwodfx;

        /// <summary>
        /// IPL TimeCycle objects.
        /// </summary>
        public List<Instance6> TimeCycles
        {
            get { return m_lTimeCycles; }
            private set { m_lTimeCycles = value; }
        }
        private List<Instance6> m_lTimeCycles;

        /// <summary>
        /// IPL Block objects.
        /// </summary>
        public List<Instance6> Blocks
        {
            get { return m_lBlocks; }
            private set { m_lBlocks = value; }
        }
        private List<Instance6> m_lBlocks;

        /// <summary>
        /// IPL Vehicle Node objects.
        /// </summary>
        public List<Instance6> VehicleNodes
        {
            get { return m_lVehicleNodes; }
            private set { m_lVehicleNodes = value; }
        }
        private List<Instance6> m_lVehicleNodes;

        /// <summary>
        /// IPL Vehicle Link objects.
        /// </summary>
        public List<Instance6> VehicleLinks
        {
            get { return m_lVehicleLinks; }
            private set { m_lVehicleLinks = value; }
        }
        private List<Instance6> m_lVehicleLinks;

        /// <summary>
        /// IPL Patrol Node objects.
        /// </summary>
        public List<Instance6> PatrolNodes
        {
            get { return m_lPatrolNodes; }
            private set { m_lPatrolNodes = value; }
        }
        private List<Instance6> m_lPatrolNodes;

        /// <summary>
        /// IPL Patrol Link objects.
        /// </summary>
        public List<Instance6> PatrolLinks
        {
            get { return m_lPatrolLinks; }
            private set { m_lPatrolLinks = value; }
        }
        private List<Instance6> m_lPatrolLinks;

        /// <summary>
        /// IPL SlowZone objects.
        /// </summary>
        public List<Instance6> SlowZones
        {
            get { return m_lSlowZones; }
            private set { m_lSlowZones = value; }
        }
        private List<Instance6> m_lSlowZones;

        /// <summary>
        /// IPL Container LOD Children.
        /// </summary>
        /// This is filled in as we serialise the instance objects.
        /// 
        public Dictionary<String, int> ContainerLODChildren
        {
            get { return m_ContainerLODChildren; }
            private set { m_ContainerLODChildren = value; }
        }
        private Dictionary<String, int> m_ContainerLODChildren;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constuctor, simply initialises our containers.
        /// </summary>
        public StaticIPL6Container()
        {
            this.m_lCullZones = new List<Instance6>();
            this.m_lPaths = new List<Instance6>();
            this.m_lGarageAreas = new List<Instance6>();
            this.m_lTimeCycles = new List<Instance6>();
            this.m_lTwodfx = new List<Instance6>();
            this.m_lBlocks = new List<Instance6>();
            this.m_lVehicleNodes = new List<Instance6>();
            this.m_lVehicleLinks = new List<Instance6>();
            this.m_lPatrolNodes = new List<Instance6>();
            this.m_lPatrolLinks = new List<Instance6>();
            this.m_lSlowZones = new List<Instance6>();
            this.m_lInstances = new List<Tree<Instance6>>();
            this.m_lMiloInstances = new List<Tree<Instance6>>();
            this.m_lMiloPlus = new List<Instance6>();
            this.m_ContainerLODChildren = new Dictionary<String, int>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Clear all sub-containers.
        /// </summary>
        public override void Clear()
        {
            base.Clear();
            this.Instances.Clear();
            this.CarGens.Clear();
            this.MiloPlus.Clear();
            this.ContainerLODChildren.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public TreeNode<Instance6> FindInstance(TargetObjectDef obj)
        {
            foreach (Tree<Instance6> tree in this.Instances)
            {
                foreach (TreeNode<Instance6> node in tree.DepthFirstSearch)
                {
                    if (node.Value.Object.Equals(obj))
                        return (node);
                }
            }
            return (null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public TreeNode<Instance6> FindMiloInstance(TargetObjectDef obj)
        {
            Debug.Assert(obj.IsMilo(), "Not an interior object.");
            foreach (Tree<Instance6> tree in this.MiloInstances)
            {
                foreach (TreeNode<Instance6> node in tree.DepthFirstSearch)
                {
                    if (node.Value.Object.Equals(obj))
                        return (node);
                }
            }
            return (null);
        }
        #endregion // Controller Methods
    }

    /// <summary>
    /// IPL Container for Streamed IPL Files
    /// </summary>
    public class StreamedIPL6Container : IPL6ContainerBase
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// IPL regular instance objects.
        /// </summary>
        public List<Instance6> Instances
        {
            get { return m_lInstances; }
            protected set { m_lInstances = value; }
        }
        private List<Instance6> m_lInstances;

        /// <summary>
        /// IPL Milo Plus objects.
        /// </summary>
        public List<Instance6> MiloPlus
        {
            get { return m_lMiloPlus; }
            protected set { m_lMiloPlus = value; }
        }
        private List<Instance6> m_lMiloPlus;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor, simply initialises our containers.
        /// </summary>
        public StreamedIPL6Container()
            : base()
        {
            this.Instances = new List<Instance6>();
            this.MiloPlus = new List<Instance6>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Clear all sub-containers.
        /// </summary>
        public override void Clear()
        {
            base.Clear(); 
            this.Instances.Clear();
            this.MiloPlus.Clear();
        }
        #endregion // Controller Methods
    }

    /// <summary>
    /// IPL Group container.
    /// </summary>
    public class GroupIPL6Container
    {
        #region Properties and Associated Member Data        
        /// <summary>
        /// IPL Group LOD and SLOD instances.
        /// </summary>
        public StreamedIPL6Container Static
        {
            get;
            protected set;
        }

        /// <summary>
        /// IPL Group HD instances (default).
        /// </summary>
        public StreamedIPL6Container Streamed
        {
            get;
            protected set;
        }

        /// <summary>
        /// 
        /// </summary>
        public String Name
        {
            get;
            private set;
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor, simply initialises our containers.
        /// </summary>
        public GroupIPL6Container(String name)
            : base()
        {
            this.Name = (name.Clone() as String);
            this.Static = new StreamedIPL6Container();
            this.Streamed = new StreamedIPL6Container();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Clear all sub-containers.
        /// </summary>
        public void Clear()
        {
            this.Static.Clear();
            this.Streamed.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Instance6 Find(TargetObjectDef obj)
        {
            Instance6 find = FindStatic(obj);
            if (null == find)
                find = FindStreamed(obj);
            return (find);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Instance6 FindStatic(TargetObjectDef obj)
        {
            return (Find(obj, this.Static.Instances));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Instance6 FindStreamed(TargetObjectDef obj)
        {
            return (Find(obj, this.Streamed.Instances));
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Find an Instance6 for an ObjectDef. 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private Instance6 Find(TargetObjectDef obj, IEnumerable<Instance6> instances)
        {
            foreach (Instance6 inst in instances)
            {
                if (inst.Object.Equals(obj))
                    return (inst);
            }
            return (null);
        }
        #endregion // Private Methods
    }

    /// <summary>
    /// 
    /// </summary>
    public class IPL6ContainerBase
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// IPL CarGen objects.
        /// </summary>
        public List<Instance6> CarGens
        {
            get;
            protected set;
        }
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constuctor, simply initialises our containers.
        /// </summary>
        public IPL6ContainerBase()
        {
            this.CarGens = new List<Instance6>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Clear all sub-containers.
        /// </summary>
        public virtual void Clear()
        {
            this.CarGens.Clear();
        }
        #endregion // Controller Methods
    }

} // RSG.SceneXml.MapExport.Collections namespace

// StaticIPL6Container.cs
