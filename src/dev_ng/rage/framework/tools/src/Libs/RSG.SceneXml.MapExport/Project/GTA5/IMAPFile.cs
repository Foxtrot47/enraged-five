﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Collections;
using RSG.Base.Math;

namespace RSG.SceneXml.MapExport.Project.GTA5
{
    /// <summary>
    /// Model object for IMAP files.
    /// </summary>
    public class IMAPFile
    {
        #region Properties
        public String Name { get; set; }
        public BoundingBox3f StreamingExtents { get; set; }
        public BoundingBox3f EntityExtents { get; set; }
        public List<String> PhysicsDictionaries { get; set; }
        public uint EntityCount { get; set; }
        public List<Entity> Entities { get; set; }
        public static readonly string Extension = ".imap";
        #endregion

        #region Constructors
        private IMAPFile()
        {
            StreamingExtents = new BoundingBox3f();
            EntityExtents = new BoundingBox3f();
            PhysicsDictionaries = new List<String>();
            Entities = new List<Entity>();
        }
        #endregion Constructors

        #region Public Static Functions
        /// <summary>
        /// Loads basic information about the specified IMAP file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static IMAPFile Load(string filename, SceneCollection sceneCollection)
        {
            try
            {
                XDocument doc = XDocument.Load(filename);
                XElement nameElem = doc.XPathSelectElement("/CMapData/name");
                XElement minEntityElem = doc.XPathSelectElement("/CMapData/entitiesExtentsMin");
                XElement maxEntityElem = doc.XPathSelectElement("/CMapData/entitiesExtentsMax");
                XElement minElem = doc.XPathSelectElement("/CMapData/streamingExtentsMin");
                XElement maxElem = doc.XPathSelectElement("/CMapData/streamingExtentsMax");                
                IEnumerable<XElement> physicsDictionariesElems = doc.XPathSelectElements("/CMapData/physicsDictionaries/Item");

                IMAPFile imap = new IMAPFile();
                imap.Name = nameElem.Value;
                imap.StreamingExtents.Min = new Vector3f(Single.Parse(minElem.Attribute("x").Value),
                    Single.Parse(minElem.Attribute("y").Value), Single.Parse(minElem.Attribute("z").Value));
                imap.StreamingExtents.Max = new Vector3f(Single.Parse(maxElem.Attribute("x").Value),
                    Single.Parse(maxElem.Attribute("y").Value), Single.Parse(maxElem.Attribute("z").Value));

                imap.EntityExtents.Min = new Vector3f(Single.Parse(minEntityElem.Attribute("x").Value),
                    Single.Parse(minEntityElem.Attribute("y").Value), Single.Parse(minEntityElem.Attribute("z").Value));
                imap.EntityExtents.Max = new Vector3f(Single.Parse(maxEntityElem.Attribute("x").Value),
                    Single.Parse(maxEntityElem.Attribute("y").Value), Single.Parse(maxEntityElem.Attribute("z").Value));

                foreach(XElement element in physicsDictionariesElems)
                {
                    imap.PhysicsDictionaries.Add(element.Value);
                }

                IEnumerable<XElement> entityElems = doc.XPathSelectElements("/CMapData/entities/Item");
                imap.EntityCount = (uint)entityElems.Count();

                //TODO:  If we've specified no scene collection, then we don't need to instantiate this information.
                //Ideally we'd be able to load this information quickly enough that we didn't have to
				//prevent this from executing.
                if (sceneCollection != null)
                {
                    foreach (XElement entityElement in entityElems)
                    {
                        Entity newEntity = ParseEntity(entityElement, sceneCollection);
                        imap.Entities.Add(newEntity);
                    }
                }

                return imap;
            }
            catch (Exception e)
            {
                return null;
            }
        }
        #endregion Public Static Functions

        #region Private Functions
        public static Entity ParseEntity(XElement entityElement, SceneCollection sceneCollection)
        {
            XElement archetypeNameElem = entityElement.XPathSelectElement("archetypeName");
            Pair<String, TargetObjectDef> objectDef = sceneCollection.Find(archetypeNameElem.Value);

            XElement positionElem = entityElement.XPathSelectElement("position");
            Vector3f position = new Vector3f(Convert.ToSingle(positionElem.Attribute("x").Value),
                Convert.ToSingle(positionElem.Attribute("y").Value),
                Convert.ToSingle(positionElem.Attribute("z").Value));

            XElement rotationElem = entityElement.XPathSelectElement("rotation");
            Quaternionf rotation = new Quaternionf(Convert.ToSingle(rotationElem.Attribute("x").Value),
                Convert.ToSingle(rotationElem.Attribute("y").Value),
                Convert.ToSingle(rotationElem.Attribute("z").Value),
                Convert.ToSingle(rotationElem.Attribute("w").Value));

            XElement scaleXYElem = entityElement.XPathSelectElement("scaleXY");
            float scaleXY = Convert.ToSingle(scaleXYElem.Attribute("value").Value);

            XElement scaleZElem = entityElement.XPathSelectElement("scaleZ");
            float scaleZ = Convert.ToSingle(scaleZElem.Attribute("value").Value);

            Entity entity = new Entity(objectDef.Second);
            entity.WorldTransformOverride = new Matrix34f(rotation);
            entity.WorldTransformOverride.Translation = position;
            entity.ScaleOverride = new Vector3f(scaleXY, scaleXY, scaleZ);

            XElement parentIndexElem = entityElement.XPathSelectElement("parentIndex");
            entity.LODParentIndex = Convert.ToInt32(parentIndexElem.Attribute("value").Value);

            //TODO:  Add the rest of the variables in the IMAP's entity section, but at the moment
            //the system doesn't have a clean way where we generate entity information partly from the object
            //definition and the entity itself.  But the object definition seems to be shared among all of the
            //objects in the scene.

            return entity;
        }

        #endregion

    } // IMAPFile
}
