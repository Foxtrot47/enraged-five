﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

using RSG.Base.Logging;
using RSG.SceneXml;
using RSG.Base;
using RSG.Platform;

namespace RSG.SceneXml.MapExport
{

    /// <summary>
    /// Serialiser object used to write out per-object IDE files for lights and
    /// the gtaDrawable resource.
    /// </summary>
    public class SceneSerialiserIDEObject
    {
        public SceneSerialiserIDEObject(Scene scene)
        {
            scene_ = scene;
        }

        public bool WriteLightObjects(TargetObjectDef[] objs, string baseDirectory)
        {
            foreach (TargetObjectDef objectDef in objs)
            {
                if (objectDef.IsContainer() || objectDef.IsMilo() || objectDef.IsMloRoom())
                {
                    WriteLightObjects(objectDef.Children, baseDirectory);
                    continue;
                }

                if (objectDef.DontExport() || objectDef.DontExportIDE())
                    continue;

                // GD: Drawable LOD states don't ever need to go into the IDE.
                if (objectDef.HasDrawableLODChildren() || objectDef.HasRenderSimParent())
                    continue;

                if (!objectDef.IsObject() || objectDef.IsRefObject() || objectDef.IsInternalRef())
                    continue;

                List<Definition> lightDefinitions = new List<Definition>();
                if (null != objectDef.SkeletonRoot)
                    GetLightDefinitionsRecursive(objectDef.SkeletonRoot, lightDefinitions);
                GetLightDefinitionsRecursive(objectDef, lightDefinitions);

                if (lightDefinitions.Count == 0)
                    continue;

                if (SceneType.EnvironmentProps == scene_.SceneType)
                {
                    // Only needed on props as other lights won't get instanced.

                    Dictionary<byte, List<TargetObjectDef>> takenHashes = new Dictionary<byte, List<TargetObjectDef>>();
                    foreach (Definition lightDefinition in lightDefinitions)
                    {
                        byte lightInstanceHash = lightDefinition.Object.AttributeGuid.ToByteArray()[0];
                        if (takenHashes.ContainsKey(lightInstanceHash))
                        {
                            String setObjects = "";
                            foreach (TargetObjectDef d in takenHashes[lightInstanceHash])
                            {
                                if (d.Parent == lightDefinition.Object.Parent) 
                                    setObjects += (d.Name + ",");
                            }
                            if (setObjects.Length>0)
                                Log.Log__ErrorCtx(lightDefinition.Object.Name, "Light {0} has an instance hash which is already existent on object {1}. https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#Duplicate_Light_hash", lightDefinition.Object.Name, setObjects);
                        }
                        else
                        {
                            takenHashes.Add(lightInstanceHash, new List<TargetObjectDef>());
                        }
                        takenHashes[lightInstanceHash].Add(lightDefinition.Object);
                    }
                }

                // Write out an .ide for each platform because some lights might be platform specific.
                foreach (var vPlatform in Enum.GetValues(typeof(RSG.Platform.Platform)))
                {
                    string rageBuilderPlatform = RSG.Platform.PlatformUtils.PlatformToRagebuilderPlatform((RSG.Platform.Platform)vPlatform);
                    string idePathname = "";

                    // Check to see if there are any disabled lights meaning we want to create platform specific .ide files.
                    if ((RSG.Platform.Platform)vPlatform == Platform.Platform.Independent ||
                        objectDef.HasPlatformSpecificAttributes((RSG.Platform.Platform)vPlatform) ||
                        CheckForPlatformLights((RSG.Platform.Platform)vPlatform, lightDefinitions))
                    {
                        if ((RSG.Platform.Platform)vPlatform == Platform.Platform.Independent)
                            idePathname = Path.Combine(baseDirectory, objectDef.Name, objectDef.Name + ".ide");
                        else
                            idePathname = Path.Combine(baseDirectory, objectDef.Name, objectDef.Name + "." + rageBuilderPlatform + ".ide");

                        string ideDirectory = Path.GetDirectoryName(idePathname);
                        if (!Directory.Exists(ideDirectory))
                            Directory.CreateDirectory(ideDirectory);

                        // Write IDE file
                        Log.Log__Message("Writing {0}...", idePathname);
                        using (StreamWriter fpFile = File.CreateText(idePathname))
                        {
                            fpFile.WriteLine("# IDE generated from SceneXml file {0}, 3dsmax file {1}",
                                Path.GetFileName(scene_.Filename), Path.GetFileName(scene_.ExportFilename));
                            fpFile.WriteLine(TAG_2DFX);

                            foreach (Definition lightDefinition in lightDefinitions)
                            {
                                if (lightDefinition.Object.IsActiveForPlatform((RSG.Platform.Platform)vPlatform))
                                {
                                    fpFile.WriteLine(lightDefinition.ToIDE2dfx());
                                }
                                else
                                {
                                    fpFile.WriteLine("#Not available for this platform - " + lightDefinition.ToIDE2dfx());
                                }
                            }

                            fpFile.WriteLine(TAG_END);
                            fpFile.Close();
                        }
                    }
                }
            }// end foreach (ObjectDef objectDef in scene_.Walk(Scene.WalkMode.DepthFirst, DeadEndFunc))
            
            return true;
        }

        public bool Write(string baseDirectory)
        {
            Log.Log__Message("Starting object IDE serialisation:");
            Log.Log__Message("  Scene:       {0}", scene_.Filename);
            Log.Log__Message("  Destination: {0}", baseDirectory);

            // Process the objects

            return WriteLightObjects(scene_.Objects, baseDirectory);
        }

        private static bool DeadEndFunc(ObjectDef obj)
        {
            if (obj.IsMAXGroup())
                return true;
            return false;
        }

        private static void GetLightDefinitionsRecursive(TargetObjectDef objectDef, ICollection<Definition> definitions)
        {
            if (objectDef.Is2dfxLightEffect() && objectDef.HasParent())
            {
                definitions.Add(new Definition(objectDef.MyScene, objectDef));
            }

            foreach (TargetObjectDef childObjectDef in objectDef.Children)
            {
                GetLightDefinitionsRecursive(childObjectDef, definitions);
            }
        }

        private static bool CheckForPlatformLights(RSG.Platform.Platform platform, ICollection<Definition> definitions)
        {
            bool platformLights = false;

            foreach (Definition lightDef in definitions)
            {
                if (lightDef.Object.HasPlatformSpecificAttributes(platform))
                {
                    platformLights = true;
                }
            }

            return platformLights;
        }

        private readonly String TAG_2DFX = "2dfx";
        private readonly String TAG_END = "end";

        private Scene scene_;
    }
}
