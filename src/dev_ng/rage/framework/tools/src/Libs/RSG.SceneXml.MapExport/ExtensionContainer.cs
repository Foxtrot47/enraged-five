﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace RSG.SceneXml.MapExport
{

    /// <summary>
    /// Extension container for any class that needs it.
    /// </summary>
    public class ExtensionContainer
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Object's extensions.
        /// </summary>
        public Extension[] Extensions
        {
            get { return (m_Extensions.ToArray()); }
        }
        protected List<Extension> m_Extensions;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ExtensionContainer()
        {
            this.m_Extensions = new List<Extension>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Add an extension to this container.
        /// </summary>
        /// <param name="extension"></param>
        public void AddExtension(Extension extension)
        {
            Debug.Assert(!m_Extensions.Contains(extension),
                "Internal error: extension already in ExtensionContainer.",
                "Extension {0} is already in the ExtensionContainer.", extension.Object);
            if (m_Extensions.Contains(extension))
                return;

            this.m_Extensions.Add(extension);
            this.m_Extensions.Sort();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="extensions"></param>
        public void AddExtensionRange(IEnumerable<Extension> extensions)
        {
            foreach (Extension ext in extensions)
                this.AddExtension(ext);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ext"></param>
        /// <returns></returns>
        public bool ContainsExtention(Extension ext)
        {
            return (this.m_Extensions.Contains(ext));
        }
        #endregion // Controller Methods
    }

} // RSG.SceneXml.MapExport namespace
