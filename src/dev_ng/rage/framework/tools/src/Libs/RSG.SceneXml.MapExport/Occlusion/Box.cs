﻿using System;
using System.Xml.Linq;

using RSG.Base.Math;
using RSG.Metadata.Util;

namespace RSG.SceneXml.MapExport.Occlusion
{
    /// <summary>
    /// This class represents an occlusion box
    /// This class includes methods to deserialise from an ObjectDef and to serialise to XML
    /// This class also includes properties that allow the split algorithm in Occlusion.Node to function
    /// </summary>
    public class Box : Model
    {
        #region Constructor

        public Box(String name, float x, float y, float z, float length, float width, float height, float rotationInZ)
        {
            name_ = name;
            centre_ = new Vector3f(x, y, z);
            length_ = length;
            width_ = width;
            height_ = height;
            rotationInZ_ = rotationInZ;

            // Calculate bounding box
            BoundingBox3f localBoundingBox = new BoundingBox3f(
                new Vector3f((length * -0.5f), (width * -0.5f), (height * -0.5f)),
                new Vector3f((length * 0.5f), (width * 0.5f), (height * 0.5f)));
            Matrix34f localToWorldRotationTransform = Matrix34f.BuildZRotationMatrix(rotationInZ);
            localToWorldRotationTransform.Translation = centre_;
            worldBoundingBox_ = localToWorldRotationTransform * localBoundingBox;
        }

        #endregion Constructor

        #region Public Properties

        public override BoundingBox3f WorldBoundingBox { get { return worldBoundingBox_; } }
        public override int DataSize { get { return boxDataSize_; } }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Factory method to construct a Box from an ObjectDef
        /// </summary>
        public static Box FromObjectDef(TargetObjectDef objectDef)
        {
            float length = objectDef.GetParameter(ParamNames.OCCLUSIONBOX_LENGTH, ParamDefaults.OCCLUSIONBOX_LENGTH);
            float width = objectDef.GetParameter(ParamNames.OCCLUSIONBOX_WIDTH, ParamDefaults.OCCLUSIONBOX_WIDTH);
            float height = objectDef.GetParameter(ParamNames.OCCLUSIONBOX_HEIGHT, ParamDefaults.OCCLUSIONBOX_HEIGHT);
            float rotationInZ = objectDef.NodeTransform.ToEulers().Z;

            return new Box(
                objectDef.Name,
                objectDef.NodeTransform.Translation.X,
                objectDef.NodeTransform.Translation.Y,
                objectDef.NodeTransform.Translation.Z,
                length,
                width,
                height,
                rotationInZ);
        }

        /// <summary>
        /// Overriden method to serialise a box to xml
        /// </summary>
        public override void AddToXmlDocument(XElement parentElement)
        {
            Int16 centreX = Convert.ToInt16(Math.Round(centre_.X * 4.0f));
            Int16 centreY = Convert.ToInt16(Math.Round(centre_.Y * 4.0f));
            Int16 centreZ = Convert.ToInt16(Math.Round(centre_.Z * 4.0f));
            Int16 length = Convert.ToInt16(Math.Round(length_ * 4.0f));
            Int16 width = Convert.ToInt16(Math.Round(width_ * 4.0f));
            Int16 height = Convert.ToInt16(Math.Round(height_ * 4.0f));
            Int16 cosZ = Convert.ToInt16(Math.Round((System.Math.Cos(rotationInZ_) * 16384.0f)));
            Int16 sinZ = Convert.ToInt16(Math.Round((System.Math.Sin(rotationInZ_) * 16384.0f)));

            XElement itemElement = new XElement("Item");
            parentElement.Add(itemElement);
            Xml.CreateComment(itemElement, String.Format("RsOcclusionBox: {0}", name_));
            Xml.CreateElementWithValueAttribute(itemElement, "iCenterX", centreX);
            Xml.CreateElementWithValueAttribute(itemElement, "iCenterY", centreY);
            Xml.CreateElementWithValueAttribute(itemElement, "iCenterZ", centreZ);
            Xml.CreateElementWithValueAttribute(itemElement, "iLength", length);
            Xml.CreateElementWithValueAttribute(itemElement, "iWidth", width);
            Xml.CreateElementWithValueAttribute(itemElement, "iHeight", height);
            Xml.CreateElementWithValueAttribute(itemElement, "iCosZ", cosZ);
            Xml.CreateElementWithValueAttribute(itemElement, "iSinZ", sinZ);
        }

        #endregion Public Methods

        #region Private Data

        private readonly static int boxDataSize_ = 16;// 8 int16 members

        private String name_;
        private Vector3f centre_;
        private float length_;
        private float width_;
        private float height_;
        private float rotationInZ_;
        private BoundingBox3f worldBoundingBox_;

        #endregion Private Data
    }
}
