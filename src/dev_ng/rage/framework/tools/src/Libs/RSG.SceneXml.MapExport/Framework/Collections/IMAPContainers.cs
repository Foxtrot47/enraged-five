﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.SceneXml.MapExport.Util;

namespace RSG.SceneXml.MapExport.Framework.Collections
{
    
    /// <summary>
    /// IMAP Container class; used by the IMAP serialiser(s).
    /// </summary>
    public class IMAPContainer
    {
        #region Constants
        /// <summary>
        /// Streaming extents error URL.
        /// </summary>
        private readonly String URL_IMAP_STREAMING_EXT = 
            "https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#IMAP_Streaming_Extents";
        #endregion // Constants

        #region Options
        /// <summary>
        /// This option is to disable entity duplication check, where 
        /// the generation of certain IMAPs could be massive (e.g. grass instances with more than 100,000)
        /// and the duplication check is both unnecessary and scales poorly.
        /// </summary>
        public bool DisableEntityDuplicationCheck
        {
            get;
            set;
        }
        #endregion

        #region Properties and Associated Member Data
        /// <summary>
        /// Container name (can only be set once).
        /// </summary>
        public String Name
        {
            get { return m_sName; }
            set
            {
                if (String.IsNullOrEmpty(m_sName))
                    m_sName = value;
                else
                    throw new NotSupportedException();
            }
        }
        protected String m_sName;

        /// <summary>
        /// IMAP container flags.
        /// </summary>
        public IMAP_eFlags Flags
        {
            get;
            set;
        }

        /// <summary>
        /// IMAP container Content Flags.
        /// </summary>
        public IMAP_eContentFlags ContentFlags
        {
            get;
            protected set;
        }

        /// <summary>
        /// Associated ITYP container.
        /// </summary>
        public ITYPContainer ITYPContainer
        {
            get;
            set;
        }

        /// <summary>
        /// Parent IMAP container (if applicable).
        /// </summary>
        public IMAPContainer Parent
        {
            get { return m_parent; }
            internal set
            {
                m_parent = value;
                if (this != m_parent)
                    Parent.Flags |= IMAP_eFlags.F_IS_PARENT;
            }
        }
        protected IMAPContainer m_parent;

        /// <summary>
        /// ITYP streaming dependencies for this IMAP container.
        /// </summary>
        public String[] Dependencies
        {
            get { return (m_Dependencies.ToArray()); }
        }
        protected List<String> m_Dependencies;

        /// <summary>
        /// Physics dictionary dependencies.
        /// </summary>
        public String[] PhysicsDictionaries
        {
            get { return (m_PhysicsDictionaries.ToArray()); }
        }
        protected List<String> m_PhysicsDictionaries;

        /// <summary>
        /// Entities within this container.
        /// </summary>
        public List<Entity> Entities
        {
            get;
            protected set;
        }

        /// <summary>
        /// Streaming bounding box.
        /// </summary>
        public BoundingBox3f StreamingBoundingBox
        {
            get;
            set;
        }

        /// <summary>
        /// Entity bounding box.
        /// </summary>
        public BoundingBox3f EntityBoundingBox
        {
            get;
            set;
        }

        /// <summary>
        /// Parent Imap Override for the case where we need to 
        /// override the LOD Parent of a container.
        /// E.G. IMAP Group lod > Slod linkage serialisation.
        /// </summary>
        public string ParentImapOverride
        {
            get;
            set;
        }

        /// <summary>
        /// Occlusion node.
        /// </summary>
        public Occlusion.Node OcclusionNode 
        {
            get
            {
                return occlusionNode_;
            }
            set
            {
                occlusionNode_ = value;
                if (occlusionNode_ != null)
                    this.ContentFlags |= IMAP_eContentFlags.CF_OCCLUDER;
                else
                    this.ContentFlags &= ~IMAP_eContentFlags.CF_OCCLUDER;
            }
        }

        private Occlusion.Node occlusionNode_;
        #endregion // Properties and Associated Member Data
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public IMAPContainer()
        {
            Reset();
        }

        /// <summary>
        /// Constructor (with name).
        /// </summary>
        public IMAPContainer(String name)
        {
            this.Name = name;
            Reset();
        }

        /// <summary>
        /// Constructor (with name and parent).
        /// </summary>
        /// <param name="name"></param>
        /// <param name="parent"></param>
        public IMAPContainer(String name, IMAPContainer parent)
        {
            this.Name = name;
            this.Parent = parent;
            Reset();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Reset internal data.
        /// </summary>
        public virtual void Reset()
        {
            DisableEntityDuplicationCheck = false;

            this.m_Dependencies = new List<String>();
            this.m_PhysicsDictionaries = new List<String>();
            this.Entities = new List<Entity>();
            this.StreamingBoundingBox = new BoundingBox3f();
            this.EntityBoundingBox = new BoundingBox3f();
            occlusionNode_ = null;
        }

        #region Dependency Collection Methods
        /// <summary>
        /// Add IMD Container dependency.
        /// </summary>
        /// <param name="name"></param>
        public void AddDependency(IMAPContainer container)
        {
            this.AddDependency(container.Name);
        }

        /// <summary>
        /// Add named container dependency (e.g. for a props file we aren't serialising).
        /// </summary>
        /// <param name="name"></param>
        public void AddDependency(String name)
        {
            // Following removed as ITYP files match the static IMAP filename.
#if false
            Debug.Assert(name != this.Name,
                "Internal error: cannot add self as a dependency.",
                "IMD Container {0} being added as own dependency.", this.Name);
            if (name == this.Name)
                return;
#endif

            Debug.Assert(!String.IsNullOrWhiteSpace(name),
                "Internal error: cannot add a null or whitespace container dependency.",
                "IMD container {0} cannot accept an empty IMAP container dependency.",
                this.Name);
            // DHM Lets not warn about dupe dependencies because it will complicate
            //     calling code.
            //Debug.Assert(!m_Dependencies.Contains(container.Name),
            //    "Internal error: dependency list already this IMD Container.",
            //    "IMD container {0} is already marked as a dependency.", container.Name);
            if (m_Dependencies.Contains(name))
                return;
            this.m_Dependencies.Add(name);
        }

        /// <summary>
        /// Add a set of containers as dependencies.
        /// </summary>
        /// <param name="container"></param>
        public void AddDependencyRange(IEnumerable<IMAPContainer> containers)
        {
            foreach (IMAPContainer dependency in containers)
                AddDependency(dependency);
        }

        /// <summary>
        /// Add a set of containers as dependencies.
        /// </summary>
        /// <param name="containers"></param>
        public void AddDependencyRange(IEnumerable<String> containers)
        {
            foreach (String name in containers)
                AddDependency(name);
        }

        /// <summary>
        /// Replace a dependency name (sourceName) with another (targetName).
        /// </summary>
        /// <param name="sourceName">Source dependency to replace.</param>
        /// <param name="targetName">New dependency.</param>
        public void ReplaceDependency(String sourceName, String targetName)
        {
            if (m_Dependencies.Contains(sourceName))
            {
                m_Dependencies.Remove(sourceName);
                m_Dependencies.Add(targetName);
            }
        }

        /// <summary>
        /// Replace a dependency name (sourceName) with a set (targetNames).
        /// </summary>
        /// <param name="sourceName">Source dependency to replace.</param>
        /// <param name="targetNames">New dependencies.</param>
        public void ReplaceDependency(String sourceName, IEnumerable<String> targetNames)
        {
            if (m_Dependencies.Contains(sourceName))
            {
                m_Dependencies.Remove(sourceName);
                m_Dependencies.AddRange(targetNames);
            }
        }
        #endregion // Dependency Collection Methods

        #region Physics Dictionary Dependency Methods
        /// <summary>
        /// Add physics dictionary dependency.
        /// </summary>
        /// <param name="dictionary"></param>
        /// <param name="obj"></param>
        public void AddPhysicsDictionary(String dictionary, TargetObjectDef obj = null)
        {
            Debug.Assert(!String.IsNullOrWhiteSpace(dictionary),
                "Internal error: attempting to add a null or empty physics dictionary dependency.",
                "IMAP Container {0} cannot accept an empty physics dictionary for object '{1}'.",
                this.Name, (null == obj) ? "None Given" : obj.Name);
            if (String.IsNullOrWhiteSpace(dictionary))
            {
                Log.Log__Error("Internal error: attempting to add a null or empty physics dictionary dependency.  IMAP Container {0} cannot accept an empty physics dictionary for object '{1}'.",
                    this.Name, (null == obj) ? "None Given" : obj.Name);
                return;
            }
            if (m_PhysicsDictionaries.Contains(dictionary))
                return;
            this.ContentFlags |= IMAP_eContentFlags.CF_ENTITIES_USING_PHYSICS;
            this.m_PhysicsDictionaries.Add(dictionary);
        }
        
        /// <summary>
        /// Add a set of physics dictionary dependencies.
        /// </summary>
        /// <param name="containers"></param>
        public void AddPhysicsDictionaryRange(IEnumerable<String> dictionaries)
        {
            foreach (String dictionary in dictionaries)
                AddPhysicsDictionary(dictionary);
        }

        /// <summary>
        /// Add a set of physics dictionary dependencies for all entities.
        /// </summary>
        public void AddPhysicsDictionariesForEntities()
        {
            foreach (Entity entity in this.Entities)
            {
                if (!(entity.Object.IsRefObject() || entity.Object.IsXRef()))
                    continue; // Skip non-external referenced objects.

                if (null == entity.Object.RefObject)
                {
                    Log.Log__WarningCtx(entity.Object.Name,
                        "Entity {0} source object {1} not found to check physics dictionary dependency.",
                        entity.Object.Name, entity.Object.RefName);
                    continue;
                }
                else if (!entity.Object.RefObject.IsAnimProxy())
                {
                    // Interiors are a special case
                    if (entity.Object.IsMiloTri())
                    {
                        string interiorDictionaryName = entity.Object.GetObjectName().ToLower();
                        AddPhysicsDictionary(interiorDictionaryName, entity.Object);
                        continue;
                    }

                    // GTA5 Bug #272098; collision group specified doesnt mean
                    // the referenced object has collision so we don't add the
                    // dependency.
                    if (entity.Object.WillHaveCollisionBaked() ||
                        entity.Object.RefObject.IsFragment() || !entity.Object.RefObject.HasCollision())
                    {
                        continue;
                    }

                    // We have to handle the Stated Anim proxies separately 
                    // because of the retarded naming convention.
                    if (null == entity.Object.RefObject)
                    {
                        Log.Log__ErrorCtx(entity.Object.Name,
                            "Entity {0} definition {1} not found to check physics dictionary dependency.",
                            entity.Object.RefName, entity.Object.Name);
                    }
                    else
                    {
                        String collisionGroup = entity.Object.RefObject.GetAttribute(AttrNames.OBJ_COLLISION_GROUP, AttrDefaults.OBJ_COLLISION_GROUP);
                        if (!String.IsNullOrWhiteSpace(collisionGroup) && 
                            0 != String.Compare(collisionGroup, AttrDefaults.OBJ_COLLISION_GROUP, true))
                        {
                            AddPhysicsDictionary(collisionGroup.ToLower(), entity.Object);
                        }
                        else
                        {
                            // We default to the "collision group" called the same as the reffile;
                            // remembering that the RefFile property was fixed up
                            // in the SceneCollection load.
                            String filename = entity.Object.RefFile;
                            String refFile = System.IO.Path.GetFileNameWithoutExtension(filename);
                            if (!String.IsNullOrWhiteSpace(refFile))
                                AddPhysicsDictionary(refFile, entity.Object);
                            else
                                Log.Log__WarningCtx(entity.Object.Name,
                                    "Entity {0} definition {1} has an empty Collision Group.",
                                    entity.Object.RefName, entity.Object.Name);
                        }
                    }
                }
                else if (entity.Object.RefObject.IsAnimProxy())
                {
                    // We have to handle the Stated Anim proxies separately 
                    // because of the retarded naming convention.
                    // Currently fudge the collision group to just the default
                    // as we're not currently using collision groups for
                    // stated anims.
                    // Additional check added to take into account the StatedAnim
                    // may not have any collision data.
                    bool hasCollision = false;
                    foreach (TargetObjectDef o in entity.Object.RefObject.MyScene.Walk(Scene.WalkMode.DepthFirst))
                    {
                        if (o.DontExport() || o.DontExportIDE())
                            continue;

                        if (o.IsCollision() && o.HasParent())
                        {
                            if (o.Parent.DontExport() || o.Parent.DontExportIDE())
                                continue;

                            hasCollision = true;
                            break;
                        }
                    }

                    if (hasCollision)
                    {
                        String filename = entity.Object.RefFile;
                        String refFile = System.IO.Path.GetFileNameWithoutExtension(filename);
                        AddPhysicsDictionary(refFile);
                    }
                }
            }
        }
        #endregion // Physics Dictionary Dependency Methods

        #region Entity Collection Methods
        /// <summary>
        /// Add a single Entity to this container.
        /// </summary>
        /// <param name="entity"></param>
        public virtual void AddEntity(Entity entity)
        {
#warning DHM FIX ME: wtf?  What is adding duplicate entities?
            if (DisableEntityDuplicationCheck == false)
            {
                Debug.Assert(!this.Entities.Contains(entity),
                "Internal error: entity already in IMAP Container.",
                "Entity {0} is already in the IMAP Container.", entity.Object.Name);
                if (this.Entities.Contains(entity))
                    return;
            }

            // LOD Level Content Flags
            fwEntityDef_eLodTypes lodLevel = LodLevel.Get(entity);
            switch (lodLevel)
            {
                case fwEntityDef_eLodTypes.LODTYPES_DEPTH_HD:
                case fwEntityDef_eLodTypes.LODTYPES_DEPTH_ORPHANHD:
                    this.ContentFlags |= IMAP_eContentFlags.CF_HIGHDETAIL;
                    break;
                case fwEntityDef_eLodTypes.LODTYPES_DEPTH_LOD:
                case fwEntityDef_eLodTypes.LODTYPES_DEPTH_SLOD1:
                    this.ContentFlags |= IMAP_eContentFlags.CF_LOD;
                    break;
                case fwEntityDef_eLodTypes.LODTYPES_DEPTH_SLOD2:
                case fwEntityDef_eLodTypes.LODTYPES_DEPTH_SLOD3:
                case fwEntityDef_eLodTypes.LODTYPES_DEPTH_SLOD4:
                    this.ContentFlags |= IMAP_eContentFlags.CF_CONTAINERLOD;
#warning DHM FIX ME: hack to ensure its set; should be able to go eventually.
                    this.Flags |= IMAP_eFlags.F_IS_PARENT;
                    break;
            }
            // Interior Content Flag
            if (entity is InteriorEntity)
                this.ContentFlags |= IMAP_eContentFlags.CF_MLO;

            if(entity.IsCritical)
                this.ContentFlags |= IMAP_eContentFlags.CF_ENTITIES_CRITICAL;
            

            this.Entities.Add(entity);

            // Extents Bounding Box
            // B* 762075 - if entity.Object.RefObject.LocalBoundingBox == null we end up with a bounding box of infinite size.
            if (entity.Object.IsRefObject() && (entity.Object.RefObject != null) && (entity.Object.RefObject.LocalBoundingBox != null))
            {
                // B* 543442 - for refs we need to look up the source object and determine the entity extents.
                // Previously we were relying on entity.WorldBoundingBox, which assumes the interior instance was correctly rendered by the RS ref system!
                TargetObjectDef refObjectDef = entity.Object.RefObject;
                BoundingBox3f localBoundingBox = refObjectDef.GetLocalBoundingBox();
                Matrix34f refObjectToWorldTransform = entity.Object.NodeTransform.Transpose();// Hack to fix scene XML bug
                BoundingBox3f worldBoundingBox = refObjectToWorldTransform * localBoundingBox;
                this.EntityBoundingBox.Expand(worldBoundingBox);
            }
            else if (entity.IsInstancedObject)
            {
                BoundingBox3f localBoundingBox = entity.Object.GetLocalBoundingBox();
                Matrix34f objectToWorldTransform = entity.WorldTransformOverride.Transpose();// Hack to fix scene XML bug
                BoundingBox3f worldBoundingBox = objectToWorldTransform * localBoundingBox;
                this.EntityBoundingBox.Expand(worldBoundingBox);
            }
            else
            {
                this.EntityBoundingBox.Expand(entity.WorldBoundingBox);
            }

            // Streaming Bounding Box
            this.StreamingBoundingBox.Expand(entity.GetStreamingExtents());
        
            // ITYP Dependencies.
            if (entity.Object.IsRefObject() && !String.IsNullOrEmpty(entity.Object.RefFile))
            {
                // External reference.
                String ityp_name = System.IO.Path.GetFileNameWithoutExtension(entity.Object.RefFile);
                if (Entity.Branch.Project.IsDLC)
                {
                    // Ensure its resolved before accessing RefObject.
                    if (null != entity.Object && null != entity.Object.RefObject)
                    {
                        if (entity.Object.RefObject.IsObject() && entity.Object.RefObject.IsNewDLCObject())
                        {
                            ityp_name = MapAsset.AppendMapPrefixIfRequired(Entity.Branch.Project, ityp_name);
                        }
                        else if ((entity.Object.RefObject.IsMilo() || entity.Object.RefObject.IsMiloTri()) &&
                            entity.Object.IsNewDLCObject())
                        {
                            ityp_name = MapAsset.AppendMapPrefixIfRequired(Entity.Branch.Project, ityp_name);
                        }
                    }
                    else
                    {
                        // Otherwise we're going to have to error.  The RsRef has failed to resolve.
                        Log.Log__WarningCtx(entity.Object.Name, 
                            "[art] External reference object '{0}' ('{1}') does not resolve (referenced from '{2}').",
                            entity.Object.Name, entity.Object.RefName, System.IO.Path.GetFileNameWithoutExtension(entity.Object.RefFile));
                    }
                }
                this.AddDependency(ityp_name);
            }
        }

        /// <summary>
        /// Add a set of Entity objects to this container.
        /// </summary>
        /// <param name="entities"></param>
        public virtual void AddEntityRange(IEnumerable<Entity> entities)
        {
            foreach (Entity entity in entities)
                this.AddEntity(entity);
        }

        /// <summary>
        /// Determine whether an Entity is already in this container.
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual bool ContainsEntity(Entity entity)
        {
            return (this.Entities.Contains(entity));
        }

        /// <summary>
        /// Find an ObjectDef; returning its Entity.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public Entity FindEntity(TargetObjectDef obj)
        {
            foreach (Entity inst in this.Entities)
            {
                if (inst.Object.Equals(obj))
                    return (inst);
            }
            return (null);
        }
        #endregion // Entity Collection Methods

        #region Test Methods
        /// <summary>
        /// Check the consistency of the streaming and entity extents; 
        /// ensuring that the streaming extents encompass the entity
        /// extents.
        /// </summary>
        public void CheckExtentsConsistency()
        {
            if (0 == this.Entities.Count)
                return;

            if (!this.StreamingBoundingBox.Contains(this.EntityBoundingBox.Min) ||
                !this.StreamingBoundingBox.Contains(this.EntityBoundingBox.Max))
            {
#warning DHM FIX ME: this should be an error!
                Log.Log__WarningCtx(this.Name, "[art] IMAP container {0} ({1} entities) has streaming extents that do not contain entity extents; do you have object pivot or LOD distance warnings?  See {2} for more information.",
                    this.Name, this.Entities.Count, URL_IMAP_STREAMING_EXT);

                // Try to locate the problem entity or entities.
                foreach (Entity entity in this.Entities)
                {
                    // Look for odd object/node transforms; indicating that a 
                    // pivot could be out.
                    if (!AlmostEqual(entity.Object.NodeTransform.Translation, entity.Object.ObjectTransform.Translation))
                        Log.Log__WarningCtx(entity.Object.Name, "Entity {0} [{1}] has a pivot offset which could cause streaming extents issues.", 
                            entity.Object.Name, entity.Object.LODLevel);
                
                    // Calculate the streaming box just for this entity; and
                    // compare that to the entity box; it should include it otherwise
                    // the LOD distance isn't large enough.
                    BoundingBox3f streamingBox = entity.GetStreamingExtents();
                    float lodDistance = entity.LODDistance;
                    // Handle InternalRefs without affecting their exported LOD Distance.
                    if (entity.Object.IsInternalRef() || entity.Object.IsRefInternalObject())
                    {
                        if (null != entity.Object.InternalRef)
                        {
                            lodDistance = SceneOverrideIntegrator.Instance.GetObjectLODDistance(entity.Object.InternalRef);
                        }
                        else
                        {
                            lodDistance = -1.0f;
                        }
                    }

                    bool contained = (streamingBox.Contains(entity.WorldBoundingBox.Min) &&
                          streamingBox.Contains(entity.WorldBoundingBox.Max));
                    if (!contained)
                    {
                        if (entity.Object.HasLODParent() && entity.Parent != null)
                        {
                            Log.Log__WarningCtx(
                                entity.Object.Name,
                                "[art] The LOD distance ({0:F2}m) for '{1}' [{2}] is not large enough; some or all of this entity's geometry exists outside its streaming extents.  " +
                                "This entity has a LOD parent ('{3}') so the game will use the parent's pivot as the centre of this entity's streaming extents.",
                                lodDistance, entity.Object.Name, entity.Object.LODLevel, entity.Parent.Object.Name);

                            double distanceLodParent = (entity.Object.NodeTransform.Translation - entity.Parent.Object.NodeTransform.Translation).Magnitude();
                            Log.Log__WarningCtx(entity.Object.Name, "Entity {0} [{1}] streaming bound does not enclose its extents because its LOD distance is not large enough.  Entity LOD distance: {2:F2}m and distance to entity LOD parent: {3:F2}m.",
                                entity.Object.Name, entity.Object.LODLevel, lodDistance, distanceLodParent);
                        }
                        else
                        {
                            double maxSize = ((entity.WorldBoundingBox.Max - entity.WorldBoundingBox.Min) / 2.0f).Magnitude();
                            Log.Log__WarningCtx(entity.Object.Name, "Entity {0} [{1}] streaming bound does not enclose its extents because its LOD distance is not large enough.  Entity LOD distance: {2:F2}m and entity size: {3:F2}m.",
                                entity.Object.Name, entity.Object.LODLevel, lodDistance, maxSize);
                        }
                    }
                }
            }
        }

        #endregion // Test Methods
        #endregion // Controller Methods

        #region Private Utility Methods
        /// <summary>
        /// Determine if two Vector3f's are equal.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        private bool AlmostEqual(Vector3f a, Vector3f b, float epsilon = 0.001f)
        {
            return (AlmostEqual(a.X, b.X, epsilon) &&
                AlmostEqual(a.Y, b.Y, epsilon) &&
                AlmostEqual(a.Z, b.Z, epsilon));
        }

        /// <summary>
        /// Determine if two float's are equal.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="epsilon"></param>
        /// <returns></returns>
        private bool AlmostEqual(float a, float b, float epsilon = 0.001f)
        {
            return (Math.Abs(b - a) < epsilon);
        }
        #endregion // Private Utility Methods
    }

} // RSG.SceneXml.MapExport.Framework.Collections namespace
