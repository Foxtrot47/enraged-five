﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using RSG.Base.Configuration;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.Base.Security.Cryptography;

namespace RSG.SceneXml.MapExport.Framework
{

    /// <summary>
    /// Cross-container LOD Management class; SLOD1 <-> SLOD2 linkage.
    /// </summary>
#warning DHM FIX ME: remove singleton pattern like a ninja.
    public class InterContainerLODManager
    {
        #region Properties
        /// <summary>
        /// Lazy initialisation instance accessor
        /// </summary>
        public static InterContainerLODManager Instance
        {
            get
            {
                if (instance_ == null)
                    instance_ = new InterContainerLODManager();

                return instance_;
            }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Singleton instance.
        /// </summary>
        private static InterContainerLODManager instance_;

        private SceneCollection sceneCollection_;
        private ContentTreeHelper contentTreeHelper_;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor (private for singleton pattern).
        /// </summary>
        private InterContainerLODManager()
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Initialisation.
        /// </summary>
        /// <param name="sceneCollection"></param>
        /// <param name="contentTreeHelper"></param>
        public void Initialise(SceneCollection sceneCollection, ContentTreeHelper contentTreeHelper)
        {
            sceneCollection_ = sceneCollection;
            contentTreeHelper_ = contentTreeHelper;
        }

        /// <summary>
        /// Return number of SLOD2 child entities.
        /// </summary>
        /// <param name="slod2ObjectDef"></param>
        /// <returns></returns>
        public int GetSLOD2ObjectChildCount(TargetObjectDef slod2ObjectDef)
        {
            return GetSLOD2ObjectLODChildren(slod2ObjectDef).Length;
        }

        /// <summary>
        /// Return SLOD2 child objects.
        /// </summary>
        /// <param name="slod2ObjectDef"></param>
        /// <returns></returns>
        public TargetObjectDef[] GetSLOD2ObjectLODChildren(TargetObjectDef slod2ObjectDef)
        {
            List<TargetObjectDef> lodChildren = new List<TargetObjectDef>();

            // From the scene xml node, we want to find all LOD siblings.
            // e.g. dt1_lod.xml => [ dt1_01.xml, dt1_01_props.xml, ... ]

            // Instance placement XML's don't map properly in the content tree so we can't resolve the asset combine nodes. Use the original SceneXML filename.
            String baseSceneXML = contentTreeHelper_.GetBaseSceneXMLFromInstancePlacementFilename(slod2ObjectDef.MyScene.Filename);
            IBranch branch = this.contentTreeHelper_.ContentTree.Branch;
            IEnumerable<IContentNode> sceneXmlMapSiblingNodes = GetDistrictMapContainerSceneXmlNodes(branch, baseSceneXML);

            foreach (IContentNode sceneXmlMapSiblingNode in sceneXmlMapSiblingNodes)
            {
                IContentNode sceneXml = contentTreeHelper_.TryGetDlcBranchEquivalent(branch, sceneXmlMapSiblingNode);

                String mapScenePathname = ((IFilesystemNode)sceneXml).AbsolutePath;
                Scene mapScene = sceneCollection_.LoadScene(mapScenePathname);

                // Count the objects that ref this SLOD2 object
                if (mapScene != null)
                {
                    GetSLOD2ObjectLODChildrenFromScene(slod2ObjectDef, mapScene, lodChildren);
                }
            }

            return lodChildren.ToArray();
        }

        /// <summary>
        /// Return SLOD2 parent entity IMAP index.
        /// </summary>
        /// <param name="slod1ObjectDef"></param>
        /// <returns></returns>
        public int GetSLOD2ParentIndex(TargetObjectDef slod1ObjectDef)
        {
            String slod2ObjectName = slod1ObjectDef.LOD.Parent.RefName;
            String slod2ObjectNameIPLGroup = slod1ObjectDef.GetAttribute(AttrNames.OBJ_IPL_GROUP, String.Empty);

            if (String.IsNullOrEmpty(slod2ObjectName))
                return -1;

            // From the scene xml node, we want to find all LOD siblings.
            // e.g. dt1_01.xml => [ dt1_lod.xml ]
            //      cs1_03_props.xml => [ cs1_lod1.xml, cs1_lod2.xml ]

            // Instance placement XML's don't map properly in the content tree so we can't resolve the asset combine nodes. Use the original SceneXML filename.
            String baseSceneXML = contentTreeHelper_.GetBaseSceneXMLFromInstancePlacementFilename(slod1ObjectDef.MyScene.Filename);
            IBranch branch = this.contentTreeHelper_.ContentTree.Branch;
            IEnumerable<IContentNode> sceneXmlLODSiblingNodes = GetDistrictLODMapContainerSceneXmlNodes(branch, baseSceneXML);

            foreach (IContentNode sceneXmlLODSiblingNode in sceneXmlLODSiblingNodes)
            {
                String lodScenePathname = ((IFilesystemNode)sceneXmlLODSiblingNode).AbsolutePath;
                Scene lodScene = sceneCollection_.LoadScene(lodScenePathname);
                if (lodScene != null)
                {
                    int imapIndex = GetObjectIMAPIndexFromScene(slod2ObjectName, lodScene, slod2ObjectNameIPLGroup);
                    if (imapIndex >= 0)
                        return imapIndex;
                }
            }

            return -1;
        }

        /// <summary>
        /// Given a container LOD ref object, returns the corresponding object def from the source scene
        /// </summary>
        /// <param name="slod2RefObject">The ref object in the map container</param>
        /// <returns>The corresponding ref object in the lod container, or null if it couldn't be found</returns>
        public TargetObjectDef GetSourceSLOD2ObjectDef(TargetObjectDef slod2RefObject)
        {
            if (slod2RefObject == null)
                throw new ArgumentNullException("objectDef");

            if (!slod2RefObject.IsContainerLODRefObject())
                throw new ArgumentException("Argument objectDef must be a Container LOD Ref Object", "objectDef");

            // From the scene xml node, we want to find all LOD siblings.
            // e.g. dt1_01.xml => [ dt1_lod.xml ]
            //      cs1_03_props.xml => [ cs1_lod1.xml, cs1_lod2.xml ]

            // Instance placement XML's don't map properly in the content tree so we can't resolve the asset combine nodes. Use the original SceneXML filename.
            String baseSceneXML = contentTreeHelper_.GetBaseSceneXMLFromInstancePlacementFilename(slod2RefObject.MyScene.Filename);
            IBranch branch = this.contentTreeHelper_.ContentTree.Branch;
            IEnumerable<IContentNode> sceneXmlLODSiblingNodes = GetDistrictLODMapContainerSceneXmlNodes(branch, baseSceneXML);

            // DHM - this isn't currently required because we'll try to enforce that the
            // _lod SLOD2 container IMAPs are available in DLC too.
#if false
            // If we are DLC; we may be looking in the DLC content for SLOD2 objects.
            // Lets look in the core project content if that failed.
            if (this.contentTreeHelper_.ContentTree.Branch.Project.IsDLC && (0 == exportZipLODSiblingNodes.Length))
            {
                String branchName = this.contentTreeHelper_.ContentTree.Branch.Name;
                RSG.Base.Configuration.IBranch dlcBranch = this.contentTreeHelper_.ContentTree.Branch;
                RSG.Base.Configuration.IBranch coreBranch = this.contentTreeHelper_.ContentTree.Branch.Project.Config.CoreProject.Branches[branchName];
                String coreProjectFilename = slod2RefObject.MyScene.Filename.Replace(dlcBranch.Export, coreBranch.Export);
                sceneXmlNode = contentTreeHelper_.CreateFile(coreProjectFilename);
                exportZipNode = contentTreeHelper_.GetExportZipNodeFromSceneXmlNode(sceneXmlNode);
                assetCombineNode = contentTreeHelper_.GetCombineOutputForNodes(new Pipeline.Core.IContentNode[] { exportZipNode });
                exportZipSiblingNodes = contentTreeHelper_.GetAllInputsForNode(assetCombineNode);
                exportZipLODSiblingNodes = exportZipSiblingNodes.Where(node => contentTreeHelper_.ExportNodeIsLODContainer(node)).ToArray();
                sceneXmlLODSiblingNodes = contentTreeHelper_.GetSceneXmlNodesFromExportZipNodes(exportZipLODSiblingNodes);
            }
#endif

            foreach (IContentNode sceneXmlLODSiblingNode in sceneXmlLODSiblingNodes)
            {
                String lodScenePathname = ((IFilesystemNode)sceneXmlLODSiblingNode).AbsolutePath;
                Scene lodScene = sceneCollection_.LoadScene(lodScenePathname);
                if (lodScene != null)
                {
                    TargetObjectDef sourceObjectDef = GetObjectDefFromScene(slod2RefObject.RefName, lodScene);
                    if (sourceObjectDef != null)
                        return sourceObjectDef;
                }
            }

            return null;
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Return district Map Container SceneXml nodes for given map SceneXml (no LODs returned).
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="sceneXmlFilename"></param>
        /// <returns></returns>
        private IEnumerable<IContentNode> GetDistrictMapContainerSceneXmlNodes(IBranch branch, String sceneXmlFilename)
        {
            // Get immediate files for district.
            IContentNode sceneXmlNode = contentTreeHelper_.CreateFile(sceneXmlFilename);
            IContentNode exportZipNode = contentTreeHelper_.GetExportZipNodeFromSceneXmlNode(sceneXmlNode);
            IContentNode assetCombineNode = contentTreeHelper_.GetCombineOutputForNodes(new IContentNode[] { exportZipNode });
            HashSet<IContentNode> exportZipSiblingNodes = contentTreeHelper_.GetAllInputsForNode(assetCombineNode);
            IContentNode[] exportZipMapSiblingNodes = exportZipSiblingNodes.Where(node => contentTreeHelper_.ExportNodeIsMapContainer(node)).ToArray();
            
            List<IContentNode> sceneXmlMapSiblingNodes = new List<IContentNode>();
            sceneXmlMapSiblingNodes.AddRange(contentTreeHelper_.GetSceneXmlNodesFromExportZipNodes(exportZipMapSiblingNodes));            

            // Add core district files if we are building DLC.
            if (branch.Project.IsDLC)
            {
                IBranch coreBranch = branch.Project.Config.CoreProject.Branches[branch.Name];
                String coreSceneXmlFilename = sceneXmlFilename.ToLower().Replace(branch.Export.ToLower(), coreBranch.Export.ToLower());

                IEnumerable<IContentNode> coreDistrictFiles = GetDistrictMapContainerSceneXmlNodes(coreBranch, coreSceneXmlFilename);
                List<String> names = sceneXmlMapSiblingNodes.Select(n => n.Name).ToList();
                foreach (IContentNode node in coreDistrictFiles)
                {
                    if (!names.Contains(node.Name, StringComparer.OrdinalIgnoreCase))
                        sceneXmlMapSiblingNodes.Add(node);
                }
            }

            return (sceneXmlMapSiblingNodes);
        }

        /// <summary>
        /// Return district LOD Map Container SceneXml nodes for given map SceneXml.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="sceneXmlFilename"></param>
        /// <returns></returns>
        private IEnumerable<IContentNode> GetDistrictLODMapContainerSceneXmlNodes(IBranch branch, String sceneXmlFilename)
        {
            IContentNode sceneXmlNode = contentTreeHelper_.CreateFile(sceneXmlFilename);
            IContentNode exportZipNode = contentTreeHelper_.GetExportZipNodeFromSceneXmlNode(sceneXmlNode);
            IContentNode assetCombineNode = contentTreeHelper_.GetCombineOutputForNodes(new IContentNode[] {exportZipNode});
            HashSet<IContentNode> exportZipSiblingNodes = contentTreeHelper_.GetAllInputsForNode(assetCombineNode);
            IEnumerable<IContentNode> exportZipLODSiblingNodes = exportZipSiblingNodes.Where(node => contentTreeHelper_.ExportNodeIsLODContainer(node));
            List<IContentNode> sceneXmlLODSiblingNodes = new List<IContentNode>(contentTreeHelper_.GetSceneXmlNodesFromExportZipNodes(exportZipLODSiblingNodes));


            // we might need to check core data LOD for SLOD2 validation, so let's try to find those core LODs shit
            if (branch.Project.IsDLC)
            {
                List<IContentNode> coreXmlLODs = sceneXmlLODSiblingNodes.Select(node => contentTreeHelper_.GetCoreBranchEquivalent(branch, node)).ToList();
                if (coreXmlLODs.Any())
                {
                    sceneXmlLODSiblingNodes.AddRange(coreXmlLODs);
                }
            }

            return sceneXmlLODSiblingNodes.Distinct();
        }

        private static void GetSLOD2ObjectLODChildrenFromScene(TargetObjectDef slod2ObjectDef, Scene childContainerScene, ICollection<TargetObjectDef> lodChildren)
        {
            foreach (TargetObjectDef objectDef in childContainerScene.Objects)
            {
                GetSLOD2ObjectLODChildrenRecursive(slod2ObjectDef, objectDef, lodChildren);
            }
        }

        private static void GetSLOD2ObjectLODChildrenRecursive(TargetObjectDef slod2ObjectDef, TargetObjectDef childContainerObjectDef, ICollection<TargetObjectDef> lodChildren)
        {
            if (childContainerObjectDef.IsContainer())
            {
                foreach (TargetObjectDef childObject in childContainerObjectDef.Children)
                {
                    GetSLOD2ObjectLODChildrenRecursive(slod2ObjectDef, childObject, lodChildren);
                }
            }
            else if (childContainerObjectDef.IsObject())
            {
                if ((childContainerObjectDef.LODLevel == ObjectDef.LodLevel.SLOD1) &&
                    (childContainerObjectDef.LOD.Parent != null) &&
                    (childContainerObjectDef.LOD.Parent.IsContainerLODRefObject()) &&
                    (String.Compare(childContainerObjectDef.LOD.Parent.RefName, slod2ObjectDef.Name, true) == 0))
                {
                    lodChildren.Add(childContainerObjectDef);
                }
            }
        }

        /// <remarks>
        /// The "iplGroupName" argument can be String.Empty; this will mean we don't check for IPL Groups.
        /// url:bugstar:3846646 - Handle IPL groups properly in the Inter Container LOD Manager.
        /// </remarks>
        private static int GetObjectIMAPIndexFromScene(string objectName, Scene scene, String iplGroupName)
        {
            int result = -1;

            List<TargetObjectDef> imapObjects = GetObjectsFromSceneInIMAPOrder(scene, iplGroupName);
            for (int objectIndex = 0; objectIndex < imapObjects.Count; ++objectIndex)
            {
                if (String.Compare(objectName, imapObjects[objectIndex].Name, true) == 0)
                {
                    return objectIndex;
                }
            }

            return result;
        }

        /// <remarks>
        /// The "iplGroupName" argument can be String.Empty; this will mean we don't check for IPL Groups.
        /// url:bugstar:3846646 - Handle IPL groups properly in the Inter Container LOD Manager.
        /// </remarks>
        private static List<TargetObjectDef> GetObjectsFromSceneInIMAPOrder(Scene scene, String iplGroupName)
        {
            List<TargetObjectDef> imapObjects = new List<TargetObjectDef>();

            // DHM: we have Scene.Walk to save us doing this everywhere.
            foreach (TargetObjectDef objectDef in scene.Objects)
            {
                GetObjectsFromSceneRecursive(objectDef, imapObjects, iplGroupName);
            }

            imapObjects.Sort(CompareObjectDefsOnLODData);

            return imapObjects;
        }

        private static void GetObjectsFromSceneRecursive(TargetObjectDef objectDef, ICollection<TargetObjectDef> imapObjects, String iplGroupName)
        {
            if (objectDef.IsContainer())
            {
                foreach (TargetObjectDef childObjectDef in objectDef.Children)
                {
                    GetObjectsFromSceneRecursive(childObjectDef, imapObjects, iplGroupName);
                }
            }
            else if (objectDef.IsObject() && !objectDef.DontExportIPL() && !objectDef.HasShadowMeshLink())
            {
                // We only want to add entities that are part of the same IPL Group because
                // otherwise we completely rubber duck the entity indices.
                String objectIplGroupName = objectDef.GetAttribute(AttrNames.OBJ_IPL_GROUP, String.Empty);

                if (String.Equals(iplGroupName, objectIplGroupName, StringComparison.OrdinalIgnoreCase))
                {
                    imapObjects.Add(objectDef);
                }
            }
        }

        private static int CompareObjectDefsOnLODData(TargetObjectDef lhs, TargetObjectDef rhs)
        {
            // This algorithm is duplicated in Entity.CompareTo(), so don't change it

            if (TargetObjectDef.IsLODParentOf(lhs, rhs))
                return (1); // Child (this) follows entity.
            else if (TargetObjectDef.IsLODChildOf(lhs, rhs))
                return (-1); // Parent (this) precedes entity.
            else
            {
                int lhsLodLevel = (int)lhs.LODLevel;
                int rhsLodLevel = (int)rhs.LODLevel;

                if (lhsLodLevel > rhsLodLevel)
                    return (-1);
                else if (lhsLodLevel < rhsLodLevel)
                    return (1);
                return (0);
            } 
        }

        private static TargetObjectDef GetObjectDefFromScene(string refName, Scene scene)
        {
            foreach (TargetObjectDef objectDef in scene.Objects)
            {
                TargetObjectDef sourceObjectDef = GetObjectDefFromSceneRecursive(refName, objectDef);

                if (sourceObjectDef != null)
                    return sourceObjectDef;
            }

            return null;
        }

        private static TargetObjectDef GetObjectDefFromSceneRecursive(string refName, TargetObjectDef objectDef)
        {
            if (objectDef.IsContainer())
            {
                foreach (TargetObjectDef childObjectDef in objectDef.Children)
                {
                    TargetObjectDef sourceObjectDef = GetObjectDefFromSceneRecursive(refName, childObjectDef);

                    if (sourceObjectDef != null)
                        return sourceObjectDef;
                }
            }
            else if (objectDef.IsObject())
            {
                if (String.Compare(refName, objectDef.Name, true) == 0)
                {
                    return objectDef;
                }
            }

            return null;
        }
        #endregion // Private Methods
    }

} // RSG.SceneXml.MapExport.Framework namespace
