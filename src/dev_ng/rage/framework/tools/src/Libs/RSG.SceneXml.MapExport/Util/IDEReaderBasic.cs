//
// File: IDEReaderBasic.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of IDEReaderBasic class
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using RSG.Base.IO;
using RSG.Base.Math;

namespace RSG.SceneXml.MapExport.Util
{

    /// <summary>
    /// Basic IDE file reader.
    /// </summary>
    /// For the IPL export we need to know every object's LOD distance, even
    /// for XRef'd objects.  So we quickly parse each existing IDE file and
    /// fetch object definition name and LOD distance so if we need it when
    /// creating our IPL(s) then we have it.
    /// 
    /// TODO: It would be nice at some point to move this to parse the 
    /// SceneXml files once its determined where they are going.
    public class IDEReaderBasic
    {
        #region Enumerations
        /// <summary>
        /// 
        /// </summary>
        private enum IDESection
        {
            Ignore,
            Objects,
            TimeObjects,
            AnimObjects,
            TimeAnimObjects,
            MiloObjects,
        }
        #endregion // Enumerations

        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<String, List<String>> ObjectNames
        {
            get { return m_dObjectNames; }
        }
        private Dictionary<String, List<String>> m_dObjectNames; // Key: IDE file, Value: List of object names
        
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<String, List<String>> TXDNames
        {
            get { return m_dTXDNames; }
            private set { m_dTXDNames = value; }
        }
        private Dictionary<String, List<String>> m_dTXDNames; // Key: IDE file, Value: List of TXD names
        #endregion // Properties and Associated Member Data

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private Dictionary<String, BoundingBox3f> ObjectBounds;

        private String m_sPathPrefix;
        private Dictionary<String, float> m_dLODDistances;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pathPrefix"></param>
        public IDEReaderBasic(String pathPrefix)
        {
            m_sPathPrefix = pathPrefix;
            m_dLODDistances = new Dictionary<String, float>();
            m_dObjectNames = new Dictionary<String, List<String>>();
            TXDNames = new Dictionary<String, List<String>>();
            ObjectBounds = new Dictionary<String, BoundingBox3f>();

            Parse();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return LOD distance read from IDE file for an object.
        /// </summary>
        /// <param name="object_name"></param>
        /// <returns></returns>
        public float GetLODDistance(String object_name)
        {
            Debug.Assert(m_dLODDistances.ContainsKey(object_name.ToLower()),
                String.Format("No LOD distance for object {0}.  Missing from IDE files?", object_name));
            if (!m_dLODDistances.ContainsKey(object_name.ToLower()))
                return (0.0f);

            return (m_dLODDistances[object_name.ToLower()]);
        }

        /// <summary>
        /// Return whether we have a LOD distance for an object.
        /// </summary>
        /// <param name="object_name"></param>
        /// <returns></returns>
        public bool HasLODDistance(String object_name)
        {
            return (m_dLODDistances.ContainsKey(object_name.ToLower()));
        }

        /// <summary>
        /// Return bounding box read from IDE file for an object.
        /// </summary>
        /// <param name="object_name"></param>
        /// <returns></returns>
        public BoundingBox3f GetBoundingBox(String object_name)
        {
            Debug.Assert(HasBoundingBox(object_name.ToLower()),
                String.Format("No LocalBoundingBox for object {0}.  Missing from IDE files?", object_name));
            if (!HasBoundingBox(object_name.ToLower()))
                return (new BoundingBox3f());

            return (ObjectBounds[object_name.ToLower()]);
        }

        /// <summary>
        /// Return whether we have a bounding box for an object.
        /// </summary>
        /// <param name="object_name"></param>
        /// <returns></returns>
        public bool HasBoundingBox(String object_name)
        {
            return (ObjectBounds.ContainsKey(object_name.ToLower()));
        }
        #endregion // Controller Methods

        #region Private Merhods
        /// <summary>
        /// Parse all project IDE files.
        /// </summary>
        private void Parse()
        {
            String[] files = FindUtil.Find(Path.Combine(m_sPathPrefix, "*.ide"), true);
            foreach (String filename in files)
                ParseFile(filename);
        }

        /// <summary>
        /// Parse a single IDE file for object name and LOD distances.
        /// </summary>
        /// <param name="filename">Absolute path to IDE file to parse</param>
        private void ParseFile(String filename)
        {
            String ide_basename = Path.GetFileNameWithoutExtension(filename);
            IDESection section = IDESection.Ignore;
            using (StreamReader reader = new StreamReader(filename))
            {
                // Create Object and TXD name storage for this IDE file.
                m_dObjectNames[ide_basename] = new List<String>();
                m_dTXDNames[ide_basename] = new List<String>();                            

                String text = String.Empty;
                while ((text = reader.ReadLine()) != null)
                {
                    if (String.IsNullOrEmpty(text))
                        continue; // Skip empty lines
                    if (text.StartsWith("#"))
                        continue; // Skip comments

                    // State changes
                    if ("objs" == text)
                    {
                        section = IDESection.Objects;
                        continue;
                    }
                    else if ("tobj" == text)
                    {
                        section = IDESection.TimeObjects;
                        continue;
                    }
                    else if ("anim" == text)
                    {
                        section = IDESection.AnimObjects;
                        continue;
                    }
                    else if ("tanm" == text)
                    {
                        section = IDESection.TimeAnimObjects;
                        continue;
                    }
                    else if ("mlo" == text)
                    {
                        section = IDESection.MiloObjects;
                    }
                    else if ("end" == text)
                    {
                        section = IDESection.Ignore;
                        continue;
                    }
                    if (IDESection.Ignore == section)
                        continue;

                    String[] parts = text.Split(new char[] { ',' });
                    if (parts.Length < 3)
                        continue;
                    String object_name = String.Empty;
                    if (parts.Length > 0)
                        object_name = parts[0].ToLower();

                    switch (section)
                    {
                        case IDESection.Ignore:
                            continue;
                        case IDESection.Objects:
                            {
                                if (parts.Length < 10)
                                    continue;
                                if (!m_dTXDNames[ide_basename].Contains(parts[1].ToLower()))
                                    m_dTXDNames[ide_basename].Add(parts[1].ToLower());
                                m_dObjectNames[ide_basename].Add(object_name);
                                m_dLODDistances[object_name] = float.Parse(parts[2]);

                                Vector3f min = new Vector3f(float.Parse(parts[5]), float.Parse(parts[6]), float.Parse(parts[7]));
                                Vector3f max = new Vector3f(float.Parse(parts[8]), float.Parse(parts[9]), float.Parse(parts[10]));
                                ObjectBounds[object_name] = new BoundingBox3f(min, max);
                            }
                            break;
                        case IDESection.TimeObjects:
                            {
                                if (!m_dTXDNames[ide_basename].Contains(parts[1].ToLower()))
                                    m_dTXDNames[ide_basename].Add(parts[1].ToLower());
                                m_dObjectNames[ide_basename].Add(object_name);
                                m_dLODDistances[object_name] = float.Parse(parts[2]);

                                Vector3f min = new Vector3f(float.Parse(parts[5]), float.Parse(parts[6]), float.Parse(parts[7]));
                                Vector3f max = new Vector3f(float.Parse(parts[8]), float.Parse(parts[9]), float.Parse(parts[10]));
                                ObjectBounds[object_name] = new BoundingBox3f(min, max);
                            }
                            break;
                        case IDESection.AnimObjects:
                            {
                                if (parts.Length < 4)
                                    continue;
                                if (!m_dTXDNames[ide_basename].Contains(parts[1].ToLower()))
                                    m_dTXDNames[ide_basename].Add(parts[1].ToLower());
                                m_dObjectNames[ide_basename].Add(object_name);
                                m_dLODDistances[object_name] = float.Parse(parts[3]);

                                Vector3f min = new Vector3f(float.Parse(parts[6]), float.Parse(parts[7]), float.Parse(parts[8]));
                                Vector3f max = new Vector3f(float.Parse(parts[9]), float.Parse(parts[10]), float.Parse(parts[11]));
                                ObjectBounds[object_name] = new BoundingBox3f(min, max);
                            }
                            break;
                        case IDESection.TimeAnimObjects:
                            {
                                if (parts.Length < 4)
                                    continue;
                                if (!m_dTXDNames[ide_basename].Contains(parts[1].ToLower()))
                                    m_dTXDNames[ide_basename].Add(parts[1].ToLower());
                                m_dObjectNames[ide_basename].Add(object_name);
                                m_dLODDistances[object_name] = float.Parse(parts[3]);

                                Vector3f min = new Vector3f(float.Parse(parts[6]), float.Parse(parts[7]), float.Parse(parts[8]));
                                Vector3f max = new Vector3f(float.Parse(parts[9]), float.Parse(parts[10]), float.Parse(parts[11]));
                                ObjectBounds[object_name] = new BoundingBox3f(min, max);
                            }
                            break;
                        case IDESection.MiloObjects:
                            if (parts.Length < 6)
                                continue;
                            m_dLODDistances[object_name] = float.Parse(parts[5]);
                            break;
                    }
                }
            }
        }
        #endregion // Private Methods
    }

} // RSG.SceneXml.MapExport.Util

// IDEReaderBasic.cs
