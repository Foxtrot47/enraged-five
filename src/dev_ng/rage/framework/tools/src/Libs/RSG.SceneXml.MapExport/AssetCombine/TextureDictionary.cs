﻿using System;
using System.Diagnostics;
using System.Xml;
using RSG.Base.Configuration;
using RSG.Base.Logging;

namespace RSG.SceneXml.MapExport.AssetCombine
{

    /// <summary>
    /// 
    /// </summary>
    public class TextureDictionary : AssetOutput
    {
        public bool IsNewDlc { get; private set; }
        public bool IsNewTxd { get; private set; }

        #region Construtor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public TextureDictionary(String name, bool isNewDlc = false, bool isNewTxd = false)
            : base(name)
        {
            IsNewDlc = isNewDlc;
            IsNewTxd = isNewTxd;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <param name="projectIsDlc"></param>
        /// <returns></returns>
        public override XmlElement ToXml(XmlDocument xmlDoc, bool projectIsDlc)
        {
            XmlElement xmlAsset = ToXml(xmlDoc, "TextureDictionary", projectIsDlc);
            xmlAsset.SetAttribute("isNewDlc", IsNewDlc.ToString());
            xmlAsset.SetAttribute("isNewTxd", IsNewTxd.ToString());
            return (xmlAsset);
        }

        protected override XmlElement ToXml(XmlDocument xmlDoc, string name, bool projectIsDlc)
        {
            XmlElement xmlElem = xmlDoc.CreateElement(name);
            xmlElem.SetAttribute("name", this.Name);
            XmlElement xmlInputs = xmlDoc.CreateElement("Inputs");
            foreach (AssetInput input in this.Inputs)
            {
                XmlElement xmlInput = input.ToXml(xmlDoc);
                xmlInputs.AppendChild(xmlInput);
                // comment out if it's a DLC and it's not a new object or not new txd 
                if (projectIsDlc && (!input.IsNewDLC || (input.IsNewDLC && !input.IsNewTXD)))
                {
                    XmlComment comment = xmlDoc.CreateComment(xmlInput.OuterXml);
                    xmlInputs.ReplaceChild(comment, xmlInput);
                    //xmlInputs.AppendChild(comment);
                }
            }
            xmlElem.AppendChild(xmlInputs);

            return (xmlElem);
        }
        #endregion // Controller Methods

        #region Static Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="xmlElem"></param>
        /// <returns></returns>
        public static TextureDictionary Create(IBranch branch, XmlElement xmlElem)
        {
            Debug.Assert(xmlElem.Name.Equals("TextureDictionary"), "Invalid XML.");
            if (!xmlElem.Name.Equals("TextureDictionary"))
            {
                Log.Log__Error("Invalid XML constructing TextureDictionary ({0}).", xmlElem.Name);
                return (null);
            }

            String name = xmlElem.GetAttribute("name");
            
            bool isNewDlc;
            bool.TryParse(xmlElem.GetAttribute("isNewDlc"), out isNewDlc);

            bool isNewTxd;
            bool.TryParse(xmlElem.GetAttribute("isNewTxd"), out isNewTxd);

            XmlNodeList xmlInputsList = xmlElem.SelectNodes("Inputs/Input");
            TextureDictionary textureDict = new TextureDictionary(name, isNewDlc, isNewTxd);
            foreach (XmlNode xmlInput in xmlInputsList)
            {
                AssetInput input = AssetInput.Create(branch, xmlInput as XmlElement);
                textureDict.AddInput(input);
            }

            return (textureDict);
        }
        #endregion // Static Controller Methods
    }

} // RSG.SceneXml.MapExport.AssetCombine namespace
