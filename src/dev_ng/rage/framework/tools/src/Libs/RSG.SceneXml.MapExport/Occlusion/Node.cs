﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Xml.Linq;

using RSG.Base.Math;

namespace RSG.SceneXml.MapExport.Occlusion
{
    /// <summary>
    /// This class represents an occlusion node and may represent any level in an occlusion hierarchy.
    /// Each instance consists of either Occlusion.Model instances, or further child nodes if it is
    /// part of the upper hierarchy.
    /// </summary>
    public class Node
    {
        #region Constructor

        public Node(IEnumerable<Model> models)
        {
            models_ = new List<Model>(models);
            childNodes_ = null;
        }

        #endregion Constructor

        #region Public Properties

        public int DataSize
        {
            get
            {
                return models_.Sum(model => model.DataSize);
            }
        }

        public BoundingBox3f WorldBoundingBox
        {
            get
            {
                if (IsLeaf)
                {
                    BoundingBox3f boundingBox = new BoundingBox3f();
                    foreach (Model model in models_)
                        boundingBox.Expand(model.WorldBoundingBox);
                    return boundingBox;
                }

                return null;
            }
        }

        #endregion Public Properties

        #region Public Methods

        /// <summary>
        /// Given an xml document and a parent element, attaches nodes for the box and mesh nodes
        /// </summary>
        public void AddOcclusionModelsToXmlDocument(XElement parentElement)
        {
            XElement boxOcclusionRootNode = new XElement("boxOccluders");
            parentElement.Add(boxOcclusionRootNode);
            foreach (Model boxModel in models_.OfType<Box>())
                boxModel.AddToXmlDocument(boxOcclusionRootNode);

            XElement meshOcclusionRootNode = new XElement("occludeModels");
            parentElement.Add(meshOcclusionRootNode);
            foreach (Model meshModel in models_.OfType<Mesh>())
                meshModel.AddToXmlDocument(meshOcclusionRootNode);
        }

        /// <summary>
        /// Splits a Node into a hierarchy of nodes based on a maximum size.  
        /// A flattened representation of that hierachy can then be accessed via GetLeafNodesRecursive().
        /// 
        /// The algorithm used here is simply to split in half in the largest dimension (x/y) recursively.
        /// To achieve this we just order models by dimension and then split into two child collections.
        /// </summary>
        public void Split(int maxNodeDataSize)
        {
            if (IsLeaf)
            {
                int nodeDataSize = models_.Sum(model => model.DataSize);
                if (nodeDataSize > maxNodeDataSize)
                {
                    // Determine the bounds
                    BoundingBox3f boundingBox = new BoundingBox3f();
                    foreach (Model model in models_)
                        boundingBox.Expand(model.WorldBoundingBox);

                    // Sort the objects in the largest min dimension (x/y)
                    Model[] sortedModels = null;
                    float boundingBoxXLength = boundingBox.Max.X - boundingBox.Min.X;
                    float boundingBoxYLength = boundingBox.Max.Y - boundingBox.Min.Y;
                    bool splitInX = (boundingBoxXLength >= boundingBoxYLength);
                    if (splitInX)
                        sortedModels = models_.OrderBy(model => model.WorldBoundingBox.Min.X).ToArray();
                    else
                        sortedModels = models_.OrderBy(model => model.WorldBoundingBox.Min.Y).ToArray();

                    // Collect objects until the data size is > half and split into two children
                    List<Model> lowerSplit = new List<Model>();
                    List<Model> upperSplit = new List<Model>();
                    bool addToLower = true;
                    int lowerSplitDataSize = 0;
                    foreach (Model model in sortedModels)
                    {
                        if (addToLower)
                        {
                            if (lowerSplitDataSize + model.DataSize <= (nodeDataSize / 2))
                            {
                                lowerSplit.Add(model);
                                lowerSplitDataSize += model.DataSize;
                            }
                            else
                            {
                                addToLower = false;
                                upperSplit.Add(model);
                            }
                        }
                        else
                        {
                            upperSplit.Add(model);
                        }
                    }

                    // Null our models out
                    models_ = null;
                    childNodes_ = new List<Node>();
                    childNodes_.Add(new Node(lowerSplit));
                    childNodes_.Add(new Node(upperSplit));
                    
                    // Call Split() on our children
                    foreach (Node childNode in childNodes_)
                        childNode.Split(maxNodeDataSize);
                }
            }
            else
            {
                Debug.Assert(childNodes_ != null);
                foreach (Node childNode in childNodes_)
                    childNode.Split(maxNodeDataSize);
            }
        }

        /// <summary>
        /// Gets a flat array of all leaf nodes under this node
        /// </summary>
        public Node[] GetLeafNodesRecursive()
        {
            List<Node> leafNodes = new List<Node>();
            GetLeafNodesRecursive(leafNodes);
            return leafNodes.ToArray();
        }

        #endregion Public Methods

        #region Private Methods

        private void GetLeafNodesRecursive(ICollection<Node> nodeCollection)
        {
            if (IsLeaf)
            {
                nodeCollection.Add(this);
            }
            else
            {
                foreach (Node childNode in childNodes_)
                {
                    childNode.GetLeafNodesRecursive(nodeCollection);
                }
            }
        }

        #endregion Private Methods

        #region Private Data

        private bool IsLeaf { get { return models_ != null && models_.Any(); } }

        private List<Model> models_;
        private List<Node> childNodes_;

        #endregion Private Data
    }
}
