using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using SMath = System.Math;
using System.Xml;
using System.Xml.Linq;
using RSG.SceneXml;
using RSG.Base.Collections;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.SceneXml.MapExport.Collections;

namespace RSG.SceneXml.MapExport
{

    /// <summary>
    /// Map IPL File Serialiser class (version 6) from SceneXml.
    /// </summary>
    public class SceneSerialiserIPL6 : SceneSerialiserIPLBase
    {
        #region Constants
        private readonly int IPL_VERSION = 6;
        #region IPL File Section Tags
        private readonly String TAG_VERS = "vers";
        private readonly String TAG_BOUND = "bounds";
        private readonly String TAG_INST = "inst";
        private readonly String TAG_MILOINST = "miloinst";
        private readonly String TAG_CULL = "cull";
        private readonly String TAG_PATH = "path";
        private readonly String TAG_GRGE = "grge";
        private readonly String TAG_CARS = "cars";
        private readonly String TAG_TCYC = "tcyc";
        private readonly String TAG_BLOK = "blok";
        private readonly String TAG_VNOD = "vnod";
        private readonly String TAG_LINK = "link";
        private readonly String TAG_PNOD = "pnod";
        private readonly String TAG_PLNK = "plnk";
        private readonly String TAG_MLOP = "mlo+";
        private readonly String TAG_2DFX = "2dfx";
        private readonly String TAG_LODCHILDREN = "lodchildren";
        private readonly String TAG_LIGHTINSTS = "lightinsts";
        private readonly String TAG_MAXLOD = "maxlod";
        private readonly String TAG_END = "end";
        #endregion // IPL File Section Tags
        #endregion // Constants

        #region Properties
        /// <summary>
        /// IPL File Version we serialise.
        /// </summary>
        public override int Version
        {
            get { return IPL_VERSION; }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Container for all scene Streamed instances.
        /// </summary>
        /// This is the container used prior to tree segmentation processing.
        protected StreamedIPL6Container SceneStreamedInstances;

        /// <summary>
        /// Container for all scene Streamed-Big instances.
        /// </summary>
        /// This is the container used prior to tree segmentation processing, 
        /// and will contain all scene objects going into StreamedIPLs.
        protected StreamedIPL6Container SceneStreamedBigInstances;

        /// <summary>
        /// Static IPL Container
        /// </summary>
        protected StaticIPL6Container StaticIPL;

        /// <summary>
        /// Bounds of all instances
        /// </summary>
        protected RSG.Base.Math.BoundingBox3f InstanceBounds;

        /// <summary>
        /// IPL Groups Container.
        /// </summary>
        protected Dictionary<String, GroupIPL6Container> IPLGroups;

        /// <summary>
        /// Streamed IPL Containers.
        /// </summary>
        protected List<StreamedIPL6Container> StreamedIPLs;

        /// <summary>
        /// Streamed-Big IPL Containers.
        /// </summary>
        protected List<StreamedIPL6Container> StreamedBigIPLs;

        /// <summary>
        /// All vehicles nodes in SceneXml (for output node indices).
        /// </summary>
        protected List<Guid> VehicleNodes;

        /// <summary>
        /// All patrol nodes in SceneXml (for output node indices).
        /// </summary>
        protected List<Guid> PatrolNodes;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Serialiser constructor.
        /// </summary>
        /// <param name="branch">Branch</param>
        /// <param name="scenes">Scenes to be serialised</param>
        /// <param name="iplstream">IPL stream directory</param>
        /// <param name="builddir">Path to project build directory (for IDE cache)</param>
        /// <param name="single">Create single IPL file iff true</param>
        public SceneSerialiserIPL6(IBranch branch, Scene[] scenes, String iplstream, String builddir, bool single)
            : base(branch, scenes, iplstream, builddir, single)
        {
            CacheVehicleNodes();
            CachePatrolNodes();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Write the serialised scene to a disk location (default options).
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public override bool Write(String filename)
        {
            return (Write(filename, new Dictionary<String, Object>()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public override bool Write(String filename, Dictionary<String, Object> options)
        {
            Log.Log__Message("Starting IPL6 serialisation:");
            Log.Log__Message("\tDestination: {0}", filename);
            Log.Log__Message("\tScenes ({0}):", this.Scenes.Length);
            foreach (Scene scene in this.Scenes)
                Log.Log__Message("\t\t{0}", scene.Filename);

            this.StaticIPL = new StaticIPL6Container();
            this.SceneStreamedInstances = new StreamedIPL6Container();
            this.SceneStreamedBigInstances = new StreamedIPL6Container();
            this.IPLGroups = new Dictionary<String, GroupIPL6Container>();
            
            ProcessSceneObjects();
            CacheBlocks();

            Algorithm.StreamInstSplitIPL6 splitAlgo = new Algorithm.StreamInstSplitIPL6();            
            AreaBinaryTree<StreamedIPL6Container> treeStreamed = splitAlgo.Split(this.SceneStreamedInstances);
            AreaBinaryTree<StreamedIPL6Container> treeStreamedLong = splitAlgo.Split(this.SceneStreamedBigInstances);
            PackageSceneInstancesIntoIPLs(treeStreamed, out this.StreamedIPLs);
            PackageSceneInstancesIntoIPLs(treeStreamedLong, out this.StreamedBigIPLs);

            // Write Static IPL
            WriteNonStreamedIPL(filename, this.StaticIPL);

            // Write Streamed-IPLs
            int n = 0;
            foreach (StreamedIPL6Container container in this.StreamedIPLs)
            {
                String streamFilename = Path.Combine(this.IPLStreamDir,
                    Path.GetFileNameWithoutExtension(filename)) + String.Format("_{0}.ipl", n);
                WriteStreamedIPL(streamFilename, container);
                ++n;
            }
            foreach (StreamedIPL6Container container in this.StreamedBigIPLs)
            {
                String streamBigFilename = Path.Combine(this.IPLStreamDir,
                    Path.GetFileNameWithoutExtension(filename)) + String.Format("_{0}.ipl", n);
                WriteStreamedIPL(streamBigFilename, container);
                ++n;
            }

            // Write IPL Group IPLs
            foreach (KeyValuePair<String, GroupIPL6Container> iplgroup in this.IPLGroups)
            {
                if (0 == iplgroup.Key.Length)
                {
                    String message = "IPL Group name is empty.  Invalid IPL Group.";
                    Debug.Assert(iplgroup.Key.Length > 0, message);
                    Log.Log__Error(message);
                    foreach (Instance6 inst in iplgroup.Value.Static.Instances)
                        Log.Log__WarningCtx(inst.Object.Name, "Object '{0}' has an empty IPL Group; default is '{1}'.", inst.Object.Name, AttrDefaults.OBJ_IPL_GROUP);
                    foreach (Instance6 inst in iplgroup.Value.Streamed.Instances)
                        Log.Log__WarningCtx(inst.Object.Name, "Object '{0}' has an empty IPL Group; default is '{1}'.", inst.Object.Name, AttrDefaults.OBJ_IPL_GROUP);
                }
                else
                {
                    String iplGroupFilename = Path.Combine(this.IPLStreamDir,
                        String.Format("{0}.ipl", iplgroup.Key));
                    WriteIPLGroup(iplGroupFilename, iplgroup.Value);
                }
            }

            Log.Log__Message("Done.");
            return (true);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xmlDoc"></param>
        /// <returns></returns>
        public override bool WriteManifestData(XDocument xmlDoc)
        {
            return (true);
        }
        #endregion // Controller Methods

        #region Protected Methods
        /// <summary>
        /// Cache all vehicle nodes in Scene.
        /// </summary>
        protected virtual void CacheVehicleNodes()
        {
            this.VehicleNodes = new List<Guid>();
            foreach (Scene scene in this.Scenes)
            {
                foreach (TargetObjectDef obj in scene.Objects)
                {
                    if (!obj.IsVehicleNode())
                        continue;
                    this.VehicleNodes.Add(obj.Guid);
                }
            }
        }

        /// <summary>
        /// Cache all patrol nodes in Scene.
        /// </summary>
        protected virtual void CachePatrolNodes()
        {
            this.PatrolNodes = new List<Guid>();
            foreach (Scene scene in this.Scenes)
            {
                foreach (TargetObjectDef obj in scene.Objects)
                {
                    if (!obj.IsPatrolNode())
                        continue;
                    this.PatrolNodes.Add(obj.Guid);
                }
            }
        }

        /// <summary>
        /// Cache block names to calculate instance block indices when writing 
        /// IPL instance data to file.
        /// </summary>
        protected override void CacheBlocks()
        {
            this.BlockNames = new String[this.StaticIPL.Blocks.Count];
            int nBlock = 0;
            foreach (Instance6 inst in this.StaticIPL.Blocks)
            {
                this.BlockNames[nBlock] = inst.Object.Name;
                ++nBlock;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="iplContainers"></param>
        protected virtual void PackageSceneInstancesIntoIPLs(AreaBinaryTree<StreamedIPL6Container> tree, out List<StreamedIPL6Container> iplContainers)
        {
            // Walk our Streamed Instances Tree, creating a new List<Instance4> 
            // for each leaf node found (those with >0 Instances in their
            // node Value property.
            iplContainers = new List<StreamedIPL6Container>();

            foreach (AreaBinaryTreeNode<StreamedIPL6Container> node in tree.Walk(AreaBinaryTree<StreamedIPL6Container>.WalkMode.DepthFirst))
            {
                if ((0 == node.Value.Instances.Count) && (0 == node.Value.CarGens.Count) &&
                    (0 == node.Value.MiloPlus.Count))
                    continue;
                Console.WriteLine("Node: inst:{0}, cargen:{1}, milo:{2}",
                    node.Value.Instances.Count, node.Value.CarGens.Count,
                    node.Value.MiloPlus.Count);

                StreamedIPL6Container iplContainer = new StreamedIPL6Container();
                iplContainers.Add(iplContainer);
                iplContainer.Instances.AddRange(node.Value.Instances);
                iplContainer.CarGens.AddRange(node.Value.CarGens);
                iplContainer.MiloPlus.AddRange(node.Value.MiloPlus);
            }
        }

        /// <summary>
        /// Write non-streamed instance information to an IPL file.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="container"></param>
        protected virtual void WriteNonStreamedIPL(String filename, StaticIPL6Container container)
        {
            // Write IDE file
            Log.Log__Message("Writing {0}...", filename);
            using (StreamWriter fpFile = File.CreateText(filename))
            {
                float maxLodDistance = 0.0f;

                fpFile.WriteLine("# IPL generated from SceneXml files:");
                foreach (Scene scene in this.Scenes)
                    fpFile.WriteLine("#\t{0}", Path.GetFileName(scene.Filename), Path.GetFileName(scene.ExportFilename));
                fpFile.WriteLine(TAG_VERS);
                fpFile.WriteLine(this.Version);
                fpFile.WriteLine(TAG_END);
                WriteBoundsSection(fpFile, container);
                fpFile.WriteLine(TAG_INST);
                int instIdx = 0;
                foreach (Tree<Instance6> instTree in container.Instances)
                {
                    foreach (TreeNode<Instance6> instNode in instTree.DepthFirstSearch)
                    {
                        // Skip non-static objects.
                        Instance6 inst = instNode.Value;
                        inst.Index = instIdx;
                        if ((inst.StreamMode != InstanceBase.StreamModeType.NotStreamed) && !this.SingleFile)
                            continue;
                        Debug.Assert(!inst.Object.DontExport(), "Object set to not export!");
                        Debug.Assert(!inst.Object.DontExportIPL(), "Object set to not add to IPL!");

                        for (int i = 0; i < inst.Indent; ++i)
                            fpFile.Write("\t");
                        fpFile.WriteLine(inst.ToIPLInst(this.BlockNames, true));

                        if (inst.LODDistance > maxLodDistance)
                            maxLodDistance = inst.LODDistance;

                        if (inst.Object.HasLODChildren())
                        {
                            // Set all LOD Children's Parent Index and clamp LOD distance
                            foreach (TreeNode<Instance6> child in instNode.DepthFirstSearch)
                            {
                                Instance6 childInst = (child.Value);
                                if (childInst.Object.LOD.Parent != inst.Object)
                                    continue;
                                childInst.LODParentIndex = instIdx;
                            }
                            // Loop through our interior instances to set their Parent index also.
                            foreach (Tree<Instance6> instMiloTree in container.MiloInstances)
                            {
                                foreach (TreeNode<Instance6> instMiloNode in instMiloTree.DepthFirstSearch)
                                {
                                    // Skip non-static objects.
                                    Instance6 instMilo = instMiloNode.Value;
                                    if ((instMilo.StreamMode != InstanceBase.StreamModeType.NotStreamed) && !this.SingleFile)
                                        continue;

                                    if (instMilo.Object.HasLODParent() && instMilo.Object.LOD.Parent == inst.Object)
                                        instMilo.LODParentIndex = instIdx;
                                }
                            }
                        }

                        instIdx += 1;
                    }
                }
                fpFile.WriteLine(TAG_END);
                fpFile.WriteLine(TAG_MILOINST);
                instIdx = 0;
                foreach (Tree<Instance6> instTree in container.MiloInstances)
                {
                    foreach (TreeNode<Instance6> instNode in instTree.DepthFirstSearch)
                    {
                        // Skip non-static objects.
                        Instance6 inst = instNode.Value;
                        if ((inst.StreamMode != InstanceBase.StreamModeType.NotStreamed) && !this.SingleFile)
                            continue;
                        Debug.Assert(!inst.Object.DontExport(), "Object set to not export!");
                        Debug.Assert(!inst.Object.DontExportIPL(), "Object set to not add to IPL!");

                        for (int i = 0; i < inst.Indent; ++i)
                            fpFile.Write("\t");
                        fpFile.WriteLine(inst.ToIPLMiloInst(this.BlockNames));

                        if (inst.Object.HasLODChildren())
                        {
                            // Set all LOD Children's Parent Index and clamp LOD distance
                            foreach (TreeNode<Instance6> child in instNode.DepthFirstSearch)
                            {
                                Instance6 childInst = (child.Value);
                                if (childInst.Object.LOD.Parent != inst.Object)
                                    continue;
                                childInst.LODParentIndex = instIdx;
                            }
                        }
                        instIdx += 1;
                    }
                }
                fpFile.WriteLine(TAG_END);
                fpFile.WriteLine(TAG_CULL);
                fpFile.WriteLine(TAG_END);
                fpFile.WriteLine(TAG_PATH);
                fpFile.WriteLine(TAG_END);
                fpFile.WriteLine(TAG_GRGE);
                fpFile.WriteLine(TAG_END);
                fpFile.WriteLine(TAG_CARS);
                fpFile.WriteLine(TAG_END);
                fpFile.WriteLine(TAG_TCYC);
                foreach (Instance6 inst in container.TimeCycles)
                    fpFile.WriteLine(inst.ToIPLTimeCycle());
                fpFile.WriteLine(TAG_END);
                fpFile.WriteLine(TAG_BLOK);
                foreach (Instance6 inst in container.Blocks)
                    fpFile.WriteLine(inst.ToIPLBlock());
                fpFile.WriteLine(TAG_END);
                fpFile.WriteLine(TAG_VNOD);
                foreach (Instance6 inst in container.VehicleNodes)
                    fpFile.WriteLine(inst.ToIPLVehicleNode());
                fpFile.WriteLine(TAG_END);
                fpFile.WriteLine(TAG_LINK);
                foreach (Instance6 inst in container.VehicleLinks)
                    fpFile.WriteLine(inst.ToIPLVehicleLink(this.VehicleNodes));
                fpFile.WriteLine(TAG_END);
                fpFile.WriteLine(TAG_PNOD);
                foreach (Instance6 inst in container.PatrolNodes)
                    fpFile.WriteLine(inst.ToIPLPatrolNode());
                fpFile.WriteLine(TAG_END);
                fpFile.WriteLine(TAG_PLNK);
                foreach (Instance6 inst in container.PatrolLinks)
                    fpFile.WriteLine(inst.ToIPLPatrolLink(this.PatrolNodes));
                fpFile.WriteLine(TAG_END);
                fpFile.WriteLine(TAG_2DFX);
                foreach (InstanceBase inst in container.Twodfx)
                    fpFile.WriteLine(inst.ToIPL2dfx());
                fpFile.WriteLine(TAG_END);
                fpFile.WriteLine(TAG_LODCHILDREN);
                foreach (KeyValuePair<String, int> pair in container.ContainerLODChildren)
                    fpFile.WriteLine("\t{0} {1}", pair.Key, pair.Value);
                fpFile.WriteLine(TAG_END);
                fpFile.WriteLine(TAG_LIGHTINSTS);
#if false
                // DHM 2010/11/11
                // Light Instancing Data.
                // GD Edited and running 2011/04/06
                //
                foreach (Tree<Instance6> instTree in container.Instances)
                {
                    foreach (TreeNode<Instance6> instNode in instTree.DepthFirstSearch)
                    {
                        // Skip non-static objects.
                        Instance6 inst = instNode.Value;
                        if ((inst.StreamMode != InstanceBase.StreamModeType.NotStreamed) && !this.SingleFile)
                            continue;
                        if (!(inst.Object.IsXRef() || inst.Object.IsRefObject()))
                            continue;
                        Debug.Assert(!inst.Object.DontExport(), "Object set to not export!");
                        Debug.Assert(!inst.Object.DontExportIPL(), "Object set to not add to IPL!");

                        foreach (ObjectDef child in inst.Object.Children)
                        {
                            if (!child.IsLightInstance())
                                continue;

                            Instance6 childInst = new Instance6(child.MyScene, child, IDEData);
                            fpFile.WriteLine(String.Format("{0}, {1}", inst.Index, childInst.ToIPLLightInst().Trim()));
                        }
                    }
                }
#endif // false
                fpFile.WriteLine(TAG_END);
                fpFile.WriteLine(TAG_MAXLOD);
                fpFile.WriteLine(String.Format("{0}", maxLodDistance));
                fpFile.WriteLine(TAG_END);

                fpFile.Close();
            }
        }

        /// <summary>
        /// Write streamed information to an IPL file.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="iplContainer"></param>
        protected virtual void WriteStreamedIPL(String filename, StreamedIPL6Container iplContainer)
        {
            // Write IPL file
            Log.Log__Message("Writing {0}...", filename);
            using (StreamWriter fpFile = File.CreateText(filename))
            {
                float maxLodDistance = 0.0f;

                fpFile.WriteLine("# IPL generated from SceneXml files:");
                foreach (Scene scene in this.Scenes)
                    fpFile.WriteLine("#\t{0}", Path.GetFileName(scene.Filename), Path.GetFileName(scene.ExportFilename));
                fpFile.WriteLine(TAG_VERS);
                fpFile.WriteLine(this.Version);
                fpFile.WriteLine(TAG_END);
                WriteBoundsSection(fpFile, iplContainer);
                fpFile.WriteLine(TAG_INST);
                int instIdx = 0;
                foreach (Instance6 inst in iplContainer.Instances)
                {
                    if (inst.LODDistance > maxLodDistance)
                        maxLodDistance = inst.LODDistance;
                    inst.Index = instIdx;
                    fpFile.WriteLine(inst.ToIPLInst(this.BlockNames, true));
                    ++instIdx;
                }
                fpFile.WriteLine(TAG_END);
                fpFile.WriteLine(TAG_CARS);
                foreach (Instance6 inst in iplContainer.CarGens)
                    fpFile.WriteLine(inst.ToIPLCarGen());
                fpFile.WriteLine(TAG_END);
                fpFile.WriteLine(TAG_MLOP);
                foreach (Instance6 inst in iplContainer.MiloPlus)
                    fpFile.WriteLine(inst.ToIPLMiloPlus(this.BlockNames));
                fpFile.WriteLine(TAG_END);
                fpFile.WriteLine(TAG_LIGHTINSTS);
#if false
                // DHM 2010/11/11
                // Light Instancing Data.
                // GD Edited and running 2011/04/06
                //
                foreach (Instance6 inst in iplContainer.Instances)
                {
                    if (!(inst.Object.IsXRef() || inst.Object.IsRefObject()))
                        continue;

                    foreach (ObjectDef child in inst.Object.Children)
                    {
                        if (!child.IsLightInstance())
                            continue;
                        Instance6 childInst = new Instance6(child.MyScene, child, IDEData);
                        fpFile.WriteLine(String.Format("{0}, {1}", inst.Index, childInst.ToIPLLightInst()));
                    }
                }
#endif // false
                fpFile.WriteLine(TAG_END);
                fpFile.WriteLine(TAG_MAXLOD);
                fpFile.WriteLine(String.Format("{0}", maxLodDistance));
                fpFile.WriteLine(TAG_END);

                fpFile.Close();
            }
        }

        /// <summary>
        /// Write IPL Group information to an IPL file.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="iplContainer"></param>
        protected virtual void WriteIPLGroup(String filename, GroupIPL6Container groupContainer)
        {
            // Write Group Static IPL file
            String staticFilename = String.Format("{0}_static.ipl",
                Path.GetFileNameWithoutExtension(filename));
            staticFilename = Path.Combine(Path.GetDirectoryName(filename), staticFilename);
            Log.Log__Message("Writing {0}...", staticFilename);
            using (StreamWriter fpFile = File.CreateText(staticFilename))
            {
                int instIdx = 0;
                float maxLodDistance = 0.0f;

                fpFile.WriteLine("# Static IPL Group generated from SceneXml files:");
                foreach (Scene scene in this.Scenes)
                    fpFile.WriteLine("#\t{0}", Path.GetFileName(scene.Filename), Path.GetFileName(scene.ExportFilename));
                fpFile.WriteLine(TAG_VERS);
                fpFile.WriteLine(this.Version);
                fpFile.WriteLine(TAG_END);
                WriteBoundsSection(fpFile, groupContainer.Static);
                fpFile.WriteLine(TAG_INST);
                foreach (Instance6 inst in groupContainer.Static.Instances)
                {
                    // Skip non-static objects.
                    inst.Index = instIdx;
                    if ((inst.StreamMode != InstanceBase.StreamModeType.NotStreamed) && !this.SingleFile)
                        continue;
                    Debug.Assert(!inst.Object.DontExport(), "Object set to not export!");
                    Debug.Assert(!inst.Object.DontExportIPL(), "Object set to not add to IPL!");

                    for (int i = 0; i < inst.Indent; ++i)
                        fpFile.Write("\t");
                    fpFile.WriteLine(inst.ToIPLInst(this.BlockNames, true));

                    if (inst.LODDistance > maxLodDistance)
                        maxLodDistance = inst.LODDistance;

                    if (inst.Object.HasLODChildren())
                    {
                        foreach (TargetObjectDef child in inst.Object.LOD.Children)
                        {
                            Instance6 childInst = groupContainer.Find(child);
                            if (null == childInst)
                            {
                                Debug.Assert(false, "Internal error.");
                                continue;
                            }
                            childInst.LODParentIndex = instIdx;
                        }
                    }
                    instIdx += 1;
                }
                fpFile.WriteLine(TAG_END);
                fpFile.WriteLine(TAG_MAXLOD);
                fpFile.WriteLine(String.Format("{0}", maxLodDistance));
                fpFile.WriteLine(TAG_END);
                fpFile.Close();
            }

            // Write Group Streamed IPL file
            Log.Log__Message("Writing {0}...", filename);
            using (StreamWriter fpFile = File.CreateText(filename))
            {
                float maxLodDistance = 0.0f;

                fpFile.WriteLine("# Streamed IPL Group generated from SceneXml files:");
                foreach (Scene scene in this.Scenes)
                    fpFile.WriteLine("#\t{0}", Path.GetFileName(scene.Filename), Path.GetFileName(scene.ExportFilename));
                fpFile.WriteLine(TAG_VERS);
                fpFile.WriteLine(this.Version);
                fpFile.WriteLine(TAG_END);
                WriteBoundsSection(fpFile, groupContainer.Streamed);
                fpFile.WriteLine(TAG_INST);
                foreach (Instance6 inst in groupContainer.Streamed.Instances)
                {
                    if (inst.LODDistance > maxLodDistance)
                        maxLodDistance = inst.LODDistance;
                    fpFile.WriteLine(inst.ToIPLInst(this.BlockNames, true));
                }
                fpFile.WriteLine(TAG_END);
                fpFile.WriteLine(TAG_MAXLOD);
                fpFile.WriteLine(String.Format("{0}", maxLodDistance));
                fpFile.WriteLine(TAG_END);
                fpFile.Close();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="fpFile"></param>
        /// <param name="container"></param>
        protected virtual void WriteBoundsSection(StreamWriter fpFile, IPL6ContainerBase container)
        {
            fpFile.WriteLine(TAG_BOUND);
            if (container is StreamedIPL6Container)
            {
                StreamedIPL6Container cont = (container as StreamedIPL6Container);
                BoundingBox3f bboxInsts;
                GetBoundsForInstances(cont.Instances, out bboxInsts);
                if (!bboxInsts.IsEmpty)
                {
                    fpFile.WriteLine("0, {0}, {1}, {2}, {3}, {4}, {5}",
                        bboxInsts.Min.X, bboxInsts.Min.Y, bboxInsts.Min.Z,
                        bboxInsts.Max.X, bboxInsts.Max.Y, bboxInsts.Max.Z);
                }
                BoundingBox3f bboxCars;
                GetBoundsForInstances(cont.CarGens, out bboxCars);
                if (!bboxCars.IsEmpty)
                {
                    fpFile.WriteLine("1, {0}, {1}, {2}, {3}, {4}, {5}",
                        bboxCars.Min.X, bboxCars.Min.Y, bboxCars.Min.Z,
                        bboxCars.Max.X, bboxCars.Max.Y, bboxCars.Max.Z);
                }
            }
            else if (container is StaticIPL6Container)
            {
                StaticIPL6Container cont = (container as StaticIPL6Container);
                BoundingBox3f bboxInsts;
                GetBoundsForInstances(cont.Instances, out bboxInsts);
                if (!bboxInsts.IsEmpty)
                {
                    fpFile.WriteLine("0, {0}, {1}, {2}, {3}, {4}, {5}",
                        bboxInsts.Min.X, bboxInsts.Min.Y, bboxInsts.Min.Z,
                        bboxInsts.Max.X, bboxInsts.Max.Y, bboxInsts.Max.Z);
                }
                BoundingBox3f bboxCars;
                GetBoundsForInstances(cont.CarGens, out bboxCars);
                if (!bboxCars.IsEmpty)
                {
                    fpFile.WriteLine("1, {0}, {1}, {2}, {3}, {4}, {5}",
                        bboxCars.Min.X, bboxCars.Min.Y, bboxCars.Min.Z,
                        bboxCars.Max.X, bboxCars.Max.Y, bboxCars.Max.Z);
                }
            }
            fpFile.WriteLine(TAG_END);
        }

        /// <summary>
        /// Expand a bounding box.
        /// </summary>
        /// <param name="inst"></param>
        /// <param name="localbox"></param>
        /// <param name="bbox"></param>        
        protected virtual void ExpandBoundsForInstance(Instance6 inst, BoundingBox3f localbox, ref BoundingBox3f bbox)
        {
            if (null != inst.Object.WorldBoundingBox)
                bbox.Expand(inst.Object.WorldBoundingBox);
            else
                bbox.Expand(inst.Object.NodeTransform.Translation);
        }

        /// <summary>
        /// Return bounding box information for a list of instances.
        /// </summary>
        /// <param name="insts"></param>
        /// <param name="bbox"></param>
        protected virtual void GetBoundsForInstances(List<Instance6> insts, out BoundingBox3f bbox)
        {
            bbox = new BoundingBox3f(); 
            foreach (Instance6 inst in insts)
            {
                String objName = inst.Object.GetObjectName();
                if ((inst.Object.IsXRef() || inst.Object.IsRefObject() || inst.Object.IsInternalRef() || inst.Object.IsRefInternalObject()) && !inst.Object.IsAnimProxy())
                {
                    if (IDEData.HasBoundingBox(objName))
                    {
                        BoundingBox3f localBound = IDEData.GetBoundingBox(objName);
                        ExpandBoundsForInstance(inst, localBound, ref bbox);
                    }
                    else
                    {
                        String message = String.Format("Error: please update your independent data, missing IDE files for IPL bounds generation ({0}).",
                            objName);
                        Debug.Assert(false, message);
                        Log.Log__Warning(message);
                        bbox.Expand(inst.Object.NodeTransform.Translation);
                    }
                }
                else
                {
                    ExpandBoundsForInstance(inst, inst.Object.LocalBoundingBox, ref bbox);
                } 
            }
        }

        /// <summary>
        /// Return bounding box information for a tree of instances.
        /// </summary>
        /// <param name="insts"></param>
        /// <param name="bbox"></param>
        protected virtual void GetBoundsForInstances(List<Tree<Instance6>> insts, out BoundingBox3f bbox)
        {
            bbox = new BoundingBox3f();
            foreach (Tree<Instance6> tree in insts)
            {
                foreach (TreeNode<Instance6> node in tree.DepthFirstSearch)
                {
                    Instance6 inst = node.Value;
                    String objName = inst.Object.GetObjectName();
                    if ((inst.Object.IsXRef() || inst.Object.IsRefObject()) && 
                        !inst.Object.IsAnimProxy() &&
                        !inst.Object.IsMiloTri())
                    {
                        if (IDEData.HasBoundingBox(objName))
                        {
                            BoundingBox3f localBound = IDEData.GetBoundingBox(objName);
                            ExpandBoundsForInstance(inst, localBound, ref bbox);
                        }
                        else
                        {
                            String message = String.Format("Missing object definition for '{0}'.  Update your export data, outdated IDE files for IPL bounds generation.",
                                objName);
                            Debug.Assert(false, message);
                            Log.Log__WarningCtx(inst.Object.Name, message);
                            bbox.Expand(inst.Object.NodeTransform.Translation);
                        }
                    }
                    else
                    {
                        ExpandBoundsForInstance(inst, inst.Object.LocalBoundingBox, ref bbox);
                    }
                }
            }
        }
        /// <summary>
        /// Process all of our scene objects, adding them to required containers.
        /// </summary>  
        protected virtual void ProcessSceneObjects()
        {
            // We unfortunately do a three passes to weed out all of the 
            // different LOD hierarchy objects.
            foreach (Scene scene in this.Scenes)
                foreach (TargetObjectDef obj in scene.Objects)
                    ProcessSceneObjectOnly(obj, ObjectDef.InstanceType.SLOD);
            foreach (Scene scene in this.Scenes)
                foreach (TargetObjectDef obj in scene.Objects)
                    ProcessSceneObjectOnly(obj, ObjectDef.InstanceType.LOD);
            foreach (Scene scene in this.Scenes)
                foreach (TargetObjectDef obj in scene.Objects)
                    ProcessSceneObjectOnly(obj, ObjectDef.InstanceType.HD);

            // Process non-Gta Object instances...
            foreach (Scene scene in this.Scenes)
            {
                foreach (TargetObjectDef o in scene.Objects)
                {
                    if (o.DontExport())
                        continue;
                    if (o.DontExportIPL())
                        continue;

                    // gunnard: Drawable LOD states don't ever need to go into the IDE.
                    if (o.HasDrawableLODChildren() || o.HasRenderSimParent())
                        continue;

                    ProcessSceneObject(o);
                }
            }
        }

        /// <summary>
        /// Process a single Gta Object.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="type"></param>
        protected void ProcessSceneObjectOnly(TargetObjectDef obj, ObjectDef.InstanceType type)
        {
            if (obj.DontExport() || obj.DontExportIPL())
                return;
            if ((type != obj.GetInstanceType()) && (!obj.IsContainer()) && (!obj.IsPropGroup()))
                return;
            // GunnarD: url:bugstar:99343 Avoid container children getting processed, too.
            if (obj.HasDrawableLODChildren() || obj.HasRenderSimParent())
                return;

            if (obj.IsObject())
            {
                // We attempt to find a previous Instance6 object for this ObjectDef.
                TreeNode<Instance6> instanceNode = this.StaticIPL.FindInstance(obj);
                Instance6 inst = null;
                if (null == instanceNode)
                    inst = new Instance6(obj.MyScene, obj, this.IDEData);
                else
                    inst = instanceNode.Value;

                if (this.InstanceBounds == null)
                {
                    this.InstanceBounds = new RSG.Base.Math.BoundingBox3f(inst.Object.NodeTransform.Translation, inst.Object.NodeTransform.Translation);
                }
                else
                {
                    this.InstanceBounds.Expand(inst.Object.NodeTransform.Translation);
                }

                if (this.SingleFile)
                {
                    this.StaticIPL.Instances.Add(new Tree<Instance6>(new TreeNode<Instance6>(inst)));
                }
                else
                {
                    switch (inst.InstType)
                    {
                        case ObjectDef.InstanceType.HD:
                            {
                                String iplGroup = obj.GetAttribute(AttrNames.OBJ_IPL_GROUP, AttrDefaults.OBJ_IPL_GROUP).ToLower();
                                bool iplGroupSet = (0 != String.Compare(iplGroup, AttrDefaults.OBJ_IPL_GROUP, true));
                                if (iplGroupSet)
                                {
                                    // Ensure the group exists.
                                    if (!this.IPLGroups.ContainsKey(iplGroup))
                                        this.IPLGroups.Add(iplGroup, new GroupIPL6Container(iplGroup));
                                    GroupIPL6Container group = this.IPLGroups[iplGroup];

                                    // Add instance to bucket.
                                    group.Streamed.Instances.Add(inst);
                                }
                                else
                                {
                                    if (obj.HasLODParent())
                                    {
                                        // Find LOD parent and add as child.
                                        TreeNode<Instance6> parent = this.StaticIPL.FindInstance(obj.LOD.Parent);
                                        if (null == parent)
                                        {
                                            String message = String.Format("[art] Didn't find LOD parent instance for {0}.  LOD Parent set to Dont Export or Dont Add to IPL?", obj.ToString());
                                            Debug.Assert(null != parent, message);
                                            Log.Log__Error(message);
                                        }
                                        else
                                        {
                                            // Set indentation
                                            if (parent.Value.Object.HasLODParent())
                                                inst.Indent = 2;
                                            else
                                                inst.Indent = 1;
                                            parent.Neighbours.Add(new TreeNode<Instance6>(inst));
                                        }
                                    }
                                    else
                                    {
                                        this.StaticIPL.Instances.Add(new Tree<Instance6>(new TreeNode<Instance6>(inst)));
                                    }
                                }
                            }
                            break;
                        case ObjectDef.InstanceType.LOD:
                            {
                                String iplGroup = obj.GetAttribute(AttrNames.OBJ_IPL_GROUP, AttrDefaults.OBJ_IPL_GROUP).ToLower();
                                bool iplGroupSet = (0 != String.Compare(iplGroup, AttrDefaults.OBJ_IPL_GROUP, true));
                                if (iplGroupSet)
                                {
                                    // Ensure the group exists.
                                    if (!this.IPLGroups.ContainsKey(iplGroup))
                                        this.IPLGroups.Add(iplGroup, new GroupIPL6Container(iplGroup));
                                    GroupIPL6Container group = this.IPLGroups[iplGroup];

                                    // Add instance to bucket and set indentation.
                                    if (obj.HasLODParent())
                                        inst.Indent = 1;
                                    group.Static.Instances.Add(inst);
                                }
                                else
                                {
                                    // LOD instances go below their parent.
                                    if (obj.HasLODParent())
                                    {
                                        // Find SLOD parent and add as child.
                                        TreeNode<Instance6> parent = this.StaticIPL.FindInstance(obj.LOD.Parent);
                                        if (null == parent)
                                        {
                                            String message = String.Format("[art] Didn't find SLOD parent instance for {0}.  LOD Parent set to Dont Export or Dont Add to IPL?", obj.ToString());
                                            Debug.Assert(null != parent, message);
                                            Log.Log__Error(message);
                                        }
                                        else
                                        {
                                            // Set indentation
                                            inst.Indent = 1;
                                            parent.Neighbours.Add(new TreeNode<Instance6>(inst));
                                        }
                                    }
                                    else
                                    {
                                        this.StaticIPL.Instances.Add(new Tree<Instance6>(new TreeNode<Instance6>(inst)));
                                    }
                                }
                            }
                            break;
                        case ObjectDef.InstanceType.SLOD:
                            {
                                String iplGroup = obj.GetAttribute(AttrNames.OBJ_IPL_GROUP, AttrDefaults.OBJ_IPL_GROUP).ToLower();
                                bool iplGroupSet = (0 != String.Compare(iplGroup, AttrDefaults.OBJ_IPL_GROUP, true));
                                if (iplGroupSet)
                                {
                                    // Instance is within an IPL Group.
                                    if (!this.IPLGroups.ContainsKey(iplGroup))
                                        this.IPLGroups.Add(iplGroup, new GroupIPL6Container(iplGroup));
                                    GroupIPL6Container group = this.IPLGroups[iplGroup];
                                    group.Static.Instances.Add(inst);
                                }
                                else
                                {
                                    // SLOD instances always go on the top of the tree.
                                    this.StaticIPL.Instances.Add(new Tree<Instance6>(new TreeNode<Instance6>(inst)));
                                }
                            }
                            break;
                        default:
                            Debug.Assert(false, "Unsupported InstanceType.  Contact tools.");
                            break;
                    }
                }

                // Verify LOD hierarchy objects aren't set to Dont Export
                // or Dont Add To IPL; check immediate parent and children.
                if (obj.HasLODParent())
                {
                    TargetObjectDef parent = obj.LOD.Parent;
                    if (parent.DontExport() || parent.DontExportIPL())
                    {
                        String message = String.Format("[art] Object {0}'s LOD parent ({1}) is set to 'Dont Export' or 'Dont Add to IPL' but it is in a LOD hierarchy.",
                            obj.Name, parent.Name);
                        Debug.Assert(false, message);
                        Log.Log__ErrorCtx(parent.Name, message);
                    }
                }
            }
            else if (obj.IsPropGroup() && 
                !obj.GetAttribute(AttrNames.PROPGROUP_DONT_EXPORT, AttrDefaults.PROPGROUP_DONT_EXPORT))
            {
                // We process RS PropGroup children.  This is currently how
                // the environment guys are getting around the issue with
                // XRefs not being supported in 3dsmax 2010 Containers.
            }
            else if (obj.IsDummy())
            {
                // Don't process Dummy children.  This is how 3dsmax Groups
                // are implemented and these aren't exported.
                return;
            }

            // Iterate through children.
            foreach (TargetObjectDef o in obj.Children)
                ProcessSceneObjectOnly(o, type);
        }

        /// <summary>
        /// Process a single top-level SceneXml ObjectDef.  This recurses into
        /// its children if required.
        /// </summary>
        /// <seealso cref="ProcessSceneObjectOnly"/>
        /// <param name="obj">ObjectDef to process</param>
        /// Note: we don't need to add instances here to the StaticIPL as they are
        /// already added.
        protected void ProcessSceneObject(TargetObjectDef obj)
        {
            if (obj.DontExport() || obj.DontExportIPL())
                return;

            // GunnarD: url:bugstar:99343 Avoid container children getting processed, too.
            if (obj.HasDrawableLODChildren() || obj.HasRenderSimParent())
                return;

            // Add instance to the correct container.
            if (obj.IsCollision() || obj.IsMAXGroup())
            {
                return; // Skipped
            }
            else if (obj.IsMiloTri())
            {
                Instance6 instance = new Instance6(obj.MyScene, obj, this.IDEData);
                this.StaticIPL.MiloInstances.Add(new Tree<Instance6>(new TreeNode<Instance6>(instance)));
            }
            else if (obj.IsObject())
            {
                // We attempt to find a previous Instance6 object for this ObjectDef.
                TreeNode<Instance6> instanceNode = this.StaticIPL.FindInstance(obj);
                Instance6 instance = null;
                if (null == instanceNode)
                    instance = new Instance6(obj.MyScene, obj, this.IDEData);
                else
                    instance = instanceNode.Value;

                String iplGroup = obj.GetAttribute(AttrNames.OBJ_IPL_GROUP, AttrDefaults.OBJ_IPL_GROUP).ToLower();
                bool iplGroupSet = (0 != String.Compare(iplGroup, "", true)) &&
                    (0 != String.Compare(iplGroup, AttrDefaults.OBJ_IPL_GROUP, true));

                if (iplGroupSet)
                {
                    Debug.Assert(!this.SingleFile, "Attempting to create single IPL from data containing IPL Groups.  Contact tools.");
                    // Instance is within an IPL Group.
                    if (!this.IPLGroups.ContainsKey(iplGroup))
                        this.IPLGroups.Add(iplGroup, new GroupIPL6Container(iplGroup));
                    GroupIPL6Container group = this.IPLGroups[iplGroup];
                    switch (instance.InstType)
                    {
                        case ObjectDef.InstanceType.SLOD:
                            if (!group.Static.Instances.Contains(instance))
                                group.Static.Instances.Add(instance);
                            break;
                        case ObjectDef.InstanceType.LOD:
                            if (!group.Static.Instances.Contains(instance))
                                group.Static.Instances.Add(instance);
                            break;
                        case ObjectDef.InstanceType.HD:
                            if (!group.Streamed.Instances.Contains(instance))
                                group.Streamed.Instances.Add(instance);
                            break;
                    }
                }
                else
                {
                    // Instance is not in an IPL Group so we just add to the relevant
                    // IPL file pool (static, streamed, or streamed big).
                    switch (instance.StreamMode)
                    {
                        case Instance6.StreamModeType.NotStreamed:
                            // Setup indentation.
                            switch (obj.GetInstanceType())
                            {
                                case ObjectDef.InstanceType.SLOD:
                                    instance.Indent = 0;
                                    break;
                                case ObjectDef.InstanceType.LOD:
                                    if (obj.HasLODParent())
                                        instance.Indent = 1;
                                    break;
                                case ObjectDef.InstanceType.HD:
                                    if (obj.HasLODParent() && obj.LOD.Parent.HasLODParent())
                                        instance.Indent = 2;
                                    else if (obj.HasLODParent())
                                        instance.Indent = 1;
                                    break;
                                default:
                                    Debug.Assert(false);
                                    break;
                            }
                            break;
                        case Instance6.StreamModeType.Streamed:
                            this.SceneStreamedInstances.Instances.Add(instance);
                            break;
                        case Instance6.StreamModeType.StreamedBig:
                            this.SceneStreamedBigInstances.Instances.Add(instance);
                            break;
                    }
                }
            }
            else if (obj.IsAnimProxy())
            {
                TreeNode<Instance6> instanceNode = this.StaticIPL.FindInstance(obj);
                Instance6 inst = null;
                if (null == instanceNode)
                    inst = new Instance6(obj.MyScene, obj, this.IDEData);
                else
                    inst = instanceNode.Value;
                this.StaticIPL.Instances.Add(new Tree<Instance6>(new TreeNode<Instance6>(inst)));
            }
            else if (obj.IsBlock())
            {
                Instance6 instance = new Instance6(obj.MyScene, obj, this.IDEData);
                this.StaticIPL.Blocks.Add(instance);
            }
            else if (obj.IsZone())
            {
                Debug.Assert(false, "Not implemented.  Contact tools.");
            }
            else if (obj.IsPickup())
            {
                Debug.Assert(false, "Not implemented.  Contact tools.");
            }
            else if (obj.IsGarageArea())
            {
                Instance6 instance = new Instance6(obj.MyScene, obj, this.IDEData);
                this.StaticIPL.GarageAreas.Add(instance);
            }
            else if (obj.IsCarGen())
            {
                Instance6 instance = new Instance6(obj.MyScene, obj, this.IDEData);
                if (!this.SingleFile)
                    this.SceneStreamedInstances.CarGens.Add(instance);
            }
            else if (obj.IsTimeCycleBox())
            {
                Instance6 instance = new Instance6(obj.MyScene, obj, this.IDEData);
                this.StaticIPL.TimeCycles.Add(instance);
            }
            else if (obj.Is2dfxScript())
            {
                // AJM: Removed the old check for 2dfx object with no parent as they were being
                // added to the ipl, causing asserts at load time
                // Gta Script needs to use 2dfx at the scene root, so add to ipl if it's a 2dfxScript
                Instance6 instance = new Instance6(obj.MyScene, obj, this.IDEData);
                this.StaticIPL.Twodfx.Add(instance);
            }
            else if (obj.Is2dfxSpawnPoint())
            {
                Instance6 instance = new Instance6(obj.MyScene, obj, this.IDEData);
                this.StaticIPL.Twodfx.Add(instance);
            }
            else if (obj.Is2dfx())
            {
                // We ignore parented 2dfx as they are instanced in the IDE file.
                // This removes a warning about them.
            }
            else if (obj.IsMiloAssoc())
            {
                Instance6 instance = new Instance6(obj.MyScene, obj, this.IDEData);
                this.StaticIPL.MiloPlus.Add(instance);
            }
            else if (obj.IsVehicleNode())
            {
                Instance6 instance = new Instance6(obj.MyScene, obj, this.IDEData);
                this.StaticIPL.VehicleNodes.Add(instance);
            }
            else if (obj.IsVehicleLink())
            {
                Instance6 instance = new Instance6(obj.MyScene, obj, this.IDEData);
                this.StaticIPL.VehicleLinks.Add(instance);
            }
            else if (obj.IsPatrolNode())
            {
                Instance6 instance = new Instance6(obj.MyScene, obj, this.IDEData);
                this.StaticIPL.PatrolNodes.Add(instance);
            }
            else if (obj.IsPatrolLink())
            {
                Instance6 instance = new Instance6(obj.MyScene, obj, this.IDEData);
                this.StaticIPL.PatrolLinks.Add(instance);
            }
            else if (obj.IsSlowZone())
            {
                Instance6 instance = new Instance6(obj.MyScene, obj, this.IDEData);
                this.StaticIPL.SlowZones.Add(instance);
            }
            else if (obj.IsPropGroup())
            {
                // We process RS PropGroup children.  This is currently how
                // the environment guys are getting around the issue with
                // XRefs not being supported in 3dsmax 2010 Containers.
                foreach (TargetObjectDef child in obj.Children)
                    ProcessSceneObject(child);
            }
            else if (obj.IsDummy())
            {
                Log.Log__Warning("Ignoring Dummy {0} as this is how 3dsmax represents Groups.", obj);
                return;
            }
            else if (obj.IsContainer())
            {
                //block details filled in from here...
                // Add initial unused block.
                Instance6 instance = new Instance6(obj.MyScene, obj, this.IDEData);
                Vector3f min = new Vector3f();
                Vector3f max = new Vector3f();
                // DHM HACK; this instance bounds stuff is going to have to change
                // soon for a bug about not following references anyways.
                if (null == this.InstanceBounds)
                {
                    this.InstanceBounds = new BoundingBox3f();
                    this.InstanceBounds.Expand(obj.NodeTransform.Translation);
                    min = this.InstanceBounds.Min;
                    max = this.InstanceBounds.Max;
                }
                else
                {
                    min = obj.NodeTransform * InstanceBounds.Min;
                    max = obj.NodeTransform * InstanceBounds.Max;
                }
                instance.Object.LocalBoundingBox = InstanceBounds;
                instance.Object.WorldBoundingBox = new BoundingBox3f(
                    new Vector3f(SMath.Min(min.X, max.X), SMath.Min(min.Y, max.Y), SMath.Min(min.Z, max.Z)),
                    new Vector3f(SMath.Max(min.X, max.X), SMath.Max(min.Y, max.Y), SMath.Max(min.Z, max.Z)));
                StaticIPL.Blocks.Add(instance);

                foreach (TargetObjectDef child in obj.Children)
                    ProcessSceneObject(child);
            }
            else if (obj.IsEditableSpline() || obj.IsText() || obj.IsLine())
            {
                // Ignored.
            }
            else
            {
                Log.Log__Warning("Ignoring {0} {1} as type not recognised by IPL6 serialiser.",
                    obj.Name, obj.Class);
            }

            // DHM Jimmy Bug #6462 highlighted this issue with Animated Objects.
            // We don't need to recurse because sub-meshes are part of the parent
            // drawable and we don't have an IDE or IPL entry for them.
            //foreach (ObjectDef child in obj.Children)
            //    ProcessSceneObject(child);
        }
        #endregion // Protected Methods
    }

} // RSG.SceneXml.MapExport namespace
