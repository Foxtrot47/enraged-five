//
// File: StreamInstSplitIPL6.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of StreamInstSplitIPL6.cs class
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using SMath = System.Math;
using RSG.Base.Collections;
using RSG.Base.Math;
using RSG.SceneXml.MapExport.Collections;

namespace RSG.SceneXml.MapExport.Algorithm
{

    using AreaTree6 = AreaBinaryTree<StreamedIPL6Container>;
    using AreaTreeNode6 = AreaBinaryTreeNode<StreamedIPL6Container>;

    /// <summary>
    /// Organise the 2D instances list of Instance6 objects as in IPL v6.
    /// </summary>
    /// The instance lists are limited based on area and trying to balance 
    /// the number of instances per file whilst limiting the number of files.
    /// (as used in IPL v6, i.e. at time of writing Jimmy).
    internal class StreamInstSplitIPL6
    {
        #region Constants
        /// <summary>
        /// Maximum number of objects per streamed-IPL file.
        /// </summary>
        private readonly int MaxObjectsPerStreamIPL = 400;
        #endregion // Constants

        #region Enumerations
        /// <summary>
        /// Axis enumeration used during axis split.
        /// </summary>
        private enum SplitAxis
        {
            XAxis,  // Split tree node across X-Axis
            YAxis   // Split tree node across Y-Axis
        }
        #endregion // Enumerations

        #region Controller Methods
        /// <summary>
        /// Organise the 2D instances List of Instance4 objects based on area 
        /// and a maximum number of Instances per node.
        /// </summary>
        /// <param name="iplContainer"></param>
        /// <returns></returns>
        public AreaTree6 Split(StreamedIPL6Container iplContainer)
        {
            BoundingBox2f initialArea = CalcInstancesArea(iplContainer.Instances);
            AreaTree6 tree = new AreaTree6();
            tree.Root = new AreaTreeNode6(initialArea, iplContainer);

            AreaTreeNode6 largest = null;
            int nNumIPLs = (iplContainer.Instances.Count + (MaxObjectsPerStreamIPL - 1)) / MaxObjectsPerStreamIPL;
            while (nNumIPLs > 1)
            {
                // Find node with largest number of instances, and split it.
                foreach (AreaTreeNode6 node in tree.Walk(AreaTree6.WalkMode.DepthFirst))
                {
                    if ((null != largest) && (node.Value.Instances.Count > largest.Value.Instances.Count))
                        largest = node;
                    else if (null == largest)
                        largest = node;
                }

                SplitNode(largest);
                largest = null;

                // Each time we split we have made an IPL file.
                --nNumIPLs;
            }
            return (tree);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Split a AreaBinaryTreeNode.
        /// </summary>
        /// <param name="node"></param>
        private void SplitNode(AreaTreeNode6 node)
        {
            // Check we have Instances in our node list.
            Debug.Assert(node.Value.Instances.Count > 0, "The Tree Node to split has no Instances!");

            // Check we haven't already split this tree node.
            Debug.Assert(null == node.Left, "Attempting to split already split Tree Node.");
            Debug.Assert(null == node.Right, "Attempting to split already split Tree Node.");

            // Check we have an area defined for this node.
            Debug.Assert(null != node.Area, "Area is not pre-calculated for the Tree Node to be split.");

            // Determine split axis
            BoundingBox2f[] splits = null;
            SplitAxis axis = CalcSplitAxisAndAreas(node, out splits);
            SplitInstances(node, splits);

            int total = node.Left.Value.Instances.Count + node.Right.Value.Instances.Count;
            Debug.Assert(total == node.Value.Instances.Count, "Split inconsistency, left and right mismatch the total instance count.");
            node.Value.Clear();
        }

        /// <summary>
        /// Calculate the split axis, and areas for a tree node.
        /// </summary>
        /// <param name="node"></param>
        /// <param name="splits"></param>
        /// <returns></returns>
        private SplitAxis CalcSplitAxisAndAreas(AreaTreeNode6 node, out BoundingBox2f[] splits)
        {
            // Calculate axis to split, simply based on which dimension is
            // largest.
            SplitAxis axis = SplitAxis.XAxis;
            float diffX = node.Area.Max.X - node.Area.Min.X;
            float diffY = node.Area.Max.Y - node.Area.Min.Y;

            if (diffY > diffX)
                axis = SplitAxis.YAxis;

            // Calculate our split areas, by calculating the average position
            // for the split axis only (determining where along that axis we
            // make the split).
            float average = 0.0f;
            float value = 0.0f;
            foreach (Instance4 inst in node.Value.Instances)
            {
                switch (axis)
                {
                    case SplitAxis.XAxis:
                        value = inst.Object.NodeTransform.Translation.X - node.Area.Min.X;
                        break;
                    case SplitAxis.YAxis:
                        value = inst.Object.NodeTransform.Translation.Y - node.Area.Min.Y;
                        break;
                }
                //Debug.Assert(value >= 0.0f, "Internal error during average value calculation.");
                average += value;
            }
            average /= node.Value.Instances.Count;

            // Define our split areas.
            splits = new BoundingBox2f[2];
            switch (axis)
            {
                case SplitAxis.XAxis:
                    // Left Split
                    splits[0] = new BoundingBox2f(
                        new Vector2f(node.Area.Min),                                   // Min (left, bottom)
                        new Vector2f(node.Area.Max.X - average, node.Area.Max.Y));     // Max (right, top)
                    // Right Split
                    splits[1] = new BoundingBox2f(
                        new Vector2f(node.Area.Max.X - average, node.Area.Min.Y),      // Min (left, bottom)
                        new Vector2f(node.Area.Max));                                  // Max (right, top)
                    break;
                case SplitAxis.YAxis:
                    // Bottom Split
                    splits[0] = new BoundingBox2f(
                        new Vector2f(node.Area.Min),                                   // Min (left, bottom)
                        new Vector2f(node.Area.Max.X, node.Area.Max.Y - average));     // Max (right, top)
                    // Top Split
                    splits[1] = new BoundingBox2f(
                        new Vector2f(node.Area.Min.X, node.Area.Max.Y - average),      // Min (left, bottom)
                        new Vector2f(node.Area.Max));                                  // Max (right, top)
                    break;
            }

            return (axis);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <param name="splits"></param>
        private void SplitInstances(AreaTreeNode6 node, BoundingBox2f[] splits)
        {
            // Create our new tree nodes ready for population.
            node.Left = new AreaTreeNode6(splits[0], new StreamedIPL6Container());
            node.Right = new AreaTreeNode6(splits[1], new StreamedIPL6Container());

            // Loop through each instance placing it in the correct node list.
            foreach (Instance6 inst in node.Value.Instances)
            {
                Vector3f pos3 = inst.Object.NodeTransform.Translation;
                Vector2f pos = new Vector2f(pos3.X, pos3.Y);

                if (splits[0].Contains(pos))
                    node.Left.Value.Instances.Add(inst);
                else
                    node.Right.Value.Instances.Add(inst);
            }
            foreach (Instance6 inst in node.Value.CarGens)
            {
                Vector3f pos3 = inst.Object.NodeTransform.Translation;
                Vector2f pos = new Vector2f(pos3.X, pos3.Y);

                if (splits[0].Contains(pos))
                    node.Left.Value.CarGens.Add(inst);
                else
                    node.Right.Value.CarGens.Add(inst);
            }
            foreach (Instance6 inst in node.Value.MiloPlus)
            {
                Vector3f pos3 = inst.Object.NodeTransform.Translation;
                Vector2f pos = new Vector2f(pos3.X, pos3.Y);

                if (splits[0].Contains(pos))
                    node.Left.Value.MiloPlus.Add(inst);
                else
                    node.Right.Value.MiloPlus.Add(inst);
            }
        }

        /// <summary>
        /// Calculate the bounds of a List of Instances.
        /// </summary>
        /// <param name="instances">Instance4 object list</param>
        /// <returns></returns>
        private BoundingBox2f CalcInstancesArea(IEnumerable<Instance6> instances)
        {
            BoundingBox2f area = new BoundingBox2f();
            foreach (Instance6 inst in instances)
            {
                if (!inst.Object.IsObject())
                    continue;
                Vector3f pos3 = inst.Object.NodeTransform.Translation;
                Vector2f pos = new Vector2f(pos3.X, pos3.Y);
                area.Expand(pos);
            }
            return (area);
        }
        #endregion // Private Methods
    }

} // RSG.SceneXml.MapExport.Algorithm

// StreamInstSplitIPL6.cs
