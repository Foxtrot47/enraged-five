﻿using System;
using System.Collections.Generic;
using RSG.SceneXml.MapExport.Framework.Collections;

namespace RSG.SceneXml.MapExport.Algorithm
{

    /// <summary>
    /// 
    /// </summary>
    public interface IIMAPSplitAlgorithm<ContainerClass>
        where ContainerClass : IMAPContainer, new()
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="prefix"></param>
        /// <param name="container"></param>
        /// <param name="containers"></param>
        /// <param name="warnMissingCarGens"></param>
        /// <param name="streamMode">Stream mode to include for HD objects.</param>
        void Split(String prefix, ContainerClass container, out List<ContainerClass> containers, bool warnMissingCarGens, Entity.StreamModeType streamMode);
    }

} // RSG.SceneXml.MapExport.Algorithm namespace
