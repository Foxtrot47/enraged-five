﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

using RSG.Base;
using RSG.Base.Math;

namespace RSG.SceneXml.MapExport
{
    /// <summary>
    /// Base Instanced Entity
    /// </summary>
    public class InstancedEntity :
        ExtensionContainer
    {
        #region Properties
        public String ArchetypeName
        {
            get;
            set;
        }

        public String HighDetailArchetypeName
        {
            get;
            set;
        }

        public Vector3f Position
        {
            get;
            set;
        }

        public Quaternionf Rotation
        {
            get;
            private set;
        }

        public float ScaleXY
        {
            get;
            private set;
        }

        public float ScaleZ
        {
            get;
            private set;
        }

        public UInt16 CompressedX
        {
            get;
            private set;
        }

        public UInt16 CompressedY
        {
            get;
            private set;
        }

        public UInt16 CompressedZ
        {
            get;
            private set;
        }

        public Plane GroundPlane
        {
            get;
            private set;
        }

        #endregion Properties

        #region Constructors
        public InstancedEntity(String archetype, String highDetailArchetype, Plane plane, Vector3f pos, Quaternionf rot, float scaleXY, float scaleZ)
        {
            ArchetypeName = archetype;
            HighDetailArchetypeName = highDetailArchetype;
            GroundPlane = plane;
            Position = pos;
            Rotation = rot;
            ScaleXY = scaleXY;
            ScaleZ = scaleZ;

            // These are calculated later once we know the entity lists bounding box.
            CompressedX = 0;
            CompressedY = 0;
            CompressedZ = 0;
        }

        public void CalculateCompressedPosition(BoundingBox3f boundingBox)
        {
            // Last failsafe to catch a bad entity position before it's quantized.
            // Because of rounding errors this will somtimes cause the entity to sit *just* outside the bounding box.
            if (!boundingBox.Contains(Position, true))
            {
                Position.X = Math.Min(boundingBox.Max.X, Math.Max(boundingBox.Min.X, Position.X));
                Position.Y = Math.Min(boundingBox.Max.Y, Math.Max(boundingBox.Min.Y, Position.Y));
                Position.Z = Math.Min(boundingBox.Max.Z, Math.Max(boundingBox.Min.Z, Position.Z));
            }

            Vector3f extents = (boundingBox.Max - boundingBox.Min);
            Vector3f transformedPosition = Position - boundingBox.Min;

            transformedPosition.X /= extents.X;
            transformedPosition.Y /= extents.Y;
            transformedPosition.Z /= extents.Z;

            transformedPosition *= UInt16.MaxValue;

            CompressedX = Convert.ToUInt16((uint)transformedPosition.X);
            CompressedY = Convert.ToUInt16((uint)transformedPosition.Y);
            CompressedZ = Convert.ToUInt16((uint)transformedPosition.Z);
        }
        #endregion Constructors
    }

    /// <summary>
    /// Grass-specific Instanced Entity
    /// </summary>
    public class InstancedGrassEntity :
        InstancedEntity
    {
        #region Properties
        public float RegionTint
        {
            get;
            private set;
        }

        public Vector4i TintColor
        {
            get;
            private set;
        }

        public Vector3f PlantColor
        {
            get;
            private set;
        }

        public Vector4i GroundColor
        {
            get;
            private set;
        }

        public Vector3f GroundNormal
        {
            get;
            private set;
        }

        public Vector4f MicroMovements
        {
            get;
            private set;
        }

        public byte AO
        {
            get;
            private set;
        }
        #endregion Properties

        #region Constructors
        public InstancedGrassEntity(string archetype, string highDetailArchetype, Vector3f pos, 
            Quaternionf rot, float scaleXY, float scaleZ, Plane plane, Vector4i color, Vector4i tintColor, byte ao)
            : base(archetype, highDetailArchetype, plane, pos, rot, scaleXY, scaleZ)
        {
            RegionTint = 0;
            GroundNormal = new Vector3f(plane.X, plane.Y, plane.Z);
            GroundColor = color;
            TintColor = tintColor;
            AO = ao;
        }
        #endregion Constructors
    }

    /// <summary>
    /// Prop-specific Instanced Entity
    /// </summary>
    public class InstancedPropEntity :
        InstancedEntity
    {
        #region Properties
        public int Index
        {
            get;
            set;
        }

        public float RegionTint
        {
            get;
            private set;
        }
        #endregion Properties

        #region Constructors
        public InstancedPropEntity(string archetype, string highDetailArchetype, Plane plane, Vector3f pos, Quaternionf rot, float scaleXY, float scaleZ)
            : base(archetype, highDetailArchetype, plane, pos, rot, scaleXY, scaleZ)
        {
            RegionTint = 0;
            Index = -1;
        }
        #endregion Constructors
    }

    /// <summary>
    /// Base abstract class for the entity list.
    /// </summary>
    public abstract class IInstancedEntityList : List<InstancedEntity>
    {
        public abstract void Process(BoundingBox3f localBoundingBox, float lodDistance, float lodFadeStartDistance, float lodInstFadeRange,
                                        float orientToTerrain, Vector3f scale, int brightness);
    }

    /// <summary>
    /// Class for the instanced entity list.
    /// </summary>
    public class InstancedEntityList : IInstancedEntityList
    {
        #region Private Properties
        private static Vector3f ZeroVector = new Vector3f(0.0f, 0.0f, 0.0f);
        private static BoundingBox3f ZeroBoundingBox = new BoundingBox3f(ZeroVector, ZeroVector);
        private static BoundingBox3f InitialBoundingBox = new BoundingBox3f();
        #endregion  

        #region Properties
        /// <summary>
        /// This is the bounding box containing the entirety of the object 
        /// (including the object's bounding box).
        /// </summary>
        public BoundingBox3f BoundingBox
        {
            get;
            protected set;
        }

        /// <summary>
        /// This is the bounding box containing all points of the objects.
        /// This does not include the object's bounding box.
        /// </summary>
        public BoundingBox3f PositionBoundingBox
        {
            get;
            protected set;
        }

        /// <summary>
        /// The archetype's local bounding box.  
        /// </summary>
        public BoundingBox3f ArchetypeBoundingBox
        {
            get;
            protected set;
        }

        /// <summary>
        /// The distance at which to drop to the next level of detail.
        /// </summary>
        public float LODDistance
        {
            get;
            protected set;
        }

        /// <summary>
        /// The distance at which this entity list begins to fade.
        /// </summary>
        public float LODFadeStartDistance
        {
            get;
            protected set;
        }

        /// <summary>
        /// The distance at which this entity list begins to fade.
        /// </summary>
        public float LODInstFadeRange
        {
            get;
            protected set;
        }

        /// <summary>
        /// Value between 0-1 of how much we want to orient to the terrain.
        /// </summary>
        public float OrientToTerrain
        {
            get;
            protected set;
        }

        /// <summary>
        /// Minimum scale value for a batch so in-game we can compute an additive scale.
        /// </summary>
        public float MinScale
        {
            get;
            protected set;
        }

        /// <summary>
        /// Maximum scale value for a batch so in-game we can compute an additive scale.
        /// </summary>
        public float MaxScale
        {
            get;
            protected set;
        }

        /// <summary>
        /// Random scale amount applied to each instance.
        /// </summary>
        public float RandomScale
        {
            get;
            protected set;
        }

        /// <summary>
        /// Brightness value to affect the tint color.
        /// </summary>
        public int Brightness
        {
            get;
            protected set;
        }

        /// <summary>
        /// List colour; colour that comes from the definition colour.
        /// </summary>
        public Color ListColour
        {
            get;
            set;
        }
        #endregion

        #region Constructors
        public InstancedEntityList()
        {
            LODDistance = float.MinValue;
            LODFadeStartDistance = 8191.0f;
            PositionBoundingBox = null;
            BoundingBox = null;
            ArchetypeBoundingBox = null;
            OrientToTerrain = 0.0f;
            MinScale = 0.0f;
            MaxScale = 0.0f;
            RandomScale = 0.0f;
            Brightness = 0;
            ListColour = Color.White;
        }

        public InstancedEntityList(BoundingBox3f localBoundingBox, float lodDistance,
            float lodFadeStartDistance, float lodInstFadeRange, float orientToTerrain, Vector3f scale, int brightness)
        {
            LODDistance = lodDistance;
            LODFadeStartDistance = lodFadeStartDistance;
            LODInstFadeRange = lodInstFadeRange;
            PositionBoundingBox = GetBoundingBox(ZeroBoundingBox);
            BoundingBox = GetBoundingBox(localBoundingBox);
            ArchetypeBoundingBox = localBoundingBox;
            OrientToTerrain = orientToTerrain;
            MinScale = scale.X;
            MaxScale = scale.Y;
            RandomScale = scale.Z;
            Brightness = brightness;
            ListColour = Color.White;
        }
        #endregion

        #region Public Functions
        public BoundingBox3f GetBoundingBox(BoundingBox3f localArchetypeBB)
        {
            BoundingBox3f boundingBox = new BoundingBox3f();
            foreach (InstancedEntity entity in this)
            {
                Vector3f worldMinBB = localArchetypeBB.Min + entity.Position;
                Vector3f worldMaxBB = localArchetypeBB.Max + entity.Position;

                boundingBox.Expand(worldMinBB);
                boundingBox.Expand(worldMaxBB);
            }

            return boundingBox;
        }

        public override void Process(BoundingBox3f localBoundingBox, float lodDistance,
            float lodFadeStartDistance, float lodInstFadeRange, float orientToTerrain, Vector3f scale, int brightness)
        {
            LODDistance = lodDistance;
            LODFadeStartDistance = lodFadeStartDistance;
            LODInstFadeRange = lodInstFadeRange;
            PositionBoundingBox = GetBoundingBox(ZeroBoundingBox);
            ArchetypeBoundingBox = localBoundingBox;
            BoundingBox = GetBoundingBox(localBoundingBox);
            OrientToTerrain = orientToTerrain;
            MinScale = scale.X;
            MaxScale = scale.Y;
            RandomScale = scale.Z;
            Brightness = brightness;

            // Compress the positions.
            foreach (InstancedEntity entity in this)
                entity.CalculateCompressedPosition(BoundingBox);
        }

        /// <summary>
        /// This is *strictly* to be used when we need to update the entity names
        /// with a DLC friendly name.
        /// </summary>
        /// <param name="updatedName"></param>
        public void UpdateArchetypeNames(string updatedName)
        {
            foreach (InstancedEntity entity in this)
            {
                entity.ArchetypeName = updatedName;
                entity.HighDetailArchetypeName = updatedName;
            }
        }

        /// <summary>
        /// Randomly shuffles the list internally.
        /// This is used to gradually fade out a batch entity list.
        /// </summary>
        public void Shuffle()
        {
            Random rng = new Random();
            int n = this.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                InstancedEntity value = this[k];
                this[k] = this[n];
                this[n] = value;
            }
        }
        #endregion 
    }
}
