//
// File: SceneSerialiserIPLBase.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of SceneSerialiserIPLBase class
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using RSG.Base.Configuration;

namespace RSG.SceneXml.MapExport
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class SceneSerialiserIPLBase : MultipleSceneSerialiserMap
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// 
        /// </summary>
        public abstract int Version
        {
            get;
        }

        /// <summary>
        /// Build directory (for finding and parsing IDE files).
        /// </summary>
        public String BuildDir
        {
            get { return m_sBuildDir; }
            private set { m_sBuildDir = value; }
        }
        private String m_sBuildDir;

        /// <summary>
        /// Streamed IPL output directory.
        /// </summary>
        public String IPLStreamDir
        {
            get { return m_sIPLStreamDir; }
            private set { m_sIPLStreamDir = value; }
        }
        private String m_sIPLStreamDir;

        /// <summary>
        /// Export a single IPL file flag.
        /// </summary>
        public bool SingleFile
        {
            get { return m_bSingleFile; }
            private set { m_bSingleFile = value; }
        }
        private bool m_bSingleFile;

        /// <summary>
        /// Container for block names (to calculate indices later)
        /// </summary>
        /// <seealso cref="CacheBlocks"/>
        public String[] BlockNames
        {
            get { return m_BlockNames; }
            protected set { m_BlockNames = value; }
        }
        private String[] m_BlockNames;

        /// <summary>
        /// Game IDE data, for resolving XRef LOD distances etc.
        /// </summary>
        public Util.IDEReaderBasic IDEData
        {
            get { return m_IDEData; }
            private set { m_IDEData = value; }
        }
        private Util.IDEReaderBasic m_IDEData;
        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Serialiser constructor.
        /// </summary>
        /// <param name="branch">Branch object</param>
        /// <param name="scenes">Scenes to be serialised</param>
        /// <param name="iplstream">IPL stream directory</param>
        /// <param name="builddir">Path to project build directory (for IDE cache)</param>
        /// <param name="single">Create single IPL file iff true</param>
        public SceneSerialiserIPLBase(IBranch branch, Scene[] scenes, String iplstream, String builddir, bool single)
            : base(branch, scenes)
        {
            Debug.Assert(Directory.Exists(builddir) || single,
                String.Format("Specified build directory ({0}) does not exist, and not single IPL export.", builddir));
            if (!single)
                Debug.Assert(Directory.Exists(iplstream),
                    String.Format("Specified IPL stream directory ({0}) does not exist.", iplstream));
            this.BuildDir = builddir;
            this.IPLStreamDir = iplstream;
            if (Directory.Exists(this.BuildDir))
                this.IDEData = new RSG.SceneXml.MapExport.Util.IDEReaderBasic(this.BuildDir);
            this.SingleFile = single;
        }
        #endregion // Constructor(s)

        #region Abstract Methods
        /// <summary>
        /// Cache block names to calculate instance block indices when writing 
        /// IPL instance data to file.
        /// </summary>
        protected abstract void CacheBlocks();
        #endregion // Abstract Methods
    }

} // RSG.SceneXml.MapExport namespace

// SceneSerialiserIPLBase.cs
