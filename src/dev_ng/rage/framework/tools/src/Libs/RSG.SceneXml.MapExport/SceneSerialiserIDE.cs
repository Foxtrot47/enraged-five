//
// File: SceneSerialiserIDE.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of SceneSerialiserIDE class
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.SceneXml;
using RSG.Base;
using RSG.Base.Configuration;

namespace RSG.SceneXml.MapExport
{

	/// <summary>
	/// Map IDE File Serialiser from RSG.SceneXml.
	/// </summary>
	public class SceneSerialiserIDE : MultipleSceneSerialiserMap
	{
		#region Constants
		private readonly String TAG_OBJS = "objs";
		private readonly String TAG_TOBJ = "tobj";
		private readonly String TAG_ANIM = "anim";
		private readonly String TAG_TANM = "tanm";
		private readonly String TAG_MLO  = "mlo";
		private readonly String TAG_2DFX = "2dfx";
		private readonly String TAG_AMAT = "amat";
		private readonly String TAG_TXD = "txdp";
		private readonly String TAG_END = "end";
		#endregion // Constants

		#region Member Data
		List<Definition> m_lObjects;
		List<Definition> m_lTimeObjects;
		List<Definition> m_lAnimObjects;
		List<Definition> m_lTimeAnimObjects;
		List<Definition> m_lMilos;
		List<Definition> m_l2dfx;
		List<Definition> m_lAudioMaterials;
		#endregion // Member Data

		#region Constructor(s)
		/// <summary>
		/// IDE serialiser constructor, multiple Scene's.
		/// </summary>
        /// <param name="branch">Branch</param>
		/// <param name="scenes">Scenes being serialised</param>
		public SceneSerialiserIDE(IBranch branch, Scene[] scenes)
			: base(branch, scenes)
		{
			Init();
		}
		#endregion // Constructor(s)

		#region Controller Methods
		/// <summary>
		/// Write the serialised scene to a disk location (default options).
		/// </summary>
		/// <param name="filename"></param>
		/// <returns></returns>
		public override bool Write(String filename)
		{
			return (Write(filename, new Dictionary<String, Object>()));
		}

		/// <summary>
		/// Write the serialised scene to a disk location (default options).
		/// </summary>
		/// <param name="filename"></param>
		/// <param name="options"></param>
		/// <returns></returns>
		public override bool Write(String filename, Dictionary<String, Object> options)
		{
			Log.Log__Message("Starting IDE serialisation:");
			Log.Log__Message("\tDestination: {0}", filename);
			Log.Log__Message("\tScenes ({0}):", this.Scenes.Length);
			foreach (Scene scene in this.Scenes)
				Log.Log__Message("\t\t{0}", scene.Filename);

			Reset();
			foreach (Scene scene in this.Scenes)
			{
				foreach (TargetObjectDef o in scene.Objects)
				{
					if (o.IsObject())
					{
						if (o.DontExport())
							continue;
					}
					else if (o.Is2dfxLightEffect())
					{
						if (o.DontExportLightEffect())
							continue;
					}

					if (o.DontExportIDE())
						continue;

					// gunnard: Drawable LOD states don't ever need to go into the IDE.
					if (o.HasDrawableLODChildren() || o.HasRenderSimParent())
						continue;

					ProcessLinking(o);
					ProcessSceneObject(o);
					ProcessScene2DFX(o);
				}
			}
			SortLists();

			// Write IDE file
			Log.Log__Message("Writing {0}...", filename);
			using (StreamWriter fpFile = File.CreateText(filename))
			{
				fpFile.WriteLine("# IDE generated from SceneXml files:");
				foreach (Scene scene in this.Scenes)
					fpFile.WriteLine("#\t{0}", Path.GetFileName(scene.Filename), Path.GetFileName(scene.ExportFilename));
				fpFile.WriteLine(TAG_OBJS);
				foreach (Definition def in this.m_lObjects)
					fpFile.WriteLine(def.ToIDEObject());
				fpFile.WriteLine(TAG_END);
				fpFile.WriteLine(TAG_TOBJ);
				foreach (Definition def in this.m_lTimeObjects)
					fpFile.WriteLine(def.ToIDETimeObject());
				fpFile.WriteLine(TAG_END);
				fpFile.WriteLine(TAG_ANIM);
				foreach (Definition def in this.m_lAnimObjects)
					fpFile.WriteLine(def.ToIDEAnimObject());
				fpFile.WriteLine(TAG_END);
				fpFile.WriteLine(TAG_TANM);
				foreach (Definition def in this.m_lTimeAnimObjects)
					fpFile.WriteLine(def.ToIDETimeAnimObject());
				fpFile.WriteLine(TAG_END);
				fpFile.WriteLine(TAG_MLO);
				foreach (Definition def in this.m_lMilos)
					fpFile.WriteLine(def.ToIDEMilo(def.Object.MyScene));
				fpFile.WriteLine(TAG_END);
				fpFile.WriteLine(TAG_2DFX);
				foreach (Definition def in this.m_l2dfx)
					fpFile.WriteLine(def.ToIDE2dfx());
				fpFile.WriteLine(TAG_END);
				fpFile.WriteLine(TAG_AMAT);
				foreach (Definition def in this.m_lAudioMaterials)
					fpFile.WriteLine(def.ToIDEAudioMaterial());
				fpFile.WriteLine(TAG_END);

				fpFile.Close();
			}
			Log.Log__Message("Done.");
			
			return (true);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="xmlDoc"></param>
		/// <returns></returns>
		public override bool WriteManifestData(XDocument xmlDoc)
		{
			return (true);
		}

        static bool DeadEndFunc(TargetObjectDef obj)
		{
			if (obj.IsMAXGroup())
				return true;
			return false;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="filename"></param>
		/// <param name="objectName"></param>
		/// <returns></returns>
		public bool WriteObject2dfx(String filename, String objectName)
		{
			Debug.Assert(1 == this.Scenes.Length, "Object 2dfx mode; more than one SceneXml scene specified.  Internal error.");
			Log.Log__Message("Starting object IDE serialisation:");
			Log.Log__Message("  Scene:       {0}", this.Scenes[0].Filename);
			Log.Log__Message("  Object:      {0}", objectName);
			Log.Log__Message("  Destination: {0}", filename);

			// Find our object.
            TargetObjectDef obj = null;
            foreach (TargetObjectDef o in this.Scenes[0].Walk(Scene.WalkMode.DepthFirst, DeadEndFunc))
			{
				if (!o.Name.Equals(objectName, StringComparison.OrdinalIgnoreCase))
					continue;
				obj = o;
				break;
			}
			if (null == obj)
			{
				String message = String.Format("Failed to find object {0} in SceneXml.  Aborting.", objectName);
				Debug.Assert(obj != null, message);
				Log.Log__Error(message);
				return (false);
			}

			ProcessLinking(obj);
			ProcessScene2DFX(obj);
			int lightCount = 0;
            Dictionary<byte, List<TargetObjectDef>> takenHashes = new Dictionary<byte, List<TargetObjectDef>>();
            foreach (Definition def in this.m_l2dfx)
			{
				if (!def.Object.Is2dfxLightEffect())
					continue;
				++lightCount;

				// Only needed on props as other lights won't get instanced.
				if (SceneType.EnvironmentProps != this.Scenes[0].SceneType)
					continue;
                byte lightInstanceHash = def.Object.AttributeGuid.ToByteArray()[0];
                if (takenHashes.ContainsKey(lightInstanceHash))
                {
                    String setObjects = "";
                    foreach (TargetObjectDef d in takenHashes[lightInstanceHash])
                    {
                        if (d.Parent == def.Object.Parent)
                            setObjects += (d.Name + ",");
                    }
                    if (setObjects.Length > 0)
                        Log.Log__ErrorCtx(def.Object.Name, "Light {0} has an instance hash which is already existent on object {1}. https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#Duplicate_Light_hash", def.Object.Name, setObjects);
                }
                else
                {
                    takenHashes.Add(lightInstanceHash, new List<TargetObjectDef>());
                }
                takenHashes[lightInstanceHash].Add(def.Object);
            }
			if (0 == lightCount)
				return (true); // Don't write empty IDE file.

			// Write IDE file
			Log.Log__Message("Writing {0}...", filename);
			using (StreamWriter fpFile = File.CreateText(filename))
			{
				fpFile.WriteLine("# IDE generated from SceneXml file {0}, 3dsmax file {1}",
					Path.GetFileName(obj.MyScene.Filename), Path.GetFileName(obj.MyScene.ExportFilename));
				fpFile.WriteLine(TAG_2DFX);
				foreach (Definition def in this.m_l2dfx)
				{
					// Only write out Lights.
					if (!def.Object.Is2dfxLightEffect())
						continue;

					fpFile.WriteLine(def.ToIDE2dfx());
				}
				fpFile.WriteLine(TAG_END);
				fpFile.Close();
			}
			Log.Log__Message("Done.");

			return (true);
		}
		#endregion // Controller Methods

		#region Private Methods
		/// <summary>
		/// Common initialisation (invoked from multiple constructors).
		/// </summary>
		private void Init()
		{
			m_lObjects = new List<Definition>();
			m_lTimeObjects = new List<Definition>();
			m_lAnimObjects = new List<Definition>();
			m_lTimeAnimObjects = new List<Definition>();
			m_lMilos = new List<Definition>();
			m_l2dfx = new List<Definition>();
			m_lAudioMaterials = new List<Definition>();
		}

		/// <summary>
		/// Clear all internal state.
		/// </summary>
		private void Reset()
		{
			m_lObjects.Clear();
			m_lTimeObjects.Clear();
			m_lAnimObjects.Clear();
			m_lTimeAnimObjects.Clear();
			m_lMilos.Clear();
			m_l2dfx.Clear();
			m_lAudioMaterials.Clear();
		}

        private void ProcessLinking(TargetObjectDef obj)
		{
			if (obj.SkeletonRootGuid != Guid.Empty)
			{
                TargetObjectDef skelObjDef = obj.MyScene.FindObject(obj.SkeletonRootGuid);
				if (null != skelObjDef)
				{
					skelObjDef.SkinObj = obj;
				}
			}
            foreach (TargetObjectDef child in obj.Children)
				ProcessLinking(child);
		}

		/// <summary>
		/// Process a single top-level SceneXml ObjectDef.  This recurses into
		/// its children if required.
		/// </summary>
		/// <param name="obj">ObjectDef to process</param>
        private void ProcessSceneObject(TargetObjectDef obj)
		{
			if (obj.DontExport() || obj.DontExportIDE())
				return;
			// GunnarD: url:bugstar:99343 Avoid container children getting processed, too.
			if (obj.HasDrawableLODChildren() || obj.HasRenderSimParent())
				return;

			if (obj.IsXRef())
				return; // Skipped
			else if (obj.IsRefObject())
				return; // Skipped
			else if (obj.IsInternalRef())
				return; // Skipped
			else if (obj.IsRefInternalObject())
				return; // Skipped
			else if (obj.IsBlock())
				return; // Skipped
			else if (obj.Is2dfx())
				return; // Skipped
			else if (obj.IsCarGen())
				return; // Skipped
			else if (obj.IsCollision())
				return; // Skipped
			else if (obj.IsAnimProxy())
				return; // Skipped
			else if (obj.IsTimeObject() && obj.IsAnimObject())
				m_lTimeAnimObjects.Add(new Definition(obj.MyScene, obj));
			else if (obj.IsTimeObject())
				m_lTimeObjects.Add(new Definition(obj.MyScene, obj));
			else if (obj.IsAnimObject())
				m_lAnimObjects.Add(new Definition(obj.MyScene, obj));
			else if (obj.IsObject())
				m_lObjects.Add(new Definition(obj.MyScene, obj));
			else if (obj.IsMilo())
			{
				m_lMilos.Add(new Definition(obj.MyScene, obj));
                foreach (TargetObjectDef child in obj.Children)
					ProcessSceneObject(child);
			}
			else if (obj.IsMloPortal() || obj.IsMloRoom())
			{
                foreach (TargetObjectDef child in obj.Children)
					ProcessSceneObject(child);
			}
			else if (obj.HasAudioMaterial())
			{
				m_lAudioMaterials.Add(new Definition(obj.MyScene, obj));
			}
			else if (obj.IsContainer())
			{
                foreach (TargetObjectDef child in obj.Children)
					ProcessSceneObject(child);
			}
			else if (obj.IsPropGroup())
			{
                foreach (TargetObjectDef child in obj.Children)
					ProcessSceneObject(child);
			}
			else if (obj.IsDummy())
			{
				Log.Log__Warning("Ignoring Dummy {0} as this is how 3dsmax represents Groups.", obj);
			}
			else if (obj.IsEditableSpline() || obj.IsText() || obj.IsLine())
			{
				// Ignore.
			}
			else if (obj.IsTimeCycleBox())
			{
				// Ignore.
			}
			else if (obj.IsOcclusion())
			{
				// Ignore.
			}
			else
			{
				Log.Log__WarningCtx(obj.Name, "Ignoring {0} {1} as type not recognised by IDE serialiser.",
					new Object[] { obj.Name, obj.Class });
			}
		}

		/// <summary>
		/// Process a single SceneXml ObjectDef for 2DFX objects.
		/// </summary>
		/// <param name="obj"></param>
		private void ProcessScene2DFX(TargetObjectDef obj)
		{
			if (obj.DontExport() || obj.DontExportIDE())
				return;

			if (obj.Is2dfx() && obj.HasParent())
			{
				if (obj.Parent.IsObject())
					m_l2dfx.Add(new Definition(obj.MyScene, obj));
			}

			// Children (for 2DFX entries)
            foreach (TargetObjectDef o in obj.Children)
				ProcessScene2DFX(o);
		}

		/// <summary>
		/// 
		/// </summary>
		private void SortLists()
		{
			IComparer<Definition> comparer = new DefinitionIDEComparer();
			m_lObjects.Sort(comparer);
			m_lTimeObjects.Sort(comparer);
			m_lAnimObjects.Sort(comparer);
			m_lTimeAnimObjects.Sort(comparer);
			m_lMilos.Sort(comparer);
			//m_l2dfx.Sort(comparer);
			m_lAudioMaterials.Sort(comparer);
		}
		#endregion // Private Methods
	}

} // RSG.SceneXml.MapExport namespace

// SceneSerialiserIDE.cs
