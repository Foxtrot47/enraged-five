﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.Model.Map.SceneOverride;
using RSG.Pipeline.Content;
using RSG.SceneXml.MapExport.Framework;

namespace RSG.SceneXml.MapExport.Util
{
    /// <summary>
    /// Class responsible for integrating scene overrides (entity deletes, LOD distance changes) from object attributes and override data
    /// </summary>
    public class SceneOverrideIntegrator
    {
        public static SceneOverrideIntegrator Instance
        {
            get
            {
                if (instance_ == null)
                    instance_ = new SceneOverrideIntegrator();

                return instance_;
            }
        }

        private static SceneOverrideIntegrator instance_;

        private SceneOverrideIntegrator()
        {
            sceneOverrideManager_ = null;
        }

        public void Initialise(IBranch branch, ContentTreeHelper treeHelper, String sceneOverridesDirectory)
        {
            sceneOverrideManager_ = SceneOverrideManagerFactory.CreateXmlSceneOverrideManager(
                branch, treeHelper, sceneOverridesDirectory, false);
        }

        public float GetObjectLODDistance(TargetObjectDef objectDef)
        {
            if (sceneOverrideManager_ != null)
            {
                if (sceneOverrideManager_.HasLODDistanceOverrideForObject(objectDef))
                {
                    return sceneOverrideManager_.GetLODDistanceOverrideForObject(objectDef);
                }
            }

            if (!objectDef.IsObject() &&
                !objectDef.IsXRef() &&
                !objectDef.IsRefObject() &&
                !objectDef.IsInternalRef() &&
                !objectDef.IsRefInternalObject() &&
                !objectDef.IsAnimProxy())
            {
                return (-1.0f);
            }

            if (objectDef.IsAnimProxy())
                return (objectDef.GetAttribute(AttrNames.STATEDANIM_LOD_DISTANCE, AttrDefaults.STATEDANIM_LOD_DISTANCE));

            // If this object is within a Drawable LOD hierarchy: we return the
            // lowest detail meshes LOD distance (as thats what should be exported).
            if (objectDef.HasDrawableLODParent())
                return GetObjectLODDistance(objectDef.DrawableLOD.Parent);

            // Default to our "LOD distance" attribute.
            return objectDef.GetAttribute(AttrNames.OBJ_LOD_DISTANCE, AttrDefaults.OBJ_LOD_DISTANCE);
        }

        public float GetObjectChildLODDistance(TargetObjectDef objectDef)
        {
            if (sceneOverrideManager_ != null)
            {
                if (sceneOverrideManager_.HasChildLODDistanceOverrideForObject(objectDef))
                {
                    return sceneOverrideManager_.GetChildLODDistanceOverrideForObject(objectDef);
                }
            }

            float childLODDistance = objectDef.GetAttribute(AttrNames.OBJ_CHILD_LOD_DISTANCE, AttrDefaults.OBJ_CHILD_LOD_DISTANCE);

            if (childLODDistance == 0)
            {
                // B* 564899 - try to look up a sensible value
                switch (objectDef.LODLevel)
                {
                    case ObjectDef.LodLevel.HD:
                    case ObjectDef.LodLevel.LOD:
                    case ObjectDef.LodLevel.SLOD1:
                    case ObjectDef.LodLevel.SLOD3:
                    case ObjectDef.LodLevel.SLOD4:
                        if(objectDef.LOD != null && objectDef.LOD.Children.Length > 0)
                        {
                            childLODDistance = objectDef.LOD.Children.Max(childObjectDef => GetObjectLODDistance(childObjectDef));
                        }
                        break;
                    case ObjectDef.LodLevel.SLOD2:
                    {
                        TargetObjectDef[] lodChildren = InterContainerLODManager.Instance.GetSLOD2ObjectLODChildren(objectDef);
                        if(lodChildren.Length > 0)
                        {
                            childLODDistance = lodChildren.Max(childObjectDef => GetObjectLODDistance(childObjectDef));
                        }
                        break;
                    }
                }
            }

            // B* 734137 - prevent childLODDistance being set on any object that has a milo as children
            if (objectDef.LODLevel == ObjectDef.LodLevel.LOD && objectDef.LOD != null && childLODDistance > 0)
            {
                if(objectDef.LOD.Children.Count(child => child.IsMilo() || child.IsMiloTri()) > 0)
                    childLODDistance = 0;
            }


            return childLODDistance;
        }

        public bool IsObjectMarkedForDeletion(TargetObjectDef objectDef)
        {
            if (objectDef.LODLevel != ObjectDef.LodLevel.ORPHANHD)
                return false;

            if (sceneOverrideManager_ != null)
                return sceneOverrideManager_.IsEntityMarkedForDeletion(objectDef);

            return false;
        }

        public bool HasAttributeOverridesForObject(TargetObjectDef objectDef)
        {
            if (sceneOverrideManager_ != null)
                return sceneOverrideManager_.HasAttributeOverridesForObject(objectDef);

            return false;
        }

        public bool GetDontCastShadowsAttributeValue(TargetObjectDef objectDef)
        {
            if (sceneOverrideManager_ == null)
                throw new InvalidOperationException("HasAttributeOverridesForObject() is not valid unless the scene override system has been initialised.");

            return sceneOverrideManager_.GetDontCastShadowsAttributeValue(objectDef);
        }

        public bool GetDontRenderInShadowsAttributeValue(TargetObjectDef objectDef)
        {
            if (sceneOverrideManager_ == null)
                throw new InvalidOperationException("GetDontRenderInShadowsAttributeValue() is not valid unless the scene override system has been initialised.");

            return sceneOverrideManager_.GetDontRenderInShadowsAttributeValue(objectDef);
        }

        public bool GetDontRenderInReflectionsAttributeValue(TargetObjectDef objectDef)
        {
            if (sceneOverrideManager_ == null)
                throw new InvalidOperationException("GetDontRenderInReflectionsAttributeValue() is not valid unless the scene override system has been initialised.");

            return sceneOverrideManager_.GetDontRenderInReflectionsAttributeValue(objectDef);
        }

        public bool GetOnlyRenderInReflectionsAttributeValue(TargetObjectDef objectDef)
        {
            if (sceneOverrideManager_ == null)
                throw new InvalidOperationException("GetOnlyRenderInReflectionsAttributeValue() is not valid unless the scene override system has been initialised.");

            return sceneOverrideManager_.GetOnlyRenderInReflectionsAttributeValue(objectDef);
        }

        public bool GetDontRenderInWaterReflectionsAttributeValue(TargetObjectDef objectDef)
        {
            if (sceneOverrideManager_ == null)
                throw new InvalidOperationException("GetDontRenderInWaterReflectionsAttributeValue() is not valid unless the scene override system has been initialised.");

            return sceneOverrideManager_.GetDontRenderInWaterReflectionsAttributeValue(objectDef);
        }

        public bool GetOnlyRenderInWaterReflectionsAttributeValue(TargetObjectDef objectDef)
        {
            if (sceneOverrideManager_ == null)
                throw new InvalidOperationException("GetOnlyRenderInWaterReflectionsAttributeValue() is not valid unless the scene override system has been initialised.");

            return sceneOverrideManager_.GetOnlyRenderInWaterReflectionsAttributeValue(objectDef);
        }

        public bool GetDontRenderInMirrorReflectionsAttributeValue(TargetObjectDef objectDef)
        {
            if (sceneOverrideManager_ == null)
                throw new InvalidOperationException("GetDontRenderInMirrorReflectionsAttributeValue() is not valid unless the scene override system has been initialised.");

            return sceneOverrideManager_.GetDontRenderInMirrorReflectionsAttributeValue(objectDef);
        }

        public bool GetOnlyRenderInMirrorReflectionsAttributeValue(TargetObjectDef objectDef)
        {
            if (sceneOverrideManager_ == null)
                throw new InvalidOperationException("GetOnlyRenderInMirrorReflectionsAttributeValue() is not valid unless the scene override system has been initialised.");

            return sceneOverrideManager_.GetOnlyRenderInMirrorReflectionsAttributeValue(objectDef);
        }

        public bool GetStreamingPriorityLowAttributeValue(TargetObjectDef objectDef)
        {
            if (sceneOverrideManager_ == null)
                throw new InvalidOperationException("GetStreamingPriorityLowAttributeValue() is not valid unless the scene override system has been initialised.");

            return sceneOverrideManager_.GetStreamingPriorityLowAttributeValue(objectDef);
        }

        public int GetPriorityAttributeValue(TargetObjectDef objectDef)
        {
            if (sceneOverrideManager_ == null)
                throw new InvalidOperationException("GetPriorityAttributeValue() is not valid unless the scene override system has been initialised.");

            return sceneOverrideManager_.GetPriorityAttributeValue(objectDef);
        }

        private ISceneOverrideManager sceneOverrideManager_;
    }
}
