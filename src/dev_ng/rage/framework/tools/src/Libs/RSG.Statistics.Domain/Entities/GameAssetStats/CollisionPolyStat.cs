﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// Information regarding a single poly count stat.
    /// </summary>
    public class CollisionPolyStat<T> : DomainEntityBase where T : DomainEntityBase
    {
        #region Properties
        /// <summary>
        /// Link to the map section stat that this is for.
        /// </summary>
        public virtual T ReferencedStat { get; set; }

        /// <summary>
        /// Type of collision this stat is for.
        /// </summary>
        public virtual uint CollisionFlags { get; set; }

        /// <summary>
        /// Optional primitive type (i.e. Triangle, Sphere, Box, etc...).
        /// </summary>
        public virtual EnumReference<CollisionPrimitiveType> PrimitiveType { get; set; }

        /// <summary>
        /// Polygon count for the specified collision flags.
        /// </summary>
        public virtual uint PolygonCount { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        internal CollisionPolyStat()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Stat"></param>
        /// <param name="flags"></param>
        /// <param name="count"></param>
        public CollisionPolyStat(T stat, uint flags, uint count)
        {
            ReferencedStat = stat;
            CollisionFlags = flags;
            PolygonCount = count;
        }
        #endregion // Constructor(s)
    } // CollisionPolyStat<T>
}
