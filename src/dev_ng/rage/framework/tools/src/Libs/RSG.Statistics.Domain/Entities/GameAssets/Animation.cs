﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Common;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    public class Animation : GameAssetBase
    {
        #region Properties
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public Animation()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // Clip
} // namespace RSG.Statistics.Domain.Entities.GameAssets

