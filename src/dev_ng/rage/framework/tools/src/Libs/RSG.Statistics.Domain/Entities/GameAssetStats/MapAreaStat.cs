﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Common.Dto.GameAssetStats.Bundles;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// Map area statistic for a particular area/build.
    /// </summary>
    public class MapAreaStat : MapNodeStatBase
    {
        #region Properties
        /// <summary>
        /// Link to the map area that this stat is for.
        /// </summary>
        public virtual MapArea MapArea { get; set; }

        /// <summary>
        /// List of child map node stats (sections/other areas).
        /// </summary>
        public virtual IList<MapNodeStatBase> ChildMapNodeStats { get; set; }

        /// <summary>
        /// Map area export data path.
        /// </summary>
        public virtual string ExportDataPath { get; set; }

        /// <summary>
        /// Map area processed data path.
        /// </summary>
        public virtual string ProcessedDataPath { get; set; }
        #endregion // Properties

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public virtual void Update(MapAreaStatBundleDto bundle)
        {
            ExportDataPath = bundle.ExportDataPath;
            ProcessedDataPath = bundle.ProcessedDataPath;
        }
        #endregion // Public Methods
    } // MapAreaStat
}
