﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public interface IDomainEntity : IHasCreatedInformation
    {
        /// <summary>
        /// 
        /// </summary>
        long Id { get; }
    } // IEntity
}
