﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public class MapNodeStatBase : DomainEntityBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public virtual Build Build
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual Level Level
        {
            get;
            set;
        }

        /// <summary>
        /// Null for root level nodes.
        /// </summary>
        public virtual MapAreaStat ParentAreaStat
        {
            get;
            set;
        }
        #endregion // Properties
    } // MapNodeStat
}
