﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Model.Common;
using RSG.Model.Common.Map;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public class RoomStat : GameAssetStatBase
    {
        #region Properties
        /// <summary>
        /// Interior stat this room stat is associated with.
        /// </summary>
        public virtual InteriorArchetypeStat InteriorStat { get; set; }

        /// <summary>
        /// Room this stat is for.
        /// </summary>
        public virtual Room Room { get; set; }

        /// <summary>
        /// Bounding box min point.
        /// NHibernate spatial doesn't appear to support bounding boxes...wtf?!?
        /// so going to use two points instead.
        /// </summary>
        [StreamableStatAttribute(StreamableRoomStat.BoundingBoxString)]
        public virtual GeoAPI.Geometries.IPoint BoundingBoxMin { get; set; }

        /// <summary>
        /// Bounding box max point.
        /// </summary>
        [StreamableStatAttribute(StreamableRoomStat.BoundingBoxString)]
        public virtual GeoAPI.Geometries.IPoint BoundingBoxMax { get; set; }

        /// <summary>
        /// List of texture dictionaries and their associated export sizes.
        /// </summary>
        [StreamableStatAttribute(StreamableRoomStat.TxdExportSizesString)]
        public virtual IList<TxdExportSizeStat<RoomStat>> TxdExportSizes { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableRoomStat.ExportGeometrySizeString)]
        public virtual uint ExportGeometrySize { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableRoomStat.PolygonCountString)]
        public virtual uint PolygonCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableRoomStat.CollisionPolygonCountString)]
        public virtual uint CollisionPolygonCount { get; set; }

        /// <summary>
        /// List of collision poly counts associated with this map section stat.
        /// </summary>
        [StreamableStatAttribute(StreamableRoomStat.CollisionTypePolygonCountsString)]
        public virtual IList<CollisionPolyStat<RoomStat>> CollisionTypePolygonCounts { get; set; }

        /// <summary>
        /// List of entity statistics for this room (lazily loaded).
        /// </summary>
        [StreamableStatAttribute(StreamableRoomStat.EntitiesString)]
        public virtual IList<EntityStat> EntityStats { get; set; }
        #endregion // Properties
    } // RoomStat
}
