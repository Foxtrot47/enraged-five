﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Common;
using RSG.Model.Common.Animation;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    public class ClipDictionary : GameAssetBase
    {
        #region Properties
        /// <summary>
        /// String representation of this clip dictionaries category.
        /// </summary>
        public virtual EnumReference<ClipDictionaryCategory> Category { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ClipDictionary()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // ClipDictionary
} // namespace RSG.Statistics.Domain.Entities.GameAssets

