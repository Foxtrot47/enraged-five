﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Model.Common;
using RSG.Model.Common.Map;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public class EntityStat : DomainEntityBase
    {
        #region Properties
        /// <summary>
        /// Build this room stat is for.
        /// </summary>
        public virtual Build Build { get; set; }

        /// <summary>
        /// Entity this stat relates to.
        /// </summary>
        public virtual Entity Entity { get; set; }

        /// <summary>
        /// Archetype stat this entity stat is linked to.
        /// </summary>
        public virtual ArchetypeStat ArchetypeStat { get; set; }

        /// <summary>
        /// Map section stat this entity stat relates to (if any).
        /// </summary>
        public virtual MapSectionStat MapSectionStat { get; set; }

        /// <summary>
        /// Interior archetype stat this entity stat relates to (if any).
        /// </summary>
        public virtual InteriorArchetypeStat InteriorStat { get; set; }

        /// <summary>
        /// Room stat this entity stat relates to (if any).
        /// </summary>
        public virtual RoomStat RoomStat { get; set; }

        /// <summary>
        /// Where this entity appears on the map.
        /// </summary>
        [StreamableStatAttribute(StreamableEntityStat.PositionString)]
        public virtual GeoAPI.Geometries.IPoint Location { get; set; }

        /// <summary>
        /// Bounding box min point (world space).
        /// NHibernate spatial doesn't appear to support bounding boxes...wtf?!?
        /// so going to use two points instead.
        /// </summary>
        [StreamableStatAttribute(StreamableEntityStat.BoundingBoxString)]
        public virtual GeoAPI.Geometries.IPoint BoundingBoxMin { get; set; }

        /// <summary>
        /// Bounding box max point (world space).
        /// </summary>
        [StreamableStatAttribute(StreamableEntityStat.BoundingBoxString)]
        public virtual GeoAPI.Geometries.IPoint BoundingBoxMax { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableEntityStat.PriorityLevelString)]
        public virtual int PriorityLevel { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableEntityStat.IsLowPriorityString)]
        public virtual bool IsLowPriority { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableEntityStat.IsReferenceString)]
        public virtual bool IsReference { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableEntityStat.IsInteriorReferenceString)]
        public virtual bool IsInteriorReference { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableEntityStat.LodLevelString)]
        public virtual EnumReference<RSG.Model.Common.Map.LodLevel> LodLevel { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [StreamableStat(StreamableEntityStat.LodDistanceOverrideString)]
        public virtual float? LodDistanceOverride { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [StreamableStatAttribute(StreamableEntityStat.LodParentString)]
        public virtual EntityStat LodParent { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual IList<EntityStat> LodChildren { get; set; }
        
        /// <summary>
        /// AttrNames.OBJ_FORCE_BAKE_COLLISION attribute
        /// </summary>
        [StreamableStat(StreamableEntityStat.ForceBakeCollisionString)]
        public virtual bool ForceBakeCollision { get; set; }
        #endregion // Properties
    } // EntityStat
}
