﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// Interface for game assets.
    /// </summary>
    public interface IGameAsset : IDomainEntity
    {
        /// <summary>
        /// Friendly name for this asset.
        /// </summary>
        String Name { get; set; }

        /// <summary>
        /// Identifier for this asset.
        /// </summary>
        String Identifier { get; set; }
    } // IGameAsset
}
