﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.ExportStats
{
    /// <summary>
    /// Contains information about a single chunk of work performed during a map export.
    /// e.g. The export, image build or map check steps.
    /// </summary>
    public abstract class MapExportSubStat : DomainEntityBase
    {
        #region Properties
        /// <summary>
        /// The map export this sub stat is a part of.
        /// </summary>
        public virtual MapExportStat MapExportStat { get; set; }

        /// <summary>
        /// When this stage of the map export started.
        /// </summary>
        public virtual DateTime Start { get; set; }

        /// <summary>
        /// When this stage of the map export process completed.
        /// </summary>
        public virtual DateTime? End { get; set; }

        /// <summary>
        /// Whether  this stage of the map export was successful or not.
        /// </summary>
        public virtual bool? Success { get; set; }
        #endregion // Properties
    } // MapExportSubStat
}
