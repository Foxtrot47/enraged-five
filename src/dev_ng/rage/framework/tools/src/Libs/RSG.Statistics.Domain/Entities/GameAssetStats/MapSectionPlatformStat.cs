﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// Per platform/map section stat memory information statistic object.
    /// </summary>
    public class MapSectionPlatformStat : DomainEntityBase
    {
        #region Properties
        /// <summary>
        /// Platform this stat is for.
        /// </summary>
        public virtual EnumReference<RSG.Platform.Platform> Platform { get; set; }

        /// <summary>
        /// FileType this stat is for.
        /// </summary>
        public virtual EnumReference<RSG.Platform.FileType> FileType { get; set; }

        /// <summary>
        /// Map section stat this is associated with.
        /// </summary>
        public virtual MapSectionStat MapSectionStat { get; set; }

        /// <summary>
        /// Physical size for this stat.
        /// </summary>
        public virtual uint PhysicalSize { get; set; }

        /// <summary>
        /// Virtual size for this stat.
        /// </summary>
        public virtual uint VirtualSize { get; set; }
        #endregion // Properties
    } // MapSectionPlatformStat
}
