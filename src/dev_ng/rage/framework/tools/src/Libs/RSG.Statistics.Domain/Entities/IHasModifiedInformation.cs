﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities
{
    /// <summary>
    /// Interface for domain entities that contain modification information.
    /// </summary>
    public interface IHasModifiedInformation
    {
        /// <summary>
        /// Information about when this domain entity was last modified.
        /// </summary>
        DateTime ModifiedOn { get; set; }
    } // IHasModifiedInformation
}
