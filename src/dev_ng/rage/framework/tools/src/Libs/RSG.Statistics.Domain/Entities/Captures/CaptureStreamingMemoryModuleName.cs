﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.Captures
{
    /// <summary>
    /// Streaming memory module name.
    /// </summary>
    public class CaptureStreamingMemoryModuleName : NameHashBase
    {
    } // CaptureStreamingMemoryModuleName
}
