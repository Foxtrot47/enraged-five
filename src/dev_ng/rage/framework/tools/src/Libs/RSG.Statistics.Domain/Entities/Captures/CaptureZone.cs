﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.Captures
{
    /// <summary>
    /// Information regarding a single capture zone.
    /// </summary>
    public class CaptureZone : NameHashBase
    {
    } // CaptureZone
}
