﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public interface IHasCreatedInformation
    {
        /// <summary>
        /// 
        /// </summary>
        DateTime CreatedOn { get; set; }
    } // IHasCreatedInformation
}
