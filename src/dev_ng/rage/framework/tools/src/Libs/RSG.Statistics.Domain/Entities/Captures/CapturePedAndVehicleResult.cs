﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.Captures
{
    /// <summary>
    /// 
    /// </summary>
    public class CapturePedAndVehicleResult : CaptureResultBase
    {
        #region Properties
        /// <summary>
        /// Minimum number of pedestrians.
        /// </summary>
        public virtual uint MinNumPeds { get; set; }

        /// <summary>
        /// Average number of pedestrians.
        /// </summary>
        public virtual uint AvgNumPeds { get; set; }

        /// <summary>
        /// Maximum number of pedestrians.
        /// </summary>
        public virtual uint MaxNumPeds { get; set; }

        /// <summary>
        /// Minimum number of active pedestrians.
        /// </summary>
        public virtual uint MinActivePeds { get; set; }

        /// <summary>
        /// Average number of active pedestrians.
        /// </summary>
        public virtual uint AvgActivePeds { get; set; }

        /// <summary>
        /// Maximum number of active pedestrians.
        /// </summary>
        public virtual uint MaxActivePeds { get; set; }

        /// <summary>
        /// Minimum number of inactive pedestrians.
        /// </summary>
        public virtual uint MinInactivePeds { get; set; }

        /// <summary>
        /// Average number of inactive pedestrians.
        /// </summary>
        public virtual uint AvgInactivePeds { get; set; }

        /// <summary>
        /// Maximum number of inactive pedestrians.
        /// </summary>
        public virtual uint MaxInactivePeds { get; set; }

        /// <summary>
        /// Minimum number of event scan high pedestrians.
        /// </summary>
        public virtual uint MinEventScanHiPeds { get; set; }

        /// <summary>
        /// Average number of event scan high pedestrians.
        /// </summary>
        public virtual uint AvgEventScanHiPeds { get; set; }

        /// <summary>
        /// Maximum number of event scan high pedestrians.
        /// </summary>
        public virtual uint MaxEventScanHiPeds { get; set; }

        /// <summary>
        /// Minimum number of event scan low pedestrians.
        /// </summary>
        public virtual uint MinEventScanLoPeds { get; set; }

        /// <summary>
        /// Average number of event scan low pedestrians.
        /// </summary>
        public virtual uint AvgEventScanLoPeds { get; set; }

        /// <summary>
        /// Maximum number of event scan low pedestrians.
        /// </summary>
        public virtual uint MaxEventScanLoPeds { get; set; }

        /// <summary>
        /// Minimum number of motion task high pedestrians.
        /// </summary>
        public virtual uint MinMotionTaskHiPeds { get; set; }

        /// <summary>
        /// Average number of motion task high pedestrians.
        /// </summary>
        public virtual uint AvgMotionTaskHiPeds { get; set; }

        /// <summary>
        /// Maximum number of motion task high pedestrians.
        /// </summary>
        public virtual uint MaxMotionTaskHiPeds { get; set; }

        /// <summary>
        /// Minimum number of motion task low pedestrians.
        /// </summary>
        public virtual uint MinMotionTaskLoPeds { get; set; }

        /// <summary>
        /// Average number of motion task low pedestrians.
        /// </summary>
        public virtual uint AvgMotionTaskLoPeds { get; set; }

        /// <summary>
        /// Maximum number of motion task low pedestrians.
        /// </summary>
        public virtual uint MaxMotionTaskLoPeds { get; set; }

        /// <summary>
        /// Minimum number of physics high pedestrians.
        /// </summary>
        public virtual uint MinPhysicsHiPeds { get; set; }

        /// <summary>
        /// Average number of physics high pedestrians.
        /// </summary>
        public virtual uint AvgPhysicsHiPeds { get; set; }

        /// <summary>
        /// Maximum number of physics high pedestrians.
        /// </summary>
        public virtual uint MaxPhysicsHiPeds { get; set; }

        /// <summary>
        /// Minimum number of physics low pedestrians.
        /// </summary>
        public virtual uint MinPhysicsLoPeds { get; set; }

        /// <summary>
        /// Average number of physics low pedestrians.
        /// </summary>
        public virtual uint AvgPhysicsLoPeds { get; set; }

        /// <summary>
        /// Maximum number of physics low pedestrians.
        /// </summary>
        public virtual uint MaxPhysicsLoPeds { get; set; }

        /// <summary>
        /// Minimum number of entity scan high pedestrians.
        /// </summary>
        public virtual uint MinEntityScanHiPeds { get; set; }

        /// <summary>
        /// Average number of entity scan high pedestrians.
        /// </summary>
        public virtual uint AvgEntityScanHiPeds { get; set; }

        /// <summary>
        /// Maximum number of entity scan high pedestrians.
        /// </summary>
        public virtual uint MaxEntityScanHiPeds { get; set; }

        /// <summary>
        /// Minimum number of entity scan low pedestrians.
        /// </summary>
        public virtual uint MinEntityScanLoPeds { get; set; }

        /// <summary>
        /// Average number of entity scan low pedestrians.
        /// </summary>
        public virtual uint AvgEntityScanLoPeds { get; set; }

        /// <summary>
        /// Maximum number of entity scan low pedestrians.
        /// </summary>
        public virtual uint MaxEntityScanLoPeds { get; set; }

        /// <summary>
        /// Minimum number of vehicles.
        /// </summary>
        public virtual uint MinNumVehicles { get; set; }

        /// <summary>
        /// Average number of vehicles.
        /// </summary>
        public virtual uint AvgNumVehicles { get; set; }

        /// <summary>
        /// Maximum number of vehicles.
        /// </summary>
        public virtual uint MaxNumVehicles { get; set; }

        /// <summary>
        /// Minimum number of active vehicles.
        /// </summary>
        public virtual uint MinActiveVehicles { get; set; }

        /// <summary>
        /// Average number of active vehicles.
        /// </summary>
        public virtual uint AvgActiveVehicles { get; set; }

        /// <summary>
        /// Maximum number of active vehicles.
        /// </summary>
        public virtual uint MaxActiveVehicles { get; set; }

        /// <summary>
        /// Minimum number of inactive vehicles.
        /// </summary>
        public virtual uint MinInactiveVehicles { get; set; }

        /// <summary>
        /// Average number of inactive vehicles.
        /// </summary>
        public virtual uint AvgInactiveVehicles { get; set; }

        /// <summary>
        /// Maximum number of inactive vehicles.
        /// </summary>
        public virtual uint MaxInactiveVehicles { get; set; }

        /// <summary>
        /// Minimum number of real vehicles.
        /// </summary>
        public virtual uint MinRealVehicles { get; set; }

        /// <summary>
        /// Average number of real vehicles.
        /// </summary>
        public virtual uint AvgRealVehicles { get; set; }

        /// <summary>
        /// Maximum number of real vehicles.
        /// </summary>
        public virtual uint MaxRealVehicles { get; set; }

        /// <summary>
        /// Minimum number of dummy vehicles.
        /// </summary>
        public virtual uint MinDummyVehicles { get; set; }

        /// <summary>
        /// Average number of dummy vehicles.
        /// </summary>
        public virtual uint AvgDummyVehicles { get; set; }

        /// <summary>
        /// Maximum number of dummy vehicles.
        /// </summary>
        public virtual uint MaxDummyVehicles { get; set; }

        /// <summary>
        /// Minimum number of super dummy vehicles.
        /// </summary>
        public virtual uint MinSuperDummyVehicles { get; set; }

        /// <summary>
        /// Average number of super dummy vehicles.
        /// </summary>
        public virtual uint AvgSuperDummyVehicles { get; set; }

        /// <summary>
        /// Maximum number of super dummy vehicles.
        /// </summary>
        public virtual uint MaxSuperDummyVehicles { get; set; }

        /// <summary>
        /// Minimum number of network dummy vehicles.
        /// </summary>
        public virtual uint MinNetworkDummyVehicles { get; set; }

        /// <summary>
        /// Average number of network dummy vehicles.
        /// </summary>
        public virtual uint AvgNetworkDummyVehicles { get; set; }

        /// <summary>
        /// Maximum number of network dummy vehicles.
        /// </summary>
        public virtual uint MaxNetworkDummyVehicles { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CapturePedAndVehicleResult()
            : base()
        {
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <param name="zone"></param>
        public CapturePedAndVehicleResult(CaptureSession session, CaptureZone zone)
            : base(session, zone)
        {
        }
        #endregion // Constructor(s)
    } // CapturePedAndVehicleResult
}
