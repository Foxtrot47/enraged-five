﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Iesi.Collections.Generic;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssetStats.Animation
{
    /// <summary>
    /// 
    /// </summary>
    public class ClipStat : GameAssetStatBase<Clip>
    {
        #region Properties
        /// <summary>
        /// Clip dictionary stat this clip stat is associated with.
        /// </summary>
        public virtual ClipDictionaryStat ClipDictionaryStat { get; set; }

        /// <summary>
        /// Animation stat objects associated with this clip (lazily loaded).
        /// </summary>
        public virtual Iesi.Collections.Generic.ISet<AnimationStat> AnimationStats { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ClipStat()
        {
            AnimationStats = new HashedSet<AnimationStat>();
        }
        #endregion // Constructor(s)
    } // ClipStat
}
