using System.Collections.Generic;
using System.Text;
using System;
using System.Linq;
using RSG.Statistics.Common.Dto.GameAssets;
using System.Diagnostics;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    public class Vehicle : GameAssetBase<VehicleDto>
    {
        #region Properties
        /// <summary>
        /// String representation of this vehicle's category.
        /// </summary>
        public virtual String Category { get; set; }

        /// <summary>
        /// Friendly name for the vehicle.
        /// </summary>
        public virtual String FriendlyName { get; set; }

        /// <summary>
        /// Name of the vehicle as it appears in game.
        /// </summary>
        public virtual String GameName { get; set; }

        /// <summary>
        /// Flag indicating whether we've manually overridden the friendly name.
        /// </summary>
        public virtual bool FriendlyNameOverridden { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public Vehicle()
            : base()
        {
        }
        #endregion // Constructor(s)

        #region GameAssetBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public override void Update(VehicleDto dto)
        {
            base.Update(dto);

            Category = dto.Category;
            GameName = dto.GameName;

            if (!FriendlyNameOverridden)
            {
                FriendlyName = dto.FriendlyName;
            }
        }
        #endregion // GameAssetBase Overrides
    } // Vehicle
}
