﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.Captures
{
    /// <summary>
    /// 
    /// </summary>
    public class CaptureThreadResult : CaptureResultBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public virtual CaptureThreadName Name { get; set; }

        /// <summary>
        /// Minimum thread duration (in milliseconds).
        /// </summary>
        public virtual float Minimum { get; set; }

        /// <summary>
        /// Average thread duration (in milliseconds).
        /// </summary>
        public virtual float Average { get; set; }

        /// <summary>
        /// Maximum thread duration (in milliseconds).
        /// </summary>
        public virtual float Maximum { get; set; }

        /// <summary>
        /// Standard deviation of the average thread duration.
        /// </summary>
        public virtual float StandardDeviation { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CaptureThreadResult()
            : base()
        {
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <param name="zone"></param>
        public CaptureThreadResult(CaptureSession session, CaptureZone zone)
            : base(session, zone)
        {
        }
        #endregion // Constructor(s)
    } // CaptureThread
}
