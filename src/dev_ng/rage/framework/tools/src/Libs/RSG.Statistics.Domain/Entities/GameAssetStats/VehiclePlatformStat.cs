﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public class VehiclePlatformStat : GameAssetPlatformStatBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public virtual VehicleStat VehicleStat
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual uint HiPhysicalSize
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual uint HiVirtualSize
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VehiclePlatformStat()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // VehiclePlatformStat
}
