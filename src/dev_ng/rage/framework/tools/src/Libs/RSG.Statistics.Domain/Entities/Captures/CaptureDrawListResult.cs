﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.Captures
{
    /// <summary>
    /// Information relating to a single draw list result.
    /// </summary>
    public class CaptureDrawListResult : CaptureResultBase
    {
        #region Properties
        /// <summary>
        /// Draw list that this result relates to.
        /// </summary>
        public virtual DrawList DrawList { get; set; }

        /// <summary>
        /// Minimum count for this particular draw list.
        /// </summary>
        public virtual uint Minimum { get; set; }

        /// <summary>
        /// Average count for this particular draw list.
        /// </summary>
        public virtual uint Average { get; set; }

        /// <summary>
        /// Maximum count for this particular draw list.
        /// </summary>
        public virtual uint Maximum { get; set; }

        /// <summary>
        /// Standard deviation of the average count.
        /// </summary>
        public virtual float StandardDeviation { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CaptureDrawListResult()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <param name="zone"></param>
        public CaptureDrawListResult(CaptureSession session, CaptureZone zone)
            : base(session, zone)
        {
        }
        #endregion // Constructor(s)
    } // CaptureDrawListResult
}
