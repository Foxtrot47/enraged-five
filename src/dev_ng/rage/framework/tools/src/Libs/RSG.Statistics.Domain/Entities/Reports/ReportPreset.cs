﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.Reports
{
    /// <summary>
    /// Preset for reports.
    /// </summary>
    public class ReportPreset : DomainEntityBase
    {
        /// <summary>
        /// Name for this preset.
        /// </summary>
        public virtual String Name { get; set; }

        /// <summary>
        /// List of parameters associated with this preset.
        /// </summary>
        public virtual IList<ReportPresetParameter> Parameters { get; set; }
    } // ReportPreset
}
