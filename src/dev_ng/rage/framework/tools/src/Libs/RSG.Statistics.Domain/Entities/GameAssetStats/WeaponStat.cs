﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public class WeaponStat : GameAssetStatBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public virtual Weapon Weapon
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual int PolyCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual int CollisionCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual int BoneCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual int GripCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual int MagazineCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual int AttachmentCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual int LodPolyCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual bool HasLod
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual IList<WeaponPlatformStat> WeaponPlatformStats
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WeaponStat()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // WeaponStat
}
