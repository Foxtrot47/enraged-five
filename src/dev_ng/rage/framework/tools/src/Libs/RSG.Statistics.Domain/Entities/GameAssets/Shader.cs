﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    public class Shader : GameAssetBase<ShaderDto>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public Shader()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // Shader
}
