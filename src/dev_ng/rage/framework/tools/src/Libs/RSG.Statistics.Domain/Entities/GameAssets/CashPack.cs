﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// Class that encapsulates information about the cash packs that the game contains.
    /// </summary>
    public class CashPack : GameAssetBase
    {
        /// <summary>
        /// Amount of in game cash that this cash pack provides.
        /// </summary>
        public virtual uint InGameAmount { get; set; }

        /// <summary>
        /// Flag indicating whether this cash pack is currently available in game.
        /// </summary>
        public virtual bool Active { get; set; }
    } // CashPack
}
