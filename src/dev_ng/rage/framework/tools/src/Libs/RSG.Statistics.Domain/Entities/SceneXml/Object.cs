﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.SceneXml
{
    /// <summary>
    /// Domain entity class which contains information about an object
    /// in a SceneXml file.
    /// </summary>
    public class Object : DomainEntityBase
    {
        #region Properties
        /// <summary>
        /// Unique guid for this object.
        /// </summary>
        public virtual Guid Guid { get; set; }

        /// <summary>
        /// Name of the object. 
        /// </summary>
        public virtual String Name { get; set; }

        /// <summary>
        /// Object DCC class.
        /// </summary>
        public virtual String Class { get; set; }

        /// <summary>
        /// Object DCC superclass.
        /// </summary>
        public virtual String SuperClass { get; set; }

        /// <summary>
        /// Attribute container class.
        /// </summary>
        public virtual String AttributeClass { get; set; }

        /// <summary>
        /// Reference to the parent object for this one if it's not a root level object.
        /// </summary>
        public virtual Object ParentObject { get; set; }

        /// <summary>
        /// Reference to the immediate parent file if it's a root level object.
        /// </summary>
        public virtual Scene ParentScene { get; set; }

        /// <summary>
        /// Reference to the material that is applied to this object (if one is applied).
        /// </summary>
        public virtual Material Material { get; set; }

        /// <summary>
        /// Number of polys in object (trimesh and collision support).
        /// </summary>
        public virtual uint PolyCount { get; set; }

        /// <summary>
        /// Child object's that this object is made up of.
        /// </summary>
        public virtual IList<Object> Children { get; set; }

        /// <summary>
        /// Attributes applied to this object.
        /// </summary>
        public virtual IList<Attribute> Attributes { get; set; }

        /// <summary>
        /// ParamBlocks applied to this object.
        /// </summary>
        public virtual IList<Parameter> Parameters { get; set; }

        /// <summary>
        /// Reference to the file that this object is a part of.
        /// </summary>
        public virtual Scene Scene { get; set; }

        /// <summary>
        /// Object translation x-value.
        /// </summary>
        public virtual float ObjectTranslationX { get; set; }

        /// <summary>
        /// Object translation y-value.
        /// </summary>
        public virtual float ObjectTranslationY { get; set; }
        
        /// <summary>
        /// Object translation z-value.
        /// </summary>
        public virtual float ObjectTranslationZ { get; set; }

        /// <summary>
        /// Object rotation x-value.
        /// </summary>
        public virtual float ObjectRotationX { get; set; }
        
        /// <summary>
        /// Object rotation y-value.
        /// </summary>
        public virtual float ObjectRotationY { get; set; }
        
        /// <summary>
        /// Object rotation z-value.
        /// </summary>
        public virtual float ObjectRotationZ { get; set; }
        
        /// <summary>
        /// Object rotation w-value.
        /// </summary>
        public virtual float ObjectRotationW { get; set; }
        
        /// <summary>
        /// Object scale x-value.
        /// </summary>
        public virtual float ObjectScaleX { get; set; }
        
        /// <summary>
        /// Object scale y-value.
        /// </summary>
        public virtual float ObjectScaleY { get; set; }
        
        /// <summary>
        /// Object scale z-value.
        /// </summary>
        public virtual float ObjectScaleZ { get; set; }
        
        /// <summary>
        /// Node translation x-value.
        /// </summary>
        public virtual float NodeTranslationX { get; set; }

        /// <summary>
        /// Node translation y-value.
        /// </summary>
        public virtual float NodeTranslationY { get; set; }
        
        /// <summary>
        /// Node translation z-value.
        /// </summary>
        public virtual float NodeTranslationZ { get; set; }

        /// <summary>
        /// Node rotation x-value.
        /// </summary>
        public virtual float NodeRotationX { get; set; }
        
        /// <summary>
        /// Node rotation y-value.
        /// </summary>
        public virtual float NodeRotationY { get; set; }
        
        /// <summary>
        /// Node rotation z-value.
        /// </summary>
        public virtual float NodeRotationZ { get; set; }
        
        /// <summary>
        /// Node rotation w-value.
        /// </summary>
        public virtual float NodeRotationW { get; set; }
        
        /// <summary>
        /// Node scale x-value.
        /// </summary>
        public virtual float NodeScaleX { get; set; }
        
        /// <summary>
        /// Node scale y-value.
        /// </summary>
        public virtual float NodeScaleY { get; set; }
        
        /// <summary>
        /// Node scale z-value.
        /// </summary>
        public virtual float NodeScaleZ { get; set; }

        /// <summary>
        /// Local bounding box minimum x value.
        /// </summary>
        public virtual float LocalBoundingBoxMinX { get; set; }
        
        /// <summary>
        /// Local bounding box minimum y value.
        /// </summary>
        public virtual float LocalBoundingBoxMinY { get; set; }
        
        /// <summary>
        /// Local bounding box minimum z value.
        /// </summary>
        public virtual float LocalBoundingBoxMinZ { get; set; }
        
        /// <summary>
        /// Local bounding box maximum x value.
        /// </summary>
        public virtual float LocalBoundingBoxMaxX { get; set; }
        
        /// <summary>
        /// Local bounding box maximum y value.
        /// </summary>
        public virtual float LocalBoundingBoxMaxY { get; set; }
        
        /// <summary>
        /// Local bounding box maximum z value.
        /// </summary>
        public virtual float LocalBoundingBoxMaxZ { get; set; }

        /// <summary>
        /// World bounding box minimum x value.
        /// </summary>
        public virtual float WorldBoundingBoxMinX { get; set; }

        /// <summary>
        /// World bounding box minimum y value.
        /// </summary>
        public virtual float WorldBoundingBoxMinY { get; set; }

        /// <summary>
        /// World bounding box minimum z value.
        /// </summary>
        public virtual float WorldBoundingBoxMinZ { get; set; }

        /// <summary>
        /// World bounding box maximum x value.
        /// </summary>
        public virtual float WorldBoundingBoxMaxX { get; set; }

        /// <summary>
        /// World bounding box maximum y value.
        /// </summary>
        public virtual float WorldBoundingBoxMaxY { get; set; }

        /// <summary>
        /// World bounding box maximum z value.
        /// </summary>
        public virtual float WorldBoundingBoxMaxZ { get; set; }
        #endregion // Properties
    } // Object
}
