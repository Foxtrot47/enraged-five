﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Common;
using RSG.Model.Common.Mission;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    public class Mission : GameAssetBase<MissionDto>
    {
        #region Properties
        /// <summary>
        /// Mission description as received from bugstar.
        /// </summary>
        public virtual String Description { get; set; }

        /// <summary>
        /// Script name for this mission.
        /// </summary>
        public virtual String ScriptName { get; set; }

        /// <summary>
        /// Mission variant.
        /// </summary>
        public virtual uint Variant { get; set; }

        /// <summary>
        /// Mission Id as entered in bugstar.
        /// </summary>
        public virtual String MissionId { get; set; }

        /// <summary>
        /// Bugstar id.
        /// </summary>
        public virtual long BugstarId { get; set; }

        /// <summary>
        /// Number of attempts designers expect this mission to take to complete.
        /// </summary>
        public virtual float ProjectedAttempts { get; set; }

        /// <summary>
        /// Minimum number of attempts this mission should require.
        /// </summary>
        public virtual float ProjectedAttemptsMin { get; set; }

        /// <summary>
        /// Maximum number of attempts this mission should require.
        /// </summary>
        public virtual float ProjectedAttemptsMax { get; set; }

        /// <summary>
        /// Temporary property while mission identifiers are in their transitional phase.
        /// </summary>
        public virtual String AltIdentifier { get; set; }

        /// <summary>
        /// What type of game is this mission for (singleplayer/multiplayer).
        /// </summary>
        public virtual EnumReference<GameType> GameType { get; set; }

        /// <summary>
        /// Whether this mission is active or not.
        /// </summary>
        public virtual bool Active { get; set; }

        /// <summary>
        /// Whether bugstar knows about mission.
        /// </summary>
        public virtual bool InBugstar { get; set; }

        /// <summary>
        /// List of mission checkpoints this mission has.
        /// </summary>
        public virtual IList<MissionCheckpoint> Checkpoints { get; set; }

        /// <summary>
        /// Category this mission falls under.
        /// </summary>
        public virtual EnumReference<MissionCategory> Category { get; set; }

        /// <summary>
        /// Number of mission this is in the game (for this cateogry).
        /// </summary>
        public virtual uint? Index { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public Mission()
            : base()
        {
            Checkpoints = new List<MissionCheckpoint>();
        }
        #endregion // Constructor(s)

        #region GameAssetBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public override void Update(MissionDto dto)
        {
            // Legacy, shouldn't be called anymore.  Will remove when the MissionService is removed.
            throw new NotSupportedException();
        }
        #endregion // GameAssetBase Overrides
    } // Mission
}
