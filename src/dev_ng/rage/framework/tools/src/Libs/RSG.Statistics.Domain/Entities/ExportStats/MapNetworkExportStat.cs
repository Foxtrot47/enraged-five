﻿namespace RSG.Statistics.Domain.Entities.ExportStats
{
    using System;
    using System.Collections.Generic;
    using RSG.Statistics.Domain.Entities.GameAssets;

    /// <summary>
    /// Information about a single map export.
    /// </summary>
    public class MapNetworkExportStat : DomainEntityBase
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="JbId"/> property.
        /// </summary>
        private Guid jobId;

        /// <summary>
        /// The private field used for the <see cref="Username"/> property.
        /// </summary>
        private string username;

        /// <summary>
        /// The private field used for the <see cref="MachineName"/> property.
        /// </summary>
        private string machineName;

        /// <summary>
        /// The private field used for the <see cref="ExportType"/> property.
        /// </summary>
        private string exportType;

        /// <summary>
        /// The private field used for the <see cref="MapSections"/> property.
        /// </summary>
        private ICollection<MapSection> mapSections;

        /// <summary>
        /// The private field used for the <see cref="JobCreationNumber"/> property.
        /// </summary>
        private uint jobCreationNumber;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="MapNetworkExportStat"/> class.
        /// </summary>
        public MapNetworkExportStat()
        {
            this.mapSections = new List<MapSection>();
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the jobs unique identifier that can be used to reference it or link it
        /// with addition statistics.
        /// </summary>
        public virtual Guid JobId
        {
            get { return this.jobId; }
            set { this.jobId = value; }
        }

        /// <summary>
        /// Gets or sets the name of the user who created the job.
        /// </summary>
        public virtual string Username
        {
            get { return this.username; }
            set { this.username = value; }
        }

        /// <summary>
        /// Gets or sets the name of the machine the export was performed on.
        /// </summary>
        public virtual string MachineName
        {
            get { return this.machineName; }
            set { this.machineName = value; }
        }

        /// <summary>
        /// Gets or sets the type of export that the user performed (e.g. Container, Selected,
        /// Preview).
        /// </summary>
        public virtual string ExportType
        {
            get { return this.exportType; }
            set { this.exportType = value; }
        }

        /// <summary>
        /// Collection of map section that were part of this export.
        /// </summary>
        public virtual ICollection<MapSection> MapSections
        {
            get { return this.mapSections; }
            set { this.mapSections = value; }
        }

        /// <summary>
        /// Gets or sets the number of times the user has used the network export functionality
        /// since 3ds max was started.
        /// </summary>
        public virtual uint JobCreationNumber
        {
            get { return this.jobCreationNumber; }
            set { this.jobCreationNumber = value; }
        }
        #endregion
    } // RSG.Statistics.Domain.Entities.ExportStats.MapExportStat {Class}
} // RSG.Statistics.Domain.Entities.ExportStats {Namespace}
