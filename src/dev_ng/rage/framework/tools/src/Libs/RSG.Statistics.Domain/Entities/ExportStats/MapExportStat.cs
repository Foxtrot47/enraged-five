﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Extensions;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.ExportStats
{
    /// <summary>
    /// Information about a single map export.
    /// </summary>
    public class MapExportStat : DomainEntityBase, IHasModifiedInformation
    {
        #region Properties
        /// <summary>
        /// Time when the db entry was last updated.
        /// This gets automatically updated by the AuditEventListener whenever the entry is persisted to the database.
        /// </summary>
        public virtual DateTime ModifiedOn { get; set; }

        /// <summary>
        /// When the export started.
        /// </summary>
        public virtual DateTime Start { get; set; }

        /// <summary>
        /// When the export successfully completed.
        /// </summary>
        public virtual DateTime? End { get; set; }

        /// <summary>
        /// Name of the user who performed the export.
        /// </summary>
        public virtual String Username { get; set; }

        /// <summary>
        /// Name of the machine the export was performed on.
        /// </summary>
        public virtual String MachineName { get; set; }

        /// <summary>
        /// Type of export that the user performed (e.g. Everything, Container, Selected).
        /// </summary>
        public virtual String ExportType { get; set; }

        /// <summary>
        /// Version of the tools the user was on when they performed the export.
        /// </summary>
        public virtual float? ToolsVersion { get; set; }

        /// <summary>
        /// Whether the export was done with XGE enabled.
        /// </summary>
        public virtual bool? XGEEnabled { get; set; }

        /// <summary>
        /// Whether the export was done using XGE in standalone mode.
        /// </summary>
        public virtual bool? XGEStandalone { get; set; }

        /// <summary>
        /// The number of cpus that were forced.
        /// </summary>
        public virtual int? XGEForceCPUCount { get; set; }
        
        /// <summary>
        /// Whether the export was successful or not.
        /// </summary>
        public virtual bool? Success { get; set; }

        /// <summary>
        /// Number of the export that this is since 3ds max was started.
        /// </summary>
        public virtual uint ExportNumber { get; set; }

        /// <summary>
        /// Name of the processor that was used for the export.
        /// </summary>
        public virtual String ProcessorName { get; set; }

        /// <summary>
        /// Number of cores the processor had.
        /// </summary>
        public virtual uint? ProcessorCores { get; set; }

        /// <summary>
        /// Amount of memory the users machine had.
        /// </summary>
        public virtual ulong? InstalledMemory { get; set; }

        /// <summary>
        /// Private bytes at the start of the export.
        /// </summary>
        public virtual ulong? PrivateBytesStart { get; set; }

        /// <summary>
        /// Private bytes at the end of the export.
        /// </summary>
        public virtual ulong? PrivateBytesEnd { get; set; }

        /// <summary>
        /// Collection of map section that were part of this export.
        /// </summary>
        public virtual ICollection<MapSection> MapSections { get; set; }

        /// <summary>
        /// Collection of platforms that were part of this export.
        /// </summary>
        public virtual ICollection<EnumReference<RSG.Platform.Platform>> Platforms { get; set; }

        /// <summary>
        /// Breakdown of this map export. Aggregated from the individual items.
        /// </summary>
        public virtual IList<MapExportSubStat> SubStats
        {
            get
            {
                IList<MapExportSubStat> subStats = new List<MapExportSubStat>();
                if (MapCheckStat != null)
                {
                    subStats.Add(MapCheckStat);
                }
                if (SectionExportStats.Any())
                {
                    subStats.AddRange(SectionExportStats);
                }
                if (ImageBuildStat != null)
                {
                    subStats.Add(ImageBuildStat);
                }
                return subStats;
            }
        }

        /// <summary>
        /// Map check that was done for this map export.
        /// Inverse reference.
        /// </summary>
        public virtual MapExportMapCheckStat MapCheckStat { get; set; }

        /// <summary>
        /// List of map section exports that make up this map export.
        /// Inverse reference.
        /// </summary>
        public virtual IList<MapExportSectionExportStat> SectionExportStats { get; set; }

        /// <summary>
        /// Map image build that was done for this map export.
        /// Inverse reference.
        /// </summary>
        public virtual MapExportImageBuildStat ImageBuildStat { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MapExportStat()
        {
            Platforms = new List<EnumReference<RSG.Platform.Platform>>();
            MapSections = new List<MapSection>();
        }
        #endregion // Constructor(s)
    } // MapExportStat
}
