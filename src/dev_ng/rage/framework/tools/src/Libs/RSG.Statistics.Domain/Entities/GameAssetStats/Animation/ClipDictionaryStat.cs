﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Iesi.Collections.Generic;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssetStats.Animation
{
    /// <summary>
    /// 
    /// </summary>
    public class ClipDictionaryStat : GameAssetStatBase<ClipDictionary>
    {
        #region Properties
        /// <summary>
        /// Category for this clip dictionary.
        /// </summary>
        public virtual EnumReference<RSG.Model.Common.Animation.ClipDictionaryCategory> Category { get; set; }

        /// <summary>
        /// Clip stat objects associated with this dictionary (lazily loaded).
        /// </summary>
        public virtual Iesi.Collections.Generic.ISet<ClipStat> ClipStats { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ClipDictionaryStat()
        {
            ClipStats = new HashedSet<ClipStat>();
        }
        #endregion // Constructor(s)
    } // ClipDictionaryStat
}
