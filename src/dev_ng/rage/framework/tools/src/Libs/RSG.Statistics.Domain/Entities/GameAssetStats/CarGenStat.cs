﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// Information about a single car gen linked to a map section stat.
    /// </summary>
    public class CarGenStat : DomainEntityBase
    {
        #region Properties
        /// <summary>
        /// Link to the map section stat that this is for.
        /// </summary>
        public virtual MapSectionStat MapSectionStat { get; set; }

        /// <summary>
        /// Name of the car gen.
        /// </summary>
        public virtual String Name { get; set; }

        /// <summary>
        /// Where this car gen is.
        /// </summary>
        public virtual GeoAPI.Geometries.IPoint Position { get; set; }

        /// <summary>
        /// Whether this car gen is for police vehicles.
        /// </summary>
        public virtual bool IsPolice { get; set; }

        /// <summary>
        /// Whether this car gen is for ambulance vehicles.
        /// </summary>
        public virtual bool IsAmbulance { get; set; }

        /// <summary>
        /// Whether this car gen is for a specific model.
        /// </summary>
        public virtual bool IsSpecificModel { get; set; }

        /// <summary>
        /// Name of the model this car gen is for.
        /// </summary>
        public virtual string ModelName { get; set; }

        /// <summary>
        /// Whether this is a high priority car gen.
        /// </summary>
        public virtual bool IsHighPriority { get; set; }
        #endregion // Properties
    } // CarGenStat
}
