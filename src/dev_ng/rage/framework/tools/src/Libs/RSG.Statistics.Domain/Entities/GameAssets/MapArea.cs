﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    public class MapArea : GameAssetBase<MapAreaDto>
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MapArea()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // MapArea
}
