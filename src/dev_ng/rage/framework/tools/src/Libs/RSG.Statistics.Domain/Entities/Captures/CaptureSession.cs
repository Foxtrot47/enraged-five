﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Common;

namespace RSG.Statistics.Domain.Entities.Captures
{
    /// <summary>
    /// A capture session provides context to a set of capture stats.
    /// There are multiple contexts for capture stats.  One of the following sets of properties
    /// need to be setup for this session:
    /// (SpSession, MpSession, AutomatedTestNumber)
    /// (ChangelistNumber)
    /// (PerBuild)  - Not yet implemented
    /// (Mission)   - Not yet implemented
    /// </summary>
    public class CaptureSession : DomainEntityBase
    {
        #region Properties

        /// <summary>
        /// Indicates these stats are of the 'perf stats' flavour
        /// </summary>
        public virtual bool PerfStats { get; set; }

        /// <summary>
        /// Game build the stat packet originated from.
        /// </summary>
        public virtual Build Build { get; set; }

        /// <summary>
        /// Platform the tests were run on.
        /// </summary>
        public virtual EnumReference<RSG.Platform.Platform> Platform { get; set; }

        /// <summary>
        /// Level the user was in when the stat packet was generated
        /// </summary>
        public virtual Level Level { get; set; }

        /// <summary>
        /// Which build configuration the game is running in (i.e. Beta, BankRelease, etc...).
        /// </summary>
        public virtual EnumReference<BuildConfig> BuildConfig { get; set; }

        /// <summary>
        /// Time when this test session ran.
        /// </summary>
        public virtual DateTime Timestamp { get; set; }

        /// <summary>
        /// Automated test associated with this capture.
        /// </summary>
        public virtual uint? AutomatedTestNumber { get; set; }

        /// <summary>
        /// Changelist number for this capture session.
        /// </summary>
        public virtual uint? ChangelistNumber { get; set; }

        /// <summary>
        /// When set to true, this capture session is from the per build capture.
        /// </summary>
        //public virtual bool? PerBuild { get; set; }

        /// <summary>
        /// Mission this capture session covers.
        /// </summary>
        //public virtual Mission Mission { get; set; }

        #region Inverse Relationship (for Cascades)
        /// <summary>
        /// Cpu results.
        /// </summary>
        public virtual IList<CaptureCpuResult> CpuResults { get; set; }

        /// <summary>
        /// Draw list results.
        /// </summary>
        public virtual IList<CaptureDrawListResult> DrawListResults { get; set; }

        /// <summary>
        /// Fps results.
        /// </summary>
        public virtual IList<CaptureFpsResult> FpsResults { get; set; }

        /// <summary>
        /// Fps results.
        /// </summary>
        public virtual IList<CaptureFrametimeResult> FrametimeResults { get; set; }

        /// <summary>
        /// Gpu results.
        /// </summary>
        public virtual IList<CaptureGpuResult> GpuResults { get; set; }

        /// <summary>
        /// Memory results.
        /// </summary>
        public virtual IList<CaptureMemoryResult> MemoryResults { get; set; }

        /// <summary>
        /// Pedestrian and vehicle results
        /// </summary>
        public virtual IList<CapturePedAndVehicleResult> PedAndVehicleResults { get; set; }

        /// <summary>
        /// Script memory results.
        /// </summary>
        public virtual IList<CaptureScriptMemoryResult> ScriptMemoryResults { get; set; }

        /// <summary>
        /// Streaming memory results.
        /// </summary>
        public virtual IList<CaptureStreamingMemoryResult> StreamingMemoryResults { get; set; }

        /// <summary>
        /// Thread results.
        /// </summary>
        public virtual IList<CaptureThreadResult> ThreadResults { get; set; }
        #endregion // Inverse Relationships (for Cascades)
        #endregion // Properties
    } // CaptureSession
}
