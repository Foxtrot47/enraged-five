﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities
{
    /// <summary>
    /// Abstract base class for classes that are just a name/hash combination.
    /// </summary>
    public abstract class NameHashBase : DomainEntityBase
    {
        /// <summary>
        /// String representation of the name.
        /// </summary>
        public virtual string Name
        {
            get;
            set;
        }

        /// <summary>
        /// Hash of the name.
        /// </summary>
        public virtual uint Hash
        {
            get;
            set;
        }
    } // NameHashBase
}
