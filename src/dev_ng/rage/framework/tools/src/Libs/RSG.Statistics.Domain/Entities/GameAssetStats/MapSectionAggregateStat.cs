﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public class MapSectionAggregateStat : DomainEntityBase
    {
        #region Properties
        /// <summary>
        /// Link to the map section stat that this is for.
        /// </summary>
        public virtual MapSectionStat MapSectionStat { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual EnumReference<StatGroup> Group { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual EnumReference<StatLodLevel> LodLevel { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual EnumReference<StatEntityType> EntityType { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual bool IncludesPropGroup { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual uint TotalCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual uint UniqueCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual uint DrawableCost { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual uint TXDCost { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual uint TXDCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual uint ShaderCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual uint TextureCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual uint PolygonCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual uint CollisionCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual IList<CollisionPolyStat<MapSectionAggregateStat>> CollisionTypePolygonCounts { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual float LodDistance { get; set; }
        #endregion // Properties
    } // MapSectionAggregateStat
}
