﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// Information regarding a multiplayer unlock.
    /// </summary>
    public class Unlock : DomainEntityBase
    {
        /// <summary>
        /// Name of the unlock.
        /// </summary>
        public virtual String Name { get; set; }

        /// <summary>
        /// Rank at which this item is unlocked.
        /// </summary>
        public virtual uint Rank { get; set; }

        /// <summary>
        /// Type of unlock this is.
        /// </summary>
        public virtual EnumReference<UnlockType> UnlockType { get; set; }
    } // Unlock
}
