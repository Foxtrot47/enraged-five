﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.ExportStats
{
    /// <summary>
    /// Stores information regarding a map section's export time.
    /// This forms part of the map export process.
    /// </summary>
    public class MapExportSectionExportStat : MapExportSubStat
    {
        #region Properties
        /// <summary>
        /// The section this export relates to.
        /// </summary>
        public virtual MapSection MapSection { get; set; }

        /// <summary>
        /// The sub stats for this map section export.
        /// Lazily loaded.
        /// </summary>
        public virtual IList<MapExportSectionExportSubStat> SubStats { get; set; }
        #endregion // Properties
    } // MapExportSectionExportStat
}
