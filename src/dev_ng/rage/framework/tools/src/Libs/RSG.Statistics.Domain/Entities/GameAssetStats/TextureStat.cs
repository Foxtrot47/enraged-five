﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public class TextureStat : DomainEntityBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public virtual Texture Texture
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual ShaderStat ShaderStat
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public TextureStat()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // TextureStat
}
