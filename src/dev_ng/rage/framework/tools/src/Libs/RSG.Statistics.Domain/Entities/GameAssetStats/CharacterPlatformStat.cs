﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public class CharacterPlatformStat : GameAssetPlatformStatBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public virtual CharacterStat CharacterStat
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CharacterPlatformStat()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // CharacterPlatformStat
}
