﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.ROS;

namespace RSG.Statistics.Domain.Entities.Vertica
{
    /// <summary>
    /// Basic gamer data cached from vertica for creating gamer groups.
    /// </summary>
    public class VerticaGamer : DomainEntityBase
    {
        /// <summary>
        /// Name of the gamer.
        /// </summary>
        public virtual String Name { get; set; }

        /// <summary>
        /// Platform this gamer belongs to.
        /// </summary>
        public virtual EnumReference<ROSPlatform> Platform { get; set; }

        /// <summary>
        /// List of gamer groups that this gamer is in.
        /// Lazily loaded.
        /// Required for the many-to-many connection.
        /// </summary>
        public virtual Iesi.Collections.Generic.ISet<VerticaGamerGroup> GamerGroups { get; set; }
    } // Gamer
}
