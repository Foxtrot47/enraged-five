﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// In game shop.
    /// </summary>
    public class Shop : FriendlyGameAssetBase
    {
        /// <summary>
        /// Reference to the type of shop this is.
        /// </summary>
        public virtual EnumReference<ShopType> ShopType { get; set; }
    } // Shop
}
