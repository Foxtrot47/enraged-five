﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Iesi.Collections.Generic;

namespace RSG.Statistics.Domain.Entities.Vertica
{
    /// <summary>
    /// Group of gamers.
    /// </summary>
    public class VerticaGamerGroup : DomainEntityBase
    {
        #region Properties
        /// <summary>
        /// Name of the group.
        /// </summary>
        public virtual String Name { get; set; }

        /// <summary>
        /// List of gamers that are in this group (lazily loaded).
        /// </summary>
        public virtual Iesi.Collections.Generic.ISet<VerticaGamer> Gamers { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public VerticaGamerGroup()
            : base()
        {
            Gamers = new HashedSet<VerticaGamer>();
        }

        public VerticaGamerGroup(String name)
            : this()
        {
            Name = name;
        }
        #endregion // Constructor(s)
    } // GamerGroup
}
