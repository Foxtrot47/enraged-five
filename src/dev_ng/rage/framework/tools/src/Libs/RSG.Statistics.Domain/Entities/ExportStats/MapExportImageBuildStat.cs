﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.ExportStats
{
    /// <summary>
    /// Stores information regarding the time spent building multiple map sections.
    /// This forms a part of map export process.
    /// </summary>
    public class MapExportImageBuildStat : MapExportSubStat
    {
        #region Properties
        /// <summary>
        /// Collection of map section that were part of this build.
        /// </summary>
        public virtual ICollection<MapSection> MapSections { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MapExportImageBuildStat()
        {
            MapSections = new List<MapSection>();
        }
        #endregion // Constructor(s)
    } // MapExportImageBuildStat
}
