﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    public class Weapon : GameAssetBase<WeaponDto>
    {
        #region Properties
        /// <summary>
        /// Model category (e.g. Rifle, Handgun, etc...)
        /// </summary>
        public virtual String Category { get; set; }

        /// <summary>
        /// Friendly name for the weapon.
        /// </summary>
        public virtual String FriendlyName { get; set; }

        /// <summary>
        /// Name of the art model.
        /// </summary>
        public virtual String ModelName { get; set; }

        /// <summary>
        /// Stat name of the weapon.
        /// </summary>
        public virtual String StatName { get; set; }

        /// <summary>
        /// Flag indicating whether we've manually overridden the friendly name.
        /// </summary>
        public virtual bool FriendlyNameOverridden { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public Weapon()
            : base()
        {
        }
        #endregion // Constructor(s)

        #region GameAssetBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public override void Update(WeaponDto dto)
        {
            base.Update(dto);

            Category = dto.Category;
            ModelName = dto.ModelName;
            StatName = dto.StatName;

            // Only update the friendly name if it isn't empty.  This is to prevent names manually
            // entered in the db to be replaced by the dto's null value.
            if (!String.IsNullOrEmpty(dto.FriendlyName) && !FriendlyNameOverridden)
            {
                FriendlyName = dto.FriendlyName;
            }
        }
        #endregion // GameAssetBase Overrides
    } // Weapon
}
