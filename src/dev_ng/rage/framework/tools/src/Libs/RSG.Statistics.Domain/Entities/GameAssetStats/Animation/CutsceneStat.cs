﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Iesi.Collections.Generic;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssetStats.Animation
{
    /// <summary>
    /// Per build statistics for a cutscene.
    /// </summary>
    public class CutsceneStat : GameAssetStatBase<GameAssets.Cutscene>
    {
        #region Properties
        /// <summary>
        /// Name of the exported zip file (for concats this will be null).
        /// </summary>
        public virtual String ExportZipFilepath { get; set; }

        /// <summary>
        /// Flag indicating whether this is a cutscene concat.
        /// </summary>
        public virtual bool IsConcat { get; set; }

        /// <summary>
        /// Flag indicating whether this cutscene contains branches.
        /// </summary>
        public virtual bool HasBranch { get; set; }

        /// <summary>
        /// Part stat objects associated with this cutscene (lazily loaded).
        /// </summary>
        public virtual Iesi.Collections.Generic.ISet<CutscenePartStat> PartStats { get; set; }

        /// <summary>
        /// Platform stat objects associated with this cutscene (lazily loaded).
        /// </summary>
        public virtual Iesi.Collections.Generic.ISet<CutscenePlatformSectionStat> PlatformSectionStats { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CutsceneStat()
            : base()
        {
            PartStats = new HashedSet<CutscenePartStat>();
            PlatformSectionStats = new HashedSet<CutscenePlatformSectionStat>();
        }
        #endregion // Constructor(s)
    } // CutsceneStat
}
