﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.GameAssetStats.Animation
{
    /// <summary>
    /// 
    /// </summary>
    public class CutscenePartStat : DomainEntityBase
    {
        /// <summary>
        /// Cutscene stat this part stat is associated with.
        /// </summary>
        public virtual CutsceneStat CutsceneStat { get; set; }

        /// <summary>
        /// Name of the part.
        /// </summary>
        public virtual String Name { get; set; }

        /// <summary>
        /// Filename containing the cutscene information.
        /// </summary>
        public virtual String CutFile { get; set; }

        /// <summary>
        /// How long this part is (in seconds).
        /// </summary>
        public virtual float Duration { get; set; }
    } // CutscenePartStat
}
