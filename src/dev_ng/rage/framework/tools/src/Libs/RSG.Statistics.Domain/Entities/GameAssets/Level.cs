﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GisSharpBlog.NetTopologySuite.Geometries;
using RSG.Statistics.Common.Dto.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    public class Level : GameAssetBase<LevelDto>
    {
        #region Properties
        /// <summary>
        /// Level description (This would normally come from bugstar)
        /// </summary>
        public virtual string Description
        {
            get;
            set;
        }

        /// <summary>
        /// Index into the list of levels as defined by the "common\data\levels.xml" file
        /// </summary>
        //TODO: Revisit this once the game has changed the way it sends the level idx.
        public virtual uint? LevelIdx
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public Level()
            : base()
        {
        }
        #endregion // Constructor(s)

        #region GameAssetBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public override void Update(LevelDto dto)
        {
            base.Update(dto);

            Description = dto.Description;
            LevelIdx = dto.LevelIdx;
        }
        #endregion // GameAssetBase Overrides
    } // Level
}
