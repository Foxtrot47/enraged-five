﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.Captures
{
    /// <summary>
    /// Statistics from a gpu capture telemetry submission.
    /// </summary>
    public class CaptureGpuResult : CaptureResultBase
    {
        #region Properties
        /// <summary>
        /// Name of the draw list for this result.
        /// </summary>
        public virtual DrawList DrawList { get; set; }

        /// <summary>
        /// Amount of time spent in this gpu metric (in milliseconds).
        /// </summary>
        public virtual float Time { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CaptureGpuResult()
            : base()
        {
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <param name="zone"></param>
        public CaptureGpuResult(CaptureSession session, CaptureZone zone)
            : base(session, zone)
        {
        }
        #endregion // Constructor(s)
    } // CaptureGpuResult
}
