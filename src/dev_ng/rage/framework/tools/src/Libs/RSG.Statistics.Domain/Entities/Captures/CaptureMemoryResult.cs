﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.Captures
{
    /// <summary>
    /// 
    /// </summary>
    public class CaptureMemoryResult : CaptureResultBase
    {
        #region Properties
        /// <summary>
        /// Name of the bucket that this information relates to.
        /// </summary>
        public virtual CaptureMemoryBucketName BucketName { get; set; }

        /// <summary>
        /// Minimum game physical memory usage (in bytes).
        /// </summary>
        public virtual uint MinimumGamePhysical { get; set; }

        /// <summary>
        /// Average game physical memory usage (in bytes).
        /// </summary>
        public virtual uint AverageGamePhysical { get; set; }

        /// <summary>
        /// Maximum game physical memory usage (in bytes).
        /// </summary>
        public virtual uint MaximumGamePhysical { get; set; }

        /// <summary>
        /// Standard deviation of the average game physical memory usage.
        /// </summary>
        public virtual float GamePhysicalStandardDeviation { get; set; }

        /// <summary>
        /// Minimum game virtual memory usage (in bytes).
        /// </summary>
        public virtual uint MinimumGameVirtual { get; set; }

        /// <summary>
        /// Average game virtual memory usage (in bytes).
        /// </summary>
        public virtual uint AverageGameVirtual { get; set; }

        /// <summary>
        /// Maximum game virtual memory usage (in bytes).
        /// </summary>
        public virtual uint MaximumGameVirtual { get; set; }

        /// <summary>
        /// Standard deviation of the average game virtual memory usage.
        /// </summary>
        public virtual float GameVirtualStandardDeviation { get; set; }

        /// <summary>
        /// Minimum resource physical memory usage (in bytes).
        /// </summary>
        public virtual uint MinimumResourcePhysical { get; set; }

        /// <summary>
        /// Average resource physical memory usage (in bytes).
        /// </summary>
        public virtual uint AverageResourcePhysical { get; set; }

        /// <summary>
        /// Maximum resource physical memory usage (in bytes).
        /// </summary>
        public virtual uint MaximumResourcePhysical { get; set; }

        /// <summary>
        /// Standard deviation of the average resource physical memory usage.
        /// </summary>
        public virtual float ResourcePhysicalStandardDeviation { get; set; }

        /// <summary>
        /// Minimum resource virtual memory usage (in bytes).
        /// </summary>
        public virtual uint MinimumResourceVirtual { get; set; }

        /// <summary>
        /// Average resource virtual memory usage (in bytes).
        /// </summary>
        public virtual uint AverageResourceVirtual { get; set; }

        /// <summary>
        /// Maximum resource virtual memory usage (in bytes).
        /// </summary>
        public virtual uint MaximumResourceVirtual { get; set; }

        /// <summary>
        /// Standard deviation of the average resource virtual memory usage.
        /// </summary>
        public virtual float ResourceVirtualStandardDeviation { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CaptureMemoryResult()
            : base()
        {
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <param name="zone"></param>
        public CaptureMemoryResult(CaptureSession session, CaptureZone zone)
            : base(session, zone)
        {
        }
        #endregion // Constructor(s)
    } // CaptureMemoryResult
}
