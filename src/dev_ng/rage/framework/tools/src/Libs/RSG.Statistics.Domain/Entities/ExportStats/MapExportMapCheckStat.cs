﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.ExportStats
{
    /// <summary>
    /// Stat for tracking the map check portion of a map export.
    /// </summary>
    public class MapExportMapCheckStat : MapExportSubStat
    {
    } // MapExportMapCheckStat
}
