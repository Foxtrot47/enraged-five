﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// A shop item is something that you can buy in one of the shops in game.
    /// </summary>
    public class ShopItem : FriendlyGameAssetBase
    {
        /// <summary>
        /// Category this item falls under.
        /// </summary>
        public virtual String Category { get; set; }

        /// <summary>
        /// Sub-category this item falls under.
        /// </summary>
        public virtual String SubCategory { get; set; }

        /// <summary>
        /// Override for the friendly name (for special cases where we have dupped friendly names)
        /// </summary>
        public virtual String FriendlyNameOverride { get; set; }
    } // ShopItem
}
