﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    public class Cutscene : GameAssetBase<CutsceneDto>
    {
        #region Properties
        /// <summary>
        /// Friendly name associated with this cutscene.
        /// </summary>
        public virtual String FriendlyName { get; set; }

        /// <summary>
        /// Optional mission that this cutscene is associated with.
        /// </summary>
        public virtual Mission Mission { get; set; }

        /// <summary>
        /// How long this cutscene is (in seconds).
        /// </summary>
        public virtual float Duration { get; set; }

        /// <summary>
        /// Flag indicating whether this contains branches.
        /// </summary>
        public virtual bool HasBranch { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public Cutscene()
            : base()
        {
        }
        #endregion // Constructor(s)

        #region GameAssetBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public override void Update(CutsceneDto dto)
        {
            base.Update(dto);

            Duration = dto.Duration;
        }
        #endregion // GameAssetBase Overrides
    } // Cutscene
}
