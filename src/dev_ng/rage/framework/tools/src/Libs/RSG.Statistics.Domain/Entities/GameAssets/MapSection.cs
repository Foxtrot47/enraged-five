﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Domain.Entities.ExportStats;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    public class MapSection : GameAssetBase<MapSectionDto>
    {
        #region Properties
        /// <summary>
        /// Type of section this is (e.g. Container, Props, Interiors, etc...)
        /// </summary>
        public virtual String Type { get; set; }

        /// <summary>
        /// List of map export image build's that this section has been a part of.
        /// Lazily loaded.
        /// Required for the many-to-many connection.
        /// </summary>
        public virtual ICollection<MapExportImageBuildStat> MapExportImageBuildStats { get; set; }

        /// <summary>
        /// List of map exports that this section has been a part of.
        /// Lazily loaded.
        /// Required for the many-to-many connection.
        /// </summary>
        public virtual ICollection<MapExportStat> MapExportStats { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MapSection()
            : base()
        {
        }
        #endregion // Constructor(s)

        #region GameAssetBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public override void Update(MapSectionDto dto)
        {
            base.Update(dto);

            Type = dto.Type;
        }
        #endregion // GameAssetBase Overrides
    } // MapSection
}
