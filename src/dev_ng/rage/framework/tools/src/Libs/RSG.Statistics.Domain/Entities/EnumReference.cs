﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.ExportStats;

namespace RSG.Statistics.Domain.Entities
{
    /// <summary>
    /// Reference class for enums
    /// </summary>
    /// <typeparam name="TEnum">Enum type</typeparam>
    public class EnumReference<TEnum> : IDomainEntity where TEnum : struct
    {
        private TEnum? m_value;
        private string m_name;

        public EnumReference(TEnum enm)
        {
            this.m_value = enm;
            this.m_name = enm.ToString();
        }

        public EnumReference()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual long Id
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual DateTime CreatedOn
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual string Name
        {
            get
            {
                return m_name;
            }
            set
            {
                TEnum val;
                if (Enum.TryParse(value, true, out val))
                {
                    m_value = val;
                }
                else
                {
                    m_value = null;
                }
                m_name = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual TEnum? Value
        {
            get { return m_value; }
            set
            {
                if (m_name == null)
                {
                    m_value = value;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override string ToString()
        {
            return Value.ToString();
        }


        /// <summary>
        /// HACK.  This gets added to all enums :[
        /// List of map exports that this platform has been a part of.
        /// Lazily loaded.
        /// Required for the many-to-many connection.
        /// </summary>
        public virtual ICollection<MapExportStat> MapExportStats { get; set; }
    } // Reference<TEnum>
}
