﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public class SpawnPointStat : DomainEntityBase
    {
        /// <summary>
        /// Level stat this spawn point is associated with (if any).
        /// </summary>
        public virtual LevelStat LevelStat { get; set; }

        /// <summary>
        /// Archetype stat this spawn point is associated with (if any).
        /// </summary>
        public virtual ArchetypeStat ArchetypeStat { get; set; }

        /// <summary>
        /// Name of the spawn point.
        /// </summary>
        public virtual String Name { get; set; }

        /// <summary>
        /// Position of this spawn point (if its related to an archetype stat its relative to the archetype otherwise its absolute).
        /// </summary>
        public virtual GeoAPI.Geometries.IPoint Position { get; set; }

        /// <summary>
        /// Source file this spawn point came from.
        /// </summary>
        public virtual string SourceFile { get; set; }

        /// <summary>
        /// Type of spawn point this is.
        /// </summary>
        public virtual string SpawnType { get; set; }

        /// <summary>
        /// Group spawn point this in.
        /// </summary>
        public virtual string SpawnGroup { get; set; }

        /// <summary>
        /// Model set associated with the spawn type.
        /// </summary>
        public virtual string ModelSet { get; set; }

        /// <summary>
        /// Game modes this spawn point is enabled in.
        /// </summary>
        public virtual EnumReference<SpawnPointAvailableModes> AvailabilityMode { get; set; }

        /// <summary>
        /// Start time active.
        /// </summary>
        public virtual int? StartTime { get; set; }

        /// <summary>
        /// End time active.
        /// </summary>
        public virtual int? EndTime { get; set; }
    } // SpawnPointStat
}
