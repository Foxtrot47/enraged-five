﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.Captures
{
    /// <summary>
    /// Streaming memory category name.
    /// </summary>
    public class CaptureStreamingMemoryCategoryName : NameHashBase
    {
    } // CaptureStreamingMemoryCategoryName
}
