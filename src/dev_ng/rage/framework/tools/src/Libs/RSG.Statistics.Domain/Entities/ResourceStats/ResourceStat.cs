﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ModelPlatform = RSG.Model.Statistics.Platform;

namespace RSG.Statistics.Domain.Entities.ResourceStats
{
    /// <summary>
    /// ResourceStat Domain object
    /// </summary>
    public class ResourceStat : DomainEntityBase
    {
        /// <summary>
        /// Platform
        /// </summary>
        public virtual EnumReference<RSG.Platform.Platform> Platform { get; set; }

        /// <summary>
        /// FileType
        /// </summary>
        public virtual EnumReference<RSG.Platform.FileType> FileType { get; set; }

        /// <summary>
        /// Resource Name
        /// </summary>
        public virtual ResourceName ResourceName { get; set; }

        /// <summary>
        /// Resource source filename.
        /// </summary>
        public virtual String SourceFilename { get; set; }

        /// <summary>
        /// Resource destination filename.
        /// </summary>
        public virtual String DestinationFilename { get; set; }

        /// <summary>
        /// Time when this was processed.
        /// </summary>
        public virtual DateTime Timestamp { get; set; }

        /// <summary>
        /// The list of buckets
        /// </summary>
        public virtual IList<ResourceBucket> Buckets { get; set; }

        #region Constructors
        /// <summary>
        /// Parameterless constructor
        /// - required.
        /// </summary>
        public ResourceStat()
            :base()
        {
        }
        #endregion // Constructors

        #region Conversion to Model
        /// <summary>
        /// Get model 'helper'
        /// </summary>
        /// <returns></returns>
        public virtual RSG.Model.Statistics.Platform.ResourceStat GetModel()
        {
            RSG.Platform.FileType fileType = (RSG.Platform.FileType)Enum.Parse(typeof(RSG.Platform.FileType), FileType.Name);
            RSG.Platform.Platform plat = (RSG.Platform.Platform)Enum.Parse(typeof(RSG.Platform.Platform), Platform.Name);

            ICollection<ModelPlatform.ResourceBucket> buckets = new List<ModelPlatform.ResourceBucket>();
            foreach (ResourceBucket bucketDomain in Buckets)
            {
                ModelPlatform.ResourceBucket bucket = bucketDomain.GetModel();
                buckets.Add(bucket);
            }
            ModelPlatform.ResourceStat resultStat = new ModelPlatform.ResourceStat(
                ResourceName.Name, this.SourceFilename, this.DestinationFilename,
                fileType, plat, buckets);
            return resultStat;
        }
        #endregion // Conversion to Model
    }
}
