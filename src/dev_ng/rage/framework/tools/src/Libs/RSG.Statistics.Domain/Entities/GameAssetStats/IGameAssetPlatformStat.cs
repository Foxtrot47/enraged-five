﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// Interface all platform based stats should adhere to.
    /// </summary>
    /// <typeparam name="TAsset"></typeparam>
    /// <typeparam name="TAssetStat"></typeparam>
    public interface IGameAssetPlatformStat<TAsset, TAssetStat>
        where TAsset : IGameAsset
        where TAssetStat : IGameAssetStat<TAsset>
    {
        /// <summary>
        /// Platform this stat is for.
        /// </summary>
        EnumReference<RSG.Platform.Platform> Platform { get; set; }

        /// <summary>
        /// Physical size for this stat.
        /// </summary>
        ulong PhysicalSize { get; set; }

        /// <summary>
        /// Virtual size for this stat.
        /// </summary>
        ulong VirtualSize { get; set; }

        /// <summary>
        /// Asset stat that this platform stat is for.
        /// </summary>
        TAssetStat AssetStat { get; set; }
    } // IGameAssetPlatformStat<TAsset, TAssetStat>
}
