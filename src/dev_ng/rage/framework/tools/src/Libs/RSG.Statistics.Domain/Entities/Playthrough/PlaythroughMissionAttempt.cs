﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.Playthrough
{
    /// <summary>
    /// Tracks playthrough mission attempt information.
    /// </summary>
    public class PlaythroughMissionAttempt : DomainEntityBase, IHasModifiedInformation
    {
        #region Properties
        /// <summary>
        /// When this entry was last modified.
        /// </summary>
        public virtual DateTime ModifiedOn { get; set; }

        /// <summary>
        /// Unique identifier that the game has given this stat.
        /// </summary>
        public virtual ulong UniqueIdentifier { get; set; }

        /// <summary>
        /// Mission this stat is associated with.
        /// </summary>
        public virtual Mission Mission { get; set; }

        /// <summary>
        /// Playthrough session this attempt was a part of.
        /// </summary>
        public virtual PlaythroughSession PlaythroughSession { get; set; }

        /// <summary>
        /// When the mission started.
        /// </summary>
        public virtual DateTime Start { get; set; }

        /// <summary>
        /// When the mission ended.
        /// </summary>
        public virtual DateTime? End { get; set; }

        /// <summary>
        /// How long the player spent on the mission (this takes into account time the user was at the pause screen).
        /// </summary>
        public virtual uint TimeToComplete { get; set; }

        /// <summary>
        /// The result of the mission.
        /// Can be null if we never received the mission "finished" telemetry event.
        /// </summary>
        public virtual EnumReference<RSG.Model.Common.Mission.MissionAttemptResult> Result { get; set; }

        /// <summary>
        /// Comment associated with this mission attempt.
        /// Only populated when the mission was performed as part of a playthrough.
        /// </summary>
        public virtual String Comment { get; set; }

        /// <summary>
        /// Flag indicating whether this attempt should be ignored.
        /// </summary>
        public virtual bool Ignore { get; set; }

        /// <summary>
        /// Rating oto associate with this mission.
        /// </summary>
        public virtual uint? Rating { get; set; }

        /// <summary>
        /// Checkpoints that were attempted during this mission attempt.
        /// </summary>
        public virtual IList<PlaythroughCheckpointAttempt> CheckpointAttempts { get; set; }
        #endregion // Properties
    } // PlaythroughMissionAttempt
}
