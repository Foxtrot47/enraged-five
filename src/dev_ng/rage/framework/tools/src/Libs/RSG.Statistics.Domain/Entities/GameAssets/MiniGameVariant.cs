﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// Variation of a mini-game.
    /// </summary>
    public class MiniGameVariant : FriendlyGameAssetBase
    {
        /// <summary>
        /// Match type for this mini-game.
        /// </summary>
        public virtual EnumReference<MatchType> MatchType { get; set; }

        /// <summary>
        /// X coordinate for this mini game.
        /// </summary>
        public virtual float X { get; set; }

        /// <summary>
        /// Y coordinate for this mini game.
        /// </summary>
        public virtual float Y { get; set; }

        /// <summary>
        /// Z coordinate for this mini game.
        /// </summary>
        public virtual float Z { get; set; }
    } // MiniGameVariant
}
