﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.ResourceStats
{
    /// <summary>
    /// Names of resources
    /// - without extension
    /// </summary>
    public class ResourceName : GameAssetBase
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ResourceName()
            : base()
        {            
        }
        #endregion // Constructor(s)
    }
}
