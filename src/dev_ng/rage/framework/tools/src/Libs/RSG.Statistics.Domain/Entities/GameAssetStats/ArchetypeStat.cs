﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Model.Common;
using RSG.Model.Common.Map;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class ArchetypeStat : GameAssetStatBase
    {
        /// <summary>
        /// Archetype this stat is for.
        /// </summary>
        public virtual Archetype Archetype { get; set; }

        /// <summary>
        /// Map section this stat is for.
        /// </summary>
        public virtual MapSectionStat MapSectionStat { get; set; }

        /// <summary>
        /// Bounding box min point.
        /// NHibernate spatial doesn't appear to support bounding boxes...wtf?!?
        /// so going to use two points instead.
        /// </summary>
        [StreamableStatAttribute(StreamableMapArchetypeStat.BoundingBoxString)]
        public virtual GeoAPI.Geometries.IPoint BoundingBoxMin { get; set; }

        /// <summary>
        /// Bounding box max point.
        /// </summary>
        [StreamableStatAttribute(StreamableMapArchetypeStat.BoundingBoxString)]
        public virtual GeoAPI.Geometries.IPoint BoundingBoxMax { get; set; }

        /// <summary>
        /// Archetype's lod distance.
        /// </summary>
        [StreamableStatAttribute(StreamableArchetypeStat.LodDistanceString)]
        public virtual float LodDistance { get; set; }

        /// <summary>
        /// Size of the exported geometry.
        /// </summary>
        [StreamableStatAttribute(StreamableMapArchetypeStat.ExportGeometrySizeString)]
        public virtual uint ExportGeometrySize { get; set; }

        /// <summary>
        /// List of texture dictionaries and their associated export sizes.
        /// </summary>
        [StreamableStatAttribute(StreamableMapArchetypeStat.TxdExportSizesString)]
        public virtual IList<TxdExportSizeStat<ArchetypeStat>> TxdExportSizes { get; set; }

        /// <summary>
        /// Archetype's polygon count.
        /// </summary>
        [StreamableStatAttribute(StreamableMapArchetypeStat.PolygonCountString)]
        public virtual uint PolygonCount { get; set; }

        /// <summary>
        /// Collision polygon count.
        /// </summary>
        [StreamableStatAttribute(StreamableMapArchetypeStat.CollisionPolygonCountString)]
        public virtual uint CollisionPolygonCount { get; set; }

        /// <summary>
        /// List of collision poly counts associated with this map section stat.
        /// </summary>
        [StreamableStatAttribute(StreamableMapArchetypeStat.CollisionTypePolygonCountsString)]
        public virtual IList<CollisionPolyStat<ArchetypeStat>> CollisionTypePolygonCounts { get; set; }

        /// <summary>
        /// Whether the archetype has a light attached to it.
        /// </summary>
        [StreamableStatAttribute(StreamableMapArchetypeStat.HasAttachedLightString)]
        public virtual bool HasAttachedLight { get; set; }

        /// <summary>
        /// Whether the archetype has an explosive effect attached to it.
        /// </summary>
        [StreamableStatAttribute(StreamableMapArchetypeStat.HasExplosiveEffectString)]
        public virtual bool HasExplosiveEffect { get; set; }

        /// <summary>
        /// List of stated anims that this archetypes is present in.
        /// Lazily loaded, inverse end.
        /// Do not add items to this collection expecting them to be persisted in the database after an Update()!
        /// </summary>
        public virtual ICollection<ArchetypeStat> StatedAnimStats { get; set; }
    } // ArchetypeStat
}
