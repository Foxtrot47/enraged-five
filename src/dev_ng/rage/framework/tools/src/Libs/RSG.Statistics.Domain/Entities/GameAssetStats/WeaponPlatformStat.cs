﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public class WeaponPlatformStat : GameAssetPlatformStatBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public virtual WeaponStat WeaponStat
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public WeaponPlatformStat()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // WeaponPlatformStat
}
