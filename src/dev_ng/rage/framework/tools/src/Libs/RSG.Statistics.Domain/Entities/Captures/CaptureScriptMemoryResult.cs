﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.Captures
{
    /// <summary>
    /// Information relating to script memory results.
    /// </summary>
    public class CaptureScriptMemoryResult : CaptureResultBase
    {
        #region Properties
        /// <summary>
        /// Minimum physical memory usage (in bytes).
        /// </summary>
        public virtual uint MinimumPhysical { get; set; }

        /// <summary>
        /// Average physical memory usage (in bytes).
        /// </summary>
        public virtual uint AveragePhysical { get; set; }

        /// <summary>
        /// Maximum physical memory usage (in bytes).
        /// </summary>
        public virtual uint MaximumPhysical { get; set; }

        /// <summary>
        /// Standard deviation of the average physical memory usage.
        /// </summary>
        public virtual float PhysicalStandardDeviation { get; set; }

        /// <summary>
        /// Minimum virtual memory usage (in bytes).
        /// </summary>
        public virtual uint MinimumVirtual { get; set; }

        /// <summary>
        /// Average virtual memory usage (in bytes).
        /// </summary>
        public virtual uint AverageVirtual { get; set; }

        /// <summary>
        /// Maximum virtual memory usage (in bytes).
        /// </summary>
        public virtual uint MaximumVirtual { get; set; }

        /// <summary>
        /// Standard deviation of the average virtual memory usage.
        /// </summary>
        public virtual float VirtualStandardDeviation { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CaptureScriptMemoryResult()
            : base()
        {
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <param name="zone"></param>
        public CaptureScriptMemoryResult(CaptureSession session, CaptureZone zone)
            : base(session, zone)
        {
        }
        #endregion // Constructor(s)
    } // CaptureScriptMemoryResult
}
