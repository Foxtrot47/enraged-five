﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.Captures
{
    /// <summary>
    /// Statistics from a cpu capture telemetry submission.
    /// </summary>
    public class CaptureCpuResult : CaptureResultBase
    {
        #region Properties
        /// <summary>
        /// Set that this capture was taken in (Main/Render/Gpu)
        /// </summary>
        public virtual CaptureCpuSet Set { get; set; }

        /// <summary>
        /// Name of the metric that was captured.
        /// </summary>
        public virtual CaptureCpuMetric Metric { get; set; }

        /// <summary>
        /// Minimum cpu duration (in milliseconds).
        /// </summary>
        public virtual float Minimum { get; set; }

        /// <summary>
        /// Average cpu duration (in milliseconds).
        /// </summary>
        public virtual float Average { get; set; }

        /// <summary>
        /// Maximum cpu duration (in milliseconds).
        /// </summary>
        public virtual float Maximum { get; set; }

        /// <summary>
        /// Standard deviation of the average cpu duration.
        /// </summary>
        public virtual float StandardDeviation { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CaptureCpuResult()
            : base()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <param name="zone"></param>
        public CaptureCpuResult(CaptureSession session, CaptureZone zone)
            : base(session, zone)
        {
        }
        #endregion // Constructor(s)
    } // CaptureCpu
}
