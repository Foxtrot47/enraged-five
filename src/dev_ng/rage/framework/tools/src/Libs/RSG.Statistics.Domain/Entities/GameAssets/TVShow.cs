﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// Game asset for TV shows.
    /// </summary>
    public class TVShow : FriendlyGameAssetBase
    {
    } // TVShow
}
