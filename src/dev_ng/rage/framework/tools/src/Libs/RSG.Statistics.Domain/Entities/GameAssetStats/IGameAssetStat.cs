﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// Interface that all build based stats should adhere to.
    /// MET: This is needed due to GameAssetStatBase being set up incorrectly :/.
    ///      Any new game asset stats should inherit from the generic version below.
    ///      
    ///      This will eventually be removed, so don't use it!
    /// </summary>
    public interface IGameAssetStat : IDomainEntity
    {
        /// <summary>
        /// Build this stat is for.
        /// </summary>
        Build Build { get; set; }
    } // IGameAssetStat


    /// <summary>
    /// Interface that all build based stats should adhere to.
    /// </summary>
    public interface IGameAssetStat<TAsset> : IDomainEntity where TAsset : IGameAsset
    {
        /// <summary>
        /// Build this stat is for.
        /// </summary>
        Build Build { get; set; }

        /// <summary>
        /// Asset that this stat is for.
        /// </summary>
        TAsset Asset { get; set; }
    } // IGameAssetStat<TAsset>
}
