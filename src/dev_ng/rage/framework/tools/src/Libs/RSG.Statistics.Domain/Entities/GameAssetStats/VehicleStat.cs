﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public class VehicleStat : GameAssetStatBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public virtual Vehicle Vehicle { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int PolyCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int CollisionCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int BoneCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int DoorCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int ElevatorCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int ExtraCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int HandlebarCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual bool HasLod1 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual bool HasLod2 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int LightCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int PropellerCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int RotorCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int RudderCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int SeatCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int WheelCount { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual IList<VehiclePlatformStat> VehiclePlatformStats { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public VehicleStat()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // VehicleStat
}
