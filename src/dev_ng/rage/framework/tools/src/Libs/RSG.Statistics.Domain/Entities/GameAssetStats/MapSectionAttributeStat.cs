﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public class MapSectionAttributeStat : DomainEntityBase
    {
        #region Properties
        /// <summary>
        /// Link to the map section stat that this is for.
        /// </summary>
        public virtual MapSectionStat MapSectionStat { get; set; }

        /// <summary>
        /// Name of the attribute.
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// String representation of the attribute type.
        /// </summary>
        public virtual string Type { get; set; }

        /// <summary>
        /// String containing the serialised value.
        /// </summary>
        public virtual string Value { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        internal MapSectionAttributeStat()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="type"></param>
        /// <param name="value"></param>
        public MapSectionAttributeStat(MapSectionStat stat, string name, string type, string value)
        {
            MapSectionStat = stat;
            Name = name;
            Type = type;
            Value = value;
        }
        #endregion // Constructor(s)
    } // MapSectionAttributeStat
}
