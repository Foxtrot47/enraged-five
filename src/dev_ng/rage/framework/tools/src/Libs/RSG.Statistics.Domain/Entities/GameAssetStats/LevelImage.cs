﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public class LevelImage : DomainEntityBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public virtual long BugstarId
        {
            get;
            set;
        }

        /// <summary>
        /// Byte data for the image
        /// </summary>
        public virtual byte[] ImageData
        {
            get;
            set;
        }
        #endregion // Properties
    } // LevelImage
}
