﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public class PortalStat : DomainEntityBase
    {
        #region Properties
        /// <summary>
        /// Interior stat this portal is associated with.
        /// </summary>
        public virtual InteriorArchetypeStat InteriorStat { get; set; }

        /// <summary>
        /// Name of the portal.
        /// </summary>
        public virtual String Name { get; set; }

        /// <summary>
        /// Source room.
        /// </summary>
        public virtual RoomStat RoomStatA { get; set; }

        /// <summary>
        /// Destination room.
        /// </summary>
        public virtual RoomStat RoomStatB { get; set; }
        #endregion // Properties
    } // PortalStat
}
