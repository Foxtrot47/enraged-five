﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;
using System.Xml.Serialization;
using RSG.Statistics.Common.Dto.GameAssetStats.Bundles;
using RSG.Statistics.Common.Dto.GameAssetStats;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using GeoAPI.Geometries;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// Map section statistic for a particular section/build.
    /// </summary>
    public class MapSectionStat : MapNodeStatBase
    {
        #region Properties
        /// <summary>
        /// Link to the map section that this stat is for.
        /// </summary>
        public virtual MapSection MapSection { get; set; }

        /// <summary>
        /// The section's vector map for this build.
        /// </summary>
        public virtual IPolygon VectorMapPoints { get; set; }

        /// <summary>
        /// Whether the section exports archetypes.
        /// </summary>
        [StreamableStatAttribute(StreamableMapSectionStat.ExportArchetypesString)]
        public virtual bool? ExportArchetypes { get; set; }

        /// <summary>
        /// Whether the section exports entities.
        /// </summary>
        [StreamableStatAttribute(StreamableMapSectionStat.ExportEntitiesString)]
        public virtual bool? ExportEntities { get; set; }

        /// <summary>
        /// Map sections top level collision poly count.
        /// </summary>
        [StreamableStatAttribute(StreamableMapSectionStat.CollisionPolygonCountString)]
        public virtual uint? CollisionPolygonCount { get; set; }

        /// <summary>
        /// List of collision poly counts associated with this map section stat.
        /// </summary>
        [StreamableStatAttribute(StreamableMapSectionStat.CollisionPolygonCountBreakdownString)]
        public virtual IList<CollisionPolyStat<MapSectionStat>> CollisionTypePolygonCounts { get; set; }

        /// <summary>
        /// Last person to export this section.
        /// </summary>
        [StreamableStatAttribute(StreamableMapSectionStat.LastExportUserString)]
        public virtual String LastExportUser { get; set; }

        /// <summary>
        /// Last time this section was exported.
        /// </summary>
        [StreamableStatAttribute(StreamableMapSectionStat.LastExportTimeString)]
        public virtual DateTime? LastExportTime { get; set; }

        /// <summary>
        /// DCC source filename (excluding extension).
        /// </summary>
        [StreamableStatAttribute(StreamableMapSectionStat.DCCSourceFilenameString)]
        public virtual String DCCSourceFilename { get; set; }

        /// <summary>
        /// Name of the exported zip file.
        /// </summary>
        [StreamableStatAttribute(StreamableMapSectionStat.ExportZipFilepathString)]
        public virtual String ExportZipFilepath { get; set; }

        /// <summary>
        /// List of archetypes this map section contains.
        /// </summary>
        [StreamableStatAttribute(StreamableMapSectionStat.ArchetypesString)]
        public virtual IList<ArchetypeStat> Archetypes { get; set; }

        /// <summary>
        /// List of entities this map section contains.
        /// </summary>
        [StreamableStatAttribute(StreamableMapSectionStat.EntitiesString)]
        public virtual IList<EntityStat> Entities { get; set; }

        /// <summary>
        /// List of car gens this map section contains.
        /// </summary>
        [StreamableStatAttribute(StreamableMapSectionStat.CarGensString)]
        public virtual IList<CarGenStat> CarGens { get; set; }

        /// <summary>
        /// Per platform memory stats for this section stat.
        /// </summary>
        [StreamableStatAttribute(StreamableMapSectionStat.PlatformStatsString)]
        public virtual IList<MapSectionPlatformStat> MapSectionPlatformStats { get; set; }

        /// <summary>
        /// List of attributes this section has.
        /// </summary>
        [StreamableStatAttribute(StreamableMapSectionStat.ContainerAttributesString)]
        public virtual IList<MapSectionAttributeStat> Attributes { get; set; }

        /// <summary>
        /// List of attributes this section has.
        /// </summary>
        [StreamableStatAttribute(StreamableMapSectionStat.AggregatedStatsString)]
        public virtual IList<MapSectionAggregateStat> AggregatedStats { get; set; }
        #endregion // Properties

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public virtual void Update(MapSectionStatBundleDto bundle)
        {
            ExportArchetypes = bundle.ExportArchetypes;
            ExportEntities = bundle.ExportEntities;
            CollisionPolygonCount = bundle.CollisionPolygonCount;
            LastExportUser = bundle.LastExportUser;
            LastExportTime = bundle.LastExportTime;
            DCCSourceFilename = bundle.DCCSourceFilename;
            ExportZipFilepath = bundle.ExportZipFilepath;

            // Collision Types
            CollisionTypePolygonCounts = new List<CollisionPolyStat<MapSectionStat>>();
            foreach (CollisionPolyStatDto polyStatDto in bundle.CollisionTypePolygonCounts)
            {
                CollisionTypePolygonCounts.Add(new CollisionPolyStat<MapSectionStat>(this, polyStatDto.CollisionFlags, polyStatDto.PolygonCount));
            }

            // Attributes
            Attributes = new List<MapSectionAttributeStat>();
            foreach (MapSectionAttributeStatDto attributeStatDto in bundle.Attributes)
            {
                Attributes.Add(new MapSectionAttributeStat(this, attributeStatDto.Name, attributeStatDto.Type, attributeStatDto.Value));
            }

            // MapSectionPlatformStats and AggregateStats can't be updated here due to needing the repository locator for resolving enums.
        }
        #endregion // Public Methods
    } // MapSectionStat
}
