﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.ROS;
using RSG.Statistics.Domain.Entities.Vertica;

namespace RSG.Statistics.Domain.Entities.Playthrough
{
    /// <summary>
    /// 
    /// </summary>
    public class PlaythroughUser : DomainEntityBase, IHasModifiedInformation
    {
        #region Properties
        /// <summary>
        /// When this user was last modified.
        /// </summary>
        public virtual DateTime ModifiedOn { get; set; }

        /// <summary>
        /// Name of the user.
        /// </summary>
        public virtual String Name { get; set; }

        /// <summary>
        /// Gamer handle that the player was using when playing the game.
        /// </summary>
        public virtual String GamerHandle { get; set; }

        /// <summary>
        /// Platform this user is associated with.
        /// </summary>
        public virtual EnumReference<ROSPlatform> Platform { get; set; }

        /// <summary>
        /// Version of the leaderboards that the version of the game they were playing was on.
        /// </summary>
        public virtual uint? LeaderboardVersion { get; set; }
        #endregion // Properties
    } // PlaythroughUser
}
