﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.Playthrough
{
    /// <summary>
    /// Tracks playthrough checkpoint attempt information.
    /// </summary>
    public class PlaythroughCheckpointAttempt : DomainEntityBase, IHasModifiedInformation
    {
        #region Properties
        /// <summary>
        /// When this entry was last modified.
        /// </summary>
        public virtual DateTime ModifiedOn { get; set; }

        /// <summary>
        /// Mission this stat is associated with.
        /// </summary>
        public virtual PlaythroughMissionAttempt PlaythroughMissionAttempt { get; set; }

        /// <summary>
        /// Checkpoint this stat is for.
        /// </summary>
        public virtual MissionCheckpoint Checkpoint { get; set; }

        /// <summary>
        /// When the mission started.
        /// </summary>
        public virtual DateTime Start { get; set; }

        /// <summary>
        /// When the mission ended.
        /// </summary>
        public virtual DateTime End { get; set; }

        /// <summary>
        /// How long the player spent on the checkpoint (this takes into account time the user was at the pause screen).
        /// </summary>
        public virtual uint TimeToComplete { get; set; }

        /// <summary>
        /// Comment associated with this mission attempt.
        /// Only populated when the mission was performed as part of a playthrough.
        /// </summary>
        public virtual String Comment { get; set; }

        /// <summary>
        /// Flag indicating whether this attempt should be ignored.
        /// </summary>
        public virtual bool Ignore { get; set; }
        #endregion // Properties
    } // PlaythroughCheckpointAttempt
}
