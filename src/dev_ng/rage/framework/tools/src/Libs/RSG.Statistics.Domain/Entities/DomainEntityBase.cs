﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto;

namespace RSG.Statistics.Domain.Entities
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class DomainEntityBase : IDomainEntity, IComparable<DomainEntityBase>
    {
        #region Member Data
        /// <summary>
        /// Used for transient entities that haven't been persisted to the database yet.
        /// </summary>
        protected internal virtual Guid InternalId { get; set; }
        #endregion // Member Data

        #region Properties
        /// <summary>
        /// Unique id for this entity.
        /// </summary>
        public virtual long Id { get; set; }

        /// <summary>
        /// When this entity was created.
        /// </summary>
        public virtual DateTime CreatedOn { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        protected DomainEntityBase()
        {
            InternalId = Guid.NewGuid();
        }
        #endregion // Constructor(s)

        #region Object Overrides
        /// <summary>
        /// 
        /// </summary>
        public override bool Equals(object other)
        {
            return (other is DomainEntityBase ? CompareTo(other as DomainEntityBase) == 0 : false);
        }

        public override int GetHashCode()
        {
            return InternalId.GetHashCode();
        }
        #endregion // Object Overrides

        #region IComparable<DomainEntityBase> Members
        /// <summary>
        /// Compares two domain entities.
        /// </summary>
        public virtual int CompareTo(DomainEntityBase other)
        {
            if (Id == 0 || other.Id == 0)
            {
                return InternalId.CompareTo(other.InternalId);
            }

            return Id.CompareTo(other.Id);
        }
        #endregion
    } // EntityBase
}
