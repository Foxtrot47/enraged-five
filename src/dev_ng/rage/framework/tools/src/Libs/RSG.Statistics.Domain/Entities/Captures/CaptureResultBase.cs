﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.Captures
{
    /// <summary>
    /// Abstract base class that contains data common to all captures.
    /// </summary>
    public abstract class CaptureResultBase : DomainEntityBase
    {
        #region Properties
        /// <summary>
        /// Capture session this result is associated with.
        /// </summary>
        public virtual CaptureSession Session { get; set; }

        /// <summary>
        /// Zone that this capture was taken in.
        /// </summary>
        public virtual CaptureZone Zone { get; set; }

        /// <summary>
        /// Where the user was on the map when this result occurred.
        /// </summary>
        public virtual GeoAPI.Geometries.IPoint Location { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        protected CaptureResultBase()
            : base()
        {
        }

        /// <summary>
        /// Convenience constructor.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="zone"></param>
        protected CaptureResultBase(CaptureSession session, CaptureZone zone)
            : base()
        {
            Session = session;
            Zone = zone;
        }
        #endregion // Constructor(s)
    } // CaptureBase
}
