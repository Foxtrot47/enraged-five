﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.Captures
{
    /// <summary>
    /// Information relating to a single streaming memory result
    /// </summary>
    public class CaptureStreamingMemoryResult : CaptureResultBase
    {
        #region Properties
        /// <summary>
        /// Module this result is for.
        /// </summary>
        public virtual CaptureStreamingMemoryModuleName Module { get; set; }

        /// <summary>
        /// Category this result is a part of.
        /// </summary>
        public virtual CaptureStreamingMemoryCategoryName Category { get; set; }

        /// <summary>
        /// Virtual memory usage (in bytes).
        /// </summary>
        public virtual uint VirtualMemory { get; set; }

        /// <summary>
        /// Physical memory usage (in bytes).
        /// </summary>
        public virtual uint PhysicalMemory { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CaptureStreamingMemoryResult()
            : base()
        {
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <param name="zone"></param>
        public CaptureStreamingMemoryResult(CaptureSession session, CaptureZone zone)
            : base(session, zone)
        {
        }
        #endregion // Constructor(s)
    } // CaptureStreamingMemoryResult
}
