﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    public class MissionCheckpoint : GameAssetBase
    {
        #region Properties
        /// <summary>
        /// Mission this checkpoint is associated with.
        /// </summary>
        public virtual Mission Mission { get; set; }

        /// <summary>
        /// Number of attempts we expect this checkpoint to require.
        /// </summary>
        public virtual float ProjectedAttempts { get; set; }

        /// <summary>
        /// Minimum number of attempts we expect this checkpoint to require.
        /// </summary>
        public virtual float ProjectedAttemptsMin { get; set; }

        /// <summary>
        /// Maximum number of attempts we expect this checkpoint to require.
        /// </summary>
        public virtual float ProjectedAttemptsMax { get; set; }

        /// <summary>
        /// Whether bugstar knows about this checkpoint.
        /// </summary>
        public virtual bool InBugstar { get; set; }

        /// <summary>
        /// Index for the checkpoint.
        /// Can be null if the checkpoint is no longer active.
        /// </summary>
        public virtual uint? Index { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public MissionCheckpoint()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // MissionCheckpoint
}
