﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    public class Character : GameAssetBase<CharacterDto>
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public virtual string Category
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public Character()
            : base()
        {
        }
        #endregion // Constructor(s)

        #region GameAssetBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public override void Update(CharacterDto dto)
        {
            base.Update(dto);

            Category = dto.Category;
        }
        #endregion // GameAssetBase Overrides
    } // Character
}
