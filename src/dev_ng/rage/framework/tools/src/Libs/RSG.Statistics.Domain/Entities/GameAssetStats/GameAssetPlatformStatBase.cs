﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// OBSOLETE: Prefer the generic version below.
    /// This will eventually be removed.
    /// </summary>
    public class GameAssetPlatformStatBase : DomainEntityBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public virtual EnumReference<RSG.Platform.Platform> Platform { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual uint PhysicalSize { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual uint VirtualSize { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        protected GameAssetPlatformStatBase()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // GameAssetPlatformStatBase


    /// <summary>
    /// Base class for all platform stats.
    /// </summary>
    /// <typeparam name="TAssetStat"></typeparam>
    public abstract class GameAssetPlatformStatBase<TAsset, TAssetStat> : DomainEntityBase, IGameAssetPlatformStat<TAsset, TAssetStat>
        where TAsset : IGameAsset
        where TAssetStat : IGameAssetStat<TAsset>
    {
        #region Properties
        /// <summary>
        /// Platform this stat is for.
        /// </summary>
        public virtual EnumReference<RSG.Platform.Platform> Platform { get; set; }

        /// <summary>
        /// Physical size for this stat.
        /// </summary>
        public virtual ulong PhysicalSize { get; set; }

        /// <summary>
        /// Virtual size for this stat.
        /// </summary>
        public virtual ulong VirtualSize { get; set; }

        /// <summary>
        /// Asset stat that this platform stat is for.
        /// </summary>
        public virtual TAssetStat AssetStat { get; set; }

        /// <summary>
        /// Path to the rpf file this data is in.
        /// </summary>
        public virtual String DataPath { get; set; }
        #endregion // Properties

    } // GameAssetPlatformStatBase<TAssetStat>
}
