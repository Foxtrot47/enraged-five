﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// Information regarding a single poly count stat.
    /// </summary>
    public class TxdExportSizeStat<T> : DomainEntityBase where T : DomainEntityBase
    {
        #region Properties
        /// <summary>
        /// Link to the map section stat that this is for.
        /// </summary>
        public virtual T ReferencedStat { get; set; }

        /// <summary>
        /// Texture dictionary name.
        /// </summary>
        public virtual string TxdName { get; set; }

        /// <summary>
        /// Size of the txd.
        /// </summary>
        public virtual uint Size { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        internal TxdExportSizeStat()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="name"></param>
        /// <param name="size"></param>
        public TxdExportSizeStat(T stat, string name, uint size)
        {
            ReferencedStat = stat;
            TxdName = name;
            Size = size;
        }
        #endregion // Constructor(s)
    } // TxdExportSizeStat<T>
}
