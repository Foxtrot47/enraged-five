﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// Categories that XP gain can fall under.
    /// </summary>
    public class XPCategory : FriendlyGameAssetBase
    {
    } // XPCategory
}
