﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class Archetype : DomainEntityBase, IHasModifiedInformation
    {
        #region Properties
        /// <summary>
        /// Information about when this domain entity was last modified.
        /// </summary>
        public virtual DateTime ModifiedOn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual string Identifier { get; set; }
        #endregion // Properties

    } // Archetype<TDto>

    /// <summary>
    /// 
    /// </summary>
    public class DrawableArchetype : Archetype
    {
    } // DrawableArchetype

    /// <summary>
    /// 
    /// </summary>
    public class FragmentArchetype : Archetype
    {
    } // FragmentArchetype

    /// <summary>
    /// 
    /// </summary>
    public class InteriorArchetype : Archetype
    {
    } // InteriorArchetype

    /// <summary>
    /// 
    /// </summary>
    public class StatedAnimArchetype : Archetype
    {
    } // StatedAnimArchetype
}
