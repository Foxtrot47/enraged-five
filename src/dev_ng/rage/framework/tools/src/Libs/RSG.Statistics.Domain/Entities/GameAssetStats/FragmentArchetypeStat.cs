﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Model.Common.Map;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public class FragmentArchetypeStat : SimpleMapArchetypeStat
    {
        /// <summary>
        /// Number of fragments this archetype is made up of.
        /// </summary>
        [StreamableStatAttribute(StreamableFragmentArchetypeStat.FragmentCountString)]
        public virtual uint FragmentCount { get; set; }

        /// <summary>
        /// Whether this fragment is a piece of cloth.
        /// </summary>
        [StreamableStatAttribute(StreamableFragmentArchetypeStat.IsClothString)]
        public virtual bool IsCloth { get; set; }
    } // FragmentArchetypeStat
}
