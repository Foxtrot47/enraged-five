﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.Playthrough
{
    /// <summary>
    /// 
    /// </summary>
    public class PlaythroughComment : DomainEntityBase
    {
        /// <summary>
        /// Playthrough session this comment should be attributed to.
        /// </summary>
        public virtual PlaythroughSession PlaythroughSession { get; set; }

        /// <summary>
        /// Generic comment on something.
        /// </summary>
        public virtual String Comment { get; set; }

        /// <summary>
        /// Optional mission this comment is for.
        /// </summary>
        public virtual Mission Mission { get; set; }

        /// <summary>
        /// Optional weapon this comment is for.
        /// </summary>
        public virtual Weapon Weapon { get; set; }

        /// <summary>
        /// Optional vehicle this comment is for.
        /// </summary>
        public virtual Vehicle Vehicle { get; set; }
    } // PlaythroughComment
}
