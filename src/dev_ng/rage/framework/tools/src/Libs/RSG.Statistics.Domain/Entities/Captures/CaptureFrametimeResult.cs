﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.Captures
{
    /// <summary>
    /// Statistics from a frametime capture telemetry submission.
    /// </summary>
    public class CaptureFrametimeResult : CaptureResultBase
    {
        #region Properties
        /// <summary>
        /// Minimum fps.
        /// </summary>
        public virtual float Minimum { get; set; }

        /// <summary>
        /// Average fps.
        /// </summary>
        public virtual float Average { get; set; }

        /// <summary>
        /// Maximum fps.
        /// </summary>
        public virtual float Maximum { get; set; }

        /// <summary>
        /// Standard deviation of the average fps.
        /// </summary>
        public virtual float StandardDeviation { get; set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CaptureFrametimeResult()
            : base()
        {
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <param name="zone"></param>
        public CaptureFrametimeResult(CaptureSession session, CaptureZone zone)
            : base(session, zone)
        {
        }
        #endregion // Constructor(s)
    } // CaptureFrametimeResult
}
