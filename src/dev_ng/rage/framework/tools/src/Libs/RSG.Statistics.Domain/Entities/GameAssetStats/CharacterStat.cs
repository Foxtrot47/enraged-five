﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public class CharacterStat : GameAssetStatBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public virtual Character Character
        {
            get;
            set;
        }
        
        /// <summary>
        /// 
        /// </summary>
        public virtual int PolyCount
        {
            get;
            set;
        }
        
        /// <summary>
        /// 
        /// </summary>
        public virtual int CollisionCount
        {
            get;
            set;
        }
        
        /// <summary>
        /// 
        /// </summary>
        public virtual int BoneCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual int ClothPolyCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual int ClothCount
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual IList<CharacterPlatformStat> CharacterPlatformStats
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public CharacterStat()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // CharacterStat
}
