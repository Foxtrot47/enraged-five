﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssets;
using System.Diagnostics;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    public class Build : GameAssetBase<BuildDto>
    {
        #region Properties
        /// <summary>
        /// Version of the build that the game uses.
        /// </summary>
        public virtual uint? GameVersion { get; set; }

        /// <summary>
        /// Flag indicating whether the build has asset stats associated with it.
        /// </summary>
        public virtual bool HasAssetStats { get; set; }

        /// <summary>
        /// Flag indicating whether the build has processed automated everything stats associated with it.
        /// </summary>
        public virtual bool HasAutomatedEverythingStats { get; set; }

        /// <summary>
        /// Flag indicating whether the build has processed automated map only stats associated with it.
        /// </summary>
        public virtual bool HasAutomatedMapOnlyStats { get; set; }

        /// <summary>
        /// Flag indicating whether the build has processed automated map only stats associated with it.
        /// </summary>
        public virtual bool HasAutomatedNighttimeMapOnlyStats { get; set; }

        /// <summary>
        /// Flag indicating whether the build has raw mag demo stats associated with it.
        /// </summary>
        public virtual bool HasMagDemoStats { get; set; }

        /// <summary>
        /// Flag indicating whether the build has raw memory shortfall stats associated with it.
        /// </summary>
        public virtual bool HasMemShortfallStats { get; set; }

        /// <summary>
        /// Flag indicating whether the build has shape test stats associated with it.
        /// </summary>
        public virtual bool HasShapeTestStats { get; set; }

        /// <summary>
        /// Flag indicating whether we should ignore this build for the website
        /// (the is for duff data in the db informing us that certain builds exist when they don't actually).
        /// </summary>
        public virtual bool Hidden { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public Build()
            : base()
        {
        }
        #endregion // Constructor(s)

        #region GameAssetBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public override void Update(BuildDto dto)
        {
            base.Update(dto);

            GameVersion = dto.GameVersion;
            HasAssetStats = dto.HasAssetStats;
            HasAutomatedEverythingStats = dto.HasAutomatedEverythingStats;
            HasAutomatedMapOnlyStats = dto.HasAutomatedMapOnlyStats;
            HasAutomatedNighttimeMapOnlyStats = dto.HasAutomatedNighttimeMapOnlyStats;
        }
        #endregion // GameAssetBase Overrides
    } // Build
}
