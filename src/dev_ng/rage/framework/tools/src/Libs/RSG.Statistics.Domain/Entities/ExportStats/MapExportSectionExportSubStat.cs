﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common;

namespace RSG.Statistics.Domain.Entities.ExportStats
{
    /// <summary>
    /// Contains information about a single chunk of work performed during a map export section export.
    /// e.g. Anim, geom, texture, collision exports.
    /// </summary>
    public class MapExportSectionExportSubStat : DomainEntityBase
    {
        #region Properties
        /// <summary>
        /// The map section export this stat is a part of.
        /// </summary>
        public virtual MapExportSectionExportStat SectionExportStat { get; set; }

        /// <summary>
        /// When this stage of the map export started.
        /// </summary>
        public virtual DateTime Start { get; set; }

        /// <summary>
        /// When this stage of the map export process completed.
        /// </summary>
        public virtual DateTime? End { get; set; }

        /// <summary>
        /// Whether  this stage of the map export was successful or not.
        /// </summary>
        public virtual bool? Success { get; set; }

        /// <summary>
        /// Which sub step this stat is for.
        /// </summary>
        public virtual EnumReference<SectionExportSubTask> ExportSubTask { get; set; }
        #endregion // Properties
    } // MapExportSectionExportSubStat
}
