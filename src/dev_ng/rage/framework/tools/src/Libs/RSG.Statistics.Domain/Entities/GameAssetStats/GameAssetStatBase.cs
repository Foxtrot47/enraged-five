﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Model.Common;
using RSG.Model.Common.Map;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// OBSOLETE:  Don't use this any more.
    /// </summary>
    public abstract class GameAssetStatBase : DomainEntityBase, IGameAssetStat
    {
        #region Properties
        /// <summary>
        /// Build this stat is for.
        /// </summary>
        public virtual Build Build { get; set; }

        /// <summary>
        /// List of shaders associated with this game asset stat.
        /// MET: This shouldn't be included here as not all game asset stats required it.
        /// </summary>
        [StreamableStatAttribute(StreamableMapArchetypeStat.ShadersString)]
        [StreamableStatAttribute(StreamableRoomStat.ShadersString)]
        public virtual IList<ShaderStat> ShaderStats { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        protected GameAssetStatBase()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // GameAssetStatBase


    /// <summary>
    /// Base class for all game asset stats.
    /// </summary>
    public abstract class GameAssetStatBase<TAsset> : DomainEntityBase, IGameAssetStat<TAsset> where TAsset : IGameAsset
    {
        #region Properties
        /// <summary>
        /// Build this stat is for.
        /// </summary>
        public virtual Build Build { get; set; }

        /// <summary>
        /// Asset that this stat is for.
        /// </summary>
        public virtual TAsset Asset { get; set; }
        #endregion // Properties
    } // GameAssetStatBase<TAsset>
}
