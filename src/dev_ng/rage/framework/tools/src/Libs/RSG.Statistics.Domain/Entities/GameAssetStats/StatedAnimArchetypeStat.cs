﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Model.Common.Map;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public class StatedAnimArchetypeStat : ArchetypeStat
    {
        /// <summary>
        /// List of archetypes that make up this stated anim.
        /// Lazily loaded, non-inverse end.
        /// </summary>
        [StreamableStatAttribute(StreamableStatedAnimArchetypeStat.ComponentArchetypesString)]
        public virtual ICollection<ArchetypeStat> ComponentArchetypeStats { get; set; }
    } // StatedAnimArchetypeStat
}
