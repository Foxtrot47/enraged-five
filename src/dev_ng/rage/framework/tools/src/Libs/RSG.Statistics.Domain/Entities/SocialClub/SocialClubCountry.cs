﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.SocialClub
{
    /// <summary>
    /// Social club countries (cached from social club services).
    /// </summary>
    public class SocialClubCountry : DomainEntityBase
    {
        /// <summary>
        /// Country code for this country (e.g. US, GB).
        /// </summary>
        public virtual String CountryCode { get; set; }

        /// <summary>
        /// Friendly name for the country.
        /// </summary>
        public virtual String Name { get; set; }
    } // SocialClubCountry
}
