﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Cfg;
using NHibernate.Spatial.Mapping;
using NHibernate.Tool.hbm2ddl;
using System.Diagnostics;
using RSG.Statistics.Domain.Entities;
using NHibernate.Event;
using RSG.Statistics.Domain.Listeners;
using RSG.Statistics.Common.Config;
using RSG.Base.Configuration.Services;

namespace RSG.Statistics.Domain.Config
{
    /// <summary>
    /// 
    /// </summary>
    public class DomainConfig : IDomainConfig
    {
        #region Properties
        /// <summary>
        /// NHibernate configuration
        /// </summary>
        public Configuration NHibernateConfig { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public SchemaExport SchemaExport
        {
            get
            {
                if (m_schemaExportInstance == null)
                {
                    m_schemaExportInstance = new SchemaExport(NHibernateConfig);
                }

                return m_schemaExportInstance;
            }
        }
        private SchemaExport m_schemaExportInstance;

        /// <summary>
        /// 
        /// </summary>
        public SchemaUpdate SchemaUpdate
        {
            get
            {
                if (m_schemaUpdateInstance == null)
                {
                    m_schemaUpdateInstance = new SchemaUpdate(NHibernateConfig);
                }
                return m_schemaUpdateInstance;
            }
        }
        private SchemaUpdate m_schemaUpdateInstance;
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public DomainConfig(INHibernateConfig config)
        {
            CreateNHibernateConfig(config);
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private void CreateNHibernateConfig(INHibernateConfig config)
        {
            // Construct the connection string
            string dbConnectionString =
                String.Format("Database={0};Data Source={1};User Id={2};Password={3};",
                              config.DatabaseName,
                              config.DatabaseLocation,
                              config.DatabaseUsername,
                              config.DatabasePassword);
            RSG.Base.Logging.Log.Log__Debug("DB Connection String: {0}", dbConnectionString);

            // Set up the config
            try
            {
                NHibernateConfig = new Configuration();
            }
            catch (System.Exception ex)
            {
                RSG.Base.Logging.Log.Log__Exception(ex, "Error while attempting to create the nhibernate config.");
            }

            NHibernateConfig.SetProperty("dialect", config.HibernateDialect);
            NHibernateConfig.SetProperty("connection.driver_class", config.DatabaseDriver);
            NHibernateConfig.SetProperty("connection.connection_string", dbConnectionString);
            NHibernateConfig.SetProperty("show_sql", config.DebugMode.ToString());
            NHibernateConfig.SetProperty("format_sql", config.DebugMode.ToString());
            NHibernateConfig.SetProperty("command_timeout", config.CommandTimeout.ToString());

            // Add the event listeners
            AuditEventListener auditEventListener = new AuditEventListener();
            NHibernateConfig.SetListener(ListenerType.PreInsert, auditEventListener);
            NHibernateConfig.SetListener(ListenerType.PreUpdate, auditEventListener);

            NHibernateConfig.AddAuxiliaryDatabaseObject(new SpatialAuxiliaryDatabaseObject(NHibernateConfig));
            NHibernateConfig.AddAssembly(typeof(IDomainEntity).Assembly);
        }
        #endregion // Private Methods
    } // NHibernateConfig
}
