﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.Profile
{
    /// <summary>
    /// Profile stat definitions that the social club services are aware of.
    /// </summary>
    public class ProfileStatDefinition : DomainEntityBase
    {
        /// <summary>
        /// Id that the social club services endpoint has for this stat.
        /// </summary>
        public virtual int RosId { get; set; }

        /// <summary>
        /// Name of the stat.
        /// </summary>
        public virtual String Name { get; set; }

        /// <summary>
        /// Type of stat this is (int, label, bool, etc...).
        /// </summary>
        public virtual String Type { get; set; }

        /// <summary>
        /// Default value for this stat.
        /// </summary>
        public virtual String DefaultValue { get; set; }

        /// <summary>
        /// Comment associated with this stat.
        /// </summary>
        public virtual String Comment { get; set; }

        /// <summary>
        /// Whether this stat is still active as far as social club services is concerned.
        /// </summary>
        public virtual bool Active { get; set; }
    } // ProfileStatDefinition
}
