﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Statistics.Platform;

using ModelPlatform = RSG.Model.Statistics.Platform;

namespace RSG.Statistics.Domain.Entities.ResourceStats
{
    /// <summary>
    /// Domain object for a bucket
    /// </summary>
    public class ResourceBucket : DomainEntityBase
    {
        /// <summary>
        /// Bucket size in bytes.
        /// </summary>
        public virtual UInt32 Size { get; set; }

        /// <summary>
        /// Bucket capacity in bytes.
        /// </summary>
        public virtual UInt32 Capacity { get; set; }

        /// <summary>
        /// Bucket ID 
        /// - sequential ids for buckets in each buckettype per resource
        /// </summary>
        public virtual UInt32 BucketId { get; set; }

        /// <summary>
        /// Bucket Type
        /// </summary>
        /// RSG.Platform.FileType
        public virtual EnumReference<ResourceBucketType> BucketType { get; set; }

        /// <summary>
        /// point back to container / parent
        /// </summary>
        public virtual ResourceStat ResourceStat { get; set; }

        #region Constructors
        /// <summary>
        /// Parameterless constructor
        /// - required
        /// </summary>
        public ResourceBucket()
            : base()
        {
        }
        #endregion // Constructors

        #region Conversion to Model
        /// <summary>
        /// Get Model 'helper'
        /// </summary>
        /// <returns></returns>
        public virtual RSG.Model.Statistics.Platform.ResourceBucket GetModel()
        {
            ResourceBucketType bucketType = (ResourceBucketType)BucketType.Value;
            ModelPlatform.ResourceBucket bucket = new ModelPlatform.ResourceBucket(bucketType, BucketId, Size, Capacity);
            return bucket;
        }
        #endregion // Conversion to Model

    }
}
