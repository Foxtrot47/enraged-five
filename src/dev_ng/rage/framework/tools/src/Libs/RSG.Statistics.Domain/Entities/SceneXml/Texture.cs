﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.SceneXml.Material;

namespace RSG.Statistics.Domain.Entities.SceneXml
{
    /// <summary>
    /// Domain entity class which contains information about a texture
    /// in a SceneXml file.
    /// </summary>
    public class Texture : DomainEntityBase
    {
        #region Properties
        /// <summary>
        /// Full path to the texture on disk.
        /// </summary>
        public virtual String Filename { get; set; }

        /// <summary>
        /// Full path to the alpha filename for this texture.
        /// </summary>
        public virtual String AlphaFilename { get; set; }

        /// <summary>
        /// Type of texture this is.
        /// </summary>
        public virtual TextureTypes Type { get; set; }

        /// <summary>
        /// Material this texture is used in.
        /// </summary>
        public virtual Material Material { get; set; }

        /// <summary>
        /// Reference to the file that this object is a part of.
        /// </summary>
        public virtual Scene Scene { get; set; }
        #endregion // Properties
    } // Texture
}
