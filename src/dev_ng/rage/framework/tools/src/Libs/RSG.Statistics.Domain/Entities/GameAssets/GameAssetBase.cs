﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Common.Dto;
using System.Diagnostics;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class GameAssetBase<T> : GameAssetBase
        where T : GameAssetDtoBase
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        protected GameAssetBase()
            : base()
        {
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public virtual void Update(T dto)
        {
            Name = dto.Name;
            Identifier = dto.Identifier;
        }
        #endregion // Public Methods
    } // GameAssetBase

    /// <summary>
    /// Non-dto version of GameAssetBase
    /// </summary>
    public abstract class GameAssetBase : DomainEntityBase, IGameAsset, IHasModifiedInformation
    {
        #region Properties
        /// <summary>
        /// When this asset was last modified.
        /// </summary>
        public virtual DateTime ModifiedOn { get; set; }

        /// <summary>
        /// Friendly name for this asset.
        /// </summary>
        public virtual String Name { get; set; }

        /// <summary>
        /// Identifier for this asset.
        /// </summary>
        public virtual String Identifier { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        protected GameAssetBase()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // GameAssetBase

    /// <summary>
    /// GameAssetBase that has a friendly name.
    /// </summary>
    public abstract class FriendlyGameAssetBase : GameAssetBase
    {
        #region Properties
        /// <summary>
        /// Friendly name for the asset.
        /// </summary>
        public virtual String FriendlyName { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        protected FriendlyGameAssetBase()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // FriendlyGameAssetBase
}
