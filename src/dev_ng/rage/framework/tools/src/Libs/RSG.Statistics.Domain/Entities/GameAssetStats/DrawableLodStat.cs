﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Map;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public class DrawableLodStat : DomainEntityBase
    {
        #region Properties
        /// <summary>
        /// Link to the map section stat that this is for.
        /// </summary>
        public virtual ArchetypeStat ArchetypeStat { get; set; }

        /// <summary>
        /// Name of the lod object.
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Lod distance this kicks in at.
        /// </summary>
        public virtual float LodDistance { get; set; }

        /// <summary>
        /// Poly count of the lod object.
        /// </summary>
        public virtual uint PolygonCount { get; set; }

        /// <summary>
        /// Lod level associated with this object.
        /// </summary>
        public virtual EnumReference<MapDrawableLODLevel> LodLevel { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        internal DrawableLodStat()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stat"></param>
        /// <param name="type"></param>
        /// <param name="value"></param>
        public DrawableLodStat(ArchetypeStat stat, string name, float lodDistance, uint polyCount, EnumReference<MapDrawableLODLevel> lodLevel)
        {
            ArchetypeStat = stat;
            Name = name;
            LodDistance = lodDistance;
            PolygonCount = polyCount;
            LodLevel = lodLevel;
        }
        #endregion // Constructor(s)
    } // MapDrawableLodStat
}
