﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public class ShaderStat : DomainEntityBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
	    public virtual Shader Shader
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual GameAssetStatBase GameAssetStatBase
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual IList<TextureStat> TextureStats
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ShaderStat()
            : base()
        {
        }
        #endregion // Constructor(s)
    } // ShaderStat
}
