﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Model.Common;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public class LevelStat : DomainEntityBase
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public virtual Build Build { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual Level Level { get; set; }

        /// <summary>
        /// Level export data path.
        /// </summary>
        [StreamableStatAttribute(StreamableLevelStat.ExportDataPathString)]
        public virtual String ExportDataPath { get; set; }

        /// <summary>
        /// Level processed data path.
        /// </summary>
        [StreamableStatAttribute(StreamableLevelStat.ProcessedDataPathString)]
        public virtual String ProcessedDataPath { get; set; }

        /// <summary>
        /// Lower left point for the level
        /// </summary>
        [StreamableStatAttribute(StreamableLevelStat.ImageBoundsString)]
        public virtual GeoAPI.Geometries.IPoint LowerLeft { get; set; }

        /// <summary>
        /// Upper right point for the level
        /// </summary>
        [StreamableStatAttribute(StreamableLevelStat.ImageBoundsString)]
        public virtual GeoAPI.Geometries.IPoint UpperRight { get; set; }

        /// <summary>
        /// Background image for this level stat.
        /// </summary>
        [StreamableStatAttribute(StreamableLevelStat.BackgroundImageString)]
        public virtual LevelImage BackgroundImage { get; set; }

        /// <summary>
        /// List of spawn points that were gathered from the metadata files.
        /// </summary>
        [StreamableStatAttribute(StreamableLevelStat.SpawnPointsString)]
        public virtual IList<SpawnPointStat> SpawnPointStats { get; set; }
        #endregion // Properties
    } // LevelStat
}
