﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.Reports
{
    /// <summary>
    /// A single parameter for a particular report preset.
    /// </summary>
    public class ReportPresetParameter : DomainEntityBase
    {
        /// <summary>
        /// Preset this parameter is associated with.
        /// </summary>
        public virtual ReportPreset Preset { get; set; }

        /// <summary>
        /// Key for this parameter.
        /// </summary>
        public virtual String Key { get; set; }

        /// <summary>
        /// Value for this parameter.
        /// </summary>
        public virtual String Value { get; set; }
    } // ReportPresetParameter
}
