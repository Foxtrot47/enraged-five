﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.Vertica
{
    /// <summary>
    /// Basic user data cached from vertica for creating user groups.
    /// </summary>
    public class VerticaUser : DomainEntityBase
    {
        /// <summary>
        /// Name of the user.
        /// </summary>
        public virtual String Name { get; set; }

        /// <summary>
        /// List of user groups that this user is in.
        /// Lazily loaded.
        /// Required for the many-to-many connection.
        /// </summary>
        public virtual Iesi.Collections.Generic.ISet<VerticaGamerGroup> UserGroups { get; set; }
    } // VerticaUser
}
