﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssetStats.Animation
{
    /// <summary>
    /// 
    /// </summary>
    public class AnimationStat : GameAssetStatBase<GameAssets.Animation>
    {
        #region Properties
        /// <summary>
        /// Clip stat this anim stat is associated with.
        /// </summary>
        public virtual ClipStat ClipStat { get; set; }
        #endregion // Properties
    } // AnimationStat
}
