﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    public class Rank : DomainEntityBase
    {
        /// <summary>
        /// Level for this rank.
        /// </summary>
        public virtual uint Level { get; set; }

        /// <summary>
        /// Rank at which this item is unlocked.
        /// </summary>
        public virtual uint Experience { get; set; }

        /// <summary>
        /// Name of the unlock.
        /// </summary>
        public virtual String Name { get; set; }
    } // Rank
}
