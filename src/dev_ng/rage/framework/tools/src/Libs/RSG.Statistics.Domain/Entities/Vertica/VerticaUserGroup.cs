﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Iesi.Collections.Generic;

namespace RSG.Statistics.Domain.Entities.Vertica
{
    /// <summary>
    /// Basic user data cached from vertica for creating user groups.
    /// </summary>
    public class VerticaUserGroup : DomainEntityBase
    {
        #region Properties
        /// <summary>
        /// Name of the group.
        /// </summary>
        public virtual String Name { get; set; }

        /// <summary>
        /// List of users that are in this group (lazily loaded).
        /// </summary>
        public virtual Iesi.Collections.Generic.ISet<VerticaUser> Users { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public VerticaUserGroup()
            : base()
        {
            Users = new HashedSet<VerticaUser>();
        }

        public VerticaUserGroup(String name)
            : this()
        {
            Name = name;
        }
        #endregion // Constructor(s)
    } // VerticaUserGroup
}
