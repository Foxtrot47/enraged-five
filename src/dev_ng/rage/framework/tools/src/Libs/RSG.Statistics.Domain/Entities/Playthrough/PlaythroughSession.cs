﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.Playthrough
{
    /// <summary>
    /// 
    /// </summary>
    public class PlaythroughSession : DomainEntityBase, IHasModifiedInformation
    {
        #region Properties
        /// <summary>
        /// When this user was last modified.
        /// </summary>
        public virtual DateTime ModifiedOn { get; set; }

        /// <summary>
        /// Build this session was taking part on.
        /// </summary>
        public virtual Build Build { get; set; }

        /// <summary>
        /// Platform this session was taking part on.
        /// </summary>
        public virtual EnumReference<RSG.Platform.Platform> Platform { get; set; }

        /// <summary>
        /// User this session is for.
        /// </summary>
        public virtual PlaythroughUser User { get; set; }

        /// <summary>
        /// Optional friendly name for the playthrough session.
        /// </summary>
        public virtual String FriendlyName { get; set; }

        /// <summary>
        /// When the session started.
        /// </summary>
        public virtual DateTime Start { get; set; }

        /// <summary>
        /// Total time the user spent playing (in seconds).
        /// </summary>
        public virtual uint TimeSpentPlaying { get; set; }

        /// <summary>
        /// List of mission attempts associated with this playthrough session.
        /// </summary>
        public virtual IList<PlaythroughMissionAttempt> MissionAttempts { get; set; }
        #endregion // Properties
    } // PlaythroughSession
}
