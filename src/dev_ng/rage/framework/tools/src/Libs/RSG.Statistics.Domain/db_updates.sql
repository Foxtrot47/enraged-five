-------------------------------------------------------------------------
-- 2014/09/22 - MET - Export filename for SceneXmlTexture.
-------------------------------------------------------------------------
alter table SceneXmlTexture add column AlphaFilename VARCHAR(255);

-------------------------------------------------------------------------
-- 2014/09/10 - MET - Scene xml parameters. (Originally by Tom F.)
-------------------------------------------------------------------------
create table SceneXmlParameter (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME, Name VARCHAR(255), StringValue VARCHAR(255), IntValue INTEGER, FloatValue FLOAT, BoolValue TINYINT(1), ObjectId BIGINT, primary key (Id));
alter table SceneXmlParameter add index (ObjectId),add constraint FK26B4E5E63C40789 foreign key (ObjectId) references SceneXmlObject (Id);

-------------------------------------------------------------------------
-- 2014/05/16 - MET - Scene xml file.
-------------------------------------------------------------------------
create table SceneXmlAttribute (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME, Name VARCHAR(255), StringValue VARCHAR(255), IntValue INTEGER, FloatValue FLOAT, BoolValue TINYINT(1), ObjectId BIGINT, primary key (Id));
create table SceneXmlScene (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME, Filename VARCHAR(255), Version FLOAT, Timestamp DATETIME, Username VARCHAR(255), primary key (Id));
create table SceneXmlMaterial (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME, Guid CHAR(36), Type VARCHAR(255), Preset VARCHAR(255), ParentSceneId BIGINT, ParentMaterialId BIGINT, MaterialIndex INTEGER UNSIGNED, SceneId BIGINT, primary key (Id));
create table SceneXmlObject (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME, Guid CHAR(36), Name VARCHAR(255), Class VARCHAR(255), SuperClass VARCHAR(255), AttributeClass VARCHAR(255), ParentObjectId BIGINT, ParentSceneId BIGINT, MaterialId BIGINT, PolyCount INTEGER UNSIGNED, SceneId BIGINT, ObjectTranslationX FLOAT, ObjectTranslationY FLOAT, ObjectTranslationZ FLOAT, ObjectRotationX FLOAT, ObjectRotationY FLOAT, ObjectRotationZ FLOAT, ObjectRotationW FLOAT, ObjectScaleX FLOAT, ObjectScaleY FLOAT, ObjectScaleZ FLOAT, NodeTranslationX FLOAT, NodeTranslationY FLOAT, NodeTranslationZ FLOAT, NodeRotationX FLOAT, NodeRotationY FLOAT, NodeRotationZ FLOAT, NodeRotationW FLOAT, NodeScaleX FLOAT, NodeScaleY FLOAT, NodeScaleZ FLOAT, LocalBoundingBoxMinX FLOAT, LocalBoundingBoxMinY FLOAT, LocalBoundingBoxMinZ FLOAT, LocalBoundingBoxMaxX FLOAT,LocalBoundingBoxMaxY FLOAT, LocalBoundingBoxMaxZ FLOAT, WorldBoundingBoxMinX FLOAT, WorldBoundingBoxMinY FLOAT, WorldBoundingBoxMinZ FLOAT, WorldBoundingBoxMaxX FLOAT, WorldBoundingBoxMaxY FLOAT, WorldBoundingBoxMaxZ FLOAT, primary key (Id));
create table SceneXmlTexture (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME, Filename VARCHAR(255), Type VARCHAR(255), MaterialId BIGINT, SceneId BIGINT, primary key (Id));
alter table SceneXmlAttribute add index (ObjectId),add constraint FK11A4BBF363C40789 foreign key (ObjectId) references SceneXmlObject (Id);
alter table SceneXmlMaterial add index (ParentSceneId), add constraint FK22D1E86277524EA7 foreign key (ParentSceneId) references SceneXmlScene (Id);
alter table SceneXmlMaterial add index (ParentMaterialId), add constraint FK22D1E8629773BB11 foreign key (ParentMaterialId) references SceneXmlMaterial (Id);
alter table SceneXmlMaterial add index (SceneId), add constraint FK22D1E862A3A9163 foreign key (SceneId) references SceneXmlScene (Id);
alter table SceneXmlObject add index (ParentObjectId), add constraint FKC93B9CC8BBE661DD foreign key (ParentObjectId) references SceneXmlObject (Id);
alter table SceneXmlObject add index (ParentSceneId), add constraint FKC93B9CC877524EA7 foreign key (ParentSceneId) references SceneXmlScene (Id);
alter table SceneXmlObject add index (MaterialId), add constraint FKC93B9CC88DC1CAD foreign key (MaterialId) references SceneXmlMaterial (Id);
alter table SceneXmlObject add index (SceneId), add constraint FKC93B9CC8A3A9163 foreign key (SceneId) references SceneXmlScene (Id);
alter table SceneXmlTexture add index (MaterialId), add constraint FK1A71BB028DC1CAD foreign key (MaterialId) references SceneXmlMaterial (Id);
alter table SceneXmlTexture add index (SceneId), add constraint FK1A71BB02A3A9163 foreign key (SceneId) references SceneXmlScene (Id);

-------------------------------------------------------------------------
-- 2014/01/14 - MET - Friendly name overrides for other items.
-------------------------------------------------------------------------
alter table Weapon add column FriendlyNameOverridden TINYINT(1) NOT NULL;
alter table Vehicle add column FriendlyNameOverridden TINYINT(1) NOT NULL;
update Weapon set FriendlyNameOverridden=0;
update Vehicle set FriendlyNameOverridden=0;

-------------------------------------------------------------------------
-- 2013/09/09 - MET - Friendly name overrides for shop items (for dupes).
-------------------------------------------------------------------------
alter table ShopItem add column FriendlyNameOverride VARCHAR(255);

-------------------------------------------------------------------------
-- 2013/08/20 - MET - Need to rename this flag due to a Nhibernate issue with reserved MySQL names.
-------------------------------------------------------------------------
alter table Build change `Ignore` Hidden TINYINT(1) NOT NULL;

-------------------------------------------------------------------------
-- 2013/08/20 - MET - Flag for ignoring builds.
-------------------------------------------------------------------------
alter table Build add column `Ignore` TINYINT(1) NOT NULL;
update Build set `Ignore`=0;

-------------------------------------------------------------------------
-- 2013/08/16 - MET - Making drawlists more consistent with the other game assets.
-------------------------------------------------------------------------
alter table DrawList add column Identifier VARCHAR(255) not null;
update DrawList set Identifier = CAST(Hash AS CHAR(255));
alter table DrawList drop column Hash;
alter table DrawList change Identifier Identifier VARCHAR(255) not null unique;

-------------------------------------------------------------------------
-- 2013/08/12 - MET - Odd Jobs are encoded in the mission table.  No need for something separate.
-------------------------------------------------------------------------
drop table OddJob;

-------------------------------------------------------------------------
-- 2013/08/08 - MET - Sorting out weapon friendly names
-------------------------------------------------------------------------
UPDATE Weapon SET FriendlyName=NULL WHERE FriendlyName='Invalid';
UPDATE Weapon SET FriendlyName=NULL WHERE FriendlyName=Name;

-------------------------------------------------------------------------
-- 2013/07/22 - MET - XP information
-------------------------------------------------------------------------
create table XPCategory (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255), Identifier VARCHAR(255) not null unique, FriendlyName VARCHAR(255), primary key (Id));
create table XPType (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255), Identifier VARCHAR(255) not null unique, FriendlyName VARCHAR(255), primary key (Id));

-------------------------------------------------------------------------
-- 2013/07/22 - MET - Shop/shop item tweaks
-------------------------------------------------------------------------
create table ShopType (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Name VARCHAR(255) not null unique, Value INTEGER, primary key (Id));
rename table ShopName to Shop;
alter table Shop add column FriendlyName VARCHAR(255), add column ShopTypeId BIGINT not null;
update Shop SET ShopTypeId=1;
INSERT INTO ShopType (CreatedOn, Name, Value) VALUES (NOW(), 'HairDresser', 0);
alter table Shop add index (ShopTypeId), add constraint FK6DEF0A4AA4A9FC70 foreign key (ShopTypeId) references ShopType (Id);

alter table ShopItem add column FriendlyName VARCHAR(255), add column Category VARCHAR(255), add column SubCategory VARCHAR(255);

-------------------------------------------------------------------------
-- 2013/06/29 - MET - Deleting tables that are no longer needed
-------------------------------------------------------------------------
alter table CaptureCpuResult drop foreign key FKD64510B51E161B1, drop column TelemetryStatId;
alter table CaptureDrawListResult drop foreign key FKB9621E8351E161B1, drop column TelemetryStatId;
alter table CaptureFpsResult drop foreign key FKA20E73FA51E161B1, drop column TelemetryStatId;
alter table CaptureFrametimeResult drop foreign key FK332B04F551E161B1, drop column TelemetryStatId;
alter table CaptureGpuResult drop foreign key FK9B071E9F51E161B1, drop column TelemetryStatId;
alter table CaptureMemoryResult drop foreign key FK8686408251E161B1, drop column TelemetryStatId;
alter table CapturePedAndVehicleResult drop foreign key FK8D7FB6BF51E161B1, drop column TelemetryStatId;
alter table CaptureScriptMemoryResult drop foreign key FK2EAD34CB51E161B1, drop column TelemetryStatId;
alter table CaptureStreamingMemoryResult drop foreign key FK688122F251E161B1, drop column TelemetryStatId;
alter table CaptureThreadResult drop foreign key FK6766A87351E161B1, drop column TelemetryStatId;

alter table CaptureSession drop foreign key FK396ADBD887772080, drop column SpSessionId;
alter table CaptureSession drop foreign key FK396ADBD8422854E5, drop column MpGamerSessionId;

alter table PlaythroughMissionAttempt drop foreign key FK6DAC85FE2531ACF2, drop column GamerId;

set FOREIGN_KEY_CHECKS=0;
drop table CutsceneViewStat;
drop table MissionAttemptStat;
drop table MissionCheckpointAttempt;
drop table RadioStationSessionStat;
drop table WantedLevelStat;
drop table VehicleDistanceDrivenStat;
drop table TelemetryShopPurchase;
drop table CrateDropMissionInstance;
drop table HoldUpMissionInstance;
drop table ImportExportMissionInstance;
drop table ProstituteMissionInstance;
drop table RaceToPointMissionInstance;
drop table SecurityVanMissionInstance;
drop table MultiplayerGameMode;
drop table MultiplayerGamerSession;
drop table MultiplayerMatch;
drop table MultiplayerMatchParticipant;
drop table MultiplayerSession;
drop table SingleplayerSession;
drop table ProcessedDrawListStat;
drop table ProcessedFpsStat;
drop table ProcessedMemShortfallStat;
drop table ProcessedShapeTestStat;
drop table ProcessedStatResolution;
drop table TelemetryClipStat;
drop table TelemetryDrawListStat;
drop table TelemetryMemoryPool;
drop table TelemetryMemoryPools;
drop table TelemetryMemorySkeleton;
drop table TelemetryMemoryStore;
drop table TelemetryMemoryStores;
drop table TelemetryStatFile;
drop table TelemetryStatPacket;
drop table TelemetryStat;
drop table TelemetryCoordStat;
drop table TelemetryCoordFpsStat;
drop table TelemetryCoordWantedLevelStat;
drop table TelemetryCoordEmergencySvcsStat;
drop table TelemetryCoordDeathStat;
drop table TelemetryCoordVehicleStat;
drop table TelemetryCoordWeaponStat;
drop table TelemetryCoordMemoryStat;
drop table TelemetryCoordMemShortfallStat;
drop table TelemetryCoordMpRemoteGamerStat;
drop table TelemetryCoordPlayerInjuryStat;
drop table TelemetryCutsceneStat;
drop table TelemetryWeaponStat;
drop table TelemetryRadioStationStat;
drop table TelemetryMissionCheckpointStat;
drop table SocialClubActor;
drop table SocialClubGroup_SocialClubActor;
drop table SocialClubMission;
drop table ActorGroup_Actor;
drop table Gamer;
drop table TelemetryMetric;
set FOREIGN_KEY_CHECKS=1;

-------------------------------------------------------------------------
-- 2013/06/29 - MET - Cash packs.
-------------------------------------------------------------------------
create table CashPack (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255), Identifier VARCHAR(255) not null unique, InGameAmount INTEGER UNSIGNED, Active TINYINT(1), primary key (Id));

-------------------------------------------------------------------------
-- 2013/06/28 - MET - Movie game asset.
-------------------------------------------------------------------------
create table Movie (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255), Identifier VARCHAR(255) not null unique, FriendlyName VARCHAR(255), primary key (Id));

-------------------------------------------------------------------------
-- 2013/06/28 - MET - Mini-game information.
-------------------------------------------------------------------------
create table MatchType (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Name VARCHAR(255) not null unique, Value INTEGER, primary key (Id));
create table MiniGameVariant (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255), Identifier VARCHAR(255) not null unique, FriendlyName VARCHAR(255), MatchTypeId BIGINT not null, X FLOAT not null, Y FLOAT not null, Z FLOAT not null, primary key (Id));
alter table MiniGameVariant add index (MatchTypeId), add constraint FKCEAEA6C01B2F0A7A foreign key (MatchTypeId) references MatchType (Id);

-------------------------------------------------------------------------
-- 2013/06/28 - MET - Cutscene has branch flag.
-------------------------------------------------------------------------
alter table Cutscene add column HasBranch TINYINT(1);
alter table CutsceneStat add column HasBranch TINYINT(1);

-------------------------------------------------------------------------
-- 2013/06/19 - MET - Additional social club mission information.
-------------------------------------------------------------------------
alter table SocialClubMission add column StartX float;
alter table SocialClubMission add column StartY float;
alter table SocialClubMission add column StartZ float;
alter table SocialClubMission add column AverageRating float;

-------------------------------------------------------------------------
-- 2013/06/18 - MET - Cutscene modifications.
-------------------------------------------------------------------------
alter table Cutscene add column FriendlyName VARCHAR(255);
alter table Cutscene add column MissionId BIGINT;
alter table Cutscene add index (MissionId), add constraint FKD0470C6EC144F77D foreign key (MissionId) references Mission (Id);

-------------------------------------------------------------------------
-- 2013/06/18 - MET - Property game assets.
-------------------------------------------------------------------------
create table Property (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255), Identifier VARCHAR(255) not null unique, primary key (Id));

-------------------------------------------------------------------------
-- 2013/06/17 - MET - Additional social club information for missions.
-------------------------------------------------------------------------
alter table MultiplayerMatch drop foreign key FK7BB14F2D36FADC40;
truncate SocialClubMission;
alter table SocialClubMission add column CreatorPlatformId BIGINT;
alter table SocialClubMission add column MatchType INTEGER;
alter table SocialClubMission add column SubType INTEGER;
alter table SocialClubMission add column VehicleType INTEGER;
alter table SocialClubMission add column RaceType INTEGER;
alter table SocialClubMission add column TeamDeathmatch TINYINT(1);
alter table SocialClubMission add column VehicleDeathmatch TINYINT(1);
alter table SocialClubMission add index (CreatorPlatformId), add constraint FKEFE5EADFB61CB666 foreign key (CreatorPlatformId) references ROSPlatform (Id);
alter table SocialClubMission drop foreign key FKEFE5EADF7BB663A, drop column MatchTypeId;

-------------------------------------------------------------------------
-- 2013/06/14 - MET - Platforms for playthrough users.
-------------------------------------------------------------------------
alter table PlaythroughUser add column PlatformId BIGINT;
alter table PlaythroughUser add index (PlatformId), add constraint FK1383CEAED26829F2 foreign key (PlatformId) references ROSPlatform (Id);

-------------------------------------------------------------------------
-- 2013/06/12 - MET - Adding friendly names for all assets.
-------------------------------------------------------------------------
alter table TVShow add column FriendlyName VARCHAR(255);

-------------------------------------------------------------------------
-- 2013/06/10 - MET - Ratings for missions.
-------------------------------------------------------------------------
alter table PlaythroughMissionAttempt add column Rating INTEGER UNSIGNED;

-------------------------------------------------------------------------
-- 2013/06/07 - MET - Readding profile stat definition table for getting default values.
-------------------------------------------------------------------------
create table ProfileStatDefinition (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, RosId INTEGER not null, Name VARCHAR(255) not null, Type VARCHAR(255) not null, DefaultValue VARCHAR(255) not null, Comment VARCHAR(255) not null, Active TINYINT(1) not null, primary key (Id));

-------------------------------------------------------------------------
-- 2013/06/04 - MET - Valid vs invalid builds.
-------------------------------------------------------------------------
alter table ClipDictionary add column CategoryId BIGINT not null;
update ClipDictionary set CategoryId=3;
alter table ClipDictionary add index (CategoryId), add constraint FKC16F35FCC76774EA foreign key (CategoryId) references ClipDictionaryCategory (Id);

-------------------------------------------------------------------------
-- 2013/06/04 - MET - Valid vs invalid builds.
-------------------------------------------------------------------------
alter table Build drop column BuildDate, drop column BuildInfo, drop column BugstarId, add column GameVersion INTEGER UNSIGNED;

-------------------------------------------------------------------------
-- 2013/05/30 - MET - Add the leaderboard version for playthroughusers.
-------------------------------------------------------------------------
alter table PlaythroughUser add column LeaderboardVersion INTEGER UNSIGNED;

-------------------------------------------------------------------------
-- 2013/05/24 - MET - Per build cutscene stats.
-------------------------------------------------------------------------
create table CutscenePartStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, CutsceneStatId BIGINT not null, Name VARCHAR(255) not null, CutFile VARCHAR(255) not null, Duration float not null, primary key (Id));
create table CutscenePlatformSectionStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, PlatformId BIGINT not null, PhysicalSize BIGINT UNSIGNED not null, VirtualSize BIGINT UNSIGNED not null, CutsceneStatId BIGINT not null, Idx INTEGER UNSIGNED not null, DataPath VARCHAR(255) not null, primary key (Id));
create table CutsceneStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, BuildId BIGINT not null, CutsceneId BIGINT not null, ExportZipFilepath VARCHAR(255), IsConcat TINYINT(1) not null, primary key (Id));
alter table CutscenePartStat add index (CutsceneStatId), add constraint FK8C676EDDE8FFEADB foreign key (CutsceneStatId) references CutsceneStat (Id);
alter table CutscenePlatformSectionStat add index (PlatformId), add constraint FK206539D6B1A9B7F8 foreign key (PlatformId) references Platform (Id);
alter table CutscenePlatformSectionStat add index (CutsceneStatId), add constraint FK206539D6E8FFEADB foreign key (CutsceneStatId) references CutsceneStat (Id);
alter table CutsceneStat add index (BuildId), add constraint FK13E8BF5CD949985D foreign key (BuildId) references Build (Id);
alter table CutsceneStat add index (CutsceneId), add constraint FK13E8BF5C9837D559 foreign key (CutsceneId) references Cutscene (Id);

-------------------------------------------------------------------------
-- 2013/05/20 - MET - Ranks/unlocks.
-------------------------------------------------------------------------
create table UnlockType (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Name VARCHAR(255) not null unique, Value INTEGER, primary key (Id));
create table UnlockTable (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Name VARCHAR(255) not null, Rank INTEGER UNSIGNED, UnlockTypeId BIGINT not null, primary key (Id));
alter table UnlockTable add index (UnlockTypeId), add constraint FK8C59B2D6205618B4 foreign key (UnlockTypeId) references UnlockType (Id);
create table Rank (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Level INTEGER UNSIGNED, Experience INTEGER UNSIGNED not null, Name VARCHAR(255), primary key (Id));

-------------------------------------------------------------------------
-- 2013/05/18 - MET - Vertica user/user groups.
-------------------------------------------------------------------------
create table VerticaUser (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Name VARCHAR(255) not null, primary key (Id), unique (Name));
create table VerticaUserGroup_VerticaUser (UserId BIGINT not null, UserGroupId BIGINT not null, primary key (UserGroupId, UserId));
create table VerticaUserGroup (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Name VARCHAR(255) not null, primary key (Id), unique (Name));
alter table VerticaUserGroup_VerticaUser add index (UserGroupId), add constraint FK6F1E996CE1905270 foreign key (UserGroupId) references VerticaUserGroup (Id);
alter table VerticaUserGroup_VerticaUser add index (UserId), add constraint FK6F1E996CA129A68 foreign key (UserId) references VerticaUser (Id);

-------------------------------------------------------------------------
-- 2013/05/17 - MET - TV Show game asset.
-------------------------------------------------------------------------
create table TVShow (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255), Identifier VARCHAR(255) not null unique, primary key (Id));

-------------------------------------------------------------------------
-- 2013/05/17 - MET - Vertica gamer/gamer groups
-------------------------------------------------------------------------
create table ROSPlatform (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Name VARCHAR(255) not null unique, Value INTEGER, primary key (Id));
create table VerticaGamer (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Name VARCHAR(255) not null, PlatformId BIGINT not null, primary key (Id), unique (Name, PlatformId));
create table VerticaGamerGroup_VerticaGamer (GamerId BIGINT not null, GamerGroupId BIGINT not null, primary key (GamerGroupId, GamerId));
create table VerticaGamerGroup (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Name VARCHAR(255) not null, primary key (Id), unique (Name));
alter table VerticaGamer add index (PlatformId), add constraint FK64F65E18D26829F2 foreign key (PlatformId) references ROSPlatform (Id);
alter table VerticaGamerGroup_VerticaGamer add index (GamerGroupId), add constraint FKBAFF8E8E16C96DC2 foreign key (GamerGroupId) references VerticaGamerGroup (Id);
alter table VerticaGamerGroup_VerticaGamer add index (GamerId), add constraint FKBAFF8E8EEDEBEC2C foreign key (GamerId) references VerticaGamer (Id);

-------------------------------------------------------------------------
-- 2013/05/16 - MET - Add web site game asset table.
-------------------------------------------------------------------------
create table WebSite (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255), Identifier VARCHAR(255) not null unique, primary key (Id));

-------------------------------------------------------------------------
-- 2013/05/15 - MET - Add oddjob/shop name game asset table.
-------------------------------------------------------------------------
create table OddJob (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255), Identifier VARCHAR(255) not null unique, primary key (Id));
create table ShopName (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255), Identifier VARCHAR(255) not null unique, primary key (Id));

-------------------------------------------------------------------------
-- 2013/05/14 - DHM - ResourceStats Source and Destination Filenames
-------------------------------------------------------------------------
alter table ResourceStat add column SourceFilename VARCHAR(255);
alter table ResourceStat add column DestinationFilename VARCHAR(255);

-------------------------------------------------------------------------
-- 2013/05/13 - MET - Tracking gamer handles for the playthrough users.
-------------------------------------------------------------------------
alter table PlaythroughUser add column GamerHandle VARCHAR(255);

-------------------------------------------------------------------------
-- 2013/05/08 - MET - Resource stat index to improve query time.
-------------------------------------------------------------------------
create index TimestampIdx on ResourceStat (Timestamp);

-------------------------------------------------------------------------
-- 2013/05/02 - MET - Clip dictionary/clip/anim stats per build.
-------------------------------------------------------------------------
create table ClipDictionaryCategory (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Name VARCHAR(255) not null unique, Value INTEGER, primary key (Id));
create table AnimationStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, BuildId BIGINT not null, AnimationId BIGINT not null, ClipStatId BIGINT not null, primary key (Id));
create table ClipDictionaryStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, BuildId BIGINT not null, ClipDictionaryId BIGINT not null, CategoryId BIGINT not null, primary key (Id));
create table ClipStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, BuildId BIGINT not null, ClipId BIGINT not null, ClipDictionaryStatId BIGINT not null, primary key (Id));
alter table AnimationStat add index (BuildId), add constraint FK9B5A1796D949985D foreign key (BuildId) references Build (Id);
alter table AnimationStat add index (AnimationId), add constraint FK9B5A1796D664909D foreign key (AnimationId) references Animation (Id);
alter table AnimationStat add index (ClipStatId), add constraint FK9B5A1796117DFF4B foreign key (ClipStatId) references ClipStat (Id);
alter table ClipDictionaryStat add index (BuildId), add constraint FKC938D92ED949985D foreign key (BuildId) references Build (Id);
alter table ClipDictionaryStat add index (ClipDictionaryId), add constraint FKC938D92EC006C871 foreign key (ClipDictionaryId) references ClipDictionary (Id);
alter table ClipDictionaryStat add index (CategoryId), add constraint FKC938D92EC76774EA foreign key (CategoryId) references ClipDictionaryCategory (Id);
alter table ClipStat add index (BuildId), add constraint FK81EB8FFAD949985D foreign key (BuildId) references Build (Id);
alter table ClipStat add index (ClipId), add constraint FK81EB8FFAAD6C9431 foreign key (ClipId) references Clip (Id);
alter table ClipStat add index (ClipDictionaryStatId), add constraint FK81EB8FFAFA76F257 foreign key (ClipDictionaryStatId) references ClipDictionaryStat (Id);

-------------------------------------------------------------------------
-- 2013/04/29 - MET - Clearing data from tables that we no longer retrieve data from.
-------------------------------------------------------------------------
set FOREIGN_KEY_CHECKS=0;
truncate table processedshapeteststat;
truncate table processedmemshortfallstat;
truncate table processeddrawliststat;
truncate table processedfpsstat;
truncate table telemetrymemorypool;
truncate table telemetrymemorypools;
truncate table telemetrydrawliststat;
truncate table telemetrycoordfpsstat;
truncate table telemetrymemorystore;
truncate table telemetrymemorystores;
truncate table telemetrycoordmemorystat;
set FOREIGN_KEY_CHECKS=1;

-------------------------------------------------------------------------
-- 2013/04/29 - MET - Mission categories.
-------------------------------------------------------------------------
create table MissionCategory (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Name VARCHAR(255) not null unique, Value INTEGER, primary key (Id));
insert into MissionCategory values ('1', '2013-04-29 00:00:00', 'Other', '0');
alter table Mission add column CategoryId BIGINT not null, add column Idx INTEGER UNSIGNED;
update Mission set CategoryId=1;
alter table Mission add index (CategoryId), add constraint FK43DFC6EC9BACCDB4 foreign key (CategoryId) references MissionCategory (Id);

-- Only run the following after you've run the server at least once.
update Mission SET CategoryId=8 WHERE Name Like 'AMB %';
update Mission SET CategoryId=6 WHERE Name Like 'MG %';
update Mission SET CategoryId=5 WHERE Name Like 'Odd Job %';
update Mission SET CategoryId=7 WHERE Name Like 'Property %';
update Mission SET CategoryId=3 WHERE Name Like 'RC %';
update Mission SET CategoryId=4 WHERE Name Like 'RE %';

-------------------------------------------------------------------------
-- 2013/04/25 - MET - Memory heap table for the MEMORY_USAGE report.
-------------------------------------------------------------------------
create table MemoryHeap (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255), Identifier VARCHAR(255) not null unique, primary key (Id));

-------------------------------------------------------------------------
-- 2013/04/25 - MET - Killing off the profile stat tables.
-------------------------------------------------------------------------
drop table ProfileStatString;
drop table ProfileStatBoolean;
drop table ProfileStatDouble;
drop table ProfileStatInteger;
drop table ProfileStatBase;
drop table ProfileStatDefinition;

-------------------------------------------------------------------------
-- 2013/04/24 - MET - More playthrough tweaks.
-------------------------------------------------------------------------
update PlaythroughMissionAttempt set End=NULL WHERE End == '0001-01-01 00:00:00';

-------------------------------------------------------------------------
-- 2013/04/22 - MET - Generic playthrough comments.
-------------------------------------------------------------------------
create table PlaythroughComment (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, PlaythroughSessionId BIGINT not null, Comment text, MissionId BIGINT, WeaponId BIGINT, VehicleId BIGINT, primary key (Id));
alter table PlaythroughComment add index (PlaythroughSessionId), add constraint FK5BD2BBAE142644E7 foreign key (PlaythroughSessionId) references PlaythroughSession (Id);
alter table PlaythroughComment add index (MissionId), add constraint FK5BD2BBAEC144F77D foreign key (MissionId) references Mission (Id);
alter table PlaythroughComment add index (WeaponId), add constraint FK5BD2BBAE72CFEB39 foreign key (WeaponId) references Weapon (Id);
alter table PlaythroughComment add index (VehicleId), add constraint FK5BD2BBAE5761A995 foreign key (VehicleId) references Vehicle (Id);

alter table PlaythroughSession add column TimeSpentPlaying INTEGER UNSIGNED not null;
update PlaythroughSession set TimeSpentPlaying=TIMESTAMPDIFF(SECOND, Start, End) where End != '0001-01-01 00:00:00' AND End > Start;
alter table PlaythroughSession drop column End;
alter table PlaythroughSession modify Start DATETIME not null;

-------------------------------------------------------------------------
-- 2013/04/18 - MET - Separate playthrough attempt tables
-------------------------------------------------------------------------
create table PlaythroughMissionAttempt (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, UniqueIdentifier BIGINT UNSIGNED not null, MissionId BIGINT not null, GamerId BIGINT not null, PlaythroughSessionId BIGINT not null, Start DATETIME, End DATETIME, TimeToComplete INTEGER UNSIGNED, ResultId BIGINT not null, Comment text, IgnoreReading TINYINT(1) not null, primary key (Id));
alter table PlaythroughMissionAttempt add index (MissionId), add constraint FK6DAC85FEC144F77D foreign key (MissionId) references Mission (Id);
alter table PlaythroughMissionAttempt add index (GamerId), add constraint FK6DAC85FE2531ACF2 foreign key (GamerId) references Gamer (Id);
alter table PlaythroughMissionAttempt add index (PlaythroughSessionId), add constraint FK6DAC85FE142644E7 foreign key (PlaythroughSessionId) references PlaythroughSession (Id);
alter table PlaythroughMissionAttempt add index (ResultId), add constraint FK6DAC85FE4DD96549 foreign key (ResultId) references MissionAttemptResult (Id);
create index UniqueIdentifierIdx on PlaythroughMissionAttempt (UniqueIdentifier);
create index StartIdx on PlaythroughMissionAttempt (Start);
create index EndIdx on PlaythroughMissionAttempt (End);
create table PlaythroughCheckpointAttempt (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, PlaythroughMissionAttemptId BIGINT not null, CheckpointId BIGINT not null, Start DATETIME, End DATETIME, TimeToComplete INTEGER UNSIGNED, Comment text, IgnoreReading TINYINT(1) not null, primary key (Id));
alter table PlaythroughCheckpointAttempt add index (PlaythroughMissionAttemptId), add constraint FK118EF698D7745B43 foreign key (PlaythroughMissionAttemptId) references PlaythroughMissionAttempt (Id);
alter table PlaythroughCheckpointAttempt add index (CheckpointId), add constraint FK118EF698E18C79 foreign key (CheckpointId) references MissionCheckpoint (Id);
create index StartIdx on PlaythroughCheckpointAttempt (Start);
create index EndIdx on PlaythroughCheckpointAttempt (End);

-------------------------------------------------------------------------
-- 2013/04/16 - DJE - Network export statistics
-------------------------------------------------------------------------
create table MapNetworkExportStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, JobId CHAR(36) not null, Username VARCHAR(255) not null, MachineName VARCHAR(255) not null, ExportType VARCHAR(255) not null, JobCreationNumber INTEGER UNSIGNED not null, primary key (Id));
create table MapNetworkExportStat_MapSection (MapNetworkExportStatId BIGINT not null, MapSectionId BIGINT not null, primary key (MapNetworkExportStatId, MapSectionId));
alter table MapNetworkExportStat_MapSection add index (MapSectionId), add constraint FKDF34FE06B9B3DA6B foreign key (MapSectionId) references MapSection (Id);
alter table MapNetworkExportStat_MapSection add index (MapNetworkExportStatId), add constraint FKDF34FE06FDFCABD9 foreign key (MapNetworkExportStatId) references MapNetworkExportStat (Id);
create index UsernameIdx on MapNetworkExportStat (Username);

-------------------------------------------------------------------------
-- 2013/04/12 - MET - Build feature update
-------------------------------------------------------------------------
alter table MissionAttemptStat add column TimeToComplete INTEGER UNSIGNED;
alter table MissionCheckpointAttempt add column TimeToComplete INTEGER UNSIGNED;

-------------------------------------------------------------------------
-- 2013/04/09 - MET - Build feature update
-------------------------------------------------------------------------
alter table Build drop column HasProcessedStats;
alter table Build add column HasAutomatedNighttimeMapOnlyStats TINYINT(1) NOT NULL;

-------------------------------------------------------------------------
-- 2013/04/08 - MET - Capture stat fixes
-------------------------------------------------------------------------
alter table CapturePedAndVehicleResult MODIFY MinNumPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY AvgNumPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MaxNumPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MinActivePeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY AvgActivePeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MaxActivePeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MinInactivePeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY AvgInactivePeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MaxInactivePeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MinEventScanHiPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY AvgEventScanHiPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MaxEventScanHiPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MinEventScanLoPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY AvgEventScanLoPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MaxEventScanLoPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MinMotionTaskHiPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY AvgMotionTaskHiPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MaxMotionTaskHiPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MinMotionTaskLoPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY AvgMotionTaskLoPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MaxMotionTaskLoPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MinPhysicsHiPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY AvgPhysicsHiPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MaxPhysicsHiPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MinPhysicsLoPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY AvgPhysicsLoPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MaxPhysicsLoPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MinEntityScanHiPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY AvgEntityScanHiPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MaxEntityScanHiPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MinEntityScanLoPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY AvgEntityScanLoPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MaxEntityScanLoPeds INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MinNumVehicles INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY AvgNumVehicles INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MaxNumVehicles INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MinActiveVehicles INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY AvgActiveVehicles INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MaxActiveVehicles INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MinInactiveVehicles INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY AvgInactiveVehicles INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MaxInactiveVehicles INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MinRealVehicles INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY AvgRealVehicles INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MaxRealVehicles INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MinDummyVehicles INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY AvgDummyVehicles INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MaxDummyVehicles INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MinSuperDummyVehicles INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY AvgSuperDummyVehicles INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MaxSuperDummyVehicles INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MinNetworkDummyVehicles INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY AvgNetworkDummyVehicles INTEGER UNSIGNED;
alter table CapturePedAndVehicleResult MODIFY MaxNetworkDummyVehicles INTEGER UNSIGNED;


-------------------------------------------------------------------------
-- 2013/03/26 - DW - Resource Stats
-------------------------------------------------------------------------

create table ResourceBucketType (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Name VARCHAR(255) not null unique, Value INTEGER, primary key (Id));
create table ResourceBucket (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Size INTEGER UNSIGNED not null, Capacity INTEGER UNSIGNED not null, BucketId INTEGER UNSIGNED not null, BucketTypeId BIGINT not null, ResourceStatId BIGINT not null, primary key (Id));
create table ResourceName (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255) not null, Identifier VARCHAR(255) not null, primary key (Id) );
create table ResourceStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, PlatformId BIGINT not null, FileTypeId BIGINT not null, ResourceNameId BIGINT not null, Timestamp DATETIME not null, primary key (Id));

alter table ResourceBucket add index (BucketTypeId), add constraint FK281409B097AA2559 foreign key (BucketTypeId) references ResourceBucketType (Id);
alter table ResourceBucket add index (ResourceStatId), add constraint FK281409B0C9D24D97 foreign key (ResourceStatId) references ResourceStat (Id);
create index IdentifierIdx on ResourceName (Identifier);

alter table ResourceStat add index (PlatformId), add constraint FK3415098B1A9B7F8 foreign key (PlatformId) references Platform (Id);
alter table ResourceStat add index (FileTypeId), add constraint FK3415098471283B6 foreign key (FileTypeId) references FileType (Id);
alter table ResourceStat add index (ResourceNameId), add constraint FK341509861458DA3 foreign key (ResourceNameId) references ResourceName (Id);

-------------------------------------------------------------------------
-- 2013/03/20 - MET - Friendly names
-------------------------------------------------------------------------
alter table Weapon add column FriendlyName VARCHAR(255);
alter table Vehicle add column FriendlyName VARCHAR(255);
alter table RadioStation add column FriendlyName VARCHAR(255);


-------------------------------------------------------------------------
-- 2013/03/15 - MET - Map export stats additional information.
-------------------------------------------------------------------------
create table MapExportStat_Platform (PlatformId BIGINT not null, MapExportStatId BIGINT not null, primary key (MapExportStatId, PlatformId));
alter table MapExportStat_Platform add index (MapExportStatId), add constraint FKFA99D7628E9AC6F5 foreign key (MapExportStatId) references MapExportStat (Id);
alter table MapExportStat_Platform add index (PlatformId), add constraint FKFA99D762B1A9B7F8 foreign key (PlatformId) references Platform (Id);
create table MapExportStat_MapSection (MapSectionId BIGINT not null, MapExportStatId BIGINT not null, primary key (MapExportStatId, MapSectionId));
alter table MapExportStat_MapSection add index (MapExportStatId), add constraint FKA9A740108E9AC6F5 foreign key (MapExportStatId) references MapExportStat (Id);
alter table MapExportStat_MapSection add index (MapSectionId), add constraint FKA9A74010B9B3DA6B foreign key (MapSectionId) references MapSection (Id);

alter table MapExportStat add column ProcessorName VARCHAR(255), add column ProcessorCores INTEGER UNSIGNED, add column InstalledMemory BIGINT UNSIGNED, add column PrivateBytesStart BIGINT UNSIGNED, add column PrivateBytesEnd BIGINT UNSIGNED;
alter table MapExportStat drop column UsingAP3;
alter table MapExportStat MODIFY ToolsVersion float;

INSERT IGNORE INTO MapExportStat_MapSection (MapSectionId, MapExportStatId) SELECT MapSectionId, MapExportStatId FROM mapexportsectionexportstat WHERE MapSectionId IS NOT NULL;


-------------------------------------------------------------------------
-- 2013/03/12 - MET - Shop purchase information.
-------------------------------------------------------------------------
create table ShopItem (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255), Identifier VARCHAR(255) not null, primary key (Id));
create table TelemetryShopPurchase (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Timestamp DATETIME not null, GamerId BIGINT not null, PlatformId BIGINT not null, BuildId BIGINT not null, SpSessionId BIGINT, MpGamerSessionId BIGINT, ShopItemId BIGINT not null, AmountSpent INTEGER UNSIGNED not null, primary key (Id));

alter table TelemetryShopPurchase add index (GamerId), add constraint FK76F37CEA2531ACF2 foreign key (GamerId) references Gamer (Id);
alter table TelemetryShopPurchase add index (PlatformId), add constraint FK76F37CEAB1A9B7F8 foreign key (PlatformId) references Platform (Id);
alter table TelemetryShopPurchase add index (BuildId), add constraint FK76F37CEAD949985D foreign key (BuildId) references Build (Id);
alter table TelemetryShopPurchase add index (SpSessionId), add constraint FK76F37CEA87772080 foreign key (SpSessionId) references SingleplayerSession (Id);
alter table TelemetryShopPurchase add index (MpGamerSessionId), add constraint FK76F37CEA422854E5 foreign key (MpGamerSessionId) references MultiplayerGamerSession (Id);
alter table TelemetryShopPurchase add index (ShopItemId), add constraint FK76F37CEAA975098B foreign key (ShopItemId) references ShopItem (Id);
create index TimestampIdx on TelemetryShopPurchase (Timestamp);


-------------------------------------------------------------------------
-- 2013/03/12 - MET - Game name/model name seperation for vehicles.
-------------------------------------------------------------------------
alter table Vehicle add column GameName VARCHAR(255);


-------------------------------------------------------------------------
-- 2013/03/05 - MET - Freemode ambient missions.
-------------------------------------------------------------------------
create table CrateDropMissionInstance (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Timestamp DATETIME not null, GamerId BIGINT not null, PlatformId BIGINT not null, BuildId BIGINT not null, MpGamerSessionId BIGINT not null, ExperienceEarned INTEGER UNSIGNED, CashEarned INTEGER UNSIGNED, CashSpent INTEGER UNSIGNED, WeaponId BIGINT, EnemiesKilled INTEGER UNSIGNED, primary key (Id));
create table HoldUpMissionInstance (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Timestamp DATETIME not null, GamerId BIGINT not null, PlatformId BIGINT not null, BuildId BIGINT not null, MpGamerSessionId BIGINT not null, ExperienceEarned INTEGER UNSIGNED, CashEarned INTEGER UNSIGNED, CashSpent INTEGER UNSIGNED, ShopkeepersKilled INTEGER UNSIGNED, primary key (Id));
create table ImportExportMissionInstance (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Timestamp DATETIME not null, GamerId BIGINT not null, PlatformId BIGINT not null, BuildId BIGINT not null, MpGamerSessionId BIGINT not null, ExperienceEarned INTEGER UNSIGNED, CashEarned INTEGER UNSIGNED, CashSpent INTEGER UNSIGNED, primary key (Id));
create table ProstituteMissionInstance (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Timestamp DATETIME not null, GamerId BIGINT not null, PlatformId BIGINT not null, BuildId BIGINT not null, MpGamerSessionId BIGINT not null, ExperienceEarned INTEGER UNSIGNED, CashEarned INTEGER UNSIGNED, CashSpent INTEGER UNSIGNED, PlayerWasProstitute TINYINT(1), NumberOfServices INTEGER UNSIGNED, primary key (Id));
create table RaceToPointMissionInstance (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Timestamp DATETIME not null, GamerId BIGINT not null, PlatformId BIGINT not null, BuildId BIGINT not null, MpGamerSessionId BIGINT not null, ExperienceEarned INTEGER UNSIGNED, CashEarned INTEGER UNSIGNED, CashSpent INTEGER UNSIGNED, RaceWon TINYINT(1), primary key (Id));
create table SecurityVanMissionInstance (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Timestamp DATETIME not null, GamerId BIGINT not null, PlatformId BIGINT not null, BuildId BIGINT not null, MpGamerSessionId BIGINT not null, ExperienceEarned INTEGER UNSIGNED, CashEarned INTEGER UNSIGNED, CashSpent INTEGER UNSIGNED, primary key (Id));
alter table CrateDropMissionInstance add index (GamerId), add constraint FKB6C4A8AD2531ACF2 foreign key (GamerId) references Gamer (Id);
alter table CrateDropMissionInstance add index (PlatformId), add constraint FKB6C4A8ADB1A9B7F8 foreign key (PlatformId) references Platform (Id);
alter table CrateDropMissionInstance add index (BuildId), add constraint FKB6C4A8ADD949985D foreign key (BuildId) references Build (Id);
alter table CrateDropMissionInstance add index (MpGamerSessionId), add constraint FKB6C4A8AD422854E5 foreign key (MpGamerSessionId) references MultiplayerGamerSession (Id);
alter table CrateDropMissionInstance add index (WeaponId), add constraint FKB6C4A8AD72CFEB39 foreign key (WeaponId) references Weapon (Id);
create index TimestampIdx on CrateDropMissionInstance (Timestamp);
alter table HoldUpMissionInstance add index (GamerId), add constraint FK19CA3A9B2531ACF2 foreign key (GamerId) references Gamer (Id);
alter table HoldUpMissionInstance add index (PlatformId), add constraint FK19CA3A9BB1A9B7F8 foreign key (PlatformId) references Platform (Id);
alter table HoldUpMissionInstance add index (BuildId), add constraint FK19CA3A9BD949985D foreign key (BuildId) references Build (Id);
alter table HoldUpMissionInstance add index (MpGamerSessionId), add constraint FK19CA3A9B422854E5 foreign key (MpGamerSessionId) references MultiplayerGamerSession (Id);
create index TimestampIdx on HoldUpMissionInstance (Timestamp);
alter table ImportExportMissionInstance add index (GamerId), add constraint FKFAEA0B122531ACF2 foreign key (GamerId) references Gamer (Id);
alter table ImportExportMissionInstance add index (PlatformId), add constraint FKFAEA0B12B1A9B7F8 foreign key (PlatformId) references Platform (Id);
alter table ImportExportMissionInstance add index (BuildId), add constraint FKFAEA0B12D949985D foreign key (BuildId) references Build (Id);
alter table ImportExportMissionInstance add index (MpGamerSessionId), add constraint FKFAEA0B12422854E5 foreign key (MpGamerSessionId) references MultiplayerGamerSession (Id);
create index TimestampIdx on ImportExportMissionInstance (Timestamp);
alter table ProstituteMissionInstance add index (GamerId), add constraint FKEEDD98B62531ACF2 foreign key (GamerId) references Gamer (Id);
alter table ProstituteMissionInstance add index (PlatformId), add constraint FKEEDD98B6B1A9B7F8 foreign key (PlatformId) references Platform (Id);
alter table ProstituteMissionInstance add index (BuildId), add constraint FKEEDD98B6D949985D foreign key (BuildId) references Build (Id);
alter table ProstituteMissionInstance add index (MpGamerSessionId), add constraint FKEEDD98B6422854E5 foreign key (MpGamerSessionId) references MultiplayerGamerSession (Id);
create index TimestampIdx on ProstituteMissionInstance (Timestamp);
alter table RaceToPointMissionInstance add index (GamerId), add constraint FK887EA6A32531ACF2 foreign key (GamerId) references Gamer (Id);
alter table RaceToPointMissionInstance add index (PlatformId), add constraint FK887EA6A3B1A9B7F8 foreign key (PlatformId) references Platform (Id);
alter table RaceToPointMissionInstance add index (BuildId), add constraint FK887EA6A3D949985D foreign key (BuildId) references Build (Id);
alter table RaceToPointMissionInstance add index (MpGamerSessionId), add constraint FK887EA6A3422854E5 foreign key (MpGamerSessionId) references MultiplayerGamerSession (Id);
create index TimestampIdx on RaceToPointMissionInstance (Timestamp);
alter table SecurityVanMissionInstance add index (GamerId), add constraint FK6EF41C822531ACF2 foreign key (GamerId) references Gamer (Id);
alter table SecurityVanMissionInstance add index (PlatformId), add constraint FK6EF41C82B1A9B7F8 foreign key (PlatformId) references Platform (Id);
alter table SecurityVanMissionInstance add index (BuildId), add constraint FK6EF41C82D949985D foreign key (BuildId) references Build (Id);
alter table SecurityVanMissionInstance add index (MpGamerSessionId), add constraint FK6EF41C82422854E5 foreign key (MpGamerSessionId) references MultiplayerGamerSession (Id);
create index TimestampIdx on SecurityVanMissionInstance (Timestamp);

-------------------------------------------------------------------------
-- 2013/03/05 - MET - Profile stats fix.
-------------------------------------------------------------------------
alter table ProfileStatBase add column SubmissionDate DATETIME not null, drop column ModifiedOn, drop index CreatedOnIdx;
update ProfileStatBase set SubmissionDate=CreatedOn;
create index SubmissionDateIdx on ProfileStatBase (SubmissionDate);

-------------------------------------------------------------------------
-- 2013/03/01 - MET - Filtering by build.
-------------------------------------------------------------------------
alter table RadioStationSessionStat add column BuildId BIGINT;
alter table VehicleDistanceDrivenStat add column BuildId BIGINT;
alter table CutsceneViewStat add column BuildId BIGINT;
alter table RadioStationSessionStat add index (BuildId), add constraint FKF58E5E33D949985D foreign key (BuildId) references Build (Id);
alter table VehicleDistanceDrivenStat add index (BuildId), add constraint FK933C92D3D949985D foreign key (BuildId) references Build (Id);
alter table CutsceneViewStat add index (BuildId), add constraint FKE77B7C6FD949985D foreign key (BuildId) references Build (Id);


-------------------------------------------------------------------------
-- 2013/02/27 - MET - Social Club user information.
-------------------------------------------------------------------------
create table SocialClubCountry (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, CountryCode VARCHAR(255) not null, Name VARCHAR(255) not null, primary key (Id));
create index CountryCodeIdx on SocialClubCountry (CountryCode);

create table SocialClubActor (Id BIGINT NOT NULL AUTO_INCREMENT, SubType TINYINT UNSIGNED not null, CreatedOn DATETIME not null, Name VARCHAR(255) not null, CountryId BIGINT, DateOfBirth DATETIME, SCRockstarId BIGINT, LastUpdated DATETIME, ParentGroupId BIGINT, primary key (Id));
create table SocialClubGroup_SocialClubActor (SocialClubActorId BIGINT not null, SocialClubGroupId BIGINT not null, primary key (SocialClubGroupId, SocialClubActorId));
alter table SocialClubActor add index (CountryId), add constraint FK449AEAE696BB4D60 foreign key (CountryId) references SocialClubCountry (Id);
alter table SocialClubGroup_SocialClubActor add index (SocialClubGroupId), add constraint FKDB4ACB8FFCF29783 foreign key (SocialClubGroupId) references SocialClubActor (Id);
alter table SocialClubGroup_SocialClubActor add index (SocialClubActorId), add constraint FKDB4ACB8FABADC2B7 foreign key (SocialClubActorId) references SocialClubActor (Id);

alter table Gamer add column SocialClubUserId BIGINT, add column SCUserLastVerified DATETIME not null;
alter table Gamer add index (SocialClubUserId), add constraint FK95F41A62AA5CDBFF foreign key (SocialClubUserId) references SocialClubActor (Id);
update Gamer set SCUserLastVerified='0001-01-01 00:00:00';


-------------------------------------------------------------------------
-- 2013/02/21 - MET - Processed stat resolutions.
-------------------------------------------------------------------------
alter table ProcessedStatResolution add column Enabled TINYINT(1) not null;
update ProcessedStatResolution set Enabled=1;


-------------------------------------------------------------------------
-- 2013/02/21 - MET - Cleanup
-------------------------------------------------------------------------
drop table MissionDeathStat;


-------------------------------------------------------------------------
-- 2013/02/21 - MET - Report presets.
-------------------------------------------------------------------------
create table ReportPreset (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Name VARCHAR(255) not null, primary key (Id));
create table ReportPresetParameter (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, PresetId BIGINT not null, KeyName VARCHAR(255) not null, Value VARCHAR(255) not null, primary key (Id));
alter table ReportPresetParameter add index (PresetId), add constraint FK5DC6205214E5BAC9 foreign key (PresetId) references ReportPreset (Id);


-------------------------------------------------------------------------
-- 2013/02/18 - MET - Multiplayer match friendly names.
-------------------------------------------------------------------------
create table SocialClubMission (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, UGCIdentifier VARCHAR(255) not null, Name VARCHAR(255) not null, Description VARCHAR(255), Creator VARCHAR(255) not null, MatchTypeId BIGINT not null, primary key (Id));
create index UniqueIdentifierIdx on SocialClubMission (UGCIdentifier);
alter table MultiplayerMatch add column SCMissionId BIGINT;
alter table MultiplayerMatch add index (SCMissionId), add constraint FK7BB14F2D36FADC40 foreign key (SCMissionId) references SocialClubMission (Id);
alter table SocialClubMission add index (MatchTypeId), add constraint FKEFE5EADF7BB663A foreign key (MatchTypeId) references FreemodeMatchType (Id);


-------------------------------------------------------------------------
-- 2013/02/18 - MET - Player injury stats.
-------------------------------------------------------------------------
create table TelemetryCoordPlayerInjuryStat (Id BIGINT not null, TimeSinceSpawned FLOAT not null, SpawnX FLOAT not null, SpawnY FLOAT not null, SpawnZ FLOAT not null, primary key (Id));


-------------------------------------------------------------------------
-- 2013/02/18 - MET - Allowing multiple telemetry syncs per day.
-------------------------------------------------------------------------

alter table TelemetryStatFile change SubmissionDate Start DATETIME;
alter table TelemetryStatFile add End DATETIME;
alter table TelemetryStatFile drop index submission_date_idx;
create index StartIdx on TelemetryStatFile (Start);
create index EndIdx on TelemetryStatFile (End);
update TelemetryStatFile set End=DATE(DATE_ADD(Start, INTERVAL 1 DAY));


-------------------------------------------------------------------------
-- 2013/02/15 - MET - Adding index to profile stat table.
-------------------------------------------------------------------------

create index CreatedOnIdx on ProfileStatBase (CreatedOn);

-------------------------------------------------------------------------
-- 2013/02/13 - DW - Adding the timesplayed played to the clip telemetry
-------------------------------------------------------------------------

alter table TelemetryClipStat add column TimesPlayed INTEGER UNSIGNED not null;

-------------------------------------------------------------------------
-- 2013/02/13 - MET - Capture frame times
-------------------------------------------------------------------------

create table CaptureFrametimeResult (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, SessionId BIGINT not null, ZoneId BIGINT not null, TelemetryStatId BIGINT, Location LONGBLOB, Minimum FLOAT not null, Average FLOAT not null, Maximum FLOAT not null, StandardDeviation FLOAT not null, primary key (Id));
alter table CaptureFrametimeResult add index (SessionId), add constraint FK332B04F5DF629E57 foreign key (SessionId) references CaptureSession (Id);
alter table CaptureFrametimeResult add index (ZoneId), add constraint FK332B04F566E3E5C7 foreign key (ZoneId) references NameHash (Id);
alter table CaptureFrametimeResult add index (TelemetryStatId), add constraint FK332B04F551E161B1 foreign key (TelemetryStatId) references TelemetryStat (Id);

-------------------------------------------------------------------------
-- 2013/02/07 - DW - Memory Skeleton Telemetry
-------------------------------------------------------------------------

create table TelemetryMemorySkeleton (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, BuildId BIGINT not null, PlatformId BIGINT not null, LocationX FLOAT not null, LocationY FLOAT not null, LocationZ FLOAT not null, MissionId BIGINT not null, BuildConfigId BIGINT, Timestamp DATETIME not null, Vehicles INTEGER UNSIGNED not null, Peds INTEGER UNSIGNED not null, Objects INTEGER UNSIGNED not null, Buildings INTEGER UNSIGNED not null, Total INTEGER UNSIGNED not null, primary key (Id));
alter table TelemetryMemorySkeleton add index (BuildId), add constraint FK86BC3DFFD949985D foreign key (BuildId) references Build (Id);
alter table TelemetryMemorySkeleton add index (PlatformId), add constraint FK86BC3DFFB1A9B7F8 foreign key (PlatformId) references Platform (Id);
alter table TelemetryMemorySkeleton add index (MissionId), add constraint FK86BC3DFFC144F77D foreign key (MissionId) references Mission (Id);
alter table TelemetryMemorySkeleton add index (BuildConfigId), add constraint FK86BC3DFFAD58D888 foreign key (BuildConfigId) references BuildConfig (Id);


-------------------------------------------------------------------------
-- 2013/01/31 - MET - Playthrough tracking changes.
-------------------------------------------------------------------------
alter table PlaythroughSession add column FriendlyName varchar(255);
alter table MissionAttemptStat add column PlaythroughComment text;


-------------------------------------------------------------------------
-- 2013/01/28 - DW - Memory pools and memory stores telemetry tables
-------------------------------------------------------------------------

create table TelemetryMemoryStores (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, BuildId BIGINT not null, PlatformId BIGINT not null, LocationX FLOAT not null, LocationY FLOAT not null, LocationZ FLOAT not null, BuildConfigId BIGINT, Timestamp DATETIME not null, primary key (Id));
create table TelemetryMemoryPools (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, BuildId BIGINT not null, PlatformId BIGINT not null, LocationX FLOAT not null, LocationY FLOAT not null, LocationZ FLOAT not null, MissionId BIGINT not null, BuildConfigId BIGINT, Timestamp DATETIME not null, primary key (Id));
create table TelemetryMemoryStore (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, MemoryStoreId BIGINT not null, Used INTEGER UNSIGNED not null, Free INTEGER UNSIGNED not null, Peak INTEGER UNSIGNED not null, StoresId BIGINT not null, primary key (Id));
create table TelemetryMemoryPool (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, MemoryPoolId BIGINT not null, Peak INTEGER UNSIGNED not null, PoolsId BIGINT not null, primary key (Id));
create table MemoryStore (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255), Identifier VARCHAR(255) not null unique, primary key (Id));
create table MemoryPool (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255), Identifier VARCHAR(255) not null unique, primary key (Id));
alter table TelemetryMemoryStores add index (BuildId), add constraint FKA1F4E7D8D949985D foreign key (BuildId) references Build (Id);
alter table TelemetryMemoryStores add index (PlatformId), add constraint FKA1F4E7D8B1A9B7F8 foreign key (PlatformId) references Platform (Id);
alter table TelemetryMemoryStores add index (BuildConfigId), add constraint FKA1F4E7D8AD58D888 foreign key (BuildConfigId) references BuildConfig (Id);
alter table TelemetryMemoryPools add index (BuildId), add constraint FK9DD534A9D949985D foreign key (BuildId) references Build (Id);
alter table TelemetryMemoryPools add index (PlatformId), add constraint FK9DD534A9B1A9B7F8 foreign key (PlatformId) references Platform (Id);
alter table TelemetryMemoryPools add index (MissionId), add constraint FK9DD534A9C144F77D foreign key (MissionId) references Mission (Id);
alter table TelemetryMemoryPools add index (BuildConfigId), add constraint FK9DD534A9AD58D888 foreign key (BuildConfigId) references BuildConfig (Id);
alter table TelemetryMemoryStore add index (MemoryStoreId), add constraint FKF01556655ECFE01D foreign key (MemoryStoreId) references MemoryStore (Id);
alter table TelemetryMemoryStore add index (StoresId), add constraint FKF0155665963B30E7 foreign key (StoresId) references TelemetryMemoryStores (Id);
alter table TelemetryMemoryPool add index (MemoryPoolId), add constraint FK884F3128AD89F753 foreign key (MemoryPoolId) references MemoryPool (Id);
alter table TelemetryMemoryPool add index (PoolsId), add constraint FK884F3128E54A30B foreign key (PoolsId) references TelemetryMemoryPools (Id);


-------------------------------------------------------------------------
-- 2013/01/25 - MET - Multiplayer matches.
-------------------------------------------------------------------------
create table FreemodeMatchType (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255), Identifier VARCHAR(255) not null unique, primary key (Id));
INSERT INTO `freemodematchtype` VALUES ('1', '2013-01-29 11:28:08', '2013-01-29 11:28:11', 'MISSION', '0');
INSERT INTO `freemodematchtype` VALUES ('2', '2013-01-29 11:28:29', '2013-01-29 11:28:31', 'DEATHMATCH', '1');
INSERT INTO `freemodematchtype` VALUES ('3', '2013-01-29 11:28:50', '2013-01-29 11:28:52', 'RACE', '2');
INSERT INTO `freemodematchtype` VALUES ('4', '2013-01-29 11:29:00', '2013-01-29 11:29:02', 'SURVIVAL', '3');
INSERT INTO `freemodematchtype` VALUES ('5', '2013-01-29 11:29:16', '2013-01-29 11:29:18', 'RAMPAGE', '5');
INSERT INTO `freemodematchtype` VALUES ('6', '2013-01-29 11:29:30', '2013-01-29 11:29:33', 'GANGHIDEOUT', '6');
INSERT INTO `freemodematchtype` VALUES ('7', '2013-01-29 11:29:46', '2013-01-29 11:29:48', 'BASE_JUMP', '8');
INSERT INTO `freemodematchtype` VALUES ('8', '2013-01-29 11:30:00', '2013-01-29 11:30:02', 'GOLF', '11');
INSERT INTO `freemodematchtype` VALUES ('9', '2013-01-29 11:30:11', '2013-01-29 11:30:13', 'TENNIS', '12');
INSERT INTO `freemodematchtype` VALUES ('10', '2013-01-29 11:30:22', '2013-01-29 11:30:25', 'SHOOTING_RANGE', '13');
INSERT INTO `freemodematchtype` VALUES ('11', '2013-01-29 11:30:35', '2013-01-29 11:30:37', 'DARTS', '14');
INSERT INTO `freemodematchtype` VALUES ('12', '2013-01-29 11:30:49', '2013-01-29 11:30:51', 'ARM_WRESTLING', '15');

create table MultiplayerGamerSession (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, MultiplayerSessionId BIGINT not null, GamerId BIGINT not null, Joined DATETIME, _Left DATETIME, primary key (Id));
alter table MultiplayerGamerSession add index (MultiplayerSessionId), add constraint FKC630533E7755AE7E foreign key (MultiplayerSessionId) references MultiplayerSession (Id);
alter table MultiplayerGamerSession add index (GamerId), add constraint FKC630533E2531ACF2 foreign key (GamerId) references Gamer (Id);

create table MultiplayerMatch (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, SessionId BIGINT not null, UniqueIdentifier BIGINT UNSIGNED not null, MatchTypeId BIGINT, MatchCreator VARCHAR(255), MatchName VARCHAR(255), Start DATETIME not null, primary key (Id));
alter table MultiplayerMatch add index (SessionId), add constraint FK7BB14F2DC2001D88 foreign key (SessionId) references MultiplayerSession (Id);
alter table MultiplayerMatch add index (MatchTypeId), add constraint FK7BB14F2D7BB663A foreign key (MatchTypeId) references FreemodeMatchType (Id);
create index UniqueIdentifierIdx on MultiplayerMatch (UniqueIdentifier);

create table MultiplayerMatchParticipant (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, MatchId BIGINT not null, GamerId BIGINT not null, Start DATETIME, End DATETIME, Experience INTEGER UNSIGNED, Cash INTEGER UNSIGNED, KillStreak INTEGER UNSIGNED, Kills INTEGER UNSIGNED, Deaths INTEGER UNSIGNED, Suicides INTEGER UNSIGNED, Rank INTEGER UNSIGNED, VehicleId BIGINT, primary key (Id));
alter table MultiplayerMatchParticipant add index (MatchId), add constraint FKA05F650CC267545A foreign key (MatchId) references MultiplayerMatch (Id);
alter table MultiplayerMatchParticipant add index (GamerId), add constraint FKA05F650C2531ACF2 foreign key (GamerId) references Gamer (Id);    
alter table MultiplayerMatchParticipant add index (VehicleId), add constraint FKA05F650C5761A995 foreign key (VehicleId) references Vehicle (Id);

alter table CaptureSession drop foreign key FK396ADBD8332FA5E3, drop column MpSessionId;
alter table CaptureSession add column MpGamerSessionId BIGINT;
alter table CaptureSession add index (MpGamerSessionId), add constraint FK396ADBD8422854E5 foreign key (MpGamerSessionId) references MultiplayerGamerSession (Id);

alter table CutsceneViewStat drop foreign key FKE77B7C6F332FA5E3, drop column MpSessionId;
alter table CutsceneViewStat add column PlatformId BIGINT not null;
alter table CutsceneViewStat add column MpGamerSessionId BIGINT;
update CutsceneViewStat set PlatformId=4;
alter table CutsceneViewStat add index (PlatformId), add constraint FKE77B7C6FB1A9B7F8 foreign key (PlatformId) references `Platform` (Id);
alter table CutsceneViewStat add index (MpGamerSessionId), add constraint FKE77B7C6F422854E5 foreign key (MpGamerSessionId) references MultiplayerGamerSession (Id);

alter table MissionAttemptStat drop foreign key FKB85827FB332FA5E3, drop column MpSessionId;

alter table RadioStationSessionStat drop foreign key FKF58E5E33332FA5E3, drop column MpSessionId;
alter table RadioStationSessionStat add column PlatformId BIGINT not null;
update RadioStationSessionStat set PlatformId=4;
alter table RadioStationSessionStat add index (PlatformId), add constraint FKF58E5E33B1A9B7F8 foreign key (PlatformId) references Platform (Id);
alter table RadioStationSessionStat add column MpGamerSessionId BIGINT;
alter table RadioStationSessionStat add index (MpGamerSessionId), add constraint FKF58E5E33422854E5 foreign key (MpGamerSessionId) references MultiplayerGamerSession (Id);

update MultiplayerGameMode set Name=Label;
alter table MultiplayerGameMode drop column Label;

alter table MultiplayerSession drop column Start, drop column End, drop column IsRanked;
alter table MultiplayerSession add column BuildId BIGINT not null, add column PlatformId BIGINT not null, add column LevelId BIGINT not null, add column GameModeId BIGINT not null;
update MultiplayerSession set BuildId=1, PlatformId=4, LevelId=2, GameModeId=2;
alter table MultiplayerSession add index (BuildId), add constraint FK87BC3A6D949985D foreign key (BuildId) references Build (Id), add index (PlatformId), add constraint FK87BC3A6B1A9B7F8 foreign key (PlatformId) references Platform (Id), add index (LevelId), add constraint FK87BC3A67DC7F5BD foreign key (LevelId) references Level (Id), add index (GameModeId), add constraint FK87BC3A666D857C8 foreign key (GameModeId) references MultiplayerGameMode (Id);

alter table SingleplayerSession add column BuildId BIGINT not null, add column PlatformId BIGINT not null, add column LevelId BIGINT not null;
update SingleplayerSession set BuildId=1, PlatformId=4, LevelId=2;
alter table SingleplayerSession add index (BuildId), add constraint FK5B994717D949985D foreign key (BuildId) references Build (Id), add index (PlatformId), add constraint FK5B994717B1A9B7F8 foreign key (PlatformId) references Platform (Id), add index (LevelId), add constraint FK5B9947177DC7F5BD foreign key (LevelId) references Level (Id);

alter table TelemetryStatPacket drop foreign key FK349A2F913EE6B8B2, drop column GamerId, drop foreign key FK349A2F91D949985D, drop column BuildId, drop foreign key FK349A2F91B1A9B7F8, drop column PlatformId, drop foreign key FK349A2F917DC7F5BD, drop column LevelId, drop foreign key FK349A2F91ADE1531C, drop column GameTypeId, drop foreign key FK349A2F9118B3906B, drop column MissionAttemptId;

alter table TelemetryStat drop foreign key FK28ABAAFD332FA5E3, drop column MpSessionId, drop foreign key FK28ABAAFDADE1531C, drop column GameTypeId, add column MpGamerSessionId BIGINT, add column MpMatchId BIGINT;
alter table TelemetryStat add index (MpGamerSessionId), add constraint FK28ABAAFD422854E5 foreign key (MpGamerSessionId) references MultiplayerGamerSession (Id), add index (MpMatchId), add constraint FK28ABAAFDEAF1FB7F foreign key (MpMatchId) references MultiplayerMatch (Id);

-------------------------------------------------------------------------
-- 2013/01/24 - DJE - Adding XGE map export statistics.
-------------------------------------------------------------------------
alter table mapexportstat add column XGEEnabled TINYINT(1);
alter table mapexportstat add column XGEStandalone TINYINT(1);
alter table mapexportstat add column XGEForceCPUCount INT;

-------------------------------------------------------------------------
-- 2013/01/23 - MET - Adding mission attempt to cutscene view stats.
-------------------------------------------------------------------------
alter table CutsceneViewStat add column MissionAttemptId BIGINT;
alter table CutsceneViewStat add index (MissionAttemptId), add constraint FKE77B7C6F18B3906B foreign key (MissionAttemptId) references MissionAttemptStat (Id);
alter table MissionAttemptStat add column BuildId BIGINT;
alter table MissionAttemptStat add index (BuildId), add constraint FKB85827FBD949985D foreign key (BuildId) references Build (Id);


-------------------------------------------------------------------------
-- 2013/01/16 - DW - Clip Stats ( telemetry) Table Created
--   reupdated on 21/01/13
-------------------------------------------------------------------------
CREATE TABLE TelemetryClipStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ClipId BIGINT not null, ClipDictionaryId BIGINT not null, BuildId BIGINT not null, Timestamp DATETIME not null, primary key (Id));
ALTER TABLE TelemetryClipStat add index (ClipId), add constraint FK56E41B13AD6C9431 foreign key (ClipId) references Clip (Id);
ALTER TABLE TelemetryClipStat add index (ClipDictionaryId), add constraint FK56E41B13C006C871 foreign key (ClipDictionaryId) references ClipDictionary (Id);
ALTER TABLE TelemetryClipStat add index (BuildId), add constraint FK56E41B13D949985D foreign key (BuildId) references Build (Id);


-------------------------------------------------------------------------
-- 2013/01/16 - DW - New tables added to support Clip stat telemetry 
-------------------------------------------------------------------------
CREATE TABLE ClipDictionary ( Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255), Identifier VARCHAR(255) not null unique, primary key (Id));
CREATE TABLE Animation (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255), Identifier VARCHAR(255) not null unique, primary key (Id));
CREATE TABLE Clip (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255), Identifier VARCHAR(255) not null unique, primary key (Id));


-------------------------------------------------------------------------
-- 2013/01/23 - MET - Removing some tables that are slowing telemetry data population
-------------------------------------------------------------------------
alter table TelemetryStat drop foreign key FK28ABAAFD217D5535, drop column MpLifeId, drop foreign key FK28ABAAFDB5245CDC, drop column SpLifeId;
drop table MultiplayerLife;
drop table SingleplayerLife;


-------------------------------------------------------------------------
-- 2013/01/23 - MET - Adding concept of Actor Groups
-------------------------------------------------------------------------
alter table Gamer add column SubType TINYINT UNSIGNED not null;
alter table Gamer change GamerTag Name VARCHAR(255) not null;
update Gamer set SubType=0;

create table ActorGroup_Actor (ActorId BIGINT not null, ActorGroupId BIGINT not null, primary key (ActorGroupId, ActorId));
alter table ActorGroup_Actor add index (ActorGroupId), add constraint FKA314CAA48FB5053E foreign key (ActorGroupId) references Gamer (Id);
alter table ActorGroup_Actor add index (ActorId), add constraint FKA314CAA4F4910B6E foreign key (ActorId) references Gamer (Id);


-------------------------------------------------------------------------
-- 2013/01/16 - MET - Adding a few indices
-------------------------------------------------------------------------
create index IdxIdx on MissionCheckpoint (Idx);
create index SessionIdIdx on MultiplayerSession (SessionId);
create index HashIdx on NameHash (Hash);
create index CreatedOnIdx on VehicleDistanceDrivenStat (CreatedOn);


-------------------------------------------------------------------------
-- 2013/01/16 - MET - MissionCheckpointAttempts
-------------------------------------------------------------------------

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `missionattemptresult`;
CREATE TABLE `missionattemptresult` (
  `Id` bigint(20) NOT NULL AUTO_INCREMENT,
  `CreatedOn` datetime NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Value` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE KEY `Name` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
INSERT INTO `missionattemptresult` VALUES ('1', '2013-01-15 16:46:53', 'Passed', '0');
INSERT INTO `missionattemptresult` VALUES ('2', '2013-01-15 16:46:53', 'Failed', '1');
INSERT INTO `missionattemptresult` VALUES ('3', '2013-01-15 16:46:53', 'Cancelled', '2');
INSERT INTO `missionattemptresult` VALUES ('4', '2013-01-15 16:46:53', 'Over', '3');
INSERT INTO `missionattemptresult` VALUES ('5', '2013-01-15 16:46:53', 'Unknown', '4');
SET FOREIGN_KEY_CHECKS=1;

update MissionAttemptStat set ResultId=5;
update MissionAttemptStat set ResultId=2 WHERE MissionFailed=1;
update MissionAttemptStat set ResultId=4 WHERE MissionOver=1;
update MissionAttemptStat set ResultId=3 WHERE MissionPassed=0 and MissionOver=0 and MissionFailed=0 and Start is not null and End is not null;
update MissionAttemptStat set ResultId=1 WHERE MissionPassed=1;
alter table MissionAttemptStat drop column MissionFailed, drop column MissionPassed, drop column MissionOver;
update MissionAttemptStat set Start='0000-00-00 00:00:00' where Start is null;
update MissionAttemptStat set End='0000-00-00 00:00:00' where End is null;
alter table MissionAttemptStat MODIFY Start DATETIME not null, modify End DATETIME not null;
alter table MissionAttemptStat drop column PlaythroughComment, drop foreign key FKB85827FBE18C79, drop column CheckpointId;

alter table TelemetryStatPacket drop foreign key FK349A2F91C144F77D, drop column MissionId, drop column MissionVariant;
alter table TelemetryStatPacket add column MissionAttemptId BIGINT, add index (MissionAttemptId), add constraint FK349A2F9118B3906B foreign key (MissionAttemptId) references MissionAttemptStat (Id);

alter table PlaythroughSession change StartTime Start DATETIME, change EndTime End DATETIME;

create table MissionCheckpointAttempt (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, MissionAttemptId BIGINT not null, CheckpointId BIGINT not null, Start DATETIME not null, End DATETIME not null, ResultId BIGINT not null, PlaythroughComment text, primary key (Id));
alter table MissionCheckpointAttempt add index (MissionAttemptId), add constraint FK63F7E26B18B3906B foreign key (MissionAttemptId) references MissionAttemptStat (Id);
alter table MissionCheckpointAttempt add index (CheckpointId), add constraint FK63F7E26BE18C79 foreign key (CheckpointId) references MissionCheckpoint (Id);
alter table MissionCheckpointAttempt add index (ResultId), add constraint FK63F7E26B4DD96549 foreign key (ResultId) references MissionAttemptResult (Id);
create index StartIdx on MissionCheckpointAttempt (Start);
create index EndIdx on MissionCheckpointAttempt (End);

alter table TelemetryStat drop foreign key FK28ABAAFDC144F77D, drop column MissionId, drop column MissionVariant, add column MissionCheckpointAttemptId BIGINT, add index (MissionCheckpointAttemptId), add constraint FK28ABAAFDEFD9A2A9 foreign key (MissionCheckpointAttemptId) references MissionCheckpointAttempt (Id);


-------------------------------------------------------------------------
-- 2013/01/10 - MET - Playthrough tracking
-------------------------------------------------------------------------
create table PlaythroughSession (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, BuildId BIGINT not null, PlatformId BIGINT not null, UserId BIGINT not null, StartTime DATETIME not null, EndTime DATETIME not null, primary key (Id));
create table PlaythroughUser (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255), primary key (Id));
alter table PlaythroughSession add index (BuildId), add constraint FKB0152429D949985D foreign key (BuildId) references Build (Id);
alter table PlaythroughSession add index (PlatformId), add constraint FKB0152429B1A9B7F8 foreign key (PlatformId) references Platform (Id);
alter table PlaythroughSession add index (UserId), add constraint FKB0152429C9134DE8 foreign key (UserId) references PlaythroughUser (Id);

create table MissionAttemptResult (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Name VARCHAR(255) not null unique, Value INTEGER, primary key (Id));

alter table Mission drop index Identifier;
create index IdentifierIdx on Mission (Identifier);
alter table Mission add column Active TINYINT(1) not null, add column InBugstar TINYINT(1) not null;
alter table MissionCheckpoint add column InBugstar TINYINT(1) not null;

alter table MissionAttemptStat add column UniqueIdentifier BIGINT(20) not null;
create index UniqueIdentifierIdx on MissionAttemptStat (UniqueIdentifier);
alter table MissionAttemptStat add column CheckpointId BIGINT, add column PlaythroughSessionId BIGINT, add column ResultId BIGINT, add column PlaythroughComment text, add index (CheckpointId), add constraint FKB85827FBE18C79 foreign key (CheckpointId) references MissionCheckpoint (Id), add index (PlaythroughSessionId), add constraint FKB85827FB142644E7 foreign key (PlaythroughSessionId) references PlaythroughSession (Id), add index (ResultId), add constraint FKB85827FB4DD96549 foreign key (ResultId) references MissionAttemptResult (Id);


-------------------------------------------------------------------------
-- 2013/01/10 - MET - Mission checkpoints
-------------------------------------------------------------------------
create table MissionCheckpoint (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255) not null, Identifier VARCHAR(255) not null, MissionId BIGINT not null, ProjectedAttempts FLOAT not null, ProjectedAttemptsMin FLOAT not null, ProjectedAttemptsMax FLOAT not null, Idx INTEGER UNSIGNED, primary key (Id));
alter table MissionCheckpoint add index (MissionId), add constraint FK33F4B6C6C144F77D foreign key (MissionId) references Mission (Id);
ALTER TABLE Mission MODIFY ProjectedAttempts FLOAT not null, MODIFY ProjectedAttemptsMin FLOAT not null, MODIFY ProjectedAttemptsMax FLOAT not null;


-------------------------------------------------------------------------
-- 2012/12/11 - MET - Add flag for capturing whether a mission is over.
-- For multiplayer missions where neither side fulfills all criteria
-- set byt the mission.
-------------------------------------------------------------------------
alter table MissionAttemptStat add column MissionOver TINYINT(1);
create index MissionOverIdx on MissionAttemptStat (MissionOver);
update MissionAttemptStat set MissionOver=0;
create table TelemetryMissionCheckpointStat (Id BIGINT not null, Checkpoint INTEGER UNSIGNED not null, PreviousCheckpoint INTEGER UNSIGNED not null, primary key (Id));
alter table TelemetryMissionCheckpointStat add index (Id), add constraint FKC66CE257F42D6966 foreign key (Id) references TelemetryStat (Id);


-------------------------------------------------------------------------
-- 2012/12/11 - DW - Add boolean 'PerfStats' to Capture Sesssion
-- - which is used for determining if a capture was made by the Cruise 
-- process 'perftester'.
-------------------------------------------------------------------------
alter table CaptureSession add column PerfStats TINYINT(1);
create index PerfStatsIdx on CaptureSession (PerfStats);

------------------------------------------------
-- 2012/12/07 - Adding flags to indicate whether builds have mem shortfall/shape test stats.
------------------------------------------------
alter table Build ADD HasMemShortfallStats TINYINT(1) NOT NULL;
alter table Build ADD HasShapeTestStats TINYINT(1) NOT NULL;

alter table ProcessedDrawListStat CHANGE MeanEntityCount TotalEntityCount float not null;
update ProcessedDrawListStat SET TotalEntityCount=TotalEntityCount*SampleSize;

alter table ProcessedMemShortfallStat CHANGE MeanPhysicalShortfall TotalPhysicalShortfall float not null;
alter table ProcessedMemShortfallStat CHANGE MeanVirtualShortfall TotalVirtualShortfall float not null;
update ProcessedMemShortfallStat SET TotalPhysicalShortfall=TotalPhysicalShortfall*SampleSize;
update ProcessedMemShortfallStat SET TotalVirtualShortfall=TotalVirtualShortfall*SampleSize;

alter table ProcessedFpsStat CHANGE MeanFps TotalFps float not null;
alter table ProcessedFpsStat CHANGE MeanUpdateTime TotalUpdateTime float not null;
alter table ProcessedFpsStat CHANGE MeanDrawTime TotalDrawTime float not null;
alter table ProcessedFpsStat CHANGE MeanGpuTime TotalGpuTime float not null;
alter table ProcessedFpsStat CHANGE MeanIndexCount TotalIndexCount float not null;
alter table ProcessedFpsStat CHANGE MeanGBufferIndexCount TotalGBufferIndexCount float not null;
update ProcessedFpsStat SET TotalFps=TotalFps*SampleSize;
update ProcessedFpsStat SET TotalUpdateTime=TotalUpdateTime*SampleSize;
update ProcessedFpsStat SET TotalDrawTime=TotalDrawTime*SampleSize;
update ProcessedFpsStat SET TotalGpuTime=TotalGpuTime*SampleSize;
update ProcessedFpsStat SET TotalIndexCount=TotalIndexCount*SampleSize;
update ProcessedFpsStat SET TotalGBufferIndexCount=TotalGBufferIndexCount*SampleSize;


------------------------------------------------
-- 2012/12/07 - Mapper service for weapon stat name to ingame weapon name
------------------------------------------------
alter table Weapon add column StatName VARCHAR(255);

------------------------------------------------
-- 2012/12/06 - Mission Death Stats
------------------------------------------------
create table MissionDeathStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, MissionId BIGINT not null, GamerId BIGINT not null, primary key (Id));
alter table MissionDeathStat add index (MissionId), add constraint FK229842D0C144F77D foreign key (MissionId) references Mission (Id);
alter table MissionDeathStat add index (GamerId), add constraint FK229842D03EE6B8B2 foreign key (GamerId) references Gamer (Id);

------------------------------------------------
-- 2012/12/03 - Shape Test Stats
------------------------------------------------
create table ProcessedShapeTestStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, LowerLeft LONGBLOB not null, LowerLeftX FLOAT not null, LowerLeftY FLOAT not null, SampleSize INTEGER UNSIGNED not null, BuildId BIGINT not null, PlatformId BIGINT not null, LevelId BIGINT not null, GameTypeId BIGINT not null, ProcessedStatResolutionId BIGINT not null, Min FLOAT not null, Max FLOAT not null, Mean FLOAT not null, primary key (Id));
alter table ProcessedShapeTestStat add index (BuildId), add constraint FKF6D70E79D949985D foreign key (BuildId) references Build (Id);
alter table ProcessedShapeTestStat add index (PlatformId), add constraint FKF6D70E79B1A9B7F8 foreign key (PlatformId) references Platform (Id);
alter table ProcessedShapeTestStat add index (LevelId), add constraint FKF6D70E797DC7F5BD foreign key (LevelId) references Level (Id);
alter table ProcessedShapeTestStat add index (GameTypeId), add constraint FKF6D70E79ADE1531C foreign key (GameTypeId) references GameType (Id);
alter table ProcessedShapeTestStat add index (ProcessedStatResolutionId), add constraint FKF6D70E79B311B08B foreign key (ProcessedStatResolutionId) references ProcessedStatResolution (Id);
create index LowerLeftXIdx on ProcessedShapeTestStat (LowerLeftX);
create index LowerLeftYIdx on ProcessedShapeTestStat (LowerLeftY);


------------------------------------------------
-- 2012/12/03 - Capture stats location
------------------------------------------------
alter table CaptureCpuResult add column Location LONGBLOB;
alter table CaptureDrawListResult add column Location LONGBLOB;
alter table CaptureFpsResult add column Location LONGBLOB;
alter table CaptureGpuResult add column Location LONGBLOB;
alter table CaptureMemoryResult add column Location LONGBLOB;
alter table CapturePedAndVehicleResult add column Location LONGBLOB;
alter table CaptureScriptMemoryResult add column Location LONGBLOB;
alter table CaptureStreamingMemoryResult add column Location LONGBLOB;
alter table CaptureThreadResult add column Location LONGBLOB;


------------------------------------------------
-- 2012/11/30 - Storing aggregate section stats.
------------------------------------------------
create table StatGroup (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Name VARCHAR(255) not null unique, Value INTEGER, primary key (Id));
create table StatLodLevel (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Name VARCHAR(255) not null unique, Value INTEGER, primary key (Id));
create table StatEntityType (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Name VARCHAR(255) not null unique, Value INTEGER, primary key (Id));
create table MapSectionAggregateStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, MapSectionStatId BIGINT not null, GroupId BIGINT not null, LodLevelId BIGINT not null, EntityTypeId BIGINT not null, IncludesPropGroup TINYINT(1) not null, TotalCount INTEGER UNSIGNED not null, UniqueCount INTEGER UNSIGNED not null, DrawableCost INTEGER UNSIGNED not null, TXDCost INTEGER UNSIGNED not null, TXDCount INTEGER UNSIGNED not null, ShaderCount INTEGER UNSIGNED not null, TextureCount INTEGER UNSIGNED not null, PolygonCount INTEGER UNSIGNED not null, CollisionCount INTEGER UNSIGNED not null, LodDistance FLOAT not null, primary key (Id));
create table MapSectionAggregateCollisionPolyStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ReferencedStatId BIGINT not null, CollisionFlags INTEGER UNSIGNED not null, PrimitiveTypeId BIGINT, PolygonCount INTEGER UNSIGNED not null, primary key (Id));
alter table MapSectionAggregateStat add index (MapSectionStatId), add constraint FK451E07048CEF34D9 foreign key (MapSectionStatId) references MapSectionStat (Id);
alter table MapSectionAggregateStat add index (GroupId), add constraint FK451E070425ED5CEC foreign key (GroupId) references StatGroup (Id);
alter table MapSectionAggregateStat add index (LodLevelId), add constraint FK451E070449DFFC26 foreign key (LodLevelId) references StatLodLevel (Id);
alter table MapSectionAggregateStat add index (EntityTypeId), add constraint FK451E07048B9BA9A8 foreign key (EntityTypeId) references StatEntityType (Id);
alter table MapSectionAggregateCollisionPolyStat add index (ReferencedStatId), add constraint FKA14ACBDA7846E32 foreign key (ReferencedStatId) references MapSectionAggregateStat (Id);
alter table MapSectionAggregateCollisionPolyStat add index (PrimitiveTypeId), add constraint FKA14ACBDA197AB164 foreign key (PrimitiveTypeId) references CollisionPrimitiveType (Id);


------------------------------------------------
-- 2012/11/30 - Changing how vector map points are stored.
------------------------------------------------
alter table MapSectionStat drop column VectorMap;
alter table MapSectionStat add column VectorMapPoints LONGBLOB;


------------------------------------------------
-- 2012/11/28 - Associating telemetry stats with mission attempts.
------------------------------------------------
alter table TelemetryStat add column MissionAttemptStatId BIGINT;
alter table TelemetryStat add index (MissionAttemptStatId), add constraint FK28ABAAFD65DA2DB9 foreign key (MissionAttemptStatId) references MissionAttemptStat (Id);

------------------------------------------------
-- 2012/11/23 - Adding primitive type to collision stats tables
------------------------------------------------
create table CollisionPrimitiveType (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Name VARCHAR(255) not null unique, Value INTEGER, primary key (Id));
alter table RoomCollisionPolyStat add column PrimitiveTypeId BIGINT;
alter table MapSectionCollisionPolyStat add column PrimitiveTypeId BIGINT;
alter table ArchetypeCollisionPolyStat add column PrimitiveTypeId BIGINT;
alter table RoomCollisionPolyStat add index (PrimitiveTypeId), add constraint FK5F6EC85D197AB164 foreign key (PrimitiveTypeId) references CollisionPrimitiveType (Id);
alter table MapSectionCollisionPolyStat add index (PrimitiveTypeId), add constraint FKBE74AB7B197AB164 foreign key (PrimitiveTypeId) references CollisionPrimitiveType (Id);
alter table ArchetypeCollisionPolyStat add index (PrimitiveTypeId), add constraint FK9FE2D8AB197AB164 foreign key (PrimitiveTypeId) references CollisionPrimitiveType (Id);

alter table DrawableArchetypeStat drop column HasDefaultCollisionGroup;
alter table FragmentArchetypeStat drop column HasDefaultCollisionGroup;

alter table EntityStat drop column LodDistance;
alter table CarGenStat add column Name VARCHAR(255) not null;
alter table PortalStat add column Name VARCHAR(255) not null;
alter table SpawnPointStat add column Name VARCHAR(255) not null;
alter table LevelStat add column ExportDataPath VARCHAR(255) not null;
alter table LevelStat add column ProcessedDataPath VARCHAR(255) not null;

------------------------------------------------
-- 2012/11/22 - Adding missing index/making existing one more consistent.
------------------------------------------------
alter table WantedLevelStat drop index Start_Idx;
create index StartIdx on WantedLevelStat (Start);
create index EndIdx on WantedLevelStat (End);


------------------------------------------------
-- 2012/11/21 - Changes to entity stats columns.
------------------------------------------------
ALTER TABLE EntityStat MODIFY BoundingBoxMin LONGBLOB;
ALTER TABLE EntityStat MODIFY BoundingBoxMax LONGBLOB;


------------------------------------------------
-- 2012/11/21 - Cutscene duration
------------------------------------------------
alter table Cutscene add column Duration FLOAT not null;


------------------------------------------------
-- 2012/11/16 - Removal of redundant data from cutscene stats table.
------------------------------------------------
alter table TelemetryCutsceneStat drop column CMetricId;
alter table TelemetryCutsceneStat drop column CGamerId;


------------------------------------------------
-- 2012/11/16 - New cutscene view stat table.
------------------------------------------------
create table CutsceneViewStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, GamerId BIGINT not null, CutsceneId BIGINT not null, SpSessionId BIGINT, MpSessionId BIGINT, Start DATETIME, End DATETIME, Completed TINYINT(1) not null, Skipped TINYINT(1), primary key (Id));
alter table CutsceneViewStat add index (GamerId), add constraint FKE77B7C6F3EE6B8B2 foreign key (GamerId) references Gamer (Id);
alter table CutsceneViewStat add index (CutsceneId), add constraint FKE77B7C6F9837D559 foreign key (CutsceneId) references Cutscene (Id);
alter table CutsceneViewStat add index (SpSessionId), add constraint FKE77B7C6F87772080 foreign key (SpSessionId) references SingleplayerSession (Id);
alter table CutsceneViewStat add index (MpSessionId), add constraint FKE77B7C6F332FA5E3 foreign key (MpSessionId) references MultiplayerSession (Id);
create index StartIdx on CutsceneViewStat (Start);
create index EndIdx on CutsceneViewStat (End);
create index CompletedIdx on CutsceneViewStat (Completed);


------------------------------------------------
-- 2012/11/16 - Adding index to mission attempt stat table.
------------------------------------------------
create index EndIdx on MissionAttemptStat (End);


------------------------------------------------
-- 2012/11/15 - Distance driven stats
------------------------------------------------
create table VehicleDistanceDrivenStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, VehicleId BIGINT not null, GamerId BIGINT not null, PlatformId BIGINT not null, GameTypeId BIGINT not null, Distance FLOAT not null, primary key (Id));
alter table VehicleDistanceDrivenStat add index (VehicleId), add constraint FK933C92D35761A995 foreign key (VehicleId) references Vehicle (Id);
alter table VehicleDistanceDrivenStat add index (GamerId), add constraint FK933C92D33EE6B8B2 foreign key (GamerId) references Gamer (Id);
alter table VehicleDistanceDrivenStat add index (PlatformId), add constraint FK933C92D3B1A9B7F8 foreign key (PlatformId) references Platform (Id);
alter table VehicleDistanceDrivenStat add index (GameTypeId), add constraint FK933C92D3ADE1531C foreign key (GameTypeId) references GameType (Id);


------------------------------------------------
-- 2012/11/13 - Adding mission type
------------------------------------------------
alter table Mission add column GameTypeId BIGINT;
alter table Mission add index (GameTypeId), add constraint FK43DFC6ECADE1531C foreign key (GameTypeId) references GameType (Id);


------------------------------------------------
-- 2012/11/12 - Fixing capture tables
------------------------------------------------
drop table CaptureCpuResult;
drop table CaptureDrawListResult;
drop table CaptureFpsResult;
drop table CaptureGpuResult;
drop table CaptureMemoryResult;
drop table CapturePedAndVehicleResult;
drop table CaptureScriptMemoryResult;
drop table CaptureStreamingMemoryResult;
drop table CaptureThreadResult;

create table CaptureMemoryResult (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, SessionId BIGINT not null, ZoneId BIGINT not null, TelemetryStatId BIGINT, BucketNameId BIGINT not null, MinimumGamePhysical INTEGER UNSIGNED not null, AverageGamePhysical INTEGER UNSIGNED not null, MaximumGamePhysical INTEGER UNSIGNED not null, GamePhysicalStandardDeviation FLOAT not null, MinimumGameVirtual INTEGER UNSIGNED not null, AverageGameVirtual INTEGER UNSIGNED not null, MaximumGameVirtual INTEGER UNSIGNED not null, GameVirtualStandardDeviation FLOAT not null, MinimumResourcePhysical INTEGER UNSIGNED not null, AverageResourcePhysical INTEGER UNSIGNED not null, MaximumResourcePhysical INTEGER UNSIGNED not null, ResourcePhysicalStandardDeviation FLOAT not null, MinimumResourceVirtual INTEGER UNSIGNED not null, AverageResourceVirtual INTEGER UNSIGNED not null, MaximumResourceVirtual INTEGER UNSIGNED not null, ResourceVirtualStandardDeviation FLOAT not null, primary key (Id));
create table CaptureStreamingMemoryResult (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, SessionId BIGINT not null, ZoneId BIGINT not null, TelemetryStatId BIGINT, ModuleId BIGINT not null, CategoryId BIGINT not null, VirtualMemory INTEGER UNSIGNED not null, PhysicalMemory INTEGER UNSIGNED not null, primary key (Id));
create table CaptureGpuResult (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, SessionId BIGINT not null, ZoneId BIGINT not null, TelemetryStatId BIGINT, DrawListId BIGINT not null, Time FLOAT not null, primary key (Id));
create table CaptureScriptMemoryResult (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, SessionId BIGINT not null, ZoneId BIGINT not null, TelemetryStatId BIGINT, MinimumPhysical INTEGER UNSIGNED not null, AveragePhysical INTEGER UNSIGNED not null, MaximumPhysical INTEGER UNSIGNED not null, PhysicalStandardDeviation FLOAT not null, MinimumVirtual INTEGER UNSIGNED not null, AverageVirtual INTEGER UNSIGNED not null, MaximumVirtual INTEGER UNSIGNED not null, VirtualStandardDeviation FLOAT not null, primary key (Id));
create table CaptureThreadResult (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, SessionId BIGINT not null, ZoneId BIGINT not null, TelemetryStatId BIGINT, NameId BIGINT not null, Minimum FLOAT not null, Average FLOAT not null, Maximum FLOAT not null, StandardDeviation FLOAT not null, primary key (Id));
create table CaptureCpuResult (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, SessionId BIGINT not null, ZoneId BIGINT not null, TelemetryStatId BIGINT, SetId BIGINT not null, MetricId BIGINT not null, Minimum FLOAT not null, Average FLOAT not null, Maximum FLOAT not null, StandardDeviation FLOAT not null, primary key (Id));
create table CaptureFpsResult (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, SessionId BIGINT not null, ZoneId BIGINT not null, TelemetryStatId BIGINT, Minimum FLOAT not null, Average FLOAT not null, Maximum FLOAT not null, StandardDeviation FLOAT not null, primary key (Id));
create table CaptureDrawListResult (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, SessionId BIGINT not null, ZoneId BIGINT not null, TelemetryStatId BIGINT, DrawListId BIGINT not null, Minimum INTEGER UNSIGNED not null, Average INTEGER UNSIGNED not null, Maximum INTEGER UNSIGNED not null, StandardDeviation FLOAT not null, primary key (Id));
create table CapturePedAndVehicleResult (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, SessionId BIGINT not null, ZoneId BIGINT not null, TelemetryStatId BIGINT, MinNumPeds INTEGER UNSIGNED not null, AvgNumPeds INTEGER UNSIGNED not null, MaxNumPeds INTEGER UNSIGNED not null, MinActivePeds INTEGER UNSIGNED not null, AvgActivePeds INTEGER UNSIGNED not null, MaxActivePeds INTEGER UNSIGNED not null, MinInactivePeds INTEGER UNSIGNED not null, AvgInactivePeds INTEGER UNSIGNED not null, MaxInactivePeds INTEGER UNSIGNED not null, MinEventScanHiPeds INTEGER UNSIGNED not null, AvgEventScanHiPeds INTEGER UNSIGNED not null, MaxEventScanHiPeds INTEGER UNSIGNED not null, MinEventScanLoPeds INTEGER UNSIGNED not null, AvgEventScanLoPeds INTEGER UNSIGNED not null, MaxEventScanLoPeds INTEGER UNSIGNED not null, MinMotionTaskHiPeds INTEGER UNSIGNED not null, AvgMotionTaskHiPeds INTEGER UNSIGNED not null, MaxMotionTaskHiPeds INTEGER UNSIGNED not null, MinMotionTaskLoPeds INTEGER UNSIGNED not null, AvgMotionTaskLoPeds INTEGER UNSIGNED not null, MaxMotionTaskLoPeds INTEGER UNSIGNED not null, MinPhysicsHiPeds INTEGER UNSIGNED not null, AvgPhysicsHiPeds INTEGER UNSIGNED not null, MaxPhysicsHiPeds INTEGER UNSIGNED not null, MinPhysicsLoPeds INTEGER UNSIGNED not null, AvgPhysicsLoPeds INTEGER UNSIGNED not null, MaxPhysicsLoPeds INTEGER UNSIGNED not null, MinEntityScanHiPeds INTEGER UNSIGNED not null, AvgEntityScanHiPeds INTEGER UNSIGNED not null, MaxEntityScanHiPeds INTEGER UNSIGNED not null, MinEntityScanLoPeds INTEGER UNSIGNED not null, AvgEntityScanLoPeds INTEGER UNSIGNED not null, MaxEntityScanLoPeds INTEGER UNSIGNED not null, MinNumVehicles INTEGER UNSIGNED not null, AvgNumVehicles INTEGER UNSIGNED not null, MaxNumVehicles INTEGER UNSIGNED not null, MinActiveVehicles INTEGER UNSIGNED not null, AvgActiveVehicles INTEGER UNSIGNED not null, MaxActiveVehicles INTEGER UNSIGNED not null, MinInactiveVehicles INTEGER UNSIGNED not null, AvgInactiveVehicles INTEGER UNSIGNED not null, MaxInactiveVehicles INTEGER UNSIGNED not null, MinRealVehicles INTEGER UNSIGNED not null, AvgRealVehicles INTEGER UNSIGNED not null, MaxRealVehicles INTEGER UNSIGNED not null, MinDummyVehicles INTEGER UNSIGNED not null, AvgDummyVehicles INTEGER UNSIGNED not null, MaxDummyVehicles INTEGER UNSIGNED not null, MinSuperDummyVehicles INTEGER UNSIGNED not null, AvgSuperDummyVehicles INTEGER UNSIGNED not null, MaxSuperDummyVehicles INTEGER UNSIGNED not null, MinNetworkDummyVehicles INTEGER UNSIGNED not null, AvgNetworkDummyVehicles INTEGER UNSIGNED not null, MaxNetworkDummyVehicles INTEGER UNSIGNED not null, primary key (Id));
create table CaptureSession (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, BuildId BIGINT not null, PlatformId BIGINT not null, LevelId BIGINT not null, BuildConfigId BIGINT, Timestamp DATETIME not null, SpSessionId BIGINT, MpSessionId BIGINT, AutomatedTestNumber INTEGER UNSIGNED, ChangelistNumber INTEGER UNSIGNED, primary key (Id));
alter table CaptureMemoryResult add index (SessionId), add constraint FK86864082DF629E57 foreign key (SessionId) references CaptureSession (Id);
alter table CaptureMemoryResult add index (ZoneId), add constraint FK8686408266E3E5C7 foreign key (ZoneId) references NameHash (Id);
alter table CaptureMemoryResult add index (TelemetryStatId), add constraint FK8686408251E161B1 foreign key (TelemetryStatId) references TelemetryStat (Id);
alter table CaptureMemoryResult add index (BucketNameId), add constraint FK868640825AF3E294 foreign key (BucketNameId) references NameHash (Id);
alter table CaptureStreamingMemoryResult add index (SessionId), add constraint FK688122F2DF629E57 foreign key (SessionId) references CaptureSession (Id);
alter table CaptureStreamingMemoryResult add index (ZoneId), add constraint FK688122F266E3E5C7 foreign key (ZoneId) references NameHash (Id);
alter table CaptureStreamingMemoryResult add index (TelemetryStatId), add constraint FK688122F251E161B1 foreign key (TelemetryStatId) references TelemetryStat (Id);
alter table CaptureStreamingMemoryResult add index (ModuleId), add constraint FK688122F2EC0AAC29 foreign key (ModuleId) references NameHash (Id);
alter table CaptureStreamingMemoryResult add index (CategoryId), add constraint FK688122F256D6DF51 foreign key (CategoryId) references NameHash (Id);
alter table CaptureGpuResult add index (SessionId), add constraint FK9B071E9FDF629E57 foreign key (SessionId) references CaptureSession (Id);
alter table CaptureGpuResult add index (ZoneId), add constraint FK9B071E9F66E3E5C7 foreign key (ZoneId) references NameHash (Id);
alter table CaptureGpuResult add index (TelemetryStatId), add constraint FK9B071E9F51E161B1 foreign key (TelemetryStatId) references TelemetryStat (Id);
alter table CaptureGpuResult add index (DrawListId), add constraint FK9B071E9FA21E330B foreign key (DrawListId) references DrawList (Id);
alter table CaptureScriptMemoryResult add index (SessionId), add constraint FK2EAD34CBDF629E57 foreign key (SessionId) references CaptureSession (Id);
alter table CaptureScriptMemoryResult add index (ZoneId), add constraint FK2EAD34CB66E3E5C7 foreign key (ZoneId) references NameHash (Id);
alter table CaptureScriptMemoryResult add index (TelemetryStatId), add constraint FK2EAD34CB51E161B1 foreign key (TelemetryStatId) references TelemetryStat (Id);
alter table CaptureThreadResult add index (SessionId), add constraint FK6766A873DF629E57 foreign key (SessionId) references CaptureSession (Id);
alter table CaptureThreadResult add index (ZoneId), add constraint FK6766A87366E3E5C7 foreign key (ZoneId) references NameHash (Id);
alter table CaptureThreadResult add index (TelemetryStatId), add constraint FK6766A87351E161B1 foreign key (TelemetryStatId) references TelemetryStat (Id);
alter table CaptureThreadResult add index (NameId), add constraint FK6766A8731EC32E6F foreign key (NameId) references NameHash (Id);
alter table CaptureCpuResult add index (SessionId), add constraint FKD64510BDF629E57 foreign key (SessionId) references CaptureSession (Id);
alter table CaptureCpuResult add index (ZoneId), add constraint FKD64510B66E3E5C7 foreign key (ZoneId) references NameHash (Id);
alter table CaptureCpuResult add index (TelemetryStatId), add constraint FKD64510B51E161B1 foreign key (TelemetryStatId) references TelemetryStat (Id);
alter table CaptureCpuResult add index (SetId), add constraint FKD64510BF3864355 foreign key (SetId) references NameHash (Id);
alter table CaptureCpuResult add index (MetricId), add constraint FKD64510B9019CB1D foreign key (MetricId) references NameHash (Id);
alter table CaptureFpsResult add index (SessionId), add constraint FKA20E73FADF629E57 foreign key (SessionId) references CaptureSession (Id);
alter table CaptureFpsResult add index (ZoneId), add constraint FKA20E73FA66E3E5C7 foreign key (ZoneId) references NameHash (Id);
alter table CaptureFpsResult add index (TelemetryStatId), add constraint FKA20E73FA51E161B1 foreign key (TelemetryStatId) references TelemetryStat (Id);
alter table CaptureDrawListResult add index (SessionId), add constraint FKB9621E83DF629E57 foreign key (SessionId) references CaptureSession (Id);
alter table CaptureDrawListResult add index (ZoneId), add constraint FKB9621E8366E3E5C7 foreign key (ZoneId) references NameHash (Id);
alter table CaptureDrawListResult add index (TelemetryStatId), add constraint FKB9621E8351E161B1 foreign key (TelemetryStatId) references TelemetryStat (Id);
alter table CaptureDrawListResult add index (DrawListId), add constraint FKB9621E83A21E330B foreign key (DrawListId) references DrawList (Id);
alter table CapturePedAndVehicleResult add index (SessionId), add constraint FK8D7FB6BFDF629E57 foreign key (SessionId) references CaptureSession (Id);
alter table CapturePedAndVehicleResult add index (ZoneId), add constraint FK8D7FB6BF66E3E5C7 foreign key (ZoneId) references NameHash (Id);
alter table CapturePedAndVehicleResult add index (TelemetryStatId), add constraint FK8D7FB6BF51E161B1 foreign key (TelemetryStatId) references TelemetryStat (Id);
alter table CaptureSession add index (BuildId), add constraint FK396ADBD8D949985D foreign key (BuildId) references Build (Id);
alter table CaptureSession add index (PlatformId), add constraint FK396ADBD8B1A9B7F8 foreign key (PlatformId) references Platform (Id);
alter table CaptureSession add index (LevelId), add constraint FK396ADBD87DC7F5BD foreign key (LevelId) references Level (Id);
alter table CaptureSession add index (BuildConfigId), add constraint FK396ADBD8AD58D888 foreign key (BuildConfigId) references BuildConfig (Id);
alter table CaptureSession add index (SpSessionId), add constraint FK396ADBD887772080 foreign key (SpSessionId) references SingleplayerSession (Id);
alter table CaptureSession add index (MpSessionId), add constraint FK396ADBD8332FA5E3 foreign key (MpSessionId) references MultiplayerSession (Id);
create index AutomatedTestNumberIdx on CaptureSession (AutomatedTestNumber);
create index ChangelistNumberIdx on CaptureSession (ChangelistNumber);


------------------------------------------------
-- 2012/11/06 - Radio station stats
------------------------------------------------
create table RadioStation (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255), Identifier VARCHAR(255) not null unique, primary key (Id));
create table TelemetryRadioStationStat (Id BIGINT not null, RadioStationId BIGINT not null, primary key (Id));
create table RadioStationSessionStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, RadioStationId BIGINT not null, GamerId BIGINT not null, SpSessionId BIGINT, MpSessionId BIGINT, Start DATETIME, End DATETIME, Completed TINYINT(1) not null, primary key (Id));
alter table TelemetryRadioStationStat add index (Id), add constraint FKABEC010EF42D6966 foreign key (Id) references TelemetryStat (Id);
alter table TelemetryRadioStationStat add index (RadioStationId), add constraint FKABEC010E1CC4193F foreign key (RadioStationId) references RadioStation (Id);
alter table RadioStationSessionStat add index (RadioStationId), add constraint FKF58E5E331CC4193F foreign key (RadioStationId) references RadioStation (Id);
alter table RadioStationSessionStat add index (GamerId), add constraint FKF58E5E333EE6B8B2 foreign key (GamerId) references Gamer (Id);
alter table RadioStationSessionStat add index (SpSessionId), add constraint FKF58E5E3387772080 foreign key (SpSessionId) references SingleplayerSession (Id);
alter table RadioStationSessionStat add index (MpSessionId), add constraint FKF58E5E33332FA5E3 foreign key (MpSessionId) references MultiplayerSession (Id);
create index StartIdx on RadioStationSessionStat (Start);
create index EndIdx on RadioStationSessionStat (End);
create index CompletedIdx on RadioStationSessionStat (Completed);

-- Making index names more consistent
alter table MissionAttemptStat drop index Start_Idx;
alter table MissionAttemptStat drop index MissionPassed_Idx;
alter table MissionAttemptStat drop index MissionFailed_Idx;
create index StartIdx on MissionAttemptStat (Start);
create index MissionPassedIdx on MissionAttemptStat (MissionPassed);
create index MissionFailedIdx on MissionAttemptStat (MissionFailed);


------------------------------------------------
-- 2012/11/06 - Fix for alt mission identifiers not being uinque in bugstar :/
------------------------------------------------
alter table Mission drop index AltIdentifier;
create index AltIdentifierIdx on Mission (AltIdentifier);


------------------------------------------------
-- 2012/11/02 - MissionVariant for telemetry packets/stats
------------------------------------------------
alter table TelemetryStatPacket ADD MissionVariant INT;
create index MissionVariantIdx on TelemetryStatPacket (MissionVariant);

alter table TelemetryStat ADD MissionVariant INT;
create index MissionVariantIdx on TelemetryStat (MissionVariant);


------------------------------------------------
-- 2012/10/31 - Map export stat additions/changes
------------------------------------------------
alter table MapExportStat ADD UsingAP3 TINYINT(1);

rename table MapSectionExportStat to MapExportSectionExportStat;
alter table MapExportSectionExportStat change ExportStart Start DATETIME not null;
alter table MapExportSectionExportStat change ExportEnd End DATETIME;
alter table MapExportSectionExportStat drop index ExportStartIdx;
alter table MapExportSectionExportStat drop index ExportEndIdx;
create index StartIdx on MapExportSectionExportStat (Start);
create index EndIdx on MapExportSectionExportStat (End);

rename table MapImageBuildStat to MapExportImageBuildStat;
alter table MapExportImageBuildStat change BuildStart Start DATETIME not null;
alter table MapExportImageBuildStat change BuildEnd End DATETIME;
alter table MapExportImageBuildStat drop index BuildStartIdx;
alter table MapExportImageBuildStat drop index BuildEndIdx;
create index StartIdx on MapExportImageBuildStat (Start);
create index EndIdx on MapExportImageBuildStat (End);

rename table MapImageBuildStat_MapSection to MapExportImageBuildStat_MapSection;
alter table MapExportImageBuildStat_MapSection drop foreign key FK564600755BC9CF37;
alter table MapExportImageBuildStat_MapSection drop index MapImageBuildStatId;
alter table MapExportImageBuildStat_MapSection change column MapImageBuildStatId MapExportImageBuildStatId BIGINT not null;
create index MapExportImageBuildStatId on MapExportImageBuildStat_MapSection (MapExportImageBuildStatId);
alter table MapExportImageBuildStat_MapSection add constraint FK564600755BC9CF37 foreign key (MapExportImageBuildStatId) references mapexportimagebuildstat(Id);

create table SectionExportSubTask (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Name VARCHAR(255) not null unique, Value INTEGER, primary key (Id));

create table MapExportMapCheckStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, MapExportStatId BIGINT not null unique, Start DATETIME not null, End DATETIME, Success TINYINT(1), primary key (Id));
alter table MapExportMapCheckStat add index (MapExportStatId), add constraint FKBCC3173A8E9AC6F5 foreign key (MapExportStatId) references MapExportStat (Id);
create index StartIdx on MapExportMapCheckStat (Start);
create index EndIdx on MapExportMapCheckStat (End);

create table MapExportSectionExportSubStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, SectionExportStatId BIGINT not null, Start DATETIME not null, End DATETIME, Success TINYINT(1), ExportSubTaskId BIGINT not null, primary key (Id));
alter table MapExportSectionExportSubStat add index (SectionExportStatId), add constraint FKF03F24A31EA053A5 foreign key (SectionExportStatId) references MapExportSectionExportStat (Id);
alter table MapExportSectionExportSubStat add index (ExportSubTaskId), add constraint FKF03F24A3E762212B foreign key (ExportSubTaskId) references SectionExportSubTask (Id);
create index StartIdx on MapExportSectionExportSubStat (Start);
create index EndIdx on MapExportSectionExportSubStat (End);


------------------------------------------------
-- 2012/10/30 - Map archetype stats
------------------------------------------------
create table RoomCollisionPolyStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ReferencedStatId BIGINT not null, CollisionFlags INTEGER UNSIGNED not null, PolygonCount INTEGER UNSIGNED not null, primary key (Id));
create table MapSectionAttributeStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, MapSectionStatId BIGINT not null, Name VARCHAR(255) not null, Type VARCHAR(255) not null, Value VARCHAR(255) not null, primary key (Id));
create table DrawableLodStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ArchetypeStatId BIGINT not null, Name VARCHAR(255) not null, LodDistance FLOAT not null, PolygonCount INTEGER UNSIGNED not null, LodLevelId BIGINT not null, primary key (Id));
create table ArchetypeStat (Id BIGINT not null, ArchetypeId BIGINT not null, MapSectionStatId BIGINT not null, BoundingBoxMin LONGBLOB not null, BoundingBoxMax LONGBLOB not null, LodDistance FLOAT not null, ExportGeometrySize INTEGER UNSIGNED not null, PolygonCount INTEGER UNSIGNED not null, CollisionPolygonCount INTEGER UNSIGNED not null, HasAttachedLight TINYINT(1) not null, HasExplosiveEffect TINYINT(1) not null, primary key (Id));
create table StatedAnimArchetypeStat_ArchetypeStat (ArchetypeStatId BIGINT not null, StatedAnimArchetypeStatId BIGINT not null, primary key (StatedAnimArchetypeStatId, ArchetypeStatId));
create table DrawableArchetypeStat (Id BIGINT not null, IsDynamic TINYINT(1) not null, CollisionGroup VARCHAR(255) not null, HasDefaultCollisionGroup TINYINT(1) not null, ForceBakeCollision TINYINT(1) not null, NeverBakeCollision TINYINT(1) not null, primary key (Id));
create table FragmentArchetypeStat (Id BIGINT not null, IsDynamic TINYINT(1) not null, CollisionGroup VARCHAR(255) not null, HasDefaultCollisionGroup TINYINT(1) not null, ForceBakeCollision TINYINT(1) not null, NeverBakeCollision TINYINT(1) not null, FragmentCount INTEGER UNSIGNED not null, IsCloth TINYINT(1) not null, primary key (Id));
create table InteriorArchetypeStat (Id BIGINT not null, primary key (Id));
create table StatedAnimArchetypeStat (Id BIGINT not null, primary key (Id));
create table RoomStat (Id BIGINT not null, RoomId BIGINT not null, InteriorStatId BIGINT not null, BoundingBoxMin LONGBLOB not null, BoundingBoxMax LONGBLOB not null, PolygonCount INTEGER UNSIGNED not null, CollisionPolygonCount INTEGER UNSIGNED not null, primary key (Id));
create table Room (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255), Identifier VARCHAR(255) not null unique, primary key (Id));
create table EntityStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, BuildId BIGINT not null, ArchetypeStatId BIGINT not null, EntityId BIGINT not null, MapSectionStatId BIGINT, InteriorStatId BIGINT, RoomStatId BIGINT, Location LONGBLOB not null, BoundingBoxMin LONGBLOB not null, BoundingBoxMax LONGBLOB not null, PriorityLevel INTEGER not null, IsLowPriority TINYINT(1) not null, IsReference TINYINT(1) not null, IsInteriorReference TINYINT(1) not null, LodLevelId BIGINT not null, LodDistance FLOAT not null, LodDistanceOverride FLOAT, LodParentId BIGINT, ForceBakeCollision TINYINT(1) not null, primary key (Id));
create table LodLevel (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Name VARCHAR(255) not null unique, Value INTEGER, primary key (Id));
create table MapDrawableLodLevel (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Name VARCHAR(255) not null unique, Value INTEGER, primary key (Id));
create table SpawnPointAvailableMode (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, Name VARCHAR(255) not null unique, Value INTEGER, primary key (Id));
create table Archetype (Id BIGINT NOT NULL AUTO_INCREMENT, SubType CHAR(1) not null, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255), Identifier VARCHAR(255) not null, primary key (Id));
create table SpawnPointStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, LevelStatId BIGINT, ArchetypeStatId BIGINT, Position LONGBLOB, SourceFile VARCHAR(255), SpawnType VARCHAR(255), SpawnGroup VARCHAR(255), ModelSet VARCHAR(255), AvailabilityMode BIGINT, StartTime INTEGER, EndTime INTEGER, primary key (Id));
create table ArchetypeTxdExportSizeStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ReferencedStatId BIGINT not null, TxdName VARCHAR(255) not null, Size INTEGER UNSIGNED not null, primary key (Id));
create table PortalStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, InteriorStatId BIGINT not null, RoomStatAId BIGINT, RoomStatBId BIGINT, primary key (Id));
create table ArchetypeCollisionPolyStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ReferencedStatId BIGINT not null, CollisionFlags INTEGER UNSIGNED not null, PolygonCount INTEGER UNSIGNED not null, primary key (Id));
create table Entity (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ModifiedOn DATETIME not null, Name VARCHAR(255), Identifier VARCHAR(255) not null unique, primary key (Id));
create table RoomTxdExportSizeStat (Id BIGINT NOT NULL AUTO_INCREMENT, CreatedOn DATETIME not null, ReferencedStatId BIGINT not null, TxdName VARCHAR(255) not null, Size INTEGER UNSIGNED not null, primary key (Id));
alter table RoomCollisionPolyStat add index (ReferencedStatId), add constraint FK5F6EC85D1D61B1CD foreign key (ReferencedStatId) references RoomStat (Id);
alter table MapSectionAttributeStat add index (MapSectionStatId), add constraint FKEDB8241F8CEF34D9 foreign key (MapSectionStatId) references MapSectionStat (Id);
alter table DrawableLodStat add index (ArchetypeStatId), add constraint FKBA2C7277C3DC86F foreign key (ArchetypeStatId) references ArchetypeStat (Id);
alter table DrawableLodStat add index (LodLevelId), add constraint FKBA2C7272203AFAE foreign key (LodLevelId) references MapDrawableLodLevel (Id);
alter table ArchetypeStat add index (Id), add constraint FK25F9B30B8782F908 foreign key (Id) references GameAssetStatBase (Id);
alter table ArchetypeStat add index (ArchetypeId), add constraint FK25F9B30BC5D5C92D foreign key (ArchetypeId) references Archetype (Id);
alter table ArchetypeStat add index (MapSectionStatId), add constraint FK25F9B30B8CEF34D9 foreign key (MapSectionStatId) references MapSectionStat (Id);
alter table StatedAnimArchetypeStat_ArchetypeStat add index (StatedAnimArchetypeStatId), add constraint FK2EE965AB7C3244AF foreign key (StatedAnimArchetypeStatId) references StatedAnimArchetypeStat (Id);
alter table StatedAnimArchetypeStat_ArchetypeStat add index (ArchetypeStatId), add constraint FK2EE965AB7C3DC86F foreign key (ArchetypeStatId) references ArchetypeStat (Id);
alter table DrawableArchetypeStat add index (Id), add constraint FKAB540F0DBCD63684 foreign key (Id) references ArchetypeStat (Id);
alter table FragmentArchetypeStat add index (Id), add constraint FKF1590C7BBCD63684 foreign key (Id) references ArchetypeStat (Id);
alter table InteriorArchetypeStat add index (Id), add constraint FKBAE3C27BBCD63684 foreign key (Id) references ArchetypeStat (Id);
alter table StatedAnimArchetypeStat add index (Id), add constraint FKF0C14DC7BCD63684 foreign key (Id) references ArchetypeStat (Id);
alter table RoomStat add index (Id), add constraint FKE1787B4D8782F908 foreign key (Id) references GameAssetStatBase (Id);
alter table RoomStat add index (RoomId), add constraint FKE1787B4D4CB12FCF foreign key (RoomId) references Room (Id);
alter table RoomStat add index (InteriorStatId), add constraint FKE1787B4DB1E6A496 foreign key (InteriorStatId) references InteriorArchetypeStat (Id);
alter table EntityStat add index (BuildId), add constraint FKDA554179D949985D foreign key (BuildId) references Build (Id);
alter table EntityStat add index (ArchetypeStatId), add constraint FKDA5541797C3DC86F foreign key (ArchetypeStatId) references ArchetypeStat (Id);
alter table EntityStat add index (EntityId), add constraint FKDA55417945954A0F foreign key (EntityId) references Entity (Id);
alter table EntityStat add index (MapSectionStatId), add constraint FKDA5541798CEF34D9 foreign key (MapSectionStatId) references MapSectionStat (Id);
alter table EntityStat add index (InteriorStatId), add constraint FKDA554179B1E6A496 foreign key (InteriorStatId) references InteriorArchetypeStat (Id);
alter table EntityStat add index (RoomStatId), add constraint FKDA554179C5E9741D foreign key (RoomStatId) references RoomStat (Id);
alter table EntityStat add index (LodLevelId), add constraint FKDA55417994B37E20 foreign key (LodLevelId) references LodLevel (Id);
alter table EntityStat add index (LodParentId), add constraint FKDA554179FBFFF7AF foreign key (LodParentId) references EntityStat (Id);
create index identifier_idx on Archetype (Identifier);
alter table SpawnPointStat add index (LevelStatId), add constraint FK10DC4D75FE7B5F9B foreign key (LevelStatId) references LevelStat (Id);
alter table SpawnPointStat add index (ArchetypeStatId), add constraint FK10DC4D757C3DC86F foreign key (ArchetypeStatId) references ArchetypeStat (Id);
alter table SpawnPointStat add index (AvailabilityMode), add constraint FK10DC4D7528D7A92D foreign key (AvailabilityMode) references SpawnPointAvailableMode (Id);
alter table ArchetypeTxdExportSizeStat add index (ReferencedStatId), add constraint FK730B4B8E98B00DB7 foreign key (ReferencedStatId) references ArchetypeStat (Id);
alter table PortalStat add index (InteriorStatId), add constraint FKB638438EB1E6A496 foreign key (InteriorStatId) references InteriorArchetypeStat (Id);
alter table PortalStat add index (RoomStatAId), add constraint FKB638438E4BFD1EFA foreign key (RoomStatAId) references RoomStat (Id);
alter table PortalStat add index (RoomStatBId), add constraint FKB638438E4BFD1E5D foreign key (RoomStatBId) references RoomStat (Id);
alter table ArchetypeCollisionPolyStat add index (ReferencedStatId), add constraint FK9FE2D8AB98B00DB7 foreign key (ReferencedStatId) references ArchetypeStat (Id);
alter table RoomTxdExportSizeStat add index (ReferencedStatId), add constraint FKFCA6E1161D61B1CD foreign key (ReferencedStatId) references RoomStat (Id);


------------------------------------------------
-- 2012/10/17 - Memory shortfall stats.
------------------------------------------------
create table TelemetryCoordMemShortfallStat (
    Id BIGINT not null,
    PhysicalShortfall INTEGER UNSIGNED not null,
    VirtualShortfall INTEGER UNSIGNED not null,
    primary key (Id)
);
alter table TelemetryCoordMemShortfallStat 
    add index (Id), 
    add constraint FKC6FB496453E2A29 
    foreign key (Id) 
    references TelemetryCoordStat (Id);
create table ProcessedMemShortfallStat (
	Id BIGINT NOT NULL AUTO_INCREMENT,
	CreatedOn DATETIME not null,
	LowerLeft LONGBLOB not null,
	LowerLeftX FLOAT not null,
	LowerLeftY FLOAT not null,
	SampleSize INTEGER UNSIGNED not null,
	BuildId BIGINT not null,
	PlatformId BIGINT not null,
	LevelId BIGINT not null,
	GameTypeId BIGINT not null,
	ProcessedStatResolutionId BIGINT not null,
	MeanPhysicalShortfall DOUBLE not null,
	MinPhysicalShortfall DOUBLE not null,
	MaxPhysicalShortfall DOUBLE not null,
	MeanVirtualShortfall DOUBLE not null,
	MinVirtualShortfall DOUBLE not null,
	MaxVirtualShortfall DOUBLE not null,
	primary key (Id)
);
alter table ProcessedMemShortfallStat 
	add index (BuildId), 
	add constraint FK408FE4C8D949985D 
	foreign key (BuildId) 
	references Build (Id);
alter table ProcessedMemShortfallStat 
	add index (PlatformId), 
	add constraint FK408FE4C8B1A9B7F8 
	foreign key (PlatformId) 
	references Platform (Id);
alter table ProcessedMemShortfallStat 
	add index (LevelId), 
	add constraint FK408FE4C87DC7F5BD 
	foreign key (LevelId) 
	references Level (Id);
alter table ProcessedMemShortfallStat 
	add index (GameTypeId), 
	add constraint FK408FE4C8ADE1531C 
	foreign key (GameTypeId) 
	references GameType (Id);
alter table ProcessedMemShortfallStat 
	add index (ProcessedStatResolutionId), 
	add constraint FK408FE4C8B311B08B 
	foreign key (ProcessedStatResolutionId) 
	references ProcessedStatResolution (Id);
create index lower_left_x_idx on ProcessedMemShortfallStat (LowerLeftX);
create index lower_left_y_idx on ProcessedMemShortfallStat (LowerLeftY);


------------------------------------------------
-- 2012/10/17 - Additional map area/section stats
--				including per platform stats.
------------------------------------------------
alter table MapAreaStat 
	add column ExportDataPath VARCHAR(255);
alter table MapAreaStat 
	add column ProcessedDataPath VARCHAR(255);
alter table MapSectionStat 
	add column ExportArchetypes TINYINT(1);
alter table MapSectionStat 
	add column ExportEntities TINYINT(1);
alter table MapSectionStat 
	add column CollisionPolygonCount INTEGER UNSIGNED;
alter table MapSectionStat 
	add column LastExportUser VARCHAR(255);
alter table MapSectionStat 
	add column LastExportTime DATETIME;
alter table MapSectionStat 
	add column DCCSourceFilename VARCHAR(255);
alter table MapSectionStat 
	add column ExportZipFilepath VARCHAR(255);
alter table MapSection
	drop column ExportedOn;
alter table MapSection
	drop column LastExportDuration;
alter table MapSection
	drop column CheckedInOn;
alter table MapSection
	drop column CheckedInBy;
alter table MapSection
	drop column CheckInChangelist;
create table FileType (
	Id BIGINT NOT NULL AUTO_INCREMENT,
   CreatedOn DATETIME not null,
   Name VARCHAR(255) not null unique,
   Value INTEGER,
   primary key (Id)
);
create table CarGenStat (
	Id BIGINT NOT NULL AUTO_INCREMENT,
   CreatedOn DATETIME not null,
   MapSectionStatId BIGINT not null,
   Position LONGBLOB not null,
   IsPolice TINYINT(1) not null,
   IsAmbulance TINYINT(1) not null,
   IsSpecificModel TINYINT(1) not null,
   ModelName VARCHAR(255),
   IsHighPriority TINYINT(1) not null,
   primary key (Id)
);
create table MapSectionCollisionPolyStat (
	Id BIGINT NOT NULL AUTO_INCREMENT,
   CreatedOn DATETIME not null,
   ReferencedStatId BIGINT not null,
   CollisionFlags INTEGER UNSIGNED not null,
   PolygonCount INTEGER UNSIGNED not null,
   primary key (Id)
);
create table MapSectionPlatformStat (
	Id BIGINT NOT NULL AUTO_INCREMENT,
   CreatedOn DATETIME not null,
   PlatformId BIGINT not null,
   FileTypeId BIGINT not null,
   MapSectionStatId BIGINT not null,
   PhysicalSize INTEGER UNSIGNED not null,
   VirtualSize INTEGER UNSIGNED not null,
   primary key (Id)
);
alter table CarGenStat 
	add index (MapSectionStatId), 
	add constraint FKC9D0EBC48CEF34D9 
	foreign key (MapSectionStatId) 
	references MapSectionStat (Id);
alter table MapSectionCollisionPolyStat 
	add index (ReferencedStatId), 
	add constraint FKBE74AB7B9C5EAAEB 
	foreign key (ReferencedStatId) 
	references MapSectionStat (Id);
alter table MapSectionPlatformStat 
	add index (PlatformId), 
	add constraint FK983621EB1A9B7F8 
	foreign key (PlatformId) 
	references Platform (Id);
alter table MapSectionPlatformStat 
	add index (FileTypeId), 
	add constraint FK983621E471283B6 
	foreign key (FileTypeId) 
	references FileType (Id);
alter table MapSectionPlatformStat 
	add index (MapSectionStatId), 
	add constraint FK983621E8CEF34D9 
	foreign key (MapSectionStatId) 
	references MapSectionStat (Id);


------------------------------------------------
-- 2012/10/11 - Tools version in export stats
------------------------------------------------
alter table MapExportStat ADD ToolsVersion INTEGER;


------------------------------------------------
-- 2012/10/09 - Cutscene stat table flattening
------------------------------------------------
alter table TelemetryCutsceneStat ADD CMetricId BIGINT not null;
alter table TelemetryCutsceneStat ADD CGamerId BIGINT not null;
UPDATE TelemetryCutsceneStat as c
	INNER JOIN TelemetryStat b ON c.Id=b.Id
	SET c.CMetricId=b.MetricId, c.CGamerId=b.GamerId;


------------------------------------------------
-- 2012/10/05 - Capture related stats tables
------------------------------------------------
create table CaptureMemoryResult (
	Id BIGINT NOT NULL AUTO_INCREMENT,
   CreatedOn DATETIME not null,
   BuildId BIGINT not null,
   Timestamp DATETIME not null,
   PlatformId BIGINT not null,
   AutomatedTestNumber INTEGER UNSIGNED,
   TelemetryStatId BIGINT,
   ZoneId BIGINT not null,
   CaptureIndex INTEGER not null,
   BucketNameId BIGINT not null,
   MinimumGamePhysical INTEGER UNSIGNED not null,
   AverageGamePhysical INTEGER UNSIGNED not null,
   MaximumGamePhysical INTEGER UNSIGNED not null,
   GamePhysicalStandardDeviation FLOAT not null,
   MinimumGameVirtual INTEGER UNSIGNED not null,
   AverageGameVirtual INTEGER UNSIGNED not null,
   MaximumGameVirtual INTEGER UNSIGNED not null,
   GameVirtualStandardDeviation FLOAT not null,
   MinimumResourcePhysical INTEGER UNSIGNED not null,
   AverageResourcePhysical INTEGER UNSIGNED not null,
   MaximumResourcePhysical INTEGER UNSIGNED not null,
   ResourcePhysicalStandardDeviation FLOAT not null,
   MinimumResourceVirtual INTEGER UNSIGNED not null,
   AverageResourceVirtual INTEGER UNSIGNED not null,
   MaximumResourceVirtual INTEGER UNSIGNED not null,
   ResourceVirtualStandardDeviation FLOAT not null,
   primary key (Id)
);
create table CaptureStreamingMemoryResult (
	Id BIGINT NOT NULL AUTO_INCREMENT,
   CreatedOn DATETIME not null,
   BuildId BIGINT not null,
   Timestamp DATETIME not null,
   PlatformId BIGINT not null,
   AutomatedTestNumber INTEGER UNSIGNED,
   TelemetryStatId BIGINT,
   ZoneId BIGINT not null,
   CaptureIndex INTEGER not null,
   ModuleId BIGINT not null,
   CategoryId BIGINT not null,
   VirtualMemory INTEGER not null,
   PhysicalMemory INTEGER not null,
   primary key (Id)
);
create table CaptureGpuResult (
	Id BIGINT NOT NULL AUTO_INCREMENT,
   CreatedOn DATETIME not null,
   BuildId BIGINT not null,
   Timestamp DATETIME not null,
   PlatformId BIGINT not null,
   AutomatedTestNumber INTEGER UNSIGNED,
   TelemetryStatId BIGINT,
   ZoneId BIGINT not null,
   CaptureIndex INTEGER not null,
   DrawListId BIGINT not null,
   Time FLOAT not null,
   primary key (Id)
);
create table CaptureScriptMemoryResult (
	Id BIGINT NOT NULL AUTO_INCREMENT,
   CreatedOn DATETIME not null,
   BuildId BIGINT not null,
   Timestamp DATETIME not null,
   PlatformId BIGINT not null,
   AutomatedTestNumber INTEGER UNSIGNED,
   TelemetryStatId BIGINT,
   ZoneId BIGINT not null,
   CaptureIndex INTEGER not null,
   MinimumPhysical INTEGER UNSIGNED not null,
   AveragePhysical INTEGER UNSIGNED not null,
   MaximumPhysical INTEGER UNSIGNED not null,
   PhysicalStandardDeviation FLOAT not null,
   MinimumVirtual INTEGER UNSIGNED not null,
   AverageVirtual INTEGER UNSIGNED not null,
   MaximumVirtual INTEGER UNSIGNED not null,
   VirtualStandardDeviation FLOAT not null,
   primary key (Id)
);
create table NameHash (
	Id BIGINT NOT NULL AUTO_INCREMENT,
   SubType INTEGER not null,
   CreatedOn DATETIME not null,
   Name VARCHAR(255) not null,
   Hash INTEGER UNSIGNED not null,
   primary key (Id)
);
create table CaptureCpuResult (
	Id BIGINT NOT NULL AUTO_INCREMENT,
   CreatedOn DATETIME not null,
   BuildId BIGINT not null,
   Timestamp DATETIME not null,
   PlatformId BIGINT not null,
   AutomatedTestNumber INTEGER UNSIGNED,
   TelemetryStatId BIGINT,
   ZoneId BIGINT not null,
   CaptureIndex INTEGER not null,
   SetId BIGINT not null,
   MetricId BIGINT not null,
   Minimum FLOAT not null,
   Average FLOAT not null,
   Maximum FLOAT not null,
   StandardDeviation FLOAT not null,
   primary key (Id)
);
create table CaptureFpsResult (
	Id BIGINT NOT NULL AUTO_INCREMENT,
   CreatedOn DATETIME not null,
   BuildId BIGINT not null,
   Timestamp DATETIME not null,
   PlatformId BIGINT not null,
   AutomatedTestNumber INTEGER UNSIGNED,
   TelemetryStatId BIGINT,
   ZoneId BIGINT not null,
   CaptureIndex INTEGER not null,
   Minimum FLOAT not null,
   Average FLOAT not null,
   Maximum FLOAT not null,
   StandardDeviation FLOAT not null,
   primary key (Id)
);
create table CaptureDrawListResult (
	Id BIGINT NOT NULL AUTO_INCREMENT,
   CreatedOn DATETIME not null,
   BuildId BIGINT not null,
   Timestamp DATETIME not null,
   PlatformId BIGINT not null,
   AutomatedTestNumber INTEGER UNSIGNED,
   TelemetryStatId BIGINT,
   ZoneId BIGINT not null,
   CaptureIndex INTEGER not null,
   DrawListId BIGINT not null,
   Minimum INTEGER UNSIGNED not null,
   Average INTEGER UNSIGNED not null,
   Maximum INTEGER UNSIGNED not null,
   StandardDeviation FLOAT not null,
   primary key (Id)
);
create table CaptureThreadResult (
	Id BIGINT NOT NULL AUTO_INCREMENT,
   CreatedOn DATETIME not null,
   BuildId BIGINT not null,
   Timestamp DATETIME not null,
   PlatformId BIGINT not null,
   AutomatedTestNumber INTEGER UNSIGNED,
   TelemetryStatId BIGINT,
   ZoneId BIGINT not null,
   CaptureIndex INTEGER not null,
   NameId BIGINT not null,
   Minimum FLOAT not null,
   Average FLOAT not null,
   Maximum FLOAT not null,
   StandardDeviation FLOAT not null,
   primary key (Id)
);
create table CapturePedAndVehicleResult (
	Id BIGINT NOT NULL AUTO_INCREMENT,
   CreatedOn DATETIME not null,
   BuildId BIGINT not null,
   Timestamp DATETIME not null,
   PlatformId BIGINT not null,
   AutomatedTestNumber INTEGER UNSIGNED,
   TelemetryStatId BIGINT,
   ZoneId BIGINT not null,
   CaptureIndex INTEGER not null,
   MinNumPeds INTEGER UNSIGNED not null,
   AvgNumPeds INTEGER UNSIGNED not null,
   MaxNumPeds INTEGER UNSIGNED not null,
   MinActivePeds INTEGER UNSIGNED not null,
   AvgActivePeds INTEGER UNSIGNED not null,
   MaxActivePeds INTEGER UNSIGNED not null,
   MinInactivePeds INTEGER UNSIGNED not null,
   AvgInactivePeds INTEGER UNSIGNED not null,
   MaxInactivePeds INTEGER UNSIGNED not null,
   MinEventScanHiPeds INTEGER UNSIGNED not null,
   AvgEventScanHiPeds INTEGER UNSIGNED not null,
   MaxEventScanHiPeds INTEGER UNSIGNED not null,
   MinEventScanLoPeds INTEGER UNSIGNED not null,
   AvgEventScanLoPeds INTEGER UNSIGNED not null,
   MaxEventScanLoPeds INTEGER UNSIGNED not null,
   MinMotionTaskHiPeds INTEGER UNSIGNED not null,
   AvgMotionTaskHiPeds INTEGER UNSIGNED not null,
   MaxMotionTaskHiPeds INTEGER UNSIGNED not null,
   MinMotionTaskLoPeds INTEGER UNSIGNED not null,
   AvgMotionTaskLoPeds INTEGER UNSIGNED not null,
   MaxMotionTaskLoPeds INTEGER UNSIGNED not null,
   MinPhysicsHiPeds INTEGER UNSIGNED not null,
   AvgPhysicsHiPeds INTEGER UNSIGNED not null,
   MaxPhysicsHiPeds INTEGER UNSIGNED not null,
   MinPhysicsLoPeds INTEGER UNSIGNED not null,
   AvgPhysicsLoPeds INTEGER UNSIGNED not null,
   MaxPhysicsLoPeds INTEGER UNSIGNED not null,
   MinEntityScanHiPeds INTEGER UNSIGNED not null,
   AvgEntityScanHiPeds INTEGER UNSIGNED not null,
   MaxEntityScanHiPeds INTEGER UNSIGNED not null,
   MinEntityScanLoPeds INTEGER UNSIGNED not null,
   AvgEntityScanLoPeds INTEGER UNSIGNED not null,
   MaxEntityScanLoPeds INTEGER UNSIGNED not null,
   MinNumVehicles INTEGER UNSIGNED not null,
   AvgNumVehicles INTEGER UNSIGNED not null,
   MaxNumVehicles INTEGER UNSIGNED not null,
   MinActiveVehicles INTEGER UNSIGNED not null,
   AvgActiveVehicles INTEGER UNSIGNED not null,
   MaxActiveVehicles INTEGER UNSIGNED not null,
   MinInactiveVehicles INTEGER UNSIGNED not null,
   AvgInactiveVehicles INTEGER UNSIGNED not null,
   MaxInactiveVehicles INTEGER UNSIGNED not null,
   MinRealVehicles INTEGER UNSIGNED not null,
   AvgRealVehicles INTEGER UNSIGNED not null,
   MaxRealVehicles INTEGER UNSIGNED not null,
   MinDummyVehicles INTEGER UNSIGNED not null,
   AvgDummyVehicles INTEGER UNSIGNED not null,
   MaxDummyVehicles INTEGER UNSIGNED not null,
   MinSuperDummyVehicles INTEGER UNSIGNED not null,
   AvgSuperDummyVehicles INTEGER UNSIGNED not null,
   MaxSuperDummyVehicles INTEGER UNSIGNED not null,
   MinNetDummyVehicles INTEGER UNSIGNED not null,
   AvgNetDummyVehicles INTEGER UNSIGNED not null,
   MaxNetDummyVehicles INTEGER UNSIGNED not null,
   primary key (Id)
);
alter table CaptureMemoryResult 
	add index (BuildId), 
	add constraint FK86864082D949985D 
	foreign key (BuildId) 
	references Build (Id);
alter table CaptureMemoryResult 
	add index (PlatformId), 
	add constraint FK86864082B1A9B7F8 
	foreign key (PlatformId) 
	references Platform (Id);
alter table CaptureMemoryResult 
	add index (TelemetryStatId), 
	add constraint FK8686408251E161B1 
	foreign key (TelemetryStatId) 
	references TelemetryStat (Id);
alter table CaptureMemoryResult 
	add index (ZoneId), 
	add constraint FK8686408266E3E5C7 
	foreign key (ZoneId) 
	references NameHash (Id);
alter table CaptureMemoryResult 
	add index (BucketNameId), 
	add constraint FK868640825AF3E294 
	foreign key (BucketNameId) 
	references NameHash (Id);
create index automated_test_number_idx on CaptureMemoryResult (AutomatedTestNumber);
alter table CaptureStreamingMemoryResult 
	add index (BuildId), 
	add constraint FK688122F2D949985D 
	foreign key (BuildId) 
	references Build (Id);
alter table CaptureStreamingMemoryResult 
	add index (PlatformId), 
	add constraint FK688122F2B1A9B7F8 
	foreign key (PlatformId) 
	references Platform (Id);
alter table CaptureStreamingMemoryResult 
	add index (TelemetryStatId), 
	add constraint FK688122F251E161B1 
	foreign key (TelemetryStatId) 
	references TelemetryStat (Id);
alter table CaptureStreamingMemoryResult 
	add index (ZoneId), 
	add constraint FK688122F266E3E5C7 
	foreign key (ZoneId) 
	references NameHash (Id);
alter table CaptureStreamingMemoryResult 
	add index (ModuleId), 
	add constraint FK688122F2EC0AAC29 
	foreign key (ModuleId) 
	references NameHash (Id);
alter table CaptureStreamingMemoryResult 
	add index (CategoryId), 
	add constraint FK688122F256D6DF51 
	foreign key (CategoryId) 
	references NameHash (Id);
create index automated_test_number_idx on CaptureStreamingMemoryResult (AutomatedTestNumber);
alter table CaptureGpuResult 
	add index (BuildId), 
	add constraint FK9B071E9FD949985D 
	foreign key (BuildId) 
	references Build (Id);
alter table CaptureGpuResult 
	add index (PlatformId), 
	add constraint FK9B071E9FB1A9B7F8 
	foreign key (PlatformId) 
	references Platform (Id);
alter table CaptureGpuResult 
	add index (TelemetryStatId), 
	add constraint FK9B071E9F51E161B1 
	foreign key (TelemetryStatId) 
	references TelemetryStat (Id);
alter table CaptureGpuResult 
	add index (ZoneId), 
	add constraint FK9B071E9F66E3E5C7 
	foreign key (ZoneId) 
	references NameHash (Id);
alter table CaptureGpuResult 
	add index (DrawListId), 
	add constraint FK9B071E9FA21E330B 
	foreign key (DrawListId) 
	references DrawList (Id);
create index automated_test_number_idx on CaptureGpuResult (AutomatedTestNumber);
alter table CaptureScriptMemoryResult 
	add index (BuildId), 
	add constraint FK2EAD34CBD949985D 
	foreign key (BuildId) 
	references Build (Id);
alter table CaptureScriptMemoryResult 
	add index (PlatformId), 
	add constraint FK2EAD34CBB1A9B7F8 
	foreign key (PlatformId) 
	references Platform (Id);
alter table CaptureScriptMemoryResult 
	add index (TelemetryStatId), 
	add constraint FK2EAD34CB51E161B1 
	foreign key (TelemetryStatId) 
	references TelemetryStat (Id);
alter table CaptureScriptMemoryResult 
	add index (ZoneId), 
	add constraint FK2EAD34CB66E3E5C7 
	foreign key (ZoneId) 
	references NameHash (Id);
create index automated_test_number_idx on CaptureScriptMemoryResult (AutomatedTestNumber);
alter table CaptureCpuResult 
	add index (BuildId), 
	add constraint FKD64510BD949985D 
	foreign key (BuildId) 
	references Build (Id);
alter table CaptureCpuResult 
	add index (PlatformId), 
	add constraint FKD64510BB1A9B7F8 
	foreign key (PlatformId) 
	references Platform (Id);
alter table CaptureCpuResult 
	add index (TelemetryStatId), 
	add constraint FKD64510B51E161B1 
	foreign key (TelemetryStatId) 
	references TelemetryStat (Id);
alter table CaptureCpuResult 
	add index (ZoneId), 
	add constraint FKD64510B66E3E5C7 
	foreign key (ZoneId) 
	references NameHash (Id);
alter table CaptureCpuResult 
	add index (SetId), 
	add constraint FKD64510BF3864355 
	foreign key (SetId) 
	references NameHash (Id);
alter table CaptureCpuResult 
	add index (MetricId), 
	add constraint FKD64510B9019CB1D 
	foreign key (MetricId) 
	references NameHash (Id);
create index automated_test_number_idx on CaptureCpuResult (AutomatedTestNumber);
alter table CaptureFpsResult 
	add index (BuildId), 
	add constraint FKA20E73FAD949985D 
	foreign key (BuildId) 
	references Build (Id);
alter table CaptureFpsResult 
	add index (PlatformId), 
	add constraint FKA20E73FAB1A9B7F8 
	foreign key (PlatformId) 
	references Platform (Id);
alter table CaptureFpsResult 
	add index (TelemetryStatId), 
	add constraint FKA20E73FA51E161B1 
	foreign key (TelemetryStatId) 
	references TelemetryStat (Id);
alter table CaptureFpsResult 
	add index (ZoneId), 
	add constraint FKA20E73FA66E3E5C7 
	foreign key (ZoneId) 
	references NameHash (Id);
create index automated_test_number_idx on CaptureFpsResult (AutomatedTestNumber);
alter table CaptureDrawListResult 
	add index (BuildId), 
	add constraint FKB9621E83D949985D 
	foreign key (BuildId) 
	references Build (Id);
alter table CaptureDrawListResult 
	add index (PlatformId), 
	add constraint FKB9621E83B1A9B7F8 
	foreign key (PlatformId) 
	references Platform (Id);
alter table CaptureDrawListResult 
	add index (TelemetryStatId), 
	add constraint FKB9621E8351E161B1 
	foreign key (TelemetryStatId) 
	references TelemetryStat (Id);
alter table CaptureDrawListResult 
	add index (ZoneId), 
	add constraint FKB9621E8366E3E5C7 
	foreign key (ZoneId) 
	references NameHash (Id);
alter table CaptureDrawListResult 
	add index (DrawListId), 
	add constraint FKB9621E83A21E330B 
	foreign key (DrawListId) 
	references DrawList (Id);
create index automated_test_number_idx on CaptureDrawListResult (AutomatedTestNumber);
alter table CaptureThreadResult 
	add index (BuildId), 
	add constraint FK6766A873D949985D 
	foreign key (BuildId) 
	references Build (Id);
alter table CaptureThreadResult 
	add index (PlatformId), 
	add constraint FK6766A873B1A9B7F8 
	foreign key (PlatformId) 
	references Platform (Id);
alter table CaptureThreadResult 
	add index (TelemetryStatId), 
	add constraint FK6766A87351E161B1 
	foreign key (TelemetryStatId) 
	references TelemetryStat (Id);
alter table CaptureThreadResult 
	add index (ZoneId), 
	add constraint FK6766A87366E3E5C7 
	foreign key (ZoneId) 
	references NameHash (Id);
alter table CaptureThreadResult 
	add index (NameId), 
	add constraint FK6766A8731EC32E6F 
	foreign key (NameId) 
	references NameHash (Id);
create index automated_test_number_idx on CaptureThreadResult (AutomatedTestNumber);
alter table CapturePedAndVehicleResult 
	add index (BuildId), 
	add constraint FK8D7FB6BFD949985D 
	foreign key (BuildId) 
	references Build (Id);
alter table CapturePedAndVehicleResult 
	add index (PlatformId), 
	add constraint FK8D7FB6BFB1A9B7F8 
	foreign key (PlatformId) 
	references Platform (Id);
alter table CapturePedAndVehicleResult 
	add index (TelemetryStatId), 
	add constraint FK8D7FB6BF51E161B1 
	foreign key (TelemetryStatId) 
	references TelemetryStat (Id);
alter table CapturePedAndVehicleResult 
	add index (ZoneId), 
	add constraint FK8D7FB6BF66E3E5C7 
	foreign key (ZoneId) 
	references NameHash (Id);
create index automated_test_number_idx on CapturePedAndVehicleResult (AutomatedTestNumber);



------------------------------------------------
-- 2012/10/03 - Rename profilestatbase LastUpdated to ModifiedOn
------------------------------------------------
ALTER TABLE ProfileStatBase CHANGE COLUMN LastUpdated ModifiedOn DATETIME NOT NULL;


------------------------------------------------
-- 2012/09/25 - Additional indices to improve telemetry stat population
------------------------------------------------

create index GamerTag_Idx on Gamer (GamerTag);
create unique index AltIdentifier on Mission (AltIdentifier);
create index Start_Idx on WantedLevelStat (Start);
create index Start_Idx on MultiplayerSession (Start);
create index PacketIdx_Idx on TelemetryStatPacket (PacketIdx);
create index LifeId_Idx on SingleplayerLife (LifeId);
create index Spawned_Idx on SingleplayerLife (Spawned);
create index Start_Idx on SingleplayerSession (Start);
create index LifeId_Idx on MultiplayerLife (LifeId);
create index Spawned_Idx on MultiplayerLife (Spawned);
create index Start_Idx on MissionAttemptStat (Start);
create index MissionPassed_Idx on MissionAttemptStat (MissionPassed);
create index MissionFailed_Idx on MissionAttemptStat (MissionFailed);


------------------------------------------------
-- 2012/09/20 - Timestamp index
------------------------------------------------

create index TimestampIdx on TelemetryStat (`Timestamp`);




------------------------------------------------
-- Map export stats
------------------------------------------------

create table MapImageBuildStat (
    Id BIGINT NOT NULL AUTO_INCREMENT,
    CreatedOn DATETIME not null,
    MapExportStatId BIGINT not null unique,
    BuildStart DATETIME not null,
    BuildEnd DATETIME,
    Success TINYINT(1),
    primary key (Id)
)

create table MapImageBuildStat_MapSection (
    MapImageBuildStatId BIGINT not null,
    MapSectionId BIGINT not null,
    primary key (MapSectionId, MapImageBuildStatId)
)

create table MapSectionExportStat (
    Id BIGINT NOT NULL AUTO_INCREMENT,
    CreatedOn DATETIME not null,
    MapExportStatId BIGINT not null,
    MapSectionId BIGINT,
    ExportStart DATETIME not null,
    ExportEnd DATETIME,
    Success TINYINT(1),
    primary key (Id)
)

create table MapExportStat (
    Id BIGINT NOT NULL AUTO_INCREMENT,
    CreatedOn DATETIME not null,
    ModifiedOn DATETIME not null,
    Start DATETIME not null,
    End DATETIME,
    Username VARCHAR(255) not null,
    MachineName VARCHAR(255) not null,
    ExportType VARCHAR(255) not null,
    Success TINYINT(1),
    ExportNumber INTEGER UNSIGNED not null,
    primary key (Id)
)

alter table MapImageBuildStat 
    add index (MapExportStatId), 
    add constraint FK54B555278E9AC6F5 
    foreign key (MapExportStatId) 
    references MapExportStat (Id)

create index BuildStartIdx on MapImageBuildStat (BuildStart)

create index BuildEndIdx on MapImageBuildStat (BuildEnd)

alter table MapImageBuildStat_MapSection 
    add index (MapSectionId), 
    add constraint FK56460075B9B3DA6B 
    foreign key (MapSectionId) 
    references MapSection (Id)

alter table MapImageBuildStat_MapSection 
    add index (MapImageBuildStatId), 
    add constraint FK564600755BC9CF37 
    foreign key (MapImageBuildStatId) 
    references MapImageBuildStat (Id)

alter table MapSectionExportStat 
    add index (MapExportStatId), 
    add constraint FK27876AC38E9AC6F5 
    foreign key (MapExportStatId) 
    references MapExportStat (Id)

alter table MapSectionExportStat 
    add index (MapSectionId), 
    add constraint FK27876AC3B9B3DA6B 
    foreign key (MapSectionId) 
    references MapSection (Id)

create index ExportStartIdx on MapSectionExportStat (ExportStart)

create index ExportEndIdx on MapSectionExportStat (ExportEnd)

create index StartIdx on MapExportStat (Start)

create index EndIdx on MapExportStat (End)

create index UsernameIdx on MapExportStat (Username)
