﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// Represents a room.
    /// </summary>
    public class Room : GameAssetBase<RoomDto>
    {
    } // Room
}
