﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// Free mode match types.
    /// </summary>
    public class FreemodeMatchType : GameAssetBase
    {
    } // FreemodeMatchType
}
