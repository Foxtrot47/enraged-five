﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Model.Common.Map;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class SimpleMapArchetypeStat : ArchetypeStat
    {
        /// <summary>
        /// Whether this is a dynamic archetype.
        /// </summary>
        [StreamableStatAttribute(StreamableSimpleMapArchetypeStat.IsDynamicString)]
        public virtual bool IsDynamic { get; set; }

        /// <summary>
        /// Collision group for this archetype.
        /// </summary>
        [StreamableStatAttribute(StreamableSimpleMapArchetypeStat.CollisionGroupString)]
        public virtual String CollisionGroup { get; set; }

        /// <summary>
        /// Whether to force the collision to be baked.
        /// </summary>
        [StreamableStatAttribute(StreamableSimpleMapArchetypeStat.ForceBakeCollisionString)]
        public virtual bool ForceBakeCollision { get; set; }

        /// <summary>
        /// Whether collision is never baked.
        /// </summary>
        [StreamableStatAttribute(StreamableSimpleMapArchetypeStat.NeverBakeCollisionString)]
        public virtual bool NeverBakeCollision { get; set; }

        /// <summary>
        /// Drawable lod hierarchy information.
        /// </summary>
        [StreamableStatAttribute(StreamableSimpleMapArchetypeStat.DrawableLODsString)]
        public virtual IList<DrawableLodStat> DrawableLods { get; set; }

        /// <summary>
        /// List of spawn points that were gathered from the metadata files.
        /// </summary>
        [StreamableStatAttribute(StreamableSimpleMapArchetypeStat.SpawnPointsString)]
        public virtual IList<SpawnPointStat> SpawnPointStats { get; set; }
    } // SimpleMapArchetypeStat
}
