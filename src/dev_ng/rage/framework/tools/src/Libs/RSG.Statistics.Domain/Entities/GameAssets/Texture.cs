﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssets
{
    /// <summary>
    /// 
    /// </summary>
    public class Texture : GameAssetBase<TextureDto>
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public virtual string Filename
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual string AlphaFilename
        {
            get;
            set;
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual string TextureType
        {
            get;
            set;
        }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public Texture()
            : base()
        {
        }
        #endregion // Constructor(s)

        #region GameAssetBase Overrides
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        public override void Update(TextureDto dto)
        {
            base.Update(dto);

            Filename = dto.Filename;
            Filename = dto.AlphaFilename;
            Filename = dto.TextureType;
        }
        #endregion // GameAssetBase Overrides
    } // Texture
}
