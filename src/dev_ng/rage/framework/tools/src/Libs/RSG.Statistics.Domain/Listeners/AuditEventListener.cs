﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NHibernate.Event;
using RSG.Statistics.Domain.Entities;
using NHibernate.Persister.Entity;

namespace RSG.Statistics.Domain.Listeners
{
    /// <summary>
    /// Custom NHibernate event listener used for updating created on and modified on audit information
    /// </summary>
    public class AuditEventListener : IPreUpdateEventListener, IPreInsertEventListener
    {
        /// <summary>
        /// Called prior to updates in the DB
        /// </summary>
        /// <param name="event"></param>
        /// <returns>Returns true if the operation should be vetoed</returns>
        public bool OnPreUpdate(PreUpdateEvent @event)
        {
            // Check whether the entity is one we are looking for
            IHasModifiedInformation audit = @event.Entity as IHasModifiedInformation;
            if (audit != null)
            {
                // Update both the state and the entity's modified on times
                DateTime time = DateTime.Now;
                Set(@event.Persister, @event.State, "ModifiedOn", time);
                audit.ModifiedOn = time;
            }

            return false;
        }

        /// <summary>
        /// Called prior to inserts in the DB
        /// </summary>
        /// <param name="event"></param>
        /// <returns>Returns true if the operation should be vetoed</returns>
        public bool OnPreInsert(PreInsertEvent @event)
        {
            DateTime time = DateTime.Now;

            // Check whether the entity is one we are looking for
            IHasCreatedInformation created = @event.Entity as IHasCreatedInformation;
            if (created != null)
            {
                // Update both the state and the entity's created on times
                Set(@event.Persister, @event.State, "CreatedOn", time);
                created.CreatedOn = time;
            }

            IHasModifiedInformation modified = @event.Entity as IHasModifiedInformation;
            if (modified != null)
            {
                Set(@event.Persister, @event.State, "ModifiedOn", time);
                modified.ModifiedOn = time;
            }

            return false;
        }

        /// <summary>
        /// Helper for setting the state property
        /// </summary>
        /// <param name="persister"></param>
        /// <param name="state"></param>
        /// <param name="propertyName"></param>
        /// <param name="value"></param>
        private void Set(IEntityPersister persister, object[] state, string propertyName, object value)
        {
            int index = Array.IndexOf(persister.PropertyNames, propertyName);
            if (index != -1)
            {
                state[index] = value;
            }
        }
    } // AuditEventListener
}
