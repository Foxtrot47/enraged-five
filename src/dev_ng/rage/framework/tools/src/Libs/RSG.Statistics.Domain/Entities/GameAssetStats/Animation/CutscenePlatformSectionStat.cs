﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Domain.Entities.GameAssets;

namespace RSG.Statistics.Domain.Entities.GameAssetStats.Animation
{
    /// <summary>
    /// 
    /// </summary>
    public class CutscenePlatformSectionStat : GameAssetPlatformStatBase<Cutscene, CutsceneStat>
    {
        /// <summary>
        /// Index of this item.
        /// </summary>
        public virtual uint Index { get; set; }
    } // CutscenePlatformSectionStat
}
