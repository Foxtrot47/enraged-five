﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common;
using RSG.Model.Common.Map;

namespace RSG.Statistics.Domain.Entities.GameAssetStats
{
    /// <summary>
    /// 
    /// </summary>
    public class InteriorArchetypeStat : ArchetypeStat
    {
        /// <summary>
        /// List of room statistics for this interior (lazily loaded).
        /// </summary>
        [StreamableStatAttribute(StreamableInteriorArchetypeStat.RoomsString)]
        public virtual IList<RoomStat> RoomStats { get; set; }

        /// <summary>
        /// List of portal statistics for this interior (lazily loaded).
        /// </summary>
        [StreamableStatAttribute(StreamableInteriorArchetypeStat.PortalsString)]
        public virtual IList<PortalStat> PortalStats { get; set; }

        /// <summary>
        /// List of entity statistics for this interior (lazily loaded).
        /// </summary>
        [StreamableStatAttribute(StreamableInteriorArchetypeStat.EntitiesString)]
        public virtual IList<EntityStat> EntityStats { get; set; }
    } // InteriorArchetypeStat
}
