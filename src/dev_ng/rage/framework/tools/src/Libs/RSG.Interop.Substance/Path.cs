﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using RSG.Base.Configuration;

namespace RSG.Interop.Substance
{
    public static class Path
    {
        private static IConfig _config = ConfigFactory.CreateConfig();

        private static string _substanceRoot = _config.Project.Root + "\\art\\techart\\substance";
        private static string _archives = _substanceRoot + "\\archives";
        private static string _batchTools = _config.ToolsBin + "\\substance\\batchTools\\" + Substance.BatchToolsVersion.ToString() + ".x";
        private static string _resources = _config.ToolsBin + "\\substance\\resources\\" + Substance.BatchToolsVersion.ToString() + ".x";

        private static string _sbsRenderExe = _batchTools + "\\sbsrender.exe";
        private static string _sbsCookerExe = _batchTools + "\\sbscooker.exe";
        private static string _sbsBakerExe = _batchTools + "\\sbsbaker.exe";
        private static string _sbsMutatorExe = _batchTools + "\\sbsmutator.exe";

        public static string Root
        {
            get { return _substanceRoot; }
        }

        public static string Archives
        {
            get { return _archives; }
        }

        public static string Resources
        {
            get { return _resources; }
        }

        public static string SbsRenderExe
        {
            get { return _sbsRenderExe; }
        }

        public static string SbsCookerExe
        {
            get { return _sbsCookerExe; }
        }

        public static string SbsBakerExe
        {
            get { return _sbsBakerExe; }
        }

        public static string SbsMutatorExe
        {
            get { return _sbsMutatorExe; }
        }
    }
}
