﻿using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Interop.Substance
{
    public class SbsInfo
    {
        public SbsInfo(string filename)
        {
        }
    }

    public class SbsWrapper
    {
        private string _filename;
        private string _outputName;
        private Dictionary<string, string> _inputImages = new Dictionary<string, string>();
        private Dictionary<string, string> _inputParameters = new Dictionary<string, string>();
        private SbsInfo _info;
        private string _outputImageType;

        public SbsWrapper(string sbsFilename, string outputImageType = "tif")
        {
            _filename = sbsFilename;
            _info = new SbsInfo(sbsFilename);
            _outputImageType = outputImageType;
        }

        public void SetInputParameter(string identifier, string value)
        {
            if (!_inputParameters.ContainsKey(identifier))
            {
                _inputParameters.Add(identifier, value);
            }
            else
            {
                _inputParameters[identifier] = value;
            }
        }

        public void SetInputImage(string identifier, string filename)
        {
            if (!_inputImages.ContainsKey(identifier))
            {
                _inputImages.Add(identifier, filename);
            }
            else
            {
                _inputImages[identifier] = filename;
            }
        }

        public void SetOutputName(string name)
        {
            _outputName = name;
        }

        public void SetOutputSize(int width, int height)
        {
            int wExponent = 1;
            int hExponent = 1;

            int w = width;
            int h = height;

            while (w != 2)
            {
                wExponent += 1;
                w /= 2;
            }

            while (h != 2)
            {
                hExponent += 1;
                h /= 2;
            }
            string size = wExponent.ToString() + "," + hExponent.ToString();
            this.SetInputParameter("$outputsize", size);
        }

        public List<string> Render(string outputDir)
        {
            List<string> outputImages = new List<string>();

            StringBuilder cmdStr = new StringBuilder();
            cmdStr.Append("render ");
            cmdStr.Append("--inputs " + _filename + " ");

            // Set input images.
            foreach (KeyValuePair<string, string> entry in _inputImages)
            {
                // Set identifier and image filename.
                cmdStr.Append("--set-entry " + entry.Key + "@\"" + entry.Value + "\" ");
            }

            // Set input parameters.
            foreach (KeyValuePair<string, string> entry in _inputParameters)
            {
                cmdStr.Append("--set-value " + entry.Key + "@" + entry.Value + " ");
            }

            // Set output parameters.
            cmdStr.Append("--output-path \"" + outputDir + "\" ");
            cmdStr.Append("--output-name " + _outputName + "_{outputNodeName} ");
            cmdStr.Append("--output-format " + _outputImageType + " ");

            // Run sbsrender.exe with our command string.
            Process cmd = new Process();
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.FileName = Path.SbsRenderExe;
            cmd.StartInfo.Arguments = cmdStr.ToString();
            cmd.StartInfo.UseShellExecute = false;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.Start();

            StreamReader stream = cmd.StandardOutput;
            string output = stream.ReadToEnd();
            cmd.WaitForExit();

            // Parse output for resulting image filenames.
            string[] lines = output.Split('\n');

            foreach (string line in lines)
            {
                if (line.StartsWith("* Output"))
                {
                    string tLine = line.TrimEnd();
                    int sep = tLine.IndexOf(':');
                    string path = tLine.Substring(sep + 2);
                    string filename = path.Replace("\"","");
                    outputImages.Add(System.IO.Path.GetFullPath(filename));
                }
            }

            return outputImages;
        }
    }

    public static class Substance
    {
        private const int BATCH_TOOLS_VERSION = 3;

        public static int BatchToolsVersion
        {
            get { return BATCH_TOOLS_VERSION; }
        }
    }
}
