﻿using System;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Pipeline.Core;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common.JobManagement;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Triggers;
using System.Reflection;

namespace RSG.Pipeline.Automation.Common.JobFactories
{

    /// <summary>
    /// CodeBuilder2Job object factory.
    /// - it could be worth considering if having this factory distinct from the class of the Job it creates is worthy?
    /// - what not just have some static methods in the class? - presumably this is because a Factory is not always a static class?
    /// </summary>
    public static class CodeBuilder2JobFactory
    {    
        #region Constants
        /// <summary>
        /// The context.
        /// </summary>
        private static readonly String LOG_CTX = "CodeBuilder2JobFactory";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Log object.
        /// </summary>
        private static IUniversalLog Log { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Static constructor.
        /// </summary>
        static CodeBuilder2JobFactory()
        {
            LogFactory.Initialize();
            Log = LogFactory.CreateUniversalLog(LOG_CTX);
        }
        #endregion // Constructor(s)

        #region Public Static Methods

        /// <summary>
        /// Create a job from an xelement
        /// </summary>
        /// <param name="xelement"></param>
        /// <returns></returns>
        public static CodeBuilder2Job CreateJob(IBranch branch, XElement xelement)
        {
            CodeBuilder2Job job = CreateUsingActivator(branch, xelement);
            return job;
        }

        /// <summary>
        /// Process the changelist creating jobs according the internal dispatch settings.
        /// </summary>
        /// <param name="changelist"></param>
        /// <param name="changeListFileMappings"></param>
        /// <param name="dispatchScheme"></param>
        /// <param name="monitoredPaths"></param>
        /// <returns></returns>
        public static IEnumerable<CodeBuilder2Job> CreateJobsForChangelist(Changelist changelist, IEnumerable<FileMapping> changeListFileMappings, DispatchScheme dispatchScheme, IEnumerable<String> monitoredPaths, IBranch branch)
        {
            ICollection<CodeBuilder2Job> jobs = new List<CodeBuilder2Job>();

            if (!changeListFileMappings.Any())
            {
                Log.ErrorCtx(LOG_CTX, "No files in changelist : this is unexpected.");
                return jobs;
            }

            // The guid created for this instance of factory creation 
            //  this is used for associating groups of jobs. eg. those created for the same changelist.
            Guid triggerId = Guid.NewGuid();

            int totalJobCreated = 0;

            foreach (DispatchGroup dispatchGroup in dispatchScheme.Groups)
            {
                Log.MessageCtx(LOG_CTX, "Considering dispatch group '{0}', which has {1} items",
                dispatchGroup.Name, dispatchGroup.Items.Count());

                Log.MessageCtx(LOG_CTX, "Creating CodeBuilder2Jobs from changelist {0} with the following {1} files:",
                    changelist.Number, changeListFileMappings.Count());

                foreach (FileMapping fm in changeListFileMappings)
                    Log.MessageCtx(LOG_CTX, "\t'{0}'", fm.DepotFilename);

                ICollection<String> changeListFilenames = new List<String>();
                foreach (FileMapping changeListFileMapping in changeListFileMappings)
                {
                    changeListFilenames.Add(changeListFileMapping.DepotFilename);
                }

                //
                // FOR EVERY SOLUTION ITEM IN THE DISPATCH GROUPS CREATE A JOB
                // - client workers don't work filter on paths contained within the files.
                // - we just create a mass of jobs to be processed by the worker farm.
                //
                int numJobsCreated = 0;
                IEnumerable<IItem> items = dispatchGroup.Items.Where(dg => dg is SolutionItem);
                int count = items.Count();
                Log.MessageCtx(LOG_CTX, " ");

                foreach (IItem item in items)
                {
                    SolutionItem solutionItem = (SolutionItem)item;
                    Log.MessageCtx(LOG_CTX, "Creating CodeBuilder2Job ({0}/{1}) for {2} from changelist {3} : ({4,12}|{5,-12}) : \t{6}", numJobsCreated + 1, count, dispatchGroup.MachineName, changelist.Number, solutionItem.Platform, solutionItem.BuildConfig, solutionItem.SolutionFilename);
                    CodeBuilder2Job cbj = CreateUsingActivator(changelist, changeListFilenames, solutionItem, branch, monitoredPaths, item.PrebuildCommands, item.PostbuildCommands, item.Labels, triggerId); 
                    jobs.Add(cbj);
                    numJobsCreated++;
                }
                totalJobCreated += numJobsCreated;
            }
            Log.MessageCtx(LOG_CTX, "Created {0} Codebuilder2 jobs for CL {1} with triggerId {2}", totalJobCreated, changelist.Number, triggerId.ToString());
            
            return jobs;
        }

        /// <summary>
        /// Consume jobs : for a number of jobs to consume attempt to consume them, collating their details into a consuming job
        /// </summary>
        /// <param name="jobsToConsume">The jobs that are to be consumed</param>
        /// <param name="jobsConsumed">The jobs that were consumed.</param>
        /// <returns>The consuming job</returns>
        public static CodeBuilder2Job CreateConsumingJob(CommandOptions options, IEnumerable<CodeBuilder2Job> jobsToConsume, out IEnumerable<CodeBuilder2Job> jobsConsumed)
        {
            // Collate all the details across all the jobs to build the consuming job.           
            try
            {
                // Single() by virtue of the expection it throws assures us we are not losing information.
                String solutionFilename = jobsToConsume.Select(j => j.SolutionFilename).Distinct().Single();
                String targetDir = jobsToConsume.Select(j => j.TargetDir).Distinct().Single();
                String publishDir = jobsToConsume.Select(j => j.PublishDir).Distinct().Single();
                String buildConfig = jobsToConsume.Select(j => j.BuildConfig).Distinct().Single();
                String platform = jobsToConsume.Select(j => j.Platform).Distinct().Single();
                String tool = jobsToConsume.Select(j => j.Tool).Distinct().Single();
                bool rebuild = jobsToConsume.Select(j => j.Rebuild).Distinct().Single();
                bool requiresPostJobNotification = jobsToConsume.Select(j => j.RequiresPostJobNotification).Distinct().Single();
                int skipConsume = jobsToConsume.Select(j => j.SkipConsume).Distinct().Single(); // only used to throw exception if not single.

                // DW: really this should be checked to be distinct and single too, but CanBind() should already assure this.
                CodeBuilder2Job firstJob = jobsToConsume.First();
                IEnumerable<String> prebuildCommands = firstJob.PrebuildCommands;
                IEnumerable<String> postbuildCommands = firstJob.PostbuildCommands;
                IEnumerable<String> labels = firstJob.Labels;

                // DW: these priorities should be extracted into a common class, I don't see that need more than one priority class?
                RSG.Pipeline.Automation.Common.JobManagement.Priority priority = firstJob.Priority == JobPriority.Normal ? RSG.Pipeline.Automation.Common.JobManagement.Priority.Standard : RSG.Pipeline.Automation.Common.JobManagement.Priority.High;

                IEnumerable<ITrigger> triggers = jobsToConsume.Select(j => j.Trigger);
                int skipConsumeState = -3;
                SolutionItem solutionItem = new SolutionItem(solutionFilename, targetDir, publishDir, priority, platform, buildConfig, tool, rebuild, prebuildCommands, postbuildCommands, labels, skipConsumeState, requiresPostJobNotification);

                IBranch branch;
                IProject project;
                if (!firstJob.GetProjectAndBranch(options, Log, out project, out branch))
                {
                    Log.ErrorCtx(LOG_CTX, "Job binding: Couldn't derive project and branch for codebuilder job");
                    jobsConsumed = null;
                    return null;
                }

                CodeBuilder2Job consumingJob = CreateUsingActivator(triggers, solutionItem, branch);

                // We ensure we externally list all the jobs consumed
                jobsConsumed = jobsToConsume;
                
                return consumingJob;
            }
            catch (InvalidOperationException ex)
            {
                Log.ToolExceptionCtx(LOG_CTX, ex, "Distinct binding of jobs has non single properties that would be bound");
            }

            jobsConsumed = null;
            return null;
        }

        /// <summary>
        /// Create job using activator.
        /// </summary>
        /// <param name="triggers"></param>
        /// <param name="solutionItem"></param>
        /// <returns></returns>
        private static CodeBuilder2Job CreateUsingActivator(IEnumerable<ITrigger> triggers, SolutionItem solutionItem, IBranch branch)
        {
            Guid triggerId = Guid.NewGuid();
            DateTime triggeredAt = DateTime.Now.ToUniversalTime();

            CodeBuilder2Job job = (CodeBuilder2Job)CreateNonPublicUsingActivator(new object[] { triggers, solutionItem, branch, triggerId, triggeredAt });
            return job;
        }

        /// <summary>
        /// Create job using activator
        /// </summary>
        /// <param name="change"></param>
        /// <param name="pathnamesToProcess"></param>
        /// <param name="solutionItem"></param>
        /// <param name="monitoringPaths"></param>
        /// <param name="prebuildCommands"></param>
        /// <param name="postbuildCommands"></param>
        /// <param name="triggerId"></param>
        /// <returns></returns>
        private static CodeBuilder2Job CreateUsingActivator(Changelist change, IEnumerable<String> pathnamesToProcess, SolutionItem solutionItem, IBranch branch, IEnumerable<String> monitoringPaths, IEnumerable<String> prebuildCommands, IEnumerable<String> postbuildCommands, IEnumerable<String> labels, Guid triggerId)
        {
            CodeBuilder2Job job = CreateNonPublicUsingActivator(new object[] { change, pathnamesToProcess, solutionItem, branch, monitoringPaths, prebuildCommands, postbuildCommands, labels, triggerId });
            return job;
        }

        /// <summary>
        /// Create job using xelement : for deserialisation.
        /// </summary>
        /// <param name="xelement"></param>
        /// <returns></returns>
        private static CodeBuilder2Job CreateUsingActivator(IBranch branch, XElement xelement)
        {
            CodeBuilder2Job job = CreateNonPublicUsingActivator(new object[] { branch, xelement });
            return job;
        }

        /// <summary>
        /// Gets around the privateness of the constructor using reflection.
        /// - this is not a hack, by using this we ensure the the codebuilder2job is immutable.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private static CodeBuilder2Job CreateNonPublicUsingActivator(object[] args)
        {
            try
            {
                CodeBuilder2Job job = (CodeBuilder2Job)Activator.CreateInstance(typeof(CodeBuilder2Job), BindingFlags.NonPublic | BindingFlags.Instance, null, args, null);
                return job;
            }
            catch (MissingMethodException ex)
            {
                Log.ToolExceptionCtx(LOG_CTX, ex, "Factory failed : constructor missing?");
            }
            return null;
        }

        #endregion // Public Static Methods
    } // class CodeBuilder2JobFactory
} // namespace RSG.Pipeline.Automation.ServerHost.JobFactory
