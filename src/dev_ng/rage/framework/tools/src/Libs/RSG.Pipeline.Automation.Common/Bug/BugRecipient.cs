﻿using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Bug
{
    /// <summary>
    /// A bug recipient for sending bug to a user
    /// - modelled of the way in which we send email.
    /// </summary>
    public sealed class BugRecipient :
          IBugRecipient
    {
        #region Properties
        /// <summary>
        /// Rule to determine whether to bug recipient.
        /// </summary>
        public BugRule Rule { get; private set; }
        /// <summary>
        /// Bug Owner
        /// </summary>
        public String Owner { get; private set; }
        /// <summary>
        /// Bug QA 
        /// </summary>
        public String QA { get; private set; }
        /// <summary>
        /// Bug Reviewer
        /// </summary>
        public String Reviewer { get; private set; }
        /// <summary>
        /// Users to cc on bug
        /// </summary>
        public ICollection<String> CcList { get; private set; }
        /// <summary>
        /// Bug component
        /// </summary>
        public String Component { get; private set; }
        /// <summary>
        /// Bug Tag
        /// </summary>
        public String Tag { get; private set; }
        /// <summary>
        /// Bug class
        /// </summary>
        public String Class { get; private set; }
        /// <summary>
        /// Bug priority
        /// </summary>
        public String Priority { get; private set; }
        /// <summary>
        /// Estimated Time in minutes.
        /// </summary>
        public float EstimatedTime { get; private set; }
        #endregion // properties

        #region Constructor(s)
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="bugOwner"></param>
        /// <param name="bugQA"></param>
        /// <param name="bugReviewer"></param>
        /// <param name="ccList"></param>
        /// <param name="bugComponent"></param>
        /// <param name="bugTag"></param>
        /// <param name="bugClass"></param>
        /// <param name="bugPriority"></param>
        /// <param name="bugRule"></param>
        public BugRecipient(String bugOwner, String bugQA, String bugReviewer, ICollection<string> ccList, String bugComponent, String bugTag, String bugClass, String bugPriority, BugRule bugRule, float estimatedTime)
        {
            Owner = bugOwner;
            QA = bugQA;
            Reviewer = bugReviewer;
            CcList = ccList;
            Component = bugComponent;
            Tag = bugTag;
            Class = bugClass;
            Priority = bugPriority;
            Rule = bugRule;
            EstimatedTime = estimatedTime;
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Determines whether a notification is requried for a given notification recipient.
        /// - more support required for other jobstates.
        /// </summary>
        /// <param name="job"></param>
        /// <param name="bugRecipient"></param>
        /// <returns></returns>
        public bool BugRequired(IJob job)
        {
            if ((job.State == JobState.Errors && Rule.HasFlag(BugRule.OnErrors)))
                return (true);
            return (false);
        }
        #endregion // Public Methods

        #region Static Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rule"></param>
        /// <returns></returns>
        public static BugRule NotifyStringToRule(String ruleString)
        {
            BugRule result = BugRule.Never;

            List<string> rules = ruleString.Split(',').Select(s => s.Trim()).ToList();

            foreach (string rule in rules)
            {
                switch (rule)
                {
                    case "errors":
                        result |= BugRule.OnErrors;
                        break;
                    case "all":
                        result = BugRule.OnAll; // ALL means ALL
                        break;
                    case "none":
                    default:
                        result = BugRule.Never;
                        break;
                }
            }
            return result;
        }
        #endregion // Static Functions
    }
}
