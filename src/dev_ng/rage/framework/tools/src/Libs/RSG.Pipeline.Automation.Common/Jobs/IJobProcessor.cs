﻿using System;
using System.Collections.Generic;
using RSG.Base.Configuration;
using RSG.Pipeline.Services;
using RSG.Pipeline.Automation.Common.Triggers;

namespace RSG.Pipeline.Automation.Common.Jobs
{

    /// <summary>
    /// Job processing interface; abstracting the processing logic from the job
    /// storage (IJob).
    /// </summary>
    public interface IJobProcessor
    {
        #region Properties
        /// <summary>
        /// Job processor supported roles and job types.
        /// </summary>
        CapabilityType Role { get; }

        /// <summary>
        /// Pipeline command options object.
        /// </summary>
        CommandOptions Options{ get; }

        /// <summary>
        /// Job processor parameters (automatically loaded from %RS_TOOLSCONFIG%/automation/!name!.xml).
        /// </summary>
        IDictionary<String, Object> Parameters { get; }

        /// <summary>
        /// Build Tokens supported by the job processor
        /// </summary>
        IEnumerable<BuildToken> SupportedBuildTokens {get; }

        #endregion // Properties

        #region Methods
        /// <summary>
        /// Process requested job.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        IJobResult Process(IJob job);

        /// <summary>
        /// Check client suitability to run he job processor.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        bool CheckClientSuitability();
        #endregion // Methods
    }

} // RSG.Pipeline.Automation.Common.Jobs namespace
