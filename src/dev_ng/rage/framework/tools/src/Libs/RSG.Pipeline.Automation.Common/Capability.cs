﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace RSG.Pipeline.Automation.Common
{

    /// <summary>
    /// Abstraction of client and server capabilities.
    /// </summary>
    [DataContract(Namespace = "")]
    public class Capability
    {

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public CapabilityType Type
        {
            get;
            private set;
        }

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="type"></param>
        public Capability(CapabilityType type)
        {
            this.Type = type;
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Automation.Common namespace
