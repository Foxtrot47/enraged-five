﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Export
{
    
    /// <summary>
    /// 
    /// </summary>
    public class UserExportRequestEventArgs : EventArgs
    {
        #region Properties
        /// <summary>
        /// Request info.
        /// </summary>
        public UserExportRequest Request
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="request"></param>
        public UserExportRequestEventArgs(UserExportRequest request)
        {
            this.Request = request;
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Automation.Common.Export namespace
