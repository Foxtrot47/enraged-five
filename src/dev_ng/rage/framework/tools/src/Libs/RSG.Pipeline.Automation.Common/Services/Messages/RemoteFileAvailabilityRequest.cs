﻿using System;
using System.ServiceModel;

namespace RSG.Pipeline.Automation.Common.Services.Messages
{

    /// <summary>
    /// Remote file availability request object; specifying the job ID for which
    /// the user wants to determine whether files are available.
    /// </summary>
    [MessageContract]
    public class RemoteFileAvailabilityRequest
    {
        #region Properties
        /// <summary>
        /// Job identifier.
        /// </summary>
        [MessageBodyMember]
        public Guid Job { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public RemoteFileAvailabilityRequest()
        {
            this.Job = Guid.Empty;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="job"></param>
        public RemoteFileAvailabilityRequest(Guid job)
        {
            this.Job = job;
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Automation.Common.Services.Messages namespace
