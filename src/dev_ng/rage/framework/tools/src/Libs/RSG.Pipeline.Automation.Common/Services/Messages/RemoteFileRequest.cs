﻿using System;
using System.ServiceModel;

namespace RSG.Pipeline.Automation.Common.Services.Messages
{

    /// <summary>
    /// Remote file request object; for requesting downloads.
    /// </summary>
    [MessageContract]
    public class RemoteFileRequest
    {
        #region Properties
        /// <summary>
        /// Filename string.
        /// </summary>
        [MessageBodyMember]
        public String Filename { get; set; }

        /// <summary>
        /// Job identifier.
        /// </summary>
        [MessageBodyMember]
        public Guid Job { get; set; }
        #endregion // Properties
    }

} // RSG.Pipeline.Automation.Common.Services.Messages namespace
