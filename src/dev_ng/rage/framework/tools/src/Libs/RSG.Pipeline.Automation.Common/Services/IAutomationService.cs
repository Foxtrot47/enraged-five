﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using RSG.Pipeline.Automation.Common.Jobs;

namespace RSG.Pipeline.Automation.Common.Services
{

    /// <summary>
    /// Automation service interface; automation service is used for handing
    /// jobs out to a pool of clients.
    /// </summary>
    [ServiceContract(ConfigurationName = "IAutomationService")]
    public interface IAutomationService
    {
        /// <summary>
        /// Register worker to the automation service; providing information 
        /// about what the worker's capabilities are.
        /// </summary>
        /// <param name="workerConnection"></param>
        /// <param name="capability"></param>
        [OperationContract(IsOneWay = false)]
        Guid RegisterWorker(String workerConnection, Capability capability);

        /// <summary>
        /// Unregister worker from automation service; worker is being stopped,
        /// killed or shutdown.
        /// </summary>
        /// this.m_Workers.Add(worker);<param name="id">
        /// </param>
        [OperationContract(IsOneWay = true)]
        void UnregisterWorker(Guid id);

        /// <summary>
        /// Queries to see if a worker with a given ID is registered
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OperationContract(IsOneWay = false)]
        bool IsWorkerRegistered(Guid id);

        /// <summary>
        /// Externally enqueue a job onto a task queue; based on the job role
        /// and priority.  This should be used for user-invoked rebuilds.
        /// </summary>
        /// <param name="job"></param>
        /// <returns>true iff successfully enqueued; false otherwise.</returns>
        [OperationContract]
        [ServiceKnownType(typeof(Job))]
        [ServiceKnownType(typeof(MapExportJob))]
        [ServiceKnownType(typeof(AssetBuilderJob))]
        [ServiceKnownType(typeof(CodeBuilderJob))]
        [ServiceKnownType(typeof(IntegratorJob))]
        [ServiceKnownType(typeof(CodeBuilder2Job))]
        [ServiceKnownType(typeof(ScriptBuilderJob))]
        bool EnqueueJob(IJob job);

        /// <summary>
        /// Worker notification to automation service that the job was completed.
        /// </summary>
        /// <param name="job"></param>
        [OperationContract(IsOneWay = true)]
        [ServiceKnownType(typeof(Job))]
        [ServiceKnownType(typeof(MapExportJob))]
        [ServiceKnownType(typeof(AssetBuilderJob))]
		[ServiceKnownType(typeof(CodeBuilderJob))]
        [ServiceKnownType(typeof(IntegratorJob))]
        [ServiceKnownType(typeof(JobResult))]
        [ServiceKnownType(typeof(AssetBuilderJobResult))]
        [ServiceKnownType(typeof(IntegratorJobResult))]
        [ServiceKnownType(typeof(CodeBuilder2Job))]
        [ServiceKnownType(typeof(ScriptBuilderJob))]
        void JobCompleted(IJob job, IEnumerable<IJobResult> jobResults);
    }

} // RSG.Pipeline.Automation.Common.Services namespace
