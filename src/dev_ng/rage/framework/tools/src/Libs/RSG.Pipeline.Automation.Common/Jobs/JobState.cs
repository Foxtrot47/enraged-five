﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using RSG.Base.Attributes;

namespace RSG.Pipeline.Automation.Common.Jobs
{

    /// <summary>
    /// Job state enumeration; for all possible states for an individual job.
    /// </summary>
    /// Browsable attributes are used to toggle filtering in the Automation Monitor
    /// for that particular JobState value.
    /// 
    [DataContract]
    public enum JobState
    {
        [Browsable(true)]
        [FieldDisplayName("Pending")]
        [EnumMember]
        Pending,

        [Browsable(true)]
        [FieldDisplayName("Assigned")]
        [EnumMember]
        Assigned,

        [Browsable(true)]
        [FieldDisplayName("Completed")]
        [EnumMember]
        Completed,

        [Browsable(true)]
        [FieldDisplayName("Errors")]
        [EnumMember]
        Errors,

        [Browsable(true)]
        [FieldDisplayName("Skipped")]
        [EnumMember]
        Skipped,

        [Browsable(true)]
        [FieldDisplayName("SkippedConsumed")]
        [EnumMember]
        SkippedConsumed,

        [Browsable(true)]
        [FieldDisplayName("Aborted")]
        [EnumMember]
        Aborted,

        [Browsable(true)]
        [FieldDisplayName("Client Error")]
        [EnumMember]
        ClientError,
    }

} // RSG.Pipeline.Automation.Common.Jobs namespace
