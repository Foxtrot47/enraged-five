﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using RSG.Pipeline.Automation.Common.Jobs;

namespace RSG.Pipeline.Automation.Common.Tasks
{

    /// <summary>
    /// 
    /// </summary>
    [DataContract(Namespace = "")]
    public class MonitoringTaskStatus : TaskStatus
    {
        #region Properties
        /// <summary>
        /// Monitored paths.
        /// </summary>
        [DataMember]
        public IEnumerable<String> MonitoredPaths { get; set; }

        /// <summary>
        /// Currently processing changelist number.
        /// </summary>
        [DataMember]
        public uint CurrentChangelist { get; set; }

        /// <summary>
        /// Current maximum changelist number.
        /// </summary>
        [DataMember]
        public uint MaximumChangelist { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="task"></param>
        public MonitoringTaskStatus(MonitoringTaskBase task)
            : base(task)
        {
            this.MonitoredPaths = new List<String>(task.MonitoredPaths);
            this.CurrentChangelist = task.CurrentChangelist;
            this.MaximumChangelist = task.MaximumChangelist;
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Automation.Common.Tasks namespace
