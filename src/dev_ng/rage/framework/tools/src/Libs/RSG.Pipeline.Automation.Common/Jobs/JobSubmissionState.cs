using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Jobs
{
    /// <summary>
    /// Enum of changelist submission states
    /// - this class may eventually find it's place elsewhere
    /// - for now it has a loose association with asset builder jobs.
    /// </summary>
    public enum JobSubmissionState : int
    {
        /// <summary>
        /// Indicates a CL hasn't yet been submitted.
        /// </summary>
        NotSubmitted = -1,

        /// <summary>
        /// Indicates a CL was revert unchanged.
        /// </summary>
        RevertUnchanged = -2,

        /// <summary>
        /// Indicates a CL had no files in it.
        /// </summary>
        NoFiles = -3,

        /// <summary>
        /// Indicates a CL submit operation had errors.
        /// </summary>
        Errored = -4,

        /// <summary>
        /// Indicates a CL submit did not want to submit.
        /// </summary>
        SubmitDisabled = -5,

        /// <summary>
        /// Indicates a CL was reverted.
        /// </summary>
        Reverted = -6,

        /// <summary>
        /// Indicates a CL was integrated.
        /// </summary>
        Integrated = -7,

        /// <summary>
        /// Indicates a CL submit did not want to submit because errors were present
        /// </summary>
        SubmitDisabledDueToErrors = -8,
    }
}
