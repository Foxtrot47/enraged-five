﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using System.Xml.Linq;
using System.Xml;
using RSG.Pipeline.Automation.Common.Client;
using RSG.Pipeline.Automation.Common.Services.Messages;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.ServerHost.WCFExtensions;

namespace RSG.Pipeline.Automation.Common.Services
{

    /// <summary>
    /// Server administration service.
    /// </summary>
    [ServiceContract(ConfigurationName = "IAutomationAdminService")]
    public interface IAutomationAdminService
    {
        /// <summary>
        /// Return status objects for tasks that are part of this automation.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet]
        [CorsEnabled]
        [Description("Returns monitoring information about the automation service.")]
        TaskStatus[] Monitor();

        /// <summary>
        /// Return status objects for tasks that are part of this automation (filtered).
        /// service.
        /// </summary>
        /// <param name="role">Role filter</param>
        /// <param name="start">Start index</param>
        /// <param name="count">Count</param>
        /// <returns></returns>
        /// <seealso cref="MonitorFilteredCount"/>
        [OperationContract]
        [WebGet(UriTemplate = "/MonitorFiltered?role={role}&start={start}&count={count}")]
        [Description("Returns monitoring information about the automation service (filtered).")]
        TaskStatus[] MonitorFiltered(CapabilityType role, int start, int count);

        /// <summary>
        /// Return job count for a particular task (used for paginating MonitorFiltered).
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "/MonitorFilteredCount?role={role}")]
        [Description("Return job count for a particular task (used for paginating MonitorFiltered).")]
        int MonitorFilteredCount(CapabilityType role);

        /// <summary>
        /// Return status objects for all the clients that are part of this
        /// automation service.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebGet]
        [CorsEnabled]
        [Description("Returns information about the clients connected to the automation service.")]
        WorkerStatus[] Clients();

        /// <summary>
        /// Request to skip a job.
        /// </summary>
        /// <param name="changelist"></param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "/skip?job={id}")]
        [CorsEnabled]
        [Description("Skip a job from being processed.")]
        JobResponse SkipJob(Guid id);

        /// <summary>
        /// Request to prioritise a job.
        /// </summary>
        /// <param name="changelist"></param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "/prioritise?job={id}")]
        [CorsEnabled]
        [Description("Prioritise a job to be processed sooner.")]
        JobResponse PrioritiseJob(Guid id);

        /// <summary>
        /// Request to reprocess a job.
        /// </summary>
        /// <param name="changelist"></param>
        /// <returns></returns>
        [OperationContract]
        [WebGet(UriTemplate = "/reprocess?job={id}")]
        [CorsEnabled]
        [Description("Reprocess a job.")]
        JobResponse ReprocessJob(Guid id);

        /// <summary>
        /// Return dictionary of int => String for a particular enum type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        /// This service is required because the Microsoft JSON serialiser 
        /// doesn't serialise the String representation; which we typically
        /// want for friendly names etc on our pretty webpages.
        [OperationContract]
        [WebGet(UriTemplate = "/EnumValues?type={type}")]
        [CorsEnabled]
        IDictionary<int, String> EnumValues(String type);

        /// <summary>
        /// Return total message count on coordinator log.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        [OperationContract]
        [WebGet]
        Int32 CoordinatorLogMessageCount();

        /// <summary>
        /// Return messages starting at startindx through to messageCount from coordinator log.
        /// </summary>
        /// <returns></returns>
        [OperationContract, XmlSerializerFormat]
        [WebGet(UriTemplate = "/CoordinatorLog?start={startidx}&count={messagecount}", BodyStyle = WebMessageBodyStyle.WrappedRequest,
                    ResponseFormat = WebMessageFormat.Xml)]
        [CorsEnabled]
        XmlDocument CoordinatorLog(Int32 startIdx = 0, Int32 messageCount = 0);


        /// <summary>
        /// Request for shutdown.
        /// </summary>
        [OperationContract(IsOneWay = true)]
        [WebGet(UriTemplate = "/shutdown?type={shutdownType}&reason={reason}")]
        [Description("Shutdown ( hosts and/or workers ) with options")]
        void Shutdown(ShutdownType shutdownType, String reason);

        /// <summary>
        /// Request for clear cache of all workers.
        /// </summary>
        [OperationContract(IsOneWay = true)]
        [WebGet(UriTemplate = "/clearcache")]
        [Description("Clear cache")]
        void ClearAllWorkersCache();

        /// <summary>
        /// Start Task
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "/starttask?taskname={taskname}")]
        [CorsEnabled]
        [Description("Start Task")]
        void StartTask(String taskName);

        /// <summary>
        /// Stop Task
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "/stoptask?taskname={taskname}")]
        [CorsEnabled]
        [Description("Stop Task")]
        void StopTask(String taskName);

        /// <summary>
        /// Set Task Parameters
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "/settaskparam?taskname={taskname}&paramname={paramname}&paramvalue={paramvalue}")]
        [Description("Sets a task parameter, this setting will persist.")]
        void SetTaskParam(String taskName, String paramName, String paramValue);

        /// <summary>
        /// Get Task Parameters
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "/gettaskparams?taskname={taskname}")]
        [Description("Gets a task parameters.")]
        IDictionary<String, Object> GetTaskParams(String taskName);

        /// <summary>
        /// Set Client Update Tick
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "/setclientupdatetick?timeMs={timems}")]
        [Description("Sets the client update tick in milliseconds.")]
        void SetClientUpdateTick(Int32 timeMs);

        /// <summary>
        /// Set job dispatch hosts
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "/setjobdispatch?jobid={jobId}&host={host}")]
        [Description("Sets the job's dispatch to be be assigned to a particular host.")]
        void SetJobDispatch(Guid jobId, String host);   
    }
   
} // RSG.Pipeline.Automation.Common.Services namespace
