﻿using System;
using System.ServiceModel;

namespace RSG.Pipeline.Automation.Common.Services.Messages
{

    /// <summary>
    /// 
    /// </summary>
    [MessageContract]
    public class TransferResponse
    {
        [MessageBodyMember(Order = 1)]
        public bool Successful { get; set; }
    }

} // RSG.Pipeline.Automation.Common.Services.Messages namespace
