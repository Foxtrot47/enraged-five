﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.SourceControl.Perforce;

namespace RSG.Pipeline.Automation.Common.Tasks
{
    /// <summary>
    /// Interface for tasks that integrate
    /// </summary>
    public interface IIntegratorTask : ITask
    {
        #region Properties
        /// <summary>
        /// Monitored path locations.
        /// </summary>
        IEnumerable<Integration> Integrations { get; }
        #endregion // Properties
    }
}
