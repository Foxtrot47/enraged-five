﻿using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Jobs
{
    /// <summary>
    /// Job Extensions.
    /// - helper methods for jobs.
    /// </summary>
    public static class JobExtensions
    {
        /// <summary>
        /// Log Context
        /// </summary>
        private static readonly String LOG_CTX = "Job";

        /// <summary>
        /// Get the project and branch from a job.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="log"></param>
        /// <param name="job"></param>
        /// <param name="project"></param>
        /// <param name="branch"></param>
        /// <returns>false if not found</returns>
        public static bool GetProjectAndBranch(this IJob job, CommandOptions options, IUniversalLog log, out IProject project, out IBranch branch)
        {
            branch = options.Branch;
            project = branch.Project;

            KeyValuePair<String, IProject> projectKvp = options.Config.AllProjects().Where(p => p.Value.Name.Equals(job.ProjectName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
            if (projectKvp.Equals(default(KeyValuePair<String, IProject>)))
            {
                log.ErrorCtx(LOG_CTX, "Project {0} is unknown : cannot derive project and branch for job {0}", job.ProjectName, job.ID);
                return false;
            }
            project = projectKvp.Value;

            KeyValuePair<String, IBranch> branchKvp = project.Branches.Where(b => b.Key.Equals(job.BranchName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
            if (branchKvp.Equals(default(KeyValuePair<String, IBranch>)))
            {
                log.ErrorCtx(LOG_CTX, "Branch {0} within project {1} is unknown : cannot derive project and branch for job {2}", job.BranchName, job.ProjectName, job.ID);
                return false;
            }
            branch = branchKvp.Value;

            return true;
        }
    }
}
