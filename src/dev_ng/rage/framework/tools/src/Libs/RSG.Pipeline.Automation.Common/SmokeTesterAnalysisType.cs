﻿using System;
using System.Runtime.Serialization;

namespace RSG.Pipeline.Automation.Common
{

    /// <summary>
    /// Automation capability mode enumeration. 
    /// </summary>
    [DataContract(Namespace = "")]
    public enum SmokeTesterAnalysisType
    {
        /// <summary>
        /// Quick performance metrics
        /// </summary>
        [EnumMember]
        QuickMetrics,

        /// <summary>
        /// Exercise MP
        /// </summary>
        [EnumMember]
        MPSmoketest,

        /// <summary>
        /// Update $(build)/$(branch)/non_final/IPL_Loader.xml
        /// </summary>
        [EnumMember]
        MonitorOutput,
    }

} // RSG.Pipeline.Automation.Common namespace
