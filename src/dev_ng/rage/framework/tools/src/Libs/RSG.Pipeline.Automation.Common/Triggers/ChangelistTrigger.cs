﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.SourceControl.Perforce;

namespace RSG.Pipeline.Automation.Common.Triggers
{
    /// <summary>
    /// Trigger that was triggered by a changelist.
    /// </summary>
    [DataContract(Namespace = "")]
    public class ChangelistTrigger :
        FilesTrigger,
        IChangelistTrigger,
        IHasDescription,
        IHasUsername
    {
        #region Properties
        /// <summary>
        /// Changelist number.
        /// </summary>
        [DataMember]
        public uint Changelist { get; private set; }

        /// <summary>
        /// Changelist description.
        /// </summary>
        [DataMember]
        public String Description { get; private set; }

        /// <summary>
        /// Changelist owner.
        /// </summary>
        [DataMember]
        public String Username { get; private set; }

        /// <summary>
        /// Changelist client.
        /// </summary>
        [DataMember]
        public String Client { get; private set; }

        /// <summary>
        /// Monitored Paths of the server to create the trigger
        /// </summary>
        [DataMember]
        public IEnumerable<String> MonitoredPaths { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ChangelistTrigger()
            : base()
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="changelist"></param>
        public ChangelistTrigger(Changelist changelist, Guid triggerId = default(Guid))
            : base(triggerId)
        {
            this.Files = changelist.Files.ToArray();
            this.Changelist = changelist.Number;
            this.Description = changelist.Description.Trim();
            this.Username = changelist.Username;
            this.Client = changelist.Client;
            this.MonitoredPaths = new HashSet<String>();
            this.TriggeredAt = changelist.Time.ToUniversalTime();
        }


        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="changelist"></param>
        public ChangelistTrigger(Changelist changelist, IEnumerable<String> files, Guid triggerId = default(Guid))
            : base(files, triggerId)
        {
            this.Changelist = changelist.Number;
            this.Description = changelist.Description.Trim();
            this.Username = changelist.Username;
            this.Client = changelist.Client;
            this.MonitoredPaths = new HashSet<String>();
            this.TriggeredAt = changelist.Time.ToUniversalTime();
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="changelist"></param>
        public ChangelistTrigger(Changelist changelist, IEnumerable<String> files, IEnumerable<String> syncDirectories, Guid triggerId = default(Guid))
            : base(files, triggerId)
        {
            this.Changelist = changelist.Number;
            this.Description = changelist.Description.Trim();
            this.Username = changelist.Username;
            this.Client = changelist.Client;
            this.TriggeredAt = changelist.Time.ToUniversalTime();
            if (syncDirectories != null)
            {
                this.MonitoredPaths = new HashSet<String>(syncDirectories);
            }
            else
            {
                this.MonitoredPaths = new HashSet<String>();
            }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="changelist"></param>
        public ChangelistTrigger(uint changelist, String description, String client, IEnumerable<String> files, IEnumerable<String> monitoringPaths, String username, Guid triggerId = default(Guid), DateTime triggeredAt = default(DateTime))
            : base(files, triggerId)
        {
            this.Changelist = changelist;
            this.Description = description.Trim();
            this.Username = username;
            this.Client = client;
            this.TriggeredAt = triggeredAt;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise job representation to XML.
        /// </summary>
        /// <returns></returns>
        public override XElement Serialise()
        {
            XElement xmlTrigger = base.Serialise();

            String escapedDescription = RSG.Base.Xml.Utils.XmlEscape(this.Description);

            xmlTrigger.Add(
                new XElement("Changelist", this.Changelist),
                new XElement("Client", this.Client),
                new XElement("Username", this.Username),
                new XElement("Description", escapedDescription)
            );

            if (this.MonitoredPaths.Any())
            {
                xmlTrigger.Add(new XElement("MonitoredPaths", this.MonitoredPaths.Select(mp => new XElement("MonitoredPath", mp))));
            }

            return (xmlTrigger);
        }

        /// <summary>
        /// Deserialise job representation from XML.
        /// </summary>
        /// <param name="xmlTriggerElem"></param>
        /// <returns></returns>
        public override bool Deserialise(XElement xmlTriggerElem)
        {
            bool result = true;
            try
            {
                base.Deserialise(xmlTriggerElem);
                this.Changelist = uint.Parse(xmlTriggerElem.XPathSelectElement("Changelist").Value);
                this.Client = xmlTriggerElem.XPathSelectElement("Client").Value;
                this.Username = xmlTriggerElem.XPathSelectElement("Username").Value;
                String unescapedDescription = xmlTriggerElem.XPathSelectElement("Description").Value;
                this.Description = RSG.Base.Xml.Utils.XmlUnescape(unescapedDescription);

                IEnumerable<String> monitoredPaths = xmlTriggerElem.XPathSelectElements("MonitoredPaths/MonitoredPath").
                    Select(xmlMonitoredPathElem => xmlMonitoredPathElem.Value);

                if (monitoredPaths.Any())
                {
                    this.MonitoredPaths = new HashSet<String>(monitoredPaths);
                }
                else
                {
                    this.MonitoredPaths = new HashSet<String>();
                }
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Error deserialising Changelist Trigger.");
                result = false;
            }
            return (result);
        }
        #endregion // Controller Methods
    }
} // RSG.Pipeline.Automation.Common.Triggers namespace
