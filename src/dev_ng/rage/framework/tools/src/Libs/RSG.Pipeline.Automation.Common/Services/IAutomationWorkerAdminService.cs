﻿using System;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Web;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Services.Messages;

namespace RSG.Pipeline.Automation.Common.Services
{

    /// <summary>
    /// Automation Worker Administration Service.
    /// </summary>
    [ServiceContract]
    public interface IAutomationWorkerAdminService
    {
        /// <summary>
        /// 
        /// </summary>
        [OperationContract(IsOneWay = true)]
        [WebGet(UriTemplate = "/shutdown?type={shutdownType}")]
        [Description("Shuts down a worker, with options")]
        void Shutdown(ShutdownType shutdownType);

        /// <summary>
        /// 
        /// </summary>
        [OperationContract(IsOneWay = true)]
        [WebGet(UriTemplate = "/clearcache")]
        [Description("Clears a worker's cache")]
        void FlagClearCache();
    }

} // RSG.Pipeline.Automation.Common.Services namespace
