﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Pipeline.Core;
using RSG.SourceControl.Perforce;

namespace RSG.Pipeline.Automation.Common.Jobs
{
    /// <summary>
    /// Integrator client side job.
    /// </summary>
    [DataContract]
    public class IntegratorJob :
        Job,
        IJob
    {
        #region Properties
        [DataMember]
        public Integration Integration { get; set; }

        [DataMember]
        public int SourceChangelist { get; set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Default constructor (for deserialisation).
        /// </summary>
        public IntegratorJob()
            : base()
        {
            Role = CapabilityType.Integrator;
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public IntegratorJob(Changelist change, Integration integration, IBranch branch, Guid triggerId)
            : base(branch, CapabilityType.Integrator, change, triggerId)
        {
            Integration = integration;
            SourceChangelist = (int)change.Number;
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// Serialise job representation to XML.
        /// </summary>
        /// <returns></returns>
        public override XElement Serialise()
        {
            XElement xmlJob = base.Serialise();
            xmlJob.Add(this.Integration.Serialise());
            xmlJob.Add(new XElement("SourceChangelist", this.SourceChangelist));
            return (xmlJob);
        }

        /// <summary>
        /// Deserialise job representation from XML.
        /// </summary>
        /// <param name="xmlJobElem"></param>
        /// <returns></returns>
        public override bool Deserialise(XElement xmlJobElem)
        {
            bool result = true;
            try
            {
                result &= base.Deserialise(xmlJobElem);

                this.Integration = new Integration();
                result &= this.Integration.Deserialise(xmlJobElem.XPathSelectElement("Integration"));
                this.SourceChangelist = int.Parse(xmlJobElem.XPathSelectElement("SourceChangelist").Value);
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Error deserialising Integration Job.");
                result = false;
            }
            return (result);
        }
        #endregion // Controller Methods
    }
}
