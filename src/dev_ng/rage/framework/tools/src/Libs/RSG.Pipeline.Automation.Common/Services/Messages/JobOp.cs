﻿using System;
using System.Runtime.Serialization;

namespace RSG.Pipeline.Automation.Common.Services.Messages
{

    /// <summary>
    /// Enum defining available job operations.
    /// </summary>
    [DataContract(Namespace = "")]
    public enum JobOp
    {
        /// <summary>
        /// Skip job operation.
        /// </summary>
        [EnumMember]
        Skip,

        /// <summary>
        /// Prioritise job operation.
        /// </summary>
        [EnumMember]
        Prioritise,

        /// <summary>
        /// Reprocess job operation.
        /// </summary>
        [EnumMember]
        Reprocess,
    }

} // RSG.Pipeline.Automation.Common.Services.Messages namespace
