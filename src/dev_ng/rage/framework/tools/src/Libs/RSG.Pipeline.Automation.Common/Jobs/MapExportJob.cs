﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Configuration;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Automation.Common.Export;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;
using RSG.Pipeline.Content.Algorithm;
using RSG.Pipeline.Services;
using RSG.Platform;
using RSG.SceneXml;
using RSG.SceneXml.Material;

namespace RSG.Pipeline.Automation.Common.Jobs
{

    /// <summary>
    /// Map exporting client-side Job class.
    /// </summary>
    /// Note: you shouldn't normally have to create custom job subclasses for
    /// every type.  You must use the role/CapabilityType field correctly though
    /// when your Job's are constructed for the right JobProcessor to be selected
    /// on the client.
    /// 
    /// This class splits up the maps into separate Export Process objects;
    /// as they will be exported by the client workers individually.
    /// 
    [DataContract]
    public class MapExportJob :
        Job,
        IJob
    {
        #region Properties
        /// <summary>
        /// Collection of map export information.
        /// </summary>
        [DataMember]
        public ICollection<ExportProcess> ExportProcesses { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor (for deserialisation).
        /// </summary>
        public MapExportJob()
        {
        }

        /// <summary>
        /// Constructor; specifying AP3 content-tree for map data lookup.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="tree"></param>
        /// <param name="dccFilenames"></param>
        /// <param name="additionalExportFiles"></param>
        /// <param name="editedSourceTextures"></param>
        /// <param name="network"></param>
        internal MapExportJob(IBranch branch, IContentTree tree, IEnumerable<String> dccFilenames, 
            IEnumerable<String> additionalExportFiles, IEnumerable<String> editedSourceTextures, bool network)
            : base(branch, network ? CapabilityType.MapExportNetwork : CapabilityType.MapExport)
        {
            this.Init(branch, tree, dccFilenames, additionalExportFiles, editedSourceTextures);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise job representation to XML.
        /// </summary>
        /// <returns></returns>
        public override XElement Serialise()
        {
            XElement xmlJob = base.Serialise();
            xmlJob.Add(
                new XElement("ExportProcesses", 
                    this.ExportProcesses.Select(p => p.Serialise()))
            );
            return (xmlJob);
        }

        /// <summary>
        /// Deserialise job representation from XML.
        /// </summary>
        /// <param name="xmlJobElem"></param>
        /// <returns></returns>
        public override bool Deserialise(XElement xmlJobElem)
        {
            bool result = true;
            try
            {
                base.Deserialise(xmlJobElem);
                this.ExportProcesses = new List<ExportProcess>();
                IEnumerable<XElement> processes = xmlJobElem.XPathSelectElements(
                    "ExportProcesses/ExportProcess");
                foreach (XElement xmlExportProcess in processes)
                {
                    ExportProcess process = new ExportProcess();
                    if (process.Deserialise(xmlExportProcess))
                        this.ExportProcesses.Add(process);
                }
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Error deserialising Map Export Job.");
                result = false;
            }
            return (result);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Initilaisation; from AP3 content-tree.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="tree"></param>
        /// <param name="dccFilenames"></param>
        /// <param name="additionalExportFiles"></param>
        /// <param name="editedSourceTextures"></param>
        private void Init(IBranch branch, IContentTree tree, IEnumerable<String> dccFilenames, 
            IEnumerable<String> additionalExportFiles, IEnumerable<String> editedSourceTextures)
        {
            this.ExportProcesses = new List<ExportProcess>();

            // For each dcc filename we hope to create an ExportProcess object;
            // depending on how the node is configured in the content-tree.
            foreach (String mapDccFilename in dccFilenames)
            {
                Debug.Assert(mapDccFilename.EndsWith(".max") || mapDccFilename.EndsWith(".maxc"),
                    "Invalid Map DCC Source Filename.");
                if (!tree.IsFile(mapDccFilename))
                {
                    Log.Log__Warning("DCC Source File: {0} is not a max file.  Ignoring.",
                        mapDccFilename);
                    continue;
                }

                IContentNode mapNode = tree.CreateFile(mapDccFilename);
                IEnumerable<IProcess> processes = tree.FindProcessesWithInput(mapNode);
                IProcess exportProcess = processes.FirstOrDefault();
                if (null == exportProcess)
                {
                    Log.Log__Warning("DCC Source File: {0} is not a map.  Ignoring.",
                        mapDccFilename);
                    continue;
                }

                bool autoExportMap = false;
                if (mapNode.Parameters.ContainsKey(Constants.Map_AutoExport))
                    autoExportMap = (bool)mapNode.Parameters[Constants.Map_AutoExport];
                if (this.Role.Equals(CapabilityType.MapExport) && !autoExportMap)
                {                
                    Log.Log__Warning("DCC Source File: {0} is not set to auto-export.  Ignoring.",
                        mapDccFilename);
                    continue;
                }

                ICollection<String> exportFiles = new HashSet<String>();
                exportFiles.AddRange(exportProcess.Outputs.Select(output =>
                     (output as Content.File).AbsolutePath));
                exportFiles.AddRange(additionalExportFiles);

                // Only export for the platforms the user has enabled themselves
                IEnumerable<Platform.Platform> overridePlatforms = RSG.Pipeline.Services.Platform.PlatformProcessBuilder.GetInstallerEnabledPlatforms(branch);
                Log.Log__Message("Platforms enabled for network export job: {0}", String.Join(", ", overridePlatforms.Select(p => p.ToString())));

                if (!overridePlatforms.Any())
                {
                    Log.Log__Error(String.Format("No Platforms are enabled for this branch({0})!  MapExportJob creation skipped.", branch.Name));
                    return;
                }

                ExportProcess process = new ExportProcess(mapDccFilename,
                    exportFiles, editedSourceTextures, overridePlatforms);

                this.ExportProcesses.Add(process);
            }
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.Common.Jobs namespace
