﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RSG.Base.Collections;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common.Client;
using RSG.Pipeline.Services;

namespace RSG.Pipeline.Automation.Common.Notifications.Alerts
{
    /// <summary>
    /// 
    /// </summary>
    public class GeneralAlertNotification
        : AlertNotificationBase
    {
        #region Constructors
        /// <summary>
        /// Parameterised constructor
        /// </summary>
        /// <param name="job"></param>
        /// <param name="maxErrors"></param>
        public GeneralAlertNotification(ILog log, CommandOptions options, string summary = null, IEnumerable<string> detail = null)
            : base(log, summary, detail, options)
        {
        }
        #endregion // Constructors

        #region Controller Methods

        /// <summary>
        /// Override for setting the subject
        /// </summary>
        public override void SetSubject()
        {
            this.Subject = String.Format("[{1} {2}: Automation Alert] {0}", base.Summary, Options.CoreProject.FriendlyName, Options.Branch.Name);
        }

        /// <summary>
        /// Override for setting the body
        /// </summary>
        public override void SetBody()
        {
            // the first line of the body can be seen in systray notifications : make it as informative as possible.
            this.Body.AppendLine(" ");
            this.Body.AppendLine("*** GENERAL ALERT ***");
            this.Body.AppendLine(" ");
        }
        #endregion // Controller Methods
    }
}
