﻿using RSG.Base.Configuration;
using RSG.Base.Configuration.Bugstar;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Interop.Bugstar;
using RSG.Interop.Bugstar.Organisation;
using RSG.Pipeline.Automation.Common.Bug;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Notifications
{
    /// <summary>
    /// Notifies Bugstar about bugs.
    /// </summary>
    public sealed class BugNotification
        : IBugNotification
    {
        #region Constants
        /// <summary>
        /// Log context.
        /// </summary>
        protected static readonly String LOG_CTX = "Bug Notification";
        #endregion // Constants

        #region Properties

        /// <summary>
        /// Log object.
        /// </summary>
        public IUniversalLog Log
        {
            get;
            private set;
        }

        /// <summary>
        /// Job
        /// </summary>
        public IJob Job
        {
            get;
            private set;
        }

        /// <summary>
        /// Job results
        /// </summary>
        public IEnumerable<IJobResult> JobResults
        {
            get;
            private set;
        }
        
        /// <summary>
        /// Worker Uri
        /// </summary>
        public Uri WorkerUri
        {
            get;
            private set;
        }

        /// <summary>
        /// Worker Guid
        /// </summary>
        public Guid WorkerGuid 
        {
            get;
            private set;
        }
        
        /// <summary>
        /// Log filename
        /// </summary>   
        public String LogFilename
        {
            get;
            private set;
        }

        // Command options
        public CommandOptions Options
        {
            get;
            private set;
        }

        /// <summary>
        /// Bugstar connection
        /// </summary>
        public BugstarConnection BugstarConnection
        {
            get;
            private set;
        }

        /// <summary>
        /// Bugstar config
        /// </summary>
        public IBugstarConfig BugstarConfig
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="log"></param>
        /// <param name="job"></param>
        /// <param name="jobResults"></param>
        /// <param name="workerUri"></param>
        /// <param name="workerGuid"></param>
        /// <param name="logFilename"></param>
        /// <param name="options"></param>
        /// <param name="connection"></param>
        /// <param name="bugstarConfig"></param>
        public BugNotification(IUniversalLog log, IJob job, IEnumerable<IJobResult> jobResults, Uri workerUri, Guid workerGuid, String logFilename, CommandOptions options, BugstarConnection connection, IBugstarConfig bugstarConfig)
        {
            this.Log = log;
            this.Job = job;
            this.JobResults = jobResults;
            this.WorkerUri = workerUri;
            this.WorkerGuid = workerGuid;
            this.LogFilename = logFilename;
            this.Options = options;
            this.BugstarConnection = connection;
            this.BugstarConfig = bugstarConfig;
        }
        #endregion // Constructors

        #region Public Methods
        /// <summary>
        /// Create bugs ( if required ) for the following recipients
        /// </summary>
        /// <param name="bugRecipients"></param>
        /// <returns></returns>
        public IEnumerable<uint> Create(IEnumerable<IBugRecipient> bugRecipients)
        {
            lock (bugRecipients)
            {
                ICollection<uint> bugIdsCreated = new List<uint>();
                try
                {
                    foreach (IBugRecipient bugRecipient in bugRecipients.Where(br => br.BugRequired(Job)))
                    {
                        uint bugId = CreateBug(bugRecipient);

                        if (bugId > 0)
                        {
                            bugIdsCreated.Add(bugId);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.ExceptionCtx(LOG_CTX, ex, "Bug Notification exception");
                }
                return bugIdsCreated;
            }
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// Creates a bugstar bug for the given recipient.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="task"></param>
        /// <param name="job"></param>
        /// <param name="jobResults"></param>
        /// <param name="workerUri"></param>
        /// <param name="workerGuid"></param>
        /// <param name="bugRecipient"></param>
        /// <returns>bug id that was created.</returns>
        private uint CreateBug(IBugRecipient bugRecipient)
        {
            uint bugId = 0;

            try
            {
                RSG.Interop.Bugstar.Organisation.Project bugstarProject = RSG.Interop.Bugstar.Organisation.Project.GetProjectById(BugstarConnection, BugstarConfig.ProjectId);
                IEnumerable<User> allUsers = bugstarProject.GetUsersList();         
                
                String username = String.Empty;
                StringBuilder description = CreateBugDescription(ref username);

                if (String.IsNullOrEmpty(username))
                {
                    this.Log.MessageCtx(LOG_CTX, "Bug not created, no username.");
                    return 0;
                }
                
                User jobDeveloper = CreateBugOwner(bugRecipient, username, allUsers);
                if (jobDeveloper == null)
                {
                    return 0;
                }

                User qa = CreateBugQa(bugRecipient, username, allUsers, jobDeveloper);
                User bugReviewer = CreateBugReviewer(bugRecipient, username, allUsers);
                Component bugComponent = CreateBugComponent(bugRecipient, bugstarProject);

                BugBuilder bugBuilder = new BugBuilder(bugstarProject);

                IBranch branch = this.Options.Branch;
                IProject project = branch.Project;
                if (!this.Job.GetProjectAndBranch(this.Options, this.Log, out project, out branch))
                {
                    this.Log.WarningCtx(LOG_CTX, "Couldn't find the project and branch in the job for the bug creation");
                }

                IEnumerable<String> triggeringFiles = Job.Trigger.TriggeringFiles();
                bugBuilder.Summary = String.Format("[{0} {1}: {2}] {3} | Triggered by {4} (#1 of {5} files)", project.FriendlyName, branch.Name, Job.Role, Job.State, triggeringFiles.FirstOrDefault(), triggeringFiles.Count());
                bugBuilder.Description = description.ToString();
                bugBuilder.Category = BugCategoryUtils.FromBugstarString(bugRecipient.Class);
                bugBuilder.Priority = BugPriorityUtils.FromBugstarString(bugRecipient.Priority);
                bugBuilder.State = BugState.Dev;
                bugBuilder.EstimatedTime = bugRecipient.EstimatedTime;

                foreach (String ccName in bugRecipient.CcList) 
                {
                    User cc = CreateBugCc(ccName, username, allUsers);
                    if (cc != null)
                    {
                        bugBuilder.CCList.Add(cc.Id);
                    }
                }

                bugBuilder.DeveloperId = jobDeveloper.Id;
                bugBuilder.TesterId = qa.Id;

                if (null != bugReviewer)
                    bugBuilder.ReviewerId = bugReviewer.Id;

                if (null != bugComponent)
                    bugBuilder.ComponentId = bugComponent.Id;

                bugBuilder.Tags.Add(bugRecipient.Tag);

                Attachment logAttachment = CreateBugAttachment(bugstarProject, bugBuilder);

                // Create bug and attachment objects; submit to server.
                try
                {
                    RSG.Interop.Bugstar.Bug bug = bugBuilder.ToBug(BugstarConnection);
                    if (null != logAttachment)
                        RSG.Interop.Bugstar.Bug.Attach(bug, logAttachment);
                    this.Log.MessageCtx(LOG_CTX, "Bug {0} created.", bug.Id);

                    bugId = bug.Id;
                }
                catch (Exception ex)
                {
                    this.Log.ToolExceptionCtx(LOG_CTX, ex, "Bug submit failed.");
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolExceptionCtx(LOG_CTX, ex, "Failed to construct bug.");
            }

            return bugId;
        }

        /// <summary>
        /// Creates the bug attachment
        /// </summary>
        /// <param name="bugstarProject"></param>
        /// <param name="bugBuilder"></param>
        /// <returns></returns>
        private Attachment CreateBugAttachment(RSG.Interop.Bugstar.Organisation.Project bugstarProject, BugBuilder bugBuilder)
        {
            // Get ready for log attachment; do this here so we can add a comment if
            // the log file doesn't exist.
            Attachment logAttachment = null;
            if (System.IO.File.Exists(LogFilename))
            {
                AttachmentBuilder logAttachmentBuilder = new AttachmentBuilder(bugstarProject);
                logAttachment = logAttachmentBuilder.ToAttachment(BugstarConnection, LogFilename);
                bugBuilder.Comments.Add("See attached ulog for details.");
            }
            else
            {
                bugBuilder.Comments.Add(
                    String.Format("Failed to attach log: {0} (doesn't exist).", LogFilename));
                this.Log.WarningCtx("Bug Creation", "Failed to attach log: {0} (doesn't exist).",
                    LogFilename);
            }

            return logAttachment;
        }

        /// <summary>
        /// Create the bug description and also creates the username that triggered the job.
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        private StringBuilder CreateBugDescription(ref String username)
        {
            IEnumerable<uint> changelistNumbers = Job.Trigger.ChangelistNumbers();
            IEnumerable<String> filesTriggering = Job.Trigger.TriggeringFiles();
            IEnumerable<String> usernames = Job.Trigger.Usernames();
            IEnumerable<DateTime> triggeredAtDateTimes = Job.Trigger.TriggeredAt();

            StringBuilder description = new StringBuilder();
            description.AppendFormat("Project (Branch): {0} ({1})\n", this.Options.CoreProject.FriendlyName, this.Options.Branch.Name);
            description.AppendFormat("Job {0}\n", Job.ID);

            if (triggeredAtDateTimes.Any())
            {
                if (triggeredAtDateTimes.HasAtLeast(2))
                {
                    description.AppendFormat(String.Format("Triggered between: {0} to {1}\n", triggeredAtDateTimes.First(), triggeredAtDateTimes.Last()));
                }
                else
                {
                    description.AppendFormat(String.Format("Triggered at: {0}\n", triggeredAtDateTimes.Last()));
                }
            }            
            
            description.AppendFormat("Processed at: {0}\n", Job.ProcessedAt);
            description.AppendFormat("Processing time: {0:hh\\:mm\\:ss}\n", Job.ProcessingTime);
            description.AppendFormat("Processed By (GUID): {0}\n", WorkerUri);
            description.AppendFormat("Processed By (URI): {0}\n", WorkerGuid);
            description.AppendFormat("Automation server: {0}\n", System.Environment.MachineName);
            description.AppendLine();

            username = usernames.Last();
            if (changelistNumbers.Any())
            {
                description.AppendFormat("Changelists: {0}\n", String.Join(",", changelistNumbers));
            }

            if (filesTriggering.Any())
            {
                description.AppendFormat("Files:\n{0}", String.Join("\n", filesTriggering) + "\n");
            }

            description.AppendLine();
            return description;
        }

        /// <summary>
        /// Create the bug developer/owner
        /// </summary>
        /// <param name="bugRecipient"></param>
        /// <param name="username"></param>
        /// <param name="allUsers"></param>
        /// <returns></returns>
        private User CreateBugOwner(IBugRecipient bugRecipient, String username, IEnumerable<User> allUsers)
        {
            User jobDeveloper = null;
            // Developer (NOT optional) 
            if (!String.IsNullOrEmpty(bugRecipient.Owner) && ((bugRecipient.Owner.Equals("$owner") || bugRecipient.Owner.Equals("$(owner)"))))
            {
                jobDeveloper = allUsers.Where(user => 0 == String.Compare(user.Email, username, true)).FirstOrDefault();
                if (jobDeveloper == null)
                {
                    this.Log.WarningCtx("Bug Creation", "Couldn't create a bug for developer {0}, no bug was created.", bugRecipient.Owner);
                }
            }
            else
            {
                jobDeveloper = allUsers.Where(user => 0 == String.Compare(user.Email, bugRecipient.Owner, true)).FirstOrDefault();
                if (jobDeveloper == null)
                {
                    this.Log.WarningCtx("Bug Creation", "Couldn't create a bug for developer with specified name of {0}, no bug was created.", bugRecipient.Owner);
                }
            }
            return jobDeveloper;
        }

        /// <summary>
        /// Create the bug QA
        /// - No QA specified? - bug QA is set to go to the developer.
        /// </summary>
        /// <param name="bugRecipient"></param>
        /// <param name="username"></param>
        /// <param name="allUsers"></param>
        /// <param name="jobDeveloper"></param>
        /// <returns></returns>
        private User CreateBugQa(IBugRecipient bugRecipient, String username, IEnumerable<User> allUsers, User jobDeveloper)
        {
            User qa = jobDeveloper; // default is the developer.
            if (!String.IsNullOrEmpty(bugRecipient.QA))
            {
                if ((bugRecipient.QA.Equals("$owner") || bugRecipient.QA.Equals("$(owner)")))
                {
                    qa = allUsers.Where(user => 0 == String.Compare(user.Email, username, true)).FirstOrDefault();
                    if (qa == null)
                    {
                        this.Log.WarningCtx("Bug Creation", "Couldn't create a bug for developer {0}, no bug was created.", username);
                    }
                }
                else
                {
                    qa = allUsers.Where(user => 0 == String.Compare(user.Email, bugRecipient.QA, true)).FirstOrDefault();
                }

                if (qa == null)
                {
                    this.Log.WarningCtx("Bug Creation", "Couldn't create the QA for user {0}, this bug was created with QA of the developer : {1}", bugRecipient.QA, username);
                }
            }

            return qa;
        }

        /// <summary>
        /// Create the bug CC for a user
        /// </summary>
        /// <param name="bugRecipient"></param>
        /// <param name="username"></param>
        /// <param name="allUsers"></param>
        /// <param name="jobDeveloper"></param>
        /// <returns></returns>
        private User CreateBugCc(String ccName, String username, IEnumerable<User> allUsers)
        {
            User cc = null;
            if (!String.IsNullOrEmpty(ccName))
            {
                if ((ccName.Equals("$owner") || ccName.Equals("$(owner)")))
                {
                    cc = allUsers.Where(user => 0 == String.Compare(user.Email, username, true)).FirstOrDefault();
                    if (cc == null)
                    {
                        this.Log.WarningCtx("Bug Creation", "Couldn't create a cc for developer {0}.", username);
                    }
                }
                else
                {
                    cc = allUsers.Where(user => 0 == String.Compare(user.Email, ccName, true)).FirstOrDefault();
                }

                if (cc == null)
                {
                    this.Log.WarningCtx("Bug Creation", "Couldn't create the CC for user {0}, this bug was created with CC of the developer : {1}", ccName, username);
                }
            }

            return cc;
        }


        /// <summary>
        /// Create the bug reviewer.
        /// - optional, bugs can be created without a reviewer.
        /// </summary>
        /// <param name="bugRecipient"></param>
        /// <param name="username"></param>
        /// <param name="allUsers"></param>
        /// <returns></returns>
        private User CreateBugReviewer(IBugRecipient bugRecipient, String username, IEnumerable<User> allUsers)
        {
            // Reviewer (optional)
            User bugReviewer = null;
            if (!String.IsNullOrEmpty(bugRecipient.Reviewer) && (bugRecipient.Reviewer.Equals("$owner") || bugRecipient.Reviewer.Equals("$(owner)")))
            {
                bugReviewer = allUsers.Where(user => 0 == String.Compare(user.Email, username, true)).FirstOrDefault();
            }
            else if (!String.IsNullOrEmpty(bugRecipient.Reviewer))
            {
                IEnumerable<User> users = allUsers.Where(user => 0 == String.Compare(user.Email, bugRecipient.Reviewer, true));
                if (users.Count() > 0)
                {
                    bugReviewer = users.First();
                }
                else
                {
                    this.Log.WarningCtx("Bug Creation", "Couldn't find the bug reviewer {0}, the bug was still created, albeit without a reviewer.", bugRecipient.Reviewer);
                }
            }
            return bugReviewer;
        }

        /// <summary>
        /// Create the bug component 
        /// - eg. tools, art etc...
        /// </summary>
        /// <param name="bugRecipient"></param>
        /// <param name="bugstarProject"></param>
        /// <returns></returns>
        private static Component CreateBugComponent(IBugRecipient bugRecipient, RSG.Interop.Bugstar.Organisation.Project bugstarProject)
        {
            Component bugComponent = null;
            if (!String.IsNullOrEmpty(bugRecipient.Component))
            {
                IEnumerable<Component> components = bugstarProject.Components;
                bugComponent = components.Where(component => 0 == String.Compare(component.FullName, bugRecipient.Component, true)).FirstOrDefault();
            }
            return bugComponent;
        }
        #endregion // Private Methods
    }
}
