﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Notifications
{
    public class EmailRedirect
    {
        #region Properties
        /// <summary>
        /// Friendly name for email recipient. 
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Email address of recipient. 
        /// </summary>
        public String Email { get; set; }

        public EmailRedirect(string email, string name)
        {
            this.Name = name;
            this.Email = email;
        }
        #endregion
    }
}
