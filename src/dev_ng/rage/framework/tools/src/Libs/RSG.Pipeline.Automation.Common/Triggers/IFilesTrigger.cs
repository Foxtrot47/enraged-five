﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Triggers
{
    /// <summary>
    /// Trigger that was triggered by files.
    /// </summary>
    public interface IFilesTrigger : 
        ITrigger
    {
        #region Properties
        /// <summary>
        /// Set of files causing the trigger.
        /// </summary>
        String[] Files { get; }
        #endregion // Properties
    }
}
