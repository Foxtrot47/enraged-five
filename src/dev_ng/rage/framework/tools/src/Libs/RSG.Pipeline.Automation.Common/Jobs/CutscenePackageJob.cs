﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;
using RSG.Pipeline.Core;
using RSG.SourceControl.Perforce;

namespace RSG.Pipeline.Automation.Common.Jobs
{
    /// <summary>
    /// Asset builder client side job.
    /// </summary>
    [DataContract]
    public class CutscenePackageJob :
        Job,
        IJob
    {
        #region Properties
        /// <summary>
        /// Engine flags 
        /// </summary>
        [DataMember]
        public EngineFlags EngineFlags { get; set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Default constructor (for deserialisation).
        /// </summary>
        public CutscenePackageJob()
            : base(CapabilityType.CutscenePackage)
        {
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CutscenePackageJob(Changelist change)
            : base(CapabilityType.CutscenePackage, change)
        {
            EngineFlags = EngineFlags.Default;
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CutscenePackageJob(Changelist change, IEnumerable<String> pathnamesToProcess)
            : base(CapabilityType.CutscenePackage, change, pathnamesToProcess)
        {
            EngineFlags = EngineFlags.Default;
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// Serialise job representation to XML.
        /// </summary>
        /// <returns></returns>
        public override XElement Serialise()
        {
            XElement xmlJob = base.Serialise();
            xmlJob.Add(new XElement("EngineFlags", this.EngineFlags));

            return (xmlJob);
        }

        /// <summary>
        /// Deserialise job representation from XML.
        /// </summary>
        /// <param name="xmlJobElem"></param>
        /// <returns></returns>
        public override bool Deserialise(XElement xmlJobElem)
        {
            bool result = true;
            try
            {
                result &= base.Deserialise(xmlJobElem);
                this.EngineFlags = (EngineFlags)Enum.Parse(typeof(EngineFlags),
                    xmlJobElem.XPathSelectElement("EngineFlags").Value);
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Error deserialising Asset Builder Job.");
                result = false;
            }
            return (result);
        }
        #endregion // Controller Methods
    }
}
