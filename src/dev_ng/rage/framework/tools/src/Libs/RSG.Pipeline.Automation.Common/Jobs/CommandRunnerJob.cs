﻿using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Pipeline.Automation.Common.Jobs.JobData;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.SourceControl.Perforce;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Pipeline.Automation.Common.Jobs
{
    [DataContract]
    public class CommandRunnerJob :
        Job,
        IJob
    {
        /// <summary>
        /// List on main build commands to be run. 
        /// </summary>
        [DataMember]
        public IEnumerable<CommandRunnerJobInfo> Commands { get; private set; }

        /// <summary>
        /// Name of the task. This is to help with identifying the task
        /// </summary>
        [DataMember]
        public String CommandName { get; private set; }

        /// <summary>
        /// Indicates if we should email out after the job
        /// </summary>
        [DataMember]
        public bool RequiresPostJobNotification { get; private set; }

        /// <summary>
        /// Directory in which files are written as a result of running the build. 
        /// - also known as Artefacts
        /// </summary>
        [DataMember]
        public String TargetDir { get; private set; }

        /// <summary>
        /// Used to filter out files when scanning the target directory
        /// </summary>
        [DataMember]
        public String TargetRegex { get; private set; }

        /// <summary>
        /// Directory in which files that were marked for upload are published ( copied and optionally checked in )
        /// - files for upload are copied from the upload dir to the publish dir
        /// - if the publish dir is mapped in p4 the files are submitted.
        /// - publish dir can be the same as the upload dir.
        /// </summary>
        [DataMember]
        public String PublishDir { get; private set; }

        [DataMember]
        public int SkipConsume { get; private set; }

        /// <summary>
        /// Default constructor (for WCF deserialisation only).
        /// </summary>
        public CommandRunnerJob()
            : base()
        {
            Role = CapabilityType.CommandRunner;
        }

        public CommandRunnerJob(String commandName, IBranch branch)
            : base(branch, CapabilityType.CommandRunner)
        {
            CommandName = commandName;
            Commands = new List<CommandRunnerJobInfo>();
            RequiresPostJobNotification = true;
        }

        public CommandRunnerJob(String commandName, IBranch branch, Changelist change, int skipConsume,  IEnumerable<CommandRunnerJobInfo> commands, IEnumerable<String> pathnamesToProcess, IEnumerable<String> monitoringPaths = null, IEnumerable<String> prebuildCommands = null, IEnumerable<String> postbuildCommands = null, bool requiresPostJobNotification = true, String targetDir = "", String publishDir = "", String targetRegex = "", Guid triggerId = default(Guid), DateTime triggeredAt = default(DateTime))
            : base(branch, CapabilityType.CommandRunner, change, pathnamesToProcess, monitoringPaths, prebuildCommands, postbuildCommands)
        {
            Commands = commands;
            CommandName = commandName;
            RequiresPostJobNotification = requiresPostJobNotification;
            TargetDir = Environment.ExpandEnvironmentVariables(targetDir);
            PublishDir = Environment.ExpandEnvironmentVariables(publishDir);
            TargetRegex = targetRegex;
            SkipConsume = skipConsume;
        }

        public CommandRunnerJob(String commandName, IBranch branch, IEnumerable<ITrigger> triggers, IEnumerable<CommandRunnerJobInfo> commands, IEnumerable<String> prebuildCommands = null, IEnumerable<String> postbuildCommands = null, bool requiresPostJobNotification = true, String targetDir = "", String publishDir = "", String targetRegex = "", Guid triggerId = default(Guid), DateTime triggeredAt = default(DateTime))
            : base(branch, CapabilityType.CommandRunner, triggers, prebuildCommands, postbuildCommands,null, triggerId, triggeredAt)
        {
            Commands = commands;
            CommandName = commandName;
            RequiresPostJobNotification = requiresPostJobNotification;
            TargetDir = Environment.ExpandEnvironmentVariables(targetDir);
            PublishDir = Environment.ExpandEnvironmentVariables(publishDir);
            TargetRegex = targetRegex;
            SkipConsume = 0;
        }


        public CommandRunnerJob(CapabilityType capabilityType, DateTime completedAt, Guid consumedBy, DateTime createdAt, Guid id, IEnumerable<string> postbuildCommands,
            IEnumerable<string> prebuildCommands, JobPriority jobPriority, DateTime processedAt, string processingHost, TimeSpan processingTime,
            JobState jobState, ITrigger trigger, object userData, IEnumerable<CommandRunnerJobInfo> commands, String commandName,
            bool requiresPostJobNotification, String branchName, String targetDir, String targetRegex, String publishDir)
            : base(capabilityType, completedAt, consumedBy, createdAt, id, postbuildCommands, prebuildCommands, jobPriority, processedAt, processingHost,
                    processingTime, jobState, trigger, userData)
        {
            Commands = commands;
            CommandName = commandName;
            RequiresPostJobNotification = requiresPostJobNotification;
            BranchName = branchName;
            TargetDir = targetDir;
            TargetRegex = targetRegex;
            PublishDir = publishDir;
        }

        public override XElement Serialise()
        {
            XElement xmlJobElem = base.Serialise();
            xmlJobElem.Add(new XElement("Commands", this.Commands.Select(c => c.Serialise())));
            xmlJobElem.Add(new XElement("Name", CommandName));
            xmlJobElem.Add(new XElement("PublishDir", PublishDir));
            xmlJobElem.Add(new XElement("TargetDir", TargetDir));
            xmlJobElem.Add(new XElement("SkipConsume", SkipConsume));

            return xmlJobElem;
        }

        /// <summary>
        /// Deserialise job representation from XML.
        /// </summary>
        /// <param name="xmlJobElem"></param>
        /// <returns></returns>
        public override bool Deserialise(XElement xmlJobElem)
        {
            bool result = true;
            try
            {
                result &= base.Deserialise(xmlJobElem);

                IEnumerable<XElement> commandElements = xmlJobElem.XPathSelectElements("Commands/CommandInfo");
                this.Commands = new List<CommandRunnerJobInfo>();

                if (commandElements.Any())
                {
                    foreach (XElement infoElem in commandElements)
                    {
                        CommandRunnerJobInfo info = new CommandRunnerJobInfo();
                        info.Deserialize(infoElem);
                    }
                }

                CommandName = xmlJobElem.Element("Name").Value;

                XElement publishDirElem = xmlJobElem.Element("PublishDir");
                if(publishDirElem != null)
                {
                    PublishDir = publishDirElem.Value;
                }

                XElement targetDirElem = xmlJobElem.Element("TargetDir");
                if (targetDirElem != null)
                {
                    TargetDir = targetDirElem.Value;
                }

                XElement skipConsumeElem = xmlJobElem.Element("SkipConsume");
                int skipConsume = 0;
                if (skipConsumeElem != null)
                {
                    int.TryParse(skipConsumeElem.Value, out skipConsume);
                }

                SkipConsume = skipConsume;
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Error deserialising Command Runner Job.");
                result = false;
            }
            return (result);
        }

        /// <summary>
        /// Jobs don't normally change but here it is transformed when consumed by another job.
        /// </summary>
        /// <param name="jobConsuming">the job consuming this job</param>
        public void SetConsumed(IJob jobConsuming)
        {
            // Hold a 'guid reference' to the consuming job
            this.ConsumedByJobID = jobConsuming.ID;

            // Change state to no longer be pending but is now consumed.
            this.State = JobState.SkippedConsumed;

            // The skip setting of the job is set to an enum value that indicates it has now been consumed.
            this.SkipConsume = -2;
        }
    }
}
