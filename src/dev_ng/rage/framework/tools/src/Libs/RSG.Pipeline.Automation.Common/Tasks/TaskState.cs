﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Tasks
{
    /// <summary>
    /// The possible states of a task
    /// </summary>
    /// 
    [DataContract]
    public enum TaskState
    {
        [EnumMember]
        Unknown,
        [EnumMember]
        Inactive,
        [EnumMember]
        Active
    }
}
