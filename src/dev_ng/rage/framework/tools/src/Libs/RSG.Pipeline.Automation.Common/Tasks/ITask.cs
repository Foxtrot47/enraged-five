﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Client;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Bug;
using RSG.Pipeline.Automation.Common.Notifications;
using RSG.Pipeline.Automation.Common.Services.Messages;

namespace RSG.Pipeline.Automation.Common.Tasks
{

    /// <summary>
    /// Interface for server-side tasks; tasks produce jobs based on certain
    /// criteria, e.g. monitoring a source control system or creating a job at
    /// a specific time interval.
    /// </summary>
    public interface ITask
    {
        #region Properties
        /// <summary>
        /// Task name string.
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Task description string.
        /// </summary>
        String Description { get; }

        /// <summary>
        /// Role of this server-side task.
        /// </summary>
        CapabilityType Role { get; }

        /// <summary>
        /// Task priority.
        /// </summary>
        TaskPriority Priority { get; }

        /// <summary>
        /// Jobs to invoke for this task.
        /// </summary>
        IEnumerable<IJob> Jobs { get; }

        /// <summary>
        /// Convenience property for pending jobs.
        /// </summary>
        IEnumerable<IJob> PendingJobs { get; }

        /// <summary>
        /// Job results
        /// </summary>
        IEnumerable<IJobResult> JobResults { get; }

        /// <summary>
        /// Status filename XML.
        /// </summary>
        String Filename { get; }

        /// <summary>
        /// Task parameters.
        /// </summary>
        IDictionary<String, Object> Parameters { get; }

        /// <summary>
        /// Email notification recipients
        /// </summary>
        IEnumerable<IEmailRecipient> EmailRecipients { get; }

        /// <summary>
        /// Bug recipients
        /// </summary>
        IEnumerable<IBugRecipient> BugRecipients { get; }

        /// <summary>
        /// The state of the task
        /// </summary>
        TaskState TaskState { get; }
        #endregion // Properties

        #region Controller Methods
        /// <summary>
        /// Start task processing thread(s) if required.
        /// </summary>
        /// *Most tasks will not require this*
        /// 
        void Start();

        /// <summary>
        /// Stop task processing thread(s) if required.
        /// </summary>
        /// *Most tasks will not require this*
        /// 
        void Stop();

        /// <summary>
        /// Update task; updating collection of Job objects.
        /// </summary>
        void Update();

        /// <summary>
        /// Enqueue an externally requested job onto a task queue.
        /// </summary>
        /// <param name="job"></param>
        /// This should be used for rebuild functionality for task queues.
        /// 
        void EnqueueJob(IJob job);

        /// <summary>
        /// Automation service notification to ITask that job is complete.
        /// </summary>
        /// <param name="job"></param>
        /// This is invoked prior to the SendNotifications call.
        void JobCompleted(IJob job, IEnumerable<IJobResult> jobResults);

        /// <summary>
        /// Send out job notifications (e.g. email) to stakeholders defined in the notification configuration file prior to job processing.
        /// </summary>
        /// <param name="job"></param>
        void SendPreJobNotifications(IJob job, Uri workerUri, Guid workerGuid);

        /// <summary>
        /// Send out job notifications (e.g. email) to stakeholders defined in the notification configuration file after the job has been assigned.
        /// </summary>
        /// <param name="job"></param>
        void SendAssignedJobNotifications(IJob job, Uri workerUri, Guid workerGuid);

        /// <summary>
        /// Indicates that post job notification is required.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        bool PostJobNotificationRequired(IJob job);

        /// <summary>
        /// Send out post job notifications (e.g. email) to stakeholders defined in the notification configuration file after a job has been processed.
        /// </summary>
        /// <param name="job"></param>
        void SendPostJobNotifications(IJob job, IEnumerable<IJobResult> jobResults, Uri workerUri, Guid workerGuid, String logFilename);

        /// <summary>
        /// Return status of task.
        /// </summary>
        /// <returns></returns>
        TaskStatus Status();

        /// <summary>
        /// Set tasks current job (if it keeps track).
        /// </summary>
        /// <param name="job"></param>
        void SetCurrentJob(IJob job);

        /// <summary>
        /// Load state from disk.
        /// </summary>
        void Load();

        /// <summary>
        /// Save state to disk.
        /// </summary>
        void Save();

        /// <summary>
        /// Skip the processing of a job.
        /// </summary>
        /// <param name="id"></param>
        bool Skip(Guid id);

        /// <summary>
        /// Prioritise processing of a job.
        /// </summary>
        /// <param name="id"></param>
        bool Prioritise(Guid id);

        /// <summary>
        /// Abort processing of a job.
        /// </summary>
        /// <param name="id"></param>
        bool Abort(Guid id);

        /// <summary>
        /// Reprocess a job.
        /// </summary>
        /// <param name="id"></param>
        bool Reprocess(Guid id);

        /// <summary>
        /// Retrieve a suitable job for a given workers machine
        /// </summary>
        /// <param name="machinename"></param>
        /// <returns></returns>
        IJob GetSuitableJob(String machinename);

        /// <summary>
        /// Exception Alert 
        /// </summary>
        /// <param name="log"></param>
        /// <param name="exception"></param>
        /// <param name="summary"></param>
        /// <param name="detail"></param>
        void ExceptionAlert(ILog log, Exception exception, string summary, string detail);

        /// <summary>
        /// Shutdown Alert
        /// </summary>
        /// <param name="log"></param>
        /// <param name="shutdownType"></param>
        /// <param name="summary"></param>
        /// <param name="detail"></param>
        void ShutdownAlert(ILog log, ShutdownType shutdownType, string summary, string detail);

        /// <summary>
        /// Startup Alert
        /// </summary>
        /// <param name="log"></param>
        /// <param name="summary"></param>
        /// <param name="detail"></param>
        void StartupAlert(ILog log, string summary, string detail);

        /// <summary>
        /// General Alert
        /// </summary>
        /// <param name="log"></param>
        /// <param name="summary"></param>
        /// <param name="detail"></param>
        void GeneralAlert(ILog log, string summary, string detail,IEnumerable<String> extraNotificationRecipients);

        /// <summary>
        /// Two jobs are designated the same 'type of work', and from the perpective of the task are said to be 'associated'
        /// - this association is used to group bindable jobs and to associate jobs that form an incremental breakers list.        
        /// - association can be a loose association, does not mean there are identical.
        /// - in the case of codebuilder2 jobs it means they are building the same solution in the same way.
        /// </summary>
        /// <param name="job1"></param>
        /// <param name="job2"></param>
        /// <returns>true if they are associated</returns>
        bool Associated(IJob job1, IJob job2);

        #endregion // Controller Methods
    }

} // RSG.Pipeline.Automation.Common.Tasks namespace
