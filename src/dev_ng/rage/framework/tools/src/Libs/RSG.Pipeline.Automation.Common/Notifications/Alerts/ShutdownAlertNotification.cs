﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RSG.Base.Collections;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Services;
using RSG.Pipeline.Automation.Common.Services.Messages;

namespace RSG.Pipeline.Automation.Common.Notifications.Alerts
{
    /// <summary>
    /// 
    /// </summary>
    public class ShutdownAlertNotification
        : AlertNotificationBase
    {
        #region Properties

        public ShutdownType ShutdownType
        {
            get;
            private set;
        }

        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="job"></param>
        /// <param name="maxErrors"></param>
        public ShutdownAlertNotification(ILog log, CommandOptions options, ShutdownType shutdownType, string summary = null, IEnumerable<string> detail = null)
            : base(log, summary, detail, options)
        {
            this.ShutdownType = shutdownType;
        }
        #endregion // Constructors

        #region Controller Methods

        /// <summary>
        /// Override for setting the subject
        /// </summary>
        public override void SetSubject()
        {
            this.Subject = String.Format("[{1} {2}: Automation Alert] SHUTDOWN: {0}", base.Summary, Options.CoreProject.FriendlyName, Options.Branch.Name);
        }

        /// <summary>
        /// Override for setting the body
        /// </summary>
        public override void SetBody()
        {
            // the first line of the body can be seen in systray notifications : make it as informative as possible.
            this.Body.AppendLine(" ");
            this.Body.AppendLine("*** SHUTDOWN ALERT ***");
            this.Body.AppendLine(" ");

            base.Body.AppendLine(string.Format("Shutdown has been triggered on {0}\n", Environment.MachineName));
            
            if ((this.ShutdownType.HasFlag(ShutdownType.Resume)))
            {
                base.Body.AppendLine(string.Format("Process will resume afterwards.\n"));
            }
            else
            {
                base.Body.AppendLine(string.Format("Process will NOT resume afterwards.\n"));
            }
            // DW: TODO : create a method that represent the shutdown as a string, can't flesh this out right now since it's still awaiting buddy
        }
        #endregion // Controller Methods
    }
}
