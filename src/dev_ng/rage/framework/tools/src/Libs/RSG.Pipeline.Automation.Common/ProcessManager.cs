﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace RSG.Pipeline.Automation.Common
{
    public class ProcessManager : IDisposable
    {
        List<Process> processes = new List<Process>();
        private static ProcessManager m_instance;

        public static ProcessManager Instance
        {
            get
            {
                if (m_instance == null)
                    m_instance = new ProcessManager();

                return m_instance;
            }
        }

        public Process Start(ProcessStartInfo info)
        {
            var newProcess = Process.Start(info);
            newProcess.EnableRaisingEvents = true;
            processes.Add(newProcess);
            newProcess.Exited += (sender, e) => processes.Remove(newProcess);
            return newProcess;
        }

        private ProcessManager()
        {

        }

        ~ProcessManager()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            foreach (var process in processes)
            {
                try
                {
                    if (!process.HasExited)
                        process.Kill();
                }
                catch { }
            }
        }
    }
}
