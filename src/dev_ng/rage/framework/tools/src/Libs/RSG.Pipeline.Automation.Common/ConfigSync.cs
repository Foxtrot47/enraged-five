﻿using RSG.Base.Configuration;
using RSG.SourceControl.Perforce;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Logging;

namespace RSG.Pipeline.Automation.Common
{
    /// <summary>
    /// Supports config syncing mechanism that is shared by the serverhost and the bootstrap mechanism.
    /// </summary>
    public static class ConfigSync
    {
        #region Constants
        /// <summary>
        /// Log context.
        /// </summary>
        private static readonly String LOG_CTX = "ConfigSync";

        /// <summary>
        /// Max content recurse depth.
        /// </summary>
        private static readonly int MAX_CONTENT_RECURSE_DEPTH = 10;
        #endregion // LOG_CTX

        #region Public Static Methods
        /// <summary>
        /// Return files that are considered worthy of restarting & syncing to. 
        /// - tools config files and dependencies thereof. 
        /// - content.xml files and dependencies thereof.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="branch"></param>
        /// <param name="config"></param>
        /// <param name="syncToolsConfig">toggle tools config syncing</param>
        /// <param name="syncContentTree">toggle content tree syncing</param>
        /// <returns>the depot filenames we wish to sync to</returns>
        public static IEnumerable<String> DepotFilesTriggeringRestartResync(P4 p4, IBranch branch, IConfig config, bool syncToolsConfig, bool syncContentTree)
        {
            // Get all the config files and directories we wish to sync
            ISet<String> configFiles = new HashSet<String>();

            if (syncToolsConfig)
            {
                // Add config.xml
                configFiles.Add(Path.GetFullPath(config.Environment.Subst(config.Filename)));

                // This will support DLC filenames that are not yet recognised by config.AllProjects since the file does not exist on local disk.
                IEnumerable<IProjectSummary> projectSummaries = ConfigFactory.CreateProjectSummary();
                foreach (IProjectSummary projectSummary in projectSummaries)
                {
                    configFiles.Add(Path.GetFullPath(config.Environment.Subst(projectSummary.Filename)));
                }
            }

            // Now for all projects ( including DLC ) add the tools config files pertinent to their project.
            foreach (KeyValuePair<String, IProject> kvp in config.AllProjects())
            {
                IProject project = kvp.Value;

                if (syncToolsConfig)
                {
                    // Add project.xml                    
                    configFiles.Add(Path.GetFullPath(config.Environment.Subst(project.Filename)));
                }

                if (syncContentTree)
                {
                    // Add content.xml
                    if (project.Branches.ContainsKey(branch.Name))
                    {
                        IBranch projectBranch = project.Branches[branch.Name];
                        String dirName = Path.GetDirectoryName(projectBranch.Content);

                        // Non DLC projects have a different setup of content tree in which we parse the content and xi:includes.
                        if (project != config.CoreProject)
                        {
                            String contentFilename = ExpandContentFilename(branch, null, projectBranch.Content);

                            int depth = 0;
                            IEnumerable<String> contentFiles = GetContentFilenames(branch, contentFilename, ref depth);
                            configFiles.UnionWith(contentFiles);
                        }
                        else
                        {
                            // tools/etc/content : add the whole directory ( as opposed to recursively parsing the content.xml file )
                            configFiles.Add(String.Format("{0}/...", dirName));
                        }
                    }
                }
            }

            // Evaluate in p4 and return as depot paths
            FileMapping[] filesMapped = FileMapping.Create(p4, configFiles.ToArray());
            IEnumerable<String> depotFilenames = filesMapped.Select(f => f.DepotFilename);
            return depotFilenames;
        }
        #endregion // Public Static Methods

        #region Private Static Methods
        /// <summary>
        /// Recursive content file search - depth first.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="filename">the filename to add to list of content files and search for further included files.</param>
        /// <param name="recursionDepth">recursion depth guage, higher the value the deeper the recursion.</param>
        /// <returns>content files found recursively.</returns>
        private static IEnumerable<String> GetContentFilenames(IBranch branch, String filename, ref int recursionDepth)
        {
            ISet<String> contentFilenames = new HashSet<String>() { filename };
            if (!File.Exists(filename))
            {
                return contentFilenames;
            }

            if (recursionDepth > MAX_CONTENT_RECURSE_DEPTH)
            {
                // Probably as a result of infinite recursion - stop now.
                Log.Log__ErrorCtx(LOG_CTX, "possible infinite recursion in content files found at depth {0} whilst processing filename {1}", recursionDepth, filename);
                return contentFilenames;
            }

            try
            {
                recursionDepth++;
                XDocument xmlDoc = XDocument.Load(filename);
                // Get all external references of the xi:includes or otherwise via a simple xpath for all href attributes
                IEnumerable<XElement> xmlElems = xmlDoc.XPathSelectElements("//*[@href]");

                foreach (XElement xmlElem in xmlElems)
                {
                    String includedFilename = xmlElem.Attributes("href").First().Value;

                    includedFilename = ExpandContentFilename(branch, filename, includedFilename);

                    // Add the included filenames, and filenames thereof to our config file list
                    // *** IT'S IMPORTANT TO REALISE THE FILE MIGHT NOT EXIST YET - Don't check if the file exists! ***
                    contentFilenames.UnionWith(GetContentFilenames(branch, includedFilename, ref recursionDepth));
                }
            }
            finally
            {
                recursionDepth--;
            }
            return contentFilenames;
        }

        /// <summary>
        /// Env expand, root filename et al
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="relativeFilename">the filename the includedfilename is relative to</param>
        /// <param name="includedFilename">the file to expand</param>
        /// <returns>the expanded filename</returns>
        private static string ExpandContentFilename(IBranch branch, String relativeFilename, String includedFilename)
        {
            // Env expand the filename
            includedFilename = System.Environment.ExpandEnvironmentVariables(branch.Environment.Subst(includedFilename));

            // Root it.
            if (!String.IsNullOrEmpty(relativeFilename) && !Path.IsPathRooted(includedFilename))
            {
                includedFilename = Path.Combine(Path.GetDirectoryName(relativeFilename), includedFilename);
            }

            // Iron out any wrinkles in the path name.
            includedFilename = Path.GetFullPath(includedFilename);
            return includedFilename;
        }
        #endregion // Private static methods
    }
}
