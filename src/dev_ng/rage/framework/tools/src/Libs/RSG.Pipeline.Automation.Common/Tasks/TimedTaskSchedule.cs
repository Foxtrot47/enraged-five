﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Pipeline.Automation.Common.Triggers;

namespace RSG.Pipeline.Automation.Common.Tasks
{
    /// <summary>
    /// A wrapper for the TimedTaskList.  Possibly dispensible, consider removing.
    /// </summary>
    public sealed class TimedTaskSchedule
    {
        /// <summary>
        /// List of events TimedTaskEvents
        /// </summary>
        public IList<TimedTaskEvent> Events { get; set; }
        #region Constructor
        /// <summary>
        /// Default constructor.
        /// </summary>
        public TimedTaskSchedule()
        {
            this.Events = new List<TimedTaskEvent>();
        }
        #endregion // Constructor
    }

    /// <summary>
    /// A TimedTaskEvent is created for every TimedTask that should fire for a given TimedTrigger.
    /// </summary>
    public sealed class TimedTaskEvent
    {
        #region Properties
        /// <summary>
        /// Time the task is schedueld to fire.
        /// </summary>
        public DateTime ScheduledTime { get; private set; }

        /// <summary>
        /// Time the task actually fired.
        /// </summary>
        public DateTime ActualTime { get; set; }

        /// <summary>
        /// TimedTrigger associated with this TimedTaskEvent
        /// </summary>
        public TimedTrigger TimedTrigger { get; private set; }
        #endregion // Properties

        #region Constructor
        public TimedTaskEvent(DateTime scheduledTime, TimedTrigger timedTrigger)
        {
            this.ScheduledTime = scheduledTime;
            this.ActualTime = DateTime.MinValue;
            this.TimedTrigger = timedTrigger;
        }
        #endregion // Constructor
    }
}
