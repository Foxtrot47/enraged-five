﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;

namespace RSG.Pipeline.Automation.Common.Triggers
{

    /// <summary>
    /// User-request trigger.
    /// </summary>
    [DataContract(Namespace = "")]
    public class UserRequestTrigger : 
        FilesTrigger,
        IUserRequestTrigger,
        IHasCommand,
        IHasDescription,
        IHasUsername
    {
        #region Properties
        /// <summary>
        /// User requesting job.
        /// </summary>
        [DataMember]
        public String Username { get; private set; }

        /// <summary>
        /// Description
        /// </summary>
        [DataMember]
        public String Description { get; private set; }

        /// <summary>
        /// A string that can be parsed differently once on the client.
        /// </summary>
        [DataMember]
        public String Command { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor (used for deserialisation).
        /// </summary>
        public UserRequestTrigger()
            : base()
        {
            this.Command = String.Empty;
        }

        /// <summary>
        /// Constructor.  This is primarily used in {tools folder}\dcc\current\{max folder}\scripts\pipeline\export\maps\mapnetworkexport.ms
        /// </summary>
        /// <param name="files"></param>
        public UserRequestTrigger(IEnumerable<String> files)
            : this(Environment.UserName, files, System.Guid.NewGuid(), System.DateTime.UtcNow, String.Empty)
        {
            this.Command = String.Empty;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="files"></param>
        public UserRequestTrigger(IEnumerable<String> files, Guid triggerId = default(Guid), DateTime triggeredAt = default(DateTime), String description = "No description provided - please do!")
            : this(Environment.UserName, files, triggerId, triggeredAt)
        {
            this.Command = String.Empty;
            this.Description = description;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="files"></param>
        public UserRequestTrigger(String username, IEnumerable<String> files, Guid triggerId = default(Guid), DateTime triggeredAt = default(DateTime), String description = "No description provided - please do!")
            : base(files, triggerId, triggeredAt)
        {
            this.Username = username;
            this.Command = String.Empty;
            this.Description = description;
            this.Command = String.Empty;
        }

        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise trigger representation to XML.
        /// </summary>
        /// <returns></returns>
        public override XElement Serialise()
        {
            XElement xmlTrigger = base.Serialise();
            xmlTrigger.Add(
                new XElement("Username", this.Username)
            );
            xmlTrigger.Add(
                new XElement("Command", this.Command)
            );
            xmlTrigger.Add(
                new XElement("Description", this.Description != null ? this.Description : String.Empty)
            );


            return (xmlTrigger);
        }

        /// <summary>
        /// Deserialise trigger representation from XML.
        /// </summary>
        /// <param name="xmlTriggerElem"></param>
        /// <returns></returns>
        public override bool Deserialise(XElement xmlTriggerElem)
        {
            bool result = true;
            try
            {
                base.Deserialise(xmlTriggerElem);
                this.Username = xmlTriggerElem.XPathSelectElement("Username").Value;

                // DW: backwards compatibility...
                XElement formattedCmdEle = xmlTriggerElem.XPathSelectElement("FormattedCommand");
                if (formattedCmdEle != null)
                {
                    this.Command = formattedCmdEle.Value;
                }

                XElement cmdEle = xmlTriggerElem.XPathSelectElement("Command");
                if (cmdEle != null)
                {
                    this.Command = cmdEle.Value;
            }

                // Make sure that a description exists before trying to read it.
                XElement descriptionElement = xmlTriggerElem.XPathSelectElement("Description");
                if(descriptionElement != null)
                {
                    this.Description = descriptionElement.Value;
                }
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Error deserialising User Request Trigger.");
                result = false;
            }
            return (result);
        }
        #endregion // Controller Methods

        #region Public Methods

        public void SetFormattedStringCommand(String arg)
        {
            this.Command = arg; 
        }
        #endregion //  Public Methods
    }

} // RSG.Pipeline.Automation.Common.Triggers namespace
