﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RSG.Base.Configuration;
using RSG.Base.Collections;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Services;

namespace RSG.Pipeline.Automation.Common.Notifications.Alerts
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class AlertNotificationBase
        : EmailNotificationBase,
            IEmailNotification
    {
        #region Constants
        private readonly String WIKI = "\nhttps://devstar.rockstargames.com/wiki/index.php/Automation_Monitor.";
        #endregion // Constants

        #region Properties

        protected string Summary { get; private set; }
        protected IEnumerable<string> Details { get; private set; }

        #endregion // Properties

        #region Contructors()
        /// <summary>
        /// 
        /// </summary>
        /// <param name="job"></param>
        public AlertNotificationBase(ILog log, string summary, IEnumerable<string> details, CommandOptions options)
            : base(log, options, false)
        {
            this.Priority = System.Net.Mail.MailPriority.High;
            this.Summary = summary;
            this.Details = details;
        }
        #endregion // Contructors

        #region Controller Methods
        /// <summary>
        /// Email subject builder.
        /// </summary>
        public virtual void SetSubject()
        {
            this.Subject = String.Format("[{0} {1}: Automation Alert] {2}", Options.CoreProject.FriendlyName, Options.Branch.Name, Summary);
        }

        /// <summary>
        /// Email body header builder.
        /// </summary>
        public virtual void SetHeader()
        {
            // Overload with more meaningful message in the parent class
            // the first line of the header can be seen in systray notifications : make it as informative as possible.
            if (Details != null)
            {
                foreach (string detail in Details)
                {
                    this.Body.AppendLine(detail);
                }
            }
        }

        /// <summary>
        /// Email main body builder.
        /// </summary>
        public virtual void SetBody()
        {
        }

        /// <summary>
        /// Email body footer builder.
        /// </summary>
        public virtual void SetFooter()
        {
            Footer.Append(WIKI);
        }
        #endregion // Controller Methods

        #region IEmailNotification Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool Send(IEnumerable<IEmailRecipient> notificationRecipients, IDictionary<string, EmailRedirect> emailRedirects)
        {
            this.SetHeader();
            this.SetBody();
            this.SetFooter();
            this.SetSubject();

            base.Send(notificationRecipients, emailRedirects);

            return (true);
        }
        #endregion // IEmailNotification
    } // AlertNotificationBase
} // namespace RSG.Pipeline.Automation.Common.Notifications
