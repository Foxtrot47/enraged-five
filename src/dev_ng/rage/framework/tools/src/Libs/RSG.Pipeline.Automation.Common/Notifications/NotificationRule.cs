﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Notifications
{
    #region Enumerations
    /// <summary>
    /// Enum flags of notification
    /// </summary>
    /// 
    [Flags]
    public enum NotificationRule : uint
    {
        Never = 0,
        OnWarnings = (1 << 0),
        OnErrors = (1 << 1),
        OnExceptions = (1 << 2),
        OnShutdown = (1 << 3),
        OnStartup = (1 << 4),
        OnGeneralAlert = (1 << 5),
        OnCompleted = (1 << 6),
        OnAssigned = (1 << 7),
        OnAll = unchecked((uint)~0)
    }
    #endregion // Enumerations
}
