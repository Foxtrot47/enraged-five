﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Bug
{
    #region Enumerations
    /// <summary>
    /// Enum flags of bug
    /// </summary>
    /// 
    [Flags]
    public enum BugRule : uint
    {
        Never = 0,
        OnErrors = (1 << 0),           
        OnAll = unchecked((uint)~0)
    }
    #endregion // Enumerations
}
