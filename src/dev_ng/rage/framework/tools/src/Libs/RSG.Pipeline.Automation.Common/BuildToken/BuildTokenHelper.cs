﻿using RSG.Base.Logging.Universal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace RSG.Pipeline.Automation.Common.Triggers
{
    /// <summary>
    /// Helper class for using build tokens
    /// </summary>
    public static class BuildTokenHelper
    {
        /// <summary>
        /// Build token regex for only finding the presence of build tokens 
        /// </summary>
        private static readonly String BUILD_TOKEN_SIMPLE_REGEX = @"#{0}";

        /// <summary>
        /// Build token regex that can find the values of build tokens
        /// </summary>
        private static readonly String BUILD_TOKEN_REGEX = @"#{0}\s*[=]*\s*(\S*)";

        /// <summary>
        /// Log context
        /// </summary>
        private static readonly String LOG_CTX = "BuildTokenHelper";

        /// <summary>
        /// Is a build token set - irrespecive of whether it has value(s) associated with it.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="description"></param>
        /// <param name="buildTokenName"></param>
        /// <returns>true if the tag is found</returns>
        public static bool HasBuildToken(IUniversalLog log, String description, BuildToken buildToken)
        {
            String regex = String.Format(BUILD_TOKEN_SIMPLE_REGEX, buildToken);
            Regex buildTokenRegex = new Regex(regex, RegexOptions.IgnoreCase);
            if (buildTokenRegex.IsMatch(description))
            {
                log.DebugCtx(LOG_CTX, "Build Token {0} has been found.", buildToken);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Get the build tokens for a particular buildtoken in a description.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="description"></param>
        /// <param name="buildToken"></param>
        /// <returns></returns>
        public static ICollection<String> GetBuildTokens(IUniversalLog log, String description, BuildToken buildToken)
        {
            ICollection<String> buildTokenResults = new List<String>();
            String regex = String.Format(BUILD_TOKEN_REGEX, buildToken);
            Regex buildTokenRegex = new Regex(regex, RegexOptions.IgnoreCase);

            foreach (Match match in buildTokenRegex.Matches(description))
            {
                if (match.Groups.Count == 1)
                {
                    log.DebugCtx(LOG_CTX, "\tBuildToken {0} ( with no value associated )", buildToken);
                    buildTokenResults.Add(String.Empty);
                }
                else if (match.Groups.Count > 1)
                {
                    String value = match.Groups[1].Value;
                    log.DebugCtx(LOG_CTX, "\tBuildToken {0}={1}", buildToken, value);
                    buildTokenResults.Add(value);
                }
            }

            return buildTokenResults;
        }
    }
}
