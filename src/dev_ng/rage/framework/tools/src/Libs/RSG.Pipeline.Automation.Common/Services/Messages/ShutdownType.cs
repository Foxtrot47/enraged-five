using System;
using System.Runtime.Serialization;

namespace RSG.Pipeline.Automation.Common.Services.Messages
{
    [Flags]
    public enum ShutdownType : uint
    {
        None            = 0,            // NOP
        Resume          = (1 << 0),     // Resume automation process after exit
        ScmSyncHead     = (1 << 1),     // Sync to head of tools after exit
        ScmSyncLabel    = (1 << 2),     // Sync to labelled tools after exit
        ScmSyncConfig   = (1 << 3),     // Sync to config after exit
        ScmSyncContent  = (1 << 4),     // Sync to content after exit
        WorkersAndHost  = (1 << 5),     // Workers And Host ( both are usually required to be shutdown at same time )
        CleanCache      = (1 << 6)      // Clean the cache folder before restart
    }
} // RSG.Pipeline.Automation.Common.Services.Messages namespace
