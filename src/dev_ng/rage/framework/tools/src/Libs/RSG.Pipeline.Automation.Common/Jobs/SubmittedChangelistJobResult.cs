﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.XPath;
using System.Xml.Linq;
using RSG.Base.Logging;

namespace RSG.Pipeline.Automation.Common.Jobs
{
    [DataContract]
    public class SubmittedChangelistJobResult :
        JobResult,
        IJobResult,
        ISubmittedChangelist
    {
        /// <summary>
        /// Submitted Changelist number
        /// </summary>
        [DataMember]
        public int SubmittedChangelistNumber { get; set; }

         #region Constructors
        /// <summary>
        /// Default constructor (for deserialisation).
        /// </summary>
        public SubmittedChangelistJobResult()
            : base()
        { 
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SubmittedChangelistJobResult(Guid jobId, bool succeeded, int submittedChangelistNumber = (int)JobSubmissionState.NotSubmitted)
            : base(jobId, succeeded)
        {
            SubmittedChangelistNumber = submittedChangelistNumber;
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// Serialise job result representation to XML.
        /// </summary>
        /// <returns>The Xelement serialised</returns>
        public override XElement Serialise()
        {
            XElement xmlJobResult = base.Serialise();
            xmlJobResult.Add(new XElement("SubmittedChangelistNumber", this.SubmittedChangelistNumber));

            return (xmlJobResult);
        }

        /// <summary>
        /// Deserialise job result representation from XML.
        /// </summary>
        /// <param name="xmlJobResultElem"></param>
        /// <returns>bool for success</returns>
        public override bool Deserialise(XElement xmlJobResultElem)
        {
            bool result = true;
            try
            {
                result &= base.Deserialise(xmlJobResultElem);
                this.SubmittedChangelistNumber = int.Parse(xmlJobResultElem.XPathSelectElement("SubmittedChangelistNumber").Value);
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Error deserialising Asset Builder Job.");
                result = false;
            }
            return (result);
        }

        /// <summary>
        /// Override ToString
        /// - A prettier version. 
        /// - suitable for generic presentation ( do not specialise ) 
        /// - ought to be terse.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string prettyClNumber = "?";

            if (SubmittedChangelistNumber >= 0)
            {
                prettyClNumber = SubmittedChangelistNumber.ToString();                
            }
            else 
            {
                JobSubmissionState changeSubmissionState;
                if (Enum.TryParse<JobSubmissionState>(SubmittedChangelistNumber.ToString(), out changeSubmissionState))
                {
                    prettyClNumber = changeSubmissionState.ToString();
                }
                else
                {
                    Log.Log__Warning("Unknown JobSubmissionState enum {0}", SubmittedChangelistNumber);                    
                }
            }

            string str = String.Format("{0} CL:{1}", base.ToString(), prettyClNumber);
            return str;
        }

        #endregion // Controller Methods
    }
}
