﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Pipeline.Automation.Common;

namespace RSG.Pipeline.Automation.Common.Tasks
{

    /// <summary>
    /// Interface for tasks that monitor file paths to be invoked.
    /// </summary>
    public interface IMonitoringTask : ITask
    {
        #region Properties
        /// <summary>
        /// Monitored path locations.
        /// </summary>
        IEnumerable<String> MonitoredPaths { get; }

        /// <summary>
        /// Currently processing changelist number.
        /// </summary>
        UInt32 CurrentChangelist { get; }

        /// <summary>
        /// Current maximum changelist number.
        /// </summary>
        UInt32 MaximumChangelist { get; }
        #endregion // Properties
    }

} // RSG.Pipeline.Automation.Common.Tasks namespace
