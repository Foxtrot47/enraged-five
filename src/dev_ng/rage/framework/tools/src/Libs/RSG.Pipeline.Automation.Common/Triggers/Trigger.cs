﻿using RSG.Base.Logging;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Extensions;
using RSG.Pipeline.Automation.Common.Utils;

namespace RSG.Pipeline.Automation.Common.Triggers
{
    /// <summary>
    /// Basic trigger class; containing a set of files.
    /// </summary>
    [DataContract]
    [KnownType(typeof(FilesTrigger))]
    [KnownType(typeof(TimedTrigger))]
    [KnownType(typeof(ChangelistTrigger))]
    [KnownType(typeof(UserRequestTrigger))]
    [KnownType(typeof(MultiTrigger))]
    public abstract class Trigger : 
        ITrigger
    {
        #region Properties
        /// <summary>
        /// Triggering date/time.
        /// </summary>
        [DataMember]
        public DateTime TriggeredAt { get; protected set; }

        /// <summary>
        /// Trigger Id
        /// </summary>
        [DataMember]
        public Guid TriggerId { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// (Internal) Default constructor.
        /// </summary>
         public Trigger()
            :this( DateTime.Now.ToUniversalTime() )
        {
        }

        /// <summary>
        /// Parameterised constructor with the triggeredAt passed in.
        /// </summary>
        public Trigger(DateTime triggeredAt, Guid triggerId = default(Guid))
        {
            TriggeredAt = triggeredAt;
            TriggerId = triggerId;
        }

        /// <summary>
        /// Chained constructor.
        /// </summary>
        public Trigger(Guid triggerId = default(Guid))
            : this(DateTime.Now.ToUniversalTime(), triggerId) 
        {      
        }
       
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// Serialise job representation to XML.
        /// </summary>
        /// <returns></returns>
        public virtual XElement Serialise()
        {
            XElement xmlJob = new XElement("Trigger",
                new XAttribute("type", this.GetType().Name));

            DateTime triggeredAtTime = SettingsXml.GetDateTimeForSerialisationUtc(this.TriggeredAt);

            xmlJob.Add(new XElement("TriggeredAt", triggeredAtTime));
            xmlJob.Add(new XElement("TriggerId", this.TriggerId.ToString()));

            return (xmlJob);
        }

        /// <summary>
        /// Deserialise job representation from XML.
        /// </summary>
        /// <param name="xmlJobElem"></param>
        /// <returns></returns>
        public virtual bool Deserialise(XElement xmlJobElem)
        {
            this.TriggeredAt = SettingsXml.DeserialiseDateTimeUtc(xmlJobElem.XPathSelectElement("TriggeredAt"), DateTime.MinValue.ToUniversalTime());

            XElement triggerIdEle = xmlJobElem.XPathSelectElement("TriggerId");
            if (triggerIdEle != null)
                this.TriggerId = Guid.Parse(triggerIdEle.Value);
            else
                this.TriggerId = default(Guid);


            return (true);
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Automation.Common.Triggers namespace
