﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Triggers
{
    /// <summary>
    /// A multi trigger
    /// - an enumerable of triggers.
    /// </summary>
    public interface IMultiTrigger : 
        ITrigger
    {
        #region Properties
        /// <summary>
        /// the triggers the multi trigger encompasses
        /// </summary>
        IEnumerable<ITrigger> Triggers { get; }
        #endregion // Properties
    }
}
