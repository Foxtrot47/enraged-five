﻿using RSG.Pipeline.Automation.Common.Triggers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Automation.Common.Jobs
{
    /// <summary>
    /// Job Result interface
    /// </summary>
    public interface IJobResult
    {
        #region Properties
        /// <summary>
        /// Job unique identifier.
        /// </summary>
        Guid JobId { get; set; }

        /// <summary>
        /// Job Succeeded or failed?
        /// </summary>
        bool Succeeded { get; set; }

        /// <summary>
        /// The build tokens that were processed
        /// </summary>
        ICollection<KeyValuePair<BuildToken, String>> ProcessedBuildTokens { get; set; }

        #endregion // Properties

        #region Methods
        /// <summary>
        /// Serialise job result representation to XML.
        /// </summary>
        /// <returns></returns>
        XElement Serialise();       

        /// <summary>
        /// Deserialise job result representation from XML.
        /// </summary>
        /// <returns></returns>
        bool Deserialise(XElement xmlJobElem);
        #endregion // Methods
    }
}
