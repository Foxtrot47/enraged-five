﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using RSG.SourceControl.Perforce;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;

namespace RSG.Pipeline.Automation.Common.Jobs
{
    /// <summary>
    /// Integrator job result.
    /// - job result information pertaining to an Integrator job.
    /// </summary>
    [DataContract]
    public class IntegratorJobResult :
        JobResult,
        IJobResult
    {
        #region Constants
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Submitted Changelist number
        /// - the changelist that was successfully submitted
        /// - or if negative an error code ( see JobSubmissionState )
        /// </summary>
        [DataMember]
        public int SubmittedChangelistNumber { get; set; }

        /// <summary>
        /// Pending Integration Changelist number
        /// </summary>
        [DataMember]
        public int PendingIntegrationChangelistNumber { get; set; }
        #endregion // Properties


        #region Constructors
        /// <summary>
        /// Default constructor (for deserialisation).
        /// </summary>
        public IntegratorJobResult()
            : base()
        {
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public IntegratorJobResult(Guid jobId, bool succeeded, int submittedChangelistNumber = (int)JobSubmissionState.NotSubmitted, int pendingIntegrationChangelistNumber = (int)JobSubmissionState.NotSubmitted)
            : base(jobId, succeeded)
        {
            SubmittedChangelistNumber = submittedChangelistNumber;
            PendingIntegrationChangelistNumber = pendingIntegrationChangelistNumber;
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// Serialise job result representation to XML.
        /// </summary>
        /// <returns>The Xelement serialised</returns>
        public override XElement Serialise()
        {
            XElement xmlJobResult = base.Serialise();
            xmlJobResult.Add(new XElement("SubmittedChangelistNumber", this.SubmittedChangelistNumber));
            xmlJobResult.Add(new XElement("PendingIntegrationChangelistNumber", this.PendingIntegrationChangelistNumber));

            return (xmlJobResult);
        }

        /// <summary>
        /// Deserialise job result representation from XML.
        /// </summary>
        /// <param name="xmlJobResultElem"></param>
        /// <returns>bool for success</returns>
        public override bool Deserialise(XElement xmlJobResultElem)
        {
            bool result = true;
            try
            {
                result &= base.Deserialise(xmlJobResultElem);
                this.SubmittedChangelistNumber = int.Parse(xmlJobResultElem.XPathSelectElement("SubmittedChangelistNumber").Value);
                this.PendingIntegrationChangelistNumber = int.Parse(xmlJobResultElem.XPathSelectElement("PendingIntegrationChangelistNumber").Value);
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Error deserialising Integrator Job Result.");
                result = false;
            }
            return result;
        }

        /// <summary>
        /// Override ToString
        /// - A prettier version. 
        /// - suitable for generic presentation ( do not specialise ) 
        /// - ought to be terse.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string prettyClNumber = "?";

            if (SubmittedChangelistNumber >= 0)
            {
                prettyClNumber = SubmittedChangelistNumber.ToString();
            }
            else
            {
                JobSubmissionState changeSubmissionState;
                if (Enum.TryParse<JobSubmissionState>(SubmittedChangelistNumber.ToString(), out changeSubmissionState))
                {
                    prettyClNumber = changeSubmissionState.ToString();
                }
                else
                {
                    Log.Log__Warning("Unknown JobSubmissionState enum {0}", SubmittedChangelistNumber);
                }
            }

            string str = String.Format("{0} CL:{1}", base.ToString(), prettyClNumber);

            if (PendingIntegrationChangelistNumber >= 0)
            {
                str = String.Format("{0} * Needs Integrate CL:{1} *", str, PendingIntegrationChangelistNumber);
            }
            else if (PendingIntegrationChangelistNumber == (int)JobSubmissionState.Integrated)
            {
                str = String.Format("* Hand Integrated *");
            }

            return str;
        }

        #endregion // Controller Methods
    }
}
