﻿using System;
using System.Xml.Linq;

namespace RSG.Pipeline.Automation.Common.Triggers
{
    /// <summary>
    /// Trigger interface.
    /// </summary>
    public interface ITrigger
    {
        #region Properties
        /// <summary>
        /// Gets the time this trigger was triggered
        /// - eg. to clarify this is the time the changelist was submitted, NOT neccessarily when *this* object was created.
        /// </summary>
        DateTime TriggeredAt { get; }

        /// <summary>
        /// The Id of the trigger
        /// - any other triggers with the same id are triggered by the same event even if the TriggeredAt time may differ by small triggeredAt time ticks.
        /// </summary>
        Guid TriggerId { get; }

        #endregion // Properties

        #region Methods
        /// <summary>
        /// Serialise job representation to XML.
        /// </summary>
        /// <returns></returns>
        XElement Serialise();

        /// <summary>
        /// Deserialise job representation from XML.
        /// </summary>
        /// <returns></returns>
        bool Deserialise(XElement xmlJobElem);
        #endregion // Methods
    }
} // RSG.Pipeline.Automation.Common.Triggers
