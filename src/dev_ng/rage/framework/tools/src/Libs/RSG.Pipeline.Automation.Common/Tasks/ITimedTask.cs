﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Pipeline.Automation.Common.Triggers;

namespace RSG.Pipeline.Automation.Common.Tasks
{

    /// <summary>
    /// Interface for tasks that are invoked at a specific time or interval.
    /// </summary>
    public interface ITimedTask : ITask
    {
        /// <summary>
        /// Enumerable of time trigger objects
        /// </summary>
        IEnumerable<TimedTrigger> TimedTriggers { get; }
    }

} // RSG.Pipeline.Automation.Common.Tasks namespace
