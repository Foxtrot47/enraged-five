﻿using RSG.Pipeline.Automation.Common.JobManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Tasks
{
    /// <summary>
    /// Interface for tasks that support dispatch scheme.
    /// </summary>
    public interface IDispatchSchemeTask : ITask
    {
        /// <summary>
        /// Dispatch scheme used by task
        /// </summary>
        DispatchScheme DispatchScheme { get; }
    }
}
