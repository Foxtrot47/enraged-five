﻿using System;
using System.Runtime.Serialization;

namespace RSG.Pipeline.Automation.Common.Export
{

    /// <summary>
    /// Export state enumeration.
    /// </summary>
    [DataContract]
    public enum ExportState
    {
        [EnumMember]
        Unknown,

        [EnumMember]
        Ok,

        [EnumMember]
        Warnings,

        [EnumMember]
        Errors,
    }

} // RSG.Pipeline.Automation.Common.Export namespace
