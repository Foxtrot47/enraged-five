﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;
using RSG.Platform;

namespace RSG.Pipeline.Automation.Common.Export
{

    /// <summary>
    /// Class representing an export process; taking a DCC source file to a
    /// series of export files.
    /// </summary>
    [DataContract]
    public class ExportProcess
    {
        #region Properties
        /// <summary>
        /// DCC Source File for this process.
        /// </summary>
        [DataMember]
        public String DCCSourceFilename { get; set; }

        /// <summary>
        /// Files exported by this process.
        /// </summary>
        [DataMember]
        public IEnumerable<String> ExportFiles { get; set; }

        /// <summary>
        /// Source texture files required by this export process.
        /// </summary>
        [DataMember]
        public IEnumerable<String> EditedSourceTextures { get; set; }

        /// <summary>
        /// Platforms to do the export for (if none, the installer specified platforms are used).
        /// </summary>
        [DataMember]
        public IEnumerable<Platform.Platform> ExportPlatforms { get; set; }

        /// <summary>
        /// Export start time.
        /// </summary>
        [DataMember]
        public DateTime ExportAt 
        {
            get { return m_dtExportAt; }
            set { m_dtExportAt = value; }
        }
        private DateTime m_dtExportAt;

        /// <summary>
        /// Export duration.
        /// </summary>
        [DataMember]
        public TimeSpan ExportTime 
        { 
            get { return m_tsExportTime; }
            set { m_tsExportTime = value; }
        }
        private TimeSpan m_tsExportTime;

        /// <summary>
        /// Export state.
        /// </summary>
        [DataMember]
        public ExportState ExportState { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ExportProcess()
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="file"></param>
        public ExportProcess(String dccSourceFilename, IEnumerable<String> exportFiles, IEnumerable<String> sourceTextures, 
            IEnumerable<RSG.Platform.Platform> exportPlatforms)
        {
            this.DCCSourceFilename = dccSourceFilename;
            this.ExportFiles = new List<String>(exportFiles);
            this.ExportAt = default(DateTime);
            this.ExportTime = default(TimeSpan);
            this.ExportState = ExportState.Unknown;
            this.EditedSourceTextures = new List<String>(sourceTextures);
            this.ExportPlatforms = new List<Platform.Platform>(exportPlatforms);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise job representation to XML.
        /// </summary>
        /// <returns></returns>
        public virtual XElement Serialise()
        {
            XElement xmlExportProcess = new XElement("ExportProcess",
                new XElement("DCCSourceFilename", this.DCCSourceFilename),
                new XElement("EditedSourceTextures", this.EditedSourceTextures.Select(t => new XElement("Texture", t))),
                new XElement("ExportFiles", this.ExportFiles.Select(f => new XElement("File", f))),
                this.ExportPlatforms != null ?
                    new XElement("ExportPlatforms", this.ExportPlatforms.Select(p => new XElement("Platform", p))) :
                    new XElement("ExportPlatforms"),
                new XElement("ExportAt", this.ExportAt),
                new XElement("ExportTime", this.ExportTime),
                new XElement("ExportState", this.ExportState)
            );
            return (xmlExportProcess);
        }

        /// <summary>
        /// Deserialise job representation from XML.
        /// </summary>
        /// <param name="xmlExportProcessElem"></param>
        /// <returns></returns>
        public virtual bool Deserialise(XElement xmlExportProcessElem)
        {
            bool result = true;
            try
            {
                this.DCCSourceFilename = xmlExportProcessElem.XPathSelectElement("DCCSourceFilename").Value;

                this.ExportState = (ExportState)Enum.Parse(typeof(ExportState),
                    xmlExportProcessElem.XPathSelectElement("ExportState").Value);

                IEnumerable<String> files = xmlExportProcessElem.XPathSelectElements("ExportFiles/File").
                    Select(xmlFileElem => xmlFileElem.Value);
                this.ExportFiles = new List<String>(files).ToArray();

                IEnumerable<Platform.Platform> platforms = xmlExportProcessElem.XPathSelectElements("ExportPlatforms/Platform").
                    Select(xmlFileElem => (Platform.Platform)Enum.Parse(typeof(Platform.Platform),xmlFileElem.Value));
                this.ExportPlatforms = platforms.ToArray();

                IEnumerable<String> textures = xmlExportProcessElem.XPathSelectElements("EditedSourceTextures/Texture").
                     Select(xmlFileElem => xmlFileElem.Value);
                this.EditedSourceTextures = new List<String>(textures).ToArray();

                this.ExportAt = DateTime.MinValue;
                DateTime.TryParse(xmlExportProcessElem.XPathSelectElement("ExportAt").Value,
                    out this.m_dtExportAt);

                this.ExportTime = TimeSpan.MinValue;
                TimeSpan.TryParse(xmlExportProcessElem.XPathSelectElement("ExportTime").Value,
                    out this.m_tsExportTime);
                
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Error deserialising Export Process.");
                result = false;
            }
            return (result);
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Automation.Common.Export namespace
