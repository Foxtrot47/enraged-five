﻿using RSG.Base.Configuration;
using RSG.Pipeline.Automation.Common.Bug;
using RSG.Pipeline.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Notifications
{
    /// <summary>
    /// Bug notification interface.
    /// </summary>
    public interface IBugNotification
    {
        /// <summary>
        /// Method for creating bugs
        /// </summary>
        /// <returns></returns>
        IEnumerable<uint> Create(IEnumerable<IBugRecipient> bugRecipients);
    }
}
