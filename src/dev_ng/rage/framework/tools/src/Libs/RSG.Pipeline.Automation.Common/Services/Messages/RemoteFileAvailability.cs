﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace RSG.Pipeline.Automation.Common.Services.Messages
{

    /// <summary>
    /// Remote file availability object; for returning to clients data about
    /// what remote files are available on the file transfer service.
    /// </summary>
    [MessageContract]
    [Obsolete("Use RemoteFileAvailability2 instead.")]
    public class RemoteFileAvailability
    {
        #region Properties
        /// <summary>
        /// Number of files available for the specified job.
        /// </summary>
        [MessageBodyMember]
        public int FileCount { get; set; }

        /// <summary>
        /// Enumerable of filename strings.
        /// </summary>
        [MessageBodyMember]
        public IEnumerable<String> Filenames { get; set; }

        /// <summary>
        /// Job identifier.
        /// </summary>
        [MessageBodyMember]
        public Guid Job { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public RemoteFileAvailability()
        {
            this.FileCount = 0;
            this.Filenames = new List<String>();
            this.Job = Guid.Empty;
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Automation.Common.Services.Messages namespace
