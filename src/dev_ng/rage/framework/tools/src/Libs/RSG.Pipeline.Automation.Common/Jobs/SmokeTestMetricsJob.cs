﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;
using RSG.Pipeline.Core;
using RSG.SourceControl.Perforce;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Platform;
using RSG.Pipeline.Content;
using RSG.Statistics.Common;

namespace RSG.Pipeline.Automation.Common.Jobs
{
    /// <summary>
    /// Smoketest for Metrics test 
    /// </summary>
    [DataContract]
    public class SmokeTestMetricsJob :
        BaseSmokeTestJob
    {
        #region Properties

        /// <summary>
        /// If we have a desired client machine for the job
        /// </summary>
        [DataMember]
        public String ClientName { get; set; }
        #endregion

        public SmokeTestMetricsJob(XElement jobEle, CapabilityType type, ITrigger trigger, RSG.Platform.Platform platform, 
                                        BuildConfig buildConfig,IBranch branch, uint exeCL, uint rageCL, uint originalCL, uint shelvedCL=0)
            : base(jobEle, trigger, type, platform, buildConfig, branch, exeCL, rageCL, originalCL, shelvedCL)
        {
            FriendlyName = this.GetType().ToString();

            XElement clientNameElement = jobEle.Element("ClientName");
            if (clientNameElement != null)
            {
                ClientName = Environment.ExpandEnvironmentVariables(clientNameElement.Value);
            }
            else
            {
                Log.Log__Warning("Warning creating SmoketestJob from XML. Missing <ClientName>");
                ClientName = "";
            }
        }

        /// <summary>
        /// Serialise job representation to XML.
        /// </summary>
        /// <returns></returns>
        public override XElement Serialise()
        {
            XElement xmlJob = base.Serialise();
            xmlJob.Add(new XElement("ClientName", this.ClientName));
            
            return (xmlJob);
        }

        /// <summary>
        /// Deserialise job representation from XML.
        /// </summary>
        /// <param name="xmlJobElem"></param>
        /// <returns></returns>
        public override bool Deserialise(XElement xmlJobElem)
        {
            bool result = true;
            try
            {
                result &= base.Deserialise(xmlJobElem);
                this.ClientName = Environment.ExpandEnvironmentVariables((String)xmlJobElem.XPathSelectElement("ClientName").Value);
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Error deserialising Smoketest Metrics Job.");
                result = false;
            }
            return (result);
        }
    }
}
