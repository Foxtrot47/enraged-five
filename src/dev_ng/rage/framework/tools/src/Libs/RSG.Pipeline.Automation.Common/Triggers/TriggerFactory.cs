﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;

namespace RSG.Pipeline.Automation.Common.Triggers
{
    /// <summary>
    /// Trigger factory for deserialisation of ITrigger objects.
    /// </summary>
    internal static class TriggerFactory
    {
        #region Constants
        private static readonly String LOG_CTX = "Trigger Factory";
        #endregion // Constants

        #region Member Data
        private static IDictionary<String, Type> TriggerTypes;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Static constructor.
        /// </summary>
        static TriggerFactory()
        {
            TriggerFactory.TriggerTypes = new Dictionary<String, Type>();
            Assembly assembly = Assembly.GetExecutingAssembly();
            foreach (Type type in assembly.GetTypes())
            {
                if (type.IsAbstract)
                    continue;
                if (type.IsInterface)
                    continue;

                if (typeof(ITrigger).IsAssignableFrom(type))
                    TriggerFactory.TriggerTypes.Add(type.Name, type);
            }
        }
        #endregion // Constructor(s)

        /// <summary>
        /// Create trigger object; deserialising from XML.
        /// </summary>
        /// <param name="xmlJob"></param>
        /// <returns></returns>
        public static ITrigger Create(XElement xmlJob)
        {
            XAttribute xmlTypeAttr = ((IEnumerable)xmlJob.XPathEvaluate("@type")).
                Cast<XAttribute>().FirstOrDefault();
            if (null == xmlTypeAttr)
            {
                Log.Log__ErrorCtx(LOG_CTX, "Serialised Trigger does not contain type attribute!");
                return (null);
            }

            String typeName = xmlTypeAttr.Value;
            if (String.IsNullOrEmpty(typeName))
            {
                Log.Log__ErrorCtx(LOG_CTX, "Invalid type name for trigger XML.");
                return (null);
            }

            if (TriggerFactory.TriggerTypes.ContainsKey(typeName))
            {
                ITrigger trigger = (ITrigger)Activator.CreateInstance(TriggerFactory.TriggerTypes[typeName]);
                trigger.Deserialise(xmlJob);
                return (trigger);
            }
            else
            {
                Log.Log__ErrorCtx(LOG_CTX, "Trigger factory unknown type: {0}.", typeName);
                return (null);
            }
        }
    }

} // RSG.Pipeline.Automation.Common.Triggers namespace
