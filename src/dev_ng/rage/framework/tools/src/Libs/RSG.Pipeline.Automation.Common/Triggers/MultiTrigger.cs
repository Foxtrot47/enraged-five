﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;
using RSG.SourceControl.Perforce;

namespace RSG.Pipeline.Automation.Common.Triggers
{

    /// <summary>
    /// A multi trigger
    /// - an enumerable of triggers.
    /// </summary>
    [DataContract(Namespace = "")]
    public class MultiTrigger :
        Trigger,
        IMultiTrigger
    {
        #region Properties
        /// <summary>
        /// Triggers
        /// </summary>
        [DataMember]
        public IEnumerable<ITrigger> Triggers { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MultiTrigger()
            : base()
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public MultiTrigger(IEnumerable<ITrigger> triggers, Guid triggerId = default(Guid), DateTime triggeredAt = default(DateTime))
            : base(triggeredAt, triggerId)
        {
            this.Triggers = triggers;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public MultiTrigger(Guid triggerId, DateTime triggeredAt = default(DateTime))
            : base(triggeredAt, triggerId)
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise trigger representation to XML.
        /// </summary>
        /// <returns></returns>
        public override XElement Serialise()
        {
            XElement xmlTrigger = base.Serialise();
            xmlTrigger.Add(this.Triggers.Select(t => t.Serialise()));
            return (xmlTrigger);
        }

        /// <summary>
        /// Deserialise trigger representation from XML.
        /// </summary>
        /// <param name="xmlTriggerElem"></param>
        /// <returns></returns>
        public override bool Deserialise(XElement xmlTriggerElem)
        {
            bool result = true;
            try
            {
                base.Deserialise(xmlTriggerElem);
                IEnumerable<XElement> triggerElements = xmlTriggerElem.XPathSelectElements("Trigger");
                                
                ICollection<ITrigger> triggers = new List<ITrigger>();
                foreach (XElement triggerElement in triggerElements)
                {
                    triggers.Add(TriggerFactory.Create(triggerElement));
                }
                this.Triggers = triggers;
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Error deserialising MultiTrigger.");
                result = false;
            }
            return (result);
        }
        #endregion // Controller Methods
    }
} // RSG.Pipeline.Automation.Common.Triggers namespace
