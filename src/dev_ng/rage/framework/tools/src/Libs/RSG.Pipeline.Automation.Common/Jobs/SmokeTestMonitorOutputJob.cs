﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;
using RSG.Pipeline.Core;
using RSG.SourceControl.Perforce;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Platform;
using RSG.Pipeline.Content;
using RSG.Statistics.Common;

namespace RSG.Pipeline.Automation.Common.Jobs
{
    /// <summary>
    /// Smoketest for updating the IPL_Loader.xml
    /// </summary>
    [DataContract]
    public class SmokeTesMonitorOutputLJob:
        BaseSmokeTestJob,
        ISmokeTestCompletedMessages
    {
        #region Properties
        /// <summary>
        //Label to sync all of Branch.Build to.
        //This should be populated with the LatestDeployedBuild
        /// </summary>
        [DataMember]
        public String LabelName { get; set; }

        /// <summary>
        // A friendly name used to be more descriptive in e-mails or other reports
        /// </summary>
        [DataMember]
        public String FriendlyName { get; set; }

        /// <summary>
        //A list of files to update in p4. These files should have been edited while the game is running
        /// </summary>
        [DataMember]
        public IEnumerable<String> FilesToUpdate { get; set; }

        /// <summary>
        //This String will be parsed for in the output of the game to signify when the SmokeTest is done successfully
        /// </summary>
        [DataMember]
        public IEnumerable<String> SuccessMessages { get; set; }

        /// <summary>
        //This String will be parsed for in the output of the game to signify when the SmokeTest has failed
        /// </summary>
        [DataMember]
        public IEnumerable<String> FailureMessages { get; set; }
        #endregion

        /// <summary>
        /// Default constructor (for deserialisation).
        /// </summary>
        public SmokeTesMonitorOutputLJob()
            : base()
        {
            FilesToUpdate = new List<String>();
            LabelName = "";
            FailureMessages = new List<String>();
            SuccessMessages = new List<String>();
            FriendlyName = "";
        }

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <returns></returns>
        public SmokeTesMonitorOutputLJob(XElement jobEle, CapabilityType type, String labelName, ITrigger trigger, RSG.Platform.Platform platform, BuildConfig buildConfig, IBranch branch)
            : base(jobEle, trigger, type, platform, buildConfig, branch)
        {
            LabelName = labelName;
            FilesToUpdate = new List<String>();
            FailureMessages = new List<String>();
            SuccessMessages = new List<String>();

            XElement xmlReadElement = jobEle.Element("SuccessMessages");
            if (xmlReadElement != null)
            {
                IEnumerable<XElement> messageList = xmlReadElement.Descendants("Message");
                SuccessMessages = messageList.Select(msg => Environment.ExpandEnvironmentVariables(msg.Value));
            }
            else
            {
				String msg = "Error creating SmoketestMonitoringOuput from XML. Missing <SuccessMessages> item.";
                Log.Log__Error(msg);
                throw new Exception(msg);
            }

            xmlReadElement = jobEle.Element("FailureMessages");
            if (xmlReadElement != null)
            {
                IEnumerable<XElement> messageList = xmlReadElement.Descendants("Message");
                FailureMessages = messageList.Select(msg => Environment.ExpandEnvironmentVariables(msg.Value));
            }

            xmlReadElement = jobEle.Element("FriendlyName");
            if (xmlReadElement != null)
            {
                FriendlyName = xmlReadElement.Value;
            }
            else
            {
                String msg = "Error creating SmoketestMonitoringOuput from XML. Missing <FriendlyName> item.";
                Log.Log__Error(msg);
                throw new Exception(msg);
            }

            xmlReadElement = jobEle.Element("FilesToUpdate");
            IEnumerable<XElement> fileList = xmlReadElement.Descendants("File");
            if (fileList != null)
            {
                FilesToUpdate = fileList.Select(f => f.Value);
            }
            else
            {
                String msg = "Error creating SmoketestMonitoringOuput from XML. Missing <FilesToUpdate> item.";
                Log.Log__Error(msg);
                throw new Exception(msg);
            }
        }

        /// <summary>
        /// Serialise job representation to XML.
        /// </summary>
        /// <returns></returns>
        public override XElement Serialise()
        {
            XElement xmlJob = base.Serialise();
            xmlJob.Add(new XElement("LabelName", LabelName));
            xmlJob.Add(new XElement("FriendlyName", FriendlyName));

            List<XElement> successMessageEles = new List<XElement>();
            foreach (String msg in SuccessMessages)
            {
                successMessageEles.Add(new XElement("Message", msg));
            }
            xmlJob.Add(new XElement("SuccessMessages", successMessageEles.ToArray()));

            List<XElement> failedMessageEles = new List<XElement>();
            foreach (String msg in FailureMessages)
            {
                successMessageEles.Add(new XElement("Message", msg));
            }
            xmlJob.Add(new XElement("FailureMessages", successMessageEles.ToArray()));

            List<XElement> filesToUpdateEles = new List<XElement>();
            foreach (String file in FilesToUpdate)
            {
                filesToUpdateEles.Add(new XElement("File", file));
            }
            xmlJob.Add(new XElement("FilesToUpdate", filesToUpdateEles.ToArray()));

            return xmlJob;
        }

        /// <summary>
        /// Deserialise job representation from XML.
        /// </summary>
        /// <param name="xmlJobElem"></param>
        /// <returns></returns>
        public override bool Deserialise(XElement xmlJobElem)
        {
            bool result = base.Deserialise(xmlJobElem);
            LabelName = xmlJobElem.XPathSelectElement("LabelName").Value;
            FriendlyName = xmlJobElem.XPathSelectElement("FriendlyName").Value;

            XElement xmlJobSubEle = xmlJobElem.XPathSelectElement("SuccessMessages");
            if (xmlJobSubEle != null)
            {
                SuccessMessages = xmlJobSubEle.Descendants("Message").Select(f => f.Value);
            }
            else
            {
                throw new Exception("Missing \"SuccessMessages\" item during deserialisation of SmokeTesMonitorOutputLJob!");
            }
            xmlJobSubEle = xmlJobElem.XPathSelectElement("FailureMessages");
            if (xmlJobSubEle != null)
            {
                FailureMessages = xmlJobSubEle.Descendants("Message").Select(f => f.Value);
            }
            else
            {
                throw new Exception("Missing \"FailureMessages\" item during deserialisation of SmokeTesMonitorOutputLJob!");
            }

            xmlJobSubEle = xmlJobElem.XPathSelectElement("FilesToUpdate");
            if (xmlJobSubEle != null)
            {
                FilesToUpdate = xmlJobSubEle.Descendants("File").Select(f => f.Value);
            }
            else
            {
                throw new Exception("Missing \"FilesToUpdate\" item during deserialisation of SmokeTesMonitorOutputLJob!");
            }

            return result; 
        }
    }
}
