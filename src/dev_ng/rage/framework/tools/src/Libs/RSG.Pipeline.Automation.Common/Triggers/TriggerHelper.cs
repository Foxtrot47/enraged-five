﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Triggers
{
    /// <summary>
    /// Helper class for triggers that contains extension methods for the ITrigger interface.
    /// - faciltates multitrigger considerations (these are recursive methods to search nested triggers for common properties)
    /// </summary>
    public static class TriggerHelper
    {
        #region Public Static Methods
        /// <summary>
        /// Recursive helper method to retreive a unique list of files from triggers
        /// </summary>
        /// <param name="trigger"></param>
        /// <returns></returns>
        public static IEnumerable<String> TriggeringFiles(this ITrigger trigger)
        {
            ISet<String> files = new HashSet<String>();

            if (trigger is IFilesTrigger)
            {
                files.UnionWith(((IFilesTrigger)trigger).Files);
            }
            else if (trigger is IMultiTrigger)
            {
                foreach (ITrigger subTrigger in ((IMultiTrigger)trigger).Triggers)
                {
                    files.UnionWith(TriggeringFiles(subTrigger));
                }
            }
            else
            {
                //Make this more obvious when trying get unsupported trigger data.
                throw new NotImplementedException(String.Format("Attempting to get triggering files from an unsupported trigger type. Type: {0}.", trigger.GetType().ToString()));
            }

            return files;
        }

        /// <summary>
        /// Helper method to retreive a unique list of changelists number(s) if any from a trigger.
        /// </summary>
        /// <param name="trigger"></param>
        /// <returns></returns>
        public static IEnumerable<uint> ChangelistNumbers(this ITrigger trigger)
        {
            ISet<uint> changelists = new HashSet<uint>();

            if (trigger is IChangelistTrigger)
            {
                changelists.Add(((IChangelistTrigger)trigger).Changelist);
            }
            else if (trigger is IMultiTrigger)
            {
                foreach (ITrigger subTrigger in ((IMultiTrigger)trigger).Triggers)
                {
                    changelists.UnionWith(ChangelistNumbers(subTrigger));
                }
            }

            return changelists;
        }

        /// <summary>
        /// Helper method to retreive a unique list of user(s) of a trigger.
        /// </summary>
        /// <param name="trigger"></param>
        /// <returns></returns>
        public static IEnumerable<String> Usernames(this ITrigger trigger)
        {
            ISet<String> usernames = new HashSet<String>();

            if (trigger is IHasUsername)
            {
                usernames.Add(((IHasUsername)trigger).Username);
            }
            else if (trigger is IMultiTrigger)
            {
                foreach (ITrigger subTrigger in ((IMultiTrigger)trigger).Triggers)
                {
                    usernames.UnionWith(Usernames(subTrigger));
                }
            }
            else if (trigger is TimedTrigger) // DW: this looks bad, timed trigger should not be used it should have used an interface ITimedTrigger anyway.
            {
                usernames.Add(((IUserRequestTrigger)trigger).Username);
            }

            return usernames;
        }

        /// <summary>
        /// Helper method to retreive a unique list of triggered at datetime(s) of a trigger.
        /// </summary>
        /// <param name="trigger"></param>
        /// <returns></returns>
        public static IEnumerable<DateTime> TriggeredAt(this ITrigger trigger)
        {
            ISet<DateTime> triggeredAtDateTimes = new HashSet<DateTime>();

            if (trigger is IMultiTrigger)
            {
                foreach (ITrigger subTrigger in ((IMultiTrigger)trigger).Triggers)
                {
                    triggeredAtDateTimes.UnionWith(TriggeredAt(subTrigger));
                }
            }
            else
            {
                triggeredAtDateTimes.Add(trigger.TriggeredAt);
            }

            return triggeredAtDateTimes;
        }

        /// <summary>
        /// Helper method to retreive a unique list of description(s) of a trigger.
        /// - may not be desirable to have used a HashSet here - hard to decide yet.
        /// </summary>
        /// <param name="trigger"></param>
        /// <returns></returns>
        public static IEnumerable<String> Descriptions(this ITrigger trigger)
        {
            ISet<String> descriptions = new HashSet<String>();

            if (trigger is IHasDescription)
            {
                descriptions.Add(((IHasDescription)trigger).Description);
            }
            else if (trigger is IMultiTrigger)
            {
                foreach (ITrigger subTrigger in ((IMultiTrigger)trigger).Triggers)
                {
                    descriptions.UnionWith(Descriptions(subTrigger));
                }
            }

            return descriptions;
        }

        /// <summary>
        /// Helper method to retreive a unique list of all the monitored path(s) of a trigger.
        /// </summary>
        /// <param name="trigger"></param>
        /// <returns></returns>
        public static IEnumerable<String> MonitoredPaths(this ITrigger trigger)
        {
            ISet<String> monitoredPaths = new HashSet<String>();

            if (trigger is IChangelistTrigger)
            {
                monitoredPaths.UnionWith(((IChangelistTrigger)trigger).MonitoredPaths);
            }
            else if (trigger is IMultiTrigger)
            {
                foreach (ITrigger subTrigger in ((IMultiTrigger)trigger).Triggers)
                {
                    monitoredPaths.UnionWith(MonitoredPaths(subTrigger));
                }
            }

            return monitoredPaths;
        }
        #endregion // Public Static Methods
    }
}
