﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Services;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Services;
using RSG.SourceControl.Perforce;
using RSG.SourceControl.Perforce.Util;
using RSG.SourceControl.Perforce.Extensions;

namespace RSG.Pipeline.Automation.Common.Tasks
{
    /// <summary>
    /// Perforce monitoring task base class; the monitor is somewhat configurable
    /// through some properties.
    /// </summary>
    public abstract class MonitoringTaskBase : 
        TaskBase,
        IMonitoringTask,
        IDisposable
    {
        #region Constants
        /// <summary>
        /// 'P4 changes' command is chunked up by numbers of paths it needs to query.
        /// </summary>
        private static readonly int P4_CHANGES_CHUNK_SIZE = 10;

        /// <summary>
        /// Time to look back to find CLs submitted if no previous CL processed
        /// </summary>
        private static readonly int CL_LOOK_BACK_MINUTES_DEFAULT = 15;

        // Minutes to look behind time.now if no previous CL has been processed
        private static readonly String PARAM_CL_LOOK_BACK_MINUTES = "CL Look Back Minutes";

        #endregion // Constants

        #region Properties
        /// <summary>
        /// Monitored path locations.
        /// </summary>
        public IEnumerable<String> MonitoredPaths
        {
            get { return m_MonitoredPaths; }
        }
        private ICollection<String> m_MonitoredPaths;
        
        /// <summary>
        /// Currently processing changelist number.
        /// </summary>
        public UInt32 CurrentChangelist 
        {
            get { return m_CurrentChangelist; }
            protected set { m_CurrentChangelist = value; }
        }
        private UInt32 m_CurrentChangelist;

        /// <summary>
        /// Current maximum changelist number.
        /// </summary>
        public UInt32 MaximumChangelist 
        {
            get { return m_MaximumChangelist; }
            protected set { m_MaximumChangelist = value; }
        }
        private UInt32 m_MaximumChangelist;

        /// <summary>
        /// Perforce connection.
        /// </summary>
        public P4 P4
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; specfying a monitored path.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="role"></param>
        /// <param name="location"></param>
        public MonitoringTaskBase(String name, String description, CapabilityType role, String location, CommandOptions options)
            : this(name, description, role, new String[] { location }, options)
        {
        }

        /// <summary>
        /// Constructor; specifying one or more monitored paths.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="locations"></param>
        public MonitoringTaskBase(String name, String description, CapabilityType role, IEnumerable<String> locations, CommandOptions options)
            : base(name, description, role, options)
        {
            this.P4 = this.Config.Project.SCMConnect();
            this.m_MonitoredPaths = new List<String>();

            if (locations.Count() > 0)
            {
                FileMapping[] fms = FileMapping.Create(this.P4, locations.ToArray());
                this.m_MonitoredPaths.AddRange(fms.Select(fm => fm.DepotFilename));
            }
        }

        ~MonitoringTaskBase()
        {
            this.Dispose(false);
        }

        /// <summary>
        /// Object dispose.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Return status of task.
        /// </summary>
        /// <returns></returns>
        public override TaskStatus Status()
        {
            return (new MonitoringTaskStatus(this));
        }

        /// <summary>
        /// Set tasks current job (if it keeps track).
        /// </summary>
        /// <param name="job"></param>
        public override void SetCurrentJob(IJob job)
        {
            IEnumerable<uint> clNums = job.Trigger.ChangelistNumbers();
            if (!clNums.Any())
                return;

            this.CurrentChangelist = clNums.Max();
        }

        /// <summary>
        /// Load state from disk.
        /// </summary>
        public override void Load()
        {
            this.Log.MessageCtx(LOG_CTX, "Loading state from {0}.", this.Filename);
            try
            {
                if (!File.Exists(this.Filename))
                    return; // No state to load.

                XDocument xmlDoc = XDocument.Load(this.Filename);
                String typeName = ((IEnumerable)xmlDoc.Root.XPathEvaluate("/Task/@type")).
                    Cast<XAttribute>().First().Value;
                Debug.Assert(0 == String.Compare(this.GetType().Name, typeName),
                    String.Format("Loading task status information from incorrect file: {0} {1}.",
                    this.Name, this.Filename));

                // Monitor status.
                UInt32.TryParse(xmlDoc.Root.XPathSelectElement("/Task/CurrentChangelist").Value,
                    out this.m_CurrentChangelist);
                UInt32.TryParse(xmlDoc.Root.XPathSelectElement("/Task/MaximumChangelist").Value,
                    out this.m_MaximumChangelist);

                LoadJobs(xmlDoc);

                LoadJobResults(xmlDoc);

                LoadState(xmlDoc);
            }
            catch (Exception ex)
            {
                this.Log.ExceptionCtx(LOG_CTX, ex, "Error deserialising status from disk: {0}.",
                    this.Filename);
            }
        }

        /// <summary>
        /// Save state to disk.
        /// </summary>
        public override void Save()
        {
            this.Log.MessageCtx(LOG_CTX, "Saving state to {0}.", this.Filename);
            try
            {
                XDocument xmlDoc = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "yes"),
                    new XElement("Task",
                        new XAttribute("name", this.Name),
                        new XAttribute("type", this.GetType().Name),
                        new XElement("CurrentChangelist", this.CurrentChangelist),
                        new XElement("MaximumChangelist", this.MaximumChangelist),
                        new XElement("State", this.TaskState.ToString()),
                        new XElement("Jobs", this.Jobs.Select(j => j.Serialise())),
                        new XElement("JobResults", this.JobResults.Select(jr => jr.Serialise())))
                );

                String directoryName = Path.GetDirectoryName(this.Filename);
                if (!Directory.Exists(directoryName))
                    Directory.CreateDirectory(directoryName);
                lock(this.Filename)
                {
                    xmlDoc.Save(this.Filename);
                }
            }
            catch (Exception ex)
            {
                this.Log.ExceptionCtx(LOG_CTX, ex, "Error serialising status to disk: {0}.",
                    this.Filename);
            }
        }
        #endregion // Controller Methods

        #region Protected Methods
        /// <summary>
        /// Monitoring task common feature for fetching a list of changelists
        /// from the server.  This should typically be invoked in MonitoringTaskBase
        /// subclasses and then the Changelist objects turned into Job's of the
        /// correct type.
        /// </summary>
        /// <returns></returns>
        protected IEnumerable<Changelist> GetSubmittedChangelists()
        {
            HashSet<uint> allClNums = new HashSet<uint>();
            
            foreach (IJob job in Jobs.Where(j => j.State != JobState.SkippedConsumed))
            {
                IEnumerable<uint> jobClNums = job.Trigger.ChangelistNumbers();
                allClNums.UnionWith(jobClNums);
            }

            uint lastFetchedChangelist = allClNums.Any() ? allClNums.Max() : this.MaximumChangelist;
          
            uint earliest = lastFetchedChangelist;
            IEnumerable<String> paths;

#warning DHM FIX ME: can't use UTC time, needs to be in Perforce server-timezone!
            if (0 == earliest)
            {
                
                int clLookBackMinutes = Parameters.ContainsKey(PARAM_CL_LOOK_BACK_MINUTES) ?
                    (int)Parameters[PARAM_CL_LOOK_BACK_MINUTES] : CL_LOOK_BACK_MINUTES_DEFAULT;

                DateTime fromTime = DateTime.Now - TimeSpan.FromMinutes(clLookBackMinutes);
                paths = this.MonitoredPaths.Select(path => MonitoringPathToHeadFileSpec(path, fromTime.ToString("yyyy/MM/dd:HH:mm:ss")));
            }
            else
            {
                paths = this.MonitoredPaths.Select(path => MonitoringPathToHeadFileSpec( path, (earliest + 1).ToString()));
            
            }

            if (!this.MonitoredPaths.Any())
            {
                this.Log.ErrorCtx(LOG_CTX, "No monitored Paths");
                return (new Changelist[] { });
            }

            // Run p4 changes command chunked up.
            IEnumerable<P4API.P4RecordSet> recordSets = this.P4.ChangesChunked(paths, P4_CHANGES_CHUNK_SIZE);

            if (recordSets.Count() == 0 )
                return (new Changelist[] { });

            IEnumerable<Changelist> changelists = Changelist.CreateFromChangesRecords(this.P4, recordSets);

            // Ensure we return no duplicates 
            IEnumerable<Changelist> uniqueChangelists = changelists.Distinct(cl => cl.Number);

            // NOW order by CL number
            IEnumerable<Changelist> orderedChangelists = uniqueChangelists.OrderBy(cl => cl.Number);

            return orderedChangelists;
        }
        /// <summary>
        /// Dispose implementation.
        /// </summary>
        /// <param name="disposing"></param>
        /// <see cref="http://msdn.microsoft.com/en-us/library/ms244737(v=vs.110).aspx" />
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // Free managed resources.
                this.P4.Dispose();
            }
        }
        #endregion // Protected Methods

        #region Private Methods
        /// <summary>
        /// Used to add suffix to the paths. This means we can have single files and file types
        /// as monitoring paths rather than just folders.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="suffix"></param>
        /// <returns></returns>
        private String MonitoringPathToHeadFileSpec(String path, String suffix)
        {
            // Checking for paths that are already formated for P4 folders.
            if (path.Contains("..."))
            {
                return String.Format("{0}@{1},#head", path, suffix);
            }
            // Checks for single file
            else if (path.Contains("."))
            {
                return String.Format("{0}@{1},#head", path, suffix);

            }
            // Append ... we think this is a folder not already formatted
            return String.Format("{0}/...@{1},#head", path, suffix);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.Common.Tasks namespace
