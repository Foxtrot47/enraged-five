﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.Configuration;
using RSG.Pipeline.Services;
using System.Text.RegularExpressions;
using System.Net.Mail;

namespace RSG.Pipeline.Automation.Common.Notifications
{
    public abstract class EmailNotificationBase
        : IEmailNotification
    {
        #region Constants
        /// <summary>
        /// Log context.
        /// </summary>
        protected static readonly String LOG_CTX = "Email Notification";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Email notification subject.
        /// </summary>
        public String Subject
        {
            get;
            protected set;
        }

        /// <summary>
        /// Email content that should be at the top of the email.
        /// </summary>
        public StringBuilder Header
        {
            get;
            protected set;
        }

        /// <summary>
        /// Email content in the body of the email
        /// </summary>
        public StringBuilder Body
        {
            get;
            protected set;
        }

        /// <summary>
        /// Email content to appear at the end of the email.
        /// </summary>
        public StringBuilder Footer
        {
            get;
            protected set;
        }

        /// <summary>
        /// User name, initialised to unkown in the contructor as this may not be relevant.
        /// </summary>
        public String Owner
        {
            get;
            protected set;
        }

        /// <summary>
        /// Configuration object.
        /// </summary>
        public IConfig Config
        {
            get;
            protected set;
        }

        /// <summary>
        /// Command Options
        /// </summary>
        public CommandOptions Options
        {
            get;
            protected set;
        }

        /// <summary>
        /// Priority
        /// </summary>
        public System.Net.Mail.MailPriority Priority
        {
            get;
            protected set;
        }

        /// <summary>
        /// Log object.
        /// </summary>
        public ILog Log
        {
            get;
            protected set;
        }

        /// <summary>
        /// Body is in Html format?
        /// </summary>
        public bool Html
        {
            get;
            protected set;
        }

        /// <summary>
        /// Associated Users
        /// </summary>
        public IEnumerable<String> AssociatedUsernames
        {
            get;
            protected set;
        }

        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="job"></param>
        public EmailNotificationBase(ILog log, CommandOptions options, bool html, IEnumerable<String> associatedUsernames = null)
        {
            this.Options = options;
            this.Config = options.Config;
            this.Owner = "Unknown";
            this.AssociatedUsernames = associatedUsernames;
            this.Log = log;

            this.Priority = System.Net.Mail.MailPriority.Normal;
            this.Subject = "None";
            this.Header = new StringBuilder();
            this.Body = new StringBuilder();
            this.Footer = new StringBuilder();
            this.Html = html;
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="notificationRecipient"></param>
        /// <returns></returns>
        public virtual bool Send(IEnumerable<IEmailRecipient> notificationRecipients, IDictionary<string, EmailRedirect> emailRedirects)
        {
            lock (notificationRecipients)
            {
                IStudio thisStudio = this.Config.Studios.ThisStudio;
                if (thisStudio == null)
                {
                    this.Log.ErrorCtx(LOG_CTX, "Configuration could not determine which studio this is.  Is the machine on the correct subnet?");
                    return false;
                }

                if (thisStudio.BuilderUser == null || thisStudio.BuilderName == null)
                {
                    if (thisStudio.BuilderUser == null)
                        this.Log.ErrorCtx(LOG_CTX, "Configuration could not determine the builder user for this domain.");

                    if (thisStudio.BuilderName == null)
                        this.Log.ErrorCtx(LOG_CTX, "Configuration could not determine the builder name for this domain.");

                    this.Log.ErrorCtx(LOG_CTX, "Configuration is incomplete.  Unable to send e-mails.");
                    return false;
                }

                PrincipalContext domainContext = new PrincipalContext(ContextType.Domain, this.Config.Studios.ThisStudio.ActiveDirectoryDomain);

                String recipientAddresses = null;
                foreach (IEmailRecipient notificationRecipient in notificationRecipients)
                {
                    String recipientAddress = String.Empty;
                    if (notificationRecipient.Name.Equals("$owner") || notificationRecipient.Name.Equals("$(owner)"))
                    {
                        UserPrincipal user = UserPrincipal.FindByIdentity(domainContext, this.Owner);
                        if (user == null)
                            recipientAddress = String.Format("{0}@{1}", this.Owner, thisStudio.Domain);
                        else
                            recipientAddress = user.EmailAddress;
                    }
                    else if (notificationRecipient.Name.Equals("$associated") || notificationRecipient.Name.Equals("$(associated)") && AssociatedUsernames != null)
                    {
                        String associatedRecipientAddress = String.Empty;
                        foreach (String associatedUsername in AssociatedUsernames)
                        {
                            UserPrincipal user = UserPrincipal.FindByIdentity(domainContext, associatedUsername);
                            if (user == null)
                                associatedRecipientAddress = String.Format("{0}@{1}", associatedUsername, thisStudio.Domain);
                            else
                                associatedRecipientAddress = user.EmailAddress;
                            recipientAddresses = AddRecipient(recipientAddresses, notificationRecipient, recipientAddress);
                        }
                    }
                    else if (notificationRecipient.Email == null)
                    {
                        UserPrincipal user = UserPrincipal.FindByIdentity(domainContext, notificationRecipient.Name);
                        if (user == null)
                            recipientAddress = String.Format("{0}@{1}", this.Owner, thisStudio.Domain);
                        else
                            recipientAddress = user.EmailAddress;

                        if (String.IsNullOrEmpty(recipientAddress))
                        {
                            this.Log.WarningCtx(LOG_CTX, "Email address unresolved (Active Directory?) : Recipient name {0} Recipient Email {1} Domain {2} Owner {3}",
                               notificationRecipient.Name, notificationRecipient.Email, domainContext, this.Owner);
                        }
                    }
                    else
                    {
                        recipientAddress = notificationRecipient.Email;
                    }

                    // Used to translate certain email addresses into others.
                    if (emailRedirects != null)
                    {
                        foreach (KeyValuePair<string, EmailRedirect> redirect in emailRedirects)
                        {
                            Regex match = new Regex(redirect.Key);
                            if (match.IsMatch(recipientAddress))
                            {
                                this.Log.MessageCtx(LOG_CTX, "EMail Redirect - Converting {0} to {1}", recipientAddress, redirect.Value.Email);
                                recipientAddress = redirect.Value.Email;
                            }
                        }
                    }

                    recipientAddresses = AddRecipient(recipientAddresses, notificationRecipient, recipientAddress);
                }

                if (recipientAddresses == null || recipientAddresses.Length == 0)
                {
                    this.Log.WarningCtx(LOG_CTX, "No valid email addresses, no email will be sent.");
                    return false;
                }
                this.Log.MessageCtx(LOG_CTX, "Sending email notification to: " + recipientAddresses + ".");

                try
                {
                    System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                    message.To.Add(recipientAddresses);
                    message.Subject = this.Subject.ToString();

                    message.Priority = this.Priority;
                    message.From = new System.Net.Mail.MailAddress(String.Format("{0}@{1}", thisStudio.BuilderUser, thisStudio.Domain), thisStudio.BuilderName);
                    message.Body = this.Header.ToString() + this.Body.ToString() + this.Footer.ToString();
                    message.IsBodyHtml = Html;
                    message.SubjectEncoding = Encoding.UTF8;

                    using (System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(thisStudio.ExchangeServer))
                    {
                        smtp.UseDefaultCredentials = true;

                        smtp.Send(message);
                    }
                }
                catch (SmtpException e)
                {
                    this.Log.ToolException(e, "Email failed to send");
                }

                return (true);
            }
        }

        private string AddRecipient(String recipientAddresses, IEmailRecipient notificationRecipient, String recipientAddress)
        {

            if (RSG.Pipeline.Services.Email.IsValidEmail(recipientAddress))
            {
                if (recipientAddresses != null)
                    recipientAddresses += ", ";

                recipientAddresses += recipientAddress;
            }
            else
            {
                this.Log.WarningCtx(LOG_CTX, "Invalid email address '{0}' setup for notification recipient {1}.  Please check the Notification has not been sent.  Please check the notification file for the relevant service.",
                    recipientAddress, notificationRecipient.Name);
            }
            return recipientAddresses;
        }
        #endregion
    }
}
