﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Triggers
{

    /// <summary>
    /// 
    /// </summary>
    public interface IChangelistTrigger :
        ITrigger,
        IHasDescription,
        IHasUsername
    {
        #region Properties
        /// <summary>
        /// Associated changelist number.
        /// </summary>
        uint Changelist { get; }

        /// <summary>
        /// Changelist client.
        /// </summary>
        String Client { get; }

        /// <summary>
        /// Monitored paths.
        /// </summary>
        IEnumerable<String> MonitoredPaths { get; }
        #endregion // Properties
    }

} // RSG.Pipeline.Automation.Common.Triggers namespace
