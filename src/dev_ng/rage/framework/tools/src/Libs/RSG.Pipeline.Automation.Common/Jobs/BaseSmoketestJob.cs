﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;
using RSG.Pipeline.Core;
using RSG.SourceControl.Perforce;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Platform;
using RSG.Statistics.Common;

namespace RSG.Pipeline.Automation.Common.Jobs
{

    /// <summary>
    /// Smoketest client side job.
    /// </summary>
    [DataContract]
    public class BaseSmokeTestJob :
        Job,
        IJob
    {
        #region Properties
        /// <summary>
        // A friendly name used to be more descriptive in e-mails or other reports
        /// </summary>
        [DataMember]
        public String FriendlyName { get; set; }

        // These are all config-file specified
        [DataMember]
        public String CommandLine { get; set; }

        /// <summary>
        // Path to the LogFile to be used during job
        /// </summary>
        [DataMember]
        public String LogFile { get; set; }

        /// <summary>
        // 
        /// </summary>
        [DataMember]
        public int Runtime { get; set; }

        /// <summary>
        // A friendly name used to be more descriptive in e-mails or other reports
        /// </summary>
        [DataMember]
        public List<String> Targets { get; set; }

        /// <summary>
        // 
        /// </summary>
        [DataMember]
        public List<String> Artifacts { get; set; }

        /// <summary>
        // 
        /// </summary>
        [DataMember]
        public bool RebootOnFailure { get; set; }

        [DataMember]
        public bool SyncFiles { get; set; }

        /// <summary>
        /// Name of the Build Config.
        /// </summary>
        [DataMember]
        public String BuildConfig { get; private set; }

        /// <summary>
        /// Name of the Platform.
        /// </summary>
        [DataMember]
        public String Platform { get; private set; }

        /// <summary>
        // 
        /// </summary>
        [DataMember]
        public TimeSpan HangTimeout { get; set; }

        /// <summary>
        // 
        /// </summary>
        [DataMember]
        public TimeSpan LogNotFoundTimeout { get; set; }

        /// <summary>
        // 
        /// </summary>
        [DataMember]
        public TimeSpan FatalTimeout { get; set; }

        /// <summary>
        // 
        /// </summary>
        [DataMember]
        public TimeSpan CrashWaitForStartCoreDumpTimeout { get; set; }

        /// <summary>
        // 
        /// </summary>
        [DataMember]
        public TimeSpan CrashWaitForEndCoreDumpTimeout { get; set; }

        /// <summary>
        // 
        /// </summary>
        [DataMember]
        public SmokeTesterAnalysisType AnalysisType { get; set; }

        /// <summary>
        // 
        /// </summary>
        [DataMember]
        public String OutputDir { get; set; }

        /// <summary>
        // 
        /// </summary>
        [DataMember]
        public String ExePath { get; set; }

        /// <summary>
        /// The original source code change.
        /// </summary>
        [DataMember]
        public uint Changelist { get; set; }

        /// <summary>
        /// The change when the Codebuilder Executables were submitted
        /// </summary>
        [DataMember]
        public uint CodeBuilderChangelist { get; set; }

        /// <summary>
        /// The change that rage source is currently at during processing
        /// </summary>
        [DataMember]
        public uint RageCL { get; set; }

        /// <summary>
        /// If a shelved changelist is used to trigger this build. This String can be empty
        /// </summary>
        [DataMember]
        public uint ShelvedCL { get; set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Default constructor (for deserialisation).
        /// </summary>
        public BaseSmokeTestJob()
            : base()
        {
            Targets = new List<String>();
            Artifacts = new List<String>();
            FriendlyName = "";
            Role = CapabilityType.SmokeTester;
        }

        /// <summary>
        /// Constructor used to populate all of the temporary CL data. This data should only be stored per SmokeTest specific Job
        /// In order to keep the BaseSmokeTest clean. Should remove once associated Changelists are properly handled.
        /// </summary>
        public BaseSmokeTestJob(XElement xmlJobElement, ITrigger trigger, CapabilityType role, RSG.Platform.Platform platform, BuildConfig buildConfig, IBranch branch, uint exeCL, uint rageCL, uint originalCL, uint shelvedCL=0)
            : this(xmlJobElement, trigger, role, platform, buildConfig, branch)
        {
            CodeBuilderChangelist = exeCL;
            Changelist = originalCL;
            RageCL = rageCL;
            ShelvedCL = shelvedCL;
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public BaseSmokeTestJob(XElement xmlJobElement, ITrigger trigger, CapabilityType role, RSG.Platform.Platform platform, BuildConfig buildConfig, IBranch branch)
            : base(branch, role, trigger)
        {
            Platform = platform.ToString();
            BuildConfig = buildConfig.ToString();

            XElement commandLineElement = xmlJobElement.Element("CommandLine");
            if (commandLineElement != null)
            {
                CommandLine = Environment.ExpandEnvironmentVariables(commandLineElement.Value);
            }
            else 
            {
                Log.Log__Error("Error creating SmoketestJob from XML. Missing <CommandLine>");
            }

            XElement friendlyNameElement = xmlJobElement.Element("FriendlyName");
            if (friendlyNameElement != null)
            {
                FriendlyName = friendlyNameElement.Value;
            }
            else
            {
                Log.Log__Error("Error creating SmokeTestJob from XML. Missing <FriendlyName>");
            }

            XElement logFileElement = xmlJobElement.Element("LogFile");
            if (logFileElement != null)
            {
                LogFile = Environment.ExpandEnvironmentVariables(logFileElement.Value);
            }
            else 
            {
                Log.Log__Error("Error creating SmoketestJob from XML. Missing <LogFile>");
            }

            XElement runtime = xmlJobElement.Element("Runtime");
            int runtimeNum = 0;
            if (runtime != null && int.TryParse(runtime.Value, out runtimeNum))
            {
                Runtime = runtimeNum;
            }
            else
            {
                Log.Log__Error("Error creating SmoketestJob from XML. Missing <Runtime>");
            }

            Targets = new List<String>();
            XElement targetsElement = xmlJobElement.Element("Targets");
            IEnumerable<XElement> targetList = targetsElement.Descendants("Target");
            if (targetList != null)
            {
                targetList.ForEach(f => Targets.Add(Environment.ExpandEnvironmentVariables(f.Value)));
            }

            if (Targets.Count <= 0)
            {
                Log.Log__Error("Error creating SmoketestJob from XML. No targets specified. Add <Target>.");
            }

            Artifacts = new List<String>();
            IEnumerable<XElement> artifactList = xmlJobElement.Descendants("Artifact");
            if (artifactList != null)
            {
                artifactList.ForEach(f => Artifacts.Add(Environment.ExpandEnvironmentVariables(f.Value)));
            }

            XElement rebootElement = xmlJobElement.Element("RebootOnFailure");
            bool rebootOnFailure = false;
            if (rebootElement != null && bool.TryParse(rebootElement.Value, out rebootOnFailure))
            {
                RebootOnFailure = rebootOnFailure;
            }
            else
            {
                Log.Log__Error("Error creating SmoketestJob from XML. Missing <RebootOnFailure>");
            }

            XElement hangTimeoutElement = xmlJobElement.Element("HangTimeout");
            TimeSpan hangTimeout = new TimeSpan(0, 2, 0);
            if (hangTimeoutElement != null && TimeSpan.TryParse(hangTimeoutElement.Value, out hangTimeout))
            {
                HangTimeout = hangTimeout;
            }
            else
            {
                Log.Log__Error("Error creating SmoketestJob from XML. Missing <HangTimeout>");
            }

            XElement fatalTimeoutElement = xmlJobElement.Element("FatalTimeout");
            TimeSpan fatalTimeout = new TimeSpan(0, 2, 0);
            if (fatalTimeoutElement != null && TimeSpan.TryParse(fatalTimeoutElement.Value, out fatalTimeout))
            {
                FatalTimeout = fatalTimeout;
            }
            else
            {
                Log.Log__Error("Error creating SmoketestJob from XML. Missing <FatalTimeout>");
            }

            XElement logNotFoundTimeoutElement = xmlJobElement.Element("LogNotFoundTimeout");
            TimeSpan logNotFoundTimeout = new TimeSpan(0, 2, 0);
            if (logNotFoundTimeoutElement != null && TimeSpan.TryParse(logNotFoundTimeoutElement.Value, out logNotFoundTimeout))
            {
                LogNotFoundTimeout = LogNotFoundTimeout;
            }
            else
            {
                Log.Log__Error("Error creating SmoketestJob from XML. Missing <LogNotFoundTimeout>");
            }

            XElement crashWaitForStartCoreDumpTimeoutElement = xmlJobElement.Element("CrashWaitForStartCoreDumpTimeout");
            TimeSpan crashWaitForStartCoreDumpTimeout = new TimeSpan(0, 3, 0);
            if (crashWaitForStartCoreDumpTimeoutElement != null && 
                TimeSpan.TryParse(crashWaitForStartCoreDumpTimeoutElement.Value, out crashWaitForStartCoreDumpTimeout))
            {
                CrashWaitForStartCoreDumpTimeout = crashWaitForStartCoreDumpTimeout;
            }
            else
            {
                Log.Log__Error("Error creating SmoketestJob from XML. Missing <CrashWaitForStartCoreDumpTimeout>");
            }

            XElement crashWaitForEndCoreDumpTimeoutElement = xmlJobElement.Element("CrashWaitForEndCoreDumpTimeout");
            TimeSpan crashWaitForEndCoreDumpTimeout = new TimeSpan(0, 5, 0);
            if (crashWaitForEndCoreDumpTimeoutElement != null && 
                TimeSpan.TryParse(crashWaitForEndCoreDumpTimeoutElement.Value, out crashWaitForEndCoreDumpTimeout))
            {
                CrashWaitForEndCoreDumpTimeout = crashWaitForEndCoreDumpTimeout;
            }
            else
            {
                Log.Log__Error("Error creating SmoketestJob from XML. Missing <CrashWaitForEndCoreDumpTimeout>");
            }

            XElement analysisTypeElement = xmlJobElement.Element("AnalysisType");
            if (analysisTypeElement != null)
            {
                AnalysisType = (SmokeTesterAnalysisType)Enum.Parse(typeof(SmokeTesterAnalysisType), analysisTypeElement.Value);
            }
            else
            {
                Log.Log__Error("Error creating SmoketestJob from XML. Missing <AnalysisType>");
                AnalysisType = SmokeTesterAnalysisType.QuickMetrics;
            }

            XElement outputDirElement = xmlJobElement.Element("OutputDir");
            if (outputDirElement != null)
            {
                OutputDir = Environment.ExpandEnvironmentVariables(outputDirElement.Value);
            }
            else
            {
                Log.Log__Error("Error creating SmoketestJob from XML. Missing <OutputDir>");
            }
        }
        #endregion // Constructors
        
        #region Controller Methods

        /// <summary>
        /// Serialise job representation to XML.
        /// </summary>
        /// <returns></returns>
        public override XElement Serialise()
        {
            XElement xmlJob = base.Serialise();
            xmlJob.Add(new XElement("CommandLine", this.CommandLine));
            xmlJob.Add(new XElement("FriendlyName", this.FriendlyName));
            xmlJob.Add(new XElement("LogFile", this.LogFile));
            xmlJob.Add(new XElement("OutputDir", this.OutputDir));     
            xmlJob.Add(new XElement("ExePath", this.ExePath));
            xmlJob.Add(new XElement("Runtime", this.Runtime));
            xmlJob.Add(new XElement("Changelist", this.Changelist));
            xmlJob.Add(new XElement("CodeBuilderChangelist", this.CodeBuilderChangelist));
            xmlJob.Add(new XElement("RageChangelist", this.RageCL));
            xmlJob.Add(new XElement("ShelvedChangelist", this.ShelvedCL));
            xmlJob.Add(new XElement("AnalysisType", this.AnalysisType.ToString()));
            xmlJob.Add(new XElement("Platform", this.Platform));
            xmlJob.Add(new XElement("BuildConfig", this.BuildConfig));

            if (this.Targets != null)
            { 
                List<XElement> targetListEles = new List<XElement>();
                foreach( String file in Targets)
                {
                    targetListEles.Add(new XElement("Target", file));
                }
                xmlJob.Add(new XElement("Targets", targetListEles.ToArray()));
            }

            if (this.Artifacts != null)
            {
                List<XElement> artifactListEles = new List<XElement>();
                foreach (String file in Artifacts)
                {
                    artifactListEles.Add(new XElement("Artifact", file));
                }
                xmlJob.Add(new XElement("Artifacts", artifactListEles.ToArray()));
            }

            xmlJob.Add(new XElement("RebootOnFailure", this.RebootOnFailure));
            return (xmlJob);
        }

        /// <summary>
        /// Deserialise job representation from XML.
        /// </summary>
        /// <param name="xmlJobElem"></param>
        /// <returns></returns>
        public override bool Deserialise(XElement xmlJobElem)
        {
            bool result = true;
            try
            {
                result &= base.Deserialise(xmlJobElem);
                this.CommandLine = Environment.ExpandEnvironmentVariables((String)xmlJobElem.XPathSelectElement("CommandLine").Value);
                this.FriendlyName = Environment.ExpandEnvironmentVariables((String)xmlJobElem.XPathSelectElement("FriendlyName").Value);
                this.LogFile = Environment.ExpandEnvironmentVariables((String)xmlJobElem.XPathSelectElement("LogFile").Value);
                this.OutputDir = Environment.ExpandEnvironmentVariables((String)xmlJobElem.XPathSelectElement("OutputDir").Value);                
                this.ExePath = Environment.ExpandEnvironmentVariables((String)xmlJobElem.XPathSelectElement("ExePath").Value);
                this.AnalysisType = (SmokeTesterAnalysisType)Enum.Parse(typeof(SmokeTesterAnalysisType), (String)xmlJobElem.XPathSelectElement("AnalysisType").Value);
                this.Platform = Environment.ExpandEnvironmentVariables((String)xmlJobElem.XPathSelectElement("Platform").Value);
                this.BuildConfig= Environment.ExpandEnvironmentVariables((String)xmlJobElem.XPathSelectElement("BuildConfig").Value);

                // BaseSmokeTestJob used to have its own 'Branch' string property, but now that lives in the base Job class.  So if that's not
                // set, see if there's an old Branch value for this job and use that.
                if (this.BranchName == String.Empty) { this.BranchName = Environment.ExpandEnvironmentVariables((String)xmlJobElem.XPathSelectElement("Branch").Value); }

                XElement xmlJobSubEle = xmlJobElem.XPathSelectElement("Targets");
                if (xmlJobSubEle != null)
                {
                    foreach (XElement targetEle in xmlJobElem.Descendants("Target"))
                    {
                        this.Targets.Add(targetEle.Value);
                    }
                }

                xmlJobSubEle = xmlJobElem.XPathSelectElement("Artifacts");
                if (xmlJobSubEle != null)
                {
                    foreach (XElement artifactEle in xmlJobElem.Descendants("Artifact"))
                    {
                        this.Artifacts.Add(artifactEle.Value);
                    }
                }

                uint changelist = 0;
                if ( xmlJobElem.XPathSelectElement("Changelist") != null )
                    uint.TryParse(xmlJobElem.XPathSelectElement("Changelist").Value, out changelist);
                this.Changelist = changelist;

                uint codeBuilderChangeList = 0;
                if (xmlJobElem.XPathSelectElement("CodeBuilderChangelist") != null)
                    uint.TryParse(xmlJobElem.XPathSelectElement("CodeBuilderChangelist").Value, out codeBuilderChangeList);
                this.CodeBuilderChangelist = codeBuilderChangeList;

                uint rageChangeList = 0;
                if (xmlJobElem.XPathSelectElement("RageChangelist") != null)
                    uint.TryParse(xmlJobElem.XPathSelectElement("RageChangelist").Value, out rageChangeList);
                this.RageCL = rageChangeList;

                uint shelvedChangeList = 0;
                if (xmlJobElem.XPathSelectElement("ShelvedChangelist") != null)
                    uint.TryParse(xmlJobElem.XPathSelectElement("ShelvedChangelist").Value, out shelvedChangeList);
                this.ShelvedCL = shelvedChangeList;

                int runtime = 0;
                if (xmlJobElem.XPathSelectElement("Runtime") != null)
                    int.TryParse(xmlJobElem.XPathSelectElement("Runtime").Value, out runtime);
                this.Runtime = runtime;
                
                bool rebootOnFailure = false;
                if ( xmlJobElem.XPathSelectElement("RebootOnFailure") != null )
                    bool.TryParse(xmlJobElem.XPathSelectElement("RebootOnFailure").Value, out rebootOnFailure);
                this.RebootOnFailure = rebootOnFailure;
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Error deserialising Smoketest Job.");
                result = false;
            }
            return (result);
        }
        #endregion // Controller Methods
    }
}
