﻿using System;
using System.Runtime.Serialization;

namespace RSG.Pipeline.Automation.Common.Jobs
{

    /// <summary>
    /// Job priority enumeration.
    /// </summary>
    [DataContract]
    public enum JobPriority
    {
        /// <summary>
        /// Job with normal priority.
        /// </summary>
        [EnumMember]
        Normal,

        /// <summary>
        /// Job with elevated priority.
        /// </summary>
        [EnumMember]
        High,
    }

} // RSG.Pipeline.Automation.Common.Jobs namespace
