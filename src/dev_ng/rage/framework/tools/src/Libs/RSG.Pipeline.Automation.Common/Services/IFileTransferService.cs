﻿using System;
using System.ServiceModel;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Services.Messages;

namespace RSG.Pipeline.Automation.Common.Services
{

    /// <summary>
    /// File transfer service; to/from main automation server.
    /// </summary>
    /// See http://bartwullems.blogspot.co.uk/2011/01/streaming-files-over-wcf.html.
    [ServiceContract]
    public interface IFileTransferService
    {
        /// <summary>
        /// Upload a file.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        TransferResponse UploadFile(RemoteFile request);

        /// <summary>
        /// Download a file.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        RemoteFile DownloadFile(RemoteFileRequest request);

        /// <summary>
        /// Read information about the available files.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Obsolete("Use FileAvailability2 - more file information is provided in an extensible way.")]
        [OperationContract]
        RemoteFileAvailability FileAvailability(RemoteFileAvailabilityRequest request);

        /// <summary>
        /// Read information about the available files.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [OperationContract]
        RemoteFileAvailability2 FileAvailability2(RemoteFileAvailabilityRequest request);

        /// <summary>
        /// Return directory used for files transferred to client.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        [ServiceKnownType(typeof(Job))]
        [ServiceKnownType(typeof(MapExportJob))]
        [ServiceKnownType(typeof(AssetBuilderJob))]
        [ServiceKnownType(typeof(CodeBuilderJob))]
        [ServiceKnownType(typeof(CodeBuilder2Job))]
        [ServiceKnownType(typeof(ScriptBuilderJob))]
        [OperationContract]
        String GetClientFileDirectory(IJob job);
    }

} // RSG.Pipeline.Automation.Common.Services namespace
