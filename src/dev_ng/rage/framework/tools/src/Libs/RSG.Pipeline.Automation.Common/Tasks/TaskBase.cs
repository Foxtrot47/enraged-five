﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Collections;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Interop.Bugstar;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Services;
using RSG.Pipeline.Automation.Common.Notifications;
using RSG.Pipeline.Automation.Common.Client;
using RSG.Pipeline.Automation.Common.Notifications.Alerts;
using RSG.Pipeline.Automation.Common.Utils;
using RSG.Pipeline.Automation.Common.Bug;
using RSG.Base.Configuration.Bugstar;
using RSG.Pipeline.Automation.Common.Services.Messages;
using System.Text.RegularExpressions;
using RSG.Pipeline.Automation.Common.Labels;
using P4API;
using RSG.Base.IO;
using System.Runtime.InteropServices;


namespace RSG.Pipeline.Automation.Common.Tasks
{
    /// <summary>
    /// Abstract base task class.
    /// </summary>
    public abstract class TaskBase : ITask
    {
        #region Constants
        /// <summary>
        /// Log context.
        /// </summary>
        protected static readonly String LOG_CTX = "Task";

        /// <summary>
        /// Window title string that is shown alongside a task when it is stopped.
        /// </summary>
        private static readonly String TASK_STOPPED = "( *STOPPED* )";

        // Auto syncing of tools config
        public static readonly String PARAM_AUTO_SYNC_TOOLS_CONFIG = "Auto Sync Tools Config";
        public static readonly String PARAM_AUTO_SYNC_CONTENT_TREE = "Auto Sync Content Tree";

        // Bugstar Login Parameters
        public static readonly String PARAM_BUGSTAR_LOGIN_USERNAME = "Bugstar Login Username";
        public static readonly String PARAM_BUGSTAR_LOGIN_PASSWORD = "Bugstar Login Password";
        public static readonly String PARAM_BUGSTAR_LOGIN_DOMAIN = "Bugstar Login Domain";

        // Task priority 
        public static readonly String PARAM_TASK_PRIORITY = "Task Priority";

        /// <summary>
        /// Parameter to indicate that instead of just reloading dispatch a sync to p4 should be done first
        /// </summary>
        public static readonly String PARAM_SYNC_DISPATCH = "Sync Dispatch";

        #endregion // Constants

        #region Properties
        /// <summary>
        /// Task name string.
        /// </summary>
        public String Name
        {
            get;
            private set;
        }

        /// <summary>
        /// Task description string.
        /// </summary>
        public String Description
        {
            get;
            private set;
        }

        /// <summary>
        /// Role of this server-side task.
        /// </summary>
        public CapabilityType Role
        {
            get;
            private set;
        }

        /// <summary>
        /// Task priority.
        /// </summary>
        public TaskPriority Priority 
        { 
            get; 
            private set; 
        }

        /// <summary>
        /// Configuration object.
        /// </summary>
        public IConfig Config
        {
            get;
            private set;
        }

        /// <summary>
        /// Jobs to invoke for this task.
        /// </summary>
        /// Get accessor returns a copy as other threads are modifying that 
        /// collection.
        /// 
        public IEnumerable<IJob> Jobs
        {
            get 
            {
                lock (m_Jobs)
                {
                    IJob[] jobs = new IJob[this.m_Jobs.Count];
                    this.m_Jobs.Select(j => j.Value).ToArray().CopyTo(jobs, 0);
                    return (jobs);
                }
            }
        }
        protected PriorityQueue<JobPriority, IJob> m_Jobs;

        /// <summary>
        /// Convenience property for pending jobs.
        /// </summary>
        public IEnumerable<IJob> PendingJobs
        {
            get
            {
                return (Jobs.Where(j => JobState.Pending == j.State));
            }
        }

        /// <summary>
        /// Convenience property for assigned and pending jobs.
        /// </summary>
        public IEnumerable<IJob> OutstandingJobs
        {
            get
            {
                return (Jobs.Where(j => (JobState.Pending == j.State || JobState.Assigned == j.State)));
            }
        }

        /// <summary>
        /// Jobs results for this task.
        /// </summary>
        /// Get accessor returns a copy as other threads are modifying that 
        /// collection.
        /// 
        public IEnumerable<IJobResult> JobResults
        {
            get
            {
                lock (m_JobResults)
                {
                    IJobResult[] jobResults = new IJobResult[this.m_JobResults.Count];
                    this.m_JobResults.CopyTo(jobResults, 0);
                    return (jobResults);
                }
            }
        }
        protected ICollection<IJobResult> m_JobResults;


        /// <summary>
        /// Filename used for saving/loading state.
        /// </summary>
        public String Filename
        {
            get;
            private set;
        }

        /// <summary>
        /// Log object.
        /// </summary>
        protected IUniversalLog Log
        {
            get;
            private set;
        }

        /// <summary>
        /// Email recipients.
        /// </summary>
        public IEnumerable<IEmailRecipient> EmailRecipients
        {
            get { return m_EmailRecipients; }
        }
        private IEnumerable<IEmailRecipient> m_EmailRecipients;

        /// <summary>
        /// Bug recipients.
        /// </summary>
        public IEnumerable<IBugRecipient> BugRecipients
        {
            get { return m_BugRecipients; }
        }
        private IEnumerable<IBugRecipient> m_BugRecipients;

        /// <summary>
        /// The task labels that the task will try to set.
        /// </summary>
        public IEnumerable<TaskLabel> TaskLabels
        {
            get;
            private set;
        }

        /// <summary>
        /// Command options object.
        /// </summary>
        protected CommandOptions Options
        {
            get;
            private set;
        }

        /// <summary>
        /// Task parameters.
        /// </summary>
        public IDictionary<String, Object> Parameters 
        { 
            get; 
            protected set; 
        }

        public IDictionary<string, EmailRedirect> EmailRedirects
        {
            get { return m_EmailRedirects; }
        }
        private IDictionary<string, EmailRedirect> m_EmailRedirects;

        /// <summary>
        /// The state of the task
        /// </summary>
        public TaskState TaskState
        {
            get;
            protected set;
        }

        /// <summary>
        /// Md5 checksum for the last label file loaded.
        /// </summary>
        private byte[] LabelFileMd5 { get; set; }

        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="role"></param>
        public TaskBase(String name, String description, CapabilityType role, CommandOptions options)
        {
            this.TaskState = TaskState.Unknown;
            this.Name = name;
            this.Description = description;
            this.Role = role;
            this.Config = options.Config;
            this.m_Jobs = new PriorityQueue<JobPriority, IJob>(new JobPriorityComparer());
            this.m_JobResults = new List<IJobResult>();
            this.Filename = Path.Combine(this.Config.ToolsConfig, "automation",
                "local", String.Format("{0}.xml", this.Name));
            LogFactory.Initialize();
            this.Log = LogFactory.CreateUniversalLog(this.Name);
            this.m_EmailRecipients = new List<IEmailRecipient>();
            this.m_EmailRedirects = new Dictionary<string, EmailRedirect>();
            this.m_BugRecipients = new List<IBugRecipient>();
            this.Options = options;
            this.Parameters = new Dictionary<String, Object>();
            this.Priority = TaskPriority.Normal;
            this.TaskLabels = new List<TaskLabel>();
            this.LabelFileMd5 = null;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Start task update
        /// </summary>
        public virtual void Start() 
        {
            TaskState = TaskState.Active;
            Console.Title = Console.Title.Replace(String.Format("{0}{1}", this.Role, TASK_STOPPED), this.Role.ToString());
            Console.BackgroundColor = ConsoleColor.Black;
        }

        /// <summary>
        /// Stop task update
        /// </summary>
        public virtual void Stop()
        {
            TaskState = TaskState.Inactive;
            Console.Title = Console.Title.Replace(String.Format("{0}{1}", this.Role, TASK_STOPPED), this.Role.ToString());
            Console.Title = Console.Title.Replace(this.Role.ToString(), String.Format("{0}{1}", this.Role, TASK_STOPPED));
        }

        /// <summary>
        /// Update task; returning a series of Jobs (can be empty) to the IServer.
        /// </summary>
        public abstract void Update();

        /// <summary>
        /// Enqueue an externally requested job onto a task queue.
        /// </summary>
        /// <param name="job"></param>
        /// This should be used for rebuild functionality for task queues.
        /// 
        public virtual void EnqueueJob(IJob job)
        {
            lock (this.m_Jobs)
            {
                this.m_Jobs.Enqueue(job.Priority, job);
            }
        }

        /// <summary>
        /// Automation service notification to ITask that job is complete.
        /// </summary>
        /// <param name="job"></param>
        /// This is invoked prior to the SendNotifications call.
        public virtual void JobCompleted(IJob job, IEnumerable<IJobResult> jobResults) 
        {
            lock (this.m_JobResults)
            {
                foreach (IJobResult jobResult in jobResults)
                {
                    this.m_JobResults.Add(jobResult);
                }
            }
        }

        /// <summary>
        /// Send pre job completion notifications
        /// </summary>
        /// <param name="job"></param>
        /// <param name="workerUri"></param>
        /// <param name="workerGuid"></param>
        public virtual void SendPreJobNotifications(IJob job, Uri workerUri, Guid workerGuid)
        {
            LoadNotificationData();
            String jobUser = String.Empty;

            IEnumerable<String> files = job.Trigger.TriggeringFiles();
            FileMapping[] fms = new FileMapping[0];
            
            if (files.Any())
            {
                using (P4 p4 = this.Config.Project.SCMConnect())
                {
                    if (p4 != null)
                    {
                        fms = FileMapping.Create(p4, files.ToArray());
                    }
                }

            }

            List<IEmailRecipient> sendList = new List<IEmailRecipient>();
            foreach (IEmailRecipient notificationRecipient in EmailRecipients)
            {
                if (this.NotificationRequired(job, notificationRecipient, fms))
                    sendList.Add(notificationRecipient);
            }

            if (sendList.Count > 0)
            {
                PreJobEmailNotification(sendList, job, workerUri, workerGuid);
            }
        }

        /// <summary>
        /// Send assigned job completion notifications
        /// </summary>
        /// <param name="job"></param>
        /// <param name="workerUri"></param>
        /// <param name="workerGuid"></param>
        public virtual void SendAssignedJobNotifications(IJob job, Uri workerUri, Guid workerGuid)
        {
            LoadNotificationData();
            String jobUser = String.Empty;

            IEnumerable<String> files = job.Trigger.TriggeringFiles();
            FileMapping[] fms = new FileMapping[0];

            if (files.Any())
            {
                using (P4 p4 = this.Config.Project.SCMConnect())
                {
                    if (p4 != null)
                    {
                        fms = FileMapping.Create(p4, files.ToArray());
                    }
                }

            }

            List<IEmailRecipient> sendList = new List<IEmailRecipient>();
            foreach (IEmailRecipient notificationRecipient in EmailRecipients)
            {
                if (this.NotificationRequired(job, notificationRecipient, fms))
                    sendList.Add(notificationRecipient);
            }

            if (sendList.Count > 0)
            {
                AssignedJobEmailNotification(sendList, job, workerUri, workerGuid);
            }
        }
 
        /// <summary>
        /// Send post job completion notifications
        /// </summary>
        /// <param name="job"></param>
        /// <param name="workerUri"></param>
        /// <param name="workerGuid"></param>
        public virtual void SendPostJobNotifications(IJob job, IEnumerable<IJobResult> jobResults, Uri workerUri, Guid workerGuid, String logFilename)
        {
            LoadNotificationData();

            FileMapping[] fms = new FileMapping[0];
            IEnumerable<String> files = job.Trigger.TriggeringFiles();

            using (P4 p4 = this.Config.Project.SCMConnect())
            {
                if (p4 != null)
                {
                    fms = FileMapping.Create(p4, files.ToArray());
                }
            }

            List<IEmailRecipient> sendList = new List<IEmailRecipient>();
            foreach (IEmailRecipient notificationRecipient in EmailRecipients)
            {
                if (this.NotificationRequired(job, notificationRecipient, fms))
                {
                    sendList.Add(notificationRecipient);
                }
            }

            try
            {
                // Create bug(s) if required, return the bugids created
                IEnumerable<uint> bugIds = this.PostJobBugNotification(job, jobResults, workerUri, workerGuid, logFilename);

                // Create emails if required - pass the bugids so the email can show which bugs were raised.
                this.PostJobEmailNotification(sendList, bugIds, job, jobResults, workerUri, workerGuid, logFilename);
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                foreach (IEmailRecipient recipient in sendList)
                {
                    sb.Append(String.Format("Name :{0}", recipient.Name));
                    sb.Append(String.Format("Email :{0}", recipient.Email));
                }

     //           String details = String.Format("Error sending Post Job Notification job {0}, Derek Ward wants to know about these for B* 1358050 : Details : {1}", job.ID, sb);

//                GeneralAlert(this.Log, details);
                this.Log.ToolExceptionCtx(LOG_CTX, ex, "Error sending post job notification to ", sendList);
            }
        }

        /// <summary>
        /// Return status of task.
        /// </summary>
        /// <returns></returns>
        public abstract TaskStatus Status();

        /// <summary>
        /// After a job has finished does the task think the job requires postjob notification?
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public virtual bool PostJobNotificationRequired(IJob job)
        {
            return true;
        }

        /// <summary>
        /// Set tasks current job (if it keeps track).
        /// </summary>
        /// <param name="job"></param>
        public virtual void SetCurrentJob(IJob job) { }
        
        /// <summary>
        /// Load state from disk.
        /// </summary>
        public virtual void Load()
        {
            this.Log.MessageCtx(LOG_CTX, "Loading state from {0}.", this.Filename);
            try
            {
                if (!File.Exists(this.Filename))
                {
                    this.Start();
                    return; // No state to load.
                }

                XDocument xmlDoc = XDocument.Load(this.Filename);
                String typeName = ((IEnumerable)xmlDoc.Root.XPathEvaluate("/Task/@type")).
                    Cast<XAttribute>().First().Value;
                Debug.Assert(0 == String.Compare(this.GetType().Name, typeName),
                    String.Format("Loading task status information from incorrect file: {0} {1}.",
                    this.Name, this.Filename));

                LoadJobResults(xmlDoc);

                LoadJobs(xmlDoc);

                LoadState(xmlDoc);
            }
            catch (Exception ex)
            {
                this.Log.ToolExceptionCtx(LOG_CTX, ex, "Error deserialising status from disk: {0}.",
                    this.Filename);
            }
        }

        /// <summary>
        /// Save state to disk.
        /// </summary>
        public virtual void Save()
        {
            this.Log.MessageCtx(LOG_CTX, "Saving state to {0}.", this.Filename);
            try
            {
                XDocument xmlDoc = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "yes"),
                    new XElement("Task",
                        new XAttribute("name", this.Name),
                        new XAttribute("type", this.GetType().Name),
                        new XElement("State",  this.TaskState.ToString()),
                        new XElement("Jobs", this.Jobs.Select(j => j.Serialise())),
                        new XElement("JobResults", this.JobResults.Select(jr => jr.Serialise())))
                );

                String directoryName = Path.GetDirectoryName(this.Filename);
                if (!Directory.Exists(directoryName))
                    Directory.CreateDirectory(directoryName);
                lock (this.Filename)
                {
                    xmlDoc.Save(this.Filename);
                }
            }
            catch (Exception ex)
            {
                this.Log.ExceptionCtx(LOG_CTX, ex, "Error serialising status to disk: {0}.",
                    this.Filename);
            }
        }

        /// <summary>
        /// Skip the processing of a job.
        /// </summary>
        /// <param name="id"></param>
        public virtual bool Skip(Guid id)
        {
            IJob job = this.GetJob(id);
            if (null != job)
            {
                if (JobState.Pending == job.State)
                {
                    job.State = JobState.Skipped;
                    return true;
                }
                else
                {
                    this.Log.ErrorCtx(LOG_CTX, "Job ID {0} is already in state {1}", id, job.State.ToString());
                }
            }
            else
            {
                this.Log.ErrorCtx(LOG_CTX, "No job on task with ID {0}", id);
            }
            return false;
        }

        /// <summary>
        /// Prioritise processing of a job.
        /// </summary>
        /// <param name="id"></param>
        public virtual bool Prioritise(Guid id)
        {
            lock (this.m_Jobs)
            {
                KeyValuePair<JobPriority, IJob> kvp = this.m_Jobs.Where(j => j.Value.ID == id).FirstOrDefault();
                IJob job = kvp.Value;
                if (null != job)
                {
                    this.m_Jobs.Remove(kvp);
                    job.Priority = JobPriority.High;
                    this.m_Jobs.Enqueue(job.Priority, job);
                    return (true);
                }
                else
                {
                    this.Log.ErrorCtx(LOG_CTX, "No job on task with ID {0}", id);
                    return (false);
                }
            }
        }

        /// <summary>
        /// Abort processing of a job.
        /// </summary>
        /// <param name="id"></param>
        public virtual bool Abort(Guid id)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Reprocess a job.
        /// </summary>
        /// <param name="id"></param>
        public virtual bool Reprocess(Guid id)
        {
            IJob job = this.GetJob(id);
            if (null != job)
            {
                if (JobState.Pending != job.State)
                {
                    job.State = JobState.Pending;
                }
                else
                {
                    this.Log.WarningCtx(LOG_CTX, "Job ID {0} is still pending.  Status has not been changed.", id);
                }
            }
            else
            {
                this.Log.ErrorCtx(LOG_CTX, "No job on task with ID {0}", id);
            }
            return false;
        }

        /// <summary>
        /// File containing information required to send notifications.
        /// </summary>
        /// <returns></returns>
        public virtual String GetNotificationDataFile()
        {
            return (String.Empty);
        }


        /// <summary>
        /// File containing information required to set labels.
        /// </summary>
        /// <returns></returns>
        public virtual String GetLabelDataFile()
        {
            return (String.Empty);
        }

        /// <summary>
        /// Default implementation of job retrieval function.
        /// </summary>
        /// <param name="machinename"></param>
        /// <returns></returns>
        public virtual IJob GetSuitableJob(String machinename)
        {
            if (!CanGetSuitableJob(machinename))
            {
                return null;
            }

            lock (this.m_Jobs)
            {
                return (this.PendingJobs.FirstOrDefault());
            }
        }

        /// <summary>
        /// Can a job possibly be returned from the task?
        /// </summary>
        /// <param name="machinename"></param>
        /// <returns></returns>
        public virtual bool CanGetSuitableJob(String machinename)
        {
            if (this.TaskState != TaskState.Active)
            {
                Log.MessageCtx(LOG_CTX, "No Job given to host {0} as the task {1} is {2}", machinename, this.Name, this.TaskState);
                return false;
            }

            return true;
        }
		
		/// <summary>
        /// Exception Alert
        /// </summary>
        /// <param name="log"></param>
        /// <param name="exception"></param>
        /// <param name="detail"></param>
        public virtual void ExceptionAlert(ILog log, Exception exception, string summary = null, string detail = null)
        {
            LoadNotificationData();

            ICollection<string> details = TaskAlertAdditionalInfo(ref summary, detail);

            List<IEmailRecipient> notificationRecipients = new List<IEmailRecipient>();
            foreach (IEmailRecipient notificationRecipient in EmailRecipients)
            {
                if (notificationRecipient.Rule.HasFlag(NotificationRule.OnExceptions))
                {
                    notificationRecipients.Add(notificationRecipient);
                }
            }

            if (notificationRecipients.Any())
            {
                ExceptionAlertNotification notifier = new ExceptionAlertNotification(exception, log, this.Options, summary, details);
                notifier.Send(notificationRecipients, EmailRedirects);
            }
        }

        /// <summary>
        /// Shutdown Alert
        /// </summary>
        /// <param name="log"></param>
        /// <param name="exception"></param>
        /// <param name="detail"></param>
        public virtual void ShutdownAlert(ILog log, ShutdownType shutdownType, string summary = null, string detail = null)
        {
            LoadNotificationData();

			List<IEmailRecipient> notificationRecipients = new List<IEmailRecipient>();
            foreach (IEmailRecipient notificationRecipient in EmailRecipients)
            {
                if (notificationRecipient.Rule.HasFlag(NotificationRule.OnShutdown))
                {
                    notificationRecipients.Add(notificationRecipient);
                }
            }

            if (notificationRecipients.Any())
            {
	            ICollection<string> details = TaskAlertAdditionalInfo(ref summary, detail);
                ShutdownAlertNotification notifier = new ShutdownAlertNotification(log, this.Options, shutdownType, summary, details);
                notifier.Send(notificationRecipients, EmailRedirects);
            }
        }

        /// <summary>
        /// Startup Alert
        /// </summary>
        /// <param name="log"></param>
        /// <param name="exception"></param>
        /// <param name="detail"></param>
        public virtual void StartupAlert(ILog log, string summary = null, string detail = null)
        {
            LoadNotificationData();

			List<IEmailRecipient> notificationRecipients = new List<IEmailRecipient>();
            foreach (IEmailRecipient notificationRecipient in EmailRecipients)
            {
                if (notificationRecipient.Rule.HasFlag(NotificationRule.OnStartup))
                {
                    notificationRecipients.Add(notificationRecipient);
                }
            }

			if (notificationRecipients.Any())
            {
	            ICollection<string> details = TaskAlertAdditionalInfo(ref summary, detail);
                StartupAlertNotification notifier = new StartupAlertNotification(log, this.Options, summary, details);
                notifier.Send(notificationRecipients, EmailRedirects);
            }
        }

        /// <summary>
        /// General Alert
        /// </summary>
        /// <param name="log"></param>
        /// <param name="exception"></param>
        /// <param name="detail"></param>
        public virtual void GeneralAlert(ILog log, string summary = null, string detail = null, IEnumerable<String> extraNotificationRecipients = null)
        {
            LoadNotificationData();

			List<IEmailRecipient> notificationRecipients = new List<IEmailRecipient>();

            if (extraNotificationRecipients != null)
            {
                foreach (String extra in extraNotificationRecipients)
                {
                    IEmailRecipient extraRecipient = new NotificationRecipient(extra, null, NotificationRule.OnGeneralAlert);
                    notificationRecipients.Add(extraRecipient);
                }
            }

            foreach (IEmailRecipient notificationRecipient in EmailRecipients)
            {
                if (notificationRecipient.Rule.HasFlag(NotificationRule.OnGeneralAlert))
                {
                    notificationRecipients.Add(notificationRecipient);
                }
            }

            if (notificationRecipients.Any())
            {
	            ICollection<string> details = TaskAlertAdditionalInfo(ref summary, detail);
                GeneralAlertNotification notifier = new GeneralAlertNotification(log, this.Options, summary, details);
                notifier.Send(notificationRecipients, EmailRedirects);
            }
        }

        /// <summary>
        /// Load labelling data
        /// </summary>
        public void LoadLabelData()
        {
            String labelFile = this.GetLabelDataFile();
            if (String.IsNullOrWhiteSpace(labelFile))
            {
                this.Log.MessageCtx(LOG_CTX, "No label file was specified.");
                return;
            }

            if (!File.Exists(labelFile))
            {
                this.Log.WarningCtx(LOG_CTX, "Label file does not exist ({0}).", labelFile);
                return;
            }

            byte[] md5 = FileMD5.ComputeMD5(labelFile);
            if (LabelFileMd5 == null || !FileMD5.Equal(LabelFileMd5, md5))
            {
                this.Log.MessageCtx(LOG_CTX, "Loading label file {0}", labelFile);
                LabelFileMd5 = md5;
                XDocument xdoc = XDocument.Load(labelFile);
                LoadLabels(xdoc);
            }
        }


        /// <summary>
        /// Load our email & bug notification data.
        /// </summary>
        public void LoadNotificationData()
        {
            
            String notificationsFile = this.GetNotificationDataFile();
            if (String.IsNullOrWhiteSpace(notificationsFile))
            {
                this.Log.MessageCtx(LOG_CTX, "No notification file was specified.");
                return;
            }

            if (!File.Exists(notificationsFile))
            {
                this.Log.WarningCtx(LOG_CTX, "Notification file does not exist ({0}).  Will not send out any notifications.", notificationsFile);
                return;
            }

            XDocument xdoc = XDocument.Load(notificationsFile);
            LoadEmailRecipients(xdoc);
            LoadBugRecipients(xdoc);
        }

        /// <summary>
        /// Two jobs are designated the same 'type of work', and from the perpective of the task are said to be 'associated'
        /// - this association is used to group bindable jobs and to associate jobs that form an incremental breakers list.        
        /// - association can be a loose association, does not mean there are identical.
        /// - in the case of codebuilder2 jobs it means they are building the same solution in the same way.
        /// </summary>
        /// <param name="job1"></param>
        /// <param name="job2"></param>
        /// <returns>true if they are associated</returns>
        public virtual bool Associated(IJob job1, IJob job2)
        {
            return false;
        }

        #endregion // controller methods

        #region Protected Methods
        /// <summary>
        /// Dequeue next job (based on priority).
        /// </summary>
        /// <returns></returns>
        protected IJob DequeueJob()
        {
            if (0 == this.m_Jobs.Count)
                return (null);

            lock (this.m_Jobs)
            {
                return (this.m_Jobs.DequeueValue());
            }
        }

        /// <summary>
        /// Load jobs from xmldoc, sort and enqueue
        /// </summary>
        /// <param name="xmlDoc"></param>
        protected void LoadJobs(XDocument xmlDoc)
        {
            if (Parameters.ContainsKey(PARAM_TASK_PRIORITY))
            {
                this.Priority = (TaskPriority)Enum.Parse(typeof(TaskPriority), Parameters[PARAM_TASK_PRIORITY].ToString(), true);
            }

            // --- JOBS ---
            IEnumerable<XElement> xmlJobs = xmlDoc.Root.XPathSelectElements("/Task/Jobs/Job");

            ICollection<IJob> jobs = new List<IJob>();

            foreach (XElement xmlJob in xmlJobs)
            {
                IJob job = JobFactory.Create(this.Config.CoreProject.DefaultBranch, xmlJob);

                if (null != job)
                {
                    // Prior to enqueuing the job back onto the automation server;
                    // we should update its state.  If it was previously Assigned we
                    // interrupted it... so push it back to Pending.
                    if (JobState.Assigned == job.State)
                        job.State = JobState.Pending;
                    jobs.Add(job);
                }
            }
            this.Log.MessageCtx(LOG_CTX, "\t{0} jobs loaded from state data.",
                jobs.Count);

            jobs = jobs.OrderBy(j => j.CreatedAt).ToList();
            this.Log.MessageCtx(LOG_CTX, "\tjobs sorted");

            foreach (IJob job in jobs)
            {
                this.EnqueueJob(job);
            }
            this.Log.MessageCtx(LOG_CTX, "\tjobs enqueued");
        }

        /// <summary>
        /// Load job results
        /// </summary>
        /// <param name="xmlDoc"></param>
        protected void LoadJobResults(XDocument xmlDoc)
        {
            // --- JOB RESULTS ---
            IEnumerable<XElement> xmlJobResults = xmlDoc.Root.XPathSelectElements("/Task/JobResults/JobResult");

            this.m_JobResults = new List<IJobResult>();

            foreach (XElement xmlJob in xmlJobResults)
            {
                IJobResult jobResult = JobResultFactory.Create(xmlJob);
                if (null != jobResult)
                    m_JobResults.Add(jobResult);
            }
            this.Log.MessageCtx(LOG_CTX, "\t{0} job results loaded from state data.",
                this.m_JobResults.Count());
        }

        /// <summary>
        /// Load state
        /// </summary>
        /// <param name="xmlDoc"></param>
        protected void LoadState(XDocument xmlDoc)
        {
            XElement xmlState = xmlDoc.Root.XPathSelectElements("/Task/State").FirstOrDefault();

            if (xmlState != null)
            {
                String val = xmlState.Value;
                TaskState taskState;

                if (TaskState.TryParse(val, true, out taskState))
                {
                    TaskState = taskState;
                    this.Log.MessageCtx(LOG_CTX, "\tTask state loaded from state data : {0}", TaskState);
                    return;
                }
                this.Log.ErrorCtx(LOG_CTX, "\tTask state not recognised in state data");
            }

            this.Log.WarningCtx(LOG_CTX, "\tTask state not found in state data");
        }


        #endregion // Protected Methods

        #region Private Methods

        /// <summary>
        /// Determines whether a notification is requried for a given notification recipient.
        /// </summary>
        /// <param name="job"></param>
        /// <param name="notificationRecipient"></param>
        /// <param name="fms"></param>
        /// <returns></returns>
        protected bool NotificationRequired(IJob job, IEmailRecipient notificationRecipient, FileMapping[] fms = null)
        {
            NotificationRule recipientRule = notificationRecipient.Rule;
            if (notificationRecipient.MonitoringLocations.Count() == 0)
            {
                if ((job.State == JobState.Errors && notificationRecipient.Rule.HasFlag(NotificationRule.OnErrors)) ||
                    (job.State == JobState.Completed && notificationRecipient.Rule.HasFlag(NotificationRule.OnCompleted)) ||
                    (job.State == JobState.Assigned && notificationRecipient.Rule.HasFlag(NotificationRule.OnAssigned))
                    )
                    return (true);
            }
            else
            {
                foreach (MonitoringLocation monitoringLocation in notificationRecipient.MonitoringLocations)
                {
                    String resolvedMonitoringLocation = this.Config.Project.DefaultBranch.Environment.Subst(monitoringLocation.Location);
                    resolvedMonitoringLocation = resolvedMonitoringLocation.Replace("/", "\\");

                    var monitoredFiles = fms.Where(fm => fm.LocalFilename.StartsWith(resolvedMonitoringLocation, StringComparison.CurrentCultureIgnoreCase));
                    if (monitoredFiles.Any())
                    {
                        if (job.State == JobState.Errors && monitoringLocation.Rule.HasFlag(NotificationRule.OnErrors) ||
                            job.State == JobState.Completed && monitoringLocation.Rule.HasFlag(NotificationRule.OnCompleted) ||
                            job.State == JobState.Assigned && notificationRecipient.Rule.HasFlag(NotificationRule.OnAssigned)
                            )
                            return (true);
                    }
                }
            }
            return (false);
        }


        /// <summary>
        /// Loads recipients ( for mail ) from xdoc
        /// </summary>
        /// <param name="xdoc"></param>
        private void LoadEmailRecipients(XDocument xdoc)
        {
            lock (this.EmailRecipients)
            {
                (this.EmailRecipients as List<IEmailRecipient>).Clear();
                IEnumerable<XElement> recipientElements = SettingsXml.GetElements(this.Options.Branch, this.Log, xdoc, "recipients", "emailrecipients/emailrecipient");
                IEnumerable<XElement> redirectElements = SettingsXml.GetElements(this.Options.Branch, this.Log, xdoc, "recipients", "emailredirects/emailredirect");

                // Make sure the list if clear as reloading
                m_EmailRedirects.Clear();

                // Get the redirect rules and put them into a dictionary for rule matching
                foreach (XElement redirect in redirectElements)
                {
                    String redirectpattern = redirect.Attribute("redirectpattern").Value;
                    String email = redirect.Attribute("email").Value;
                    String name = redirect.Attribute("name").Value;

                    if (!String.IsNullOrEmpty(redirectpattern) && !String.IsNullOrEmpty(email))
                    {
                        m_EmailRedirects.Add(redirectpattern, new EmailRedirect(email, name));
                    }
                }

                foreach (XElement recipientElem in recipientElements)
                {
                    String name = recipientElem.Attribute("name").Value;
                    String email = recipientElem.Attribute("email").Value;
                    String recipientRuleString = recipientElem.Attribute("rule").Value;
                    NotificationRule recipientRule = NotificationRecipient.NotifyStringToRule(recipientRuleString);

                    NotificationRecipient notificationrecipient = new NotificationRecipient(name, email, recipientRule);
                    foreach (XElement locationElement in recipientElem.Descendants("location"))
                    {
                        String path = locationElement.Attribute("path").Value;
                        String locationRuleString = String.Empty;
                        if (locationElement.Attribute("rule") != null)
                            locationRuleString = locationElement.Attribute("rule").Value;
                        else
                            locationRuleString = recipientRuleString;

                        NotificationRule locationRule = NotificationRecipient.NotifyStringToRule(locationRuleString);
                        MonitoringLocation monitoringLocation = new MonitoringLocation(path, locationRule);
                        (notificationrecipient.MonitoringLocations as List<MonitoringLocation>).Add(monitoringLocation);
                    }

                    (this.EmailRecipients as List<IEmailRecipient>).Add(notificationrecipient);
                }
            }
        }

        /// <summary>
        /// Loads recipients ( for bugs ) from xdoc
        /// </summary>
        /// <param name="xdoc"></param>
        private void LoadBugRecipients(XDocument xdoc)
        {
            lock (this.BugRecipients)
            {
                (this.BugRecipients as List<IBugRecipient>).Clear();

                IEnumerable<XElement> bugRecipientElements = SettingsXml.GetElements(this.Options.Branch, this.Log, xdoc, "recipients", "bugrecipients/bugrecipient");

                foreach (XElement bugRecipientElem in bugRecipientElements)
                {
                    String bugRecipientRuleString = bugRecipientElem.Attribute("rule").Value;
                    BugRule bugRule = BugRecipient.NotifyStringToRule(bugRecipientRuleString);
                    String bugOwner = bugRecipientElem.Attribute("owner") != null ? bugRecipientElem.Attribute("owner").Value : String.Empty;
                    String bugQA = bugRecipientElem.Attribute("qa") != null ? bugRecipientElem.Attribute("qa").Value : String.Empty;
                    String bugReviewer = bugRecipientElem.Attribute("reviewer") != null ? bugRecipientElem.Attribute("reviewer").Value : String.Empty;
                    String bugComponent = bugRecipientElem.Attribute("component") != null ? bugRecipientElem.Attribute("component").Value : String.Empty;
                    String bugTag = bugRecipientElem.Attribute("tag") != null ? bugRecipientElem.Attribute("tag").Value : String.Empty;
                    String bugClass = bugRecipientElem.Attribute("class") != null ? bugRecipientElem.Attribute("class").Value : String.Empty;
                    String bugPriority = bugRecipientElem.Attribute("priority") != null ? bugRecipientElem.Attribute("priority").Value : String.Empty;
                    ICollection<String> ccList = bugRecipientElem.Attribute("ccList") != null ? bugRecipientElem.Attribute("ccList").Value.Split(',').ToList() : new List<String>();

                    float estimatedTime = RSG.Interop.Bugstar.Bug.ESTIMATED_TIME_DEFAULT;
                    if (!float.TryParse(bugRecipientElem.Attribute("estimatedTime").Value, out estimatedTime))
                    {
                        Log.WarningCtx(LOG_CTX, "could not parse {0} as a float for estimatedTime", bugRecipientElem.Attribute("estimatedTime").Value);
                    }

                    BugRecipient bugRecipient = new BugRecipient(bugOwner, bugQA, bugReviewer, ccList, bugComponent, bugTag, bugClass, bugPriority, bugRule, estimatedTime);
                    (this.BugRecipients as List<IBugRecipient>).Add(bugRecipient);
                }
            }
        }

        /// <summary>
        /// Loads labels from xdoc
        /// </summary>
        /// <param name="xdoc"></param>
        private void LoadLabels(XDocument xdoc)
        {
            (this.TaskLabels as List<TaskLabel>).Clear();

            this.Log.MessageCtx(LOG_CTX, "Loading label elements for branch {0}", this.Options.Branch.Name);
            IEnumerable<XElement> labelElements = SettingsXml.GetElements(this.Options.Branch, this.Log, xdoc, "labels", "label");
            this.Log.MessageCtx(LOG_CTX, "Loaded {0} label elements", labelElements.Count());            

            using (P4 p4 = Options.CoreProject.SCMConnect())
            {
                foreach (XElement labelElem in labelElements)
                {
                    String labelName = labelElem.Attribute("labelName") != null ? labelElem.Attribute("labelName").Value : String.Empty;
                    this.Log.MessageCtx(LOG_CTX, "Loading label {0}", labelName);
                    String permittedHost = labelElem.Attribute("permittedHost") != null ? labelElem.Attribute("permittedHost").Value : String.Empty;
                    bool labelAllMonitoredPaths = labelElem.Attribute("labelAllMonitoredPaths") != null ? bool.Parse(labelElem.Attribute("labelAllMonitoredPaths").Value) : true;
                    String additionalLabelPaths = labelElem.Attribute("additionalLabelPaths") != null ? labelElem.Attribute("additionalLabelPaths").Value : String.Empty;
                    JobState jobStateToTriggerLabel = labelElem.Attribute("jobStateToTriggerLabel") != null ? (JobState)Enum.Parse(typeof(JobState), labelElem.Attribute("jobStateToTriggerLabel").Value) : JobState.Completed;
                    bool setView = labelElem.Attribute("setView") != null ? bool.Parse(labelElem.Attribute("setView").Value) : true;

                    IEnumerable<String> additionalLabelPathsAsEnumerable = additionalLabelPaths.Split(',').Select(lp => lp.Trim());

                    TaskLabel taskLabel = new TaskLabel(p4, this.Log, labelName, permittedHost, labelAllMonitoredPaths, additionalLabelPathsAsEnumerable, jobStateToTriggerLabel, setView);

                    (this.TaskLabels as List<TaskLabel>).Add(taskLabel);
                }
            }

            this.Log.MessageCtx(LOG_CTX, "Loaded {0} labels", this.TaskLabels.Count());
        }

        /// <summary>
        /// Set any labels as a result of a job finishing
        /// </summary>
        /// <param name="finishedJob">The job that has just finished</param>
        protected void SetLabels(IJob finishedJob)
        {
            if (finishedJob.Labels == null || !finishedJob.Labels.Any())
            {
                Log.MessageCtx(LOG_CTX, "Job does not contribute labels");
                return;
            }

            lock (this.m_Jobs)
            {
                // Process each label that the job contributes to.
                foreach (String labelName in finishedJob.Labels)
                {
                    TaskLabel taskLabel = TaskLabels.Where(tl => tl.Name.Equals(labelName, StringComparison.CurrentCultureIgnoreCase)).SingleOrDefault();
                    if (taskLabel == default(TaskLabel))
                    {
                        Log.WarningCtx(LOG_CTX, "Task is not listening to label {0} which job {1} contributes to. It listens to the following {2} labels", labelName, finishedJob.ID, TaskLabels.Count());
                        foreach (TaskLabel label in TaskLabels)
                        {
                            Log.WarningCtx(LOG_CTX, "\tlabel {0}.", label.Name);
                        }
                    }
                    else
                    {
                        taskLabel.SetLabel(Options, Log, finishedJob, this.Jobs, this.Role);
                    }
                }
            }
        }

        /// <summary>
        /// Return server IJob from the owning task.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private IJob GetJob(Guid id)
        {
            IJob job = null;
            job = this.Jobs.Where(j => j.ID.Equals(id)).FirstOrDefault();
           
            return (job);
        }

        private ICollection<string> TaskAlertAdditionalInfo(ref string summary, string detail)
        {
            if (summary == null)
            {
                summary = "";
            }

            summary = String.Format("{0} : {1}", summary, this.Role.ToString());

            ICollection<string> details = new List<string>();
            if (detail != null)
                details.Add(detail);
            details.Add(this.Role.ToString());
            return details;
        }

        private void PreJobEmailNotification(IEnumerable<IEmailRecipient> sendList, IJob job, Uri workerUri, Guid workerGuid)
        {
            // DW, there is something wrong about a pre job notification that calls a JobPostNotification?!
            IEmailNotification emailNotification = new JobPostNotification(job, new List<IJob>(), new List<JobResult>(), Log, workerUri, workerGuid, Options, null, true);
            emailNotification.Send(sendList, EmailRedirects);
        }

        private void AssignedJobEmailNotification(IEnumerable<IEmailRecipient> sendList, IJob job, Uri workerUri, Guid workerGuid)
        {
            // DW, there is something wrong about a pre job notification that calls a JobPostNotification?!
            IEmailNotification emailNotification = new JobPostNotification(job, new List<IJob>(), new List<JobResult>(), Log, workerUri, workerGuid, Options, null, true);
            emailNotification.Send(sendList, EmailRedirects);
        }

        protected IEnumerable<uint> PostJobBugNotification(IJob job, IEnumerable<IJobResult> jobResults, Uri workerUri, Guid workerGuid, String logFilename)
        {
            String bugstarLoginUsername = Parameters.ContainsKey(PARAM_BUGSTAR_LOGIN_USERNAME) ?
                (String)Parameters[PARAM_BUGSTAR_LOGIN_USERNAME] : String.Empty;
            String bugstarLoginPassword = Parameters.ContainsKey(PARAM_BUGSTAR_LOGIN_PASSWORD) ?
                (String)Parameters[PARAM_BUGSTAR_LOGIN_PASSWORD] : String.Empty;
            String bugstarLoginDomain = Parameters.ContainsKey(PARAM_BUGSTAR_LOGIN_DOMAIN) ?
                (String)Parameters[PARAM_BUGSTAR_LOGIN_DOMAIN] : String.Empty;

            if (String.IsNullOrEmpty(bugstarLoginUsername) || String.IsNullOrEmpty(bugstarLoginPassword))
            {
                return null;
            }

            try
            {
                IBugstarConfig bugstarConfig = ConfigFactory.CreateBugstarConfig(Options.Branch.Project);
                BugstarConnection bugstarConnection = new BugstarConnection(bugstarConfig.RESTService, bugstarConfig.AttachmentService);
                bugstarConnection.Login(bugstarLoginUsername, bugstarLoginPassword, bugstarLoginDomain);
                IBugNotification bugNotification = new BugNotification(Log, job, jobResults, workerUri, workerGuid, logFilename, Options, bugstarConnection, bugstarConfig);
                IEnumerable<uint> bugsIdsCreated = bugNotification.Create(BugRecipients);
                return bugsIdsCreated;
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Bug creation exception");
                return null;
            }
        }

        protected virtual void PostJobEmailNotification(IEnumerable<IEmailRecipient> sendList, IEnumerable<uint> bugIds, IJob job, IEnumerable<IJobResult> jobResults, Uri workerUri, Guid workerGuid, String logFilename)
        {
            // A list of contiguous jobs of the same state that are associated and have been previously processed.
            IEnumerable<IJob> associatedJobs = new List<IJob>();

            if (job.State == JobState.Errors)
            {
                associatedJobs = this.Jobs.OrderByDescending(j => j.ProcessedAt)
                         .Where(j => Associated(job, j))
                         .TakeWhile(j => j.State == job.State);
            }

            IEmailNotification emailNotification = new JobPostNotification(job, associatedJobs, jobResults, Log, workerUri, workerGuid, Options, bugIds, true);
            emailNotification.Send(sendList, EmailRedirects);
        }

        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.Common.Tasks namespace
