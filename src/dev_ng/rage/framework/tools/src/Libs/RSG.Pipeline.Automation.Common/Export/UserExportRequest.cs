﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Export
{

    /// <summary>
    /// 
    /// </summary>
    public class UserExportRequest
    {
        #region Properties
        /// <summary>
        /// Unique identifier.
        /// </summary>
        public Guid ID
        {
            get;
            private set;
        }

        /// <summary>
        /// Username owning request.
        /// </summary>
        public String Username
        {
            get;
            private set;
        }

        /// <summary>
        /// Export source filenames.
        /// </summary>
        public IEnumerable<String> DCCSourceFilenames
        {
            get;
            private set;
        }

        /// <summary>
        /// Timestamp of request.
        /// </summary>
        public DateTime RequestedAt
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="dccSourceFilename"></param>
        public UserExportRequest(String username, IEnumerable<String> dccSourceFilename)
        {
            this.ID = Guid.NewGuid();
            this.Username = username;
            this.DCCSourceFilenames = new List<String>(DCCSourceFilenames);
            this.RequestedAt = DateTime.UtcNow;
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Automation.Common.Export namespace
