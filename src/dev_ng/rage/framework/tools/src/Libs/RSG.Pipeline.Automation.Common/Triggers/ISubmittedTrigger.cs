﻿namespace RSG.Pipeline.Automation.Common.Triggers
{
    using System;

    /// <summary>
    /// When implemented represents a trigger that has a submitted at property.
    /// </summary>
    public interface ISubmittedTrigger
    {
        #region Properties
        /// <summary>
        /// Gets the submition time of this trigger.
        /// </summary>
        DateTime SubmittedAt { get; }
        #endregion
    } // RSG.Pipeline.Automation.Common.Triggers.ISubmittedTrigger {Interface}
} // RSG.Pipeline.Automation.Common.Triggers {Namespace}
