﻿using RSG.Pipeline.Automation.Common.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Notifications
{
    public sealed class NotificationRecipient :
          IEmailRecipient
    {
        #region Properties
        /// <summary>
        /// Friendly name for email recipient. 
        /// </summary>
        public String Name { get; private set; }

        /// <summary>
        /// Email address of recipient. 
        /// </summary>
        public String Email { get; private set; }

        /// <summary>
        /// Rule to determine whether to mail recipient.
        /// </summary>
        public NotificationRule Rule { get; private set; }

        /// <summary>
        /// List of monitoring location objects
        /// </summary>
        public IEnumerable<MonitoringLocation> MonitoringLocations { get; private set; }

        #endregion // Properties

        #region Constructor
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="email"></param>
        /// <param name="notify"></param>
        public NotificationRecipient(String name, String email, NotificationRule notify)
        {
            this.Name = name;
            this.Email = email;
            this.Rule = notify;
            this.MonitoringLocations = new List<MonitoringLocation>();
        }
        #endregion // Constructor

        #region Static Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="rule"></param>
        /// <returns></returns>
        public static NotificationRule NotifyStringToRule(String ruleString)
        {
            NotificationRule result = NotificationRule.Never;

            List<string> rules = ruleString.Split(',').Select(s => s.Trim()).ToList();

            foreach (string rule in rules)
            {
                switch (rule)
                {
                    case "completed":
                        result |= NotificationRule.OnCompleted;
                        break;
                    case "warnings":
                        result |= NotificationRule.OnWarnings;
                        break;
                    case "errors":
                        result |= NotificationRule.OnErrors;
                        break;
                    case "exceptions":
                        result |= NotificationRule.OnExceptions;
                        break;
                    case "shutdown":
                        result |= NotificationRule.OnShutdown;
                        break;
                    case "startup":
                        result |= NotificationRule.OnStartup;
                        break;
                    case "alert":
                        result |= NotificationRule.OnGeneralAlert;
                        break;
                    case "alerts":
                        result |= NotificationRule.OnExceptions;
                        result |= NotificationRule.OnShutdown;
                        result |= NotificationRule.OnStartup;
                        result |= NotificationRule.OnGeneralAlert;
                        break;
                    case "assigned":
                        result |= NotificationRule.OnAssigned;
                        break;
                    case "all":
                        result = NotificationRule.OnAll; // ALL means ALL
                        break;
                    case "none":
                    default:
                        result = NotificationRule.Never;
                        break;
                }
            }
            return result;
        }
        #endregion // Static Functions
    }
}
