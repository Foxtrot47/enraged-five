﻿using System;
using System.Runtime.Serialization;

namespace RSG.Pipeline.Automation.Common.Services.Messages
{

    /// <summary>
    /// Changelist operation response object.
    /// </summary>
    [DataContract(Namespace = "")]
    public class JobResponse
    {
        #region Properties
        /// <summary>
        /// Job identifier.
        /// </summary>
        [DataMember(Order = 1)]
        public Guid Job{ get; set; }

        /// <summary>
        /// Changelist operation.
        /// </summary>
        [DataMember(Order = 2)]
        public JobOp Operation { get; set; }

        /// <summary>
        /// Operation status (true successful, false otherwise).
        /// </summary>
        [DataMember(Order = 3)]
        public bool Status { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="op"></param>
        /// <param name="status"></param>
        public JobResponse(Guid jobId, JobOp op, bool status)
        {
            this.Job = jobId;
            this.Operation = op;
            this.Status = status;
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Automation.Common.Services.Messages namespace
