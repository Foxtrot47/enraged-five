﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace RSG.Pipeline.Automation.Common.Triggers
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract(Namespace = "")]
    //[Obsolete("This is soon to be retired, see url:bugstar:1917233")]
    public class TimedTrigger :
        Trigger,
        ITrigger
    {
        #region Properties
        /// <summary>
        /// Human readable identifier.
        /// </summary>
        public String Name { get; set; }

        /// <summary>
        /// Time task will run for the first time.  This can be in the past.
        /// </summary>
        public DateTime Start { get; set; }

        /// <summary>
        /// Time task will not run after this DateTime.
        /// </summary>
        public DateTime End { get; set; }

        /// <summary>
        /// Frequency with which timed task should execute.
        /// </summary>
        public TimeSpan Interval { get; set; }

        /// <summary>
        /// Files to be displayed in Workbench/major files of the build
        /// </summary>
        [DataMember]
        public String[] Files { get; set; }

        /// <summary>
        /// Changelist associated with this build
        /// </summary>
        [DataMember]
        public uint Changelist { get; set; }
        #endregion // properties

        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public TimedTrigger()
            : base()
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="files"></param>
        public TimedTrigger(String name, DateTime start, DateTime end, TimeSpan interval, IEnumerable<String> files = null, uint changelistNum = 0)
            : base()
        {
            this.Name = name;
            this.Start = start;
            this.End = end;
            this.Interval = interval;
            this.Changelist = changelistNum;
            if (files == null)
                this.Files = new String[0];
            else
                this.Files = files.ToArray();
        }
        #endregion // Constructors
    }

}
