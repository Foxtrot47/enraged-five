﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Pipeline.Core;
using RSG.SourceControl.Perforce;
using RSG.Base.Extensions;
using RSG.Pipeline.Automation.Common;
//using RSG.Platform;

namespace RSG.Pipeline.Automation.Common.Jobs
{
    public enum BuildCommand
    {
        Build,
        Rebuild,
        Clean
    }

    public enum BuildSystemType
    {
        Incredibuild,
        VisualStudio,
        VSI
    }

    /// <summary>
    /// Code builder client side job.
    /// </summary>
    [DataContract]
    public class CodeBuilderJob :
        Job,
        IJob
    {
        #region Properties
        /// <summary>
        /// Path to compiler configurations.
        /// Using envVar instead of branch specific, in order to avoid a configuration load
        /// </summary>
        private static readonly String COMPILER_CONFIGS_PATH = "%RS_TOOLSROOT%/etc/automation/tasks/CodebuilderCompilerConfigurations.xml";

        /// <summary>
        /// Build configurations that are read in from COMPILER_CONFIGS_PATH
        /// </summary>
        public static HashSet<String> BuildConfigurations { get; set; }

        /// <summary>
        /// Solution Path
        /// </summary>
        [DataMember]
        public String SolutionPath { get; set; }

        /// <summary>
        /// Name of the configuration.
        /// </summary>
        [DataMember]
        public String Configuration { get; set; }

        /// <summary>
        /// Name of the platform.
        /// </summary>
       // [DataMember]
       // public RSG.Platform.Platform Platform { get; set; }

        /// <summary>
        /// Version of build system to use (Visual Studio, VSI, Incredibuild)
        /// </summary>
        [DataMember]
        public BuildSystemType BuildSystem { get; set; }

        /// <summary>
        /// Changelist to sync to
        /// </summary>
        [DataMember]
        public uint Changelist { get; set; }

        /// <summary>
        /// Label to sync to
        /// </summary>
        [DataMember]
        public String LabelName { get; set; }

        /// <summary>
        /// Type of command to execute (e.g. build, rebuild, clean).
        /// </summary>
        [DataMember]
        public BuildCommand BuildCodeCommand { get; set; }

        /// <summary>
        /// Property used to execute any pre-build steps.  
        /// </summary>
        [DataMember]
        public String PreBuildCommand { get; set; }

        /// <summary>
        /// Property used to supply any arguments to the pre-build steps.
        /// </summary>
        [DataMember]
        public String PreBuildArguments { get; set; }

        /// <summary>
        /// Property used to execute any pre-build steps.  
        /// </summary>
        [DataMember]
        public String PostBuildCommand { get; set; }

        /// <summary>
        /// Property used to supply any arguments to the pre-build steps.
        /// </summary>
        [DataMember]
        public String PostBuildArguments { get; set; }

        /// <summary>
        /// A Perforce directory that states where these files are outputted to.
        /// </summary>
        [DataMember]
        public List<String> CheckoutDepotFilepaths { get; set; }

        /// <summary>
        /// Files to upload on successful build
        /// </summary>
        [DataMember]
        public List<String> UploadFiles { get; set; }

        /// <summary>
        /// Determines whether to submit files upon success.
        /// </summary>
        [DataMember]
        public bool SubmitFiles { get; set; }

        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Default constructor (for deserialisation).
        /// </summary>
        public CodeBuilderJob()
            : base()
        {
            SolutionPath = null;
            if (BuildConfigurations == null || BuildConfigurations.Count() == 0)
            {
                InitializeBuildConfigurations();
            }
            Configuration = BuildConfigurations.First();
            //Platform = RSG.Platform.Platform.Win64;
            BuildSystem = BuildSystemType.Incredibuild;
            BuildCodeCommand = BuildCommand.Build;
            PreBuildCommand = null;
            PreBuildArguments = null;
            PostBuildCommand = null;
            PostBuildArguments = null;
            LabelName = ""; 
            UploadFiles = new List<String>();
            SubmitFiles = false;
            Role = CapabilityType.CodeBuilder;
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CodeBuilderJob(XElement xmlJobElement, CapabilityType role, IBranch branch)
            : base(branch, role)
        {
            if (BuildConfigurations == null || BuildConfigurations.Count() == 0)
            {
                InitializeBuildConfigurations();
            }
            XElement solutionPathElement = xmlJobElement.Element("SolutionPath");
            if (solutionPathElement != null)
            {
                SolutionPath = Environment.ExpandEnvironmentVariables(solutionPathElement.Value);
            }
            else 
            {
                Log.Log__Error("Error creating CodeBuilderJob from XML. Missing Solution Path");
            }

            XElement configurationElement = xmlJobElement.Element("Configuration");
            if (configurationElement != null)
            {
                Configuration =  configurationElement.Value;
            }
            else 
            {
                Log.Log__Error("Error creating CodeBuilderJob from XML. Missing Build Configuration");
                Configuration = BuildConfigurations.First();
            }

            XElement platformElement = xmlJobElement.Element("Platform");
            if (platformElement != null)
            {
                //Platform = (RSG.Platform.Platform)Enum.Parse(typeof(RSG.Platform.Platform), platformElement.Value);
            }
            else 
            {
                Log.Log__Error("Error creating CodeBuilderJob from XML. Missing Build Platform");
                //Platform = (RSG.Platform.Platform)Enum.Parse(typeof(RSG.Platform.Platform), "Win64");

            }

            XElement buildCommandElement = xmlJobElement.Element("BuildComamnd");
            if (buildCommandElement != null)
            { 
                switch (buildCommandElement.Value)
                {
                    case "Build":
                        BuildCodeCommand = BuildCommand.Build;
                    break;
                    case "Rebuild":
                        BuildCodeCommand = BuildCommand.Rebuild;
                    break;
                    case "Clean":
                        BuildCodeCommand = BuildCommand.Clean;
                    break;
                    default:
                    Log.Log__Warning(String.Format("Bad BuildCommand given. Expecting: Build, Rebuild, or Clean. Got: {0}.", buildCommandElement.Value));
                        BuildCodeCommand = BuildCommand.Rebuild;
                    break;
                };
            }

            XElement buildSystemTypeElement = xmlJobElement.Element("BuildSystemType");
            if (buildSystemTypeElement != null)
            {
                BuildSystem = (BuildSystemType)Enum.Parse(typeof(BuildSystemType), buildSystemTypeElement.Value);
            }
            else
            {
                Log.Log__Warning("No BuildSystemType specified in CodeBuilderJob definition .xml. Setting to Incredibuild.");
                BuildSystem = (BuildSystemType)Enum.Parse(typeof(BuildSystemType), "Incredibuild");
            }

            XElement changelist = xmlJobElement.Element("ChangelistNumToSync");
            uint changelistNum = 0;
            if (changelist != null && uint.TryParse(changelist.Value, out changelistNum))
            {
                Changelist = changelistNum;
            }

            XElement labelEle = xmlJobElement.Element("LabelToSync");            
            if (labelEle != null && !String.IsNullOrWhiteSpace(labelEle.Value))
            {
                LabelName = labelEle.Value;
            }

            XElement preBuildElement = xmlJobElement.Element("PreBuildCommand");
            if (preBuildElement != null)
            {
                PreBuildCommand = Environment.ExpandEnvironmentVariables(preBuildElement.Value);
            }

            XElement preBuildCommandArgElement = xmlJobElement.Element("PreBuildArguments");
            if (preBuildCommandArgElement != null)
            {
                PreBuildArguments = Environment.ExpandEnvironmentVariables(preBuildCommandArgElement.Value);
            }

            XElement postBuildElement = xmlJobElement.Element("PostBuildCommand");
            if (postBuildElement != null)
            {
                PostBuildCommand = Environment.ExpandEnvironmentVariables(postBuildElement.Value);
            }

            XElement postBuildCommandArgElement = xmlJobElement.Element("PostBuildArguments");
            if (postBuildCommandArgElement != null)
            {
                PostBuildArguments = Environment.ExpandEnvironmentVariables(postBuildCommandArgElement.Value);
            }

            UploadFiles = new List<String>();
            IEnumerable<XElement> xmlFilesToUpload = xmlJobElement.Descendants("UploadFile");
            if (xmlFilesToUpload != null)
            {
                xmlFilesToUpload.ForEach(f => UploadFiles.Add(Environment.ExpandEnvironmentVariables(f.Value)));
            }

            IEnumerable<XElement> xmlCheckoutFileElements = xmlJobElement.XPathSelectElements("CheckoutFiles/CheckoutFile");
            CheckoutDepotFilepaths = new List<String>();
            if (xmlCheckoutFileElements != null)
            {
                xmlCheckoutFileElements.ForEach(cf => CheckoutDepotFilepaths.Add(Environment.ExpandEnvironmentVariables(cf.Value)));
            }

            SubmitFiles = false;
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CodeBuilderJob(String solutionPath, String configuration, /*RSG.Platform.Platform platform,*/ String buildSystemType, BuildCommand buildCommand, Changelist change, IBranch branch)
            : base(branch, CapabilityType.CodeBuilder, change)
        {
            if (System.IO.File.Exists(solutionPath))
            {
                SolutionPath = solutionPath;
            }
            else
            {
                Log.Log__Error("Solution path given to CodeBuilder doesn't exist locally!");
            }
            if (BuildConfigurations == null || BuildConfigurations.Count() == 0)
            {
                InitializeBuildConfigurations();
            }

            Configuration = configuration;
            //Platform = platform;
            BuildSystem = (BuildSystemType)Enum.Parse(typeof(BuildSystemType), buildSystemType);
            BuildCodeCommand = buildCommand;
            PreBuildCommand = null;
            PreBuildArguments = null;
            PostBuildCommand = null;
            PostBuildArguments = null;
            UploadFiles = new List<String>();
            CheckoutDepotFilepaths = new List<String>();
            Role = CapabilityType.CodeBuilder;
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CodeBuilderJob(String solutionPath, String configuration, /*RSG.Platform.Platform platform,*/ String buildSystemType, BuildCommand buildCommand, String label, IBranch branch)
            : base(branch, CapabilityType.CodeBuilder)
        {
            if (System.IO.File.Exists(solutionPath))
            {
                SolutionPath = solutionPath;
            }
            else
            {
                Log.Log__Error("Solution path given to CodeBuilder doesn't exist locally!");
            }
            if (BuildConfigurations == null || BuildConfigurations.Count() == 0)
            {
                InitializeBuildConfigurations();
            }
            Configuration = configuration;
            //Platform = platform;
            BuildSystem = (BuildSystemType)Enum.Parse(typeof(BuildSystemType), buildSystemType);
            BuildCodeCommand = buildCommand;
            LabelName = label;
            PreBuildCommand = null;
            PreBuildArguments = null;
            PostBuildCommand = null;
            PostBuildArguments = null;
            UploadFiles = new List<String>();
            CheckoutDepotFilepaths = new List<String>();
            Role = CapabilityType.CodeBuilder;
        }
        #endregion // Constructors
        
        #region Controller Methods

        /// <summary>
        /// Reading in build configuration settings
        /// The path is not passed in via parameters in order to keep this a static function. 
        /// </summary>
        public static HashSet<String> InitializeBuildConfigurations()
        {
            String compilerConfigurationsFile = Environment.ExpandEnvironmentVariables(COMPILER_CONFIGS_PATH);
            if (System.IO.File.Exists(compilerConfigurationsFile))
            {
                XDocument xmlDoc = XDocument.Load(compilerConfigurationsFile);
                IEnumerable<XElement> xmlConfigurations = xmlDoc.Descendants("configuration");
                if (BuildConfigurations == null)
                {
                    BuildConfigurations = new HashSet<String>();
                }
                xmlConfigurations.ForEach(xmlConfig =>
                    BuildConfigurations.Add(xmlConfig.LastAttribute.Value)
                );
            }
            else
            {
                Log.Log__Error("Could not find compiler confiugration file at: {0}. Check to have it locally.");
            }
            return BuildConfigurations;
        }


        /// <summary>
        /// Serialise job representation to XML.
        /// </summary>
        /// <returns></returns>
        public override XElement Serialise()
        {
            XElement xmlJob = base.Serialise();
            xmlJob.Add(new XElement("SolutionPath", this.SolutionPath));
            xmlJob.Add(new XElement("Configuration", this.Configuration));
            //xmlJob.Add(new XElement("Platform", this.Platform.ToString()));
            xmlJob.Add(new XElement("BuildCommand", this.BuildCodeCommand.ToString()));
            xmlJob.Add(new XElement("LabelToSync", this.LabelName));
            xmlJob.Add(new XElement("Changelist", this.Changelist));   
            if ( this.BuildSystem != null )
                xmlJob.Add(new XElement("BuildSystemType", this.BuildSystem.ToString()));
            if (this.PreBuildCommand != null)
                xmlJob.Add(new XElement("PreBuildCommand", this.PreBuildCommand));
            if (this.PreBuildArguments != null)
                xmlJob.Add(new XElement("PreBuildArguments", this.PreBuildArguments));
            if (this.PostBuildCommand != null)
                xmlJob.Add(new XElement("PostBuildCommand", this.PostBuildCommand));
            if (this.PostBuildArguments != null)
                xmlJob.Add(new XElement("PostBuildArguments", this.PostBuildArguments));
            if (this.UploadFiles != null)
            { 
                List<XElement> uploadFileListEles = new List<XElement>();
                foreach( String file in UploadFiles)
                {
                    uploadFileListEles.Add(new XElement("UploadFile", file));
                }
                xmlJob.Add(new XElement("UploadFiles", uploadFileListEles.ToArray()));
            }
            xmlJob.Add(new XElement("SubmitFiles", this.SubmitFiles));
            return (xmlJob);
        }

        /// <summary>
        /// Deserialise job representation from XML.
        /// </summary>
        /// <param name="xmlJobElem"></param>
        /// <returns></returns>
        public override bool Deserialise(XElement xmlJobElem)
        {
            bool result = true;
            try
            {
                result &= base.Deserialise(xmlJobElem);
                this.SolutionPath = Environment.ExpandEnvironmentVariables((String)xmlJobElem.XPathSelectElement("SolutionPath").Value);
                this.Configuration = (String)xmlJobElem.XPathSelectElement("Configuration").Value;
                //this.Platform = (RSG.Platform.Platform)Enum.Parse(typeof(RSG.Platform.Platform), (String)xmlJobElem.XPathSelectElement("Platform").Value);

                if (xmlJobElem.XPathSelectElement("BuildSystemType") != null)
                    this.BuildSystem = (BuildSystemType)Enum.Parse( typeof(BuildSystemType), (String)xmlJobElem.XPathSelectElement("BuildSystemType").Value);
                if (xmlJobElem.XPathSelectElement("LabelToSync") != null && !String.IsNullOrEmpty(xmlJobElem.XPathSelectElement("LabelToSync").Value))
                    this.LabelName =  xmlJobElem.XPathSelectElement("LabelToSync").Value;
                
                BuildCommand buildCommand = BuildCommand.Build;
                if ( xmlJobElem.XPathSelectElement("BuildCommand") != null )
                    Enum.TryParse(xmlJobElem.XPathSelectElement("BuildCommand").Value, out buildCommand);
                this.BuildCodeCommand = buildCommand;

                if ( xmlJobElem.XPathSelectElement("PreBuildCommand") != null )
                    this.PreBuildCommand = Environment.ExpandEnvironmentVariables((String)xmlJobElem.XPathSelectElement("PreBuildCommand").Value);

                if (xmlJobElem.XPathSelectElement("PreBuildArguments") != null)
                    this.PreBuildArguments = Environment.ExpandEnvironmentVariables((String)xmlJobElem.XPathSelectElement("PreBuildArguments").Value);

                if (xmlJobElem.XPathSelectElement("PostBuildCommand") != null)
                    this.PostBuildCommand = Environment.ExpandEnvironmentVariables((String)xmlJobElem.XPathSelectElement("PostBuildCommand").Value);

                if (xmlJobElem.XPathSelectElement("PostBuildArguments") != null)
                    this.PostBuildArguments = Environment.ExpandEnvironmentVariables((String)xmlJobElem.XPathSelectElement("PostBuildArguments").Value);

                if (xmlJobElem.XPathSelectElement("UploadFile") != null)
                {
                    foreach (XElement uploadFileEle in xmlJobElem.Descendants("UploadFile"))
                    {
                        this.UploadFiles.Add(uploadFileEle.Value);
                    }
                }
                
                bool submitFiles = false;
                if ( xmlJobElem.XPathSelectElement("SubmitFiles") != null )
                    bool.TryParse(xmlJobElem.XPathSelectElement("SubmitFiles").Value, out submitFiles);
                this.SubmitFiles = submitFiles;
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Error deserialising Code Builder Job.");
                result = false;
            }
            return (result);
        }
        #endregion // Controller Methods
    }
}
