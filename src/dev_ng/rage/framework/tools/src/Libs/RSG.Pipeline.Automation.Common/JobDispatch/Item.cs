using RSG.Pipeline.Automation.Common.Jobs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.JobManagement
{
    #region Enums
    /// <summary>
    /// 
    /// </summary>
    public enum ItemType
    {
        Default,
        Path,
        Config,
        Solution,
        ScriptProject,
        RegEx
    }
    #endregion


    /// <summary>
    /// Job management Item interface.
    /// </summary>
    public interface IItem
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        Priority Priority { get; set; }

        /// <summary>
        /// Prebuild commands
        /// </summary>
        IEnumerable<String> PrebuildCommands { get; set; }

        /// <summary>
        /// Postbuild commands
        /// </summary>
        IEnumerable<String> PostbuildCommands { get; set; }

        /// <summary>
        /// Labels
        /// </summary>
        IEnumerable<String> Labels { get; set; }
        #endregion // Properties
    }

    /// <summary>
    /// Base job management item class
    /// </summary>
    public class Item :
        IItem
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Priority Priority { get; set; }

        /// <summary>
        /// Prebuild commands
        /// </summary>
        public IEnumerable<String> PrebuildCommands { get; set; }

        /// <summary>
        /// Postbuild commands
        /// </summary>
        public IEnumerable<String> PostbuildCommands { get; set; }

        /// <summary>
        /// Labels
        /// </summary>
        public IEnumerable<String> Labels { get; set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public Item(Priority priority, IEnumerable<String> prebuildCommands = null, IEnumerable<String> postbuildCommands = null, IEnumerable<String> labels = null)
        {
            Priority = priority;
            PrebuildCommands = prebuildCommands;
            PostbuildCommands = postbuildCommands;
            Labels = labels;
        }
        #endregion // Constructors

        #region Static Functions
        /// <summary>
        /// Converts item type string to item type.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static ItemType ConvertItemStringToItemType(String item)
        {
            if (item.Equals("path", StringComparison.CurrentCultureIgnoreCase))
                return ItemType.Path;
            else if (item.Equals("solution", StringComparison.CurrentCultureIgnoreCase))
                return ItemType.Solution;
            else if (item.Equals("script_project", StringComparison.CurrentCultureIgnoreCase))
                return ItemType.ScriptProject;
            else if (item.Equals("regex", StringComparison.CurrentCultureIgnoreCase))
                return ItemType.RegEx;
            else
                return ItemType.Default;
        }

        #endregion // Static Functions
    }

    /// <summary>
    /// Job management item managed by path.
    /// </summary>
    public class PathItem :
        Item,
        IItem
    {
        #region Properties
        /// <summary>
        /// The local filesystem path
        /// </summary>
        public String LocalPath { get; private set; }

        /// <summary>
        /// The depot path
        /// </summary>
        public String DepotPath { get; private set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public PathItem(String localPath, String depotPath, Priority priority)
            : base(priority)
        {
            this.LocalPath = Path.GetFullPath(localPath);
            this.DepotPath = depotPath;
        }
        #endregion // Constructors
    }

    /// <summary>
    /// Define a regular expression to match dispatch groups .
    /// </summary>
    public class PathRegexItem :
        Item,
        IItem
    {
        #region Properties
        /// <summary>
        /// The local filesystem path
        /// </summary>
        public String RegexPath { get; private set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public PathRegexItem(String regexPath, Priority priority)
            : base(priority)
        {
            this.RegexPath = regexPath;
        }
        #endregion // Constructors
    }

    /// <summary>
    /// Job management item managed by solution.
    /// </summary>
    public class SolutionItem :
        Item,
        IItem
    {
        #region Properties
        /// <summary>
        /// the full path to the local solution filename 
        /// </summary>
        public String SolutionFilename { get; private set; }

        /// <summary>
        /// the full path to the target directory - where build artefacts are written
        /// </summary>
        public String TargetDir { get; private set; }

        /// <summary>
        /// the full path to the publish directory
        /// - files for upload are copied from the target dir to the publish dir
        /// - if the publish dir is mapped in p4 the files are submitted.
        /// - publish dir can be the same as the target dir.
        /// </summary>
        public String PublishDir { get; private set; }

        /// <summary>
        /// the platform name to build
        /// </summary>
        public String Platform { get; private set; }

        /// <summary>
        /// the build configuraton name to build
        /// </summary>
        public String BuildConfig { get; private set; }

        /// <summary>
        /// the tool used to build the solution
        /// </summary>
        public String Tool { get; private set; }

        /// <summary>
        /// flag indicating a rebuild is required
        /// </summary>
        public bool Rebuild { get; private set; }

        /// <summary>
        /// The number of pending jobs that would allow jobs to be skipconsumed
        /// </summary>
        public int SkipConsume { get; private set; }

        /// <summary>
        /// Indicates this solution item requires post job notification
        /// </summary>
        public bool RequiresPostJobNotification { get; private set; }

        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Parameterized Constructor
        /// </summary>
        /// <param name="solutionFilename"></param>
        /// <param name="targetDir"></param>
        /// <param name="publishDir"></param>
        /// <param name="priority"></param>
        /// <param name="platform"></param>
        /// <param name="buildConfig"></param>
        /// <param name="tool"></param>
        /// <param name="rebuild"></param>
        /// <param name="prebuildCommands"></param>
        /// <param name="postbuildCommands"></param>
        /// <param name="skipConsume"></param>
        /// <param name="requiresPostJobNotification"></param>
        public SolutionItem(String solutionFilename, String targetDir, String publishDir, Priority priority, String platform, String buildConfig, String tool, bool rebuild, IEnumerable<String> prebuildCommands, IEnumerable<String> postbuildCommands, IEnumerable<String> labels, int skipConsume, bool requiresPostJobNotification)
            : base(priority, prebuildCommands, postbuildCommands, labels)
        {
            this.SolutionFilename = solutionFilename;
            this.TargetDir = targetDir;
            this.PublishDir = publishDir;
            this.Platform = platform;
            this.BuildConfig = buildConfig;
            this.Tool = tool;
            this.Rebuild = rebuild;
            this.SkipConsume = skipConsume;
            this.RequiresPostJobNotification = requiresPostJobNotification;
        }
        #endregion // Constructors
    }

    /// <summary>
    /// Job management item managed by script project.
    /// </summary>
    public class ScriptProjectItem :
        Item,
        IItem
    {
        #region Properties
        /// <summary>
        /// the full path to the local script project filename 
        /// </summary>
        public String ScriptProjectFilename { get; private set; }

        /// <summary>
        /// the platform name to build
        /// </summary>
        public String Platform { get; private set; }

        /// <summary>
        /// the build configuraton name to build
        /// </summary>
        public String BuildConfig { get; private set; }

        /// <summary>
        /// the tool used to build the solution
        /// </summary>
        public String Tool { get; private set; }

        /// <summary>
        /// Alias that is used for friendly lookups
        /// </summary>
        public String Alias { get; private set; }

        /// <summary>
        /// flag indicating a rebuild is required
        /// </summary>
        public bool Rebuild { get; private set; }

        /// <summary>
        /// The number of pending jobs that would allow jobs to be skipconsumed
        /// </summary>
        public int SkipConsume { get; private set; }

        /// <summary>
        /// Indicates this solution item requires post job notification
        /// </summary>
        public bool RequiresPostJobNotification { get; private set; }

        /// <summary>
        /// Indicates this script project item should generate a job when monitoring paths
        /// </summary>
        public bool MonitoringPaths { get; private set; }

        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Parameterized Constructor
        /// </summary>
        public ScriptProjectItem(String scriptProjectFilename, Priority priority, String buildConfig, String tool, bool rebuild, IEnumerable<String> prebuildCommands, IEnumerable<String> postbuildCommands, IEnumerable<String> labels, int skipConsume, bool requiresPostJobNotification, bool monitoringPaths, String alias)
            : base(priority, prebuildCommands, postbuildCommands, labels)
        {
            this.ScriptProjectFilename = scriptProjectFilename;
            this.BuildConfig = buildConfig;
            this.Tool = tool;
            this.Rebuild = rebuild;
            this.SkipConsume = skipConsume;
            this.RequiresPostJobNotification = requiresPostJobNotification;
            this.MonitoringPaths = monitoringPaths;
            this.Alias = alias;
        }
        #endregion // Constructors
    }

    /// <summary>
    /// Job management item managed by Build Configuration.
    /// </summary>
    public class BuildConfigItem :
        Item,
        IItem
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String BuildConfig { get; set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public BuildConfigItem(String buildConfig, Priority priority)
            : base(priority)
        {
            this.BuildConfig = buildConfig;
        }
        #endregion // Constructors

    }

    /// <summary>
    /// Job management item managed by Build Platform.
    /// </summary>
    public class BuildPlatformItem :
        Item,
        IItem
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public String BuildPlatform { get; set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        public BuildPlatformItem(String buildPlatform, Priority priority)
            : base(priority)
        {
            this.BuildPlatform = buildPlatform;
        }
        #endregion // Constructors

    }    
}
