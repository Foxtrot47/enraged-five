﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;
using RSG.Pipeline.Automation.Common.Triggers;

namespace RSG.Pipeline.Automation.Common.Jobs
{
    /// <summary>
    /// Job object; created on server, passed to and processed on clients.
    /// </summary>
    [DataContract]
    [KnownType(typeof(AssetBuilderJobResult))]
    [KnownType(typeof(SubmittedChangelistJobResult))]
    public class JobResult : IJobResult
    {
        #region Properties
        /// <summary>
        /// The job unique identifier this result relates to.
        /// </summary>
        [DataMember]
        public Guid JobId { get; set; }

        /// <summary>
        /// Flag for the overall notion of success
        /// </summary>
        [DataMember]
        public bool Succeeded { get; set; }

        /// <summary>
        /// The build tokens that were processed
        /// </summary>
        [DataMember]
        public ICollection<KeyValuePair<BuildToken, String>> ProcessedBuildTokens { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor (for deserialisation).
        /// </summary>
        public JobResult()
        {
        }

        /// <summary>
        /// Default constructor (for deserialisation).
        /// </summary>
        public JobResult(Guid jobId, bool succeeded = false)
        {
            JobId = jobId;
            Succeeded = succeeded;
            ProcessedBuildTokens = new List<KeyValuePair<BuildToken, String>>();
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Registers the build token as being processed
        /// </summary>
        /// <param name="tokenName"></param>
        /// <param name="tokenValue"></param>
        public void RegisterBuildTokenProcessed(BuildToken buildToken, String tokenValue)
        {
            this.ProcessedBuildTokens.Add(new KeyValuePair<BuildToken, String>(buildToken, tokenValue));
        }
        #endregion // Public Methods

        #region Controller Methods
        /// <summary>
        /// Serialise job result to XML.
        /// </summary>
        /// <returns></returns>
        public virtual XElement Serialise()
        {
            XElement xmlJobResult = new XElement("JobResult",
                new XAttribute("type", this.GetType().Name),
                new XElement("JobId", this.JobId),
                new XElement("Succeeded", this.Succeeded)
            );

            if (ProcessedBuildTokens.Count > 0)
            {
                XElement buildTokensElement = new XElement("ProcessedBuildTokens");

                foreach (KeyValuePair<BuildToken, String> processedBuildToken in ProcessedBuildTokens)
                {
                    String buildTokenName = processedBuildToken.Key.ToString();
                    buildTokensElement.Add(new XElement("BuildToken", new XAttribute("name", buildTokenName), processedBuildToken.Value));
                }
                xmlJobResult.Add(buildTokensElement);
            }
            return (xmlJobResult);
        }

        /// <summary>
        /// Deserialise job representation from XML.
        /// </summary>
        /// <param name="xmlJobElem"></param>
        /// <returns></returns>
        public virtual bool Deserialise(XElement xmlJobResultElem)
        {
            bool result = true;
            try
            {
                this.JobId = Guid.Parse(xmlJobResultElem.XPathSelectElement("JobId").Value);
                this.Succeeded = Boolean.Parse(xmlJobResultElem.XPathSelectElement("Succeeded").Value);
                IEnumerable<XElement> processBuildTokenElements = xmlJobResultElem.XPathSelectElements("ProcessedBuildTokens/BuildToken");
                this.ProcessedBuildTokens = new List<KeyValuePair<BuildToken, String>>();
                foreach (XElement processBuildTokenElement in processBuildTokenElements)
                {
                    String buildTokenName = processBuildTokenElement.Attribute("name").Value;
                    String value = processBuildTokenElement.Value;
                    BuildToken buildToken;
                    if (Enum.TryParse<BuildToken>(buildTokenName, out buildToken))
                    {
                        this.ProcessedBuildTokens.Add(new KeyValuePair<BuildToken, String>(buildToken, value));
                    }
                    else
                    {
                        Log.Log__Warning("unrecognised build token : {0}", buildTokenName);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Error deserialising job.");
                result = false;
            }
            return (result);
        }

        /// <summary>
        /// Override ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string success = this.Succeeded ? "OK!" : "ERR"; 
            //string weeGuid = this.JobId.ToString().Split('-')[0];
            //string str = String.Format("{0}.. {1}", weeGuid, success);
            string str = String.Format("{0}", success);
            return str;
        }
        #endregion // Controller Methods
    }
}
