﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.SourceControl.Perforce;

namespace RSG.Pipeline.Automation.Common.Tasks
{

    /// <summary>
    /// 
    /// </summary>
    [DataContract(Namespace = "")]
    public class IntegratorTaskStatus : TaskStatus
    {
        #region Properties
        /// <summary>
        /// Integrations
        /// </summary>
        [DataMember]
        public IEnumerable<Integration> Integrations { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="task"></param>
        public IntegratorTaskStatus(IIntegratorTask task)
            : base(task)
        {
            this.Integrations = new List<Integration>(task.Integrations);
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Automation.Common.Tasks namespace
