﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using RSG.Pipeline.Automation.Common.Jobs;

namespace RSG.Pipeline.Automation.Common.Tasks
{

    /// <summary>
    /// 
    /// </summary>
    [DataContract(Namespace = "")]
    [KnownType(typeof(IntegratorTaskStatus))]
    [KnownType(typeof(MonitoringTaskStatus))]
    [KnownType(typeof(TimedTaskStatus))]
    [KnownType(typeof(Job))]
    [KnownType(typeof(JobResult))]
    [KnownType(typeof(AssetBuilderJobResult))]
    [KnownType(typeof(IntegratorJobResult))]
    public class TaskStatus
    {
        #region Properties
        /// <summary>
        /// Task friendly name.
        /// </summary>
        [DataMember]
        public String Name { get; set; }

        /// <summary>
        /// Task role.
        /// </summary>
        [DataMember]
        public CapabilityType Role { get; set; }

        /// <summary>
        /// Task job status.
        /// </summary>
        [DataMember]
        public IEnumerable<IJob> Jobs { get; set; }

        /// <summary>
        /// Task job results
        /// </summary>
        [DataMember]
        public IEnumerable<IJobResult> JobResults { get; set; }

        /// <summary>
        /// Number of jobs.
        /// </summary>
        [DataMember]
        public int AvailableJobCount { get; set; }

        /// <summary>
        /// Task State.
        /// </summary>
        [DataMember]
        public TaskState TaskState { get; set; }

        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="task"></param>
        public TaskStatus(ITask task)
        {
            this.Name = task.Name;
            this.Role = task.Role;
            this.Jobs = task.Jobs;
            this.JobResults = task.JobResults;
            this.AvailableJobCount = task.Jobs.Where(j => (JobState.Assigned == j.State) || (JobState.Pending == j.State)).Count();
            this.TaskState = task.TaskState;
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Automation.Common.Tasks
