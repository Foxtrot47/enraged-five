﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Notifications
{
    /// <summary>
    /// 
    /// </summary>
    public interface IEmailRecipient
    {
        #region Properties
        /// <summary>
        /// Friendly name for email recipient. 
        /// </summary>
        String Name { get; }

        /// <summary>
        /// Email address of recipient. 
        /// </summary>
        String Email { get; }

        /// <summary>
        /// Bool to determine whether to mail recipient.
        /// </summary>
        NotificationRule Rule { get; }

        /// <summary>
        /// List of monitoring location objects
        /// </summary>
        IEnumerable<MonitoringLocation> MonitoringLocations { get; }
        #endregion // Properties
    }
}
