﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;

namespace RSG.Pipeline.Automation.Common.Triggers
{
    /// <summary>
    /// Trigger that was triggered by files.
    /// </summary>
    [DataContract(Namespace = "")]
    [KnownType(typeof(ChangelistTrigger))]
    [KnownType(typeof(UserRequestTrigger))]
    public class FilesTrigger : 
        Trigger,
        IFilesTrigger
    {
        #region Properties
        /// <summary>
        /// Set of files causing the trigger.
        /// </summary>
        [DataMember]
        public String[] Files { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public FilesTrigger()
            : base()
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="files"></param>
        public FilesTrigger(IEnumerable<String> files, Guid triggerId = default(Guid), DateTime triggeredAt = default(DateTime))
            : base(triggeredAt, triggerId)
        {
            this.Files = files.Distinct().ToArray();
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public FilesTrigger(Guid triggerId = default(Guid), DateTime triggeredAt = default(DateTime))
            : base(triggeredAt, triggerId)
        {
        }

        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise trigger representation to XML.
        /// </summary>
        /// <returns></returns>
        public override XElement Serialise()
        {
            XElement xmlTrigger = base.Serialise();
            xmlTrigger.Add(
                new XElement("Files", this.Files.Select(f => new XElement("File", f)))
            );

            return (xmlTrigger);
        }

        /// <summary>
        /// Deserialise trigger representation from XML.
        /// </summary>
        /// <param name="xmlTriggerElem"></param>
        /// <returns></returns>
        public override bool Deserialise(XElement xmlTriggerElem)
        {
            bool result = true;
            try
            {
                base.Deserialise(xmlTriggerElem);
                IEnumerable<String> files = xmlTriggerElem.XPathSelectElements("Files/File").
                    Select(xmlFileElem => xmlFileElem.Value);
                this.Files = (new List<String>(files)).Distinct().ToArray();
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Error deserialising files trigger.");
                result = false;
            }
            return (result);
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Automation.Common.Triggers namespace
