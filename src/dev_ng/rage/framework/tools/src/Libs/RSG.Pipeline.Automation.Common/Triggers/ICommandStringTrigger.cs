﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Triggers
{
    /// <summary>
    /// A trigger that holds data for various jobs
    /// EX: can hold pairings of Environment settings
    /// </summary>
    public interface ICommandStringTrigger
    {
        #region
        /// <summary>
        /// A generic string that can be used to serialize various data for any trigger
        /// </summary>
        String FormattedCommand { get; }
        #endregion // Properties
    }
}
