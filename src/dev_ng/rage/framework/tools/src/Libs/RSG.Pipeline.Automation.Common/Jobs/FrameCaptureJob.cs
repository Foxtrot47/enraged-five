﻿using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.SourceControl.Perforce;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Pipeline.Automation.Common.Jobs
{
    [DataContract]
    public class FrameCaptureJob :
        Job,
        IJob
    {
        #region Constants
        private static readonly String XML_ELEMENT_NAME_TARGET_DIR = "TargetDir";
        private static readonly String XML_ELEMENT_NAME_PUBLISH_DIR = "PublishDir";
        private static readonly String XML_ELEMENT_NAME_ANIMATION_NAME = "AnimationName";
        private static readonly String XML_ELEMENT_NAME_POSITION = "Position";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Directory in which files are written as a result of running the export. 
        /// - also known as Artefacts
        /// </summary>
        [DataMember]
        public String TargetDir { get; private set; }

        /// <summary>
        /// Directory in which files that were marked for upload are published ( copied and optionally checked in )
        /// - files for upload are copied from the upload dir to the publish dir
        /// - if the publish dir is mapped in p4 the files are submitted.
        /// - publish dir can be the same as the upload dir.
        /// </summary>
        [DataMember]
        public String PublishDir { get; private set; }

        [DataMember]
        public String Animation { get; private set; }

        /// <summary>
        /// Spawn position for the player
        /// </summary>
        [DataMember]
        public String Position { get; private set; }
        #endregion // Properties

        #region Constructors

                /// <summary>
        /// Default constructor (for deserialisation).
        /// </summary>
        public FrameCaptureJob()
            : base()
        {
            Role = CapabilityType.FrameCapture;
        }

        /// <summary>
        /// Default constructor (for deserialisation).
        /// </summary>
        public FrameCaptureJob(IBranch branch)
            : base(branch, CapabilityType.FrameCapture)
        {
            TargetDir = null;
            PublishDir = null;
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public FrameCaptureJob(IBranch branch, XElement xmlJobElement)
            : base(branch, CapabilityType.FrameCapture)
        {
            base.Deserialise(xmlJobElement);
            this.TargetDir = Environment.ExpandEnvironmentVariables(xmlJobElement.XPathSelectElement(XML_ELEMENT_NAME_TARGET_DIR).Value);
            this.PublishDir = Environment.ExpandEnvironmentVariables(xmlJobElement.XPathSelectElement(XML_ELEMENT_NAME_PUBLISH_DIR).Value);
            this.Animation = xmlJobElement.XPathSelectElement(XML_ELEMENT_NAME_ANIMATION_NAME).Value;

            XElement positionElem = xmlJobElement.XPathSelectElement(XML_ELEMENT_NAME_POSITION);
            if (positionElem != null)
            {
                this.Position = positionElem.Value;
            }
        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        public FrameCaptureJob(IBranch branch, Changelist change, String animationName, IEnumerable<String> pathnamesToProcess, String position, IEnumerable<String> monitoringPaths = null)
            : base(branch, CapabilityType.FrameCapture, change, pathnamesToProcess, monitoringPaths)
        {
            /*TargetDir = Environment.ExpandEnvironmentVariables(solutionItem.TargetDir);
            PublishDir = Environment.ExpandEnvironmentVariables(solutionItem.PublishDir);*/
            Animation = animationName;
            Position = position;
        }

        /// <summary>
        /// Serialise job representation to XML.
        /// </summary>
        /// <returns></returns>
        public override XElement Serialise()
        {
            XElement xmlJob = base.Serialise();
            xmlJob.Add(new XElement(XML_ELEMENT_NAME_PUBLISH_DIR, this.PublishDir));
            xmlJob.Add(new XElement(XML_ELEMENT_NAME_TARGET_DIR, this.TargetDir));
            xmlJob.Add(new XElement(XML_ELEMENT_NAME_ANIMATION_NAME, this.Animation));
            xmlJob.Add(new XElement(XML_ELEMENT_NAME_POSITION, this.Position));
            return (xmlJob);
        }

        /// <summary>
        /// Deserialise job representation from XML.
        /// </summary>
        /// <param name="xmlJobElem"></param>
        /// <returns></returns>
        public override bool Deserialise(XElement xmlJobElem)
        {
            bool result = true;
            try
            {
                result &= base.Deserialise(xmlJobElem);
                this.TargetDir = Environment.ExpandEnvironmentVariables(xmlJobElem.XPathSelectElement(XML_ELEMENT_NAME_TARGET_DIR).Value);
                this.PublishDir = Environment.ExpandEnvironmentVariables(xmlJobElem.XPathSelectElement(XML_ELEMENT_NAME_PUBLISH_DIR).Value);
                this.Animation = xmlJobElem.XPathSelectElement(XML_ELEMENT_NAME_ANIMATION_NAME).Value;
                XElement positionElem = xmlJobElem.XPathSelectElement(XML_ELEMENT_NAME_POSITION);
                if(positionElem != null)
                {
                    this.Position = positionElem.Value;
                }
                
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Error deserialising FrameCaptureJob.");
                result = false;
            }
            return (result);
        }

        #endregion // Constructors
    }
}
