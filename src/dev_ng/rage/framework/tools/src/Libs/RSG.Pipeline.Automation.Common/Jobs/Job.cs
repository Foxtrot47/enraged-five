﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;
using RSG.Base.Configuration;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.SourceControl.Perforce;
using RSG.Base.Extensions;
using RSG.Pipeline.Automation.Common.Utils;

namespace RSG.Pipeline.Automation.Common.Jobs
{

    /// <summary>
    /// Job object; created on server, passed to and processed on clients.
    /// </summary>
    [DataContract]
    [KnownType(typeof(MapExportJob))]
    [KnownType(typeof(AssetBuilderJob))]
    [KnownType(typeof(CodeBuilderJob))]
    [KnownType(typeof(MultiTrigger))]
    [KnownType(typeof(ChangelistTrigger))]
    [KnownType(typeof(FilesTrigger))]
    [KnownType(typeof(UserRequestTrigger))]
    [KnownType(typeof(IntegratorJob))]
    [KnownType(typeof(CodeBuilder2Job))]
    [KnownType(typeof(ScriptBuilderJob))]
	[KnownType(typeof(BaseSmokeTestJob))]
    [KnownType(typeof(SmokeTestMetricsJob))]
    [KnownType(typeof(CommandRunnerJob))]
    [KnownType(typeof(CutSceneRPFBuilderJob))]
    [KnownType(typeof(FrameCaptureJob))]
    public class Job : IJob
    {
        #region Properties
        /// <summary>
        /// Job unique identifier.
        /// </summary>
        [DataMember]
        public Guid ID { get; set; }

        /// <summary>
        /// Job consuming this job.
        /// </summary>
        [DataMember]
        public Guid ConsumedByJobID { get; set; }

        /// <summary>
        /// Job state.
        /// </summary>
        [DataMember]
        public JobState State { get; set; }

        /// <summary>
        /// Job priority.
        /// </summary>
        [DataMember]
        public JobPriority Priority { get; set; }

        /// <summary>
        /// Job role; allows us to reuse Job classes for different processors.
        /// </summary>
        [DataMember]
        public CapabilityType Role { get; set; }

        /// <summary>
        /// Trigger that invoked this job.
        /// </summary>
        [DataMember]
        public ITrigger Trigger { get; set; }

        /// <summary>
        /// User defined data shipped with the job.
        /// </summary>
        [DataMember]
        public Object UserData { get; set; }
        
        /// <summary>
        /// Job creation time.
        /// </summary>
        [DataMember]
        public DateTime CreatedAt
        {
            get { return m_dtCreatedAt; }
            set { m_dtCreatedAt = value; }
        }
        private DateTime m_dtCreatedAt;

        /// <summary>
        /// Job processing start time.
        /// </summary>
        [DataMember]
        public DateTime ProcessedAt 
        {
            get { return m_dtProcessedAt; }
            set { m_dtProcessedAt = value; }
        }
        private DateTime m_dtProcessedAt;

        /// <summary>
        /// The hostname that processed or is processing the job.
        /// </summary>
        [DataMember]
        public String ProcessingHost
        {
            get { return m_processingHost; }
            set
            {
                m_processingHost = value;
                if (m_processingHost != null)
                {
                    m_processingHost = m_processingHost.ToLower();
                }
            }
        }
        private String m_processingHost;

        /// <summary>
        /// The Branch that this job is for.
        /// </summary>
        [DataMember]
        public String BranchName
        {
            get { return m_branchName; }
            set { m_branchName = value; }
        }
        private String m_branchName;

        /// <summary>
        /// The Project that this job is for.
        /// </summary>
        [DataMember]
        public String ProjectName
        {
            get { return m_projectName; }
            set { m_projectName = value; }
        }
        private String m_projectName;

        /// <summary>
        /// Time when the job completed.
        /// </summary>
        [DataMember]
        public DateTime CompletedAt
        {
            get { return m_dtCompletedAt; }
            set { m_dtCompletedAt = value; }
        }
        private DateTime m_dtCompletedAt;

        /// <summary>
        /// Amount of time client took to process job.
        /// </summary>
        [DataMember]
        public TimeSpan ProcessingTime
        {
            get { return m_tsProcessingTime; }
            set { m_tsProcessingTime = value; }
        }
        private TimeSpan m_tsProcessingTime;

        /// <summary>
        /// Prebuild commands
        /// </summary>
        [DataMember]
        public IEnumerable<String> PrebuildCommands
        {
            get { return m_prebuildCommands; }
            set { m_prebuildCommands = value; }
        }
        private IEnumerable<String> m_prebuildCommands;

        /// <summary>
        /// Postbuild commands
        /// </summary>
        [DataMember]
        public IEnumerable<String> PostbuildCommands
        {
            get { return m_postbuildCommands; }
            set { m_postbuildCommands = value; }
        }
        private IEnumerable<String> m_postbuildCommands;

       

        /// <summary>
        /// The labels that the job can PARTICIPATE in setting of.
        /// </summary>
        [DataMember]
        public IEnumerable<String> Labels
        {
            get { return m_labels; }
            set { m_labels = value; }
        }
        private IEnumerable<String> m_labels;
        
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor (for deserialisation).
        /// </summary>
        public Job()
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public Job(IBranch branch, CapabilityType role)
        {
            this.ID = Guid.NewGuid();
            this.ConsumedByJobID = Guid.Empty;
            this.Role = role;
            this.State = JobState.Pending;
            this.Priority = JobPriority.Normal;
            this.CreatedAt = DateTime.UtcNow;
            this.ProcessedAt = DateTime.MaxValue.ToUniversalTime();
            this.ProcessingTime = TimeSpan.Zero;
            this.ProcessingHost = String.Empty;
            this.CompletedAt = DateTime.MaxValue.ToUniversalTime();
            this.PrebuildCommands = new List<String>();
            this.PostbuildCommands = new List<String>();
            this.Labels = new List<String>();
            this.BranchName = branch != null ? branch.Name : String.Empty;
            this.ProjectName = branch != null ? branch.Project.Name : String.Empty;
        }

        /// <summary>
        /// Constructor; with file list trigger.
        /// </summary>
        /// <param name="files"></param>
        public Job(IBranch branch, CapabilityType role, IEnumerable<String> files, Guid triggerId = default(Guid), DateTime triggeredAt = default(DateTime))
            : this(branch, role)
        {
            this.Trigger = new FilesTrigger(files, triggerId, triggeredAt);
        }

        /// <summary>
        /// Constructor; with changelist trigger.
        /// </summary>
        /// <param name="change"></param>
        public Job(IBranch branch, CapabilityType role, Changelist change, Guid triggerId = default(Guid))
            : this(branch, role)
        {
            this.Trigger = new ChangelistTrigger(change, triggerId);
        }

        /// <summary>
        /// Constructor; with multi trigger.
        /// </summary>
        /// <param name="change"></param>
        public Job(IBranch branch, CapabilityType role, IEnumerable<ITrigger> triggers, IEnumerable<String> prebuildCommands = null, IEnumerable<String> postbuildCommands = null, IEnumerable<String> labels = null, Guid triggerId = default(Guid), DateTime triggeredAt = default(DateTime))
            : this(branch, role)
        {
            this.Trigger = new MultiTrigger(triggers, triggerId, triggeredAt);
            this.PrebuildCommands = prebuildCommands;
            this.PostbuildCommands = postbuildCommands;
            this.Labels = labels;
        }

        /// <summary>
        /// Constructor; with any ITrigger.
        /// </summary>
        /// <param name="change"></param>
        public Job(IBranch branch, CapabilityType role, ITrigger trigger, IEnumerable<String> prebuildCommands = null, IEnumerable<String> postbuildCommands = null, IEnumerable<String> labels = null)
            : this(branch, role)
        {
            this.Trigger = trigger;
            this.PrebuildCommands = prebuildCommands;
            this.PostbuildCommands = postbuildCommands;
            this.Labels = labels;
        }

        /// <summary>
        /// Constructor; with changelist trigger (and filtered files list).
        /// </summary>
        /// <param name="change"></param>
        public Job(IBranch branch, CapabilityType role, Changelist change, IEnumerable<String> files, Guid triggerId = default(Guid))
            : this(branch, role)
        {
            if (triggerId == default(Guid))
            {
                triggerId = Guid.NewGuid();
            }

            this.Trigger = new ChangelistTrigger(change, files, triggerId);
        }

        /// <summary>
        /// Constructor; with changelist trigger (and filtered files list).
        /// </summary>
        /// <param name="change"></param>
        public Job(IBranch branch, CapabilityType role, Changelist change, IEnumerable<String> files, IEnumerable<String> monitoredDirs = null, IEnumerable<String> prebuildCommands = null, IEnumerable<String> postbuildCommands = null, IEnumerable<String> labels = null, Guid triggerId = default(Guid))
            : this(branch, role)
        {
            if (triggerId == default(Guid))
            {
                triggerId = Guid.NewGuid();
            }

            this.Trigger = new ChangelistTrigger(change, files, monitoredDirs, triggerId);
            this.PrebuildCommands = prebuildCommands;
            this.PostbuildCommands = postbuildCommands;
            this.Labels = labels;
        }

        /// <summary>
        /// Load all the members from outside the class. This is used in certain situations such as converting from database objects.
        /// </summary>
        /// <param name="capabilityType"></param>
        /// <param name="completedAt"></param>
        /// <param name="consumedBy"></param>
        /// <param name="createdAt"></param>
        /// <param name="id"></param>
        /// <param name="postbuildCommands"></param>
        /// <param name="prebuildCommands"></param>
        /// <param name="jobPriority"></param>
        /// <param name="processedAt"></param>
        /// <param name="processingHost"></param>
        /// <param name="processingTime"></param>
        /// <param name="jobState"></param>
        /// <param name="trigger"></param>
        /// <param name="userData"></param>
        public Job(CapabilityType capabilityType, DateTime completedAt, Guid consumedBy, DateTime createdAt, Guid id, IEnumerable<string> postbuildCommands,
                    IEnumerable<string> prebuildCommands, JobPriority jobPriority, DateTime processedAt, string processingHost, TimeSpan processingTime, 
                    JobState jobState, ITrigger trigger, object userData)
        {
            this.Role = capabilityType;
            this.CompletedAt = completedAt;
            this.ConsumedByJobID = consumedBy;
            this.CreatedAt = createdAt;
            this.ID = id;
            this.PostbuildCommands = postbuildCommands;
            this.PrebuildCommands = prebuildCommands;
            this.Priority = jobPriority;
            this.ProcessedAt = processedAt;
            this.ProcessingHost = processingHost;
            this.ProcessingTime = processingTime;
            this.State = jobState;
            this.Trigger = trigger;
            this.UserData = userData;
        }

        //public Job(CapabilityType role, ITrigger trigger, DateTime completedAt, Guid consumedBy, DateTime completedAt, Guid id,  )


        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise job representation to XML.
        /// </summary>
        /// <returns></returns>
        public virtual XElement Serialise()
        {
            XElement xmlJob = new XElement("Job",
                new XAttribute("type", this.GetType().Name),
                new XElement("ID", this.ID),
                new XElement("ConsumedByJobID", this.ConsumedByJobID), 
                new XElement("State", this.State),
                new XElement("Priority", this.Priority),
                new XElement("Role", this.Role),
                this.Trigger.Serialise(),
                new XElement("CreatedAt", this.CreatedAt),
                new XElement("ProcessedAt", this.ProcessedAt),
                new XElement("CompletedAt", this.CompletedAt),
                String.IsNullOrEmpty(this.ProcessingHost) ? new XElement("ProcessingHost", "") : new XElement("ProcessingHost", this.ProcessingHost),
                String.IsNullOrEmpty(this.BranchName) ? new XElement("BranchName", "") : new XElement("BranchName", this.BranchName),
                String.IsNullOrEmpty(this.ProjectName) ? new XElement("ProjectName", "") : new XElement("ProjectName", this.ProjectName),
                new XElement("ProcessingTime", this.ProcessingTime.ToString()),
                new XElement("UserData", this.UserData)
            );

            if (this.PrebuildCommands != null && this.PrebuildCommands.Any())
            {
                xmlJob.Add(new XElement("PrebuildCommands", this.PrebuildCommands.Select(c => new XElement("PrebuildCommand", c))));
            }

            if (this.PostbuildCommands != null && this.PostbuildCommands.Any())
            {
                xmlJob.Add(new XElement("PostbuildCommands", this.PostbuildCommands.Select(c => new XElement("PostbuildCommand", c))));            
            }

            if (this.Labels != null && this.Labels.Any())
            {
                xmlJob.Add(new XElement("Labels", this.Labels.Where(l => !String.IsNullOrEmpty(l)).Select(label => new XElement("Label", label))));
            }
            
            return (xmlJob);
        }  

        /// <summary>
        /// Deserialise job representation from XML.
        /// </summary>
        /// <param name="xmlJobElem"></param>
        /// <returns></returns>
        public virtual bool Deserialise(XElement xmlJobElem)
        {
            bool result = true;
            try
            {
                this.ID = Guid.Parse(xmlJobElem.XPathSelectElement("ID").Value);

                XElement consumedByElement = xmlJobElem.XPathSelectElement("ConsumedByJobID");
                if (null != consumedByElement)
                    this.ConsumedByJobID = Guid.Parse(consumedByElement.Value);

                this.State = (JobState)Enum.Parse(typeof(JobState),
                    xmlJobElem.XPathSelectElement("State").Value);
                this.Priority = (JobPriority)Enum.Parse(typeof(JobPriority),
                    xmlJobElem.XPathSelectElement("Priority").Value);
                this.Role = (CapabilityType)Enum.Parse(typeof(CapabilityType),
                    xmlJobElem.XPathSelectElement("Role").Value);
                this.Trigger = TriggerFactory.Create(xmlJobElem.XPathSelectElement("Trigger"));

                this.m_dtCreatedAt = SettingsXml.DeserialiseDateTimeUtc(xmlJobElem.XPathSelectElement("CreatedAt"), DateTime.Now.ToUniversalTime());
                this.m_dtProcessedAt = SettingsXml.DeserialiseDateTimeUtc(xmlJobElem.XPathSelectElement("ProcessedAt"), DateTime.MaxValue.ToUniversalTime());
                this.m_dtCompletedAt = SettingsXml.DeserialiseDateTimeUtc(xmlJobElem.XPathSelectElement("CompletedAt"), DateTime.MaxValue.ToUniversalTime()); 

                this.m_processingHost = String.Empty;
                XElement processingHostElement = xmlJobElem.XPathSelectElement("ProcessingHost");
                if (null != processingHostElement)
                    this.m_processingHost = processingHostElement.Value;

                this.m_branchName = String.Empty;
                XElement branchNameElement = xmlJobElem.XPathSelectElement("BranchName");
                if (null  != branchNameElement)
                    this.m_branchName = branchNameElement.Value;

                this.m_projectName = String.Empty;
                XElement projectNameElement = xmlJobElem.XPathSelectElement("ProjectName");
                if (null != projectNameElement)
                    this.m_projectName = projectNameElement.Value;

                this.m_tsProcessingTime = TimeSpan.Zero;
                XElement processingTimeElement = xmlJobElem.XPathSelectElement("ProcessingTime");
                if (null != processingTimeElement)
                    TimeSpan.TryParse(processingTimeElement.Value, out this.m_tsProcessingTime);

                XElement ud = xmlJobElem.XPathSelectElement("UserData");
                if(ud != null)
                    this.UserData = ud.Value;

                IEnumerable<String> prebuildCommands = xmlJobElem.XPathSelectElements("PrebuildCommands/PrebuildCommand").Select(pbc => pbc.Value);

                if (prebuildCommands.Any())
                {
                    this.PrebuildCommands = new List<String>(prebuildCommands);
                }
                else
                {
                    this.PrebuildCommands = new List<String>();
                }

                IEnumerable<String> postbuildCommands = xmlJobElem.XPathSelectElements("PostbuildCommands/PostbuildCommand").Select(pbc => pbc.Value);

                if (postbuildCommands.Any())
                {
                    this.PostbuildCommands = new List<String>(postbuildCommands);
                }
                else
                {
                    this.PostbuildCommands = new List<String>();
                }

                IEnumerable<String> labels = xmlJobElem.XPathSelectElements("Labels/Label").Select(label => label.Value).Where( l => !String.IsNullOrEmpty(l));

                if (labels!= null && labels.Any())
                {
                    this.Labels = new List<String>(labels);
                }
                else
                {
                    this.Labels = new List<String>();
                }

            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Error deserialising job.");
                result = false;
            }
            return (result);
        }

        /// <summary>
        /// The friendly name of the job
        /// </summary>
        /// <returns></returns>
        public virtual String FriendlyName()
        {
            return String.Empty;
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Automation.Common.Jobs namespace
