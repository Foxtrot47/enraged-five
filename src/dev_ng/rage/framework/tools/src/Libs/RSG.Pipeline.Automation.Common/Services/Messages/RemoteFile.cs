﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;

namespace RSG.Pipeline.Automation.Common.Services.Messages
{

    /// <summary>
    /// Remote file object; with access to the remote file byte stream for 
    /// requesting uploads or receiving downloads.
    /// </summary>
    [MessageContract]
    public class RemoteFile : IDisposable
    {
        #region Properties 
        /// <summary>
        /// Filename string.
        /// </summary>
        [MessageHeader(MustUnderstand = true)]
        public String Filename { get; set; }

        /// <summary>
        /// Length of file data (bytes).
        /// </summary>
        [MessageHeader(MustUnderstand = true)]
        public long Length { get; set; }

        /// <summary>
        /// Pre-calculated 128-bit MD5 hash.
        /// </summary>
        [MessageHeader(MustUnderstand = true)]
        public Byte[] Hash { get; set; }

        /// <summary>
        /// Associated job identifier.
        /// </summary>
        [MessageHeader(MustUnderstand = true)]
        public Guid Job { get; set; }

        /// <summary>
        /// Data stream.
        /// </summary>
        [MessageBodyMember(Order = 1)]
        public Stream Stream { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor; required for WCF serialisation.
        /// </summary>
        public RemoteFile()
        {
        }

        /// <summary>
        /// Constructor; computes MD5 hash and sets up the Stream.
        /// </summary>
        /// <param name="job"></param>
        /// <param name="filename"></param>
        public RemoteFile(Guid job, String filename)
        {
            FileInfo fi = new FileInfo(filename);
            this.Job = job;
            this.Filename = filename;
            this.Stream = new FileStream(this.Filename, FileMode.Open, FileAccess.Read);
            this.Length = fi.Length;

            // Calculate MD5.
            this.Hash = RSG.Base.IO.FileMD5.ComputeMD5(this.Stream);
            this.Stream.Seek(0, SeekOrigin.Begin);
        }

        /// <summary>
        /// Constructor; indicating some failure of request.
        /// </summary>
        /// <param name="job"></param>
        /// <param name="filename"></param>
        /// <param name="failure"></param>
        public RemoteFile(Guid job, String filename, bool failure)
        {
            this.Job = job;
            this.Filename = filename;
            this.Stream = null;
            this.Length = 0;
            this.Hash = new Byte[16] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        }
        #endregion // Constructor(s)

        #region IDisposable Interface Methods
        /// <summary>
        /// Object dispose.
        /// </summary>
        public void Dispose()
        {
            if (null != this.Stream)
            {
                this.Stream.Close();
                this.Stream = null;
            }
        }
        #endregion // IDisposable Interface Methods
    }

} // RSG.Pipeline.Automation.Common.Services.Messages namespace
