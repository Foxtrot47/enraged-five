﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Triggers
{
    /// <summary>
    /// Username associated with a trigger.
    /// </summary>
    public interface IHasUsername
    {
        /// <summary>
        /// User associated with trigger.
        /// </summary>
        String Username { get; }
    }
}
