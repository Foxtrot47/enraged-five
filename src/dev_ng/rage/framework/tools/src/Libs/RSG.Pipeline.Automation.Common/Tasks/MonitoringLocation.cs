﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Notifications
{
    /// <summary>
    /// Allows for location based notifications.
    /// </summary>
    public sealed class MonitoringLocation
    {
        #region Properties
        /// <summary>
        /// Location which is being monitored for the notification.
        /// </summary>
        public String Location { get; private set; }

        /// <summary>
        /// Notificaiton rule for the location level.
        /// </summary>
        public NotificationRule Rule { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="location"></param>
        /// <param name="rule"></param>
        public MonitoringLocation(String location, NotificationRule rule)
        {
            this.Location = location;
            this.Rule = rule;
        }
        #endregion // Constructor
    }
}
