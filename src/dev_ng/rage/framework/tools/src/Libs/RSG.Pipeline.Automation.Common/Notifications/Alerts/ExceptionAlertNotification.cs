﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RSG.Base.Collections;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Services;

namespace RSG.Pipeline.Automation.Common.Notifications.Alerts
{
    /// <summary>
    /// 
    /// </summary>
    public class ExceptionAlertNotification
        : AlertNotificationBase
    {
        #region Properties

        private Exception Exception { get; set; }

        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="job"></param>
        /// <param name="maxErrors"></param>
        public ExceptionAlertNotification(Exception ex, ILog log, CommandOptions options, string summary = null, IEnumerable<string> detail = null)
            : base(log, summary, detail, options)
        {
            this.Exception = ex;
        }
        #endregion // Constructors

        #region Controller Methods

        /// <summary>
        /// Override for setting the subject 
        /// </summary>
        public override void SetSubject()
        {
            this.Subject = String.Format("[{1} {2}: Automation Alert] EXCEPTION: {0}", base.Summary, Options.CoreProject.FriendlyName, Options.Branch.Name);
        }

        /// <summary>
        /// Override for setting the body
        /// </summary>
        public override void SetBody()
        {
            this.Body.AppendLine(" ");
            this.Body.AppendLine("*** EXCEPTION ALERT ***");
            this.Body.AppendLine(" ");

            this.Body.AppendLine(Exception.ToString());
        }
        #endregion // Controller Methods
    }
}
