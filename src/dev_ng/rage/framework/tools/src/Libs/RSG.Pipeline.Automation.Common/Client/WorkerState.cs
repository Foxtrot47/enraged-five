﻿using System;
using System.Runtime.Serialization;

namespace RSG.Pipeline.Automation.Common.Client
{

    /// <summary>
    /// Enumeration representing possible client states.
    /// </summary>
    [DataContract]
    public enum WorkerState
    {
        [EnumMember]
        Idle,

        [EnumMember]
        Busy,

        [EnumMember]
        ShuttingDown,

        [EnumMember]
        Error,
    }

} // RSG.Pipeline.Automation.Common.Job namespace
