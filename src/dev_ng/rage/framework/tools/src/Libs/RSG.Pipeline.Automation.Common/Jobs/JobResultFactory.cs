﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;

namespace RSG.Pipeline.Automation.Common.Jobs
{

    /// <summary>
    /// Job Result factory for deserialisation of IJobResult objects.
    /// </summary>
    internal static class JobResultFactory
    {
        #region Constants
        private static readonly String LOG_CTX = "Job Result Factory";
        #endregion // Constants

        #region Member Data
        private static IDictionary<String, Type> JobResultTypes;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Static constructor.
        /// </summary>
        static JobResultFactory()
        {
            JobResultFactory.JobResultTypes = new Dictionary<String, Type>();
            Assembly assembly = Assembly.GetExecutingAssembly();
            foreach (Type type in assembly.GetTypes())
            {
                if (type.IsAbstract)
                    continue;
                if (type.IsInterface)
                    continue;

                if (typeof(IJobResult).IsAssignableFrom(type))
                    JobResultFactory.JobResultTypes.Add(type.Name, type);
            }
        }
        #endregion // Constructor(s)

        /// <summary>
        /// Create jobResult object; deserialising from XML.
        /// </summary>
        /// <param name="xmlJob"></param>
        /// <returns></returns>
        public static IJobResult Create(XElement xmlJobResult)
        {
            XAttribute xmlTypeAttr = ((IEnumerable)xmlJobResult.XPathEvaluate("@type")).
                Cast<XAttribute>().FirstOrDefault();
            if (null == xmlTypeAttr)
            {
                Log.Log__ErrorCtx(LOG_CTX, "Serialised JobResult does not contain type attribute!");
                return (null);
            }

            String typeName = xmlTypeAttr.Value;
            if (String.IsNullOrEmpty(typeName))
            {
                Log.Log__ErrorCtx(LOG_CTX, "Invalid type name for jobResult XML.");
                return (null);
            }

            if (JobResultFactory.JobResultTypes.ContainsKey(typeName))
            {
                IJobResult jobResult = (IJobResult)Activator.CreateInstance(JobResultFactory.JobResultTypes[typeName]);
                jobResult.Deserialise(xmlJobResult);
                return (jobResult);
            }
            else
            {
                Log.Log__ErrorCtx(LOG_CTX, "Job Result factory unknown type: {0}.", typeName);
                return (null);
            }
        }
    }

} // RSG.Pipeline.Automation.Common.Jobs namespace

