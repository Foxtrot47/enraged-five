﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Logging;

namespace RSG.Pipeline.Automation.Common.Jobs
{

    /// <summary>
    /// Job factory for deserialisation of IJob objects.
    /// </summary>
    internal static class JobFactory
    {
        #region Constants
        private static readonly String LOG_CTX = "Job Factory";
        #endregion // Constants

        #region Member Data
        private static IDictionary<String, Type> JobTypes;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Static constructor.
        /// </summary>
        static JobFactory()
        {
            JobFactory.JobTypes = new Dictionary<String, Type>();
            Assembly assembly = Assembly.GetExecutingAssembly();
            foreach (Type type in assembly.GetTypes())
            {
                if (type.IsAbstract)
                    continue;
                if (type.IsInterface)
                    continue;

                if (typeof(IJob).IsAssignableFrom(type))
                    JobFactory.JobTypes.Add(type.Name, type);
            }
        }
        #endregion // Constructor(s)

        /// <summary>
        /// Create job object; deserialising from XML.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="xmlJob"></param>
        /// <returns></returns>
        public static IJob Create(IBranch branch, XElement xmlJob)
        {
            XAttribute xmlTypeAttr = ((IEnumerable)xmlJob.XPathEvaluate("@type")).
                Cast<XAttribute>().FirstOrDefault();
            if (null == xmlTypeAttr)
            {
                Log.Log__ErrorCtx(LOG_CTX, "Serialised Job does not contain type attribute!");
                return (null);
            }

            String typeName = xmlTypeAttr.Value;
            if (String.IsNullOrEmpty(typeName))
            {
                Log.Log__ErrorCtx(LOG_CTX, "Invalid type name for job XML.");
                return (null);
            }

            if (JobFactory.JobTypes.ContainsKey(typeName))
            {
                // DW: This is a temporary measure - perhaps...
                // We can't have two distinct factories creating jobs, instead nesting the factories seems a workaround for now as we fix up each job type.
                // Using 2 stages of reflection we 1. get the factory the job uses and 2. call its CreateJob(XElement) method. This could well be overkill (discuss)
                // don't really like this factory to be honest - a factory just for deserialisation?!... one factory for all might be better.
                Type jobType = JobFactory.JobTypes[typeName];
                MethodInfo methodInfo = jobType.GetMethod("GetFactory");
                if (null != methodInfo)
                {
                    Type factoryType = (Type)methodInfo.Invoke(null, null);
                    if (factoryType != null)
                    {
                        IJob jobCreated = (IJob)factoryType.GetMethod("CreateJob").Invoke(null, new object[] { branch, xmlJob });
                        return jobCreated;
                    }
                }

                IJob job = (IJob)Activator.CreateInstance(JobFactory.JobTypes[typeName]);
                job.Deserialise(xmlJob);
                return (job);
            }
            else
            {
                Log.Log__ErrorCtx(LOG_CTX, "Job factory unknown type: {0}.", typeName);
                return (null);
            }
        }
    }

} // RSG.Pipeline.Automation.Common.Jobs namespace
