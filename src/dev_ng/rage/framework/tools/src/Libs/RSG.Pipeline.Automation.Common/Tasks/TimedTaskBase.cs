﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Collections;
using System.IO;
using System.Linq;
using System.Xml.XPath;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Services;

namespace RSG.Pipeline.Automation.Common.Tasks
{

    /// <summary>
    /// 
    /// </summary>
    public abstract class TimedTaskBase : 
        TaskBase,
        ITimedTask
    {
        #region Properties
        /// <summary>
        /// Enumerable of time trigger objects.  Created 
        /// </summary>
        public IEnumerable<TimedTrigger> TimedTriggers
        {
            get;
            private set;
        }

        /// <summary>
        /// Schedule of TimedTaskEvents
        /// </summary>
        public TimedTaskSchedule TimedTaskSchedule
        {
            get;
            private set;
        }
        
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; specifying start time and interval.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="description"></param>
        /// <param name="role"></param>
        /// <param name="start"></param>
        /// <param name="interval"></param>
        public TimedTaskBase(String name, String description, CapabilityType role, CommandOptions options)
            : base(name, description, role, options)
        {
            this.TimedTriggers = new List<TimedTrigger>();
            this.TimedTaskSchedule = new TimedTaskSchedule();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        

        /// <summary>
        /// Base implementation for populating timed task data.
        /// </summary>
        public virtual void LoadTimedTasks()
        {
            List<TimedTrigger> validTriggers = new List<TimedTrigger>();
            List<TimedTaskEvent> events = new List<TimedTaskEvent>();
            List<TimedTrigger> allTriggers = new List<TimedTrigger>();

            String timeTriggerFile = GetTimedTasksFile();
            if (File.Exists(timeTriggerFile))
            {
                try
                {
                    XDocument xdoc = XDocument.Load(timeTriggerFile);
                    foreach (XElement elem in xdoc.Descendants("TimedTrigger"))
                    {
                        String name = elem.Attribute("name").Value;
                        DateTime start = Convert.ToDateTime(elem.Attribute("start").Value);
                        DateTime end = DateTime.MaxValue;
                        if (null != elem.Attribute("end"))
                            end = Convert.ToDateTime(elem.Attribute("end").Value);

                        // Discard if this trigger has scheduled to end.
                        if (end < DateTime.UtcNow)
                            continue;

                        TimeSpan interval = TimeSpan.FromHours(Convert.ToDouble(elem.Attribute("interval").Value));
                        TimedTrigger trigger = new TimedTrigger(name, start, end, interval);
                        // Only add newly added triggers.
                        if (this.TimedTriggers.Where(t => t.Name == name).Count() == 0)
                        {
                            validTriggers.Add(trigger);
                            events.Add(new TimedTaskEvent(TimedTaskBase.GetNextScheduledTime(trigger), trigger));
                        }

                        allTriggers.Add(trigger);
                    }

                    // Remove triggers that have been removed.
                    List<TimedTrigger> removedTriggers = new List<TimedTrigger>();
                    foreach (TimedTrigger tt in this.TimedTriggers)
                    {
                        if (allTriggers.Where(at => at.Name == tt.Name).Count() > 0)
                            continue;
                        else
                            removedTriggers.Add(tt);
                    }

                    foreach (TimedTrigger timedTrigger in removedTriggers)
                    {
                        (this.TimedTriggers as List<TimedTrigger>).Remove(timedTrigger);

                        // Remove any scheduled events that haven't fired yet.
                        IEnumerable<TimedTaskEvent> danglingTimedTasks = this.TimedTaskSchedule.Events.Where(te => te.TimedTrigger == timedTrigger && te.ActualTime == DateTime.MinValue);
                    }

                    (this.TimedTaskSchedule.Events as List<TimedTaskEvent>).AddRange(events);
                    (this.TimedTriggers as List<TimedTrigger>).AddRange(validTriggers);
                }
                catch (Exception ex)
                {
                    this.Log.ExceptionCtx(LOG_CTX, ex, "Exception loading timed tasks from {0}", timeTriggerFile);
                }
            }
            else
            {
                this.Log.ErrorCtx(LOG_CTX, "Error loading timed task data.  No tasks will execute.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override TaskStatus Status()
        {
            return (new TaskStatus(this));
        }

        /// <summary>
        /// Load state from disk.
        /// </summary>
        public override void Load()
        {
            this.Log.MessageCtx(LOG_CTX, "Loading state from {0}.", this.Filename);
            try
            {
                if (!File.Exists(this.Filename))
                    return; // No state to load.

                XDocument xmlDoc = XDocument.Load(this.Filename);
                String typeName = ((IEnumerable)xmlDoc.Root.XPathEvaluate("/Task/@type")).
                    Cast<XAttribute>().First().Value;
                Debug.Assert(0 == String.Compare(this.GetType().Name, typeName),
                    String.Format("Loading task status information from incorrect file: {0} {1}.",
                    this.Name, this.Filename));

                // Job list.
                IEnumerable<XElement> xmlJobs = xmlDoc.Root.XPathSelectElements("/Task/Jobs/Job");
                foreach (XElement xmlJob in xmlJobs)
                {
                    IJob job = JobFactory.Create(this.Config.CoreProject.DefaultBranch, xmlJob);
                    if (null != job)
                        this.m_Jobs.Enqueue(job.Priority, job);
                }
                this.Log.MessageCtx(LOG_CTX, "\t{0} jobs loaded from state data.",
                    this.m_Jobs.Count);
            }
            catch (Exception ex)
            {
                this.Log.ExceptionCtx(LOG_CTX, ex, "Error deserialising status from disk: {0}.",
                    this.Filename);
            }
        }

        /// <summary>
        /// Save state to disk.
        /// </summary>
        public override void Save()
        {
            this.Log.MessageCtx(LOG_CTX, "Saving state to {0}.", this.Filename);
            try
            {
                XDocument xmlDoc = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "yes"),
                    new XElement("Task",
                        new XAttribute("name", this.Name),
                        new XAttribute("type", this.GetType().Name),
                        new XElement("Jobs", this.Jobs.Select(j => j.Serialise())))
                );

                String directoryName = Path.GetDirectoryName(this.Filename);
                if (!Directory.Exists(directoryName))
                    Directory.CreateDirectory(directoryName);
                xmlDoc.Save(this.Filename);
            }
            catch (Exception ex)
            {
                this.Log.ExceptionCtx(LOG_CTX, ex, "Error serialising status to disk: {0}.",
                    this.Filename);
            }
        }

        /// <summary>
        /// Update() inhertied from Taskbase is sealed to ensure timed task execution is handled correctly TimedTaskEvent execution.
        /// </summary>
        public sealed override void Update()
        {
            this.LoadTimedTasks();
            TimedTaskEvent timedTaskEvent = this.GetNextTimedTaskEvent();
            if (timedTaskEvent == null)
                return;
            else
            {
                this.ExecuteTimedTask(timedTaskEvent);

                //Remove this timed event events, then create the next event.
                (this.TimedTaskSchedule.Events as List<TimedTaskEvent>).Remove(timedTaskEvent);
                (this.TimedTaskSchedule.Events as List<TimedTaskEvent>).Add(new TimedTaskEvent(TimedTaskBase.GetNextScheduledTime(timedTaskEvent.TimedTrigger), timedTaskEvent.TimedTrigger));
            }
        }
        #endregion // Controller Methods

        #region Abstract Methods
        /// <summary>
        /// Execute the timed task for a given TimedTaskEvent.
        /// </summary>
        /// <param name="timedTaskEvent"></param>
        public abstract void ExecuteTimedTask(TimedTaskEvent timedTaskEvent);

        /// <summary>
        /// Get location of file describing tasks TimedTrigger(s).
        /// </summary>
        /// <returns></returns>
        public abstract String GetTimedTasksFile();
        #endregion // Abstract Methods

        #region Private Methods
        /// <summary>
        /// Evaluate whether the timed task requires execution.
        /// </summary>
        /// <returns></returns>
        TimedTaskEvent GetNextTimedTaskEvent()
        {
            foreach (TimedTrigger trigger in this.TimedTriggers)
            {
                TimedTaskEvent timedTaskEvent = TimedTaskSchedule.Events.Where(e => e.TimedTrigger == trigger &&
                                                                                DateTime.UtcNow > e.ScheduledTime &&
                                                                                trigger.End > DateTime.UtcNow &&
                                                                                e.ActualTime == DateTime.MinValue).Select(e => e).FirstOrDefault();
                if (timedTaskEvent != null)
                    return timedTaskEvent;
            }
            return null;
        }

        /// <summary>
        /// Calculate the next time the schedueld task needs to execute. 
        /// </summary>
        /// <param name="start"></param>
        /// <param name="interval"></param>
        /// <returns></returns>
        protected static DateTime GetNextScheduledTime(TimedTrigger timedTrigger)
        {
            TimeSpan duration = DateTime.UtcNow - timedTrigger.Start;
            double ticksToNextEvent = ((double)duration.Ticks % (double)timedTrigger.Interval.Ticks) / (double)timedTrigger.Interval.Ticks;
            TimeSpan variation = timedTrigger.Interval - TimeSpan.FromTicks((long)(ticksToNextEvent * timedTrigger.Interval.Ticks));
            return DateTime.UtcNow + variation;
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.Common.Tasks namespace
