﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.SourceControl.Perforce;
using RSG.Base.Logging;

namespace RSG.Pipeline.Automation.Common.Batch
{
    /// <summary>
    /// A key for which we use in a dictionary when batching up files across projects & branches. ( or any other criteria )
    /// - Wraps the Tuple
    /// </summary>
    public class ProjectBatchKey
        : Tuple<IProject, IBranch>
    {
        #region Properties
        /// <summary>
        /// Gets the Platform from the Tuple key
        /// </summary>
        public IProject Project { get { return base.Item1; } }

        /// <summary>
        /// Gets the Branch from the Tuple key
        /// </summary>
        public IBranch Branch { get { return base.Item2; } }
        #endregion // Properties

        #region Constructors
        public ProjectBatchKey(IProject project, IBranch branch)
            : base(project, branch)
        {
        }
        #endregion // Constructors
    }
}
