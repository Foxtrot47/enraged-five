﻿using System;
using System.ServiceModel;

namespace RSG.Pipeline.Automation.Common.Services.Messages
{

    /// <summary>
    /// Remote file information.
    /// </summary>
    [MessageContract]
    public class RemoteFileInfo
    {
        #region Properties
        /// <summary>
        /// full path to file on server
        /// </summary>
        public String FullPath { get; set; }

        /// <summary>
        /// Path relative to server directory
        /// </summary>
        public String RelativePath { get; set; }

        /// <summary>
        /// Filename.
        /// </summary>
        public String Filename { get; set; }

        /// <summary>
        /// File size (in bytes).
        /// </summary>
        public long FileSize { get; set; }
        #endregion // Properties
    }

} // RSG.Pipeline.Automation.Common.Services.Messages namespace
