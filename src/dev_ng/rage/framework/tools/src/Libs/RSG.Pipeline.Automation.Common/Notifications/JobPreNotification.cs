using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Base.Configuration;
using RSG.Pipeline.Services;

namespace RSG.Pipeline.Automation.Common.Notifications
{
    /// <summary>
    /// 
    /// </summary>
    public class JobPreNotification
        : JobNotificationBase
    {
        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="job"></param>
        /// <param name="maxErrors"></param>
        public JobPreNotification(IJob job, IEnumerable<IJob> associatedJobs, ILog log, Uri workerUri, Guid workerGuid, CommandOptions options, String logFilename, bool html = false)
            : base(job, associatedJobs, null, log, workerUri, workerGuid, html, options)
        {
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// Override for setting the body of the job notification email
        /// </summary>
        public override void SetBody()
        {
            this.Body.AppendLine(String.Format("\nJob {0} has been dispatched for processing.", this.Job.ID));
            base.SetBody();
        }

        /// <summary>
        /// Email body footer builder.
        /// </summary>
        public override void SetFooter()
        {
            // No footer
        }
        #endregion // Controller Methods
    }
}
