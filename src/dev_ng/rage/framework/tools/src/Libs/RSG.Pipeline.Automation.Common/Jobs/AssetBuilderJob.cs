﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Pipeline.Core;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Automation.Common.Triggers;

namespace RSG.Pipeline.Automation.Common.Jobs
{
    /// <summary>
    /// Asset builder client side job.
    /// </summary>
    [DataContract]
    public class AssetBuilderJob :
        Job,
        IJob
    {
        #region Properties
        /// <summary>
        /// Engine flags 
        /// </summary>
        [DataMember]
        public EngineFlags EngineFlags { get; set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Default constructor (for deserialisation).
        /// </summary>
        public AssetBuilderJob()
            : base()
        {
            Role = CapabilityType.AssetBuilder;
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public AssetBuilderJob(IBranch branch)
            : base(branch, CapabilityType.AssetBuilder)
        {
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public AssetBuilderJob(IBranch branch, Changelist change)
            : base(branch, CapabilityType.AssetBuilder, change)
        {
            EngineFlags = EngineFlags.Default;
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public AssetBuilderJob(IBranch branch, Changelist change, IEnumerable<String> pathnamesToProcess, Guid triggerId = default(Guid))
            : base(branch, CapabilityType.AssetBuilder, change, pathnamesToProcess, triggerId)
        {
            EngineFlags = EngineFlags.Default;
        }
        
        /// <summary>
        /// Constructor taking an enumerable of triggers.
        /// </summary>
        /// <param name="triggers"></param>
        /// <param name="engineFlags"></param>
        public AssetBuilderJob(IBranch branch, IEnumerable<ITrigger> triggers, EngineFlags engineFlags)
            : base(branch, CapabilityType.AssetBuilder, triggers)
        {
            EngineFlags = engineFlags;
        }

        public AssetBuilderJob(CapabilityType capabilityType, DateTime completedAt, Guid consumedBy, DateTime createdAt, Guid id, IEnumerable<string> postbuildCommands,
                    IEnumerable<string> prebuildCommands, JobPriority jobPriority, DateTime processedAt, string processingHost, TimeSpan processingTime, 
                    JobState jobState, ITrigger trigger, object userData, EngineFlags engineFlags)
            : base(capabilityType, completedAt, consumedBy, createdAt, id, postbuildCommands, prebuildCommands, jobPriority, processedAt, processingHost, 
                    processingTime, jobState, trigger, userData)
        {
            this.EngineFlags = engineFlags;
        }
        #endregion // Constructors
        
        #region Controller Methods
        /// <summary>
        /// Serialise job representation to XML.
        /// </summary>
        /// <returns></returns>
        public override XElement Serialise()
        {
            XElement xmlJob = base.Serialise();
            xmlJob.Add(new XElement("EngineFlags", this.EngineFlags));

            return (xmlJob);
        }

        /// <summary>
        /// Deserialise job representation from XML.
        /// </summary>
        /// <param name="xmlJobElem"></param>
        /// <returns></returns>
        public override bool Deserialise(XElement xmlJobElem)
        {
            bool result = true;
            try
            {
                result &= base.Deserialise(xmlJobElem);
                this.EngineFlags = (EngineFlags)Enum.Parse(typeof(EngineFlags),
                    xmlJobElem.XPathSelectElement("EngineFlags").Value);
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Error deserialising Asset Builder Job.");
                result = false;
            }
            return (result);
        }
        #endregion // Controller Methods

        #region Public Methods
        /// <summary>
        /// Jobs don't normally change but here it is transformed when consumed by another job.
        /// </summary>
        /// <param name="jobConsuming">the job consuming this job</param>
        public void SetConsumed(IJob jobConsuming)
        {
            // Hold a 'guid reference' to the consuming job
            this.ConsumedByJobID = jobConsuming.ID;

            // Change state to no longer be pending but is now consumed.
            this.State = JobState.SkippedConsumed;
        }
        #endregion // Public Methods.
    }
}
