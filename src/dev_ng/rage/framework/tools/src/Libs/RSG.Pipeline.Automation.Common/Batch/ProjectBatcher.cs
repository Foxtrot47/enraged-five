using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.SourceControl.Perforce;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Services;

namespace RSG.Pipeline.Automation.Common.Batch
{
    /// <summary>
    /// Helper of project/ branch batching of files.
    /// - splits up an array of filesystem paths into batches that belong in project & branch.
    /// - the files are returned as a dictionary so we can know by what criteria they were split up by.
    /// </summary>
    public static class ProjectBatcher
    {
        /// <summary>
        /// Create a dictionary of the filemappings ( batches )
        /// - a new collection of filemappings is keyed for each project. ( facilitated with a Tuple key. )
        /// - dictionary allows the keyed data to grow as we match new projects and branch which the files reside within.
        /// </summary>
        /// <param name="files"></param>
        /// <param name="options"></param>
        /// <param name="branchFilters">if set - only branches of this name should be batched.</param>
        /// <returns>files are returned as a dictionary so we can know by what criteria they were split up by</returns>
        public static IDictionary<ProjectBatchKey, IEnumerable<FileMapping>> Batch(IEnumerable<FileMapping> files, CommandOptions options, IUniversalLog log, IEnumerable<String> branchFilters = null)
        {
            IDictionary<String, IProject> allProjects = options.Config.AllProjects();
            IDictionary<ProjectBatchKey, ICollection<FileMapping>> dict = new Dictionary<ProjectBatchKey, ICollection<FileMapping>>();

            foreach (KeyValuePair<String, IProject> projectKvp in allProjects)
            {
                IProject project = projectKvp.Value;

                // You can only build for the branch you are running with - in options.branch
                // This has confused me before but lets make it abundantly clear - you cannot build for more than one branch 
                // - unless of course these are branches of the same name in DLC.
                foreach (KeyValuePair<String, IBranch> branchKvp in project.Branches.Where(b => options.Branch.Name.Equals(b.Key, StringComparison.CurrentCultureIgnoreCase)))
                {
                    IBranch branch = branchKvp.Value;

                    // No batches created for locked branches.
                    if (!branch.Locked)
                    {
                        // Insert into correct batch in dictionary
                        foreach (FileMapping fm in files)
                        {
                            String normalisedFilename = System.IO.Path.GetFullPath(fm.LocalFilename).ToLower();
                            if (normalisedFilename.StartsWith(branch.Export, StringComparison.OrdinalIgnoreCase) ||
                                normalisedFilename.StartsWith(branch.Processed, StringComparison.OrdinalIgnoreCase))
                            {
                                // Create key
                                ProjectBatchKey projectBranchKey = new ProjectBatchKey(project, branch);

                                // Check if filtered
                                if (branchFilters != null && 
                                    branchFilters.Any() && 
                                    !branchFilters.Contains<String>(branch.Name, StringComparer.CurrentCultureIgnoreCase))
                                {
                                    log.Warning("Branch filter(s) specified - this means we should not batch for Project {0}, Branch {1}", project.Name, branch.Name);
                                    continue;
                                }

                                // Insert into dictionary
                                if (!dict.ContainsKey(projectBranchKey))
                                {
                                    // New key
                                    ICollection<FileMapping> firstFile = new List<FileMapping>() { fm };
                                    dict.Add(projectBranchKey, firstFile);
                                }
                                else
                                {
                                    // Append
                                    dict[projectBranchKey].Add(fm);
                                }
                            }
                        }
                    }
                }
            }

            // DW: If you ever stumble across this and know a better way please enlighten me ( striving for const correctness - I don't want to return a Collection ) 
            IDictionary<ProjectBatchKey, IEnumerable<FileMapping>> result = new Dictionary<ProjectBatchKey, IEnumerable<FileMapping>>();
            dict.ToList().ForEach(kvp => result.Add(kvp.Key, kvp.Value));
            
            return result;
        }

        /// <summary>
        /// Logs the batches
        /// </summary>
        /// <param name="changeListFiles"></param>
        /// <param name="changeListFilesBatched"></param>
        public static void DisplayBatched(IEnumerable<FileMapping> files, IDictionary<ProjectBatchKey, IEnumerable<FileMapping>> filesBatched, IUniversalLog log)
        {
            log.Message("Files : ({0}) Batches : ({1})", files.Count(), filesBatched.Count);
            foreach (KeyValuePair<ProjectBatchKey, IEnumerable<FileMapping>> batchedFilesKvp in filesBatched)
            {
                // Decipher kvp
                ProjectBatchKey projectBranchKey = batchedFilesKvp.Key;
                IEnumerable<FileMapping> batchedFiles = batchedFilesKvp.Value;
                log.Message("\t{0} {1}", projectBranchKey.Project.Name, projectBranchKey.Branch.Name);
                batchedFiles.ForEach(bf => log.Message("\t\t{0}", bf.DepotFilename));
            }
        }
    }
}
