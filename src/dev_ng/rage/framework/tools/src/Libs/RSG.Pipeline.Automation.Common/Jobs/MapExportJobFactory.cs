﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using SIO = System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;
using RSG.Pipeline.Content.Algorithm;
using RSG.SourceControl.Perforce;
using RSG.SceneXml;

namespace RSG.Pipeline.Automation.Common.Jobs
{

    /// <summary>
    /// MapExportJob object factory.
    /// </summary>
    public static class MapExportJobFactory
    {
        /// <summary>
        /// Create a MapExportJob given a map name (user-defined trigger).
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="tree"></param>
        /// <param name="p4"></param>
        /// <param name="mapDccFilename"></param>
        /// <param name="network"></param>
        /// <returns></returns>
        public static MapExportJob Create(IBranch branch, IContentTree tree, 
            P4API.P4Connection p4, String mapDccFilename, bool network)
        {
            MapExportJob job = CreateBasicJob(branch, tree, p4, mapDccFilename, network);
            if (null == job)
                return (null);

            IEnumerable<String> files = job.ExportProcesses.Select(p => p.DCCSourceFilename);
            job.Trigger = new Triggers.UserRequestTrigger(Environment.UserName, files, Guid.NewGuid(), DateTime.UtcNow);

            return (job);
        }

        /// <summary>
        /// Create a MapExportJob given a set of map names (user-defined trigger).
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="tree"></param>
        /// <param name="p4"></param>
        /// <param name="mapDccFilenames"></param>
        /// <param name="network"></param>
        /// <returns></returns>
        public static IEnumerable<MapExportJob> Create(IBranch branch, IContentTree tree,
            P4API.P4Connection p4, IEnumerable<String> mapDccFilenames, bool network)
        {
            ICollection<MapExportJob> jobs = new List<MapExportJob>();
            foreach (String mapDccFilename in mapDccFilenames)
            {
                MapExportJob job = Create(branch, tree, p4, mapDccFilename, network);
                if (null == job)
                    continue;

                IEnumerable<String> files = job.ExportProcesses.Select(p => p.DCCSourceFilename);
                job.Trigger = new Triggers.UserRequestTrigger(Environment.UserName, files);
                jobs.Add(job);
            }
            return (jobs);
        }

        /// <summary>
        /// Create a MapExportJob given a changelist (changelist trigger).
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="tree"></param>
        /// <param name="p4"></param>
        /// <param name="changelist"></param>
        /// <param name="network"></param>
        /// <returns></returns>
        public static IEnumerable<MapExportJob> Create(IBranch branch, IContentTree tree,
            P4API.P4Connection p4, Changelist changelist, bool network)
        {
            ICollection<MapExportJob> jobs = new List<MapExportJob>();
            FileMapping[] fms = FileMapping.Create(p4, changelist.Files.ToArray());

            foreach (FileMapping fm in fms)
            {
                String filename = fm.LocalFilename;
                if (!filename.EndsWith(".max") && !filename.EndsWith(".maxc"))
                    continue;
                
                // Create job for max file.
                MapExportJob job = Create(branch, tree, p4, filename, network);
                if (null == job)
                    continue;

                job.Trigger = new Triggers.ChangelistTrigger(changelist);
                jobs.Add(job);
            }
            return (jobs);
        }

        #region Private Methods        
        /// <summary>
        /// Create base MapExportJob object (using AP3 content-tree).
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="tree"></param>
        /// <param name="p4"></param>
        /// <param name="mapDccFilename"></param>
        /// <param name="network"></param>
        /// <returns></returns>
        private static MapExportJob CreateBasicJob(IBranch branch, IContentTree tree, P4API.P4Connection p4, String mapDccFilename, bool network)
        {
            Debug.Assert(mapDccFilename.EndsWith(".max") || mapDccFilename.EndsWith(".maxc"),
                "Invalid Map DCC Source Filename.");
            if (!tree.IsFile(mapDccFilename))
            {
                Log.Log__Warning("DCC Source File: {0} is not a max file known by the Content-Tree.  Ignoring.",
                    mapDccFilename);
                return (null);
            }

            IContentNode mapNode = tree.CreateFile(mapDccFilename);
            IEnumerable<IProcess> processes = tree.FindProcessesWithInput(mapNode);
            IProcess exportProcess = processes.FirstOrDefault();
            if (null == exportProcess)
            {
                Log.Log__Warning("DCC Source File: {0} is not a map.  Ignoring.",
                    mapDccFilename);
                return (null);
            }

            String basename = SIO.Path.GetFileNameWithoutExtension(mapDccFilename).ToLower();
            List<String> exportFiles = new List<String>();
            List<String> sourceTextures = new List<String>();

            foreach (IProcess process in processes)
            {
                if (!process.ProcessorClassName.Equals("RSG.Pipeline.Processor.Common.Dcc3dsmaxExportProcessor"))
                    continue;

                foreach (IContentNode output in process.Outputs)
                {
                    if (!(output is Content.File))
                        continue;

                    Content.File fileOutput = (output as Content.File);
                    exportFiles.Add(fileOutput.AbsolutePath);
                    if (fileOutput.AbsolutePath.EndsWith(".zip"))
                    {
                        String sceneXmlFilename = SIO.Path.ChangeExtension(fileOutput.AbsolutePath, ".xml");
                        exportFiles.Add(sceneXmlFilename);

                        if (SIO.File.Exists(sceneXmlFilename))
                        {
                            Scene scene = Scene.Load(sceneXmlFilename, SceneXml.LoadOptions.All, false);
                            if (scene != null)
                            {
                                scene.GetSceneTextureFilenames(ref sourceTextures, true);
                            }
                            else
                            {
                                Log.Log__Error("Scene XML load of {0} for map name {1} failed, texture usage couldn't be found!",
                                    sceneXmlFilename, mapDccFilename);
                            }
                        }
                        else
                        {
                            Log.Log__Error("Scene XML file does not exist! {0}", sceneXmlFilename);
                        }
                    }
                }
                exportFiles.AddRange(process.Outputs.Select(output => (output as Content.File).AbsolutePath));
            }

            if (!sourceTextures.Any())
            {
                Log.Log__Message("No source textures in map section {0} to check in map network export job setup.", 
                    mapDccFilename);
            }
            List<String> editedSourceTextures = Utils.CommonFuncs.GetTexturesRequiredToSend(p4, sourceTextures.ToList());
            if (editedSourceTextures.Count == 0)
            {
                Log.Log__Message("No edited source textures to send with map network export job.");
            }

            MapExportJob job = new MapExportJob(branch, tree, new String[] { mapDccFilename }, exportFiles, editedSourceTextures, network);
            return (job);
        }
        #endregion // Private Methods
    } // class MapExportJobFactory
} // namespace RSG.Pipeline.Automation.Consumers.Jobs
