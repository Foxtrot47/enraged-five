﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Services;
using RSG.Pipeline.Automation.Common.Utils;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.SourceControl.Perforce;
using RSG.SourceControl.Perforce.Util;

namespace RSG.Pipeline.Automation.Common.JobManagement
{
    /// <summary>
    /// Comparer for job groups.
    /// </summary>
    public sealed class DispatchGroupComparer :
        IComparer<DispatchGroup>
    {
        #region IComparer<DispatchGroup> Interface Methods
        /// <summary>
        /// Compare two job groups.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public int Compare(DispatchGroup a, DispatchGroup b)
        {
            if (a.Priority == b.Priority)
                return (0);
            else if (a.Priority < b.Priority)
                return (-1);
            else
                return (1);
        }
        #endregion // IComparer<DispatchGroup> Interface Methods
    }

    #region Enums
    /// <summary>
    /// 
    /// </summary>
    public enum Priority
    {
        High,
        Standard,
        Low,
    }
    #endregion // Enums

    /// <summary>
    ///  Class for managing job groups
    /// </summary>
    public sealed class DispatchScheme
    {
        #region Constants
        /// <summary>
        /// The task log context.
        /// </summary>
        private static readonly String LOG_CTX = "Automation:JobManager";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// List of available worker machines (this definately isnt the best way of handling this...)
        /// </summary>
        //IEnumerable<String> WorkerMachines { get; set; }

        /// <summary>
        /// Dispatch groups that make up the dispatch scheme.
        /// </summary>
        public IEnumerable<DispatchGroup> Groups { get; set; }

        /// <summary>
        /// Configuration object.
        /// </summary>
        public IConfig Config { get; private set; }

        /// <summary>
        /// Configuration object.
        /// </summary>
        public CommandOptions CommandOptions { get; private set; }

        /// <summary>
        /// Universal log object
        /// </summary>
        public IUniversalLog Log { get; private set; }

        #endregion // Properties

        #region Constructors
        /// <summary>
        /// JobGroupManager constructor
        /// </summary>
        public DispatchScheme(IConfig config, CommandOptions options, IUniversalLog log, String filename)
        {
            this.Config = config;
            this.CommandOptions = options;
            this.Log = log;
            this.Groups = new List<DispatchGroup>();

            bool syncDispatch = true;
            this.LoadDispatchData(filename, options, syncDispatch);
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// Loads dispatch group data from data file.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="sync">optional sync P4</param>
        /// <returns></returns>
        public bool LoadDispatchData(String filename, CommandOptions options, bool sync = false)
        {
            try
            {
                List<DispatchGroup> groups = new List<DispatchGroup>();
                if (!File.Exists(filename))
                {
                    this.Log.ErrorCtx(LOG_CTX, "Job manager file does not exist: {0}.  Automation service will dispatch jobs on a FIFO basis.", filename);
                    return false;
                }

                // Optionally sync to the dispatch file before opening it.
                if (sync)
                {
                    SyncDispatchFile(filename, options);
                }

                XDocument xdoc = XDocument.Load(filename);
                IEnumerable<XElement> groupElements = SettingsXml.GetElements(this.CommandOptions.Branch, this.Log, xdoc, "groups", "group");

                using (P4 p4 = options.Config.Project.SCMConnect())
                {
                    foreach (XElement groupElem in groupElements)
                    {
                        DispatchGroup dispatchGroup = CreateDispatchGroup(groupElem, p4);
                        groups.Add(dispatchGroup);
                    }
                }

                groups.Sort(new DispatchGroupComparer());
                lock (this.Groups)
                {
                    // Preserve the jobguids assigned to the group.
                    foreach (DispatchGroup newGroup in groups)
                    {
                        DispatchGroup oldGroup = this.Groups.Where(g => g.Name.Equals(newGroup.Name)).FirstOrDefault();
                        if (oldGroup != null)
                        {
                            newGroup.JobGuids.UnionWith(oldGroup.JobGuids);
                        }
                    }

                    (this.Groups as List<DispatchGroup>).Clear();                    
                    this.Groups = groups;
                }
            }
            catch (Exception ex)
            {
                this.Log.ExceptionCtx(LOG_CTX, ex, "Exception loading job management data from {0}", filename);
                return false;
            }

            return true;
        }

        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Sync to the head of the dispatch file if it's not open for edit.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="options"></param>
        private void SyncDispatchFile(String filename, CommandOptions options)
        {
            using (P4 p4 = options.Project.SCMConnect())
            {
                FileMapping fm = FileMapping.Create(p4, filename).FirstOrDefault();
                FileState filestate = FileState.Create(p4, new String[] { fm.DepotFilename }).FirstOrDefault();
                if (filestate.OpenAction != FileAction.Edit)
                {
                    if (filestate.HaveRevision != filestate.HeadRevision)
                    {
                        this.Log.MessageCtx(LOG_CTX, "Syncing Dispatch File as we are not on the head revision ({0}/{1}) : {2}", filestate.HaveRevision, filestate.HeadRevision, fm.DepotFilename);
                        P4API.P4RecordSet recordSet = p4.Run("sync", fm.DepotFilename);
                        p4.ParseAndLogRecordSetForDepotFilename("sync", recordSet);
                        p4.LogP4RecordSetMessages(recordSet);
                    }
                }
                else
                {
                    this.Log.WarningCtx(LOG_CTX, "Dispatch file is opened for edit - cannot sync : {0}", filename);
                }
            }
        }
        #endregion // Private Methods

        #region Private Static Methods
        /// <summary>
        /// Converts item type string to item type.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        ItemType ConvertItemStringToItemType(String item)
        {
            if (item.Equals("path"))
                return ItemType.Path;
            else if (item.Equals("solution"))
                return ItemType.Solution;
            else if (item.Equals("script_project"))
                return ItemType.ScriptProject;
            else if (item.Equals("regex"))
                return ItemType.RegEx;
            else
                return ItemType.Default;
        }

        /// <summary>
        /// Converts priority string to priority type.
        /// </summary>
        /// <param name="priority"></param>
        /// <returns></returns>
        Priority ConvertPriorityStringToPriority(String priority)
        {
            switch (priority)
            {
                case "high":
                    return Priority.High;
                case "standard":
                    return Priority.Standard;
                case "low":
                    return Priority.Low;
                default:
                    return Priority.Standard;
            }
        }

        /// <summary>
        /// Create new items based on the groups item type
        /// - may return more than one if DLC projects are in use.
        /// </summary>
        /// <param name="itemType"></param>
        /// <returns></returns>
        IEnumerable<IItem> CreateItems(XElement itemElement, P4 p4)
        {
            IItem item;
            Priority itemPriority = Priority.Standard;
            if (itemElement.Attribute("priority") != null)
                itemPriority = ConvertPriorityStringToPriority(itemElement.Attribute("priority").Value);

            // Split pre/postbuild commands - this is an interim measure until we support xelements
            IEnumerable<String> prebuildCommands = new List<String>();
            IEnumerable<String> postbuildCommands = new List<String>();

            if (itemElement.Attribute("prebuild_commands") != null)
            {
                String allPrebuildCommands = itemElement.Attribute("prebuild_commands").Value;
                allPrebuildCommands = Environment.ExpandEnvironmentVariables(this.CommandOptions.Branch.Environment.Subst(allPrebuildCommands).Replace("/", "\\"));
                prebuildCommands = allPrebuildCommands.Split(new String[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => p.Trim());
            }

            if (itemElement.Attribute("postbuild_commands") != null)
            {
                String allPostbuildCommands = itemElement.Attribute("postbuild_commands").Value;
                allPostbuildCommands = Environment.ExpandEnvironmentVariables(this.CommandOptions.Branch.Environment.Subst(allPostbuildCommands).Replace("/", "\\"));
                postbuildCommands = allPostbuildCommands.Split(new String[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(p => p.Trim());
            }

            ItemType itemType = ConvertItemStringToItemType(itemElement.Attribute("type").Value);

            ICollection<IItem> items = new List<IItem>();

            switch (itemType)
            {
                case ItemType.Solution:
                    {
                        String solutionPath = itemElement.Attribute("solution_filename").Value;
                        solutionPath = this.CommandOptions.Branch.Environment.Subst(solutionPath).Replace("/", "\\");
                        if (!File.Exists(solutionPath))
                            Log.ToolErrorCtx(LOG_CTX, "Solution Filename {0} does not exist", solutionPath);

                        String targetDir = itemElement.Attribute("target_dir").Value;
                        targetDir = this.CommandOptions.Branch.Environment.Subst(targetDir).Replace("/", "\\");
                        if (!String.IsNullOrEmpty(targetDir) && !Directory.Exists(targetDir))
                            Log.ToolErrorCtx(LOG_CTX, "Target Directory {0} does not exist", targetDir);

                        String publishDir = itemElement.Attribute("publish_dir").Value;
                        publishDir = this.CommandOptions.Branch.Environment.Subst(publishDir).Replace("/", "\\");


                        String platform = itemElement.Attribute("platform").Value;
                        String buildConfig = itemElement.Attribute("build_config").Value;
                        String tool = itemElement.Attribute("tool").Value.ToLower();
                        bool rebuild = bool.Parse(itemElement.Attribute("rebuild").Value);
                        int skipConsume = itemElement.Attribute("skip_consume") != null ? int.Parse(itemElement.Attribute("skip_consume").Value) : -1;
                        bool requiresPostJobNotification = itemElement.Attribute("post_job_notify") != null ? bool.Parse(itemElement.Attribute("post_job_notify").Value) : true;
                        String labelsCsv = itemElement.Attribute("labels") != null ? itemElement.Attribute("labels").Value : String.Empty;
                        
                        // Now support csv - expand the csvs for buildconfig and platform and create mutliple solution items.
                        IEnumerable<String> platforms = platform.Split(',').Select(p => p.Trim());
                        IEnumerable<String> buildConfigs = buildConfig.Split(',').Select(p => p.Trim());
                        IEnumerable<String> labels = labelsCsv.Split(',').Select(p => p.Trim());                        

                        foreach (String p in platforms.Where(p => !String.IsNullOrEmpty(p)))
                        {
#warning DW: TODO : not likely but could consider if it's worth filtering the platforms from those selected at tools install - the only show stopper with that is that we can't build for oldgen on dev_ng for example.
                            foreach (String bc in buildConfigs.Where(bc => !String.IsNullOrEmpty(bc)))
                            {
                                item = new SolutionItem(solutionPath, targetDir, publishDir, itemPriority, p, bc, tool, rebuild, prebuildCommands, postbuildCommands, labels, skipConsume, requiresPostJobNotification);
                                items.Add(item);
                            }
                        }
                    }
                    break;
                case ItemType.ScriptProject:
                    {
                        String scriptProjectPath = itemElement.Attribute("script_project_filename").Value;
                        scriptProjectPath = Environment.ExpandEnvironmentVariables(this.CommandOptions.Branch.Environment.Subst(scriptProjectPath).Replace("/", "\\"));
                        if (!File.Exists(scriptProjectPath))
                            Log.WarningCtx(LOG_CTX, "Script Project Filename {0} does not exist", scriptProjectPath);

                        String buildConfig = itemElement.Attribute("build_config").Value;
                        String tool = itemElement.Attribute("tool").Value.ToLower();
                        String alias = itemElement.Attribute("alias") != null ? itemElement.Attribute("alias").Value.ToLower() : null;
                        bool rebuild = bool.Parse(itemElement.Attribute("rebuild").Value);
                        int skipConsume = itemElement.Attribute("skip_consume") != null ? int.Parse(itemElement.Attribute("skip_consume").Value) : -1;
                        bool requiresPostJobNotification = itemElement.Attribute("post_job_notify") != null ? bool.Parse(itemElement.Attribute("post_job_notify").Value) : true;
                        bool monitoringPaths = itemElement.Attribute("monitor_paths") != null ? bool.Parse(itemElement.Attribute("monitor_paths").Value) : true;
                        String labelsCsv = itemElement.Attribute("labels") != null ? itemElement.Attribute("labels").Value : String.Empty;
                        IEnumerable<String> labels = labelsCsv.Split(',').Select(p => p.Trim());                              

                        // Now support csv - expand the csvs for buildconfig and platform and create mutliple solution items.
                        IEnumerable<String> buildConfigs = buildConfig.Split(',').Select(p => p.Trim());

                        foreach (String bc in buildConfigs.Where(bc => !String.IsNullOrEmpty(bc)))
                        {
                            item = new ScriptProjectItem(scriptProjectPath, itemPriority, bc, tool, rebuild, prebuildCommands, postbuildCommands, labels, skipConsume, requiresPostJobNotification, monitoringPaths, alias);
                            items.Add(item);
                        }
                    }
                    break;
                case ItemType.Path:
                    {                        
                        String origPath = itemElement.Attribute("value").Value;
                        String path = this.CommandOptions.Branch.Environment.Subst(origPath).Replace("/", "\\");
                        ICollection<String> pathsToAdd = new List<String>() { path };

                        String branchName = this.CommandOptions.Branch.Name;

                        // DLC paths are also added for any DLC projects we are aware of.
                        foreach (KeyValuePair<string, IProject> kvp in this.Config.DLCProjects)
                        {
                            string dlcProjectName = kvp.Key;
                            IProject dlcProject = kvp.Value;

                            if (dlcProject.Branches.ContainsKey(branchName))
                            {
                                String dlcPath = dlcProject.Branches[branchName].Environment.Subst(origPath).Replace("/", "\\");
                                pathsToAdd.Add(dlcPath);
                            }
                            // LA: Commented out as was causing spam in the console on asset builders
                            /*else
                            {
                                Log.ToolErrorCtx(LOG_CTX, "DLC {1} branch {0} does not exist", branchName, dlcProjectName);
                            }*/
                        }

                        // Now add all the items passing in the local & depot paths so that both can be matched on.
                        // this is because there can legitmately be local paths & depot paths that are used in triggers.
                        if (pathsToAdd.Any())
                        {
                            IEnumerable<FileMapping> fileMappings = FileMapping.Create(p4, pathsToAdd.ToArray()).ToList();
                            foreach (FileMapping fm in fileMappings)
                            {
                                item = new PathItem(fm.LocalFilename, fm.DepotFilename, itemPriority);
                                items.Add(item);
                            }
                        }
                    }
                    break;
                case ItemType.RegEx:
                    {
                        String origPath = itemElement.Attribute("value").Value;
                        String path = this.CommandOptions.Branch.Environment.Subst(AsciiExpansion.Expand(origPath)).Replace("/", "\\");
                        ICollection<String> pathsToAdd = new List<String>() { path };

                        String branchName = this.CommandOptions.Branch.Name;

                        // DLC paths are also added for any DLC projects we are aware of.
                        foreach (KeyValuePair<string, IProject> kvp in this.Config.DLCProjects)
                        {
                            string dlcProjectName = kvp.Key;
                            IProject dlcProject = kvp.Value;

                            if (dlcProject.Branches.ContainsKey(branchName))
                            {
                                String dlcPath = dlcProject.Branches[branchName].Environment.Subst(origPath).Replace("/", "\\");
                                pathsToAdd.Add(dlcPath);
                            }
                        }

                        if (pathsToAdd.Any())
                        {
                            IEnumerable<FileMapping> fileMappings = FileMapping.Create(p4, pathsToAdd.ToArray()).ToList();
                            foreach (FileMapping fm in fileMappings)
                            {
                                item = new PathRegexItem(fm.LocalFilename, itemPriority);
                                items.Add(item);
                            }
                        }
                    }
                    break;
                default:
                    item = new Item(itemPriority, prebuildCommands, postbuildCommands);
                    items.Add(item);
                    break;
            }

            return items;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupElement"></param>
        /// <returns></returns>
        DispatchGroup CreateDispatchGroup(XElement groupElement, P4 p4)
        {
            String name = groupElement.Attribute("name").Value;
            String machine = groupElement.Attribute("machine").Value.ToLower();

            Priority groupPriority = ConvertPriorityStringToPriority(groupElement.Attribute("priority").Value);
            DispatchGroup dispatchGroup = new DispatchGroup(name, groupPriority, machine);
            foreach (XElement itemElement in groupElement.Descendants("item"))
            {
                IEnumerable<IItem> items = CreateItems(itemElement, p4);
                dispatchGroup.Items.AddRange(items);
            }

            foreach (XElement itemElement in groupElement.Descendants("jobguid"))
            {
                Guid guid;
                if (Guid.TryParse(itemElement.Value, out guid))
                {
                    dispatchGroup.JobGuids.Add(guid);
                }
            }            

            return dispatchGroup;
        }
        #endregion // Private Static Methods
    }
}
