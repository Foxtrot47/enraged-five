﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Triggers
{
    /// <summary>
    /// 'Build token' that can be expressed in descriptions of triggers.
    /// - build tokens can be arsed from the descriptions of triggers. 
    /// - build tokens can be acted upon within the job processor or task.
    /// </summary>    
    [DataContract]
    public enum BuildToken
    {
        /// <summary>
        /// Invalid build token.
        /// </summary>
        [EnumMember]
        INVALID = -1,

        /// <summary>
        /// This is for demo purposes only. 
        /// - this changes the logging context int he codebuilder2 job processor.
        /// </summary>
        [EnumMember]
        CODEBUILDER_CONTEXT,


        /// <summary>
        /// Used in the Face animation processor TASK to filter changelists.
        /// </summary>
        [EnumMember]
        FACE,

        /// <summary>
        /// Used in the frame capture system to indicate parent folder.
        /// </summary>
        [EnumMember]
        PARENT,

        /// <summary>
        /// Used in the frame capture system to indicate animation name.
        /// </summary>
        [EnumMember]
        ANIMATION,

        /// <summary>
        /// Used to indicate what project is used eg core or dlc
        /// </summary>
        [EnumMember]
        PROJECT,

        /// <summary>
        /// Used to indicate branch eg dev_ng
        /// </summary>
        [EnumMember]
        BRANCH,

        /// <summary>
        /// Used to submit to network exporter.
        /// </summary>
        [EnumMember]
        EXPORT,

        /// <summary>
        /// Used to indicate a position.
        /// </summary>
        [EnumMember]
        POSITION,

        /// <summary>
        /// Used to indicate a changelist.
        /// </summary>
        [EnumMember]
        CHANGELIST,
		
		/// <summary>
        /// Used in the frame capture system to indicate the resolution to record at
        /// </summary>
        [EnumMember]
        RESOLUTION,

        /// <summary>
        /// Used in the frame capture system to record the original fbx
        /// </summary>
        [EnumMember]
        SOURCEFILE,

        /// <summary>
        /// Used in the frame capture system to record the changelist of the original fbx
        /// </summary>
        [EnumMember]
        SOURCECHANGELIST,

        /// <summary>
        /// Used in the frame capture system to record the revision of the original fbx
        /// </summary>
        [EnumMember]
        SOURCEREVISION,

        /// <summary>
        /// Used in the frame capture system to indicate if the cameras should be baked pre-export
        /// </summary>
        [EnumMember]
        BAKECAMERA,

        /// <summary>
        /// Used in the frame capture system to indicate the cutscene range to record
        /// </summary>
        [EnumMember]
        RANGE,

        /// <summary>
        /// Used in the frame capture system to indicate if lighting should be enabled (incompatible with RANGE)
        /// </summary>
        [EnumMember]
        LIGHTING,
    }
}
