using RSG.Base.IO;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.SourceControl.Perforce.Util;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.JobManagement
{
    /// <summary>
    /// Prioritised group of job management items.  Can hold explicit reference to a machine via MachineName.
    /// </summary>
    public sealed class DispatchGroup
    {
        #region Constructors
        /// <summary>
        /// JobGroup constructor
        /// </summary>
        public DispatchGroup(String name, Priority priority, String machinename)
        {
            this.Name = name;
            this.Priority = priority;
            this.MachineName = machinename;
            this.Items = new List<IItem>();
            this.JobGuids = new HashSet<Guid>();
        }
        #endregion // Constructors

        #region Properties
        /// <summary>
        /// List of job management items.
        /// </summary>
        public ICollection<IItem> Items { get; private set; }

        /// <summary>
        /// List of job guid that are directly assigned to this group.
        /// </summary>
        public ISet<Guid> JobGuids { get; private set; }        

        /// <summary>
        /// Name of the group.
        /// </summary>
        public String Name { get; private set; }

        /// <summary>
        /// Group priority used to dispatch according to machine availability.
        /// </summary>
        public Priority Priority { get; set; }

        /// <summary>
        /// Name of machine associated with group.  If set, this takes precedence over Priority.
        /// </summary>
        public String MachineName { get; private set; }
        #endregion // Properties

        #region Controller Methods
        /// <summary>
        /// Determines if any list of files is handled by job management path items in the group.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="files"></param>
        /// <returns></returns>
        public bool ContainsAnyPath(IUniversalLog log, IEnumerable<String> files)
        {
            IEnumerable<PathItem> pathItems = this.Items.OfType<PathItem>();
            IEnumerable<PathRegexItem> regExItems = this.Items.OfType<PathRegexItem>();

            IEnumerable<String> depotPaths = pathItems.Select(pi => pi.DepotPath);
            foreach (String depotPath in depotPaths)
            {
                foreach (String file in files)
                {
                    if (file.StartsWith(depotPath, StringComparison.CurrentCultureIgnoreCase))
                    {
                        //log.Debug("file {0} starts with {1}", file, depotPath);
                        return true;
                    }
                    //log.Debug("file {0} does not start with {1}", file, depotPath);
                }
            }

            IEnumerable<String> localPaths = pathItems.Select(pi => pi.LocalPath);
            foreach (String localPath in localPaths)
            {
                String fullLocalPath = Path.GetFullPath(localPath);
                foreach (String file in files)
                {
                    String fullFilename = Path.GetFullPath(file);
                    if (file.StartsWith(fullLocalPath, StringComparison.CurrentCultureIgnoreCase))
                    {
                        //log.Debug("file {0} starts with {1}", file, fullLocalPath);
                        return true;
                    }

                    //log.Debug("file {0} does not start with {1}", file, fullLocalPath);
                }
            }

            IEnumerable<String> regExPaths = regExItems.Select(pi => pi.RegexPath);
            foreach (String regExPath in regExPaths)
            {
                foreach (String file in files)
                {
                    String fullFilename = Path.GetFullPath(file);

                    Wildcard wildcard = new Wildcard(regExPath);
                    if (wildcard.Match(fullFilename).Success)
                    {
                        //log.Debug("file {0} matches regex with {1}", file, regExPath);
                        return true;
                    }

                    //log.Debug("file {0} does not match regex with {1}", file, regExPath);
                }
            }

            return false;
        }

        /// <summary>
        /// Is the job contained by the dispatch group.
        /// </summary>
        /// <param name="job"></param>
        /// <returns>true if the job is contained in this dispatch group</returns>
        public bool ContainsJob(IJob job)
        {
            foreach (Guid guid in this.JobGuids)
            {
                if (job.ID == guid)
                {
                    return true;
                }

                if (job.ConsumedByJobID == guid)
                {
                    // TODO : handle these somehow? - might be hard.
                }
            }
            return false;
        }

        #endregion Controller Methods
    }
}
