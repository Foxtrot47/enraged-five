﻿using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Bug
{
    /// <summary>
    /// Bug creation details.
    /// </summary>
    public interface IBugRecipient
    {
        #region Properties
        /// <summary>
        /// Bug Owner
        /// </summary>
        String Owner { get; }
        /// <summary>
        /// Bug QA
        /// </summary>
        String QA { get; }
        /// <summary>
        /// Bug Reviewer
        /// </summary>
        String Reviewer { get; }
        /// <summary>
        /// List of users to cc on bug
        /// </summary>
        ICollection<String> CcList { get; }
        /// <summary>
        /// Bug Component
        /// </summary>
        String Component { get; }
        /// <summary>
        /// Bug Tag
        /// </summary>
        String Tag { get; }
        /// <summary>
        /// Bug Class
        /// </summary>
        String Class { get; }
        /// <summary>
        /// Bug Priority
        /// </summary>
        String Priority { get; }
        /// <summary>
        /// Rule to determine whether to create a bug.
        /// </summary>
        BugRule Rule { get; }
        /// <summary>
        /// Estimated Time in minutes.
        /// </summary>
        float EstimatedTime { get; }
        #endregion // properties

        #region Methods
        /// <summary>
        /// Is a bug required?
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        bool BugRequired(IJob job);
        #endregion // Methods
    }
}
