﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace RSG.Pipeline.Automation.Common.Jobs
{

    /// <summary>
    /// 
    /// </summary>
    [DataContract(Namespace = "")]
    [KnownType(typeof(ChangelistJobStatus))]
    public class JobStatus
    {
        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public JobState State { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public DateTime ProcessedAt { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public DateTime FinishedAt { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public Guid AssignedClient { get; set; }
        

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="job"></param>
        public JobStatus(IJob job)
        {
            this.State = job.State;
            this.AssignedClient = job.Client;
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Automation.Common.Jobs
