﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Triggers
{
    /// <summary>
    /// Description of a trigger.
    /// </summary>
    public interface IHasDescription
    {
        #region Properties
        /// <summary>
        /// Description of the trigger.
        /// </summary>
        String Description { get; }
        #endregion // Properties
    }
}