﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.Pipeline.Services;

namespace RSG.Pipeline.Automation.Common.Notifications
{
    /// <summary>
    /// Email notification interface.
    /// </summary>
    public interface IEmailNotification
    {
        #region Properties
        /// <summary>
        /// Email notification subject.
        /// </summary>
        String Subject { get; }

        /// <summary>
        /// Email content that should be at the top of the email.
        /// </summary>
        StringBuilder Header { get; }

        /// <summary>
        /// Email content in the body of the email
        /// </summary>
        StringBuilder Body { get; }

        /// <summary>
        /// Email content to appear at the end of the email.
        /// </summary>
        StringBuilder Footer  { get; }

        /// <summary>
        /// Configuration object.
        /// </summary>
        IConfig Config  { get; }

        /// <summary>
        /// Command Options
        /// </summary>
        CommandOptions Options { get; }

        #endregion // Properties

        #region Controller Methods
        /// <summary>
        /// Method for taking care of email dispatch to exchange.
        /// </summary>
        /// <param name="notificationRecipients"></param>
        /// <returns></returns>
        bool Send(IEnumerable<IEmailRecipient> notificationRecipients, IDictionary<string, EmailRedirect> emailRedirects);
        #endregion // Controller Methods
    }
}
