﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Triggers
{
    /// <summary>
    /// A trigger that was user triggered
    /// </summary>
    public interface IUserRequestTrigger :
        IHasDescription,
        IHasUsername,
        IHasCommand
    {
        #region Properties
        // DW: Deliberately left empty - do not remove.
        #endregion // Properties
    }
}
