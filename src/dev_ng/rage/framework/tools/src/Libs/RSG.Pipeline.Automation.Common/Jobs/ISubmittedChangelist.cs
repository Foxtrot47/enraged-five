﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Jobs
{
    public interface ISubmittedChangelist
    {
        /// <summary>
        /// Submitted Changelist number
        /// </summary>
       int SubmittedChangelistNumber { get; }
    }
}
