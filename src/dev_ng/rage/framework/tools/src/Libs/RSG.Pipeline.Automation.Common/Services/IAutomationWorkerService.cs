﻿using System;
using System.ServiceModel;
using RSG.Pipeline.Automation.Common.Jobs;

namespace RSG.Pipeline.Automation.Common.Services
{

    /// <summary>
    /// Automation Worker Service.
    /// </summary>
    [ServiceContract]
    public interface IAutomationWorkerService
    {
        /// <summary>
        /// Request current client state.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [ServiceKnownType(typeof(Client.WorkerState))]
        Client.WorkerState GetState();

        /// <summary>
        /// Request to process a particular job.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        [OperationContract]
        [ServiceKnownType(typeof(Job))]
        [ServiceKnownType(typeof(MapExportJob))]
        [ServiceKnownType(typeof(AssetBuilderJob))]
        [ServiceKnownType(typeof(CodeBuilderJob))]
        [ServiceKnownType(typeof(IntegratorJob))]
        [ServiceKnownType(typeof(CodeBuilder2Job))]
        [ServiceKnownType(typeof(ScriptBuilderJob))]
        bool Process(IJob job); 

        /// <summary>
        /// Request to abort particular job.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        [OperationContract]
        [ServiceKnownType(typeof(Job))]
        [ServiceKnownType(typeof(MapExportJob))]
        [ServiceKnownType(typeof(AssetBuilderJob))]
        [ServiceKnownType(typeof(CodeBuilderJob))]
        [ServiceKnownType(typeof(IntegratorJob))]
        [ServiceKnownType(typeof(CodeBuilder2Job))]
        [ServiceKnownType(typeof(ScriptBuilderJob))]
        bool Abort(IJob job);
    }

} // RSG.Pipeline.Automation.Common.Services namespace
