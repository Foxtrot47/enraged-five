﻿using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Pipeline.Core;
using RSG.SourceControl.Perforce;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Automation.Common.Jobs
{
    [DataContract]
    public class CutSceneRPFBuilderJob :
        Job,
        IJob
    {

        #region Constants
        private static readonly String XML_ELEMENT_NAME_ENGINE_FLAGS = "EngineFlags";

        #endregion // Constants

        /// <summary>
        /// Engine flags 
        /// </summary>
        [DataMember]
        public EngineFlags EngineFlags { get; set; }

         #region Constructors

        /// <summary>
        /// Default constructor (for deserialisation).
        /// </summary>
        public CutSceneRPFBuilderJob()
            : base()
        {
            Role = CapabilityType.CutSceneRPFBuilder;
            EngineFlags = EngineFlags.Default;
        }

        /// <summary>
        /// Default constructor (for deserialisation).
        /// </summary>
        public CutSceneRPFBuilderJob(IBranch branch)
            : base(branch, CapabilityType.CutSceneRPFBuilder)
        {
            EngineFlags = EngineFlags.Default;
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CutSceneRPFBuilderJob(IBranch branch, XElement xmlJobElement)
            : base(branch, CapabilityType.CutSceneRPFBuilder)
        {
            this.EngineFlags = (EngineFlags)Enum.Parse(typeof(EngineFlags), xmlJobElement.Elements(XML_ELEMENT_NAME_ENGINE_FLAGS).First().Value);
        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        public CutSceneRPFBuilderJob(IBranch branch, Changelist change, IEnumerable<String> pathnamesToProcess, IEnumerable<String> monitoringPaths = null)
            : base(branch, CapabilityType.CutSceneRPFBuilder, change, pathnamesToProcess, monitoringPaths)
        {
            EngineFlags = EngineFlags.Default;
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// Serialise job representation to XML.
        /// </summary>
        /// <returns></returns>
        public override XElement Serialise()
        {
            XElement xmlJob = base.Serialise();
            xmlJob.Add(new XElement(XML_ELEMENT_NAME_ENGINE_FLAGS, this.EngineFlags));

            return (xmlJob);
        }

        /// <summary>
        /// Deserialise job representation from XML.
        /// </summary>
        /// <param name="xmlJobElem"></param>
        /// <returns></returns>
        public override bool Deserialise(XElement xmlJobElem)
        {
            bool result = true;
            try
            {
                result &= base.Deserialise(xmlJobElem);
                this.EngineFlags = (EngineFlags)Enum.Parse(typeof(EngineFlags), xmlJobElem.Elements(XML_ELEMENT_NAME_ENGINE_FLAGS).First().Value);
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Error deserialising Asset Builder Job.");
                result = false;
            }
            return (result);
        }
        #endregion // Controller Methods
    }
}
