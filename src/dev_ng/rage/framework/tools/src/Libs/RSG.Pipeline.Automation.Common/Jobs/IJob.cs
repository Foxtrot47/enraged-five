﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml.Linq;
using RSG.Pipeline.Automation.Common.Triggers;

namespace RSG.Pipeline.Automation.Common.Jobs
{

    /// <summary>
    /// Job interface; created on server, passed to and processed on clients.
    /// </summary>
    public interface IJob
    {
        #region Properties
        /// <summary>
        /// Job unique identifier.
        /// </summary>
        Guid ID { get; set; }

        /// <summary>
        /// Consuming Job ID ( Guid )
        /// </summary>
        Guid ConsumedByJobID { get; set; }

        /// <summary>
        /// Job state.
        /// </summary>
        JobState State { get; set; }

        /// <summary>
        /// Job priority.
        /// </summary>
        JobPriority Priority { get; set; }

        /// <summary>
        /// Job role; allows us to reuse Job classes for different processors.
        /// </summary>
        CapabilityType Role { get; set; }

        /// <summary>
        /// Trigger that invoked this job.
        /// </summary>
        ITrigger Trigger { get; set; }

        /// <summary>
        /// Job creation time.
        /// </summary>
        DateTime CreatedAt { get; set; }

        /// <summary>
        /// Job processing start time.
        /// </summary>
        DateTime ProcessedAt { get; set; }

        /// <summary>
        /// The hostname that processed or is processing the job.
        /// </summary>
        String ProcessingHost { get; set; }

        /// <summary>
        /// Branch the job is for
        /// </summary>
        String BranchName { get; set; }
        
        /// <summary>
        /// The Project the job is for
        /// </summary>
        String ProjectName { get; set; }

        /// <summary>
        /// Time when the job completed.
        /// </summary>
        DateTime CompletedAt { get; set; }

        /// <summary>
        /// Amount of time client took to process job.
        /// </summary>
        TimeSpan ProcessingTime { get; set; }

        /// <summary>
        /// User defined data shipped with the job.
        /// </summary>
        Object UserData { get; set; }

        /// <summary>
        /// Prebuild commands
        /// </summary>
        IEnumerable<String> PrebuildCommands { get; set; }

        /// <summary>
        /// Postbuild commands
        /// </summary>
        IEnumerable<String> PostbuildCommands { get; set; }

        /// <summary>
        /// Labels
        /// </summary>
        IEnumerable<String> Labels { get; set; }

        #endregion // Properties

        #region Methods
        /// <summary>
        /// Serialise job representation to XML.
        /// </summary>
        /// <returns></returns>
        XElement Serialise();

        /// <summary>
        /// Deserialise job representation from XML.
        /// </summary>
        /// <returns></returns>
        bool Deserialise(XElement xmlJobElem);

        /// <summary>
        /// The friendly name of this job.
        /// </summary>
        /// <returns></returns>
        String FriendlyName();
        #endregion // Methods
    }
    
    /// <summary>
    /// Comparer for two JobPriority values; for use in the PriorityQueue.
    /// </summary>
    public class JobPriorityComparer : IComparer<JobPriority>
    {
        /// <summary>
        /// Compare two job priorities.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public int Compare(JobPriority a, JobPriority b)
        {
            if (JobPriority.High == a)
                return (-1);
            if (JobPriority.High == b)
                return (1);

            // No default sort order; as its a priority queue.
            return (0);
        }
    }

} // RSG.Pipeline.Automation.Common.Jobs namespace
