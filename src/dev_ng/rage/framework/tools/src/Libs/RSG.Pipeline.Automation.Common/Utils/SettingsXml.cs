﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Services;
using RSG.Base.Logging;
using RSG.Base.Extensions;
using System.Globalization;

namespace RSG.Pipeline.Automation.Common.Utils
{
    /// <summary>
    /// Helper class for common xml operations on automation settings files.
    /// </summary>
    public static class SettingsXml
    {
        #region Public static methods
        /// <summary>
        /// Get xml nodes of a naming convention passed in, where the root node and element name are specified.
        /// - filters by the branch you are on.
        /// - permits unbranched style too so that we can transition to/from branched settings easily.
        /// - uses xpath so is case sensitive.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="log"></param>
        /// <param name="xdoc"></param>
        /// <param name="rootName">the name of the root node ( case sensitive )</param>
        /// <param name="elementName">the element name sought ( case sensitive )</param>
        /// <returns>enumerable of all the elements found</returns>
        public static IEnumerable<XElement> GetElements(IBranch branch, IUniversalLog log, XDocument xdoc, String rootName, String elementName)
        {
            String xpath = String.Format("/{0}/branches/branch[@name='{2}']/{1}", rootName, elementName, branch.Name);
            IEnumerable<XElement> elements = xdoc.XPathSelectElements(xpath);
            return elements;
        }

        /// <summary>
        /// helper for reading UTC datetimes from Xelement
        /// </summary>
        /// <param name="xElement"></param>
        /// <param name="dateTimeAsString"></param>
        /// <param name="fallback"></param>
        /// <returns></returns>
        public static DateTime DeserialiseDateTimeUtc(XElement xElement, DateTime fallback)
        {
            DateTime result = fallback.ToUniversalTime();
            if (null != xElement)
            {
                String dateTimeAsString = xElement.Value;

                // Exact Utc parsing! must be of the form 2014-06-07T19:46:22Z
                if (DateTime.TryParseExact(dateTimeAsString,
                                            "yyyy-MM-dd'T'HH:mm:ss'Z'",
                                            CultureInfo.InvariantCulture,
                                            DateTimeStyles.AssumeUniversal |
                                            DateTimeStyles.AdjustToUniversal, out result))
                {
                }
                else if (DateTime.TryParse(dateTimeAsString, out result))
                {
                    result = result.ToUniversalTime();
                }

                if (result.Kind != DateTimeKind.Utc)
                {
                    Log.Log__Warning("Timeleak! : Date Time {0} could not be parsed into a UTC DateTime object, it is falling back to {1}", dateTimeAsString, result);
                }
            }
            return result;
        }

        /// <summary>
        /// Helper method for ensuring UTC date times are being serialised.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static DateTime GetDateTimeForSerialisationUtc(DateTime source)
        {
            if (source.Kind != DateTimeKind.Utc)
            {
                DateTime result = source.ToUniversalTime();
                Log.Log__Warning("Potential non-UTC date time conversion in code (Timeleak!) Internally all our date time operations should be using UTC! : Date Time {0} is not UTC!... converting to UTC : {1}", source, result);
                return result;
            }
            return source;
        }

        #endregion // Public static methods
    }
}
