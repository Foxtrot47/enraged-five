﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RSG.SourceControl.Perforce;

namespace RSG.Pipeline.Automation.Common.Utils 
{
    /// <summary>
    /// Common functions for 3dsMax
    /// </summary>
    public static class CommonFuncs
    {
        #region Constants
        /// <summary>
        /// 
        /// </summary>
        public static readonly String _networkExportChangelistFilesZip = "MapNetworkExport_ChangelistFiles.zip";
        #endregion // Constants

        /// <summary>
        /// Takes a list of textures and figures out which of them are 1) Not in perforce 2) Marked for add or 3) Edited from perforce versions
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="allSceneTextures"></param>
        /// <returns></returns>
        public static List<String> GetTexturesRequiredToSend(P4API.P4Connection p4, List<String> allSceneTextures)
        {
            List<String> sourceTexturesToSend = new List<String>();

            if (allSceneTextures.Count > 0)
            {
                // Get the depot paths
                FileMapping[] fms = FileMapping.Create(p4, allSceneTextures.ToArray());
                IEnumerable<String> textureSourceDepotFilenames = fms.Select(fileMap => fileMap.DepotFilename).Distinct();

                // Sync the texture list, files checked out for edit won't be overwritten, and new files won't either
                // so this should be fine.  Also check for new textures which aren't in perforce yet 
                p4.Run("sync", textureSourceDepotFilenames.ToArray());

                // Find out what files we need to send, files checked out that have changes, which is what -sa returns
                List<String> args = new List<String>();
                args.Add("-sa");
                args.AddRange(textureSourceDepotFilenames);

                P4API.P4RecordSet diffRecordSet = p4.Run("diff", args.ToArray());
                foreach (P4API.P4Record record in diffRecordSet.Records)
                {
                    if (record.Fields.ContainsKey("clientFile"))
                        sourceTexturesToSend.Add(record["clientFile"]);
                }

                // There will be a debug assert during Filestate creation if there are files which are not marked for add which
                // are not in perforce.  This is fine in this case as it lets us know there are some textures to find that aren't 
                // in perforce and send them across too.
                FileState[] fss = FileState.Create(p4, textureSourceDepotFilenames.ToArray());
                if (fss.Count() != fms.Count())
                {
                    foreach (FileMapping fileMap in fms)
                    {
                        bool notMatched = true;
                        foreach (FileState fileState in fss)
                        {
                            // A bit ugly, but try and find the file mapping client filename in the file state array.
                            // If it's not found then we know it's a texture that's not even marked for add.
                            if (Path.GetFileNameWithoutExtension(fileMap.LocalFilename).ToLower().Equals(Path.GetFileNameWithoutExtension(fileState.ClientFilename).ToLower()))
                            {
                                notMatched = false;
                            }
                        }
                        if (notMatched)
                            sourceTexturesToSend.Add(fileMap.LocalFilename);
                    }
                }

                // If marked for add, we want to send it across to server
                for (int i = 0; i < fss.Count(); i++)
                {
                    if ((fss[i].OpenAction == FileAction.Add) && !sourceTexturesToSend.Contains(fss[i].ClientFilename))
                        sourceTexturesToSend.Add(fss[i].ClientFilename);
                }
            }
            return sourceTexturesToSend;
        }
    }
}
