﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RSG.Base.Configuration;
using RSG.Base.Collections;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Services;

namespace RSG.Pipeline.Automation.Common.Notifications
{
    /// <summary>
    /// DW: this doesn't make sense, a job notification IS an email notification?!
    /// needs reviewed.
    /// </summary>
    public abstract class JobNotificationBase
        :   EmailNotificationBase,
            IEmailNotification
    {
        #region Properties
        /// <summary>
        /// Jobs
        /// </summary>
        public IJob Job
        {
            get;
            private set;
        }

        /// <summary>
        /// Job results
        /// </summary>
        public IEnumerable<IJobResult> JobResults
        {
            get;
            private set;
        }

        /// <summary>
        /// Maximum number of files to list if job utilises a file trigger.
        /// </summary>
        public Int32 MaxFileList
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public Uri WorkerUri 
        { 
            get; 
            private set; 
        }

        /// <summary>
        /// 
        /// </summary>
        public Guid WorkerGuid 
        { 
            get; 
            private set; 
        }

        #endregion

        #region Contructors()
        /// <summary>
        /// 
        /// </summary>
        /// <param name="job"></param>
        public JobNotificationBase(IJob job, IEnumerable<IJob> associatedJobs, IEnumerable<IJobResult> jobResults, ILog log, Uri workerUri, Guid workerGuid, bool html, CommandOptions options, Int32 maxFileList = 50)
            : base(log, options, html, new HashSet<String>(associatedJobs.SelectMany(j => j.Trigger.Usernames())))
        {
            this.MaxFileList = maxFileList;
            this.Job = job;
            this.JobResults = jobResults;
            this.WorkerUri = workerUri;
            this.WorkerGuid = workerGuid;
        }
        #endregion

        #region Controller Methods
        /// <summary>
        /// Email subject builder.
        /// </summary>
        public virtual void SetSubject()
        {
            IConfig config = Options.Config;

            string prefix = String.Format("[{0} {1}] {2}: {3} : User: {4}", config.Project.FriendlyName, Options.Branch.Name, this.Job.Role, this.Job.State, this.Owner);

            IEnumerable<uint> changelistNumbers = Job.Trigger.ChangelistNumbers();
            IEnumerable<String> filesTriggering = Job.Trigger.TriggeringFiles();

            if (changelistNumbers.Any())
            {
                if (changelistNumbers.HasAtLeast(2))
                {
                    String changelistSummary = String.Format("({0}) {1} to {2}", changelistNumbers.Count(), changelistNumbers.Min(), changelistNumbers.Max());
                    this.Subject = String.Format("{0} Changes: ({1}) {2} to {3}", prefix, changelistNumbers.Count(), changelistNumbers.Min(), changelistNumbers.Max());
                }
                else
                {
                    this.Subject = String.Format("{0} Change: {1}", prefix, changelistNumbers.Last());
                }
            }
            else if (filesTriggering.Any())
            {
                // just grab the first file as requested (url:bugstar:1160828)
                string firstTriggerFilename = Path.GetFileName(filesTriggering.First());
                this.Subject = String.Format("{0} - {1} - Job: {2}", prefix, firstTriggerFilename, this.Job.ID);
            }
            else
            {
                this.Subject = String.Format("{0} Job: {1}", prefix, this.Job.ID);
            }
        }

        /// <summary>
        /// Email body header builder.
        /// </summary>
        public virtual void SetHeader()
        {
            this.Body.AppendLine(String.Format("Project + Branch: {0} {1}", Options.CoreProject.FriendlyName, Options.Branch.Name));

            IEnumerable<uint> changelistNumbers = Job.Trigger.ChangelistNumbers();
            IEnumerable<String> filesTriggering = Job.Trigger.TriggeringFiles();
            IEnumerable<String> usernames = Job.Trigger.Usernames();
            IEnumerable<String> descriptions = Job.Trigger.Descriptions();
            IEnumerable<DateTime> triggeredAtDateTimes = Job.Trigger.TriggeredAt();

            if (changelistNumbers.Any())
            {
                if (changelistNumbers.HasAtLeast(2))
                {
                    this.Body.AppendLine(String.Format("Changelists: {0}\n", String.Join(",", changelistNumbers)));
                }
                else
                {
                    this.Body.AppendLine(String.Format("Changelist: {0}\n", changelistNumbers.Last()));
                }
            }

            if (descriptions.Any())
            {
                if (descriptions.HasAtLeast(2))
                {
                    this.Body.AppendLine(String.Format("Last Description: {0}", descriptions.Last()));
                }
                else
                {
                    this.Body.AppendLine(String.Format("Description: {0}", descriptions.Last()));
                }
            }

            if (usernames.Any())
            {
                this.Owner = usernames.Last();
                if (usernames.HasAtLeast(2))
                {
                    this.Body.AppendLine(String.Format("Usernames: {0}\n", String.Join(",", usernames)));
                    this.Body.AppendLine(String.Format("Owner: {0}\n", this.Owner));
                }
                else
                {
                    this.Body.AppendLine(String.Format("Username: {0}\n", String.Join(",", usernames)));
                }
            }

            if (filesTriggering.Any())
            {
                this.Body.AppendLine(String.Format("Files:\n{0}", String.Join("\n", filesTriggering)));
            }

            if (triggeredAtDateTimes.Any())
            {
                if (triggeredAtDateTimes.HasAtLeast(2))
                {
                    this.Body.AppendLine(String.Format("Triggered between: {0} to {1}\n", triggeredAtDateTimes.First(), triggeredAtDateTimes.Last()));
                }
                else
                {
                    this.Body.AppendLine(String.Format("Triggered at: {0}\n", triggeredAtDateTimes.Last()));
                }
            }

            this.Body.AppendLine(String.Format("Job (GUID) : {0}", this.Job.ID));
            this.Body.AppendLine(String.Format("Processed By (GUID): {0}", this.WorkerGuid));
            this.Body.AppendLine(String.Format("Processed By (URI): {0}", this.WorkerUri));

            if (JobResults != null)
            {
                foreach (IJobResult jobResult in JobResults)
                {
                    this.Body.AppendLine(String.Format("Result: {0}", jobResult));
                }
            }
        }
       
        /// <summary>
        /// Email main body builder.
        /// </summary>
        public virtual void SetBody()
        {
            IEnumerable<String> filesTriggering = Job.Trigger.TriggeringFiles();
            if (filesTriggering.Any())
            {
                SetBodyWithTriggeringFiles(filesTriggering);
            }
        }

        // Common functionality pulled out.
        private void SetBodyWithTriggeringFiles(IEnumerable<String> filesTriggering)
        {
            this.Body.AppendLine(String.Format("\nFiles processed: {0}", filesTriggering.Count()));
            Int32 filecount = 0;
            foreach (String file in filesTriggering)
            {
                this.Body.AppendLine(file);
                filecount++;
                if (filecount == this.MaxFileList)
                {
                    this.Body.AppendLine(String.Format("Truncated after {0} files", this.MaxFileList));
                    break;
                }
            }
        }

        public virtual void SetBodyHtml()
        {
        }

        /// <summary>
        /// Email body footer builder.
        /// </summary>
        public virtual void SetFooter()
        {
            Footer.Append("\nCheck Workbench for a full log.  For information on viewing the log in Workbench, navigate to https://devstar.rockstargames.com/wiki/index.php/Automation_Monitor.");
        }
        #endregion // Controller Methods

        #region IEmailNotification Methods
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool Send(IEnumerable<IEmailRecipient> notificationRecipients, IDictionary<string, EmailRedirect> emailRediects)
        {
            this.SetHeader();
            this.SetFooter();
            this.SetSubject();
            if (Html)
            {
                this.SetBodyHtml();
            }
            else
            {
                this.SetBody();
            }

            base.Send(notificationRecipients, emailRediects);

            return (true);
        }
        #endregion // IEmailNotification
    }
}
