﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Pipeline.Core;
using RSG.SourceControl.Perforce;
using RSG.Base.Extensions;
using RSG.Pipeline.Automation.Common.JobManagement;
using System.Xml.Serialization;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Common.JobFactories;

namespace RSG.Pipeline.Automation.Common.Jobs
{
    /// <summary>
    /// Code builder 2 client side job.
    /// </summary>
    [DataContract]
    public class CodeBuilder2Job :
        Job,
        IJob
    {
        #region Constants
#warning DW: TODO : we should use memberinfo attributes to drive serialisation using reflection, then *perhaps* we would not require to override the serialisation.
        private static readonly String XML_ELEMENT_NAME_SOLUTION_FILENAME = "SolutionFilename";
        private static readonly String XML_ELEMENT_NAME_BUILD_CONFIG = "BuildConfig";
        private static readonly String XML_ELEMENT_NAME_PLATFORM = "Platform";
        private static readonly String XML_ELEMENT_NAME_TOOL = "Tool";
        private static readonly String XML_ELEMENT_NAME_REBUILD = "Rebuild";
        private static readonly String XML_ELEMENT_NAME_TARGET_DIR = "TargetDir";
        private static readonly String XML_ELEMENT_NAME_PUBLISH_DIR = "PublishDir";
        private static readonly String XML_ELEMENT_SKIP_CONSUME = "SkipConsume";
        private static readonly String XML_ELEMENT_REQUIRES_POST_JOB_NOTIFICATION = "RequiresPostJobNotification";            
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Solution Path
        /// </summary>
        [DataMember]
        public String SolutionFilename { get; private set; }

        /// <summary>
        /// Directory in which files are written as a result of running the codebuild. 
        /// - also known as Artefacts
        /// </summary>
        [DataMember]
        public String TargetDir { get; private set; }

        /// <summary>
        /// Directory in which files that were marked for upload are published ( copied and optionally checked in )
        /// - files for upload are copied from the upload dir to the publish dir
        /// - if the publish dir is mapped in p4 the files are submitted.
        /// - publish dir can be the same as the upload dir.
        /// </summary>
        [DataMember]
        public String PublishDir { get; private set; }

        /// <summary>
        /// Name of the Build Config.
        /// </summary>
        [DataMember]
        public String BuildConfig { get; private set; }

        /// <summary>
        /// Name of the Platform.
        /// </summary>
        [DataMember]
        public String Platform { get; private set; }

        /// <summary>
        /// Build Tool to use (Visual Studio, VSI, Incredibuild)
        /// </summary>
        [DataMember]
        public String Tool { get; private set; }

        /// <summary>
        /// Rebuild flag
        /// </summary>
        [DataMember]
        public bool Rebuild { get; private set; }

        /// <summary>
        /// Skip consume
        /// - when jobs are created this was the skip setting it was created with.
        /// - this may change when jobs are combined.
        /// </summary>
        [DataMember]
        public int SkipConsume { get; private set; }

        /// <summary>
        /// Does this job require post job notification?
        /// </summary>
        [DataMember]
        public bool RequiresPostJobNotification { get; private set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Default constructor (for WCF deserialisation only).
        /// </summary>
        private CodeBuilder2Job()
            : base()
        {
            Role = CapabilityType.CodeBuilder2;
        }

        /// <summary>
        /// Default constructor.
        /// - private use factory to create
        /// </summary>
        private CodeBuilder2Job(IBranch branch, XElement xmlJobElement)
            : base(branch, CapabilityType.CodeBuilder2)
        {
            base.Deserialise(xmlJobElement);
            SolutionFilename = Environment.ExpandEnvironmentVariables(xmlJobElement.Elements(XML_ELEMENT_NAME_SOLUTION_FILENAME).First().Value);
            TargetDir = Environment.ExpandEnvironmentVariables(xmlJobElement.Elements(XML_ELEMENT_NAME_TARGET_DIR).First().Value);
            PublishDir = Environment.ExpandEnvironmentVariables(xmlJobElement.Elements(XML_ELEMENT_NAME_PUBLISH_DIR).First().Value);
            BuildConfig = xmlJobElement.Elements(XML_ELEMENT_NAME_BUILD_CONFIG).First().Value;
            Platform = xmlJobElement.Elements(XML_ELEMENT_NAME_PLATFORM).First().Value;
            Tool = xmlJobElement.Elements(XML_ELEMENT_NAME_TOOL).First().Value;
            Rebuild = bool.Parse(xmlJobElement.Elements(XML_ELEMENT_NAME_REBUILD).First().Value);
            SkipConsume = int.Parse(xmlJobElement.Elements(XML_ELEMENT_SKIP_CONSUME).First().Value);
            RequiresPostJobNotification = bool.Parse(xmlJobElement.Elements(XML_ELEMENT_REQUIRES_POST_JOB_NOTIFICATION).First().Value);
        }

        /// <summary>
        /// Parameterized constructor.
        /// - private use factory to create
        /// </summary>
        private CodeBuilder2Job(Changelist change, IEnumerable<String> pathnamesToProcess, SolutionItem solutionItem, IBranch branch, IEnumerable<String> monitoringPaths = null, IEnumerable<String> prebuildCommands = null, IEnumerable<String> postbuildCommands = null, IEnumerable<String> labels = null, Guid triggerId = default(Guid))
            : base(branch, CapabilityType.CodeBuilder2, change, pathnamesToProcess, monitoringPaths, prebuildCommands, postbuildCommands, labels, triggerId)
        {
            SolutionFilename = Environment.ExpandEnvironmentVariables(solutionItem.SolutionFilename);
            TargetDir = Environment.ExpandEnvironmentVariables(solutionItem.TargetDir);
            PublishDir = Environment.ExpandEnvironmentVariables(solutionItem.PublishDir);
            BuildConfig = solutionItem.BuildConfig;
            Platform = solutionItem.Platform;
            Tool = solutionItem.Tool;
            Rebuild = solutionItem.Rebuild;
            SkipConsume = solutionItem.SkipConsume;
            RequiresPostJobNotification = solutionItem.RequiresPostJobNotification;
        }

        /// <summary>
        /// Parameterized constructor - multi trigger.
        /// - private use factory to create
        /// </summary>
        private CodeBuilder2Job(IEnumerable<ITrigger> triggers, SolutionItem solutionItem, IBranch branch, Guid triggerId, DateTime triggeredAt)
            : base(branch, CapabilityType.CodeBuilder2, triggers, solutionItem.PrebuildCommands, solutionItem.PostbuildCommands, solutionItem.Labels, triggerId, triggeredAt)
        {
            SolutionFilename = Environment.ExpandEnvironmentVariables(solutionItem.SolutionFilename);
            TargetDir = Environment.ExpandEnvironmentVariables(solutionItem.TargetDir);
            PublishDir = Environment.ExpandEnvironmentVariables(solutionItem.PublishDir);
            BuildConfig = solutionItem.BuildConfig;
            Platform = solutionItem.Platform;
            Tool = solutionItem.Tool;
            Rebuild = solutionItem.Rebuild;
            SkipConsume = solutionItem.SkipConsume;
            RequiresPostJobNotification = solutionItem.RequiresPostJobNotification;
        }

        /// <summary>
        /// Used to construct the object from outside. Example of this could be database objects.
        /// </summary>
        /// <param name="capabilityType"></param>
        /// <param name="completedAt"></param>
        /// <param name="consumedBy"></param>
        /// <param name="createdAt"></param>
        /// <param name="id"></param>
        /// <param name="postbuildCommands"></param>
        /// <param name="prebuildCommands"></param>
        /// <param name="jobPriority"></param>
        /// <param name="processedAt"></param>
        /// <param name="processingHost"></param>
        /// <param name="processingTime"></param>
        /// <param name="jobState"></param>
        /// <param name="trigger"></param>
        /// <param name="userData"></param>
        /// <param name="solutionFilename"></param>
        /// <param name="targetDir"></param>
        /// <param name="publishDir"></param>
        /// <param name="buildConfig"></param>
        /// <param name="platform"></param>
        /// <param name="tool"></param>
        /// <param name="rebuild"></param>
        /// <param name="skipConsume"></param>
        /// <param name="requiresPostJobNotification"></param>
        public CodeBuilder2Job(CapabilityType capabilityType, DateTime completedAt, Guid consumedBy, DateTime createdAt, Guid id, IEnumerable<string> postbuildCommands,
                    IEnumerable<string> prebuildCommands, JobPriority jobPriority, DateTime processedAt, string processingHost, TimeSpan processingTime,
                    JobState jobState, ITrigger trigger, object userData, String solutionFilename, String targetDir, String publishDir, String buildConfig, String platform,
                    String tool, bool rebuild, int skipConsume, bool requiresPostJobNotification)
            : base(capabilityType, completedAt, consumedBy, createdAt, id, postbuildCommands, prebuildCommands, jobPriority, processedAt, processingHost, 
                    processingTime, jobState, trigger, userData)
        {
            SolutionFilename = solutionFilename;
            TargetDir = targetDir;
            PublishDir = publishDir;
            BuildConfig = buildConfig;
            Platform = platform;
            Tool = tool;
            Rebuild = rebuild;
            SkipConsume = skipConsume;
            RequiresPostJobNotification = requiresPostJobNotification;
        }

        #endregion // Constructors

 		#region Public Static Methods
        /// <summary>
        /// Returns the Factory we use to create these jobs.
        /// </summary>
        /// <returns>The Type of the factory used.</returns>
        public static Type GetFactory()
        {
            return typeof(CodeBuilder2JobFactory);
        }
        #endregion // Public Static Methods

        #region Controller Methods
        /// <summary>
        /// Serialise job representation to XML.
        /// </summary>
        /// <returns></returns>
        public override XElement Serialise()
        {
            XElement xmlJob = base.Serialise();
            xmlJob.Add(new XElement(XML_ELEMENT_NAME_SOLUTION_FILENAME, SolutionFilename));
            xmlJob.Add(new XElement(XML_ELEMENT_NAME_TARGET_DIR, TargetDir));
            xmlJob.Add(new XElement(XML_ELEMENT_NAME_PUBLISH_DIR, PublishDir));
            xmlJob.Add(new XElement(XML_ELEMENT_NAME_BUILD_CONFIG, BuildConfig));
            xmlJob.Add(new XElement(XML_ELEMENT_NAME_PLATFORM, Platform));
            xmlJob.Add(new XElement(XML_ELEMENT_NAME_TOOL, Tool));
            xmlJob.Add(new XElement(XML_ELEMENT_NAME_REBUILD, Rebuild.ToString()));
            xmlJob.Add(new XElement(XML_ELEMENT_SKIP_CONSUME, SkipConsume));
            xmlJob.Add(new XElement(XML_ELEMENT_REQUIRES_POST_JOB_NOTIFICATION, RequiresPostJobNotification));              
            return (xmlJob);
        }

        /// <summary>
        /// Deserialise job representation from XML.
        /// - this is typically called when constructing jobs from local.xml
        /// </summary>
        /// <param name="xmlJobElem"></param>
        /// <returns></returns>
        [Obsolete("Use constructor instead")]
        public override bool Deserialise(XElement xmlJobElem)
        {
            throw new NotSupportedException();
            return false;
        }

        /// <summary>
        /// The friendly description of this job.
        /// </summary>
        /// <returns></returns>
        public override String FriendlyName()
        {
            String friendlyName = String.Format("{0} : {1}|{2}", this.SolutionFilename, this.BuildConfig, this.Platform);
            return friendlyName;
        }

        #endregion // Controller Methods

        #region public methods
        /// <summary>
        /// Jobs don't normally change but here it is transformed when consumed by another job.
        /// </summary>
        /// <param name="jobConsuming">the job consuming this job</param>
        public void SetConsumed(IJob jobConsuming)
        {
            // Hold a 'guid reference' to the consuming job
            this.ConsumedByJobID = jobConsuming.ID;

            // Change state to no longer be pending but is now consumed.
            this.State = JobState.SkippedConsumed;

            // The skip setting of the job is set to an enum value that indicates it has now been consumed.
            this.SkipConsume = -2;
        }
        #endregion // public methods.

    }
}
