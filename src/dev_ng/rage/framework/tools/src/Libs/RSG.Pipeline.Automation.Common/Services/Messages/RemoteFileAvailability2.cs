﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace RSG.Pipeline.Automation.Common.Services.Messages
{

    /// <summary>
    /// Remote file availability object (version 2); for returning to 
    /// clients data about what remote files are available on the file 
    /// transfer service.
    /// </summary>
    /// This adds a file sizes data structure.
    /// 
    [MessageContract]
    public class RemoteFileAvailability2
    {
        #region Properties
        /// <summary>
        /// Number of files available for the specified job.
        /// </summary>
        [MessageBodyMember]
        public int FileCount { get; set; }

        /// <summary>
        /// Enumerable of filename strings.
        /// </summary>
        [MessageBodyMember]
        public IEnumerable<RemoteFileInfo> Files { get; set; }

        /// <summary>
        /// Job identifier.
        /// </summary>
        [MessageBodyMember]
        public Guid Job { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public RemoteFileAvailability2()
            : base()
        {
            this.FileCount = 0;
            this.Files = new RemoteFileInfo[0];
            this.Job = Guid.Empty;
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Automation.Common.Services.Messages namespace
