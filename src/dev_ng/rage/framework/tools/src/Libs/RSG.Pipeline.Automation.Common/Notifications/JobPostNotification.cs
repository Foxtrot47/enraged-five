﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RSG.Base.Collections;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Services;
using System.Text.RegularExpressions;

namespace RSG.Pipeline.Automation.Common.Notifications
{
    /// <summary>
    /// 
    /// </summary>
    public class JobPostNotification
        : JobNotificationBase
    {
        #region Constants

        private static readonly string TEMPLATE_FILENAME = "email-tmpl.html";
        private static readonly string ERROR_BODY_FILENAME = "error-body-tmpl.html";
        private static readonly string SUCCESS_BODY_FILENAME = "success-body-tmpl.html";

        private static readonly string DELIMITER = "!";
        private static readonly string ERROR_MSGS_TOKEN = "ERROR_MSGS";
        private static readonly string SUCCESS_MSGS_TOKEN = "SUCCESS_MSGS";
        private static readonly string PERSONAL_MSG_NAME_TOKEN = "PERSONAL_MSG_NAME";
        private static readonly string PERSONAL_MSG_TEXT_TOKEN = "PERSONAL_MSG_TEXT";
        private static readonly string ERROR_BODY_TOKEN = "ERROR_BODY";
        private static readonly string SUCCESS_BODY_TOKEN = "SUCCESS_BODY";
        private static readonly string PROCESSED_FILES_NUM_TOKEN = "PROCESSED_FILES_NUM";
        private static readonly string PROCESSED_FILES_LIST_TOKEN = "PROCESSED_FILES_LIST";
        private static readonly string PROCESSED_ERROR_COUNT_TOKEN = "PROCESSED_ERROR_COUNT";
        private static readonly string ERROR_LIST_TOKEN = "ERROR_LIST";
        private static readonly string ERROR_SUMMARY_TOKEN = "ERROR_SUMMARY";
        private static readonly string INSTRUCTIVE_MESSAGE_TOKEN = "INSTRUCTIVE_MESSAGE";
        private static readonly string COMMON_ERRORS_WIKI_TOKEN = "COMMON_ERRORS_WIKI";
        private static readonly string HEADER_HEIGHT_TOKEN = "HEADER_HEIGHT";
        private static readonly string HEADER_FONT_SIZE_TOKEN = "HEADER_FONT_SIZE";
        private static readonly string HEADER_LINE_HEIGHT_TOKEN = "HEADER_LINE_HEIGHT";

        private static readonly string SUCCESS_MSG_TOKEN = "SUCCESS_MSG";

        private static readonly string[] HTML_TOKENS = {    ERROR_BODY_TOKEN,
                                                                SUCCESS_BODY_TOKEN,
                                                                ERROR_MSGS_TOKEN, 
                                                                SUCCESS_MSGS_TOKEN, 
                                                                PERSONAL_MSG_NAME_TOKEN, 
                                                                PERSONAL_MSG_TEXT_TOKEN, 
                                                                PROCESSED_FILES_NUM_TOKEN, 
                                                                PROCESSED_FILES_LIST_TOKEN, 
                                                                PROCESSED_ERROR_COUNT_TOKEN, 
                                                                ERROR_LIST_TOKEN, 
                                                                ERROR_SUMMARY_TOKEN, 
                                                                SUCCESS_MSG_TOKEN,
                                                                INSTRUCTIVE_MESSAGE_TOKEN, 
                                                                COMMON_ERRORS_WIKI_TOKEN,
                                                                HEADER_HEIGHT_TOKEN,
                                                                HEADER_FONT_SIZE_TOKEN,
                                                                HEADER_LINE_HEIGHT_TOKEN};                           
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public Int32 MaxErrors
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public CommandOptions Options
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        public IEnumerable<uint> BugIdsCreated
        {
            get;
            private set;
        }

        public String BranchNameOverride
        {
            get;
            private set;
        }

        public String JobNameOverride
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// 
        /// </summary>
        /// <param name="job"></param>
        /// <param name="maxErrors"></param>
        public JobPostNotification(IJob job, IEnumerable<IJob> associatedJobs, IEnumerable<IJobResult> jobResults, ILog log, Uri workerUri, Guid workerGuid, CommandOptions options, IEnumerable<uint> bugIdsCreated, bool html, Int32 maxErrors = 20, String branchNameOverride = "", String jobNameOverride = "")
            : base(job, associatedJobs, jobResults, log, workerUri, workerGuid, html, options)
        {
            this.MaxErrors = maxErrors;
            this.Options = options;
            this.BugIdsCreated = bugIdsCreated;
            this.BranchNameOverride = branchNameOverride;
            this.JobNameOverride = jobNameOverride;
        }
        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// Email subject builder.
        /// </summary>
        public override void SetSubject()
        {
            IConfig config = Options.Config;

            string prefix = String.Format("[{0} {1}] {2}: {3} : User: {4}", config.Project.FriendlyName, String.IsNullOrEmpty(this.BranchNameOverride) ? Options.Branch.Name : this.BranchNameOverride, String.IsNullOrEmpty(this.JobNameOverride) ? this.Job.Role.ToString() : this.JobNameOverride, this.Job.State, this.Owner);

            IEnumerable<uint> changelistNumbers = Job.Trigger.ChangelistNumbers();
            IEnumerable<String> filesTriggering = Job.Trigger.TriggeringFiles();

            if (changelistNumbers.Any())
            {
                if (changelistNumbers.HasAtLeast(2))
                {
                    String changelistSummary = String.Format("({0}) {1} to {2}", changelistNumbers.Count(), changelistNumbers.Min(), changelistNumbers.Max());
                    this.Subject = String.Format("{0} Changes: ({1}) {2} to {3}", prefix, changelistNumbers.Count(), changelistNumbers.Min(), changelistNumbers.Max());
                }
                else
                {
                    this.Subject = String.Format("{0} Change: {1}", prefix, changelistNumbers.Last());
                }
            }
            else if (filesTriggering.Any())
            {
                // just grab the first file as requested (url:bugstar:1160828)
                string firstTriggerFilename = Path.GetFileName(filesTriggering.First());
                this.Subject = String.Format("{0} - {1} - Job: {2}", prefix, firstTriggerFilename, this.Job.ID);
            }
            else
            {
                this.Subject = String.Format("{0} Job: {1}", prefix, this.Job.ID);
            }
        }

        /// <summary>
        /// Override for setting the body of the job notification email
        /// </summary>
        /// 
        public override void SetBody()
        {
            String logfile = this.Options.Config.Project.Environment.Subst(String.Format("$(cache)/automation/server/jobs/{0}/job.ulog", this.Job.ID));
            bool logHasErrors = true;
            base.SetBody();
            if (File.Exists(logfile))
            {
                logHasErrors = UniversalLogFile.ContainsErrors(logfile);
                if (logHasErrors)
                {
                    IEnumerable<Pair<String, String>> errorList = UniversalLogFile.GetErrors(logfile, this.MaxErrors);
                    this.Body.AppendLine(String.Format("\nError count {0}", UniversalLogFile.GetNumErrors(logfile)));
                    foreach (Pair<String, String> error in errorList)
                        this.Body.AppendLine(String.Format("{0} : {1}", error.First, error.Second));

                    if (errorList.Count() == this.MaxErrors)
                    {
                        this.Body.AppendLine(String.Format("Error list truncated after {0} errors.", this.MaxErrors));
                    }
                }
            }
            else
            {
                this.Body.AppendLine(String.Format("\nError: No log file found on server!  Expected one at {0}.", logfile));
            }
        }

        /// <summary>
        /// HTML body
        /// - this is just an interim solution
        /// </summary>
        public override void SetBodyHtml()
        {
            String logfile = this.Options.Config.Project.Environment.Subst(String.Format("$(cache)/automation/server/jobs/{0}/job.ulog", this.Job.ID));
            bool logHasErrors = true;
            if (File.Exists(logfile))
            {
                logHasErrors = UniversalLogFile.ContainsErrors(logfile);

                string templateFolder = Path.Combine(this.Options.Config.ToolsRoot, "web", "release", "automation", "tmpl");
                string templateFilename = Path.Combine(templateFolder, TEMPLATE_FILENAME);
                String errorBodyFilename = Path.Combine(templateFolder, ERROR_BODY_FILENAME);                                   
                string successBodyFilename = Path.Combine(templateFolder, SUCCESS_BODY_FILENAME);

                // Build up a dictionary of tokenised replacements
                IDictionary<string, string> tokenDict = new Dictionary<string, string>();

                // Empty - so can be appended to
                foreach (string token in HTML_TOKENS)
                {
                    tokenDict.Add(new KeyValuePair<string, string>(token, ""));
                }

                tokenDict[HEADER_HEIGHT_TOKEN] = "30";
                tokenDict[HEADER_FONT_SIZE_TOKEN] = "15";
                tokenDict[HEADER_LINE_HEIGHT_TOKEN] = "20";

                if (this.Job is AssetBuilderJob)
                {
                    tokenDict[INSTRUCTIVE_MESSAGE_TOKEN] = "The build machine takes the independent data you have checked in and converts them to ps3, 360 etc.\nAs artists check files in, this is automatically done in the background. When you export locally\nthis is done on your own machine. Occasionally different errors occur on the build machine and\nthis may be one of them.\n";
                    tokenDict[COMMON_ERRORS_WIKI_TOKEN] = "Asset_Pipeline_Common_Errors";

                    tokenDict[HEADER_HEIGHT_TOKEN] = "40";
                    tokenDict[HEADER_FONT_SIZE_TOKEN] = "30";
                    tokenDict[HEADER_LINE_HEIGHT_TOKEN] = "35";                
                }
                else if (this.Job is IntegratorJob)
                {
                    tokenDict[INSTRUCTIVE_MESSAGE_TOKEN] = "The autointegrator attempts to integrate all changes from one project/branch to another.\nIf there are conflicts such as this one, the process is halted until the conflict is resolved manually and submitted.\nTypically this requires some code or text to be merged by hand.If you're not certain how to resolve this conflict, please contact the original authors of the changes in question, or any of the people on this email.\n";
                    tokenDict[COMMON_ERRORS_WIKI_TOKEN] = "Automated_Integrator_Resolve_Procedure#Procedure_for_unblocking_integrator";
                }

                tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("Project (Branch): {0} ({1})\n", this.Options.CoreProject.FriendlyName, String.IsNullOrEmpty(this.BranchNameOverride) ?  this.Options.Branch.Name : this.BranchNameOverride);
                
                if (this.Job is IntegratorJob)
                {
                    IntegratorJob integratorJob = (IntegratorJob)this.Job;
                    Integration integration = integratorJob.Integration;
                    String integrationInfo = String.Format("Integration: {0} ({1})\n", integration.BranchSpec, integration.P4ResolveOptions);
                    tokenDict[PERSONAL_MSG_TEXT_TOKEN] += integrationInfo;
                }

                // Get the html template
                string htmlContents = File.ReadAllText(templateFilename);

                // Glue other html files together
                if (logHasErrors)
                {
                    if (File.Exists(errorBodyFilename))
                    {
                        string errorBodyHtmlContents = File.ReadAllText(errorBodyFilename);
                        htmlContents = htmlContents.Replace(HtmlToken(ERROR_BODY_TOKEN), errorBodyHtmlContents);
                    }
                }
                else
                {
                    if (File.Exists(successBodyFilename))
                    {
                        string successBodyHtmlContents = File.ReadAllText(successBodyFilename);
                        htmlContents = htmlContents.Replace(HtmlToken(SUCCESS_BODY_TOKEN), successBodyHtmlContents);
                    }
                }

                // Subject line again in the Body
                if (logHasErrors)
                {
                    IEnumerable<uint> changelistNumbers = Job.Trigger.ChangelistNumbers();

                    if (changelistNumbers.Any())
                    {
                        if (changelistNumbers.HasAtLeast(2))
                        {
                            String changelistSummary = String.Format("({0}) {1} to {2}", changelistNumbers.Count(), changelistNumbers.Min(), changelistNumbers.Max());
                            tokenDict[ERROR_MSGS_TOKEN] = String.Format("{0} {1} ({2}) : {3} - Owner: {4} - Changelist(s): {5}", String.IsNullOrEmpty(this.JobNameOverride) ? this.Job.Role.ToString() : this.JobNameOverride, this.Options.CoreProject.FriendlyName, String.IsNullOrEmpty(this.BranchNameOverride) ? this.Options.Branch.Name : this.BranchNameOverride, this.Job.State.ToString(), this.Owner, changelistSummary);
                        }
                        else
                        {
                            tokenDict[ERROR_MSGS_TOKEN] = String.Format("{0} {1} ({2}) : {3} - Owner: {4} - Changelist(s): {5}", String.IsNullOrEmpty(this.JobNameOverride) ? this.Job.Role.ToString() : this.JobNameOverride, this.Options.CoreProject.FriendlyName, String.IsNullOrEmpty(this.BranchNameOverride) ? this.Options.Branch.Name : this.BranchNameOverride, this.Job.State.ToString(), this.Owner, changelistNumbers.Last());
                        }
                    }
                    else
                    {
                        tokenDict[ERROR_MSGS_TOKEN] = String.Format("{0} {1} ({2}) : {3} - User: {4}", String.IsNullOrEmpty(this.JobNameOverride) ? this.Job.Role.ToString() : this.JobNameOverride, this.Options.CoreProject.FriendlyName, String.IsNullOrEmpty(this.BranchNameOverride) ? this.Options.Branch.Name : this.BranchNameOverride, this.Job.State.ToString(), this.Owner);
                    }
                }
                else
                {
                    tokenDict[SUCCESS_MSGS_TOKEN] = String.Format("Success : {0} {1} ({2})", String.IsNullOrEmpty(this.JobNameOverride) ? this.Job.Role.ToString() : this.JobNameOverride, this.Options.CoreProject.FriendlyName, String.IsNullOrEmpty(this.BranchNameOverride) ? this.Options.Branch.Name : this.BranchNameOverride);
                }

                // Populate further replacements with values
                if (logHasErrors)
                {
                    int numErrors = UniversalLogFile.GetNumErrors(logfile);
                    IEnumerable<Pair<String, String>> errorList = UniversalLogFile.GetErrors(logfile, this.MaxErrors);

                    tokenDict[PROCESSED_ERROR_COUNT_TOKEN] += numErrors.ToString();

                    foreach (Pair<String, String> error in errorList)
                    {
                        if (this.Job is CodeBuilder2Job || this.Job is ScriptBuilderJob)
                        {
#warning DW : TODO : probably not the best way to handle this, needs to be abstracted, just not decided how yet.
                            tokenDict[ERROR_LIST_TOKEN] += String.Format("{0}\n", error.Second);
                        }
                        else
                        {
                            tokenDict[ERROR_LIST_TOKEN] += String.Format("{0} : {1}\n", error.First, error.Second);
                        }
                    }

                    if (errorList.Count() == this.MaxErrors)
                    {
                        tokenDict[ERROR_LIST_TOKEN] += String.Format("Error list truncated after {0} errors.\n", this.MaxErrors);
                    }

                    tokenDict[PROCESSED_ERROR_COUNT_TOKEN] = String.Format("{0}/{1}", Math.Min(MaxErrors, numErrors), numErrors);
                }
                else
                {
                    tokenDict[SUCCESS_MSG_TOKEN] += String.Format("Successful Convert! {0} job run on {3} completed in {1:hh\\:mm\\:ss} with a status of {2}.", this.Job.Role, this.Job.ProcessingTime, this.Job.State, this.WorkerUri);
                }

                if (Job is CodeBuilder2Job)
                {
                    CodeBuilder2Job codebuilder2Job = (CodeBuilder2Job)Job;
                    tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("Codebuilder Solution    : {0}\n",codebuilder2Job.SolutionFilename);
                    tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("Codebuilder Platform    : {0}\n",codebuilder2Job.Platform);
                    tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("Codebuilder Build Config: {0}\n",codebuilder2Job.BuildConfig);
                }
                else if (Job is ScriptBuilderJob)
                {
                    ScriptBuilderJob scriptbuilderJob = (ScriptBuilderJob)Job;
                    tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("ScriptBuilder Project    : {0}\n", scriptbuilderJob.ScriptProjectFilename);
                    tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("ScriptBuilder Build Config: {0}\n", scriptbuilderJob.BuildConfig);
                }
                else if (Job is IntegratorJob)
                {
                    IntegratorJob integratorJob = (IntegratorJob)Job;
                    tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("Integrator Branch Spec      : {0}\n",integratorJob.Integration.BranchSpec);
                    tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("Integrator Resolve Options  : {0}\n",integratorJob.Integration.P4ResolveOptions);
                }

                using (P4 p4 = this.Options.Config.Project.SCMConnect())
                {
                    IEnumerable<String> files = Job.Trigger.TriggeringFiles();
                    IEnumerable<uint> changelists = Job.Trigger.ChangelistNumbers();
                    IEnumerable<String> usernames = Job.Trigger.Usernames();
                    IEnumerable<DateTime> triggeredAtDateTimes = Job.Trigger.TriggeredAt();
                    IEnumerable<String> descriptions = Job.Trigger.Descriptions();

                    if (changelists.Any())
                    {
                        ICollection<String> prettyCls = new List<String>();
                        foreach (uint clNum in changelists)
                        {
                            String changelistP4Web = P4Web.ChangelistHyperLink(p4, clNum);
                            prettyCls.Add(changelistP4Web);
                        }

                        if (descriptions.HasAtLeast(2))
                        {
                            tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("Changelist(s) ({1}): {0}\n", String.Join(",", prettyCls), prettyCls.Count());
                        }
                        else
                        {
                            tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("Changelist: {0}\n", prettyCls.Last());
                        }
                    }

                    if (descriptions.Any())
                    {
                        if (descriptions.HasAtLeast(2))
                        {
                            tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("Last Description: {0}\n", descriptions.Last());
                        }
                        else
                        {
                            tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("Description: {0}\n", descriptions.Last());
                        }
                    }

                    tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("Files:\n");
                    foreach (String filename in files)
                    {
                        tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("\t{0}\n", filename);
                    }

                    if (logHasErrors)
                    {
                        if (triggeredAtDateTimes.Any())
                        {
                            if (triggeredAtDateTimes.HasAtLeast(2))
                            {
                                tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("Triggered between: {0} to {1}\n", triggeredAtDateTimes.First(), triggeredAtDateTimes.Last());
                            }
                            else
                            {
                                tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("Triggered at: {0}\n", triggeredAtDateTimes.Last());
                            }
                        }

                        if (usernames.Any())
                        {
                            if (usernames.HasAtLeast(2))
                            {
                                tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("Users: {0}\n", String.Join(",", usernames));
                            }
                            else
                            {
                                tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("User: {0}\n", usernames.Last());
                            }
                        }

                        tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("Processed By (GUID): {0}\n", this.WorkerGuid);
                        tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("Processed By (URI): {0}\n", this.WorkerUri);
                    }

                    if (JobResults != null)
                    {
                        foreach (IJobResult jobResult in JobResults)
                        {
                            string result = jobResult.ToString();

                            // Special formatting requested in B* 1513525
                            AssetBuilderJobResult assetBuilderJobResult = jobResult as AssetBuilderJobResult;
                            if (assetBuilderJobResult != null)
                            {                                
                                // Part 1) - Success or failed.
                                tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("Result: {0}\n", jobResult.Succeeded ? "SUCCESS" : "FAILED");
                                
                                // Part 2) Submitted CL if submitted, otherwise the reason it was not submitted.
                                JobSubmissionState changeSubmissionState;
                                String prettyClNumber = Enum.TryParse<JobSubmissionState>(assetBuilderJobResult.SubmittedChangelistNumber.ToString(), out changeSubmissionState) ? changeSubmissionState.ToString() : "Unknown?";

                                String finalClDetails = P4Web.ChangelistHyperLinks(p4, prettyClNumber);
                                
                                // Remove prefix
                                finalClDetails = finalClDetails.Replace(P4Web.CHANGELIST_PREFIX, "");
                                
                                // Add a wordy 'result'
                                tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("Submitted Changelist of Resource Files: {0}\n", finalClDetails);
                            }
                            else
                            {
                                result = P4Web.ChangelistHyperLinks(p4, result);
                                tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("Result: {0}\n", result);
                            }                            
                        }
                    }

                    if (BugIdsCreated != null)
                    {
                        foreach (uint bugId in BugIdsCreated)
                        {
                            tokenDict[PERSONAL_MSG_TEXT_TOKEN] += String.Format("Bug Created: url:bugstar:{0}\n", bugId);
                        }
                    }
                }

                UpdateTokenFilesTrigger(tokenDict);

                // Now replace the token delimited in ! in html with the token contents.
                foreach (string token in HTML_TOKENS)
                {
                    htmlContents = htmlContents.Replace(HtmlToken(token), tokenDict[token]);
                }

                // Get rid of other gunk
                this.Body.Clear();
                this.Header.Clear();
                this.Footer.Clear();

                // Set the body to html
                this.Body.Append(htmlContents);
            }
            else
            {
                //tokenDict[ERROR_SUMMARY_TOKEN] += String.Format("Internal Error");
                //tokenDict[ERROR_LIST_TOKEN] += String.Format("Error: No log file found on server!  Expected one at {0}.", logfile);

                this.Html = false;
            }
        }
        #endregion // Controller Methods

        #region Private methods

        private static string HtmlToken(string token)
        {
            return String.Format("{1}{0}{1}", token, DELIMITER);
        }

        private void UpdateTokenFilesTrigger(IDictionary<string, string> tokenDict)
        {
            IEnumerable<String> files = Job.Trigger.TriggeringFiles();
            UpdateTokenFilesTrigger(tokenDict, files);
        }

        private void UpdateTokenFilesTrigger(IDictionary<string, string> tokenDict, IEnumerable<String> files)
        {
            tokenDict[PROCESSED_FILES_NUM_TOKEN] += files.Count().ToString();
            Int32 filecount = 0;
            foreach (String file in files)
            {
                tokenDict[PROCESSED_FILES_LIST_TOKEN] += file + "\n";
                filecount++;
                if (filecount == this.MaxFileList)
                {
                    tokenDict[PROCESSED_FILES_LIST_TOKEN] += String.Format("Truncated after {0} files", this.MaxFileList);
                    break;
                }
            }
        }
        #endregion // Private methods
    }
}
