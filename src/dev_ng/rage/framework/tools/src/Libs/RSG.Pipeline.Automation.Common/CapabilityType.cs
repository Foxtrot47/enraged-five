﻿using System;
using System.Runtime.Serialization;

namespace RSG.Pipeline.Automation.Common
{

    /// <summary>
    /// Automation capability mode enumeration. 
    /// </summary>
    [DataContract(Namespace = "")]
    [Flags]
    public enum CapabilityType
    {
        /// <summary>
        /// No capability; default.
        /// </summary>
        [EnumMember]
        None = 0,

        /// <summary>
        /// Asset Builder capability.
        /// </summary>
        [EnumMember]
        AssetBuilder = 1 << 0,

        /// <summary>
        /// Asset Builder (rebuilds) capability.
        /// </summary>
        [EnumMember]
        AssetBuilderRebuilds = 1 << 1,

        /// <summary>
        /// Asset Builder (user) capability.
        /// </summary>
        [EnumMember]
        AssetBuilderUser = 1 << 2,

        /// <summary>
        /// Map auto-exporter capability.
        /// </summary>
        [EnumMember]
        MapExport = 1 << 3,

        /// <summary>
        /// Map auto-exporter without Perforce submission.
        /// </summary>
        [EnumMember]
        MapExportNetwork = 1 << 4,

        /// <summary>
        /// Cutscene exporter.
        /// </summary>
        [EnumMember]
        CutsceneExportNetwork = 1 << 5,

        /// <summary>
        /// Cutscene Package capability.
        /// </summary>
        [EnumMember]
        CutscenePackage = 1 << 6,

        /// <summary>
        /// Code Building capability.
        /// </summary>
        [EnumMember]
        CodeBuilder = 1 << 7,

        /// <summary>
        /// Scheduling unit testing capability.
        /// </summary>
        [EnumMember]
        SchedulingUnitTesting = 1 << 8,

        /// <summary>
        /// Sync user details (from bugstar) capability.
        /// </summary>
        [EnumMember]
        UserDetails = 1 << 9,

        /// <summary>
        /// Sync and rebuild map global texture dictionary capability.
        /// </summary>
        [EnumMember]
        MapGTXDExportTask = 1 << 10,

        /// <summary>
        /// SCM Integration capability.
        /// </summary>
        [EnumMember]
        Integrator = 1 << 11,

        /// <summary>
        /// Code builder 2 capability.
        /// </summary>
        [EnumMember]
        CodeBuilder2 = 1 << 12,

        /// <summary>
        /// </summary>
        [EnumMember]
        RagebuilderTest = 1 << 13,

        /// <summary>
        /// Cutscene Scripter capability.
        /// </summary>
        [EnumMember]
        CutsceneScripter = 1 << 14,

        /// <summary>
        /// Script builder capability.
        /// </summary>
        [EnumMember]
        ScriptBuilder = 1 << 15,

        /// <summary>
        /// SmokeTest capability.
        /// </summary>
        [EnumMember]
        SmokeTester = 1 << 16,

		/// <summary>
		/// PrebuildLabelTimed capability.
		/// </summary>
		[EnumMember]
		PrebuildLabelTimed = 1 << 17,

		/// <summary>
		/// Texture Processor task.
		/// </summary>
		[EnumMember]
		TextureProcessor = 1 << 18,

		/// <summary>
		/// Face Animation Processor task.
		/// </summary>
		[EnumMember]
		FaceAnimationProcessor = 1 << 19,

		/// <summary>
		/// Nav Mesh Generator task.
		/// </summary>
		[EnumMember]
		NavMeshGenerator = 1 << 20,

		/// <summary>
		/// Ptfx Package Processor task.
		/// </summary>
		[EnumMember]
		PtfxPackage = 1 << 21,

		/// <summary>
		/// Database Manager Task
		/// </summary>
		[EnumMember]
		DatabaseManager = 1 << 22,

        /// <summary>
        /// Command Runner Task
        /// </summary>
        [EnumMember]
        CommandRunner = 1 << 23,

        /// <summary>
        /// <summary>
        /// CutSceneRPFBuilder capability.
        /// </summary>
        [EnumMember]
        CutSceneRPFBuilder = 1 << 24,

        /// <summary>
        /// FrameCapture capability.
        /// </summary>
        [EnumMember]
        FrameCapture = 1 << 25,

        /// <summary>
        /// Animation Package capability.
        /// </summary>
        [EnumMember]
        AnimationPackage = 1 << 26,

        /// <summary>
        /// CutsceneExport capability.
        /// </summary>
        [EnumMember]
        CutsceneExport = 1 << 27,

        /// <summary>
        /// All capabilities.
        /// </summary>
        [EnumMember]
        All = 0x7FFFFFFF,
    }

} // RSG.Pipeline.Automation.Common namespace
