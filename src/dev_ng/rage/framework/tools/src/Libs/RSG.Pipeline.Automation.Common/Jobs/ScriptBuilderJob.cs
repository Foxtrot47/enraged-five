﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Pipeline.Core;
using RSG.SourceControl.Perforce;
using RSG.Base.Extensions;
using RSG.Pipeline.Automation.Common.JobManagement;
using System.Xml.Serialization;
using RSG.Pipeline.Automation.Common.Triggers;

namespace RSG.Pipeline.Automation.Common.Jobs
{
    /// <summary>
    /// ScriptBuilder client side job.
    /// </summary>
    [DataContract]
    public class ScriptBuilderJob :
        Job,
        IJob
    {
        #region Constants
#warning DW: TODO : we should use memberinfo attributes to drive serialisation using reflection.
        private static readonly String XML_ELEMENT_NAME_SCRIPTPROJECT_FILENAME = "ScriptProjectFilename";
        private static readonly String XML_ELEMENT_NAME_BUILD_CONFIG = "BuildConfig";
        private static readonly String XML_ELEMENT_NAME_REBUILD = "Rebuild";
        private static readonly String XML_ELEMENT_NAME_TOOL = "Tool";
        private static readonly String XML_ELEMENT_SKIP_CONSUME = "SkipConsume";
        private static readonly String XML_ELEMENT_REQUIRES_POST_JOB_NOTIFICATION = "RequiresPostJobNotification";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Script Project filename Path (.scproj)
        /// </summary>
        [DataMember]
        public String ScriptProjectFilename { get; private set; }

        /// <summary>
        /// Name of the Build Config.
        /// </summary>
        [DataMember]
        public String BuildConfig { get; private set; }

        /// <summary>
        /// Build Tool to use (Visual Studio, VSI, Incredibuild)
        /// </summary>
        [DataMember]
        public String Tool { get; private set; }

        /// <summary>
        /// Rebuild flag
        /// </summary>
        [DataMember]
        public bool Rebuild { get; private set; }

        /// <summary>
        /// Skip consume
        /// - when jobs are created this was the skip setting it was created with.
        /// - this may change when jobs are combined.
        /// </summary>
        [DataMember]
        public int SkipConsume { get; private set; }

        /// <summary>
        /// Does this job require post job notification?
        /// </summary>
        [DataMember]
        public bool RequiresPostJobNotification { get; private set; }

        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Default constructor (for deserialisation).
        /// </summary>
        public ScriptBuilderJob()
            : base()
        {
            ScriptProjectFilename = null;
            BuildConfig = null;
            Tool = null;
            Rebuild = false;
            SkipConsume = -1;
            RequiresPostJobNotification = true;
            Role = CapabilityType.ScriptBuilder;
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ScriptBuilderJob(XElement xmlJobElement)
            : base()
        {
            ScriptProjectFilename = Environment.ExpandEnvironmentVariables(xmlJobElement.Elements(XML_ELEMENT_NAME_SCRIPTPROJECT_FILENAME).First().Value);
            BuildConfig = xmlJobElement.Elements(XML_ELEMENT_NAME_BUILD_CONFIG).First().Value;
            Rebuild = bool.Parse(xmlJobElement.Elements(XML_ELEMENT_NAME_REBUILD).First().Value);
            Tool = xmlJobElement.Elements(XML_ELEMENT_NAME_TOOL).First().Value;
            SkipConsume = int.Parse(xmlJobElement.Elements(XML_ELEMENT_SKIP_CONSUME).First().Value);
            RequiresPostJobNotification = bool.Parse(xmlJobElement.Elements(XML_ELEMENT_REQUIRES_POST_JOB_NOTIFICATION).First().Value);
            Role = CapabilityType.ScriptBuilder;
        }

        /// <summary>
        /// Parameterized constructor.
        /// </summary>
        public ScriptBuilderJob(Changelist change, IEnumerable<String> pathnamesToProcess, ScriptProjectItem scriptProjectItem, IBranch branch, IEnumerable<String> monitoringPaths = null, Guid triggerId = default(Guid))
            : base(branch, CapabilityType.ScriptBuilder, change, pathnamesToProcess, monitoringPaths, scriptProjectItem.PrebuildCommands, scriptProjectItem.PostbuildCommands, scriptProjectItem.Labels, triggerId)
        {
            ScriptProjectFilename = Environment.ExpandEnvironmentVariables(scriptProjectItem.ScriptProjectFilename);
            BuildConfig = scriptProjectItem.BuildConfig;
            Rebuild = scriptProjectItem.Rebuild;
            Tool = scriptProjectItem.Tool;
            SkipConsume = scriptProjectItem.SkipConsume;
            RequiresPostJobNotification = scriptProjectItem.RequiresPostJobNotification;
        }

        /// <summary>
        /// Parameterized constructor - multi trigger.
        /// </summary>
        public ScriptBuilderJob(IEnumerable<ITrigger> triggers, ScriptProjectItem scriptProjectItem, IBranch branch)
            : base(branch, CapabilityType.ScriptBuilder, triggers, scriptProjectItem.PrebuildCommands, scriptProjectItem.PostbuildCommands, scriptProjectItem.Labels)
        {
            ScriptProjectFilename = Environment.ExpandEnvironmentVariables(scriptProjectItem.ScriptProjectFilename);
            BuildConfig = scriptProjectItem.BuildConfig;
            Tool = scriptProjectItem.Tool;
            Rebuild = scriptProjectItem.Rebuild;
            SkipConsume = scriptProjectItem.SkipConsume;
            RequiresPostJobNotification = scriptProjectItem.RequiresPostJobNotification;
        }

        /// <summary>
        /// Parameterized constructor - ITrigger
        /// </summary>
        public ScriptBuilderJob(ITrigger trigger, ScriptProjectItem scriptProjectItem, IBranch branch)
            : base(branch, CapabilityType.ScriptBuilder, trigger, scriptProjectItem.PrebuildCommands, scriptProjectItem.PostbuildCommands, scriptProjectItem.Labels)
        {
            ScriptProjectFilename = Environment.ExpandEnvironmentVariables(scriptProjectItem.ScriptProjectFilename);
            BuildConfig = scriptProjectItem.BuildConfig;
            Tool = scriptProjectItem.Tool;
            Rebuild = scriptProjectItem.Rebuild;
            SkipConsume = scriptProjectItem.SkipConsume;
            RequiresPostJobNotification = scriptProjectItem.RequiresPostJobNotification;
        }

        public ScriptBuilderJob(CapabilityType capabilityType, DateTime completedAt, Guid consumedBy, DateTime createdAt, Guid id, IEnumerable<string> postbuildCommands,
                    IEnumerable<string> prebuildCommands, JobPriority jobPriority, DateTime processedAt, string processingHost, TimeSpan processingTime,
                    JobState jobState, ITrigger trigger, object userData
                    , String scriptProjectFilename, String targetDir, String publishDir, String zipFilePath,
                    String buildConfig, String tool, bool rebuild, int skipConsume, bool requiresPostJobNotification)
            : base(capabilityType, completedAt, consumedBy, createdAt, id, postbuildCommands, prebuildCommands, jobPriority, processedAt, processingHost, 
                    processingTime, jobState, trigger, userData)
        {
            ScriptProjectFilename = scriptProjectFilename;
            /*TargetDir = targetDir;
            PublishDir = publishDir;
            ZipFilePath = zipFilePath;*/
            BuildConfig = buildConfig;
            Tool = tool;
            Rebuild = rebuild;
            SkipConsume = skipConsume;
            RequiresPostJobNotification = requiresPostJobNotification;
        }

        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// Serialise job representation to XML.
        /// </summary>
        /// <returns></returns>
        public override XElement Serialise()
        {
            XElement xmlJob = base.Serialise();
            xmlJob.Add(new XElement(XML_ELEMENT_NAME_SCRIPTPROJECT_FILENAME, this.ScriptProjectFilename));
            xmlJob.Add(new XElement(XML_ELEMENT_NAME_BUILD_CONFIG, this.BuildConfig));
            xmlJob.Add(new XElement(XML_ELEMENT_NAME_TOOL, this.Tool));
            xmlJob.Add(new XElement(XML_ELEMENT_NAME_REBUILD, this.Rebuild.ToString()));
            xmlJob.Add(new XElement(XML_ELEMENT_SKIP_CONSUME, this.SkipConsume));
            xmlJob.Add(new XElement(XML_ELEMENT_REQUIRES_POST_JOB_NOTIFICATION, this.RequiresPostJobNotification));
            return (xmlJob);
        }

        /// <summary>
        /// Deserialise job representation from XML.
        /// </summary>
        /// <param name="xmlJobElem"></param>
        /// <returns></returns>
        public override bool Deserialise(XElement xmlJobElem)
        {
            bool result = true;
            try
            {
                result &= base.Deserialise(xmlJobElem);
                this.ScriptProjectFilename = Environment.ExpandEnvironmentVariables(xmlJobElem.XPathSelectElement(XML_ELEMENT_NAME_SCRIPTPROJECT_FILENAME).Value);
                this.BuildConfig = xmlJobElem.XPathSelectElement(XML_ELEMENT_NAME_BUILD_CONFIG).Value;
                this.Tool = xmlJobElem.XPathSelectElement(XML_ELEMENT_NAME_TOOL).Value;
                this.Rebuild = bool.Parse(xmlJobElem.XPathSelectElement(XML_ELEMENT_NAME_REBUILD).Value);
                this.SkipConsume = int.Parse(xmlJobElem.XPathSelectElement(XML_ELEMENT_SKIP_CONSUME).Value);
                this.RequiresPostJobNotification = xmlJobElem.XPathSelectElement(XML_ELEMENT_REQUIRES_POST_JOB_NOTIFICATION) != null ? bool.Parse(xmlJobElem.XPathSelectElement(XML_ELEMENT_REQUIRES_POST_JOB_NOTIFICATION).Value) : true;
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Error deserialising ScriptBuilder Job.");
                result = false;
            }
            return (result);
        }
        #endregion // Controller Methods

        #region Public Methods
        /// <summary>
        /// Jobs don't normally change but here it is transformed when consumed by another job.
        /// </summary>
        /// <param name="jobConsuming">the job consuming this job</param>
        public void SetConsumed(IJob jobConsuming)
        {
            // Hold a 'guid reference' to the consuming job
            this.ConsumedByJobID = jobConsuming.ID;

            // Change state to no longer be pending but is now consumed.
            this.State = JobState.SkippedConsumed;

            // The skip setting of the job is set to an enum value that indicates it has now been consumed.
            this.SkipConsume = -2;
        }
        #endregion // Public Methods.

    }
}
