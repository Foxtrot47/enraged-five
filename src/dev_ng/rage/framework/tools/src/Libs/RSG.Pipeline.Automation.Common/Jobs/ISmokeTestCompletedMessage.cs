﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Jobs
{
    public interface ISmokeTestCompletedMessages
    {
        /// <summary>
        /// Strings to monitor for in game output that designate the SmokeTest completing successfully
        /// </summary>
        IEnumerable<String> SuccessMessages { get; }

        /// <summary>
        /// Strings to monitor for in game output that designate the SmokeTest failing
        /// </summary>
        IEnumerable<String> FailureMessages { get; }
    }
}
