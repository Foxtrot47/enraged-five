﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace RSG.Pipeline.Automation.Common.Tasks
{

    /// <summary>
    /// 
    /// </summary>
    [DataContract(Namespace = "")]
    public class TimedTaskStatus : TaskStatus
    {
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="task"></param>
        public TimedTaskStatus(ITask task)
            : base(task)
        {
    
        }
        #endregion // Constructor(s)
    }

} // RSG.Pipeline.Automation.Common.Tasks namespace
