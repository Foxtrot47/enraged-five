﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Automation.Common.Jobs.JobData
{
    [DataContract]
    public class CommandRunnerJobInfo
    {
        [DataMember]
        public Guid ID { get; private set; }

        [DataMember]
        public String Command { get; private set; }
        
        [DataMember]
        public String Args { get; private set; }
        
        [DataMember]
        public String ErrorRegex { get; private set; }

        [DataMember]
        public String WarningRegex { get; private set; }


        public CommandRunnerJobInfo(Guid id, String command, String args, String errorRegex, String warningRegex)
            : this(command, args)
        {
            ID = id;
            ErrorRegex = errorRegex;
            WarningRegex = warningRegex;
        }

        public CommandRunnerJobInfo(String command, String args, String errorRegex, String warningRegex)
            : this(command, args)
        {
            ErrorRegex = errorRegex;
            WarningRegex = warningRegex;
        }
        public CommandRunnerJobInfo(String command, String args)
            : this(command)
        {
            Args = args;
        }

        public CommandRunnerJobInfo(String command)
        {
            ID = Guid.NewGuid();
            Command = command;
        }

        public CommandRunnerJobInfo()
        {
            ErrorRegex = String.Empty;
            WarningRegex = String.Empty;
            Args = String.Empty;
            Command = String.Empty;
            ID = Guid.NewGuid();
        }

        public override string ToString()
        {
            return String.Format("'{0}' {1}", Command, Args);
        }

        public XElement Serialise()
        {
            XElement element = new XElement("CommandInfo",
                                            new XElement("command", Command),
                                            new XElement("args", Args),
                                            new XElement("error_regex", ErrorRegex),
                                            new XElement("warning_regex", WarningRegex),
                                            new XElement("CommandRunnerID", ID));

            return element;
        }

        public void Deserialize(XElement element)
        {
            XElement command = element.Element("command");
            if (command != null)
            {
                Command = command.Value;
            }

            XElement args = element.Element("args");
            if (args != null)
            {
                Args = args.Value;
            }

            XElement errorRegex = element.Element("error_regex");
            if (errorRegex != null)
            {
                ErrorRegex = errorRegex.Value;
            }

            XElement warningRegex = element.Element("warning_regex");
            if (warningRegex != null)
            {
                WarningRegex = warningRegex.Value;
            }

            XElement commandRunnerID = element.Element("CommandRunnerID");
            Guid id = default(Guid);
            if (commandRunnerID != null)
            {
                Guid.TryParse(commandRunnerID.Value, out id);
            }

            ID = id;
        }
    }
}
