﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Pipeline.Automation.Common.JobManagement;

namespace RSG.Pipeline.Automation.Common.Tasks
{
    public interface IJobManagedTask
    {
        #region Properties
        /// <summary>
        /// Dispatch mapping object.
        /// </summary>
        DispatchScheme DispatchScheme { get; }
        #endregion // Properties
        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        String GetJobManagementFile();
        #endregion // Controller Methods
    }
}
