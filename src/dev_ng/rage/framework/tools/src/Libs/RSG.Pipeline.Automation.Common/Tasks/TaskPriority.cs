﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Tasks
{
    /// <summary>
    /// Task priority enumeration.
    /// </summary>
    [DataContract]
    public enum TaskPriority
    {
        [EnumMember]
        Low,
        /// <summary>
        /// Task with normal priority.
        /// </summary>
        [EnumMember]
        Normal,

        /// <summary>
        /// Task with elevated priority.
        /// </summary>
        [EnumMember]
        High,

        /// <summary>
        /// Task with elevated priority.
        /// </summary>
        [EnumMember]
        VeryHigh,
    }
}
