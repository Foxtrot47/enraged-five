﻿using P4API;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.SourceControl.Perforce;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.Common.Labels
{
    /// <summary>
    /// A label that a task can set
    /// - typically based upon the success of a collection of jobs.
    /// </summary>
    public class TaskLabel
    {
        #region Constants
        /// <summary>
        /// The context.
        /// </summary>
        private static readonly String LOG_CTX = "Automation:TaskLabel";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// The name of the label
        /// </summary>
        public String Name { get; private set; }

        /// <summary>
        /// The Host machine which is permitted to set the label
        /// </summary>
        public String PermittedHost { get; private set; }

        /// <summary>
        /// The target of the label operation is all monitored paths
        /// </summary>
        public bool LabelAllMonitoredPaths { get; private set; }

        /// <summary>
        /// Additonal paths that are to labelled.
        /// </summary>
        public IEnumerable<String> AdditionalLabelPaths { get; private set; }

        /// <summary>
        /// The job states that need to be set trigger a label ( typically completed )
        /// </summary>
        public JobState JobStateToTriggerLabel { get; private set; }

        /// <summary>
        /// Set the view of the label?
        /// </summary>
        public bool SetView { get; private set; }

        /// <summary>
        /// The last known changelist number this label has set.
        /// - does not persist between restarts
        /// </summary>
        public uint LastKnownChangeListNumberLabelSet { get; private set; }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public TaskLabel()
            : this(null, null, String.Empty, String.Empty, false, null, JobState.Completed, true, 0)
        {
        }

        /// <summary>
        /// Parameterised constructor
        /// </summary>
        /// <param name="labelName"></param>
        /// <param name="permittedHost"></param>
        /// <param name="labelAllMonitoredPaths"></param>
        /// <param name="additionalLabelPaths"></param>
        /// <param name="jobStateToTriggerLabel"></param>
        public TaskLabel(P4 p4, IUniversalLog log, String labelName, String permittedHost, bool labelAllMonitoredPaths, IEnumerable<String> additionalLabelPaths, JobState jobStateToTriggerLabel, bool setView, uint LastKnownChangelist = 0)
        {
            Name = labelName;
            PermittedHost = permittedHost;
            LabelAllMonitoredPaths = labelAllMonitoredPaths;
            AdditionalLabelPaths = additionalLabelPaths;
            JobStateToTriggerLabel = jobStateToTriggerLabel;
            SetView = setView;
            LastKnownChangeListNumberLabelSet = LastKnownChangelist;

            uint cl;
            if (GetLabelRevision(p4, log, labelName, out cl))
            {
                LastKnownChangeListNumberLabelSet = cl;
            }
        }
        #endregion // Constructors(s)

        #region Public Methods

        /// <summary>
        /// Sets a label for this task label.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="log"></param>
        /// <param name="finishedJob">the job that has just finished</param>
        /// <param name="allJobs">all jobs to consider.</param>
        /// <param name="role"></param>
        /// <returns>bool, true if label was set</returns>
        public bool SetLabel(CommandOptions options, IUniversalLog log, IJob finishedJob, IEnumerable<IJob> allJobs, CapabilityType role)
        {
            bool labelWasSet = false;

            // Get all the changelists of the job, sorted beyond the last changelist we used to label with.
            // NB. Because we could theoretically reprocess a job we allow the last label to be set again.
            IEnumerable<uint> changelistNumbersFinishedJob = finishedJob.Trigger.ChangelistNumbers()
                                                                        .Where(cl => cl >= LastKnownChangeListNumberLabelSet)
                                                                        .OrderByDescending(i => i).ToList();
            if (!changelistNumbersFinishedJob.Any())
            {
                log.MessageCtx(LOG_CTX, "No changelists to label");
                return labelWasSet;
            }

            IEnumerable<IJob> changelistJobsTaggedWithLabel = allJobs.Where(j => j.Trigger is ChangelistTrigger) // The only jobs we would consider are jobs that are also changelist triggered
                                                                     .Where(j => changelistNumbersFinishedJob.Contains(((ChangelistTrigger)j.Trigger).Changelist)) // Thes jobs must be triggered by a CL contained within the same changelist number range of the finished job.
                                                                     .Where(j => j.Labels.Contains(Name, StringComparer.InvariantCultureIgnoreCase)); // Of these jobs only jobs that have the label are to be considered.

            if (!changelistJobsTaggedWithLabel.Any())
            {
                log.WarningCtx(LOG_CTX, "No jobs were found that contain the label {0} and are of CLs in the range of {1}-{2} : this is unusual!", Name, changelistNumbersFinishedJob.Min(), changelistNumbersFinishedJob.Max());
                return labelWasSet;
            }

            // Starting with the highest changelist search for a 'labelling row' of jobs.
            uint changelistToLabel = 0;

            // Here we store the jobs that contain a resultant job state for which we need to check satifies our label
            ICollection<IJob> consumingJobs = new List<IJob>();

            // Search for a labelling row...
            // * X denotes a failed build and O a successful build
            // * Job 'A' as just completed with the processing of multiple CLs ( CL2,3,4,5 )
            // * We check rows 5,4,3,2 and discover CL2 should be labelled.
            //
            //          CL5 O   OX
            //          CL4 O OXOXOX  XOXO
            //          CL3 OOOXOOOOOOOOOX
            //          CL2 OOOOOOOOOOOOOO  <- This is labelled.
            //              ABCDEFGHIJKLMN

            // Create a useful hash of the jobs for quick lookup
            Dictionary<Guid, IJob> jobDict = new Dictionary<Guid, IJob>();
            foreach (IJob job in allJobs)
            {
                jobDict.Add(job.ID, job);
            }

            foreach (uint cl in changelistNumbersFinishedJob)
            {
                // For the potential jobs ensure they were triggered by the changelist we are searching.
                IEnumerable<IJob> jobsCurrentCL = changelistJobsTaggedWithLabel.Where(j => ((ChangelistTrigger)j.Trigger).Changelist == cl);

                if (!jobsCurrentCL.Any())
                {
                    // This would be unusual...
                    continue;
                }

                // Here we store the jobs that contain a resultant job state for which we need to check satisfies our label
                consumingJobs.Clear();

                // We have to deal with consuming jobs only.
                foreach (IJob job in jobsCurrentCL)
                {
                    if (job.State == JobState.SkippedConsumed && job.ConsumedByJobID != default(Guid))
                    {
                        // Find the consuming job
                        IJob consumingJob = jobDict[job.ConsumedByJobID];
                        if (consumingJob != null)
                        {
                            consumingJobs.Add(consumingJob);
                        }
                        else
                        {
                            log.WarningCtx(LOG_CTX, "Could not find a consuming job {0}", job.ConsumedByJobID);
                        }
                    }
                    else
                    {
                        consumingJobs.Add(job);
                    }
                }

                if (!consumingJobs.Any())
                {
                    continue;
                }

                // Find the jobs that do not have the required state for labelling.
                IEnumerable<IJob> jobsWithIncorrectState = consumingJobs.Where(j => j.State != JobStateToTriggerLabel);

                if (jobsWithIncorrectState.Any())
                {
                    //log.MessageCtx(LOG_CTX, "Despite finishing job {0} which processed CL {1}, {2} jobs are not in state {3} required to set label {4}", finishedJob.ID, cl, changelistJobsOfLabelWithCurrentCLWithIncorrectState.Count(), JobStateToTriggerLabel, Name);
                    continue;
                }
                else
                {
                    changelistToLabel = cl;
                    break;
                }
            }

            if (changelistToLabel == 0)
            {
                log.MessageCtx(LOG_CTX, "No Label could be set for label {0}", Name);
                return labelWasSet;
            }

            log.MessageCtx(LOG_CTX, " ");
            log.MessageCtx(LOG_CTX, "================================================================");
            log.MessageCtx(LOG_CTX, "Labelling {0} @{1} => @{2}...", Name, LastKnownChangeListNumberLabelSet != 0 ? LastKnownChangeListNumberLabelSet.ToString() : "?", changelistToLabel);   

            // Add all monitored into a hashset of the paths to label / sync
            ISet<String> monitoredPaths = new HashSet<String>();
            ISet<String> monitoredPathsSync = new HashSet<String>();
            if (LabelAllMonitoredPaths)
            {
                foreach (IJob job in consumingJobs)
                {
                    // Add monitored paths
                    foreach (String monitoredPath in job.Trigger.MonitoredPaths())
                    {
                        String pathToLabel, pathToLabelSync;
                        FormatDepotPaths(changelistToLabel, monitoredPath, out pathToLabel, out pathToLabelSync);

                        monitoredPathsSync.Add(pathToLabelSync);
                        monitoredPaths.Add(pathToLabel);
                    }
                }
            }

            // Also add additional paths
            ISet<String> additionalPaths = new HashSet<String>();
            ISet<String> additionalPathsSync = new HashSet<String>();
            foreach (String path in AdditionalLabelPaths.Where(p => !String.IsNullOrEmpty(p)))
            {
                String pathToLabel, pathToLabelSync;
                FormatDepotPaths(changelistToLabel, path, out pathToLabel, out pathToLabelSync);
                additionalPathsSync.Add(pathToLabelSync);
                additionalPaths.Add(pathToLabel);
            }

            // Get all the paths that would be labelled and/or synced.
            IEnumerable<String> allPathsToLabel = monitoredPaths.Union(additionalPaths);
            IEnumerable<String> allPathsSync = monitoredPathsSync.Union(additionalPathsSync);
            
            String description = BuildLabelDescription(role, consumingJobs, monitoredPaths, additionalPaths, allPathsSync);
            log.MessageCtx(LOG_CTX, description);
            
            // Now actually create or edit the label
            if (HostIsPermittedToSetLabel(log))
            {
                using (P4 p4 = options.Config.CoreProject.SCMConnect())
                {
                    labelWasSet = CreateOrEditLabel(log, p4, Name, changelistToLabel, description, allPathsToLabel);
                    if (labelWasSet)
                    {
                        LastKnownChangeListNumberLabelSet = changelistToLabel;
                    }
                }
            }

            log.MessageCtx(LOG_CTX, "================================================================");
            log.MessageCtx(LOG_CTX, " ");

            return labelWasSet;
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// safety net is important! - labels could be dangerous if set by another machine accidentally.
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        private bool HostIsPermittedToSetLabel(IUniversalLog log)
        {
            if (PermittedHost.Equals(Environment.MachineName, StringComparison.CurrentCultureIgnoreCase) || PermittedHost.Equals("any", StringComparison.CurrentCultureIgnoreCase))
            {
                return true;
            }
            else
            {
                log.WarningCtx(LOG_CTX, "This machine {0} is not permitted to set the label {1}, as a safety precaution only machine {2} can do this.", Environment.MachineName, Name, PermittedHost);
                return false;
            }
        }

        /// <summary>
        /// Reads the label to try to get the revision number.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="log"></param>
        /// <param name="labelName"></param>
        /// <param name="cl">the changelist revision of the label</param>
        /// <returns>bool whether it read the label revision ok</returns>
        private static bool GetLabelRevision(P4 p4, IUniversalLog log, String labelName, out uint cl)
        {
            cl = 0;
            if (p4 != null)
            {
                // Read label to get the current revision.
                try
                {
                    // Create Form
                    P4Form form = p4.Fetch_Form("label", labelName);

                    String rev = form["Revision"];
                    if (!String.IsNullOrEmpty(rev))
                    {
                        rev = rev.TrimStart('@');

                        if (!String.IsNullOrEmpty(rev))
                        {
                            if (uint.TryParse(rev, out cl))
                            {
                                log.MessageCtx(LOG_CTX, "Read latest revision of Label {0} as CL{1}", labelName, cl);
                                return true;
                            }
                            else
                            {
                                log.WarningCtx(LOG_CTX, "Could not read revision of Label {0}", labelName);
                            }
                        }
                    }
                }
                catch (P4API.Exceptions.P4APIExceptions ex)
                {
                    log.ToolExceptionCtx(LOG_CTX, ex, "Perforce exception while accessing label '{0}'.", labelName);
                }
            }
            return false;
        }

        /// <summary>
        /// Creates or edits a label
        /// </summary>
        /// <param name="log"></param>
        /// <param name="p4"></param>
        /// <param name="labelName"></param>
        /// <param name="changelistToLabel"></param>
        /// <param name="description"></param>
        /// <param name="view"></param>
        /// <returns></returns>
        private bool CreateOrEditLabel(IUniversalLog log, P4 p4, String labelName, uint changelistToLabel, String description, IEnumerable<String> view = null)
        {
            try
            {
                // Create Form
                P4Form form = p4.Fetch_Form("label", labelName);

                // Edit Form
                form["Label"] = labelName;
                form["Revision"] = String.Format("@{0}", changelistToLabel);
                form["Description"] = description;

                if (this.SetView && view != null)
                {
                    form.ArrayFields["View"] = view.ToArray();
                }

                // Save the form
                P4UnParsedRecordSet recordSet = p4.Save_Form(form);
                p4.LogP4RecordSetMessages(recordSet);
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Perforce exception while updating label '{0}'.", labelName);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Builds a description used for the label description
        /// </summary>
        /// <param name="role"></param>
        /// <param name="changelistJobsOfLabelWithCurrentCLResults"></param>
        /// <param name="monitoredPaths"></param>
        /// <param name="additionalPaths"></param>
        /// <param name="allPathsSync"></param>
        /// <returns></returns>
        private string BuildLabelDescription(CapabilityType role, ICollection<IJob> changelistJobsOfLabelWithCurrentCLResults, ISet<String> monitoredPaths, ISet<String> additionalPaths, IEnumerable<String> allPathsSync)
        {
            // Create a description for the label : gone for overkill intially - so we can debug it easier.
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(String.Format("Automatically created by tools automation framework on {0} : {1}\n", role, Environment.MachineName));
            sb.AppendLine(String.Format("Monitored Paths :\n{0}\n", String.Join("\n", monitoredPaths.ToArray())));
            sb.AppendLine(String.Format("Additional Paths :\n{0}\n", String.Join("\n", additionalPaths.ToArray())));
            sb.AppendLine(String.Format("Sync:\np4 sync -n {0}\n", String.Join(" ", allPathsSync.ToArray())));
            sb.AppendLine(String.Format("{0} Jobs of state '{1}' triggered setting of label :", changelistJobsOfLabelWithCurrentCLResults.Count(), JobStateToTriggerLabel));

            foreach (IJob job in changelistJobsOfLabelWithCurrentCLResults)
            {
                sb.AppendLine(String.Format("{0}\t:\t{1}", job.ID, job.FriendlyName()));
            }

            return sb.ToString();
        }

        /// <summary>
        /// Formats the ...s and filespecs - should find a new hoem somewhere?
        /// </summary>
        /// <param name="changelistToLabel"></param>
        /// <param name="monitoredPath"></param>
        /// <param name="pathToLabel"></param>
        /// <param name="pathToLabelSync"></param>
        private void FormatDepotPaths(uint changelistToLabel, String monitoredPath, out String pathToLabel, out String pathToLabelSync)
        {
            if (monitoredPath.Contains("..."))
            {
                pathToLabelSync = String.Format("{0}@{1}", monitoredPath, Name);
                pathToLabel = monitoredPath;
            }
            else if (monitoredPath.Contains("."))
            {
                pathToLabelSync = String.Format("{0}@{1}", monitoredPath, Name);
                pathToLabel = monitoredPath;
            }
            else
            {
                pathToLabelSync = String.Format("{0}/...@{1}", monitoredPath, Name);
                pathToLabel = String.Format("{0}/...", monitoredPath);
            }
        }

        #endregion // Private Methods

    }
}
