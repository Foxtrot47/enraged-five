﻿using System;
using System.Runtime.Serialization;
using RSG.Pipeline.Automation.Common.Jobs;

namespace RSG.Pipeline.Automation.Common.Client
{

    /// <summary>
    /// Worker status class.
    /// </summary>
    [DataContract]
    public class WorkerStatus
    {
        #region Properties
        /// <summary>
        /// Worker GUID.
        /// </summary>
        [DataMember(Order = 1)]
        public Guid ID { get; set; }

        /// <summary>
        /// Worker service connection.
        /// </summary>
        [DataMember(Order = 2)]
        public Uri ServiceConnection { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 3)]
        public WorkerState State { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 4)]
        public Guid AssignedJob { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember(Order = 5)]
        public Capability Capability { get; set; }
        #endregion // Properties
    }

} // RSG.Pipeline.Automation.Common.Job namespace
