﻿//---------------------------------------------------------------------------------------------
// <copyright file="IGameConnection.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2015. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Services.GameConnection.Contracts.DataContracts
{         
    using System;
    using System.Net;
    using RSG.Platform;

    /// <summary>
    /// Interface that represents a single game connection from a PC host.
    /// </summary>
    public interface IGameConnection
    {
        /// <summary>
        /// Game platform IP address.
        /// </summary>
        IPAddress GameIP { get; }

        /// <summary>
        /// Platform.
        /// </summary>
        Platform Platform { get; }

        /// <summary>
        /// Host IP address.
        /// </summary>
        IPAddress HostIP { get; }

        /// <summary>
        /// Game executable name.
        /// </summary>
        String ExecutableName { get; }

        /// <summary>
        /// Game build type.
        /// </summary>
        GameBuildType BuildType { get; }

        /// <summary>
        /// Timestamp for initial connection to the game.
        /// </summary>
        DateTime ConnectedAt { get; }
    }

} // RSG.Services.GameConnection.Contracts.DataContracts namespace
