﻿//---------------------------------------------------------------------------------------------
// <copyright file="GameConnection.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2015. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Services.GameConnection.Contracts.DataContracts
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Serialization;
    using System.Net;
    using System.Threading.Tasks;
    using RSG.Platform;

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class GameConnection : 
        IEquatable<GameConnection>,
        IGameConnection
    {
        #region Properties
        /// <summary>
        /// Game platform IP address.
        /// </summary>
        public IPAddress GameIP { get; set; }
        
        /// <summary>
        /// Surrogate property for serialising the Game IP address.
        /// </summary>
        [DataMember]
        private byte[] GameIPSurrogate
        {
            get { return GameIP.GetAddressBytes(); }
            set { GameIP = new IPAddress(value); }
        }

        /// <summary>
        /// Platform.
        /// </summary>
        [DataMember]
        public Platform Platform { get; set; }

        /// <summary>
        /// Host IP address.
        /// </summary>
        public IPAddress HostIP { get; set; }
        
        /// <summary>
        /// Surrogate property for serialising the Host IP address.
        /// </summary>
        [DataMember]
        private byte[] HostIPSurrogate
        {
            get { return HostIP.GetAddressBytes(); }
            set { HostIP = new IPAddress(value); }
        }

        /// <summary>
        /// Game executable name.
        /// </summary>
        [DataMember]
        public String ExecutableName { get; set; }

        /// <summary>
        /// Game build type.
        /// </summary>
        [DataMember]
        public GameBuildType BuildType { get; set; }

        /// <summary>
        /// Timestamp for initial connection to the game.
        /// </summary>
        [DataMember]
        public DateTime ConnectedAt { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public GameConnection()
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="gameIp"></param>
        /// <param name="platform"></param>
        /// <param name="hostIp"></param>
        /// <param name="executableName"></param>
        /// <param name="buildType"></param>
        /// <param name="connectedAt"></param>
        public GameConnection(IPAddress gameIp, Platform platform, IPAddress hostIp,
            String executableName, GameBuildType buildType, DateTime connectedAt)
        {
            this.GameIP = gameIp;
            this.Platform = platform;
            this.HostIP = hostIp;
            this.ExecutableName = executableName;
            this.BuildType = buildType;
            this.ConnectedAt = connectedAt;
        }
        #endregion // Constructor(s)

        #region IEquatable<GameConnection> Implementation
        /// <summary>
        /// Indicates whether this GameConnection is equal to another one.  Equality is
        /// determined by the game's connection time and IP.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(GameConnection other)
        {
            if (other == null)
            {
                return false;
            }

            return (this.ConnectedAt == other.ConnectedAt && this.GameIP.Equals(other.GameIP));
        }
        #endregion // IEquatable<GameConnection> Implementation

        #region Object Overrides
        /// <summary>
        /// Indicates whether this GameConnection is equal to another object.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(Object obj)
        {
            GameConnection connection = obj as GameConnection;
            if (connection == null)
            {
                return false;
            }

            return Equals(connection);
        }

        /// <summary>
        /// Retrieves the hash code for this object.
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return this.ConnectedAt.GetHashCode() ^ this.GameIP.GetHashCode();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Indicates whether two GameConnection objects can be considered to be equal.
        /// </summary>
        /// <param name="person1"></param>
        /// <param name="person2"></param>
        /// <returns></returns>
        public static bool operator ==(GameConnection connection1, GameConnection connection2)
        {
            if ((object)connection1 == null || ((object)connection2) == null)
            {
                return Object.Equals(connection1, connection2);
            }

            return connection1.Equals(connection2);
        }

        /// <summary>
        /// Indicates whether two GameConnection objects aren't the same.
        /// </summary>
        /// <param name="person1"></param>
        /// <param name="person2"></param>
        /// <returns></returns>
        public static bool operator !=(GameConnection connection1, GameConnection connection2)
        {
            if (connection1 == null || connection2 == null)
            {
                return !Object.Equals(connection1, connection2);
            }

            return !connection1.Equals(connection2);
        }
        #endregion // Methods
    }

} // RSG.Services.GameConnection.Contracts.DataContracts namespace
