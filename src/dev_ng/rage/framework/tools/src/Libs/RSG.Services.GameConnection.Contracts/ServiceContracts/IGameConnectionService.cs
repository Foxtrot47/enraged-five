﻿//---------------------------------------------------------------------------------------------
// <copyright file="IGameConnectionService.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2015. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Services.GameConnection.Contracts.ServiceContracts
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.ServiceModel;
    using RSG.Services.GameConnection.Contracts.DataContracts;

    /// <summary>
    /// Game Connection service interface; allowing applications to read information
    /// regarding the host PC's available game instances.
    /// </summary>
    [ServiceContract]
    public interface IGameConnectionService
    {
        /// <summary>
        /// Return information about all available game connections.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [ServiceKnownType(typeof(GameConnection))]
        IEnumerable<IGameConnection> GetGameConnections();
    }

} // RSG.Services.GameConnection.Contracts.ServiceContracts namespace
