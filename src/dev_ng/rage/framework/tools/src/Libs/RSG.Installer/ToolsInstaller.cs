﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml;
using Microsoft.Win32;
using RSG.Base.Configuration;
using RSG.Base.Configuration.Installer;
using RSG.Base.Extensions;
using RSG.Base.IO;
using RSG.Base.Logging.Universal;
using RSG.Interop.Autodesk3dsmax;
using RSG.Interop.AutodeskMotionBuilder;
using RSG.Interop.AutodeskMudbox;
using RSG.Interop.Microsoft.Windows;
using RSG.SourceControl.Perforce;
using RSG.Editor.Controls;
using System.Windows;

namespace RSG.Installer
{

    /*! \mainpage
     *
     * \section intro_sec Purpose
     *
     * The RSG.Installer assembly provides Rockstar Games Tools Installation
     * platform in a separate assembly from the GUI Tools Installer.  This
     * allows scripts to access its functionality for quickly rolling out
     * for automation machines etc.
     * 
     */

    /// <summary>
    /// This is the main Tools Installation class.
    /// </summary>
    public class ToolsInstaller
    {
        #region Constants
        private readonly static String LOG_CTX = "Install";
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// Writable configuration data.
        /// </summary>
        private IWritableConfig m_Config;

        /// <summary>
        /// Environment path that gets set during an install.
        /// </summary>
        private String m_envPath;

        /// <summary>
        /// Start menu folder path
        /// </summary>
        private String m_appStartMenuPath;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="config"></param>
        public ToolsInstaller(IWritableConfig config)
        {
            this.m_Config = config;

            string shortcutFolderName = string.Format("Rockstar Games ({0})", this.m_Config.CoreProject.FriendlyName);
            string commonStartMenuPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonStartMenu);
            m_appStartMenuPath = Path.Combine(commonStartMenuPath, shortcutFolderName);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Install full toolchain (top-level method).
        /// </summary>
        /// <param name="log"></param>
        /// <param name="userDefaultBranch">User-defined Default Branch</param>
        /// <param name="maxToolSet">Autodesk 3dsmax Tool Set</param>
        /// <param name="mbToolSet">Autodesk MotionBuilder Tool Set</param>
        /// <param name="mudboxToolSet">Autodesk Mudbox Tool Set</param>
        /// <returns></returns> 
        public bool Install(IUniversalLog log, String userDefaultBranch, DCCToolSet maxToolSet, DCCToolSet mbToolSet, DCCToolSet mudboxToolSet, bool mbInstallPython)
        {
            IInstallerConfig installConfig = ConfigFactory.CreateInstallerConfig(m_Config.Project);
            
            bool result = true;
            result &= SaveSettings(log);

            try
            {
                CreateShortcuts(log);
            }
            catch(Exception ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Exception creating start menu shortcuts.");
            }

            try
            {
                this.ForceMaxMenuRebuild(log);
            }
            catch (Exception ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Exception setting Autodesk ini value.");
            }

            try
            {
                if (DCCToolSet.DoNotInstall != maxToolSet)
                    result &= InstallAutodesk3dsmax(log, installConfig, maxToolSet);
            }
            catch (Autodesk3dsmaxException ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Exception installing Autodesk 3dsmax.");
            }
            try
            {
                if (DCCToolSet.DoNotInstall != mbToolSet)
                    result &= InstallAutodeskMotionBuilder(log, installConfig, mbToolSet, mbInstallPython);
            }
            catch (AutodeskMotionBuilderException ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Exception installing Autodesk MotionBuilder.");
            }
            try
            {
                if (DCCToolSet.DoNotInstall != mudboxToolSet)
                    result &= InstallAutodeskMudbox(log, installConfig, mudboxToolSet);
            }
            catch (AutodeskMudboxException ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Exception installing Autodesk Mudbox.");
            }

            if (SetEnvironment(log, userDefaultBranch))
            {
                log.MessageCtx(LOG_CTX, "Restarting selected resident processes.");

                try
                {
                    RestartProcesses();
                }
                catch (Exception e)
                {
                    log.ToolExceptionCtx(LOG_CTX, e, "Exception trying to restart selected resident processes.");

                    return false;
                }
            }                
            else
            {
                log.ErrorCtx(LOG_CTX, "Failed to set User-Environment.");
                result = false;
            }

            // Needs to happen after setting the environment.
            if (!CreateLocalSwitchBatchFile(userDefaultBranch))
            {
                log.ErrorCtx(LOG_CTX, "Failed to create the local switch batch file.");
                result = false;
            }

            return (result);
        }

        /// <summary>
        /// Reads a list of programs from an xml file and creates start menu shortcuts for those programs.
        /// </summary>
        public void CreateShortcuts(IUniversalLog log)
        {
            DateTime current = DateTime.UtcNow;

            string xmlPath = Path.Combine(this.m_Config.ToolsConfig, @"shortcutMenu\shortcuts_tools.xml");
            if (File.Exists(xmlPath))
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(xmlPath);

                XmlNodeList nodes = doc.DocumentElement.SelectNodes("//ExecutableTask");

                for (int i = 0; i < nodes.Count; i++)
                {
                    XmlNode node = nodes[i];

                    XmlAttribute nameAttribute = node.Attributes["name"];
                    if (nameAttribute == null)
                        continue;

                    string name = nameAttribute.Value;

                    XmlAttribute commandAttribute = node.Attributes["command"];
                    if (commandAttribute == null)
                        continue;

                    string command = commandAttribute.Value;

                    XmlAttribute argsAttribute = node.Attributes["args"];
                    string args = "";
                    if (argsAttribute != null)
                    {
                        args = argsAttribute.Value;
                    }

                    command = this.m_Config.CoreProject.Environment.Subst(command);

                    if (!string.IsNullOrEmpty(name) && !string.IsNullOrEmpty(command))
                    {
                        CreateStartMenuShortcut(name, command, args);
                    }
                }
            }

            if(Directory.Exists(m_appStartMenuPath))
            {
                foreach(var file in Directory.GetFiles(m_appStartMenuPath))
                {
                    DateTime last = File.GetLastWriteTimeUtc(file);
                    if (last.IsEarlierThan(current))
                    {
                        File.Delete(file);
                    }
                }

                if (Directory.GetFiles(m_appStartMenuPath).Count() == 0)
                {
                    Directory.Delete(m_appStartMenuPath);
                }
            }
        }

        /// <summary>
        /// Validate Autodesk 3dsmax configuration for fresh install.
        /// </summary>
        /// <param name="version"></param>
        /// <param name="is64bit"></param>
        /// <returns></returns>
        public void Autodesk3dsmaxConfigValidation(MaxVersion version, bool is64bit)
        {
            String configFilename = "";

            try
            {
                configFilename = Autodesk3dsmax.GetPluginConfigFilename(version, is64bit);
            }
            catch (Autodesk3dsmaxException)
            {
                throw;
            }
            catch (Exception)
            {
                configFilename = null;
            }

            if (configFilename == null || !File.Exists(configFilename))
            {
                string msg = String.Format("Fresh install detected. Open and close '{0}' then click OK.", version.GetDisplayName());
                RsMessageBox.Show(Application.Current.MainWindow, msg, "Tools Installer", MessageBoxButton.OK);
            }
        }

        /// <summary>
        /// Validate Autodesk Motionbuilder configuration for fresh install.
        /// </summary>
        /// <param name="version"></param>
        /// <param name="is64bit"></param>
        /// <returns></returns>
        public void AutodeskMotionbuilderConfigValidation(MotionBuilderVersion version, bool is64bit)
        {
            String configFilename = AutodeskMotionBuilder.GetPluginConfigFilename(version, is64bit);

            if (!File.Exists(configFilename))
            {
                string msg = String.Format("Fresh install detected. Open and close '{0}' then click OK.", version.GetDisplayName());
                RsMessageBox.Show(msg, "Tools Installer", MessageBoxButton.OK);
            }
        }
        
        /// <summary>
        /// Install Autodesk 3dsmax.
        /// </summary>
        /// <param name="installerConfig"></param>
        /// <param name="toolSet"></param>
        /// <returns></returns>
        public bool InstallAutodesk3dsmax(IUniversalLog log, IInstallerConfig installerConfig, DCCToolSet toolSet)
        {
            IApplicationInstall maxInstallations =
                installerConfig.Installs.Where(i => i.Name == "Autodesk3dsmax").FirstOrDefault();
            if (null == maxInstallations)
                return (true);

            foreach (KeyValuePair<String, bool> version in maxInstallations.Versions)
            {
                if (!version.Value)
                    continue;

                MaxVersion ver = MaxVersion.None;
                if (Enum.TryParse<MaxVersion>(version.Key, out ver))
                {
                    if (!Autodesk3dsmax.IsVersionInstalled(ver, true))
                        continue;

                    Autodesk3dsmaxConfigValidation(ver, true);

                    if (DCCToolSet.None == toolSet)
                    {
                        // Set default paths.
                        String userConfigPath = ver.UserConfigPath(true);
                        String plugCfgDir = Path.Combine(userConfigPath, "plugcfg");
                        String pluginDir = Path.Combine(Autodesk3dsmax.GetInstallPath(ver, true), "plugins");
                        String scriptsDir = Path.Combine(userConfigPath, "scripts");
                        String startupDir = Path.Combine(userConfigPath, "scripts", "startup");
                        String macroDir = Path.Combine(userConfigPath, "usermacros");
                        String userIconsDir = Path.Combine(userConfigPath, "UI", "usericons");

                        Autodesk3dsmax.SetAdditionalPluginPaths(ver, true, new Dictionary<String, String>()
                        {
                            { "RsPluginPath", pluginDir }
                        });
                        Autodesk3dsmax.ValidateAdditionalPluginPaths(ver, true);
                        Autodesk3dsmax.SetAdditionalConfigPaths(ver, true, plugCfgDir, scriptsDir,
                            startupDir, macroDir, userIconsDir);
                    }
                    else
                    {
                        String toolSetPath = toolSet.ToolPath();
                        String toolPath = Path.Combine(this.m_Config.ToolsRoot, "dcc",
                            toolSetPath, ver.ToolPath());
                        String plugCfgDir = Path.Combine(toolPath, "plugcfg");
                        String pluginDir = Path.Combine(toolPath, "plugins", "x64");
                        String scriptsDir = Path.Combine(toolPath, "scripts");
                        String startupDir = Path.Combine(scriptsDir, "startup");
                        String macroDir = Path.Combine(toolPath, "UI", "MacroScripts");
                        String userIconsDir = Path.Combine(toolPath, "UI", "usericons");

                        Autodesk3dsmax.SetAdditionalPluginPaths(ver, true, new Dictionary<String, String>()
                        {
                            { "RsPluginPath", pluginDir }
                        });
                        Autodesk3dsmax.ValidateAdditionalPluginPaths(ver, true);
                        Autodesk3dsmax.SetAdditionalConfigPaths(ver, true, plugCfgDir, scriptsDir,
                            startupDir, macroDir, userIconsDir);
                        Autodesk3dsmax.CopyDefaultPlugCfg(ver, true, plugCfgDir);
                    }
                }
            }

            return (true);
        }

        /// <summary>
        /// Return the installed tool-set for Autodesk 3dsmax.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="installerConfig"></param>
        /// <returns></returns>
        public DCCToolSet GetInstalledAutodesk3dsmaxToolSet(IUniversalLog log, IInstallerConfig installerConfig)
        {
            IApplicationInstall maxInstallations =
                installerConfig.Installs.Where(i => i.Name == "Autodesk3dsmax").FirstOrDefault();
            if (null == maxInstallations)
                return (DCCToolSet.None);

            foreach (KeyValuePair<String, bool> version in maxInstallations.Versions)
            {
                if (!version.Value)
                    continue;

                MaxVersion ver = MaxVersion.None;
                if (Enum.TryParse<MaxVersion>(version.Key, out ver))
                {
                    if (!Autodesk3dsmax.IsVersionInstalled(ver, true))
                        continue;

                    foreach (DCCToolSet ts in Enum.GetValues(typeof(DCCToolSet)).Cast<DCCToolSet>())
                    {
                        if ((DCCToolSet.None == ts) || (DCCToolSet.DoNotInstall == ts))
                            continue;

                        String toolSetPath = ts.ToolPath();
                        IDictionary<String, String> pluginPaths = Autodesk3dsmax.GetAdditionalPluginPaths(ver, true);
                        IEnumerable<String> paths = pluginPaths.Values.Where(p => p.Contains(toolSetPath));
                        if (paths.Any())
                            return (ts);
                    }
                }
            }
            return (DCCToolSet.None);
        }

        /// <summary>
        /// Install Autodesk MotionBuilder.
        /// </summary>
        /// <param name="installerConfig"></param>
        /// <param name="toolSet"></param>
        /// <returns></returns>
        public bool InstallAutodeskMotionBuilder(IUniversalLog log, IInstallerConfig installerConfig, DCCToolSet toolSet, bool installPython)
        {
            IApplicationInstall mbInstallations =
                installerConfig.Installs.Where(i => i.Name == "AutodeskMotionBuilder").FirstOrDefault();
            if (null == mbInstallations)
                return (true);

            foreach (KeyValuePair<String, bool> version in mbInstallations.Versions)
            {
                if (!version.Value)
                    continue;

                MotionBuilderVersion ver = MotionBuilderVersion.None;
                if (Enum.TryParse<MotionBuilderVersion>(version.Key, out ver))
                {
                    if (!AutodeskMotionBuilder.IsVersionInstalled(ver, true))
                        continue;

                    AutodeskMotionbuilderConfigValidation(ver, true);

                    if (DCCToolSet.None == toolSet)
                    {
                        AutodeskMotionBuilder.SetAdditionalPluginPaths(ver, true, new String[] { });
                        AutodeskMotionBuilder.SetPythonStartupPath(ver, true, String.Empty);
                        AutodeskMotionBuilder.SetAdditionalRockstarPluginPath(ver, true, String.Empty);
                    }
                    else
                    {
                        String toolSetPath = toolSet.ToolPath();
                        String toolPath = Path.Combine(this.m_Config.ToolsRoot, "dcc", toolSetPath,
                            ver.ToolPath(), "plugins", "x64");
                        String expressionToolPath = Path.Combine(this.m_Config.ToolsRoot, "dcc", toolSetPath,
                            ver.ToolPath(), "plugins", "x64", "expressions");
                        String pythonStartupPath = (installPython) ? Path.Combine(this.m_Config.ToolsRoot, 
                            "dcc", toolSetPath, ver.ToolPath(), "script") :  String.Empty;
                        String assembilyToolPath = Path.Combine(this.m_Config.ToolsRoot, "dcc", toolSetPath,
                            ver.ToolPath(), "assemblies", "x64");
                        AutodeskMotionBuilder.SetAdditionalPluginPaths(ver, true, new String[] 
                        {
                            toolPath,
                            expressionToolPath
                        });
                        AutodeskMotionBuilder.SetPythonStartupPath(ver, true, pythonStartupPath);
                        AutodeskMotionBuilder.SetAdditionalRockstarPluginPath(ver, true, assembilyToolPath);
                    }
                }
            }

            return (true);
        }

        /// <summary>
        /// Return the installed tool-set for Autodesk MotionBuilder.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="installerConfig"></param>
        /// <returns></returns>
        public DCCToolSet GetInstalledAutodeskMotionBuilderToolSet(IUniversalLog log, IInstallerConfig installerConfig)
        {
            IApplicationInstall mbInstallations =
                installerConfig.Installs.Where(i => i.Name == "AutodeskMotionBuilder").FirstOrDefault();
            if (null == mbInstallations)
                return (DCCToolSet.None);

            foreach (KeyValuePair<String, bool> version in mbInstallations.Versions)
            {
                if (!version.Value)
                    continue;

                MotionBuilderVersion ver = MotionBuilderVersion.None;
                if (Enum.TryParse<MotionBuilderVersion>(version.Key, out ver))
                {
                    if (!AutodeskMotionBuilder.IsVersionInstalled(ver, true))
                        continue;

                    foreach (DCCToolSet ts in Enum.GetValues(typeof(DCCToolSet)).Cast<DCCToolSet>())
                    {
                        if ((DCCToolSet.None == ts) || (DCCToolSet.DoNotInstall == ts))
                            continue;

                        String toolSetPath = ts.ToolPath();
                        String toolPath = Path.Combine(this.m_Config.ToolsRoot, "dcc", toolSetPath,
                            ver.ToolPath(), "plugins", "x64");
                        String expressionToolPath = Path.Combine(this.m_Config.ToolsRoot, "dcc", toolSetPath,
                            ver.ToolPath(), "plugins", "x64", "expressions");
                        
#if false
                        IEnumerable<String> pluginPaths = AutodeskMotionBuilder.GetAdditionalPluginPaths(ver, version.Value);
                        IEnumerable<String> paths = pluginPaths.Select(p => Path.GetFullPath(p));
                        if (pluginPaths.Contains(toolPath) || pluginPaths.Contains(expressionToolPath))
                            return (ts);
#endif               

                        IEnumerable<String> pluginPaths = AutodeskMotionBuilder.GetAdditionalPluginPaths(ver, version.Value);
                        IEnumerable<String> paths = pluginPaths.Where(p => p.Contains(toolSetPath));
                        if (paths.Any())
                            return (ts);
                    }
                }
            }
            return (DCCToolSet.None);
        }

        /// <summary>
        /// Return if motionbuilder python startup path is set.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="installerConfig"></param>
        /// <returns></returns>
        public bool GetInstalledAutodeskMotionBuilderScripts(IUniversalLog log, IInstallerConfig installerConfig)
        {
            IApplicationInstall mbInstallations =
                installerConfig.Installs.Where(i => i.Name == "AutodeskMotionBuilder").FirstOrDefault();
            if (null == mbInstallations)
                return false;

            foreach (KeyValuePair<String, bool> version in mbInstallations.Versions)
            {
                if (!version.Value)
                    continue;

                MotionBuilderVersion ver = MotionBuilderVersion.None;
                if (Enum.TryParse<MotionBuilderVersion>(version.Key, out ver))
                {
                    if (!AutodeskMotionBuilder.IsVersionInstalled(ver, true))
                        continue;

                    String pythonPath = AutodeskMotionBuilder.GetPythonStartupPath(ver, version.Value);
                    return (pythonPath != "");
                }
            }
            return false;
        }

        /// <summary>
        /// Install Autodesk Mudbox.
        /// </summary>
        /// <param name="installerConfig"></param>
        /// <param name="toolSet"></param>
        /// <returns></returns>
        public bool InstallAutodeskMudbox(IUniversalLog log, IInstallerConfig installerConfig, DCCToolSet toolSet)
        {
            IApplicationInstall mudboxInstallations =
                installerConfig.Installs.Where(i => i.Name == "AutodeskMudbox").FirstOrDefault();
            if (null == mudboxInstallations)
                return (true);

            foreach (KeyValuePair<String, bool> version in mudboxInstallations.Versions)
            {
                if (!version.Value)
                    continue;

                MudboxVersion ver = MudboxVersion.None;
                if (Enum.TryParse<MudboxVersion>(version.Key, out ver))
                {
                    if (!AutodeskMudbox.IsVersionInstalled(ver))
                        continue;

                    if (DCCToolSet.None == toolSet)
                    {
                        // Set default paths.
                        AutodeskMudbox.SetAdditionalPluginPaths(ver, Enumerable.Empty<String>());
                    }
                    else
                    {
                        String toolPath = Path.Combine(this.m_Config.ToolsRoot, "techart",
                            "dcc", "Mudbox", ver.ToolPath());

                        AutodeskMudbox.SetAdditionalPluginPaths(ver, new String[] { toolPath });
                    }
                }
            }

            return (true);
        }

        /// <summary>
        /// Return the installed tool-set for Autodesk Mudbox.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="installerConfig"></param>
        /// <returns></returns>
        public DCCToolSet GetInstalledAutodeskMudboxToolSet(IUniversalLog log, IInstallerConfig installerConfig)
        {
            IApplicationInstall mudboxInstallations =
                installerConfig.Installs.Where(i => i.Name == "AutodeskMudbox").FirstOrDefault();
            if (null == mudboxInstallations)
                return (DCCToolSet.None);

            foreach (KeyValuePair<String, bool> version in mudboxInstallations.Versions)
            {
                if (!version.Value)
                    continue;

                MudboxVersion ver = MudboxVersion.None;
                if (Enum.TryParse<MudboxVersion>(version.Key, out ver))
                {
                    if (!AutodeskMudbox.IsVersionInstalled(ver))
                        continue;

                    foreach (DCCToolSet ts in Enum.GetValues(typeof(DCCToolSet)).Cast<DCCToolSet>())
                    {
                        if ((DCCToolSet.None == ts) || (DCCToolSet.DoNotInstall == ts))
                            continue;

                        IEnumerable<String> pluginPaths = AutodeskMudbox.GetAdditionalPluginPaths(ver);
                        if (pluginPaths.Any())
                            return (ts);
                    }
                }
            }
            return (DCCToolSet.None);
        }

        /// <summary>
        /// Generate environment from current configuration object.
        /// </summary>
        /// <param name="defaultBranch">User-defined default branch.</param>
        /// <returns></returns>
        public IDictionary<String, String> GenerateEnvironment(String userDefaultBranch)
        {
            Debug.Assert(this.m_Config.Project.Branches.ContainsKey(userDefaultBranch),
                String.Format("User-defined default branch {0} is not available.", userDefaultBranch));
            if (!this.m_Config.Project.Branches.ContainsKey(userDefaultBranch))
                throw (new ArgumentException(String.Format("User-defined default branch {0} is not available.", userDefaultBranch), "defaultBranch"));

            IDictionary<String, String> env = new Dictionary<String, String>();

            env.Add("RS_PROJECT", this.m_Config.Project.Name);
            env.Add("RS_PROJROOT", this.m_Config.Project.Root);
            env.Add("RS_TOOLSROOT", this.m_Config.ToolsRoot);

            IBranch userBranch = this.m_Config.Project.Branches[userDefaultBranch];
            env.Add("RS_BUILDBRANCH", userBranch.Build);
            env.Add("RS_CODEBRANCH", userBranch.Code);
            env.Add("RS_SCRIPTBRANCH", userBranch.Script);
            env.Add("RAGE_DIR", userBranch.RageCode);

            env.Add("RUBYOPT", "rubygems");
            env.Add("RUBYLIB", System.IO.Path.Combine(this.m_Config.ToolsRoot, "lib"));
            
            String thirdPartyDir = Path.Combine(Path.GetPathRoot(this.m_Config.Project.Root),
                "3rdparty", "dev");
            env.Add("RAGE_3RDPARTY", thirdPartyDir);

            return (env);
        }

        /// <summary>
        /// Set environment up for tools location.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="userDefaultBranch"></param>
        /// <returns></returns>
        public bool SetEnvironment(IUniversalLog log, String userDefaultBranch)
        {
            // Get the current value of tools root.
            String previousToolsRoot = Environment.GetEnvironmentVariable("RS_TOOLSROOT", EnvironmentVariableTarget.User);
            if (previousToolsRoot != null)
            {
                previousToolsRoot = Path.GetFullPath(previousToolsRoot);
            }

            // Update all the environment variables.
            IDictionary<String, String> env = GenerateEnvironment(userDefaultBranch);

            foreach (KeyValuePair<String, String> entry in env)
            {
                String var = Environment.GetEnvironmentVariable(entry.Key, EnvironmentVariableTarget.User);

                if (0 != String.Compare(var, entry.Value))
                {
                    SetUserEnvironmentVariable(entry.Key, entry.Value);
                }
            }

            // Handle %PATH% environment setting; ensuring we remove any 
            // conflicts from other tools locations.
            String pathEnv = Environment.GetEnvironmentVariable("PATH", EnvironmentVariableTarget.User);
            List<String> pathElemsNew = new List<String>();

            if (!String.IsNullOrEmpty(pathEnv))
            {
                String[] pathElems = pathEnv.Split(new char[] { ';' },
                    StringSplitOptions.RemoveEmptyEntries);

                foreach (String pathElem in pathElems)
                {
                    // Strip out quotation marks because Path.Combine can't handle them.
                    String strippedQuotes = pathElem.Replace("\"", "");
                    String pathElemStrip = strippedQuotes;

                    try
                    {
                        pathElemStrip = Path.GetFullPath(strippedQuotes);
                    }
                    catch (System.Exception)
                    {
                    	// Ignore exceptions.
                    }

                    if (previousToolsRoot != null && pathElemStrip.StartsWith(previousToolsRoot))
                    {
                        pathElemsNew.Add(pathElemStrip.Replace(previousToolsRoot, this.m_Config.ToolsRoot));
                    }
                    else
                    {
                        pathElemsNew.Add(pathElem);
                    }
                }
            }

            Debug.Assert(String.Empty != this.m_Config.ToolsRoot, "RS_TOOLSROOT is empty!  Internal error.");
            pathElemsNew.Add(Path.Combine(this.m_Config.ToolsRoot, "bin"));
            pathElemsNew.Add(Path.Combine(this.m_Config.ToolsRoot, "bin", "ruby", "bin"));

            this.m_envPath = String.Join(";", pathElemsNew.Distinct());
            SetUserEnvironmentVariable("PATH", this.m_envPath);

            // Broadcast that the environment variables have changed.
            int dwReturnValue;
            int result = User32.SendMessageTimeout((IntPtr)WindowHandle.BROADCAST, (uint)WindowMessage.SETTINGCHANGE,
                IntPtr.Zero, "Environment", SendMessageTimeoutFlags.AbortIfHung, 5000, out dwReturnValue);

            return (true);
        }

        /// <summary>
        /// Creates the local switch batch file which is used when running tools from a different branch to
        /// the one that is currently installed.
        /// </summary>
        /// <param name="userDefaultBranch"></param>
        /// <returns></returns>
        public bool CreateLocalSwitchBatchFile(String userDefaultBranch)
        {
            try
            {
                String switchFilename = System.IO.Path.Combine(this.m_Config.ToolsRoot, "script\\local\\switchenv.bat");
                String switchDir = Path.GetDirectoryName(switchFilename);

                // Create the directory if it doesn't exist.
                if (Directory.Exists(switchDir) == false)
                {
                    Directory.CreateDirectory(switchDir);
                }

                FileStream batchFile = File.Open(switchFilename, FileMode.Create, FileAccess.Write);
                StreamWriter batchWriter = new StreamWriter(batchFile);

                batchWriter.WriteLine("@echo off");
                batchWriter.WriteLine("SET RS_PROJECT={0}", this.m_Config.Project.Name);
                batchWriter.WriteLine("SET RS_PROJROOT={0}", this.m_Config.Project.Root);
                batchWriter.WriteLine("SET RS_TOOLSROOT={0}", this.m_Config.ToolsRoot);

                IBranch userBranch = this.m_Config.Project.Branches[userDefaultBranch];
                batchWriter.WriteLine("SET RS_BUILDBRANCH={0}", userBranch.Build);
                batchWriter.WriteLine("SET RS_CODEBRANCH={0}", userBranch.Code);
                batchWriter.WriteLine("SET RS_SCRIPTBRANCH={0}", userBranch.Script);
                batchWriter.WriteLine("SET RAGE_DIR={0}", userBranch.RageCode);

                batchWriter.WriteLine("SET RUBYLIB={0}", System.IO.Path.Combine(this.m_Config.ToolsRoot, "lib"));

                batchWriter.WriteLine("IF '%1'=='nopath' GOTO Continue");
                batchWriter.WriteLine("SET PATH={0};%PATH%", this.m_envPath);
                batchWriter.WriteLine(":Continue");
                batchWriter.WriteLine("");

                batchWriter.Close();
                batchFile.Close();
            }
            catch (System.Exception)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Restart Windows Explorer and Command Prompt processes (to pick up 
        /// new environment).
        /// </summary>
        /// <returns></returns>
        public bool RestartProcesses()
        {
            // If the user doesn't have the "Launch folder windows in a separate process" option set
			// then they we will need to set them and ask them to reboot.
            if (!RSG.Interop.Microsoft.WindowsExplorer.IsSeparateProcessesSet())
                return (true);

            // If  we have a separate process for explorer windows, close them all off and kill the process.
            // This ensures that the environment variables are properly set when a new window is opened.
            RSG.Interop.Microsoft.WindowsExplorer.CloseAllTopLevelWindows();
            int taskbarProcessId = RSG.Interop.Microsoft.WindowsExplorer.GetTaskbarProcessId();

            Process[] processes = Process.GetProcessesByName("explorer");
            foreach (Process process in processes)
            {
                if (process.Id != taskbarProcessId)
                {
                    process.Kill();
                }
            }

            return (true);
        }

        /// <summary>
        /// Forces max to rebuild the menus by setting the "RebuildMenus" flag inside the
        /// RS.ini file.
        /// </summary>
        /// <returns></returns>
        public void ForceMaxMenuRebuild(IUniversalLog log)
        {
            string filepath = System.Environment.GetEnvironmentVariable("LocalAppData");
            filepath = Path.Combine(filepath, "Autodesk", "3dsMax", "Rs3dsMax.ini");

            if (!File.Exists(filepath))
            {
                String error = String.Format("Unable to write Autodesk 3ds Max settings.  The file {0} does not exist.",
                    filepath);
                log.ErrorCtx(LOG_CTX, error);
                return;
            }

            IniFile file = IniFile.Load(filepath);
            file.SetValue("RebuildMenus", "RsSettings", "true");
            file.Save(filepath, IniFileSaveOption.None);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Save our configured settings to disk.
        /// </summary>
        /// <returns></returns>
        private bool SaveSettings(IUniversalLog log)
        {
            // Save TOOLSCONFIG data into project root directory.
            String configurationFilename = Path.Combine(this.m_Config.CoreProject.Root, ".tools");
            using (FileStream stream = new FileStream(configurationFilename, FileMode.Create, FileAccess.Write))
            using (StreamWriter writer = new StreamWriter(stream))
            {
                writer.WriteLine("RS_TOOLSROOT={0}", this.m_Config.ToolsRoot);
            }

            // Only create the P4CONFIG files if version control is enabled.
            if (this.m_Config.VersionControlEnabled)
            {
                using (P4 p4 = new P4())
                {
                    p4.Port = this.m_Config.VersionControlSettings.First().Server.ToString();
                    p4.User = this.m_Config.VersionControlSettings.First().Username;
                    p4.Client = this.m_Config.VersionControlSettings.First().Workspace;
                    p4.Connect();

                    String userSysConfig = System.Environment.GetEnvironmentVariable("P4CONFIG");
                    String config = p4.Run_Set("P4CONFIG");
                    if (!String.IsNullOrEmpty(userSysConfig))
                    {
                        log.WarningCtx(LOG_CTX, "P4CONFIG set as user/system Environment Variable: {0}.  Syncing Perforce registry value.", userSysConfig);
                        p4.Run_Set("P4CONFIG", userSysConfig);
                        config = userSysConfig;
                    }

                    if (String.IsNullOrEmpty(config))
                    {
                        log.MessageCtx(LOG_CTX, "P4CONFIG not set; initialising to default.");
                        config = ".p4config";
                        p4.Run_Set("P4CONFIG", config);
                    }
                    else
                    {
                        int colon = config.IndexOfAny(new char[] { ':', '/', '\\' });

                        if (colon != -1)
                        {
                            log.ErrorCtx(LOG_CTX, "P4CONFIG set to {0} - it has path info in it. That's crazy. Aborting", config);
                            throw new Exception("P4CONFIG contains path characters, aborting install");
                        }
                    }

                    // Write out a project P4CONFIG file containing the Perforce
                    // options the user has specified in the installer to all unique root paths.
                    HashSet<string> uniqueRoots = new HashSet<string>();
                    uniqueRoots.Add(this.m_Config.Project.Root.ToLower());
                    if (this.m_Config.DLCProjects != null)
                    {
                        foreach (IProject project in this.m_Config.DLCProjects.Values)
                        {
                            // Used to find the root folder of an arbitrary path without going
                            // through a splitting the string up.
                            DirectoryInfo parent = new DirectoryInfo(project.Root);
                            DirectoryInfo root = null;
                            while (parent != null && parent.Parent != null)
                            {
                                root = parent;
                                parent = parent.Parent;
                            }

                            if (root != null && root.FullName != null)
                            {
                                uniqueRoots.Add(root.FullName.ToLower());
                            }
                        }
                    }

                    foreach (string uniqueRoot in uniqueRoots)
                    {
                        string filepath = System.IO.Path.Combine(uniqueRoot, config);
                        if (!Directory.Exists(uniqueRoot))
                        {
                            Directory.CreateDirectory(uniqueRoot);
                        }

                        List<String> lines = new List<String>();
                        lines.Add(String.Format("P4CLIENT={0}", p4.Client));
                        lines.Add(String.Format("P4PORT={0}", p4.Port));
                        lines.Add(String.Format("P4USER={0}", p4.User));
                        System.IO.File.WriteAllLines(filepath, lines);
                    }
                }
            }

            this.m_Config.InstallTime = DateTime.UtcNow;
            return (this.m_Config.SaveLocal());
        }

        /// <summary>
        /// Sets an user environment variable without broadcasting the change.
        /// </summary>
        /// <param name="variable">The environment variable to set.</param>
        /// <param name="value">The variables new value.</param>
        private void SetUserEnvironmentVariable(String variable, String value)
        {
            using (RegistryKey environmentKey = Registry.CurrentUser.OpenSubKey("Environment", true))
            {
                Debug.Assert(environmentKey != null, @"HKCU\Environment is missing!");
                if (environmentKey != null)
                {
                    if (value == null)
                    {
                        environmentKey.DeleteValue(variable, false);
                    }
                    else
                    {
                        environmentKey.SetValue(variable, value);
                    }
                }
            }
        }

        /// <summary>
        /// Creates a start menu shortcut in the folder Rockstar Games (Project Name).
        /// </summary>
        /// <param name="name">Shortcut name</param>
        /// <param name="path">Exe path</param>
        /// <param name="args">Arguments</param>
        private void CreateStartMenuShortcut(string name, string path, string args)
        {       
            if (!Directory.Exists(m_appStartMenuPath))
                Directory.CreateDirectory(m_appStartMenuPath);
            string shortcutLocation = Path.Combine(m_appStartMenuPath, name + ".lnk");
            IWshRuntimeLibrary.WshShell shell = new IWshRuntimeLibrary.WshShell();
            IWshRuntimeLibrary.IWshShortcut shortcut = (IWshRuntimeLibrary.IWshShortcut)shell.CreateShortcut(shortcutLocation);

            shortcut.Description = name;
            shortcut.TargetPath = path;
            shortcut.WorkingDirectory = Path.GetDirectoryName(path);
            shortcut.Arguments = args;
            shortcut.Save();
        }
        #endregion // Private Methods
    }

} // RSG.Installer namespace
