﻿using System;
using System.Linq;
using System.Reflection;
using RSG.Base.Attributes;

namespace RSG.Installer
{

    /// <summary>
    /// DCC Package Tool Set enumeration.
    /// </summary>
    public enum DCCToolSet
    {
        [FieldDisplayName("Do Not Install")]
        DoNotInstall,

        [FieldDisplayName("None")]
        None,

        [FieldDisplayName("Current")]
        [ToolPath("current")]
        Current,
    }

    /// <summary>
    /// DCCToolSet Enumeration Extension methods.
    /// </summary>
    public static class DCCToolSetExtensions
    {
        /// <summary>
        /// Return DCC Tool Set friendly-name.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static String GetDisplayName(this DCCToolSet version)
        {
            Type type = version.GetType();

            FieldInfo fi = type.GetField(version.ToString());
            FieldDisplayNameAttribute[] attributes =
                fi.GetCustomAttributes(typeof(FieldDisplayNameAttribute), false) as FieldDisplayNameAttribute[];

            FieldDisplayNameAttribute attribute = attributes.FirstOrDefault();
            if (null != attribute)
                return (attribute.DisplayName);
            else
                throw (new ArgumentException("DCC Tool Set has no FieldDisplayName defined."));
        }

        /// <summary>
        /// Return DCC Tool Set path.
        /// </summary>
        /// <param name="version"></param>
        /// <returns></returns>
        public static String ToolPath(this DCCToolSet version)
        {
            Type type = version.GetType();

            FieldInfo fi = type.GetField(version.ToString());
            ToolPathAttribute[] attributes =
                fi.GetCustomAttributes(typeof(ToolPathAttribute), false) as ToolPathAttribute[];

            ToolPathAttribute attribute = attributes.FirstOrDefault();
            if (null != attribute)
                return (attribute.Path);
            else
                throw (new ArgumentException("DCC Tool Set has no Tool Path defined."));
        }
    }

} // RSG.Installer namespace
