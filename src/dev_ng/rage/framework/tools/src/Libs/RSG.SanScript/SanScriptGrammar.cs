﻿namespace RSG.SanScript
{
    using Irony.Parsing;

    /// <summary>
    /// Specifies the grammer of the SanSctipt scripting language to irony.
    /// </summary>
    /// <remarks>
    /// The grammar is built up of one or more script statements inside a single script block.
    /// A script statement can be any declaration, the global section, any function definition,
    /// all loops and logic gates, and all calls.
    /// </remarks>
    [Language("SanScript", "1.0", "SanScript scripting language")]
    public class SanScriptGrammar : Grammar
    {
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SanScriptGrammar"/> class.
        /// </summary>
        public SanScriptGrammar()
            : base(false)
        {
            NonGrammarTerminals.Add(
                new CommentTerminal(
                    "SingleLineComment",
                    "//",
                    "\r",
                    "//",
                    "\n",
                    "\u2085",
                    "\u2028",
                    "\u2029"));

            NonGrammarTerminals.Add(new CommentTerminal("MultiLineComment", "/*", "*/"));

            #region Terminals
            // IF ELSE ELIF ENDIF WHILE ENDWHILE REPEAT ENDREPEAT SCRIPT ENDSCRIPT RETURN EXIT ENDPROC ENDFUNC
            KeyTerm IF = this.ToTerm("IF");
            KeyTerm ELSE = this.ToTerm("ELSE");
            KeyTerm ELIF = this.ToTerm("ELIF");
            KeyTerm ENDIF = this.ToTerm("ENDIF");
            KeyTerm WHILE = this.ToTerm("WHILE");
            KeyTerm ENDWHILE = this.ToTerm("ENDWHILE");
            KeyTerm REPEAT = this.ToTerm("REPEAT");
            KeyTerm ENDREPEAT = this.ToTerm("ENDREPEAT");
            KeyTerm SCRIPT = this.ToTerm("SCRIPT");
            KeyTerm ENDSCRIPT = this.ToTerm("ENDSCRIPT");
            KeyTerm RETURN = this.ToTerm("RETURN");
            KeyTerm EXIT = this.ToTerm("EXIT");
            KeyTerm ENDPROC = this.ToTerm("ENDPROC");
            KeyTerm ENDFUNC = this.ToTerm("ENDFUNC");

            // AND OR NOT GE LE NE LSH RSH FALLTHRU FOR FORWARD ENDFOR TO STEP TYPEDEF
            KeyTerm AND = this.ToTerm("AND", "And Operator");
            KeyTerm OR = this.ToTerm("OR", "Or Operator");
            KeyTerm NOT = this.ToTerm("!", "Not Equal To Operator");
            KeyTerm GE = this.ToTerm(">=", "Greater Than Or Equal To Operator");
            KeyTerm LE = this.ToTerm("<=", "Less Than Or Equal To Operator");
            KeyTerm LSH = new KeyTerm("<<", "left_vector_bracket");
            //LSH.SetFlag(TermFlags.IsPunctuation, true);
            KeyTerm RSH = new KeyTerm(">>", "right_vector_bracket");
            //RSH.SetFlag(TermFlags.IsPunctuation, true);
            KeyTerm FALLTHRU = this.ToTerm("FALLTHRU");
            KeyTerm FOR = this.ToTerm("FOR");
            KeyTerm FORWARD = this.ToTerm("FORWARD");
            KeyTerm ENDFOR = this.ToTerm("ENDFOR");
            KeyTerm TO = this.ToTerm("TO");
            KeyTerm STEP = this.ToTerm("STEP");
            KeyTerm TYPEDEF = this.ToTerm("TYPEDEF");

            // PLUS_TIME MINUS_TIME STRLIT USING CATCH THROW CALL
            KeyTerm PLUS_TIME = this.ToTerm("+@", "Plus Time Operator");
            KeyTerm MINUS_TIME = this.ToTerm("-@", "Minus Time Operator");
            StringLiteral STRLIT = new StringLiteral("StringLiteral", "\"");
            KeyTerm USING = this.ToTerm("USING");
            KeyTerm CATCH = this.ToTerm("CATCH");
            KeyTerm THROW = this.ToTerm("THROW");
            KeyTerm CALL = this.ToTerm("CALL");

            // TYPENAME FUNCNAME PROCNAME INTLIT FLOATLIT NEWSYM VAR_REF FIELD TRUE FALSE LABEL
            //IdentifierTerminal FUNCNAME = new IdentifierTerminal("FUNCNAME");
            //IdentifierTerminal PROCNAME = new IdentifierTerminal("PROCNAME");
            NumberLiteral INTLIT = new NumberLiteral("INTLIT");
            NumberLiteral FLOATLIT = new NumberLiteral("FLOATLIT");
            IdentifierTerminal VAR_REF = new IdentifierTerminal("Identifier");
            IdentifierTerminal FIELD = new IdentifierTerminal("FIELD");
            KeyTerm TRUE = this.ToTerm("TRUE");
            KeyTerm FALSE = this.ToTerm("FALSE");
            KeyTerm LABEL = this.ToTerm("LABEL");

            // SWITCH ENDSWITCH CASE DEFAULT BREAK GOTO SCOPE ENDSCOPE
            KeyTerm SWITCH = this.ToTerm("SWITCH");
            KeyTerm ENDSWITCH = this.ToTerm("ENDSWITCH");
            KeyTerm CASE = this.ToTerm("CASE");
            KeyTerm DEFAULT = this.ToTerm("DEFAULT");
            KeyTerm BREAK = this.ToTerm("BREAK");
            KeyTerm GOTO = this.ToTerm("GOTO");
            KeyTerm SCOPE = this.ToTerm("SCOPE");
            KeyTerm ENDSCOPE = this.ToTerm("ENDSCOPE");

            // CONST_INT CONST_FLOAT FUNC PROC NATIVE GLOBALS ENDGLOBALS ENUM ENDENUM ENUMLIT STRUCT ENDSTRUCT NL
            KeyTerm CONST_INT = this.ToTerm("CONST_INT");
            KeyTerm CONST_FLOAT = this.ToTerm("CONST_FLOAT");
            KeyTerm FUNC = this.ToTerm("FUNC");
            KeyTerm PROC = this.ToTerm("PROC");
            KeyTerm NATIVE = this.ToTerm("NATIVE");
            KeyTerm GLOBALS = this.ToTerm("GLOBALS");
            KeyTerm ENDGLOBALS = this.ToTerm("ENDGLOBALS");
            KeyTerm ENUM = this.ToTerm("ENUM");
            KeyTerm ENDENUM = this.ToTerm("ENDENUM");
            KeyTerm STRUCT = this.ToTerm("STRUCT");
            KeyTerm ENDSTRUCT = this.ToTerm("ENDSTRUCT");
            NewLineTerminal NL = new NewLineTerminal("NL");

            // PLUSPLUS MINUSMINUS PLUS_EQ MINUS_EQ TIMES_EQ DIVIDE_EQ COUNT_OF ENUM_TO_INT INT_TO_ENUM SIZE_OF
            KeyTerm PLUSPLUS = new KeyTerm("++", "plus_plus");
            KeyTerm MINUSMINUS = new KeyTerm("--", "minus_minus");
            KeyTerm PLUS_EQ = new KeyTerm("+=", "plus_equals");
            KeyTerm MINUS_EQ = new KeyTerm("-=", "minus_equals");
            KeyTerm TIMES_EQ = new KeyTerm("*=", "multiply_equals");
            KeyTerm DIVIDE_EQ = new KeyTerm("/=", "divide_equals");
            KeyTerm COUNT_OF = this.ToTerm("COUNT_OF");
            KeyTerm ENUM_TO_INT = this.ToTerm("ENUM_TO_INT");
            KeyTerm INT_TO_ENUM = this.ToTerm("INT_TO_ENUM");
            KeyTerm SIZE_OF = this.ToTerm("SIZE_OF");

            // AND_EQ OR_EQ XOR_EQ
            KeyTerm AND_EQ = new KeyTerm("&=", "and_equals");
            KeyTerm OR_EQ = new KeyTerm("|=", "or_equals");
            KeyTerm XOR_EQ = new KeyTerm("^=", "xor_equals");

            // NATIVE_TO_INT INT_TO_NATIVE HASH DEBUGONLY VARARGS VARARGS1 VARARGS2 VARARGS3 STRICT_ENUM HASH_ENUM STRICT_HASH_ENUM TWEAK_INT TWEAK_FLOAT ANDALSO ORELSE
            KeyTerm NATIVE_TO_INT = this.ToTerm("NATIVE_TO_INT");
            KeyTerm INT_TO_NATIVE = this.ToTerm("INT_TO_NATIVE");
            KeyTerm HASH = this.ToTerm("HASH");
            KeyTerm DEBUGONLY = this.ToTerm("DEBUGONLY");
            KeyTerm VARARGS = this.ToTerm("VARARGS");
            KeyTerm VARARGS1 = this.ToTerm("VARARGS1");
            KeyTerm VARARGS2 = this.ToTerm("VARARGS2");
            KeyTerm VARARGS3 = this.ToTerm("VARARGS3");
            KeyTerm STRICT_ENUM = this.ToTerm("STRICT_ENUM");
            KeyTerm HASH_ENUM = this.ToTerm("HASH_ENUM");
            KeyTerm STRICT_HASH_ENUM = this.ToTerm("STRICT_HASH_ENUM");
            KeyTerm TWEAK_INT = this.ToTerm("TWEAK_INT");
            KeyTerm TWEAK_FLOAT = this.ToTerm("TWEAK_FLOAT");
            KeyTerm ANDALSO = this.ToTerm("ANDALSO");
            KeyTerm ORELSE = this.ToTerm("ORELSE");

            // UNUSED_PARAMETER
            KeyTerm UNUSED_PARAMETER = this.ToTerm("UNUSED_PARAMETER");

            NumberLiteral NUMBERLITERAL = new NumberLiteral("NumberLiteral");
            KeyTerm INT = this.ToTerm("INT");
            KeyTerm BOOL = this.ToTerm("BOOL");
            KeyTerm VECTOR = this.ToTerm("VECTOR");
            KeyTerm FLOAT = this.ToTerm("FLOAT");
            KeyTerm STRING = this.ToTerm("STRING");
            KeyTerm NULL = this.ToTerm("NULL");
            KeyTerm LEFT_BRACKET = new KeyTerm("(", "left_bracket");
            LEFT_BRACKET.SetFlag(TermFlags.IsPunctuation, true);
            KeyTerm RIGHT_BRACKET = new KeyTerm(")", "right_bracket");
            RIGHT_BRACKET.SetFlag(TermFlags.IsPunctuation, true);
            KeyTerm COMMA = new KeyTerm(",", "comma");
            COMMA.SetFlag(TermFlags.IsPunctuation, true);

            this.RegisterOperators(6, "==", "!=", "<>");
            this.RegisterOperators(7, "<", ">", "<=", ">=");
            this.RegisterOperators(8, "+@", "-@");

            KeyTerm HASH_IF = ToTerm("#IF");
            HASH_IF.SetFlag(TermFlags.IsKeyword, true);
            KeyTerm HASH_IFDEF = ToTerm("#IFDEF");
            HASH_IFDEF.SetFlag(TermFlags.IsKeyword, true);

            KeyTerm HASH_IFNDEF = ToTerm("#IFNDEF");
            HASH_IFNDEF.SetFlag(TermFlags.IsKeyword, true);

            KeyTerm HASH_ENDIF = ToTerm("#ENDIF");
            HASH_ENDIF.SetFlag(TermFlags.IsKeyword, true);
            #endregion Terminals

            #region Non-Terminals
            NonTerminal notEqualOperators = new NonTerminal("Not Equal Operators");
            NonTerminal usingStatement = new NonTerminal("using_statement", "using statement");
            NonTerminal typeName = new NonTerminal("type_Name", "type name");
            NonTerminal hashIf = new NonTerminal("hashIf");
            NonTerminal program = new NonTerminal("script_block");
            NonTerminal scriptSection = new NonTerminal("script_section");

            NonTerminal optStruct = new NonTerminal("optStruct");
            NonTerminal optDeclList = new NonTerminal("optDeclList");
            NonTerminal declList = new NonTerminal("declList");
            NonTerminal debugFunc = new NonTerminal("debugFunc");
            NonTerminal debugProc = new NonTerminal("debugProc");
            NonTerminal decl = new NonTerminal("decl");

            NonTerminal optBlockIndex = new NonTerminal("optBlockIndex");
            NonTerminal enumOrStrictEnum = new NonTerminal("enum_Or_Strict_Enum");
            NonTerminal stmt = new NonTerminal("script_statement");
            NonTerminal intexpr = new NonTerminal("int_expr");
            NonTerminal floatexpr = new NonTerminal("floatexpr");
            NonTerminal optFormalList = new NonTerminal("optFormalList");
            NonTerminal optFormalOrVarargsList = new NonTerminal("optFormalOrVarargsList");
            NonTerminal formalList = new NonTerminal("formalList");
            NonTerminal formal = new NonTerminal("formal");
            NonTerminal optStep = new NonTerminal("opt_Step");
            NonTerminal enumerantList = new NonTerminal("enumerantList");
            NonTerminal enumerant = new NonTerminal("enumerant");
            NonTerminal structMemberList = new NonTerminal("structMemberList");
            NonTerminal structMember = new NonTerminal("structMember");
            NonTerminal svarDeclList = new NonTerminal("svarDeclList");
            NonTerminal svarDecl = new NonTerminal("svarDecl");
            NonTerminal enumExpr = new NonTerminal("enumExpr");
            NonTerminal lvarDeclList = new NonTerminal("lvarDeclList");
            NonTerminal gvarDeclList = new NonTerminal("gvarDeclList");
            NonTerminal lvarDecl = new NonTerminal("lvarDecl");
            NonTerminal optAssignment = new NonTerminal("opt_Assignment");
            NonTerminal gvarDecl = new NonTerminal("gvarDecl");
            NonTerminal foptArrays = new NonTerminal("fopt_Arrays");
            NonTerminal optArraysOrDefaultValue = new NonTerminal("optArraysOrDefaultValue");
            NonTerminal optArrays = new NonTerminal("opt_Arrays");
            NonTerminal optGlobalOrStaticAssignment = new NonTerminal("optGlobalOrStaticAssignment");
            NonTerminal optElses = new NonTerminal("opt_Elses");
            NonTerminal scopedStmtList = new NonTerminal("scoped_script_statement_list");
            NonTerminal optStmtList = new NonTerminal("optional_script_statement_list");
            NonTerminal stmtList = new NonTerminal("script_statement_list");
            NonTerminal primaryExpr = new NonTerminal("primary_Expr");
            NonTerminal argExprList = new NonTerminal("arg_Expr_List");
            NonTerminal exprOrDefaultList = new NonTerminal("expr_or_default_list");
            NonTerminal exprOrDefault = new NonTerminal("expr_or_default");
            NonTerminal lvalue = new NonTerminal("lvalue");
            NonTerminal rvalue = new NonTerminal("rvalue");
            NonTerminal arrayReference = new NonTerminal("array_Reference");
            NonTerminal unaryExpr = new NonTerminal("unary_Expr");
            NonTerminal multExpr = new NonTerminal("mult_Expr");
            NonTerminal addExpr = new NonTerminal("add_Expr");
            NonTerminal relExpr = new NonTerminal("rel_Expr");
            NonTerminal equalityExpr = new NonTerminal("equality_Expr");
            NonTerminal bitAndExpr = new NonTerminal("bit_And_Expr");
            NonTerminal bitXorExpr = new NonTerminal("bit_Xor_Expr");
            NonTerminal bitOrExpr = new NonTerminal("bit_Or_Expr");
            NonTerminal logAndExpr = new NonTerminal("log_And_Expr");
            NonTerminal logOrExpr = new NonTerminal("log_Or_Expr");
            NonTerminal conditionalExpr = new NonTerminal("conditional_Expr");
            NonTerminal expr = new NonTerminal("expr");
            #endregion Non-Terminals

            hashIf.Rule = HASH_IF + VAR_REF;
            hashIf.Rule |= HASH_IFDEF + VAR_REF;
            hashIf.Rule |= HASH_IFNDEF + VAR_REF;
            hashIf.Rule |= HASH_IF + "defined" + VAR_REF;
            hashIf.Rule |= HASH_IF + "not" + "defined" + VAR_REF;

            usingStatement.Rule = USING + STRLIT;

            typeName.Rule = INT;
            typeName.Rule |= BOOL;
            typeName.Rule |= FLOAT;
            typeName.Rule |= VECTOR;
            typeName.Rule |= STRING;

            notEqualOperators.Rule = "!=";
            notEqualOperators.Rule |= "<>";

            optStruct.Rule = LEFT_BRACKET + typeName + VAR_REF + RIGHT_BRACKET;
            optStruct.Rule |= Empty;

            optDeclList.Rule = declList;
            optDeclList.Rule |= Empty;

            declList.Rule = declList + decl;
            declList.Rule |= decl;

            debugFunc.Rule = DEBUGONLY + FUNC;
            debugFunc.Rule |= FUNC;

            debugProc.Rule = DEBUGONLY + PROC;
            debugProc.Rule |= PROC;

            decl.Rule = typeName + gvarDeclList;
            decl.Rule |= VAR_REF + gvarDeclList;
            decl.Rule |= GLOBALS + optBlockIndex;
            decl.Rule |= ENDGLOBALS;
            decl.Rule |= FORWARD + enumOrStrictEnum + VAR_REF;
            decl.Rule |= FORWARD + enumOrStrictEnum + typeName;
            decl.Rule |= FORWARD + STRUCT + VAR_REF;
            decl.Rule |= FORWARD + STRUCT + typeName;
            decl.Rule |= enumOrStrictEnum + VAR_REF + enumerantList + ENDENUM;
            decl.Rule |= enumOrStrictEnum + typeName + enumerantList + ENDENUM;
            decl.Rule |= STRUCT + VAR_REF + structMemberList + ENDSTRUCT;
            decl.Rule |= STRUCT + typeName + structMemberList + ENDSTRUCT;
            decl.Rule |= NATIVE + debugFunc + typeName + VAR_REF + optFormalOrVarargsList;
            decl.Rule |= NATIVE + debugFunc + VAR_REF + VAR_REF + optFormalOrVarargsList;
            decl.Rule |= NATIVE + debugProc + VAR_REF + optFormalOrVarargsList;
            decl.Rule |= NATIVE + VAR_REF + ":" + typeName;
            decl.Rule |= NATIVE + VAR_REF;
            decl.Rule |= NATIVE + typeName;
            decl.Rule |= NATIVE + typeName + ":" + typeName;
            decl.Rule |= debugFunc + typeName + VAR_REF + optFormalList + optStmtList + ENDFUNC;
            decl.Rule |= debugFunc + VAR_REF + VAR_REF + optFormalList + optStmtList + ENDFUNC;
            decl.Rule |= debugProc + VAR_REF + optFormalList + optStmtList + ENDPROC;
            decl.Rule |= CONST_INT + VAR_REF + expr;
            decl.Rule |= TWEAK_INT + VAR_REF + expr;
            decl.Rule |= TYPEDEF + FUNC + typeName + VAR_REF + optFormalList;
            decl.Rule |= TYPEDEF + FUNC + VAR_REF + VAR_REF + optFormalList;
            decl.Rule |= TYPEDEF + PROC + VAR_REF + optFormalList;
            decl.Rule |= CONST_FLOAT + VAR_REF + floatexpr;
            decl.Rule |= TWEAK_FLOAT + VAR_REF + floatexpr;
            decl.Rule |= GOTO + FALSE;
            decl.Rule |= GLOBALS + optBlockIndex + TRUE;
            decl.Rule |= NATIVE_TO_INT + FALSE;
            decl.Rule |= INT_TO_NATIVE + FALSE;

            scriptSection.Rule = SCRIPT + program + ENDSCRIPT;

            optBlockIndex.Rule = INTLIT;
            optBlockIndex.Rule |= Empty;

            enumOrStrictEnum.Rule = ENUM;
            enumOrStrictEnum.Rule |= HASH_ENUM;
            enumOrStrictEnum.Rule |= STRICT_ENUM;
            enumOrStrictEnum.Rule |= STRICT_HASH_ENUM;

            stmt.Rule = usingStatement;
            stmt.Rule |= decl;
            stmt.Rule |= SCOPE + scopedStmtList + ENDSCOPE;
            stmt.Rule |= lvalue + "=" + expr;
            stmt.Rule |= lvalue + PLUS_EQ + expr;
            stmt.Rule |= lvalue + MINUS_EQ + expr;
            stmt.Rule |= lvalue + TIMES_EQ + expr;
            stmt.Rule |= lvalue + DIVIDE_EQ + expr;
            stmt.Rule |= lvalue + AND_EQ + expr;
            stmt.Rule |= lvalue + OR_EQ + expr;
            stmt.Rule |= lvalue + XOR_EQ + expr;
            stmt.Rule |= lvalue + PLUSPLUS;
            stmt.Rule |= PLUSPLUS + lvalue;
            stmt.Rule |= lvalue + MINUSMINUS;
            stmt.Rule |= MINUSMINUS + lvalue;
            stmt.Rule |= IF + expr + scopedStmtList + optElses + ENDIF;
            stmt.Rule |= WHILE + expr + scopedStmtList + ENDWHILE;
            stmt.Rule |= REPEAT + expr + lvalue + scopedStmtList + ENDREPEAT;
            stmt.Rule |= FOR + lvalue + "=" + expr + TO + expr + optStep + scopedStmtList + ENDFOR;
            stmt.Rule |= SWITCH + expr + scopedStmtList + ENDSWITCH;
            stmt.Rule |= CASE + intexpr;
            stmt.Rule |= CASE + VAR_REF;
            stmt.Rule |= BREAK;
            stmt.Rule |= FALLTHRU;
            stmt.Rule |= DEFAULT;
            stmt.Rule |= VAR_REF + LEFT_BRACKET + argExprList + RIGHT_BRACKET;
            stmt.Rule |= scriptSection;
            stmt.Rule |= CALL + rvalue + LEFT_BRACKET + argExprList + RIGHT_BRACKET;
            stmt.Rule |= typeName + lvarDeclList;
            stmt.Rule |= CONST_INT + VAR_REF + expr;
            stmt.Rule |= EXIT;
            stmt.Rule |= RETURN + expr;
            stmt.Rule |= VAR_REF + ":";
            stmt.Rule |= LABEL + ":";
            stmt.Rule |= GOTO + VAR_REF;
            stmt.Rule |= VAR_REF + VAR_REF;
            stmt.Rule |= GOTO + LABEL;
            stmt.Rule |= THROW;
            stmt.Rule |= THROW + LEFT_BRACKET + expr + RIGHT_BRACKET;
            stmt.Rule |= UNUSED_PARAMETER + LEFT_BRACKET + VAR_REF + RIGHT_BRACKET;
            stmt.Rule |= hashIf + scopedStmtList + HASH_ENDIF;

            intexpr.Rule = INTLIT;
            intexpr.Rule |= "-" + INTLIT;
            intexpr.Rule |= HASH + LEFT_BRACKET + STRLIT + RIGHT_BRACKET;

            floatexpr.Rule = FLOATLIT;
            floatexpr.Rule |= "-" + FLOATLIT;

            optFormalList.Rule = LEFT_BRACKET + formalList + RIGHT_BRACKET;
            optFormalList.Rule |= LEFT_BRACKET + RIGHT_BRACKET;
            optFormalList.Rule |= Empty;

            optFormalOrVarargsList.Rule = optFormalList;
            optFormalOrVarargsList.Rule |= LEFT_BRACKET + VARARGS + RIGHT_BRACKET;
            optFormalOrVarargsList.Rule |= LEFT_BRACKET + VARARGS1 + RIGHT_BRACKET;
            optFormalOrVarargsList.Rule |= LEFT_BRACKET + VARARGS2 + RIGHT_BRACKET;
            optFormalOrVarargsList.Rule |= LEFT_BRACKET + VARARGS3 + RIGHT_BRACKET;

            formalList.Rule = formal + COMMA + formalList;
            formalList.Rule |= formal;

            formal.Rule = typeName + "&" + VAR_REF + foptArrays;
            formal.Rule |= VAR_REF + "&" + VAR_REF + foptArrays;
            formal.Rule |= typeName + VAR_REF + optArraysOrDefaultValue;
            formal.Rule |= VAR_REF + VAR_REF + optArraysOrDefaultValue;
	        formal.Rule |= ENUM_TO_INT + VAR_REF + optArraysOrDefaultValue;
	        formal.Rule |= ENUM_TO_INT + "&" + VAR_REF + foptArrays;
            formal.Rule |= STRUCT + "&" + VAR_REF;

            optStep.Rule = STEP + intexpr;
            optStep.Rule |= Empty;

            enumerantList.Rule = enumerant;
            enumerantList.Rule |= enumerantList + COMMA + enumerant;

            enumerant.Rule = VAR_REF;
            enumerant.Rule |= VAR_REF + "=" + enumExpr;

            structMemberList.Rule = structMember;
            structMemberList.Rule |= structMemberList + structMember;

            structMember.Rule = typeName + svarDeclList;
            structMember.Rule |= VAR_REF + svarDeclList;

            svarDeclList.Rule = svarDeclList + COMMA + svarDecl;
            svarDeclList.Rule |= svarDecl;

            svarDecl.Rule = VAR_REF + optArraysOrDefaultValue;

            enumExpr.Rule = VAR_REF;
            enumExpr.Rule |= INTLIT;
            enumExpr.Rule |= "-" + INTLIT;
            enumExpr.Rule |= enumExpr + "+" + INTLIT;
            enumExpr.Rule |= enumExpr + "+" + VAR_REF;
            enumExpr.Rule |= HASH + LEFT_BRACKET + STRLIT + RIGHT_BRACKET;

            lvarDeclList.Rule = lvarDeclList + COMMA + lvarDecl;
            lvarDeclList.Rule |= lvarDecl;

            gvarDeclList.Rule = gvarDeclList + gvarDecl;
            gvarDeclList.Rule |= gvarDecl;

            lvarDecl.Rule = VAR_REF + optArrays + optAssignment;

            optAssignment.Rule = "=" + expr;
            optAssignment.Rule |= Empty;

            gvarDecl.Rule = this.PreferShiftHere() + VAR_REF + optArrays + optGlobalOrStaticAssignment;

            foptArrays.Rule = "[" + expr + "]" + optArrays;
            foptArrays.Rule |= "[" + "]";
            foptArrays.Rule |= Empty;

            optArraysOrDefaultValue.Rule = optArrays;
            //optArraysOrDefaultValue.Rule |= "=" + enumExpr;
            optArraysOrDefaultValue.Rule |= "=" + floatexpr;
            optArraysOrDefaultValue.Rule |= "=" + TRUE;
            optArraysOrDefaultValue.Rule |= "=" + FALSE;
            optArraysOrDefaultValue.Rule |= "=" + VAR_REF;

            optArrays.Rule = "[" + expr + "]" + optArrays;
            optArrays.Rule |= "[" + "]";
            optArrays.Rule |= Empty;

            optGlobalOrStaticAssignment.Rule = "=" + expr;
            optGlobalOrStaticAssignment.Rule |= Empty;

            // Optional Else Rules
            optElses.Rule = ELSE + scopedStmtList;
            optElses.Rule |= ELIF + expr + scopedStmtList + optElses;
            optElses.Rule |= Empty;

            // Scoped Statement List Rules
            scopedStmtList.Rule = optStmtList;

            // Optional Statement List Rules
            optStmtList.Rule = Empty;
            optStmtList.Rule |= stmtList;

            // Statement List Rules
            stmtList.Rule = stmtList + stmt;
            stmtList.Rule |= stmt;

            // Primary Expression Rules
            primaryExpr.Rule = LEFT_BRACKET + expr + RIGHT_BRACKET;
            primaryExpr.Rule |= LSH + expr + COMMA + expr + COMMA + expr + RSH;
            primaryExpr.Rule |= rvalue;
            primaryExpr.Rule |= INTLIT;
            primaryExpr.Rule |= FLOATLIT;
            primaryExpr.Rule |= STRLIT;
            primaryExpr.Rule |= TRUE;
            primaryExpr.Rule |= FALSE;
            primaryExpr.Rule |= VAR_REF + LEFT_BRACKET + argExprList + RIGHT_BRACKET;
            primaryExpr.Rule |= CALL + rvalue + LEFT_BRACKET + argExprList + RIGHT_BRACKET;
            primaryExpr.Rule |= "&" + VAR_REF;
            primaryExpr.Rule |= CATCH;
            primaryExpr.Rule |= ENUM_TO_INT + LEFT_BRACKET + expr + RIGHT_BRACKET;
            primaryExpr.Rule |= NATIVE_TO_INT + LEFT_BRACKET + expr + RIGHT_BRACKET;
            primaryExpr.Rule |= COUNT_OF + LEFT_BRACKET + arrayReference + RIGHT_BRACKET;
            primaryExpr.Rule |= COUNT_OF + LEFT_BRACKET + VAR_REF + this.PreferShiftHere() + RIGHT_BRACKET;
            primaryExpr.Rule |= SIZE_OF + LEFT_BRACKET + VAR_REF + this.PreferShiftHere() + RIGHT_BRACKET;
            primaryExpr.Rule |= SIZE_OF + LEFT_BRACKET + rvalue + RIGHT_BRACKET;
            primaryExpr.Rule |= INT_TO_ENUM + LEFT_BRACKET + VAR_REF + COMMA + expr + RIGHT_BRACKET;
            primaryExpr.Rule |= INT_TO_NATIVE + LEFT_BRACKET + VAR_REF + COMMA + expr + RIGHT_BRACKET;
            primaryExpr.Rule |= HASH + LEFT_BRACKET + STRLIT + RIGHT_BRACKET;

            // Argument Expression List Rules
            argExprList.Rule = exprOrDefaultList;
            argExprList.Rule |= Empty;

            // Expression Or Default List Rules
            exprOrDefaultList.Rule = exprOrDefault + COMMA + exprOrDefaultList;
            exprOrDefaultList.Rule |= exprOrDefault;

            // Expression Or Default Rules
            exprOrDefault.Rule = expr;
            exprOrDefault.Rule |= DEFAULT;

            // Left Value Rules
            lvalue.Rule = this.PreferShiftHere() + VAR_REF;
            lvalue.Rule |= lvalue + PreferShiftHere() + "." + FIELD;
            lvalue.Rule |= lvalue + PreferShiftHere() + "[" + expr + "]";

            // Right Value Rules
            rvalue.Rule = this.PreferShiftHere() + VAR_REF;
            rvalue.Rule |= rvalue + PreferShiftHere() + "." + FIELD;
            rvalue.Rule |= rvalue + PreferShiftHere() + "[" + expr + "]";

            // Array Reference Rules
            arrayReference.Rule = this.PreferShiftHere() + VAR_REF;
            arrayReference.Rule |= arrayReference + this.PreferShiftHere() + "." + lvalue;
            arrayReference.Rule |= arrayReference + this.PreferShiftHere() + "[" + expr + "]";

            // Unary Expression Rules
            unaryExpr.Rule = primaryExpr;
            unaryExpr.Rule |= NOT + unaryExpr;
            unaryExpr.Rule |= "-" + unaryExpr;

            // Multiply Expression Rules
            multExpr.Rule = unaryExpr;
            multExpr.Rule |= multExpr + "*" + unaryExpr;
            multExpr.Rule |= multExpr + "/" + unaryExpr;
            multExpr.Rule |= multExpr + "%" + unaryExpr;

            // Add Expression Rules
            addExpr.Rule = multExpr;
            addExpr.Rule |= addExpr + "+" + multExpr;
            addExpr.Rule |= addExpr + "-" + multExpr;
            addExpr.Rule |= addExpr + PLUS_TIME + multExpr;
            addExpr.Rule |= addExpr + MINUS_TIME + multExpr;

            // Relationship Expression Rules
            relExpr.Rule = addExpr;
            relExpr.Rule |= relExpr + "<" + addExpr;
            relExpr.Rule |= relExpr + LE + addExpr;
            relExpr.Rule |= relExpr + ">" + addExpr;
            relExpr.Rule |= relExpr + GE + addExpr;

            // Equality Expression Rules
            equalityExpr.Rule = relExpr;
            equalityExpr.Rule |= equalityExpr + "=" + relExpr;
            equalityExpr.Rule |= equalityExpr + notEqualOperators + relExpr;

            // bitAnd Expression Rules
            bitAndExpr.Rule = equalityExpr;
            bitAndExpr.Rule |= bitAndExpr + "&" + equalityExpr;

            // bitXor Expression Rules
            bitXorExpr.Rule = bitAndExpr;
            bitXorExpr.Rule |= bitXorExpr + "^" + bitAndExpr;

            // bitOr Expression Rules
            bitOrExpr.Rule = bitXorExpr;
            bitOrExpr.Rule |= bitOrExpr + "|" + bitXorExpr;

            // logAnd Expression Rules
            logAndExpr.Rule = bitOrExpr;
            logAndExpr.Rule |= logAndExpr + AND + bitOrExpr;
            logAndExpr.Rule |= logAndExpr + ANDALSO + bitOrExpr;

            // logOr Expression Rules
            logOrExpr.Rule = logAndExpr;
            logOrExpr.Rule |= logOrExpr + OR + logAndExpr;
            logOrExpr.Rule |= logOrExpr + ORELSE + logAndExpr;

            // Conditional Expression Rules
            conditionalExpr.Rule = logOrExpr;

            // Expression Rules
            expr.Rule = conditionalExpr;

            program.Rule = this.MakeStarRule(program, stmt);
            this.Root = program;
        }
        #endregion Constructors
    } // RSG.SanScript.SanScriptGrammar {Class}
} // RSG.SanScript {Namespace}
