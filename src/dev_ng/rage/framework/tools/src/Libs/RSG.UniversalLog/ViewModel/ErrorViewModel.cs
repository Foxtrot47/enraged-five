﻿using System.Data;
using RSG.Base.Logging.Universal;

namespace RSG.UniversalLog.ViewModel
{
    /// <summary>
    /// Represent a error component in a universal log file.
    /// </summary>
    public class ErrorViewModel : UniversalLogComponentViewModel
    {
        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="RSG.UniversalLog.ViewModel.ErrorViewModel"/> class.
        /// </summary>
        /// <param name="row">
        /// The data provider for the component.
        /// </param>
        public ErrorViewModel(InMemoryUniversalLogTarget.BufferedMessage message)
            : base(message)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the component type.
        /// </summary>
        public override UniversalLogComponentTypes ComponentType
        {
            get { return UniversalLogComponentTypes.Error; }
        }
        #endregion
    }
}
