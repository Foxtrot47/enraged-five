﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;

namespace RSG.UniversalLog.Controls
{
    /// <summary>
    /// Represents a text box that when empty shows a watermark. 
    /// </summary>
    public class FilterTextBox : Control
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="Watermark"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty WatermarkProperty;

        /// <summary>
        /// Identifies the <see cref="WatermarkTemplate"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty WatermarkTemplateProperty;

        /// <summary>
        /// Identifies the <see cref="WatermarkTemplate"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsTextBoxFocusedProperty;

        /// <summary>
        /// Identifies the <see cref="Text"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty TextProperty;

        /// <summary>
        /// Identifies the <see cref="NoResults"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty NoResultsProperty;

        /// <summary>
        /// Identifies the <see cref="UseRegularExpression"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty UseRegularExpressionProperty;

        /// <summary>
        /// Identifies the <see cref="MatchWholeWords"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty MatchWholeWordsProperty;

        /// <summary>
        /// Identifies the <see cref="MatchCase"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty MatchCaseProperty;

        /// <summary>
        /// Identifies the <see cref="OnlyShowMatching"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty OnlyShowMatchingProperty;

        /// <summary>
        /// Reference to the popup control defined inside the controls template.
        /// </summary>
        private Popup popup;

        /// <summary>
        /// Reference to the toggle button control defined inside the controls template.
        /// </summary>
        private ToggleButton toggleButton;

        /// <summary>
        /// Reference to the textbox control defined inside the controls template.
        /// </summary>
        private TextBox textbox;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="RSG.UniversalLog.Controls.FilterTextBox"/> class.
        /// </summary>
        static FilterTextBox()
        {
            WatermarkProperty =
            DependencyProperty.Register("Watermark", typeof(object),
            typeof(FilterTextBox), new UIPropertyMetadata(null));

            WatermarkTemplateProperty =
            DependencyProperty.Register("WatermarkTemplate", typeof(DataTemplate),
            typeof(FilterTextBox), new UIPropertyMetadata(null));

            IsTextBoxFocusedProperty =
            DependencyProperty.Register("IsTextBoxFocused", typeof(bool),
            typeof(FilterTextBox), new UIPropertyMetadata(false, OnIsTextBoxFocusedChanged));

            TextProperty =
            DependencyProperty.Register("Text", typeof(string),
            typeof(FilterTextBox), new FrameworkPropertyMetadata(string.Empty,
            FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

            NoResultsProperty =
            DependencyProperty.Register("NoResults", typeof(bool),
            typeof(FilterTextBox), new FrameworkPropertyMetadata(false,
            FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

            UseRegularExpressionProperty =
            DependencyProperty.Register("UseRegularExpression", typeof(bool),
            typeof(FilterTextBox), new FrameworkPropertyMetadata(false,
            FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

            MatchWholeWordsProperty =
            DependencyProperty.Register("MatchWholeWords", typeof(bool),
            typeof(FilterTextBox), new FrameworkPropertyMetadata(false,
            FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

            MatchCaseProperty =
            DependencyProperty.Register("MatchCase", typeof(bool),
            typeof(FilterTextBox), new FrameworkPropertyMetadata(false,
            FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

            OnlyShowMatchingProperty =
            DependencyProperty.Register("OnlyShowMatching", typeof(bool),
            typeof(FilterTextBox), new FrameworkPropertyMetadata(false,
            FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(FilterTextBox),
                new FrameworkPropertyMetadata(typeof(FilterTextBox)));
        }

        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="RSG.UniversalLog.Controls.FilterTextBox"/> class.
        /// </summary>
        public FilterTextBox()
        {
        }
        #endregion //Constructors

        #region Properties
        /// <summary>
        /// Gets or sets the actual watermark that is shown to the user when the textbox
        /// is empty.
        /// </summary>
        public object Watermark
        {
            get { return (object)GetValue(WatermarkProperty); }
            set { SetValue(WatermarkProperty, value); }
        }

        /// <summary>
        /// Gets or sets the template that is used to render the watermark on the textbox.
        /// </summary>
        public DataTemplate WatermarkTemplate
        {
            get { return (DataTemplate)GetValue(WatermarkTemplateProperty); }
            set { SetValue(WatermarkTemplateProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the textbox has focus.
        /// </summary>
        public bool IsTextBoxFocused
        {
            get { return (bool)GetValue(IsTextBoxFocusedProperty); }
            set { SetValue(IsTextBoxFocusedProperty, value); }
        }

        /// <summary>
        /// Gets or sets the text value inside the search text box.
        /// </summary>
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether there are no results for this search.
        /// </summary>
        public bool NoResults
        {
            get { return (bool)GetValue(NoResultsProperty); }
            set { SetValue(NoResultsProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether there are no results for this search.
        /// </summary>
        public bool UseRegularExpression
        {
            get { return (bool)GetValue(UseRegularExpressionProperty); }
            set { SetValue(UseRegularExpressionProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether there are no results for this search.
        /// </summary>
        public bool MatchWholeWords
        {
            get { return (bool)GetValue(MatchWholeWordsProperty); }
            set { SetValue(MatchWholeWordsProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether there are no results for this search.
        /// </summary>
        public bool MatchCase
        {
            get { return (bool)GetValue(MatchCaseProperty); }
            set { SetValue(MatchCaseProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the search should just show the matching
        /// results or all results.
        /// </summary>
        public bool OnlyShowMatching
        {
            get { return (bool)GetValue(OnlyShowMatchingProperty); }
            set { SetValue(OnlyShowMatchingProperty, value); }
        }
        #endregion

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.toggleButton = this.GetTemplateChild("PART_SearchDropDownButton") as ToggleButton;
            this.popup = this.GetTemplateChild("PART_Popup") as Popup;
            this.textbox = this.GetTemplateChild("PART_SearchBox") as TextBox;

            if (this.textbox != null)
            {
                this.textbox.LostFocus += OnLostFocus;
                this.textbox.LostKeyboardFocus += this.OnLostFocus;
                this.textbox.LostMouseCapture += this.OnLostMouseCapture;
            }

            if (this.toggleButton != null)
            {
                this.toggleButton.Checked += ToggleButtonChecked;
                this.toggleButton.Unchecked += ToggleButtonUnchecked;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnLostMouseCapture(object sender, MouseEventArgs e)
        {
            this.UncheckToggleButton();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnLostFocus(object sender, RoutedEventArgs e)
        {
            this.IsTextBoxFocused = false;
            this.UncheckToggleButton();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ToggleButtonChecked(object sender, RoutedEventArgs e)
        {
            Mouse.Capture(this, CaptureMode.SubTree);
            this.MouseDown += this.OnMouseDown;

            if (this.textbox != null)
            {
                if (!this.textbox.IsFocused)
                    this.textbox.Focus();
            }

            if (this.popup != null)
                this.popup.IsOpen = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ToggleButtonUnchecked(object sender, RoutedEventArgs e)
        {
            Mouse.Capture(this, CaptureMode.None);
            this.MouseDown -= this.OnMouseDown;

            if (this.popup != null)
                this.popup.IsOpen = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            this.UncheckToggleButton();
        }

        /// <summary>
        /// 
        /// </summary>
        private void UncheckToggleButton()
        {
            if (this.toggleButton == null)
                return;

            this.toggleButton.IsChecked = false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OnIsTextBoxFocusedChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue == true)
            {
                if ((sender as FilterTextBox).textbox != null)
                    (sender as FilterTextBox).textbox.Focus();
            }
        }
        #endregion
    }
}