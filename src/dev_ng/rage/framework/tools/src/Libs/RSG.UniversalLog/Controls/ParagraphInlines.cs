﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Collections;
using System.Collections.Specialized;
using System.Windows.Documents;
using System.Windows.Media;
using System.ComponentModel;
using RSG.UniversalLog.ViewModel;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace RSG.UniversalLog.Controls
{
    /// <summary>
    /// A extension method to bind paragraph inline children to a observable collection.
    /// </summary>
    public class ParagraphExtension
    {
        #region Fields
        /// <summary>
        /// Identifies the ParagraphInlines dependency property.
        /// </summary>
        public static DependencyProperty ParagraphInlinesProperty;

        /// <summary>
        /// Mapping between collections and dependency objects.
        /// </summary>
        private static Dictionary<WeakReference, WeakReference> Collections;

        private static SolidColorBrush ErrorBrush;

        private static SolidColorBrush WarningBrush;

        private static SolidColorBrush MessageBrush;

        private static SolidColorBrush DebugBrush;

        public const string msgFmt = "{0}[{1}]:{2} - {3}";
        #endregion

        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="UniversalLogViewer.ParagraphExtension"/> class.
        /// </summary>
        static ParagraphExtension()
        {
            Collections = new Dictionary<WeakReference, WeakReference>();

            ErrorBrush = new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
            ErrorBrush.Freeze();

            WarningBrush = new SolidColorBrush(Color.FromArgb(255, 128, 128, 0));
            WarningBrush.Freeze();

            MessageBrush = new SolidColorBrush(Color.FromArgb(255, 0, 0, 139));
            MessageBrush.Freeze();

            DebugBrush = new SolidColorBrush(Color.FromArgb(255, 0, 0, 139));
            DebugBrush.Freeze();

            ParagraphInlinesProperty =
            DependencyProperty.RegisterAttached("ParagraphInlines",
            typeof(IEnumerable<UniversalLogComponentViewModel>),
            typeof(ParagraphExtension), new PropertyMetadata(null, OnInlinesChanged));
        }
        #endregion

        #region Methods
        /// <summary>
        /// Sets the ParagraphInlines attached property on the
        /// specified element to the specified value.
        /// </summary>
        /// <param name="element">
        /// The element to set the attached property on.
        /// </param>
        /// <param name="value">
        /// The value to set the attached property to.
        /// </param>
        public static void SetParagraphInlines(DependencyObject element, IEnumerable<UniversalLogComponentViewModel> value)
        {
            element.SetValue(ParagraphInlinesProperty, value);
        }

        /// <summary>
        /// Gets the <see cref="ParagraphInlines"/> attached property for the
        /// specified element.
        /// </summary>
        /// <param name="element">
        /// The element to get the attached property on.
        /// </param>
        /// <returns>
        /// The value of the ParagraphInlines attached property on the specified element.
        /// </returns>
        public static IEnumerable<UniversalLogComponentViewModel> GetParagraphInlines(DependencyObject element)
        {
            return (IEnumerable<UniversalLogComponentViewModel>)element.GetValue(ParagraphInlinesProperty);
        }

        /// <summary>
        /// Gets called when the inline collection changes value.
        /// </summary>
        /// <param name="sender">
        /// The dependency object who sent this event. 
        /// </param>
        /// <param name="e">
        /// The event data.
        /// </param>
        private static void OnInlinesChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (e.OldValue is INotifyCollectionChanged)
            {
                (e.OldValue as INotifyCollectionChanged).CollectionChanged -= ItemsChanged;
            }

            if (!(sender is Paragraph))
                return;

            if (e.NewValue is INotifyCollectionChanged)
            {
                (e.NewValue as INotifyCollectionChanged).CollectionChanged += ItemsChanged;
                Collections.Add(new WeakReference(e.NewValue), new WeakReference(sender));

            }

            ResetInlines(sender as Paragraph);
        }

        /// <summary>
        /// Gets called when the collection that the inline property is bound to changes.
        /// </summary>
        /// <param name="sender">
        /// The collection that changed.
        /// </param>
        /// <param name="e">
        /// The event data.
        /// </param>
        static void ItemsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            Paragraph paragraph = null;
            foreach (KeyValuePair<WeakReference, WeakReference> reference in Collections)
            {
                if (reference.Key.Target != null && reference.Key.Target == sender)
                {
                    paragraph = reference.Value.Target as Paragraph;
                }
            }

            if (paragraph == null)
                return;

            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    OnItemsAdded(e.NewItems, e.NewStartingIndex, paragraph);
                    break;
                case NotifyCollectionChangedAction.Remove:
                    OnItemsRemoved(e.OldItems, paragraph);
                    break;
                case NotifyCollectionChangedAction.Move:
                case NotifyCollectionChangedAction.Replace:
                case NotifyCollectionChangedAction.Reset:
                default:
                    ResetInlines(paragraph);
                    break;
            }
        }

        /// <summary>
        /// Uses the current value that the property is bound to to create the inlines for
        /// the paragraph.
        /// </summary>
        /// <param name="paragraph">
        /// The paragraph the inlines need to be added to.
        /// </param>
        private static void ResetInlines(Paragraph paragraph)
        {
            paragraph.Inlines.Clear();
            IEnumerable<UniversalLogComponentViewModel> value = GetParagraphInlines(paragraph);
            if (value == null)
                return;

            OnItemsAdded(value.ToList(), 0, paragraph);
        }

        /// <summary>
        /// Called with the collection the specified paragraph is bound to has items added.
        /// </summary>
        /// <param name="newItems">
        /// The items that have been added to the collection.
        /// </param>
        /// <param name="paragraph">
        /// The paragraph the collection is bound to.
        /// </param>
        private static void OnItemsAdded(IList newItems, int index, Paragraph paragraph)
        {
            if (newItems == null)
                return;

            int i = 0;
            Inline before = null;
            if (paragraph.Inlines.Count != index)
            {
                foreach (Inline inline in paragraph.Inlines)
                {
                    if (i == index)
                    {
                        before = inline;
                        break;
                    }
                    i++;
                }
            }

            foreach (var newItem in newItems)
            {
                if (!(newItem is UniversalLogComponentViewModel))
                    continue;

                UniversalLogComponentViewModel vm = newItem as UniversalLogComponentViewModel;
                if (vm.ComponentType == UniversalLogComponentTypes.Profiling)
                    continue;

                Paragraph newParagraph = new Paragraph();
                SolidColorBrush foreground = Brushes.Black;
                switch (vm.ComponentType)
                {
                    case UniversalLogComponentTypes.DebugMessage:
                        foreground = DebugBrush;
                        break;
                    case UniversalLogComponentTypes.Error:
                        foreground = ErrorBrush;
                        break;
                    case UniversalLogComponentTypes.Message:
                        foreground = MessageBrush;
                        break;
                    case UniversalLogComponentTypes.Warning:
                        foreground = WarningBrush;
                        break;
                    default:
                        break;
                }

                Run prefix = new Run();
                prefix.Text = string.Format(msgFmt, vm.TimeStamp, vm.Type, vm.Context, "");
                prefix.Foreground = foreground;

                paragraph.Inlines.Add(prefix);
                string[] words = vm.Message.Split(new char[1] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                StringBuilder currentPortion = new StringBuilder(vm.Message.Length);
                bool isHyperLink = false;
                int inlineCount = 0;
                int wordIndex = 0;
                foreach (string word in words)
                {
                    if (word.StartsWith("http://") || word.StartsWith("\"http://"))
                    {
                        if (currentPortion.Length > 0)
                        {
                            Run message = new Run();
                            if (inlineCount > 0)
                                message.Text = string.Format(" {0}", currentPortion.ToString());
                            else
                                message.Text = string.Format("{0}", currentPortion.ToString());

                            message.Foreground = foreground;
                            paragraph.Inlines.Add(message);
                            inlineCount++;
                            currentPortion.Clear();
                        }


                        if (word[0] == '"')
                        {
                            isHyperLink = true;
                            currentPortion.Append(word);
                        }
                        else
                        {
                            Hyperlink link = new Hyperlink();
                            if (inlineCount > 0)
                            {
                                paragraph.Inlines.Add(new Run(" "));
                            }

                            link.Inlines.Add(new Run(word));

                            try
                            {
                                link.NavigateUri = new Uri(word);
                                link.RequestNavigate += (s, e) =>
                                {
                                    Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
                                    e.Handled = true;
                                };
                                paragraph.Inlines.Add(link);
                            }
                            catch (System.UriFormatException)
                            {
                                Run message = new Run();
                                if (inlineCount > 0)
                                    message.Text = string.Format(" {0}", currentPortion.ToString());
                                else
                                    message.Text = string.Format("{0}", currentPortion.ToString());

                                message.Foreground = foreground;
                                paragraph.Inlines.Add(message);
                            }

                            inlineCount++;
                        }
                    }
                    else
                    {
                        if (currentPortion.Length == 0)
                        {
                            currentPortion.Append(word);
                        }
                        else
                        {
                            currentPortion.Append(" " + word);
                        }

                        if (wordIndex == words.Length - 1)
                        {
                            Run message = new Run();
                            if (inlineCount > 0)
                                message.Text = string.Format(" {0}", currentPortion.ToString());
                            else
                                message.Text = string.Format("{0}", currentPortion.ToString());

                            message.Foreground = foreground;
                            paragraph.Inlines.Add(message);
                            inlineCount++;
                            currentPortion.Clear();
                        }
                    }

                    if (isHyperLink && word.EndsWith("\"") || isHyperLink && wordIndex == words.Length - 1)
                    {
                        isHyperLink = false;
                        Hyperlink link = new Hyperlink();
                        link.Inlines.Add(new Run(currentPortion.ToString().Replace("\"", string.Empty).Replace(" ", "%20")));

                        try
                        {
                            string url = currentPortion.ToString().Replace("\"", string.Empty).Replace(" ", "%20");
                            link.NavigateUri = new Uri(url);
                            link.RequestNavigate += (s, e) =>
                            {
                                Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
                                e.Handled = true;
                            };

                            if (inlineCount > 0)
                            {
                                paragraph.Inlines.Add(new Run(" "));
                            }
                            paragraph.Inlines.Add(link);
                        }
                        catch (System.UriFormatException)
                        {
                            Run message = new Run();
                            if (inlineCount > 0)
                                message.Text = string.Format(" {0}", currentPortion.ToString());
                            else
                                message.Text = string.Format("{0}", currentPortion.ToString());

                            message.Foreground = foreground;
                            paragraph.Inlines.Add(message);
                        }

                        inlineCount++;
                        currentPortion.Clear();
                    }

                    wordIndex++;
                }

                paragraph.Inlines.Add(new LineBreak());
            }
        }

        /// <summary>
        /// Called with the collection the specified paragraph is bound to has items removed.
        /// </summary>
        /// <param name="oldItems">
        /// The items that have been removed to the collection.
        /// </param>
        /// <param name="paragraph">
        /// The paragraph the collection is bound to.
        /// </param>
        private static void OnItemsRemoved(IList oldItems, Paragraph paragraph)
        {
            foreach (var oldItem in oldItems)
            {
                if (!(oldItem is UniversalLogComponentViewModel))
                    continue;

                UniversalLogComponentViewModel vm = oldItem as UniversalLogComponentViewModel;
                if (vm.ComponentType == UniversalLogComponentTypes.Profiling)
                    continue;

                string text = string.Format(msgFmt, vm.TimeStamp, vm.Type, vm.Context, vm.Message);
                Inline found = null;
                foreach (Inline inline in paragraph.Inlines)
                {
                    if (!(inline is Run))
                        continue;

                    if ((inline as Run).Text == text)
                    {
                        found = inline;
                        break;
                    }
                }
                if (found == null)
                    continue;

                paragraph.Inlines.Remove(found);
            }
        }
        #endregion
    }
}
