﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;

using RSG.UniversalLog.ViewModel;

namespace RSG.UniversalLog.DefaultViewer
{
    enum ProfileGraphViewSelection
    {
        VIEW_DEFAULT,
        VIEW_CONGLOMERATED
    }

    class DefaultProfileViewerViewModel : INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// Private field for the <see cref="Filenames"/> property.
        /// </summary>
        private List<string> filenames;
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets the collection of universal log component view models that the
        /// view is currently bound to.
        /// </summary>
        public List<UniversalLogComponentViewModel> Components
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the single filename based on the files that is currently loaded by the viewer.
        /// </summary>
        public string Filename
        {
            get
            {
                if (this.Filenames == null || this.Filenames.Count == 0)
                    return null;

                if (this.Filenames.Count == 1)
                    return " - " + this.Filenames[0];

                return string.Format(" - {0} files loaded.", this.Filenames.Count);
            }
        }

        /// <summary>
        /// Gets or sets the filenames of the files that is currently loaded by the viewer.
        /// </summary>
        public List<string> Filenames
        {
            get { return this.filenames; }
            set { this.SetProperty(ref this.filenames, value, "Filename", "StatusText"); }
        }

        #endregion

        #region Constructor

        public DefaultProfileViewerViewModel(List<string> filenames, List<UniversalLogComponentViewModel> viewModels)
        {
            this.filenames = filenames;
            Components = viewModels;

            UniversalLogProfileGraphViewModel.GlobalViewModels = viewModels;  ///TODO: How do I usher model data back through to the viewmodel prior to/during its instantation?
        }

        /// <summary>
        /// Destroys this instance of the
        /// <see cref="UniversalLogViewer.ProfileGraphWindowViewModel"/> class.
        /// </summary>
        ~DefaultProfileViewerViewModel()
        {
            
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Methods

        /// <summary>
        /// Sets the given field to the given value making sure the correct events are fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the property we wish to set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field we wish to set.
        /// </param>
        /// <param name="value">
        /// The value we wish to set the field to.
        /// </param>
        /// <param name="names">
        /// A array of property names that are changed due to this one change.
        /// </param>
        private void SetProperty<T>(ref T field, T value, params string[] names)
        {
            if (EqualityComparer<T>.Default.Equals(field, value))
                return;

            field = value;
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler == null || names == null)
                return;

            foreach (string name in names)
                handler(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Event handler for this window on close.
        /// </summary>
        public void OnClosing()
        {
          
        }

        #endregion
    }
}


