﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Data;
using System.Windows.Threading;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Collections;
using RSG.Base.Editor;
using RSG.Base.Editor.Command;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace RSG.UniversalLog
{
    
    /// <summary>
    /// Universal Log model to store and refresh content in-memory.
    /// </summary>
    public class UniversalLogViewModel : ViewModelBase
    {
        #region Constants
        /// <summary>
        /// Universal log file filter.
        /// </summary>
        private readonly String ULOG_FILTER = "*.ulog";
        #endregion // Constants

        #region Properties and Associated Member Data
        /// <summary>
        /// Absolute path to Universal Logs.
        /// </summary>
        public String Path
        {
            get { return m_sPath; }
            protected set
            {
                SetPropertyValue(value, () => this.Path,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_sPath = (String)newValue;
                        }
                ));
            }
        }
        private String m_sPath;

        /// <summary>
        /// Absolute filenames for Universal Logs.
        /// </summary>
        public ObservableCollection<String> LogFiles
        {
            get { return m_LogFiles; }
            protected set
            {
                SetPropertyValue(value, () => this.LogFiles,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_LogFiles = (ObservableCollection<String>)newValue;
                        }
                ));
            }
        }
        protected ObservableCollection<String> m_LogFiles;
        
        /// <summary>
        /// Log messages collection.
        /// </summary>
        public ObservableDictionary<String, ICollectionView> LogMessages
        {
            get { return m_LogMessages; }
            set
            {
                SetPropertyValue(value, () => this.LogMessages,
                    new PropertySetDelegate(
                        delegate(Object newValue)
                        {
                            m_LogMessages = (ObservableDictionary<String, ICollectionView>)newValue;
                        }
                ));
            }
        }
        protected ObservableDictionary<String, ICollectionView> m_LogMessages;
        #endregion // Properties and Associated Member Data

        #region Member Data
        /// <summary>
        /// File system watcher to reload the universal log files.
        /// </summary>
        private FileSystemWatcher m_watcher;

        /// <summary>
        /// Collections of log messages; deserialised from XML.
        /// </summary>
        private Dictionary<String, ObservableCollection<UniversalLogFileBufferedMessage>> m_logs;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        public UniversalLogViewModel(String path)
        {
            this.Path = (path.Clone() as String);
            this.LogMessages = new ObservableDictionary<String, ICollectionView>();
            m_logs = new Dictionary<String, ObservableCollection<UniversalLogFileBufferedMessage>>();
            m_watcher = new FileSystemWatcher(this.Path, ULOG_FILTER);
            m_watcher.NotifyFilter = NotifyFilters.FileName | NotifyFilters.LastWrite;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Start monitoring for Universal Log file changes.
        /// </summary>
        public void Start()
        {
            // Add our file system watcher event handlers.
            m_watcher.Changed += FileChanged;
            m_watcher.Created += FileChanged;
            m_watcher.Deleted += FileChanged;
            
            m_watcher.EnableRaisingEvents = true;
        }

        /// <summary>
        /// Stop monitoring for Universal Log file changes.
        /// </summary>
        public void Stop()
        {
            m_watcher.EnableRaisingEvents = false;

            // Remove our file system watcher event handlers.
            m_watcher.Changed -= FileChanged;
            m_watcher.Created -= FileChanged;
            m_watcher.Deleted -= FileChanged;
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Initialise this view model from log XML files.
        /// </summary>
        private void Initialise()
        {
            Debug.Assert(Directory.Exists(this.Path), 
                String.Format("Universal Log directory does not exist! ({0}).", this.Path));
            String[] files = Directory.GetFiles(this.Path, ULOG_FILTER);
            foreach (String filename in files)
            {
                try
                {
                    ObservableCollection<UniversalLogFileBufferedMessage> messages =
                        new ObservableCollection<UniversalLogFileBufferedMessage>();
                    messages.AddRange(LoadFile(filename));
                    m_logs.Add(filename, messages);
                    RefreshVM(filename);
                }
                catch (System.Exception ex)
                {
#warning DHM FIX ME: handle this for misformed XML documents.
                }
            }
        }

        /// <summary>
        /// Load a new Universal Log XML document.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private IEnumerable<UniversalLogFileBufferedMessage> LoadFile(String filename)
        {
            XDocument xmlDoc = XDocument.Load(filename);
            IEnumerable<XElement> xmlMessages = xmlDoc.Root.XPathSelectElements("/ulog/*");
            IEnumerable<UniversalLogFileBufferedMessage> messages =
                xmlMessages.Select(msg => new UniversalLogFileBufferedMessage(msg));
            return (messages);
        }

        /// <summary>
        /// 
        /// </summary>
        private void RefreshVM(String filename)
        {
            m_LogMessages[filename] = CollectionViewSource.GetDefaultView(m_logs[filename]);
        }

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FileChanged(Object sender, FileSystemEventArgs e)
        {
            // Reload our message collection for the changed file.
            Log.Log__Debug("ULog Change: {0}.", e.FullPath);
            if (m_logs.ContainsKey(e.FullPath))
            {
                ObservableCollection<UniversalLogFileBufferedMessage> messages =
                    new ObservableCollection<UniversalLogFileBufferedMessage>();
                messages.AddRange(LoadFile(e.FullPath));
                m_logs[e.FullPath].Clear();
                m_logs[e.FullPath].AddRange(messages);
                RefreshVM(e.FullPath);
            }
            else
            {
                ObservableCollection<UniversalLogFileBufferedMessage> messages =
                    new ObservableCollection<UniversalLogFileBufferedMessage>();
                messages.AddRange(LoadFile(e.FullPath));
                m_logs.Add(e.FullPath, messages);
                RefreshVM(e.FullPath);
            }
        }
        #endregion // Event Handlers
        #endregion // Private Methods
    }

} // RSG.UniversalLog namespace
