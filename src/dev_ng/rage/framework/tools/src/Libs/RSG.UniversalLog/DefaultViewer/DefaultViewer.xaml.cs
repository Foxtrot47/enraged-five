﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RSG.UniversalLog.DefaultViewer
{
    /// <summary>
    /// Interaction logic for DefaultViewer.xaml
    /// </summary>
    public partial class DefaultViewer : UserControl
    {
        public DefaultViewer()
        {
            InitializeComponent();
            DefaultViewerViewModel dataContext = new DefaultViewerViewModel();
            dataContext.OwnerControl = this;
            this.DataContext = dataContext;

        }

        #region Events
        public event SelectionChangedEventHandler SelectionChanged;
        #endregion

        private void UniversalLogControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (this.SelectionChanged != null)
                this.SelectionChanged(sender, e);
        }
    }
}
