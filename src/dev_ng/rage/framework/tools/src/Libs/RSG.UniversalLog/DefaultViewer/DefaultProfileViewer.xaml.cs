﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using RSG.UniversalLog.ViewModel;

namespace RSG.UniversalLog.DefaultViewer
{
    /// <summary>
    /// Interaction logic for DefaultProfileViewer.xaml
    /// </summary>
    public partial class DefaultProfileViewer : Window
    {
        #region Fields
        private DefaultProfileViewerViewModel viewModel;
        #endregion

        #region Constructor
        public DefaultProfileViewer(List<string> filenames, List<UniversalLogComponentViewModel> viewModels)
        {
            viewModel = new DefaultProfileViewerViewModel(filenames, viewModels);
            this.DataContext = viewModel;

            InitializeComponent();
        }
        #endregion

        #region Events
        /// <summary>
        /// Event handler on close of this window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DefaultGraphWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            viewModel.OnClosing();
        }
        #endregion
    }
}
