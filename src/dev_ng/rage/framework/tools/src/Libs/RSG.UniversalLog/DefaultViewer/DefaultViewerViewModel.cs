﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.UniversalLog.ViewModel;
using System.Windows.Threading;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Data;
using System.Text.RegularExpressions;
using RSG.UniversalLog.Controls;
using System.IO;
using System.Windows.Controls;
using System.Windows.Media;
using RSG.Base.Logging.Universal;
using RSG.Base.Configuration;

namespace RSG.UniversalLog.DefaultViewer
{
    /// <summary>
    /// </summary>
    public class DefaultViewerViewModel
    {
        #region Fields
        /// <summary>
        /// A generic object used to control the creation of commands from the UI.
        /// </summary>
        private static object sync;

        /// <summary>
        /// The private field for the <see cref="RefreshCommand"/> property.
        /// </summary>
        private SimpleCommand refreshCommand;

        /// <summary>
        /// The private field for the <see cref="EmailCommand"/> property.
        /// </summary>
        private SimpleCommand emailCommand;

        /// <summary>
        /// The private field for the <see cref="ShowProfileCommand"/> property.
        /// </summary>
        private SimpleCommand showProfileCommand;

        /// <summary>
        /// The private field for the <see cref="FindCommand"/> property.
        /// </summary>
        private SimpleCommand findCommand;

        /// <summary>
        /// The private field for the <see cref="CopyCommand"/> property.
        /// </summary>
        private SimpleCommand copyCommand;

        /// <summary>
        /// Private field for the <see cref="Filenames"/> property.
        /// </summary>
        private List<string> filenames;

        /// <summary>
        /// Private field for the <see cref="WordWrap"/> property.
        /// </summary>
        private bool wordWrap;

        /// <summary>
        /// Private field for the <see cref="ShowErrors"/> property.
        /// </summary>
        private bool showErrors;

        /// <summary>
        /// Private field for the <see cref="ShowWarnings"/> property.
        /// </summary>
        private bool showWarnings;

        /// <summary>
        /// Private field for the <see cref="ShowMessages"/> property.
        /// </summary>
        private bool showMessages;

        /// <summary>
        /// Private field for the <see cref="ShowDebugMessages"/> property.
        /// </summary>
        private bool showDebugMessages;

        /// <summary>
        /// Private field for the <see cref="FindBoxHasFocus"/> property.
        /// </summary>
        private bool findBoxHasFocus;

        /// <summary>
        /// Private field for the <see cref="SearchText"/> property.
        /// </summary>
        private string searchText;

        /// <summary>
        /// Private field for the <see cref="NoSearchResults"/> property.
        /// </summary>
        private bool noSearchResults;

        /// <summary>
        /// Private field for the <see cref="UseRegularExpression"/> property.
        /// </summary>
        private bool useRegularExpression;

        /// <summary>
        /// Private field for the <see cref="MatchWholeWords"/> property.
        /// </summary>
        private bool matchWholeWords;

        /// <summary>
        /// Private field for the <see cref="MatchCase"/> property.
        /// </summary>
        private bool matchCase;

        /// <summary>
        /// Private field for the <see cref="MatchCase"/> property.
        /// </summary>
        private List<UniversalLogComponentViewModel> allViewModels;

        /// <summary>
        /// A valid indicting whether the opened file has been sent in a email to the
        /// address group.
        /// </summary>
        private bool emailSent;

        /// <summary>
        /// A dispatch timer used to make sure we are not sorting/filtering as the user
        /// is typing.
        /// </summary>
        private DispatcherTimer timer;

        /// <summary>
        /// Private field for the <see cref="SelectedText"/> property.
        /// </summary>
        private string selectedText;

        /// <summary>
        /// Private field for the <see cref="OwnerControl"/> property.
        /// </summary>
        private UserControl ownerControl;

        /// <summary>
        /// Private field for the <see cref="OwnerWindow"/> property.
        /// </summary>
        private Window ownerWindow;

        /// <summary>
        /// Private field for controlling the graph window.
        /// </summary>
        private DefaultProfileViewer graphWindow;

        #endregion

        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="UniversalLogViewer.UniversalLogViewerViewModel"/> class.
        /// </summary>
        static DefaultViewerViewModel()
        {
            sync = new object();
        }

        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="UniversalLogViewer.UniversalLogViewerViewModel"/> class.
        /// </summary>
        public DefaultViewerViewModel()
        {
            this.Filenames = new List<string>();
            this.RecentFiles = new ObservableCollection<string>();
            this.Components = new ObservableCollection<UniversalLogComponentViewModel>();
            this.allViewModels = new List<UniversalLogComponentViewModel>();

            this.ShowErrors = true;
            this.ShowWarnings = true;
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Occurs whenever the search text changes. 
        /// </summary>
        public event EventHandler SearchTextChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Gets the command used to refresh the contents of the loaded universal log file.
        /// </summary>
        public SimpleCommand RefreshCommand
        {
            get
            {
                lock (sync)
                {
                    if (this.refreshCommand == null)
                    {
                        this.refreshCommand = new SimpleCommand(this.Refresh, this.CanRefresh);
                    }
                }
                return this.refreshCommand;
            }
        }

        /// <summary>
        /// Gets the command used to send a email with the contents of the file as
        /// an attachment.
        /// </summary>
        public SimpleCommand EmailCommand
        {
            get
            {
                lock (sync)
                {
                    if (this.emailCommand == null)
                    {
                        this.emailCommand = new SimpleCommand(this.Email, this.CanEmail);
                    }
                }
                return this.emailCommand;
            }
        }

        /// <summary>
        /// Gets the command used to send a email with the contents of the file as
        /// an attachment.
        /// </summary>
        public SimpleCommand ShowProfileCommand
        {
            get
            {
                lock (sync)
                {
                    if (this.showProfileCommand == null)
                    {
                        this.showProfileCommand = new SimpleCommand(this.ShowProfile);
                    }
                }
                return this.showProfileCommand;
            }
        }


        /// <summary>
        /// Gets the command used to send a email with the contents of the file as
        /// an attachment.
        /// </summary>
        public SimpleCommand FindCommand
        {
            get
            {
                lock (sync)
                {
                    if (this.findCommand == null)
                    {
                        this.findCommand = new SimpleCommand(this.Find);
                    }
                }
                return this.findCommand;
            }
        }

        /// <summary>
        /// Gets the command used to copy the selected text to the clipboard.
        /// </summary>
        public SimpleCommand CopyCommand
        {
            get
            {
                lock (sync)
                {
                    if (this.copyCommand == null)
                    {
                        this.copyCommand = new SimpleCommand(this.Copy);
                    }
                }
                return this.copyCommand;
            }
        }

        /// <summary>
        /// Gets or sets the list of recently opened file paths.
        /// </summary>
        public ObservableCollection<string> RecentFiles
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the collection of universal log component view models that the
        /// view is currently bound to.
        /// </summary>
        public ObservableCollection<UniversalLogComponentViewModel> Components
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the single filename based on the files that is currently loaded by the viewer.
        /// </summary>
        public string Filename
        {
            get
            {
                if (this.Filenames == null || this.Filenames.Count == 0)
                    return null;

                if (this.Filenames.Count == 1)
                    return " - " + this.Filenames[0];

                return string.Format(" - {0} files loaded.", this.Filenames.Count);
            }
        }

        /// <summary>
        /// Gets or sets the filenames of the files that is currently loaded by the viewer.
        /// </summary>
        public List<string> Filenames
        {
            get { return this.filenames; }
            set { this.SetProperty(ref this.filenames, value, "Filename", "StatusText"); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user is currently viewing the 
        /// universal log messages with word wrap enabled or not.
        /// </summary>
        public bool WordWrap
        {
            get { return this.wordWrap; }
            set { this.SetProperty(ref this.wordWrap, value, "WordWrap"); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user should be able to see the error
        /// messages or not.
        /// </summary>
        public bool ShowErrors
        {
            get
            {
                return this.showErrors;
            }

            set
            {
                this.SetProperty(ref this.showErrors, value, "ShowErrors");
                this.SortAndFilterComponents();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user should be able to see the warning
        /// messages or not.
        /// </summary>
        public bool ShowWarnings
        {
            get
            {
                return this.showWarnings;
            }

            set
            {
                this.SetProperty(ref this.showWarnings, value, "ShowWarnings");
                this.SortAndFilterComponents();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user should be able to see the normal
        /// messages or not.
        /// </summary>
        public bool ShowMessages
        {
            get
            {
                return this.showMessages;
            }

            set
            {
                this.SetProperty(ref this.showMessages, value, "ShowMessages");
                this.SortAndFilterComponents();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the user should be able to see the debug
        /// messages or not.
        /// </summary>
        public bool ShowDebugMessages
        {
            get
            {
                return this.showDebugMessages;
            }

            set
            {
                this.SetProperty(ref this.showDebugMessages, value, "ShowDebugMessages");
                this.SortAndFilterComponents();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the search box currently has
        /// keyboard focus.
        /// </summary>
        public bool FindBoxHasFocus
        {
            get { return this.findBoxHasFocus; }
            set { this.SetProperty(ref this.findBoxHasFocus, value, "FindBoxHasFocus"); }
        }

        /// <summary>
        /// Gets or sets the text currently in the search box.
        /// </summary>
        public string SearchText
        {
            get
            {
                return this.searchText;
            }

            set
            {
                this.SetProperty(ref this.searchText, value, "SearchText");
                if (this.timer != null)
                {
                    this.timer.Stop();
                    this.timer.Tick -= OnTimerElasped;
                    this.timer = null;
                }

                this.timer = new DispatcherTimer(DispatcherPriority.Normal, this.ownerControl.Dispatcher);
                this.timer.Interval = TimeSpan.FromMilliseconds(500.0);
                this.timer.IsEnabled = true;
                this.timer.Tick += new EventHandler(OnTimerElasped);
                this.timer.Start();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating that the currently search text has found
        /// no entries.
        /// </summary>
        public bool NoSearchResults
        {
            get { return this.noSearchResults; }
            set { this.SetProperty(ref this.noSearchResults, value, "NoSearchResults"); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the use of the search text should include
        /// regular expressions.
        /// </summary>
        public bool UseRegularExpression
        {
            get
            {
                return this.useRegularExpression;
            }

            set
            {
                this.SetProperty(ref this.useRegularExpression, value, "UseRegularExpression");
                SortAndFilterComponents();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the use of the search text should match
        /// whole words or not.
        /// </summary>
        public bool MatchWholeWords
        {
            get
            {
                return this.matchWholeWords;
            }

            set
            {
                this.SetProperty(ref this.matchWholeWords, value, "MatchWholeWords");
                SortAndFilterComponents();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the use of the search text should match
        /// the case or not.
        /// </summary>
        public bool MatchCase
        {
            get
            {
                return this.matchCase;
            }

            set
            {
                this.SetProperty(ref this.matchCase, value, "MatchCase");
                SortAndFilterComponents();
            }
        }

        /// <summary>
        /// Gets or sets the text that is currently selected in the view.
        /// </summary>
        public string SelectedText
        {
            get { return this.selectedText; }
            set { this.selectedText = value; }
        }

        /// <summary>
        /// Gets or sets a reference to the user control that this view model is attached to.
        /// </summary>
        internal UserControl OwnerControl
        {
            get
            {
                return this.ownerControl;
            }

            set
            {
                this.ownerControl = value;

                DependencyObject parent = VisualTreeHelper.GetParent(this.ownerControl);
                Window window = parent as Window;
                while (parent != null && window == null)
                {
                    parent = VisualTreeHelper.GetParent(parent);
                    window = parent as Window;
                }

                this.ownerWindow = window;
            }
        }

        /// <summary>
        /// Gets or sets a reference to the user control that this view model is attached to.
        /// </summary>
        private Window OwnerWindow
        {
            get { return this.ownerWindow; }
            set { this.ownerWindow = value; }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Opens the given universal log filename.
        /// </summary>
        /// <param name="filename">
        /// The filename to the universal log to open.
        /// </param>
        public void OpenFiles(IEnumerable<string> filenames, bool clearCurrent)
        {
            List<string> validFilenames = new List<string>();
            foreach (string filename in filenames)
            {
                if (File.Exists(filename))
                {
                    validFilenames.Add(filename);
                }
            }

            foreach (string filename in validFilenames)
            {
                this.RecentFiles.Remove(filename);
                this.RecentFiles.Insert(0, filename);
            }

            while (this.RecentFiles.Count > 5)
            {
                this.RecentFiles.RemoveAt(this.RecentFiles.Count - 1);
            }

            UniversalLogFileListener ulog = new UniversalLogFileListener(validFilenames);
            if (clearCurrent)
            {
                this.Filenames = validFilenames.ToList();
            }
            else
            {
                this.Filenames.AddRange(validFilenames);
            }

            this.emailSent = false;
            CreateComponents(ulog.Buffer, clearCurrent);
            if (ulog.HasErrors)
            {
                CreateComponents(ulog.ParseErrors, false);
            }
        }

        /// <summary>
        /// Gets called when the search text has changed but not for the previous 500
        /// milliseconds.
        /// </summary>
        /// <param name="sender">
        /// The object this event is attached to.
        /// </param>
        /// <param name="e">
        /// The System.EventArgs data for this event.
        /// </param>
        private void OnTimerElasped(object sender, EventArgs e)
        {
            this.timer.Stop();

            SortAndFilterComponents();
            if (this.SearchTextChanged != null)
                this.SearchTextChanged(this, EventArgs.Empty);
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct events are fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the property we wish to set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field we wish to set.
        /// </param>
        /// <param name="value">
        /// The value we wish to set the field to.
        /// </param>
        /// <param name="names">
        /// A array of property names that are changed due to this one change.
        /// </param>
        private void SetProperty<T>(ref T field, T value, params string[] names)
        {
            if (EqualityComparer<T>.Default.Equals(field, value))
                return;

            field = value;
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler == null || names == null)
                return;

            foreach (string name in names)
                handler(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Determines whether the refresh command can be executed by the user.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in by the view.
        /// </param>
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanRefresh(object parameter)
        {
            if (this.Filenames == null || this.Filenames.Count == 0)
                return false;

            return true;
        }

        /// <summary>
        /// Refreshes the currently opened file by reloading the universal log model.
        /// </summary>
        private void Refresh()
        {
            if (this.Filenames == null || this.Filenames.Count == 0)
                return;

            UniversalLogFileListener ulog = new UniversalLogFileListener(this.Filenames);
            CreateComponents(ulog.Buffer, true);
            if (ulog.HasErrors)
            {
                CreateComponents(ulog.ParseErrors, false);
            }
        }

        /// <summary>
        /// Creates the component view models from the universal log model.
        /// </summary>
        /// <param name="ulog"></param>
        private void CreateComponents(IEnumerable<InMemoryUniversalLogTarget.BufferedMessage> buffer, bool clearCurrent)
        {
            try
            {
                if (clearCurrent)
                {
                    this.Components.Clear();
                    this.allViewModels.Clear();
                }

                List<UniversalLogComponentViewModel> viewModels = new List<UniversalLogComponentViewModel>();
                foreach (InMemoryUniversalLogTarget.BufferedMessage message in buffer)
                {
                    UniversalLogComponentViewModel vm = UniversalLogComponentViewModel.Create(message);
                    if (vm != null)
                    {
                        viewModels.Add(vm);
                    }
                }

                SortedList<DateTime, List<UniversalLogComponentViewModel>> sorted = new SortedList<DateTime, List<UniversalLogComponentViewModel>>();
                foreach (var vm in this.allViewModels)
                {
                    DateTime time = vm.DateTime;
                    if (!sorted.ContainsKey(time))
                        sorted.Add(time, new List<UniversalLogComponentViewModel>());

                    sorted[time].Add(vm);
                }

                foreach (var vm in viewModels)
                {
                    DateTime time = vm.DateTime;
                    if (!sorted.ContainsKey(time))
                        sorted.Add(time, new List<UniversalLogComponentViewModel>());

                    sorted[time].Add(vm);
                }

                this.allViewModels.Clear();
                foreach (var kvp in sorted)
                {
                    foreach (var vm in kvp.Value)
                    {
                        this.allViewModels.Add(vm);
                    }
                }

                this.SortAndFilterComponents();

                int errors = 0;
                int warnings = 0;
                int other = 0;
                foreach (UniversalLogComponentViewModel vm in this.allViewModels)
                {
                    if (vm.ComponentType == UniversalLogComponentTypes.Error)
                    {
                        errors++;
                    }
                    else if (vm.ComponentType == UniversalLogComponentTypes.Warning)
                    {
                        warnings++;
                    }
                    else
                    {
                        other++;
                    }
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// Gets called when the exit command gets executed by the user.
        /// </summary>
        private void Exit()
        {
            Application.Current.Shutdown();
        }

        /// <summary>
        /// Determines whether the email command can be executed by the user.
        /// </summary>
        /// <param name="parameter">
        /// The command parameter passed in by the view.
        /// </param>
        /// <returns>
        /// True if the command can be executed; otherwise, false.
        /// </returns>
        private bool CanEmail(object parameter)
        {
            if (this.Filenames == null || this.Filenames.Count == 0)
                return false;

            return true;
        }

        /// <summary>
        /// Gets called when requests to email the report to a specific user.
        /// </summary>
        private void Email()
        {
            if (this.Filenames == null || this.Filenames.Count == 0)
                return;

            if (this.emailSent)
            {
                string msgText = @"You have already sent this universal log in an email to the"
                    + " designated recipients. Are you sure you wish to send it again?";
                MessageBoxResult result = MessageBox.Show(msgText, @"Sanity Check", MessageBoxButton.OKCancel, MessageBoxImage.Stop);
                if (result == MessageBoxResult.Cancel)
                {
                    return;
                }
            }

            AdditionalInformationWindow info = new AdditionalInformationWindow();
            info.Topmost = true;
            if (this.ownerWindow != null)
            {
                info.WindowStartupLocation = WindowStartupLocation.CenterOwner;
                info.Owner = this.ownerWindow;
            }
            else
            {
                info.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            }

            bool? dialogResult = info.ShowDialog();
            if (dialogResult == false)
            {
                return;
            }

            string additionalInfo = info.AdditionalInformationText.Text;
            this.emailSent = true;
            IConfig config = ConfigFactory.CreateConfig();

            List<string> recipients = new List<string>();
            recipients.Add("*tools@rockstarnorth.com");

            string userEmail = config.EmailAddress;
            if (userEmail != null && !recipients.Contains(userEmail))
            {
                recipients.Add(userEmail);
            }

            if (recipients == null || recipients.Count == 0)
            {
                return;
            }

            string[] attachments = this.Filenames.ToArray();
            string subject = @"Universal Log Report from " + Environment.UserName;
            string body = "Additional Information:\n\n" + additionalInfo;

            RSG.UniversalLog.Util.Email.SendEmail(config, recipients.ToArray(), subject, body, attachments);
        }

        /// <summary>
        /// Gets called when the user requests to see the profile window.
        /// </summary>
        private void ShowProfile()
        {
            if (graphWindow == null)
            {
                graphWindow = new DefaultProfileViewer(this.filenames, this.allViewModels);
                graphWindow.Closed += new EventHandler(OnProfileGraphClosed);
                graphWindow.Show();
            }
            else
            {
                graphWindow.Focus();
            }
        }

        /// <summary>
        /// Handles closing event for 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnProfileGraphClosed(object sender, EventArgs e)
        {
            graphWindow = null;
        }

        /// <summary>
        /// Gets called when requests to email the report to a specific user.
        /// </summary>
        private void Find()
        {
            this.FindBoxHasFocus = true;
        }

        /// <summary>
        /// Gets called when copy command gets executed by the user.
        /// </summary>
        private void Copy()
        {
            Clipboard.SetText(this.SelectedText);
        }

        /// <summary>
        /// Uses the current search text to determine which messages can be shown.
        /// </summary>
        private void DetermineSearchResults()
        {
            this.NoSearchResults = false;
            if (string.IsNullOrEmpty(this.SearchText))
                return;

            this.NoSearchResults = true;
        }

        /// <summary>
        /// 
        /// </summary>
        private void SortAndFilterComponents()
        {
            try
            {
                int validCount = 0;
                this.NoSearchResults = false;
                this.Components.Clear();
                for (int i = 0; i < this.allViewModels.Count; i++)
                {
                    UniversalLogComponentViewModel vm = this.allViewModels[i];
                    bool valid = true;

                    if (vm.ComponentType == UniversalLogComponentTypes.Profiling)
                        valid = false;

                    if (vm.ComponentType == UniversalLogComponentTypes.Error && this.ShowErrors == false)
                        valid = false;

                    if (vm.ComponentType == UniversalLogComponentTypes.Warning && this.ShowWarnings == false)
                        valid = false;

                    if (vm.ComponentType == UniversalLogComponentTypes.Message && this.ShowMessages == false)
                        valid = false;

                    if (vm.ComponentType == UniversalLogComponentTypes.DebugMessage && this.ShowDebugMessages == false)
                        valid = false;

                    if (valid)
                    {
                        string search = this.SearchText;
                        if (!string.IsNullOrEmpty(this.SearchText))
                        {
                            string text = string.Format(RSG.UniversalLog.Controls.ParagraphExtension.msgFmt + "\n", vm.TimeStamp, vm.Type, vm.Context, vm.Message);
                            if (this.UseRegularExpression)
                            {
                                if (!this.MatchCase)
                                {
                                    Regex rgx = new Regex(search, RegexOptions.IgnoreCase);
                                    if (!rgx.IsMatch(text))
                                        valid = false;
                                }
                                else
                                {
                                    Regex rgx = new Regex(search);
                                    if (!rgx.IsMatch(text))
                                        valid = false;
                                }
                            }
                            else
                            {
                                if (!this.MatchCase)
                                {
                                    search = search.ToLower();
                                    text = text.ToLower();
                                }

                                if (!this.MatchWholeWords)
                                {
                                    if (!text.Contains(search))
                                        valid = false;
                                }
                                else
                                {
                                    Regex rgx = new Regex("[^a-zA-Z0-9 ]");
                                    text = rgx.Replace(text, "");
                                    if (!text.EndsWith(" "))
                                        text += " ";
                                    if (!text.StartsWith(" "))
                                        text = " " + search;

                                    if (!search.EndsWith(" "))
                                        search += " ";
                                    if (!search.StartsWith(" "))
                                        search = " " + search;

                                    if (!text.Contains(search))
                                        valid = false;
                                }
                            }
                        }
                    }

                    if (valid && !this.Components.Contains(vm))
                    {
                        this.Components.Add(vm);
                    }

                    if (!valid && this.Components.Contains(vm))
                        this.Components.Remove(vm);

                    validCount++;
                }
            }
            finally
            {
                if (this.Components.Count > 0 && (this.ShowErrors || this.ShowWarnings || this.ShowMessages || this.ShowDebugMessages))
                {
                    this.NoSearchResults = this.Components.Count == 0;
                }
            }
        }
        #endregion
    }
}
