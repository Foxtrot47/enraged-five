﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RSG.UniversalLog.DefaultViewer
{
    /// <summary>
    /// Interaction logic for AdditionalnformationWindow.xaml
    /// </summary>
    public partial class AdditionalInformationWindow : Window
    {
        public AdditionalInformationWindow()
        {
            InitializeComponent();
        }

        private void OkClicked(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void CancelClicked(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
