﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Collections;
using System.Collections.Specialized;
using System.Windows.Documents;
using System.Windows.Media;
using System.ComponentModel;
using RSG.UniversalLog.ViewModel;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Windows.Controls;

namespace RSG.UniversalLog.Controls
{
    /// <summary>
    /// A extension method to bind textblock inline children to string that can contain
    /// hyperlinks.
    /// </summary>
    public class TextBlockExtension
    {
        #region Fields
        /// <summary>
        /// Identifies the TextBlockInlines dependency property.
        /// </summary>
        public static DependencyProperty TextBlockInlinesProperty;
        #endregion
        
        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="UniversalLogViewer.TextBlockExtension"/> class.
        /// </summary>
        static TextBlockExtension()
        {
            TextBlockInlinesProperty =
            DependencyProperty.RegisterAttached("TextBlockInlines",
            typeof(string),
            typeof(TextBlockExtension), new PropertyMetadata(null, OnInlinesChanged));
        }
        #endregion

        #region Methods
        /// <summary>
        /// Sets the <see cref="TextBlockInlines"/> attached property on the
        /// specified element to the specified value.
        /// </summary>
        /// <param name="element">
        /// The element to set the attached property on.
        /// </param>
        /// <param name="value">
        /// The value to set the attached property to.
        /// </param>
        public static void SetTextBlockInlines(DependencyObject element, string value)
        {
            element.SetValue(TextBlockInlinesProperty, value);
        }

        /// <summary>
        /// Gets the <see cref="TextBlockInlines"/> attached property for the
        /// specified element.
        /// </summary>
        /// <param name="element">
        /// The element to get the attached property on.
        /// </param>
        /// <returns>
        /// The value of the TextBlockInlines attached property on the specified element.
        /// </returns>
        public static string GetTextBlockInlines(DependencyObject element)
        {
            return (string)element.GetValue(TextBlockInlinesProperty);
        }

        /// <summary>
        /// Gets called when the inline collection changes value.
        /// </summary>
        /// <param name="sender">
        /// The dependency object who sent this event. 
        /// </param>
        /// <param name="e">
        /// The event data.
        /// </param>
        private static void OnInlinesChanged(DependencyObject sender, DependencyPropertyChangedEventArgs args)
        {
            TextBlock textBlock = sender as TextBlock;
            if (textBlock == null)
            {
                return;
            }

            textBlock.Inlines.Clear();
            if (!(args.NewValue is String) || (args.NewValue as string).Length == 0)
            {
                return;
            }

            string stringValue = args.NewValue as string;
            Brush foreground = textBlock.Foreground;
            string[] words = stringValue.Split(new char[1] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

            StringBuilder currentPortion = new StringBuilder(stringValue.Length);
            bool isHyperLink = false;
            int inlineCount = 0;
            int wordIndex = 0;
            foreach (string word in words)
            {
                if (word.StartsWith("http://") || word.StartsWith("\"http://") ||
                    word.StartsWith("https://") || word.StartsWith("\"https://") ||
                    word.StartsWith("htp://") || word.StartsWith("\"htp://"))
                {
                    if (currentPortion.Length > 0)
                    {
                        Run message = new Run();
                        if (inlineCount > 0)
                            message.Text = string.Format(" {0}", currentPortion.ToString());
                        else
                            message.Text = string.Format("{0}", currentPortion.ToString());

                        message.Foreground = foreground;
                        textBlock.Inlines.Add(message);
                        inlineCount++;
                        currentPortion.Clear();
                    }


                    if (word[0] == '"')
                    {
                        isHyperLink = true;
                        currentPortion.Append(word);
                    }
                    else
                    {
                        Hyperlink link = new Hyperlink();
                        if (inlineCount > 0)
                        {
                            textBlock.Inlines.Add(new Run(" "));
                        }

                        link.Inlines.Add(new Run(word));

                        try
                        {
                            link.NavigateUri = new Uri(word);
                            link.RequestNavigate += (s, e) =>
                            {
                                Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
                                e.Handled = true;
                            };
                            textBlock.Inlines.Add(link);
                        }
                        catch (System.UriFormatException)
                        {
                            Run message = new Run();
                            if (inlineCount > 0)
                                message.Text = string.Format(" {0}", currentPortion.ToString());
                            else
                                message.Text = string.Format("{0}", currentPortion.ToString());

                            message.Foreground = foreground;
                            textBlock.Inlines.Add(message);
                        }

                        inlineCount++;
                    }
                }
                else
                {
                    if (currentPortion.Length == 0)
                    {
                        currentPortion.Append(word);
                    }
                    else
                    {
                        currentPortion.Append(" " + word);
                    }

                    if (wordIndex == words.Length - 1)
                    {
                        Run message = new Run();
                        if (inlineCount > 0)
                            message.Text = string.Format(" {0}", currentPortion.ToString());
                        else
                            message.Text = string.Format("{0}", currentPortion.ToString());

                        message.Foreground = foreground;
                        textBlock.Inlines.Add(message);
                        inlineCount++;
                        currentPortion.Clear();
                    }
                }

                if (isHyperLink && word.EndsWith("\"") || isHyperLink && wordIndex == words.Length - 1)
                {
                    isHyperLink = false;
                    Hyperlink link = new Hyperlink();
                    link.Inlines.Add(new Run(currentPortion.ToString().Replace("\"", string.Empty).Replace(" ", "%20")));

                    try
                    {
                        string url = currentPortion.ToString().Replace("\"", string.Empty).Replace(" ", "%20");
                        link.NavigateUri = new Uri(url);
                        link.RequestNavigate += (s, e) =>
                        {
                            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
                            e.Handled = true;
                        };

                        if (inlineCount > 0)
                        {
                            textBlock.Inlines.Add(new Run(" "));
                        }
                        textBlock.Inlines.Add(link);
                    }
                    catch (System.UriFormatException)
                    {
                        Run message = new Run();
                        if (inlineCount > 0)
                            message.Text = string.Format(" {0}", currentPortion.ToString());
                        else
                            message.Text = string.Format("{0}", currentPortion.ToString());

                        message.Foreground = foreground;
                        textBlock.Inlines.Add(message);
                    }

                    inlineCount++;
                    currentPortion.Clear();
                }

                wordIndex++;
            }
        }
        #endregion
    }
}
