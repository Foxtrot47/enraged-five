﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace RSG.UniversalLog.ViewModel
{
    public class UniversalLogProfileGraphViewModel : INotifyPropertyChanged
    {
        #region Fields

        /// <summary>
        /// Private field for the <see cref="MatchCase"/> property.
        /// </summary>
        private List<UniversalLogComponentViewModel> viewModels;

        /// <summary>
        /// Private field for the profile data property.
        /// </summary>
        public List<KeyValuePair<string, int>> Data;

        public List<KeyValuePair<string, object[]>> HierarchicalData;

        public enum ViewSelectionEnum
        {
            DEFAULT_BAR_CHART,
            CONGLOMERATED_BAR_CHART,
            DEFAULT_PIE_CHART,
            HIERARCHICAL_CHART
        }

        public List<string> ViewSelectionTypeNames = new List<string> {
            "Bar Chart",
            "Conglomerated Bar Chart",
            "Pie Chart",
            "Hierarchical Bar Chart"
        };

        /// <summary>
        /// Gets a list of all view selection types.
        /// </summary>
        public ObservableCollection<string> ViewSelectionTypes
        {
            get;
            private set;
        }

        /// <summary>
        /// Public property for the currently selected view.
        /// </summary>
        public string SelectedViewType
        {
            get;
            set;
        }

        /// <summary>
        /// Data bound to the graph.
        /// </summary>
        public List<KeyValuePair<string, int>> GraphData
        {
            get { return this.Data; }
        }

        /// <summary>
        /// Data bound to the graph.
        /// </summary>
        public List<KeyValuePair<string, object[]>> HierarchicalGraphData
        {
            get { return this.HierarchicalData; }
        }

        public static List<UniversalLogComponentViewModel> GlobalViewModels;

        public string SelectedViewTypes { get; private set; }
        
        #endregion

        #region Structures
        /// <summary>
        /// Helper structure for organizing profiling data.
        /// </summary>
        struct ProfileData
        {
            public string Message;
            public int TotalMilliseconds;
            public int UniqueIdentifier;

            public ProfileData(string message, int totalMilliseconds)
            {
                Message = message;
                TotalMilliseconds = totalMilliseconds;
                UniqueIdentifier = 0;
            }
        }

        /// <summary>
        /// Helper class for organizing profiling data.
        /// </summary>
        class HierarchicalProfileData
        {
            public string Message;
            public int TotalMilliseconds;
            public int StartMillisecond;
            public int UniqueIdentifier;
            public UniversalLogComponentViewModel Donor;
            public List<HierarchicalProfileData> Children;

            public HierarchicalProfileData(UniversalLogComponentViewModel donor, string message, int startMillisecond, int totalMilliseconds)
            {
                Donor = donor;
                Children = new List<HierarchicalProfileData>();
                Message = message;
                StartMillisecond = startMillisecond;
                TotalMilliseconds = totalMilliseconds;
                UniqueIdentifier = 0;
            }
        }
        #endregion

        #region Constructor
        public UniversalLogProfileGraphViewModel()
        {
            this.viewModels = GlobalViewModels;

            this.Data = GetConglomeratedList();

            ViewSelectionTypes = new ObservableCollection<string>();
            foreach(string viewSelection in ViewSelectionTypeNames)
            {
                ViewSelectionTypes.Add(viewSelection);
            }

            SelectedViewType = ViewSelectionTypeNames[0];
        }

        #endregion

        #region Events
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        #endregion

        #region Methods
        /// <summary>
        /// Sets the given field to the given value making sure the correct events are fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the property we wish to set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field we wish to set.
        /// </param>
        /// <param name="value">
        /// The value we wish to set the field to.
        /// </param>
        /// <param name="names">
        /// A array of property names that are changed due to this one change.
        /// </param>Not
        private void SetProperty<T>(ref T field, T value, params string[] names)
        {
            if (EqualityComparer<T>.Default.Equals(field, value))
                return;

            field = value;
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler == null || names == null)
                return;

            foreach (string name in names)
                handler(this, new PropertyChangedEventArgs(name));
        }

        /// <summary>
        /// Re-binds data based on the view specified.
        /// </summary>
        /// <param name="index"></param>
        public void SetDataView(int index)
        {
            bool setData = true;

            List<KeyValuePair<string, int>> newData = new List<KeyValuePair<string, int>>();
            if (index == (int)ViewSelectionEnum.DEFAULT_BAR_CHART)
            {
                newData = GetDefaultList();
            }
            else if (index == (int)ViewSelectionEnum.CONGLOMERATED_BAR_CHART)
            {
                newData = GetConglomeratedList();
            }
            else if (index == (int)ViewSelectionEnum.DEFAULT_PIE_CHART)
            {
                newData = GetDefaultList();
            }
            else if (index == (int)ViewSelectionEnum.HIERARCHICAL_CHART)
            {
                var newTreeData = GetHierarchicalList();
                this.SetProperty(ref this.HierarchicalData, newTreeData, "HierarchicalGraphData");
                setData = false;
            }

            if (setData)
            {
                this.SetProperty(ref this.Data, newData, "GraphData");
            }
        }

        /// <summary>
        /// Every profile entry is added to the graph in sequential order.
        /// </summary>
        /// <returns></returns>
        private List<KeyValuePair<string, int>> GetDefaultList()
        {
            if (this.viewModels == null)
                return new List<KeyValuePair<string, int>>();

            List<ProfileData> profileData = new List<ProfileData>();
            Stack<ProfilingViewModel> profileStack = new Stack<ProfilingViewModel>();
            
            foreach (UniversalLogComponentViewModel componentViewModel in this.viewModels)
            {
                if (componentViewModel.ComponentType == UniversalLogComponentTypes.Profiling)
                {
                    ProfilingViewModel profilingComponentModel = (ProfilingViewModel)componentViewModel;
                    if (profilingComponentModel.IsProfileEnd == false)
                    {
                        profileStack.Push(profilingComponentModel);
                    }
                    else
                    {
                        if (profileStack.Count > 0)  //Protect against some stray profile end message; empty stack.
                        {
                            ProfilingViewModel startProfilingModelView = profileStack.Pop();
                            TimeSpan timeSpan = componentViewModel.DateTime - startProfilingModelView.DateTime;
                            ProfileData newProfileData = new ProfileData(startProfilingModelView.Message, (int)timeSpan.TotalMilliseconds);
                                
                            //Need to assign it a unique identifier.
                            for (int dataIndex = 0; dataIndex < profileData.Count; dataIndex++)
                            {
                                ProfileData data = profileData[dataIndex];
                                if (data.Message == startProfilingModelView.Message)
                                {
                                    newProfileData.UniqueIdentifier++;
                                }
                            }

                            profileData.Add(newProfileData);
                        }
                    }
                }
            }

            //Now create a list of key value pairs.
            List<KeyValuePair<string, int>> keyValuePairs = new List<KeyValuePair<string, int>>();

            foreach (ProfileData data in profileData)
            {
                keyValuePairs.Add(new KeyValuePair<string, int>(String.Format("{0} {1}", data.Message, data.UniqueIdentifier), data.TotalMilliseconds));
            }

            return keyValuePairs;
        }

        /// <summary>
        /// Fills in a list of key value pairs that represent the points on the graph.
        /// </summary>
        /// <returns>The data points.</returns>
        private List<KeyValuePair<string, object[]>> GetHierarchicalList()
        {
            var seriesData = new List<KeyValuePair<string, object[]>>();

            var entries = FillTreeView();

            foreach (var entry in entries)
            {
                FillSeries(entry, seriesData);
            }

            return seriesData;
        }

        /// <summary>
        /// Fill in the series data from the hierarchical data.
        /// </summary>
        /// <param name="profileData">Hierarchical </param>
        /// <param name="seriesData"></param>
        private void FillSeries(HierarchicalProfileData profileData, List<KeyValuePair<string, object[]>> seriesData)
        {
            seriesData.Insert(0, new KeyValuePair<string, object[]>(profileData.Message, new object[] { profileData.StartMillisecond, profileData.TotalMilliseconds + profileData.StartMillisecond }));

            foreach (var entry in profileData.Children)
            {
                FillSeries(entry, seriesData);
            }
        }

        /// <summary>
        /// Create a tree view of data based upon the profile start/end tags in the universal log file.
        /// </summary>
        /// <returns>Logical tree of profile tags.</returns>
        private List<HierarchicalProfileData> FillTreeView()
        {
            HierarchicalProfileData profileData = new HierarchicalProfileData(null, "ROOT", 0, 0);

            Stack<HierarchicalProfileData> stack = new Stack<HierarchicalProfileData>();
            stack.Push(profileData);

            if (viewModels == null || viewModels.Count == 0)
            {
                return new List<HierarchicalProfileData>();
            }

            var firstEntry = viewModels.FirstOrDefault();
            DateTime kickOff = firstEntry.DateTime;

            foreach (var entry in viewModels)
            {
                HierarchicalProfileData currentContainer = stack.Peek();

                if (entry is ProfilingViewModel)
                {
                    var profiling = entry as ProfilingViewModel;
                    if (!profiling.IsProfileEnd)
                    {
                        TimeSpan startTimeSpan = entry.DateTime - kickOff;
                        HierarchicalProfileData hpd = new HierarchicalProfileData(entry, entry.Message, (int)startTimeSpan.TotalMilliseconds, 0);
                        currentContainer.Children.Add(hpd);

                        var dateTime = currentContainer.Donor == null ? kickOff : currentContainer.Donor.DateTime;
                        currentContainer.TotalMilliseconds = (int)((entry.DateTime - dateTime).TotalMilliseconds);
                        stack.Push(hpd);
                    }
                    else
                    {
                        if (stack.Count > 1)
                        {
                            var profData = stack.Pop();
                            TimeSpan diff = entry.DateTime - profData.Donor.DateTime;
                            profData.TotalMilliseconds = (int)diff.TotalMilliseconds;
                        }
                    }
                }
            }

            return profileData.Children;
        }
        
        /// <summary>
        /// Provides a list of all profiles grouped by name.  Identically named profile group are added to a single bar.
        /// </summary>
        /// <returns></returns>
        private List<KeyValuePair<string, int>> GetConglomeratedList()
        {
            if (this.viewModels == null)
                return new List<KeyValuePair<string, int>>();

            List<ProfileData> profileData = new List<ProfileData>();
            Stack<ProfilingViewModel> profileStack = new Stack<ProfilingViewModel>();

            foreach (UniversalLogComponentViewModel componentViewModel in this.viewModels)
            {
                if (componentViewModel.ComponentType == UniversalLogComponentTypes.Profiling)
                {
                    ProfilingViewModel profilingComponentModel = (ProfilingViewModel) componentViewModel;
                    if (profilingComponentModel.IsProfileEnd == false )
                    {
                        profileStack.Push(profilingComponentModel);
                    }
                    else 
                    {
                        if (profileStack.Count > 0)  //Protect against some stray profile end message; empty stack.
                        {
                            ProfilingViewModel startProfilingModelView = profileStack.Pop();
                            TimeSpan timeSpan = componentViewModel.DateTime - startProfilingModelView.DateTime;

                            bool found = false;
                            for (int dataIndex = 0; dataIndex < profileData.Count; dataIndex++)
                            {
                                ProfileData data = profileData[dataIndex];
                                if (data.Message == startProfilingModelView.Message)
                                {
                                    data.TotalMilliseconds += (int)timeSpan.TotalMilliseconds;
                                    found = true;
                                    break;
                                }
                            }

                            if (found == false)
                            {
                                profileData.Add(new ProfileData(startProfilingModelView.Message, (int)timeSpan.TotalMilliseconds));
                            }
                        }
                    }
                }
            }

            //Now create a list of key value pairs.
            List<KeyValuePair<string, int>> keyValuePairs = new List<KeyValuePair<string, int>>();
            
            foreach(ProfileData data in profileData)
            {
                keyValuePairs.Add(new KeyValuePair<string, int>(data.Message, data.TotalMilliseconds));
            }
            
            return keyValuePairs;
        }
#endregion
    }
}
