﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections;
using System.Globalization;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace RSG.UniversalLog
{
    /// <summary>
    /// Interaction logic for ContextFilter.xaml
    /// </summary>
    public partial class ContextFilter : UserControl
    {
        #region Events
        /// <summary>
        /// 
        /// </summary>
        public event EventHandler SelectionChanged;
        #endregion

        public class ItemViewModel : INotifyPropertyChanged
        {
            #region INotifyPropertyChanged Members
            public event PropertyChangedEventHandler PropertyChanged;
            #endregion

            #region Properties

            /// <summary>
            /// 
            /// </summary>
            public bool IsChecked
            {
                get { return m_isChecked; }
                set { m_isChecked = value; }
            }
            private bool m_isChecked;

            /// <summary>
            /// 
            /// </summary>
            public string Name
            {
                get { return m_name; }
                set { m_name = value; OnPropertyChange(ref m_name, value, "Name"); }
            }
            private string m_name;
            #endregion

            private void OnPropertyChange<T>(ref T field, T newValue, string propertyName)
            {
                if (Object.Equals(field, newValue))
                    return;

                field = newValue;
                if (this.PropertyChanged != null)
                    this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #region Properties
        /// <summary> 
        ///Gets or sets a collection used to generate the content of the ComboBox 
        /// </summary> 
        public IList<string> ItemsSource
        {
            get { return (IList<string>)GetValue(ItemsSourceProperty); }
            set
            {
                SetValue(ItemsSourceProperty, value);

                SetText();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public List<string> SelectedItems
        {
            get { return (List<string>)GetValue(SelectedItemsProperty); }
            set { SetValue(SelectedItemsProperty, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Delimiter
        {
            get;
            set;
        }

        /// <summary> 
        ///Gets or sets the text displayed in the ComboBox 
        /// </summary> 
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        /// <summary> 
        ///Gets or sets the text displayed in the ComboBox if there are no selected items 
        /// </summary> 
        public String DefaultEmptyText
        {
            get { return (string)GetValue(DefaultEmptyTextProperty); }
            set { SetValue(DefaultEmptyTextProperty, value); }
        }

        /// <summary> 
        ///Gets or sets the text displayed in the ComboBox if there are no selected items 
        /// </summary> 
        public String DefaultFullText
        {
            get { return (string)GetValue(DefaultFullTextProperty); }
            set { SetValue(DefaultFullTextProperty, value); }
        }

        /// <summary>
        /// 
        /// </summary>
        public ObservableCollection<ItemViewModel> Items
        {
            get;
            set;
        }

        #region Dependency Properties
        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(IList<string>), typeof(ContextFilter), new UIPropertyMetadata(null, OnSourceChanged));

        private static void OnSourceChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            ((ContextFilter)sender).Items.Clear();
            if (e.NewValue != null)
            {
                if (e.NewValue is INotifyCollectionChanged)
                {
                    (e.NewValue as INotifyCollectionChanged).CollectionChanged +=  ((ContextFilter)sender).ContextFilter_CollectionChanged;
                }
                foreach (string item in e.NewValue as IEnumerable<string>)
                {
                    ItemViewModel newItem = new ItemViewModel();
                    newItem.IsChecked = true;
                    newItem.Name = item;
                    ((ContextFilter)sender).Items.Add(newItem);
                }
                ((ContextFilter)sender).SetText();
            }
        }

        private void ContextFilter_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            this.Items.Clear();
            foreach (string item in sender as IList<string>)
            {
                ItemViewModel newItem = new ItemViewModel();
                newItem.IsChecked = true;
                newItem.Name = item;
                this.Items.Add(newItem);
            }
            this.SetText();
        }

        public static readonly DependencyProperty SelectedItemsProperty =
            DependencyProperty.Register("SelectedItems", typeof(List<string>), typeof(ContextFilter), new UIPropertyMetadata(new List<string>()));

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(ContextFilter), new UIPropertyMetadata(String.Empty));

        // Using a DependencyProperty as the backing store for DefaultText.  
        // This enables animation, styling, binding, etc... 
        public static readonly DependencyProperty DefaultEmptyTextProperty =
            DependencyProperty.Register("DefaultEmptyText", typeof(string),
            typeof(ContextFilter), new UIPropertyMetadata(string.Empty, OnDefaultEmptyTextChanged));

        // Using a DependencyProperty as the backing store for DefaultText.  
        // This enables animation, styling, binding, etc... 
        public static readonly DependencyProperty DefaultFullTextProperty =
            DependencyProperty.Register("DefaultFullText", typeof(string),
            typeof(ContextFilter), new UIPropertyMetadata(string.Empty, OnDefaultFullTextChanged));
        #endregion
        #endregion

        #region Constructor
        public ContextFilter()
        {
            this.Items = new ObservableCollection<ItemViewModel>();
            this.Delimiter = "&";
            InitializeComponent();
        }
        #endregion
        
        /// <summary> 
        ///Set the text property of this control
        /// </summary> 
        private void SetText()
        {
            string text = string.Empty;
            bool allChecked = true;

            int includeCount = 0;
            List<string> selected = new List<string>();
            foreach (ItemViewModel item in this.Items)
            {
                if (item.IsChecked)
                {
                    if (includeCount != 0)
                    {
                        string delimiter = " " + this.Delimiter ?? "&";
                        text += delimiter + " " + item.Name;
                    }
                    else
                    {
                        text += item.Name;
                    }
                    includeCount++;
                    selected.Add(item.Name);
                }
                else
                {
                    allChecked = false;
                }
            }
            // set DefaultText if nothing else selected
            if (this.Text != text)
            {
                this.SelectedItems = new List<string>(selected);
                if (string.IsNullOrWhiteSpace(text))
                {
                    this.Text = this.DefaultEmptyText;
                }
                else if (allChecked)
                {
                    this.Text = this.DefaultFullText;
                }
                else
                {
                    this.Text = text;
                }

                if (this.SelectionChanged != null)
                    this.SelectionChanged(this, EventArgs.Empty);
            }
        }

        private static void OnDefaultEmptyTextChanged(Object s, DependencyPropertyChangedEventArgs e)
        {
            ContextFilter t = s as ContextFilter;
            if (t == null)
                return;

            t.SetText();
        }

        private static void OnDefaultFullTextChanged(Object s, DependencyPropertyChangedEventArgs e)
        {
            ContextFilter t = s as ContextFilter;
            if (t == null)
                return;

            t.SetText();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            SetText();
        }

        private void CheckBox_UnChecked(object sender, RoutedEventArgs e)
        {
            SetText();
        }
    }
}
