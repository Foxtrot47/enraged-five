﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Xml;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace RSG.UniversalLog
{

    /// <summary>
    /// Universal Log file listener; not wrapping a UniversalLogFile object but
    /// wrapping the file on disk.
    /// </summary>
    public class UniversalLogFileListener : InMemoryUniversalLogTarget, IUniversalLogListener
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="ParseErrors"/> and
        /// <see cref="HasParseErrors"/>properties.
        /// </summary>
        private List<BufferedMessage> errors;
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="RSG.UniversalLog.UniversalLogListener"/> class.
        /// </summary>
        public UniversalLogFileListener(IEnumerable<String> filenames)
        {
            this.errors = new List<BufferedMessage>();
            foreach (string filename in filenames)
            {
                this.DeserialiseUniversalLog(filename);
            }
        }

        /// <summary>
        /// Initialises a new instance of the 
        /// RSG.UniversalLog.UniversalLogListener class.
        /// </summary>
        /// <param name="filename">
        /// A path to the universal log file that
        /// you want to watch.
        /// </param>
        public UniversalLogFileListener(string filename)
        {
            this.errors = new List<BufferedMessage>();
            this.DeserialiseUniversalLog(filename);
        }
        #endregion

        #region Properties
        /// <summary>
        /// Whether there were any errors while parsing the files.
        /// </summary>
        public bool HasParseErrors
        {
            get { return errors.Count > 0; }
        }

        public IEnumerable<InMemoryUniversalLogTarget.BufferedMessage> ParseErrors
        {
            get
            {
                foreach (InMemoryUniversalLogTarget.BufferedMessage message in this.errors)
                {
                    yield return message;
                }
            }
        }

        /// <summary>
        /// Gets a value that indicates whether there were any errors while parsing the files.
        /// </summary>
        public bool HasErrors
        {
            get;
            private set;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Creates a single data table populated by
        /// the messages inside the specified filename.
        /// </summary>
        /// <param name="filename">
        /// The file to deserialise.
        /// </param>
        private void DeserialiseUniversalLog(string filename)
        {
            using (FileStream stream = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                if (stream.Length == 0)
                {
                    return;
                }

                DeserialiseUniversalLog(filename, stream);
            }
        }

        private void DeserialiseUniversalLog(string filename, Stream stream)
        {
            if (!File.Exists(filename))
            {
                return;
            }

            string appContext = null;
            string name = Path.GetFileNameWithoutExtension(filename);
            using (XmlReader reader = XmlReader.Create(stream))
            {
                try
                {
                    reader.MoveToContent();
                    reader.ReadStartElement();

                    while (reader.MoveToContent() != XmlNodeType.None)
                    {
                        if (!reader.IsStartElement() || reader.Name != "context")
                        {
                            reader.Read();
                            continue;
                        }

                        appContext = reader.GetAttribute("name");
                        if (string.IsNullOrWhiteSpace(appContext))
                        {
                            appContext = "Default";
                        }

                        if (reader.IsEmptyElement)
                        {
                            reader.Skip();
                            continue;
                        }

                        reader.ReadStartElement();
                        while (reader.MoveToContent() != XmlNodeType.EndElement)
                        {
                            if (!reader.IsStartElement())
                            {
                                reader.Skip();
                            }

                            long ticks;
                            DateTime timestamp = DateTime.UtcNow;
                            if (long.TryParse(reader.GetAttribute("timestamp_ticks"), out ticks))
                            {
                                timestamp = new DateTime(ticks);
                            }

                            string context = reader.GetAttribute("context");
                            if (context == null)
                            {
                                context = string.Empty;
                            }

                            string message = string.Empty;
                            LogLevel level = LogLevelUtils.GetLogLevelFromXmlElementName(reader.Name);
                            if (!reader.IsEmptyElement)
                            {
                                reader.ReadStartElement();
                                if (reader.NodeType == XmlNodeType.Text)
                                {
                                    message = reader.ReadString();
                                    reader.ReadEndElement();
                                }
                                else
                                {
                                    reader.ReadEndElement();
                                }
                            }
                            else
                            {
                                reader.Skip();
                            }

                            this.AddLogMessage(name, appContext, context, level, message, message, timestamp);
                        }
                    }
                }
                catch (Exception ex)
                {
                    this.HasErrors = true;
                    appContext = "File Parsing Error: " + Path.GetFileName(filename);
                    this.errors.Add(new BufferedMessage(name, LogLevel.ToolException, appContext, string.Empty, ex.Message, ex.Message, DateTime.UtcNow));
                }
            }
        }

        protected override void DisposeManagedResources()
        {
        }
        #endregion
    } // RSG.UniversalLog.UniversalLogFileListener
} // RSG.UniversalLogFileListener
