﻿using System.Data;
using RSG.Base.Logging.Universal;

namespace RSG.UniversalLog.ViewModel
{
    /// <summary>
    /// Represent a message component in a universal log file.
    /// </summary>
    public class MessageViewModel : UniversalLogComponentViewModel
    {
        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="RSG.UniversalLog.ViewModel.MessageViewModel"/> class.
        /// </summary>
        /// <param name="message">
        /// The data provider for the component.
        /// </param>
        public MessageViewModel(InMemoryUniversalLogTarget.BufferedMessage message)
            : base(message)
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the component type.
        /// </summary>
        public override UniversalLogComponentTypes ComponentType
        {
            get { return UniversalLogComponentTypes.Message; }
        }
        #endregion
    }
}
