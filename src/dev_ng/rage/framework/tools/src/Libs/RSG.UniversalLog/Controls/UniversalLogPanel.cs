﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Input;

namespace RSG.UniversalLog.Controls
{

    /// <summary>
    /// Represents 
    /// </summary>
    public class UniversalLogPanel : ContentControl
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="RefreshCommand"/> dependency property.
        /// </summary>
        internal static DependencyProperty RefreshCommandProperty;

        /// <summary>
        /// Identifies the <see cref="SendEmailCommand"/> dependency property.
        /// </summary>
        internal static DependencyProperty SendEmailCommandProperty;

        /// <summary>
        /// Identifies the <see cref="ShowErrors"/> dependency property.
        /// </summary>
        internal static DependencyProperty ShowErrorsProperty;

        /// <summary>
        /// Identifies the <see cref="ShowWarnings"/> dependency property.
        /// </summary>
        internal static DependencyProperty ShowWarningsProperty;

        /// <summary>
        /// Identifies the <see cref="ShowDebugMessages"/> dependency property.
        /// </summary>
        internal static DependencyProperty ShowDebugMessagesProperty;

        /// <summary>
        /// Identifies the <see cref="ShowMessages"/> dependency property.
        /// </summary>
        internal static DependencyProperty ShowMessagesProperty;

        /// <summary>
        /// Identifies the <see cref="ShowAppContext"/> dependency property.
        /// </summary>
        internal static DependencyProperty ShowAppContextProperty;

        /// <summary>
        /// Identifies the <see cref="ShowMessageContext"/> dependency property.
        /// </summary>
        internal static DependencyProperty ShowMessageContextProperty;

        /// <summary>
        /// Identifies the <see cref="ShowGrid"/> dependency property.
        /// </summary>
        internal static DependencyProperty ShowGridProperty;

        /// <summary>
        /// Identifies the <see cref="WordWrap"/> dependency property.
        /// </summary>
        internal static DependencyProperty WordWrapProperty;

        /// <summary>
        /// Identifies the <see cref="ShowGridTextOption"/> dependency property.
        /// </summary>
        internal static DependencyProperty ShowGridTextOptionProperty;

        /// <summary>
        /// Identifies the <see cref="ShowWordWrapOption"/> dependency property.
        /// </summary>
        internal static DependencyProperty ShowWordWrapOptionProperty;

        /// <summary>
        /// Identifies the <see cref="ShowRefreshOption"/> dependency property.
        /// </summary>
        internal static DependencyProperty ShowRefreshOptionProperty;

        /// <summary>
        /// Identifiers the <see cref="ShowProfile"/> dependency property.
        /// </summary>
        internal static DependencyProperty ShowProfileCommandProperty;

        /// <summary>
        /// Identifies the <see cref="ShowProfileOption"/> dependency property.
        /// </summary>
        internal static DependencyProperty ShowProfileOptionProperty;

        /// <summary>
        /// Identifiers the <see cref="ClearLogCommand"/> dependency property.
        /// </summary>
        internal static DependencyProperty ClearLogCommandProperty;

        /// <summary>
        /// Identifies the <see cref="ShowClearOption"/> dependency property.
        /// </summary>
        internal static DependencyProperty ShowClearOptionProperty;
        #endregion

        #region Constructors
        /// <summary>
        /// First time initialisation of the
        /// <see cref="UniversalLogViewer.Controls.UniversalLogControl"/> class.
        /// </summary>
        static UniversalLogPanel()
        {
            RefreshCommandProperty =
                DependencyProperty.Register(
                "RefreshCommand",
                typeof(ICommand),
                typeof(UniversalLogPanel),
                new UIPropertyMetadata(null));

            SendEmailCommandProperty =
                DependencyProperty.Register(
                "SendEmailCommand",
                typeof(ICommand),
                typeof(UniversalLogPanel),
                new UIPropertyMetadata(null));

            ShowProfileCommandProperty =
               DependencyProperty.Register(
               "ShowProfileCommand",
               typeof(ICommand),
               typeof(UniversalLogPanel),
               new UIPropertyMetadata(null));

            ShowErrorsProperty =
                DependencyProperty.Register(
                "ShowErrors",
                typeof(bool),
                typeof(UniversalLogPanel),
                new FrameworkPropertyMetadata(true));

            ShowWarningsProperty =
                DependencyProperty.Register(
                "ShowWarnings",
                typeof(bool),
                typeof(UniversalLogPanel),
                new FrameworkPropertyMetadata(true));

            ShowDebugMessagesProperty =
                DependencyProperty.Register(
                "ShowDebugMessages",
                typeof(bool),
                typeof(UniversalLogPanel),
                new FrameworkPropertyMetadata(true));

            ShowMessagesProperty =
                DependencyProperty.Register(
                "ShowMessages",
                typeof(bool),
                typeof(UniversalLogPanel),
                new FrameworkPropertyMetadata(true));

            ShowAppContextProperty =
                DependencyProperty.Register(
                "ShowAppContext",
                typeof(bool),
                typeof(UniversalLogPanel),
                new FrameworkPropertyMetadata(false));

            ShowMessageContextProperty =
                DependencyProperty.Register(
                "ShowMessageContext",
                typeof(bool),
                typeof(UniversalLogPanel),
                new FrameworkPropertyMetadata(true));

            ShowGridProperty =
                DependencyProperty.Register(
                "ShowGrid",
                typeof(bool),
                typeof(UniversalLogPanel),
                new FrameworkPropertyMetadata(true));

            WordWrapProperty =
                DependencyProperty.Register(
                "WordWrap",
                typeof(bool),
                typeof(UniversalLogPanel),
                new FrameworkPropertyMetadata(false));

            ShowGridTextOptionProperty =
                DependencyProperty.Register(
                "ShowGridTextOption",
                typeof(bool),
                typeof(UniversalLogPanel),
                new FrameworkPropertyMetadata(true));

            ShowWordWrapOptionProperty =
                DependencyProperty.Register(
                "ShowWordWrapOption",
                typeof(bool),
                typeof(UniversalLogPanel),
                new FrameworkPropertyMetadata(true));

            ShowRefreshOptionProperty =
                DependencyProperty.Register(
                "ShowRefreshOption",
                typeof(bool),
                typeof(UniversalLogPanel),
                new FrameworkPropertyMetadata(true));

            ShowProfileOptionProperty =
                DependencyProperty.Register(
                "ShowProfileOption",
                typeof(bool),
                typeof(UniversalLogPanel),
                new FrameworkPropertyMetadata(true));

            ClearLogCommandProperty =
                DependencyProperty.Register(
                "ClearLogCommand",
                typeof(ICommand),
                typeof(UniversalLogPanel),
                new UIPropertyMetadata(null));
            
            ShowClearOptionProperty =
                DependencyProperty.Register(
                "ShowClearOption",
                typeof(bool),
                typeof(UniversalLogPanel),
                new FrameworkPropertyMetadata(false));

            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(UniversalLogPanel),
                new FrameworkPropertyMetadata(typeof(UniversalLogPanel)));
        }

        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="UniversalLogViewer.Controls.UniversalLogPanel"/> class.
        /// </summary>
        public UniversalLogPanel()
        {
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the command to execute when the user presses the refresh button.
        /// </summary>
        public ICommand RefreshCommand
        {
            get { return (ICommand)GetValue(RefreshCommandProperty); }
            set { SetValue(RefreshCommandProperty, value); }
        }

        /// <summary>
        /// Gets or sets the command to execute when the user presses the send email button.
        /// </summary>
        public ICommand SendEmailCommand
        {
            get { return (ICommand)GetValue(SendEmailCommandProperty); }
            set { SetValue(SendEmailCommandProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control should show the option to
        /// refresh the contents.
        /// </summary>
        public ICommand ShowProfileCommand
        {
            get { return (ICommand)GetValue(ShowProfileCommandProperty); }
            set { SetValue(ShowProfileCommandProperty, value); }
        }
        
        /// <summary>
        /// Gets or sets a value indicating whether this control signals that the error
        /// messages should be shown.
        /// </summary>
        public bool ShowErrors
        {
            get { return (bool)GetValue(ShowErrorsProperty); }
            set { SetValue(ShowErrorsProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control signals that the warning
        /// messages should be shown.
        /// </summary>
        public bool ShowWarnings
        {
            get { return (bool)GetValue(ShowWarningsProperty); }
            set { SetValue(ShowWarningsProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control signals that the debug
        /// messages should be shown.
        /// </summary>
        public bool ShowDebugMessages
        {
            get { return (bool)GetValue(ShowDebugMessagesProperty); }
            set { SetValue(ShowDebugMessagesProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control signals that the normal
        /// messages should be shown.
        /// </summary>
        public bool ShowMessages
        {
            get { return (bool)GetValue(ShowMessagesProperty); }
            set { SetValue(ShowMessagesProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control signals that the application
        /// context should be shown per message.
        /// </summary>
        public bool ShowAppContext
        {
            get { return (bool)GetValue(ShowAppContextProperty); }
            set { SetValue(ShowAppContextProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control signals that the per message
        /// context should be shown.
        /// </summary>
        public bool ShowMessageContext
        {
            get { return (bool)GetValue(ShowMessageContextProperty); }
            set { SetValue(ShowMessageContextProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control should show its items as text
        /// or in a grid.
        /// </summary>
        public bool ShowGrid
        {
            get { return (bool)GetValue(ShowGridProperty); }
            set { SetValue(ShowGridProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control should show the text
        /// representation of the items with word wrap or not.
        /// </summary>
        public bool WordWrap
        {
            get { return (bool)GetValue(WordWrapProperty); }
            set { SetValue(WordWrapProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control should show the option to
        /// toggle between the grid view and text view.
        /// </summary>
        public bool ShowGridTextOption
        {
            get { return (bool)GetValue(ShowGridTextOptionProperty); }
            set { SetValue(ShowGridTextOptionProperty, value); }
        } 

        /// <summary>
        /// Gets or sets a value indicating whether this control should show the option to
        /// toggle word wrap on and off.
        /// </summary>
        public bool ShowWordWrapOption
        {
            get { return (bool)GetValue(ShowWordWrapOptionProperty); }
            set { SetValue(ShowWordWrapOptionProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control should show the option to
        /// refresh the contents.
        /// </summary>
        public bool ShowRefreshOption
        {
            get { return (bool)GetValue(ShowRefreshOptionProperty); }
            set { SetValue(ShowRefreshOptionProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control should show the option to
        /// display the profile button.
        /// </summary>
        public bool ShowProfileOption
        {
            get { return (bool)GetValue(ShowProfileOptionProperty); }
            set { SetValue(ShowProfileOptionProperty, value); }
        }

        /// <summary>
        /// Gets or sets the command used to clear the log shown to the user.
        /// </summary>
        public ICommand ClearLogCommand
        {
            get { return (ICommand)GetValue(ClearLogCommandProperty); }
            set { SetValue(ClearLogCommandProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control should show the option to
        /// clear the log.
        /// </summary>
        public bool ShowClearOption
        {
            get { return (bool)GetValue(ShowClearOptionProperty); }
            set { SetValue(ShowClearOptionProperty, value); }
        }
        
        #endregion
    }
}
