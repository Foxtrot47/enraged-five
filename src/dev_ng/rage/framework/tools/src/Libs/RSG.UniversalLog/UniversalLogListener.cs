﻿using System;                                                                                              
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Data;
using System.Xml;
using System.Windows.Threading;
using System.Threading;
using System.Xml.XPath;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace RSG.UniversalLog
{
    /// <summary>
    /// Is used to listen to a specific
    /// ulog file or collection of files and keeps an updated
    /// cache of the messages in there in memory.
    /// </summary>
    public class UniversalLogListener : InMemoryUniversalLogTarget, IUniversalLogListener
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private ILog listenedToLog;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor; from a IUniversalLog object.
        /// </summary>
        /// <param name="log"></param>
        public UniversalLogListener(IUniversalLog log)
        {
            this.listenedToLog = log;
            this.Connect(this.listenedToLog);
        }
        #endregion

        #region Controller Methods
        /// <summary>
        /// Connect to a log object.
        /// </summary>
        /// <param name="log"></param>
        public void Connect(ILog log)
        {
            if (null == log)
                return;

            log.LogMessage += this.HandleLogMessage;
            log.LogError += this.HandleLogMessage;
            log.LogWarning += this.HandleLogMessage;
            log.LogProfile += this.HandleLogMessage;
#if DEBUG
            log.LogDebug += this.HandleLogMessage;
#endif // DEBUG
        }

        /// <summary>
        /// Disconnect from a log object.
        /// </summary>
        /// <param name="log"></param>
        public void Disconnect(ILog log)
        {
            log.LogMessage -= this.HandleLogMessage;
            log.LogError -= this.HandleLogMessage;
            log.LogWarning -= this.HandleLogMessage;
            log.LogProfile -= this.HandleLogMessage;
#if DEBUG
            log.LogDebug -= this.HandleLogMessage;
#endif // DEBUG
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void DisposeManagedResources()
        {
            if (this.listenedToLog == null)
            {
                return;
            }

            this.Disconnect(this.listenedToLog);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Event handler for log messages (when connected to an ILog).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HandleLogMessage(object sender, LogMessageEventArgs e)
        {
            string name = string.Empty;
            ILog log = sender as ILog;
            if (log != null)
            {
                name = log.Name;
            }

            this.AddLogMessage(name, e);
        }
        #endregion // Private Methods
    } // UniversalLogListener
} // RSG.UniversalLog
