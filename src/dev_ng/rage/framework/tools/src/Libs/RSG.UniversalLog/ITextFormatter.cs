﻿using System.Windows.Documents;

namespace RSG.UniversalLog
{
    public interface ITextFormatter
    {
        string GetText(FlowDocument document);
        void SetText(FlowDocument document, string text);
    }
}
