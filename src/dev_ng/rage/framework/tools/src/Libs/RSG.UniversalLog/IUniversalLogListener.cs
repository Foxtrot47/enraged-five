﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Xml;
using RSG.Base.Logging.Universal;

namespace RSG.UniversalLog
{

    /// <summary>
    /// Universal Log listener interface; defines the core interface for all
    /// log listeners.
    /// </summary>
    public interface IUniversalLogListener
    {
        #region Events
        /// <summary>
        /// Event raised when the log being listened to changes; specifying
        /// the data changing.
        /// </summary>
        event EventHandler<UniversalBufferChangedEventArgs> UniversalBufferChanged;
        #endregion // Events

        #region Properties
        /// <summary>
        /// Gets a iterator around the buffered messages currently in memory.
        /// </summary>
        IEnumerable<InMemoryUniversalLogTarget.BufferedMessage> Buffer { get; }
        #endregion
        #region Methods
        void Clear();

        void Serialise(XmlWriter writer);
        #endregion
    }

    /// <summary>
    /// Universal Log Listener updated event argument class.
    /// </summary>
    public class UniversalLogListenerEventArgs : EventArgs
    {
        #region Properties
        /// <summary>
        /// Data row specifying log data change.
        /// </summary>
        public IEnumerable<DataRow> Data
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data"></param>
        public UniversalLogListenerEventArgs(DataRow data)
        {
            this.Data = Enumerable.Repeat(data, 1);
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="data"></param>
        public UniversalLogListenerEventArgs(IEnumerable<DataRow> data)
        {
            this.Data = Enumerable.Distinct(data);
        }
        #endregion // COnstructor(s)
    }

} // RSG.UniversalLog namespace
