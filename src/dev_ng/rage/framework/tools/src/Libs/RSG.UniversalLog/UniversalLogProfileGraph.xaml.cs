﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using RSG.UniversalLog.ViewModel;

namespace RSG.UniversalLog
{
    /// <summary>
    /// Interaction logic for UniversalLogProfileGraph.xaml
    /// </summary>
    public partial class UniversalLogProfileGraph : UserControl
    {
        #region Fields
        /// <summary>
        /// Cached view model object for this control.
        /// </summary>
        private UniversalLogProfileGraphViewModel profileViewModel;
        #endregion

        #region Constructors
        public UniversalLogProfileGraph()
        {
            profileViewModel = new UniversalLogProfileGraphViewModel();
            this.DataContext = profileViewModel;

            InitializeComponent();
        }
        #endregion

        #region Events
        /// <summary>
        /// Event handler for selection changed in the dropdown combo box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewSelection_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox comboBox = (ComboBox)sender;
            profileViewModel.SetDataView(comboBox.SelectedIndex);

            if (this.IsLoaded == true)
            {
                if (comboBox.SelectedIndex == (int)UniversalLogProfileGraphViewModel.ViewSelectionEnum.DEFAULT_BAR_CHART)
                {
                    barchart.Visibility = Visibility.Visible;
                    piechart.Visibility = Visibility.Hidden;
                    hierarchical.Visibility = Visibility.Hidden;
                }
                else if (comboBox.SelectedIndex == (int)UniversalLogProfileGraphViewModel.ViewSelectionEnum.CONGLOMERATED_BAR_CHART)
                {
                    barchart.Visibility = Visibility.Visible;
                    piechart.Visibility = Visibility.Hidden;
                    hierarchical.Visibility = Visibility.Hidden;
                }
                else if (comboBox.SelectedIndex == (int)UniversalLogProfileGraphViewModel.ViewSelectionEnum.DEFAULT_PIE_CHART)
                {
                    barchart.Visibility = Visibility.Hidden;
                    piechart.Visibility = Visibility.Visible;
                    hierarchical.Visibility = Visibility.Hidden;
                }
                else if (comboBox.SelectedIndex == (int)UniversalLogProfileGraphViewModel.ViewSelectionEnum.HIERARCHICAL_CHART)
                {
                    barchart.Visibility = Visibility.Hidden;
                    piechart.Visibility = Visibility.Hidden;
                    hierarchical.Visibility = Visibility.Visible;
                }
            }
        }
        #endregion
    }
}
