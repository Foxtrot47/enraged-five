﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Data;
using System.Globalization;

namespace RSG.UniversalLog.Controls
{
    /// <summary>
    /// </summary>
    public class UniversalLogControl : ItemsControl
    {
        #region Fields
        /// <summary>
        /// Identifies the <see cref="ShowGrid"/> dependency property.
        /// </summary>
        internal static DependencyProperty ShowGridProperty;

        /// <summary>
        /// Identifies the <see cref="WordWrap"/> dependency property.
        /// </summary>
        internal static DependencyProperty WordWrapProperty;

        /// <summary>
        /// Identifies the <see cref="SelectedText"/> dependency property.
        /// </summary>
        internal static DependencyProperty SelectedTextProperty;

        /// <summary>
        /// Identifies the <see cref="ShowAppContext"/> dependency property.
        /// </summary>
        internal static DependencyProperty ShowAppContextProperty;

        /// <summary>
        /// Identifies the <see cref="ShowMessageContext"/> dependency property.
        /// </summary>
        internal static DependencyProperty ShowMessageContextProperty;

        /// <summary>
        /// Private reference to the rich text box defined in the control template.
        /// </summary>
        RichTextBox richTextBox;

        /// <summary>
        /// Private reference to the data grid defined in the control template.
        /// </summary>
        DataGrid dataGrid;

        /// <summary>
        /// 
        /// </summary>
        private List<object> selectedItems;

        /// <summary>
        /// Private field used for the <see cref="CopyCommand"/> property.
        /// </summary>
        private SimpleCommand copyCommand;
        #endregion

        #region Constructors
        /// <summary>
        /// First time initialisation of the
        /// <see cref="UniversalLogViewer.Controls.UniversalLogControl"/> class.
        /// </summary>
        static UniversalLogControl()
        {
            ShowGridProperty =
                DependencyProperty.Register(
                "ShowGrid",
                typeof(bool),
                typeof(UniversalLogControl),
                new FrameworkPropertyMetadata(true, ShowGridChanged));

            WordWrapProperty =
                DependencyProperty.Register(
                "WordWrap",
                typeof(bool),
                typeof(UniversalLogControl), new FrameworkPropertyMetadata(false));

            SelectedTextProperty = 
                DependencyProperty.Register(
                    "SelectedText",
                    typeof(string),
                    typeof(UniversalLogControl),
                    new FrameworkPropertyMetadata(string.Empty));

            ShowAppContextProperty =
                DependencyProperty.Register(
                "ShowAppContext",
                typeof(bool),
                typeof(UniversalLogControl),
                new FrameworkPropertyMetadata(false));

            ShowMessageContextProperty =
                DependencyProperty.Register(
                "ShowMessageContext",
                typeof(bool),
                typeof(UniversalLogControl),
                new FrameworkPropertyMetadata(true));


            DefaultStyleKeyProperty.OverrideMetadata(
                typeof(UniversalLogControl),
                new FrameworkPropertyMetadata(typeof(UniversalLogControl)));
        }

        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="UniversalLogViewer.Controls.UniversalLogControl"/> class.
        /// </summary>
        public UniversalLogControl()
        {
            this.selectedItems = new List<object>();
        }
        #endregion

        #region Events
        public event SelectionChangedEventHandler SelectionChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets a value indicating whether this control should show its items as text
        /// or in a grid.
        /// </summary>
        public bool ShowGrid
        {
            get { return (bool)GetValue(ShowGridProperty); }
            set { SetValue(ShowGridProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control should show the text
        /// representation of the items with word wrap or not.
        /// </summary>
        public bool WordWrap
        {
            get { return (bool)GetValue(WordWrapProperty); }
            set { SetValue(WordWrapProperty, value); }
        }

        /// <summary>
        /// Gets or sets the text that is currently selected in this control.
        /// </summary>
        public string SelectedText
        {
            get { return (string)GetValue(SelectedTextProperty); }
            set { SetValue(SelectedTextProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control signals that the application
        /// context should be shown per message.
        /// </summary>
        public bool ShowAppContext
        {
            get { return (bool)GetValue(ShowAppContextProperty); }
            set { SetValue(ShowAppContextProperty, value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this control signals that the per message
        /// context should be shown.
        /// </summary>
        public bool ShowMessageContext
        {
            get { return (bool)GetValue(ShowMessageContextProperty); }
            set { SetValue(ShowMessageContextProperty, value); }
        }

        /// <summary>
        /// Gets the currently selected grid view items.
        /// </summary>
        public List<object> SelectedItems
        {
            get { return this.selectedItems; }
        }

        public object[] SelectedItemsAsArray
        {
            get
            {
                object[] array = new object[this.SelectedItems.Count];
                for (int i = 0; i < this.SelectedItems.Count; i++)
                {
                    array[i] = this.SelectedItems[i];
                }

                return array;
            }
        }

        public SimpleCommand CopyCommand
        {
            get
            {
                if (this.copyCommand == null)
                {
                    this.copyCommand = new SimpleCommand(this.Copy);
                }

                return this.copyCommand;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Is invoked whenever application code or internal processes call
        /// System.Windows.FrameworkElement.ApplyTemplate().
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.richTextBox = this.GetTemplateChild("PART_Text") as RichTextBox;
            if (this.richTextBox != null)
            {
                // Desired effect: The rich text box is a control and we want the text contained within
                // to have the SAME font as the UI. So, assigning the document the same font family and
                // size will give us the same font in the rich text box as we have for the rest of the UI.
                richTextBox.Document.FontFamily = richTextBox.FontFamily;
                richTextBox.Document.FontSize = richTextBox.FontSize;
                this.richTextBox.SelectionChanged += text_SelectionChanged;
            }

            this.dataGrid = this.GetTemplateChild("PART_Grid") as DataGrid;
            if (this.dataGrid != null)
            {
                foreach (DataGridColumn col in this.dataGrid.Columns)
                {
                    col.SetValue(FrameworkElement.DataContextProperty, this);
                }

                this.dataGrid.SelectionChanged += grid_SelectionChanged;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void grid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.SetSelectionBasedOnGrid();

            if (this.SelectionChanged != null)
                this.SelectionChanged(this, e);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void text_SelectionChanged(object sender, RoutedEventArgs e)
        {
            this.SetSelectionBasedOnText();
        }

        private static void ShowGridChanged(DependencyObject s, DependencyPropertyChangedEventArgs e)
        {
            if ((bool)e.NewValue)
            {
                (s as UniversalLogControl).SetSelectionBasedOnGrid();
            }
            else
            {
                (s as UniversalLogControl).SetSelectionBasedOnText();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void SetSelectionBasedOnText()
        {
            if (this.richTextBox == null)
            {
                this.SelectedText = string.Empty;
            }
            else
            {
                this.SelectedText = this.richTextBox.Selection.Text;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void SetSelectionBasedOnGrid()
        {
            if (this.dataGrid == null)
            {
                this.SelectedText = string.Empty;
            }
            else
            {
                string selectedText = string.Empty;
                this.selectedItems.Clear();
                foreach (object selection in this.dataGrid.SelectedItems)
                {
                    this.selectedItems.Add(selection);
                    if (!(selection is ViewModel.UniversalLogComponentViewModel))
                        continue;

                    ViewModel.UniversalLogComponentViewModel vm = selection as ViewModel.UniversalLogComponentViewModel;

                    string msgFmt = Controls.ParagraphExtension.msgFmt + "\n";
                    selectedText += string.Format(msgFmt, vm.TimeStamp, vm.Type, vm.Context, vm.Message);
                }
                this.SelectedText = selectedText;
            }
        }

        /// <summary>
        /// Gets called when the <see cref="CopyCommand"/> get executed by the user.
         /// </summary>
        private void Copy()
        {
            Clipboard.SetText(this.SelectedText);
        }

        protected override void OnContextMenuOpening(ContextMenuEventArgs e)
        {
            if (this.ShowGrid == false)
            {
                base.OnContextMenuOpening(e);
                return;
            }

            this.ContextMenu = new ContextMenu();
            this.ContextMenu.Items.Add(new MenuItem() { Header = "Copy", Command = this.CopyCommand });

            base.OnContextMenuOpening(e);
        }
        #endregion
    }

    public sealed class StringDateFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime date = (DateTime)value;
            string result = date.ToString(culture);

            return date.ToString(culture);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }
}
