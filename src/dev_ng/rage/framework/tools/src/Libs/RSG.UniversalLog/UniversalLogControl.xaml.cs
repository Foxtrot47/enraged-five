﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace RSG.UniversalLog
{
    /// <summary>
    /// Interaction logic for UniversalLogControl.xaml
    /// </summary>
    public partial class UniversalLogControl : UserControl
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public UniversalLogControl()
        {
            InitializeComponent();
        }
        #endregion // Constructor(s)
    }

} // RSG.UniversalLog namespace
