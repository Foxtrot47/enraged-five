﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.UniversalLog
{
    public interface IFilterDataModel
    {
        #region Properties
        Dictionary<string, IUniversalLogListener> Listeners
        { get; }
        #endregion
    }
}
