﻿namespace RSG.UniversalLog.ViewModel
{
    /// <summary>
    /// The different types of components found in a universal log file.
    /// </summary>
    public enum UniversalLogComponentTypes
    {
        Error,
        Warning,
        Message,
        DebugMessage,
        Profiling,
    }
}
