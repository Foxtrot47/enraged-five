﻿using System.Data;
using RSG.Base.Logging.Universal;

namespace RSG.UniversalLog.ViewModel
{
    /// <summary>
    /// Represent a debug message component in a universal log file.
    /// </summary>
    public class ProfilingViewModel : UniversalLogComponentViewModel
    {
        #region Fields
        private bool isEnd;
        #endregion
        
        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="RSG.UniversalLog.ViewModel.ProfilingViewModel"/> class.
        /// </summary>
        /// <param name="message">
        /// The data provider for the component.
        /// </param>
        public ProfilingViewModel(InMemoryUniversalLogTarget.BufferedMessage message, bool end)
            : base(message)
        {
            isEnd = end;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the component type.
        /// </summary>
        public override UniversalLogComponentTypes ComponentType
        {
            get { return UniversalLogComponentTypes.Profiling; }
        }

        /// <summary>
        /// Returns whether this profiling model is to denote a start or an end.
        /// </summary>
        public bool IsProfileEnd
        {
            get { return isEnd; }
        }
        #endregion
    }
}