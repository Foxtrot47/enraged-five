﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System;
using RSG.Base.Logging.Universal;
using RSG.Base.Logging;
using System.Globalization;

namespace RSG.UniversalLog.ViewModel
{
    /// <summary>
    /// Provides a base class for all the message view models
    /// </summary>
    public abstract class UniversalLogComponentViewModel : INotifyPropertyChanged
    {
        #region Fields
        /// <summary>
        /// The private field for the <see cref="Data"/> property.
        /// </summary>
        private InMemoryUniversalLogTarget.BufferedMessage data;

        /// <summary>
        /// The private field for the <see cref="Visible"/> property.
        /// </summary>
        private bool notVisible;
        #endregion

        #region Constructor
        /// <summary>
        /// Initialises a new instance of the
        /// <see cref="RSG.UniversalLog.ViewModel.UniversalLogComponentViewModel"/> class.
        /// </summary>
        /// <param name="message">
        /// The data provider for the component.
        /// </param>
        protected UniversalLogComponentViewModel(InMemoryUniversalLogTarget.BufferedMessage message)
        {
            this.data = message;
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the row that defines the data for this component.
        /// </summary>
        public InMemoryUniversalLogTarget.BufferedMessage Data
        {
            get
            {
                return this.data;
            }

            set
            {
                string[] changedValues =
                {
                    "Row",
                    "Message",
                    "Type",
                    "TimeStamp",
                    "Context",
                };
                this.SetProperty(ref this.data, value, changedValues);
            }
        }

        /// <summary>
        /// Gets the message associated with this component.
        /// </summary>
        public string Message
        {
            get
            {
                string message = this.data.Message;
                if (message == null)
                {
                    return null;
                }

                message = message.Replace("\n", "");
                message = message.Replace("&amp;", "&");
                message = message.Replace("&lt;", "<");
                message = message.Replace("&gt;", ">");
                message = message.Replace("&apos;", "'");
                message = message.Replace("&quot;", "\"");
                return message;
            }
        }

        /// <summary>
        /// Gets the type associated with this component.
        /// </summary>
        public string Type
        {
            get { return this.data.Level.ToString().ToLower(); }
        }

        /// <summary>
        /// Gets the timestamp associated with this component.
        /// </summary>
        public string TimeStamp
        {
            get
            {
                DateTime dateTime = this.data.Timestamp.ToLocalTime();
                return dateTime.ToString();
            }
        }

        /// <summary>
        /// Gets the timestamp associated with this component.
        /// </summary>
        public DateTime DateTime
        {
            get
            {
                return this.data.Timestamp.ToLocalTime();
            }
        }

        /// <summary>
        /// Gets the context this component has.
        /// </summary>
        public string Context
        {
            get { return this.data.ApplicationContext; }
        }

        /// <summary>
        /// Gets the context this component has.
        /// </summary>
        public string ObjectContext
        {
            get { return this.data.Context; }
        }

        /// <summary>
        /// Gets a value indicating whether the component is currently visible to the user
        /// based on filtering etc.
        /// </summary>
        public bool NotVisible
        {
            get { return this.notVisible; }
            set { this.SetProperty(ref this.notVisible, value, "NotVisible"); }
        }

        /// <summary>
        /// Gets the component type.
        /// </summary>
        public abstract UniversalLogComponentTypes ComponentType { get; }
        #endregion

        #region Methods
        public static UniversalLogComponentViewModel Create(InMemoryUniversalLogTarget.BufferedMessage message)
        {
            LogLevel level = message.Level;
            switch (level)
            {
                case LogLevel.Debug:
                    return new DebugMessageViewModel(message);
                case LogLevel.Error:
                case LogLevel.ToolError:
                case LogLevel.ToolException:
                    return new ErrorViewModel(message);
                case LogLevel.Info:
                    return new MessageViewModel(message);
                case LogLevel.Profile:
                    return new ProfilingViewModel(message, false);
                case LogLevel.ProfileEnd:
                    return new ProfilingViewModel(message, true);
                case LogLevel.Warning:
                    return new WarningViewModel(message);
            }

            return null;
        }

        /// <summary>
        /// Sets the given field to the given value making sure the correct events are fired.
        /// </summary>
        /// <typeparam name="T">
        /// The type of the property we wish to set.
        /// </typeparam>
        /// <param name="field">
        /// A reference to the field we wish to set.
        /// </param>
        /// <param name="value">
        /// The value we wish to set the field to.
        /// </param>
        /// <param name="names">
        /// A array of property names that are changed due to this one change.
        /// </param>
        private void SetProperty<T>(ref T field, T value, params string[] names)
        {
            if (EqualityComparer<T>.Default.Equals(field, value))
                return;

            field = value;
            PropertyChangedEventHandler handler = this.PropertyChanged;
            if (handler == null || names == null)
                return;

            foreach (string name in names)
                handler(this, new PropertyChangedEventArgs(name));
        }
        #endregion
    }
}
