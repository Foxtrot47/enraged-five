﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace RSG.Rag.Compression.Tests
{
    [TestClass]
    public class CompressionMethodTests
    {
        [TestMethod]
        public void NoCompressionMethod_EncodeDecodeSameAsOriginal()
        {
            byte[] buffer = GetBufferFromFile("Data\\testfile.dat");
            ICompressionMethod method = new NoCompressionMethod();

            // Make sure that the encoded data is the same as the normal data.
            byte[] encodedData = method.Encode(buffer);
            CollectionAssert.AreEquivalent(buffer, encodedData);

            // Make sure that the decoded data is the same as the encoded data.
            byte[] decodedData = method.Decode(encodedData, buffer.Length);
            CollectionAssert.AreEquivalent(decodedData, encodedData);
        }
        
        [TestMethod]
        public void NoCompressionMethod_Roundtrip()
        {
            TestCompressionRoundtrip(new NoCompressionMethod());
        }

        [TestMethod]
        public void DatCompressionMethod_Roundtrip()
        {
            TestCompressionRoundtrip(new DatCompressionMethod());
        }

        [TestMethod]
        public void FiCompressionMethod_Roundtrip()
        {
            TestCompressionRoundtrip(new FiCompressionMethod(16, false));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private byte[] GetBufferFromFile(String filename)
        {
            using (FileStream stream = File.OpenRead(filename))
            {
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    return reader.ReadBytes((int)stream.Length);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="method"></param>
        private void TestCompressionRoundtrip(ICompressionMethod method)
        {
            byte[] buffer = GetBufferFromFile("Data\\testfile.dat");

            // Make sure that the decoded version of the data is the same as the original.
            byte[] encodedData = method.Encode(buffer);
            byte[] decodedData = method.Decode(encodedData, buffer.Length);
            CollectionAssert.AreEquivalent(decodedData, buffer);
        }
    }
}
