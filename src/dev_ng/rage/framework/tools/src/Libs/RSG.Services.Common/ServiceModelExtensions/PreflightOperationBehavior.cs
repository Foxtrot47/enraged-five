﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;

namespace RSG.Services.Common.ServiceModelExtensions
{
    /// <summary>
    /// 
    /// </summary>
    internal class PreflightOperationBehavior : IOperationBehavior
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private OperationDescription _preflightOperation;

        /// <summary>
        /// 
        /// </summary>
        private IList<string> _allowedMethods;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="preflightOperation"></param>
        public PreflightOperationBehavior(OperationDescription preflightOperation)
        {
            this._preflightOperation = preflightOperation;
            this._allowedMethods = new List<string>();
        }
        #endregion // Constructor(s)

        #region Public Interface
        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpMethod"></param>
        public void AddAllowedMethod(string httpMethod)
        {
            this._allowedMethods.Add(httpMethod);
        }
        #endregion // Public Interface

        #region IOperationBehaviour Implementation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationDescription"></param>
        /// <param name="bindingParameters"></param>
        public void AddBindingParameters(OperationDescription operationDescription, BindingParameterCollection bindingParameters)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationDescription"></param>
        /// <param name="clientOperation"></param>
        public void ApplyClientBehavior(OperationDescription operationDescription, ClientOperation clientOperation)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationDescription"></param>
        /// <param name="dispatchOperation"></param>
        public void ApplyDispatchBehavior(OperationDescription operationDescription, DispatchOperation dispatchOperation)
        {
            dispatchOperation.Invoker = new PreflightOperationInvoker(operationDescription.Messages[1].Action, this._allowedMethods);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationDescription"></param>
        public void Validate(OperationDescription operationDescription)
        {
        }
        #endregion // IOperationBehaviour Implementation
    } // PreflightOperationBehavior
}
