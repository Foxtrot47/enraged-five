﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;

namespace RSG.Services.Common.ServiceModelExtensions
{
    /// <summary>
    /// Attribute to tag up whether an endpoint should support CORS requests.
    /// Implements IOperationBehavior for easy access from within the custom CorsEnabledServiceHost.
    /// 
    /// Code based on the following article:
    /// http://code.msdn.microsoft.com/Implementing-CORS-support-c1f9cd4b
    /// </summary>
    public class CorsEnabledAttribute : Attribute, IOperationBehavior
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationDescription"></param>
        /// <param name="bindingParameters"></param>
        public void AddBindingParameters(OperationDescription operationDescription, BindingParameterCollection bindingParameters)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationDescription"></param>
        /// <param name="clientOperation"></param>
        public void ApplyClientBehavior(OperationDescription operationDescription, ClientOperation clientOperation)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationDescription"></param>
        /// <param name="dispatchOperation"></param>
        public void ApplyDispatchBehavior(OperationDescription operationDescription, DispatchOperation dispatchOperation)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="operationDescription"></param>
        public void Validate(OperationDescription operationDescription)
        {
        }
    } // CorsEnabledAttribute
}
