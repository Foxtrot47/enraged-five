﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Configuration.Services;

namespace RSG.Services.Common.ServiceModelExtensions
{
    /// <summary>
    /// 
    /// </summary>
    public class ConfigInstanceProvider : IInstanceProvider, IContractBehavior
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private readonly IServiceHostConfig _config;

        /// <summary>
        /// 
        /// </summary>
        private readonly Type _serviceType;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ConfigInstanceProvider(IServiceHostConfig config, Type serviceType)
        {
            Debug.Assert(config != null, "Config parameter is null.");
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }

            _config = config;
            _serviceType = serviceType;
        }
        #endregion // Constructor(s)

        #region IInstanceProvider Implementation
        /// <summary>
        /// 
        /// </summary>
        public object GetInstance(InstanceContext instanceContext, Message message)
        {
            return GetInstance(instanceContext);
        }

        /// <summary>
        /// Creates the new service instance passing in the server config object.
        /// </summary>
        public object GetInstance(InstanceContext instanceContext)
        {
            return Activator.CreateInstance(_serviceType, _config);
        }

        /// <summary>
        /// 
        /// </summary>
        public void ReleaseInstance(InstanceContext instanceContext, object instance)
        {
        }
        #endregion // IInstanceProvider Implementation

        #region IContractBehavior Implementation
        /// <summary>
        /// 
        /// </summary>
        public void AddBindingParameters(ContractDescription contractDescription, ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        public void ApplyClientBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
        }

        /// <summary>
        /// Tells the dispatch runtime that this object is in charge of creating service instances.
        /// </summary>
        public void ApplyDispatchBehavior(ContractDescription contractDescription, ServiceEndpoint endpoint, DispatchRuntime dispatchRuntime)
        {
            dispatchRuntime.InstanceProvider = this;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Validate(ContractDescription contractDescription, ServiceEndpoint endpoint)
        {
            if (!_config.UsingIIS)
            {
                String modifiedAddress = endpoint.Address.Uri.ToString();

                if (endpoint.Binding is WebHttpBinding)
                {
                    WebHttpBinding webBinding = (WebHttpBinding)endpoint.Binding;
                    if (webBinding.Security.Mode == WebHttpSecurityMode.None)
                    {
                        modifiedAddress = modifiedAddress.Replace("8080", _config.HttpPort.ToString());
                    }
                    else if (webBinding.Security.Mode == WebHttpSecurityMode.Transport)
                    {
                        modifiedAddress = modifiedAddress.Replace("8081", _config.HttpsPort.ToString());
                    }
                    else
                    {
                        throw new NotSupportedException("Unsupported security mode encountered.");
                    }
                }
                else if (endpoint.Binding is NetTcpBinding)
                {
                    modifiedAddress = modifiedAddress.Replace("9000", _config.TcpPort.ToString());
                }

                endpoint.Address = new EndpointAddress(modifiedAddress);
            }
        }
        #endregion // IContractBehavior Implementation
    } // ConfigInstanceProvider<TService>
}
