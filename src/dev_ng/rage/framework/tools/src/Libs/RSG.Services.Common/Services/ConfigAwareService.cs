﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Configuration.Services;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace RSG.Services.Common.Services
{
    /// <summary>
    /// Base class from which all services that interact with a database should inherit from.
    /// </summary>
    public abstract class ConfigAwareService
    {
        #region Fields
        /// <summary>
        /// Service level log object.
        /// </summary>
        protected readonly ILog _log;

        /// <summary>
        /// Service host configuration object.
        /// </summary>
        private readonly IServiceHostConfig _config;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ConfigAwareService(IServiceHostConfig config)
        {
            _log = LogFactory.CreateUniversalLog(GetType().FullName);
            _config = config;
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Service host configuration object.
        /// </summary>
        public IServiceHostConfig Config
        {
            get { return _config; }
        }
        #endregion // Properties
    } // ServiceBase
}
