﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using RSG.Services.Common.Services;
using RSG.Base.Configuration.Services;

namespace RSG.Services.Common.ServiceModelExtensions
{
    /// <summary>
    /// Custom service host to support changing the default base address of HTTP endpoints.
    /// </summary>
    public class ConfigServiceHost : CorsEnabledServiceHost
    {
        #region Constructor(s)
        /// <summary>
        /// Main constructor.
        /// </summary>
        /// <param name="serviceType"></param>
        public ConfigServiceHost(IServiceHostConfig config, Type serviceType, params Uri[] baseAddresses)
            : base(serviceType, baseAddresses)
        {
            foreach (var cd in this.ImplementedContracts.Values)
            {
                cd.Behaviors.Add(new ConfigInstanceProvider(config, serviceType));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public ConfigServiceHost(object singletonInstance, params Uri[] baseAddresses)
            : base(singletonInstance, baseAddresses)
        {
            Debug.Assert(singletonInstance is ConfigAwareService, "Singleton instance passed in does not inherit from ServiceBase.");
            if (!(singletonInstance is ConfigAwareService))
            {
                throw new ArgumentException("Singleton instance passed in does not inherit from ServiceBase.");
            }
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        protected override void ApplyConfiguration()
        {
            base.ApplyConfiguration();

            if (SingletonInstance != null)
            {
                ConfigAwareService serviceInstance = SingletonInstance as ConfigAwareService;

                // Check if there is an http or tcp base address.
                foreach (ServiceEndpoint endpoint in this.Description.Endpoints)
                {
                    String modifiedAddress = endpoint.Address.Uri.ToString();

                    if (endpoint.Binding is WebHttpBinding)
                    {
                        WebHttpBinding webBinding = (WebHttpBinding)endpoint.Binding;
                        if (webBinding.Security.Mode == WebHttpSecurityMode.None)
                        {
                            modifiedAddress = modifiedAddress.Replace("8080", serviceInstance.Config.HttpPort.ToString());
                        }
                        else if (webBinding.Security.Mode == WebHttpSecurityMode.Transport)
                        {
                            modifiedAddress = modifiedAddress.Replace("8081", serviceInstance.Config.HttpsPort.ToString());
                        }
                        else
                        {
                            throw new NotSupportedException("Unsupported security mode encountered.");
                        }
                    }
                    else if (endpoint.Binding is NetTcpBinding)
                    {
                        modifiedAddress = modifiedAddress.Replace("9000", serviceInstance.Config.TcpPort.ToString());
                    }

                    endpoint.Address = new EndpointAddress(modifiedAddress);
                }
            }
        }
        #endregion // Overrides
    } // ConfigServiceHost
}
