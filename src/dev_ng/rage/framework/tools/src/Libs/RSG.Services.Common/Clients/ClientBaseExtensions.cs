﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace RSG.Services.Common.Clients
{
    /// <summary>
    /// Extension methods for WCF clients.
    /// </summary>
    public static class ClientBaseExtensions
    {
        /// <summary>
        /// Closes or aborts a client based on it's current state.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="client"></param>
        public static void CloseOrAbort<TChannel>(this ClientBase<TChannel> client) where TChannel : class
        {
            if (client.State != CommunicationState.Faulted)
            {
                client.Close();
            }
            else
            {
                client.Abort();
            }
        }
    }
}
