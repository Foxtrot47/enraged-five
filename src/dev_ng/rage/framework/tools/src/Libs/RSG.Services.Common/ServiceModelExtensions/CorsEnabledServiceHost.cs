﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Text;

namespace RSG.Services.Common.ServiceModelExtensions
{
    /// <summary>
    /// Custom service host that enables CORS functionality.
    /// 
    /// Code based on the following article:
    /// http://code.msdn.microsoft.com/Implementing-CORS-support-c1f9cd4b
    /// </summary>
    public class CorsEnabledServiceHost : ServiceHost
    {
        #region Fields
        /// <summary>
        /// Type of contract that this service host is for.
        /// </summary>
        private readonly Type[] _contractTypes;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="serviceType"></param>
        /// <param name="baseAddresses"></param>
        public CorsEnabledServiceHost(Type serviceType, params Uri[] baseAddresses)
            : base(serviceType, baseAddresses)
        {
            this._contractTypes = GetContractType(serviceType);
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="singletonInstance"></param>
        /// <param name="baseAddresses"></param>
        public CorsEnabledServiceHost(object singletonInstance, params Uri[] baseAddresses)
            : base(singletonInstance, baseAddresses)
        {
            this._contractTypes = GetContractType(singletonInstance.GetType());
        }
        #endregion // Constructor(s)

        #region ServiceHost Overrides
        /// <summary>
        /// Called when the service host is opening and is used for ....
        /// </summary>
        protected override void OnOpening()
        {
            // Go over all the contract types looking for ones that have a WebHttpBinding endpoint.
            foreach (ContractDescription contract in this.ImplementedContracts.Values)
            {
                IEnumerable<ServiceEndpoint> webHttpEndpoints = GetEndpointsWithWebHttpBindingForContractType(contract);

                if (webHttpEndpoints.Any())
                {
                    List<OperationDescription> corsEnabledOperations =
                        contract.Operations
                                .Where(o => o.Behaviors.Find<CorsEnabledAttribute>() != null)
                                .ToList();

                    AddPreflightOperations(corsEnabledOperations);

                    foreach (ServiceEndpoint endpoint in webHttpEndpoints)
                    {
                        endpoint.Behaviors.Add(new EnableCorsEndpointBehavior());
                    }
                }
            }

            base.OnOpening();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceType"></param>
        /// <returns></returns>
        private Type[] GetContractType(Type serviceType)
        {
            if (HasServiceContract(serviceType))
            {
                return new Type[] { serviceType };
            }

            Type[] contractTypes = serviceType.GetInterfaces()
                .Where(i => HasServiceContract(i))
                .ToArray();

            if (contractTypes.Length == 0)
            {
                throw new InvalidOperationException("Service type " + serviceType.FullName +
                    " does not implement any interface decorated with the ServiceContractAttribute.");
            }
            else
            {
                return contractTypes;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static bool HasServiceContract(Type type)
        {
            return Attribute.IsDefined(type, typeof(ServiceContractAttribute), false);
        }

        /// <summary>
        /// I think the method name is long enough to explain what this is for :].
        /// </summary>
        /// <param name="contractType"></param>
        /// <returns></returns>
        private IEnumerable<ServiceEndpoint> GetEndpointsWithWebHttpBindingForContractType(ContractDescription contract)
        {
            // see if there are any endpoints that fulfill the criteria.
            foreach (ServiceEndpoint endpoint in this.Description.Endpoints)
            {
                if (endpoint.Contract == contract && endpoint.Binding is WebHttpBinding)
                {
                    yield return endpoint;
                }
            }
        }

        /// <summary>
        /// Adds additional methods to the service contract which will deal with the OPTIONS requests for non-GET requests.
        /// </summary>
        /// <param name="corsOperations"></param>
        private void AddPreflightOperations(List<OperationDescription> corsOperations)
        {
            Dictionary<string, PreflightOperationBehavior> uriTemplates =
                new Dictionary<string, PreflightOperationBehavior>(StringComparer.OrdinalIgnoreCase);

            foreach (var operation in corsOperations)
            {
                // No need to add preflight operation for GET requests
                if (operation.Behaviors.Find<WebGetAttribute>() != null)
                {
                    continue;
                }

                // No support for 1-way messages
                if (operation.IsOneWay)
                {
                    continue;
                }

                //
                string originalUriTemplate;
                WebInvokeAttribute originalWia = operation.Behaviors.Find<WebInvokeAttribute>();

                if (originalWia != null && originalWia.UriTemplate != null)
                {
                    originalUriTemplate = NormalizeTemplate(originalWia.UriTemplate);
                }
                else
                {
                    originalUriTemplate = operation.Name;
                }

                string originalMethod = originalWia != null && originalWia.Method != null ? originalWia.Method : "POST";

                // Check whether there already is an OPTIONS operation for this URI that we can reuse.
                if (uriTemplates.ContainsKey(originalUriTemplate))
                {
                    PreflightOperationBehavior operationBehavior = uriTemplates[originalUriTemplate];
                    operationBehavior.AddAllowedMethod(originalMethod);
                }
                else
                {
                    // Need to create a new one.
                    ContractDescription contract = operation.DeclaringContract;
                    OperationDescription preflightOperation = new OperationDescription(operation.Name + CorsConstants.PreflightSuffix, contract);

                    // Add an input message width a single body part of type Message.
                    MessageDescription inputMessage = new MessageDescription(operation.Messages[0].Action + CorsConstants.PreflightSuffix, MessageDirection.Input);
                    inputMessage.Body.Parts.Add(new MessagePartDescription("input", contract.Namespace) { Index = 0, Type = typeof(Message) });
                    preflightOperation.Messages.Add(inputMessage);

                    // Add an output message with a return value of the same type.
                    MessageDescription outputMessage = new MessageDescription(operation.Messages[1].Action + CorsConstants.PreflightSuffix, MessageDirection.Output);
                    outputMessage.Body.ReturnValue = new MessagePartDescription(preflightOperation.Name + "Return", contract.Namespace) { Type = typeof(Message) };
                    preflightOperation.Messages.Add(outputMessage);

                    // Use the normalised uri template for the new operation.
                    WebInvokeAttribute wia = new WebInvokeAttribute();
                    wia.UriTemplate = originalUriTemplate;
                    wia.Method = "OPTIONS";
                    preflightOperation.Behaviors.Add(wia);

                    // Add a DataContractSerializerOperationBehavior to the operation description, since it will give us a formatter which
                    // understands the (Message in, Message out) pattern.
                    preflightOperation.Behaviors.Add(new DataContractSerializerOperationBehavior(preflightOperation));

                    // Finally add our custom operation behavior that will deal with the preflight request.
                    PreflightOperationBehavior preflightOperationBehavior = new PreflightOperationBehavior(preflightOperation);
                    preflightOperationBehavior.AddAllowedMethod(originalMethod);
                    preflightOperation.Behaviors.Add(preflightOperationBehavior);
                    uriTemplates.Add(originalUriTemplate, preflightOperationBehavior);

                    // And now add it as an operation that the service contract supports.
                    contract.Operations.Add(preflightOperation);
                }
            }
        }

        /// <summary>
        /// Normalises a URL template (remove query string parameters and remove the parameter lists replacing them with wildcards).
        /// Two operations with similar URI templates (e.g., [WebInvoke(Method = "POST", UriTemplate = "/products/{param1}?x={param2}")]
        /// and [WebInvoke(Method = "DELETE", UriTemplate = "/products/{id}")]) will both map to the same "/products/*" URI.
        /// </summary>
        /// <param name="uriTemplate"></param>
        /// <returns></returns>
        private string NormalizeTemplate(string uriTemplate)
        {
            int queryIndex = uriTemplate.IndexOf('?');
            if (queryIndex >= 0)
            {
                // no query string used for this
                uriTemplate = uriTemplate.Substring(0, queryIndex);
            }

            int paramIndex;
            while ((paramIndex = uriTemplate.IndexOf('{')) >= 0)
            {
                // Replacing all named parameters with wildcards
                int endParamIndex = uriTemplate.IndexOf('}', paramIndex);
                if (endParamIndex >= 0)
                {
                    uriTemplate = uriTemplate.Substring(0, paramIndex) + '*' + uriTemplate.Substring(endParamIndex + 1);
                }
            }

            return uriTemplate;
        }
        #endregion // ServiceHost Overrides
    } // CorsEnabledServiceHost
}
