﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text;
using RSG.Base.Logging;

namespace RSG.Services.Common.Inspectors
{
    /// <summary>
    /// Message inspector that outputs a message in the log when a after a request is received.
    /// </summary>
    internal class LoggingMessageInspector : IClientMessageInspector, IDispatchMessageInspector
    {
        /// <summary>
        /// Log object to which to log messages.
        /// </summary>
        private readonly ILog _log;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="log"></param>
        internal LoggingMessageInspector(ILog log)
        {
            _log = log;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="channel"></param>
        /// <returns></returns>
        object IClientMessageInspector.BeforeSendRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel)
        {
            object requestMessage;
            if (request.Properties.TryGetValue(HttpRequestMessageProperty.Name, out requestMessage))
            {
                HttpRequestMessageProperty httpReq = (HttpRequestMessageProperty)requestMessage;
                Uri requestUri = request.Headers.To;
                _log.Message("Sending request to: [{0}] {1}", httpReq.Method, requestUri);
            }
            else
            {
                _log.Message("Sending request to: {0}", request.Headers.Action);
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reply"></param>
        /// <param name="correlationState"></param>
        void IClientMessageInspector.AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <param name="channel"></param>
        /// <param name="instanceContext"></param>
        /// <returns></returns>
        object IDispatchMessageInspector.AfterReceiveRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel, System.ServiceModel.InstanceContext instanceContext)
        {
            object requestMessage;
            if (request.Properties.TryGetValue(HttpRequestMessageProperty.Name, out requestMessage))
            {
                HttpRequestMessageProperty httpReq = (HttpRequestMessageProperty)requestMessage;
                Uri requestUri = request.Headers.To;
                _log.Message("Recieved HTTP request: [{0}] {1}", httpReq.Method, requestUri);
            }
            else
            {
                _log.Message("Received TCP/PIPE request: {0}", request.Headers.Action);
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reply"></param>
        /// <param name="correlationState"></param>
        void IDispatchMessageInspector.BeforeSendReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
        }
    } // LoggingMessageInspector
}
