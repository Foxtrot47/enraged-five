﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Services.Common
{
    /// <summary>
    /// 
    /// </summary>
    internal class CorsConstants
    {
        /// <summary>
        /// 
        /// </summary>
        internal const string Origin = "Origin";

        /// <summary>
        /// 
        /// </summary>
        internal const string AccessControlAllowOrigin = "Access-Control-Allow-Origin";

        /// <summary>
        /// 
        /// </summary>
        internal const string AccessControlRequestMethod = "Access-Control-Request-Method";

        /// <summary>
        /// 
        /// </summary>
        internal const string AccessControlRequestHeaders = "Access-Control-Request-Headers";

        /// <summary>
        /// 
        /// </summary>
        internal const string AccessControlAllowMethods = "Access-Control-Allow-Methods";

        /// <summary>
        /// 
        /// </summary>
        internal const string AccessControlAllowHeaders = "Access-Control-Allow-Headers";

        /// <summary>
        /// Suffix to add to preflight messages.
        /// </summary>
        internal const string PreflightSuffix = "_preflight_";
    } // CorsConstants
}
