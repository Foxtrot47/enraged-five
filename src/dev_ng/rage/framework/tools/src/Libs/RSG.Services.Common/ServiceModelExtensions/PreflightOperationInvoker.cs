﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;
using System.Text;

namespace RSG.Services.Common.ServiceModelExtensions
{
    /// <summary>
    /// Custom invoker for handling incoming CORS requests.
    /// </summary>
    internal class PreflightOperationInvoker : IOperationInvoker
    {
        #region Fields
        /// <summary>
        /// 
        /// </summary>
        private string _replyAction;

        /// <summary>
        /// 
        /// </summary>
        private IList<string> _allowedHttpMethods;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="replyAction"></param>
        /// <param name="allowedHttpMethods"></param>
        public PreflightOperationInvoker(string replyAction, IList<string> allowedHttpMethods)
        {
            this._replyAction = replyAction;
            this._allowedHttpMethods = allowedHttpMethods;
        }
        #endregion // Constructor(s)

        #region IOperationInvoker Implementation
        /// <summary>
        /// 
        /// </summary>
        public bool IsSynchronous
        {
            get { return true; }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public object[] AllocateInputs()
        {
            return new object[1];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="inputs"></param>
        /// <param name="outputs"></param>
        /// <returns></returns>
        public object Invoke(object instance, object[] inputs, out object[] outputs)
        {
            Message input = (Message)inputs[0];
            outputs = null;
            return HandlePreflight(input);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="inputs"></param>
        /// <param name="callback"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        public IAsyncResult InvokeBegin(object instance, object[] inputs, AsyncCallback callback, object state)
        {
            throw new NotSupportedException("Only synchronous invocation");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="outputs"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public object InvokeEnd(object instance, out object[] outputs, IAsyncResult result)
        {
            throw new NotSupportedException("Only synchronous invocation");
        }
        #endregion // IOperationInvoker Implementation

        #region Utility Methods
        /// <summary>
        /// Takes the incoming message HttpRequestMessageProperty property, gets any CORS-specific headers, then creates an empty reply message,
        /// and adds to it a HttpResponseMessageProperty property with the appropriate headers.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private Message HandlePreflight(Message input)
        {
            HttpRequestMessageProperty httpRequest = (HttpRequestMessageProperty)input.Properties[HttpRequestMessageProperty.Name];
            string origin = httpRequest.Headers[CorsConstants.Origin];
            string requestMethod = httpRequest.Headers[CorsConstants.AccessControlRequestMethod];
            string requestHeaders = httpRequest.Headers[CorsConstants.AccessControlRequestHeaders];

            Message reply = Message.CreateMessage(MessageVersion.None, _replyAction);
            HttpResponseMessageProperty httpResponse = new HttpResponseMessageProperty();
            reply.Properties.Add(HttpResponseMessageProperty.Name, httpResponse);

            httpResponse.SuppressEntityBody = true;
            httpResponse.StatusCode = HttpStatusCode.OK;
            if (origin != null)
            {
                httpResponse.Headers.Add(CorsConstants.AccessControlAllowOrigin, origin);
            }

            if (requestMethod != null && this._allowedHttpMethods.Contains(requestMethod))
            {
                httpResponse.Headers.Add(CorsConstants.AccessControlAllowMethods, string.Join(",", this._allowedHttpMethods));
            }

            if (requestHeaders != null)
            {
                httpResponse.Headers.Add(CorsConstants.AccessControlAllowHeaders, requestHeaders);
            }

            return reply;
        }
        #endregion // Utility Methods
    } // PreflightOperationInvoker
}
