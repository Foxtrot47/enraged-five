﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.Text;
using RSG.Base.Configuration.Services;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Services.Common.Behaviors;

namespace RSG.Services.Common.Clients
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class ConfigAwareClient<TChannel> : ClientBase<TChannel> where TChannel : class
    {
        #region Fields
        /// <summary>
        /// Client level log object.
        /// </summary>
        private ILog _log;

        /// <summary>
        /// Service host configuration object.
        /// </summary>
        private readonly IServiceHostConfig _config;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ConfigAwareClient(IServiceHostConfig config)
        {
            _log = LogFactory.CreateUniversalLog(GetType().FullName);
            _config = config;

            // Update the endpoint address based on the config
            String modifiedAddress = this.Endpoint.Address.Uri.ToString();
            modifiedAddress = modifiedAddress.Replace("127.0.0.1", config.Address);

            // Check if we need to tweak the port to connect on.
            if (this.Endpoint.Binding is WebHttpBinding)
            {
                modifiedAddress = modifiedAddress.Replace("8080", config.HttpPort.ToString());
            }
            else if (this.Endpoint.Binding is NetTcpBinding)
            {
                modifiedAddress = modifiedAddress.Replace("9000", config.TcpPort.ToString());
            }

            // Set the modified address.
            this.Endpoint.Address = new EndpointAddress(modifiedAddress);

            // Add the logging message inspector.
            this.Endpoint.Behaviors.Add(new LoggingEndpointBehavior(_log));

            // Make sure that the data contract behaviour can cope with large numbers of items in the object graph.
            foreach (OperationDescription op in this.Endpoint.Contract.Operations)
            {
                DataContractSerializerOperationBehavior dataContractBehaviour =
                    op.Behaviors.Find<DataContractSerializerOperationBehavior>();
                if (dataContractBehaviour != null)
                {
                    dataContractBehaviour.MaxItemsInObjectGraph = int.MaxValue;
                }
            }
        }

        /// <summary>
        /// MET: Not sure whether we should support this constructor.
        /// </summary>
        /// <param name="binding"></param>
        /// <param name="remoteAddress"></param>
        public ConfigAwareClient(Binding binding, EndpointAddress remoteAddress)
            : base(binding, remoteAddress)
        {
            _log = LogFactory.CreateUniversalLog(GetType().FullName);

            // Add the logging message inspector.
            this.Endpoint.Behaviors.Add(new LoggingEndpointBehavior(_log));

            // Make sure that the data contract behaviour can cope with large numbers of items in the object graph.
            foreach (OperationDescription op in this.Endpoint.Contract.Operations)
            {
                DataContractSerializerOperationBehavior dataContractBehaviour =
                    op.Behaviors.Find<DataContractSerializerOperationBehavior>();
                if (dataContractBehaviour != null)
                {
                    dataContractBehaviour.MaxItemsInObjectGraph = int.MaxValue;
                }
            }
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Service host configuration object.
        /// </summary>
        public IServiceHostConfig Config
        {
            get { return _config; }
        }
        #endregion // Properties

        #region Protected Methods
        /// <summary>
        /// Executes a command through a transaction manager
        /// </summary>
        protected void ExecuteCommand(Action command)
        {
            try
            {
                command.Invoke();
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Executes a command through a transaction manager
        /// </summary>
        protected T ExecuteCommand<T>(Func<T> command)
        {
            try
            {
                return command.Invoke();
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion // Protected Methods
    } // ConfigAwareClient<T>
}
