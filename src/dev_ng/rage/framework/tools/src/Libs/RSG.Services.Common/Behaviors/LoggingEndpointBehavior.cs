﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using RSG.Base.Logging;
using RSG.Services.Common.Inspectors;

namespace RSG.Services.Common.Behaviors
{
    /// <summary>
    /// 
    /// </summary>
    public class LoggingEndpointBehavior : IEndpointBehavior
    {
        #region Fields
        /// <summary>
        /// Reference to the log object.
        /// </summary>
        private readonly ILog _log;
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="log"></param>
        public LoggingEndpointBehavior(ILog log)
        {
            _log = log;
        }
        #endregion // Constructor(s)

        #region IEndpointBehavior
        /// <summary>
        /// 
        /// </summary>
        /// <param name="endpoint"></param>
        /// <param name="bindingParameters"></param>
        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="endpoint"></param>
        /// <param name="clientRuntime"></param>
        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            LoggingMessageInspector inspector = new LoggingMessageInspector(_log);
            clientRuntime.MessageInspectors.Add(inspector);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="endpoint"></param>
        /// <param name="endpointDispatcher"></param>
        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            LoggingMessageInspector inspector = new LoggingMessageInspector(_log);
            endpointDispatcher.DispatchRuntime.MessageInspectors.Add(inspector);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="endpoint"></param>
        public void Validate(ServiceEndpoint endpoint)
        {
        }
        #endregion // IEndpointBehavior
    } // LoggingEndpointBehavior
}
