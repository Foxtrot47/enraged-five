﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web;
using RSG.Base.Configuration.Services;
using RSG.Base.Logging;
using RSG.Services.Common.ServiceModelExtensions;

namespace RSG.Services.Common
{
    /// <summary>
    /// Custom service host factory that injects the server config object into the services that are created by IIS.
    /// </summary>
    public abstract class ConfigServiceHostFactory : ServiceHostFactory
    {
        /// <summary>
        /// Service host configuration object to pass into the service.
        /// </summary>
        protected abstract IServiceHostConfig ServiceHostConfig { get; }

        /// <summary>
        /// Override the creation of a service host to create our local server host that injects the server config.
        /// </summary>
        protected override ServiceHost CreateServiceHost(Type t, Uri[] baseAddresses)
        {
            ServiceBehaviorAttribute att =
                t.GetCustomAttributes(typeof(ServiceBehaviorAttribute), true).FirstOrDefault() as ServiceBehaviorAttribute;
            Debug.Assert(att != null, "Trying to instantiate a service instance that doesn't have a ServiceBehaviorAttribute associated with it.");
            if (att == null)
            {
                throw new ArgumentNullException("Trying to instantiate a service instance that doesn't have a ServiceBehaviorAttribute associated with it.");
            }

            // Construct the config service host based on the instance context mode of the service.
            if (att.InstanceContextMode == InstanceContextMode.Single)
            {
                object serviceInstance = Activator.CreateInstance(t, ServiceHostConfig);
                return new ConfigServiceHost(serviceInstance, baseAddresses);
            }
            else
            {
                return new ConfigServiceHost(ServiceHostConfig, t, baseAddresses);
            }
        }
    } // ConfigServiceHostFactory
}