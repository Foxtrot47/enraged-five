﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetCombineMap.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Map.AssetCombine
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;

    /// <summary>
    /// Class that reads and stores Asset Combine executable document output.
    /// </summary>
    public class AssetCombineMap
    {
        #region Constants
        private const String _xmlElemName = "Name";
        private const String _xmlElemCacheDirectory = "CacheDirectory";
        private const String _xmlElemDrawableDictionaries = "DrawableDictionaries";
        private const String _xmlElemDrawableDictionary = "Item";
        private const String _xmlAttrDrawableDictionary = "DrawableDictionary";
        private const String _xmlElemTextureDictionaries = "TextureDictionaries";
        private const String _xmlElemTextureDictionary = "Item";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Name of the Asset Combined map.
        /// </summary>
        public String Name { get; private set; }

        /// <summary>
        /// Absolute path for cache directory.
        /// </summary>
        public String CacheDirectory { get; private set; }

        /// <summary>
        /// Drawable dictionaries to build.
        /// </summary>
        public ICollection<DrawableDictionary> DrawableDictionaries { get { return _drawableDictionaries; } }
        
        /// <summary>
        /// Texture dictionaries to build.
        /// </summary>
        public ICollection<TextureDictionary> TextureDictionaries { get { return _textureDictionaries; } }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Drawable dictionary collection.
        /// </summary>
        private ICollection<DrawableDictionary> _drawableDictionaries;

        /// <summary>
        /// Texture dictionary collection.
        /// </summary>
        private ICollection<TextureDictionary> _textureDictionaries;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="cacheDir"></param>
        /// <param name="drawableDictionaries"></param>
        /// <param name="textureDictionaries"></param>
        public AssetCombineMap(String name, String cacheDir, IEnumerable<DrawableDictionary> drawableDictionaries,
            IEnumerable<TextureDictionary> textureDictionaries)
        {
            this.Name = name;
            this.CacheDirectory = cacheDir;
            this._drawableDictionaries = drawableDictionaries.ToList();
            this._textureDictionaries = textureDictionaries.ToList();
        }

        /// <summary>
        /// Constructor (from XElement).
        /// </summary>
        /// <param name="element"></param>
        public AssetCombineMap(XElement element)
        {
            this.Name = element.Element(_xmlElemName).Value;
            this.CacheDirectory = element.Element(_xmlElemCacheDirectory).Value;
            this._drawableDictionaries = element.Element(_xmlElemDrawableDictionaries).Elements(_xmlElemDrawableDictionary).
                Select(drawableDictionaryElement => new DrawableDictionary(drawableDictionaryElement)).ToList();
            this._textureDictionaries = element.Element(_xmlElemTextureDictionaries).Elements(_xmlElemTextureDictionary).
                Select(textureDictionaryElement => new TextureDictionary(textureDictionaryElement)).ToList();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="drawableDict"></param>
        public void AddDrawableDictionary(DrawableDictionary drawableDict)
        {
            this._drawableDictionaries.Add(drawableDict);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="txd"></param>
        public void AddTextureDictionary(TextureDictionary txd)
        {
            this._textureDictionaries.Add(txd);
        }

        /// <summary>
        /// Serialise this AssetCombineMap to an XElement.
        /// </summary>
        /// <param name="elementName"></param>
        /// <param name="isProjectDLC"></param>
        /// <returns></returns>
        public XElement ToXElement(String elementName, bool isProjectDLC)
        {
            XElement xmlMapElem = new XElement(elementName,
                new XElement(_xmlElemName, this.Name),
                new XElement(_xmlElemCacheDirectory, this.CacheDirectory),
                new XElement(_xmlElemDrawableDictionaries,
                    this.DrawableDictionaries.Select(dd => dd.ToXElement(_xmlElemDrawableDictionary))
                ),
                new XElement(_xmlElemTextureDictionaries,
                    this.TextureDictionaries.Select(td => td.ToXElement(_xmlElemTextureDictionary))
                )
            ); // Root

            return (xmlMapElem);
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Services.Map.AssetCombine namespace

