﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetCombineLODHierarchy2.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2010-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Map.AssetCombine.Algorithms
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using RSG.Base.Configuration;
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;
    using RSG.SceneXml;
    using RSG.SceneXml.MapExport;
    using MEXAB = RSG.SceneXml.MapExport.AssetCombine;
    using RSG.SceneXml.Material;

    /// <summary>
    /// LOD hierarchy analysis and asset combine algorithm.
    /// </summary>
    internal class AssetCombineLODHierarchy2 : IAssetCombineAlgorithm2
    {
        #region IAssetCombineAlgorithm2 Methods
        /// <summary>
        /// Run the combine algorithm.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="branch"></param>
        /// <param name="mapName"></param>
        /// <param name="inputs"></param>
        /// <param name="document"></param>
        /// <returns></returns>
        public bool ProcessArea(IUniversalLog log, IBranch branch, String mapName,
            IEnumerable<AssetCombineInput> inputs, out AssetCombineDocument document)
        {
            using (new ProfileContext(log, String.Format("Processing LOD Hierarchy for {0}.", mapName)))
            {
                // To correctly build data for SLOD2 objects we need to build a map of SLOD2 objects and their children.
                Dictionary<TargetObjectDef, List<Tuple<TargetObjectDef, AssetCombineInput>>> slod2ChildrenMap = BuildSLOD2ChildrenMap(log, inputs);

                List<DrawableDictionary> drawableDictionaries = new List<DrawableDictionary>();
                List<TextureDictionary> textureDictionaries = new List<TextureDictionary>();

                foreach (AssetCombineInput input in inputs)
                {
                    foreach (TargetObjectDef obj in input.Scene.Walk(Scene.WalkMode.DepthFirst))
                    {
                        if (!IsDrawable(obj))
                            continue; // Skip; non-drawable.
                        if (obj.DontExport() || obj.DontExportIDE())
                            continue; // Skip; not-exported.

                        // With SceneLinks we can handle each LOD level the same!
                        switch (obj.LODLevel)
                        {
                            case ObjectDef.LodLevel.SLOD2:
                                {
                                    // Use cached inter-container data to build outputs here
                                    Tuple<DrawableDictionary, TextureDictionary> result =
                                        CreateOutputForInterContainerSiblings(log, branch, obj, slod2ChildrenMap[obj]);
                                    if (null != result.Item1)
                                        drawableDictionaries.Add(result.Item1);
                                    if (null != result.Item2)
                                        textureDictionaries.Add(result.Item2);
                                }
                                break;
                            case ObjectDef.LodLevel.SLOD4:
                            case ObjectDef.LodLevel.SLOD3:
                            case ObjectDef.LodLevel.SLOD1:
                                // Loop through our immediate children; they are
                                // siblings are drawables will be merged.
                                {
                                    Tuple<DrawableDictionary, TextureDictionary> result = CreateOutputForSiblings(log, branch, input, obj);
                                    if (null != result.Item1)
                                        drawableDictionaries.Add(result.Item1);
                                    if (null != result.Item2)
                                        textureDictionaries.Add(result.Item2);
                                }
                                break;
                            default:
                                break; // Ignore objects with no children to merge.
                        }
                    }
                }
                
                AssetCombineMap map = new AssetCombineMap(mapName, String.Empty, drawableDictionaries, textureDictionaries);
                document = new AssetCombineDocument(map, branch.Project.IsDLC);
            }

            return (true);
        }
        #endregion // IAssetCombineAlgorithm2 Methods

        #region Private Methods
        /// <summary>
        /// Constructs a map matching all SLOD2 objects with their SLOD1 children from an enumeration of content node/scene pairs
        /// </summary>
        /// <param name="log"></param>
        /// <param name="inputs"></param>
        private Dictionary<TargetObjectDef, List<Tuple<TargetObjectDef, AssetCombineInput>>> BuildSLOD2ChildrenMap(
            IUniversalLog log, IEnumerable<AssetCombineInput> inputs)
        {
            // Pass 1: collect all SLOD1 and SLOD2 objects
            List<Tuple<TargetObjectDef, AssetCombineInput>> slod1Objects = new List<Tuple<TargetObjectDef, AssetCombineInput>>();
            List<TargetObjectDef> slod2Objects = new List<TargetObjectDef>();
            foreach (AssetCombineInput input in inputs)
            {
                foreach (TargetObjectDef obj in input.Scene.Walk(Scene.WalkMode.DepthFirst))
                {
                    if (!IsDrawable(obj))
                        continue; // Skip; non-drawable.
                    if (obj.DontExport() || obj.DontExportIDE())
                        continue; // Skip; not-exported.

                    switch (obj.LODLevel)
                    {
                        case ObjectDef.LodLevel.SLOD1:
                            if (obj.LOD != null && obj.LOD.Parent != null && obj.LOD.Parent.IsContainerLODRefObject())
                                slod1Objects.Add(new Tuple<TargetObjectDef, AssetCombineInput>(obj, input));
                            break;
                        case ObjectDef.LodLevel.SLOD2:
                            slod2Objects.Add(obj);
                            break;
                        default:
                            break; // Ignore objects with no children to merge.
                    }
                }
            }

            // Pass 2: build the map
            Dictionary<TargetObjectDef, List<Tuple<TargetObjectDef, AssetCombineInput>>> slod2ChildrenMap =
                new Dictionary<TargetObjectDef, List<Tuple<TargetObjectDef, AssetCombineInput>>>();

            foreach (TargetObjectDef slod2Object in slod2Objects)
            {
                slod2ChildrenMap.Add(slod2Object, new List<Tuple<TargetObjectDef, AssetCombineInput>>());
            }

            foreach (Tuple<TargetObjectDef, AssetCombineInput> slod1Object in slod1Objects)
            {
                TargetObjectDef slod1Parent = slod2Objects.FirstOrDefault(slod2Object => String.Equals(slod2Object.Name, slod1Object.Item1.LOD.Parent.RefName, StringComparison.OrdinalIgnoreCase));
                if (slod1Parent != null)
                    slod2ChildrenMap[slod1Parent].Add(slod1Object);
                else
                    log.Warning("Unable to find SLOD2 parent for '{0}'", slod1Object.Item1.Name);
            }

            return (slod2ChildrenMap);
        }

        /// <summary>
        /// Create Drawable and Texture Dictionaries for immediate LOD children of an ObjectDef.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="branch"></param>
        /// <param name="input"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        private Tuple<DrawableDictionary, TextureDictionary> CreateOutputForSiblings(IUniversalLog log, 
            IBranch branch, AssetCombineInput input, TargetObjectDef parent)
        {
            // Validate input ObjectDef.
            Debug.Assert(parent.HasLODChildren(),
                String.Format("Internal error: parent object {0} doesn't have LOD children.", parent.Name));
            if (!parent.HasLODChildren())
            {
                log.WarningCtx(parent.Name, "Internal error: parent object {0} doesn't have LOD children.", parent.Name);
                return (null);
            }

            String zipPathname = input.ZipPathname;
            String contentName = Path.GetFileNameWithoutExtension(parent.MyScene.Filename);

            // avoid recreating a full combine of non DLC object by renaming them
            bool containsNewDlcChild = (branch.Project.ForceFlags.AllNewContent || parent.LOD.Children.Any(o => o.IsNewDLCObject()));
            IProject coreProject = branch.Project.Config.CoreProject;
            IProject project = branch.Project;
            String dictionaryName = MapAsset.AppendMapPrefixIfRequired(project, string.Format("{0}_children", parent.Name));
            
            List<DrawableDictionaryInput> drawableInputs = new List<DrawableDictionaryInput>();
            // Dictionary of required items per source TXD input.
            IDictionary<SourceTXD, ICollection<String>> txdRequiredItems = new Dictionary<SourceTXD, ICollection<String>>();

            foreach (TargetObjectDef child in parent.LOD.Children)
            {
                if (!IsDrawable(child))
                    continue; // Skip; non-drawable.
                if (child.DontExport() || child.DontExportIDE())
                    continue; // Skip; not-exported.

                // When we're DLC ensure we get the right source zip.
                bool isNewDlcObject = (project.ForceFlags.AllNewContent || child.IsNewDLCObject());
                String cacheDir;
                if (input.IsDLC && isNewDlcObject)
                {
                    zipPathname = input.ZipPathname;
                    cacheDir = Path.Combine(project.Cache, branch.Name, "maps", contentName);
                }
                else if (input.IsDLC && !isNewDlcObject)
                {
                    zipPathname = input.CoreInput.ZipPathname;
                    cacheDir = Path.Combine(coreProject.Cache, branch.Name, "maps", contentName);
                }
                else
                {
                    cacheDir = Path.Combine(coreProject.Cache, branch.Name, "maps", contentName);
                }

                DrawableDictionaryInput drawable = new DrawableDictionaryInput(child.GetObjectName(), cacheDir, zipPathname, isNewDlcObject);
                drawableInputs.Add(drawable);

                // Collect the required items for this object's source TXD.
                AddTextureDictionaryInputs(branch, child, txdRequiredItems, contentName, input);
            }

            // Loop through our required items dictionary creating the final TextureDictionaryInputs.
            List<TextureDictionaryInput> txdInputs = new List<TextureDictionaryInput>();
            foreach (KeyValuePair<SourceTXD, ICollection<String>> kvp in txdRequiredItems)
            {
                SourceTXD sourceTxd = kvp.Key;
                TextureDictionaryInput txdInput = new TextureDictionaryInput(sourceTxd.Name, sourceTxd.CacheDirectory,
                    sourceTxd.ZipFilename, sourceTxd.IsNewDLC, kvp.Value);
                txdInputs.Add(txdInput);
            }

            // Create dictionary objects to return; if we only have one TXD input then
            // we don't rename it.  Just keep the exported TXD.
            String txdName = dictionaryName;
            bool createMergedTXD = true;
            if (1 == txdInputs.Count)
            {
                txdName = txdInputs[0].Name;
                createMergedTXD = false;
            }

            ObjectDef.LodLevel childLodLevel = GetChildLodLevel(parent);
            TextureDictionary textureDictionary = null;
            if (createMergedTXD)
                textureDictionary = new TextureDictionary(txdName, containsNewDlcChild, txdInputs);
            DrawableDictionary drawableDictionary = new DrawableDictionary(dictionaryName, containsNewDlcChild, txdName, childLodLevel.ToString(), drawableInputs);
            Tuple<DrawableDictionary, TextureDictionary> result = new Tuple<DrawableDictionary, TextureDictionary>(
                drawableDictionary, textureDictionary);
            return (result);
        }

        /// <summary>
        /// Create AssetOutputs for child siblings of an ObjectDef that are supplied and assumed to be SLOD1
        /// </summary>
        /// <param name="log"></param>
        /// <param name="branch"></param>
        /// <param name="map"></param>
        /// <param name="parent"></param>
        /// <param name="children"></param>
        /// <returns></returns>
        private Tuple<DrawableDictionary, TextureDictionary> CreateOutputForInterContainerSiblings(IUniversalLog log,
            IBranch branch, TargetObjectDef parent, IEnumerable<Tuple<TargetObjectDef, AssetCombineInput>> children)
        {
            // avoid recreating a full combine of non DLC object by renaming them
            bool containsNewDlcChild = (branch.Project.ForceFlags.AllNewContent || children.Any(pair => pair.Item1.IsNewDLCObject()));
            IProject coreProject = branch.Project.Config.CoreProject;
            IProject project = branch.Project;

            string dictionaryName = string.Format("{0}_children", parent.Name);
            String drawableDictionaryName = MapAsset.AppendMapPrefixIfRequired(project, dictionaryName);
            String textureDictionaryName = MapAsset.AppendMapPrefixIfRequired(project, dictionaryName);

            List<DrawableDictionaryInput> drawableInputs = new List<DrawableDictionaryInput>();
            // Dictionary of required items per source TXD input.
            IDictionary<SourceTXD, ICollection<String>> txdRequiredItems = new Dictionary<SourceTXD, ICollection<String>>();

            foreach (Tuple<TargetObjectDef, AssetCombineInput> childItem in children)
            {
                TargetObjectDef child = childItem.Item1;
                AssetCombineInput input = childItem.Item2;

                if (!IsDrawable(child))
                    continue; // Skip; non-drawable.
                if (child.DontExport() || child.DontExportIDE())
                    continue; // Skip; not-exported.

                String zipPathname = input.ZipPathname;
                String contentName = Path.GetFileNameWithoutExtension(child.MyScene.Filename);
                String cacheDir = String.Empty;

                // When we're DLC ensure we get the right source zip.
                bool isNewDlcObject = (project.ForceFlags.AllNewContent || child.IsNewDLCObject());
                if (input.IsDLC && isNewDlcObject)
                {
                    zipPathname = input.ZipPathname;
                    cacheDir = Path.Combine(project.Cache, branch.Name, "maps", contentName);
                }
                else if (input.IsDLC && !isNewDlcObject)
                {
                    zipPathname = input.CoreInput.ZipPathname;
                    cacheDir = Path.Combine(coreProject.Cache, branch.Name, "maps", contentName);
                }
                else
                {
                    cacheDir = Path.Combine(coreProject.Cache, branch.Name, "maps", contentName);
                }

                DrawableDictionaryInput drawable = new DrawableDictionaryInput(child.GetObjectName(), cacheDir, zipPathname, isNewDlcObject);
                drawableInputs.Add(drawable);

                // Collect the required items for this object's source TXD.
                AddTextureDictionaryInputs(branch, child, txdRequiredItems, contentName, input);
            }

            // Loop through our required items dictionary creating the final TextureDictionaryInputs.
            List<TextureDictionaryInput> txdInputs = new List<TextureDictionaryInput>();
            foreach (KeyValuePair<SourceTXD, ICollection<String>> kvp in txdRequiredItems)
            {
                SourceTXD sourceTxd = kvp.Key;
                TextureDictionaryInput txdInput = new TextureDictionaryInput(sourceTxd.Name, sourceTxd.CacheDirectory, 
                    sourceTxd.ZipFilename, sourceTxd.IsNewDLC, kvp.Value);
                txdInputs.Add(txdInput);
            }

            // Create dictionary objects to return; if we only have one TXD input then
            // we don't rename it.  Just keep the exported TXD.
            String txdName = textureDictionaryName;
            bool createMergedTXD = true;
            if (1 == txdInputs.Count)
            {
                txdName = txdInputs[0].Name;
                createMergedTXD = false;
            }

            ObjectDef.LodLevel childLodLevel = GetChildLodLevel(parent);
            TextureDictionary textureDictionary = null;
            if (createMergedTXD)
                textureDictionary = new TextureDictionary(txdName, containsNewDlcChild, txdInputs);
            DrawableDictionary drawableDictionary = new DrawableDictionary(drawableDictionaryName, containsNewDlcChild, txdName, childLodLevel.ToString(), drawableInputs);
            Tuple<DrawableDictionary, TextureDictionary> result = new Tuple<DrawableDictionary, TextureDictionary>(
                drawableDictionary, textureDictionary);
            return (result);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="objectDef"></param>
        /// <param name="textureDictionary"></param>
        /// <param name="contentName"></param>
        /// <param name="input"></param>
        private static void AddTextureDictionaryInputs(IBranch branch, TargetObjectDef objectDef,
            IDictionary<SourceTXD, ICollection<String>> txdRequiredItems, String contentName, AssetCombineInput input)
        {
            // This is the source (or input) TXD.
            String txdName = objectDef.GetAttribute(AttrNames.OBJ_REAL_TXD, AttrDefaults.OBJ_REAL_TXD).ToLower();
            if (txdName.Equals(AttrDefaults.OBJ_REAL_TXD, StringComparison.OrdinalIgnoreCase))
                return; // Skip; no TXD defined.

            IProject project = branch.Project;
            IProject coreProject = branch.Project.Config.CoreProject;

            String zipPathname = input.ZipPathname;
            String cacheDir;

            // When we're DLC ensure we get the right source zip.
            bool isNewDlcObject = (branch.Project.ForceFlags.AllNewContent || objectDef.IsNewDLCObject());
            if (input.IsDLC && isNewDlcObject)
            {
                zipPathname = input.ZipPathname;
                cacheDir = Path.Combine(project.Cache, "maps", branch.Name, contentName);
            }
            else if (input.IsDLC && !isNewDlcObject)
            {
                zipPathname = input.CoreInput.ZipPathname;
                cacheDir = Path.Combine(coreProject.Cache, "maps", branch.Name, contentName);
            }
            else
            {
                cacheDir = Path.Combine(coreProject.Cache, "maps", branch.Name, contentName);
            }

            SourceTXD sourceTxd = txdRequiredItems.Keys.FirstOrDefault(txd => txd.Name.Equals(txdName) && txd.ZipFilename.Equals((zipPathname)));
            if (null == sourceTxd)
            {
                sourceTxd = new SourceTXD(txdName, zipPathname, cacheDir, isNewDlcObject);
                txdRequiredItems.Add(sourceTxd, new List<String>());
            }

            if (objectDef.Material != Guid.Empty)
            {
                MaterialDef childMaterialDef = objectDef.MyScene.MaterialLookup[objectDef.Material];
                IEnumerable<String> textureBasenames = childMaterialDef.GetTextureBasenamesRecursive();
                foreach (String textureBasename in textureBasenames)
                {
                    txdRequiredItems[sourceTxd].Add(textureBasename);
                }
            }
        }


        /// <summary>
        /// Return child LOD level from a parent object.
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        private ObjectDef.LodLevel GetChildLodLevel(TargetObjectDef parent)
        {
            switch (parent.LODLevel)
            {
                case ObjectDef.LodLevel.SLOD4:
                    return (ObjectDef.LodLevel.SLOD3);
                case ObjectDef.LodLevel.SLOD3:
                    return (ObjectDef.LodLevel.SLOD2);
                case ObjectDef.LodLevel.SLOD2:
                    return (ObjectDef.LodLevel.SLOD1);
                case ObjectDef.LodLevel.SLOD1:
                    return (ObjectDef.LodLevel.LOD);
                default:
                    throw (new NotSupportedException());
            }
        }

        /// <summary>
        /// Determine whether an ObjectDef is a usable Drawable.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private bool IsDrawable(TargetObjectDef obj)
        {
            if (!obj.IsObject())
                return (false); // Ignore non-objects.
            if (obj.IsRefObject() || obj.IsXRef())
                return (false); // Ignore external reference objects.
            if (obj.IsRefInternalObject() || obj.IsInternalRef())
                return (false); // Ignore internal reference objects.
            if (obj.IsFragmentProxy())
                return (false);
            if (obj.IsAnimProxy())
                return (false);

            return (true);
        }
        #endregion // Private Methods
    }

    /// <summary>
    /// Source TXD structure used internally.
    /// </summary>
    class SourceTXD
    {
        public String Name;
        public String ZipFilename;
        public String CacheDirectory;
        public bool IsNewDLC;

        public SourceTXD(String txdName, String zipFilename, String cacheDir, bool isNewDLC)
        {
            this.Name = txdName;
            this.ZipFilename = zipFilename;
            this.CacheDirectory = cacheDir;
            this.IsNewDLC = isNewDLC;
        }
    }

} // RSG.Pipeline.Services.Map.AssetCombine.Algorithms namespace
