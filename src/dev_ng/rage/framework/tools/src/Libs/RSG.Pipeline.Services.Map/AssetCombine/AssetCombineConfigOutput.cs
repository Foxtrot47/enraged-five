﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetCombineConfigOutput.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Map.AssetCombine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;

    /// <summary>
    /// Asset Combine Output; representing a output directory (for generated Zip files) and
    /// Asset Combine document path.  This is part of the Asset Combine config file.
    /// </summary>
    public class AssetCombineConfigOutput
    {
        #region Properties
        /// <summary>
        /// Cache directory (used for Asset Combine merged data output).
        /// </summary>
        public String CacheDirectory { get; private set; }

        /// <summary>
        /// Asset Combine document filename.
        /// </summary>
        public String Filename { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="cacheDirectory"></param>
        /// <param name="filename"></param>
        public AssetCombineConfigOutput(String cacheDirectory, String filename)
        {
            CacheDirectory = String.Intern(cacheDirectory);
            Filename = String.Intern(filename);
        }

        /// <summary>
        /// Constructor; from XML.
        /// </summary>
        /// <param name="element"></param>
        public AssetCombineConfigOutput(XElement element)
        {
            CacheDirectory = String.Intern(element.Element(_xmlElemCacheDirectory).Value);
            Filename = String.Intern(element.Element(_xmlElemFilename).Value);
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise to XML.
        /// </summary>
        /// <returns></returns>
        public XElement ToXElement()
        {
            return new XElement("Output",
                new XElement(_xmlElemCacheDirectory, CacheDirectory),
                new XElement(_xmlElemFilename, Filename));
        }
        #endregion // Controller Methods

        #region Private Data
        private const String _xmlElemCacheDirectory = "CacheDirectory";
        private const String _xmlElemFilename = "Filename";
        #endregion // Private Data
    }
} // RSG.Pipeline.Services.Map.AssetCombine namespace
