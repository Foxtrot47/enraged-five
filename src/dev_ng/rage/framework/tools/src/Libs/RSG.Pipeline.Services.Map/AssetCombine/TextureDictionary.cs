﻿//---------------------------------------------------------------------------------------------
// <copyright file="TextureDictionary.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Map.AssetCombine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using RSG.Pipeline.Services.Map.AssetCombine;

    /// <summary>
    /// Class representing a merged Texture Dictionary.
    /// </summary>
    public class TextureDictionary : DictionaryAsset
    {
        #region Constants
        private const String _xmlElemInputs = "Inputs";
        private const String _xmlElemInput = "Item";
        private const String _xmlAttrTextureDictionary = "TextureDictionary";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Texture dictionary inputs (texture dictionaries to merge).
        /// </summary>
        public IEnumerable<TextureDictionaryInput> Inputs { get { return _inputs; } }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Inputs collection.
        /// </summary>
        private ICollection<TextureDictionaryInput> _inputs;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="isNewDLC"></param>
        /// <param name="inputs"></param>
        public TextureDictionary(String name, bool isNewDLC, IEnumerable<TextureDictionaryInput> inputs)
            : base(name, isNewDLC)
        {
            this._inputs = new List<TextureDictionaryInput>(inputs);
        }

        /// <summary>
        /// Constructor (from XElement)
        /// </summary>
        /// <param name="xmlElem"></param>
        public TextureDictionary(XElement xmlElem)
            : base(xmlElem)
        {
            this._inputs = xmlElem.Element(_xmlElemInputs).Elements(_xmlElemInput).
                Select(input => new TextureDictionaryInput(input)).ToList();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Determine whether we already have this input defined.
        /// </summary>
        /// <param name="txdName"></param>
        /// <returns></returns>
        public bool HasInput(String txdName)
        {
            return (_inputs.Any(input => String.Equals(input.Name, txdName, StringComparison.OrdinalIgnoreCase)));
        }

        /// <summary>
        /// Add a single input object.
        /// </summary>
        /// <param name="input"></param>
        public void AddInput(TextureDictionaryInput input)
        {
            this._inputs.Add(input);
        }

        /// <summary>
        /// Get a named input.
        /// </summary>
        /// <param name="txdName"></param>
        /// <returns></returns>
        public TextureDictionaryInput GetInputByName(String txdName)
        {
            return (this._inputs.FirstOrDefault(input => String.Equals(input.Name, txdName, StringComparison.OrdinalIgnoreCase)));
        }

        /// <summary>
        /// Serialise to XElement.
        /// </summary>
        /// <param name="xmlElemName"></param>
        /// <returns></returns>
        public override XElement ToXElement(String xmlElemName)
        {
            XElement xmlTextureDictionaryElem = base.ToXElement(xmlElemName);
            xmlTextureDictionaryElem.Add(
                new XAttribute("type", _xmlAttrTextureDictionary),
                new XElement(_xmlElemInputs, this.Inputs.Select(i => i.ToXElement(_xmlElemInput))
            )); // xmlTextureDictionaryElem

            return (xmlTextureDictionaryElem);
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Services.Map.AssetCombine namespace
