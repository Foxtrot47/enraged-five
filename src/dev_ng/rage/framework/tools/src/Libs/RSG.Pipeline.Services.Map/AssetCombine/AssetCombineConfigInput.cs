﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetCombineConfigInput.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Map.AssetCombine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;

    /// <summary>
    /// Asset Combine Input; representing a 3dsmax export data set as defined in 
    /// the Asset Combine config file.
    /// </summary>
    public class AssetCombineConfigInput
    {
        #region Properties
        public String SceneXmlPathname { get; private set; }
        public String ZipPathname { get; private set; }
        public String SceneType { get; private set; }
        public bool IsDLC { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor; from user-data.
        /// </summary>
        /// <param name="sceneXmlPathname"></param>
        /// <param name="zipPathname"></param>
        /// <param name="sceneType"></param>
        public AssetCombineConfigInput(String sceneXmlPathname, String zipPathname, String sceneType, bool isDLC)
        {
            this.SceneXmlPathname = String.Intern(sceneXmlPathname);
            this.ZipPathname = String.Intern(zipPathname);
            this.SceneType = String.Intern(sceneType);
            this.IsDLC = isDLC;
        }

        /// <summary>
        /// Constructor; from XElement.
        /// </summary>
        /// <param name="xmlElem"></param>
        public AssetCombineConfigInput(XElement xmlElem)
        {
            SceneXmlPathname = String.Intern(xmlElem.Element(_xmlElemSceneXmlFilename).Value);
            ZipPathname = String.Intern(xmlElem.Element(_xmlElemZipFilename).Value);
            SceneType = String.Intern(xmlElem.Element(_xmlElemSceneType).Value);

            bool isDLC = false;
            if (bool.TryParse(xmlElem.Element(_xmlElemIsDLC).Attribute("value").Value, out isDLC))
                IsDLC = isDLC;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise to an XElement.
        /// </summary>
        /// <returns></returns>
        public XElement ToXElement(String elementName)
        {
            return new XElement(elementName,
                new XAttribute("type", _xmlAttrType),
                new XElement(_xmlElemSceneXmlFilename, SceneXmlPathname),
                new XElement(_xmlElemZipFilename, ZipPathname),
                new XElement(_xmlElemSceneType, SceneType),
                new XElement(_xmlElemIsDLC, 
                    new XAttribute("value", IsDLC)));
        }
        #endregion // Controller Methods

        #region Private Data
        private const String _xmlAttrType = "AssetCombineConfigInput";
        private const String _xmlElemSceneXmlFilename = "SceneXmlFilename";
        private const String _xmlElemZipFilename = "ZipFilename";
        private const String _xmlElemSceneType = "SceneType";
        private const String _xmlElemIsDLC = "IsDLC";
        #endregion // Private Data
    }

} // RSG.Pipeline.Services.Map.AssetCombine namespace
