﻿using System;

namespace RSG.Pipeline.Services.Map.AssetCombine.Algorithms
{

    /// <summary>
    /// Attribute used to define the fallback algorithm.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    internal class FallbackAlgorithm : Attribute
    {
    }

    /// <summary>
    /// Attribute used to disable algorithms from being used.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    internal class DisableAlgorithm : Attribute
    {
    }

} // RSG.Pipeline.Services.Map.AssetCombine.Algorithms namespace
