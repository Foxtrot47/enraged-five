﻿//---------------------------------------------------------------------------------------------
// <copyright file="TextureDictionary.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Map.AssetCombine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;
    
    /// <summary>
    /// Class representing a Texture Dictionary input (being merged).
    /// </summary>
    public class TextureDictionaryInput : InputAsset
    {
        #region Constants
        private const String _xmlElemRequiredItems = "RequiredItems";
        private const String _xmlElemRequiredItem = "Item";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Required texture filenames.
        /// </summary>
        public IEnumerable<String> RequiredItems { get { return (_requiredItems); } }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private ICollection<String> _requiredItems;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="cacheDir"></param>
        /// <param name="sourceZipFilename"></param>
        /// <param name="isNewDLC"></param>
        /// <param name="requiredItems"></param>
        public TextureDictionaryInput(String name, String cacheDir, String sourceZipFilename, bool isNewDLC, IEnumerable<String> requiredItems)
            : base(name, cacheDir, sourceZipFilename, isNewDLC)
        {
            this._requiredItems = new List<String>(requiredItems);
        }

        /// <summary>
        /// Constructor (from XElement).
        /// </summary>
        /// <param name="xmlElem"></param>
        public TextureDictionaryInput(XElement xmlElem)
            : base(xmlElem)
        {
            XElement requiredItemsElement = xmlElem.Element(_xmlElemRequiredItems);
            if (requiredItemsElement != null)
                this._requiredItems = requiredItemsElement.Elements(_xmlElemRequiredItem).Select(itemElement => itemElement.Value).ToList();
            else
                this._requiredItems = new List<String>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Add required texture item.
        /// </summary>
        /// <param name="textureName"></param>
        public void AddRequiredItem(String textureFilename)
        {
            this._requiredItems.Add(String.Intern(textureFilename));
        }

        /// <summary>
        /// Serialise to XElement.
        /// </summary>
        /// <param name="xmlElemName"></param>
        /// <returns></returns>
        public override XElement ToXElement(String xmlElemName)
        {
            XElement xmlTextureDictionaryInputElem = base.ToXElement(xmlElemName);

            xmlTextureDictionaryInputElem.Add(new XAttribute("type", "TextureDictionaryInput"));
            xmlTextureDictionaryInputElem.Add(
                new XElement(_xmlElemRequiredItems, 
                    this.RequiredItems.Select(i => new XElement(_xmlElemRequiredItem, i)
                ))
            );

            return (xmlTextureDictionaryInputElem);
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Services.Map.AssetCombine namespace
