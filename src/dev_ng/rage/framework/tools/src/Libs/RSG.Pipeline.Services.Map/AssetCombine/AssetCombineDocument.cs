﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetCombineDocument.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Map.AssetCombine
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    /// <summary>
    /// Class that reads the Asset Combine output XML document.
    /// </summary>
    public class AssetCombineDocument
    {
        #region Constants
        private const String _xmlElemDocument = "AssetCombineDocument";
        private const String _xmlElemIsDLC = "IsDLC";
        private const String _xmlElemMap = "Map";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// 
        /// </summary>
        public AssetCombineMap Map { get; private set; }

        /// <summary>
        /// Whether this document is for a DLC project.
        /// </summary>
        public bool IsDLC { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param
        /// <param name="map"></param>
        /// <param name="isDLC"></param>
        public AssetCombineDocument(AssetCombineMap map, bool isDLC)
        {
            this.Map = map;
            this.IsDLC = isDLC;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="document"></param>
        private AssetCombineDocument(XDocument document)
        {
            IEnumerable<XElement> mapElements = document.Root.Elements(_xmlElemMap);
            Debug.Assert(mapElements.Count() == 1);

            this.Map = new AssetCombineMap(mapElements.First());
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Save document to XML file.
        /// </summary>
        /// <param name="filename"></param>
        public void Save(String filename)
        {
            using (FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.Write))
            {
                Save(fs);
            }
        }

        /// <summary>
        /// Save document to stream.
        /// </summary>
        /// <param name="stream"></param>
        public void Save(Stream stream)
        {
            XDocument xmlDoc = new XDocument(
                new XDeclaration("1.0", "utf-8", "yes"),
                new XElement(_xmlElemDocument,
                    new XElement(_xmlElemIsDLC, this.IsDLC),
                    this.Map.ToXElement(_xmlElemMap, this.IsDLC)
                ) // Document
            );
            xmlDoc.Save(stream);
        }

        /// <summary>
        /// Load Asset Combine Document from XML file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static AssetCombineDocument Load(String filename)
        {
            XDocument document = XDocument.Load(filename);
            return (new AssetCombineDocument(document));
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Services.Map.AssetCombine namespace
