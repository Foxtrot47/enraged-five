﻿//---------------------------------------------------------------------------------------------
// <copyright file="InputAsset.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Map.AssetCombine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;
    
    /// <summary>
    /// Abstract base class for all Asset Combine input assets.
    /// </summary>
    public abstract class InputAsset
    {
        #region Constants
        private const String _xmlElemName = "Name";
        private const String _xmlElemType = "Type";
        private const String _xmlElemCacheDirectory = "CacheDirectory";
        private const String _xmlElemSourceZipFilename = "SourceZipFilename";
        private const String _xmlElemIsNewDLC = "IsNewDLC";
        #endregion // Constants

        #region Properties
        public String Name { get; private set; }
        public String CacheDirectory { get; private set; }
        public String SourceZipFilename { get; private set; }

        /// <summary>
        /// Whether the drawable is flagged as "New DLC".
        /// </summary>
        public bool IsNewDLC { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="cacheDir"></param>
        /// <param name="sourceZipFilename"></param>
        /// <param name="isNewDLC"></param>
        public InputAsset(String name, String cacheDir, String sourceZipFilename, bool isNewDLC)
        {
            this.Name = name;
            this.CacheDirectory = cacheDir;
            this.SourceZipFilename = sourceZipFilename;
            this.IsNewDLC = isNewDLC;
        }

        /// <summary>
        /// Constructor (from XElement).
        /// </summary>
        /// <param name="element"></param>
        public InputAsset(XElement xmlElem)
        {
            this.Name = xmlElem.Element(_xmlElemName).Value;
            this.CacheDirectory = String.Intern(xmlElem.Element(_xmlElemCacheDirectory).Value);
            this.SourceZipFilename = String.Intern(xmlElem.Element(_xmlElemSourceZipFilename).Value);

            bool isNewDLC = false;
            if (null != xmlElem.Element(_xmlElemIsNewDLC))
            {
                if (bool.TryParse(xmlElem.Element(_xmlElemIsNewDLC).Value, out isNewDLC))
                    this.IsNewDLC = isNewDLC;
                else
                    this.IsNewDLC = false;
            }
        }
        #endregion // Constructor(s)
        
        #region Controller Methods
        /// <summary>
        /// Serialise to XElement.
        /// </summary>
        /// <param name="xmlElemName"></param>
        /// <returns></returns>
        public virtual XElement ToXElement(String xmlElemName)
        {
            XElement xmlDrawableDictionaryInputElem = new XElement(xmlElemName,
                new XElement(_xmlElemName, this.Name),
                new XElement(_xmlElemCacheDirectory, this.CacheDirectory),
                new XElement(_xmlElemSourceZipFilename, this.SourceZipFilename)
            ); // DrawableDictionaryInputElem

            return (xmlDrawableDictionaryInputElem);
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Services.Map.AssetCombine namespace
