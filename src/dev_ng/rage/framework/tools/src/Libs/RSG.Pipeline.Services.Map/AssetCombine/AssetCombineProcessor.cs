﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetCombineProcessor.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2010-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Map.AssetCombine
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using RSG.Base.Collections;
    using RSG.Base.Configuration;
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;
    using RSG.Pipeline.Services;
    using RSG.Pipeline.Services.Map.AssetCombine.Algorithms;
    using RSG.SceneXml;
    using RSG.SceneXml.MapExport.AssetCombine;
    
    /// <summary>
    /// The AssetCombineProcessor; the class that ties all of our components
    /// together and does the analysis required for combining drawables and
    /// texture dictionaries.
    /// </summary>
    public static class AssetCombineProcessor
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="log"></param>
        /// <param name="branch"></param>
        /// <param name="inputs"></param>
        /// <param name="cacheDir"></param>
        /// <param name="outputName"></param>
        /// <returns></returns>
        public static AssetCombineDocument ProcessArea(IUniversalLog log, IBranch branch, 
            IEnumerable<AssetCombineInput> inputs, String cacheDir, String outputName)
        {
            // Load algorithms using reflection.
            List<IAssetCombineAlgorithm2> algorithms = LoadAlgorithms<IAssetCombineAlgorithm2>(log, false);
            if (algorithms.Count == 0)
                log.ToolError("No Asset Combine algorithms available.  This will cause problems as it propagates through map data.");

            Debug.Assert(1 == algorithms.Count,
                "Only one IAssetCombineAlgorithm2 support at the current time.");
            if (1 != algorithms.Count)
                throw (new NotSupportedException("Only one IAssetCombineAlgorithm2 support at the current time."));

            // Where our data goes.
            String combineMapCacheDir = Path.GetFullPath(Path.Combine(cacheDir, outputName));
            MapAssetCollection map = new MapAssetCollection(outputName, combineMapCacheDir);

            AssetCombineDocument document;
            if (!algorithms[0].ProcessArea(log, branch, outputName, inputs, out document))
                log.Error("Asset Combine Algorithm failed.  See previous errors.");

            return (document);
        }

        #region Old Map Pipeline Asset Combine (DHM TO REMOVE)
        /// <summary>
        /// Process multiple map combine inputs.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="branch"></param>
        /// <param name="inputs"></param>
        /// <param name="cacheDir"></param>
        /// <param name="outputName"></param>
        /// <param name="outputs"></param>
        /// <returns></returns>
        public static bool Process(IBranch branch, IUniversalLog log,
            AssetCombineInput[] inputs, String cacheDir, String outputName, out MapAssetCollection[] outputs)
        {
            Debug.Assert(inputs.Length > 0, "No inputs specified.");
            if (0 == inputs.Length)
            {
                outputs = new MapAssetCollection[0];
                log.Error("No inputs specified to Asset Combine Processor.");
                return (false);
            }

            bool result = true;
            List<MapAssetCollection> assetOutputs = new List<MapAssetCollection>();
            MapAssetCollection map = ProcessArea(branch, log, inputs, cacheDir, outputName);
            if (null != map)
                assetOutputs.Add(map);
            else
                result &= false;

            outputs = assetOutputs.ToArray();
            return (result);
        }

        /// <summary>
        /// Process a map area; map areas
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="log"></param>
        /// <param name="inputs"></param>
        /// <param name="cacheDir"></param>
        /// <param name="outputName"></param>
        /// <returns></returns>
        private static MapAssetCollection ProcessArea(IBranch branch, IUniversalLog log,
            AssetCombineInput[] inputs, String cacheDir, String outputName)
        {
            // Load algorithms using reflection.
            List<IAssetCombineAlgorithm> algorithms = LoadAlgorithms<IAssetCombineAlgorithm>(log, false);
            if (algorithms.Count == 0)
                log.ToolError("No Asset Combine algorithms available.  This will cause problems as it propagates through map data.");

            // Where our data goes.
            String combineMapCacheDir = Path.GetFullPath(Path.Combine(cacheDir, outputName));
            MapAssetCollection map = new MapAssetCollection(outputName, combineMapCacheDir);
            // Run through our combine algorithms.
            foreach (IAssetCombineAlgorithm algorithm in algorithms)
            {
                algorithm.ProcessArea(branch, log, ref map, inputs);
            }

            return map;
        }
        #endregion // Old Map Pipeline Asset Combine (DHM TO REMOVE)
                
        /// <summary>
        /// Load and create instances of all required algorithms.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="fallback"></param>
        /// <typeparam name="T">Validator interface type</typeparam>
        /// <returns>List of algorithm objects adhering to algorithm interface</returns>
        /// 
        static List<T> LoadAlgorithms<T>(IUniversalLog log, bool fallback)
            where T : class
        {
            Assembly entryAssembly = Assembly.GetCallingAssembly();
            Type[] assemblyTypes = entryAssembly.GetTypes();
            List<T> algorithms = new List<T>();
            
            // Load and create algorithm objects.
            foreach (Type assType in assemblyTypes)
            {
                if (assType.IsAbstract)
                    continue;
                if (!typeof(T).IsAssignableFrom(assType))
                    continue;

                bool isFallback = false;
                bool isDisabled = false;
                Attribute[] attributes = Attribute.GetCustomAttributes(assType);
                foreach (Attribute attr in attributes)
                {
                    if (attr is DisableAlgorithm)
                        isDisabled = true;
                    else if (attr is FallbackAlgorithm)
                        isFallback = true;
                }

                // Only add requested fallback or non-fallback algorithms.
                if (!isDisabled)
                {                
                    if (fallback && isFallback)
                    {                    
                        algorithms.Add(Activator.CreateInstance(assType) as T);
                    }
                    else if (!fallback && !isFallback)
                    {
                        algorithms.Add(Activator.CreateInstance(assType) as T);
                    }
                }
                else
                {
                    log.Warning("Algorithm disabled: {0}.  Not loaded.", assType.Name);
                }
            }
            return (algorithms);
        }
    }

} // RSG.Pipeline.Services.Map.AssetCombine namespace
