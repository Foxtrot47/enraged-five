﻿//---------------------------------------------------------------------------------------------
// <copyright file="IAssetCombineAlgorithm2.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2010-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------


namespace RSG.Pipeline.Services.Map.AssetCombine.Algorithms
{
    using System;
    using System.Collections.Generic;
    using RSG.Base.Collections;
    using RSG.Base.Configuration;
    using RSG.Base.Logging.Universal;
    using RSG.SceneXml;
    using RSG.SceneXml.MapExport.AssetCombine;
    using RSG.Pipeline.Services;

    /// <summary>
    /// Interface representing a combine algorithm; allows us to handle
    /// different combining algorithms independently.
    /// </summary>
    /// Note: this is MKII of the algorithm interface; supporting the newer
    /// classes for serialising out the Asset Combine Document.
    /// 
    internal interface IAssetCombineAlgorithm2
    {    
        /// <summary>
        /// Run the combine algorithm.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="branch"></param>
        /// <param name="mapName"></param>
        /// <param name="inputs"></param>
        /// <param name="document"></param>
        bool ProcessArea(IUniversalLog log, IBranch branch, String mapName,
            IEnumerable<AssetCombineInput> inputs, out AssetCombineDocument document);
    }

} // RSG.Pipeline.Services.Map.AssetCombine.Algorithms namespace

