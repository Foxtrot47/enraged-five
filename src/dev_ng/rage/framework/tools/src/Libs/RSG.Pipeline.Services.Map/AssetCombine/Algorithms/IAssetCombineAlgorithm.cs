﻿using System;
using System.Collections.Generic;
using RSG.Base.Collections;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;
using RSG.SceneXml;
using RSG.SceneXml.MapExport.AssetCombine;
using RSG.Pipeline.Services;

namespace RSG.Pipeline.Services.Map.AssetCombine.Algorithms
{

    /// <summary>
    /// Interface representing a combine algorithm; allows us to handle 
    /// different combining algorithms independently.
    /// </summary>
    internal interface IAssetCombineAlgorithm
    {
        /// <summary>
        /// Run the combine algorithm.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="log"></param>
        /// <param name="map"></param>
        /// <param name="inputs"></param>
        bool ProcessArea(IBranch branch, IUniversalLog log, 
            ref MapAssetCollection map, IEnumerable<AssetCombineInput> inputs);
    }

} // RSG.Pipeline.Services.Map.AssetCombine.Algorithms namespace
