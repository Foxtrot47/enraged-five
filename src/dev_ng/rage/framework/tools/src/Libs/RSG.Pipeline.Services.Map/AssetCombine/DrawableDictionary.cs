﻿//---------------------------------------------------------------------------------------------
// <copyright file="DrawableDictionary.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Map.AssetCombine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using RSG.Pipeline.Services.Map.AssetCombine;

    /// <summary>
    /// Mutable class represents a merged Drawable Dictionary (specifying the input drawables).
    /// </summary>
    public class DrawableDictionary : DictionaryAsset
    {
        #region Constants
        private const String _xmlElemName = "Name";
        private const String _xmlElemTextureDictionaryName = "TextureDictionaryName";
        private const String _xmlElemLODLevel = "LOD";
        private const String _xmlElemInputs = "Inputs";
        private const String _xmlElemInput = "Item";
        private const String _xmlAttrDrawableDictionary = "DrawableDictionary";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Associated Texture Dictionary name.
        /// </summary>
        public String TextureDictionaryName { get; private set; }
        
        /// <summary>
        /// LOD level.
        /// </summary>
        public String LOD { get; private set; }

        /// <summary>
        /// Drawable dictionary inputs (drawables).
        /// </summary>
        public IEnumerable<DrawableDictionaryInput> Inputs { get; private set; }
        #endregion // Properties
                
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="txdName"></param>
        /// <param name="lodLevel"></param>
        /// <param name="inputs"></param>
        public DrawableDictionary(String name, bool isNewDLC, String txdName, String lodLevel, 
            IEnumerable<DrawableDictionaryInput> inputs)
            : base(name, isNewDLC)
        {
            this.TextureDictionaryName = txdName;
            this.LOD = lodLevel;
            this.Inputs = inputs;
        }

        /// <summary>
        /// Constructor (from XElement).
        /// </summary>
        /// <param name="xmlElem"></param>
        public DrawableDictionary(XElement xmlElem)
            : base(xmlElem)
        {
            this.TextureDictionaryName = xmlElem.Element(_xmlElemTextureDictionaryName).Value;
            this.LOD = xmlElem.Element(_xmlElemLODLevel).Value;
            this.Inputs = xmlElem.Element(_xmlElemInputs).Elements(_xmlElemInput).
                Select(input => new DrawableDictionaryInput(input));
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise to XElement.
        /// </summary>
        /// <param name="xmlElemName"></param>
        /// <returns></returns>
        public override XElement ToXElement(String xmlElemName)
        {
            XElement xmlDrawableDictionaryElem = base.ToXElement(xmlElemName);
            xmlDrawableDictionaryElem.Add(
                new XAttribute("type", _xmlAttrDrawableDictionary),                
                new XElement(_xmlElemTextureDictionaryName, this.TextureDictionaryName),
                new XElement(_xmlElemLODLevel, this.LOD),
                new XElement(_xmlElemInputs,
                    this.Inputs.Select(i => i.ToXElement(_xmlElemInput))
                )
            ); // DrawableDictionaryElem

            return (xmlDrawableDictionaryElem);
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Services.Map.AssetCombine namespace
