﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Services.Map.AssetCombine
{

    /// <summary>
    /// Mutable class representing a Drawable (being merged into a Drawable Dictionary).
    /// </summary>
    public class DrawableDictionaryInput : InputAsset
    {
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="type"></param>
        /// <param name="cacheDir"></param>
        /// <param name="sourceZipFilename"></param>
        /// <param name="isNewDLC"></param>
        public DrawableDictionaryInput(String name, String cacheDir, String sourceZipFilename, bool isNewDLC)
            : base(name, cacheDir, sourceZipFilename, isNewDLC)
        {
        }
        
        /// <summary>
        /// Constructor (from XElement).
        /// </summary>
        /// <param name="xmlElem"></param>
        public DrawableDictionaryInput(XElement xmlElem)
            : base(xmlElem)
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise to XElement.
        /// </summary>
        /// <param name="xmlElemName"></param>
        /// <returns></returns>
        public override XElement ToXElement(String xmlElemName)
        {
            XElement xmlBaseElem = base.ToXElement(xmlElemName);
            xmlBaseElem.Add(new XAttribute("type", "DrawableDictionaryInput"));

            return (xmlBaseElem);
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Services.Map.AssetCombine namespace
