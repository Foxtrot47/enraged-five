﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetCombineInput.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2010-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Map.AssetCombine
{
    using System;
    using System.Diagnostics;
    using RSG.SceneXml;

    /// <summary>
    /// Asset Combine process input; loaded SceneXml and some other data.
    /// </summary>
    /// This may encapsulate two input XML nodes from the config XML file;
    /// for DLC, we include the main game and DLC content in a single input
    /// as the DLC input will override anything in the main game.
    /// 
    public class AssetCombineInput
    {
        #region Properties
        /// <summary>
        /// Export SceneXml filename.
        /// </summary>
        public String XmlPathname { get; private set; }
        
        /// <summary>
        /// Export map zip filename.
        /// </summary>
        public String ZipPathname { get; private set; }
        
        /// <summary>
        /// Is this a DLC input?
        /// </summary>
        public bool IsDLC { get; private set; }
        
        /// <summary>
        /// DLC equivalent core input (if IsDLC, null otherwise).
        /// </summary>
        public AssetCombineInput CoreInput { get; private set; }

        /// <summary>
        /// Scene/content-basename.
        /// </summary>
        public String ContentName { get; private set; }
        
        /// <summary>
        /// Loaded SceneXml object.
        /// </summary>
        public Scene Scene { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="combineInput"></param>
        /// <param name="scene"></param>
        private AssetCombineInput(AssetCombineConfigInput combineInput, Scene scene)
        {
            this.ContentName = System.IO.Path.GetFileNameWithoutExtension(
                combineInput.SceneXmlPathname);
            this.XmlPathname = combineInput.SceneXmlPathname;
            this.ZipPathname = combineInput.ZipPathname;
            this.Scene = scene;
            this.IsDLC = combineInput.IsDLC;
        }
        #endregion // Constructor(s)

        #region Static Controller Methods
        /// <summary>
        /// Create regular AssetCombineInput object; from AssetCombineInput and scene.
        /// </summary>
        /// <param name="combineInput"></param>
        /// <param name="scene"></param>
        /// <param name="sceneType"></param>
        /// <returns></returns>
        public static AssetCombineInput Create(AssetCombineConfigInput combineInput, Scene scene)
        {
            return (new AssetCombineInput(combineInput, scene));
        }

        /// <summary>
        /// Create paired DLC/Core AssetCombineInput object.
        /// </summary>
        /// <param name="coreCombineInput"></param>
        /// <param name="coreScene"></param>
        /// <param name="coreSceneType"></param>
        /// <param name="dlcCombineInput"></param>
        /// <param name="dlcScene"></param>
        /// <param name="dlcSceneType"></param>
        /// <returns></returns>
        public static AssetCombineInput Create(AssetCombineConfigInput coreCombineInput, Scene coreScene, SceneType coreSceneType,
            AssetCombineConfigInput dlcCombineInput, Scene dlcScene, SceneType dlcSceneType)
        {
            AssetCombineInput coreInput = new AssetCombineInput(coreCombineInput, coreScene);
            Debug.Assert(!coreInput.IsDLC, "Internal error: DLC input specified as core input.");
            
            AssetCombineInput dlcInput = new AssetCombineInput(dlcCombineInput, dlcScene);
            Debug.Assert(dlcInput.IsDLC, "Internal error: Core input specified as DLC input.");

            dlcInput.CoreInput = coreInput;
            return (dlcInput);
        }
        #endregion // Static Controller Methods
    }

} // RSG.Pipeline.Services.Map.AssetCombine namespace
