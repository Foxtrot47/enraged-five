﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.SceneXml;
using RSG.SceneXml.MapExport;
using MEXAB = RSG.SceneXml.MapExport.AssetCombine;
using RSG.SceneXml.Material;

namespace RSG.Pipeline.Services.Map.AssetCombine.Algorithms
{

    /// <summary>
    /// LOD hierarchy analysis and asset combine algorithm.
    /// </summary>
    /// Use AssetCombineLODHierarchy2 for the future.
    /// 
    internal class AssetCombineLODHierarchy : IAssetCombineAlgorithm
    {
        #region IAssetCombineAlgorithm Methods
        /// <summary>
        /// Run the combine algorithm.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="log"></param>
        /// <param name="map"></param>
        /// <param name="inputs"></param>
        public bool ProcessArea(IBranch branch, IUniversalLog log,
            ref MEXAB.MapAssetCollection map, IEnumerable<AssetCombineInput> inputs)
        {
            using (new ProfileContext(log, String.Format("Processing LOD Hierarchy for {0}.", map.Name)))
            {
                // To correctly build data for SLOD2 objects we need to build a map of SLOD2 objects and their children.
                Dictionary<TargetObjectDef, List<Tuple<TargetObjectDef, AssetCombineInput>>> slod2ChildrenMap = BuildSLOD2ChildrenMap(log, inputs);

                foreach (AssetCombineInput input in inputs)
                {
                    foreach (TargetObjectDef obj in input.Scene.Walk(Scene.WalkMode.DepthFirst))
                    {
                        if (!IsDrawable(obj))
                            continue; // Skip; non-drawable.
                        if (obj.DontExport() || obj.DontExportIDE())
                            continue; // Skip; not-exported.

                        switch (obj.LODLevel)
                        {
                            case ObjectDef.LodLevel.SLOD2:
                                // Use cached inter-container data to build outputs here
                                CreateOutputForInterContainerSiblings(branch, log, ref map, obj, slod2ChildrenMap[obj]);
                                break;
                            case ObjectDef.LodLevel.SLOD4:
                            case ObjectDef.LodLevel.SLOD3:
                            case ObjectDef.LodLevel.SLOD1:
                                // Loop through our immediate children; they are
                                // siblings are drawables will be merged.
                                CreateOutputForSiblings(branch, log, ref map, input, obj);
                                break;
                            default:
                                break; // Ignore objects with no children to merge.
                        }
                    }
                }
            }
            
            return (true);
        }
        #endregion // IAssetCombineAlgorithm Methods

        #region Private Methods
        /// <summary>
        /// Constructs a map matching all SLOD2 objects with their SLOD1 children from an enumeration of content node/scene pairs
        /// </summary>
        /// <param name="log"></param>
        /// <param name="inputs"></param>
        private Dictionary<TargetObjectDef, List<Tuple<TargetObjectDef, AssetCombineInput>>> BuildSLOD2ChildrenMap(
            IUniversalLog log, IEnumerable<AssetCombineInput> inputs)
        {
            // Pass 1: collect all SLOD1 and SLOD2 objects
            List<Tuple<TargetObjectDef, AssetCombineInput>> slod1Objects = new List<Tuple<TargetObjectDef, AssetCombineInput>>();
            List<TargetObjectDef> slod2Objects = new List<TargetObjectDef>();
            foreach (AssetCombineInput input in inputs)
            {
                foreach (TargetObjectDef obj in input.Scene.Walk(Scene.WalkMode.DepthFirst))
                {
                    if (!IsDrawable(obj))
                        continue; // Skip; non-drawable.
                    if (obj.DontExport() || obj.DontExportIDE())
                        continue; // Skip; not-exported.

                    switch (obj.LODLevel)
                    {
                        case ObjectDef.LodLevel.SLOD1:
                            if(obj.LOD != null && obj.LOD.Parent != null && obj.LOD.Parent.IsContainerLODRefObject())
                                slod1Objects.Add(new Tuple<TargetObjectDef, AssetCombineInput>(obj, input));
                            break;
                        case ObjectDef.LodLevel.SLOD2:
                            slod2Objects.Add(obj);
                            break;
                        default:
                            break; // Ignore objects with no children to merge.
                    }
                }
            }

            // Pass 2: build the map
            Dictionary<TargetObjectDef, List<Tuple<TargetObjectDef, AssetCombineInput>>> slod2ChildrenMap =
                new Dictionary<TargetObjectDef, List<Tuple<TargetObjectDef, AssetCombineInput>>>();

            foreach (TargetObjectDef slod2Object in slod2Objects)
            {
                slod2ChildrenMap.Add(slod2Object, new List<Tuple<TargetObjectDef, AssetCombineInput>>());
            }

            foreach (Tuple<TargetObjectDef, AssetCombineInput> slod1Object in slod1Objects)
            {
                TargetObjectDef slod1Parent = slod2Objects.FirstOrDefault(slod2Object => String.Equals(slod2Object.Name, slod1Object.Item1.LOD.Parent.RefName, StringComparison.OrdinalIgnoreCase));
                if (slod1Parent != null)
                    slod2ChildrenMap[slod1Parent].Add(slod1Object);
                else
                    log.Warning("Unable to find SLOD2 parent for '{0}'", slod1Object.Item1.Name);
            }

            return (slod2ChildrenMap);
        }

        /// <summary>
        /// Create AssetOutputs for LOD children  of an ObjectDef.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="log"></param>
        /// <param name="map"></param>
        /// <param name="input"></param>
        /// <param name="parent"></param>
        private void CreateOutputForSiblings(IBranch branch, IUniversalLog log,
            ref MEXAB.MapAssetCollection map, AssetCombineInput input, TargetObjectDef parent)
        {
            // Validate input ObjectDef.
            Debug.Assert(parent.HasLODChildren(),
                String.Format("Internal error: parent object {0} doesn't have LOD children.", parent.Name));
            if (!parent.HasLODChildren())
            {
                log.WarningCtx(parent.Name, "Internal error: parent object {0} doesn't have LOD children.", parent.Name);
                return;
            }
            // Verify siblings being built have the same TXD DLC flags.  As they are being merged
            // they need the same TXD DLC flag set; either referencing disc or not.
            // This ensures that we only output one TXD for the merged drawables.
            if (input.IsDLC)
            {
                IEnumerable<TargetObjectDef> newDlcChildren = parent.LOD.Children.Where(o => o.IsNewDLCObject());
                bool allNewTxdConsistent = newDlcChildren.All(o => o.HasNewTxd()) || newDlcChildren.All(o => !o.HasNewTxd());
                if (!allNewTxdConsistent)
                {
                    log.ErrorCtx(parent.Name, "Only some LOD children of '{0}' are set to export a new TXD.  Since they will be merged all need to be consistent.",
                        parent.ObjectDef.Name);
                }
            }

            String zipPathname = input.ZipPathname;
            String contentName = Path.GetFileNameWithoutExtension(parent.MyScene.Filename);

            // avoid recreating a full combine of non DLC object by renaming them
            bool containsNewDlcChild = (branch.Project.ForceFlags.AllNewContent || parent.LOD.Children.Any(o=> o.IsNewDLCObject()));
            IProject coreProject = branch.Project.Config.CoreProject;
            IProject project = branch.Project;

            String dictionaryName = MapAsset.AppendMapPrefixIfRequired(project, string.Format("{0}_children", parent.Name));

            MEXAB.DrawableDictionary iddOutput = new MEXAB.DrawableDictionary(dictionaryName, containsNewDlcChild);
            MEXAB.TextureDictionary txdOutput = new MEXAB.TextureDictionary(parent.Name, containsNewDlcChild, false);

            MEXAB.TextureDictionary newTxdOutput = new MEXAB.TextureDictionary(dictionaryName, containsNewDlcChild, true);

            foreach (TargetObjectDef child in parent.LOD.Children)
            {
                if (!IsDrawable(child))
                    continue; // Skip; non-drawable.
                if (child.DontExport() || child.DontExportIDE())
                    continue; // Skip; not-exported.

                // When we're DLC ensure we get the right source zip.
                bool isNewDlcObject = (project.ForceFlags.AllNewContent || child.IsNewDLCObject());
                String cacheDir;
                if (input.IsDLC)
                {
                    if (isNewDlcObject)
                    {
                        zipPathname = input.ZipPathname;
                        cacheDir = Path.Combine(project.Cache, "maps", branch.Name, contentName);
                    }
                    else
                    {
                        zipPathname = input.CoreInput.ZipPathname;
                        cacheDir = Path.Combine(coreProject.Cache, "maps", branch.Name, contentName);
                    }
                }
                else
                {
                    cacheDir = Path.Combine(coreProject.Cache, "maps", branch.Name, contentName);
                }

                bool isNewTxd = child.HasNewTxd();

                MEXAB.AssetInput drawableAsset = new MEXAB.AssetInput(child.GetObjectName(), cacheDir, zipPathname, MEXAB.AssetInput.InputType.Drawable, isNewDlcObject, isNewTxd);
                iddOutput.AddInput(drawableAsset);

                if (isNewTxd)
                    AddTextureDictionaryInputs(branch, child, newTxdOutput, contentName, input);
                else
                    AddTextureDictionaryInputs(branch, child, txdOutput, contentName, input);

            }

            MEXAB.DrawableDictionary.LodLevel lod = MEXAB.DrawableDictionary.LodLevel.HD;
            if (parent.LOD.Children.Any())
            {
                lod = MEXAB.DrawableDictionary.GetLodLevel(parent.LOD.Children.First());
            }

            iddOutput.LOD = lod;

            // Append our outputs to the system.
            if (iddOutput.Inputs.Length > 0)
            {
                map.AddContent(iddOutput);
                if (txdOutput.Inputs.Length > 1)
                {
                    iddOutput.TXD = txdOutput.Name;
                    map.AddContent(txdOutput);
                }
                else if (1 == txdOutput.Inputs.Length && newTxdOutput.Inputs.None())
                {
                    iddOutput.TXD = txdOutput.Inputs[0].Name;
                }
                else if (newTxdOutput.Inputs.Length == 1)
                {
                    // TRF Note - check if the child is a new txd. If so we need to prefix it.
                    // this is because the metadata serialiser will take this name as it is and write it
                    // directly into the ITYP with no additional processing.
                    String dlcTxdName = newTxdOutput.Inputs[0].Name;
                    if (newTxdOutput.Inputs[0].IsNewTXD)
                    {
                        dlcTxdName = MapAsset.AppendMapPrefixIfRequired(project, dlcTxdName);
                    }

                    iddOutput.TXD = dlcTxdName;
                }
                else if (newTxdOutput.Inputs.Any())
                {
                    iddOutput.TXD = newTxdOutput.Name;
                    map.AddContent(newTxdOutput);
                }
            }
        }

        /// <summary>
        /// Create AssetOutputs for child siblings of an ObjectDef that are supplied and assumed to be SLOD1.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="log"></param>
        /// <param name="map"></param>
        /// <param name="parent"></param>
        /// <param name="children"></param>
        private void CreateOutputForInterContainerSiblings(IBranch branch, IUniversalLog log,
            ref MEXAB.MapAssetCollection map, TargetObjectDef parent,
            IEnumerable<Tuple<TargetObjectDef, AssetCombineInput>> children)
        {
            // Verify siblings being built have the same TXD DLC flags.  As they are being merged
            // they need the same TXD DLC flag set; either referencing disc or not.
            // This ensures that we only output one TXD for the merged drawables.
            if (branch.Project.IsDLC)
            {
                IEnumerable<TargetObjectDef> newDlcChildren = children.Select(p => p.Item1).Where(o => o.IsNewDLCObject());
                bool allNewTxdConsistent = newDlcChildren.All(o => o.HasNewTxd()) || newDlcChildren.All(o => !o.HasNewTxd());
                if (!allNewTxdConsistent)
                {
                    log.ErrorCtx(parent.Name, "Only some LOD children of '{0}' are set to export a new TXD.  Since they will be merged all need to be consistent.",
                        parent.ObjectDef.Name);
                }
            }

            // avoid recreating a full combine of non DLC object by renaming them
            bool containsNewDlcChild = (branch.Project.ForceFlags.AllNewContent || children.Any(pair => pair.Item1.IsNewDLCObject()));
            IProject coreProject = branch.Project.Config.CoreProject;
            IProject project = branch.Project;

            string dictionaryName = string.Format("{0}_children", parent.Name);
            string drawableDictionaryName = MapAsset.AppendMapPrefixIfRequired(project, dictionaryName);
            string textureDictionaryName = dictionaryName; // keep the txd name

            MEXAB.DrawableDictionary iddOutput = new MEXAB.DrawableDictionary(drawableDictionaryName, textureDictionaryName, MEXAB.DrawableDictionary.LodLevel.SLOD1, containsNewDlcChild);
            MEXAB.TextureDictionary txdOutput = new MEXAB.TextureDictionary(textureDictionaryName, containsNewDlcChild);
            bool createNewTxd = children.Any(pair => pair.Item1.HasNewTxd());

            MEXAB.TextureDictionary newTxdOutput = null;
            if (createNewTxd)
            {
                string newTextureDictionaryName = MapAsset.AppendMapPrefixIfRequired(project, dictionaryName);
                newTxdOutput = new MEXAB.TextureDictionary(newTextureDictionaryName, containsNewDlcChild, createNewTxd);
            }


            foreach (Tuple<TargetObjectDef, AssetCombineInput> childItem in children)
            {
                TargetObjectDef child = childItem.Item1;
                AssetCombineInput input = childItem.Item2;

                if (!IsDrawable(child))
                    continue; // Skip; non-drawable.
                if (child.DontExport() || child.DontExportIDE())
                    continue; // Skip; not-exported.

                String zipPathname = input.ZipPathname;
                String contentName = Path.GetFileNameWithoutExtension(child.MyScene.Filename);
                String cacheDir = String.Empty;

                // When we're DLC ensure we get the right source zip.
                bool isNewDlcObject = (project.ForceFlags.AllNewContent || child.IsNewDLCObject());
                if (input.IsDLC)
                {
                    if (isNewDlcObject)
                    {
                        zipPathname = input.ZipPathname;
                        cacheDir = Path.Combine(project.Cache, "maps", branch.Name, contentName);
                    }
                    else
                    {
                        zipPathname = input.CoreInput.ZipPathname;
                        cacheDir = Path.Combine(coreProject.Cache, "maps", branch.Name, contentName);
                    }
                }
                else
                {
                    cacheDir = Path.Combine(coreProject.Cache, "maps", branch.Name, contentName);
                }

                bool isNewTxd = child.HasNewTxd();

                if (isNewTxd)
                {
                    AddTextureDictionaryInputs(branch, child, newTxdOutput, contentName, input);
                    iddOutput.TXD = newTxdOutput.Name;
                }
                else
                {
                    AddTextureDictionaryInputs(branch, child, txdOutput, contentName, input);
                }

                MEXAB.AssetInput drawableAsset = new MEXAB.AssetInput(child.GetObjectName(), cacheDir, zipPathname, MEXAB.AssetInput.InputType.Drawable, isNewDlcObject, isNewTxd);
                iddOutput.AddInput(drawableAsset);
                
            }

            // Append our outputs to the system.
            if (iddOutput.Inputs.Any())
                map.AddContent(iddOutput);
            if (txdOutput.Inputs.Any())
                map.AddContent(txdOutput);
            if(newTxdOutput!= null && newTxdOutput.Inputs.Any())
                map.AddContent(newTxdOutput);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="objectDef"></param>
        /// <param name="textureDictionary"></param>
        /// <param name="contentName"></param>
        /// <param name="input"></param>
        private static void AddTextureDictionaryInputs(IBranch branch, TargetObjectDef objectDef,
            MEXAB.TextureDictionary textureDictionary, String contentName, AssetCombineInput input)
        {
            String txdName = objectDef.GetAttribute(AttrNames.OBJ_REAL_TXD, AttrDefaults.OBJ_REAL_TXD);
            if (txdName != AttrDefaults.OBJ_REAL_TXD)
            {
                IProject project = branch.Project;
                IProject coreProject = branch.Project.Config.CoreProject;

                String zipPathname = input.ZipPathname;
                String cacheDir;

                // When we're DLC ensure we get the right source zip.
                bool isNewDlcObject = (branch.Project.ForceFlags.AllNewContent || objectDef.IsNewDLCObject());
                if (input.IsDLC)
                {
                    if (isNewDlcObject)
                    {
                        zipPathname = input.ZipPathname;
                        cacheDir = Path.Combine(project.Cache, "maps", branch.Name, contentName);
                    }
                    else 
                    {
                        zipPathname = input.CoreInput.ZipPathname;
                        cacheDir = Path.Combine(coreProject.Cache, "maps", branch.Name, contentName);
                    }
                }
                else
                {
                    cacheDir = Path.Combine(coreProject.Cache, "maps", branch.Name, contentName);
                }

                if (!textureDictionary.HasInput(txdName))
                {
                    bool isNewTxd = objectDef.HasNewTxd();
                    textureDictionary.AddInput(new MEXAB.AssetInput(txdName, cacheDir, zipPathname, MEXAB.AssetInput.InputType.TextureDictionary, isNewDlcObject, isNewTxd));
                }

                if (objectDef.Material != Guid.Empty)
                {
                    MaterialDef childMaterialDef = objectDef.MyScene.MaterialLookup[objectDef.Material];
                    IEnumerable<String> textureBasenames = childMaterialDef.GetTextureBasenamesRecursive();
                    foreach (String textureBasename in textureBasenames)
                    {
                        textureDictionary.GetInput(txdName).AddRequiredItem(textureBasename);
                    }
                }
            }
        }
        
        /// <summary>
        /// Determine whether an ObjectDef is a usable Drawable.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private bool IsDrawable(TargetObjectDef obj)
        {
            if (!obj.IsObject())
                return (false); // Ignore non-objects.
            if (obj.IsRefObject() || obj.IsXRef())
                return (false); // Ignore external reference objects.
            if (obj.IsRefInternalObject() || obj.IsInternalRef())
                return (false); // Ignore internal reference objects.
            if (obj.IsFragmentProxy())
                return (false);
            if (obj.IsAnimProxy())
                return (false);

            return (true);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Services.Map.AssetCombine.Algorithms namespace
