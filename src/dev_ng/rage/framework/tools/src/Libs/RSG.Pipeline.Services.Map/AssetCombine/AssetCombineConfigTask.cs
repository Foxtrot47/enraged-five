﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetCombineConfigTask.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Map.AssetCombine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;

    /// <summary>
    /// AssetCombine task class; representing an Asset Combine process.
    /// </summary>
    public class AssetCombineConfigTask
    {
        #region Properties
        public String Name { get; private set; }
        public IEnumerable<AssetCombineConfigInput> Inputs { get; private set; }
        public AssetCombineConfigOutput Output { get; private set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor (from XElement).
        /// </summary>
        /// <param name="element"></param>
        public AssetCombineConfigTask(XElement xmlElem)
        {
            this.Name = xmlElem.Element(_xmlElemName).Value;
            Inputs = xmlElem.Element(_xmlElemInputs).Elements(_xmlElemInput).Select(
                elem => new AssetCombineConfigInput(elem)).ToArray();
            Output = new AssetCombineConfigOutput(xmlElem.Element(_xmlOutputElem));
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="inputs"></param>
        /// <param name="output"></param>
        public AssetCombineConfigTask(String name, IEnumerable<AssetCombineConfigInput> inputs, AssetCombineConfigOutput output)
        {
            this.Name = name;
            this.Inputs = inputs.ToArray();
            this.Output = output;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise to XML.
        /// </summary>
        /// <param name="elementName"></param>
        /// <returns></returns>
        public XElement ToXElement(String elementName)
        {
            XElement xmlTaskElem = new XElement(elementName,
                new XAttribute("type", "AssetCombineConfigTask"),
                new XElement(_xmlElemName, this.Name),
                new XElement(_xmlElemInputs,
                    this.Inputs.Select(i => i.ToXElement(_xmlElemInput))
                ), // Inputs
                Output.ToXElement()
            ); // xmlTaskElem
            return (xmlTaskElem);
        }
        #endregion // Controller Methods

        #region Private Data
        private const String _xmlElemName = "Name";
        private const String _xmlElemInputs = "Inputs";
        private const String _xmlElemInput = "Item";
        private const String _xmlOutputElem = "Output";
        #endregion // Private Data
    }

} // RSG.Pipeline.Services.Map.AssetCombine namespace
