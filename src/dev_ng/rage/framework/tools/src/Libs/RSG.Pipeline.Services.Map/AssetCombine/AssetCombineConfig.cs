﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetCombineConfig.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2012-2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Map.AssetCombine
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Xml.Linq;
    
    /// <summary>
    /// Asset Combine configuration data.
    /// </summary>
    public class AssetCombineConfig
    {
        #region Properties
        /// <summary>
        /// Asset Combine tasks to execute.
        /// </summary>
        public IEnumerable<AssetCombineConfigTask> Tasks { get; private set; }
        #endregion Properties
        
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="filename"></param>
        private AssetCombineConfig(String filename)
        {
            XDocument document = XDocument.Load(filename);
            Debug.Assert(document.Root.Name.Equals(_xmlElemDocument));

            this.Tasks = document.Root.Element(_xmlElemTasks).Elements(_xmlElemTask).Select(
                elem => new AssetCombineConfigTask(elem)).ToArray();
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="tasks"></param>
        public AssetCombineConfig(IEnumerable<AssetCombineConfigTask> tasks)
        {
            this.Tasks = tasks.ToArray();
        }
        #endregion Constructor(s)
        
        #region Controller Methods
        /// <summary>
        /// Save configuration object to an XML file.
        /// </summary>
        /// <param name="filename"></param>
        public void Save(String filename)
        {
            XDocument document = new XDocument(
                new XElement(_xmlElemDocument,
                    new XElement(_xmlElemTasks,
                        Tasks.Select(task => task.ToXElement(_xmlElemTask)))
                    )
                );
            document.Save(filename);
        }
        #endregion // Controller Methods

        #region Static Controller Methods
        /// <summary>
        /// Load file into a configuration object.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static AssetCombineConfig Load(String filename)
        {
            return (new AssetCombineConfig(filename));
        }
        #endregion // Static Controller Methods

        #region Private Data
        private const String _xmlElemDocument = "AssetCombineConfig";
        private const String _xmlElemTasks = "Tasks";
        private const String _xmlElemTask = "Item";
        #endregion // Private Data
    }

} // RSG.Pipeline.Services.Map.AssetCombine namespace
