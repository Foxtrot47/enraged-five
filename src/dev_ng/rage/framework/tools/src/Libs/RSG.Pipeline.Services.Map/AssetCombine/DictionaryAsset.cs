﻿//---------------------------------------------------------------------------------------------
// <copyright file="DictionaryAsset.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Services.Map.AssetCombine
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Linq;
    using RSG.Pipeline.Services.Map.AssetCombine;

    /// <summary>
    /// Abstract base class represents a merged dictionary asset.
    /// </summary>
    public abstract class DictionaryAsset
    {
        #region Constants
        private const String _xmlElemName = "Name";
        private const String _xmlElemIsNewDLC = "IsNewDLC";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Drawable dictionary name.
        /// </summary>
        public String Name { get; private set; }

        /// <summary>
        /// Whether the dictionary is new for DLC.
        /// </summary>
        public bool IsNewDLC { get; private set; }
        #endregion // Properties
        
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="isNewDLC"></param>
        /// <param name="inputs"></param>
        public DictionaryAsset(String name, bool isNewDLC)
        {
            this.Name = name;
            this.IsNewDLC = isNewDLC;
        }

        /// <summary>
        /// Constructor (from XElement).
        /// </summary>
        /// <param name="element"></param>
        public DictionaryAsset(XElement xmlElem)
        {
            this.Name = xmlElem.Element(_xmlElemName).Value;
            bool isNewDLC = false;
            if (bool.TryParse(xmlElem.Element(_xmlElemIsNewDLC).Value, out isNewDLC))
                this.IsNewDLC = isNewDLC;
            else
                this.IsNewDLC = false;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise to XElement.
        /// </summary>
        /// <param name="xmlElemName"></param>
        /// <returns></returns>
        public virtual XElement ToXElement(String xmlElemName)
        {
            XElement xmlDictionaryElem = new XElement(xmlElemName,
                new XElement(_xmlElemName, this.Name),
                new XElement(_xmlElemIsNewDLC, this.IsNewDLC)
            ); // xmlDictionaryElem

            return (xmlDictionaryElem);
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Services.Map.AssetCombine namespace
