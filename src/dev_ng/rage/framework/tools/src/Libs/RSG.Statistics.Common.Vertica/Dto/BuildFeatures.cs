﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Vertica.Dto
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class BuildFeatures
    {
        /// <summary>
        /// Build this information is for.
        /// </summary>
        [DataMember]
        public uint BuildVersion { get; set; }

        /// <summary>
        /// Map only automated stats.
        /// </summary>
        [DataMember]
        public bool HasMapOnlyStats { get; set; }

        /// <summary>
        /// Everything automated stats.
        /// </summary>
        [DataMember]
        public bool HasEverythingStats { get; set; }

        /// <summary>
        /// Memory shortfall automated stats.
        /// </summary>
        [DataMember]
        public bool HasMemShortfallStats { get; set; }

        /// <summary>
        /// Physics shape cost automated stats.
        /// </summary>
        [DataMember]
        public bool HasPhysicsShapeCostStats { get; set; }

        /// <summary>
        /// Night time map only stats.
        /// </summary>
        [DataMember]
        public bool HasNightTimeMapOnlyStats { get; set; }
    } // BuildFeatures
}
