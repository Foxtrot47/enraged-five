﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Vertica.Dto.ReportResults
{
    /// <summary>
    /// Result transfer object for communication between stats/vertica servers.
    /// </summary>
    [DataContract]
    [ReportType]
    public class WeaponModPurchaseResult
    {
        /// <summary>
        /// Hash of the weapon.
        /// </summary>
        [DataMember]
        public uint WeaponHash { get; set; }

        /// <summary>
        /// Hash of the weapon mod.
        /// </summary>
        [DataMember]
        public uint WeaponModHash { get; set; }

        /// <summary>
        /// Number of times the item was purchased.
        /// </summary>
        [DataMember]
        public ulong TimesPurchased { get; set; }

        /// <summary>
        /// Number of unique gamers that purchased this item.
        /// </summary>
        [DataMember]
        public ulong UniqueGamers { get; set; }

        /// <summary>
        /// Total amount of money spent to purchase this item.
        /// </summary>
        [DataMember]
        public ulong TotalSpent { get; set; }
    } // WeaponModPurchaseResult
}
