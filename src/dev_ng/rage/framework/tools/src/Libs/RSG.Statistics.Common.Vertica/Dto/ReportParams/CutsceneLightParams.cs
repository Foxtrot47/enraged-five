﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.ROS;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Vertica.Dto.ReportParams
{
    /// <summary>
    /// Parameter transfer object for communication between stats/vertica servers.
    /// </summary>
    [DataContract]
    [ReportType]
    public class CutsceneLightParams
    {
        [DataMember]
        public uint Build { get; set; }

        [DataMember]
        public ROSPlatform Platform { get; set; }

        [DataMember]
        public bool? DepthOfFieldActive { get; set; }

        [DataMember]
        public bool? CameraApproved { get; set; }

        [DataMember]
        public bool? LightingApproved { get; set; }
    } // CutsceneLightParams
}
