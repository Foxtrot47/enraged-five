﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Vertica.Dto.ReportParams
{
    /// <summary>
    /// Parameter transfer object for communication between stats/vertica servers.
    /// </summary>
    [DataContract]
    [ReportType]
    public class MPExpenditureParams : TelemetryParams
    {
        [DataMember]
        public IDictionary<VehicleCategory, IList<uint>> VehicleLookup { get; set; }

        [DataMember]
        public IList<uint> ShopHashes { get; set; }
    } // MPExpenditureParams
}
