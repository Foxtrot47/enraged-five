﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using RSG.Statistics.Common.Vertica.Report;

namespace RSG.Statistics.Common.Vertica.ServiceContract
{
    /// <summary>
    /// Vertica based report service.
    /// </summary>
    [ServiceContract]
    [ServiceKnownType("GetKnownTypes", typeof(VerticaReportCollection))]
    public interface IReportService
    {
        /// <summary>
        /// Executes a particular report with the passed in parameters.
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "{reportIdentifier}")]
        object RunReport(String reportIdentifier, object parameters);
    } // IVerticaReportService
}
