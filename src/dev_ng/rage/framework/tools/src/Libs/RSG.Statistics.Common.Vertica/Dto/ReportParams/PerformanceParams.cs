﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.ROS;

namespace RSG.Statistics.Common.Vertica.Dto.ReportParams
{
    /// <summary>
    /// Class for performance related params.
    /// </summary>
    [DataContract]
    [ReportType]
    public class PerformanceParams
    {
        [DataMember]
        public uint Build { get; set; }

        [DataMember]
        public ROSPlatform Platform { get; set; }

        [DataMember]
        public BuildConfig BuildConfig { get; set; }
    } // PerformanceParams
}
