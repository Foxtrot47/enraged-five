﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace RSG.Statistics.Common.Vertica.Dto
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    public class MissionSearchFilters
    {
        [DataMember]
        public MatchType? MatchType { get; set; }

        [DataMember]
        public FreemodeMissionCategory? MissionCategory { get; set; }

        [DataMember]
        public bool? PublishedOnly { get; set; }
    } // MissionSearchFilters
}
