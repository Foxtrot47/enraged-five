﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.ROS;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Vertica.Dto.ReportParams
{
    /// <summary>
    /// Parameter transfer object for communication between stats/vertica servers.
    /// </summary>
    [DataContract]
    [ReportType]
    public class PhysicsShapeCostParams
    {
        [DataMember]
        public uint Build { get; set; }

        [DataMember]
        public ROSPlatform Platform { get; set; }

        [DataMember]
        public uint Level { get; set; }

        [DataMember]
        public uint Resolution { get; set; }
    } // PhysicsShapeCostParams
}
