﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Vertica.Dto.ReportResults
{
    /// <summary>
    /// Result transfer object for communication between stats/vertica servers.
    /// </summary>
    [DataContract]
    [ReportType]
    public class CutsceneViewResult
    {
        /// <summary>
        /// Hash of the shop item.
        /// </summary>
        [DataMember]
        public uint CutsceneHash { get; set; }

        /// <summary>
        /// How long people spent on average watching this cutscene.
        /// </summary>
        [DataMember]
        public double AverageTimeWatched { get; set; }

        /// <summary>
        /// Number of times we registered that the cutscene started playing.
        /// </summary>
        [DataMember]
        public ulong Started { get; set; }

        /// <summary>
        /// Number of times users chose to skip the cutscene.
        /// </summary>
        [DataMember]
        public ulong Skipped { get; set; }

        /// <summary>
        /// Number of distinct gamers that have started viewing this cutscene.
        /// </summary>
        [DataMember]
        public ulong NumGamers { get; set; }

        /// <summary>
        /// Number of gamers that have watched this cutscene to completion.
        /// </summary>
        [DataMember]
        public ulong PlayersWatchedToCompletion { get; set; }
    } // CutsceneViewResult
}
