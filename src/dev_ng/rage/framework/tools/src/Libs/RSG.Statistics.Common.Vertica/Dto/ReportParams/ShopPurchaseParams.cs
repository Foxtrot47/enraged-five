﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Vertica.Dto.ReportParams
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [ReportType]
    public class ShopPurchaseParams : TelemetryParams
    {
        [DataMember]
        public IList<Tuple<ShopType, IList<uint>>> Groupings { get; set; }

        [DataMember]
        public IList<uint> PaintColourHashes { get; set; }

        [DataMember]
        public IList<String> PaintColourLabels { get; set; }
    } // ShopPurchaseParams
}
