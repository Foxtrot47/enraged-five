﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Vertica.Dto.ReportParams
{
    /// <summary>
    /// Parameter transfer object for communication between stats/vertica servers.
    /// </summary>
    [DataContract]
    [ReportType]
    public class FreemodeTelemetryParams : TelemetryParams
    {
        [DataMember]
        public List<String> TutorialRaceRootContentIds { get; set; }

        [DataMember]
        public List<String> TutorialMissionRootContentIds { get; set; }

        [DataMember]
        public List<String> TutorialLTSMissionRootContentIds { get; set; }
    } // MPTutorialParams
}
