﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.ROS;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Vertica.Dto.ReportParams
{
    /// <summary>
    /// Parameter transfer object for communication between stats/vertica servers.
    /// </summary>
    [DataContract]
    public abstract class ProfileStatParams
    {
        [DataMember]
        public List<ROSPlatform> Platforms { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public int? AgeMin { get; set; }

        [DataMember]
        public int? AgeMax { get; set; }

        [DataMember]
        public List<String> CountryCodes { get; set; }

        [DataMember]
        public List<String> SocialClubUsernames { get; set; }

        [DataMember]
        public List<Tuple<String, ROSPlatform>> GamerHandlePlatformPairs { get; set; }

        [DataMember]
        public double? BucketSize { get; set; }
    } // ProfileStatParams
    
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [ReportType]
    public class AggregateProfileStatParams : ProfileStatParams
    {
        [DataMember]
        public List<String> StatNames { get; set; }

        [DataMember]
        public String AggregateFunction { get; set; }
    } // AggregateProfileStatParams

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [ReportType]
    public class CombinedProfileStatParams : ProfileStatParams
    {
        [DataMember]
        public IList<Tuple<String, String>> StatNames { get; set; }

        [DataMember]
        public bool IgnoreDefaultValues { get; set; }

        [DataMember]
        public float MaxValue { get; set; }

        [DataMember]
        public float? MinValue { get; set; }
    } // CombinedProfileStatParams

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [ReportType]
    public class DividedProfileStatParams : ProfileStatParams
    {
        [DataMember]
        public IList<Tuple<String, String>> NumeratorStatNames { get; set; }

        [DataMember]
        public IList<Tuple<String, String>> DenominatorStatNames { get; set; }

        [DataMember]
        public float MaxNumeratorValue { get; set; }

        [DataMember]
        public float MaxDenominatorValue { get; set; }
    } // DividedProfileStatParams

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [ReportType]
    public class HistoricalProfileStatParams : ProfileStatParams
    {
        [DataMember]
        public List<String> StatNames { get; set; }

        [DataMember]
        public uint SampleIntervalMinutes { get; set; }
    } // HistoricalProfileStatParams
}
