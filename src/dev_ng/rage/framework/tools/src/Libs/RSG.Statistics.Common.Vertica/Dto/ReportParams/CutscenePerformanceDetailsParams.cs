﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Vertica.Dto.ReportParams
{
    /// <summary>
    /// Parameter transfer object for communication between stats/vertica servers.
    /// </summary>
    [DataContract]
    [ReportType]
    public class CutscenePerformanceDetailsParams : CutscenePerformanceParams
    {
        /// <summary>
        /// Hash of the cutscene that we wish to retrieve detailed information for.
        /// </summary>
        [DataMember]
        public uint CutsceneHash { get; set; }
    } // CutscenePerformanceDetailsParams
}
