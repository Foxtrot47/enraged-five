﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.ROS;

namespace RSG.Statistics.Common.Vertica.Dto.ReportParams
{
    /// <summary>
    /// Parameter transfer object for communication between stats/vertica servers.
    /// </summary>
    [DataContract]
    [ReportType]
    public class TelemetryParams
    {
        [DataMember]
        public List<ROSPlatform> Platforms { get; set; }

        [DataMember]
        public GameType? GameTypeRestriction { get; set; }

        [DataMember]
        public List<uint> Builds { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }

        [DataMember]
        public uint? CompanywidePlaytestId { get; set; }

        [DataMember]
        public List<Tuple<String, ROSPlatform>> GamerHandlePlatformPairs { get; set; }

        [DataMember]
        public int? AgeMin { get; set; }

        [DataMember]
        public int? AgeMax { get; set; }

        [DataMember]
        public List<String> CountryCodes { get; set; }

        [DataMember]
        public List<String> SocialClubUsernames { get; set; }

        [DataMember]
        public uint? CharacterSlot { get; set; }
    } // TelemetryParams
}
