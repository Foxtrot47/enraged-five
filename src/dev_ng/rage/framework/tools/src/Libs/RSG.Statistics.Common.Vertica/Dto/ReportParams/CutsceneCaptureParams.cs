﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.ROS;

namespace RSG.Statistics.Common.Vertica.Dto.ReportParams
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [ReportType]
    public class CutsceneCaptureParams
    {
        [DataMember]
        public uint ZoneHash { get; set; }

        [DataMember]
        public ROSPlatform Platform { get; set; }
    } // CutsceneCaptureParams
}
