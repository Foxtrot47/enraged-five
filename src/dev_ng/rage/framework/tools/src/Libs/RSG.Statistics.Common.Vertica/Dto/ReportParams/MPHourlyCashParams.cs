﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Vertica.Dto.ReportParams
{
    /// <summary>
    /// Parameter transfer object for communication between stats/vertica servers.
    /// </summary>
    [DataContract]
    [ReportType]
    public class MPHourlyCashParams : TelemetryParams
    {
        [DataMember]
        public bool IncludeDebug { get; set; }

        [DataMember]
        public uint MaxEarnAmount { get; set; }

        [DataMember]
        public uint MaxXPAmount { get; set; }
    } // MPHourlyCashParams
}
