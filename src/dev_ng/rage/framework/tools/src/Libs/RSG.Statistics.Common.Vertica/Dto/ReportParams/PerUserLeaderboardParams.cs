﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.ROS;

namespace RSG.Statistics.Common.Vertica.Dto.ReportParams
{
    /// <summary>
    /// Per user leaderboard reports require the leaderboard version to check to be sent along with each player.
    /// </summary>
    [DataContract]
    [ReportType]
    public class PerUserLeaderboardParams : PerUserParams
    {
        [DataMember]
        public new IDictionary<Tuple<String, ROSPlatform>, uint> GamerHandleLeaderboardVersions { get; set; }
    } // PerUserLeaderboardParams
}
