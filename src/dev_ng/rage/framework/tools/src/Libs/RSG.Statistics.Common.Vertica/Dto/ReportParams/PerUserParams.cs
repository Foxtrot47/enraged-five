﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.ROS;

namespace RSG.Statistics.Common.Vertica.Dto.ReportParams
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [ReportType]
    public abstract class PerUserParams
    {
        [DataMember]
        public List<Tuple<String, ROSPlatform>> GamerHandlePlatformPairs { get; set; }

        [DataMember]
        public DateTime? StartDate { get; set; }

        [DataMember]
        public DateTime? EndDate { get; set; }
    } // PerUserParams

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [ReportType]
    public class PerUserTelemetryParams : PerUserParams
    {
        [DataMember]
        public List<uint> Builds { get; set; }
    } // PerUserTelemetryParams
}
