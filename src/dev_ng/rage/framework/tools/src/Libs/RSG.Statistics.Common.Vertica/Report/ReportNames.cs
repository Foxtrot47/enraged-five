﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Statistics.Common.Vertica.Report
{
    /// <summary>
    /// Static class to store report names.
    /// Shared between the Vertica service and Statistic service assemblies.
    /// </summary>
    public static class ReportNames
    {
        // Profile
        public static readonly String CombinedProfileStatReport = "ProfileCombined";
        public static readonly String CombinedDiffProfileStatReport = "ProfileCombinedDiff";
        public static readonly String DividedProfileStatReport = "ProfileDivided";
        public static readonly String DividedDiffProfileStatReport = "ProfileDividedDiff";
        public static readonly String HistoricalProfileStatReport = "ProfileHistorical";
        public static readonly String LatestProfileStatReport = "ProfileLatest";
        public static readonly String AggregateProfileStatReport = "ProfileAggregate";

        // Telemetry.Captures
        public static readonly String CutsceneCaptureReport = "CutsceneCaptures";

        // Telemetry.Debug
        public static readonly String CutsceneLightsReport = "CutsLights";
        public static readonly String OnlineGamersReport = "OnlineGamers";

        // Telemetry.Performance
        public static readonly String BasicFpsPerformanceReport = "BasicFpsPerf";
        public static readonly String ClipUsageReport = "ClipUsage";
        public static readonly String CutscenePerformanceReport = "CutsPerf";
        public static readonly String CutscenePerformanceDetailsReport = "CutsPerfDetails";
        public static readonly String FpsDrawListPerformanceReport = "FpsDrawListPerf";
        public static readonly String FpsPerformanceReport = "FpsPerf";
        public static readonly String MemoryShortfallReport = "MemShortfall";
        public static readonly String MemoryPoolsReport = "MemoryPools";
        public static readonly String MemorySkeletonsReport = "MemorySkeletons";
        public static readonly String MemoryStoresReport = "MemoryStores";
        public static readonly String MemoryUsageReport = "MemoryUsage";
        public static readonly String MissionPerformanceReport = "MissionPerf";
        public static readonly String PhsyicsShapeCostReport = "PhysicsShapeCost";

        // Telemetry.Online
        public static readonly String MissionParticipantReport = "MissionParticipant";
        public static readonly String NewBettingReport = "NewMPBetting";
        public static readonly String WeatherReport = "MPWeather";

        // Telemetry.PlayerStats
        public static readonly String CashPurchaseReport = "CashPurchase";
        public static readonly String CharacterCashReport = "CharacterCash";
        public static readonly String CharacterSkillsReport = "CharacterSkills";
        public static readonly String ConcurrentUsersReport = "ConcurrentUsers";
        public static readonly String CutscenesWatchedReport = "CutsWatched";
        public static readonly String FreemodeAmbientMissionsReport = "FreemodeAmbientMissions";
        public static readonly String FreemodeMatchesReport = "FreemodeMatches";
        public static readonly String FreemodeRacesReport = "FreemodeRaces";
        public static readonly String KillDeathReport = "KillDeath";
        public static readonly String MissionsAttemptsReport = "MissionAttempts";
        public static readonly String MissionDeathsReport = "MissionDeaths";
        public static readonly String MPAverageBetCountReport = "MPAverageBets";
        public static readonly String MPBettingReport = "MPBetting";
        public static readonly String MPDeathmatchInjuryReport = "MPDeathmatchInjury";
        public static readonly String MPEarningsReport = "MPEarnings";
        public static readonly String MPExpenditureReport = "MPExpenditure";
        public static readonly String MPHourlyCashReport = "MPHourlyCash";
        public static readonly String MPInvitesReport = "MPInvitesReport";
        public static readonly String MPTopRankedGamersReport = "MPTopRankedGamers";
        public static readonly String MPTransactionHistoryReport = "MPTransactionHistory";
        public static readonly String MPTutorialReport = "MPTutorial";
        public static readonly String OnlineGamerCountReport = "OnlineGamerCount";
        public static readonly String PerRankReport = "PerRankInfo";
        public static readonly String RadioStationListenerReport = "RadioStationListener";
        public static readonly String ShopPurchasesReport = "ShopPurchases";
        public static readonly String SPCheaterReport = "SPCheaters";
        public static readonly String VideoViewerReport = "Videos";
        public static readonly String VehicleDistanceDrivenReport = "VehicleDistanceDriven";
        public static readonly String WantedLevelReport = "WantedLevels";
        public static readonly String WeaponModPurchaseReport = "WeaponModPuchases";
        public static readonly String WebsiteVisitsReport = "WebsiteVisits";

        // Telemetry.PlayerStats (Per User)
        public static readonly String PerUserClothChangesReport = "PerUserClothChanges";
        public static readonly String PerUserFriendActivitiesReport = "PerUserFriendActivities";
        public static readonly String PerUserMiniGameMedalsReport = "PerUserMiniGameMedals";
        public static readonly String PerUserMissionAttemptReport = "PerUserMissionAttempt";
        public static readonly String PerUserMissionCashReport = "PerUserMissionCash";
        public static readonly String PerUserMPHourlyCashReport = "PerUserMPHourlyCash";
        public static readonly String PerUserOddJobReport = "PerUserOddJobs";
        public static readonly String PerUserPropChangesReport = "PerUserPropChanges";
        public static readonly String PerUserSPCheatsReport = "PerUserSPCheats";
        public static readonly String PerUserWantedLevelsReport = "PerUserWantedLevels";
        public static readonly String PerUserWebsiteVisitsReport = "PerUserWebsiteVisits";

        // UGC
        public static readonly String MissionContentSummaryReport = "MissionContentSummary";
        public static readonly String MissionContentDetailsReport = "MissionContentDetails";
    } // Reports
}
