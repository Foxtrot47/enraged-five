﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.ROS;

namespace RSG.Statistics.Common.Vertica.Dto.ReportParams
{
    /// <summary>
    /// Parameter transfer object for communication between stats/vertica servers.
    /// </summary>
    [DataContract]
    [ReportType]
    public class MPUserHourlyParams
    {
        [DataMember]
        public uint AccountId { get; set; }

        [DataMember]
        public ROSPlatform Platform { get; set; }

        [DataMember]
        public uint CharacterSlot { get; set; }

        [DataMember]
        public uint CharacterId { get; set; }

        [DataMember]
        public IList<uint> Builds { get; set; }
    } // MPUserHourlyParams
}
