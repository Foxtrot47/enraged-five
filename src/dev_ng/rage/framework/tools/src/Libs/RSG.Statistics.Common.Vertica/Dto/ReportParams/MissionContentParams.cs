﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;
using RSG.ROS;

namespace RSG.Statistics.Common.Vertica.Dto.ReportParams
{
    /// <summary>
    /// Parameter transfer object for communication between stats/vertica servers.
    /// </summary>
    [DataContract]
    [ReportType]
    public class MissionContentParams
    {
        [DataMember]
        public IList<FreemodeMissionCategory> MissionCategories { get; set; }

        [DataMember]
        public DateTime? MissionCreatedStart { get; set; }

        [DataMember]
        public DateTime? MissionCreatedEnd { get; set; }

        [DataMember]
        public IList<Tuple<String, ROSPlatform>> MissionCreatorUserIdPlatformPairs { get; set; }

        [DataMember]
        public bool? MissionPublishedOnly { get; set; }

        [DataMember]
        public bool? LatestVersionsOnly { get; set; }

        [DataMember]
        public bool? IncludeDeleted { get; set; }

        [DataMember]
        public IDictionary<int, String> RadioStationLookup { get; set; }
    } // MissionContentParams
}
