﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.ROS;
using RSG.Model.Common.Report;
namespace RSG.Statistics.Common.Vertica.Dto.ReportParams
{
    /// <summary>
    /// Parameter transfer object for communication between stats/vertica servers.
    /// </summary>
    [DataContract]
    [ReportType]
    public class CutscenePerformanceParams : PerformanceParams
    {
        [DataMember]
        public uint LowerPercentileClamp { get; set; }

        [DataMember]
        public uint UpperPercentileClamp { get; set; }

        [DataMember]
        public uint StartHour { get; set; }

        [DataMember]
        public uint EndHour { get; set; }
    } // CutscenePerformanceParams
}
