﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.ROS;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Vertica.Dto.ReportParams
{
    /// <summary>
    /// Parameter transfer object for communication between stats/vertica servers.
    /// </summary>
    [DataContract]
    [ReportType]
    public class FpsPerformanceParams : PerformanceParams
    {
        [DataMember]
        public uint Level { get; set; }

        [DataMember]
        public uint Resolution { get; set; }

        [DataMember]
        public GameType GameType { get; set; }

        [DataMember]
        public uint? AutomatedTestNumber { get; set; }

        [DataMember]
        public bool OffMissionAndCutsceneOnly { get; set; }
    } // FpsPerformanceQuery

    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [ReportType]
    public class FpsDrawListPerformanceParams : FpsPerformanceParams
    {
        [DataMember]
        public uint DrawList { get; set; }
    } // FpsDrawListPerformanceParams
}
