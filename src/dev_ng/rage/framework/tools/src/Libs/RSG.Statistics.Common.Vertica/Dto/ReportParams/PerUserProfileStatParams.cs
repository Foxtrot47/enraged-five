﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Vertica.Dto.ReportParams
{
    /// <summary>
    /// 
    /// </summary>
    [DataContract]
    [ReportType]
    public class PerUserProfileStatParams : PerUserParams
    {
        [DataMember]
        public IList<Tuple<String, String>> StatNames { get; set; }
    } // PerUserProfileStatParams
}
