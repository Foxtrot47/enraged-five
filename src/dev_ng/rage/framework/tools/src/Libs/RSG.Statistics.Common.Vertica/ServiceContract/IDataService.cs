﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ServiceModel.Web;
using RSG.Statistics.Common.Vertica.Dto;
using RSG.ROS;
using RSG.Statistics.Common.Model.VerticaData;
using RSG.Statistics.Common.Model.SocialClub;

namespace RSG.Statistics.Common.Vertica.ServiceContract
{
    /// <summary>
    /// Service for retrieving various bits of data from Vertica.
    /// </summary>
    [ServiceContract]
    public interface IDataService
    {
        #region Builds
        /// <summary>
        /// Returns feature information about all the builds.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "Builds/Features")]
        List<BuildFeatures> GetAllBuildFeatures();
        #endregion // Builds

        #region Clans
        /// <summary>
        /// Searches for a clan based on a partial name.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "Clans/Search?name={searchName}")]
        List<Clan> SearchForClan(String searchName);

        /// <summary>
        /// Returns the list of gamers that are members of a particular clan.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "Clans/{id}/Members")]
        List<Gamer> GetClanMembers(String id);
        #endregion // Clans

        #region Gamers
        /// <summary>
        /// Searches for a gamer with a particular name and optionally restricting the search to a particular platform.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GamerSearch?name={searchName}")]
        List<Gamer> SearchForGamer(String searchName, IList<ROSPlatform> platforms);

        /// <summary>
        /// Checks that the list of gamers passed in exist in the Vertica DB.  
        /// </summary>
        /// <returns>List of valid gamers.</returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GamerValidation")]
        List<Gamer> ValidateGamerList(List<Gamer> gamers);
        #endregion // Gamers

        #region Social Club
        /// <summary>
        /// Searches for a social club user with a particular name (optionally restricted to a particular match type).
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "UserSearch?name={searchName}")]
        List<String> SearchForUser(String searchName);

        /// <summary>
        /// Checks that the list of gamers passed in exist in the Vertica DB.  
        /// </summary>
        /// <returns>List of valid users.</returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UserValidation")]
        List<String> ValidateUserList(List<String> users);

        /// <summary>
        /// Searches for a UGC mission with a particular name.
        /// </summary>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "MissionSearch?name={searchName}")]
        List<UGCMission> SearchForMission(String searchName, MissionSearchFilters filters);

        /// <summary>
        /// Retrieves the list of countries.
        /// </summary>
        [OperationContract]
        [WebGet(UriTemplate = "Countries")]
        List<Country> GetCountries();
        #endregion // Social Club
    } // IBuildService
}
