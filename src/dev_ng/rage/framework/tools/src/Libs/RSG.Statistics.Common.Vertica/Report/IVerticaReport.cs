﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Vertica.Report
{
    /// <summary>
    /// Report data that hits the vertica database for 
    /// </summary>
    public interface IVerticaReport : IReport
    {
    } // IVerticaReport
}
