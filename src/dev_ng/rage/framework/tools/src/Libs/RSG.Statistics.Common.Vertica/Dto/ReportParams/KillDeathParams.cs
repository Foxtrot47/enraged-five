﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using RSG.Model.Common.Report;

namespace RSG.Statistics.Common.Vertica.Dto.ReportParams
{
    /// <summary>
    /// 
    /// </summary>
    public enum KillDeathMode
    {
        Kills,
        Deaths
    }

    /// <summary>
    /// Parameter transfer object for communication between stats/vertica servers.
    /// </summary>
    [DataContract]
    [ReportType]
    public class KillDeathParams : TelemetryParams
    {
        [DataMember]
        public KillDeathMode KillsOrDeaths { get; set; }

        [DataMember]
        public IList<MatchType> MatchTypeRestrictions { get; set; }

        [DataMember]
        public bool FreeroamOnly { get; set; }
    } // KillDeathParams
}
