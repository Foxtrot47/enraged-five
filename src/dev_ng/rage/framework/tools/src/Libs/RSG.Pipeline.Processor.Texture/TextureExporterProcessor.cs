﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using SIO = System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Extensions;
using XGE = RSG.Interop.Incredibuild.XGE;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Attributes;
using RSG.Pipeline.Content;
using RSG.Pipeline.Services;
using Mats = RSG.Pipeline.Services.Materials;
using RSG.Platform;
using RSG.SceneXml;
using RSG.SceneXml.Material;

namespace RSG.Pipeline.Processor.Texture
{

    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IProcessor))]
    [ProcessorFlags(ProcessorFlags.Compatibility)]
    class TextureExporterProcessor : 
        ProcessorBase,
        IProcessor
    {
        #region Constants
        /// <summary>
        /// Processor description string.
        /// </summary>
        private static readonly String DESCRIPTION = "Texture Export Processor";

        /// <summary>
        /// Processor log context.
        /// </summary>
        private static readonly String LOG_CTX = "Texture Export";

        /// <summary>
        /// Texture export output format.
        /// </summary>
        private static readonly String PARAM_OUTPUT_FORMAT = "Output Format";

        /// <summary>
        /// Texture type (set on IContentNode's).
        /// </summary>
        private static readonly String PARAM_TEXTURE_TYPE = "Texture Type";

        /// <summary>
        /// Default texture export output format.
        /// </summary>
        private static readonly String DEFAULT_OUTPUT_FORMAT = ".tif";

        /// <summary>
        /// Split characters for source texture filenames.
        /// </summary>
        private static readonly Char[] SOURCE_SPLIT_CHARS = new Char[] { '+', '>' };
        #endregion // Constants
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public TextureExporterProcessor()
            : base(DESCRIPTION)
        {
        }
        #endregion // Constructor(s)
        
        #region IProcessor Interface Methods
        /// <summary>
        /// Prebuild stage; passing back enumerable of dependencies for the
        /// specific input data.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="process"></param>
        /// <param name="processors"></param>
        /// <param name="owner"></param>
        /// <param name="resultantProcesses"></param>
        /// <returns></returns>
        public override bool Prebuild(IEngineParameters param,
            IProcess process, IProcessorCollection processors,
            IContentTree owner, out IEnumerable<IContentNode> syncDependencies, 
            out IEnumerable<IProcess> resultantProcesses)
        {
            ICollection<IProcess> processes = new List<IProcess>();
            ICollection<IContentNode> syncDeps = new List<IContentNode>();
            this.LoadParameters(param);

            Debug.Assert(1 == process.Outputs.Count(), "Expected single output for texture export.");
            if (1 != process.Outputs.Count())
            {
                param.Log.ErrorCtx(LOG_CTX, "Expected single output for texture export.");
                resultantProcesses = processes;
                syncDependencies = syncDeps;
                return (false);
            }
            Debug.Assert(process.Outputs.First() is Directory, 
                String.Format("Expected single output directory for texture export ({0}).", process.Outputs.First().GetType()));
            if (!(process.Outputs.First() is Directory))
            {
                param.Log.ErrorCtx(LOG_CTX, "Expected single output directory for texture export ({0}).",
                    process.Outputs.First().GetType());
                resultantProcesses = processes;
                syncDependencies = syncDeps;
                return (false);
            }

            // Loop through our SceneXml inputs; collating all unique source textures.
            bool result = true;
            String outputDirectory = ((Directory)process.Outputs.First()).AbsolutePath;
            String outputFormat = this.Parameters.ContainsKey(PARAM_OUTPUT_FORMAT) ? 
                (String)this.Parameters[PARAM_OUTPUT_FORMAT] : DEFAULT_OUTPUT_FORMAT;
            IDictionary<String, Mats.ShaderPreset> presets = Mats.ShaderPreset.Create(param.CoreBranch);

            // Set of unique source textures (encoded alpha, +)
            ISet<Mats.Texture> inputTextures = new HashSet<Mats.Texture>(new Mats.SameExportTexture());
            foreach (IContentNode input in process.Inputs)
            {
                Debug.Assert(input is File, 
                    String.Format("Unsupported input file for Texture Export.  Expected SceneXml asset ({0}).",
                    input.GetType()));
                File inputFile = (File)input;

                SceneType sceneType = Scene.GetSceneType(input);
                Scene scene = new Scene(sceneType, inputFile.AbsolutePath, LoadOptions.Materials, true); 
                foreach (MaterialDef material in scene.Materials)
                {
                    IEnumerable<Mats.Texture> textures = 
                        Mats.Texture.Create(presets, material, outputDirectory, outputFormat);
                    inputTextures.AddRange(textures);
                }
            }

            // Loop through all unique source textures creating export processes.
            ISet<String> missingSourceTextures = new HashSet<String>();
            ISet<String> corruptExportTextures = new HashSet<String>(); // TEMP
            param.Log.MessageCtx(LOG_CTX, "Creating {0} texture export processes.", inputTextures.Count()); 
            foreach (Mats.Texture texture in inputTextures)
            {
                IEnumerable<IContentNode> sourceTextures = texture.SourceTextures.Select(t =>
                    owner.CreateFile(t));
                
                IContentNode exportTexture = owner.CreateFile(texture.ExportTexture);
                if (SIO.Path.GetExtension(texture.ExportTexture) == ".dds")
                    continue; // Skipped.
                if (SIO.Path.GetFileNameWithoutExtension(texture.ExportTexture).EndsWith("_pal"))
                    continue; // Skipped.

                bool exportTextureCorrupt = false;
                foreach (IContentNode source in sourceTextures)
                {
                    if (!SIO.File.Exists(((File)source).AbsolutePath))
                    {
                        exportTextureCorrupt = true;
                        missingSourceTextures.Add(((File)source).AbsolutePath);
                    }
                }
                if (exportTextureCorrupt && SIO.File.Exists(((File)exportTexture).AbsolutePath))
                {
                    corruptExportTextures.Add(((File)exportTexture).AbsolutePath);
                }

                // Create the process.
                ProcessBuilder textureExportPb = new ProcessBuilder(this, owner);
                textureExportPb.Inputs.AddRange(sourceTextures);
                textureExportPb.Outputs.Add(exportTexture);
                IProcess textureExportProcess = textureExportPb.ToProcess();
                textureExportProcess.State = ProcessState.Prebuilt;
                processes.Add(textureExportProcess);

                // Add the texture sources and export texture to our sync dependencies.
                syncDeps.AddRange(sourceTextures);
                syncDeps.Add(exportTexture);
            }

            foreach (String source in missingSourceTextures)
            {
                param.Log.ErrorCtx(LOG_CTX, "Source texture: {0} doesn't exist.", source);
                result = false;
            }
            foreach (String export in corruptExportTextures)
            {
                param.Log.ErrorCtx(LOG_CTX, "Export texture: {0} may be corrupt.", export);
                result = false;
            }

            process.State = ProcessState.Discard;
            resultantProcesses = processes;
            syncDependencies = syncDeps;
            return (result);
        }
        
        /// <summary>
        /// Prepare a build for the processes; populating the XGE project with
        /// the tools, environments and tasks required.  Including setting up
        /// the task dependency chain.
        /// </summary>
        /// <param name="param">Engine build parameters.</param>
        /// <param name="processes"></param>
        /// <param name="tools"></param>
        /// <param name="tasks"></param>
        /// <returns></returns>
        public override bool Prepare(IEngineParameters param, IEnumerable<IProcess> processes,
            out IEnumerable<XGE.ITool> tools, out IEnumerable<XGE.ITask> tasks)
        {
            bool result = true;
            XGE.ITool tool = XGEFactory.GetTextureTool(param.Log, param.Branch.Project.Config,
                "Texture Export", XGEFactory.TextureToolType.TextureExport);
            ICollection<XGE.ITool> textureTools = new List<XGE.ITool>();
            textureTools.Add(tool);

            int exportJobCounter = 0;
            ICollection<XGE.ITask> textureTasks = new List<XGE.ITask>();
            foreach (IProcess process in processes)
            {
                IEnumerable<File> fileInputs = process.Inputs.OfType<File>();
                String inputTexture = String.Empty; 
                if (fileInputs.Count() > 1)
                    inputTexture = String.Join("+", fileInputs.Select(f => f.AbsolutePath).ToArray());
                inputTexture = fileInputs.First().AbsolutePath;

                String outputTexture = ((File)process.Outputs.First()).AbsolutePath;
                String caption = String.Format("Texture Export {0}", exportJobCounter++);
                String name = caption.Replace(' ', '_');
                StringBuilder paramSb = new StringBuilder();
                if (process.Rebuild == RebuildType.Yes || param.Flags.HasFlag(EngineFlags.Rebuild))
                    paramSb.Append("-force ");
                paramSb.AppendFormat("-input \"{0}\" ", inputTexture);
                paramSb.AppendFormat("-output \"{0}\" ", outputTexture);

                XGE.TaskJob task = new XGE.TaskJob(name);
                task.Parameters = paramSb.ToString();
                task.SourceFile = inputTexture;
                task.OutputFiles.Add(outputTexture);
                task.Caption = caption;
                task.Tool = tool;

                textureTasks.Add(task);

                process.Parameters.Add(Constants.ProcessXGE_Task, task);
                process.Parameters.Add(Constants.ProcessXGE_TaskName, name);
            }

            // Assign method output.
            tools = textureTools;
            tasks = textureTasks;

            return (result);
        }
        #endregion // IProcessor Interface Methods
    }

} // RSG.Pipeline.Processor.Texture namespace
