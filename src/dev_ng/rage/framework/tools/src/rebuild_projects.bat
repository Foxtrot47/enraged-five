@echo off
pushd %~dp0

call setenv > NUL

set build_script=%RS_TOOLSROOT%\script\util\projgen\rebuildCsharpProject.bat
set cli=%RAGE_DIR%\framework\tools\src\cli
set libs=%RAGE_DIR%\framework\tools\src\libs
set projgen_config=%RS_TOOLSCONFIG%\projgen

@echo ====================================================================================== 
@echo ============================ PROJECT GENERATOR 3 STARTED =============================
@echo = https://devstar.rockstargames.com/wiki/index.php/Dev:Project_builder3#C.23_support = 
set started=%time% 
@echo STARTED : %started%
echo.

echo ********
echo * SYNC *
echo ********
echo.
call %RS_TOOLSROOT%\script\util\projGen\sync.bat
echo.

echo **************************************
echo * BUILDING PIPELINE LIBRARY PROJECTS *
echo **************************************
call %build_script% 	%projgen_config%/RSG.Pipeline.Library.build ^
			%libs%/RSG.Pipeline.Engine/RSG.Pipeline.Engine.makefile ^
			%libs%/RSG.Pipeline.Processor.Animation.Common/RSG.Pipeline.Processor.Animation.Common.makefile ^
			%libs%/RSG.Pipeline.Processor.Animation.Cutscene/RSG.Pipeline.Processor.Animation.Cutscene.makefile ^
			%libs%/RSG.Pipeline.Processor.Animation.InGame/RSG.Pipeline.Processor.Animation.InGame.makefile ^
			%libs%/RSG.Pipeline.Processor.Animation.Networks/RSG.Pipeline.Processor.Animation.Networks.makefile ^
			%libs%/RSG.Pipeline.Processor.Character/RSG.Pipeline.Processor.Character.makefile ^
			%libs%/RSG.Pipeline.Processor.Common/RSG.Pipeline.Processor.Common.makefile ^
			%libs%/RSG.Pipeline.Processor.Map/RSG.Pipeline.Processor.Map.makefile ^
			%libs%/RSG.Pipeline.Processor.Map.InstancePlacement/RSG.Pipeline.Processor.Map.InstancePlacement.makefile ^
			%libs%/RSG.Pipeline.Processor.Map2/RSG.Pipeline.Processor.Map2.makefile ^
			%libs%/RSG.Pipeline.Processor.Material/RSG.Pipeline.Processor.Material.makefile ^
			%libs%/RSG.Pipeline.Processor.Platform/RSG.Pipeline.Processor.Platform.makefile ^
			%libs%/RSG.Pipeline.Processor.Texture/RSG.Pipeline.Processor.Texture.makefile ^
			%libs%/RSG.Pipeline.Processor.Vehicle/RSG.Pipeline.Processor.Vehicle.makefile

echo *****************************************
echo * BUILDING PIPELINE EXECUTABLE PROJECTS *
echo *****************************************
call %build_script% 	%projgen_config%/RSG.Pipeline.Executable.build ^
			%cli%/RSG.Pipeline.Animation.ClipDictionary/RSG.Pipeline.Animation.ClipDictionary.makefile ^
			%cli%/RSG.Pipeline.Animation.ClipGroup/RSG.Pipeline.Animation.ClipGroup.makefile ^
			%cli%/RSG.Pipeline.Animation.Coalesce/RSG.Pipeline.Animation.Coalesce.makefile ^
			%cli%/RSG.Pipeline.Animation.Compress/RSG.Pipeline.Animation.Compress.makefile ^
			%cli%/RSG.Pipeline.Animation.Cutscene.CutsceneConcatenation/RSG.Pipeline.Animation.Cutscene.CutsceneConcatenation.makefile ^
			%cli%/RSG.Pipeline.Animation.Cutscene.CutsceneConcatenationLighting/RSG.Pipeline.Animation.Cutscene.CutsceneConcatenationLighting.makefile ^
			%cli%/RSG.Pipeline.Animation.Cutscene.FinaliseCutfile/RSG.Pipeline.Animation.Cutscene.FinaliseCutfile.makefile ^
			%cli%/RSG.Pipeline.Animation.Cutscene.InjectDof/RSG.Pipeline.Animation.Cutscene.InjectDof.makefile ^
			%cli%/RSG.Pipeline.Animation.Cutscene.MergeExternal/RSG.Pipeline.Animation.Cutscene.MergeExternal.makefile ^
			%cli%/RSG.Pipeline.Animation.Cutscene.MergeFacial/RSG.Pipeline.Animation.Cutscene.MergeFacial.makefile ^
			%cli%/RSG.Pipeline.Animation.Cutscene.MergeLights/RSG.Pipeline.Animation.Cutscene.MergeLights.makefile ^
			%cli%/RSG.Pipeline.Animation.Cutscene.MergeSubtitle/RSG.Pipeline.Animation.Cutscene.MergeSubtitle.makefile ^
			%cli%/RSG.Pipeline.Animation.Cutscene.PostProcess/RSG.Pipeline.Animation.Cutscene.PostProcess.makefile ^
			%cli%/RSG.Pipeline.Animation.Cutscene.Section/RSG.Pipeline.Animation.Cutscene.Section.makefile ^
			%cli%/RSG.Pipeline.Animation.Cutscene.SliceDice/RSG.Pipeline.Animation.Cutscene.SliceDice.makefile ^
			%cli%/RSG.Pipeline.Animation.DictionaryMetadata/RSG.Pipeline.Animation.DictionaryMetadata.makefile ^
			%cli%/RSG.Pipeline.Animation.Postprocess/RSG.Pipeline.Animation.Postprocess.makefile ^
			%cli%/RSG.Pipeline.AssetPack/RSG.Pipeline.AssetPack.makefile ^
			%cli%/RSG.Pipeline.AssetProcessor/RSG.Pipeline.AssetProcessor.makefile ^
			%cli%/RSG.Pipeline.Automation.ClientWorker/RSG.Pipeline.Automation.ClientWorker.makefile ^
			%cli%/RSG.Pipeline.Automation.Console/RSG.Pipeline.Automation.Console.makefile ^
			%cli%/RSG.Pipeline.Automation.ServerHost/RSG.Pipeline.Automation.ServerHost.makefile ^
			%cli%/RSG.Pipeline.BoundsProcessor/RSG.Pipeline.BoundsProcessor.makefile ^
			%cli%/RSG.Pipeline.CollisionProcessor/RSG.Pipeline.CollisionProcessor.makefile ^
			%cli%/RSG.Pipeline.Convert/RSG.Pipeline.Convert.makefile ^
			%cli%/RSG.Pipeline.LeakTest/RSG.Pipeline.LeakTest.makefile ^
			%cli%/RSG.Pipeline.MapAssetCombine/RSG.Pipeline.MapAssetCombine.makefile ^
			%cli%/RSG.Pipeline.MapExportMetadata/RSG.Pipeline.MapExportMetadata.makefile ^
			%cli%/RSG.Pipeline.MaterialPresetConverter/RSG.Pipeline.MaterialPresetConverter.makefile ^
			%cli%/RSG.Pipeline.Rage.FragmentGenerator/RSG.Pipeline.Rage.FragmentGenerator.makefile ^
			%cli%/RSG.Pipeline.RpfCreate/RSG.Pipeline.RpfCreate.makefile ^
			%cli%/RSG.Pipeline.RpfManifest/RSG.Pipeline.RpfManifest.makefile ^
			%cli%/RSG.Pipeline.SoakTest/RSG.Pipeline.SoakTest.makefile ^
			%cli%/RSG.Pipeline.TextureExport/RSG.Pipeline.TextureExport.makefile ^
			%cli%/RSG.Pipeline.ZipCreate/RSG.Pipeline.ZipCreate.makefile ^
			%cli%/RSG.Pipeline.ZipExtract/RSG.Pipeline.ZipExtract.makefile ^
			%libs%/RSG.Pipeline.InstancePlacementProcessor/RSG.Pipeline.InstancePlacementProcessor.makefile						
:END
@echo STARTED  : %started% 
@echo FINISHED : %time% 
@echo.
@echo * Default changelist contains project generated files. *
@echo.
@echo ===================== PROJECT GENERATOR 3 COMPLETE ======================
@echo =========================================================================

pause
