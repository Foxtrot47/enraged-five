

class key_entry_class
{
public:
	UInt16 number_of_occurrences;
	UInt8 key[8];

	void uppercaserize(void);
};

class key_array_class
{
public:
	key_entry_class *pBuffer;
	Int32 count;
	UInt32 total_number_of_keys;

	key_array_class(UInt32 number_of_keys);
	~key_array_class(void);
	void add(UInt8 *key);
	bool should_this_key_be_ignored(UInt8 *key);
	void save(void);
	void sort(void);
	void uppercaserize(void);
};
