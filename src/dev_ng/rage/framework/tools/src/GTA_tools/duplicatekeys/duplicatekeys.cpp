
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dvant.h"
#include "terror.h"
#include "tfile.h"

#include "duplicatekeys.h"

key_array_class *pAllTextKeys;

char error_message[100];

Int32 wide_string_length(UInt16 *text)
{
	Int32 char_index;
	
	char_index = 0;
	
	while (text[char_index] != 0)
	{
		char_index++;
	}
	
	return char_index;
}

void wide_string_copy(UInt16 *dest_string, UInt16 *source_string)
{
	Int32 char_index;
	
	char_index = 0;
	
	if (source_string != NULL)
	{
		while (source_string[char_index] != 0)
		{
			dest_string[char_index] = source_string[char_index];
			char_index++;
		}
	}
	
	dest_string[char_index] = 0;
}


/****************************** keys ***************************/

key_array_class::key_array_class(UInt32 number_of_keys)
{
	count = 0;
	total_number_of_keys = number_of_keys;
	
	pBuffer = (key_entry_class *) new key_entry_class[number_of_keys];
	if (pBuffer == NULL) error (ERR_MEMORY);

}

key_array_class::~key_array_class(void)
{
	delete [] pBuffer;
}


static int compare(const void *elem1, const void *elem2)
{
	UInt8 str[20];
	int s = strcmp((char*) (((key_entry_class *)elem1)->key), (char*) (((key_entry_class *)elem2)->key));

	if (s==0) 
	{
		sprintf((char*)str,"key : %s\n", (char*) (((key_entry_class *)elem1)->key) );
		lprint((char*)str);
		error (ERR_DUPLICATE_KEYS);
	}	
	return s;
}


void ignore_mission_part_of_key(UInt8 *pFullKeyName, UInt8 *pNewKeyName)
{
	UInt32 loop;
	Bool8 bFinished;

	bFinished = false;
	loop = 0;
	while ( (loop < 8) && !bFinished)
	{
		if ( (pFullKeyName[loop] == ']') || (pFullKeyName[loop] == ':') )
		{
			bFinished = true;
		}
		else
		{
			pNewKeyName[loop] = pFullKeyName[loop];
			loop++;
		}
	}
	
	if ( (loop == 8) && !bFinished)
	{
		error(ERR_KEY_SIZE);
	}
	
	pNewKeyName[loop] = 0;
}


// add new key
void key_array_class::add(UInt8 *key)
{
	static UInt8 str[256];
	char error_string[128];
	UInt32 loop;
	Bool8 bFinished;
	static UInt8 internal_key[8];

	ignore_mission_part_of_key(key, internal_key);
	
	Int32 s = strlen((char*)internal_key);

	if (count >= total_number_of_keys) error (ERR_KEY_COUNT);
	
	bFinished = false;
	loop = 0;
	while ( (loop < count) && !bFinished)
	{
		if (strcmp( ((char*)internal_key), ((char*)pBuffer[loop].key) ) == 0)
		{
			sprintf(error_string,"Duplicate key : %s\n", ((char*) internal_key) );
			lprint((char*)error_string);
			bFinished = true;
			pBuffer[loop].number_of_occurrences++;
		}
		loop++;
	}

	if (!bFinished)
	{	
		pBuffer[count].number_of_occurrences = 1;

		strcpy((char*)pBuffer[count].key, (char*)internal_key);
		if (s!=7)
			strnset((char*)(pBuffer[count].key+s+1), '\0', 7-s);
		count++;
	}
}

bool key_array_class::should_this_key_be_ignored(UInt8 *key)
{
	char error_string[128];
	UInt32 loop;
//	Bool8 bFinished;
	static UInt8 internal_key[8];
	
	ignore_mission_part_of_key(key, internal_key);
	
	Int32 s = strlen((char*)internal_key);

	for (loop = 0; loop < count; loop++)
	{
		if (strcmp( ((char *)internal_key), ((char *)pBuffer[loop].key) ) == 0)
		{
			if (pBuffer[loop].number_of_occurrences == 1)
			{
				return false;
			}
			else
			{
				pBuffer[loop].number_of_occurrences--;
				return true;
			}
		}
	}
	
	sprintf(error_string,"Couldn't find key on second pass - does it have any text? : %s\n", ((char*) internal_key) );
	lprint((char*)error_string);
	return false;
}

// sort the keys !
void key_array_class::sort(void)
{
	qsort(pBuffer, count, sizeof(key_entry_class), compare);
}

// turn the keys into upper case
void key_array_class::uppercaserize(void)
{
	Int16	C;
	for ( C = 0; C < count; C++)
	{
		pBuffer[C].uppercaserize();
	}
}

// turn the keys into upper case
void key_entry_class::uppercaserize(void)
{
	Int16	C;
	for ( C = 0; C < 8; C++)
	{
		if (key[C] >= 'a' && key[C] <= 'z')
		{
			key[C] += 'A' - 'a';
		}
	}
}


/*************************** end of keys *********************/

Bool8 white_space(UInt16 c)
{
        return ( (c==' ') || (c=='\t') || (c=='\x0A') || (c=='\x0D') );
}


void do_convert_asc ( UInt8 *in, Int32 inlen, UInt8 *out, Int32 *outlen, bool bWriteToBuffer)
{
    Int32 i, roll_back_to_here, j=0;
    enum {WHITE_SPACE, HEADER, TEXT, WHITE_SPACE_INTERNAL, FIRST_WHITE_SPACE, COMMENT_WHITE_SPACE, COMMENT_FIRST_WHITE_SPACE, COMMENT_WHITE_SPACE_INTERNAL, COMMENT_TEXT} state = FIRST_WHITE_SPACE;
	UInt8 *start_key = NULL;
	Bool8 bDataStarted = false;
	Int32 nest = 0;
	bool bCopyToOut = true;

    for (i=0; i<inlen; i++)
    {
        switch (state)
        {
			case COMMENT_FIRST_WHITE_SPACE:
				if ( in[i]=='{' )
				{
					error_str (ERR_NESTED_COMMENTS, (char*)start_key);
				}
				else if ( in[i]=='}' )
				{
					if (nest==0)
						state = FIRST_WHITE_SPACE;
					else
						nest--;
				}
				break;

			case COMMENT_WHITE_SPACE:
				if ( in[i]=='{' )
				{
					error_str (ERR_NESTED_COMMENTS, (char*)start_key);
				}
				else if ( in[i]=='}' )
				{
					if (nest==0)
						state = WHITE_SPACE;
					else
						nest--;
				}
				break;

			case COMMENT_WHITE_SPACE_INTERNAL:
				if ( in[i]=='{' )
				{
					error_str (ERR_NESTED_COMMENTS, (char*)start_key);
				}
				else if ( in[i]=='}' )
				{
					if (nest==0)
						state = WHITE_SPACE_INTERNAL;
					else
						nest--;
				}
				break;

			case COMMENT_TEXT:
				if ( in[i]=='{' )
				{
					error_str (ERR_NESTED_COMMENTS, (char*)start_key);
				}
				else if ( in[i]=='}' )
				{
					if (nest==0)
					{
						state = TEXT;
					}
					else
						nest--;
				}
				break;

            case FIRST_WHITE_SPACE:
                if ( in[i]=='[' )
                {
                    state = HEADER;
					start_key = &in[i+1];
					roll_back_to_here = j;
					bCopyToOut = true;
                }
				else if ( in[i]=='{')
					state = COMMENT_FIRST_WHITE_SPACE;
				else if ( in[i]=='}' )
				{
					error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
				}
                break;
            
            case WHITE_SPACE:
                if ( in[i]=='[' )
                {
					if (bDataStarted)
					{
						if (!bWriteToBuffer)
						{
							pAllTextKeys->add(start_key);
						}
						bDataStarted = false;
					}

                    state = HEADER;
					start_key = &in[i+1];
					roll_back_to_here = j;
					bCopyToOut = true;
                }
				else if ( in[i]=='{')
					state = COMMENT_WHITE_SPACE;
				else if ( in[i]=='}' )
				{
					error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
				}
                else if (!white_space ( in[i] ))
                {
					bDataStarted = true;
                    state = TEXT;
                }
                break;
            
            case WHITE_SPACE_INTERNAL:
                if ( in[i]=='[' )
                {
					if (bDataStarted)
					{
						if (!bWriteToBuffer)
						{
							pAllTextKeys->add(start_key);
						}
						bDataStarted = false;
					}

                    state = HEADER;
					start_key = &in[i+1];
					roll_back_to_here = j;
					bCopyToOut = true;
                }
				else if ( in[i]=='{')
					state = COMMENT_WHITE_SPACE_INTERNAL;
				else if ( in[i]=='}' )
				{
					error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
				}
                else if (!white_space ( in[i] ))
                {
					state = TEXT;
                }
                break;
            
            case HEADER:
                if ( in[i]==']' )
                {
//					in[i] = '\0';
                    state = WHITE_SPACE;
                    if (bWriteToBuffer)
                    {
	                    if (pAllTextKeys->should_this_key_be_ignored(start_key))	//	remember start_key is terminated with ']'
	                    {
	                    	j = roll_back_to_here;
	                    	bCopyToOut = false;
	                    }
	                }
                }
				else if ( in[i]=='}' )
				{
					error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
				}
                break;
                    
            case TEXT:
                if (white_space(in[i]))
				{
                    state = WHITE_SPACE_INTERNAL;
				}
				else if ( in[i]=='{')
				{
					state = COMMENT_TEXT;
				}
				else if ( in[i]=='}' )
				{
					error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
				}
                else
                {
                    if ( in[i]=='[' )
						error_str(ERR_NO_WHITESPACE_BEFORE_HEADER, (char*)start_key);
                }
                break;                                                                                          

            default:
				break;
        }
        
		if (bCopyToOut && bWriteToBuffer)
		{
			out[j++] = in[i];
		}
    }
    if ( state == HEADER ) 
		error_str (ERR_BRACKET, (char*)start_key);
	else if ( ( state == COMMENT_FIRST_WHITE_SPACE ) ||
			  ( state == COMMENT_WHITE_SPACE ) ||
			  ( state == COMMENT_WHITE_SPACE_INTERNAL ) ||
			  ( state == COMMENT_TEXT ) )
		error_str (ERR_COMMENT_BRACKET, (char*)start_key);

    *outlen = j;
}


void main ( Int32 argc, Char *argv[] )
{
    UInt8 str[256];
    UInt8 *in;
	UInt8 *out;
    Int32 inlen, outlen;

    open_log();

	if (argc != 3)
	{
        lprint ("\nUsage : duplicatekeys filename newfilename\n");
        lprint ("\n Any duplicate keys are removed from the first file\n");
        lprint ("\n It is assumed that the last copy of the key in the file is the one to keep\n");
        close_log();
        exit (-1);
	}

	pAllTextKeys = new key_array_class(5000);

    load_file (argv[1], (Char**)&in, &inlen );

//	scan for duplicate keys
	sprintf((char*)str,"Checking %s for duplicate keys\n", argv[1]);

	do_convert_asc(in, inlen, NULL, &outlen, false);

//	write new text file
    sprintf ((char*)str,"Converting %s -> %s\n",argv[1],argv[2]);
    lprint ((char*)str);

	out = (UInt8*) malloc (inlen);
	if (out == NULL) error (ERR_MEMORY);

    do_convert_asc(in, inlen, out, &outlen, true);

	save_file((char*) out, outlen, argv[2]);

	delete pAllTextKeys;

    close_log();
    exit (0);
}
