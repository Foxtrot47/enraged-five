/************************************
 * DVant.h                          *
 *                           	    *
 * special DVant header file   	    *
 *                             	    *
 * KRH 22/08/95                	    *
 * update KRH 27/08/97              *
 * again KRH 23/09/97               *
 *                              	*
 * (c) 1995-1997 DMA Design Ltd   	*
 ************************************/

#ifndef DVANT_H
#define DVANT_H

#ifdef PSX_DMA
#include "shell.h"
#endif

#ifdef _DEMO
#ifndef _DEBUG
#define _DEMO_RELEASE
#endif
#endif

#ifdef WIN32

#ifndef PC_DMA
#define PC_DMA
#endif

#ifdef PC_DMA
#include <stdlib.h>
//for debug versions of malloc, free, new, delete and others
#ifndef MISS2_DMA

#define	_CRTDBG_MAP_ALLOC
//	#pragma	auto_inline( on )
#include <crtdbg.h>
//	#pragma	auto_inline( off )

#ifdef	_DEBUG
#undef new
#define new new(_NORMAL_BLOCK, __FILE__, __LINE__)
#endif //_DEBUG
#endif //MISS2_DMA

#endif

#pragma warning( disable : 4201 )
#pragma warning( disable : 4710 )

#ifdef _DEBUG
#define PC_DMA_DEBUG
#endif

#endif	//WIN32

#include "dma.h"

#ifdef USE_F32

#ifdef FIXED_POINT
#include "fixed.h"
#else
#include "gbhfloat.h"
#endif

#else

typedef float Float32;

#endif

#ifdef _DEBUG
inline void BREAK(void)
{
#ifdef PC_DMA
__asm{
	int 3
	}
#endif
}
#else
inline void BREAK(void){}
#endif


#ifdef _DEBUG
#define IF_DEBUG(x) x
#else
#define IF_DEBUG(x)
#endif

#endif
