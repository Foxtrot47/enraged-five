/**********************************
 * tfile.cpp                      *
 *                                *
 * file operations for dino tools *
 *                                *
 * KRH 26/06/95                   *
 *                                *
 * (c) 1995 DMA Design Ltd        *
 **********************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>
#include <winbase.h>

#include "dvant.h"
#include "terror.h"
#include "tfile.h"


/*
 * swap_word
 * ---------
 *
 * function		: swap a 16-bit word between big-endian and little-endian formats
 *
 */

UInt16 swap_word ( UInt16 word )
{
	return ( ((word&0x00FF)<<8) + ((word&0xFF00)>>8) );
}


/*
 * swap_dword
 * ---------
 *
 * function		: swap a 32-bit dword between big-endian and little-endian formats
 *
 */

UInt32 swap_dword ( UInt32 dword )
{
	return ( ((dword&0x000000FF)<<24) + ((dword&0x0000FF00)<<8) + ((dword&0x00FF0000)>>8)+ ((dword&0xFF000000)>>24) );
}


/*
 * lower_case
 * ----------
 *
 * function             : convert char to lower case
 * input                : char
 * return value : char
 *
 */

static Char lower_case ( Char c )
{
		if ( (c>='A') && (c<='Z') )
				return (Char) ((c-'A')+'a');
		else
				return  (Char) c;
}
/*
 * last_letter
 * -----------
 *
 * function             : get the last letter of the root part of a filename
 * input                : filename
 * return value : the letter
 *
 */

Char last_letter (Char *filename)
{
		Int32 i;

		for (i=0; (filename[i]!='\0') && (filename[i]!='.'); i++)
				;
		return (lower_case(filename[i-1]));
}


/*
 * change_last_letter
 * ------------------
 *
 * function             : change the last letter of the root part of a filename
 * input                : filename
 * return value : a copy of the filename with the last letter changed
 *
 */

Char *change_last_letter (Char *filename, Char last_lett)
{
		Int32 i;
		static Char dest[256];

		strcpy (dest, filename);

		for (i=0; (dest[i]!='\0') && (dest[i]!='.'); i++)
				;
		dest[i-1] = last_lett;
		return (dest);
}


/*
 * change_ext
 * ----------
 *
 * input        : string, new extension(including dot if required)
 * function     : change extension to new one
 * Return value : string with extension changed
 *              
 */

Char *change_ext ( Char *src, Char *ext )
{
	static Char dest[256];
	Int32 i,j;

	for ( i=0; (src[i]!='.') && (src[i]); i++)
		dest[i] = src[i];
	for ( j=0; ext[j]; j++)
		dest[i++] = ext[j];
	dest[i]='\0';

	return (dest);  
}


/*
 * file_size
 * ---------
 *
 * function     : get size of file ( exit if error )
 * input        : file pointer to open file
 * return value : file size in bytes
 *
 */
								
Int32 file_size(FILE *fp)
{
		Int32 s;
		Int32 save_pos, size_of_file;
		
		save_pos = ftell(fp);        
		if ( save_pos == -1L) error (ERR_FTELL);
		s = fseek(fp, 0L, SEEK_END);
		if ( s ) error (ERR_FSEEK);
		size_of_file = ftell(fp);
		if ( size_of_file == -1L) error (ERR_FTELL);
		s = fseek(fp, save_pos, SEEK_SET);
		if ( s ) error (ERR_FSEEK);
		return (size_of_file);
}


/*
 * file_exists
 * -----------
 *
 * input                : filename
 * return value : TRUE if file exists, else FALSE
 *
 */

Bool32 file_exists ( Char *filename )
{
		FILE *f;
		Int32 s;

		f = fopen ( filename, "rb" );
		if ( f==NULL) return (FALSE);
	s = fclose (f);
	if (s) error (ERR_CLOSE);
	return (TRUE);
}

/*
 * file_read
 * ---------
 *
 * function     : read data from file into buffer, with error check
 * input        : file, buffer, length
 * output       : data from file into buffer
 *
 */

void file_read ( Char *buffer, Int32 len, FILE *f )
{
		Int32 s;

		s = fread (buffer, len, 1, f);
	if ( s!= 1 ) error (ERR_READ); 
}

/*
 * file_len
 * --------
 *
 * function		: measure length of a file
 * input		: filename
 * return value	: file length in bytes
 *
 */

Int32 file_len ( Char *infile )
{
	FILE *f;
	Int32 len;
	Int32 s;
	Char str[80];

    f = fopen (infile, "rb");
	if ( f==NULL )
	{
		sprintf (str,"attempting to open %s\n", infile );
		lprint(str);
		error (ERR_OPEN_INPUT);
	}
   	len = file_size(f);
   	if (len<0) error (len);

   	s = fclose (f);
   	if (s) error (ERR_CLOSE);

	return (len);
}


/*
 * load_file
 * ---------
 *
 * function     : load a file
 * input        : filename
 * output       : buffer, size
 *
 */

void load_file ( Char *filename, Char **buffer, Int32 *size )
{
	FILE *f;
	Int32 s;
	static Char str[80];

	f = fopen ( filename, "rb" );
	if ( f==NULL ) 
	{	
		sprintf (str,"attempting to open %s\n", filename );
		lprint(str);
		error (ERR_OPEN_INPUT);
	}
	*size = file_size(f);
	*buffer = (Char*) malloc ( *size );
	if (*buffer == NULL) error (ERR_MEMORY);  

	file_read (*buffer, *size, f);

	s = fclose (f);
	if (s) error (ERR_CLOSE);
}


/*
 * load_file_f
 * -----------
 *
 * function     : load a file into pre-allocated buffer
 * input        : filename
 * output       : size
 *
 */

void load_file_f ( Char *filename, Char *buffer, Int32 *size )
{
	FILE *f;
	Int32 s;
	static Char str[128];

	sprintf (str,"loading file : %s\n", filename );
	lprint (str);

	f = fopen ( filename, "rb" );
	if ( f==NULL ) error (ERR_OPEN_INPUT);

	*size = file_size(f);
	file_read (buffer, *size, f);

	s = fclose (f);
	if (s) error (ERR_CLOSE);
}


/*
 * save_file
 * ---------
 *
 * function     : save a file
 * input        : filename, buffer, size
 *
 */

void save_file ( Char *buffer, Int32 size, Char *filename)
{
	FILE *f;
	Int32 s;
	static Char str[128];

	sprintf (str,"saving file : %s - %d bytes\n", filename, size );
	lprint (str);

	if ( size <= 0 ) error (ERR_FILE_LEN);
	f = fopen ( filename, "wb" );
	if ( f==NULL ) error (ERR_OPEN_OUTPUT);
	s = fwrite ( buffer, size, 1, f );
	if ( s != 1 ) error (ERR_WRITE);
	s = fclose (f);
	if (s) error (ERR_CLOSE);
}

FILE *fg;

void open_save_file ( Char *filename )
{
	static Char str[128];

	sprintf (str,"saving file : %s \n", filename );
	lprint (str);

	fg = fopen ( filename, "wb" );
	if ( fg==NULL ) error (ERR_OPEN_OUTPUT);
	
}

void write_file ( Char *buffer, Int32 size )
{
	Int32 s;

	if ( size <= 0 ) error (ERR_FILE_LEN);
	s = fwrite ( buffer, size, 1, fg );
	if ( s != 1 ) error (ERR_WRITE);
} 

void close_file ()
{
	Int32 s;

	s = fclose (fg);
	if (s) error (ERR_CLOSE);
}

FILE *fg2;

void open_save_file2 ( Char *filename )
{
	static Char str[128];

	sprintf (str,"saving file : %s \n", filename );
	lprint (str);

	fg2 = fopen ( filename, "wb" );
	if ( fg2==NULL ) error (ERR_OPEN_OUTPUT);
	
}

void write_file2 ( Char *buffer, Int32 size )
{
	Int32 s;

	if ( size <= 0 ) error (ERR_FILE_LEN);
	s = fwrite ( buffer, size, 1, fg2 );
	if ( s != 1 ) error (ERR_WRITE);
} 

void close_file2 ()
{
	Int32 s;

	s = fclose (fg2);
	if (s) error (ERR_CLOSE);
}

void open_load_file ( Char *filename )
{
	static Char str[128];

	sprintf (str,"opening file : %s \n", filename );
	lprint (str);

	fg = fopen ( filename, "rb" );
	if ( fg==NULL ) error (ERR_OPEN_INPUT);
	
}

/*
 * read_file
 * ---------
 *
 * input	: buffer (pre-allocated), size to read
 * function	: read size bytes from fg file into buffer
 * return value : TRUE if OK, FALSE if not
 *
 */

Bool8 read_file (Char *buffer, Int32 size)
{
	Int32 s;

	s = fread (buffer, size, 1, fg);
	if ( s!= 1 ) return (FALSE);
	return (TRUE);
}


Bool8 read_file_a (Char **buffer, Int32 size)
{
	*buffer = (Char*) malloc (size);
	if (*buffer==NULL) error (ERR_MEMORY);
	return ( read_file(*buffer, size));
}


/*
 * skip_file
 * ---------
 *
 * input	: len bytes
 * function	: skip len bytes in fg file
 *
 */

void skip_file (Int32 len)
{
	Int32 s;

	s = fseek (fg, len, SEEK_CUR);
	if (s != 0) error (ERR_FSEEK);
}


/*
 * save_file_append
 * ----------------
 *
 * function     : save data on end of a file
 * input        : filename, buffer, size
 *
 */

void save_file_append ( Char *buffer, Int32 size, Char *filename)
{
	FILE *f;
	Int32 s;

	if ( size <= 0 ) error (ERR_FILE_LEN);
	f = fopen ( filename, "ab" );
	if ( f==NULL ) error (ERR_OPEN_OUTPUT);
	s = fwrite ( buffer, size, 1, f );
	if ( s != 1 ) error (ERR_WRITE);
	s = fclose (f);
	if (s) error (ERR_CLOSE);
}




/*
 * append_file
 * -----------
 *
 * function     : write data to an already open file
 * input        : file handle, buffer, size
 *
 */

void append_file ( FILE *f, Char *buffer, Int32 size )
{
		Int32 s;

		if ( size == 0 ) error (ERR_WRITE_ZERO);
	s = fwrite ( buffer, size, 1, f );
	if ( s != 1 ) error (ERR_WRITE);

 }



/*
 * copy_file
 * ---------
 *
 * function     : copy data from another file to an already open file
 * input        : file handle, other filename
 *
 */

void copy_file ( FILE *f, Char *filename )
{
		Char *buffer;
		Int32 buffer_size;

		load_file (filename, &buffer, &buffer_size);
		append_file (f, buffer, buffer_size);
}




/*
 * compare
 * -------
 *
 * function             : compare 2 strings ( for use by qsort only ) - ignoring case
 * input                : 2 strings
 * return value : -1,0,1 for less than, equal, greater than
 * note                 : uses non-standard types to keep Watcom happy
 *
 */

int compare( const void *op1, const void *op2 )
  {
	const char **p1 = (const char **) op1;
	const char **p2 = (const char **) op2;
	return( _stricmp( *p1, *p2 ) );
  }


/*
 * find_all_files
 * --------------
 *
 * function     : find all of the files in the current directory
 *            which match the given pattern
 *
 * input        : match - the filenames to look for
 * output       : list of pointers to filenames goes in file_list
 *
 * return value : no. files found
 *
 */

Int32 find_all_files (Char *file_list[], Char *match)
{
	Int32 file_list_size;
	WIN32_FIND_DATA fd;
	HANDLE fh;
	Char drive[_MAX_DRIVE];
	Char dir[_MAX_DIR];
	Char fname[_MAX_FNAME];
	Char ext[_MAX_EXT];
	Char drive1[_MAX_DRIVE];
	Char dir1[_MAX_DIR];
	Char fname1[_MAX_FNAME];
	Char ext1[_MAX_EXT];
	Char path[_MAX_PATH];
	

	_splitpath (match, drive, dir, fname, ext);
	file_list_size = 0;

	fh = FindFirstFile(match,&fd);
	if (fh==INVALID_HANDLE_VALUE) return (0);
	
	for(;;)
	{
		if (file_list_size==FILE_LIST_MAX) error (ERR_TOO_MANY_FILES);

		_splitpath(fd.cFileName, drive1, dir1, fname1, ext1);
		_makepath(path,drive,dir,fname1,ext1);

		file_list[file_list_size] = (Char*) malloc ( strlen(path)+1 );
		if (file_list[file_list_size]==NULL) error (ERR_MEMORY);
		strcpy (file_list[file_list_size], path);

		file_list_size++;
		if (!FindNextFile(fh,&fd))
			break;
	}
				
	FindClose(fh);

	qsort( file_list, file_list_size, sizeof(Char *), compare );

	return (file_list_size);
}


/*
 * Amiga4ToL
 * ---------
 *
 * function             : convert Amiga format 32-bit quantity to PC format
 * input                : Amiga number as 4-byte string
 * return value : PC number as unsigned int
 *
 */

static UInt32 Amiga4ToL(Char *charlen)
{
		UInt32 len;

		len= (UInt32)charlen[3];
		len+=(UInt32)(charlen[2]<<8);
		len+=(UInt32)(charlen[1]<<16);
		len+=(UInt32)(charlen[0]<<24);

		return len;

}


/*
 * Amiga2ToL
 * ---------
 *
 * function             : convert Amiga format 16-bit quantity to PC format
 * input                : Amiga number as 2-byte string
 * return value : PC number as unsigned int
 *
 */

static UInt32 Amiga2ToL(Char *charlen)
{
		UInt32 len;

		len=charlen[1]+(charlen[0]*256);

		return len;
}


/*
 * ReadLbmLine
 * -----------
 *
 * function             : read one line of lbm data
 * input                : file pointer width
 * output               : lbm data written uncompressed to LineBuffer
 *
 */

static void ReadLbmLine(FILE *f, Int32 LbmWidth, Char *LineBuffer)
{
	int LineCtr;
	int c;
	int n;
	int i;

	LineCtr = 0;

	while (LineCtr<LbmWidth && !feof(f)) 
	{
		c=getc(f);
		if (c<128)
		{
			for (i=0;i<c+1;i++) 
			{
				LineBuffer[LineCtr]= (Char) getc(f);
				LineCtr++;
			}
		}
		else 
		{
			c=257-c;
			n=getc(f);
			for (i=0;i<c;i++) 
			{
				LineBuffer[LineCtr]= (Char) n;
				LineCtr++;
			}
		}
	}
}


/*
 * read_lbm_header
 * ---------------
 *
 * function             : read lbm header
 * input                : file
 * output               : width, height, planes
 *
 */

static void read_lbm_header ( FILE *f, Int32 *lbm_width, Int32 *lbm_height, Int32 *lbm_planes )
{
		static Char str[128];
		Char BlckBuffer[5];
		Int32 LbmCompr;

		memset (BlckBuffer,0,5);
		file_read(BlckBuffer, 4, f);
		if (strcmp(BlckBuffer, "FORM")) error (ERR_LBM_HEADER);

		file_read(BlckBuffer, 4, f);    // LBM Len

		file_read(BlckBuffer, 4, f);    // PMB/ILBM
		if (strcmp(BlckBuffer, "PBM ")) error (ERR_NOT_DPE);

		file_read(BlckBuffer, 4, f);    // BMHD
		if (strcmp(BlckBuffer, "BMHD")) error (ERR_LBM_BITMAP);

		file_read(BlckBuffer, 4, f); // BMHD Length, usually 20 bytes
		if (Amiga4ToL(BlckBuffer)!=20) error (ERR_LBM_BITMAP_LENGTH);

		file_read(BlckBuffer, 2, f);
		*lbm_width=Amiga2ToL(BlckBuffer); // width

		file_read(BlckBuffer, 2, f);
		*lbm_height=Amiga2ToL(BlckBuffer);  // height

		file_read(BlckBuffer,4, f);  // xpos, ypos

		file_read(BlckBuffer, 2, f);    // Planes
		*lbm_planes=BlckBuffer[0];

		file_read(BlckBuffer, 2, f);    // Compression
		LbmCompr=BlckBuffer[0];
		if (LbmCompr != 1) error (ERR_LBM_COMPR);

		file_read(BlckBuffer, 8,f);     // To end of BMHD

		sprintf (str,"%d x %d x %d planes\n", *lbm_width, *lbm_height, *lbm_planes);
		lprint (str);
										  
}


static void skip_read ( Char *buffer, Char**skip_buffer, Int32 size )
{
	memcpy ( buffer, *skip_buffer, size );
	(*skip_buffer) += size;
}


/*
 * skip_lbm_header
 * ---------------
 *
 * function             : skip lbm header
 * input                : buffer pos
 * output               : new buffer pos, width, height, planes
 *
 */

static void skip_lbm_header ( Char **buffer, Int32 *lbm_width, Int32 *lbm_height, Int32 *lbm_planes )
{
		static Char str[128];
		Char BlckBuffer[5];
		Int32 LbmCompr;

		memset (BlckBuffer,0,5);

		skip_read (BlckBuffer, buffer, 4);

		if (strcmp(BlckBuffer, "FORM")) error (ERR_LBM_HEADER);

		skip_read(BlckBuffer, buffer, 4);    // LBM Len

		skip_read(BlckBuffer, buffer, 4);    // PMB/ILBM
		if (strcmp(BlckBuffer, "PBM ")) error (ERR_NOT_DPE);

		skip_read(BlckBuffer, buffer, 4);    // BMHD
		if (strcmp(BlckBuffer, "BMHD")) error (ERR_LBM_BITMAP);

		skip_read(BlckBuffer, buffer, 4); // BMHD Length, usually 20 bytes
		if (Amiga4ToL(BlckBuffer)!=20) error (ERR_LBM_BITMAP_LENGTH);

		skip_read(BlckBuffer, buffer, 2);
		*lbm_width=Amiga2ToL(BlckBuffer); // width

		skip_read(BlckBuffer, buffer, 2);
		*lbm_height=Amiga2ToL(BlckBuffer);  // height

		skip_read(BlckBuffer, buffer, 4);  // xpos, ypos

		skip_read(BlckBuffer, buffer, 2);    // Planes
		*lbm_planes=BlckBuffer[0];

		skip_read(BlckBuffer, buffer, 2);    // Compression
		LbmCompr=BlckBuffer[0];
		if (LbmCompr != 1) error (ERR_LBM_COMPR);

		skip_read(BlckBuffer, buffer, 8);     // To end of BMHD

		sprintf (str,"%d x %d x %d planes\n", *lbm_width, *lbm_height, *lbm_planes);
		lprint (str);
										  
}


/*
 * load_lbm_file
 * -------------
 *
 * function             : load an lbm file into an uncompressed buffer
 * input                : filename
 * output               : buffer position, width, height
 *
 */

void load_lbm_file ( Char *filename, Char **lbm_buffer, Int32 *lbm_width, Int32 *lbm_height )
{
		static Char BlckBuffer[5];
		static Char ReadBuffer[5];
		Int32 GenLength;
		Int32 i,s;
		Int32 lbm_planes;
		FILE *f;
		static Char str[128];

	memset (BlckBuffer,0,5);
	memset (ReadBuffer,0,5);

	sprintf (str, "loading lbm file : %s\n", filename );
		lprint (str);

	f = fopen ( filename, "rb" );
	if ( f==NULL )
	{
		error (ERR_OPEN_INPUT);
	}
	read_lbm_header ( f, lbm_width, lbm_height, &lbm_planes );

	*lbm_buffer = (Char*) malloc ( (*lbm_height) * (*lbm_width) );
	if ( (*lbm_buffer) == NULL) error (ERR_MEMORY);

	file_read(BlckBuffer, 4, f);

	while (strcmp(BlckBuffer, "BODY"))
	{
		file_read(ReadBuffer, 4, f);
		GenLength=Amiga4ToL(ReadBuffer);
		fseek(f, GenLength, SEEK_CUR);
		file_read(BlckBuffer, 4, f);
		if (*BlckBuffer=='\x0')    // Dangerous - assumes end of 'TINY' == 0
		{                                                  // For DPaint Enhanced
			fseek(f, -3, SEEK_CUR); // where only even lengths
			file_read(BlckBuffer, 4, f);    // correct although odd are
		}                                               // sometimes given.
	}

	file_read(ReadBuffer, 4, f);

	for (i=0; i< *lbm_height; i++)
	{
		ReadLbmLine (f, *lbm_width, (*lbm_buffer) + (i* (*lbm_width)));
	}

	s = fclose (f);
	if (s) error (ERR_CLOSE);
}


/*
 * load_lbm_pal
 * ------------
 *
 * function             : load the palette of an lbm file into an uncompressed buffer
 * input                : filename
 * output               : buffer position, size
 *
 */

void load_lbm_pal ( Char *filename, Char **pal_buffer, Int32 *pal_size )
{
		Char BlckBuffer[5];
		Char ReadBuffer[5]; 
		Bool32 pal_found = FALSE;
		Int32 GenLength,s;
		FILE *f;
		Int32 lbm_planes, lbm_width, lbm_height;
		Int32 i;
		static Char str[128];

	memset (BlckBuffer,0,5);
	memset (ReadBuffer,0,5);
	sprintf (str,"loading lbm file : %s\n", filename );
		lprint (str);

	f = fopen ( filename, "rb" );
	if ( f==NULL ) error (ERR_OPEN_INPUT);

	read_lbm_header ( f, &lbm_width, &lbm_height, &lbm_planes );

	file_read(BlckBuffer, 4, f);

	while (strcmp(BlckBuffer, "BODY") != 0)
	{
		if (strcmp(BlckBuffer, "CMAP") == 0)
		{
			file_read(ReadBuffer, 4, f);
			*pal_size = Amiga4ToL(ReadBuffer);
			*pal_buffer = (Char *) malloc ( *pal_size );
			if (*pal_buffer == NULL) error (ERR_MEMORY);                    
			file_read(*pal_buffer, *pal_size, f);
			for (i=0;i<*pal_size;i++)
				*((*pal_buffer)+i)>>=2;
			pal_found = TRUE;
			break;
		}
		else
		{
			file_read(ReadBuffer, 4, f);
			GenLength=Amiga4ToL(ReadBuffer);
			fseek(f, GenLength, SEEK_CUR);
		}
		file_read(BlckBuffer, 4, f);
		if (*BlckBuffer=='\x0')    // Dangerous - assumes end of 'TINY' == 0
		{                                                  // For DPaint Enhanced
			fseek(f, -3, SEEK_CUR); // where only even lengths
			file_read(BlckBuffer, 4, f);    // correct although odd are
		}                                               // sometimes given.
	}
	if ( !pal_found ) error (ERR_LBM_PAL);
	s = fclose (f);
	if (s) error (ERR_CLOSE);
}


/*
 * find_lbm_pal
 * ------------
 *
 * function             : find the palette of an lbm file ( no shifting )
 * input				: buffer
 * output               : position in old buffer, palette & palette size
 *
 */

void find_lbm_pal ( Char **buffer, Char **pal_buffer, Int32 *pal_size )
{
		Char BlckBuffer[5];
		Char ReadBuffer[5]; 
		Bool32 pal_found = FALSE;
		Int32 GenLength;
		Int32 lbm_planes, lbm_width, lbm_height;

	memset (BlckBuffer,0,5);
	memset (ReadBuffer,0,5);


		skip_lbm_header ( buffer, &lbm_width, &lbm_height, &lbm_planes );

		skip_read(BlckBuffer, buffer, 4);

		while (strcmp(BlckBuffer, "BODY") != 0)
		{
				if (strcmp(BlckBuffer, "CMAP") == 0)
				{
						skip_read(ReadBuffer, buffer, 4);
						*pal_size = Amiga4ToL(ReadBuffer);
						*pal_buffer = (Char *) malloc ( *pal_size );
						if (*pal_buffer == NULL) error (ERR_MEMORY);                    
						memcpy ( *pal_buffer, *buffer, *pal_size);
						pal_found = TRUE;
						break;
				}
				else
				{
						skip_read(ReadBuffer, buffer, 4);
						GenLength=Amiga4ToL(ReadBuffer);
						(*buffer) -= GenLength;
				}
				skip_read(BlckBuffer, buffer, 4 );
				if (*BlckBuffer=='\x0')    // Dangerous - assumes end of 'TINY' == 0
				{                                                  // For DPaint Enhanced
						(*buffer) -= 3; // where only even lengths
						skip_read(BlckBuffer, buffer, 4);    // correct although odd are
				}                                               // sometimes given.
		}
		if ( !pal_found ) error (ERR_LBM_PAL);
}

 
/*
 * get_pixel
 * ---------
 *
 * function     : get one pixel from picture
 * input        : picture buffer, its width, required (x,y)
 * return value : colour of pixel
 *
 */

static Char get_pixel (Char *pic_buffer, Int32 pic_width, Int32 pix_x, Int32 pix_y)
{
	return ( *(pic_buffer+(pic_width*pix_y)+pix_x));
}


/*
 * get_grid_size
 * -------------
 *
 * function : get the required grid size by examining picture
 * input    : picture buffer, with width and height
 * output   : correct width and height for grid
 *
 */

void get_grid_size ( Char *pic_buffer, Int32 pic_width, Int32 pic_height, Int32 *grid_width, Int32 *grid_height)
{
	Char grid_colour = get_pixel(pic_buffer,pic_width,0,0);
	Int32 x,y, x_max, y_max;
	Bool32 OK;
	
	for (x=0; x<pic_width; x++)
	{
		if ( get_pixel(pic_buffer,pic_width,x,0) != grid_colour )
			break;
	}
	x_max = x-1;    

	for (y=0; y<pic_height; y++)
	{
		if ( get_pixel(pic_buffer,pic_width,0,y) != grid_colour )
			break;
	}
	y_max = y-1;

	for (x=1; x<pic_width; x++)
	{
		if ( get_pixel(pic_buffer,pic_width,x,1) == grid_colour )
		{
			OK=TRUE;
			for (y=2; y<=y_max; y++)
			{
				if (get_pixel(pic_buffer,pic_width,x,y) != grid_colour)
				{
					OK=FALSE;
					break;
				}
			}
			if (OK) break;
		}
	}
	*grid_width = x-1;  

	for (y=1; y<pic_height; y++)
	{
		if ( get_pixel(pic_buffer,pic_width,1,y) == grid_colour )
		{
			OK=TRUE;
			for (x=2; x<=x_max; x++)
			{
				if (get_pixel(pic_buffer,pic_width,x,y) != grid_colour)
				{
					OK=FALSE;
					break;
				}
			}
			if (OK) break;
		}
	}
	*grid_height = y-1;
	
	if ( (*grid_width==0) || (*grid_height==0) ) error (ERR_INVALID_GRID);
}


/*
 * check_zero
 * ----------
 *
 * function     : checks if input buffer is all zeros
 * input        : input buffer and size
 * return value : TRUE if all zero, else false
 *
 */

static Bool32 check_zero ( Char *buffer, Int32 size )
{
	Int32 i;

	for (i=0; i<size; i++)
	{
		if (buffer[i])
			return (FALSE);
	}
	return (TRUE);
}


/*
 * grab_frame
 * ----------
 *
 * function : garb a frame from a picture
 * input    : picture buffer & width + co-ords & width & height of frame
 * output   : frame in buffer
 *
 */

static void grab_frame ( Char *pic_buffer, Int32 pic_width, Int32 fr_x, Int32 fr_y, Int32 fr_width, Int32 fr_height, Char *fr_buffer)
{
	Int32 i;
	Char *start = pic_buffer+(fr_y*pic_width)+fr_x;

	for (i=0; i<fr_height; i++)
	{
		memcpy (fr_buffer+(i*fr_width), start+(i*pic_width), fr_width);
	}
}


/*
 * pic2raw
 * -------
 *
 * function     : convert a picture to a raw column of sprites
 * input        : picture buffer with width and height + reqd sprite width & height
 * output       : raw buffer & size
 *
 */

void pic2raw (Char *pic_buffer, Int32 pic_width, Int32 pic_height, Int32 spr_width, Int32 spr_height, Char **raw_buffer, Int32 *raw_length)
{
	Int32 i,j,n=0;
//      Char str[128];

	*raw_buffer = (Char*) malloc ( pic_width * pic_height );
	if ( *raw_buffer == NULL ) error (ERR_MEMORY);

	for (i=1; i<=(pic_height-spr_height); i+=(spr_height+1) )
	{
		for (j=1; j<=(pic_width-spr_width); j+=(spr_width+1) )
		{
			grab_frame ( pic_buffer, pic_width, j, i, spr_width, spr_height, (*raw_buffer)+(n*spr_width*spr_height) );
			if (!check_zero(  (*raw_buffer)+(n*spr_width*spr_height), spr_width*spr_height )) n++;
		}
	}
	*raw_length = n*spr_width*spr_height; 
		//sprintf (str, "%d sprites @ %d x %d pixels\n", n, spr_width, spr_height );
		//lprint (str);
}


/*
 * copy_frame
 * ----------
 * 
 * function copy frame from in to out
 *
 */

static void copy_frame ( Char *in_buffer, Int32 in_width, Int32 frame_in_x, Int32 frame_in_y, Int32 frame_width, Int32 frame_height, Char *out_buffer, Int32 out_width, Int32 frame_out_x, Int32 frame_out_y ) 
{
	Int32 i;

	for (i=0; i<frame_height; ++i)
	{
		memcpy (out_buffer+((i+frame_out_y)*out_width)+frame_out_x, in_buffer+((i+frame_in_y)*in_width)+frame_in_x,frame_width);
	}
}

/*
 * pic2rawpic
 * ----------
 *
 * function     : convert a gridded picture to a raw picture
 * input        : picture buffer with width and height + reqd sprite width & height
 * output       : raw buffer ( same size as input )
 *
 */

void pic2rawpic (Char *pic_buffer, Int32 pic_width, Int32 pic_height, Int32 spr_width, Int32 spr_height, Char **raw_buffer)
{
	Int32 i,j,n=0;
	Int32 outx=0, outy=0;
//      Char str[128];

	*raw_buffer = (Char*) calloc ( pic_width * pic_height,1 );
	if ( *raw_buffer == NULL ) error (ERR_MEMORY);

	for (i=1; i<=(pic_height-spr_height); i+=(spr_height+1) )
	{
		for (j=1; j<=(pic_width-spr_width); j+=(spr_width+1) )
		{
			copy_frame ( pic_buffer, pic_width, j, i, spr_width, spr_height, (*raw_buffer), pic_width, outx, outy);
			outx+=spr_width;
		}
		outx=0;
		outy+=spr_height;
	}
}


/*
 * grab_frame_scale
 * ----------------
 *
 * function : garb a frame from a picture, scaled down
 * input    : picture buffer & width + co-ords & width & height of frame, scale
 * output   : frame in buffer
 *
 */

static void grab_frame_scale ( Char *pic_buffer, Int32 pic_width, Int32 fr_x, Int32 fr_y, Int32 fr_width, Int32 fr_height, Char *fr_buffer, Int32 scale)
{
	Int32 i,j;
	Char *start = pic_buffer+(fr_y*pic_width)+fr_x;

	for (i=0; i<fr_height; i+=scale)
	{
		for (j=0; j<fr_width; j+=scale)
			fr_buffer[((i/scale)*(fr_width/scale))+(j/scale)] = start[i*pic_width+j];
	}
}


/*
 * pic2raw_scale
 * -------------
 *
 * function     : convert a picture to a raw column of sprites, scale down
 * input        : picture buffer with width and height + reqd sprite width & height, scale
 * output       : raw buffer & size
 *
 */

void pic2raw_scale (Char *pic_buffer, Int32 pic_width, Int32 pic_height, Int32 spr_width, Int32 spr_height, Char **raw_buffer, Int32 *raw_length, Int32 scale)
{
	Int32 i,j,n=0;
	Int32 l;
//      Char str[128];

	*raw_buffer = (Char*) malloc ( pic_width * pic_height );
	if ( *raw_buffer == NULL ) error (ERR_MEMORY);

	for (i=1; i<=(pic_height-spr_height); i+=(spr_height+1) )
	{
		for (j=1; j<=(pic_width-spr_width); j+=(spr_width+1) )
		{
			l = n*(spr_width/scale)*(spr_height/scale); 
			grab_frame_scale ( pic_buffer, pic_width, j, i, spr_width, spr_height, (*raw_buffer)+l, scale );
			if (!check_zero( (*raw_buffer)+l, (spr_width/scale)*(spr_height/scale) )) n++;
		}
	}
	*raw_length = n*(spr_width/scale)*(spr_height/scale); 
		//sprintf (str, "%d sprites @ %d x %d pixels\n", n, spr_width, spr_height );
		//lprint (str);
}

