#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dvant.h"
#include "terror.h"
#include "tfile.h"


#define MAX_KEYS	(108000)
#define CHARS_IN_KEY	(128)

#define BUFFER_SIZE	(MAX_KEYS * (CHARS_IN_KEY + 1) )	//	8 for the string and 1 for the newline in the text file

struct KeyData
{
	char KeyString[CHARS_IN_KEY];
	Bool8 bOccursInOtherFile;
};

KeyData FirstFileKeys[MAX_KEYS];
UInt32 NumberOfKeysInFirstFile;

KeyData SecondFileKeys[MAX_KEYS];
UInt32 NumberOfKeysInSecondFile;

char error_message[100];

Char *pTempBuffer;

Bool8 white_space(Char c)
{
	return ( (c==' ') || (c=='\t') || (c=='\x0A') || (c=='\x0D') );
}


void uppercaserize(Char *pBuffer, UInt32 BufferSize)
{
	Int32 C;
	
	for ( C = 0; C < BufferSize; C++)
	{
		if (pBuffer[C] >= 'a' && pBuffer[C] <= 'z')
		{
			pBuffer[C] += 'A' - 'a';
		}
	}
}

void ReadKeysFromFile(char *pFileName, KeyData *pKeyArray, UInt32 *pNumOfKeys)
{
	FILE *pCurrFile;
	UInt32 FileSize;
	UInt32 SizeRead;
	UInt32 StatusFlag;
	UInt16 ReadingKeysStatus;
	UInt32 CurrKey, CurrCharInKey;
	UInt32 CharLoop;
	
	pCurrFile = fopen (pFileName, "r");
	if (pCurrFile == NULL)
	{
		sprintf(error_message, "Couldn't open %s\n", pFileName);
		lprint(error_message);
		error (ERR_OPEN_INPUT);
	}

	FileSize = file_size(pCurrFile);
//	FileSize = get_size_of_file(pCurrFile);

	if (FileSize > BUFFER_SIZE)
	{
		sprintf(error_message, "Not enough memory for %s\n", pFileName);
		lprint(error_message);
		error (ERR_MEMORY);
	}

	SizeRead = fread(pTempBuffer, 1, FileSize, pCurrFile);

	if (SizeRead > FileSize)
		error(ERR_READ);
		
	StatusFlag = fclose (pCurrFile);
	if (StatusFlag) error(ERR_CLOSE);
	
	ReadingKeysStatus = 0;
	CurrKey = 0;
	CurrCharInKey = 0;
	
	for (CharLoop = 0; CharLoop < SizeRead; CharLoop++)
	{
		if (white_space(pTempBuffer[CharLoop]))
		{
			if (ReadingKeysStatus == 1)
			{
				CurrKey++;
				if (CurrKey >= MAX_KEYS)
				{
					lprint("Too many keys\n");
					error (ERR_MEMORY);
				}
				CurrCharInKey = 0;
				ReadingKeysStatus = 0;
			}
		}
		else
		{
			ReadingKeysStatus = 1;
			pKeyArray[CurrKey].KeyString[CurrCharInKey++] = pTempBuffer[CharLoop];
		}
	}
	
	if (ReadingKeysStatus == 1)
	{
		CurrKey++;
		if (CurrKey >= MAX_KEYS)
		{
			lprint("Too many keys\n");
			error (ERR_MEMORY);
		}
	}
	
	*pNumOfKeys = CurrKey;

}

void main( Int32 argc, Char *argv[] )
{
	UInt32 loop, loop2;
	Bool8 bFound;
	FILE *pCurrFile;
	UInt32 StatusFlag;
	char temp_string_for_swap[CHARS_IN_KEY];

	if (argc != 6)
	{
        lprint ("keycheck expects five parameters\n");
        lprint ("Name of first file of text keys\n");
        lprint ("Name of second file of text keys\n");
        lprint ("Name of first output file\n");
        lprint ("Name of second output file\n");
        lprint ("Name of output file containing keys in both files\n");
        exit (-1);
	}

	pTempBuffer = (Char*) malloc(BUFFER_SIZE * sizeof(Char));
	if (*pTempBuffer == NULL) error (ERR_MEMORY);

	NumberOfKeysInFirstFile = 0;
	NumberOfKeysInSecondFile = 0;

	for (loop = 0; loop < MAX_KEYS; loop++)
	{
		for (loop2 = 0; loop2 < CHARS_IN_KEY; loop2++)
		{
			FirstFileKeys[loop].KeyString[loop2] = 0;
			SecondFileKeys[loop].KeyString[loop2] = 0;
		}
		
		FirstFileKeys[loop].bOccursInOtherFile = false;
		SecondFileKeys[loop].bOccursInOtherFile = false;
	}

	ReadKeysFromFile(argv[1], &FirstFileKeys[0], &NumberOfKeysInFirstFile);
	ReadKeysFromFile(argv[2], &SecondFileKeys[0], &NumberOfKeysInSecondFile);

/*	This bit alphabeticises the two files
	for (loop = 0; loop < NumberOfKeysInFirstFile; loop++)
	{
		for (loop2 = loop + 1; loop2 < NumberOfKeysInFirstFile; loop2++)
		{
			if (stricmp(FirstFileKeys[loop].KeyString, FirstFileKeys[loop2].KeyString) > 0)
			{
				strcpy(temp_string_for_swap, FirstFileKeys[loop].KeyString);
				strcpy(FirstFileKeys[loop].KeyString, FirstFileKeys[loop2].KeyString);
				strcpy(FirstFileKeys[loop2].KeyString, temp_string_for_swap);
			}
		}
	}


	for (loop = 0; loop < NumberOfKeysInSecondFile; loop++)
	{
		for (loop2 = loop + 1; loop2 < NumberOfKeysInSecondFile; loop2++)
		{
			if (stricmp(SecondFileKeys[loop].KeyString, SecondFileKeys[loop2].KeyString) > 0)
			{
				strcpy(temp_string_for_swap, SecondFileKeys[loop].KeyString);
				strcpy(SecondFileKeys[loop].KeyString, SecondFileKeys[loop2].KeyString);
				strcpy(SecondFileKeys[loop2].KeyString, temp_string_for_swap);
			}
		}
	}
*/

	for (loop = 0; loop < NumberOfKeysInFirstFile; loop++)
	{
		loop2 = 0;
		bFound = false;
		while ( (loop2 < NumberOfKeysInSecondFile) && (!bFound) )
		{
			if (stricmp(FirstFileKeys[loop].KeyString, SecondFileKeys[loop2].KeyString) == 0)
			{
				FirstFileKeys[loop].bOccursInOtherFile = true;
				bFound = true;
			}
			loop2++;
		}
	}
	
	for (loop = 0; loop < NumberOfKeysInSecondFile; loop++)
	{
		loop2 = 0;
		bFound = false;
		
		while ( (loop2 < NumberOfKeysInFirstFile) && (!bFound) )
		{
			if (stricmp(SecondFileKeys[loop].KeyString, FirstFileKeys[loop2].KeyString) == 0)
			{
				SecondFileKeys[loop].bOccursInOtherFile = true;
				bFound = true;
			}
			loop2++;
		}
	}
	
	pCurrFile = fopen (argv[3], "w");
	if (pCurrFile == NULL)
	{
		sprintf(error_message, "Couldn't open %s for writing\n", argv[3]);
		lprint(error_message);
		error (ERR_OPEN_INPUT);
	}
	
	for (loop = 0; loop < NumberOfKeysInFirstFile; loop++)
	{
		if (FirstFileKeys[loop].bOccursInOtherFile == false)
		{
			fprintf(pCurrFile, FirstFileKeys[loop].KeyString);
			fprintf(pCurrFile, "\n");
		}
	}

	StatusFlag = fclose (pCurrFile);
	if (StatusFlag) error(ERR_CLOSE);


	pCurrFile = fopen (argv[4], "w");
	if (pCurrFile == NULL)
	{
		sprintf(error_message, "Couldn't open %s for writing\n", argv[4]);
		lprint(error_message);
		error (ERR_OPEN_INPUT);
	}
	
	for (loop = 0; loop < NumberOfKeysInSecondFile; loop++)
	{
		if (SecondFileKeys[loop].bOccursInOtherFile == false)
		{
			fprintf(pCurrFile, SecondFileKeys[loop].KeyString);
			fprintf(pCurrFile, "\n");
		}
	}

	StatusFlag = fclose (pCurrFile);
	if (StatusFlag) error(ERR_CLOSE);
	
	pCurrFile = fopen(argv[5], "w");
	if (pCurrFile == NULL)
	{
		sprintf(error_message, "Couldn't open %s for writing\n", argv[5]);
		lprint(error_message);
		error (ERR_OPEN_INPUT);
	}
	
	for (loop = 0; loop < NumberOfKeysInFirstFile; loop++)
	{
		if (FirstFileKeys[loop].bOccursInOtherFile == true)
		{
			fprintf(pCurrFile, FirstFileKeys[loop].KeyString);
			fprintf(pCurrFile, "\n");
		}
	}

	StatusFlag = fclose (pCurrFile);
	if (StatusFlag) error(ERR_CLOSE);
	
	free(pTempBuffer);
}
