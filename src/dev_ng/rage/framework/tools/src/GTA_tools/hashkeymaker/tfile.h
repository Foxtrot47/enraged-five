/***************************************************
 * tfile.h                                         *
 *                                                 *
 * declarations for file operations for dino tools *
 *                                                 *
 * KRH 26/06/95                                    *
 *                                                 *
 * (c) 1995 DMA Design Ltd                         *
 ***************************************************/

#ifndef TFILE_H
#define TFILE_H

#define FILE_LIST_MAX (10000)

extern UInt16 swap_word ( UInt16 word );
extern UInt32 swap_dword ( UInt32 dword );
extern char *change_ext ( char *src, char *ext );
extern char last_letter ( char *filename );
extern char *change_last_letter ( char *filename, char last_letter );
extern bool file_exists(char *filename);
extern Int32 file_size(FILE *fp);
extern void file_read ( char *buffer, Int32 len, FILE *f );
extern Int32 file_len ( char *infile );
extern void load_file ( char *filename, char **buffer, Int32 *size );
extern void load_file_f ( char *filename, char *buffer, Int32 *size );
extern void save_file ( char *buffer, Int32 size, char *filename);
extern void save_file_append ( char *buffer, Int32 size, char *filename);
extern void append_file ( FILE *f, char *buffer, Int32 size );
extern void copy_file ( FILE *f, char *filename );
extern Int32 find_all_files (char *file_list[], char *match);
extern void load_lbm_file ( char *filename, char **lbm_buffer, Int32 *lbm_width, Int32 *lbm_height );
extern void load_lbm_pal ( char *filename, char **pal_buffer, Int32 *pal_size );
extern void get_grid_size ( char *pic_buffer, Int32 pic_width, Int32 pic_height, Int32 *grid_width, Int32 *grid_height);
extern void pic2raw (char *pic_buffer, Int32 pic_width, Int32 pic_height, Int32 spr_width, Int32 spr_height, char **raw_buffer, Int32 *raw_length);
extern void pic2rawpic (char *pic_buffer, Int32 pic_width, Int32 pic_height, Int32 spr_width, Int32 spr_height, char **raw_buffer);
extern void pic2raw_scale (char *pic_buffer, Int32 pic_width, Int32 pic_height, Int32 spr_width, Int32 spr_height, char **raw_buffer, Int32 *raw_length, Int32 scale);

extern void find_lbm_pal ( char **buffer, char **pal_buffer, Int32 *pal_size );

extern void open_save_file ( char *filename );
extern void write_file ( char *buffer, Int32 size );
extern void close_file ();

extern void open_save_file2 ( char *filename );
extern void write_file2 ( char *buffer, Int32 size );
extern void close_file2 ();

extern void open_load_file ( char *filename );
extern Bool8 read_file (char *buffer, Int32 size);
extern Bool8 read_file_a (char **buffer, Int32 size);
extern void skip_file (Int32 len);

extern void set_file_pos(Int32 position);
extern Int32 get_file_pos(void);

#endif