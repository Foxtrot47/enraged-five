/********************************
 * terror.cpp                   *
 *                              *
 * error control for dino tools *
 *                              *
 * KRH 23/06/95                 *
 *                              *
 * (c)	1995 DMA Design Ltd     *
 ********************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
//	#include <ansi_files.h>

#include "dvant.h"
#include "terror.h"

static char* error_string[] = {"OK",
						"cannot open input file",
						"cannot open output file",
						"cannot close file",
						"read failure",
						"write failure",
						"ftell failure",
						"fseek failure",
						"out of memory",
						"not whole delta",
						"invalid file length",
						"too many deltas",
						"delta too big",
						"not whole sprite",
						"too many sprites",
						"no matching files in directory",
						"allocating zero bytes dos memory",
						"allocating non-16 bytes dos memory",
						"out of dos memory",
						"failure freeing dos memory",
						"interrupt simulate failure",
						"vesa support failure",
						"vesa failure",
						"interrupt 31 failure",
						"no lbm header found",
						"lbm file not DPaint Enhanced format",
						"no lbm bitmap information found",
						"lbm bitmap header incorrect length",
						"lbm unknown compression type",
						"invalid grid",
						"writing zero bytes",
						"no palette in lbm",
						"cannot open log file",
						"cannot close log file",
						"cannot write to log file",
						"cannot find compressed map file",
						"multiple compressed map files",
						"block overflow",
						"column overflow",
						"stuff overflow",
						"sprite too big - max is 255x255",
						"incorrect file version",
						"missing ]",
						"routes added already",
						"grid width is not a multiple of 8",
						"invalid grid width",
						"invalid remap number",
						"lbm palette invalid size",
						"invalid case",
						"too many tiles",
						"string length incorrect",
						"too many palettes",
						"graphics overflow",
						"deltas too big",
						"too many deltas for one sprite",
						"invalid grid height",
						"too many files",
						"incorrect file type",
						"invalid size",
						"cannot remove file",
						"cannot rename file",
						"key too large",
						"too many keys",
						"too much text",
						"no whitespace before header",
						"duplicate keys",
						"missing }",
						"unsupported .tga format",
						"} without earlier {",
						"cannot nest comments",
						"should only be one character between a pair of ~",
						"word is too long",
						"unknown mission name",
						"Couldn't open japanese metrics file",
						"Japanese metrics file should begin with METRICS1",
						"Header of Japanese metrics file should end with 5",
						"Expected only numbers as data in the Japanese metrics file",
						"Couldn't find character from text file in the table of all used Japanese characters",
						"Unmatched tildes",
						"Character between square brackets is not an 8 bit ASCII character",
						"Character following -m should be a number between 1 and 9 followed by that number of .txt filenames",
						"out of memory when allocating Key Array Class",
						"out of memory when allocating Data Class",
						"out of memory when allocating Output",
						"out of memory when allocating First Text Buffer",
						"out of memory when allocating Extra Text Buffer",
						"number after -key should be 4 or 8",
						"number after -bytes should be 1 or 2",
						"long string is just too long",
						"These two labels have the same hash key. This is quite rare. Can you change one character in one of the labels?",
						"Didn't expect a ] in the middle of a line of text",
						"Can't have white space in the middle of a text key"
						};

static FILE *logff;
static bool log_open = FALSE;

/*
 * time_string
 * -----------
 *
 * function		: get time of day in a formatted string
 * return value	: string pointer
 *
 */

static char *time_string ()
{
    time_t time_of_day;
	char *tstr;
	static char str[128];

    time_of_day = time( NULL );
	tstr = ctime( &time_of_day );
	tstr[strlen(tstr)-1]='\0';
	sprintf( str, "\n------ %s ------\n", tstr );
	return (str);
}


/*
 * open_log
 * --------
 *
 * function		: open log file
 *
 */

void open_log (void)
{
	logff = fopen ("log", "at");
	if ( logff==NULL) error (ERR_OPEN_LOG);
	log_open = TRUE;

	loprint(time_string());
}


/*
 * close_log
 * ---------
 *
 * function		: close log file
 *
 */

void close_log (void)
{
	Int32 s;

	if ( log_open )
	{
		log_open = FALSE;
		s = fclose (logff);
		if (s) error (ERR_CLOSE_LOG);
	}
}


/*
 * loprint
 * ------
 *
 * function		: print string to log file only
 * input		: string
 *
 */

void loprint (char *str)
{
	Int32 s;

	if ( log_open )
	{
		s = fprintf (logff, str);
		if (s<0)
		{
			close_log();
			error (ERR_WRITE_LOG);
		}
	}
}
 

/*
 * lprint
 * ------
 *
 * function		: print string to screen and to log file
 * input		: string
 *
 */

void lprint (char *str)
{
	printf (str);
	loprint (str);
}
 

/*
 * error
 * -----
 *
 * function	: display error message and exit
 * input	: error number
 *
 */

void error (Int32 error_number)
{
	static char str[128];

	if (error_number)
	{
		sprintf (str,"Error %d : %s\n", error_number, error_string[error_number]);
		lprint(str);
	}
	_fcloseall();
//	__close_all();
	exit (-1);
}

void error_str (Int32 error_number, char *errstr)
{
	static char str[128];
	char short_error_string[100];

	if (error_number)
	{
		sprintf (str,"Error %d : %s\n", error_number, error_string[error_number]);
		lprint(str);
		if(errstr)
		{
			if (strlen(errstr) > 80)
			{
				strncpy(short_error_string, errstr, 80);
				short_error_string[80] = 0;
				sprintf (str,"Last Key Recognised: %s\n", short_error_string);
			}
			else
			{
				sprintf (str,"Last Key Recognised: %s\n", errstr);
			}
			lprint(str);
		}
	}
	_fcloseall();
//	__close_all();
	exit (-1);
}
	





