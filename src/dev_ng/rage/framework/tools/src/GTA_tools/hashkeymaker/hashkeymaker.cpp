#include <direct.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>	//	.h>
#include <set>

#include "basetypes.h"

#include "string\stringhash.h"

//////////////////////////////////////////////////////////////////////////
// FUNCTION : IsInt
// PURPOSE :  Returns true if this string is a proper int (only numbers)
//			  This string should have spaces stripped out already.
//////////////////////////////////////////////////////////////////////////
Bool8 IsInt(char String[])
{
	Int16	Index;

	Index = 0;

	if (String[0] == 0) return (0);

	while (String[Index] != 0)
	{
		if ( (String[Index] < '0' || String[Index] > '9') )	//	&& String[Index] != '-')
		{
			if ( (String[Index] == '-') && (Index == 0) )
			{

			}
			else
			{
				return (0);
			}
		}

		Index++;
	}
	return (1);
}

bool white_space(char c)
{
	return ( (c==' ') || (c=='\t') || (c=='\x0A') || (c=='\x0D') );
}


void RemoveFirstNCharactersFromString(char String[], Int16 NumberOfChars)
{
	Int16 CurrentChar = NumberOfChars;

	// Copy the whole thing back a bit
	while (String[CurrentChar] != 0)
	{
		String[CurrentChar - NumberOfChars] = String[CurrentChar];
		CurrentChar++;
	}
	String[CurrentChar - NumberOfChars] = 0;
}


void RemoveFirstWordFromString(char String[])
{
	Int16 Search;

	Search = 0;

	// Skip initial blank spaces
	while (String[Search] == ' ') Search++;
	// Skip first word
	while (String[Search] != ' ' && String[Search] != 0) Search++;
	while (String[Search] == ' ') Search++;

	RemoveFirstNCharactersFromString(String, Search);
}


//////////////////////////////////////////////////////////////////////////
// FUNCTION : LineBeginsWithString
// PURPOSE :  Checks whether the first word in LineString matches 
//				StringToCheckFor and returns TRUE if it does.
//
//////////////////////////////////////////////////////////////////////////
Bool8 LineBeginsWithString(char LineString[], char StringToCheckFor[])
{
	char FirstWord[128];
	UInt16 SourceIndex, DestIndex;

	SourceIndex = 0;
	DestIndex = 0;

//	Ignore any leading spaces
	while ((LineString[SourceIndex] == 9) || (LineString[SourceIndex] == ' '))
	{
		if (LineString[SourceIndex] == 0)
		{
			return FALSE;
		}
		
		SourceIndex++;
	}

	while ((LineString[SourceIndex] != 9) 
		&& (LineString[SourceIndex] != ' ')
		&& (LineString[SourceIndex] != 0)
		&& (DestIndex < 127))
	{
		FirstWord[DestIndex++] = LineString[SourceIndex++];
	}

	FirstWord[DestIndex] = 0;

	if (!strcmp(FirstWord, StringToCheckFor))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


//////////////////////////////////////////////////////////////////////////
// FUNCTION : ContainsLetters
// PURPOSE :  Returns true if this string is a proper string (has at least 
//				one letter)
//			  This string should have spaces stripped out already.
//////////////////////////////////////////////////////////////////////////
bool ContainsLetters(char String[])
{
	UInt16 string_length, loop;

	string_length = (UInt16)strlen(String);

	loop = 0;

	while (loop < string_length)
	{
		if (((String[loop] >= 'a' && String[loop] <= 'z') ||
			 (String[loop] >= 'A' && String[loop] <= 'Z')))
		{
			return TRUE;
		}

		loop++;
	}

	return FALSE;
}


bool LineBeginsWithNumber(char LineString[])
{
	char FirstWord[128];
	UInt16 SourceIndex, DestIndex;

	SourceIndex = 0;
	DestIndex = 0;

//	Ignore any leading spaces
	while ((LineString[SourceIndex] == 9) || (LineString[SourceIndex] == ' ') || (LineString[SourceIndex] == ','))
	{
		if (LineString[SourceIndex] == 0)
		{
			return FALSE;
		}
		
		SourceIndex++;
	}

	while ((LineString[SourceIndex] != 9) 
		&& (LineString[SourceIndex] != ' ')
		&& (LineString[SourceIndex] != 0)
		&& (LineString[SourceIndex] != ',')
		&& (DestIndex < 127))
	{
		FirstWord[DestIndex++] = LineString[SourceIndex++];
	}

	FirstWord[DestIndex] = 0;

	if (IsInt(FirstWord))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}



int ReadOneLine(FILE *fp, char String[], UInt16 MaxStringLength)
{
	Int16	NumChar;
	Int16	Char;

	NumChar = 0;

	Char = getc(fp);
	while (Char != EOF && Char != '\n' && Char != 0)
	{
		String[NumChar++] = (char)Char;
		Char = getc(fp);
	}
	
	String[NumChar] = (char)0;

	if (Char == EOF && NumChar == 0)
	{
		return (EOF);
	}

	if (NumChar >= MaxStringLength)
	{
		printf("**** ReadOneLine - Line in file is too long\n");	//	ErrorString
		return (-2);
	}

	return (NumChar);
}


/*
	StringLength = 0;
	while (ObjectDataString[StringLength] != 0)
	{
		ProcessedString[StringLength] = ObjectDataString[StringLength];
		if (ProcessedString[StringLength] >= 'a' && ProcessedString[StringLength] <= 'z')
		{
			ProcessedString[StringLength] += 'A'-'a';
		}
		StringLength++;
	}
	ProcessedString[StringLength] = 0;
*/



#define MAX_NAME_LENGTH	(64)

struct sModelData
{
	char ModelName[MAX_NAME_LENGTH];
	int32 ModelHashKey;
};

#define MAX_MODELS	(32000)

sModelData ArrayOfAllModelData[MAX_MODELS];
UInt32 nTotalNumberOfModels = 0;

bool bWrittenFirstEntry = false;
bool bWrittenAtLeastOneEntry = false;

bool ReadModelNamesFromFile(char *input_filename)
{
	const int MaxLengthOfInputLine = 1024;
	char InputString[MaxLengthOfInputLine];
	char TempString[MaxLengthOfInputLine];

	nTotalNumberOfModels = 0;

	FILE *pInputFile = NULL;
	if ((pInputFile = fopen(input_filename, "r" )) == NULL)
	{
		printf("**** Couldn't open %s for reading\n", input_filename);
		return false;
	}

	int return_error = -1;
	while ((return_error = ReadOneLine(pInputFile, InputString, MaxLengthOfInputLine)) >= 0)
	{
		int StringLength = 0;
		while (InputString[StringLength] != 0)
		{
			// We have to replace tabs by spaces. sscanf doesn't seem to deal with them properly.
			if (InputString[StringLength] == 9) InputString[StringLength] = ' ';

			// replace commas and brackets with spaces. (these are separators anyway)
			if (InputString[StringLength] == ',') InputString[StringLength] = ' ';
			else if (InputString[StringLength] == '(') InputString[StringLength] = ' ';
			else if (InputString[StringLength] == ')') InputString[StringLength] = ' ';

			//	Change any equals signs to spaces
			if (InputString[StringLength] == '=') InputString[StringLength] = ' ';

			StringLength++;
		}

		bool bSkipThisLine = false;
		if (InputString[0] == '/' && InputString[1] == '/')
		{
			bSkipThisLine = true;
		}

		if (LineBeginsWithString(InputString, "ENUM"))
		{
			bSkipThisLine = true;
		}

		if (LineBeginsWithString(InputString, "ENDENUM"))
		{
			bSkipThisLine = true;
		}

		if (LineBeginsWithString(InputString, "DUMMY_MODEL_FOR_SCRIPT"))
		{
			bSkipThisLine = true;
		}

		if (!bSkipThisLine)
		{
			if (sscanf(InputString, "%s", TempString) <= 0)
			{
				printf("**** Couldn't read first item in string\n");
				return false;
			}

			if (strlen(TempString) >= MAX_NAME_LENGTH)
			{
				printf("**** String %s is too long. Need to increase MAX_NAME_LENGTH\n", TempString);
				return false;
			}

			strncpy(ArrayOfAllModelData[nTotalNumberOfModels].ModelName, TempString, MAX_NAME_LENGTH);
			ArrayOfAllModelData[nTotalNumberOfModels].ModelHashKey = 0;

			RemoveFirstWordFromString(InputString);

			StringLength = (int) strlen(InputString);
			if (StringLength > 0)
			{	//	If the line ends with a comma then remove the comma
				if (InputString[StringLength-1] == ',')
				{
					InputString[StringLength-1] = '\0';
				}

				if (strlen(InputString) > 0)
				{
					while (white_space(InputString[0]))
					{
						RemoveFirstNCharactersFromString(InputString, 1);
					}

					if (strlen(InputString) > 0)
					{
						if (sscanf(InputString, "%s", TempString) <= 0)
						{
							printf("**** Couldn't read second item in string\n");
							return false;
						}

						if (strlen(TempString) >= MAX_NAME_LENGTH)
						{
							printf("**** String %s is too long. Need to increase MAX_NAME_LENGTH\n", TempString);
							return false;
						}

						if (IsInt(TempString))
						{
							ArrayOfAllModelData[nTotalNumberOfModels].ModelHashKey = atoi(TempString);
						}
						else
						{
							printf("**** Expected the second item in the string to be an integer %s\n", TempString);
							return false;
						}
					}
				}
			}

			nTotalNumberOfModels++;
		}
	}

	if (pInputFile)
	{
		fclose(pInputFile);
		pInputFile = NULL;
	}

	if (return_error < 0 && return_error != EOF)
	{
		printf("**** Error when reading data from file %s\n", input_filename);
		return false;
	}

	return true;
}

bool CreateOutputFiles(char *output_model_enums_filename, char *output_text_filename)
{
	FILE *pOutputModelEnumsFile = NULL;
	if (_stricmp(output_model_enums_filename, "NULL"))
	{
		if ((pOutputModelEnumsFile = fopen(output_model_enums_filename, "w" )) == NULL)
		{
			printf("**** Couldn't open %s for writing - check it is writable\n", output_model_enums_filename);
			return false;
		}

		fprintf(pOutputModelEnumsFile, "ENUM MODEL_NAMES\n");
		fprintf(pOutputModelEnumsFile, "DUMMY_MODEL_FOR_SCRIPT=0,\n");

		bWrittenFirstEntry = false;
		bWrittenAtLeastOneEntry = false;
	}

	FILE *pOutputTextFile = NULL;
	if (_stricmp(output_text_filename, "NULL"))
	{
		if ((pOutputTextFile = fopen(output_text_filename, "w" )) == NULL)
		{
			printf("**** Couldn't open %s for writing - check it is writable\n", output_text_filename);
			return false;
		}
	}

	int tempHashKey = 0;
	for (uint32 model_loop = 0; model_loop < nTotalNumberOfModels; model_loop++)
	{
		if (pOutputTextFile)
		{	//	Write to the text file
			fprintf(pOutputTextFile, "%s\n", ArrayOfAllModelData[model_loop].ModelName);
		}

		if (pOutputModelEnumsFile)
		{	//	Write to the model enums file
			if (bWrittenFirstEntry)
			{
				fprintf(pOutputModelEnumsFile, ",\n");
			}

			tempHashKey = ArrayOfAllModelData[model_loop].ModelHashKey;
			ArrayOfAllModelData[model_loop].ModelHashKey = rage::atStringHash(ArrayOfAllModelData[model_loop].ModelName);
			if ( (tempHashKey > 0) && (tempHashKey != ArrayOfAllModelData[model_loop].ModelHashKey) )
			{
				printf("**** %s Hash key read from input file (%d) does not match the hash key that I've just calculated (%d)\n", ArrayOfAllModelData[model_loop].ModelName, tempHashKey, ArrayOfAllModelData[model_loop].ModelHashKey);
				return false;
			}
			fprintf(pOutputModelEnumsFile, "%s=%d", ArrayOfAllModelData[model_loop].ModelName, ArrayOfAllModelData[model_loop].ModelHashKey);

			bWrittenFirstEntry = true;
			bWrittenAtLeastOneEntry = true;
		}
	}

	if (pOutputModelEnumsFile)
	{
		if (bWrittenAtLeastOneEntry)
		{
			fprintf(pOutputModelEnumsFile, "\n");
		}
		fprintf(pOutputModelEnumsFile, "ENDENUM\n");

		fclose(pOutputModelEnumsFile);
		pOutputModelEnumsFile = NULL;
	}

	if (pOutputTextFile)
	{
		fclose(pOutputTextFile);
		pOutputTextFile = NULL;
	}

	return true;
}



int main( int argc, char *argv[] )
{
	if (argc != 4)
	{
		printf("**** Expected three arguments - input file, output model enums file (specify NULL if you don't need this), output text file (specify NULL if you don't need this) ****\n");
		return (-1);
	}

	if (ReadModelNamesFromFile(argv[1]) == false)
	{
		printf("**** Error when reading input file ****\n");
		return (-1);
	}

	if (CreateOutputFiles(argv[2], argv[3]) == false)
	{
		printf("**** Error when writing output files ****\n");
		return (-1);
	}

	return 0;
}
