#ifndef COMMON_TOOLBAR_H
#define COMMON_TOOLBAR_H

#include "libwin/window.h"

///////////////////////////////////////////////////////////////////////////////////////////////
class ToolBar : public Window
///////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	void init(int32 iIDToolbar,int64 iIDBitmap,Window* p_wndParent);
	void addButton();
	void enableButton(int32 iIndex,bool bOn);

protected:
};

#endif COMMON_TOOLBAR_H