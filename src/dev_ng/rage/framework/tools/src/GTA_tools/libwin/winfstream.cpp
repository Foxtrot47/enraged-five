#include <fcntl.h>
#include <io.h>
#include <stdlib.h>
#include <stdio.h>
#include <process.h>
#include <windows.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "libwin/winfstream.h"
#include "libcore/exception.h"

using namespace WinLib;

///////////////////////////////////////////////////////////////////////////////////////////////
WinFileStream::WinFileStream():
	m_hFile(NULL),
	m_osCurrent(IS_CLOSED)
{
	
}

///////////////////////////////////////////////////////////////////////////////////////////////
WinFileStream::~WinFileStream()
{
	close();
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool WinFileStream::isOpen()
{
	if(m_hFile == NULL)
	{
		return false;
	}	

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool WinFileStream::open(const char* p_cFilename,const char* p_cOpenFlags)
{
/*
	if(	(m_osCurrent == IS_READING) && 
		(stricmp(p_cFilename,m_cOpenFilename) == 0) && 
		(strchr(p_cOpenFlags,'r') || strchr(p_cOpenFlags,'R')))
	{
		m_uiReadOffset = 0;
		return true;
	}
*/
	close();

	if(strchr(p_cOpenFlags,'w') || strchr(p_cOpenFlags,'W'))
	{
		m_osCurrent = IS_WRITING;

		if(strchr(p_cOpenFlags,'t') || strchr(p_cOpenFlags,'T'))
		{		
			m_hFile = CreateFile(	p_cFilename,
									GENERIC_WRITE,
									0,
									NULL,
									CREATE_ALWAYS,
									FILE_ATTRIBUTE_NORMAL|FILE_FLAG_SEQUENTIAL_SCAN,
									NULL);
		}
		else
		{
			m_hFile = CreateFile(	p_cFilename,
									GENERIC_WRITE,
									0,
									NULL,
									OPEN_ALWAYS,
									FILE_ATTRIBUTE_NORMAL|FILE_FLAG_SEQUENTIAL_SCAN,
									NULL);
		}
	}
	else if(strchr(p_cOpenFlags,'r') || strchr(p_cOpenFlags,'R'))
	{
		m_osCurrent = IS_READING;

		m_hFile = CreateFile(	p_cFilename,
								GENERIC_READ,
								FILE_SHARE_READ,
								NULL,
								OPEN_EXISTING,
								FILE_ATTRIBUTE_NORMAL|FILE_FLAG_SEQUENTIAL_SCAN,
								NULL);
	}	

	if((m_hFile == INVALID_HANDLE_VALUE) || (m_hFile == NULL))
	{
		return false;
	}

//	strcpy(m_cOpenFilename,p_cFilename);

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////
void WinFileStream::close()
{
	if(m_hFile)
	{
		m_osCurrent = IS_CLOSED;
		CloseHandle(m_hFile);
		m_hFile = NULL;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////
uint32 WinFileStream::getPending()
{
	if(m_hFile)
	{
		DWORD dwBytesSize,dwBytesHigh,dwOffset;

		dwBytesSize = GetFileSize(m_hFile,&dwBytesHigh);
		dwOffset = SetFilePointer(m_hFile,0,NULL,FILE_CURRENT);

		return (dwBytesSize - dwOffset);
	}

	return 0;
}

///////////////////////////////////////////////////////////////////////////////////////////////
uint32 WinFileStream::seek(uint32 uOffset,int32 seekOrigin)
{
	switch(seekOrigin)
	{
	case STREAM_BEGIN:
		SetFilePointer(m_hFile,uOffset,NULL,FILE_BEGIN);
		break;
	case STREAM_CURRENT:
		SetFilePointer(m_hFile,uOffset,NULL,FILE_CURRENT);
		break;
	case STREAM_END:
		SetFilePointer(m_hFile,uOffset,NULL,FILE_END);
		break;
	default:
		SetFilePointer(m_hFile,uOffset,NULL,FILE_BEGIN);
		break;
	}

	return getPos();
}

///////////////////////////////////////////////////////////////////////////////////////////////
uint32 WinFileStream::getPos()
{
	return SetFilePointer(m_hFile,0,NULL,FILE_CURRENT);
}

///////////////////////////////////////////////////////////////////////////////////////////////
uint32 WinFileStream::write(void* p_cWriteFrom,uint32 uiBytes)
{
	if(m_osCurrent != IS_WRITING)
	{
		Assert(false);
		return 0;
	}

	DWORD dwBytesWritten;

	WriteFile(m_hFile,p_cWriteFrom,uiBytes,&dwBytesWritten,NULL);

	return dwBytesWritten;
}

///////////////////////////////////////////////////////////////////////////////////////////////
uint32 WinFileStream::read(void* p_cReadTo,uint32 uiBytes)
{
	if(m_osCurrent != IS_READING)
	{
		Assert(false);
		return 0;
	}

	DWORD dwBytesRead;

	ReadFile(m_hFile,p_cReadTo,uiBytes,&dwBytesRead,NULL);

	return dwBytesRead;
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool WinFileStream::moveFile(const char* p_cFilePathSource,const char* p_cFilePathDest)
{
	if(MoveFile(p_cFilePathSource,p_cFilePathDest) == 0)
	{
		return false;
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool WinFileStream::getTempFilename(const char* p_cPath,const char* p_cPrefix,uint32 Unique,char* p_cOutput)
{
	if(GetTempFileName(	p_cPath,
						p_cPrefix,
						Unique,
						p_cOutput) == 0)
	{
		return false;
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////
uint8* WinFileStream::readAll(const char* p_cFilename,uint32& r_uiSize)
{
	WinFileStream fsTemp;

	uint8* p_cData;

	fsTemp.open(p_cFilename,"r");

	r_uiSize = fsTemp.getPending();

	p_cData = new uint8[r_uiSize];

	fsTemp.read(p_cData,r_uiSize);

	fsTemp.close();

	return p_cData;
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool WinFileStream::createFile(const char* p_cFilePath)
{
	HANDLE hFile = CreateFile(	p_cFilePath,
								GENERIC_WRITE,
								FILE_SHARE_WRITE,
								NULL,
								CREATE_NEW,
								FILE_ATTRIBUTE_NORMAL,
								NULL);

	CloseHandle(hFile);

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////
int32 WinFileStream::getFileSize(const char* p_cFilePath)
{
	int32 iFileID = _open(p_cFilePath,_O_RDONLY|_O_BINARY);
	int32 iSize = -1;

	if(iFileID == -1)
	{
		return iSize;
	}

	iSize = _lseek(iFileID,0,SEEK_END);
	_close(iFileID);

	return iSize;
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool WinFileStream::copyFile(const char* p_cSourcePath,const char* p_cTargetPath)
{
	BOOL bRet = CopyFile(p_cSourcePath,p_cTargetPath,FALSE);

	if(bRet == TRUE)
	{
		SetFileAttributes(p_cTargetPath,FILE_ATTRIBUTE_NORMAL);

		return true;
	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////
uint64 WinFileStream::findFirstFile(char* p_cFilename,const char* p_cWildCard)
{
	WIN32_FIND_DATA FindFileData;

	uint64 iHandle = (uint64)FindFirstFile(p_cWildCard,&FindFileData);	

	if((HANDLE)iHandle == INVALID_HANDLE_VALUE)
	{
		p_cFilename[0] = '\0';
		return 0;
	}

	strcpy(p_cFilename,FindFileData.cFileName);

	return iHandle;
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool WinFileStream::findNextFile(char* p_cFilename,uint64 iHandle)
{
	WIN32_FIND_DATA FindFileData;

	if(FindNextFile((HANDLE)iHandle,&FindFileData) == 0)
	{
		FindClose((HANDLE)iHandle);
		p_cFilename[0] = '\0';
		return false;
	}

	strcpy(p_cFilename,FindFileData.cFileName);

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////
void WinFileStream::findEnd(uint64 iHandle)
{
	FindClose((HANDLE)iHandle);
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool WinFileStream::createDirectory(const char* p_cDirPath)
{
	BOOL bRet = CreateDirectory(p_cDirPath,NULL);

	if(bRet == FALSE)
	{
		return false;
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool WinFileStream::deleteDirectory(const char* p_cDirPath)
{
	char cDirBuffer[MAX_PATH];

	strcpy(cDirBuffer,p_cDirPath);
/*
	int32 iLength = strlen(cDirBuffer);

	if(iLength > 0)
	{
		if(cDirBuffer[iLength - 1] == '\\')
		{
			cDirBuffer[iLength - 1] = '\0';
		}
	}
*/
	BOOL bRet = RemoveDirectory(cDirBuffer);

	if(bRet == FALSE)
	{
		return false;
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool WinFileStream::deleteFile(const char* p_cFilePath)
{
	if(strchr(p_cFilePath,'\\'))
	{
		char cFilename[MAX_PATH];
		char cDirectory[MAX_PATH];

		strcpy(cDirectory,p_cFilePath);
		makeJustDir(cDirectory);

		int64 iFind;

		if((iFind = findFirstFile(cFilename,p_cFilePath)))
		{
			do
			{
				char cFullPath[MAX_PATH];

				sprintf(cFullPath,"%s%s",cDirectory,cFilename);

				BOOL bRet = DeleteFile(cFullPath);
			}
			while(findNextFile(cFilename,iFind));
		}
	}
	else
	{
		if(!DeleteFile(p_cFilePath))
		{
			return false;
		}
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool WinFileStream::fileExists(const char* p_cFilePath)
{
	char cFileFound[MAX_PATH];

	int64 iFind;

	if((iFind = findFirstFile(cFileFound,p_cFilePath)))
	{
		findEnd(iFind);
		return true;
	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool WinFileStream::isReadOnly(const char* p_cFilePath)
{
	if(!fileExists(p_cFilePath))
	{
		return false;
	}

	uint32 uOpenFlags = _O_BINARY|_O_WRONLY;

	int32 iFileID = _open(p_cFilePath,uOpenFlags,_S_IREAD|_S_IWRITE);

	if(iFileID == -1)
	{
		return true;
	}

	_close(iFileID);

	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool WinFileStream::makeWritable(const char* p_cFilePath)
{
	DWORD attrib = GetFileAttributes(p_cFilePath);

	attrib &= ~FILE_ATTRIBUTE_READONLY;

	BOOL bRet = SetFileAttributes(p_cFilePath,attrib);

	if(bRet == TRUE)
	{
		return true;
	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////
void WinFileStream::makeJustDir(char* p_cFilename)
{
	char* p_FirstFind = strrchr(p_cFilename,'\\');
	char* p_SecondFind = strrchr(p_cFilename,'/');

	if((p_FirstFind == NULL) && (p_SecondFind == NULL))
	{
		*p_cFilename = '\0';
	}
	else if((p_SecondFind == NULL) || ((p_FirstFind != NULL) && (p_FirstFind > p_SecondFind)))
	{
		p_FirstFind++;
		*p_FirstFind = NULL;
	}
	else if((p_FirstFind == NULL) || ((p_SecondFind != NULL) && (p_SecondFind > p_FirstFind)))
	{
		p_SecondFind++;
		*p_SecondFind = NULL;
	}
	else
	{
		*p_cFilename = '\0';
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////
int32 WinFileStream::runCmd(const char* p_cCommand)
{
	return system(p_cCommand);
}