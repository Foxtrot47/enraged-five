#ifndef COMMON_CONTROLLER_H
#define COMMON_CONTROLLER_H

#include <dinput.h>
#include "libcore/vector2d.h"

///////////////////////////////////////////////////////////////////////////////////////////////
class PCController
///////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	virtual void update() = 0;

	static LPDIRECTINPUT8 mp_di8;
};

///////////////////////////////////////////////////////////////////////////////////////////////
class PCKeyboardController : public PCController
///////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	void init(HWND hWnd,HINSTANCE hInstance);
	bool isKeyDown(uint32 uDirectInputEnum);
	void update();

protected:
	LPDIRECTINPUTDEVICE8 mp_did8;
	uint8 m_cBuffer[256];
};

///////////////////////////////////////////////////////////////////////////////////////////////
class PCMouseController : public PCController
///////////////////////////////////////////////////////////////////////////////////////////////
{
public:

	void init(HWND hWnd,HINSTANCE hInstance);
	void update();

	const bool isLeftDown() { return m_bLeftDown; }
	const bool isLeftJustDown() { return m_bLeftJustDown; }
	const bool isLeftJustUp() { return m_bLeftJustUp; }

	const bool isRightDown() { return m_bRightDown; }
	const bool isRightJustDown() { return m_bRightJustDown; }
	const bool isRightJustUp() { return m_bRightJustUp; }

	const void getCursorPos(CVector2D& r_vecPos) { r_vecPos = m_vecPos; }

protected: 

	HANDLE m_hEvent;
	LPDIRECTINPUTDEVICE8 mp_did8;
	uint8 m_cBuffer[16];
	CVector2D m_vecPos;
	bool m_bLeftDown;
	bool m_bLeftJustDown;
	bool m_bLeftJustUp;
	bool m_bRightDown;
	bool m_bRightJustDown;
	bool m_bRightJustUp;
};

#endif