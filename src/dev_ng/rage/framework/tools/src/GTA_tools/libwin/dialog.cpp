#include <windows.h>

#include "libwin/dialog.h"
#include "libwin/log.h"
#include "libwin/window.h"

//////////////////////////////////////////////////////////////////////////////////////////////////
INT_PTR CALLBACK DialogProc(HWND hwndDlg,UINT uMsg,WPARAM wParam,LPARAM lParam)
{
	switch(uMsg)
	{
	case WM_INITDIALOG:
		{
			Window::centreToParent(hwndDlg);
		}
	
		return 0;

   	case WM_COMMAND:

		switch(wParam)
		{
		case IDOK:	
			EndDialog(hwndDlg,1);
			break;
		case IDCANCEL:
			EndDialog(hwndDlg,0);
			break;
		}

		return 0;

	}

	return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
Dialog::Dialog():
	m_hWnd(NULL)
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool Dialog::init(uint32 uDialogID,DLGPROC dlgProc)
{
	m_hWnd = CreateDialog(	GetModuleHandle(NULL),
							MAKEINTRESOURCE(uDialogID),
							NULL,
							dlgProc ? dlgProc : DialogProc);

	if(m_hWnd != NULL)
	{
		return true;
	}
	
	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool Dialog::initModal(uint32 uDialogID,HWND hParent,DLGPROC dlgProc)
{
	if(DialogBox(	GetModuleHandle(NULL),
				MAKEINTRESOURCE(uDialogID),
				hParent,
				dlgProc ? dlgProc : DialogProc))
	{
		return true;
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool Dialog::update()
{
	MSG msg;

    if(PeekMessage(&msg,m_hWnd,0,0,PM_REMOVE))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);

		if(msg.message == WM_QUIT)
		{
			return false;
		}
	}
	
	return true;
}