#include "libwin/registry.h"
#include "libwin/os.h"
#include "libcore/exception.h"

//////////////////////////////////////////////////////////////////////////////////////////////////
HKEY CreateKey(HKEY hParent,const char* p_cName)
{
	HKEY hNew;
	DWORD dwDisposition;

	if (RegCreateKeyEx(	hParent,
						p_cName,
						0,
						NULL,
						REG_OPTION_NON_VOLATILE,
						KEY_ALL_ACCESS,
						NULL,
						&hNew,
						&dwDisposition) != ERROR_SUCCESS)
	{
		os->log("error creating/opening key\n");
		throw new Exception("createkey: could not create registry key\n");
		return NULL;
	}

	return hNew;
}

/////////////////////////////////////////////////////////////////////
Registry::Registry(const char* p_cAppName)
{
	mp_cAppName = p_cAppName;
}

/////////////////////////////////////////////////////////////////////
Registry::~Registry()
{
}

/////////////////////////////////////////////////////////////////////
void Registry::setString(const char* p_cEntryName,const char* p_cValue)
{
	HKEY hSoftware = CreateKey(HKEY_CURRENT_USER,"Software");
	HKEY hRockstar = CreateKey(hSoftware,"RockstarNorth");
	HKEY hAIEditor = CreateKey(hRockstar,mp_cAppName);

	RegSetValueEx(  hAIEditor,
					p_cEntryName,
					NULL,
					REG_SZ,
					(const unsigned char*)p_cValue,
					strlen(p_cValue) + 1);

	RegCloseKey(hAIEditor);
	RegCloseKey(hRockstar);
	RegCloseKey(hSoftware);
}

/////////////////////////////////////////////////////////////////////
bool Registry::getString(const char* p_cEntryName,char* p_cValue)
{
	//if the keys dont already exist then set up the defaults
	DWORD uType;
	uint8 cBuffer[1024];
	DWORD uSize = 1024;
	bool bRet;

	HKEY hSoftware = CreateKey(HKEY_CURRENT_USER,"Software");
	HKEY hRockstar = CreateKey(hSoftware,"RockstarNorth");
	HKEY hAIEditor = CreateKey(hRockstar,mp_cAppName);

	uint64 uRet = RegQueryValueEx(hAIEditor,p_cEntryName,NULL,&uType,cBuffer,&uSize);

	if(uRet == ERROR_SUCCESS)
	{
		strcpy(p_cValue,(const char*)cBuffer);
		bRet = true;
	}
	else
	{
		*p_cValue = '\0';
		bRet = false;
	}

	RegCloseKey(hAIEditor);
	RegCloseKey(hRockstar);
	RegCloseKey(hSoftware);

	return bRet;
}

/////////////////////////////////////////////////////////////////////
void Registry::getStringDefault(const char* p_cEntryName,char* p_cValue,const char* p_cDefault)
{
	if(!getString(p_cEntryName,p_cValue))
	{
		strcpy(p_cValue,p_cDefault);
		setString(p_cEntryName,p_cDefault);
	}
}

/////////////////////////////////////////////////////////////////////
void Registry::setInteger(const char* p_cEntryName,int32 iValue)
{
	HKEY hSoftware = CreateKey(HKEY_CURRENT_USER,"Software");
	HKEY hRockstar = CreateKey(hSoftware,"RockstarNorth");
	HKEY hAIEditor = CreateKey(hRockstar,mp_cAppName);

	RegSetValueEx(  hAIEditor,
					p_cEntryName,
					NULL,
					REG_DWORD,
					(unsigned char*)&iValue,
					sizeof(int32));

	RegCloseKey(hAIEditor);
	RegCloseKey(hRockstar);
	RegCloseKey(hSoftware);
}

/////////////////////////////////////////////////////////////////////
bool Registry::getInteger(const char* p_cEntryName,int32& r_iValue)
{
	//if the keys dont already exist then set up the defaults
	DWORD uType;
	DWORD uSize = sizeof(int32);
	bool bRet;

	HKEY hSoftware = CreateKey(HKEY_CURRENT_USER,"Software");
	HKEY hRockstar = CreateKey(hSoftware,"RockstarNorth");
	HKEY hAIEditor = CreateKey(hRockstar,mp_cAppName);
	
	uint64 uRet = RegQueryValueEx(hAIEditor,p_cEntryName,NULL,&uType,(unsigned char*)&r_iValue,&uSize);

	if(uRet == ERROR_SUCCESS)
	{
		bRet = true;
	}
	else
	{
		bRet = false;
	}

	RegCloseKey(hAIEditor);
	RegCloseKey(hRockstar);
	RegCloseKey(hSoftware);

	return bRet;
}