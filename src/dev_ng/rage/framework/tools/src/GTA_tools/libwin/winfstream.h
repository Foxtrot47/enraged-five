#ifndef COMMON_WINFSTREAM_H
#define COMMON_WINFSTREAM_H

#include <windows.h>
#include "libcore/fstream.h"

namespace WinLib {

///////////////////////////////////////////////////////////////////////////////////////////////
class WinFileStream : public FileStream
///////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	WinFileStream();
	~WinFileStream();

	bool isOpen();
	bool open(const char* p_cFilename,const char* p_cOpenFlags);
	void close();

	uint32 seek(uint32 uOffset,int32 seekOrigin);
	uint32 getPos();
	uint32 write(void* p_cWriteFrom,uint32 uiBytes);
	uint32 read(void* p_cReadTo,uint32 uiBytes);
	uint32 getPending();
	uint64 getFileID();

	//these should all go into a file device

	bool moveFile(const char* p_cFilePathSource,const char* p_cFilePathDest);
	bool getTempFilename(const char* p_cPath,const char* p_cPrefix,uint32 Unique,char* p_cOutput);
	bool isReadOnly(const char* p_cFilePath);
	bool deleteFile(const char* p_cFilePath);
	bool createDirectory(const char* p_cDirPath);
	int32 runCmd(const char* p_cCommand);
	bool fileExists(const char* p_cFilePath);
	bool makeWritable(const char* p_cFilePath);
	bool deleteDirectory(const char* p_cDirPath);
	bool createFile(const char* p_cFilePath);
	bool copyFile(const char* p_cSourcePath,const char* p_cTargetPath);
	int32 getFileSize(const char* p_cFilePath);
	uint8* readAll(const char* p_cFilename,uint32& r_uiSize);
	void makeJustDir(char* p_cFilename);
	uint64 findFirstFile(char* p_cFilename,const char* p_cWildCard = NULL);
	bool findNextFile(char* p_cFilename,uint64 iHandle);
	void findEnd(uint64 iHandle);

protected:
	enum OpenState
	{
		IS_CLOSED = 0,
		IS_READING = 1,
		IS_WRITING = 2,
	};

	OpenState m_osCurrent;
	HANDLE m_hFile;
};

} //namespace WinLib

#endif //COMMON_WINFSTREAM_H