#ifndef COMMON_OS_H
#define COMMON_OS_H

#include "libwin/log.h"

/////////////////////////////////////////////////////////////////////
class Os
/////////////////////////////////////////////////////////////////////
{
public:
	virtual void log(const char*,...) = 0;
	virtual void error(const char*,...) = 0;
};

/////////////////////////////////////////////////////////////////////
class Win32Os : public Os
/////////////////////////////////////////////////////////////////////
{
public:
	Win32Os(bool bCreateLog = false);
	virtual ~Win32Os();

	void init();
	void log(const char*,...);
	void error(const char*,...);

	const char* getArg(uint32 uIndex);
	uint32 getNumArgs();

	void setLog(Log* p_newLog);

protected:

	void setArgs();

	enum
	{
		MAX_ARGS = 32
	};

	Log* mp_log;
	char mp_cArgs[MAX_ARGS][MAX_PATH];
	uint32 m_uNumArgs;
};

/////////////////////////////////////////////////////////////////////
int32 programEntry(HINSTANCE hInstance);

extern Os* os;
extern Win32Os* win32os;

#endif //COMMON_OS_H