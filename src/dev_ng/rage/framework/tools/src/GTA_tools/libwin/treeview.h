#ifndef COMMON_TREEVIEW_H
#define COMMON_TREEVIEW_H

#include "libwin/window.h"

//////////////////////////////////////////////////////////////////////////////////////////////////
class TreeView : public Window
//////////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	TreeView();

	virtual bool init(RECT rectDim,Window* p_wndParent = NULL,uint32 hImageList = NULL);
	virtual bool update();

	void clear();
	uint64 addNode(char* p_cName,uint64 iParent = 0,uint32 iImage = 0,uint64 iData = 0);
	void remove(uint64 hItem);
	uint64 getSelected();
	void getText(uint64 hTreeItem,char* p_cText,int32 iMaxSize);
	uint64 getData(uint64 hTreeItem);

	uint64 beginDrag(POINT pnt);
	bool isDragging();
	void updateDrag(POINT pnt);
	uint64 endDrag();

protected:
	uint64 m_hImageDrag;
	bool m_bDragging;
};

#endif //COMMON_TREEVIEW_H