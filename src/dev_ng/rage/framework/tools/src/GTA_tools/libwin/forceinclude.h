#ifndef __MAX_FORCEINCLUDE_H__
#define __MAX_FORCEINCLUDE_H__

//this file is included through the forceinclude setting
//in the project so is included in every file

//we have to include max first because it defines some functions
//that are macros in the rage headers (Assert)

#if defined(_DEBUG) && defined(_M_X64)
	#include "forceinclude/win64_tooldebug.h"
#elif defined(_DEBUG)
	#include "forceinclude/win32_tooldebug.h"
#elif defined(NDEBUG) && defined(_M_X64)
	#include "forceinclude/win64_toolrelease.h"
#elif defined(NDEBUG)
	#include "forceinclude/win32_toolrelease.h"
#elif defined(_M_X64)
	#include "forceinclude/win64_toolbeta.h"
#else
	#include "forceinclude/win32_toolbeta.h"
#endif

#define COLLISION_MESH_CLASS_ID		Class_ID(0x5d2707b2, 0xb5c5d30)
#define COLLISION_BOX_CLASS_ID      Class_ID(0x7fca381e, 0x47a93bdf)
#define COLLISION_SPHERE_CLASS_ID   Class_ID(0x9e12205, 0x62dc4b42)
#define COLLISION_CAPSULE_CLASS_ID	Class_ID(0x5745733c, 0x7f435158)
#define COLLISION_CYLINDER_CLASS_ID	Class_ID(0x695d2eb9, 0x77fb6428)


#pragma warning( disable: 4800 )
#pragma warning( disable: 4265 )
#pragma warning( disable: 4263 )


#endif //__MAX_FORCEINCLUDE_H__

