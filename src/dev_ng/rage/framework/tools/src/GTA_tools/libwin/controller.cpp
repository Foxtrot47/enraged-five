#include "controller.h"
#include "libcore/exception.h"
#include "libwin/os.h"

LPDIRECTINPUT8 PCController::mp_di8 = NULL;

///////////////////////////////////////////////////////////////////////////////////////////////
void PCKeyboardController::init(HWND hWnd,HINSTANCE hInstance)
{
	HRESULT hr;

	if(!mp_di8)
	{
		hr = DirectInput8Create(hInstance,DIRECTINPUT_VERSION,IID_IDirectInput8,(void**)&mp_di8,NULL); 

		if(hr != DI_OK)
		{ 
			throw new Exception("pckeyboardcontroller:init:falied to create direct input");
		} 
	}

	hr = mp_di8->CreateDevice(GUID_SysKeyboard,&mp_did8,NULL); 

	if(hr != DI_OK)
	{
		throw new Exception("pckeyboardcontroller:init:failed create keyboard device");
	} 

	hr = mp_did8->SetDataFormat(&c_dfDIKeyboard); 

	if(hr != DI_OK) 
	{ 
		throw new Exception("pckeyboardcontroller:init:failed to set data format");
	}

	hr = mp_did8->SetCooperativeLevel(hWnd,DISCL_FOREGROUND|DISCL_NONEXCLUSIVE); 

	if(hr != DI_OK)
	{ 
		throw new Exception("pckeyboardcontroller:init:failed to set cooperative level");
	} 

	mp_did8->Acquire(); 
}

///////////////////////////////////////////////////////////////////////////////////////////////
bool PCKeyboardController::isKeyDown(uint32 uDirectInputEnum)
{
	if(m_cBuffer[uDirectInputEnum] & 0x80)
	{
		return true;
	}

	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PCKeyboardController::update()
{
	HRESULT hr;

    hr = mp_did8->GetDeviceState(sizeof(m_cBuffer),(LPVOID)&m_cBuffer); 

	if (hr != DI_OK) 
	{ 
		// If it failed, the device has probably been lost. 
		// Check for (hr == DIERR_INPUTLOST) 
		// and attempt to reacquire it here. 
		return; 
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////
void PCMouseController::init(HWND hWnd,HINSTANCE hInstance)
{
	HRESULT hr;

	m_bLeftDown = false;
	m_bLeftJustDown = false;
	m_bLeftJustUp = false;
	m_bRightDown = false;
	m_bRightJustDown = false;
	m_bRightJustUp = false;

	m_vecPos.x = 0.0f;
	m_vecPos.y = 0.0f;

	if(!mp_di8)
	{
		hr = DirectInput8Create(hInstance,DIRECTINPUT_VERSION,IID_IDirectInput8,(void**)&mp_di8,NULL); 

		if(hr != DI_OK)
		{ 
			throw new Exception("pcmousecontroller:init:falied to create direct input");
		} 
	}

	hr = mp_di8->CreateDevice(GUID_SysMouse,&mp_did8,NULL); 

	if(hr != DI_OK)
	{
		throw new Exception("pcmousecontroller:init:failed create keyboard device");
	} 

	hr = mp_did8->SetDataFormat(&c_dfDIMouse); 

	if(hr != DI_OK) 
	{ 
		throw new Exception("pcmousecontroller:init:failed to set data format");
	}

	//hr = mp_did8->SetCooperativeLevel(hWnd,DISCL_EXCLUSIVE|DISCL_FOREGROUND); 
	hr = mp_did8->SetCooperativeLevel(hWnd,DISCL_NONEXCLUSIVE|DISCL_FOREGROUND); 

	if(hr != DI_OK)
	{ 
		throw new Exception("pcmousecontroller:init:failed to set cooperative level");
	}

	m_hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	if(!m_hEvent)
	{
		throw new Exception("pccontroller:init:failed to create keyboard device\n");
	}

	if(FAILED(hr = mp_did8->SetEventNotification(m_hEvent)))
	{
		throw new Exception("pccontroller:init:failed to create keyboard device\n");
	}

	DIPROPDWORD dipdw;

    dipdw.diph.dwSize       = sizeof(DIPROPDWORD);
    dipdw.diph.dwHeaderSize = sizeof(DIPROPHEADER);
    dipdw.diph.dwObj        = 0;
    dipdw.diph.dwHow        = DIPH_DEVICE;
    dipdw.dwData            = 16;

	hr = mp_did8->SetProperty(DIPROP_BUFFERSIZE, &dipdw.diph);

	if(FAILED(hr)) 
	{
		throw new Exception("pcmousecontroller:init:failed to set property");
	}
 

	mp_did8->Acquire(); 
}

///////////////////////////////////////////////////////////////////////////////////////////////
void PCMouseController::update()
{
	m_bLeftJustDown = false;
	m_bLeftJustUp = false;
	m_bRightJustDown = false;
	m_bRightJustUp = false;

	DWORD dwResult = WaitForSingleObject(m_hEvent,0); 
	bool bDone = false;
	HRESULT hr;

	mp_did8->Acquire();

	if(dwResult == WAIT_OBJECT_0)
	{ 
		while(!bDone)
		{
			DIDEVICEOBJECTDATA od;
			DWORD dwElements = 1;

			hr = mp_did8->GetDeviceData(sizeof(DIDEVICEOBJECTDATA),&od, &dwElements, 0);

			if(FAILED(hr) || dwElements == 0) 
			{
				break;
			}

			switch(od.dwOfs) 
			{
			case DIMOFS_X:

				m_vecPos.x += ((float)((int32)od.dwData));

				break;
			case DIMOFS_Y: 

				m_vecPos.y += ((float)((int32)od.dwData));

				break;
			case DIMOFS_BUTTON0:

				if(od.dwData & 0x80) //just pressed
				{
					m_bLeftDown = true;
					m_bLeftJustDown = true;
				}
				else //just released
				{
					m_bLeftDown = false;
					m_bLeftJustUp = true;
				}

				break;
			case DIMOFS_BUTTON1:

				if(od.dwData & 0x80) //just pressed
				{
					m_bRightDown = true;
					m_bRightJustDown = true;
				}
				else //just released
				{
					m_bRightDown = false;
					m_bRightJustUp = true;
				}

				break;
			}
		}
	}
}