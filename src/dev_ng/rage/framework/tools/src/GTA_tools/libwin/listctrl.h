#ifndef COMMON_LISTCTRL_H
#define COMMON_LISTCTRL_H

#include "window.h"

//////////////////////////////////////////////////////////////////////////////////////////////////
class ListCtrl : public Window
//////////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	void clear();
	int32 appendItem(const char* p_cItem,void* p_vData = NULL);
	void setCurSelByName(const char* p_cItem);
	int32 getCurSel();
	bool getString(int32 uIndex,char* p_cString);
	void* getItemData(int32 uIndex);
};

#endif //COMMON_LISTCTRL_H