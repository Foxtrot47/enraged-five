#include <windows.h>

#pragma warning(disable:4668)
#include <commctrl.h>
#include <stdio.h>
#include <stdarg.h>
#pragma warning(error:4668)

#include "libwin/os.h"
//////////////////////////////////////////////////////////////////////////////////////////////////
Os* os = NULL;
Win32Os* win32os = NULL;

//////////////////////////////////////////////////////////////////////////////////////////////////
Win32Os::Win32Os(bool bCreateLog):
	m_uNumArgs(0),
	mp_log(NULL)
{
	if(bCreateLog)
	{
		mp_log = new Win32LogConsole;
		mp_log->init();
	}

	setArgs();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
Win32Os::~Win32Os()
{
	if(mp_log)
	{
		mp_log->shutdown();
		delete mp_log;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void Win32Os::init()
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void Win32Os::setLog(Log* p_newLog)
{
	if(mp_log)
	{
		delete mp_log;
		mp_log = NULL;
	}

	mp_log = p_newLog;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void Win32Os::log(const char* pFormat,...)
{
	char tempString[1024];
	
	va_list argptr;
	va_start(argptr, pFormat);
	vsprintf(tempString, pFormat, argptr);
	va_end(argptr);

	if(!mp_log)
	{
		return;
	}

	mp_log->printf(tempString);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void Win32Os::error(const char* pFormat,...)
{
	char tempString[1024];
	
	va_list argptr;
	va_start(argptr, pFormat);
	vsprintf(tempString, pFormat, argptr);
	va_end(argptr);

	MessageBox(NULL,tempString,"error",MB_OK);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
const char* Win32Os::getArg(uint32 uIndex)
{
	if(uIndex >= m_uNumArgs)
	{
		return NULL;
	}

	return (const char*)mp_cArgs[uIndex];
}

//////////////////////////////////////////////////////////////////////////////////////////////////
uint32 Win32Os::getNumArgs()
{
	return m_uNumArgs;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void Win32Os::setArgs()
{
	char* p_cCmdLine = GetCommandLine();
	char* p_cNextToken;
	char cBuffer[4096];
	bool bMulti = false;

//	log(p_cCmdLine);
//	log("\n");

	p_cNextToken = strtok(p_cCmdLine," ");

	cBuffer[0] = '\0';

	while(p_cNextToken && (m_uNumArgs < MAX_ARGS))
	{
		if((p_cNextToken[0] == '\"') && (p_cNextToken[strlen(p_cNextToken) - 1] == '\"'))
		{
			strcpy(mp_cArgs[m_uNumArgs++],p_cNextToken);
		}
		else if(p_cNextToken[0] == '\"')
		{
			bMulti = true;
			cBuffer[0] = '\0';
			strcat(cBuffer,p_cNextToken);
		}
		else if ((p_cNextToken[strlen(p_cNextToken) - 1] == '\"') && bMulti)
		{
			bMulti = false;
			strcat(cBuffer," ");
			strcat(cBuffer,p_cNextToken);
			strcpy(mp_cArgs[m_uNumArgs++],cBuffer);
		}
		else if (!bMulti)
		{
			strcpy(mp_cArgs[m_uNumArgs++],p_cNextToken);
		}
		else
		{
			strcat(cBuffer," ");
			strcat(cBuffer,p_cNextToken);
		}
	
		p_cNextToken = strtok(NULL," ");
	}
}

#ifndef NO_OS_STUB

//////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
int WINAPI WinMain(HINSTANCE hInstance,HINSTANCE hPrevInstance,LPSTR lpCmdLine,int nCmdShow)
{
	os = win32os = new Win32Os();
	win32os->init();

	InitCommonControls();

	int32 iRetVal = programEntry(hInstance);

	delete os;
	os = NULL;

	return iRetVal;
}

#endif //NO_OS_STUB