#include "libwin/tabs.h"

#include <windows.h>
#include <commctrl.h>

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
void TabWindow::init(Window* p_wndParent)
{
	m_hWnd = CreateWindowEx(NULL,
							WC_TABCONTROL,
							"tabs",
							WS_CHILD|TCS_MULTILINE|TCS_VERTICAL,
							0,0,0,0,
							p_wndParent->m_hWnd,
							NULL,
							GetModuleHandle(NULL),
							NULL);


}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
int32 TabWindow::addTab(const char* p_cName,int32 iNewIndex)
{
	TCITEM tcItem;

	tcItem.mask = TCIF_TEXT;
	tcItem.pszText = (char*)p_cName;
	tcItem.cchTextMax = strlen(p_cName) + 1;
	tcItem.iImage = -1;

	return SendMessage(m_hWnd,TCM_INSERTITEM,(WPARAM)iNewIndex,(LPARAM)&tcItem);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
int32 TabWindow::getCurSel()
{
	return SendMessage(m_hWnd,TCM_GETCURSEL,0,0);
}