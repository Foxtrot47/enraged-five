#include "libwin/treeview.h"
#include "libwin/os.h"

#include <windows.h>
#include <commctrl.h>

//////////////////////////////////////////////////////////////////////////////////////////////////
TreeView::TreeView():
	m_bDragging(false)
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool TreeView::init(RECT rectDim,Window* p_wndParent,uint32 hImageList)
{
	if(p_wndParent->m_hWnd)
	{
		GetClientRect(p_wndParent->m_hWnd,&rectDim);
	}

    m_hWnd = CreateWindowEx(0,
							WC_TREEVIEW,
							"ListView", 
							WS_VISIBLE|WS_CHILD|WS_BORDER|TVS_HASLINES|TVS_LINESATROOT|TVS_HASBUTTONS|TVS_EDITLABELS,
							rectDim.left,rectDim.top,
							rectDim.right,rectDim.bottom,
							p_wndParent->m_hWnd,
							NULL,
							GetModuleHandle(NULL),
							NULL);

    if(m_hWnd == NULL) 
	{
		os->error("TreeView::init:cannot create a window");
		return false;
    }

	ShowWindow(m_hWnd,SW_SHOW);

	if(hImageList)
	{
		SendMessage(m_hWnd,TVM_SETIMAGELIST,TVSIL_NORMAL,hImageList);
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool TreeView::update()
{
	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void TreeView::clear()
{
	HTREEITEM hRoot = TreeView_GetRoot(m_hWnd);

	remove((uint64)hRoot);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
uint64 TreeView::addNode(char* p_cName,uint64 uParent,uint32 uImage,uint64 uData)
{
	TVINSERTSTRUCT insert;

	insert.hParent = (HTREEITEM)uParent;
	insert.hInsertAfter = TVI_LAST;
	insert.itemex.mask = TVIF_TEXT|TVIF_IMAGE|TVIF_SELECTEDIMAGE|TVIF_STATE|TVIF_PARAM;
	insert.itemex.pszText = p_cName;
	insert.itemex.iImage = uImage;
	insert.itemex.iSelectedImage = uImage;
	insert.itemex.stateMask = TVIS_STATEIMAGEMASK;
	insert.itemex.state = 0;
	insert.itemex.lParam = (LPARAM)uData;

	uint64 uHandle = SendMessage(m_hWnd,TVM_INSERTITEM,(WPARAM)0,(LPARAM)&insert);

	SendMessage(m_hWnd,TVM_ENSUREVISIBLE,(WPARAM)0,(LPARAM)uHandle);

	return uHandle;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void TreeView::remove(uint64 hItem)
{
	SendMessage(m_hWnd,TVM_DELETEITEM,0,(LPARAM)hItem);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void TreeView::getText(uint64 hTreeItem,char* p_cText,int32 iMaxSize)
{
	TVITEM item;

	item.mask = TVIF_TEXT|TVIF_HANDLE;
	item.hItem = (HTREEITEM)hTreeItem;
	item.cchTextMax = iMaxSize;
	item.pszText = p_cText;

	SendMessage(m_hWnd,TVM_GETITEM,0,(LPARAM)&item);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
uint64 TreeView::getData(uint64 hTreeItem)
{
	TVITEM item;

	item.mask = TVIF_PARAM|TVIF_HANDLE;
	item.hItem = (HTREEITEM)hTreeItem;

	SendMessage(m_hWnd,TVM_GETITEM,0,(LPARAM)&item);

	return item.lParam;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
uint64 TreeView::getSelected()
{
	return (uint64)TreeView_GetSelection(m_hWnd);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
uint64 TreeView::beginDrag(POINT pnt)
{
	TVHITTESTINFO tv_ht ;
	ZeroMemory(&tv_ht, sizeof(TVHITTESTINFO)) ;
	tv_ht.flags = TVHT_ONITEM ;
	tv_ht.pt.x = pnt.x;
	tv_ht.pt.y = pnt.y;

	HTREEITEM hItem = (HTREEITEM)SendMessage(m_hWnd, TVM_HITTEST, 0, (LPARAM)&tv_ht) ;

	if(hItem == 0)
	{
		return 0;
	}

	TreeView_SelectItem(m_hWnd, (HTREEITEM)hItem) ;

	m_bDragging = true;

	m_hImageDrag = (uint64)TreeView_CreateDragImage(m_hWnd,(HTREEITEM)hItem);

	ImageList_BeginDrag((HIMAGELIST)m_hImageDrag, 0, 0, 0) ;

	ShowCursor(FALSE);
	SetCapture(GetParent(m_hWnd));

	ClientToScreen(m_hWnd, &pnt) ;
	ImageList_DragEnter(NULL, pnt.x, pnt.y);

	return (uint64)hItem;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool TreeView::isDragging()
{
	return m_bDragging;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void TreeView::updateDrag(POINT pnt)
{
	if(!m_bDragging)
	{
		return;
	}

	POINT pntOrig = pnt;
	RECT rectDim;

	GetClientRect(m_hWnd,&rectDim);

	ImageList_DragLeave(NULL);
	ClientToScreen(GetParent(m_hWnd), &pnt) ;

	TVHITTESTINFO tv_ht ;
	ZeroMemory(&tv_ht, sizeof(TVHITTESTINFO)) ;
	tv_ht.flags = TVHT_ONITEM ;
	tv_ht.pt.x = pnt.x;
	tv_ht.pt.y = pnt.y;
	ScreenToClient(m_hWnd, &(tv_ht.pt)) ;

	HTREEITEM h_item = (HTREEITEM)SendMessage(m_hWnd, TVM_HITTEST, 0, (LPARAM)&tv_ht) ;

	if(h_item)
	{
		if(pntOrig.y < 10)
		{
			HTREEITEM hOff = (HTREEITEM)SendMessage(m_hWnd,TVM_GETNEXTITEM,TVGN_PREVIOUSVISIBLE,(LPARAM)h_item);

			if(hOff)
			{
				SendMessage(m_hWnd, TVM_ENSUREVISIBLE,0,(LPARAM)hOff);
			}
		}
		else if((rectDim.bottom - pntOrig.y) < 10)
		{
			HTREEITEM hOff = (HTREEITEM)SendMessage(m_hWnd,TVM_GETNEXTITEM,TVGN_NEXTVISIBLE,(LPARAM)h_item);

			if(hOff)
			{
				SendMessage(m_hWnd, TVM_ENSUREVISIBLE,0,(LPARAM)hOff);
			}
		}
		else
		{
			SendMessage(m_hWnd, TVM_ENSUREVISIBLE,0,(LPARAM)h_item) ;
		}

		SendMessage(m_hWnd, TVM_SELECTITEM, TVGN_DROPHILITE, (LPARAM)h_item) ;
	}

	ImageList_DragMove(pnt.x,pnt.y) ;
	ImageList_DragEnter(NULL, pnt.x, pnt.y) ;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
uint64 TreeView::endDrag()
{
	if(!m_bDragging)
	{
		return 0;
	}

	TVHITTESTINFO tv_ht;
	TVITEM tv;

	ZeroMemory(&tv, sizeof(TVITEM)) ;
	ZeroMemory(&tv_ht, sizeof(TVHITTESTINFO)) ;

	ImageList_DragLeave(NULL) ;
	ImageList_EndDrag() ;
	ReleaseCapture();

	GetCursorPos(&(tv_ht.pt)) ;
	ScreenToClient(m_hWnd, &(tv_ht.pt)) ;

	tv_ht.flags = TVHT_ONITEM ;
	HTREEITEM h_item = (HTREEITEM)SendMessage(m_hWnd, TVM_HITTEST, 0, (LPARAM)&tv_ht) ;

	ShowCursor(TRUE);

	if(h_item)
	{	
		TreeView_SelectDropTarget(m_hWnd, NULL) ;
		TreeView_SelectItem(m_hWnd, (HTREEITEM)h_item) ;
	}

	m_bDragging = false;

	return (uint64)h_item;
}