#include <windows.h>

#include "libwin/window.h"
#include "libwin/os.h"
#include "libcore/exception.h"

//////////////////////////////////////////////////////////////////////////////////////////////////
Window::Window():
	m_hWnd(NULL)
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void Window::show()
{
	ShowWindow(m_hWnd,SW_SHOW);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void Window::hide()
{
	ShowWindow(m_hWnd,SW_HIDE);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void Window::centreToParent(HWND hWnd)
{
	HWND hParent = GetParent(hWnd); 

	if(hParent)
	{
		RECT rectParentDim;
		RECT rectDim;

		GetWindowRect(hParent,&rectParentDim);
		GetWindowRect(hWnd,&rectDim);

		rectDim.right = rectDim.right - rectDim.left;
		rectDim.bottom = rectDim.bottom - rectDim.top;
		rectDim.left = rectParentDim.left + (((rectParentDim.right - rectParentDim.left) / 2) - (rectDim.right / 2));
		rectDim.top = rectParentDim.top + (((rectParentDim.bottom - rectParentDim.top) / 2) - (rectDim.bottom / 2));

		SetWindowPos(hWnd,HWND_TOP,rectDim.left,rectDim.top,rectDim.right,rectDim.bottom,SWP_SHOWWINDOW);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void Window::createClass(const char* p_cName,uint32 menuID,WNDPROC wndproc,const char* p_cIconName)
{
	WNDCLASS wc;
	HINSTANCE hInstance = GetModuleHandle(NULL);

	wc.style         = CS_OWNDC;
	//wc.style         = CS_HREDRAW|CS_VREDRAW;
	wc.lpfnWndProc   = (WNDPROC)(wndproc ? wndproc : DefWindowProc);
	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;
	wc.hInstance     = hInstance;

	if(p_cIconName)
	{
		wc.hIcon         = LoadIcon(NULL, p_cIconName);
	}
	else
	{
		wc.hIcon         = LoadIcon(NULL, IDI_WINLOGO);
	}

	wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)CreateSolidBrush(GetSysColor(COLOR_BTNFACE));
	wc.lpszMenuName  = MAKEINTRESOURCE(menuID);
	wc.lpszClassName = p_cName;

	if(!RegisterClass(&wc)) 
	{
		throw new Exception("window::init:cannot register window class");
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
LONG WINAPI WindowProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{ 
    static PAINTSTRUCT ps;

    switch(uMsg) 
	{
    case WM_SIZE:
		PostMessage(hWnd, WM_PAINT, 0, 0);
		return 0;

    case WM_CHAR:
		switch (wParam) 
		{
		case 27:
			PostQuitMessage(0);
			break;
		}
		return 0;

    case WM_CLOSE:
		DestroyWindow(hWnd);
		return 0;
    }

    return (LONG)DefWindowProc(hWnd, uMsg, wParam, lParam); 
} 

//////////////////////////////////////////////////////////////////////////////////////////////////
StandardWindow::StandardWindow():
	m_hAccel(NULL)
{
}

void StandardWindow::setAccelTable(HACCEL hAccel)
{
	m_hAccel = hAccel;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool StandardWindow::init(const char* p_cClass,const char* p_cTitle,RECT rectDim,HWND hParent,HMENU hMenu)
{   
    m_hWnd = CreateWindow(	p_cClass, 
							p_cTitle, 
							WS_OVERLAPPEDWINDOW,
							rectDim.left,rectDim.top,
							rectDim.right,rectDim.bottom,
							hParent,
							hMenu,
							GetModuleHandle(NULL),
							NULL);

    if(m_hWnd == NULL) 
	{
		throw new Exception("window::init:cannot create a window");
    }

	ShowWindow(m_hWnd,SW_SHOW);
	UpdateWindow(m_hWnd);   

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool StandardWindow::update()
{
	MSG msg;

    if(GetMessage(&msg,NULL,0,0))
	{
		if(!TranslateAccelerator(m_hWnd,m_hAccel,&msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		return true;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool StandardWindow::updateRet()
{
	MSG msg;

	if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))
	{
		if(!TranslateAccelerator(NULL,m_hAccel,&msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		return true;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////	
void StaticText::init(RECT rectDim,const char* p_cText,Window* p_wndParent)
{
	m_hWnd = CreateWindow(	"STATIC",
							p_cText,
							WS_VISIBLE|WS_CHILD,
							rectDim.left,
							rectDim.top,
							rectDim.right,
							rectDim.bottom,
							p_wndParent->m_hWnd,
							0,
							GetModuleHandle(NULL),
							0);

}