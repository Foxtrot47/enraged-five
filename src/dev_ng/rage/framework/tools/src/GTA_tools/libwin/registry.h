#ifndef COMMON_REGISTRY_H
#define COMMON_REGISTRY_H

/////////////////////////////////////////////////////////////////////
class Registry
/////////////////////////////////////////////////////////////////////
{
public:
	Registry(const char* p_cAppName);
	~Registry();

	void setString(const char* p_cEntryName,const char* p_cValue);
	bool getString(const char* p_cEntryName,char* p_cValue);
	void getStringDefault(const char* p_cEntryName,char* p_cValue,const char* p_cDefault);

	void setInteger(const char* p_cEntryName,int32 bValue);
	bool getInteger(const char* p_cEntryName,int32& r_bValue);

protected:
	const char* mp_cAppName;
};

#endif //COMMON_REGISTRY_H