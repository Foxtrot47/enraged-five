#include "libwin/window.h"
#include "libwin/textview.h"
#include "libwin/os.h"

#include <windows.h>
#include <winuser.h>

////////////////////////////////////////////////////////////////////////////////////////
bool TextView::init(RECT rectDim,HWND hParent)
{
	if(hParent)
	{
		GetClientRect(hParent,&rectDim);
	}

	m_hWnd = CreateWindowEx(WS_EX_CLIENTEDGE,
							"EDIT", 
							"", 
							WS_HSCROLL|WS_VSCROLL|WS_CLIPSIBLINGS|WS_CLIPCHILDREN|WS_CHILD|WS_VISIBLE|ES_MULTILINE,
							rectDim.left,rectDim.top,
							rectDim.right,rectDim.bottom,
							hParent,
							NULL,
							GetModuleHandle(NULL),
							NULL);
 
    if(m_hWnd == NULL) 
	{
		char buffer[1024];

		DWORD err = GetLastError();
		FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM,0,err,0,buffer,1024,NULL);

		os->error("textview::init:cannot create a window");
		return false;
    }

	return true;
}

////////////////////////////////////////////////////////////////////////////////////////
bool TextView::update()
{
	return true;
}

////////////////////////////////////////////////////////////////////////////////////////
void TextView::addString(const char* p_cString)
{
	SendMessage(m_hWnd,EM_SETSEL,-1,-1);
	SendMessage(m_hWnd,EM_REPLACESEL,0,(LPARAM)p_cString);
}