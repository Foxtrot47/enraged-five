#ifndef COMMON_DIALOG_H
#define COMMON_DIALOG_H

#include <windows.h>

//////////////////////////////////////////////////////////////////////////////////////////////////
class Dialog
//////////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	Dialog();

	bool init(uint32 uDialogID,DLGPROC dlgProc = NULL);
	bool update();

	bool initModal(uint32 uDialogID,HWND hParent,DLGPROC dlgProc = NULL);

	HWND m_hWnd;
};

#endif //COMMON_DIALOG_H