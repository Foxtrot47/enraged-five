#include "libwin/logwindow.h"
#include "libwin/os.h"

#include <windows.h>

#pragma warning(disable:4668)
#include <commctrl.h>
#include <richedit.h>
#pragma warning(error:4668)

//////////////////////////////////////////////////////////////////////////////////////////////////
bool RichLogWindow::init(RECT rectDim,Window* p_wndParent)
{
	if(p_wndParent->m_hWnd)
	{
		GetClientRect(p_wndParent->m_hWnd,&rectDim);
	}

    m_hWnd = CreateWindowEx(0,
							RICHEDIT_CLASS, 
							"", 
							WS_VISIBLE|WS_CHILD|WS_BORDER|ES_MULTILINE|ES_OEMCONVERT|ES_READONLY|WS_VSCROLL|ES_AUTOVSCROLL,
							rectDim.left,rectDim.top,
							rectDim.right,rectDim.bottom,
							p_wndParent->m_hWnd,
							NULL,
							GetModuleHandle(NULL),
							NULL);

    if(m_hWnd == NULL) 
	{
		os->error("RichLogWindow::init:cannot create a window");
		return false;
    }

	ShowWindow(m_hWnd,SW_SHOW);

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool RichLogWindow::update()
{
	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void RichLogWindow::addLog(const char* p_cMsg)
{
	uint32 uStart,uEnd;

	SendMessage(m_hWnd,EM_SETSEL,(WPARAM)0,(LPARAM)-1);
	SendMessage(m_hWnd,EM_GETSEL,(WPARAM)&uStart,(LPARAM)&uEnd);

	while(uEnd > DEF_BUFFERSIZE)
	{
		int32 iLength = 0;
		int32 iNewLength;
		bool bProperLine = false;

		do
		{
			iNewLength = SendMessage(m_hWnd,EM_LINELENGTH,iLength,0);

			if(iNewLength == 0)
			{
				iNewLength++;
			}
			else
			{
				bProperLine = true;
			}

			iLength += iNewLength;
		}
		while(!bProperLine);

		SendMessage(m_hWnd,EM_SETSEL,(WPARAM)0,(LPARAM)iLength);
		SendMessage(m_hWnd,EM_REPLACESEL,(WPARAM)0,(LPARAM)"");
		SendMessage(m_hWnd,EM_SETSEL,(WPARAM)0,(LPARAM)-1);
		SendMessage(m_hWnd,EM_GETSEL,(WPARAM)&uStart,(LPARAM)&uEnd);
	}

	CHARFORMAT charFormat;

	charFormat.cbSize = sizeof(CHARFORMAT);
	charFormat.dwMask = CFM_SIZE;

	SendMessage(m_hWnd,EM_GETCHARFORMAT,(WPARAM)0,(LPARAM)&charFormat);

	charFormat.yHeight;

	SendMessage(m_hWnd,EM_SETSEL,-1,-1);
	SendMessage(m_hWnd,EM_REPLACESEL,(WPARAM)0,(LPARAM)p_cMsg);

	SendMessage(m_hWnd,EM_GETSEL,(WPARAM)&uStart,(LPARAM)&uEnd);
	int32 iLines = SendMessage(m_hWnd,EM_EXLINEFROMCHAR,(WPARAM)0,(LPARAM)uEnd);

	iLines -= 18;

	POINT pntScroll;

	pntScroll.x = 0;
	pntScroll.y = 16 * iLines;

	SendMessage(m_hWnd,EM_SETSCROLLPOS,(WPARAM)0,(LPARAM)&pntScroll);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////
bool LogWindow::init(RECT rectDim,Window* p_wndParent)
{
	if(p_wndParent->m_hWnd)
	{
		GetClientRect(p_wndParent->m_hWnd,&rectDim);
	}

    m_hWnd = CreateWindowEx(0,
							"EDIT", 
							"", 
							WS_VISIBLE|WS_CHILD|WS_BORDER|ES_MULTILINE|ES_READONLY|WS_VSCROLL,
							rectDim.left,rectDim.top,
							rectDim.right,rectDim.bottom,
							p_wndParent->m_hWnd,
							NULL,
							GetModuleHandle(NULL),
							NULL);

    if(m_hWnd == NULL) 
	{
		os->error("TreeView::init:cannot create a window");
		return false;
    }

	SendMessage(m_hWnd,EM_LIMITTEXT,(WPARAM)(DEF_BUFFERSIZE),0);
	ShowWindow(m_hWnd,SW_SHOW);

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool LogWindow::update()
{
	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void LogWindow::addLog(const char* p_cMsg)
{
	uint32 uEnd;

	uEnd = SendMessage(m_hWnd,WM_GETTEXTLENGTH,0,0);

	uint32 uStringLen = strlen(p_cMsg);

	if(uStringLen == 0)
	{
		return;
	}

	while((uEnd + uStringLen) >= DEF_BUFFERSIZE)
	{
		int32 iLength = 0;
		int32 iNewLength;
		bool bProperLine = false;

		do
		{
			iNewLength = SendMessage(m_hWnd,EM_LINELENGTH,iLength,0);

			if(iNewLength == 0)
			{
				iNewLength++;
			}
			else
			{
				bProperLine = true;
			}

			iLength += iNewLength;
		}
		while(!bProperLine);

		SendMessage(m_hWnd,EM_SETSEL,(WPARAM)0,(LPARAM)iLength);
		SendMessage(m_hWnd,EM_REPLACESEL,(WPARAM)0,(LPARAM)"");
		uEnd = SendMessage(m_hWnd,WM_GETTEXTLENGTH,0,0);
	}

	SendMessage(m_hWnd,EM_SETSEL,0,-1);
	SendMessage(m_hWnd,EM_SETSEL,-1,-1);
	SendMessage(m_hWnd,EM_REPLACESEL,(WPARAM)0,(LPARAM)p_cMsg);
	SendMessage(m_hWnd,EM_SCROLLCARET,(WPARAM)0,(LPARAM)0);
}