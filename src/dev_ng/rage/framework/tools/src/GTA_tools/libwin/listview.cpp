#include <windows.h>
#pragma warning(disable:4668)
#include <commctrl.h>
#pragma warning(error:4668)

#include "libwin/os.h"
#include "libwin/listview.h"

//////////////////////////////////////////////////////////////////////////////////////////////////
ListView::ListView():
	m_uCurrent(0)
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ListView::init(RECT rectDim,Window* p_wndParent,bool bAscending,int32 hImageList)
{
	if(p_wndParent)
	{
		GetClientRect(p_wndParent->m_hWnd,&rectDim);

		int32 iFlags = LVS_EDITLABELS|WS_HSCROLL|WS_VSCROLL|WS_CLIPSIBLINGS|WS_CLIPCHILDREN|WS_CHILD|LVS_REPORT|LVS_SHOWSELALWAYS|WS_VISIBLE;

		if(bAscending)
		{
			iFlags |= LVS_SORTASCENDING;
		}

		m_hWnd = CreateWindowEx(WS_EX_CLIENTEDGE,
								WC_LISTVIEW, 
								"ListView", 
								iFlags,
								rectDim.left,rectDim.top,
								rectDim.right,rectDim.bottom,
								p_wndParent->m_hWnd,
								NULL,
								GetModuleHandle(NULL),
								NULL);
	}
	else
	{
		m_hWnd = CreateWindowEx(WS_EX_CLIENTEDGE,
								WC_LISTVIEW, 
								"ListView", 
								LVS_EDITLABELS|WS_HSCROLL|WS_VSCROLL|WS_CLIPSIBLINGS|WS_CLIPCHILDREN|WS_OVERLAPPEDWINDOW|LVS_REPORT|LVS_SHOWSELALWAYS|WS_VISIBLE|LVS_SORTASCENDING,
								rectDim.left,rectDim.top,
								rectDim.right,rectDim.bottom,
								NULL,
								NULL,
								GetModuleHandle(NULL),
								NULL);
	}
 
    if(m_hWnd == NULL) 
	{
		char buffer[1024];

		DWORD err = GetLastError();
		FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM,0,err,0,buffer,1024,NULL);

		os->error("ListView::init:cannot create a window");
		return false;
    }

	if(hImageList)
	{
		SendMessage(m_hWnd,LVM_SETIMAGELIST,LVSIL_SMALL,hImageList);
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ListView::update()
{
	return true;
}

int32 g_iSortColumn;
bool g_bAscending;
bool g_bNumbers;


//////////////////////////////////////////////////////////////////////////////////////////////////
int CALLBACK CompareFunc(LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort)
{
	LVITEM itemDataA;
	LVITEM itemDataB;
	char cTextA[4096],cTextB[4096];

	itemDataA.mask = LVIF_TEXT;
	itemDataA.iItem = lParam1; 
	itemDataA.iSubItem = g_iSortColumn; 
	itemDataA.pszText = (char*)cTextA;
	itemDataA.cchTextMax = 4096;

	itemDataB.mask = LVIF_TEXT;
	itemDataB.iItem = lParam2; 
	itemDataB.iSubItem = g_iSortColumn; 
	itemDataB.pszText = (char*)cTextB;
	itemDataB.cchTextMax = 4096;

	SendMessage((HWND)lParamSort,LVM_GETITEM,0,(LPARAM)&itemDataA);
	SendMessage((HWND)lParamSort,LVM_GETITEM,0,(LPARAM)&itemDataB);

	int iRet;

	if(g_bNumbers)
	{
		float fA = (float)atof(cTextA);
		float fB = (float)atof(cTextB);

		if(fA > fB)
		{
			iRet = 1;
		}
		else if(fB > fA)
		{
			iRet = -1;
		}
		else
		{
			iRet = 0;
		}
	}
	else
	{
		iRet = strcmp(cTextA,cTextB);
	}

	if(g_bAscending)
	{
		iRet = -iRet;
	}

	return iRet;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ListView::sortItems(int32 iColumn,bool bAscending,bool bNumbers)
{
	g_iSortColumn = iColumn;
	g_bAscending = bAscending;
	g_bNumbers = bNumbers;

	SendMessage(m_hWnd,LVM_SORTITEMSEX,(LPARAM)m_hWnd,(WPARAM)CompareFunc);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ListView::addColumn(const char* p_cHeader,uint32 uWidth,int32 fmt,int iIndex)
{
	LVCOLUMN colData;
    colData.mask = LVCF_FMT|LVCF_TEXT|LVCF_WIDTH; 
    colData.fmt = fmt; 
    colData.cx = uWidth; 
    colData.pszText = (char*)p_cHeader; 
    colData.cchTextMax = strlen(p_cHeader); 
	SendMessage(m_hWnd,LVM_INSERTCOLUMN,iIndex,(LPARAM)&colData);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
int32 ListView::addItem(const char* p_cItem,int32 iImage)
{
	uint32 uCount = SendMessage(m_hWnd,LVM_GETITEMCOUNT,0,0);

	LVITEM itemData;
	itemData.mask = LVIF_TEXT|LVIF_IMAGE;
	itemData.iItem = uCount; 
	itemData.iSubItem = 0; 
	itemData.pszText = (char*)p_cItem;
	itemData.cchTextMax = strlen(p_cItem);
	itemData.iImage = iImage;

	return SendMessage(m_hWnd,LVM_INSERTITEM,0,(LPARAM) &itemData);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ListView::setItemData(uint32 uItem,void* p_vData)
{
	LVITEM itemData;
	itemData.mask = LVIF_PARAM;
	itemData.iItem = uItem; 
	itemData.iSubItem = 0; 
	itemData.lParam = (LPARAM)p_vData;

	SendMessage(m_hWnd,LVM_SETITEM,0,(LPARAM) &itemData);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void* ListView::getItemData(uint32 uItem)
{
	LVITEM itemData;
	itemData.mask = LVIF_PARAM;
	itemData.iItem = uItem; 
	itemData.iSubItem = 0; 

	SendMessage(m_hWnd,LVM_GETITEM,0,(LPARAM) &itemData);

	return (void*)itemData.lParam;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ListView::setItem(uint32 uItem,const char* p_cData,uint32 uColumn)
{
	LVITEM itemData;
	itemData.mask = LVIF_TEXT;
	itemData.iItem = uItem; 
	itemData.iSubItem = uColumn; 
	itemData.pszText = (char*)p_cData;
	itemData.cchTextMax = strlen(p_cData);

	SendMessage(m_hWnd,LVM_SETITEM,0,(LPARAM) &itemData);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
char* ListView::getItem(uint32 uItem,uint32 uColumn,char* p_cData,int32 iSize)
{
	LVITEM itemData;
	itemData.mask = LVIF_TEXT;
	itemData.iItem = uItem; 
	itemData.iSubItem = uColumn; 
	itemData.pszText = p_cData;
	itemData.cchTextMax = iSize;

	SendMessage(m_hWnd,LVM_GETITEM,0,(LPARAM) &itemData);

	return itemData.pszText;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ListView::clearList()
{
	SendMessage(m_hWnd,LVM_DELETEALLITEMS,0,0);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
uint32 ListView::getNumItems()
{
	return SendMessage(m_hWnd,LVM_GETITEMCOUNT,0,0);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
uint32 ListView::getNumItemsSelected()
{
	uint32 uSelected = 0;
	uint32 uLength = SendMessage(m_hWnd,LVM_GETITEMCOUNT,0,0);

	for(uint32 i=0;i<uLength;i++)
	{
		if(SendMessage(m_hWnd,LVM_GETITEMSTATE,i,LVIS_SELECTED))
		{
			uSelected++;
		}
	}

	return uSelected;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ListView::setItemText(uint32 uIndex,const char* p_cNewName)
{
	char cBuffer[4096];
	uint32 uLength = SendMessage(m_hWnd,LVM_GETITEMCOUNT,0,0);

	if(uIndex >= uLength)
	{
		return false;
	}

	strcpy(cBuffer,p_cNewName);

	LVITEM itemData;
	itemData.mask = LVIF_TEXT;
	itemData.iItem = uIndex; 
	itemData.iSubItem = 0; 
	itemData.pszText = cBuffer;
	itemData.cchTextMax = 4096;

	SendMessage(m_hWnd,LVM_SETITEMTEXT,uIndex,(LPARAM)&itemData);

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ListView::getItemText(uint32 uIndex,char* p_cReturnName)
{
	char cBuffer[4096];
	uint32 uLength = SendMessage(m_hWnd,LVM_GETITEMCOUNT,0,0);

	if(uIndex >= uLength)
	{
		return false;
	}

	LVITEM itemData;
	itemData.mask = LVIF_TEXT;
	itemData.iItem = uIndex; 
	itemData.iSubItem = 0; 
	itemData.pszText = cBuffer;
	itemData.cchTextMax = 4096;

	SendMessage(m_hWnd,LVM_GETITEM,0,(LPARAM)&itemData);

	strcpy(p_cReturnName,itemData.pszText);

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ListView::getNextSelected(uint32* p_iIndex)
{
	while(1)
	{
		uint32 uLength = SendMessage(m_hWnd,LVM_GETITEMCOUNT,0,0);

		if(m_uCurrent >= uLength)
		{
			m_uCurrent = 0;
			return false;
		}

		if(SendMessage(m_hWnd,LVM_GETITEMSTATE,m_uCurrent,LVIS_SELECTED))
		{
			*p_iIndex = m_uCurrent;

			m_uCurrent++;
			return true;
		}
		else
		{
			m_uCurrent++;
		}
	}	
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ListView::getNextSelected(char* p_cReturnName)
{
	char cBuffer[4096];

	while(1)
	{
		uint32 uLength = SendMessage(m_hWnd,LVM_GETITEMCOUNT,0,0);

		if(m_uCurrent == uLength)
		{
			m_uCurrent = 0;
			return false;
		}

		if(SendMessage(m_hWnd,LVM_GETITEMSTATE,m_uCurrent,LVIS_SELECTED))
		{
			LVITEM itemData;
			itemData.mask = LVIF_TEXT;
			itemData.iItem = m_uCurrent; 
			itemData.iSubItem = 0; 
			itemData.pszText = cBuffer;
			itemData.cchTextMax = 4096;

			SendMessage(m_hWnd,LVM_GETITEM,0,(LPARAM)&itemData);

			strcpy(p_cReturnName,itemData.pszText);

			m_uCurrent++;
			return true;
		}
		else
		{
			m_uCurrent++;
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ListView::removeNextSelected(uint32* p_iIndex)
{
	char cBuffer[4096];

	while(1)
	{
		uint32 uLength = SendMessage(m_hWnd,LVM_GETITEMCOUNT,0,0);

		if(m_uCurrent == uLength)
		{
			m_uCurrent = 0;
			return false;
		}

		if(SendMessage(m_hWnd,LVM_GETITEMSTATE,m_uCurrent,LVIS_SELECTED))
		{
			LVITEM itemData;
			itemData.mask = LVIF_TEXT;
			itemData.iItem = m_uCurrent; 
			itemData.iSubItem = 0; 
			itemData.pszText = cBuffer;
			itemData.cchTextMax = 4096;

			SendMessage(m_hWnd,LVM_DELETEITEM,(WPARAM)m_uCurrent,0);

			*p_iIndex = m_uCurrent;

			return true;
		}
		else
		{
			m_uCurrent++;
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ListView::removeNextSelected(char* p_cReturnName)
{
	char cBuffer[4096];

	while(1)
	{
		uint32 uLength = SendMessage(m_hWnd,LVM_GETITEMCOUNT,0,0);

		if(m_uCurrent == uLength)
		{
			m_uCurrent = 0;
			return false;
		}

		if(SendMessage(m_hWnd,LVM_GETITEMSTATE,m_uCurrent,LVIS_SELECTED))
		{
			LVITEM itemData;
			itemData.mask = LVIF_TEXT;
			itemData.iItem = m_uCurrent; 
			itemData.iSubItem = 0; 
			itemData.pszText = cBuffer;
			itemData.cchTextMax = 4096;

			SendMessage(m_hWnd,LVM_GETITEM,0,(LPARAM)&itemData);

			strcpy(p_cReturnName,itemData.pszText);

			SendMessage(m_hWnd,LVM_DELETEITEM,(WPARAM)m_uCurrent,0);

			return true;
		}
		else
		{
			*p_cReturnName = '\0';
			m_uCurrent++;
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ListView::invertSelection()
{
	LVITEM itemData;
	uint32 uLength = SendMessage(m_hWnd,LVM_GETITEMCOUNT,0,0);

	for(uint32 i=0;i<uLength;i++)
	{
		itemData.mask = LVIF_STATE;
		itemData.iItem = i; 
		itemData.iSubItem = 0; 
		itemData.stateMask = LVIS_SELECTED;

		if(SendMessage(m_hWnd,LVM_GETITEMSTATE,i,LVIS_SELECTED))
		{
			itemData.state = 0;
		}
		else
		{
			itemData.state = LVIS_SELECTED;
		}

		SendMessage(m_hWnd,LVM_SETITEMSTATE,i,(LPARAM)&itemData);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ListView::selectAll()
{
	LVITEM itemData;
	uint32 uLength = SendMessage(m_hWnd,LVM_GETITEMCOUNT,0,0);

	for(uint32 i=0;i<uLength;i++)
	{
		itemData.mask = LVIF_STATE;
		itemData.iItem = i; 
		itemData.iSubItem = 0; 
		itemData.stateMask = LVIS_SELECTED;
		itemData.state = LVIS_SELECTED;

		SendMessage(m_hWnd,LVM_SETITEMSTATE,i,(LPARAM)&itemData);
	}	
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ListView::clearSelection()
{
	LVITEM itemData;
	uint32 uLength = SendMessage(m_hWnd,LVM_GETITEMCOUNT,0,0);

	for(uint32 i=0;i<uLength;i++)
	{
		itemData.mask = LVIF_STATE;
		itemData.iItem = i; 
		itemData.iSubItem = 0; 
		itemData.stateMask = LVIS_SELECTED;
		//itemData.state = LVIS_SELECTED;
		itemData.state = 0;

		SendMessage(m_hWnd,LVM_SETITEMSTATE,i,(LPARAM)&itemData);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ListView::remove(uint32 uItem)
{
	SendMessage(m_hWnd,LVM_DELETEITEM,(WPARAM)uItem,0);

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ListView::find(const char* p_cText,bool bFromBeginning)
{
	uint32 i;
	LVITEM itemData;
	char cBuffer[MAX_PATH];
	char cCheck[MAX_PATH];

	strcpy(cCheck,p_cText);
	strupr(cCheck);

	if(bFromBeginning)
	{
		m_uCurrent = 0;
	}

	//clear the currently selected items

	uint32 numItems = getNumItems();

	for(i=0;i<numItems;i++)
	{
		itemData.mask = LVIF_STATE;
		itemData.iItem = i; 
		itemData.iSubItem = 0; 
		itemData.stateMask = LVIS_SELECTED|LVIS_FOCUSED;
		itemData.state = 0;

		SendMessage(m_hWnd,LVM_SETITEMSTATE,i,(LPARAM)&itemData);
	}

	//find the next item matching the search string

	for(i=m_uCurrent;i<numItems;i++)
	{
		itemData.mask = LVIF_TEXT;
		itemData.iItem = i; 
		itemData.iSubItem = 0; 
		itemData.pszText = cBuffer;
		itemData.cchTextMax = MAX_PATH;

		SendMessage(m_hWnd,LVM_GETITEM,0,(LPARAM)&itemData);

		strupr(cBuffer);

		if(strstr(cBuffer,cCheck))
		{
			m_uCurrent = i + 1;

			itemData.mask = LVIF_STATE;
			itemData.stateMask = LVIS_SELECTED|LVIS_FOCUSED;
			itemData.state = LVIS_SELECTED|LVIS_FOCUSED;

			SendMessage(m_hWnd,LVM_SETITEMSTATE,i,(LPARAM)&itemData);
			SendMessage(m_hWnd,LVM_ENSUREVISIBLE,i,FALSE);
		
			return true;
		}
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
int32 ListView::getIndex(const char* p_cText,bool bFromBeginning)
{
	uint32 i;
	LVITEM itemData;
	char cBuffer[MAX_PATH];
	char cCheck[MAX_PATH];

	strcpy(cCheck,p_cText);
	strupr(cCheck);

	if(bFromBeginning)
	{
		m_uCurrent = 0;
	}

	//find the next item matching the search string

	uint32 numItems = getNumItems();

	for(i=m_uCurrent;i<numItems;i++)
	{
		itemData.mask = LVIF_TEXT;
		itemData.iItem = i; 
		itemData.iSubItem = 0; 
		itemData.pszText = cBuffer;
		itemData.cchTextMax = MAX_PATH;

		SendMessage(m_hWnd,LVM_GETITEM,0,(LPARAM)&itemData);

		strupr(cBuffer);

		if(strstr(cBuffer,cCheck))
		{
			m_uCurrent = i + 1;
	
			return i;
		}
	}

	return -1;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ListView::ensureVisible(int32 uIndex)
{
	LVITEM itemData;
	char cBuffer[MAX_PATH];	

	itemData.iItem = uIndex; 
	itemData.iSubItem = 0; 
	itemData.pszText = cBuffer;
	itemData.cchTextMax = MAX_PATH;
	itemData.mask = LVIF_STATE;
	itemData.stateMask = LVIS_SELECTED|LVIS_FOCUSED;
	itemData.state = LVIS_SELECTED|LVIS_FOCUSED;

	SendMessage(m_hWnd,LVM_SETITEMSTATE,uIndex,(LPARAM)&itemData);
	SendMessage(m_hWnd,LVM_ENSUREVISIBLE,uIndex,FALSE);
}
