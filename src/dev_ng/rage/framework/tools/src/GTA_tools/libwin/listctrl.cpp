#include "listctrl.h"

//////////////////////////////////////////////////////////////////////////////////////////////////
void ListCtrl::clear()
{
	SendMessage(m_hWnd,LB_RESETCONTENT,0,0);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
int32 ListCtrl::appendItem(const char* p_cItem,void* p_vData)
{
	int32 iIndex = SendMessage(m_hWnd,LB_ADDSTRING,0,(LPARAM)p_cItem);

	SendMessage(m_hWnd,LB_SETITEMDATA,(WPARAM)iIndex,(LPARAM)p_vData);

	return iIndex;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
int32 ListCtrl::getCurSel()
{
	int32 iRet = SendMessage(m_hWnd,LB_GETCURSEL,0,0);

	if(iRet == LB_ERR)
	{
		return -1;
	}

	return iRet;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ListCtrl::setCurSelByName(const char* p_cItem)
{
	char cBuffer[4096];
	int32 iCount = SendMessage(m_hWnd,LB_GETCOUNT,(WPARAM) 0,(LPARAM) 0);

	for(int32 i=0;i<iCount;i++)
	{
		getString(i,cBuffer);

		if(stricmp(p_cItem,cBuffer)==0)
		{
			SendMessage(m_hWnd,LB_SETCURSEL,i,0);
			return;
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ListCtrl::getString(int32 uIndex,char* p_cString)
{
	int32 iRet = SendMessage(m_hWnd,LB_GETTEXT,(WPARAM) uIndex,(LPARAM) p_cString);

	if(iRet == LB_ERR)
	{
		return false;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void* ListCtrl::getItemData(int32 uIndex)
{
	int64 iRet = SendMessage(m_hWnd,LB_GETITEMDATA,(WPARAM) uIndex,0);

	if(iRet == LB_ERR)
	{
		return NULL;
	}

	return (void*)iRet;
}