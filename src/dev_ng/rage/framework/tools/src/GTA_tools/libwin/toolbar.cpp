#include "toolbar.h"

//using namespace WinLib;

#include <commctrl.h>

///////////////////////////////////////////////////////////////////////////////////////////////
void ToolBar::init(int32 iIDToolbar,int64 iIDBitmap,Window* p_wndParent)
{
#if 1
	m_hWnd = CreateWindowEx(0,
							TOOLBARCLASSNAME, 
							(LPSTR) NULL, 
							WS_CHILD|WS_VISIBLE|TBSTYLE_TOOLTIPS, 
							0, 
							0, 
							0, 
							0, 
							p_wndParent->m_hWnd, 
							(HMENU) iIDBitmap, 
							GetModuleHandle(NULL), 
							NULL); 

	SendMessage(m_hWnd, TB_BUTTONSTRUCTSIZE, (WPARAM) sizeof(TBBUTTON), 0); 

	TBBUTTON but[2];	

	but[0].iBitmap = 0;
	but[0].idCommand = 40003;
	but[0].fsState = TBSTATE_ENABLED;
	but[0].fsStyle = TBSTYLE_BUTTON;
	but[0].dwData = 0;
	but[0].iString = 0;

	but[1].iBitmap = 1;
	but[1].idCommand = 40008;
	but[1].fsState = TBSTATE_ENABLED;
	but[1].fsStyle = TBSTYLE_BUTTON;
	but[1].dwData = 0;
	but[1].iString = 1;

	SendMessage(m_hWnd, TB_ADDBUTTONS, (WPARAM) 2, (LPARAM) (LPTBBUTTON) but); 
    SendMessage(m_hWnd, TB_AUTOSIZE, 0, 0); 


#else
	TBBUTTON but[2];	

	but[0].iBitmap = 0;
	but[0].idCommand = 40003;
	but[0].fsState = TBSTATE_ENABLED;
	but[0].fsStyle = TBSTYLE_BUTTON;
	but[0].dwData = 0;
	but[0].iString = 0;

	but[1].iBitmap = 1;
	but[1].idCommand = 40008;
	but[1].fsState = TBSTATE_ENABLED;
	but[1].fsStyle = TBSTYLE_BUTTON;
	but[1].dwData = 0;
	but[1].iString = 1;

	m_hWnd = CreateToolbarEx(	p_wndParent->m_hWnd,
								WS_CHILD|WS_BORDER|WS_VISIBLE|TBSTYLE_TOOLTIPS,
								iIDBitmap,//iIDToolbar,
								2,
								GetModuleHandle(NULL),
								iIDBitmap,
								but,
								2,
								0,
								0,
								16,
								15,
								sizeof(TBBUTTON));

	SendMessage(m_hWnd,TB_SETEXTENDEDSTYLE,0,TBSTYLE_EX_MIXEDBUTTONS);
	SendMessage(m_hWnd,TB_SETMAXTEXTROWS,0,0);

	char cNameA[64];
	char cNameB[64];

	strcpy(cNameA,"Execute");
	SendMessage(m_hWnd,TB_ADDSTRING,(WPARAM)0,(LPARAM)cNameA);
	strcpy(cNameB,"Pause");
	SendMessage(m_hWnd,TB_ADDSTRING,(WPARAM)0,(LPARAM)cNameB);
#endif
}

///////////////////////////////////////////////////////////////////////////////////////////////
void ToolBar::addButton()
{

}

///////////////////////////////////////////////////////////////////////////////////////////////
void ToolBar::enableButton(int32 iIndex,bool bOn)
{
	int32 uState = SendMessage(m_hWnd,TB_GETSTATE,iIndex,0);

	if(bOn)
	{
		uState |= TBSTATE_ENABLED;
	}
	else
	{
		uState &= ~TBSTATE_ENABLED;
	}

	SendMessage(m_hWnd,TB_SETSTATE,(WPARAM)iIndex,(LPARAM)MAKELONG(uState,0));
}