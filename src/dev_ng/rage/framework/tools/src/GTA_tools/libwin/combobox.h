#ifndef COMMON_COMBOBOX_H
#define COMMON_COMBOBOX_H

#include "window.h"

//////////////////////////////////////////////////////////////////////////////////////////////////
class ComboBox : public Window
//////////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	void clear();
	int32 appendItem(const char* p_cItem,void* p_vData = NULL);
	int32 getCurSel();
	bool getString(int32 uIndex,char* p_cString);
	void* getItemData(int32 uIndex);
};

#endif //COMMON_COMBOBOX_H