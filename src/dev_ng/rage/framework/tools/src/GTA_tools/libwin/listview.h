#ifndef COMMON_LISTVIEW_H
#define COMMON_LISTVIEW_H

#pragma warning(disable:4668)
#include <commctrl.h>
#pragma warning(error:4668)
#include "window.h"

//////////////////////////////////////////////////////////////////////////////////////////////////
class ListView : public Window
//////////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	ListView();

	virtual bool init(RECT rectDim,Window* p_wndParent = NULL,bool bAscending = true,int32 hImageList = NULL);
	virtual bool update();

	void addColumn(const char* p_cHeader,uint32 uWidth,int32 fmt = LVCFMT_LEFT,int iIndex = 0);
	int32 addItem(const char* p_cItem,int32 iImage = 0);
	void setItem(uint32 uItem,const char* p_cData,uint32 uColumn);
	char* getItem(uint32 uItem,uint32 uColumn,char* p_cData,int32 iSize);
	void setItemData(uint32 uItem,void* p_vData);
	void* getItemData(uint32 uItem);
	void clearList();
	uint32 getNumItemsSelected();
	uint32 getNumItems();
	void resetCurrent() { m_uCurrent = 0; }
	bool removeNextSelected(char* p_cReturnName);
	bool removeNextSelected(uint32* p_iIndex);
	bool getNextSelected(char* p_cReturnName);
	bool getNextSelected(uint32* p_iIndex);
	bool getItemText(uint32 uIndex,char* p_cReturnName);
	bool setItemText(uint32 uIndex,const char* p_cNewName);
	void sortItems(int32 iColumn,bool bAscending,bool bNumbers);
	void invertSelection();
	void selectAll();
	void clearSelection();

	bool remove(uint32 uItem);
	bool find(const char* p_cText,bool bFromBeginning = false);
	int32 getIndex(const char* p_cText,bool bFromBeginning = false);
	void ensureVisible(int32 uIndex);

//protected:
	uint32 m_uCurrent;
};

#endif