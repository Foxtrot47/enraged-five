#include <stdio.h>

#include "libcore/bstream.h"
#include "libcore/fstream.h"
#include "libcore/mstream.h"
#include "libcore/exception.h"
#include "libcore/quaternion.h"
#include "libgta/iffload.h"
#include "libgta/cpanload.h"

using namespace WinLib;

float AnimKeySet::m_fVisTimeStep(0.02f);

BoneTree* BoneTree::mp_btCurrent = NULL;
BoneTree* BoneTree::mp_btRoot = NULL;

enum
{
	BONETAG_ROOT = 0,
	/*
	BONETAG_PELVIS = 1,
	BONETAG_SPINE_1 = 2,
	BONETAG_SPINE_2 = 3,
	BONETAG_NECK = 4,
	BONETAG_HEAD = 5,
	BONETAG_L_BROW = 6,
	BONETAG_R_BROW = 7,
	BONETAG_JAW = 8,
	BONETAG_R_CLAVICLE = 21,
	BONETAG_R_UPPERARM = 22,
	BONETAG_R_FOREARM = 23,
	BONETAG_R_HAND = 24,
	BONETAG_R_FINGER = 25,
	BONETAG_R_FINGER01 = 26,
	BONETAG_R_FINGER02 = 27,
	BONETAG_RTHUMB1 = 28,
	BONETAG_RTHUMB2 = 29,
	BONETAG_LLIP11 = 30,
	BONETAG_L_CLAVICLE = 31,
	BONETAG_L_UPPERARM = 32,
	BONETAG_L_FOREARM = 33,
	BONETAG_L_HAND = 34,
	BONETAG_L_FINGER = 35,
	BONETAG_L_FINGER01 = 36,
	BONETAG_L_FINGER02 = 37,
	BONETAG_LTHUMB1 = 38,
	BONETAG_LTHUMB2 = 39,
	BONETAG_JAW22 = 40,
	BONETAG_L_THIGH = 41,
	BONETAG_L_CALF = 42,
	BONETAG_L_FOOT = 43,
	BONETAG_L_TOE = 44,
	BONETAG_R_THIGH = 51,
	BONETAG_R_CALF = 52,
	BONETAG_R_FOOT = 53,
	BONETAG_R_TOE = 54,
	BONETAG_BELLY = 201,
	BONETAG_R_BREAST = 301,
	BONETAG_L_BREAST = 302,

	BONETAG_HAND_R_FOREARM = 2201,
	BONETAG_HAND_R_HAND = 2202,
	BONETAG_HAND_R_THUMB1 = 2203,
	BONETAG_HAND_R_THUMB2 = 2204,
	BONETAG_HAND_R_THUMB3 = 2205,
	BONETAG_HAND_R_INDEX1 = 2206,
	BONETAG_HAND_R_INDEX2 = 2207,
	BONETAG_HAND_R_INDEX3 = 2208,
	BONETAG_HAND_R_MIDDLE1 = 2209,
	BONETAG_HAND_R_MIDDLE2 = 2210,
	BONETAG_HAND_R_MIDDLE3 = 2211,
	BONETAG_HAND_R_RING1 = 2212,
	BONETAG_HAND_R_RING2 = 2213,
	BONETAG_HAND_R_RING3 = 2214,
	BONETAG_HAND_R_LITTLE1 = 2215,
	BONETAG_HAND_R_LITTLE2 = 2216,
	BONETAG_HAND_R_LITTLE3 = 2217,

	BONETAG_HAND_L_FOREARM = 3201,
	BONETAG_HAND_L_HAND = 3202,
	BONETAG_HAND_L_THUMB1 = 3203,
	BONETAG_HAND_L_THUMB2 = 3204,
	BONETAG_HAND_L_THUMB3 = 3205,
	BONETAG_HAND_L_INDEX1 = 3206,
	BONETAG_HAND_L_INDEX2 = 3207,
	BONETAG_HAND_L_INDEX3 = 3208,
	BONETAG_HAND_L_MIDDLE1 = 3209,
	BONETAG_HAND_L_MIDDLE2 = 3210,
	BONETAG_HAND_L_MIDDLE3 = 3211,
	BONETAG_HAND_L_RING1 = 3212,
	BONETAG_HAND_L_RING2 = 3213,
	BONETAG_HAND_L_RING3 = 3214,
	BONETAG_HAND_L_LITTLE1 = 3215,
	BONETAG_HAND_L_LITTLE2 = 3216,
	BONETAG_HAND_L_LITTLE3 = 3217,

	BONETAG_RBROW1 = 5001,
	BONETAG_RBROW2 = 5002,
	BONETAG_LBROW2 = 5003,
	BONETAG_LBROW1 = 5004,
	BONETAG_RLID = 5005,
	BONETAG_LLID = 5006,
	BONETAG_RTLIP1 = 5009,
	BONETAG_RTLIP2 = 5010,
	BONETAG_RTLIP3 = 5007,
	BONETAG_LTLIP1 = 5011,
	BONETAG_LTLIP2 = 5012,
	BONETAG_LTLIP3 = 5008,
	BONETAG_RCORNER = 5013,
	BONETAG_LCORNER = 5014,
	BONETAG_JAW1 = 5015,
	BONETAG_JAW2 = 5016,
	BONETAG_LLIP1 = 5017,
	BONETAG_REYE = 5018,
	BONETAG_LEYE = 5019,
	BONETAG_RCHEEK = 5020,
	BONETAG_LCHEEK = 5021,*/
};

//////////////////////////////////////////////////////////////////////////////////////////////////
BoneTree::~BoneTree()
{
	delete mp_Child;
	delete mp_Next;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
BoneTree* BoneTree::findBone(int32 iBoneID)
{
	BoneTree* p_btFind = NULL;

	if(ComponentAnim::getBoneIDFromName(m_strName.c_str()) == iBoneID)
	{
		return this;
	}

	if(mp_Child)
	{
		p_btFind = mp_Child->findBone(iBoneID);

		if(p_btFind)
		{
			return p_btFind;
		}
	}

	if(mp_Next)
	{
		p_btFind = mp_Next->findBone(iBoneID);

		if(p_btFind)
		{
			return p_btFind;
		}
	}

	return p_btFind;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void BoneTree::addChild(BoneTree* p_btChild)
{
	if(mp_Child)
	{
		mp_Child->addSibling(p_btChild);
	}
	else
	{
		mp_Child = p_btChild;
		mp_Child->mp_Parent = this;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void BoneTree::addSibling(BoneTree* p_btSibling)
{
	if(mp_Next)
	{
		mp_Next->addSibling(p_btSibling);
	}
	else
	{
		p_btSibling->mp_Parent = mp_Parent;
		mp_Next = p_btSibling;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
BoneTree* BoneTree::GetNext(BoneTree* mp_btBegin)
{
	if(mp_btBegin)
	{
		mp_btCurrent = mp_btRoot = mp_btBegin;

		return mp_btBegin;
	}

	if(mp_btCurrent == NULL)
	{
		return NULL;
	}

	if(mp_btCurrent->mp_Child)
	{
		mp_btCurrent = mp_btCurrent->mp_Child;

		return mp_btCurrent;
	}

	while(!mp_btCurrent->mp_Next)
	{
		mp_btCurrent = mp_btCurrent->mp_Parent;

		if(mp_btCurrent == mp_btRoot)
		{
			mp_btCurrent = NULL;
			mp_btRoot = NULL;

			return mp_btCurrent;
		}

		if(mp_btCurrent == NULL)
		{
			mp_btRoot = NULL;

			return mp_btCurrent;
		}
	}

	mp_btCurrent = mp_btCurrent->mp_Next;

	if(mp_btCurrent == NULL)
	{
		mp_btRoot = NULL;
	}

	return mp_btCurrent;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
struct InfoStruct
//////////////////////////////////////////////////////////////////////////////////////////////////
{
	int32 numNodes;
	int32 totalNumKeyframes;
};

//////////////////////////////////////////////////////////////////////////////////////////////////
struct AnimDataStruct
//////////////////////////////////////////////////////////////////////////////////////////////////
{
	char name[ANIM_NAME_LENGTH];
	uint32	flags;
	uint32	numFrames;
	uint32	startLoop, endLoop;
	int32	boneTag;
};

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////
bool GtaCamera::isMatch(float fA,float fB,float fTolerance)
{
	if((fA > (fB - fTolerance)) && (fA < (fB + fTolerance)))
	{
		return true;
	}

	return false;
}

/////////////////////////////////////////////////////////////////////////////////////////
bool solveQuadratic(const float a, const float b, const float c, float& xminus, float& xplus)
{
    const float alpha=b*b-4*a*c;
	if(alpha<0)
	{
		return false;
	}
	else
	{
		const float beta = sqrtf(alpha);
		xplus=0.5f*(-b+beta)/a;
		xminus=0.5f*(-b-beta)/a;
		return true;
	}	
}

/////////////////////////////////////////////////////////////////////////////////////////
bool sphereIntersect(const CVector& r_vecSpherePos,float fSphereRadius,const CVector& r_vecRayPos,const CVector& r_vecRayDir)
{
	const CVector vp = r_vecRayPos - r_vecSpherePos;
	
	const float a = 1.0f;
	const float b = DotProduct(vp,r_vecRayDir) * 2.0f;
	const float c = vp.MagnitudeSqr() - fSphereRadius * fSphereRadius;
	
	float tMinus;
	float tPlus;

	if(!solveQuadratic(a,b,c,tMinus,tPlus))
	{
	    return false;
	}
		
	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
float AnimKeySet::m_fOptCamAngle(30.0f);
float AnimKeySet::m_fOptCamDist(0.4f);

//////////////////////////////////////////////////////////////////////////////////////////////////
AnimKeySet::AnimKeySet():
	mp_rt(NULL),
	m_iType(TYPE_UNKNOWN)
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////
AnimKeySet::AnimKeySet(const AnimKeySet& r_aksIn):
	m_uNumKeys(r_aksIn.m_uNumKeys),
	m_iBoneTag(r_aksIn.m_iBoneTag),
	m_iStartLoop(r_aksIn.m_iStartLoop),
	m_iEndLoop(r_aksIn.m_iEndLoop),
	m_iFlags(r_aksIn.m_iFlags)
{
	switch(r_aksIn.m_iType)
	{
	case TYPE_ROT:
		{
			mp_r = new AnimKeyR[m_uNumKeys];
			memcpy(mp_r,r_aksIn.mp_r,sizeof(AnimKeyR) * m_uNumKeys);
			removeQuatFlips();
		}
		break;
	case TYPE_ROT_TRANS:
		{
			mp_rt = new AnimKeyRT[m_uNumKeys];
			memcpy(mp_rt,r_aksIn.mp_rt,sizeof(AnimKeyRT) * m_uNumKeys);
			removeQuatFlips();
		}
		break;
	default:
		m_iType = TYPE_UNKNOWN;
		return;
	}

	removeQuatFlips();
	m_iType = r_aksIn.m_iType;
	strcpy(mp_cName,r_aksIn.mp_cName);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
AnimKeySet::AnimKeySet(const AnimKeyRT* p_rtIn,uint32 uNumKeys):
	mp_rt(NULL)

{
	m_iType = TYPE_ROT_TRANS;
	m_uNumKeys = uNumKeys;

	mp_rt = new AnimKeyRT[m_uNumKeys];
	memcpy(mp_rt,p_rtIn,sizeof(AnimKeyRT) * m_uNumKeys);
	removeQuatFlips();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
AnimKeySet::AnimKeySet(const AnimKeyR* p_rIn,uint32 uNumKeys):
	mp_rt(NULL)
{
	m_iType = TYPE_ROT;
	m_uNumKeys = uNumKeys;

	mp_r = new AnimKeyR[m_uNumKeys];
	memcpy(mp_r,p_rIn,sizeof(AnimKeyR) * m_uNumKeys);
	removeQuatFlips();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
AnimKeySet::AnimKeySet(const OldAnimKeyRTS* p_rtsIn,uint32 uNumKeys)
{
	m_uNumKeys = uNumKeys;

	m_iType = TYPE_ROT_TRANS;

	mp_rt = new AnimKeyRT[m_uNumKeys];

	for(uint32 i=0;i<uNumKeys;i++)
	{
		mp_rt[i].pos.x = p_rtsIn[i].tx;
		mp_rt[i].pos.y = p_rtsIn[i].ty;
		mp_rt[i].pos.z = p_rtsIn[i].tz;
		mp_rt[i].time = p_rtsIn[i].time;

		CQuaternion quat(p_rtsIn[i].rx,p_rtsIn[i].ry,p_rtsIn[i].rz,p_rtsIn[i].rw);

		mp_rt[i].quat = quat;
		mp_rt[i].quat.Invert();
	}

	removeQuatFlips();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
AnimKeySet::AnimKeySet(const OldAnimKeyRT* p_rtIn,uint32 uNumKeys)
{
	m_uNumKeys = uNumKeys;

	m_iType = TYPE_ROT_TRANS;

	mp_rt = new AnimKeyRT[m_uNumKeys];

	for(uint32 i=0;i<uNumKeys;i++)
	{
		mp_rt[i].pos.x = p_rtIn[i].tx;
		mp_rt[i].pos.y = p_rtIn[i].ty;
		mp_rt[i].pos.z = p_rtIn[i].tz;
		mp_rt[i].time = p_rtIn[i].time;

		CQuaternion quat(p_rtIn[i].rx,p_rtIn[i].ry,p_rtIn[i].rz,p_rtIn[i].rw);

		mp_rt[i].quat = quat;
		mp_rt[i].quat.Invert();
	}

	removeQuatFlips();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
AnimKeySet::AnimKeySet(const OldAnimKeyR* p_rIn,uint32 uNumKeys)
{
	m_uNumKeys = uNumKeys;

	m_iType = TYPE_ROT;

	mp_r = new AnimKeyR[m_uNumKeys];

	for(uint32 i=0;i<uNumKeys;i++)
	{
		mp_r[i].time = p_rIn[i].time;

		CQuaternion quat(p_rIn[i].rx,p_rIn[i].ry,p_rIn[i].rz,p_rIn[i].rw);

		mp_r[i].quat = quat;
		mp_r[i].quat.Invert();
	}

	removeQuatFlips();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
AnimKeySet::~AnimKeySet()
{
	reset();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
int32 AnimKeySet::getWriteSize()
{
	switch(m_iType)
	{
	case TYPE_ROT:
		return sizeof(AnimKeyR) * m_uNumKeys;
	case TYPE_ROT_TRANS:
		return sizeof(AnimKeyRT) * m_uNumKeys;
	case TYPE_ROT_COMPRESSED:
		return sizeof(AnimKeyRCompressed) * m_uNumKeys;
	case TYPE_ROT_TRANS_COMPRESSED:
		return sizeof(AnimKeyRTCompressed) * m_uNumKeys;
	}

	return 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void AnimKeySet::setName(const char* p_cName)
{
	memset(mp_cName,0,ANIM_NAME_LENGTH);

	if(strlen(p_cName) >= ANIM_NAME_LENGTH)
	{
		strncpy(mp_cName,p_cName,ANIM_NAME_LENGTH);
		mp_cName[ANIM_NAME_LENGTH - 1] = '\0';
	}
	else
	{
		strcpy(mp_cName,p_cName);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void AnimKeySet::setBoneTag(int32 iBoneTag)
{
	m_iBoneTag = iBoneTag;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void AnimKeySet::removeQuatFlips()
{
	if(m_iType == TYPE_ROT)
	{
		if(m_uNumKeys < 2)
			return;
					
		for(uint32 i=1; i<m_uNumKeys; i++)
		{		
			float fCosT = DotProduct(mp_r[i - 1].quat, mp_r[i].quat);

			if(fCosT < 0.0f)
			{
				mp_r[i].quat.x = -mp_r[i].quat.x;
				mp_r[i].quat.y = -mp_r[i].quat.y;
				mp_r[i].quat.z = -mp_r[i].quat.z;
				mp_r[i].quat.w = -mp_r[i].quat.w;
			}
		}
	}

	if(m_iType == TYPE_ROT_TRANS)
	{
		if(m_uNumKeys < 2)
			return;
					
		for(uint32 i=1; i<m_uNumKeys; i++)
		{		
			float fCosT = DotProduct(mp_rt[i - 1].quat, mp_rt[i].quat);

			if(fCosT < 0.0f)
			{
				mp_rt[i].quat.x = -mp_rt[i].quat.x;
				mp_rt[i].quat.y = -mp_rt[i].quat.y;
				mp_rt[i].quat.z = -mp_rt[i].quat.z;
				mp_rt[i].quat.w = -mp_rt[i].quat.w;
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void AnimKeySet::reset()
{
	switch(m_iType)
	{
	case TYPE_ROT:
		delete[] mp_r;
		mp_r = NULL;
		break;
	case TYPE_ROT_TRANS:
		delete[] mp_rt;
		mp_rt = NULL;
		break;
	case TYPE_ROT_COMPRESSED:
		delete[] mp_crt;
		mp_crt = NULL;
		break;
	case TYPE_ROT_TRANS_COMPRESSED:
		delete[] mp_cr;
		mp_cr = NULL;
		break;
	}

	m_iType = TYPE_UNKNOWN;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void AnimKeySet::cullTimeRot(float fStartTime,float fEndTime)
{
	//figure out how many frames we will have after the cull

	uint32 uNewKeys = 0;
	uint32 i;

	bool bOverStart = false;
	bool bOverEnd = false;

	if(m_uNumKeys < 2)
	{
		return;
	}

	if((mp_r[0].time >= fStartTime) && (mp_r[0].time <= fEndTime))
	{
		uNewKeys++;
		bOverStart = true;
	}

	if((fStartTime < mp_r[m_uNumKeys - 1].time) && (fEndTime > mp_r[m_uNumKeys - 1].time))
	{
		uNewKeys++;
		bOverEnd = true;
	}

	for(i=0;i<m_uNumKeys;i++)
	{
		if(mp_r[i].time < fStartTime)
		{
			uNewKeys++;
		}

		if(mp_r[i].time > fEndTime)
		{
			uNewKeys++;
		}
	}

	if(uNewKeys == m_uNumKeys)
	{
		return;
	}

	//allocate them

	AnimKeyR* p_rNew = new AnimKeyR[uNewKeys];
	uint32 iMaxNewsKeys = uNewKeys;
	
	//fill them
	uNewKeys = 0;

	bool bFirstEndKey = true;

	for(i=0;i<m_uNumKeys;i++)
	{
		if(mp_r[i].time < fStartTime)
		{
			Assert(uNewKeys < iMaxNewsKeys);
			p_rNew[uNewKeys] = mp_r[i];
			uNewKeys++;
		}
		else 
		{
			if(bOverEnd)
			{
				Assert(uNewKeys < iMaxNewsKeys);
				p_rNew[uNewKeys] = mp_r[i];
				p_rNew[uNewKeys].time = fEndTime;
				uNewKeys++;
				bOverEnd = false;
			}
		}

		if(mp_r[i].time > fEndTime)
		{
			if(bFirstEndKey)
			{
				if(uNewKeys > 0)
				{
					p_rNew[uNewKeys - 1].quat = mp_r[i].quat;
				}

				bFirstEndKey = false;
			}

			if(bOverStart)
			{
				Assert(uNewKeys < iMaxNewsKeys);
				p_rNew[uNewKeys] = mp_r[i];
				p_rNew[uNewKeys].time = fStartTime;
				uNewKeys++;		
				bOverStart = false;
			}

			Assert(uNewKeys < iMaxNewsKeys);
			p_rNew[uNewKeys] = mp_r[i];
			uNewKeys++;
		}
		
	}

	m_uNumKeys = uNewKeys;
	delete[] mp_r;
	mp_r = p_rNew;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void AnimKeySet::cullTimeRotTrans(float fStartTime,float fEndTime)
{
	//figure out how many frames we will have after the cull

	uint32 uNewKeys = 0;
	uint32 i;

	bool bOverStart = false;
	bool bOverEnd = false;

	if(m_uNumKeys < 2)
	{
		return;
	}

	if((mp_rt[0].time >= fStartTime) && (mp_rt[0].time <= fEndTime))
	{
		uNewKeys++;
		bOverStart = true;
	}

	if((fStartTime < mp_rt[m_uNumKeys - 1].time) && (fEndTime > mp_rt[m_uNumKeys - 1].time))
	{
		uNewKeys++;
		bOverEnd = true;
	}

	for(i=0;i<m_uNumKeys;i++)
	{
		if(mp_rt[i].time < fStartTime)
		{
			uNewKeys++;
		}

		if(mp_rt[i].time > fEndTime)
		{
			uNewKeys++;
		}
	}

	if(uNewKeys == m_uNumKeys)
	{
		return;
	}

	//allocate them

	AnimKeyRT* p_rNew = new AnimKeyRT[uNewKeys];
	uint32 iMaxNewsKeys = uNewKeys;
	
	AnimKeyRT frameSave = mp_rt[0];

	//fill them
	uNewKeys = 0;

	bool bFirstEndKey = true;

	for(i=0;i<m_uNumKeys;i++)
	{
		if(mp_rt[i].time < fStartTime)
		{
			Assert(uNewKeys < iMaxNewsKeys);
			p_rNew[uNewKeys] = mp_rt[i];

			frameSave = mp_rt[i];

			uNewKeys++;
		}
		else 
		{
			if(bOverEnd)
			{
				Assert(uNewKeys < iMaxNewsKeys);
				p_rNew[uNewKeys] = mp_rt[i];
				p_rNew[uNewKeys].time = fEndTime;
				uNewKeys++;
				bOverEnd = false;
			}
		}

		if(mp_rt[i].time > fEndTime)
		{
			/*
			if(bFirstEndKey)
			{
				if(uNewKeys > 0)
				{
					p_rNew[uNewKeys - 1].pos = mp_rt[i].pos;
					p_rNew[uNewKeys - 1].quat = mp_rt[i].quat;
				}

				bFirstEndKey = false;
			}*/

			if(bOverStart)
			{
				Assert(uNewKeys < iMaxNewsKeys);
				p_rNew[uNewKeys] = mp_rt[i];
				p_rNew[uNewKeys].time = fStartTime;
				uNewKeys++;		
				bOverStart = false;
			}

			Assert(uNewKeys < iMaxNewsKeys);
			p_rNew[uNewKeys] = mp_rt[i];

			if(bFirstEndKey)
			{
				p_rNew[uNewKeys].pos = frameSave.pos;
				p_rNew[uNewKeys].quat = frameSave.quat;
				bFirstEndKey = false;
			}

			uNewKeys++;
		}
		
	}

	m_uNumKeys = uNewKeys;
	delete[] mp_rt;
	mp_rt = p_rNew;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool AnimKeySet::appendRot(const AnimKeySet& r_aksAppend,float fAddTime)
{
	AnimKeyR* p_r = new AnimKeyR[m_uNumKeys + r_aksAppend.m_uNumKeys];

	memcpy(p_r,mp_r,sizeof(AnimKeyR) * m_uNumKeys);
	memcpy(p_r + m_uNumKeys,r_aksAppend.mp_r,sizeof(AnimKeyR) * r_aksAppend.m_uNumKeys);

	int32 iNewNumKeys = m_uNumKeys + r_aksAppend.m_uNumKeys;

	for(int32 i = m_uNumKeys;i < iNewNumKeys;i++)
	{
		p_r[i].time += fAddTime;
	}

	delete[] mp_r;
	mp_r = p_r;
	m_uNumKeys = iNewNumKeys;

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool AnimKeySet::appendRotTrans(const AnimKeySet& r_aksAppend,float fAddTime)
{
	AnimKeyRT* p_rt = new AnimKeyRT[m_uNumKeys + r_aksAppend.m_uNumKeys];

	memcpy(p_rt,mp_rt,sizeof(AnimKeyRT) * m_uNumKeys);
	memcpy(p_rt + m_uNumKeys,r_aksAppend.mp_rt,sizeof(AnimKeyRT) * r_aksAppend.m_uNumKeys);

	int32 iNewNumKeys = m_uNumKeys + r_aksAppend.m_uNumKeys;

	for(int32 i = m_uNumKeys;i < iNewNumKeys;i++)
	{
		p_rt[i].time += fAddTime;
	}

	delete[] mp_rt;
	mp_rt = p_rt;
	m_uNumKeys = iNewNumKeys;

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool AnimKeySet::append(const AnimKeySet& r_aksAppend,float fAddTime)
{
	if(isCompressed() || r_aksAppend.isCompressed()) 
	{
		return false;
	}

	AnimKeySet aksCopy(r_aksAppend);

	if(aksCopy.m_iType != m_iType)
	{
		if(aksCopy.hasTrans())
		{
			CMatrix matTrans;
			aksCopy.getMatrixAtKey(0,matTrans);

			addTrans(matTrans.GetTranslate());
		}
		else
		{
			CMatrix matTrans;
			getMatrixAtKey(m_uNumKeys - 1,matTrans);

			aksCopy.addTrans(matTrans.GetTranslate());
		}
	}

	switch(m_iType)
	{
	case TYPE_ROT:
		return appendRot(aksCopy,fAddTime);
	case TYPE_ROT_TRANS:
		return appendRotTrans(aksCopy,fAddTime);
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void AnimKeySet::cullTime(float fStartTime,float fEndTime)
{
	switch(m_iType)
	{
	case TYPE_ROT:
		cullTimeRot(fStartTime,fEndTime);
		break;
	case TYPE_ROT_TRANS:
		cullTimeRotTrans(fStartTime,fEndTime);
		break;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void AnimKeySet::subTimeRot(float fStartTime,float fEndTime)
{
	if(m_uNumKeys < 2)
	{
		return;
	}

	//cull time either side of this

	cullTimeRot(fEndTime,mp_r[m_uNumKeys - 1].time);

	if(m_uNumKeys > 0)
	{
		if(fStartTime > mp_r[0].time)
		{
			cullTimeRot(mp_r[0].time,fStartTime);
		}
	}

	//move the times down

	for(uint32 i=0;i<m_uNumKeys;i++)
	{
		mp_r[i].time -= fStartTime;
	}

	if(m_uNumKeys > 0)
	{
		mp_r[0].time = 0.0f;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void AnimKeySet::subTimeRotTrans(float fStartTime,float fEndTime)
{
	if(m_uNumKeys < 2)
	{
		return;
	}

	//cull time either side of this

	cullTimeRotTrans(fEndTime,mp_rt[m_uNumKeys - 1].time);

	if(m_uNumKeys > 0)
	{
		if(fStartTime > mp_rt[0].time)
		{
			cullTimeRotTrans(mp_rt[0].time,fStartTime);
		}
	}

	//move the times down

	for(uint32 i=0;i<m_uNumKeys;i++)
	{
		mp_rt[i].time -= fStartTime;
	}

	if(m_uNumKeys > 0)
	{
		mp_rt[0].time = 0.0f;
	}

	if(m_uNumKeys > 2)
	{
		if((mp_rt[1].time - mp_rt[0].time) < 0.1f)
		{
			mp_rt[1].time = mp_rt[0].time + ((mp_rt[2].time - mp_rt[0].time) / 2.0f);
			mp_rt[0].pos.x += 10.0f;
			mp_rt[0].pos.y += 10.0f;
			mp_rt[0].pos.z += 10.0f;
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void AnimKeySet::subTime(float fStartTime,float fEndTime)
{
	if(m_uNumKeys < 1)
	{
		return;
	}

	switch(m_iType)
	{
	case TYPE_ROT:
		subTimeRot(fStartTime,fEndTime);
		break;
	case TYPE_ROT_TRANS:
		subTimeRotTrans(fStartTime,fEndTime);
		break;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void AnimKeySet::startAndEndRot(float fStartTime,float fEndTime)
{
	if(mp_r[m_uNumKeys - 1].time >= fEndTime)
	{
		return;
	}

	AnimKeyR* p_r = new AnimKeyR[m_uNumKeys + 1];
	memcpy(p_r,mp_r,sizeof(AnimKeyR) * m_uNumKeys);

	p_r[m_uNumKeys] = mp_r[m_uNumKeys - 1];
	p_r[m_uNumKeys].time = fEndTime;
	m_uNumKeys++;

	delete[] mp_r;
	mp_r = p_r;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void AnimKeySet::startAndEndRotTrans(float fStartTime,float fEndTime)
{
	if(mp_rt[m_uNumKeys - 1].time >= fEndTime)
	{
		return;
	}

	AnimKeyRT* p_rt = new AnimKeyRT[m_uNumKeys + 1];
	memcpy(p_rt,mp_rt,sizeof(AnimKeyRT) * m_uNumKeys);

	p_rt[m_uNumKeys] = mp_rt[m_uNumKeys - 1];
	p_rt[m_uNumKeys].time = fEndTime;
	m_uNumKeys++;

	delete[] mp_rt;
	mp_rt = p_rt;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void AnimKeySet::startAndEnd(float fStartTime,float fEndTime)
{
	if(m_uNumKeys < 1)
	{
		return;
	}

	switch(m_iType)
	{
	case TYPE_ROT:
		startAndEndRot(fStartTime,fEndTime);
		break;
	case TYPE_ROT_TRANS:
		startAndEndRotTrans(fStartTime,fEndTime);
		break;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool AnimKeySet::isVisibleRot(bool bRootCheck,std::list<AnimKeySet*>& r_llParents,const GtaCamera& cam,float fTimeA,float fTimeB,float fRadius)
{
	float fFOV;
	CMatrix matTrans;
	CMatrix matTemp;

	//find first key

	uint32 iFirstKey = 0;
	uint32 iLastKey = 0;

	while(mp_r[iFirstKey].time < fTimeA)
	{
		iFirstKey++;

		if(iFirstKey == m_uNumKeys)
		{
			return true;
		}
	}

	iLastKey = iFirstKey;

	//find last key

	while(mp_r[iLastKey].time < fTimeB)
	{
		iLastKey++;

		if(iLastKey == (m_uNumKeys - 1))
		{
			break;
		}
	}

	iLastKey++;

	CVector vecActualPos,vecActualDir,vecActualUp,vecActualRight;
	CVector vecDir;

	vecActualUp = CVector(0.0f,0.0f,1.0f);

	float fDistance;

	CVector vecToObj;

	for(uint32 i = iFirstKey;i < iLastKey;i++)
	{
		Assert(i < m_uNumKeys);

		matTrans.SetUnity();

		std::list<AnimKeySet*>::iterator begin = r_llParents.begin();
		std::list<AnimKeySet*>::iterator end = r_llParents.end();

		while(begin != end)
		{
			(*begin)->getMatrixAtTime(mp_r[i].time,matTemp);

			matTrans = matTemp * matTrans;

			begin++;
		}		

		CVector vecPos = CVector(0.0f,0.0f,0.0f);

		cam.getAtTime(mp_r[i].time,vecActualPos,vecActualDir,fFOV);
		vecActualDir.Normalise();

		vecToObj = vecPos - (vecActualPos - (m_fOptCamDist * vecActualDir));
		fDistance = vecToObj.Magnitude();
		vecToObj.Normalise();

		if((fDistance > 1.5f) && !bRootCheck)
		{
			return true;
		}

		if(DotProduct(vecToObj,vecActualDir) >= 0.0f)
		{
			vecActualRight = CrossProduct(vecActualDir,vecActualUp);

			vecDir = vecActualDir;
			vecDir.Normalise();

			if(sphereIntersect(vecPos,(fFOV / m_fOptCamAngle) * fRadius * (1.1f + fDistance),vecActualPos,vecActualDir))
			{
				return true;
			}
		}
	}
		
	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool AnimKeySet::isVisibleRotTrans(bool bRootCheck,std::list<AnimKeySet*>& r_llParents,const GtaCamera& cam,float fTimeA,float fTimeB,float fRadius)
{	
	float fFOV;
	CMatrix matTrans;
	CMatrix matTemp;

	//find first key

	uint32 iFirstKey = 0;
	uint32 iLastKey = 0;

	while(mp_rt[iFirstKey].time < fTimeA)
	{
		iFirstKey++;

		if(iFirstKey == m_uNumKeys)
		{
			return true;
		}
	}

	iLastKey = iFirstKey;

	//find last key

	while(mp_rt[iLastKey].time < fTimeB)
	{
		if(iLastKey >= (m_uNumKeys - 1))
		{
			break;
		}

		iLastKey++;
	}

	iLastKey++;

	CVector vecActualPos,vecActualDir,vecActualUp,vecActualRight;
	CVector vecDir;

	vecActualUp = CVector(0.0f,0.0f,1.0f);

	float fDistance;

	CVector vecToObj;

	for(uint32 i = iFirstKey;i < iLastKey;i++)
	{
		Assert(i < m_uNumKeys);

		matTrans.SetUnity();

		std::list<AnimKeySet*>::iterator begin = r_llParents.begin();
		std::list<AnimKeySet*>::iterator end = r_llParents.end();

		while(begin != end)
		{
			(*begin)->getMatrixAtTime(mp_rt[i].time,matTemp);

			matTrans = matTemp * matTrans;

			begin++;
		}		

		CVector vecPos = matTrans * mp_rt[i].pos;

		cam.getAtTime(mp_rt[i].time,vecActualPos,vecActualDir,fFOV);
		vecActualDir.Normalise();

		vecToObj = vecPos - (vecActualPos - (m_fOptCamDist * vecActualDir));
		fDistance = vecToObj.Magnitude();
		vecToObj.Normalise();

		if((fDistance > 1.5f) && !bRootCheck)
		{
			return true;
		}

		if(DotProduct(vecToObj,vecActualDir) >= 0.0f)
		{
			vecActualRight = CrossProduct(vecActualDir,vecActualUp);

			vecDir = vecActualDir;
			vecDir.Normalise();

			if(sphereIntersect(vecPos,(fFOV / m_fOptCamAngle) * fRadius * (1.0f + fDistance),vecActualPos,vecActualDir))
			{
				return true;
			}
		}
	}
		
	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool AnimKeySet::isVisible(bool bRootCheck,std::list<AnimKeySet*>& r_llParents,const GtaCamera& cam,float fTimeA,float fTimeB,float fRadius)
{
	if(m_uNumKeys < 2)
	{
		return true;
	}

	switch(m_iType)
	{
	case TYPE_ROT:
		return isVisibleRot(bRootCheck,r_llParents,cam,fTimeA,fTimeB,fRadius);
		break;
	case TYPE_ROT_TRANS:
		return isVisibleRotTrans(bRootCheck,r_llParents,cam,fTimeA,fTimeB,fRadius);
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool AnimKeySet::hasTrans() const
{
	switch(m_iType)
	{
	case TYPE_ROT_TRANS_COMPRESSED:
		return true;
	case TYPE_ROT_TRANS:
		return true;
	}	

	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool AnimKeySet::isCompressed() const
{
	switch(m_iType)
	{
	case TYPE_ROT_COMPRESSED:
		return true;
	case TYPE_ROT_TRANS_COMPRESSED:
		return true;
	}	

	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool AnimKeySet::canCompress() const
{
	uint32 i;

	switch(m_iType)
	{
	case TYPE_ROT:
		for(i=0;i<m_uNumKeys;i++)
		{
			if(!mp_r[i].canCompress())
			{
				return false;
			}
		}
		break;
	case TYPE_ROT_TRANS:
		for(i=0;i<m_uNumKeys;i++)
		{
			if(!mp_rt[i].canCompress())
			{
				return false;
			}
		}
		break;
	case TYPE_ROT_COMPRESSED:
		return true;
		break;
	case TYPE_ROT_TRANS_COMPRESSED:
		return true;
		break;
	}	

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool AnimKeySet::removeTrans()
{
	if(m_iType != TYPE_ROT_TRANS)
	{
		return false;
	}

	AnimKeyR* mp_newR = new AnimKeyR[m_uNumKeys];

	for(uint32 i=0;i<m_uNumKeys;i++)
	{
		mp_newR[i].quat = mp_rt[i].quat;
		mp_newR[i].time = mp_rt[i].time;
	}

	delete[] mp_rt;
	mp_r = mp_newR;

	m_iType = TYPE_ROT;

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
float AnimKeySet::lerp(float fBeforeVal,float fBeforeTime,float fAfterVal,float fAfterTime,float fCurrentTime)
{
	float fTD1 = fAfterTime - fBeforeTime;
	float fVD1 = fAfterVal - fBeforeVal;
	float fTDX = fCurrentTime - fBeforeTime;

	float fVX = ((fTDX * fVD1) / fTD1) + fBeforeVal;
	
	return fVX;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
CVector AnimKeySet::lerp(const CVector& vecBeforeVal,float fBeforeTime,const CVector& vecAfterVal,float fAfterTime,float fCurrentTime)
{
	CVector vecRet;

	vecRet[0] = lerp(vecBeforeVal.x,fBeforeTime,vecAfterVal.x,fAfterTime,fCurrentTime);
	vecRet[1] = lerp(vecBeforeVal.y,fBeforeTime,vecAfterVal.y,fAfterTime,fCurrentTime);
	vecRet[2] = lerp(vecBeforeVal.z,fBeforeTime,vecAfterVal.z,fAfterTime,fCurrentTime);

	return vecRet;
}

const float AnimKeySet::m_fCutGap(0.1f);

//////////////////////////////////////////////////////////////////////////////////////////////////
void AnimKeySet::cameraCutsRot(const GtaCamera& r_cam)
{
	Assert(false);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void AnimKeySet::cameraCutsRotTrans(const GtaCamera& r_cam)
{
	int32 iIndex = -1;
	uint32 iOldKey = 0;
	uint32 iNewKey = 0;
	uint32 iNewNumKeys = m_uNumKeys;

	//find out how may keys we will need

	while(r_cam.getNextCut(iIndex))
	{
		while(mp_rt[iOldKey].time < r_cam.mp_posTimes[iIndex])
		{
			iOldKey++;

			if(iOldKey >= m_uNumKeys)
			{
				break;
			}
		}

		if(iOldKey >= m_uNumKeys)
		{
			break;
		}

		if(mp_rt[iOldKey].time == r_cam.mp_posTimes[iIndex])
		{
			iOldKey++;
			iNewNumKeys += 1;
		}
		else if(((r_cam.mp_posTimes[iIndex] - mp_rt[iOldKey - 1].time) < m_fCutGap) &&
				 (mp_rt[iOldKey].time - r_cam.mp_posTimes[iIndex + 1]) < m_fCutGap)
		{
			iOldKey++;
		}
		else
		{
			iNewNumKeys += 2;
		}
	}

	if(iIndex == -1)
	{
		return;
	}

	if(iNewNumKeys == m_uNumKeys)
	{
		return;
	}

	//create data for the new keys

	AnimKeyRT* p_rt = new AnimKeyRT[iNewNumKeys];

	iOldKey = 0;
	iIndex = -1;

	//write the new keys with cuts

	while(r_cam.getNextCut(iIndex))
	{
		//write all the keys up to the point of the cut

		while(mp_rt[iOldKey].time < r_cam.mp_posTimes[iIndex])
		{
//			Assert(iNewKey < iNewNumKeys);
//			Assert(iOldKey < m_uNumKeys);

			if(iOldKey >= m_uNumKeys)
			{
				break;
			}

			if(iNewKey >= iNewNumKeys)
			{
				break;
			}

			p_rt[iNewKey] = mp_rt[iOldKey];
			iNewKey++;
			iOldKey++;
		}

		if(iNewKey >= iNewNumKeys)
		{
			break;
		}

		if(iOldKey >= m_uNumKeys)
		{
			break;
		}

		//write the cut

		if(mp_rt[iOldKey].time == r_cam.mp_posTimes[iIndex])
		{
			Assert(iNewKey < iNewNumKeys);
			p_rt[iNewKey] = mp_rt[iOldKey];
			iNewKey++;
			iOldKey++;

			if(iOldKey >= m_uNumKeys)
			{
				Assert(iNewKey < iNewNumKeys);
				p_rt[iNewKey] = mp_rt[iOldKey - 1];
				p_rt[iNewKey].time = r_cam.mp_posTimes[iIndex + 1];
				iNewKey++;
			}
			else
			{
				Assert(iNewKey < iNewNumKeys);
				p_rt[iNewKey] = mp_rt[iOldKey];
				p_rt[iNewKey].time = r_cam.mp_posTimes[iIndex + 1];
				iNewKey++;
			}
		}
		else if(((r_cam.mp_posTimes[iIndex] - mp_rt[iOldKey - 1].time) < m_fCutGap) &&
				 (mp_rt[iOldKey].time - r_cam.mp_posTimes[iIndex + 1]) < m_fCutGap)
		{
			Assert(iNewKey < iNewNumKeys);
			p_rt[iNewKey - 1].time = r_cam.mp_posTimes[iIndex];

			if(iOldKey >= m_uNumKeys)
			{
				Assert(iNewKey < iNewNumKeys);
				p_rt[iNewKey] = mp_rt[iOldKey - 1];
				p_rt[iNewKey].time = r_cam.mp_posTimes[iIndex + 1];
				iNewKey++;
			}
			else
			{
				Assert(iNewKey < iNewNumKeys);
				p_rt[iNewKey] = mp_rt[iOldKey];
				p_rt[iNewKey].time = r_cam.mp_posTimes[iIndex + 1];
				iNewKey++;
				iOldKey++;
			}
		}
		else
		{
			Assert(iNewKey < iNewNumKeys);
			p_rt[iNewKey] = p_rt[iNewKey - 1];
			p_rt[iNewKey].time = r_cam.mp_posTimes[iIndex];
			iNewKey++;

			if(iOldKey >= m_uNumKeys)
			{
				Assert(iNewKey < iNewNumKeys);
				p_rt[iNewKey] = mp_rt[iOldKey - 1];
				p_rt[iNewKey].time = r_cam.mp_posTimes[iIndex + 1];
				iNewKey++;
			}
			else
			{
				Assert(iNewKey < iNewNumKeys);
				p_rt[iNewKey] = mp_rt[iOldKey];
				p_rt[iNewKey].time = r_cam.mp_posTimes[iIndex + 1];
				iNewKey++;
			}
		}
	}

	//write any keys that are left after the final cut
	for(uint32 i=iOldKey;i<m_uNumKeys;i++)
	{
		Assert(iNewKey < iNewNumKeys);
		p_rt[iNewKey] = mp_rt[i];
		iNewKey++;
	}

	delete[] mp_rt;
	mp_rt = p_rt;
	m_uNumKeys = iNewNumKeys;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void AnimKeySet::cameraCuts(const GtaCamera& r_cam)
{
	if(m_uNumKeys < 2)
	{
		return;
	}

	switch(m_iType)
	{
	case TYPE_ROT:
		cameraCutsRot(r_cam);
		break;
	case TYPE_ROT_TRANS:
		cameraCutsRotTrans(r_cam);
		break;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool AnimKeySet::addTrans(CVector r_vecPos)
{
	if(m_iType != TYPE_ROT)
	{
		return false;
	}

	AnimKeyRT* mp_newRT = new AnimKeyRT[m_uNumKeys];

	for(uint32 i=0;i<m_uNumKeys;i++)
	{
		mp_newRT[i].pos = r_vecPos;
		mp_newRT[i].quat = mp_r[i].quat;
		mp_newRT[i].time = mp_r[i].time;
	}

	delete[] mp_r;
	mp_rt = mp_newRT;

	m_iType = TYPE_ROT_TRANS;

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void AnimKeySet::removeRedundantTrans()
{
	if(m_iType != TYPE_ROT_TRANS)
	{
		return;
	}

	if(m_uNumKeys < 2)
	{
		return;
	}

	bool bRedundant = true;

	float fFirstX,fFirstY,fFirstZ;
	float fCompX,fCompY,fCompZ;

	fFirstX = mp_rt[0].pos.x;
	fFirstY = mp_rt[0].pos.y;
	fFirstZ = mp_rt[0].pos.z;

	for(uint32 i=1;i<m_uNumKeys;i++)
	{
		fCompX = mp_rt[i].pos.x;
		fCompY = mp_rt[i].pos.y;
		fCompZ = mp_rt[i].pos.z;

		if((!GtaCamera::isMatch(fFirstX,fCompX)) || (!GtaCamera::isMatch(fFirstY,fCompY)) || (!GtaCamera::isMatch(fFirstZ,fCompZ)))
		{
			bRedundant = false;
			break;
		}
	}

	if(!bRedundant)
	{
		return;
	}

	removeTrans();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void AnimKeySet::compress()
{
	if(m_iType == TYPE_ROT)
	{
		m_iType = TYPE_ROT_COMPRESSED;
		AnimKeyRCompressed* p_cr = new AnimKeyRCompressed[m_uNumKeys];

		for(uint32 i=0;i<m_uNumKeys;i++)
		{
			p_cr[i].m_fDeltaTime = CompressKeyframeTimeFloat(mp_r[i].time);
			CompressQuaternion(p_cr[i].m_quatRotation,mp_r[i].quat);
		}

		delete[] mp_r;
		mp_cr = p_cr;
	}

	if(m_iType == TYPE_ROT_TRANS)
	{
		m_iType = TYPE_ROT_TRANS_COMPRESSED;
		AnimKeyRTCompressed* p_crt = new AnimKeyRTCompressed[m_uNumKeys];

		for(uint32 i=0;i<m_uNumKeys;i++)
		{
			p_crt[i].m_fDeltaTime = CompressKeyframeTimeFloat(mp_rt[i].time);
			CompressQuaternion(p_crt[i].m_quatRotation,mp_rt[i].quat);
			CompressAnimTranslationVector(p_crt[i].m_vecTranslation,mp_rt[i].pos);
		}

		delete[] mp_rt;
		mp_crt = p_crt;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void AnimKeySet::uncompress()
{
	if(m_iType == TYPE_ROT_COMPRESSED)
	{
		m_iType = TYPE_ROT;
		AnimKeyR* p_r = new AnimKeyR[m_uNumKeys];

		for(uint32 i=0;i<m_uNumKeys;i++)
		{
			p_r[i].time = DecompressKeyframeTimeFloat(mp_cr[i].m_fDeltaTime);
			DecompressQuaternion(p_r[i].quat,mp_cr[i].m_quatRotation);
		}

		delete[] mp_cr;
		mp_r = p_r;
	}

	if(m_iType == TYPE_ROT_TRANS_COMPRESSED)
	{
		m_iType = TYPE_ROT_TRANS;
		AnimKeyRT* p_rt = new AnimKeyRT[m_uNumKeys];

		for(uint32 i=0;i<m_uNumKeys;i++)
		{
			p_rt[i].time = DecompressKeyframeTimeFloat(mp_crt[i].m_fDeltaTime);
			DecompressQuaternion(p_rt[i].quat,mp_crt[i].m_quatRotation);
			DecompressAnimTranslationVector(p_rt[i].pos,mp_crt[i].m_vecTranslation);
		}

		delete[] mp_crt;
		mp_rt = p_rt;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
float AnimKeySet::getMaxTime()
{
	switch(m_iType)
	{
	case TYPE_ROT_TRANS_COMPRESSED:
		return mp_crt[m_uNumKeys - 1].m_fDeltaTime;

	case TYPE_ROT_COMPRESSED:
		return mp_cr[m_uNumKeys - 1].m_fDeltaTime;

	case TYPE_ROT_TRANS:
		return mp_rt[m_uNumKeys - 1].time;

	case TYPE_ROT:
		return mp_r[m_uNumKeys - 1].time;
	}

	return 0.0f;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool AnimKeySet::loadOptimised(BinaryStream& r_streamIn)
{
	uint32 i;
		
	reset();

	r_streamIn.read(mp_cName,ANIM_NAME_LENGTH);
	m_iType = (AnimKeyType)r_streamIn.readU32();
	m_uNumKeys = r_streamIn.readU32();
	m_iBoneTag = r_streamIn.readU32();

	switch(m_iType)
	{
	case TYPE_ROT_TRANS_COMPRESSED:

		mp_crt = new AnimKeyRTCompressed[m_uNumKeys];

		for(i=0;i<m_uNumKeys;i++)
		{
			mp_crt[i].loadOptimised(r_streamIn);
		}

		break;

	case TYPE_ROT_COMPRESSED:

		mp_cr = new AnimKeyRCompressed[m_uNumKeys];

		for(i=0;i<m_uNumKeys;i++)
		{
			mp_cr[i].loadOptimised(r_streamIn);
		}

		break;
	case TYPE_ROT_TRANS:

		mp_rt = new AnimKeyRT[m_uNumKeys];

		for(i=0;i<m_uNumKeys;i++)
		{
			mp_rt[i].loadOptimised(r_streamIn);
		}

		break;

	case TYPE_ROT:

		mp_r = new AnimKeyR[m_uNumKeys];

		for(i=0;i<m_uNumKeys;i++)
		{
			mp_r[i].loadOptimised(r_streamIn);
		}

		break;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool AnimKeySet::saveOptimised(BinaryStream& r_streamOut)
{
	uint32 i;

	r_streamOut.write(mp_cName,ANIM_NAME_LENGTH);
	r_streamOut.writeU32(m_iType);
	r_streamOut.writeU32(m_uNumKeys);
	r_streamOut.writeU32(m_iBoneTag);

	switch(m_iType)
	{
	case TYPE_ROT_TRANS_COMPRESSED:
		for(i=0;i<m_uNumKeys;i++)
		{
			mp_crt[i].saveOptimised(r_streamOut);
		}

		break;

	case TYPE_ROT_COMPRESSED:
		for(i=0;i<m_uNumKeys;i++)
		{
			mp_cr[i].saveOptimised(r_streamOut);
		}

		break;
	case TYPE_ROT_TRANS:
		for(i=0;i<m_uNumKeys;i++)
		{
			mp_rt[i].saveOptimised(r_streamOut);
		}

		break;

	case TYPE_ROT:
		for(i=0;i<m_uNumKeys;i++)
		{
			mp_r[i].saveOptimised(r_streamOut);
		}

		break;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void AnimKeySet::getMatrixAtKey(int32 iKey,CMatrix& r_mat)
{
	switch(m_iType)
	{
	case TYPE_ROT_TRANS:
		{
			r_mat.SetUnity();
			r_mat.SetRotate(mp_rt[iKey].quat);
			r_mat.SetTranslate(mp_rt[iKey].pos);
		}

		break;

	case TYPE_ROT:
		{
			r_mat.SetUnity();
			r_mat.SetRotate(mp_r[iKey].quat);
		}

		break;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void AnimKeySet::getMatrixAtTime(float fTime,CMatrix& r_mat)
{
	uint32 i;

	switch(m_iType)
	{
	case TYPE_ROT_TRANS:
		{
			for(i = 0;i<m_uNumKeys;i++)
			{
				if(mp_rt[i].time >= fTime)
				{
					break;
				}
			}

			r_mat.SetUnity();
			r_mat.SetRotate(mp_rt[i].quat);
			r_mat.SetTranslate(mp_rt[i].pos);
		}

		break;

	case TYPE_ROT:
		{
			for(i = 0;i<m_uNumKeys;i++)
			{
				if(mp_r[i].time >= fTime)
				{
					break;
				}
			}

			r_mat.SetUnity();
			r_mat.SetRotate(mp_r[i].quat);
		}

		break;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////
bool OldAnimKeyRTS::canCompress() const
{
	if(!CanCompressQuaternion(CQuaternion(rx,ry,rz,rw)))
	{
		return false;
	}

	if(!CanCompressAnimTranslationVector(CVector(tx,ty,tz)))
	{
		return false;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////
bool OldAnimKeyRT::canCompress() const
{
	if(!CanCompressQuaternion(CQuaternion(rx,ry,rz,rw)))
	{
		return false;
	}

	if(!CanCompressAnimTranslationVector(CVector(tx,ty,tz)))
	{
		return false;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////
bool OldAnimKeyR::canCompress() const
{
	if(!CanCompressQuaternion(CQuaternion(rx,ry,rz,rw)))
	{
		return false;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////
bool AnimKeyRT::canCompress() const
{
	if(!CanCompressQuaternion(quat))
	{
		return false;
	}

	if(!CanCompressAnimTranslationVector(pos))
	{
		return false;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool AnimKeyRT::saveOptimised(BinaryStream& r_streamOut)
{
	r_streamOut.write(this,sizeof(AnimKeyRT));

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////
bool AnimKeyR::canCompress() const
{
	if(!CanCompressQuaternion(quat))
	{
		return false;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool AnimKeyR::saveOptimised(BinaryStream& r_streamOut)
{
	r_streamOut.write(this,sizeof(AnimKeyR));

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////
bool AnimKeyRTCompressed::saveOptimised(BinaryStream& r_streamOut)
{
	r_streamOut.write(this,sizeof(AnimKeyRTCompressed));

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////
bool AnimKeyRCompressed::saveOptimised(BinaryStream& r_streamOut)
{
	r_streamOut.write(this,sizeof(AnimKeyRCompressed));

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////
bool AnimKeyRT::loadOptimised(BinaryStream& r_streamIn)
{
	r_streamIn.read(this,sizeof(AnimKeyRT));

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////
bool AnimKeyR::loadOptimised(BinaryStream& r_streamIn)
{
	r_streamIn.read(this,sizeof(AnimKeyR));

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////
bool AnimKeyRTCompressed::loadOptimised(BinaryStream& r_streamIn)
{
	r_streamIn.read(this,sizeof(AnimKeyRTCompressed));

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////
bool AnimKeyRCompressed::loadOptimised(BinaryStream& r_streamIn)
{
	r_streamIn.read(this,sizeof(AnimKeyRCompressed));

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////
ComponentAnimSet::ComponentAnimSet(const char* pBlockName):
	m_iNumAnims(0)
{
	if(pBlockName)
	{
		strcpy(m_cBlockName,pBlockName);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
ComponentAnimSet::~ComponentAnimSet()
{
	reset();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ComponentAnimSet::addAnim(ComponentAnim* pComponentAnim)
{
	mpp_ComponentAnims[m_iNumAnims] = pComponentAnim;
	m_iNumAnims++;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ComponentAnimSet::reset()
{
	for(int32 i=0;i<m_iNumAnims;i++)
	{
		delete mpp_ComponentAnims[i];
		mpp_ComponentAnims[i] = NULL;
	}

	m_iNumAnims = 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ComponentAnimSet::load(BinaryStream& r_streamIn,bool bCompress)
{
	char cHeader[4];
	uint32 uiSize;
	uint32 uiReadIn;

	reset();

	r_streamIn.read(cHeader,4);
	uiSize = r_streamIn.readU32();

	if(strncmp(cHeader,"ANPK",4) == 0)
	{
		r_streamIn.read(cHeader,4);
		uiReadIn = r_streamIn.readU32();
		m_iNumAnims = r_streamIn.readU32();

		uiReadIn += 3;
		uiReadIn>>=2;
		uiReadIn<<=2;
		uiReadIn -= 4;

		memset(m_cBlockName,0,ANIM_NAME_LENGTH);
		r_streamIn.read(m_cBlockName,uiReadIn);

		for(int32 i=0;i<m_iNumAnims;i++)
		{
			mpp_ComponentAnims[i] = new ComponentAnim;

			r_streamIn.read(cHeader,4);
			uiReadIn = r_streamIn.readU32();

			uiReadIn += 3;
			uiReadIn >>= 2;
			uiReadIn <<= 2;

			r_streamIn.read(mpp_ComponentAnims[i]->mp_cName,uiReadIn);

			mpp_ComponentAnims[i]->load(r_streamIn,bCompress);
		}

		return true;
	}
	else if(strncmp(cHeader,"ANP2",4) == 0)
	{
		r_streamIn.read(m_cBlockName,ANIM_NAME_LENGTH);
		m_iNumAnims = r_streamIn.readU32();
		
		for(int32 i=0;i<m_iNumAnims;i++)
		{
			mpp_ComponentAnims[i] = new ComponentAnim;
			mpp_ComponentAnims[i]->loadOptimisedVer2(r_streamIn);
		}

		return true;
	}
	else if(strncmp(cHeader,"ANP3",4) == 0)
	{
		r_streamIn.read(m_cBlockName,ANIM_NAME_LENGTH);
		m_iNumAnims = r_streamIn.readU32();
		
		for(int32 i=0;i<m_iNumAnims;i++)
		{
			mpp_ComponentAnims[i] = new ComponentAnim;
			mpp_ComponentAnims[i]->loadOptimisedVer3(r_streamIn);
		}

		return true;
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ComponentAnimSet::save(BinaryStream& r_streamOut)
{
	struct AnpkInfo
	{
		int32 iNumAnims;
		char cName[64];
	};

	AnpkInfo anpkInfo;

	anpkInfo.iNumAnims = m_iNumAnims;
	strcpy(anpkInfo.cName,m_cBlockName);

	IffBlock* p_iffRoot = new IffBlock("ANPK",0,NULL);
	IffBlock* p_iffInfo = new IffBlock("INFO",sizeof(int32) + strlen(anpkInfo.cName) + 1,&anpkInfo);
	
	p_iffRoot->addBlock(p_iffInfo);

	for(int32 i=0;i<m_iNumAnims;i++)
	{
		char* p_cName = (char*)mpp_ComponentAnims[i]->getName();

		IffBlock* p_iffNewName = new IffBlock("NAME",strlen(p_cName) + 1,p_cName);

		p_iffRoot->addBlock(p_iffNewName);

		IffBlock* p_iffNewAnim = mpp_ComponentAnims[i]->getForSave();

		p_iffRoot->addBlock(p_iffNewAnim);
	}

	p_iffRoot->save(r_streamOut);

	delete p_iffRoot;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ComponentAnim::removeRedundantTrans(BoneTree* p_btRoot)
{
	AnimKeySet* p_aksCheck = NULL;

	for(int32 i=0;i<m_iNumNodes;i++)
	{
		p_aksCheck = mp_aksNodes[i];

		if(p_aksCheck->m_iBoneTag != BONETAG_ROOT)
		{
			if(!(p_btRoot && (stricmp(p_btRoot->m_strName.c_str(),p_aksCheck->mp_cName) == 0)))
			{
				p_aksCheck->removeRedundantTrans();
			}
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
AnimKeySet* ComponentAnim::findSetByName(const char* p_cName)
{
	for(int32 i = 0;i<m_iNumNodes;i++)
	{
		if(strnicmp(mp_aksNodes[i]->mp_cName,p_cName,ANIM_NAME_LENGTH) == 0)
		{
			return mp_aksNodes[i];
		}
	}

	return NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ComponentAnim::cameraCuts(const char* p_cCameraFilename)
{
	GtaCamera cam;

	if(!cam.load(p_cCameraFilename))
	{
		return;
	}

	for(int32 i=0;i<m_iNumNodes;i++)
	{
		mp_aksNodes[i]->cameraCuts(cam);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ComponentAnim::optimiseToCamera(BoneTree* p_btRoot,const char* p_cCameraFilename,float fRadius)
{
	bool bIsRoot = true;
	int32 i;
	std::list<AnimKeySet*> llParents;

	if(!p_btRoot)
	{
		return false;
	}

	if(p_btRoot->mp_Parent != NULL)
	{
		bIsRoot = false;
	}

	//load the camera

	GtaCamera cam;

	if(!cam.load(p_cCameraFilename))
	{
		return false;
	}

	//find the bone data we want to check against
	
	AnimKeySet* p_aksCheck = NULL;

	for(i=0;i<m_iNumNodes;i++)
	{
		if(stricmp(mp_aksNodes[i]->mp_cName,p_btRoot->m_strName.c_str()) == 0)
		{
			p_aksCheck = mp_aksNodes[i];
			break;
		}
	}

	if(!p_aksCheck)
	{
		return false;
	}

	//find the parent nodes

	BoneTree* p_btCurr = p_btRoot->mp_Parent;

	while(p_btCurr)
	{
		AnimKeySet* p_aksAdd = NULL;

		for(i=0;i<m_iNumNodes;i++)
		{
			if(stricmp(mp_aksNodes[i]->mp_cName,p_btCurr->m_strName.c_str()) == 0)
			{
				p_aksAdd = mp_aksNodes[i];
				break;
			}
		}

		//Assert(p_aksAdd);
		
		if(!p_aksAdd)
		{
			return false;
		}

		llParents.push_back(p_aksAdd);

		p_btCurr = p_btCurr->mp_Parent;
	}

	//we have a bone so see what times this object is off screen
	
	float fCurTime = 0.0f;
	float fNextTime = AnimKeySet::m_fVisTimeStep;
	float fMaxTime = cam.maxTime();
	int32 uNumSteps = (int32) (fMaxTime / AnimKeySet::m_fVisTimeStep);
	int32 uNumCull = 0;
	float* cullStart = new float[uNumSteps];
	float* cullEnd = new float[uNumSteps];
	float timeA,timeB;

	while(fNextTime < fMaxTime)
	{
		timeA = fCurTime;
		timeB = fNextTime;

		fCurTime = fNextTime;
		fNextTime += AnimKeySet::m_fVisTimeStep;
		
		if(!p_aksCheck->isVisible(bIsRoot,llParents,cam,timeA,timeB,fRadius))
		{
			if((uNumCull > 0) && (cullStart[uNumCull - 1] != 0.0f) && (timeA == cullEnd[uNumCull - 1]))
			{
				Assert((uNumCull - 1) < uNumSteps);

				cullEnd[uNumCull - 1] = timeB;
			}
			else
			{
				Assert(uNumCull < uNumSteps);

				cullStart[uNumCull] = timeA;
				cullEnd[uNumCull] = timeB;
				uNumCull++;
			}
		}
	}

	//and cull the frames from the animation
	//(but only from those nodes that are children
	//in the bone tree)

	for(i=0;i<uNumCull;i++)
	{
		if(cullStart[i] > 0.0f)
		{
			BoneTree* p_btCurr = BoneTree::GetNext(p_btRoot);
		
			while(p_btCurr)
			{
				AnimKeySet* p_aks = findSetByName(p_btCurr->m_strName.c_str());

				if(p_aks)
				{
					cullTime(p_aks,cullStart[i],cullEnd[i]);
				}

				p_btCurr = BoneTree::GetNext();
			}
		}
	}

	delete[] cullStart;
	delete[] cullEnd;

	if(uNumCull > 0)
	{
		return true;
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ComponentAnim::cullTime(AnimKeySet* p_aks,float fStartTime,float fEndTime,bool bLeadInOut)
{
	if(!p_aks)
	{
		return;
	}

	if(bLeadInOut)
	{
		fStartTime += (AnimKeySet::m_fVisTimeStep * 5.0f);
		fEndTime -= (AnimKeySet::m_fVisTimeStep * 5.0f);

		if(fStartTime >= fEndTime)
		{
			return;
		}
	}

	p_aks->cullTime(fStartTime,fEndTime);
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ComponentAnim::cullTimeAll(float fStartTime,float fEndTime,bool bLeadInOut)
{
	if(bLeadInOut)
	{
		fStartTime += (AnimKeySet::m_fVisTimeStep * 5.0f);
		fEndTime -= (AnimKeySet::m_fVisTimeStep * 5.0f);

		if(fStartTime >= fEndTime)
		{
			return;
		}
	}

	for(int32 i = 0;i<m_iNumNodes;i++)
	{
		mp_aksNodes[i]->cullTime(fStartTime,fEndTime);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ComponentAnim::subTime(float fStartTime,float fEndTime)
{
	//leave some lead in and out

	if(fStartTime >= fEndTime)
	{
		return;
	}

	for(int32 i = 0;i<m_iNumNodes;i++)
	{
		mp_aksNodes[i]->subTime(fStartTime,fEndTime);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ComponentAnimSet::saveOptimisedVer2(BinaryStream& r_streamOut)
{
	int32 i;

	//find out the size of memory we will need to store
	//the optimised block

	//use a temporary memory stream to fix up the data
	MemoryStream mStream(16 * 1024 * 1024);

	mStream.write(m_cBlockName,ANIM_NAME_LENGTH);
	mStream.writeU32(m_iNumAnims);

	for(i=0;i<m_iNumAnims;i++)
	{
		//save it
		mpp_ComponentAnims[i]->saveOptimisedVer2(mStream);
	}

	//write out the block of memory
	IffBlock* p_iffRoot = new IffBlock("ANP2",mStream.getPending(),mStream.getRead(),false);
	p_iffRoot->save(r_streamOut);
	delete p_iffRoot;

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ComponentAnimSet::saveOptimisedVer3(BinaryStream& r_streamOut)
{
	int32 i;

	//find out the size of memory we will need to store
	//the optimised block

	//use a temporary memory stream to fix up the data
	MemoryStream mStream(4 * 1024 * 1024);

	mStream.write(m_cBlockName,ANIM_NAME_LENGTH);
	mStream.writeU32(m_iNumAnims);

	for(i=0;i<m_iNumAnims;i++)
	{
		//save it
		mpp_ComponentAnims[i]->saveOptimisedVer3(mStream);
	}

	//write out the block of memory
	IffBlock* p_iffRoot = new IffBlock("ANP3",mStream.getPending(),mStream.getRead(),false);
	p_iffRoot->save(r_streamOut);
	delete p_iffRoot;

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////
ComponentAnim::ComponentAnim():
	m_iNumNodes(0),
	m_iFlags(0)
{
}

//////////////////////////////////////////////////////////////////////////////////////////////////
ComponentAnim::~ComponentAnim()
{
	reset();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ComponentAnim::setName(const char* p_cName)
{
	if(strlen(p_cName) >= ANIM_NAME_LENGTH)
	{
		strncpy(mp_cName,p_cName,ANIM_NAME_LENGTH);
		mp_cName[ANIM_NAME_LENGTH - 1] = '\0';

		printf("error: animation name exceeds 23 character limit\n");
		getchar();
	}
	else
	{
		strcpy(mp_cName,p_cName);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ComponentAnim::reset()
{
	for(int i=0;i<m_iNumNodes;i++)
	{
		delete mp_aksNodes[i];
	}
		
	m_iNumNodes = 0;
	m_iFlags = 0;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ComponentAnim::addAnimKeySet(AnimKeySet* p_aksAdd)
{
	if(m_iNumNodes == MAX_NODES)
	{
		return false;
	}

	mp_aksNodes[m_iNumNodes++] = p_aksAdd;

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
float ComponentAnim::getMaxTime()
{
	float fMaxTime = 0.0f;

	for(int32 i = 0;i< m_iNumNodes;i++)
	{
		float fNewTime = mp_aksNodes[i]->getMaxTime();

		if(fNewTime > fMaxTime)
		{
			fMaxTime = fNewTime;
		}
	}

	return fMaxTime;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ComponentAnim::startAndEnd()
{
	float fMaxTime = getMaxTime();

	for(int32 i = 0;i< m_iNumNodes;i++)
	{
		mp_aksNodes[i]->startAndEnd(0.0f,fMaxTime);
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ComponentAnim::append(const ComponentAnim& r_caAdd)
{
	float fAddTime = getMaxTime() + (1.0f / 60.0f);

	//make sure each animkey set in the one to add is in
	//this one
	
	AnimKeySet* p_aksAdd;
	AnimKeySet* p_aksCompare;

	for(int32 i = 0;i<r_caAdd.m_iNumNodes;i++)
	{
		p_aksAdd = r_caAdd.mp_aksNodes[i];
		p_aksCompare = findSetByName(p_aksAdd->mp_cName);

		if(!p_aksCompare)
		{
			return false;
		}

//		if(p_aksCompare->m_iType != p_aksAdd->m_iType)
//		{
//			return false;
//		}

		if(p_aksCompare->m_iBoneTag != p_aksAdd->m_iBoneTag)
		{
			return false;
		}

		p_aksAdd++;
	}

	//if we get here then we should be able to append each one
	for(i = 0;i<r_caAdd.m_iNumNodes;i++)
	{
		p_aksAdd = r_caAdd.mp_aksNodes[i];
		p_aksCompare = findSetByName(p_aksAdd->mp_cName);
		p_aksCompare->append(*p_aksAdd,fAddTime);

		if((int32)p_aksCompare->m_uNumKeys > m_iTotalKeyFrames)
		{
			m_iTotalKeyFrames = p_aksCompare->m_uNumKeys;
		}
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ComponentAnim::load(BinaryStream& r_streamIn,bool bCompress)
{
	IffBlock* p_iffbRoot;

	reset();

	try
	{
		IffBlock* p_iffbData;

		p_iffbRoot = new IffBlock(r_streamIn);

		if(strnicmp(p_iffbRoot->getTagName(),"DGAN",4) != 0)
		{
			throw new Exception();
		}

		p_iffbData = p_iffbRoot->getBlock(0);

		if(strnicmp(p_iffbData->getTagName(),"INFO",4) != 0)
		{
			throw new Exception();
		}

		InfoStruct* p_isInfo = (InfoStruct*)p_iffbData->getData();

		m_iTotalKeyFrames = p_isInfo->totalNumKeyframes;

		for(int32 i=0;i<p_isInfo->numNodes;i++)
		{
			IffBlock* p_iffbData = p_iffbRoot->getBlock(i + 1);

			if(!p_iffbData)
			{
				throw new Exception();
			}

			if(strnicmp(p_iffbData->getTagName(),"CPAN",4) != 0)
			{
				throw new Exception();
			}

			p_iffbData->readBlocks();

			int32 iNumBlocks = p_iffbData->getNumBlocks();

			if(iNumBlocks != 2)
			{
				throw new Exception();
			}

			IffBlock* p_iffbAnim = p_iffbData->getBlock(0);

			if(strnicmp(p_iffbAnim->getTagName(),"ANIM",4) != 0)
			{
				throw new Exception();
			}

			AnimDataStruct* p_adAnim = (AnimDataStruct*)p_iffbAnim->getData();

			IffBlock* p_iffbFrames = p_iffbData->getBlock(1);
			AnimKeySet* p_animKey;

			if(strnicmp(p_iffbFrames->getTagName(),"KRTS",4) == 0)
			{
				p_animKey = new AnimKeySet((OldAnimKeyRTS*)p_iffbFrames->getData(),p_iffbFrames->getSize() / sizeof(OldAnimKeyRTS));
			}
			else if(strnicmp(p_iffbFrames->getTagName(),"KRT0",4) == 0)
			{
				p_animKey = new AnimKeySet((OldAnimKeyRT*)p_iffbFrames->getData(),p_iffbFrames->getSize() / sizeof(OldAnimKeyRT));
			}
			else if(strnicmp(p_iffbFrames->getTagName(),"KR00",4) == 0)
			{
				p_animKey = new AnimKeySet((OldAnimKeyR*)p_iffbFrames->getData(),p_iffbFrames->getSize() / sizeof(OldAnimKeyR));
			}
			else
			{
				throw new Exception();
			}

			if(p_iffbAnim->getSize() != sizeof(AnimDataStruct))
			{
				p_animKey->setBoneTag(-1);
			}
			else
			{
				p_animKey->setBoneTag(p_adAnim->boneTag);
			}

			//p_animKey->m_uNumKeys = p_adAnim->numFrames;
			p_animKey->m_iStartLoop = p_adAnim->startLoop;
			p_animKey->m_iEndLoop = p_adAnim->endLoop;
			p_animKey->m_iFlags = p_adAnim->flags;

			p_animKey->setName(p_adAnim->name);

			mp_aksNodes[m_iNumNodes] = p_animKey;
			m_iNumNodes++;
		}

		calcWriteSize();

		if(bCompress)
		{
			tryAndCompress();
		}

		delete p_iffbRoot;
	}
	catch(Exception* p_ex)
	{
		reset();
		delete p_ex;
		delete p_iffbRoot;
		return false;
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ComponentAnim::saveDebug(const char* p_cFilename)
{
	AnimKeySet* p_aksCurrent;
	FileStream* fsDebug = WinLib::FileStream::m_cbCreateStream();
	fsDebug->open(p_cFilename,"wctb");

	int32 iTotalCount = 0;

	for(int32 i=0;i<m_iNumNodes;i++)
	{
		p_aksCurrent = mp_aksNodes[i];

		iTotalCount++;
	}

	fsDebug->writeLine("number of nodes: %d",iTotalCount);	

	for(i=0;i<m_iNumNodes;i++)
	{
		p_aksCurrent = mp_aksNodes[i];

		fsDebug->writeLine("============================");
		fsDebug->writeLine("============================");
		fsDebug->writeLine("name: %s",p_aksCurrent->mp_cName);
		fsDebug->writeLine("boneid: %d",p_aksCurrent->m_iBoneTag);
		fsDebug->writeLine("numkeys: %d",p_aksCurrent->m_uNumKeys);
//		fsDebug->writeLine("startloop: %d",p_aksCurrent->m_uStartLoop);
//		fsDebug->writeLine("endloop: %d",p_aksCurrent->m_uEndLoop);
//		fsDebug->writeLine("flags: %d",p_aksCurrent->m_uFlags);
		fsDebug->writeLine("============================");

		switch(p_aksCurrent->m_iType)
		{
		case AnimKeySet::TYPE_ROT_TRANS:
			{
				for(uint32 j=0;j<p_aksCurrent->m_uNumKeys;j++)
				{
					AnimKeyRT* p_rt = p_aksCurrent->mp_rt;

					fsDebug->writeLine("time: %f pos: %f %f %f rot: %f %f %f %f",p_rt[j].time,
																				p_rt[j].pos.x,
																				p_rt[j].pos.y,
																				p_rt[j].pos.z,
																				p_rt[j].quat.x,
																				p_rt[j].quat.y,
																				p_rt[j].quat.z,
																				p_rt[j].quat.w);
				}
			}

			break;

		case AnimKeySet::TYPE_ROT:
			{
				for(uint32 j=0;j<p_aksCurrent->m_uNumKeys;j++)
				{
					AnimKeyR* p_r = p_aksCurrent->mp_r;

					fsDebug->writeLine("time: %f rot: %f %f %f %f",	p_r[j].time,
																	p_r[j].quat.x,
																	p_r[j].quat.y,
																	p_r[j].quat.z,
																	p_r[j].quat.w);
				}
			}

			break;

		}

		fsDebug->writeLine("============================");
		fsDebug->writeLine("============================");
	}

	fsDebug->close();
	delete fsDebug;

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
IffBlock* ComponentAnim::getForSave()
{
	uint32 j;
	IffBlock* p_iffbRoot = new IffBlock("DGAN",0,NULL,false);

	InfoStruct* p_isInfo = new InfoStruct;
	p_isInfo->numNodes = m_iNumNodes;
	p_isInfo->totalNumKeyframes = m_iTotalKeyFrames;

	IffBlock* p_iffbInfo = new IffBlock("INFO",sizeof(InfoStruct),(char*)p_isInfo);
	p_iffbRoot->addBlock(p_iffbInfo);

	AnimKeySet* p_aksCurrent;

	for(int32 i=0;i<m_iNumNodes;i++)
	{
		p_aksCurrent = mp_aksNodes[i];

		IffBlock* p_iffbCpan = new IffBlock("CPAN",0,NULL,false);

		AnimDataStruct* p_adInfo = new AnimDataStruct;

		p_adInfo->flags = 0;
		p_adInfo->boneTag = p_aksCurrent->m_iBoneTag;
		p_adInfo->startLoop = p_aksCurrent->m_iStartLoop;
		p_adInfo->endLoop = p_aksCurrent->m_iEndLoop;
		p_adInfo->numFrames = p_aksCurrent->m_uNumKeys;
		p_adInfo->flags = p_aksCurrent->m_iFlags;

		memset(p_adInfo->name,0,ANIM_NAME_LENGTH);
		strcpy(p_adInfo->name,p_aksCurrent->mp_cName);

		IffBlock* p_iffbAnim;

		p_iffbAnim = new IffBlock("ANIM",sizeof(AnimDataStruct),(char*)p_adInfo);

		IffBlock* p_iffbData;

		switch(p_aksCurrent->m_iType)
		{
		case AnimKeySet::TYPE_ROT:
			{
				OldAnimKeyR* p_save = new OldAnimKeyR[p_aksCurrent->m_uNumKeys];

				for(j=0;j<p_aksCurrent->m_uNumKeys;j++)
				{
					p_save[j].time = p_aksCurrent->mp_r[j].time;
					p_save[j].rx = -p_aksCurrent->mp_r[j].quat.x;
					p_save[j].ry = -p_aksCurrent->mp_r[j].quat.y;
					p_save[j].rz = -p_aksCurrent->mp_r[j].quat.z;
					p_save[j].rw = p_aksCurrent->mp_r[j].quat.w;
				}

			//	p_isInfo->totalNumKeyframes += p_aksCurrent->m_uNumKeys * 4;
				p_iffbData = new IffBlock("KR00",sizeof(OldAnimKeyR) * p_aksCurrent->m_uNumKeys,p_save,false);
			}

			break;
		case AnimKeySet::TYPE_ROT_TRANS:
			{
				OldAnimKeyRT* p_save = new OldAnimKeyRT[p_aksCurrent->m_uNumKeys];

				for(j=0;j<p_aksCurrent->m_uNumKeys;j++)
				{
					p_save[j].time = p_aksCurrent->mp_rt[j].time;
					p_save[j].tx = p_aksCurrent->mp_rt[j].pos.x;
					p_save[j].ty = p_aksCurrent->mp_rt[j].pos.y;
					p_save[j].tz = p_aksCurrent->mp_rt[j].pos.z;
					p_save[j].rx = -p_aksCurrent->mp_rt[j].quat.x;
					p_save[j].ry = -p_aksCurrent->mp_rt[j].quat.y;
					p_save[j].rz = -p_aksCurrent->mp_rt[j].quat.z;
					p_save[j].rw = p_aksCurrent->mp_rt[j].quat.w;
				}

			//	p_isInfo->totalNumKeyframes += p_aksCurrent->m_uNumKeys * 7;
				p_iffbData = new IffBlock("KRT0",sizeof(OldAnimKeyRT) * p_aksCurrent->m_uNumKeys,p_save,true);
			}

			break;
		}

		p_iffbCpan->addBlock(p_iffbAnim);
		p_iffbCpan->addBlock(p_iffbData);

		p_iffbRoot->addBlock(p_iffbCpan);
	}

	return p_iffbRoot;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ComponentAnim::save(BinaryStream& r_streamOut)
{
	IffBlock* p_iffbRoot = getForSave();
	p_iffbRoot->save(r_streamOut);
	delete p_iffbRoot;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ComponentAnim::saveOptimisedVer2(BinaryStream& r_streamOut)
{
	int32 i;

	r_streamOut.write(mp_cName,ANIM_NAME_LENGTH);
	r_streamOut.writeU32(m_iNumNodes);

	//go through each component animation, writing it into memory
	for(i=0;i<m_iNumNodes;i++)
	{
		//save it
		mp_aksNodes[i]->saveOptimised(r_streamOut);
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ComponentAnim::loadOptimisedVer2(BinaryStream& r_streamIn)
{
	int32 i;

	r_streamIn.read(mp_cName,ANIM_NAME_LENGTH);
	m_iNumNodes = r_streamIn.readU32();

	for(i=0;i<m_iNumNodes;i++)
	{
		mp_aksNodes[i] = new AnimKeySet;
		mp_aksNodes[i]->loadOptimised(r_streamIn);
	}

	m_iFlags = 0;

	if(	(mp_aksNodes[0]->m_iType == AnimKeySet::TYPE_ROT_COMPRESSED) ||
		(mp_aksNodes[0]->m_iType == AnimKeySet::TYPE_ROT_TRANS_COMPRESSED))
	{
		m_iFlags |= FLAG_COMPRESSED;
	}

	calcWriteSize();

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ComponentAnim::saveOptimisedVer3(BinaryStream& r_streamOut)
{
	int32 i;

	r_streamOut.write(mp_cName,ANIM_NAME_LENGTH);
	r_streamOut.writeU32(m_iNumNodes);
	r_streamOut.writeU32(m_iWriteSize);
	r_streamOut.writeU32(m_iFlags);

	//go through each component animation, writing it into memory
	for(i=0;i<m_iNumNodes;i++)
	{
		//save it
		mp_aksNodes[i]->saveOptimised(r_streamOut);
	}

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ComponentAnim::loadOptimisedVer3(BinaryStream& r_streamIn)
{
	int32 i;

	r_streamIn.read(mp_cName,ANIM_NAME_LENGTH);
	m_iNumNodes = r_streamIn.readU32();
	m_iWriteSize = r_streamIn.readU32();
	m_iFlags = r_streamIn.readU32();

	for(i=0;i<m_iNumNodes;i++)
	{
		mp_aksNodes[i] = new AnimKeySet;
		mp_aksNodes[i]->loadOptimised(r_streamIn);
	}

	//might as well make sure of these values...

	m_iFlags = 0;

	if(	(mp_aksNodes[0]->m_iType == AnimKeySet::TYPE_ROT_COMPRESSED) ||
		(mp_aksNodes[0]->m_iType == AnimKeySet::TYPE_ROT_TRANS_COMPRESSED))
	{
		m_iFlags |= FLAG_COMPRESSED;
	}

	calcWriteSize();

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ComponentAnim::tryAndCompress()
{
	for(int32 i=0;i<m_iNumNodes;i++)
	{
		if(!mp_aksNodes[i]->canCompress())
		{
			printf("cant compress %s\n",mp_cName);
			return;
		}
	}

	m_iFlags |= FLAG_COMPRESSED;

	//its possible to compress all the nodes so lets do it man!

	for(i=0;i<m_iNumNodes;i++)
	{
		mp_aksNodes[i]->compress();
	}

	calcWriteSize();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ComponentAnim::checkCompressConsistent()
{
	if(m_iNumNodes < 2)
	{
		return;
	}

	bool bHasCompressed = false;
	bool bHasUnCompressed = false;

	for(int32 i=0;i<m_iNumNodes;i++)
	{
		if(	(mp_aksNodes[i]->m_iType == AnimKeySet::TYPE_ROT_COMPRESSED) || 
			(mp_aksNodes[i]->m_iType == AnimKeySet::TYPE_ROT_TRANS_COMPRESSED))
		{
			bHasCompressed = true;
		}
		else
		{
			bHasUnCompressed = true;
		}
	}

	//if we have a mix of compressed and uncompressed then
	//we are going to have to uncompress all of the nodes

	if(bHasCompressed && bHasUnCompressed)
	{
		m_iFlags &= ~FLAG_COMPRESSED;

		for(int32 i=0;i<m_iNumNodes;i++)
		{
			mp_aksNodes[i]->uncompress();
		}
	}

	calcWriteSize();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ComponentAnim::calcWriteSize()
{
	m_iWriteSize = 0;

	for(int32 i=0;i<m_iNumNodes;i++)
	{
		m_iWriteSize += mp_aksNodes[i]->getWriteSize();
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

const float GtaCamera::m_fCutTime(0.032f);

//////////////////////////////////////////////////////////////////////////////////////////////////
bool GtaCamera::getNextCut(int32& r_iKey) const
{
	while(r_iKey < ((int32)(m_uNumPos - 2)))
	{
		r_iKey++;

		if((mp_posTimes[r_iKey + 1] - mp_posTimes[r_iKey]) <= m_fCutTime)
		{
			return true;
		}
	}

	return false;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
float GtaCamera::maxTime() const
{
	return mp_posTimes[m_uNumPos - 1];
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void GtaCamera::getAtTime(float fTime,CVector& r_vecPos,CVector& r_vecDir,float& r_fFOV) const
{
	//pos time should be an exact match

	float fRatio;
	bool bFound = false;
	CVector vecPos;
	uint32 i;

	if(fTime > mp_posTimes[m_uNumPos - 1])
	{
		fTime = mp_posTimes[m_uNumPos - 1];
	}

	for(i = 1;i < m_uNumPos;i++)
	{
		if(mp_posTimes[i] >= fTime)
		{
			bFound = true;
			break;
		}
	}

	Assert(bFound);

	if(!bFound)
	{
		return;
	}

	fRatio = (fTime - mp_posTimes[i - 1]) / (mp_posTimes[i] - mp_posTimes[i - 1]);

	r_vecPos.x = mp_pos[i - 1].x + fRatio * (mp_pos[i].x - mp_pos[i - 1].x);
	r_vecPos.y = mp_pos[i - 1].y + fRatio * (mp_pos[i].y - mp_pos[i - 1].y);
	r_vecPos.z = mp_pos[i - 1].z + fRatio * (mp_pos[i].z - mp_pos[i - 1].z);

	bFound = false;

	for(i = 1;i < m_uNumTarget;i++)
	{
		if(mp_targTimes[i] >= fTime)
		{
			bFound = true;
			break;
		}
	}

	Assert(bFound);

	if(!bFound)
	{
		return;
	}

	fRatio = (fTime - mp_targTimes[i - 1]) / (mp_targTimes[i] - mp_targTimes[i - 1]);
	CVector vecTargOut;

	vecTargOut.x = mp_target[i - 1].x + fRatio * (mp_target[i].x - mp_target[i - 1].x);
	vecTargOut.y = mp_target[i - 1].y + fRatio * (mp_target[i].y - mp_target[i - 1].y);
	vecTargOut.z = mp_target[i - 1].z + fRatio * (mp_target[i].z - mp_target[i - 1].z);

	r_vecDir = vecTargOut - r_vecPos;
	r_vecDir.Normalise();

	for(i = 0;i<m_uNumFov;i++)
	{
		if(fTime >= mp_fovTimes[i])
		{
			break;
		}
	}

	if(i == m_uNumFov)
	{
		r_fFOV = 60.0f;
		return;
	}

	r_fFOV = mp_fovs[i];
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool GtaCamera::load(const char* p_cFilename)
{
	WinLib::FileStream* fsCamera = WinLib::FileStream::m_cbCreateStream();

	if(!fsCamera->open(p_cFilename,"r"))
	{
		delete fsCamera;
		return false;
	}

	uint32 i;
	char cTempLine[8192];

	fsCamera->readLine(cTempLine,8192);
	sscanf(cTempLine,"%d,",&m_uNumFov);

	mp_fovTimes = new float[m_uNumFov];
	mp_fovs = new float[m_uNumFov];

	for(i = 0;i<m_uNumFov;i++)
	{
		fsCamera->readLine(cTempLine,8192);
		sscanf(cTempLine,"%ff,%f,",&mp_fovTimes[i],&mp_fovs[i]);
	}

	//semi colon
	fsCamera->readLine(cTempLine,8192);

	fsCamera->readLine(cTempLine,8192);
	sscanf(cTempLine,"%d,",&m_uNumRoll);

	for(i = 0;i<m_uNumRoll;i++)
	{
		fsCamera->readLine(cTempLine,8192);
	}

	//semi colon
	fsCamera->readLine(cTempLine,8192);

	fsCamera->readLine(cTempLine,8192);
	sscanf(cTempLine,"%d,",&m_uNumPos);

	mp_posTimes = new float[m_uNumPos];
	mp_pos = new CVector[m_uNumPos];

	for(i = 0;i<m_uNumPos;i++)
	{
		fsCamera->readLine(cTempLine,8192);
		sscanf(cTempLine,"%ff,%f,%f,%f,",&mp_posTimes[i],&mp_pos[i].x,&mp_pos[i].y,&mp_pos[i].z);
	}

	//semi colon
	fsCamera->readLine(cTempLine,8192);

	fsCamera->readLine(cTempLine,8192);
	sscanf(cTempLine,"%d,",&m_uNumTarget);

	mp_targTimes = new float[m_uNumTarget];
	mp_target = new CVector[m_uNumTarget];

	for(i = 0;i<m_uNumTarget;i++)
	{
		fsCamera->readLine(cTempLine,8192);
		sscanf(cTempLine,"%ff,%f,%f,%f,",&mp_targTimes[i],&mp_target[i].x,&mp_target[i].y,&mp_target[i].z);
	}

	//semi colon
	fsCamera->readLine(cTempLine,8192);

	fsCamera->close();
	delete fsCamera;

	return true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
static bool stristr(const char* p_In,const char* p_Find)
{
	char cIn[8192];
	char cFind[8192];

	strcpy(cIn,p_In);
	strcpy(cFind,p_Find);

	strlwr(cIn);
	strlwr(cFind);

	if(strstr(cIn,cFind))
	{
		return true;
	}

	return false;
}

std::map<std::string,int32> ComponentAnim::m_mapBoneTags;

void ComponentAnim::clearBoneTagSet()
{
	m_mapBoneTags.clear();
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void ComponentAnim::addDefaultBoneTagSet()
{/*
	addBoneIDTag("HANDS:R FOREARM",BONETAG_HAND_R_FOREARM);
	addBoneIDTag("HANDS:R HAND",BONETAG_HAND_R_HAND);
	addBoneIDTag("HANDS:R THUMB1",BONETAG_HAND_R_THUMB1);
	addBoneIDTag("HANDS:R THUMB2",BONETAG_HAND_R_THUMB2);
	addBoneIDTag("HANDS:R THUMB3",BONETAG_HAND_R_THUMB3);
	addBoneIDTag("HANDS:R INDEX1",BONETAG_HAND_R_INDEX1);
	addBoneIDTag("HANDS:R INDEX2",BONETAG_HAND_R_INDEX2);
	addBoneIDTag("HANDS:R INDEX3",BONETAG_HAND_R_INDEX3);
	addBoneIDTag("HANDS:R MIDDLE1",BONETAG_HAND_R_MIDDLE1);
	addBoneIDTag("HANDS:R MIDDLE2",BONETAG_HAND_R_MIDDLE2);
	addBoneIDTag("HANDS:R MIDDLE3",BONETAG_HAND_R_MIDDLE3);
	addBoneIDTag("HANDS:R RING1",BONETAG_HAND_R_RING1);
	addBoneIDTag("HANDS:R RING2",BONETAG_HAND_R_RING2);
	addBoneIDTag("HANDS:R RING3",BONETAG_HAND_R_RING3);
	addBoneIDTag("HANDS:R LITTLE1",BONETAG_HAND_R_LITTLE1);
	addBoneIDTag("HANDS:R LITTLE2",BONETAG_HAND_R_LITTLE2);
	addBoneIDTag("HANDS:R LITTLE3",BONETAG_HAND_R_LITTLE3);
	addBoneIDTag("HANDS:L FOREARM",BONETAG_HAND_L_FOREARM);
	addBoneIDTag("HANDS:L HAND",BONETAG_HAND_L_HAND);
	addBoneIDTag("HANDS:L THUMB1",BONETAG_HAND_L_THUMB1);
	addBoneIDTag("HANDS:L THUMB2",BONETAG_HAND_L_THUMB2);
	addBoneIDTag("HANDS:L THUMB3",BONETAG_HAND_L_THUMB3);
	addBoneIDTag("HANDS:L INDEX1",BONETAG_HAND_L_INDEX1);
	addBoneIDTag("HANDS:L INDEX2",BONETAG_HAND_L_INDEX2);
	addBoneIDTag("HANDS:L INDEX3",BONETAG_HAND_L_INDEX3);
	addBoneIDTag("HANDS:L MIDDLE1",BONETAG_HAND_L_MIDDLE1);
	addBoneIDTag("HANDS:L MIDDLE2",BONETAG_HAND_L_MIDDLE2);
	addBoneIDTag("HANDS:L MIDDLE3",BONETAG_HAND_L_MIDDLE3);
	addBoneIDTag("HANDS:L RING1",BONETAG_HAND_L_RING1);
	addBoneIDTag("HANDS:L RING2",BONETAG_HAND_L_RING2);
	addBoneIDTag("HANDS:L RING3",BONETAG_HAND_L_RING3);
	addBoneIDTag("HANDS:L LITTLE1",BONETAG_HAND_L_LITTLE1);
	addBoneIDTag("HANDS:L LITTLE2",BONETAG_HAND_L_LITTLE2);
	addBoneIDTag("HANDS:L LITTLE3",BONETAG_HAND_L_LITTLE3);
	addBoneIDTag("Root",BONETAG_ROOT);
	addBoneIDTag("Pelvis",BONETAG_PELVIS);
	addBoneIDTag("Spine 1",BONETAG_SPINE_1);
	addBoneIDTag("Spine 2",BONETAG_SPINE_2);
	addBoneIDTag("Spine1",BONETAG_SPINE_2);
	addBoneIDTag("Spine",BONETAG_SPINE_1);
	addBoneIDTag("Neck",BONETAG_NECK);
	addBoneIDTag("Head",BONETAG_HEAD);
	addBoneIDTag("L Brow",BONETAG_L_BROW);
	addBoneIDTag("R Brow",BONETAG_R_BROW);
	addBoneIDTag("R Clavicle",BONETAG_R_CLAVICLE);
	addBoneIDTag("R UpperArm",BONETAG_R_UPPERARM);
	addBoneIDTag("R Forearm",BONETAG_R_FOREARM);
	addBoneIDTag("R Hand",BONETAG_R_HAND);
	addBoneIDTag("R Finger01",BONETAG_R_FINGER01);
	addBoneIDTag("R Finger02",BONETAG_R_FINGER02);
	addBoneIDTag("R Finger",BONETAG_R_FINGER);
	addBoneIDTag("RThumb1",BONETAG_RTHUMB1);
	addBoneIDTag("RThumb2",BONETAG_RTHUMB2);
	addBoneIDTag("L Clavicle",BONETAG_L_CLAVICLE);
	addBoneIDTag("L UpperArm",BONETAG_L_UPPERARM);
	addBoneIDTag("L Forearm",BONETAG_L_FOREARM);
	addBoneIDTag("L Hand",BONETAG_L_HAND);
	addBoneIDTag("L Finger01",BONETAG_L_FINGER01);
	addBoneIDTag("L Finger02",BONETAG_L_FINGER02);
	addBoneIDTag("L Finger",BONETAG_L_FINGER);
	addBoneIDTag("LThumb1",BONETAG_LTHUMB1);
	addBoneIDTag("LThumb2",BONETAG_LTHUMB2);
	addBoneIDTag("L Thigh",BONETAG_L_THIGH);
	addBoneIDTag("L Calf",BONETAG_L_CALF);
	addBoneIDTag("L Foot",BONETAG_L_FOOT);
	addBoneIDTag("L Toe",BONETAG_L_TOE);
	addBoneIDTag("R Thigh",BONETAG_R_THIGH);
	addBoneIDTag("R Calf",BONETAG_R_CALF);
	addBoneIDTag("R Foot",BONETAG_R_FOOT);
	addBoneIDTag("R Toe",BONETAG_R_TOE);
	addBoneIDTag("L Breast",BONETAG_L_BREAST);
	addBoneIDTag("R Breast",BONETAG_R_BREAST);
	addBoneIDTag("Belly",BONETAG_BELLY);
	addBoneIDTag("rbrow1",BONETAG_RBROW1);
	addBoneIDTag("rbrow2",BONETAG_RBROW2);
	addBoneIDTag("lbrow2",BONETAG_LBROW2);
	addBoneIDTag("lbrow1",BONETAG_LBROW1);
	addBoneIDTag("rlid",BONETAG_RLID);
	addBoneIDTag("llid",BONETAG_LLID);
	addBoneIDTag("rtlip1",BONETAG_RTLIP1);
	addBoneIDTag("rtlip2",BONETAG_RTLIP2);
	addBoneIDTag("rtlip3",BONETAG_RTLIP3);
	addBoneIDTag("ltlip1",BONETAG_LTLIP1);
	addBoneIDTag("ltlip2",BONETAG_LTLIP2);
	addBoneIDTag("ltlip3",BONETAG_LTLIP3);
	addBoneIDTag("rcorner",BONETAG_RCORNER);
	addBoneIDTag("lcorner",BONETAG_LCORNER);
	addBoneIDTag("jaw1",BONETAG_JAW1);
	addBoneIDTag("jaw22",BONETAG_JAW22);
	addBoneIDTag("jaw2",BONETAG_JAW2);
	addBoneIDTag("jaw",BONETAG_JAW);
	addBoneIDTag("llip11",BONETAG_LLIP11);
	addBoneIDTag("llip1",BONETAG_LLIP1);
	addBoneIDTag("reye",BONETAG_REYE);
	addBoneIDTag("leye",BONETAG_LEYE);
	addBoneIDTag("rcheek",BONETAG_RCHEEK);
	addBoneIDTag("lcheek",BONETAG_LCHEEK);*/
}

//#define CASE_SENSITIVE

//////////////////////////////////////////////////////////////////////////////////////////////////
bool ComponentAnim::addBoneIDTag(const char* pName,int32 iTagID)
{
#ifdef CASE_SENSITIVE

	m_mapBoneTags.insert(std::map<std::string,int32>::value_type(std::string(pName),iTagID));

#else

	char cCopy[128];
	strncpy(cCopy,pName,128);
	strlwr(cCopy);
	m_mapBoneTags.insert(std::map<std::string,int32>::value_type(std::string(cCopy),iTagID));

#endif

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
int32 ComponentAnim::getBoneIDFromName(const char* pName)
{
#ifdef CASE_SENSITIVE

	std::map<std::string,int32>::iterator found = m_mapBoneTags.find(std::string(pName));

#else

	char cCopy[128];
	strncpy(cCopy,pName,128);
	strlwr(cCopy);
	std::map<std::string,int32>::iterator found = m_mapBoneTags.find(std::string(cCopy));

#endif

	std::map<std::string,int32>::iterator end = m_mapBoneTags.end();

	if(found == end)
	{
		return -1;
	}

	return found->second;
}