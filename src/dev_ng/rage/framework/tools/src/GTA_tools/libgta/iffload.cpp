#include "libcore/exception.h"
#include "libgta/iffload.h"

#include <string.h>

using namespace WinLib;

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////
IffBlock::IffBlock(BinaryStream& r_streamIn):
	mp_cData(NULL),
	m_iNumBlocks(0)
{
	mp_tag[4] = '\0';

	r_streamIn.read((unsigned char*)mp_tag,4);
	r_streamIn.read((unsigned char*)&m_iSize,4);

	delete[] mp_cData;
	mp_cData = NULL;

	uint32 uiReadIn = m_iSize;
	uiReadIn += 3;
	uiReadIn>>=2;
	uiReadIn<<=2;

	mp_cData = new char[m_iSize];
	r_streamIn.read((unsigned char*)mp_cData,uiReadIn);

	if(!readBlocks())
	{
		delete[] mp_cData;
		mp_cData = NULL;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
IffBlock::IffBlock(char *pTag,int32 size,void* ptr,bool owner):
	mp_cData(NULL),
	m_iNumBlocks(0)
{
    strncpy(mp_tag, pTag, 4);
    mp_tag[4]='\0';

	m_iSize = size;

	m_bOwner = owner;
	delete[] mp_cData;

	if(m_bOwner)
	{
		mp_cData = new char[m_iSize];

		if(ptr)
			memcpy(mp_cData, (char*)ptr, m_iSize);
		else
			memset(mp_cData, 0, m_iSize);
	}
	else
	{
		mp_cData = (char*)ptr;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
IffBlock::~IffBlock()
{
	if(m_bOwner)
	{
		delete[] mp_cData;
	}
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool IffBlock::readBlocks()
{
    IffBlock *pCurrent;
    int32 size, blockSize;
        
	char* p_cData = mp_cData;
	int32 iDataSize = m_iSize;

	size = 0;
    while(size != iDataSize)
    {
		if(size < 0 || size > iDataSize)
			return false;

		blockSize = *((uint32*)(p_cData + size + 4));
		if(blockSize <= 0)
			return false;

		pCurrent = new IffBlock(p_cData + size, blockSize, p_cData + size + 8, false);

		Assert(m_iNumBlocks < MAX_BLOCKS);

		mp_iffbData[m_iNumBlocks++] = pCurrent;

		size += pCurrent->getSize() + 8;

		if(pCurrent->getSize() & 3)
		{
			size += 4 - (pCurrent->getSize() & 3);
		}
    }

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
int32 IffBlock::getWriteSize() const
{
	int32 iReturn = m_iSize;

	iReturn += 3;
	iReturn>>=2;
	iReturn<<=2;

	return iReturn; 
}

//////////////////////////////////////////////////////////////////////////////////////////////////
int32 IffBlock::getSize() const 
{
	return m_iSize; 
}

//////////////////////////////////////////////////////////////////////////////////////////////////
bool IffBlock::addBlock(IffBlock* p_iffbAdd)
{
	if(m_iNumBlocks == MAX_BLOCKS)
	{
		return false;
	}

	mp_iffbData[m_iNumBlocks++] = p_iffbAdd;

	m_iSize += p_iffbAdd->getWriteSize();
	m_iSize += 8; //for tag and size

	return true;
}

//////////////////////////////////////////////////////////////////////////////////////////////////
void IffBlock::save(BinaryStream& fsWrite)
{
	fsWrite.write((uint8*)mp_tag,4);
	fsWrite.write((uint8*)&m_iSize,4);
	
	if(m_iNumBlocks == 0)
	{
		uint32 uiWrite = m_iSize;
		uiWrite += 3;
		uiWrite>>=2;
		uiWrite<<=2;

		uiWrite -= m_iSize;

		fsWrite.write((uint8*)mp_cData,m_iSize);

		while(uiWrite)
		{
			fsWrite.writeU8(0);
			uiWrite--;
		}
	}
	else
	{
		for(int32 i=0;i<m_iNumBlocks;i++)
		{
			mp_iffbData[i]->save(fsWrite);
		}
	}
}
