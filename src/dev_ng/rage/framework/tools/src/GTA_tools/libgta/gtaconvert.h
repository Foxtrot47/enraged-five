#ifndef WINLIB_GTACONVERT_H
#define WINLIB_GTACONVERT_H

//#include "..\cdimage\cdimage\forceinclude.h"
#include "..\cdimage\cdimage\cdimage.h"

typedef bool (*ConvertProgressCallback)(uint32 uiData);

void GtaConvertIPLInPack(RockstarPack& r_rpOptimise,const char* p_cTempDir,ConvertProgressCallback cbProgress = NULL);

#endif //WINLIB_GTACONVERT_H