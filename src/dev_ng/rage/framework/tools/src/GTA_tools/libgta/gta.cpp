#include "libcore/rect.h"
#include "libcore/exception.h"
#include "libcore/vector2d.h"
#include "libgta/gta.h"

#include <string.h>
#include <stdio.h>

using namespace WinLib;

/////////////////////////////////////////////////////////////////////////////////////////////////////
IdeSet::IdeSet()
{
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
IdeSet::~IdeSet()
{
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IdeSet::reset()
{

}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IdeSet::getNextInt(char* p_Read,int32& r_In)
{
	char* p_cIndex = strtok(p_Read,", ");
	r_In = atoi(p_cIndex);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IdeSet::getNextUInt(char* p_Read,uint32& r_In)
{
	char* p_cIndex = strtok(p_Read,", ");
	r_In = atoi(p_cIndex);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IdeSet::getNextSInt(char* p_Read,int16& r_In)
{
	char* p_cIndex = strtok(p_Read,", ");
	r_In = atoi(p_cIndex);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IdeSet::getNextFloat(char* p_Read,float& r_In)
{
	char* p_cIndex = strtok(p_Read,", ");
	r_In = (float)atof(p_cIndex);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IdeSet::getNextString(char* p_Read,char* p_cIn)
{
	char* p_cIndex = strtok(p_Read,", ");
	strcpy(p_cIn,p_cIndex);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
bool IdeSet::add(BinaryStream& r_sRead)
{
	char cLineBuffer[4096];

	enum ReadState
	{
		STATE_FREE,
		STATE_OBJS,
		STATE_TOBJS,
		STATE_WEAP,
		STATE_CLUMP,
		STATE_ANIMCLUMP,
		STATE_VEHICLE,
		STATE_PED,
		STATE_2DFX,
	};

	ReadState rsCurrent = STATE_FREE;

	while(r_sRead.getPending())
	{
		r_sRead.readLine(cLineBuffer,4096);

		if(stricmp(cLineBuffer,"end") == 0)
		{
			rsCurrent = STATE_FREE;
			continue;
		}

		switch(rsCurrent)
		{
		case STATE_FREE:

			if(stricmp(cLineBuffer,"objs") == 0)
			{
				rsCurrent = STATE_OBJS;
			}
			else if(stricmp(cLineBuffer,"tobj") == 0)
			{
				rsCurrent = STATE_TOBJS;
			}
			else if(stricmp(cLineBuffer,"weap") == 0)
			{
				rsCurrent = STATE_WEAP;
			}
			else if(stricmp(cLineBuffer,"hier") == 0)
			{
				rsCurrent = STATE_CLUMP;
			}
			else if(stricmp(cLineBuffer,"anim") == 0)
			{
				rsCurrent = STATE_ANIMCLUMP;
			}
			else if(stricmp(cLineBuffer,"cars") == 0)
			{
				rsCurrent = STATE_VEHICLE;
			}
			else if(stricmp(cLineBuffer,"peds") == 0)
			{
				rsCurrent = STATE_PED;
			}
			else if(stricmp(cLineBuffer,"2dfx") == 0)
			{
				rsCurrent = STATE_2DFX;
			}

			break;

		case STATE_OBJS:
			{
				GtaObjectClass gtaObjClass;

				getNextInt(cLineBuffer,gtaObjClass.iIndex);
				getNextString(NULL,gtaObjClass.cObjName);
				getNextString(NULL,gtaObjClass.cTxdName);
				getNextFloat(NULL,gtaObjClass.fLod);
				getNextInt(NULL,gtaObjClass.iFlags);
				
				m_llObjectClasses.push_back(gtaObjClass);
			}

			break;

		case STATE_TOBJS:
			{
				GtaTimeObject gtaTimeObject;

				getNextInt(cLineBuffer,gtaTimeObject.iIndex);
				getNextString(NULL,gtaTimeObject.cObjName);
				getNextString(NULL,gtaTimeObject.cTxdName);
				getNextFloat(NULL,gtaTimeObject.fLod);
				getNextInt(NULL,gtaTimeObject.iFlags);
				getNextInt(NULL,gtaTimeObject.iTimeOn);
				getNextInt(NULL,gtaTimeObject.iTimeOff);
				
				m_llTimeObjects.push_back(gtaTimeObject);
			}
			break;
		case STATE_WEAP:
			{
				GtaWeapon gtaWeapon;

				getNextInt(cLineBuffer,gtaWeapon.iIndex);
				getNextString(NULL,gtaWeapon.name);
				getNextString(NULL,gtaWeapon.txdName);
				getNextString(NULL,gtaWeapon.animFileName);

				strtok(NULL,", "); //ignored

				getNextFloat(NULL,gtaWeapon.lodDist);
				
				m_llWeapons.push_back(gtaWeapon);
			}
			break;
		case STATE_CLUMP:
			{
				GtaClump gtaClump;

				getNextInt(cLineBuffer,gtaClump.id);
				getNextString(NULL,gtaClump.name);
				getNextString(NULL,gtaClump.txdname);

				m_llClumps.push_back(gtaClump);
			}
			break;
		case STATE_ANIMCLUMP:
			{
				GtaAnimClump gtaAnimClump;

				getNextInt(cLineBuffer,gtaAnimClump.id);
				getNextString(NULL,gtaAnimClump.name);
				getNextString(NULL,gtaAnimClump.txdname);
				getNextString(NULL,gtaAnimClump.animFileName);
				getNextFloat(cLineBuffer,gtaAnimClump.lodDistance);
				getNextInt(cLineBuffer,gtaAnimClump.flags);

				m_llAnimClumps.push_back(gtaAnimClump);
			}
			break;
		case STATE_VEHICLE:
			{
				GtaVehicle gtaVehicle;

				getNextInt(cLineBuffer,gtaVehicle.vehicleId);
				getNextString(NULL,gtaVehicle.name);
				getNextString(NULL,gtaVehicle.txdName);
				getNextString(NULL,gtaVehicle.typeString);
				getNextString(NULL,gtaVehicle.handlingString);
				getNextString(NULL,gtaVehicle.gameName);
				getNextString(NULL,gtaVehicle.animFileName);
				getNextString(NULL,gtaVehicle.rating);
				getNextInt(NULL,gtaVehicle.freq);
				getNextInt(NULL,gtaVehicle.levels);
				getNextUInt(NULL,gtaVehicle.rules);
				getNextInt(NULL,gtaVehicle.wheelId);
				getNextFloat(NULL,gtaVehicle.wheelScale);
				getNextInt(NULL,gtaVehicle.wheelClass);

				m_llVehicles.push_back(gtaVehicle);
			}
			break;
		case STATE_PED:
			{
				GtaPed gtaPed;

				getNextInt(cLineBuffer,gtaPed.id);
				getNextString(NULL,gtaPed.name);
				getNextString(NULL,gtaPed.txdname);
				getNextString(NULL,gtaPed.pedtype);
				getNextString(NULL,gtaPed.pedstats);
				getNextString(NULL,gtaPed.animtype);
				getNextUInt(NULL,gtaPed.flags);
				getNextString(NULL,gtaPed.animFileName);
				getNextInt(NULL,gtaPed.radio1);
				getNextInt(NULL,gtaPed.radio2);

				m_llPeds.push_back(gtaPed);
			}
			break;
		case STATE_2DFX:
			{
				Gta2dfx gta2dfx;

				sscanf(cLineBuffer, "%d %f %f %f %d", &gta2dfx.id, &gta2dfx.x, &gta2dfx.y, &gta2dfx.z, &gta2dfx.type);

				switch(gta2dfx.type)
				{
				case ET_LIGHT:
					{
						Gta2dfxLight gta2dfxLight;

						getNextInt(cLineBuffer,gta2dfxLight.id);
						getNextFloat(NULL,gta2dfxLight.x);
						getNextFloat(NULL,gta2dfxLight.y);
						getNextFloat(NULL,gta2dfxLight.z);
						getNextInt(NULL,gta2dfxLight.type);
						getNextInt(NULL,gta2dfxLight.colR);
						getNextInt(NULL,gta2dfxLight.colG);
						getNextInt(NULL,gta2dfxLight.colB);
						getNextInt(NULL,gta2dfxLight.colA);
						getNextString(NULL,gta2dfxLight.coronaName);
						getNextString(NULL,gta2dfxLight.shadowName);
						getNextFloat(NULL,gta2dfxLight.lodDistance);
						getNextFloat(NULL,gta2dfxLight.size);
						getNextFloat(NULL,gta2dfxLight.coronaSize);
						getNextFloat(NULL,gta2dfxLight.shadowSize);
						getNextInt(NULL,gta2dfxLight.shadowAlpha);
						getNextInt(NULL,gta2dfxLight.flashiness);
						getNextInt(NULL,gta2dfxLight.reflectionType);
						getNextInt(NULL,gta2dfxLight.lensFlareType);
						getNextInt(NULL,gta2dfxLight.flags);

						m_ll2dfxLights.push_back(gta2dfxLight);
					}
					break;
				case ET_PARTICLE:
					{
						Gta2dfxParticle gta2dfxParticle;

						getNextInt(cLineBuffer,gta2dfxParticle.id);
						getNextFloat(NULL,gta2dfxParticle.x);
						getNextFloat(NULL,gta2dfxParticle.y);
						getNextFloat(NULL,gta2dfxParticle.z);
						getNextInt(NULL,gta2dfxParticle.type);
						getNextString(NULL,gta2dfxParticle.name);

						m_ll2dfxParticles.push_back(gta2dfxParticle);
					}
					break;
				case ET_PEDQUEUE:
					{
						Gta2dfxPedQueue gtaPedQueue;

						getNextInt(cLineBuffer,gtaPedQueue.id);
						getNextFloat(NULL,gtaPedQueue.x);
						getNextFloat(NULL,gtaPedQueue.y);
						getNextFloat(NULL,gtaPedQueue.z);
						getNextInt(NULL,gtaPedQueue.type);
						getNextInt(NULL,gtaPedQueue.lookAtType);
						getNextFloat(NULL,gtaPedQueue.queueDirX);
						getNextFloat(NULL,gtaPedQueue.queueDirY);
						getNextFloat(NULL,gtaPedQueue.queueDirZ);
						getNextFloat(NULL,gtaPedQueue.forwardDirX);
						getNextFloat(NULL,gtaPedQueue.forwardDirY);
						getNextFloat(NULL,gtaPedQueue.forwardDirZ);
						getNextString(NULL,gtaPedQueue.m_scriptName);

						m_ll2dfxPedQueues.push_back(gtaPedQueue);
					}
					break;
				case ET_INTERIOR:
					{
						Gta2dfxInterior gta2dfxInterior;

						getNextInt(cLineBuffer,gta2dfxInterior.id);
						getNextFloat(NULL,gta2dfxInterior.x);
						getNextFloat(NULL,gta2dfxInterior.y);
						getNextFloat(NULL,gta2dfxInterior.z);
						getNextInt(NULL,gta2dfxInterior.type);
						getNextInt(NULL,gta2dfxInterior.inttype);
						getNextFloat(NULL,gta2dfxInterior.width);
						getNextFloat(NULL,gta2dfxInterior.depth);
						getNextFloat(NULL,gta2dfxInterior.height);
						getNextFloat(NULL,gta2dfxInterior.rot);
						getNextSInt(NULL,gta2dfxInterior.door);
						getNextSInt(NULL,gta2dfxInterior.seed);
						getNextSInt(NULL,gta2dfxInterior.group);
						getNextSInt(NULL,gta2dfxInterior.status);
						getNextSInt(NULL,gta2dfxInterior.lDoorStart);
						getNextSInt(NULL,gta2dfxInterior.lDoorEnd);
						getNextSInt(NULL,gta2dfxInterior.tDoorStart);
						getNextSInt(NULL,gta2dfxInterior.tDoorEnd);
						getNextSInt(NULL,gta2dfxInterior.rDoorStart);
						getNextSInt(NULL,gta2dfxInterior.rDoorEnd);
						getNextSInt(NULL,gta2dfxInterior.lWindowStart);
						getNextSInt(NULL,gta2dfxInterior.lWindowEnd);
						getNextSInt(NULL,gta2dfxInterior.tWindowStart);
						getNextSInt(NULL,gta2dfxInterior.tWindowEnd);
						getNextSInt(NULL,gta2dfxInterior.rWindowStart);
						getNextSInt(NULL,gta2dfxInterior.rWindowEnd);

						m_ll2dfxInteriors.push_back(gta2dfxInterior);
					}
					break;
				case ET_ENTRYEXIT:
					{
						Gta2dfxEntryExit gta2dfxEntryExit;

						getNextInt(cLineBuffer,gta2dfxEntryExit.id);
						getNextFloat(NULL,gta2dfxEntryExit.x);
						getNextFloat(NULL,gta2dfxEntryExit.y);
						getNextFloat(NULL,gta2dfxEntryExit.z);
						getNextInt(NULL,gta2dfxEntryExit.type);
						getNextFloat(NULL,gta2dfxEntryExit.prot);
						getNextFloat(NULL,gta2dfxEntryExit.wx);
						getNextFloat(NULL,gta2dfxEntryExit.wy);
						getNextFloat(NULL,gta2dfxEntryExit.spawnx);
						getNextFloat(NULL,gta2dfxEntryExit.spawny);
						getNextFloat(NULL,gta2dfxEntryExit.spawnz);
						getNextFloat(NULL,gta2dfxEntryExit.spawnrot);
						getNextInt(NULL,gta2dfxEntryExit.areacode);
						getNextInt(NULL,gta2dfxEntryExit.flags);
						getNextInt(NULL,gta2dfxEntryExit.extracol);
						getNextString(NULL,gta2dfxEntryExit.name);

						m_ll2dfxEntryExits.push_back(gta2dfxEntryExit);
					}
					break;
				case ET_ROADSIGN:
					{
						Gta2dfxRoadSign gtaRoadSign;

						getNextInt(cLineBuffer,gtaRoadSign.id);
						getNextFloat(NULL,gtaRoadSign.x);
						getNextFloat(NULL,gtaRoadSign.y);
						getNextFloat(NULL,gtaRoadSign.z);
						getNextInt(NULL,gtaRoadSign.type);
						getNextFloat(NULL,gtaRoadSign.rsWidth);
						getNextFloat(NULL,gtaRoadSign.rsHeight);
						getNextFloat(NULL,gtaRoadSign.rsRotX);
						getNextFloat(NULL,gtaRoadSign.rsRotY);
						getNextFloat(NULL,gtaRoadSign.rsRotZ);
						getNextUInt(NULL,gtaRoadSign.flags32);
						getNextString(NULL,gtaRoadSign.text1);
						getNextString(NULL,gtaRoadSign.text2);
						getNextString(NULL,gtaRoadSign.text3);
						getNextString(NULL,gtaRoadSign.text4);

						m_ll2dfxRoadSigns.push_back(gtaRoadSign);
					}
					break;
				case ET_TRIGGERPOINT:
					{
						Gta2dfxTriggerPoint gta2dfxTriggerPoint;

						getNextInt(cLineBuffer,gta2dfxTriggerPoint.id);
						getNextFloat(NULL,gta2dfxTriggerPoint.x);
						getNextFloat(NULL,gta2dfxTriggerPoint.y);
						getNextFloat(NULL,gta2dfxTriggerPoint.z);
						getNextInt(NULL,gta2dfxTriggerPoint.type);
						getNextInt(NULL,gta2dfxTriggerPoint.index);

						m_ll2dfxTriggerPoints.push_back(gta2dfxTriggerPoint);
					}
					break;
				case ET_COVERPOINT:
					{
						Gta2dfxCoverPoint gta2dfxCoverPoint;

						getNextInt(cLineBuffer,gta2dfxCoverPoint.id);
						getNextFloat(NULL,gta2dfxCoverPoint.x);
						getNextFloat(NULL,gta2dfxCoverPoint.y);
						getNextFloat(NULL,gta2dfxCoverPoint.z);
						getNextInt(NULL,gta2dfxCoverPoint.type);
						getNextFloat(NULL,gta2dfxCoverPoint.dirX);
						getNextFloat(NULL,gta2dfxCoverPoint.dirY);
						getNextInt(NULL,gta2dfxCoverPoint.usage);

						m_ll2dfxCoverPoints.push_back(gta2dfxCoverPoint);
					}
					break;
				case ET_ESCALATOR:
					{
						Gta2dfxEscalator gta2dfxEscalator;

						getNextInt(cLineBuffer,gta2dfxEscalator.id);
						getNextFloat(NULL,gta2dfxEscalator.x);
						getNextFloat(NULL,gta2dfxEscalator.y);
						getNextFloat(NULL,gta2dfxEscalator.z);
						getNextInt(NULL,gta2dfxEscalator.type);
						getNextFloat(NULL,gta2dfxEscalator.crs1X);
						getNextFloat(NULL,gta2dfxEscalator.crs1Y);
						getNextFloat(NULL,gta2dfxEscalator.crs1Z);
						getNextFloat(NULL,gta2dfxEscalator.crs2X);
						getNextFloat(NULL,gta2dfxEscalator.crs2Y);
						getNextFloat(NULL,gta2dfxEscalator.crs3Z);
						getNextFloat(NULL,gta2dfxEscalator.crs3X);
						getNextFloat(NULL,gta2dfxEscalator.crs3Y);
						getNextFloat(NULL,gta2dfxEscalator.crs3Z);
						getNextInt(NULL,gta2dfxEscalator.goingUp);

						m_ll2dfxEscalators.push_back(gta2dfxEscalator);
					}
					break;
				}
			}
			break;
		}
	}

	return true;
	//return false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IdeSet::saveBinary(BinaryStream& r_sWrite)
{
	struct GtaIDEHeader
	{
		char cHeader[4];
		int32 iSize;
		int16 iNumObjectClasses;
		int16 iNumTimeObjects;
		int16 iNumWeapons;
		int16 iNumClumps;
		int16 iNumAnimClumps;
		int16 iNumVehicles;
		int16 iNumPeds;
		int16 iNum2dfxRoadSigns;
		int16 iNum2dfxLights;
		int16 iNum2dfxParticles;
		int16 iNum2dfxPedQueues;
		int16 iNum2dfxInteriors;
		int16 iNum2dfxEntryExits;
		int16 iNum2dfxTriggerPoints;
		int16 iNum2dfxCoverPoints;
		int16 iNum2dfxEscalators;
	};

	GtaIDEHeader gtaIDEHeader;

	strncpy(gtaIDEHeader.cHeader,"bnry",4);
	gtaIDEHeader.iNumObjectClasses = static_cast<int16>( m_llObjectClasses.size() );
	gtaIDEHeader.iNumTimeObjects = static_cast<int16>( m_llTimeObjects.size());
	gtaIDEHeader.iNumWeapons = static_cast<int16>( m_llWeapons.size());
	gtaIDEHeader.iNumClumps = static_cast<int16>( m_llClumps.size());
	gtaIDEHeader.iNumAnimClumps = static_cast<int16>( m_llAnimClumps.size());
	gtaIDEHeader.iNumVehicles = static_cast<int16>( m_llVehicles.size());
	gtaIDEHeader.iNumPeds = static_cast<int16>( m_llPeds.size());
	gtaIDEHeader.iNum2dfxRoadSigns = static_cast<int16>( m_ll2dfxRoadSigns.size());
	gtaIDEHeader.iNum2dfxLights = static_cast<int16>( m_ll2dfxLights.size());
	gtaIDEHeader.iNum2dfxParticles = static_cast<int16>( m_ll2dfxParticles.size());
	gtaIDEHeader.iNum2dfxPedQueues = static_cast<int16>( m_ll2dfxPedQueues.size());
	gtaIDEHeader.iNum2dfxInteriors = static_cast<int16>( m_ll2dfxInteriors.size());
	gtaIDEHeader.iNum2dfxEntryExits = static_cast<int16>( m_ll2dfxEntryExits.size());
	gtaIDEHeader.iNum2dfxTriggerPoints = static_cast<int16>( m_ll2dfxTriggerPoints.size());
	gtaIDEHeader.iNum2dfxCoverPoints = static_cast<int16>( m_ll2dfxCoverPoints.size());
	gtaIDEHeader.iNum2dfxEscalators = static_cast<int16>( m_ll2dfxEscalators.size());

	gtaIDEHeader.iSize = sizeof(GtaIDEHeader) - 8;
	gtaIDEHeader.iSize += gtaIDEHeader.iNumObjectClasses * sizeof(GtaObjectClass);
	gtaIDEHeader.iSize += gtaIDEHeader.iNumTimeObjects * sizeof(GtaTimeObject);
	gtaIDEHeader.iSize += gtaIDEHeader.iNumWeapons * sizeof(GtaWeapon);
	gtaIDEHeader.iSize += gtaIDEHeader.iNumClumps * sizeof(GtaClump);
	gtaIDEHeader.iSize += gtaIDEHeader.iNumAnimClumps * sizeof(GtaAnimClump);
	gtaIDEHeader.iSize += gtaIDEHeader.iNumVehicles * sizeof(GtaVehicle);
	gtaIDEHeader.iSize += gtaIDEHeader.iNumPeds * sizeof(GtaPed);
	gtaIDEHeader.iSize += gtaIDEHeader.iNum2dfxRoadSigns * sizeof(Gta2dfxRoadSign);
	gtaIDEHeader.iSize += gtaIDEHeader.iNum2dfxLights * sizeof(Gta2dfxLight);
	gtaIDEHeader.iSize += gtaIDEHeader.iNum2dfxParticles * sizeof(Gta2dfxParticle);
	gtaIDEHeader.iSize += gtaIDEHeader.iNum2dfxPedQueues * sizeof(Gta2dfxPedQueue);
	gtaIDEHeader.iSize += gtaIDEHeader.iNum2dfxInteriors * sizeof(Gta2dfxInterior);
	gtaIDEHeader.iSize += gtaIDEHeader.iNum2dfxEntryExits * sizeof(Gta2dfxEntryExit);
	gtaIDEHeader.iSize += gtaIDEHeader.iNum2dfxTriggerPoints * sizeof(Gta2dfxTriggerPoint);
	gtaIDEHeader.iSize += gtaIDEHeader.iNum2dfxCoverPoints * sizeof(Gta2dfxCoverPoint);
	gtaIDEHeader.iSize += gtaIDEHeader.iNum2dfxEscalators * sizeof(Gta2dfxEscalator);

	r_sWrite.write(&gtaIDEHeader,sizeof(GtaIDEHeader));

	std::list<GtaObjectClass>::iterator obegin = m_llObjectClasses.begin();
	std::list<GtaObjectClass>::iterator oend = m_llObjectClasses.end();
	while(obegin!=oend)
	{
		r_sWrite.write(&(*obegin),sizeof(GtaObjectClass));
		obegin++;
	}
	std::list<GtaTimeObject>::iterator tobegin = m_llTimeObjects.begin();
	std::list<GtaTimeObject>::iterator toend = m_llTimeObjects.end();
	while(tobegin!=toend)
	{
		r_sWrite.write(&(*tobegin),sizeof(GtaTimeObject));
		tobegin++;
	}
	std::list<GtaWeapon>::iterator wbegin = m_llWeapons.begin();
	std::list<GtaWeapon>::iterator wend = m_llWeapons.end();
	while(wbegin!=wend)
	{
		r_sWrite.write(&(*wbegin),sizeof(GtaWeapon));
		wbegin++;
	}
	std::list<GtaClump>::iterator cbegin = m_llClumps.begin();
	std::list<GtaClump>::iterator cend = m_llClumps.end();
	while(cbegin!=cend)
	{
		r_sWrite.write(&(*cbegin),sizeof(GtaClump));
		cbegin++;
	}
	std::list<GtaAnimClump>::iterator acbegin = m_llAnimClumps.begin();
	std::list<GtaAnimClump>::iterator acend = m_llAnimClumps.end();
	while(acbegin!=acend)
	{
		r_sWrite.write(&(*acbegin),sizeof(GtaAnimClump));
		acbegin++;
	}
	std::list<GtaVehicle>::iterator vbegin = m_llVehicles.begin();
	std::list<GtaVehicle>::iterator vend = m_llVehicles.end();
	while(vbegin!=vend)
	{
		r_sWrite.write(&(*vbegin),sizeof(GtaVehicle));
		vbegin++;
	}
	std::list<GtaPed>::iterator pbegin = m_llPeds.begin();
	std::list<GtaPed>::iterator pend = m_llPeds.end();
	while(pbegin!=pend)
	{
		r_sWrite.write(&(*pbegin),sizeof(GtaPed));
		pbegin++;
	}
	std::list<Gta2dfxRoadSign>::iterator rsbegin = m_ll2dfxRoadSigns.begin();
	std::list<Gta2dfxRoadSign>::iterator rsend = m_ll2dfxRoadSigns.end();
	while(rsbegin!=rsend)
	{
		r_sWrite.write(&(*rsbegin),sizeof(Gta2dfxRoadSign));
		rsbegin++;
	}
	std::list<Gta2dfxLight>::iterator lbegin = m_ll2dfxLights.begin();
	std::list<Gta2dfxLight>::iterator lend = m_ll2dfxLights.end();
	while(lbegin!=lend)
	{
		r_sWrite.write(&(*lbegin),sizeof(Gta2dfxLight));
		lbegin++;
	}
	std::list<Gta2dfxParticle>::iterator pabegin = m_ll2dfxParticles.begin();
	std::list<Gta2dfxParticle>::iterator paend = m_ll2dfxParticles.end();
	while(pabegin!=paend)
	{
		r_sWrite.write(&(*pabegin),sizeof(Gta2dfxParticle));
		pabegin++;
	}
	std::list<Gta2dfxPedQueue>::iterator pqbegin = m_ll2dfxPedQueues.begin();
	std::list<Gta2dfxPedQueue>::iterator pqend = m_ll2dfxPedQueues.end();
	while(pqbegin!=pqend)
	{
		r_sWrite.write(&(*pqbegin),sizeof(Gta2dfxPedQueue));
		pqbegin++;
	}
	std::list<Gta2dfxInterior>::iterator ibegin = m_ll2dfxInteriors.begin();
	std::list<Gta2dfxInterior>::iterator iend = m_ll2dfxInteriors.end();
	while(ibegin!=iend)
	{
		r_sWrite.write(&(*ibegin),sizeof(Gta2dfxInterior));
		ibegin++;
	}
	std::list<Gta2dfxEntryExit>::iterator eebegin = m_ll2dfxEntryExits.begin();
	std::list<Gta2dfxEntryExit>::iterator eeend = m_ll2dfxEntryExits.end();
	while(eebegin!=eeend)
	{
		r_sWrite.write(&(*eebegin),sizeof(Gta2dfxEntryExit));
		eebegin++;
	}
	std::list<Gta2dfxTriggerPoint>::iterator tpbegin = m_ll2dfxTriggerPoints.begin();
	std::list<Gta2dfxTriggerPoint>::iterator tpend = m_ll2dfxTriggerPoints.end();
	while(tpbegin!=tpend)
	{
		r_sWrite.write(&(*tpbegin),sizeof(Gta2dfxTriggerPoint));
		tpbegin++;
	}
	std::list<Gta2dfxCoverPoint>::iterator cpbegin = m_ll2dfxCoverPoints.begin();
	std::list<Gta2dfxCoverPoint>::iterator cpend = m_ll2dfxCoverPoints.end();
	while(cpbegin!=cpend)
	{
		r_sWrite.write(&(*cpbegin),sizeof(Gta2dfxCoverPoint));
		cpbegin++;
	}
	std::list<Gta2dfxEscalator>::iterator esbegin = m_ll2dfxEscalators.begin();
	std::list<Gta2dfxEscalator>::iterator esend = m_ll2dfxEscalators.end();
	while(esbegin!=esend)
	{
		r_sWrite.write(&(*esbegin),sizeof(Gta2dfxEscalator));
		esbegin++;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////////////
IplSet::IplSet()
{
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
IplSet::~IplSet()
{
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::setIDE(IdeSet& r_ideSet)
{
	mp_ideSet = &r_ideSet;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::draw()
{
#if 0
	if(!mp_ideSet)
	{
		return;
	}

	RpClump* p_clump;
	RwFrame* p_frame;

	RwV3d vecPos;
	RwV3d vecAxis;

	vecAxis.x = 0.0f;
	vecAxis.y = 1.0f;
	vecAxis.z = 0.0f;

	for(int32 i =0;i<m_iNumIplEntries;i++)
	{
		IdeEntry* p_ideEntry = mp_ideSet->getEntry(mpp_iplList[i]->iObjectID);

		if(!p_ideEntry)
		{
			continue;
		}

		p_clump = p_ideEntry->p_clump;

		if(!p_clump)
		{
			continue;
		}

		p_frame = RpClumpGetFrame(p_clump);

		vecPos.x = mpp_iplList[i]->x;
		vecPos.y = mpp_iplList[i]->y;
		vecPos.z = mpp_iplList[i]->z;

		//RwFrameRotate(p_frame,&vecAxis,180.0f,rwCOMBINEREPLACE);
		RwFrameTranslate(p_frame,&vecPos,rwCOMBINEREPLACE);

		RpClumpRender(p_clump);
	}
#endif
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::reset()
{
	m_llObjectInstances.clear();
	m_llMultiBuildings.clear();
	m_llZones.clear();
	m_llCullZones.clear();
	m_llOcclusionVolumes.clear();
	m_llEntryExits.clear();
	m_llGarages.clear();
	m_llPickups.clear();
	m_llCarGenerators.clear();
	m_llStuntJumps.clear();
	m_llTimeCyclesMods.clear();
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
bool IplSet::add(BinaryStream& r_sRead)
{
	enum IPLLoadingStatus 
	{
		LOADING_NOTHING,
		LOADING_IGNORE,
		LOADING_INSTANCES,
		LOADING_MULTIBUILDINGS,
		LOADING_ZONES,
		LOADING_CULLZONES,
		LOADING_OCCLUSION,
		LOADING_PATHS,
		LOADING_GARAGE,
		LOADING_ENTRYEXIT,
		LOADING_PICKUPS,
		LOADING_CARGENERATORS,
		LOADING_STUNTJUMPS,
		LOADING_TIMECYCLEMODS
	};

	enum PathTypeEnum 
	{
		LOADING_PED_PATH,
		LOADING_CAR_PATH,
		LOADING_WATER_PATH
	};

	reset();

	GtaPath g_LoadingPath;
	bool bLoadingPath = false;
	char pLine[4096];
	IPLLoadingStatus status = LOADING_NOTHING;
	int32 pathNode = -1;
	int32 pathType = -1;
	int32 numLoadedBuildings = 0;
	
	while(r_sRead.getPending())
	{
		r_sRead.readLine(pLine,4096);

		if(*pLine == '\0')
			continue;

		if(*pLine == '#')
			continue;

		if(status == LOADING_NOTHING)
		{
			if(strncmp(pLine,"bnry",4)==0)
			{
				printf("binary file: not loaded\n");
				return false;
			}

			if(strncmp(pLine,"inst",4)==0)
				status = LOADING_INSTANCES;
			if(strncmp(pLine,"cars",4)==0)
				status = LOADING_CARGENERATORS;
#if 0
			//make sure only instances come through at the moment

			else if(strncmp(pLine,"mult",4)==0)
				status = LOADING_MULTIBUILDINGS;
			else if(strncmp(pLine,"zone",4)==0)
				status = LOADING_ZONES;
			else if(strncmp(pLine,"cull",4)==0)
				status = LOADING_CULLZONES;
			else if(strncmp(pLine,"path",4)==0)
				status = LOADING_PATHS;
			else if(strncmp(pLine,"occl",4)==0)
				status = LOADING_OCCLUSION;
			else if(strncmp(pLine,"grge",4)==0)
				status = LOADING_GARAGE;
			else if(strncmp(pLine,"enex",4)==0)
				status = LOADING_ENTRYEXIT;
			else if(strncmp(pLine,"pick",4)==0)
				status = LOADING_PICKUPS;
			else if(strncmp(pLine,"cars",4)==0)
				status = LOADING_STUNTJUMPS;
			else if(strncmp(pLine,"tcyc",4)==0)
				status = LOADING_TIMECYCLEMODS;
#endif
		}
		else
		{
			// 'end' tag indicates end of current section
			if(*pLine == 'e' && *(pLine+1) == 'n' && *(pLine+2) == 'd')
			{
				status = LOADING_NOTHING;

				if(bLoadingPath)
				{
					bLoadingPath = false;
					m_llPaths.push_back(g_LoadingPath);
				}
			}
			else
			{
				switch(status)
				{
				case LOADING_INSTANCES:
					txtLoadObjectInstance(pLine);
					break;
				case LOADING_MULTIBUILDINGS:
					txtLoadMultiBuilding(pLine);
					break;
				case LOADING_ZONES:
					txtLoadZone(pLine);
					break;
				case LOADING_CULLZONES:
					txtLoadCullZone(pLine);
					break;
				case LOADING_OCCLUSION:
					txtLoadOcclusionVolume(pLine);
					break;
				case LOADING_ENTRYEXIT:
					txtLoadEntryExit(pLine);
					break;
				case LOADING_GARAGE:
					txtLoadGarage(pLine);
					break;
				case LOADING_PATHS:
					if(pathNode == -1)
					{
						bLoadingPath = true;
						g_LoadingPath.m_llNodes.clear();

						txtLoadPathHeader(pLine, g_LoadingPath);
						pathNode = 0;
					}
					else
					{
						txtLoadPathNode(pLine, g_LoadingPath);

						pathNode++;
						if(pathNode == GTAFILE_MAXNUMNODESPEROBJECT)
						{
							bLoadingPath = false;
							m_llPaths.push_back(g_LoadingPath);
							pathNode = -1;
						}
					}
					break;
				case LOADING_PICKUPS:
					txtLoadPickup(pLine);
					break;
				case LOADING_CARGENERATORS:
					txtLoadCarGenerator(pLine);
					break;
				case LOADING_STUNTJUMPS:
					txtLoadStuntJump(pLine);
					break;
				case LOADING_TIMECYCLEMODS:
					txtLoadTimeCyclesModifier(pLine);
					break;
				case LOADING_IGNORE:
					break;	
				default:
					Assert(0);
					break;	
				}
			}
		}		
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::txtLoadObjectInstance(char* pLine)
{
	GtaObjectInstance objectInstance;

	char* pToken = strtok(pLine, " ");
	objectInstance.instanceIndex = atoi(pToken);

	pToken = strtok(NULL, " ");

	pLine = pToken + strlen(pToken) + 1;

	sscanf(pLine, "%d, %f, %f, %f, %f, %f, %f, %f, %d",	&objectInstance.areaCode,
														&objectInstance.posx, 
														&objectInstance.posy, 
														&objectInstance.posz,
														&objectInstance.q.x, 
														&objectInstance.q.y, 
														&objectInstance.q.z, 
														&objectInstance.q.w, 
														&objectInstance.lodIndex);

	m_llObjectInstances.push_back(objectInstance);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::txtLoadMultiBuilding(char* pLine)
{
	GtaMultiBuilding multiBuilding;

	char* pToken = strtok(pLine, " ");

	multiBuilding.num = atoi(pToken);

	if(multiBuilding.num > GTAFILE_MAX_BUILDINGS)
	{
		printf("error: can't have more than 16 building in a multi building\n");
		return;
	}

	for(int32 i=0; i<multiBuilding.num; i++)
	{
		pToken = strtok(NULL, " ");

		if(!pToken)
		{
			printf("error: reading building id's\n");
			return;
		}

		multiBuilding.buildings[i] = atoi(pToken);
	}
	pLine = pToken + strlen(pToken) + 1;

	sscanf(pLine, "%d, %d, %f, %f, %f, %f, %f, %f, %f, %d",	&multiBuilding.rule,
															&multiBuilding.areaCode,
															&multiBuilding.posn.x, 
															&multiBuilding.posn.y, 
															&multiBuilding.posn.z,
															&multiBuilding.q.x, 
															&multiBuilding.q.y, 
															&multiBuilding.q.z, 
															&multiBuilding.q.w, 
															&multiBuilding.lodIndex);

	m_llMultiBuildings.push_back(multiBuilding);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::txtLoadZone(char* pLine)
{
	GtaZone zone; 

	sscanf(pLine, "%s, %d, %f, %f, %f, %f, %f, %f, %d, %s",	zone.name,
															&zone.type,
															&zone.min.x, 
															&zone.min.y, 
															&zone.min.z,
															&zone.max.x, 
															&zone.max.y, 
															&zone.max.z, 
															&zone.level, 
															zone.text_label);

	m_llZones.push_back(zone);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::txtLoadCullZone(char* pLine)
{
	GtaCullZone cullZone;

	sscanf(pLine, "%f, %f, %f, %f, %f, %f, %f, %f, %f, %d, %d",	&cullZone.posn.x, 
																&cullZone.posn.y, 
																&cullZone.posn.z, 
																&cullZone.MinX, 
																&cullZone.MinY,
																&cullZone.MinZ, 
																&cullZone.MaxX, 
																&cullZone.MaxY, 
																&cullZone.MaxZ, 
																&cullZone.Flags, 
																&cullZone.wantedLevelDrop);

	m_llCullZones.push_back(cullZone);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::txtLoadOcclusionVolume(char* pLine)
{
	GtaOcclusionVolume occlusionVolume;

	sscanf(pLine, "%f, %f, %f, %f, %f, %f, %f",	&occlusionVolume.posn.x, 
												&occlusionVolume.posn.y, 
												&occlusionVolume.posn.z, 
												&occlusionVolume.width, 
												&occlusionVolume.length, 
												&occlusionVolume.height, 
												&occlusionVolume.zRot);

	m_llOcclusionVolumes.push_back(occlusionVolume);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::txtLoadEntryExit(char* pLine)
{
	GtaEntryExit entryExit;

	sscanf(pLine, "%f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %d, %d, %s, %d, %f, %f, %f, %f, %f, %f, %d, %d",	&entryExit.px,
																												&entryExit.py,
																												&entryExit.pz,
																												&entryExit.prot,
																												&entryExit.wx,
																												&entryExit.wy,
																												&entryExit.wz,
																												&entryExit.spawnx,
																												&entryExit.spawny,
																												&entryExit.spawnz,
																												&entryExit.spawnrot,
																												&entryExit.areacode,
																												&entryExit.flags,entryExit.cTitle,
																												&entryExit.extracol,
																												&entryExit.posX,
																												&entryExit.posY,
																												&entryExit.posZ,
																												&entryExit.normX,
																												&entryExit.normY,
																												&entryExit.normZ,
																												&entryExit.openTime, 
																												&entryExit.shutTime);

	m_llEntryExits.push_back(entryExit);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::txtLoadGarage(char* pLine)
{
	GtaGarage garage;

	sscanf(pLine, "%f, %f, %f, %f, %f, %f, %f, %f, %d, %d, %s",	&garage.x1,
																&garage.y1,
																&garage.z1,
																&garage.x2,
																&garage.y2,
																&garage.x3,
																&garage.y3,
																&garage.ztop,
																&garage.flag,
																&garage.type,
																&garage.cBuffer);

	m_llGarages.push_back(garage);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::txtLoadPathHeader(char* pLine, GtaPath& r_filePath)
{	
	sscanf(pLine, "%d", r_filePath.type);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::txtLoadPathNode(char* pLine, GtaPath& r_filePath)
{
	GtaPathNode pathNode;

	pathNode.density = 1.0f;
	pathNode.special = 0;

	sscanf(pLine, "%d, %d, %d, %f, %f, %f, %f, %d, %d, %d, %d, %f, %d",	&pathNode.type,
																		&pathNode.linkedTo, 
																		&pathNode.linkType, 
																		&pathNode.x, 
																		&pathNode.y, 
																		&pathNode.z, 
																		&pathNode.width, 
																		&pathNode.lanesIn, 
																		&pathNode.lanesOut, 
																		&pathNode.speed, 
																		&pathNode.flags, 
																		&pathNode.density, 
																		&pathNode.special);

	r_filePath.m_llNodes.push_back(pathNode);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::txtLoadPickup(char* pLine)
{
	GtaPickup pickup;

	sscanf(pLine, "%d, %f, %f, %f", &pickup.type, 
									&pickup.x, 
									&pickup.y, 
									&pickup.z);

	m_llPickups.push_back(pickup);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::txtLoadCarGenerator(char* pLine)
{	
	GtaCarGenerator carGenerator;

	sscanf(pLine, "%f, %f, %f, %f, %d, %d, %d, %d, %d, %d, %d, %d",	&carGenerator.x,
																	&carGenerator.y,
																	&carGenerator.z,
																	&carGenerator.r,
																	&carGenerator.modelID,
																	&carGenerator.col1,
																	&carGenerator.col2,
																	&carGenerator.flagVal,
																	&carGenerator.alarmChance,
																	&carGenerator.lockedChance,
																	&carGenerator.minDelay,
																	&carGenerator.maxDelay);

	m_llCarGenerators.push_back(carGenerator);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::txtLoadStuntJump(char* pLine)
{	
	GtaStuntJump stuntJump;

	sscanf(pLine, "%f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %d",	&stuntJump.fStartMinX,
																					&stuntJump.fStartMinY,
																					&stuntJump.fStartMinZ,
																					&stuntJump.fStartMaxX,
																					&stuntJump.fStartMaxY,
																					&stuntJump.fStartMaxZ,
																					&stuntJump.fEndMinX,
																					&stuntJump.fEndMinY,
																					&stuntJump.fEndMinZ,
																					&stuntJump.fEndMaxX,
																					&stuntJump.fEndMaxY,
																					&stuntJump.fEndMaxZ,
																					&stuntJump.fCamX,
																					&stuntJump.fCamY,
																					&stuntJump.fCamZ,
																					&stuntJump.iScore);

	m_llStuntJumps.push_back(stuntJump);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::txtLoadTimeCyclesModifier(char* pLine)
{
	GtaTimeCycleMod timeCycleMod;

	sscanf(pLine, "%f, %f, %f, %f, %f, %f, %d, %d, %f, %f",	&timeCycleMod.fMinX,
															&timeCycleMod.fMinY,
															&timeCycleMod.fMinZ,
															&timeCycleMod.fMaxX,
															&timeCycleMod.fMaxY,
															&timeCycleMod.fMaxZ,
															&timeCycleMod.farClip,
															&timeCycleMod.index,
															&timeCycleMod.fPercentage,
															&timeCycleMod.fRange);

	m_llTimeCyclesMods.push_back(timeCycleMod);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
bool IplSet::getBounds(CRect& r_rectBounds)
{
	if(m_llObjectInstances.size() == 0)
	{
		return false;
	}

	std::list<GtaObjectInstance>::iterator begin = m_llObjectInstances.begin();
	std::list<GtaObjectInstance>::iterator end = m_llObjectInstances.end();

	r_rectBounds.init(CVector2D(begin->posx,begin->posy));
	begin++;

	while(begin != end)
	{
		r_rectBounds.addPoint(CVector2D(begin->posx,begin->posy));
		begin++;
	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
void IplSet::saveBinary(BinaryStream& r_sWrite)
{
	//save out the file

	struct GtaHeader
	{
		char cHeader[4];

		int16 iNumObjectInstance;
		int16 iNumMultiBuilding;
		int16 iNumZone;
		int16 iNumCullZone;
		int16 iNumOcclusionVolume;
		int16 iNumEntryExit;
		int16 iNumGarage;
		int16 iNumPickup;
		int16 iNumCarGenerator;
		int16 iNumStuntJump;
		int16 iNumTimeCyclesMod;
		int16 iNumPath;

		int32 iObjectInstanceOffset;
		int32 iMultiBuildingOffset;
		int32 iZoneOffset;
		int32 iCullZoneOffset;
		int32 iOcclusionVolumeOffset;
		int32 iEntryExitOffset;
		int32 iGarageOffset;
		int32 iPickupOffset;
		int32 iCarGeneratorOffset;
		int32 iStuntJumpOffset;
		int32 iTimeCyclesModOffset;
		int32 iPathOffset;
	};

	GtaHeader fileHeader;

	memset(&fileHeader,0,sizeof(GtaHeader));

	strncpy(fileHeader.cHeader,"bnry",4);

	fileHeader.iNumObjectInstance = static_cast<int16>( m_llObjectInstances.size());
	fileHeader.iNumMultiBuilding = static_cast<int16>( m_llMultiBuildings.size());
	fileHeader.iNumZone = static_cast<int16>( m_llZones.size());
	fileHeader.iNumCullZone = static_cast<int16>( m_llCullZones.size());
	fileHeader.iNumOcclusionVolume = static_cast<int16>( m_llOcclusionVolumes.size());
	fileHeader.iNumEntryExit = static_cast<int16>( m_llEntryExits.size());
	fileHeader.iNumGarage = static_cast<int16>( m_llGarages.size());
	fileHeader.iNumPickup = static_cast<int16>( m_llPickups.size());
	fileHeader.iNumCarGenerator = static_cast<int16>( m_llCarGenerators.size());
	fileHeader.iNumStuntJump = static_cast<int16>( m_llStuntJumps.size());
	fileHeader.iNumTimeCyclesMod = static_cast<int16>( m_llTimeCyclesMods.size());
	fileHeader.iNumPath = static_cast<int16>( m_llPaths.size());

	int32 iCurrentOffset = 0;

	if(m_llObjectInstances.size() > 0)
	{
		fileHeader.iObjectInstanceOffset = sizeof(GtaHeader);
		iCurrentOffset += static_cast<int32>( m_llObjectInstances.size() * sizeof(GtaObjectInstance) );
	}
	if(m_llMultiBuildings.size() > 0)
	{
		fileHeader.iMultiBuildingOffset = iCurrentOffset + sizeof(GtaHeader);
		iCurrentOffset += static_cast<int32>( m_llMultiBuildings.size() * sizeof(GtaMultiBuilding) );
	}
	if(m_llZones.size() > 0)
	{
		fileHeader.iZoneOffset = iCurrentOffset + sizeof(GtaHeader);
		iCurrentOffset += static_cast<int32>( m_llZones.size() * sizeof(GtaZone) );
	}
	if(m_llCullZones.size() > 0)
	{
		fileHeader.iCullZoneOffset = iCurrentOffset + sizeof(GtaHeader);
		iCurrentOffset += static_cast<int32>( m_llCullZones.size() * sizeof(GtaCullZone) );
	}
	if(m_llOcclusionVolumes.size() > 0)
	{
		fileHeader.iOcclusionVolumeOffset = iCurrentOffset + sizeof(GtaHeader);
		iCurrentOffset += static_cast<int32>( m_llOcclusionVolumes.size() * sizeof(GtaOcclusionVolume) );
	}
	if(m_llEntryExits.size() > 0)
	{
		fileHeader.iEntryExitOffset = iCurrentOffset + sizeof(GtaHeader);
		iCurrentOffset += static_cast<int32>( m_llEntryExits.size() * sizeof(GtaEntryExit) );
	}
	if(m_llGarages.size() > 0)
	{
		fileHeader.iGarageOffset = iCurrentOffset + sizeof(GtaHeader);
		iCurrentOffset += static_cast<int32>( m_llGarages.size() * sizeof(GtaGarage) );
	}
	if(m_llPickups.size() > 0)
	{
		fileHeader.iPickupOffset = iCurrentOffset + sizeof(GtaHeader);
		iCurrentOffset += static_cast<int32>( m_llPickups.size() * sizeof(GtaPickup) );
	}
	if(m_llCarGenerators.size() > 0)
	{
		fileHeader.iCarGeneratorOffset = iCurrentOffset + sizeof(GtaHeader);
		iCurrentOffset += static_cast<int32>( m_llCarGenerators.size() * sizeof(GtaCarGenerator) );
	}
	if(m_llStuntJumps.size() > 0)
	{
		fileHeader.iStuntJumpOffset = iCurrentOffset + sizeof(GtaHeader);
		iCurrentOffset += static_cast<int32>( m_llStuntJumps.size() * sizeof(GtaStuntJump) );
	}
	if(m_llTimeCyclesMods.size() > 0)
	{
		fileHeader.iTimeCyclesModOffset = iCurrentOffset + sizeof(GtaHeader);
		iCurrentOffset += static_cast<int32>( m_llTimeCyclesMods.size() * sizeof(GtaTimeCycleMod) );
	}
	if(m_llPaths.size() > 0)
	{
		fileHeader.iPathOffset = iCurrentOffset + sizeof(GtaHeader);
	}

	r_sWrite.write(&fileHeader,sizeof(GtaHeader));

	//write object instances

	std::list<GtaObjectInstance>::iterator obBegin = m_llObjectInstances.begin();
	std::list<GtaObjectInstance>::iterator obEnd = m_llObjectInstances.end();

	while(obBegin != obEnd)
	{
		r_sWrite.write(&(*obBegin),sizeof(GtaObjectInstance));
		obBegin++;
	}

	//write  multi buildings

	std::list<GtaMultiBuilding>::iterator mbBegin = m_llMultiBuildings.begin();
	std::list<GtaMultiBuilding>::iterator mbEnd = m_llMultiBuildings.end();

	while(mbBegin != mbEnd)
	{
		r_sWrite.write(&(*mbBegin),sizeof(GtaMultiBuilding));
		mbBegin++;
	}

	//write zones

	std::list<GtaZone>::iterator zBegin = m_llZones.begin();
	std::list<GtaZone>::iterator zEnd = m_llZones.end();

	while(zBegin != zEnd)
	{
		r_sWrite.write(&(*zBegin),sizeof(GtaZone));
		zBegin++;
	}

	//write cull zones

	std::list<GtaCullZone>::iterator czBegin = m_llCullZones.begin();
	std::list<GtaCullZone>::iterator czEnd = m_llCullZones.end();

	while(czBegin != czEnd)
	{
		r_sWrite.write(&(*czBegin),sizeof(GtaCullZone));
		czBegin++;
	}

	//write occlusion volumes

	std::list<GtaOcclusionVolume>::iterator ovBegin = m_llOcclusionVolumes.begin();
	std::list<GtaOcclusionVolume>::iterator ovEnd = m_llOcclusionVolumes.end();

	while(ovBegin != ovEnd)
	{
		r_sWrite.write(&(*ovBegin),sizeof(GtaOcclusionVolume));
		ovBegin++;
	}

	//write entry exits

	std::list<GtaEntryExit>::iterator eeBegin = m_llEntryExits.begin();
	std::list<GtaEntryExit>::iterator eeEnd = m_llEntryExits.end();

	while(eeBegin != eeEnd)
	{
		r_sWrite.write(&(*eeBegin),sizeof(GtaEntryExit));
		eeBegin++;
	}

	//write garages

	std::list<GtaGarage>::iterator gBegin = m_llGarages.begin();
	std::list<GtaGarage>::iterator gEnd = m_llGarages.end();

	while(gBegin != gEnd)
	{
		r_sWrite.write(&(*gBegin),sizeof(GtaGarage));
		gBegin++;
	}

	//write pickups

	std::list<GtaPickup>::iterator pBegin = m_llPickups.begin();
	std::list<GtaPickup>::iterator pEnd = m_llPickups.end();

	while(pBegin != pEnd)
	{
		r_sWrite.write(&(*pBegin),sizeof(GtaPickup));
		pBegin++;
	}

	//write car generators

	std::list<GtaCarGenerator>::iterator cgBegin = m_llCarGenerators.begin();
	std::list<GtaCarGenerator>::iterator cgEnd = m_llCarGenerators.end();

	while(cgBegin != cgEnd)
	{
		r_sWrite.write(&(*cgBegin),sizeof(GtaCarGenerator));
		cgBegin++;
	}

	//write stunt jumps

	std::list<GtaStuntJump>::iterator sjBegin = m_llStuntJumps.begin();
	std::list<GtaStuntJump>::iterator sjEnd = m_llStuntJumps.end();

	while(sjBegin != sjEnd)
	{
		r_sWrite.write(&(*sjBegin),sizeof(GtaStuntJump));
		sjBegin++;
	}

	//write time cycles modifiers

	std::list<GtaTimeCycleMod>::iterator tcBegin = m_llTimeCyclesMods.begin();
	std::list<GtaTimeCycleMod>::iterator tcEnd = m_llTimeCyclesMods.end();

	while(tcBegin != tcEnd)
	{
		r_sWrite.write(&(*tcBegin),sizeof(GtaTimeCycleMod));
		tcBegin++;
	}

	//write path nodes (this should always be last unless you want to calculate
	//the exact size of the structure before writing it out)

	std::list<GtaPath>::iterator pathBegin = m_llPaths.begin();
	std::list<GtaPath>::iterator pathEnd = m_llPaths.end();

	while(pathBegin != pathEnd)
	{
		r_sWrite.write(&(*pathBegin),sizeof(int32));
		r_sWrite.write(&(*pathBegin),static_cast<uint32>(pathBegin->m_llNodes.size()));

		std::list<GtaPathNode>::iterator pnBegin = pathBegin->m_llNodes.begin();
		std::list<GtaPathNode>::iterator pnEnd = pathBegin->m_llNodes.end();

		while(pnBegin != pnEnd)
		{
			r_sWrite.write(&(*pnBegin),sizeof(GtaPathNode));

			pnBegin++;
		}

		pathBegin;
	}
}

