#ifndef COMMON_CPANLOAD
#define COMMON_CPANLOAD

#include <list>
#include <string>
#include <map>

namespace WinLib {

#define ANIM_NAME_LENGTH 24

class IffBlock;

#include "libcore/quaternion.h"
#include "libcore/vector.h"
#include "libcore/compressedfloat.h"

//////////////////////////////////////////////////////////////////////////////////////////////////
class GtaCamera
//////////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	static bool isMatch(float fA,float fB,float fTolerance = 0.0000005f);

	float maxTime() const;
	bool load(const char* p_cFilename);
	void getAtTime(float fTime,CVector& r_vecPos,CVector& r_vecDir,float& r_fFOV) const;

	bool getNextCut(int32& r_iKey) const;

	uint32 m_uNumFov;
	uint32 m_uNumRoll;
	uint32 m_uNumPos;
	uint32 m_uNumTarget;
	float* mp_fovTimes;
	float* mp_fovs;
	float* mp_posTimes;
	CVector* mp_pos;
	float* mp_targTimes;
	CVector* mp_target;

	static const float m_fCutTime;
};

//////////////////////////////////////////////////////////////////////////////////////////////////
struct OldAnimKeyRTS
//////////////////////////////////////////////////////////////////////////////////////////////////
{
	bool canCompress() const;

	float rx, ry, rz, rw;
	float tx, ty, tz;
	float sx, sy, sz;
	float time;
};

//////////////////////////////////////////////////////////////////////////////////////////////////
struct OldAnimKeyRT
//////////////////////////////////////////////////////////////////////////////////////////////////
{
	bool canCompress() const;

	float rx, ry, rz, rw;
	float tx, ty, tz;
	float time;
};

//////////////////////////////////////////////////////////////////////////////////////////////////
struct OldAnimKeyR
//////////////////////////////////////////////////////////////////////////////////////////////////
{
	bool canCompress() const;

	float rx, ry, rz, rw;
	float time;
};

//////////////////////////////////////////////////////////////////////////////////////////////////
struct AnimKeyRTS
//////////////////////////////////////////////////////////////////////////////////////////////////
{
	CQuaternion quat;
	float time;
	CVector pos;
	CVector scale;
};

//////////////////////////////////////////////////////////////////////////////////////////////////
struct AnimKeyRT
//////////////////////////////////////////////////////////////////////////////////////////////////
{
	bool canCompress() const;
	bool saveOptimised(BinaryStream& r_streamOut);
	bool loadOptimised(BinaryStream& r_streamIn);

	CQuaternion quat;
	float time;
	CVector pos;
};

//////////////////////////////////////////////////////////////////////////////////////////////////
struct AnimKeyR
//////////////////////////////////////////////////////////////////////////////////////////////////
{
	AnimKeyR() {}
	AnimKeyR(const AnimKeyRT& key) 
	{
		quat = key.quat;
		time = key.time;
	}

	bool canCompress() const;
	bool saveOptimised(BinaryStream& r_streamOut);
	bool loadOptimised(BinaryStream& r_streamIn);

	CQuaternion quat;
	float time;
};

//////////////////////////////////////////////////////////////////////////////////////////////////
struct AnimKeyRTCompressed
//////////////////////////////////////////////////////////////////////////////////////////////////
{
	bool saveOptimised(BinaryStream& r_streamOut);
	bool loadOptimised(BinaryStream& r_streamIn);

	CompressedQuat m_quatRotation;
	CompressedFloat m_fDeltaTime;
	CompressedVector m_vecTranslation;
};

//////////////////////////////////////////////////////////////////////////////////////////////////
struct AnimKeyRCompressed
//////////////////////////////////////////////////////////////////////////////////////////////////
{
	bool saveOptimised(BinaryStream& r_streamOut);
	bool loadOptimised(BinaryStream& r_streamIn);

	CompressedQuat m_quatRotation;
	CompressedFloat m_fDeltaTime;
};

//////////////////////////////////////////////////////////////////////////////////////////////////
struct AnimKeySet
//////////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	AnimKeySet();
	AnimKeySet(const AnimKeySet& r_aksIn);
	AnimKeySet(const AnimKeyRT* p_rtIn,uint32 uNumKeys);
	AnimKeySet(const AnimKeyR* p_rIn,uint32 uNumKeys);
	AnimKeySet(const OldAnimKeyRTS* p_rtsIn,uint32 uNumKeys);
	AnimKeySet(const OldAnimKeyRT* p_rtIn,uint32 uNumKeys);
	AnimKeySet(const OldAnimKeyR* p_rIn,uint32 uNumKeys);
	~AnimKeySet();

	void reset();
	void setName(const char* p_cName);
	void setBoneTag(int32 iBoneTag);

	bool saveOptimised(BinaryStream& r_streamOut);
	bool loadOptimised(BinaryStream& r_streamIn);
	
	bool isCompressed() const;
	bool canCompress() const;
	void compress();
	void uncompress();

	void removeRedundantTrans();
	bool hasTrans() const;
	bool removeTrans();
	bool addTrans(CVector r_vecPos);

	void startAndEnd(float fStartTime,float fEndTime);
	bool append(const AnimKeySet& r_aksAppend,float fAddTime);
	void cullTime(float fStartTime,float fEndTime);
	void subTime(float fStartTime,float fEndTime);

	void cameraCuts(const GtaCamera& r_cam);

	bool isVisible(bool bRootCheck,std::list<AnimKeySet*>& r_llParents,const GtaCamera& cam,float fTimeA,float fTimeB,float fRadius);
	bool isVisibleRot(bool bRootCheck,std::list<AnimKeySet*>& r_llParents,const GtaCamera& cam,float fTimeA,float fTimeB,float fRadius);
	bool isVisibleRotTrans(bool bRootCheck,std::list<AnimKeySet*>& r_llParents,const GtaCamera& cam,float fTimeA,float fTimeB,float fRadius);

	float lerp(float fBeforeVal,float fBeforeTime,float fAfterVal,float fAfterTime,float fCurrentTime);
	CVector lerp(const CVector& vecBeforeVal,float fBeforeTime,const CVector& vecAfterVal,float fAfterTime,float fCurrentTime);

	int32 getWriteSize();

	void getMatrixAtTime(float fTime,CMatrix& r_mat);
	void getMatrixAtKey(int32 iKey,CMatrix& r_mat);
	float getMaxTime();

	enum AnimKeyType
	{
		TYPE_UNKNOWN,

		TYPE_ROT,
		TYPE_ROT_TRANS,

		TYPE_ROT_COMPRESSED,
		TYPE_ROT_TRANS_COMPRESSED,
	};

	char mp_cName[ANIM_NAME_LENGTH];
	AnimKeyType m_iType;
	uint32 m_uNumKeys;
	int32	m_iBoneTag;
	int32	m_iStartLoop;
	int32	m_iEndLoop;
	int32	m_iFlags;

	union
	{
		AnimKeyRTCompressed* mp_crt;
		AnimKeyRCompressed* mp_cr;
		AnimKeyRTS* mp_rts; 
		AnimKeyRT* mp_rt;
		AnimKeyR* mp_r;
	};

	static float m_fVisTimeStep;
	static float m_fOptCamAngle;
	static float m_fOptCamDist;
	static const float m_fCutGap;

protected:
	void cameraCutsRot(const GtaCamera& r_cam);
	void cameraCutsRotTrans(const GtaCamera& r_cam);
	bool appendRot(const AnimKeySet& r_aksAppend,float fAddTime);
	bool appendRotTrans(const AnimKeySet& r_aksAppend,float fAddTime);
	void cullTimeRot(float fStartTime,float fEndTime);
	void cullTimeRotTrans(float fStartTime,float fEndTime);
	void subTimeRot(float fStartTime,float fEndTime);
	void subTimeRotTrans(float fStartTime,float fEndTime);
	void startAndEndRot(float fStartTime,float fEndTime);
	void startAndEndRotTrans(float fStartTime,float fEndTime);

	void removeQuatFlips();
};

//////////////////////////////////////////////////////////////////////////////////////////////////
class BoneTree
//////////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	BoneTree():mp_Next(NULL),mp_Child(NULL),mp_Parent(NULL) {}
	~BoneTree();

	void addChild(BoneTree* p_btChild);
	void addSibling(BoneTree* p_btSibling);
	BoneTree* findBone(int32 iBoneID);

	std::string m_strName;
	BoneTree* mp_Next;
	BoneTree* mp_Child;
	BoneTree* mp_Parent;

	static BoneTree* GetNext(BoneTree* mp_btBegin = NULL);

protected:

	static BoneTree* mp_btCurrent;
	static BoneTree* mp_btRoot;
};

//////////////////////////////////////////////////////////////////////////////////////////////////
class ComponentAnim
//////////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	friend class ComponentAnimSet;

	ComponentAnim();
	~ComponentAnim();

	void setName(const char* p_cName);
	const char* getName() { return mp_cName; }

	AnimKeySet* findSetByName(const char* p_cName);
	void removeRedundantTrans(BoneTree* p_btRoot);
	void cameraCuts(const char* p_cCameraFilename);
	bool optimiseToCamera(BoneTree* p_btRoot,const char* p_cCameraFilename,float fRadius);
	void cullTimeAll(float fStartTime,float fEndTime,bool bLeadInOut = true);
	void cullTime(AnimKeySet* p_aks,float fStartTime,float fEndTime,bool bLeadInOut = true);
	void subTime(float fStartTime,float fEndTime);
	void startAndEnd();
	bool append(const ComponentAnim& r_caAdd);
	void reset();
	bool load(BinaryStream& r_streamIn,bool bCompress = false);
	void save(BinaryStream& r_streamOut);
	bool saveDebug(const char* p_cFilename);

	bool addAnimKeySet(AnimKeySet* p_aksAdd);
	void calcWriteSize();

	static void clearBoneTagSet();
	static void addDefaultBoneTagSet();
	static bool addBoneIDTag(const char* pName,int32 iTagID);
	static int32 getBoneIDFromName(const char* pName);

	float getMaxTime();

	enum
	{
		MAX_NODES = 128
	};

	enum
	{
		FLAG_NONE = 0,
		FLAG_COMPRESSED = 1
	};

	char mp_cName[ANIM_NAME_LENGTH];
	int32 m_iNumNodes;
	int32 m_iWriteSize;
	int32 m_iFlags;
	int32 m_iTotalKeyFrames;
	AnimKeySet* mp_aksNodes[MAX_NODES];

protected:

	static std::map<std::string,int32> m_mapBoneTags;

	IffBlock* getForSave();

	bool saveOptimisedVer2(BinaryStream& r_streamOut);
	bool loadOptimisedVer2(BinaryStream& r_streamIn);

	bool saveOptimisedVer3(BinaryStream& r_streamOut);
	bool loadOptimisedVer3(BinaryStream& r_streamIn);

	void checkCompressConsistent();
	void tryAndCompress();
};

//////////////////////////////////////////////////////////////////////////////////////////////////
class ComponentAnimSet
//////////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	ComponentAnimSet(const char* pBlockName = NULL);
	~ComponentAnimSet();

	void reset();
	bool load(BinaryStream& r_streamIn,bool bCompress = false);
	void save(BinaryStream& r_streamOut);
	bool saveOptimisedVer2(BinaryStream& r_streamOut);
	bool saveOptimisedVer3(BinaryStream& r_streamOut);
	void addAnim(ComponentAnim* pComponentAnim);

	enum
	{
		MAX_COMPONENTANIMS = 4096
	};

	char m_cBlockName[ANIM_NAME_LENGTH];
	int32 m_iNumAnims;
	ComponentAnim* mpp_ComponentAnims[MAX_COMPONENTANIMS];
};

} //namespace WinLib {

#endif //COMMON_CPANLOAD
