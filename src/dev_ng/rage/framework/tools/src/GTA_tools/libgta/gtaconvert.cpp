#include <stdio.h>

#include "libcore/fstream.h"
#include "libcore/rect.h"
#include "libgta/gta.h"
#include "libgta/gtaconvert.h"
//#include "colmodel.h"
//#include "cpanload.h"

using namespace WinLib;

/*

void GtaConvertCOLInPack(RockstarPack& r_rpOptimise,const char* p_cTempDir,ConvertProgressCallback cbProgress)
{
	char cTempCOLDirectory[MAX_PATH];
	char cTempFile[MAX_PATH];
	uint8* p_cData;
	uint32 uLength;
	FileStream fsTemp;
	ColModelSet colSet;

	if(p_cTempDir)
	{
		strcpy(cTempCOLDirectory,p_cTempDir);
	}
	else
	{
		strcpy(cTempCOLDirectory,"c:\\coltemp\\");
	}

	FileStream::createDirectory(cTempCOLDirectory);

	RockstarPack::RockstarPackFileEntry rpeRead;

	r_rpOptimise.getNextFile(rpeRead,true);

	i32 iCount = 1;

	do
	{
		if(strstr(strlwr(rpeRead.m_rpeEntry.name),".col"))
		{
			cbProgress(iCount);

			sprintf(cTempFile,"%s%s",cTempCOLDirectory,rpeRead.m_rpeEntry.name);

			p_cData = r_rpOptimise.readFile(rpeRead,uLength);
	
			if(fsTemp.open(cTempFile,"wc"))
			{
				fsTemp.write(p_cData,uLength);
				fsTemp.close();
			}

			colSet.reset();
			fsTemp.open(cTempFile,"r");
			bool bAdded = colSet.load(fsTemp);
			fsTemp.close();

			if(bAdded)
			{
				fsTemp.open(cTempFile,"wc");
				colSet.save(fsTemp);
				fsTemp.close();
			}

			r_rpOptimise.replaceFile(cTempFile);

			iCount++;
		}
	}
	while(r_rpOptimise.getNextFile(rpeRead));
}

*/

void GtaConvertIPLInPack(RockstarPack& r_rpOptimise,const char* p_cTempDir,ConvertProgressCallback cbProgress)
{
	char cTempIPLDirectory[MAX_PATH];
	char cTempFile[MAX_PATH];
	uint8* p_cData;
	uint32 uLength;
	IplSet iplSet;
	FileStream* fsTemp = WinLib::FileStream::m_cbCreateStream();

	if(p_cTempDir)
	{
		strcpy(cTempIPLDirectory,p_cTempDir);
	}
	else
	{
		strcpy(cTempIPLDirectory,"c:\\ipltemp\\");
	}

	fsTemp->createDirectory(cTempIPLDirectory);

	RockstarPack::RockstarPackFileEntry rpeRead;

	r_rpOptimise.getNextFile(rpeRead,true);

	int32 iCount = 1;

	do
	{
		if(strstr(strlwr(rpeRead.m_rpeEntry.name),".ipl"))
		{
			cbProgress(iCount);

			sprintf(cTempFile,"%s%s",cTempIPLDirectory,rpeRead.m_rpeEntry.name);

			p_cData = r_rpOptimise.readFile(rpeRead,uLength);
	
			if(fsTemp->open(cTempFile,"wct"))
			{
				fsTemp->write(p_cData,uLength);
				fsTemp->close();
			}

			iplSet.reset();

			fsTemp->open(cTempFile,"r");
			bool bAdded = iplSet.add(*fsTemp);
			fsTemp->close();

			if(bAdded)
			{
				fsTemp->open(cTempFile,"wct");
				iplSet.saveBinary(*fsTemp);
				fsTemp->close();
			}

			r_rpOptimise.replaceFile(cTempFile);

			iCount++;
		}
	}
	while(r_rpOptimise.getNextFile(rpeRead));

	delete fsTemp;
}

/*

void GtaConvertIFPInPack(RockstarPack& r_rpOptimise,const char* p_cTempDir,ConvertProgressCallback cbProgress)
{
	char cTempIFPDirectory[MAX_PATH];
	char cTempFile[MAX_PATH];
	uint8* p_cData;
	uint32 uLength;
	ComponentAnimSet cpanSet;
	FileStream fsTemp;

	if(p_cTempDir)
	{
		strcpy(cTempIFPDirectory,p_cTempDir);
	}
	else
	{
		strcpy(cTempIFPDirectory,"c:\\ifptemp\\");
	}

	FileStream::createDirectory(cTempIFPDirectory);

	RockstarPack::RockstarPackFileEntry rpeRead;

	r_rpOptimise.getNextFile(rpeRead,true);

	i32 iCount = 1;

	do
	{
		if(strstr(strlwr(rpeRead.m_rpeEntry.name),".ifp"))
		{
			cbProgress(iCount);

			sprintf(cTempFile,"%s%s",cTempIFPDirectory,rpeRead.m_rpeEntry.name);

			p_cData = r_rpOptimise.readFile(rpeRead,uLength);
	
			if(fsTemp.open(cTempFile,"wc"))
			{
				fsTemp.write(p_cData,uLength);
				fsTemp.close();
			}

			cpanSet.reset();

			fsTemp.open(cTempFile,"r");
			bool bAdded = cpanSet.load(fsTemp,true);
			fsTemp.close();

			if(bAdded)
			{
				fsTemp.open(cTempFile,"wc");
				cpanSet.saveOptimisedVer3(fsTemp);
				fsTemp.close();
			}

			r_rpOptimise.replaceFile(cTempFile);

			iCount++;
		}
	}
	while(r_rpOptimise.getNextFile(rpeRead));
}

*/