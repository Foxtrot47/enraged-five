#ifndef COMMON_IFFLOAD_H
#define COMMON_IFFLOAD_H

#include "libcore/bstream.h"

namespace WinLib {

//////////////////////////////////////////////////////////////////////////////////////////////////
class IffBlock
//////////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	IffBlock(BinaryStream& r_streamIn);
	IffBlock(char *pTag,int32 size,void* ptr,bool owner = true);
	~IffBlock();

	void save(BinaryStream& r_streamOut);

	bool addBlock(IffBlock* p_iffbAdd);

    const char *getTagName() const { return mp_tag; }
    int32 getSize() const;
	int32 getWriteSize() const;
    const char *getData() const { return mp_cData; }
    char *getData() { return mp_cData; }

	int32 getNumBlocks() { return m_iNumBlocks; }
	IffBlock* getBlock(int32 iIndex) { if(iIndex >= m_iNumBlocks) return NULL; return mp_iffbData[iIndex]; }
	bool readBlocks();

protected:

	enum
	{
		MAX_BLOCKS = 1024
	};

	IffBlock* mp_iffbData[MAX_BLOCKS];
	int32 m_iNumBlocks;

    char mp_tag[5];
    int32 m_iSize;
    char *mp_cData;
	bool m_bOwner;
};

} //namespace WinLib {

#endif //COMMON_IFFLOAD_H
