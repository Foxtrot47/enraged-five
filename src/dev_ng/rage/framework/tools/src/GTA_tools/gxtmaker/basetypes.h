/*
 * basetypes.h
 * _____
 *
 * Contains basetypes that should be used throughout all RockstarNorth games
 *
 */
 
#ifndef INC_BASETYPES_H_
#define INC_BASETYPES_H_


/*
 * GTA Core FINAL defines
 */
#if __FINAL
	#define FINAL
	#define FINALBUILD
	#define MASTER
#endif

/*
 * type based defines
 */
#define MIN_INT8	(-128)
#define MAX_INT8	127
#define MIN_INT16	(-32768)
#define MAX_INT16	32767
#define MAX_INT32	2147483647
#define MIN_INT32	(-MAX_INT32-1)
#define MAX_UINT8	0xff
#define MAX_UINT16	0xffff
#define MAX_UINT32	0xffffffff


/* integer types */
typedef signed char 	Int8;
typedef signed char		int8;
typedef signed short	Int16;    
typedef signed short	int16;
typedef unsigned short  Char16;
typedef unsigned short  char16;
typedef signed int		Int32;
typedef signed int		int32;
#if __WIN32PC
typedef signed __int64	Int64;
typedef signed __int64	int64;
#elif __XENON
typedef signed __int64	Int64;
typedef signed __int64	int64;
#elif __PSN
typedef signed long		Int64;
typedef signed long		int64;
#else
#error "Unrecognised platform"
#endif

/* unsigned integer types */
typedef unsigned char 	UInt8;
typedef unsigned char	uint8;
typedef unsigned short	UInt16;
typedef unsigned short	uint16;
typedef unsigned int	UInt32;
typedef unsigned int	uint32;
#if __WIN32PC
typedef unsigned __int64	UInt64;
typedef unsigned __int64	uint64;
#elif __XENON
typedef unsigned __int64	UInt64;
typedef unsigned __int64	uint64;
#elif __PSN
typedef unsigned long		UInt64;
typedef unsigned long		uint64;
#else
#error "Unrecognised platform"
#endif

/* Boolean types (TRUE/FALSE) */
typedef unsigned char 	Bool8;
typedef unsigned char	bool8;

/*
 * Useful macros. All four macros assume nothing about type. The only macro 
 * to return a defined type is SIGN which returns an 'Int32' 
 */
#undef ABS
#undef MAX
#undef MIN
#undef SIGN

#define ABS(a)		(((a) < 0) ? (-(a)) : (a))
#define MAX(a, b)	(((a) > (b)) ? (a) : (b))
#define MIN(a, b)	(((a) < (b)) ? (a) : (b))
#define SIGN(a)		(int32)(((a) < 0) ? (-1) : (1))

/*
 * Useful defines
 */
#undef TRUE
#undef FALSE
#define TRUE		1
#define FALSE		0

#ifdef FINAL
	typedef const float TweakFloat;
	typedef const uint32 TweakUInt32;
	typedef const int32 TweakInt32;
#else
	typedef float TweakFloat;
	typedef uint32 TweakUInt32;
	typedef int32 TweakInt32;
#endif

using namespace rage;

#endif //INC_BASETYPES_H_
