/***************************************
 * gxtmaker.cpp                        *
 * ------------                        *
 *                                     *
 * program to convert txt files to gxt *
 *                                     *
 * KRH 12/05/98                        *
 *                                     *
 * based on fxtmaker KRH 30/01/96      *
 *                                     *
 * v1.1 - 24/08/98 KRH                 *
 * modified so that keys are stored as *
 * 8-bit ( everything else is 16-bit ) *
 *                                     *
 * v2.0 - 07/09/98 KRH                 *
 * indexing, and no encryption         *
 *                                     *
 * v2.5 - 26/05/99 KRH                 *
 * Spanish characters added            *
 *                                     *
 * v3.0,3.2 - 23/09/99 KRH             *
 * - non-translate options added       *
 *                                     *
 * v3.3 - 20/10/99 KRH                 *
 *  -r Russian option added            *
 *                                     *
 * (c) 1996-1998 DMA Design Ltd        *
 ***************************************/

#include <direct.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>

#include "dvant.h"
#include "terror.h"
#include "tfile.h"

#include "gxtmaker.h"

#include "basetypes.h"
#include "KeyGen.h"

#define MAX_ADDITIONAL_FILES	(50)

static Bool8 non_translate = FALSE;
static Bool8 non_translate_temporary = FALSE;
static Bool8 russian = FALSE;
static Bool8 use_unicode = false;
static Bool8 use_hash_keys = true;

data_class *pMainText;
key_array_class *pMainTextKeys;

key_array_class *pAllTextKeys;

data_class *pMissionText[MAX_MISSION_FILES];
key_array_class *pMissionTextKeys[MAX_MISSION_FILES];

char adding_to_block[MISSION_NAME_LENGTH];

struct sChunkSizes
{
	UInt32 ChunkIndex;
	UInt32 ChunkSize;
};

sChunkSizes ChunkSizesArray[MAX_MISSION_FILES];
UInt32 NumberOfEntriesInChunkSizesArray = 0;

FILE *fpBlockSizes = NULL;

static mission_file_class mission_files;

char error_message[150];

char start_dialogue_chunk[] = "GXTSTRT";
char end_dialogue_chunk[] = "GXTFINI";

char start_allow_long_keys[] = "ALLOW_LONG_STRT";
char end_allow_long_keys[] = "ALLOW_LONG_END";

static bool bAllowLongTextKeys = false;

const int MAX_FILES_ALLOWING_LONG_TEXT_KEYS = 5;

char *files_allowing_long_text_keys[MAX_FILES_ALLOWING_LONG_TEXT_KEYS];
UInt32 number_of_files_allowing_long_text_keys = 0;

const int MAX_LONG_TEXT_KEYS_TO_REPORT = 100;
u32 NumberOfLongTextKeys = 0;
char LongTextKeysToReport[MAX_LONG_TEXT_KEYS_TO_REPORT][KEY_SIZE];

const int MAX_LENGTH_OF_DIRECTORY_NAMES = 256;
char workingDirectory[MAX_LENGTH_OF_DIRECTORY_NAMES];
char pathForDebugFiles[MAX_LENGTH_OF_DIRECTORY_NAMES];
bool bPathForDebugFilesHasBeenSet = false;

Int32 wide_string_length(UInt16 *text)
{
	Int32 char_index;
	
	char_index = 0;
	
	while (text[char_index] != 0)
	{
		char_index++;
	}
	
	return char_index;
}

void wide_string_copy(UInt16 *dest_string, UInt16 *source_string)
{
	Int32 char_index;
	
	char_index = 0;
	
	if (source_string != NULL)
	{
		while (source_string[char_index] != 0)
		{
			dest_string[char_index] = source_string[char_index];
			char_index++;
		}
	}
	
	dest_string[char_index] = 0;
}



// ******************************* long strings *************************************************

#define NUMBER_OF_LONG_STRINGS	(10)
#define MAX_LONG_STRING_LENGTH	(2048)

struct LongStringData
{
	UInt32 length_of_string;
	char *key;
	UInt16 data[MAX_LONG_STRING_LENGTH];
};

LongStringData LongStrings[NUMBER_OF_LONG_STRINGS];

void InitLongestStrings()
{
	for (uint32 loop = 0; loop < NUMBER_OF_LONG_STRINGS; loop++)
	{
		LongStrings[loop].length_of_string = 0;
		LongStrings[loop].key = NULL;
		LongStrings[loop].data[0] = 0;
	}
}

void CheckAgainstLongestStrings(char *key, UInt16 *data)
{
	if (data == NULL)
	{
		return;
	}

	UInt32 length_of_new_string = wide_string_length(data);

	UInt32 index_of_shortest_string_entry = 0;
	UInt32 shortest_string_length = LongStrings[0].length_of_string;
	for (UInt32 loop = 1; loop < NUMBER_OF_LONG_STRINGS; loop++)
	{
		// Find current shortest string in the array
		if (LongStrings[loop].length_of_string < shortest_string_length)
		{
			index_of_shortest_string_entry = loop;
			shortest_string_length = LongStrings[loop].length_of_string;
		}
	}

	if (length_of_new_string > shortest_string_length)
	{
		if (length_of_new_string >= MAX_LONG_STRING_LENGTH)
		{
			error_str (ERR_LONG_STRING_IS_TOO_LONG, (char*)key);
		}

		LongStrings[index_of_shortest_string_entry].length_of_string = length_of_new_string;
		LongStrings[index_of_shortest_string_entry].key = key;
		wide_string_copy(LongStrings[index_of_shortest_string_entry].data, data);
	}
}

void WriteLongestStringsFile()
{
	FILE *fpLongestStrings = fopen("longest_strings.txt", "w");

	if (fpLongestStrings == NULL)
	{
		printf("**** Couldn't open longest_strings.txt - check it is writable");
		return;
	}

	for (UInt32 loop = 0; loop < NUMBER_OF_LONG_STRINGS; loop++)
	{
		if (LongStrings[loop].length_of_string > 0)
		{
			fprintf(fpLongestStrings, "%d %s %S\n\n\n", LongStrings[loop].length_of_string, LongStrings[loop].key, LongStrings[loop].data);
		}
	}

	fclose(fpLongestStrings);
}

// ******************************* end of long strings *************************************************

// **************************** invalid characters ***********************************************

const int MAX_LINES_CONTAINING_INVALID_CHARACTERS = 50;
const int MAX_LENGTH_OF_INVALID_CHARACTER = 7;
u32 NumberOfLinesContainingInvalidCharacters = 0;
char LinesContainingInvalidCharacters[MAX_LINES_CONTAINING_INVALID_CHARACTERS][KEY_SIZE];
char InvalidCharacters[MAX_LINES_CONTAINING_INVALID_CHARACTERS][MAX_LENGTH_OF_INVALID_CHARACTER];


void AddToArrayOfLinesContainingInvalidCharacters(const char *pTextLabel, const char *pInvalidCharacter)
{
	if (NumberOfLinesContainingInvalidCharacters < MAX_LINES_CONTAINING_INVALID_CHARACTERS)
	{
		strncpy(LinesContainingInvalidCharacters[NumberOfLinesContainingInvalidCharacters], pTextLabel, KEY_SIZE);
		strncpy(InvalidCharacters[NumberOfLinesContainingInvalidCharacters], pInvalidCharacter, MAX_LENGTH_OF_INVALID_CHARACTER);
	}
	NumberOfLinesContainingInvalidCharacters++;
}

void write_file_containing_keys_of_lines_with_invalid_characters(const char *pFilename)
{
	char invalid_keys_filename[256];
	strncpy(invalid_keys_filename, change_ext(pFilename, ".inv"), 256);

	if (bPathForDebugFilesHasBeenSet)
	{
		_chdir(pathForDebugFiles);
	}
	FILE *fpInvalidKeysFile = fopen(invalid_keys_filename, "w");

	if (bPathForDebugFilesHasBeenSet)
	{
		_chdir(workingDirectory);
	}

	if (fpInvalidKeysFile == NULL)
	{
		printf("**** Couldn't open file to write out the list of keys of lines containing invalid characters %s - check it is writable", invalid_keys_filename);
		exit (-1);
	}

	if (NumberOfLinesContainingInvalidCharacters > 0)
	{
		char warning_string[256];

		//	Output a list of the text keys to the file
		sprintf(warning_string, "There are %d lines containing invalid characters.\n", NumberOfLinesContainingInvalidCharacters);
		fprintf(fpInvalidKeysFile, warning_string);
		lprint(warning_string);

		if (NumberOfLinesContainingInvalidCharacters > MAX_LINES_CONTAINING_INVALID_CHARACTERS)
		{
			sprintf(warning_string, "Only the first %d are listed here\n", MAX_LINES_CONTAINING_INVALID_CHARACTERS);
			fprintf(fpInvalidKeysFile, warning_string);
			lprint(warning_string);
		}
		u32 NumberOfLinesContainingInvalidCharactersToOutput = MIN(NumberOfLinesContainingInvalidCharacters, MAX_LINES_CONTAINING_INVALID_CHARACTERS);
		for (u32 loop = 0; loop < NumberOfLinesContainingInvalidCharactersToOutput; loop++)
		{
			sprintf(warning_string, "%s contains %s\n", LinesContainingInvalidCharacters[loop], InvalidCharacters[loop]);
			fprintf(fpInvalidKeysFile, warning_string);
			lprint(warning_string);
		}

		//	Output an error
		sprintf(warning_string, "Warning : %d lines contain invalid characters. Check %s for a list\n", NumberOfLinesContainingInvalidCharacters, invalid_keys_filename);
		lprint(warning_string);
		//		error(ERR_INVALID_CHARACTERS_EXIST);
	}
	else
	{
		fprintf(fpInvalidKeysFile, "There are no lines containing invalid characters.\n");
	}

	fclose(fpInvalidKeysFile);
}


// **************************** end of invalid characters ***********************************************

/****************************** keys ***************************/

key_array_class::key_array_class(UInt32 number_of_keys)
{
	count = 0;
	total_number_of_keys = number_of_keys;
	
	pBuffer = (key_entry_class *) new key_entry_class[number_of_keys];
	if (pBuffer == NULL) error (ERR_MEMORY_KEY_ARRAY_CLASS);

}

key_array_class::~key_array_class(void)
{
	delete [] pBuffer;
}


static int compare(const void *elem1, const void *elem2)
{
	UInt8 str[20];
	int s;
	
	key_entry_class *pKey1, *pKey2;
	
	pKey1 = (key_entry_class *)elem1;
	pKey2 = (key_entry_class *)elem2;
	
	if (use_hash_keys)
	{
		if (pKey1->HashKey == pKey2->HashKey)
		{
			s = 0;
		}
		else if (pKey1->HashKey > pKey2->HashKey)
		{
			s = 1;
		}
		else
		{
			s = -1;
		}
	}
	else
	{
		s = strcmp((char*) (pKey1->key), (char*) (pKey2->key));
	}

	if (s==0) 
	{
		if (use_hash_keys)
		{
			sprintf((char*)str,"keys : %s and %s\n", (char*) (pKey1->key), (char*) (pKey2->key) );
			lprint((char*)str);
			error (ERR_HASH_KEY_CLASH);
		}
		else
		{
			sprintf((char*)str,"key : %s\n", (char*) (pKey1->key) );
			lprint((char*)str);
			error (ERR_DUPLICATE_KEYS);
		}
	}
	
	return s;
}


// add new key
void key_array_class::add(UInt8 *key, UInt16 *data)
{
	static UInt8 str[256];
	char error_string[128];
	UInt32 loop;
	
	Int32 s = (Int32) strlen((char*)key);

	if (count >= total_number_of_keys)
	{
		printf("**** Max number of keys = %d blockname = %s \n", total_number_of_keys, adding_to_block);
		error (ERR_KEY_COUNT);
	}
	
	for (loop = 0; loop < count; loop++)
	{
		if (strcmp( ((char*)key), ((char*)pBuffer[loop].key) ) == 0)
		{
			sprintf(error_string,"Duplicate key : %s\n", ((char*) key) );
			lprint((char*)error_string);
//			error (ERR_DUPLICATE_KEYS);
		}
	}
	
	pBuffer[count].data = data;
	if (s > (KEY_SIZE-1))
	{
		if (s<248)
		{
			sprintf((char*)str, "key : %s\n", key);
			lprint((char*)str);
		}
		error (ERR_KEY_SIZE);
	}

	if (!data)	//	Hacky way to check that this is pAllTextKeys
	{
		if ( (s > 15) && !bAllowLongTextKeys)
		{
			if (NumberOfLongTextKeys < MAX_LONG_TEXT_KEYS_TO_REPORT)
			{
				strncpy(LongTextKeysToReport[NumberOfLongTextKeys], (char*)key, KEY_SIZE);
			}
			NumberOfLongTextKeys++;
		}
	}


	strcpy((char*)pBuffer[count].key, (char*)key);
	if (s < (KEY_SIZE-1))
		_strnset((char*)(pBuffer[count].key+s+1), '\0', KEY_SIZE-1-s);
	
	pBuffer[count].HashKey = CKeyGen::GetUppercaseKey((const char *) pBuffer[count].key);
	
	count++;
}

// sort the keys !
void key_array_class::sort(void)
{
	qsort(pBuffer, count, sizeof(key_entry_class), compare);
}

// turn the keys into upper case
void key_array_class::uppercaserize(void)
{
	UInt32 C;
	for ( C = 0; C < count; C++)
	{
		pBuffer[C].uppercaserize();
	}
}

// turn the keys into upper case
void key_entry_class::uppercaserize(void)
{
	Int16	C;
	for ( C = 0; C < KEY_SIZE; C++)
	{
		if (key[C] >= 'a' && key[C] <= 'z')
		{
			key[C] += 'A' - 'a';
		}
	}
}



// save the key array, complete with a chunk header
void key_array_class::save(void)
{ 
	char *head = "TKEY";
	UInt32 size, loop;
	UInt32 IndexOfFirstCharacter;
	
	if (use_hash_keys)	// save hashkey else save string
	{
		size = count * 8;	//	UInt16 *data; + UInt32 HashKey;
	}
	else
	{
		size = count * (4 + KEY_SIZE);	//	UInt16 *data; + UInt8 key[KEY_SIZE];
	}
	
	write_file (head, 4);
	write_file ((char*)&size, 4);

	fprintf(fpBlockSizes, "Number of Keys = %d\n", count);
	fprintf(fpBlockSizes, "Size of Key Block = %d\n", size);

//	write_file ((char*)pBuffer, s);
	for (loop = 0; loop < count; loop++)
	{
		IndexOfFirstCharacter = (UInt32) pBuffer[loop].data;
		if (use_unicode == false)
		{
			IndexOfFirstCharacter /= 2;
		}
//		write_file((char*)&pBuffer[loop].data, 4);
		write_file((char*)&IndexOfFirstCharacter, 4);
		if (use_hash_keys)
		{
			write_file((char*)&pBuffer[loop].HashKey, 4);
		}
		else
		{
			write_file((char*)&pBuffer[loop].key[0], KEY_SIZE);
		}
	}
}

/*************************** end of keys *********************/

/******************************* data *****************************/

data_class::data_class(UInt32 size)
{
	count = 0;
	size_of_buffer = size;
	
    pBuffer = (UInt16*) new UInt16[size_of_buffer];
    if (pBuffer == NULL) error (ERR_MEMORY_DATA_CLASS);
}

data_class::~data_class(void)
{
	delete [] pBuffer;
}


// add new text
// returns		: relative pointer
UInt16 *data_class::add(UInt16 *text)
{
	UInt16 *return_value = (UInt16*)(count*2);
	Int32 s = wide_string_length(text);

	if (s+count >= size_of_buffer)
	{
		printf("**** Max memory for text = %d blockname = %s \n", size_of_buffer, adding_to_block);
		error (ERR_TEXT_SIZE);
	}
	wide_string_copy(pBuffer+count, text);
	count += s + 1;

	return (return_value);
}


// save the data, complete with a chunk header
void data_class::save(void)
{ 
	char *head = "TDAT";
	UInt32 s;
	char *pCurrentBufferPosition;
	char curr_char;
	UInt32 char_loop;
	
	if (use_unicode)
	{
		s = sizeof(UInt16)*count;
	}
	else
	{
		s = sizeof(UInt8)*count;
	}

	write_file (head, 4);
	write_file ((char*)&s, 4);

	fprintf(fpBlockSizes, "Size of Data Block = %d\n", s);

	if (use_unicode)
	{
		write_file ((char*)pBuffer, s); 
	}
	else
	{
		pCurrentBufferPosition = (char*)pBuffer;
		for (char_loop = 0; char_loop < count; char_loop++)
		{
			curr_char = *pCurrentBufferPosition;
			write_file (pCurrentBufferPosition, 1);
			pCurrentBufferPosition++;
			pCurrentBufferPosition++;
		}
	}
}

/****************************** end of data ************************/
/*
UInt16 foreign_table[128] =
{ 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  ,  // 128 - 143
  0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  ,  // 144 - 159
  0  , 94 , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  ,  // 160 - 175
  0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 175,  // 176 - 191
  128, 129, 130, 0  , 131, 0  , 132, 133, 134, 135, 136, 137, 138, 139, 140, 141,  // 192 - 207
  0  , 173, 142, 143, 144, 0  , 145, 0  , 0  , 146, 147, 148, 149, 0  , 0  , 150,  // 208 - 223
  151, 152, 153, 0  , 154, 0  , 155, 156, 157, 158, 159, 160, 161, 162, 163, 164,  // 224 - 239
  0  , 174, 165, 166, 167, 0  , 168, 0  , 0  , 169, 170, 171, 172, 0  , 0  , 0   };// 240 - 255

UInt16 russian_table[128] =
{ 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  ,  // 128 - 143
  0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  ,  // 144 - 159
  0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 177, 0  , 0  , 0  , 0  , 0  , 0  , 0  ,  // 160 - 175
  0  , 0  , 0  , 0  , 0  , 0  , 0  , 0  , 210, 0  , 0  , 0  , 0  , 0  , 0  , 0  ,  // 176 - 191
  193, 208, 192, 184, 198, 182, 199, 187, 205, 178, 181, 197, 204, 183, 196, 194,  // 192 - 207
  195, 203, 206, 180, 190, 188, 179, 202, 185, 186, 189, 191, 207, 200, 209, 201,  // 208 - 223
  226, 241, 225, 217, 231, 215, 232, 220, 238, 211, 214, 230, 237, 216, 229, 227,  // 224 - 239
  228, 236, 239, 213, 223, 221, 212, 235, 218, 219, 222, 224, 240, 233, 242, 234 };// 240 - 255

UInt16 used_foreign[258];
*/

/*
UInt16 current_word[100];
UInt16 current_letter;
FILE *fpBigWords;

void clear_big_word_data(void)
{
	UInt16 loop;

	for (loop = 0; loop < 100; loop++)
	{
		current_word[loop] = 0;
	}

	current_letter = 0;
}

void add_letter_to_big_word(UInt16 character, char *strt_key)
{
	current_word[current_letter++] = character;

	if (current_letter >= 100)
	{
		error_str (ERR_WORD_TOO_LONG, (char*)strt_key);
	}
}

void store_big_word(char *start_key)
{
	if (current_letter > 15)
	{
		current_word[current_letter] = 0;
		fprintf(fpBigWords, "%s %S\n",start_key, current_word);
	}
}
*/

// dealing with foreign characters (European)
UInt16 character_code (UInt8 a, char *strt_key)
{
/*
	UInt16 c, b;

	if ( (a<128) || non_translate || non_translate_temporary ) return ( (UInt16)a );
*/
	Int16 stop_var;

	stop_var = 11;

	if (a > 255)
	{
		stop_var = 23;
		if (use_unicode == false)
		{
			printf("**** %s Character requires more than 8 bits to store - ASCII code %d\n", strt_key, a);
			exit(-1);
		}
	}
	else
	{
		return ( (UInt16)a );
	}

//	used_foreign[a]++;
/*
	if (russian)
		c = russian_table[a-128];
	else
		c = foreign_table[a-128];

	if (c == 0)
	{
		stop_var = 43;
		printf("**** %s Unknown character - ASCII code %d\n", strt_key, a);
		exit(-1);
	}

	b = c - 32;

	if (used_foreign[b] == 0)
	{
		used_foreign[b]++;
	}

	return c;
*/
}


bool CheckForInvalidCharacter(u32 CharacterToTest, UInt16 *out, Int32 &out_index, char *strt_key)
{
	switch (CharacterToTest)
	{
		case 8211 :
		case 8212 :
			out[out_index++] = character_code('-', strt_key);
			printf("Replacing invalid character in %s with -\n", strt_key);
			return true;
			break;

		case 8216 :
		case 8217 :
			out[out_index++] = character_code('\'', strt_key);
			printf("Replacing invalid character in %s with \'\n", strt_key);
			return true;
			break;

		case 8220 :
		case 8221 :
			out[out_index++] = character_code('\"', strt_key);
			printf("Replacing invalid character in %s with \"\n", strt_key);
			return true;
			break;

		case 8230 :
			out[out_index++] = character_code('.', strt_key);
			out[out_index++] = character_code('.', strt_key);
			out[out_index++] = character_code('.', strt_key);
			printf("Replacing invalid character in %s with ...\n", strt_key);
			return true;
			break;
	}

	return false;
}

u32 ExtractCharacterCodeFromUTF8Sequence(UInt8 *pArrayOfUtf8Bytes, u32 NumberOfUtf8Bytes, const char *strt_key)
{
	u32 FullCharacterCode = 0;
	u32 CharacterCodeComponents[4];

	switch (NumberOfUtf8Bytes)
	{
	case 2 :
		CharacterCodeComponents[0] = pArrayOfUtf8Bytes[0];
		CharacterCodeComponents[0] &= 0x1f;

		CharacterCodeComponents[1] = pArrayOfUtf8Bytes[1];
		CharacterCodeComponents[1] &= 0x3f;

		FullCharacterCode = (CharacterCodeComponents[0] << 6) | CharacterCodeComponents[1];
		break;

	case 3 :
		CharacterCodeComponents[0] = pArrayOfUtf8Bytes[0];
		CharacterCodeComponents[0] &= 0x0f;

		CharacterCodeComponents[1] = pArrayOfUtf8Bytes[1];
		CharacterCodeComponents[1] &= 0x3f;

		CharacterCodeComponents[2] = pArrayOfUtf8Bytes[2];
		CharacterCodeComponents[2] &= 0x3f;

		FullCharacterCode = (CharacterCodeComponents[0] << 12) | (CharacterCodeComponents[1] << 6) | CharacterCodeComponents[2];
		break;

	case 4 :
		CharacterCodeComponents[0] = pArrayOfUtf8Bytes[0];
		CharacterCodeComponents[0] &= 0x07;

		CharacterCodeComponents[1] = pArrayOfUtf8Bytes[1];
		CharacterCodeComponents[1] &= 0x3f;

		CharacterCodeComponents[2] = pArrayOfUtf8Bytes[2];
		CharacterCodeComponents[2] &= 0x3f;

		CharacterCodeComponents[3] = pArrayOfUtf8Bytes[3];
		CharacterCodeComponents[3] &= 0x3f;

		FullCharacterCode = (CharacterCodeComponents[0] << 18) | (CharacterCodeComponents[1] << 12) | (CharacterCodeComponents[2] << 6) | CharacterCodeComponents[3];
		break;

	default:
		printf("**** %s Expected a UTF-8 character to have between 2 and 4 bytes\n", strt_key);
		exit(-1);
		break;
	}

	return FullCharacterCode;
}

#define BIT3	(1<<3)
#define BIT4	(1<<4)
#define BIT5	(1<<5)
#define BIT6	(1<<6)
#define BIT7	(1<<7)


u32 FindNumberOfContinuationCharactersInUtf8Sequence(u8 ByteToCheck, char *strt_key)
{
	if ((ByteToCheck & 192) == 192)
	{	//	The top two bits are set
		u32 number_of_continuation_bytes = 1;

		if (ByteToCheck & BIT5)
		{
			number_of_continuation_bytes++;
			if (ByteToCheck & BIT4)
			{
				number_of_continuation_bytes++;
			}
		}

		if (ByteToCheck & BIT3)
		{
			printf("**** %s Character has bits 7,6 and 3 set. The gxtmaker can't deal with UTF-8 characters with more than 4 bytes\n", strt_key);
			exit(-1);
		}

		return number_of_continuation_bytes;
	}

	return 0;
}

void character_code_with_utf8_check(UInt8 *in, Int32 inlen, Int32 &character_index, char *strt_key, UInt16 *out, Int32 &out_index)
{
	if (character_index >= inlen)
	{
		printf("**** %s Character index %d is beyond the end of the buffer %d\n", strt_key, character_index, inlen);
		exit(-1);
	}

	u32 number_of_continuation_bytes = FindNumberOfContinuationCharactersInUtf8Sequence(in[character_index], strt_key);
	if (number_of_continuation_bytes > 0)
	{	//	This is the first byte of a UTF-8 sequence
		bool bIsUtf8Character = true;

		UInt8 CurrentUtf8Bytes[MAX_LENGTH_OF_INVALID_CHARACTER];
		u32 NumberOfUtf8Bytes = 0;

		for (u32 clear_byte_loop = 0; clear_byte_loop < MAX_LENGTH_OF_INVALID_CHARACTER; clear_byte_loop++)
		{
			CurrentUtf8Bytes[clear_byte_loop] = 0;
		}

		CurrentUtf8Bytes[NumberOfUtf8Bytes++] = in[character_index];

		for (u32 continuation_byte_loop = 0; continuation_byte_loop < number_of_continuation_bytes; continuation_byte_loop++)
		{
			character_index++;

			if (character_index < inlen)
			{
				if ( (in[character_index] & BIT7) && ((in[character_index] & BIT6) == 0) )
				{	//	The top two bits of each continuation byte should be 10
					CurrentUtf8Bytes[NumberOfUtf8Bytes++] = in[character_index];
				}
				else
				{
					bIsUtf8Character = false;
					AddToArrayOfLinesContainingInvalidCharacters(strt_key, (char*) CurrentUtf8Bytes);
				}
			}
			else
			{
				bIsUtf8Character = false;
				AddToArrayOfLinesContainingInvalidCharacters(strt_key, (char*) CurrentUtf8Bytes);
			}
		}

		if (bIsUtf8Character)
		{
			u32 FullCharacterCode = ExtractCharacterCodeFromUTF8Sequence(CurrentUtf8Bytes, NumberOfUtf8Bytes, strt_key);

			if (!CheckForInvalidCharacter(FullCharacterCode, out, out_index, strt_key))
			{
				for (u32 utf8_byte_loop = 0; utf8_byte_loop < NumberOfUtf8Bytes; utf8_byte_loop++)
				{
					out[out_index++] = character_code(CurrentUtf8Bytes[utf8_byte_loop], strt_key);
				}

				printf("Info - %s contains UTF8 character %s (ASCII code %u)\n", strt_key, CurrentUtf8Bytes, FullCharacterCode);
			}
		}
	}
	else if ((in[character_index] & 128) == 128)
	{
		char TempDebugString[2];
		TempDebugString[0] = in[character_index];
		TempDebugString[1] = '\0';
		AddToArrayOfLinesContainingInvalidCharacters(strt_key, TempDebugString);
	}
	else
	{
		out[out_index++] = character_code(in[character_index], strt_key);
	}
}

Int16 number_of_characters_between_tilde;

//	Returns true if the character is not a ~ or between two ~s
Bool8 handle_tilde(UInt16 character)	//	, char *strt_key)
{
	if ( character == '~')
	{
		if (number_of_characters_between_tilde == -1)
		{
			number_of_characters_between_tilde = 0;
		}
		else
		{
			if (number_of_characters_between_tilde != 1)
			{
//				error_str (ERR_TILDE_ERROR, (char*)strt_key);
			}

			number_of_characters_between_tilde = -1;
		}
	}
	else
	{
		if (number_of_characters_between_tilde >= 0)
		{
			number_of_characters_between_tilde++;

			if (number_of_characters_between_tilde > 1)
			{
//				error_str (ERR_TILDE_ERROR, (char*)strt_key);
			}
		}
		else
		{
			return true;
		}
	}
	
	return false;
}


/*
 * encrypt
 * -------
 *
 * function             : encrypt loaded file
 * input                : buffer, size, key
 *
 */

//static void encrypt ( UInt16 *buffer, Int32 size, UInt16 key )
//{
//        Int32 i;
//        UInt8 key1 =  key&0x00FF;
//        UInt8 key2 = (key&0xFF00)>>8;
//
//        for (i=0; i<size; i++)
//        {
//                buffer[i] += key1;
//                key1 += key2;
//                key2 += key2;
//        }
//}


Bool8 is_digit(UInt16 c)
{
	return ((c>='0') && (c<='9'));
}

//static Int32 stop=0;
/*
static Bool8 check_for_gang_briefing(UInt16 *start_data, UInt8 *start_key)
{
	UInt16 *p;

//	if ( (start_key[0]=='3') && (start_key[1]=='1') && (start_key[2]=='8') && (start_key[3]=='3') )
//		stop = 1;

	if ( (strlen((char*)start_key)==6) && 
		 is_digit(start_key[0]) &&
		 is_digit(start_key[1]) &&
		 is_digit(start_key[2]) &&
		 is_digit(start_key[3]) &&
		 (start_key[4]=='_') )
	{ // found a gang briefing key
		// turn it into a normal key
		start_key[4]=0; 
		// add control code to the data

		// find the end of the data
		p = start_data;
		while(*p)
			p++;

		// now move everything up 1
		while(p>=start_data)
		{
			*(p+1) = *p;
			p--;
		}

		// now put in the control code
		*start_data = start_key[5] + ('!'<<8) ;

		return (TRUE);
	}
	return (FALSE);
}
*/

Bool8 white_space(UInt16 c)
{
        return ( (c==' ') || (c=='\t') || (c=='\x0A') || (c=='\x0D') );
}


void check_for_netui ( UInt8 *start_key )
{
	if ( (strlen((char*)start_key) >= 5) && !strncmp((char*)start_key,"netui",5) )
		non_translate_temporary = TRUE;
	else
		non_translate_temporary = FALSE;
}


void add_to_correct_mission_chunk(UInt16 *start_data, UInt8 *start_key)
{
	bool Found = false;
	UInt16 mission_file_index;
	UInt32 length_of_key;
	UInt16 *data_relative;
	char mission_name[MISSION_NAME_LENGTH];
	UInt16 loop, mission_name_loop;
	
	for (loop = 0; loop < MISSION_NAME_LENGTH; loop++)
	{
		mission_name[loop] = 0;
	}
	
//	scan start_key for :
//	anything after this is the mission this text should appear in
//	anything before this is the actual text key
//	start key is null terminated
	
	length_of_key = strlen((const char *)start_key);
	
	loop = 0;
	Found = false;
	while ( (loop < length_of_key) && !Found)
	{
		if (start_key[loop] == ':')
		{
			Found = true;
			start_key[loop] = 0;
		}
		
		loop++;
	}
	
	if (Found)
	{
		mission_name_loop = 0;
		
		while (loop < length_of_key)
		{
			mission_name[mission_name_loop] = start_key[loop];
			start_key[loop] = 0;
			loop++;
			mission_name_loop++;
			
			if (mission_name_loop >= MISSION_NAME_LENGTH)
			{
				lprint("Mission name is too long");
				error(ERR_WORD_TOO_LONG);
			}
		}
		
		mission_name[mission_name_loop] = 0;
	}
	
	
	if (mission_name[0])
	{	//	This text should be added to a mission - find the correct mission slot
		Found = false;
		mission_file_index = 0;
		
		while ( (mission_file_index < mission_files.num_of_mission_files) && (!Found) )
		{
			if (strcmp(mission_files.filenames[mission_file_index], mission_name) == 0)
			{
				Found = true;
			}
			else
			{
				mission_file_index++;
			}
		}
		
		if (Found)
		{	//	add to the mission with index mission_text_index
			strncpy(adding_to_block, mission_name, MISSION_NAME_LENGTH);
			data_relative = pMissionText[mission_file_index]->add(start_data);
			pMissionTextKeys[mission_file_index]->add(start_key, data_relative);
			CheckAgainstLongestStrings((char *)start_key, start_data);
		}
		else
		{	//	error message
			error_str (ERR_UNKNOWN_MISSION_NAME, (char*)start_key);
		}
	}
	else
	{
		strncpy(adding_to_block, "main", MISSION_NAME_LENGTH);
		data_relative = pMainText->add(start_data);
		pMainTextKeys->add(start_key, data_relative);
		CheckAgainstLongestStrings((char *)start_key, start_data);
	}
	
	strncpy(adding_to_block, "AllText", MISSION_NAME_LENGTH);
	pAllTextKeys->add(start_key, NULL);
}



void do_convert_asc ( UInt8 *in, Int32 inlen, UInt16 *out, Int32 *outlen)
{
        Int32 i,j=0;
		Bool8 half = FALSE;
        enum {WHITE_SPACE, HEADER, TEXT, WHITE_SPACE_INTERNAL, FIRST_WHITE_SPACE, COMMENT_WHITE_SPACE, COMMENT_FIRST_WHITE_SPACE, COMMENT_WHITE_SPACE_INTERNAL, COMMENT_TEXT} state = FIRST_WHITE_SPACE;
		UInt8 *start_key = NULL;
		UInt16 *start_data = NULL;
//		UInt16 *data_relative;
		Int32 nest = 0;
//		Bool8 result;
		Bool8 bInDialogueChunk = false;

        for (i=0; i<inlen; i++)
        {
                switch (state)
                {
						case COMMENT_FIRST_WHITE_SPACE:
								if ( in[i]=='{' )
								{
//									nest++;
									error_str (ERR_NESTED_COMMENTS, (char*)start_key);
								}
								else if ( in[i]=='}' )
								{
									if (nest==0)
										state = FIRST_WHITE_SPACE;
									else
										nest--;
								}
								break;

						case COMMENT_WHITE_SPACE:
								if ( in[i]=='{' )
								{
//									nest++;
									error_str (ERR_NESTED_COMMENTS, (char*)start_key);
								}
								else if ( in[i]=='}' )
								{
									if (nest==0)
										state = WHITE_SPACE;
									else
										nest--;
								}
								break;

						case COMMENT_WHITE_SPACE_INTERNAL:
								if ( in[i]=='{' )
								{
//									nest++;
									error_str (ERR_NESTED_COMMENTS, (char*)start_key);
								}
								else if ( in[i]=='}' )
								{
									if (nest==0)
										state = WHITE_SPACE_INTERNAL;
									else
										nest--;
								}
								break;

						case COMMENT_TEXT:
								if ( in[i]=='{' )
								{
//									nest++;
									error_str (ERR_NESTED_COMMENTS, (char*)start_key);
								}
								else if ( in[i]=='}' )
								{
									if (nest==0)
									{
										state = TEXT;
//										clear_big_word_data();
									}
									else
										nest--;
								}
								break;

                        case FIRST_WHITE_SPACE:
                                if ( in[i]=='[' )
                                {
                                        out[j++] = in[i];
                                        state = HEADER;
								        if (number_of_characters_between_tilde != -1)
								        {
								        	error_str(ERR_TILDE, (char*)start_key);
								        }
										start_key = &in[i+1];
                                }
								else if ( in[i]=='{')
									state = COMMENT_FIRST_WHITE_SPACE;
								else if ( in[i]=='}' )
								{
									error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
								}

                                break;
                        
                        case WHITE_SPACE:
                                if ( in[i]=='[' )
                                {
                                        out[j++] = '\0';
										
										if (start_data)
										{
//											result = check_for_gang_briefing(start_data, start_key);
//											if (result) j++;
//											data_relative = pMainText->add(start_data);
//											pMainTextKeys->add(start_key, data_relative);
											add_to_correct_mission_chunk(start_data, start_key);
											start_data = NULL;
										}

                                        out[j++] = in[i];
                                        state = HEADER;
								        if (number_of_characters_between_tilde != -1)
								        {
								        	error_str(ERR_TILDE, (char*)start_key);
								        }
										start_key = &in[i+1];
                                }
								else if ( in[i]=='{')
									state = COMMENT_WHITE_SPACE;
								else if ( in[i]=='}' )
								{
									error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
								}
                                else if (!white_space ( in[i] ))
                                {
										start_data = &out[j];
/*	Switched this off for GTA4 - Simon said he can add ~z~ to the beginning of text as it is exported from the spreadsheet if needed
										if (bInDialogueChunk)
										{
											out[j++] = (UInt16) '~';
											out[j++] = (UInt16) 'z';
											out[j++] = (UInt16) '~';
										}
*/
										character_code_with_utf8_check(in, inlen, i, (char*)start_key, out, j);
                                        state = TEXT;
										handle_tilde(in[i]);	//	, (char*)start_key);
//										clear_big_word_data();
//										add_letter_to_big_word( character_code(in[i], (char*)start_key), (char*)start_key );
                                }
                                break;
                        
                        case WHITE_SPACE_INTERNAL:
                                if ( in[i]=='[' )
                                {
                                        out[j++] = '\0';

										if (start_data)
										{
//											result = check_for_gang_briefing(start_data, start_key);
//											if (result) j++;
//											data_relative = pMainText->add(start_data);
//											pMainTextKeys->add(start_key, data_relative);
											add_to_correct_mission_chunk(start_data, start_key);
											start_data = NULL;
										}

                                        out[j++] = in[i];
                                        state = HEADER;
								        if (number_of_characters_between_tilde != -1)
								        {
								        	error_str(ERR_TILDE, (char*)start_key);
								        }
										start_key = &in[i+1];
                                }
								else if ( in[i]=='{')
									state = COMMENT_WHITE_SPACE_INTERNAL;
								else if ( in[i]=='}' )
								{
									error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
								}
                                else if (!white_space ( in[i] ))
                                {
                                        out[j++] = ' ';
										character_code_with_utf8_check(in, inlen, i, (char*)start_key, out, j);
                                        state = TEXT;
										handle_tilde(in[i]);	//	, (char*)start_key);
//										clear_big_word_data();
//										add_letter_to_big_word( character_code(in[i], (char*)start_key), (char*)start_key );
                                }                                       
                                break;
                        
                        case HEADER:
								if (white_space(in[i]))
								{
									error_str (ERR_WHITESPACE_IN_HEADER, (char*)start_key);
								}
								else if ( in[i]==']' )
                                {
									in[i] = '\0';
									check_for_netui(start_key);
									if (half) j++;
	                                out[j++] = in[i];
									half = FALSE;
                                    state = WHITE_SPACE;
                                    
                                    if (strcmp((char *) start_key, start_dialogue_chunk) == 0)
	                                {
                                    	bInDialogueChunk = true;
                                    }
                                    
                                    if (strcmp((char *) start_key, end_dialogue_chunk) == 0)
                                    {
                                    	bInDialogueChunk = false;
                                    }

									if (strcmp((char *) start_key, start_allow_long_keys) == 0)
									{
										bAllowLongTextKeys = true;
									}

									if (strcmp((char *) start_key, end_allow_long_keys) == 0)
									{
										bAllowLongTextKeys = false;
									}
                                }
								else if ( in[i]=='}' )
								{
									error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
								}
								else
								{
									if (half)
									{
										out[j] |= in[i]<<8;
										half = FALSE;
										j++;
									}
									else
									{
										out[j] = in[i];
										half = TRUE;
									}
								}
                                break;
                                
                        case TEXT:
                                if (white_space(in[i]))
								{
                                    state = WHITE_SPACE_INTERNAL;
//									store_big_word((char*)start_key);
								}
								else if ( in[i]=='{')
								{
									state = COMMENT_TEXT;
//									store_big_word((char*)start_key);
								}
								else if ( in[i]=='}' )
								{
									error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
								}
                                else
                                {
									character_code_with_utf8_check(in, inlen, i, (char*)start_key, out, j);

                                    if ( in[i]=='[' )
										error_str(ERR_NO_WHITESPACE_BEFORE_HEADER, (char*)start_key);

                                    if ( in[i]==']' )
										error_str(ERR_CLOSING_BRACKET_IN_TEXT, (char*)start_key);

									handle_tilde(in[i]);	//	, (char*)start_key);
//									add_letter_to_big_word( character_code(in[i], (char*)start_key), (char*)start_key );
                                }

                                break;                                                                                          

                        default:
                                break;
                }

        }
        
        if (number_of_characters_between_tilde != -1)
        {
        	error_str(ERR_TILDE, (char*)start_key);
        }
        
        if ( state == HEADER ) 
			error_str (ERR_BRACKET, (char*)start_key);
		else if ( ( state == COMMENT_FIRST_WHITE_SPACE ) ||
				  ( state == COMMENT_WHITE_SPACE ) ||
				  ( state == COMMENT_WHITE_SPACE_INTERNAL ) ||
				  ( state == COMMENT_TEXT ) )
			error_str (ERR_COMMENT_BRACKET, (char*)start_key);

        out[j++]='\0';
        out[j++]='[';
        out[j++]=']';

        *outlen = j;
}

#define MAX_NUM_OF_JAPANESE_CHARACTERS	(2000)

japanese_character JapaneseCharacters[MAX_NUM_OF_JAPANESE_CHARACTERS];
UInt16 number_of_japanese_characters;

void clear_japanese_metrics_file_data(void)
{
	UInt16 loop;
	
	for (loop = 0; loop < MAX_NUM_OF_JAPANESE_CHARACTERS; loop++)
	{
		JapaneseCharacters[loop].CharacterCode = 0;
		JapaneseCharacters[loop].Left = 0;
		JapaneseCharacters[loop].Top = 0;
		JapaneseCharacters[loop].Width = 0;
		JapaneseCharacters[loop].Height = 0;
	}
	number_of_japanese_characters = 0;
}

Bool8 read_one_word(FILE *pInputFile, char NewWord[], UInt16 MaxWordSize)
{
	UInt16 loop;
	UInt8 current_char;
	Bool8 bFinished, bReadAProperCharacter;
	Int32 s;
	
	for (loop = 0; loop < MaxWordSize; loop++)
	{
		NewWord[loop] = 0;
	}

	bFinished = false;
	bReadAProperCharacter = false;
	loop = 0;
	while ((!bFinished)  && !feof(pInputFile))
	{
		s = fread(&current_char, 1, 1, pInputFile);
		if (s == 1)
		{
			if (white_space(current_char))
			{	//	Ignore leading white space
				if (bReadAProperCharacter)
				{
					bFinished = true;
				}
			}
			else
			{
				NewWord[loop++] = current_char;
				bReadAProperCharacter = true;
			}
		}
	}
	
	return bReadAProperCharacter;
}

Bool8 IsInt(char String[])
{
	Int16	Index;

	Index = 0;

	if (String[0] == 0) return false;

	while (String[Index] != 0)
	{
		if ( (String[Index] < '0' || String[Index] > '9') && String[Index] != '-')
		{
			return false;
		}
		Index++;
	}
	return true;
}

void read_japanese_metrics_file(char *file_name)
{
	FILE *fpMetricsFile;
	
	#define SIZE_OF_CURRENT_WORD (30)
	char curr_word[SIZE_OF_CURRENT_WORD];

	fpMetricsFile = fopen(file_name, "r");

	if (fpMetricsFile == NULL)
	{
//		printf("**** Couldn't open japanese metrics file %s\n", file_name);
//		exit (-1);
		error(ERR_READ_METRICS_FILE);
	}
	
	read_one_word(fpMetricsFile, &curr_word[0], SIZE_OF_CURRENT_WORD);
	if (strcmp(curr_word, "METRICS1") != 0)
	{
//		printf("**** Japanese metrics file should begin with METRICS1\n");
//		fclose(fpMetricsFile);
//		exit(-1);
		error(ERR_METRICS_HEADER);
	}
	
	read_one_word(fpMetricsFile, &curr_word[0], SIZE_OF_CURRENT_WORD);
	read_one_word(fpMetricsFile, &curr_word[0], SIZE_OF_CURRENT_WORD);
	
	if (strcmp(curr_word, "5") != 0)
	{
//		printf("**** Header of Japanese metrics file should end with 5\n");
//		fclose(fpMetricsFile);
//		exit(-1);
		error(ERR_METRICS_HEADER_2);
	}
	
	number_of_japanese_characters = 0;
	
//	ready to read in the data for each Japanese character now
	while (!feof(fpMetricsFile))
	{
//	Character Code
		if (read_one_word(fpMetricsFile, &curr_word[0], SIZE_OF_CURRENT_WORD))
		{
			if (!IsInt(curr_word))
			{
				error(ERR_METRICS_DATA);
			}
			JapaneseCharacters[number_of_japanese_characters].CharacterCode = atoi(curr_word);

//	Left
			read_one_word(fpMetricsFile, &curr_word[0], SIZE_OF_CURRENT_WORD);
			
			if (!IsInt(curr_word))
			{
				error(ERR_METRICS_DATA);
			}

			JapaneseCharacters[number_of_japanese_characters].Left = atoi(curr_word);

//	Top
			read_one_word(fpMetricsFile, &curr_word[0], SIZE_OF_CURRENT_WORD);
			
			if (!IsInt(curr_word))
			{
				error(ERR_METRICS_DATA);
			}

			JapaneseCharacters[number_of_japanese_characters].Top = atoi(curr_word);

//	Width
			read_one_word(fpMetricsFile, &curr_word[0], SIZE_OF_CURRENT_WORD);
			
			if (!IsInt(curr_word))
			{
				error(ERR_METRICS_DATA);
			}
			JapaneseCharacters[number_of_japanese_characters].Width = atoi(curr_word) - JapaneseCharacters[number_of_japanese_characters].Left;

//	Height
			read_one_word(fpMetricsFile, &curr_word[0], SIZE_OF_CURRENT_WORD);
			
			if (!IsInt(curr_word))
			{
				error(ERR_METRICS_DATA);
			}
			JapaneseCharacters[number_of_japanese_characters].Height = atoi(curr_word) - JapaneseCharacters[number_of_japanese_characters].Top;

			number_of_japanese_characters++;
		}
	}
	
	fclose(fpMetricsFile);
}

void save_japanese_character_information(void)
{
	UInt16 loop;
	
	if (number_of_japanese_characters == 0)
		return;
	
	char *head = "TJAP";
	UInt32 s = sizeof(japanese_character) * number_of_japanese_characters;

	write_file(head, 4);
	write_file((char*)&s, 4);
	for (loop = 0; loop < number_of_japanese_characters; loop++)
	{
		write_file((char*)(&JapaneseCharacters[loop]), sizeof(japanese_character));
	}
}

UInt16 convert_unicode_character_to_japanese_index(UInt16 unicode_character)
{
	Bool8 bFound;
	UInt16 table_index;

	bFound = false;
	table_index = 0;
	
	while ( (table_index < number_of_japanese_characters) && !bFound)
	{
		if (JapaneseCharacters[table_index].CharacterCode == unicode_character)
		{
			bFound = true;
		}
		else
		{
			table_index++;
		}
	}
	
	if (!bFound)
	{
		error(ERR_MISSING_JAPANESE_CHARACTER);
	}
	
//	table_index |= 0x8000;
	table_index += 32;
	
	return table_index;
}

/*
void do_convert_shiftjis ( UInt8 *in, Int32 inlen, UInt16 *out, Int32 *outlen)
{
		UInt16 c;
        Int32 i=0,j=0;
		Bool8 half = FALSE;
        enum {WHITE_SPACE, HEADER, TEXT, WHITE_SPACE_INTERNAL, FIRST_WHITE_SPACE, COMMENT_WHITE_SPACE, COMMENT_FIRST_WHITE_SPACE, COMMENT_WHITE_SPACE_INTERNAL, COMMENT_TEXT} state = FIRST_WHITE_SPACE;
		UInt8 *start_key = NULL;
		UInt16 *start_data = NULL;
//		UInt16 *data_relative;
		Int32 nest = 0;
//		Bool8 result;

        while(i<inlen)
        {
                if (in[i] & 0x80)
                { // shift-jis
                        c= ((UInt8)in[i]<<8) + (UInt8)in[i+1];
                        i+=2;
                }
                else
                { // ascii
                        c = in[i];
                        i++;
                }
                switch (state)
                {
						case COMMENT_FIRST_WHITE_SPACE:
								if ( c=='{' )
								{
//									nest++;
									error_str (ERR_NESTED_COMMENTS, (char*)start_key);
								}
								else if ( c=='}' )
								{
									if (nest==0)
										state = FIRST_WHITE_SPACE;
									else
										nest--;
								}
								break;

						case COMMENT_WHITE_SPACE:
								if ( c=='{' )
								{
//									nest++;
									error_str (ERR_NESTED_COMMENTS, (char*)start_key);
								}
								else if ( c=='}' )
								{
									if (nest==0)
										state = WHITE_SPACE;
									else
										nest--;
								}
								break;

						case COMMENT_WHITE_SPACE_INTERNAL:
								if ( c=='{' )
								{
//									nest++;
									error_str (ERR_NESTED_COMMENTS, (char*)start_key);
								}
								else if ( c=='}' )
								{
									if (nest==0)
										state = WHITE_SPACE_INTERNAL;
									else
										nest--;
								}
								break;

						case COMMENT_TEXT:
								if ( c=='{' )
								{
//									nest++;
									error_str (ERR_NESTED_COMMENTS, (char*)start_key);
								}
								else if ( c=='}' )
								{
									if (nest==0)
									{
										state = TEXT;
										clear_big_word_data();
									}
									else
										nest--;
								}
								break;

                        case FIRST_WHITE_SPACE:
                                if ( c=='[' )
                                {
                                        out[j++] = c;
                                        state = HEADER;
								        if (number_of_characters_between_tilde != -1)
								        {
								        	error_str(ERR_TILDE, (char*)start_key);
								        }
										start_key = &in[i];
                                }
								else if ( c=='{')
									state = COMMENT_FIRST_WHITE_SPACE;
								else if ( c=='}' )
								{
									error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
								}

                                break;
                        
                        case WHITE_SPACE:
                                if ( c=='[' )
                                {
                                        out[j++] = '\0';

										if (start_data)
										{
//											result = check_for_gang_briefing(start_data, start_key);
//											if (result) j++;
//											data_relative = pMainText->add(start_data);
//											pMainTextKeys->add(start_key, data_relative);
											add_to_correct_mission_chunk(start_data, start_key);
											start_data = NULL;
										}
                                        out[j++] = c;
                                        state = HEADER;
								        if (number_of_characters_between_tilde != -1)
								        {
								        	error_str(ERR_TILDE, (char*)start_key);
								        }
										start_key = &in[i];
                                }
								else if ( c=='{')
									state = COMMENT_WHITE_SPACE;
								else if ( c=='}' )
								{
									error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
								}
                                else if (!white_space ( c ))
                                {
										start_data = &out[j];
                                        out[j++] = c;
                                        state = TEXT;
										handle_tilde(c, (char*)start_key);
										clear_big_word_data();
										add_letter_to_big_word( c, (char*)start_key );
                                }
                                break;
                        
                        case WHITE_SPACE_INTERNAL:
                                if ( c=='[' )
                                {
                                        out[j++] = '\0';

										if (start_data)
										{
//											result = check_for_gang_briefing(start_data, start_key);
//											if (result) j++;
//											data_relative = pMainText->add(start_data);
//											pMainTextKeys->add(start_key, data_relative);
											add_to_correct_mission_chunk(start_data, start_key);
											start_data = NULL;
										}
                                        out[j++] = c;
                                        state = HEADER;
								        if (number_of_characters_between_tilde != -1)
								        {
								        	error_str(ERR_TILDE, (char*)start_key);
								        }
										start_key = &in[i];
                                }
								else if ( c=='{')
									state = COMMENT_WHITE_SPACE_INTERNAL;
								else if ( c=='}' )
								{
									error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
								}
                                else if (!white_space ( c ))
                                {
                                        out[j++] = ' ';
                                        out[j++] = c;
                                        state = TEXT;
										handle_tilde(c, (char*)start_key);
										clear_big_word_data();
										add_letter_to_big_word( c, (char*)start_key );
                                }                                       
                                break;
                        
                        case HEADER:
                                if ( c==']' )
                                {
									in[i-1] = '\0';
									c = '\0';
									if (half) j++;
	                                out[j++] = c;
									half = FALSE;
                                    state = WHITE_SPACE;
                                }
								else if ( c=='}' )
								{
									error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
								}
								else
								{
									if (half)
									{
										out[j] |= c<<8;
										half = FALSE;
										j++;
									}
									else
									{
										out[j] = c;
										half = TRUE;
									}
								}
                                break;
                                
                        case TEXT:
                                if (white_space(c))
								{
									state = WHITE_SPACE_INTERNAL;
									store_big_word((char*)start_key);
								}
								else if ( c=='{')
								{
									state = COMMENT_TEXT;
									store_big_word((char*)start_key);
								}
								else if ( c=='}' )
								{
									error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
								}
                                else
                                {
                                    out[j++] = c;

                                    if ( c=='[' )
										error_str(ERR_NO_WHITESPACE_BEFORE_HEADER, (char*)start_key);

									handle_tilde(c, (char*)start_key);
									add_letter_to_big_word( c, (char*)start_key );
                                }

                                break;                                                                                          

                        default:
                                break;
                }

        }

        if (number_of_characters_between_tilde != -1)
        {
        	error_str(ERR_TILDE, (char*)start_key);
        }
        
        if ( state == HEADER ) error_str (ERR_BRACKET, (char*)start_key);
		else if ( ( state == COMMENT_FIRST_WHITE_SPACE ) ||
				  ( state == COMMENT_WHITE_SPACE ) ||
				  ( state == COMMENT_WHITE_SPACE_INTERNAL ) ||
				  ( state == COMMENT_TEXT ) )
			error_str (ERR_COMMENT_BRACKET, (char*)start_key);

        out[j++]='\0';
        out[j++]='[';
        out[j++]=']';

        *outlen = j;
}
*/

void do_convert_japanese(UInt8 *in, Int32 inlen, UInt16 *out, Int32 *outlen)
{
		UInt16 c;
        Int32 i=0,j=0;
//		Bool8 half = FALSE;
        enum {WHITE_SPACE, HEADER, TEXT, WHITE_SPACE_INTERNAL, FIRST_WHITE_SPACE, COMMENT_WHITE_SPACE, COMMENT_FIRST_WHITE_SPACE, COMMENT_WHITE_SPACE_INTERNAL, COMMENT_TEXT} state = FIRST_WHITE_SPACE;
//		UInt8 *start_key = NULL;
		UInt8 start_key[50];	//	needs to be big enough to hold :mission name too?
		UInt8 start_key_index = 0;
		UInt16 *start_data = NULL;
//		UInt16 *data_relative;
		Int32 nest = 0;
//		Bool8 result;
		Bool8 bInDialogueChunk = false;

        while(i<inlen)
        {
//                if (in[i] & 0x80)
//                { // shift-jis
//                        c= ((UInt8)in[i]<<8) + (UInt8)in[i+1];
				c= ((UInt8)in[i+1]<<8) + (UInt8)in[i];
                i+=2;
                        
			//	Convert TrueType characters into normal ASCII ones
			//	~ 65374 and / 65295 are not converted as the ASCII characters are used for other things
				if ((c >= 65281) && (c <= 65373))
				{
					if (c != 65295)
					{
						c -= 65248;
					}
				}

/*
                }
                else
                { // ascii
                        c = in[i];
                        i++;
                }
*/
                switch (state)
                {
						case COMMENT_FIRST_WHITE_SPACE:
								if ( c=='{' )
								{
//									nest++;
									error_str (ERR_NESTED_COMMENTS, (char*)start_key);
								}
								else if ( c=='}' )
								{
									if (nest==0)
										state = FIRST_WHITE_SPACE;
									else
										nest--;
								}
								break;

						case COMMENT_WHITE_SPACE:
								if ( c=='{' )
								{
//									nest++;
									error_str (ERR_NESTED_COMMENTS, (char*)start_key);
								}
								else if ( c=='}' )
								{
									if (nest==0)
										state = WHITE_SPACE;
									else
										nest--;
								}
								break;

						case COMMENT_WHITE_SPACE_INTERNAL:
								if ( c=='{' )
								{
//									nest++;
									error_str (ERR_NESTED_COMMENTS, (char*)start_key);
								}
								else if ( c=='}' )
								{
									if (nest==0)
										state = WHITE_SPACE_INTERNAL;
									else
										nest--;
								}
								break;

						case COMMENT_TEXT:
								if ( c=='{' )
								{
//									nest++;
									error_str (ERR_NESTED_COMMENTS, (char*)start_key);
								}
								else if ( c=='}' )
								{
									if (nest==0)
									{
										state = TEXT;
//										clear_big_word_data();
									}
									else
										nest--;
								}
								break;

                        case FIRST_WHITE_SPACE:
                                if ( c=='[' )
                                {
                                        out[j++] = c;
                                        state = HEADER;
								        if (number_of_characters_between_tilde != -1)
								        {
								        	error_str(ERR_TILDE, (char*)start_key);
								        }
								        start_key[0] = 0;
								        start_key_index = 0;
//										start_key = &in[i];
                                }
								else if ( c=='{')
									state = COMMENT_FIRST_WHITE_SPACE;
								else if ( c=='}' )
								{
									error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
								}

                                break;
                        
                        case WHITE_SPACE:
                                if ( c=='[' )
                                {
                                        out[j++] = '\0';

										if (start_data)
										{
//											result = check_for_gang_briefing(start_data, start_key);
//											if (result) j++;
//											data_relative = pMainText->add(start_data);
//											pMainTextKeys->add(start_key, data_relative);
											add_to_correct_mission_chunk(start_data, &start_key[0]);
											start_data = NULL;
										}
                                        out[j++] = c;
                                        state = HEADER;
								        if (number_of_characters_between_tilde != -1)
								        {
								        	error_str(ERR_TILDE, (char*)start_key);
								        }
								        start_key[0] = 0;
								        start_key_index = 0;
//										start_key = &in[i];
                                }
								else if ( c=='{')
									state = COMMENT_WHITE_SPACE;
								else if ( c=='}' )
								{
									error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
								}
                                else if (!white_space ( c ))
                                {
										start_data = &out[j];
/*	Switched this off for GTA4 - Simon said he can add ~z~ to the beginning of text as it is exported from the spreadsheet if needed
										if (bInDialogueChunk)
										{
											out[j++] = ((UInt16) '~') | 0x8000;	//	looks like need to set the topmost
											out[j++] = ((UInt16) 'z') | 0x8000;	//	bit for tildes and any characters
											out[j++] = ((UInt16) '~') | 0x8000;	//	between them in a Japanese text file
										}
*/
                                        state = TEXT;
//										clear_big_word_data();
//										add_letter_to_big_word( c, (char*)start_key );
										
										if (handle_tilde(c))
										{
											c = convert_unicode_character_to_japanese_index(c);
										}
										else
										{
											c |= 0x8000;
										}
                                        out[j++] = c;
                                }
                                break;
                        
                        case WHITE_SPACE_INTERNAL:
                                if ( c=='[' )
                                {
                                        out[j++] = '\0';

										if (start_data)
										{
//											result = check_for_gang_briefing(start_data, start_key);
//											if (result) j++;
//											data_relative = pMainText->add(start_data);
//											pMainTextKeys->add(start_key, data_relative);
											add_to_correct_mission_chunk(start_data, &start_key[0]);
											start_data = NULL;
										}
                                        out[j++] = c;
                                        state = HEADER;
								        if (number_of_characters_between_tilde != -1)
								        {
								        	error_str(ERR_TILDE, (char*)start_key);
								        }
								        start_key[0] = 0;
								        start_key_index = 0;
//										start_key = &in[i];
                                }
								else if ( c=='{')
									state = COMMENT_WHITE_SPACE_INTERNAL;
								else if ( c=='}' )
								{
									error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
								}
                                else if (!white_space ( c ))
                                {
                                        out[j++] = ' ';
                                        state = TEXT;
//										clear_big_word_data();
//										add_letter_to_big_word( c, (char*)start_key );
										if (handle_tilde(c))
										{
											c = convert_unicode_character_to_japanese_index(c);
										}
										else
										{
											c |= 0x8000;
										}

                                        out[j++] = c;
                                }
                                break;
                        
                        case HEADER:
                                if (white_space(c))
								{
									error_str (ERR_WHITESPACE_IN_HEADER, (char*)start_key);
								}
								else if ( c==']' )
                                {
//									in[i-1] = '\0';
//									c = '\0';
//									if (half) j++;
//	                                out[j++] = c;
//									half = FALSE;
							        start_key[start_key_index] = 0;
							        start_key_index++;

                                    state = WHITE_SPACE;
                                    
                                    if (strcmp((char *) start_key, start_dialogue_chunk) == 0)
	                                {
                                    	bInDialogueChunk = true;
                                    }
                                    
                                    if (strcmp((char *) start_key, end_dialogue_chunk) == 0)
                                    {
                                    	bInDialogueChunk = false;
                                    }

									if (strcmp((char *) start_key, start_allow_long_keys) == 0)
									{
										bAllowLongTextKeys = true;
									}

									if (strcmp((char *) start_key, end_allow_long_keys) == 0)
									{
										bAllowLongTextKeys = false;
									}
                                }
								else if ( c=='}' )
								{
									error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
								}
								else
								{
							        if (c > 255)
							        {
							        	error(ERR_HEADER_CHARACTER);
							        }
							        start_key[start_key_index] = c;
							        start_key_index++;
							        start_key[start_key_index] = 0;
/*
									if (half)
									{
										out[j] |= c<<8;
										half = FALSE;
										j++;
									}
									else
									{
										out[j] = c;
										half = TRUE;
									}
*/
								}
                                break;
                                
                        case TEXT:
                                if (white_space(c))
								{
									state = WHITE_SPACE_INTERNAL;
//									store_big_word((char*)start_key);
								}
								else if ( c=='{')
								{
									state = COMMENT_TEXT;
//									store_big_word((char*)start_key);
								}
								else if ( c=='}' )
								{
									error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
								}
                                else
                                {
                                    if ( c=='[' )
										error_str(ERR_NO_WHITESPACE_BEFORE_HEADER, (char*)start_key);

                                    if ( c==']' )
										error_str(ERR_CLOSING_BRACKET_IN_TEXT, (char*)start_key);

//									add_letter_to_big_word( c, (char*)start_key );
									if (handle_tilde(c))
									{
										c = convert_unicode_character_to_japanese_index(c);
									}
									else
									{
										c |= 0x8000;
									}

                                    out[j++] = c;
                                }

                                break;                                                                                          

                        default:
                                break;
                }

        }
	    if (number_of_characters_between_tilde != -1)
	    {
	    	error_str(ERR_TILDE, (char*)start_key);
	    }
        
        if ( state == HEADER ) error_str (ERR_BRACKET, (char*)start_key);
		else if ( ( state == COMMENT_FIRST_WHITE_SPACE ) ||
				  ( state == COMMENT_WHITE_SPACE ) ||
				  ( state == COMMENT_WHITE_SPACE_INTERNAL ) ||
				  ( state == COMMENT_TEXT ) )
			error_str (ERR_COMMENT_BRACKET, (char*)start_key);

        out[j++]='\0';
        out[j++]='[';
        out[j++]=']';

        *outlen = j;
}

void do_convert ( UInt8 *in, Int32 inlen, UInt16 *out, Int32 *outlen, Bool8 bJapanese )
{
        if (bJapanese)
                do_convert_japanese( in, inlen, out, outlen );
        else
                do_convert_asc (in, inlen, out, outlen);
}


void mission_file_class::add(char *new_text_key)
{
	UInt16 loop;
	
	if (strlen(new_text_key) >= MISSION_NAME_LENGTH)
	{
		lprint("Mission name is too long");
		error(ERR_WORD_TOO_LONG);
	}
	
	// check text key is not already in the list
	for (loop = 0; loop < num_of_mission_files; loop++)
	{
		if (strcmp(new_text_key, filenames[loop]) == 0)
		{
			printf("**** %s occurs twice in the list of mission text keys", new_text_key);
			exit (-1);
		}
	}
	
	strcpy(filenames[num_of_mission_files], new_text_key);
	num_of_mission_files++;
	
	if (num_of_mission_files >= MAX_MISSION_FILES)
	{
		printf("**** Too many entries in the list of mission text keys %d", MAX_MISSION_FILES);
		exit (-1);
	}
}

void mission_file_class::alphabeticise(void)
{
	UInt16 loop, loop2, charloop;
	char temp_filename[MISSION_NAME_LENGTH];
	
	if (num_of_mission_files > 1)
	{
		for (loop = 0; loop < (num_of_mission_files - 1); loop++)
		{
			loop2 = loop + 1;
		
			while (loop2 < num_of_mission_files)
			{
				if (strcmp(filenames[loop], filenames[loop2]) > 0)
				{
					for (charloop = 0; charloop < MISSION_NAME_LENGTH; charloop++)
					{
						temp_filename[charloop] = 0;
					}
					strcpy(temp_filename, filenames[loop]);
					strcpy(filenames[loop], filenames[loop2]);
					strcpy(filenames[loop2], temp_filename);
				}
				loop2++;
			}
		}
	}
}

void mission_file_class::initialise()
{
	UInt16 loop, loop2;
	
	// check text key is not already in the list
	for (loop = 0; loop < MAX_MISSION_FILES; loop++)
	{
		for (loop2 = 0; loop2 < MISSION_NAME_LENGTH; loop2++)
		{
			filenames[loop][loop2] = 0;
		}
	}
	
	num_of_mission_files = 0;
}


UInt32 mission_file_class::check_for_mission_filenames( UInt8 *in, Int32 inlen, Bool8 bJapanese)
{
	UInt32 char_count = 0;
	char current_text_key[MISSION_NAME_LENGTH];
	bool reading_text_key = false;
	char look_ahead_string[MISSION_NAME_LENGTH];
	UInt32 loop;
	Bool8 bFoundEnd;
	char current_character;
	UInt8 current_text_key_character = 0;
	
	for (loop = 0; loop < MISSION_NAME_LENGTH; loop++)
	{
		current_text_key[loop] = 0;
		look_ahead_string[loop] = 0;
	}
	
//	initialise();

	bFoundEnd = false;
	u32 step_increment = 1;
	if (bJapanese)
	{
		char_count += 2;	//	skip past the first two characters that say 'this is unicode'
		step_increment = 2;
	}

	while ( (white_space(in[char_count])) && (char_count < inlen) )
	{
		char_count += step_increment;
	}

	for (loop = 0; loop < 5; loop++)
	{
		look_ahead_string[loop] = in[char_count];
		char_count += step_increment;
	}


	if (strncmp( (const char *) &look_ahead_string[0], "start", 5) == 0)
	{
		for (loop = 0; loop < MISSION_NAME_LENGTH; loop++)
		{
			current_text_key[loop] = 0;
			look_ahead_string[loop] = 0;
		}
		
		while (char_count < inlen)
		{
			if (bJapanese)
			{
				current_character = in[char_count];
				look_ahead_string[0] = in[char_count];
				look_ahead_string[1] = in[char_count + 2];
				look_ahead_string[2] = in[char_count + 4];
				if (strncmp( (const char *) &look_ahead_string[0], "end", 3) == 0)
				{
					char_count += 6;
					bFoundEnd = true;
				}
				else
				{
					char_count += 2;
				}
			}
			else
			{
				current_character = in[char_count];
				look_ahead_string[0] = in[char_count];
				look_ahead_string[1] = in[char_count + 1];
				look_ahead_string[2] = in[char_count + 2];
				if (strncmp( (const char *) &look_ahead_string[0], "end", 3) == 0)
				{
					char_count += 3;
					bFoundEnd = true;
				}
				else
				{
					char_count++;
				}
			}

			if (bFoundEnd)
			{
				if (reading_text_key)
				{
					reading_text_key = false;
					add(current_text_key);
					
					for (loop = 0; loop < MISSION_NAME_LENGTH; loop++)
					{
						current_text_key[loop] = 0;
					}
					current_text_key_character = 0;
				}

				return char_count;
			}
			
			if (white_space(current_character))
			{
				if (reading_text_key)
				{
					reading_text_key = false;
					add(current_text_key);
					
					for (loop = 0; loop < MISSION_NAME_LENGTH; loop++)
					{
						current_text_key[loop] = 0;
					}
					current_text_key_character = 0;
				}
			}
			else
			{
				reading_text_key = true;
				
				current_text_key[current_text_key_character++] = current_character;
				
				if (current_text_key_character >= MISSION_NAME_LENGTH)
				{
					sprintf(error_message, "**** One of the names in the list of mission text keys is longer than %d characters", MISSION_NAME_LENGTH);
					lprint(error_message);
					exit (-1);
				}
			}
		}

		lprint("**** Couldn't find an end for the list of mission text keys");
		exit (-1);
	}
	
	if (bJapanese)
	{
		return 2;	//	skip past the first two characters that say 'this is unicode'
	}
	else
	{
		return 0;
	}
}

int32 align4bytes(int32 value)
{
	return (value+3)&(~3);
}

void AddToChunkSizesArray(UInt32 Index, UInt32 Size)
{
	if (NumberOfEntriesInChunkSizesArray >= MAX_MISSION_FILES)
	{
		sprintf(error_message, "**** Too many entries in Chunk Sizes Array");
		lprint(error_message);
		exit (-1);
	}

	ChunkSizesArray[NumberOfEntriesInChunkSizesArray].ChunkIndex = Index;
	ChunkSizesArray[NumberOfEntriesInChunkSizesArray].ChunkSize = Size;
	NumberOfEntriesInChunkSizesArray++;
}

void SortChunkSizesArray()
{
	UInt32 loop1 = 0;
	UInt32 loop2 = 0;

	sChunkSizes TempChunk;

	for (loop1 = 0; loop1 < NumberOfEntriesInChunkSizesArray; loop1++)
	{
		for (loop2 = (loop1 + 1); loop2 < NumberOfEntriesInChunkSizesArray; loop2++)
		{
			if (ChunkSizesArray[loop1].ChunkSize < ChunkSizesArray[loop2].ChunkSize)
			{
				TempChunk = ChunkSizesArray[loop1];
				ChunkSizesArray[loop1] = ChunkSizesArray[loop2];
				ChunkSizesArray[loop2] = TempChunk;
			}
		}
	}
}

void OutputChunkSizesToBlockSizesTextFile()
{
	for (UInt32 loop = 0; loop < NumberOfEntriesInChunkSizesArray; loop++)
	{
		if (ChunkSizesArray[loop].ChunkSize >= 16384)
		{
			printf ("%s (size %d bytes) is >= 16384 bytes \n", mission_files.filenames[ChunkSizesArray[loop].ChunkIndex], ChunkSizesArray[loop].ChunkSize);
		}
		fprintf(fpBlockSizes, "%s %d\n", mission_files.filenames[ChunkSizesArray[loop].ChunkIndex], ChunkSizesArray[loop].ChunkSize);
	}
}

UInt32 save_offsets_table(void)
{
	static UInt8 str[256];
	
	char string_to_write[MISSION_NAME_LENGTH];
	UInt32 number_of_entries;
	UInt32 offset_to_chunk;
	UInt16 loop, mission_loop;
	UInt32 size_of_table;
	
	UInt32 size_of_main_text, size_of_last_mission_text_chunk;
	UInt32 size_of_this_mission_text_chunk, size_of_largest_mission_text;
	
	size_of_main_text = 0;
	size_of_largest_mission_text = 0;
	size_of_last_mission_text_chunk = 0;
	size_of_this_mission_text_chunk = 0;

	NumberOfEntriesInChunkSizesArray = 0;
	
	//	write 4 characters TABL
	strcpy(string_to_write, "TABL");
	
	write_file(string_to_write, 4);
	
	//	write UInt32 number of entries in table (mission chunks plus main chunk)
	number_of_entries = mission_files.num_of_mission_files + 1;
	size_of_table = number_of_entries * (MISSION_NAME_LENGTH + 4);
	write_file((char*)&size_of_table, 4);
	
	fprintf(fpBlockSizes, "Size of Block Table at start of file = %d\n\n", size_of_table);

	//	write a table of offsets for each chunk of mission text
	//	each chunk should probably start with its name (MISSION_NAME_LENGTH characters) except for main chunk
	// sizeof this table is number of entries * (MISSION_NAME_LENGTH + 4) name + offset
	// offset of main file is 4 (2 for key format, 2 for text format) + 8 + size of table
	offset_to_chunk = 4 + 4 + 4 + size_of_table;
	offset_to_chunk = align4bytes(offset_to_chunk);
	
	for (loop = 0; loop < MISSION_NAME_LENGTH; loop++)
	{
		string_to_write[loop] = 0;
	}
	strcpy(string_to_write, "MAIN");
	
	write_file(string_to_write, MISSION_NAME_LENGTH);
	write_file((char *)&offset_to_chunk, 4);
	
	// offset of first mission is 8 + size of table + size of main keys + size of main data
	// offset of second mission is offset of first + MISSION_NAME_LENGTH (for first mission name) + size of first mission keys + size of first mission data
	
//	offset_to_chunk += MISSION_NAME_LENGTH;										//	for name of main chunk (this has been removed)
	if (use_hash_keys)
	{
		offset_to_chunk += 8 + (pMainTextKeys->count * 8);	//	for size saved in key_array_class::save
	}
	else
	{
		offset_to_chunk += 8 + (pMainTextKeys->count * (4 + KEY_SIZE));	//	for size saved in key_array_class::save
	}

	if (use_unicode)
	{
		offset_to_chunk += 8 + (pMainText->count * sizeof(UInt16));					//	for size saved in data_class::save
	}
	else
	{
		offset_to_chunk += 8 + (pMainText->count * sizeof(UInt8));					//	for size saved in data_class::save
	}
	offset_to_chunk = align4bytes(offset_to_chunk);
	
	size_of_main_text = offset_to_chunk;
	
	for (mission_loop = 0; mission_loop < mission_files.num_of_mission_files; mission_loop++)
	{
		for (loop = 0; loop < MISSION_NAME_LENGTH; loop++)
		{
			string_to_write[loop] = 0;
		}
		strcpy(string_to_write, mission_files.filenames[mission_loop]);
		write_file(string_to_write, MISSION_NAME_LENGTH);
		write_file((char *)&offset_to_chunk, 4);
		
		size_of_last_mission_text_chunk = offset_to_chunk;
		
		if (pMissionTextKeys[mission_loop]->count == 0)
		{
			sprintf(error_message, "%s : There is no text for this mission file - remove it from the list at the top of the text file", mission_files.filenames[mission_loop]);
			lprint(error_message);
			exit (-1);
		}
		
		// Calculate the correct offset for the next chunk
		offset_to_chunk += MISSION_NAME_LENGTH;	//	for name of chunk

		if (use_hash_keys)
		{
			offset_to_chunk += 8 + (pMissionTextKeys[mission_loop]->count * 8);	//	for size saved in key_array_class::save
		}
		else
		{
			offset_to_chunk += 8 + (pMissionTextKeys[mission_loop]->count * (4 + KEY_SIZE));	//	for size saved in key_array_class::save
		}

		if (use_unicode)
		{
			offset_to_chunk += 8 + (pMissionText[mission_loop]->count * sizeof(UInt16));				//	for size saved in data_class::save
		}
		else
		{
			offset_to_chunk += 8 + (pMissionText[mission_loop]->count * sizeof(UInt8));					//	for size saved in data_class::save
		}

		offset_to_chunk = align4bytes(offset_to_chunk);

		// Check the size of this chunk
		size_of_this_mission_text_chunk = offset_to_chunk - size_of_last_mission_text_chunk;
		if (size_of_this_mission_text_chunk > size_of_largest_mission_text)
		{
			size_of_largest_mission_text = size_of_this_mission_text_chunk;
		}

		AddToChunkSizesArray(mission_loop, size_of_this_mission_text_chunk);
	}
	
	sprintf ((char*)str,"Main Size %d  Largest Mission Size %d\n", size_of_main_text, size_of_largest_mission_text);
	lprint ((char*)str);
	
	return offset_to_chunk;
}

void ReplaceUTF8ByteOrderMark(u8 *in, s32 inlen)
{
	if (inlen > 3)
	{
		if ( (in[0] == 0xef) && (in[1] == 0xbb) && (in[2] == 0xbf))
		{
			for (u32 loop = 0; loop < 3; loop++)
			{
				in[loop] = ' ';
			}
		}
	}
}

void do_gxtmaker ( char *infile, char *outfile, char *alphafile, Bool8 bJapanese )
{
        static UInt8 str[256];
        UInt8 *in, *start_address_of_input_buffer;
		UInt16 *out;
        Int32 inlen, outlen;

		FILE *fpAlpha;
		UInt32 loop, loop2;
		
		Int32 file_pos;
		
		char chunk_name[MISSION_NAME_LENGTH];
		
		UInt32 characters_read;
		
		InitLongestStrings();
/*
		fpBigWords = fopen("big_words.txt", "w");
		if (fpBigWords == NULL)
		{
			printf("**** Couldn't open big word file - check it is writable");
			exit (-1);
		}
*/
/*
		for (loop = 0; loop < 258; loop++)
		{
			used_foreign[loop] = 0;
		}
*/
		number_of_characters_between_tilde = -1;

		pMainText = new data_class(1024 * 1024);
		pMainTextKeys = new key_array_class(16000);
		
		pAllTextKeys = new key_array_class(77500);

		for (loop = 0; loop < MAX_MISSION_FILES; loop++)
		{
			pMissionText[loop] = new data_class(96 * 1024);
			pMissionTextKeys[loop] = new key_array_class(3000);
		}

        sprintf ((char*)str,"Converting %s -> %s\n",infile,outfile);
        lprint ((char*)str);
		if (non_translate) lprint ("(without translation)\n");
        load_file ((char*)infile, (char**)&in, &inlen );
		ReplaceUTF8ByteOrderMark(in, inlen);

        out = (UInt16*) malloc (inlen*2*3);
        if (out == NULL) error (ERR_MEMORY_OUTPUT);
        
        mission_files.initialise();
        characters_read = mission_files.check_for_mission_filenames( in, inlen, bJapanese);

		start_address_of_input_buffer = in;
		in += characters_read;
		inlen -= characters_read;
//		out += characters_read;
//		outlen -= characters_read;

/*
		char look_ahead_string[MISSION_NAME_LENGTH];
		char curr_char;

		loop = 0;
		curr_char = in[loop++];
		if (bJapanese)
		{
			loop++;
		}
		while ( (white_space(curr_char)) && (loop < inlen) )
		{
			curr_char = in[loop++];
			if (bJapanese)
			{
				loop++;
			}
		}
		
		if (loop < inlen)
		{
			if (bJapanese)
			{
				loop -= 2;
				look_ahead_string[0] = in[loop++];
				loop++;
				look_ahead_string[1] = in[loop++];
				loop++;
				look_ahead_string[2] = in[loop++];
				loop++;
				look_ahead_string[3] = in[loop++];
				loop++;
				look_ahead_string[4] = in[loop++];
				loop++;
			}
			else
			{
				loop--;
				look_ahead_string[0] = in[loop++];
				look_ahead_string[1] = in[loop++];
				look_ahead_string[2] = in[loop++];
				look_ahead_string[3] = in[loop++];
				look_ahead_string[4] = in[loop++];
			}
			look_ahead_string[5] = 0;
			
			if (strncmp( ((const char *) &look_ahead_string[0]), "start", 5) == 0)
			{
				sprintf(error_message, "There may be more than one table at the start of the .txt file");
				lprint(error_message);
				exit (-1);
			}
		}
*/

        do_convert ( in, inlen, out, &outlen, bJapanese );

		pMainTextKeys->uppercaserize();	// Make sure we only have uppers
//		pMainTextKeys->sort();
		
		pAllTextKeys->uppercaserize();
		pAllTextKeys->sort();	//	Sort by hash key first to check for duplicate hashes across all blocks (i.e. pMainTextKeys and the pMissionTextKeys array)

		use_hash_keys = false;	//	Bit dodgy but we want to sort this list alphabetically rather
		pAllTextKeys->sort();	//	than by hash key.
		use_hash_keys = true;	//	Set back to using hash keys for all other structures.
		
		for (loop = 0; loop < mission_files.num_of_mission_files; loop++)
		{
			pMissionTextKeys[loop]->uppercaserize();
			pMissionTextKeys[loop]->sort();
		}

		if (bPathForDebugFilesHasBeenSet)
		{
			_chdir(pathForDebugFiles);
		}
		fpAlpha = fopen(alphafile, "w");

		if (bPathForDebugFilesHasBeenSet)
		{
			_chdir(workingDirectory);
		}

		if (fpAlpha == NULL)
		{
			printf("**** Couldn't open alphabetical text file key usage file - check it is writable");

			exit (-1);
		}

		for (loop = 0; loop < pAllTextKeys->count; loop++)
		{
			fprintf(fpAlpha, "%s Unsigned Hash = %u Signed Hash = %d\n", pAllTextKeys->pBuffer[loop].key, pAllTextKeys->pBuffer[loop].HashKey, pAllTextKeys->pBuffer[loop].HashKey);
		}

		fclose(fpAlpha);
//		fclose(fpBigWords);

		fpBlockSizes = fopen("block_sizes.txt", "w");
		if (fpBlockSizes == NULL)
		{
			printf("**** Couldn't open block_sizes.txt - check it is writable");
			exit (-1);
		}

		WriteLongestStringsFile();

		open_save_file((char*)outfile);
/*		
		if (bJapanese)
		{
			save_japanese_character_information();
		}
*/

		UInt16 temp_uint16;
		if (use_hash_keys)
		{
			temp_uint16 = 4;
		}
		else
		{
			temp_uint16 = KEY_SIZE;
		}
		write_file ((char*)&temp_uint16, 2);

		if (use_unicode)
		{
			temp_uint16 = 16;
		}
		else
		{
			temp_uint16 = 8;
		}
		write_file ((char*)&temp_uint16, 2);

		if (mission_files.num_of_mission_files > 0)
		{
			save_offsets_table();
			file_pos = get_file_pos();
			file_pos = align4bytes(file_pos);
			set_file_pos(file_pos);
		}
/*
		for (loop2 = 0; loop2 < MISSION_NAME_LENGTH; loop2++)
		{
			chunk_name[loop2] = 0;
		}
		strcpy(chunk_name, "MAIN");
		write_file (chunk_name, MISSION_NAME_LENGTH);
*/

		fprintf(fpBlockSizes, "Main Block\n");

		pMainTextKeys->save();
		pMainText->save();
		
		for (loop = 0; loop < mission_files.num_of_mission_files; loop++)
		{
			file_pos = get_file_pos();
			file_pos = align4bytes(file_pos);
			set_file_pos(file_pos);

			for (loop2 = 0; loop2 < MISSION_NAME_LENGTH; loop2++)
			{
				chunk_name[loop2] = 0;
			}
			strcpy(chunk_name, mission_files.filenames[loop]);
			write_file (chunk_name, MISSION_NAME_LENGTH);

			fprintf(fpBlockSizes, "\n%s\n", chunk_name);

			pMissionTextKeys[loop]->save();
			pMissionText[loop]->save();
		}
		
		close_file();

		fprintf(fpBlockSizes, "\n\n ****************************************** \n\n");

		SortChunkSizesArray();
		OutputChunkSizesToBlockSizesTextFile();

		fclose(fpBlockSizes);

		delete pMainText;
		delete pMainTextKeys;
		
		delete pAllTextKeys;
		
		for (loop = 0; loop < MAX_MISSION_FILES; loop++)
		{
			delete pMissionText[loop];
			delete pMissionTextKeys[loop];
		}
		
		free(start_address_of_input_buffer);
		free(out);
}

void param_error()
{
        lprint ("\nUsage : gxtmaker [-j] <filename>\n");
//        lprint ("         -j : convert shift-jis data\n");
		lprint ("		  -j : convert japanese data - must be followed by metrics filename\n");
        lprint ("         -r : convert Russian data\n");
        lprint ("         -n : don't translate foreign characters\n");
        lprint ("         -m : followed by a number then filenames to specify multiple .txt files\n");
        lprint ("              note : keys beginning ""netui"" not translated\n");
		lprint ("		  -out : should be followed by the path and name of the output gxt file\n");
		lprint ("		  -pathForDebugFiles : should be followed by a path to specify where the .alp, .inv and .lng should be written to\n");
		lprint ("		  -longkeys : should be followed by a number to say how many files are allowed to contain long keys and then a list of those filenames\n");
        close_log();
        exit (-1);
}

void OutputUnicodeString(FILE *pOutputFile, char *pString)
{
	UInt16 string_length, loop;
	Char16 unicode_character[2];

	unicode_character[1] = 0;
	
	string_length = strlen(pString);
	
	for (loop = 0; loop < string_length; loop++)
	{
		unicode_character[0] = (Char16) pString[loop];
		fwprintf(pOutputFile, (const wchar_t *) unicode_character);
	}
}

bool DoesThisFileAllowLongTextKeys(const char *pFilename)
{
	for (u32 loop = 0; loop < number_of_files_allowing_long_text_keys; loop++)
	{
		if (_stricmp(pFilename, files_allowing_long_text_keys[loop]) == 0)
		{
			return true;
		}
	}

	return false;
}

void concatenate_text_files(char *filename, char *additional_filenames[MAX_ADDITIONAL_FILES], UInt8 number_of_additional_files, char *outputfilename, Bool8 bJapanese)
{
    UInt8 *in, *startofrealtext;
    Int32 inlen;
    UInt32 loop;

	UInt32 characters_read;
	char *alltextbuffers[MAX_ADDITIONAL_FILES+1];
	size_t sizeof_alltextbuffers[MAX_ADDITIONAL_FILES+1];
	Bool8 bTextBufferContainsDialogue[MAX_ADDITIONAL_FILES+1];
	bool bTextBufferAllowsLongKeys[MAX_ADDITIONAL_FILES+1];
	FILE *pOutputFile;
	char UnicodeIdentifier[4];
	
	//	read each file
	//	if the file contains a list of mission names then add these names to an array
	//	store the rest of the file
	load_file(filename, (char**)&in, &inlen );
	ReplaceUTF8ByteOrderMark(in, inlen);

	mission_files.initialise();
	characters_read = mission_files.check_for_mission_filenames( in, inlen, bJapanese);

	startofrealtext = in + characters_read;
	inlen -= characters_read;

	if (strstr(_strlwr(filename), "dialogue"))
	{
		bTextBufferContainsDialogue[0] = true;
	}
	else
	{
		bTextBufferContainsDialogue[0] = false;
	}

	bTextBufferAllowsLongKeys[0] = DoesThisFileAllowLongTextKeys(filename);

	alltextbuffers[0] = (char *) malloc(inlen + 2);
	if (alltextbuffers[0] == NULL) error (ERR_MEMORY_FIRST_TEXT_BUFFER);
	memcpy( alltextbuffers[0], startofrealtext, inlen );
	alltextbuffers[0][inlen] = 0;
	alltextbuffers[0][inlen + 1] = 0;
	sizeof_alltextbuffers[0] = inlen;
	free(in);
	
	for (loop = 0; loop < number_of_additional_files; loop++)
	{
		load_file(additional_filenames[loop], (char**)&in, &inlen );
		ReplaceUTF8ByteOrderMark(in, inlen);

	    characters_read = mission_files.check_for_mission_filenames( in, inlen, bJapanese);
		startofrealtext = in + characters_read;
		inlen -= characters_read;

		if (strstr(_strlwr(additional_filenames[loop]), "dialogue"))
		{
			bTextBufferContainsDialogue[(loop + 1)] = true;
		}
		else
		{
			bTextBufferContainsDialogue[(loop + 1)] = false;
		}

		bTextBufferAllowsLongKeys[loop+1] = DoesThisFileAllowLongTextKeys(additional_filenames[loop]);

		alltextbuffers[(loop + 1)] = (char *) malloc(inlen + 2);
		if (alltextbuffers[(loop + 1)] == NULL) error (ERR_MEMORY_EXTRA_TEXT_BUFFER);
		memcpy( alltextbuffers[(loop + 1)], startofrealtext, inlen );
		alltextbuffers[(loop + 1)][inlen] = 0;
		alltextbuffers[(loop + 1)][inlen + 1] = 0;
		sizeof_alltextbuffers[(loop + 1)] = inlen;
		free(in);
	}
	
	//	after all files have been read
	//	sort the mission name list into alphabetical order and write them to the output file
	mission_files.alphabeticise();
	//	write all the rest of the data to the output file
	//	Does Japanese file have to be saved as a unicode file?
	
	pOutputFile = fopen(outputfilename, "wb");

	if (pOutputFile == NULL)
	{
		printf("**** Couldn't open %s - check it is writable", outputfilename);
		exit(-1);
	}
	
	if (bJapanese)
	{
		//	Write the two characters to signify that the file is Unicode (either FEFF or FFFE)
		//	Each character in the list of mission names should have a space after it
		
		UnicodeIdentifier[0] = 0xff;
		UnicodeIdentifier[1] = 0xfe;
		UnicodeIdentifier[2] = 0;
		UnicodeIdentifier[3] = 0;
		fwprintf(pOutputFile, (const wchar_t *) UnicodeIdentifier);

		if (mission_files.num_of_mission_files > 0)
		{
			OutputUnicodeString(pOutputFile, "start\r\n");
			for (loop = 0; loop < mission_files.num_of_mission_files; loop++)
			{
				OutputUnicodeString(pOutputFile, mission_files.filenames[loop]);
				OutputUnicodeString(pOutputFile, "\r\n");
			}
			
			OutputUnicodeString(pOutputFile, "end");	//	"\r\n");
		}
	}
	else
	{
		if (mission_files.num_of_mission_files > 0)
		{
			fprintf(pOutputFile, "start\r\n");
			for (loop = 0; loop < mission_files.num_of_mission_files; loop++)
			{
				fprintf(pOutputFile, mission_files.filenames[loop]);
				fprintf(pOutputFile, "\r\n");
			}
			
			fprintf(pOutputFile, "end");	//	"\r\n");
		}
	}

	if (bJapanese)
	{
		for (loop = 0; loop <= number_of_additional_files; loop++)
		{
			if (bTextBufferContainsDialogue[loop])
			{
				OutputUnicodeString(pOutputFile, "\r\n[");
				OutputUnicodeString(pOutputFile, start_dialogue_chunk);
				OutputUnicodeString(pOutputFile, "]\r\n");
			}

			if (bTextBufferAllowsLongKeys[loop])
			{
				OutputUnicodeString(pOutputFile, "\r\n[");
				OutputUnicodeString(pOutputFile, start_allow_long_keys);
				OutputUnicodeString(pOutputFile, "]\r\n");
			}

//	Does this need changed to fwrite too?
			fwprintf(pOutputFile, (const wchar_t *) alltextbuffers[loop]);
			if (bTextBufferContainsDialogue[loop])
			{
				OutputUnicodeString(pOutputFile, "\r\n[");
				OutputUnicodeString(pOutputFile, end_dialogue_chunk);
				OutputUnicodeString(pOutputFile, "]\r\n");
			}

			if (bTextBufferAllowsLongKeys[loop])
			{
				OutputUnicodeString(pOutputFile, "\r\n[");
				OutputUnicodeString(pOutputFile, end_allow_long_keys);
				OutputUnicodeString(pOutputFile, "]\r\n");
			}
		}
	}
	else
	{
		for (loop = 0; loop <= number_of_additional_files; loop++)
		{
			if (bTextBufferContainsDialogue[loop])
			{
				fprintf(pOutputFile, "\r\n[");
				fprintf(pOutputFile, start_dialogue_chunk);
				fprintf(pOutputFile, "]\r\n");
			}

			if (bTextBufferAllowsLongKeys[loop])
			{
				fprintf(pOutputFile, "\r\n[");
				fprintf(pOutputFile, start_allow_long_keys);
				fprintf(pOutputFile, "]\r\n");
			}

//	Changed fprintf to fwrite as the text buffer contained a %n that caused fprintf to crash
//			fprintf(pOutputFile, alltextbuffers[loop]);
			fwrite(alltextbuffers[loop], 1, sizeof_alltextbuffers[loop], pOutputFile);
			if (bTextBufferContainsDialogue[loop])
			{
				fprintf(pOutputFile, "\r\n[");
				fprintf(pOutputFile, end_dialogue_chunk);
				fprintf(pOutputFile, "]\r\n");
			}

			if (bTextBufferAllowsLongKeys[loop])
			{
				fprintf(pOutputFile, "\r\n[");
				fprintf(pOutputFile, end_allow_long_keys);
				fprintf(pOutputFile, "]\r\n");
			}
		}
	}

	fclose(pOutputFile);
	
	for (loop = 0; loop <= number_of_additional_files; loop++)
	{
		free(alltextbuffers[loop]);
	}

}

void main ( Int32 argc, char *argv[] )
{
        Int32 i;
        char *filename = NULL;
        char *japanese_metrics_filename = NULL;
//        Bool8 shiftjis = FALSE;
		Bool8 bJapanese = false;
		bool bOutputFilenameHasBeenSet = false;
		char outfilename[256];
		char alphafilename[256];
		char file_to_load[256];
		UInt32 number_of_files, filenameloop, number_of_bits, number_of_bytes;
		char *additional_filenames[MAX_ADDITIONAL_FILES];
		bool bAdditionalFilesHaveBeenSpecified = false;

		adding_to_block[0] = '\0';

		_getcwd(workingDirectory, MAX_LENGTH_OF_DIRECTORY_NAMES);

        open_log();

        lprint ("\ngxtmaker v3.3.1       by KRH\n");
        lprint (  "(c) 1996-1999 DMA Design Ltd\n");

        for (i=1; i<argc; i++)
        {
//				if ((!shiftjis) && (!strcmp(argv[i],"-j")))
//						shiftjis = TRUE;
			if ((!bJapanese) && (!strcmp(argv[i], "-j")))
			{
				i++;
				if (i >= argc)
				{
					param_error();
				}
				else
				{
					japanese_metrics_filename = argv[i];
					bJapanese = true;
				}
			}
            else if ((!non_translate) && (!strcmp(argv[i],"-n")))
                    non_translate = TRUE;
            else if ((!russian) && (!strcmp(argv[i],"-r")))
                    russian = TRUE;
			else if (!strcmp(argv[i], "-m"))
			{
				i++;
				if (i >= argc)
				{
					param_error();
				}
				else
				{
					if (!IsInt(argv[i]))
					{
						error(ERR_MULTIPLE_FILE_LOADER);
					}
					
					number_of_files = atoi(argv[i]);
					
					if ( (number_of_files < 1) || (number_of_files > (MAX_ADDITIONAL_FILES + 1) ) )
					{
						error(ERR_MULTIPLE_FILE_LOADER);
					}
					
					i++;
					if (i >= argc)
					{
						param_error();
					}
					else
					{
						filename = argv[i];
						for (filenameloop = 0; filenameloop < (number_of_files - 1); filenameloop++)
						{
							i++;
							if (i >= argc)
							{
								param_error();
							}
							else
							{
								additional_filenames[filenameloop] = argv[i];
							}
						}

						bAdditionalFilesHaveBeenSpecified = true;
					}
				}
			}
			else if (!strcmp(argv[i], "-key"))
			{
				i++;
				if (i >= argc)
				{
					param_error();
				}
				else
				{
					if (strlen(argv[i]) != 1)
					{
						error(ERR_KEY_FORMAT);
					}
					if (!is_digit(*argv[i]))
					{
						error(ERR_KEY_FORMAT);
					}
					
					number_of_bits = atoi(argv[i]);
					
					if (number_of_bits == 4)
					{
						use_hash_keys = true;
					}
//					else if (number_of_bits == KEY_SIZE)
//					{
//						use_hash_keys = false;
//					}
					else
					{
						error(ERR_KEY_FORMAT);
					}
				}
			}
			else if (!strcmp(argv[i], "-bytes"))
			{
				i++;
				if (i >= argc)
				{
					param_error();
				}
				else
				{
					if (strlen(argv[i]) != 1)
					{
						error(ERR_DATA_FORMAT);
					}
					if (!is_digit(*argv[i]))
					{
						error(ERR_DATA_FORMAT);
					}
					
					number_of_bytes = atoi(argv[i]);
					
					if (number_of_bytes == 1)
					{
						use_unicode = false;
					}
					else if (number_of_bytes == 2)
					{
						use_unicode = true;
					}
					else
					{
						error(ERR_DATA_FORMAT);
					}
				}
			}
			else if (!strcmp(argv[i], "-out"))
			{
				i++;

				strncpy(outfilename, argv[i], 256);
				const char *pOutFilenameWithoutPath = FileName(outfilename);
				strncpy(alphafilename, change_ext(pOutFilenameWithoutPath,".alp"), 256);
				bOutputFilenameHasBeenSet = true;
			}
			else if (!strcmp(argv[i], "-pathForDebugFiles"))
			{
				i++;
				strncpy(pathForDebugFiles, argv[i], MAX_LENGTH_OF_DIRECTORY_NAMES);
				bPathForDebugFilesHasBeenSet = true;
			}
			else if (!strcmp(argv[i], "-longkeys"))
			{
				i++;
				if (i >= argc)
				{
					param_error();
				}
				else
				{
					if (!IsInt(argv[i]))
					{
						error(ERR_LONG_KEYS_FILE_LOADER);
					}
					
					number_of_files_allowing_long_text_keys = atoi(argv[i]);

					if ( (number_of_files_allowing_long_text_keys < 1) || (number_of_files_allowing_long_text_keys > MAX_FILES_ALLOWING_LONG_TEXT_KEYS) )
					{
						error(ERR_LONG_KEYS_FILE_LOADER);
					}

					for (filenameloop = 0; filenameloop < number_of_files_allowing_long_text_keys; filenameloop++)
					{
						i++;
						if (i >= argc)
						{
							param_error();
						}
						else
						{
							files_allowing_long_text_keys[filenameloop] = argv[i];
						}
					}
				}
			}
            else if (filename==NULL)
            {
                    filename = argv[i];
                    strcpy(file_to_load, filename);
			}
            else
                    param_error();
        }

		if (bAdditionalFilesHaveBeenSpecified)
		{
			strcpy(file_to_load, "temptxt.txt");
			concatenate_text_files(filename, additional_filenames, (number_of_files - 1), file_to_load, bJapanese);
		}
        
        clear_japanese_metrics_file_data();
        if (japanese_metrics_filename != NULL)
        {
			read_japanese_metrics_file(japanese_metrics_filename);
        }
        if (filename==NULL)
                param_error();

		if (!bOutputFilenameHasBeenSet)
		{
			strncpy(outfilename, change_ext(filename,".gxt"), 256);
			strncpy(alphafilename, change_ext(filename,".alp"), 256);
		}

        do_gxtmaker ( file_to_load, outfilename, alphafilename, bJapanese );

		const char *pOutputFilenameWithoutPath = FileName(outfilename);

		write_file_containing_keys_of_lines_with_invalid_characters(pOutputFilenameWithoutPath);

		char longkeys_filename[256];
		strncpy(longkeys_filename, change_ext(pOutputFilenameWithoutPath, ".lng"), 256);

		if (bPathForDebugFilesHasBeenSet)
		{
			_chdir(pathForDebugFiles);
		}

		FILE *fpLongKeysFile = fopen(longkeys_filename, "w");

		if (bPathForDebugFilesHasBeenSet)
		{
			_chdir(workingDirectory);
		}

		if (fpLongKeysFile == NULL)
		{
			printf("**** Couldn't open file to write out the list of text keys longer than 15 characters - check it is writable");
			exit (-1);
		}

		if (NumberOfLongTextKeys > 0)
		{
			char warning_string[256];

//	Output a list of the overly-long text keys to the file
			sprintf(warning_string, "There are %d text keys longer than 15 characters. Can they be shortened?\n", NumberOfLongTextKeys);
			fprintf(fpLongKeysFile, warning_string);
			lprint(warning_string);

			if (NumberOfLongTextKeys > MAX_LONG_TEXT_KEYS_TO_REPORT)
			{
				sprintf(warning_string, "Only the first %d are listed here\n", MAX_LONG_TEXT_KEYS_TO_REPORT);
				fprintf(fpLongKeysFile, warning_string);
				lprint(warning_string);
			}
			u32 NumberOfLongTextKeysToOutput = MIN(NumberOfLongTextKeys, MAX_LONG_TEXT_KEYS_TO_REPORT);
			for (u32 loop = 0; loop < NumberOfLongTextKeysToOutput; loop++)
			{
				sprintf(warning_string, "%s\n", LongTextKeysToReport[loop]);
				fprintf(fpLongKeysFile, warning_string);
				lprint(warning_string);
			}

//	Output an error
			sprintf(warning_string, "Warning : %d text keys are longer than 15 characters. Check %s for a list\n", NumberOfLongTextKeys, longkeys_filename);
			lprint(warning_string);
			error(ERR_LONG_KEYS_EXIST);
		}
		else
		{
			fprintf(fpLongKeysFile, "There are no text keys longer than 15 characters.\n");
		}

		fclose(fpLongKeysFile);

        close_log();
        
        exit (0);
}
