#ifndef PCH_PRECOMPILED

//
// filename:		rwplugin.h
// description:		defines for all the plugin ids in the project. This
//					files exists so we don't have any clashes with plugin
//					ids. This file should also be shared with the tools
//					code
// written by:		Adam Fowler
//
#ifndef INC_RWPLUGIN_H_
#define INC_RWPLUGIN_H_

// Company ID
#define rwVENDORID_ROCKSTAR		(152562)

// plugin IDs
#define rwGTA_ATOMIC_ID			(0x00)		// RpAtomic (4 bytes)
#define rwGTA_CLUMP_ID			(0x01)		// RpClump (8 bytes)
#define rwGTA_FRAME_ID			(0x02)		// RwFrame (4 bytes)


#define rwGEOMETRYNORMALS_ID	(0xF2)		// RpGeometry (4 bytes) (Xbox: Normals for Buildings)
#define rwPIPELINE_ID			(0xF3)		// RpGeometry (4 bytes)
#define rwCUSTENVMAPATM_ID		(0xF4)		// RpAtomic (4 bytes)
#define rwTXDPARENT_ID			(0xF5)		// RwTexDictionary (4 bytes)
#define rwCUSTSPECMAPMAT_ID		(0xF6)		// RpMaterial (4 bytes)
#define rwUVANIM_ID				(0xF7)		// RpAtomic (4 bytes)
#define rw2DEFFECTS_ID			(0xF8)		// RpGeometry (4 bytes)
#define rwEXTRAVERTCOL_ID		(0xF9)		// RpGeometry (4 bytes)
#define rwVEHICLECOL_ID			(0xFA)		// RpClump (0 bytes)
#define rwANIMBLEND_ID			(0xFB)		// RpClump (4 bytes)
#define rwCUSTENVMAPMAT_ID		(0xFC)		// RpMaterial (4 bytes)
#define rwBREAKABLE_ID 			(0xFD)		// RpGeometry (4 bytes)
#define rwNODENAME_ID			(0xFE)		// RwFrame (24 bytes !!!!!!)

//dont use id's above 0xFE
//its ok to use ones descending from 0xFE



//
// PDS plugin IDs
// (they are not connected with RW plugin IDs (above), but we are using different ID ranges
//	- just in case):
//

//
// Compressed Vertex Building (CVB) PDS IDs:
//
// 1. standard CVB:
#define rwCUSTPDS_CVB_ATM_PIPE_ID				(0x80)
#define rwCUSTPDS_CVB_MAT_PIPE_ID				(0x81)
// 2. "Day&Night" CVB:
#define rwCUSTPDS_CVBDN_ATM_PIPE_ID				(0x82)
#define rwCUSTPDS_CVBDN_MAT_PIPE_ID				(0x83)


//
// Compressed Vertex Car (CVC) PDS IDs:
//
// 1. standard CVC:
#define rwCUSTPDS_CVC_ATM_PIPE_ID				(0x84)
#define rwCUSTPDS_CVC_MAT_PIPE_ID				(0x85)
// 2. CVC + "EnvMap":
#define rwCUSTPDS_CVCENVMAP_ATM_PIPE_ID			(0x86)
#define rwCUSTPDS_CVCENVMAP_MAT_PIPE_ID			(0x87)



//
// Compressed Vertex Skinned Ped (CVSP) PDS IDs:
//
// 1. standard CVSP:
#define rwCUSTPDS_CVSP_ATM_PIPE_ID				(0x88)
#define rwCUSTPDS_CVSP_MAT_PIPE_ID				(0x89)



// 3. CVC "EnvMap UV2" fx pipe ID:
#define rwCUSTPDS_CVCENVMAP_UV2_ATM_PIPE_ID		(0x8A)
#define rwCUSTPDS_CVCENVMAP_UV2_MAT_PIPE_ID		(0x8B)


// 3. CVB + "EnvMap":
#define rwCUSTPDS_CVBENVMAP_ATM_PIPE_ID			(0x8C)
#define rwCUSTPDS_CVBENVMAP_MAT_PIPE_ID			(0x8D)


// 4. CVB "Day&Night" + "EnvMap":
#define rwCUSTPDS_CVBDNENVMAP_ATM_PIPE_ID		(0x8E)
#define rwCUSTPDS_CVBDNENVMAP_MAT_PIPE_ID		(0x8F)

// 5. CVB "UV Anim":
#define rwCUSTPDS_CVBUVA_ATM_PIPE_ID			(0x90)
#define rwCUSTPDS_CVBUVA_MAT_PIPE_ID			(0x91)

// 6. CVB "Day&Night" "UV Anim":
#define rwCUSTPDS_CVBDNUVA_ATM_PIPE_ID			(0x92)
#define rwCUSTPDS_CVBDNUVA_MAT_PIPE_ID			(0x93)


// 2. CVSP + "SpecMap":
#define rwCUSTPDS_CVSPSPECMAP_ATM_PIPE_ID		(0x94)
#define rwCUSTPDS_CVSPSPECMAP_MAT_PIPE_ID		(0x95)


//
// PC & XBOX pipelines:
//
// Xbox: Building pipe ("Day&Night"):
#define rwCUSTPDSXB_CBDN_ATM_PIPE_ID			(0x96)
//#define rwCUSTPDSXB_CBDN_MAT_PIPE_ID			(0x97)

// PC: Building pipe ("Day&Night" + optional "EnvMap"):
#define rwCUSTPDSPC_CBDNENVMAP_ATM_PIPE_ID		(0x98)
//#define rwCUSTPDSPC_CBDNENVMAP_MAT_PIPE_ID	(0x99)

// PC/Xbox: CarFX pipe (both standard and "EnvMap"):
#define rwCUSTPDSPC_CARFX_ATM_PIPE_ID			(0x9A)
//#define rwCUSTPDSPC_CARFX_MAT_PIPE_ID			(0x9B)

// PC: Standard building pipe (+optional "EnvMap"):
#define rwCUSTPDSPC_CBENVMAP_ATM_PIPE_ID		(0x9C)
//#define rwCUSTPDSPC_CBENVMAP_MAT_PIPE_ID		(0x9D)


// Xbox: Standard Building pipe:
#define rwCUSTPDSXB_CB_ATM_PIPE_ID				(0x9E)
//#define rwCUSTPDSXB_CB_MAT_PIPE_ID				(0x9F)

// Xbox: Standard Building pipe + EnvMap:
#define rwCUSTPDSXB_CBENVMAP_ATM_PIPE_ID		(0xA0)
//#define rwCUSTPDSXB_CBENVMAP_MAT_PIPE_ID		(0xA1)

// Xbox: "Day&Night" Building pipe + EnvMap:
#define rwCUSTPDSXB_CBDNENVMAP_ATM_PIPE_ID		(0xA2)
//#define rwCUSTPDSXB_CBDNENVMAP_MAT_PIPE_ID		(0xA3)



//
// pipeline IDs
//
#define rwPDS_OptmisedLight_AtmPipeID				(0x5001)
#define rwPDS_OptmisedLightSkinned_AtmPipeID		(0x5002)
#define rwPDS_VehicleColour_MatPipeID				(0x5003)
#define rwPDS_Tag_MatPipeID							(0x5004)




//
//
// useful macro for creating custom pipelineIDs on PC/XBOX (copied from rpPDS_MAKEPIPEID() on PS2):
//
#if defined(GTA_PC) || defined(GTA_XBOX)
	#define rpPDSPC_MAKEPIPEID(vendorID, pipeID)		(((vendorID & 0xFFFF) << 16) | (pipeID & 0xFFFF))
#endif



#endif // INC_RWPLUGIN_H_


#endif