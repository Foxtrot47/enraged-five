#ifndef PCH_PRECOMPILED

/*
 * dma.h
 * _____
 *
 * see - Rules and Guidelines for C Style at DMA Design
 *       rev 2.2 - 2/8/95
 *
 * v1.0 by AF (PC/PSX/U64)
 *
 * copyright DMA Design Ltd 1995
 *
 *   When using this header file one of the following must be defined PSX_DMA, 
 * PC_DMA, SATURN_DMA or U64_DMA.  These indicate which platform is being used, 
 * Sony Playstation, PC, Sega Saturn or Nintendo Ultra64 respectively.  
 * For the PC's PCDOS_DMA or PCWIN_DMA must be defined.  These indicate if 
 * DOS or Microsoft Windows is being used.  PCDOS_DMA and PCWIN_DMA defines do 
 * not affect anything at the moment.
 *   
 *   This file contains type definitions to replace the standard 'int', 'long' 
 * etc.  Also some new types have been added e.g fixed point numbers, booleans.
 * 
 *   Some standard macros have been included.  These macros should be used 
 * instead of your own.  Any previously defined version of a macro will be 
 * undefined before defining the new version.
 *
 */

#ifndef _DMA_H_
#define _DMA_H_



/*
 * type based defines
 */
#define MIN_INT8	(-128)
#define MAX_INT8	127
#define MIN_INT16	(-32768)
#define MAX_INT16	32767
#define MAX_INT32	2147483647
#define MIN_INT32	(-MAX_INT32-1)
#define MAX_UINT8	0xff
#define MAX_UINT16	0xffff
#define MAX_UINT32	0xffffffff





/* 
 *  PC types and macros used for the PSX2
 */

//// Slightly modified cause on the PSX2 a long is not 32 bits
// but 64
// Also added types bool and Bool

/* integer types */
typedef signed char 	Int8;
typedef signed char		int8;
typedef signed char		INT8;
typedef signed short	Int16;    
typedef signed short	int16;
typedef unsigned short  Char16;
typedef unsigned short  char16;
typedef unsigned short  CHAR16;
typedef signed short	INT16;
typedef signed int		Int32;
typedef signed int		int32;
typedef signed int		INT32;
#if defined(GTA_PS2)
typedef signed long		Int64;
typedef signed long		int64;
typedef signed long		INT64;
#elif defined(GTA_PC)
typedef signed __int64	Int64;
typedef signed __int64	int64;
#elif defined (GTA_XBOX)
typedef signed __int64	Int64;
typedef signed __int64	int64;
#else
#error No 64bit integer defined
#endif

/* unsigned integer types */
typedef unsigned char 	UInt8;
typedef unsigned char	uint8;
typedef unsigned char	UINT8;
typedef unsigned short	UInt16;
typedef unsigned short	uint16;
typedef unsigned short	UINT16;
typedef unsigned int	UInt32;
typedef unsigned int	uint32;
typedef unsigned int	UINT32;
#if defined(GTA_PS2)
typedef unsigned long	UInt64;
typedef unsigned long	uint64;
typedef unsigned long	UINT64;
#elif defined(GTA_PC)
typedef unsigned __int64	UInt64;
typedef unsigned __int64	uint64;
#elif defined(GTA_XBOX)
typedef unsigned __int64	UInt64;
typedef unsigned __int64	uint64;
#else
#error No 64bit integer defined
#endif

#if 0	// do we ever use these
/* fixed point value types ( position of point not defined ) */
typedef signed char 	Fix8;
typedef signed char		fix8;
typedef signed char		FIX8;
typedef signed short 	Fix16;
typedef signed short 	fix16;
typedef signed short 	FIX16;
typedef signed int	 	Fix32;
typedef signed int	 	fix32;
typedef signed int	 	FIX32;
typedef int64 			Fix64;
typedef int64 			fix64;
typedef int64 			FIX64;

/* unsigned fixed point value types */
typedef unsigned char 	UFix8;
typedef unsigned char	ufix8;
typedef unsigned char	UFIX8;
typedef unsigned short 	UFix16;
typedef unsigned short 	ufix16;
typedef unsigned short 	UFIX16;
typedef unsigned int 	UFix32;
typedef unsigned int 	ufix32;
typedef unsigned int 	UFIX32;
typedef uint64 			UFix64;
typedef uint64 			ufix64;
typedef uint64 			UFIX64;

#endif

/* character types ( ASCII 8-bit ) */
typedef char		Char;
typedef char		CHAR;

/* Boolean types (TRUE/FALSE) */
typedef unsigned char 	Bool8;
typedef unsigned char	bool8;
typedef unsigned char	BOOL8;
typedef unsigned short 	Bool16;
typedef unsigned short 	bool16;
typedef unsigned short 	BOOL16;
typedef unsigned int 	Bool32;
typedef unsigned int 	bool32;
typedef unsigned int 	BOOL32;

typedef uint32 Bool;
//typedef uint32 bool;

/* Scope declarators */
//#define GLOBAL	These are simply STUPID
//#define SEMIGLOBAL              static
//#define IMPORT                  extern

/*
 * Useful macros. All four macros assume nothing about type. The only macro 
 * to return a defined type is SIGN which returns an 'Int32' 
 */
#undef ABS
#undef MAX
#undef MIN
#undef SIGN

#define ABS(a)		(((a) < 0) ? (-(a)) : (a))
#define MAX(a, b)	(((a) > (b)) ? (a) : (b))
#define MIN(a, b)	(((a) < (b)) ? (a) : (b))
#define SIGN(a)		(Int32)(((a) < 0) ? (-1) : (1))

/*
 * Useful defines
 */
#undef TRUE
#undef FALSE
#define TRUE		1
#define FALSE		0
#undef true
#undef false
#define true		1
#define false		0

//	#define USE_UNICODE_FOR_TEXT_FILE

#ifdef USE_UNICODE_FOR_TEXT_FILE
	typedef Char16 GxtChar;
#else
	typedef unsigned char GxtChar;
#endif


#endif








#endif