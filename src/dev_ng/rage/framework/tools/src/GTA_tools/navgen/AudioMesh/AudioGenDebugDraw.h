#ifndef AUDGEN_DEBUGDRAW_H
#define AUDGEN_DEBUGDRAW_H

namespace rage
{
	struct agPolyMesh;
}

inline int bit(int a, int b)
{
	return (a & (1 << b)) >> b;
}

inline void intToCol(int i, float* col)
{
	int	r = bit(i, 0) + bit(i, 3) * 2 + 1;
	int	g = bit(i, 1) + bit(i, 4) * 2 + 1;
	int	b = bit(i, 2) + bit(i, 5) * 2 + 1;
	col[0] = 1 - r*63.0f/255.0f;
	col[1] = 1 - g*63.0f/255.0f;
	col[2] = 1 - b*63.0f/255.0f;
}

void agDebugDrawHeightfieldSolid(const struct agHeightField& hf);
void agDebugDrawHeightfieldWalkable(const struct agHeightField& hf);

void agDebugDrawMesh(const float* verts, int nverts, const int* tris, const float* normals, int ntris, const unsigned char* flags);
void agDebugDrawMeshSlope(const float* verts, int nverts, const int* tris, const float* normals, int ntris, const float walkableSlopeAngle);

void agDebugDrawCompactHeightfieldSolid(const struct agCompactHeightField& compact);
void agDebugDrawCompactHeightfieldRegions(const struct agCompactHeightField& chf);
void agDebugDrawCompactHeightfieldExtrudedRegions(const agCompactHeightField &compact);
void agDebugDrawCompactVolumefieldDistance(const struct agCompactVolumefield& compact);
void agDebugDrawCompactHeightFieldDistance(const struct agCompactHeightField &compact);
void agDebugDrawCompactHeightfieldAudConnections( const struct agCompactHeightField &compact, const struct agContourSet &cset, float alpha);
void agDrawPolymeshAudConnections(const struct agPolyMesh &mesh, const struct agContourSet &cset, float alpha);

void agDebugDrawRegionConnections(const struct agContourSet& cset, const float alpha = 1.0f);
void agDebugDrawRawContours(const struct agContourSet& cset, const float alpha = 1.0f);
void agDebugDrawContours(const struct agContourSet& cset, const float alpha = 1.0f);
void agDebugDrawPolyMesh(const struct agPolyMesh& mesh);
void agDebugDrawPolyMeshDetail(const struct agPolyMeshDetail& dmesh);
void agDebugDrawAudMeshStitching(const agPolyMesh &mesh, const agPolyMesh * const nmeshes[4]);
void agDebugDrawStitchedPolys(const struct agPolyMesh& mesh);
void agDebugDrawRegsMesh(const struct agPolyMesh& mesh);

void agDebugDrawCylinderWire(float minx, float miny, float minz, float maxx, float maxy, float maxz, const float* col);
void agDebugDrawBoxWire(float minx, float miny, float minz, float maxx, float maxy, float maxz, const float* col);
void agDebugDrawBox(float minx, float miny, float minz, float maxx, float maxy, float maxz,
					const float* col1, const float* col2);
void agDrawArc(const float* p0, const float* p1);

#endif // AUDGEN_DEBUGDRAW_H
