#ifndef C_AUDGEN_H
#define C_AUDGEN_H


////////////////////////////////////////////////////////
//CAudioGen is used to provide a low-poly representation of the world geometory -  which can be used
//for various environmental audio purposes - from a triangle soup provided by navgen's geometry export
//code using the following steps.
//1. The triangles are rasterized against the x-z plane to generate a voxelized representation
//(heightfield) of the geometry.

#include "CMeshGen.h"

//Non-class structs and functions
#include "AudioGen.h"
#include "AudioGenLog.h"




class CAudioGen : public CMeshGen
{
public:
	CAudioGen();
	virtual ~CAudioGen();

	virtual void HandleSettings();
	virtual void HandleDebugMode();

	virtual void HandleRender();
	virtual void HandleRenderOverlay();
	virtual void HandleMeshHasChanged(const float* verts, int nverts, const int* tris, const float* trinorms,
			int ntris, const float* bmin, const float* bmax, char * path, char * meshName);

	virtual bool HandleBuild();

	virtual void ResetSettings();

	//This is used by the command line interface to apply inputs normally done by the gui
	virtual void ApplyInputs(AudGenInputs &inputs);

protected:
	const float* m_verts;
	int m_nverts;
	const int* m_tris;
	const float* m_trinorms;
	int m_ntris;
	float m_bmin[3], m_bmax[3];

	float m_cellSize;
	float m_cellHeight;
	float m_borderSize;
	float m_minRegionSize;
	float m_mergeRegionSize;
	float m_agentHeight;
	float m_agentRadius;
	float m_agentMaxClimb;
	float m_agentMaxSlope;
	float m_edgeMaxLen;
	float m_edgeMaxError;
	float m_vertsPerPoly;
	float m_detailSampleDist;
	float m_detailSampleMaxError;
	
	bool m_buildDetailMesh;

	static const int MAX_POLYS = 256;

	//Stuff to do with the generated mesh goes here
	
	//More stuff to go here for post-voxellation process
	
	bool m_keepInterResults;
	agBuildTimes m_buildTimes;
	u8* m_triflags;
	agHeightField* m_solid;
	agCompactHeightField *m_compact;
	agContourSet *m_cset;
	agPolyMesh *m_pmesh;
	agPolyMeshDetail *m_dmesh;
	agConfig m_cfg;
	agPolyMesh *m_loadMesh;
	agCompactHeightField *m_loadCompact;
	char *m_path;
	char *m_meshName;

	enum DrawMode
	{
		DRAWMODE_MESH,
		DRAWMODE_VOXELS,
		DRAWMODE_COMPACT,
		DRAWMODE_DISTANCE,
		DRAWMODE_REGIONS,
		DRAWMODE_REGION_CONNECTIONS,
		DRAWMODE_RAW_CONTOURS,
		DRAWMODE_BOTH_CONTOURS,
		DRAWMODE_CONTOURS,
		DRAWMODE_POLYMESH,
		DRAWMODE_POLYMESH_DETAIL,
		DRAWMODE_AUD_CONNECTIONS,
		DRAWMODE_POLY_AUD_CONNECTIONS,
		DRAWMODE_LOADED_MESH,
		MAX_DRAWMODE
	};

	DrawMode m_drawMode;

	void cleanup();
		
};

#endif
