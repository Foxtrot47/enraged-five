@echo off
setlocal

pushd "%~dp0"

CALL setenv.bat

set BUILD_FOLDER=%RS_BUILDROOT%\%RS_BUILDBRANCH%
set SCE_PS3_ROOT=X:/usr/local/300_001/cell
pushd ..\..\..\..\..\..
set RAGE_DIR=%CD%
popd

ECHO LOAD_SLN ENVIRONMENT
ECHO DEV_BRANCH:   	%DEV_BRANCH%
ECHO BUILD_FOLDER: 	%BUILD_FOLDER%
ECHO RAGE_DIR: 		%RAGE_DIR%
ECHO SCE_PS3_ROOT: 	%SCE_PS3_ROOT%
ECHO END LOAD_SLN ENVIRONMENT

set BUILD_FOLDER=x:\gta5\build\dev
set SCE_PS3_ROOT=X:/usr/local/300_001/cell

start "" %cd%\AudioMeshMaker.sln

popd
