#ifndef AUDIOGEN_TIMER_H
#define AUDIOGEN_TIMER_H

#ifdef __GNUC__
#include <stdint.h>
typedef int64_t agTimeVal;
#else
typedef __int64 agTimeVal;
#endif

agTimeVal agGetPerformanceTimer();
int agGetDeltaTimeUsec(agTimeVal start, agTimeVal end);

#endif // AUDIOGEN_TIMER_H
