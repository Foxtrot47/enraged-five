/////////////////////////////////////////////////////////////////////////////////
// FILE		: CAudioMeshMaker.cpp
// PURPOSE	: To provide gui based generation of audio meshes.
// AUTHOR	: C. Walder (Based on Recast by Mikko Menonen)
// STARTED	: 31/08/2009
////////////////////////////////////////////////////////////////////////////////
#include <windows.h>
#include <GL/gl.h> 
#include <stdio.h>
//#include <math.h>
#include <io.h>

#include "CAudioMeshMaker.h"

#include "AudioGenDebugDraw.h"
#include "CAudioGen.h"
#include "CAudioStitch.h"
#include "CAudioGen_Display.h"
#include "CAudioLevelMesh.h"
#include "AudioGenLog.h"

#include "SDL.h"
#include "SDL_opengl.h"
#include "imgui.h"
#include "imguiRenderGL.h"



#define snprintf _snprintf

void CAudioMeshMaker::ScanDirectory(const char* path, const char* ext, FileList& list)
{
	list.clear();

	_finddata_t dir;
	char pathWithExt[MAX_PATH];
	long fh;
	strcpy(pathWithExt, path);
	strcat(pathWithExt, "/*");
	strcat(pathWithExt, ext);
	fh = _findfirst(pathWithExt, &dir);
	if(fh == -1L)
	{
		return;
	}

	do
	{
		list.add(dir.name);
	}
	while (_findnext(fh, &dir) == 0);
	_findclose(fh);

	list.sort();
}

bool CAudioMeshMaker::IntersectSegmentTriangle(const float* sp, const float* sq, const float * a, const float* b, const float* c, float &t)
{
	float v, w;
	float ab[3], ac[3], qp[3], ap[3], norm[3], e[3];
	vsub(ab, b, a);
	vsub(ac, c, a);
	vsub(qp, sp, sq);

	//Compute triangle normal. Can be precalulated or cached if
	//intersecting multiple segments against the same triangle
	vcross(norm, ab, ac);

	//Compute denominator d. If d <= 0, sement is parallel to or
	//points away from triangle, so exit early
	float d = vdot(qp, norm); 

	if(d <= 0.0f)
	{
		return false;
	}

	//Compute intersection t value of pq with plane of triangle. A ray
	//intersects if 0 <= t. Segment intersects if 0 <= t <-1. Delay
	//dividing be d until intersection has been found to pierce tringle
	vsub(ap, sp, a);
	t = vdot(ap, norm);
	if(t < 0.0f) return false;
	if(t > d) return false; //for segment; exlude this line of code for ray test.

	//Compute barycentric oordinate components and test if within bounds
	vcross(e, qp, ap);
	v = vdot(ac, e);
	if(v < 0.f || v > d) return false;
	w = -vdot(ab, e);
	if(w < 0.f || v+ w > d) return false;

	//Segment/ray intersects triangle. Perform delayed devision
	t /= d;

	return true;
}

bool CAudioMeshMaker::Raycast(CAudioLevelMesh &mesh, float *src, float *dst, float &tmin)
{
	float dir[3];
	vsub(dir, dst, src);

	int nt = mesh.GetTriCount();
	const float* verts = mesh.GetVerts();
	const float* normals = mesh.GetNormals();
	const int* tris = mesh.GetTris();
	tmin = 1.f;
	bool hit = false;

	for(int i = 0; i < nt*3; i += 3)
	{
		const float* n = &normals[i];
		if(vdot(dir, n) > 0)
		{
			continue;
		}

		float t = 1;
		if(IntersectSegmentTriangle(src, dst, &verts[tris[i]*3], &verts[tris[i+1]*3], &verts[tris[i+2]*3], t))
		{
			if(t<tmin)
			{
				tmin = t;
			}
			hit = true;
		}
	}

	return hit;
}

CMeshGen* createAudioGen()
{
	return rage_new CAudioGen();
}

CMeshGen* createAudioStitch()
{
	return rage_new CAudioStitch();
}

CMeshGen* createAudioGen_Display()
{
	return rage_new CAudioGen_Display();
}

struct BuilderItem
{
	CMeshGen* (*create)();
	const char* name;
};

static BuilderItem g_builders[] = 
{
	{ createAudioGen, "Audio mesh builder"	},
	{ createAudioStitch, "Audio mesh stitch" },
	{ createAudioGen_Display, "Audio mesh display"},
};

static const int g_nbuilders = sizeof(g_builders)/sizeof(BuilderItem);


int CAudioMeshMaker::Main(char * triFile, char * paramsFile, char * buildMode, char * inputPath)
{

	if(!triFile)
	{ //Run gui version

		//Init SDL
		if(SDL_Init(SDL_INIT_EVERYTHING) !=0)
		{
			Assertf(0, "Could not initialise SDL");
			return -1;
		}

		//Init OpenGl
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
		SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);	
		SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);

		const SDL_VideoInfo* vi = SDL_GetVideoInfo();

		int width = vi->current_w - 20;
		int height = vi->current_h - 80;
		
		SDL_Surface* screen = SDL_SetVideoMode(width, height, 0, SDL_OPENGL);
		if(!screen)
		{
			Assertf(0, "Could not initialize SDL opengl");
			return -1;
		}

		SDL_WM_SetCaption("Audio Mesh Maker", 0);


		if(!imguiRenderGLInit("x:/tools/bin/navgen/NavMeshMaker/DroidSans.ttf"))
		{
			Assertf(0, "Could not init GUI renderer.");
			SDL_Quit();
			return -1;
		}

		float t = 0.0f;
		u32 lastTime = SDL_GetTicks();
		int mx = 0, my = 0;
		float rx = 45;
		float ry = -45;
		float moveW = 0, moveS = 0, moveA = 0, moveD = 0, moveQ = 0, moveE = 0;
		float camx = 0, camy = 0, camz = 0, camr = 10;
		float origrx=0.f, origry=0.f;
		int origx =0, origy=0;
		bool rotate = false;
		float rays[3], raye[3];
		bool mouseOverMenu = false;
		bool showMenu = true;
		bool showLog = false;
		bool showDebugMode = true;
		//bool showTools = true;
		bool showLevels = false;
		bool showBuilder = false;

		int propScroll = 0;
		int logScroll = 0;
		//int toolsScroll = 0;
		int debugScroll = 0;

		char builderName[64] = "Choose Builder....";

		FileList meshFiles;
		char meshName[128] = "Choose Mesh....";
		char meshFolder[256] = "x:/gta5/exported_navmeshes/";

		CAudioLevelMesh* mesh = 0;
		float meshBMin[3], meshBMax[3];

		//float mpos[3]


		CMeshGen* builder = 0;

		agLog log;
		log.clear();
		agSetLog(&log);

		glEnable(GL_CULL_FACE);

		float fogCol[4];
		fogCol[0] = 0.32f;
		fogCol[1] = 0.25f;
		fogCol[2] = 0.25f;
		fogCol[3] = 1;

		glEnable(GL_FOG);
		glFogi(GL_FOG_MODE, GL_LINEAR);
		glFogf(GL_FOG_START, 0);
		glFogf(GL_FOG_END, 10);
		glFogfv(GL_FOG_COLOR, fogCol);

		glEnable(GL_POINT_SMOOTH);
		glEnable(GL_LINE_SMOOTH);

		bool done = false;
		while(!done)
		{
			//Handle input events.
			int mscroll = 0;
			SDL_Event event;
			while(SDL_PollEvent(&event))
			{
				switch(event.type)
				{
					case SDL_KEYDOWN:
					{
						//Handle any key presses here
						if(event.key.keysym.sym == SDLK_ESCAPE)
						{
							done = true;
						}
						break;
					}
					case SDL_MOUSEBUTTONDOWN:
					{
						//Handle mouse clicks here.
						if(!mouseOverMenu)
						{
							//Rotate view
							if(event.button.button == SDL_BUTTON_RIGHT)
							{
								rotate = true;
								origx = mx;
								origy = my;
								origrx = rx;
								origry = ry;
							}
							else if(event.button.button == SDL_BUTTON_LEFT)
							{
								//Hit test mesh
								if(mesh && builder)
								{
									float t;
									if(Raycast(*mesh, rays, raye, t))
									{
										//Do stuff for the left mouse button (selecting regions etc)
									}
								}
							}
						}
						if(event.button.button == SDL_BUTTON_WHEELUP)
						{
							mscroll--;
						}
						else if(event.button.button == SDL_BUTTON_WHEELDOWN)
						{
							mscroll++;
						}
						break;
					}
					case SDL_MOUSEBUTTONUP:
					{
						//Handle mouse click releases here
						if(event.button.button == SDL_BUTTON_RIGHT)
						{
							rotate = false;
						}
						break;
					}
					case SDL_MOUSEMOTION:
					{
						mx = event.motion.x;
						my = height-1 - event.motion.y; 
						if(rotate)
						{
							int dx = mx - origx;
							int dy = my - origy;
							rx = origrx - dy*0.25f;
							ry = origry + dx*0.25f;
						}
						break;
					}
					case SDL_QUIT:
					{
						done = true;
						break;
					}
					default:
					{
						break;
					}
				}
			}

		
			u8 mbut = 0;
			if(SDL_GetMouseState(0,0) & SDL_BUTTON_LMASK)
			{
				mbut |= IMGUI_MBUT_LEFT;
			}
			if(SDL_GetMouseState(0,0) & SDL_BUTTON_RMASK)
			{
				mbut |= IMGUI_MBUT_RIGHT;
			}

			u32 time = SDL_GetTicks();
			float dt = (time - lastTime)/1000.f;
			lastTime = time;

			t += dt;

			//Update and render
			glViewport(0,0, width, height);
			glClearColor(0.3f, 0.3f, 0.32f, 1.f);
			glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glDisable(GL_TEXTURE_2D);

			//Render 3d
			glEnable(GL_DEPTH_TEST);
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			gluPerspective(50.f, (float)width/(float)height, 1.f, camr);
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();
			glRotatef(rx, 1, 0, 0);
			glRotatef(ry, 0, 1, 0);
			glTranslatef(-camx, -camy, -camz);

			//Get hit ray position and direction
			GLdouble proj[16];
			GLdouble model[16];
			GLint view[4];
			glGetDoublev(GL_PROJECTION_MATRIX, proj);
			glGetDoublev(GL_MODELVIEW_MATRIX, model);
			glGetIntegerv(GL_VIEWPORT, view);
			GLdouble x, y, z;
			gluUnProject(mx, my, 0.f, model, proj, view, &x, &y, &z);
			rays[0] = (float)x; rays[1] = (float)y; rays[2] = (float)z;
			gluUnProject(mx, my, 1.f, model, proj, view, &x, &y, &z);
			raye[0] = (float)x; raye[1] = (float)y; raye[2] = (float)z;

		

			//Handle keyboard movement
			u8* keystate = SDL_GetKeyState(NULL);
			moveW = Clamp(moveW + dt*4*(keystate[SDLK_w] ? 1:-1), 0.f, 1.f);
			moveS = Clamp(moveS + dt*4*(keystate[SDLK_s] ? 1:-1), 0.f, 1.f);
			moveA = Clamp(moveA + dt*4*(keystate[SDLK_a] ? 1:-1), 0.f, 1.f);
			moveD = Clamp(moveD + dt*4*(keystate[SDLK_d] ? 1:-1), 0.f, 1.f);
			moveQ = Clamp(moveQ + dt*4*(keystate[SDLK_q] ? 1:-1), 0.f, 1.f);
			moveE = Clamp(moveE + dt*4*(keystate[SDLK_e] ? 1:-1), 0.f, 1.f);

			float keybSpeed = 22.f;
			if(SDL_GetModState() & KMOD_SHIFT)
			{
				keybSpeed *= 4.f;
			}

			float movex = (moveD - moveA)*keybSpeed*dt;
			float movey = (moveS - moveW)*keybSpeed*dt;
			float movez = (moveE - moveQ)*keybSpeed*dt; //This one is up/down....y and z are wonky

			camx += movex * (float)model[0];
			camy += movex * (float)model[4];
			camz += movex * (float)model[8];

			camx += movey * (float)model[2];
			camy += movey * (float)model[6];
			camz += movey * (float)model[10];

			camx += movez * (float)model[1];
			camy += movez * (float)model[5];
			camz += movez * (float)model[9];

			glEnable(GL_FOG);

			if(builder)
			{
				builder->HandleRender();
			}

			//Render GUI
			glDisable(GL_DEPTH_TEST);
			glMatrixMode(GL_PROJECTION);
			glLoadIdentity();
			gluOrtho2D(0, width, 0, height);
			glMatrixMode(GL_MODELVIEW);
			glLoadIdentity();

			imguiBeginFrame(mx, my, mbut, mscroll);

			if(builder)
			{
				builder->HandleRenderOverlay(/*(double*)proj, (double*)model, (int*)view*/);
			}

			//Help text
			if(showMenu)
			{
				const char msg[] = "W/S/A/D: Move around    RMB: Rotate cam";
				imguiDrawText(width/2, height-20, IMGUI_ALIGN_CENTER, msg, imguiRGBA(255, 255, 255, 128));
			}

			mouseOverMenu = false;

			if(showMenu)
			{
				int propDiv = showDebugMode ? (int)(height*0.6f) : height;

				if(imguiBeginScrollArea("Properties", width-250-10, 10+height-propDiv, 250, propDiv-20, &propScroll))
				{
					mouseOverMenu = true;
				}

				if(imguiCheck("ShowLog", showLog))
				{
					showLog = !showLog;
				}
 
				imguiSeparator();
				imguiLabel("Builder");
				if(imguiButton(builderName))
				{
					if(showBuilder)
					{
						showBuilder = false;
					}
					else
					{
						showBuilder = true;
						showLevels = false;
					}
				}

				if(builder)
				{
					imguiSeparator();
					imguiLabel("InputMesh");
					if(imguiButton(meshName))
					{
						if(showLevels)
						{
							showLevels = false;
						}
						else
						{
							showBuilder = false;
							showLevels = true;
							ScanDirectory("X:/gta5/exported_navmeshes/", ".tri", meshFiles);
						}
					}
					if(mesh)
					{
						char text[64];
						snprintf(text, 64, "Verts: %.1f  Tris: %.1f", mesh->GetVertCount(), mesh->GetTriCount());
						imguiValue(text);
					}
					imguiSeparator();
				}

				if(mesh && builder)
				{
					builder->HandleSettings();

					if(imguiButton("Build"))
					{
						log.clear();
						if(!builder->HandleBuild())
						{
							showLog = true;
							logScroll = 0;
						}
					}
					imguiSeparator();
				}

				imguiEndScrollArea();

				if(showDebugMode)
				{
					if(imguiBeginScrollArea("Debug Mode", width-250-10, 10, 250, height-propDiv-10, &debugScroll))
					{
						mouseOverMenu = true;
					}

					if(builder)
					{
						builder->HandleDebugMode();
					}

					imguiEndScrollArea();
				}
			}

			//builder selection dialog
			if(showBuilder)
			{
				static int levelScroll = 0;
				if(imguiBeginScrollArea("Choose Builder", width-10-250-10-200, height-10-250, 200, 250, &levelScroll))
				{
					mouseOverMenu = true;
				}

				CMeshGen* newBuilder = 0;
				for(int i = 0; i < g_nbuilders; ++i)
				{
					if(imguiItem(g_builders[i].name))
					{
						newBuilder = g_builders[i].create();
						if(newBuilder)
						{
							strcpy(builderName, g_builders[i].name);
						}
					}
				}
				if(newBuilder)
				{
					delete builder;
					builder = newBuilder;
					if (mesh && builder)
					{
						builder->HandleMeshHasChanged(mesh->GetVerts(), mesh->GetVertCount(), mesh->GetTris(), mesh->GetNormals(),
								mesh->GetTriCount(), meshBMin, meshBMax, meshFolder, meshName);
					}
					showBuilder = false;
				}

				imguiEndScrollArea();
			}

			//level selection dialog
			if(showLevels)
			{
				static int levelScroll = 0;
				if(imguiBeginScrollArea("Choose Mesh", width-10-250-10-200, height-10-250, 200, 250, &levelScroll))
				{
					mouseOverMenu = true;
				}

				int meshToLoad = -1;
				for(int i = 0; i < meshFiles.size; ++i)
				{
					if(imguiItem(meshFiles.files[i]))
					{
						meshToLoad = i;
					}
				}

				if(meshToLoad != -1)
				{
					strncpy(meshName, meshFiles.files[meshToLoad], sizeof(meshName));
					meshName[sizeof(meshName)-1] = '\0';
					showLevels = false;

					delete mesh;
					mesh = 0;

					char path[256] = "";
					strcpy(path, meshFolder);
					strcat(path, meshName);

					mesh = rage_new CAudioLevelMesh;
					if(!mesh || !mesh->Load(path))
					{
						delete mesh;
						mesh = 0;
					}

					if(mesh && mesh->GetVerts())
					{
						agCalcBounds(mesh->GetVerts(), mesh->GetVertCount(), meshBMin,  meshBMax);
					} 

					if(builder)
					{	
						float bmin[3]; bmin[0] = (mesh->GetBMin())[0]; bmin[1] = meshBMin[1]; bmin[2] = (mesh->GetBMin())[2];
						float bmax[3]; bmax[0] = (mesh->GetBMax())[0]; bmax[1] = meshBMax[1]; bmax[2] = (mesh->GetBMax())[2];
						builder->HandleMeshHasChanged(mesh->GetVerts(), mesh->GetVertCount(), mesh->GetTris(),
								mesh->GetNormals(), mesh->GetTriCount(), bmin, bmax, meshFolder, meshName); //meshBMin, meshBMax);
					}

					//Reset camera and fog to match the mesh bounds
					camr = Sqrtf(square<f32>(meshBMax[0] - meshBMin[0]) +
							square<f32>(meshBMax[1]-meshBMin[1]) +
							square<f32>(meshBMax[2]-meshBMin[2])) / 2;
					camx = (meshBMax[0] + meshBMin[0]) / 2 + camr;
					camy = (meshBMax[1] + meshBMin[1]) / 2 + camr;
					camz = (meshBMax[2] + meshBMin[2]) / 2 + camr;
					camr *= 3;
					rx = 45;
					ry = -45;
					glFogf(GL_FOG_START, camr*0.2f);
					glFogf(GL_FOG_END, camr*1.25f);
				}

				imguiEndScrollArea();
			}

			//Log
			if(showLog && showMenu)
			{
				if(imguiBeginScrollArea("Log", 10, 10, width - 300, 200, &logScroll))
				{
					mouseOverMenu = true;
				}
				for (int i = 0; i < log.getMessageCount(); ++i)
				{
					imguiLabel(log.getMessageText(i));
				}
				imguiEndScrollArea();
			}

			imguiEndFrame();
			imguiRenderGLDraw();

			glEnable(GL_DEPTH_TEST);
			SDL_GL_SwapBuffers();
			glFlush();
		}

		imguiRenderGLDestroy();

		SDL_Quit();

		delete builder;
		delete mesh;

		return 0;
	}
	else if(triFile && buildMode ) //Run cmd line version
	{

		AudGenInputs inputs;

		Assert(triFile);

		char * tmpStr = "./";

		if(!inputPath)
		{
			inputPath = tmpStr;
		}

		Displayf("Processing %s for %s", buildMode, triFile);

		if(paramsFile)
		{
			FILE * filePtr = fopen(paramsFile, "rt");

			if(!filePtr)
			{
				Assertf(filePtr, "Couldn't open params file \"%s\" for Audio mesh generator", paramsFile);
				return -1;
			}


			CAudioMeshMaker::ReadParamsFile(filePtr, inputs);

			fclose(filePtr);
		}

		CMeshGen *builder = 0;
		CAudioLevelMesh *mesh = 0;
		float meshBMin[3], meshBMax[3];

		mesh = rage_new CAudioLevelMesh;

		char filePath[256] = "";

		strcpy(filePath, inputPath);
		strcat(filePath, "/");
		strcat(filePath, triFile);

		if(!mesh || !mesh->Load(filePath))
		{
			delete mesh;
			return -1;
		}
		

		if(mesh && mesh->GetVerts())
		{
			agCalcBounds(mesh->GetVerts(), mesh->GetVertCount(), meshBMin,  meshBMax);
		}


		for(int i=0; i < g_nbuilders; ++i)
		{
			if(!strcmp(buildMode, g_builders[i].name))
			{
				builder = g_builders[i].create();
				break;
			}
		}

		
		if(builder)
		{
			float bmin[3]; bmin[0] = (mesh->GetBMin())[0]; bmin[1] = meshBMin[1]; bmin[2] = (mesh->GetBMin())[2];
			float bmax[3]; bmax[0] = (mesh->GetBMax())[0]; bmax[1] = meshBMax[1]; bmax[2] = (mesh->GetBMax())[2];
			builder->HandleMeshHasChanged(mesh->GetVerts(), mesh->GetVertCount(), mesh->GetTris(),
					mesh->GetNormals(), mesh->GetTriCount(), bmin, bmax, inputPath, triFile);

			builder->ApplyInputs(inputs);


			builder->HandleBuild();

		delete builder;
		delete mesh;
		return 1;


		}
		else
		{
			Assertf(0, "Failed to create builder!");
		}

		delete builder;
		delete mesh;
		return 0;
		
	}
	else
	{
		Errorf("CAudioMeshMaker::Main requires both triFile and buildMode when called from the commandline");
		return 0;
	}
}

void CAudioMeshMaker::ReadParamsFile(FILE * pfile, AudGenInputs &inputs)
{
	char paramBuf[256] = {0};

	do
	{
		char *pRet = fgets(paramBuf, 256, pfile);
		if(!pRet)
			break;

		if(paramBuf[0] == '#')
			continue;

		//Extract parameters from file
		char * paramPtr = strstr(paramBuf, "-cellSize");
		if(paramPtr)
		{
		        sscanf(paramPtr, "-cellSize %f", &inputs.cellSize);
		}

		paramPtr = strstr(paramBuf, "-cellHeight");
		if(paramPtr)
		{
		        sscanf(paramPtr, "-cellHeight %f", &inputs.cellHeight);
		}

		paramPtr = strstr(paramBuf, "-minRegionSize");
		if(paramPtr)
		{
		        sscanf(paramPtr, "-minRegionSize %i", &inputs.minRegionSize);
		}
	
		paramPtr = strstr(paramBuf, "-mergeRegionSize");
		if(paramPtr)
		{
		        sscanf(paramPtr, "-mergeRegionSize %i", &inputs.mergeRegionSize);
		}

		paramPtr = strstr(paramBuf, "-maxEdgeLen");
		if(paramPtr)
		{
		        sscanf(paramPtr, "-maxEdgeLen %i", &inputs.maxEdgeLen);
		}

		paramPtr = strstr(paramBuf, "-maxVertsPerPoly");
		if(paramPtr)
		{
		        scanf(paramPtr, "-vertsPerPoly %i", &inputs.vertsPerPoly);
		}

		paramPtr = strstr(paramBuf, "-walkableRadius");
		if(paramPtr)
		{
		        sscanf(paramPtr, "-walkableRadius %i", &inputs.agentRadius);
		}
		
	} while(1);
}






	

