/////////////////////////////////////////////////////////////////////////////////
// FILE		: CAudioGen.cpp
// PURPOSE	: To generate audio meshes from level geometry.
// AUTHOR	: C. Walder (based on Recast by Mikko Menonen)
// STARTED	: 31/08/2009
///////////////////////////////////////////////////////////////////////////////
//
#include <float.h>
//#define _USE_MATH_DEFINES
//#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "CAudioGen.h"
#include "AudioGenLog.h"
#include "AudioGenTimer.h"
#include "AudioGenDebugDraw.h"

#include "imgui.h"
#include "SDL.h"
#include "SDL_opengl.h"


CAudioGen::CAudioGen():
	m_verts(NULL),
	m_nverts(0),
	m_tris(NULL),
	m_trinorms(NULL),
	m_ntris(0),
	m_keepInterResults(true),
	m_triflags(0),
	m_solid(0),
	m_compact(0),
	m_cset(0),
	m_pmesh(0),
	m_dmesh(0),
	m_loadMesh(0),
	m_loadCompact(0),
	m_drawMode(DRAWMODE_MESH),
	m_path(0),
	m_meshName(0),
	m_buildDetailMesh(true)
{
	m_path = rage_new char[128];
	m_meshName = rage_new char[128];
	ResetSettings();
}

CAudioGen::~CAudioGen()
{
	cleanup();
	delete [] m_path;
	m_path = 0;
	delete [] m_meshName;
	m_meshName = 0;
}

void CAudioGen::cleanup()
{
	delete [] m_triflags;
	m_triflags = 0;
	delete m_solid;
	m_solid = 0;
	delete m_compact;
	m_compact = 0;
	delete m_cset;
	m_cset = 0;
	delete m_pmesh;
	m_pmesh = 0;
	delete m_dmesh;
	m_dmesh = 0;
	delete m_loadMesh;
	m_loadMesh = 0;
	delete m_loadCompact;
	m_loadCompact = 0;
}

void CAudioGen::ResetSettings()
{
	m_cellHeight = 0.39f;
	m_cellSize = 0.39f;
	m_borderSize = 1;
	m_minRegionSize = 50;
	m_mergeRegionSize = 20;
	memset(m_bmin, 0, sizeof(float)*3);
	memset(m_bmax, 0, sizeof(float)*3);
	m_edgeMaxLen = 12.0f;
	m_edgeMaxError = 1.3f;
	m_vertsPerPoly = 6.0f;
	m_detailSampleDist = 6.0f;
	m_detailSampleMaxError = 1.0f;
	m_agentHeight = 0.1f; //Settings for sound: a very small, thin agent
	m_agentRadius = 0.1f; 
	m_agentMaxClimb = 0.9f;
	m_agentMaxSlope = 45.0f;
	ZeroMemory(m_path, sizeof(char)*128);
	ZeroMemory(m_meshName, sizeof(char)*128);
}

void CAudioGen::HandleRender()
{
	if(!m_verts || ! m_tris || !m_trinorms)
	{
		return;
	}

	//Draw mesh
	if(m_drawMode == DRAWMODE_MESH)
	{
		agDebugDrawMesh(m_verts, m_nverts, m_tris, m_trinorms, m_ntris, 0);
	}

	glDisable(GL_FOG);
	glDepthMask(GL_FALSE);


	//Draw bounds
	float col[4] = {1, 1, 1, 0.5};
	agDebugDrawBoxWire(m_bmin[0], m_bmin[1], m_bmin[2], m_bmax[0], m_bmax[1], m_bmax[2], col);


	glDepthMask(GL_TRUE);

	if(m_solid && m_drawMode == DRAWMODE_VOXELS)
	{
		glEnable(GL_FOG);
		agDebugDrawHeightfieldSolid(*m_solid);
		glDisable(GL_FOG); 
	}

	if(m_compact && m_drawMode == DRAWMODE_COMPACT)
	{
		glEnable(GL_FOG);
		agDebugDrawHeightfieldSolid(*m_solid);
		glDisable(GL_FOG);
		//agDebugDrawMesh(m_verts, m_nverts, m_tris, m_trinorms, m_ntris, 0);
		agDebugDrawCompactHeightfieldSolid(*m_compact);
	}

	if(m_compact && m_drawMode == DRAWMODE_DISTANCE)
	{
		agDebugDrawMesh(m_verts, m_nverts, m_tris, m_trinorms, m_ntris, 0);
		agDebugDrawCompactHeightFieldDistance(*m_compact);
	}

	if(m_compact && m_drawMode == DRAWMODE_REGIONS)
	{
		agDebugDrawMesh(m_verts, m_nverts, m_tris, m_trinorms, m_ntris, 0);
		agDebugDrawCompactHeightfieldRegions(*m_compact);
//		agDebugDrawCompactHeightfieldExtrudedRegions(*m_compact);
	}

	if (m_cset && m_drawMode == DRAWMODE_RAW_CONTOURS)
	{
		agDebugDrawMesh(m_verts, m_nverts, m_tris, m_trinorms, m_ntris, 0);
		glDepthMask(GL_FALSE);
		agDebugDrawRawContours(*m_cset);
		glDepthMask(GL_TRUE);
	}
	if (m_cset && m_drawMode == DRAWMODE_BOTH_CONTOURS)
	{
		agDebugDrawMesh(m_verts, m_nverts, m_tris, m_trinorms, m_ntris, 0);
		glDepthMask(GL_FALSE);
		agDebugDrawRawContours(*m_cset, 0.5f);
		agDebugDrawContours(*m_cset);
		glDepthMask(GL_TRUE);
	}
	if (m_cset && m_drawMode == DRAWMODE_CONTOURS)
	{
		agDebugDrawMesh(m_verts, m_nverts, m_tris, m_trinorms, m_ntris, 0);
		glDepthMask(GL_FALSE);
		agDebugDrawContours(*m_cset);
		glDepthMask(GL_TRUE);
	}
	if (m_compact && m_cset && m_drawMode == DRAWMODE_REGION_CONNECTIONS)
	{
		agDebugDrawMesh(m_verts, m_nverts, m_tris, m_trinorms, m_ntris, 0);
		agDebugDrawCompactHeightfieldRegions(*m_compact);
			
		glDepthMask(GL_FALSE);
		agDebugDrawRegionConnections(*m_cset);
		glDepthMask(GL_TRUE); 
	}
	if(m_compact && m_cset && m_drawMode == DRAWMODE_AUD_CONNECTIONS)
	{
		agDebugDrawMesh(m_verts, m_nverts, m_tris, m_trinorms, m_ntris, 0);
//		agDebugDrawCompactHeightfieldRegions(*m_compact);

		glDepthMask(GL_FALSE);
		agDebugDrawContours(*m_cset);
		agDebugDrawCompactHeightfieldAudConnections(*m_compact, *m_cset, 1.f);
		glDepthMask(GL_TRUE);
	}
	if(m_pmesh && m_drawMode == DRAWMODE_POLYMESH)
	{
		agDebugDrawMesh(m_verts, m_nverts, m_tris, m_trinorms, m_ntris, 0);
		glDepthMask(GL_FALSE);
		agDebugDrawPolyMesh(*m_pmesh);
		glDepthMask(GL_TRUE);
	} 
	if (m_dmesh && m_drawMode == DRAWMODE_POLYMESH_DETAIL)
	{
		agDebugDrawMesh(m_verts, m_nverts, m_tris, m_trinorms, m_ntris, 0);
		glDepthMask(GL_FALSE);
		agDebugDrawPolyMeshDetail(*m_dmesh);
		glDepthMask(GL_TRUE);
	}
	if(m_pmesh && m_cset && m_drawMode == DRAWMODE_POLY_AUD_CONNECTIONS)
	{	agDebugDrawMesh(m_verts, m_nverts, m_tris, m_trinorms, m_ntris, 0);
		glDepthMask(GL_FALSE);
		agDebugDrawPolyMesh(*m_pmesh);
		agDrawPolymeshAudConnections(*m_pmesh, *m_cset, 1.f);
		glDepthMask(GL_TRUE);
	}
	if(m_loadMesh && m_cset && m_drawMode == DRAWMODE_LOADED_MESH)
	{	agDebugDrawMesh(m_verts, m_nverts, m_tris, m_trinorms, m_ntris, 0);
		glDepthMask(GL_FALSE);
		agDebugDrawPolyMesh(*m_loadMesh);
		agDrawPolymeshAudConnections(*m_loadMesh, *m_cset, 1.f);
		glDepthMask(GL_TRUE);
	}

	
}

void CAudioGen::HandleRenderOverlay()
{
	//Nothing to see here.....(would be the toolRender Overlay if there was one
}

void CAudioGen::HandleMeshHasChanged(const float* verts, int nverts, 
		const int* tris, const float* trinorms,
		int ntris, const float* bmin, const float* bmax, char * path, char * meshName)
{
	m_verts = verts;
	m_nverts = nverts;
	m_tris = tris;
	m_trinorms = trinorms;
	m_ntris = ntris;
	vcopy(m_bmin, bmin);
	vcopy(m_bmax, bmax);
	FastAssert(path);
	FastAssert(meshName);
	strcpy(m_path, path);
	strcpy(m_meshName, meshName);
}

void CAudioGen::ApplyInputs(AudGenInputs &inputs)
{
	m_cellSize = inputs.cellSize;
	m_cellHeight = inputs.cellHeight;
	m_agentHeight = inputs.agentHeight;
	m_agentRadius = inputs.agentRadius;
	m_agentMaxClimb = inputs.maxClimb;
	m_agentMaxSlope = inputs.maxSlope;
	m_borderSize = inputs.borderSize;
	m_minRegionSize = inputs.minRegionSize;
	m_mergeRegionSize = inputs.mergeRegionSize;
	m_edgeMaxLen = inputs.maxEdgeLen;
	m_edgeMaxError = inputs.maxEdgeError;
	m_vertsPerPoly = inputs.vertsPerPoly;
	//m_detailSampleDist = inputs.detailSampleDist;
	//m_detailSampleMaxError = inputs.detailSampleMaxError;

	m_buildDetailMesh = false;
}

void CAudioGen::HandleSettings()
{
	imguiLabel("Rasterization");
	imguiSlider("Cell Size", &m_cellSize, 0.1f, 10.f, 0.01f);
	imguiSlider("Cell height", &m_cellHeight, 0.1f, 10.f, 0.01f);

	imguiSeparator();
	imguiLabel("Agent");
	imguiSlider("Height", &m_agentHeight, 0.1f, 5.0f, 0.1f);
	imguiSlider("Radius", &m_agentRadius, 0.0f, 5.0f, 0.1f);
	imguiSlider("Max Climb", &m_agentMaxClimb, 0.1f, 5.0f, 0.1f);
	imguiSlider("Max Slope", &m_agentMaxSlope, 0.0f, 90.0f, 1.0f);


	imguiLabel("Regions");
	imguiSlider("Border size", &m_borderSize, 0, 10, 1);
	imguiSlider("Min region size", &m_minRegionSize, 0, 150, 1);
	imguiSlider("Merge region size", &m_mergeRegionSize, 0, 150, 1);

	imguiSeparator();
	imguiLabel("Polygonization");
	imguiSlider("Max Edge Length", &m_edgeMaxLen, 0.0f, 50.0f, 1.0f);
	imguiSlider("Max Edge Error", &m_edgeMaxError, 0.1f, 3.0f, 0.1f);
	imguiSlider("Verts Per Poly", &m_vertsPerPoly, 3.0f, 12.0f, 1.0f);		

	imguiSeparator();
	imguiLabel("Detail Mesh");
	imguiSlider("Sample Distance", &m_detailSampleDist, 0.0f, 16.0f, 1.0f);
	imguiSlider("Max Sample Error", &m_detailSampleMaxError, 0.0f, 16.0f, 1.0f);
	
	imguiSeparator();

	int gw = 0; //grid width (x-axis)
	int gh = 0; //grid height (z-axis)
	agCalcGridSize(m_bmin, m_bmax, m_cellSize, &gw, &gh);
	char text[64];
	sprintf(text, "Voxels %d x %d", gw, gh);
	imguiValue(text);

	imguiSeparator();
	
	//More stuff for post voxellization etc
	
	if(imguiCheck("Keep Intermediate results", m_keepInterResults))
	{
		m_keepInterResults = !m_keepInterResults;
	}

	imguiSeparator();
	
}

bool CAudioGen::HandleBuild()
{
	if(!m_verts || !m_tris)
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "buildNavigation: Input mesh is not specified.");
		}
		return false;
	}

	cleanup();

	//
	//Step 1. Initialize build config.
	//
	
	//Init build config from gui
	memset(&m_cfg, 0, sizeof(m_cfg));
	m_cfg.cellSize = m_cellSize;
	m_cfg.cellHeight = m_cellHeight;
	m_cfg.borderSize = (int)m_borderSize;
	m_cfg.minRegionSize = (int)m_minRegionSize;
	m_cfg.mergeRegionSize = (int)m_mergeRegionSize;
	m_cfg.walkableSlopeAngle = m_agentMaxSlope;
	m_cfg.walkableHeight = (int)ceilf(m_agentHeight / m_cfg.cellHeight);
	m_cfg.walkableClimb = (int)ceilf(m_agentMaxClimb / m_cfg.cellHeight);
	m_cfg.walkableRadius = (int)ceilf(m_agentRadius / m_cfg.cellHeight);
	m_cfg.maxEdgeLen = (int)(m_edgeMaxLen / m_cellSize);
	m_cfg.maxSimplificationError = m_edgeMaxError;
	m_cfg.maxVertsPerPoly = (int)m_vertsPerPoly;
	m_cfg.detailSampleDist = m_detailSampleDist < 0.9f ? 0 : m_cellSize * m_detailSampleDist;
	m_cfg.detailSampleMaxError = m_cellHeight * m_detailSampleMaxError;

	agCalcGridSize(m_bmin, m_bmax, m_cellSize, &m_cfg.width, &m_cfg.length);
	
	//Set the area where the navivation will be built
	//The bounds of the input mesh are used but we could
	//specify it with a user defined box etc.
	vcopy(m_cfg.boundMin, m_bmin);
	vcopy(m_cfg.boundMax, m_bmax);
	//Reset build times gathering
	memset(&m_buildTimes, 0, sizeof(m_buildTimes));
	agSetBuildTimes(&m_buildTimes);

	//Start the build process.
	agTimeVal totStartTime = agGetPerformanceTimer();

	if(agGetLog())
	{
		agGetLog()->log(AG_LOG_PROGRESS, "Bulding navigation:");
		agGetLog()->log(AG_LOG_PROGRESS, " - %d x %d cells", m_cfg.width, m_cfg.length);
		agGetLog()->log(AG_LOG_PROGRESS, " - %.1fK verts, %.1fK tris", m_nverts/1000.f, m_ntris/1000.f);
	}


	//
	// Step 2. rasterize input polygon soup.
	//
	
	// Allocate voxel heightfield where we rasterize our input data to.
	m_solid = rage_new agHeightField;
	if(!m_solid)
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "buildNavigation: out of memory 'solid'.");
		}
		return false;
	}
	if(!agCreateHeightfield(*m_solid, m_cfg.width, m_cfg.length, m_cfg.boundMin, m_cfg.boundMax, m_cfg.cellSize, m_cfg.cellHeight))
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "buildNavigation: could not create solid heightfield");
		}
		return false;
	}

	// Allocate array that can hold triangle flags.
	// If we have multiple meshes we need to process, 
	// we allocate an array big enough for the max number
	// of triangles we need to process
	m_triflags = rage_new u8[m_ntris];
	if(!m_triflags)
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "buildNavigation: out of memory 'triangleFlags' (%d).", m_ntris);
		}
		return false;
	}

	//Do any tests on the original tris we want to mark them up with flags
	//then rasterize; if there are multiple meshes, they can be transformed,
	//flagged up and rasterized here.
	memset(m_triflags, 0, m_ntris*sizeof(u8));
	//If we were doing an ai navmesh, we'd mark the walkable tris here
	agRasterizeTriangles(m_verts, m_nverts, m_tris, m_triflags, m_ntris, *m_solid);

	if(!m_keepInterResults)
	{
		delete [] m_triflags;
		m_triflags = 0;
	}

	//Step 3.
	//Filter any spans that are marked as ignorable or do any other span-flag based gubbins
	

	//Step 4.
	//Create a compact heightfield for faster data processing; creates more cache coherent data
	//and calculates neighbours between cells 
	
//	m_compact = rage_new agCompactVolumefield;
	m_compact = rage_new agCompactHeightField;

	if(!m_compact)
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "buildNavigation: Out of memory 'compact'.");
		}
		return false;
	}

//	agBuildCompactVolumeField(*m_solid, *m_compact);
	agBuildCompactHeightField(2, 2, 0, *m_solid, *m_compact);
	//check the compact has been successfully built here
	
	//Generate distances for each span to the nearest edge
	agBuildDistanceField(*m_compact);
	//check distance field was built succesfully here here

	//Use watershedding on the distance field to partition the compact into regions
	agBuildRegions(*m_compact, m_cfg.walkableRadius, m_cfg.borderSize, m_cfg.minRegionSize, m_cfg.mergeRegionSize);
	//check regions were built successfully here

	//
	///Step 5. Trace and Simplify region contours
	//

	// Create Contours
	m_cset = rage_new agContourSet;
	if(!m_cset)
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "buildnavigation: out of memory 'cset' .");
		}
		return false;
	}
	if(!agBuildContours(*m_compact, m_cfg.maxSimplificationError, m_cfg.maxEdgeLen, *m_cset))
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "buildNavigation: Could not create contours.");
		}
		return false;
	}

	//
	// Step 6. Build polygons mesh from contours.
	//
	
	// Build polygon navmesh from the contours.
	m_pmesh = rage_new agPolyMesh;
	if (!m_pmesh)
	{
		if (agGetLog())
			agGetLog()->log(AG_LOG_ERROR, "buildNavigation: Out of memory 'pmesh'.");
		return false;
	}
	if (!agBuildPolyMesh(*m_cset, *m_compact, m_cfg.maxVertsPerPoly, *m_pmesh))
	{
		if (agGetLog())
			agGetLog()->log(AG_LOG_ERROR, "buildNavigation: Could not triangulate contours.");
		return false;
	}


	if(m_buildDetailMesh)
	{
		//
		// Step 7. Create detail mesh which allows to access approximate height on each polygon.
		//

		//Polymesh detail currently is buggy: if we ever want an AI navmesh we'll need to fix this. 
		m_dmesh = rage_new agPolyMeshDetail;
		if (!m_dmesh)
		{
			if (agGetLog())
				agGetLog()->log(AG_LOG_ERROR, "buildNavigation: Out of memory 'pmdtl'.");
			return false;
		}

		if (!agBuildPolyMeshDetail(*m_pmesh, *m_compact, m_cfg.detailSampleDist, m_cfg.detailSampleMaxError, *m_dmesh))
		{
			if (agGetLog())
				agGetLog()->log(AG_LOG_ERROR, "buildNavigation: Could not build detail mesh.");
		}
	}

	char saveName[128] = {0};
	//char meshIndex[32] = {0};

	int Xindex = 0, Yindex = 0;

	sscanf(m_meshName, "navmesh[%d][%d].tri", &Xindex, &Yindex);
	
	//Save the polymesh and additional stitching information in the intermediate folder
	formatf(saveName, "%s/audmesh[%i][%i].agi", m_path, Xindex, Yindex);
	
	agSavePolyMeshBinary(saveName, m_meshName, *m_pmesh, m_compact);

	m_loadMesh = rage_new agPolyMesh();
	if(!m_loadMesh)
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "buildNavigation: Out of memory 'loadMesh'");
		}
		return false;
	}
	
	m_loadCompact = rage_new agCompactHeightField();
	if(!m_loadCompact)
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "buildNavigation: Out of memory 'loadCompact'");
		}
		return false;
	}
	if(!agLoadSurfaceBinary(saveName, *m_loadMesh, m_loadCompact))
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "buildNavigation: Could not load polymesh");
		}
	}

	
	agTimeVal totEndTime = agGetPerformanceTimer();

	//Show performance stats
	if(agGetLog())
	{
		const float pc = 100.f / agGetDeltaTimeUsec(totStartTime, totEndTime);

		agGetLog()->log(AG_LOG_PROGRESS, "Rasterize; %.1fms (%.1f%%)", m_buildTimes.rasterizeTriangles/1000.f, m_buildTimes.rasterizeTriangles*pc);

		agGetLog()->log(AG_LOG_PROGRESS, "Build compact; %.1fms (%.1f%%)", m_buildTimes.buildCompact/1000.f, m_buildTimes.buildCompact*pc);

		agGetLog()->log(AG_LOG_PROGRESS, "Build Distancefield: %.1fms (%.1f%%)", m_buildTimes.buildDistanceField/1000.f, m_buildTimes.buildDistanceField*pc);
		agGetLog()->log(AG_LOG_PROGRESS, "- distance: %.1fms (%.1f%%)", m_buildTimes.buildDistanceFieldDist/1000.f, m_buildTimes.buildDistanceFieldDist*pc);

		
		agGetLog()->log(AG_LOG_PROGRESS, "Build Regions: %.1fms (%.1f%%)", m_buildTimes.buildRegions/1000.0f, m_buildTimes.buildRegions*pc);
		agGetLog()->log(AG_LOG_PROGRESS, "  - watershed: %.1fms (%.1f%%)", m_buildTimes.buildRegionsReg/1000.0f, m_buildTimes.buildRegionsReg*pc);
		agGetLog()->log(AG_LOG_PROGRESS, "    - expand: %.1fms (%.1f%%)", m_buildTimes.buildRegionsExp/1000.0f, m_buildTimes.buildRegionsExp*pc);
		agGetLog()->log(AG_LOG_PROGRESS, "    - find catchment basins: %.1fms (%.1f%%)", m_buildTimes.buildRegionsFlood/1000.0f, m_buildTimes.buildRegionsFlood*pc);
		agGetLog()->log(AG_LOG_PROGRESS, "  - filter: %.1fms (%.1f%%)", m_buildTimes.buildRegionsFilter/1000.0f, m_buildTimes.buildRegionsFilter*pc);
		
		agGetLog()->log(AG_LOG_PROGRESS, "Build Contours: %.1fms (%.1f%%)", m_buildTimes.buildContours/1000.0f, m_buildTimes.buildContours*pc);
		agGetLog()->log(AG_LOG_PROGRESS, "  - trace: %.1fms (%.1f%%)", m_buildTimes.buildContoursTrace/1000.0f, m_buildTimes.buildContoursTrace*pc);
		agGetLog()->log(AG_LOG_PROGRESS, "  - simplify: %.1fms (%.1f%%)", m_buildTimes.buildContoursSimplify/1000.0f, m_buildTimes.buildContoursSimplify*pc);
		
		agGetLog()->log(AG_LOG_PROGRESS, "Build Polymesh: %.1fms (%.1f%%)", m_buildTimes.buildPolymesh/1000.0f, m_buildTimes.buildPolymesh*pc);
		agGetLog()->log(AG_LOG_PROGRESS, "Build Polymesh Detail: %.1fms (%.1f%%)", m_buildTimes.buildDetailMesh/1000.0f, m_buildTimes.buildDetailMesh*pc);
		
		agGetLog()->log(AG_LOG_PROGRESS, "Polymesh: Verts:%d  Polys:%d", m_pmesh->nverts, m_pmesh->npolys);
		agGetLog()->log(AG_LOG_PROGRESS, "Polymesh detail: Verts:%d  Meshes:%d Tris:%d", m_dmesh->nverts, m_dmesh->nmeshes, m_dmesh->ntris);

		agGetLog()->log(AG_LOG_PROGRESS, "TOTAL: %.1fms", agGetDeltaTimeUsec(totStartTime, totEndTime)/1000.f);
	}

	return true;	
}

void CAudioGen::HandleDebugMode()
{
	//Check which modes are valid
	bool valid[MAX_DRAWMODE] = {0};

	if(m_verts && m_tris)
	{
		valid[DRAWMODE_MESH] = true;
		valid[DRAWMODE_VOXELS] = m_solid != 0;
		valid[DRAWMODE_COMPACT] = m_compact != 0;
		valid[DRAWMODE_DISTANCE] = m_compact !=0;
		valid[DRAWMODE_REGIONS] = m_compact != 0;
		valid[DRAWMODE_REGION_CONNECTIONS] = m_cset != 0;
		valid[DRAWMODE_RAW_CONTOURS] = m_cset != 0;
		valid[DRAWMODE_BOTH_CONTOURS] = m_cset != 0;
		valid[DRAWMODE_CONTOURS] = m_cset != 0; 
		valid[DRAWMODE_AUD_CONNECTIONS] = m_cset != 0 && m_compact != 0;
		valid[DRAWMODE_POLYMESH] = m_pmesh !=0;
		valid[DRAWMODE_POLYMESH_DETAIL] = m_dmesh !=0;
		valid[DRAWMODE_POLY_AUD_CONNECTIONS] = m_pmesh !=0 && m_cset !=0;
		valid[DRAWMODE_LOADED_MESH] = m_loadMesh !=0 && m_cset != 0;
	}

	int unavail = 0;
	for(int i = 0; i < MAX_DRAWMODE; ++i)
	{
		if(!valid[i]) unavail++;
	}

	if(unavail == MAX_DRAWMODE)
	{
		return;
	}

	imguiLabel("Draw");

	if(imguiCheck("Input Mesh", m_drawMode == DRAWMODE_MESH, valid[DRAWMODE_MESH]))
	{
		m_drawMode = DRAWMODE_MESH;
	}
	if(imguiCheck("Voxels", m_drawMode == DRAWMODE_VOXELS, valid[DRAWMODE_VOXELS]))
	{
		m_drawMode = DRAWMODE_VOXELS;
	}
	if(imguiCheck("Compact", m_drawMode == DRAWMODE_COMPACT, valid[DRAWMODE_COMPACT]))
	{
		m_drawMode = DRAWMODE_COMPACT;
	}
	if(imguiCheck("Distance", m_drawMode == DRAWMODE_DISTANCE, valid[DRAWMODE_DISTANCE]))
	{
		m_drawMode = DRAWMODE_DISTANCE;
	}
	if(imguiCheck("Regions", m_drawMode == DRAWMODE_REGIONS, valid[DRAWMODE_REGIONS]))
	{
		m_drawMode = DRAWMODE_REGIONS;
	}
	if (imguiCheck("Region Connections", m_drawMode == DRAWMODE_REGION_CONNECTIONS, valid[DRAWMODE_REGION_CONNECTIONS]))
	{
		m_drawMode = DRAWMODE_REGION_CONNECTIONS;
	}
	if (imguiCheck("Raw Contours", m_drawMode == DRAWMODE_RAW_CONTOURS, valid[DRAWMODE_RAW_CONTOURS]))
	{
		m_drawMode = DRAWMODE_RAW_CONTOURS;
	}
	if (imguiCheck("Both Contours", m_drawMode == DRAWMODE_BOTH_CONTOURS, valid[DRAWMODE_BOTH_CONTOURS]))
	{
		m_drawMode = DRAWMODE_BOTH_CONTOURS;
	}
	if (imguiCheck("Contours", m_drawMode == DRAWMODE_CONTOURS, valid[DRAWMODE_CONTOURS]))
	{
		m_drawMode = DRAWMODE_CONTOURS;
	}
	if(imguiCheck("Aud Connections", m_drawMode == DRAWMODE_AUD_CONNECTIONS, valid[DRAWMODE_AUD_CONNECTIONS]))
	{
		m_drawMode = DRAWMODE_AUD_CONNECTIONS;
	}
	if(imguiCheck("Polymesh", m_drawMode == DRAWMODE_POLYMESH, valid[DRAWMODE_POLYMESH]))
	{
		m_drawMode = DRAWMODE_POLYMESH;
	}
	if(imguiCheck("Poly aud connections", m_drawMode == DRAWMODE_POLY_AUD_CONNECTIONS, valid[DRAWMODE_POLY_AUD_CONNECTIONS]))
	{
		m_drawMode = DRAWMODE_POLY_AUD_CONNECTIONS;
	}
	if(imguiCheck("Polymesh Detail", m_drawMode == DRAWMODE_POLYMESH_DETAIL, valid[DRAWMODE_POLYMESH_DETAIL]))
	{
		m_drawMode = DRAWMODE_POLYMESH_DETAIL;
	}
	if(imguiCheck("Loaded mesh", m_drawMode == DRAWMODE_LOADED_MESH,  valid[DRAWMODE_LOADED_MESH]))
	{
		m_drawMode = DRAWMODE_LOADED_MESH;
	}
	
	if(unavail)
	{
		imguiValue("Tick 'Keep Intermediate Results'");
		imguiValue("to see more results.");
	}
}







