#include <float.h>
//#define _USE_MATH_DEFINES
//#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "CAudioGen.h"
#include "AudioGenLog.h"
#include "AudioGenTimer.h"
#include "AudioGenDebugDraw.h"

#include "imgui.h"
#include "SDL.h"
#include "SDL_opengl.h"

void agIntArray::resize(int n)
{
	if (n > m_cap)
	{
		if (!m_cap) m_cap = 8;
		while (m_cap < n) m_cap *= 2;
		int* newData = rage_new int[m_cap];
		if (m_size && newData) memcpy(newData, m_data, m_size*sizeof(int));
		delete [] m_data;
		m_data = newData;
	}
	m_size = n;
}

void agCalcBounds(const float* verts, int nv, float* bmin, float* bmax)
{
	// Calculate bounding box.
	vcopy(bmin, verts);
	vcopy(bmax, verts);
	for (int i = 1; i < nv; ++i)
	{
		const float* v = &verts[i*3];
		vmin(bmin, v);
		vmax(bmax, v);
	}
}

void agCalcGridSize(const float* bmin, const float* bmax, float cs, int* w, int* h)
{
	*w = (int)((bmax[0] - bmin[0])/cs+0.5f);
	*h = (int)((bmax[2] - bmin[2])/cs+0.5f);
}

bool agCreateHeightfield(agHeightField& hf, int width, int length,
						 const float* bmin, const float* bmax,
						 float cs, float ch)
{
	hf.width = width;
	hf.length = length;
	hf.spans = rage_new agSpan*[hf.width*hf.length];
	vcopy(hf.boundMin, bmin);
	vcopy(hf.boundMax, bmax);
	hf.cellSize = cs;
	hf.cellHeight = ch;
	if (!hf.spans)
		return false;
	memset(hf.spans, 0, sizeof(agSpan*)*hf.width*hf.length);
	return true;
}

static int getVoxelCount(agHeightField &field)
{
	int w = field.width;
	int l = field.length;
	int spanCount = 0;


	for (int y = 0; y < l; ++y)
	{
		for(int x = 0; x < w; ++x)
		{
			for (agSpan *s = field.spans[x + y*w]; s; s = s->next)
			{
				//Flags to exclude spans/voxels would be tested here
				spanCount++;
			}
		}
	}
	return spanCount;
}

static int getSpanCount(u8 flags, agHeightField &field)
{
	int w = field.width;
	int l = field.length;
	int spanCount = 0;
	for(int y=0; y <l; ++y)
	{
		for(int x=0; x < w; ++x)
		{
			for(agSpan *s = field.spans[x+y*w]; s; s= s->next)
			{
				if(s->flags == flags)
				{
					spanCount++;
				}
			}
		}
	}
	return spanCount;
}

//Set any voxel faces which aren't covered up to active
static bool markActiveVoxelFaces(agCompactVolumefield &compact)
{
	int w = compact.width;
	int l = compact.length;
	for(int y=0; y<l; ++y)
	{
		for(int x=0; x<w; ++x)
		{
			agCompact3dCell &cell = compact.cells[x + y*w];
			for(int i = (int)cell.index, endi = (int)(cell.index+cell.count), j=0; i<endi; ++i, ++j)
			{
				agCompactVoxel & vox = compact.voxels[i];
				
				//Check up - is the next voxel directly above us?
				if(j+1 == cell.count || (compact.voxels[i+1].y != (vox.y +1)))
				{
					vox.faces[AG_VOX_TOP].flags |= AG_FACE_ACTIVE;
				}

				//Check down - is the previous voxel directly below us?
				if(j != 0 && (compact.voxels[i-1].y != (vox.y-1)))
				{
					vox.faces[AG_VOX_BOTTOM].flags |= AG_FACE_ACTIVE;
				}

				//Check to the sides - look in the adjacent cells to see if they have a voxel at the same height
				for(int dir=0; dir < 4; ++dir)
				{
					//get neighbour cell's x and y
					int nx = x + agGetTopDirOffsetX(dir);
					int ny = y + agGetTopDirOffsetY(dir);
					//First check if the neibour is out of bounds
					if(nx < 0 || ny <0 || nx >= w || ny >= l)
					{
						continue;
					}

					//Iterate over voxels in neighbour cells
					const agCompact3dCell nCell = compact.cells[nx + ny*w];
					vox.faces[agGetTopDirFace(dir)].flags |= AG_FACE_ACTIVE;
					for(int k = (int)nCell.index, endk = int(nCell.index+nCell.count); k<endk; k++)
					{
						const agCompactVoxel &nVox = compact.voxels[k];
						if(nVox.y == vox.y)
						{
							vox.faces[agGetTopDirFace(dir)].flags &= ~AG_FACE_ACTIVE;
							break;
						}
						else if(nVox.y > vox.y)
						{
							break;
						}
					}
				}
			}
		}
	}
	return true;
}

inline void setCon(agCompactVoxel &vox, u32 face, int dir, int index)
{
	//Check we're in bounds
	FastAssert(face < AG_NUM_FACES);
	//Unset the index for this direction
//	vox.faces[face].con &= ~(AG_NULL_CONNECTION << (dir*10)); //con is 40bits of a u64 storing 4 10bit indexes (to neighbour connections)
	//Set the new index
	vox.faces[face].con[dir] = (u16)index;
}

inline void setCon(agCompactSpan &span, int dir, int index)
{
	span.con &= ~(0xf << (dir*4));
	span.con |= (index&0xf) << (dir*4);
}

//This was originally used to prohibit regions being created of faces with existing connections
//but the algorithm works better without it 
inline u64 getFaceIsConnected(agCompactVoxel &/*vox*/, u32 /*face*/)
{
	static const u64 mask = (u64)(AG_NULL_CONNECTION + (AG_NULL_CONNECTION << 10) + (AG_NULL_CONNECTION<<20) + (AG_NULL_CONNECTION << 30));
	return false; //(vox.faces[face].con[0] != AG_NULL_CONNECTION || vox.faces[face].con[1] != AG_NULL_CONNECTION || vox.faces[face].con[2] != AG_NULL_CONNECTION || vox.faces[face].con[3] != AG_NULL_CONNECTION);//(vox.faces[face].con); //&& (vox.faces[face].con ^ mask);
}

static bool setupTopConnections(agCompactVolumefield &compact, int maxStep, int w /*width*/, int l /*length*/)
{
	//First do the top faces (what would be the floor in a navmesh generation process)
	for(int y=0; y<l; ++y)
	{
		for(int x=0; x<w; ++x)
		{
			agCompact3dCell &cell = compact.cells[x + y*w];
			for(int i = (int)cell.index, endi = (int)(cell.index+cell.count); i<endi; ++i)
			{
				agCompactVoxel &vox = compact.voxels[i];
				//If the face isn't active, skip it
				if(!(vox.faces[AG_VOX_TOP].flags & AG_FACE_ACTIVE))
				{
					continue;
				}
				for(int dir =0; dir < 4; ++dir)
				{
					//This initiallizes the faces connections to 'none' (AG_NULL_CONNECTION)
					setCon(vox, AG_VOX_TOP, dir, AG_NULL_CONNECTION);
					int nx = x + agGetTopDirOffsetX(dir); //neighbour x coordinate
					int ny = y + agGetTopDirOffsetY(dir); //neighbour y coordinate (y in 2d or z in 3d)

					//Check that the neighbour cell is in bounds 
					if(nx <0 || ny < 0 || nx >= w || ny >= l)
					{
						continue;
					}
					
					//check neighbour faces to see if any are valid for connections
					int neighbourIndex = -1;
					const agCompact3dCell &ncell = compact.cells[nx + ny*w];
					for(int k = (int)ncell.index, endk = (int)(ncell.index+ncell.count); k<endk; ++k)
					{
						const agCompactVoxel &nvox = compact.voxels[k];
						//Is the face viable?
						//TODO: make the step height variable from the GUI
						if((nvox.faces[AG_VOX_TOP].flags & AG_FACE_ACTIVE) && Abs(nvox.y - vox.y) <= maxStep)
						{
							neighbourIndex = k-ncell.index;
						}

					}

					//We should now have the highes viable neighbour (if there is one)
					if(neighbourIndex >= 0)
					{
						setCon(vox, AG_VOX_TOP, dir, neighbourIndex);
						//Deactivate any faces that are hidden by this new connection
						const agCompactVoxel &nvox = compact.voxels[ncell.index+neighbourIndex];
						if(nvox.y < vox.y)
						{
							//If the neighbour is below our voxel we need to deactivate faces in the current cell (looking down on the neighbour voxel) 
							for(int m = 0; m < (vox.y - nvox.y); m++)
							{
								compact.voxels[i-m].faces[agGetTopDirFace(dir)].flags &= ~AG_FACE_ACTIVE;
							}
						}
						else if(nvox.y > vox.y)
						{
							//If the neighbour is above our voxel we need to deactivate the faces in the neighbour cell looking down on our voxel
							for(int m = 0; m < (nvox.y - vox.y); m++)
							{
								//We need to deactivate the face opposite to the direction so add 2 to it
								compact.voxels[ncell.index+neighbourIndex+m].faces[agGetTopDirFace(dir+2)].flags &= ~AG_FACE_ACTIVE;
							}
						}

					}
				}
			}
		}
	}
	return true;
}

static bool setupBottomConnections(agCompactVolumefield &compact, int maxStep, int w /*width*/, int l /*length*/)
{

	for(int  y=0; y < l; ++y)
	{
		for(int x=0; x < w; ++x)
		{
			agCompact3dCell &cell = compact.cells[x + y*w];
			for(int i = (int)cell.index, endi = (int)(cell.index+cell.count); i<endi; ++i)
			{
				agCompactVoxel &vox = compact.voxels[i];
				//If the face isn't active, skip it
				if(!(vox.faces[AG_VOX_BOTTOM].flags & AG_FACE_ACTIVE))
				{
					continue;
				}
				for(int dir =0; dir < 4; ++dir)
				{
					//This initiallizes the faces connections to 'none' (AG_NULL_CONNECTION)
					setCon(vox, AG_VOX_BOTTOM, dir, AG_NULL_CONNECTION);
					int nx = x + agGetTopDirOffsetX(dir); //neighbour x coordinate
					int ny = y + agGetTopDirOffsetY(dir); //neighbour y coordinate (y in 2d or z in 3d)

					//Check that the neighbour cell is in bounds 
					if(nx <0 || ny < 0 || nx >= w || ny >= l)
					{
						continue;
					}
					
					//check neighbour faces to see if any are valid for connections
					int neighbourIndex = -1;
					const agCompact3dCell &ncell = compact.cells[nx + ny*w];
					for(int k = (int)ncell.index, endk = (int)(ncell.index+ncell.count); k<endk; ++k)
					{
						const agCompactVoxel &nvox = compact.voxels[k];
						//Is the face viable?
						//TODO: make the step height variable from the GUI
						if((nvox.faces[AG_VOX_BOTTOM].flags & AG_FACE_ACTIVE) && Abs(nvox.y - vox.y) <= maxStep)
						{
							neighbourIndex = k-ncell.index;
							break; //for the bottom faces we're interested in the lowest viable face
						}

					}

					//We should now have the highest viable neighbour (if there is one)
					if(neighbourIndex >= 0)
					{
						setCon(vox, AG_VOX_BOTTOM, dir, neighbourIndex);
						//Deactivate any faces that are hidden by this new connection
						const agCompactVoxel &nvox = compact.voxels[ncell.index+neighbourIndex];
						if(nvox.y < vox.y)
						{
							//If the neighbour is below our voxel we need to deactivate faces in the neighbour cell looking up on our voxel 
							for(int m = 0; m < (vox.y - nvox.y); m++)
							{
								//We need to deactivate the face opposite to the direction so add 2 to it
								compact.voxels[ncell.index+neighbourIndex-m].faces[agGetTopDirFace(dir+2)].flags &= ~AG_FACE_ACTIVE;
							}
						}
						else if(nvox.y > vox.y)
						{
							//If the neighbour is above our voxel we need to deactivate the faces in our cell looking up on the neighbour voxel
							for(int m = 0; m < (nvox.y - vox.y); m++)
							{
								compact.voxels[ncell.index+neighbourIndex+m].faces[agGetTopDirFace(dir)].flags &= ~AG_FACE_ACTIVE;
							}
						}

					}
				}
			}
		}
	}
	return true;
}

static bool setupLeftConnections(agCompactVolumefield &compact, int maxStep, int w /*width*/, int l /*length*/)
{
	for(int y=0; y<l; ++y)
	{
		for(int x=0; x<w; ++x)
		{
			int found = 0;
			agCompact3dCell &cell = compact.cells[x+y*w];
			for(int i= (int)cell.index, endi = (int)(cell.index+cell.count); i<endi; ++i)
			{
				agCompactVoxel &vox = compact.voxels[i];
				//If the face isn't active, skip it
				if(!(vox.faces[AG_VOX_LEFT].flags & AG_FACE_ACTIVE))
				{
					continue;
				}
				
				setCon(vox, AG_VOX_LEFT, 0, AG_NULL_CONNECTION);
				//Check front (direction 0), looking at cells in the next row up for valid voxels
				for(int k = (x-maxStep), endk = (x+maxStep); k<=endk; k++)
				{
					//Check against bounds
					if(k<0 || k >=w || (y+1) >= l)
					{
						continue;
					}
					agCompact3dCell &ncell = compact.cells[k+(y+1)*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; m++)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if(nvox.y == vox.y && (nvox.faces[AG_VOX_LEFT].flags&AG_FACE_ACTIVE))
						{
							//Deactivate any faces that are hidden by this new connection
							if(k < x)
							{
								//We can't set up a connection that crosses an existing connection so ignore this connection if this happens
								if(getFaceIsConnected(nvox, AG_VOX_BACK)) continue;
								nvox.faces[AG_VOX_BACK].flags &= ~AG_FACE_ACTIVE;
								for(int q = k+1; q < x; ++q)
								{
									agCompact3dCell &qcell = compact.cells[q+(y+1)*w];
									for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
									{
										agCompactVoxel & rvox = compact.voxels[r];
										if(rvox.y == vox.y)
										{
											if(getFaceIsConnected(rvox, AG_VOX_BACK)) continue;
											rvox.faces[AG_VOX_BACK].flags &= ~AG_FACE_ACTIVE;
											break;
										}
										else if(rvox.y > (vox.y)) break;
									}
								}
							}
							else if(x < k)
							{
								//We can't set up a connection that crosses an existing connection so bail if this happens
								if(getFaceIsConnected(vox, AG_VOX_FRONT)) continue;
								vox.faces[AG_VOX_FRONT].flags &= ~AG_FACE_ACTIVE;
								for(int q = x+1; q < k; ++q)
								{
									agCompact3dCell &qcell = compact.cells[q+y*w];
									for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
									{
										agCompactVoxel &rvox = compact.voxels[r];
										if(rvox.y == vox.y)
										{
											if(getFaceIsConnected(rvox, AG_VOX_FRONT)) continue;
											rvox.faces[AG_VOX_FRONT].flags &= ~AG_FACE_ACTIVE;
										}
										else if(rvox.y > (vox.y)) break;
									}
								}
							}
							//Unlike the horizontal faces, we have to store the x coordinate of the neighbour cell
							//not the index....we might be able to squeeze that in too though with a bit of creative
							//bit fiddling...
							found = 1;
							setCon(vox, AG_VOX_LEFT, 0, k);
							break;
						}
						else if(nvox.y > vox.y)
						{
							break; //no voxels in this cell have the required height
						}
					}
					if(found)
					{
						found = 0;
						break;
					}
				}
					
				setCon(vox, AG_VOX_LEFT, 2, AG_NULL_CONNECTION);
				//Check back (direction 2), looking at cells in the next row up for valid voxels
				for(int k = (x-maxStep), endk = (x+maxStep); k<=endk; k++)
				{
					//Check against bounds
					if(k<0 || k >=w || (y-1) < 0)
					{
						continue;
					}
					agCompact3dCell &ncell = compact.cells[k+(y-1)*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; m++)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if(nvox.y == vox.y && (nvox.faces[AG_VOX_LEFT].flags&AG_FACE_ACTIVE))
						{

							//Deactivate any faces that are hidden by this new connection
							if(k < x)
							{
								//We can't set up a connection that crosses an existing connection so bail if this happens
								if(getFaceIsConnected(nvox, AG_VOX_FRONT)) continue;
								nvox.faces[AG_VOX_FRONT].flags &= ~AG_FACE_ACTIVE;
								for(int q = k+1; q < x; ++q)
								{
									agCompact3dCell &qcell = compact.cells[q+(y-1)*w];
									for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
									{
										agCompactVoxel & rvox = compact.voxels[r];
										if(rvox.y == vox.y)
										{
											if(getFaceIsConnected(rvox, AG_VOX_FRONT)) continue;
											rvox.faces[AG_VOX_FRONT].flags &= ~AG_FACE_ACTIVE;
											break;
										}
										else if(rvox.y > (vox.y)) break;
									}
								} 
							}
							else if(x < k)
							{
								//We can't set up a connection that crosses an existing connection so bail if this happens
								if(getFaceIsConnected(vox, AG_VOX_BACK)) continue;
								vox.faces[AG_VOX_BACK].flags &= ~AG_FACE_ACTIVE;
								for(int q = x+1; q < k; ++q)
								{
									agCompact3dCell &qcell = compact.cells[q+y*w];
									for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
									{
										agCompactVoxel &rvox = compact.voxels[r];
										if(rvox.y == vox.y)
										{
											if(getFaceIsConnected(rvox, AG_VOX_BACK)) continue;
											rvox.faces[AG_VOX_BACK].flags &= ~AG_FACE_ACTIVE;
										}
										else if(rvox.y > (vox.y)) break;
			
									}
								}
							}
							//Unlike the horizontal faces, we have to store the x coordinate of the neighbour cell
							//not the index....we might be able to squeeze that in too though with a bit of creative
							//bit fiddling...
							setCon(vox, AG_VOX_LEFT, 2, k);
							found = 1;
							break;
						}
						else if(nvox.y > vox.y)
						{
							break; //no voxels in this cell have the required y coordinate
						}
					}
					if(found)
					{
						found = 0;
						break;
					}
				}

				//Check up (direction 1)
				setCon(vox, AG_VOX_LEFT, 1, AG_NULL_CONNECTION);
				//We can skip searching the cell group containing the current voxel
				for(int k = (x - maxStep); k < x; ++k)
				{
					if(k<0)
					{
						continue;
					}
					agCompact3dCell &ncell = compact.cells[k+y*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; ++m)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if(nvox.y == (vox.y + 1) && nvox.faces[AG_VOX_LEFT].flags&AG_FACE_ACTIVE)
						{
							
							//Deactivate any faces that have been hidden by the new connection
							//We can't set up a connection that crosses an existing connection so bail if this happens
							if(getFaceIsConnected(nvox, AG_VOX_BOTTOM)) continue;
							nvox.faces[AG_VOX_BOTTOM].flags &= ~AG_FACE_ACTIVE;
							for(int q = k+1; q < x; ++q)
							{
								agCompact3dCell &qcell = compact.cells[q+y*w];
								for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
								{
									agCompactVoxel & rvox = compact.voxels[r];
									if(rvox.y == (vox.y+1))
									{
										if(getFaceIsConnected(rvox, AG_VOX_BOTTOM)) continue;
										rvox.faces[AG_VOX_BOTTOM].flags &= ~AG_FACE_ACTIVE;
										break;
									}
									else if(rvox.y > (vox.y+1)) break;
								}
							}
							setCon(vox, AG_VOX_LEFT, 1, k);
							found = 1;
							break;

						}
						else if(nvox.y > (vox.y +1)) break;
					}
					if(found)
					{
						found = 0;
						break;
					}
				}

				//Look to see if there's a voxel directly above us
				if(!found && (i-cell.index)<(u32)(cell.count-1) && (compact.voxels[i+1].y == vox.y+1) && compact.voxels[i+1].faces[AG_VOX_LEFT].flags&AG_FACE_ACTIVE)
				{
					setCon(vox, AG_VOX_LEFT, 1, x);
					found = 1;
				}

				for(int k = x+1, endk = x+maxStep; !found && k<=endk; k++)
				{
					if(k>=w)
					{
						continue;
					}

					agCompact3dCell &ncell = compact.cells[k+y*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; ++m)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if((nvox.y == (vox.y + 1)) && nvox.faces[AG_VOX_LEFT].flags&AG_FACE_ACTIVE)
						{
							
							//We can't set up a connection that crosses an existing connection so bail if this happens
							if(getFaceIsConnected(vox, AG_VOX_TOP)) continue;
							//Deactivate any faces that have been hidden by the new connection
							vox.faces[AG_VOX_TOP].flags &= ~AG_FACE_ACTIVE;
							for(int q = x+1; q < k; ++q)
							{
								agCompact3dCell &qcell = compact.cells[q+y*w];
								for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; r++)
								{
									agCompactVoxel & rvox = compact.voxels[r];
									if(rvox.y == vox.y)
									{
										if(getFaceIsConnected(rvox, AG_VOX_TOP)) continue;
										rvox.faces[AG_VOX_TOP].flags &= ~AG_FACE_ACTIVE;
										break;
									}
									else if(rvox.y > (vox.y)) break;
								}
							}
							setCon(vox, AG_VOX_LEFT, 1, k);
							found = 1;
							break;

						}
						else if(nvox.y > (vox.y + 1)) break;
					}
					if(found)
					{
						found = 0;
						break;
					}
				}

				
				//Check down (direction 3)
				setCon(vox, AG_VOX_LEFT, 3, AG_NULL_CONNECTION);
				//We can skip searching the cell group containing the current voxel
				for(int k = (x - maxStep); k < x; ++k)
				{
					if(k<0)
					{
						continue;
					}
					agCompact3dCell &ncell = compact.cells[k+y*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; ++m)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if(nvox.y == (vox.y - 1) && nvox.faces[AG_VOX_LEFT].flags&AG_FACE_ACTIVE)
						{
							
							//We can't set up a connection that crosses an existing connection so bail if this happens
							if(getFaceIsConnected(nvox, AG_VOX_TOP)) continue;
							//Deactivate any faces that have been hidden by the new connection
							nvox.faces[AG_VOX_TOP].flags &= ~AG_FACE_ACTIVE;
							for(int q = k+1; q < x; ++q)
							{
								agCompact3dCell &qcell = compact.cells[q+y*w];
								for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
								{
									agCompactVoxel & rvox = compact.voxels[r];
									if(rvox.y == (vox.y-1))
									{
										if(getFaceIsConnected(rvox, AG_VOX_TOP)) continue;
										rvox.faces[AG_VOX_TOP].flags &= ~AG_FACE_ACTIVE;
										break;
									}
									else if(rvox.y >= (vox.y)) break;
								}
							}
							setCon(vox, AG_VOX_LEFT, 3, k);
							found = 1;
							break;

						}
						else if(nvox.y >= (vox.y)) break;
					}
					if(found)
					{
						found = 0;
						break;
					}
				}

				//Look to see if there's a voxel directly below us
				if(!found && (i-cell.index)>=0 && (compact.voxels[i-1].y == (vox.y-1)) && compact.voxels[i-1].faces[AG_VOX_LEFT].flags&AG_FACE_ACTIVE)
				{
					setCon(vox, AG_VOX_LEFT, 3, x);
					found = 1;
				}

				for(int k = x+1, endk = x+maxStep; !found && k<=endk; ++k)
				{
					if(k >=w)
					{
						break;
					}
					agCompact3dCell &ncell = compact.cells[k+y*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; ++m)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if(nvox.y == (vox.y - 1)  && (nvox.faces[AG_VOX_LEFT].flags&AG_FACE_ACTIVE))
						{
							
							//We can't set up a connection that crosses an existing connection so bail if this happens
							if(getFaceIsConnected(vox, AG_VOX_BOTTOM)) continue;
							//Deactivate any faces that have been hidden by the new connection
							vox.faces[AG_VOX_BOTTOM].flags &= ~AG_FACE_ACTIVE;
							for(int q = x+1; q < k; ++q)
							{
								agCompact3dCell &qcell = compact.cells[q+y*w];
								for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
								{
									agCompactVoxel & rvox = compact.voxels[r];
									if(rvox.y == vox.y)
									{
										if(getFaceIsConnected(rvox, AG_VOX_BOTTOM)) continue;
										rvox.faces[AG_VOX_BOTTOM].flags &= ~AG_FACE_ACTIVE;
										break;
									}
									else if(rvox.y > vox.y)	break;
								}
							}
							setCon(vox, AG_VOX_LEFT, 3, k);
							found = 1;
							break;

						}
						else if(nvox.y > (vox.y-1)) break;
					}
					if(found)
					{
						found = 0;
						break;
					}
				}
			}
		}
	}
	return true;	
}

static bool setupRightConnections(agCompactVolumefield &compact, int maxStep, int w /*width*/, int l /*length*/)
{
	for(int y=0; y<l; ++y)
	{
		for(int x=0; x<w; ++x)
		{
			int found = 0;
			agCompact3dCell &cell = compact.cells[x+y*w];
			for(int i= (int)cell.index, endi = (int)(cell.index+cell.count); i<endi; ++i)
			{
				agCompactVoxel &vox = compact.voxels[i];
				//If the face isn't active, skip it
				if(!(vox.faces[AG_VOX_RIGHT].flags & AG_FACE_ACTIVE))
				{
					continue;
				}
				
				//Check back (direction 0), looking at cells in the next row up for valid voxels
				setCon(vox, AG_VOX_RIGHT, 0, AG_NULL_CONNECTION);
				for(int k = (x+maxStep), endk = (x-maxStep); k>=endk;k--)
				{
					//Check against bounds
					if(k<0 || k >=w || (y-1) < 0)
					{
						continue;
					}
					agCompact3dCell &ncell = compact.cells[k+(y-1)*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; ++m)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if(nvox.y == vox.y && (nvox.faces[AG_VOX_RIGHT].flags&AG_FACE_ACTIVE))
						{

							//Deactivate any faces that are hidden by this new connection
							if(k < x)
							{
								//We can't set up a connection that crosses an existing connection so bail if this happens
								if(getFaceIsConnected(vox, AG_VOX_BACK)) continue;
								vox.faces[AG_VOX_BACK].flags &= ~AG_FACE_ACTIVE;
								for(int q = k+1; q < x; ++q)
								{
									agCompact3dCell &qcell = compact.cells[q+y*w];
									for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
									{
										agCompactVoxel & rvox = compact.voxels[r];
										if(rvox.y == vox.y)
										{
											if(getFaceIsConnected(rvox, AG_VOX_BACK)) continue;
											rvox.faces[AG_VOX_BACK].flags &= ~AG_FACE_ACTIVE;
											break;
										}
										else if(rvox.y > (vox.y)) break;
									}
								}
							}
							else if(x < k)
							{
								//We can't set up a connection that crosses an existing connection so bail if this happens
								if(getFaceIsConnected(nvox, AG_VOX_FRONT)) continue;
								nvox.faces[AG_VOX_FRONT].flags &= ~AG_FACE_ACTIVE;
								for(int q = x+1; q < k; ++q)
								{
									agCompact3dCell &qcell = compact.cells[q+(y-1)*w];
									for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
									{
										agCompactVoxel &rvox = compact.voxels[r];
										if(rvox.y == vox.y)
										{
											if(getFaceIsConnected(rvox, AG_VOX_FRONT)) continue;
											rvox.faces[AG_VOX_FRONT].flags &= ~AG_FACE_ACTIVE;
										}
										else if(rvox.y > (vox.y)) break;
									}
								}
							}
							//Unlike the horizontal faces, we have to store the x coordinate of the neighbour cell
							//not the index....we might be able to squeeze that in too though with a bit of creative
							//bit fiddling...
							setCon(vox, AG_VOX_RIGHT, 0, k);
							found = 1; 
							break;
						}
						else if(nvox.y > vox.y)
						{
							break; //no voxels in this cell have the required height
						}
					}
					if(found)
					{
						found = 0;
						break;
					}
				}
					
				//Check front (direction 2), looking at cells in the next row up for valid voxels
				setCon(vox, AG_VOX_RIGHT, 2, AG_NULL_CONNECTION);
				for(int k = (x+maxStep), endk = (x-maxStep); k>=endk; k--)
				{
					//Check against bounds
					if(k<0 || k >=w || (y+1) >= l)
					{
						continue;
					}
					agCompact3dCell &ncell = compact.cells[k+(y+1)*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; m++)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if(nvox.y == vox.y && (nvox.faces[AG_VOX_RIGHT].flags&AG_FACE_ACTIVE))
						{
							//Deactivate any faces that are hidden by this new connection
							if(k < x)
							{
								//We can't set up a connection that crosses an existing connection so bail if this happens
								if(getFaceIsConnected(vox, AG_VOX_FRONT)) continue;
								vox.faces[AG_VOX_FRONT].flags &= ~AG_FACE_ACTIVE;
								for(int q = k+1; q < x; ++q)
								{
									agCompact3dCell &qcell = compact.cells[q+y*w];
									for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
									{
										agCompactVoxel & rvox = compact.voxels[r];
										if(rvox.y == vox.y)
										{
											if(getFaceIsConnected(rvox, AG_VOX_FRONT)) continue;
											rvox.faces[AG_VOX_FRONT].flags &= ~AG_FACE_ACTIVE;
											break;
										}
										else if(rvox.y > (vox.y)) break;
									}
								} 
							}
							else if(x < k)
							{
								//We can't set up a connection that crosses an existing connection so bail if this happens
								if(getFaceIsConnected(nvox, AG_VOX_FRONT)) continue;
								nvox.faces[AG_VOX_BACK].flags &= ~AG_FACE_ACTIVE;
								for(int q = x+1; q < k; ++q)
								{
									agCompact3dCell &qcell = compact.cells[q+(y+1)*w];
									for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
									{
										agCompactVoxel &rvox = compact.voxels[r];
										if(rvox.y == vox.y)
										{
											if(getFaceIsConnected(rvox, AG_VOX_BACK)) continue;
											rvox.faces[AG_VOX_BACK].flags &= ~AG_FACE_ACTIVE;
										}
										else if(rvox.y > (vox.y)) break;
									}
								}
							}
							//Unlike the horizontal faces, we have to store the x coordinate of the neighbour cell
							//not the index....we might be able to squeeze that in too though with a bit of creative
							//bit fiddling...
							setCon(vox, AG_VOX_RIGHT, 2, k);
							found = 1;
							break;
						}
						else if(nvox.y > vox.y)
						{
							break; //no voxels in this cell have the required y coordinate
						}
					}
					if(found)
					{
						found = 0; 
						break;
					}
				}

				//Check up (direction 1)
				setCon(vox, AG_VOX_RIGHT, 1, AG_NULL_CONNECTION);
				//We can skip searching the cell group containing the current voxel
				for(int k = (x + maxStep); k > x; --k)
				{
					if(k>=w)
					{
						continue;
					}
					agCompact3dCell &ncell = compact.cells[k+y*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; ++m)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if(nvox.y == (vox.y + 1) && (nvox.faces[AG_VOX_RIGHT].flags&AG_FACE_ACTIVE))
						{
							
							//We can't set up a connection that crosses an existing connection so bail if this happens
							if(getFaceIsConnected(nvox, AG_VOX_BOTTOM)) continue;
							//Deactivate any faces that have been hidden by the new connection
							nvox.faces[AG_VOX_BOTTOM].flags &= ~AG_FACE_ACTIVE;
							for(int q = x+1; q < k; ++q)
							{
								agCompact3dCell &qcell = compact.cells[q+y*w];
								for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
								{
									agCompactVoxel & rvox = compact.voxels[r];
									if(rvox.y == (vox.y+1))
									{
										if(getFaceIsConnected(rvox, AG_VOX_BOTTOM)) continue;
										rvox.faces[AG_VOX_BOTTOM].flags &= ~AG_FACE_ACTIVE;
										break;
									}
									else if(rvox.y > (vox.y+1)) break;
								}
							}
							setCon(vox, AG_VOX_RIGHT, 1, k);
							found = 1;
							break;

						}
						else if(nvox.y > (vox.y +1)) break;
					}
					if(found)
					{
						found = 0;
						break;
					}
				}

				//Look to see if there's a voxel directly above us
				if(!found && (i-cell.index)< (u32)(cell.count-1) && (compact.voxels[i+1].y == (vox.y+1)) && compact.voxels[i+1].faces[AG_VOX_RIGHT].flags&AG_FACE_ACTIVE)
				{
					setCon(vox, AG_VOX_RIGHT, 1, x);
					found = 1;
				}

				for(int k = x-1, endk = x-maxStep; !found && k>=endk; --k)
				{
					if(k<0)
					{
						break;
					}
					agCompact3dCell &ncell = compact.cells[k+y*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; ++m)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if(nvox.y == (vox.y+1) && nvox.faces[AG_VOX_RIGHT].flags&AG_FACE_ACTIVE)
						{
							
							//We can't set up a connection that crosses an existing connection so bail if this happens
							if(getFaceIsConnected(vox, AG_VOX_TOP)) continue;
							//Deactivate any faces that have been hidden by the new connection
							vox.faces[AG_VOX_TOP].flags &= ~AG_FACE_ACTIVE;
							for(int q = k+1; q < x; ++q)
							{
								agCompact3dCell &qcell = compact.cells[q+y*w];
								for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
								{
									agCompactVoxel & rvox = compact.voxels[r];
									if(rvox.y == vox.y)
									{
										if(getFaceIsConnected(rvox, AG_VOX_TOP)) continue;
										rvox.faces[AG_VOX_TOP].flags &= ~AG_FACE_ACTIVE;
										break;
									}
									else if(rvox.y > (vox.y)) break;
								}
							}
							setCon(vox, AG_VOX_RIGHT, 1, k);
							found = 1;
							break;

						}
						else if(nvox.y > (vox.y+1)) break;
					}
					if(found)
					{
						found = 0;
						break;
					}
				}

				
				//Check down (direction 3)
				setCon(vox, AG_VOX_RIGHT, 3, AG_NULL_CONNECTION);
				//We can skip searching the cell group containing the current voxel
				for(int k = (x + maxStep); k > x; --k)
				{
					if(k>=w)
					{
						continue;
					}
					agCompact3dCell &ncell = compact.cells[k+y*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; ++m)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if(nvox.y == (vox.y-1) && nvox.faces[AG_VOX_RIGHT].flags&AG_FACE_ACTIVE)
						{
							
							//We can't set up a connection that crosses an existing connection so bail if this happens
							if(getFaceIsConnected(nvox, AG_VOX_TOP)) continue;
							//Deactivate any faces that have been hidden by the new connection
							nvox.faces[AG_VOX_TOP].flags &= ~AG_FACE_ACTIVE;
							for(int q = x+1; q < k; ++q)
							{
								agCompact3dCell &qcell = compact.cells[q+y*w];
								for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
								{
									agCompactVoxel & rvox = compact.voxels[r];
									if(rvox.y == (vox.y-1))
									{
										if(getFaceIsConnected(rvox, AG_VOX_TOP)) continue;
										rvox.faces[AG_VOX_TOP].flags &= ~AG_FACE_ACTIVE;
										break;
									}
									else if(rvox.y >= (vox.y)) break;
								}
							}
							setCon(vox, AG_VOX_RIGHT, 3, k);
							found=1;
							break;

						}
						else if(nvox.y >= (vox.y)) break;
					}
					if(found)
					{
						found = 0;
						break;
					}
				}

				//Look to see if there's a voxel directly below us
				if(!found && (i-cell.index)>0 && (compact.voxels[i-1].y == (vox.y-1)) && compact.voxels[i-1].faces[AG_VOX_RIGHT].flags&AG_FACE_ACTIVE)
				{
					setCon(vox, AG_VOX_RIGHT, 3, x);
					found = 1;
				}

				for(int k = x-1, endk = x-maxStep; !found && k<=endk; --k)
				{
					if(k<0)
					{
						break;
					}
					agCompact3dCell &ncell = compact.cells[k+y*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; ++m)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if(nvox.y == (vox.y-1) && nvox.faces[AG_VOX_RIGHT].flags&AG_FACE_ACTIVE)
						{
							
							//We can't set up a connection that crosses an existing connection so bail if this happens
							if(getFaceIsConnected(vox, AG_VOX_BOTTOM)) continue;
							//Deactivate any faces that have been hidden by the new connection
							vox.faces[AG_VOX_BOTTOM].flags &= ~AG_FACE_ACTIVE;
							for(int q = k+1; q < x; ++q)
							{
								agCompact3dCell &qcell = compact.cells[q+y*w];
								for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
								{
									agCompactVoxel & rvox = compact.voxels[r];
									if(rvox.y == vox.y)
									{
										if(getFaceIsConnected(rvox, AG_VOX_BOTTOM)) continue;
										rvox.faces[AG_VOX_BOTTOM].flags &= ~AG_FACE_ACTIVE;
										break;
									}
									else if(rvox.y > vox.y) break;
								}
							}
							setCon(vox, AG_VOX_RIGHT, 3, k);
							found = 1;
							break;

						}
						else if(nvox.y > (vox.y+1)) break;
					}
					if(found)
					{
						found = 0;
						break;
					}
				}
			}
		}
	}
	return true;	
}

static bool setupFrontConnections(agCompactVolumefield &compact, int maxStep, int w /*width*/, int l /*length*/)
{
	if(1)
	{
		for(int y=0; y<l; ++y)
		{
			for(int x=0; x<w; ++x)
			{
				agCompact3dCell &cell = compact.cells[x+y*w];
				for(int i = cell.index, endi = (cell.index+cell.count); i<endi; ++i)
				{
					agCompactVoxel &vox = compact.voxels[i];
					if(vox.faces[AG_VOX_FRONT].flags&AG_FACE_ACTIVE)
					{
						vox.faces[AG_VOX_FRONT].con[0] = AG_NULL_CONNECTION;
						if(x-1 >= 0)
						{
							agCompact3dCell &ncell = compact.cells[x-1+(y)*w];
							for(int j = ncell.index, endj = (ncell.index + ncell.count); j<endj; ++j)
							{
								agCompactVoxel &nvox = compact.voxels[j];
								if(nvox.y == vox.y && nvox.faces[AG_VOX_FRONT].flags&AG_FACE_ACTIVE)
								{
									vox.faces[AG_VOX_FRONT].con[0] = (u16)j;
								}
							}
						}
						vox.faces[AG_VOX_FRONT].con[2] = AG_NULL_CONNECTION;
						if(x+1<l)
						{
							agCompact3dCell &ncell = compact.cells[x+1+(y)*w];
							for(int j = ncell.index, endj = (ncell.index + ncell.count); j<endj; ++j)
							{
								agCompactVoxel& nvox = compact.voxels[j];
								if(nvox.y == vox.y && nvox.faces[AG_VOX_FRONT].flags&AG_FACE_ACTIVE)
								{
									vox.faces[AG_VOX_FRONT].con[2] = (u16)j;
								}
							}
						}
						vox.faces[AG_VOX_FRONT].con[1] = AG_NULL_CONNECTION;
						if(i > (int)cell.index)
						{
							agCompactVoxel &nvox = compact.voxels[i-1];
							if(nvox.y == (vox.y-1) && nvox.faces[AG_VOX_FRONT].flags&AG_FACE_ACTIVE)
							{
								vox.faces[AG_VOX_FRONT].con[1] = (u16)(i-1);
							}
						}
						vox.faces[AG_VOX_FRONT].con[3] = AG_NULL_CONNECTION;
						if(i < (endi-1))
						{
							agCompactVoxel &nvox = compact.voxels[i+1];
							if(nvox.y == (vox.y+1) && nvox.faces[AG_VOX_FRONT].flags&AG_FACE_ACTIVE)
							{
								vox.faces[AG_VOX_FRONT].con[3] = (u16)(i+1);
							}
						}
					}
				}
			}
		}
		return true;
	}

	if(1)
	{
		for(int i=0; i < compact.voxelCount; i++)
		{
			for(int dir = 0; dir < 4; dir++)
			{
				compact.voxels[i].faces[AG_VOX_FRONT].con[dir] = 1;
			}	
		}
		return true;
	}

	for(int y=0; y<l; ++y)
	{
		for(int x=0; x<w; ++x)
		{
			int found = 0;
			agCompact3dCell &cell = compact.cells[x+y*w];
			for(int i= (int)cell.index, endi = (int)(cell.index+cell.count); i<endi; ++i)
			{
				agCompactVoxel &vox = compact.voxels[i];
				//If the face isn't active, skip it
				if(!(vox.faces[AG_VOX_FRONT].flags & AG_FACE_ACTIVE))
				{
					continue;
				}
				
				//Check left (direction 0), looking at cells in the next row up for valid voxels
				setCon(vox, AG_VOX_FRONT, 0, AG_NULL_CONNECTION);
				for(int k = (y+maxStep), endk = (y-maxStep); k>=endk; --k)
				{
					//Check against bounds
					if(k<0 || k>=l || (x-1)<0)
					{
						continue;
					}
					agCompact3dCell &ncell = compact.cells[x-1+k*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; m++)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if(nvox.y == vox.y && (nvox.faces[AG_VOX_FRONT].flags&AG_FACE_ACTIVE))
						{
							//Unlike the horizontal faces, we have to store the y coordinate of the neighbour cell
							//not the index....we might be able to squeeze that in too though with a bit of creative
							//bit fiddling...
							setCon(vox, AG_VOX_FRONT, 0, k);
							found = 1;
							//Deactivate any faces that are hidden by this new connection
							if(k < y)
							{
								if(getFaceIsConnected(vox, AG_VOX_LEFT)) continue;
								vox.faces[AG_VOX_LEFT].flags &= ~AG_FACE_ACTIVE;
								for(int q = k+1; q < y; ++q)
								{
									agCompact3dCell &qcell = compact.cells[x + q*w];
									for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
									{
										agCompactVoxel & rvox = compact.voxels[r];
										if(rvox.y == vox.y)
										{
											if(getFaceIsConnected(rvox, AG_VOX_LEFT)) continue;
											rvox.faces[AG_VOX_LEFT].flags &= ~AG_FACE_ACTIVE;
											break;
										}
										else if(rvox.y > (vox.y)) break;
									}
								}
							}
							else if(y < k)
							{
								if(getFaceIsConnected(nvox, AG_VOX_RIGHT)) continue;
								nvox.faces[AG_VOX_RIGHT].flags &= ~AG_FACE_ACTIVE;
								for(int q = y+1; q < k; ++q)
								{
									agCompact3dCell &qcell = compact.cells[x-1 + q*w];
									for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
									{
										agCompactVoxel &rvox = compact.voxels[r];
										if(rvox.y == vox.y)
										{
											if(getFaceIsConnected(rvox, AG_VOX_RIGHT)) continue;
											rvox.faces[AG_VOX_RIGHT].flags &= ~AG_FACE_ACTIVE;
										}
										else if(rvox.y > (vox.y)) break;
									}
								}
							}
							break;
						}
						else if(nvox.y > vox.y)
						{
							break; //no voxels in this cell have the required height
						}
					}
					if(found)
					{
						found = 0;
						break;
					}
				}
					
				//Check right (direction 2), looking at cells in the next row up for valid voxels
				setCon(vox, AG_VOX_FRONT, 2, AG_NULL_CONNECTION);
				for(int k = (y+maxStep), endk = (y-maxStep); k>=endk; k--)
				{
					//Check against bounds
					if(k<0 || k>=l || (x+1) >= w)
					{
						continue;
					}
					agCompact3dCell &ncell = compact.cells[x+1+k*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; m++)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if(nvox.y == vox.y && (nvox.faces[AG_VOX_FRONT].flags&AG_FACE_ACTIVE))
						{
							//Unlike the horizontal faces, we have to store the y coordinate of the neighbour cell
							//not the index....we might be able to squeeze that in too though with a bit of creative
							//bit fiddling...
							setCon(vox, AG_VOX_FRONT, 2, k);
							found = 1;
							//Deactivate any faces that are hidden by this new connection
							if(k < y)
							{
								if(getFaceIsConnected(vox, AG_VOX_RIGHT)) continue;
								vox.faces[AG_VOX_RIGHT].flags &= ~AG_FACE_ACTIVE;
								for(int q = k+1; q < y; ++q)
								{
									agCompact3dCell &qcell = compact.cells[x +q*w];
									for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
									{
										agCompactVoxel & rvox = compact.voxels[r];
										if(rvox.y == vox.y)
										{
											if(getFaceIsConnected(rvox, AG_VOX_RIGHT)) continue;
											rvox.faces[AG_VOX_RIGHT].flags &= ~AG_FACE_ACTIVE;
											break;
										}
										else if(rvox.y > (vox.y)) break;
									}
								} 
							}
							else if(y < k)
							{
								if(getFaceIsConnected(nvox, AG_VOX_LEFT)) continue;
								nvox.faces[AG_VOX_LEFT].flags &= ~AG_FACE_ACTIVE;
								for(int q = y+1; q < k; ++q)
								{
									agCompact3dCell &qcell = compact.cells[x+1+q*w];
									for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
									{
										agCompactVoxel &rvox = compact.voxels[r];
										if(rvox.y == vox.y)
										{
											if(getFaceIsConnected(rvox, AG_VOX_LEFT)) continue;
											rvox.faces[AG_VOX_LEFT].flags &= ~AG_FACE_ACTIVE;
										}
										else if(rvox.y > (vox.y)) break;
			
									}
								}
							}
							break;
						}
						else if(nvox.y > vox.y)
						{
							break; //no voxels in this cell have the required y coordinate
						}
					}
					if(found)
					{
						found = 0;
						break;
					}
				}

				//Check up (direction 1)
				setCon(vox, AG_VOX_FRONT, 1, AG_NULL_CONNECTION);
				//We can skip searching the cell group containing the current voxel
				for(int k = (y + maxStep); k > y; --k)
				{
					if(k>=l)
					{
						continue;
					}
					agCompact3dCell &ncell = compact.cells[x+k*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; ++m)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if(nvox.y == (vox.y + 1) && nvox.faces[AG_VOX_FRONT].flags&AG_FACE_ACTIVE)
						{
							setCon(vox, AG_VOX_FRONT, 1, k);
							found = 1;
							
							if(getFaceIsConnected(nvox, AG_VOX_BOTTOM)) continue;
							//Deactivate any faces that have been hidden by the new connection
							nvox.faces[AG_VOX_BOTTOM].flags &= ~AG_FACE_ACTIVE;
							for(int q = y+1; q < k; ++q)
							{
								agCompact3dCell &qcell = compact.cells[x+q*w];
								for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
								{
									agCompactVoxel & rvox = compact.voxels[r];
									if(rvox.y == (vox.y+1))
									{
										if(getFaceIsConnected(rvox, AG_VOX_BOTTOM)) continue;
										rvox.faces[AG_VOX_BOTTOM].flags &= ~AG_FACE_ACTIVE;
										break;
									}
									else if(rvox.y > (vox.y+1)) break;
								}
							}
							break;

						}
						else if(nvox.y > (vox.y +1)) break;
					}
					if(found)
					{
						found = 0;
						break;
					}
				}

				//Look to see if there's a voxel directly above us
				if(!found && (i-cell.index)<(u32)(cell.count-1) && (compact.voxels[i+1].y == (vox.y+1)) && compact.voxels[i+1].faces[AG_VOX_FRONT].flags&AG_FACE_ACTIVE)
				{
					setCon(vox, AG_VOX_FRONT, 1, y);
					found = 1;
				}

				for(int k = y-1, endk = y-maxStep; !found && k>=endk; --k)
				{
					if(k<0)
					{
						break;
					}

					agCompact3dCell &ncell = compact.cells[x+k*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; ++m)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if(nvox.y == (vox.y+1) && nvox.faces[AG_VOX_FRONT].flags&AG_FACE_ACTIVE)
						{
							setCon(vox, AG_VOX_FRONT, 1, k);
							found = 1;
							
							if(getFaceIsConnected(vox, AG_VOX_TOP)) continue;
							//Deactivate any faces that have been hidden by the new connection
							vox.faces[AG_VOX_TOP].flags &= ~AG_FACE_ACTIVE;
							for(int q = k+1; q < y; ++q)
							{
								agCompact3dCell &qcell = compact.cells[x+q*w];
								for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
								{
									agCompactVoxel & rvox = compact.voxels[r];
									if(rvox.y == vox.y)
									{
										if(getFaceIsConnected(rvox, AG_VOX_TOP)) continue;
										rvox.faces[AG_VOX_TOP].flags &= ~AG_FACE_ACTIVE;
										break;
									}
									else if(rvox.y > (vox.y)) break;
								}
							}
							break;

						}
						else if(nvox.y > (vox.y+1)) break;
					}
					if(found)
					{
						found = 0;
						break;
					}
				}

				
				//Check down (direction 3)
				setCon(vox, AG_VOX_FRONT, 3, AG_NULL_CONNECTION);
				//We can skip searching the cell group containing the current voxel
				for(int k = (y + maxStep); k > y; --k)
				{
					if(k>=l)
					{
						continue;
					}
					agCompact3dCell &ncell = compact.cells[x+k*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; ++m)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if(nvox.y == (vox.y-1) && nvox.faces[AG_VOX_FRONT].flags&AG_FACE_ACTIVE)
						{
							setCon(vox, AG_VOX_FRONT, 3, k);
							found = 1;
							
							if(getFaceIsConnected(nvox, AG_VOX_TOP)) continue;
							//Deactivate any faces that have been hidden by the new connection
							nvox.faces[AG_VOX_TOP].flags &= ~AG_FACE_ACTIVE;
							for(int q = y+1; q < k; ++q)
							{
								agCompact3dCell &qcell = compact.cells[x + q*w];
								for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
								{
									agCompactVoxel & rvox = compact.voxels[r];
									if(rvox.y == (vox.y-1))
									{
										if(getFaceIsConnected(rvox, AG_VOX_TOP)) continue;
										rvox.faces[AG_VOX_TOP].flags &= ~AG_FACE_ACTIVE;
										break;
									}
									else if(rvox.y >= (vox.y)) break;
								}
							}
							break;

						}
						else if(nvox.y >= (vox.y)) break;
					}
					if(found)
					{
						found = 0;
						break;
					}
				}

				//Look to see if there's a voxel directly below us
				if(!found && (i-cell.index)>=0 && (compact.voxels[i-1].y == (vox.y-1)) && compact.voxels[i-1].faces[AG_VOX_FRONT].flags&AG_FACE_ACTIVE)
				{
					setCon(vox, AG_VOX_FRONT, 3, y);
					found = 1;
				}

				for(int k = y-1, endk = y-maxStep; !found && k>=endk; --k)
				{
					if(k < 0)
					{
						break;
					}
					agCompact3dCell &ncell = compact.cells[x+k*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; ++m)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if(nvox.y == (vox.y - 1)  && (nvox.faces[AG_VOX_FRONT].flags&AG_FACE_ACTIVE))
						{
							setCon(vox, AG_VOX_FRONT, 3, k);
							found = 1;
							
							if(getFaceIsConnected(vox, AG_VOX_BOTTOM)) continue;
							//Deactivate any faces that have been hidden by the new connection
							vox.faces[AG_VOX_BOTTOM].flags &= ~AG_FACE_ACTIVE;
							for(int q = k+1; q < y; ++q)
							{
								agCompact3dCell &qcell = compact.cells[x+q*w];
								for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
								{
									agCompactVoxel & rvox = compact.voxels[r];
									if(rvox.y == vox.y)
									{
										if(getFaceIsConnected(rvox, AG_VOX_BOTTOM)) continue;
										rvox.faces[AG_VOX_BOTTOM].flags &= ~AG_FACE_ACTIVE;
										break;
									}
									else if(rvox.y > vox.y)	break;
								}
							}
							break;

						}
						else if(nvox.y > (vox.y-1)) break;
					}
					if(found)
					{
						found = 0;
						break;
					}
				}
			}
		}
	}
	return true;	
}

static bool setupBackConnections(agCompactVolumefield &compact, int maxStep, int w /*width*/, int l /*length*/)
{
	for(int y=0; y<l; ++y)
	{
		for(int x=0; x<w; ++x)
		{
			int found = 0;
			agCompact3dCell &cell = compact.cells[x+y*w];
			for(int i= (int)cell.index, endi = (int)(cell.index+cell.count); i<endi; ++i)
			{
				agCompactVoxel &vox = compact.voxels[i];
				//If the face isn't active, skip it
				if(!(vox.faces[AG_VOX_BACK].flags & AG_FACE_ACTIVE))
				{
					continue;
				}
				
				//Check left (direction 0), looking at cells in the next row up for valid voxels
				setCon(vox, AG_VOX_BACK, 0, AG_NULL_CONNECTION);
				for(int k = (y-maxStep), endk = (y+maxStep); k<=endk; ++k)
				{
					//Check against bounds
					if(k<0 || k >=l || (x-1)<0)
					{
						continue;
					}
					agCompact3dCell &ncell = compact.cells[x-1+k*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; m++)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if(nvox.y == vox.y && (nvox.faces[AG_VOX_BACK].flags&AG_FACE_ACTIVE))
						{
							//Unlike the horizontal faces, we have to store the y coordinate of the neighbour cell
							//not the index....we might be able to squeeze that in too though with a bit of creative
							//bit fiddling...
							setCon(vox, AG_VOX_BACK, 0, k);
							found = 1;
							//Deactivate any faces that are hidden by this new connection
							if(k < y)
							{
								if(getFaceIsConnected(nvox, AG_VOX_RIGHT)) continue;
								nvox.faces[AG_VOX_RIGHT].flags &= ~AG_FACE_ACTIVE;
								for(int q = k+1; q < y; ++q)
								{
									agCompact3dCell &qcell = compact.cells[x-1 + q*w];
									for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
									{
										agCompactVoxel & rvox = compact.voxels[r];
										if(rvox.y == vox.y)
										{
											if(getFaceIsConnected(rvox, AG_VOX_RIGHT)) continue;
											rvox.faces[AG_VOX_RIGHT].flags &= ~AG_FACE_ACTIVE;
											break;
										}
										else if(rvox.y > (vox.y)) break;
									}
								}
							}
							else if(y < k)
							{
								if(getFaceIsConnected(vox, AG_VOX_LEFT)) continue;
								vox.faces[AG_VOX_LEFT].flags &= ~AG_FACE_ACTIVE;
								for(int q = y+1; q < k; ++q)
								{
									agCompact3dCell &qcell = compact.cells[x + q*w];
									for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
									{
										agCompactVoxel &rvox = compact.voxels[r];
										if(rvox.y == vox.y)
										{
											if(getFaceIsConnected(rvox, AG_VOX_LEFT)) continue;
											rvox.faces[AG_VOX_LEFT].flags &= ~AG_FACE_ACTIVE;
										}
										else if(rvox.y > (vox.y)) break;
									}
								}
							}
							break;
						}
						else if(nvox.y > vox.y)
						{
							break; //no voxels in this cell have the required height
						}
					}
					if(found)
					{
						found = 0;
						break;
					}
				}
					
				//Check right (direction 2), looking at cells in the next row up for valid voxels
				setCon(vox, AG_VOX_BACK, 2, AG_NULL_CONNECTION);
				for(int k = (y-maxStep), endk = (y+maxStep); k<=endk; ++k)
				{
					//Check against bounds
					if(k<0 || k >=l || (x+1)>=w)
					{
						continue;
					}
					agCompact3dCell &ncell = compact.cells[x+1+k*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; m++)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if(nvox.y == vox.y && (nvox.faces[AG_VOX_BACK].flags&AG_FACE_ACTIVE))
						{
							//Unlike the horizontal faces, we have to store the y coordinate of the neighbour cell
							//not the index....we might be able to squeeze that in too though with a bit of creative
							//bit fiddling...
							setCon(vox, AG_VOX_BACK, 2, k);
							found = 1;
							//Deactivate any faces that are hidden by this new connection
							if(k < y)
							{
								if(getFaceIsConnected(nvox, AG_VOX_LEFT)) continue;
								nvox.faces[AG_VOX_LEFT].flags &= ~AG_FACE_ACTIVE;
								for(int q = k+1; q < y; ++q)
								{
									agCompact3dCell &qcell = compact.cells[x+1 +q*w];
									for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
									{
										agCompactVoxel & rvox = compact.voxels[r];
										if(rvox.y == vox.y)
										{
											if(getFaceIsConnected(rvox, AG_VOX_LEFT)) continue;
											rvox.faces[AG_VOX_LEFT].flags &= ~AG_FACE_ACTIVE;
											break;
										}
										else if(rvox.y > (vox.y)) break;
									}
								} 
							}
							else if(y < k)
							{
								if(getFaceIsConnected(vox, AG_VOX_RIGHT)) continue;
								vox.faces[AG_VOX_RIGHT].flags &= ~AG_FACE_ACTIVE;
								for(int q = y+1; q < k; ++q)
								{
									agCompact3dCell &qcell = compact.cells[x+q*w];
									for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
									{
										agCompactVoxel &rvox = compact.voxels[r];
										if(rvox.y == vox.y)
										{
											if(getFaceIsConnected(rvox, AG_VOX_RIGHT)) continue;
											rvox.faces[AG_VOX_RIGHT].flags &= ~AG_FACE_ACTIVE;
										}
										else if(rvox.y > (vox.y)) break;
			
									}
								}
							}
							break;
						}
						else if(nvox.y > vox.y)
						{
							break; //no voxels in this cell have the required y coordinate
						}
					}
					if(found)
					{
						found = 0;
						break;
					}
				}

				//Check up (direction 1)
				setCon(vox, AG_VOX_BACK, 1, AG_NULL_CONNECTION);
				//We can skip searching the cell group containing the current voxel
				for(int k = (y - maxStep); k < y; ++k)
				{
					if(k<0)
					{
						continue;
					}
					agCompact3dCell &ncell = compact.cells[x+k*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; ++m)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if(nvox.y == (vox.y+1) && nvox.faces[AG_VOX_BACK].flags&AG_FACE_ACTIVE)
						{
							setCon(vox, AG_VOX_BACK, 1, k);
							found = 1;
							
							if(getFaceIsConnected(nvox, AG_VOX_BOTTOM)) continue;
							//Deactivate any faces that have been hidden by the new connection
							nvox.faces[AG_VOX_BOTTOM].flags &= ~AG_FACE_ACTIVE;
							for(int q = k+1; q < y; ++q)
							{
								agCompact3dCell &qcell = compact.cells[x+q*w];
								for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
								{
									agCompactVoxel & rvox = compact.voxels[r];
									if(rvox.y == (vox.y+1))
									{
										if(getFaceIsConnected(rvox, AG_VOX_BOTTOM)) continue;
										rvox.faces[AG_VOX_BOTTOM].flags &= ~AG_FACE_ACTIVE;
										break;
									}
									else if(rvox.y > (vox.y+1)) break;
								}
							}
							break;

						}
						else if(nvox.y > (vox.y +1)) break;
					}
					if(found)
					{
						found = 0;
						break;
					}
				}

				//Look to see if there's a voxel directly above us
				if(!found && (i-cell.index)<(u32)(cell.count-1) && (compact.voxels[i+1].y == (vox.y+1)) && compact.voxels[i+1].faces[AG_VOX_BACK].flags&AG_FACE_ACTIVE)
				{
					setCon(vox, AG_VOX_BACK, 1, y);
					found = 1;
				}

				for(int k = y+1, endk = y+maxStep; !found && k<=endk; ++k)
				{
					if(k>=l)
					{
						break;
					}

					agCompact3dCell &ncell = compact.cells[x+k*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; ++m)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if(nvox.y == (vox.y+1) && nvox.faces[AG_VOX_BACK].flags&AG_FACE_ACTIVE)
						{
							setCon(vox, AG_VOX_BACK, 1, k);
							found = 1;
							
							if(getFaceIsConnected(vox, AG_VOX_TOP)) continue;
							//Deactivate any faces that have been hidden by the new connection
							vox.faces[AG_VOX_TOP].flags &= ~AG_FACE_ACTIVE;
							for(int q = y+1; q < k; ++q)
							{
								agCompact3dCell &qcell = compact.cells[x+q*w];
								for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
								{
									agCompactVoxel & rvox = compact.voxels[r];
									if(rvox.y == vox.y)
									{
										if(getFaceIsConnected(rvox, AG_VOX_TOP)) continue;
										rvox.faces[AG_VOX_TOP].flags &= ~AG_FACE_ACTIVE;
										break;
									}
									else if(rvox.y > (vox.y)) break;
								}
							}
							break;

						}
						else if(nvox.y > (vox.y + 1)) break;
					}
					if(found)
					{
						found = 0;
						break;
					}
				}

				
				//Check down (direction 3)
				setCon(vox, AG_VOX_BACK, 3, AG_NULL_CONNECTION);
				//We can skip searching the cell group containing the current voxel
				for(int k = (y - maxStep); k < y; ++k)
				{
					if(k<0)
					{
						continue;
					}
					agCompact3dCell &ncell = compact.cells[x+k*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; ++m)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if(nvox.y == (vox.y - 1) && nvox.faces[AG_VOX_BACK].flags&AG_FACE_ACTIVE)
						{
							setCon(vox, AG_VOX_BACK, 3, k);
							found = 1;
							
							if(getFaceIsConnected(nvox, AG_VOX_TOP)) continue;
							//Deactivate any faces that have been hidden by the new connection
							nvox.faces[AG_VOX_TOP].flags &= ~AG_FACE_ACTIVE;
							for(int q = k+1; q < y; ++q)
							{
								agCompact3dCell &qcell = compact.cells[x + q*w];
								for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
								{
									agCompactVoxel & rvox = compact.voxels[r];
									if(rvox.y == (vox.y-1))
									{
										if(getFaceIsConnected(rvox, AG_VOX_TOP)) continue;
										rvox.faces[AG_VOX_TOP].flags &= ~AG_FACE_ACTIVE;
										break;
									}
									else if(rvox.y >= (vox.y)) break;
								}
							}
							break;

						}
						else if(nvox.y >= (vox.y)) break;
					}
					if(found)
					{
						found = 0;
						break;
					}
				}

				//Look to see if there's a voxel directly below us
				if(!found && (i-cell.index)>=0 && (compact.voxels[i-1].y == (vox.y-1)) && compact.voxels[i-1].faces[AG_VOX_BACK].flags&AG_FACE_ACTIVE)
				{
					setCon(vox, AG_VOX_BACK, 3, y);
					found = 1;
				}

				for(int k = y+1, endk = y+maxStep; !found && k<=endk; ++k)
				{
					if(k >= l)
					{
						break;
					}
					agCompact3dCell &ncell = compact.cells[x+k*w];
					for(int m = ncell.index, endm = (ncell.index+ncell.count); m<endm; ++m)
					{
						agCompactVoxel &nvox = compact.voxels[m];
						if(nvox.y == (vox.y-1)  && (nvox.faces[AG_VOX_BACK].flags&AG_FACE_ACTIVE))
						{
							setCon(vox, AG_VOX_BACK, 3, k);
							found = 1;
							
							if(getFaceIsConnected(vox, AG_VOX_BOTTOM)) continue;
							//Deactivate any faces that have been hidden by the new connection
							vox.faces[AG_VOX_BOTTOM].flags &= ~AG_FACE_ACTIVE;
							for(int q = y+1; q < k; ++q)
							{
								agCompact3dCell &qcell = compact.cells[x+q*w];
								for(int r = qcell.index, endr = (qcell.index+qcell.count); r<endr; ++r)
								{
									agCompactVoxel & rvox = compact.voxels[r];
									if(rvox.y == vox.y)
									{
										if(getFaceIsConnected(rvox, AG_VOX_BOTTOM)) continue;
										rvox.faces[AG_VOX_BOTTOM].flags &= ~AG_FACE_ACTIVE;
										break;
									}
									else if(rvox.y > vox.y)	break;
								}
							}
							break;

						}
						else if(nvox.y > (vox.y - 1)) break;
					}
					if(found)
					{
						found = 0;
						break;
					}
				}
			}
		}
	}
	return true;	
}

bool setupNeighbourConnections(agCompactVolumefield &compact)
{
	int w = compact.width;
	int l = compact.length;
	static const int maxStep = 2;

	//We have to process the voxels one face at a time so that the algorithm has a chance to deactivate faces
	//that become 'hidden' as connections are set up. [TODO] On second thoughts it might be possible to roll them into
	//pairs of opposite faces though.
	
	//NB: Rather confusingly there's two different y-axes at play here depending on whether we're looking at a cell or a voxel
	//Cells are arranged on a 2d grid so in their case y is the length along that 2d grid (so is horizontal). Voxels are arranged
	//in vertical columns so for them y (e.g. vox.y) refers to the vertical height position. At some point I may re name things to
	//be less confusing but for the time being BE WARNED.
	setupTopConnections(compact, maxStep, w, l);	

	//Next do the bottom faces (ceiling)
	setupBottomConnections(compact, maxStep, w, l);

	//Next we'll do the sides, starting with the left
	setupLeftConnections(compact, maxStep, w, l);

	//And the right
	setupRightConnections(compact, maxStep, w, l);

	//And front
	setupFrontConnections(compact, maxStep, w, l);

	//And back
	setupBackConnections(compact, maxStep, w, l);

	return true;
}

bool agBuildCompactVolumeField(agHeightField &field, agCompactVolumefield & compact)
{
	agTimeVal startTime = agGetPerformanceTimer();

	int w = field.width;
	int l = field.length;
	int voxelCount = getVoxelCount(field);
	
	//Fill in basic gubbins
	compact.width = w;
	compact.length = l;
	compact.voxelCount = voxelCount;
	compact.maxRegions = 0;
	vcopy(compact.bmin, field.boundMin);
	vcopy(compact.bmax, field.boundMax);
	compact.cellSize = field.cellSize;
	compact.cellHeight = field.cellHeight;
	compact.cells = new agCompact3dCell[w*l];
	if(!compact.cells)
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "agBuildCompactVolumeField: out of memory 'compact.cells (%d)'", w*l);
		}
		
		return false;
	}
	memset (compact.cells, 0, sizeof(agCompact3dCell)*w*l);
	
	compact.voxels = new agCompactVoxel[voxelCount];
	if(!compact.voxels)
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "agBuildCompactVolumeField: out of memory 'compact.voxels (%d)'", voxelCount);
		}
		return false;
	}
	memset(compact.voxels, 0, sizeof(agCompactVoxel)*voxelCount);

	// Fill in cells and voxels
	int index = 0;
	for(int y = 0; y < l; ++y)
	{
		for (int x = 0; x < w; ++x)
		{
			const agSpan *s = field.spans[x + y*w];
			//If there are no spans in this cell then just leave the data to index=0, count=0
			if(!s) continue;
			agCompact3dCell &cell = compact.cells[x + y*w];
			cell.index = index;
			cell.count = 0;
			while (s)
			{
				//If we have flags to exclude spans from the volumefield, the logic goes here
				compact.voxels[index].y = s->minY;
				index++;
				cell.count++;
				//

				s = s->next;
			}
		}
	}

	//Mark up active faces (faces exposed to the world)
	//Currently this just checks the face isn't blocked by another voxel but could be expanded to include 
	//graphics card based checks that the face isn't looking out mostly onto backwards facing polygons
	markActiveVoxelFaces(compact);

	//Set up connections to neighbouring faces
	setupNeighbourConnections(compact);

	agTimeVal endTime = agGetPerformanceTimer();

	if(agGetBuildTimes())
	{
		agGetBuildTimes()->buildCompact += agGetDeltaTimeUsec(startTime, endTime);
	}

	return true;
}

bool agBuildCompactHeightField(int walkableHeight, int walkableClimb, u8 flags, agHeightField &field, agCompactHeightField & compact)
{
	agTimeVal startTime = agGetPerformanceTimer();

	int w = field.width;
	int h = field.length;
	int spanCount = getSpanCount(flags, field);

	//Fill in header
	compact.width = w;
	compact.length = h;
	compact.spanCount = spanCount;
	compact.walkableHeight = walkableHeight;
	compact.walkableClimb = walkableClimb;
	compact.maxRegions = 0;
	vcopy(compact.boundMin, field.boundMin);
	vcopy(compact.boundMax, field.boundMax);
	compact.boundMax[1] += walkableHeight*field.cellHeight;
	compact.cellSize = field.cellSize;
	compact.cellHeight = field.cellHeight;
	compact.cells = rage_new agCompactCell[w*h];
	if(!compact.cells)
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "agBuildCompactHeightField: Out of memory 'compact.spans' (%d)", spanCount);
		}
		return false;
	} 
	memset(compact.cells, 0, sizeof(agCompactCell)*w*h);

	compact.spans = rage_new agCompactSpan[spanCount];
	if(!compact.spans)
	{
		if(agGetLog())
		{
			agGetLog()->log(AG_LOG_ERROR, "agBuildCompactHeightField: Out of memory 'compact.spans' (%d)", spanCount);
		}
		return false;
	}
	memset(compact.spans, 0, sizeof(agCompactSpan)*spanCount);

	int MAX_HEIGHT = 0xffff;

	//Fill in cells and spans
	int idx = 0;
	for (int y = 0; y<h; ++y)
	{
		for(int x=0; x< w; ++x)
		{
			const agSpan * span = field.spans[x + y*w];
			// If there are no spans at thes cell, just leave the data to index=0, count=0
			if(!span) continue;
			agCompactCell &cell = compact.cells[x+y*w];
			cell.index = idx;
			cell.count = 0;
			while(span)
			{
				if(span->flags == flags)
				{
					int bot = (int)span->maxY;
					int top = (int)span->next?(int)span->next->minY : MAX_HEIGHT;
					compact.spans[idx].y = (u16)Clamp(bot, 0, 0xffff);
					compact.spans[idx].height = (u8)Clamp(Abs(top - bot), 0, 0xff);
					Assert(span->next? (top-bot) < MAX_HEIGHT:1);
					idx++;
					cell.count++;
				}
				span = span->next;
			}
		}
	}

	//Find neighbour connections
	for(int y = 0; y < h; ++y)
	{
		for (int x=0; x<w; ++x)
		{
			const agCompactCell &cell = compact.cells[x+y*w];
			for(int i = (int)cell.index, endi = (int)(cell.index+cell.count); i < endi; ++i)
			{
				agCompactSpan &span = compact.spans[i];
				for(int dir=0; dir<4; ++dir)
				{
					setCon(span, dir, 0xf);
					int nx = x + agGetDirOffsetX(dir);
					int ny = y + agGetDirOffsetY(dir);
					//First check that the neighbour cell is in bounds
					if(nx < 0 || ny <0 || nx >= w || ny >= h)
					{
						continue;
					}

					//Iterate over all neighbour spans and check if any of them are 
					//accesible from the current span
					agCompactCell &ncell = compact.cells[nx+ny*w];
					for (int k = (int)ncell.index, endk = (int)(ncell.index+ncell.count); k<endk; ++k)
					{
						agCompactSpan &nspan = compact.spans[k];
						
						int bot = Max(span.y, nspan.y);
						int top = Min(span.y+span.height, nspan.y+nspan.height);

						//Check that the gap between the spans falls within our tolerence
						if((top-bot) >= walkableHeight && Abs((int)nspan.y - (int)span.y) <= walkableClimb)
						{
							//Set connection
							setCon(span, dir, k - (int)ncell.index);
							break;
						}
					}
				}
			}
		}
	}

	agTimeVal endTime = agGetPerformanceTimer();

	if(agGetBuildTimes())
	{
		agGetBuildTimes()->buildCompact += agGetDeltaTimeUsec(startTime, endTime);
	}

	return true;

}

























