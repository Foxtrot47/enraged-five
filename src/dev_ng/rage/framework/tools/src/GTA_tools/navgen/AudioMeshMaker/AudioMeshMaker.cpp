#include "AudioMeshMaker.h"
#include "ai/navmesh/navmeshextents.h"
#include "phcore/materialmgrimpl.h"
#include "phcore/surface.h"
#include "system/FileMgr.h"
#include "system/Param.h"
#include "GameDataFolder.h"
#include "CAudioMeshMaker.h"
#include <time.h>
#include <direct.h>

#define USE_STOCK_ALLOCATOR	// include before "system/main.h"
#include "system/main.h"

char logFileName[256];

// Which mode this has been invoked as
int g_Mode = -1;
// The name of a command file which will be used
char * g_CommandFileName = NULL;
// Create audio mesh
bool g_AudioMesh = false;
// Audio mesh params file
char * g_AudioMeshParamsFile = NULL;

vector<TAudioMeshParams*> g_SourceAudioMeshes;

// An input path to be prepended to all input file-names
char * g_InputPath = NULL;
// An output path to be prepended to all output file-names
char * g_OutputPath = NULL;
// This external is defined in GameDataFolder.h, it is set via the -gamedatafolder cmdline parameter
// and is required so that the physics material manager can load the game's material types in
char * g_pGameFolder = _strdup("X:\\gta\\build");

char g_LogFilePrefix[64];

// This file is used to log compilation info to
FILE * g_pLogFile = NULL;
// Not sure why this was needed
char *XEX_TITLE_ID = NULL;

TAudioMeshParams::~TAudioMeshParams()
{
	if(m_pFileName)
		free(m_pFileName);
}

void
CAudioMeshMakerApp::OutputText(char * pText)
{
	printf(pText);

	// Print out to g_pLogFile - but not the g_pBlankLine's..
	if(g_pLogFile)
	{
		if(*pText != '\r')
		{
			fprintf(g_pLogFile, pText);
		}
		else
		{
			fprintf(g_pLogFile, "\n");
		}
		fflush(g_pLogFile);
	}

	//OutputDebugString(pText);
}

void LogArgs()
{
	if(g_pLogFile)
	{
		int argc = sysParam::GetArgCount();
		char ** argv = sysParam::GetArgArray();

		fprintf(g_pLogFile, "--------------------------------------------------------------------\n");
		fprintf(g_pLogFile, "NumArgs : %i\n", argc);

		for(int a=0; a<argc; a++)
		{
			fprintf(g_pLogFile, "Args %i: %s\n", a, argv[a]);
		}

		fprintf(g_pLogFile, "--------------------------------------------------------------------\n");
	}
}

int
Main()
{
	int argc = sysParam::GetArgCount();
	char ** argv = sysParam::GetArgArray();

#if __DEV && !__OPTIMIZED
	// Enable the CRT to debug memory leak info at exit
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif

	// We have to make sure that there is a valid working directory.  When NavMeshMaker is run, it
	// should ideally have a full working directory specified in the args parsed below.
	_chdir(g_pGameFolder);

	// Parse the parameters passed to the program
	if(!CAudioMeshMakerApp::ParseParams(argc, argv))
	{
		return -1;
	}

	//Uncomment this to use the audiomesh gui
	//g_AudioMesh = (g_Mode != GenerateAudMeshes) && (g_Mode != StitchAudMeshes);

	if(g_AudioMesh)
	{
		CAudioMeshMaker audioMesh;
		return audioMesh.Main();
	}

	if(!g_OutputPath)
	{
		g_OutputPath = _strdup(g_InputPath);
	}

#if __DEV
	RAGE_LOG_DISABLE = 1;
#endif

	CFileMgr::Initialise();

	CPathServerExtents::m_iNumSectorsPerNavMesh = 0;
	CAudioMeshMakerApp::ReadDatFile();

	if(CPathServerExtents::m_iNumSectorsPerNavMesh==0)
	{
		printf("g_iNumSectorsPerNavMesh==0\n");
		return -1;
	}

	CAudioMeshMakerApp::OutputText("\n");

	CAudioMeshMakerApp::InitMeshFilenames();

	if(g_CommandFileName)
	{
		if(!CAudioMeshMakerApp::ReadCmdFile())
		{
			CAudioMeshMakerApp::ClearData();
			return -1;
		}
	}

	if(g_Mode == GenerateAudMeshes)
	{
		sprintf(logFileName, "%s/%s%s", g_InputPath, g_LogFilePrefix, "audiomesh_log.txt");
		g_pLogFile = fopen(logFileName, "wt");
		CAudioMeshMakerApp::OutputText(logFileName);
		LogArgs();

		if(CAudioMeshMakerApp::GenerateAudioMeshes())
		{
			CAudioMeshMakerApp::ClearData();
			return -1;
		}
	}

	if(g_Mode == StitchAudMeshes)
	{
		sprintf(logFileName, "%s/%s%s", g_InputPath, g_LogFilePrefix, "audiostitch_log.txt");
		g_pLogFile = fopen(logFileName, "wt");
		CAudioMeshMakerApp::OutputText(logFileName);
		LogArgs();

		if(CAudioMeshMakerApp::StitchAudioMeshes())
		{
			CAudioMeshMakerApp::ClearData();
			return -1;
		}
	}

	CAudioMeshMakerApp::ClearData();

	printf("Done!");
	return 1;
}



void
CAudioMeshMakerApp::ClearData(void)
{
	uint32 i;

	g_Mode = -1;

	free(g_CommandFileName);
	g_CommandFileName = NULL;

	free(g_InputPath);
	g_InputPath = NULL;

	free(g_OutputPath);
	g_OutputPath = NULL;

	for(i=0; i<g_SourceAudioMeshes.size(); i++)
	{
		TAudioMeshParams * pAudioParams = g_SourceAudioMeshes[i];
		delete pAudioParams;
	}
	g_SourceAudioMeshes.clear();

	if(g_pLogFile)
	{
		fclose(g_pLogFile);
		g_pLogFile = NULL;
	}
}

//**************************************************************
// pGameDataPath will be something like "x:/gta5/build/dev"

void CAudioMeshMakerApp::ReadDatFile()
{
	char pathForNavDatFile[512];
	sprintf(pathForNavDatFile, "%s\\common\\data\\nav.dat", g_pGameFolder);

	FILE * pFile = fopen(pathForNavDatFile, "rt");
	if(!pFile)
	{
		return;
	}

	char string[256];

	while(fgets(string, 256, pFile))
	{
		if(string[0] == '#')
			continue;

		int iNumMatched = 0;
		int iValue;

		// SECTORS_PER_NAVMESH defines the resolution which this game's navmesh system works at.
		iNumMatched = sscanf(string, "SECTORS_PER_NAVMESH = %i", &iValue);
		if(iNumMatched)
		{
			CPathServerExtents::m_iNumSectorsPerNavMesh = iValue;
			continue;
		}
	}

	fclose(pFile);
}

bool
CAudioMeshMakerApp::ParseParams(int argc, char *argv[])
{
	// First argument is always the .exe itself
	int argIndex = 1;

	// Read in the arguments
	while(argIndex < argc)
	{
		char * pArg = argv[argIndex];
		if(!pArg)
		{
			break;
		}

		// Specifying -? or /? or /h or -h will show the usage
		else if(stricmp(pArg, "-?")==0 || stricmp(pArg, "/?")==0 || stricmp(pArg, "-h")==0 || stricmp(pArg, "/?")==0)
		{
			ShowUsage();
			return false;
		}
		// If specified, an input path is expected to follow
		else if(stricmp(pArg, "-inputpath")==0)
		{
			// Read the path
			argIndex++;
			pArg = argv[argIndex];
			if(!pArg || !*pArg)
			{
				OutputText("Error : -inputpath was used, but no path was found.\n");
				return false;
			}
			free(g_InputPath);
			g_InputPath = _strdup(pArg);
		}
		// If specified, an output path is expected to follow
		else if(stricmp(pArg, "-outputpath")==0)
		{
			// Read the path
			argIndex++;
			pArg = argv[argIndex];
			if(!pArg || !*pArg)
			{
				OutputText("Error : -outputpath was used, but no path was found.\n");
				return false;
			}
			free(g_OutputPath);
			g_OutputPath = _strdup(pArg);
		}
		// If specified, the path to the game's data folder is expected to follow
		else if(stricmp(pArg, "-gamepath")==0)
		{
			// Read the path
			argIndex++;
			pArg = argv[argIndex];
			if(!pArg || !*pArg)
			{
				OutputText("Error : -gamepath was used, but no path was found.\n");
				return false;
			}
			free(g_pGameFolder);
			g_pGameFolder = _strdup(pArg);

			// We HAVE to set the current working directory to equal this path.  This is because
			// the game now uses virtual relative devices to look for assert & this assumes that
			// they are all relative to the current working directory.
			_chdir(g_pGameFolder);
		}
		// If specified, then a text file is being used to detail all the files to process
		else if(stricmp(pArg, "-cmdfile")==0)
		{
			// Read the command filename
			argIndex++;
			pArg = argv[argIndex];
			if(!pArg || !*pArg)
			{
				OutputText("Error : -cmdfile was used, but no command file was specified.\n");
				return false;
			}
			g_CommandFileName = _strdup(pArg);
		}
		else if(stricmp(pArg, "-audiomesh")==0)
		{
			g_Mode = GenerateAudMeshes;
		}
		else if(stricmp(pArg, "-audiostitch")==0)
		{
			g_Mode = StitchAudMeshes;
		}
		else if(stricmp(pArg, "-audioparamfile")==0)
		{
			// Read the path to the param file
			argIndex++;
			pArg = argv[argIndex];
			if(!pArg || !*pArg)
			{
				OutputText("Error : -audioparamfile was used, but no path was found.\n");
				return false;
			}
			free(g_AudioMeshParamsFile);
			g_AudioMeshParamsFile = _strdup(pArg);
		}

		argIndex++;
	}

	return true;
}

TAudioMeshParams * CAudioMeshMakerApp::InitAudioMeshParams(int iSectorX, int iSectorY, char * pAudioMeshName)
{
	TAudioMeshParams * pMeshParams = new TAudioMeshParams();

	pMeshParams->m_iStartSectorX = iSectorX;
	pMeshParams->m_iStartSectorY = iSectorY;

	// Depending upon what MODE we're currently in, replace/append file extension with appropriate type
	char * pExt = "tri";
	char filename[256];

	if(g_InputPath)
	{
		if(pAudioMeshName)
		{
			sprintf_s(filename, "%s/%s.%s", g_InputPath, pAudioMeshName, pExt);
		}
		else
		{
			sprintf_s(filename, "%s/navmesh[%i][%i].%s", g_InputPath, pMeshParams->m_iStartSectorX, pMeshParams->m_iStartSectorY, pExt);
		}
	}
	else
	{
		if(pAudioMeshName)
		{
			sprintf_s(filename, "%s.%s", pAudioMeshName, pExt);
		}
		else
		{
			sprintf_s(filename, "navmesh[%i][%i].%s", pMeshParams->m_iStartSectorX, pMeshParams->m_iStartSectorY, pExt);
		}
	}

	pMeshParams->m_pFileName = _strdup(filename);

	return pMeshParams;
}

void
CAudioMeshMakerApp::ShowUsage(void)
{
	OutputText("AudioMeshMaker\n");
	OutputText("A command-line tool for creating audio meshes.\n");
	OutputText("(c) Rockstar North 2010, author Colin Walder\n");
	OutputText("\n");
}


void CAudioMeshMakerApp::InitMeshFilenames()
{
	int iNumX = CPathServerExtents::m_iNumNavMeshesInX;
	int iNumY = CPathServerExtents::m_iNumNavMeshesInY;

	for(int y=0; y<iNumY; y++)
	{
		for(int x=0; x<iNumX; x++)
		{
			TAudioMeshParams * pMeshParams = InitAudioMeshParams(x*CPathServerExtents::m_iNumSectorsPerNavMesh, y*CPathServerExtents::m_iNumSectorsPerNavMesh);
			g_SourceAudioMeshes.push_back(pMeshParams);
		}
	}
}

bool CAudioMeshMakerApp::ReadCmdFile()
{
	char textBuf[256];
	char fileNameBuf[256];

	FILE * filePtr = fopen(g_CommandFileName, "rt");

	if(!filePtr)
	{
		sprintf(textBuf, "Note : couldn't open command file \"%s\".\n", g_CommandFileName);
		OutputText(textBuf);
		return true;
	}

	do
	{
		char * pRet = fgets(fileNameBuf, 256, filePtr);
		if(!pRet)
			break;

		if(fileNameBuf[0]=='#' || fileNameBuf[0]=='\t' || fileNameBuf[0]==10)
			continue;

		// Find which navmesh this is
		int iX = -1, iY = -1;
		char * pIndexStart = strstr(fileNameBuf, "[");
		if(!pIndexStart)
			continue;

		int iNumScanned = sscanf(pIndexStart, "[%i][%i]", &iX, &iY);

		if(iNumScanned==2)
		{
			uint32 n;
			for(n=0; n<g_SourceAudioMeshes.size(); n++)
			{
				TAudioMeshParams * pAudioParams = g_SourceAudioMeshes[n];
				if(pAudioParams->m_iStartSectorX==iX && pAudioParams->m_iStartSectorY==iY)
					break;
			}
			// If we don't already have this entry, then create a new one.
			if(n==g_SourceAudioMeshes.size())
			{
				TAudioMeshParams * pMeshParams = InitAudioMeshParams(iX, iY);
				g_SourceAudioMeshes.push_back(pMeshParams);
			}
		}

	} while(1);

	fclose(filePtr);
	return true;

}

bool CAudioMeshMakerApp::StitchAudioMeshes(void)
{
	if(!g_InputPath)
	{
		return false;
	}

	//int count = 0;

	for(u32 m=0; m<g_SourceAudioMeshes.size(); m++)
	{
		TAudioMeshParams * pAudioParams = g_SourceAudioMeshes[m];

		//TODO: delete old files

		CAudioMeshMaker audioMeshMaker;	

		char fileName[256] = {0};
		formatf(fileName, "navmesh[%i][%i].tri", pAudioParams->m_iStartSectorX, pAudioParams->m_iStartSectorY);

		audioMeshMaker.Main(fileName, g_AudioMeshParamsFile, "Audio mesh stitch", g_InputPath);
	}

	return true;
}



bool CAudioMeshMakerApp::GenerateAudioMeshes(void)
{
	if(!g_InputPath)
	{
		return false;
	}

	//int count = 0;

	for(u32 m=0; m<g_SourceAudioMeshes.size(); m++)
	{
		TAudioMeshParams * pAudioParams = g_SourceAudioMeshes[m];

		//TODO: delete old files

		CAudioMeshMaker audioMeshMaker;	

		char fileName[256] = {0};
		formatf(fileName, "navmesh[%i][%i].tri", pAudioParams->m_iStartSectorX, pAudioParams->m_iStartSectorY);

		audioMeshMaker.Main(fileName, g_AudioMeshParamsFile, "Audio mesh builder", g_InputPath); 
	}

	return true;
}

