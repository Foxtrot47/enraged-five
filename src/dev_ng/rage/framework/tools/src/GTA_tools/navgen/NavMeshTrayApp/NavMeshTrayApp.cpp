#include "NavMeshTrayApp.h"
#include "resource.h"
#include <winsock2.h>
#include <Ws2tcpip.h>
#include <stdlib.h>

#pragma warning(disable: 4312)	// Supress warning

TAppData AppData;

void TSettings::SetDefault()
{
	memset(this, 0, sizeof(TSettings));

	iMinNavmeshesImgSizeForSuccessfulBuildKb = 256;
	
	bDeleteTempFilesBeforeBuild = TRUE;

	bGetLatestToolsFirst = TRUE;
	bGetLatestDataFirst = TRUE;
	bGetLatestExeFirst = TRUE;
	bCheckNavmeshesIntoPerforce = TRUE;
	bDoCompile = TRUE;
	bDoExport = TRUE;

	if(DEFAULT_PROJECT_ROOT)
		strcpy_s(projectRoot, DEFAULT_PROJECT_ROOT);
	if(DEFAULT_PATH_FOR_EXPORT)
		strcpy_s(pathForExport, DEFAULT_PATH_FOR_EXPORT);
	if(DEFAULT_NAVMESHES_IMG_PATH)
		strcpy_s(navmeshesImgPath, DEFAULT_NAVMESHES_IMG_PATH);
	if(DEFAULT_INI_FILENAME)
		strcpy_s(iniFileName, DEFAULT_INI_FILENAME);

	strcpy_s(exporterExeFile, DEFAULT_EXPORTER_EXE_FILE);

	strcpy_s(pathForBatchFiles, DEFAULT_PATH_FOR_BATCH_FILES);
	strcpy_s(batchFileForDelTempFiles, DEFAULT_DELTEMPFILES_FILE);
	strcpy_s(batchFileForGetLatestTools, DEFAULT_GETLATESTTOOLS_FILE);
	strcpy_s(batchFileForGetLatestData, DEFAULT_GETLATESTDATA_FILE);
	strcpy_s(batchFileForGetLatestExe, DEFAULT_GETLATESTEXE_FILE);
	strcpy_s(batchFileForExportGeometry, DEFAULT_EXPORTGEOMETRY_FILE);
	strcpy_s(batchFileForExportRegion, DEFAULT_EXPORTREGION_FILE);
	strcpy_s(batchFileForExportContinueFrom, DEFAULT_EXPORTCONTINUEFROM_FILE);
	strcpy_s(batchFileForExportDLC, DEFAULT_EXPORTDLC_FILE);
	strcpy_s(batchFileForJoinDLC, DEFAULT_JOINDLC_FILE);
	strcpy_s(batchFileForScheduledCompile, DEFAULT_BUILDWHENEXPORTCOMPLETE_FILE);
	strcpy_s(batchFileForCheckOutNavmeshes, DEFAULT_CHECKOUTNAVMESHES_FILE);
	strcpy_s(batchFileForCheckInNavmeshes, DEFAULT_CHECKIN_NAVMESHES_FILE);
	strcpy_s(batchFileForCheckInAudmeshes, DEFAULT_CHECKIN_AUDMESHES_FILE);
	strcpy_s(batchFileForCheckInNavnodes, DEFAULT_CHECKIN_NAVNODES_FILE);
	strcpy_s(batchFileForCheckInMapping, DEFAULT_CHECKIN_MAPPING_FILE);

	iBuildHour24 = DEFAULT_HOUR_FOR_BUILD;
	iBuildMinute = DEFAULT_MINUTE_FOR_BUILD;

	bWholeMapBuild = true;
	bContinueFrom = false;
	bRegion = false;
	bDLC = false;

	iRegionMinX = iRegionMinY = 0;
	iRegionMaxX = iRegionMaxY = 0;
}


bool ScanLine(const char * pToken, char * pBuffer, char * pDestString, int iMaxNumChars)
{
	char token[256];
	sprintf_s(token, "%s = ", pToken);
	char * pos = strstr(pBuffer, token);
	if(!pos)
		return false;
	pos += strlen(token);

	char* nextLine = strpbrk(pos, "\n");
	_ASSERT(nextLine != NULL);
	size_t charCount = nextLine - pos;
	strncpy_s(pDestString, iMaxNumChars, pos, charCount);
	
	_ASSERT(pDestString != NULL);
	return (pDestString != NULL);
}


BOOL LoadSettings_New(TSettings & settings)
{
	FILE * pFile = NULL;
	fopen_s(&pFile, settings.iniFileName, "rt");
	if(!pFile)
		return FALSE;

	int iVersion;
	fscanf_s(pFile, "version = %i\n", &iVersion);
	if(iVersion != SETTINGS_FILE_VERSION)
	{
		_ASSERT(iVersion == SETTINGS_FILE_VERSION);
		fclose(pFile);
		return FALSE;
	}

	// Read into memory
	fseek(pFile, 0, SEEK_END);
	int iSize = ftell(pFile);
	char * buffer = new char[iSize];
	fseek(pFile, 0, SEEK_SET);
	fread_s(buffer, iSize, 1, iSize, pFile);
	fclose(pFile);

	// Set defaults
	ZeroMemory(&settings, sizeof(settings));
	settings.SetDefault();

	char data[MAX_BATCHFILENAME];

	if( ScanLine("projectRoot", buffer, data, MAX_BATCHFILENAME) )
		strcpy_s( settings.projectRoot, MAX_BATCHFILENAME, data );
	if( ScanLine("pathForExport", buffer, data, MAX_BATCHFILENAME) )
		strcpy_s( settings.pathForExport, MAX_BATCHFILENAME, data );
	if( ScanLine("navmeshesImgPath", buffer, data, MAX_BATCHFILENAME) )
		strcpy_s( settings.navmeshesImgPath, MAX_BATCHFILENAME, data );
	if( ScanLine("levelCommonPath", buffer, data, MAX_BATCHFILENAME) )
		strcpy_s( settings.levelCommonPath, MAX_BATCHFILENAME, data );
	if( ScanLine("iniFileName", buffer, data, MAX_BATCHFILENAME) )
		strcpy_s( settings.iniFileName, MAX_BATCHFILENAME, data );
	if( ScanLine("navmeshesListFile", buffer, data, MAX_BATCHFILENAME) )
		strcpy_s( settings.navmeshesListFile, MAX_BATCHFILENAME, data );
	if( ScanLine("exporterExeFile", buffer, data, MAX_BATCHFILENAME) )
		strcpy_s( settings.exporterExeFile, MAX_BATCHFILENAME, data );

	if( ScanLine("gameExtraParams", buffer, data, MAX_BATCHFILENAME) )
	{
		if( MAX_BATCHFILENAME > 1)
			strcpy_s( settings.gameExtraParams, MAX_BATCHFILENAME, data );
	}

	if( ScanLine("DLC_areasXML", buffer, data, MAX_BATCHFILENAME) )
		strcpy_s( settings.DLC_areasXML, MAX_BATCHFILENAME, data );
	if( ScanLine("DLC_extraContentParams", buffer, data, MAX_BATCHFILENAME) )
		strcpy_s( settings.DLC_extraContentParams, MAX_BATCHFILENAME, data );
	if( ScanLine("DLC_finalOutputFolder", buffer, data, MAX_BATCHFILENAME) )
		strcpy_s( settings.DLC_finalOutputFolder, MAX_BATCHFILENAME, data );

	if( ScanLine("pathForBatchFiles", buffer, data, MAX_BATCHFILENAME) )
		strcpy_s( settings.pathForBatchFiles, MAX_BATCHFILENAME, data );
	if( ScanLine("batchFileForDelTempFiles", buffer, data, MAX_BATCHFILENAME) )
		strcpy_s( settings.batchFileForDelTempFiles, MAX_BATCHFILENAME, data );
	if( ScanLine("batchFileForGetLatestTools", buffer, data, MAX_BATCHFILENAME) )
		strcpy_s( settings.batchFileForGetLatestTools, MAX_BATCHFILENAME, data );
	if( ScanLine("batchFileForGetLatestData", buffer, data, MAX_BATCHFILENAME) )
		strcpy_s( settings.batchFileForGetLatestData, MAX_BATCHFILENAME, data );
	if( ScanLine("batchFileForGetLatestExe", buffer, data, MAX_BATCHFILENAME) )
		strcpy_s( settings.batchFileForGetLatestExe, MAX_BATCHFILENAME, data );

	if( ScanLine("batchFileForCompileNavmeshes", buffer, data, MAX_BATCHFILENAME) )
		strcpy_s( settings.batchFileForCompileNavmeshes, MAX_BATCHFILENAME, data );
	if( ScanLine("batchFileForCheckOutNavmeshes", buffer, data, MAX_BATCHFILENAME) )
		strcpy_s( settings.batchFileForCheckOutNavmeshes, MAX_BATCHFILENAME, data );
	if( ScanLine("batchFileForCheckInNavmeshes", buffer, data, MAX_BATCHFILENAME) )
		strcpy_s( settings.batchFileForCheckInNavmeshes, MAX_BATCHFILENAME, data );
	if( ScanLine("batchFileForCheckInAudmeshes", buffer, data, MAX_BATCHFILENAME) )
		strcpy_s( settings.batchFileForCheckInAudmeshes, MAX_BATCHFILENAME, data );
	if( ScanLine("batchFileForCheckInNavnodes", buffer, data, MAX_BATCHFILENAME) )
		strcpy_s( settings.batchFileForCheckInNavnodes, MAX_BATCHFILENAME, data );
	if( ScanLine("batchFileForCheckInMapping", buffer, data, MAX_BATCHFILENAME) )
		strcpy_s( settings.batchFileForCheckInMapping, MAX_BATCHFILENAME, data );

	if( ScanLine("batchFileForExportGeometry", buffer, data, MAX_BATCHFILENAME) )
		strcpy_s( settings.batchFileForExportGeometry, MAX_BATCHFILENAME, data );
	if( ScanLine("batchFileForExportRegion", buffer, data, MAX_BATCHFILENAME) )
		strcpy_s( settings.batchFileForExportRegion, MAX_BATCHFILENAME, data );

	//-------------
	// Booleans

	if( ScanLine("bGetLatestToolsFirst", buffer, data, MAX_BATCHFILENAME) )
		settings.bGetLatestToolsFirst = atoi(data);
	if( ScanLine("bGetLatestDataFirst", buffer, data, MAX_BATCHFILENAME) )
		settings.bGetLatestDataFirst = atoi(data);
	if( ScanLine("bGetLatestExeFirst", buffer, data, MAX_BATCHFILENAME) )
		settings.bGetLatestExeFirst = atoi(data);
	if( ScanLine("bDeleteTempFilesBeforeBuild", buffer, data, MAX_BATCHFILENAME) )
		settings.bDeleteTempFilesBeforeBuild = atoi(data);
	if( ScanLine("bCheckNavmeshesIntoPerforce", buffer, data, MAX_BATCHFILENAME) )
		settings.bCheckNavmeshesIntoPerforce = atoi(data);
	if( ScanLine("bDoExport", buffer, data, MAX_BATCHFILENAME) )
		settings.bDoExport = atoi(data);
	if( ScanLine("bDoCompile", buffer, data, MAX_BATCHFILENAME) )
		settings.bDoCompile = atoi(data);
	if( ScanLine("bDeleteTempFilesBeforeBuild", buffer, data, MAX_BATCHFILENAME) )
		settings.bDeleteTempFilesBeforeBuild = atoi(data);

	if( ScanLine("bWholeMapBuild", buffer, data, MAX_BATCHFILENAME) )
		settings.bWholeMapBuild = atoi(data);
	if( ScanLine("bContinueFrom", buffer, data, MAX_BATCHFILENAME) )
		settings.bContinueFrom = atoi(data);
	if( ScanLine("bRegion", buffer, data, MAX_BATCHFILENAME) )
		settings.bRegion = atoi(data);
	if( ScanLine("bDLC", buffer, data, MAX_BATCHFILENAME) )
		settings.bDLC = atoi(data);
	if( ScanLine("bIsMultiplayerDLC", buffer, data, MAX_BATCHFILENAME) )
		settings.bIsMultiplayerDLC = atoi(data);
	

	//---------------
	// Integer vals

	if( ScanLine("iMinNavmeshesImgSizeForSuccessfulBuildKb", buffer, data, MAX_BATCHFILENAME) )
		settings.iMinNavmeshesImgSizeForSuccessfulBuildKb = atoi(data);
	if( ScanLine("iBuildHour24", buffer, data, MAX_BATCHFILENAME) )
		settings.iBuildHour24 = atoi(data);
	if( ScanLine("iBuildMinute", buffer, data, MAX_BATCHFILENAME) )
		settings.iBuildMinute = atoi(data);

	if( ScanLine("iRegionMinX", buffer, data, MAX_BATCHFILENAME) )
		settings.iRegionMinX = atoi(data);
	if( ScanLine("iRegionMinY", buffer, data, MAX_BATCHFILENAME) )
		settings.iRegionMinY = atoi(data);
	if( ScanLine("iRegionMaxX", buffer, data, MAX_BATCHFILENAME) )
		settings.iRegionMaxX = atoi(data);
	if( ScanLine("iRegionMaxY", buffer, data, MAX_BATCHFILENAME) )
		settings.iRegionMaxY = atoi(data);

	return TRUE;
}

/*
BOOL LoadSettings(TSettings & settings)
{
	FILE * pFile = NULL;
	fopen_s(&pFile, settings.iniFileName, "rt");
	if(!pFile)
		return FALSE;

	int iVersion;
	fscanf_s(pFile, "version = %i\n", &iVersion);
	if(iVersion != SETTINGS_FILE_VERSION)
	{
		fclose(pFile);
		return FALSE;
	}

	ZeroMemory(&settings, sizeof(settings));
	settings.SetDefault();
	
	// Used for storing the file position if we have to seek back if failing to find
	// a parameter.
	long pos;

	fscanf_s(pFile, "projectRoot = %s\n", &settings.projectRoot, MAX_BATCHFILENAME);	
	fscanf_s(pFile, "pathForExport = %s\n", &settings.pathForExport, MAX_BATCHFILENAME);
	fscanf_s(pFile, "navmeshesImgPath = %s\n", &settings.navmeshesImgPath, MAX_BATCHFILENAME);
	fscanf_s(pFile, "levelCommonPath = %s\n", &settings.levelCommonPath, MAX_BATCHFILENAME);
	fscanf_s(pFile, "iniFileName = %s\n", &settings.iniFileName, MAX_BATCHFILENAME);
	fscanf_s(pFile, "navmeshesListFile = %s\n", &settings.navmeshesListFile, MAX_BATCHFILENAME);
	pos = ftell(pFile);
	if(fscanf_s(pFile, "exporterExeFile = %s\n", &settings.exporterExeFile, MAX_BATCHFILENAME) != 1)
	{
		fseek(pFile, pos, SEEK_SET);
	}

	char dummyStr[MAX_BATCHFILENAME];
	pos = ftell(pFile);
	if(fscanf_s(pFile, "360GameFolder = %s\n", &dummyStr, MAX_BATCHFILENAME) != 1)
	{
		fseek(pFile, pos, SEEK_SET);
	}
	pos = ftell(pFile);
	if(fscanf_s(pFile, "360GameTitle = %s\n", &dummyStr, MAX_BATCHFILENAME) != 1)
	{
		fseek(pFile, pos, SEEK_SET);
	}

	fscanf_s(pFile, "gameExtraParams = "); // skip/match these tokens
	fgets(settings.gameExtraParams, MAX_BATCHFILENAME, pFile);
	size_t iLen = strlen(settings.gameExtraParams);
	if(iLen > 1)
		settings.gameExtraParams[ iLen-1 ] = 0;

	fscanf_s(pFile, "pathForBatchFiles = %s\n", &settings.pathForBatchFiles, MAX_BATCHFILENAME);
	fscanf_s(pFile, "batchFileForDelTempFiles = %s\n", &settings.batchFileForDelTempFiles, MAX_BATCHFILENAME);
	fscanf_s(pFile, "batchFileForGetLatestTools = %s\n", &settings.batchFileForGetLatestTools, MAX_BATCHFILENAME);
	fscanf_s(pFile, "batchFileForGetLatestData = %s\n", &settings.batchFileForGetLatestData, MAX_BATCHFILENAME);
	fscanf_s(pFile, "batchFileForGetLatestExe = %s\n", &settings.batchFileForGetLatestExe, MAX_BATCHFILENAME);
	pos = ftell(pFile);
	if(fscanf_s(pFile, "batchFileForRunGame = %s\n", &dummyStr, MAX_BATCHFILENAME) != 1)
	{
		fseek(pFile, pos, SEEK_SET);
	}
	fscanf_s(pFile, "batchFileForCompileNavmeshes = %s\n", &settings.batchFileForCompileNavmeshes, MAX_BATCHFILENAME);
	fscanf_s(pFile, "batchFileForCheckOutNavmeshes = %s\n", &settings.batchFileForCheckOutNavmeshes, MAX_BATCHFILENAME);
	fscanf_s(pFile, "batchFileForCheckInNavmeshes = %s\n", &settings.batchFileForCheckInNavmeshes, MAX_BATCHFILENAME);
	fscanf_s(pFile, "batchFileForCheckInAudmeshes = %s\n", &settings.batchFileForCheckInAudmeshes, MAX_BATCHFILENAME);
	fscanf_s(pFile, "batchFileForCheckInNavnodes = %s\n", &settings.batchFileForCheckInNavnodes, MAX_BATCHFILENAME);
	fscanf_s(pFile, "batchFileForCheckInMapping = %s\n", &settings.batchFileForCheckInMapping, MAX_BATCHFILENAME);

	pos = ftell(pFile);
	if(fscanf_s(pFile, "batchFileForExportGeometry = %s\n", &settings.batchFileForExportGeometry, MAX_BATCHFILENAME) != 1)
	{
		fseek(pFile, pos, SEEK_SET);
	}
	pos = ftell(pFile);
	if(fscanf_s(pFile, "batchFileToResetXBox360 = %s\n", &dummyStr, MAX_BATCHFILENAME) != 1)
	{
		fseek(pFile, pos, SEEK_SET);
	}

	fscanf_s(pFile, "bGetLatestToolsFirst = %i\n", &settings.bGetLatestToolsFirst);
	fscanf_s(pFile, "bGetLatestDataFirst = %i\n", &settings.bGetLatestDataFirst);
	fscanf_s(pFile, "bGetLatestExeFirst = %i\n", &settings.bGetLatestExeFirst);
	fscanf_s(pFile, "bDeleteTempFilesBeforeBuild = %i\n", &settings.bDeleteTempFilesBeforeBuild);
	fscanf_s(pFile, "bCheckNavmeshesIntoPerforce = %i\n", &settings.bCheckNavmeshesIntoPerforce);
	fscanf_s(pFile, "bDoExport = %i\n", &settings.bDoExport);
	fscanf_s(pFile, "bDoCompile = %i\n", &settings.bDoCompile);
	fscanf_s(pFile, "bDeleteTempFilesBeforeBuild = %i\n", &settings.bDeleteTempFilesBeforeBuild);
	fscanf_s(pFile, "iMinNavmeshesImgSizeForSuccessfulBuildKb = %i\n", &settings.iMinNavmeshesImgSizeForSuccessfulBuildKb);

	fscanf_s(pFile, "iBuildHour24 = %i\n", &settings.iBuildHour24);
	fscanf_s(pFile, "iBuildMinute = %i\n", &settings.iBuildMinute);

	pos = ftell(pFile);
	int dummy;
	if(fscanf_s(pFile, "bUseNewToolForExport = %i\n", &dummy) != 1)
	{
		fseek(pFile, pos, SEEK_SET);
	}

	fclose(pFile);

	CloseLogFile();
	OpenLogFile();

	return TRUE;
}
*/

BOOL SaveSettings(TSettings & settings)
{
	FILE * pFile = NULL;
	fopen_s(&pFile, settings.iniFileName, "wt");
	if(!pFile)
		return FALSE;

	fprintf(pFile, "version = %i\n", SETTINGS_FILE_VERSION);

	fprintf(pFile, "projectRoot = %s\n", settings.projectRoot);	
	fprintf(pFile, "pathForExport = %s\n", settings.pathForExport);
	fprintf(pFile, "navmeshesImgPath = %s\n", settings.navmeshesImgPath);
	fprintf(pFile, "levelCommonPath = %s\n", settings.levelCommonPath);
	fprintf(pFile, "iniFileName = %s\n", settings.iniFileName);
	fprintf(pFile, "navmeshesListFile = %s\n", settings.navmeshesListFile);
	fprintf(pFile, "exporterExeFile = %s\n", settings.exporterExeFile);
	fprintf(pFile, "gameExtraParams = %s\n", settings.gameExtraParams);

	fprintf(pFile, "DLC_areasXML = %s\n", settings.DLC_areasXML);
	fprintf(pFile, "DLC_extraContentParams = %s\n", settings.DLC_extraContentParams);
	fprintf(pFile, "DLC_finalOutputFolder = %s\n", settings.DLC_finalOutputFolder);

	fprintf(pFile, "pathForBatchFiles = %s\n", settings.pathForBatchFiles);
	fprintf(pFile, "batchFileForDelTempFiles = %s\n", settings.batchFileForDelTempFiles);
	fprintf(pFile, "batchFileForGetLatestTools = %s\n", settings.batchFileForGetLatestTools);
	fprintf(pFile, "batchFileForGetLatestData = %s\n", settings.batchFileForGetLatestData);
	fprintf(pFile, "batchFileForGetLatestExe = %s\n", settings.batchFileForGetLatestExe);
	fprintf(pFile, "batchFileForCompileNavmeshes = %s\n", settings.batchFileForCompileNavmeshes);
	fprintf(pFile, "batchFileForCheckOutNavmeshes = %s\n", settings.batchFileForCheckOutNavmeshes);
	fprintf(pFile, "batchFileForCheckInNavmeshes = %s\n", settings.batchFileForCheckInNavmeshes);
	fprintf(pFile, "batchFileForCheckInAudmeshes = %s\n", settings.batchFileForCheckInAudmeshes);
	fprintf(pFile, "batchFileForCheckInNavnodes = %s\n", settings.batchFileForCheckInNavnodes);

	fprintf(pFile, "batchFileForCheckInMapping = %s\n", settings.batchFileForCheckInMapping);
	fprintf(pFile, "batchFileForExportGeometry = %s\n", settings.batchFileForExportGeometry);
	fprintf(pFile, "batchFileForExportRegion = %s\n", settings.batchFileForExportRegion);

	fprintf(pFile, "bGetLatestToolsFirst = %i\n", settings.bGetLatestToolsFirst);
	fprintf(pFile, "bGetLatestDataFirst = %i\n", settings.bGetLatestDataFirst);
	fprintf(pFile, "bGetLatestExeFirst = %i\n", settings.bGetLatestExeFirst);
	fprintf(pFile, "bDeleteTempFilesBeforeBuild = %i\n", settings.bDeleteTempFilesBeforeBuild);
	fprintf(pFile, "bCheckNavmeshesIntoPerforce = %i\n", settings.bCheckNavmeshesIntoPerforce);
	fprintf(pFile, "bDoExport = %i\n", settings.bDoExport);
	fprintf(pFile, "bDoCompile = %i\n", settings.bDoCompile);
	fprintf(pFile, "bDeleteTempFilesBeforeBuild = %i\n", settings.bDeleteTempFilesBeforeBuild);
	fprintf(pFile, "iMinNavmeshesImgSizeForSuccessfulBuildKb = %i\n", settings.iMinNavmeshesImgSizeForSuccessfulBuildKb);

	fprintf(pFile, "iBuildHour24 = %i\n", settings.iBuildHour24);
	fprintf(pFile, "iBuildMinute = %i\n", settings.iBuildMinute);

	fprintf(pFile, "bWholeMapBuild = %i\n", settings.bWholeMapBuild);
	fprintf(pFile, "bContinueFrom = %i\n", settings.bContinueFrom);
	fprintf(pFile, "bRegion = %i\n", settings.bRegion);
	fprintf(pFile, "bDLC = %i\n", settings.bDLC);
	fprintf(pFile, "bIsMultiplayerDLC = %i\n", settings.bIsMultiplayerDLC);

	fprintf(pFile, "iRegionMinX = %i\n", settings.iRegionMinX);
	fprintf(pFile, "iRegionMinY = %i\n", settings.iRegionMinY);
	fprintf(pFile, "iRegionMaxX = %i\n", settings.iRegionMaxX);
	fprintf(pFile, "iRegionMaxY = %i\n", settings.iRegionMaxY);

	fprintf(pFile, "\n");
	fclose(pFile);

	return TRUE;
}


#ifdef _DEBUG

char dbgPrintTmp[8192];

void DebugPrint(char * fmt, ...)
{
	dbgPrintTmp[0] = 0;
	dbgPrintTmp[8191] = 0;

	va_list argptr;
	va_start(argptr, fmt);
	_vsnprintf_s(dbgPrintTmp, 8192, fmt, argptr);
	printf("%s", dbgPrintTmp);
	OutputDebugString(dbgPrintTmp);
	if(AppData.pLogFile)
	{
		fprintf(AppData.pLogFile, dbgPrintTmp);
		fflush(AppData.pLogFile);
	}
	va_end(argptr);
}


void DebugPrintSettings()
{
	DebugPrint("Settings:\n");
	DebugPrint("iBuildHour24 : %i\n", AppData.settings.iBuildHour24);
	DebugPrint("iBuildMinute : %i\n", AppData.settings.iBuildMinute);

	DebugPrint("bGetLatestToolsFirst : %s\n", AppData.settings.bGetLatestToolsFirst ? "true" : "false");
	DebugPrint("bGetLatestDataFirst : %s\n", AppData.settings.bGetLatestDataFirst ? "true" : "false");
	DebugPrint("bCheckNavmeshesIntoPerforce : %s\n", AppData.settings.bCheckNavmeshesIntoPerforce ? "true" : "false");
	DebugPrint("pathForExport : %s\n", AppData.settings.pathForExport);
	DebugPrint("\n");
}

void DebugPrintTime(tm & Time)
{
	DebugPrint("[%i:%i:%i]", Time.tm_hour, Time.tm_min, Time.tm_sec);
}

void DebugPrintTime()
{
	time_t t;
	time(&t);
	tm currentTime;
	localtime_s(&currentTime, &t);
	DebugPrintTime(currentTime);
}

#endif	// _DEBUG

void StripFileTitle(const char * pFile, char * pTitleBuffer)
{
	const char * pStart = &pFile[strlen(pFile)-1];
	while(pStart != pFile && *pStart != '\\' && *pStart != '/')
		pStart--;
	if(*pStart=='\\' || *pStart=='/')
		pStart++;

	const char * pEnd = &pFile[strlen(pFile)-1];
	while(pEnd != pStart && *pEnd != '.')
		pEnd--;

	if(pStart==pEnd)
		*pTitleBuffer = 0;
	else
	{
		while (pStart!=pEnd)
		{
			*pTitleBuffer = *pStart;
			pTitleBuffer++;
			pStart++;
		}
		*pTitleBuffer=0;
	} 
}

void OpenLogFile()
{
	char iniFileTitle[256];
	StripFileTitle(AppData.settings.iniFileName, iniFileTitle);
	if(!iniFileTitle[0])
		strcpy_s(iniFileTitle, 256, "none");

	char logFileName[512];

	if(*AppData.settings.pathForExport)
	{
		sprintf_s(logFileName, 512, "%s/NavMeshScheduler_%s.log", AppData.settings.pathForExport, iniFileTitle);
	}
	else
	{
		sprintf_s(logFileName, 512, "NavMeshScheduler_%s.log", iniFileTitle);
	}

	AppData.pLogFile = _fsopen(logFileName, "wt", _SH_DENYWR);
}

void CloseLogFile()
{
	if(AppData.pLogFile)
		fclose(AppData.pLogFile);
	AppData.pLogFile = NULL;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	ZeroMemory(&AppData, sizeof(TAppData));
	AppData.hInstance = hInstance;
	AppData.bMinimized = false;
	AppData.bScheduledBuildEnabled = false;
	AppData.settings.bWholeMapBuild = true;
	AppData.settings.bContinueFrom = false;

	/*
	// Only allow one instance of this app at any time
	HANDLE hMutex = CreateMutexA(NULL, TRUE, "NavMeshTrayApp");
	if(GetLastError()==ERROR_ALREADY_EXISTS)
	{
		ReleaseMutex(hMutex);
		return 0;
	}
	*/

	memset(&AppData.settings, 0, sizeof(TSettings));
	AppData.settings.SetDefault();

	/*
	BOOL bLoadedSettings = LoadSettings(AppData.settings);
	if(!bLoadedSettings)
	{
		AppData.settings.SetDefault();
		SaveSettings(AppData.settings);
	}
	*/

	// Prevent the system from going to sleep or powering down whilst this sys-tray app is active
	EXECUTION_STATE iReturn = SetThreadExecutionState(ES_SYSTEM_REQUIRED|ES_DISPLAY_REQUIRED|ES_CONTINUOUS);

	CalcTimeUntilNextBuild();

	InitCommonControls();

	// Create the main window for this app
	CreateMainWindow();

	//****************************************************************************
	// Initially disable controls

	ToggleScheduledBuildControls(!AppData.bScheduledBuildEnabled);
	ToggleMapRegionCompileControls(AppData.settings.bWholeMapBuild, AppData.settings.bContinueFrom, AppData.settings.bRegion, AppData.settings.bDLC);

	// Update the app settings from the control values
	InitSettingFromControls();
	// And then update the controls from the settings in case any values were not allowed
	InitControlsFromSettings();
	DebugPrintSettings();

	// This timer makes sure that the message-loop activates every POLL_BATCH_PROCESS_FREQUENCY
	// It is inside the message loop that we poll the progress of the navmesh build.
	SetTimer(AppData.hWnd, STAY_ALIVE_TIMER_ID, POLL_BATCH_PROCESS_FREQUENCY, NULL);

	// Enter the message loop
	MessageLoop();

	// Allow the system to return to normal again
	iReturn = SetThreadExecutionState(ES_CONTINUOUS);

	if(AppData.pLogFile)
	{
		DebugPrint("Quitting app at ");
		DebugPrintTime();
		DebugPrint("\n");

		fclose(AppData.pLogFile);
	}

	return 1;
}

BOOL MessageLoop()
{
	MSG Msg;
	while(GetMessage(&Msg, NULL, 0, 0) > 0)
	{
		IsDialogMessage(AppData.hWnd, &Msg);

		Sleep(0);

		//*****************************************************************
		// Every so often check to see if a navmesh build should be started

		if(ShouldStartBuild())
		{
			//*******************************************************************************************
			// Calculate the next time a build is required, and a timeout time for this build to complete

			CalcTimeUntilNextBuild();

			//**********************
			// Start a navmesh build

			StartNavmeshBuild();
		}

		//****************************************************************************
		// If the batch-file handle is non-zero then wait for it to complete before
		// going onto the next stage of the build process.

		if(AppData.bBuildIsCurrentlyActive)
		{
			if(AppData.hBatchFileProcess!=0)
			{
				//***************************************************************************
				// Handle incrementing the build stage every time our batch file completes

				DWORD exitCode;
				GetExitCodeProcess(AppData.hBatchFileProcess, &exitCode);
				if(exitCode != STILL_ACTIVE)
				{
					AppData.hBatchFileProcess = 0;
					DoNavMeshBuild();
				}
				else if(AppData.eBuildStage==BS_ExportingCollision)
				{
					// Not sure if we still need to do this, now when we don't export
					// using a console. /FF

					//******************************************************************
					// Handle aborting the export process if it appears to have hung

					UINT32 iDeltaTime = timeGetTime() - AppData.iTimeAtStartOfExportStageMS;
					if(iDeltaTime > MAX_NUM_SECONDS_FOR_EXPORT_TO_COMPLETE*1000)
					{
						AppData.eBuildStage = BS_Finished;
						AppData.hBatchFileProcess = 0;
						AppData.bBuildIsCurrentlyActive = FALSE;

						DebugPrint("Export stage has hung.  Build aborted at ");
						DebugPrintTime();
						DebugPrint("\n");

						char * pWindowTitle = ConstructWindowTitle();
						SetWindowText(AppData.hWnd, pWindowTitle);
					}
				}
			}
		}
	}

	return TRUE;
}

void CreateMainWindow()
{
	LPTSTR dialogRes = MAKEINTRESOURCE(IDD_NAVMESHDIALOG);

	AppData.hWnd = CreateDialog(AppData.hInstance, dialogRes, GetDesktopWindow(), NavMeshDialogProc);

	char * pWindowTitle = ConstructWindowTitle();
	SetWindowText(AppData.hWnd, pWindowTitle);
}

BOOL CALLBACK NavMeshDialogProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch(uMsg)
	{
		case WM_INITDIALOG:
		{
			CheckDlgButton(hwndDlg, IDC_CHECK_BUILDENABLED, BST_UNCHECKED);
			ShowWindow(hwndDlg, SW_SHOW);
			return TRUE;
		}
		case WM_COMMAND:
		{
			int wNotifyCode = HIWORD(wParam); 
			int wID = LOWORD(wParam); 
			HWND hwndCtl = (HWND) lParam;

			HandleWMCOMMAND(wNotifyCode, wID, hwndCtl);
			
			return TRUE;
		}
		case WM_TIMER:
		{
			switch(wParam)
			{
				case STAY_ALIVE_TIMER_ID:
				{
					// Do nothing.  The purpose of this timer is purely so we can
					// poll the progress of the build from within the message-loop.
					break;
				}
				case SCHEDULED_BUILD_TIMER_ID:
				{
					if(AppData.bScheduledBuildEnabled && !AppData.bBuildIsCurrentlyActive)
					{
						AppData.bTimerMsgReceived = TRUE;
					}
					KillTimer(AppData.hWnd, SCHEDULED_BUILD_TIMER_ID);
					break;
				}
			}
			return TRUE;
		}
		case WM_TRAY_MESSAGE:
		{
			if(wParam==TASKBAR_ICON_ID)
			{
				if(lParam==WM_LBUTTONDBLCLK)
				{
					MaximizeToWindow();
				}
			}
			return TRUE;
		}
		case WM_POWERBROADCAST:
		{
			// Don't allow power-management to shutdown the PC whilst we're waiting for a scheduled build
			return BROADCAST_QUERY_DENY;
		}
		case WM_SYSCOMMAND:
		{
			// Attempt to revent the screensaver & monitor power-saving from activating
			UINT32 iCode = ((UINT32)wParam & 0xFFF0);
			if(iCode==SC_SCREENSAVE)
			{
				DebugPrint("Blocked screen-saver activation at ");
				DebugPrintTime();
				DebugPrint("\n");

				return TRUE;
			}
			if(iCode==SC_MONITORPOWER)
			{
				DebugPrint("Blocked monitor power-down at ");
				DebugPrintTime();
				DebugPrint("\n");

				return TRUE;
			}
		}
	}

	return FALSE;
}

void HandleWMCOMMAND(int iNotifyCode, int iCtrlID, HWND hWnd)
{
	//*****************************************************
	// Is this the text in an edit control being altered?
	
	if(iNotifyCode==EN_CHANGE)
	{
		char str[256];
		switch(iCtrlID)
		{
			case IDC_EDIT_PROJECTROOT:
				GetDlgItemText(AppData.hWnd, IDC_EDIT_PROJECTROOT, str, 255);
				strcpy_s(AppData.settings.projectRoot, 255, str);
				return;
			case IDC_EDIT_EXPORTPATH:
				GetDlgItemText(AppData.hWnd, IDC_EDIT_EXPORTPATH, str, 255);
				strcpy_s(AppData.settings.pathForExport, 255, str);
				return;
			case IDC_EDIT_GAMEEXTRAPARAMS:
				GetDlgItemText(AppData.hWnd, IDC_EDIT_GAMEEXTRAPARAMS, str, 255);
				strcpy_s(AppData.settings.gameExtraParams, 255, str);
				return;
			case IDC_EDIT_NAVMESHES_IMG_PATH:
				GetDlgItemText(AppData.hWnd, IDC_EDIT_NAVMESHES_IMG_PATH, str, 255);
				strcpy_s(AppData.settings.navmeshesImgPath, 255, str);
				return;
			case IDC_EDIT_LEVEL_COMMON_DATA_PATH:
				GetDlgItemText(AppData.hWnd, IDC_EDIT_LEVEL_COMMON_DATA_PATH, str, 255);
				strcpy_s(AppData.settings.levelCommonPath, 255, str);
				return;
			case IDC_EDIT_INI_FILENAME:
				GetDlgItemText(AppData.hWnd, IDC_EDIT_INI_FILENAME, str, 255);
				strcpy_s(AppData.settings.iniFileName, 255, str);
				return;
			case IDC_EDIT_COMPILE_LST_FILE:
				GetDlgItemText(AppData.hWnd, IDC_EDIT_COMPILE_LST_FILE, str, 255);
				strcpy_s(AppData.settings.navmeshesListFile, 255, str);
				return;
			case IDC_EDIT_COMPILE_BATCH_FILE:
				GetDlgItemText(AppData.hWnd, IDC_EDIT_COMPILE_BATCH_FILE, str, 255);
				strcpy_s(AppData.settings.batchFileForCompileNavmeshes, 255, str);
				return;
			case IDC_EDIT_EXPORTER_NAME:
				GetDlgItemText(AppData.hWnd, IDC_EDIT_EXPORTER_NAME, str, 255);
				strcpy_s(AppData.settings.exporterExeFile, 255, str);
				return;
		}
	}

	if(iNotifyCode == BN_CLICKED)
	{
		InitSettingFromControls();
	}

	//********************************************************
	// Handle WM_COMMAND msg based upon the ID of the control

	switch(iCtrlID)
	{
		case ID_MINIMIZE_TO_TRAY:
		{
			MinimizeToSystemTray();
			break;
		}
		case ID_START_NOW:
		{
			AppData.bForceStartBuildNow = TRUE;
			break;
		}
		case ID_LOAD_SETTINGS:
		{
			LoadSettings_New(AppData.settings);
			InitControlsFromSettings();
			ToggleScheduledBuildControls(!AppData.bScheduledBuildEnabled);
			ToggleMapRegionCompileControls(AppData.settings.bWholeMapBuild, AppData.settings.bContinueFrom, AppData.settings.bRegion, AppData.settings.bDLC);
			break;
		}
		case ID_SAVE_SETTINGS:
		{
			SaveSettings(AppData.settings);

			break;
		}
		case IDC_BUTTON_BROWSE_FOR_INI_FILE:
		{
			BrowseForSettingsFile();
			InitControlsFromSettings();
			ToggleScheduledBuildControls(!AppData.bScheduledBuildEnabled);
			ToggleMapRegionCompileControls(AppData.settings.bWholeMapBuild, AppData.settings.bContinueFrom, AppData.settings.bRegion, AppData.settings.bDLC);
			break;
		}
		case IDC_BUTTON_BROWSE_FOR_LST_FILE:
		{
			BrowseForLstFile();
			InitControlsFromSettings();
			ToggleScheduledBuildControls(!AppData.bScheduledBuildEnabled);
			ToggleMapRegionCompileControls(AppData.settings.bWholeMapBuild, AppData.settings.bContinueFrom, AppData.settings.bRegion, AppData.settings.bDLC);
			break;
		}
		case IDC_BUTTON_BROWSE_FOR_COMPILE_BATCHFILE:
		{
			BrowseForCompileBatchFile();
			InitControlsFromSettings();
			ToggleScheduledBuildControls(!AppData.bScheduledBuildEnabled);
			ToggleMapRegionCompileControls(AppData.settings.bWholeMapBuild, AppData.settings.bContinueFrom, AppData.settings.bRegion, AppData.settings.bDLC);
			break;
		}
		case ID_EXIT:
		{
			PostQuitMessage(0);
			break;
		}
		case IDC_CHECK_BUILDENABLED:
		{
			AppData.bScheduledBuildEnabled = !AppData.bScheduledBuildEnabled;
			ToggleScheduledBuildControls(!AppData.bScheduledBuildEnabled);

			// Update the app settings from the control values
			InitSettingFromControls();
			// And then update the controls from the settings in case any values were not allowed
			InitControlsFromSettings();

			DebugPrintSettings();

			CalcTimeUntilNextBuild();

			break;
		}
		case IDC_RADIO_WHOLEMAP:
		{
			AppData.settings.bWholeMapBuild = TRUE;
			AppData.settings.bContinueFrom = FALSE;
			AppData.settings.bRegion = FALSE;
			AppData.settings.bDLC = FALSE;
			ToggleMapRegionCompileControls(AppData.settings.bWholeMapBuild, AppData.settings.bContinueFrom, AppData.settings.bRegion, AppData.settings.bDLC);
			break;
		}
		case IDC_RADIO_CONTINUE:
		{
			AppData.settings.bWholeMapBuild = FALSE;
			AppData.settings.bContinueFrom = TRUE;
			AppData.settings.bRegion = FALSE;
			AppData.settings.bDLC = FALSE;
			ToggleMapRegionCompileControls(AppData.settings.bWholeMapBuild, AppData.settings.bContinueFrom, AppData.settings.bRegion, AppData.settings.bDLC);
			break;
		}
		case IDC_RADIO_REGION:
		{
			AppData.settings.bWholeMapBuild = FALSE;
			AppData.settings.bContinueFrom = FALSE;
			AppData.settings.bRegion = TRUE;
			AppData.settings.bDLC = FALSE;
			ToggleMapRegionCompileControls(AppData.settings.bWholeMapBuild, AppData.settings.bContinueFrom, AppData.settings.bRegion, AppData.settings.bDLC);
			break;
		}
		case IDC_RADIO_DLC:
		{
			AppData.settings.bWholeMapBuild = FALSE;
			AppData.settings.bContinueFrom = FALSE;
			AppData.settings.bRegion = FALSE;
			AppData.settings.bDLC = TRUE;
			ToggleMapRegionCompileControls(AppData.settings.bWholeMapBuild, AppData.settings.bContinueFrom, AppData.settings.bRegion, AppData.settings.bDLC);
			break;
		}
	}
}

void ToggleScheduledBuildControls(BOOL bEnabled)
{
	int iItems[] =
	{
		IDC_EDIT_HOUR,
		IDC_EDIT_MINUTE,
		IDC_CHECK_GETLATESTDATA,
		IDC_CHECK_GETLATESTTOOLS,
		IDC_CHECK_GETLATESTEXE,
		IDC_CHECK_CHECK_IN,
		IDC_CHECK_CONVERT_INDEPENDENT_AFTER,
		IDC_CHECK_DO_EXPORT,
		IDC_CHECK_DO_COMPILE,
		IDC_CHECK_DELETE_TEMP_FILES,

		IDC_EDIT_AB_USERNAME,
		IDC_EDIT_AB_PROJECTNAME,
		IDC_EDIT_PROJECTROOT,
		IDC_EDIT_EXPORTPATH,
		IDC_EDIT_MAP_PARAM,
		IDC_EDIT_NAVMESHES_IMG_PATH,
		IDC_EDIT_LEVEL_COMMON_DATA_PATH,
		IDC_EDIT_INI_FILENAME,
		IDC_EDIT_COMPILE_LST_FILE,
		IDC_EDIT_COMPILE_BATCH_FILE,
		IDC_EDIT_GAMEEXTRAPARAMS,
		IDC_EDIT_EXPORTER_NAME,

		-1
	};

	int i=0;
	while(iItems[i]!=-1)
	{
		HWND hCtrl = GetDlgItem(AppData.hWnd, iItems[i]);
		EnableWindow(hCtrl, bEnabled);
		i++;
	}

	// IDC_EDIT_EXPORTPATH is always disabled for now, as it could be dangerous to specify another location :
	// the location is hardcoded into some parts of the compile process and (more importantly) the export
	// script does a del *.* inside this location!

//	HWND hExportPath = GetDlgItem(AppData.hWnd, IDC_EDIT_EXPORTPATH);
//	EnableWindow(hExportPath, FALSE);

	DisableDeprecatedControls();

	DebugPrintSettings();
}

void EnableItems(int * pItems, BOOL bEnable)
{
	int i=0;
	while(pItems[i]!=-1)
	{
		HWND hCtrl = GetDlgItem(AppData.hWnd, pItems[i]);
		EnableWindow(hCtrl, bEnable);
		i++;
	}
}

void ToggleMapRegionCompileControls(BOOL bWholeMap, BOOL bContinueFrom, BOOL bRegion, BOOL bDLC)
{
	_ASSERTE( (((int)bWholeMap) + ((int)bRegion) + ((int)bContinueFrom) + ((int)bDLC)) == 1);

	CheckDlgButton(AppData.hWnd, IDC_RADIO_WHOLEMAP, BST_UNCHECKED);
	CheckDlgButton(AppData.hWnd, IDC_RADIO_CONTINUE, BST_UNCHECKED);
	CheckDlgButton(AppData.hWnd, IDC_RADIO_REGION, BST_UNCHECKED);
	CheckDlgButton(AppData.hWnd, IDC_RADIO_DLC, BST_UNCHECKED);

	int iItemsWholeMap[] =
	{
		-1
	};
	int iItemsContinueFrom[] =
	{
		IDC_EXTENTS_MIN_X,
		IDC_EXTENTS_MIN_Y,
		-1
	};
	int iItemsRegion[] =
	{
		IDC_EXTENTS_MIN_X,
		IDC_EXTENTS_MIN_Y,
		IDC_EXTENTS_MAX_X,
		IDC_EXTENTS_MAX_Y,
		-1
	};
	int iItemsDLC[] =
	{
		IDC_EDIT_EXPORTDLC_FILE,
		IDC_EDIT_EXTRACONTENT,
		IDC_EDIT_FINAL_FOLDER,
		IDC_CHECK_MULTIPLAYER_DLC,
		-1
	};

	if(bWholeMap)
	{
		EnableItems(iItemsRegion, FALSE);
		EnableItems(iItemsContinueFrom, FALSE);
		EnableItems(iItemsDLC, FALSE);

		EnableItems(iItemsWholeMap, TRUE);

		CheckDlgButton(AppData.hWnd, IDC_RADIO_WHOLEMAP, BST_CHECKED);
	}

	if(bContinueFrom)
	{
		EnableItems(iItemsWholeMap, FALSE);
		EnableItems(iItemsRegion, FALSE);
		EnableItems(iItemsDLC, FALSE);

		EnableItems(iItemsContinueFrom, TRUE);

		CheckDlgButton(AppData.hWnd, IDC_RADIO_CONTINUE, BST_CHECKED);
	}

	if(bRegion)
	{
		EnableItems(iItemsWholeMap, FALSE);
		EnableItems(iItemsContinueFrom, FALSE);
		EnableItems(iItemsDLC, FALSE);

		EnableItems(iItemsRegion, TRUE);

		CheckDlgButton(AppData.hWnd, IDC_RADIO_REGION, BST_CHECKED);
	}

	if(bDLC)
	{
		EnableItems(iItemsWholeMap, FALSE);
		EnableItems(iItemsContinueFrom, FALSE);
		EnableItems(iItemsRegion, FALSE);

		EnableItems(iItemsDLC, TRUE);

		CheckDlgButton(AppData.hWnd, IDC_RADIO_DLC, BST_CHECKED);
	}

	DebugPrintSettings();
}


void InitControlsFromSettings()
{
	CheckDlgButton(AppData.hWnd, IDC_CHECK_GETLATESTTOOLS, AppData.settings.bGetLatestToolsFirst ? BST_CHECKED :  BST_UNCHECKED);
	CheckDlgButton(AppData.hWnd, IDC_CHECK_GETLATESTDATA, AppData.settings.bGetLatestDataFirst ? BST_CHECKED :  BST_UNCHECKED);
	CheckDlgButton(AppData.hWnd, IDC_CHECK_GETLATESTEXE, AppData.settings.bGetLatestExeFirst ? BST_CHECKED :  BST_UNCHECKED);

	CheckDlgButton(AppData.hWnd, IDC_CHECK_CHECK_IN, AppData.settings.bCheckNavmeshesIntoPerforce ? BST_CHECKED :  BST_UNCHECKED);
	CheckDlgButton(AppData.hWnd, IDC_CHECK_DO_EXPORT, AppData.settings.bDoExport ? BST_CHECKED :  BST_UNCHECKED);
	CheckDlgButton(AppData.hWnd, IDC_CHECK_DO_COMPILE, AppData.settings.bDoCompile ? BST_CHECKED :  BST_UNCHECKED);
	CheckDlgButton(AppData.hWnd, IDC_CHECK_DELETE_TEMP_FILES, AppData.settings.bDeleteTempFilesBeforeBuild? BST_CHECKED :  BST_UNCHECKED);

	SetDlgItemInt(AppData.hWnd, IDC_EDIT_HOUR, AppData.settings.iBuildHour24, FALSE);
	SetDlgItemInt(AppData.hWnd, IDC_EDIT_MINUTE, AppData.settings.iBuildMinute, FALSE);

	SetDlgItemText(AppData.hWnd, IDC_EDIT_PROJECTROOT, AppData.settings.projectRoot);
	SetDlgItemText(AppData.hWnd, IDC_EDIT_EXPORTPATH, AppData.settings.pathForExport);
	SetDlgItemText(AppData.hWnd, IDC_EDIT_GAMEEXTRAPARAMS, AppData.settings.gameExtraParams);
	SetDlgItemText(AppData.hWnd, IDC_EDIT_NAVMESHES_IMG_PATH, AppData.settings.navmeshesImgPath);
	SetDlgItemText(AppData.hWnd, IDC_EDIT_LEVEL_COMMON_DATA_PATH, AppData.settings.levelCommonPath);
	SetDlgItemText(AppData.hWnd, IDC_EDIT_INI_FILENAME, AppData.settings.iniFileName);
	SetDlgItemText(AppData.hWnd, IDC_EDIT_COMPILE_LST_FILE, AppData.settings.navmeshesListFile);
	SetDlgItemText(AppData.hWnd, IDC_EDIT_COMPILE_BATCH_FILE, AppData.settings.batchFileForCompileNavmeshes);
	SetDlgItemText(AppData.hWnd, IDC_EDIT_EXPORTER_NAME, AppData.settings.exporterExeFile);

	SetDlgItemText(AppData.hWnd, IDC_EDIT_EXPORTDLC_FILE, AppData.settings.DLC_areasXML);
	SetDlgItemText(AppData.hWnd, IDC_EDIT_EXTRACONTENT, AppData.settings.DLC_extraContentParams);
	SetDlgItemText(AppData.hWnd, IDC_EDIT_FINAL_FOLDER, AppData.settings.DLC_finalOutputFolder);

	CheckDlgButton(AppData.hWnd, IDC_RADIO_WHOLEMAP, AppData.settings.bWholeMapBuild ? BST_CHECKED :  BST_UNCHECKED);
	CheckDlgButton(AppData.hWnd, IDC_RADIO_CONTINUE, AppData.settings.bContinueFrom ? BST_CHECKED :  BST_UNCHECKED);
	CheckDlgButton(AppData.hWnd, IDC_RADIO_REGION, AppData.settings.bRegion ? BST_CHECKED :  BST_UNCHECKED);
	CheckDlgButton(AppData.hWnd, IDC_RADIO_DLC, AppData.settings.bDLC ? BST_CHECKED :  BST_UNCHECKED);

	CheckDlgButton(AppData.hWnd, IDC_CHECK_MULTIPLAYER_DLC, AppData.settings.bIsMultiplayerDLC ? BST_CHECKED :  BST_UNCHECKED);

	SetDlgItemInt(AppData.hWnd, IDC_EXTENTS_MIN_X, AppData.settings.iRegionMinX, TRUE);
	SetDlgItemInt(AppData.hWnd, IDC_EXTENTS_MIN_Y, AppData.settings.iRegionMinY, TRUE);
	SetDlgItemInt(AppData.hWnd, IDC_EXTENTS_MAX_X, AppData.settings.iRegionMaxX, TRUE);
	SetDlgItemInt(AppData.hWnd, IDC_EXTENTS_MAX_Y, AppData.settings.iRegionMaxY, TRUE);

	char * pWindowTitle = ConstructWindowTitle();
	SetWindowText(AppData.hWnd, pWindowTitle);

	DisableDeprecatedControls();
}

void InitSettingFromControls()
{
	UINT iState;
	BOOL bSuccess;

	iState = IsDlgButtonChecked(AppData.hWnd, IDC_CHECK_GETLATESTTOOLS);
	AppData.settings.bGetLatestToolsFirst = (iState==BST_CHECKED);

	iState = IsDlgButtonChecked(AppData.hWnd, IDC_CHECK_GETLATESTDATA);
	AppData.settings.bGetLatestDataFirst = (iState==BST_CHECKED);

	iState = IsDlgButtonChecked(AppData.hWnd, IDC_CHECK_GETLATESTEXE);
	AppData.settings.bGetLatestExeFirst = (iState==BST_CHECKED);
	

	iState = IsDlgButtonChecked(AppData.hWnd, IDC_CHECK_CHECK_IN);
	AppData.settings.bCheckNavmeshesIntoPerforce = (iState==BST_CHECKED);

	iState = IsDlgButtonChecked(AppData.hWnd, IDC_CHECK_DO_EXPORT);
	AppData.settings.bDoExport = (iState==BST_CHECKED);

	iState = IsDlgButtonChecked(AppData.hWnd, IDC_CHECK_DO_COMPILE);
	AppData.settings.bDoCompile = (iState==BST_CHECKED);

	iState = IsDlgButtonChecked(AppData.hWnd, IDC_CHECK_DELETE_TEMP_FILES);
	AppData.settings.bDeleteTempFilesBeforeBuild = (iState==BST_CHECKED);

	UINT32 iHour = GetDlgItemInt(AppData.hWnd, IDC_EDIT_HOUR, &bSuccess, FALSE);
	if(iHour < 0) iHour = 0;
	if(iHour > 23) iHour = 23;
	AppData.settings.iBuildHour24 = iHour;

	UINT32 iMinute = GetDlgItemInt(AppData.hWnd, IDC_EDIT_MINUTE, &bSuccess, FALSE);
	if(iMinute < 0) iMinute = 0;
	if(iMinute > 59) iMinute = 59;
	AppData.settings.iBuildMinute = iMinute;

	UINT iNumChars;
	iNumChars = GetDlgItemText(AppData.hWnd, IDC_EDIT_GAMEEXTRAPARAMS, AppData.settings.gameExtraParams, MAX_BATCHFILENAME);

	iNumChars = GetDlgItemText(AppData.hWnd, IDC_EDIT_EXPORTDLC_FILE, AppData.settings.DLC_areasXML, MAX_BATCHFILENAME);
	iNumChars = GetDlgItemText(AppData.hWnd, IDC_EDIT_EXTRACONTENT, AppData.settings.DLC_extraContentParams, MAX_BATCHFILENAME);
	iNumChars = GetDlgItemText(AppData.hWnd, IDC_EDIT_FINAL_FOLDER, AppData.settings.DLC_finalOutputFolder, MAX_BATCHFILENAME);

	AppData.settings.bWholeMapBuild = (IsDlgButtonChecked(AppData.hWnd, IDC_RADIO_WHOLEMAP)==BST_CHECKED);
	AppData.settings.bContinueFrom = (IsDlgButtonChecked(AppData.hWnd, IDC_RADIO_CONTINUE)==BST_CHECKED);
	AppData.settings.bRegion = (IsDlgButtonChecked(AppData.hWnd, IDC_RADIO_REGION)==BST_CHECKED);
	AppData.settings.bDLC = (IsDlgButtonChecked(AppData.hWnd, IDC_RADIO_DLC)==BST_CHECKED);

	AppData.settings.bIsMultiplayerDLC = (IsDlgButtonChecked(AppData.hWnd, IDC_CHECK_MULTIPLAYER_DLC)==BST_CHECKED);

	AppData.settings.iRegionMinX = GetDlgItemInt(AppData.hWnd, IDC_EXTENTS_MIN_X, &bSuccess, TRUE);
	AppData.settings.iRegionMinY = GetDlgItemInt(AppData.hWnd, IDC_EXTENTS_MIN_Y, &bSuccess, TRUE);
	AppData.settings.iRegionMaxX = GetDlgItemInt(AppData.hWnd, IDC_EXTENTS_MAX_X, &bSuccess, TRUE);
	AppData.settings.iRegionMaxY = GetDlgItemInt(AppData.hWnd, IDC_EXTENTS_MAX_Y, &bSuccess, TRUE);

	//---------------------------------------------------------------

	CalcTimeUntilNextBuild();

	char * pWindowTitle = ConstructWindowTitle();
	SetWindowText(AppData.hWnd, pWindowTitle);
}

// I've just added this to disable certain controls which aren't working currently;
// These will be fixed in future, so I haven't removed them entirely
void DisableDeprecatedControls()
{
	EnableWindow( GetDlgItem(AppData.hWnd, IDC_CHECK_GETLATESTTOOLS), FALSE );
	EnableWindow( GetDlgItem(AppData.hWnd, IDC_CHECK_GETLATESTEXE), FALSE );
}

void BrowseForSettingsFile()
{
	GetFileNameToOpen(AppData.settings.iniFileName, MAX_BATCHFILENAME, "Please select .ini file");
}
void BrowseForLstFile()
{
	GetFileNameToOpen(AppData.settings.navmeshesListFile, MAX_BATCHFILENAME, "Please select .lst file");
}
void BrowseForCompileBatchFile()
{
	GetFileNameToOpen(AppData.settings.batchFileForCompileNavmeshes, MAX_BATCHFILENAME, "Please select .bat file");
}
void GetFileNameToOpen(char * pResultBuffer, int iBufferSize, char * pPromptTxt, char * pFilter, char * pDefaultExtension)
{
	char fileName[256] = { 0 };
	char fileTitle[256] = { 0 };

	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = AppData.hWnd;
	ofn.hInstance = AppData.hInstance;
	ofn.lpstrFilter = pFilter;
	ofn.lpstrFile = fileName;
	ofn.nMaxFile = 256;
	ofn.lpstrFileTitle = fileTitle;
	ofn.nMaxFileTitle = 256;
	ofn.lpstrTitle = pPromptTxt;
	ofn.Flags = 0;
	ofn.lpstrDefExt = pDefaultExtension;

	if(GetOpenFileName(&ofn))
	{
		strcpy_s(pResultBuffer, iBufferSize, fileName);
	}
}

void MinimizeToSystemTray()
{
	LPTSTR iconRes = MAKEINTRESOURCE(IDI_TRAYICON);
	HICON hIcon = LoadIcon(AppData.hInstance, iconRes);

	NOTIFYICONDATA nid;
	ZeroMemory(&nid, sizeof(NOTIFYICONDATA));
	nid.cbSize = sizeof(NOTIFYICONDATA);
	nid.hWnd = AppData.hWnd;
	nid.uID = TASKBAR_ICON_ID;
	nid.uFlags = (NIF_ICON | NIF_MESSAGE | NIF_TIP | NIF_STATE);
	nid.hIcon = hIcon;
	nid.uCallbackMessage = WM_TRAY_MESSAGE;
	strcpy_s(nid.szTip, "NavMesh Scheduler");
	nid.dwState = 0;

	BOOL bRet = Shell_NotifyIcon(NIM_ADD, &nid);

	ShowWindow(AppData.hWnd, SW_HIDE);

	AppData.bMinimized = TRUE;
}

char * ConstructWindowTitle()
{
	char * pTitleName = AppData.settings.gameExtraParams && (*AppData.settings.gameExtraParams) ?
		AppData.settings.gameExtraParams : "Unknown";

	static char tmp[4096];
	if(AppData.bBuildIsCurrentlyActive)
		sprintf_s(tmp, 4096, "Navmesh Scheduler - %s (active)", pTitleName);
	else
		sprintf_s(tmp, 4096, "Navmesh Scheduler - %s (idle)", pTitleName);

	return tmp;
}

void MaximizeToWindow()
{
	NOTIFYICONDATA nid;
	ZeroMemory(&nid, sizeof(NOTIFYICONDATA));
	nid.cbSize = sizeof(NOTIFYICONDATA);
	nid.hWnd = AppData.hWnd;
	nid.uID = TASKBAR_ICON_ID;

	BOOL bRet = Shell_NotifyIcon(NIM_DELETE, &nid);

	ShowWindow(AppData.hWnd, SW_SHOW);

	AppData.bMinimized = FALSE;
}

HANDLE RunBatchFile(char * pFilename, char * pParam1, char * pParam2, char * pParam3, char * pParam4, char * pParam5, char * pParam6, char * pParam7, char * pParam8, char * pParam9, char * pParam10)
{
	char fullFilename[8192];
	char params[8192];

	sprintf_s(fullFilename, "\"%s%s\"", AppData.settings.pathForBatchFiles, pFilename);

	if(pParam1 && pParam2 && pParam3 && pParam4 && pParam5 && pParam6 && pParam7 && pParam8 && pParam9 && pParam10)
	{
		sprintf_s(params, "%s %s %s %s %s %s %s", pParam1, pParam2, pParam3, pParam4, pParam5, pParam6, pParam7, pParam8, pParam9, pParam10);
	}
	else if(pParam1 && pParam2 && pParam3 && pParam4 && pParam5 && pParam6 && pParam7 && pParam8 && pParam9)
	{
		sprintf_s(params, "%s %s %s %s %s %s %s", pParam1, pParam2, pParam3, pParam4, pParam5, pParam6, pParam7, pParam8, pParam9);
	}
	else if(pParam1 && pParam2 && pParam3 && pParam4 && pParam5 && pParam6 && pParam7 && pParam8)
	{
		sprintf_s(params, "%s %s %s %s %s %s %s", pParam1, pParam2, pParam3, pParam4, pParam5, pParam6, pParam7, pParam8);
	}
	else if(pParam1 && pParam2 && pParam3 && pParam4 && pParam5 && pParam6 && pParam7)
	{
		sprintf_s(params, "%s %s %s %s %s %s %s", pParam1, pParam2, pParam3, pParam4, pParam5, pParam6, pParam7);
	}
	else if(pParam1 && pParam2 && pParam3 && pParam4 && pParam5 && pParam6)
	{
		sprintf_s(params, "%s %s %s %s %s %s", pParam1, pParam2, pParam3, pParam4, pParam5, pParam6);
	}
	else if(pParam1 && pParam2 && pParam3 && pParam4 && pParam5)
	{
		sprintf_s(params, "%s %s %s %s %s", pParam1, pParam2, pParam3, pParam4, pParam5);
	}
	else if(pParam1 && pParam2 && pParam3 && pParam4)
	{
		sprintf_s(params, "%s %s %s %s", pParam1, pParam2, pParam3, pParam4);
	}
	else if(pParam1 && pParam2 && pParam3)
	{
		sprintf_s(params, "%s %s %s", pParam1, pParam2, pParam3);
	}
	else if(pParam1 && pParam2)
	{
		sprintf_s(params, "%s %s", pParam1, pParam2);
	}
	else if(pParam1)
	{
		sprintf_s(params, "%s", pParam1);
	}
	else
	{
		params[0] = 0;
	}

	if(params[0])
	{
		DebugPrintTime();
		DebugPrint(" - Running Batch File \"%s\" with params \"%s\"\n", fullFilename, params);
	}
	else
	{
		DebugPrintTime();
		DebugPrint(" - Running Batch File \"%s\"\n", fullFilename);
	}


	SHELLEXECUTEINFO shellEx;
	shellEx.cbSize = sizeof(SHELLEXECUTEINFO);
	shellEx.fMask = SEE_MASK_NOCLOSEPROCESS;
	shellEx.hwnd = AppData.hWnd;
	shellEx.lpVerb = "open";
	shellEx.lpFile = fullFilename;
	shellEx.lpParameters = params;
	shellEx.lpDirectory = NULL;
	shellEx.nShow = SW_SHOW;
	shellEx.hInstApp = 0; // hInstance returned here
	shellEx.hProcess = 0; // hProcess returned here

	BOOL bOk = ShellExecuteEx(&shellEx);
	if(!bOk)
	{
		return 0;
	}

	return shellEx.hProcess;
}

// Changed.  This now calculates how many millisecs there are until the next build.
void CalcTimeUntilNextBuild()
{
	time_t t;
	time(&t);
	tm currentTime;
	localtime_s(&currentTime, &t);

	int iDeltaHrs = AppData.settings.iBuildHour24 - currentTime.tm_hour;
	if(iDeltaHrs < 0)
		iDeltaHrs += 24;

	int iDeltaMins = AppData.settings.iBuildMinute - currentTime.tm_min;
	if(iDeltaMins < 0)
		iDeltaMins += 60;

	if(iDeltaHrs==0 && iDeltaMins==0)
	{
		iDeltaHrs += 24;
	}

	AppData.iTimeUntilNextBuildMS = (iDeltaMins * 60000) + (iDeltaHrs * 3600000);

	DebugPrint("Time until next build is %u millisecs\n", AppData.iTimeUntilNextBuildMS);

	AppData.bTimerMsgReceived = FALSE;
	KillTimer(AppData.hWnd, SCHEDULED_BUILD_TIMER_ID);
	SetTimer(AppData.hWnd, SCHEDULED_BUILD_TIMER_ID, AppData.iTimeUntilNextBuildMS, NULL);
}

BOOL ShouldStartBuild()
{
	if(AppData.bForceStartBuildNow)
	{
		AppData.bForceStartBuildNow = FALSE;
		return TRUE;
	}

	if(AppData.bBuildIsCurrentlyActive || !AppData.bScheduledBuildEnabled)
	{
		return FALSE;
	}

	if(AppData.bTimerMsgReceived)
	{
		AppData.bTimerMsgReceived = FALSE;
		return TRUE;
	}

	return FALSE;
}

BOOL StartNavmeshBuild()
{
	AppData.eBuildStage = BS_None;
	AppData.bBuildIsCurrentlyActive = TRUE;
	AppData.iTimeBuildStartedMS = timeGetTime();

	DebugPrint("Started build at ");
	DebugPrintTime();
	DebugPrint("\n");

	DoNavMeshBuild();

	return TRUE;
}

BOOL DoNavMeshBuild()
{
	DebugPrint("Incremented build stage at ");
	DebugPrintTime();
	DebugPrint("\n");

	_ASSERTE(AppData.hBatchFileProcess==0);

	// Increment the build stage
	AppData.eBuildStage = (EBuildStage) ((int)AppData.eBuildStage+1);

	if(AppData.eBuildStage == BS_GetLatestTools && !AppData.settings.bGetLatestToolsFirst)
	{
		AppData.eBuildStage = BS_GetLatestData;
	}
	if(AppData.eBuildStage == BS_GetLatestData && !AppData.settings.bGetLatestDataFirst)
	{
		AppData.eBuildStage = BS_GetLatestExe;
	}
	if(AppData.eBuildStage == BS_GetLatestExe && !AppData.settings.bGetLatestExeFirst)
	{
		AppData.eBuildStage = BS_DeleteTempFiles;
	}

	switch(AppData.eBuildStage)
	{
		case BS_None:
			break;
		case BS_GetLatestTools:
			if(AppData.settings.bGetLatestToolsFirst)
			{
				// Get latest on tools
				AppData.hBatchFileProcess = RunBatchFile(AppData.settings.batchFileForGetLatestTools);
			}
			break;
		case BS_GetLatestExe:
			if(AppData.settings.bGetLatestExeFirst)
			{
				// Get latest game executable(s)
				AppData.hBatchFileProcess = RunBatchFile(AppData.settings.batchFileForGetLatestExe);
			}
			break;
		case BS_GetLatestData:
			if(AppData.settings.bGetLatestDataFirst)
			{
				// Get latest game map data
				AppData.hBatchFileProcess = RunBatchFile(AppData.settings.batchFileForGetLatestData);
			}
			break;
		case BS_DeleteTempFiles:
			// Now maybe delete all the temporary files from a previous build
			if(AppData.settings.bDeleteTempFilesBeforeBuild)
			{
				AppData.hBatchFileProcess = RunBatchFile(
					AppData.settings.batchFileForDelTempFiles,
					AppData.settings.pathForExport
				);
				break;
			}
			else
			{
				// Intentional fall-thru to next case statement below..
				AppData.hBatchFileProcess = 0;
				AppData.eBuildStage = BS_ExportingCollision;
			}
		case BS_ExportingCollision:
			if(AppData.settings.bDoExport)
			{
				// Now launch the game in a mode which will automatically export all the collision data

				if(AppData.settings.bWholeMapBuild)
				{
					char extraParams[MAX_BATCHFILENAME];
					sprintf_s(extraParams, "\"%s\"", AppData.settings.gameExtraParams);

					AppData.hBatchFileProcess = RunBatchFile(
						AppData.settings.batchFileForExportGeometry,
						AppData.settings.projectRoot,
						AppData.settings.exporterExeFile,
						AppData.settings.pathForExport,
						AppData.settings.navmeshesListFile,
						extraParams,
						NULL
					);
				}
				else if(AppData.settings.bContinueFrom)
				{
					char extraParams[MAX_BATCHFILENAME];
					sprintf_s(extraParams, "\"%s\"", AppData.settings.gameExtraParams);

					char region[256];
					sprintf_s(region, 256, "\"%i,%i\"", AppData.settings.iRegionMinX, AppData.settings.iRegionMinY);

					AppData.hBatchFileProcess = RunBatchFile(
						AppData.settings.batchFileForExportContinueFrom,
						AppData.settings.projectRoot,
						AppData.settings.exporterExeFile,
						AppData.settings.pathForExport,
						AppData.settings.navmeshesListFile,
						extraParams,
						region,
						NULL
						);
				}
				else if(AppData.settings.bDLC)
				{
					char extraParams[MAX_BATCHFILENAME];

					BOOL bIsMultiPlayerDLC = (IsDlgButtonChecked(AppData.hWnd, IDC_CHECK_MULTIPLAYER_DLC)==BST_CHECKED);

					if(bIsMultiPlayerDLC)
					{
						sprintf_s(extraParams, "\"%s -MP_DLC\"", AppData.settings.gameExtraParams);
					}
					else
					{
						sprintf_s(extraParams, "\"%s\"", AppData.settings.gameExtraParams);
					}

					char extraContent[MAX_BATCHFILENAME];
					sprintf_s(extraContent, MAX_BATCHFILENAME, "\"-extracontent=%s\"", AppData.settings.DLC_extraContentParams);

					AppData.hBatchFileProcess = RunBatchFile(
						AppData.settings.batchFileForExportDLC,
						AppData.settings.projectRoot,
						AppData.settings.exporterExeFile,
						AppData.settings.pathForExport,
						AppData.settings.navmeshesListFile,
						extraParams,
						AppData.settings.DLC_areasXML,
						extraContent
						);
				}
				else
				{
					char extraParams[MAX_BATCHFILENAME];
					sprintf_s(extraParams, "\"%s\"", AppData.settings.gameExtraParams);

					_ASSERTE(AppData.settings.bRegion);

					char region[256];
					sprintf_s(region, 256, "\"%i,%i,%i,%i\"", AppData.settings.iRegionMinX, AppData.settings.iRegionMinY, AppData.settings.iRegionMaxX, AppData.settings.iRegionMaxY);

					AppData.hBatchFileProcess = RunBatchFile(
						AppData.settings.batchFileForExportRegion,
						AppData.settings.projectRoot,
						AppData.settings.exporterExeFile,
						AppData.settings.pathForExport,
						AppData.settings.navmeshesListFile,
						extraParams,
						region,
						NULL
					);
				}

				AppData.iTimeAtStartOfExportStageMS = timeGetTime();

				break;
			}
			else
			{
				// Intentional fall-thru to next case statement below..
				AppData.hBatchFileProcess = 0;
				AppData.eBuildStage = BS_CompilingNavmeshes;
			}
		case BS_CompilingNavmeshes:
			if(AppData.settings.bDoCompile)
			{
				// Wait for the collision export to finish & start navmesh build running
				AppData.hBatchFileProcess = RunBatchFile(
					AppData.settings.batchFileForScheduledCompile,
					AppData.settings.pathForExport,
					AppData.settings.batchFileForCompileNavmeshes,
					AppData.settings.navmeshesListFile
				);
				break;
			}
			else
			{
				// Intentional fall-thru to next case statement below..
				AppData.hBatchFileProcess = 0;
				AppData.eBuildStage = BS_Finished;
			}
		case BS_JoinDLC:
			if(AppData.settings.bDLC)
			{
				AppData.hBatchFileProcess = RunBatchFile(
					AppData.settings.batchFileForJoinDLC,
					AppData.settings.projectRoot,
					AppData.settings.exporterExeFile,
					AppData.settings.pathForExport,
					AppData.settings.DLC_finalOutputFolder
				);
			}
			else
			{
				// Intentional fall-thru to next case statement below..
				AppData.hBatchFileProcess = 0;
				AppData.eBuildStage = BS_Finished;
			}
		case BS_Finished:
			// If build was successful, then check files back into Alienbrain, etc.
			AppData.hBatchFileProcess = 0;

			if(AppData.settings.bCheckNavmeshesIntoPerforce)
			{
				FinaliseIfBuildIsComplete();
			}
			AppData.bBuildIsCurrentlyActive = FALSE;

			DebugPrint("Finished build at ");
			DebugPrintTime();
			DebugPrint("\n");

			break;
	}

	char * pWindowTitle = ConstructWindowTitle();
	SetWindowText(AppData.hWnd, pWindowTitle);

	return TRUE;
}

BOOL IsCurrentlyBuilding()
{
	return AppData.bBuildIsCurrentlyActive;
}

BOOL AbortBuildIfTakenTooLong(BOOL bForceAbort)
{
	UINT32 iDeltaMs = timeGetTime() - AppData.iTimeBuildStartedMS;
	iDeltaMs /= 1000;	//secs
	iDeltaMs /= 60;		//hours

	if(iDeltaMs > MAX_NUM_HOURS_FOR_BUILD_TO_COMPLETE || bForceAbort)
	{
		DebugPrint("Aborting build at ");
		DebugPrintTime();
		if(!bForceAbort)
			DebugPrint(" - it took too long");
		DebugPrint("\n");
		
		AppData.bBuildIsCurrentlyActive = FALSE;

		return TRUE;
	}

	return FALSE;
}


BOOL CheckFileExistsInExportFolder(const char * pFilename)
{
	char testFile[MAX_BATCHFILENAME];
	sprintf_s(testFile, "%s\\%s", AppData.settings.pathForExport, pFilename);

	FILE * pFile=NULL;

	fopen_s(&pFile, testFile, "rt");
	if(!pFile)
		return FALSE;
	fclose(pFile);
	return TRUE;
}

BOOL DoesNavMeshesImageAppearValid()
{
	char navmeshesFile[MAX_BATCHFILENAME];
	sprintf_s(navmeshesFile, "%s\\navmeshes.zip", AppData.settings.pathForExport);

	FILE * pFile=NULL;
	fopen_s(&pFile, navmeshesFile, "rb");
	if(!pFile)
	{
		// navmeshes.img file didn't exist, so try navmeshes0.img
		// we had to change the navmesh system to use 10 RPF files
		// due to restrictions on the number of files in an RPF
		sprintf_s(navmeshesFile, "%s\\navmeshes0.zip", AppData.settings.pathForExport);

		pFile=NULL;
		fopen_s(&pFile, navmeshesFile, "rb");

		if(!pFile)
			return FALSE;
	}

	fseek(pFile, 0, SEEK_END);
	int iApproxFileSize = ftell(pFile);
	fclose(pFile);

	if(iApproxFileSize < AppData.settings.iMinNavmeshesImgSizeForSuccessfulBuildKb*1024)
	{
		return FALSE;
	}

	return TRUE;
}

BOOL FinaliseIfBuildIsComplete()
{
	//***********************************************************************
	// Check for the existence of "compile_complete.txt" in the build folder

	if(!CheckFileExistsInExportFolder("compile_complete.txt"))
		return FALSE;

	//********************************************************************************
	// Examine the navmeshes.img file to see what size it is.
	// If it is over the minimum filesize, then proceed to check in into Alienbrain.
	// If not, then there was probably an error during the export or compile.

	if(!DoesNavMeshesImageAppearValid())
	{
		// navmeshes.img file either didn't exist or was suspiciously small, so abort
		AbortBuildIfTakenTooLong(TRUE);
		return TRUE;
	}

	//*********************************************************************************
	// Run the batch file which checks navmeshes.zip & navnodes.zip back into Perforce

	char navnodesFile[MAX_BATCHFILENAME];
	sprintf_s(navnodesFile, "%s\\navnodes.zip", AppData.settings.pathForExport);
	char navnodesTargetFilename[MAX_BATCHFILENAME];
	sprintf_s(navnodesTargetFilename, MAX_BATCHFILENAME, "%s\\navnodes.zip", AppData.settings.navmeshesImgPath);

	char audmeshesFile[MAX_BATCHFILENAME];
	sprintf_s(audmeshesFile, "%s\\audmeshes.zip", AppData.settings.pathForExport);
	char audmeshesTargetFilename[MAX_BATCHFILENAME];
	sprintf_s(audmeshesTargetFilename, "%s\\audmeshes.zip", AppData.settings.navmeshesImgPath);

	char navIndexMappingFile[MAX_BATCHFILENAME];
	sprintf_s(navIndexMappingFile, "%s\\NavmeshIndexMapping.dat", AppData.settings.pathForExport);
	char navIndexMappingTargetFilename[MAX_BATCHFILENAME];
	sprintf_s(navIndexMappingTargetFilename, "%s\\NavmeshIndexMapping.dat", AppData.settings.levelCommonPath);


	// Checkout, overwrite & submit navmeshes
	// They will be checked-out & submitted in a single CL to avoid the asset builder's
	// seemingly ad hoc skipping/ignoring of changelists

	char navmeshesTargetPath[MAX_BATCHFILENAME];
	sprintf_s(navmeshesTargetPath, MAX_BATCHFILENAME, "%s", AppData.settings.navmeshesImgPath);
	char navmeshesSourcePath[MAX_BATCHFILENAME];
	sprintf_s(navmeshesSourcePath, "%s", AppData.settings.pathForExport);

	RunBatchFile(
		AppData.settings.batchFileForCheckInNavmeshes,
		navmeshesTargetPath,
		navmeshesSourcePath
	);

	RunBatchFile(
		AppData.settings.batchFileForCheckInMapping,
		navIndexMappingTargetFilename,
		navIndexMappingFile
	);

	AbortBuildIfTakenTooLong(TRUE);

	return TRUE;
}
