
#define LARGE_BUDDY_HEAP

#if __WIN32PC

#define SIMPLE_HEAP_SIZE		(200*1024)
#define SIMPLE_PHYSICAL_SIZE	(352*1024)

#else

#error "Not supported for this platform.")

#endif	// __WIN32PC

#include "fwnavgen/main.h"

#include "NavGenTest_Win32.h"
#include <time.h>

// JB: Including cpp file here because of linker errors
// *sigh* NavGenTest has become so fucked up now that its just a matter of keeping it
// stitched together until GTA5 has shipped..
// After that we'll need to write a new navmesh testbed from the ground up.
#include "ai\navmesh\pathserverthread.cpp"

#define USE_STOCK_ALLOCATOR

//#define SIMPLE_HEAP_SIZE	(512*1024)

//Rage headers
#include "bank/bkmgr.h"
#include "bank/bank.h"
#include "system/main.h"
#include "parser/manager.h"
#include "parser/visitorutils.h"
#include "phcore/materialmgrimpl.h"
#include "phcore/surface.h"
#include "system/FileMgr.h"
#include "system/Param.h"

#include "fwMaths/random.h"
#include "fwnavgen/config.h"
#include "fwnavgen/NavMeshMaker.h"
#include "pathserver/PathServer.h"
#include "ai/navmesh/pathserverbase.h"
#include "ai/navmesh/tessellation.h"

#include <crtdbg.h>
#include <direct.h>

char * g_pGameFolder = "X:\\gta5\\build\\dev";

CNavGenApp * g_App = NULL;
static const float DESIRED_FRAMERATE = 60.0f;
static const float fWorldLimitSize = FLT_MAX;	//350.0f;	// 64.0f

namespace rage
{
	bool g_bAerialNavigation = false;
}

using namespace rage;

// Maximum number of verts in ColPolys vertex-buffer
//#define MAX_NUM_COLPOLY_VB_VERTS	(3 * 128)
//#define MAX_NUM_LINESEG_VB_VERTS	(2 * 128)
#define MAX_NUM_COLPOLY_VB_VERTS	(3 * 65535)
#define MAX_NUM_LINESEG_VB_VERTS	(4 * 131071)



bool CNavGenApp::m_bWanderPath = false;
bool CNavGenApp::m_bFleePath = false;
bool CNavGenApp::m_bCoverPath = false;
bool CNavGenApp::m_bPreservePathHeights = true;
bool CNavGenApp::m_bNeverClimb = false;
bool CNavGenApp::m_bNeverDrop = false;
bool CNavGenApp::m_bNeverUseLadders = false;
bool CNavGenApp::m_bSingleStepPathFinding = false;
bool CNavGenApp::ms_bDetessellatePriorToPathSearch = false;
bool CNavGenApp::m_bSmallObject = false;
bool CNavGenApp::m_bExhaustiveTesting = false;
float CNavGenApp::ms_fCompletionRadius = 1.0f;
float CNavGenApp::ms_fActorRadius = PATHSERVER_PED_RADIUS;
float CNavGenApp::m_fNavMeshPathReferenceDistance = 30.0f;
float CNavGenApp::ms_fMaxAngleNavigable = 0.0f;
bool CNavGenApp::ms_bFavourEnclosedAreas = false;

bool CNavGenApp::ms_bAvoidObjects = true;
bool CNavGenApp::ms_bReduceBBoxes = false;
bool CNavGenApp::ms_bIgnoreObjectsWithNoUprootLimit = false;

bool CNavGenApp::m_bAdjacenciesUnderStartMarker = false;
bool CNavGenApp::m_bPathParentPolys = false;
bool CNavGenApp::m_bDrawTessellationNavMesh = true;
bool CNavGenApp::m_bDrawSpecialLinks = true;
bool CNavGenApp::m_bDrawLowClimbs = true;
bool CNavGenApp::m_bDrawHighClimbs = true;
bool CNavGenApp::m_bDrawDropDowns = true;
bool CNavGenApp::m_bDrawHierarchical = true;
bool CNavGenApp::m_bDrawPolyMinMaxUnderStartMarker = false;
bool CNavGenApp::m_bDrawMinMaxOfObjects = false;
bool CNavGenApp::m_bDrawObjectsGridCellMinMax = false;
bool CNavGenApp::m_bDrawPolyCentroids = false;
bool CNavGenApp::m_bDrawPolyAddresses = false;
bool CNavGenApp::m_bDrawPolyCosts = false;
bool CNavGenApp::m_bDrawPolyDistanceTravelled = false;
bool CNavGenApp::m_bDrawPointsInPolys = false;
bool CNavGenApp::m_bDrawPolyParents = false;
bool CNavGenApp::m_bRenderPathStats = false;
bool CNavGenApp::m_bDrawFreeSpaceBeyondPolyEdges = false;
bool CNavGenApp::m_bDrawFreeSpaceAroundVertices = false;
bool CNavGenApp::m_bMarkStartingPolys = false;
bool CNavGenApp::ms_bDebugSurroundingPolysToo = false;

TNavMeshPoly * CNavGenApp::ms_pDebugPoly = NULL;
bool CNavGenApp::m_bDrawDebugPoly = false;
bool CNavGenApp::m_bDrawDebugPolyTraceback = false;
bool CNavGenApp::m_bDumpDebugPolyTraceback = false;

char CNavGenApp::ms_DebugPolyEditWidget[256] = { 0 };

// Generation Params
float CNavGenApp::m_fDistanceToSeedNodes = 1.0f;
float CNavGenApp::m_TriangulationMaxHeightDiff = 1.5f;
float CNavGenApp::m_HeightAboveNodeBaseAtWhichToTriangulate = 0.35f;
float CNavGenApp::m_MaxHeightChangeUnderTriangleEdge = 0.3f;
bool CNavGenApp::m_bFixJaggiesAfterTriangulation = true;
bool CNavGenApp::m_bMoveNodesAwayFromWalls = true;
bool CNavGenApp::m_bUseTestSlicePlane = false;
float CNavGenApp::m_fTestSlicePlaneZ = 400.0f;
bool CNavGenApp::ms_bRemoveTrisBehindWalls = true;
float CNavGenApp::m_fTestClearHeightInTriangulation = -1.0f;

CNavGenApp::CNavGenApp(void)
{
	ClearMembers();

	//sprintf(m_navMeshesFolder, "X:\\gta5\\exported_navmeshes\\gta5");
	//sprintf(m_navMeshesFolder, "X:\\gta5\\exported_navmeshes\\climbtest");
	//sprintf(m_navMeshesFolder, "X:\\gta5\\exported_navmeshes\\AA-off");
	//sprintf(m_navMeshesFolder, "X:\\gta5\\exported_navmeshes\\testbed");
	//sprintf(m_navMeshesFolder, "X:\\gta5\\exported_navmeshes\\fleePavement");
	sprintf(m_navMeshesFolder, "X:\\gta5\\exported_navmeshes\\shitFlee");

	sprintf(m_gameDataFolder, "X:\\gta5\\build\\dev\\common\\data");
}

CNavGenApp::~CNavGenApp(void)
{
	Shutdown();
}

void
CNavGenApp::ClearMembers(void) 
{
	//**************************************************
	// Win32 & system variables
	//**************************************************

	m_Hinstance = 0;
	m_Hwnd = 0;

	m_ScreenWidth = 1024;
	m_ScreenHeight = 768;
	m_WindowLeftEdge = 0;
	m_WindowTopEdge = 0;

	m_SecsThisFrame = 0.0f;
	m_bHasLoadedUp = false;

	for(int i=0; i<256; i++)
	{
		m_Keys[i] = m_LastKeys[i] = 0;
	}

	m_bMouseClicked = false;
	m_bMouseHeld = false;
	m_bCtrlDown = false;
	m_iSelectedTri = -1;
	m_pSelectedNavSurfaceTri = NULL;
	m_pDestinationNavSurfaceTri = NULL;
	m_pPathTriRenderVerts = NULL;
	m_iNumPathTriRenderVerts = 0;
	m_pNavSurfaceVertexBuffer = NULL;
	m_iNumTrianglesInNavSurfaceVB = 0;
	m_bNavSurfaceHasChanged = false;

	m_DynamicNavMesh = NULL;

	m_pVoxelMapVertexBuffer = NULL;
	m_iNumTrianglesInVoxelMapVB = 0;

	m_iDrawNavMeshSinglePolyIndex = NAVMESH_POLY_INDEX_NONE;

	//**************************************************
	// DirectInput stuff
	//**************************************************

	m_pDirectInput8 = NULL;
	m_pMouseDevice = NULL;

	//**************************************************
	// D3D stuff
	//**************************************************

	m_pD3D9 = NULL;
	m_pD3DDevice = NULL;
	ZeroMemory(&m_D3DPresentParams, sizeof(_D3DPRESENT_PARAMETERS_));
	ZeroMemory(&m_D3DViewPort, sizeof(D3DVIEWPORT9));
	m_ViewPortWidth = m_ScreenWidth;
	m_ViewPortHeight = m_ScreenHeight;
	m_pD3DXFont = NULL;

	m_pCollisionPolysVB = NULL;
	m_iNumVertsInCollisionPolyVB = 0;
	m_iMaxNumVertsInCollisionPolyVB = 0;
	m_pCollisionPolyVertexData = NULL;

	m_pLineSegVB = NULL;
	m_iNumVertsInLineSegVB = 0;
	m_iMaxNumVertsInLineSegVB = 0;
	m_pLineSegVertexData = NULL;

	m_FOV = D3DXToRadian(75.0f);

	m_iNumNavMeshTrisRenderedThisFrame = 0;

	//**************************************************
	// D3D helper functions, etc
	//**************************************************

	for(int t=0; t<8; t++)
	{
		m_iColourOp[t]		= 0;
		m_iColourArg1[t]	= 0;
		m_iColourArg2[t]	= 0;
		m_iAlphaOp[t]		= 0;
		m_iAlphaArg1[t]		= 0;
		m_iAlphaArg2[t]		= 0;
	}

	m_bPrintRenderInfo = false;

	//**************************************************
	// Node-Gen stuff
	//**************************************************

	m_bDrawCollisionPolysDumbass = true;//false;
	m_bDrawOctreeBounds = true;
	m_bDrawCollisionPolys = true;
	m_bDrawWater = false;
	m_bDrawPathNodes = false;
	m_bDrawNavigationSurfaces = false;
	m_bToggleNavigationSurfaceSolid = false;
	m_bDisplayPathServerMesh = true;
	m_bShadeSurfaceTypes = true;

#ifdef DEBUG_OCTREE_CONSTRUCTION
	m_bDrawSplitPolys = true;
#else
	m_bDrawSplitPolys = false;
#endif

	// Get high freq timer resolution, if available
	m_bHasHighFreqTimer = QueryPerformanceFrequency(&m_iPerformanceCounterFrequency);
	DWORD64 freq = m_iPerformanceCounterFrequency.QuadPart;
	// I'm let to believe that QPC shouldn't be trusted, if not one of the following frequencies
	if(freq != 1193182 && freq  != 3579545)
	{
		m_bHasHighFreqTimer = false;
	}
	else
	{
		m_fHighFreqTimerFrequency = (double)freq;
	}


	m_vViewPos = Vector3(-1150.0, 702.0, 291.0);
	m_mViewOrientation.Identity();

	m_fNearPlaneDistance = 1.0f;
	m_fFarPlaneDistance = 256.0f;//128.0f;

	// no restriction
//	m_vRestrictMin = Vector3(-FLT_MAX, -FLT_MAX, -FLT_MAX);
//	m_vRestrictMax = Vector3(FLT_MAX, FLT_MAX, FLT_MAX);
	m_vRestrictMin = m_vViewPos - Vector3(fWorldLimitSize, fWorldLimitSize, fWorldLimitSize);
	m_vRestrictMax = m_vViewPos + Vector3(fWorldLimitSize, fWorldLimitSize, fWorldLimitSize);
//	m_vRestrictMin = m_vViewPos - Vector3(500.0f, 500.0f, 500.0f);
//	m_vRestrictMax = m_vViewPos + Vector3(500.0f, 500.0f, 500.0f);
//	m_vRestrictMin = m_vViewPos - Vector3(200.0f, 200.0f, 200.0f);
//	m_vRestrictMax = m_vViewPos + Vector3(200.0f, 200.0f, 200.0f);

	m_pSingleNavMesh = NULL;
	m_pSingleNavMeshTrianglesVB = NULL;
	m_iSingleNavMeshNumTriangles = 0;
	m_pSingleNavMeshLinesVB = NULL;
	m_iSingleNavMeshNumLines = 0;

	m_bPathServerInitialised = false;
	m_bPavements = false;

	ZeroMemory(&m_NavMeshVertexBuffers, sizeof(CNavMeshVertexBuffers) * MAX_NUM_NAVMESH_VBS);
	ZeroMemory(&m_DynamicNavMeshVertexBuffer, sizeof(CNavMeshVertexBuffers));

	m_vNavMeshPathSearchStartPos = Vector3(0,0,0);
	m_vNavMeshPathSearchEndPos = Vector3(0,0,0);
	m_iNavMeshNumPathPoints = 0;

	m_iNavMeshNumPointsInPolys = 0;
	m_iNavMeshNumTriPts = 0;

	m_bExhaustiveTesting = false;
	m_vExhaustiveTestOrigin = Vector3(0,0,0);
	m_fExhaustiveRadius = 0.0f;
	m_hExhaustiveTestingPathHandle = PATH_HANDLE_NULL;

	m_pSelDynObj = NULL;

	m_CursorMode = eCursorMovesSelectedObject;

	//g_App->m_NodeGenerator.m_fMinimumVerticalDistanceBetweenNodes = 1.0f;

	CPathServer::m_pNavMeshStores[0] = new aiNavMeshStore(NULL, 0, 0, false, false, 0);
}


//****************************************************************
//	CNavGenApp::Init
//****************************************************************

bool
CNavGenApp::Init(HINSTANCE instance)
{
	RAGE_LOG_DISABLE = 1;

	m_Hinstance = instance;

	//*********************************
	// Win32
	//*********************************
	
	if(!InitWindow())
	{
		MessageBox(m_Hwnd, "Couldn't create window.", "Error", MB_OK | MB_ICONWARNING);
		return false;
	}

	m_bMouseLook = true;
	//SetCapture(m_Hwnd);
	//ConfineCursorToWindow(true);

	//*********************************
	// DirectInput
	//*********************************

	if(!InitDirectInput())
	{
		MessageBox(m_Hwnd, "Couldn't create DirectInput8.", "Error", MB_OK | MB_ICONWARNING);
		return false;
	}

	//*********************************
	// D3D
	//*********************************
	
	HRESULT retval = S_OK;

	if(!InitD3D())
	{
		MessageBox(m_Hwnd, "Couldn't init Direct3D 9.", "Error", MB_OK | MB_ICONWARNING);
		return false;
	}
	
	// Get device caps
	m_pD3DDevice->GetDeviceCaps(&m_D3DCaps);

	// Create a default projection matrix
//	D3DXMatrixPerspectiveFovRH(

	D3DXMatrixPerspectiveFovLH(
		&m_ProjectionMatrix,
		m_FOV,
		0.75f,
		m_fNearPlaneDistance,
		m_fFarPlaneDistance
	);
	m_pD3DDevice->SetTransform(D3DTS_PROJECTION, &m_ProjectionMatrix);
	
	// Set some initial device states
	m_pD3DDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
	m_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
	m_pD3DDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ZERO);
	m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
	m_pD3DDevice->SetRenderState(D3DRS_LIGHTING, FALSE);

	m_SrcBlend = D3DBLEND_ONE;
	m_DestBlend = D3DBLEND_ZERO;
	m_bAlphaBlendEnabled = false;

	m_ClearCol = D3DCOLOR_COLORVALUE(0.1f, 0.3f, 0.6f, 1.0f);
	m_FogCol = D3DCOLOR_COLORVALUE(0.1f, 0.3f, 0.6f, 1.0f);
	//m_FogCol = D3DCOLOR_COLORVALUE(0, 0, 0, 1.0f);

	m_bUseFogging = true;
	m_bPixelFog = (m_D3DCaps.RasterCaps & D3DPRASTERCAPS_FOGTABLE);

	if(m_bUseFogging)
	{
		SetFogging();
	}
	
	// set up texture stage states
	for(int s=0; s<8; s++)
	{
		m_iColourOp[s] = D3DTOP_DISABLE;
		m_iColourArg1[s] = D3DTA_CURRENT;
		m_iColourArg2[s] = D3DTA_CURRENT;
		
		m_iAlphaOp[s] = D3DTOP_DISABLE;
		m_iAlphaArg1[s] = D3DTA_CURRENT;
		m_iAlphaArg2[s] = D3DTA_CURRENT;
	}
	SetColourStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);
	SetAlphaStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);
	SetColourStage(1, D3DTOP_DISABLE, 0, 0);
	SetAlphaStage(1, D3DTOP_DISABLE, 0, 0);	
	
	m_pD3DDevice->GetViewport(&m_D3DViewPort);

	// Create a font
//	HFONT hFont = (HFONT)GetStockObject(DEFAULT_GUI_FONT);

	retval = D3DXCreateFont(
		m_pD3DDevice,
		8, 8, 1,
		1,
		FALSE,
		ANSI_CHARSET,
		OUT_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		FF_DONTCARE,
		"Courier",
		&m_pD3DXFont
	);

	m_pD3DDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW);


	//**********************************************
	// Create all the vertex buffers we may need
	//**********************************************


	// VB for collision polys
	retval = m_pD3DDevice->CreateVertexBuffer(
		sizeof(LitVertex) * MAX_NUM_COLPOLY_VB_VERTS,
		D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY,
		LitVertexFVF,
		D3DPOOL_DEFAULT,
		&m_pCollisionPolysVB,
		NULL
	);

	m_iMaxNumVertsInCollisionPolyVB = MAX_NUM_COLPOLY_VB_VERTS;


	// VB for line segments
	retval = m_pD3DDevice->CreateVertexBuffer(
		sizeof(LitVertex) * MAX_NUM_LINESEG_VB_VERTS,
		D3DUSAGE_DYNAMIC | D3DUSAGE_WRITEONLY,
		LitVertexFVF,
		D3DPOOL_DEFAULT,
		&m_pLineSegVB,
		NULL
	);

	m_iMaxNumVertsInLineSegVB = MAX_NUM_LINESEG_VB_VERTS;

	//*********************************
	// App stuff
	//*********************************

	InitWidgets();

	/*
	Enter app
	*/

	ConfineCursorToWindow(true);
	m_bHasLoadedUp = true;
	
//	ShowCursor(FALSE);
	int exitcode = MsgLoop();

	/*
	Exit app
	*/
	
//	ShowCursor(TRUE);
	return exitcode;
}

bool
CNavGenApp::Shutdown(void)
{
	CPathServer::Shutdown();


	if(m_pSingleNavMesh)
	{
		delete m_pSingleNavMesh;
		m_pSingleNavMesh = NULL;
	}

	if(m_DynamicNavMesh)
		delete m_DynamicNavMesh;

	for(int n=0; n<MAX_NUM_NAVMESH_VBS; n++)
	{
		m_NavMeshVertexBuffers[n].Release();
	}
	m_DynamicNavMeshVertexBuffer.Release();

	if(m_pD3DXFont)
	{
		m_pD3DXFont->Release();
		m_pD3DXFont = NULL;
	}

	if(m_pPathTriRenderVerts)
	{
		delete[] m_pPathTriRenderVerts;
		m_pPathTriRenderVerts = NULL;
	}

	ShutdownD3D();

	ShutdownDirectInput();

	return true;
}

void
CNavGenApp::ToggleExhaustiveTesting()
{
	// Start exhaustive testing of random paths based around the area where the markers are placed.
	// The start marker is the origin of a circle, and the end marker defines the radius.  Within this
	// random paths will repeatedly be found until a path fails.

	g_App->m_bExhaustiveTesting = !g_App->m_bExhaustiveTesting;

	if(g_App->m_bExhaustiveTesting)
	{
		g_App->m_vExhaustiveTestOrigin = g_App->m_vNavMeshPathSearchStartPos;
		g_App->m_fExhaustiveRadius = (g_App->m_vNavMeshPathSearchStartPos - g_App->m_vNavMeshPathSearchEndPos).XYMag();
		g_App->m_hExhaustiveTestingPathHandle = 0;
	}
	else
	{
		if(g_App->m_hExhaustiveTestingPathHandle != PATH_HANDLE_NULL)
		{
			CPathServer::CancelRequest(g_App->m_hExhaustiveTestingPathHandle);
			g_App->m_hExhaustiveTestingPathHandle = PATH_HANDLE_NULL;
		}
	}
}

CPathRequest *
CNavGenApp::GetLastPath()
{
	TPathHandle iHighestHandle = PATH_HANDLE_NULL;
	CPathRequest * pLastReq = NULL;

	for(int i=0; i<MAX_NUM_PATH_REQUESTS; i++)
	{
		CPathRequest * pReq = &CPathServer::m_PathRequests[i];
		TPathHandle hPath = pReq->m_hHandleThisWas;

		if(hPath >= iHighestHandle)
		{
			iHighestHandle = hPath;
			pLastReq = pReq;
		}
	}

	return pLastReq;
}

void
CNavGenApp::SaveLastPathAsProbFile()
{
#if __BANK
	CPathRequest * pLastReq = GetLastPath();

	if(pLastReq)
	{
		CPathServer::OutputPathRequestProblem(pLastReq);
	}
#endif
}

void
CNavGenApp::LoadPathProbFile()
{
#if __BANK
	char filename[512];
	char * pPath = g_App->SelectFileToOpen("PathFinding Problem Files..", "*.prob");
	if(!pPath)
		return;
	strcpy(filename, pPath);

	CPathServer::LoadPathRequestProblem(filename);

	g_App->m_vNavMeshPathSearchStartPos = CPathServer::m_PathRequests[0].m_vUnadjustedStartPoint;
	g_App->m_vNavMeshPathSearchEndPos = CPathServer::m_PathRequests[0].m_vUnadjustedEndPoint;
	g_App->m_fNavMeshPathReferenceDistance = CPathServer::m_PathRequests[0].m_fReferenceDistance;

	// If this is a wander path, then adjust the path-end by the reference vector - this is how
	// a wander path's direction is determined in NavGenTest app.
	if(CPathServer::m_PathRequests[0].m_bWander)
	{
		g_App->m_vNavMeshPathSearchEndPos += CPathServer::m_PathRequests[0].m_vReferenceVector;
	}
#endif

	g_App->m_vViewPos = (g_App->m_vNavMeshPathSearchStartPos + g_App->m_vNavMeshPathSearchEndPos) * 0.5f;
}

void CNavGenApp::RepeatLastPathRequest()
{
	g_App->ClearAllPolyCosts();

	CPathRequest * pRequest = &CPathServer::m_PathRequests[0];
	//CPathServer::m_PathServerThread.UpdateAllDynamicObjectsPositions();

	pRequest->m_fDistanceBelowStartToLookForPoly = CPathServerThread::ms_fNormalDistBelowToLookForPoly;
	pRequest->m_fDistanceAboveStartToLookForPoly = CPathServerThread::ms_fNormalDistAboveToLookForPoly;
	pRequest->m_fDistanceBelowEndToLookForPoly = CPathServerThread::ms_fNormalDistBelowToLookForPoly;
	pRequest->m_fDistanceAboveEndToLookForPoly = CPathServerThread::ms_fNormalDistAboveToLookForPoly;

	pRequest->m_StartNavmeshAndPoly.Reset();

	pRequest->m_bComplete = false;
	pRequest->m_bSlotEmpty = false;
	pRequest->m_bRequestActive = false;
	pRequest->m_bWasAborted = false;
	pRequest->m_bRequestPending = true;
	pRequest->m_hHandle = 0x01;

	pRequest->m_fReferenceDistance = pRequest->m_fInitialReferenceDistance;

	CPathServer::m_PathServerThread.CalculateSearchExtents( pRequest, CPathServer::m_PathServerThread.m_Vars.m_PathSearchDistanceMinMax, CPathServer::m_PathServerThread.m_Vars.m_vSearchExtentsMin, CPathServer::m_PathServerThread.m_Vars.m_vSearchExtentsMax );


//	pRequest->m_vPathStart = g_App->m_vNavMeshPathSearchStartPos;
//	pRequest->m_vPathEnd = g_App->m_vNavMeshPathSearchEndPos;


	while(!CPathServer::IsRequestResultReady(0x01))
	{
		Sleep(0);
	}

	g_App->ProcessRequestResult(0x01);
}

void CNavGenApp::ProcessRequestResult(s32 iPathHandle)
{
	Vector3 * pPts;
	TNavMeshWaypointFlag * pWptFlags;
	EPathServerErrorCode iCompletionCode = CPathServer::LockPathResult(iPathHandle, &m_iNavMeshNumPathPoints, pPts, pWptFlags);

	m_vNavMeshPathPoints.Reset();
	m_iNavMeshPathWaypointFlags.Reset();
	m_vNavMeshPolyCentroids.Reset();
	m_iNavMeshNumTriPts = 0;

	if(iCompletionCode == PATH_FOUND)
	{
		for(int p=0; p<m_iNavMeshNumPathPoints; p++)
		{
			m_vNavMeshPathPoints.PushAndGrow(pPts[p]);
			m_iNavMeshPathWaypointFlags.PushAndGrow(pWptFlags[p]);
		}

		// HACK. To get the path polys & points for later.
		for(int i=0; i<MAX_NUM_PATH_REQUESTS; i++)
		{
			CPathRequest * pReq = &CPathServer::m_PathRequests[i];
			if(pReq->m_hHandle == (u32)iPathHandle)
			{
				m_iNavMeshNumTriPts = 0;
				m_iNavMeshNumPointsInPolys = pReq->m_iNumInitiallyFoundPolys;

				for(u32 p=0; p<pReq->m_iNumInitiallyFoundPolys; p++)
				{
					m_vNavMeshPointsInPolys[p] = pReq->m_InitiallyFoundPathPointInPolys[p];
					TNavMeshPoly * pPoly = pReq->m_InitiallyFoundPathPolys[p];
					if(pPoly)
					{
						CNavMesh * pNavMesh = CPathServer::GetNavMeshFromIndex(pPoly->GetNavMeshIndex(), kNavDomainRegular);

						Vector3 v0,v1,v2;
						pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, 0), v0);

						for(u32 v=1; v<pPoly->GetNumVertices()-1; v++)
						{
							pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, v), v1);
							pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, v+1), v2);

							m_vNavMeshPolyTriVerts[ m_iNavMeshNumTriPts++ ] = v2;
							m_vNavMeshPolyTriVerts[ m_iNavMeshNumTriPts++ ] = v1;
							m_vNavMeshPolyTriVerts[ m_iNavMeshNumTriPts++ ] = v0;

							if(m_iNavMeshNumTriPts >= 4096-3)
								break;
						}
					}
				}

				for(int p=0; p<m_iNavMeshNumPathPoints; p++)
				{
					if(pReq->m_PathPolys[p])
					{
						CNavMesh * pPlyMesh = CPathServer::GetNavMeshFromIndex(pReq->m_PathPolys[p]->GetNavMeshIndex(), kNavDomainRegular);
						Assert(pPlyMesh);
						Vector3 vCentroid;
						pPlyMesh->GetPolyCentroid(pPlyMesh->GetPolyIndex(pReq->m_PathPolys[p]), vCentroid);
						m_vNavMeshPolyCentroids.PushAndGrow( vCentroid );
					}
					else
					{
						m_vNavMeshPolyCentroids.PushAndGrow( pPts[p] );
					}
				}

				break;
			}
		}
	}

	CPathServer::UnlockPathResultAndClear(iPathHandle);

	MakeVertexBuffersForAllNavMeshes();
	MakeVertexBufferForTessellationNavMesh();
}

void NavGenApp_CreateObject()
{

}

void
CNavGenApp::RunLastPathOneHundredTimes()
{
	if(!g_App->m_bPathServerInitialised)
		return;

	CPathRequest * pLastReq = GetLastPath();

	if(!pLastReq)
		return;

	CPathServer::m_PathServerThread.UpdateAllDynamicObjectsPositions();

	Vector3 vPlayerOrigin = (g_App->m_vNavMeshPathSearchStartPos + g_App->m_vNavMeshPathSearchEndPos) / 2.0f;
	CPathServer::Process(vPlayerOrigin);


	float fAverageTotalTime = 0.0f;
	float fAverageFindPolysTime = 0.0f;
	float fAverageDoPolygonPathTime = 0.0f;
	float fAverageRefinePathTime = 0.0f;
	float fAverageMinimiseDistTime = 0.0f;

	s32 iNumRepeats = 100;
	for(int p=0; p<iNumRepeats; p++)
	{
		pLastReq->m_bComplete = false;
		pLastReq->m_bSlotEmpty = false;
		pLastReq->m_bRequestActive = false;
		pLastReq->m_bWasAborted = false;
		pLastReq->m_bRequestPending = true;
		pLastReq->m_hHandle = pLastReq->m_hHandleThisWas;

		//	pRequest->m_vPathStart = g_App->m_vNavMeshPathSearchStartPos;
		//	pRequest->m_vPathEnd = g_App->m_vNavMeshPathSearchEndPos;

		pLastReq->m_fDistanceBelowStartToLookForPoly = CPathServerThread::ms_fNormalDistBelowToLookForPoly;
		pLastReq->m_fDistanceAboveStartToLookForPoly = CPathServerThread::ms_fNormalDistAboveToLookForPoly;
		pLastReq->m_fDistanceBelowEndToLookForPoly = CPathServerThread::ms_fNormalDistBelowToLookForPoly;
		pLastReq->m_fDistanceAboveEndToLookForPoly = CPathServerThread::ms_fNormalDistAboveToLookForPoly;

		pLastReq->m_StartNavmeshAndPoly.Reset();

		while(!CPathServer::IsRequestResultReady(pLastReq->m_hHandle))
		{
			Sleep(0);
		}

		int iNumPts;
		Vector3 * pPts;
		TNavMeshWaypointFlag * pWptFlags;
		EPathServerErrorCode iCompletionCode = CPathServer::LockPathResult(pLastReq->m_hHandle, &iNumPts, pPts, pWptFlags);

		if(iCompletionCode == PATH_FOUND)
		{
			CPathRequest * pReq = CNavGenApp::GetLastPath();
			Assert(pReq->m_hHandle == pLastReq->m_hHandle);

			fAverageTotalTime += pReq->m_fTotalProcessingTimeInMillisecs;
			fAverageFindPolysTime += pReq->m_fMillisecsToFindPolys;
			fAverageDoPolygonPathTime += pReq->m_fMillisecsToFindPath;
			fAverageRefinePathTime += pReq->m_fMillisecsToRefinePath;
			fAverageMinimiseDistTime += pReq->m_fMillisecsToMinimisePathLength;
		}

		CPathServer::UnlockPathResultAndClear(pLastReq->m_hHandle);
	}

	float fNum = (float)iNumRepeats;

	fAverageTotalTime /= fNum;
	fAverageFindPolysTime /= fNum;
	fAverageDoPolygonPathTime /= fNum;
	fAverageRefinePathTime /= fNum;
	fAverageMinimiseDistTime /= fNum;

	DEBUGOUT("\n");
	DEBUGOUT("\n");
	DEBUGOUT("*************************************************************\n");
	DEBUGOUT1("Averaged results of running the path %i times...\n", iNumRepeats);
	DEBUGOUT1("Average total time : %.4fms\n", fAverageTotalTime);
	DEBUGOUT1("Average time to find start/end polys : %.4fms\n", fAverageFindPolysTime);
	DEBUGOUT1("Average time to find polygon path : %.4fms\n", fAverageDoPolygonPathTime);
	DEBUGOUT1("Average time to refine path : %.4fms\n", fAverageRefinePathTime);
	DEBUGOUT1("Average time to minimise length : %.4fms\n", fAverageMinimiseDistTime);
	DEBUGOUT("*************************************************************");
}

#define __INIT_WIDGETS	1 //__BANK

void
CNavGenApp::InitWidgets()
{
#if __INIT_WIDGETS
	bkManager::CreateBankManager("NavGenTest");

	bkBank & bank1 = BANKMGR.CreateBank("Generation");
	bank1.AddButton("Create ground plane", datCallback(CFA(CNavGenApp::CreateGroundPlane)));
	bank1.AddButton("Add test resolution area at camera", datCallback(CFA(CNavGenApp::AddTestResolutionArea)));
	bank1.AddButton("Load resolution areas", datCallback(CFA(CNavGenApp::LoadResolutionAreas)));
	bank1.AddButton("Load collision triangles", datCallback(CFA(CNavGenApp::LoadCollisionTris)));
	bank1.AddButton("Load extras info", datCallback(CFA(CNavGenApp::LoadExtrasInfo)));
	bank1.AddButton("Load authored adjacencies", datCallback(CFA(CNavGenApp::LoadAuthoredAdjacencies)));
	bank1.AddButton("Create octree", datCallback(CFA(CNavGenApp::CreateOctreeCB)));
	bank1.AddButton("Sample and triangulate", datCallback(CFA(CNavGenApp::SampleAndTriangulate)));
	bank1.AddToggle("Remove tris behind walls", &CNavGenApp::ms_bRemoveTrisBehindWalls);
	bank1.AddToggle("Fix jaggies", &g_App->m_bFixJaggiesAfterTriangulation);
	bank1.AddToggle("Move nodes away from walls", &g_App->m_bMoveNodesAwayFromWalls);
		
	bank1.AddSlider("Distance To Seed Nodes", &CNavGenApp::m_fDistanceToSeedNodes, 0.01f, 10.0f, 0.01f);
	bank1.AddSlider("Triangulation max height diff", &CNavGenApp::m_TriangulationMaxHeightDiff, 0.01f, 10.0f, 0.01f);
	bank1.AddSlider("Height above node base to triangulate", &CNavGenApp::m_HeightAboveNodeBaseAtWhichToTriangulate, 0.01f, 10.0f, 0.01f);
//	bank1.AddToggle("Check for height change under tri edge", &CNavGen::ms_bCheckForSuddenHeightChangesUnderTriEdges);
	bank1.AddSlider("Max height change under a triangle edge", &CNavGenApp::m_MaxHeightChangeUnderTriangleEdge, 0.01f, 10.0f, 0.01f);
//	bank1.AddSlider("MinimumVerticalDistanceBetweenNodes", &CNavGen::m_fMinimumVerticalDistanceBetweenNodes, 0.1f, 4.0f, 0.1f);
	bank1.AddSlider("TestClearHeightInTriangulation", &CNavGenApp::m_fTestClearHeightInTriangulation, -1.0f, 4.0f, 0.1f);

	bank1.AddSeparator();
	bank1.AddButton("Generate using voxel map", CNavGenApp::GenerateUsingVoxelMap);
	bank1.AddSeparator();

	bank1.AddToggle("Use test slice-plane", &CNavGenApp::m_bUseTestSlicePlane);
	bank1.AddSlider("Test slice-plane Z", &CNavGenApp::m_fTestSlicePlaneZ, -1000.0f, 1000.0f, 1.0f);

	bank1.AddButton("Optimise", datCallback(CFA(CNavGenApp::OptimiseNavMesh)));

	bank1.AddButton("RelexVertexPositions", datCallback(CFA(CNavGenApp::RelaxVertexPositions)));
	bank1.AddButton("CalcFreeSpaceBeyondPolyEdges", datCallback(CFA(CNavGenApp::CalcFreeSpaceBeyondPolyEdges)));
	
	bank1.AddButton("Move verts to smooth pavement edges", datCallback(CFA(CNavGenApp::MoveVertsToSmoothPavementEdges)));
	bank1.AddButton("Move verts to smooth jaggies", datCallback(CFA(CNavGenApp::MoveVertsToSmoothJaggies)));

	bank1.AddButton("MoveNodesAwayFromWalls", CNavGenApp::MoveNodesAwayFromWalls);

	bank1.AddButton("AntiAliasBoundaryEdges", CNavGenApp::AntiAliasBoundaryEdges);
	bank1.AddButton("AntiAliasPavementEdges", CNavGenApp::AntiAliasPavementEdges);
	bank1.AddButton("AntiAliasSteepSlopeEdges", CNavGenApp::AntiAliasSteepSlopeEdges);

	bank1.AddSeparator();
	bank1.AddButton("ResetEdgeIterator", CNavGenApp::ResetEdgeIterator);
	bank1.AddButton("Test BoundaryEdge iterator from selected tri", CNavGenApp::TestBoundaryEdgeIteratorFromSelectedTriangle);
	bank1.AddButton("Test PavementEdge iterator from selected tri", CNavGenApp::TestPavementEdgeIteratorFromSelectedTriangle);
	
	

	bkBank & bank2 = BANKMGR.CreateBank("Pathfinding");
	bank2.AddButton("Repeat last path request", CNavGenApp::RepeatLastPathRequest);
	bank2.AddToggle("Wander Path", &m_bWanderPath, NullCB, NULL);
	bank2.AddToggle("Flee Path", &m_bFleePath, NullCB, NULL);
	bank2.AddToggle("Cover Path", &m_bCoverPath, NullCB, NULL);
	bank2.AddToggle("Single step", &m_bSingleStepPathFinding, NullCB, NULL);
	bank2.AddToggle("Refine paths", &CPathServer::ms_bRefinePaths, NullCB, NULL);
	bank2.AddToggle("Minimise path lengths", &CPathServer::ms_bMinimisePathDistance, NullCB, NULL);

	bank2.AddToggle("Minimise before refine!!", &CPathServer::ms_bMinimiseBeforeRefine, NullCB, NULL);
	bank2.AddToggle("Pull path out from edges", &CPathServer::ms_bPullPathOutFromEdges, NullCB, NULL);
	bank2.AddToggle("Pull path out from edges twice", &CPathServer::ms_bPullPathOutFromEdgesTwice, NullCB, NULL);
	bank2.AddToggle("Pull path out from edges String pull", &CPathServer::ms_bPullPathOutFromEdgesStringPull, NullCB, NULL);
	
	bank2.AddToggle("Use LOS to early out path requests", &CPathServer::ms_bUseLosToEarlyOutPathRequests);
	bank2.AddToggle("Preserve Z heights in path", &CNavGenApp::m_bPreservePathHeights, NullCB, NULL);
	bank2.AddToggle("Sleep thread during long path requests", &CPathServer::m_bSleepPathServerThreadOnLongPathRequests);
	bank2.AddButton("Exhaustive Random Test", datCallback(CFA(CNavGenApp::ToggleExhaustiveTesting)));
	bank2.AddButton("Average 100 Paths", datCallback(CFA(CNavGenApp::RunLastPathOneHundredTimes)));
	bank2.AddToggle("Check every poly for objects", &CPathServer::ms_bTestObjectsForEveryPoly, NullCB, NULL);
	bank2.AddToggle("Use grid for DynamicObjectLOS", &CPathServer::ms_bUseGridToCullObjectLOS, NullCB, NULL);
	bank2.AddToggle("Use optimised poly centroids", &CPathServer::ms_bUseOptimisedPolyCentroids, NullCB, NULL);
	bank2.AddButton("Save last path as .\"prob\"", datCallback(CFA(CNavGenApp::SaveLastPathAsProbFile)));
	bank2.AddSlider("Path reference distance", &CNavGenApp::m_fNavMeshPathReferenceDistance, 0.0f, 200.0f, 0.5f);
	bank2.AddSlider("Path Completion Radius", &CNavGenApp::ms_fCompletionRadius, 0.0f, 10.0f, 0.1f);
	bank2.AddSlider("Entity Radius", &CNavGenApp::ms_fActorRadius, 0.0f, 20.0f, 0.1f);
	bank2.AddSlider("Pull out dist", &CPathServerThread::ms_fPullOutFromEdgesDistance, 0.0f, 10.0f, 0.1f);
	bank2.AddToggle("m_bNeverClimb", &CNavGenApp::m_bNeverClimb);
	bank2.AddToggle("m_bNeverDrop", &CNavGenApp::m_bNeverDrop);
	bank2.AddToggle("m_bNeverUseLadders", &CNavGenApp::m_bNeverUseLadders);
	bank2.AddTitle("Tessellation:");
	bank2.AddToggle("Do Tessellation", &CPathServer::ms_bDoPolyTessellation, NullCB, NULL);
	bank2.AddToggle("Do DeTessellation", &CPathServer::ms_bDoPolyUnTessellation, NullCB, NULL);
	bank2.AddToggle("Do preemptive tessellation", &CPathServer::ms_bDoPreemptiveTessellation, NullCB, NULL);
	bank2.AddToggle("Use spatial hash optimisations", &CPathServer::ms_bUseGridCellCache);
	bank2.AddToggle("Use fragment objects cache optimisations", &CPathServer::ms_bUseTessellatedPolyObjectCache);

	bank2.AddToggle("Detessellate prior to pathsearch", &CNavGenApp::ms_bDetessellatePriorToPathSearch, NullCB, NULL);
	bank2.AddToggle("Test connection poly exit conditions", &CPathServer::ms_bTestConnectionPolyExit);
	bank2.AddToggle("Allow tessellation of open nodes", &CPathServer::ms_bAllowTessellationOfOpenNodes);
	

	bank2.AddSlider("Max slope navigable", &CNavGenApp::ms_fMaxAngleNavigable, 0.0f, 90.0f, 1.0f);
	bank2.AddToggle("Favour enclosed areas", &CNavGenApp::ms_bFavourEnclosedAreas);
	bank2.AddToggle("Always use more points in polys", &CPathServer::ms_bAlwaysUseMorePointsInPolys);
	bank2.AddSlider("Smallest tesselated edge threshold sqr",  &CNavMesh::ms_fSmallestTessellatedEdgeThresholdSqr, 0.1f, 64.0f, 0.1f);
	bank2.AddToggle("Object avoidance", &CNavGenApp::ms_bAvoidObjects);
	bank2.AddToggle("Reduce BBoxes", &CNavGenApp::ms_bReduceBBoxes);
	bank2.AddToggle("Ignore non-uproot", &CNavGenApp::ms_bIgnoreObjectsWithNoUprootLimit);

	bkBank & bank3 = BANKMGR.CreateBank("Objects");
	bank3.AddToggle("Small Object", &m_bSmallObject, NullCB, NULL);
	bank3.AddButton("Create Object", datCallback(CFA(NavGenApp_CreateObject)));

	bkBank & bank4 = BANKMGR.CreateBank("Display");
	bank4.AddToggle("Octree", &g_App->m_bDrawOctreeBounds, NullCB, NULL);
	bank4.AddToggle("Collision Polys", &g_App->m_bDrawCollisionPolys, NullCB, NULL);
	bank4.AddToggle("Water", &g_App->m_bDrawWater, NullCB, NULL);
	bank4.AddToggle("Generated NavMesh", &g_App->m_bDrawNavigationSurfaces, NullCB, NULL);
	bank4.AddToggle("Solid", &g_App->m_bToggleNavigationSurfaceSolid, NullCB, NULL);
	bank4.AddToggle("PathServer NavMeshes", &g_App->m_bDisplayPathServerMesh, NullCB, NULL);
	bank4.AddToggle("Render Info", &g_App->m_bPrintRenderInfo, NullCB, NULL);
	bank4.AddToggle("Mark Visited Polys", &CPathServer::ms_bMarkVisitedPolys, NullCB, NULL);
	bank4.AddToggle("Mark Starting Polys", &CNavGenApp::m_bMarkStartingPolys);
	bank4.AddToggle("Print Out Path Results", &CPathServer::ms_bOutputDetailsOfPaths, NullCB, NULL);
	bank4.AddToggle("Display tessellation navmesh", &m_bDrawTessellationNavMesh, NullCB, NULL);
	bank4.AddToggle("Display adjacency (special-links)", &m_bDrawSpecialLinks, NullCB, NULL);
	bank4.AddToggle("Display adjacency (low-climbs)", &m_bDrawLowClimbs, NullCB, NULL);
	bank4.AddToggle("Display adjacency (high-climbs)", &m_bDrawHighClimbs, NullCB, NULL);
	bank4.AddToggle("Display adjacency (drop-downs)", &m_bDrawDropDowns, NullCB, NULL);
	bank4.AddToggle("Display hierarchical nodes", &m_bDrawHierarchical, NullCB, NULL);
	bank4.AddToggle("Display poly minmax under start marker", &m_bDrawPolyMinMaxUnderStartMarker, NullCB, NULL);
	bank4.AddToggle("Display minmax of objects", &m_bDrawMinMaxOfObjects, NullCB, NULL);
	bank4.AddToggle("Display objects grid cells minmax", &m_bDrawObjectsGridCellMinMax, NullCB, NULL);
	bank4.AddToggle("Draw poly Quick-Centroids", &m_bDrawPolyCentroids);
	bank4.AddToggle("Draw poly addresses", &m_bDrawPolyAddresses);
	bank4.AddToggle("Draw poly costs", &m_bDrawPolyCosts);
	bank4.AddToggle("Draw poly distances", &m_bDrawPolyDistanceTravelled);
	bank4.AddToggle("Draw points-in-polys", &m_bDrawPointsInPolys);
	bank4.AddToggle("Draw poly parents", &m_bDrawPolyParents);
	bank4.AddToggle("Display path stats", &m_bRenderPathStats);
	bank4.AddToggle("Draw free space beyond poly edges", &m_bDrawFreeSpaceBeyondPolyEdges);
	bank4.AddToggle("Draw free space aroudn vertices", &m_bDrawFreeSpaceAroundVertices);
	bank4.AddToggle("Draw adjacencies for poly under start marker", &m_bAdjacenciesUnderStartMarker);
	bank4.AddToggle("Draw path parent polys", &m_bPathParentPolys);
	

	bkBank & bank5 = BANKMGR.CreateBank("Default A* heuristics");
	{
		bank5.AddSlider("ms_fDefaultCostFromTargetMultiplier", &CPathFindMovementCosts::ms_fDefaultCostFromTargetMultiplier, 0.0f, 200.0f, 0.1f);
		bank5.AddSlider("ms_fDefaultDistanceTravelledMultiplier", &CPathFindMovementCosts::ms_fDefaultDistanceTravelledMultiplier, 0.0f, 200.0f, 0.1f);
		bank5.AddSlider("ms_fDefaultClimbHighPenalty", &CPathFindMovementCosts::ms_fDefaultClimbHighPenalty, 0.0f, 2000.0f, 1.0f);
		bank5.AddSlider("ms_fDefaultClimbLowPenalty", &CPathFindMovementCosts::ms_fDefaultClimbLowPenalty, 0.0f, 2000.0f, 1.0f);
		bank5.AddSlider("ms_fDefaultDropDownPenalty", &CPathFindMovementCosts::ms_fDefaultDropDownPenalty, 0.0f, 2000.0f, 1.0f);
		bank5.AddSlider("ms_fDefaultDropDownPenaltyPerMetre", &CPathFindMovementCosts::ms_fDefaultDropDownPenaltyPerMetre, 0.0f, 2000.0f, 1.0f);
		bank5.AddSlider("ms_fDefaultClimbLadderPenalty", &CPathFindMovementCosts::ms_fDefaultClimbLadderPenalty, 0.0f, 2000.0f, 1.0f);
		bank5.AddSlider("ms_fDefaultClimbLadderPenaltyPerMetre", &CPathFindMovementCosts::ms_fDefaultClimbLadderPenaltyPerMetre, 0.0f, 2000.0f, 1.0f);
		bank5.AddSlider("ms_fDefaultEnterWaterPenalty", &CPathFindMovementCosts::ms_fDefaultEnterWaterPenalty, 0.0f, 2000.0f, 1.0f);
		bank5.AddSlider("ms_fDefaultLeaveWaterPenalty", &CPathFindMovementCosts::ms_fDefaultLeaveWaterPenalty, 0.0f, 2000.0f, 1.0f);
		bank5.AddSlider("ms_fDefaultBeInWaterPenalty", &CPathFindMovementCosts::ms_fDefaultBeInWaterPenalty, 0.0f, 2000.0f, 1.0f);
		//bank5.AddSlider("ms_fDefaultFixedCostToPushObject", &CPathFindMovementCosts::ms_fDefaultFixedCostToPushObject, 0.0f, 2000.0f, 1.0f);
		//bank5.AddSlider("ms_fDefaultFixedCostToClimbObject", &CPathFindMovementCosts::ms_fDefaultFixedCostToClimbObject, 0.0f, 2000.0f, 1.0f);
		bank5.AddSlider("ms_fDefaultClimbObjectPenaltyPerMetre", &CPathFindMovementCosts::ms_fDefaultClimbObjectPenaltyPerMetre, 0.0f, 2000.0f, 1.0f);
		bank5.AddSlider("ms_fDefaultWanderPenaltyForZeroPedDensity", &CPathFindMovementCosts::ms_fDefaultWanderPenaltyForZeroPedDensity, 0.0f, 2000.0f, 0.1f);
		bank5.AddSlider("ms_fDefaultMoveOntoSteepSurfacePenalty", &CPathFindMovementCosts::ms_fDefaultMoveOntoSteepSurfacePenalty, 0.0f, 2000.0f, 1.0f);
		bank5.AddSlider("ms_fDefaultPenaltyForNoDirectionalCover", &CPathFindMovementCosts::ms_fDefaultPenaltyForNoDirectionalCover, 0.0f, 2000.0f, 1.0f);
		bank5.AddSlider("ms_fDefaultPenaltyForFavourCoverPerUnsetBit", &CPathFindMovementCosts::ms_fDefaultPenaltyForFavourCoverPerUnsetBit, 0.0f, 2000.0f, 1.0f);
		bank5.AddSlider("ms_iDefaultPenaltyForMovingToLowerPedDensity", &CPathFindMovementCosts::ms_iDefaultPenaltyForMovingToLowerPedDensity, 0, 2000, 1);

		bank5.AddSlider("ms_fFleePathDoubleBackCost", &CPathFindMovementCosts::ms_fFleePathDoubleBackCost, 0.0f, 2000.0f, 0.1f);
		bank5.AddSlider("ms_fFleePathDoubleBackMultiplier", &CPathFindMovementCosts::ms_fFleePathDoubleBackMultiplier, 0.0f, 2000.0f, 0.1f);
	}

	bkBank & bank6 = BANKMGR.CreateBank("Test");
	bank6.AddButton("Tessellate Poly Under Start Marker", datCallback(CFA(CNavGenApp::TessellatePolyUnderStartMarker)));
	bank6.AddButton("Clip Poly Under Start Marker", datCallback(CFA(CNavGenApp::ClipPolyUnderStartMarker)));
	bank6.AddButton("DeTessellate Now", datCallback(CFA(CNavGenApp::DetessellateNow)));
	bank6.AddButton("Debug Poly Under Start Marker", datCallback(CFA(CNavGenApp::DebugNavMeshPolyUnderStartMarker)));	
	bank6.AddButton("Debug Poly Under Camera", datCallback(CFA(CNavGenApp::DebugNavMeshPolyUnderfoot)));
	bank6.AddText("Poly Address:", ms_DebugPolyEditWidget, 255, false);
	bank6.AddButton("Debug Poly By Address", datCallback(CFA(CNavGenApp::DebugNavMeshPolyByAddress)));	
	bank6.AddToggle("Dump out debug poly traceback", &CNavGenApp::m_bDumpDebugPolyTraceback);
	bank6.AddToggle("Debug draw poly also", &CNavGenApp::m_bDrawDebugPoly);
	bank6.AddToggle("Debug draw parent traceback also", &CNavGenApp::m_bDrawDebugPolyTraceback);
	bank6.AddButton("Print start marker pos", datCallback(CFA(CNavGenApp::PrintStartMarkerPos)));
	bank6.AddToggle("(debug surrounding polys too)", &CNavGenApp::ms_bDebugSurroundingPolysToo);
	bank6.AddButton("LOS Test (request)", datCallback(CFA(CNavGenApp::LOSTest)));
	bank6.AddButton("TestNavMeshLOS (immediate)", datCallback(CFA(CNavGenApp::TestNavMeshLOS)));
	bank6.AddButton("Test all adjacencies", datCallback(CFA(CNavGenApp::TestAllAdjacencies)));
	bank6.AddButton("Expand objects by radius delta", datCallback(CFA(CNavGenApp::ExpandObjects)));
	bank6.AddButton("Contract objects by radius delta", datCallback(CFA(CNavGenApp::ContractObjects)));
	bank6.AddButton("Stitch Map Swap For EntireMap", datCallback(CFA(CNavGenApp::StitchMapSwapForEntireMap)));

#endif
}

bool
CNavGenApp::LockCollTriVB(void)
{
	m_iNumVertsInCollisionPolyVB = 0;

	HRESULT retval;
	retval = m_pCollisionPolysVB->Lock(
		0,
		sizeof(LitVertex) * MAX_NUM_COLPOLY_VB_VERTS,
		(void**)&m_pCollisionPolyVertexData,
		D3DLOCK_DISCARD
	);
	if(retval != D3D_OK)
	{
		return false;
	}

	return true;
}

bool
CNavGenApp::UnlockCollTriVB(void)
{
	HRESULT retval;
	retval = m_pCollisionPolysVB->Unlock();
	if(retval != D3D_OK)
	{
		return false;
	}
	return true;
}

// Adds a tri to our collision-triangle VertexBuffer
bool
CNavGenApp::AddTri(Vector3 & v1, Vector3 & v2, Vector3 & v3, DWORD rgb1, DWORD rgb2, DWORD rgb3)
{
	if(m_iNumVertsInCollisionPolyVB + 3 > m_iMaxNumVertsInCollisionPolyVB)
		return false;

	if(!m_pCollisionPolyVertexData)
		return false;

	m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].x = v1.x;
	m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].y = v1.y;
	m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].z = v1.z;
	m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].diffuse = rgb1;
	m_iNumVertsInCollisionPolyVB++;

	m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].x = v2.x;
	m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].y = v2.y;
	m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].z = v2.z;
	m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].diffuse = rgb2;
	m_iNumVertsInCollisionPolyVB++;

	m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].x = v3.x;
	m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].y = v3.y;
	m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].z = v3.z;
	m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].diffuse = rgb3;
	m_iNumVertsInCollisionPolyVB++;

	return true;
}

// Adds a tri to our collision-triangle VertexBuffer
bool
CNavGenApp::AddPoly(int numverts, Vector3 * verts, DWORD col)
{
	if(numverts < 3)
		return false;

	int numTris = 1 + (numverts - 3);

	if(m_iNumVertsInCollisionPolyVB + (numTris*3) > m_iMaxNumVertsInCollisionPolyVB)
		return false;

	if(!m_pCollisionPolyVertexData)
		return false;

	int lastv = 1;
	for(int v=2; v<numverts; v++)
	{
		m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].x = verts[0].x;
		m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].y = verts[0].y;
		m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].z = verts[0].z;
		m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].diffuse = col;
		m_iNumVertsInCollisionPolyVB++;

		m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].x = verts[lastv].x;
		m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].y = verts[lastv].y;
		m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].z = verts[lastv].z;
		m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].diffuse = col;
		m_iNumVertsInCollisionPolyVB++;

		m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].x = verts[v].x;
		m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].y = verts[v].y;
		m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].z = verts[v].z;
		m_pCollisionPolyVertexData[m_iNumVertsInCollisionPolyVB].diffuse = col;
		m_iNumVertsInCollisionPolyVB++;

		lastv = v;
	}

	return true;
}

bool
CNavGenApp::LockLineSegVB(void)
{
	m_iNumVertsInLineSegVB = 0;

	HRESULT retval;
	retval = m_pLineSegVB->Lock(
		0,
		sizeof(LitVertex) * MAX_NUM_LINESEG_VB_VERTS,
		(void**)&m_pLineSegVertexData,
		D3DLOCK_DISCARD
	);
	if(retval != D3D_OK)
	{
		return false;
	}

	return true;
}

bool
CNavGenApp::UnlockLineSegVB(void)
{
	HRESULT retval;
	retval = m_pLineSegVB->Unlock();
	if(retval != D3D_OK)
	{
		return false;
	}
	return true;
}

// Adds a line to our line-seg VertexBuffer
bool
CNavGenApp::AddLine(const Vector3 & v1, const Vector3 & v2, DWORD rgb1, DWORD rgb2)
{
	if(m_iNumVertsInLineSegVB + 2 > m_iMaxNumVertsInLineSegVB)
		return false;

	if(!m_pLineSegVertexData)
		return false;

	m_pLineSegVertexData[m_iNumVertsInLineSegVB].x = v1.x;
	m_pLineSegVertexData[m_iNumVertsInLineSegVB].y = v1.y;
	m_pLineSegVertexData[m_iNumVertsInLineSegVB].z = v1.z;
	m_pLineSegVertexData[m_iNumVertsInLineSegVB].diffuse = rgb1;
	m_iNumVertsInLineSegVB++;

	m_pLineSegVertexData[m_iNumVertsInLineSegVB].x = v2.x;
	m_pLineSegVertexData[m_iNumVertsInLineSegVB].y = v2.y;
	m_pLineSegVertexData[m_iNumVertsInLineSegVB].z = v2.z;
	m_pLineSegVertexData[m_iNumVertsInLineSegVB].diffuse = rgb2;
	m_iNumVertsInLineSegVB++;

	return true;
}

//****************************************************************
//
//	CNavGenApp::InitWindow
//
//****************************************************************

bool
CNavGenApp::InitWindow(void) {

	/*
	Register a windows class if not aready done so
	*/
	WNDCLASSEX wndclass;

	if(!GetClassInfoEx(m_Hinstance, "NavGenTest", &wndclass)) {

		wndclass.cbSize = sizeof(WNDCLASSEX);
		wndclass.style = CS_BYTEALIGNCLIENT | CS_BYTEALIGNWINDOW;
		wndclass.lpfnWndProc = ProcMsg;
		wndclass.cbClsExtra = 0;
		wndclass.cbWndExtra = 4;
		wndclass.hInstance = m_Hinstance;
		wndclass.hIcon = NULL;
		wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
		wndclass.hbrBackground = (HBRUSH) NULL_BRUSH;
		wndclass.lpszMenuName = NULL;
		wndclass.lpszClassName = "NavGenTest";
		wndclass.hIconSm = NULL;

		if(!RegisterClassEx(&wndclass)) {
			return FALSE;
		}
	}

	/*
	Fiddle with window size to accomodate render target if not full-screen
	*/

	DWORD style = 0;
	RECT rect;

	HWND hdesktop = GetDesktopWindow();
	RECT drect;
	GetWindowRect(hdesktop, &drect);

	style = WS_OVERLAPPEDWINDOW ;//WS_POPUP;

	m_WindowLeftEdge = GetSystemMetrics(SM_CXSIZEFRAME);
	m_WindowTopEdge = GetSystemMetrics(SM_CYSIZEFRAME);
	if(style == WS_OVERLAPPEDWINDOW || style == WS_OVERLAPPED)
		m_WindowTopEdge += GetSystemMetrics(SM_CYCAPTION);

	rect.left = 0;
	rect.top = 0;
	rect.right = m_ScreenWidth;
	rect.bottom = m_ScreenHeight;
	rect.right += m_WindowLeftEdge * 2;
	rect.bottom += GetSystemMetrics(SM_CYSIZEFRAME) * 2;
	if(style == WS_OVERLAPPEDWINDOW || style == WS_OVERLAPPED)
		rect.bottom += GetSystemMetrics(SM_CYCAPTION);

	int xoffset = (drect.right - (rect.right - rect.left)) / 2;
	int yoffset = (drect.bottom - (rect.bottom - rect.top)) / 2;

	rect.right -= rect.left;
	rect.bottom -= rect.top;

	rect.left = xoffset;
	rect.top = yoffset;

	// Hmmm. For some reasons the window dimensions are 2 pixels out horiz & vert.
	// This means that stretch-blts are invoked which are slow & not always supported.
	// Am gonna manually hack this, and subtract the offending 2 pixels.
	rect.right -= 2;
	rect.bottom -= 2;

	/*
	Create the window
	*/
	m_Hwnd = CreateWindowEx(
        0,
        "NavGenTest",
        "NavGenTest - Mouse look (SPACE to toggle)",
        style,
        rect.left, rect.top,
		rect.right, rect.bottom,
        NULL,
        HMENU (NULL),
        m_Hinstance,
        LPVOID (NULL)
	);

	HDC dc = GetDC(m_Hwnd);
	SetBkColor(dc, RGB(0,0,0));

	SetLastError(0);

	if(SetWindowLong(m_Hwnd, GWL_USERDATA, (LONG)this) == 0) {
		if(GetLastError()) {
			return FALSE;
		}
	}

    // set initial show state
	int showstate = SW_SHOW;
	//int showstate = SW_SHOWMAXIMIZED;

    ShowWindow(m_Hwnd, showstate);
    UpdateWindow(m_Hwnd);

	RECT windowRect, clientRect;
	GetWindowRect(m_Hwnd, &windowRect);
	GetClientRect(m_Hwnd, &clientRect);

	return true;
}


void
CNavGenApp::ConfineCursorToWindow(bool bConfine)
{
	/*
	if(bConfine)
	{
		RECT wRect;
		GetWindowRect(m_Hwnd, &wRect);

		wRect.left += m_WindowLeftEdge;
		wRect.top  += m_WindowTopEdge;
		wRect.right  -= GetSystemMetrics(SM_CXFRAME);
		wRect.bottom -= GetSystemMetrics(SM_CYFRAME);
		ClipCursor(&wRect);
	}
	else
	{
		ClipCursor(NULL);
	}

	m_bCursorConfinedToWindow = bConfine;
	*/
}


char *
CNavGenApp::SelectFileToOpen(char * pDialogTitle, char * pFilterString)
{
	m_FileName[0] = 0;
	m_FileTitle[0] = 0;

	OPENFILENAME openFileName;
	ZeroMemory(&openFileName, sizeof(OPENFILENAME));

	// Set up the OPENFILENAME struct
	openFileName.lStructSize = sizeof(OPENFILENAME);
	openFileName.hwndOwner = m_Hwnd;
	openFileName.hInstance = m_Hinstance;
	openFileName.lpstrFilter = pFilterString;
	openFileName.lpstrCustomFilter = NULL;
	openFileName.nMaxCustFilter = 0;
	openFileName.nFilterIndex = 1;
	openFileName.lpstrFile = m_FileName;
	openFileName.nMaxFile = 256;
	openFileName.lpstrFileTitle = m_FileTitle;
	openFileName.nMaxFileTitle = 256;
	openFileName.lpstrInitialDir = NULL;
	openFileName.lpstrTitle = pDialogTitle;
	openFileName.Flags = OFN_FILEMUSTEXIST;
	openFileName.nFileOffset = 0;
	openFileName.nFileExtension = 0;
	openFileName.lpstrDefExt = NULL;


	bool ret = GetOpenFileName(&openFileName);
	if(ret)
	{
		return openFileName.lpstrFile;
	}
	else
	{
		return NULL;
	}
}






bool
CNavGenApp::InitDirectInput(void)
{
	HRESULT retval;

	// Create direct input
	retval = DirectInput8Create(
		GetModuleHandle(0),	//m_Hinstance,
		DIRECTINPUT_VERSION,
		IID_IDirectInput8A,
		(LPVOID*)&m_pDirectInput8,
		NULL
	);
	if(!m_pDirectInput8 || retval != S_OK)
	{
		return false;
	}

	// Create the mouse device
	retval = m_pDirectInput8->CreateDevice(GUID_SysMouse, &m_pMouseDevice, NULL);
	if(!m_pMouseDevice || retval != S_OK)
	{
		return false;
	}

	// Set data format, etc
	retval = m_pMouseDevice->SetDataFormat(&c_dfDIMouse);
	retval = m_pMouseDevice->SetCooperativeLevel(m_Hwnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	retval = m_pMouseDevice->Acquire();

	return true;
}


bool
CNavGenApp::ShutdownDirectInput(void)
{
	if(m_pMouseDevice)
	{
		m_pMouseDevice->Unacquire();
		m_pMouseDevice->Release();
		m_pMouseDevice = NULL;
	}

	if(m_pDirectInput8)
	{
		m_pDirectInput8->Release();
		m_pDirectInput8 = NULL;
	}

	return true;
}

void
CNavGenApp::AcquireMouse(bool bAcquire)
{
	if(g_App->m_pMouseDevice)
	{
		if(bAcquire)
		{
			g_App->m_pMouseDevice->Acquire();
		}
		else
		{
			g_App->m_pMouseDevice->Unacquire();
		}
	}
}

//****************************************************************
//
//	CNavGenApp::InitD3D
//
//****************************************************************

bool
CNavGenApp::InitD3D(void)
{
	HRESULT retval = D3D_OK;

	m_pD3D9 = Direct3DCreate9(D3D_SDK_VERSION);
	if(!m_pD3D9)
	{
		return false;
	}
	
	m_D3DPresentParams.BackBufferWidth = m_ViewPortWidth;
	m_D3DPresentParams.BackBufferHeight = m_ViewPortHeight;
	m_D3DPresentParams.BackBufferFormat = D3DFMT_A8R8G8B8;
	m_D3DPresentParams.BackBufferCount = 1;
	m_D3DPresentParams.MultiSampleType = D3DMULTISAMPLE_NONE;
	m_D3DPresentParams.MultiSampleQuality = 0;
	m_D3DPresentParams.SwapEffect = D3DSWAPEFFECT_DISCARD;
	m_D3DPresentParams.hDeviceWindow = m_Hwnd;
	m_D3DPresentParams.Windowed = TRUE;
	m_D3DPresentParams.EnableAutoDepthStencil = TRUE;
	m_D3DPresentParams.AutoDepthStencilFormat = D3DFMT_D24S8;
	m_D3DPresentParams.Flags = 0;
	m_D3DPresentParams.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
	m_D3DPresentParams.PresentationInterval = D3DPRESENT_INTERVAL_DEFAULT;
	
	// First try for mixed vp, then fall back to sw-only vp
	DWORD vpFlags[] = { D3DCREATE_MIXED_VERTEXPROCESSING, D3DCREATE_SOFTWARE_VERTEXPROCESSING };
	
	int iNumAdapters = m_pD3D9->GetAdapterCount();
	int a;

	for(a=0; a<iNumAdapters; a++)
	{
		for(int t=0; t<2; t++)
		{
			DWORD flags = vpFlags[t];	// | D3DCREATE_MANAGED,

			retval = m_pD3D9->CreateDevice(
				a,
				D3DDEVTYPE_HAL,
				m_Hwnd,
				flags,
				&m_D3DPresentParams,
				&m_pD3DDevice
			);

			if(retval == D3D_OK)
				break;
		}
	}
	if(retval != D3D_OK)
	{
		for(a=0; a<iNumAdapters; a++)
		{
			for(int t=0; t<2; t++)
			{
				DWORD flags = vpFlags[t];	// | D3DCREATE_MANAGED,

				retval = m_pD3D9->CreateDevice(
					a,
					D3DDEVTYPE_SW,
					m_Hwnd,
					flags,
					&m_D3DPresentParams,
					&m_pD3DDevice
				);

				if(retval == D3D_OK)
					break;
			}
		}
	}
	if(retval != D3D_OK)
	{
		for(a=0; a<iNumAdapters; a++)
		{
			for(int t=0; t<2; t++)
			{
				DWORD flags = vpFlags[t];	// | D3DCREATE_MANAGED,

				retval = m_pD3D9->CreateDevice(
					a,
					D3DDEVTYPE_REF,
					m_Hwnd,
					flags,
					&m_D3DPresentParams,
					&m_pD3DDevice
				);

				if(retval == D3D_OK)
					break;
			}
		}
	}
	// Couldn't create any device
	if(retval != D3D_OK)
	{
		D3DError(retval);
		return false;
	}
	
	return true;
}

void
CNavGenApp::ShutdownD3D(void)
{
	if(m_pCollisionPolysVB)
	{
		m_pCollisionPolysVB->Release();
	}
	if(m_pLineSegVB)
	{
		m_pLineSegVB->Release();
	}

	if(m_pSingleNavMeshTrianglesVB)
	{
		m_pSingleNavMeshTrianglesVB->Release();
	}
	if(m_pSingleNavMeshLinesVB)
	{
		m_pSingleNavMeshLinesVB->Release();
	}

	m_pD3DDevice->Release();
	m_pD3D9->Release();
}

void
CNavGenApp::D3DError(HRESULT retval)
{
	char * errTxt = NULL;
	
	switch(retval)
	{
		case D3DERR_INVALIDCALL:
			errTxt = "Invalid Call";
			break;
			
		case D3DERR_NOTAVAILABLE:
			errTxt = "Not Available";
			break;
			
		case D3DERR_OUTOFVIDEOMEMORY:
			errTxt = "Out of Video Memory";
			break;
	}
	
	OutputDebugString(errTxt);
}

void
CNavGenApp::SetColourStage(DWORD stage, DWORD op, DWORD arg1, DWORD arg2)
{
	if(m_iColourOp[stage] != op)
	{
		m_pD3DDevice->SetTextureStageState(stage, D3DTSS_COLOROP, op);
		m_iColourOp[stage] = op;
	}
	
	if(m_iColourArg1[stage] != arg1)
	{
		m_pD3DDevice->SetTextureStageState(stage, D3DTSS_COLORARG1, arg1);
		m_iColourArg1[stage] = arg1;
	}
	
	if(m_iColourArg2[stage] != arg2)
	{
		m_pD3DDevice->SetTextureStageState(stage, D3DTSS_COLORARG2, arg2);
		m_iColourArg2[stage] = arg2;
	}
}

void
CNavGenApp::SetAlphaStage(DWORD stage, DWORD op, DWORD arg1, DWORD arg2)
{
	if(m_iAlphaOp[stage] != op)
	{
		m_pD3DDevice->SetTextureStageState(stage, D3DTSS_ALPHAOP, op);
		m_iAlphaOp[stage] = op;
	}
	
	if(m_iAlphaArg1[stage] != arg1)
	{
		m_pD3DDevice->SetTextureStageState(stage, D3DTSS_ALPHAARG1, arg1);
		m_iAlphaArg1[stage] = arg1;
	}
	
	if(m_iAlphaArg2[stage] != arg2)
	{
		m_pD3DDevice->SetTextureStageState(stage, D3DTSS_ALPHAARG2, arg2);
		m_iAlphaArg2[stage] = arg2;
	}

}

void
CNavGenApp::SetAlphaBlendEnable(bool bState)
{
	if(bState != m_bAlphaBlendEnabled)
	{
		m_pD3DDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, bState);
		m_bAlphaBlendEnabled = bState;
	}
}

void
CNavGenApp::SetSrcBlend(D3DBLEND srcBlend)
{
	if(srcBlend != m_SrcBlend)
	{
		m_pD3DDevice->SetRenderState(D3DRS_SRCBLEND, srcBlend);
		m_SrcBlend = srcBlend;
	}
}

void
CNavGenApp::SetDestBlend(D3DBLEND destBlend)
{
	if(destBlend != m_DestBlend)
	{
		m_pD3DDevice->SetRenderState(D3DRS_DESTBLEND, destBlend);
		m_DestBlend = destBlend;
	}
}



void
CNavGenApp::D3DXMatrixToCMatrix(Matrix34 & mat, const D3DXMATRIX & d3dMat)
{
	mat.a.x = d3dMat._11;
	mat.a.y = d3dMat._12;
	mat.a.z = d3dMat._13;

	mat.b.x = d3dMat._21;
	mat.b.y = d3dMat._22;
	mat.b.z = d3dMat._23;

	mat.c.x = d3dMat._31;
	mat.c.y = d3dMat._32;
	mat.c.z = d3dMat._33;

	mat.d.x = d3dMat._41;
	mat.d.y = d3dMat._42;
	mat.d.z = d3dMat._43;
}


void
CNavGenApp::CMatrixToD3DMatrix(D3DXMATRIX & d3dMat, const Matrix34 & mat)
{
	d3dMat._11 = mat.a.x;
	d3dMat._12 = mat.a.y;
	d3dMat._13 = mat.a.z;
	d3dMat._14 = 0.0f;

	d3dMat._21 = mat.b.x;
	d3dMat._22 = mat.b.y;
	d3dMat._23 = mat.b.z;
	d3dMat._24 = 0.0f;

	d3dMat._31 = mat.c.x;
	d3dMat._32 = mat.c.y;
	d3dMat._33 = mat.c.z;
	d3dMat._34 = 0.0f;

	d3dMat._41 = mat.d.x;
	d3dMat._42 = mat.d.y;
	d3dMat._43 = mat.d.z;
	d3dMat._44 = 1.0f;
}



//****************************************************************
//
//	CNavGenApp::MsgLoop
//
//****************************************************************


int
CNavGenApp::MsgLoop(void) {

	MSG msg;
	int ticks, last_ticks, delta_ticks;
	float secs = 0;

	last_ticks = timeGetTime();

	while(1) {
	
		while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {
            
			if(msg.message == WM_QUIT) return (int)msg.wParam;

			TranslateMessage(&msg);
			DispatchMessage(&msg);
			Sleep(0);
		}

		/*
		Run application
		*/
		ticks = timeGetTime();
		delta_ticks = ticks - last_ticks;
		last_ticks = ticks;

		secs += ((float)delta_ticks) / 1000.0f;		
		
		if(secs < (1.0f / DESIRED_FRAMERATE))
		{
			Sleep(0);
			continue;
		}

		if(secs > 1.0f)
			secs = 1.0f;

		if(!Run(secs))
		{
			break;
		}
		
		secs = 0;

		Sleep(0);
	}

	return 1;
}


//****************************************************************
//
//	CNavGenApp::MsgLoop
//
//****************************************************************

LRESULT CALLBACK
CNavGenApp::ProcMsg(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam) {

	switch(msg)
	{
		case WM_KEYDOWN :
		{
			if(wparam >= 0 && wparam < 256)
			{
				g_App->m_Keys[wparam] = 1;
			}
			return 0;
		}
		case WM_KEYUP :
		{
			if(wparam >= 0 && wparam < 256)
			{
				g_App->m_Keys[wparam] = 0;
			}
			return 0;
		}

		case WM_LBUTTONDOWN :
		{
			POINTS pos = MAKEPOINTS(lparam);
			// NB : If we have any kind of window scaling, then we'll want to account for it here..
			g_App->m_MouseXPos = pos.x;
			g_App->m_MouseYPos = pos.y;
			g_App->m_bMouseClicked = false;//true;
			g_App->m_bMouseHeld = true;

			if(wparam & MK_CONTROL)
			{
				g_App->m_bCtrlDown = true;
			}
			else
			{
				g_App->m_bCtrlDown = false;
			}

			return 0;
		}
		case WM_LBUTTONUP :
		{
			POINTS pos = MAKEPOINTS(lparam);
			// NB : If we have any kind of window scaling, then we'll want to account for it here..
			g_App->m_MouseXPos = pos.x;
			g_App->m_MouseYPos = pos.y;
			g_App->m_bMouseClicked = true;//false;
			g_App->m_bMouseHeld = false;
			return 0;
		}

		case WM_CLOSE :
		{
			PostQuitMessage(0);
			return 0;
		}

		case WM_PAINT :
		{
			break;
		}

		case WM_ACTIVATE :
		{
			bool bIsActive = (LOWORD(wparam) != WA_INACTIVE);

			if(bIsActive)
			{
				if(g_App->m_bHasLoadedUp)
				{
					g_App->ConfineCursorToWindow(true);
				}

				if(g_App->m_pMouseDevice && g_App->m_bMouseLook)
				{
					g_App->m_pMouseDevice->Acquire();
				}
			}
			else
			{
				g_App->ConfineCursorToWindow(false);

				if(g_App->m_pMouseDevice && g_App->m_bMouseLook)
				{
					g_App->m_pMouseDevice->Unacquire();
				}
			}

			return 0;
		}

		case WM_DESTROY :
		{
			g_App->ConfineCursorToWindow(false);
			return 0;
		}
	}

	return(DefWindowProc(hwnd, msg, wparam, lparam));
}



//****************************************************************
//
//	CNavGenApp::Run
//
//****************************************************************

bool
CNavGenApp::Run(float fSecs)
{
	ShowCursor(TRUE);

	//_ASSERTE(_CrtCheckMemory());

	// make sure we don't exceed the speed-limit!
	m_SecsThisFrame = fSecs;

//	if(m_SecsThisFrame < (1.0f / ((float)DESIRED_FRAMERATE)))
//	{
//		return true;
//	}
	
	HRESULT retval;
	
	// Clear backbuffer/zbuffer
	retval = m_pD3DDevice->Clear(0, NULL, D3DCLEAR_STENCIL | D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, m_ClearCol, 1.0f, 0);
	if(retval != D3D_OK)
	{
		D3DError(retval);
		return false;
	}

	// Get input
	GetInput(fSecs);

	m_vViewForwards = m_mViewOrientation.b;//GetForward();
	
	// Set up view matrix
	D3DXVECTOR3 vUpD3D(0, 0, 1.0f);

	Vector3 vAt = m_vViewPos + (m_vViewForwards * 10.0f);
	D3DXVECTOR3 vAtD3D;
	vAtD3D.x = vAt.x;
	vAtD3D.y = vAt.y;
	vAtD3D.z = vAt.z;

	D3DXVECTOR3 vViewPosD3D;
	vViewPosD3D.x = m_vViewPos.x;
	vViewPosD3D.y = m_vViewPos.y;
	vViewPosD3D.z = m_vViewPos.z;

	D3DXMatrixLookAtLH(&m_ViewMatrix, &vViewPosD3D, &vAtD3D, &vUpD3D);

	// Set the view transform				
	m_pD3DDevice->SetTransform(D3DTS_VIEW, &m_ViewMatrix);

	// Get the frustum planes for culling
	ExtractFrustumPlanes();

	
	//****************************************
	// Start of drawing stuff..
	//****************************************

	m_iNumNodesVisitedForRender = 0;
	m_iNumLeavesVisitedForRender = 0;
	m_iNumNavMeshTrisRenderedThisFrame = 0;

	// Lock vertex buffers
	LockCollTriVB();
	LockLineSegVB();

	CPathServer::Process((m_vNavMeshPathSearchStartPos + m_vNavMeshPathSearchStartPos) / 2.0f);

	RunExhaustiveTesting();


	// Draw all the shit for this frame
	// Draw collision polys, etc
	if(m_bDrawCollisionPolysDumbass && !m_NodeGenerator.m_Octree.RootNode())
	{
		DrawTrisDumbass();
	}
	if(m_bDrawOctreeBounds || m_bDrawCollisionPolys || m_bDrawSplitPolys)
	{
		m_NodeGenerator.m_Octree.IncTimeStamp();
		DrawOctree();
	}
	if(m_bDrawWater)
	{
		DrawWater();
	}
	if(m_NodeGenerator.m_CollisionTriangles.size())
	{
		DrawBoxFromMinMax(m_vSectorMins, m_vSectorMaxs + Vector3(0, 0, 20.0f), D3DCOLOR_COLORVALUE(0.5f, 0.5f, 1.0f, 1.0f));
	}
	if(m_bDrawPathNodes)
	{
		DrawNodes();
	}
	if(m_bDrawNavigationSurfaces)
	{
		DrawNavSurface();
	}

	DrawExtrasInfo();

	DrawResolutionAreas();

	DrawAuthoredAdjacencies();

	if(m_bAdjacenciesUnderStartMarker)
	{
		TNavMeshPoly * pPoly = GetPolyUnderPosition(m_vNavMeshPathSearchStartPos, true);
		if(pPoly)
			DrawAdjacenciesForPoly(pPoly);
	}

	if( m_TestEdgeIterator.GetStarting().m_pTri != NULL )
	{
		CNavSurfaceTri * pTri;
		s32 iEdge;
		CNavGenNode * pNode1, * pNode2;
		m_TestEdgeIterator.GetCurrent(&pTri, &iEdge, &pNode1, &pNode2);
		AddLine(pNode1->m_vBasePos+ZAXIS, pNode2->m_vBasePos+ZAXIS, D3DCOLOR_RGBA(255, 0, 255, 255), D3DCOLOR_RGBA(255, 0, 255, 255));
	}

//	if(m_PathNodes.GetCount())
//	{
//		DrawPathNodes(m_PathNodes);
//	}
	if(m_vNavMeshPathPoints.GetCount())
	{
		DrawPathNodes(m_vNavMeshPathPoints, m_iNavMeshPathWaypointFlags, m_vNavMeshPolyCentroids);
	}

	if(m_bPathServerInitialised)
	{
		DrawPathNode(m_vNavMeshPathSearchStartPos, D3DCOLOR_RGBA(128, 128, 255, 255), D3DCOLOR_RGBA(0, 255, 0, 255), 16.0f);
		DrawPathNode(m_vNavMeshPathSearchEndPos, D3DCOLOR_RGBA(128, 128, 255, 255), D3DCOLOR_RGBA(255, 0, 0, 255), 16.0f);
	}

	RenderNavMeshPathPolysEtc();

	const aiNavDomain domain = kNavDomainRegular;

	if(m_iDrawNavMeshSinglePolyIndex != NAVMESH_POLY_INDEX_NONE)
	{
		TNavMeshIndex iNavmesh = CPathServer::GetNavMeshIndexFromPosition(m_vNavMeshPathSearchStartPos, domain);
		CNavMesh * pNavMesh = CPathServer::GetNavMeshFromIndex(iNavmesh, domain);
		if(pNavMesh)
		{
			DrawNavMeshSinglePoly(pNavMesh, m_iDrawNavMeshSinglePolyIndex);
			
			Vector3 vCentroid;
			pNavMesh->GetPolyCentroid(m_iDrawNavMeshSinglePolyIndex, vCentroid);
			AddLine(vCentroid, vCentroid + Vector3(0.0f,0.0f,20.0f), D3DCOLOR_RGBA(255,0,255,255), D3DCOLOR_RGBA(255,0,255,255));
		}
	}
	if(m_bDrawPolyMinMaxUnderStartMarker)
	{
		CNavMesh * pNavMesh = CPathServer::GetNavMeshFromIndex( CPathServer::GetNavMeshIndexFromPosition(m_vNavMeshPathSearchStartPos, domain), domain );
		if(pNavMesh)
		{
			Vector3 vIntersect;
			const u32 iPoly = pNavMesh->GetPolyBelowPoint(m_vNavMeshPathSearchStartPos + Vector3(0.0f,0.0f,1.0f), vIntersect, 4.0f);
			if(iPoly != NAVMESH_POLY_INDEX_NONE)
			{
				TNavMeshPoly * pPoly = pNavMesh->GetPoly(iPoly);
				Vector3 vMin, vMax;
				pPoly->m_MinMax.GetAsFloats(vMin, vMax);
				DrawBoxFromMinMax(vMin, vMax, D3DCOLOR_RGBA(255, 50, 50, 255));
			}
		}
	}
	if(m_bDrawMinMaxOfObjects)
	{
		for(s32 o=0; o<PATHSERVER_MAX_NUM_DYNAMIC_OBJECTS; o++)
		{
			TDynamicObject & obj = CPathServer::m_PathServerThread.m_DynamicObjectsStore[o];
			if(obj.m_bIsActive)
			{
				Vector3 vMin, vMax;
				obj.m_MinMax.GetAsFloats(vMin, vMax);
				DrawBoxFromMinMax(vMin, vMax, D3DCOLOR_RGBA(50, 255, 50, 255));
			}
		}
	}
	if(m_bDrawObjectsGridCellMinMax)
	{
		for(s32 g=0; g<CDynamicObjectsContainer::GetNumGrids(); g++)
		{
			const CDynamicObjectsGridCell * pGrid = CDynamicObjectsContainer::GetGridCell(g);
			Vector3 vMin, vMax;
			pGrid->m_MinMaxOfObjectsInGrid.GetAsFloats(vMin, vMax);
			DrawBoxFromMinMax(vMin, vMax, D3DCOLOR_RGBA(50, 50, 255, 255));
		}
	}
	if(m_bDrawPolyCentroids)
	{
		CNavMesh * pNavMesh = CPathServer::GetNavMeshFromIndex( CPathServer::GetNavMeshIndexFromPosition(m_vViewPos, domain), domain );
		if(pNavMesh)
			DrawPolyPrecalcCentroids(pNavMesh);
	}
	if(m_bDrawPolyAddresses)
	{
		CNavMesh * pNavMesh = CPathServer::GetNavMeshFromIndex( CPathServer::GetNavMeshIndexFromPosition(m_vViewPos, domain), domain );
		if(pNavMesh)
			DrawPolyAddresses(pNavMesh);
		DrawPolyAddresses(fwPathServer::GetTessellationNavMesh());
	}

	if(m_bDrawPolyCosts)
	{
		CNavMesh * pNavMesh = CPathServer::GetNavMeshFromIndex( CPathServer::GetNavMeshIndexFromPosition(m_vViewPos, domain), domain );
		if(pNavMesh)
			DrawPolyCosts(pNavMesh);
		DrawPolyCosts(fwPathServer::GetTessellationNavMesh());
	}

	if(m_bDrawPolyDistanceTravelled)
	{
		CNavMesh * pNavMesh = CPathServer::GetNavMeshFromIndex( CPathServer::GetNavMeshIndexFromPosition(m_vViewPos, domain), domain );
		if(pNavMesh)
			DrawPolyDistanceTravelled(pNavMesh);
		DrawPolyDistanceTravelled(fwPathServer::GetTessellationNavMesh());
	}

	if(m_bDrawPointsInPolys)
	{
		CNavMesh * pNavMesh = CPathServer::GetNavMeshFromIndex( CPathServer::GetNavMeshIndexFromPosition(m_vViewPos, domain), domain );
		if(pNavMesh)
			DrawPointsInPolys(pNavMesh);
		DrawPointsInPolys(fwPathServer::GetTessellationNavMesh());
	}

	if(m_bDrawPolyParents)
	{
		CNavMesh * pNavMesh = CPathServer::GetNavMeshFromIndex( CPathServer::GetNavMeshIndexFromPosition(m_vViewPos, domain), domain );
		if(pNavMesh)
			DrawPolyParents(pNavMesh);
		DrawPolyParents(fwPathServer::GetTessellationNavMesh());
	}

	if(ms_pDebugPoly)
	{
		if(m_bDrawDebugPoly)
		{
			DrawNavMeshSinglePoly(ms_pDebugPoly);
		}

		if(m_bDrawDebugPolyTraceback)
		{
			TNavMeshPoly * pCurr = ms_pDebugPoly;
			while(pCurr)
			{
				DrawNavMeshSinglePoly(pCurr);
				pCurr = pCurr->GetPathParentPoly();
			}
		}
	}

	if(m_bDrawFreeSpaceBeyondPolyEdges || m_bDrawFreeSpaceAroundVertices)
	{
		RenderSpaceBeyondPolygonEdges();
	}

	// unlock vertex buffers
	UnlockCollTriVB();
	UnlockLineSegVB();

	// Begin the scene
	m_pD3DDevice->BeginScene();

	// render all the vertex buffers, etc.
	DrawFrame();

	DrawText(fSecs);

	if(m_bRenderPathStats)
	{
		DisplayPathStats();
	}

	// End the scene
	m_pD3DDevice->EndScene();

	//****************************************
	// End of drawing stuff.
	//****************************************

	retval = m_pD3DDevice->Present(NULL, NULL, NULL, NULL);
	if(retval != D3D_OK)
	{
		D3DError(retval);
		return false;
	}

	if(m_bPrintRenderInfo)
	{
		PrintRenderInfo();
	}

	// copy keys array into last keys array
	memcpy(m_LastKeys, m_Keys, 256);

	return true;
}

int CNavGenApp::DrawString(const Vector3 & vPos, char * txt, DWORD textCol)
{
	RECT textRect;
	D3DXVECTOR3 vScreenPos;
	D3DXVECTOR3 vVertexPos;
	vVertexPos.x = vPos.x;
	vVertexPos.y = vPos.y;
	vVertexPos.z = vPos.z;
	D3DXVec3Project(&vScreenPos, &vVertexPos, &m_D3DViewPort, &m_ProjectionMatrix, &m_ViewMatrix, NULL);

	if(vScreenPos.z > 0.0f)
	{
		textRect.left = (int)vScreenPos.x;
		textRect.right = 0;
		textRect.top = (int)vScreenPos.y;
		textRect.bottom = 0;

		m_pD3DXFont->DrawText(NULL, txt, -1, &textRect, DT_CALCRECT | DT_LEFT | DT_TOP, textCol);
		m_pD3DXFont->DrawText(NULL, txt, -1, &textRect, DT_LEFT | DT_TOP, textCol);

		return textRect.bottom;
	}

	return 0;
}

int CNavGenApp::DrawString(int x, int y, char * txt, DWORD textCol)
{
	RECT textRect;
	textRect.left = x;
	textRect.top = y;
	textRect.right = 0;
	textRect.bottom = 0;

	m_pD3DXFont->DrawText(NULL, txt, -1, &textRect, DT_CALCRECT | DT_LEFT | DT_TOP, textCol);
	m_pD3DXFont->DrawText(NULL, txt, -1, &textRect, DT_LEFT | DT_TOP, textCol);

	return textRect.bottom;
}

void CNavGenApp::DrawText(float fSecs)
{
	fSecs;

	RECT textRect;
	textRect.left = 0;
	textRect.top = 0;
	textRect.right = 0;
	textRect.bottom = 0;

	D3DCOLOR textCol = D3DCOLOR_COLORVALUE(1.0f, 1.0f, 1.0f, 1.0f);

	char textBuf[256];

	sprintf(textBuf, "CameraPos (%.2f, %.2f, %.2f)", m_vViewPos.x, m_vViewPos.y, m_vViewPos.z);
	m_pD3DXFont->DrawText(NULL, textBuf, -1, &textRect, DT_CALCRECT | DT_LEFT | DT_TOP, textCol);
	m_pD3DXFont->DrawText(NULL, textBuf, -1, &textRect, DT_LEFT | DT_TOP, textCol);
	textRect.top = textRect.bottom;

	sprintf(textBuf, "StartPos (%.2f, %.2f, %.2f)", m_vNavMeshPathSearchStartPos.x, m_vNavMeshPathSearchStartPos.y, m_vNavMeshPathSearchStartPos.z);
	m_pD3DXFont->DrawText(NULL, textBuf, -1, &textRect, DT_CALCRECT | DT_LEFT | DT_TOP, textCol);
	m_pD3DXFont->DrawText(NULL, textBuf, -1, &textRect, DT_LEFT | DT_TOP, textCol);
	textRect.top = textRect.bottom;

	sprintf(textBuf, "EndPos (%.2f, %.2f, %.2f)", m_vNavMeshPathSearchEndPos.x, m_vNavMeshPathSearchEndPos.y, m_vNavMeshPathSearchEndPos.z);
	m_pD3DXFont->DrawText(NULL, textBuf, -1, &textRect, DT_CALCRECT | DT_LEFT | DT_TOP, textCol);
	m_pD3DXFont->DrawText(NULL, textBuf, -1, &textRect, DT_LEFT | DT_TOP, textCol);
	textRect.top = textRect.bottom;

	char * pCursorTxt;
	switch(m_CursorMode)
	{
		case eCursorMovesSelectedObject:
			pCursorTxt = "Cursor-keys move objects";
			break;
		case eCursorMovesStartPoint:
			pCursorTxt = "Cursor-keys move path start-pos";
			break;
		case eCursorMovesEndPoint:
			pCursorTxt = "Cursor-keys move path end-pos";
			break;
		default:
			pCursorTxt = NULL;
			break;
	}
	if(pCursorTxt)
	{
		m_pD3DXFont->DrawText(NULL, pCursorTxt, -1, &textRect, DT_CALCRECT | DT_LEFT | DT_TOP, textCol);
		m_pD3DXFont->DrawText(NULL, pCursorTxt, -1, &textRect, DT_LEFT | DT_TOP, textCol);
		textRect.top = textRect.bottom;
	}

	sprintf(textBuf, "SelecObject : 0x%x", m_pSelDynObj);
	m_pD3DXFont->DrawText(NULL, textBuf, -1, &textRect, DT_CALCRECT | DT_LEFT | DT_TOP, textCol);
	m_pD3DXFont->DrawText(NULL, textBuf, -1, &textRect, DT_LEFT | DT_TOP, textCol);
	textRect.top = textRect.bottom;

	if(m_iDrawNavMeshSinglePolyIndex != NAVMESH_POLY_INDEX_NONE)
	{
		sprintf(textBuf, "DrawSingleNavmeshPoly : %i", m_iDrawNavMeshSinglePolyIndex);
		m_pD3DXFont->DrawText(NULL, textBuf, -1, &textRect, DT_CALCRECT | DT_LEFT | DT_TOP, textCol);
		m_pD3DXFont->DrawText(NULL, textBuf, -1, &textRect, DT_LEFT | DT_TOP, textCol);
		textRect.top = textRect.bottom;
	}
}


bool
CNavGenApp::CreateOctree(void)
{
	CNavGenParams params;
	params.m_vAreaMin					= m_vWorldMin;
	params.m_vAreaMax					= m_vWorldMax;
	params.m_vUpVector					= Vector3(0, 0, 1.0f);

	params.m_vSectorMins				= m_vSectorMins;
	params.m_vSectorMaxs				= m_vSectorMaxs;

	//params.m_fDistanceToInitiallySeedNodes	= 50.0f / 75.0f;
	params.m_fDistanceToInitiallySeedNodes	= m_fDistanceToSeedNodes;
	params.m_TriangulationMaxHeightDiff = m_TriangulationMaxHeightDiff;
	params.m_HeightAboveNodeBaseAtWhichToTriangulate = m_HeightAboveNodeBaseAtWhichToTriangulate;
	params.m_MaxHeightChangeUnderTriangleEdge = m_MaxHeightChangeUnderTriangleEdge;
	params.m_bFixJaggiesAfterTriangulation = m_bFixJaggiesAfterTriangulation;
	params.m_bMoveNodesAwayFromWalls = m_bMoveNodesAwayFromWalls;

	if(!g_App->m_NodeGenerator.m_CollisionTriangles.size())
	{
		params.m_fDistanceToInitiallySeedNodes = 4.0f;
		params.m_fMaxTriangleSideLength = 32.0f;
		params.m_fMaxTriangleArea = 160.0f;
		OutputDebugString("Water-only navmesh, using large triangulation params.\n");
	}

#ifdef DEBUG_OCTREE_CONSTRUCTION
	params.m_bAlsoStoreSplitPolysInOctreeLeaves = true;
#else
	params.m_bAlsoStoreSplitPolysInOctreeLeaves = false;
#endif

	time_t startTime;
	time(&startTime);
	_strtime(tmpBuf);
	u32 msStartTime = timeGetTime();

	OutputDebugString("\n");
	OutputDebugString("//********************************************************\n");

	OutputDebugString("// Started building octree at : ");
	OutputDebugString(tmpBuf);
	OutputDebugString("\n");

	//**************************************************************
	// Create the node-generator & build the octree of the geometry
	//**************************************************************

	m_NodeGenerator.Init(&params);

	//************************************
	// Apply overrides ?

	if(m_fTestClearHeightInTriangulation >= 0.0f)
		m_NodeGenerator.m_fTestClearHeightInTriangulation =  m_fTestClearHeightInTriangulation;

	//*******************
	// Create octree..

	m_NodeGenerator.InitOctree();

	Vector3 vMin, vMax;
	m_NodeGenerator.m_Octree.GetOctreeExtents(vMin, vMax);
	m_vViewPos = (vMin + vMax) * 0.5f;

	time_t endTime;
	time(&endTime);
	_strtime(tmpBuf);
	u32 msEndTime = timeGetTime();

	OutputDebugString("// Finished building octree at : ");
	OutputDebugString(tmpBuf);
	OutputDebugString("\n");

	u32 msDelta = msEndTime - msStartTime;
	float fSecs = ((float)msDelta) / 1000.0f;

	sprintf(tmpBuf, "// timeGetTime() says it took : %.2f secs", fSecs);
	OutputDebugString(tmpBuf);
	OutputDebugString("\n");

	sprintf(tmpBuf, "// Num internal nodes : %i\n", m_NodeGenerator.m_Octree.m_iTotalNumInternalNodes);
	OutputDebugString(tmpBuf);
	sprintf(tmpBuf, "// Num leaves : %i\n", m_NodeGenerator.m_Octree.m_iTotalNumLeaves);
	OutputDebugString(tmpBuf);
	sprintf(tmpBuf, "// Maximum depth : %i\n", m_NodeGenerator.m_Octree.m_iMaximumDepth);
	OutputDebugString(tmpBuf);

	OutputDebugString("//********************************************************\n");
	OutputDebugString("\n");

	return true;
}

/*
void
CDebug::DebugMessage(const char * fmt, ...)
{

}
*/


void
CNavGenApp::PrintRenderInfo(void)
{
	char tmpBuf[256];

	OutputDebugString("//********************************************************\n");
	OutputDebugString("// Render Info\n");

	sprintf(tmpBuf, "// ViewPos : (%.2f, %.2f, %.2f)\n", m_vViewPos.x, m_vViewPos.y, m_vViewPos.z);
	OutputDebugString(tmpBuf);

	sprintf(tmpBuf, "// Num Nodes Visited : %i\n", m_iNumNodesVisitedForRender);
	OutputDebugString(tmpBuf);

	sprintf(tmpBuf, "// Num Leaves Visited : %i\n", m_iNumLeavesVisitedForRender);
	OutputDebugString(tmpBuf);

	sprintf(tmpBuf, "// Num Visible Tris : %i\n", m_iNumVertsInCollisionPolyVB/3);
	OutputDebugString(tmpBuf);

	sprintf(tmpBuf, "// Num Nav-Surface Triangles : %i\n", m_iNumNavMeshTrisRenderedThisFrame);
	OutputDebugString(tmpBuf);

	OutputDebugString("//********************************************************\n");

}

void
CNavGenApp::RunExhaustiveTesting(void)
{
	if(!m_bExhaustiveTesting)
		return;

	bool bRequestNewPath = true;

	if(m_hExhaustiveTestingPathHandle)
	{
		if(CPathServer::IsRequestResultReady(m_hExhaustiveTestingPathHandle))
		{
			EPathServerErrorCode ePathRet = GetPathResult(m_hExhaustiveTestingPathHandle);
			m_hExhaustiveTestingPathHandle = PATH_HANDLE_NULL;

			// And now we want to rebuild all the navmesh vertex buffers for navmeshes, and
			// also for the CPathServer::m_pTessellationNavMesh - if we did any tessellation.
			MakeVertexBufferForTessellationNavMesh();

			if(ePathRet != PATH_FOUND)
			{
				m_bExhaustiveTesting = false;
				return;
			}
		}
		else
		{
			bRequestNewPath = false;
		}
	}
	if(bRequestNewPath)
	{
		Vector3 vStartPos,vEndPos;
		do 
		{
			vStartPos.x = fwRandom::GetRandomNumberInRange(-m_fExhaustiveRadius, m_fExhaustiveRadius);
			vStartPos.y = fwRandom::GetRandomNumberInRange(-m_fExhaustiveRadius, m_fExhaustiveRadius);
			vStartPos.z = 0.0f;
		} while(vStartPos.Mag() > m_fExhaustiveRadius);

		vStartPos += m_vExhaustiveTestOrigin;
		//vStartPos.z += 2.0f;

		do 
		{
			vEndPos.x = fwRandom::GetRandomNumberInRange(-m_fExhaustiveRadius, m_fExhaustiveRadius);
			vEndPos.y = fwRandom::GetRandomNumberInRange(-m_fExhaustiveRadius, m_fExhaustiveRadius);
			vEndPos.z = 0.0f;
		} while(vEndPos.Mag() > m_fExhaustiveRadius);

		vEndPos += m_vExhaustiveTestOrigin;
		//vEndPos.z += 2.0f;

		u32 iFlags = 0;
		float fCompletionRadius = 1.0f;

		m_vNavMeshPathSearchStartPos = vStartPos;
		m_vNavMeshPathSearchEndPos = vEndPos;

		m_hExhaustiveTestingPathHandle = CPathServer::RequestPath(vStartPos, vEndPos, iFlags, fCompletionRadius);
	}
}


EPathServerErrorCode
CNavGenApp::GetPathResult(TPathHandle hPath)
{
	Vector3 * pPts = NULL;
	TNavMeshWaypointFlag * pWptFlags = NULL;

	const aiNavDomain domain = kNavDomainRegular;

	EPathServerErrorCode iCompletionCode = CPathServer::LockPathResult(m_hExhaustiveTestingPathHandle, &m_iNavMeshNumPathPoints, pPts, pWptFlags);

	if(iCompletionCode != PATH_FOUND)
	{
		bool bNoPathFound = true;
		bNoPathFound;
	}

	else if(iCompletionCode == PATH_FOUND)
	{
		m_vNavMeshPathPoints.Reset();
		m_iNavMeshPathWaypointFlags.Reset();
		m_iNavMeshNumTriPts = 0;

		for(int p=0; p<m_iNavMeshNumPathPoints; p++)
		{
			m_vNavMeshPathPoints.PushAndGrow(pPts[p]);
			m_iNavMeshPathWaypointFlags.PushAndGrow(pWptFlags[p]);
		}

		// HACK. To get the path polys & points for later.
		for(int i=0; i<MAX_NUM_PATH_REQUESTS; i++)
		{
			CPathRequest * pReq = &CPathServer::m_PathRequests[i];
			if(pReq->m_hHandle == hPath)
			{
				m_iNavMeshNumTriPts = 0;
				m_iNavMeshNumPointsInPolys = pReq->m_iNumInitiallyFoundPolys;

				for(u32 p=0; p<pReq->m_iNumInitiallyFoundPolys; p++)
				{
					m_vNavMeshPointsInPolys[p] = pReq->m_InitiallyFoundPathPointInPolys[p];
					TNavMeshPoly * pPoly = pReq->m_InitiallyFoundPathPolys[p];
					CNavMesh * pNavMesh = CPathServer::GetNavMeshFromIndex(pPoly->GetNavMeshIndex(), domain);

					Vector3 v0,v1,v2;
					pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, 0), v0);

					for(u32 v=1; v<pPoly->GetNumVertices()-1; v++)
					{
						pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, v), v1);
						pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, v+1), v2);

						m_vNavMeshPolyTriVerts[ m_iNavMeshNumTriPts++ ] = v2;
						m_vNavMeshPolyTriVerts[ m_iNavMeshNumTriPts++ ] = v1;
						m_vNavMeshPolyTriVerts[ m_iNavMeshNumTriPts++ ] = v0;

						if(m_iNavMeshNumTriPts >= 4096-3)
							break;
					}
				}

				char tmp[256];

				// Also print out some debug info about the path that we got :
				sprintf(tmp, "Path was %i points\n", m_iNavMeshNumPathPoints);
				OutputDebugString(tmp);

				sprintf(tmp, "Num polys in the initial triangle path were %i\n", pReq->m_iNumInitiallyFoundPolys);
				OutputDebugString(tmp);

				sprintf(tmp, "Total num polys visited was %i\n", pReq->m_iNumVisitedPolygons);
				OutputDebugString(tmp);

				sprintf(tmp, "Time taken to find the start and end polys was %.02f\n", pReq->m_fMillisecsToFindPolys);
				OutputDebugString(tmp);

				sprintf(tmp, "Time taken to find the polygon path was %.02f\n", pReq->m_fMillisecsToFindPath);
				OutputDebugString(tmp);

				sprintf(tmp, "Time taken to refine the path was %.02f\n", pReq->m_fMillisecsToRefinePath);
				OutputDebugString(tmp);

				sprintf(tmp, "Time taken to minimise the path dist was %.02f\n", pReq->m_fMillisecsToMinimisePathLength);
				OutputDebugString(tmp);

				sprintf(tmp, "Current num tessellation polys in CPathServer is %i\n", CPathServer::m_iCurrentNumTessellationPolys);
				OutputDebugString(tmp);

				sprintf(tmp, "Overall time to process this request : %.3f\n", pReq->m_fTotalProcessingTimeInMillisecs);
				OutputDebugString(tmp);

				if(pReq->m_fTotalProcessingTimeInMillisecs > 200.0f)
				{
					iCompletionCode = PATH_NOT_FOUND;
					OutputDebugString(">> THAT WAS WAAAAY TO LONG TO FIND THE PATH, WHAT'S GOING ON??\n");
				}

				break;
			}
		}
	}

	CPathServer::UnlockPathResultAndClear(hPath);

	return iCompletionCode;
}

//********************************************************************************
// Panic stations - this function makes sure that the the adjacencies are kosher

void
CNavGenApp::TestAllAdjacencies()
{
	for(int n=0; n<NAVMESH_MAX_MAP_INDEX; n++)
	{
		CNavMesh * pNavMesh = CPathServer::m_pNavMeshStores[kNavDomainRegular]->GetMeshByIndex(n);
		if(pNavMesh)
		{
			for(u32 a=0; a<pNavMesh->GetSizeOfPools(); a++)
			{
				ASSERT_ONLY( const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly(a); )
				Assert(adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes())==adjPoly.GetOriginalNavMeshIndex(pNavMesh->GetAdjacentMeshes()));
				Assert(adjPoly.GetPolyIndex()==adjPoly.GetOriginalPolyIndex());
			}
		}
	}
}

void CNavGenApp::MoveNodesAwayFromWalls()
{
	g_App->m_NodeGenerator.MoveNodesAwayFromWalls();
	g_App->m_bNavSurfaceHasChanged = true;
}

bool IsExteriorBoundaryEdgeFN(const CNavSurfaceTri * pTri, const s32 iEdge)
{
	return (pTri->m_AdjacentTris[iEdge] == NULL);
}

void CNavGenApp::ResetEdgeIterator()
{
	g_App->m_TestEdgeIterator.Reset();

	// Reset AA flags on all polygon edges
	for(s32 t=0; t<g_App->m_NodeGenerator.m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = g_App->m_NodeGenerator.m_NavSurfaceTriangles[t];
		pTri->m_AntiAliasPass[0] = 0;
		pTri->m_AntiAliasPass[1] = 0;
		pTri->m_AntiAliasPass[2] = 0;
	}
}

void CNavGenApp::TestBoundaryEdgeIteratorFromSelectedTriangle()
{
	if(!g_App->m_NodeGenerator.m_NavSurfaceTriangles.size())
		return;

	if(!g_App->m_pSelectedNavSurfaceTri)
		return;

	if( !g_App->m_TestEdgeIterator.GetStarting().m_pTri )
	{
		// Reset AA flags on all polygon edges
		for(s32 t=0; t<g_App->m_NodeGenerator.m_NavSurfaceTriangles.size(); t++)
		{
			CNavSurfaceTri * pTri = g_App->m_NodeGenerator.m_NavSurfaceTriangles[t];
			if(pTri->IsRemoved())
				continue;

			for(s32 e=0; e<3; e++)
				pTri->m_AntiAliasPass[e] = 0;
		}

		s32 iStartEdge = -1;
		for(s32 e=0; e<3; e++)
		{
			if( DefaultIsBoundaryEdgeFN( g_App->m_pSelectedNavSurfaceTri, e ) )
			{
				iStartEdge = e;
				break;
			}
		}

		g_App->m_TestEdgeIterator.Init( g_App->m_pSelectedNavSurfaceTri, iStartEdge, DefaultIsBoundaryEdgeFN );
	}
	else
	{
		g_App->m_TestEdgeIterator.Step(1);
	}
}

void CNavGenApp::TestPavementEdgeIteratorFromSelectedTriangle()
{
	if(!g_App->m_NodeGenerator.m_NavSurfaceTriangles.size())
		return;

	if(!g_App->m_pSelectedNavSurfaceTri)
		return;

	if( !g_App->m_TestEdgeIterator.GetStarting().m_pTri )
	{
		// Reset AA flags on all polygon edges
		for(s32 t=0; t<g_App->m_NodeGenerator.m_NavSurfaceTriangles.size(); t++)
		{
			CNavSurfaceTri * pTri = g_App->m_NodeGenerator.m_NavSurfaceTriangles[t];
			if(pTri->IsRemoved())
				continue;

			for(s32 e=0; e<3; e++)
				pTri->m_AntiAliasPass[e] = 0;
		}

		s32 iStartEdge = -1;
		for(s32 e=0; e<3; e++)
		{
			if( PavementIsBoundaryEdgeFN( g_App->m_pSelectedNavSurfaceTri, e ) )
			{
				iStartEdge = e;
				break;
			}
		}

		g_App->m_TestEdgeIterator.Init( g_App->m_pSelectedNavSurfaceTri, iStartEdge, PavementIsBoundaryEdgeFN );
	}
	else
	{
		g_App->m_TestEdgeIterator.Step(1);
	}
}


void CNavGenApp::StitchMapSwapForEntireMap()
{
	s32 x,y;

	for(y=0; y<CPathServerExtents::m_iNumNavMeshesInY; y++)
	{
		for(x=0; x<CPathServerExtents::m_iNumNavMeshesInX; x++)
		{
			CNavMesh * pNavMesh = CPathServer::GetNavMeshFromIndex( (y * CPathServerExtents::m_iNumNavMeshesInX) + x, kNavDomainRegular );
			if(!pNavMesh)
				continue;
			bool bMainDLC = (pNavMesh->GetFlags() & NAVMESH_DLC) != 0;

			CNavMesh * pAdjacent[4] = { NULL, NULL, NULL, NULL };
			const eNavMeshEdge iAdjSides[4] = { eNegX, ePosX, eNegY, ePosY };

			if(x > 0)
			{
				pAdjacent[0] = CPathServer::GetNavMeshFromIndex( (y * CPathServerExtents::m_iNumNavMeshesInX) + (x - 1), kNavDomainRegular );
			}
			if(x < CPathServerExtents::m_iNumNavMeshesInX-1)
			{
				pAdjacent[1] = CPathServer::GetNavMeshFromIndex( (y * CPathServerExtents::m_iNumNavMeshesInX) + (x + 1), kNavDomainRegular );
			}
			if(y > 0)
			{
				pAdjacent[2] = CPathServer::GetNavMeshFromIndex( ( (y - 1) * CPathServerExtents::m_iNumNavMeshesInX) + x, kNavDomainRegular );
			}
			if(y < CPathServerExtents::m_iNumNavMeshesInY-1)
			{
				pAdjacent[3] = CPathServer::GetNavMeshFromIndex( ( (y + 1) * CPathServerExtents::m_iNumNavMeshesInX) + x, kNavDomainRegular );
			}

			for(s32 a=0; a<4; a++)
			{
				if(pAdjacent[a])
				{
					bool bAdjacentDLC = (pAdjacent[a]->GetFlags() & NAVMESH_DLC) != 0;

					if(bMainDLC == bAdjacentDLC)
						continue;

					StitchAcrossMapSwap( bMainDLC ? pAdjacent[a] : pNavMesh, bMainDLC ? pNavMesh : pAdjacent[a], bMainDLC ? iAdjSides[a] : GetOppositeEdge(iAdjSides[a]) );
				}
			}
		}
	}
}

bool VertexLiesAlongEdge(const Vector3 & vMin, const Vector3 & vMax, const Vector3 & vPoint, const eNavMeshEdge iEdge)
{
	const float fEps = 1.0f;

	if(iEdge==eNegX)
	{
		return IsClose( vPoint.x, vMin.x, fEps );
	}
	else if(iEdge==ePosX)
	{
		return IsClose( vPoint.x, vMax.x, fEps );
	}
	else if(iEdge==eNegY)
	{
		return IsClose( vPoint.y, vMin.y, fEps );
	}
	else
	{
		return IsClose( vPoint.y, vMax.y, fEps );
	}
}

bool IsStitchVertSame(const Vector3 & v1, const Vector3 & v2, const float fEpsXYSqr, const float fEpsZ)
{
	if( (v1.x - v2.x) * (v1.y - v2.y) < fEpsXYSqr )
	{
		if( Abs( v1.z - v2.z ) < fEpsZ )
		{
			return true;
		}
	}
	return false;
}

void CNavGenApp::StitchAcrossMapSwap(CNavMesh * pMainMapMesh, CNavMesh * pSwapMesh, eNavMeshEdge iSideOfMainMapMesh)
{
	Assert( (pMainMapMesh->GetFlags()&NAVMESH_DLC)==0 );
	Assert( (pSwapMesh->GetFlags()&NAVMESH_DLC)!=0 );

	const float fSameVertEpsXYSqr = square(0.05f);
	const float fSameVertEpsZ = 0.25f;

	s32 p1,p2;
	s32 a1,a2;

	//-------------------------------------------------------------------------------
	// Disable all non-standard adjacencies from the pMainMapMesh to the pSwapMesh

	for(a1=0; a1<pMainMapMesh->GetSizeOfPools(); a1++)
	{
		TAdjPoly * pAdj = pMainMapMesh->GetAdjacentPolysArray().Get(a1);

		if( pAdj->GetOriginalNavMeshIndex(pMainMapMesh->GetAdjacentMeshes()) == pSwapMesh->GetIndexOfMesh() )
		{
			if( pAdj->GetAdjacencyType() == ADJACENCY_TYPE_NORMAL )
			{
				pAdj->SetOriginalNavMeshIndex( NAVMESH_NAVMESH_INDEX_NONE, pMainMapMesh->GetAdjacentMeshes() );
				pAdj->SetNavMeshIndex( NAVMESH_NAVMESH_INDEX_NONE, pMainMapMesh->GetAdjacentMeshes() );
				pAdj->SetOriginalPolyIndex( NAVMESH_POLY_INDEX_NONE );
				pAdj->SetPolyIndex( NAVMESH_POLY_INDEX_NONE );
			}
			else
			{
				pAdj->SetAdjacencyDisabled(true);
			}
		}
	}

	//------------------------------------------------------------------
	// Disable all special-links from the pMainMapMesh to the pSwapMesh

	for(s32 s=0; s<pMainMapMesh->GetNumSpecialLinks(); s++)
	{
		CSpecialLinkInfo & specialLink = pMainMapMesh->GetSpecialLinks()[s];
		if(specialLink.GetOriginalLinkToNavMesh() == pSwapMesh->GetIndexOfMesh())
		{
			specialLink.SetIsDisabled(true);
		}
	}

	//-------------------------------------------
	// Retarget matching adjacencies along edge

	const Vector3 vMin = pMainMapMesh->GetNonResourcedData()->m_vMinOfNavMesh;
	const Vector3 vMax = vMin + pMainMapMesh->GetExtents();

	Vector3 vPoly1_v1, vPoly1_v2;
	Vector3 vPoly2_v1, vPoly2_v2;

	for(p1=0; p1<pMainMapMesh->GetNumPolys(); p1++)
	{
		TNavMeshPoly * pPoly1 = pMainMapMesh->GetPoly(p1);
		if(pPoly1->GetLiesAlongEdgeOfMesh())
		{
			for(a1=0; a1<pPoly1->GetNumVertices(); a1++)
			{
				TAdjPoly * pAdj1 = pMainMapMesh->GetAdjacentPolysArray().Get( pPoly1->GetFirstVertexIndex() + a1 );
				if(pAdj1->GetIsExternalEdge())
				{
					bool bAdjacencyFound = false;

					pMainMapMesh->GetVertex( pMainMapMesh->GetPolyVertexIndex(pPoly1, a1), vPoly1_v1 );

					/*
					if(!VertexLiesAlongEdge(vMin, vMax, vPoly1_v1, iSideOfMainMapMesh))
						continue;
					*/

					pMainMapMesh->GetVertex( pMainMapMesh->GetPolyVertexIndex(pPoly1, (a1+1)%pPoly1->GetNumVertices()), vPoly1_v2 );

					/*
					if(!VertexLiesAlongEdge(vMin, vMax, vPoly1_v2, iSideOfMainMapMesh))
						continue;
					*/

					for(p2=0; p2<pSwapMesh->GetNumPolys(); p2++)
					{
						TNavMeshPoly * pPoly2 = pSwapMesh->GetPoly(p2);
						if(pPoly2->GetIsZeroAreaStichPolyDLC())
						{
							for(a2=0; a2<pPoly2->GetNumVertices(); a2++)
							{
								TAdjPoly * pAdj2 = pSwapMesh->GetAdjacentPolysArray().Get( pPoly2->GetFirstVertexIndex() + a2 );
								if(pAdj2->GetIsExternalEdge()) // && pAdj2->GetNavMeshIndex(pSwapMesh->GetAdjacentMeshes())==NAVMESH_NAVMESH_INDEX_NONE)
								{
									pSwapMesh->GetVertex( pSwapMesh->GetPolyVertexIndex(pPoly2, a2), vPoly2_v1 );

									/*
									if(!VertexLiesAlongEdge(vMin, vMax, vPoly2_v1, iSideOfMainMapMesh))
										continue;
									*/

									pSwapMesh->GetVertex( pSwapMesh->GetPolyVertexIndex(pPoly2, (a2+1)%pPoly2->GetNumVertices()), vPoly2_v2 );

									/*
									if(!VertexLiesAlongEdge(vMin, vMax, vPoly2_v2, iSideOfMainMapMesh))
										continue;
									*/

									if( IsStitchVertSame( vPoly1_v1, vPoly2_v2, fSameVertEpsXYSqr, fSameVertEpsZ ) && IsStitchVertSame( vPoly1_v2, vPoly2_v1, fSameVertEpsXYSqr, fSameVertEpsZ ) )
									{
										pAdj1->SetOriginalNavMeshIndex( pSwapMesh->GetIndexOfMesh(), pMainMapMesh->GetAdjacentMeshes() );
										pAdj1->SetNavMeshIndex( pSwapMesh->GetIndexOfMesh(), pMainMapMesh->GetAdjacentMeshes() );
										pAdj1->SetOriginalPolyIndex( p2 );
										pAdj1->SetPolyIndex( p2 );
										pAdj1->SetAdjacencyType( ADJACENCY_TYPE_NORMAL );

										pAdj2->SetOriginalNavMeshIndex( pMainMapMesh->GetIndexOfMesh(), pSwapMesh->GetAdjacentMeshes() );
										pAdj2->SetNavMeshIndex( pMainMapMesh->GetIndexOfMesh(), pSwapMesh->GetAdjacentMeshes() );
										pAdj2->SetOriginalPolyIndex( p1 );
										pAdj2->SetPolyIndex( p1 );
										pAdj2->SetAdjacencyType( ADJACENCY_TYPE_NORMAL );

										break;
									}
								}
							}
						}
					}
				}
			}
		}
	}
}



















void
InitRageStuff(void)
{
	//sysParam::Init(0, NULL);
	//CFileMgr::Initialise();
	//CNavGen::InitSurfaceInfoTable();

	INIT_PARSER;
}
void
ShutdownRageStuff(void)
{
	SHUTDOWN_PARSER;
}

bool Main_Prologue() { return true; }
bool Main_OneLoopIteration() { return true; }
void Main_Epilogue() { }

#define USE_WINMAIN !__DEV


void StartDebuggerSupport()
{
	g_DebugData = new unsigned int[2];
	g_DebugData[0] = g_DebugData[1] = 0;

}

#if USE_WINMAIN
int Main()
{
	return 0;
}

int PASCAL WINAPI WinMain(HINSTANCE instance, HINSTANCE prev_instance, LPSTR cmd_line, int cmd_show)
{
	//Main();

#endif

#if !USE_WINMAIN
int Main(void)
{
	//InitGameHeap();
	//sysMemAllocator::SetCurrent( sysMemAllocator::GetMaster() );

#if __DEV
	// Enable the CRT to debug memory leak info at exit
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif
#endif

#if !USE_WINMAIN
	HINSTANCE instance = 0;
#endif

	// We have to make sure that there is a valid working directory.  When NavMeshMaker is run, it
	// should ideally have a full working directory specified in the args parsed below.
	_chdir(g_pGameFolder);

	InitRageStuff();

#if __DEV
	RAGE_LOG_DISABLE = 1;
#endif

	INIT_PARSER;
	fwNavGenConfigManagerImpl<CNavMeshGeneratorConfig>::InitClass("x:/gta5/tools/bin/navgen/NavMeshMaker/navgenconfig.xml");
//	const fwNavGenConfig& cfg = fwNavGenConfig::Get();

	CNavGenApp * pNodeGenApp = rage_new CNavGenApp();
	g_App = pNodeGenApp;
	pNodeGenApp->Init(instance);

	delete pNodeGenApp;
	pNodeGenApp = NULL;

	fwNavGenConfigManager::ShutdownClass();

	//ClipCursor(NULL);
	return 1;
}

