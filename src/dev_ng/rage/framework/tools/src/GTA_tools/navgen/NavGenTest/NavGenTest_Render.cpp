#include "NavGenTest_Win32.h"
#include "pathserver\NavMeshGta.h"

#if !__DEV
namespace rage
{
	bool ArrayAssertFailed(int)
	{
		return true;
	}
}
#endif

void
CNavGenApp::DrawFrame(void)
{
	HRESULT retval;

	if(m_bUseFogging)
	{
		m_pD3DDevice->SetRenderState(D3DRS_FOGENABLE, TRUE);
	}

	SetAlphaBlendEnable(false);

	if(!m_bPathServerInitialised && m_iNumVertsInCollisionPolyVB)
	{
		SetColourStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);
		SetAlphaStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);
		SetColourStage(1, D3DTOP_DISABLE, 0, 0);
		SetAlphaStage(1, D3DTOP_DISABLE, 0, 0);

		retval = m_pD3DDevice->SetStreamSource(0, m_pCollisionPolysVB, 0, sizeof(LitVertex));
		retval = m_pD3DDevice->SetFVF(LitVertexFVF);
		retval = m_pD3DDevice->DrawPrimitive(
			D3DPT_TRIANGLELIST,
			0,
			m_iNumVertsInCollisionPolyVB / 3
		);	
	}

	DrawBugriddenTris();

	// Draw selected tris & path tris
	if(m_pSelectedNavSurfaceTri)
	{
		DrawNavSurfaceTri(m_pSelectedNavSurfaceTri, D3DCOLOR_RGBA(64, 64, 255, 128), true, true, 0.05f);
	}
	if(m_pDestinationNavSurfaceTri)
	{
		DrawNavSurfaceTri(m_pDestinationNavSurfaceTri, D3DCOLOR_RGBA(64, 64, 255, 128), true);
	}
	
	if(m_PathTris.size())
	{
		DrawNavSurfaceTris(m_PathTris, D3DCOLOR_RGBA(255, 0, 255, 128));
	}

	SetAlphaBlendEnable(false);


	// Draw NavMeshes...

	/*
	if(m_pSingleNavMesh && m_bDisplayPathServerMesh)
	{
		DrawNavMesh(
			m_pSingleNavMesh,
			m_pSingleNavMeshTrianglesVB,		m_iSingleNavMeshNumTriangles,
			m_pSingleNavMeshLinesVB,			m_iSingleNavMeshNumLines,
			m_pSingleNavMeshAdjLinesVB,			m_iSingleNavMeshNumAdjLines,
			NULL, 0
		);
	}
	else
	*/
	{
		if(CPathServer::ms_bInitialised)
		{
			if(m_DynamicNavMesh)
			{
				DrawNavMesh( m_DynamicNavMesh, m_DynamicNavMeshVertexBuffer, false );
			}
			else
			{
				const aiNavDomain domain = kNavDomainRegular;
				for(s32 n=0; n<CPathServer::m_pNavMeshStores[domain]->GetMaxMeshIndex(); n++)
				{
					CNavMesh * pNavMesh = CPathServer::m_pNavMeshStores[domain]->GetMeshByIndex(n);

					if(pNavMesh && m_bDisplayPathServerMesh)
					{
						Vector3 vMid = (pNavMesh->GetQuadTree()->m_Mins + pNavMesh->GetQuadTree()->m_Maxs) / 2.0f;
						Vector3 vDiff = vMid - m_vViewPos;
						if(vDiff.XYMag() < 500.0f)
						{
							DrawNavMesh( pNavMesh, m_NavMeshVertexBuffers[n], false );
						}
					}
				}
			}
		}
	}

	{
		if(CPathServer::m_pTessellationNavMesh && m_NavMeshVertexBuffers[NAVMESH_INDEX_TESSELLATION].m_pNavMeshTrianglesVB && m_bDrawTessellationNavMesh)
		{
			DrawNavMesh( CPathServer::m_pTessellationNavMesh, m_NavMeshVertexBuffers[NAVMESH_INDEX_TESSELLATION], true );
		}

		// Draw the dynamic objects in the mesh
		TDynamicObject * pDynObj = CPathServer::m_PathServerThread.m_pFirstDynamicObject;
		Vector3 vRaiseVec(0,0,0.25f);

		Vector3 vTopPts[4];
		Vector3 vBottomPts[4];

		while(pDynObj)
		{
			u32 col;
			if(pDynObj == m_pSelDynObj)
			{
				col = 0xFFFFFFFF;
			}
			else
			{
				if(pDynObj->m_bIsOpenable)
				{
					col = 0xFF8080FF;
				}
				else
					col = 0xFF000000;
			}

			for(int p=0; p<4; p++)
			{
				vTopPts[p] = Vector3(pDynObj->m_vVertices[p].x, pDynObj->m_vVertices[p].y, pDynObj->m_Bounds.GetTopZ());
				vBottomPts[p] = Vector3(pDynObj->m_vVertices[p].x, pDynObj->m_vVertices[p].y, pDynObj->m_Bounds.GetBottomZ());
			}


			StartPolyLine(vTopPts[0], col);
			DrawPolyLineTo(vTopPts[1], col);
			DrawPolyLineTo(vTopPts[2], col);
			DrawPolyLineTo(vTopPts[3], col);
			DrawPolyLineTo(vTopPts[0], col);

			StartPolyLine(vBottomPts[0], col);
			DrawPolyLineTo(vBottomPts[1], col);
			DrawPolyLineTo(vBottomPts[2], col);
			DrawPolyLineTo(vBottomPts[3], col);
			DrawPolyLineTo(vBottomPts[0], col);

			StartPolyLine(vTopPts[0], col);
			DrawPolyLineTo(vBottomPts[0], col);
			StartPolyLine(vTopPts[1], col);
			DrawPolyLineTo(vBottomPts[1], col);
			StartPolyLine(vTopPts[2], col);
			DrawPolyLineTo(vBottomPts[2], col);
			StartPolyLine(vTopPts[3], col);
			DrawPolyLineTo(vBottomPts[3], col);

			pDynObj = pDynObj->m_pNext;
		}
	}
	if(m_bPathServerInitialised && m_iNumVertsInCollisionPolyVB)
	{
		SetAlphaBlendEnable(true);

		SetColourStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);
		SetAlphaStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);
		SetColourStage(1, D3DTOP_DISABLE, 0, 0);
		SetAlphaStage(1, D3DTOP_DISABLE, 0, 0);

		retval = m_pD3DDevice->SetStreamSource(0, m_pCollisionPolysVB, 0, sizeof(LitVertex));
		retval = m_pD3DDevice->SetFVF(LitVertexFVF);
		retval = m_pD3DDevice->DrawPrimitive(
			D3DPT_TRIANGLELIST,
			0,
			m_iNumVertsInCollisionPolyVB / 3
		);
	}

	if(m_iNumVertsInLineSegVB)
	{
		SetAlphaBlendEnable(true);

		SetColourStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);
		SetAlphaStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);
		SetColourStage(1, D3DTOP_DISABLE, 0, 0);
		SetAlphaStage(1, D3DTOP_DISABLE, 0, 0);

		// NB : It can be useful to turn ON alpha-blending here, since it makes it visible
		// where pathnodes have been duplicated as they show up double-bright.  This in fact
		// should not be occuring anymore, since the algorithm is now fixed.

//		SetSrcBlend(D3DBLEND_ONE);
//		SetDestBlend(D3DBLEND_ONE);

		retval = m_pD3DDevice->SetStreamSource(0, m_pLineSegVB, 0, sizeof(LitVertex));
		retval = m_pD3DDevice->SetFVF(LitVertexFVF);
		retval = m_pD3DDevice->DrawPrimitive(
			D3DPT_LINELIST,
			0,
			m_iNumVertsInLineSegVB / 2
		);

		//SetAlphaBlendEnable(false);
	}

#ifdef VISUALISE_NAVIGATION_SURFACE

	if(m_pNavSurfaceVertexBuffer && m_bDrawNavigationSurfaces)
	{
		SetColourStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);
		SetAlphaStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);
		SetColourStage(1, D3DTOP_DISABLE, 0, 0);
		SetAlphaStage(1, D3DTOP_DISABLE, 0, 0);

		SetAlphaBlendEnable(true);
		SetSrcBlend(D3DBLEND_SRCALPHA);
		SetDestBlend(D3DBLEND_INVSRCALPHA);

		if(!m_bToggleNavigationSurfaceSolid)
			m_pD3DDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);

		retval = m_pD3DDevice->SetStreamSource(0, m_pNavSurfaceVertexBuffer, 0, sizeof(LitVertex));
		retval = m_pD3DDevice->SetFVF(LitVertexFVF);
		retval = m_pD3DDevice->DrawPrimitive(
			D3DPT_TRIANGLELIST,
			0,
			m_iNumTrianglesInNavSurfaceVB
		);

		m_iNumNavMeshTrisRenderedThisFrame += m_iNumTrianglesInNavSurfaceVB;

		if(m_bToggleNavigationSurfaceSolid)
		{
			SetAlphaBlendEnable(false);
			SetColourStage(0, D3DTOP_SELECTARG1, D3DTA_TFACTOR, 0);
			SetAlphaStage(0, D3DTOP_SELECTARG1, D3DTA_TFACTOR, 0);
			m_pD3DDevice->SetRenderState(D3DRS_TEXTUREFACTOR, D3DCOLOR_RGBA(0, 0, 0, 255));

			m_pD3DDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);

			retval = m_pD3DDevice->DrawPrimitive(
				D3DPT_TRIANGLELIST,
				0,
				m_iNumTrianglesInNavSurfaceVB
			);

			m_pD3DDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
			SetColourStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);
			SetAlphaStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);

			m_iNumNavMeshTrisRenderedThisFrame += m_iNumTrianglesInNavSurfaceVB;
			SetAlphaBlendEnable(true);
		}

		SetAlphaBlendEnable(false);

		if(!m_bToggleNavigationSurfaceSolid)
			m_pD3DDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	}

	// Visualise the voxel map
	if(m_pVoxelMapVertexBuffer)
	{
		SetAlphaBlendEnable(false);
		SetColourStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);
		SetAlphaStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);
		//m_pD3DDevice->SetRenderState(D3DRS_TEXTUREFACTOR, D3DCOLOR_RGBA(0, 0, 0, 255));

		m_pD3DDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);

		retval = m_pD3DDevice->SetStreamSource(0, m_pVoxelMapVertexBuffer, 0, sizeof(LitVertex));
		retval = m_pD3DDevice->SetFVF(LitVertexFVF);

		retval = m_pD3DDevice->DrawPrimitive(
			D3DPT_TRIANGLELIST,
			0,
			m_iNumTrianglesInVoxelMapVB
			);

		m_pD3DDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
		SetColourStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);
		SetAlphaStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);

		//m_iNumNavMeshTrisRenderedThisFrame += m_iNumTrianglesInNavSurfaceVB;
		SetAlphaBlendEnable(true);
	}

#endif 

	if(m_bUseFogging)
	{
		m_pD3DDevice->SetRenderState(D3DRS_FOGENABLE, FALSE);
	}
}

void CNavGenApp::DrawNavMeshSinglePoly(TNavMeshPoly * pPoly)
{
	CNavMesh * pNavMesh = CPathServer::GetNavMeshFromIndex(pPoly->GetNavMeshIndex(), kNavDomainRegular);
	u32 iPoly = pNavMesh->GetPolyIndex(pPoly);
	DrawNavMeshSinglePoly(pNavMesh, iPoly, 0.25f, 0xaaFFFFFF, true);
}

void CNavGenApp::DrawNavMeshSinglePoly(CNavMesh * pNavMesh, const u32 iPolyIndex, const float fRaiseZ, const DWORD col, bool bBothSides)
{
	if(!pNavMesh || iPolyIndex >= pNavMesh->GetNumPolys())
		return;

	TNavMeshPoly * pPoly = pNavMesh->GetPoly(iPolyIndex);
	Vector3 vVerts[NAVMESHPOLY_MAX_NUM_VERTICES];
	
	for(s32 v=pPoly->GetNumVertices()-1; v>=0; v--)
	{
		pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, v), vVerts[pPoly->GetNumVertices()-1-v] );
		vVerts[v].z += fRaiseZ;
	}

	AddPoly(pPoly->GetNumVertices(), vVerts, col);

	if(bBothSides)
	{
		for(s32 v=0; v<pPoly->GetNumVertices(); v++)
		{
			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, v), vVerts[v] );
			vVerts[v].z += fRaiseZ;
		}

		AddPoly(pPoly->GetNumVertices(), vVerts, col);
	}
}

void CNavGenApp::DrawPolyPrecalcCentroids(CNavMesh * pNavMesh)
{
	Vector3 vCentroid;
	for(int p=0; p<pNavMesh->GetNumPolys(); p++)
	{
		TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);
		pNavMesh->GetPolyCentroidQuick(pPoly, vCentroid);

		AddLine(vCentroid, vCentroid + ZAXIS, D3DCOLOR_RGBA(0,255,0,255), D3DCOLOR_RGBA(0,255,0,255));
	}
}

void CNavGenApp::DrawPolyAddresses(CNavMesh * pNavMesh)
{
	Vector3 vUp(0.0f,0.0f,0.2f);
	const bool bTessellation = pNavMesh->GetIndexOfMesh()==NAVMESH_INDEX_TESSELLATION;
	static TNavMeshPoly * pTestAddress = NULL;

	char tmp[64];

	Vector3 vCentroid;
	for(int p=0; p<pNavMesh->GetNumPolys(); p++)
	{
		TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);

		if(bTessellation)
		{
			if(!CPathServer::GetTessInfo(p)->m_bInUse)
				continue;
			pNavMesh->GetPolyCentroid(p, vCentroid);
		}
		else
		{
			pNavMesh->GetPolyCentroidQuick(pPoly, vCentroid);
		}

		if((m_vViewPos-vCentroid).Mag2() < 25.0f*25.0f)
		{
			const float fD = - DotProduct(m_vViewForwards, m_vViewPos);
			if(DotProduct(vCentroid, m_vViewForwards) + fD > 1.0f)
			{
				sprintf(tmp, "%i) 0x%x", p, pPoly);
				DrawString(vCentroid+vUp, tmp, pPoly==pTestAddress ? D3DCOLOR_RGBA(255,255,255,255) : D3DCOLOR_RGBA(0,128,0,255));
			}
		}
	}
}

void CNavGenApp::DrawNavMesh( CNavMesh * pNavMesh, CNavMeshVertexBuffers & VB, bool bTessellationNavMesh )
{
	HRESULT retval;

	if(VB.m_pNavMeshTrianglesVB)
	{
		if(bTessellationNavMesh)
		{
			SetAlphaBlendEnable(true);

			SetColourStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);
			SetAlphaStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);

			SetSrcBlend(D3DBLEND_SRCALPHA);
			SetDestBlend(D3DBLEND_INVSRCALPHA);
		}
		else
		{
			SetAlphaBlendEnable(false);
			SetColourStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);
			SetAlphaStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);

			SetSrcBlend(D3DBLEND_SRCALPHA);
			SetDestBlend(D3DBLEND_INVSRCALPHA);
		}

		SetColourStage(1, D3DTOP_DISABLE, 0, 0);
		SetAlphaStage(1, D3DTOP_DISABLE, 0, 0);

		m_pD3DDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);

		retval = m_pD3DDevice->SetStreamSource(0, VB.m_pNavMeshTrianglesVB, 0, sizeof(LitVertex));
		retval = m_pD3DDevice->SetFVF(LitVertexFVF);
		retval = m_pD3DDevice->DrawPrimitive(
			D3DPT_TRIANGLELIST,
			0,
			VB.m_iNavMeshNumTriangles
		);

		SetAlphaBlendEnable(true);
		SetColourStage(0, D3DTOP_SELECTARG1, D3DTA_TFACTOR, 0);
		SetAlphaStage(0, D3DTOP_SELECTARG1, D3DTA_TFACTOR, 0);

		m_pD3DDevice->SetRenderState(D3DRS_TEXTUREFACTOR, D3DCOLOR_RGBA(255, 255, 255, 24));

		m_pD3DDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);

		retval = m_pD3DDevice->SetStreamSource(0, VB.m_pNavMeshTrianglesVB, 0, sizeof(LitVertex));
		retval = m_pD3DDevice->SetFVF(LitVertexFVF);
		retval = m_pD3DDevice->DrawPrimitive(
			D3DPT_TRIANGLELIST,
			0,
			VB.m_iNavMeshNumTriangles
		);
		SetAlphaBlendEnable(false);
	}

	if(VB.m_pNavMeshLinesVB)
	{
		SetColourStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);
		SetAlphaStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);
		SetColourStage(1, D3DTOP_DISABLE, 0, 0);
		SetAlphaStage(1, D3DTOP_DISABLE, 0, 0);

		SetSrcBlend(D3DBLEND_SRCALPHA);
		SetDestBlend(D3DBLEND_INVSRCALPHA);

		m_pD3DDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);

		retval = m_pD3DDevice->SetStreamSource(0, VB.m_pNavMeshLinesVB, 0, sizeof(LitVertex));
		retval = m_pD3DDevice->SetFVF(LitVertexFVF);
		retval = m_pD3DDevice->DrawPrimitive(
			D3DPT_LINELIST,
			0,
			VB.m_iNavMeshNumLines
		);
	}

	if(pNavMesh->GetQuadTree())
	{
		Vector3 vSize = pNavMesh->GetQuadTree()->m_Maxs - pNavMesh->GetQuadTree()->m_Mins;
		DrawNavMeshCoverPoints(pNavMesh, pNavMesh->GetQuadTree(), &pNavMesh->GetQuadTree()->m_Mins, &vSize);

		SetAlphaBlendEnable(true);
		DrawBoxFromMinMax(pNavMesh->GetQuadTree()->m_Mins, pNavMesh->GetQuadTree()->m_Maxs, D3DCOLOR_RGBA(128, 128, 128, 128));
		SetAlphaBlendEnable(false);
	}

	DrawNavMeshNonStandardAdjacencies(pNavMesh);

	if(m_bDrawSpecialLinks)
		DrawNavMeshSpecialLinks(pNavMesh);
}

CNavMeshCoverPoint *
CNavGenApp::GetNavMeshCoverPointByIndex(CNavMeshQuadTree * pTree, int iCoverPointIndex)
{
	if(pTree->m_pLeafData)
	{
		if(iCoverPointIndex >= pTree->m_pLeafData->m_iIndexOfFirstCoverPointInList &&
			iCoverPointIndex < pTree->m_pLeafData->m_iIndexOfFirstCoverPointInList+pTree->m_pLeafData->m_iNumCoverPoints)
		{
			int iIndex = iCoverPointIndex - pTree->m_pLeafData->m_iIndexOfFirstCoverPointInList;
			return &pTree->m_pLeafData->m_CoverPoints[iIndex];
		}
		return NULL;
	}

	for(int c=0; c<4; c++)
	{
		CNavMeshCoverPoint * pCP = GetNavMeshCoverPointByIndex(pTree->m_pChildren[c], iCoverPointIndex);
		if(pCP)
			return pCP;
	}

	return NULL;
}

void
CNavGenApp::SetFogging(void)
{
	float fFogStart, fFogEnd, fFogDensity;

	fFogStart = m_fFarPlaneDistance - (m_fFarPlaneDistance / 4.0f);
	if(m_bPixelFog)
	{
		//fFogStart =	fFogStart / m_fFarPlaneDistance;
		fFogEnd = m_fFarPlaneDistance;//1.0f;
		fFogDensity = 0.66f;

		m_pD3DDevice->SetRenderState(D3DRS_FOGTABLEMODE, D3DFOG_LINEAR);//D3DFOG_EXP2);
		m_pD3DDevice->SetRenderState(D3DRS_FOGDENSITY, *(DWORD*)(&fFogDensity));
	}
	else
	{
		m_pD3DDevice->SetRenderState(D3DRS_FOGVERTEXMODE, D3DFOG_EXP2);
		fFogEnd = m_fFarPlaneDistance;
	}

	m_pD3DDevice->SetRenderState(D3DRS_FOGCOLOR, m_FogCol);
	m_pD3DDevice->SetRenderState(D3DRS_FOGSTART, *((DWORD*)(&fFogStart)));
	m_pD3DDevice->SetRenderState(D3DRS_FOGEND, *((DWORD*)(&fFogEnd)));
}

void CNavGenApp::DrawResolutionAreas()
{
	static const float fMaxDist2 = 150.0f*150.0f;

	for(int r=0; r<m_NodeGenerator.m_ResolutionAreasWholeMap.size(); r++)
	{
		CNavResolutionArea * pArea = m_NodeGenerator.m_ResolutionAreasWholeMap[r];
		if(m_vViewPos.Dist2((pArea->m_vMin+pArea->m_vMax)*0.5f) < fMaxDist2)
		{
			DWORD iCol;
			if(m_vViewPos.IsGreaterOrEqualThan(pArea->m_vMin) && m_vViewPos.IsLessThanAll(pArea->m_vMax))
			{
				iCol = D3DCOLOR_RGBA(255,255,128,255);
			}
			else
			{
				iCol = D3DCOLOR_RGBA(255,128,128,255);
			}

			DrawBoxFromMinMax(pArea->m_vMin, pArea->m_vMax, iCol);
		}
	}
}

void CNavGenApp::DrawAdjacenciesForPoly(TNavMeshPoly * pPoly)
{
	char tmp[64];
	Vector3 vUp(0.0f,0.0f,0.2f);
	CNavMesh * pNavMesh = fwPathServer::GetNavMeshFromIndex(pPoly->GetNavMeshIndex(), kNavDomainRegular);

	if(pNavMesh)
	{
		for(u32 a=0; a<pPoly->GetNumVertices(); a++)
		{
			const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly(pPoly->GetFirstVertexIndex() + a);

			if(adjPoly.GetPolyIndex() != NAVMESH_POLY_INDEX_NONE)
			{
				Vector3 p1v1, p1v2, p1mid;
				pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, a), p1v1);
				pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, (a+1)%pPoly->GetNumVertices()), p1v2);
				p1mid = (p1v1+p1v2)*0.5f;

				int iAdjMesh = adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes());

				CNavMesh * pAdjNavMesh = fwPathServer::GetNavMeshFromIndex(iAdjMesh, kNavDomainRegular);
				if(pAdjNavMesh)
				{
					Vector3 p2center;
					pAdjNavMesh->GetPolyCentroid(adjPoly.GetPolyIndex(), p2center);

					AddLine(p1mid + vUp, p2center + vUp, D3DCOLOR_RGBA(50,50,255,255), D3DCOLOR_RGBA(50,50,255,255));

					sprintf(tmp, "%i", adjPoly.GetPolyIndex());
					DrawString(p1mid, tmp, D3DCOLOR_RGBA(50,50,255,255));
				}
			}
		}
	}
}

void CNavGenApp::DrawAuthoredAdjacencies()
{
	for(s32 a=0; a<m_AuthoredAdjacencyPositions.GetCount(); a++)
	{
		CNavGen::TAuthoredAdjacencyHintPos & hintPos = m_AuthoredAdjacencyPositions[a];

		DWORD col = (hintPos.m_iType == CNavGen::TAuthoredAdjacencyHintPos::TYPE_CLIMB) ? D3DCOLOR_RGBA(0,255,0,255) : D3DCOLOR_RGBA(255,0,0,255);

		AddLine( hintPos.m_vStartPosition, hintPos.m_vStartPosition + ZAXIS, col, col );
		AddLine( hintPos.m_vStartPosition + (ZAXIS*0.25f), (hintPos.m_vStartPosition + (ZAXIS*0.25f)) + (hintPos.m_vHeading * 0.25f), col, col);
	}
}

void CNavGenApp::ClearAllPolyCosts()
{
	for(s32 n=0; n<CPathServer::m_pNavMeshStores[kNavDomainRegular]->GetMaxMeshIndex(); n++)
	{
		CNavMesh * pNavMesh = CPathServer::m_pNavMeshStores[kNavDomainRegular]->GetMeshByIndex(n);
		
		if(pNavMesh)
		{
			for(u32 p=0; p<pNavMesh->GetNumPolys(); p++)
			{
				TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);
				pPoly->m_fPathCost = 0.0f;
				pPoly->m_fDistanceTravelled = 0.0f;
			}
		}
	}
}

void CNavGenApp::DrawPolyCosts(CNavMesh * pNavMesh)
{
	char tmp[64];
	Vector3 vUp(0.0f,0.0f,-0.5f);

	for(u32 p=0; p<pNavMesh->GetNumPolys(); p++)
	{
		TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);
		if(pPoly->m_AStarTimeStamp == CPathServer::m_PathServerThread.m_iAStarTimeStamp)
		{
			Vector3 vCenter;
			pNavMesh->GetPolyCentroid(pNavMesh->GetPolyIndex(pPoly), vCenter);

			sprintf(tmp, "%.2f", pPoly->m_fPathCost);
			DrawString(vCenter+vUp, tmp, 
				(pPoly->GetReplacedByTessellation() || pPoly->GetIsDegenerateConnectionPoly()) ?
					D3DCOLOR_RGBA(75,75,75,255) : D3DCOLOR_RGBA(100,100,255,255));
		}
	}
}
void CNavGenApp::DrawPolyDistanceTravelled(CNavMesh * pNavMesh)
{
	char tmp[64];
	Vector3 vUp(0.0f,0.0f,-0.75f);

	for(u32 p=0; p<pNavMesh->GetNumPolys(); p++)
	{
		TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);
		if(pPoly->m_AStarTimeStamp == CPathServer::m_PathServerThread.m_iAStarTimeStamp)
		{
			Vector3 vCenter;
			pNavMesh->GetPolyCentroid(pNavMesh->GetPolyIndex(pPoly), vCenter);

			sprintf(tmp, "%.2f", pPoly->m_fDistanceTravelled);
			DrawString(vCenter+vUp, tmp, 
				(pPoly->GetReplacedByTessellation() || pPoly->GetIsDegenerateConnectionPoly()) ?
				D3DCOLOR_RGBA(75,75,75,255) : D3DCOLOR_RGBA(100,255,100,255));
		}
	}
}

void CNavGenApp::DrawPointsInPolys(CNavMesh * pNavMesh)
{
	CPathRequest * pRequest = &CPathServer::m_PathRequests[0];

	Vector3 vPolyPts[NAVMESHPOLY_MAX_NUM_VERTICES];
	Vector3 vPointInPoly;
	for(u32 p=0; p<pNavMesh->GetNumPolys(); p++)
	{
		TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);
		for(u32 v=0; v<pPoly->GetNumVertices(); v++)
			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, v), vPolyPts[v] );

		GetPointInPolyFromPointEnum(pNavMesh, pPoly, vPolyPts, &vPointInPoly, pPoly->GetPointEnum(),
			CPathServer::GetPathServerThread()->ShouldUseMorePointsForPoly(*pPoly, pRequest) );

		if((m_vViewPos-vPointInPoly).Mag2() < 75.0f*75.0f)
		{
			AddLine(vPointInPoly, vPointInPoly+ZAXIS, D3DCOLOR_RGBA(50,150,255,255), D3DCOLOR_RGBA(50,150,255,255));
		}
	}
}

void CNavGenApp::DrawPolyParents(CNavMesh * pNavMesh)
{
	Vector3 vUp(0.0f,0.0f,0.2f);

	for(u32 p=0; p<pNavMesh->GetNumPolys(); p++)
	{
		TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);

		if(pPoly->m_AStarTimeStamp == CPathServer::m_PathServerThread.m_iAStarTimeStamp && pPoly->GetPathParentPoly())
		{
			Vector3 vCenter;
			pNavMesh->GetPolyCentroid(pNavMesh->GetPolyIndex(pPoly), vCenter);

			CNavMesh * pParentNavMesh = fwPathServer::GetNavMeshFromIndex(pPoly->GetPathParentPoly()->GetNavMeshIndex(), kNavDomainRegular);

			if(pParentNavMesh)
			{
				Vector3 vParentCenter;
				pParentNavMesh->GetPolyCentroid(pParentNavMesh->GetPolyIndex(pPoly->GetPathParentPoly()), vParentCenter);

				AddLine(vParentCenter + vUp, vCenter + vUp, D3DCOLOR_RGBA(0,0,0,255), D3DCOLOR_RGBA(255,255,255,255));
			}
		}
	}
}



// A silly way of drawing stuff
// Just go through the entire triangle list and draw those which are within 25m of the view position
// Only meant for testing purposes!
static const float DUMBASS_VIEW_DISTANCE		= 128.0f;

void CNavGenApp::DrawTrisDumbass(void)
{
	s32 t;
	Vector3 vCenter, vDiff;
	float fRecipOfThree = 1.0f / 3.0f;
	float fMaxDistSrq = DUMBASS_VIEW_DISTANCE * DUMBASS_VIEW_DISTANCE;
	float fDistSqr;

	DWORD col;
	DWORD i;
	DWORD b = 250;

	//s32 num = 64;
	s32 num = m_NodeGenerator.m_CollisionTriangles.size();

	for(t=0; t<num; t++)
	{
		CNavGenTri * pTri = &m_NodeGenerator.m_CollisionTriangles[t];

		i = (DWORD)pTri;
		if(i < 100)
			i += 128;

		if(pTri->m_colPolyData.GetIsRoad())
			col = D3DCOLOR_RGBA(b, i, i, 255);
		else if(pTri->m_colPolyData.GetIsRiverBound())
			col = D3DCOLOR_RGBA(0, 0, 128, 255);
		else
			col = D3DCOLOR_RGBA(i, i, b, 255);

		vCenter = pTri->m_vPts[0] + pTri->m_vPts[1] + pTri->m_vPts[2];
		vCenter *= fRecipOfThree;

		vDiff = m_vViewPos - vCenter;
		fDistSqr = vDiff.Mag2();

		if(fDistSqr > fMaxDistSrq)
			continue;

		int pl;
		for(pl=0; pl<6; pl++)
		{
			int iNumOut=0;
			for(int v=0; v<3; v++)
			{
				float dot = DotProduct(m_vFrustumPlaneNormals[pl], pTri->m_vPts[v]) + m_fFrustumPlaneDists[pl];
				if(dot < 0)
					iNumOut++;
			}
			if(iNumOut==3)
				break;
		}

		if(pl != 6)
			continue;

		AddTri(pTri->m_vPts[0], pTri->m_vPts[1], pTri->m_vPts[2], col, col, col);
	}

}

void
CNavGenApp::ExtractFrustumPlanes(void)
{
    D3DXMATRIXA16 mat;
	D3DXMatrixMultiply(&mat, &m_ViewMatrix, &m_ProjectionMatrix);
    D3DXMatrixInverse(&mat, NULL, &mat);

	D3DXVECTOR3 vecFrustum[8];
	D3DXPLANE planeFrustum[6];

    vecFrustum[0] = D3DXVECTOR3(-1.0f, -1.0f,  0.0f); // xyz
    vecFrustum[1] = D3DXVECTOR3( 1.0f, -1.0f,  0.0f); // Xyz
    vecFrustum[2] = D3DXVECTOR3(-1.0f,  1.0f,  0.0f); // xYz
    vecFrustum[3] = D3DXVECTOR3( 1.0f,  1.0f,  0.0f); // XYz
    vecFrustum[4] = D3DXVECTOR3(-1.0f, -1.0f,  1.0f); // xyZ
    vecFrustum[5] = D3DXVECTOR3( 1.0f, -1.0f,  1.0f); // XyZ
    vecFrustum[6] = D3DXVECTOR3(-1.0f,  1.0f,  1.0f); // xYZ
    vecFrustum[7] = D3DXVECTOR3( 1.0f,  1.0f,  1.0f); // XYZ

    for(int i = 0; i < 8; i++)
	{
        D3DXVec3TransformCoord(&vecFrustum[i], &vecFrustum[i], &mat);
	}


    D3DXPlaneFromPoints(
		&planeFrustum[0],
		&vecFrustum[0], 
        &vecFrustum[1],
		&vecFrustum[2]
	); // Near

    D3DXPlaneFromPoints(
		&planeFrustum[1],
		&vecFrustum[6],
        &vecFrustum[7],
		&vecFrustum[5]
	); // Far

    D3DXPlaneFromPoints(
		&planeFrustum[2],
		&vecFrustum[2],
        &vecFrustum[6],
		&vecFrustum[4]
	); // Left

    D3DXPlaneFromPoints(
		&planeFrustum[3],
		&vecFrustum[7],
        &vecFrustum[3],
		&vecFrustum[5]
	); // Right

    D3DXPlaneFromPoints(
		&planeFrustum[4],
		&vecFrustum[2], 
        &vecFrustum[3],
		&vecFrustum[6]
	); // Top

    D3DXPlaneFromPoints(
		&planeFrustum[5],
		&vecFrustum[1],
        &vecFrustum[0],
		&vecFrustum[4]
	); // Bottom


	for(int p=0; p<6; p++)
	{
		m_vFrustumPlaneNormals[p].x = planeFrustum[p].a;
		m_vFrustumPlaneNormals[p].y = planeFrustum[p].b;
		m_vFrustumPlaneNormals[p].z = planeFrustum[p].c;
		m_fFrustumPlaneDists[p] = planeFrustum[p].d;
	}
}

bool
CNavGenApp::BoxIntersectsFrustum(const Vector3 & vBoxMin, const Vector3 & vBoxMax)
{
	// For now lets just take a bounding sphere of the AABB
	Vector3 vMid = (vBoxMin + vBoxMax) * 0.5f;
	Vector3 vDiff = vMid - vBoxMin;
	float fBound = vDiff.Mag();
	float fDot;

	// Check each plane
	int pl;
	for(pl=0; pl<6; pl++)
	{
		fDot = DotProduct(m_vFrustumPlaneNormals[pl], vMid) + m_fFrustumPlaneDists[pl];

		// If is completely outside of any plane, then return false
		if(fDot < -fBound)
			return false;
	}

	return true;
}

// Converts a point in screen-space, to a point projected onto the far view plane.
// This is for use when picking things in the 3d environment.  It needs m_ViewMatrix
// and m_D3DViewPort members to be set properly.
void
CNavGenApp::ConvertScreenCoordsToPointsOnNearAndFarPlanes(int screenX, int screenY, Vector3 & outPosNear, Vector3 & outPosFar)
{
	float x = (float)screenX;
	float y = (float)screenY;

	float fWidth  = (float)m_D3DViewPort.Width;
	float fHeight = (float)m_D3DViewPort.Height;

	float fHalfWidth = fWidth / 2.0f;
	float fHalfHeight = fHeight / 2.0f;

	float fAspect = fWidth / fHeight;

	float dx = (x / fHalfWidth - 1.0f) / fAspect;
	float dy = 1.0f - y / fHalfHeight;

	dx = tanf(m_FOV * 0.5f) * (x / fHalfWidth - 1.0f);// / fAspect;
	dy = tanf(m_FOV * 0.5f) * (1.0f - y / fHalfHeight);

	Vector3 vNear(dx*m_fNearPlaneDistance, dy*m_fNearPlaneDistance, m_fNearPlaneDistance);
	Vector3 vFar(dx*m_fFarPlaneDistance, dy*m_fFarPlaneDistance, m_fFarPlaneDistance);


	Matrix34 viewMat;
	D3DXMatrixToCMatrix(viewMat, m_ViewMatrix);

	//Matrix34 invViewMat = Invert(viewMat);
	Matrix34 invViewMat;
	invViewMat.Inverse(viewMat);
	

//	outPosNear = invViewMat * vNear;
//	outPosFar = invViewMat * vFar;

	invViewMat.Transform(vNear, outPosNear);
	invViewMat.Transform(vFar, outPosFar);


	/*
	// get viewport
	float fWidth  = m_D3DViewPort.Width;
	float fHeight = m_D3DViewPort.Height;
	float fAspect = fHeight / fWidth;	//fWidth / fHeight;	//fHeight / fWidth;	

    float fScrSize, mx, my;
    
	fScrSize = CMaths::Tan(m_FOV / 2.0f) * m_fNearPlaneDistance;

    mx = fScrSize * (((float)screenX) / (fWidth/2.0f) - 1.0f) / fAspect;
    my = fScrSize * (1.0f - ((float)screenY) / (fHeight/2.0f));

    // point on near plane
	outPosNear = Vector3(mx, my, m_fNearPlaneDistance);
	outPosNear = invViewMat * outPosNear;

	// scale between near & far plane
	float fScale = m_fFarPlaneDistance / m_fNearPlaneDistance;

	// point on far plane
	outPosFar = Vector3(mx * fScale, my * fScale, m_fFarPlaneDistance);
	outPosFar = invViewMat * outPosFar;
	*/
}

void CNavGenApp::DrawWater()
{
	const DWORD watercol = D3DCOLOR_RGBA(80,80,255,255);

	if(m_NodeGenerator.m_bHasWater)
	{
		if(!m_NodeGenerator.m_bHasVariableWaterLevel)
		{
			Vector3 vWaterVerts[4] =
			{
				Vector3( m_NodeGenerator.m_vSectorMins.x, m_NodeGenerator.m_vSectorMins.y, m_NodeGenerator.m_fWaterLevel),
				Vector3( m_NodeGenerator.m_vSectorMaxs.x, m_NodeGenerator.m_vSectorMins.y, m_NodeGenerator.m_fWaterLevel),
				Vector3( m_NodeGenerator.m_vSectorMaxs.x, m_NodeGenerator.m_vSectorMaxs.y, m_NodeGenerator.m_fWaterLevel),
				Vector3( m_NodeGenerator.m_vSectorMins.x, m_NodeGenerator.m_vSectorMaxs.y, m_NodeGenerator.m_fWaterLevel)
			};
		
			AddPoly(4, vWaterVerts, watercol);
		}
		else
		{
			float x,y;
			float fHeight;
			for(y=m_NodeGenerator.m_vSectorMins.y; y<m_NodeGenerator.m_vSectorMaxs.y; y+=m_NodeGenerator.m_fWaterSamplingStepSize)
			{
				for(x=m_NodeGenerator.m_vSectorMins.x; x<m_NodeGenerator.m_vSectorMaxs.x; x+=m_NodeGenerator.m_fWaterSamplingStepSize)
				{
					if(m_NodeGenerator.GetWaterLevel(x, y, fHeight))
					{
						AddLine( Vector3(x,y,fHeight), Vector3(x,y,fHeight+0.1f), watercol, watercol);
					}
				}
			}
		}
	}
}

void CNavGenApp::DrawOctree(void)
{
	CNavGenOctreeNodeBase * pRootNode = m_NodeGenerator.m_Octree.RootNode();

	Vector3 vRootNodeMin, vRootNodeMax;
	m_NodeGenerator.m_Octree.GetOctreeExtents(vRootNodeMin, vRootNodeMax);

	if(pRootNode)
	{
		DrawOctreeR(pRootNode, vRootNodeMin, vRootNodeMax);
	}
}

Vector3 vCurrLinePos;
u32 vCurrLineCol;

void
CNavGenApp::StartPolyLine(const Vector3 & vStartPos, u32 col)
{
	vCurrLinePos = vStartPos;
	vCurrLineCol = col;
}

void
CNavGenApp::DrawPolyLineTo(const Vector3 & vPos, u32 col)
{
	AddLine(vCurrLinePos, vPos, vCurrLineCol, col);
	vCurrLinePos = vPos;
	vCurrLineCol = col;
}

void CNavGenApp::DrawBoxFromMinMax(const Vector3 & vMin, const Vector3 & vMax, u32 col)
{
	Vector3 boxVecs[8] = 
	{
		Vector3(vMin.x, vMin.y, vMin.z),
		Vector3(vMax.x, vMin.y, vMin.z),
		Vector3(vMax.x, vMax.y, vMin.z),
		Vector3(vMin.x, vMax.y, vMin.z),

		Vector3(vMin.x, vMin.y, vMax.z),
		Vector3(vMax.x, vMin.y, vMax.z),
		Vector3(vMax.x, vMax.y, vMax.z),
		Vector3(vMin.x, vMax.y, vMax.z)
	};

	StartPolyLine(	boxVecs[0], col);
	DrawPolyLineTo(	boxVecs[1], col);
	DrawPolyLineTo(	boxVecs[2], col);
	DrawPolyLineTo(	boxVecs[3], col);
	DrawPolyLineTo(	boxVecs[0], col);

	StartPolyLine(	boxVecs[4], col);
	DrawPolyLineTo(	boxVecs[5], col);
	DrawPolyLineTo(	boxVecs[6], col);
	DrawPolyLineTo(	boxVecs[7], col);
	DrawPolyLineTo(	boxVecs[4], col);

	AddLine( boxVecs[0], boxVecs[4], col, col );
	AddLine( boxVecs[1], boxVecs[5], col, col );
	AddLine( boxVecs[2], boxVecs[6], col, col );
	AddLine( boxVecs[3], boxVecs[7], col, col );
}

void CNavGenApp::DrawOrientedBoxFromMinMax(const Vector3 & vMin, const Vector3 & vMax, const Matrix34 & mTransform, u32 col)
{
	Vector3 boxVecs[8] = 
	{
		Vector3(vMin.x, vMin.y, vMin.z),
		Vector3(vMax.x, vMin.y, vMin.z),
		Vector3(vMax.x, vMax.y, vMin.z),
		Vector3(vMin.x, vMax.y, vMin.z),

		Vector3(vMin.x, vMin.y, vMax.z),
		Vector3(vMax.x, vMin.y, vMax.z),
		Vector3(vMax.x, vMax.y, vMax.z),
		Vector3(vMin.x, vMax.y, vMax.z)
	};

	mTransform.Transform(boxVecs[0]);
	mTransform.Transform(boxVecs[1]);
	mTransform.Transform(boxVecs[2]);
	mTransform.Transform(boxVecs[3]);
	mTransform.Transform(boxVecs[4]);
	mTransform.Transform(boxVecs[5]);
	mTransform.Transform(boxVecs[6]);
	mTransform.Transform(boxVecs[7]);

	StartPolyLine(	boxVecs[0], col);
	DrawPolyLineTo(	boxVecs[1], col);
	DrawPolyLineTo(	boxVecs[2], col);
	DrawPolyLineTo(	boxVecs[3], col);
	DrawPolyLineTo(	boxVecs[0], col);

	StartPolyLine(	boxVecs[4], col);
	DrawPolyLineTo(	boxVecs[5], col);
	DrawPolyLineTo(	boxVecs[6], col);
	DrawPolyLineTo(	boxVecs[7], col);
	DrawPolyLineTo(	boxVecs[4], col);

	AddLine( boxVecs[0], boxVecs[4], col, col );
	AddLine( boxVecs[1], boxVecs[5], col, col );
	AddLine( boxVecs[2], boxVecs[6], col, col );
	AddLine( boxVecs[3], boxVecs[7], col, col );
}

void CNavGenApp::DrawAxis(const Vector3 & vOrigin, const Vector3 & vX, const Vector3 & vY, const Vector3 & vZ, const float fSize)
{
	AddLine( vOrigin, vOrigin + (vX * fSize), D3DCOLOR_RGBA(255,0,0,255), D3DCOLOR_RGBA(255,0,0,255) );
	AddLine( vOrigin, vOrigin + (vY * fSize), D3DCOLOR_RGBA(0,255,0,255), D3DCOLOR_RGBA(0,255,0,255) );
	AddLine( vOrigin, vOrigin + (vZ * fSize), D3DCOLOR_RGBA(0,0,255,255), D3DCOLOR_RGBA(0,0,255,255) );
}

void CNavGenApp::DrawSphere(const Vector3 & vPos, const float fRadius, u32 col)
{
	Vector3 vPoints[8];
	const float fAngleInc = (360.0f / 8.0f) * DtoR;
	const Vector2 vOffset(0.0f, fRadius);

	float fRot = 0.0f;
	for(int s=0; s<8; s++)
	{
		Vector2 vRotOffset = vOffset;
		vRotOffset.Rotate(fRot);
		vPoints[s] = Vector3(vRotOffset.x, vRotOffset.y, 0.0f);
		fRot += fAngleInc;
	}

	for(int r=0; r<8; r++)
	{
		AddLine(vPos + vPoints[r], vPos + vPoints[(r+1)%8], col, col);
	}
}

void
CNavGenApp::DrawNodes(void)
{
	if(m_bDrawPathNodes)
	{
		u32 n;
		DWORD baseCol = D3DCOLOR_RGBA(0, 255, 0, 255);
		DWORD topCol = D3DCOLOR_RGBA(255, 0, 0, 255);

		DWORD debugBaseCol = D3DCOLOR_RGBA(255, 255, 0, 255);
		DWORD debugTopCol = D3DCOLOR_RGBA(255, 255, 255, 255);

		//DWORD baseCol = ((u32)pLeaf);
		//DWORD topCol = baseCol;
		Vector3 vTopPos;

		for(n=0; n<m_NodeGenerator.m_NavSurfaceNodes.size(); n++)
		{
			CNavGenNode * pPathNode = m_NodeGenerator.m_NavSurfaceNodes[n];
			if(pPathNode->m_iFlags & NAVGENNODE_REMOVED)
			{
				continue;
			}
			vTopPos.x = pPathNode->m_vBasePos.x;
			vTopPos.y = pPathNode->m_vBasePos.y;
			vTopPos.z = pPathNode->m_vBasePos.z + 1.0f;

			if(pPathNode->m_iFlags & NAVGENNODE_ON_EDGE_OF_MESH)
			{
				AddLine(pPathNode->m_vBasePos, vTopPos, debugBaseCol, debugTopCol);
			}
			else
			{
				AddLine(pPathNode->m_vBasePos, vTopPos, baseCol, topCol);
			}
		}
	}
}

void CNavGenApp::DrawExtrasInfo()
{
	u32 i;

	for(i=0; i<m_ExtraInfo.m_LaddersList.size(); i++)
	{
		fwNavLadderInfo & ladderInfo = m_ExtraInfo.m_LaddersList[i];

		AddLine(ladderInfo.GetBase(), ladderInfo.GetTop(), D3DCOLOR_RGBA(128,128,255,255), D3DCOLOR_RGBA(128,128,255,255));
	}

	for(i=0; i<m_ExtraInfo.m_DynamicEntityBounds.size(); i++)
	{
		fwDynamicEntityInfo * pEntityInfo = m_ExtraInfo.m_DynamicEntityBounds[i];

		const float fDistSqr = (pEntityInfo->m_Matrix.d - m_vViewPos).Mag2();
		if(fDistSqr < 100.0*100.0)
		{
			Vector3 vObjMid = GetCenter(pEntityInfo->m_OOBB[0], pEntityInfo->m_OOBB[1], pEntityInfo->m_Matrix);
			//Vector3 vObjMid = (pEntityInfo->m_vCorners[0] + pEntityInfo->m_vCorners[1] + pEntityInfo->m_vCorners[2] + pEntityInfo->m_vCorners[3]) * 0.25f;
			//vObjMid.z = (pEntityInfo->m_fTopZ + pEntityInfo->m_fBottomZ) * 0.5f;

			DrawAxis(vObjMid, pEntityInfo->m_Matrix.a, pEntityInfo->m_Matrix.b, pEntityInfo->m_Matrix.c, 1.0f);

			DrawOrientedBoxFromMinMax(pEntityInfo->m_OOBB[0], pEntityInfo->m_OOBB[1], pEntityInfo->m_Matrix, D3DCOLOR_RGBA(128,255,128,255));
		}
		if(fDistSqr < 20.0*20.0)
		{
			static char tmp[8];
			sprintf(tmp, "(%i)", i);
			DrawString(pEntityInfo->m_Matrix.d, tmp, D3DCOLOR_RGBA(128,255,128,255));
		}
	}
}

void
CNavGenApp::DrawNavSurface(void)
{
#ifdef VISUALISE_NAVIGATION_SURFACE

	if(m_bDrawNavigationSurfaces)
	{
		if(m_bNavSurfaceHasChanged)
		{
			CreateVertexAndIndexBuffersForNavSurface();
			m_bNavSurfaceHasChanged = false;
		}
	}

#endif
}

DWORD octreeLineCol = D3DCOLOR_COLORVALUE(1.0f, 0, 0, 1.0f);

void
CNavGenApp::DrawOctreeR(CNavGenOctreeNodeBase * pNodeBase, const Vector3 & vNodeMin, const Vector3 & vNodeMax)
{
	if(pNodeBase->m_Flags & NAVGEN_OCTREE_FLAG_ISLEAF)
	{
		m_iNumLeavesVisitedForRender++;

		CNavGenOctreeLeaf * pLeaf = (CNavGenOctreeLeaf*)pNodeBase;

		if(!BoxIntersectsFrustum(pLeaf->m_NodeMin, pLeaf->m_NodeMax))
		{
			return;
		}

		// The octree node's bounds
		if(m_bDrawOctreeBounds)
		{
			// This draws the leaf-node's bounds - which is the min/max of the contained geometry only
			DrawBoxFromMinMax(pLeaf->m_NodeMin, pLeaf->m_NodeMax, octreeLineCol);

			// This draw the node's min/max as passed down the tree, and it the geometrical min/max based
			// on the subdivision of the octree bounds, all the way down from the root node..
			//DrawBoxFromMinMax(vNodeMin, vNodeMax, octreeLineCol);
		}

		// The collision polys
		if(m_bDrawCollisionPolys && !m_bDrawSplitPolys)
		{
			u32 t;
			DWORD col;
			u32 timeStamp = m_NodeGenerator.m_Octree.GetTimeStamp();

			for(t=0; t<pLeaf->m_Triangles.size(); t++)
			{
				CNavGenTri * pTri = pLeaf->m_Triangles[t];
				if(pTri->m_iTimeStamp == timeStamp)
					continue;

				pTri->m_iTimeStamp = timeStamp;

				col = (((DWORD)pTri) & 0xFF);
				if(col < 150)
					col = min(255, col + 200);


				// Walkable or not..
				if(pTri->m_iFlags & NAVGENTRI_WALKABLE)
				{
					col = D3DCOLOR_RGBA(col, col, 250, 255);
				}
				else
				{
					col = D3DCOLOR_RGBA(col, col>>1, 125, 255);
				}

				if(pTri->m_colPolyData.GetIsNotClimbableCollision())
				{
					col = D3DCOLOR_RGBA(100, 100, 0, 128);
				}
				else if(pTri->m_colPolyData.GetIsClimbableObject())
				{
					col = D3DCOLOR_RGBA(0, 128, 0, 128);
				}
				else if(pTri->m_colPolyData.GetIsExteriorPortal())
				{
					col = D3DCOLOR_RGBA(255, 255, 0, 255);
				}

				if(pTri->m_colPolyData.GetIsNoNetworkSpawn())
				{
					//col = D3DCOLOR_RGBA(185, col, 100, 255);
				}
				else if(pTri->m_iFlags & NAVGENTRI_STAIRS)
				{
					col = D3DCOLOR_RGBA(250, 250, col, 255);
				}
				else if(pTri->m_iFlags & NAVGENTRI_PAVEMENT)
				{
					col = D3DCOLOR_RGBA(127 + (col%127), col % 15, 127 + (col%127), 255);
				}
				else if(pTri->m_colPolyData.GetIsFixedObject())
				{
					col = D3DCOLOR_RGBA(128, col, col, 255);

					AddLine(pTri->m_vPts[0], pTri->m_vPts[1], 0xffffffff, 0xffffffff);
					AddLine(pTri->m_vPts[1], pTri->m_vPts[2], 0xffffffff, 0xffffffff);
					AddLine(pTri->m_vPts[2], pTri->m_vPts[0], 0xffffffff, 0xffffffff);
				}
				else if(pTri->m_colPolyData.GetIsInterior())
				{
					col = (((DWORD)pTri) & 0xFF);
					col = D3DCOLOR_RGBA(200, 200, col, 255);
				}
				else if(pTri->m_colPolyData.GetPedDensity() > 0.0f)
				{
					float s = pTri->m_colPolyData.GetPedDensity() / 7.0f;
					int val = (int)(255.0f * s);
					col = D3DCOLOR_RGBA(val, val, 0, 255);
				}
				else if(pTri->m_colPolyData.GetIsRiverBound())
				{
					col = D3DCOLOR_RGBA(0, 0, 255, 255);
				}
				else if(pTri->m_colPolyData.GetIsTrainTracks())
				{
					col = D3DCOLOR_RGBA(40, 70, 50, 255);
				}
				else if(pTri->m_colPolyData.GetIsRoad())
				{
					col = D3DCOLOR_RGBA(col, 0, 0, 255);
				}

				// Selected triangle ?
				if(pTri->m_iIndexInCollisionPolysList == (u32)m_iSelectedTri)
				{
					col = D3DCOLOR_RGBA(255, 0, 255, 255);
				}

				AddTri(pTri->m_vPts[0], pTri->m_vPts[1], pTri->m_vPts[2], col, col, col);
			}
		}

#ifdef DEBUG_OCTREE_CONSTRUCTION
		// The split octree polys (if we stored them)
		if(m_bDrawSplitPolys && pLeaf->m_pFragments)
		{
			int t;
			DWORD col;

			for(t=0; t<pLeaf->m_pFragments->size(); t++)
			{
				CSplitPoly * pPoly = (*pLeaf->m_pFragments)[t];
				col = (DWORD)pPoly;
				if(col < 150)
					col = min(255, col + 150);
				col = D3DCOLOR_RGBA(250, col, 250, 255);

				vector<Vector3>::pointer ptr = &pPoly->m_Pts[0];

				AddPoly(pPoly->m_Pts.size(), ptr, col);
				//AddTri(pPoly->m_Pts[0], pPoly->m_Pts[1], pPoly->m_Pts[2], col, col, col);
			}
		}
#endif
		return;
	}

	if(!BoxIntersectsFrustum(vNodeMin, vNodeMax))
	{
		return;
	}

	m_iNumNodesVisitedForRender++;

	CNavGenOctreeNode * pNode = (CNavGenOctreeNode*)pNodeBase;
	for(s32 o=0; o<8; o++)
	{
		// Get the min/max's for this octant based upon the parent node's extents
		Vector3 vOctantMin, vOctantMax;
		m_NodeGenerator.m_Octree.GetOctantExtents(o, vNodeMin, vNodeMax, vOctantMin, vOctantMax);

		DrawOctreeR(pNode->m_ChildNodes[o], vOctantMin, vOctantMax);
	}
}



#ifdef VISUALISE_NAVIGATION_SURFACE

void CNavGenApp::CreateVertexAndIndexBuffersForNavSurface(void)
{
	u32 iNumTris = 0;
	float fHeightAboveNodeBase = m_NodeGenerator.m_Params.m_HeightAboveNodeBaseAtWhichToTriangulate;

	u32 t,v;
	for(t=0; t<m_NodeGenerator.m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NodeGenerator.m_NavSurfaceTriangles[t];
		if(pTri->IsRemoved())
		{
			continue;
		}
		iNumTris++;
	}

	if(m_pNavSurfaceVertexBuffer)
	{
		m_pNavSurfaceVertexBuffer->Release();
		m_pNavSurfaceVertexBuffer = NULL;
		m_iNumTrianglesInNavSurfaceVB = 0;
	}

	if(!iNumTris)
	{
		return;
	}


	// NB : we can optimise this greatly by using indexed vertices here, but for now
	// am just going to use triangle-lists for simplicity.
	HRESULT retval;

	// Create the vertex buffer
	retval = m_pD3DDevice->CreateVertexBuffer(
		sizeof(LitVertex) * (iNumTris * 3),
		D3DUSAGE_WRITEONLY,
		LitVertexFVF,
		D3DPOOL_DEFAULT,
		&m_pNavSurfaceVertexBuffer,
		NULL
	);
	if(retval != D3D_OK)
	{
		return;
	}

	LitVertex * pVertexData;

	retval = m_pNavSurfaceVertexBuffer->Lock(
		0,
		sizeof(LitVertex) * iNumTris,
		(void**)&pVertexData,
		0
	);
	if(retval != D3D_OK)
	{
		return;
	}

	int iVertIndex = 0;
	m_iNumTrianglesInNavSurfaceVB = 0;
	DWORD triCol;

	for(t=0; t<m_NodeGenerator.m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NodeGenerator.m_NavSurfaceTriangles[t];
		if(pTri->IsRemoved())
		{
			continue;
		}

		int iNumAdjacent = 0;
		if(pTri->m_AdjacentTris[0])
			iNumAdjacent++;
		if(pTri->m_AdjacentTris[1])
			iNumAdjacent++;
		if(pTri->m_AdjacentTris[2])
			iNumAdjacent++;

		if(m_bShadeSurfaceTypes)
		{
			if(pTri->m_colPolyData.GetIsNoNetworkSpawn())
			{
				triCol = D3DCOLOR_RGBA(185, 0, 100, 128);
			}
			else if(pTri->m_colPolyData.GetIsPavement())
			{
				triCol = D3DCOLOR_RGBA(255, 0, 0, 128);
			}
			else if(pTri->m_bIsTooSteep)
			{
				triCol = D3DCOLOR_RGBA(64, 64, 128, 128);
			}
			else
			{
				//triCol = D3DCOLOR_RGBA(pTri->m_iSurfaceType, pTri->m_iSurfaceType, pTri->m_iSurfaceType, 128);
				triCol = D3DCOLOR_RGBA(255,255,255, 128);
			}
		}
		else
		{
			if(pTri->m_iFlags & NAVSURFTRI_ISMARKED_RED)
			{
				triCol = D3DCOLOR_RGBA(255, 0, 0, 128);
			}
			else if(pTri->m_iFlags & NAVSURFTRI_ISMARKED_YELLOW)
			{
				triCol = D3DCOLOR_RGBA(255, 255, 0, 128);
			}
			else if(pTri->m_iFlags & NAVSURFTRI_ISMARKED_GREEN)
			{
				triCol = D3DCOLOR_RGBA(0, 255, 0, 128);
			}
			else
			{
				triCol = D3DCOLOR_RGBA(255, 255, 255, 128);
			}
		}

		for(v=0; v<3; v++)
		{
			Assert(!pTri->m_Nodes[v]->IsRemoved());

			pVertexData[iVertIndex].x = pTri->m_Nodes[v]->m_vBasePos.x;
			pVertexData[iVertIndex].y = pTri->m_Nodes[v]->m_vBasePos.y;
			pVertexData[iVertIndex].z = pTri->m_Nodes[v]->m_vBasePos.z + fHeightAboveNodeBase;
			pVertexData[iVertIndex].diffuse = triCol;
			iVertIndex++;
		}
	}

	m_iNumTrianglesInNavSurfaceVB = iNumTris;

	retval = m_pNavSurfaceVertexBuffer->Unlock();
}

LitVertex * g_pVoxelVertexData = NULL;
const s32 g_iMaxVoxelTris = 131072; //32768;

bool AddVoxelTriangles(CVoxelMap::CElement * pElement, Vector3::Vector3Param vVoxelMin, Vector3::Vector3Param vVoxelMax, void * data)
{
	if(g_iMaxVoxelTris - g_App->m_iNumTrianglesInVoxelMapVB < 12)
		return false;

	const DWORD col = 0xFFFFFFFF;

	const Vector3 a( vVoxelMin.x, vVoxelMin.y, vVoxelMin.z );
	const Vector3 b( vVoxelMax.x, vVoxelMin.y, vVoxelMin.z );
	const Vector3 c( vVoxelMax.x, vVoxelMax.y, vVoxelMin.z );
	const Vector3 d( vVoxelMin.x, vVoxelMax.y, vVoxelMin.z );
	const Vector3 e( vVoxelMin.x, vVoxelMin.y, vVoxelMax.z );
	const Vector3 f( vVoxelMax.x, vVoxelMin.y, vVoxelMax.z );
	const Vector3 g( vVoxelMax.x, vVoxelMax.y, vVoxelMax.z );
	const Vector3 h( vVoxelMin.x, vVoxelMax.y, vVoxelMax.z );

	const Vector3 verts[12*3] =
	{
		b, c, d,
		a, b, d,
		g, f, e,
		h, g, e,
		g, c, b,
		f, g, b,
		h, d, c,
		g, h, c,
		h, e, a,
		d, h, a,
		f, b, a,
		e, f, a
	};

	for(s32 t=0; t<12; t++)
	{
		const Vector3 v0 = verts[t*3];
		const Vector3 v1 = verts[(t*3)+1];
		const Vector3 v2 = verts[(t*3)+2];

		g_pVoxelVertexData[(g_App->m_iNumTrianglesInVoxelMapVB*3)].x = v0.x;
		g_pVoxelVertexData[(g_App->m_iNumTrianglesInVoxelMapVB*3)].y = v0.y;
		g_pVoxelVertexData[(g_App->m_iNumTrianglesInVoxelMapVB*3)].z = v0.z;
		g_pVoxelVertexData[(g_App->m_iNumTrianglesInVoxelMapVB*3)].diffuse = col;

		g_pVoxelVertexData[(g_App->m_iNumTrianglesInVoxelMapVB*3)+1].x = v1.x;
		g_pVoxelVertexData[(g_App->m_iNumTrianglesInVoxelMapVB*3)+1].y = v1.y;
		g_pVoxelVertexData[(g_App->m_iNumTrianglesInVoxelMapVB*3)+1].z = v1.z;
		g_pVoxelVertexData[(g_App->m_iNumTrianglesInVoxelMapVB*3)+1].diffuse = col;

		g_pVoxelVertexData[(g_App->m_iNumTrianglesInVoxelMapVB*3)+2].x = v2.x;
		g_pVoxelVertexData[(g_App->m_iNumTrianglesInVoxelMapVB*3)+2].y = v2.y;
		g_pVoxelVertexData[(g_App->m_iNumTrianglesInVoxelMapVB*3)+2].z = v2.z;
		g_pVoxelVertexData[(g_App->m_iNumTrianglesInVoxelMapVB*3)+2].diffuse = col;

		g_App->m_iNumTrianglesInVoxelMapVB++;
	}

	return true;
}

void CNavGenApp::CreateVertexBufferForVoxelMap()
{
	HRESULT retval = m_pD3DDevice->CreateVertexBuffer(
		sizeof(LitVertex) * (g_iMaxVoxelTris * 3),
		D3DUSAGE_WRITEONLY,
		LitVertexFVF,
		D3DPOOL_DEFAULT,
		&m_pVoxelMapVertexBuffer,
		NULL);
	
	if(retval != D3D_OK)
		return;

	m_iNumTrianglesInVoxelMapVB = 0;


	retval = m_pVoxelMapVertexBuffer->Lock( 0, sizeof(LitVertex) * g_iMaxVoxelTris, (void**)&g_pVoxelVertexData, 0 );
	if(retval != D3D_OK)
		return;

	// Create triangles for all the solid voxels
	m_NodeGenerator.m_VoxelMap.ForAllSolidVoxels(AddVoxelTriangles, NULL);

	retval = m_pVoxelMapVertexBuffer->Unlock();
}

#endif


void
CNavGenApp::DrawNavSurfaceTri(CNavSurfaceTri * pTri, DWORD col, bool bDrawAdjacent, bool bMarkVerts, const float fExtra)
{
	float fHeightAboveNodeBase = m_NodeGenerator.m_Params.m_HeightAboveNodeBaseAtWhichToTriangulate;

	if(!pTri)
		return;

	LitVertex triVerts[3];
	SetColourStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);
	SetAlphaStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);
	SetColourStage(1, D3DTOP_DISABLE, 0, 0);
	SetAlphaStage(1, D3DTOP_DISABLE, 0, 0);
	SetAlphaBlendEnable(true);
	SetSrcBlend(D3DBLEND_SRCALPHA);
	SetDestBlend(D3DBLEND_INVSRCALPHA);
	m_pD3DDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	m_pD3DDevice->SetFVF(LitVertexFVF);

	int v;
	for(v=0; v<3; v++)
	{
		triVerts[v].x = pTri->m_Nodes[v]->m_vBasePos.x;
		triVerts[v].y = pTri->m_Nodes[v]->m_vBasePos.y;
		triVerts[v].z = pTri->m_Nodes[v]->m_vBasePos.z + fHeightAboveNodeBase;	//1.0f;

		triVerts[v].x += pTri->m_vNormal.x * fExtra;
		triVerts[v].y += pTri->m_vNormal.y * fExtra;
		triVerts[v].z += pTri->m_vNormal.z * fExtra;

		triVerts[v].diffuse = col;//D3DCOLOR_RGBA(64, 64, 255, 128);
	}

	m_pD3DDevice->DrawPrimitiveUP(
		D3DPT_TRIANGLELIST,
		1,
		&triVerts[0],
		sizeof(LitVertex)
	);

	if(bDrawAdjacent)
	{
		int e;
		for(e=0; e<3; e++)
		{
			CNavSurfaceTri * pAdjacentTri = pTri->m_AdjacentTris[e];
			if(!pAdjacentTri)
				continue;

			for(v=0; v<3; v++)
			{
				triVerts[v].x = pAdjacentTri->m_Nodes[v]->m_vBasePos.x;
				triVerts[v].y = pAdjacentTri->m_Nodes[v]->m_vBasePos.y;
				triVerts[v].z = pAdjacentTri->m_Nodes[v]->m_vBasePos.z + fHeightAboveNodeBase;	// 1.0f;
				triVerts[v].diffuse = D3DCOLOR_RGBA(64, 64, 64, 128);
			}

			m_pD3DDevice->DrawPrimitiveUP(
				D3DPT_TRIANGLELIST,
				1,
				&triVerts[0],
				sizeof(LitVertex)
			);
		}
	}

	if(bMarkVerts)
	{
		char tmp[64];
		for(v=0; v<3; v++)
		{
#if __DEV
			sprintf(tmp, "v%i [%i] node:0x%p (%.2f, %.2f, %.2f)", v, pTri->m_Nodes[v]->m_iDebugNodeIndex, pTri->m_Nodes[v], pTri->m_Nodes[v]->m_vBasePos.x, pTri->m_Nodes[v]->m_vBasePos.y, pTri->m_Nodes[v]->m_vBasePos.z);
#else
			sprintf(tmp, "v%i node:0x%p (%.2f, %.2f, %.2f)", v, pTri->m_Nodes[v], pTri->m_Nodes[v]->m_vBasePos.x, pTri->m_Nodes[v]->m_vBasePos.y, pTri->m_Nodes[v]->m_vBasePos.z);
#endif
			DrawString(pTri->m_Nodes[v]->m_vBasePos + ZAXIS, tmp, D3DCOLOR_RGBA(255,255,0,255));
		}
	}
}


void
CNavGenApp::DrawNavSurfaceTris(vector<CNavSurfaceTri*> & pathTris, DWORD col)
{
	int iNumTris = pathTris.size();
	int iNumVerts = iNumTris * 3;

	if(m_iNumPathTriRenderVerts < iNumVerts)
	{
		if(m_pPathTriRenderVerts)
			delete[] m_pPathTriRenderVerts;

		m_pPathTriRenderVerts = rage_new LitVertex[iNumVerts];
	}

	SetColourStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);
	SetAlphaStage(0, D3DTOP_SELECTARG1, D3DTA_DIFFUSE, 0);
	SetColourStage(1, D3DTOP_DISABLE, 0, 0);
	SetAlphaStage(1, D3DTOP_DISABLE, 0, 0);
	SetAlphaBlendEnable(true);
	SetSrcBlend(D3DBLEND_SRCALPHA);
	SetDestBlend(D3DBLEND_INVSRCALPHA);
	m_pD3DDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
	m_pD3DDevice->SetFVF(LitVertexFVF);

	int v,t;
	int vi = 0;

	for(t=0; t<iNumTris; t++)
	{
		CNavSurfaceTri * pTri = pathTris[t];

		for(v=0; v<3; v++)
		{
			m_pPathTriRenderVerts[vi].x = pTri->m_Nodes[v]->m_vBasePos.x;
			m_pPathTriRenderVerts[vi].y = pTri->m_Nodes[v]->m_vBasePos.y;
			m_pPathTriRenderVerts[vi].z = pTri->m_Nodes[v]->m_vBasePos.z + 1.0f;
			m_pPathTriRenderVerts[vi].diffuse = col;

			vi++;
		}
	}

	m_pD3DDevice->DrawPrimitiveUP(
		D3DPT_TRIANGLELIST,
		iNumTris,
		&m_pPathTriRenderVerts[0],
		sizeof(LitVertex)
	);

}



void
CNavGenApp::DrawPathNodes(atArray<Vector3> & pathNodes, atArray<TNavMeshWaypointFlag> & waypointFlags, atArray<Vector3> & pathNodePolyCentroids)
{
	if(pathNodes.GetCount() < 2)
		return;

	s32 p;
	static const float fLineHeight = 2.0f;
	static const float fMarkerHeight = 8.0f;

	// Firstly, draw a poly line connecting all of the path nodes
	DWORD lineCol = D3DCOLOR_RGBA(255, 0, 0, 255);
	DWORD lineDark = D3DCOLOR_RGBA(0, 255, 0, 255);
	DWORD lineLight = D3DCOLOR_RGBA(220, 235, 255, 255);

	// 1st draw in red
	Vector3 vStartVec = pathNodes[0];
	vStartVec.z += fLineHeight;
	StartPolyLine(vStartVec, lineCol);
	for(p=1; p<pathNodes.GetCount(); p++)
	{
		Vector3 vPos = pathNodes[p];
		vPos.z += fLineHeight;
		DrawPolyLineTo(vPos, lineCol);
	}
	// 2nd draw in black
	vStartVec = pathNodes[0];
	vStartVec.z += fLineHeight + 0.1f;
	StartPolyLine(vStartVec, lineDark);
	for(p=1; p<pathNodes.GetCount(); p++)
	{
		Vector3 vPos = pathNodes[p];
		vPos.z += fLineHeight + 0.1f;
		DrawPolyLineTo(vPos, lineDark);
	}
	// 3rd draw in light
	vStartVec = pathNodes[0];
	vStartVec.z += fLineHeight + 0.2f;
	StartPolyLine(vStartVec, lineLight);
	for(p=1; p<pathNodes.GetCount(); p++)
	{
		Vector3 vPos = pathNodes[p];
		vPos.z += fLineHeight + 0.2f;
		DrawPolyLineTo(vPos, lineLight);
	}
	// Then go and render path markers at each node
	DWORD nodeCol = D3DCOLOR_RGBA(255, 255, 255, 255);
	DWORD nodeTopCol = D3DCOLOR_RGBA(255, 0, 0, 255);
	DWORD nodeTopColJumpClimb = D3DCOLOR_RGBA(255, 255, 64, 255);

	DWORD linkToPolyCentroidCol = D3DCOLOR_RGBA(100, 200, 150, 128);

	char tmp[256];

	for(p=0; p<pathNodes.GetCount(); p++)
	{
		Vector3 vPos = pathNodes[p];
		TNavMeshWaypointFlag iWaypointFlags = waypointFlags[p];

		DrawPathNode(
			vPos,
			nodeCol,
			(iWaypointFlags.GetSpecialAction() == 0) ? nodeTopCol : nodeTopColJumpClimb,
			fMarkerHeight
		);

		char actions[256] = { 0 };

		if(iWaypointFlags.IsClimb())
		{
			if(iWaypointFlags.GetSpecialActionFlags()&WAYPOINT_FLAG_CLIMB_HIGH)
				strcat(actions, "high_climb ");
			if(iWaypointFlags.GetSpecialActionFlags()&WAYPOINT_FLAG_CLIMB_LOW)
				strcat(actions, "low_climb ");
		}
		if(iWaypointFlags.IsDrop())
			strcat(actions, "drop ");
		if(iWaypointFlags.IsLadderClimb())
			strcat(actions, "ladder ");
		if(iWaypointFlags.IsNarrowIn())
			strcat(actions, "narrow_in ");
		if(iWaypointFlags.IsNarrowIn())
			strcat(actions, "narrow ");

		StartPolyLine(vPos, linkToPolyCentroidCol);
		DrawPolyLineTo(pathNodePolyCentroids[p], linkToPolyCentroidCol);

		sprintf(tmp, "(%i) %s", p, actions);
		DrawString(vPos + (ZAXIS*2.0f), tmp, D3DCOLOR_RGBA(255,255,200,255));
	}
}

void
CNavGenApp::DrawPathNode(const Vector3 & vPos, DWORD nodeCol, DWORD topCol, float fHeight)
{
	Vector3 vTopPos = vPos;
	vTopPos.z += fHeight;
	
	StartPolyLine(vPos, nodeCol);
	DrawPolyLineTo(vTopPos, nodeCol);

	Vector3 topCross[4] =
	{
		vTopPos - Vector3(1.0f, 0, 0),
		vTopPos + Vector3(1.0f, 0, 0),
		vTopPos - Vector3(0, 1.0f, 0),
		vTopPos + Vector3(0, 1.0f, 0)
	};

	StartPolyLine(topCross[0], topCol);
	DrawPolyLineTo(topCross[1], topCol);

	StartPolyLine(topCross[2], topCol);
	DrawPolyLineTo(topCross[3], topCol);
}


void
CNavGenApp::FindBugriddenNavSurfaceTris(void)
{
	for(u32 t=0; t<m_NodeGenerator.m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NodeGenerator.m_NavSurfaceTriangles[t];
		if(pTri->m_iFlags & NAVSURFTRI_BUGRIDDEN)
		{
			m_BugriddenNavTris.push_back(pTri);
		}
	}
}


void
CNavGenApp::DrawBugriddenTris(void)
{
	DWORD col = D3DCOLOR_RGBA(255, 0, 255, 255);

	for(u32 t=0; t<m_BugriddenNavTris.size(); t++)
	{
		CNavSurfaceTri * pTri = m_BugriddenNavTris[t];
		DrawNavSurfaceTri(pTri, col, false);
	}
}

void CNavGenApp::SanityCheckNavMesh(CNavMesh * pNavMesh)
{
  	const float fCloseEps = 0.005f;

	//***********************************************
	// Check that we don't have duplicated vertices

	s32 iNumIdentical=0;
	u32 v1,v2;
	for(v1=0; v1<pNavMesh->GetNumVertices(); v1++)
	{
		Vector3 vVertex1;
		pNavMesh->GetVertex(v1, vVertex1);

		for(v2=v1+1; v2<pNavMesh->GetNumVertices(); v2++)
		{
			Vector3 vVertex2;
			pNavMesh->GetVertex(v2, vVertex2);

			if(vVertex1.IsClose(vVertex2, fCloseEps))
				iNumIdentical++;
		}
	}
	printf("Num Vertices Identical : %i\n", iNumIdentical);

	//*****************************************
	// Check that every single vertex is used

	atBitSet bits(pNavMesh->GetNumVertices());
	bits.Reset();

	u32 f1,f2;
	for(f1=0; f1<pNavMesh->GetNumPolys(); f1++)
	{
		TNavMeshPoly * pPoly = pNavMesh->GetPoly(f1);

		for(v1=0; v1<pPoly->GetNumVertices(); v1++)
		{
			int iIndex = pNavMesh->GetPolyVertexIndex(pPoly, v1);
			bits.Set(iIndex);
		}
	}
	for(v1=0; v1<pNavMesh->GetNumVertices(); v1++)
	{
		Assert(!bits.IsClear(v1));
	}

	//************************************
	// Check that no faces are duplicates

	for(f1=0; f1<pNavMesh->GetNumPolys(); f1++)
	{
		TNavMeshPoly * pPoly1 = pNavMesh->GetPoly(f1);

		for(f2=f1+1; f2<pNavMesh->GetNumPolys(); f2++)
		{
			TNavMeshPoly * pPoly2 = pNavMesh->GetPoly(f2);

			if(pPoly1->GetNumVertices()==pPoly2->GetNumVertices())
			{
				int iNumVerticesIdentical=0;
				int iNumIndicesIdentical=0;

				for(v1=0; v1<pPoly1->GetNumVertices(); v1++)
				{
					s32 iIndex1 = pNavMesh->GetPolyVertexIndex(pPoly1, v1);
					Vector3 vVertex1;
					pNavMesh->GetVertex(iIndex1, vVertex1);

					for(v2=0; v2<pPoly2->GetNumVertices(); v2++)
					{
						s32 iIndex2 = pNavMesh->GetPolyVertexIndex(pPoly2, v2);
						Vector3 vVertex2;
						pNavMesh->GetVertex(iIndex2, vVertex2);

						if(iIndex1==iIndex2)
							iNumIndicesIdentical++;
						if(vVertex1.IsClose(vVertex2, fCloseEps))
							iNumVerticesIdentical++;
					}
				}

				//Assert(iNumIndicesIdentical < (int)pPoly1->GetNumVertices());
				//Assert(iNumVerticesIdentical < (int)pPoly1->GetNumVertices());
			}
		}
	}
}


void
CNavGenApp::MakeVertexBuffersForAllNavMeshes()
{
	const aiNavDomain domain = kNavDomainRegular;

	if(m_DynamicNavMesh)
	{
		MakeNavMesh_VertexBuffers( m_DynamicNavMesh, m_DynamicNavMeshVertexBuffer, false );
	}
	else
	{
		for(s32 n=0; n<CPathServer::m_pNavMeshStores[domain]->GetMaxMeshIndex(); n++)
		{
			if(CPathServer::m_pNavMeshStores[domain]->GetMeshByIndex(n))
			{
				MakeNavMesh_VertexBuffers( CPathServer::m_pNavMeshStores[domain]->GetMeshByIndex(n), m_NavMeshVertexBuffers[n], false );
			}
		}
	}
}

void CNavGenApp::MakeVertexBufferForTessellationNavMesh()
{
	// And now we want to rebuild all the navmesh vertex buffers for navmeshes, and
	// also for the CPathServer::m_pTessellationNavMesh - if we did any tessellation.

	CNavMeshVertexBuffers & VB = m_NavMeshVertexBuffers[NAVMESH_INDEX_TESSELLATION];

	MakeNavMesh_VertexBuffers( CPathServer::m_pTessellationNavMesh, VB, true );
}

bool CNavGenApp::MakeNavMesh_VertexBuffers( CNavMesh * pNavMesh, CNavMeshVertexBuffers & VB, bool bTessellationNavMesh)
{
	VB.Release();

	const aiNavDomain domain = kNavDomainRegular;

	VB.m_iNavMeshNumTriangles = 0;
	VB.m_iNavMeshNumLines = 0;

	for(u32 p=0; p<pNavMesh->GetNumPolys(); p++)
	{
		TNavMeshPoly & poly = *pNavMesh->GetPoly(p);

		if(poly.TestFlags(NAVMESHPOLY_REPLACED_BY_TESSELLATION))
			continue;
		if(bTessellationNavMesh && !poly.GetIsTessellatedFragment())
			continue;

		VB.m_iNavMeshNumTriangles += poly.GetNumVertices() - 2;
		VB.m_iNavMeshNumLines += poly.GetNumVertices();
	}

	if(!VB.m_iNavMeshNumTriangles)
		return false;

	HRESULT retval;

	int iNumTriVerts = (VB.m_iNavMeshNumTriangles * 3);
	int iNumLineVerts = (VB.m_iNavMeshNumLines * 2);

	// Create the vertex buffer for the triangles
	retval = m_pD3DDevice->CreateVertexBuffer(
		sizeof(LitVertex) * iNumTriVerts,
		D3DUSAGE_WRITEONLY,
		LitVertexFVF,
		D3DPOOL_DEFAULT,
		&VB.m_pNavMeshTrianglesVB,
		NULL
	);
	if(retval != D3D_OK)
	{
		return false;
	}

	// Create the vertex buffer for the lines
	retval = m_pD3DDevice->CreateVertexBuffer(
		sizeof(LitVertex) * iNumLineVerts,
		D3DUSAGE_WRITEONLY,
		LitVertexFVF,
		D3DPOOL_DEFAULT,
		&VB.m_pNavMeshLinesVB,
		NULL
	);
	if(retval != D3D_OK)
	{
		return false;
	}

	LitVertex * pTrianglesVertexData = NULL;
	retval = VB.m_pNavMeshTrianglesVB->Lock(0, sizeof(LitVertex) * iNumTriVerts, (void**)&pTrianglesVertexData, 0 );
	if(retval != D3D_OK)
	{
		return false;
	}

	LitVertex * pLinesVertexData = NULL;
	retval = VB.m_pNavMeshLinesVB->Lock(0, sizeof(LitVertex) * iNumLineVerts, (void**)&pLinesVertexData, 0 );
	if(retval != D3D_OK)
	{
		return false;
	}

	int iTrianglesVertexCount = 0;
	int iLinesVertexCount = 0;

	int iAlpha = bTessellationNavMesh ? 200 : 255;
	float fHeightForTessNavMesh = 0.05f;

	DWORD iTriCol = bTessellationNavMesh ?
		D3DCOLOR_RGBA(150, 120, 150, iAlpha) :
		D3DCOLOR_RGBA(192, 192, 192, iAlpha);

	DWORD iTriColPavement = D3DCOLOR_RGBA(80, 80, 80, iAlpha);
	DWORD iTriColInterior = D3DCOLOR_RGBA(190, 190, 80, iAlpha);
	DWORD iTriColWater = D3DCOLOR_RGBA(60, 60, 255, iAlpha);
	DWORD iTriColWaterShallow = D3DCOLOR_RGBA(200, 200, 255, iAlpha);
	DWORD iTriColIsolated = D3DCOLOR_RGBA(190, 0, 0, iAlpha);
	DWORD iTriColSteep = D3DCOLOR_RGBA(32, 0, 0, iAlpha);
	DWORD iMarkedTriCol = D3DCOLOR_RGBA(255, 255, 64, iAlpha);
	DWORD iStartingTriCol = D3DCOLOR_RGBA(0, 255, 0, iAlpha);
	DWORD iNetworkSpawn = D3DCOLOR_RGBA(255, 255, 255, iAlpha);
	DWORD iLineCol = D3DCOLOR_RGBA(0, 0, 128, 255);

	DWORD iLowClimbOverFromCol = D3DCOLOR_RGBA(255, 0, 255, 255);
	DWORD iLowClimbOverToCol = D3DCOLOR_RGBA(255, 128, 255, 255);
	DWORD iHighClimbOverFromCol = D3DCOLOR_RGBA(128, 0, 128, 255);
	DWORD iHighClimbOverToCol = D3DCOLOR_RGBA(128, 128, 128, 255);
	DWORD iDropDownFromCol = D3DCOLOR_RGBA(255, 128, 0, 255);
	DWORD iDropDownToCol = D3DCOLOR_RGBA(255, 64, 0, 255);

	DWORD iFatalDropCol = D3DCOLOR_RGBA(128, 0, 0, 255);

	DWORD iColLastVert;
	DWORD iColVert;

	Vector3 v1,v2,v3;

	iColLastVert = iLineCol;
	iColVert = iLineCol;

	bool bAltCol = false;
	DWORD iOffCol = D3DCOLOR_RGBA(0, 0, 0, 255);
	DWORD iOnCol = D3DCOLOR_RGBA(255, 0, 0, 255);
	DWORD iOffColEdge = D3DCOLOR_RGBA(0, 0, 255, 255);
	DWORD iOnColEdge = D3DCOLOR_RGBA(255, 0, 255, 255);

	for(u32 p=0; p<pNavMesh->GetNumPolys(); p++)
	{
		TNavMeshPoly * poly = pNavMesh->GetPoly(p);

		if(poly->TestFlags(NAVMESHPOLY_REPLACED_BY_TESSELLATION))
		{
			continue;
		}
		if(bTessellationNavMesh && !poly->GetIsTessellatedFragment())
			continue;

		Vector3 vCentroid;
		pNavMesh->GetPolyCentroid(p, vCentroid);

		if(bTessellationNavMesh)
		{
			vCentroid.z += fHeightForTessNavMesh;
		}

		// Do the edge lines
		int lastv = poly->GetNumVertices()-1;
		u32 v;
		for(v=0; v<poly->GetNumVertices(); v++)
		{
			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(poly, lastv), v1);
			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(poly, v), v2);

			const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly(poly->GetFirstVertexIndex() + lastv);

			if(bTessellationNavMesh)
			{
				v1.z += fHeightForTessNavMesh;
				v2.z += fHeightForTessNavMesh;
			}

			Vector3 vVecToCentroid1 = vCentroid - v1;
			vVecToCentroid1 *= 0.15f;

			Vector3 vVecToCentroid2 = vCentroid - v2;
			vVecToCentroid2 *= 0.15f;

			v1 += vVecToCentroid1;
			v2 += vVecToCentroid2;

			// NB : Put this bit in if trying to debug whether colinear lines have been removed
			// It will colour each edge line-segment a different shade

			int iColStep = 32;
			iColLastVert = D3DCOLOR_RGBA(lastv * iColStep, lastv * iColStep, lastv * iColStep, 255);
			iColVert = D3DCOLOR_RGBA(lastv * iColStep, lastv * iColStep, lastv * iColStep, 255);
			int r = (rand() & 0xFF) + 128;
			if(r > 255) r = 255;
			int g = (rand() & 0xFF) + 128;
			if(g > 255) g = 255;
			int b = (rand() & 0xFF) + 128;
			if(b > 255) b = 255;

			iColLastVert = D3DCOLOR_RGBA(r, g, b, 255);
			iColVert = iColLastVert;

			if(bAltCol)
			{
				if(adjPoly.GetIsExternalEdge())
					iColLastVert = iColVert = iOffColEdge;
				else
					iColLastVert = iColVert = iOffCol;
			}
			else
			{
				if(adjPoly.GetIsExternalEdge())
					iColLastVert = iColVert = iOnColEdge;
				else
					iColLastVert = iColVert = iOnCol;
			}

			if(adjPoly.GetEdgeProvidesCover())
			{
				iColVert = D3DCOLOR_RGBA(255, 255, 0, 255);
				iColLastVert = D3DCOLOR_RGBA(255, 255, 0, 255);
			}

			pLinesVertexData[iLinesVertexCount].x = v1.x;
			pLinesVertexData[iLinesVertexCount].y = v1.y;
			pLinesVertexData[iLinesVertexCount].z = v1.z + 0.01f;
			pLinesVertexData[iLinesVertexCount].diffuse = iColLastVert;
			iLinesVertexCount++;

			pLinesVertexData[iLinesVertexCount].x = v2.x;
			pLinesVertexData[iLinesVertexCount].y = v2.y;
			pLinesVertexData[iLinesVertexCount].z = v2.z + 0.01f;
			pLinesVertexData[iLinesVertexCount].diffuse = iColVert;
			iLinesVertexCount++;

			bAltCol = !bAltCol;
			lastv = v;
		}

		// Do the triangles
		DWORD iPolyCol;
 		if(m_bMarkStartingPolys && poly->TestFlags(NAVMESHPOLY_ALTERNATIVE_STARTING_POLY))
			iPolyCol = iStartingTriCol;
		else if(poly->GetDebugMarked())
			iPolyCol = iMarkedTriCol;
		else if(poly->m_Struct3.m_bZeroAreaStichPolyDLC)
			iPolyCol = D3DCOLOR_RGBA(255, 255, 0, 255);
		else if(poly->GetIsWater())
		{
			//iPolyCol = poly->GetIsShallow() ? iTriColWaterShallow : iTriColWater;
			iPolyCol = iTriColWater;
		}
		else if(poly->GetIsIsolated())
			iPolyCol = iTriColIsolated;
		else if(poly->GetPedDensity()>0)
		{
			iPolyCol = D3DCOLOR_RGBA(poly->GetPedDensity()*32, 0, 0, 255);
		}
		else if(poly->GetIsTrainTrack())
			iPolyCol = D3DCOLOR_RGBA(80, 40, 40, 255);
		else if(poly->TestFlags(NAVMESHPOLY_IS_PAVEMENT))
			iPolyCol = iTriColPavement;
		else if(poly->GetIsInterior())
			iPolyCol = iTriColInterior;
		else if(poly->TestFlags(NAVMESHPOLY_TOO_STEEP_TO_WALK_ON))
			iPolyCol = iTriColSteep;
//		else if(poly->m_NonResourced.m_bNoNetworkSpawn)
//			iPolyCol = D3DCOLOR_RGBA(255, 0, 0, 255);
		else if(poly->GetIsNetworkSpawnCandidate())
			iPolyCol = iNetworkSpawn;
		else
			iPolyCol = iTriCol;

		pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(poly, 0), v1);

		if(bTessellationNavMesh)
		{
			v1.z += fHeightForTessNavMesh;
		}

		for(v=1; v<poly->GetNumVertices()-1; v++)
		{
			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(poly, v), v2);
			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(poly, v+1), v3);

			if(bTessellationNavMesh)
			{
				v2.z += fHeightForTessNavMesh;
				v3.z += fHeightForTessNavMesh;
			}

			pTrianglesVertexData[iTrianglesVertexCount].x = v3.x;
			pTrianglesVertexData[iTrianglesVertexCount].y = v3.y;
			pTrianglesVertexData[iTrianglesVertexCount].z = v3.z;
			pTrianglesVertexData[iTrianglesVertexCount].diffuse = iPolyCol;
			iTrianglesVertexCount++;

			pTrianglesVertexData[iTrianglesVertexCount].x = v2.x;
			pTrianglesVertexData[iTrianglesVertexCount].y = v2.y;
			pTrianglesVertexData[iTrianglesVertexCount].z = v2.z;
			pTrianglesVertexData[iTrianglesVertexCount].diffuse = iPolyCol;
			iTrianglesVertexCount++;

			pTrianglesVertexData[iTrianglesVertexCount].x = v1.x;
			pTrianglesVertexData[iTrianglesVertexCount].y = v1.y;
			pTrianglesVertexData[iTrianglesVertexCount].z = v1.z;
			pTrianglesVertexData[iTrianglesVertexCount].diffuse = iPolyCol;
			iTrianglesVertexCount++;
		}
	}


	//************************************
	// Now do the edge-groups, etc..
	//************************************


	//************************************
	// Unlock everything
	//************************************

	retval = VB.m_pNavMeshTrianglesVB->Unlock();
	retval = VB.m_pNavMeshLinesVB->Unlock();

	return true;
}



void
CNavGenApp::DrawNavMeshCoverPoints(CNavMesh * pNavMesh, CNavMeshQuadTree * pNode, Vector3 * vNavMeshMins, Vector3 * vNavMeshSize)
{
	if(pNode->m_pLeafData)
	{
		static const DWORD iColRed = D3DCOLOR_RGBA(255, 128, 128, 255);
		static const DWORD iColGreen = D3DCOLOR_RGBA(128, 255, 128, 255);
		static const DWORD iColBlue = D3DCOLOR_RGBA(128, 128, 255, 255);
		static const DWORD iColAqua = D3DCOLOR_RGBA(0, 255, 255, 255);
		static const DWORD iColViolet = D3DCOLOR_RGBA(255, 0, 255, 255);
		static const DWORD iColYellow = D3DCOLOR_RGBA(255, 255, 0, 255);
		static const DWORD iColWhite = D3DCOLOR_RGBA(255, 255, 255, 255);
		static const DWORD iColNavyBlue = D3DCOLOR_RGBA(0, 0, 255, 255);
		static const DWORD iColLinkStart = D3DCOLOR_RGBA(0, 128, 0, 255);
		static const DWORD iColLinkEnd = D3DCOLOR_RGBA(128, 0, 0, 255);
		
		DWORD iCoverCol;
		Vector3 vCoverArrowEnd;

		for(int c=0; c<pNode->m_pLeafData->m_iNumCoverPoints; c++)
		{
			CNavMeshCoverPoint & coverPoint = pNode->m_pLeafData->m_CoverPoints[c];
			Vector3 vCoverPointBase;
			DecompressVertex(
				vCoverPointBase,
				coverPoint.m_iX,
				coverPoint.m_iY,
				coverPoint.m_iZ,
				*vNavMeshMins,
				*vNavMeshSize
			);

			
			u32 iCoverType = coverPoint.m_CoverPointFlags.GetCoverType();
			if(iCoverType == NAVMESH_COVERPOINT_LOW_WALL) iCoverCol = iColBlue;
			else if(iCoverType == NAVMESH_COVERPOINT_LOW_WALL_TO_LEFT) iCoverCol = iColAqua;
			else if(iCoverType == NAVMESH_COVERPOINT_LOW_WALL_TO_RIGHT) iCoverCol = iColViolet;
			else if(iCoverType == NAVMESH_COVERPOINT_WALL_TO_LEFT) iCoverCol = iColGreen;
			else if(iCoverType == NAVMESH_COVERPOINT_WALL_TO_RIGHT) iCoverCol = iColRed;
			else if(iCoverType == NAVMESH_COVERPOINT_WALL_TO_NEITHER) iCoverCol = iColYellow;
			else iCoverCol = iColNavyBlue;

			u32 iCoverDir = coverPoint.m_CoverPointFlags.GetCoverDir();
			Assert(iCoverDir < 256);
			Vector3FromCoverDir(vCoverArrowEnd, (u8)iCoverDir);

			vCoverArrowEnd += vCoverPointBase;

			StartPolyLine(vCoverPointBase, iColNavyBlue);
			DrawPolyLineTo(vCoverPointBase + Vector3(0.0f, 0.0f, 3.0f), iCoverCol);

			StartPolyLine(vCoverPointBase + Vector3(0.0f, 0.0f, 1.0f), iColNavyBlue);
			DrawPolyLineTo(vCoverArrowEnd + Vector3(0.0f, 0.0f, 1.0f), iColNavyBlue);
		}
	}
	else
	{
		for(int q=0; q<4; q++)
		{
			if(pNode->m_pChildren[q])
			{
				DrawNavMeshCoverPoints(pNavMesh, pNode->m_pChildren[q], vNavMeshMins, vNavMeshSize);
			}
		}
	}
}

void
CNavGenApp::DrawNavMeshSpecialLinks(CNavMesh * pNavMesh)
{
	CSpecialLinkInfo * pLink;
	Vector3 vLinkStart, vLinkEnd;
	u32 s;

	static const DWORD iColWhite = D3DCOLOR_RGBA(240, 255, 255, 255);
	static const DWORD iColLightGreen = D3DCOLOR_RGBA(170, 255, 170, 255);
	static const DWORD iColDarkGreen = D3DCOLOR_RGBA(100, 200, 100, 255);
	static const DWORD iColBlack = D3DCOLOR_RGBA(0, 0, 0, 255);
	static const DWORD iColDarkOrange = D3DCOLOR_RGBA(100, 100, 50, 255);

	for(s=0; s<pNavMesh->GetNumSpecialLinks(); s++)
	{
		pLink = &pNavMesh->GetSpecialLinks()[s];

		CNavMesh * pStartNavMesh = CPathServer::GetNavMeshFromIndex(pLink->GetLinkFromNavMesh(), kNavDomainRegular);
		CNavMesh * pOriginalStartNavMesh = CPathServer::GetNavMeshFromIndex(pLink->GetOriginalLinkFromNavMesh(), kNavDomainRegular);
		CNavMesh * pEndNavMesh = CPathServer::GetNavMeshFromIndex(pLink->GetLinkToNavMesh(), kNavDomainRegular);
		CNavMesh * pOriginalEndNavMesh = CPathServer::GetNavMeshFromIndex(pLink->GetOriginalLinkToNavMesh(), kNavDomainRegular);

		pOriginalStartNavMesh->DecompressVertex(vLinkStart, pLink->GetLinkFromPosX(), pLink->GetLinkFromPosY(), pLink->GetLinkFromPosZ(), pOriginalStartNavMesh->GetQuadTree()->m_Mins, pOriginalStartNavMesh->GetExtents());
		pOriginalEndNavMesh->DecompressVertex(vLinkEnd, pLink->GetLinkToPosX(), pLink->GetLinkToPosY(), pLink->GetLinkToPosZ(), pOriginalEndNavMesh->GetQuadTree()->m_Mins, pOriginalEndNavMesh->GetExtents());

		Vector3 vStartPolyCentroid;
		pStartNavMesh->GetPolyCentroid(pLink->GetLinkFromPoly(), vStartPolyCentroid);
		Vector3 vEndPolyCentroid;
		pEndNavMesh->GetPolyCentroid(pLink->GetLinkToPoly(), vEndPolyCentroid);

		StartPolyLine(vStartPolyCentroid, iColDarkOrange);

		DrawPolyLineTo(vLinkStart, iColWhite);
		DrawPolyLineTo(vLinkStart + Vector3(0.0f, 0.0f, 0.5f), iColLightGreen);
		DrawPolyLineTo(vLinkEnd + Vector3(0.0f, 0.0f, 1.0f), iColLightGreen);
		DrawPolyLineTo(vLinkEnd, iColDarkGreen);

		DrawPolyLineTo(vEndPolyCentroid, iColDarkOrange);


		Vector3 vTextPos = ((vLinkStart + vLinkEnd) * 0.5f) + (ZAXIS * 3.0f);
		char tmp[256];
		sprintf(tmp, "link:%i", s, D3DCOLOR_RGBA(128,128,80,255));
		DrawString(vTextPos, tmp);
	}
}

void CNavGenApp::DrawNavMeshNonStandardAdjacencies(CNavMesh * pNavMesh)
{
	DWORD colOrange1 = D3DCOLOR_RGBA(255,200,0,255);
	DWORD colOrange2 = D3DCOLOR_RGBA(255,128,0,255);
	DWORD colOrange3 = D3DCOLOR_RGBA(255,80,0,255);
	DWORD colRed = D3DCOLOR_RGBA(255,0,0,255);

	static float fMaxDistSqr = 100.0f*100.0f;

	u32 p,a;
	for(p=0; p<pNavMesh->GetNumPolys(); p++)
	{
		TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);
		for(a=0; a<pPoly->GetNumVertices(); a++)
		{
			const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly( pPoly->GetFirstVertexIndex() + a );
			if(adjPoly.GetAdjacencyType() == ADJACENCY_TYPE_NORMAL)
				continue;

			CNavMesh * pAdjNavMesh = CPathServer::GetNavMeshFromIndex( adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes()), kNavDomainRegular );
			if(!pAdjNavMesh)
				continue;
			TNavMeshPoly * pAdjPoly = pAdjNavMesh->GetPoly( adjPoly.GetPolyIndex() );

			Vector3 vEdgeMid;
			pNavMesh->GetPolyEdgeMidpoint(vEdgeMid, pPoly, a);

			Vector3 vToPolyMid;
			pAdjNavMesh->GetPolyCentroidQuick(pAdjPoly, vToPolyMid);

			if((vEdgeMid - m_vViewPos).Mag2() > fMaxDistSqr && (vToPolyMid - m_vViewPos).Mag2() > fMaxDistSqr)
				continue;

			bool bHighDrop = adjPoly.GetHighDropOverEdge();
			float fMaxZ = Max(vEdgeMid.z, vToPolyMid.z);
			float fHeightAbove = 0.0f;

			switch(adjPoly.GetAdjacencyType())
			{
				case ADJACENCY_TYPE_CLIMB_LOW:
					if(!m_bDrawLowClimbs)
						continue;
					fHeightAbove = 2.0f;
					break;
				case ADJACENCY_TYPE_CLIMB_HIGH:
					if(!m_bDrawHighClimbs)
						continue;
					fHeightAbove = 4.0f;
					break;
				case ADJACENCY_TYPE_DROPDOWN:
					if(!m_bDrawDropDowns)
						continue;
					fHeightAbove = 0.5f;
					break;
			}

			AddLine( vEdgeMid, Vector3(vEdgeMid.x, vEdgeMid.y, fMaxZ + fHeightAbove), colOrange1, colOrange1);
			AddLine( Vector3(vEdgeMid.x, vEdgeMid.y, fMaxZ + fHeightAbove), Vector3(vToPolyMid.x, vToPolyMid.y, fMaxZ + fHeightAbove), colOrange1, colOrange1);
			AddLine( Vector3(vToPolyMid.x, vToPolyMid.y, fMaxZ + fHeightAbove), vToPolyMid, colOrange1, bHighDrop ? colRed : colOrange1);
		}
	}
}

void
CNavGenApp::RenderNavMeshPathPolysEtc(void)
{
	DWORD iPtCol = D3DCOLOR_RGBA(255, 255, 0, 255);
	DWORD iHierPtCol = D3DCOLOR_RGBA(255, 255, 255, 255);
	DWORD iHierMarkerCol = D3DCOLOR_RGBA(128, 128, 255, 255);

	Vector3 vHeightAdd(0,0,0.5f);

	for(int v=0; v<m_iNavMeshNumPointsInPolys; v++)
	{
		StartPolyLine(m_vNavMeshPointsInPolys[v], iPtCol);

		Vector3 vPtAbove = m_vNavMeshPointsInPolys[v] + vHeightAdd;
		DrawPolyLineTo(vPtAbove, iPtCol);
	}

	DWORD iTriCol = D3DCOLOR_RGBA(128, 64, 64, 64);
	//DWORD iTriCol = D3DCOLOR_RGBA(255, 255, 255, 255);

	for(int t=0; t<m_iNavMeshNumTriPts; t+=3)
	{
		AddTri(m_vNavMeshPolyTriVerts[t], m_vNavMeshPolyTriVerts[t+1], m_vNavMeshPolyTriVerts[t+2], iTriCol, iTriCol, iTriCol);
	}

	int iNumH = m_vNavMeshHierarchicalPoints.GetCount();
	int h;

	for(h=0; h<iNumH; h++)
	{
		Vector3 vec = m_vNavMeshHierarchicalPoints[h];
		Vector3 nextvec = vec;
		nextvec.z += 20.0f;

		StartPolyLine(vec, iHierMarkerCol);
		DrawPolyLineTo(nextvec, iHierMarkerCol);
	}
	for(h=0; h<iNumH-1; h++)
	{
		Vector3 vec = m_vNavMeshHierarchicalPoints[h];
		vec.z += 20.0f;
		Vector3 nextvec = m_vNavMeshHierarchicalPoints[h+1];
		nextvec.z += 20.0f;

		StartPolyLine(vec, iHierPtCol);
		DrawPolyLineTo(nextvec, iHierPtCol);
	}
}



void CNavGenApp::DrawHierarchicalNavData(CNavMesh * pNavMesh, CHierarchicalNavData * pHierNav)
{
	const DWORD iNodeCol1 = D3DCOLOR_COLORVALUE(0.93f, 0.85f, 0.68f, 1.0f);
	const DWORD iNodeCol2 = D3DCOLOR_COLORVALUE(0.96f, 0.87f, 0.70f, 1.0f);
	const DWORD iLinkCol1 = D3DCOLOR_COLORVALUE(0.94f, 1.0f, 1.0f, 1.0f);
	const DWORD iLinkCol2 = D3DCOLOR_COLORVALUE(0.88f, 0.93f, 0.93f, 1.0f);

	const aiNavDomain domain = kNavDomainRegular;

	u32 n,l;
	Vector3 vNodePos, vLinkedNodePos;

	for(n=0; n<pHierNav->GetNumNodes(); n++)
	{
		CHierarchicalNavNode * pNode = pHierNav->GetNode(n);
		pNode->GetNodePosition(vNodePos, pHierNav->GetMin(), pHierNav->GetSize());
		AddLine(vNodePos, vNodePos + Vector3(0.0f,0.0f,2.0f), iNodeCol1, iNodeCol2);

		for(l=0; l<pNode->GetNumLinks(); l++)
		{
			CHierarchicalNavLink * pLink = pHierNav->GetLink(pNode->GetStartOfLinkData()+l);

			CNavMesh * pLinkedNavMesh;
			CHierarchicalNavData * pLinkedHierNav;

			const u32 iLinkedNavMesh = pLink->GetNavMeshIndex();
			if(iLinkedNavMesh==pNode->GetNavMeshIndex())
			{
				pLinkedNavMesh = pNavMesh;
				pLinkedHierNav = pHierNav;
			}
			else
			{
				pLinkedNavMesh = CPathServer::m_pNavMeshStores[domain]->GetMeshByIndex(pLink->GetNavMeshIndex());
#if __HIERARCHICAL_NODES_ENABLED
				s32 iNodesIndex = CPathServer::NavMeshIndexToNavNodesIndex(pLink->GetNavMeshIndex());
				pLinkedHierNav = CPathServer::m_pNavNodesStore->GetMeshByIndex(iNodesIndex);
#endif
			}

			if(pLinkedNavMesh && pLinkedHierNav)
			{
				CHierarchicalNavNode * pLinkedNode = pLinkedHierNav->GetNode(pLink->GetNodeIndex());
				pLinkedNode->GetNodePosition(vLinkedNodePos, pLinkedHierNav->GetMin(), pLinkedHierNav->GetSize());
				AddLine(vNodePos + Vector3(0.0f,0.0f,0.75f), vLinkedNodePos + Vector3(0.0f,0.0f,1.0f), iLinkCol1, iLinkCol2);
			}
		}
	}
}

void CNavGenApp::DisplayPathStats()
{
	int iYPos = 72;
	char txt[256];

	CPathRequest * pPathReq = GetLastPath();
	if(!pPathReq)
		return;

	
	sprintf(txt, "m_fTotalProcessingTimeInMillisecs : %.2f", pPathReq->m_fTotalProcessingTimeInMillisecs);
	iYPos = DrawString(0, iYPos, txt);
	sprintf(txt, "m_fMillisecsToFindPolys : %.2f", pPathReq->m_fMillisecsToFindPolys);
	iYPos = DrawString(0, iYPos, txt);
	sprintf(txt, "m_fMillisecsToFindPath : %.2f", pPathReq->m_fMillisecsToFindPath);
	iYPos = DrawString(0, iYPos, txt);
	sprintf(txt, "m_fMillisecsToRefinePath : %.2f", pPathReq->m_fMillisecsToRefinePath);
	iYPos = DrawString(0, iYPos, txt);
	sprintf(txt, "m_fMillisecsToPostProcessZHeights : %.2f", pPathReq->m_fMillisecsToPostProcessZHeights);
	iYPos = DrawString(0, iYPos, txt);
	sprintf(txt, "m_fMillisecsToSmoothPath : %.2f", pPathReq->m_fMillisecsToSmoothPath);
	iYPos = DrawString(0, iYPos, txt);
	sprintf(txt, "m_fMillisecsToMinimisePathLength : %.2f", pPathReq->m_fMillisecsToMinimisePathLength);
	iYPos = DrawString(0, iYPos, txt);
	sprintf(txt, "m_fMillisecsSpentInTessellation : %.2f", pPathReq->m_fMillisecsSpentInTessellation);
	iYPos = DrawString(0, iYPos, txt);
	sprintf(txt, "m_iNumInitiallyFoundPolys : %i", pPathReq->m_iNumInitiallyFoundPolys);
	iYPos = DrawString(0, iYPos, txt);
	sprintf(txt, "m_iNumVisitedPolygons : %i", pPathReq->m_iNumVisitedPolygons);
	iYPos = DrawString(0, iYPos, txt);
	sprintf(txt, "m_iNumTessellations : %i", pPathReq->m_iNumTessellations);
	iYPos = DrawString(0, iYPos, txt);
	sprintf(txt, "m_iNumTessellatedPolys : %i", pPathReq->m_iNumTessellatedPolys);
	iYPos = DrawString(0, iYPos, txt);
	sprintf(txt, "m_iNumTestDynamicObjectLOS : %i", pPathReq->m_iNumTestDynamicObjectLOS);
	iYPos = DrawString(0, iYPos, txt);
	sprintf(txt, "m_iNumTestNavMeshLOS : %i", pPathReq->m_iNumTestNavMeshLOS);
	iYPos = DrawString(0, iYPos, txt);
	sprintf(txt, "m_iNumTimesSlept : %i", pPathReq->m_iNumTimesSlept);
	iYPos = DrawString(0, iYPos, txt);
	sprintf(txt, "m_iNumFindPathIterations : %i", pPathReq->m_iNumFindPathIterations);
	iYPos = DrawString(0, iYPos, txt);
	sprintf(txt, "m_iNumRefinePathIterations : %i", pPathReq->m_iNumRefinePathIterations);
	iYPos = DrawString(0, iYPos, txt);

}




