/////////////////////////////////////////////////////////////////////////////////
// FILE		: CAudioLevelMesh.cpp
// PURPOSE	: To load in level geometry for the audio mesh generation tool.
// AUTHOR	: C. Walder (based on Recast be Mikko Menonen)
// STARTED	: 31/08/2009
///////////////////////////////////////////////////////////////////////////////

#include "AudioGenTimer.h"
#include <windows.h>

agTimeVal agGetPerformanceTimer()
{
	__int64 count;
	QueryPerformanceCounter((LARGE_INTEGER*)&count);
	return count;
}

int agGetDeltaTimeUsec(agTimeVal start, agTimeVal end)
{
	static __int64 freq = 0;
	if (freq == 0)
		QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
	__int64 elapsed = end - start;
	return (int)(elapsed*1000000 / freq);
}
