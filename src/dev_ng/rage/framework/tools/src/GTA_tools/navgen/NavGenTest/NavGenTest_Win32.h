#ifndef NAVGEN_WIN32
#define NAVGEN_WIN32

//Rage headers
#include "system\xtl.h"	// Solves masses of compile errors (warning C4668). Read the RAGE 0.4 "readme.txt" for why this is needed.

//Windows & D3D headers
#include <d3d9.h>
#include <d3dx9.h>
#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>
//#include <crtdbg.h>	// catch mem leaks

//Navgen headers
#include "fwnavgen/config.h"
#include "fwnavgen/datatypes.h"
#include "fwnavgen/NavGen.h"
#include "fwnavgen/NavGen_Octree.h"
#include "fwnavgen/NavGen_Utils.h"
#include "pathserver\PathServer.h"
#include "ai/navmesh/tessellation.h"

//int PASCAL WINAPI WinMain(HINSTANCE instance, HINSTANCE prev_instance, LPSTR cmd_line, int cmd_show);

#define DEBUGOUT(txt)			{ OutputDebugString(txt); }
#define DEBUGOUT1(fmt, a)		{ char tmp[256]; sprintf(tmp, fmt, a); OutputDebugString(tmp); }
#define DEBUGOUT2(fmt, a, b)	{ char tmp[256]; sprintf(tmp, fmt, a, b); OutputDebugString(tmp); }
#define DEBUGOUT3(fmt, a, b, c)	{ char tmp[256]; sprintf(tmp, fmt, a, b, c); OutputDebugString(tmp); }

struct LitVertex;

#define MAX_NUM_NAVMESH_VBS		(NAVMESH_INDEX_TESSELLATION+1)

/*
struct CNavGenTestConfig : public fwNavGenConfig
{
	static const CNavGenTestConfig& Get()
	{
		return static_cast<const CNavGenTestConfig&>(fwNavGenConfigManager::GetInstance().GetConfig());
	}
};
*/

struct CNavMeshGeneratorConfig : public fwNavGenConfig
{
	static const CNavMeshGeneratorConfig& Get()
	{	return static_cast<const CNavMeshGeneratorConfig&>(fwNavGenConfigManager::GetInstance().GetConfig());	}

	bool	m_ExportVehicles;

	//PAR_PARSABLE;
};

class CNavGenExtraInfo
{
public:

	vector<fwNavLadderInfo> m_LaddersList;
	vector<TCarNode*> m_CarNodesList;
	vector<spdSphere*> m_PortalBoundaries;
	vector<fwDynamicEntityInfo*> m_DynamicEntityBounds;
};

class CNavMeshVertexBuffers
{
public:
	CNavMeshVertexBuffers() { memset(this, 0, sizeof(this)); }
	void Release()
	{
		if(m_pNavMeshTrianglesVB) m_pNavMeshTrianglesVB->Release();
		if(m_pNavMeshLinesVB) m_pNavMeshLinesVB->Release();

		m_pNavMeshTrianglesVB = NULL;
		m_pNavMeshLinesVB = NULL;

		m_iNavMeshNumTriangles = 0;
		m_iNavMeshNumLines = 0;
	}
public:

	// The vertex buffers etc for all meshes in the pathserver
	LPDIRECT3DVERTEXBUFFER9 m_pNavMeshTrianglesVB;
	LPDIRECT3DVERTEXBUFFER9 m_pNavMeshLinesVB;
	int m_iNavMeshNumTriangles;
	int m_iNavMeshNumLines;
};


class CNavGenApp
{
public:
	CNavGenApp(void);
	~CNavGenApp(void);

	//**************************************************
	// Win32 & system stuff
	//**************************************************

	void ClearMembers(void);
	bool Init(HINSTANCE instance);
	bool Shutdown(void);

	bool InitWindow(void);
	int MsgLoop(void);
	static LRESULT CALLBACK ProcMsg(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);

	char * SelectFileToOpen(char * pDialogTitle, char * pFilterString);
	char m_FileName[256];
	char m_FileTitle[256];

	//**************************************************
	// Win32 & app variables
	//**************************************************

	HINSTANCE m_Hinstance;
	HWND m_Hwnd;

	int m_ScreenWidth;
	int m_ScreenHeight;
	int m_WindowLeftEdge;
	int m_WindowTopEdge;

	float m_SecsThisFrame;
	bool m_bHasLoadedUp;

	u32 m_iNumNodesVisitedForRender;
	u32 m_iNumLeavesVisitedForRender;
	u32 m_iNumNavMeshTrisRenderedThisFrame;

	u8 m_Keys[256];
	u8 m_LastKeys[256];

	inline bool KeyJustDown(u8 keycode)
	{
		return(m_Keys[keycode] && !m_LastKeys[keycode]);
	}
	inline bool KeyJustUp(u8 keycode)
	{
		return(!m_Keys[keycode] && m_LastKeys[keycode]);
	}
	inline bool KeyDown(u8 keycode)
	{
		return(m_Keys[keycode]);
	}
	inline bool KeyUp(u8 keycode)
	{
		return(!m_Keys[keycode]);
	}

	bool m_bMouseLook;
	bool m_bCursorConfinedToWindow;

	int m_MouseXPos;
	int m_MouseYPos;
	bool m_bMouseClicked;
	bool m_bMouseHeld;

	bool m_bCtrlDown;

	// This index to the currently selected collsion tri (or -1 if none selected)
	s32 m_iSelectedTri;
	// This pointer is to the surrently selected nav-surface tri (or NULL if none selected).
	// NB Careful when optimising the surface, since triangles tend to disappear..
	CNavSurfaceTri * m_pSelectedNavSurfaceTri;
	CNavSurfaceTri * m_pDestinationNavSurfaceTri;

	LitVertex * m_pPathTriRenderVerts;
	int m_iNumPathTriRenderVerts;

	// The list of path triangles, which form an initial path guide
	vector<CNavSurfaceTri*> m_PathTris;
	// The list of path nodes we will actully used - refined from the above path triangles
	atArray<Vector3> m_PathNodes;

	void DrawNodes();

	void DrawExtrasInfo();

	void DrawNavSurface();
	void DrawNavSurfaceTri(CNavSurfaceTri * pTri, DWORD col, bool bDrawAdjacent, bool bMarkVerts=true, const float fExtra=0.0f);
	void DrawNavSurfaceTris(vector<CNavSurfaceTri*> & pathTris, DWORD col);

	void DrawPathNodes(atArray<Vector3> & pathNodes, atArray<TNavMeshWaypointFlag> & waypointFlags, atArray<Vector3> & pathNodePolyCentroids);
	void DrawPathNode(const Vector3 & vPos, DWORD nodeCol, DWORD topCol, float fHeight);

	void ConfineCursorToWindow(bool bConfine);

	void FindBugriddenNavSurfaceTris(void);
	void DrawBugriddenTris(void);
	vector<CNavSurfaceTri*> m_BugriddenNavTris;

	//**************************************************
	// Direct Input stuff
	//**************************************************

	bool InitDirectInput(void);
	bool ShutdownDirectInput(void);

	LPDIRECTINPUT8 m_pDirectInput8;
	LPDIRECTINPUTDEVICE8 m_pMouseDevice;

	void AcquireMouse(bool bAcquire);

	//**************************************************
	// D3D stuff
	//**************************************************

	bool InitD3D(void);
	void ShutdownD3D(void);
	void D3DError(HRESULT retval);

	IDirect3D9 * m_pD3D9;
	IDirect3DDevice9 * m_pD3DDevice;
	D3DCAPS9 m_D3DCaps;
	_D3DPRESENT_PARAMETERS_ m_D3DPresentParams;
	D3DVIEWPORT9 m_D3DViewPort;
	int m_ViewPortWidth;
	int m_ViewPortHeight;
	LPD3DXFONT m_pD3DXFont;

	D3DXMATRIX m_ViewMatrix;
	D3DXMATRIX m_ProjectionMatrix;

	float m_FOV;

	// Vertex-Buffer containing the triangles in the currently-being-generated navmesh
	LPDIRECT3DVERTEXBUFFER9 m_pNavSurfaceVertexBuffer;
	s32 m_iNumTrianglesInNavSurfaceVB;
	bool m_bNavSurfaceHasChanged;

	// Vertex-Buffer for rendering the collision polys as trilists
	LPDIRECT3DVERTEXBUFFER9 m_pCollisionPolysVB;
	s32 m_iNumVertsInCollisionPolyVB;
	s32 m_iMaxNumVertsInCollisionPolyVB;
	LitVertex * m_pCollisionPolyVertexData;

	// Vertex-Buffer for rendering line segments
	LPDIRECT3DVERTEXBUFFER9 m_pLineSegVB;
	s32 m_iNumVertsInLineSegVB;
	s32 m_iMaxNumVertsInLineSegVB;
	LitVertex * m_pLineSegVertexData;

	// Vertex-Buffer for rendering voxel map
	LPDIRECT3DVERTEXBUFFER9 m_pVoxelMapVertexBuffer;
	s32 m_iNumTrianglesInVoxelMapVB;

	//**************************************************
	// D3D helper functions, etc
	//**************************************************

	DWORD m_iColourOp[8];
	DWORD m_iColourArg1[8];
	DWORD m_iColourArg2[8];
	DWORD m_iAlphaOp[8];
	DWORD m_iAlphaArg1[8];
	DWORD m_iAlphaArg2[8];

	bool m_bAlphaBlendEnabled;
	D3DBLEND m_SrcBlend;
	D3DBLEND m_DestBlend;

	D3DCOLOR m_ClearCol;
	D3DCOLOR m_FogCol;
	bool m_bUseFogging;
	bool m_bPixelFog;

	// Frustum planes in worldspace
	enum FrustumPlanes
	{
		FRUSTUM_NEAR,
		FRUSTUM_FAR,
		FRUSTUM_LEFT,
		FRUSTUM_RIGHT,
		FRUSTUM_TOP,
		FRUSTUM_BOTTOM
	};
	Vector3 m_vFrustumPlaneNormals[6];
	float m_fFrustumPlaneDists[6];

	float m_fNearPlaneDistance;
	float m_fFarPlaneDistance;

	void SetFogging(void);
		
	void SetColourStage(DWORD stage, DWORD op, DWORD arg1, DWORD arg2);
	void SetAlphaStage(DWORD stage, DWORD op, DWORD arg1, DWORD arg2);

	void SetAlphaBlendEnable(bool bState);
	void SetSrcBlend(D3DBLEND srcBlend);
	void SetDestBlend(D3DBLEND destBlend);

	// Adds a tri to our collision-triangle VertexBuffer
	bool AddTri(Vector3 & v1, Vector3 & v2, Vector3 & v3, DWORD rgb1, DWORD rgb2, DWORD rgb3);
	bool AddPoly(int numverts, Vector3 * verts, DWORD col);
	bool LockCollTriVB(void);
	bool UnlockCollTriVB(void);

	// Adds a line to our line-segment VertexBuffer
	bool AddLine(const Vector3 & v1, const Vector3 & v2, DWORD rgb1, DWORD rgb2);
	bool LockLineSegVB(void);
	bool UnlockLineSegVB(void);

	void StartPolyLine(const Vector3 & vStartPos, u32 col);
	void DrawPolyLineTo(const Vector3 & vPos, u32 col);
	void DrawBoxFromMinMax(const Vector3 & vMin, const Vector3 & vMax, u32 col);
	void DrawOrientedBoxFromMinMax(const Vector3 & vMin, const Vector3 & vMax, const Matrix34 & mTransform, u32 col);
	void DrawSphere(const Vector3 & vPos, const float fRadius, u32 col);
	void DrawAxis(const Vector3 & vOrigin, const Vector3 & vX, const Vector3 & vY, const Vector3 & vZ, const float fSize);

	// Extract the frustum planes from the world * projection matrix
	void ExtractFrustumPlanes(void);

	// Returns whether the AABB is intersecting the current view frustum
	bool BoxIntersectsFrustum(const Vector3 & vBoxMin, const Vector3 & vBoxMax);

	// Given a screen pos (x,y) this will generate appropriate projected points on the near & far clip planes
	void ConvertScreenCoordsToPointsOnNearAndFarPlanes(int screenX, int screenY, Vector3 & outPosNear, Vector3 & outPosFar);

	// Print rendering stats
	bool m_bPrintRenderInfo;
	void PrintRenderInfo(void);

	void D3DXMatrixToCMatrix(Matrix34 & mat, const D3DXMATRIX & d3dMat);
	void CMatrixToD3DMatrix(D3DXMATRIX & d3dMat, const Matrix34 & mat);

	//**************************************************
	// Node-Gen stuff
	//**************************************************

	bool m_bDrawCollisionPolysDumbass;

	bool m_bDrawOctreeBounds;
	bool m_bDrawCollisionPolys;
	bool m_bDrawSplitPolys;
	bool m_bDrawWater;
	bool m_bDrawPathNodes;
	bool m_bDrawNavigationSurfaces;
	bool m_bToggleNavigationSurfaceSolid;
	bool m_bDisplayPathServerMesh;
	bool m_bShadeSurfaceTypes;

	bool Run(float fSecs);

	void DrawText(float fSecs);
	int DrawString(int x, int y, char * txt, DWORD textCol=0xFFFFFFFF);
	int DrawString(const Vector3 & vPos, char * txt, DWORD textCol=0xFFFFFFFF);

	bool CreateOctree(void);

	void GetInput(float fSecs);
	void DrawFrame(void);

	// Brain-dead way of drawing the collision polys, likely to be extremely slow
	void DrawTrisDumbass();
	void DrawResolutionAreas();
	void DrawAdjacenciesForPoly(TNavMeshPoly * pPoly);
	void DrawAuthoredAdjacencies();

	void ClearAllPolyCosts();

	void DrawPolyCosts(CNavMesh * pNavMesh);
	void DrawPolyDistanceTravelled(CNavMesh * pNavMesh);
	void DrawPointsInPolys(CNavMesh * pNavMesh);
	void DrawPolyParents(CNavMesh * pNavMesh);

	void DrawOctree(void);
	void DrawOctreeR(CNavGenOctreeNodeBase * pNodeBase, const Vector3 & vNodeMin, const Vector3 & vNodeMax);

	void DrawWater();

	void DrawResAreas(void);

#ifdef VISUALISE_NAVIGATION_SURFACE
	// Creates the vertex & index buffers required to display this node's naviagation surface triangles
	void CreateVertexAndIndexBuffersForNavSurface(void);
	void CreateVertexBufferForVoxelMap();
#endif

	Vector3 m_vViewPos;
	Matrix34 m_mViewOrientation;
	Vector3 m_vViewForwards;

	// The extents which importer will restrict imported data to
	Vector3 m_vRestrictMin;
	Vector3 m_vRestrictMax;

	// All geometry outside of these extents will be chopped away
	// NB : A lot of this shit should be in the NodeGenParams and not randomly placed here..
	Vector3 m_vSectorMins;
	Vector3 m_vSectorMaxs;
	int m_iSectorStartX;
	int m_iSectorStartY;
	int m_iNumSectorsX;
	int m_iNumSectorsY;

	// The extents of the world which has been imported
	Vector3 m_vWorldMin;
	Vector3 m_vWorldMax;

	CNavGen m_NodeGenerator;

	CNavGenExtraInfo m_ExtraInfo;

	static float m_fDistanceToSeedNodes;
	static float m_TriangulationMaxHeightDiff;
	static float m_HeightAboveNodeBaseAtWhichToTriangulate;
	static float m_MaxHeightChangeUnderTriangleEdge;
	static bool m_bFixJaggiesAfterTriangulation;
	static bool m_bMoveNodesAwayFromWalls;
	static bool m_bUseTestSlicePlane;
	static float m_fTestSlicePlaneZ;
	static bool ms_bRemoveTrisBehindWalls;
	static float m_fTestClearHeightInTriangulation;

	char m_NameOfCurrentFile[256];

	bool m_bHasHighFreqTimer;

	LARGE_INTEGER m_iPerformanceCounterFrequency;
	double m_fHighFreqTimerFrequency;

	// a single nav mesh for viewing
	CNavMesh * m_pSingleNavMesh;
	bool MakeNavMesh_VertexBuffers( CNavMesh * pNavMesh, CNavMeshVertexBuffers & VB, bool bTessellationNavMesh );

	LPDIRECT3DVERTEXBUFFER9 m_pSingleNavMeshTrianglesVB;
	LPDIRECT3DVERTEXBUFFER9 m_pSingleNavMeshLinesVB;
	LPDIRECT3DVERTEXBUFFER9 m_pSingleNavMeshAdjLinesVB;
	int m_iSingleNavMeshNumTriangles;
	int m_iSingleNavMeshNumLines;
	int m_iSingleNavMeshNumAdjLines;

	void MakeVertexBuffersForAllNavMeshes();
	void MakeVertexBufferForTessellationNavMesh();
	void SanityCheckNavMesh(CNavMesh * pNavMesh);

	void DrawNavMeshCoverPoints(CNavMesh * pNavMesh, CNavMeshQuadTree * pNode, Vector3 * vNavMeshMins, Vector3 * vNavMeshSize);
	CNavMeshCoverPoint * GetNavMeshCoverPointByIndex(CNavMeshQuadTree * pTree, int iCoverPointIndex);

	void DrawNavMeshNonStandardAdjacencies(CNavMesh * pNavMesh);
	void DrawNavMeshSpecialLinks(CNavMesh * pNavMesh);

	void DrawNavMesh( CNavMesh * pNavMesh, CNavMeshVertexBuffers & VB, bool bTessellationNavMesh);

	void DrawNavMeshSinglePoly(TNavMeshPoly * pPoly);
	void DrawNavMeshSinglePoly(CNavMesh * pNavMesh, const u32 iPolyIndex, const float fRaiseZ=0.0f, const DWORD col=D3DCOLOR_RGBA(40,255,40,255), bool bBothSides=false);
	void DrawPolyPrecalcCentroids(CNavMesh * pNavMesh);
	void DrawPolyAddresses(CNavMesh * pNavMesh);

	void RenderSpaceBeyondPolygonEdges();
	void RenderSpaceBeyondPolygonEdges(CNavMesh * pNavMesh);

	static TNavMeshPoly * GetPolyUnderPosition(const Vector3 & vPos, const bool bIncludeTessellationNavMesh);

	u32 m_iDrawNavMeshSinglePolyIndex;

	CNavMeshVertexBuffers m_NavMeshVertexBuffers[MAX_NUM_NAVMESH_VBS];

	CNavMesh * m_DynamicNavMesh;
	CNavMeshVertexBuffers m_DynamicNavMeshVertexBuffer;

	// The vertex buffers etc for all meshes in the pathserver
	/*
	LPDIRECT3DVERTEXBUFFER9 m_pNavMeshTrianglesVB[MAX_NUM_NAVMESH_VBS];
	LPDIRECT3DVERTEXBUFFER9 m_pNavMeshLinesVB[MAX_NUM_NAVMESH_VBS];
	LPDIRECT3DVERTEXBUFFER9 m_pNavMeshAdjLinesVB[MAX_NUM_NAVMESH_VBS];
	LPDIRECT3DVERTEXBUFFER9 m_pNavMeshPrecalcPathLinesVB[MAX_NUM_NAVMESH_VBS];
	int m_iNavMeshNumTriangles[MAX_NUM_NAVMESH_VBS];
	int m_iNavMeshNumLines[MAX_NUM_NAVMESH_VBS];
	int m_iNavMeshNumAdjLines[MAX_NUM_NAVMESH_VBS];
	int m_iNavMeshPrecalcPathNumLines[MAX_NUM_NAVMESH_VBS];
	*/

	Vector3 m_vNavMeshPathSearchStartPos;
	Vector3 m_vNavMeshPathSearchEndPos;
	static float ms_fCompletionRadius;
	static float ms_fActorRadius;
	static float m_fNavMeshPathReferenceDistance;
	static float ms_fMaxAngleNavigable;
	static bool ms_bFavourEnclosedAreas;

	static bool ms_bAvoidObjects;
	static bool ms_bReduceBBoxes;
	static bool ms_bIgnoreObjectsWithNoUprootLimit;

	int m_iNavMeshNumPathPoints;
	atArray<Vector3> m_vNavMeshPathPoints;
	atArray<TNavMeshWaypointFlag> m_iNavMeshPathWaypointFlags;
	atArray<Vector3> m_vNavMeshPolyCentroids;
	atArray<Vector3> m_vNavMeshHierarchicalPoints;

	atArray<CNavGen::TAuthoredAdjacencyHintPos> m_AuthoredAdjacencyPositions;

	Vector3 m_vNavMeshPointsInPolys[256];
	Vector3 m_vNavMeshPolyTriVerts[4096];
	int m_iNavMeshNumPointsInPolys;
	int m_iNavMeshNumTriPts;

	bool m_bPathServerInitialised;
	bool m_bPavements;
	TDynamicObject * m_pSelDynObj;

	static bool m_bWanderPath;
	static bool m_bFleePath;
	static bool m_bCoverPath;
	static bool m_bPreservePathHeights;
	static bool m_bNeverClimb;
	static bool m_bNeverDrop;
	static bool m_bNeverUseLadders;
	static bool m_bSingleStepPathFinding;
	static bool ms_bDetessellatePriorToPathSearch;
	static bool ms_iNumPathRequestsRepeats;
	static bool m_bSmallObject;
	static bool m_bExhaustiveTesting;
	static bool m_bAdjacenciesUnderStartMarker;
	static bool m_bPathParentPolys;
	static bool m_bDrawTessellationNavMesh;
	static bool m_bMarkStartingPolys;
	static bool ms_bDebugSurroundingPolysToo;
	static bool m_bDrawSpecialLinks;
	static bool m_bDrawLowClimbs;
	static bool m_bDrawHighClimbs;
	static bool m_bDrawDropDowns;
	static bool m_bDrawHierarchical;
	static bool m_bDrawPolyMinMaxUnderStartMarker;
	static bool m_bDrawMinMaxOfObjects;
	static bool m_bDrawObjectsGridCellMinMax;
	static bool m_bDrawPolyCentroids;
	static bool m_bDrawPolyAddresses;
	static bool m_bDrawPolyCosts;
	static bool m_bDrawPolyDistanceTravelled;
	static bool m_bDrawPointsInPolys;
	static bool m_bDrawPolyParents;
	static bool m_bRenderPathStats;
	static bool m_bDrawFreeSpaceBeyondPolyEdges;
	static bool m_bDrawFreeSpaceAroundVertices;

	static TNavMeshPoly * ms_pDebugPoly;
	static bool m_bDrawDebugPoly;
	static bool m_bDrawDebugPolyTraceback;
	static bool m_bDumpDebugPolyTraceback;

	static char ms_DebugPolyEditWidget[256];

	static void CreateGroundPlane();
	static void LoadCollisionTris();
	static void LoadExtrasInfo();
	static CNavMesh * LoadDynamicNavMesh(const char * pFilename);
	static void CreateOctreeCB();
	static void SampleAndTriangulate();
	static void OptimiseNavMesh();
	static void RelaxVertexPositions();

	static void GenerateUsingVoxelMap();

	static void CalcFreeSpaceBeyondPolyEdges();

	static void MoveVertsToSmoothPavementEdges();
	static void MoveVertsToSmoothJaggies();
	static void AntiAliasBoundaryEdges();
	static void AntiAliasPavementEdges();
	static void AntiAliasSteepSlopeEdges();

	static void AddTestResolutionArea();
	static void LoadResolutionAreas();
	static void LoadAuthoredAdjacencies();

	static void ClipPolyUnderStartMarker();
	static void ClipPolyUnderfoot();
	static void ClipPolyUnderPosition(const Vector3 & vPos);

	static void TessellatePolyUnderStartMarker();
	static void TessellatePolyUnderfoot();
	static void TessellatePolyUnderPosition(const Vector3 & vPos);

	static void ExpandObjects();
	static void ContractObjects();

	static void DetessellateNow();

	static void DebugNavMeshPolyUnderStartMarker();
	static void PrintStartMarkerPos();
	static void DebugNavMeshPolyUnderfoot();
	static void DebugNavMeshPolyUnderPosition(const Vector3 & vPos);
	static void DebugNavMeshPolyByAddress();
	static void DebugNavMeshPoly(TNavMeshPoly * pPoly, CNavMesh * pNavMesh, bool bDebugSurrounding=false);

	static void LOSTest();
	static void TestNavMeshLOS();


	static void TestAllAdjacencies();

	static void MoveNodesAwayFromWalls();

	static void ResetEdgeIterator();
	static void TestBoundaryEdgeIteratorFromSelectedTriangle();
	static void TestPavementEdgeIteratorFromSelectedTriangle();

	static void StitchMapSwapForEntireMap();
	static void StitchAcrossMapSwap(CNavMesh * pMainMapMesh, CNavMesh * pSwapMesh, eNavMeshEdge iSideOfMainMapMesh);

	Vector3 m_vExhaustiveTestOrigin;
	float m_fExhaustiveRadius;
	TPathHandle m_hExhaustiveTestingPathHandle;

	CNavEdgeIterator m_TestEdgeIterator;

	static void ToggleExhaustiveTesting();
	void RunExhaustiveTesting(void);
	EPathServerErrorCode GetPathResult(TPathHandle hPath);

	static void RepeatLastPathRequest();
	static void RunLastPathOneHundredTimes();

	void ProcessRequestResult(s32 iPathHandle);

	static void SaveLastPathAsProbFile();
	static void LoadPathProbFile();

	void RenderNavMeshPathPolysEtc(void);
	void DrawNavMeshEdgeGroups(CNavMesh * pNavMesh);
	void DrawHierarchicalNavData(CNavMesh * pNavMesh, CHierarchicalNavData * pHierNav);
	void DisplayPathStats();

	static CPathRequest * GetLastPath();

	enum eCursorMode
	{
		eCursorMovesSelectedObject,
		eCursorMovesStartPoint,
		eCursorMovesEndPoint,

		eNumCursorModes
	};

	eCursorMode m_CursorMode;

	void InitWidgets();

	char m_navMeshesFolder[512];
	char m_gameDataFolder[512];
};

extern CNavGenApp * g_App;

//*****************************************************8
// Some structures for D3D, etc
//*****************************************************8
struct LitVertex
{
    FLOAT    x, y, z;
    D3DCOLOR diffuse;
};

const DWORD LitVertexFVF = (D3DFVF_XYZ | D3DFVF_DIFFUSE);


#endif
