#include "DummyPathServerStore.h"

//#include "pathserver/PathServer.h"
//#include "scene/world/WorldLimits.h"	// TODO: find out the new way to get min/max possible world extents

NAVMESH_OPTIMISATIONS()



//****************************************************************************************

aiNavMeshStore::aiNavMeshStore(
   const char* pModuleName, 
   int moduleTypeIndex, 
   int size,
   bool requiresTempMemory,
   bool canDefragment,
   int rscVersion)
   : aiMeshStore<CNavMesh>(pModuleName, moduleTypeIndex, 10000, requiresTempMemory, canDefragment, rscVersion)
{
	aiMeshStore<CNavMesh>::m_iMeshIndexNone = NAVMESH_NAVMESH_INDEX_NONE;
}
aiNavMeshStore::~aiNavMeshStore()
{

}
void aiNavMeshStore::Init()
{
	for(int i=0; i<m_iMaxMeshIndex; i++)
	{
		m_pStore[i] = NULL;
	}
}

#if __HIERARCHICAL_NODES_ENABLED

aiNavNodesStore::aiNavNodesStore(
 const char* pModuleName, 
 const char* pFileExt, 
 int size,
 bool requiresTempMemory,
 bool canDefragment,
 int rscVersion)
 : aiMeshStore<CHierarchicalNavData>(pModuleName, pFileExt, 10000, requiresTempMemory, canDefragment, rscVersion)
{
	aiMeshStore<CHierarchicalNavData>::m_iMeshIndexNone = NAVMESH_NODE_INDEX_NONE;
}
aiNavNodesStore::~aiNavNodesStore()
{

}

#endif	// __HIERARCHICAL_NODES_ENABLED


