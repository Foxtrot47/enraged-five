#ifndef AUDGEN_H
#define AUDGEN_H

#include "fwaudio/audmesh.h"

//General settings for rasterization and mesh generation
//The units of parameterws are specified as following
//(vx) voxels, (wu) world units
struct agConfig
{
	int width, length;	//x-axis and z-axis dimensions of the hightfield (vx)
	int tileSize;		//x-axis and z-axis dimensions of a tile (vx) ???? - what are tiles used for?
	int borderSize;		//Non-navigable border around the heightfield (vx) ???? - we're probably going to be stitching things together like the navmesh not doing everything on one big lump
	float cellSize;		//x and z dimensions of the grid cells (wu)
	float cellHeight;	//y (height) dimension of the grid cells (wu)
	float boundMin[3], boundMax[3]; //Grid bounds (wu)
	int walkableHeight;	//Minimum heights where the agent can still walk NB: currently not implemented but might have an audio analogue perhaps? (vx)
	int minRegionSize;	//Minimum region size. Smaller regions will be deleted (vx)
	int mergeRegionSize;	//Max number of vertices per polygon (vx)
	int maxEdgeLen;		// Maximum contour edge length (vx)
	float maxSimplificationError; //Maximum distance error from contour to cells (vx)
	int maxVertsPerPoly;	//max number of vertices per polygon
	float detailSampleDist;	//Detail mesh sample spacing
	float detailSampleMaxError; //Detail mesh simplification max sample error.
	float walkableSlopeAngle;		// Maximum walkble slope angle in degrees.
	int walkableClimb;				// Maximum height between grid cells the agent can climb (vx)
	int walkableRadius;				// Radius of the agent in cells (vx)
	//More stuff to go in here for the post-raster processes
};

//Heightfield span; verically contiguous voxells above each grid cell are merged into continuous regions called spans
struct agSpan
{
	u32 minY : 15;	//bottom of the span
	u32 maxY : 15;	//top of the span
	u32 flags : 2;	//span flags
	agSpan* next;	//next span for this grid cell
};

//Memory pools used for quick span allocation (see CAudioGen::AllocSpan() in AudioGen_Rasterize.cpp)
static const int AG_SPANS_PER_POOL = 2048;
struct agSpanPool
{
	agSpanPool* next;	//Pointer to next pool
	agSpan items[1];	//Array of spans (size AG_SPANS_PER_POOL)	
};

// Simple dynamic array ints.
class agIntArray
{
	int* m_data;
	int m_size, m_cap;
public:
	inline agIntArray() : m_data(0), m_size(0), m_cap(0) {}
	inline agIntArray(int n) : m_data(0), m_size(0), m_cap(n) { m_data = rage_new int[n]; }
	agIntArray(const agIntArray&);
	agIntArray& operator=(const agIntArray&);
	inline ~agIntArray() { delete [] m_data; }
	void resize(int n);
	inline void push(int item) { resize(m_size+1); m_data[m_size-1] = item; }
	inline int pop() { if (m_size > 0) m_size--; return m_data[m_size]; }
	inline const int& operator[](int i) const { return m_data[i]; }
	inline int& operator[](int i) { return m_data[i]; }
	inline int size() const { return m_size; }
};

struct agRegion
{
	inline agRegion() : count(0), id(0), remap(false) {}
	
	int count;
	unsigned short id;
	bool remap;
	agIntArray connections;
	agIntArray floors;
};

struct agCompactRegion
{
	inline agCompactRegion() : /*count(0),*/ id(0) {}

//	agIntArray connections;
	agIntArray audConnections;
//	int count;
	unsigned short id;
};

//Stores data about regions on the polymesh
struct agPolymeshRegion
{
	agPolymeshRegion() : audConnections(0), numConnections(0) {}
	~agPolymeshRegion()
	{
		delete [] audConnections;
	}
	//TODO: change this into an index into a single unified array of audConnections on the pmesh....there must be some way to compress this more, there's so much duplication of data
	unsigned short *audConnections; //array of region ids that this region has an audibility connection to; possibly a few spare bits for flags here...edge associated with connection perhaps, tho that might be cheap at runtime too
	unsigned short numConnections; //number of audio connections the region has; definitely room for some flags here, could comfortably drop to 8bit, maybe more 
};

//Dynamic span heightfield
struct agHeightField
{
	inline agHeightField() : width(0), length(0), spans(0), pools(0), freelist(0) {}
	inline ~agHeightField()
	{
		//Delete the span array
		delete [] spans;
		//delete span pools
		while(pools)
		{
			agSpanPool* next = pools->next;
			delete [] reinterpret_cast<unsigned char*>(pools);
			pools = next;
		}
	}
	int width, length;	//x-axis and z-axis dimensions of the heightfield
	float boundMin[3], boundMax[3];	//bounding box of the heightfield
	float cellSize;		//x and z dimentions of the grid cells
	float cellHeight;	//y (height) dimension of the grid cells
	agSpan** spans;		//heightfield of spans (width*length)
	agSpanPool* pools;	//linked list of span pools
	agSpan* freelist;	//Pointer to the next free span
};

struct agCompactCell
{
	u32 index : 24;		//index to the first span in the column
	u32 count : 8;		//number of spans in the column
};

//The number of the boundary corresponds to the direction e.g 'dir' in agGetDirOffsetX(int dir)
enum 
{
	AG_STITCH_0 = 1,
	AG_STITCH_1 = 2,
	AG_STITCH_2 = 4,
	AG_STITCH_3 = 8,
	AG_STITCH = AG_STITCH_0 | AG_STITCH_1 | AG_STITCH_2 | AG_STITCH_3,
};		

struct agCompactSpan
{
	u16 y;			//Bottom co-ordinate of the span
	u16 reg;		//region ID
	u16 dist;		//Distance to border
	u16 con;		//connections to neighbour cells
	u8 height;		//height of the span
	u8 flags;		
};

//Compact static heightfield
struct agCompactHeightField
{
	inline agCompactHeightField() : /*maxDistance(0), maxRegions(0),*/ cells(0), spans(0), regions(0) {}
	inline ~agCompactHeightField() 
	{
		delete [] cells; delete [] spans; delete [] regions;
	}
	int width, length;	//x-axis and z-axis dimensions of the heightfield
	int spanCount;		//Number of spans in the heightfield
	int walkableHeight;	//Thresholds for the height difference between spans that connect
	int walkableClimb;
	u16 totalConnections;
	u16 maxRegions;
	int maxDistance;
	float boundMin[3], boundMax[3]; //Bounding box of the heightfield
	float cellSize, cellHeight; //cell size and height
	agCompactCell* cells;	//pointer to width*height cells
	agCompactSpan* spans;	//pointer to spans
	agCompactRegion *regions; //pointer to regions
};

struct agContour
{
	inline agContour() : verts(0), nverts(0), rverts(0), nrverts(0) {}
	inline ~agContour() { delete [] verts; delete [] rverts; }
	int *verts; //Vertex coordinates, each vertex contains 4 components
	int nverts; //number of vertices.
	int *rverts; //number of raw vertices
	int nrverts; //number of raw vertices
	u16 reg; //region id of the contour
};

struct agContourSet
{
	inline agContourSet() : conts(0), nconts(0) {}
	inline ~agContourSet() {delete [] conts; }
	agContour *conts;	//Pointer to all the contours
	int nconts;		//number of contours
	float bmin[3], bmax[3];	//bounding box of the heightfield
	float cs, ch;	//Cell height and size
};

struct agCompact3dCell
{
	u32 index; //index to the first voxel in the column
	u16 count; //number of voxels in this column
};

//Idexes for voxel faces
enum
{
	AG_VOX_TOP = 0,
	AG_VOX_BOTTOM,
	AG_VOX_LEFT,
	AG_VOX_RIGHT,
	AG_VOX_FRONT,
	AG_VOX_BACK,
	AG_NUM_FACES
};

//Voxel face flags
enum
{
	AG_FACE_ACTIVE = 1,  //If this is set the face is a candidate for region watershedding 
	AG_FACE_BOUNDARY = 2, //The voxel lies within a boundary region
	AG_FACE_FLAG3 = 4, //Unused flag
	AG_FACE_FLAG4 = 8 //Unsued flag
};

//If the the heightfield/volumefield region ID has the following bit set, 
static const unsigned short AG_BORDER_REG = 0x8000;

static const u16 AG_NULL_CONNECTION = 0xffff; //This is the value of 'con' on a voxel face that indicates there is no neighbour connection
						//in a given direction. It should be set to the maximum value of con.
					

// If contour region ID has the following bit set, the vertex will be later
// removed in order to match the segments and vertices at tile boundaries.
static const int AG_BORDER_VERTEX = 0x10000;


struct agVoxelFace
{
	u16 con[4]; //Connections to neighbouring faces: 
		      //in practice this operates as 4 10 bit indexes which are 
		      //operated on using bit shifts via the compact helper functions 
	u16 reg; //region id
	u16 dist; //distance to the border
	u8 flags; //Flags
};
	

struct agCompactVoxel
{
	agVoxelFace faces[6]; //Information for each face of the voxel
	u16 y; //Height coordinate of the voxel
};


//Compact static volumefield
struct agCompactVolumefield
{
	inline agCompactVolumefield() : maxRegions(0), cells(0), voxels(0)
	{
	}
	inline ~agCompactVolumefield()
	{
		delete [] cells;
		delete [] voxels;
	}
	int width, length; //x-axis and z-axis of the volumefield
	int maxHeight;	//the highest point in the volumefield
	int voxelCount; //number of voxels in the volumefield
	float bmin[3], bmax[3]; //bounding box of the volumefield
	float cellSize, cellHeight; //Cell size and height
	agCompact3dCell *cells; //Pointer to the width * height cells
	agCompactVoxel *voxels; //Pointer to the voxels
	u16 maxDistance[AG_NUM_FACES]; //Maximum distance stored in the volumefield
	u16 maxRegions; //Maximum Region id stored in the volumefield
};

/////////////////////////////////////////////////
//agPolyMesh is now located in audioengine/audmesh.h 
///////////////////////////////////////////////////

// Detail mesh generated from a rcPolyMesh.
// Each submesh represents a polygon in the polymesh and they are stored in
// excatly same order. Each submesh is described as 4 values:
// base vertex, vertex count, base triangle, triangle count. That is,
//   const unsigned char* t = &dtl.tris[(tbase+i)*3]; and
//   const float* v = &dtl.verts[(vbase+t[j])*3];
// If the input polygon has 'n' vertices, those vertices are first in the
// submesh vertex list. This allows to compres the mesh by not storing the
// first vertices and using the polymesh vertices instead.

struct agPolyMeshDetail
{
	inline agPolyMeshDetail() :
		meshes(0), verts(0), tris(0),
		nmeshes(0), nverts(0), ntris(0) {}
	inline ~agPolyMeshDetail()
	{
		delete [] meshes; delete [] verts; delete [] tris;
	}
	
	unsigned short* meshes;	// Pointer to all mesh data.
	float* verts;			// Pointer to all vertex data.
	unsigned char* tris;	// Pointer to all triangle data.
	int nmeshes;			// Number of meshes.
	int nverts;				// Number of total vertices.
	int ntris;				// Number of triangles.
};


//This is used to parse the params file that supplies the input values for the command line
//interface
struct AudGenInputs
{

	AudGenInputs() :
		cellSize(0.39f),
		cellHeight(0.39f),
		agentHeight(0.1f),
		agentRadius(0.1f),
		maxClimb(0.9f),
		maxSlope(45.f),
		borderSize(1),
		minRegionSize(50),
		mergeRegionSize(20),
		maxEdgeLen(12),
		maxEdgeError(1.3f),
		vertsPerPoly(6),
		sampleDistance(6),
		maxSampleError(1)
	{}

	float	cellSize;
	float	cellHeight;
	float	agentHeight;
	float	agentRadius;
	float	maxClimb;
	float	maxSlope;
	float	borderSize;
	float	minRegionSize;
	float	mergeRegionSize;
	float	maxEdgeLen;
	float	maxEdgeError;
	float	vertsPerPoly;
	float	sampleDistance;
	float	maxSampleError;
};



bool agBuildCompactVolumeField(agHeightField &field, agCompactVolumefield & compact);
bool agBuildDistanceFields(agCompactVolumefield &compact);
bool agSavePolyMeshBinary(const char *pFileName, const char * pMeshName, const agPolyMesh &mesh, const agCompactHeightField * compact = NULL);
bool agLoadSurfaceBinary(char * pFileName, agPolyMesh & mesh, agCompactHeightField * compact = NULL);
//This looks in the direction specified by dir, and populates regs with all adjacent, audible regions (e.g. could be several floors)
void agGetNextAudibleRegionsDir(const agCompactHeightField &compact, const agCompactSpan &span, int x, int y, int regs[16], int dir);
void agStitchMeshes(agPolyMesh &mesh, const agCompactHeightField &compact, const agCompactHeightField * const ncompacts[4]);

template<class T> inline T agSqr(T a) { return a*a;}

// Common vector helper functions.
inline void vcross(float* dest, const float* v1, const float* v2)
{
	dest[0] = v1[1]*v2[2] - v1[2]*v2[1];
	dest[1] = v1[2]*v2[0] - v1[0]*v2[2];
	dest[2] = v1[0]*v2[1] - v1[1]*v2[0]; 
}

inline float vdot(const float* v1, const float* v2)
{
	return v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2];
}

inline void vmad(float* dest, const float* v1, const float* v2, const float s)
{
	dest[0] = v1[0]+v2[0]*s;
	dest[1] = v1[1]+v2[1]*s;
	dest[2] = v1[2]+v2[2]*s;
}

inline void vadd(float* dest, const float* v1, const float* v2)
{
	dest[0] = v1[0]+v2[0];
	dest[1] = v1[1]+v2[1];
	dest[2] = v1[2]+v2[2];
}

inline void vsub(float* dest, const float* v1, const float* v2)
{
	dest[0] = v1[0]-v2[0];
	dest[1] = v1[1]-v2[1];
	dest[2] = v1[2]-v2[2];
}

inline void vmin(float* mn, const float* v)
{
	mn[0] = Min(mn[0], v[0]);
	mn[1] = Min(mn[1], v[1]);
	mn[2] = Min(mn[2], v[2]);
}

inline void vmax(float* mx, const float* v)
{
	mx[0] = Max(mx[0], v[0]);
	mx[1] = Max(mx[1], v[1]);
	mx[2] = Max(mx[2], v[2]);
}

inline void vcopy(float* dest, const float* v)
{
	dest[0] = v[0];
	dest[1] = v[1];
	dest[2] = v[2];
}

inline void vcopy(u16 *dest, const u16* v)
{
	dest[0] = v[0];
	dest[1] = v[1];
	dest[2] = v[2];
}

inline void vcopy(u32 *dest, const u32* v)
{
	dest[0] = v[0];
	dest[1] = v[1];
	dest[2] = v[2];
}

inline void vcopy(u8 *dest, const u8* v)
{
	dest[0] = v[0];
	dest[1] = v[1];
	dest[2] = v[2];
}
inline float vdist(const float* v1, const float* v2)
{
	float dx = v2[0] - v1[0];
	float dy = v2[1] - v1[1];
	float dz = v2[2] - v1[2];
	return sqrtf(dx*dx + dy*dy + dz*dz);
}

inline float vdistSqr(const float* v1, const float* v2)
{
	float dx = v2[0] - v1[0];
	float dy = v2[1] - v1[1];
	float dz = v2[2] - v1[2];
	return dx*dx + dy*dy + dz*dz;
}

inline void vnormalize(float* v)
{
	float d = 1.0f / sqrtf(Sqrtf(v[0]) + Sqrtf(v[1]) + Sqrtf(v[2]));
	v[0] *= d;
	v[1] *= d;
	v[2] *= d;
}

inline bool vequal(const float* p0, const float* p1)
{
	static const float thr = Sqrtf(1.0f/16384.0f);
	const float d = vdistSqr(p0, p1);
	return d < thr;
}


template<class T> inline void agSwap(T& a, T& b) { T t = a; a = b; b = t; }

inline int agGetCon(const agCompactSpan &span, int dir)
{
	return (span.con >> (dir*4))&0xf;
}

//get direction offsets from top/bottom face
inline int agGetTopDirOffsetX(int dir)
{
	int offset[4] = {-1, 0, 1, 0};
	return offset[dir&0x3];
}

inline int agGetTopDirOffsetY(int dir)
{
	int offset[4] = {0, 1, 0, -1};
	return offset[dir&0x3];
}

inline int agGetTopDirFace(int dir)
{
	int face[4] = {AG_VOX_LEFT, AG_VOX_FRONT, AG_VOX_RIGHT, AG_VOX_BACK};
	return face[dir&0x3];
}

inline int agGetSideDirOffsetY(int dir)
{
	int offset[4] = {1, 0, -1, 0};
	return offset[dir&0x03];
}

inline int agGetSideOffsetHeight(int dir)
{
	int offset[4] = {0, 1, 0, -1};
	return offset[dir&0x03];
}

//get direction offsets for front/back face
inline int agGetFrontDirOffsetX(int dir)
{
	int offset[4] = {-1, 0, 1, 0};
	return offset[dir&0x03];
}

inline u16 agGetConnectionData(const agCompactVoxel &vox, int face, int dir)
{
	return vox.faces[face].con[dir];
}

inline int agGetDirOffsetX(int dir)
{
	const int offset[4] = { -1, 0, 1, 0, };
	return offset[dir&0x03];
}

inline int agGetDirOffsetY(int dir)
{
	const int offset[4] = { 0, 1, 0, -1 };
	return offset[dir&0x03];
}

inline int agGetDirOffsetX(int face, int dir)
{
	switch(face)
	{
	case AG_VOX_TOP: 
	case AG_VOX_BOTTOM:
		return agGetTopDirOffsetX(dir);
	case AG_VOX_FRONT:
	case AG_VOX_BACK:
		return agGetFrontDirOffsetX(dir);
	default: return 0;
	}
}

inline int agGetDirOffsetY(int face, int dir)
{
	switch(face)
	{
	case AG_VOX_TOP:
	case AG_VOX_BOTTOM:
		return agGetTopDirOffsetY(dir);
	case AG_VOX_LEFT:
	case AG_VOX_RIGHT:
		return agGetSideDirOffsetY(dir);
	default: return 0;
	}
}

inline int agGetFrontOffsetHeight(int dir)
{
	int offset[4] = {0, 1, 0,-1};
	return offset[dir&0x03];
}

inline int agGetDirOffsetHeight(int face, int dir)
{
	switch(face)
	{
	case AG_VOX_LEFT:
	case AG_VOX_RIGHT:
		return agGetSideOffsetHeight(dir);
	case AG_VOX_FRONT:
	case AG_VOX_BACK:
		return agGetFrontOffsetHeight(dir);
	default: return 0;
	}
}

inline int agGetFaceConnection(const agCompactVolumefield &compact, const agCompactVoxel &vox, int face, int dir, int x, int y)
{
	if(vox.faces[face].con[dir] == AG_NULL_CONNECTION)
	{
		return AG_NULL_CONNECTION;
	}

	int w = compact.width;
	int l = compact.length;

	switch (face)
	{
		case AG_VOX_TOP:
		case AG_VOX_BOTTOM:
		{
			int nx = x + agGetTopDirOffsetX(dir);
			int ny = y + agGetTopDirOffsetY(dir);
			if(ny<0 || nx < 0 || ny>= l || nx >= w)
			{
				return AG_NULL_CONNECTION;
			} 
			return compact.cells[nx + ny*w].index + agGetConnectionData(vox, face, dir);
		}
			break;
		case AG_VOX_LEFT:
		case AG_VOX_RIGHT:
		{
			int ny = y + agGetSideDirOffsetY(dir);
			int height = vox.y + agGetSideOffsetHeight(dir);
			if(ny<0 || x<0 || ny>= l || x>= w)
			{
				return AG_NULL_CONNECTION;
			}
			agCompact3dCell &cell = compact.cells[agGetConnectionData(vox, face, dir) + (ny*w)];
			for(int i = cell.index, endi = (int)(cell.index+cell.count); i<endi; ++i)
			{
				agCompactVoxel &nvox = compact.voxels[i];
				if(nvox.y == height)
				{
					return i;
				}
			}	
		}
			break;
		case AG_VOX_FRONT:
		case AG_VOX_BACK:
		{
			if(face == AG_VOX_BACK)
			{
				return AG_NULL_CONNECTION;
			}
//			int nx = x +agGetFrontDirOffsetX(dir);
			//int height = vox.y +agGetSideHeightOffset(dir);
//			if(nx<0 || y < 0 || nx >= w || y >= l)
//			{
//				return AG_NULL_CONNECTION;
//			}
			return agGetConnectionData(vox, face, dir);
		}
			break;
		default:
			Assertf(0, "Unknown face id passed into agGetFaceConnection");
			return AG_NULL_CONNECTION;
			break;
	}
	return AG_NULL_CONNECTION;
}






// Calculated bounding box of array of vertices.
// Params:
//	verts - (in) array of vertices
//	nv - (in) vertex count
//	bmin, bmax - (out) bounding box
void agCalcBounds(const float* verts, int nv, float* bmin, float* bmax);

// Calculates grid size based on bounding box and grid cell size.
// Params:
//	bmin, bmax - (in) bounding box
//	cs - (in) grid cell size
//	w - (out) grid width
//	h - (out) grid height
void agCalcGridSize(const float* bmin, const float* bmax, float cs, int* w, int* h);

// Creates and initializes new heightfield.
// Params:
//	hf - (in/out) heightfield to initialize.
//	width - (in) width of the heightfield.
//	height - (in) height of the heightfield.
//	bmin, bmax - (in) bounding box of the heightfield
//	cs - (in) grid cell size
//	ch - (in) grid cell height
bool agCreateHeightfield(agHeightField& hf, int width, int height,
						 const float* bmin, const float* bmax,
						 float cs, float ch);

// Sets the WALKABLE flag for every triangle whose slope is below
// the maximun walkable slope angle.
// Params:
//	walkableSlopeAngle - (in) maximun slope angle in degrees.
//	verts - (in) array of vertices
//	nv - (in) vertex count
//	tris - (in) array of triangle vertex indices
//	nt - (in) triangle count
//	flags - (out) array of triangle flags
//void agMarkWalkableTriangles(const float walkableSlopeAngle,
//							 const float* verts, int nv,
//							 const int* tris, int nt,
//							 unsigned char* flags); 

// Rasterizes a triangle into heightfield spans.
// Params:
//	v0,v1,v2 - (in) the vertices of the triangle.
//	flags - (in) triangle flags (uses WALKABLE)
//	solid - (in) heighfield where the triangle is rasterized
void agRasterizeTriangle(const float* v0, const float* v1, const float* v2,
						 unsigned char flags, agHeightField& solid);

// Rasterizes the triangles into heightfield spans.
// Params:
//	verts - (in) array of vertices
//	nv - (in) vertex count
//	tris - (in) array of triangle vertex indices
//	norms - (in) array of triangle normals
//	flags - (in) array of triangle flags (uses WALKABLE)
//	nt - (in) triangle count
//	solid - (in) heighfield where the triangles are rasterized
void agRasterizeTriangles(const float* verts, int nv,
						  const int* tris, const unsigned char* flags, int nt,
						  agHeightField& solid);

// Removes WALKABLE flag from all spans that are at ledges. This filtering
// removes possible overestimation of the conservative voxelization so that
// the resulting mesh will not have regions hanging in air over ledges.
// Params:
//	walkableHeight - (in) minimum height where the agent can still walk
//	walkableClimb - (in) maximum height between grid cells the agent can climb
//	solid - (in/out) heightfield describing the solid space
//void agFilterLedgeSpans(const int walkableHeight,
//						const int walkableClimb,
//						rcHeightfield& solid);

// Removes WALKABLE flag from all spans which have smaller than
// 'walkableHeight' clearane above them.
// Params:
//	walkableHeight - (in) minimum height where the agent can still walk
//	solid - (in/out) heightfield describing the solid space
//void agFilterWalkableLowHeightSpans(int walkableHeight,
//									agHeightfield& solid);

// Marks spans which are reachable from any of the topmost spans.
// Params:
//	walkableHeight - (in) minimum height where the agent can still walk
//	walkableClimb - (in) maximum height between grid cells the agent can climb
//	solid - (in/out) heightfield describing the solid space
// Returns false if operation ran out of memory.
//bool agMarkReachableSpans(const int walkableHeight,
//						  const int walkableClimb,
//						  agHeightfield& solid);

// Builds compact representation of the heightfield.
// Params:
//	walkableHeight - (in) minimum height where the agent can still walk
//	walkableClimb - (in) maximum height between grid cells the agent can climb
//	hf - (in) heightfield to be compacted
//	chf - (out) compact heightfield representing the open space.
// Returns false if operation ran out of memory.
bool agBuildCompactHeightField(const int walkableHeight, const int walkableClimb,
							   unsigned char flags,
							   agHeightField& hf,
							   agCompactHeightField& chf);

// Builds distance field and stores it into the combat heightfield.
// Params:
//	chf - (in/out) compact heightfield representing the open space.
// Returns false if operation ran out of memory.
bool agBuildDistanceField(agCompactHeightField& chf);

// Divides the walkable heighfied into simple regions.
// Each region has only one contour and no overlaps.
// The regions are stored in the compact heightfield 'reg' field.
// The regions will be shrinked by the radius of the agent.
// The process sometimes creates small regions. The parameter
// 'minRegionSize' specifies the smallest allowed regions size.
// If the area of a regions is smaller than allowed, the regions is
// removed or merged to neighbour region. 
// Params:
//	chf - (in/out) compact heightfield representing the open space.
//	walkableRadius - (in) the radius of the agent.
//	minRegionSize - (in) the smallest allowed regions size.
//	maxMergeRegionSize - (in) the largest allowed regions size which can be merged.
//// Returns false if operation ran out of memory.
bool agBuildRegions(agCompactHeightField& chf,
					int walkableRadius, int borderSize,
					int minRegionSize, int mergeRegionSize);

// Builds simplified contours from the regions outlines.
// Params:
//	chf - (in) compact heightfield which has regions set.
//	maxError - (in) maximum allowed distance between simplified countour and cells.
//	maxEdgeLen - (in) maximum allowed contour edge length in cells.
//	cset - (out) Resulting contour set.
// Returns false if operation ran out of memory.
bool agBuildContours(agCompactHeightField& chf,
					 const float maxError, const int maxEdgeLen,
					 agContourSet& cset);

// Builds connected convex polygon mesh from contour polygons.
// Params:
//	cset - (in) contour set.
//	nvp - (in) maximum number of vertices per polygon.
//	mesh - (out) poly mesh.
// Returns false if operation ran out of memory.
bool agBuildPolyMesh(agContourSet& cset, agCompactHeightField &compact, int nvp, agPolyMesh& mesh);
////
bool agMergePolyMeshes(agPolyMesh** meshes, const int nmeshes, agPolyMesh& mesh);

// Builds detail triangle mesh for each polygon in the poly mesh.
// Params:
//	mesh - (in) poly mesh to detail.
//	chf - (in) compacy height field, used to query height for new vertices.
//  sampleDist - (in) spacing between height samples used to generate more detail into mesh.
//  sampleMaxError - (in) maximum allowed distance between simplified detail mesh and height sample.
//	pmdtl - (out) detail mesh.
// Returns false if operation ran out of memory.
bool agBuildPolyMeshDetail(const agPolyMesh& mesh, const agCompactHeightField& chf,
						   const float sampleDist, const float sampleMaxError,
						   agPolyMeshDetail& dmesh);

bool agMergePolyMeshDetails(agPolyMeshDetail** meshes, const int nmeshes, agPolyMeshDetail& mesh);
#endif
