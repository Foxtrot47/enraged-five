#ifndef AUDIOGEN_LOG_H
#define AUDIOGEN_LOG_H

enum agLogCategory
{
	AG_LOG_PROGRESS = 1,
	AG_LOG_WARNING,
	AG_LOG_ERROR,
};

class agLog
{
public:
	agLog();
	~agLog();
	
	void log(agLogCategory category, const char* format, ...);
	inline void clear() { m_messageCount = 0; m_textPoolSize = 0; }
	inline int getMessageCount() const { return m_messageCount; }
	inline char getMessageType(int i) const { return *m_messages[i]; }
	inline const char* getMessageText(int i) const { return m_messages[i]+1; }

private:
	static const int MAX_MESSAGES = 1000;
	const char* m_messages[MAX_MESSAGES];
	int m_messageCount;
	static const int TEXT_POOL_SIZE = 8000;
	char m_textPool[TEXT_POOL_SIZE];
	int m_textPoolSize;
};

struct agBuildTimes
{
	int rasterizeTriangles;
	int buildCompact;
	int buildContours;
	int buildContoursTrace;
	int buildContoursSimplify;
	int filterBorder;
	int filterWalkable;
	int filterMarkReachable;
	int buildPolymesh;
	int buildDistanceField;
	int buildDistanceFieldDist;
	int buildDistanceFieldBlur;
	int buildRegions;
	int buildRegionsReg;
	int buildRegionsExp;
	int buildRegionsFlood;
	int buildRegionsFilter;
	int buildDetailMesh;
	int mergePolyMesh;
	int mergePolyMeshDetail;
};


void agSetLog(agLog* log);
agLog* agGetLog();

void agSetBuildTimes(agBuildTimes* btimes);
agBuildTimes* agGetBuildTimes();

#endif // AUDIOGEN_LOG_H
