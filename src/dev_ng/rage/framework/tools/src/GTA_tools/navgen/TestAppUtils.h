//**************************************************************************************
//	Filename : TestAppUtils
//	Description : Some functions for use with the NavMesh dev applications
//	Author : James Broad
//	Date : 14/5/06
//**************************************************************************************

#ifndef TESTAPPUTILS_H
#define TESTAPPUTILS_H

#include "grcore/vertexbuffer.h"
#include "grcore/vertexdecl.h"

#define CleanDelete(n)	if(n) { delete n; n = NULL; }
#define CleanVectorDelete(n)	if(n) { delete[] n; n = NULL; }

class CNavGen;
class CNavGenOctreeNodeBase;

class CTestAppUtils
{
public:
	static void DrawBox(const Vector3 & vMin, const Vector3 & vMax, const Color32 & col);
};

class CCollisionTrisRenderer
{
public:

	CCollisionTrisRenderer(void);
	~CCollisionTrisRenderer(void);

	bool Init(CNavGen & navMeshGenerator);
	void Shutdown();
	void Draw(bool bWireFrameAlso = true);

private:

	grcVertexDeclaration * m_pVertexDecl;
	grcVertexBuffer * m_pVertexBuffer;
	int m_iNumVertices;
};

class CNavGenOctreeRenderer
{
public:

	static bool Draw(CNavGen & navMeshGenerator);

private:

	static void DrawNodeRecursive(CNavGenOctreeNodeBase * pOctree);
};

#endif