#include "TestAppCommon.h"

#include <direct.h>
#include <d3d9.h>
#include "bank/bank.h"
#include "file/asset.h"
#include "grcore/effect.h"
#include "grcore/im.h"
#include "grcore/state.h"
#include "grcore/texture.h"
#include "grmodel/modelfactory.h"
#include "grmodel/shaderfx.h"
#include "grmodel/shaderpreset.h"
#include "input/input.h"
#include "input/pad.h"
#include "system/timemgr.h"

#include "TestAppUtils.h"

using namespace rage;

CTestAppBase * CTestAppBase::ms_pApp = NULL;
char CTestAppBase::m_WorkingDir[1024] = { 0 };
grcSetup * CTestAppBase::m_pGrcSetup = NULL;
grcViewport * CTestAppBase::m_pViewport = NULL;
CGtaDebugCam * CTestAppBase::m_pDebugCam = NULL;
CWorldGrid * CTestAppBase::m_pWorldGrid = NULL;
bool CTestAppBase::m_bDrawWorldGrid = true;
Color32 CTestAppBase::m_ClearCol(0,0,0,0);

bool CTestAppBase::m_bInitialised = false;
bool CTestAppBase::m_bWantsToQuit = false;

ioMapper CTestAppBase::m_MouseMapper;
ioValue CTestAppBase::m_MouseLeftButton;

CTestAppBase::CTestAppBase()
{
	getcwd(m_WorkingDir, 1024);
	ms_pApp = this;
}


CTestAppBase::~CTestAppBase()
{
	Shutdown();
}

bool
CTestAppBase::Init()
{
	m_pGrcSetup = new grcSetup();

	m_pGrcSetup->Init(GetAppAssetPath(), GetAppName());

	// the Rage shaders folder (and subfolders) must be in the working dir of the app
	char shadersPath[512];
	sprintf(shadersPath, "%s/shaders", GetAppAssetPath());
	grcEffect::SetDefaultPath(shadersPath);
	m_pGrcSetup->BeginGfx(true);

	rage::INPUT.Begin(true);

	m_pViewport = new grcViewport;

	m_pGrcSetup->CreateDefaultFactories();

//	grmShaderFactory::CreateStandardShaderFactory(true);
//	grmShaderFxTemplate::InitClass();
//	grmShaderFactory::GetInstance().PreloadShaders(shadersPath);

	// Init the shader database
//	s32 maxPresets = 1024;
//	grmShaderPreset::InitClass(maxPresets);
//	grmShaderPreset::Preload(shadersPath);

	//Vector3 vCameraPos(-25.0f, -25.0f, 10.0f);
	Vector3 vCameraPos(0.0f, 0.0f, 0.0f);
	Vector3 vCameraTarget(0.0f, 10.0f, 0.0f);

	m_pDebugCam = new CGtaDebugCam();
	m_pDebugCam->Init();
	m_pDebugCam->SetUpAxis(Vector3(0.0f,0.0f,1.0f));
	m_pDebugCam->SetPosition(vCameraPos);
	m_pDebugCam->LookAt(vCameraTarget);

	ASSET.SetPath(shadersPath);

	m_pWorldGrid = new CWorldGrid();
	Color32 axisCol(255,255,255,255);
	Color32 fadeCol(0,0,0,255);
	m_pWorldGrid->Init(10, 10.0f, axisCol, fadeCol);

	InitCommonWidgets();

	AppSpecificInit();

	m_MouseMapper.Map(IOMS_MOUSE_BUTTON, ioMouse::MOUSE_LEFT, m_MouseLeftButton);

	m_bInitialised = true;

	return true;
}




bool
CTestAppBase::Shutdown()
{
	if(!m_bInitialised)
		return true;

	AppSpecificShutdown();

	if(m_pGrcSetup)
		m_pGrcSetup->DestroyFactories();

	CleanDelete(m_pWorldGrid);
	CleanDelete(m_pDebugCam);
	CleanDelete(m_pViewport);

//	grmShaderPreset::ShutdownClass();
//	grmShaderFxTemplate::ShutdownClass();

	rage::INPUT.End();

	if(m_pGrcSetup)
	{
		m_pGrcSetup->EndGfx();
		m_pGrcSetup->Shutdown();
	}
	CleanDelete(m_pGrcSetup);

	m_bInitialised = false;

	return true;
}

void
Widget_QuitApp(void)
{
	if(CTestAppBase::ms_pApp)
		CTestAppBase::ms_pApp->SetWantsToQuit();
}

void
CTestAppBase::InitCommonWidgets(void)
{
	bkBank & bankCommon = BANKMGR.CreateBank("Common");

	bankCommon.AddToggle("Draw Grid", &m_bDrawWorldGrid);
	bankCommon.AddButton("Quit", datCallback(CFA(Widget_QuitApp)));

	//m_pGrcSetup->AddWidgets(bank);
}


bool
CTestAppBase::Run()
{
	Matrix34 xRot;
	xRot.Identity();
	xRot.MakeRotateX(PI*0.5f);

	m_ClearCol = Color32(255,0,0,255);

	while(!m_bWantsToQuit)
	{
		rage::INPUT.Update();
		TIME.Update();

		m_pDebugCam->Run();

		Matrix34 camMat = m_pDebugCam->GetMatrix();
		camMat.Dot3x3FromLeft(xRot);
		camMat.d = m_pDebugCam->GetPosition();

		m_pViewport->Perspective(70.0f, 0.0f, 1.0f, 500.0f);
		grcViewport::SetCurrent(m_pViewport);

		m_pGrcSetup->BeginUpdate();
		m_pGrcSetup->BeginDraw();

		if(m_bDrawWorldGrid)
		{
			m_pWorldGrid->Draw(m_pDebugCam->GetPosition(), camMat, m_pViewport);
		}
		m_pViewport->SetCameraMtx(camMat);
		m_pViewport->SetWorldMtx(M34_IDENTITY);

		if(!AppSpecificRunLoop())
			m_bWantsToQuit = true;

		// Try to recover from shaderfx rendering
		grcState::Default();	
		
		grcWorldIdentity();

		m_pGrcSetup->EndDraw();
		m_pGrcSetup->EndUpdate();

		m_pGrcSetup->SetClearColor(m_ClearCol);
	
	}

	return true;
}


void
CTestAppBase::DrawGrid()
{
	const float FLOOR_HEIGHT = 0.0f;

	grcViewport::SetCurrentWorldIdentity();

	int x;
	Color32 color(0.0f,0.5f,1.0f,0.3f);

	grcBindTexture(NULL);
	grcState::SetDepthWrite(false);

	Color32 colorAxes(0.3f, 0.8f, 1.0f, 0.8f);

	const Matrix34 & camMat = m_pDebugCam->GetMatrix();
	//Vector3 vOrigin(0,0,0);
	Vector3 vOrigin = camMat.d;
	int iX = (int)(vOrigin.x / 10.0f);
	int iY = (int)(vOrigin.y / 10.0f);
	vOrigin.x = ((float)iX)*10.0f;
	vOrigin.y = ((float)iY)*10.0f;

	// draw y lines:
	for(x=-50;x<50;x+=2) {
		Color32 c = (x==0) ? colorAxes : color;
		grcBegin(drawLineStrip,11);
		for (float y=-50.0f; y<=50.0f; y+=10.0f)
		{
			grcVertex((float)x+vOrigin.x,y+vOrigin.y,FLOOR_HEIGHT,0,1,0,c,0.0f,0.0f);
		}
		grcEnd();
	}

	// draw x lines:
	for(x=-50;x<50;x+=2) {
		Color32 c = (x==0) ? colorAxes : color;
		grcBegin(drawLineStrip,11);
		for (float y=-50.0f; y<=50.0f; y+=10.0f)
		{
			grcVertex(y+vOrigin.y,(float)x+vOrigin.x,FLOOR_HEIGHT,0,1,0,c,0.0f,0.0f);
		}
		grcEnd();
	}

	grcState::SetDepthWrite(true);
}









CGtaDebugCam::CGtaDebugCam(void)
{
	SetUpAxis(Vector3(0.0f,0.0f,1.0f));
	SetIdentity();
	m_CameraMatrix.d = Vector3(0.0f,0.0f,0.0f);
	m_vPosition = Vector3(0.0f,0.0f,0.0f);
	m_vVelocity = Vector3(0.0f,0.0f,0.0f);
	m_fHeading = 0.0f;
	m_fPitch = 0.0f;
}


CGtaDebugCam::~CGtaDebugCam(void)
{

}

void
CGtaDebugCam::Init(void)
{
	m_CameraMatrix.Identity();

	m_Mapper.Map(IOMS_PAD_AXIS, IOM_AXIS_LX, m_Heading);
	m_Mapper.Map(IOMS_PAD_AXIS, IOM_AXIS_LY, m_Pitch);

	m_Mapper.Map(IOMS_PAD_AXIS, IOM_AXIS_RX, m_MoveX);
	m_Mapper.Map(IOMS_PAD_AXIS, IOM_AXIS_RY, m_MoveY);

	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::RDOWN, m_Forwards);
	m_Mapper.Map(IOMS_PAD_DIGITALBUTTON, ioPad::RLEFT, m_Backwards);
}

void
CGtaDebugCam::Shutdown(void)
{

}

void
CGtaDebugCam::SetIdentity(void)
{
	float fUpDot = DotProduct(m_vUpAxis, Vector3(0.0f,0.0f,1.0f));
	if(fUpDot >= 0.707f)
	{
		m_CameraMatrix.Identity();
		m_CameraMatrix.a = Vector3(1.0f,0.0f,0.0f);
		m_CameraMatrix.b = Vector3(0.0f,0.0f,1.0f);
		m_CameraMatrix.c = Vector3(0.0f,1.0f,0.0f);
	}
	else
	{
		m_CameraMatrix.Identity();
	}
}

void
CGtaDebugCam::SetViewVector(const Vector3 & vForwardsIn)
{
	if(vForwardsIn.Mag() == 0.0f)
		return;

	Vector3 vForwards = vForwardsIn;
	vForwards.Normalize();

	Vector3 vRight = CrossProduct(vForwards, m_vUpAxis);
	Vector3 vUp = CrossProduct(vRight, vForwards);

	m_CameraMatrix.a = vRight;
	m_CameraMatrix.b = vForwards;
	m_CameraMatrix.c = vUp;
}

void
CGtaDebugCam::LookAt(const Vector3 & vTarget)
{

}

void
CGtaDebugCam::Run(void)
{
	m_Mapper.Update();

	float fSecs = TIME.GetSeconds();
	if(fSecs == 0.0f)
		return;

	m_vVelocity *= 0.9f;

	Matrix34 newCamMat;
	newCamMat.Identity();

	const float fDeadZone = 0.3f;
	uint8 iVal = m_Heading.GetValue();
	float fHeadingChange = m_Heading.GetNorm(fDeadZone);
	float fPitchChange = m_Pitch.GetNorm(fDeadZone);
	float fMoveX = m_MoveX.GetNorm(fDeadZone);
	float fMoveY = m_MoveY.GetNorm(fDeadZone);
	float fForwards = m_Forwards.IsDown() ? 1.0f : 0.0f;
	float fBackwards = m_Backwards.IsDown() ? 1.0f : 0.0f;

	m_fHeading += fHeadingChange * fSecs * 2.0f;
	m_fPitch += fPitchChange * fSecs * 2.0f;

	static const float fTwoPI = PI*2.0f;

	while(m_fHeading < 0.0f) m_fHeading += fTwoPI;
	while(m_fHeading >= fTwoPI) m_fHeading -= fTwoPI;
	while(m_fPitch < 0.0f) m_fPitch += fTwoPI;
	while(m_fPitch >= fTwoPI) m_fPitch -= fTwoPI;

	newCamMat.Identity();
	newCamMat.RotateFullUnitAxis(m_vUpAxis, -m_fHeading);
	newCamMat.RotateFullUnitAxis(newCamMat.a, m_fPitch);

	m_vPosition.x += fMoveX;
	m_vPosition.y += fMoveY;

	m_vVelocity -= newCamMat.b * fForwards;
	m_vVelocity += newCamMat.b * fBackwards;

	m_vPosition += m_vVelocity;

	m_CameraMatrix = newCamMat;
}



CWorldGrid::CWorldGrid(void) :
	m_pVertexBuffer(NULL),
	m_pVertexDecl(NULL),
	m_pShader(NULL),
	m_iNumLines(0),
	m_iNumVertices(0),
	m_fSpacing(0.0f),
	m_iAxisCol(0,0,0,1),
	m_iFadeCol(0,0,0,1)
{

}

CWorldGrid::~CWorldGrid(void)
{
	Shutdown();
}

bool
CWorldGrid::Init(int iNumLines, float fSpacing, Color32 iAxisCol, Color32 iFadeCol)
{
	m_iNumLines = iNumLines;
	m_fSpacing = fSpacing;
	m_iAxisCol = iAxisCol;
	m_iFadeCol = iFadeCol;

	grcFvf fvf;
	fvf.SetPosChannel(true);
	fvf.SetDiffuseChannel(true);
	m_pVertexDecl = grmModelFactory::BuildDeclarator(&fvf, NULL, NULL, NULL);

	m_iNumVertices = ((m_iNumLines * m_iNumLines)*3)*2;
	m_pVertexBuffer = grcVertexBuffer::Create(m_iNumVertices,0,fvf);
	m_pVertexBuffer->Lock();

	int idx=0;
	float x,y,z;
	Vector3 vPos;

	// do XY plane lines
	float max = m_fSpacing*m_iNumLines;
	float half = max/2.0f;

	for(x=-half; x<half; x+=m_fSpacing)
	{
		for(y=-half; y<half; y+=m_fSpacing)
		{
			vPos = Vector3(x,y,-half);
			m_pVertexBuffer->SetPosition(idx, vPos);
			m_pVertexBuffer->SetCPV(idx, m_iAxisCol);
			idx++;

			vPos = Vector3(x,y,half);
			m_pVertexBuffer->SetPosition(idx, vPos);
			m_pVertexBuffer->SetCPV(idx, m_iAxisCol);
			idx++;
		}
	}
	// do YZ plane lines
	for(y=-half; y<half; y+=m_fSpacing)
	{
		for(z=-half; z<half; z+=m_fSpacing)
		{
			vPos = Vector3(-half,y,z);
			m_pVertexBuffer->SetPosition(idx, vPos);
			m_pVertexBuffer->SetCPV(idx, m_iAxisCol);
			idx++;

			vPos = Vector3(half,y,z);
			m_pVertexBuffer->SetPosition(idx, vPos);
			m_pVertexBuffer->SetCPV(idx, m_iAxisCol);
			idx++;
		}
	}				
	// do XZ plane lines
	for(x=-half; x<half; x+=m_fSpacing)
	{
		for(z=-half; z<half; z+=m_fSpacing)
		{
			vPos = Vector3(x,-half,z);
			m_pVertexBuffer->SetPosition(idx, vPos);
			m_pVertexBuffer->SetCPV(idx, m_iAxisCol);
			idx++;

			vPos = Vector3(x,half,z);
			m_pVertexBuffer->SetPosition(idx, vPos);
			m_pVertexBuffer->SetCPV(idx, m_iAxisCol);
			idx++;
		}
	}

	m_pVertexBuffer->Unlock();

	return true;
}

void
CWorldGrid::Shutdown()
{
	CleanDelete(m_pVertexBuffer);
	CleanDelete(m_pShader);
	grmModelFactory::FreeDeclarator(m_pVertexDecl);
}


void
CWorldGrid::Draw(const Vector3 & vCameraPos, const Matrix34 & mCameraMat, grcViewport * pViewport)
{
	Matrix34 modelMat = mCameraMat;
	modelMat.d.x = fmod(vCameraPos.x, m_fSpacing);
	modelMat.d.y = fmod(vCameraPos.y, m_fSpacing);
	modelMat.d.z = fmod(vCameraPos.z, m_fSpacing);

	pViewport->SetCameraMtx(modelMat);

	/*
	grcDeviceHandle * pD3dDevice = GRCDEVICE.GetCurrent();
	float fFogStart = 400.0f;
	float fFogEnd = 500.0f;
	float fFogDensity = 0.5f;
	pD3dDevice->SetRenderState(D3DRS_FOGSTART, *((DWORD*) (&fFogStart)));
	pD3dDevice->SetRenderState(D3DRS_FOGEND, *((DWORD*) (&fFogEnd)));
	pD3dDevice->SetRenderState(D3DRS_FOGCOLOR, m_iFadeCol.GetColor());
	pD3dDevice->SetRenderState(D3DRS_FOGDENSITY, *((DWORD*) (&fFogDensity)));
	pD3dDevice->SetRenderState(D3DRS_FOGENABLE, TRUE);
	*/

	grcViewport::SetCurrentWorldIdentity();
	grcBindTexture(NULL);
	grcState::SetDepthWrite(true);
	grcState::SetDepthTest(true);
	


	GRCDEVICE.SetDefaultEffect(false, false);

	GRCDEVICE.SetVertexDeclaration(m_pVertexDecl);
	GRCDEVICE.SetStreamSource(0, *m_pVertexBuffer, 0, m_pVertexBuffer->GetVertexStride());
	GRCDEVICE.DrawPrimitive(drawLines,0,m_iNumVertices);

	//pD3dDevice->SetRenderState(D3DRS_FOGENABLE, FALSE);
}





