//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by NavMeshTrayApp.rc
//
#define ID_START_NOW                    2
#define IDOK2                           3
#define ID_EXIT                         3
#define ID_EXIT2                        4
#define ID_CLOSE_RAG_WINDOW             4
#define ID_LOAD_SETTINGS                4
#define ID_SAVE_SETTINGS                5
#define IDD_NAVMESHDIALOG               101
#define IDI_TRAYICON                    102
#define IDC_DATETIMEPICKER              1001
#define IDC_CHECK_GETLATESTDATA         1002
#define IDC_CHECK_CHECKIN               1003
#define IDC_CHECK_CONVERT_INDEPENDENT_AFTER 1003
#define IDC_CHECK_GETLATESTTOOLS        1003
#define IDC_EDIT_AB_USERNAME            1004
#define IDC_CHECK_GETLATESTTOOLS2       1004
#define IDC_CHECK_GETLATESTEXE          1004
#define IDC_EDIT_EXPORTPATH             1005
#define IDC_CHECK_BUILDENABLED          1006
#define ID_MINIMIZE_TO_TRAY             1007
#define IDC_EDIT1                       1008
#define IDC_EDIT_MINUTE                 1008
#define IDC_EDIT_HOUR                   1009
#define IDC_CHECK_CHECKIN2              1010
#define IDC_EDIT_EXPORTPATH2            1010
#define IDC_EDIT_GAMEEXTRAPARAMS        1010
#define IDC_EDIT_MAP_PARAM              1011
#define IDC_EDIT_MAPFILE_LOCATION       1011
#define IDC_CHECK_DO_EXPORT             1011
#define IDC_BUTTON_BROWSE_FOR_INI_FILE  1012
#define IDC_EDIT_NAVMESHES_IMG_PATH     1013
#define IDC_EDIT_INI_FILENAME           1014
#define IDC_EDIT_PROJECTROOT            1015
#define IDC_CHECKIN                     1016
#define IDC_CHECK_IN                    1016
#define IDC_CHECK_CHECK_IN              1016
#define IDC_EDIT_COMPILE_BATCH_FILE     1019
#define IDC_EDIT_COMPILE_LST_FILE       1020
#define IDC_BUTTON_BROWSE_FOR_LST_FILE  1021
#define IDC_BUTTON_BROWSE_FOR_COMPILE_BATCHFILE 1022
#define IDC_EDIT_AB_USERNAME2           1023
#define IDC_EDIT_AB_PROJECTNAME         1023
#define IDC_EDIT_NAVMESHES_IMG_PATH2    1023
#define IDC_EDIT_LEVEL_COMMON_DATA_PATH 1023
#define IDC_CHECK_DO_COMPILE            1024
#define IDC_CHECK_GETLATESTEXE2         1025
#define IDC_CHECK_DELETE_TEMP_FILES     1025
#define IDC_EDIT_EXPORTER_NAME          1026
#define IDC_EXTENTS_MIN_X               1029
#define IDC_CONTINUE_FROM               1030
#define IDC_RADIO1                      1031
#define IDC_RADIO_WHOLEMAP              1031
#define IDC_RADIO_CONTINUE              1032
#define IDC_RADIO_DLC                   1033
#define IDC_EDIT_EXPORTDLC_FILE         1034
#define IDC_EXTENTS_MIN_Y               1035
#define IDC_EDIT_EXTRACONTENT           1036
#define IDC_EXTENTS_MAX_X               1037
#define IDC_EXTENTS_MAX_Y               1038
#define IDC_EDIT_FINAL_FOLDER           1039
#define IDC_CHECK1                      1040
#define IDC_CHECK_MULTIPLAYER_DLC       1040
#define IDC_RADIO_DLC2                  1041
#define IDC_RADIO_REGION                1041

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1041
#define _APS_NEXT_SYMED_VALUE           102
#endif
#endif
