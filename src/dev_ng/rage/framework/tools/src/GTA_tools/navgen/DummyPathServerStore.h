#ifndef DUMMY_PATHSERVER_STORE_H
#define DUMMY_PATHSERVER_STORE_H

#include "ai/navmesh/navmeshextents.h"
#include "atl/array.h"
#include "fwscene/world/WorldLimits.h"
#include "system/FileMgr.h"

#if __BANK
#include "fwsys/timer.h"
#endif

#ifdef NAVGEN_TOOL
#include "grcore/vertexbuffer.h"
#endif

//-----------------------------------------------------------------------------

class aiMeshLoadRegion
{
public:
	Vector2 m_vOrigin;
	float m_fRadius;
};

typedef void (*PrepareToUnloadCB)(void * pMeshObject);


template <typename T> class aiMeshStore
{
public:

	aiMeshStore(int size)
	{
				
	}
	
	aiMeshStore(const char* pModuleName, int /*moduleTypeIndex*/, int size, bool requiresTempMemory, bool canDefragment = false, int rscVersion = 0)
	{
		m_iMaxMeshIndex = 0;

		m_iMeshIndexNone = 0xFFFF;
		m_vMin = Vector2(0.0f, 0.0f);
		m_fMeshSize = 0.0f;
		m_iNumMeshesInX = 0;
		m_iNumMeshesInY = 0;
		m_iMaxMeshesInAnyLevel = 0;
		m_iNumSectorsPerMesh = 0;

		m_pStore = new T* [size];
		for(int m=0; m<size; m++)
		{
			m_pStore[m] = NULL;
		}
	}

	virtual ~aiMeshStore()
	{
		delete[] m_pStore;
		m_pStore = NULL;
	}

	virtual void Init()
	{

	}
	virtual void Shutdown()
	{

	}

	void Set(int iIndex, T * pObject)
	{
		Assert(iIndex >= 0 && iIndex <= m_iMaxMeshIndex);
		Assert(m_pStore);
		Assert(!m_pStore[iIndex]);

		m_pStore[iIndex] = pObject;
	}

	virtual void RequestAndEvict(atArray<aiMeshLoadRegion> & loadRegions, PrepareToUnloadCB fnPrepareToUnloadMesh);

	virtual void* GetPtr(s32 UNUSED_PARAM(index)) { Assertf(false, "aiMeshStore::GetPtr() must be implemented in the derived class."); return NULL; }
	virtual int Register(const char* UNUSED_PARAM(name)) { Assertf(false, "aiMeshStore::Register() must be implemented in the derived class."); return -1; }
	virtual void Remove(s32 UNUSED_PARAM(index)) { Assertf(false, "aiMeshStore::Remove() must be implemented in the derived class."); }

	bool Load(s32 index, void* pData, int size)
	{
		datResourceMap * pResMap = NULL;
		datResourceInfo * pResHdr = NULL;
		return LoadOrPlace(index, pData, size, pResMap, pResHdr);
	}
	void PlaceResource(s32 UNUSED_PARAM(index), datResourceMap & UNUSED_PARAM(map), datResourceInfo & UNUSED_PARAM(header)) { }
	virtual bool LoadOrPlace(s32 UNUSED_PARAM(iStreamingIndex), void * UNUSED_PARAM(pMemForRead), s32 UNUSED_PARAM(iLengthForRead), datResourceMap* & UNUSED_PARAM(pMapForPlacement), datResourceInfo* & UNUSED_PARAM(pHeaderForPlacement))	{ Assertf(false, "aiMeshStore::LoadOrPlace() must be implemented in the derived class."); return false; }

	void AllocateMapping(const s32 UNUSED_PARAM(iMaxIndex)) { }
	void SetMapping(const s32 UNUSED_PARAM(iMeshIndex), const s16 UNUSED_PARAM(iStreamingIndex)) { }

	s32 GetStreamingIndex(const s32 iMeshIndex) const { return iMeshIndex; }

	bool GetIsMeshInImage(const s32 UNUSED_PARAM(iMeshIndex)) { return true; }
	bool GetDoesMeshExist(const s32 iMeshIndex)
	{
		Assert(iMeshIndex <= m_iMaxMeshIndex);
		return m_pStore[iMeshIndex] != NULL;
	}
	bool GetIsMeshRequired(const s32 iMeshIndex)
	{
		return false;
	}
	void SetIsMeshRequired(const s32 UNUSED_PARAM(iMeshIndex), const bool UNUSED_PARAM(bRequired))
	{

	}
	T * GetMeshByIndex(const s32 iMeshIndex)
	{
		Assert(iMeshIndex <= m_iMaxMeshIndex);
		return m_pStore[iMeshIndex];
	}
	T * GetMeshByStreamingIndex(const s32 iStreamingIndex)
	{
		Assert(iStreamingIndex <= m_iMaxMeshIndex);
		return m_pStore[iStreamingIndex];
	}

	s32 MeshCoordsToMeshIndex(const int x, const int y)
	{
		const int iMeshIndex = (y * GetNumMeshesInX()) + x;
		Assert(iMeshIndex <= m_iMaxMeshIndex);
		if(iMeshIndex > m_iMaxMeshIndex)
			return m_iMeshIndexNone;
		return iMeshIndex;
	}
	void MeshIndexToMeshCoords(const s32 iMeshIndex, int & iX, int & iY)
	{
		Assert(iMeshIndex != m_iMeshIndexNone);
		iY = iMeshIndex / GetNumMeshesInX();
		iX = iMeshIndex - (GetNumMeshesInX() * iY);
	}
	void GetMeshExtentsFromIndex(const s32 iMeshIndex, Vector2 & vMin, Vector2 & vMax)
	{
		const int iY = iMeshIndex / GetNumMeshesInY();
		const int iX = iMeshIndex - (iY * GetNumMeshesInY());
		const float fMeshSize = GetMeshSize();
		vMin.x = ((((float)iX) * fMeshSize) + CPathServerExtents::m_vWorldMin.x);
		vMin.y = ((((float)iY) * fMeshSize) + CPathServerExtents::m_vWorldMin.y);
		vMax.x = vMin.x + fMeshSize;
		vMax.y = vMin.y + fMeshSize;
	}
	s32 GetMeshIndexFromPosition(const Vector3 & vPos)
	{
		const int iSectorX = CPathServerExtents::GetWorldToSectorX(vPos.x);
		const int iSectorY = CPathServerExtents::GetWorldToSectorY(vPos.y);
		return GetMeshIndexFromSectorCoords(iSectorX, iSectorY);
	}
	s32 GetMeshIndexFromSectorCoords(const s32 iSectorX, const s32 iSectorY)	// TODO: phase out sector usage when we get time
	{
		const int iNavMeshBlockX = (int) (iSectorX / (float) m_iNumSectorsPerMesh);
		const int iNavMeshBlockY = (int) (iSectorY / (float) m_iNumSectorsPerMesh);
		if(iNavMeshBlockX < 0 || iNavMeshBlockX >= m_iNumMeshesInX || iNavMeshBlockY < 0 || iNavMeshBlockY >= m_iNumMeshesInY)
			return m_iMeshIndexNone;
		const s32 iIndex = (iNavMeshBlockY * m_iNumMeshesInX) + iNavMeshBlockX;
		return iIndex;
	}
	void GetSectorCoordsFromMeshIndex(const s32 iMeshIndex, s32 & iSectorX, s32 & iSectorY)
	{
		MeshIndexToMeshCoords(iMeshIndex, iSectorX, iSectorY);
		iSectorX *= m_iNumSectorsPerMesh;
		iSectorY *= m_iNumSectorsPerMesh;
	}
	inline s32 GetMaxMeshIndex() const { return m_iMaxMeshIndex; }
	inline s32 GetMeshIndexNone() const { return m_iMeshIndexNone; }
	inline s32 GetNumMeshesInX() const { return m_iNumMeshesInX; }
	inline s32 GetNumMeshesInY() const { return m_iNumMeshesInY; }
	inline s32 GetNumMeshesInAnyLevel() const { return m_iMaxMeshesInAnyLevel; }
	inline float GetMeshSize() const { return m_fMeshSize; }
	inline const Vector2 & GetMin() const { return m_vMin; }

	inline void SetMaxMeshIndex(const s32 i) { m_iMaxMeshIndex = i; }
	inline void SetMeshIndexNone(const s32 i) { m_iMeshIndexNone = i; }
	inline void SetNumMeshesInX(const s32 x) { m_iNumMeshesInX = x; }
	inline void SetNumMeshesInY(const s32 y) { m_iNumMeshesInY = y; }
	inline void SetNumMeshesInAnyLevel(const int n) { m_iMaxMeshesInAnyLevel = n; }
	inline void SetMeshSize(const float f) { m_fMeshSize = f; }
	inline void SetMin(const Vector2 & v) { m_vMin = v; }

	inline s32 GetNumSectorsPerMesh() const { return m_iNumSectorsPerMesh; }
	inline void SetNumSectorsPerMesh(const s32 i) { m_iNumSectorsPerMesh = i; }

	inline const atArray<s32> & GetLoadedMeshes() { return m_LoadedMeshes; }

protected:

	s32 m_iMaxMeshIndex;				// The maximum mesh index addressable
	s32 m_iMeshIndexNone;			// A special index to be used to indicate 'not found'
	s16 * m_pIndexMappingArray;		// A table used to map the mesh index to the streaming index
	Vector2 m_vMin;						// The minimum XY coordinate of any mesh.
	float m_fMeshSize;					// The XY size of each mesh
	s32 m_iNumMeshesInX;				// How many meshes across the whole map in XY (starting from m_vMin)
	s32 m_iNumMeshesInY;
	s32 m_iMaxMeshesInAnyLevel;		// The maximum across all levels (because we can't reinitialise streaming modules)

	s32 m_iNumSectorsPerMesh;			// Legacy - to be removed

	atArray<s32> m_LoadedMeshes;		// Array of which meshes are loaded

	T ** m_pStore;
};

template <typename T> void aiMeshStore<T>::RequestAndEvict(atArray<aiMeshLoadRegion> & UNUSED_PARAM(loadRegions), PrepareToUnloadCB UNUSED_PARAM(fnPrepareToUnloadMesh))
{

}

//-----------------------------------------------------------------------------

class aiNavMeshStore : public aiMeshStore<CNavMesh>
{
public:
	aiNavMeshStore(
		const char* pModuleName, 
		int moduleTypeIndex, 
		int size,
		bool requiresTempMemory,
		bool canDefragment = false,
		int rscVersion = 0);
	virtual ~aiNavMeshStore();

	virtual void Init();
};

//-----------------------------------------------------------------------------

#if __HIERARCHICAL_NODES_ENABLED

typedef rage::aiMeshAssetDef<CHierarchicalNavData> aiNavNodesAssetDef;

class aiNavNodesStore : public aiMeshStore<CHierarchicalNavData, aiNavNodesAssetDef>
{
public:
	aiNavNodesStore(
		const char* pModuleName, 
		const char* pFileExt, 
		int size,
		bool requiresTempMemory,
		bool canDefragment = false,
		int rscVersion = 0);
	virtual ~aiNavNodesStore();

	/*
	void Init();
	virtual void Shutdown();

	virtual void* GetPtr(int index);
	virtual int Register(const char* name);
	virtual void Remove(int index);
	virtual bool LoadOrPlace(s32 iStreamingIndex, void * pMemForRead, s32 iLengthForRead, datResourceMap* & pMapForPlacement, datResourceInfo* & pHeaderForPlacement);
#if !__FINAL
	virtual const char* GetName(int index) const;
#endif
	*/
};

#endif	// __HIERARCHICAL_NODES_ENABLED

class CDynamicNavMeshEntry
{
public:
	CDynamicNavMeshEntry() {
		m_pEntity = NULL;
		m_pNavMesh = NULL;
		m_Matrix.Identity();
		m_bCurrentlyCopyingMatrix = false;
		m_fBoundingRadius = 0.0f;

		for(int p=0; p<6; p++)
		{
			m_vBoundingPlanes[p] = Vector3(0.0f,0.0f,0.0f);
			m_fBoundingPlaneDists[p] = 0.0f;
		}
	}
	~CDynamicNavMeshEntry() { }

	class CEntity* GetEntity() const
	{
		return m_pEntity;
	}

	void SetEntity(CEntity *pNewEntity)
	{
		m_pEntity = pNewEntity;
	}

	Matrix34 m_Matrix;
	Vector3 m_vBoundingPlanes[6];
	float m_fBoundingRadius;
	float m_fBoundingPlaneDists[6];
	CNavMesh * m_pNavMesh;
	s32 m_bCurrentlyCopyingMatrix;

private:

#ifdef GTA_ENGINE
	RegdEnt m_pEntity;
#else
	CEntity * m_pEntity;
#endif

};

//******************************************************************************
//	TModelInfoNavMeshRef
//	A mapping between a model-index & a dynamic navmesh.

class CModelInfoNavMeshRef
{
public:
	CModelInfoNavMeshRef() { m_iModelIndex=-1; m_iNumRefs=0; m_pBackUpNavmeshCopy=NULL; m_iStreamingIndex=-1; m_pNavFileName=NULL; }
	~CModelInfoNavMeshRef() { Shutdown(); }

	inline void Shutdown() {
		if(m_pBackUpNavmeshCopy) { delete m_pBackUpNavmeshCopy; m_pBackUpNavmeshCopy = NULL; }
		if(m_pNavFileName) { delete[] m_pNavFileName; m_pNavFileName = NULL; }
		m_iNumRefs = 0;
		m_iModelIndex = -1;
		m_iStreamingIndex = -1;
	}

	// The model info which has this dynamic navmesh
	int m_iModelIndex;
	// The number of entities in existence which use this type of navmesh
	int m_iNumRefs;
	// A single backup copy of the navmesh, from which instances are copied
	CNavMesh * m_pBackUpNavmeshCopy;
	// The index that this navmesh is assigned within the streaming system
	s32 m_iStreamingIndex;
	// The name of the nav file within the navmeshes.img file
	char * m_pNavFileName;
};


class CDynamicNavMeshStore
{
public:
	CDynamicNavMeshStore() { m_iMaxDynamicNavmeshTypes = 0; m_iNumNavMeshes = 0; m_NavMeshes = NULL; }
	~CDynamicNavMeshStore() { }
	inline void Init(const int iMaxDynamicNavmeshTypes)
	{
		m_iMaxDynamicNavmeshTypes = iMaxDynamicNavmeshTypes;
		Assert(m_iMaxDynamicNavmeshTypes >= 0);
		if(m_iMaxDynamicNavmeshTypes > 0)
		{
			m_NavMeshes = rage_new CModelInfoNavMeshRef[m_iMaxDynamicNavmeshTypes];
		}
		m_iNumNavMeshes = 0;
	}
	inline void Shutdown() {
		for(int i=0; i<m_iNumNavMeshes; i++)
		{
			m_NavMeshes[i].Shutdown();
		}
		m_iNumNavMeshes = 0;
	}
	inline int GetNum() { return m_iNumNavMeshes; }
	inline int GetMaxNum() { return m_iMaxDynamicNavmeshTypes; }
	inline CModelInfoNavMeshRef * Add()
	{
		if(m_iNumNavMeshes < m_iMaxDynamicNavmeshTypes)
			return &m_NavMeshes[m_iNumNavMeshes++];
		else return NULL;
	}

	inline CModelInfoNavMeshRef & Get(int i) { Assert(i < m_iNumNavMeshes); return m_NavMeshes[i]; }
	inline const CModelInfoNavMeshRef & Get(int i) const { Assert(i < m_iNumNavMeshes); return m_NavMeshes[i]; }

	inline const CModelInfoNavMeshRef * GetByModelIndex(int mi) const
	{
		for(int n=0; n<m_iNumNavMeshes; n++)
		{
			if(m_NavMeshes[n].m_iModelIndex==mi)
				return &m_NavMeshes[n];
		}
		return NULL;
	}

private:

	CModelInfoNavMeshRef * m_NavMeshes;
	int m_iMaxDynamicNavmeshTypes;
	int m_iNumNavMeshes;
};

#endif // DUMMY_PATHSERVER_STORE_H
