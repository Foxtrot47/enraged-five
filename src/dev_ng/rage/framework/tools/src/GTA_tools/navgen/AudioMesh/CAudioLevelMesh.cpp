/////////////////////////////////////////////////////////////////////////////////
// FILE		: CAudioLevelMesh.cpp
// PURPOSE	: To load in level geometry for the audio mesh generation tool.
// AUTHOR	: C. Walder (based on Recast be Mikko Menonen)
// STARTED	: 31/08/2009
///////////////////////////////////////////////////////////////////////////////

#include "CAudioLevelMesh.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#define _USE_MATH_DEFINES
//#include <math.h>

CAudioLevelMesh::CAudioLevelMesh() :
	m_verts(0),
	m_tris(0), 
	m_normals(0), 
	m_vertCount(0),
	m_triCount(0),
	m_normalCount(0)
{
}

CAudioLevelMesh::~CAudioLevelMesh()
{
	delete [] m_verts;
	delete [] m_normals;
	delete [] m_tris;
}

void CAudioLevelMesh::AddVertex(float x, float y, float z, int &cap)
{
	if(m_vertCount+1 > cap)
	{
		cap = !cap ? 8 : cap*2;
		float *nv = rage_new float[cap*3];
		if (m_vertCount)
		{
			memcpy(nv, m_verts, m_vertCount*3*sizeof(float));
		}
		delete [] m_verts;
		m_verts = nv;
	}

	float *dst = &m_verts[m_vertCount*3];
	*dst++ = x;
	*dst++ = y;
	*dst++ = z;
	m_vertCount++;
}

void CAudioLevelMesh::AddTriangle(int a, int b, int c, int &cap)
{
	if(m_triCount+1 > cap)
	{
		cap = !cap ? 8 : cap *2;
		int *nt = rage_new int[cap*3];
		if(m_triCount)
		{
			memcpy(nt, m_tris, m_triCount*3*sizeof(int));
		}
		delete [] m_tris;
		m_tris = nt;
	}
	int *dst = &m_tris[m_triCount*3];
	*dst++ = a;
	*dst++ = b;
	*dst++ = c;
	m_triCount++;
}

void CAudioLevelMesh::AddNormal(float x, float y, float z, int &cap)
{
	if(m_normalCount+1 > cap)
	{
		cap = !cap ? 8 : cap *2;
		float *nn = rage_new float[cap*3];
		if(m_normalCount)
		{
			memcpy(nn, m_normals, m_normalCount*3*sizeof(float));
		}
		delete [] m_normals;
		m_normals = nn;
	}
	float *dst = &m_normals[m_normalCount*3];
	*dst++ = x;
	*dst++ = y;
	*dst++ = z;
	m_normalCount++;
}


bool CAudioLevelMesh::Load(const char *inputFileName, bool isMainMesh)
{
	//Stuff which isn't used by us but comes from the navgen side which we robbed this from
	Vector3 vSectorMins, vSectorMaxs;
	int iSectorStartX, iSectorStartY;
	int iNumSectorsX, iNumSectorsY;
	Vector3 vWorldMin, vWorldMax;
	Vector2 vMapMin;

	char filename[1024];
	strcpy(filename, inputFileName);
	strlwr(filename);

	if(strstr(filename, ".tri"))
	{
		fiStream * pStream = fiStream::Open(filename);
		if(!pStream)
			return false;

		const char triHeader[] = "NAVTRI6";
		char header[8];

		pStream->Read(header, 8);

		if(strcmp(header, triHeader)!=0)
		{
			pStream->Close();
			return false;
		}


		uint32 iReserved;
		pStream->ReadInt(&iReserved, 1);

		pStream->ReadFloat(&vMapMin.x, 1);
		pStream->ReadFloat(&vMapMin.y, 1);
		
		pStream->ReadFloat(&vSectorMins.x, 1);
		pStream->ReadFloat(&vSectorMins.z, 1);
		pStream->ReadFloat(&vSectorMins.y, 1);

		pStream->ReadFloat(&vSectorMaxs.x, 1);
		pStream->ReadFloat(&vSectorMaxs.z, 1);
		pStream->ReadFloat(&vSectorMaxs.y, 1);

		if(isMainMesh)
		{
			m_bmin = vSectorMins;
			m_bmax = vSectorMaxs;
		}

		pStream->ReadInt(&iSectorStartX, 1);
		pStream->ReadInt(&iSectorStartY, 1);
		pStream->ReadInt(&iNumSectorsX, 1);
		pStream->ReadInt(&iNumSectorsY, 1);


		uint8 bHasWater=false;
		float fWaterLevel=0.0f;
		uint8 bHasVariableWaterLevel=false;

		pStream->ReadByte(&bHasWater, 1);
		pStream->ReadFloat(&fWaterLevel, 1);
		pStream->ReadByte(&bHasVariableWaterLevel, 1);

		// If there is a variable water level in this navmesh area (this includes navmeshes
		// which have gaps in the water coverage) then read in sampled water heights.
		// !!!Not used in Audio mesh but we still need to go through the motions of reading 
		// it out.
		u16 tmp16;

		if(bHasVariableWaterLevel)
		{
			float tmpf;
			int numWaterSamples;
			pStream->ReadFloat(&tmpf, 1);
			pStream->ReadInt(&numWaterSamples, 1);

			Assert(numWaterSamples > 0);

			for(int s=0; s<numWaterSamples; s++)
			{
				pStream->ReadShort(&tmp16, 1);
			}
		}

		//***********************************************************************************
		// This is done to all imported data, to ensure that the divisions of the
		// sector never lie exactly on a cardinal number - as this can cause problems
		// when building the octree.  We offset ever-so-slightly so that no errors occur.
		// (essentially this is the same as using an epsilon value for plane-tests).

		float x,y,z;
		TColPolyData colPolyData;
		uint8 iBakedInBitField;
		Vector3 pts[3];
		Vector3 vEdge1, vEdge2, vEdge3;
		int p,n;
		if(isMainMesh)
		{
			delete [] m_verts; m_verts = 0;
			delete [] m_tris; m_tris = 0;
			delete [] m_normals; m_normals = 0;
			m_vertexCap = 0;
			m_triCap = 0;
			m_normalCap = 0;
		}

		for(;;)
		{
			n = pStream->ReadByte(&iBakedInBitField, 1);
			if(!n)
				break;

			//!!!We don't use these at the moment but there may come a time where 
			//we want them and we still have to read them out regardless
			u8 tmp8;
			pStream->ReadShort(&tmp16, 1);
			pStream->ReadByte(&tmp8, 1);

			//colPolyData.SetDontCreateNavMeshOn(false);

			for(p=0; p<3; p++)
			{
				pStream->ReadFloat(&x, 1);
				pStream->ReadFloat(&y, 1);
				pStream->ReadFloat(&z, 1);

				pts[p].x = x;
				pts[p].y = y;
				pts[p].z = z;
			}

			// Make sure triangle's not degenerate
			vEdge1 = pts[1] - pts[0];
			vEdge2 = pts[2] - pts[0];
			vEdge3 = pts[2] - pts[1];

			static const float fMinTriArea = 0.0001f;
			static const float fMinEdgeLength = 0.001f;

			float fEdgeLength1 = vEdge1.Mag();
			float fEdgeLength2 = vEdge2.Mag();
			float fEdgeLength3 = vEdge3.Mag();

			if(fEdgeLength1 < fMinEdgeLength || fEdgeLength2 < fMinEdgeLength || fEdgeLength3 < fMinEdgeLength)
			{
				continue;
			}

			float fArea = CalcTriArea(fEdgeLength1, fEdgeLength2, fEdgeLength3);
			if(Abs(fArea) < fMinTriArea)
			{
				continue;
			}

			// Calculate normal
			Vector3 normal = CrossProduct(vEdge2, vEdge1);
			normal.Normalize();

			// Small normal can indicate degenerate tri
			if(normal.Mag() < 0.5f)
			{
				continue;
			}

			//!!!Because we're not working with a .obj file we'll have to make up
			//the faces as we go along...there might be a case for going through the
			//whole system and removing this redundant information
			int vtx[3];

			for(int i=0; i<3; i++)
			{
				vtx[i] = m_vertCount;
				//FIXME: putting these the wrong way round seems to make it work...I have absolutely no idea why though....
				//perhaps when it comes to exporting stuff we'll have to do the reverse. Very odd indeed.
				AddVertex(pts[i].x, pts[i].z, pts[i].y, m_vertexCap);
			}

			AddTriangle(vtx[0], vtx[1], vtx[2], m_triCap);
			//FIXME: same as above, z ad y axis seem to be swapped although it doesn't make sense that they would be

			AddNormal(normal.x, normal.z, normal.y, m_normalCap);

			// Test triangle against the optional min/max supplied.  This is to cull interior polys
			// to the extents of the main-map navmesh which they are in.
			//not sure if we need this...I'm guessing we'll want it eventtually but for the time 
			//being we can probably do without it
//			if(pCullToThisMin && pCullToThisMax)
//			{
//				if(vTriMin.x > pCullToThisMax->x || vTriMin.y > pCullToThisMax->y || vTriMin.z > pCullToThisMax->z ||
//					pCullToThisMin->x > vTriMax.x || pCullToThisMin->y > vTriMax.y || pCullToThisMin->z > vTriMax.z)
//				{
//					delete pTri;
//					continue;
//				}
//			}

		}

		pStream->Close();

		if(isMainMesh)
		{
			char adjSectorName[256] = {0};
			if(iSectorStartX-2 >=0)
			{	
				formatf(adjSectorName, "x:/jimmmy/exported_navmeshes/navmesh[%i][%i].tri", iSectorStartX-2, iSectorStartY);
				Load(adjSectorName, false);
			}
			if(iSectorStartX+2 <200)
			{
				formatf(adjSectorName, "x:/jimmy/exported_navmeshes/navmesh[%i][%i].tri", iSectorStartX+2, iSectorStartY);
				Load(adjSectorName, false);
			} 
			if(iSectorStartY-2 >=0)
			{	
				formatf(adjSectorName, "x:/jimmy/exported_navmeshes/navmesh[%i][%i].tri", iSectorStartX, iSectorStartY-2);
				Load(adjSectorName, false);
			}
			if(iSectorStartY+2 <200)
			{
				formatf(adjSectorName, "x:/jimmy/exported_navmeshes/navmesh[%i][%i].tri", iSectorStartX, iSectorStartY+2);
				Load(adjSectorName, false);
			}
		}
	}

	return true;
}



