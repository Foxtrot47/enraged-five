//**************************************************************************************
//	Filename : TestAppCommon
//	Description : A class which can be derived from to create a simple rage test app.
//	Author : James Broad
//	Date : 7/5/06
//**************************************************************************************

#ifndef TESTAPPCOMMON_H
#define TESTAPPCOMMON_H

#include "bank/bkmgr.h"
#include "devcam/cammgr.h"
#include "grcore/setup.h"
#include "grcore/vertexbuffer.h"
#include "grcore/vertexdecl.h"
#include "grcore/viewport.h"
#include "grmodel/shader.h"
#include "input/mapper.h"

class CGtaDebugCam;
class CWorldGrid;

class CTestAppBase
{
public:
	CTestAppBase();
	virtual ~CTestAppBase();

	// Initialise systems
	bool Init();
	// Run the app until exit
	bool Run();
	// Shut everything down
	bool Shutdown();

	virtual const char * GetAppAssetPath() { return m_WorkingDir; }
	virtual const char * GetAppName() { return "TestAppBase"; }

	void SetWantsToQuit(void) { m_bWantsToQuit = true; }

	static CTestAppBase * ms_pApp;

protected:

	// Called for derived classes at init
	virtual bool AppSpecificInit() = 0;
	// Called for derived classes at shutdown
	virtual bool AppSpecificShutdown() = 0;
	// Called every frame for derived class to run
	virtual bool AppSpecificRunLoop() = 0;

	static void InitCommonWidgets(void);

	static void DrawGrid();

	static char m_WorkingDir[1024];
	static grcSetup * m_pGrcSetup;	
	static grcViewport * m_pViewport;
	static CGtaDebugCam * m_pDebugCam;
	static CWorldGrid * m_pWorldGrid;

	static Color32 m_ClearCol;

	static bool m_bInitialised;
	static bool m_bDrawWorldGrid;
	static bool m_bWantsToQuit;

	static ioMapper m_MouseMapper;
	static ioValue m_MouseLeftButton;
};


class CGtaDebugCam
{
public:

	CGtaDebugCam(void);
	~CGtaDebugCam(void);

	void Init(void);
	void Shutdown(void);
	void Run(void);

	inline void SetPosition(const Vector3 & vPos) { m_vPosition = vPos; }
	inline void SetMatrix(const Matrix34 & mat) { m_CameraMatrix = mat; }
	void SetViewVector(const Vector3 & vForwardsIn);

	inline void SetUpAxis(const Vector3 & vUp) { m_vUpAxis = vUp; }
	void LookAt(const Vector3 & vTarget);
	inline const Matrix34 & GetMatrix() { return m_CameraMatrix; }
	inline Vector3 GetPosition() { return m_vPosition; }

private:

	void SetIdentity(void);

	Vector3 m_vPosition;
	Matrix34 m_CameraMatrix;
	Vector3 m_vUpAxis;
	float m_fHeading;
	float m_fPitch;
	Vector3 m_vVelocity;

	ioMapper m_Mapper;
	ioValue m_Heading;
	ioValue m_Pitch;
	ioValue m_MoveX;
	ioValue m_MoveY;
	ioValue m_Forwards;
	ioValue m_Backwards;

};

class CWorldGrid
{
public:
	CWorldGrid(void);
	~CWorldGrid(void);

	bool Init(int iNumLines, float fSpacing, Color32 iAxisCol = Color32(0.25f, 0.7f, 0.25f, 1.0f), Color32 iFadeCol = Color32(0.0f, 0.0f, 0.0f, 1.0f));
	void Shutdown(void);

	void Draw(const Vector3 & vCameraPos, const Matrix34 & mCameraMat, grcViewport * pViewport);

private:

	int m_iNumLines;
	int m_iNumVertices;
	Color32 m_iAxisCol;
	Color32 m_iFadeCol;
	float m_fSpacing;

	grcVertexBuffer * m_pVertexBuffer;
	grcVertexDeclaration * m_pVertexDecl;
	grmShader * m_pShader;
};

class CMarker
{
public:
	CMarker(void);
	~CMarker(void);

	bool Init(float fSize=1.0f, Color32 col = Color32(255,255,255,255));
	void Shutdown();

	void Draw();

	void SetSize(float fSize);
	void SetHeight(float fSize);
	void SetCol(Color32 col);
	void SetPosition(const Vector3 & vPos);
	void SetRotation(float fRot);

	inline const Vector3 & GetPosition() { return m_vPosition; }

private:
	Vector3 m_vPosition;
	float m_fRotation;
	float m_fSize;
	float m_fHeight;
};

#endif

