#include "NavGenTest_Win32.h"
#include "ai/navmesh/navmeshextents.h"
#include "vector/quaternion.h"
#include "pathserver\NavMeshGta.h"
#include "pathserver\PathServer.h"
#include "fwnavgen/toolnavmesh.h"

#include "fwnavgen/navgen_store.h"
#include "atl\binheap.h"


using namespace rage;

static const float ROTATE_SPEED		= 45.0f;
static const float MOVE_SPEED		= 50.0f;

class CPathServerGameInterfaceNavGenTest : public fwPathServerGameInterface
{
public:
	virtual void OverridePathRequestParameters(const TRequestPathStruct& UNUSED_PARAM(reqStruct), CPathRequest& pathRequest) const
	{
		pathRequest.m_iPedRandomSeed = timeGetTime();
	}
};


static CPathServerGameInterfaceNavGenTest s_PathServerGameInterface;

void
CNavGenApp::GetInput(float fSecs)
{
	//*******************************************
	//
	//	Handle mouse input
	//
	//*******************************************

	if(m_bMouseLook && m_pMouseDevice)
	{
		static const float fMouseScale = 10.0f;

		DIMOUSESTATE mouseState;
		HRESULT retval = m_pMouseDevice->GetDeviceState(sizeof(DIMOUSESTATE), &mouseState);

		if(retval == S_OK && (mouseState.lX || mouseState.lY || mouseState.lZ))
		{
			Vector3 mouseMovement;
			mouseMovement.x = ((mouseState.lX / 255.0f) * fMouseScale) * fSecs;
			mouseMovement.y = ((-mouseState.lY / 255.0f) * fMouseScale) * fSecs;
			mouseMovement.z = ((mouseState.lZ / 255.0f) * fMouseScale) * fSecs;

			float fYaw = (mouseMovement.x * ROTATE_SPEED) * DtoR;
			float fPitch = (mouseMovement.y * (ROTATE_SPEED*2.0f)) * DtoR;

			//*****************************************************
			//	Apply yaw around world's up vector (0, 0, 1.0f)
			//*****************************************************

			Matrix34 matYaw;
			Matrix34 matPitch;
			Matrix34 matTmp;

			matYaw.Identity();
			matPitch.Identity();

			Vector3 yawAxis(0.0f, 0.0f, 1.0f);

			// Use a quaternion for axis & angle
			Quaternion quatYaw;
			quatYaw.FromRotation(yawAxis, fYaw);

			// copy into a Matrix34
			matYaw.FromQuaternion(quatYaw);
			matTmp.Dot(m_mViewOrientation, matYaw);

			//*****************************************************
			//	Apply pitch around the view matrix's right vector
			//*****************************************************

			Vector3 vRight = matTmp.a * 1.0f;
			Vector3 pitchAxis(vRight.x, vRight.y, vRight.z);

			// Use a quaternion for axis & angle
			Quaternion quatPitch;
			quatPitch.FromRotation(pitchAxis, fPitch);

			matPitch.FromQuaternion(quatPitch);
			m_mViewOrientation.Dot(matTmp, matPitch);
		}
	}

	//*******************************************
	//
	//	Handle mouse clicks
	//
	//*******************************************

	if(!m_bMouseLook)
	{
		if(m_bMouseClicked)
		{
			m_bMouseClicked = false;

			// Project the clicked-pos to get points on the near & far clip-planes
			Vector3 vecNear, vecFar;
			ConvertScreenCoordsToPointsOnNearAndFarPlanes(m_MouseXPos, m_MouseYPos, vecNear, vecFar);

			Vector3 vRay = vecFar - vecNear;
			vRay.Normalize();
			vecFar += vRay * 100.0f;

			//********************************************************************
			//	Do we want to select one of the collision triangles, or one of
			//	the navigation-surface triangles?
			//	If we have the CPathServer initialised, then we'll use that.
			//	Otherwise if we have a generated nav-mesh we'll use that.
			//	Finally, if none of the above - we'll use the collision triangles.
			//********************************************************************

 			if(m_bPathServerInitialised && m_bDisplayPathServerMesh)
			{
				Vector3 vIntersectPos;
				float fIntersectDist;
				u16 iHitPoly = NAVMESH_POLY_INDEX_NONE;

				if(m_DynamicNavMesh && m_DynamicNavMesh->IntersectRay(vecNear, vecFar, vIntersectPos, fIntersectDist, iHitPoly))
				{

				}
				else
				{
					int iMeshIndices[9];
					float fMeshSize = CPathServerExtents::GetWorldWidthOfSector() * CPathServerExtents::m_iNumSectorsPerNavMesh;

					const aiNavDomain domain = kNavDomainRegular;

					iMeshIndices[0] = CPathServer::GetNavMeshIndexFromPosition(m_vViewPos + Vector3(-fMeshSize,-fMeshSize,0), domain);
					iMeshIndices[1] = CPathServer::GetNavMeshIndexFromPosition(m_vViewPos + Vector3(0,-fMeshSize,0), domain);
					iMeshIndices[2] = CPathServer::GetNavMeshIndexFromPosition(m_vViewPos + Vector3(fMeshSize,-fMeshSize,0), domain);

					iMeshIndices[3] = CPathServer::GetNavMeshIndexFromPosition(m_vViewPos + Vector3(-fMeshSize,0,0), domain);
					iMeshIndices[4] = CPathServer::GetNavMeshIndexFromPosition(m_vViewPos + Vector3(0,0,0), domain);
					iMeshIndices[5] = CPathServer::GetNavMeshIndexFromPosition(m_vViewPos + Vector3(fMeshSize,0,0), domain);

					iMeshIndices[6] = CPathServer::GetNavMeshIndexFromPosition(m_vViewPos + Vector3(-fMeshSize,fMeshSize,0), domain);
					iMeshIndices[7] = CPathServer::GetNavMeshIndexFromPosition(m_vViewPos + Vector3(0,fMeshSize,0), domain);
					iMeshIndices[8] = CPathServer::GetNavMeshIndexFromPosition(m_vViewPos + Vector3(fMeshSize,fMeshSize,0), domain);

					for(int i=0; i<9; i++)
					{
						if(iMeshIndices[i] == NAVMESH_NAVMESH_INDEX_NONE)
							continue;

						CNavMesh * pNavMesh = CPathServer::GetNavMeshFromIndex(iMeshIndices[i], domain);

						if(pNavMesh && pNavMesh->IntersectRay(vecNear, vecFar, vIntersectPos, fIntersectDist, iHitPoly))
						{
							break;
						}
					}
				}
				if(iHitPoly != NAVMESH_POLY_INDEX_NONE)
				{
					if(m_bCtrlDown)
					{
						m_vNavMeshPathSearchEndPos = vIntersectPos;
					}
					else
					{
						m_vNavMeshPathSearchStartPos = vIntersectPos;
					}
				}
			}
			else if(!m_bDrawNavigationSurfaces)
			{
				CNavGenTri * pHitTri = NULL;
				Vector3 vHitPos;
				float fHitDist;
				u32 iRetFlags;

				bool bHit = m_NodeGenerator.m_Octree.ProcessLineOfSight(
					vecNear,
					vecFar,
					iRetFlags,
					TColPolyData::COLLISION_TYPE_ALL,
					vHitPos,
					fHitDist,
					pHitTri
				);
  				if(bHit && pHitTri)
				{
					m_iSelectedTri = pHitTri->m_iIndexInCollisionPolysList;
					m_pSelectedNavSurfaceTri = NULL;
					m_pDestinationNavSurfaceTri = NULL;

					char tmp[256];

					sprintf(tmp, "\nCollTri : 0x%X, Index : %i, bPavement : %i, iPedDensity : %i, bRoad: %i, bTrainTracks: %i\n",
						pHitTri, m_iSelectedTri,
						pHitTri->m_colPolyData.GetIsPavement(),
						pHitTri->m_colPolyData.GetPedDensity(),
						pHitTri->m_colPolyData.GetIsRoad(),
						pHitTri->m_colPolyData.GetIsTrainTracks()
					);
					OutputDebugString(tmp);

					sprintf(tmp, "Verts : (%.2f, %.2f, %.2f) (%.2f, %.2f, %.2f) (%.2f, %.2f, %.2f)\n",
						pHitTri->m_vPts[0].x, pHitTri->m_vPts[0].y, pHitTri->m_vPts[0].z,
						pHitTri->m_vPts[1].x, pHitTri->m_vPts[1].y, pHitTri->m_vPts[1].z,
						pHitTri->m_vPts[2].x, pHitTri->m_vPts[2].y, pHitTri->m_vPts[2].z
					);
					OutputDebugString(tmp);

					sprintf(tmp, "Normal : (%.2f, %.2f, %.2f)\n",
						pHitTri->m_vNormal.x, pHitTri->m_vNormal.y, pHitTri->m_vNormal.z
					);
					OutputDebugString(tmp);

					sprintf(tmp, "Bound Flags: ");
					if(pHitTri->m_colPolyData.GetIsMoverBound())
						strcat(tmp, "MOVER ");
					if(pHitTri->m_colPolyData.GetIsWeaponBound())
						strcat(tmp, "WEAPON ");
					if(pHitTri->m_colPolyData.GetIsRiverBound())
						strcat(tmp, "RIVER ");
					if(pHitTri->m_colPolyData.GetIsCoverBound())
						strcat(tmp, "COVER ");
					if(pHitTri->m_colPolyData.GetIsStairSlopeBound())
						strcat(tmp, "STARISLOPE ");

					OutputDebugString(tmp);
				}
			}
			// Nav-surface tri-picking
			else
			{
				CNavSurfaceTri * pTri = m_NodeGenerator.PickNavSurfaceTriangle(vecNear, vecFar);
				m_iSelectedTri = -1;

				if(m_bCtrlDown)
				{
					m_pDestinationNavSurfaceTri = pTri;
				}
				else
				{
					m_pSelectedNavSurfaceTri = pTri;

					if(pTri)
					{
						char tmp[256];
						sprintf(tmp, "\nSelNavTri : 0x%X (Adjacent : 0x%X, 0x%X, 0x%X)\n",
							pTri, pTri->m_AdjacentTris[0], pTri->m_AdjacentTris[1], pTri->m_AdjacentTris[2]
						);
						OutputDebugString(tmp);

						sprintf(tmp, "bPavement : %i, fPedDensity : %.1f, bIsWater: %s\n",
							pTri->m_colPolyData.GetIsPavement(),
							pTri->m_colPolyData.GetPedDensity(),
							pTri->m_bIsWater ? "true":"false"
 
						);
						OutputDebugString(tmp);
 

						for(int n=0; n<3; n++)
						{
							sprintf(tmp, "Nodes[%i] 0x%X (%.2f, %.2f, %.2f), Tris: (",
								n,
								pTri->m_Nodes[n],
								pTri->m_Nodes[n]->m_vBasePos.x, pTri->m_Nodes[n]->m_vBasePos.y, pTri->m_Nodes[n]->m_vBasePos.z
							);

							OutputDebugString(tmp);

							for(u32 t=0; t<pTri->m_Nodes[n]->m_TrianglesUsingThisNode.size(); t++)
							{
								sprintf(tmp, "0x%X ", pTri->m_Nodes[n]->m_TrianglesUsingThisNode[t]);
								OutputDebugString(tmp);
							}
							OutputDebugString(")\n");
						}
					}
				}
			}
		}
	}

	//*******************************************
	//
	//	Handle general key input
	//
	//*******************************************

	if(KeyJustDown(VK_ESCAPE))
	{
		PostQuitMessage(0);
		return;
	}

	if(KeyJustDown(VK_TAB))
	{
		m_CursorMode = (eCursorMode)((int)m_CursorMode + 1);
		if(m_CursorMode == eNumCursorModes)
			m_CursorMode = (eCursorMode)0;
	}

	// Test an immediate-mode seek path
/*
	if(KeyJustDown('Y'))
	{
		Vector3 vPts[16];
		u32 iNumPts;
		CPathServer::CalculateSeekPathImmediate(
			m_vNavMeshPathSearchStartPos,
			m_vNavMeshPathSearchEndPos,
			16,
			iNumPts,
			vPts
		);

		m_vNavMeshPathPoints.Reset();
		m_iNavMeshPathWaypointFlags.Reset();
		m_iNavMeshNumTriPts = 0;

		for(u32 v=0; v<iNumPts; v++)
		{
			m_vNavMeshPathPoints.PushAndGrow(vPts[v]);

			TNavMeshWaypointFlag emptyWptFlag;
			emptyWptFlag.Clear();
			m_iNavMeshPathWaypointFlags.PushAndGrow(emptyWptFlag);
		}
	}
*/

/*
	// Test an immediate-mode flee path
	if(KeyJustDown('Y'))
	{
		ConfineCursorToWindow(false);
		AcquireMouse(false);

		m_vNavMeshPathSearchStartPos = Vector3(908.009888f, 1773.38818f, 20.7151012f);
		m_vNavMeshPathSearchEndPos = Vector3(998.591431f, 1773.74988f, 18.6348553f);
		static const float fReqDist = 150.606140f;

		Vector3 vPts[16];
		u32 iNumPts=0;
		CPathServer::CalculateFleePathImmediate(
			m_vNavMeshPathSearchStartPos,
			m_vNavMeshPathSearchEndPos,
			fReqDist,
			16, iNumPts,
			vPts
		);

		m_vNavMeshPathPoints.Reset();
		m_iNavMeshPathWaypointFlags.Reset();
		m_iNavMeshNumTriPts = 0;

		for(u32 v=0; v<iNumPts; v++)
		{
			m_vNavMeshPathPoints.PushAndGrow(vPts[v]);
			TNavMeshWaypointFlag flag;
			flag.Clear();
			m_iNavMeshPathWaypointFlags.PushAndGrow(flag);
		}
	}
*/


	// This creates a rectangular test dynamic object at the green flag, orientated towards the red flag
	if(KeyJustDown('O'))
	{
		CObject * pEntity = rage_new CObject();

		if(GetKeyState(VK_LCONTROL))
		{
			pEntity->m_fWidth = 1.0f;
			pEntity->m_fDepth = 1.0f;
			pEntity->m_fHeight = 3.0f;
		}
		else
		{
			pEntity->m_fWidth = 5.0f;
			pEntity->m_fDepth = 2.0f;
			pEntity->m_fHeight = 3.0f;
		}

		pEntity->m_vPosition = m_vNavMeshPathSearchStartPos;
		pEntity->m_vPosition.z += (pEntity->m_fHeight / 2.0f) - 0.1f ;
		pEntity->m_fRotationInDegrees = 0.0f;

		pEntity->m_iPathServerDynObjId = DYNAMIC_OBJECT_INDEX_NONE;

		CPathServerGta::AddDynamicObject((CEntity*)pEntity, NULL);
		CPathServerGta::UpdateDynamicObject((CEntity*)pEntity);
		CPathServer::m_PathServerThread.UpdateAllDynamicObjectsPositions();
	}
 	if(KeyJustDown('N'))
	{
		bool bInReverse = GetKeyState(VK_LSHIFT);

		TDynamicObject * pDynObj = CPathServer::m_PathServerThread.m_pFirstDynamicObject;

		if(!m_pSelDynObj)
		{
			m_pSelDynObj = pDynObj;
		}
		else
		{
			if(!bInReverse)
			{
				while(pDynObj)
				{
					if(m_pSelDynObj == pDynObj)
					{
						m_pSelDynObj = pDynObj->m_pNext;
						if(!m_pSelDynObj)
						{
							m_pSelDynObj = CPathServer::m_PathServerThread.m_pFirstDynamicObject;
						}
						break;
					}
					pDynObj = pDynObj->m_pNext;
				}
			}
			else
			{
				while(pDynObj)
				{
					if(m_pSelDynObj == pDynObj->m_pNext)
					{
						m_pSelDynObj = pDynObj;
						if(!m_pSelDynObj)
						{
							m_pSelDynObj = CPathServer::m_PathServerThread.m_pFirstDynamicObject;
						}
						break;
					}
					pDynObj = pDynObj->m_pNext;
				}
			}
		}
	}
	if(KeyJustDown('M'))
	{
		if(m_pSelDynObj)
		{
			CEntity * pEntity = (CEntity*)m_pSelDynObj->m_pEntity;
			pEntity->m_fRotationInDegrees += 10.0f;
			while(pEntity->m_fRotationInDegrees >= 360.0f)
				pEntity->m_fRotationInDegrees -= 360.0f;

			pEntity->m_Matrix.RotateZ(pEntity->m_fRotationInDegrees * DtoR);

			CPathServerGta::UpdateDynamicObject(pEntity);
		}
	}

	if(KeyJustDown('V'))
	{
		m_bPavements = !m_bPavements;

		Vector3 vHitPos;
		float fHitDist;
		u32 iRetFlags;
		CNavGenTri * pHitTri;
		bool bHit = m_NodeGenerator.m_Octree.ProcessLineOfSight(
			m_vViewPos, m_vViewPos - Vector3(0.0f,0.0f,20.0f),
			iRetFlags,
			TColPolyData::COLLISION_TYPE_ALL,
			vHitPos, fHitDist, pHitTri);
		bHit;
	}
/*
	if(KeyJustDown('E'))
	{
		CNavGenApp::ToggleExhaustiveTesting();
	}
*/
	if(KeyJustDown('B'))
	{
		TNavMeshIndex iNavmesh = CPathServer::GetNavMeshIndexFromPosition(m_vNavMeshPathSearchStartPos, kNavDomainRegular);
		CNavMesh * pNavMesh = CPathServer::GetNavMeshFromIndex(iNavmesh, kNavDomainRegular);

		if(m_iDrawNavMeshSinglePolyIndex==NAVMESH_POLY_INDEX_NONE)
		{
			m_iDrawNavMeshSinglePolyIndex = 0;
		}
		else
		{
			m_iDrawNavMeshSinglePolyIndex++;
			if(pNavMesh && m_iDrawNavMeshSinglePolyIndex >= pNavMesh->GetNumPolys())
				m_iDrawNavMeshSinglePolyIndex = NAVMESH_POLY_INDEX_NONE;
		}

		if(pNavMesh && m_iDrawNavMeshSinglePolyIndex != NAVMESH_POLY_INDEX_NONE)
		{
			DebugNavMeshPoly(pNavMesh->GetPoly(m_iDrawNavMeshSinglePolyIndex), pNavMesh);
		}
	}

	if(KeyJustDown('P') || KeyJustDown('F') || KeyJustDown('R') || KeyJustDown('I') || KeyJustDown('C'))
	{
		ConfineCursorToWindow(false);
		AcquireMouse(false);

		g_App->ClearAllPolyCosts();

		if(g_App->ms_bDetessellatePriorToPathSearch)
		{
			g_App->DetessellateNow();
		}

		CPathServer::ms_bHaveAnyDynamicObjectsChanged = true;
		CPathServer::m_PathServerThread.UpdateAllDynamicObjectsPositions();

		if(m_bPathServerInitialised)
		{
			// Set midway between the paths as the origin for the player.
			// The path server never lets paths get too far away from the player, or peds will be 'disappeared'!
			Vector3 vPlayerOrigin = (m_vNavMeshPathSearchStartPos + m_vNavMeshPathSearchEndPos) / 2.0f;
			CPathServer::Process(vPlayerOrigin);

			u32 iPathHandle = 0;

			int iNumRequests = 1;
			do
			{

				// If 'F' is down, then calculate a 'flee' path.
				// This finds a path from the 'start' flag, and gets as far away as possible from the 'end' flag
				if(KeyJustDown('F'))
				{
					TRequestPathStruct reqStruct;
					reqStruct.m_vPathStart = m_vNavMeshPathSearchStartPos;
					reqStruct.m_vPathEnd = m_vNavMeshPathSearchEndPos;
					reqStruct.m_fReferenceDistance = m_fNavMeshPathReferenceDistance;
					reqStruct.m_iFlags = PATH_FLAG_FLEE_TARGET;

					if(ms_bFavourEnclosedAreas)
						reqStruct.m_iFlags |= PATH_FLAG_FAVOUR_ENCLOSED_SPACES;
					if(m_bPreservePathHeights)
						reqStruct.m_iFlags |= PATH_FLAG_PRESERVE_SLOPE_INFO_IN_PATH;
					if(m_bNeverClimb)
						reqStruct.m_iFlags |= PATH_FLAG_NEVER_CLIMB_OVER_STUFF;
					if(m_bNeverDrop)
						reqStruct.m_iFlags |= PATH_FLAG_NEVER_DROP_FROM_HEIGHT;
					if(m_bNeverUseLadders)
						reqStruct.m_iFlags |= PATH_FLAG_NEVER_USE_LADDERS;

					iPathHandle = CPathServer::RequestPath(reqStruct);
				}
				// If 'W' is down, then calculate a 'wander' path.
				// This finds a path from the 'start' flag, trying to wander in the direction of the vector from the 'end' flag to the 'start' flag
				else if(KeyJustDown('R'))
				{
					Vector3 vWanderDir = m_vNavMeshPathSearchEndPos - m_vNavMeshPathSearchStartPos;
					vWanderDir.z = 0.0f;
					vWanderDir.Normalize();

					TRequestPathStruct reqStruct;
					reqStruct.m_vPathStart = m_vNavMeshPathSearchStartPos;
					reqStruct.m_vPathEnd = m_vNavMeshPathSearchStartPos;
					reqStruct.m_vReferenceVector = vWanderDir;
					reqStruct.m_fReferenceDistance = m_fNavMeshPathReferenceDistance;
					reqStruct.m_iFlags = (PATH_FLAG_NEVER_LEAVE_PAVEMENTS | PATH_FLAG_WANDER | PATH_FLAG_NEVER_ENTER_WATER);

					if(m_bPreservePathHeights)
						reqStruct.m_iFlags |= PATH_FLAG_PRESERVE_SLOPE_INFO_IN_PATH;
					if(m_bNeverClimb)
						reqStruct.m_iFlags |= PATH_FLAG_NEVER_CLIMB_OVER_STUFF;
					if(m_bNeverDrop)
						reqStruct.m_iFlags |= PATH_FLAG_NEVER_DROP_FROM_HEIGHT;
					if(m_bNeverUseLadders)
						reqStruct.m_iFlags |= PATH_FLAG_NEVER_USE_LADDERS;

					iPathHandle = CPathServer::RequestPath(reqStruct);
				}
				// If 'I' is down, then calculate a normal path with a single influence sphere
				// halfway between the start & end flags
				else if(KeyJustDown('I'))
				{
					Vector3 vMidPoint = (m_vNavMeshPathSearchEndPos + m_vNavMeshPathSearchStartPos) * 0.5f;
					Vector3 vRefVec(0,0,0);
					Vector3 vWanderDir = m_vNavMeshPathSearchEndPos - m_vNavMeshPathSearchStartPos;
					vWanderDir.z = 0.0f;
					vWanderDir.Normalize();

					TInfluenceSphere iSphere;
					float fInfDist = (m_vNavMeshPathSearchEndPos - m_vNavMeshPathSearchStartPos).Mag();
					fInfDist /= 4.0f;	// fInfDist is quater of the distance between the two flags
					fInfDist = Max(1.0f, fInfDist);
					//iSphere.Init(vMidPoint, fInfDist, 100.0f, 50.0f);
					iSphere.Init(vMidPoint, fInfDist, 1000.0f, 500.0f);

					u32 iFlags = 0;
					if(m_bPreservePathHeights)
						iFlags |= PATH_FLAG_PRESERVE_SLOPE_INFO_IN_PATH;
					if(m_bNeverClimb)
						iFlags |= PATH_FLAG_NEVER_CLIMB_OVER_STUFF;
					if(m_bNeverDrop)
						iFlags |= PATH_FLAG_NEVER_DROP_FROM_HEIGHT;
					if(m_bNeverUseLadders)
						iFlags |= PATH_FLAG_NEVER_USE_LADDERS;

					iPathHandle = CPathServer::RequestPath(
						m_vNavMeshPathSearchStartPos,
						m_vNavMeshPathSearchEndPos,
						vWanderDir,
						m_fNavMeshPathReferenceDistance,
						iFlags,								// styleflags
						0.0f,							// completion radius
						1, &iSphere,					// infpluence spheres
						NULL, NULL
					);
				}
				else
				{
					TRequestPathStruct reqStruct;
					reqStruct.m_iFlags = PATH_FLAG_CUT_SHARP_CORNERS | PATH_FLAG_USE_LARGER_SEARCH_EXTENTS;
					reqStruct.m_vPathStart = m_vNavMeshPathSearchStartPos;
					reqStruct.m_vPathEnd = m_vNavMeshPathSearchEndPos;
					reqStruct.m_fCompletionRadius = CNavGenApp::ms_fCompletionRadius;

					if(m_bPavements)
						reqStruct.m_iFlags |= PATH_FLAG_PREFER_PAVEMENTS;

					if(ms_bFavourEnclosedAreas)
						reqStruct.m_iFlags |= PATH_FLAG_FAVOUR_ENCLOSED_SPACES;

					if(!ms_bAvoidObjects)
						reqStruct.m_iFlags |= PATH_FLAG_DONT_AVOID_DYNAMIC_OBJECTS;
					if(ms_bReduceBBoxes)
						reqStruct.m_iFlags |= PATH_FLAG_REDUCE_OBJECT_BBOXES;
					if(ms_bIgnoreObjectsWithNoUprootLimit)
						reqStruct.m_iFlags |= PATH_FLAG_IGNORE_NON_SIGNIFICANT_OBJECTS;
					if(m_bPreservePathHeights)
						reqStruct.m_iFlags |= PATH_FLAG_PRESERVE_SLOPE_INFO_IN_PATH;
					if(m_bNeverClimb)
						reqStruct.m_iFlags |= PATH_FLAG_NEVER_CLIMB_OVER_STUFF;
					if(m_bNeverDrop)
						reqStruct.m_iFlags |= PATH_FLAG_NEVER_DROP_FROM_HEIGHT;
					if(m_bNeverUseLadders)
						reqStruct.m_iFlags |= PATH_FLAG_NEVER_USE_LADDERS;

					reqStruct.m_fMaxSlopeNavigable = (ms_fMaxAngleNavigable * DtoR);

					reqStruct.m_fEntityRadius = ms_fActorRadius;

					// 'C' requests cover from the camera position
					if(KeyJustDown('C'))
					{
						reqStruct.m_iFlags |= PATH_FLAG_USE_DIRECTIONAL_COVER;
						reqStruct.m_vCoverOrigin = m_vViewPos;
					}

 					iPathHandle = CPathServer::RequestPath(reqStruct);
				}

				if(iPathHandle == 0)
					return;

				while(!CPathServer::IsRequestResultReady(iPathHandle))
				{
					Sleep(0);
				}

				iNumRequests--;

			} while (iNumRequests >0);


			// Store off points, etc
			ProcessRequestResult(iPathHandle);


			m_bMouseLook = false;
			ConfineCursorToWindow(true);
			AcquireMouse(true);
		}

		ConfineCursorToWindow(true);
		AcquireMouse(true);

		return;
	}


	// Space toggles on & off "mouse-look"
	if(KeyJustDown(VK_SPACE))
	{
		m_bMouseLook = !m_bMouseLook;
		if(m_bMouseLook)
		{
			ConfineCursorToWindow(true);
		}
		else
		{
			ConfineCursorToWindow(false);
		}

		if(m_pMouseDevice)
		{
			if(m_bMouseLook)
			{
				AcquireMouse(true);
			}
			else
			{
				AcquireMouse(false);
			}
		}

		if(m_bMouseLook)
		{
			SetWindowText(m_Hwnd, "NavGenTest - Mouse look (SPACE to toggle)");
		}
		else
		{
			SetWindowText(m_Hwnd, "NavGenTest - View locked (SPACE to toggle)");
		}
	}

	// Holding left-shift speeds up movement x10
	float fMoveSpeed;
	if(GetKeyState(VK_LSHIFT))
	{
		fMoveSpeed = MOVE_SPEED / 10.0f;
	}
	else
	{
		fMoveSpeed = MOVE_SPEED;
	}

	if(KeyDown('W'))
	{
		m_vViewPos += m_vViewForwards * (fMoveSpeed * fSecs);
	}
	if(KeyDown('S'))
	{
		m_vViewPos -= m_vViewForwards * (fMoveSpeed * fSecs);
	}
	if(KeyDown('A'))
	{
		Vector3 vLateral = m_mViewOrientation.a;
		m_vViewPos += (vLateral * (fMoveSpeed * fSecs));
	}
	if(KeyDown('D'))
	{
		Vector3 vLateral = m_mViewOrientation.a;
		m_vViewPos -= (vLateral * (fMoveSpeed * fSecs));
	}

	// Handle cursor-keys input
	Vector3 vForwards, vUp, vRight;

	if(GetKeyState(VK_LSHIFT))
	{
		vForwards = Vector3(0.0f, 1.0f, 0.0f);
		vRight = Vector3(1.0f, 0.0f, 0.0f);
		vUp = Vector3(0.0f, 0.0f, 1.0f);
	}
	else
	{
		vForwards = Vector3(0.0f, 0.1f, 0.0f);
		vRight = Vector3(0.1f, 0.0f, 0.0f);
		vUp = Vector3(0.0f, 0.0f, 0.1f);
	}

	switch(m_CursorMode)
	{
		case eCursorMovesSelectedObject:
		{
			if(m_pSelDynObj)
			{
				if(KeyDown(VK_UP))
					((CEntity*)m_pSelDynObj->m_pEntity)->m_vPosition += vForwards;
				else if(KeyDown(VK_DOWN))
					((CEntity*)m_pSelDynObj->m_pEntity)->m_vPosition -= vForwards;
				else if(KeyDown(VK_LEFT))
					((CEntity*)m_pSelDynObj->m_pEntity)->m_vPosition -= vRight;
				else if(KeyDown(VK_RIGHT))
					((CEntity*)m_pSelDynObj->m_pEntity)->m_vPosition += vRight;
				else if(KeyDown(VK_HOME))
					((CEntity*)m_pSelDynObj->m_pEntity)->m_vPosition += vUp;
				else if(KeyDown(VK_END))
					((CEntity*)m_pSelDynObj->m_pEntity)->m_vPosition -= vUp;

				if((m_pSelDynObj->m_pEntity))
					CPathServerGta::UpdateDynamicObject((CEntity*)m_pSelDynObj->m_pEntity);
			}
			break;
		}
		case eCursorMovesStartPoint:
		{
			if(KeyDown(VK_UP))
				m_vNavMeshPathSearchStartPos += vForwards;
			else if(KeyDown(VK_DOWN))
				m_vNavMeshPathSearchStartPos -= vForwards;
			else if(KeyDown(VK_LEFT))
				m_vNavMeshPathSearchStartPos -= vRight;
			else if(KeyDown(VK_RIGHT))
				m_vNavMeshPathSearchStartPos += vRight;
			else if(KeyDown(VK_HOME))
				m_vNavMeshPathSearchStartPos += vUp;
			else if(KeyDown(VK_END))
				m_vNavMeshPathSearchStartPos -= vUp;
			break;
		}
		case eCursorMovesEndPoint:
		{
			if(KeyDown(VK_UP))
				m_vNavMeshPathSearchEndPos += vUp;
			else if(KeyDown(VK_DOWN))
				m_vNavMeshPathSearchEndPos -= vUp;
			else if(KeyDown(VK_LEFT))
				m_vNavMeshPathSearchEndPos -= vRight;
			else if(KeyDown(VK_RIGHT))
				m_vNavMeshPathSearchEndPos += vRight;
			else if(KeyDown(VK_HOME))
				m_vNavMeshPathSearchEndPos += vUp;
			else if(KeyDown(VK_END))
				m_vNavMeshPathSearchEndPos -= vUp;
			break;
		}
		default:
			break;
	}


	// Test the heap
	if(KeyJustDown('H'))
	{
		atBinHeap<float, int> testHeap(1024);
		float fKey;
		int iData;

		testHeap.Insert(1.0f, 1);

		testHeap.ExtractMin(fKey, iData);

		testHeap.Insert(2.0f, 2);
		testHeap.Insert(3.0f, 3);
	}

	// If we've got a nav-mesh triangle selected, and we press 'F' then
	// do a flood-fill from that triangle.  All triangles visited will be
	// marked as "NAVSURFTRI_ACCESSIBLE".
	if(KeyJustDown('L'))
	{
		TPathHandle iPathHandle = CPathServer::RequestLineOfSight(m_vNavMeshPathSearchStartPos, m_vNavMeshPathSearchEndPos, 0.0f);

		if(iPathHandle == 0)
			return;

		while(!CPathServer::IsRequestResultReady(iPathHandle))
		{
			Sleep(0);
		}

		bool bLosResult;
		CPathServer::GetLineOfSightResult(iPathHandle, bLosResult);

		sprintf(tmpBuf, "LOS exists : %i\n", (int)bLosResult);
		OutputDebugString(tmpBuf);
	}

	if(KeyJustDown('X'))
	{
		m_BugriddenNavTris.clear();

		if(m_NodeGenerator.m_Octree.RootNode())
		{
			FindBugriddenNavSurfaceTris();
		}
	}

	if(KeyJustDown('1'))
	{
		m_bDrawOctreeBounds = !m_bDrawOctreeBounds;
	}
	if(KeyJustDown('2'))
	{
		m_bDrawCollisionPolys = !m_bDrawCollisionPolys;
	}
	if(KeyJustDown('3'))
	{
		m_bDrawSplitPolys = !m_bDrawSplitPolys;
	}
	if(KeyJustDown('4'))
	{
		m_bDrawPathNodes = !m_bDrawPathNodes;
	}
	if(KeyJustDown('5'))
	{
		m_bDrawNavigationSurfaces = !m_bDrawNavigationSurfaces;
	}
	if(KeyJustDown('6'))
	{
		m_bToggleNavigationSurfaceSolid = !m_bToggleNavigationSurfaceSolid;
	}
	if(KeyJustDown('7'))
	{
		m_bDisplayPathServerMesh = !m_bDisplayPathServerMesh;
	}
	if(KeyJustDown('8'))
	{
		m_bPrintRenderInfo = !m_bPrintRenderInfo;
	}

	if(KeyJustDown(VK_F9) || KeyJustDown(VK_F8))
	{
		ConfineCursorToWindow(false);
		AcquireMouse(false);

		bool bMainMap = KeyJustDown(VK_F9);
		char * pPath = NULL;
		
		if(bMainMap)
		{
			pPath = SelectFileToOpen("Select navmeshes folder for main map..", "*.inv");
		}
		else
		{
			pPath = SelectFileToOpen("Select dynamic navmesh to load..", "*.inv");
		}

		if(!pPath)
		{
			m_navMeshesFolder[0] = 0;
		}
		else
		{
			strcpy(m_navMeshesFolder, pPath);

			char * ptr = &m_navMeshesFolder[ strlen(m_navMeshesFolder)-1 ];
			while(ptr != m_navMeshesFolder && *ptr != '\\' && *ptr != '/')
			{
				ptr--;
			}
			if(*ptr == '\\' || *ptr == '/')
			{
				*ptr = 0;
			}
		}

		if(m_navMeshesFolder[0])
		{
			if(!m_bPathServerInitialised)
			{
				//CPathServer::Init(s_PathServerGameInterface, CPathServer::EMultiThreaded, 0, NULL, m_gameDataFolder, m_navMeshesFolder : NULL);

				CPathServer::Init(s_PathServerGameInterface, CPathServer::EMultiThreaded, 0, NULL, m_gameDataFolder, NULL);

				if(bMainMap)
				{
					CPathServer::LoadAllMeshes(m_navMeshesFolder);
				}
				else
				{
					m_DynamicNavMesh = LoadDynamicNavMesh(pPath);
				}

				m_bPathServerInitialised = true;

				MakeVertexBuffersForAllNavMeshes();
			}
			else
			{
				//CPathServer::LoadAllMeshes(pPath);
			}

			Vector3 vNewPos(0,0,0);

			for(s32 n=0; n<CPathServer::m_pNavMeshStores[kNavDomainRegular]->GetMaxMeshIndex(); n++)
			{
				if(CPathServer::m_pNavMeshStores[kNavDomainRegular]->GetMeshByIndex(n))
				{
					CNavMesh * pNavMesh = CPathServer::m_pNavMeshStores[kNavDomainRegular]->GetMeshByIndex(n);
					vNewPos = (pNavMesh->GetQuadTree()->m_Mins + pNavMesh->GetQuadTree()->m_Maxs) * 0.5f;
 					vNewPos.z = pNavMesh->GetQuadTree()->m_Mins.z;
					break;
				}
			}
			m_vViewPos = vNewPos;
		}

		ConfineCursorToWindow(true);
		AcquireMouse(true);
	}

	if(KeyJustDown(VK_F11))
	{
		LoadPathProbFile();
	}

	/*
	if(KeyJustDown('C'))
	{
 		ConfineCursorToWindow(false);
		AcquireMouse(false);
#ifdef NAVGEN_SPEW_DEBUG
		m_NodeGenerator.SanityCheckEverything();
#endif
		m_bNavSurfaceHasChanged = true;

		ConfineCursorToWindow(true);
		AcquireMouse(true);
	}
	*/
}


void CNavGenApp::CreateGroundPlane()
{
	Vector2 vAreaMin(-900.0f, -900.0f);
	Vector2 vAreaMax(-750.0f, -750.0f);
	Vector2 vPlaneMin(-850.0f, -850.0f);
	Vector2 vPlaneMax(-800.0f, -800.0f);
	float fPlaneZ = 0.0f;

	g_App->m_vSectorMins = Vector3(vAreaMin.x, vAreaMin.y, -500.0f);
	g_App->m_vSectorMaxs = Vector3(vAreaMax.x, vAreaMax.y, 510.0f);
	g_App->m_vWorldMin = Vector3(vPlaneMin.x, vPlaneMin.y, -500.0f);
	g_App->m_vWorldMax = Vector3(vPlaneMax.x, vPlaneMax.y, 510.0f);
	g_App->m_iNumSectorsX = 3;
	g_App->m_iNumSectorsY = 3;
	g_App->m_iSectorStartX = 102;
	g_App->m_iSectorStartY = 102;

	g_App->m_NodeGenerator.CreateGroundPlane(vAreaMin, vAreaMax, vPlaneMin, vPlaneMax, fPlaneZ);
	g_App->m_NodeGenerator.CreateGroundPlane(vAreaMin, vAreaMax, vPlaneMin, vPlaneMax, fPlaneZ+30.0f);

	Vector2 vMapSectorsMin(-6000.0f, -6000.0f);
	g_App->m_vViewPos = (g_App->m_vWorldMin + g_App->m_vWorldMax) * 0.5f;

	g_App->m_iSelectedTri = -1;
	g_App->m_pSelectedNavSurfaceTri = NULL;
	g_App->m_pDestinationNavSurfaceTri = NULL;
	g_App->m_PathTris.clear();


	CNavResolutionArea * pResArea = new CNavResolutionArea();
	pResArea->m_vMin = Vector3(-835.0f, -835.0f, fPlaneZ - 10.0f);
	pResArea->m_vMax = Vector3(-825.0f, -825.0f, fPlaneZ + 10.0f);
	pResArea->m_fMultiplier = 0.25f;
	g_App->m_NodeGenerator.m_ResolutionAreasWholeMap.push_back(pResArea);

	pResArea = new CNavResolutionArea();
	pResArea->m_vMin = Vector3(-830.0f, -830.0f, fPlaneZ - 10.0f);
	pResArea->m_vMax = Vector3(-820.0f, -820.0f, fPlaneZ + 10.0f);
	pResArea->m_fMultiplier = 0.5f;
	g_App->m_NodeGenerator.m_ResolutionAreasWholeMap.push_back(pResArea);
}

void CNavGenApp::LoadResolutionAreas()
{
	g_App->ConfineCursorToWindow(false);
	g_App->AcquireMouse(false);

	char * pFileName = g_App->SelectFileToOpen("Select res-areas file to open", "XML files\0*.xml\0");

	if(pFileName)
	{
		CNavGen::m_ResolutionAreasWholeMap.clear();
		CNavGen::LoadCustomResolutionAreas(pFileName);
	}
	g_App->ConfineCursorToWindow(true);
	g_App->AcquireMouse(true);
}


void CNavGenApp::LoadCollisionTris()
{
	g_App->ConfineCursorToWindow(false);
	g_App->AcquireMouse(false);

	//**************************************
	// Prompt for a .tri/.bnd file to load
	//**************************************

	char * pFileName = g_App->SelectFileToOpen("Select a collision file to open", "Triangle files\0*.tri\0phBound files\0*.bnd\0");

	if(pFileName)
	{
		char * ptr = pFileName;
		while(*ptr && *ptr != '.')
		{
			g_App->m_NameOfCurrentFile[ptr-pFileName] = *ptr;
			ptr++;
		}
		g_App->m_NameOfCurrentFile[ptr-pFileName] = 0;

		//**************************************
		// Load the list of collision triangles
		//**************************************

		OutputDebugString("\n");
		OutputDebugString("//********************************************************\n");
		OutputDebugString("// Loading collision tris\n");

		Vector2 vMapSectorsMin;
		// Load triangles
		bool ok = g_App->m_NodeGenerator.LoadCollisionTriangles(
			pFileName,
			g_App->m_vSectorMins, g_App->m_vSectorMaxs,
			g_App->m_iSectorStartX, g_App->m_iSectorStartY,
			g_App->m_iNumSectorsX, g_App->m_iNumSectorsY,
			g_App->m_vWorldMin, g_App->m_vWorldMax,
			vMapSectorsMin
		);

		if(ok)
		{
			g_App->m_vViewPos = (g_App->m_vWorldMin + g_App->m_vWorldMax) * 0.5f;
		}

		sprintf(tmpBuf, "// Number of collision triangles : %i\n", g_App->m_NodeGenerator.m_CollisionTriangles.size());
		OutputDebugString(tmpBuf);

		OutputDebugString("//********************************************************\n");
		OutputDebugString("\n");
	}

	g_App->m_iSelectedTri = -1;
	g_App->m_pSelectedNavSurfaceTri = NULL;
	g_App->m_pDestinationNavSurfaceTri = NULL;
	g_App->m_PathTris.clear();

	g_App->ConfineCursorToWindow(true);
	g_App->AcquireMouse(true);
}

void CNavGenApp::LoadExtrasInfo()
{
	g_App->ConfineCursorToWindow(false);
	g_App->AcquireMouse(false);

	//**************************************
	// Prompt for a .tri/.bnd file to load
	//**************************************

	char * pFileName = g_App->SelectFileToOpen("Select a navgen extras file to open", "Data files\0*.dat\0\0");

	if(pFileName)
	{
		CNavGen::ParseExtraInfoFile(pFileName, &g_App->m_ExtraInfo.m_LaddersList, &g_App->m_ExtraInfo.m_CarNodesList, &g_App->m_ExtraInfo.m_PortalBoundaries, &g_App->m_ExtraInfo.m_DynamicEntityBounds);
	}

	g_App->ConfineCursorToWindow(true);
	g_App->AcquireMouse(true);
}

CNavMesh * CNavGenApp::LoadDynamicNavMesh(const char * pFilename)
{
	g_App->m_DynamicNavMesh = CNavMesh::LoadBinary(pFilename);
	if(g_App->m_DynamicNavMesh)
	{
		Assert(g_App->m_DynamicNavMesh->GetIndexOfMesh()==NAVMESH_INDEX_FIRST_DYNAMIC);
	}
	return g_App->m_DynamicNavMesh;
}

void CNavGenApp::LoadAuthoredAdjacencies()
{
	g_App->ConfineCursorToWindow(false);
	g_App->AcquireMouse(false);

	char * pFileName = g_App->SelectFileToOpen("Select 3dsMax ASE file to open", "ASE files\0*.ase\0");

	if(pFileName)
	{
		g_App->m_AuthoredAdjacencyPositions.clear();
		CNavGen::Load3dsMaxAsciiMarkup(pFileName, g_App->m_AuthoredAdjacencyPositions);
	}
	g_App->ConfineCursorToWindow(true);
	g_App->AcquireMouse(true);
}


void CNavGenApp::CreateOctreeCB()
{
	if(!g_App->m_NodeGenerator.m_Octree.RootNode())
	{
		g_App->ConfineCursorToWindow(false);
		g_App->AcquireMouse(false);

		// Mark which triangles are walkable, base only upon surface orientation
		g_App->m_NodeGenerator.FlagWalkableTris();

		// Free octree, and compile collision tris into a new octree
		g_App->CreateOctree();

		g_App->m_iSelectedTri = -1;
		g_App->m_pSelectedNavSurfaceTri = NULL;
		g_App->m_pDestinationNavSurfaceTri = NULL;
		g_App->m_PathTris.clear();

		g_App->ConfineCursorToWindow(true);
		g_App->AcquireMouse(true);
	}
}

void CNavGenApp::AddTestResolutionArea()
{
	float fSize = 10.0f;
	float fRes = 0.25f;

	CNavResolutionArea * pResArea = new CNavResolutionArea();
	pResArea->m_vMin = g_App->m_vViewPos - Vector3(fSize,fSize,fSize);
	pResArea->m_vMax = g_App->m_vViewPos + Vector3(fSize,fSize,fSize);
	pResArea->m_fMultiplier = fRes;
	g_App->m_NodeGenerator.m_ResolutionAreasWholeMap.push_back(pResArea);
}

void CNavGenApp::SampleAndTriangulate()
{
	g_App->ConfineCursorToWindow(false);
	g_App->AcquireMouse(false);

	//CNavGen::ms_bRemoveTrisBehindWalls = true;

	if(m_bUseTestSlicePlane)
	{
		CSlicePlane * pPlane = new CSlicePlane();
		pPlane->m_vNormal = Vector3(0.0f,0.0f,1.0f);
		pPlane->m_fDist = -m_fTestSlicePlaneZ;
		g_App->m_NodeGenerator.m_SlicePlanes.push_back(pPlane);
	}

	g_App->m_NodeGenerator.GenerateNavSurface(NULL, 0, true);

	char textBuf[256];
	sprintf(textBuf, "Took %.2f secs (nodes : %.2f) (tris : %.2f)\n",
		g_App->m_NodeGenerator.m_fTimeTakenToPlaceNodes + g_App->m_NodeGenerator.m_fTimeTakenToTriangulate,
		g_App->m_NodeGenerator.m_fTimeTakenToPlaceNodes,
		g_App->m_NodeGenerator.m_fTimeTakenToTriangulate
	);
	OutputDebugString(textBuf);
	OutputDebugString("\n");

	//g_App->m_NodeGenerator.RemoveSteepSlopeJaggies();

	g_App->m_bNavSurfaceHasChanged = true;

	g_App->m_iSelectedTri = -1;
	g_App->m_pSelectedNavSurfaceTri = NULL;
	g_App->m_pDestinationNavSurfaceTri = NULL;
	g_App->m_PathTris.clear();

	g_App->ConfineCursorToWindow(true);
	g_App->AcquireMouse(true);
}

void CNavGenApp::GenerateUsingVoxelMap()
{
	g_App->m_NodeGenerator.GenerateNavSurface_VoxelMap();

	g_App->CreateVertexBufferForVoxelMap();
}

void CNavGenApp::OptimiseNavMesh()
{
	char filePath[1024];
	strcpy(filePath, g_App->m_FileName);
	StripFilename(filePath);

	char datFile[1024];
	sprintf(datFile, "%s/Extras_%i_%i.dat", filePath, g_App->m_iSectorStartX, g_App->m_iSectorStartY);

	g_App->ConfineCursorToWindow(false);
	g_App->AcquireMouse(false);

	bool bQuit = false;
	bool bSinglePass = false;

	while(!bQuit)
	{
		OutputDebugString("//***********************************************\n");
		sprintf(tmpBuf, "// Optimising Nav-Surface\n");
		OutputDebugString(tmpBuf);
		OutputDebugString("//***********************************************\n");

		g_App->m_NodeGenerator.m_bDontMoveNonConnectedFaces = false;

		int numTimesZero = 0;

		do
		{
			g_App->m_NodeGenerator.OptimiseNavSurface();

			if(g_App->m_NodeGenerator.m_iNumEdgesCollapsedThisPass == 0)
			{
				numTimesZero++;
			}
			else
			{
				numTimesZero = 0;
			}
			if(bSinglePass)
			{
				bQuit = true;
				break;
			}

		} while(numTimesZero < 2);


		OutputDebugString("//***********************************************\n");
		sprintf(tmpBuf, "// Optimising edges (tri fans)\n");
		OutputDebugString(tmpBuf);
		OutputDebugString("//***********************************************\n");

		int iTrisBefore = g_App->m_NodeGenerator.m_iOptimisedTriangleCount;

		g_App->m_NodeGenerator.OptimiseEdges();

		int iTrisAfter = g_App->m_NodeGenerator.m_iOptimisedTriangleCount;

		if(iTrisBefore == iTrisAfter)
			bQuit = true;
	}

	g_App->m_NodeGenerator.CullTrianglesBehindWallsAfterOptimisation();

	g_App->m_NodeGenerator.CleanBadData();
	g_App->m_bNavSurfaceHasChanged = true;

	g_App->m_iSelectedTri = -1;
	g_App->m_pSelectedNavSurfaceTri = NULL;
	g_App->m_pDestinationNavSurfaceTri = NULL;
	g_App->m_PathTris.clear();

	g_App->ConfineCursorToWindow(true);
	g_App->AcquireMouse(true);

	OutputDebugString("//***********************************************\n");
	sprintf(tmpBuf, "// Num triangles after optimisation : %i\n", g_App->m_NodeGenerator.m_iOptimisedTriangleCount);
	OutputDebugString(tmpBuf);
	OutputDebugString("//***********************************************\n");
}

void
CNavGenApp::RelaxVertexPositions()
{
	g_App->ConfineCursorToWindow(false);
	g_App->AcquireMouse(false);

	static const float fThreshold = 0.01f;

	while(1)
	{
		float fMag = g_App->m_NodeGenerator.RelaxVertexPositions();
		printf("RelaxVertexPositions - minimum move was %.3f\n", fMag);

		if(fMag < fThreshold)
			break;
	}

	g_App->m_bNavSurfaceHasChanged = true;
}


void CNavGenApp::CalcFreeSpaceBeyondPolyEdges()
{
	//CNavGen_NavMeshStore::LoadAll(g_App->m_navMeshesFolder);

	u32 p;
	for(p=0; p<=NAVMESH_MAX_MAP_INDEX; p++)
	{
		CNavGen_NavMeshStore::Set( p, CPathServer::GetNavMeshFromIndex(p, kNavDomainRegular) );
	}

	for(p=0; p<=NAVMESH_MAX_MAP_INDEX; p++)
	{
		CNavMesh * pStoreNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(p);
		if(pStoreNavMesh)
		{
			CNavGen::CalcFreeSpaceBeyondPolyEdges(pStoreNavMesh);
			CNavGen::CalcFreeSpaceAroundVertices(pStoreNavMesh, NULL, NULL, NULL, NULL);

			/*
			CNavMesh * pPathServerNavMesh = CPathServer::GetNavMeshFromIndex(p, kNavDomainRegular);
			if(pPathServerNavMesh)
			{
				Assert(pPathServerNavMesh->GetSizeOfPools()==pStoreNavMesh->GetSizeOfPools());
				for(s32 i=0; i<pStoreNavMesh->GetSizeOfPools(); i++)
				{
					pPathServerNavMesh->m_AdjacentPolyPool[i].SetFreeSpaceBeyondEdge( pStoreNavMesh->m_AdjacentPolyPool[i].GetFreeSpaceBeyondEdge() );
					pPathServerNavMesh->m_AdjacentPolyPool[i].SetFreeSpaceAroundVertex( pStoreNavMesh->m_AdjacentPolyPool[i].GetFreeSpaceAroundVertex() );
				}
			}
			*/
		}
	}

	//CNavGen_NavMeshStore::UnloadAll();
}

void CNavGenApp::MoveVertsToSmoothPavementEdges()
{
	if(g_App->m_pSelectedNavSurfaceTri)
		g_App->m_NodeGenerator.MoveVertsToSmoothPavementEdges(g_App->m_pSelectedNavSurfaceTri);

	g_App->m_bNavSurfaceHasChanged = true;
}

void CNavGenApp::MoveVertsToSmoothJaggies()
{
	if(g_App->m_pSelectedNavSurfaceTri)
		g_App->m_NodeGenerator.MoveVertsToSmoothJaggies(g_App->m_pSelectedNavSurfaceTri);

	g_App->m_bNavSurfaceHasChanged = true;
}

void CNavGenApp::AntiAliasBoundaryEdges()
{
	CNavAntiAlias navAA(&g_App->m_NodeGenerator);
	navAA.AntiAliasBoundaryEdges();

	g_App->m_bNavSurfaceHasChanged = true;
}

void CNavGenApp::AntiAliasPavementEdges()
{
	CNavAntiAlias navAA(&g_App->m_NodeGenerator);
	navAA.AntiAliasPavementEdges();

	g_App->m_bNavSurfaceHasChanged = true;
}

void CNavGenApp::AntiAliasSteepSlopeEdges()
{
	CNavAntiAlias navAA(&g_App->m_NodeGenerator);
	navAA.AntiAliasSteepSlopeEdges();

	g_App->m_bNavSurfaceHasChanged = true;
}


void
CNavGenApp::ClipPolyUnderStartMarker()
{
	ClipPolyUnderPosition(g_App->m_vNavMeshPathSearchStartPos);
}

void
CNavGenApp::ClipPolyUnderfoot()
{
	ClipPolyUnderPosition(g_App->m_vViewPos);
}

void
CNavGenApp::ClipPolyUnderPosition(const Vector3 & vInputPos)
{
	vInputPos;
#if 0	// currently commented out in game, but don't delete this code!

	// Raise up a little, because marker positions may be exactly on the navmesh
	Vector3 vIsectPos;
	Vector3 vPos = vInputPos;
	vPos.z += 1.0f;

	int iMeshIndex = CPathServer::GetNavMeshIndexFromPosition(vPos);
	CNavMesh * pNavMesh = CPathServer::GetNavMeshFromIndex(iMeshIndex);

	if(pNavMesh)
	{
		TNavMeshPoly * pPoly = NULL;
		int iPolyIndex = pNavMesh->GetPolyBelowPoint(vPos, vIsectPos, 10.0f);

		if(iPolyIndex != NAVMESH_POLY_INDEX_NONE)
		{
			pPoly = &pNavMesh->m_Polys[iPolyIndex];

			if(pPoly->m_Flags & NAVMESHPOLY_REPLACED_BY_TESSELLATION)
			{
				pPoly = NULL;
				pNavMesh = CPathServer::m_pTessellationNavMesh;
				iPolyIndex = pNavMesh->GetPolyBelowPoint(vPos, vIsectPos, 10.0f);

				if(iPolyIndex != NAVMESH_POLY_INDEX_NONE)
				{
					pPoly = &pNavMesh->m_Polys[iPolyIndex];
				}
			}
		}

		if(pNavMesh && pPoly && !pPoly->GetIsSmall())
		{
			TAdjPoly prevPolyInfo, tessPolyInfo;

			Vector3 vPolyPts[NAVMESHPOLY_MAX_NUM_VERTICES];
			for(int v=0; v<pPoly->GetNumVertices(); v++)
			{
				pNavMesh->GetVertex(pNavMesh->GetPolyVertexIndex(pPoly, v), vPolyPts[v]);
			}

			bool bOk = CPathServer::m_PathServerThread.ClipNavMeshPolyToDynamicObjects(pNavMesh, pPoly, vPolyPts);

	#if SANITY_CHECK_TESSELLATION
			CPathServer::SanityCheckPolyConnectionsForAllNavMeshes();
	#endif

			// And now we want to rebuild all the navmesh vertex buffers for navmeshes, and
			// also for the CPathServer::m_pTessellationNavMesh - if we did any tessellation.
			g_App->MakeNavMesh_VertexBuffers(
				CPathServer::m_pTessellationNavMesh,
				g_App->m_pNavMeshTrianglesVB[NAVMESH_INDEX_TESSELLATION], g_App->m_iNavMeshNumTriangles[NAVMESH_INDEX_TESSELLATION],
				g_App->m_pNavMeshLinesVB[NAVMESH_INDEX_TESSELLATION], g_App->m_iNavMeshNumLines[NAVMESH_INDEX_TESSELLATION],
				g_App->m_pNavMeshAdjLinesVB[NAVMESH_INDEX_TESSELLATION], g_App->m_iNavMeshNumAdjLines[NAVMESH_INDEX_TESSELLATION],
				g_App->m_pNavMeshPrecalcPathLinesVB[NAVMESH_INDEX_TESSELLATION], g_App->m_iNavMeshPrecalcPathNumLines[NAVMESH_INDEX_TESSELLATION],
				true
			);
		}
	}
	#endif
}

void CNavGenApp::ExpandObjects()
{
	const float fRadiusDelta = CNavGenApp::ms_fActorRadius-PATHSERVER_PED_RADIUS;
	TShortMinMax dummyMinMax;
	CDynamicObjectsContainer::AdjustAllBoundsByAmount(dummyMinMax, fRadiusDelta, true);
}
void CNavGenApp::ContractObjects()
{
	const float fRadiusDelta = PATHSERVER_PED_RADIUS-CNavGenApp::ms_fActorRadius;
	TShortMinMax dummyMinMax;
	CDynamicObjectsContainer::AdjustAllBoundsByAmount(dummyMinMax, fRadiusDelta, false);
}

void CNavGenApp::RenderSpaceBeyondPolygonEdges()
{
	int iMeshIndex = CPathServer::GetNavMeshIndexFromPosition(m_vViewPos, kNavDomainRegular);
	CNavMesh * pNavMesh = CPathServer::GetNavMeshFromIndex(iMeshIndex, kNavDomainRegular);
	RenderSpaceBeyondPolygonEdges(pNavMesh);
	RenderSpaceBeyondPolygonEdges(fwPathServer::GetTessellationNavMesh());
}
void CNavGenApp::RenderSpaceBeyondPolygonEdges(CNavMesh * pNavMesh)
{
	const float fRangeSqr = 25.0f*25.0f;
	Vector3 vCentroid, vMidEdge, vPos;
	D3DXVECTOR3 vVertexPos, vScreenPos;
	DWORD col1 = D3DCOLOR_RGBA(255,255,30,255);
	DWORD col2 = D3DCOLOR_RGBA(60,60,255,255);
	char txt[64];

	Vector3 vMin,vMax;

	if(pNavMesh)
	{
		for(s32 p=0; p<pNavMesh->GetNumPolys(); p++)
		{
			TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);
			if(pNavMesh->GetIndexOfMesh()==NAVMESH_INDEX_TESSELLATION &&
				(!pPoly->GetIsTessellatedFragment() || pPoly->GetReplacedByTessellation() || pPoly->GetIsDegenerateConnectionPoly()))
				continue;

			pPoly->m_MinMax.GetAsFloats(vMin,vMax);
			if(!BoxIntersectsFrustum(vMin,vMax))
				continue;

			pNavMesh->GetPolyCentroid(p, vCentroid);
			const float fMag2 = (m_vViewPos - vCentroid).XYMag2();

			if(fMag2 > fRangeSqr)
				continue;

			if(m_bDrawFreeSpaceBeyondPolyEdges)
			{
				for(u32 v=0; v<pPoly->GetNumVertices(); v++)
				{
					const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly(pPoly->GetFirstVertexIndex() + v);
					pNavMesh->GetPolyEdgeMidpoint(vMidEdge, pPoly, v);

					vPos = (vCentroid + vMidEdge) * 0.5f;
					vVertexPos.x = vPos.x;
					vVertexPos.y = vPos.y;
					vVertexPos.z = vPos.z;

					const float fSpace = (float)adjPoly.GetFreeSpaceBeyondEdge();
					sprintf(txt, "%.1f", fSpace);
					D3DXVec3Project(&vScreenPos, &vVertexPos, &m_D3DViewPort, &m_ProjectionMatrix, &m_ViewMatrix, NULL);

					DrawString((int)vScreenPos.x, (int)vScreenPos.y, txt, col1);
				}
			}
			if(m_bDrawFreeSpaceAroundVertices)
			{
				for(s32 v=0; v<pPoly->GetNumVertices(); v++)
				{
					const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly(pPoly->GetFirstVertexIndex() + v);

					u32 vi = pNavMesh->GetPolyVertexIndex(pPoly, v);
					pNavMesh->GetVertex(vi, vPos);
					vVertexPos.x = vPos.x;
					vVertexPos.y = vPos.y;
					vVertexPos.z = vPos.z;

					const float fSpace = (float)adjPoly.GetFreeSpaceAroundVertex();
					sprintf(txt, "%i) %.1f", vi, fSpace);
					D3DXVec3Project(&vScreenPos, &vVertexPos, &m_D3DViewPort, &m_ProjectionMatrix, &m_ViewMatrix, NULL);

					DrawString((int)vScreenPos.x, (int)vScreenPos.y, txt, col2);

					DrawSphere(vPos + (ZAXIS * 0.125f), fSpace, col2);
				}
			}
		}
	}
}

void CNavGenApp::DetessellateNow()
{
	CPathServer::DetessellateAllPolys();

	g_App->MakeVertexBuffersForAllNavMeshes();
	g_App->MakeVertexBufferForTessellationNavMesh();
}

void
CNavGenApp::TessellatePolyUnderStartMarker()
{
	TessellatePolyUnderPosition(g_App->m_vNavMeshPathSearchStartPos);
}

void
CNavGenApp::TessellatePolyUnderfoot()
{
	TessellatePolyUnderPosition(g_App->m_vViewPos);
}

void
CNavGenApp::TessellatePolyUnderPosition(const Vector3 & vInputPos)
{
	// Raise up a little, because marker positions may be exactly on the navmesh
	Vector3 vIsectPos;
	Vector3 vPos = vInputPos;
	vPos.z += 1.0f;

	int iMeshIndex = CPathServer::GetNavMeshIndexFromPosition(vPos, kNavDomainRegular);
	CNavMesh * pNavMesh = CPathServer::GetNavMeshFromIndex(iMeshIndex, kNavDomainRegular);

	if(pNavMesh)
	{
		TNavMeshPoly * pPoly = NULL;
		int iPolyIndex = pNavMesh->GetPolyBelowPoint(vPos, vIsectPos, 10.0f);

		if(iPolyIndex != NAVMESH_POLY_INDEX_NONE)
		{
			pPoly = pNavMesh->GetPoly(iPolyIndex);

			if(pPoly->TestFlags(NAVMESHPOLY_REPLACED_BY_TESSELLATION))
			{
				pPoly = NULL;
				pNavMesh = CPathServer::m_pTessellationNavMesh;
				iPolyIndex = pNavMesh->GetPolyBelowPoint(vPos, vIsectPos, 10.0f);

				if(iPolyIndex != NAVMESH_POLY_INDEX_NONE)
				{
					pPoly = pNavMesh->GetPoly(iPolyIndex);
				}
			}
		}
		if(pNavMesh && pPoly && !pPoly->GetIsSmall())
		{
			TAdjPoly prevPolyInfo, tessPolyInfo;

			bool bOk = CPathServer::m_PathServerThread.TessellateNavMeshPolyAndCreateDegenerateConnections(
				pNavMesh,
				pPoly,
				iPolyIndex,
				false, false,
				NULL, NAVMESH_NAVMESH_INDEX_NONE, NAVMESH_POLY_INDEX_NONE
			);
			bOk;

#if SANITY_CHECK_TESSELLATION
			CPathServer::SanityCheckPolyConnectionsForAllNavMeshes();
#endif

			// And now we want to rebuild all the navmesh vertex buffers for navmeshes, and
			// also for the CPathServer::m_pTessellationNavMesh - if we did any tessellation.
			g_App->MakeVertexBufferForTessellationNavMesh();
		}
	}
}

void
CNavGenApp::PrintStartMarkerPos()
{
	char tmp[256];
	sprintf(tmp, "(%.2f, %.2f, %.2f)\n", g_App->m_vNavMeshPathSearchStartPos.x, g_App->m_vNavMeshPathSearchStartPos.y, g_App->m_vNavMeshPathSearchStartPos.z);
	OutputDebugString(tmp);
}


void CNavGenApp::DebugNavMeshPolyUnderStartMarker()
{
	DebugNavMeshPolyUnderPosition(g_App->m_vNavMeshPathSearchStartPos);
}

void CNavGenApp::DebugNavMeshPolyUnderfoot()
{
	DebugNavMeshPolyUnderPosition(g_App->m_vViewPos);
}

void CNavGenApp::DebugNavMeshPolyByAddress()
{
	u32 iPolyAddress = 0;
	if( sscanf(ms_DebugPolyEditWidget, "%x", &iPolyAddress) == 1)
	{
		TNavMeshPoly * pPoly = reinterpret_cast<TNavMeshPoly*>(iPolyAddress);

		ms_pDebugPoly = pPoly;

		if(pPoly != NULL)
		{
			u32 iMeshIndex = pPoly->GetNavMeshIndex();
			if((iMeshIndex >= 0 && iMeshIndex < NAVMESH_MAX_MAP_INDEX) || iMeshIndex==NAVMESH_INDEX_TESSELLATION)
			{
				CNavMesh * pNavMesh = CPathServer::GetNavMeshFromIndex(iMeshIndex, kNavDomainRegular);
				DebugNavMeshPoly(pPoly, pNavMesh);

				if(m_bDumpDebugPolyTraceback)
				{
					pPoly = ms_pDebugPoly;
					while(pPoly)
					{
						iMeshIndex = pPoly->GetNavMeshIndex();
						if((iMeshIndex >= 0 && iMeshIndex < NAVMESH_MAX_MAP_INDEX) || iMeshIndex==NAVMESH_INDEX_TESSELLATION)
						{
							pNavMesh = CPathServer::GetNavMeshFromIndex(iMeshIndex, kNavDomainRegular);

							OutputDebugString("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^\n");
							DebugNavMeshPoly(pPoly, pNavMesh);

							//u32 iPolyIndex = pNavMesh->GetPolyIndex(pPoly);
							//sprintf(tmp, "Poly: (0x%X) NavMesh:%i, PolyIndex:%i, NumVerts: %i\n", ((u32)pPoly), pNavMesh->m_iIndexOfMesh, iPolyIndex, pPoly->GetNumVertices());
							//OutputDebugString(tmp);
						}

						pPoly = pPoly->GetPathParentPoly();
					}
				}
			}
		}
	}
}


void CNavGenApp::DebugNavMeshPolyUnderPosition(const Vector3 & vInputPos)
{
	char tmp[256];
	sprintf(tmp, "-----------------------------------------------------------------------------------------------\n");
	OutputDebugString(tmp);

	// Raise up a little, because marker positions may be exactly on the navmesh
	Vector3 vIsectPos;
	Vector3 vPos = vInputPos;
	vPos.z += 1.0f;

	CNavMesh * pNavMesh = NULL;
	if(g_App->m_DynamicNavMesh)
	{
		pNavMesh = g_App->m_DynamicNavMesh;
	}
	else
	{
		int iMeshIndex = CPathServer::GetNavMeshIndexFromPosition(vPos, kNavDomainRegular);
		pNavMesh = CPathServer::GetNavMeshFromIndex(iMeshIndex, kNavDomainRegular);
	}

	if(pNavMesh)
	{
		TNavMeshPoly * pPoly = NULL;
		int iPolyIndex = pNavMesh->GetPolyBelowPoint(vPos, vIsectPos, 10.0f);

		if(iPolyIndex != NAVMESH_POLY_INDEX_NONE)
		{
			pPoly = pNavMesh->GetPoly(iPolyIndex);

			if(pPoly->TestFlags( NAVMESHPOLY_REPLACED_BY_TESSELLATION))
			{
				pPoly = NULL;
				pNavMesh = CPathServer::m_pTessellationNavMesh;
				iPolyIndex = pNavMesh->GetPolyBelowPoint(vPos, vIsectPos, 10.0f);

				if(iPolyIndex != NAVMESH_POLY_INDEX_NONE)
				{
					pPoly = pNavMesh->GetPoly(iPolyIndex);
				}
			}
		}
		if(pPoly && pNavMesh)
		{
			DebugNavMeshPoly(pPoly, pNavMesh, ms_bDebugSurroundingPolysToo);
		}
	}
}

void CNavGenApp::DebugNavMeshPoly(TNavMeshPoly * pPoly, CNavMesh * pNavMesh, bool bDebugSurrounding)
{
	char tmp[256];

	u32 iPolyIndex = pNavMesh->GetPolyIndex(pPoly);
	sprintf(tmp, "Poly: (0x%X) NavMesh:%i, PolyIndex:%i, NumVerts: %i\n", ((u32)pPoly), pNavMesh->GetIndexOfMesh(), iPolyIndex, pPoly->GetNumVertices());
	OutputDebugString(tmp);

	OutputDebugString("Size: ");
	if(pPoly->GetIsSmall()) OutputDebugString("(small)");
	else if(pPoly->GetIsLarge()) OutputDebugString("(large)");
	else OutputDebugString("(medium)");

	float fActualArea = pNavMesh->GetPolyArea(iPolyIndex);
	Vector3 vNormal;
	pNavMesh->GetPolyNormal(vNormal, pPoly);
	sprintf(tmp, " Actual Area: %.2f, Poly Normal: (%.2f, %.2f, %.2f), Flags: 0x%x ", fActualArea, vNormal.x, vNormal.y, vNormal.z, pPoly->GetFlags());
	OutputDebugString(tmp);

	OutputDebugString("\n");

	for(u32 v=0; v<pPoly->GetNumVertices(); v++)
	{
		Vector3 vVertex, vNextVertex;
		int iNextVert = (v+1)%pPoly->GetNumVertices();

		pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, v), vVertex);
		pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, iNextVert), vNextVertex);

		const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly(pPoly->GetFirstVertexIndex() + v);

		float fEdgeLen = (vNextVertex - vVertex).Mag();

		u32 vi = pNavMesh->GetPolyVertexIndex(pPoly, v);
		u32 iAdjNavMesh = adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes());
		u32 iAdjPoly = adjPoly.GetPolyIndex();
		u32 iOriginalAdjNavMesh = adjPoly.GetOriginalNavMeshIndex(pNavMesh->GetAdjacentMeshes());
		u32 iOriginalAdjPoly = adjPoly.GetOriginalPolyIndex();
		float fFreeSpaceBeyondEdge = adjPoly.GetFreeSpaceBeyondEdge();
		float fFreeSpaceAroundVertex = adjPoly.GetFreeSpaceAroundVertex();

		TNavMeshPoly * pAdjPoly = NULL;
		CNavMesh * pAdjNavMesh = (iAdjNavMesh == pNavMesh->GetIndexOfMesh()) ? pNavMesh : CPathServer::GetNavMeshFromIndex(iAdjNavMesh, kNavDomainRegular);
		if(pAdjNavMesh)
			pAdjPoly = pAdjNavMesh->GetPoly(iAdjPoly);

		sprintf(tmp, "%i) Vec[%i] (%.2f, %.2f, %.2f) \t\t EdgeLen(%i to %i) : %.2f, \tAdjNav : %i \tAdjPoly : %i ( 0x%X ) \tAdjType : %i (OrigAdjNav : %i, OrigAdjPoly : %i) iFlags:0x%x Space (edge:%.1f, vertex:%.1f)\n",
			v,
			vi,
			vVertex.x, vVertex.y, vVertex.z,
			v, iNextVert,
			fEdgeLen,
			iAdjNavMesh, iAdjPoly, pAdjPoly, adjPoly.GetAdjacencyType(), iOriginalAdjNavMesh, iOriginalAdjPoly,
			pAdjPoly ? pAdjPoly->GetFlags() : -1,
			fFreeSpaceBeyondEdge, fFreeSpaceAroundVertex
			);
		OutputDebugString(tmp);
	}

	if(bDebugSurrounding)
	{
		u32 a;
		for(a=0; a<pPoly->GetNumVertices(); a++)
		{
			sprintf(tmp, "Adjacent Poly %i:\n", a);
			OutputDebugString(tmp);

			const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly(pPoly->GetFirstVertexIndex() + a);

			if(adjPoly.GetAdjacencyType()==ADJACENCY_TYPE_NORMAL &&
				adjPoly.GetPolyIndex()!=NAVMESH_POLY_INDEX_NONE &&
				adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes())!=NAVMESH_NAVMESH_INDEX_NONE)
			{
				CNavMesh * pAdjNavMesh = CPathServer::GetNavMeshFromIndex(adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes()), kNavDomainRegular);
				if(pAdjNavMesh)
				{
					TNavMeshPoly * pAdjPoly = pAdjNavMesh->GetPoly(adjPoly.GetPolyIndex());
					DebugNavMeshPoly(pAdjPoly, pAdjNavMesh);
				}
			}
		}
	}
}

TNavMeshPoly * CNavGenApp::GetPolyUnderPosition(const Vector3 & vPos, const bool bIncludeTessellationNavMesh)
{
	CNavMesh * pNavMesh = NULL;
	TNavMeshPoly * pPoly = NULL;

	if(g_App->m_DynamicNavMesh)
	{
		pNavMesh = g_App->m_DynamicNavMesh;
	}
	else
	{
		int iMeshIndex = CPathServer::GetNavMeshIndexFromPosition(vPos, kNavDomainRegular);
		pNavMesh = CPathServer::GetNavMeshFromIndex(iMeshIndex, kNavDomainRegular);
	}

	if(pNavMesh)
	{
		Vector3 vIntersect;
		int iPolyIndex = pNavMesh->GetPolyBelowPoint(vPos + ZAXIS, vIntersect, 10.0f);

		if(iPolyIndex != NAVMESH_POLY_INDEX_NONE)
		{
			pPoly = pNavMesh->GetPoly(iPolyIndex);

			if(bIncludeTessellationNavMesh && pPoly->TestFlags( NAVMESHPOLY_REPLACED_BY_TESSELLATION))
			{
				pPoly = NULL;
				pNavMesh = CPathServer::m_pTessellationNavMesh;
				iPolyIndex = pNavMesh->GetPolyBelowPoint(vPos + ZAXIS, vIntersect, 10.0f);

				if(iPolyIndex != NAVMESH_POLY_INDEX_NONE)
				{
					pPoly = pNavMesh->GetPoly(iPolyIndex);
				}
			}
		}
	}
	return pPoly;
}

void CNavGenApp::TestNavMeshLOS()
{
	TNavMeshPoly * pStartPoly = GetPolyUnderPosition(g_App->m_vNavMeshPathSearchStartPos, true);
	if(pStartPoly)
	{
		TNavMeshPoly * pEndPoly = GetPolyUnderPosition(g_App->m_vNavMeshPathSearchEndPos, true);
		if(pEndPoly)
		{
			Vector3 vLosEndIsect;
			CPathServer::m_PathServerThread.IncTimeStamp();
			bool bLOS = CPathServer::m_PathServerThread.TestNavMeshLOS(
				g_App->m_vNavMeshPathSearchStartPos,
				g_App->m_vNavMeshPathSearchEndPos,
				pEndPoly, pStartPoly, NULL,
				0, kNavDomainRegular);

			if(bLOS)
			{
				OutputDebugString("LOS succeeded\n");
			}
			else
			{
				OutputDebugString("LOS failed\n");
			}
		}
	}
}

void CNavGenApp::LOSTest()
{
	TPathHandle hLos = CPathServer::RequestLineOfSight(g_App->m_vNavMeshPathSearchStartPos, g_App->m_vNavMeshPathSearchEndPos, 0.0f);
	while(!CPathServer::IsRequestResultReady(hLos))
	{
		Sleep(0);
	}

	bool bIsClear;
	char tmp[256];
	if(CPathServer::GetLineOfSightResult(hLos, bIsClear)==PATH_NO_ERROR)
	{
		sprintf(tmp, "bLos : %s\n", bIsClear ? "true" : "false");
	}
	else
	{
		sprintf(tmp, "error %i in LOS request\n");
	}
	printf(tmp);
	OutputDebugString(tmp);

	/*
	Assert(g_App->m_NodeGenerator.m_Octree.RootNode());

	Vector3 vStart( 1208.7826f, 1450.9186f, 15.936627f );
	Vector3 vEnd( 1207.7340f, 1448.1078f, 15.936627f );

	Vector3 vHitPos;
	float fHitDist;
	CNavGenTri * pHitTri = NULL;

	bool bLos = g_App->m_NodeGenerator.m_Octree.ProcessLineOfSight( vStart, vEnd, vHitPos, fHitDist, pHitTri);

	printf("bLos : %s\n", bLos ? "true" : "false");
	*/
}