#ifndef GTATYPES_H
#define GTATYPES_H

#ifndef __MASTER
#define __MASTER	0
#endif

// Rage Includes
//#include "system/new.h"
#include "vector/matrix34.h"
#include "spatialdata\sphere.h"

// GTA Includes
//#include "fwMaths/Line.h"
#include "fwMaths/Vector.h"

// NavGen Includes
#include "fwnavgen/common.h"
#include "fwnavgen/NavGen_Utils.h"

//*******************************************************************
//
//	GTATypes.h
//	Basic types & stubs for the GTA engine.  This file is included
//	by PathServer.h when running the pathfinding system outside of
//	the GTA engine.  It makes sure that basic classes are implemented
//	which are required.  This is so that the NodeGenTest app can run
//	path queries for development without having to load up the whole
//	game, etc.
//
//*******************************************************************

#ifndef NAVGEN_TOOL
#define NAVGEN_TOOL
#endif

#define REGREF
#define TIDYREF
//#define DEBUG_DRAW	1

// These are defined only to allow core files to compile
#define __GTANY	0
#define __JIMMY	1

// Make sure HACK_RDR3 has a definition, so we can use it with #if rather than #ifdef
// for any RDR3-specific code changes.
#ifndef HACK_RDR3
#define HACK_RDR3 0
#endif

// Now in common.h: /FF
//	#ifndef ASSERT
//	#define ASSERT	Assert
//	#endif

#ifndef ASSERTMSG
#define ASSERTMSG AssertMsg
#endif

namespace rage
{
typedef u16 TDynamicObjectIndex;
}

#if __DEV

//inline void CDebug::DebugLog(const char* fmt, ...) { fmt; }
//#define NAVMESH_OPTIMISATIONS()	__pragma(optimize("", off))		// PRAGMA-OPTIMIZE-ALLOW
#define VFX_OPTIMISATIONS()	__pragma(optimize("", off))			// PRAGMA-OPTIMIZE-ALLOW

#else

#define VFX_OPTIMISATIONS()

#endif

//#define RAGE_TRACK

#define aiCondErrorf


//******************************
// Stuff from Entity.h
//******************************
#define ENTITY_TYPE_VEHICLE		2
#define ENTITY_TYPE_OBJECT		4

class CEntity
{
public:
	
	CEntity(void) { m_Matrix.Identity(); }
	~CEntity(void) { }

	Vector3 m_vPosition;

	float m_fWidth;
	float m_fDepth;
	float m_fHeight;
	float m_fRotationInDegrees;

	Matrix34 m_Matrix;

	inline Vector3 GetPosition(void) { return m_vPosition; }
	inline int GetModelIndex(void) { return 0; }
	inline float GetBoundRadius(void)
	{
		Vector3 vec(m_fWidth, m_fDepth, 0);
		return vec.Mag();
	}
	inline int GetType() { return ENTITY_TYPE_OBJECT; }
	inline bool GetIsTypeObject() { return GetType()==ENTITY_TYPE_OBJECT; }

	inline bool GetIsTypeVehicle(void) { return false; }

	inline const Matrix34 & GetMatrix(void) { return m_Matrix; }

	inline float GetMass() { return 1.0f; }
	inline bool GetIsFixed() { return false; }
};

struct TObjFlags
{
	bool bIsDynamicObjectInPathServer;
	bool bBoundsNeedRecalculatingForNavigation;
};

class CObject : public CEntity
{
public:
	CObject(void) { }
	~CObject(void) { }

	TObjFlags m_nObjectFlags;
	TDynamicObjectIndex m_iPathServerDynObjId;

	inline TDynamicObjectIndex GetPathServerDynamicObjectIndex() { return m_iPathServerDynObjId; }
	inline void SetPathServerDynamicObjectIndex(TDynamicObjectIndex i) { m_iPathServerDynObjId = i; }
};

#define VEHICLE_TYPE_BIKE 1
class CVehicle : public CEntity
{
public:
	CVehicle(void) { }
	~CVehicle(void) { }

	TObjFlags m_nObjectFlags;
	TDynamicObjectIndex m_iPathServerDynObjId;

	inline int GetVehicleType() { return 0; }
	inline TDynamicObjectIndex GetPathServerDynamicObjectIndex() { return m_iPathServerDynObjId; }
	inline void SetPathServerDynamicObjectIndex(TDynamicObjectIndex i) { m_iPathServerDynObjId = i; }
};

class CEntityBoundAI
{
public:
	CEntityBoundAI(CEntity & entity, float height, float radius, bool bCalcTopAndBottomZ)
	{
		Init(entity, height, radius, bCalcTopAndBottomZ);
	}
	~CEntityBoundAI(void) { }

	void Init(CEntity & entity, float height, float radius, bool bCalcTopAndBottomZ)
	{
		height; radius; bCalcTopAndBottomZ;

		m_fRadius = 0.0f;
		Matrix34 rotMat;
		rotMat.Identity();
		rotMat.RotateFullZ(entity.m_fRotationInDegrees * DtoR);

		Vector3 inputPts[4];
		inputPts[0] = Vector3(-entity.m_fWidth, entity.m_fDepth, 0);
		inputPts[1] = Vector3(entity.m_fWidth, entity.m_fDepth, 0);
		inputPts[2] = Vector3(entity.m_fWidth, -entity.m_fDepth, 0);
		inputPts[3] = Vector3(-entity.m_fWidth, -entity.m_fDepth, 0);

		float fRad2 = 0.0f;
		Vector3 outputPts[4];
		int v;
		for(v=0; v<4; v++)
		{
			rotMat.Transform(inputPts[v], outputPts[v]);
			m_vCorners[v] = outputPts[v] + entity.m_vPosition;

			float fDist2 = (m_vCorners[v] - entity.m_vPosition).Mag2();
			if(fDist2 > fRad2) fRad2 = fDist2;
		}

		if(fRad2) m_fRadius = rage::Sqrtf(fRad2);

		int lastv = 3;
		for(v=0; v<4; v++)
		{
			Vector3 vec1 = m_vCorners[v];
			Vector3 vec2 = m_vCorners[lastv];
			Vector3 vEdge = vec1 - vec2;
			vEdge.Normalize();
			Vector3 vNormal = CrossProduct(Vector3(0,0,1.0f), vEdge);
			float fDist = -DotProduct(vNormal, vec1);

			//m_vPlaneNormals[(v+1)%4] = vNormal;
			//m_fPlaneDists[(v+1)%4] = fDist;
			m_vPlaneNormals[lastv] = vNormal;
			m_fPlaneDists[lastv] = fDist;

			lastv = v;
		}
	}

	void GetCorners(Vector3 * pts)
	{
		pts[0] = m_vCorners[0];
		pts[1] = m_vCorners[1];
		pts[2] = m_vCorners[2];
		pts[3] = m_vCorners[3];
	}
	void GetPlanes(Vector3 * vecs, float * dists)
	{
		vecs[0] = m_vPlaneNormals[0];
		vecs[1] = m_vPlaneNormals[1];
		vecs[2] = m_vPlaneNormals[2];
		vecs[3] = m_vPlaneNormals[3];

		dists[0] = m_fPlaneDists[0];
		dists[1] = m_fPlaneDists[1];
		dists[2] = m_fPlaneDists[2];
		dists[3] = m_fPlaneDists[3];
	}
	void GetPlanes(Vector4 * planes)
	{
		for(s32 p=0; p<4; p++)
		{
			planes[p].x = m_vPlaneNormals[p].x;
			planes[p].y = m_vPlaneNormals[p].y;
			planes[p].z = m_vPlaneNormals[p].z;
			planes[p].w = m_fPlaneDists[p];
		}
	}
	void ComputeSegmentPlanes(Vector3* segmentPlaneNormals, float* segmentPlaneDs) const
	{
		Vector3 m_centre = (m_vCorners[0] + m_vCorners[1] + m_vCorners[2] + m_vCorners[3]) * 0.25f;

		//Compute the planes bounding each region.
		int i;
		for(i=0;i<4;i++)
		{
			int index = (i+1)%4;

			Vector3 vDir;
			vDir=m_vCorners[i]-m_centre;
			segmentPlaneNormals[index]=Vector3(-vDir.y,vDir.x,0);
			segmentPlaneDs[index]=-DotProduct(segmentPlaneNormals[index], m_vCorners[i]);
		}
	}
	void ComputeSegmentPlanes(Vector2* segmentPlaneNormals, float* segmentPlaneDs) const
	{
		Vector3 m_centre = (m_vCorners[0] + m_vCorners[1] + m_vCorners[2] + m_vCorners[3]) * 0.25f;

		//Compute the planes bounding each region.
		int i;
		for(i=0;i<4;i++)
		{
			int index = (i+1)%4;

			Vector3 vDir;
			vDir=m_vCorners[i]-m_centre;
			segmentPlaneNormals[index] = Vector2(-vDir.y,vDir.x);
			segmentPlaneDs[index]= - segmentPlaneNormals[index].Dot( Vector2(m_vCorners[i].x, m_vCorners[i].y) );
		}
	}

	void GetSphere(spdSphere& sphere) const
	{
		Vec4V vSphere = VECTOR3_TO_VEC4V( (m_vCorners[0]+m_vCorners[1]+m_vCorners[2]+m_vCorners[3]) * 0.25f);
		sphere.SetV4( vSphere );
		sphere.SetRadiusf( m_fRadius ); 
	}

//	void ComputeCorners(CEntity & entity, const float fHeight, const float fPedRadius);
//	void ComputeCentre(CEntity & entity, const float fHeight, const float fPedRadius);
//	void ComputeSphere(CEntity & entity, const float fHeight, const float fPedRadius);

	Vector3 m_vCorners[4];
	Vector3 m_vPlaneNormals[4];
	float m_fPlaneDists[4];
	float m_fRadius;
};

//******************************
// Stuff from World.h
//******************************

//extern Vector2 g_vMapMins;
//extern float g_vMapMinsX;
//extern float g_vMapMinsY;

//#define WORLD_WIDTHINSECTORS 200
//#define WORLD_DEPTHINSECTORS 200
//#define WORLD_WIDTHOFSECTOR 50.0f
//#define WORLD_DEPTHOFSECTOR 50.0f

/*
#define WORLD_BORDERXMAX ((WORLD_WIDTHINSECTORS / 2) * WORLD_WIDTHOFSECTOR)
#define WORLD_BORDERXMIN ((-WORLD_WIDTHINSECTORS / 2) * WORLD_WIDTHOFSECTOR)
#define WORLD_BORDERYMAX ((WORLD_DEPTHINSECTORS / 2) * WORLD_DEPTHOFSECTOR)
#define WORLD_BORDERYMIN ((-WORLD_DEPTHINSECTORS / 2) * WORLD_DEPTHOFSECTOR)
#define WORLD_BORDERZMIN (-1000.0f)
#define WORLD_BORDERZMAX (5000.0f)
*/

//#define WORLD_XSHIFTINSECTORS (0)

//#define WORLD_WORLDTOSECTORX(x) ((s32) Floorf((x - g_vMapMinsX) / WORLD_WIDTHOFSECTOR) )
//#define WORLD_WORLDTOSECTORY(y) ((s32) Floorf((y - g_vMapMinsY) / WORLD_DEPTHOFSECTOR) )
//#define WORLD_SECTORTOWORLDX(x) ((float) (g_vMapMinsX + (x * WORLD_WIDTHOFSECTOR)) )
//#define WORLD_SECTORTOWORLDY(y) ((float) (g_vMapMinsY + (y * WORLD_WIDTHOFSECTOR)) )

//#define WORLD_WORLDTOSECTORX(x) ((s32) Floorf(((x) / WORLD_WIDTHOFSECTOR) + (WORLD_WIDTHINSECTORS / 2.0f)))
//#define WORLD_WORLDTOSECTORY(y) ((s32) Floorf(((y) / WORLD_DEPTHOFSECTOR) + (WORLD_DEPTHINSECTORS / 2.0f)))
//#define WORLD_SECTORTOWORLDX(x) ((float) ((x) - WORLD_WIDTHINSECTORS / 2) * WORLD_WIDTHOFSECTOR)
//#define WORLD_SECTORTOWORLDY(y) ((float) ((y) - WORLD_DEPTHINSECTORS / 2) * WORLD_DEPTHOFSECTOR)


//**********************************
// Stuff from PedGeometryAnalyser.h
//**********************************
class CPedGeometryAnalyser
{
public:

	inline static void ComputeEntityBoundingBoxCorners(float fHeight, CEntity & entity, Vector3 * pPts) { fHeight; entity; pPts; }
	inline static void ComputeEntityBoundingBoxPlanes(float fHeight, CEntity & entity, Vector3 * pNormals, float * pDists) { fHeight; entity; pNormals; pDists; }

};

//******************************
// Stuff from Population.h
//******************************
#define POPULATION_CULL_RANGE_FAR_AWAY		(65.0f) 	// for special peds like the ones generated at roadblocks

//******************************
// Stuff from Ped.h
//******************************

// Now in common.h: /FF
//	#define PED_RADIUS							(0.35f)

//**********************************
// Stuff from Cover.h
//**********************************
class CCover
{
public:

	inline static void AddCoverPoint(int iType, CEntity * pEntity, Vector3 * vPos, int iUsage, u8 iCoverDir) { iType; pEntity; vPos; iUsage; iCoverDir; }
};

class CCoverPoint
{
public:
	enum { COVTYPE_POINTONMAP };
};


//**********************************
// Stuff from Skidmarks.h
//**********************************
class CSkidmark
{
public:
	CSkidmark(void) { }
	~CSkidmark(void) { }

	enum eSkidmarkType
	{
		SKIDMARKTYPE_DEFAULT = 0,
		SKIDMARKTYPE_MUDDY,
		SKIDMARKTYPE_SANDY,
		SKIDMARKTYPE_BLOODY
	};
};

//**********************************
// Stuff from Timer.h
//**********************************
/*
class CTimer
{
public:
	static inline u32 GetTimeInMilliseconds(void) { return 0; }
	static inline u32 GetElapsedTimeInMilliseconds(void) { return 0; }
};
*/

enum eWeaponType
{
	// IF YOU CHANGE THE ORDER OR ADD ANY NEW WEAPONS - 
	// ALSO CHANGE THE TABLES IN PICKUPS.CPP
	WEAPONTYPE_UNARMED=0,

	WEAPONTYPE_NIGHTSTICK,
	WEAPONTYPE_BASEBALLBAT,

	WEAPONTYPE_GRENADE,
	WEAPONTYPE_MOLOTOV,
	WEAPONTYPE_ROCKET,

	// FIRST SKILL WEAPON
	WEAPONTYPE_PISTOL,			// handguns
	WEAPONTYPE_SHOTGUN,			// shotguns
	WEAPONTYPE_MICRO_UZI,		// submachine guns
	WEAPONTYPE_AK47,			// machine guns

	// END SKILL WEAPONS

	WEAPONTYPE_SNIPERRIFLE,	
	WEAPONTYPE_ROCKETLAUNCHER,	// specials
	WEAPONTYPE_FLAMETHROWER,	// specials
	WEAPONTYPE_MINIGUN,	// specials
	WEAPONTYPE_DETONATOR,	// specials
	WEAPONTYPE_CAMERA,

	WEAPONTYPE_LAST_WEAPONTYPE,
	WEAPONTYPE_ARMOUR,	// Armour has to be the first after WEAPONTYPE_LAST_WEAPONTYPE
	WEAPONTYPE_RAMMEDBYCAR,
	WEAPONTYPE_RUNOVERBYCAR,
	WEAPONTYPE_EXPLOSION,
	WEAPONTYPE_UZI_DRIVEBY,
	WEAPONTYPE_DROWNING,
	WEAPONTYPE_FALL,
	WEAPONTYPE_UNIDENTIFIED,			// Used for damage being done
	WEAPONTYPE_ANYMELEE,
	WEAPONTYPE_ANYWEAPON,
};

enum eWeaponGroup
{
	WEAPONGROUP_FIST = 0,
	WEAPONGROUP_KNUCKLEDUSTER, 
	WEAPONGROUP_BLUNT_SHORT, 
	WEAPONGROUP_BLUNT_LONG, 
	WEAPONGROUP_SHARP_SHORT, 
	WEAPONGROUP_SHARP_LONG, 
	WEAPONGROUP_CHAINSAW,
	WEAPONGROUP_SMALL_PISTOL, 
	WEAPONGROUP_LARGE_PISTOL, 
	WEAPONGROUP_SHOTGUN, 
	WEAPONGROUP_MACHINE_GUN, 
	WEAPONGROUP_RIFLE, 
	WEAPONGROUP_MINIGUN, 
	WEAPONGROUP_THROWN,
	WEAPONGROUP_MOLOTOV, 
	WEAPONGROUP_FIRE, 
	WEAPONGROUP_EXPLOSIVE, 
	WEAPONGROUP_ROCKET,

	NUM_WEAPON_GROUPS
};

class CTxdStore
{
public:
	static int FindSlot(char *) { return 0; }
	static void RemoveRef(int) { }
};

class CBaseModelInfo
{
public:
	inline bool GetUsesDoorPhysics() { return false; }
};

#endif
