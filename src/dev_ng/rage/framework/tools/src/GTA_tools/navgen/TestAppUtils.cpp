#include "TestAppUtils.h"
#include "NavGen.h"

#include "grcore/im.h"
#include "grcore/state.h"
#include "grmodel/modelfactory.h"

using namespace rage;

void
CTestAppUtils::DrawBox(const Vector3 & vMin, const Vector3 & vMax, const Color32 & col)
{
	Vector3 vCorners[9] = {
		Vector3(vMin.x, vMin.y, vMin.z),
		Vector3(vMax.x, vMin.y, vMin.z),
		Vector3(vMax.x, vMax.y, vMin.z),
		Vector3(vMin.x, vMax.y, vMin.z),
		Vector3(vMin.x, vMin.y, vMax.z),
		Vector3(vMax.x, vMin.y, vMax.z),
		Vector3(vMax.x, vMax.y, vMax.z),
		Vector3(vMin.x, vMax.y, vMax.z)
	};

	grcBegin(drawLineStrip, 5);
	grcVertex(vCorners[0].x, vCorners[0].y, vCorners[0].z, 0.0f, 0.0f, 0.0f, col, 0.0f, 0.0f);
	grcVertex(vCorners[1].x, vCorners[1].y, vCorners[1].z, 0.0f, 0.0f, 0.0f, col, 0.0f, 0.0f);
	grcVertex(vCorners[2].x, vCorners[2].y, vCorners[2].z, 0.0f, 0.0f, 0.0f, col, 0.0f, 0.0f);
	grcVertex(vCorners[3].x, vCorners[3].y, vCorners[3].z, 0.0f, 0.0f, 0.0f, col, 0.0f, 0.0f);
	grcVertex(vCorners[0].x, vCorners[0].y, vCorners[0].z, 0.0f, 0.0f, 0.0f, col, 0.0f, 0.0f);
	grcEnd();

	grcBegin(drawLineStrip, 5);
	grcVertex(vCorners[4].x, vCorners[4].y, vCorners[4].z, 0.0f, 0.0f, 0.0f, col, 0.0f, 0.0f);
	grcVertex(vCorners[5].x, vCorners[5].y, vCorners[5].z, 0.0f, 0.0f, 0.0f, col, 0.0f, 0.0f);
	grcVertex(vCorners[6].x, vCorners[6].y, vCorners[6].z, 0.0f, 0.0f, 0.0f, col, 0.0f, 0.0f);
	grcVertex(vCorners[7].x, vCorners[7].y, vCorners[7].z, 0.0f, 0.0f, 0.0f, col, 0.0f, 0.0f);
	grcVertex(vCorners[4].x, vCorners[4].y, vCorners[4].z, 0.0f, 0.0f, 0.0f, col, 0.0f, 0.0f);
	grcEnd();

	grcBegin(drawLines, 8);
	grcVertex(vCorners[0].x, vCorners[0].y, vCorners[0].z, 0.0f, 0.0f, 0.0f, col, 0.0f, 0.0f);
	grcVertex(vCorners[4].x, vCorners[4].y, vCorners[4].z, 0.0f, 0.0f, 0.0f, col, 0.0f, 0.0f);
	grcVertex(vCorners[1].x, vCorners[1].y, vCorners[1].z, 0.0f, 0.0f, 0.0f, col, 0.0f, 0.0f);
	grcVertex(vCorners[5].x, vCorners[5].y, vCorners[5].z, 0.0f, 0.0f, 0.0f, col, 0.0f, 0.0f);
	grcVertex(vCorners[2].x, vCorners[2].y, vCorners[2].z, 0.0f, 0.0f, 0.0f, col, 0.0f, 0.0f);
	grcVertex(vCorners[6].x, vCorners[6].y, vCorners[6].z, 0.0f, 0.0f, 0.0f, col, 0.0f, 0.0f);
	grcVertex(vCorners[3].x, vCorners[3].y, vCorners[3].z, 0.0f, 0.0f, 0.0f, col, 0.0f, 0.0f);
	grcVertex(vCorners[7].x, vCorners[7].y, vCorners[7].z, 0.0f, 0.0f, 0.0f, col, 0.0f, 0.0f);
	grcEnd();
}


CCollisionTrisRenderer::CCollisionTrisRenderer() :
	m_pVertexDecl(NULL),
	m_pVertexBuffer(NULL),
	m_iNumVertices(0)
{

}

CCollisionTrisRenderer::~CCollisionTrisRenderer()
{
	Shutdown();
}

bool
CCollisionTrisRenderer::Init(CNavGen & navMeshGenerator)
{
	CleanDelete(m_pVertexBuffer);
	CleanDelete(m_pVertexDecl);

	grcFvf fvf;
	fvf.SetPosChannel(true);
	fvf.SetNormalChannel(true);
	fvf.SetDiffuseChannel(true);
	m_pVertexDecl = grmModelFactory::BuildDeclarator(&fvf, NULL, NULL, NULL);

	int iNumTris = navMeshGenerator.m_CollisionTriangles.size();
	Color32 iTriColWalkable(255,255,255,255);
	Color32 iTriColNotWalkable(192,64,64,255);

	Color32 iColBlindData1(64,192,64,255);

	
	m_iNumVertices = iNumTris*3;
	m_pVertexBuffer = grcVertexBuffer::Create(m_iNumVertices,0,fvf);
	m_pVertexBuffer->Lock();

	int t;
	int idx=0;
	Color32 triCol;

	for(t=0; t<iNumTris; t++)
	{
		CNavGenTri * pColTri = navMeshGenerator.m_CollisionTriangles[t];

		if(IsSurfaceTypePavement(pColTri->m_iSurfaceType))	// TODO : Replace this with a check on a 'blind-data' bitfield in pColTri
			triCol = iColBlindData1;
		else
			triCol = navMeshGenerator.IsWalkable(pColTri) ? iTriColWalkable : iTriColNotWalkable;

		m_pVertexBuffer->SetPosition(idx, pColTri->m_vPts[2]);
		m_pVertexBuffer->SetNormal(idx, Vector3(0.0f,0.0f,0.0f));
		m_pVertexBuffer->SetCPV(idx, triCol);
		idx++;

		m_pVertexBuffer->SetPosition(idx, pColTri->m_vPts[1]);
		m_pVertexBuffer->SetNormal(idx, Vector3(0.0f,0.0f,0.0f));
		m_pVertexBuffer->SetCPV(idx, triCol);
		idx++;

		m_pVertexBuffer->SetPosition(idx, pColTri->m_vPts[0]);
		m_pVertexBuffer->SetNormal(idx, Vector3(0.0f,0.0f,0.0f));
		m_pVertexBuffer->SetCPV(idx, triCol);
		idx++;
	}

	m_pVertexBuffer->Unlock();

	return true;
}

void
CCollisionTrisRenderer::Shutdown()
{
	CleanDelete(m_pVertexBuffer);
}

void
CCollisionTrisRenderer::Draw(bool bWireFrameAlso)
{
	grcState::SetCullMode(grccmBack);
	grcState::SetDepthWrite(true);
	grcState::SetDepthTest(true);
	grcState::SetFillMode(grcfmSolid);
	

	GRCDEVICE.SetDefaultEffect(false, false);

	GRCDEVICE.SetVertexDeclaration(m_pVertexDecl);
	GRCDEVICE.SetStreamSource(0, *m_pVertexBuffer, 0, m_pVertexBuffer->GetVertexStride());
	GRCDEVICE.DrawPrimitive(drawTris,0,m_iNumVertices);

	if(bWireFrameAlso)
	{
		grcState::SetDepthWrite(true);
		grcState::SetDepthTest(true);
		grcState::SetFillMode(grcfmWireframe);
		

		GRCDEVICE.SetDefaultEffect(true, false);
		GRCDEVICE.DrawPrimitive(drawTris,0,m_iNumVertices);

		grcState::SetFillMode(grcfmSolid);
		
	}
}



bool
CNavGenOctreeRenderer::Draw(CNavGen & navMeshGenerator)
{
	if(!navMeshGenerator.m_Octree.RootNode())
		return false;

	grcViewport::SetCurrentWorldIdentity();
	grcBindTexture(NULL);
	grcState::SetDepthWrite(false);
	grcState::SetDepthTest(true);

	DrawNodeRecursive(navMeshGenerator.m_Octree.RootNode());

	return true;
}


void
CNavGenOctreeRenderer::DrawNodeRecursive(CNavGenOctreeNodeBase * pOctree)
{
	if(pOctree->m_Flags & NAVGEN_OCTREE_FLAG_ISLEAF)
	{
		CNavGenOctreeLeaf * pLeaf = (CNavGenOctreeLeaf*)pOctree;
		static const Color32 col(255,0,0,255);
		CTestAppUtils::DrawBox(pLeaf->m_NodeMin, pLeaf->m_NodeMax, col);
		return;
	}

	CNavGenOctreeNode * pNode = (CNavGenOctreeNode*)pOctree;
	for(int o=0; o<8; o++)
	{
		if(pNode->m_ChildNodes[o])
			DrawNodeRecursive(pNode->m_ChildNodes[o]);
	}
}

