/////////////////////////////////////////////////////////////////////////////////
// FILE		: CAudioLevelMesh.cpp
// PURPOSE	: To load in level geometry for the audio mesh generation tool.
// AUTHOR	: C. Walder (based on Recast be Mikko Menonen)
// STARTED	: 31/08/2009
///////////////////////////////////////////////////////////////////////////////
		
#define _USE_MATH_DEFINES
//#include <math.h>
#include <windows.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include "AudioGenDebugDraw.h"
#include "SDL.h"
#include "SDL_opengl.h"
#include "CAudioLevelMesh.h"
#include "AudioGen.h"

void agDebugDrawMesh(const float* verts, int UNUSED_PARAM(nverts),
					 const int* tris, const float* normals, int ntris,
					 const unsigned char* flags)
{	
	glBegin(GL_TRIANGLES);
	for (int i = 0; i < ntris*3; i += 3)
	{
		float a = (2+normals[i+0]+normals[i+1])/4;
		if (flags && !flags[i/3])
			glColor3f(a,a*0.3f,a*0.1f);
		else
			glColor3f(a,a,a);
		glVertex3fv(&verts[tris[	i]*3]);
		glVertex3fv(&verts[tris[i+1]*3]);
		glVertex3fv(&verts[tris[i+2]*3]);
	}
	glEnd();
}

void agDebugDrawMeshSlope(const float* verts, int UNUSED_PARAM(nverts),
						  const int* tris, const float* normals, int ntris,
						  const float walkableSlopeAngle)
{
	const float walkableThr = cosf(walkableSlopeAngle/180.0f*PI);
	
	glBegin(GL_TRIANGLES);
	for (int i = 0; i < ntris*3; i += 3)
	{
		const float* norm = &normals[i];
		float a = (2+norm[0]+norm[1])/4;
		if (norm[1] > walkableThr)
			glColor3f(a,a,a);
		else
			glColor3f(a,a*0.3f,a*0.1f);
		glVertex3fv(&verts[tris[i]*3]);
		glVertex3fv(&verts[tris[i+1]*3]);
		glVertex3fv(&verts[tris[i+2]*3]);
	}
	glEnd();
}

void drawBoxWire(float minx, float miny, float minz, float maxx, float maxy, float maxz, const float* col)
{
	glColor4fv(col);
	
	// Top
	glVertex3f(minx, miny, minz);
	glVertex3f(maxx, miny, minz);
	glVertex3f(maxx, miny, minz);
	glVertex3f(maxx, miny, maxz);
	glVertex3f(maxx, miny, maxz);
	glVertex3f(minx, miny, maxz);
	glVertex3f(minx, miny, maxz);
	glVertex3f(minx, miny, minz);
	
	// bottom
	glVertex3f(minx, maxy, minz);
	glVertex3f(maxx, maxy, minz);
	glVertex3f(maxx, maxy, minz);
	glVertex3f(maxx, maxy, maxz);
	glVertex3f(maxx, maxy, maxz);
	glVertex3f(minx, maxy, maxz);
	glVertex3f(minx, maxy, maxz);
	glVertex3f(minx, maxy, minz);
	
	// Sides
	glVertex3f(minx, miny, minz);
	glVertex3f(minx, maxy, minz);
	glVertex3f(maxx, miny, minz);
	glVertex3f(maxx, maxy, minz);
	glVertex3f(maxx, miny, maxz);
	glVertex3f(maxx, maxy, maxz);
	glVertex3f(minx, miny, maxz);
	glVertex3f(minx, maxy, maxz);
}

void drawBox(float minx, float miny, float minz, float maxx, float maxy, float maxz,
			 const float* col1, const float* col2)
{
	float verts[8*3] =
	{
		minx, miny, minz,
		maxx, miny, minz,
		maxx, miny, maxz,
		minx, miny, maxz,
		minx, maxy, minz,
		maxx, maxy, minz,
		maxx, maxy, maxz,
		minx, maxy, maxz,
	};
	static const float dim[6] =
	{
		0.95f, 0.55f, 0.65f, 0.85f, 0.65f, 0.85f, 
	};
	static const unsigned char inds[6*5] =
	{
		0,  7, 6, 5, 4,
		1,  0, 1, 2, 3,
		2,  1, 5, 6, 2,
		3,  3, 7, 4, 0,
		4,  2, 6, 7, 3,
		5,  0, 4, 5, 1,
	};
	
	const unsigned char* in = inds;
	for (int i = 0; i < 6; ++i)
	{
		float d = dim[*in]; in++;
		if (i == 0)
			glColor4f(d*col2[0],d*col2[1],d*col2[2], col2[3]);
		else
			glColor4f(d*col1[0],d*col1[1],d*col1[2], col1[3]);
		glVertex3fv(&verts[*in*3]); in++;
		glVertex3fv(&verts[*in*3]); in++;
		glVertex3fv(&verts[*in*3]); in++;
		glVertex3fv(&verts[*in*3]); in++;
	}
}

void agDebugDrawCylinderWire(float minx, float miny, float minz, float maxx, float maxy, float maxz, const float* col)
{
	static const int NUM_SEG = 16;
	float dir[NUM_SEG*2];
	for (int i = 0; i < NUM_SEG; ++i)
	{
		const float a = (float)i/(float)NUM_SEG*(float)PI*2;
		dir[i*2] = cosf(a);
		dir[i*2+1] = sinf(a);
	}

	const float cx = (maxx + minx)/2;
	const float cz = (maxz + minz)/2;
	const float rx = (maxx - minx)/2;
	const float rz = (maxz - minz)/2;
	
	glColor4fv(col);
	glBegin(GL_LINES);
	for (int i = 0, j=NUM_SEG-1; i < NUM_SEG; j=i++)
	{
		glVertex3f(cx+dir[j*2+0]*rx, miny, cz+dir[j*2+1]*rz);
		glVertex3f(cx+dir[i*2+0]*rx, miny, cz+dir[i*2+1]*rz);
		glVertex3f(cx+dir[j*2+0]*rx, maxy, cz+dir[j*2+1]*rz);
		glVertex3f(cx+dir[i*2+0]*rx, maxy, cz+dir[i*2+1]*rz);
	}
	for (int i = 0; i < NUM_SEG; i += NUM_SEG/4)
	{
		glVertex3f(cx+dir[i*2+0]*rx, miny, cz+dir[i*2+1]*rz);
		glVertex3f(cx+dir[i*2+0]*rx, maxy, cz+dir[i*2+1]*rz);
	}
	glEnd();
}

void agDebugDrawBoxWire(float minx, float miny, float minz, float maxx, float maxy, float maxz, const float* col)
{
	glBegin(GL_LINES);
	drawBoxWire(minx, miny, minz, maxx, maxy, maxz, col);
	glEnd();
}

void agDebugDrawBox(float minx, float miny, float minz, float maxx, float maxy, float maxz,
					const float* col1, const float* col2)
{
	glBegin(GL_QUADS);
	drawBox(minx, miny, minz, maxx, maxy, maxz, col1, col2);
	glEnd();
}

void agDebugDrawHeightfieldSolid(const agHeightField& hf)
{
	static const float col0[4] = { 1,1,1,1 };
	
	const float* orig = hf.boundMin;
	const float cs = hf.cellSize;
	const float ch = hf.cellHeight;
	
	const int w = hf.width;
	const int h = hf.length;
	
	glBegin(GL_QUADS);
	
	for (int y = 0; y < h; ++y)
	{
		for (int x = 0; x < w; ++x)
		{
			float fx = orig[0] + x*cs;
			float fz = orig[2] + y*cs;
			const agSpan* s = hf.spans[x + y*w];
			while (s)
			{
				drawBox(fx, orig[1]+s->minY*ch, fz, fx+cs, orig[1] + s->maxY*ch, fz+cs, col0, col0);
				s = s->next;
			}
		}
	}
	glEnd();
}

void agDebugDrawHeightfieldWalkable(const agHeightField& hf)
{
	static const float col0[4] = { 1,1,1,1 };
	static const float col1[4] = { 0.25f,0.44f,0.5f,1 };
	
	const float* orig = hf.boundMin;
	const float cs = hf.cellSize;
	const float ch = hf.cellHeight;
	
	const int w = hf.width;
	const int h = hf.length;
	
	glBegin(GL_QUADS);
	
	for (int y = 0; y < h; ++y)
	{
		for (int x = 0; x < w; ++x)
		{
			float fx = orig[0] + x*cs;
			float fz = orig[2] + y*cs;
			const agSpan* s = hf.spans[x + y*w];
			while (s)
			{
				bool csel = (s->flags & 0x1) == 0;
				drawBox(fx, orig[1]+s->minY*ch, fz, fx+cs, orig[1] + s->maxY*ch, fz+cs, col0, csel ? col0 : col1);
				s = s->next;
			}
		}
	}
	glEnd();
}

void agDebugDrawCompactHeightfieldSolid(const agCompactHeightField& compact)
{
	const float cs = compact.cellSize;
	const float ch = compact.cellHeight;

	glColor3ub(64,112,128);

	glBegin(GL_QUADS);
	for (int y = 0; y < compact.length; ++y)
	{
		for (int x = 0; x < compact.width; ++x)
		{
			const float fx = compact.boundMin[0] + x*cs;
			const float fz = compact.boundMin[2] + y*cs;
			const agCompactCell& c = compact.cells[x+y*compact.width];

			for (unsigned i = c.index, ni = c.index+c.count; i < ni; ++i)
			{
				const agCompactSpan& s = compact.spans[i];
				if(s.flags&0xf) glColor3ub(112, 64, 128);
				const float fy = compact.boundMin[1] + (s.y+1)*ch;
				glVertex3f(fx, fy, fz);
				glVertex3f(fx, fy, fz+cs);
				glVertex3f(fx+cs, fy, fz+cs);
				glVertex3f(fx+cs, fy, fz);
				glColor3ub(64, 112, 128);
			}
		}
	}
	glEnd();
}

void agDebugDrawCompactVolumefieldSolid(const agCompactVolumefield& compact)
{
	const float cs = compact.cellSize;
	const float ch = compact.cellHeight;



	glBegin(GL_QUADS);

	glColor3ub(128, 128, 128);
	glVertex3f(compact.bmin[0], compact.bmin[1], compact.bmin[2]);
	glVertex3f(compact.bmin[0]+cs, compact.bmin[1], compact.bmin[2]);
	glVertex3f(compact.bmin[0]+cs, compact.bmin[1]+cs, compact.bmin[2]);
	glVertex3f(compact.bmin[0], compact.bmin[1]+cs, compact.bmin[2]);
	for (int y = 0; y < compact.length; ++y)
	{
		for (int x = 0; x < compact.width; ++x)
		{
			float fx = compact.bmin[0] + x*cs;
			float tx = fx+cs;
			float fz = compact.bmin[2] + y*cs;
			float tz = fz+cs;
			const agCompact3dCell& cell = compact.cells[x+y*compact.width];

			for (unsigned i = cell.index, ni = cell.index+cell.count; i < ni; ++i)
			{
				const agCompactVoxel& vox = compact.voxels[i];
				const float fy = compact.bmin[1] + vox.y*ch;
				float ty = fy + ch;
				if(vox.faces[AG_VOX_TOP].flags&AG_FACE_ACTIVE)
				{
					glColor3ub(64,112,128);
					glVertex3f(fx, ty, fz);
					glVertex3f(fx, ty, tz);
					glVertex3f(tx, ty, tz);
					glVertex3f(tx, ty, fz);
				}

				if(vox.faces[AG_VOX_BOTTOM].flags&AG_FACE_ACTIVE)
				{
					glColor3ub(112, 64, 128);
					glVertex3f(fx, fy, fz);
					glVertex3f(tx, fy, fz);
					glVertex3f(tx, fy, tz);
					glVertex3f(fx, fy, tz);
				}

				if(vox.faces[AG_VOX_LEFT].flags&AG_FACE_ACTIVE)
				{
					glColor3ub(128, 112, 64);
					glVertex3f(fx, fy, fz);
					glVertex3f(fx, fy, tz);
					glVertex3f(fx, ty, tz);
					glVertex3f(fx, ty, fz);
				}

				if(vox.faces[AG_VOX_RIGHT].flags&AG_FACE_ACTIVE)
				{
					glColor3ub(64, 112, 64);
					glVertex3f(tx, fy, fz);
					glVertex3f(tx, ty, fz);
					glVertex3f(tx, ty, tz);
					glVertex3f(tx, fy, tz);
				}

				if(vox.faces[AG_VOX_FRONT].flags&AG_FACE_ACTIVE)
				{
					glColor3ub(112, 64, 112);
					glVertex3f(fx, fy, tz);
					glVertex3f(tx, fy, tz);
					glVertex3f(tx, ty, tz);
					glVertex3f(fx, ty, tz);
				}

				if(vox.faces[AG_VOX_BACK].flags&AG_FACE_ACTIVE)
				{
					glColor3ub(128, 64, 64);
					glVertex3f(fx, fy, fz);
					glVertex3f(fx, ty, fz);
					glVertex3f(tx, ty, fz);
					glVertex3f(tx, fy, fz);
				}
			}
		}
	}
	glEnd();
}

void agDebugDrawCompactHeightfieldExtrudedRegions(const agCompactHeightField &compact)
{
	float cs = compact.cellSize;
	float ch = compact.cellHeight;

	float col[4] = {1,1,1,1};

	glBegin(GL_QUADS);
	for(int y=0; y<compact.length; ++y)
	{
		for(int x=0; x<compact.width; ++x)
		{
			float fx = compact.boundMin[0] + x*cs;
			float fz = compact.boundMin[2] + y*cs;
			const agCompactCell &cell = compact.cells[x+y*compact.width];

			for(u32 i = cell.index, endi = cell.index+cell.count; i<endi; ++i)
			{
				const agCompactSpan &span = compact.spans[i];
				if(span.reg)
				{
					bool isRegionEdge = false;
					if (agGetCon(span, 0) == 0xf)
					{
						isRegionEdge = true;
					}
					else
					{
						const int ax = x + agGetDirOffsetX(0);
						const int ay = y + agGetDirOffsetY(0);
						const int ai = (int)compact.cells[ax+ay*compact.width].index + agGetCon(span, 0);
						agCompactSpan &nspan = compact.spans[ai];
						if(span.reg != nspan.reg)
						{
							isRegionEdge = true;
						}
					}

					if(isRegionEdge)
					{
						float fy = compact.boundMin[1] + (span.y+1)*ch;
						intToCol(span.reg, col);
						col[3] = 0.5f;
						glColor4fv(col);
						float height = (span.height*ch >5.f)? 5.f:span.height*ch;

						glVertex3f(fx, fy, fz);
						glVertex3f(fx, fy+height, fz);
						glVertex3f(fx, fy+height, fz+cs);
						glVertex3f(fx, fy, fz+cs);
						
						glVertex3f(fx, fy, fz+cs);
						glVertex3f(fx, fy+height, fz+cs);
						glVertex3f(fx, fy+height, fz);
						glVertex3f(fx, fy, fz);
					}
				}
			}
			
		}
	}
	glEnd();
}

void agDebugDrawCompactHeightfieldRegions(const agCompactHeightField& compact)
{
	const float cs = compact.cellSize;
	const float ch = compact.cellHeight;

	float col[4] = { 1,1,1,1 };
	
	glBegin(GL_QUADS);
	for (int y = 0; y < compact.length; ++y)
	{
		for (int x = 0; x < compact.width; ++x)
		{
			const float fx = compact.boundMin[0] + x*cs;
			const float fz = compact.boundMin[2] + y*cs;
			const agCompactCell& cell = compact.cells[x+y*compact.width];
			
			for (unsigned i = cell.index, ni = cell.index+cell.count; i < ni; ++i)
			{
				const agCompactSpan& span = compact.spans[i];
				if (span.reg)
				{
					intToCol(span.reg, col);
					glColor4fv(col);
				}
				else
				{
					glColor4ub(0,0,0,128);
				}
				const float fy = compact.boundMin[1] + (span.y+1)*ch;
				glVertex3f(fx, fy, fz);
				glVertex3f(fx, fy, fz+cs);
				glVertex3f(fx+cs, fy, fz+cs);
				glVertex3f(fx+cs, fy, fz);
			}
		}
	}
	glEnd();
}

void agDebugDrawCompactHeightFieldDistance(const agCompactHeightField &compact)
{
	const float cs = compact.cellSize;
	const float ch = compact.cellHeight;
		
	float maxd = (float)compact.maxDistance;
	if (maxd < 1.0f) maxd = 1;
	float dscale = 1.0f / maxd;
	
	glBegin(GL_QUADS);
	for (int y = 0; y < compact.length; ++y)
	{
		for (int x = 0; x < compact.width; ++x)
		{
			const float fx = compact.boundMin[0] + x*cs;
			const float fz = compact.boundMin[2] + y*cs;
			const agCompactCell& cell = compact.cells[x+y*compact.width];
			
			for (unsigned i = cell.index, endi = cell.index+cell.count; i < endi; ++i)
			{
				const agCompactSpan& span = compact.spans[i];
				const float fy = compact.boundMin[1] + (span.y+1)*ch;
				float cd = (float)span.dist * dscale;
				glColor3f(cd, cd, cd);
				glVertex3f(fx, fy, fz);
				glVertex3f(fx, fy, fz+cs);
				glVertex3f(fx+cs, fy, fz+cs);
				glVertex3f(fx+cs, fy, fz);
			}
		}
	}
	glEnd();
}

void agDebugDrawCompactVolumefieldDistance(const agCompactVolumefield& compact)
{
	const float cs = compact.cellSize;
	const float ch = compact.cellHeight;
		
	float dscale[AG_NUM_FACES];
	for(int i = 0; i < AG_NUM_FACES; ++i)
	{
		float maxd = compact.maxDistance[i];
		if (maxd < 1.0f) maxd = 1;
		dscale[i] = 1.0f / maxd;
	}
	
	glBegin(GL_QUADS);
	for (int y = 0; y < compact.length; ++y)
	{
		for (int x = 0; x < compact.width; ++x)
		{
			float fx = compact.bmin[0] + x*cs, nx = fx+cs;
			float fz = compact.bmin[2] + y*cs, nz = fz+cs;
			const agCompact3dCell& c = compact.cells[x+y*compact.width];
			
			for (u32 i = c.index, ni = c.index+c.count; i < ni; ++i)
			{
				const agCompactVoxel& vox = compact.voxels[i];
				const float fy = compact.bmin[1] + vox.y*ch;
				const float ny = fy +ch;
				if(vox.faces[AG_VOX_TOP].flags&AG_FACE_ACTIVE)
				{
					float cd = (float)vox.faces[AG_VOX_TOP].dist * dscale[AG_VOX_TOP];
					glColor3f(cd, cd, cd);
					glVertex3f(fx, ny, fz);
					glVertex3f(fx, ny, nz);
					glVertex3f(nx, ny, nz);
					glVertex3f(nx, ny, fz);
				}
				if(vox.faces[AG_VOX_BOTTOM].flags&AG_FACE_ACTIVE)
				{
					float cd = (float)vox.faces[AG_VOX_BOTTOM].dist * dscale[AG_VOX_BOTTOM];
					glColor3f(cd, cd, cd);
					glVertex3f(nx, fy, fz);
					glVertex3f(nx, fy, nz);
					glVertex3f(fx, fy, nz);
					glVertex3f(fx, fy, fz);
				}
				if(vox.faces[AG_VOX_LEFT].flags&AG_FACE_ACTIVE)
				{
					float cd = (float)vox.faces[AG_VOX_LEFT].dist * dscale[AG_VOX_LEFT];
					glColor3f(cd, cd, cd);
					glVertex3f(fx, fy, fz);
					glVertex3f(fx, fy, nz);
					glVertex3f(fx, ny, nz);
					glVertex3f(fx, ny, fz);
				}
				if(vox.faces[AG_VOX_RIGHT].flags&AG_FACE_ACTIVE)
				{
					float cd = (float)vox.faces[AG_VOX_RIGHT].dist * dscale[AG_VOX_RIGHT];
					glColor3f(cd, cd, cd);
					glVertex3f(nx, fy, fz);
					glVertex3f(nx, ny, fz);
					glVertex3f(nx, ny, nz);
					glVertex3f(nx, fy, nz);
				}
				if(vox.faces[AG_VOX_FRONT].flags&AG_FACE_ACTIVE)
				{
					float cd = (float)vox.faces[AG_VOX_FRONT].dist * dscale[AG_VOX_FRONT];
					glColor3f(cd, cd, cd);
					glVertex3f(fx, fy, nz);
					glVertex3f(nx, fy, nz);
					glVertex3f(nx, ny, nz);
					glVertex3f(fx, ny, nz);
				}
				if(vox.faces[AG_VOX_BACK].flags&AG_FACE_ACTIVE)
				{
					float cd = (float)vox.faces[AG_VOX_BACK].dist * dscale[AG_VOX_BACK];
					glColor3f(cd, cd, cd);
					glVertex3f(fx, fy, fz);
					glVertex3f(fx, ny, fz);
					glVertex3f(nx, ny, fz);
					glVertex3f(nx, fy, fz);
				}
			}
		}
	}
	glEnd();
}

static void getPolyRegCenter(const agPolyMesh &mesh, int reg, const float *orig, float *center)
{
	float cs = mesh.cs;
	float ch = mesh.ch;
	int nverts = 0;

	center[0] = 0;
	center[1] = 0;
	center[2] = 0;

	for(int i=0; i<mesh.npolys; ++i)
	{
		if(mesh.regs[i] == reg)
		{
			//int polyindex = i*mesh.nvp*2;
			u16 vi[3];
			const u16 * p = &mesh.polys[i*mesh.nvp*2];
			for(int j=2; j<mesh.nvp; ++j)
			{
				if(p[j] == 0xffff) break;
				vi[0] = p[0];
				vi[1] = p[j-1];
				vi[2] = p[j];
				for(int k = 0; k<3; ++k)
				{
					const u16 *v = &mesh.verts[vi[k]*3];
					center[0] += (float)v[0];
					center[1] += (float)(v[1]+1);
					center[2] += (float)v[2];
					++nverts;
				}
			}
		}
	}

	float s = 1.f/nverts;
	center[0] *= s*cs;
	center[1] *= s*ch;
	center[2] *= s*cs;
	center[0] += orig[0];
	center[1] += orig[1] + 4*ch;
	center[2] += orig[2];
}

static void getContourCenter(const agContour* cont, const float* orig, float cs, float ch, float* center)
{
	center[0] = 0;
	center[1] = 0;
	center[2] = 0;
	if (!cont->nverts)
		return;
	for (int i = 0; i < cont->nverts; ++i)
	{
		const int* v = &cont->verts[i*4];
		center[0] += (float)v[0];
		center[1] += (float)v[1];
		center[2] += (float)v[2];
	}
	const float s = 1.0f / cont->nverts;
	center[0] *= s * cs;
	center[1] *= s * ch;
	center[2] *= s * cs;
	center[0] += orig[0];
	center[1] += orig[1]+4*ch;
	center[2] += orig[2];
}

static const agContour* findContourFromSet(const agContourSet& cset, unsigned short reg)
{
	for (int i = 0; i < cset.nconts; ++i)
	{
		if (cset.conts[i].reg == reg)
			return &cset.conts[i];
	}
	return 0;
}

static void drawArc(const float* p0, const float* p1)
{
	static const int NPTS = 8;
	float pts[NPTS*3];
	float dir[3];
	vsub(dir, p1, p0);
	const float len = sqrtf(vdistSqr(p0, p1));
	for (int i = 0; i < NPTS; ++i)
	{
		float u = (float)i / (float)(NPTS-1);
		float* p = &pts[i*3];
		p[0] = p0[0] + dir[0] * u;
		p[1] = p0[1] + dir[1] * u + (len/4) * (1-agSqr(u*2-1));
		p[2] = p0[2] + dir[2] * u;
	}
	for (int i = 0; i < NPTS-1; ++i)
	{
		glVertex3fv(&pts[i*3]);
		glVertex3fv(&pts[(i+1)*3]);
	}
}

void agDrawArc(const float* p0, const float* p1)
{
	glBegin(GL_LINES);
	drawArc(p0, p1);
	glEnd();
}

static bool PointIsInBBox(const agContourSet &cset, const float pos[3])
{
	const float *min = cset.bmin;
	const float *max = cset.bmax;
	if(pos[0] < min[0] || pos[0] > max[0] || pos[1] < min[1] || pos[1] > max[1] || pos[2] < min[2] || pos[2] > max[2])
	{
		return false;
	}

	return true;
}

void agDebugDrawRegionConnections(const agContourSet& cset, const float alpha)
{
	const float* orig = cset.bmin;
	const float cs = cset.cs;
	const float ch = cset.ch;
	
	// Draw centers
	float pos[3], pos2[3];

	glColor4ub(0,0,0,196);

	glLineWidth(2.0f);
	glBegin(GL_LINES);
	for (int i = 0; i < cset.nconts; ++i)
	{
		const agContour* cont = &cset.conts[i];
		getContourCenter(cont, orig, cs, ch, pos);
		for (int j = 0; j < cont->nverts; ++j)
		{
			const int* v = &cont->verts[j*4];
			if (v[3] == 0 || (unsigned short)v[3] < cont->reg) continue;
			const agContour* cont2 = findContourFromSet(cset, (unsigned short)v[3]);
			if (cont2)
			{
				getContourCenter(cont2, orig, cs, ch, pos2);
				drawArc(pos, pos2);
			}
		}
	}
	glEnd();

	float col[4] = { 1,1,1,alpha };
	
	glPointSize(7.0f);
	glBegin(GL_POINTS);
	for (int i = 0; i < cset.nconts; ++i)
	{
		const agContour* cont = &cset.conts[i];
		intToCol(cont->reg, col);
		col[0] *= 0.5f;
		col[1] *= 0.5f;
		col[2] *= 0.5f;
		glColor4fv(col);
		getContourCenter(cont, orig, cs, ch, pos);
		glVertex3fv(pos);
	}
	glEnd();
	
	
	glLineWidth(1.0f);
	glPointSize(1.0f);
}

void agDebugDrawAudMeshStitching(const agPolyMesh &mesh, const agPolyMesh * const nmeshes[4])
{
	float pos[3] = {0};
	float pos2[3] = {0};
	
	glColor4ub(0,0,0,196);

	glLineWidth(2.0f);
	glBegin(GL_LINES);

	for(int dir=0; dir<4; ++dir)
	{
		if(nmeshes[dir])
		{
			for(int stitch=0; stitch<mesh.nstitches[dir]; ++stitch)
			{		
				getPolyRegCenter(mesh, mesh.localStitches[dir][stitch], mesh.bmin, pos);
				getPolyRegCenter(*nmeshes[dir], mesh.neighbourStitches[dir][stitch], nmeshes[dir]->bmin, pos2);

				//TODO: there are nan's appearing due getPolyCenter not matching any regions on some mesh(es?) FIXME
				const float *bmin = nmeshes[dir]->bmin;
				const float *bmax = nmeshes[dir]->bmax;
				if((pos2[0] >= bmin[0] && pos2[0] < bmax[0] && pos2[1] >= bmin[1] && pos2[1] < bmax[1] && pos2[2] >= bmin[2] && pos2[2] < bmax[2]))
				{
					drawArc(pos, pos2);
				}
			}
		}
	}
	
	glEnd();

	float col[4] = { 1,1,1,0.5f };
	
	glPointSize(7.0f);
	glBegin(GL_POINTS);


	for(int dir=0; dir<4; ++dir)
	{
		if(nmeshes[dir])
		{
			for(int stitch=0; stitch<mesh.nstitches[dir]; ++stitch)
			{
				intToCol(mesh.localStitches[dir][stitch], col);
				col[0] *=0.5f;
				col[1] *=0.5f;
				col[2] *= 0.5f;
				glColor4fv(col);
				getPolyRegCenter(mesh, mesh.localStitches[dir][stitch], mesh.bmin, pos);
				intToCol(mesh.neighbourStitches[dir][stitch], col);
				col[0] *=0.5f;
				col[1] *=0.5f;
				col[2] *= 0.5f;
				glColor4fv(col);
				getPolyRegCenter(*nmeshes[dir], mesh.neighbourStitches[dir][stitch], nmeshes[dir]->bmin, pos2);

				//TODO: there are nan's appearing due getPolyCenter not matching any regions on some mesh(es?) FIXME
				const float *bmin = nmeshes[dir]->bmin;
				const float *bmax = nmeshes[dir]->bmax;
				if((pos2[0] >= bmin[0] && pos2[0] < bmax[0] && pos2[1] >= bmin[1] && pos2[1] < bmax[1] && pos2[2] >= bmin[2] && pos2[2] < bmax[2]))
				{
					glVertex3fv(pos);
					glVertex3fv(pos2);
				}
			}
		}
	}
	glEnd();
	
	
	glLineWidth(1.0f);
	glPointSize(1.0f);
}

void agDebugDrawRawContours(const agContourSet& cset, const float alpha)
{
	const float* orig = cset.bmin;
	const float cs = cset.cs;
	const float ch = cset.ch;
	float col[4] = { 1,1,1,alpha };
	glLineWidth(2.0f);
	glPointSize(2.0f);
	for (int i = 0; i < cset.nconts; ++i)
	{
		const agContour& c = cset.conts[i];
		intToCol(c.reg, col);
		glColor4fv(col);
		glBegin(GL_LINE_LOOP);
		for (int j = 0; j < c.nrverts; ++j)
		{
			const int* v = &c.rverts[j*4];
			float fx = orig[0] + v[0]*cs;
			float fy = orig[1] + (v[1]+1+(i&1))*ch;
			float fz = orig[2] + v[2]*cs;
			glVertex3f(fx,fy,fz);
		}
		glEnd();

		col[0] *= 0.5f;
		col[1] *= 0.5f;
		col[2] *= 0.5f;
		glColor4fv(col);		

		glBegin(GL_POINTS);
		for (int j = 0; j < c.nrverts; ++j)
		{
			const int* v = &c.rverts[j*4];
			
			float off = 0;
			if (v[3] & AG_BORDER_VERTEX)
			{
				glColor4ub(255,255,255,255);
				off = ch*2;
			}
			else
			{
				glColor4fv(col);
			}
			
			float fx = orig[0] + v[0]*cs;
			float fy = orig[1] + (v[1]+1+(i&1))*ch + off;
			float fz = orig[2] + v[2]*cs;
			glVertex3f(fx,fy,fz);
		}
		glEnd();
	}
	glLineWidth(1.0f);
	glPointSize(1.0f);
}

void agDebugDrawContours(const agContourSet& cset, const float UNUSED_PARAM(alpha))
{
	const float* orig = cset.bmin;
	const float cs = cset.cs;
	const float ch = cset.ch;
	float col[4] = { 1,1,1,1 };
	glLineWidth(2.5f);
	glPointSize(3.0f);
	for (int i = 0; i < cset.nconts; ++i)
	{
		const agContour& c = cset.conts[i];
		intToCol(c.reg, col);
		glColor4fv(col);

		glBegin(GL_LINE_LOOP);
		for (int j = 0; j < c.nverts; ++j)
		{
			const int* v = &c.verts[j*4];
			float fx = orig[0] + v[0]*cs;
			float fy = orig[1] + (v[1]+1+(i&1))*ch;
			float fz = orig[2] + v[2]*cs;
			glVertex3f(fx,fy,fz);
		}
		glEnd();

		col[0] *= 0.5f;
		col[1] *= 0.5f;
		col[2] *= 0.5f;
		glColor4fv(col);		
		glBegin(GL_POINTS);
		for (int j = 0; j < c.nverts; ++j)
		{
			const int* v = &c.verts[j*4];
			float off = 0;
			if (v[3] & AG_BORDER_VERTEX)
			{
				glColor4ub(255,255,255,255);
				off = ch*2;
			}
			else
			{
				glColor4fv(col);
			}

			float fx = orig[0] + v[0]*cs;
			float fy = orig[1] + (v[1]+1+(i&1))*ch + off;
			float fz = orig[2] + v[2]*cs;
			glVertex3f(fx,fy,fz);
		}
		glEnd();
	}
	glLineWidth(1.0f);
	glPointSize(1.0f);
}

void agDebugDrawCompactHeightfieldAudConnections( const agCompactHeightField &compact, const agContourSet &cset, float alpha)
{
	const float* orig = cset.bmin;
	float cs = cset.cs;
	float ch = cset.ch;
	//float col[4] = {1,1,1,1};
	float pos[3], pos2[3];
	int count = 0;

	glLineWidth(2.f);
	glColor4ub(0,0,0,196);

	glBegin(GL_LINES);
	for (int i = 0; i < cset.nconts; ++i)
	{
		const agContour * cont = &cset.conts[i];
		if(!cont->nverts)
		{
			continue;
		}
		const agCompactRegion &reg = compact.regions[cont->reg];
		getContourCenter(cont, orig, cs, ch, pos);
		for(int j=0; j<reg.audConnections.size(); ++j)
		{
			const agContour *ncont = findContourFromSet(cset, (u16)reg.audConnections[j]);
			if(ncont)
			{
				getContourCenter(ncont, orig, ch, ch, pos2);
				Assertf(PointIsInBBox(cset, pos2), "Point pos2 (%f, %f, %f) is not in the bounding box! nverts is %i", pos2[0], pos2[1], pos2[2], ncont->nverts);
				Assertf(PointIsInBBox(cset, pos), "Point pos (%f, %f, %f) is not in the bounding box! nverts is %i", pos[0], pos[1], pos[2], ncont->nverts);
				if(ncont->nverts && PointIsInBBox(cset, pos2) && PointIsInBBox(cset, pos))
				{
					drawArc(pos, pos2);
					++count;
				}
			}
		}
	}
	glEnd();

	float col[4] = { 1,1,1,alpha };
	
	glPointSize(7.0f);
	glBegin(GL_POINTS);
	for (int i = 0; i < cset.nconts; ++i)
	{
		const agContour* cont = &cset.conts[i];
		intToCol(cont->reg, col);
		col[0] *= 0.5f;
		col[1] *= 0.5f;
		col[2] *= 0.5f;
		glColor4fv(col);
		getContourCenter(cont, orig, cs, ch, pos);
		glVertex3fv(pos);
	}
	glEnd();


	glLineWidth(1.f);
	glPointSize(1.f);	
}

void agDrawPolymeshAudConnections(const struct agPolyMesh &mesh, const struct agContourSet &cset, float alpha)
{
	const float *orig = cset.bmin;
	float cs = cset.cs;
	float ch = cset.ch;
	float pos[3], pos2[3];
	int count = 0;
	int count2 = 0;

	glBegin(GL_LINES);
	for(int i=0; i<mesh.nregs; ++i)
	{
		const agContour *cont = findContourFromSet(cset, (u16)i);
		if(!cont || !cont->nverts)
		{
			continue;
		}

		getContourCenter(cont, orig, cs, ch, pos);
		for(int j=0; j<mesh.nrconns[i]; ++j)
		{
			const agContour *ncont = findContourFromSet(cset, mesh.conns[mesh.rcindexs[i]+j]);
			if(ncont)
			{
				getContourCenter(ncont, orig, ch, ch, pos2);
				Assertf(PointIsInBBox(cset, pos2), "Point pos2 (%f, %f, %f) is not in the bounding box! nverts is %i", pos2[0], pos2[1], pos2[2], ncont->nverts);
				Assertf(PointIsInBBox(cset, pos), "Point pos (%f, %f, %f) is not in the bounding box! nverts is %i", pos[0], pos[1], pos[2], ncont->nverts);
				if(ncont->nverts && PointIsInBBox(cset, pos2) && PointIsInBBox(cset, pos))
				{
					drawArc(pos, pos2);
					++count;
				}
			}
			++count2;
		}
	}
	glEnd();

	float col[4] = { 1,1,1,alpha };
	
	glPointSize(7.0f);
	glBegin(GL_POINTS);
	for (int i = 0; i < cset.nconts; ++i)
	{
		const agContour* cont = &cset.conts[i];
		intToCol(cont->reg, col);
		col[0] *= 0.5f;
		col[1] *= 0.5f;
		col[2] *= 0.5f;
		glColor4fv(col);
		getContourCenter(cont, orig, cs, ch, pos);
		glVertex3fv(pos);
	}
	glEnd();

	glLineWidth(1.f);
	glPointSize(1.f);	
}

void agDebugDrawStitchedPolys(const struct agPolyMesh& mesh)
{
	const int nvp = mesh.nvp;
	const float cs = mesh.cs;
	const float ch = mesh.ch;
	const float* orig = mesh.bmin;
	float col[4] = {1,1,1,0.75f};
	glBegin(GL_TRIANGLES);
	for (int i = 0; i < mesh.npolys; ++i)
	{
		//Only draw polys that are stitched
		if(!(mesh.regFlags[mesh.regs[i]]&AG_STITCH))
		{
			continue;
		}
		const unsigned short* p = &mesh.polys[i*nvp*2];
		intToCol(i, col);
		glColor4fv(col);
		unsigned short vi[3];
		for (int j = 2; j < nvp; ++j)
		{
			if (p[j] == 0xffff) break;
			vi[0] = p[0];
			vi[1] = p[j-1];
			vi[2] = p[j];
			for (int k = 0; k < 3; ++k)
			{
				const unsigned short* v = &mesh.verts[vi[k]*3];
				const float x = orig[0] + v[0]*cs;
				const float y = orig[1] + (v[1]+1)*ch;
				const float z = orig[2] + v[2]*cs;
				glVertex3f(x, y, z);
			}
		}
	}
	glEnd();

	// Draw tri boundaries
	glColor4ub(0,48,64,32);
	glLineWidth(1.5f);
	glBegin(GL_LINES);
	for (int i = 0; i < mesh.npolys; ++i)
	{
		//Only draw polys that are stitched
		if(!(mesh.regFlags[mesh.regs[i]]&AG_STITCH))
		{
			continue;
		}
		const unsigned short* poly = &mesh.polys[i*nvp*2];
		for (int j = 0; j < nvp; ++j)
		{
			if (poly[j] == 0xffff) break;
			if (poly[nvp+j] == 0xffff) continue;
			int vi[2];
			vi[0] = poly[j];
			if (j+1 >= nvp || poly[j+1] == 0xffff)
				vi[1] = poly[0];
			else
				vi[1] = poly[j+1];
			for (int k = 0; k < 2; ++k)
			{
				const unsigned short* v = &mesh.verts[vi[k]*3];
				const float x = orig[0] + v[0]*cs;
				const float y = orig[1] + (v[1]+1)*ch + 0.1f;
				const float z = orig[2] + v[2]*cs;
				glVertex3f(x, y, z);
			}
		}
	}
	glEnd();
	
	// Draw boundaries
	glLineWidth(2.5f);
	glColor4ub(0,48,64,220);
	glBegin(GL_LINES);
	for (int i = 0; i < mesh.npolys; ++i)
	{
		//Only draw polys that are stitched
		if(!(mesh.regFlags[mesh.regs[i]]&AG_STITCH))
		{
			continue;
		}
		const unsigned short* poly = &mesh.polys[i*nvp*2];
		for (int j = 0; j < nvp; ++j)
		{
			if (poly[j] == 0xffff) break;
			if (poly[nvp+j] != 0xffff) continue;
			int vi[2];
			vi[0] = poly[j];
			if (j+1 >= nvp || poly[j+1] == 0xffff)
				vi[1] = poly[0];
			else
				vi[1] = poly[j+1];
			for (int k = 0; k < 2; ++k)
			{
				const unsigned short* v = &mesh.verts[vi[k]*3];
				const float x = orig[0] + v[0]*cs;
				const float y = orig[1] + (v[1]+1)*ch + 0.1f;
				const float z = orig[2] + v[2]*cs;
				glVertex3f(x, y, z);
			}
		}
	}
	glEnd();
	glLineWidth(1.0f);
	/*
	glPointSize(3.0f);
	glColor4ub(0,0,0,220);
	glBegin(GL_POINTS);
	for (int i = 0; i < mesh.nverts; ++i)
	{
		const unsigned short* v = &mesh.verts[i*3];
		const float x = orig[0] + v[0]*cs;
		const float y = orig[1] + (v[1]+1)*ch + 0.1f;
		const float z = orig[2] + v[2]*cs;
		glVertex3f(x, y, z);
	}
	glEnd();
	glPointSize(1.0f);
	*/
}

void agDebugDrawRegsMesh(const struct agPolyMesh& mesh)
{
	const int nvp = mesh.nvp;
	const float cs = mesh.cs;
	const float ch = mesh.ch;
	const float* orig = mesh.bmin;
	float col[4] = {1,1,1,0.75f};
	glBegin(GL_TRIANGLES);
	for (int i = 0; i < mesh.npolys; ++i)
	{
		const unsigned short* p = &mesh.polys[i*nvp*2];
		intToCol(mesh.regs[i], col);
		glColor4fv(col);
		unsigned short vi[3];
		for (int j = 2; j < nvp; ++j)
		{
			if (p[j] == 0xffff) break;
			vi[0] = p[0];
			vi[1] = p[j-1];
			vi[2] = p[j];
			for (int k = 0; k < 3; ++k)
			{
				const unsigned short* v = &mesh.verts[vi[k]*3];
				const float x = orig[0] + v[0]*cs;
				const float y = orig[1] + (v[1]+1)*ch;
				const float z = orig[2] + v[2]*cs;
				glVertex3f(x, y, z);
			}
		}
	}
	glEnd();

	// Draw tri boundaries
	glColor4ub(0,48,64,32);
	glLineWidth(1.5f);
	glBegin(GL_LINES);
	for (int i = 0; i < mesh.npolys; ++i)
	{
		const unsigned short* poly = &mesh.polys[i*nvp*2];
		for (int j = 0; j < nvp; ++j)
		{
			if (poly[j] == 0xffff) break;
			if (poly[nvp+j] == 0xffff) continue;
			int vi[2];
			vi[0] = poly[j];
			if (j+1 >= nvp || poly[j+1] == 0xffff)
				vi[1] = poly[0];
			else
				vi[1] = poly[j+1];
			for (int k = 0; k < 2; ++k)
			{
				const unsigned short* v = &mesh.verts[vi[k]*3];
				const float x = orig[0] + v[0]*cs;
				const float y = orig[1] + (v[1]+1)*ch + 0.1f;
				const float z = orig[2] + v[2]*cs;
				glVertex3f(x, y, z);
			}
		}
	}
	glEnd();
	
	// Draw boundaries
	glLineWidth(2.5f);
	glColor4ub(0,48,64,220);
	glBegin(GL_LINES);
	for (int i = 0; i < mesh.npolys; ++i)
	{
		const unsigned short* poly = &mesh.polys[i*nvp*2];
		for (int j = 0; j < nvp; ++j)
		{
			if (poly[j] == 0xffff) break;
			if (poly[nvp+j] != 0xffff) continue;
			int vi[2];
			vi[0] = poly[j];
			if (j+1 >= nvp || poly[j+1] == 0xffff)
				vi[1] = poly[0];
			else
				vi[1] = poly[j+1];
			for (int k = 0; k < 2; ++k)
			{
				const unsigned short* v = &mesh.verts[vi[k]*3];
				const float x = orig[0] + v[0]*cs;
				const float y = orig[1] + (v[1]+1)*ch + 0.1f;
				const float z = orig[2] + v[2]*cs;
				glVertex3f(x, y, z);
			}
		}
	}
	glEnd();
	glLineWidth(1.0f);
	
	glPointSize(3.0f);
	glColor4ub(0,0,0,220);
	glBegin(GL_POINTS);
	for (int i = 0; i < mesh.nverts; ++i)
	{
		const unsigned short* v = &mesh.verts[i*3];
		const float x = orig[0] + v[0]*cs;
		const float y = orig[1] + (v[1]+1)*ch + 0.1f;
		const float z = orig[2] + v[2]*cs;
		glVertex3f(x, y, z);
	}
	glEnd();
	glPointSize(1.0f);
}

void agDebugDrawPolyMesh(const struct agPolyMesh& mesh)
{
	const int nvp = mesh.nvp;
	const float cs = mesh.cs;
	const float ch = mesh.ch;
	const float* orig = mesh.bmin;
	float col[4] = {1,1,1,0.75f};
	glBegin(GL_TRIANGLES);
	for (int i = 0; i < mesh.npolys; ++i)
	{
		const unsigned short* p = &mesh.polys[i*nvp*2];
		intToCol(i, col);
		glColor4fv(col);
		unsigned short vi[3];
		for (int j = 2; j < nvp; ++j)
		{
			if (p[j] == 0xffff) break;
			vi[0] = p[0];
			vi[1] = p[j-1];
			vi[2] = p[j];
			for (int k = 0; k < 3; ++k)
			{
				const unsigned short* v = &mesh.verts[vi[k]*3];
				const float x = orig[0] + v[0]*cs;
				const float y = orig[1] + (v[1]+1)*ch;
				const float z = orig[2] + v[2]*cs;
				glVertex3f(x, y, z);
			}
		}
	}
	glEnd();

	// Draw tri boundaries
	glColor4ub(0,48,64,32);
	glLineWidth(1.5f);
	glBegin(GL_LINES);
	for (int i = 0; i < mesh.npolys; ++i)
	{
		const unsigned short* poly = &mesh.polys[i*nvp*2];
		for (int j = 0; j < nvp; ++j)
		{
			if (poly[j] == 0xffff) break;
			if (poly[nvp+j] == 0xffff) continue;
			int vi[2];
			vi[0] = poly[j];
			if (j+1 >= nvp || poly[j+1] == 0xffff)
				vi[1] = poly[0];
			else
				vi[1] = poly[j+1];
			for (int k = 0; k < 2; ++k)
			{
				const unsigned short* v = &mesh.verts[vi[k]*3];
				const float x = orig[0] + v[0]*cs;
				const float y = orig[1] + (v[1]+1)*ch + 0.1f;
				const float z = orig[2] + v[2]*cs;
				glVertex3f(x, y, z);
			}
		}
	}
	glEnd();
	
	// Draw boundaries
	glLineWidth(2.5f);
	glColor4ub(0,48,64,220);
	glBegin(GL_LINES);
	for (int i = 0; i < mesh.npolys; ++i)
	{
		const unsigned short* poly = &mesh.polys[i*nvp*2];
		for (int j = 0; j < nvp; ++j)
		{
			if (poly[j] == 0xffff) break;
			if (poly[nvp+j] != 0xffff) continue;
			int vi[2];
			vi[0] = poly[j];
			if (j+1 >= nvp || poly[j+1] == 0xffff)
				vi[1] = poly[0];
			else
				vi[1] = poly[j+1];
			for (int k = 0; k < 2; ++k)
			{
				const unsigned short* v = &mesh.verts[vi[k]*3];
				const float x = orig[0] + v[0]*cs;
				const float y = orig[1] + (v[1]+1)*ch + 0.1f;
				const float z = orig[2] + v[2]*cs;
				glVertex3f(x, y, z);
			}
		}
	}
	glEnd();
	glLineWidth(1.0f);
	
	glPointSize(3.0f);
	glColor4ub(0,0,0,220);
	glBegin(GL_POINTS);
	for (int i = 0; i < mesh.nverts; ++i)
	{
		const unsigned short* v = &mesh.verts[i*3];
		const float x = orig[0] + v[0]*cs;
		const float y = orig[1] + (v[1]+1)*ch + 0.1f;
		const float z = orig[2] + v[2]*cs;
		glVertex3f(x, y, z);
	}
	glEnd();
	glPointSize(1.0f);
}

void agDebugDrawPolyMeshDetail(const struct agPolyMeshDetail& dmesh)
{
	float col[4] = {1,1,1,0.75f};
	
	glBegin(GL_TRIANGLES);
	for (int i = 0; i < dmesh.nmeshes; ++i)
	{
		const unsigned short* m = &dmesh.meshes[i*4];
		const unsigned short bverts = m[0];
		const unsigned short btris = m[2];
		const unsigned short ntris = m[3];
		const float* verts = &dmesh.verts[bverts*3];
		const unsigned char* tris = &dmesh.tris[btris*4];

		intToCol(i, col);
		glColor4fv(col);
		for (int j = 0; j < ntris; ++j)
		{
			glVertex3fv(&verts[tris[j*4+0]*3]);
			glVertex3fv(&verts[tris[j*4+1]*3]);
			glVertex3fv(&verts[tris[j*4+2]*3]);
		}
	}
	glEnd();

	// Internal edges.
	glLineWidth(1.0f);
	glColor4ub(0,0,0,64);
	glBegin(GL_LINES);
	for (int i = 0; i < dmesh.nmeshes; ++i)
	{
		const unsigned short* m = &dmesh.meshes[i*4];
		const unsigned short bverts = m[0];
		const unsigned short btris = m[2];
		const unsigned short ntris = m[3];
		const float* verts = &dmesh.verts[bverts*3];
		const unsigned char* tris = &dmesh.tris[btris*4];
		
		for (int j = 0; j < ntris; ++j)
		{
			const unsigned char* t = &tris[j*4];
			for (int k = 0, kp = 2; k < 3; kp=k++)
			{
				unsigned char ef = (u8)((t[3] >> (kp*2)) & 0x3);
				if (ef == 0)
				{
					// Internal edge
					if (t[kp] < t[k])
					{
						glVertex3fv(&verts[t[kp]*3]);
						glVertex3fv(&verts[t[k]*3]);
					}
				}
			}
		}
	}
	glEnd();
	
	// External edges.
	glLineWidth(2.0f);
	glColor4ub(0,0,0,64);
	glBegin(GL_LINES);
	for (int i = 0; i < dmesh.nmeshes; ++i)
	{
		const unsigned short* m = &dmesh.meshes[i*4];
		const unsigned short bverts = m[0];
		const unsigned short btris = m[2];
		const unsigned short ntris = m[3];
		const float* verts = &dmesh.verts[bverts*3];
		const unsigned char* tris = &dmesh.tris[btris*4];
		
		for (int j = 0; j < ntris; ++j)
		{
			const unsigned char* t = &tris[j*4];
			for (int k = 0, kp = 2; k < 3; kp=k++)
			{
				unsigned char ef = (u8)((t[3] >> (kp*2)) & 0x3);
				if (ef != 0)
				{
					// Ext edge
					glVertex3fv(&verts[t[kp]*3]);
					glVertex3fv(&verts[t[k]*3]);
				}
			}
		}
	}
	glEnd();
	
	glLineWidth(1.0f);

	glPointSize(3.0f);
	glBegin(GL_POINTS);
	for (int i = 0; i < dmesh.nmeshes; ++i)
	{
		const unsigned short* m = &dmesh.meshes[i*4];
		const unsigned short bverts = m[0];
		const unsigned short nverts = m[1];
		const float* verts = &dmesh.verts[bverts*3];
		for (int j = 0; j < nverts; ++j)
		{
			glColor4ub(0,0,0,64);
			glVertex3fv(&verts[j*3]);
		}
	}
	glEnd();
	glPointSize(1.0f);
}
