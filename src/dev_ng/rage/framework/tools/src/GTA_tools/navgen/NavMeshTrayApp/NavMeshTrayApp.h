#ifndef NAVMESHTRAYAPP_H
#define NAVMESHTRAYAPP_H

//***********************************************************************************************
//	Filename    : NavMeshTrayApp
//	Author      : James Broad
//	Description : This is a little app which minimises to the systray, and handles scheduled
//                builds for navmeshes.  The main impetus was because the Windows task scheduler
//                is no good for this, having issues with security settings on our network.
//***********************************************************************************************

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <commctrl.h>
#include <commdlg.h>
#include <crtdbg.h>
#include <mmsystem.h>
#include <shellapi.h>
#include <stdio.h>
#include <share.h>
#include <time.h>

#define SETTINGS_FILE_VERSION							4

#define MAX_BATCHFILENAME								2048
#define DEFAULT_MIN_IMG_SIZE_FOR_SUCCESSFUL_BUILD_KB	64
#define MAX_NUM_HOURS_FOR_BUILD_TO_COMPLETE				17
#define MAX_NUM_SECONDS_FOR_EXPORT_TO_COMPLETE			86400 // (24 hours)

#define DEFAULT_PROJECT_ROOT							0
#define DEFAULT_PATH_FOR_EXPORT							0
#define DEFAULT_MAPFILE_LOCATION						0
#define DEFAULT_NAVMESHES_IMG_PATH						0
#define DEFAULT_INI_FILENAME							0

#define DEFAULT_PATH_FOR_BATCH_FILES					"X:\\gta5\\tools\\bin\\navgen\\NavMeshMaker\\ScheduledBuild\\"
#define DEFAULT_EXPORTGEOMETRY_FILE						"ExportGeometry.bat"
#define DEFAULT_EXPORTREGION_FILE						"ExportRegion.bat"
#define DEFAULT_EXPORTCONTINUEFROM_FILE					"ExportContinueFrom.bat"
#define DEFAULT_EXPORTDLC_FILE							"ExportDLC.bat"
#define DEFAULT_JOINDLC_FILE							"JoinDLC.bat"
#define DEFAULT_BUILDWHENEXPORTCOMPLETE_FILE			"ScheduledCompile.bat"
#define DEFAULT_CHECKOUTNAVMESHES_FILE					"CheckOutNavmeshes.bat"
#define DEFAULT_CHECKIN_NAVMESHES_FILE					"CheckInNavmeshes.bat"
#define DEFAULT_CHECKIN_AUDMESHES_FILE					"CheckInAudmeshes.bat"
#define DEFAULT_CHECKIN_NAVNODES_FILE					"CheckInNavnodes.bat"
#define DEFAULT_CHECKIN_MAPPING_FILE					"CheckInMapping.bat"
#define DEFAULT_EXPORTER_EXE_FILE						"X:\\gta5\\tools\\bin\\navgen\\NavMeshMaker\\NavMeshGenerator.exe"
#define DEFAULT_DELTEMPFILES_FILE						"DeleteTempFiles.bat"
#define DEFAULT_GETLATESTTOOLS_FILE						"GetLatestAB.bat"
#define DEFAULT_GETLATESTDATA_FILE						"GetLatestJimmyP4.bat"
#define DEFAULT_GETLATESTEXE_FILE						"GetLatestJimmyExe.bat"

#define DEFAULT_HOUR_FOR_BUILD							0
#define DEFAULT_MINUTE_FOR_BUILD						0

struct TSettings
{
	void SetDefault();

	BOOL bGetLatestToolsFirst;
	BOOL bGetLatestDataFirst;
	BOOL bGetLatestExeFirst;
	BOOL bDeleteTempFilesBeforeBuild;
	BOOL bCheckNavmeshesIntoPerforce;
	BOOL bDoExport;
	BOOL bDoCompile;

	char projectRoot[MAX_BATCHFILENAME];
	char pathForExport[MAX_BATCHFILENAME];
	char navmeshesImgPath[MAX_BATCHFILENAME];
	char levelCommonPath[MAX_BATCHFILENAME];
	char iniFileName[MAX_BATCHFILENAME];
	char navmeshesListFile[MAX_BATCHFILENAME];
	char exporterExeFile[MAX_BATCHFILENAME];
	char gameExtraParams[MAX_BATCHFILENAME];

	char DLC_areasXML[MAX_BATCHFILENAME];
	char DLC_extraContentParams[MAX_BATCHFILENAME];
	char DLC_finalOutputFolder[MAX_BATCHFILENAME];


	char pathForBatchFiles[MAX_BATCHFILENAME];
	char batchFileForDelTempFiles[MAX_BATCHFILENAME];
	char batchFileForGetLatestTools[MAX_BATCHFILENAME];
	char batchFileForGetLatestData[MAX_BATCHFILENAME];
	char batchFileForGetLatestExe[MAX_BATCHFILENAME];
	char batchFileForExportGeometry[MAX_BATCHFILENAME];
	char batchFileForExportRegion[MAX_BATCHFILENAME];
	char batchFileForExportContinueFrom[MAX_BATCHFILENAME];
	char batchFileForExportDLC[MAX_BATCHFILENAME];
	char batchFileForJoinDLC[MAX_BATCHFILENAME];
	char batchFileForScheduledCompile[MAX_BATCHFILENAME];
	char batchFileForCompileNavmeshes[MAX_BATCHFILENAME];
	char batchFileForCheckOutNavmeshes[MAX_BATCHFILENAME];
	char batchFileForCheckInNavmeshes[MAX_BATCHFILENAME];
	char batchFileForCheckInAudmeshes[MAX_BATCHFILENAME];
	char batchFileForCheckInNavnodes[MAX_BATCHFILENAME];
	char batchFileForCheckInMapping[MAX_BATCHFILENAME];
	
	int iMinNavmeshesImgSizeForSuccessfulBuildKb;
	int iBuildHour24;
	int iBuildMinute;

	BOOL bWholeMapBuild;
	BOOL bContinueFrom;
	BOOL bRegion;
	BOOL bDLC;

	BOOL bIsMultiplayerDLC;

	int iRegionMinX;
	int iRegionMinY;
	int iRegionMaxX;
	int iRegionMaxY;
};

enum EBuildStage
{
	BS_None,
	BS_GetLatestTools,
	BS_GetLatestExe,
	BS_GetLatestData,
	BS_DeleteTempFiles,
	BS_ExportingCollision,
	BS_CompilingNavmeshes,
	BS_JoinDLC,

	BS_Finished
};


struct TAppData
{
	HWND hWnd;
	HINSTANCE hInstance;
	BOOL bMinimized;
	BOOL bForceStartBuildNow;
	BOOL bBuildIsCurrentlyActive;
	BOOL bScheduledBuildEnabled;
	BOOL bTimerMsgReceived;
	UINT32 iTimeUntilNextBuildMS;
	UINT32 iTimeBuildStartedMS;
	UINT32 lastScreenSaverKillTime;
	UINT32 iTimeAtStartOfExportStageMS;
	EBuildStage eBuildStage;
	HANDLE hBatchFileProcess;
	TSettings settings;
	FILE * pLogFile;
};
extern TAppData AppData;

#define WM_TRAY_MESSAGE					(WM_USER + 1)
#define TASKBAR_ICON_ID					16
#define POLL_BATCH_PROCESS_FREQUENCY	5000	// every 5 seconds
#define DEACTIVATE_SCREENSAVER_FREQ		10000	// every 10 seconds
#define STAY_ALIVE_TIMER_ID				1		// every second whilst we're building
#define SCHEDULED_BUILD_TIMER_ID		2		// at a calculated period

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow);

void CreateMainWindow();

BOOL MessageLoop();

BOOL CALLBACK NavMeshDialogProc(HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);

void HandleWMCOMMAND(int iNotifyCode, int iCtrlID, HWND hWnd);

void MinimizeToSystemTray();

void ToggleScheduledBuildControls(BOOL bEnabled);

void ToggleMapRegionCompileControls(BOOL bWholeMap, BOOL bContinueFrom, BOOL bRegion, BOOL bDLC);

void MaximizeToWindow();

//BOOL LoadSettings(TSettings & settings);

BOOL LoadSettings_New(TSettings & settings);

BOOL SaveSettings(TSettings & settings);

void OpenLogFile();
void CloseLogFile();

void InitControlsFromSettings();

void InitSettingFromControls();

void DisableDeprecatedControls();

void CalcTimeUntilNextBuild();

BOOL ShouldStartBuild();

BOOL StartNavmeshBuild();

BOOL DoNavMeshBuild();

BOOL IsCurrentlyBuilding();

BOOL AbortBuildIfTakenTooLong(BOOL bForceAbort=FALSE);

BOOL CheckFileExistsInExportFolder(const char * pFilename);

BOOL DoesNavMeshesImageAppearValid();

BOOL FinaliseIfBuildIsComplete();

HANDLE RunBatchFile(
	char * pFilename,
	char * pParam1 = NULL,
	char * pParam2 = NULL,
	char * pParam3 = NULL,
	char * pParam4 = NULL,
	char * pParam5 = NULL,
	char * pParam6 = NULL,
	char * pParam7 = NULL,
	char * pParam8 = NULL,
	char * pParam9 = NULL,
	char * pParam10 = NULL);

void BrowseForSettingsFile();
void BrowseForLstFile();
void BrowseForCompileBatchFile();

void GetFileNameToOpen(char * pResultBuffer, int iBufferSize, char * pPromptTxt, char * pFilter=NULL, char * pDefaultExtension=NULL);

void CloseRagWindow();

char * ConstructWindowTitle();

#ifdef _DEBUG
void DebugPrint(char * fmt, ...);
void DebugPrintSettings();
void DebugPrintTime(tm & Time);
void DebugPrintTime();
#else
inline void DebugPrint(char * fmt, ...) { }
inline void DebugPrintSettings() { }
inline void DebugPrintTime(tm & Time) { }
inline void DebugPrintTime() { }
#endif	// _DEBUG

#endif