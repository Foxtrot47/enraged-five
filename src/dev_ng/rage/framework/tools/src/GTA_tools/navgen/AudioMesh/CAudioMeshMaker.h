#ifndef AUD_MESH_MAKER
#define AUD_MESH_MAKER

class CAudioLevelMesh;
struct AudGenInputs;

struct FileList
{
	static const int MAX_FILES = 256;
	 FileList() : size(0) {}
	inline ~FileList()
	{
		clear();
	}

	void clear()
	{
		for (int i = 0; i < size; ++i)
		{
			delete [] files[i];
		}
		size = 0;
	}

	void add(const char* path)
	{
		if(size >= MAX_FILES)
		{
			return;
		}

		int n = strlen(path);
		files[size] = new char[n+1];
		strcpy(files[size], path);
		size++;
	}

	static int cmp(const void* a, const void* b)
	{
		return strcmp(*(const char**)a, *(const char**)b);
	}

	void sort()
	{
		if(size > 1)
		{
			qsort(files, size, sizeof(char*), cmp);
		}
	}

	char * files[MAX_FILES];
	int size;

};

class CAudioMeshMaker
{
public:
	CAudioMeshMaker() {}
	~CAudioMeshMaker() {}
	int Main(char * triFile = NULL, char * paramsFile = NULL, char * mode = NULL, char * inputPath = NULL);
private:
	static bool Raycast(CAudioLevelMesh &mesh, float *src, float *dst, float &tmin);
	static bool IntersectSegmentTriangle(const float* sp, const float* sq, const float * a, const float* b, const float* c, float &t);
	static void ScanDirectory(const char* path, const char* ext, FileList& list);
	static void ReadParamsFile(FILE * pfile, AudGenInputs &inputs);
};















#endif
