#ifndef NAVMESHMAKER_H
#define NAVMESHMAKER_H

//********************************************************************************************************
//
//	Filename : AudioMeshMaker.h
//	Author : Colin Walder / James Broad
//	RockstarNorth (c) 2010
//
//	------------------------------------------------------------------------------------------------------
//
//********************************************************************************************************

#include <vector>
using namespace std;
#include "fwMaths\Vector.h"

namespace rage
{
	class CNavMesh;
}
class CNavGen;
class CAudioMeshHolder;
class CSlicePlane;
struct TAudioMeshParams;

class CAudioMeshMakerApp
{
public:
	CAudioMeshMakerApp() { }
	~CAudioMeshMakerApp() { }

	static bool ParseParams(int argc, char *argv[]);
	static void ShowUsage();
	static void ReadDatFile();
	static void InitMeshFilenames();
	static bool ReadCmdFile();
	static bool GenerateAudioMeshes(void);
	static bool StitchAudioMeshes(void);
	static void OutputText(char * pText);
	static void ClearData();
	static TAudioMeshParams * InitAudioMeshParams(int iSectorX, int iSectorY, char * pNavMeshName = NULL);
};

// The two modes of operation
enum AudioMeshMode
{
	//Do the initial generation of audio meshes which creates intermediate .agi files which contain polymeshes plus data for stitching
	GenerateAudMeshes,
	//Stitch audio meshes together forming audio connections between adjacent meshes
	StitchAudMeshes
};

struct TAudioMeshParams
{
	TAudioMeshParams()
	{
		m_pFileName = NULL;
		m_iStartSectorX = -1;
		m_iStartSectorY = -1;
	}
	~TAudioMeshParams();

	char * m_pFileName;
	int m_iStartSectorX;
	int m_iStartSectorY;
};	



#endif

