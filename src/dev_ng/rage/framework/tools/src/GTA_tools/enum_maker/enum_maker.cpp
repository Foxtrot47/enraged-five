#include <direct.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>	//	.h>
#include <set>

#include "basetypes.h"

#include "string\stringhash.h"

FILE *fpAllModelIndices = NULL;		//	The output .sch file
bool bWrittenFirstEntry = false;
bool bWrittenAtLeastOneEntry = false;

std::set<std::string> model_names_already_added;

#define MAX_NAME_LENGTH	(64)

//////////////////////////////////////////////////////////////////////////
// FUNCTION : IsInt
// PURPOSE :  Returns true if this string is a proper int (only numbers)
//			  This string should have spaces stripped out already.
//////////////////////////////////////////////////////////////////////////
Bool8 IsInt(char String[])
{
	Int16	Index;

	Index = 0;

	if (String[0] == 0) return (0);

	while (String[Index] != 0)
	{
		if ( (String[Index] < '0' || String[Index] > '9') )	//	&& String[Index] != '-')
		{
			if ( (String[Index] == '-') && (Index == 0) )
			{

			}
			else
			{
				return (0);
			}
		}

		Index++;
	}
	return (1);
}


void RemoveFirstNCharactersFromString(char String[], Int16 NumberOfChars)
{
	Int16 CurrentChar = NumberOfChars;

	// Copy the whole thing back a bit
	while (String[CurrentChar] != 0)
	{
		String[CurrentChar - NumberOfChars] = String[CurrentChar];
		CurrentChar++;
	}
	String[CurrentChar - NumberOfChars] = 0;
}


void RemoveFirstWordFromString(char String[])
{
	Int16 Search;

	Search = 0;

	// Skip initial blank spaces
	while (String[Search] == ' ') Search++;
	// Skip first word
	while (String[Search] != ' ' && String[Search] != 0) Search++;
	while (String[Search] == ' ') Search++;

	RemoveFirstNCharactersFromString(String, Search);
}


//////////////////////////////////////////////////////////////////////////
// FUNCTION : LineBeginsWithString
// PURPOSE :  Checks whether the first word in LineString matches 
//				StringToCheckFor and returns TRUE if it does.
//
//////////////////////////////////////////////////////////////////////////
Bool8 LineBeginsWithString(char LineString[], char StringToCheckFor[])
{
	char FirstWord[128];
	UInt16 SourceIndex, DestIndex;

	SourceIndex = 0;
	DestIndex = 0;

//	Ignore any leading spaces
	while ((LineString[SourceIndex] == 9) || (LineString[SourceIndex] == ' '))
	{
		if (LineString[SourceIndex] == 0)
		{
			return FALSE;
		}
		
		SourceIndex++;
	}

	while ((LineString[SourceIndex] != 9) 
		&& (LineString[SourceIndex] != ' ')
		&& (LineString[SourceIndex] != 0)
		&& (DestIndex < 127))
	{
		FirstWord[DestIndex++] = LineString[SourceIndex++];
	}

	FirstWord[DestIndex] = 0;

	if (!strcmp(FirstWord, StringToCheckFor))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


//////////////////////////////////////////////////////////////////////////
// FUNCTION : ContainsLetters
// PURPOSE :  Returns true if this string is a proper string (has at least 
//				one letter)
//			  This string should have spaces stripped out already.
//////////////////////////////////////////////////////////////////////////
bool ContainsLetters(char String[])
{
	UInt16 string_length, loop;

	string_length = (UInt16)strlen(String);

	loop = 0;

	while (loop < string_length)
	{
		if (((String[loop] >= 'a' && String[loop] <= 'z') ||
			 (String[loop] >= 'A' && String[loop] <= 'Z')))
		{
			return TRUE;
		}

		loop++;
	}

	return FALSE;
}


bool LineBeginsWithNumber(char LineString[])
{
	char FirstWord[128];
	UInt16 SourceIndex, DestIndex;

	SourceIndex = 0;
	DestIndex = 0;

//	Ignore any leading spaces
	while ((LineString[SourceIndex] == 9) || (LineString[SourceIndex] == ' ') || (LineString[SourceIndex] == ','))
	{
		if (LineString[SourceIndex] == 0)
		{
			return FALSE;
		}
		
		SourceIndex++;
	}

	while ((LineString[SourceIndex] != 9) 
		&& (LineString[SourceIndex] != ' ')
		&& (LineString[SourceIndex] != 0)
		&& (LineString[SourceIndex] != ',')
		&& (DestIndex < 127))
	{
		FirstWord[DestIndex++] = LineString[SourceIndex++];
	}

	FirstWord[DestIndex] = 0;

	if (IsInt(FirstWord))
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


Int16 Process1LineOfObjectData(char *ObjectDataString)
{
	char ProcessedString[1024];
	char TempString[128];

	Int16 StringLength;
	UInt16 CharCounter;

	char ObjectName[MAX_NAME_LENGTH];
	UInt32 ObjectIndex;
	bool bObjectIndexHasBeenRead;

	StringLength = 0;
	while (ObjectDataString[StringLength] != 0)
	{
		ProcessedString[StringLength] = ObjectDataString[StringLength];
		if (ProcessedString[StringLength] >= 'a' && ProcessedString[StringLength] <= 'z')
		{
			ProcessedString[StringLength] += 'A'-'a';
		}
		StringLength++;
	}
	ProcessedString[StringLength] = 0;

	if (StringLength > 1024)
	{
		fprintf(stderr, "**** Line in object data has more than 1024 characters\n");	//	ErrorString
		return(-1);
	}

	// We have to replace tabs by spaces. sscanf doesn't seem to deal with them properly.
	CharCounter = 0;
	while (ProcessedString[CharCounter] != 0)
	{
		if (ProcessedString[CharCounter] == 9) ProcessedString[CharCounter] = ' ';
		CharCounter++;
	}

	// replace commas and brackets with spaces. (these are separators anyway)
	//	Some model names have brackets in them so replace brackets with _ instead (later)
	for (CharCounter = 0; CharCounter < StringLength; CharCounter++)
	{
		if (ProcessedString[CharCounter] == ',') ProcessedString[CharCounter] = ' ';
//		else if (ProcessedString[CharCounter] == '(') ProcessedString[CharCounter] = ' ';
//		else if (ProcessedString[CharCounter] == ')') ProcessedString[CharCounter] = ' ';
	}

	if (sscanf(ProcessedString, "%s", TempString) <= 0)
	{
//		printf("**** Couldn't read first item in string");	//	ErrorString
//		return(-1);
		return 0;
	}

	if (TempString[0] == '#')
	{	//	This line is commented out so ignore it - just return now
		return(0);
	}

	bObjectIndexHasBeenRead = false;

//	If the line begins with an integer then assume that this is the model index and the next string will be the model name.
//	If the line begins with a string then assume that the line does not contain a model index and calculate a hash key from 
//	the model name to use as a unique integer for the model
	if (IsInt(TempString))
	{
//		ObjectIndex = atoi(TempString);

		RemoveFirstWordFromString(ProcessedString);

//		bObjectIndexHasBeenRead = true;
	}

	if (sscanf(ProcessedString, "%s", ObjectName) <= 0)
	{
		fprintf(stderr, "**** Couldn't read object model name from line in data file\n");	//	ErrorString
		return(-1);
	}

	if (!ContainsLetters(ObjectName))
	{
		fprintf(stderr, "**** Expected object model name - item in data file is not a string\n");	//	ErrorString
		return(-1);
	}

	if (strlen(ObjectName) > MAX_NAME_LENGTH)
	{
		fprintf(stderr, "**** Object name is too long %s\n", ObjectName);	//	ErrorString
		return(-1);
	}

	// Can ignore rest of line in file
	if (bObjectIndexHasBeenRead == false)
	{
		ObjectIndex = rage::atStringHash(ObjectName);
	}

	if (fpAllModelIndices)
	{
		bool bSafeToWrite = true;

		if (model_names_already_added.find(ObjectName) != model_names_already_added.end())
		{
			bSafeToWrite = false;
		}
		else
		{
			for (CharCounter = 0; CharCounter < strlen(ObjectName); CharCounter++)
			{
				if (ObjectName[CharCounter] == '-')
				{
	//				printf("**** %s contains a - this has been replaced by a _\n", ObjectName);
					printf("**** %s contains a - so it's been ignored\n", ObjectName);
					ObjectName[CharCounter] = '_';
					bSafeToWrite = false;
				}

				if (ObjectName[CharCounter] == '+')
				{
	//				printf("**** %s contains a + this has been replaced by a _\n", ObjectName);
					printf("**** %s contains a + so it's been ignored\n", ObjectName);
					ObjectName[CharCounter] = '_';
					bSafeToWrite = false;
				}

				if (ObjectName[CharCounter] == '=')
				{
	//				printf("**** %s contains a = this has been replaced by a _\n", ObjectName);
					printf("**** %s contains a = so it's been ignored\n", ObjectName);
					ObjectName[CharCounter] = '_';
					bSafeToWrite = false;
				}

				if (ObjectName[CharCounter] == '(')
				{
	//				printf("**** %s contains a ( this has been replaced by a _\n", ObjectName);
					printf("**** %s contains a ( so it's been ignored\n", ObjectName);
					ObjectName[CharCounter] = '_';
					bSafeToWrite = false;
				}

				if (ObjectName[CharCounter] == ')')
				{
	//				printf("**** %s contains a ) this has been replaced by a _\n", ObjectName);
					printf("**** %s contains a ) so it's been ignored\n", ObjectName);
					ObjectName[CharCounter] = '_';
					bSafeToWrite = false;
				}

				if (ObjectName[CharCounter] == '.')
				{
	//				printf("**** %s contains a . this has been replaced by a _\n", ObjectName);
					printf("**** %s contains a . so it's been ignored\n", ObjectName);
					ObjectName[CharCounter] = '_';
					bSafeToWrite = false;
				}

				if (ObjectName[CharCounter] == '$')
				{
	//				printf("**** %s contains a $ this has been replaced by a _\n", ObjectName);
					printf("**** %s contains a $ so it's been ignored\n", ObjectName);
					ObjectName[CharCounter] = '_';
					bSafeToWrite = false;
				}

				if (ObjectName[CharCounter] == '�')
				{
	//				printf("**** %s contains a � this has been replaced by a _\n", ObjectName);
					printf("**** %s contains a � so it's been ignored\n", ObjectName);
					ObjectName[CharCounter] = '_';
					bSafeToWrite = false;
				}
			}

			if ( (ObjectName[0] < 'A') || (ObjectName[0] > 'Z') )
			{	//	Object name begins with a number so stick a letter 'a' in front of it
	//			fprintf(fpAllModelIndices, "a%s = %d", ObjectName, ObjectIndex);
				printf("**** %s doesn't begin with a letter so it's been ignored\n", ObjectName);
				bSafeToWrite = false;
			}
		}

		if (bSafeToWrite)
		{
			if (bWrittenFirstEntry)
			{
				fprintf(fpAllModelIndices, ",\n");
			}

			fprintf(fpAllModelIndices, "%s=%d", ObjectName, ObjectIndex);
			model_names_already_added.insert(ObjectName);

			bWrittenFirstEntry = true;
			bWrittenAtLeastOneEntry = true;
		}
	}

	return 0;
}



int ReadOneLine(FILE *fp, char String[], UInt16 MaxStringLength)
{
	Int16	NumChar;
	Int16	Char;

	NumChar = 0;

	Char = getc(fp);
	while (Char != EOF && Char != '\n' && Char != 0)
	{
		String[NumChar++] = (char)Char;
		Char = getc(fp);
	}
	
	String[NumChar] = (char)0;

	if (Char == EOF && NumChar == 0)
	{
		return (EOF);
	}

	if (NumChar >= MaxStringLength)
	{
		fprintf(stderr, "**** ReadOneLine - Line in file is too long\n");	//	ErrorString
		return (-2);
	}

	return (NumChar);
}

//	Set this to true when finding all the model names used by the scripts
bool bReadingListOfUniqueUsedModelNames = false;

Int16 LoadObjectData(char *filename, char *block1name, char *block2name, char *block3name, bool bDealWithMloBlock)
{
	FILE *pFile;

	char InputString[1024];
	bool ReadingObjects;
	bool ReadingMlos;
	bool AlreadyReadingAMlo;

	int return_error = -1;
	
	ReadingObjects = FALSE;
	ReadingMlos = FALSE;
	AlreadyReadingAMlo = FALSE;

if (bReadingListOfUniqueUsedModelNames)
{
	ReadingObjects = TRUE;
}

	if ((pFile = fopen( filename, "r" )) == NULL)
    {
		fprintf(stderr, "**** Couldn't open .ide file %s\n", filename);	//	ErrorString
		return(-1);
    }

	while ((return_error = ReadOneLine(pFile, InputString, 1024)) >= 0)
	{
//		if ( LineBeginsWithString(InputString, "objs") || LineBeginsWithString(InputString, "tobj") 
//				|| LineBeginsWithString(InputString, "anim") )
		if ( LineBeginsWithString(InputString, block1name) || LineBeginsWithString(InputString, block2name) 
				|| LineBeginsWithString(InputString, block3name) )
		{
			ReadingObjects = TRUE;
		}
		else if (bDealWithMloBlock && LineBeginsWithString(InputString, "mlo") )
		{
			ReadingMlos = TRUE;
			AlreadyReadingAMlo = FALSE;
		}
		else if (ReadingObjects)
		{
			if (LineBeginsWithString(InputString, "end"))
			{
				ReadingObjects = FALSE;
			}
			else
			{	//	Add the data from this line to the array of objects
				Process1LineOfObjectData(InputString);
			}
		}
		else if (ReadingMlos)
		{
			if (LineBeginsWithString(InputString, "end"))
			{
				ReadingMlos = FALSE;
				AlreadyReadingAMlo = FALSE;
			}
			else
			{
				if (AlreadyReadingAMlo)
				{
					if (LineBeginsWithString(InputString, "mloend"))
					{
						AlreadyReadingAMlo = FALSE;
					}
				}
				else
				{	//	Add the data from this line to the array of objects
					Process1LineOfObjectData(InputString);
					AlreadyReadingAMlo = TRUE;
				}
			}
		}
	}
	if (return_error < 0 && return_error != EOF)
	{
		fprintf(stderr, "**** Error when reading data from file %s\n", filename);	//	ErrorString
		return -1;
	}

	fclose(pFile);

	return 0;
}

Int16 Process1LineOfIDEFileList(char *IDEFileString)
{
	char ProcessedString[255];
	char TempString[128];
	
	Int16 StringLength;
	UInt16 CharCounter;
	
	char IDEFilename[255];

	StringLength = 0;
	while (IDEFileString[StringLength] != 0)
	{
		ProcessedString[StringLength] = IDEFileString[StringLength];
		if (ProcessedString[StringLength] >= 'a' && ProcessedString[StringLength] <= 'z')
		{
			ProcessedString[StringLength] += 'A' - 'a';
		}
		StringLength++;
	}
	ProcessedString[StringLength] = 0;
	
	if (StringLength > 255)
	{
		fprintf(stderr, "**** Line in file containing names of IDE files has more than 255 characters\n");	//	ErrorString
		return(-1);
	}
	
	// We have to replace tabs by spaces. sscanf doesn't seem to deal with them properly.
	CharCounter = 0;
	while (ProcessedString[CharCounter] != 0)
	{
		if (ProcessedString[CharCounter] == 9) ProcessedString[CharCounter] = ' ';
		CharCounter++;
	}

	// replace commas and brackets with spaces. (these are separators anyway)
	for (CharCounter = 0; CharCounter < StringLength; CharCounter++)
	{
		if (ProcessedString[CharCounter] == ',') ProcessedString[CharCounter] = ' ';
		else if (ProcessedString[CharCounter] == '(') ProcessedString[CharCounter] = ' ';
		else if (ProcessedString[CharCounter] == ')') ProcessedString[CharCounter] = ' ';
	}

	if (sscanf(ProcessedString, "%s", TempString) <= 0)
	{
		fprintf(stderr, "**** Couldn't read first item in string\n");	//	ErrorString
		return(-1);
	}

	enum eReadingFileOfType
	{
		FILE_TYPE_MAP,
		FILE_TYPE_DEFAULT,
		FILE_TYPE_PEDS,
		FILE_TYPE_VEHICLES
	};

	eReadingFileOfType ReadingFileType = FILE_TYPE_MAP;

	if (strcmp(TempString, "IDE") == 0)
	{
		ReadingFileType = FILE_TYPE_MAP;
	}
	else if (strcmp(TempString, "DEFAULT") == 0)
	{
		ReadingFileType = FILE_TYPE_DEFAULT;
	}
	else if (strcmp(TempString, "PEDS") == 0)
	{
		ReadingFileType = FILE_TYPE_PEDS;
	}
	else if (strcmp(TempString, "VEHICLES") == 0)
	{
		ReadingFileType = FILE_TYPE_VEHICLES;
	}
	else
	{
		fprintf(stderr, "**** First item in string is not IDE, DEFAULT, PEDS or VEHICLES\n");	//	ErrorString
		return(-1);
	}
	
	RemoveFirstWordFromString(ProcessedString);
	
	if (sscanf(ProcessedString, "%s", TempString) <= 0)
	{
		fprintf(stderr, "**** Couldn't read second item in string\n");	//	ErrorString
		return(-1);
	}

	if (!ContainsLetters(TempString))
	{
		fprintf(stderr, "**** Second item in string is not a string\n");	//	ErrorString
		return(-1);
	}

	if (sscanf(TempString, "%s", IDEFilename) <= 0)
	{
		fprintf(stderr, "**** Couldn't read second item (string) in string\n");	//	ErrorString
		return(-1);
	}

	if (strlen(IDEFilename) > 255)
	{
		fprintf(stderr, "**** IDE Filename is too long %s\n", IDEFilename);	//	ErrorString
		return(-1);
	}

	switch (ReadingFileType)
	{
		case FILE_TYPE_MAP :
		{
			if (strncmp(IDEFilename, "COMMON:/", 8) == 0)
			{
				RemoveFirstNCharactersFromString(IDEFilename, 8);
			}
			else if (strncmp(IDEFilename, "PLATFORM:/", 10) == 0)
			{
				RemoveFirstNCharactersFromString(IDEFilename, 10);
			}
			else
			{
				fprintf(stderr, "**** IDE Filename does not begin with common:/ or platform:/ %s\n", IDEFilename);	//	ErrorString
				return(-1);
			}

			bool bIsLevels = !strncmp(IDEFilename, "LEVELS", 6) && IDEFilename[6] == 47;
			bool bIsData = !strncmp(IDEFilename, "DATA", 4) && IDEFilename[4] == 47;

			if (bIsLevels || bIsData)
			{
				if (LoadObjectData(&IDEFilename[0], "objs", "tobj", "anim", true) < 0)
				{
					return(-1);
				}
			}
			else
			{
				fprintf(stderr, "**** IDE Filename does not begin with DATA or LEVELS\\ %s\n", IDEFilename);	//	ErrorString
				return(-1);
			}
		}
		break;

		//	default, peds and vehicles ide files should have absolute paths
		case FILE_TYPE_DEFAULT :
		{
			if (LoadObjectData(&IDEFilename[0], "objs", "weap", "hier", false) < 0)
			{
				fprintf(stderr, "**** Problem when reading from default ide file %s\n", IDEFilename);
				return(-1);
			}
		}
			break;

		case FILE_TYPE_PEDS :
		{
			if (LoadObjectData(&IDEFilename[0], "peds", "peds", "peds", false) < 0)
			{
				fprintf(stderr, "**** Problem when reading from peds ide file %s\n", IDEFilename);
				return(-1);
			}
		}
			break;

		case FILE_TYPE_VEHICLES :
		{
			if (LoadObjectData(&IDEFilename[0], "cars", "cars", "cars", false) < 0)
			{
				fprintf(stderr, "**** Problem when reading from vehicles ide file %s\n", IDEFilename);
				return(-1);
			}
		}
			break;

		default :
			fprintf(stderr, "**** File is not of type IDE, DEFAULT, PEDS or VEHICLES %s \n", IDEFilename);
			return(-1);
			break;
	}

	return 0;
}



Int16 LoadObjectDataFromMultipleFiles(char *filename, char *directory_containing_the_map_files)
{
	FILE *pDatFile;
	
	char InputString[255];

	int return_error = -1;
	
	if ((pDatFile = fopen(filename, "r")) == NULL)
	{
		fprintf(stderr, "**** Couldn't open .dat file %s\n", filename);	//	ErrorString
		return(-1);
	}
	
	_chdir(directory_containing_the_map_files);

	while ((return_error = ReadOneLine(pDatFile, InputString, 255)) >= 0)
	{
		if (LineBeginsWithString(InputString, "IDE") || 
			LineBeginsWithString(InputString, "DEFAULT") || 
			LineBeginsWithString(InputString, "PEDS") || 
			LineBeginsWithString(InputString, "VEHICLES") )
		{
		//	This line should consist of IDE, DEFAULT, PEDS or VEHICLES followed by a filename
		//	The default, peds and vehicles filenames should contain an absolute path
			if (Process1LineOfIDEFileList(InputString) < 0)
			{
				return(-1);
			}
		}
	}
	if (return_error < 0 && return_error != EOF)
	{
		fprintf(stderr, "**** Error when reading line from .dat file %s\n", filename);	//	ErrorString
		return -1;
	}

	fclose(pDatFile);
	
	return 0;
}



bool CreateEnumFile(char *name_of_dat_file, char *directory_containing_map_files, char *name_of_additional_dat_file, char *directory_containing_additional_map_files, char *file_to_write)
{
	model_names_already_added.clear();

	if (bReadingListOfUniqueUsedModelNames)
	{
		fpAllModelIndices = fopen("x:/gta/gta_rage_scripts/unique_with_hash.txt", "w");
	}
	else
	{
		fpAllModelIndices = fopen(file_to_write, "w");
	}
	if (fpAllModelIndices == NULL)
	{
		fprintf(stderr, "**** Couldn't open %s for writing - check it is writable\n", file_to_write);	//	ErrorString
		return false;
	}

	fprintf(fpAllModelIndices, "ENUM MODEL_NAMES\n");
	fprintf(fpAllModelIndices, "DUMMY_MODEL_FOR_SCRIPT=0,\n");

	bWrittenFirstEntry = false;
	bWrittenAtLeastOneEntry = false;


	if (bReadingListOfUniqueUsedModelNames)
	{
		if (LoadObjectData("x:/gta/gta_rage_scripts/unique_symbols.txt", "objs", "tobj", "anim", true) < 0)
		{
			return false;
		}
	}
	else
	{
		if (LoadObjectDataFromMultipleFiles(name_of_dat_file, directory_containing_map_files) < 0)
		{
			fprintf(stderr, "**** Problem when reading from map ide files\n");	//	ErrorString
			return false;
		}

		if ( (strcmp(name_of_additional_dat_file, "NULL")) 
			&& (strcmp(directory_containing_additional_map_files, "NULL")) )
		{
			if (LoadObjectDataFromMultipleFiles(name_of_additional_dat_file, directory_containing_additional_map_files) < 0)
			{
				fprintf(stderr, "**** Problem when reading from additional map ide files\n");	//	ErrorString
				return false;
			}
		}
	}

	if (bWrittenAtLeastOneEntry)
	{
		fprintf(fpAllModelIndices, "\n");
	}
	fprintf(fpAllModelIndices, "ENDENUM\n");
	fclose(fpAllModelIndices);
	fpAllModelIndices = NULL;

	return true;
}



int main( int argc, char *argv[] )
{
	if (argc != 6)
	{
		fprintf(stderr, "**** Expected five arguments - name of map_files_that_the_script_needs_to_know_about.dat file, absolute path for directory containing all the map files, name of additional map_files_that_the_script_needs_to_know_about.dat file (specify NULL if you don't need this), absolute path for directory containing all the additional map files (specify NULL if you don't need this), absolute path for new model_enums.sch file (including filename) ****\n");
		return (-1);
	}

	if (CreateEnumFile(argv[1], argv[2], argv[3], argv[4], argv[5]) == false)
	{
		fprintf(stderr, "**** Error occurred ****\n");
		return (-1);
	}

	return 0;
}
