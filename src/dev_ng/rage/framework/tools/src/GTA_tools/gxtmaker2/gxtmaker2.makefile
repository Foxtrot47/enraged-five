Project gxtmaker2 
ConfigurationType exe 
RootDirectory . 

IncludePath $(RAGE_DIR)\base\src
IncludePath $(RAGE_DIR)\suite\src
IncludePath $(RAGE_DIR)\framework\tools\src\dcc\libs
IncludePath X:\3rdParty\dev\cli\libxml2-2.7.6\include

Files { 
    gxtmaker2.cpp 
} 
Libraries { 
    %RAGE_DIR%\base\src\vcproj\RageCore\RageCore
    ..\..\dcc\libs\RsULog\RsULog
    X:\3rdParty\dev\cli\libxml2-2.7.6\vcproj\libxml2
} 

