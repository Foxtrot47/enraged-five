// gxtmaker2
// Copyright (C) 1999-2012, Rockstar Games

#include "atl/map.h"
#include "string/stringhash.h"
#include "system/endian.h"
#include "system/main.h"
#include "system/platform.h"
#include "system/param.h"
#include "system/timer.h"
#include "system/new.h"
#include "file/stream.h"
#include "file/packfile_builder.h"
#include "RsULog/ULogger.h"
#include <algorithm>
#include <vector>
#include <cctype>
#include <sstream>
#include <iostream>

static UniversalLogHelper LogHelper = UniversalLogHelper::GetInstance();
static UniversalLogFile *Log = NULL; 

static bool hadError(false), hadWarning(false);

static bool outputtingToMetadata = false;
static bool outputtingTxtFile = false;
static bool patchText = false;
static bool processingpatchtext = false;
static bool nopatchdupewarn = false;
static bool verbose = false;
static bool quiet = false;
static bool noLogWindow = false;

#define MAX_PATCHFILES 100
static char g_patchfilelist[MAX_PATCHFILES][RAGE_MAX_PATH];
static int patchfilecnt=0;

static rage::fiRpf7Builder builder(0);

// when adding a new platform filter, be sure to add another %s to the _ERROR string so it'll output properly
#define VALID_PLATFORM_STRINGS	"ps3", "360", "pc", "ps4", "xb1"
#define VALID_PLATFORM_ERROR	"\"%s\", \"%s\", \"%s\", \"%s\", or \"%s\""
#define PLATFORM_ELSE			"else"

static std::vector<std::string> g_PlatformFilters;
static const char* g_pCurPlatform = NULL;

static char const c_cDataStart[] = "<![CDATA[";
static char const c_cDataEnd[] = "]]>";

struct String {
	static const size_t NumHashChains = 1023;
	String(const char *code,const char *txt, const char* platformString, const char* textComment, const char *path,
			const char *fileName, int line) 
		: Code( strupr( rage::StringDuplicate(code) ) )
		, Text( rage::StringDuplicate(txt) )
		, PlatformString( 0 )
		, TextComment( 0 )
		, Path( rage::StringDuplicate(path) )
		, FileName( rage::StringDuplicate( fileName ) )
		, Line(line) 
	{
		Hash = rage::atStringHash(code);
		Next = 0;
		// Add to the correct global hash chain
		NextByHash = FirstByHash[Hash % NumHashChains];
		FirstByHash[Hash % NumHashChains] = this;
		++TotalStrings;

		// copy the optional platform string
		if( platformString && platformString[0] != '\0' )
		{
			PlatformString = rage::StringDuplicate( platformString );
		}

		if( textComment && textComment[0] != '\0' )
		{
			TextComment = rage::StringDuplicate( textComment );
		}
	}
	~String() 
	{
		rage::StringFree(Code);
		rage::StringFree(Text);

		if( PlatformString )
		{
			rage::StringFree(PlatformString);
		}

		if( TextComment )
		{
			rage::StringFree(TextComment);
		}

		rage::StringFree(Path);
		rage::StringFree(FileName);

		Next = NULL;
	}

	static void Cleanup( String*& first )
	{
		String* target = first;
		while( target )
		{
			String* next = target->Next;
			delete target;
			target = next;
		}

		first = NULL;
	}

	static String *lookupCode(const char *code, const char* platform, bool bSuppressWarning = false, bool ignorePlatformString = true ) 
	{
		unsigned hash = rage::atStringHash(code);

		for (String *s=String::FirstByHash[hash % NumHashChains]; s; s=s->NextByHash)
		{
			if ( s->Hash == hash && // Hashes match...
				( ignorePlatformString || // Do we care about the platform string?
				( 
				( s->PlatformString  == 0 && ( platform == 0 || ( platform && platform[0] == '\0' ) ) ) || // both strings are any platform or..
				( s->PlatformString && platform && strcmp( s->PlatformString, platform ) == 0 ) // ...both strings are same platform
				)
				) )
			{
				if( !bSuppressWarning )
					printf("%s(%d): First seen here.\n",s->Path,s->Line);

				return s;
			}
		}

		return NULL;
	}

	char *Code, *Text;
	char * PlatformString;
	char * TextComment;
	const char* Path;
	const char* FileName;
	unsigned Hash;
	int Line;
	const char*section;
	String *Next, *NextByHash;

	static String *FirstByHash[NumHashChains];
	static int TotalStrings;
};

String *String::FirstByHash[String::NumHashChains];
int String::TotalStrings = 0;
int TotalStringsPatched = 0;

struct PackedString {
	unsigned Hash;
	const char *Text;
	bool operator<(const PackedString &that) const {
		return this->Hash < that.Hash;
	}
};

bool byteSwap;

inline void writeInt(rage::u8 *&dp,unsigned i) {
	if (byteSwap)
		i = rage::sysEndian::Swap(i);
	*(unsigned*)dp = i;
	dp += sizeof(unsigned);
}

struct Section {
	Section(const char *name) 
	{
		Next = First;
		First = this;
		Code = NULL;
		CodeCount = 0;
		rage::safecpy(Name,name);
		Size = 16;		// header and footer
	}

	~Section()
	{
		if( Code )
		{
			String::Cleanup( Code );
		}
	}

	static Section *lookup(const char *name) {
		for (Section *s = First; s; s=s->Next)
			if (!strcmp(s->Name,name))
				return s;
		return NULL;
	}

	static Section *create(const char *name) {
		Section *s = lookup(name);
		if (!s)
			s = rage_new Section(name);
		return s;
	}

	void addString(const char *code,const char *txt, const char* platformString, const char* commentString, 
		const char *path, const char *fileName, int line )
	{
		String *c = String::lookupCode(code,platformString,true,false);
		if(c!=NULL)
		{
			Assertf(!stricmp(Name,c->section),"Text code [%s] from section '%s' duplicated in section '%s'",code,c->section,Name);
			if(stricmp(Name,c->section))
			{
				LogHelper.SetProgressMessage("ERROR: Text code [%s] from section '%s' (%s:%d) duplicated in different section '%s' (%s:%d)",code,c->section,c->Path,c->Line,Name,path,line);
				//hadError = true;
				c=NULL;	// should abort, but for now we'll report the error and continue with the original behavior (allocate redundant/unused string) so as not to introduce any changes yet
			}
		}
		if(c==NULL)
		{
			c = rage_new String(code,txt,platformString,commentString,path,fileName,line);
			c->Next = Code;
			Code = c;
			++CodeCount;
			Size += 8 + ustrlen(txt) + 1;
			c->section=Name;

			//LogHelper.SetProgressMessage("String added  - %s %s(%d) %s", code, path, line, txt);
		}
		else
		{
			if(!strcmp(c->Text,txt) && !nopatchdupewarn)
			{
				if(processingpatchtext)
					LogHelper.SetProgressMessage("WARNING: Patched text is the same - %s %s(%d)->%s(%d)  %s",code,c->Path,c->Line,path,line,platformString&&platformString[0]?platformString:"");
				else
					LogHelper.SetProgressMessage("INFO: Duplicate string is the same - %s %s(%d)->%s(%d)  %s",code,c->Path,c->Line,path,line,platformString&&platformString[0]?platformString:"");
				if(!quiet) printf("%s\n%s\n",c->Text,txt);
				hadWarning = true;
			}

			Size -= 8 + ustrlen(c->Text) + 1;

			rage::StringFree(c->Text);
			if(c->PlatformString)
				rage::StringFree(c->PlatformString);
			if(c->TextComment)
				rage::StringFree(c->TextComment);
			rage::StringFree(c->Path);
			rage::StringFree(c->FileName);

			c->Text = rage::StringDuplicate(txt);
			c->PlatformString = 0;
			c->TextComment = 0;
			c->Path = rage::StringDuplicate(path);
			c->FileName = rage::StringDuplicate( fileName );
			c->Line = line;
			if(platformString && platformString[0] != '\0')
				c->PlatformString = rage::StringDuplicate(platformString);
			if(commentString && commentString[0] != '\0')
				c->TextComment = rage::StringDuplicate(commentString);

			Size += 8 + ustrlen(c->Text) + 1;

			TotalStringsPatched++;
		}

	}

	void writeOutputToRPF() 
	{
		char outname[128];
		rage::formatf(outname,"%s.gxt2",Name);

		rage::u8 *data = rage_new rage::u8[Size], *dp = data;

		unsigned magic = 'GXT2';
		writeInt(dp,magic);
		writeInt(dp,CodeCount);

		PackedString *ps = rage_new PackedString[CodeCount], *i = ps;
		for (String *s=Code; s; s=s->Next, i++) {
			i->Hash = s->Hash;
			i->Text = s->Text;
		}
		// Sort the strings by their hash code; there's a dummy entry at the end
		// so we can always deduce the length by subtracting next item's offset.
		std::sort(ps+0,ps+CodeCount);

		// Write the directory
		unsigned offset = (CodeCount*2+4)*4;
		for (unsigned k=0; k<CodeCount; k++) {
			writeInt(dp,ps[k].Hash);
			writeInt(dp,offset);
			offset += (unsigned)strlen(ps[k].Text) + 1;
		}
		// Write a fake final entry
		writeInt(dp,magic);
		writeInt(dp,offset);

		// Write all the strings
		for (unsigned k=0; k<CodeCount; k++) {
			size_t len = strlen(ps[k].Text) + 1;
			memcpy(dp, ps[k].Text, len);
			dp += len;
		}

		Assertf(offset == Size,"Section %s - Wrote %d bytes, but size was expected to be %d bytes",Name,offset,Size);

		if(!quiet)
			LogHelper.SetProgressMessage("INFO: Created section %-16s - %5d strings, %7u bytes in string heap",Name,CodeCount,offset);

		delete[] ps;

		// Add the data to the archive
		builder.AddData( data, offset, outname, true );
	}

	static void writeAllOutputsToRPF() {
		for (Section *s=First; s; s=s->Next)
			s->writeOutputToRPF();
	}

	void writeOutputToMetaFile( char const * const outputPath ) 
	{
		if( Code == NULL )
		{
			LogHelper.SetProgressMessage("WARNING: Section \"%s\" has no strings, no file will be generated at %s\\%s.idlb.\n", Name , outputPath, Name);
			return;
		}

		static char const * const c_xmlHeader = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n\n";

		static char const * const c_databaseHeaderFormat	=	"<!--\n    FILE: %s, Generated by gxtConvertor.\n-->\n<rage__fwTextDatabaseSource>\n<TextTable>\n";

		static char const * const c_textEntryHeaderFormat				=	"<Item>\n<Key>%s</Key>%s<DisplayText>";
		static char const * const c_textEntryHeaderFormatWithPlatform	=	"<Item platform=\"%s\">\n<Key>%s</Key>%s<DisplayText>";
		static char const * const c_textEntryCommentFormat				=	"\n<!--    %s    -->\n";
		static char const * const c_textEntryFooter						=	"</DisplayText>\n</Item>\n";

		static char const * const c_databaseFooter						=	"</TextTable>\n</rage__fwTextDatabaseSource>\n";

		char outputName[512];
		rage::formatf( outputName, "%s\\%s.ildb", outputPath, Name);

		rage::fiStream *outputFile = rage::fiStream::Create(outputName);
		if( !outputFile )
		{
			LogHelper.SetProgressMessage("ERROR: Unable to create output file %s! Text database for %s will not be converted.", outputName, Name );
			return;
		}

		int lineSize = istrlen( c_xmlHeader ); 
		outputFile->WriteByte( c_xmlHeader, lineSize );

		char outputBuffer[1024];
		char commentBuffer[1024];

		//! Now the start of the text database information
		lineSize = rage::formatf_n( outputBuffer, c_databaseHeaderFormat, Name );
		outputFile->WriteByte( outputBuffer, lineSize );

		//! For each string..
		for ( String *s = Code; s; s = s->Next ) 
		{
			//! Skip out the old dummy padding elements, they are not needed in our new data
			if( strcmp( s->Code, "DUMMY1" ) == 0 )
				continue;

			if( s->TextComment )
			{
				rage::formatf( commentBuffer, c_textEntryCommentFormat, s->TextComment );
			}
			else
			{
				commentBuffer[0] = '\n';
				commentBuffer[1] = '\0';
			}

			if( s->PlatformString )
			{
				//! Format the key into the header element
				lineSize = rage::formatf_n( outputBuffer, c_textEntryHeaderFormatWithPlatform, s->PlatformString, s->Code, commentBuffer );
				outputFile->WriteByte( outputBuffer, lineSize );
			}
			else
			{
				//! Format the key into the header element
				lineSize = rage::formatf_n( outputBuffer, c_textEntryHeaderFormat, s->Code, commentBuffer );
				outputFile->WriteByte( outputBuffer, lineSize );
			}

			//Wrap the literal in CData
			lineSize = sizeof(c_cDataStart) - 1;
			outputFile->WriteByte( c_cDataStart, lineSize );

			// Output the raw UTF-8 string
			lineSize = istrlen( s->Text );
			outputFile->WriteByte( s->Text, lineSize );

			//Finish wrapping
			lineSize = (int)sizeof(c_cDataEnd) - 1;
			outputFile->WriteByte( c_cDataEnd, lineSize );

			// and now finish off the entry
			lineSize = istrlen( c_textEntryFooter ); 
			outputFile->WriteByte( c_textEntryFooter, lineSize );
		}

		lineSize = istrlen( c_databaseFooter ); 

		//! ...and finish it all up as nice well-formed XML!
		outputFile->WriteByte( c_databaseFooter, lineSize );

		// Close the file now it's all ready!
		outputFile->Close();
		outputFile = NULL;
	}

	void writeOutputToPlainText( char const * const outputPath ) 
	{
		if( Code == NULL )
		{
			LogHelper.SetProgressMessage("WARNING: Section \"%s\" has no strings, no file will be generated at %s\\%s.txt.\n", Name , outputPath, Name);
			return;
		}


		static char const * const c_topHeader	=	"{ generated file - do not edit }\n";

		static char const * const c_textEntryHeaderFormat				=	"\n[%s]\n";
		static char const * const c_textEntryCommentFormat				=	"\n{    %s    }\n";

		char outputName[512];
		rage::formatf( outputName, "%s\\%s.txt", outputPath, Code[0].FileName );

		rage::fiStream *outputFile = rage::fiStream::Create(outputName);
		if( !outputFile )
		{
			LogHelper.SetProgressMessage("ERROR: Unable to create output file %s! Text database for %s will not be converted.", outputName, Name );
			return;
		}

		int lineSize = istrlen( c_topHeader ); 
		outputFile->WriteByte( c_topHeader, lineSize );

		char outputBuffer[1024];

		//! For each string..
		for ( String *s = Code; s; s = s->Next ) 
		{
			if( s->TextComment )
			{
				//! Output the key
				lineSize = rage::formatf_n( outputBuffer, c_textEntryCommentFormat, s->TextComment );
				outputFile->WriteByte( outputBuffer, lineSize );
			}

			//! Output the key
			lineSize = rage::formatf_n( outputBuffer, c_textEntryHeaderFormat, s->Code );
			outputFile->WriteByte( outputBuffer, lineSize );

			// Output the raw UTF-8 string
			lineSize = istrlen( s->Text );
			outputFile->WriteByte( s->Text, lineSize );
			outputFile->WriteByte( "\n", 1 );

		}

		// Close the file now it's all ready!
		outputFile->Close();
		outputFile = NULL;
	}

	static void writeAllOutputsToMetaFiles( char const * const outputPath ) 
	{
		for (Section *s=First; s; s=s->Next)
			s->writeOutputToMetaFile( outputPath );
	}

	static void writeAllOutputsToPlainText( char const * const outputPath ) 
	{
		for (Section *s=First; s; s=s->Next)
			s->writeOutputToPlainText( outputPath );
	}

	static void cleanup()
	{
		 Section* target = First;
		 while( target )
		 {
			 Section* next = target->Next;
			 delete target;
			 target = next;
		 }

		 First = NULL;
	}

	static Section *First;
	Section *Next;

	char Name[64];
	String *Code;
	unsigned CodeCount, Size;
};

Section* Section::First;


static size_t copyAndWrapInCData( rage::u8* dest, size_t maxDest, rage::u8 const * src, size_t srcSize )
{
	rage::u8 *origDest = dest;

	while( dest && maxDest && src && *src && srcSize )
	{
		//! Copy in the CData start
		size_t tempSrcSize = sizeof(c_cDataStart) - 1;
		char const * tempSrc = c_cDataStart;

		while( maxDest && tempSrcSize )
		{
			*dest++ = *tempSrc++;

			--maxDest;
			--tempSrcSize;
		}

		while( maxDest && *src && srcSize )
		{
			*dest++ = *src++;
			--maxDest;
			--srcSize;
		}

		//! Copy in the CData end
		tempSrcSize = sizeof(c_cDataEnd) - 1;
		tempSrc = c_cDataEnd;

		while( maxDest && tempSrcSize )
		{
			*dest++ = *tempSrc++;

			--maxDest;
			--tempSrcSize;
		}
	}

	return dest - origDest;
}

static size_t copyAndRemoveInvalidCharacters(rage::u8 *dest,size_t maxDest,const rage::u8 *src, const char *pLastKeyName) {
	rage::u8 *origDest = dest;
	rage::u32 number_of_tildes = 0;
	rage::u32 number_of_bytes_for_this_character = 1;

	while (*src) {
		number_of_bytes_for_this_character = 1;

		if (src[0]==0xEF && src[1] == 0xBF && src[2] == 0xBD)
		{	//	Apparently this is the UTF-8 encoding for U+FFFC which is the "object replacement character"
			//	so I'll just replace it with a space and report an error
			if (maxDest)
				*dest++ = ' ', maxDest--;
			src += 3;

			LogHelper.SetProgressMessage("ERROR: Line of text with key '%s' contains an invalid character\n", pLastKeyName);
			hadError = true;
			continue;
		}
		/*else if( src[0] == '&' )
		{	// Wrap any literal ampersands in CDATA segments

		bool isAmpersandLiteral = true;

		u8 const * ampersandLiteralEnd = src + 1;

		while( ampersandLiteralEnd )
		{
		if( isspace( *ampersandLiteralEnd ) != 0 )
		{
		break;
		}
		else if( *ampersandLiteralEnd == ';' )
		{
		isAmpersandLiteral = false;
		break;
		}

		++ampersandLiteralEnd;
		}

		if( isAmpersandLiteral )
		{
		size_t tagLength = ampersandLiteralEnd - src;

		size_t writtenSize = copyAndWrapInCData( dest, maxDest, src, tagLength );

		dest += writtenSize;
		maxDest -= writtenSize;

		src = ampersandLiteralEnd;
		}
		else
		{
		while( maxDest && *src && src <= ampersandLiteralEnd )
		{
		*dest++ = *src++;
		--maxDest;
		}
		}

		}
		else if( src[0] == '<' )
		{	// Wrap any implicit HTML/Markup in CData

		u8 const* endTag = (u8 const*)strchr( (char const*)src, '>' );
		if( endTag )
		{
		size_t tagLength = endTag - src + 1;

		size_t writtenSize = copyAndWrapInCData( dest, maxDest, src, tagLength );

		dest += writtenSize;
		maxDest -= writtenSize;

		src = endTag + 1;
		}
		else
		{
		// Assume it's a less than!

		*dest++ = *src++;
		--maxDest;
		}
		}*/
		else if (src[0]==0xE2 && src[1] == 0x80) {
			// Handle En Dash (U+2013) and Em Dash (U+2014)
			// Handle left and right single quotation marks (U+2018 and U+2019)
			if (src[2] == 0x98 || src[2] == 0x99) {
				// printf("Replaced unicode left/right single quote with normal quote.\n");
				if (maxDest)
					*dest++ = '\'', maxDest--;
				src += 3;
				continue;
			}
			// Handle left and right double quotation marks (U+201C and U+201D)
			else if (src[2] == 0x9C || src[2] == 0x9D) {
				// printf("Replaced unicode left/right double quote with normal quote.\n");
				if (maxDest)
					*dest++ = '\"', maxDest--;
				src += 3;
				continue;
			}
			// Handle ellipsis (U+2026)
			else if (src[2] == 0xA6) {
				// printf("Replaced unicode ellipsis with three dots.\n");
				if (maxDest)
					*dest++ = '.', maxDest--;
				if (maxDest)
					*dest++ = '.', maxDest--;
				if (maxDest)
					*dest++ = '.', maxDest--;
				src += 3;
				continue;
			}
		}

		if ((src[0] & 0x80) == 0x80)
		{	//	Top bit is set
			//	Check for multi-byte UTF-8 character
			if ((src[0] & 0xE0) == 0xC0)
			{
				if (src[1] && ((src[1] & 0xC0) == 0x80))
				{
					number_of_bytes_for_this_character = 2;
				}
			}
			else if ((src[0] & 0xF0) == 0xE0)
			{
				if (src[1] && ((src[1] & 0xC0) == 0x80)
					&& src[2] && ((src[2] & 0xC0) == 0x80))
				{
					number_of_bytes_for_this_character = 3;
				}
			}

			if (number_of_bytes_for_this_character == 1)
			{
				LogHelper.SetProgressMessage("ERROR: Line of text with key '%s' contains an invalid UTF-8 character with code %u. Check that the txt file is actually saved with UTF-8 encoding. Open the file in Notepad and resave it using UTF-8 encoding\n", pLastKeyName, src[0]);
				hadError = true;
			}
		}

		if (number_of_bytes_for_this_character == 2)
		{
			// Two-byte unicode
			if (maxDest)
				*dest++ = *src++, maxDest--;
			if (maxDest)
				*dest++ = *src++, maxDest--;
			// printf("Line of text with key '%s' contains two-byte unicode character\n", pLastKeyName);
			continue;
		}
		else if (number_of_bytes_for_this_character == 3)
		{
			// Three-byte unicode
			if (maxDest)
				*dest++ = *src++, maxDest--;
			if (maxDest)
				*dest++ = *src++, maxDest--;
			if (maxDest)
				*dest++ = *src++, maxDest--;
			// printf("Line of text with key '%s' contains three-byte unicode character\n", pLastKeyName);
			continue;
		}
		else if (src[0] == 0x7E)
		{	//	The tilde character
			number_of_tildes++;
		}
		else if (src[0] == '{')
		{
			LogHelper.SetProgressMessage("ERROR: A { character was found that was not the first character on the line. Last Key Read was '%s'\n", pLastKeyName);
			hadError = true;
		}

		if (maxDest)
			*dest++ = *src, maxDest--;
		src++;
	}

	if ( (number_of_tildes % 2) != 0)
	{
		LogHelper.SetProgressMessage("ERROR: Odd number of tildes. Last Key Read was '%s'\n", pLastKeyName);
		hadError = true;
	}

	*dest = 0;
	return dest - origDest;
}

void processFile(const char *path) 
{
	rage::fiStream *S = rage::fiStream::Open(path);
	if (!S) 
	{
		LogHelper.SetProgressMessage("ERROR: problem opening '%s'\n",path);
		hadError = true;
		return;
	}

	char filenameBuf[4096];
	filenameBuf[0];
	{
		char const* folderSeperatorLocation = strrchr( path, '\\' );
		char const* extensionLocation = strrchr( path, '.' );
		char const* stringStart = folderSeperatorLocation ? folderSeperatorLocation + 1 : path;
		char const* extenstionStart = extensionLocation ? extensionLocation + 1 : NULL;

		if( stringStart && extenstionStart && extenstionStart > stringStart )
		{
			size_t const fileNameLength = extenstionStart - stringStart;
			rage::safecpy( filenameBuf, stringStart, fileNameLength );
		}
	}

	char linebuf[4096];
	char currentText[8192] = "";
	size_t currentTextLen = 0;
	int line = 0;
	enum { NOSTATE, IN_START, IN_CODE, IN_WRONG_PLATFORM } state = NOSTATE;
	Section *currentSection = 0;
	char currentCode[64] = "";
	char currentPlatformString[64] = "";
	char currentComment[1024] = "";
	bool hadWarningThisFile = false;

	LogHelper.SetProgressMessage("Processing %s ...",path);

	while (fgets(linebuf,sizeof(linebuf),S)) {
		// skip UTF8 marker if present
		if (line == 0)
		{
			if (((unsigned char*)linebuf)[0]==0xEF && ((unsigned char*)linebuf)[1]==0xBB && ((unsigned char*)linebuf)[2]==0xBF)
			{
				strcpy(linebuf,linebuf+3);
			}
			else
			{
				//				printf("%s : error X1004: File is not using UTF-8 Encoding. Open the file in Notepad and resave it using UTF-8 Encoding\n",name);
				//				hadError = true;
			}
		}

		++line;
		size_t sl = strlen(linebuf);

		// Check for overly long strings
		if (sl == sizeof(linebuf)-1) {
			LogHelper.SetProgressMessage("ERROR: %s(%d) : error X1000: Really long line, will be truncated!\n",path,line);
			hadError = true;
		}

		// Strip trailing whitespace and newlines, but respect unicode
		size_t nonspace = 0, thisSize = 1;
		for (size_t i=0; i<sl; i+=thisSize) {
			thisSize = 1;
			if ((((rage::u8*)linebuf)[i] & 0xE0) == 0xC0)
				thisSize = 2;
			else if ((((rage::u8*)linebuf)[i] & 0xF0) == 0xE0)
				thisSize = 3;
			if (((rage::u8*)linebuf)[i] > 32)
				nonspace = i+thisSize;
		}
		linebuf[nonspace] = '\0';

		if (linebuf[0]=='{' || linebuf[0] == 0)	// comment or empty line
			continue;
		else if( linebuf[0]=='{' )
		{
			/*char *startOfComment = linebuf + 1;
			char *endOfComment = strchr(linebuf,'}');

			size_t copySize = ( endOfComment - startOfComment ) + 1;
			size_t bufferSize = sizeof( currentComment );

			copySize = copySize > bufferSize ? bufferSize : copySize;
			safecpy( currentComment, startOfComment, copySize );*/
		}
		else if (!strcmp(linebuf,"start"))
			state = IN_START;
		else if (!strcmp(linebuf,"end") && state == IN_START)
			state = NOSTATE;
		else if (linebuf[0]=='[') 
		{
			// Finish previous section if it was active
			if (currentSection && currentCode[0] && currentText[0])
			{
				currentSection->addString( currentCode, currentText, currentPlatformString, currentComment, 
					path, filenameBuf, line-1);
				currentPlatformString[0] = '\0';
				currentCode[0] = '\0';
			}

			currentComment[0] = '\0';

			char *closeBracket = strchr(linebuf,']');
			if (!closeBracket) 
			{
				LogHelper.SetProgressMessage("ERROR: %s(%d) : error X1001: Missing ]\n",path,line);
				hadError = true;
			}
			else 
			{
				// ignore anything else on the line
				closeBracket[0] = 0;
				char *missionFilter = strchr(linebuf,':');
				char *platformFilter = strchr(linebuf,'!');
				bool bHitElse = false;

				// nullify colon first before we process anything else
				if (missionFilter) {
					*missionFilter = 0;
					++missionFilter;
				}
				// nullify bang then check it
				if (platformFilter) 
				{
					*platformFilter = 0;
					++platformFilter;

					bHitElse = (stricmp(platformFilter, PLATFORM_ELSE) == 0);

					if( outputtingToMetadata )
					{
						//! Else isn't a valid platform, so don't copy it in!
						if( !bHitElse )
						{
							size_t platformStringLength = (closeBracket - platformFilter) + 1;
							rage::safecpy( currentPlatformString, platformFilter, platformStringLength );
						}
					}
					else if (!bHitElse)
					{
						// if platform doesn't match, skip it!
						bool match = false;

						std::string str(platformFilter);
						std::transform(str.begin(), str.end(), str.begin(), ::toupper);
						if (str.find(",") != std::string::npos)
						{
							// Support the reuse of strings between platforms i.e. [HASH_NAME!PS4,PS5]
							std::string s;
							std::istringstream nasa;
							nasa.str(str);

							while (std::getline(nasa, s, ','))
							{
								if ((std::find(g_PlatformFilters.begin(), g_PlatformFilters.end(), str)) != g_PlatformFilters.end())
								{
									match = true;
									break;
								}
							}
						}
						else
						{
							// Support the inclusion of strings from other platforms
							if ((std::find(g_PlatformFilters.begin(), g_PlatformFilters.end(), str)) != g_PlatformFilters.end())
							{
								match = true;
							}
						}

						if (!match)
						{
							// sanity check
							static const char* validPlatforms[] = {VALID_PLATFORM_STRINGS};
							bool bFound = false;
							// weird math is what COUNTOF does w/o including nelem
							for( int platformFilterIndex = 0; !bFound && platformFilterIndex < (sizeof(validPlatforms)/sizeof(validPlatforms[0])); ++platformFilterIndex)
							{
								bFound = stricmp(platformFilter, validPlatforms[platformFilterIndex]) == 0;
							}

							if( !bFound )
							{
								LogHelper.SetProgressMessage("WARNING: %s(%d) : warning X1003: Unexpected platform filter '%s' found. Expected " VALID_PLATFORM_ERROR "\n", path,line, platformFilter, VALID_PLATFORM_STRINGS);
								hadWarning = true;
							}

							state = IN_WRONG_PLATFORM;
							continue;
						}
					}
				}
				if( missionFilter )
				{
					currentSection = Section::lookup(missionFilter);
					if (!currentSection) {
						if (!hadWarningThisFile)
							LogHelper.SetProgressMessage("ERROR: %s(%d) : error X1002: Unknown section '%s', please add to start/end block\n",path,line,missionFilter);
						hadError = hadWarning = hadWarningThisFile = true;
						currentSection = Section::create(missionFilter);
					}
				}
				else 
					currentSection = Section::lookup("GLOBAL");

				// If PLATFORM_ELSE'S fail the duplicate check, count them as 'out of platform'
				// unless the key doesn't exist, in which case we can assume that the other platforms were filtered out
				if( bHitElse ) 
				{
					if( String::lookupCode(linebuf+1, currentPlatformString, true, !outputtingToMetadata ) ) 
					{
						currentCode[0] = 0;
						state = IN_WRONG_PLATFORM;
						continue;
					}
				}

				// Remember the current code word.
				rage::safecpy(currentCode,linebuf+1);
				String *s=String::lookupCode(currentCode, currentPlatformString, true, !outputtingToMetadata);
				if (s!=NULL) 
				{
					if (processingpatchtext)
					{
						if(verbose)
						{
							if( outputtingToMetadata && currentPlatformString )
							{
								LogHelper.SetProgressMessage("INFO: (line %5d) replacement for %s code %-32s - original: %s(%d)",line,currentPlatformString,currentCode,s->Path,s->Line);
							}
							else
							{
								LogHelper.SetProgressMessage("INFO: (line %5d) replacement for code %-32s - original: %s(%d)",line,currentCode,s->Path,s->Line);
							}
						}
					}
					else
					{
						if( outputtingToMetadata && currentPlatformString )
						{
							LogHelper.SetProgressMessage("ERROR: %s(%d) : error X1003: Duplicate code [%s] for Platform '%s' - original: %s(%d)",path,line,currentCode,currentPlatformString,s->Path,s->Line);
						}
						else
						{
							LogHelper.SetProgressMessage("ERROR: %s(%d) : error X1003: Duplicate code [%s] - original: %s(%d)",path,line,currentCode,s->Path,s->Line);
						}
						hadError = true;
					}
				}
				state = IN_CODE;
				currentText[0] = 0;
				currentTextLen = 0;
			}
		}
		else
		{
			switch(state)
			{
			case IN_START:
				if (!Section::lookup(linebuf)) {
					// printf("Created section '%s'\n",linebuf);
					Section::create(linebuf);
				}
				break;
			case IN_CODE:
				currentTextLen += copyAndRemoveInvalidCharacters((rage::u8*)currentText+currentTextLen,sizeof(currentText)-1-currentTextLen,(rage::u8*)linebuf, currentCode);
				break;

			case IN_WRONG_PLATFORM:
				// we're just ignoring everything until we hit the next '['
				break;

			default:
				LogHelper.SetProgressMessage("WARNING: %s(%d) : Inconsistent state %d here.\n",path,line,state);
				hadWarning = true;
				break;
			}
		}
	}

	// Make sure the last text in the file is handled unless it's empty (a dummy label)
	if (currentSection && currentCode[0] && currentText[0])
		currentSection->addString(currentCode,currentText,currentPlatformString, currentComment, 
			path, filenameBuf, line);

	S->Close();
}


//	Reads a list of keys from pInputFilename
//	Writes those keys with their section name and text data to pOutputFilename
void processKeyListFile(const char *pInputFilename, const char *pOutputFilename)
{
	struct sTextData
	{
		const char *pSectionName;
		const char *pTextData;
	};

	rage::atMap<const char*, sTextData> TextDataMap;

	//	I have to do this because the text files contain more than 65167 text labels
	//	Without these 3 lines, I get the "invalid size %u in atHashNextSize" assert during a call to TextDataMap.Insert
	TextDataMap.SetAllowRecompute(true);
	TextDataMap.Recompute(rage::atHashNextSize(64000) - 1);	//	65166
	TextDataMap.SetAllowRecompute(false);

	sTextData EntryToAddToMap;

	//	Create map (key is text key, data is struct containing section name and text data)
	for (Section *s=Section::First; s; s=s->Next)
	{
		EntryToAddToMap.pSectionName = s->Name;
		for (String *c=s->Code; c; c=c->Next)
		{
			EntryToAddToMap.pTextData = c->Text;
			TextDataMap.Insert(c->Code, EntryToAddToMap);
		}
	}


	rage::fiStream *inputFile = rage::fiStream::Open(pInputFilename);
	if (!inputFile) {
		LogHelper.SetProgressMessage("ERROR: error opening input file '%s'\n",pInputFilename);
		hadError = true;
		return;
	}

	rage::fiStream *outputFile = rage::fiStream::Create(pOutputFilename);
	if (!outputFile) {
		LogHelper.SetProgressMessage("ERROR: error opening output file '%s'\n",pOutputFilename);
		hadError = true;
		return;
	}

	int line = 0;
	char linebuf[2048];
	while (fgets(linebuf,sizeof(linebuf),inputFile)) {
		// skip UTF8 marker if present
		if (line == 0 && ((unsigned char*)linebuf)[0]==0xEF && ((unsigned char*)linebuf)[1]==0xBB && ((unsigned char*)linebuf)[2]==0xBF)
			strcpy(linebuf,linebuf+3);

		line++;

		size_t sl = strlen(linebuf);

		// Check for overly long strings
		if (sl == sizeof(linebuf)-1) {
			LogHelper.SetProgressMessage("ERROR: %s(%d) : error X1000: Really long line, will be truncated!\n",pInputFilename,line);
			hadError = true;
		}

		// Strip trailing whitespace and newlines
		while (sl && linebuf[sl-1] <= 32)
			linebuf[--sl] = 0;

		if (linebuf[0] == 0)	// empty line
			continue;

		sTextData *pFoundTextData = TextDataMap.Access(linebuf);
		if (pFoundTextData)
		{
			if (strcmp(pFoundTextData->pSectionName, "GLOBAL") == 0)
			{
				fprintf(outputFile, "[%s]\t\t%s\r\n", linebuf, pFoundTextData->pTextData);
			}
			else
			{
				fprintf(outputFile, "[%s:%s]\t\t%s\r\n", linebuf, pFoundTextData->pSectionName, pFoundTextData->pTextData);
			}
		}
		else
		{	//	This should never happen really. It's expected that all text keys in the input file will exist in one of the american*.txt files
			fprintf(outputFile, "[%s]\r\n", linebuf);
		}
	}

	inputFile->Close();
	outputFile->Close();
}

struct StringComparisonFunctor {
	bool operator()( const char *a, const char *b ) const {
		return strcmp(a,b) < 0;
	}
};

void addtopatchfilelist(const char *patchfilename)
{
	strncpy(g_patchfilelist[patchfilecnt++],patchfilename,RAGE_MAX_PATH);
}

namespace rage { XPARAM(aeskey); }

// not used, here for the help strings they provide
PARAM(platform,"Set output platform");
PARAM(platformkey,"Set platform filter key (in case you want it different from platform)");
PARAM(alp,"Set alp filename");
PARAM(metadata,"Generate meta-data output for new text streaming system");
PARAM(plaintext,"Generate plaintext output for errorcode system");
PARAM(keylist,"Take a file containing a list of text keys. Output those keys along with their text data");
PARAM(nologwindow,"Suppress uLog window");

struct InitGameHeap_t;
char logFilePath[RAGE_MAX_PATH]; 

int GXTMain(int argc,char **argv) 
{

	// Default to old behaviour, so assume no meta-data output and no text output
	outputtingToMetadata = false;
	outputtingTxtFile = false;
	patchText = false;

	if (argc == 1) {
		puts("Usage: %s [-alp alpfile] [-keylist keylistfile outputfile] outputRpfName *.txt");
		puts("       american american*.txt");
		puts("-keylist takes a text file containing a list of text keys and outputs a file listing those keys with their text");
		puts("\nOR to generate new metadata output: -metadata [outputPath *.txt] e.g. %RS_PROJROOT%\\assets\\export\\data\\text\\american american*.txt");
		puts("\nOR to generate plain text output: -plaintext [outputPath *.txt] e.g. %RS_PROJROOT%\\build\\dev_ng\\x64\\data\\errorcodes american*.txt");
		return 0;
	}

	rage::sysTimer T;
	Section::create("GLOBAL");

	const char *platform = NULL;
	const char* platformkeys = NULL;

	const char *alpname = NULL;
	const char *inputlistname = NULL;
	const char *outputlistname = NULL;
	char patchdir[RAGE_MAX_PATH];

	rage::formatf(logFilePath, "X:\\log.ulog" );
	rage::formatf(patchdir,".");

	if (!strcmp(argv[1],"-metadata")) 
	{
		outputtingToMetadata = true;
		argc-=1, argv+=1;
	}
	else
	{
		while (argv[1][0] == '-') 
		{
			if (!stricmp(argv[1],"-nologwindow"))
			{
				noLogWindow = true;
				argc-=1, argv+=1;
			}
			if (!strcmp(argv[1],"-alp"))
			{
				alpname = argv[2];
				argc-=2, argv+=2;
			}
			else if (!strcmp(argv[1],"-platform"))
			{
				platform = argv[2];
				rage::g_sysPlatform = rage::sysGetPlatform(argv[2]);
				byteSwap = rage::sysGetByteSwap(rage::g_sysPlatform);
				argc-=2, argv+=2;
			}
			else if (!strcmp(argv[1],"-keylist"))
			{
				inputlistname = argv[2];
				outputlistname = argv[3];
				argc-=3, argv+=3;
			}
			else if (!strcmp(argv[1],"-platformkey"))
			{
				platformkeys = argv[2];

				std::string str(argv[2]);
				std::transform(str.begin(), str.end(), str.begin(), ::toupper);
				g_PlatformFilters.push_back(str);
				argc-=2, argv+=2;
			}
			else if (!strcmp(argv[1], "-platformkeys"))
			{
				platformkeys = argv[2];

				std::string str;
				std::string data(argv[2]);
				std::istringstream nasa;
				nasa.str(str);
				while (std::getline(nasa, str, ','))
				{
					std::transform(str.begin(), str.end(), str.begin(), ::toupper);
					g_PlatformFilters.push_back(str);
				}
				argc -= 2, argv += 2;
			}
			else if (!strcmp(argv[1],"-plaintext"))
			{
				outputtingTxtFile = true;
				argc-=1, argv+=1;
			}
			else if (!strcmp(argv[1],"-verbose"))
			{
				verbose = true;
				quiet = false;
				argc-=1, argv+=1;
			}
			else if (!strcmp(argv[1],"-quiet"))
			{
				verbose = false;
				quiet = true;
				argc-=1, argv+=1;
			}
			else if (!strcmp(argv[1],"-nopatchdupewarn"))	// suppress "patch text is identical to original" warning
			{
				nopatchdupewarn = true;
				argc-=1, argv+=1;
			}
			else if (!strcmp(argv[1],"-log"))
			{
				rage::formatf(logFilePath,"%s",argv[2]);
				argc-=2, argv+=2;
			}
			else if (!strcmp(argv[1],"-patchdir"))	// sorry, this has to come before -patch for now
			{
				rage::formatf(patchdir,"%s",argv[2]);
				argc-=2, argv+=2;
			}
			else if (!strcmp(argv[1],"-patch"))	// must set -patchdir first on the commandline; this will be fixed soon
			{
				patchText = true;
				while(argc && argv[2][0]!='-')
				{
					char pfile[RAGE_MAX_PATH];
					rage::formatf(pfile,"%s\\%s",patchdir,argv[2]);
					addtopatchfilelist(pfile);
					argc-=1, argv+=1;
				}
				argc-=1, argv+=1;
			}
		}
	}

	remove(logFilePath);
	LogHelper.PreExport(logFilePath, "GXTMaker2", false, true);
	Log = LogHelper.GetULogFile();

	LogHelper.SetProgressMessage("Platform %s with keys %s", (platform!=NULL) ? platform : "unknown", (platformkeys!=NULL) ? platformkeys : "none");

	for (int i=2; i<argc; i++)
		processFile(argv[i]);

	if(patchText)
	{
		processingpatchtext = true;
		for(int i=0;i<patchfilecnt;i++)
			processFile(g_patchfilelist[i]);
		processingpatchtext = false;
	}

	if (!hadError)
	{
		if( outputtingToMetadata )
		{
			Section::writeAllOutputsToMetaFiles( argv[1] );

			LogHelper.SetProgressMessage("%d total strings processed in %f seconds.",String::TotalStrings,T.GetTime());
			if(TotalStringsPatched)
				LogHelper.SetProgressMessage("(%d strings patched)",TotalStringsPatched);

			T.Reset();
		}
		else if( outputtingTxtFile )
		{
			Section::writeAllOutputsToPlainText( argv[1] );

			LogHelper.SetProgressMessage("%d total strings processed in %f seconds.",String::TotalStrings,T.GetTime());
			if(TotalStringsPatched)
				LogHelper.SetProgressMessage("(%d strings patched)",TotalStringsPatched);

			T.Reset();
		}
		else
		{
			if (inputlistname && outputlistname)
			{
				processKeyListFile(inputlistname, outputlistname);
			}
			else
			{
				// produce .alp file
				if (alpname) 
				{
					T.Reset();
					const char **strings = rage_new const char*[String::TotalStrings];
					unsigned ct = 0;
					for (Section *s=Section::First; s; s=s->Next) {
						for (String *c=s->Code; c; c=c->Next)
							strings[ct++] = c->Code;
					}
					std::sort( strings+0, strings + String::TotalStrings, StringComparisonFunctor() );
					rage::fiStream *S = rage::fiStream::Create(alpname);
					if (S) {
						for (unsigned i=0; i<ct; i++)
						{
							rage::u32 Hash = rage::atStringHash(strings[i]);
							fprintf(S,"%s Unsigned Hash = %u Signed Hash = %d\r\n",strings[i], Hash, Hash);
						}
						S->Close();
					}
					delete[] strings;
					LogHelper.SetProgressMessage("codes sorted and written to '%s' in %f seconds.\n",alpname,T.GetTime());
				}
				else
				{
					Section::writeAllOutputsToRPF();

					LogHelper.SetProgressMessage("%d total strings processed in %f seconds.",String::TotalStrings,T.GetTime());
					if(TotalStringsPatched)
						LogHelper.SetProgressMessage("(%d strings patched)",TotalStringsPatched);

					T.Reset();

					// Let AES key deduce project from output path.
					char const * const outputPath = argv[1];
					rage::PARAM_aeskey.Set( outputPath );
					rage::fiStream *rpf = rage::fiStream::Create(argv[1]);
					if (!rpf) {
						LogHelper.SetProgressMessage("ERROR: Unable to create output archive '%s'\n",argv[1]);
						return 1;
					}
					builder.Save(rpf);
					rpf->Close();
					LogHelper.SetProgressMessage("created output '%s' in %f seconds.\n",argv[1],T.GetTime());
				}
			}
		}
	}
	else
	{
		LogHelper.SetProgressMessage("ERROR: Errors found please correct then attempt again\n");
	}

	Section::cleanup();
	LogHelper.PostExport(!noLogWindow, !noLogWindow); 
	Log = NULL;
	return hadError ? -1 : 0;
}

int Main()
{
	return GXTMain( rage::sysParam::GetArgCount(), rage::sysParam::GetArgArray() );
}
