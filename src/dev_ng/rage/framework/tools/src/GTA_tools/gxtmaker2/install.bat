CALL setenv.bat

@echo on

REM GxtMaker2 hasn't been moved to tools in NG yet :(
REM pushd %RS_TOOLSROOT%\bin

pushd %RS_PROJROOT%\assets_ng\GameText\dev_ng\scripts\
p4 edit gxtmaker2.exe
copy %RS_BUILDBRANCH%\gxtmaker2_win64_toolrelease.exe gxtmaker2.exe


REM update another copy of the exe for the DLCTextBuilder
pushd %RS_PROJROOT%\tools_ng\bin\DLCTextBuilder\
p4 edit gxtmaker2.exe
copy %RS_BUILDBRANCH%\gxtmaker2_win64_toolrelease.exe gxtmaker2.exe

popd

pause
