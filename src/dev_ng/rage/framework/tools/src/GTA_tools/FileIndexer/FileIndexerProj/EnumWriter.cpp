#include "EnumWriter.h"
#include "FileFile.h"

bool EnumWriter::Write
(const std::string& sPedFileName, 
 const std::string& sGroupFileName, 
 const std::string& sPedFileNameForScript, 
 const std::string& sGroupFileNameForScript, 
 const std::vector<std::string>& enumNames, 
 const std::vector<int>& enumIndices, 
 const std::vector<int>& enumBelongsTo,
 const int FileType) const
{
    FileWriter PedFileWriter;
    PedFileWriter.Open(sPedFileName);
    if(!PedFileWriter.IsOpen())
    {
		printf("Can't write to output file %s \n", sPedFileName.c_str());
        return false;
    }

    FileWriter GroupFileWriter;
    GroupFileWriter.Open(sGroupFileName);
    if(!GroupFileWriter.IsOpen())
    {
		printf("Can't write to output file %s \n", sGroupFileName.c_str());
        return false;
    }

    FileWriter PedFileWriterForScript;
    PedFileWriterForScript.Open(sPedFileNameForScript);
    if(!PedFileWriterForScript.IsOpen())
    {
		printf("Can't write to output file %s \n", sPedFileNameForScript.c_str());
		return false;
    }
	if (FileType == 0)
	{
		PedFileWriterForScript.WriteStr("ENUM CHAR_EVENT_NAMES");
	}
	else
	{
		PedFileWriterForScript.WriteStr("ENUM CHAR_TASK_NAMES");
	}

	FileWriter GroupFileWriterForScript;
    GroupFileWriterForScript.Open(sGroupFileNameForScript);
    if(!GroupFileWriterForScript.IsOpen())
    {
		printf("Can't write to output file %s \n", sGroupFileNameForScript.c_str());
        return false;
    }
	if (FileType == 0)
	{
		GroupFileWriterForScript.WriteStr("ENUM GROUP_EVENT_NAMES");
	}
	else
	{
		GroupFileWriterForScript.WriteStr("ENUM GROUP_TASK_NAMES");
	}


    int i;
    const int N=enumNames.size();
	bool bWrittenALineToScriptPedFile = false;
	bool bWrittenALineToScriptGroupFile = false;
    for(i=0;i<N;i++)
    {
        const std::string& sEnumName=enumNames[i];
        const int index=enumIndices[i];
		const int iBelongsTo=enumBelongsTo[i];

        if(-1==sEnumName.find("%%"))
        {
            char number[256];
			char line_for_ped_script_file[256];
			char line_for_group_script_file[256];
            sprintf(number," %d \n",index);
			if (FileType == 0)
			{	//	Script compiler doesn't like two enum entries to have the same name so have to alter the names for Events
				sprintf(line_for_ped_script_file,"\n\tPED_%s = %d",sEnumName.c_str(), index);
				sprintf(line_for_group_script_file,"\n\tGROUP_%s = %d",sEnumName.c_str(), index);
			}
			else
			{	//	No overlap between Ped and Group Tasks so we don't have to add a word to the start of the string.
				//	If this changes in the future then just use the same code as for Events above.
				sprintf(line_for_ped_script_file,"\n\t%s = %d",sEnumName.c_str(), index);
				sprintf(line_for_group_script_file,"\n\t%s = %d",sEnumName.c_str(), index);
			}
			std::string sLine;
            sLine.append(sEnumName);
            sLine.append(number);


			bool bWriteToPedFiles = false;
			bool bWriteToGroupFiles = false;

			switch (iBelongsTo)
			{
				case 1 :
					bWriteToPedFiles = true;
					break;
				case 2 :
					bWriteToGroupFiles = true;
					break;
				case 3 :
					bWriteToPedFiles = true;
					bWriteToGroupFiles = true;
					break;
			}

			if (bWriteToPedFiles)
			{
				PedFileWriter.WriteString(sLine);
				if (bWrittenALineToScriptPedFile)
				{
					PedFileWriterForScript.WriteStr(",");
				}
				PedFileWriterForScript.WriteStr(line_for_ped_script_file);
				bWrittenALineToScriptPedFile = true;
			}

			if (bWriteToGroupFiles)
			{
				GroupFileWriter.WriteString(sLine);
				if (bWrittenALineToScriptGroupFile)
				{
					GroupFileWriterForScript.WriteStr(",");
				}
				GroupFileWriterForScript.WriteStr(line_for_group_script_file);
				bWrittenALineToScriptGroupFile = true;
			}
        }
    }

    PedFileWriter.Close();
	GroupFileWriter.Close();

	PedFileWriterForScript.WriteStr("\nENDENUM\n");
	PedFileWriterForScript.Close();

	GroupFileWriterForScript.WriteStr("\nENDENUM\n");
	GroupFileWriterForScript.Close();

    return true;
}
