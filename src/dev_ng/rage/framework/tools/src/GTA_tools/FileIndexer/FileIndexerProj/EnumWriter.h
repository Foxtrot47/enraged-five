#ifndef ENUM_WRITER_H
#define ENUM_WRITER_H

#include <vector>
#include <string>

class EnumWriter
{
public:

    EnumWriter(){}
    ~EnumWriter(){}

    bool Write(const std::string& sPedFileName, 
		const std::string& sGroupFileName, 
		const std::string& sPedFileNameForScript, 
		const std::string& sGroupFileNameForScript, 
		const std::vector<std::string>& enumNames, const std::vector<int>& enumIndices, const std::vector<int>& enumBelongsTo,
		const int FileType) const;
};

#endif
