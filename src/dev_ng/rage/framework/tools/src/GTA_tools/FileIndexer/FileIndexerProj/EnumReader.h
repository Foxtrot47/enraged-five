#ifndef ENUM_READER_H
#define ENUM_READER_H

#include <vector>
#include <string>

class EnumReader
{
public:

    EnumReader(){}
    ~EnumReader(){}

    bool Read(const std::string& sFileName);

    const std::vector<std::string>& GetEnumStrings() const {return m_enumStrings;}
    const std::vector<int>& GetEnumIndices() const {return m_enumIndices;}
	const std::vector<int>& GetEnumBelongsTo() const {return m_enumBelongsTo;}
    
private:

	std::vector<int> m_enumIndices;
    std::vector<std::string> m_enumStrings;
	std::vector<int> m_enumBelongsTo;

};
#endif
