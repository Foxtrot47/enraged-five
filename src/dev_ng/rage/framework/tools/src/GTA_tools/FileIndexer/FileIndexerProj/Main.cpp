#include <stdio.h>

#include "EnumReader.h"
#include "EnumWriter.h"


int main(int argc, char *argv[ ])
{
	std::string inputFileNames[2];
	inputFileNames[0].append(argv[2]);
	inputFileNames[1].append(argv[2]);
	inputFileNames[0].append("/Event/EventTypes.h");
	inputFileNames[1].append("/Task/TaskTypes.h");
	std::string outputPedFileNames[2];
	outputPedFileNames[0]=std::string("x:/gta/gta_bin/aiedit/PedEventChoices.txt");
	outputPedFileNames[1]=std::string("x:/gta/gta_bin/aiedit/PedTaskChoices.txt");
	std::string outputGroupFileNames[2];
	outputGroupFileNames[0]=std::string("x:/gta/gta_bin/aiedit/GroupEventChoices.txt");
	outputGroupFileNames[1]=std::string("x:/gta/gta_bin/aiedit/GroupTaskChoices.txt");

	// Not enough arguments
	if( argc == 1 )
	{
		printf("Not enough arguments:\n	Usage: FileInedexerRelease [DecisionMakerDir]\n");
		getchar();
		return -1;
	}	
	
	std::string outputPedFileNamesForScript[2];
	outputPedFileNamesForScript[0].append(argv[1]);
	outputPedFileNamesForScript[1].append(argv[1]);
	outputPedFileNamesForScript[0].append("/PedEventChoices.sch");
	outputPedFileNamesForScript[1].append("/PedTaskChoices.sch");
	std::string outputGroupFileNamesForScript[2];
	outputGroupFileNamesForScript[0].append(argv[1]);
	outputGroupFileNamesForScript[1].append(argv[1]);
	outputGroupFileNamesForScript[0].append("/GroupEventChoices.sch");
	outputGroupFileNamesForScript[1].append("/GroupTaskChoices.sch");
	
	int i;
	for(i=0;i<2;i++)
	{
	    EnumReader enumReader;
	    if(!enumReader.Read(inputFileNames[i]))
	    {
	        printf("Can't read from input file %s \n", inputFileNames[i].c_str());
	    }

	    EnumWriter enumWriter;
	    if(!enumWriter.Write(outputPedFileNames[i],
							outputGroupFileNames[i],
							outputPedFileNamesForScript[i],
							outputGroupFileNamesForScript[i],
							enumReader.GetEnumStrings(),enumReader.GetEnumIndices(),enumReader.GetEnumBelongsTo(),
							i))
	    {
	        printf("Can't write to output file\n");	//	, outputFileNames[i].c_str());
	    }

	}
	return 0;

}