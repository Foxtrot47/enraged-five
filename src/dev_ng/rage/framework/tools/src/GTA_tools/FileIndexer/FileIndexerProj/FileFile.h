#ifndef FILE_FILE_H
#define FILE_FILE_H

#include <string>
#include <stdio.h>	
//#include "fstream.h"

class FileBase
{
public:

	FileBase(const bool bUseBinary=false)
		: m_pFile(0),
		  m_bUseBinary(bUseBinary)
	{
	}

	virtual ~FileBase()
	{
		m_pFile=0;
	}

	virtual void Open(const std::string& sName)=0;

	bool IsOpen() const
	{
		if(m_pFile)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	void Close()
	{
		if(m_pFile)
		{
			fclose(m_pFile);
			m_pFile=0;
		}
	}

	FILE* GetFile() {return m_pFile;}
	void SetFile(FILE* p) {m_pFile=p;}

	void ReadStr(char* p)
	{
		char temp[256];

		if(0==m_pFile)
		{
			return;
		}

		do
		{
			fgets(temp, 256, m_pFile);
		} while ((temp[0] == '/') || (temp[0] == '\n'));


		char* pTemp=&temp[0];
		while(*pTemp==' ')
		{
			pTemp++;
		}
		sprintf(p,pTemp);

		return;
	}

	void WriteStr(const char* p)
	{
		if(0==m_pFile)
		{
			return;
		}
		
		fputs(p,m_pFile);
	}

private:

	FILE* m_pFile;

protected:
	
	bool m_bUseBinary;

};

class FileWriter : public FileBase
{

public:

	FileWriter(const bool bUseBinary=false)
		: FileBase(bUseBinary)
	{
	}
	~FileWriter()
	{
	}

	void Open(const std::string& sName)
	{
		if(m_bUseBinary)
		{
			SetFile(fopen(sName.c_str(), "wb"));
		}
		else
		{
			SetFile(fopen(sName.c_str(), "wt"));
		}
	}

	void WriteBool(const bool b)
	{
		char line[256];
		if(b)
		{
			sprintf(line,"%d\n",1);
		}
		else
		{
			sprintf(line,"%d\n",0);
		}
		WriteStr(line);
	}

	void WriteInt(const int N)
	{
		char line[256];
		sprintf(line,"%d\n",N);
		WriteStr(line);
	}

	void WriteFloat(const float val)
	{
		char line[256];
		float f=val;
		sprintf(line,"%f\n",f);
		WriteStr(line);
	}

	void WriteString(const std::string& s)
	{
		std::string t(s);
		t.append("\n");
		const char* p=t.c_str();
		WriteStr(p);
	}

	void WriteComment(const std::string& s)
	{
		if(m_bUseBinary)
		{
			return;
		}
		
		std::string a("//");
		a.append(s);
		WriteString(a);
	}

private:

};

class FileReader : public FileBase
{

public:
		
	FileReader(const bool bUseBinary=false)
		: FileBase(bUseBinary)
	{
	}
	~FileReader()
	{
	}

	void Open(const std::string& sName)
	{
		if(m_bUseBinary)
		{
			SetFile(fopen(sName.c_str(), "rb"));
		}
		else
		{
			SetFile(fopen(sName.c_str(), "rt"));
		}
	}

	void ReadBool(bool& b)
	{
		char line[256];
		ReadStr(line);
		int N;
		sscanf(line,"%d\n",&N);
		if(0==N)
		{
			b=false;
		}
		else
		{
			b=true;
		}
	}

	void ReadInt(int& N)
	{
		char line[256];
		ReadStr(line);
		int i;
		sscanf(line,"%d\n",&i);
		N=i;
	}

	void ReadFloat(float& val)
	{
		char line[256];
		ReadStr(line);
		float f;
		sscanf(line,"%f\n",&f);
		val=f;
	}

	void ReadString(std::string& s)
	{
		char line[256];
		ReadStr(line);
		s=std::string(line);
		if(s.find("\n")!=std::string::npos)
		{
			s.erase(s.find("\n"));
		}
	}

private:
	
};

#endif
