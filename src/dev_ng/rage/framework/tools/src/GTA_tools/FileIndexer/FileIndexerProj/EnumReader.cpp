#include "EnumReader.h"
#include "FileFile.h"

bool EnumReader::Read(const std::string& sFileName)
{
     //Open the file.
     FileReader fileReader;
     fileReader.Open(sFileName);
     if(!fileReader.IsOpen())
     {
         return false;
     }
     
	//Read a line at a time until the string "enum" is found.
	bool bFoundEnumString=false;
	while(!bFoundEnumString)
	{
	     std::string sName;
		 fileReader.ReadString(sName);
		 if(-1!=sName.find("enum"))
		 {
		 	bFoundEnumString=true;
		}
	}
	
	//Read every line until the string "MAX_NUM_" is found.
	std::vector<std::string> enumStrings;
	std::vector<int> enumIndices;
	std::vector<int> enumBelongsTo;	//	available;
	bool bFoundMaxNumString=false;
	while(!bFoundMaxNumString)
	{
	    std::string sName;
		fileReader.ReadString(sName);
		if(-1!=sName.find("MAX_NUM_"))
		{
			bFoundMaxNumString=true;
		}
		else
		{

			if(-1!=sName.find(","))
			{

				if(-1!=sName.find("//PED_SCRIPT_DECISION"))
				{
					enumBelongsTo.push_back(1);	//	true);
				}
				else if (-1!=sName.find("//GROUP_SCRIPT_DECISION"))
				{
					enumBelongsTo.push_back(2);
				}
				else if (-1!=sName.find("//ALL_SCRIPT_DECISION"))
				{
					enumBelongsTo.push_back(3);
				}
				else
				{
					enumBelongsTo.push_back(0);	//	false);
				}
				
				sName.erase(sName.find(","));

				// remove any tabs from the string
				while( sName.find("\t") != -1 )
				{
					sName.erase( sName.find("\t"), sName.find("\t") + 1 );
				}
				
				if(-1!=sName.find(std::string("=")))
				{
					int index;
					char sNewName[256];
					sscanf(sName.c_str(),"%s = %d",sNewName,&index);	
					enumStrings.push_back(sNewName);
					enumIndices.push_back(index);
				}
				else
				{
					 enumStrings.push_back(sName);   
					 enumIndices.push_back(enumIndices.back()+1);
				}

			}
		}
	}	
	
	int i;
	const int N=enumBelongsTo.size();
	for(i=0;i<N;i++)
	{
		if(enumBelongsTo[i])	//	available[i])
		{
			m_enumStrings.push_back(enumStrings[i]);
			m_enumIndices.push_back(enumIndices[i]);
			m_enumBelongsTo.push_back(enumBelongsTo[i]);
		}
	}
	
	
	
	/*
     //Read a string until the end of the file.
     std::string sEnd("END");
     bool bFinished=false;
     while(false==bFinished)
     {
	     std::string sName;
		 fileReader.ReadString(sName);
         if(0==sName.compare(sEnd))
         {
             bFinished=true;
         }
         else
         {
             bFinished=false;
         	 if(-1!=sName.find(std::string("=")))
         	 {
         	 	int index;
         	 	char sNewName[256];
         	 	sscanf(sName.c_str(),"%s = %d",sNewName,&index);	
         	 	m_enumStrings.push_back(sNewName);
         	 	m_enumIndices.push_back(index);
         	 }
         	 else
         	 {
	             m_enumStrings.push_back(sName);   
	             m_enumIndices.push_back(m_enumIndices.back()+1);
         	 }
         }
     }
     */

     fileReader.Close();

     return true;
}

