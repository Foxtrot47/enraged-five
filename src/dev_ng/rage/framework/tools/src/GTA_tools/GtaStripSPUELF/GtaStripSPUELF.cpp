//
// GtaStripSpuElf utility:
// 
// This application pre-processes ELF files into a known form for SpuPM system to use at run-time.
//
// Usage
// GtaStripSPUELF input.elf output.elf
//
//	2009/01/20	-	Andrzej:	- initial: portions stolen from PhyreEngineStripSpuElf (2.2.0);
//	2009/03/04	-	Andrzej:	- integrated into Jimmy;
//
//
//
//
//
#define _CRT_SECURE_NO_WARNINGS	// disable VC nonsence complaining about nonsecure fopen(), etc.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//! GCC specific:
#ifdef __GNUC__
	#include <sys/types.h>
	#include <stdint.h>
#endif  //! __GNUC__

// Description:
// Helper function to build a four character code from individual chars.
// Arguments:
// c1 - First character of four character code.
// c2 - Second character of four character code.
// c3 - Third character of four character code.
// c4 - Fourth character of four character code.
// Returns:
// Four character code.
static inline unsigned int SEfourCC(unsigned char c1, unsigned char c2, unsigned char c3, unsigned char c4)
{
	return	((unsigned int)c1 << 24) |
			((unsigned int)c2 << 16) |
			((unsigned int)c3 <<  8) |
			((unsigned int)c4      );
}

// Description:
// Swap word macro to convert data when using PSSG on little endian machines.
// Arguments:
// _a : 4 byte value to swap.
// Note:
// This macro performs no operation on big endian machines.
#define SE_SWAP4(_a) ((((_a)&0xff000000)>>24)|(((_a)&0xff0000)>>8)|(((_a)&0xff00)<<8)|(((_a)&0xff)<<24))

// Description:
// Swap half-word macro to convert data when using PSSG on little endian machines.
// Arguments:
// _a : 2 byte value to swap.
// Note:
// This macro performs no operation on big endian machines.
#define SE_SWAP2(_a) ((((_a)&0xff00)>>8)|(((_a)&0xff)<<8))

// Description:
// Basic <c>max()</c> function for cross platform comparisons.
// Arguments:
// a :  First expression to compare.
// b :  Second expression to compare.
// Returns:
// Maximum value of a and b.
template <typename T> inline T maxOfPair(const T a, const T b) { return (a>b) ? a : b; }

// Description:
// Basic <c>min()</c> function for cross platform comparisons.
// Arguments:
// a :  First expression to compare.
// b :  Second expression to compare.
// Returns:
// Minimum value of a and b.
template <typename T> inline T minOfPair(const T a, const T b) { return (a<b) ? a : b; }

#include "X:\Jimmy\src\dev\game\renderer\SpuPM\SpuPmMgr.h"		// CElfInfo{}...

#ifdef _WIN32
	typedef unsigned short	uint16_t;
	typedef unsigned int	uint32_t;
	#define WIN32_LEAN_AND_MEAN	// Macro used on Win32 platforms to exclude rarely used definitions from Windows headers.
	#include <Windows.h>
#else //! _WIN32
	// todo?
#endif //! _WIN32


////////////////////////////////////////////////////////////////////////////////////////////////////////
//! Structures used for ELF parsing
// Description:
// ELF header.
typedef struct
{
	unsigned char e_ident[16];	// File identifier.
	uint16_t    e_type;			// File type.
	uint16_t    e_machine;		// Target architecture.
	uint32_t    e_version;		// ELF version.
	uint32_t    e_entry;		// Starting virtual address.
	uint32_t    e_phoff;		// Program header offset.
	uint32_t    e_shoff;		// Section header offset.
	uint32_t    e_flags;		// Architecture specific flags.
	uint16_t    e_ehsize;		// ELF header size.
	uint16_t    e_phentsize;	// Program header entry size.
	uint16_t    e_phnum;		// Number of program header entries.
	uint16_t    e_shentsize;	// Section header entry size.
	uint16_t    e_shnum;		// Number of section header entries.
	uint16_t    e_shstrndx;		// String section header index.
} Elf32_Ehdr;

// Description:
// ELF program header.
typedef struct
{
	uint32_t    p_type;			// The segment type.
	uint32_t    p_offset;		// Offset to the segment.
	uint32_t    p_vaddr;		// Virtual address of the segment.
	uint32_t    p_paddr;		// Physical address of the segment.
	uint32_t    p_filesz;		// File size of the segment.
	uint32_t    p_memsz;		// Memory size of the segment.
	uint32_t    p_flags;		// Flags for the segment.
	uint32_t    p_align;		// Alignment requirements for the segment.
} Elf32_Phdr;

// Description:
// ELF section header.
typedef struct
{
	uint32_t    sh_name;		// Name of the section.
	uint32_t    sh_type;		// Type of the section.
	uint32_t    sh_flags;		// Attributes of the section.
	uint32_t    sh_addr;		// Memory location of the section.
	uint32_t    sh_offset;		// Offset of the section.
	uint32_t    sh_size;		// Size of the section.
	uint32_t    sh_link;		// Section dependent information.
	uint32_t    sh_info;		// Extra information for the section.
	uint32_t    sh_addralign;	// Address alignment for the section.
	uint32_t    sh_entsize;		// Size of entry in the section.
} Elf32_Shdr;

// Description:
// Symbol table entry.
typedef struct
{
	uint32_t    	st_name;      // Symbol name.
	uint32_t    	st_value;     // Symbol value.
	uint32_t    	st_size;      // Symbol size.
	unsigned char	st_info;      // Symbol type and binding.
	unsigned char	st_other;     // Symbol visibility.
	uint16_t 		st_shndx;  	  // Section index.
} Elf32_Sym;

// To check for sections that occupy memory during execution
#define SHF_ALLOC		(0x2)

// To check for bss section
#define SHT_NOBITS		(8)

// To check for undefined section references
#define SHN_UNDEF		(0)

// To check for the symbol table
#define SHT_SYMTAB		(2)
////////////////////////////////////////////////////////////////////////////////////////////////////////

// LS size
#define PC_LS_SIZE                      (256*1024)

////////////////////////////////////////////////////////////////////////////////////////////////////////






// Description:
// Parses SPU ELF file and populates an <c>PElfInfo</c> structure with information about the SPU ELF.
// Arguments:
// elfInfo : <c>PElfInfo</c> structure to receive the parsed information from the SPU ELF file.
// spuElf :  Pointer to SPU ELF
// name :    Contextual identifier to be printed out with warnings/errors if they occur.
// imageStart: Pointer to the start of the SPU ELF image will be stored here.
// programName: The name of the program for error reporting.
// Returns:
// 0 - No error during the parse process.
// -1 - An error happened furing the parse process.
static int ParseELF(CElfInfo &elfInfo, const void *spuElf, const char *name, void *&imageStart, const char *programName)
{
	if (!name)
		name = "Unknown";

	// Magic number to identify an elf
	unsigned int elfMagicNumber = SEfourCC(0x7f, 'E', 'L', 'F');

	const Elf32_Ehdr *ehdr = (const Elf32_Ehdr *)spuElf;
	const Elf32_Shdr *shdr;
	const Elf32_Shdr *sh;

	unsigned int spuImageStart			= ~0U;
	unsigned int spuImageEnd			= 0U;
	unsigned int spuImageLSDestination	= ~0U;

	const unsigned int *elfIdentifier = (const unsigned int *)spuElf;

	// Check if it is actualy and elf
	if ((SE_SWAP4(*elfIdentifier)) != elfMagicNumber)
	{
		fprintf(stderr,"%s:ParseELF(%s): Not a valid ELF file.", programName, name);
		return -1;
	}

	// Verify the machine code. SPU=0x17
	if (SE_SWAP2(ehdr->e_machine) != 0x17) {
		fprintf(stderr,"%s:ParseELF(%s): Elf is not an spu ELF.", programName, name);
		return -1;
	}

    unsigned int bssStart = 0;
    unsigned int bssSize = 0;

	shdr = (const Elf32_Shdr *)((const char *)ehdr + (SE_SWAP4(ehdr->e_shoff)));

	// Look for a ctors section - flag this if found, since we don't support global constructors in modifiers
	if ((SE_SWAP2(ehdr->e_shstrndx) != SHN_UNDEF) && (SE_SWAP2(ehdr->e_shstrndx) < SE_SWAP2(ehdr->e_shnum)))
	{
		const Elf32_Shdr &nameSection = shdr[SE_SWAP2(ehdr->e_shstrndx)];
		const char *strTable = ((const char *)ehdr + (SE_SWAP4(nameSection.sh_offset)));
		bool ctorsFound = false;

		for (sh=shdr; sh<&shdr[SE_SWAP2(ehdr->e_shnum)]; ++sh)
		{
			ctorsFound = ctorsFound || (strcmp(&strTable[SE_SWAP4(sh->sh_name)], ".ctors") == 0);
		}

		if (ctorsFound)
			printf("%s:ParseELF(%s): Warning: Global constructors found in ELF file will not be executed.\n", programName, name);
	}

	unsigned int preProcessEntry = ~0U;

	// Calulate image size, bss parameters and ls destination address
	// Also check for the modifier preProcess function entry
	for (sh = shdr; sh < &shdr[SE_SWAP2(ehdr->e_shnum)];++sh)
	{
		// Search the symbol table for the preProcess function entry
        if ( SE_SWAP4(sh->sh_type)  == SHT_SYMTAB )
        {
        	const char * symStrtab = (char *)ehdr + (SE_SWAP4(shdr[SE_SWAP4(sh->sh_link)].sh_offset));
        	const Elf32_Sym *firstSym = (const Elf32_Sym *)((const char *)ehdr + (SE_SWAP4(sh->sh_offset)));
        	const Elf32_Sym *lastSym = (const Elf32_Sym *)((const char *)ehdr + (SE_SWAP4(sh->sh_offset) + SE_SWAP4(sh->sh_size)));
        	for (const Elf32_Sym *currentSym = firstSym; currentSym < lastSym; currentSym++)
			{
                  if (strcmp("_Z12__preProcessPcS_",&symStrtab[SE_SWAP4(currentSym->st_name)]) == 0)
				  {
                        preProcessEntry = SE_SWAP4(currentSym->st_value);
						break;
				  }
			}
        }

		// Only consider segments that have the ALLOC flag set
		if ((SE_SWAP4(sh->sh_flags)&SHF_ALLOC) && (SE_SWAP4( sh->sh_size )))
		{
			// Find image start address
			spuImageStart = minOfPair(spuImageStart, SE_SWAP4(sh->sh_offset));

			// Check if this section is a BSS section and remember its details
			if(SE_SWAP4(sh->sh_type) & SHT_NOBITS)
			{
				bssStart = SE_SWAP4(sh->sh_offset);
				bssSize = SE_SWAP4(sh->sh_size);
			}

			// Find image end address
			spuImageEnd = maxOfPair(spuImageEnd, (SE_SWAP4(sh->sh_offset) + SE_SWAP4(sh->sh_size)));

			// Find lowest destination address
			spuImageLSDestination = minOfPair(spuImageLSDestination, SE_SWAP4(sh->sh_addr));
		}
	}

	// Check if we have no bss section
	if (bssSize)
	{
		fprintf(stderr,"%s:ParseELF(%s): Elf has a non-zero BSS section.\n", programName, name);
        return -1;
	}

	// Round size up the next 16 bytes for transfer
	elfInfo.m_imageSize = ((spuImageEnd - spuImageStart) + 15 ) & ~15;
	if ( elfInfo.m_imageSize > PC_LS_SIZE )
	{
		fprintf(stderr,"%s:ParseELF(%s): Elf size is bigger than spu LS.\n", programName, name);
		return -1;
	}
	elfInfo.m_LSDestination = spuImageLSDestination;
	elfInfo.m_entry = SE_SWAP4(ehdr->e_entry);
	elfInfo.m_preProcessEntry = preProcessEntry;

	imageStart = (void *) ((unsigned char *)spuElf + spuImageStart);

	return 0;
}




#define COMPILE_TIME_ASSERT(e) typedef char __COMPILE_TIME_ASSERT__[(e)?1:-1]

//
//
//
//
int main(int argc, char *argv[])
{
	// Check if PElfInfo is a multiple of 16)
	COMPILE_TIME_ASSERT((sizeof(CElfInfo)%16) == 0);

	// Determine the executable name minus the path from where it came from
	char *programName = argv[0];
	if (strrchr(programName, '\\'))
		programName = strrchr(programName, '\\') + 1;
	if (strrchr(programName, '/'))
		programName = strrchr(programName, '/') + 1;
		
	// Need at least src and destination file name
	if (argc < 3)
	{
		fprintf(stderr, "%s: Error: Input and/or output ELF file not specified\n", programName);
		return -1;
	}
	
	// Open input and output file
	FILE *inFile = fopen(argv[1], "rb");
	if (!inFile)
	{
		fprintf(stderr, "%s: Error: Could not open %s\n", programName, argv[1]);
		return -1;
	}

	FILE *outFile = fopen(argv[2], "wb");
    if (!outFile)
	{
        fprintf(stderr, "%s: Error: Could not open %s\n", programName, argv[2]);
		return -1;
	}

	// Read in ELF
	fseek(inFile, 0, SEEK_END);
	unsigned long fileSize = ftell(inFile);
	fseek(inFile, 0, SEEK_SET);
	void *spuElf = (void *) malloc(fileSize); 
	if (!spuElf)
	{
		fprintf(stderr, "%s: Error: Could not allocate memory\n", programName);
		return -1;
	}
	fread(spuElf, fileSize,1, inFile);

	// Parse the ELF
	CElfInfo elfInfo;
	void *imageStart;
	int returnVal = ParseELF(elfInfo, (const void *)spuElf, argv[1], imageStart, programName);
	if (returnVal)
	{
		fprintf(stderr, "%s: Error: Could not parse %s.\n", programName, argv[1]);
		return -1;
	}

#ifdef _DEBUG
	printf("Info for %s: ls dest %x, ls size %x, ls entry %x, ls pre-process entry %x\n", argv[1],
				 elfInfo.m_LSDestination, elfInfo.m_imageSize, elfInfo.m_entry, elfInfo.m_preProcessEntry);
#endif

	// Remember image start and size
	unsigned int imageSize = elfInfo.m_imageSize;

	// Swap ELF info for cell
	elfInfo.m_LSDestination		= SE_SWAP4(elfInfo.m_LSDestination);
	elfInfo.m_imageSize			= SE_SWAP4(elfInfo.m_imageSize);
	elfInfo.m_entry				= SE_SWAP4(elfInfo.m_entry);
	elfInfo.m_preProcessEntry	= SE_SWAP4(elfInfo.m_preProcessEntry);

	// Write output file.
	fwrite(&elfInfo, sizeof(elfInfo), 1, outFile);
	fwrite(imageStart, imageSize, 1, outFile);

	// Close files and free memory.
	fclose(inFile);
	fclose(outFile);
	free(spuElf);

	return 0;
}
