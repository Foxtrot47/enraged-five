
#define KEY_SIZE (32)	//	was 8

class key_entry_class
{
public:
	//	UInt16 *data;
	char *pointer_to_data_using_char;
	UInt8 key[KEY_SIZE];
	UInt32 HashKey;

	void uppercaserize(void);
};

class key_array_class
{
public:
	key_entry_class *pBuffer;
	UInt32 count;
	UInt32 total_number_of_keys;

	key_array_class(UInt32 number_of_keys);
	~key_array_class(void);
	void add(UInt8 *key, char *data_using_char);
	char *find(char *key);
	void save(void);
	void sort(void);
	void uppercaserize(void);
};


class data_class
{
public:
//	UInt16 *pBuffer;
	char *text_buffer;
	UInt32 count;
	UInt32 size_of_buffer;


	data_class(UInt32 size);
	~data_class(void);
//	UInt16 *add(UInt16 *text);
	char *data_class::add(char *text);
	void save(void);
};


#define MISSION_NAME_LENGTH	(8)
#define MAX_MISSION_FILES	(1000)

class mission_file_class
{
public:
	char filenames[MAX_MISSION_FILES][MISSION_NAME_LENGTH];
	UInt16 num_of_mission_files;
	
	mission_file_class(void) { num_of_mission_files = 0; }
	void add(char *new_text_key);
	void initialise(void);
	void alphabeticise(void);
	UInt32 check_for_mission_filenames( UInt8 *in, Int32 inlen, Bool8 bJapanese);
};

struct japanese_character
{
	UInt16 CharacterCode;
	UInt16 Left;
	UInt16 Top;
	UInt16 Width;
	UInt16 Height;
};

