#undef UNICODE
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>


void main(int argc, char *argv[])
{
	if ( (argc < 4) || ( (argc % 2) != 0) )
	{
		printf("You need to pass in at least three strings. There should be pairs of imgs and sco folders.\n");
		printf("1) Location of batch file\n");
		printf("2) First img location\n");
		printf("3) First sco folder\n");
		printf("4) Second img location\n");
		printf("5) Second sco folder\n");
		printf("etc...\n\n");

		for (int loop = 0; loop < argc; loop++)
		{
			printf("Arg %d is %s\n", loop, argv[loop]);
		}

		exit(0);
	}

	int folder_to_check = 3;

	int most_recently_modified_folder = -1;

	FILETIME TimeOfMostRecentFolder;
	TimeOfMostRecentFolder.dwLowDateTime = 0;
	TimeOfMostRecentFolder.dwHighDateTime = 0;

	while (folder_to_check < argc)
	{
		printf("ProjectCompiler2 - checking the time/date of files in %s\n", argv[folder_to_check]);
		char path[MAX_PATH];
		WIN32_FIND_DATA fd;
		DWORD dwAttr = FILE_ATTRIBUTE_DIRECTORY;
		sprintf_s( path, MAX_PATH, "%s\\*", argv[folder_to_check]);
		HANDLE hFind = FindFirstFile( path, &fd);
		if(hFind != INVALID_HANDLE_VALUE)
		{
			do
			{
				if( !(fd.dwFileAttributes & dwAttr))
				{
					//	ftCreationTime or ftLastAccessTime or ftLastWriteTime
					if (CompareFileTime(&TimeOfMostRecentFolder, &fd.ftLastWriteTime) < 0)
					{
						printf("%s is the most recent file found so far\n", fd.cFileName);
						if (most_recently_modified_folder != folder_to_check)
						{
							most_recently_modified_folder = folder_to_check;
							printf("ProjectCompiler2 - Will make %s from %s\n", argv[most_recently_modified_folder-1], argv[most_recently_modified_folder]);
						}
						TimeOfMostRecentFolder = fd.ftLastWriteTime;
					}
				}
			}while( FindNextFile( hFind, &fd));
			FindClose( hFind);
		}
		else
		{
			printf("ProjectCompiler2 - Couldn't find %s directory\n", argv[folder_to_check]);
		}

		folder_to_check += 2;
	}

	//	Without this, the output of the buildimgfromfolder below is printed in the Rage Script Editor before the printf's above
	fflush(stdout);

	if (most_recently_modified_folder > 0)
	{
#define MAX_LENGTH_OF_BATCH_FILE_STRING	(1024)
		char batchFileString[MAX_LENGTH_OF_BATCH_FILE_STRING];
		sprintf_s(batchFileString, MAX_LENGTH_OF_BATCH_FILE_STRING, "%s %s %s", argv[1], argv[most_recently_modified_folder-1], argv[most_recently_modified_folder]);
		system(batchFileString);
	}
}

