//------------------------------------------------------------------------------
// $Workfile: Metrics1.h $
//   Created: Gottfried Chen
// Copyright: 2003, rockstar vienna
// $Revision: $
//   $Author: $
//     $Date: $
//------------------------------------------------------------------------------
// Description: 
//------------------------------------------------------------------------------

#ifndef METRICS1_H
#define METRICS1_H

#include <fstream>

#include "Metrics.h"

class Metrics1 : public Metrics
{
public:
    Metrics1(const char* path) : Metrics(path) {}

    virtual
    void writeHeader(const char* texture, const char* mask = 0);

    virtual
    void writeCharacter(unsigned short characterCode,
                        int left, int top, int right, int bottom);
};

#endif // METRICS1_H