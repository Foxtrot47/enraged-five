//------------------------------------------------------------------------------
// $Workfile: Metrics2.h $
//   Created: Gottfried Chen
// Copyright: 2003, rockstar vienna
// $Revision: $
//   $Author: $
//     $Date: $
//------------------------------------------------------------------------------
// Description: 
//------------------------------------------------------------------------------

#ifndef METRICS2_H
#define METRICS2_H

#include <fstream>

#include "Metrics.h"

class Metrics2 : public Metrics
{
public:
    Metrics2(const char* path) : Metrics(path) {}

    virtual
    void writeHeader(const char* texture, const char* mask = 0);

    // int parameters are unused in Metrics2
    virtual
    void writeCharacter(unsigned short character, int, int, int, int);
};

#endif // METRICS2_H