//------------------------------------------------------------------------------
// $Workfile: FontOptions.h $
//   Created: Gottfried Chen
// Copyright: 2003, rockstar vienna
// $Revision: $
//   $Author: $
//     $Date: $
//------------------------------------------------------------------------------
// Description: 
//------------------------------------------------------------------------------
#pragma once
#ifndef FONT_OPTIONS_H
#define FONT_OPTIONS_H

struct FontOptions
{
    FontOptions() :
    mSupersample(1),
    mWidth1(0),
    mHeight1(0),
    mWidth2(0),
    mHeight2(0),
    mFontSize(0),
    mBorder(0),
    mTextFile(0),
    mFontName(0),
    mMetrics(0), // 0 is Metrics1, 1 is Metrics2
    mWeight(0)
    {
    }

    int mSupersample;
    int mWidth1;
    int mHeight1;
    int mWidth2;
    int mHeight2;
    int mFontSize;
    int mBorder;
    const char* mTextFile;
    const char* mFontName;
    int mMetrics;
    int mWeight;
};

#endif