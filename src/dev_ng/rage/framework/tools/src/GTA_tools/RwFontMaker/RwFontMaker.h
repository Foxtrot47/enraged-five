// RwFontMaker.h : Hauptheaderdatei f�r die RwFontMaker-Anwendung
//

#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// Hauptsymbole


// CRwFontMakerApp:
// Siehe RwFontMaker.cpp f�r die Implementierung dieser Klasse
//

class CRwFontMakerApp : public CWinApp
{
public:
	CRwFontMakerApp();

// �berschreibungen
	public:
	virtual BOOL InitInstance();

// Implementierung

//	DECLARE_MESSAGE_MAP()
};

extern CRwFontMakerApp theApp;
