//------------------------------------------------------------------------------
// $Workfile: Metrics2.cpp $
//   Created: Gottfried Chen
// Copyright: 2003, rockstar vienna
// $Revision: $
//   $Author: $
//     $Date: $
//------------------------------------------------------------------------------
// Description: 
//------------------------------------------------------------------------------

#include "stdafx.h"
#include "Metrics2.h"

#include <assert.h>
#include <iomanip>
#include <windows.h>

#define ENDL '\r'<<std::endl;


//------------------------------------------------------------------------------    
void Metrics2::writeHeader(const char* texture, const char* mask)
//------------------------------------------------------------------------------
{
    assert(texture != 0);
    if (mStream)
    {
        mStream << "METRICS2" << ENDL;
        mStream << texture;
        if (mask)
        {
            mStream << ' ' << mask;
        }
        mStream << ENDL;

        // Write the baseline
        mStream << 5 << ENDL;
        mHeaderWritten = true;
    }
}

//------------------------------------------------------------------------------
void Metrics2::writeCharacter(unsigned short character, int, int, int, int)
//------------------------------------------------------------------------------
{
    assert(mHeaderWritten);
    if (mStream && mHeaderWritten)
    {
        char buffer[20];
        int ok = WideCharToMultiByte(
            CP_UTF8, 0,
            (LPCWSTR)&character, 1, 
            buffer, 20,
            0, 0);
        assert(ok > 0);
        buffer[ok] = 0;
        mStream << buffer << ENDL;
    }
}
                        
