//------------------------------------------------------------------------------
// $Workfile: UsedCharactersFilter.h $
//   Created: Gottfried Chen
// Copyright: 2003, rockstar vienna
// $Revision: $
//   $Author: $
//     $Date: $
//------------------------------------------------------------------------------
// Description: 
//------------------------------------------------------------------------------

#ifndef USED_CHARACTERS_FILTER_H
#define USED_CHARACTERS_FILTER_H

#include <set>
#include <fstream>
#include <assert.h>

// Parses a text file and generates a string with all unique
// characters used in the file.

template<class Char>
class UsedCharactersFilter
{
public:
    void parse(const char* fileName);
    
    size_t getCharacterCount();
    
    // No '\0' is appended at the end of the string.
    void fillCharacterString(Char*);

private:
    typedef std::set<Char> CharacterSet;
    CharacterSet mCharacters;
    
    unsigned short NumberOfOccurrences[65535];
    unsigned short CharacterCode[65535];
    
};

template<class Char>
//------------------------------------------------------------------------------
void UsedCharactersFilter<Char>::parse(const char* fileName)
//------------------------------------------------------------------------------
{
	unsigned long loop, loop2, stop_var;
	unsigned short temp_val, line_number;
	FILE *pCountFile;

    std::ifstream file(fileName, std::ios_base::binary);
    bool ok = (file != 0);
    mCharacters.clear();
    if (sizeof(Char) == 2 && ok)
    {
        unsigned short unicodeHeader;
        file.read((char*)&unicodeHeader, 2);
        assert(unicodeHeader == 0xfeff);
        ok = unicodeHeader == 0xfeff;
    }

	for (loop = 0; loop < 65535; loop++)
	{
		NumberOfOccurrences[loop] = 0;
		CharacterCode[loop] = 0;
	}

    stop_var = 7;

    if (ok)
    {
        while(file)
        {
            Char character;
            file.read((char*)&character, sizeof(Char));
            if (character != '\t' && character != ' ' &&
                character != '\r' && character != '\n')
            {
                mCharacters.insert(character);
                if (character < 65535)
                {
                	NumberOfOccurrences[character]++;
                	CharacterCode[character] = character;
                }
                else
                {
                	stop_var = 12;
                }
            }
        }
    }
    
    stop_var = 7;

    for (loop = 0; loop < 65535; loop++)
    {
    	for (loop2 = (loop + 1); loop2 < 65535; loop2++)
    	{
    		if (NumberOfOccurrences[loop2] > NumberOfOccurrences[loop])
    		{
    			temp_val = NumberOfOccurrences[loop];
    			NumberOfOccurrences[loop] = NumberOfOccurrences[loop2];
    			NumberOfOccurrences[loop2] = temp_val;
    			temp_val = CharacterCode[loop];
    			CharacterCode[loop] = CharacterCode[loop2];
    			CharacterCode[loop2] = temp_val;
    		}
    	}
    }

	line_number = 1;
    
    pCountFile = fopen("kanji_counter.txt", "w");
    
    if (pCountFile)
    {
	    for (loop = 0; loop < 65535; loop++)
	    {
			if (NumberOfOccurrences[loop] > 0)
			{
//	#pragma wchar_type on
//				fprintf(pCountFile, "%d\t%c\t%d\t\t%d\n", line_number, CharacterCode[loop], CharacterCode[loop], NumberOfOccurrences[loop]);
//	should I switch wide characters off here?
				fprintf(pCountFile, "%d\t\t%d\n", CharacterCode[loop], NumberOfOccurrences[loop]);
				line_number++;
			}
	    }
	    
	    fclose(pCountFile);
	}
    
	stop_var = 23;
}

template<class Char>
//------------------------------------------------------------------------------
size_t UsedCharactersFilter<Char>::getCharacterCount()
//------------------------------------------------------------------------------
{
    return int(mCharacters.size());
}

template<class Char>
//------------------------------------------------------------------------------
void UsedCharactersFilter<Char>::fillCharacterString(Char* string)
//------------------------------------------------------------------------------
{
/*
    for (CharacterSet::const_iterator i = mCharacters.begin();
         i != mCharacters.end();
         i++)
    {
        *string = *i;
        ++string;
    }
*/
	unsigned long loop;

    for (loop = 0; loop < 65535; loop++)
    {
		if (NumberOfOccurrences[loop] > 0)
		{
			*string = CharacterCode[loop];
			++string;
		}
	}

}

#endif // #ifndef USED_CHARACTERS_FILTER_H