//------------------------------------------------------------------------------
// $Workfile: TextureBuilder.cpp $
//   Created: Gottfried Chen
// Copyright: 2003, rockstar vienna
// $Revision: $
//   $Author: $
//     $Date: $
//------------------------------------------------------------------------------
// Description: 
//------------------------------------------------------------------------------
#include "stdafx.h"
#include "TextureBuilder.h"
#include "FontOptions.h"
#include "Metrics1.h"
#include "Metrics2.h"

#include <windows.h>
#include <assert.h>
#include <stdio.h>
#include <wstring.h>

using namespace std;
//#define DRAW_DEBUG_BORDER
static
//------------------------------------------------------------------------------
void GenerateMemoryBitmap(HDC target, FontOptions *options, wchar_t* string)
//------------------------------------------------------------------------------                          
{
    char buffer[128];
    RECT rc = {0, 0, 0, 0};
    
    sprintf(buffer, "%s%d.met", options->mFontName, options->mFontSize);
    Metrics* metrics = 0;
    if (options->mMetrics == 0)
    {
        metrics = new Metrics1(buffer);
    }
    else
    {
        metrics = new Metrics2(buffer);
    }


    sprintf(buffer, "%s%d.bmp", options->mFontName, options->mFontSize);
    metrics->writeHeader(buffer);
    

#ifdef DRAW_DEBUG_BORDER
    HBRUSH borderBrush = CreateSolidBrush(RGB(0,255,0));
#endif

    size_t len = wcslen(string);
    for(unsigned int n = 0; n<len; ++n)
    {
        wchar_t c = string[n];
        int nCellWidth, nCellHeight;

        // Calculate character dimensions
        DrawTextW(target, &c, 1, &rc, DT_CALCRECT | DT_NOPREFIX | DT_SINGLELINE);

        /*
         * Ensure an exact multiple of options->mSupersample...
         */
        nCellWidth = ((rc.right - rc.left + (options->mSupersample - 1)) /
            options->mSupersample) * options->mSupersample;

        nCellHeight = ((rc.bottom - rc.top + (options->mSupersample - 1)) /
             options->mSupersample) * options->mSupersample;

        // -1 because a rect does not include the right and bottom border
        rc.right = rc.left + nCellWidth + 2*options->mBorder - 1;
        rc.bottom = rc.top + nCellHeight + 2*options->mBorder - 1;

        // Step down to next line
        if( (rc.right) >= options->mWidth1 * options->mSupersample )
        {
            int nCellHeight = rc.bottom - rc.top;

            rc.top = rc.bottom;
            rc.bottom = rc.top + nCellHeight;

            if (rc.bottom > options->mHeight1)
            {
                MessageBox(0, "Texture dimensions are too small.", "Error", MB_OK);
                delete metrics;
                return;
            }

            rc.left = 0;
            rc.right = rc.left + nCellWidth + 2*options->mBorder - 1;
        }

        
        // Draw the character to the texture
        RECT characterRect;
        characterRect.left = rc.left + options->mBorder;
        characterRect.right = rc.right - options->mBorder+1;
        characterRect.top = rc.top + options->mBorder;
        characterRect.bottom = rc.bottom - options->mBorder+1;
        DrawTextW(target, &c, 1, &characterRect, DT_NOPREFIX | DT_SINGLELINE);

#ifdef DRAW_DEBUG_BORDER
        FrameRect(target, &characterRect, borderBrush);
#endif

        //Draw a single red pixel in the upper right corner in Metrics2
        if (options->mMetrics == 1)
        {
            SetPixel(target, rc.right, rc.top, RGB(255,0,0));
            // First character also has little dot in the upper and lower left corner 
            if (n == 0)
            {
                SetPixel(target, rc.left, rc.top, RGB(255,0,0));
                SetPixel(target, rc.left, rc.bottom, RGB(255,0,0));
            }
        }

        
        // Write the dimensions to the metrics file
        metrics->writeCharacter(
            unsigned short(c),
            (int)(rc.left / options->mSupersample),
            (int)(rc.top / options->mSupersample),
            (int)(rc.right / options->mSupersample),
            (int)(rc.bottom / options->mSupersample));
        
        // Step to next character
        rc.left = rc.right /*+ options->rmargin * options->mSupersample*/;
    }

#ifdef DRAW_DEBUG_BORDER
    DeleteObject(borderBrush);
#endif
    delete metrics;
}


static
//--------------------------------------------------------------------------------------
void GenerateMemoryBitmapEqualSpacing(HDC target, FontOptions *options, wchar_t* string)
//--------------------------------------------------------------------------------------
{
    char buffer[128];
    unsigned int n;
    RECT rc = {0, 0, 0, 0};
    int nCellWidth, nCellHeight;
    
    sprintf(buffer, "%s%d.met", options->mFontName, options->mFontSize);
    Metrics* metrics = 0;
    if (options->mMetrics == 0)
    {
        metrics = new Metrics1(buffer);
    }
    else
    {
        metrics = new Metrics2(buffer);
    }


    sprintf(buffer, "%s%d.bmp", options->mFontName, options->mFontSize);
    metrics->writeHeader(buffer);
    

#ifdef DRAW_DEBUG_BORDER
    HBRUSH borderBrush = CreateSolidBrush(RGB(0,255,0));
#endif

    size_t len = wcslen(string);
    
    int GreatestWidth = 0;
    int GreatestHeight = 0;
    
//	Calculate greatest width and height
    for(n = 0; n<len; ++n)
    {
        wchar_t c = string[n];

        // Calculate character dimensions
        DrawTextW(target, &c, 1, &rc, DT_CALCRECT | DT_NOPREFIX | DT_SINGLELINE);

        /*
         * Ensure an exact multiple of options->mSupersample...
         */
        nCellWidth = ((rc.right - rc.left + (options->mSupersample - 1)) /
            options->mSupersample) * options->mSupersample;

        nCellHeight = ((rc.bottom - rc.top + (options->mSupersample - 1)) /
             options->mSupersample) * options->mSupersample;
             
		if (nCellWidth > GreatestWidth)
		{
			GreatestWidth = nCellWidth;
		}
		
		if (nCellHeight > GreatestHeight)
		{
			GreatestHeight = nCellHeight;
		}
             
        // -1 because a rect does not include the right and bottom border
//        rc.right = rc.left + nCellWidth + 2*options->mBorder - 1;
//        rc.bottom = rc.top + nCellHeight + 2*options->mBorder - 1;
	}

	//	Reset rc
	rc.left = 0;
	rc.right = 0;
	rc.top = 0;
	rc.bottom = 0;
    
    for(n = 0; n<len; ++n)
    {
        wchar_t c = string[n];
//        int nCellWidth, nCellHeight;

        // Calculate character dimensions
//        DrawTextW(target, &c, 1, &rc, DT_CALCRECT | DT_NOPREFIX | DT_SINGLELINE);

        /*
         * Ensure an exact multiple of options->mSupersample...
         */
/*
        nCellWidth = ((rc.right - rc.left + (options->mSupersample - 1)) /
            options->mSupersample) * options->mSupersample;

        nCellHeight = ((rc.bottom - rc.top + (options->mSupersample - 1)) /
             options->mSupersample) * options->mSupersample;
*/
        // -1 because a rect does not include the right and bottom border
        rc.right = rc.left + GreatestWidth + 2*options->mBorder - 1;
        rc.bottom = rc.top + GreatestHeight + 2*options->mBorder - 1;

        // Step down to next line
        if( (rc.right) >= options->mWidth1 * options->mSupersample )
        {
            nCellHeight = rc.bottom - rc.top;

            rc.top = rc.bottom;
            rc.bottom = rc.top + nCellHeight;

            if (rc.bottom > options->mHeight1)
            {
                MessageBox(0, "Texture dimensions are too small.", "Error", MB_OK);
                delete metrics;
                return;
            }

            rc.left = 0;
//            rc.right = rc.left + nCellWidth + 2*options->mBorder - 1;
			rc.right = rc.left + GreatestWidth + 2*options->mBorder - 1;
        }

        
        // Draw the character to the texture
        RECT characterRect;
        characterRect.left = rc.left + options->mBorder;
        characterRect.right = rc.right - options->mBorder+1;
        characterRect.top = rc.top + options->mBorder;
        characterRect.bottom = rc.bottom - options->mBorder+1;
        DrawTextW(target, &c, 1, &characterRect, DT_NOPREFIX | DT_SINGLELINE);

#ifdef DRAW_DEBUG_BORDER
        FrameRect(target, &characterRect, borderBrush);
#endif

        //Draw a single red pixel in the upper right corner in Metrics2
        if (options->mMetrics == 1)
        {
            SetPixel(target, rc.right, rc.top, RGB(255,0,0));
            // First character also has little dot in the upper and lower left corner 
            if (n == 0)
            {
                SetPixel(target, rc.left, rc.top, RGB(255,0,0));
                SetPixel(target, rc.left, rc.bottom, RGB(255,0,0));
            }
        }

        
        // Write the dimensions to the metrics file
        metrics->writeCharacter(
            unsigned short(c),
            (int)(rc.left / options->mSupersample),
            (int)(rc.top / options->mSupersample),
            (int)(rc.right / options->mSupersample),
            (int)(rc.bottom / options->mSupersample));
        
        // Step to next character
        rc.left = rc.right /*+ options->rmargin * options->mSupersample*/;
    }

#ifdef DRAW_DEBUG_BORDER
    DeleteObject(borderBrush);
#endif
    delete metrics;
}


//------------------------------------------------------------------------------
void ProcessFont(FontOptions* options, wchar_t* string)
//-------------------------------------- ----------------------------------------
{
    // Create DC
    HDC hdcMemory = CreateCompatibleDC(NULL);

    // Create the bitmap
    BITMAPINFO bmi;
    void *pPixels;
    ZeroMemory(&bmi.bmiHeader, sizeof(BITMAPINFOHEADER));
    bmi.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
    bmi.bmiHeader.biWidth = options->mWidth1 * options->mSupersample;
    bmi.bmiHeader.biHeight = options->mHeight1 * options->mSupersample;
    bmi.bmiHeader.biPlanes = 1;
    bmi.bmiHeader.biBitCount = 24;
    bmi.bmiHeader.biCompression = BI_RGB;

    HBITMAP hbmp = CreateDIBSection(hdcMemory, &bmi, DIB_RGB_COLORS, &pPixels, NULL, 0);

    // Select it into the current DC
    HBITMAP hbmpOusted = (HBITMAP) SelectObject(hdcMemory, hbmp);

    // Create the font. If a font is not available a substitute font will be selected instead.
    HFONT hfnt = CreateFont(
        -MulDiv(options->mFontSize * options->mSupersample,
                GetDeviceCaps(hdcMemory, LOGPIXELSY),
                72),
        0, 0, 0,
        options->mWeight, FALSE, FALSE, FALSE,
        DEFAULT_CHARSET, OUT_DEFAULT_PRECIS,
        CLIP_DEFAULT_PRECIS,
        PROOF_QUALITY,
        DEFAULT_PITCH | FF_DONTCARE,
        _T(options->mFontName));

    // Select the font into the DC
    HFONT hfntOusted = (HFONT) SelectObject(hdcMemory, hfnt);

    // Clear bitmap to black
    RECT rcFill =
    {
        0,
        0,
        options->mWidth1 * options->mSupersample,
        options->mHeight1 * options->mSupersample
    };
    FillRect(hdcMemory, &rcFill, (HBRUSH) GetStockObject(BLACK_BRUSH));

    // Select text and background colors
    SetBkColor(hdcMemory, RGB(0x00, 0x00, 0x00));
    SetTextColor(hdcMemory, RGB(0xFF, 0xFF, 0xFF));

    // Generate the bitmap and write the metrics
//    GenerateMemoryBitmap(hdcMemory, options, string);
	GenerateMemoryBitmapEqualSpacing(hdcMemory, options, string);

    // Delete the font
    DeleteObject(SelectObject(hdcMemory, hfntOusted));

    // Write the bitmap to file
    SelectObject(hdcMemory, hbmpOusted);
    {
        char filename[128];
        FILE *fp;

        sprintf(filename, "%s%d.bmp", options->mFontName, options->mFontSize);
        fp = fopen(filename, "wb");

        int nHeaderSize = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);

        int nPixelsSize = bmi.bmiHeader.biHeight * (((bmi.bmiHeader.biWidth *
            bmi.bmiHeader.biBitCount) + 31) >> 5 << 2);

        BITMAPFILEHEADER bmfh = 
        {
            WORD('B') + (WORD('M') << 8),
            nHeaderSize + nPixelsSize,
            0,
            0,
            nHeaderSize
        };

        fwrite(&bmfh, sizeof(BITMAPFILEHEADER), 1, fp);
        fwrite(&bmi.bmiHeader, sizeof(BITMAPINFOHEADER), 1, fp);
        fwrite(pPixels, nPixelsSize, 1, fp);

        fclose(fp);
    }

    // Delete the bitmap
    DeleteObject(hbmp);

    // Delete the DC
    DeleteDC(hdcMemory);

    return;
}
