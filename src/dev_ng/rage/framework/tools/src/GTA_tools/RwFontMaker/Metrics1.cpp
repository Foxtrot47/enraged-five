//------------------------------------------------------------------------------
// $Workfile: Metrics1.cpp $
//   Created: Gottfried Chen
// Copyright: 2003, rockstar vienna
// $Revision: $
//   $Author: $
//     $Date: $
//------------------------------------------------------------------------------
// Description: 
//------------------------------------------------------------------------------

#include "stdafx.h"
#include "Metrics1.h"

#include <assert.h>
#include <iomanip>



//------------------------------------------------------------------------------    
void Metrics1::writeHeader(const char* texture, const char* mask)
//------------------------------------------------------------------------------
{
    assert(texture != 0);
    if (mStream)
    {
        mStream << "METRICS1" << std::endl;
        mStream << texture;
        if (mask)
        {
            mStream << ' ' << mask;
        }
        mStream << std::endl;

        // Write the baseline
        mStream << 5 << std::endl;
        mHeaderWritten = true;
    }
}

//------------------------------------------------------------------------------
void Metrics1::writeCharacter(unsigned short characterCode,
                              int left, int top, int right, int bottom)
//------------------------------------------------------------------------------
{
    assert(mHeaderWritten);
    if (mStream && mHeaderWritten)
    {
        mStream << std::setw(5) << characterCode << ' '
                << std::setw(4) << left << ' '
                << std::setw(4) << top << ' '
                << std::setw(4) << right << ' '
                << std::setw(4) << bottom << std::endl;
    }
}
                        
