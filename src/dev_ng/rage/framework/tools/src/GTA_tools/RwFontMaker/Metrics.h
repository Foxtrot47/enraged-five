//------------------------------------------------------------------------------
// $Workfile: Metrics.h $
//   Created: Gottfried Chen
// Copyright: 2003, rockstar vienna
// $Revision: $
//   $Author: $
//     $Date: $
//------------------------------------------------------------------------------
// Description: 
//------------------------------------------------------------------------------

#ifndef METRICS_H
#define METRICS_H

#include <assert.h>

class Metrics
{
public:
    Metrics(const char* path);

    virtual
    void writeHeader(const char* texture, const char* mask = 0) = 0;

    virtual
    void writeCharacter(unsigned short character, int, int, int, int) = 0;

protected:
    std::ofstream mStream;
    bool mHeaderWritten;
};

inline
//------------------------------------------------------------------------------
Metrics::Metrics(const char* path) :
//------------------------------------------------------------------------------
mStream(path, std::ios_base::binary),
mHeaderWritten(false)
{
    assert(mStream != 0);
}

#endif // METRICS_H
