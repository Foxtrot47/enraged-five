// RwFontMakerDlg.h : Headerdatei
//

#pragma once


// CRwFontMakerDlg Dialogfeld
class CRwFontMakerDlg : public CDialog
{
// Konstruktion
public:
	CRwFontMakerDlg(CWnd* pParent = NULL);	// Standardkonstruktor

// Dialogfelddaten
	enum { IDD = IDD_RWFONTMAKER_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV-Unterst�tzung


// Implementierung
protected:
	HICON m_hIcon;

	// Generierte Funktionen f�r die Meldungstabellen
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	
	DECLARE_MESSAGE_MAP()
private:
    CString mFontName;
    CString mTextFile;
    int mFontSize;
    int mWidth1;
    int mHeight1;
    int mWidth2;
    int mHeight2;
    int mWeight;
    int mMetrics;
    char mWorkingDir[_MAX_PATH];

public:
    afx_msg void OnBnClickedMakefont();
    afx_msg void OnBnClickedBrowsetextfile();
    
   	void CountThisCharacter(wchar_t character);
	void ParseTheTextFile(const char* fileName);
};
