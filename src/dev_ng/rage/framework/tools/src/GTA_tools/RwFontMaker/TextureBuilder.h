//------------------------------------------------------------------------------
// $Workfile: TextureBuilder.h $
//   Created: Gottfried Chen
// Copyright: 2003, rockstar vienna
// $Revision: $
//   $Author: $
//     $Date: $
//------------------------------------------------------------------------------
// Description: 
//------------------------------------------------------------------------------

#ifndef TEXTURE_BUILDER_H
#define TEXTURE_BUILDER_H

#pragma once

#include <tchar.h>

struct FontOptions;

// Generates a texture and metrics file from <string> and <options>
void ProcessFont(FontOptions* options, wchar_t* string);

#endif // TEXTURE_BUILDER