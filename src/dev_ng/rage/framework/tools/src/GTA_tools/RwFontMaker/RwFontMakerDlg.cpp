// RwFontMakerDlg.cpp : Implementierungsdatei
//

#include "stdafx.h"
#include "RwFontMaker.h"
#include "RwFontMakerDlg.h"

//	#include "UsedCharactersFilter.h"
#include "TextureBuilder.h"
#include "FontOptions.h"

#include <direct.h>
#include <fstream>
#include <assert.h>

/*
#ifdef _DEBUG
#define new DEBUG_NEW
#endif
*/

unsigned short NumberOfOccurrences[65535];
unsigned short CharacterCode[65535];

bool bBetweenTilde;

// CAboutDlg-Dialogfeld f�r Anwendungsbefehl 'Info'
/*
class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialogfelddaten
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV-Unterst�tzung

// Implementierung
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()
*/
// CRwFontMakerDlg Dialogfeld



CRwFontMakerDlg::CRwFontMakerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRwFontMakerDlg::IDD, pParent)
    , mFontName(_T("Arial"))
    , mTextFile(_T(""))
    , mWidth1(256)
    , mHeight1(256)
    , mWidth2(0)
    , mHeight2(0)
    , mFontSize(10)
    , mWeight(400)
    , mMetrics(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
    _getcwd(mWorkingDir, _MAX_PATH);
}

void CRwFontMakerDlg::DoDataExchange(CDataExchange* pDX)
{
    CDialog::DoDataExchange(pDX);
    DDX_Text(pDX, IDC_FONTNAME, mFontName);
    DDX_Text(pDX, IDC_TEXTFILE, mTextFile);
    DDX_Text(pDX, IDC_WIDTH1, mWidth1);
    DDX_Text(pDX, IDC_HEIGHT1, mHeight1);
    DDX_Text(pDX, IDC_WIDTH2, mWidth2);
    DDX_Text(pDX, IDC_HEIGHT2, mHeight2);
    DDX_Text(pDX, IDC_FONTSIZE, mFontSize);
    DDV_MinMaxInt(pDX, mFontSize, 2, 100);
    DDV_MinMaxInt(pDX, mWidth1, 0, 2048);
    DDV_MinMaxInt(pDX, mHeight1, 0, 2048);
    DDV_MinMaxInt(pDX, mWidth2, 0, 2048);
    DDV_MinMaxInt(pDX, mHeight2, 0, 2048);
    DDX_Text(pDX, IDC_WEIGHT, mWeight);
    DDV_MinMaxInt(pDX, mWeight, 100, 900);
    DDX_Radio(pDX, IDC_METRIC1, mMetrics);
}

BEGIN_MESSAGE_MAP(CRwFontMakerDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
    ON_BN_CLICKED(IDMAKEFONT, OnBnClickedMakefont)
    ON_BN_CLICKED(IDC_BROWSETEXTFILE, OnBnClickedBrowsetextfile)
END_MESSAGE_MAP()

// CRwFontMakerDlg Meldungshandler

BOOL CRwFontMakerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Hinzuf�gen des Men�befehls "Info..." zum Systemmen�.

	// IDM_ABOUTBOX muss sich im Bereich der Systembefehle befinden.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Symbol f�r dieses Dialogfeld festlegen. Wird automatisch erledigt
	//  wenn das Hauptfenster der Anwendung kein Dialogfeld ist
	SetIcon(m_hIcon, TRUE);			// Gro�es Symbol verwenden
	SetIcon(m_hIcon, FALSE);		// Kleines Symbol verwenden

	// TODO: Hier zus�tzliche Initialisierung einf�gen
	
	return TRUE;  // Geben Sie TRUE zur�ck, au�er ein Steuerelement soll den Fokus erhalten
}

void CRwFontMakerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
//		CAboutDlg dlgAbout;
//		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Wenn Sie dem Dialogfeld eine Schaltfl�che "Minimieren" hinzuf�gen, ben�tigen Sie 
//  den nachstehenden Code, um das Symbol zu zeichnen. F�r MFC-Anwendungen, die das 
//  Dokument/Ansicht-Modell verwenden, wird dies automatisch ausgef�hrt.

void CRwFontMakerDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Ger�tekontext zum Zeichnen

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Symbol in Clientrechteck zentrieren
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Symbol zeichnen
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// Die System ruft diese Funktion auf, um den Cursor abzufragen, der angezeigt wird, w�hrend der Benutzer
//  das minimierte Fenster mit der Maus zieht.
HCURSOR CRwFontMakerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

static inline 
int bitcount (unsigned int n)  
{
    int count=0;    
    while (n)
    {
        count += n & 0x1u ;
        n >>= 1 ;
    }
    return count ;
}

bool white_space(wchar_t c)
{
	return ( (c==' ') || (c=='\t') || (c=='\x0A') || (c=='\x0D') );
}

void CRwFontMakerDlg::CountThisCharacter(wchar_t character)
{
    if (character < 65535)
    {
		if (character == '~')
		{
			if (bBetweenTilde == false)
			{
				bBetweenTilde = true;
			}
			else
			{
				bBetweenTilde = false;
			}
		}
		else
		{
			if (bBetweenTilde == false)
			{
				//	Convert TrueType characters into normal ASCII ones
				//	~ 65374 and / 65295 are not converted as the ASCII characters are used for other things
				if ((character >= 65281) && (character <= 65373))
				{
					if (character != 65295)
					{
						character -= 65248;
					}
				}

		    	NumberOfOccurrences[character]++;
	    		CharacterCode[character] = character;
	    	}
    	}
    }
    else
    {	//	probably won't ever reach here
    	MessageBox("Character has code greater than 65535");
    }
}

void CRwFontMakerDlg::ParseTheTextFile(const char* fileName)
{
	unsigned long loop, loop2;
	wchar_t character;
    enum {WHITE_SPACE, HEADER, TEXT, WHITE_SPACE_INTERNAL, FIRST_WHITE_SPACE, COMMENT_WHITE_SPACE, COMMENT_FIRST_WHITE_SPACE, COMMENT_WHITE_SPACE_INTERNAL, COMMENT_TEXT} state = FIRST_WHITE_SPACE;
	unsigned char start_key[50];	//	needs to be big enough to hold :mission name too?
	unsigned char start_key_index = 0;

    std::ifstream file(fileName, std::ios_base::binary);
    bool ok = (file != 0);
//    mCharacters.clear();
//    if (sizeof(Char) == 2 && ok)
	if (ok)
    {
        unsigned short unicodeHeader;
        file.read((char*)&unicodeHeader, 2);
        assert(unicodeHeader == 0xfeff);
        ok = unicodeHeader == 0xfeff;
    }

	for (loop = 0; loop < 65535; loop++)
	{
		NumberOfOccurrences[loop] = 0;
		CharacterCode[loop] = 0;
	}

//	Ensure that all the common ASCII characters are present
	for (loop = 32; loop < 128; loop++)
	{
		NumberOfOccurrences[loop] = 1;
		CharacterCode[loop] = loop;
	}

	bBetweenTilde = false;

    if (ok)
    {
        while(file)
        {
//            Char character;
//          file.read((char*)&character, sizeof(Char));
//			wchar_t character;
			file.read((char*)&character, sizeof(wchar_t));
			
//            if (character != '\t' && character != ' ' &&
//                character != '\r' && character != '\n')
//            {
		//                mCharacters.insert(character);
//                if (character < 65535)
//                {
//                	NumberOfOccurrences[character]++;
//                	CharacterCode[character] = character;
//                }
//                else
//                {
//                	stop_var = 12;
//                }
//            }

	        switch (state)
	        {
				case COMMENT_FIRST_WHITE_SPACE:
					if ( character=='{' )
					{
						MessageBox("Nested Comments");
					}
					else if ( character=='}' )
					{
						state = FIRST_WHITE_SPACE;
					}
					break;

				case COMMENT_WHITE_SPACE:
					if ( character=='{' )
					{
						MessageBox("Nested Comments");
					}
					else if ( character=='}' )
					{
						state = WHITE_SPACE;
					}
					break;

				case COMMENT_WHITE_SPACE_INTERNAL:
					if ( character=='{' )
					{
						MessageBox("Nested Comments");
					}
					else if ( character=='}' )
					{
						state = WHITE_SPACE_INTERNAL;
					}
					break;

				case COMMENT_TEXT:
					if ( character=='{' )
					{
						MessageBox("Nested Comments");
					}
					else if ( character=='}' )
					{
						state = TEXT;
					}
					break;

	            case FIRST_WHITE_SPACE:
	                if ( character=='[' )
	                {
						state = HEADER;
						start_key[0] = 0;
						start_key_index = 0;
	                }
					else if ( character=='{')
						state = COMMENT_FIRST_WHITE_SPACE;
					else if ( character=='}' )
					{
						MessageBox("Unmatched Bracket");
					}
	                break;
	            
	            case WHITE_SPACE:
	                if ( character=='[' )
	                {
						state = HEADER;
				        start_key[0] = 0;
				        start_key_index = 0;
	                }
					else if ( character=='{')
						state = COMMENT_WHITE_SPACE;
					else if ( character=='}' )
					{
						MessageBox("Unmatched Bracket");
					}
	                else if (!white_space ( character ))
	                {
	                    state = TEXT;
						CountThisCharacter(character);
	                }
	                break;
	            
	            case WHITE_SPACE_INTERNAL:
	                if ( character=='[' )
	                {
						state = HEADER;
				        start_key[0] = 0;
				        start_key_index = 0;
	                }
					else if ( character=='{')
						state = COMMENT_WHITE_SPACE_INTERNAL;
					else if ( character=='}' )
					{
						MessageBox("Unmatched Bracket");
					}
	                else if (!white_space ( character ))
	                {
	                    state = TEXT;
						CountThisCharacter(character);
	                }
	                break;
	            
	            case HEADER:
	                if ( character==']' )
	                {
				        start_key[start_key_index] = 0;
				        start_key_index++;
	                    state = WHITE_SPACE;
	                }
					else if ( character=='}' )
					{
						MessageBox("Unmatched Bracket");
					}
					else
					{
				        start_key[start_key_index] = character;
				        start_key_index++;
				        start_key[start_key_index] = 0;
					}
	                break;
	                    
	            case TEXT:
	                if (white_space(character))
					{
						state = WHITE_SPACE_INTERNAL;
					}
					else if ( character=='{')
					{
						state = COMMENT_TEXT;
					}
					else if ( character=='}' )
					{
						MessageBox("Unmatched Bracket");
					}
	                else
	                {
	                    if ( character=='[' )
	                    	MessageBox("No Whitespace Before Header");

						CountThisCharacter(character);
	                }

	                break;                                                                                          

	            default:
					break;
	        }
	    }
	    if ( state == HEADER )
	    {
	    	MessageBox("Incomplete Header at end of file");
	    }
		else if ( ( state == COMMENT_FIRST_WHITE_SPACE ) ||
				  ( state == COMMENT_WHITE_SPACE ) ||
				  ( state == COMMENT_WHITE_SPACE_INTERNAL ) ||
				  ( state == COMMENT_TEXT ) )
		{
			MessageBox("Still in a comment at end of file");
		}

    }
}

size_t GetNumberOfCharactersUsed(void)
{
	unsigned long loop;
	size_t NumberOfUsedCharacters;
	
	NumberOfUsedCharacters = 0;
	
	for (loop = 0; loop < 65535; loop++)
	{
		if (NumberOfOccurrences[loop] > 0)
		{
			NumberOfUsedCharacters++;
		}
	}
	
	return NumberOfUsedCharacters;
}

void FillCharacterString(wchar_t* string)
{
	unsigned long loop;

    for (loop = 0; loop < 65535; loop++)
    {
		if (NumberOfOccurrences[loop] > 0)
		{
			*string = (wchar_t) CharacterCode[loop];
			++string;
		}
	}
}

void OutputCharacterCountFile(void)
{
	unsigned short temp_val, line_number;
	FILE *pCountFile;
	unsigned long loop, loop2;

    for (loop = 0; loop < 65535; loop++)
    {
    	for (loop2 = (loop + 1); loop2 < 65535; loop2++)
    	{
    		if (NumberOfOccurrences[loop2] > NumberOfOccurrences[loop])
    		{
    			temp_val = NumberOfOccurrences[loop];
    			NumberOfOccurrences[loop] = NumberOfOccurrences[loop2];
    			NumberOfOccurrences[loop2] = temp_val;
    			temp_val = CharacterCode[loop];
    			CharacterCode[loop] = CharacterCode[loop2];
    			CharacterCode[loop2] = temp_val;
    		}
    	}
    }

	line_number = 1;
    
    pCountFile = fopen("kanji_counter.txt", "w");
    
    if (pCountFile)
    {
	    for (loop = 0; loop < 65535; loop++)
	    {
			if (NumberOfOccurrences[loop] > 0)
			{
//	#pragma wchar_type on
//				fprintf(pCountFile, "%d\t%c\t%d\t\t%d\n", line_number, CharacterCode[loop], CharacterCode[loop], NumberOfOccurrences[loop]);
//	should I switch wide characters off here?
//				fprintf(pCountFile, "%d\t\t%d\n", CharacterCode[loop], NumberOfOccurrences[loop]);
				fprintf(pCountFile, "%d\t%d\t\t%d\n", line_number, CharacterCode[loop], NumberOfOccurrences[loop]);
				line_number++;
			}
	    }
	    
	    fclose(pCountFile);
	}
}


void CRwFontMakerDlg::OnBnClickedMakefont()
{
    UpdateData(TRUE);
    
    if (bitcount(mWidth1)>1 || bitcount(mHeight1)>1 ||
        bitcount(mWidth2)>1 || bitcount(mHeight2)>1)
    {
        MessageBox("Texture dimensions must be a power of 2.", "Error");
        return;
    }

    if (mFontName == "")
    {
        MessageBox("No font name provided.", "Error");
        return;
    }

    if (mTextFile == "")
    {
        MessageBox("No text file provided.", "Error");
        return;
    }

    FontOptions options;
    options.mBorder = mMetrics == 0 ? 1 : 2;
    options.mFontName = mFontName.GetBuffer(0);
    options.mFontSize = mFontSize;
    options.mHeight1 = mHeight1;
    options.mHeight2 = mHeight2;
    options.mMetrics = mMetrics;
    options.mSupersample = 1;
    options.mTextFile = mTextFile.GetBuffer(0);
    options.mWidth1 = mWidth1;
    options.mWidth2 = mWidth2;
    options.mWeight = mWeight;
			/*
			    UsedCharactersFilter<wchar_t> characterFilter;
			    characterFilter.parse(options.mTextFile);
			    size_t count = characterFilter.getCharacterCount();
			    wchar_t* string = new wchar_t[count+1];
			    characterFilter.fillCharacterString(string);
			*/    
	ParseTheTextFile(options.mTextFile);
	size_t count = GetNumberOfCharactersUsed();
	wchar_t *string = new wchar_t[count+1];
	FillCharacterString(string);
    string[count] = 0;
    _chdir(mWorkingDir);
    ProcessFont(&options, string);
    
    delete[] string;

//	OutputCharacterCountFile();

    MessageBox("Done");
}

void CRwFontMakerDlg::OnBnClickedBrowsetextfile()
{
    UpdateData(TRUE);
    CFileDialog dialog(TRUE);
    if (IDOK == dialog.DoModal())
    {
        mTextFile = dialog.GetPathName();
        UpdateData(FALSE);
    }
}
