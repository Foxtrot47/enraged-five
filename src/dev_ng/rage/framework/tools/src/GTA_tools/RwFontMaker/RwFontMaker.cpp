// RwFontMaker.cpp : Definiert das Klassenverhalten f�r die Anwendung.
//

#include "stdafx.h"
#include "RwFontMaker.h"
#include "RwFontMakerDlg.h"

/*
#ifdef _DEBUG
#define new DEBUG_NEW
#endif
*/


// CRwFontMakerApp
/*
BEGIN_MESSAGE_MAP(CRwFontMakerApp, CWinApp)
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()
*/
// CRwFontMakerApp-Erstellung

CRwFontMakerApp::CRwFontMakerApp()
{
	// TODO: Hier Code zur Konstruktion einf�gen
	// Alle wichtigen Initialisierungen in InitInstance positionieren
}


// Das einzige CRwFontMakerApp-Objekt

CRwFontMakerApp theApp;

// CRwFontMakerApp Initialisierung

BOOL CRwFontMakerApp::InitInstance()
{
	// InitCommonControls() ist f�r Windows XP erforderlich, wenn ein Anwendungsmanifest
	// die Verwendung von ComCtl32.dll Version 6 oder h�her zum Aktivieren
	// von visuellen Stilen angibt. Ansonsten treten beim Erstellen von Fenstern Fehler auf.
//	InitCommonControls();

//	CWinApp::InitInstance();

//	AfxEnableControlContainer();

//	CRwFontMakerDlg dlg;
//	m_pMainWnd = &dlg;
	m_pMainWnd = new CRwFontMakerDlg;
	INT_PTR nResponse = ((CRwFontMakerDlg *) m_pMainWnd)->DoModal();
	if (nResponse == IDOK)
	{
		// TODO: F�gen Sie hier Code ein, um das Schlie�en des
		//  Dialogfelds �ber OK zu steuern
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: F�gen Sie hier Code ein, um das Schlie�en des
		//  Dialogfelds �ber "Abbrechen" zu steuern
	}
	
	// Da das Dialogfeld geschlossen wurde, FALSE zur�ckliefern, so dass wir die
	//  Anwendung verlassen, anstatt das Nachrichtensystem der Anwendung zu starten.

	return FALSE;
//	return CWinApp::ExitInstance();
}
