//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by RwFontMaker.rc
//
#define IDR_MANIFEST                    1
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_RWFONTMAKER_DIALOG          102
#define IDR_MAINFRAME                   128
#define IDC_FONTNAME                    1000
#define IDC_FONTNAME_LABEL              1001
#define IDC_TEXTFILE                    1002
#define IDC_WIDTH1                      1003
#define IDC_HEIGHT1                     1004
#define IDC_WIDTH2                      1005
#define IDC_HEIGHT2                     1006
#define IDC_FONTWEIGHTNORMAL            1007
#define IDC_FONTWEIGHTBOLD              1008
#define IDC_METRIC1                     1010
#define IDC_METRIC2                     1011
#define IDC_FONTSIZE                    1012
#define IDC_FONTSIZE_LABEL              1013
#define IDMAKEFONT                      1014
#define IDC_TEXTFILE_LABEL              1015
#define IDC_WIDTH1_LABEL                1016
#define IDC_HEIGHT1_LABEL               1017
#define IDC_WIDTH2_LABEL                1018
#define IDC_HEIGHT2_LABEL               1019
#define IDC_BROWSETEXTFILE              1028
#define IDC_EDIT1                       1029
#define IDC_WEIGHT                      1029

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1030
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
