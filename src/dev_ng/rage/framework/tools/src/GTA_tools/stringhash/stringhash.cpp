/**
 * @brief stringhash.cpp
 * @desc RAGE atStringHash Command Line Util
 *
 * @author David Muir <david.muir@rockstarnorth.com>
 */

#include <stdlib.h>

#include "atl/map.h"
#include "system/main.h"
#include "system/param.h"
#include "string/stringhash.h"
using namespace rage;

/* Defines */
#define MAX_MESSAGE (512)

FPARAM( 1, message, "String to hash." );
PARAM( hex, "Format hash value as hexadecimal." );
PARAM( verbose, "Shout about what we are hashing." );
PARAM( short, "16-bit string hash." );
PARAM( ushort, "Unsigned 16-bit string hash." );

/* Entry */

int
Main( )
{
	if ( PARAM_message.Get() )
	{
		const char* pMessage = NULL;
		PARAM_message.Get( pMessage );

		if ( PARAM_verbose.Get() )
			printf( "Message: %s\n", pMessage );

		// Hash Algorithm Options
		u32 hash = 0x00000000;
		if ( PARAM_short.Get() )
		{
			// atHash16
			hash = static_cast<u32>( atHash16( pMessage ) );
		}
		else if ( PARAM_ushort.Get() )
		{
			// atHash16U
			hash = static_cast<u32>( atHash16U( pMessage ) );
		}
		else
		{
			// atStringHash (default)
			hash = atStringHash( pMessage );
		}

		if ( PARAM_hex.Get() )
			printf( "0x%.8X\n", hash );
		else
			printf( "%u\n", hash );

		return ( EXIT_SUCCESS );
	}

	return ( EXIT_FAILURE );
}
