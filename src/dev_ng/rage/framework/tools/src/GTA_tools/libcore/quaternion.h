// Title	:	Quaternion.h
// Author	:	Aaron Puzey
// Started	:	07/07/99

#ifndef _LIBCORE_QUATERNION_H_
#define _LIBCORE_QUATERNION_H_

// Game headers
#include "libcore/maths.h"
#include "libcore/matrix.h"

// Use this structure to read quaternion data from non-aligned memory

class CQuaternion
{
public:
	float x, y, z, w;

public:
	CQuaternion() {;}
//	CQuaternion(const RtQuat& q) {x = q.imag.x; y = q.imag.y; z = q.imag.z; w = q.real;}
	CQuaternion(const CMatrix& mat) { Set(mat); }
	CQuaternion(float rx, float ry, float rz) { Set(rx,ry,rz); }
	CQuaternion(float ax, float ay, float az, float aw) { Set(ax,ay,az,aw); }
	explicit CQuaternion(const CVector& vecOrientation) { Set(vecOrientation.x, vecOrientation.y, vecOrientation.z); }
	CQuaternion(const CQuaternion& quat) { Copy(quat); }
	~CQuaternion() {}

	void Set(const CMatrix& mat);
	void Set(float ax, float ay, float az);
	void Set(float ax, float ay, float az, float aw);
	void Set(const CVector& vec) { Set(x, y, z); }
	void Set(CVector* pvec, float fRotation);
	void Get(CMatrix* pmat) const;
	void Get(float* pax, float* pay, float* paz) const;
	void Get(CVector* pvec) const { Get(&pvec->x, &pvec->y, &pvec->z); }
	void Get(CVector* pvec, float* fRotation) const;

	float GetLengthSquared();

	void Invert();
	void Add(const CQuaternion& quat1, const CQuaternion& quat2);
	void Subtract(const CQuaternion& quat1, const CQuaternion& quat2);
	void Multiply(const CQuaternion& quat1, const CQuaternion& quat2);
	void Normalise();
	void Scale(float f);
	void Copy(const CQuaternion& quat);

	void Slerp(const CQuaternion& quatStart, const CQuaternion& quatEnd, float fInterpValue);
	void Slerp(const CQuaternion& quatStart, const CQuaternion& quatEnd, float fTheta, float fOOSinTheta, float fInterpValue);

	CQuaternion& operator=(const CQuaternion& q);
	CQuaternion& operator+=(const CQuaternion& q);
	CQuaternion& operator-=(const CQuaternion& q);
	CQuaternion& operator*=(const float f);
	CQuaternion& operator/=(const float f);

	friend CQuaternion operator-(const CQuaternion& q);
	friend CQuaternion operator+(const CQuaternion& q1, const CQuaternion& q2);
	friend CQuaternion operator-(const CQuaternion& q1, const CQuaternion& q2);
	friend CQuaternion operator*(const float f, const CQuaternion& v);
	friend CQuaternion operator*(const CQuaternion& q, const float f) { return (f * q); }
	friend CQuaternion operator/(const CQuaternion& q, const float f) { return (q * (1.0f / f)); }

	friend float DotProduct(const CQuaternion& quat1, const CQuaternion& quat2);
	friend void InitSlerp(const CQuaternion& quatStart, const CQuaternion& pquatEnd, float *pTheta, float *pOOSinTheta);
};


inline void CQuaternion::Set(float ax, float ay, float az, float aw)
{
	x = ax;
	y = ay;
	z = az;
	w = aw;
}

#ifdef GTA_PS2
inline asm void CQuaternion::Copy(const CQuaternion& quat)
{
	lq  t0,0(a1)
	sq	t0,0(a0)
	
	jr		ra
	nop
}

inline asm void CQuaternion::Add(const CQuaternion& quat1, const CQuaternion& quat2)
{
	// load vectors into VU0
	lqc2	vf01,0(a1)
	lqc2	vf02,0(a2)
	// add vectors and store
	vadd.xyzw	vf01,vf01,vf02
	sqc2	vf01,0(a0)

	jr		ra
	nop
}

inline asm void CQuaternion::Subtract(const CQuaternion& quat1, const CQuaternion& quat2)
{
	// load vectors into VU0
	lqc2	vf01,0(a1)
	lqc2	vf02,0(a2)
	// subtract vectors and store
	vsub.xyzw	vf01,vf01,vf02
	sqc2	vf01,0(a0)

	jr		ra
	nop
}

inline asm void CQuaternion::Scale(float f)
{
	// load vector into Vu0
	lqc2	vf01,0(a0)
	// move scale from FPU to CPU to VU0
	mfc1	t0,$f12
	qmtc2	t0,vf02
	// multiple vector by scale and store
	vmulx.xyzw	vf01,vf01,vf02x
	sqc2	vf01,0(a0)

	jr		ra
	nop
}

inline asm void CQuaternion::Invert()
{
	// load quaternion into VU0
	lqc2	vf01,0(a0)
	// subtract first three elements from (0,0,0) and store
	vsub.xyz	vf01,vf0,vf01
	sqc2		vf01,0(a0)

	jr		ra
	qmove	v0,a0
}

inline asm float CQuaternion::GetLengthSquared()
{
	// load elements of quaternion
	lwc1	$f1,0(a0)
	lwc1	$f2,4(a0)
	lwc1	$f3,8(a0)
	lwc1	$f4,12(a0)
	// multiple and add elements in accumulator
	mula.s	$f1,$f1
	madda.s	$f2,$f2
	madda.s	$f3,$f3
	madd.s	$f0,$f4,$f4
	
	jr		ra
	nop
}

inline void CQuaternion::Normalise()
{
	float det = CMaths::RecipSqrt(GetLengthSquared());
	Scale(det);
}

inline asm CQuaternion& CQuaternion::operator+=(const CQuaternion& q)
{
	// load quaternions into VU0
	lqc2	vf01,0(a0)
	lqc2	vf02,0(a1)
	// add quaternions and store
	vadd.xyzw	vf01,vf01,vf02
	sqc2	vf01,0(a0)

	jr		ra
	qmove	v0,a0
}

inline asm CQuaternion& CQuaternion::operator-=(const CQuaternion& q)
{
	// load quaternions into VU0
	lqc2	vf01,0(a0)
	lqc2	vf02,0(a1)
	// add quaternions and store
	vsub.xyzw	vf01,vf01,vf02
	sqc2	vf01,0(a0)

	jr		ra
	qmove	v0,a0
}

inline asm CQuaternion& CQuaternion::operator*=(float f)
{
	// load quaternion into Vu0
	lqc2	vf01,0(a0)
	// move scale from FPU to CPU to VU0
	mfc1	t0,$f12
	qmtc2	t0,vf02
	// multiple quaternion by scale and store
	vmulx.xyzw	vf01,vf01,vf02x
	sqc2	vf01,0(a0)

	jr		ra
	qmove	v0,a0
}

inline CQuaternion& CQuaternion::operator/=(float f)
{
	Scale(1.0f / f);

	return *this;
}

// --- friend operators ---------------------------------------------------------

inline asm CQuaternion operator-(const CQuaternion& q1)
{
	// load quaternion into VU0
	lqc2	vf01,0(a1)
	// create (0,0,0,0) in accumulator and subtract quaternion from accumulator
	vmulax.xyzw	ACC,vf0,vf0x
	vmsubw.xyzw	vf01,vf01,vf0w
	sqc2	vf01,0(a0)
	
	jr		ra
	qmove	v0,a0
}

inline asm CQuaternion operator+(const CQuaternion& q1, const CQuaternion& q2)
{
	// load quaternions into VU0
	lqc2	vf01,0(a1)
	lqc2	vf02,0(a2)
	// add quaternions and store
	vadd.xyzw	vf01,vf01,vf02
	sqc2	vf01,0(a0)

	jr		ra
	qmove	v0,a0
}

inline asm CQuaternion operator-(const CQuaternion& q1, const CQuaternion& q2)
{
	// load quaternions into VU0
	lqc2	vf01,0(a1)
	lqc2	vf02,0(a2)
	// add quaternions and store
	vsub.xyzw	vf01,vf01,vf02
	sqc2	vf01,0(a0)

	jr		ra
	qmove	v0,a0
}

inline asm CQuaternion operator*(const float f, const CQuaternion& q)
{
	// load quaternion into Vu0
	lqc2	vf01,0(a1)
	// move scale from FPU to CPU to VU0
	mfc1	t0,$f12
	qmtc2	t0,vf02
	// multiple quaternion by scale and store
	vmulx.xyzw	vf01,vf01,vf02x
	sqc2	vf01,0(a0)

	jr		ra
	qmove	v0,a0
}

inline asm float DotProduct(const CQuaternion& quat1, const CQuaternion& quat2)
{
	// load elements of quaternions
	lwc1	$f1,0(a0)
	lwc1	$f2,4(a0)
	lwc1	$f3,8(a0)
	lwc1	$f4,12(a0)
	lwc1	$f5,0(a1)
	lwc1	$f6,4(a1)
	lwc1	$f7,8(a1)
	lwc1	$f8,12(a1)
	// multiple and add elements in the accumulator
	mula.s	$f1,$f5
	madda.s	$f2,$f6
	madda.s	$f3,$f7
	madd.s	$f0,$f4,$f8
	
	jr		ra
	nop
}

#else
inline void CQuaternion::Copy(const CQuaternion& quat)
{
	x = quat.x;
	y = quat.y;
	z = quat.z;
	w = quat.w;
}

inline void CQuaternion::Add(const CQuaternion& quat1, const CQuaternion& quat2)
{
	x = quat1.x + quat2.x;
	y = quat1.y + quat2.y;
	z = quat1.z + quat2.z;
	w = quat1.w + quat2.w;
}

inline void CQuaternion::Subtract(const CQuaternion& quat1, const CQuaternion& quat2)
{
	x = quat1.x - quat2.x;
	y = quat1.y - quat2.y;
	z = quat1.z - quat2.z;
	w = quat1.w - quat2.w;
}

inline void CQuaternion::Scale(float f)
{
	x *= f;
	y *= f;
	z *= f;
	w *= f;
}

inline CQuaternion& CQuaternion::operator=(const CQuaternion& q)
{
	Copy(q);

	return *this;
}

inline void CQuaternion::Invert()
{
	x = -x;
	y = -y;
	z = -z;
}

inline float CQuaternion::GetLengthSquared()
{
	return (x*x + y*y + z*z + w*w);
}

inline void CQuaternion::Normalise()
{
	float lenSqr = GetLengthSquared();

	if(lenSqr == 0.0f)
	{
		w = 1.0f;
		return;		
	}
	
	float det = CMaths::RecipSqrt(GetLengthSquared());
	x *= det;
	y *= det;
	z *= det;
	w *= det;
}

inline CQuaternion& CQuaternion::operator+=(const CQuaternion& q)
{
	x += q.x;
	y += q.y;
	z += q.z;
	w += q.w;

	return *this;
}

inline CQuaternion& CQuaternion::operator-=(const CQuaternion& q)
{
	x -= q.x;
	y -= q.y;
	z -= q.z;
	w -= q.w;

	return *this;
}

inline CQuaternion& CQuaternion::operator*=(float f)
{
	Scale(f);

	return *this;
}

inline CQuaternion& CQuaternion::operator/=(float f)
{
	Scale(1.0f / f);

	return *this;
}

inline CQuaternion operator+(const CQuaternion& q1, const CQuaternion& q2)
{
	CQuaternion quatTemp;
	quatTemp.Add(q1, q2);

	return quatTemp;
}

inline CQuaternion operator-(const CQuaternion& q1, const CQuaternion& q2)
{
	CQuaternion quatTemp;
	quatTemp.Subtract(q1, q2);

	return quatTemp;
}

inline CQuaternion operator*(const float f, const CQuaternion& q)
{
	CQuaternion quatTemp(q);
	quatTemp.Scale(f);

	return quatTemp;
}

inline float DotProduct(const CQuaternion& quat1, const CQuaternion& quat2)
{
	return quat1.x*quat2.x + quat1.y*quat2.y + quat1.z*quat2.z + quat1.w*quat2.w;
}

#endif

//
// name:		InitSlerp
// description:	Calculate theta and one/sin(theta) for a quaternion spherical slerp
inline void InitSlerp(const CQuaternion& quatStart, const CQuaternion& quatEnd, float *pTheta, float *pOOSinTheta)
{
	float fCosT = DotProduct(quatStart, quatEnd);

	
/*	if (fCosT < 0.0f)	// A bit of a fudge but better than crashing
	{
		*pOOSinTheta = 0.0f;
		return;
	}*/
//	ASSERT(fCosT >= 0.0f);
	if(fCosT > 1.0f)
		fCosT = 1.0f;

	*pTheta = CMaths::ACos(fCosT);

	if(*pTheta != 0.0f)
		*pOOSinTheta = 1.0f / CMaths::Sin(*pTheta);
	else
		*pOOSinTheta = 0.0f;
}

#endif	//_LIBCORE_QUATERNION_H_

