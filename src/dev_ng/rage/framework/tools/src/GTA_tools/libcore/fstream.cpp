#include "libcore/fstream.h"

using namespace WinLib;

static CreateStream g_cbCreateStream(NULL);

void FileStream::SetCreateStream(CreateStream cbCreateStream) { g_cbCreateStream = cbCreateStream; }

WinLib::FileStream* FileStream::m_cbCreateStream()
{
	if(!g_cbCreateStream)
		return NULL;

	return g_cbCreateStream();
}