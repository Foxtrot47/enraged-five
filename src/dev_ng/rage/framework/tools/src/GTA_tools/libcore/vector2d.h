// Title	:	Vector2D.h
// Author	:	G. Speirs
// Started	:	07/10/99

#ifndef _VECTOR2D_H_
#define _VECTOR2D_H_

#include "libcore/Maths.h"
#include "libcore/Vector.h"

class CVector2D
{
public:
	float x, y;

public:
	CVector2D(){}
	CVector2D(float x, float y) { this->x = x; this->y = y; }
	CVector2D(const CVector& v) {x = v.x; y = v.y;}

	void Normalise();

	float Magnitude() const { return CMaths::Sqrt(x * x + y * y); }
	float MagnitudeSqr() const { return (x * x + y * y); }

	static CVector2D ConvertTo2D(const CVector& v) { return CVector2D(v.x, v.y); }

	friend float DotProduct(const CVector2D& v1, const CVector2D& v2) { return (v1.x * v2.x + v1.y * v2.y); }

	CVector2D& operator=(const CVector2D& v);
	CVector2D& operator=(const CVector& v);
	CVector2D& operator+=(const CVector2D& v);
	CVector2D& operator-=(const CVector2D& v);
	CVector2D& operator*=(float f);
	CVector2D& operator/=(float f);

	friend CVector2D operator-(const CVector2D& v) { return CVector2D(-v.x, -v.y); }
	friend CVector2D operator+(const CVector2D& v1, const CVector2D& v2) { return CVector2D(v1.x + v2.x, v1.y + v2.y); }
	friend CVector2D operator-(const CVector2D& v1, const CVector2D& v2) { return CVector2D(v1.x - v2.x, v1.y - v2.y); }
	friend CVector2D operator*(float f, const CVector2D& v) { return CVector2D(f * v.x, f * v.y); }
	friend CVector2D operator*(const CVector2D& v, float f) { return (f * v); }
	friend CVector2D operator/(const CVector2D& v, float f) { return (v * (1.0f / f)); }

	friend bool operator==(const CVector2D& v1, const CVector2D& v2) { return ((v1.x == v2.x) && (v1.y == v2.y)); }
	friend bool operator!=(const CVector2D& v1, const CVector2D& v2) { return !(v1 == v2); }

	float& operator[](int iIndex) { return (iIndex == 0) ? x : y; }

};


inline void CVector2D::Normalise()
{
	float MagSqr = MagnitudeSqr();
	if(MagSqr <= 0.0f)
	{
		x = 1.0f;
	//	ASSERT(MagnitudeSqr() > 0.0f);
		return;
	}

	*this *= CMaths::RecipSqrt(MagSqr);
}

inline CVector2D& CVector2D::operator=(const CVector2D& v)
{
	x = v.x;
	y = v.y;

	return *this;
}

inline CVector2D& CVector2D::operator=(const CVector& v)
{
	x = v.x;
	y = v.y;

	return *this;
}

inline CVector2D& CVector2D::operator+=(const CVector2D& v)
{
	x += v.x;
	y += v.y;

	return *this;
}

inline CVector2D& CVector2D::operator-=(const CVector2D& v)
{
	x -= v.x;
	y -= v.y;

	return *this;
}

inline CVector2D& CVector2D::operator*=(float f)
{
	x *= f;
	y *= f;

	return *this;
}

inline CVector2D& CVector2D::operator/=(float f)
{
	float oof = 1.0f / f;
	x *= oof;
	y *= oof;

	return *this;
}

#endif
