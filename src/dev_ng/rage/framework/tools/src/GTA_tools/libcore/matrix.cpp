// Title	:	Matrix.cpp
// Author	:	Richard Jobling
// Started	:	19/10/98

// Game headers
#include "libcore/Matrix.h"
#include "libcore/Maths.h"
#include "libcore/Quaternion.h"

#include <string>

CMatrix::CMatrix(const CMatrix& m)
{
	*this = m;
}

//
//        name: CMatrix::~CMatrix
// description: Destructor
//
CMatrix::~CMatrix()
{
}

CMatrix& CMatrix::operator=(const CMatrix& m)
{
	memcpy(&xx, &m.xx, sizeof(float)*16);

	return *this;
}

void CMatrix::CopyOnlyMatrix(const CMatrix& m)
{
	memcpy(&xx, &m.xx, sizeof(float)*16);
}

CMatrix& CMatrix::operator+=(const CMatrix& m)
{
	xx += m.xx;
	xy += m.xy;
	xz += m.xz;
	yx += m.yx;
	yy += m.yy;
	yz += m.yz;
	zx += m.zx;
	zy += m.zy;
	zz += m.zz;
	tx += m.tx;
	ty += m.ty;
	tz += m.tz;
	return *this;
}

void CMatrix::SetUnity()
{
	xx = 1.0f; yx = 0.0f; zx = 0.0f;
	xy = 0.0f; yy = 1.0f; zy = 0.0f;
	xz = 0.0f; yz = 0.0f; zz = 1.0f;
	tx = 0.0f; ty = 0.0f; tz = 0.0f;
}

void CMatrix::ResetOrientation()
{
	xx = 1.0f; yx = 0.0f; zx = 0.0f;
	xy = 0.0f; yy = 1.0f; zy = 0.0f;
	xz = 0.0f; yz = 0.0f; zz = 1.0f;
}

void CMatrix::SetScale(float s)
{
	xx =    s; yx = 0.0f; zx = 0.0f;
	xy = 0.0f; yy =    s; zy = 0.0f;
	xz = 0.0f; yz = 0.0f; zz =    s;
	tx = 0.0f; ty = 0.0f; tz = 0.0f;
}

void CMatrix::SetScale(float sx, float sy, float sz)
{
	xx =   sx; yx = 0.0f; zx = 0.0f;
	xy = 0.0f; yy =   sy; zy = 0.0f;
	xz = 0.0f; yz = 0.0f; zz =   sz;
	tx = 0.0f; ty = 0.0f; tz = 0.0f;
}

void CMatrix::SetTranslate(float tx, float ty, float tz)
{
	xx = 1.0f; yx = 0.0f; zx = 0.0f;
	xy = 0.0f; yy = 1.0f; zy = 0.0f;
	xz = 0.0f; yz = 0.0f; zz = 1.0f;
	this->tx = tx; this->ty = ty; this->tz = tz; 
}

void CMatrix::SetTranslateOnly(float tx, float ty, float tz)
{
	this->tx = tx; this->ty = ty; this->tz = tz; 
}

void CMatrix::SetRotateXOnly(float a)
{
	float ca = CMaths::Cos(a);
	float sa = CMaths::Sin(a);

	xx = 1.0f; yx = 0.0f; zx = 0.0f;
	xy = 0.0f; yy =   ca; zy =   sa;
	xz = 0.0f; yz =  -sa; zz =   ca;
}

void CMatrix::SetRotateYOnly(float a)
{
 	float ca = CMaths::Cos(a);
	float sa = CMaths::Sin(a);

	xx =   ca; yx = 0.0f; zx =  -sa;
	xy = 0.0f; yy = 1.0f; zy = 0.0f;
	xz =   sa; yz = 0.0f; zz =   ca;
}

void CMatrix::SetRotateZOnly(float a)
{
	float ca = CMaths::Cos(a);
	float sa = CMaths::Sin(a);

	xx =   ca; yx =   sa; zx = 0.0f;
	xy =  -sa; yy =   ca; zy = 0.0f;
	xz = 0.0f; yz = 0.0f; zz = 1.0f;
}



void CMatrix::SetRotateX(float a)
{
	SetRotateXOnly(a);
	tx = 0.0f; ty = 0.0f; tz = 0.0f;
}

void CMatrix::SetRotateY(float a)
{
	SetRotateYOnly(a);
	tx = 0.0f; ty = 0.0f; tz = 0.0f;
}

void CMatrix::SetRotateZ(float a)
{
	SetRotateZOnly(a);
	tx = 0.0f; ty = 0.0f; tz = 0.0f;
}


void CMatrix::SetRotate(float ax, float ay, float az)
{
	// | cay  0  say  0 |   |  caz saz  0  0 |   | 1    0   0  0 |   | caz*cay-saz*sax*say saz*cay+caz*sax*say -cax*say  0 |
	// |   0  1    0  0 | . | -saz caz  0  0 | . | 0  cax sax  0 | = |            -saz*cax             caz*cax      sax  0 |
	// | say  0 -cay  0 |   |    0   0  1  0 |   | 0 -sax cax  0 |   | caz*say+saz*sax*cay saz*say-caz*sax*cay  cax*cay  0 |
	// |   0  0    0  1 |   |    0   0  0  1 |   | 0    0   0  1 |   |                   0                   0        0  1 |
	//
	// | cay  0  say  0 |   |      caz      saz   0  0 |   | caz*cay-saz*sax*say saz*cay+caz*sax*say -cax*say  0 |
	// |   0  1    0  0 | . | -saz*cax  caz*cax sax  0 | = |            -saz*cax             caz*cax      sax  0 |
	// | say  0 -cay  0 |   |  saz*sax -caz*sax cax  0 |   | caz*say+saz*sax*cay saz*say-caz*sax*cay  cax*cay  0 |
	// |   0  0    0  1 |   |        0        0   0  1 |   |                   0                   0        0  1 |

	float cax = CMaths::Cos(ax);
	float sax = CMaths::Sin(ax);
	float cay = CMaths::Cos(ay);
	float say = CMaths::Sin(ay);
	float caz = CMaths::Cos(az);
	float saz = CMaths::Sin(az);

	float saz_x_sax = saz * sax;
	float caz_x_sax = caz * sax;

	xx = caz * cay - saz_x_sax * say; yx = saz * cay + caz_x_sax * say; zx = -cax * say;
	xy =                  -saz * cax; yy =                   caz * cax; zy =        sax;
	xz = caz * say + saz_x_sax * cay; yz = saz * say - caz_x_sax * cay; zz =  cax * cay;
	tx =                        0.0f; ty =                        0.0f; tz =       0.0f;
}

//
// name:		SetRotate
// description:	Set rotation from quaternion
void CMatrix::SetRotate(const CQuaternion& q)
{
#if 0
	asm volatile ("
		lwc1	$f1,0(%0)		//x
		lwc1	$f2,4(%0)		//y
		lwc1	$f3,8(%0)		//z
		lwc1	$f4,12(%0)		//w
		add.s	$f5,$f1,$f1		//2x
		add.s	$f6,$f2,$f2		//2y
		add.s	$f7,$f3,$f3		//2z
		
		// 0x3f800000 is 1.0 in floating point. Can't use li.s
		li		t0,0x3f800000
		mtc1	t0,$f8
		
		
	/*	float x2 = x + x;
		float y2 = y + y;
		float z2 = z + z;

		float xx = x * x2;
		float xy = x * y2;
		float xz = x * z2;

		float yy = y * y2;
		float yz = y * z2;
		float zz = z * z2;

		float wx = w * x2;
		float wy = w * y2;
		float wz = w * z2;
		
		pmat->right.x = 1.0f - (yy + zz);
		pmat->up.x = xy - wz;
		pmat->at.x = xz + wy;*/
		
		mula.s	$f8,$f8
		msuba.s	$f2,$f6
		msub.s	$f9,$f3,$f7
		mula.s	$f1,$f6
		msub.s	$f10,$f4,$f7
		mula.s	$f1,$f7
		madd.s	$f11,$f4,$f6
		swc1	$f9,0(%1)
		swc1	$f10,16(%1)
		swc1	$f11,32(%1)

	/*	pmat->right.y = xy + wz;
		pmat->up.y = 1.0f - (xx + zz);
		pmat->at.y = yz - wx;*/
		
		mula.s	$f1,$f6
		madd.s	$f9,$f4,$f7
		mula.s	$f8,$f8
		msuba.s	$f1,$f5
		msub.s	$f10,$f3,$f7
		mula.s	$f2,$f7
		msub.s	$f11,$f4,$f5
		swc1	$f9,4(%1)
		swc1	$f10,20(%1)
		swc1	$f11,36(%1)

	/*	pmat->right.z = xz - wy;
		pmat->up.z = yz + wx;
		pmat->at.z = 1.0f - (xx + yy);*/
		mula.s	$f1,$f7
		msub.s	$f9,$f4,$f6
		mula.s	$f2,$f7
		madd.s	$f10,$f4,$f5
		mula.s	$f8,$f8
		msuba.s	$f1,$f5
		msub.s	$f11,$f2,$f6
		swc1	$f9,8(%1)
		swc1	$f10,24(%1)
		swc1	$f11,40(%1)" 
	: 
	: "r"(&q), "r"(this)
	: "$f1", "$f2", "$f3", "$f4", "$f5", "$f6", "$f7", "$f8", "$f9", "$f10", "$f11");

#endif
}

void CMatrix::RotateX(float a)
{
	// | 1   0  0  0 |   | a  b  c  0 |   | a ca*b-sa*c sa*b+ca*c  0 |
	// | 0  ca sa  0 | . | d  e  f  0 | = | d ca*e-sa*f sa*e+ca*f  0 |
	// | 0 -sa ca  0 |   | g  h  i  0 |   | g ca*h-sa*i sa*h+ca*i  0 |
	// | 0   0  0  1 |   | j  k  l  1 |   | j ca*k-sa*l sa*k+ca*l  1 |

	float ca = CMaths::Cos(a);
	float sa = CMaths::Sin(a);

	float tmp[8];
	tmp[0] = ca * yx - sa * zx; tmp[1] = sa * yx + ca * zx;
	tmp[2] = ca * yy - sa * zy; tmp[3] = sa * yy + ca * zy;
	tmp[4] = ca * yz - sa * zz; tmp[5] = sa * yz + ca * zz;
	tmp[6] = ca * ty - sa * tz; tmp[7] = sa * ty + ca * tz;

	yx = tmp[0]; zx = tmp[1];
	yy = tmp[2]; zy = tmp[3];
	yz = tmp[4]; zz = tmp[5];
	ty = tmp[6]; tz = tmp[7];
}

void CMatrix::RotateY(float a)
{
	// | ca  0  sa  0 |   | a  b  c  0 |   | ca*a+sa*c   b -sa*a+ca*c  0 |
	// |  0  1   0  0 | . | d  e  f  0 | = | ca*d+sa*f   e -sa*d+ca*f  0 |
	// | sa  0 -ca  0 |   | g  h  i  0 |   | ca*g+sa*i   h -sa*g+ca*i  0 |
	// |  0  0   0  1 |   | j  k  l  1 |   | ca*j+sa*l   k -sa*j+ca*l  1 |

	float ca = CMaths::Cos(a);
	float sa = CMaths::Sin(a);

	float tmp[8];
	tmp[0] =  ca * xx + sa * zx; tmp[1] = -sa * xx + ca * zx;
	tmp[2] =  ca * xy + sa * zy; tmp[3] = -sa * xy + ca * zy;
	tmp[4] =  ca * xz + sa * zz; tmp[5] = -sa * xz + ca * zz;
	tmp[6] =  ca * tx + sa * tz; tmp[7] = -sa * tx + ca * tz;

	xx = tmp[0]; zx = tmp[1];
	xy = tmp[2]; zy = tmp[3];
	xz = tmp[4]; zz = tmp[5];
	tx = tmp[6]; tz = tmp[7];
}

void CMatrix::RotateZ(float a)
{
	// |  ca sa  0  0 |   | a  b  c  0 |   | ca*a-sa*b sa*a+ca*b  c  0 |
	// | -sa ca  0  0 | . | d  e  f  0 | = | ca*d-sa*e sa*d+ca*e  f  0 |
	// |   0  0  1  0 |   | g  h  i  0 |   | ca*g-sa*h sa*g+ca*h  i  0 |
	// |   0  0  0  1 |   | j  k  l  1 |   | ca*j-sa*k sa*j+ca*k  l  1 |

	float ca = CMaths::Cos(a);
	float sa = CMaths::Sin(a);

	float tmp[8];
	tmp[0] = ca * xx - sa * yx; tmp[1] = sa * xx + ca * yx;
	tmp[2] = ca * xy - sa * yy;	tmp[3] = sa * xy + ca * yy;
	tmp[4] = ca * xz - sa * yz;	tmp[5] = sa * xz + ca * yz;
	tmp[6] = ca * tx - sa * ty;	tmp[7] = sa * tx + ca * ty;

	xx = tmp[0]; yx = tmp[1];
	xy = tmp[2]; yy = tmp[3];
	xz = tmp[4]; yz = tmp[5];
	tx = tmp[6]; ty = tmp[7];
}

void CMatrix::Rotate(float ax, float ay, float az)
{
	float cax = CMaths::Cos(ax);
	float sax = CMaths::Sin(ax);
	float cay = CMaths::Cos(ay);
	float say = CMaths::Sin(ay);
	float caz = CMaths::Cos(az);
	float saz = CMaths::Sin(az);

	float saz_x_sax = saz * sax;
	float caz_x_sax = caz * sax;

	float mat[9];
	mat[0] = caz * cay - saz_x_sax * say; mat[1] = saz * cay + caz_x_sax * say; mat[2] = -cax * say;
	mat[3] =                  -saz * cax; mat[4] =                   caz * cax; mat[5] =        sax;
	mat[6] = caz * say + saz_x_sax * cay; mat[7] = saz * say - caz_x_sax * cay; mat[8] =  cax * cay;

	float tmp[12];
	tmp[ 0] = mat[0] * xx + mat[3] * yx + mat[6] * zx;
	tmp[ 1] = mat[1] * xx + mat[4] * yx + mat[7] * zx;
	tmp[ 2] = mat[2] * xx + mat[5] * yx + mat[8] * zx;
	tmp[ 3] = mat[0] * xy + mat[3] * yy + mat[6] * zy;
	tmp[ 4] = mat[1] * xy + mat[4] * yy + mat[7] * zy;
	tmp[ 5] = mat[2] * xy + mat[5] * yy + mat[8] * zy;
	tmp[ 6] = mat[0] * xz + mat[3] * yz + mat[6] * zz;
	tmp[ 7] = mat[1] * xz + mat[4] * yz + mat[7] * zz;
	tmp[ 8] = mat[2] * xz + mat[5] * yz + mat[8] * zz;
	tmp[ 9] = mat[0] * tx + mat[3] * ty + mat[6] * tz;
	tmp[10] = mat[1] * tx + mat[4] * ty + mat[7] * tz;
	tmp[11] = mat[2] * tx + mat[5] * ty + mat[8] * tz;

	xx = tmp[ 0]; yx = tmp[ 1]; zx = tmp[ 2];
	xy = tmp[ 3]; yy = tmp[ 4]; zy = tmp[ 5];
	xz = tmp[ 6]; yz = tmp[ 7]; zz = tmp[ 8];
	tx = tmp[ 9]; ty = tmp[10]; tz = tmp[11];
}



void CMatrix::Reorthogonalise()
{
	CVector	TempUp, TempRight, TempForward;
	float	Length, RLength;
	
	// TempUp = CrossPr(Right, Forward)
	TempUp.x =  yx * zy - zx * yy;
	TempUp.y = -xx * zy + zx * xy;
	TempUp.z =  xx * yy - yx * xy;
	// Normalise TempUp
	Length = CMaths::Sqrt(TempUp.x*TempUp.x + TempUp.y*TempUp.y + TempUp.z*TempUp.z);
	RLength = 1.0f / Length;
	TempUp.x *= RLength;
	TempUp.y *= RLength;
	TempUp.z *= RLength;
	// TempRight = CrossProduct(GetForward(), TempUp);
	TempRight.x = -TempUp.y * zy + TempUp.z * yy;
	TempRight.y =  TempUp.x * zy - TempUp.z * xy;
	TempRight.z = -TempUp.x * yy + TempUp.y * xy;

//	TempRight.x = -TempRight.x;
//	TempRight.y = -TempRight.y;
//	TempRight.z = -TempRight.z;
	// Normalise TempRight
	Length = CMaths::Sqrt(TempRight.x*TempRight.x + TempRight.y*TempRight.y + TempRight.z*TempRight.z);
	RLength = 1.0f / Length;
	TempRight.x *= RLength;
	TempRight.y *= RLength;
	TempRight.z *= RLength;

	// TempForward = CrossProduct(TempRight, TempUp);
	TempForward.x =  TempUp.y * TempRight.z - TempUp.z * TempRight.y;
	TempForward.y = -TempUp.x * TempRight.z + TempUp.z * TempRight.x;
	TempForward.z =  TempUp.x * TempRight.y - TempUp.y * TempRight.x;
	
	xx = TempRight.x;
	yx = TempRight.y;
	zx = TempRight.z;
	xy = TempForward.x;
	yy = TempForward.y;
	zy = TempForward.z;
	xz = TempUp.x;
	yz = TempUp.y;
	zz = TempUp.z;
}


/*
asm void CMatrix::Reorthogonalise()
{
	// load top 3 rows of matrix
	lqc2	vf01,0(a0)
	lqc2	vf02,16(a0)
	lqc2	vf03,32(a0)

	// square elements of first and second row
	vmul.xyz vf04,vf01,vf01
	vmul.xyz vf05,vf02,vf02
	vnop
	vnop
	// add squared elements of first row
	vmulax.w ACC,vf0,vf04x
	vmadday.w ACC,vf0,vf04y
	vmaddz.w vf04,vf0,vf04z
	// add squared elements of second row
	vmulax.w ACC,vf0,vf05x
	vmadday.w ACC,vf0,vf05y
	vmaddz.w vf05,vf0,vf05z
	// calculate 1 over magnitude of first row
	vrsqrt Q,vf0w,vf04w
	// third row is cross product of first two rows
	vopmula ACC,vf01,vf02
	vopmsub vf03,vf02,vf01
	// first row is cross product of second and third row
	vopmula ACC,vf02,vf03
	vopmsub vf01,vf03,vf02
	vwaitq
	// calculate 1 over magnitude of second row
	vrsqrt Q,vf0w,vf05w
	// magnitude of first row is in Q
	vmulq.xyz vf03,vf03,Q
	vmulq.xyz vf01,vf01,Q
	// wait for magnitude of second row
	vwaitq
	vmulq.xyz vf01,vf01,Q
	vmulq.xyz vf02,vf02,Q
	vmulq.xyz vf03,vf03,Q
	vnop
	vmulq.xyz vf01,vf01,Q
		
	sqc2	vf01,0(a0)
	sqc2	vf02,16(a0)

	jr		ra
	sqc2	vf03,32(a0)
}
*/

// Reorthogonalises the matrix with this new up vector.
//
void CMatrix::ForceUpVector(CVector NewUp)
{
	CVector	TempRight, TempForward;

	TempRight.x = -NewUp.y * zy + NewUp.z * yy;
	TempRight.y =  NewUp.x * zy - NewUp.z * xy;
	TempRight.z = -NewUp.x * yy + NewUp.y * xy;

	TempForward.x =  NewUp.y * TempRight.z - NewUp.z * TempRight.y;
	TempForward.y = -NewUp.x * TempRight.z + NewUp.z * TempRight.x;
	TempForward.z =  NewUp.x * TempRight.y - NewUp.y * TempRight.x;
	
	xx = TempRight.x;
	yx = TempRight.y;
	zx = TempRight.z;
	xy = TempForward.x;
	yy = TempForward.y;
	zy = TempForward.z;
	xz = NewUp.x;
	yz = NewUp.y;
	zz = NewUp.z;

}

// FUNCTION: CheckIntegrity
// PURPOSE:  Does a very rough chack to see whether a matrix is indeed orthogonal and stuff
//           Will only pick up on SOME defects
void CMatrix::CheckIntegrity()
{
	float	LengthTest;

	LengthTest = ABS(xx*xx + yx*yx + zx*zx);
	Assert(LengthTest > 0.8 && LengthTest < 1.2);
	LengthTest = ABS(xy*xy + yy*yy + zy*zy);
	Assert(LengthTest > 0.8 && LengthTest < 1.2);
	LengthTest = ABS(xz*xz + yz*yz + zz*zz);
	Assert(LengthTest > 0.8 && LengthTest < 1.2);
}


CMatrix Invert(const CMatrix& m)
{
	CMatrix mat;
	
	return Invert(m, mat);
}

CMatrix &Invert(const CMatrix& m, CMatrix& output)
{
	// load first third rows to general purpose registers
//	lq		t0,0(a0)
//	lq		t1,16(a0)
//	lq		t2,32(a0)
	// load fourth row into VU4
//	lqc2	vf4,48(a0)
	
//	vmove.xyzw	vf5,vf4			//MOVING (COPYING?) V4 to V5
//	vsub.xyz	vf4,vf4,vf4		//SETTING V4 (3components) to zero
	output.tx = 0.0f;	output.ty = 0.0f;	output.tz = 0.0f;	//output.tw = m.tw;
//	vmove.xyzw	vf9,vf4			//MOVING (COPYING?) V4 to V9 (ie only w component there)
//	qmfc2	t3,vf4				//TRANSFERS (MOVES) contents of V4 to General Purpose Reg T3
	
	// transpose matrix without translation and store in t0-t2
//	pextlw	t4,t1,t0	//T4 = (T0x, T1x, T0y, T1y)
//	pextuw	t5,t1,t0	//T5 = (T0z, T1z, T0w, T1w)
//	pextlw	t6,t3,t2	//T6 = (T2x, T3x, T2y, T3y)
//	pextuw	t7,t3,t2	//T5 = (T2z, T3z, T2w, T3w)

//	pcpyld	t0,t6,t4	//T0 = (T4x, T4y, T6x, T6y) = (T0x, T1x, T2x, T3x)
//	pcpyud	t1,t4,t6	//T1 = (T4z, T4w, T6z, T6w) = (T0y, T1y, T2y, T3y)
//	pcpyld	t2,t7,t5	//T2 = (T5x, T5y, T7x, T7y) = (T0z, T1z, T2z, T3z)
	
	// load general purpose register t0-t2 into VU0 registers vf6-vf8
//	qmtc2 	t0,vf6		//TRANSFERS (MOVES) contents of T0 to V6
//	qmtc2 	t1,vf7		//TRANSFERS (MOVES) contents of T1 to V7
//	qmtc2 	t2,vf8		//TRANSFERS (MOVES) contents of T2 to V8
	output.xx = m.xx;	output.yx = m.xy;	output.zx = m.xz;	//output.wx = m.tx;
	output.xy = m.yx;	output.yy = m.yy;	output.zy = m.yz;	//output.wy = m.ty;
	output.xz = m.zx;	output.yz = m.zy;	output.zz = m.zz;	//output.wz = m.tz;

	// transform translation with new matrix
//	vmulax.xyz	ACC,vf6,vf5x	//MULTIPLY V6 by x component of V5 and STORE in ACC accumulator
	output.tx += output.xx * m.tx;	output.ty += output.yx * m.tx;	output.tz += output.zx * m.tx;	//output.tw += output.wx * m.tx;
//	vmadday.xyz	ACC,vf7,vf5y	//MULTIPLY V7 by y component of V5 and ADD to ACC
	output.tx += output.xy * m.ty;	output.ty += output.yy * m.ty;	output.tz += output.zy * m.ty;	//output.tw += output.wy * m.ty;
//	vmaddz.xyz	vf4,vf8,vf5z	//MCC + (MULTIPLY V8 by z component of V5) and ADD to V4
	output.tx += output.xz * m.tz;	output.ty += output.yz * m.tz;	output.tz += output.zz * m.tz;	//output.tw += output.wz * m.tz;
	
//	vsub.xyz	vf4,vf9,vf4		//SUBTRACT V4 from V9 and store in V4
	output.tx = -1.0f*output.tx;	output.ty = -1.0f*output.ty;	output.tz = -1.0f*output.tz;	//output.tw = m.tw - output.tw;
	
	// store matrix
//	sq		t0,0(a1)
//	sq		t1,16(a1)
//	sq		t2,32(a1)
//	sqc2	vf4,48(a1)
	return output;

//	jr		ra
//	qmove	v0,a1	

//////////////////////////////////////////
// ORIGINAL PC VERSION
//////////////////////////////////////////
/*	float tmp[3];
	tmp[0] =  m.yy * m.zz - m.zy * m.yz;
	tmp[1] = -m.xy * m.zz + m.zy * m.xz;
	tmp[2] =  m.xy * m.yz - m.yy * m.xz;

	float fDeterminant = m.xx * tmp[0] + m.yx * tmp[1] + m.zx * tmp[2];

	ASSERT(fDeterminant != 0.0f);

	float ood = 1.0f / fDeterminant;

	output.xx = tmp[0] * ood;
	output.yx = (-m.yx * m.zz + m.zx * m.yz) * ood;
	output.zx = ( m.yx * m.zy - m.zx * m.yy) * ood;

	output.xy = tmp[1] * ood;
	output.yy = ( m.xx * m.zz - m.zx * m.xz) * ood;
	output.zy = (-m.xx * m.zy + m.zx * m.xy) * ood;

	output.xz = tmp[2] * ood;
	output.yz = (-m.xx * m.yz + m.yx * m.xz) * ood;
	output.zz = ( m.xx * m.yy - m.yx * m.xy) * ood;

	output.tx = -(m.tx * m.xx + m.ty * m.yx + m.tz * m.zx);
	output.ty = -(m.tx * m.xy + m.ty * m.yy + m.tz * m.zy);
	output.tz = -(m.tx * m.xz + m.ty * m.yz + m.tz * m.zz);
	return output;
*/
}

CMatrix operator*(const CMatrix& m1, const CMatrix& m2)
{
	CMatrix mat;
	mat.xx = m1.xx * m2.xx + m1.xy * m2.yx + m1.xz * m2.zx;
	mat.yx = m1.yx * m2.xx + m1.yy * m2.yx + m1.yz * m2.zx;
	mat.zx = m1.zx * m2.xx + m1.zy * m2.yx + m1.zz * m2.zx;
	mat.xy = m1.xx * m2.xy + m1.xy * m2.yy + m1.xz * m2.zy;
	mat.yy = m1.yx * m2.xy + m1.yy * m2.yy + m1.yz * m2.zy;
	mat.zy = m1.zx * m2.xy + m1.zy * m2.yy + m1.zz * m2.zy;
	mat.xz = m1.xx * m2.xz + m1.xy * m2.yz + m1.xz * m2.zz;
	mat.yz = m1.yx * m2.xz + m1.yy * m2.yz + m1.yz * m2.zz;
	mat.zz = m1.zx * m2.xz + m1.zy * m2.yz + m1.zz * m2.zz;
	mat.tx = m1.xx * m2.tx + m1.xy * m2.ty + m1.xz * m2.tz + m1.tx;
	mat.ty = m1.yx * m2.tx + m1.yy * m2.ty + m1.yz * m2.tz + m1.ty;
	mat.tz = m1.zx * m2.tx + m1.zy * m2.ty + m1.zz * m2.tz + m1.tz;

	return mat;
}

CMatrix operator+(const CMatrix& m1, const CMatrix& m2)
{
	CMatrix mat;
	mat.xx = m1.xx + m2.xx;
	mat.yx = m1.yx + m2.yx;
	mat.zx = m1.zx + m2.zx;
	mat.xy = m1.xy + m2.xy;
	mat.yy = m1.yy + m2.yy;
	mat.zy = m1.zy + m2.zy;
	mat.xz = m1.xz + m2.xz;
	mat.yz = m1.yz + m2.yz;
	mat.zz = m1.zz + m2.zz;
	mat.tx = m1.tx + m2.tx;
	mat.ty = m1.ty + m2.ty;
	mat.tz = m1.tz + m2.tz;

	return mat;
	
}

// FUNCTION: DecompressIntoFullMatrix
// PURPOSE:  Takes a compressed matrix and unpacks it into a full matrix
void CCompressedMatrixNotAligned::DecompressIntoFullMatrix(CMatrix &Matrix)
{
	CVector	ThirdVec;

	Matrix.xx = this->Matrix_xx / 127.0f;
	Matrix.yx = this->Matrix_yx / 127.0f;
	Matrix.zx = this->Matrix_zx / 127.0f;
	Matrix.xy = this->Matrix_xy / 127.0f;
	Matrix.yy = this->Matrix_yy / 127.0f;
	Matrix.zy = this->Matrix_zy / 127.0f;

	// Work out the third vector of the matrix.
	ThirdVec = CrossProduct(Matrix.GetRight(), Matrix.GetForward());
	Matrix.xz = ThirdVec.x;
	Matrix.yz = ThirdVec.y;
	Matrix.zz = ThirdVec.z;
	
	Matrix.tx = this->CoorX;
	Matrix.ty = this->CoorY;
	Matrix.tz = this->CoorZ;
	
	Matrix.Reorthogonalise();

}


// FUNCTION: DecompressIntoFullMatrix
// PURPOSE:  Takes a compressed matrix and unpacks it into a full matrix
void CCompressedMatrixNotAligned::CompressFromFullMatrix(CMatrix &Matrix)
{
	this->Matrix_xx = (char)(Matrix.xx * 127.0f);
	this->Matrix_yx = (char)(Matrix.yx * 127.0f);
	this->Matrix_zx = (char)(Matrix.zx * 127.0f);
	this->Matrix_xy = (char)(Matrix.xy * 127.0f);
	this->Matrix_yy = (char)(Matrix.yy * 127.0f);
	this->Matrix_zy = (char)(Matrix.zy * 127.0f);

	this->CoorX = Matrix.tx;
	this->CoorY = Matrix.ty;
	this->CoorZ = Matrix.tz;
}