// Title	:	Matrix.h
// Author	:	Richard Jobling
// Started	:	19/10/98

#ifndef _MATRIX_H_
#define _MATRIX_H_

#include "libcore/vector.h"

class CQuaternion;

class CMatrix
{
	friend class CVector;

public:
	CMatrix() {}
	CMatrix(const CMatrix& mat);
	CMatrix(float sx, float sy, float sz) { SetScale(sx, sy, sz); }
	explicit CMatrix(float s) { SetScale(s); }
	~CMatrix();
	
	// These functions are a little dirty but are a lot quicker than calling a constructor
	// every time
	const CVector& GetRight() const { return *(CVector*)&xx;}
	const CVector& GetForward() const { return *(CVector*)&xy;}
	const CVector& GetUp() const { return *(CVector*)&xz;}
	const CVector& GetTranslate() const { return *(CVector*)&tx;}
	CVector& GetRight() { return *(CVector*)&xx;}
	CVector& GetForward() { return *(CVector*)&xy;}
	CVector& GetUp() { return *(CVector*)&xz;}
	CVector& GetTranslate() { return *(CVector*)&tx;}

	void CopyOnlyMatrix(const CMatrix& m);
	void SetUnity();
	void ResetOrientation();
	void SetScale(float s);
	void SetScale(float sx, float sy, float sz);
	void SetScale(const CVector& v) { SetScale(v.x, v.y, v.z); }
	void SetTranslate(float tx, float ty, float tz);
	void SetTranslate(const CVector& v) { SetTranslate(v.x, v.y, v.z); }
	void SetTranslateOnly(float tx, float ty, float tz);
	void SetTranslateOnly(const CVector& v) { SetTranslateOnly(v.x, v.y, v.z); }
	void SetRotateXOnly(float a);
	void SetRotateYOnly(float a);
	void SetRotateZOnly(float a);
	void SetRotateX(float a);
	void SetRotateY(float a);
	void SetRotateZ(float a);
	void SetRotate(float ax, float ay, float az);
	void SetRotate(const CVector& v) { SetRotate(v.x, v.y, v.z); }
	void SetRotate(const CQuaternion& quat);
	
	void Scale(float s);
	void Scale(float sx, float sy, float sz);
	void Scale(const CVector& v) { Scale(v.x, v.y, v.z); }
	void Translate(float tx, float ty, float tz);
	void Translate(const CVector& v) { Translate(v.x, v.y, v.z); }
	void RotateX(float a);
	void RotateY(float a);
	void RotateZ(float a);
	void Rotate(float ax, float ay, float az);
	void Rotate(const CVector& v) { Rotate(v.x, v.y, v.z); }

	void Reorthogonalise();
	void ForceUpVector(CVector NewUp);

	void CheckIntegrity();

	friend CMatrix Invert(const CMatrix& m);
	friend CMatrix& Invert(const CMatrix& m, CMatrix& output);

	CMatrix& operator=(const CMatrix& m);
	CMatrix& operator*=(const CMatrix& m);
	CMatrix& operator+=(const CMatrix& m);

	friend CMatrix operator+(const CMatrix& m1, const CMatrix& m2);
	friend CMatrix operator*(const CMatrix& m1, const CMatrix& m2);

	friend bool operator==(const CMatrix& m1, const CMatrix& m2);
	friend bool operator!=(const CMatrix& m1, const CMatrix& m2) { return !(m1 == m2); }

	union
	{
		struct
		{
			float xx, yx, zx, pad1;
			float xy, yy, zy, pad2;
			float xz, yz, zz, pad3;
			float tx, ty, tz, pad4;
		};

		struct
		{
			CVector right;
			CVector at;
			CVector up;
			CVector trans;
		};
	};
};

inline void CMatrix::Scale(float s)
{
	for (int32 i = 0; i < 12; i+=4)
	{
		(&xx)[i + 0] *= s;
		(&xx)[i + 1] *= s;
		(&xx)[i + 2] *= s;
	}
}

inline void CMatrix::Scale(float sx, float sy, float sz)
{
	for (int32 i = 0; i < 12; i += 4)
	{
		(&xx)[i + 0] *= sx;
		(&xx)[i + 1] *= sy;
		(&xx)[i + 2] *= sz;
	}
}

inline CMatrix& CMatrix::operator*=(const CMatrix& m)
{
	*this = *this * m;

	return *this;
}

inline void CMatrix::Translate(float x, float y, float z)
{
	tx += x;
	ty += y;
	tz += z;
}

inline bool operator==(const CMatrix& m1, const CMatrix& m2)
{
	if (m1.xx != m2.xx ||
		m1.yx != m2.yx ||
		m1.zx != m2.zx ||
		m1.xy != m2.xy ||
		m1.yy != m2.yy ||
		m1.zy != m2.zy ||
		m1.xz != m2.xz ||
		m1.yz != m2.yz ||
		m1.zz != m2.zz ||
		m1.tx != m2.tx ||
		m1.ty != m2.ty ||
		m1.tz != m2.tz)		
		return false;
	return true;
}

///////////////////////////////////////////////////////////////
// This Class is used to make matrices smaller for save purposes.

class CCompressedMatrixNotAligned
{
	public:
	float	CoorX, CoorY, CoorZ;
	Int8	Matrix_xx, Matrix_yx, Matrix_zx;
	Int8	Matrix_xy, Matrix_yy, Matrix_zy;
	void    DecompressIntoFullMatrix(CMatrix &Matrix);
	void    CompressFromFullMatrix(CMatrix &Matrix);
};

///////////////////////////////////////////////////////////////
// This Class is used to make matrices smaller for save purposes.

class CCompressedMatrix : public CCompressedMatrixNotAligned
{
	public:
	Int8	Pad1, Pad2;		// This pads the CCompressedMatrixNotAligned out to 32 bit boundary
};

#endif
