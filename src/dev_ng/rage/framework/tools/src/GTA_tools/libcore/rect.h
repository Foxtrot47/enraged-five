#ifndef COMMON_RECT_H
#define COMMON_RECT_H

#include "libcore/vector2d.h"

//////////////////////////////////////////////////////////////////////////////
class CRect
//////////////////////////////////////////////////////////////////////////////
{
public:
	float left;
	float top;
	float right;
	float bottom;

	CRect();
	CRect(float b,float l,float t,float r);

	void init(CVector2D& vecInit);
	void addPoint(CVector2D& vecAdd);

	bool isInside(const CVector2D& vecCheck) const;
	bool doesIntersect(const CRect& r_rect) const;
};

#endif //COMMON_RECT_H