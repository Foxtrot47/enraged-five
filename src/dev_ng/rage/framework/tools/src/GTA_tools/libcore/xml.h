#ifndef CORE_XML_H
#define CORE_XML_H

#include <string>
#include <map>
#include <list>

namespace WinLib {

class BinaryStream;

///////////////////////////////////////////////////////////////////////////////////////////////
struct XmlElement
///////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	XmlElement(): p_parent(NULL) {}
	~XmlElement();

	XmlElement* getChild(std::string& r_name);

	std::string name;
	std::map<std::string,std::string> params;
	std::string value;
	std::list<XmlElement*> children;
	XmlElement* p_parent;
};

///////////////////////////////////////////////////////////////////////////////////////////////
class XmlParse
///////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	XmlParse(): mp_elemRoot(NULL),mp_elemCurr(NULL) {}

	XmlElement* read(BinaryStream& r_sRead);
	void write(BinaryStream& r_sWrite,const XmlElement& r_xmlElement);

protected:
	void writeTabs(uint32 uTabs);
	void writeElement(const XmlElement& r_xmlElement,uint32 uTabLevel);
	void readHeader();
	void readTag();
	void validateChar(uint8 uChar,uint8 uCheck);

	//temporaries for reading and writing

	BinaryStream* mp_sRead;

	//temporaries for reading

	XmlElement* mp_elemRoot;
	XmlElement* mp_elemCurr;
	std::string m_readBuffer;
};

} //namespace WinLib {

#endif //CORE_XML_H