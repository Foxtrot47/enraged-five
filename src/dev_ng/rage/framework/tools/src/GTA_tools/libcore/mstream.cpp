#include "libcore/mstream.h"

#include <string.h>

using namespace WinLib;

///////////////////////////////////////////////////////////////////////////////////////////////
MemoryStream::MemoryStream(uint32 uSize)
{
	if(uSize == 0)
	{
		return;
	}

	m_uSize = uSize;
	m_pData = new uint8[m_uSize];
	m_pCurrentRead = m_pData;
	m_pCurrentWrite = m_pData;
}

///////////////////////////////////////////////////////////////////////////////////////////////
MemoryStream::~MemoryStream()
{
	delete[] m_pData;
}

///////////////////////////////////////////////////////////////////////////////////////////////
uint32 MemoryStream::seek(uint32 uOffset,int32 seekOrigin)
{
	switch(seekOrigin)
	{
	case STREAM_BEGIN:
		Assert(false);
//		m_pCurrentRead = m_pData + uOffset;
		break;
	case STREAM_CURRENT:
		m_pCurrentRead += uOffset;
		break;
	case STREAM_END:
		Assert(false);
//		m_pCurrentRead = m_pData + (m_uSize - uOffset);
		break;
	}

	return getPending();
}

///////////////////////////////////////////////////////////////////////////////////////////////
void MemoryStream::reset()
{
	m_pCurrentWrite = m_pData;
	m_pCurrentRead = m_pData;
}

///////////////////////////////////////////////////////////////////////////////////////////////
void MemoryStream::flush()
{
	if(m_pCurrentRead >= m_pCurrentWrite)
	{
		reset();
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////
void MemoryStream::advanceWrite(uint32 uAdvance)
{
	m_pCurrentWrite += uAdvance;
}

///////////////////////////////////////////////////////////////////////////////////////////////
void MemoryStream::advanceRead(uint32 uAdvance)
{
	m_pCurrentRead += uAdvance;
	flush();
}

///////////////////////////////////////////////////////////////////////////////////////////////
uint32 MemoryStream::write(void* p_cWriteFrom,uint32 uiBytes)
{
	uint32 uBytesLeft = getPending();

	Assert(uiBytes <= (m_uSize - (uint32)(m_pCurrentWrite - m_pData)));

//	if(uiBytes > uBytesLeft)
//	{
//		uiBytes = uBytesLeft;
//	}

	flush();

	memcpy(m_pCurrentWrite,p_cWriteFrom,uiBytes);
	m_pCurrentWrite += uiBytes;

	return uiBytes;
}

///////////////////////////////////////////////////////////////////////////////////////////////
uint32 MemoryStream::read(void* p_cReadTo,uint32 uiBytes)
{
	uint32 uBytesLeft = getPending();

	if(uiBytes > uBytesLeft)
	{
		uiBytes = uBytesLeft;
	}

	memcpy(p_cReadTo,m_pCurrentRead,uiBytes);
	m_pCurrentRead += uiBytes;

	return uiBytes;
}

///////////////////////////////////////////////////////////////////////////////////////////////
uint32 MemoryStream::getPos()
{
	return getPending();
}

///////////////////////////////////////////////////////////////////////////////////////////////
uint32 MemoryStream::getPending()
{
	return ((uint32)(m_pCurrentWrite - m_pCurrentRead));
}