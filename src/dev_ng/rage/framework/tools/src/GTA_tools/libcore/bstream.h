#ifndef COMMON_BSTREAM_H
#define COMMON_BSTREAM_H

namespace WinLib {

#define STREAM_BEGIN 0
#define STREAM_CURRENT 1
#define STREAM_END 2

///////////////////////////////////////////////////////////////////////////////////////////////
class BinaryStream
///////////////////////////////////////////////////////////////////////////////////////////////
{
public:

	virtual ~BinaryStream() {}

	void printf(const char*,...);

	void readLine(char* p_cRead,uint32 uMaxRead);
	void writeLine(const char* p_cWrite,...);

	uint8 readU8();
	void writeU8(uint8 uWrite);

	uint32 readU32();
	void writeU32(uint32 uWrite);

	virtual uint32 seek(uint32 uOffset,int32 seekOrigin) = 0;
	virtual uint32 getPos() = 0;
	virtual uint32 write(void* p_cWriteFrom,uint32 uiBytes) = 0;
	virtual uint32 read(void* p_cReadTo,uint32 uiBytes) = 0;
	virtual uint32 getPending() = 0;
	virtual void flush() {}
};

} //namespace WinLib

#endif //COMMON_BSTREAM_H