//
// name:		Maths.cpp
// description:	Maths tables code
// written by:	Adam Fowler
//
#include "libcore/Maths.h"


// maths tables
/*float CMaths::ms_sinTable[MATHS_TABLE_SIZE+1];
float CMaths::ms_acosTable[MATHS_TABLE_SIZE+1];
float CMaths::ms_atanTable[MATHS_TABLE_SIZE+1];


void CMaths::InitMathsTables()
{
	for(int32 i=0; i<=MATHS_TABLE_SIZE; i++)
	{
		ms_sinTable[i] = sinf(HALF_PI * i/(float)MATHS_TABLE_SIZE);
		ms_acosTable[i] = acosf(((i*2)/(float)MATHS_TABLE_SIZE)-1.0f);
		ms_atanTable[i] = atanf(i/(float)MATHS_TABLE_SIZE);
	}
}

float CMaths::QuickSin(float v)
{
	int32 index = (v / HALF_PI) * MATHS_TABLE_SIZE;
	
	// ensure we are between -PI and PI
	while(index > MATHS_TABLE_SIZE*2)
		index -= MATHS_TABLE_SIZE*4;
	while(index < -MATHS_TABLE_SIZE*2)
		index += MATHS_TABLE_SIZE*4;
		
	if(index > MATHS_TABLE_SIZE)
		index = 2*MATHS_TABLE_SIZE - index;
	else if(index < -MATHS_TABLE_SIZE)
		index = -2*MATHS_TABLE_SIZE - index;
		
	if(index < 0)
		return -ms_sinTable[-index];
	return ms_sinTable[index];
}


float CMaths::QuickCos(float v)
{
	int32 index = (v / HALF_PI) * MATHS_TABLE_SIZE;
	index += MATHS_TABLE_SIZE;
	
	while(index > MATHS_TABLE_SIZE*2)
		index -= MATHS_TABLE_SIZE*4;
	while(index < -MATHS_TABLE_SIZE*2)
		index += MATHS_TABLE_SIZE*4;
		
	if(index > MATHS_TABLE_SIZE)
		index = 2*MATHS_TABLE_SIZE - index;
	else if(index < -MATHS_TABLE_SIZE)
		index = -2*MATHS_TABLE_SIZE - index;
		
	if(index < 0)
		return -ms_sinTable[-index];
	return ms_sinTable[index];
}

float CMaths::QuickACos(float v)
{
	int32 index = ((v+1.0f) / 2.0f)*MATHS_TABLE_SIZE;
	return ms_acosTable[index];
}

float CMaths::QuickASin(float v)
{
	int32 index = ((v+1.0f) / 2.0f)*MATHS_TABLE_SIZE;
	return HALF_PI - ms_acosTable[index];
}

float CMaths::QuickATan(float v)
{
	if(v < 0)
	{
		v = -v;
		if(v > 1)
			return -(HALF_PI - ms_atanTable[(int32)((1/v)*MATHS_TABLE_SIZE)]);
		else
			return -ms_atanTable[(int32)(v*MATHS_TABLE_SIZE)];
	}
	if(v > 1)
		return HALF_PI - ms_atanTable[(int32)((1/v)*MATHS_TABLE_SIZE)];
	else
		return ms_atanTable[(int32)(v*MATHS_TABLE_SIZE)];
}

float CMaths::QuickATan2(float y, float x)
{
	float v = QuickATan(y/x);
	
	if(x==0)
	{
		if(y==0)
			return 0;
		else if(y<0)
			return -HALF_PI;
		else
			return HALF_PI;
	}
	if(x<0)
	{
		if(y>0)
			return v + PI;
		else
			return v - PI;	
	}
	return v;
}*/