// Title	:	Vector.cpp
// Author	:	Richard Jobling
// Started	:	19/10/98

#include "libcore/Vector.h"
#include "libcore/Matrix.h"


CVector CrossProduct(const CVector& v1, const CVector& v2)
{
	CVector vec;
	vec.x =  v1.y * v2.z - v1.z * v2.y;
	vec.y = -v1.x * v2.z + v1.z * v2.x;
	vec.z =  v1.x * v2.y - v1.y * v2.x;

	return vec;
}

CVector Multiply3x3(const class CMatrix& m, const CVector& v)
{
#ifdef GTA_PS2
	CVector result;
	asm volatile ("
		// load vector
        ldr		t0,0(%2)		# R12 = ?,?,?,x
        ldl		t0,7(%2)		# R12 = ?,?,y,x
        lw		t1,8(%2)		# R10 = ?,?,S,v.z
        pcpyld  t2,t1,t0		# R13 = S,z,y,x
        qmtc2	t2, vf01
		// load matrix
		lqc2	vf02,0(%1)
		lqc2	vf03,16(%1)
		lqc2	vf04,32(%1)

		// multiple vector by matrix	
		vmulax.xyz	ACC,vf02,vf01x
		vmadday.xyz	ACC,vf03,vf01y
		vmaddz.xyz	vf06,vf04,vf01z
		
		// store vector
		qmfc2	t0,vf06         # Store result in t0
		sw		t0,0(%0)        # Store result x
		prot3w	t1,t0           # Rotate y into LSW
		sw		t1,4(%0)        # Store result y
		prot3w	t0,t1           # Rotate z into LSW
		sw		t0,8(%0)        # Store result z
	
	":
	 : "r"(&result), "r"(&m), "r"(&v)
	 : "t0", "t1", "t2");
	return result;
#else	 
	CVector vec;
	vec.x = m.xx * v.x + m.xy * v.y + m.xz * v.z;
	vec.y = m.yx * v.x + m.yy * v.y + m.yz * v.z;
	vec.z = m.zx * v.x + m.zy * v.y + m.zz * v.z;
	return vec;
#endif	
}

CVector Multiply3x3(const CVector& v, const class CMatrix& m)
{
	CVector vec;
	vec.x = m.xx * v.x + m.yx * v.y + m.zx * v.z;
	vec.y = m.xy * v.x + m.yy * v.y + m.zy * v.z;
	vec.z = m.xz * v.x + m.yz * v.y + m.zz * v.z;

	return vec;
}

void CVector::FromMultiply(const CMatrix& m, const CVector& v)
{
	x = m.tx + m.xx * v.x + m.xy * v.y + m.xz * v.z;
	y = m.ty + m.yx * v.x + m.yy * v.y + m.yz * v.z;
	z = m.tz + m.zx * v.x + m.zy * v.y + m.zz * v.z;
}

void CVector::FromMultiply3X3(const CMatrix& m, const CVector& v)
{
	x = m.xx * v.x + m.xy * v.y + m.xz * v.z;
	y = m.yx * v.x + m.yy * v.y + m.yz * v.z;
	z = m.zx * v.x + m.zy * v.y + m.zz * v.z;
}

CVector operator*(const class CMatrix& m, const CVector& v)
{
#ifdef GTA_PS2
	CVector result;
	asm volatile ("
		// load vector
        ldr		t0,0(%2)		# R12 = ?,?,?,x
        ldl		t0,7(%2)		# R12 = ?,?,y,x
        lw		t1,8(%2)		# R10 = ?,?,S,v.z
        pcpyld  t2,t1,t0		# R13 = S,z,y,x
        qmtc2	t2, vf01

		// load matrix
		lqc2	vf02,0(%1)
		lqc2	vf03,16(%1)
		lqc2	vf04,32(%1)
		lqc2	vf05,48(%1)

		// multiple vector by matrix	
		vmulax.xyz	ACC,vf02,vf01x
		vmadday.xyz	ACC,vf03,vf01y
		vmaddaz.xyz	ACC,vf04,vf01z
		vmaddw.xyz 	vf06,vf05,vf0w
		
		// store vector
		qmfc2	t0,vf06         # Store result in t0
		sw		t0,0(%0)        # Store result x
		prot3w	t1,t0           # Rotate y into LSW
		sw		t1,4(%0)        # Store result y
		prot3w	t0,t1           # Rotate z into LSW
		sw		t0,8(%0)        # Store result z
	
	":
	 : "r"(&result), "r"(&m), "r"(&v)
	 : "t0", "t1", "t2");

	return result;
#else
	CVector vec;
	vec.x = m.xx * v.x + m.xy * v.y + m.xz * v.z + m.tx;
	vec.y = m.yx * v.x + m.yy * v.y + m.yz * v.z + m.ty;
	vec.z = m.zx * v.x + m.zy * v.y + m.zz * v.z + m.tz;
	return vec;
#endif	
}

void CVector::Normalise()
{
	float	MagSqr;

	MagSqr = MagnitudeSqr();
	if (MagSqr <= 0.0f)
	{
		x = 1.0f;

	//	ASSERT(MagnitudeSqr() > 0.0f);
		return;
	}

	*this *= CMaths::RecipSqrt(MagSqr);
}
