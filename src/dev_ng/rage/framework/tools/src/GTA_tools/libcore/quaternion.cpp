// Title	:	Quaternion.cpp
// Author	:	Aaron Puzey
// Started	:	07/07/98
#include "libcore/Quaternion.h"

void CQuaternion::Set(const CMatrix& mat)
{
	float t;
    
    if((t = mat.right.x + mat.up.y + mat.at.z) >= 0.0f)
    {
		// w is largest.
		w = CMaths::RecipSqrt(2.0f, (t + 1.0f));
		x = ( (mat.up.z - mat.at.y) / 4.0f ) * w;
		y = ( (mat.at.x - mat.right.z) / 4.0f ) * w;
		z = ( (mat.right.y - mat.up.x) / 4.0f ) * w;
    }
    else 
	if((t = mat.right.x - mat.up.y - mat.at.z) >= 0.0f)
    {
		// x is largest.
		x = CMaths::RecipSqrt(2.0f, (t + 1.0f));
		y = ( (mat.up.x + mat.right.y) / 4.0f ) * x;
		z = ( (mat.at.x + mat.right.z) / 4.0f ) * x;
		w = ( (mat.up.z - mat.at.y) / 4.0f ) * x;
    }
    else 
	if((t = -mat.right.x + mat.up.y - mat.at.z) >= 0.0f)
    {
		// y is largest.
		y = CMaths::RecipSqrt(2.0f, (t + 1.0f));
		w = ( (mat.at.x - mat.right.z) / 4.0f ) * y;
		x = ( (mat.up.x - mat.right.y) / 4.0f ) * y;
		z = ( (mat.at.y + mat.up.z) / 4.0f ) * y;
    }
    else
    {
		// z is largest.
		t = -mat.right.x - mat.up.y + mat.at.z;
		z = CMaths::RecipSqrt(2.0f, (t + 1.0f));
		w = ( (mat.right.y - mat.up.x) / 4.0f ) * z;
		x = ( (mat.at.x + mat.right.z) / 4.0f ) * z;
		y = ( (mat.at.y + mat.up.z) / 4.0f ) * z;
    }
}

void CQuaternion::Set(float ax, float ay, float az)
{
	float cx = CMaths::Cos(ax/2);
	float cy = CMaths::Cos(ay/2);
	float cz = CMaths::Cos(az/2);

	float sx = CMaths::Sin(ax/2);
	float sy = CMaths::Sin(ay/2);
	float sz = CMaths::Sin(az/2);

	float cxcy = cx * cy;
	float sxsy = sx * sy;
	float sxcy = sx * cy;
	float cxsy = cx * sy;

	w = cz * cxcy + sz * sxsy;
	x = sz * cxcy - cz * sxsy;
	y = cz * sxcy + sz * cxsy;
	z = cz * cxsy - sz * sxcy;
}

void CQuaternion::Set(CVector* pVec, float fRotation)
{
	float fCosRotation = CMaths::Cos(fRotation/2.0f);
	float fSinRotation = CMaths::Sin(fRotation/2.0f);

	x = pVec->x * fSinRotation;
	y = pVec->y * fSinRotation;
	z = pVec->z * fSinRotation;
	w = fCosRotation;
}

void CQuaternion::Get(CMatrix* pmat) const
{
	float x2 = x + x;
	float y2 = y + y;
	float z2 = z + z;

	float xx = x * x2;
	float xy = x * y2;
	float xz = x * z2;

	float yy = y * y2;
	float yz = y * z2;
	float zz = z * z2;

	float wx = w * x2;
	float wy = w * y2;
	float wz = w * z2;
	
	pmat->right.x = 1.0f - (yy + zz);
	pmat->up.x = xy - wz;
	pmat->at.x = xz + wy;
	pmat->right.y = xy + wz;
	pmat->up.y = 1.0f - (xx + zz);
	pmat->at.y = yz - wx;
	pmat->right.z = xz - wy;
	pmat->up.z = yz + wx;
	pmat->at.z = 1.0f - (xx + yy);
}

void CQuaternion::Get(float* pax, float* pay, float* paz) const
{
	CMatrix mat;

	Get(&mat);

	// find roll
	*paz = CMaths::ATan2(mat.right.y, mat.up.y);
	if (*paz < DEGTORAD(0.0f))
	{
		*paz += DEGTORAD(360.0f);
	}

	float saz = CMaths::Sin(*paz);
	float caz = CMaths::Cos(*paz);

	// find pitch
	*pax = CMaths::ATan2(-mat.at.y, mat.up.y * caz + mat.right.y * saz);
	if (*pax < DEGTORAD(0.0f))
	{
		*pax += DEGTORAD(360.0f);
	}

	// find yaw
	*pay = CMaths::ATan2(-(mat.right.z * caz - mat.up.z * saz), mat.right.x * caz - mat.up.x * saz);
	if (*pay < DEGTORAD(0.0f))
	{
		*pay += DEGTORAD(360.0f);
	}
}

void CQuaternion::Get(CVector* pVec, float* fRotation) const
{
	*fRotation = CMaths::ACos(2.0f * w);
	float fOOSinRotation = 1.0f / CMaths::Sin(*fRotation);

	pVec->x = x * fOOSinRotation;
	pVec->y = y * fOOSinRotation;
	pVec->z = z * fOOSinRotation;
}

void CQuaternion::Multiply(const CQuaternion& quat1, const CQuaternion& quat2)
{
	Assert(&quat1 != this);
	Assert(&quat2 != this);

	x = quat1.y*quat2.z - quat2.y*quat1.z;
	y = quat1.z*quat2.x - quat2.z*quat1.x;
	z = quat1.x*quat2.y - quat2.x*quat1.y;

	x += quat1.x*quat2.w + quat2.x*quat1.w;
	y += quat1.y*quat2.w + quat2.y*quat1.w;
	z += quat1.z*quat2.w + quat2.z*quat1.w;

	w = quat1.w*quat2.w - (quat1.x*quat2.x + quat1.y*quat2.y + quat1.z*quat2.z);
}

void CQuaternion::Slerp(const CQuaternion& quatStart, const CQuaternion& quatEnd, float fTheta, float fOOSinTheta, float fInterpValue)
{
	float fAlpha, fBeta;

	if(fTheta == 0.0f)
	{
		Copy(quatEnd);
	}
	else
	{
		if(fTheta > HALF_PI)
		{
			fTheta = PI - fTheta;
			fBeta = CMaths::Sin((1.0f - fInterpValue) * fTheta) * fOOSinTheta;
			fAlpha = -CMaths::Sin(fInterpValue * fTheta) * fOOSinTheta;
		}
		else
		{
			fBeta = CMaths::Sin((1.0f - fInterpValue) * fTheta) * fOOSinTheta;
			fAlpha = CMaths::Sin(fInterpValue * fTheta) * fOOSinTheta;
		}
		
		x = fBeta * quatStart.x + fAlpha * quatEnd.x;
		y = fBeta * quatStart.y + fAlpha * quatEnd.y;
		z = fBeta * quatStart.z + fAlpha * quatEnd.z;
		w = fBeta * quatStart.w + fAlpha * quatEnd.w;
	}
}

void CQuaternion::Slerp(const CQuaternion& quatStart, const CQuaternion& quatEnd, float fInterpValue)
{
	float fTheta, fOOSinTheta;

	InitSlerp(quatStart, quatEnd, &fTheta, &fOOSinTheta);
	Slerp(quatStart, quatEnd, fTheta, fOOSinTheta, fInterpValue);
}
