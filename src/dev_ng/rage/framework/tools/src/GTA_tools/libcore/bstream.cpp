#include "libcore/bstream.h"

#include "system/xtl.h"
#include <stdio.h>

using namespace WinLib;

///////////////////////////////////////////////////////////////////////////////////////////////	
void BinaryStream::printf(const char*,...)
{
}

///////////////////////////////////////////////////////////////////////////////////////////////	
void BinaryStream::readLine(char* p_cRead,uint32 uMaxRead)
{
	char* p_cIndex = p_cRead;

	if(!getPending())
	{
		*p_cRead = '\0';
		return;
	}

	char cRead = readU8();

	//strip any escape characters from the beginning of the sequence

	while(	(cRead == '\n') ||
			(cRead == '\r') ||
			(cRead == '\0'))
	{
		if(getPending() == 0)
		{
			*p_cIndex = '\0';
			return;
		}

		cRead = readU8();
	}

	//keep reading characters till we get to an end of line
	//or we reach the end of the buffer

	while(	(cRead != '\n') && 
			(cRead != '\r') && 
			(uMaxRead > 0) && 
			(getPending() >= 0))
	{
		*p_cIndex = cRead;
		p_cIndex++;
		uMaxRead--;

		if(getPending())
		{
			cRead = readU8();
		}
		else
		{
			break;
		}
	}

	*p_cIndex = '\0';
}

///////////////////////////////////////////////////////////////////////////////////////////////
void BinaryStream::writeLine(const char* p_cWrite,...)
{
	va_list list;
	char output[8192];
	char* p_write = output;
	int iWritten;

	va_start(list,p_cWrite);
	iWritten = vsprintf(output,p_cWrite,list);
	va_end(list);

	Assert(iWritten<8192);

	write((uint8*)p_write, (uint32) strlen(p_write));
	writeU8('\r');
	writeU8('\n');
}

///////////////////////////////////////////////////////////////////////////////////////////////
void BinaryStream::writeU8(uint8 uWrite)
{
	write(&uWrite,1);
}

///////////////////////////////////////////////////////////////////////////////////////////////
uint8 BinaryStream::readU8()
{
	uint8 uRead;

	read(&uRead,1);

	return uRead;
}

///////////////////////////////////////////////////////////////////////////////////////////////
void BinaryStream::writeU32(uint32 uWrite)
{
	write(&uWrite,4);
}

///////////////////////////////////////////////////////////////////////////////////////////////
uint32 BinaryStream::readU32()
{
	uint32 uRead;

	read(&uRead,4);

	return uRead;
}