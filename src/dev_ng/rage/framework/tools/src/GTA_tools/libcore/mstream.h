#ifndef COMMON_MSTREAM_H
#define COMMON_MSTREAM_H

#include "libcore/bstream.h"

namespace WinLib {

///////////////////////////////////////////////////////////////////////////////////////////////
class MemoryStream : public BinaryStream
///////////////////////////////////////////////////////////////////////////////////////////////
{
public:
	MemoryStream(uint32 uSize);
	~MemoryStream();

	uint32 seek(uint32 uOffset,int32 seekOrigin);
	uint32 write(void* p_cWriteFrom,uint32 uiBytes);
	uint32 read(void* p_cReadTo,uint32 uiBytes);
	uint32 getPos();
	uint32 getPending();
	
	void reset();
	void flush();

	uint8* getWrite() { return m_pCurrentWrite; }
	uint8* getRead() { return m_pCurrentRead; }

	uint32 getBufferLeft() { return m_uSize; }
	void advanceWrite(uint32 uAdvance);
	void advanceRead(uint32 uAdvance);

protected:
	uint8* m_pData;
	uint8* m_pCurrentRead;
	uint8* m_pCurrentWrite;
	uint32 m_uSize;
};

} //namespace WinLib {

#endif //COMMON_MSTREAM_H