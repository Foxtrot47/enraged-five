//
// CompressedFloat.h: Compressed float code
//
//
//
//
#ifndef INC_COMPRESSED_FLOAT_H_
#define INC_COMPRESSED_FLOAT_H_

#include "libcore/vector.h"
#include "libcore/vector2d.h"
#include "libcore/quaternion.h"

//#define COMPRESSION_OFF

#ifdef COMPRESSION_OFF
	typedef float CompressedFloat;
#else
	typedef int16 CompressedFloat;
#endif



struct CompressedVector
{
	CompressedFloat x,y,z;
};

struct CompressedVector2D
{
	CompressedFloat x, y;
};

struct CompressedQuat
{
	CompressedFloat x,y,z,w;
};



//
// borders for CompressedFloat numbers:
//
// s.8.7 = 16 bits (1 bit for sign, 8 bits for whole part, 7 bits for fractional part):
//
#define COMPRESSED_TRANSL_FLOAT_MAX			(255.0f)
#define COMPRESSED_TRANSL_FLOAT_MIN			(-COMPRESSED_TRANSL_FLOAT_MAX)
#define COMPRESSED_TRANSL_FLOAT_MAX_STR		"255"			// used for printing errors, warnings, etc.
#define COMPRESSED_TRANSL_FLOAT_MIN_STR		"-255"

inline bool CanCompressTranslationFloat(const float f)
{
	if((f >= COMPRESSED_TRANSL_FLOAT_MAX) || (f <= COMPRESSED_TRANSL_FLOAT_MIN))
	{
		return false;
	}

	return true;
}

//
// CompressTranslationFloat: return floating point value as a 
//
inline CompressedFloat CompressTranslationFloat(const float f)
{
#ifdef COMPRESSION_OFF
	return f;
#else
	return (CompressedFloat)(f*128.0f);
#endif	
}

//
// name:		CompressTranslationFloatConsistent
// description: return floating point value as a compressed value which consistently
//				rounds down
//
inline CompressedFloat CompressTranslationFloatConsistent(const float f)
{
#ifdef COMPRESSION_OFF
	return f;
#else
	CompressedFloat i = (CompressedFloat)(f*128.0f + 0.5f);
	if(i<0)
		i--;
	return i;
#endif	
}


//
// DecompressTranslationFloat: return floating point value from a compress value
//
inline float DecompressTranslationFloat(const CompressedFloat i)
{
#ifdef COMPRESSION_OFF
	return i;
#else
	return (i*(1.0f/128.0f));
#endif
}

inline bool CanCompressTranslationVector(const CVector& v)
{
	if(!CanCompressTranslationFloat(v.x))
	{
		return false;
	}

	if(!CanCompressTranslationFloat(v.y))
	{
		return false;
	}

	if(!CanCompressTranslationFloat(v.z))
	{
		return false;
	}

	return true;
}


//
//
//
//
inline void CompressTranslationVector(CompressedVector& cv, const CVector& v)
{
	cv.x = CompressTranslationFloat(v.x);
	cv.y = CompressTranslationFloat(v.y);
	cv.z = CompressTranslationFloat(v.z);
}

inline void CompressTranslationVectorConsistent(CompressedVector& cv, const CVector& v)
{
	cv.x = CompressTranslationFloatConsistent(v.x);
	cv.y = CompressTranslationFloatConsistent(v.y);
	cv.z = CompressTranslationFloatConsistent(v.z);
}


//
//
//
//
inline void DecompressTranslationVector(CVector& v, const CompressedVector& cv)
{
	v.x = DecompressTranslationFloat(cv.x);
	v.y = DecompressTranslationFloat(cv.y);
	v.z = DecompressTranslationFloat(cv.z);
}


//
//
//
//

inline bool CanCompressTranslationVector2D(const CVector2D& v)
{
	if(!CanCompressTranslationFloat(v.x))
	{
		return false;
	}
	if(!CanCompressTranslationFloat(v.y))
	{
		return false;
	}

	return true;
}

inline void CompressTranslationVector2D(CompressedVector2D& cv, const CVector2D& v)
{
	cv.x = CompressTranslationFloat(v.x);
	cv.y = CompressTranslationFloat(v.y);
}

inline void CompressTranslationVector2DConsistent(CompressedVector2D& cv, const CVector2D& v)
{
	cv.x = CompressTranslationFloatConsistent(v.x);
	cv.y = CompressTranslationFloatConsistent(v.y);
}


//
//
//
//
inline void DecompressTranslationVector2D(CVector2D& v, const CompressedVector2D& cv)
{
	v.x = DecompressTranslationFloat(cv.x);
	v.y = DecompressTranslationFloat(cv.y);
}








//
// borders for CompressedSmallFloat numbers (-32, 32):
//
// s.5.10 = 16 bits (1 bit for sign, 5 bits for whole part, 10 bits for fractional part):
//
#define COMPRESSED_SMALL_TRANSL_FLOAT_MAX			(32.0f)
#define COMPRESSED_SMALL_TRANSL_FLOAT_MIN			(-COMPRESSED_SMALL_TRANSL_FLOAT_MAX)
#define COMPRESSED_SMALL_TRANSL_FLOAT_MAX_STR		"32"			// used for printing errors, warnings, etc.
#define COMPRESSED_SMALL_TRANSL_FLOAT_MIN_STR		"-32"

//
// CompressSmallTranslationFloat: return floating point value as a 
//

inline bool CanCompressSmallTranslationFloat(const float f)
{
	if((f >= COMPRESSED_SMALL_TRANSL_FLOAT_MAX) || (f <= COMPRESSED_SMALL_TRANSL_FLOAT_MIN))
	{
		return false;
	}

	return true;
}

inline CompressedFloat CompressSmallTranslationFloat(const float f)
{
#ifdef COMPRESSION_OFF
	return f;
#else
	return (CompressedFloat)(f*1024.0f);
#endif	
}

//
// name:		CompressSmallTranslationFloatConsistent
// description: return floating point value as a compressed value which consistently
//				rounds down
//
inline CompressedFloat CompressSmallTranslationFloatConsistent(const float f)
{
#ifdef COMPRESSION_OFF
	return f;
#else
	CompressedFloat i = (CompressedFloat)(f*1024.0f + 0.5f);
	if(i<0)
		i--;
	return i;
#endif	
}


//
// DecompressTranslationFloat: return floating point value from a compress value
//
inline float DecompressSmallTranslationFloat(const CompressedFloat i)
{
#ifdef COMPRESSION_OFF
	return i;
#else
	return (i*(1.0f/1024.0f));
#endif
}

inline bool CanCompressAnimTranslationVector(const CVector& v)
{
	if(!CanCompressSmallTranslationFloat(v.x))
	{
		return false;
	}

	if(!CanCompressSmallTranslationFloat(v.y))
	{
		return false;
	}

	if(!CanCompressSmallTranslationFloat(v.z))
	{
		return false;
	}

	return true;
}

inline void CompressAnimTranslationVector(CompressedVector& cv, const CVector& v)
{
	cv.x = CompressSmallTranslationFloat(v.x);
	cv.y = CompressSmallTranslationFloat(v.y);
	cv.z = CompressSmallTranslationFloat(v.z);
}
inline void DecompressAnimTranslationVector(CVector& v, const CompressedVector& cv)
{
	v.x = DecompressSmallTranslationFloat(cv.x);
	v.y = DecompressSmallTranslationFloat(cv.y);
	v.z = DecompressSmallTranslationFloat(cv.z);
}
//
//
//
//
inline void CompressSmallTranslationVector2D(CompressedVector2D& cv, const CVector2D& v)
{
	cv.x = CompressSmallTranslationFloat(v.x);
	cv.y = CompressSmallTranslationFloat(v.y);
}


inline void CompressSmallTranslationVector2DConsistent(CompressedVector2D& cv, const CVector2D& v)
{
	cv.x = CompressSmallTranslationFloatConsistent(v.x);
	cv.y = CompressSmallTranslationFloatConsistent(v.y);
}


//
//
//
//
inline void DecompressSmallTranslationVector2D(CVector2D& v, const CompressedVector2D& cv)
{
	v.x = DecompressSmallTranslationFloat(cv.x);
	v.y = DecompressSmallTranslationFloat(cv.y);
}











//
// borders for CompressedNormalFloat numbers:
//
// s.3.12 = 16 bits (1 bit for sign, 3 bits for whole part, 12 bits for fract part):
//
#define COMPRESSED_NORMAL_FLOAT_MAX			(7.0f)
#define COMPRESSED_NORMAL_FLOAT_MIN			(-COMPRESSED_NORMAL_FLOAT_MAX)
#define COMPRESSED_NORMAL_FLOAT_MAX_STR		"7"				// used for printing errors, warnings, etc.
#define COMPRESSED_NORMAL_FLOAT_MIN_STR		"-7"


//
// CompressKeyframeTimeFloat: return floating point value as a 
//
inline CompressedFloat CompressKeyframeTimeFloat(const float f)
{
	return (CompressedFloat)((f*60.0f)+0.5f);
}

inline float DecompressKeyframeTimeFloat(const CompressedFloat f)
{
	return (f*(1.0f/60.0f));
}


inline bool CanCompressNormalFloat(const float f)
{
	if((f >= COMPRESSED_NORMAL_FLOAT_MAX) || (f <= COMPRESSED_NORMAL_FLOAT_MIN))
	{
		return false;
	}

	return true;
}

//
// CompressNormalFloat: return floating point value as a 
//
inline CompressedFloat CompressNormalFloat(const float f)
{
#ifdef COMPRESSION_OFF
	return f;
#else
	return (CompressedFloat)(f*4096.0f);
#endif
}

//
// DecompressNormalFloat: return floating point value from a compress value
//
inline float DecompressNormalFloat(const CompressedFloat i)
{
#ifdef COMPRESSION_OFF
	return i;
#else
	return (i* (1.0f/4096.0f));
#endif	
}




//
// borders for CompressedUVMapFloat numbers:
//
// s.3.12 (1 bit for sign, 3 bits for whole part, 12 bits for fract part):
//
#define COMPRESSED_UV_FLOAT_MAX			(7.0f)
#define COMPRESSED_UV_FLOAT_MIN			(-COMPRESSED_UV_FLOAT_MAX)
#define COMPRESSED_UV_FLOAT_MAX_STR		"7"				// used for printing errors, warnings, etc.
#define COMPRESSED_UV_FLOAT_MIN_STR		"-7"

inline bool CanCompressUVMapFloat(const float f)
{
	if((f >= COMPRESSED_UV_FLOAT_MAX) || (f <= COMPRESSED_UV_FLOAT_MIN))
	{
		return false;
	}

	return true;
}

//
//
// used for de/compressing UV coords in CustomCVBuildingPipe:
//
inline CompressedFloat CompressUVMapFloat(const float f)
{
#ifdef COMPRESSION_OFF
	return f;
#else
	return (CompressedFloat)(f*4096.0f);
#endif
}

//
//
//
//
inline float DecompressUVMapFloat(const CompressedFloat i)
{
#ifdef COMPRESSION_OFF
	return i;
#else
	return (i * (1.0f/4096.0f));
#endif	
}



inline bool CanCompressQuaternion(const CQuaternion& q)
{
	if(!CanCompressNormalFloat(q.x))
	{
		return false;
	}

	if(!CanCompressNormalFloat(q.y))
	{
		return false;
	}

	if(!CanCompressNormalFloat(q.z))
	{
		return false;
	}

	if(!CanCompressNormalFloat(q.w))
	{
		return false;
	}

	return true;
}


//
//
//
//
inline void CompressQuaternion(CompressedQuat& cq, const CQuaternion& q)
{
	cq.x = CompressNormalFloat(q.x);
	cq.y = CompressNormalFloat(q.y);
	cq.z = CompressNormalFloat(q.z);
	cq.w = CompressNormalFloat(q.w);
}

//
//
//
//
inline void DecompressQuaternion(CQuaternion& q, const CompressedQuat& cq)
{
	q.x = DecompressNormalFloat(cq.x);
	q.y = DecompressNormalFloat(cq.y);
	q.z = DecompressNormalFloat(cq.z);
	q.w = DecompressNormalFloat(cq.w);
}


//
//
//
//
//
inline bool operator==(const CompressedVector& a, const CompressedVector& b)
{
	if(a.x == b.x && a.y == b.y && a.z == b.z)
		return true;
	return false;	
}



#endif//INC_COMPRESSED_FLOAT_H_...

