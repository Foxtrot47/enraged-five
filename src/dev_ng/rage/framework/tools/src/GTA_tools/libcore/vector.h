// Title	:	Vector.h
// Author	:	Richard Jobling
// Started	:	19/10/98

#ifndef _VECTOR_H_
#define _VECTOR_H_

#include "libcore/Maths.h"

class CMatrix;

//////////////////////////////////////////////////////////////////////////////
class CVector //: public RwV3d
//////////////////////////////////////////////////////////////////////////////
{
public:

	float x,y,z;

public:
	CVector() {}
	CVector(float x, float y, float z) { this->x = x; this->y = y; this->z = z; }

	void Normalise();

	float Magnitude() const {return CMaths::Sqrt(MagnitudeSqr());}
	float Magnitude2D() const {return CMaths::Sqrt(x*x + y*y);}
	float MagnitudeSqr() const;

	friend float DotProduct(const CVector& v1, const CVector& v2) { return (v1.x * v2.x + v1.y * v2.y + v1.z * v2.z); }
	friend CVector CrossProduct(const CVector& v1, const CVector& v2);
	friend CVector Multiply3x3(const class CMatrix& m, const CVector& v);
	friend CVector Multiply3x3(const CVector& v, const class CMatrix& m);

	CVector& operator+=(const CVector& v);
	CVector& operator-=(const CVector& v);
	CVector& operator*=(const CVector& v);
	CVector& operator*=(float f);
	CVector& operator/=(float f);

	float& operator[](int32 iIndex)
	{
		switch(iIndex)
		{
		case 0:
			return x;
		case 1:
			return y;
		case 2:
			return z;
		}

		return x;
	}

	//-----------------------------------------------.
	//Some functions to avoid expensive construction and return of temporaries
	//-----------------------------------------------
	void FromAdd(const CVector& a, const CVector& b)
	{
	    x=a.x+b.x;
	    y=a.y+b.y;
	    z=a.z+b.z;
	}
	
	void FromSubtract(const CVector& a, const CVector& b)
	{
	    x=a.x-b.x;
	    y=a.y-b.y;
	    z=a.z-b.z;
	}
	
	void FromMultiply(const CVector& a, const float f)
	{
	    x=a.x*f;
	    y=a.y*f;
	    z=a.z*f;
	}
	
	void FromDivide(const CVector& a, const float f)
	{
	    const float g=1.0f/f;
	    x=a.x*g;
	    y=a.y*g;
	    z=a.z*g;
	}
	
	void FromCross(const CVector& a, const CVector& b)
	{
	    x=a.y*b.z-a.z*b.y;
	    y=a.z*b.x-a.x*b.z;
	    z=a.x*b.y-a.y*b.x;
	}
	
	void FromUnitCross(const CVector& a, const CVector& b)
	{
	    x=a.y*b.z-a.z*b.y;
	    y=a.z*b.x-a.x*b.z;
	    z=a.x*b.y-a.y*b.x;
	    const float f=1.0f/CMaths::Sqrt(x*x+y*y+z*z);
	    x*=f;
	    y*=f;
	    z*=f;
	}
	
	void FromMultiply(const CMatrix& m, const CVector& v);	
	void FromMultiply3X3(const CMatrix& m, const CVector& v);	
	
	void Set(const float xx, const float yy, const float zz)
	{
		x=xx;
		y=yy;
		z=zz;
	}
	

	//-----------------------------------------------	
	
	friend CVector operator-(const CVector& v) { return CVector(-v.x, -v.y, -v.z); }
	friend CVector operator+(const CVector& v1, const CVector& v2) { return CVector(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z); }
	friend CVector operator-(const CVector& v1, const CVector& v2) { return CVector(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z); }
	friend CVector operator*(float f, const CVector& v) { return CVector(f * v.x, f * v.y, f * v.z); }
	friend CVector operator*(const CVector& v, float f) { return (f * v); }
	friend CVector operator*(const CVector& v1, const CVector& v2) {return CVector(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z); }
	friend CVector operator*(const class CMatrix& m, const CVector& v);
	friend CVector operator/(const CVector& v, float f) { return (v * (1.0f / f)); }

	friend bool operator==(const CVector& v1, const CVector& v2) { return ((v1.x == v2.x) && (v1.y == v2.y) && (v1.z == v2.z)); }
	friend bool operator!=(const CVector& v1, const CVector& v2) { return !(v1 == v2); }
};

class CVector4 : public CVector
{
public:
	CVector4() {}
	CVector4(float x, float y, float z):CVector(x,y,z) {;}

	CVector operator=(const CVector& v);

public:
	float w;
};

inline CVector CVector4::operator=(const CVector& v)
{
	x = v.x;
	y = v.y;
	z = v.z;

	return *this;
}

inline CVector& CVector::operator+=(const CVector& v)
{
	x += v.x;
	y += v.y;
	z += v.z;

	return *this;
}

inline CVector& CVector::operator-=(const CVector& v)
{
	x -= v.x;
	y -= v.y;
	z -= v.z;

	return *this;
}

inline CVector& CVector::operator*=(float f)
{
	x *= f;
	y *= f;
	z *= f;

	return *this;
}

inline CVector& CVector::operator*=(const CVector& v )
{
	x *= v.x;
	y *= v.y;
	z *= v.z;

	return *this;
}

inline CVector& CVector::operator/=(float f)
{
	float oof = 1.0f / f;
	x *= oof;
	y *= oof;
	z *= oof;

	return *this;
}

inline float CVector::MagnitudeSqr() const
{
	return (x*x + y*y + z*z);
}

#endif
