#include "libcore/bstream.h"
#include "libcore/xml.h"
#include "libcore/exception.h"

using namespace WinLib;

///////////////////////////////////////////////////////////////////////////////////////////////
XmlElement::~XmlElement()
{
	std::list<XmlElement*>::iterator begin = children.begin();
	std::list<XmlElement*>::iterator end = children.end();

	while(begin != end)
	{
		delete *begin;

		begin++;
	}

	children.clear();
}

///////////////////////////////////////////////////////////////////////////////////////////////
XmlElement* XmlElement::getChild(std::string& r_name)
{
	std::list<XmlElement*>::iterator begin = children.begin();
	std::list<XmlElement*>::iterator end = children.end();

	while(begin != end)
	{
		if((*begin)->name == r_name)
		{
			return (*begin);
		}

		begin++;
	}

	return NULL;
}

///////////////////////////////////////////////////////////////////////////////////////////////
XmlElement* XmlParse::read(BinaryStream& r_sRead)
{
	mp_elemRoot = NULL;
	mp_elemCurr = NULL;
	mp_sRead = &r_sRead;
	m_readBuffer = std::string("");

	readHeader();

	bool bHitTag = false;

	while(mp_sRead->getPending())
	{
		uint8 uRead;

		while(mp_sRead->getPending())
		{
			uRead = mp_sRead->readU8();

			if(uRead == '<')
			{
				bHitTag = true;
				break;
			}

			m_readBuffer.push_back(uRead);
		}

		if(bHitTag)
		{
			readTag();
			bHitTag = false;
		}
	}

	return mp_elemRoot;
}

///////////////////////////////////////////////////////////////////////////////////////////////
void XmlParse::write(BinaryStream& r_sWrite,const XmlElement& r_xmlElement)
{
	mp_sRead = &r_sWrite;

	mp_sRead->printf("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n");
	
	writeElement(r_xmlElement,0);
}

///////////////////////////////////////////////////////////////////////////////////////////////
void XmlParse::writeTabs(uint32 uTabs)
{
	while(uTabs>0)
	{
		mp_sRead->printf("  ");
		uTabs--;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////
void XmlParse::writeElement(const XmlElement& r_xmlElement,uint32 uTabLevel)
{
	writeTabs(uTabLevel);
	mp_sRead->printf("<%s",r_xmlElement.name.c_str());

	std::map<std::string,std::string>::const_iterator beginParam = r_xmlElement.params.begin();
	std::map<std::string,std::string>::const_iterator endParam = r_xmlElement.params.end();

	while(beginParam != endParam)
	{
		mp_sRead->printf(" ");	
		mp_sRead->printf(beginParam->first.c_str());
		mp_sRead->printf("=\"");
		mp_sRead->printf(beginParam->second.c_str());
		mp_sRead->printf("\"");

		beginParam++;
	}

	if(r_xmlElement.children.size() > 0)
	{
		mp_sRead->printf(">");
		mp_sRead->printf("\n");

		std::list<XmlElement*>::const_iterator begin = r_xmlElement.children.begin();
		std::list<XmlElement*>::const_iterator end = r_xmlElement.children.end();

		while(begin != end)
		{
			writeElement(*(*begin),uTabLevel + 1);

			begin++;
		}

		writeTabs(uTabLevel);
		mp_sRead->printf("</%s>\n",r_xmlElement.name.c_str());
	}
	else
	{
		if(r_xmlElement.value == "")
		{
			mp_sRead->printf("/>\n");
		}
		else
		{
			mp_sRead->printf(">");
			mp_sRead->printf("%s",r_xmlElement.value.c_str());
			mp_sRead->printf("</%s>\n",r_xmlElement.name.c_str());
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////
void XmlParse::validateChar(uint8 uChar,uint8 uCheck)
{
	if(uChar != uCheck)
	{
		throw Exception("invalid char");
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////
void XmlParse::readHeader()
{
	uint8 uRead = mp_sRead->readU8();
	validateChar(uRead,'<');

	uRead = mp_sRead->readU8();
	validateChar(uRead,'?');

	uRead = mp_sRead->readU8();

	while(uRead != '?')
	{
		uRead = mp_sRead->readU8();
	}

	uRead = mp_sRead->readU8();
	validateChar(uRead,'>');
}

///////////////////////////////////////////////////////////////////////////////////////////////
void XmlParse::readTag()
{
	bool bEndTag = false;
	uint8 uRead = mp_sRead->readU8();

	//is this an end tag?

	if(uRead == '/')
	{
		bEndTag = true;
		uRead = mp_sRead->readU8();
	}

	//get the tag name

	std::string elementName("");
	uint8 uLast;

	while((uRead != ' ') && (uRead != '>'))
	{
		elementName.push_back(uRead);
		uLast = uRead;
		uRead = mp_sRead->readU8();
	}

	//read until the end of the tag
	if(bEndTag)
	{
		if(!mp_elemCurr)
		{
			throw new Exception("current element is null");
		}
	
		if(mp_elemCurr->name != elementName)
		{
			throw new Exception("name mismatch in tags");
		}

		//put text info into element

		mp_elemCurr->value = m_readBuffer;
		m_readBuffer = "";

		//find the end of the tag

		while(uRead != '>')
		{
			uRead = mp_sRead->readU8();
		}

		mp_elemCurr = mp_elemCurr->p_parent;
	}
	else
	{
		//create the element

		XmlElement* p_elemStore = mp_elemCurr;
		mp_elemCurr = new XmlElement;
		mp_elemCurr->p_parent = p_elemStore;
		mp_elemCurr->name = std::string(elementName);

		if(mp_elemRoot)
		{
			if(!p_elemStore)
			{
				throw new Exception("more than one root object");
			}

			p_elemStore->children.push_back(mp_elemCurr);
		}
		else
		{
			mp_elemRoot = mp_elemCurr;
		}

		//read in the parameters for a beginning tag

		uint32 uValState = 0;
		std::string paramName("");
		std::string paramValue("");

		while(uRead != '>')
		{
			switch(uValState)
			{
			case 0:

				if(uRead == '=')
				{
					uValState = 1;
				}
				else if((uRead != ' ') && (uRead != '\t'))
				{
					paramName.push_back(uRead);
				}

				break;
			case 1:

				if(uRead != '"')
				{
					throw new Exception("value not present");
				}

				uValState++;

				break;

			case 2:

				if(uRead == '"')
				{
					uValState = 0;

					if(paramName != std::string(""))
					{
						mp_elemCurr->params.insert(std::map<std::string,std::string>::value_type(paramName,paramValue));
					}

					paramName = std::string("");
					paramValue = std::string("");
				}
				else
				{
					paramValue.push_back(uRead);
				}
				break;
			}

			uLast = uRead;
			uRead = mp_sRead->readU8();
		}

		//if this is an end tag then go back up the hierarchy

		if(uLast == '/')
		{
			mp_elemCurr = mp_elemCurr->p_parent;
		}

		m_readBuffer = "";
	}
}