#ifndef COMMON_FSTREAM_H
#define COMMON_FSTREAM_H

#include "libcore/bstream.h"

namespace WinLib {

	class FileStream;

	typedef WinLib::FileStream* (*CreateStream)();

	///////////////////////////////////////////////////////////////////////////////////////////////
	class FileStream : public BinaryStream
		///////////////////////////////////////////////////////////////////////////////////////////////
	{
	public:
		static WinLib::FileStream* m_cbCreateStream();
		static void SetCreateStream(CreateStream cbCreateStream);

		virtual bool open(const char* p_cFilename,const char* p_cOpenFlags) = 0;
		virtual void close() = 0;

		virtual bool createDirectory(const char* p_cDirPath) = 0;
		virtual bool isReadOnly(const char* p_cFilePath) = 0;
		virtual bool isOpen() = 0;
		virtual bool deleteFile(const char* p_cFilePath) = 0;
		virtual bool moveFile(const char* p_cFilePathSource,const char* p_cFilePathDest) = 0;
		virtual bool getTempFilename(const char* p_cPath,const char* p_cPrefix,uint32 Unique,char* p_cOutput) = 0;
	};

} //namespace WinLib {

#endif //COMMON_FSTREAM_H