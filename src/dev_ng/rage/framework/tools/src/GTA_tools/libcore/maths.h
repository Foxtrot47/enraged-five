// Title	:	Maths.h
// Author	:	Richard Jobling
// Started	:	19/10/98
//
//	15/04/2001	- Andrzej:	CMaths::Abs() added;
//
//
//
#ifndef INC_MATHS_H_
#define INC_MATHS_H_

#include <math.h>

#undef PI

#define PI 			(3.14159265359f)
#define HALF_PI 	(PI/2.0f)
#define QUARTER_PI 	(PI/4.0f)
#define EIGHTH_PI 	(PI/8.0f)
#define THIRD_PI	(PI/3.0f) 
#define TWO_PI 		(6.28318530718f)
#define SQRTTWO		(1.414213562f)

#define DEGTORAD(a) ((a) * PI / 180.0f)
#define RADTODEG(a) ((a) * 180.0f / PI)

#define MATHS_TABLE_SIZE	1024

class CMaths
{
private:
//	static float ms_sinTable[MATHS_TABLE_SIZE+1];
//	static float ms_acosTable[MATHS_TABLE_SIZE+1];
//	static float ms_atanTable[MATHS_TABLE_SIZE+1];
	
public:
//	static void InitMathsTables();
	
//	static float QuickSin(float f);
//	static float QuickCos(float f);
//	static float QuickACos(float f);
//	static float QuickASin(float f);
//	static float QuickATan(float f);
//	static float QuickATan2(float y, float x);

//	static inline float Sin(float f) { return QuickSin(f); }
//	static inline float Cos(float f) { return QuickCos(f); }
	static inline float Sin(float f) { return sinf(f); }
	static inline float Cos(float f) { return cosf(f); }
	static inline float Tan(float f) { return tanf(f); }
	static inline float ASin(float f) { return asinf(f); }
	static inline float ACos(float f) { return acosf(f); }
	static inline float ATan(float x) { return atanf(x); }
	static inline float ATan2(float y, float x) { return atan2f(y, x); }
//	static inline float ASin(float f) { return QuickASin(f); }
//	static inline float ACos(float f) { return QuickACos(f); }
//	static inline float ATan(float x) { return QuickATan(x); }
//	static inline float ATan2(float y, float x) { return QuickATan2(y, x); }

	static inline float Sqr(float f) { return (f * f); }
	static float Sqrt(float f);
	static inline float RecipSqrt(float f) {return RecipSqrt(1.0f, f);}
	static float RecipSqrt(float n, float d);

	static inline float Pow(float x, float y) { return powf(x,  y); }
	
	static inline float Ceil(float f) { return ceilf(f); }
	static inline float Floor(float f) { return floorf(f); }
	static inline float Abs(float f);
	static inline float Min(float f1, float f2);
	static inline float Max(float f1, float f2);
};

inline float CMaths::Sqrt(float f) 
{
	return sqrtf(f);
}

inline float CMaths::RecipSqrt(float n, float d) 
{
	return (n /sqrtf(d));
}

inline float CMaths::Abs(float f)
{
	return fabsf(f);
}

inline float CMaths::Min(float v1, float v2) 
{
	if (v1 < v2)
		return v1;
	else
		return v2;
}

inline float CMaths::Max(float v1, float v2) 
{
	if (v1 > v2)
		return v1;
	else
		return v2;
}

#endif // INC_MATHS_H_
