#include "libcore/rect.h"

///////////////////////////////////////////////////////////////////////////////
CRect::CRect()
{
}

///////////////////////////////////////////////////////////////////////////////
CRect::CRect(float b,float l,float t,float r):
	left(l),
	top(t),
	right(r),
	bottom(b)
{
}

///////////////////////////////////////////////////////////////////////////////
void CRect::init(CVector2D& vecInit)
{
	left = vecInit.x;
	top = vecInit.y;
	right = vecInit.x;
	bottom = vecInit.y;
}

///////////////////////////////////////////////////////////////////////////////
void CRect::addPoint(CVector2D& vecAdd)
{
	if(vecAdd.x < left)
	{
		left = vecAdd.x;
	}
	if(vecAdd.x > right)
	{
		right = vecAdd.x;
	}

	if(vecAdd.y < bottom)
	{
		bottom = vecAdd.y;
	}
	if(vecAdd.y > top)
	{
		top = vecAdd.y;
	}
}

///////////////////////////////////////////////////////////////////////////////
bool CRect::isInside(const CVector2D& vecCheck) const
{
	if((vecCheck.x <= left) || (vecCheck.x > right) || (vecCheck.y <= bottom) || (vecCheck.y > top))
	{
		return false;
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////
bool CRect::doesIntersect(const CRect& r_rect) const
{
	if(left > r_rect.right || right < r_rect.left || bottom > r_rect.top || top < r_rect.bottom)
		return false;

	return true;	
}