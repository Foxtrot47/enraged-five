/***************************************************
 * tfile.h                                         *
 *                                                 *
 * declarations for file operations for dino tools *
 *                                                 *
 * KRH 26/06/95                                    *
 *                                                 *
 * (c) 1995 DMA Design Ltd                         *
 ***************************************************/

#ifndef TFILE_H
#define TFILE_H

#define FILE_LIST_MAX (10000)

extern UInt16 swap_word ( UInt16 word );
extern UInt32 swap_dword ( UInt32 dword );
extern Char *change_ext ( Char *src, Char *ext );
extern Char last_letter ( Char *filename );
extern Char *change_last_letter ( Char *filename, Char last_letter );
extern Bool32 file_exists(Char *filename);
extern Int32 file_size(FILE *fp);
extern void file_read ( Char *buffer, Int32 len, FILE *f );
extern Int32 file_len ( Char *infile );
extern void load_file ( Char *filename, Char **buffer, Int32 *size );
extern void load_file_f ( Char *filename, Char *buffer, Int32 *size );
extern void save_file ( Char *buffer, Int32 size, Char *filename);
extern void save_file_append ( Char *buffer, Int32 size, Char *filename);
extern void append_file ( FILE *f, Char *buffer, Int32 size );
extern void copy_file ( FILE *f, Char *filename );
extern Int32 find_all_files (Char *file_list[], Char *match);
extern void load_lbm_file ( Char *filename, Char **lbm_buffer, Int32 *lbm_width, Int32 *lbm_height );
extern void load_lbm_pal ( Char *filename, Char **pal_buffer, Int32 *pal_size );
extern void get_grid_size ( Char *pic_buffer, Int32 pic_width, Int32 pic_height, Int32 *grid_width, Int32 *grid_height);
extern void pic2raw (Char *pic_buffer, Int32 pic_width, Int32 pic_height, Int32 spr_width, Int32 spr_height, Char **raw_buffer, Int32 *raw_length);
extern void pic2rawpic (Char *pic_buffer, Int32 pic_width, Int32 pic_height, Int32 spr_width, Int32 spr_height, Char **raw_buffer);
extern void pic2raw_scale (Char *pic_buffer, Int32 pic_width, Int32 pic_height, Int32 spr_width, Int32 spr_height, Char **raw_buffer, Int32 *raw_length, Int32 scale);

extern void find_lbm_pal ( Char **buffer, Char **pal_buffer, Int32 *pal_size );

extern void open_save_file ( Char *filename );
extern void write_file ( Char *buffer, Int32 size );
extern void close_file ();

extern void open_save_file2 ( Char *filename );
extern void write_file2 ( Char *buffer, Int32 size );
extern void close_file2 ();

extern void open_load_file ( Char *filename );
extern Bool8 read_file (Char *buffer, Int32 size);
extern Bool8 read_file_a (Char **buffer, Int32 size);
extern void skip_file (Int32 len);

#endif