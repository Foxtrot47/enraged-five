/********************************
 * terror.h                     *
 *                              *
 * error control for dino tools *
 *                              *
 * KRH 25/05/95                 *
 *                              *
 * (c) 1995 DMA Design Ltd      *
 ********************************/

#ifndef TERROR_H
#define TERROR_H

// error codes

#define	A_OK			(0)
#define	ERR_OPEN_INPUT	(1)
#define ERR_OPEN_OUTPUT	(2)		
#define ERR_CLOSE		(3)
#define ERR_READ		(4)
#define ERR_WRITE		(5)
#define ERR_FTELL		(6)
#define ERR_FSEEK		(7)
#define ERR_MEMORY		(8)
#define ERR_DELTA_WHOLE (9)
#define ERR_FILE_LEN	(10)
#define ERR_MAX_DELTAS	(11)
#define ERR_MAX_DELTA_SIZE (12)
#define ERR_SPRITE_WHOLE (13)
#define ERR_MAX_SPRITES  (14)
#define ERR_DIR_MATCH	(15)
#define ERR_ALLOC_DOS_ZERO (16)
#define ERR_ALLOC_DOS_NOT16 (17)
#define ERR_ALLOC_DOS_MEM	(18)
#define ERR_FREE_DOS_MEM	(19)
#define ERR_INT_SIM			(20)
#define ERR_VESA_SUPPORT	(21)	
#define ERR_VESA_FAIL		(22)
#define ERR_INT_31			(23)
#define ERR_LBM_HEADER		(24)
#define ERR_NOT_DPE			(25)
#define ERR_LBM_BITMAP		(26)
#define ERR_LBM_BITMAP_LENGTH (27)
#define ERR_LBM_COMPR		(28)
#define ERR_INVALID_GRID	(29)
#define ERR_WRITE_ZERO		(30)
#define ERR_LBM_PAL			(31)
#define ERR_OPEN_LOG		(32)
#define ERR_CLOSE_LOG		(33)
#define ERR_WRITE_LOG		(34)
#define ERR_NO_CMP			(35)
#define ERR_MULT_CMP		(36)
#define ERR_BLOCK_OVERFLOW	(37)
#define ERR_COL_OVERFLOW	(38)
#define ERR_STUFF_OVERFLOW	(39)
#define ERR_SPRITE_TOO_BIG	(40)
#define ERR_VERSION			(41)
#define ERR_BRACKET			(42)
#define ERR_ROUTE_ALREADY	(43)
#define ERR_GRID_WIDTH_8	(44)
#define ERR_INVALID_WIDTH	(45)
#define ERR_INVALID_REMAP	(46)
#define ERR_LBM_PAL_SIZE	(47)
#define ERR_CASE			(48)
#define ERR_TOO_MANY_TILES	(49)
#define ERR_STRING_LENGTH   (50)
#define ERR_TOO_MANY_PALS	(51)
#define ERR_GRAPHICS_OVERFLOW (52)
#define ERR_MAX_DELTAS_SIZE	(53)
#define ERR_TOO_MANY_DELTAS_SPRITE (54)
#define ERR_INVALID_HEIGHT (55)
#define ERR_TOO_MANY_FILES (56)
#define ERR_FILE_TYPE		(57)
#define ERR_INVALID_SIZE	(58)
#define ERR_FILE_REMOVE		(59)
#define ERR_FILE_RENAME		(60)
#define ERR_KEY_SIZE		(61)
#define ERR_KEY_COUNT		(62)
#define ERR_TEXT_SIZE		(63)
#define ERR_NO_WHITESPACE_BEFORE_HEADER (64)
#define ERR_DUPLICATE_KEYS	(65)
#define ERR_COMMENT_BRACKET (66)
#define ERR_UNSUPPORTED_TGA_FORMAT (67)
#define ERR_UNMATCHED_BRACKET		(68)
#define ERR_NESTED_COMMENTS			(69)
#define ERR_TILDE_ERROR				(70)
#define ERR_WORD_TOO_LONG			(71)
#define ERR_UNKNOWN_MISSION_NAME		(72)
#define ERR_TOO_MANY_KEYS			(73)
#define ERR_MISSION_HAS_CHANGED		(74)
#define ERR_TOO_MANY_MISSION_FILES	(75)

extern void error (Int32 error_number);
extern void error_str (Int32 error_number, Char *errstr);
extern void open_log (void);
extern void close_log (void);
extern void lprint ( Char *str );
extern void loprint ( Char *str );

#endif