#include "dma.h"

#define NUMBER_OF_KEYS	(4000)

#define MAX_MISSION_FILES	(150)
#define MAX_TEXT_LABELS_IN_MISSION_FILE	(150)

struct KeyData
{
	char KeyString[9];
	Bool8 bInMainScript;
	Bool8 bOnlyInOneMissionScript;
};

struct TextInMissionFile
{
	Int16 TextLabelIndex[MAX_TEXT_LABELS_IN_MISSION_FILE];
	UInt16 NumberOfLabels;
};

	