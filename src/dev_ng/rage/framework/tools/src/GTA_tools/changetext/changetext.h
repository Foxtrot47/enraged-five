#include "dma.h"

#define NUMBER_OF_KEYS	(4000)

#define MISSION_NAME_LENGTH	(8)

#define MAX_MISSION_FILES	(150)

struct KeyData
{
	char KeyString[9];
	char MissionString[MISSION_NAME_LENGTH];
	Bool8 bOccursInTextFile;
};

struct MissionFileName
{
	Char FileName[MISSION_NAME_LENGTH];
	Bool8 bKeysOccurInTextFile;
};