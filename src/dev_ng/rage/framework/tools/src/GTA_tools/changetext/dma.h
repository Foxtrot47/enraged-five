/*
 * dma.h
 * _____
 *
 * see - Rules and Guidelines for C Style at DMA Design
 *	 rev 2.2 - 2/8/95
 *
 * v1.0 by AF (PC/PSX/U64)
 * v1.1 by AF Added #ifndef for CHAR for PC Visual C++ windows
 * v1.2 by AF Added 64 bit types (Int64, UInt64, Fix64, UFix64, Bool64)
 * v1.3 by AF Added 16 bit character types (Char16). Removed PCDOS_DMA and PCWIN_DMA
 *
 * copyright DMA Design Ltd 1995
 *
 *   When using this header file one of the following must be defined PSX_DMA,
 * PC_DMA, SATURN_DMA or U64_DMA.  These indicate which platform is being used,
 * Sony Playstation, PC, Sega Saturn or Nintendo Ultra64 respectively.
 *
 *   This file contains type definitions to replace the standard 'int', 'long'
 * etc.	 Also some new types have been added e.g fixed point numbers, booleans.
 *
 *   Some standard macros have been included.  These macros should be used
 * instead of your own.	 Any previously defined version of a macro will be
 * undefined before defining the new version.
 *
 */

#ifndef _DMA_H_
#define _DMA_H_

/* Both u64 and SGI use the same types */
#if defined(U64_DMA) || defined (SGI_DMA)
#define MIPS_R4000_DMA
#endif


/*
 *  PC types and macros
 */

#ifdef PC_DMA

/* integer types */
typedef signed char	Int8;
typedef signed char	int8;
typedef signed char	INT8;
typedef signed short	Int16;
typedef signed short	int16;
typedef signed short	INT16;
typedef signed long	Int32;
typedef signed long	int32;
#ifndef	_BASETSD_H_
//typedef signed long	INT32;
#endif //_BASETSD_H_
typedef signed __int64	Int64;
typedef signed __int64	int64;
typedef signed __int64	INT64;

/* unsigned integer types */
typedef unsigned char 	UInt8;
typedef unsigned char	Uint8;
typedef unsigned char	uint8;
typedef unsigned char	UINT8;
typedef unsigned short	UInt16;
typedef unsigned short	Uint16;
typedef unsigned short	uint16;
typedef unsigned short	UINT16;
typedef unsigned long	UInt32;
typedef unsigned long	Uint32;
typedef unsigned long	uint32;
#ifndef	_BASETSD_H_
//typedef unsigned long	UINT32;
#endif //_BASETSD_H_
typedef unsigned __int64	UInt64;
typedef unsigned __int64	Uint64;
typedef unsigned __int64	uint64;
typedef unsigned __int64	UINT64;

/* fixed point value types ( position of point not defined ) */
typedef signed char 	Fix8;
typedef signed char	fix8;
typedef signed char	FIX8;
typedef signed short 	Fix16;
typedef signed short 	fix16;
typedef signed short 	FIX16;
typedef signed long 	Fix32;
typedef signed long 	fix32;
typedef signed long 	FIX32;
typedef signed __int64 	Fix64;
typedef signed __int64 	fix64;
typedef signed __int64 	FIX64;

/* unsigned fixed point value types */
typedef unsigned char 	UFix8;
typedef unsigned char 	Ufix8;
typedef unsigned char	ufix8;
typedef unsigned char	UFIX8;
typedef unsigned short 	UFix16;
typedef unsigned short 	Ufix16;
typedef unsigned short 	ufix16;
typedef unsigned short 	UFIX16;
typedef unsigned long 	UFix32;
typedef unsigned long 	Ufix32;
typedef unsigned long 	ufix32;
typedef unsigned long 	UFIX32;
typedef unsigned __int64 	UFix64;
typedef unsigned __int64 	Ufix64;
typedef unsigned __int64 	ufix64;
typedef unsigned __int64 	UFIX64;

/* character types ( ASCII 8-bit ) */
typedef char		Char;
#ifndef CHAR
typedef char		CHAR; //lint !e761
#endif

/* Unicode characters */
typedef wchar_t	    Char16;
typedef wchar_t	    char16;
typedef wchar_t	    CHAR16;

/* Boolean types (TRUE/FALSE) */
typedef unsigned char 	Bool8;
typedef unsigned char	bool8;
typedef unsigned char	BOOL8;
typedef unsigned short 	Bool16;
typedef unsigned short 	bool16;
typedef unsigned short 	BOOL16;
typedef unsigned long 	Bool32;
typedef unsigned long 	bool32;
typedef unsigned long 	BOOL32;
typedef unsigned __int64 	Bool64;
typedef unsigned __int64 	bool64;
typedef unsigned __int64 	BOOL64;

/* Machine dependent macros */
#define LS8(a)		((Int8)((a) & ((UInt16)(~0) >> 8)))
#define MS8(a)		((Int8)((UInt16)(a) >> 8))
#define LS16(a)		((Int16)((a) & ((UInt32)(~0) >> 16)))
#define MS16(a)		((Int16)((UInt32)(a) >> 16))
#define LS32(a)		((Int32)((a) & ((UInt64)(~0) >> 32)))
#define MS32(a)		((Int32)((UInt64)(a) >> 32))

#endif



/*
 *  Sony Playstation types and macros
 */

#ifdef PSX_DMA

/* integer types */
typedef signed char 	Int8;
typedef signed char	int8;
typedef signed char	INT8;
typedef signed short	Int16;
typedef signed short	int16;
typedef signed short	INT16;
typedef signed long	Int32;
typedef signed long	int32;
typedef signed long	INT32;

/* unsigned integer types */
typedef unsigned char 	UInt8;
typedef unsigned char 	Uint8;
typedef unsigned char	uint8;
typedef unsigned char	UINT8;
typedef unsigned short	UInt16;
typedef unsigned short	Uint16;
typedef unsigned short	uint16;
typedef unsigned short	UINT16;
typedef unsigned long	UInt32;
typedef unsigned long	Uint32;
typedef unsigned long	uint32;
typedef unsigned long	UINT32;

/* fixed point value types ( position of point not defined ) */
typedef signed char 	Fix8;
typedef signed char	fix8;
typedef signed char	FIX8;
typedef signed short 	Fix16;
typedef signed short 	fix16;
typedef signed short 	FIX16;
typedef signed long 	Fix32;
typedef signed long 	fix32;
typedef signed long 	FIX32;

/* unsigned fixed point value types */
typedef unsigned char 	UFix8;
typedef unsigned char 	Ufix8;
typedef unsigned char	ufix8;
typedef unsigned char	UFIX8;
typedef unsigned short 	UFix16;
typedef unsigned short 	Ufix16;
typedef unsigned short 	ufix16;
typedef unsigned short 	UFIX16;
typedef unsigned long 	UFix32;
typedef unsigned long 	Ufix32;
typedef unsigned long 	ufix32;
typedef unsigned long 	UFIX32;

/* character types ( ASCII 8-bit ) */
typedef char		Char;
typedef char		CHAR;

/* Unicode characters */
//typedef unsigned short     Char16;
//typedef unsigned short     char16;
//typedef unsigned short     CHAR16;

typedef __wchar_t     Char16;
typedef __wchar_t     char16;
typedef __wchar_t     CHAR16;

/* Boolean types (TRUE/FALSE) */
typedef unsigned char 	Bool8;
typedef unsigned char	bool8;
typedef unsigned char	BOOL8;
typedef unsigned short 	Bool16;
typedef unsigned short 	bool16;
typedef unsigned short 	BOOL16;
typedef unsigned long 	Bool32;
typedef unsigned long 	bool32;
typedef unsigned long 	BOOL32;

/* Machine dependent macros */
#define LS8(a)		((Int8)((a) & ((UInt16)(~0) >> 8)))
#define MS8(a)		((Int8)((UInt16)(a) >> 8))
#define LS16(a)		((Int16)((a) & ((UInt32)(~0) >> 16)))
#define MS16(a)		((Int16)((UInt32)(a) >> 16))

#endif



/*
 *  Nintendo Ultra64 / SGI types and macros
 */

#ifdef MIPS_R4000_DMA

/* integer types */
typedef signed char 	Int8;
typedef signed char	int8;
typedef signed char	INT8;
typedef signed short	Int16;
typedef signed short	int16;
typedef signed short	INT16;
typedef signed long	Int32;
typedef signed long	int32;
typedef signed long	INT32;
typedef signed long long	Int64;
typedef signed long long	int64;
typedef signed long long	INT64;

/* unsigned integer types */
typedef unsigned char 	UInt8;
typedef unsigned char 	Uint8;
typedef unsigned char	uint8;
typedef unsigned char	UINT8;
typedef unsigned short	UInt16;
typedef unsigned short	Uint16;
typedef unsigned short	uint16;
typedef unsigned short	UINT16;
typedef unsigned long	UInt32;
typedef unsigned long	Uint32;
typedef unsigned long	uint32;
typedef unsigned long	UINT32;
typedef unsigned long long	UInt64;
typedef unsigned long long	Uint64;
typedef unsigned long long	uint64;
typedef unsigned long long	UINT64;

/* fixed point value types ( position of point not defined ) */
typedef signed char 	Fix8;
typedef signed char	fix8;
typedef signed char	FIX8;
typedef signed short 	Fix16;
typedef signed short 	fix16;
typedef signed short 	FIX16;
typedef signed long 	Fix32;
typedef signed long 	fix32;
typedef signed long 	FIX32;
typedef signed long long 	Fix64;
typedef signed long long 	fix64;
typedef signed long long 	FIX64;

/* unsigned fixed point value types */
typedef unsigned char 	UFix8;
typedef unsigned char 	Ufix8;
typedef unsigned char	ufix8;
typedef unsigned char	UFIX8;
typedef unsigned short 	UFix16;
typedef unsigned short 	Ufix16;
typedef unsigned short 	ufix16;
typedef unsigned short 	UFIX16;
typedef unsigned long 	UFix32;
typedef unsigned long 	Ufix32;
typedef unsigned long 	ufix32;
typedef unsigned long 	UFIX32;
typedef unsigned long long 	UFix64;
typedef unsigned long long 	Ufix64;
typedef unsigned long long 	ufix64;
typedef unsigned long long 	UFIX64;

/* character types ( ASCII 8-bit ) */
typedef char		Char;
typedef char		CHAR;

/* Unicode characters */
typedef unsigned short	   Char16;
typedef unsigned short	   char16;
typedef unsigned short	   CHAR16;

/* Boolean types (TRUE/FALSE) */
typedef unsigned char 	Bool8;
typedef unsigned char	bool8;
typedef unsigned char	BOOL8;
typedef unsigned short 	Bool16;
typedef unsigned short 	bool16;
typedef unsigned short 	BOOL16;
typedef unsigned long 	Bool32;
typedef unsigned long 	bool32;
typedef unsigned long 	BOOL32;
typedef unsigned long long 	Bool64;
typedef unsigned long long 	bool64;
typedef unsigned long long 	BOOL64;

/* Machine dependent macros */
#define LS8(a)		((Int8)((a) & ((UInt16)(~0) >> 8)))
#define MS8(a)		((Int8)((UInt16)(a) >> 8))
#define LS16(a)		((Int16)((a) & ((UInt32)(~0) >> 16)))
#define MS16(a)		((Int16)((UInt32)(a) >> 16))
#define LS32(a)		((Int32)((a) & ((UInt64)(~0) >> 32)))
#define MS32(a)		((Int32)((UInt64)(a) >> 32))

#endif


/* Scope declarators */
#define GLOBAL
#define SEMIGLOBAL		static
#define IMPORT			extern

/*
 * Useful macros. All four macros assume nothing about type. The only macro
 * to return a defined type is SIGN which returns an 'Int32'
 */
#undef ABS
#undef MAX
#undef MIN
#undef SIGN

#define ABS(a)		(((a) < 0) ? (-(a)) : (a))
#define MAX(a, b)	(((a) > (b)) ? (a) : (b))
#define MIN(a, b)	(((a) < (b)) ? (a) : (b))
#define SIGN(a)		(Int32)(((a) < 0) ? (-1) : (1))

/*
 * type based defines
 */
#define MIN_INT8	0x80
#define MAX_INT8	0x7f
#define MIN_INT16	0x8000
#define MAX_INT16	0x7fff
#define MIN_INT32	0x80000000
#define MAX_INT32	0x7fffffff
#define MIN_INT64	0x8000000000000000 //lint !e799
#define MAX_INT64	0x7fffffffffffffff //lint !e799
#define MAX_UINT8	0xff
#define MAX_UINT16	0xffff
#define MAX_UINT32	0xffffffff
#define MAX_UINT64	0xffffffffffffffff //lint !e799

/*
 * Useful defines
 */
#undef TRUE
#undef FALSE
#define TRUE		1
#define FALSE		0

#ifndef NULL
#define NULL 		0
#endif

#endif
