#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dvant.h"
#include "checkfortext.h"
#include "terror.h"
#include "tfile.h"

#define	BUFFER_SIZE	(256000)

KeyData Keys[NUMBER_OF_KEYS];
UInt32 NumberOfTextKeys;


TextInMissionFile TextInMissionFiles[MAX_MISSION_FILES];


char error_message[100];

Bool8 white_space(Char c)
{
	return ( (c==' ') || (c=='\t') || (c=='\x0A') || (c=='\x0D') );
}


void uppercaserize(Char *pBuffer, UInt32 BufferSize)
{
	Int32 C;
	
	for ( C = 0; C < BufferSize; C++)
	{
		if (pBuffer[C] >= 'a' && pBuffer[C] <= 'z')
		{
			pBuffer[C] += 'A' - 'a';
		}
	}
}


Bool8 is_string_in_buffer(Char *pStringToCheck, Char *pBuffer, UInt32 BufferLength)
{
	UInt32 BufferLoop;
	UInt32 CharLoop;
	
	if (pStringToCheck[0] == 0)
		return false;
		
	BufferLoop = 0;
	
	while (BufferLoop < BufferLength)
	{
		CharLoop = 0;
		while (pStringToCheck[CharLoop] == pBuffer[BufferLoop + CharLoop])
		{
			CharLoop++;
			
			if ( (pStringToCheck[CharLoop] == 0) && (white_space(pBuffer[BufferLoop + CharLoop])) )
				return true;
		}
	
		BufferLoop++;
	}
	
	return false;
}


void AddToKeyArray(Char *pTextToAdd)
{
	UInt32 CharLoop;
	Bool8 bFinished;

	CharLoop = 0;
	bFinished = false;

	if (NumberOfTextKeys >= NUMBER_OF_KEYS)
	{
		sprintf(error_message, "can only handle %d keys\n", NUMBER_OF_KEYS);
		lprint(error_message);
		error(ERR_TOO_MANY_KEYS);
	}
	
	while (!bFinished)
	{
		if (white_space(pTextToAdd[CharLoop]))
		{
			Keys[NumberOfTextKeys].KeyString[CharLoop] = 0;
			bFinished = true;
		}
		else
		{
			Keys[NumberOfTextKeys].KeyString[CharLoop] = pTextToAdd[CharLoop];
		}
		CharLoop++;
		
		if (CharLoop > 8)
		{
			sprintf(error_message, "%s key is too long\n", pTextToAdd);
			lprint(error_message);
			error(ERR_WORD_TOO_LONG);
		}
	}
	
	uppercaserize(Keys[NumberOfTextKeys].KeyString, strlen(Keys[NumberOfTextKeys].KeyString) );
 
	NumberOfTextKeys++;
}


/*
UInt32 get_size_of_file(FILE *pFileToBeMeasured)
{
	UInt32 ReturnSize;
	
	ReturnSize = 0;


	while (fgetc(pFileToBeMeasured) != EOF)
	{
		ReturnSize++;
	}
	
	fseek(pFileToBeMeasured, 0, SEEK_SET);
	
	return ReturnSize;
}
*/

void RecordTextOccursInMission(UInt16 TextIndex, UInt16 MissionIndex)
{
	if (MissionIndex >= MAX_MISSION_FILES)
	{
		lprint("Need to increase the maximum number of mission files\n");
		error (ERR_MEMORY);
	}
	
	if (TextInMissionFiles[MissionIndex].NumberOfLabels >= MAX_TEXT_LABELS_IN_MISSION_FILE)
	{
		lprint("Need to increase the number of text labels per mission file\n");
		error (ERR_MEMORY);
	}
	

	TextInMissionFiles[MissionIndex].TextLabelIndex[TextInMissionFiles[MissionIndex].NumberOfLabels] = TextIndex;
	TextInMissionFiles[MissionIndex].NumberOfLabels++;
}

Bool8 DoesTextOccurInMission(UInt16 TextIndex, UInt16 MissionIndex)
{
	UInt16 loop;
	
	if (MissionIndex >= MAX_MISSION_FILES)
	{
		lprint("Invalid mission file index\n");
		error (ERR_MEMORY);
	}

	for (loop = 0; loop < TextInMissionFiles[MissionIndex].NumberOfLabels; loop++)
	{
		if (TextInMissionFiles[MissionIndex].TextLabelIndex[loop] == TextIndex)
		{
			return true;
		}
	}

	return false;
}

void main( Int32 argc, Char *argv[] )
{
	UInt32 loop, loop2, CharLoop;
	FILE *pCurrFile;
	FILE *fpOutTextFile;
	FILE *fpReportFile;
	Char *pTempBuffer;
	UInt32 FileSize;
	UInt32 StatusFlag;
	bool bProcessingAKey;
	Char *pStartOfKey;
	UInt32 KeyLoop, MissionFileLoop;
	UInt32 NumOfMissionFiles;
	UInt32 SizeRead;
	Char SubDirectory[128];
	Bool8 bFinished;


	if (argc != 5)
	{
        lprint ("checkfortext expects four parameters\n");
        lprint ("Name of text file containing alphabetical list of text keys\n");
        lprint ("Name of main script file\n");
        lprint ("Name of new text file to contain the necessary changes\n");
        lprint ("Name of new text file to contain a list of unused text keys etc.\n");
        exit (-1);
	}

	loop = 0;
	bFinished = false;
	
	while (!bFinished)
	{
		SubDirectory[loop] = argv[2][loop];
		
		if ( (SubDirectory[loop] == '.') || (SubDirectory[loop] == 0) )
		{
			bFinished = true;
		}
		else
		{
			loop++;
		}
	}
	SubDirectory[loop] = 0;

	// Couldn't allocate 60068 bytes for some reason
	pTempBuffer = (Char*) malloc(BUFFER_SIZE * sizeof(Char));
	if (*pTempBuffer == NULL) error (ERR_MEMORY);
	
	bProcessingAKey = false;
	pStartOfKey = NULL;


	NumberOfTextKeys = 0;
	
	for (loop = 0; loop < NUMBER_OF_KEYS; loop++)
	{
		for (loop2 = 0; loop2 < 9; loop2++)
		{
			Keys[loop].KeyString[loop2] = 0;
		}
		Keys[loop].bInMainScript = false;
		Keys[loop].bOnlyInOneMissionScript = false;
	}

	for (loop = 0; loop < MAX_MISSION_FILES; loop++)
	{
		for (loop2 = 0; loop2 < MAX_TEXT_LABELS_IN_MISSION_FILE; loop2++)
		{
			TextInMissionFiles[loop].TextLabelIndex[loop2] = -1;
		}
		TextInMissionFiles[loop].NumberOfLabels = 0;
	}

	fpOutTextFile = fopen(argv[3], "w");
	if (fpOutTextFile == NULL)
	{
		sprintf(error_message, "Couldn't open %s for writing\n", argv[3]);
		lprint(error_message);
		error (ERR_OPEN_OUTPUT);
	}
	
	fpReportFile = fopen(argv[4], "w");
	if (fpReportFile == NULL)
	{
		sprintf(error_message, "Couldn't open %s for writing\n", argv[4]);
		lprint(error_message);
		error (ERR_OPEN_OUTPUT);
	}

	// Load all keys from american.alp into array of keys
	pCurrFile = fopen ( argv[1], "r" );
	if ( pCurrFile == NULL ) 
	{	
		sprintf(error_message, "Couldn't open %s\n", argv[1]);
		lprint(error_message);
		error (ERR_OPEN_INPUT);
	}
	
	FileSize = file_size(pCurrFile);
//	FileSize = get_size_of_file(pCurrFile);

	if (FileSize > BUFFER_SIZE)
	{
		sprintf(error_message, "Not enough memory for %s\n", argv[1]);
		lprint(error_message);
		error (ERR_MEMORY);
	}

	SizeRead = fread(pTempBuffer, 1, FileSize, pCurrFile);

	if (SizeRead > FileSize)
		error(ERR_READ);
		
	for (CharLoop = 0; CharLoop < SizeRead; CharLoop++)
	{
		if (white_space(pTempBuffer[CharLoop]))
		{
			if (bProcessingAKey)
			{
				AddToKeyArray(pStartOfKey);
				bProcessingAKey = false;
			}
		}
		else
		{
			if (bProcessingAKey == false)
			{
				pStartOfKey = &pTempBuffer[CharLoop];
				bProcessingAKey = true;
			}
		}
	}

	StatusFlag = fclose (pCurrFile);
	if (StatusFlag) error(ERR_CLOSE);
//	End of loading of keys from alphabetical list file	

	Char *pMissionFiles[FILE_LIST_MAX];
	
	strcat(SubDirectory, "\\*.sc");
	// Load each of the .sc files in the main directory and check for occurrences of each of the keys
//	NumOfMissionFiles = find_all_files(pMissionFiles, "main\\*.sc");
	NumOfMissionFiles = find_all_files(pMissionFiles, SubDirectory);
	
	if (NumOfMissionFiles > MAX_MISSION_FILES)
	{
		lprint("Need to increase the number of mission files that can be read\n");
		error (ERR_MEMORY);
	}

	
// Load main.sc into a buffer and check for occurrences of each of the keys
	pCurrFile = fopen(argv[2], "r");
	
	if (pCurrFile == NULL)
	{
		sprintf(error_message, "Couldn't open %s\n", argv[2]);
		lprint(error_message);
		error (ERR_OPEN_INPUT);
	}

	FileSize = file_size(pCurrFile);
//	FileSize = get_size_of_file(pCurrFile);
	
	if (FileSize > BUFFER_SIZE)
	{
		sprintf(error_message, "Not enough memory for %s\n", argv[2]);
		lprint(error_message);
		error (ERR_MEMORY);
	}

	SizeRead = fread(pTempBuffer, 1, FileSize, pCurrFile);

	if (SizeRead > FileSize)
		error(ERR_READ);

	printf("Processing %s\n", argv[2]);
	
	uppercaserize(pTempBuffer, SizeRead);

	for (KeyLoop = 0; KeyLoop < NumberOfTextKeys; KeyLoop++)
	{
		if (is_string_in_buffer(Keys[KeyLoop].KeyString, pTempBuffer, SizeRead) )
		{
			Keys[KeyLoop].bInMainScript = true;
		}
	}

	StatusFlag = fclose (pCurrFile);
	if (StatusFlag) error(ERR_CLOSE);
//	End of main script file checks


//	Check which text keys occur in which mission files
	for (MissionFileLoop = 0; MissionFileLoop < NumOfMissionFiles; MissionFileLoop++)
	{
		pCurrFile = fopen(pMissionFiles[MissionFileLoop], "r");
		
		if (pCurrFile == NULL)
		{
			sprintf(error_message, "Couldn't open %s\n", pMissionFiles[MissionFileLoop]);
			lprint(error_message);
			error (ERR_OPEN_INPUT);
		}

		FileSize = file_size(pCurrFile);
//		FileSize = get_size_of_file(pCurrFile);
		
		if (FileSize > BUFFER_SIZE)
		{
			sprintf(error_message, "Not enough memory for %s\n", pMissionFiles[MissionFileLoop]);
			lprint(error_message);
			error (ERR_MEMORY);
		}

		SizeRead = fread(pTempBuffer, 1, FileSize, pCurrFile);

		if (SizeRead > FileSize)
			error(ERR_READ);

		printf("Processing %s\n", pMissionFiles[MissionFileLoop]);
		
		uppercaserize(pTempBuffer, SizeRead);

		for (KeyLoop = 0; KeyLoop < NumberOfTextKeys; KeyLoop++)
		{
			if (is_string_in_buffer(Keys[KeyLoop].KeyString, pTempBuffer, SizeRead) )
			{
				RecordTextOccursInMission(KeyLoop, MissionFileLoop);
			}
		}

		StatusFlag = fclose (pCurrFile);
		if (StatusFlag) error(ERR_CLOSE);
	}
//	End of mission file checks

	free(pTempBuffer);
	pTempBuffer = NULL;


	UInt16 NumOfOccurrences;


//	Report some interesting stuff in the report file
	fprintf(fpReportFile, "none\n\n");

	for (KeyLoop = 0; KeyLoop < NumberOfTextKeys; KeyLoop++)
	{
		NumOfOccurrences = 0;
		
		if (Keys[KeyLoop].bInMainScript == false)
		{
			for (MissionFileLoop = 0; MissionFileLoop < NumOfMissionFiles; MissionFileLoop++)
			{
				if (DoesTextOccurInMission(KeyLoop, MissionFileLoop))
				{
					NumOfOccurrences++;
				}
			}
		
			if (NumOfOccurrences == 0)
			{
				fprintf(fpReportFile, "%s\n", Keys[KeyLoop].KeyString);
			}
		}
	}

	fprintf(fpReportFile, "\n\n");


	fprintf(fpReportFile, "main script only\n\n");

	for (KeyLoop = 0; KeyLoop < NumberOfTextKeys; KeyLoop++)
	{
		NumOfOccurrences = 0;
		
		if (Keys[KeyLoop].bInMainScript == true)
		{
			for (MissionFileLoop = 0; MissionFileLoop < NumOfMissionFiles; MissionFileLoop++)
			{
				if (DoesTextOccurInMission(KeyLoop, MissionFileLoop))
				{
					NumOfOccurrences++;
				}
			}
		
			if (NumOfOccurrences == 0)
			{
				fprintf(fpReportFile, "%s\n", Keys[KeyLoop].KeyString);
			}
		}
	}

	fprintf(fpReportFile, "\n\n\n\n");

/*
	fprintf(fpOutTextFile, "one mission only\n\n");

	for (KeyLoop = 0; KeyLoop < NumberOfTextKeys; KeyLoop++)
	{
		NumOfOccurrences = 0;
		
		if (Keys[KeyLoop].bInMainScript == false)
		{
			for (MissionFileLoop = 0; MissionFileLoop < NumOfMissionFiles; MissionFileLoop++)
			{
				if (DoesTextOccurInMission(KeyLoop, MissionFileLoop))
				{
					NumOfOccurrences++;
				}
			}
		
			if (NumOfOccurrences == 1)
			{
				fprintf(fpOutTextFile, "%s\n", Keys[KeyLoop].KeyString);
			}
		}
	}

	fprintf(fpOutTextFile, "\n\n");


	fprintf(fpOutTextFile, "more than one mission\n\n");

	for (KeyLoop = 0; KeyLoop < NumberOfTextKeys; KeyLoop++)
	{
		NumOfOccurrences = 0;

		if (Keys[KeyLoop].bInMainScript == true)
		{
			NumOfOccurrences++;
		}
		
		for (MissionFileLoop = 0; MissionFileLoop < NumOfMissionFiles; MissionFileLoop++)
		{
			if (DoesTextOccurInMission(KeyLoop, MissionFileLoop))
			{
				NumOfOccurrences++;
			}
		}
	
		if (NumOfOccurrences > 1)
		{
			fprintf(fpOutTextFile, "%s\n", Keys[KeyLoop].KeyString);
		}
	}

	fprintf(fpOutTextFile, "\n\n");
*/

	for (KeyLoop = 0; KeyLoop < NumberOfTextKeys; KeyLoop++)
	{
		fprintf(fpReportFile, "%s\n", Keys[KeyLoop].KeyString);
		
		if (Keys[KeyLoop].bInMainScript)
		{
			fprintf(fpReportFile, "main.sc\n");
		}
		
		for (MissionFileLoop = 0; MissionFileLoop < NumOfMissionFiles; MissionFileLoop++)
		{
			if (DoesTextOccurInMission(KeyLoop, MissionFileLoop))
			{
				fprintf(fpReportFile, "%s\n", pMissionFiles[MissionFileLoop]);
			}
		}
	
		fprintf(fpReportFile, "\n\n");
	}

	fclose(fpReportFile);
//	End of report file



//	List changes in the changes file
	UInt16 NumberOfKeysInOnlyOneMissionFile;
	NumberOfKeysInOnlyOneMissionFile = 0;

	for (KeyLoop = 0; KeyLoop < NumberOfTextKeys; KeyLoop++)
	{
		NumOfOccurrences = 0;
		
		if (Keys[KeyLoop].bInMainScript == false)
		{
			for (MissionFileLoop = 0; MissionFileLoop < NumOfMissionFiles; MissionFileLoop++)
			{
				if (DoesTextOccurInMission(KeyLoop, MissionFileLoop))
				{
					NumOfOccurrences++;
				}
			}
		
			if (NumOfOccurrences == 1)
			{
				Keys[KeyLoop].bOnlyInOneMissionScript = true;
				NumberOfKeysInOnlyOneMissionFile++;
			}
		}
	}

	UInt32 FileNameLength;
	bool Finished;
	Char MissionName[15];
	UInt16 MissionNameLoop;
	UInt16 StartOfStringIndex;

	for (MissionFileLoop = 0; MissionFileLoop < NumOfMissionFiles; MissionFileLoop++)
	{
		FileNameLength = strlen(pMissionFiles[MissionFileLoop]);
		Finished = false;

		CharLoop = FileNameLength - 1;
		
		while ( (CharLoop >= 0) && !Finished)
		{
			if (pMissionFiles[MissionFileLoop][CharLoop] == '\\')
			{
				Finished = true;
			}
			else
			{
				CharLoop--;
			}
		}
		
		if (Finished)
		{
			StartOfStringIndex = CharLoop + 1;
		}
		
		MissionNameLoop = 0;
		
		CharLoop = StartOfStringIndex;
		
		Finished = false;
		while  ( (CharLoop < FileNameLength) && (!Finished))
		{
			if (pMissionFiles[MissionFileLoop][CharLoop] == '.')
			{
				Finished = true;
			}
			else
			{	
				MissionName[MissionNameLoop++] = pMissionFiles[MissionFileLoop][CharLoop];
				CharLoop++;
			}
		}
		MissionName[MissionNameLoop] = 0;


		for (KeyLoop = 0; KeyLoop < NumberOfTextKeys; KeyLoop++)
		{
			if (Keys[KeyLoop].bOnlyInOneMissionScript)
			{
				if (DoesTextOccurInMission(KeyLoop, MissionFileLoop))
				{
					fprintf(fpOutTextFile, "%s %s\n", Keys[KeyLoop].KeyString, MissionName);
				}
			}
		}
	}	

	fclose(fpOutTextFile);
//	End of the changes file


	for (MissionFileLoop = 0; MissionFileLoop < NumOfMissionFiles; MissionFileLoop++)
	{
		free (pMissionFiles[MissionFileLoop]);
		pMissionFiles[MissionFileLoop] = NULL;
	}
}


