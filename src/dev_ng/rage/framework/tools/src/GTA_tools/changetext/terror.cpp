/********************************
 * terror.cpp                   *
 *                              *
 * error control for dino tools *
 *                              *
 * KRH 23/06/95                 *
 *                              *
 * (c)	1995 DMA Design Ltd     *
 ********************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ansi_files.h>

#include "dvant.h"
#include "terror.h"

static Char* error_string[] = {"OK",
						"cannot open input file",
						"cannot open output file",
						"cannot close file",
						"read failure",
						"write failure",
						"ftell failure",
						"fseek failure",
						"out of memory",
						"not whole delta",
						"invalid file length",
						"too many deltas",
						"delta too big",
						"not whole sprite",
						"too many sprites",
						"no matching files in directory",
						"allocating zero bytes dos memory",
						"allocating non-16 bytes dos memory",
						"out of dos memory",
						"failure freeing dos memory",
						"interrupt simulate failure",
						"vesa support failure",
						"vesa failure",
						"interrupt 31 failure",
						"no lbm header found",
						"lbm file not DPaint Enhanced format",
						"no lbm bitmap information found",
						"lbm bitmap header incorrect length",
						"lbm unknown compression type",
						"invalid grid",
						"writing zero bytes",
						"no palette in lbm",
						"cannot open log file",
						"cannot close log file",
						"cannot write to log file",
						"cannot find compressed map file",
						"multiple compressed map files",
						"block overflow",
						"column overflow",
						"stuff overflow",
						"sprite too big - max is 255x255",
						"incorrect file version",
						"missing ]",
						"routes added already",
						"grid width is not a multiple of 8",
						"invalid grid width",
						"invalid remap number",
						"lbm palette invalid size",
						"invalid case",
						"too many tiles",
						"string length incorrect",
						"too many palettes",
						"graphics overflow",
						"deltas too big",
						"too many deltas for one sprite",
						"invalid grid height",
						"too many files",
						"incorrect file type",
						"invalid size",
						"cannot remove file",
						"cannot rename file",
						"key too large",
						"too many keys",
						"too much text",
						"no whitespace before header",
						"duplicate keys",
						"missing }",
						"unsupported .tga format",
						"} without earlier {",
						"cannot nest comments",
						"should only be one character between a pair of ~",
						"word is too long",
						"unknown mission name",
						"too many text keys",
						"mission containing text key has changed",
						"too many mission files"
						};

static FILE *logff;
static Bool32 log_open = FALSE;

/*
 * time_string
 * -----------
 *
 * function		: get time of day in a formatted string
 * return value	: string pointer
 *
 */

static Char *time_string ()
{
    time_t time_of_day;
	Char *tstr;
	static Char str[128];

    time_of_day = time( NULL );
	tstr = ctime( &time_of_day );
	tstr[strlen(tstr)-1]='\0';
	sprintf( str, "\n------ %s ------\n", tstr );
	return (str);
}


/*
 * open_log
 * --------
 *
 * function		: open log file
 *
 */

void open_log (void)
{
	logff = fopen ("log", "at");
	if ( logff==NULL) error (ERR_OPEN_LOG);
	log_open = TRUE;

	loprint(time_string());
}


/*
 * close_log
 * ---------
 *
 * function		: close log file
 *
 */

void close_log (void)
{
	Int32 s;

	if ( log_open )
	{
		log_open = FALSE;
		s = fclose (logff);
		if (s) error (ERR_CLOSE_LOG);
	}
}


/*
 * loprint
 * ------
 *
 * function		: print string to log file only
 * input		: string
 *
 */

void loprint (Char *str)
{
	Int32 s;

	if ( log_open )
	{
		s = fprintf (logff, str);
		if (s<0)
		{
			close_log();
			error (ERR_WRITE_LOG);
		}
	}
}
 

/*
 * lprint
 * ------
 *
 * function		: print string to screen and to log file
 * input		: string
 *
 */

void lprint (Char *str)
{
	printf (str);
	loprint (str);
}
 

/*
 * error
 * -----
 *
 * function	: display error message and exit
 * input	: error number
 *
 */

void error (Int32 error_number)
{
	static Char str[128];

	if (error_number)
	{
		sprintf (str,"Error %d : %s\n", error_number, error_string[error_number]);
		lprint(str);
	}
//	(void)_fcloseall();
	__close_all();
	exit (-1);
}

void error_str (Int32 error_number, Char *errstr)
{
	static Char str[128];

	if (error_number)
	{
		sprintf (str,"Error %d : %s\n", error_number, error_string[error_number]);
		lprint(str);
		if(errstr)
		{
			sprintf (str,"Last Key Recognised: %s\n", errstr);
			lprint(str);
		}
	}
//	(void)_fcloseall();
	__close_all();
	exit (-1);
}
	





