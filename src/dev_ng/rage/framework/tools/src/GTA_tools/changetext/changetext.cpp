#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "dvant.h"
#include "changetext.h"
#include "terror.h"
#include "tfile.h"

#define	BUFFER_SIZE	(500000)
#define NEW_BUFFER_SIZE (525000)


KeyData Keys[NUMBER_OF_KEYS];
UInt32 NumberOfTextKeys;

MissionFileName MissionFileNames[MAX_MISSION_FILES];
UInt32 NumberOfMissionFileNames;


char error_message[100];

Bool8 white_space(Char c)
{
	return ( (c==' ') || (c=='\t') || (c=='\x0A') || (c=='\x0D') );
}


void uppercaserize(Char *pBuffer, UInt32 BufferSize)
{
	Int32 C;
	
	for ( C = 0; C < BufferSize; C++)
	{
		if (pBuffer[C] >= 'a' && pBuffer[C] <= 'z')
		{
			pBuffer[C] += 'A' - 'a';
		}
	}
}


void AddToKeyArray(Char *pTextToAdd)
{
	UInt32 CharLoop;
	Bool8 bFinished;

	CharLoop = 0;
	bFinished = false;

	if (NumberOfTextKeys >= NUMBER_OF_KEYS)
	{
		sprintf(error_message, "can only handle %d keys\n", NUMBER_OF_KEYS);
		lprint(error_message);
		error(ERR_TOO_MANY_KEYS);
	}
	
	while (!bFinished)
	{
		if (white_space(pTextToAdd[CharLoop]))
		{
			Keys[NumberOfTextKeys].KeyString[CharLoop] = 0;
			uppercaserize(Keys[NumberOfTextKeys].KeyString, strlen(Keys[NumberOfTextKeys].KeyString));
			bFinished = true;
		}
		else
		{
			Keys[NumberOfTextKeys].KeyString[CharLoop] = pTextToAdd[CharLoop];
		}
		CharLoop++;
		
		if (CharLoop > 8)
		{
			sprintf(error_message, "%s key is too long\n", pTextToAdd);
			lprint(error_message);
			error(ERR_WORD_TOO_LONG);
		}
	}
}


void AddToMissionNameArray(Char *pTextToAdd)
{
	UInt32 CharLoop, loop;
	Bool8 bFinished, bAlreadyInFileNameList;

	CharLoop = 0;
	bFinished = false;

	if (NumberOfTextKeys >= NUMBER_OF_KEYS)
	{
		sprintf(error_message, "can only handle %d keys\n", NUMBER_OF_KEYS);
		lprint(error_message);
		error(ERR_TOO_MANY_KEYS);
	}
	
	while (!bFinished)
	{
		if (white_space(pTextToAdd[CharLoop]))
		{
			if (CharLoop >= (MISSION_NAME_LENGTH - 1) )
			{
				Keys[NumberOfTextKeys].MissionString[(MISSION_NAME_LENGTH - 1)] = 0;
			}
			else
			{
				Keys[NumberOfTextKeys].MissionString[CharLoop] = 0;
			}
			uppercaserize(Keys[NumberOfTextKeys].MissionString, strlen(Keys[NumberOfTextKeys].MissionString));
			bFinished = true;
		}
		else
		{
			if (CharLoop >= (MISSION_NAME_LENGTH - 2) )
			{
				Keys[NumberOfTextKeys].MissionString[(MISSION_NAME_LENGTH - 2)] = pTextToAdd[CharLoop];
			}
			else
			{
				Keys[NumberOfTextKeys].MissionString[CharLoop] = pTextToAdd[CharLoop];
			}
		}
		CharLoop++;
		
/*
		if (CharLoop > MISSION_NAME_LENGTH)
		{
			sprintf(error_message, "%s mission name is too long\n", pTextToAdd);
			lprint(error_message);
			error(ERR_WORD_TOO_LONG);
		}
*/
	}

	bAlreadyInFileNameList = false;
	loop = 0;
	
	while ( (loop < NumberOfMissionFileNames) && (!bAlreadyInFileNameList) )
	{
		if (strcmp(MissionFileNames[loop].FileName, Keys[NumberOfTextKeys].MissionString) == 0)
		{
			bAlreadyInFileNameList = true;
		}
		
		loop++;
	}

	if (!bAlreadyInFileNameList)
	{
		strncpy(MissionFileNames[NumberOfMissionFileNames].FileName, Keys[NumberOfTextKeys].MissionString, MISSION_NAME_LENGTH);
		NumberOfMissionFileNames++;
		
		if (NumberOfMissionFileNames >= MAX_MISSION_FILES)
		{
			sprintf(error_message, "can only handle %d mission file names\n", MAX_MISSION_FILES);
			lprint(error_message);
			error(ERR_TOO_MANY_MISSION_FILES);
		}
	}
}

void MarkMissionTextAsUsed(Char *pMissionName)
{
	Bool8 bFound;
	UInt16 MissionLoop;
	
	bFound = false;
	MissionLoop = 0;
	
	while ( (MissionLoop < NumberOfMissionFileNames) && !bFound)
	{
		if (strcmp(MissionFileNames[MissionLoop].FileName, pMissionName) == 0)
		{
			bFound = true;
			MissionFileNames[MissionLoop].bKeysOccurInTextFile = true;
		}
		MissionLoop++;
	}
}

void InsertMissionNameIfNeeded(Char *pStartOfKey, Char *pBuffer, UInt32 *pBufferIndex)
{
	Char *pCurrChar, *pColon;
	bool bFoundColon, bFound, bWriteNewMissionName;
	Char ActualKey[10];
	Char ExistingMissionName[MISSION_NAME_LENGTH];
	UInt32 loop, KeyLoop, CharLoop;
	UInt32 MissionNameLength;
	
	pCurrChar = pStartOfKey;
	pColon = NULL;
	bFoundColon = false;
	loop = 0;
	
	while ( (*pCurrChar != ']') && !bFoundColon)
	{
		if (*pCurrChar == ':')
		{
			bFoundColon = true;
			pColon = pCurrChar;
		}
		else
		{
			ActualKey[loop++] = *pCurrChar;
		}
		pCurrChar++;
	}
	
	ActualKey[loop] = 0;
	uppercaserize(ActualKey, strlen(ActualKey));

	loop = 0;
	if (bFoundColon)
	{
		while (*pCurrChar != ']')
		{
			ExistingMissionName[loop++] = *pCurrChar;
			pCurrChar++;
		}
		ExistingMissionName[loop] = 0;
		uppercaserize(ExistingMissionName, strlen(ExistingMissionName));
	}

	bFound = false;
	KeyLoop = 0;
	
	while ( (KeyLoop < NumberOfTextKeys) && !bFound)
	{
		if (strcmp(Keys[KeyLoop].KeyString, ActualKey) == 0)
		{
			bFound = true;
			Keys[KeyLoop].bOccursInTextFile = true;
			MarkMissionTextAsUsed(Keys[KeyLoop].MissionString);
		}
		else
		{
			KeyLoop++;
		}
	}
	
	bWriteNewMissionName = false;
	
	if (bFound)
	{
		if (bFoundColon)
		{
			if (strcmp(Keys[KeyLoop].MissionString, ExistingMissionName) == 0)
			{
				//	Already has the correct mission name in the key so do nothing
			}
			else
			{
//				sprintf(error_message, "Key %s - previously in mission %s, now in %s\n", ActualKey, ExistingMissionName, Keys[KeyLoop].MissionString);
//				lprint(error_message);
//				error (ERR_MISSION_HAS_CHANGED);
				
				//	rewind pBufferIndex to the location of the colon and insert the new mission name
				(*pBufferIndex) -= pCurrChar - pColon;
				bWriteNewMissionName = true;
			}
		}
		else
		{
			bWriteNewMissionName = true;
		}
		
		if (bWriteNewMissionName)
		{
			pBuffer[(*pBufferIndex)++] = ':';
			
			MissionNameLength = strlen(Keys[KeyLoop].MissionString);
			
			for (CharLoop = 0; CharLoop < MissionNameLength; CharLoop++)
			{
				pBuffer[(*pBufferIndex)++] = Keys[KeyLoop].MissionString[CharLoop];
			}
		}
	}
	else
	{
		if (bFoundColon)
		{
//			sprintf(error_message, "Key %s - mission %s should be removed from this key\n", ActualKey, ExistingMissionName);
//			lprint(error_message);
//			error (ERR_MISSION_HAS_CHANGED);
			//	rewind pBufferIndex to the location of the colon
			(*pBufferIndex) -= pCurrChar - pColon;
		}
	}
}

UInt32 skip_existing_table(Char *pBuffer, UInt32 TotalSize)
{
	UInt32 CharIndex;
	Bool8 bFinished;
	
	CharIndex = 0;
	bFinished = false;
	
	if (strncmp( (const char *) &pBuffer[CharIndex], "start", 5) == 0)
	{
		CharIndex += 5;

		while ( (CharIndex < TotalSize) && (!bFinished) )
		{
			if (strncmp( (const char *) &pBuffer[CharIndex], "end", 3) == 0)
			{
				CharIndex += 3;
				bFinished = true;
			}
			else
			{
				CharIndex++;
			}
		}
	}
	
	return CharIndex;
}


void main( Int32 argc, Char *argv[] )
{
	UInt32 loop, loop2, CharLoop, NewLoop;
	FILE *pCurrFile;
	UInt32 FileSize, NewFileSize;
	UInt32 SizeRead, SizeWritten;
	Char *pTempBuffer;
	Char *pNewBuffer;
	UInt32 StatusFlag;
	UInt16 ReadingChangesStatus;
	Char *pStartOfText;
	
	if (argc != 4)
	{
        lprint ("changetext expects three parameters\n");
        lprint ("Name of text file of changes\n");
        lprint ("Name of game text file to be changed\n");
        lprint ("Name of new game text file with changes\n");
        exit (-1);
	}
	
	// Couldn't allocate 60068 bytes for some reason
	pTempBuffer = (Char*) malloc(BUFFER_SIZE * sizeof(Char));
	if (*pTempBuffer == NULL) error (ERR_MEMORY);

	pNewBuffer = (Char*) malloc(NEW_BUFFER_SIZE * sizeof(Char));
	if (*pNewBuffer == NULL) error (ERR_MEMORY);

	NumberOfTextKeys = 0;

	for (loop = 0; loop < NUMBER_OF_KEYS; loop++)
	{
		for (loop2 = 0; loop2 < 9; loop2++)
		{
			Keys[loop].KeyString[loop2] = 0;
		}
		
		for (loop2 = 0; loop2 < MISSION_NAME_LENGTH; loop2++)
		{
			Keys[loop].MissionString[loop2] = 0;		
		}
		Keys[loop].bOccursInTextFile = false;
	}

	NumberOfMissionFileNames = 0;

	for (loop = 0; loop < MAX_MISSION_FILES; loop++)
	{
		for (loop2 = 0; loop2 < MISSION_NAME_LENGTH; loop2++)
		{
			MissionFileNames[loop].FileName[loop2] = 0;
		}
		
		MissionFileNames[loop].bKeysOccurInTextFile = false;
	}

	//	Load file containing list of keys with mission name
	
	pCurrFile = fopen (argv[1], "r");
	if (pCurrFile == NULL)
	{
		sprintf(error_message, "Couldn't open %s\n", argv[1]);
		lprint(error_message);
		error (ERR_OPEN_INPUT);
	}

	FileSize = file_size(pCurrFile);
//	FileSize = get_size_of_file(pCurrFile);

	if (FileSize > BUFFER_SIZE)
	{
		sprintf(error_message, "Not enough memory for %s\n", argv[1]);
		lprint(error_message);
		error (ERR_MEMORY);
	}

	SizeRead = fread(pTempBuffer, 1, FileSize, pCurrFile);

	if (SizeRead > FileSize)
		error(ERR_READ);
		
	StatusFlag = fclose (pCurrFile);
	if (StatusFlag) error(ERR_CLOSE);

	ReadingChangesStatus = 0;

	for (CharLoop = 0; CharLoop < SizeRead; CharLoop++)
	{
		if (white_space(pTempBuffer[CharLoop]))
		{
			if (ReadingChangesStatus == 1)
			{
				AddToKeyArray(pStartOfText);
				ReadingChangesStatus = 2;
			}
			else if (ReadingChangesStatus == 3)
			{
				AddToMissionNameArray(pStartOfText);
				NumberOfTextKeys++;
				ReadingChangesStatus = 0;
			}
		}
		else
		{
			if (ReadingChangesStatus == 0)
			{
				pStartOfText = &pTempBuffer[CharLoop];
				ReadingChangesStatus = 1;
			}
			else if (ReadingChangesStatus == 2)
			{
				pStartOfText = &pTempBuffer[CharLoop];
				ReadingChangesStatus = 3;
			}
		}
	}

	
	pCurrFile = fopen (argv[2], "r");
	if (pCurrFile == NULL)
	{
		sprintf(error_message, "Couldn't open %s\n", argv[2]);
		lprint(error_message);
		error (ERR_OPEN_INPUT);
	}

	FileSize = file_size(pCurrFile);
//	FileSize = get_size_of_file(pCurrFile);

	if (FileSize > BUFFER_SIZE)
	{
		sprintf(error_message, "Not enough memory for %s\n", argv[2]);
		lprint(error_message);
		error (ERR_MEMORY);
	}

	SizeRead = fread(pTempBuffer, 1, FileSize, pCurrFile);

	if (SizeRead > FileSize)
		error(ERR_READ);

	StatusFlag = fclose (pCurrFile);
	if (StatusFlag) error(ERR_CLOSE);


//	skip table at the start of pTempBuffer (if it exists) by increasing CharLoop
//	Can only deal with one table - should be enough
	CharLoop = skip_existing_table(pTempBuffer, SizeRead);

//	copied from do_convert_asc ( UInt8 *in, Int32 inlen, UInt16 *out, Int32 *outlen)

	enum {WHITE_SPACE, HEADER, TEXT, WHITE_SPACE_INTERNAL, FIRST_WHITE_SPACE, COMMENT_WHITE_SPACE, COMMENT_FIRST_WHITE_SPACE, COMMENT_WHITE_SPACE_INTERNAL, COMMENT_TEXT} state = FIRST_WHITE_SPACE;
	Char *start_key = NULL;
	Int32 nest = 0;

	NewLoop = 0;

	for (; CharLoop < SizeRead; CharLoop++)
    {
    	if (NewLoop > NEW_BUFFER_SIZE)
    	{
			sprintf(error_message, "Not enough memory for new file %s\n", argv[3]);
			lprint(error_message);
			error (ERR_MEMORY);
    	}
    	
        switch (state)
        {
			case COMMENT_FIRST_WHITE_SPACE:
					if ( pTempBuffer[CharLoop] =='{' )
					{
						error_str (ERR_NESTED_COMMENTS, (char*)start_key);
					}
					else if ( pTempBuffer[CharLoop] =='}' )
					{
						if (nest==0)
							state = FIRST_WHITE_SPACE;
						else
							nest--;
					}
					pNewBuffer[NewLoop++] = pTempBuffer[CharLoop];
					break;

			case COMMENT_WHITE_SPACE:
					if ( pTempBuffer[CharLoop] =='{' )
					{
						error_str (ERR_NESTED_COMMENTS, (char*)start_key);
					}
					else if ( pTempBuffer[CharLoop] =='}' )
					{
						if (nest==0)
							state = WHITE_SPACE;
						else
							nest--;
					}
					pNewBuffer[NewLoop++] = pTempBuffer[CharLoop];
					break;

				case COMMENT_WHITE_SPACE_INTERNAL:
						if ( pTempBuffer[CharLoop] =='{' )
						{
							error_str (ERR_NESTED_COMMENTS, (char*)start_key);
						}
						else if ( pTempBuffer[CharLoop] =='}' )
						{
							if (nest==0)
								state = WHITE_SPACE_INTERNAL;
							else
								nest--;
						}
						pNewBuffer[NewLoop++] = pTempBuffer[CharLoop];
						break;

				case COMMENT_TEXT:
						if ( pTempBuffer[CharLoop] =='{' )
						{
							error_str (ERR_NESTED_COMMENTS, (char*)start_key);
						}
						else if ( pTempBuffer[CharLoop] =='}' )
						{
							if (nest==0)
							{
								state = TEXT;
							}
							else
								nest--;
						}
						pNewBuffer[NewLoop++] = pTempBuffer[CharLoop];
						break;

                case FIRST_WHITE_SPACE:
                        if ( pTempBuffer[CharLoop] =='[' )
                        {
                            state = HEADER;
							start_key = &pTempBuffer[CharLoop + 1];
                        }
						else if ( pTempBuffer[CharLoop] =='{')
							state = COMMENT_FIRST_WHITE_SPACE;
						else if ( pTempBuffer[CharLoop] =='}' )
						{
							error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
						}
						pNewBuffer[NewLoop++] = pTempBuffer[CharLoop];

                        break;
                        
                case WHITE_SPACE:
                        if ( pTempBuffer[CharLoop] =='[' )
                        {
							state = HEADER;
							start_key = &pTempBuffer[CharLoop + 1];
                        }
						else if ( pTempBuffer[CharLoop] =='{')
							state = COMMENT_WHITE_SPACE;
						else if ( pTempBuffer[CharLoop] =='}' )
						{
							error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
						}
                        else if (!white_space ( pTempBuffer[CharLoop] ))
                        {
                        	state = TEXT;
                        }
                        pNewBuffer[NewLoop++] = pTempBuffer[CharLoop];
                        break;
                        
                case WHITE_SPACE_INTERNAL:
                        if ( pTempBuffer[CharLoop] =='[' )
                        {
	                        state = HEADER;
							start_key = &pTempBuffer[CharLoop + 1];
                        }
						else if ( pTempBuffer[CharLoop] =='{')
							state = COMMENT_WHITE_SPACE_INTERNAL;
						else if ( pTempBuffer[CharLoop] =='}' )
						{
							error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
						}
                        else if (!white_space ( pTempBuffer[CharLoop] ))
                        {
                        	state = TEXT;
                        }
                        pNewBuffer[NewLoop++] = pTempBuffer[CharLoop];
                        break;
                        
                case HEADER:
                        if ( pTempBuffer[CharLoop] ==']' )
                        {
							InsertMissionNameIfNeeded(start_key, pNewBuffer, &NewLoop);
                            state = WHITE_SPACE;
                        }
						else if ( pTempBuffer[CharLoop]=='}' )
						{
							error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
						}
						
						pNewBuffer[NewLoop++] = pTempBuffer[CharLoop];
                        break;
                                
                case TEXT:
                        if (white_space(pTempBuffer[CharLoop]))
						{
                            state = WHITE_SPACE_INTERNAL;
						}
						else if ( pTempBuffer[CharLoop]=='{')
						{
							state = COMMENT_TEXT;
						}
						else if ( pTempBuffer[CharLoop] =='}' )
						{
							error_str (ERR_UNMATCHED_BRACKET, (char*)start_key);
						}
                        else
                        {
                            if ( pTempBuffer[CharLoop] =='[' )
								error_str(ERR_NO_WHITESPACE_BEFORE_HEADER, (char*)start_key);
                        }
                        pNewBuffer[NewLoop++] = pTempBuffer[CharLoop];

                        break;                                                                                          

                default:
                        break;
        }

	}

    if ( state == HEADER ) 
		error_str (ERR_BRACKET, (char*)start_key);
	else if ( ( state == COMMENT_FIRST_WHITE_SPACE ) ||
			  ( state == COMMENT_WHITE_SPACE ) ||
			  ( state == COMMENT_WHITE_SPACE_INTERNAL ) ||
			  ( state == COMMENT_TEXT ) )
		error_str (ERR_COMMENT_BRACKET, (char*)start_key);

	NewFileSize = NewLoop;
	
	
	for (loop = 0; loop < NumberOfMissionFileNames; loop++)
	{
		if (MissionFileNames[loop].bKeysOccurInTextFile == false)
		{
			printf("No text for this mission %s\n", MissionFileNames[loop].FileName);
		}
	}

	for (loop = 0; loop < NumberOfTextKeys; loop++)
	{
		if (Keys[loop].bOccursInTextFile == false)
		{
			printf("No text for this key %s\n", Keys[loop].KeyString);
		}
	}

	
	pCurrFile = fopen (argv[3], "w");
	if (pCurrFile == NULL)
	{
		sprintf(error_message, "Couldn't open %s for writing\n", argv[3]);
		lprint(error_message);
		error (ERR_OPEN_INPUT);
	}
	
	fprintf(pCurrFile, "start\n");
	
	for (loop = 0; loop < NumberOfMissionFileNames; loop++)
	{
		if (MissionFileNames[loop].bKeysOccurInTextFile)
		{
			fprintf(pCurrFile, MissionFileNames[loop].FileName);
			fprintf(pCurrFile, "\n");
		}
	}

	fprintf(pCurrFile, "end\n\n\n\n\n\n");

	SizeWritten = fwrite(pNewBuffer, 1, NewFileSize, pCurrFile);

	if (SizeWritten != NewFileSize)
		error(ERR_WRITE);

	StatusFlag = fclose (pCurrFile);
	if (StatusFlag) error(ERR_CLOSE);

	free(pTempBuffer);
	free(pNewBuffer);
}

