﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.SceneXml;
using RSG.Base;
using RSG.Base.Math;
using RSG.Base.OS;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using System.IO;
using RSG;

namespace CheckForRotatedCullplanes
{
    class Program
    {
        #region constants
        private const String OPTION_OUTPUT = "output";
        #endregion // constants
        
        #region members
        private static string m_outputFilePath;
        private static StreamWriter m_outputFileWriter;
        #endregion //members

        private static bool ProcessObj(TargetObjectDef objDef)
        {
            Dictionary<String, Object> allParams = objDef.ParamBlocks.SelectMany(dict => dict)
                         .ToLookup(pair => pair.Key, pair => pair.Value)
                         .ToDictionary(group => group.Key, group => group.First());
            bool usesCullPlane = allParams.ContainsKey("lightUseCullPlane".ToLower());// && (bool)allParams["lightUseCullPlane"] == true;
            if (    usesCullPlane &&
                    objDef.Parent != null
                )
            {
                Matrix34f rotMat = objDef.NodeTransform * objDef.Parent.NodeTransform;
                Quaternionf rotQuat = new Quaternionf(rotMat);
                if(!rotQuat.IsIdentity())
                   return true; 
            }
            return false;
        }

        private static void ProcessFolder(string exportDir)
        {
            string[] sceneXMLFiles = Directory.GetFiles(exportDir, "*.xml", SearchOption.AllDirectories);
            using (StreamWriter fw = new StreamWriter(m_outputFilePath, true))
            {
                foreach (string sceneXMLPath in sceneXMLFiles)
                {
                    Log.Log__Message("Analysing SceneXML \"{0}\"", sceneXMLPath);
                    LoadOptions sceneLoadOpts = LoadOptions.All;
                    Scene scene = Scene.Load(sceneXMLPath, sceneLoadOpts, false);
                    List<string> concernedLights = new List<string>();
                    foreach (TargetObjectDef objDef in scene.Walk(Scene.WalkMode.DepthFirst))
                    {
                        if(ProcessObj(objDef))
                            concernedLights.Add(objDef.Name);
                    }
                    if (concernedLights.Count>0)
                        fw.WriteLine("{0} ({1})", sceneXMLPath, String.Join(",", concernedLights.ToArray()));
                }
            }
        }

        static void Main(string[] args)
        {
 
            #region parameters
            // Define our executable options.  Stick with the long options
            // (e.g. --opt) rather than the single character versions.
            LongOption[] lopts = new LongOption[]
            {
                new LongOption(OPTION_OUTPUT, LongOption.ArgType.Required, 'o'),
            };
            Getopt options = new Getopt(args, lopts);
            #endregion // parameters

            if(!options.HasOption(OPTION_OUTPUT))
            {
                Log.Log__Error("No output file defined (-o[utput]) ?!");
                Environment.ExitCode = 1;
                return;
            }
            m_outputFilePath = options[OPTION_OUTPUT] as string;

            IConfig config = ConfigFactory.CreateConfig();
            string exportDir = config.CoreProject.DefaultBranch.Export;
            if (options.TrailingOptions.Length > 0)
                exportDir += "/"+options.TrailingOptions[0];

            if(!Directory.Exists(exportDir))
            {
                Log.Log__Error("Given export file path doesn't exis {0}!", exportDir);
                Environment.ExitCode = 1;
                return;
            }

            LogFactory.CreateUniversalLogFile(Log.StaticLog);
            LogFactory.CreateApplicationConsoleLogTarget();

            Log.Log__Message("Starting CheckForRotatedCullplanes. Export root folder: {0}, output file: {1}.\n", exportDir, m_outputFilePath);

             ProcessFolder(exportDir);
        }
    }
}
