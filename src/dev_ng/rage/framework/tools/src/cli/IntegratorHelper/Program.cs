﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using LitJson;

using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Base.SCM;
using RSG.Pipeline.Core;
using RSG.SourceControl.Perforce;
using RSG.Base.Configuration;

namespace IntegratorHelper
{
    /// <summary>
    /// Main entry point class
    /// </summary>
    class Program
    {
        #region Constants
        private static readonly String AUTHOR = "Derek Ward";
        private static readonly String EMAIL = "derek.ward@rockstarnorth.com";
        private static readonly String APP_NAME = "IntegratorHelper";
        private static readonly string HELP_URL = "https://devstar.rockstargames.com/wiki/index.php";

        private static readonly String OPTION_HELP = "help";

        private static readonly String LOG_CTX = "Integrator Helper";
        #endregion // Constants

        #region Properties
        #endregion //Properties

        #region Main
        /// <summary>
        /// Main entry point
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        static int Main(string[] args)
        {
            int result = Constants.Exit_Success;

            LongOption[] lopts = new LongOption[] {
                    new LongOption(OPTION_HELP, LongOption.ArgType.Optional,   "Show help/usage")                         
                };

            CommandOptions options = new CommandOptions(args, lopts);

            bool help = options.ContainsOption(OPTION_HELP);

            IUniversalLog log = LogFactory.CreateUniversalLog(LOG_CTX);
            IUniversalLogTarget logFile = LogFactory.CreateUniversalLogFile(log);
            LogFactory.CreateApplicationConsoleLogTarget();

            if (options.ShowHelp)
            {
                log.MessageCtx(LOG_CTX, options.GetUsage());
                return result;
            }

            IConfig config = ConfigFactory.CreateConfig();

            try
            {
                String version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                String helpLink = HELP_URL;
                String appAndVersion = String.Format("{0} v{1}", APP_NAME, version);
                String startupMsg = String.Format("=== {0} ===\n Command Line: {1}.exe {2}\n", appAndVersion, APP_NAME, string.Join(" ", args));
                log.MessageCtx(LOG_CTX, startupMsg);

                using (P4 p4 = config.CoreProject.SCMConnect())
                {
                    try
                    {
                        String branchSpec;
                        IEnumerable<String> depotPathFilters;
                        IEnumerable<String> validUserNames;
                        IEnumerable<String> invalidClients;
                        String resolveOptions;
                        String reverseIntegration;
                        String additionalComment;
                        bool resolve;
                                                
                        foreach (String integrationType in options.TrailingArguments)
                        {
                            switch (integrationType)
                            {
                                // Rdr3 -> Dev ng
                                case "rdr3_dev_ng_derek":
                                    GetIntegratorSettingsRdr3ToDevNg(out branchSpec, out depotPathFilters, out validUserNames, out invalidClients, out resolveOptions, out reverseIntegration, out additionalComment, out resolve);
                                    RunIntegrates(log, p4, branchSpec, depotPathFilters, validUserNames, invalidClients, resolveOptions, reverseIntegration, additionalComment, resolve);
                                    break;
                                default:
                                    log.WarningCtx("Unknown integrationType {0}", integrationType);
                                    break;
                            }                            
                        }                        
                    }
                    catch (P4API.Exceptions.P4APIExceptions ex)
                    {
                        log.ToolException(ex, "Unhandled p4 exception ({0} [{1}]).", p4.Port, p4.User);
                    }
                }
            }
            catch (Exception ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "*** Top Level Exception in {0} ***\n===Msg===\n{1}\n===Stack trace===\n{2}", APP_NAME, ex.Message, ex.StackTrace);
            }

            if (log.HasErrors)
                result = Constants.Exit_Failure;

            Environment.ExitCode = result;
            log.MessageCtx(LOG_CTX, "Exiting with {0}", result);

            LogFactory.ApplicationShutdown(LogFactory.ShutdownMode.DisplayOnError);

            return (result);
        } // Main

        #endregion // main

        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="log"></param>
        /// <param name="p4"></param>
        /// <param name="branchSpec"></param>
        /// <param name="depotPathFilters"></param>
        /// <param name="validUserNames"></param>
        /// <param name="invalidClients"></param>
        /// <param name="resolveOptions"></param>
        /// <param name="reverseIntegration"></param>
        /// <param name="additionalComment"></param>
        /// <param name="resolve"></param>
        private static void RunIntegrates(IUniversalLog log, P4 p4, String branchSpec, IEnumerable<String> depotPathFilters, IEnumerable<String> validUserNames, IEnumerable<String> invalidClients, String resolveOptions, String reverseIntegration, String additionalComment, bool resolve)
        {
            ICollection<Changelist> allChangelists = new List<Changelist>();
            foreach (string depotPathFilter in depotPathFilters)
            {
                P4API.P4RecordSet recordSet = p4.Run("interchanges", true, "-l", "-b", branchSpec, reverseIntegration, "-s", depotPathFilter);
                p4.LogP4RecordSetMessages(recordSet);

                Changelist[] changelists = Changelist.Create(p4, recordSet);
                foreach (Changelist cl in changelists)
                {
                    if (validUserNames.Contains(cl.Username) &&
                        !invalidClients.Contains(cl.Client.ToLower()))
                    {
                        log.MessageCtx(LOG_CTX, "{0}: {1}: {2}: {3}", cl.Number, cl.Username, cl.Client, cl.Description.Substring(0, Math.Min(cl.Description.Length, 40)));
                        allChangelists.Add(cl);
                    }
                }
            }

            int i = 0;
            int numChangelists = allChangelists.Count();
            log.MessageCtx(LOG_CTX, "Integrating {0} changelists", numChangelists);
            foreach (Changelist cl in allChangelists)
            {
                i++;
                // Get description of original CL
                string desc = cl.Description;

                // Create new changelist
                String newDescription = String.Format("Integrate ({0}) (using IntegrateHelper) : {1}\r\n\"\r\n{2}\r\n\"\n", branchSpec, additionalComment, desc);
                P4API.P4PendingChangelist pendingCL = p4.CreatePendingChangelist(newDescription);
                log.MessageCtx(LOG_CTX, "Pending CL {0} created", pendingCL.Number);

                P4API.P4RecordSet recordSet = p4.Run("integrate", true, "-c", pendingCL.Number.ToString(), "-b", branchSpec, reverseIntegration, "-s", String.Format("//...@{0},{0}", cl.Number));
                p4.LogP4RecordSetMessages(recordSet);

                log.MessageCtx(LOG_CTX, "({0}/{1}) Integrated {2} into pending CL {3}", i, numChangelists, cl.Number, pendingCL.Number);

                if (resolve)
                {
                    ICollection<string> resolveArgs = new List<string>();
                    resolveArgs.Add(resolveOptions);
                    resolveArgs.Add("-c");
                    resolveArgs.Add(pendingCL.Number.ToString());

                    P4API.P4RecordSet parsedRecordSetResolve = p4.Run("resolve", true, resolveArgs.ToArray());
                    p4.LogP4RecordSetMessages(parsedRecordSetResolve);
                }

                log.MessageCtx(LOG_CTX, "-----------------------------------------------------------");
                log.MessageCtx(LOG_CTX, "Now resolve and submit CL {0}", pendingCL.Number);
                log.MessageCtx(LOG_CTX, "Press enter to continue to to next integration...");
                log.MessageCtx(LOG_CTX, "-----------------------------------------------------------");
                String read = Console.ReadLine();
            }
        }

        /// <summary>
        /// Make your own customisable integration settings - leave each one as you find it.
        /// </summary>
        /// <param name="branchSpec"></param>
        /// <param name="depotPathFilters"></param>
        /// <param name="validUserNames"></param>
        /// <param name="invalidClients"></param>
        /// <param name="resolveOptions"></param>
        /// <param name="reverseIntegration"></param>
        /// <param name="additionalComment"></param>
        /// <param name="resolve"></param>
        private static void GetIntegratorSettingsRdr3ToDevNg(out String branchSpec, out IEnumerable<String> depotPathFilters, out IEnumerable<String> validUserNames, out IEnumerable<String> invalidClients, out String resolveOptions, out String reverseIntegration, out String additionalComment, out bool resolve)
        {
            branchSpec = "gta5_ng_to_rdr3";
            depotPathFilters = new List<String>() { "//rage/rdr3/...", "//rdr3..." };
            validUserNames = new List<String>() { "derek", "buildernorth" };
            invalidClients = new List<String>() { "ediw-navbuild" };
            resolveOptions = "-am";
            reverseIntegration = "-r";
            additionalComment = "testing 123";
            resolve = true;
        }
        #endregion // Private Methods
    } // class Program
} // namespace ProjectGenerator
