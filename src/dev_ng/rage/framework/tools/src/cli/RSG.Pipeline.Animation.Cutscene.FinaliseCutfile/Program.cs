﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using RSG.Base.OS;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Core;
using RSG.Base.Configuration;

namespace RSG.Pipeline.Animation.Cutscene.FinaliseCutfile
{
    class Program
    {
        #region Constants
        private static readonly String OPTION_INPUTDIR = "inputdir";
        private static readonly String OPTION_INPUTFACEDIR = "inputfacedir";
        private static readonly String OPTION_OUTPUTDIR = "outputdir";
        private static readonly String OPTION_SCENENAME = "scenename";
        private static readonly String OPTION_CUT_SECTION = "cutfsection";
        private static readonly String OPTION_TASK_NAME = "taskname";
        private static readonly String OPTION_ENFORCE_ONE_CAMERA_CUT = "enforceAtLeastOneCameraCut";
        private static readonly String LOG_CTX = "Finalise Cutfile";
        #endregion // Constants

        static int RunProcess(string strExe, string strArgs, IUniversalLog log)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.Arguments = strArgs;
            startInfo.FileName = strExe;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.CreateNoWindow = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false;

            log.MessageCtx(LOG_CTX, strExe + " " + strArgs);

            Process proc = Process.Start(startInfo);
            String output = proc.StandardOutput.ReadToEnd();
            proc.WaitForExit();

            log.MessageCtx(LOG_CTX, output);

            return proc.ExitCode;
        }

        [STAThread]
        static int Main(string[] args)
        {
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.CreateUniversalLog("FinaliseCutfile");
            try
            {
                // Define our executable options.  Stick with the long options
                // (e.g. --opt) rather than the single character versions.
                LongOption[] lopts = new LongOption[] {
                new LongOption(OPTION_INPUTDIR, LongOption.ArgType.Required,
                    "Input directory."),
                new LongOption(OPTION_INPUTFACEDIR, LongOption.ArgType.Required,
                    ""),
                new LongOption(OPTION_OUTPUTDIR, LongOption.ArgType.Required,
                    "Output Directory."),
                new LongOption(OPTION_SCENENAME, LongOption.ArgType.Required,
                    ""),
                new LongOption(OPTION_CUT_SECTION, LongOption.ArgType.Required,
                    ""),
                new LongOption(OPTION_TASK_NAME, LongOption.ArgType.Required,
                    "Asset Pipeline 3 Task Name."),
                new LongOption(OPTION_ENFORCE_ONE_CAMERA_CUT, LongOption.ArgType.None,
                    "If specified, will ensure that the data has at least one camera cut, and will error if not."),
                    
                };
                CommandOptions options = new CommandOptions(args, lopts);

                // Turn args into vars
                String inputDir = options[OPTION_INPUTDIR] as String;
                String inputFaceDir = options[OPTION_INPUTFACEDIR] as String;
                String outputDir = options[OPTION_OUTPUTDIR] as String;
                String sceneName = options[OPTION_SCENENAME] as String;
                String cutfSectionExe = options[OPTION_CUT_SECTION] as String;
                String taskName = options[OPTION_TASK_NAME] == null ? String.Empty : options[OPTION_TASK_NAME] as String;
				
                if (String.Empty != taskName)
                {
                    Console.WriteLine("TASK: {0}", taskName);
                    log.MessageCtx(LOG_CTX, "TASK: {0}", taskName);
                }

                if (!Directory.Exists(inputDir))
                {
                    log.ErrorCtx(LOG_CTX, "Directory '{0}' does not exist", inputDir);
                    return RSG.Pipeline.Core.Constants.Exit_Failure;
                }

                log.MessageCtx(LOG_CTX, "Creating directory '{0}'", outputDir);

                String decalFile = String.Format("{0}\\common\\data\\effects\\decals_cs.dat", options.CoreProject.DefaultBranch.Build);

                string cutfSectionArgs = String.Format("-inputDir \"{0}\" -cutsceneFile \"{0}/data_stream.cutxml\" -outputCutfile \"{1}/data_stream.cutxml\" -outputBinaryCutfile \"{1}/{2}\" -inputFaceDir {3} -hideAlloc -nopopups -coreDecalFile {4}",
                    inputDir, outputDir, sceneName, inputFaceDir, decalFile);

                if (options.Branch.Project.IsDLC)
                {
					string DlcDecalFile = String.Format("{0}\\common\\data\\effects\\decals_cs.dat",options.Branch.Build);
                    cutfSectionArgs += String.Format(" -dlcDecalFile {0}", DlcDecalFile);
                }

                if (options.ContainsOption(OPTION_ENFORCE_ONE_CAMERA_CUT))
                {
                    cutfSectionArgs += String.Format(" -{0}", OPTION_ENFORCE_ONE_CAMERA_CUT);
                }

                if (RunProcess(cutfSectionExe, cutfSectionArgs, log) != 0)
                {
                    if (Directory.Exists(outputDir))
                        Directory.Delete(outputDir, true);
                    return RSG.Pipeline.Core.Constants.Exit_Failure;
                }

                return RSG.Pipeline.Core.Constants.Exit_Success;
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Unhandled Exception during Finalise Cutfile Process");
                return RSG.Pipeline.Core.Constants.Exit_Failure;
            }
            finally
            {
                LogFactory.ApplicationShutdown();
            }
        }
    }
}
