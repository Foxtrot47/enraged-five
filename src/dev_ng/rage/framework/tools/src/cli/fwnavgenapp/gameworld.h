//
// fwnavgenapp/gameworld.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef FWNAVGENAPP_GAMEWORLD_H
#define FWNAVGENAPP_GAMEWORLD_H

#include "atl/array.h"
#include "fwnavgen/basetool.h"

//-----------------------------------------------------------------------------

namespace rage
{

class phInst;

//-----------------------------------------------------------------------------

/*
PURPOSE
	This subclass of fwLevelProcessToolGameInterface represents a very simple
	game world consisting of only stationary bounds, which navigation data will
	be built from. It initializes some of the streaming systems and loads
	level data from an RPF file, compatible with GTA5's assets.
*/
class fwNavToolGameWorld : public fwLevelProcessToolGameInterface
{
public:
	fwNavToolGameWorld();
	~fwNavToolGameWorld();

	virtual bool InitGame();
	virtual void ShutdownGame();

	virtual void Update1();
	virtual void Update2();

	void AddInst(phInst& inst);
	void RemoveInst(phInst& inst);

	const atArray<phInst*>& GetLoadedInsts() const
	{	return m_LoadedInsts;	}

	static fwNavToolGameWorld& GetInstance()
	{	Assert(sm_Instance);
		return *sm_Instance;
	}

protected:
	// PURPOSE:	Linked list of physics instances representing the currently
	//			loaded objects in the world.
	atArray<phInst*>	m_LoadedInsts;

	static fwNavToolGameWorld*	sm_Instance;
};

//-----------------------------------------------------------------------------

}	// namespace rage

//-----------------------------------------------------------------------------

#endif	// FWNAVGENAPP_GAMEWORLD_H

// End of file 'fwnavgen/gameworld.h'
