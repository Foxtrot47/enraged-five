//
// fwnavgenapp/main.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "exportcollision.h"
#include "gameworld.h"

using namespace rage;

//-----------------------------------------------------------------------------

#define LARGE_BUDDY_HEAP
#define SIMPLE_HEAP_SIZE		(200*1024)
#define SIMPLE_PHYSICAL_SIZE	(352*1024)

#include "fwnavgen/main.h"

namespace rage
{
	int NavMeshMakerMain();
}

PARAM(exportall, "This will export all navmeshes");
PARAM(exportsectors, "This will export specified navmeshes (x1,y1,x2,y2)");
PARAM(exportFolder, "Specifies the folder that the navmeshes should be written to.");

//-----------------------------------------------------------------------------

int Main()
{
	if(PARAM_exportsectors.Get() || PARAM_exportall.Get())
	{
		// In this case, we will load level data and export intermediate files, which
		// can later be used as input for this tool to build navigation meshes from.
		fwLevelProcessToolImpl<fwNavToolGameWorld, fwNavMeshDataExporterTool> myApp;
		myApp.Run();
	}
	else
	{
		// Here, we call into the main nav mesh builder function, which will interpret
		// command line parameters such as -sample and -merge, and generate or process
		// navigation data accordingly.
		NavMeshMakerMain();
	}

	return 0;
}

//-----------------------------------------------------------------------------

// End of file 'fwnavgenapp/main.cpp'
