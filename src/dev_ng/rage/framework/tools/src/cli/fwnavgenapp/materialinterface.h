//
// fwnavgenapp/materialinterface.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "fwnavgen/materialinterface.h"

//-----------------------------------------------------------------------------

namespace rage
{

//-----------------------------------------------------------------------------

/*
PURPOSE
	Minimal implementation of fwNavMeshMaterialInterface, without supporting
	any special material properties.
*/
class fwNavMeshMaterialInterfaceTool : public fwNavMeshMaterialInterface
{
public:

	bool GetIsBakedInAttribute1(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType);
	bool GetIsBakedInAttribute2(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType);
	bool GetIsBakedInAttribute3(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType);
	bool GetIsBakedInAttribute4(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType);
	bool GetIsBakedInAttribute5(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType);
	bool GetIsBakedInAttribute6(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType);
	bool GetIsBakedInAttribute7(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType);
	bool GetIsBakedInAttribute8(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType);

	bool IsThisSurfaceTypeGlass(phMaterialMgr::Id iSurfaceType);
	bool IsThisSurfaceTypeStairs(phMaterialMgr::Id iSurfaceType);
	bool IsThisSurfaceTypePavement(phMaterialMgr::Id iSurfaceType);
	bool IsThisSurfaceTypeRoad(phMaterialMgr::Id iSurfaceType);
	bool IsThisSurfaceTypeSeeThrough(phMaterialMgr::Id iSurfaceType);
	bool IsThisSurfaceTypeShootThrough(phMaterialMgr::Id iSurfaceType);
	bool IsThisSurfaceWalkable(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType);
	float GetPedDensityForThisSurfaceType(phMaterialMgr::Id iSurfaceType);
};

//-----------------------------------------------------------------------------

}	// namespace rage

//-----------------------------------------------------------------------------

// End of file 'fwnavgenapp/materialinterface.h'
