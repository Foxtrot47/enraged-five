//
// fwnavgenapp/exportcollision.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef FWNAVGENAPP_EXPORTCOLLISION_H
#define FWNAVGENAPP_EXPORTCOLLISION_H

#include "fwnavgen/baseexportcollisiongrid.h"

namespace rage
{

class phInst;

//-----------------------------------------------------------------------------

/*
PURPOSE
	This class works as a simple exporter of static level geometry, outputting
	intermediate (.tri) files which can be used to build navigation data from.
	It's meant to be used with an fwNavToolGameWorld object for loading the
	world data.
*/
class fwNavMeshDataExporterTool : public fwExportCollisionGridTool
{
public:
	fwNavMeshDataExporterTool();
	virtual ~fwNavMeshDataExporterTool();

	// -- fwLevelProcessToolExporterInterface interface --

	virtual void PreInit();
	virtual bool UpdateExport();

protected:

	// -- fwNavMeshDataExporterTool internal interface --

	bool ConsiderStartExporting();
	bool MaybeExportInst(phInst& inst, const Vector3& vNavMeshMinsExtra, const Vector3& vNavMeshMaxsExtra,
			std::vector<Vector3>& triangleVertices, std::vector<phMaterialMgr::Id>& triangleMaterials, std::vector<u16>& colPolyFlags);

	// -- fwExportCollisionGridTool internal interface --

	virtual void GridCellFinished();
	virtual void UpdateStreamingPos(const Vector3& pos);
};

//-----------------------------------------------------------------------------

}	// namespace rage

#endif	// FWNAVGENAPP_EXPORTCOLLISION_H

// End of file 'fwnavgenapp/exportcollision.h'
