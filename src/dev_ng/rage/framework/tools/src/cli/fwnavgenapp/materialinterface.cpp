//
// fwnavgenapp/materialinterface.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "materialinterface.h"

using namespace rage;

//-----------------------------------------------------------------------------

bool fwNavMeshMaterialInterfaceTool::GetIsBakedInAttribute1(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool fwNavMeshMaterialInterfaceTool::GetIsBakedInAttribute2(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool fwNavMeshMaterialInterfaceTool::GetIsBakedInAttribute3(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool fwNavMeshMaterialInterfaceTool::GetIsBakedInAttribute4(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool fwNavMeshMaterialInterfaceTool::GetIsBakedInAttribute5(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool fwNavMeshMaterialInterfaceTool::GetIsBakedInAttribute6(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool fwNavMeshMaterialInterfaceTool::GetIsBakedInAttribute7(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool fwNavMeshMaterialInterfaceTool::GetIsBakedInAttribute8(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	return false;
}

bool fwNavMeshMaterialInterfaceTool::IsThisSurfaceTypeGlass(phMaterialMgr::Id iSurfaceType)
{
	return false;
}

bool fwNavMeshMaterialInterfaceTool::IsThisSurfaceTypeStairs(phMaterialMgr::Id iSurfaceType)
{
	return false;
}

bool fwNavMeshMaterialInterfaceTool::IsThisSurfaceTypePavement(phMaterialMgr::Id iSurfaceType)
{
	return false;
}

bool fwNavMeshMaterialInterfaceTool::IsThisSurfaceTypeRoad(phMaterialMgr::Id iSurfaceType)
{
	return false;
}

bool fwNavMeshMaterialInterfaceTool::IsThisSurfaceTypeSeeThrough(phMaterialMgr::Id iSurfaceType)
{
	return false;
}

bool fwNavMeshMaterialInterfaceTool::IsThisSurfaceTypeShootThrough(phMaterialMgr::Id iSurfaceType)
{
	return false;
}

bool fwNavMeshMaterialInterfaceTool::IsThisSurfaceWalkable(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType))
{
	// All surfaces generate navmesh for now.  Might want to mark underwater surfaces (ocean floor, etc) as non-walkable
	return true;
}

float fwNavMeshMaterialInterfaceTool::GetPedDensityForThisSurfaceType(phMaterialMgr::Id /*iSurfaceType*/)
{
	return 1.0f;
}

//-----------------------------------------------------------------------------

// End of file 'fwnavgenapp/materialinterface.cpp'
