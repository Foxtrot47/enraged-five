//
// fwnavgenapp/gameworld.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "gameworld.h"
#include "file/device_relative.h"
#include "fwscene/stores/staticboundsstore.h"
#include "streaming/CacheLoader.h"
#include "streaming/packfilemanager.h"
#include "streaming/streamingengine.h"

using namespace rage;

//-----------------------------------------------------------------------------

#define VIRTUAL_STREAMING_BUFFER			0	
#define PHYSICAL_STREAMING_BUFFER			350000	

// Max number of archives
#define ARCHIVE_COUNT					(1280)

#define DEFAULT_BUILD_PATH			"x:/gta5/build/dev/"
#define DEFAULT_LEVEL_RPF			"platform:/levels/testbed/testbed.rpf"

// Example: -input=platform:/levels/gta5/_hills/country_01.rpf
PARAM(input, "Name of an RPF file to load level data from");

//-----------------------------------------------------------------------------

namespace rage
{

struct fwNavToolStaticBoundsStoreInterface : public fwStaticBoundsStoreInterface
{
#if HACK_GTA4
	virtual phInst* AddBoundToPhysicsLevel(phBound* pBound, s32 index, u32 /*nBoundTypeAndIncludeFlag*/)
#else // HACK_GTA4
	virtual phInst* AddBoundToPhysicsLevel(phBound* pBound, s32 index)
#endif // HACK_GTA4
	{
		//
		// create archetype
		phArchetype* pArch = rage_new phArchetypeDamp;
		//	pArch->SetTypeFlags(...);
		// set flags in archetype to say what type of physics object we wish to collide with
		//	pArch->SetIncludeFlags(...);

		// create physics instance and add to physics world
		Matrix34 mat;
		mat.Identity();
		phInst* pInst = rage_new phInst;

		pArch->SetBound(pBound);
		mat.Identity();

		pInst->Init(*pArch, mat);

		fwNavToolGameWorld::GetInstance().AddInst(*pInst);

		return pInst;
	}

	virtual void RemoveInstFromPhysicsWorld(phInst* pInst)
	{
		Assert(pInst);
		fwNavToolGameWorld::GetInstance().RemoveInst(*pInst);

		// deleting inst also deletes archetype
		delete pInst;
	}

};	// struct fwNavToolStaticBoundsStoreInterface

}	// namespace rage

static fwNavToolStaticBoundsStoreInterface g_StaticBoundsStoreInterface;

//-----------------------------------------------------------------------------

namespace rage
{

class fwNavGenToolStreamingInterface : public strStreamingInterface
{
public:
	fwNavGenToolStreamingInterface(int virtualSize, int physicalSize, int archiveCount)
			: strStreamingInterface(virtualSize, physicalSize, archiveCount)
	{}

	virtual void LoadAllRequestedObjects(bool bPriorityRequestsOnly = false)
	{	strStreamingEngine::GetLoader().LoadAllRequestedObjects(bPriorityRequestsOnly);	}

};	// class fwNavGenToolStreamingInterface

}	// namespace rage

//-----------------------------------------------------------------------------

static fiDeviceRelative gPlatformDevice;
static fiDeviceRelative gCommonDevice;

#define COMMON_DIRECTORY		"common/"
#define PLATFORM_DIRECTORY		"pc/"

#define MAX_DIRNAME_CHARS	(128)

static void sSetupDevices()
{
	// Adapted from CFileMgr::SetupDevices():

	char commonFolder[MAX_DIRNAME_CHARS];
	char platformFolder[MAX_DIRNAME_CHARS];

	safecpy(commonFolder, COMMON_DIRECTORY);
	safecpy(platformFolder, PLATFORM_DIRECTORY);

	const char* pCommonFolder = commonFolder;
	const char* pPlatformFolder = platformFolder;
	

	gCommonDevice.Init(pCommonFolder, false);
	gCommonDevice.MountAs("common:/");

	gPlatformDevice.Init(pPlatformFolder, false);
	gPlatformDevice.MountAs("platform:/");
}

//-----------------------------------------------------------------------------

fwNavToolGameWorld*	fwNavToolGameWorld::sm_Instance = NULL;

fwNavToolGameWorld::fwNavToolGameWorld()
{
	Assert(!sm_Instance);
	sm_Instance = this;
}


fwNavToolGameWorld::~fwNavToolGameWorld()
{
	Assert(sm_Instance == this);
	sm_Instance = NULL;
}


bool fwNavToolGameWorld::InitGame()
{
	ASSET.SetPath(DEFAULT_BUILD_PATH);
	sSetupDevices();

	const s32 cpuId = __XENON ? 1 : 0;
	pgStreamer::InitClass(cpuId, __PPU?PRIO_HIGHEST:PRIO_NORMAL); // MAX_INT32 since the streamer is now ignoring this value.

	// enable rage physics reference counting
	phConfig::EnableRefCounting(true);
	phConfig::FreezeRefCounting();

	strStreamingEngine::InitClass(rage_new fwNavGenToolStreamingInterface(
		VIRTUAL_STREAMING_BUFFER << 10, 
		PHYSICAL_STREAMING_BUFFER << 10,
		ARCHIVE_COUNT));

	strStreamingEngine::Init();

    fwQuadTreeNode::InitPool();
	fwPtrNodeSingleLink::InitPool(30000);	// pool initially 70000

	g_StaticBoundsStore.SetInterface(&g_StaticBoundsStoreInterface);
	g_StaticBoundsStore.SetBoxStreamerSize(DEFAULT_BOUNDS_BB_EXTRASIZE);
	g_StaticBoundsStore.Init();

	g_StaticBoundsStore.RegisterStreamingModule();

	strStreamingEngine::GetInfo().AllocateStreamingInfoArray();
	strPackFileManager::RegisterGlobalCollections();

	strPackFileManager::GetRawStreamerImageIndex();		// This seems to initialize something important as a side effect!

	const char *inputName = DEFAULT_LEVEL_RPF;
	PARAM_input.Get(inputName);

	strPackFileManager::AddImageToList(inputName, true, -1, false);


	strStreamingEngine::InitLevelWithMapUnloaded();


	strCacheLoader::Disable();
	strCacheLoader::Init();
/*	bool bCacheLoaded =*/ strCacheLoader::Load();
	g_StaticBoundsStore.UpdateChangedStaticBounds();


	// From void CFileLoader::PostProcessIplLoad():

	//g_IplStore.GetBoxStreamer().PostProcess();
	//g_PhysicsStore.GetBoxStreamer().PostProcess();
	//g_PhysicsStore.SetInitialising(false);
	g_StaticBoundsStore.GetBoxStreamer().PostProcess();

	//g_PhysicsStore.RemoveAll();
	g_StaticBoundsStore.RemoveAll();

	return true;
}


void fwNavToolGameWorld::ShutdownGame()
{
	//g_PhysicsStore.RemoveAll();
	g_StaticBoundsStore.RemoveAll();

	strStreamingEngine::ShutdownSession();

	strStreamingEngine::ShutdownLevelWithMapUnloaded();

	strCacheLoader::Shutdown();

	g_StaticBoundsStore.Shutdown();

	fwPtrNodeSingleLink::ShutdownPool();
	fwQuadTreeNode::ShutdownPool();

	strStreamingEngine::Shutdown();

	delete g_strStreamingInterface;
	g_strStreamingInterface = NULL;
	//strStreamingEngine::ShutdownClass();

	pgStreamer::ShutdownClass();
}


void fwNavToolGameWorld::Update1()
{
}


void fwNavToolGameWorld::Update2()
{
}


void fwNavToolGameWorld::AddInst(phInst& inst)
{
	m_LoadedInsts.Grow() = &inst;
}

void fwNavToolGameWorld::RemoveInst(phInst& inst)
{
	const int index = m_LoadedInsts.Find(&inst);	// O(n)
	if(Verifyf(index >= 0, "Failed to find phInst %p", &inst))
	{
		m_LoadedInsts.DeleteFast(index);
	}
}

//-----------------------------------------------------------------------------

// End of file 'fwnavgen/gameworld.cpp'
