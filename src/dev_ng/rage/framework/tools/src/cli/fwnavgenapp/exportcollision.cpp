//
// fwnavgenapp/exportcollision.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "exportcollision.h"
#include "gameworld.h"
#include "materialinterface.h"
#include "ai/navmesh/navmesh.h"
#include "ai/navmesh/navmeshextents.h"
#include "fwnavgen/helperfunc.h"
#include "fwscene/stores/staticboundsstore.h"
#include "system/param.h"

XPARAM(exportsectors);

using namespace rage;

//-----------------------------------------------------------------------------

fwNavMeshDataExporterTool::fwNavMeshDataExporterTool()
{
}


fwNavMeshDataExporterTool::~fwNavMeshDataExporterTool()
{
}


void fwNavMeshDataExporterTool::PreInit()
{
	// TODO: Move this somewhere else?
	CPathServerExtents::m_iNumSectorsPerNavMesh = 3;	// In the game, this ultimately comes from common:/data/nav.dat.
}

bool fwNavMeshDataExporterTool::UpdateExport()
{
	if(!m_bExporting)
	{
		if(!ConsiderStartExporting())
		{
			// Failed
			return true;
		}

		m_OutputPath[0] = 0;
		GetOutputFolder();
		if(!m_OutputPath[0])
		{
			return true;
		}

		// Create the class which decides which attributes to bake into exported navmeshes
		fwExportCollisionBaseTool::SetProjectSpecificDataFilter(rage_new fwNavMeshMaterialInterfaceTool());
	}

	if(!m_bExporting)
	{
		return false;
	}

	m_iNumTrianglesThisTriFile = 0;

	char extrasFileName[1024];
	GenerateFileNames(m_TriFileName, sizeof(m_TriFileName), extrasFileName, sizeof(extrasFileName));

	std::vector<Vector3> triangleVertices;
	std::vector<phMaterialMgr::Id> triangleMaterials;
	std::vector<u16> colPolyFlags;

	Vector3 vNavMeshMinsExtra = m_vNavMeshMins;	// - Vector3(fNavMeshMinMaxExtra, fNavMeshMinMaxExtra, 0.0f);
	Vector3 vNavMeshMaxsExtra = m_vNavMeshMaxs;	// + Vector3(fNavMeshMinMaxExtra, fNavMeshMinMaxExtra, 0.0f);

	const atArray<phInst*> &instList = fwNavToolGameWorld::GetInstance().GetLoadedInsts();
	const int numInsts = instList.GetCount();
	for(int i = 0; i < numInsts; i++)
	{
		MaybeExportInst(*instList[i], vNavMeshMinsExtra, vNavMeshMaxsExtra, triangleVertices, triangleMaterials, colPolyFlags);
	}

	if(triangleVertices.size())
	{
		FlushTriListToTriFile(triangleVertices, triangleMaterials, colPolyFlags);
	}

	if(m_pCurrentTriFile)
	{
		// Now we've finished writing the tri file, rewind to the start of the file
		// and update the tri-file flags to say whether there was any collision exported here.

		m_pCurrentTriFile->Seek(NAVMESH_EXPORT_TRIFILE_OFFSET_OF_FLAGS);

		u32 iFlags = 0;
		if(m_iNumTrianglesThisTriFile>0)
			iFlags |= NAVMESH_EXPORTFLAG_HAS_COLLISION_MESH;
		m_pCurrentTriFile->WriteInt(&iFlags, 1);

		m_pCurrentTriFile->Close();
		m_pCurrentTriFile = NULL;
	}

	if(!NextCellOrSubSection())
	{
		return true;
	}

	return false;
}


bool fwNavMeshDataExporterTool::ConsiderStartExporting()
{
	return ParseArgumentsAndStartExportCollision();
}


bool fwNavMeshDataExporterTool::MaybeExportInst(phInst& inst,
		const Vector3& vNavMeshMinsExtra, const Vector3& vNavMeshMaxsExtra,
		std::vector<Vector3>& triangleVertices, std::vector<phMaterialMgr::Id>& triangleMaterials, std::vector<u16>& colPolyFlags)
{
	const phArchetype* pArchetype = inst.GetArchetype();

	u16 iColPolyFlag = 0;

	if(pArchetype && pArchetype->GetBound())
	{
		const phBound* pBound = pArchetype->GetBound();

		const Matrix34& instanceMat = RCC_MATRIX34(inst.GetMatrix());

		// Check that the min/max of this bound intersect the min/max of this navmesh area
		Vector3 vBoundMin;
		Vector3 vBoundMax;
		fwNavToolHelperFuncs::TransformObjectAABB(instanceMat,
				RCC_VECTOR3(pBound->GetBoundingBoxMin()),
				RCC_VECTOR3(pBound->GetBoundingBoxMax()),
				vBoundMin, vBoundMax);

		// Test against the bounds of the current navmesh
		if(vBoundMax.x < vNavMeshMinsExtra.x || vBoundMax.y < vNavMeshMinsExtra.y || vBoundMax.z < vNavMeshMinsExtra.z ||
			vBoundMin.x > vNavMeshMaxsExtra.x || vBoundMin.y > vNavMeshMaxsExtra.y || vBoundMin.z > vNavMeshMaxsExtra.z)
		{
			// No intersection possible
			return false;
		}

		fragType* pFragType = NULL;

		ExportBound(pBound, pFragType, instanceMat, triangleVertices, triangleMaterials, colPolyFlags, iColPolyFlag);
		return true;
	}

	return false;

}


void fwNavMeshDataExporterTool::GridCellFinished()
{
}


void fwNavMeshDataExporterTool::UpdateStreamingPos(const Vector3& pos)
{
	// Adapted from CPhysics::LoadAboutPosition().

	// load physics archetypes
	//g_PhysicsStore.GetBoxStreamer().RequestAboutPosition(pos);
	//g_PhysicsStore.GetBoxStreamer().EnsureInMemory(pos);

	// load bounds
	g_StaticBoundsStore.GetBoxStreamer().RequestAboutPosition(pos);
	g_StaticBoundsStore.GetBoxStreamer().EnsureInMemory(pos);
}

//-----------------------------------------------------------------------------

// End of file 'fwnavgenapp/exportcollision.cpp'
