﻿using System;
using System.Collections.Generic;
using SIO = System.IO;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Build;
using RSG.Pipeline.Content;
using RSG.Pipeline.Engine;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Platform;
using RSG.SourceControl.Perforce;
using P4API;

namespace RSG.Pipeline.Convert
{

    /// <summary>
    /// Main Pipeline Conversion executable.
    /// </summary>
    /// This wrapper replaces IronRuby data_convert_file.rb script and was
    /// implemented because of issues in IR64.exe.
    class Program
    {
        #region Constants
        private const String AUTHOR = "RSGEDI Tools";
        private const String EMAIL = "*tools@rockstarnorth.com";
        private const String DEFAULT_PROCESSOR = "RSG.Pipeline.Processor.Platform.Rage";
        private const String ULOG_VIEWER = "$(toolsbin)/UniversalLogViewer/UniversalLogViewer.exe";
        private const String RECURSIVE_WILDCARD_DEFAULT = "*.*";

        private const String LOG_CTX = "Convert";
        private const String OPTION_INPUT = "input";
        private const String OPTION_OUTPUT = "output";
        private const String OPTION_RECURSIVE = "recursive";
        private const String OPTION_RECURSIVE_WILDCARD = "wildcard";
        private const String OPTION_SELECTED = "selected";
        private const String OPTION_REBUILD = "rebuild";
        private const String OPTION_PREVIEW = "preview";
        private const String OPTION_PROCESSOR = "proc";
        private const String OPTION_FILE_LIST = "filelist";
        private const String OPTION_PLATFORMS = "platforms";
        private const String OPTION_FLAG_NO_SYNC = "no-sync";
        private const String OPTION_FLAG_NO_XGE = "no-xge";
        private const String OPTION_FLAG_NO_CONTENT = "no-content";
        private const String OPTION_FLAG_NO_CONTENT_CACHE = "no-content-cache";
        private const String OPTION_FLAG_NO_DEFAULT_RESOLVER = "no-default-resolver";
        private const String OPTION_NPASS_ENGINE = "use-npass-engine";
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// Log.
        /// </summary>
        static IUniversalLog _log;
        #endregion // Member Data

        /// <summary>
        /// Conversion entry-point.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        [STAThread]
        static int Main(String[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += ExceptionHandler;

            // Merge our options with the default pipeline command options.
            // Define our executable options.  Stick with the long options
            // (e.g. --opt) rather than the single character versions.
            LongOption[] opts = new LongOption[] {
                new LongOption(OPTION_INPUT, LongOption.ArgType.Required,
                    "Specify input filename."), 
                new LongOption(OPTION_OUTPUT, LongOption.ArgType.Required,
                    "Specify output filename."), 
                new LongOption(OPTION_RECURSIVE, LongOption.ArgType.None,
                    "Recurse directory search (for directories only)."), 
                new LongOption(OPTION_RECURSIVE_WILDCARD, LongOption.ArgType.Required,
                    "Recursive directory search file wildcard (required: recursive option)."),
                new LongOption(OPTION_SELECTED, LongOption.ArgType.None,
                    "Selected data build; quicker iteration that doesn't build all directory content."),
                new LongOption(OPTION_REBUILD, LongOption.ArgType.None,
                    "Rebuild data; ignoring timestamp information."), 
                new LongOption(OPTION_PREVIEW, LongOption.ArgType.None,
                    "Preview data; output resources to preview folder."), 
                new LongOption(OPTION_PROCESSOR, LongOption.ArgType.Required,
                    "Define processor to use; defaults to RAGE platform conversion."), 
                new LongOption(OPTION_FILE_LIST, LongOption.ArgType.Required,
                    "Filename with a list of files we want to convert."), 
                new LongOption(OPTION_PLATFORMS, LongOption.ArgType.Required,
                    "Override installer set platforms (keys separated by commas or semicolons, e.g. \"ps3;ps4\"); defaults to installer set platforms."),
                new LongOption(OPTION_FLAG_NO_SYNC, LongOption.ArgType.Required,
                    "Disable engine Perforce dependency sync."),
                new LongOption(OPTION_FLAG_NO_XGE, LongOption.ArgType.None,
                    "Disable Xoreax XGE parallel processing engine."),
                new LongOption(OPTION_FLAG_NO_CONTENT, LongOption.ArgType.None,
                    "Disable Content-Tree loading (required for most builds)."),
                new LongOption(OPTION_FLAG_NO_CONTENT_CACHE, LongOption.ArgType.None,
                    "Disable Content-Tree cache loading."),
                new LongOption(OPTION_FLAG_NO_DEFAULT_RESOLVER, LongOption.ArgType.None,
                    "Disable Default resource-only resolver (content-tree process required)."),

                    // DHM TESTING
                new LongOption(OPTION_NPASS_ENGINE, LongOption.ArgType.None,
                    "Use new *experimental* N-pass Engine system.")
            };
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            _log = LogFactory.ApplicationLog;
            CommandOptions options = new CommandOptions(args, opts);
            int exit_code = Constants.Exit_Success;
            
            // Set console window title.
            String initialConsoleTitle = Console.Title;
            Console.Title = String.Format("{0} [{1}]: Asset Build Pipeline, converting: {2}",
                options.Project.FriendlyName, options.Branch.Name, 
                String.Join(", ", options.TrailingArguments));

            try
            {
                // Display help is requested.
                if (options.ShowHelp)
                {
                    String processName = System.Diagnostics.Process.GetCurrentProcess().ProcessName;
                    ShowUsage(processName, options);
                    return (Constants.Exit_Success);
                }
                else if (!Convert(_log, options))
                {
                    _log.ErrorCtx(LOG_CTX, "Data conversion failed.  See previous errors.");
                    exit_code = Constants.Exit_Failure;
                }
            }
            catch (Exception ex)
            {
                exit_code = Constants.Exit_Failure;
                _log.ToolException(ex, "Unhandled exception.");
                RSG.Base.Windows.ExceptionStackTraceDlg dlg =
                    new RSG.Base.Windows.ExceptionStackTraceDlg(ex, EMAIL, AUTHOR);
                dlg.ShowDialog();
            }
            finally 
            {
                // Show log if showing popups.
                if ((Constants.Exit_Failure == exit_code) && (!options.NoPopups))
                {
                    String cmd = options.Config.Environment.Subst(ULOG_VIEWER);
                    String arg = LogFactory.ApplicationLogFilename;
                    System.Diagnostics.Process.Start(cmd, arg);
                }

                LogFactory.ApplicationShutdown();
            
                // Restore console window title.
                Console.Title = initialConsoleTitle;
            }
            return (exit_code);
        }

        #region Private Methods
        /// <summary>
        /// Top-level exception handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void ExceptionHandler(Object sender, UnhandledExceptionEventArgs e)
        {
            _log.ToolException((Exception)e.ExceptionObject, "Unhandled exception.");
            RSG.Base.Windows.ExceptionStackTraceDlg dlg =
                new RSG.Base.Windows.ExceptionStackTraceDlg((Exception)e.ExceptionObject, EMAIL, AUTHOR);
        }

        /// <summary>
        /// Print usage information to console output.
        /// </summary>
        /// <param name="program"></param>
        /// <param name="options"></param>
        private static void ShowUsage(String program, CommandOptions options)
        {
            Console.WriteLine(program);
            Console.WriteLine("Usage:");
            Console.WriteLine(options.GetUsage());
        }

        /// <summary>
        /// Run the conversion.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        private static bool Convert(IUniversalLog log, CommandOptions options)
        {
            // Initialise engine flags.
            EngineFlags flags = EngineFlags.Default;
            if (options.ContainsOption(OPTION_SELECTED))
                flags |= EngineFlags.Selected;
            if (options.ContainsOption(OPTION_REBUILD))
                flags |= EngineFlags.Rebuild;
            if (options.ContainsOption(OPTION_PREVIEW))
                flags |= EngineFlags.Preview;
            if (options.ContainsOption(OPTION_FLAG_NO_SYNC))
                flags &= ~EngineFlags.SyncDependencies;
            if (options.ContainsOption(OPTION_FLAG_NO_XGE))
                flags &= ~EngineFlags.XGE;
            if (options.ContainsOption(OPTION_FLAG_NO_CONTENT))
                flags &= ~EngineFlags.LoadContent;
            if (options.ContainsOption(OPTION_FLAG_NO_CONTENT_CACHE))
                flags &= ~EngineFlags.CacheContent;
            if (options.ContainsOption(OPTION_FLAG_NO_DEFAULT_RESOLVER))
                flags &= ~EngineFlags.DefaultResolverFallback;
            if (options.NoPopups)
                flags &= ~EngineFlags.Popups;
            bool recursive = options.ContainsOption(OPTION_RECURSIVE);
            String recursive_wildcard = options.ContainsOption(OPTION_RECURSIVE_WILDCARD) ?
                (String)options[OPTION_RECURSIVE_WILDCARD] : RECURSIVE_WILDCARD_DEFAULT;
            bool useNPassEngine = options.ContainsOption(OPTION_NPASS_ENGINE);

            // Parse new platform overrides.
            IEnumerable<RSG.Platform.Platform> platforms;
            if (options.ContainsOption(OPTION_PLATFORMS))
            {
                // User defined platforms.
                String[] platformStrings = ((String)options[OPTION_PLATFORMS]).Split(new char[] { ',', ';' });
                platforms = platformStrings.Select(s => Platform.PlatformUtils.PlatformFromString(s));
            }
            else
            {
                // Default: Installer-Enabled Platforms
                platforms = PlatformProcessBuilder.GetInstallerEnabledPlatforms(options.Branch);
            }

            IEngine engine = null;
            if (options.ContainsOption(OPTION_INPUT) &&
                options.ContainsOption(OPTION_OUTPUT) &&
                options.ContainsOption(OPTION_PROCESSOR))
            {
                // Prompt to sync the content-tree in case its required.
                OptionallySyncContentTree(log, options);

                // Initialise engine.
                if (useNPassEngine)
                    engine = new Engine.Engine2(options.Branch, flags, platforms);
                else
                    engine = new Engine.Engine(options, options.Config, options.Project, options.Branch, flags, null, null, platforms);

                String input = (String)options[OPTION_INPUT];
                String output = (String)options[OPTION_OUTPUT];
                String procName = (String)options[OPTION_PROCESSOR];
                IProcessor processor = engine.Processors.FirstOrDefault(p => p.Name.Equals(procName));
                if (null == processor)
                {
                    log.Warning("Invalid processor specified: {0}.  Using default: {1}.",
                        procName, DEFAULT_PROCESSOR);
                    processor = engine.Processors.FirstOrDefault(p => p.Name.Equals(DEFAULT_PROCESSOR));
                    if (null == processor)
                    {
                        log.Error("Default processor invalid: {0}.  Aborting.",
                            DEFAULT_PROCESSOR);
                        return (false);
                    }
                }

                // Initialise content-tree.
                IContentTree tree = null;
                if (flags.HasFlag(EngineFlags.LoadContent))
                    tree = Factory.CreateTree(options.Branch);
                else
                    tree = Factory.CreateEmptyTree(options.Branch);

                ProcessBuilder pb = new ProcessBuilder(processor, tree);
                IContentNode inputNode = null;
                IContentNode outputNode = null;
                if (SIO.Directory.Exists(input))
                    inputNode = tree.CreateDirectory(input);
                else if (SIO.File.Exists(input))
                    inputNode = tree.CreateFile(input);
                else
                {
                    log.Error("Input file or directory '{0}' does not exist.", input);
                    return (false);
                }
                if (SIO.Directory.Exists(output))
                    outputNode = tree.CreateDirectory(output);
                else
                    outputNode = tree.CreateFile(output);

                pb.Inputs.Add(inputNode);
                pb.Outputs.Add(outputNode);

                IInput buildInput = new BuildInput(options.Branch);
                buildInput.RawProcesses.Add(pb.ToProcess());
                TimeSpan ts;
                IEnumerable<IOutput> outputs = null;
                bool buildResult = engine.Build(ref buildInput, out outputs, out ts);
                log.Message("Build Took: {0}.", ts.ToString());

                if (log.HasErrors)
                    buildResult = false;

                return (buildResult);
            }
            else
            {
                // If we're not explcitly set up for DLC; then we can override the project
                // and branch we initialise the Engine with if the filenames passed in are
                // DLC-related.  This is set up here; and allows the convienience of
                // right-click DLC asset builds.
                List<IBranch> branches = new List<IBranch>();
                IEnumerable<String> filenames;

                if (options.ContainsOption(OPTION_FILE_LIST))
                {
                    String fileListFilename = (String)options[OPTION_FILE_LIST];
                    if (!SIO.File.Exists(fileListFilename))
                    {
                        log.Error("File list file {0} doesn't exist.  Aborting.", fileListFilename);
                        return (false);
                    }

                    filenames = EvaluteFileListFiles(fileListFilename);
                }
                else
                {
                    filenames = EvaluateTrailingArguments(options.TrailingArguments,
                        recursive, recursive_wildcard);
                }
                
                if (!filenames.Any())
                {
                    log.Error("No files or directories specified for conversion.  Aborting.");
                    return (false);
                }

                if (options.ContainsOption("branch") && !options.Project.IsDLC)
                {
                    // If our executable was invoked with a branch argument; we try to use
                    // that to match against.  Matching against DLC if required.
                    foreach (IProject dlcProject in options.Config.DLCProjects.Values)
                    {
                        foreach (String filename in filenames)
                        {
                            IBranch branch = dlcProject.Branches.Values.FirstOrDefault(b => b.Name.Equals(options.Branch.Name));
                            if (null != branch &&
                                (branch.IsRootedPath(branch.Export, filename) ||
                                 branch.IsRootedPath(branch.Processed, filename)))
                            {
                                if (!branches.Contains(branch))
                                    branches.Add(branch);
                                break;
                            }
                        }
                    }
                }
                else if (!options.Project.IsDLC)
                {
                    foreach (IProject dlcProject in options.Config.DLCProjects.Values)
                    {
                        foreach (String filename in filenames)
                        {
                            foreach (IBranch branch in dlcProject.Branches.Values)
                            {
                                if (branch.IsRootedPath(branch.Export, filename) ||
                                    branch.IsRootedPath(branch.Processed, filename))
                                {
                                    if (!branches.Contains(branch))
                                        branches.Add(branch);

                                    break;
                                }
                            }
                        }
                    }
                }
                if (1 == branches.Count)
                {
                    options.Project = branches.First().Project;
                    options.Branch = branches.First();
                    log.Message("Switching project and branch context to: {0} ({1}).",
                        options.Project.FriendlyName, options.Branch.Name);

                    // Create engine since we've switched.
                    if (useNPassEngine)
                        engine = new Engine.Engine2(options.Branch, flags, platforms);
                    else
                        engine = new Engine.Engine(options, options.Config, options.Project, options.Branch, flags, null, null, platforms);
                }
                else if (branches.Count > 1)
                {
                    log.Error("Multiple project/branch files detected.  Aborting.");
                    return (false);
                }

                // Fallback for initialising our engine with the current branch and core
                // project as defined previously.
                if (useNPassEngine)
                    engine = new Engine.Engine2(options.Branch, flags, platforms);
                else if (null == engine)
                    engine = new Engine.Engine(options, options.Config, options.Project, options.Branch, flags, null, null, platforms);

                // Prompt to sync the content-tree in case its required; this has to
                // be done after our branch is fully initialised as above.
                OptionallySyncContentTree(log, options);

                TimeSpan ts;
                IEnumerable<IOutput> outputs = null;
                bool buildResult = engine.Build(filenames, out outputs, out ts);
                log.MessageCtx(LOG_CTX, "Build Took: {0}.", ts.ToString());

                if (log.HasErrors)
                    buildResult = false;

                return (buildResult);
            }
        }

        /// <summary>
        /// Optionally sync (prompt) the Content-Tree XML data.
        /// </summary>
        /// <param name="options"></param>
        private static void OptionallySyncContentTree(IUniversalLog log, CommandOptions options)
        {
            if (options.NoPopups || options.ContainsOption(OPTION_FLAG_NO_SYNC))
                return; // No point in checking if we cannot prompt.

            String[] contentTreeFilenames = GetAllContentTreeFilenames(options);
            using (RSG.SourceControl.Perforce.P4 p4 = options.Project.SCMConnect())
            {
                if (p4 == null)
                    return;
                IEnumerable<FileMapping> fms = FileMapping.Create(p4, contentTreeFilenames);
                IEnumerable<String> depotFilenames = fms.Select(fm => fm.DepotFilename);
                List<String> syncArgs = new List<String>();
                syncArgs.Add("-n"); // preview sync
                syncArgs.AddRange(depotFilenames);
                P4RecordSet previewResult = p4.Run("sync", false, syncArgs.ToArray()); 
                List<String> oldDepotFilenames = previewResult.Records.
                     Where(record => record.Fields.ContainsKey("depotFile")).
                     Select(record => record.Fields["depotFile"]).
                     ToList();

                System.Windows.MessageBoxResult userResult = System.Windows.MessageBoxResult.Yes;
                while (oldDepotFilenames.Count() > 0 && System.Windows.MessageBoxResult.Yes == userResult)
                {
                    int taken = Math.Min(oldDepotFilenames.Count(), 5);
                    userResult =
                        MessageBox.Show("{0} {1} Content-Tree is out-of-date.  Would you like to sync {2} files?\n\n{3}{4}",
                            System.Windows.MessageBoxButton.YesNo,
                            System.Windows.MessageBoxImage.Question,
                            System.Windows.MessageBoxResult.No,
                            (uint)20,
                            options.Project.FriendlyName,
                            options.Branch.Name,
                            oldDepotFilenames.Count(),
                            String.Join("\n", oldDepotFilenames.Take(taken)),
                            oldDepotFilenames.Count() > taken ? ", ..." : String.Empty);
                    if (System.Windows.MessageBoxResult.Yes == userResult)
                    {
                        TrySyncFiles(log, p4, depotFilenames.ToArray());
                        contentTreeFilenames = GetAllContentTreeFilenames(options);
                        
                        fms = FileMapping.Create(p4, contentTreeFilenames);
                        depotFilenames = fms.Select(fm => fm.DepotFilename);
                        syncArgs = new List<String>();
                        syncArgs.Add("-n"); // preview sync
                        syncArgs.AddRange(depotFilenames);
                        previewResult = p4.Run("sync", false, syncArgs.ToArray());
                        oldDepotFilenames = previewResult.Records.
                             Where(record => record.Fields.ContainsKey("depotFile")).
                             Select(record => record.Fields["depotFile"]).
                             ToList();
                    }
                }
            }
        }

        /// <summary>
        /// Attempt to sync files from Perforce; logging errors if required.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="p4"></param>
        /// <param name="depotFilenames"></param>
        /// <returns></returns>
        private static bool TrySyncFiles(IUniversalLog log, P4 p4, String[] depotFilenames)
        {
            bool result = true;
            P4RecordSet fileSyncRecords = p4.Run("sync", depotFilenames);
            if (fileSyncRecords.Errors.Any())
            {
                log.ErrorCtx(LOG_CTX, "{0} errors syncing dependencies from Perforce:",
                    fileSyncRecords.Errors.Count());
                fileSyncRecords.Errors.ForEachWithIndex((s, i) =>
                    log.ErrorCtx(LOG_CTX, "\t{0}: {1}", i + 1, s));
                result = false;
            }
            return (result);
        }

        /// <summary>
        /// Return all content-tree files for both core and DLC branches.
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        private static String[] GetAllContentTreeFilenames(CommandOptions options)
        {
            List<String> contentTreeFilenames = new List<String>();
            if (options.Project != options.CoreProject)
            {
                IBranch coreBranch = options.CoreProject.Branches[options.Branch.Name];
                contentTreeFilenames.AddRange(Factory.GetFilenames(coreBranch));
            }
            contentTreeFilenames.AddRange(Factory.GetFilenames(options.Branch));
            return (contentTreeFilenames.ToArray());
        }

        /// <summary>
        /// Evaluate trailing arguments depending on recursive options.
        /// </summary>
        /// <param name="trailingArgs">Trailing arguments sent to executable.</param>
        /// <param name="recursive">Expand directories recursively.</param>
        /// <param name="wildcard">Directory search wildcard (if recursive).</param>
        /// <returns></returns>
        private static IEnumerable<String> EvaluateTrailingArguments(IEnumerable<String> trailingArgs, 
            bool recursive, String wildcard)
        {
            // Expand local paths.
            IEnumerable<String> filenames = trailingArgs.Select(f => SIO.Path.GetFullPath(f));
            
            // Handle recursive call; for directories only.
            if (recursive)
            {
                List<String> expandedFilenames = new List<String>();
                foreach (String filename in filenames)
                {
                    if (SIO.Directory.Exists(filename))
                    {
                        // Directory; expand recursively and add directory search results
                        // to our set.
                        String[] files = SIO.Directory.GetFiles(filename, wildcard, 
                            SIO.SearchOption.AllDirectories);
                        expandedFilenames.AddRange(files);
                    }
                    else
                    {
                        // Simply a filename; so add to our set.
                        expandedFilenames.Add(filename);
                    }
                }
                filenames = expandedFilenames;
            }

            return (filenames);
        }

        /// <summary>
        /// Takes a filename that contains a lists of files that can be parsed and
        /// passed onto the conversion process.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private static IEnumerable<String> EvaluteFileListFiles(String filename)
        {
            SIO.StreamReader fileReader = new SIO.StreamReader(filename);
            List<String> fileList = new List<String>();

            while (!fileReader.EndOfStream)
            {
                String line = fileReader.ReadLine();
                if (!String.IsNullOrEmpty(line))
                {
                    String inputFilename = line.Trim().ToLower();
                    if (SIO.File.Exists(inputFilename))
                        fileList.Add(inputFilename);
                }
            }

            return fileList;
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Convert namespace
