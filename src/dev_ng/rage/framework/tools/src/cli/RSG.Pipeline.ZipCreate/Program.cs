﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using RSG.Base.Collections;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.AssetPack;

namespace RSG.Pipeline.ZipCreate
{
    /// <summary>
    /// Zip create executable.
    /// </summary>
    class Program
    {
        #region Constants
        private static readonly String OPTION_OUTPUT = "output";
        private static readonly String OPTION_FILELIST = "filelist";
        private static readonly String OPTION_DUPE_BEHAVIOUR = "dupebehaviour";

        private static readonly String ZIP_PARAM_RECURSIVE = "recursive";
        #endregion // Constants

        private static void RecurseDirectory(IEnumerable<Pair<String, String>> files, string root, string directory, string wildcard, IUniversalLog log)
        {
            if (Directory.Exists(directory))
            {
                foreach (String pathname in Directory.EnumerateFiles(directory, wildcard))
                {
                    Pair<String, String> pair = new Pair<String, String>(pathname, GetRelativeDir(root, pathname));
                    (files as List<Pair<String, String>>).Add(pair);
                }

                foreach (String sub in Directory.GetDirectories(directory))
                {
                    RecurseDirectory(files, root, sub, wildcard, log);
                }
            }
            else
            {
                log.Warning("Directory does not exist: {0}.", directory);
            }
        }

        private static String GetRelativeDir(String root, String absolute)
        {
            String _root = root.ToLower().Replace("/", "\\");
            String _absolute = absolute.ToLower().Replace("/", "\\");

            String relative = Path.GetDirectoryName(_absolute).Replace(_root, String.Empty);
            return relative;
        }

        /// <summary>
        /// Entry-point.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        [STAThread]
        static int Main(String[] args)
        {
            // Merge our options with the default pipeline command options.
            LongOption[] opts = new LongOption[] {
                new LongOption(OPTION_OUTPUT, LongOption.ArgType.Required,
                    "Specify output ZIP file."),
                new LongOption(OPTION_FILELIST, LongOption.ArgType.Required,
                    "Specify input file listing source and destination files."),
                new LongOption(OPTION_DUPE_BEHAVIOUR, LongOption.ArgType.Required,
                    "Specify what to do if duplicate is encountered"),
            };
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.CreateUniversalLog("ZipCreate");

            CommandOptions options = new CommandOptions(args, opts);
            if (options.ContainsOption("help"))
            {
                log.Message(options.GetUsage());
                return (Constants.Exit_Failure);
            }

            // Verify options.
            if (!options.ContainsOption(OPTION_FILELIST))
            {
                log.Error("'{0}' is a required option.", OPTION_FILELIST);
                log.Message(options.GetUsage());
                return (Constants.Exit_Failure);
            }
            if (!options.ContainsOption(OPTION_OUTPUT))
            {
                log.Error("'{0}' is a required option.", OPTION_OUTPUT);
                log.Message(options.GetUsage());
                return (Constants.Exit_Failure);
            }
            if (!options.ContainsOption(OPTION_DUPE_BEHAVIOUR))
            {
                log.Error("'{0}' is a required option.", OPTION_DUPE_BEHAVIOUR);
                log.Message(options.GetUsage());
                return (Constants.Exit_Failure);
            }
            int result = Constants.Exit_Success;
            String filelist = (String)options[OPTION_FILELIST];
            String output = (String)options[OPTION_OUTPUT];
            DuplicateBehaviour dupeBeh = (DuplicateBehaviour)AssetPackScheduleFile.ResolveDupeBehaviourString((String)options[OPTION_DUPE_BEHAVIOUR]);
            try
            {
                IEnumerable<Pair<String, String>> files = new List<Pair<String, String>>();
                using (FileStream fs = new FileStream(filelist, FileMode.Open, FileAccess.Read))
                {
                    using (StreamReader sr = new StreamReader(fs))
                    {
                        uint counter = 0;
                        String line = String.Empty;
                        while (null != (line = sr.ReadLine()))
                        {
                            String[] parts = line.Split(new char[] { ',', ';' });
                            Debug.Assert(parts.Length > 0, String.Format("Invalid filelist line [{0}]: \"{1}\".",
                                        counter, line));
                            Debug.Assert(parts.Length <= 3, String.Format("Invalid filelist line [{0}]: \"{1}\".",
                                    counter, line));

                            if (parts.Length > 0 && parts.Length <= 2)
                            {
                                if(parts[1].Contains("*"))
                                {
                                    if (Directory.Exists(parts[0]))
                                    {
                                        foreach (String pathname in Directory.EnumerateFiles(parts[0], parts[1]))
                                        {
                                            Pair<String, String> pair = new Pair<String, String>(pathname, Path.GetFileName(pathname));
                                            (files as List<Pair<String, String>>).Add(pair);
                                        }
                                    }
                                    else
                                    {
                                        log.Warning("Directory does not exist: {0}.", parts[0]);
                                    }
                                }
                                else
                                {
                                    Pair<String, String> pair = new Pair<String, String>();
                                    pair.First = parts[0];
                                    if (parts.Length > 1)
                                        pair.Second = parts[1];
                                    else
                                        pair.Second = Path.GetFileName(parts[0]);

                                    (files as List<Pair<String, String>>).Add(pair);
                                }
                            }
                            // Directory with optional destination folder
                            else if (parts.Length > 0 && parts.Length == 3)
                            {
                                if (parts[1].Contains("*"))
                                {
                                    if (Directory.Exists(parts[0]))
                                    {
                                        if (parts[2] == ZIP_PARAM_RECURSIVE)
                                        {
                                            RecurseDirectory(files, parts[0], parts[0], parts[1], log);
                                        }
                                        else
                                        {
                                            foreach (String pathname in Directory.EnumerateFiles(parts[0], parts[1]))
                                            {
                                                Pair<String, String> pair = new Pair<String, String>(pathname, parts[2]);
                                                (files as List<Pair<String, String>>).Add(pair);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        log.Warning("Directory does not exist: {0}.", parts[0]);
                                    }
                                }
                                else
                                {
                                    Pair<String, String> pair = new Pair<String, String>();
                                    pair.First = parts[0];
                                    if (parts.Length > 1)
                                        pair.Second = parts[1];
                                    else
                                        pair.Second = Path.Combine(parts[2], Path.GetFileName(parts[0]));

                                    (files as List<Pair<String, String>>).Add(pair);
                                }
                            }
                            else
                            {
                                log.Error("Invalid filelist line [{0}]: \"{1}\".",
                                    counter, line);
                            }
                            
                            ++counter;
                        }
                    }
                }

                // Handle dupes
                if(dupeBeh == DuplicateBehaviour.AcceptLast)
                    (files as List<Pair<String, String>>).Reverse();

                List<Pair<String, String>> finalFiles = new List<Pair<String, String>>();
                foreach(Pair<String, String> filepair in files)
                {
                    if (finalFiles.Where(f => f.Second == filepair.Second && Path.GetFileName(f.First) == Path.GetFileName(filepair.First)).Count() == 0)
                    {
                        finalFiles.Add(filepair);
                    }
                    // We have a dupe, handle as instructed.
                    else
                    {
                        if (dupeBeh == DuplicateBehaviour.Error)
                        {
                            log.Error("Duplicate file detected: {0}.  Attempting to construct {1}",
                                    filepair.Second, output);
                            return (Constants.Exit_Failure);
                        }
                        else
                        {
                            log.Warning("Duplicate file detected: {0}.  Using duplicate behaviour {1}.",
                                    filepair.Second, dupeBeh.ToString());
                        }
                    }
                }

                //Re-apply the Reverse just in case someone ever expects this to maintain order.
                if (dupeBeh == DuplicateBehaviour.AcceptLast)
                    (finalFiles as List<Pair<String, String>>).Reverse();

                if (System.IO.File.Exists(output))
                {
                    log.Error("{0} already exists.", output);
                    result = Constants.Exit_Failure;
                }
                else if (!Zip.Create(options.Branch, output, finalFiles, null))
                {
                    log.Error("Error constructing zip file: {0}.", output);
                    result = Constants.Exit_Failure;
                }
            }
            catch (Exception ex)
            {
                log.Exception(ex, "Error constructing zip file: {0}.", output);
                result = Constants.Exit_Failure;
            }

            return (result);
        }
    }

} // RSG.Pipeline.ZipCreate namespace
