//
// File: Program.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Program.cs class
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Text;

// Local assemblies
using RSN.Base;

// Middlewares
using Tao.DevIl;

namespace ddsinfo
{

    /// <summary>
    /// 
    /// </summary>
    class Program
    {
        #region Constants
        /// <summary>
        /// Usage string.
        /// </summary>
        private static readonly String USAGE =
            "\nddsinfo [options] <paths>\n" +
            "\nddsinfo Usage:\n\n" +
            "ddsinfo operates on all images specified on the command line, or on all\n" +
            "images within directories specified on the command line." +
            "\n\n" +
            "options:\n" +
            "\t--version|-v\tDisplay version information\n" +
            "\t--help|-h\t\tDisplay this usage information\n" +
            "\t--recurse|-r\t\tRecurse into subdirectories\n" +
            "\t--dimensions|-d\tDisplay image dimensions\n" +
            "\t--compression|-c\tDisplay image compression method\n" +
            "\t--filter|-f [filter]\tUse filter as wildcard for file search (e.g. *.dds)\n\n" +
            "\tE.g. ddsinfo -r -c -d .\n\n";

        private static readonly String FILTER = "*.dds";
        #endregion // Constants

        #region Member Data
        private static cGetOpt ms_GetOpt = null;
        #endregion // Member Data

        #region Entry-Point
        /// <summary>
        /// Entry-Point
        /// </summary>
        /// <param name="args"></param>
        static void Main(String[] args)
        {
            if (!ParseOpts(args))
                return;

            if (ms_GetOpt.Options.ContainsKey("version") ||
                ms_GetOpt.Options.ContainsKey("v"))
                Version();
            else if (ms_GetOpt.Options.ContainsKey("help") ||
                ms_GetOpt.Options.ContainsKey("h"))
                Usage();
            else if (0 == ms_GetOpt.TrailingOptions.Length)
            {
                Usage();
                return;
            }          

            // DevIL Initialisation
            Il.ilInit();

            // Process all paths specified
            Process();

            // DevIL Shutdown
            Il.ilShutDown();
        }
        #endregion // Entry-Point

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        private static bool ParseOpts(String[] argv)
        {
            try
            {
                List<cLongOpt> options = new List<cLongOpt>();
                options.Add(new cLongOpt("help", etOptArgument.None, 'h'));
                options.Add(new cLongOpt("version", etOptArgument.None, 'v'));
                options.Add(new cLongOpt("recurse", etOptArgument.None, 'r'));
                options.Add(new cLongOpt("dimensions", etOptArgument.None, 'd'));
                options.Add(new cLongOpt("compression", etOptArgument.None, 'c'));
                options.Add(new cLongOpt("filter", etOptArgument.Required, 'f'));

                ms_GetOpt = new cGetOpt(argv, options.ToArray());
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine("Argument parsing error: {0}", ex.Message);
                Usage();

                return (false);
            }
            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        private static void Usage()
        {
            Console.WriteLine(USAGE);
        }

        private static void Version()
        {
            Assembly assembly = Assembly.GetEntryAssembly();
            Console.WriteLine("ddsinfo Version: {0}", assembly.GetName().Version.ToString());
            Console.WriteLine("  Rockstar North");
            Console.WriteLine("  by David Muir <david.muir@rockstarnorth.com>");
        }

        private static void Process()
        {
            foreach (String path in ms_GetOpt.TrailingOptions)
            {
                if (File.Exists(path))
                {
                    ProcessFile(path, true);
                    ProcessFile(path, false);
                }
                else if (Directory.Exists(path))
                {
                    ProcessDirectory(path);
                }
                else 
                {
                    Console.WriteLine(String.Format("Invalid path specified: {0}, ignoring.", path));
                }
            }
        }

        private static void ProcessFile(String filename, bool quiet)
        {
            // Open image, output compression method
            bool result = Il.ilLoadImage(filename);
            Debug.Assert(result, String.Format("Failed to load image {0}", filename));
            if (!result)
            {
                Console.WriteLine(String.Format("Failed to load image: {0}", filename));
                return;
            }

            // Construct message for each file depending on arguments,
            // dimensions first, then compression if requested.
            String sFileMessage = String.Format("{0}", filename);

            if (ms_GetOpt.Options.ContainsKey("dimensions") ||
                ms_GetOpt.Options.ContainsKey("d"))
            {
                int nWidth = Il.ilGetInteger(Il.IL_IMAGE_WIDTH);
                int nHeight = Il.ilGetInteger(Il.IL_IMAGE_HEIGHT);
                int nDepth = Il.ilGetInteger(Il.IL_IMAGE_DEPTH);

                sFileMessage += String.Format(" {0} x {1} x {2}", nWidth, nHeight, nDepth);
            }
            if (ms_GetOpt.Options.ContainsKey("compression") ||
                ms_GetOpt.Options.ContainsKey("c"))
            {
                Il.ilSetInteger(Il.IL_KEEP_DXTC_DATA, Il.IL_TRUE);
                int dxtcDataFormat = Il.ilGetInteger(Il.IL_DXTC_DATA_FORMAT);
                switch (dxtcDataFormat)
                {
                    case Il.IL_DXT1:
                        sFileMessage += " DXT1";
                        break;
                    case Il.IL_DXT2:
                        sFileMessage += " DXT2";
                        break;
                    case Il.IL_DXT3:
                        sFileMessage += " DXT3";
                        break;
                    case Il.IL_DXT4:
                        sFileMessage += " DXT4";
                        break;
                    case Il.IL_DXT5:
                        sFileMessage += " DXT5";
                        break;
                    case Il.IL_DXT_NO_COMP:
                        sFileMessage += " NONE";
                        break;
                    default:
                        sFileMessage += " UNKNOWN";
                        break;
                }
            }
            if ( !quiet )
                Console.WriteLine(sFileMessage);
        }

        private static void ProcessDirectory(String directory)
        {
            String filter = FILTER;
            // Initialise default filter argument
            if (ms_GetOpt.Options.ContainsKey("filter"))
                filter = ms_GetOpt.Options["filter"].ToString();
            else if (ms_GetOpt.Options.ContainsKey("f"))
                filter = ms_GetOpt.Options["f"].ToString();

            String[] files = Directory.GetFiles(directory, filter);
            foreach (String filename in files)
            {
                ProcessFile(filename, true);
                ProcessFile(filename, false);
            }

            // Recurse?
            if (ms_GetOpt.Options.ContainsKey("recurse") ||
                ms_GetOpt.Options.ContainsKey("r"))
            {
                String[] dirs = Directory.GetDirectories(directory);
                foreach (String dir in dirs)
                    ProcessDirectory(dir);
            }
        }
        #endregion // Private Methods
    }

} // End of ddsinfo namespace

// End of file
