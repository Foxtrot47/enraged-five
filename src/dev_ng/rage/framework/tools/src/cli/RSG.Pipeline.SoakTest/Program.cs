﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Build;
using RSG.Pipeline.Engine;
using RSG.Pipeline.Services;
using RSG.SourceControl.Perforce;

namespace RSG.Pipeline.SoakTest
{
    class Program
    {
        static int Main(String[] args)
        {
            LongOption[] opts = new LongOption[] 
            { 
                new LongOption("mode", LongOption.ArgType.Required,
                    "Soak-test mode (p4, engine, onebuild).")
            };
            CommandOptions options = new CommandOptions(args, opts);

            IUniversalLog log = LogFactory.CreateUniversalLog("SoakTest");
            LogFactory.CreateApplicationConsoleLogTarget();

            if (options.ContainsOption("mode"))
            {
                String mode = (String)options["mode"];
                if (String.Equals(mode, "p4", StringComparison.CurrentCultureIgnoreCase))
                {
                    // ~ 1GB data (512 lots of ~2 MB)
                    //SetupP4ClientSoakTest(options, 512, 1 * 1024 * 1024, 3 * 1024 * 1024);
                    P4ClientSoakTest(options, 512, 1 * 1024 * 1024, 3 * 1024 * 1024, 0.5);
                }
                else if (String.Equals(mode, "engine", StringComparison.CurrentCultureIgnoreCase))
                {
                    //String[] inputPathnames = new String[] { @"X:\gta5\assets_ng\export\levels\gta5\props\vegetation\v_rocks.zip" };
                    String[] inputPathnames = new String[] { @"X:\gta5\assets_ng\export\levels\gta5\_cityw\venice_01\vb_01.zip" };
                    EngineSoakTest(options, inputPathnames);
                }
                else if (String.Equals(mode, "onebuild", StringComparison.CurrentCultureIgnoreCase))
                {
                    //String[] inputPathnames = new String[] { @"X:\gta5\assets_ng\export\levels\gta5\props\vegetation\v_rocks.zip" };
                    String[] inputPathnames = new String[] { @"X:\gta5\assets_ng\export\levels\gta5\_cityw\venice_01\vb_01.zip" };
                    EngineRunOnce(options, inputPathnames);
                }
            }

            return (Constants.Exit_Success);
        }

        static void EngineSoakTest(CommandOptions options, IEnumerable<String> inputPathnames)
        {
            String myDocumentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            String soakTestPathname = Path.Combine(myDocumentsDirectory, "Soak Test (Engine).csv");
            using (StreamWriter writer = File.CreateText(soakTestPathname))
            {
                writer.WriteLine("Start Time,Duration");
                writer.Flush();

                while (true)
                {
                    DateTime startTime = DateTime.UtcNow;
                    
                    IEnumerable<IOutput> outputs;
                    TimeSpan buildTime;

                    Engine.Engine engine = new Engine.Engine(options, options.Config, options.Project, options.Branch, EngineFlags.Default);
                    engine.Build(inputPathnames, out outputs, out buildTime);


                    writer.WriteLine("{0},{1}", startTime.ToString(), buildTime.ToString());
                    writer.Flush();
                }
            }
        }

        static void EngineRunOnce(CommandOptions options, IEnumerable<String> inputPathnames)
        {
            IEnumerable<IOutput> outputs;
            TimeSpan buildTime;

            Engine.Engine engine = new Engine.Engine(options, options.Config, options.Project, options.Branch, EngineFlags.Default);
            engine.Build(inputPathnames, out outputs, out buildTime);
        }

        /// <summary>
        /// This simulates the typical P4 usage of the asset builder job processor
        /// </summary>
        /// <param name="options"></param>
        static void SetupP4ClientSoakTest(CommandOptions options, int numFiles, int minFileSize, int maxFileSize)
        {
            String testDataDirectory = @"x:\p4_test\P4ClientSoakTest\";
            Random random = new Random();

            for (int fileIndex = 0; fileIndex < numFiles; ++fileIndex)
            {
                String pathname = Path.Combine(testDataDirectory, String.Format("{0}.dat", fileIndex.ToString("0000")));
                int fileSize = random.Next(minFileSize, maxFileSize + 1);
                using (BinaryWriter writer = new BinaryWriter(File.Create(pathname)))
                {
                    int numDoublesToWrite = fileSize / sizeof(double);
                    for (int doubleIndex = 0; doubleIndex < numDoublesToWrite; ++doubleIndex)
                    {
                        double nextDouble = random.NextDouble();
                        writer.Write(nextDouble);
                    }
                }

                Console.WriteLine("Wrote {0} bytes to file at '{1}'", fileSize, pathname);
            }
        }

        /// <summary>
        /// This simulates the typical P4 usage of the asset builder job processor
        /// </summary>
        /// <param name="options"></param>
        static void P4ClientSoakTest(CommandOptions options, int numFiles, int minFileSize, int maxFileSize, double overwriteFraction)
        {
            String testDataDirectory = @"//testing/P4ClientSoakTest/";

            String myDocumentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            String logPathname = Path.Combine(myDocumentsDirectory, "P4 Soak Test.csv");
            using (StreamWriter logWriter = File.CreateText(logPathname))
            {
                logWriter.WriteLine("Iteraion #,sync (ms),edit (ms),data manip (ms),opened (ms),revert (ms),opened (ms),submit/delete (ms),total (ms)");

                int iterationIndex = 1;

                while (true)
                {
                    Console.WriteLine("Iteration {0}", iterationIndex);

                    Stopwatch iterationStopwatch = Stopwatch.StartNew();

                    using (P4 p4 = options.Config.Project.SCMConnect())
                    {
                        // sync files
                        Console.WriteLine("\tsync");
                        Stopwatch syncStopwatch = Stopwatch.StartNew();
                        P4API.P4RecordSet syncRecordSet = p4.Run("sync", testDataDirectory + "...");
                        syncStopwatch.Stop();

                        // edit files
                        Console.WriteLine("\tedit");
                        Stopwatch editStopwatch = Stopwatch.StartNew();
                        P4API.P4PendingChangelist pendingCL = p4.CreatePendingChangelist("p4 soak test");
                        P4API.P4RecordSet editRecordSet = p4.Run("edit", "-c", pendingCL.Number.ToString(), testDataDirectory + "...");
                        editStopwatch.Stop();

                        // alter a portion of the files
                        Console.WriteLine("\tdata manip");

                        Stopwatch dataManipStopwatch = Stopwatch.StartNew();
                        String localTestDataDirectory = @"x:\p4_test\P4ClientSoakTest\";
                        Random random = new Random();
                        for (int fileIndex = 0; fileIndex < numFiles; ++fileIndex)
                        {
                            if (random.NextDouble() <= overwriteFraction)
                            {
                                String pathname = Path.Combine(localTestDataDirectory, String.Format("{0}.dat", fileIndex.ToString("0000")));
                                int fileSize = random.Next(minFileSize, maxFileSize + 1);
                                using (BinaryWriter writer = new BinaryWriter(File.Create(pathname)))
                                {
                                    int numDoublesToWrite = fileSize / sizeof(double);
                                    for (int doubleIndex = 0; doubleIndex < numDoublesToWrite; ++doubleIndex)
                                    {
                                        double nextDouble = random.NextDouble();
                                        writer.Write(nextDouble);
                                    }
                                }

                                Console.WriteLine("\tWrote {0} bytes to file at '{1}'", fileSize, pathname);
                            }
                        }
                        dataManipStopwatch.Stop();

                        // opened
                        Console.WriteLine("\topened");
                        Stopwatch opened1Stopwatch = Stopwatch.StartNew();
                        P4API.P4RecordSet opened1RecordSet = p4.Run("opened", "-c", pendingCL.Number.ToString());
                        opened1Stopwatch.Stop();

                        // revert unchanged
                        Console.WriteLine("\trevert");
                        Stopwatch revertStopwatch = Stopwatch.StartNew();
                        P4API.P4RecordSet revertRecordSet = p4.Run("revert", "-a", "-c", pendingCL.Number.ToString());
                        revertStopwatch.Stop();

                        // opened (check if there's anything to submit)
                        Console.WriteLine("\topened");
                        Stopwatch opened2Stopwatch = Stopwatch.StartNew();
                        P4API.P4RecordSet opened2RecordSet = p4.Run("opened", "-c", pendingCL.Number.ToString());
                        opened2Stopwatch.Stop();

                        Stopwatch deleteOrSubmitStopwatch = Stopwatch.StartNew();
                        if (opened2RecordSet.Records.Any())
                        {
                            // submit
                            Console.WriteLine("\tsubmit");
                            P4API.P4UnParsedRecordSet submitRecordSet = pendingCL.Submit();
                        }
                        else
                        {
                            // delete
                            Console.WriteLine("\tdelete");
                            P4API.P4UnParsedRecordSet submitRecordSet = pendingCL.Delete();
                        }
                        deleteOrSubmitStopwatch.Stop();

                        iterationStopwatch.Stop();

                        logWriter.WriteLine(
                            "{0},{1},{2},{3},{4},{5},{6},{7},{8}",
                            iterationIndex,
                            syncStopwatch.ElapsedMilliseconds,
                            editStopwatch.ElapsedMilliseconds,
                            dataManipStopwatch.ElapsedMilliseconds,
                            opened1Stopwatch.ElapsedMilliseconds,
                            revertStopwatch.ElapsedMilliseconds,
                            opened2Stopwatch.ElapsedMilliseconds,
                            deleteOrSubmitStopwatch.ElapsedMilliseconds,
                            iterationStopwatch.ElapsedMilliseconds);
                    }

                    logWriter.Flush();

                    Console.WriteLine("End of Iteration {0}", iterationIndex++);
                    System.Threading.Thread.Sleep(2000);
                }
            }
        }
    }
}
