#include "common.h"
#include "config.h"
#include "NavGen_Octree.h"
#include "NavGen.h"
#include "NavGen_Utils.h"
#include "NavGen_Store.h"
#include "ai/navmesh/datatypes.h"
#include "ai/navmesh/navmeshextents.h"
#include "spatialdata\sphere.h"

namespace rage
{

//*********************************************************************************
//
//	Title	: NavGen_Analyse.cpp
//	Author	: James Broad
//	Date	: Jan 2005
//
//	This file contains classes and functions for the analysis of navmeshes, so we
//	can extract and store extra information about the environment.
//
//	This is useful for a number of reasons :
//
//	We can find out where there are non-standard adjacencies between navmesh polys,
//	which may require climbing over walls, dropping from heights, etc.  These
//	connections could no be found without this extra step.
//
//	We can find out where the edge of a navmesh poly might provide cover, and then
//	incorporate this into the pathfinding logic.  We can find corners and low walls
//	which also provide cover, and can then supply this information upon demand to
//	the game.
//
//	We can analyse the acoustic properties of the area around each polygon, and
//	can store this to help the game audio systems apply the correct attenuation
//	and reverberation to sound emissions.
//
//	There is undoubtably a lot more which we can do at this stage as well; one of
//	the benefits of having a secondary data-structure which decribes the walkable
//	areas of the world is that we can use it to encode and retrieve all sorts of
//	other useful information other than just navigation data.
//
//*********************************************************************************


int g_iNumNavMeshPolysGathered;
int g_iMaxNumNavMeshPolysToGather;
Vector3 g_vGatherNavMeshPolysOrigin;

int g_iNumLowClimbOversFound = 0;
int g_iNumHighClimbOversFound = 0;
int g_iNumDropDownsFound = 0;

//#define DEBUG_MODE
bool g_bDebugMode = false;
int g_iTotalNumCoverPointsCount = 0;
u32 CNavGen::ms_iIndexOfCoverPointWithinNavMesh = 0;

u16 m_iNavMeshPolyTimestamp = 0;


#define __DO_NAVMESH_ADJACENCIES					1
#define __DO_DIRECTIONAL_COVER_TESTS				1

bool
CNavGen::AnalyseNavMesh(
	CNavMesh * pNavMesh, CNavGen * pNavGen,
	CNavMesh * pNavMeshMinusX, CNavGen * pNavGenMinusX,
	CNavMesh * pNavMeshPlusX, CNavGen * pNavGenPlusX,
	CNavMesh * pNavMeshMinusY, CNavGen * pNavGenMinusY,
	CNavMesh * pNavMeshPlusY, CNavGen * pNavGenPlusY,
	char * pLoadThisCvrFileInsteadOfAnalysingForCoverPoints,		// If this member is not NULL, then attempt to load file instead of creating coverpoints
	const atArray<CNavGen::TAuthoredAdjacencyHintPos> & authoredAdjacencies,
	bool bNonMapNavMesh)
{
	char tmp[256];


	//************************************************************************
	// Classify all CNavResolutionArea's wrt the current navmesh extents
	// We don't clear the array first, since it may already contain areas
	// placed there by the calling code.
	// Only add those which are relevant to the cover/analysis code..
	//************************************************************************

	u32 r;
	for(r=0; r<m_ResolutionAreasWholeMap.size(); r++)
	{
		CNavResolutionArea * pArea = m_ResolutionAreasWholeMap[r];
		if( ((pArea->m_iFlags & CNavResolutionArea::FLAG_NO_CLIMBS)!=0) ||
			((pArea->m_iFlags & CNavResolutionArea::FLAG_NO_DROPS)!=0) ||
			((pArea->m_iFlags & CNavResolutionArea::FLAG_NO_COVER)!=0) )
		{
			if(BBoxIntersect2d(pNavGen->m_vSectorMins, pNavGen->m_vSectorMaxs, pArea->m_vMin, pArea->m_vMax))
				pNavGen->m_ResolutionAreasCurrentNavMesh.push_back(pArea);
		}
	}

	//*****************************************************************************
	// Add the custom coverpoints relevant to the current navmesh extents, if any
	//*****************************************************************************
	for(u32 iCoverPoint=0; iCoverPoint<m_CustomCoverpointsWholeMap.size(); iCoverPoint++)
	{
		CNavCustomCoverpoint* pCustomCoverPoint = m_CustomCoverpointsWholeMap[iCoverPoint];
		Vector3 vCustomCoverpointPos(pCustomCoverPoint->m_CoordsX, pCustomCoverPoint->m_CoordsY, pCustomCoverPoint->m_CoordsZ);
		if(BBoxIntersect2d(pNavGen->m_vSectorMins, pNavGen->m_vSectorMaxs, vCustomCoverpointPos, vCustomCoverpointPos))
		{
			pNavGen->m_CustomCoverpointsCurrentNavMesh.push_back(pCustomCoverPoint);
		}
	}


	u32 p,v,lastv;
	Vector3 vLastVec, vVec;
	Vector3 vPointOnEdge, vPolyCentroid;
	Vector3 vDirectionAwayFromPoly;

	g_iNumLowClimbOversFound = 0;
	g_iNumHighClimbOversFound = 0;
	g_iNumDropDownsFound = 0;
	g_iNumCoverPointsFound = 0;

	CNavMesh * pNavMeshes[ANALYSE_NUM_NAVMESHES] =
	{
		pNavMesh, pNavMeshMinusX, pNavMeshPlusX, pNavMeshMinusY, pNavMeshPlusY
	};
	pNavMeshes;

	CNavGen * pNavGens[ANALYSE_NUM_NAVMESHES] =
	{
		pNavGen, pNavGenMinusX, pNavGenPlusX, pNavGenMinusY, pNavGenPlusY
	};

	

	// Clear out all adjacency types to normal
	for(p=0; p<pNavMesh->m_iNumPolys; p++)
	{
		TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);
		for(v=0; v<pPoly->GetNumVertices(); v++)
		{
			TAdjPoly & adjPoly = *pNavMesh->GetAdjacentPolysArray().Get( pPoly->GetFirstVertexIndex() + v );

			if(adjPoly.GetAdjacencyType() != ADJACENCY_TYPE_NORMAL)
			{
				adjPoly.m_iAdjacentNavMeshIndex = NAVMESH_NAVMESH_INDEX_NONE;
				adjPoly.SetOriginalNavMeshIndex(NAVMESH_NAVMESH_INDEX_NONE, pNavMesh->GetAdjacentMeshes());
				adjPoly.SetOriginalPolyIndex(NAVMESH_POLY_INDEX_NONE);
				adjPoly.SetNavMeshIndex(NAVMESH_NAVMESH_INDEX_NONE, pNavMesh->GetAdjacentMeshes());
				adjPoly.SetPolyIndex(NAVMESH_POLY_INDEX_NONE);
				
				adjPoly.SetAdjacencyType(ADJACENCY_TYPE_NORMAL);
			}

			// Ensure we set the 'm_iAdjacentNavMeshIndex' member used during navmesh construction
			adjPoly.m_iAdjacentNavMeshIndex = adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes());

			adjPoly.SetAdjacencyType(ADJACENCY_TYPE_NORMAL);
			adjPoly.SetEdgeProvidesCover(false);
			adjPoly.m_bEdgeMightProvideLowCover = false;
			adjPoly.m_fEdgeNormalX = 0.0f;
			adjPoly.m_fEdgeNormalY = 0.0f;
			adjPoly.SetHighDropOverEdge(false);
		}
	}


	//*********************************************************************************
	// During navmesh creation some attributes whose boundary-edges are not preserved,
	// can be dilated/eroded by the mesh optimisation & merging process.
	// For certain of these attributes we need to make sure that we don't underestimate
	// or overestimate their boundaries - so we perform a series of correction steps.


	CorrectPedDensity(pNavMesh, pNavGen);

	pNavGen->SetRoadFlags(pNavMesh);
	pNavGen->SetTrainTrackFlags(pNavMesh);

	pNavGen->CalculateWaterDepths(pNavMesh, pNavGen);


	//************************************************************************
	//	Analyse the audio properties for all polys in the central navmesh

	Vector3 vPolyPts[NAVMESHPOLY_MAX_NUM_VERTICES];
	static const float fRaiseUpAmtForNavMeshAudio = 0.25f;

	for(p=0; p<pNavMesh->m_iNumPolys; p++)
	{
		TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);
		
		vPolyCentroid = Vector3(0.0f,0.0f,0.0f);

		for(v=0; v<pPoly->GetNumVertices(); v++)
		{
			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, v), vPolyPts[v] );
			vPolyPts[v].z += fRaiseUpAmtForNavMeshAudio;

			vPolyCentroid += vPolyPts[v];
		}

		vPolyCentroid /= (float)pPoly->GetNumVertices();

		u32 iAudioVal = CalculateAudioPropertiesForPoly(pPoly->GetNumVertices(), vPolyPts, vPolyCentroid, pNavGens, ANALYSE_NUM_NAVMESHES);

		pPoly->SetAudioProperties(iAudioVal);
	}



#if __DO_NAVMESH_ADJACENCIES

	u32 iAdjacencyStartTime = timeGetTime();

	Vector3 vEdgeVec, vEdgeVec2D, vMidPoint, vEdgeNormal, vEdgeNormal2D;
	float fEdgeLen;

	//-------------------------------------------------------------------------
	// If we have an input list of authored adjacencies, use these as hints
	// for performing edge adjacency searches

	for(s32 a=0; a<authoredAdjacencies.GetCount(); a++)
	{
		const CNavGen::TAuthoredAdjacencyHintPos & hintPos = authoredAdjacencies[a];

		// Locate navmesh polygon directly underneath
		Vector3 vIsectPos;
		u32 iPolyIndex = pNavMesh->GetPolyBelowPoint(hintPos.m_vStartPosition, vIsectPos, 4.0f);
		if(iPolyIndex == NAVMESH_POLY_INDEX_NONE)
			continue;
		TNavMeshPoly * pPoly = pNavMesh->GetPoly(iPolyIndex);

		Vector3 vAdjTarget = vIsectPos + (hintPos.m_vHeading * 50.0f);

		// Determine the poly edge which is intersected by the direction vector
		Vector3 vLast, vCurr;
		u32 v, lastv;
		lastv = pPoly->GetNumVertices()-1;
		
		for(v=0; v<pPoly->GetNumVertices(); v++)
		{
			// We cannot create a climb/drop along an edge which is connected already in any way
			// (i.e. a regular poly/poly connection, or an edge which we've already created a climb/drop upon)
			const TAdjPoly & adj = pNavMesh->GetAdjacentPoly( pPoly->GetFirstVertexIndex()+lastv );
			if(adj.GetOriginalPolyIndex() != NAVMESH_POLY_INDEX_NONE)
			{
				lastv = v;
				continue;
			}
			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, lastv), vLast);
			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, v), vCurr);

			if( CNavMesh::LineSegsIntersect2D( vIsectPos, vAdjTarget, vLast, vCurr ) )
				break;

			lastv = v;
		}
		if(v == pPoly->GetNumVertices())
			continue;

		// We have determined that the adjacency (if any is found), should be along poly edge 'lastv'
		TAdjPoly * adj = pNavMesh->GetAdjacentPolysArray().Get( pPoly->GetFirstVertexIndex()+lastv );

		vMidPoint = (vLast + vCurr) / 2.0f;

		vEdgeVec = vCurr - vLast;
		fEdgeLen = vEdgeVec.Mag();
		vEdgeVec.Normalize();

		vEdgeVec2D = vEdgeVec;
		vEdgeVec2D.z = 0.0f;
		vEdgeVec2D.Normalize();

		vEdgeNormal = CrossProduct(vEdgeVec, Vector3(0,0,1.0f));
		vEdgeNormal.Normalize();

		vEdgeNormal2D = CrossProduct(vEdgeVec2D, Vector3(0,0,1.0f));
		vEdgeNormal2D.Normalize();

		Vector3 vToPos;
		bool bFound = ScanForAdjacentNavMeshPoly(pNavMesh, pPoly, *adj, vMidPoint, vEdgeNormal, vEdgeVec, pNavGens, pNavMeshes, vToPos, false, true);
		if(bFound)
		{
			if(hintPos.m_iType == CNavGen::TAuthoredAdjacencyHintPos::TYPE_CLIMB)
			{
				adj->SetAdjacencyType(ADJACENCY_TYPE_CLIMB_LOW);
			}
			else if(hintPos.m_iType == CNavGen::TAuthoredAdjacencyHintPos::TYPE_DROP)
			{
				adj->SetAdjacencyType(ADJACENCY_TYPE_DROPDOWN);
			}
			else
			{
				adj->SetAdjacencyType(ADJACENCY_TYPE_NORMAL);
				adj->SetNavMeshIndex(NAVMESH_NAVMESH_INDEX_NONE, pNavMesh->GetAdjacentMeshes());
				adj->SetPolyIndex(NAVMESH_POLY_INDEX_NONE);
				adj->SetOriginalNavMeshIndex(NAVMESH_NAVMESH_INDEX_NONE, pNavMesh->GetAdjacentMeshes());
				adj->SetOriginalPolyIndex(NAVMESH_POLY_INDEX_NONE);
			}
		}
	}

	//---------------------------------------------------------------------------
	// Perform detection of edge adjacencies
	// These are searched for at the midpoint of every unconnected navmesh edge

	if(!bNonMapNavMesh)
	{
		for(p=0; p<pNavMesh->m_iNumPolys; p++)
		{
			TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);

			if(pNavMesh->GetPolyArea(p) == 0.0f)
				continue;

			lastv = pPoly->GetNumVertices()-1;
			for(v=0; v<pPoly->GetNumVertices(); v++)
			{
				pNavMesh->GetVertex(pNavMesh->GetPolyVertexIndex(pPoly, lastv), vLastVec);
				pNavMesh->GetVertex(pNavMesh->GetPolyVertexIndex(pPoly, v), vVec);

				TAdjPoly adjPoly = pNavMesh->GetAdjacentPoly(pPoly->GetFirstVertexIndex() + lastv);

				if(adjPoly.m_iAdjacentNavMeshIndex==NAVMESH_NAVMESH_INDEX_NONE && adjPoly.GetPolyIndex()==NAVMESH_POLY_INDEX_NONE)
				{
					vMidPoint = (vLastVec + vVec) / 2.0f;

					// If this polygon is water, then check the depth below the edge midpoint
					// If there is above a certain depth of water here, then move the edgepoint downwards
					// so that climbpoints may be found to get out of pools/rivers/etc
					if(pPoly->GetIsWater())
					{
						Vector3 vHitPoint;
						float fHitDist = 0.0f;
						CNavGenTri * pHitTri = NULL;
						u32 iRetFlags = 0;

						bool bHitWaterBottom = pNavGen->m_Octree.ProcessLineOfSight(vMidPoint, vMidPoint - ZAXIS, iRetFlags, NAVGEN_COLLISION_TYPES_EXCEPT_RIVER, vHitPoint, fHitDist, pHitTri, true);

						if( !bHitWaterBottom || fHitDist > fwNavGenConfig::Get().m_MaxHeightChangeUnderTriangleEdge )
						{
							vMidPoint -= ZAXIS * fwNavGenConfig::Get().m_MaxHeightChangeUnderTriangleEdge;
						}
					}

					vEdgeVec = vVec - vLastVec;
					fEdgeLen = vEdgeVec.Mag();
					vEdgeVec.Normalize();

					vEdgeVec2D = vEdgeVec;
					vEdgeVec2D.z = 0.0f;
					vEdgeVec2D.Normalize();

					vEdgeNormal = CrossProduct(vEdgeVec, Vector3(0,0,1.0f));
					vEdgeNormal.Normalize();

					vEdgeNormal2D = CrossProduct(vEdgeVec2D, Vector3(0,0,1.0f));
					vEdgeNormal2D.Normalize();



					Vector3 vToPos;
					bool bFoundAdjacency = ScanForAdjacentNavMeshPoly(pNavMesh, pPoly, adjPoly, vMidPoint, vEdgeNormal, vEdgeVec, pNavGens, pNavMeshes, vToPos, false, false);

					// Ensure that we don't create climbs within areas tagged with CNavResolutionArea::FLAG_NO_CLIMBS
					if(adjPoly.GetAdjacencyType()==ADJACENCY_TYPE_CLIMB_LOW || adjPoly.GetAdjacencyType()==ADJACENCY_TYPE_CLIMB_HIGH)
					{
						if(pNavGen->IntersectsResolutionArea( CNavResolutionArea::FLAG_NO_CLIMBS, vMidPoint, vMidPoint))
						{
							bFoundAdjacency = false;
						}
					}
					// Ensure that we don't create climbs within areas tagged with CNavResolutionArea::FLAG_NO_DROPS
					else if(adjPoly.GetAdjacencyType()==ADJACENCY_TYPE_DROPDOWN)
					{
						if(pNavGen->IntersectsResolutionArea( CNavResolutionArea::FLAG_NO_DROPS, vMidPoint, vMidPoint))
						{
							bFoundAdjacency = false;
						}
					}

					if(!bFoundAdjacency)
					{
						adjPoly.SetAdjacencyType(ADJACENCY_TYPE_NORMAL);
						adjPoly.SetOriginalNavMeshIndex(NAVMESH_NAVMESH_INDEX_NONE, pNavMesh->GetAdjacentMeshes());
						adjPoly.SetNavMeshIndex(NAVMESH_NAVMESH_INDEX_NONE, pNavMesh->GetAdjacentMeshes());
						adjPoly.m_iAdjacentNavMeshIndex = NAVMESH_NAVMESH_INDEX_NONE;
						adjPoly.SetOriginalPolyIndex(NAVMESH_POLY_INDEX_NONE);
						adjPoly.SetPolyIndex(NAVMESH_POLY_INDEX_NONE);
						adjPoly.SetHighDropOverEdge(false);
					}
					else
					{
						switch(adjPoly.GetAdjacencyType())
						{
						case ADJACENCY_TYPE_DROPDOWN:
							g_iNumDropDownsFound++;
							break;
						case ADJACENCY_TYPE_CLIMB_LOW:
							g_iNumLowClimbOversFound++;
							break;
						case ADJACENCY_TYPE_CLIMB_HIGH:
							g_iNumHighClimbOversFound++;
							break;
						default:
							break;
						}
					}

					adjPoly.SetEdgeProvidesCover(false);
					adjPoly.m_bEdgeMightProvideLowCover = false;

					// Save any changes
					TAdjPoly * pDest = pNavMesh->GetAdjacentPolysArray().Get( pPoly->GetFirstVertexIndex() + lastv );
					pDest->SetAdjacencyType(adjPoly.GetAdjacencyType());
					pDest->SetPolyIndex(adjPoly.GetPolyIndex());
					pDest->SetOriginalPolyIndex(adjPoly.GetOriginalPolyIndex());
					pDest->m_Struct1.m_NavMeshIndex = adjPoly.m_Struct1.m_NavMeshIndex;
					pDest->m_iAdjacentNavMeshIndex = adjPoly.m_iAdjacentNavMeshIndex;
					pDest->m_Struct2.m_OriginalNavMeshIndex = adjPoly.m_Struct2.m_OriginalNavMeshIndex;
					pDest->SetHighDropOverEdge(adjPoly.GetHighDropOverEdge());
				}

				vLastVec = vVec;
				lastv = v;
			}
		}
	}


	//***************************************************************************************
	//	Check for whether each navmesh edge potentially provides cover.
	//	This is used later by the cover-finder code to trivially skip expensive checks for
	//	low-wall cover along edges which have been marked as probably not providing cover.

	for(p=0; p<pNavMesh->m_iNumPolys; p++)
	{
		TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);

		if(pPoly->GetIsWater())
			continue;

		lastv = pPoly->GetNumVertices()-1;
		for(v=0; v<pPoly->GetNumVertices(); v++)
		{
			const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly(pPoly->GetFirstVertexIndex() + lastv);

			if(adjPoly.GetAdjacencyType()==ADJACENCY_TYPE_NORMAL &&
				(adjPoly.m_iAdjacentNavMeshIndex != NAVMESH_NAVMESH_INDEX_NONE || adjPoly.GetPolyIndex()!=NAVMESH_POLY_INDEX_NONE))
			{
				lastv = v;
				continue;
			}

			// Get the point halfway along this poly edge
			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, lastv), vLastVec);
			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, v), vVec);
			vPointOnEdge = (vLastVec + vVec) * 0.5f;

			Vector3 vEdgeVec3d = vVec - vLastVec;
			vEdgeVec3d.Normalize();

			Vector3 vTmp = CrossProduct(vEdgeVec3d, Vector3(0,0,1.0f));
			vTmp.Normalize();

			Vector3 vEdgeVec2d = vVec - vLastVec;
			vEdgeVec2d.z = 0.0f;
			float fEdgeLength = vEdgeVec2d.Mag();
			vEdgeVec2d.Normalize();

			Vector3 vUp3d = CrossProduct(vEdgeVec3d, vTmp);
			vUp3d.Normalize();

			Vector3 vEdgeNormal3d = CrossProduct(vUp3d, vEdgeVec3d);
			vEdgeNormal3d.Normalize();

			//float fEdgeLength = vEdgeNormal3d.Mag();

			Vector3 vAverageNormalOfSurfacesHit(0.0f, 0.0f, 0.0f);
			bool bEdgeMightProvideLowCoverPoints = false;

			bool bEdgeProvidesCover = DoesEdgeProvideCover(
				ANALYSE_NUM_NAVMESHES, pNavGens,
				vLastVec,
				vEdgeVec3d, fEdgeLength, vEdgeNormal3d,
				vAverageNormalOfSurfacesHit,
				bEdgeMightProvideLowCoverPoints
			);

			TAdjPoly & destAdjPoly = *pNavMesh->GetAdjacentPolysArray().Get( pPoly->GetFirstVertexIndex() + lastv );

			destAdjPoly.SetEdgeProvidesCover(bEdgeProvidesCover);
			destAdjPoly.m_bEdgeMightProvideLowCover = bEdgeMightProvideLowCoverPoints;

	#ifdef __USE_CALCULATED_ADJPOLY_NORMALS
			destAdjPoly.m_fEdgeNormalX = vAverageNormalOfSurfacesHit.x;
			destAdjPoly.m_fEdgeNormalY = vAverageNormalOfSurfacesHit.y;
	#endif

			lastv = v;
		}
	}

	u32 iAdjacencyEndTime = timeGetTime();
	float fAdjacencySecs = ((float)(iAdjacencyEndTime - iAdjacencyStartTime) / 1000.0f);
	sprintf(tmp, "Time taken to analyse adjacencies : %.3f secs\n", fAdjacencySecs);
	OutputDebugString(tmp);
	printf(tmp);

#endif	// __DO_NAVMESH_ADJACENCIES


	//*************************************************************************
	// Check for cover in navmesh generally (corner cover, low wall cover, etc)
	//*************************************************************************

	bool bAnalyseForCoverPoints = true;
	if(pLoadThisCvrFileInsteadOfAnalysingForCoverPoints)
	{
		bool bLoadedOk = pNavGen->LoadCvrFileAndAddCoverPoints(pLoadThisCvrFileInsteadOfAnalysingForCoverPoints, pNavMesh);
		if(bLoadedOk)
		{
			printf("Loaded cached coverpoints file ok.\n");
			bAnalyseForCoverPoints = false;
		}
	}

	if(bAnalyseForCoverPoints)
	{
		u32 iCoverPointsStartTime = timeGetTime();

		FindCoverPoints(pNavGen, pNavMesh, ANALYSE_NUM_NAVMESHES, pNavGens);

		u32 iCoverPointsEndTime = timeGetTime();
		float fCoverPointsSecs = ((float)(iCoverPointsEndTime - iCoverPointsStartTime) / 1000.0f);
		sprintf(tmp, "Time taken to analyse cover : %.3f secs\n", fCoverPointsSecs);
		OutputDebugString(tmp);
		printf(tmp);

		sprintf(tmp, "Low Climb-Overs : %i High Climb-Overs : %i Drop-Downs : %i Cover-Points : %i\n",
			g_iNumLowClimbOversFound,
			g_iNumHighClimbOversFound,
			g_iNumDropDownsFound,
			g_iNumCoverPointsFound
		);

		OutputDebugString(tmp);
		printf(tmp);
	}

	//****************************************************************************
	// Check each poly in central navmesh for surrounding cover in 8 directions.
	// This value gets stored into a bitfield in each navmehs poly.

	CCulledGeom geometry;
	u32 iDirectionalCoverStartTime = timeGetTime();

	for(p=0; p<pNavMesh->m_iNumPolys; p++)
	{
		TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);

#if __DO_DIRECTIONAL_COVER_TESTS

#if __USE_GEOMCULLER_FOR_DIRECTIONAL_COVER
		geometry.Reset();
		Vector3 vGeomMin, vGeomMax;
		pPoly->m_MinMax.GetAsFloats(vGeomMin, vGeomMax);
		const float fXYCoverTestDist = DIRECTIONAL_COVER_TEST_DISTANCE + 1.0f;
		const float fHeightExtend = CDirectionalCoverInfo::ms_fTestHeights[DIRECTIONAL_COVER_NUM_HEIGHTS-1] + 1.0f;
		vGeomMin -= Vector3(fXYCoverTestDist, fXYCoverTestDist, fHeightExtend);
		vGeomMax += Vector3(fXYCoverTestDist, fXYCoverTestDist, fHeightExtend);
		InitGeomCuller(geometry, vGeomMin, vGeomMax, ANALYSE_NUM_NAVMESHES, pNavGens);
#endif
		pPoly->m_iCoverDirectionsBitfield = TestDirectionalCoverNavMeshForPoly(geometry, pNavMesh, pPoly, pNavGens, ANALYSE_NUM_NAVMESHES);
#else
		pPoly->m_iCoverDirectionsBitfield = 0;
#endif
	}

	u32 iDirectionalCoverEndTime = timeGetTime();
	float fDirectionalCoverSecs = ((float)(iDirectionalCoverEndTime - iDirectionalCoverStartTime) / 1000.0f);
	sprintf(tmp, "Time taken to analyse direction cover for polys : %.3f secs\n", fDirectionalCoverSecs);
	OutputDebugString(tmp);
	printf(tmp);


	// Clear up remaining temporary data
	pNavGen->m_ResolutionAreasCurrentNavMesh.clear();
	pNavGen->m_CustomCoverpointsCurrentNavMesh.clear();

	return true;
}

CNavResolutionArea * CNavGen::IntersectsResolutionArea(const u32 iResAreaFlags, const Vector3 & vMin, const Vector3 & vMax)
{
	for(u32 i=0; i<m_ResolutionAreasCurrentNavMesh.size(); i++)
	{
		CNavResolutionArea * pArea = m_ResolutionAreasCurrentNavMesh[i];
		if((pArea->m_iFlags & iResAreaFlags)!=0)
		{
			if(BBoxIntersect(vMin, vMax, pArea->m_vMin, pArea->m_vMax))
			{
				return pArea;
			}
		}
	}
	return NULL;
}

bool CNavGen::FindNonStandardAdjacency(CNavMesh * UNUSED_PARAM(pNavMesh), TNavMeshPoly * UNUSED_PARAM(pPoly), const Vector3 & vPointOnEdge, const Vector3 & vEdgeNormal, const Vector3 & vEdgeVec, CNavGen ** ppNavGens)
{
	// These are the heights & seperation of horizontal linetests along the edge normal [x][y] or [column][row]
	#define NUM_ROWS 6
	#define NUM_COLS 3
	static const float fPedRadius = fwNavGenConfig::Get().m_PedRadius;
	static const Vector3 vHorizontalOffsets[NUM_COLS][NUM_ROWS] =
	{
		Vector3(-fPedRadius, 0.0f, 0.0f),
		Vector3(0.0f, 0.0f, 0.0f),
		Vector3(fPedRadius, 0.0f, 0.0f),

		Vector3(-fPedRadius, 0.0f, 0.2f),
		Vector3(0.0f, 0.0f, 0.2f),
		Vector3(fPedRadius, 0.0f, 0.2f),

		Vector3(-fPedRadius, 0.0f, 0.4f),
		Vector3(0.0f, 0.0f, 0.4f),
		Vector3(fPedRadius, 0.0f, 0.4f),

		Vector3(-fPedRadius, 0.0f, 0.6f),
		Vector3(0.0f, 0.0f, 0.6f),
		Vector3(fPedRadius, 0.0f, 0.6f),

		Vector3(-fPedRadius, 0.0f, 0.8f),
		Vector3(0.0f, 0.0f, 0.8f),
		Vector3(fPedRadius, 0.0f, 0.8f),

		Vector3(-fPedRadius, 0.0f, 1.0f),
		Vector3(0.0f, 0.0f, 1.0f),
		Vector3(fPedRadius, 0.0f, 1.0f)
	};

	Vector3 vHorizontalHitVecs[NUM_COLS][NUM_ROWS];
	float fHorizontalHitDists[NUM_COLS][NUM_ROWS];
	bool bHorizontalHits[NUM_COLS][NUM_ROWS];

	static const float fHorizTestDist = 5.0f;
	
	// First perform tests along the edge-vec to see what type of obstruction (if any) exists here

	float fLowestObstruction = vPointOnEdge.z;
	float fClosestHitDist = FLT_MAX;
	float fFurthestHitDist = -FLT_MAX;

	for(int r=0; r<NUM_ROWS; r++)
	{
		for(int c=0; c<NUM_COLS; c++)
		{
			const Vector3 & vOffset = vHorizontalOffsets[c][r];

			Vector3 vRayStart = vPointOnEdge + (vEdgeVec * vOffset.x);
			vRayStart.z += vOffset.z;
			Vector3 vRayEnd = vPointOnEdge + (vEdgeNormal * fHorizTestDist) + (vEdgeVec * vOffset.x);
			vRayEnd.z += vOffset.z;

			bool bLos = TestMultipleNavGenLos(
				vRayStart,
				vRayEnd,
				ANALYSE_NUM_NAVMESHES,
				ppNavGens,
				NAVGEN_COLLISION_TYPES,
				&vHorizontalHitVecs[c][r],
				false,
				false,
				false
			);

			bHorizontalHits[c][r] = !bLos;
			if(!bLos)
			{
				const Vector3 vDiff = vHorizontalHitVecs[c][r] - vRayStart;
				if(vDiff.Mag2() > 0.001f)
					fHorizontalHitDists[c][r] = vDiff.Mag();
				else
					fHorizontalHitDists[c][r] = 0.0f;

				if(vHorizontalHitVecs[c][r].z > fLowestObstruction)
					fLowestObstruction = vHorizontalHitVecs[c][r].z;

				if(fHorizontalHitDists[c][r] < fClosestHitDist)
					fClosestHitDist = fHorizontalHitDists[c][r];

				if(fHorizontalHitDists[c][r] > fFurthestHitDist)
					fFurthestHitDist = fHorizontalHitDists[c][r];
			}
		}
	}

	return false;
}


//***********************************************************************************************
//	ScanForAdjacentNavMeshPoly
//	This function scans outwards from a navmesh poly, and tries to find non-standard adjacencies
//	from this poly to another poly in the nearby navmeshes.
//
//	pStartPoly		-	the poly we are trying to find an adjacency from
//	vStartPos		-	expected to be a position on the navmesh (ie. at foot height)
//	vScanDir		-	the direction in which to look

bool
CNavGen::ScanForAdjacentNavMeshPoly(
	CNavMesh * pNavMesh,
	TNavMeshPoly * pStartPoly,
	TAdjPoly & adjPoly,
	const Vector3 & vStartPos,
	const Vector3 & vScanDir,
	const Vector3 & UNUSED_PARAM(vRightVector),
	CNavGen ** ppNavGens,
	CNavMesh ** ppNavMeshes,
	Vector3 & vOut_ToPos,
	const bool bConsiderClimbableObjects,
	const bool bQuitWithoutPerformingTests,
	const float fScanStartDistAhead)
{
#if __DEV
	Vector3 vTestPos(-1290.99f, 2.98f, 21.59f);
	if(vTestPos.IsClose(vStartPos, 0.5f))
	{
		bool bPause=true; bPause;
	}
#endif
	//***************************************************************************
	//	These probes are used when testing top-down, and are used to determine
	//	whether there is room for a ped to stand.  If we do these 5 probes
	//	downwards and all hit at a similar height, then we can assume that there
	//	is even ground there.

	static const int iNumVerticalProbes = 5;
	static const float fPedRadius = fwNavGenConfig::Get().m_PedRadius;
	static const Vector3 vProbeVerticalOffsets[iNumVerticalProbes] =
	{
		Vector3(0.0f, 0.0f, 0.0f),
		Vector3(-fPedRadius, 0.0f, 0.0f),
		Vector3(fPedRadius, 0.0f, 0.0f),
		Vector3(0.0f, -fPedRadius, 0.0f),
		Vector3(0.0f, fPedRadius, 0.0f)
	};

	//************************************************************************
	//	These probes are aligned with the vScanDir, and are used to determine
	//	whether there is room for a ped to pass.
	//	We do 3 probes at three different height intervals.  If all succeed,
	//	then we can assume that a ped would be able to walk through.

	static const float fHeightSteps = 0.5f;

	// The height above vStartPos.z for line probes to start
	static const float fHighZOffset = 3.0f;	//5.0f;
	// The height below vStartPos.z for line probes to end
	static const float fLowZOffset = 10.0f;
	// The size of the increments between scans
	static const float fScanIncrements = 0.25f;		//1.0f;
	// The maximum distance from vStartPos to scan
	static const float fScanMaxDistAhead = 4.0f;	// 6.0f
	// The starting distance from vStartPos
//	static const float fScanStartDistAhead = 0.5f;	//1.0f;
	// If there is no obstructions & the height diff is under this amt, then we can just walk to the adjacent poly
	static const float fMaxHeightDiffForSimpleAdjacency = 0.3f;
	// If the dist between startpos & found poly intersection is under this amount, then maybe we can just walk to adjacent poly
	static const float fMaxDistBetweenForSimpleAdjacency = fScanStartDistAhead * 2.0f;
	// This is the maximum height above the start pos which we will look for a clear passage (ie. the max we will climb)
	static const float fMaxHeightAboveStartToTestForObstructions = 4.0f;
	// When testing for even ground to stand on (using vProbeVerticalOffsets) all hit-positions must be within this distance vertically
	static const float fMaxHeightDiffForEvenGround = 0.5f;
	// The maximum height which can be scaled by a quick vault
	static const float fMaxHeightForVaultOver = 1.0f;
	// The maximum height for a low climb.. This is an adjacency which is a little greater that a simple "walk-across", but probably doesn't require a climb as such
	static const float fMaxHeightForLowClimbUp = fMaxHeightDiffForSimpleAdjacency * 2.0f;
	// The maximum height which can be grabbed & climbed up
	static const float fMaxHeightForClimbUp = 3.0f;
	// Any drops over this height will be marked as 'high' drops
	static const float fMaxHeightForSafeDrop = 3.5f;


	//**********************************************************************************
	// Scan ahead from the input position, testing probes downwards into the navmeshes.
	// If we hit a navmesh poly it indicates that there is a walkable surface below,
	// do a number of further tests to ascertain whether what kind of adjacency (if any)
	// leads from the start navmesh poly to the destination navmesh poly.

	Vector3 vNavMeshHitPos;
	CNavMesh * pHitNavMesh;
	u16 iHitNavMeshPoly;

	Vector3 vCurrentPos;
	float fDist;

	for(fDist=fScanStartDistAhead; fDist<fScanMaxDistAhead; fDist+=fScanIncrements)
	{
		vCurrentPos = vStartPos + (vScanDir * fDist);

		Vector3 vProbeStart = vCurrentPos;
		Vector3 vProbeEnd = vCurrentPos;

		vProbeStart.z += fHighZOffset;
		vProbeEnd.z -= fLowZOffset;

		bool bHit = ! TestMultipleNavMeshLos(vProbeStart, vProbeEnd, ANALYSE_NUM_NAVMESHES, ppNavMeshes, pHitNavMesh, iHitNavMeshPoly, vNavMeshHitPos);

		if(!bHit)
			continue;

		// Check we are clear of collision directly below
		Vector3 vGroundProbeStart = vProbeStart;
		Vector3 vGroundProbeEnd = vNavMeshHitPos + Vector3(0.0f,0.0f,0.5f);
		Vector3 vGroundHitPos;

		bool bLosToHit = TestMultipleNavGenLos(vGroundProbeStart, vGroundProbeEnd, ANALYSE_NUM_NAVMESHES, ppNavGens, NAVGEN_COLLISION_TYPES, &vGroundHitPos, true, false, false);
		if(!bLosToHit)
			continue;

		vOut_ToPos = vNavMeshHitPos;

		// And check we are clear of collision a ped's radius behind (so we won't fail IsGroundClear() tests later and forefeit chance to create a dropdown here)
		vGroundProbeStart -= (vScanDir * fwNavGenConfig::Get().m_PedRadius);
		vGroundProbeEnd -= (vScanDir * fwNavGenConfig::Get().m_PedRadius);

		bLosToHit = TestMultipleNavGenLos(vGroundProbeStart, vGroundProbeEnd, ANALYSE_NUM_NAVMESHES, ppNavGens, NAVGEN_COLLISION_TYPES, &vGroundHitPos, true, false, false);
		if(!bLosToHit)
			continue;

		TNavMeshPoly * pHitNavMeshPoly = pHitNavMesh->GetPoly(iHitNavMeshPoly);
		if(pHitNavMeshPoly == pStartPoly)
			continue;

		Vector3 vVecToHitPos = vNavMeshHitPos - vStartPos;
		//float fDistBetweenPolys = vVecToHitPos.Mag();
		// We need to note whether the height of this drop-down is dangerous.
		// TODO : Maybe peds not concerned about dropping into water from a height?
		bool bHighDrop = (vVecToHitPos.z < -fMaxHeightForSafeDrop);

		//*************************************************************************
		// For authored climbs/drops we are not interested in determining validity

		if(bQuitWithoutPerformingTests)
		{
			adjPoly.m_iAdjacentNavMeshIndex = pHitNavMesh->m_iIndexOfMesh;
			adjPoly.SetOriginalNavMeshIndex(pHitNavMesh->m_iIndexOfMesh, pNavMesh->GetAdjacentMeshes());
			adjPoly.SetNavMeshIndex(pHitNavMesh->m_iIndexOfMesh, pNavMesh->GetAdjacentMeshes());
			adjPoly.SetOriginalPolyIndex(iHitNavMeshPoly);
			adjPoly.SetPolyIndex(iHitNavMeshPoly);
			adjPoly.SetHighDropOverEdge(bHighDrop);

			return true;
		}

		//*********************************************************************************************
		// Make sure that there is room for a ped to stand here, and the surface is approximately even

		if(!pHitNavMeshPoly->GetIsWater())	// not for the water surface, which isn't real geometry
		{
			if(!IsGroundEven(vNavMeshHitPos, iNumVerticalProbes, vProbeVerticalOffsets, ppNavGens, fMaxHeightDiffForEvenGround))
				continue;
		}

		//***********************************************************************************
		//	If the hit position is pretty similar to the start pos, then see if the ped is
		//	able to go directly from the start to the target.  Perform a set of tests from
		//	the startpos to the endpos directly.

		/*
		if(fDistBetweenPolys <= fMaxDistBetweenForSimpleAdjacency && Abs(vNavMeshHitPos.z - vStartPos.z) <= fMaxHeightDiffForSimpleAdjacency)
		{
			if(IsAdjacencyClear(vStartPos, vNavMeshHitPos, fwNavGenConfig::Get().m_PedRadius, 0.5f, 1.5f, ppNavGens, bConsiderClimbableObjects))
			{
				// A ped can possibly step directly from the start poly to the target poly
				// We might want to do a series of probes to see if there is a significant gap in-between
			}
		}
		*/

		//*************************************************************************************************
		//	Test in front of the startpos to see if there is an obstruction - and if so find whether it
		//	completely blocks the adjacency - if not then find what height the obstruction extends to.
		//	This is done by testing rays from the startpos to a position at the same height, but coincident
		//	with the navmesh intersect pos.

		Vector3 vCheckObstructionPos = vNavMeshHitPos;
		vCheckObstructionPos.z = vStartPos.z;

		float fHeightMustClimbOver;
		bool bAdjacencyBlocked = ! FindLowestClearPassage(vStartPos, vCheckObstructionPos, ppNavGens, fMaxHeightAboveStartToTestForObstructions, fHeightMustClimbOver, bConsiderClimbableObjects, true);

		// If adjacency is completely blocked infront of the start (including high climbs) then quit out completely
		if(bAdjacencyBlocked)
			return false;

		// The height of the final	 position must be lower-than or nearly equal-to the height which we've
		// calculated we need to climb.  This will rule out finding adjacencies on the floor above.
		if(vNavMeshHitPos.z - fHeightMustClimbOver > 0.5f)
			return false;

		float fClimbHeight = fHeightMustClimbOver - vStartPos.z;


		// Is there little or no climb involved in this adjacency - ie. could a ped move unobstructed to above the target position here?
		if(fClimbHeight < fMaxHeightDiffForSimpleAdjacency)
		{
			// Is this a drop down?
			if(vVecToHitPos.z < -fMaxHeightDiffForSimpleAdjacency)
			{
				// Test some probes from the position above the target, down to the target below.
				Vector3 vTestFromPos = vNavMeshHitPos + Vector3(0.0f, 0.0f, 0.5f);	// enough height to clear the ground at base
				float fHeightAboveTargetToTest = vStartPos.z - vNavMeshHitPos.z;
				fHeightAboveTargetToTest += 2.0f;									// plus enough height for a ped to stand here

				if(IsClearAbove(vTestFromPos, fHeightAboveTargetToTest, fwNavGenConfig::Get().m_PedRadius, ppNavGens, bConsiderClimbableObjects))
				{
					// Set up ADJACENCY_TYPE_DROPDOWN adjacency
					adjPoly.SetAdjacencyType(ADJACENCY_TYPE_DROPDOWN);
					adjPoly.m_iAdjacentNavMeshIndex = pHitNavMesh->m_iIndexOfMesh;
					adjPoly.SetOriginalNavMeshIndex(pHitNavMesh->m_iIndexOfMesh, pNavMesh->GetAdjacentMeshes());
					adjPoly.SetNavMeshIndex(pHitNavMesh->m_iIndexOfMesh, pNavMesh->GetAdjacentMeshes());
					adjPoly.SetOriginalPolyIndex(iHitNavMeshPoly);
					adjPoly.SetPolyIndex(iHitNavMeshPoly);
					adjPoly.SetHighDropOverEdge(bHighDrop);

					return true;
				}

				// The drop down to this poly is blocked : there is not enough clear space above the
				// target poly.  Perhaps we would want to 'continue' here so we can scan for another?
				return false;
			}
			// Is this a walk-over style adjacency?  If z diff didn't indicate a high-enough drop, then this may a walk-over adjacency.
			// Final test for this, since most of them should have been caught by the IsAdjacencyClear() code above anyway..
			if(vVecToHitPos.z < fMaxHeightDiffForSimpleAdjacency && !pStartPoly->GetIsWater())
			{
				// Set up STEPOVER adjacency.  (TODO)
				adjPoly.m_iAdjacentNavMeshIndex = pHitNavMesh->m_iIndexOfMesh;
				adjPoly.SetAdjacencyType(ADJACENCY_TYPE_DROPDOWN);
				adjPoly.SetOriginalNavMeshIndex(pHitNavMesh->m_iIndexOfMesh, pNavMesh->GetAdjacentMeshes());
				adjPoly.SetNavMeshIndex(pHitNavMesh->m_iIndexOfMesh, pNavMesh->GetAdjacentMeshes());
				adjPoly.SetOriginalPolyIndex(iHitNavMeshPoly);
				adjPoly.SetPolyIndex(iHitNavMeshPoly);
				adjPoly.SetHighDropOverEdge(bHighDrop);

				return true;
			}
			if(vVecToHitPos.z < fMaxHeightForLowClimbUp)
			{
				Vector3 vCheckObstructionPosAtHitHeight = vNavMeshHitPos;
				Vector3 vStartPosAtHitHeight = vStartPos;
				vStartPosAtHitHeight.z = vNavMeshHitPos.z;
				float fClearHeightAbove;

				// Test that there is another clear passage at the height of the hitpos.
				bool bAdjacencyBlockedAtHitHeight = ! FindLowestClearPassage(vStartPosAtHitHeight, vCheckObstructionPosAtHitHeight, ppNavGens, fMaxHeightAboveStartToTestForObstructions, fClearHeightAbove, bConsiderClimbableObjects, true);

				if(bAdjacencyBlockedAtHitHeight)
				{
					continue;
				}

				adjPoly.m_iAdjacentNavMeshIndex = pHitNavMesh->m_iIndexOfMesh;
				adjPoly.SetAdjacencyType(ADJACENCY_TYPE_CLIMB_LOW);
				adjPoly.SetOriginalNavMeshIndex(pHitNavMesh->m_iIndexOfMesh, pNavMesh->GetAdjacentMeshes());
				adjPoly.SetNavMeshIndex(pHitNavMesh->m_iIndexOfMesh, pNavMesh->GetAdjacentMeshes());
				adjPoly.SetOriginalPolyIndex(iHitNavMeshPoly);
				adjPoly.SetPolyIndex(iHitNavMeshPoly);
				adjPoly.SetHighDropOverEdge(bHighDrop);

				return true;
			}

			// If the above tests failed, it would indicate that there is an adjacent poly higher up than us - but there is no clear
			// way to get up there (even tho' the collision geometry would indicate that there was no definite blockage).
			// In this case we should just quit on this adjacency.

			// No adjacency
			return false;
		}

		// Is this a vault-over style climb?
		bool bVaultOver = fClimbHeight <= fMaxHeightForVaultOver;
		// Is this a climb-up style climb?
		bool bClimbUp = fClimbHeight <= fMaxHeightForClimbUp;

		if(!bVaultOver && !bClimbUp)
		{
			// No adjacency
			return false;
		}

		//*******************************************************************************************************************
		//	Since we've ascertained that this is a climb of some sort, see if there is a clear passage for a ped to climb up
		//	from the start to the position above the start.

		Vector3 vTestFromPos = vStartPos + Vector3(0.0f, 0.0f, 0.5f);	// enough height to clear the ground at base
		float fHeightAboveStartToTest = fHeightMustClimbOver - vStartPos.z;
		fHeightAboveStartToTest += 2.0f;	// Add enough room for a ped to stand up

		Assert(fHeightAboveStartToTest >= 0.0f);

		// Reduce slightly to account for places where we were unable to pull the navmesh correctly away from walls
		const float fStartTestRadius = Max(fwNavGenConfig::Get().m_PedRadius - 0.1f, 0.0f);

		if(!IsClearAbove(vTestFromPos, fHeightAboveStartToTest, fStartTestRadius, ppNavGens, bConsiderClimbableObjects))
		{
			// No adjacency
			return false;
		}

		// Do the same above the end position, up the the height of the climb + enough room for a ped to stand
		vTestFromPos = vNavMeshHitPos + Vector3(0.0f, 0.0f, 0.5f);
		static const float fPedHeight = 2.0f;	// 2.0f
		float fHeightAboveTargetToTest = (fHeightMustClimbOver - vNavMeshHitPos.z) + fPedHeight;
		fHeightAboveTargetToTest = Max(fHeightAboveTargetToTest, fPedHeight);

		if(!IsClearAbove(vTestFromPos, fHeightAboveTargetToTest, fwNavGenConfig::Get().m_PedRadius, ppNavGens, bConsiderClimbableObjects))
		{
			// No adjacency
			return false;
		}

		//***************************************************************************************
		//	If the target position is above the start pos, then we should now be able to say that
		//	this adjacency is clear - it seems to be a simple climb up onto a platform.
		//	We should already know that the area above the target is clear & flat, and that clear
		//	passages exist from the start to a point above, and from there to the target.

		if(vVecToHitPos.z > 0.0f)
		{
			Assert(!bHighDrop);

			eNavMeshPolyAdjacencyType adjType = bVaultOver ? ADJACENCY_TYPE_CLIMB_LOW : ADJACENCY_TYPE_CLIMB_HIGH;

			adjPoly.SetAdjacencyType(adjType);
			adjPoly.m_iAdjacentNavMeshIndex = pHitNavMesh->m_iIndexOfMesh;
			adjPoly.SetOriginalNavMeshIndex(pHitNavMesh->m_iIndexOfMesh, pNavMesh->GetAdjacentMeshes());
			adjPoly.SetNavMeshIndex(pHitNavMesh->m_iIndexOfMesh, pNavMesh->GetAdjacentMeshes());
			adjPoly.SetOriginalPolyIndex(iHitNavMeshPoly);
			adjPoly.SetPolyIndex(iHitNavMeshPoly);
			adjPoly.SetHighDropOverEdge(bHighDrop);

			return true;
		}

		//*********************************************************************************
		//	Otherwise we have a drop on the other side of the vault/climb.  We need to
		//	flag whether the height of the drop is dangerous.

		eNavMeshPolyAdjacencyType adjType = bVaultOver ? ADJACENCY_TYPE_CLIMB_LOW : ADJACENCY_TYPE_CLIMB_HIGH;

		adjPoly.SetAdjacencyType(adjType);
		adjPoly.m_iAdjacentNavMeshIndex = pHitNavMesh->m_iIndexOfMesh;
		adjPoly.SetOriginalNavMeshIndex(pHitNavMesh->m_iIndexOfMesh, pNavMesh->GetAdjacentMeshes());
		adjPoly.SetNavMeshIndex(pHitNavMesh->m_iIndexOfMesh, pNavMesh->GetAdjacentMeshes());
		adjPoly.SetOriginalPolyIndex(iHitNavMeshPoly);
		adjPoly.SetPolyIndex(iHitNavMeshPoly);
		adjPoly.SetHighDropOverEdge(bHighDrop);

		return true;
	}

	return false;
}

//*************************************************************************
//	IsGroundEven
//	This is called during the adjacency analysis.  A single probe has been
//	done downwards and found a navmesh poly; we now need to do a bunch of
//	line-tests to make sure that there is an even surface here to stand on.

bool
CNavGen::IsGroundEven(const Vector3 & vPos, const int iNumProbes, const Vector3 * pOffsets, CNavGen ** ppNavGens, const float fMaxAllowedHeightDiff)
{
	Vector3 vAbove(0.0f, 0.0f, 2.0f);
	Vector3 vHitPos;

	int p;
	for(p=0; p<iNumProbes; p++)
	{
		const Vector3 vProbeStart = vPos + pOffsets[p] + vAbove;
		const Vector3 vProbeEnd = vPos + pOffsets[p] - vAbove;

		bool bHit = ! TestMultipleNavGenLos(vProbeStart, vProbeEnd, ANALYSE_NUM_NAVMESHES, ppNavGens, NAVGEN_COLLISION_TYPES, &vHitPos, true);
		if(!bHit)
			return false;

		float fDelta = Abs(vHitPos.z - vPos.z);
		if(fDelta > fMaxAllowedHeightDiff)
			return false;
	}

	return true;
}

bool
CNavGen::IsAdjacencyClear(
	const Vector3 & vStart, const Vector3 & vEnd,
	const float fWidth, const float fHeightAboveStart, const float fTestHeight,
	CNavGen ** ppNavGens, bool bConsiderClimbableObjects)
{
	static const Vector3 vWorldUp(0.0f,0.0,1.0f);
	Vector3 vTestVec = vEnd - vStart;
	if(vTestVec.XYMag2() == 0.0f)
		return false;

	const float fExtrudeDist = vTestVec.Mag();
	vTestVec.Normalize();
	const Vector3 vRight = CrossProduct(vTestVec, vWorldUp);
	const Vector3 vUp = CrossProduct(vRight, vTestVec);

	const Vector3 vExtraHeight(0.0f, 0.0f, fHeightAboveStart);

	Vector3 vQuadPts[4];
	vQuadPts[0] = (vStart + vExtraHeight) + (vRight * fWidth);
	vQuadPts[1] = (vStart + vExtraHeight) + (vUp * fTestHeight) + (vRight * fWidth);
	vQuadPts[2] = (vStart + vExtraHeight) + (vUp * fTestHeight) - (vRight * fWidth);
	vQuadPts[3] = (vStart + vExtraHeight) - (vRight * fWidth);

	CClipVolume volume;
	volume.InitExtrudedQuad(vQuadPts, fExtrudeDist, &vTestVec);

	bool bLosIsClear = IsMultipleNavGenVolumeClear(volume, ANALYSE_NUM_NAVMESHES, ppNavGens, bConsiderClimbableObjects, NAVGEN_COLLISION_TYPES_EXCEPT_RIVER);

	return bLosIsClear;
}

/*
bool
CNavGen::IsAdjacencyClear(
	const Vector3 & vStart, const Vector3 & vEnd,
	const int iNumProbes, const Vector3 * pOffsets,
	CNavGen ** ppNavGens, const float fExtraHeight)
{
	Vector3 vExtraHeight(0.0f, 0.0f, fExtraHeight);

	int p;
	for(p=0; p<iNumProbes; p++)
	{
		const Vector3 vProbeStart = vStart + pOffsets[p] + vExtraHeight;
		const Vector3 vProbeEnd = vEnd + pOffsets[p] + vExtraHeight;

		bool bLosIsClear = TestMultipleNavGenLos(vProbeStart, vProbeEnd, ANALYSE_NUM_NAVMESHES, ppNavGens);
		if(!bLosIsClear)
			return false;
	}

	return true;
}
*/

/*
bool
CNavGen::IsClearAbove(
	const Vector3 & vPos, const float fDistAboveToTest,
	const int iNumProbes, const Vector3 * pOffsets,
	CNavGen ** ppNavGens,
	float & fHitHeight)
{
	Vector3 vHitPos;

	int p;
	for(p=0; p<iNumProbes; p++)
	{
		const Vector3 vProbeStart = vPos + pOffsets[p];
		const Vector3 vProbeEnd = vProbeStart + Vector3(0.0f, 0.0f, fDistAboveToTest);

		bool bLosIsClear = TestMultipleNavGenLos(vProbeStart, vProbeEnd, ANALYSE_NUM_NAVMESHES, ppNavGens, &vHitPos, true);
		if(!bLosIsClear)
		{
			fHitHeight = vHitPos.z;
			return false;
		}
	}

	return true;
}
*/

bool
CNavGen::IsClearAbove(
	const Vector3 & vPos, const float fDistAboveToTest,
	const float fRadius,
	CNavGen ** ppNavGens,
	bool bConsiderClimbableObjects)
{
	Assert(fDistAboveToTest > 0.0f);

	Vector3 vHitPos;
	Vector3 vQuadPts[4];
	vQuadPts[0] = vPos + Vector3(0.0f, -fRadius, 0.0f);
	vQuadPts[1] = vPos + Vector3(-fRadius, 0.0f, 0.0f);
	vQuadPts[2] = vPos + Vector3(0.0f, fRadius, 0.0f);
	vQuadPts[3] = vPos + Vector3(fRadius, 0.0f, 0.0f);

	Vector3 vExtrudeVec(0.0f,0.0f,1.0f);

	CClipVolume volume;
	volume.InitExtrudedQuad(vQuadPts, fDistAboveToTest, &vExtrudeVec);

	bool bLosIsClear = IsMultipleNavGenVolumeClear(volume, ANALYSE_NUM_NAVMESHES, ppNavGens, bConsiderClimbableObjects, NAVGEN_COLLISION_TYPES_EXCEPT_RIVER);

	return bLosIsClear;
}

//************************************************************************
//	FindLowestClearPassage
//	This function finds the lowest position at which the bundle of probes
//	from start to end is clear, if at all.  It will not process any higher
//	than fMaxHeightAboveStart above vStart.z
//

bool
CNavGen::FindLowestClearPassage(
	const Vector3 & vStart, const Vector3 & vEnd,
//	const int iNumProbes, const Vector3 * pOffsets,
	CNavGen ** ppNavGens, const float fMaxHeightAboveStart,
	float & fLowestClearHeight, bool bConsiderClimbableObjects, bool bTestForNotClimbableCollision)
{
	float fHeightOffset;
	static const float fHeightIncrement = 0.25f;
	static const float fClearPassageHeight = 2.0f;

	for(fHeightOffset=0.0f; fHeightOffset<=fMaxHeightAboveStart; fHeightOffset+=fHeightIncrement)
	{
		if(bTestForNotClimbableCollision)
		{
			Vector3 vColTestStart = vStart + Vector3(0.0f,0.0f,fHeightOffset);
			Vector3 vColTestEnd = vEnd + Vector3(0.0f,0.0f,fHeightOffset);
			Vector3 vColHit;
			float fColHit;
			CNavGenTri * pColHitTri = NULL;
			u32 iRetFlags = 0;
			if( ! ProcessMultipleNavGenLos(vColTestStart, vColTestEnd, ANALYSE_NUM_NAVMESHES, ppNavGens, iRetFlags, NAVGEN_COLLISION_TYPES_EXCEPT_RIVER, &vColHit, fColHit, pColHitTri, false, false, false) )
			{
				// If we have found non-climbable collision, then return that this adjacency is effectively blocked
				if(pColHitTri && pColHitTri->m_colPolyData.GetIsNotClimbableCollision())
					return false;
			}
		}
		bool bIsClear = IsAdjacencyClear(vStart, vEnd, fwNavGenConfig::Get().m_PedRadius, fHeightOffset, fClearPassageHeight, ppNavGens, bConsiderClimbableObjects);
		if(bIsClear)
		{
			fLowestClearHeight = vStart.z + fHeightOffset;
			return true;
		}
	}

	return false;
}


//*********************************************************************************************************
//	During the navmesh optimisation & merging, the ped density values can get averaged out amongst
//	surrounding (non-ped-density) polys.  This kind-of smears the values, and can cause problems when
//	peds then get created where they shouldn't & start wandering around.  This function corrects this
//	by setting the ped density to the maximum resampled from the collision mesh.

void
CNavGen::CorrectPedDensity(CNavMesh * pNavMesh, CNavGen * pNavGen)
{
	u32 p;
	u32 v;

	Vector3 vPt, vPtAbove, vPtBelow, vHitPos;
	float fHitDist;
	CNavGenTri * pHitTri;
	u32 iRetFlags = 0;
	
	for(p=0; p<pNavMesh->m_iNumPolys; p++)
	{
		TNavMeshPoly & poly = *pNavMesh->GetPoly(p);

		// Sample below each vertex, and a point at the center
		Vector3 vPoints[NAVMESHPOLY_MAX_NUM_VERTICES+1];
		pNavMesh->GetPolyCentroid(p, vPoints[0]);

		for(v=0; v<poly.GetNumVertices(); v++)
		{
			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex( &poly, v), vPoints[v+1] );
		}

		// For pavement polygons we take the highest ped density
		if(poly.GetIsPavement())
		{
			int iMaxPedDensity = 0;

			for(v=0; v<poly.GetNumVertices()+1; v++)
			{
				vPtAbove = vPoints[v];
				vPtAbove.z += 1.0f;
				vPtBelow = vPoints[v];
				vPtBelow.z -= 1.0f;

				if(pNavGen->m_Octree.ProcessLineOfSight(vPtAbove, vPtBelow, iRetFlags, NAVGEN_COLLISION_TYPES, vHitPos, fHitDist, pHitTri))
				{
					iMaxPedDensity = Max(pHitTri->m_colPolyData.GetPedDensity(), iMaxPedDensity);
				}
			}
			poly.SetPedDensity(iMaxPedDensity);
		}

		// For non-pavement polys which still have ped density, we set it to the minimum
		// sample of ped density under each vertex.  This is to prevent ped density spreading
		// into areas which it shouldn't
		else
		{
			bool bHit = false;

			int iMinPedDensity = MAX_PED_DENSITY;

			for(v=0; v<poly.GetNumVertices()+1; v++)
			{
				vPtAbove = vPoints[v];
				vPtAbove.z += 1.0f;
				vPtBelow = vPoints[v];
				vPtBelow.z -= 1.0f;

				if(pNavGen->m_Octree.ProcessLineOfSight(vPtAbove, vPtBelow, iRetFlags, NAVGEN_COLLISION_TYPES, vHitPos, fHitDist, pHitTri))
				{
					bHit = true;

					if(pHitTri->m_colPolyData.GetPedDensity() < iMinPedDensity)
					{
						iMinPedDensity = pHitTri->m_colPolyData.GetPedDensity();
					}
				}
			}

			if(!bHit)
				iMinPedDensity = 0;

			Assert(iMinPedDensity >= 0 && iMinPedDensity <= MAX_PED_DENSITY);
			if(iMinPedDensity < 0) iMinPedDensity = 0;
			if(iMinPedDensity > MAX_PED_DENSITY) iMinPedDensity = MAX_PED_DENSITY;

			poly.SetPedDensity(iMinPedDensity);
		}
	}
}

// NAME : SetRoadFlags
// PURPOSE : Tag as 'road' any navmesh polygon which is touching a collision polygon marked as road.
//			 This leads to an overestimate of the road in the navmesh, but this is ok since we are
//			 using this flag to rule out network spawning on roads (and thus an overestimate is ok)

void CNavGen::SetRoadFlags(CNavMesh * pNavmesh)
{
	for(s32 t=0; t<m_CollisionTriangles.size(); t++)
	{
		CNavGenTri * pTri = &m_CollisionTriangles[t];
		if(!pTri->m_colPolyData.GetIsRoad())
			continue;

		Vector3 vPts[] = { pTri->m_vPts[0], pTri->m_vPts[1], pTri->m_vPts[2] };
		vPts[0].z -= 0.5f;
		vPts[1].z -= 0.5f;
		vPts[2].z -= 0.5f;

		TShortMinMax triMinMax = pTri->m_MinMax;
		triMinMax.m_iMinZ -= (MINMAX_ONE>>1);
		triMinMax.m_iMaxZ += (MINMAX_ONE>>1);

		CClipVolume vol;
		vol.InitExtrudedTri(&vPts[0], 1.0f, &pTri->m_vNormal);

		for(u32 p=0; p<pNavmesh->GetNumPolys(); p++)
		{
			TNavMeshPoly * pPoly = pNavmesh->GetPoly(p);
			if(!pPoly->GetIsRoad() && pPoly->m_MinMax.Intersects(triMinMax))
			{
				Vector3 vPolyTri[3];
				for(u32 v=1; v<pPoly->GetNumVertices()-1; v++)
				{
					pNavmesh->GetVertex( pNavmesh->GetPolyVertexIndex(pPoly, 0), vPolyTri[0] );
					pNavmesh->GetVertex( pNavmesh->GetPolyVertexIndex(pPoly, v), vPolyTri[1] );
					pNavmesh->GetVertex( pNavmesh->GetPolyVertexIndex(pPoly, v+1), vPolyTri[2] );

					if(vol.TestTriIntersection(vPolyTri))
					{
						pPoly->SetIsRoad(true);
						break;
					}
				}
			}
		}
	}
}

// NAME : SetTrainTrackFlags
// PURPOSE : Tag as 'train-tracks' any navmesh polygon which is touching a collision polygon marked as train-tracks.

void CNavGen::SetTrainTrackFlags(CNavMesh * pNavmesh)
{
	for(s32 t=0; t<m_CollisionTriangles.size(); t++)
	{
		CNavGenTri * pTri = &m_CollisionTriangles[t];
		if(!pTri->m_colPolyData.GetIsTrainTracks())
			continue;

		Vector3 vPts[] = { pTri->m_vPts[0], pTri->m_vPts[1], pTri->m_vPts[2] };
		vPts[0].z -= 0.5f;
		vPts[1].z -= 0.5f;
		vPts[2].z -= 0.5f;

		TShortMinMax triMinMax = pTri->m_MinMax;
		triMinMax.m_iMinZ -= (MINMAX_ONE>>1);
		triMinMax.m_iMaxZ += (MINMAX_ONE>>1);

		CClipVolume vol;
		vol.InitExtrudedTri(&vPts[0], 1.0f, &pTri->m_vNormal);

		for(u32 p=0; p<pNavmesh->GetNumPolys(); p++)
		{
			TNavMeshPoly * pPoly = pNavmesh->GetPoly(p);
			if(!pPoly->GetIsTrainTrack() && pPoly->m_MinMax.Intersects(triMinMax))
			{
				Vector3 vPolyTri[3];
				for(u32 v=1; v<pPoly->GetNumVertices()-1; v++)
				{
					pNavmesh->GetVertex( pNavmesh->GetPolyVertexIndex(pPoly, 0), vPolyTri[0] );
					pNavmesh->GetVertex( pNavmesh->GetPolyVertexIndex(pPoly, v), vPolyTri[1] );
					pNavmesh->GetVertex( pNavmesh->GetPolyVertexIndex(pPoly, v+1), vPolyTri[2] );

					if(vol.TestTriIntersection(vPolyTri))
					{
						pPoly->SetIsTrainTrack(true);
						break;
					}
				}
			}
		}
	}
}

//***************************************************************
//	Helper function to reset timestamps.  Timestamps will likely
//	overflow during floodfilling functions below..

void CNavGen::SetAllPolyTimestamps(const u16 iTimestamp)
{
	int x,y;
	u32 p;

	for(y=0; y<CPathServerExtents::m_iNumNavMeshesInY; y++)
	{
		for(x=0; x<CPathServerExtents::m_iNumNavMeshesInX; x++)
		{
			const u32 iIndex = (y*CPathServerExtents::m_iNumNavMeshesInX)+x;
			CNavMesh * pNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(iIndex);
			if(pNavMesh)
			{
				for(p=0; p<pNavMesh->m_iNumPolys; p++)
				{
					TNavMeshPoly & poly = *pNavMesh->GetPoly(p);
					poly.m_AStarTimeStamp = iTimestamp;
				}
			}
		}
	}
}

//**********************************************************************************
//	SetFreeSpaceAroundPolys
//	This function uses the navmesh LOS tests to try to work out how much navigable
//	space surrounds the centre of each navmesh polygon.

/*
#define NUM_FREE_SPACE_TESTS	8

void CNavGen::SetFreeSpaceAroundPolys()
{
	Vector3 vFreeSpaceRays[NUM_FREE_SPACE_TESTS];
	const Vector3 vForwards(0.0f, 1.0f, 0.0f);
	const float fAngleInc = ((PI*2.0f) / (float)NUM_FREE_SPACE_TESTS);
	float fAngle = 0.0f;
	Matrix34 mRotMat;

	for(s32 r=0; r<NUM_FREE_SPACE_TESTS; r++)
	{
		mRotMat.Identity();
		mRotMat.RotateFullZ(fAngle);
		mRotMat.Transform(vForwards, vFreeSpaceRays[r]);
		vFreeSpaceRays[r].Normalize();

		fAngle += fAngleInc;
	}

	// Potentially one at every edge-centre, plus the poly centroid
	Vector3 vTestOrigins[NAVMESHPOLY_MAX_NUM_VERTICES+1];

	const float fMaxFreeSpaceDist = 32.0f;

	for(s32 y=0; y<CPathServerExtents::m_iNumNavMeshesInY; y++)
	{
		for(s32 x=0; x<CPathServerExtents::m_iNumNavMeshesInX; x++)
		{
			const u32 iIndex = (y*CPathServerExtents::m_iNumNavMeshesInX)+x;
			CNavMesh * pNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(iIndex);
			if(pNavMesh)
			{
				for(u32 p=0; p<pNavMesh->m_iNumPolys; p++)
				{
					TNavMeshPoly * pPoly = &pNavMesh->m_Polys[p];

					u32 v;
					Vector3 vCentroid(0.0f, 0.0f, 0.0f);
					for(v=0; v<pPoly->GetNumVertices(); v++)
					{
						pNavMesh->GetPolyEdgeMidpoint(vTestOrigins[v], pPoly, v);
						vTestOrigins[v].z += 1.0f;

						// accumulate average
						vCentroid += vTestOrigins[v];
					}
					vCentroid /= (float)pPoly->GetNumVertices();
					vTestOrigins[v]  = vCentroid;

					float fLargestClearDistance = 0.0f;

					for(u32 t=0; t<pPoly->GetNumVertices()+1; t++)
					{
						bool bHitEdge = false;

						float f;
						for(f=1.0f; f<fMaxFreeSpaceDist && !bHitEdge; f+=1.0f)
						{
							for(int r=0; r<NUM_FREE_SPACE_TESTS && !bHitEdge; r++)
							{
								const Vector3 vEndPos = vTestOrigins[t] + (vFreeSpaceRays[r] * f);
								if(!CNavGen_NavMeshStore::TestNavMeshLOS(vTestOrigins[t], vEndPos))
								{
									bHitEdge = true;
								}
							}
						}

						f = Clamp(f - 1.0f, 0.0f, fMaxFreeSpaceDist - 1.0f);

						fLargestClearDistance = Max(fLargestClearDistance, f);
					}

					pPoly->SetFreeSpaceAroundPolyCentre( (u32) fLargestClearDistance );
				}
			}
		}
	}
}
*/

bool bCalcFreeSpaceBeyondPolyEdges = false;

void CNavGen::CalcFreeSpaceBeyondPolyEdges(CNavMesh * pNavMesh)
{
	if( pNavMesh )
	{
		for(u32 p=0; p<pNavMesh->GetNumPolys(); p++)
		{
			TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);

			for(u32 e=0; e<pPoly->GetNumVertices(); e++)
			{
				const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly(pPoly->GetFirstVertexIndex() + e);
				const u32 iAdjNavMesh = adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes());

				if(bCalcFreeSpaceBeyondPolyEdges)
				{
					if(iAdjNavMesh == NAVMESH_NAVMESH_INDEX_NONE || adjPoly.GetAdjacencyType()!=ADJACENCY_TYPE_NORMAL)
					{
						CalcFreeSpaceBeyondPolyEdge(pPoly, pNavMesh, e);
					}
					else
					{
						pNavMesh->GetAdjacentPolysArray().Get( pPoly->GetFirstVertexIndex() + e )->SetFreeSpaceBeyondEdge(TAdjPoly::ms_fMaxFreeSpaceBeyondEdge);
					}
				}
				else
				{
					pNavMesh->GetAdjacentPolysArray().Get( pPoly->GetFirstVertexIndex() + e )->SetFreeSpaceBeyondEdge(0);
				}
			}
		}
	}
}

void CNavGen::CalcFreeSpaceBeyondPolyEdge(TNavMeshPoly * pPoly, CNavMesh * pNavMesh, s32 iEdge)
{
	Vector3 vEdgeVert1, vEdgeVert2;
	pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex( pPoly, iEdge ), vEdgeVert1 );
	pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex( pPoly, (iEdge+1) % pPoly->GetNumVertices() ), vEdgeVert2 );

	Vector3 vEdgeNormal;
	pNavMesh->GetPolyEdgeNormal( vEdgeNormal, pPoly, iEdge );

	static const float fMaxDist = 15.0f;
	static const float fStepsPerUnit = 1.0f;

	float fCurrentDist = fStepsPerUnit;

	while(fCurrentDist < fMaxDist)
	{
		Vector3 vDir = (vEdgeNormal * fCurrentDist);
		Vector2 vDir2d( vDir.x, vDir.y );
		Vector3 vExtrudedVert1 = vEdgeVert1 + vDir;
		Vector3 vExtrudedVert2 = vEdgeVert2 + vDir;

		// TODO@James : need to duplicate LOS code from CPathServerThread in NavMeshMaker; because this project can no longer
		// include code form the game.  Once thats done can proceed with navmesh variable-radius analysis code.

		m_iNavMeshPolyTimestamp++;
		TNavMeshPoly * pStartPoly = TestNavMeshLOS( vEdgeVert1, vDir2d, NULL, pPoly, 0 );
		if(!pStartPoly)
			break;

		m_iNavMeshPolyTimestamp++;
		TNavMeshPoly * pEndPoly = TestNavMeshLOS( vEdgeVert2, vDir2d, NULL, pPoly, 0 );
		if(!pEndPoly)
			break;

		const Vector2 vEdgeDir( vEdgeVert2.x-vEdgeVert1.x, vEdgeVert2.y-vEdgeVert1.y );

		m_iNavMeshPolyTimestamp++;
		TNavMeshPoly * pHitPoly = TestNavMeshLOS( vExtrudedVert1, vEdgeDir, NULL, pStartPoly, 0 );
		if( !pHitPoly )
			break;

		fCurrentDist += fStepsPerUnit;
	}

	// We should now have an accurate measurement of the free space beyond the edge of this polygon
	float fSpace = (fCurrentDist - fStepsPerUnit);
	fSpace = Clamp(fSpace, 0.0f, TAdjPoly::ms_fMaxFreeSpaceBeyondEdge);

	pNavMesh->GetAdjacentPolysArray().Get( pPoly->GetFirstVertexIndex() + iEdge )->SetFreeSpaceBeyondEdge(fSpace);
}


#define NUM_LOS_DIRECTIONS_AROUND_VERTEX	8	// was 16
static Vector2 vVertexLosDirections[NUM_LOS_DIRECTIONS_AROUND_VERTEX];
bool bVertexLosDirectionsCalculated = false;

void CNavGen::CalcFreeSpaceAroundVertices(CNavMesh * pNavMesh, CNavMesh * pNegX, CNavMesh * pPosX, CNavMesh * pNegY, CNavMesh * pPosY)
{
	if(!bVertexLosDirectionsCalculated)
	{
		const float fInc = (360.0f / ((float)NUM_LOS_DIRECTIONS_AROUND_VERTEX)) * DtoR;
		const Vector2 vOriginal(1.0f, 0.0f);
		float fRot = 0.1f;
		for(int i=0; i<NUM_LOS_DIRECTIONS_AROUND_VERTEX; i++)
		{
			vVertexLosDirections[i] = vOriginal;
			vVertexLosDirections[i].Rotate(fRot);
			fRot += fInc;
		}
		bVertexLosDirectionsCalculated = true;
	}

	if( pNavMesh )
	{
		float * pFreeSpace = rage_new float[pNavMesh->GetNumVertices()];

		// The space around vertex calculations are expensive
		// We can trivially reject any vertex which borders an open edge in any polygon.
/*
		for(u32 v=0; v<pNavMesh->GetNumVertices(); v++)
		{
			// Use -1.0f as a sentinel value
			pFreeSpace[v] = -1.0f;

			bool bTrivialReject = false;

			// Iterate all polygons in navmesh
			for(u32 p=0; p<pNavMesh->GetNumPolys() && !bTrivialReject; p++)
			{
				TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);

				// For each polygon, iterate their vertex indices to see if this poly uses the vertex V
				u32 lasti = pPoly->GetNumVertices()-1;
				for(u32 i=0; i<pPoly->GetNumVertices() && !bTrivialReject; i++)
				{
					u32 vi = pNavMesh->GetPolyVertexIndex(pPoly, i);
					if(vi == v)
					{
						// This polygon is using the vertex.
						// Now obtain the polygon adjacency info TAdjPoly, on each side of this vertex

						const TAdjPoly & adjBefore = pNavMesh->GetAdjacentPoly(pPoly->GetFirstVertexIndex()+lasti);
						const TAdjPoly & adjAfter = pNavMesh->GetAdjacentPoly(pPoly->GetFirstVertexIndex()+i);

						if(adjBefore.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes())==NAVMESH_NAVMESH_INDEX_NONE || adjBefore.GetPolyIndex()==NAVMESH_POLY_INDEX_NONE)
						{
							bTrivialReject = true;
							break;
						}
						else if(adjAfter.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes())==NAVMESH_NAVMESH_INDEX_NONE || adjAfter.GetPolyIndex()==NAVMESH_POLY_INDEX_NONE)
						{
							bTrivialReject = true;
							break;
						}
						else if(adjBefore.GetAdjacencyType()!=ADJACENCY_TYPE_NORMAL || adjAfter.GetAdjacencyType()!=ADJACENCY_TYPE_NORMAL)
						{
							bTrivialReject = true;
							break;
						}
					}
					lasti = i;
				}
			}

			if(bTrivialReject)
			{
				pFreeSpace[v] = 0.0f;
			}
		}
*/
		// Now iterate vertices again, and do the full calculation for any which we could not trivially reject
		// That is - any vertices whose freespace value is still it's initial -1.0f
		for(u32 v=0; v<pNavMesh->GetNumVertices(); v++)
		{
			//if(pFreeSpace[v] < 0.0f)
			{
				float fSpace = CalcFreeSpaceAroundVertex(pNavMesh, pNegX, pPosX, pNegY, pPosY, v);
				fSpace = Clamp(fSpace, 0.0f, TAdjPoly::ms_fMaxFreeSpaceAroundVertex);

				pFreeSpace[v] = fSpace;
			}
		}

		for(u32 p=0; p<pNavMesh->GetNumPolys(); p++)
		{
			TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);

			for(u32 v=0; v<pPoly->GetNumVertices(); v++)
			{
				u32 iVertexIndex = pNavMesh->GetPolyVertexIndex(pPoly, v);
				pNavMesh->GetAdjacentPolysArray().Get(pPoly->GetFirstVertexIndex()+v)->SetFreeSpaceAroundVertex( pFreeSpace[iVertexIndex] );
			}
		}

		delete[] pFreeSpace;
	}
}

const Vector3 g_vJitterOffsets[9] =
{
	Vector3(0.0f, 0.0f, 0.0f),
	Vector3(-0.99f, 0.01f, 0.0f),
	Vector3(0.99f, 0.01f, 0.0f),
	Vector3(0.01f, -0.99f, 0.0f),
	Vector3(0.01f, 0.99f, 0.0f),
	Vector3(-0.707f, -0.707f, 0.0f),
	Vector3(0.707f, -0.707f, 0.0f),
	Vector3(0.707f, 0.707f, 0.0f),
	Vector3(-0.707f, 0.707f, 0.0f)
};
u32 GetNavmeshPolyBelowPointJittered(CNavMesh * pNavMesh, const Vector3 & vPos, const float fDistBelow, const float fJitterSize, Vector3 & vIntersectPos)
{
	for(int j=0; j<9; j++)
	{
		const Vector3 vOffset = g_vJitterOffsets[j] * fJitterSize;
		u32 iStartPoly = pNavMesh->GetPolyBelowPoint(vPos + vOffset, vIntersectPos, fDistBelow);
		if(iStartPoly != NAVMESH_POLY_INDEX_NONE)
			return iStartPoly;
	}
	return NAVMESH_POLY_INDEX_NONE;
}

float CNavGen::CalcFreeSpaceAroundVertex(CNavMesh * pNavMeshCenter, CNavMesh * UNUSED_PARAM(pNegX), CNavMesh * UNUSED_PARAM(pPosX), CNavMesh * UNUSED_PARAM(pNegY), CNavMesh * UNUSED_PARAM(pPosY), const s32 iVertex)
{
	Vector3 vVertex, vIntersect;
	Vector2 vLosDir;
	pNavMeshCenter->GetVertex(iVertex, vVertex);

	float fCurrentDistance = 0.0f;
	const float fMaxDistance = TAdjPoly::ms_fMaxFreeSpaceAroundVertex;
	const float fDistanceStep = TAdjPoly::ms_fFreeSpaceFixedToFloatMult;

	int n;
	u32 iStartPoly = NAVMESH_POLY_INDEX_NONE;
	u32 iEndPoly = NAVMESH_POLY_INDEX_NONE;

	// Work out which adjacent navmeshes we may need to visit
	const float fAdjNavDist = fMaxDistance + 0.1f;
	CNavMesh * ppMeshes[9] = { pNavMeshCenter, 0, 0, 0, 0, 0, 0, 0, 0 };
	int iNumMeshes = 1;

	s32 iMeshX, iMeshY;
	s32 iSectorX, iSectorY;
	CNavGen_NavMeshStore::GetSectorFromNavMeshIndex(pNavMeshCenter->GetIndexOfMesh(), iSectorX, iSectorY);
	iMeshX = iSectorX / CPathServerExtents::m_iNumSectorsPerNavMesh;
	iMeshY = iSectorY / CPathServerExtents::m_iNumSectorsPerNavMesh;

	const bool bNegX = (vVertex.x - fAdjNavDist < pNavMeshCenter->m_pQuadTree->m_Mins.x && iMeshX > 0);
	const bool bNegY = (vVertex.y - fAdjNavDist < pNavMeshCenter->m_pQuadTree->m_Mins.y && iMeshY > 0);
	const bool bPosX = (vVertex.x + fAdjNavDist > pNavMeshCenter->m_pQuadTree->m_Maxs.x && iMeshX < CPathServerExtents::m_iNumNavMeshesInX-1);
	const bool bPosY = (vVertex.y + fAdjNavDist > pNavMeshCenter->m_pQuadTree->m_Maxs.y && iMeshY < CPathServerExtents::m_iNumNavMeshesInY-1);

	if(bNegX)
		ppMeshes[iNumMeshes++] = CNavGen_NavMeshStore::GetNavMeshFromSectorCoords(iSectorX-CPathServerExtents::m_iNumSectorsPerNavMesh,iSectorY);
	if(bNegY)
		ppMeshes[iNumMeshes++] = CNavGen_NavMeshStore::GetNavMeshFromSectorCoords(iSectorX,iSectorY-CPathServerExtents::m_iNumSectorsPerNavMesh);
	if(bPosX)
		ppMeshes[iNumMeshes++] = CNavGen_NavMeshStore::GetNavMeshFromSectorCoords(iSectorX+CPathServerExtents::m_iNumSectorsPerNavMesh,iSectorY);
	if(bPosY)
		ppMeshes[iNumMeshes++] = CNavGen_NavMeshStore::GetNavMeshFromSectorCoords(iSectorX,iSectorY+CPathServerExtents::m_iNumSectorsPerNavMesh);

	if(bNegX && bNegY)
		ppMeshes[iNumMeshes++] = CNavGen_NavMeshStore::GetNavMeshFromSectorCoords(iSectorX-CPathServerExtents::m_iNumSectorsPerNavMesh,iSectorY-CPathServerExtents::m_iNumSectorsPerNavMesh);
	if(bNegX && bPosY)
		ppMeshes[iNumMeshes++] = CNavGen_NavMeshStore::GetNavMeshFromSectorCoords(iSectorX-CPathServerExtents::m_iNumSectorsPerNavMesh,iSectorY+CPathServerExtents::m_iNumSectorsPerNavMesh);
	if(bPosX && bNegY)
		ppMeshes[iNumMeshes++] = CNavGen_NavMeshStore::GetNavMeshFromSectorCoords(iSectorX+CPathServerExtents::m_iNumSectorsPerNavMesh,iSectorY-CPathServerExtents::m_iNumSectorsPerNavMesh);
	if(bPosX && bPosY)
		ppMeshes[iNumMeshes++] = CNavGen_NavMeshStore::GetNavMeshFromSectorCoords(iSectorX+CPathServerExtents::m_iNumSectorsPerNavMesh,iSectorY+CPathServerExtents::m_iNumSectorsPerNavMesh);

	// Despite all the above combinations, we should never be visiting more that 4 meshes since the
	// scanning distance around each vertex is only a handful of meters.
	Assert(iNumMeshes <= 4);

	const float edgeEps = 0.0f; //25f;
	const Vector3 vRaiseUp = ZAXIS * 3.0f;

	while(fCurrentDistance < fMaxDistance)
	{
		for(s32 d=0; d<NUM_LOS_DIRECTIONS_AROUND_VERTEX; d++)
		{
			const Vector2 vOffset1 = vVertexLosDirections[d] * (fCurrentDistance+fDistanceStep);
			const Vector2 vOffset2 = vVertexLosDirections[(d+1)%NUM_LOS_DIRECTIONS_AROUND_VERTEX] * (fCurrentDistance+fDistanceStep);

			const Vector3 vStartPos(vVertex.x + vOffset1.x, vVertex.y + vOffset1.y, vVertex.z);
			iStartPoly = NAVMESH_POLY_INDEX_NONE;

			for(n=0; n<5; n++)
			{
				if(ppMeshes[n])
				{
					if( vStartPos.x >= ppMeshes[n]->m_pQuadTree->m_Mins.x-edgeEps && vStartPos.y >= ppMeshes[n]->m_pQuadTree->m_Mins.y-edgeEps &&
						vStartPos.x <= ppMeshes[n]->m_pQuadTree->m_Maxs.x+edgeEps && vStartPos.y <= ppMeshes[n]->m_pQuadTree->m_Maxs.y+edgeEps )
					{
						iStartPoly = GetNavmeshPolyBelowPointJittered(ppMeshes[n], vStartPos + vRaiseUp, 8.0f, 0.05f, vIntersect);
						if(iStartPoly != NAVMESH_POLY_INDEX_NONE)
							break;
					}
				}
			}

			if(iStartPoly == NAVMESH_POLY_INDEX_NONE)
				return fCurrentDistance;
			TNavMeshPoly * pStartPoly = ppMeshes[n]->GetPoly(iStartPoly);
			if(!pStartPoly)
				return fCurrentDistance;

			//--------------------------------------

			const Vector3 vEndPos(vVertex.x + vOffset2.x, vVertex.y + vOffset2.y, vVertex.z);
			iEndPoly = NAVMESH_POLY_INDEX_NONE;

			for(n=0; n<5; n++)
			{
				if(ppMeshes[n])
				{
					if( vEndPos.x >= ppMeshes[n]->m_pQuadTree->m_Mins.x-edgeEps && vEndPos.y >= ppMeshes[n]->m_pQuadTree->m_Mins.y-edgeEps &&
						vEndPos.x <= ppMeshes[n]->m_pQuadTree->m_Maxs.x+edgeEps && vEndPos.y <= ppMeshes[n]->m_pQuadTree->m_Maxs.y+edgeEps )
					{
						iEndPoly = GetNavmeshPolyBelowPointJittered(ppMeshes[n], vEndPos + vRaiseUp, 8.0f, 0.05f, vIntersect);
						if(iEndPoly != NAVMESH_POLY_INDEX_NONE)
							break;
					}
				}
			}

			if(iEndPoly == NAVMESH_POLY_INDEX_NONE)
				return fCurrentDistance;
			TNavMeshPoly * pEndPoly = ppMeshes[n]->GetPoly(iEndPoly);
			if(!pEndPoly)
				return fCurrentDistance;

			m_iNavMeshPolyTimestamp++;

			bool bLos = TestNavMeshLOS( vStartPos, vEndPos, pEndPoly, pStartPoly, NULL, 0 );
			if(!bLos)
				return fCurrentDistance;
		}
		fCurrentDistance += fDistanceStep;
	}

	return fCurrentDistance;
}



CTestNavMeshLosVars g_LosVars;

bool CNavGen::TestNavMeshLOS(const Vector3 & vStartPos, const Vector3 & vEndPos, const TNavMeshPoly * pToPoly, TNavMeshPoly * pTestPoly, TNavMeshPoly * pLastPoly, const u32 iLosFlags)
{
	g_LosVars.Init(vStartPos, vEndPos, pToPoly, iLosFlags);

	return TestNavMeshLOS_R(pTestPoly, pLastPoly);
}

TNavMeshPoly * CNavGen::TestNavMeshLOS(const Vector3 & vStartPos, const Vector2 & vLosDir, Vector3 * pvOut_IntersectionPos, TNavMeshPoly * pStartPoly, const u32 iLosFlags)
{
	if(!pStartPoly)
	{
		Vector3 vTmp;

		const u32 iStartNavMesh = CNavGen_NavMeshStore::GetNavMeshIndexFromPosition(vStartPos);
		if(iStartNavMesh == NAVMESH_NAVMESH_INDEX_NONE)
			return NULL;
		CNavMesh * pStartNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(iStartNavMesh);
		if(!pStartNavMesh)
			return NULL;
		const u32 iStartPolyIndex = pStartNavMesh->GetPolyBelowPoint(vStartPos + Vector3(0, 0, 1.0f), vTmp, 5.0f);
		if(iStartPolyIndex == NAVMESH_POLY_INDEX_NONE)
			return NULL;
		pStartPoly = pStartNavMesh->GetPoly(iStartPolyIndex);
		Assert(pStartPoly);
	}

	g_LosVars.Init(vStartPos, vLosDir, iLosFlags);

	const bool bIntersection = TestNavMeshLOS_R(pStartPoly, NULL);

	if(bIntersection)
	{
		if(pvOut_IntersectionPos)
			*pvOut_IntersectionPos = g_LosVars.m_vIntersectionPosition;
		return g_LosVars.m_pPolyEndedUpon;
	}
	return NULL;
}

bool CNavGen::TestNavMeshLOS_R(TNavMeshPoly * pTestPoly, TNavMeshPoly * pLastPoly)
{
	g_LosVars.m_iTestLosStackNumEntries = 0;

	Vector3 vEdgeVert1, vEdgeVert2;
	u32 v;
	s32 lastv;

RESTART_FOR_RECURSION:

	CNavMesh * pTestPolyNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(pTestPoly->GetNavMeshIndex());
	if(pTestPolyNavMesh)
	{
		if(g_LosVars.m_pToPoly)
		{
			// In the case where we know the destination poly, then see if we've arrived there
			if(pTestPoly == g_LosVars.m_pToPoly)
			{
				// LINE OF SIGHT EXISTS
				g_LosVars.m_iTestLosStackNumEntries = 0;
				return true;
			}
		}
		else
		{
			// In cases where we have no destination poly, then check for an minmax intersection with the known endpos
			// versus this polygon.  If this succeeds, then try a full polygon containment test.

			if(g_LosVars.m_EndPosMinMax.IntersectsXY(pTestPoly->m_MinMax))
			{
				g_LosVars.m_vAboveEndPos.z = pTestPoly->m_MinMax.GetMaxZAsFloat() + 1.0f;
				g_LosVars.m_vBelowEndPos.z = pTestPoly->m_MinMax.GetMinZAsFloat() - 1.0f;

				const bool bIntersects = pTestPolyNavMesh->RayIntersectsPoly(g_LosVars.m_vAboveEndPos, g_LosVars.m_vBelowEndPos, pTestPoly, g_LosVars.m_vIntersectionPosition);
				if(bIntersects)
				{
					g_LosVars.m_iTestLosStackNumEntries = 0;
					g_LosVars.m_pPolyEndedUpon = pTestPoly;
					return true;
				}
			}
		}

		lastv = pTestPoly->GetNumVertices()-1;
		for(v=0; v<pTestPoly->GetNumVertices(); v++)
		{
			const TAdjPoly & adjPoly = pTestPolyNavMesh->GetAdjacentPoly(pTestPoly->GetFirstVertexIndex() + lastv);

			// We cannot complete a LOS over an adjacency which is a climb-up/drop-down etc.
			if(adjPoly.GetPolyIndex() != NAVMESH_POLY_INDEX_NONE && adjPoly.GetAdjacencyType() == ADJACENCY_TYPE_NORMAL)
			{
				// Check that this NavMesh exists & is loaded. (It's quite possible for the refinement algorithm to
				// visit polys which the poly-pathfinder didn't..)
				CNavMesh * pAdjMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(adjPoly.GetNavMeshIndex(pTestPolyNavMesh->GetAdjacentMeshes()));
				if(!pAdjMesh)
				{
					lastv = v;
					continue;
				}

				TNavMeshPoly * pAdjPoly = pAdjMesh->GetPoly(adjPoly.GetPolyIndex());

				Assertf(!pAdjPoly->GetIsDegenerateConnectionPoly(), "CNavGen::TestNavMeshLOS_R() encountered a connection poly");


				// Make sure we don't recurse back & forwards indefinitely
				// However, disregard the timestamp for the "pToPoly" because we don't want to screw our chances
				// of getting a decent LOS via another polygon if the "pToPoly" is adjacent to the "pFromPoly"
				// but happens to be not directly visible by the adjoining edge..
				if(pAdjPoly != pLastPoly && (pAdjPoly->m_TimeStamp != m_iNavMeshPolyTimestamp || pAdjPoly == g_LosVars.m_pToPoly))
				{
					pTestPolyNavMesh->GetVertex( pTestPolyNavMesh->GetPolyVertexIndex(pTestPoly, lastv), vEdgeVert1);
					pTestPolyNavMesh->GetVertex( pTestPolyNavMesh->GetPolyVertexIndex(pTestPoly, v), vEdgeVert2);

					if(CNavMesh::LineSegsIntersect2D(g_LosVars.m_vStartPos, g_LosVars.m_vEndPos, vEdgeVert1, vEdgeVert2))
					{
						pAdjPoly->m_TimeStamp = m_iNavMeshPolyTimestamp;

						//****************************************************************
						// This is the point of recursion in the original algorithm.
						// Instead of recursing, we'll store off the algorithm state in a
						// stack entry in "g_LosVars.m_TestLosStack", and will jump to the label
						// "RESTART_FOR_RECURSION".

						if(g_LosVars.m_iTestLosStackNumEntries >= SIZE_TEST_LOS_STACK)
						{
							Assertf(g_LosVars.m_iTestLosStackNumEntries < SIZE_TEST_LOS_STACK, "TestNavMeshLOS recursion stack reached its limit of %i.", SIZE_TEST_LOS_STACK);
							g_LosVars.m_iTestLosStackNumEntries = 0;
							return false;
						}

						{
							TTestLosStack & stackEntry = g_LosVars.m_TestLosStack[g_LosVars.m_iTestLosStackNumEntries];
							g_LosVars.m_iTestLosStackNumEntries++;

							stackEntry.pTestPoly = pTestPoly;
							stackEntry.pLastPoly = pLastPoly;
							stackEntry.iVertex = v;

							// Set pTestPoly & pLastPoly to their new values, as would occur in recursive call.
							pTestPoly = pAdjPoly;
							pLastPoly = pTestPoly;
						}
						
						goto RESTART_FOR_RECURSION;


					}
				}
			}
DROP_OUT_OF_RECURSION:	;

			lastv = v;
		}
	}

	//********************************************************************
	// This is the point at which we would drop out of recursion.  If we
	// have any points of recursion stored in out stack, then re-enter
	// them now.

	if(g_LosVars.m_iTestLosStackNumEntries > 0)
	{
		g_LosVars.m_iTestLosStackNumEntries--;

		TTestLosStack & stackEntry = g_LosVars.m_TestLosStack[g_LosVars.m_iTestLosStackNumEntries];

		pTestPoly = stackEntry.pTestPoly;
		pLastPoly = stackEntry.pLastPoly;
		v = stackEntry.iVertex;
		lastv = stackEntry.iVertex-1;
		if(lastv < 0)
			lastv = pTestPoly->GetNumVertices()-1;
		
		pTestPolyNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(pTestPoly->GetNavMeshIndex());

		goto DROP_OUT_OF_RECURSION;
	}

	g_LosVars.m_iTestLosStackNumEntries = 0;
	return false;
}






//*********************************************************************************************************
//	Sometimes in the navmesh we will get small isolated areas of pavement.  This can happen when an expanse
//	of polygons are marked as pavement, but a small section of them gets cut off - for example by a wall
//	or fence.
//	This function goes through all the pavement polys in the world, and flood fills adjacent pavement polys
//	totaling up the area of pavement we have visited.  If enough pavement area is found to be connected,
//	then the poly can remain as pavement. If only a small area of connected pavement is found, then the
//	poly's pavement flag is removed.

static const float fMinConnectedAreaForPavement = 16.0f;	//64.0f;

void
CNavGen::RemoveIsolatedAreasOfPavement()
{
	int x,y;
	u32 p;

	printf("*******************************\n");
	printf("RemoveIsolatedAreasOfPavement()\n");
	printf("*******************************\n");
	
	SetAllPolyTimestamps(0);

	u16 iTimeStamp=1;

	for(y=0; y<CPathServerExtents::m_iNumNavMeshesInY; y++)
	{
		for(x=0; x<CPathServerExtents::m_iNumNavMeshesInX; x++)
		{
			const u32 iIndex = (y*CPathServerExtents::m_iNumNavMeshesInX)+x;
			CNavMesh * pNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(iIndex);
			if(pNavMesh)
			{
				printf("Mesh [%i,%i]\n", x*CPathServerExtents::m_iNumSectorsPerNavMesh, y*CPathServerExtents::m_iNumSectorsPerNavMesh);

				for(p=0; p<pNavMesh->m_iNumPolys; p++)
				{
					TNavMeshPoly & poly = *pNavMesh->GetPoly(p);
					if(poly.TestFlags(NAVMESHPOLY_IS_PAVEMENT))
					{
						float fAreaFound = pNavMesh->GetPolyArea(p);
						RemoveIsolatedAreasOfPavementR(pNavMesh, poly, iTimeStamp, fAreaFound);

						// If total area is beneath threshold, then unmark this as pavement
						if(fAreaFound < fMinConnectedAreaForPavement)
						{
							poly.AndFlags( ~NAVMESHPOLY_IS_PAVEMENT );
						}
						iTimeStamp++;

						if(iTimeStamp==0xFFFF)
						{
							SetAllPolyTimestamps(0);
							iTimeStamp=1;
						}
					}
				}
			}
		}
	}
}

void
CNavGen::RemoveIsolatedAreasOfPavementR(CNavMesh * pNavMesh, TNavMeshPoly & poly, u16 iTimeStamp, float & fAreaSoFar)
{
	if(fAreaSoFar > fMinConnectedAreaForPavement)
		return;

	u32 v;
	for(v=0; v<poly.GetNumVertices(); v++)
	{
		const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly( poly.GetFirstVertexIndex() + v );

		if(adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes()) != NAVMESH_NAVMESH_INDEX_NONE &&
			adjPoly.GetPolyIndex() != NAVMESH_POLY_INDEX_NONE &&
			adjPoly.GetAdjacencyType() == ADJACENCY_TYPE_NORMAL)
		{
			CNavMesh * pAdjNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes()));
			if(pAdjNavMesh)
			{
				TNavMeshPoly * pAdjPoly = pAdjNavMesh->GetPoly(adjPoly.GetPolyIndex());
				if(pAdjPoly->m_AStarTimeStamp != iTimeStamp)
				{
					pAdjPoly->m_AStarTimeStamp = iTimeStamp;
					if(pAdjPoly->TestFlags(NAVMESHPOLY_IS_PAVEMENT) || pAdjPoly->GetIsPavement())
					{
						float fArea = pAdjNavMesh->GetPolyArea(adjPoly.GetPolyIndex());
						fArea = Max(fArea, 0.25f);

						fAreaSoFar += fArea;

						RemoveIsolatedAreasOfPavementR(pAdjNavMesh, *pAdjPoly, iTimeStamp, fAreaSoFar);
					}
				}
			}
		}
	}
}


//*********************************************************************************************************
//	Same as RemoveIsolatedAreasOfPavment(), but for ped-density.
//	TODO: Make this a general-purpose flood-fill algorithm, with a per-poly callback

static const float fMinConnectedAreaForPedDensity = 64.0f;

void
CNavGen::RemoveIsolatedAreasOfPedDensity()
{
	printf("*********************************\n");
	printf("RemoveIsolatedAreasOfPedDensity()\n");
	printf("*********************************\n");

	int x,y;
	u32 p;

	SetAllPolyTimestamps(0);

	u16 iTimeStamp=1;

	for(y=0; y<CPathServerExtents::m_iNumNavMeshesInY; y++)
	{
		for(x=0; x<CPathServerExtents::m_iNumNavMeshesInX; x++)
		{
			const u32 iIndex = (y*CPathServerExtents::m_iNumNavMeshesInX)+x;
			CNavMesh * pNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(iIndex);
			if(pNavMesh)
			{
				printf("Mesh [%i,%i]\n", x*CPathServerExtents::m_iNumSectorsPerNavMesh, y*CPathServerExtents::m_iNumSectorsPerNavMesh);

				for(p=0; p<pNavMesh->m_iNumPolys; p++)
				{
					TNavMeshPoly & poly = *pNavMesh->GetPoly(p);
					if(poly.GetPedDensity() != 0)
					{
						float fAreaFound = pNavMesh->GetPolyArea(p);
						RemoveIsolatedAreasOfPedDensityR(pNavMesh, poly, iTimeStamp, fAreaFound);

						// If total area is beneath threshold, then unmark this as pavement
						if(fAreaFound < fMinConnectedAreaForPedDensity)
						{
							poly.SetPedDensity(0);
						}
						iTimeStamp++;

						if(iTimeStamp==0xFFFF)
						{
							SetAllPolyTimestamps(0);
							iTimeStamp=1;
						}
					}
				}
			}
		}
	}
}

void
CNavGen::RemoveIsolatedAreasOfPedDensityR(CNavMesh * pNavMesh, TNavMeshPoly & poly, u16 iTimeStamp, float & fAreaSoFar)
{
	if(fAreaSoFar > fMinConnectedAreaForPedDensity)
		return;

	u32 v;
	for(v=0; v<poly.GetNumVertices(); v++)
	{
		const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly( poly.GetFirstVertexIndex() + v );

		if(adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes()) != NAVMESH_NAVMESH_INDEX_NONE &&
			adjPoly.GetPolyIndex() != NAVMESH_POLY_INDEX_NONE &&
			adjPoly.GetAdjacencyType() == ADJACENCY_TYPE_NORMAL)
		{
			CNavMesh * pAdjNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes()));
			if(pAdjNavMesh)
			{
				TNavMeshPoly * pAdjPoly = pAdjNavMesh->GetPoly(adjPoly.GetPolyIndex());
				if(pAdjPoly->m_AStarTimeStamp != iTimeStamp)
				{
					pAdjPoly->m_AStarTimeStamp = iTimeStamp;
					if(pAdjPoly->GetPedDensity()!=0 || pAdjPoly->GetIsPavement())
					{
						float fArea = pAdjNavMesh->GetPolyArea(adjPoly.GetPolyIndex());
						fArea = Max(fArea, 0.25f);

						fAreaSoFar += fArea;

						RemoveIsolatedAreasOfPedDensityR(pAdjNavMesh, *pAdjPoly, iTimeStamp, fAreaSoFar);
					}
				}
			}
		}
	}
}

void
CNavGen::SetAllNavMeshFlags()
{
	int x,y;
	for(y=0; y<CPathServerExtents::m_iNumNavMeshesInY; y++)
	{
		for(x=0; x<CPathServerExtents::m_iNumNavMeshesInX; x++)
		{
			const u32 iIndex = (y*CPathServerExtents::m_iNumNavMeshesInX)+x;
			CNavMesh * pNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(iIndex);
			if(pNavMesh)
			{
				bool bHasWater=false;

				for(u32 p=0; p<pNavMesh->m_iNumPolys; p++)
				{
					TNavMeshPoly & poly = *pNavMesh->GetPoly(p);
					if(poly.GetIsWater())
					{
						bHasWater=true;
						break;
					}
				}

				if(bHasWater)
				{
					pNavMesh->m_iFlags |= NAVMESH_HAS_WATER;
				}
				else
				{
					pNavMesh->m_iFlags &= ~NAVMESH_HAS_WATER;
				}
			}
		}
	}
}

//******************************************************************************************

void CNavGen::CalculateWaterDepths(CNavMesh * pNavMesh, CNavGen * pNavGen)
{
	const float fShallowDepthThreshold = 0.5f;

	const float fRaiseAmt = 0.5f;
	const float fProbeDistance = 1.0f;

	Vector3 vPolyPts[NAVMESHPOLY_MAX_NUM_VERTICES+1];

	for(u32 p=0; p<pNavMesh->GetNumPolys(); p++)
	{
		TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);
		if(!pPoly->GetIsWater())
		{
			pPoly->SetIsShallow(false);
			continue;
		}

		float fLeastDepth = FLT_MAX;

		Vector3 vCentroid = VEC3_ZERO;
		u32 v;
		for(v=0; v<pPoly->GetNumVertices(); v++)
		{
			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly,v), vPolyPts[v] );
			vCentroid += vPolyPts[v];
		}
		vCentroid /= ((float)pPoly->GetNumVertices());
		vPolyPts[v] = vCentroid;

		for(v=0; v<pPoly->GetNumVertices()+1; v++)
		{
			Vector3 vProbeStart = vPolyPts[v] + Vector3(0.0f,0.0f,fRaiseAmt);
			Vector3 vProbeEnd = vPolyPts[v] - Vector3(0.0f,0.0f,fProbeDistance);

			u32 iRetFlags = 0;
			Vector3 vHitPos;
			float fHitDist;
			CNavGenTri * pHitTri;

			bool bHit = pNavGen->m_Octree.ProcessLineOfSight(
				vProbeStart,
				vProbeEnd,
				iRetFlags,
				NAVGEN_COLLISION_TYPES_EXCEPT_RIVER,
				vHitPos,
				fHitDist,
				pHitTri,
				true,		// vertical
				0,			// flags to test
				false,		// ignore see-thru
				false,		// ignore fixed
				true,		// ignore climbable objects
				false,		// ignore stairs
				true		// ignore riverbounds
			);

			if(bHit)
			{
				fHitDist = Max(fHitDist - fRaiseAmt, 0.0f);
				fLeastDepth = Min(fLeastDepth, fHitDist);
			}
		}

		if( fLeastDepth < fShallowDepthThreshold )
		{
			pPoly->SetIsShallow(true);
		}
		else
		{
			pPoly->SetIsShallow(false);
		}
	}
}

//******************************************************************************************
//	Marks polys which exist in small patches of navmesh, disconnected from the main surface

static const float fMinConnectedAreaForNonIsolated = 64.0f;

void CNavGen::MarkIsolatedAreasOfNavMesh()
{
	printf("****************************\n");
	printf("MarkIsolatedAreasOfNavMesh()\n");
	printf("****************************\n");

	int x,y;
	u32 p;

	SetAllPolyTimestamps(0);

	u16 iTimeStamp=1;

	for(y=0; y<CPathServerExtents::m_iNumNavMeshesInY; y++)
	{
		for(x=0; x<CPathServerExtents::m_iNumNavMeshesInX; x++)
		{
			const u32 iIndex = (y*CPathServerExtents::m_iNumNavMeshesInX)+x;
			CNavMesh * pNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(iIndex);
			if(pNavMesh)
			{
				printf("Mesh [%i,%i]\n", x*CPathServerExtents::m_iNumSectorsPerNavMesh, y*CPathServerExtents::m_iNumSectorsPerNavMesh);

				for(p=0; p<pNavMesh->m_iNumPolys; p++)
				{
					TNavMeshPoly & poly = *pNavMesh->GetPoly(p);

					Assert(poly.m_AStarTimeStamp != iTimeStamp);

					poly.m_AStarTimeStamp = iTimeStamp;
					float fAreaFound = pNavMesh->GetPolyArea(p);
					MarkIsolatedAreasOfNavMeshR(pNavMesh, poly, iTimeStamp, fAreaFound);

					// If total area is beneath threshold, then mark as isolated
					if(fAreaFound < fMinConnectedAreaForNonIsolated)
					{
						poly.SetIsIsolated(true);
						// we don't want peds trying to shelter in isolated areas
						poly.SetIsSheltered(false);
					}
					else
					{
						poly.SetIsIsolated(false);
					}
					iTimeStamp++;

					if(iTimeStamp==0xFFFF)
					{
						SetAllPolyTimestamps(0);
						iTimeStamp=1;
					}
				}
			}
		}
	}
}

void CNavGen::MarkIsolatedAreasOfNavMeshR(CNavMesh * pNavMesh, TNavMeshPoly & poly, u16 iTimeStamp, float & fAreaSoFar)
{
	if(fAreaSoFar > fMinConnectedAreaForNonIsolated)
		return;

	u32 v;
	for(v=0; v<poly.GetNumVertices(); v++)
	{
		const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly( poly.GetFirstVertexIndex() + v );

		if(adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes()) != NAVMESH_NAVMESH_INDEX_NONE &&
			adjPoly.GetPolyIndex() != NAVMESH_POLY_INDEX_NONE &&
			adjPoly.GetAdjacencyType() == ADJACENCY_TYPE_NORMAL)
		{
			CNavMesh * pAdjNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes()));
			if(pAdjNavMesh)
			{
				TNavMeshPoly * pAdjPoly = pAdjNavMesh->GetPoly(adjPoly.GetPolyIndex());
				if(pAdjPoly->m_AStarTimeStamp != iTimeStamp)
				{
					pAdjPoly->m_AStarTimeStamp = iTimeStamp;

					float fArea = pAdjNavMesh->GetPolyArea(adjPoly.GetPolyIndex());
					Assert(IsNumericallyValid(fArea));

					fAreaSoFar += fArea;

					MarkIsolatedAreasOfNavMeshR(pAdjNavMesh, *pAdjPoly, iTimeStamp, fAreaSoFar);
				}
			}
		}
	}
}

//******************************************************************************************************
// Flood fill navmesh from each poly, to determine whether each poly may be suitable for spawning peds
// during a network game.  The criteria is:
// a) there must be over some minimum area of connected navmesh
//
// NB: might need to rewrite this to recurse using a queue, as this might well blow out the stack.


static const float fMinConnectedAreaForNetworkSpawn = 100.0f*100.0f;

void CNavGen::MarkPotentialNetworkSpawnPolys(const char * pInputPath) //CDynamicEntityBoundsPerNavMesh & entityBoundsPerNavMesh)
{
	printf("********************************\n");
	printf("MarkPotentialNetworkSpawnPolys()\n");
	printf("********************************\n");

	const float fDotThreshold = Cosf(30.0f * DtoR);

	int x,y;
	u32 p;
	Vector3 vNormal;

	SetAllPolyTimestamps(0);

	u16 iTimeStamp=1;

	for(y=0; y<CPathServerExtents::m_iNumNavMeshesInY; y++)
	{
		for(x=0; x<CPathServerExtents::m_iNumNavMeshesInX; x++)
		{
			const u32 iIndex = (y*CPathServerExtents::m_iNumNavMeshesInX)+x;
			CNavMesh * pNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(iIndex);
			if(pNavMesh)
			{
				printf("Mesh [%i,%i]\n", x*CPathServerExtents::m_iNumSectorsPerNavMesh, y*CPathServerExtents::m_iNumSectorsPerNavMesh);

				for(p=0; p<pNavMesh->m_iNumPolys; p++)
				{
					TNavMeshPoly & poly = *pNavMesh->GetPoly(p);

					poly.SetIsNetworkSpawnCandidate(false);

					//-------------------------------------------------------------------------------------
					// Artists have the ability to prohibit network spawning on a per-collision poly basis
					// This information is retained in the navmesh construction and available at this point

					if(poly.m_NonResourced.m_bNoNetworkSpawn)
					{
						continue;
					}

					//----------------------
					// Don't spawn on roads
					// NB: remember that for now GetIsRoad() is a large overestimate of the road's surface
					// ie. any polygon whose bbox is touching a road collision triangle

					if(poly.GetIsRoad() && !poly.GetIsPavement())
					{
						continue;
					}

					//-----------------------
					// Ditto for train tracks

					if(poly.GetIsTrainTrack() && !poly.GetIsPavement())
					{
						continue;
					}

					//----------------------
					// Don't spawn on water

					if(poly.GetIsWater())
					{
						continue;
					}
					//--------------------------------------------------
					// And don't spawn on polys bordering water either

					u32 a;
					for(a=0; a<poly.GetNumVertices(); a++)
					{
						const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly(poly.GetFirstVertexIndex() + a);
						TNavMeshIndex iMesh = adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes());
						if(iMesh != NAVMESH_NAVMESH_INDEX_NONE)
						{
							u32 iPoly = adjPoly.GetPolyIndex();
							CNavMesh * pAdjNav = CNavGen_NavMeshStore::GetNavMeshFromIndex(iMesh);
							if(pAdjNav && pAdjNav->GetPoly(iPoly)->GetIsWater())
								break;
						}
					}
					if(a != poly.GetNumVertices())
					{
						continue;
					}

					// Don't spawn on polys which have been already identified as "too steep"
					if(poly.TestFlags(NAVMESHPOLY_TOO_STEEP_TO_WALK_ON))
					{
						continue;
					}

					// Don't spawn on polys which are orientated at over some angle from upright (40 degrees for now)
					if(pNavMesh->GetPolyNormal(vNormal, &poly))
					{
						if(DotProduct(vNormal, ZAXIS) < fDotThreshold)
							continue;
					}

					Assert(poly.m_AStarTimeStamp != iTimeStamp);

					poly.m_AStarTimeStamp = iTimeStamp;
					float fAreaFound = pNavMesh->GetPolyArea(p);
					MarkPotentialNetworkSpawnPolysR(pNavMesh, poly, iTimeStamp, fAreaFound);

					// If total area is beneath threshold, then poly is not a candidate for network spawning
					if(fAreaFound < fMinConnectedAreaForNetworkSpawn)
					{
						poly.SetIsNetworkSpawnCandidate(false);
					}
					// Otherwise we may potentially spawn peds here in the network game
					else
					{
						poly.SetIsNetworkSpawnCandidate(true);
					}
					iTimeStamp++;

					if(iTimeStamp==0xFFFF)
					{
						SetAllPolyTimestamps(0);
						iTimeStamp=1;
					}
				}
			}
		}
	}

	//--------------------------------------------------------------------------------
	// Now visit every poly which was marked as a potential spawn candidate, and
	// unmark those which are in intersection with dynamic entity locations.

	Vector3 vPolyVerts[NAVMESHPOLY_MAX_NUM_VERTICES];
	Vector4 vPolyEdgePlanes[NAVMESHPOLY_MAX_NUM_VERTICES];
	Vector3 vEdge;

	int iIndex = 0;
	for(y=0; y<CPathServerExtents::m_iNumNavMeshesInY; y++)
	{
		for(x=0; x<CPathServerExtents::m_iNumNavMeshesInX; x++)
		{
			CNavMesh * pNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(iIndex);
			if(pNavMesh)
			{
				vector<fwDynamicEntityInfo*> boundsList;

				char extrasFileName[1024];
				sprintf(extrasFileName, "%s/Extras_%i_%i.dat", pInputPath, x * CPathServerExtents::m_iNumSectorsPerNavMesh, y * CPathServerExtents::m_iNumSectorsPerNavMesh);

				CNavGen::ParseExtraInfoFile(extrasFileName, NULL, NULL, NULL, &boundsList);

				for(p=0; p<pNavMesh->m_iNumPolys; p++)
				{
					TNavMeshPoly & poly = *pNavMesh->GetPoly(p);
					if(poly.GetIsNetworkSpawnCandidate())
					{
						TShortMinMax polyMinMax = poly.m_MinMax;
						polyMinMax.Expand(0, 0, MINMAX_ONE);

						for(u32 o=0; o<boundsList.size(); o++)
						{
							fwDynamicEntityInfo * pEntityInfo = boundsList[o];

							// Approximate test
							if(polyMinMax.Intersects(pEntityInfo->m_MinMax))
							{
								// Construct edge planes
								// perform thorough test
								for(int v=0; v<(s32)poly.GetNumVertices(); v++)
								{
									pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(&poly, v), vPolyVerts[v]);
									pNavMesh->GetPolyEdgeNormal(vEdge, &poly, v);
									vEdge.z = 0.0f;
									vEdge.Normalize();
									vPolyEdgePlanes[v].SetVector3(vEdge);
									vPolyEdgePlanes[v].w = - vPolyEdgePlanes[v].Dot3(vPolyVerts[v]);
								}

								if( DynamicObjectIntersectsPolygon(pEntityInfo, poly.GetNumVertices(), vPolyVerts, vPolyEdgePlanes))
								{
									poly.SetIsNetworkSpawnCandidate(false);
									break;
								}
							}
						}
					}
				}

				// delete all the bounds we loaded for this navmesh
				for(u32 b=0; b<boundsList.size(); b++)
				{
					delete boundsList[b];
				}
			}

			iIndex++;
		}
	}
}

void CNavGen::MarkPotentialNetworkSpawnPolysR(CNavMesh * pNavMesh, TNavMeshPoly & poly, u16 iTimeStamp, float & fAreaSoFar)
{
	if(fAreaSoFar >= fMinConnectedAreaForNetworkSpawn)
		return;

	u32 v;
	for(v=0; v<poly.GetNumVertices(); v++)
	{
		const TAdjPoly adjPoly = pNavMesh->GetAdjacentPoly( poly.GetFirstVertexIndex()+v );

		if(adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes()) != NAVMESH_NAVMESH_INDEX_NONE &&
			adjPoly.GetPolyIndex() != NAVMESH_POLY_INDEX_NONE &&
			adjPoly.GetAdjacencyType() == ADJACENCY_TYPE_NORMAL)
		{
			CNavMesh * pAdjNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes()));
			if(pAdjNavMesh)
			{
				TNavMeshPoly * pAdjPoly = pAdjNavMesh->GetPoly(adjPoly.GetPolyIndex());

				if(pAdjPoly->GetIsWater())
					continue;
				
				// As soon as we find we're adjacent to a polygon which has already been marked as a candidate,
				// then we may quit - since we know we have at least that amount of free space available.
				if(pAdjPoly->GetIsNetworkSpawnCandidate())
				{
					fAreaSoFar += fMinConnectedAreaForNetworkSpawn;
					return;
				}

				if(pAdjPoly->m_AStarTimeStamp != iTimeStamp)
				{
					pAdjPoly->m_AStarTimeStamp = iTimeStamp;

					float fArea = pAdjNavMesh->GetPolyArea(adjPoly.GetPolyIndex());
					Assert(IsNumericallyValid(fArea));

					fAreaSoFar += fArea;

					MarkPotentialNetworkSpawnPolysR(pAdjNavMesh, *pAdjPoly, iTimeStamp, fAreaSoFar);
				}
			}
		}
	}
}



//*********************************************************************************************************
//	Link water polys to non-water polys along a shoreline

void
CNavGen::LinkShorePolys(CNavMesh * pNavMesh, CNavGen * pNavGen)
{
	u32 p;
	u32 lastv,v;

	// Don't do this if there is no water here
	if(!pNavGen->m_bHasWater)
		return;

	static const float fWaterLevel = pNavGen->m_fWaterLevel;
	static const float fMaxZDiff = 3.0f;
	static const float fMaxPlanarDist = 5.0f;

	//*************************************************************************************
	//	Firstly link water polys to nearby land polys, if very close & with a good LOS

	for(p=0; p<pNavMesh->m_iNumPolys; p++)
	{
		TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);
		if(!pPoly->GetIsWater())
			continue;

		Vector3 last_vertex, vertex;

		lastv =  pPoly->GetNumVertices()-1;

		for(v=0; v<pPoly->GetNumVertices(); v++)
		{
			const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly( pPoly->GetFirstVertexIndex()+lastv );

			// This must be an unconnected edge
			if(adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes()) != NAVMESH_NAVMESH_INDEX_NONE || adjPoly.GetPolyIndex() != NAVMESH_POLY_INDEX_NONE)
			{
				lastv = v;
				continue;
			}

			// get centre of edge
			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, lastv), last_vertex );
			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, v), vertex );
			Vector3 vWaterEdgeCentre = (last_vertex + vertex) * 0.5f;

			// Find closest non-water poly
			Vector3 vNotUsed;
			TNavMeshPoly * pClosestPoly = UtilGetClosestNavMeshPolyEdge(pNavMesh, vWaterEdgeCentre, 10.0f, vNotUsed, false, true);
			if(!pClosestPoly)
			{
				lastv = v;
				continue;
			}

			Assert(!pClosestPoly->GetIsWater());

			// Get centre of the closest non-water poly
			Vector3 vLandPolyCentre;
			pNavMesh->GetPolyCentroid(pNavMesh->GetPolyIndex(pClosestPoly), vLandPolyCentre);

			// Make sure z-difference isn't too great
			if(!Eq(vWaterEdgeCentre.z, vLandPolyCentre.z, fMaxZDiff))
			{
				lastv = v;
				continue;
			}

			//*************************************************************************************************************
			// Make sure the centre of this land poly is in front of the edge of the land poly we are considering
			
			Vector3 vEdgeNormal;
			pNavMesh->GetPolyEdgeNormal(vEdgeNormal, pPoly, lastv);
			float fDist = - DotProduct(vEdgeNormal, vertex);
			float fPlanarDist = DotProduct(vEdgeNormal, vLandPolyCentre) + fDist;

			if(fPlanarDist < 0.0f || fPlanarDist > fMaxPlanarDist)
			{
				lastv = v;
				continue;
			}





			// Do some line tests to see if the water poly connects to the land poly
			pNavGen->m_Octree.ResetBatchLOS();

			pNavGen->m_Octree.AddLosTestToBatch(vWaterEdgeCentre + Vector3(0.0f,0.0f,0.2f), vLandPolyCentre + Vector3(0.0f,0.0f,0.2f));
			pNavGen->m_Octree.AddLosTestToBatch(last_vertex + Vector3(0.0f,0.0f,0.2f), vLandPolyCentre + Vector3(0.0f,0.0f,0.2f));
			pNavGen->m_Octree.AddLosTestToBatch(vertex + Vector3(0.0f,0.0f,0.2f), vLandPolyCentre + Vector3(0.0f,0.0f,0.2f));

			int iNumLosHit=0;
			int iNumLosClear=0;

			pNavGen->m_Octree.StartBatchLosTest(iNumLosHit, iNumLosClear, NAVGEN_COLLISION_TYPES, CNavGenOctree::EQuitIfWeHitSomething, false);

			if(iNumLosHit != 0)
			{
				lastv = v;
				continue;
			}

			TAdjPoly * pDestAdj = pNavMesh->GetAdjacentPolysArray().Get(pPoly->GetFirstVertexIndex() + lastv);
			pDestAdj->m_iAdjacentNavMeshIndex = pNavMesh->m_iIndexOfMesh;
			pDestAdj->SetNavMeshIndex(pNavMesh->m_iIndexOfMesh, pNavMesh->GetAdjacentMeshes());
			pDestAdj->SetPolyIndex(pNavMesh->GetPolyIndex( pClosestPoly ));

			lastv = v;
		}
	}

	//***********************************************************************************************************
	//	Now link land polys whose edge is submerged, with nearby water polys

	for(p=0; p<pNavMesh->m_iNumPolys; p++)
	{
		TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);
		if(pPoly->GetIsWater())
			continue;

		Vector3 last_vertex, vertex;

		lastv =  pPoly->GetNumVertices()-1;

		for(v=0; v<pPoly->GetNumVertices(); v++)
		{
			const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly( pPoly->GetFirstVertexIndex() + lastv );

			// This must be an unconnected edge
			if(adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes()) != NAVMESH_NAVMESH_INDEX_NONE || adjPoly.GetPolyIndex() != NAVMESH_POLY_INDEX_NONE)
			{
				lastv = v;
				continue;
			}

			// Both edge verts must be underwater
			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, lastv), last_vertex );
			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, v), vertex );

			if(vertex.z > fWaterLevel || last_vertex.z > fWaterLevel)
			{
				lastv = v;
				continue;
			}

			Vector3 vLandPolyEdgeCentre = (last_vertex + vertex) * 0.5f;


			// Find closest water poly
			Vector3 vNotUsed;
			TNavMeshPoly * pClosestWaterPoly = UtilGetClosestNavMeshPolyEdge(pNavMesh, vLandPolyEdgeCentre, 10.0f, vNotUsed, true, false);
			if(!pClosestWaterPoly)
			{
				lastv = v;
				continue;
			}
			Assert(pClosestWaterPoly->GetIsWater());

			// Get centre of the closest water poly
			Vector3 vWaterPolyCentre;
			pNavMesh->GetPolyCentroid(pNavMesh->GetPolyIndex(pClosestWaterPoly), vWaterPolyCentre);


			// Make sure z-difference isn't too great
			if(!Eq(vLandPolyEdgeCentre.z, vWaterPolyCentre.z, fMaxZDiff))
			{
				lastv = v;
				continue;
			}

			//*************************************************************************************************************
			// Make sure the centre of this water poly is in front of the edge of the land poly we are considering

			Vector3 vEdgeNormal;
			pNavMesh->GetPolyEdgeNormal(vEdgeNormal, pPoly, lastv);
			float fDist = - DotProduct(vEdgeNormal, vertex);
			float fPlanarDist = DotProduct(vEdgeNormal, vWaterPolyCentre) + fDist;

			if(fPlanarDist < 0.0f || fPlanarDist > fMaxPlanarDist)
			{
				lastv = v;
				continue;
			}			





			// Do some line tests to see if the land poly connects to the water poly
			pNavGen->m_Octree.ResetBatchLOS();

			pNavGen->m_Octree.AddLosTestToBatch(vLandPolyEdgeCentre + Vector3(0.0f,0.0f,0.2f), vWaterPolyCentre + Vector3(0.0f,0.0f,0.2f));
			pNavGen->m_Octree.AddLosTestToBatch(last_vertex + Vector3(0.0f,0.0f,0.2f), vWaterPolyCentre + Vector3(0.0f,0.0f,0.2f));
			pNavGen->m_Octree.AddLosTestToBatch(vertex + Vector3(0.0f,0.0f,0.2f), vWaterPolyCentre + Vector3(0.0f,0.0f,0.2f));

			int iNumLosHit=0;
			int iNumLosClear=0;

			pNavGen->m_Octree.StartBatchLosTest(iNumLosHit, iNumLosClear, NAVGEN_COLLISION_TYPES, CNavGenOctree::EQuitIfWeHitSomething, false);

			if(iNumLosHit != 0)
			{
				lastv = v;
				continue;
			}

			TAdjPoly * pDestAdj = pNavMesh->GetAdjacentPolysArray().Get(pPoly->GetFirstVertexIndex() + lastv);
			pDestAdj->m_iAdjacentNavMeshIndex = pNavMesh->m_iIndexOfMesh;
			pDestAdj->SetNavMeshIndex(pNavMesh->m_iIndexOfMesh, pNavMesh->GetAdjacentMeshes());
			pDestAdj->SetPolyIndex(pNavMesh->GetPolyIndex( pClosestWaterPoly ));

			lastv = v;
		}
	}
}

#if NAVGEN_OBSOLETE

// Returns how many were found
int
CNavGen::GatherPolysFromNavMesh(
	CNavMesh * pNavMesh,
	TAdjPoly * polyMem,
	int iMaxToGather,
	Vector3 & vMin, Vector3 & vMax)
{
	g_iNumNavMeshPolysGathered = 0;
	g_iMaxNumNavMeshPolysToGather = iMaxToGather;
	g_vGatherNavMeshPolysOrigin = (vMin + vMax) * 0.5f;

	GatherPolysFromNavMeshR(pNavMesh->m_pQuadTree, polyMem, pNavMesh);

	return g_iNumNavMeshPolysGathered;
}

void
CNavGen::GatherPolysFromNavMeshR(CNavMeshQuadTree * pTree, TAdjPoly * pPolyMem, CNavMesh * pNavMesh)
{
	if(pTree->m_pLeafData)
	{
		for(int p=0; p<pTree->m_pLeafData->m_iNumPolys; p++)
		{
			u16 iPoly = pTree->m_pLeafData->m_Polys[p];
			TNavMeshPoly * pPoly = &pNavMesh->m_Polys[iPoly];

			if(pPoly->m_TimeStamp == m_iNavMeshPolyTimestamp)
				continue;

			pPoly->m_TimeStamp = m_iNavMeshPolyTimestamp;

			Vector3 vVec,vDiff;

			// If any of these vertices are within range of the origin, we'll store the poly
			for(u32 v=0; v<pPoly->GetNumVertices(); v++)
			{
				pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, v), vVec );
				vDiff = g_vGatherNavMeshPolysOrigin - vVec;
				if(vDiff.Mag2() <= g_fMaxDistAwayToAnalyseSqr)
				{
					if(g_iNumNavMeshPolysGathered < g_iMaxNumNavMeshPolysToGather)
					{
						pPolyMem[g_iNumNavMeshPolysGathered].SetNavMeshIndex(pNavMesh->m_iIndexOfMesh);
						pPolyMem[g_iNumNavMeshPolysGathered].SetPolyIndex(iPoly);
						g_iNumNavMeshPolysGathered++;
						break;
					}
					else
					{
						OutputDebugString("Reached max num polys in GatherPolysFromNavMesh.\n");
					}
				}
			}
		}

		return;
	}
	else
	{
		for(int c=0; c<4; c++)
		{
			GatherPolysFromNavMeshR(pTree->m_pChildren[c], pPolyMem, pNavMesh);
		}
	}
}

#endif	// NAVGEN_OBSOLETE

bool
CNavGen::IsMultipleNavGenVolumeClear(const CClipVolume & volume, int iNumNavGens, CNavGen ** pNavGens, bool bConsiderClimbableObjects, const u32 iCollisionTypes)
{
	for(int n=0; n<iNumNavGens; n++)
	{
		CNavGen * pNavGen = pNavGens[n];
		if(pNavGen)
		{
			bool bIsClear = pNavGen->m_Octree.TestVolumeIsClear(volume, iCollisionTypes, bConsiderClimbableObjects);
			if(!bIsClear)
				return false;
		}
	}
	return true;
}

void
CNavGen::InitGeomCuller(CCulledGeom & geometry, const Vector3 & vMins, const Vector3 & vMaxs, const int iNumNavGens, CNavGen ** pNavGens)
{
	geometry.Reset();
	geometry.Init(vMins, vMaxs);

	for(int n=0; n<iNumNavGens; n++)
	{
		CNavGen * pNavGen = pNavGens[n];
		if(pNavGen)
			pNavGen->m_Octree.AddTrianglesToCulledGeom(geometry);
	}
}

bool
CNavGen::TestMultipleNavGenLos(const Vector3 & vStart, const Vector3 & vEnd, int iNumNavGens, CNavGen ** pNavGens, const u32 iCollisionTypes, Vector3 * pHitPos, bool bVertical, bool bIgnoreSeeThrough, bool bIgnoreFixed)
{
	Vector3 vMins(Min(vStart.x, vEnd.x), Min(vStart.y, vEnd.y), Min(vStart.z, vEnd.z));
	Vector3 vMaxs(Max(vStart.x, vEnd.x), Max(vStart.y, vEnd.y), Max(vStart.z, vEnd.z));

	Vector3 vHitPos;
	float fHitDist;
	CNavGenTri * pHitTri;
	u32 iRetFlags = 0;

	for(int n=0; n<iNumNavGens; n++)
	{
		CNavGen * pNavGen = pNavGens[n];
		if(pNavGen)
		{
			Vector3 vNavMin = pNavGen->m_Octree.m_RootNodeMin;
			Vector3 vNavMax = pNavGen->m_Octree.m_RootNodeMax;

			if(vMins.x > vNavMax.x || vMins.y > vNavMax.y || vMins.z > vNavMax.z ||
				vNavMin.x > vMaxs.x || vNavMin.y > vMaxs.y || vNavMin.z > vMaxs.z)
			{

			}
			else
			{
				bool bHit = pNavGen->m_Octree.ProcessLineOfSight(vStart, vEnd, iRetFlags, iCollisionTypes, vHitPos, fHitDist, pHitTri, bVertical, 0, bIgnoreSeeThrough, bIgnoreFixed);
				if(bHit)
				{
					if(pHitPos)
						*pHitPos = vHitPos;
					return false;
				}
			}
		}
	}

	return true;
}


bool
CNavGen::TestMultipleNavMeshLos(const Vector3 & vStart, const Vector3 & vEnd, int iNumNavMeshes, CNavMesh ** pNavMeshes, CNavMesh *& pHitNavMesh, u16 & iHitPoly, Vector3 & vHitPos)
{
	Vector3 vMins(Min(vStart.x, vEnd.x), Min(vStart.y, vEnd.y), Min(vStart.z, vEnd.z));
	Vector3 vMaxs(Max(vStart.x, vEnd.x), Max(vStart.y, vEnd.y), Max(vStart.z, vEnd.z));

	float fIsectDist;

	for(int n=0; n<iNumNavMeshes; n++)
	{
		CNavMesh * pNavMesh = pNavMeshes[n];
		if(pNavMesh)
		{
			Vector3 vNavMin = pNavMesh->m_pQuadTree->m_Mins;
			Vector3 vNavMax = pNavMesh->m_pQuadTree->m_Maxs;

			if(vMins.x > vNavMax.x || vMins.y > vNavMax.y || vMins.z > vNavMax.z ||
				vNavMin.x > vMaxs.x || vNavMin.y > vMaxs.y || vNavMin.z > vMaxs.z)
			{

			}
			else
			{
				bool bHit = pNavMesh->IntersectRay(vStart, vEnd, vHitPos, fIsectDist, iHitPoly);

				if(bHit)
				{
					pHitNavMesh = pNavMesh;
					return false;
				}
			}
		}
	}

	return true;
}





bool
CNavGen::ProcessMultipleNavGenLos(const Vector3 & vStart, const Vector3 & vEnd, int iNumNavGens, CNavGen ** pNavGens, u32 & iOut_ReturnFlags, const u32 iCollisionTypes, Vector3 * pHitPos, float & fClosestHitDist, CNavGenTri *& pOutputHitTri, bool bVertical, bool bIgnoreSeeThrough, bool bIgnoreFixed)
{
	Vector3 vMins(Min(vStart.x, vEnd.x), Min(vStart.y, vEnd.y), Min(vStart.z, vEnd.z));
	Vector3 vMaxs(Max(vStart.x, vEnd.x), Max(vStart.y, vEnd.y), Max(vStart.z, vEnd.z));

	Vector3 vHitPos;
	fClosestHitDist = FLT_MAX;
	float fHitDist;
	CNavGenTri * pHitTri;
	u32 iRetFlags = 0;
	bool bClearLos = true;

	iOut_ReturnFlags = 0;

	for(int n=0; n<iNumNavGens; n++)
	{
		CNavGen * pNavGen = pNavGens[n];
		if(pNavGen)
		{
			Vector3 vNavMin = pNavGen->m_Octree.m_RootNodeMin;
			Vector3 vNavMax = pNavGen->m_Octree.m_RootNodeMax;

			if(vMins.x > vNavMax.x || vMins.y > vNavMax.y || vMins.z > vNavMax.z ||
				vNavMin.x > vMaxs.x || vNavMin.y > vMaxs.y || vNavMin.z > vMaxs.z)
			{

			}
			else
			{
				bool bHit = pNavGen->m_Octree.ProcessLineOfSight(vStart, vEnd, iRetFlags, iCollisionTypes, vHitPos, fHitDist, pHitTri, bVertical, 0, bIgnoreSeeThrough, bIgnoreFixed);
				if(bHit)
				{
					iOut_ReturnFlags |= iRetFlags;

					if(fHitDist < fClosestHitDist)
					{
						fClosestHitDist = fHitDist;

						if(pHitPos)
							*pHitPos = vHitPos;
						pOutputHitTri = pHitTri;

						bClearLos = false;
					}
				}
			}
		}
	}

	return bClearLos;
}

//********************************************************************************
//	Read the ".dat" extra info file associated with this navmesh, creating data
//	types for ladder links, etc

bool CNavGen::ParseExtraInfoFile(char * pFileName, vector<fwNavLadderInfo> * laddersList, vector<TCarNode*> * carNodesList, vector<spdSphere*> * portalBoundaries, vector<fwDynamicEntityInfo*> * dynamicEntityBounds)
{
	FILE * pFile = fopen(pFileName, "rt");
	if(!pFile)
		return false;

	int iNumLadders=0;
	int iNumCarNodes=0;
	int iNumPortals=0;
	int iNumDynamicEntities=0;

	fscanf(pFile, "NUM_LADDERS %i\n", &iNumLadders);
	while(iNumLadders > 0)
	{
		Vector3 vTop, vBase, vNormal;
		fscanf(pFile, "TOP %f %f %f BOTTOM %f %f %f NORMAL %f %f %f\n",
			&vTop.x, &vTop.y, &vTop.z,
			&vBase.x, &vBase.y, &vBase.z,
			&vNormal.x, &vNormal.y, &vNormal.z
		);

		if(laddersList)
		{
			fwNavLadderInfo ladderInfo;
			ladderInfo.SetTop(vTop);
			ladderInfo.SetBase(vBase);
			ladderInfo.SetNormal(vNormal);

			laddersList->push_back(ladderInfo);
		}

		iNumLadders--;
	}

	fscanf(pFile, "NUM_CARNODES %i\n", &iNumCarNodes);
	while(iNumCarNodes > 0)
	{
		Vector3 vPos;
		fscanf(pFile, "%f %f %f\n", &vPos.x, &vPos.y, &vPos.z);

		if(carNodesList)
		{
			TCarNode * carNode = rage_new TCarNode;
			carNode->vPos = vPos;
			carNodesList->push_back(carNode);
		}

		iNumCarNodes--;
	}

	fscanf(pFile, "NUM_PORTALS %i\n", &iNumPortals);
	while(iNumPortals > 0)
	{
		Vector4 vCenter;
		fscanf(pFile, "%f %f %f %f\n", &vCenter.x, &vCenter.y, &vCenter.z, &vCenter.w);

		if(portalBoundaries)
		{
			spdSphere * pSphere = rage_new spdSphere();
			pSphere->SetV4(VECTOR4_TO_VEC4V(vCenter));

			portalBoundaries->push_back(pSphere);
		}

		iNumPortals--;
	}

	char entityName[256];

	fscanf(pFile, "NUM_DYNAMIC_ENTITIES %i\n", &iNumDynamicEntities);
	while(iNumDynamicEntities > 0)
	{
		Matrix34 mEntity;
		Vector3 vMin, vMax;
		u32 iFlags;
		entityName[0] = 0;

		mEntity.Identity();

		fscanf(pFile, "%f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %u %s\n",
			&mEntity.a.x, &mEntity.a.y, &mEntity.a.z,
			&mEntity.b.x, &mEntity.b.y, &mEntity.b.z,
			&mEntity.c.x, &mEntity.c.y, &mEntity.c.z,
			&mEntity.d.x, &mEntity.d.y, &mEntity.d.z,
			&vMin.x, &vMin.y, &vMin.z, &vMax.x, &vMax.y, &vMax.z,
			&iFlags, entityName);

		Vector3 vBBSize = vMax - vMin;

		if(dynamicEntityBounds &&
			Abs(vBBSize.x) > SMALL_FLOAT &&
			Abs(vBBSize.y) > SMALL_FLOAT &&
			Abs(vBBSize.z) > SMALL_FLOAT)
		{
			fwDynamicEntityInfo * pEntityInfo = rage_new fwDynamicEntityInfo();
			mEntity.NormalizeSafe();
			pEntityInfo->m_Matrix = mEntity;
			pEntityInfo->m_OOBB[0] = vMin;
			pEntityInfo->m_OOBB[1] = vMax;
			pEntityInfo->m_iFlags = iFlags;

			ProjectBoundingBoxOntoZPlane(
				pEntityInfo->m_OOBB[0],
				pEntityInfo->m_OOBB[1],
				pEntityInfo->m_Matrix,
				0.0f,
				pEntityInfo->m_fTopZ,
				pEntityInfo->m_fBottomZ,
				pEntityInfo->m_vCorners,
				pEntityInfo->m_Planes,
				pEntityInfo->m_MinMax);

			dynamicEntityBounds->push_back(pEntityInfo);
		}

		iNumDynamicEntities--;
	}

	fclose(pFile);

	return true;
}

//********************************************************************************
//	Create any special navmesh links for this navmesh.  These are created for
//	things such as ladders, elevators, etc.  They are special connections between
//	navmesh polys, which are stored in a central list in each navmesh.
//	During a pathsearch we examine this central list iff that polygon has any
//	special adjacencies on it.
//	NOTE : For now this function only considers links within this single navmesh.
//	In future we may have other sorts of links (other than just vertical ladders!)
//	for which we will need to examine the adjacent navmeshes too.

void CNavGen::CreateSpecialNavMeshLinks(CNavMesh ** ppNavMeshes, CNavGen ** ppNavGens, int iNumNavmeshes, vector<fwNavLadderInfo> & ladderInfoList, vector<fwDynamicEntityInfo*> & dynamicEntityBounds, vector<CSpecialLinkInfo*> & allSpecialLinks)
{
	u32 i;

	//*******************************************************************
	//	Find all special links relating to ladders

	for(i=0; i<ladderInfoList.size(); i++)
	{
		fwNavLadderInfo & ladderInfo = ladderInfoList[i];
		AnalyseSpecialLink_Ladder(ppNavMeshes, ppNavGens, iNumNavmeshes, ladderInfo, allSpecialLinks);
	}

	//**************************************************************
	//	Find all special links for climbable objects

	for(i=0; i<dynamicEntityBounds.size(); i++)
	{
		fwDynamicEntityInfo * pObjectInfo = dynamicEntityBounds[i];

		// Only objects which have been marked as FLAG_CLIMABABLE_BY_AI
		if(!(pObjectInfo->m_iFlags & fwDynamicEntityInfo::FLAG_CLIMABABLE_BY_AI))
			continue;

		// Only work with objects which are Z-up for now..
		const float fDotUp = DotProduct( pObjectInfo->m_Matrix.c, ZAXIS );
		if(fDotUp < 0.707f)
			continue;

		AnalyseSpecialLink_ClimbableObject(ppNavMeshes, ppNavGens, iNumNavmeshes, pObjectInfo, allSpecialLinks);
	}
}



void
CNavGen::AnalyseSpecialLink_Ladder(CNavMesh ** ppNavMeshes, CNavGen ** ppNavGens, int iNumNavmeshes, fwNavLadderInfo & ladderInfo, vector<CSpecialLinkInfo*> & specialLinks)
{
	static const float f2PI = PI*2.0f;

	Vector3 vLadderTop = ladderInfo.GetTop();
	Vector3 vLadderBase = ladderInfo.GetBase();

	Vector3 vOffsetAtBase = ladderInfo.GetNormal() * 0.7f;	// Tweaking to match the anims better
	Vector3 vOffsetAtTop = ladderInfo.GetNormal() * -0.5f;

	vLadderTop += vOffsetAtTop;
	vLadderBase += vOffsetAtBase;

	TNavMeshPoly * pPolyAtTop = NULL;
	TNavMeshPoly * pPolyAtBase = NULL;

	CNavMesh * pNavmeshAtTop = NULL;
	CNavMesh * pNavmeshAtBase = NULL;
	int n;

	//------------------------------------------------------------------
	// Can we just find a navmesh polygon directly under this ladder?

	for(n=0; n<iNumNavmeshes; n++)
	{
		if(ppNavMeshes[n])
		{
			Vector3 vFloorBelowLadder;
			u32 iPolyUnderLadder = ppNavMeshes[n]->GetPolyBelowPoint(
				vLadderBase + Vector3(0.0f,0.0f,1.0f),
				vFloorBelowLadder,
				4.0f
			);
			// Did we find a poly directly under?
			if(iPolyUnderLadder != NAVMESH_POLY_INDEX_NONE)
			{
				pPolyAtBase = ppNavMeshes[n]->GetPoly(iPolyUnderLadder);
				pNavmeshAtBase = ppNavMeshes[n];
				break;
			}
		}
	}	

	//-------------------------------------------------------------------------------------------
	// If not, then we'll have to do some kind of search for the closest nearby navmesh polygon

	if(!pPolyAtBase)
	{
		const float fMaxScanDist = 8.0f;

		float fClosestDist = FLT_MAX;
		CNavMesh * pClosestNavmesh = NULL;
		TNavMeshPoly * pClosestPoly = NULL;

		for(n=0; n<iNumNavmeshes; n++)
		{
			if(ppNavMeshes[n])
			{
				Vector3 vClosePos;
				TNavMeshPoly * pPoly = ppNavMeshes[n]->GetClosestNavMeshPolyEdge(vLadderBase, fMaxScanDist, vClosePos);
				if(pPoly)
				{
					float fDist = (vClosePos - vLadderBase).Mag();
					if(fDist < fClosestDist && fDist < fMaxScanDist)
					{
						fClosestDist = fDist;
						pClosestPoly = pPoly;
						pClosestNavmesh = ppNavMeshes[n];
					}
				}
			}
		}
		if(pClosestPoly)
		{
			pPolyAtBase = pClosestPoly;
			pNavmeshAtBase = pClosestNavmesh;
		}
	}
	// If we have not found any suitable navmesh poly, then quit
	if(!pPolyAtBase)
		return;

	//---------------------------------------------------------------------------------------------
	// Can we find a navmesh polygon at the top of the ladder, by scanning forwards a little way?

	for(float s=0.5f; s<=1.5f; s+=0.25f)
	{
		Vector3 vScanPos = vLadderTop + (ZAXIS * 1.0f) - (ladderInfo.GetNormal() * s);
		for(n=0; n<iNumNavmeshes; n++)
		{
			if(ppNavMeshes[n])
			{
				static dev_float fScanDist = 4.0f;

				Vector3 vGroundAtTopOfLadder;
				u32 iPolyUnderLadder = ppNavMeshes[n]->GetPolyBelowPoint(
					vScanPos,
					vGroundAtTopOfLadder,
					fScanDist );

				// Did we find a poly directly under?
				if(iPolyUnderLadder != NAVMESH_POLY_INDEX_NONE)
				{
					// Oh yeh, lets do a collision test to ensure we've not found something on the floor below
					u32 iRetFlags = 0;
					Vector3 vHitPos;
					float fHitDist;
					CNavGenTri * pHitTri;
					bool bLos = ProcessMultipleNavGenLos(vScanPos, vScanPos - (ZAXIS * (fScanDist + 1.0f)), ANALYSE_NUM_NAVMESHES, ppNavGens, iRetFlags, NAVGEN_COLLISION_TYPES, &vHitPos, fHitDist, pHitTri, true, false, false);
					bool bTestPassed = bLos || IsClose(vHitPos.z, vGroundAtTopOfLadder.z, 1.0f);

					if(bTestPassed)
					{
						pPolyAtTop = ppNavMeshes[n]->GetPoly(iPolyUnderLadder);
						pNavmeshAtTop = ppNavMeshes[n];
						break;
					}
				}
			}
		}
		if(pPolyAtTop)
			break;
	}

	// Failing that, we look for a nearby polygon based on edge-distance
	// But use a position 1m below the ladder top, so that we're more likely to pick
	// up roof polygons in front of the ladder than to detect small polygons sitting
	// on a wall which the ladder is supposed to climb over.
	if(!pPolyAtTop)
	{
		const float fMaxScanDist = 8.0f;
		Vector3 vLadderTopScanPos = vLadderTop;
		vLadderTopScanPos.z -= 1.25f;

		float fClosestDist = FLT_MAX;
		CNavMesh * pClosestNavmesh = NULL;
		TNavMeshPoly * pClosestPoly = NULL;

		for(n=0; n<iNumNavmeshes; n++)
		{
			if(ppNavMeshes[n])
			{
				Vector3 vClosePos;
				TNavMeshPoly * pPoly = ppNavMeshes[n]->GetClosestNavMeshPolyEdge(vLadderTopScanPos, fMaxScanDist, vClosePos);
				if(pPoly)
				{
					float fDist = (vClosePos - vLadderTopScanPos).Mag();
					if(fDist < fClosestDist && fDist < fMaxScanDist)
					{
						fClosestDist = fDist;
						pClosestPoly = pPoly;
						pClosestNavmesh = ppNavMeshes[n];
					}
				}
			}
		}
		if(pClosestPoly)
		{
			pPolyAtTop = pClosestPoly;
			pNavmeshAtTop = pClosestNavmesh;
		}
	}

	// If we have not found any suitable navmesh poly, then quit
	if(!pPolyAtTop)
		return;

	//****************************************************************************************
	// Ensure that no special links already exist from/to this combo of navmeshes & polygons

	bool bUpLinkExists = false;
	bool bDownLinkExists = false;

	for(u32 s=0; s<specialLinks.size(); s++)
	{
		CSpecialLinkInfo * linkInfo = specialLinks[s];
		if(linkInfo->GetOriginalLinkFromNavMesh() == pNavmeshAtBase->m_iIndexOfMesh &&
			linkInfo->GetOriginalLinkToNavMesh() == pNavmeshAtTop->m_iIndexOfMesh &&
			 linkInfo->GetOriginalLinkFromPoly() == pNavmeshAtBase->GetPolyIndex(pPolyAtBase) &&
			  linkInfo->GetOriginalLinkToPoly() == pNavmeshAtTop->GetPolyIndex(pPolyAtTop))
		{
			bUpLinkExists = true;
		}
		if(linkInfo->GetOriginalLinkFromNavMesh() == pNavmeshAtTop->m_iIndexOfMesh &&
			linkInfo->GetOriginalLinkToNavMesh() == pNavmeshAtBase->m_iIndexOfMesh &&
			linkInfo->GetOriginalLinkFromPoly() == pNavmeshAtTop->GetPolyIndex(pPolyAtTop) &&
			linkInfo->GetOriginalLinkToPoly() == pNavmeshAtBase->GetPolyIndex(pPolyAtBase))
		{
			bDownLinkExists = true;
		}
	}


	//****************************************************************************************
	//	Create the links (both ways)

	if(!bUpLinkExists)
	{
		CSpecialLinkInfo * pLinkUpLadder = rage_new CSpecialLinkInfo;
		pLinkUpLadder->m_Struct1.m_iLinkFromNavMesh = (u16) pNavmeshAtBase->m_iIndexOfMesh;
		pLinkUpLadder->m_iOriginalLinkFromPoly = pLinkUpLadder->m_iLinkFromPoly = (u16) pNavmeshAtBase->GetPolyIndex(pPolyAtBase);
		CompressVertex(ladderInfo.GetBase()+vOffsetAtBase, pLinkUpLadder->m_iLinkFromPosX, pLinkUpLadder->m_iLinkFromPosY, pLinkUpLadder->m_iLinkFromPosZ, pNavmeshAtBase->m_pQuadTree->m_Mins, pNavmeshAtBase->m_vExtents);
		
		pLinkUpLadder->m_Struct1.m_iLinkToNavMesh = (u16) pNavmeshAtTop->m_iIndexOfMesh;
		pLinkUpLadder->m_iOriginalLinkToPoly = pLinkUpLadder->m_iLinkToPoly = (u16) pNavmeshAtTop->GetPolyIndex(pPolyAtTop);
		CompressVertex(ladderInfo.GetTop()+vOffsetAtTop, pLinkUpLadder->m_iLinkToPosX, pLinkUpLadder->m_iLinkToPosY, pLinkUpLadder->m_iLinkToPosZ, pNavmeshAtTop->m_pQuadTree->m_Mins, pNavmeshAtTop->m_vExtents);
		pLinkUpLadder->m_iLinkType = NAVMESH_LINKTYPE_CLIMB_LADDER;

		const float fBaseHeading = GetAngleBetweenPoints(-ladderInfo.GetNormal().x, -ladderInfo.GetNormal().y, 0.0f, 0.0f);
		pLinkUpLadder->m_iLinkFlags = (u8) ((fBaseHeading / f2PI) * 255.0f);

		specialLinks.push_back(pLinkUpLadder);
	}

	if(!bDownLinkExists)
	{
		CSpecialLinkInfo * pLinkDownLadder = rage_new CSpecialLinkInfo;
		pLinkDownLadder->m_Struct1.m_iLinkFromNavMesh = (u16) pNavmeshAtTop->m_iIndexOfMesh;
		pLinkDownLadder->m_iOriginalLinkFromPoly = pLinkDownLadder->m_iLinkFromPoly = (u16) pNavmeshAtTop->GetPolyIndex(pPolyAtTop);
		CompressVertex(ladderInfo.GetTop()+vOffsetAtTop, pLinkDownLadder->m_iLinkFromPosX, pLinkDownLadder->m_iLinkFromPosY, pLinkDownLadder->m_iLinkFromPosZ, pNavmeshAtTop->m_pQuadTree->m_Mins, pNavmeshAtTop->m_vExtents);

		pLinkDownLadder->m_Struct1.m_iLinkToNavMesh = (u16) pNavmeshAtBase->m_iIndexOfMesh;
		pLinkDownLadder->m_iOriginalLinkToPoly = pLinkDownLadder->m_iLinkToPoly = (u16) pNavmeshAtBase->GetPolyIndex(pPolyAtBase);
		CompressVertex(ladderInfo.GetBase()+vOffsetAtBase, pLinkDownLadder->m_iLinkToPosX, pLinkDownLadder->m_iLinkToPosY, pLinkDownLadder->m_iLinkToPosZ, pNavmeshAtBase->m_pQuadTree->m_Mins, pNavmeshAtBase->m_vExtents);
		pLinkDownLadder->m_iLinkType = NAVMESH_LINKTYPE_DESCEND_LADDER;

		const float fTopHeading = GetAngleBetweenPoints(ladderInfo.GetNormal().x, ladderInfo.GetNormal().y, 0.0f, 0.0f);
		pLinkDownLadder->m_iLinkFlags = (u8) ((fTopHeading / f2PI) * 255.0f);

		specialLinks.push_back(pLinkDownLadder);
	}

	
	
}


void CNavGen::AnalyseSpecialLink_ClimbableObject(CNavMesh ** ppNavMeshes, CNavGen ** ppNavGens, int /*iNumNavmeshes*/, fwDynamicEntityInfo * pObjectInfo, vector<CSpecialLinkInfo*> & specialLinks)
{
	static const float f2PI = PI*2.0f;

	const float fSizeX = pObjectInfo->m_OOBB[1].x - pObjectInfo->m_OOBB[0].x;
	const float fSizeY = pObjectInfo->m_OOBB[1].y - pObjectInfo->m_OOBB[0].y;
	Assert(fSizeX > 0.0f && fSizeY > 0.0f);

	Vector3 vObjMid = GetCenter(pObjectInfo->m_OOBB[0], pObjectInfo->m_OOBB[1], pObjectInfo->m_Matrix);

//	Vector3 vObjMid = (pObjectInfo->m_vCorners[0] + pObjectInfo->m_vCorners[1] + pObjectInfo->m_vCorners[2] + pObjectInfo->m_vCorners[3]) * 0.25f;
//	vObjMid.z = (pObjectInfo->m_fTopZ + pObjectInfo->m_fBottomZ) * 0.5f;

	const Vector3 & vPlaneNormal = (fSizeX > fSizeY) ? pObjectInfo->m_Matrix.b : pObjectInfo->m_Matrix.a;
	const Vector3 & vRight = (fSizeX > fSizeY) ? pObjectInfo->m_Matrix.a : pObjectInfo->m_Matrix.b;
	const float fDist = - DotProduct(vObjMid, vPlaneNormal);
	const Vector4 vPlane(vPlaneNormal.x, vPlaneNormal.y, vPlaneNormal.z, fDist);

	const float fObjectWidth = (fSizeX > fSizeY) ? fSizeY : fSizeX;

	for(s32 s=-1; s<=2; s+=2)
	{
		Vector3 vClimbDirection = vPlaneNormal * Sign((float)s);
		//vClimbDirection.z = 0.0f;
		vClimbDirection.Normalize();
		
		Vector3 vPosThisSide = vObjMid - (vClimbDirection * (fObjectWidth*0.5f));
		vPosThisSide -= vClimbDirection * (fwNavGenConfig::Get().m_PedRadius * 1.5f);

		Vector3 vPosOtherSide = vObjMid + (vClimbDirection * (fObjectWidth*0.5f));
		vPosOtherSide += vClimbDirection * (fwNavGenConfig::Get().m_PedRadius * 1.5f);

		const float fScanStartDist = (vPosOtherSide - vPosThisSide).Mag();

		Vector3 vClimbRight = vRight * Sign((float)s);

		CNavMesh * pNavMesh = ppNavMeshes[0];
		Vector3 vNavGroundPos;
		u32 iPoly = pNavMesh->GetPolyBelowPoint(vPosThisSide, vNavGroundPos, 3.0f);

		if( iPoly != NAVMESH_POLY_INDEX_NONE )
		{
			TNavMeshPoly * pPoly = pNavMesh->GetPoly(iPoly);
			TAdjPoly adjPoly;
			Vector3 vLinkToPos;

			bool bClimbFound = ScanForAdjacentNavMeshPoly(
				pNavMesh,
				pPoly,
				adjPoly,
				vPosThisSide,
				vClimbDirection,
				vClimbRight,		/* unused in scanning function */
				ppNavGens,
				ppNavMeshes,
				vLinkToPos,
				true,
				false,
				fScanStartDist );

			if(bClimbFound)
			{
				Vector3 vLinkFromPos = vNavGroundPos;

				const float fFromDist = vPlane.Dot3(vLinkFromPos) + vPlane.w;
				const float fToDist = vPlane.Dot3(vLinkToPos) + vPlane.w;
				const float fMaxPlanarDist = (fObjectWidth * 0.5f) + (fwNavGenConfig::Get().m_PedRadius * 2.0f) + 0.5f;

				// Ensure that start & end pos are reasonably close to the climb center
				if( Abs(fFromDist) < fMaxPlanarDist && Abs(fToDist) < fMaxPlanarDist )
				{
					// Ensure that from/to points are on either side of the plane
					if( Sign(fFromDist) != Sign(fToDist) )
					{
						Vector3 vNavMeshMins = pNavMesh->GetQuadTree()->m_Mins;
						Vector3 vNavMeshMaxs = pNavMesh->GetQuadTree()->m_Mins + pNavMesh->GetExtents();

						// For now, I am only going to allow object climbs which start & end within the same navmesh.
						// The reason is that we cannot store a compressed vertex position outside of its owning mesh extents.
						// It'll be easy enough to workaround this, but for now - which still developing the object climbing
						// links - it seems a fair enough restriction to impose.

						if( vLinkToPos.IsGreaterThan(vNavMeshMins) && vNavMeshMaxs.IsGreaterThan(vLinkToPos) )
						{
							//Vector3 vNavGroundPosTarget;
							//u32 iToPoly = pNavMesh->GetPolyBelowPoint(vLinkToPos, vNavGroundPosTarget, 3.0f);
							//if(iToPoly != NAVMESH_POLY_INDEX_NONE)
							{
								//vLinkToPos = vNavGroundPosTarget;
								//adjPoly.SetPolyIndex(iToPoly);
								//adjPoly.SetOriginalPolyIndex(iToPoly);

								bool bLinkExists = false;
								for(u32 s=0; s<specialLinks.size(); s++)
								{
									CSpecialLinkInfo * linkInfo = specialLinks[s];
									if(linkInfo->GetOriginalLinkFromNavMesh() == pNavMesh->GetIndexOfMesh() &&
										linkInfo->GetOriginalLinkToNavMesh() == adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes()) &&
										linkInfo->GetOriginalLinkFromPoly() == iPoly &&
										linkInfo->GetOriginalLinkToPoly() == adjPoly.GetPolyIndex())
									{
										bLinkExists = true;
									}
								}

								if(!bLinkExists)
								{
									CSpecialLinkInfo * pLink = new CSpecialLinkInfo();
									pLink->m_Struct1.m_iLinkFromNavMesh = (u16)pNavMesh->GetIndexOfMesh();
									pLink->m_iOriginalLinkFromPoly = pLink->m_iLinkFromPoly = (u16)iPoly;
									CompressVertex(vLinkFromPos, pLink->m_iLinkFromPosX, pLink->m_iLinkFromPosY, pLink->m_iLinkFromPosZ, vNavMeshMins, pNavMesh->GetExtents() );

									pLink->m_Struct1.m_iLinkToNavMesh = (u16)adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes());
									pLink->m_iOriginalLinkToPoly = pLink->m_iLinkToPoly = (u16)adjPoly.GetPolyIndex();
									CompressVertex(vLinkToPos, pLink->m_iLinkToPosX, pLink->m_iLinkToPosY, pLink->m_iLinkToPosZ, vNavMeshMins, pNavMesh->GetExtents() );

									const float fHeading = GetAngleBetweenPoints(vClimbDirection.x, vClimbDirection.y, 0.0f, 0.0f);
									pLink->m_iLinkFlags = (u8) ((fHeading / f2PI) * 255.0f);
									pLink->m_iLinkType = NAVMESH_LINKTYPE_CLIMB_OBJECT;

									specialLinks.push_back(pLink);
								}
							}
						}
					}
				}
			}
		}
	}
}

void CNavGen::MarkCarNodes(CNavMesh * pNavMesh, vector<TCarNode*> & carNodes)
{
	if(!pNavMesh || !carNodes.size())
		return;

	for(u32 p=0; p<pNavMesh->m_iNumPolys; p++)
	{
		pNavMesh->GetPoly(p)->SetIsNearCarNode(false);
	}

	for(u32 n=0; n<carNodes.size(); n++)
	{
		const TCarNode * carNode = carNodes[n];
		Vector3 vIntersectPos;

		Vector3 vPointAbove = carNode->vPos;
		vPointAbove.z += 1.0f;

		u32 iPolyIndex = pNavMesh->GetPolyBelowPoint(vPointAbove, vIntersectPos, 4.0f);
		if(iPolyIndex != NAVMESH_POLY_INDEX_NONE)
		{
			pNavMesh->GetPoly(iPolyIndex)->SetIsNearCarNode(true);
		}
	}
}

// Removes all ped-density from navmesh polys which intersect portal boundaries
void CNavGen::ZeroPedDensityAtPortalBoundaries(CNavMesh * pNavMesh, vector<spdSphere*> & portalBoundaries)
{
	for(u32 s=0; s<portalBoundaries.size(); s++)
	{
		spdSphere * pSphere = portalBoundaries[s];
		TShortMinMax minMax;
		minMax.SetFloat(
			pSphere->GetCenter().GetXf() - pSphere->GetRadiusf(),
			pSphere->GetCenter().GetYf() - pSphere->GetRadiusf(),
			pSphere->GetCenter().GetZf() - pSphere->GetRadiusf(),
			pSphere->GetCenter().GetXf() + pSphere->GetRadiusf(),
			pSphere->GetCenter().GetYf() + pSphere->GetRadiusf(),
			pSphere->GetCenter().GetZf() + pSphere->GetRadiusf());

		for(u32 p=0; p<pNavMesh->m_iNumPolys; p++)
		{
			TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);
			if(pPoly->GetPedDensity() > 0 && pPoly->m_MinMax.Intersects(minMax))
			{
				pPoly->SetPedDensity(0);
			}
		}
	}
}

void
CNavGen::MarkShelteredPolys(CNavMesh * pNavMesh, CNavGen * pNavGen)
{
	if(!pNavMesh)
		return;

	for(u32 p=0; p<pNavMesh->m_iNumPolys; p++)
	{
		pNavMesh->GetPoly(p)->SetIsSheltered(false);
	}

	if(!pNavGen)
		return;

	Vector3 vPolyVerts[NAVMESHPOLY_MAX_NUM_VERTICES];

	u32 v;
	for(u32 p=0; p<pNavMesh->m_iNumPolys; p++)
	{
		TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);
		if(pPoly->GetIsWater() || pPoly->GetIsInterior())
			continue;

		for(v=0; v<pPoly->GetNumVertices(); v++)
		{
			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, v), vPolyVerts[v] );
		}

		for(v=0; v<pPoly->GetNumVertices(); v++)
		{
			Vector3 vPosAboveVert = vPolyVerts[v];
			vPosAboveVert.z += 1.0f;
			Vector3 vPosBelowVert = vPolyVerts[v];
			vPosBelowVert.z -= 1.0f;
			Vector3 vPosFarAboveVert = vPolyVerts[v];
			vPosFarAboveVert.z += 20.0f;

			Vector3 vHitPos;
			float fHitDist;
			CNavGenTri * pHitTri = NULL;
			u32 iRetFlags = 0;

			bool bHit = pNavGen->m_Octree.ProcessLineOfSight(vPosAboveVert, vPosBelowVert, iRetFlags, NAVGEN_COLLISION_TYPES, vHitPos, fHitDist, pHitTri, true);

			// Has to be a surface underneath
			if(!bHit || !pHitTri)
				break;

			// Surface mustn't be road
			// JB: Turned this off for Jorge, so that he can use the 'sheltered' flag to for audio purposes.
			// This means that in-game AI code which looks for shelterd polygons, should now have to manually
			// test that these polygons aren't flagged as road.
//			if(pHitTri->m_colPolyData.GetIsRoad())
//				break;

			// Has to be shelter above
			bool bHitFarAbove = pNavGen->m_Octree.ProcessLineOfSight(vPosAboveVert, vPosFarAboveVert, iRetFlags, NAVGEN_COLLISION_TYPES, vHitPos, fHitDist, pHitTri, true);
			if(!bHitFarAbove)
				break;
		}
		// All vertices passed the test, so lets assume that this poly is sheltered
		if(v == pPoly->GetNumVertices())
		{
			pPoly->SetIsSheltered(true);
		}
	}
}


}	// namespace rage
