#ifndef NAVGENOCTREE_H
#define NAVGENOCTREE_H

//***************************************************************************************************
//	Title	: NavGen_Optimise.cpp
//	Author	: James Broad
//	Date	: Jan 2005
//
//	The following classes implement an octree for storing the world's collision triangles.
//
//***************************************************************************************************

// Rage includes
#include "system\xtl.h"	// Solves masses of compile errors (warning C4668). Read the RAGE 0.4 "readme.txt" for why this is needed.
#include "atl\array.h"
#include "atl\dlist.h"

// GTA includes
#include "fwMaths\vector.h"
#include "NavGen_Types.h"

#include "ai\navmesh\datatypes.h"

//***************************************************************************************************
//	Some #defines
//***************************************************************************************************
//
// NB : Comment this line in, in order to collect fragments at octree leaves so we can
// examine how correctly the octree has split polys as they descend to the leaves
//#define DEBUG_OCTREE_CONSTRUCTION
//
//
// NB : Comment this out to remove the D3D vertex/index buffer from each octree leaf,
// which is used to visualise the generated navigation surface
#define VISUALISE_NAVIGATION_SURFACE
// NB : Use this define to use a Rage phBoundOctree internally for all queries.
//#define USE_RAGE_BOUNDS
//
//
//***************************************************************************************************

#ifdef VISUALISE_NAVIGATION_SURFACE
#include <d3d9.h>
#endif


namespace rage
{

class CSplitPoly;

class CNavGenNode;
class CNavSurfaceTri;
class CNavTriEdge;
class CPlacedNodeMultiMap;


class CNavGenTri;


// Marks that this node is a leaf-node
#define NAVGEN_OCTREE_FLAG_ISLEAF					0x01
#define NAVGEN_OCTREE_FLAG_NAVSURFACEHASCHANGED		0x02

#define LOS_INTERSECTIONS_MAXNUM					4

struct TLosIsect
{
	Vector3 m_vPos;
	float m_fDist;
	CNavGenTri * m_pTri;

	inline TLosIsect() { }
	inline TLosIsect(const Vector3 & vPos, const float fDist, CNavGenTri * pTri) { m_vPos = vPos; m_fDist = fDist; m_pTri = pTri; }
};

class CLosIntersections
{
public:
	CLosIntersections()
	{
		m_Intersections.Reserve(4);
		Reset();
	}
	~CLosIntersections() { }

	inline void Reset() { m_Intersections.Reset(); }
	void Add(const Vector3 & vPos, const float fDist, CNavGenTri * pTri);

	inline int GetNum() { return m_Intersections.GetCount(); }
	inline const Vector3 & GetPos(int i) { Assert(i<GetNum()); return m_Intersections[i].m_vPos; }
	inline float GetDist(int i) { Assert(i<GetNum()); return m_Intersections[i].m_fDist; }
	inline CNavGenTri * GetTri(int i) { Assert(i<GetNum()); return m_Intersections[i].m_pTri; }

private:

	atArray<TLosIsect> m_Intersections;
};

#define CLIPVOLUME_MAXPLANES	6

class CClipVolume
{
public:
	inline CClipVolume() { m_iNumPlanes = 0; }

	// Initialise from an extruded triangle
	bool InitExtrudedTri(const Vector3 * pPts, const float fExtrudeDist, const Vector3 * pTriNormal);
	// Initialise from an extruded quad
	void InitExtrudedQuad(const Vector3 * pPts, const float fExtrudeDist, const Vector3 * pQuadNormal);

	// Test whether the given triangle intersects the volume at all
	bool TestTriIntersection(const Vector3 * pTriPts, Vector3 * pClosestIntersection = NULL, float * fClosestIntersectionDist = NULL) const;

	inline const TShortMinMax & GetMinMax() const { return m_MinMax; }
	inline const Vector3 & GetMin() const { return m_vMin; }
	inline const Vector3 & GetMax() const { return m_vMax; }

private:
	Vector3 m_Planes[CLIPVOLUME_MAXPLANES];
	float m_fPlaneDists[CLIPVOLUME_MAXPLANES];
	int m_iNumPlanes;
	Vector3 m_vMin, m_vMax;
	TShortMinMax m_MinMax;
};

class CProbeTester
{
public:
	CProbeTester() { }
	virtual ~CProbeTester() { }

	// Tests a line of sight.  Returns true if LOS is clear, or false if there was an intersection
	virtual bool TestLineOfSight(const Vector3 & vStart, const Vector3 & vEnd, const u32 iCollisionTypeFlags, bool bVertical = false, u32 iFlagsToTest = 0, bool bIgnoreSeeThrough = false, bool bIgnoreFixed = false, bool bIgnoreClimbableObjects = true, bool bIgnoreStairs = false, bool bIgnoreRiverBound = false) = 0;

	enum ReturnFlags
	{
		RetFlag_HitNonClimbableCollision	=	0x1
	};

	// As above, but returns data about the intersection
	virtual bool ProcessLineOfSight(
		const Vector3 & vStart,
		const Vector3 & vEnd,
		u32 & iOut_ReturnFlags,
		const u32 iCollisionTypeFlags,
		Vector3 & vHitPoint,
		float & fHitDist,
		CNavGenTri *& hitTriangle,
		bool bVertical = false,
		u32 iFlagsToTest = 0,
		bool bIgnoreSeeThrough = false,
		bool bIgnoreFixed = false,
		bool bIgnoreClimableObjects = true,
		bool bIgnoreStairs = false,
		bool bIngoreRiverBound = false) = 0;

	// performs 5 jittered vertical linetests & returns the one with the highest Z
	virtual bool ProcessLineOfSightVerticalJittered(
		const Vector3 & vStart,
		const Vector3 & vEnd,
		const float fJitterSize,
		const u32 iCollisionTypeFlags,
		Vector3 & vHitPoint,
		float & fHitDist,
		CNavGenTri *& hitTriangle,
		u32 iFlagsToTest = 0,
		bool bIgnoreSeeThrough = false,
		bool bIgnoreFixed = false,
		bool bIgnoreClimableObjects = true,
		bool bIgnoreRiverBound = false) = 0;

	virtual bool TestVolumeIsClear(const CClipVolume & volume, const u32 iCollisionTypeFlags, bool bIgnoreClimbableObjects = true) = 0;

	static const Vector3 m_vJitterOffsets[5];
};

class CCulledGeom : public CProbeTester
{
public:
	CCulledGeom() {  }
	virtual ~CCulledGeom() { }

	void Init(const Vector3 & vMin, const Vector3 & vMax);
	void Reset();

	// Tests a line of sight.  Returns true if LOS is clear, or false if there was an intersection
	virtual bool TestLineOfSight(const Vector3 & vStart, const Vector3 & vEnd, const u32 iCollisionTypeFlags, bool bVertical = false, u32 iFlagsToTest = 0, bool bIgnoreSeeThrough = false, bool bIgnoreFixed = false, bool bIgnoreClimableObjects = true, bool bIgnoreStairs = false, bool bIgnoreRiverBound = false);

	// As above, but returns data about the intersection
	virtual bool ProcessLineOfSight(
		const Vector3 & vStart,
		const Vector3 & vEnd,
		u32 & iOut_ReturnFlags,
		const u32 iCollisionTypeFlags,
		Vector3 & vHitPoint,
		float & fHitDist,
		CNavGenTri *& hitTriangle,
		bool bVertical = false,
		u32 iFlagsToTest = 0,
		bool bIgnoreSeeThrough = false,
		bool bIgnoreFixed = false,
		bool bIgnoreClimableObjects = true,
		bool bIgnoreStairs = false,
		bool bIngoreRiverBound = false);

	// performs 5 jittered vertical linetests & returns the one with the highest Z
	virtual bool ProcessLineOfSightVerticalJittered(
		const Vector3 & vStart,
		const Vector3 & vEnd,
		const float fJitterSize,
		const u32 iCollisionTypeFlags,
		Vector3 & vHitPoint,
		float & fHitDist,
		CNavGenTri *& hitTriangle,
		u32 iFlagsToTest = 0,
		bool bIgnoreSeeThrough = false,
		bool bIgnoreFixed = false,
		bool bIgnoreClimableObjects = true,
		bool bIgnoreRiverBound = false);

	virtual bool TestVolumeIsClear(const CClipVolume & volume, const u32 iCollisionTypeFlags, bool bIgnoreClimbableObjects = true);

	Vector3 m_vMin;
	Vector3 m_vMax;
	TShortMinMax m_MinMax;

	vector<CNavGenTri*> m_Triangles;
};

//***************************************************************************************************
//	CNavGenOctreeNodeBase
//	Base class from which to derive octree nodes & leaves
//***************************************************************************************************

class CNavGenOctreeNodeBase
{
	friend class CNavGen;
public:
	CNavGenOctreeNodeBase(void);
	~CNavGenOctreeNodeBase(void);

	inline bool GetIsLeafNode() const { return (m_Flags&NAVGEN_OCTREE_FLAG_ISLEAF)!=0; }

	u32 m_Flags;
};

//***************************************************************************************************
//	CNavGenOctreeNode
//	Octree node
//***************************************************************************************************

class CNavGenOctreeNode : public CNavGenOctreeNodeBase
{
public:
	CNavGenOctreeNode(void);
	~CNavGenOctreeNode(void);

	CNavGenOctreeNodeBase * m_ChildNodes[8];
};

//***************************************************************************************************
//	CNavGenOctreeLeaf
//	Octree leaf
//***************************************************************************************************

class CNavGenOctreeLeaf : public CNavGenOctreeNodeBase
{
public:
	CNavGenOctreeLeaf(void);
	~CNavGenOctreeLeaf(void);

	Vector3 m_NodeMin;
	Vector3 m_NodeMax;

	// The list of collision triangles in the leaf.  Note these are the original collision triangles
	// and NOT the polygons which have ended up being split into the octree during the octree creation
	vector<CNavGenTri*> m_Triangles;

	// For debugging the octree this member is useful.  It contains a list of all the triangle fragments
	// which have ended up in this leaf after being split into the octree.
#ifdef DEBUG_OCTREE_CONSTRUCTION
	vector<CSplitPoly*> * m_pFragments;
#endif

	// These two members are used to draw the navigation surface in each octree leaf.
	// The navigation surface may have an extremely high vertex/triangle count - so
	// am taking a static vertex buffer approach for visualising it, I don't want to be
	// constructing this vertex/index buffer on the fly if at all possible..
#ifdef VISUALISE_NAVIGATION_SURFACE
	LPDIRECT3DVERTEXBUFFER9 m_pNavSurfaceVertexBuffer;
	int m_iNumTrianglesInVB;
#endif
};

//***************************************************************************************************
//	CSplitPoly
//	This is a temporary class used when creating the octree.  Once a poly fragment ends up in a leaf
//	node, the original 'CNavGenTri' is used in it's place.  This is because whilst we do need to split
//	polys in order to make a good octree, we only want the original triangles for processing.
//***************************************************************************************************

#define PLANESIDE_POSX		0x01
#define PLANESIDE_NEGX		0x02
#define PLANESIDE_POSY		0x04
#define PLANESIDE_NEGY		0x08
#define PLANESIDE_POSZ		0x10
#define PLANESIDE_NEGZ		0x20


inline float GetPointDistFromAxisAlignedPlane(const Vector3 & vPt, const u32 iPlaneAlignmentFlag, const float & fPlaneDist)
{
	switch(iPlaneAlignmentFlag)
	{
		case PLANESIDE_POSX:
			return vPt.x - fPlaneDist;
		case PLANESIDE_NEGX:
			return vPt.x + fPlaneDist;
		case PLANESIDE_POSY:
			return vPt.y - fPlaneDist;
		case PLANESIDE_NEGY:
			return vPt.y + fPlaneDist;
		case PLANESIDE_POSZ:
			return vPt.z - fPlaneDist;
		case PLANESIDE_NEGZ:
			return vPt.z + fPlaneDist;
		default:
		{
			Assert(0);
			return 0.0f;
		}
	}
}

class CSplitPoly
{
public:
	CSplitPoly(void)
	{
		m_pParentTriangle = NULL;
		m_PlaneSideFlags = 0;
		m_bNeedsDeleted = false;
		m_Pts.Reset();
	}
	~CSplitPoly(void)
	{
		m_Pts.Reset();
	}

	atArray<Vector3> m_Pts;
	u32 m_PlaneSideFlags;
	u16 m_bNeedsDeleted;
	TColPolyData m_colPolyData;
	u8 m_iBakedInDataBitField;
	CNavGenTri * m_pParentTriangle;

	inline float CalcArea(void)
	{
		float a=0;
		Vector3 e1,e2,e3;
		s32 v1,v2;

		v1 = 1;
		for(v2=2; v2<m_Pts.GetCount(); v2++)
		{
			e1 = m_Pts[v1] - m_Pts[0];
			e2 = m_Pts[v2] - m_Pts[0];
			e3 = m_Pts[v2] - m_Pts[v1];
			a += CalcTriArea(e1.Mag(), e2.Mag(), e3.Mag());
			v1=v2;
		}

		return a;
	}
};

#define NAVGEN_MINMAX_EXPAND_AMT	MINMAX_ONE

//***************************************************************************************************
//	CNavGenOctree
//	Octree main data structure.  We'll create the octree by splitting the triangles against the
//	bounding planes of each node, creating a leaf when either the triangle count drops below a lower
//	limit or when either dimension of the node becomes smaller than some value.
//	Although we split triangles during the octree construction, we always store the original unsplit
//	triangles within each leaf.  We use a timestamp on each poly so that it never gets processed
//	twice in our intersection querying functions.
//***************************************************************************************************

enum OCTANT_ENUM
{
	POSX_POSY_POSZ		= 0,
	POSX_NEGY_POSZ		= 1,
	NEGX_NEGY_POSZ		= 2,
	NEGX_POSY_POSZ		= 3,
	POSX_POSY_NEGZ		= 4,
	POSX_NEGY_NEGZ		= 5,
	NEGX_NEGY_NEGZ		= 6,
	NEGX_POSY_NEGZ		= 7
};

// Helper function which will return one of the above enumeration values, from a combination of planeside flags
s32 OctantEnumFromPlaneSideFlags(u32 flags);


#define MAX_NUM_BATCHED_LOS_TESTS	64
#define MAX_BATCH_ISECTS			16

class CNavGenOctree : public CProbeTester
{
	friend class CNavGen;
public:

	enum EProcessOrTestLos
	{
		ETestLineOfSight,
		EProcessLineOfSight
	};
	enum EBatchTestLosMode
	{
		EContinueUntilEnd,
		EQuitIfWeHitSomething
	};


	CNavGenOctree(void);
	~CNavGenOctree(void);

	// Creates the octree from the input list of triangles
	bool Init(const Vector3 & vMeshMin, const Vector3 & vMeshMax, TColTriArray * pTriangles);

	// Deletes the octree
	bool Shutdown(void);
	void ShutdownR(CNavGenOctreeNodeBase * pNodeBase);

	// Saves out the octree
	bool SaveOctree(char * pFilename, TColTriArray * pCollisionTriangles, u32 iCRCofTriFile);
	void SaveOctreeR(CNavGenOctreeNodeBase * pNode, TColTriArray * pCollisionTriangles, FILE * filePtr);

	// Load an octree
	bool LoadOctree(char * pFilename, TColTriArray * pCollisionTriangles, u32 * iCRCtoMatch);
	CNavGenOctreeNodeBase * LoadOctreeR(TColTriArray * pCollisionTriangles, FILE * filePtr);

	inline void IncTimeStamp(void) { m_iCurrentTimeStamp++; }
	inline u32 GetTimeStamp(void) { return m_iCurrentTimeStamp; }

	// Tests a line of sight.  Returns true if LOS is clear, or false if there was an intersection
	virtual bool TestLineOfSight(const Vector3 & vStart, const Vector3 & vEnd, const u32 iCollisionTypeFlags, bool bVertical = false, u32 iFlagsToTest = 0, bool bIgnoreSeeThrough = false, bool bIgnoreFixed = false, bool bIgnoreClimableObjects = true, bool bIgnoreStairs = false, bool bIgnoreRiverBound = false);
	bool TestLineOfSight(CNavGenOctreeNodeBase * pNodeBase, const Vector3 & vNodeMin, const Vector3 & vNodeMax, const Vector3 & vStart, const Vector3 & vEnd, const u32 iCollisionTypeFlags, u32 iFlagsToTest = 0, bool bIgnoreSeeThrough = false, bool bIgnoreFixed = false, bool bIgnoreClimableObjects = true, bool bIgnoreStairs = false, bool bIgnoreRiverBound = false);

	// As above, but returns data about the intersection
	// Returns TRUE if there is an intersection
	virtual bool ProcessLineOfSight(
		const Vector3 & vStart,
		const Vector3 & vEnd,
		u32 & iOut_ReturnFlags,
		const u32 iCollisionTypeFlags,
		Vector3 & vHitPoint,
		float & fHitDist,
		CNavGenTri *& hitTriangle,
		bool bVertical = false,
		u32 iFlagsToTest = 0,
		bool bIgnoreSeeThrough = false,
		bool bIgnoreFixed = false,
		bool bIgnoreClimableObjects = true,
		bool bIgnoreStairs = false,
		bool bIngoreRiverBound = false
	);

	// performs 5 jittered vertical linetests & returns the one with the highest Z
	virtual bool ProcessLineOfSightVerticalJittered(
		const Vector3 & vStart,
		const Vector3 & vEnd,
		const float fJitterSize,
		const u32 iCollisionTypeFlags,
		Vector3 & vHitPoint,
		float & fHitDist,
		CNavGenTri *& hitTriangle,
		u32 iFlagsToTest = 0,
		bool bIgnoreSeeThrough = false,
		bool bIgnoreFixed = false,
		bool bIgnoreClimableObjects = true,
		bool bIgnoreRiverBound = false
	);

	bool ProcessLineOfSightFromNode(
		CNavGenOctreeNodeBase * pNodeToStartAt,
		const Vector3 & vNodeMin,
		const Vector3 & vNodeMax,
		const Vector3 & vStart,
		const Vector3 & vEnd,
		u32 & iOut_ReturnFlags,
		const u32 iCollisionTypeFlags,
		Vector3 & vHitPoint,
		float & fHitDist,
		CNavGenTri *& hitTriangle,
		bool bVertical = false,
		u32 iFlagsToTest = 0,
		bool bIgnoreSeeThrough = false,
		bool bIgnoreFixed = false,
		bool bIgnoreClimableObjects = true,
		bool bIgnoreStairs = false,
		bool bIgnoreRiverBound = false
	);

	void AddTrianglesToCulledGeom(CCulledGeom & culledGeom, CNavGenOctreeNodeBase * pNode=NULL);

	int FindAllTrianglesInVolume(CNavGenTri** pTriArrayOut, int maxNumTris, const CClipVolume& volume);

	virtual bool TestVolumeIsClear(const CClipVolume & volume, const u32 iCollisionTypeFlags, bool bIgnoreClimbableObjects = true);

	// Returns whether there was an intersection, and return which side of the intersecct poly we hit.
	// iSide is returned as (+1 : front side, -1 : back side, 0 : unknwown)
	bool GetRayIntersectPolySide(const Vector3 & vStart, const Vector3 & vEnd, int & iSide, const u32 iCollisionTypeFlags, Vector3 * pHitPos = NULL);

	inline CNavGenOctreeNodeBase * RootNode(void) { return m_pRootNode; }
	inline void GetOctreeExtents(Vector3 & vMin, Vector3 & vMax)
	{
		vMin = m_RootNodeMin;
		vMax = m_RootNodeMax;
	}
	void GetOctantExtents(u32 oct, const Vector3 & vParentMin, const Vector3 & vParentMax, Vector3 & vOctantMin, Vector3 & vOctantMax) const;

	void ResetBatchLOS();
	bool AddLosTestToBatch(const Vector3 & vStart, const Vector3 & vEnd);
	void StartBatchLosTest(int & iNumLosHit, int & iNumLosClear, const u32 iCollisionTypeFlags, EBatchTestLosMode eBatchLosTestMode, bool bRecordIntersections, u32 iFlagsToTest=0, bool bIgnoreSeeThrough=false, bool bIgnoreFixed=false, bool bIgnoreClimbable=true, bool bIgnoreStairs=false, bool bIgnoreRiverBound = false);

	inline int GetBatchNumIntersections(int b) { return m_iBatchNumIntersections[b]; }
	inline const Vector3 & GetBatchIntersection(int b, int i) { return m_vBatchIntersections[b][i]; }
	inline CNavGenTri * GetBatchHitTri(int b, int i) { return m_pBatchHitTris[b][i]; }
	inline float GetBatchHitDistSqr(int b, int i) { return m_fBatchHitDistsSqr[b][i]; }

	// Public stats
	u32 m_iTotalNumInternalNodes;
	u32 m_iTotalNumLeaves;
	u32 m_iMaximumDepth;

	bool m_bAlsoStoreSplitPolysInOctreeLeaves;
	int m_iDesiredNumTrisInEachLeaf;
	float m_fDesiredLeafNodeSize;

	// This timestamp is used to ensure we don't process polys more than once when they are stored in multiple octree leaves
	u32 m_iCurrentTimeStamp;

protected:

	// The bounds for the entire octree.  Bounds for individual internal nodes are
	// calculated as we descend into the octree.  Only leaves hold their own min/min's
	// which may in fact be smaller than the calculated bounds.
	Vector3 m_RootNodeMin;
	Vector3 m_RootNodeMax;

	CNavGenOctreeNodeBase * m_pRootNode;

	static const u32 ms_iMajorFileVersion;
	static const u32 ms_iMinorFileVersion;

	Vector3 m_vRayMins;
	Vector3 m_vRayMaxs;
	TShortMinMax m_NavGenPolyLosMinMax;

	CNavGenOctreeNodeBase * ConstructR(CNavGenOctreeNode * pParentNode, s32 iOctantThisIs, atDList<CSplitPoly*> * pPolys, const Vector3 & vNodeMin, const Vector3 & vNodeMax, u32 iDepth);

	bool TestLineOfSightR(
		CNavGenOctreeNodeBase * pNodeBase,
		Vector3 & vNodeMin,
		Vector3 & vNodeMax,
		const Vector3 & vStart,
		const Vector3 & vEnd
	);
	bool TestLineOfSightVerticalR(
		CNavGenOctreeNodeBase * pNodeBase,
		Vector3 & vNodeMin,
		Vector3 & vNodeMax,
		const Vector3 & vStart,
		const Vector3 & vEnd
	);


	// As above, but returns data about the intersection
	void ProcessLineOfSightR(
		CNavGenOctreeNodeBase * pNodeBase,
		Vector3 & vNodeMin,
		Vector3 & vNodeMax,
		const Vector3 & vStart,
		const Vector3 & vEnd
	);
	void ProcessLineOfSightVerticalR(
		CNavGenOctreeNodeBase * pNodeBase,
		Vector3 & vNodeMin,
		Vector3 & vNodeMax,
		const Vector3 & vStart,
		const Vector3 & vEnd
	);

	int FindAllTrianglesInVolumeR(
		CNavGenTri** pTriArrayOut, int maxNumTris,
		const CNavGenOctreeNodeBase * pNodeBase,
		const CClipVolume & volume,
		const Vector3 & vNodeMin,
		const Vector3 & vNodeMax
	) const;

	bool TestVolumeIsClearR(
		CNavGenOctreeNodeBase * pNodeBase,
		const CClipVolume & volume,
		Vector3 & vNodeMin,
		Vector3 & vNodeMax,
		const u32 iCollisionTypeFlags,
		bool bIgnoreClimbableObjects
	);

	CNavGenTri * m_pHitTri;
	Vector3 m_vHitPos;
	float m_fClosestHitDistSqr;
	u32 m_iLosFlagsToTest;
	u32 m_iCollisionTypesToTest;
	bool m_bIgnoreSeeThrough;
	bool m_bIgnoreFixed;
	bool m_bIgnoreClimbableObjects;
	bool m_bIgnoreStairs;
	bool m_bIgnoreRiverBound;
	u32 m_iOutReturnFlags;

	bool TestBatchLineOfSightR(CNavGenOctreeNodeBase * pNodeBase, Vector3 & vNodeMin, Vector3 & vNodeMax);

	Vector3 m_vBatchLosTestStarts[MAX_NUM_BATCHED_LOS_TESTS];
	Vector3 m_vBatchLosTestEnds[MAX_NUM_BATCHED_LOS_TESTS];
	TShortMinMax m_MinMaxForBatchedLosTest[MAX_NUM_BATCHED_LOS_TESTS];
	int m_iBatchLosTestNumPolyHits[MAX_NUM_BATCHED_LOS_TESTS];
	int m_iBatchNumIntersections[MAX_NUM_BATCHED_LOS_TESTS];	// how many intersections for each ray
	Vector3 m_vBatchIntersections[MAX_NUM_BATCHED_LOS_TESTS][MAX_BATCH_ISECTS];	// arrays of intersections
	CNavGenTri * m_pBatchHitTris[MAX_NUM_BATCHED_LOS_TESTS][MAX_BATCH_ISECTS];	// arrays of triangles
	float m_fBatchHitDistsSqr[MAX_NUM_BATCHED_LOS_TESTS][MAX_BATCH_ISECTS];	// arrays of intersection distances
	int m_iNumBatchedLosTests;
	EBatchTestLosMode m_eBatchedLosTestMode;
	EProcessOrTestLos m_eBatchedProcessOrTestMode;
	bool m_bBatchRecordIntersections;

	void GetRayIntersectPolySideR(CNavGenOctreeNodeBase * pNodeBase, Vector3 & vNodeMin, Vector3 & vNodeMax, const Vector3 & vStart, const Vector3 & vEnd);

	CLosIntersections m_LosIntersections;

	struct TConstructOctreeStruct
	{
		TConstructOctreeStruct(void)
		{
			// Up plane
			iPlaneFlags[0] = PLANESIDE_POSZ;
			vPlaneNormals[0] = Vector3(0, 0, 1.0f);

			// Right plane
			iPlaneFlags[1] = PLANESIDE_POSX;
			vPlaneNormals[1] = Vector3(1.0f, 0, 0);

			// Forward plane
			iPlaneFlags[2] = PLANESIDE_POSY;
			vPlaneNormals[2] = Vector3(0, 1.0f, 0);
		}

		s32 iNumPolys;
		float fMaxDimension;

		u32 iPlaneFlags[3];
		Vector3 vPlaneNormals[3];
		float fPlaneDists[3];
		Vector3 vCenter;
	};

	TConstructOctreeStruct m_ConstructStruct;
};

}	// namespace rage

#endif








