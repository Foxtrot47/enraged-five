#ifndef NAVGEN_STORE_H
#define NAVGEN_STORE_H

#include "toolnavmesh.h"				// For TNavMeshIndex

namespace rage
{

class CNavGen_NavMeshStore
{
public:

	CNavGen_NavMeshStore();
	~CNavGen_NavMeshStore();

	class CEntry
	{
	public:	
		CNavMesh * pNavMesh;
	};

	static bool LoadAll(char * pPath);
	static void UnloadAll();

	static CNavMesh * GetNavMeshFromIndex(const s32 iMesh);
	static CNavMesh * GetNavMeshFromSectorCoords(const s32 x, const s32 y);

	static bool TestNavMeshLOS(const Vector3 & vStart, const Vector3 & vEnd);

	static bool GetDoNavMeshesAdjoin(const s32 iNavMesh1, const s32 iNavMesh2);

	static void GetSectorFromNavMeshIndex(const s32 iNavMesh, s32 & iX, s32 & iY);

	static s32 GetNavMeshIndexFromPosition(const Vector3 & vPos);

	static s32 GetMeshIndexFromSectorCoords(const s32 iSectorX, const s32 iSectorY);

	static s32 GetNavMeshIndexFromFilename(const char * pFileName, s32 * iNavMeshXPos=NULL, s32 * iNavMeshYPos=NULL);

	static s32 NavMeshIndexToNavNodesIndex(const TNavMeshIndex iNavMesh);

	static void GetNavMeshExtentsFromIndex(const TNavMeshIndex iMeshIndex, Vector2 & vMin, Vector2 & vMax);

	static s32 GetTotalNumNavMeshes() { return NAVMESH_MAX_MAP_INDEX+1; }

	static void Set(s32 index, CNavMesh * pNavMesh) { m_Entries[index].pNavMesh = pNavMesh; }

private:

	static CEntry m_Entries[NAVMESH_MAX_MAP_INDEX+1];
};

}	// namespace rage

#endif	// NAVGEN_STORE_H
