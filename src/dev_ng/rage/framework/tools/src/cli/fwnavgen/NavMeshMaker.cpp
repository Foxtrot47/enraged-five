#include "NavMeshMaker.h"
#include "config.h"
#include "NavGen.h"
#include "NavGen_Store.h"
#include "workerthreads.h"
#include "ai/navmesh/navmeshextents.h"
#include "spatialdata/sphere.h"
#include "phcore/materialmgrimpl.h"
#include "phcore/surface.h"
#include "system/nelem.h"
#include "system/spinlock.h"
#include "system/param.h"
#include "diag/channel.h"

#include <time.h>
#include <direct.h>
#include <crtdbg.h>

#define __AUTO_CREATE_RESOLUTION_AREAS_FOR_DOORS		0
#define __AUTO_CREATE_RESOLUTION_AREAS_FOR_LADDERS		1
#define __USE_CACHED_COVERPOINTS						1

extern __THREAD int RAGE_LOG_DISABLE;

PARAM(nocfg, "run without pargen config file");

namespace rage
{

//#define SIMPLE_HEAP_SIZE		(384*1024)
//#define SIMPLE_PHYSICAL_SIZE	(512*1024)

//-sample -inputpath "X:\GTA\exported_navmeshes" -gamepath "X:\GTA\build" -cmdfile "X:\tools_release\navgen\NavMeshMaker\meshes_ny.lst"


// Which mode this has been invoked as
int g_Mode = -1;
// Number of worker threads to use
int g_iMaxNumThreads = 1;

// The name of a command file which will be used
char * g_CommandFileName = NULL;
char * g_ResolutionAreasFileName = NULL;
char * g_CustomCoverpointsFileName = NULL;
char * g_DLCAreasFileName = NULL;

// Create audio mesh
bool g_AudioMesh = false;
// Audio mesh params file
char * g_AudioMeshParamsFile = NULL;

vector<TNavMeshParams*> g_SourceNavMeshes;

// If this is non-zero, then it will force the navmeshes to assume this build ID
u32 g_iForceBuildID = 0;
// An input path to be prepended to all input file-names
char * g_InputPath = NULL;
// An output path to be prepended to all output file-names
char * g_OutputPath = NULL;
// Path in which DLC navmeshes are to be found
char * g_DLCPath = NULL;
// A list of containers which hold the nav-meshes in the world
vector <CNavMeshHolder*> g_NavMeshes;
// The resolution at which to sample the world
float g_fSampleResolution = 0.0f;	// Should always get a value from the configuration file or other source.
bool g_fSampleResolutionExplicitlySet = false;	// True if -res was used.
// The number of samples to take across the extent of the navmesh (g_fSampleResolution can be calculated from this)
//int g_iNumSamples = 100;
// If this is true then we are processing non-map navmeshes
bool g_bProcessingNonMapNavmeshes = false;
char * g_MappingFileExtension = NULL;
char * g_MappingOutputFilename = NULL;
char * g_CopyIntoSubFoldersExtension = NULL;

// The max height change under a triangle edge during triangulation
float g_fMaxHeightChangeUnderNavMeshTriEdge = 0.0f;		// Will be set from configuration file.
float g_fMinimumVerticalDistanceBetweenNodes = 0.0f;	// Will be set from configuration file.
#if NAVGEN_OBSOLETE
float g_fMinCoverPointSeperationWhenThinningOut = CNavGen::ms_fMinCoverPointSeparationWhenThinningOut;
#endif
bool g_bAerialNavigation = false;
bool g_bCreateNavMeshUnderwater = false;
bool g_bGeometryProximity = true;

//bool g_bGlobalRemoveTrisBehindWalls = false;
bool g_bGlobalIgnoreSteepness = false;
float g_fOverrideTestClearHeightInTriangulation = -1.0f;

char * g_CompareDirectory1 = NULL;
char * g_CompareDirectory2 = NULL;

// This external is defined in GameDataFolder.h, it is set via the -gamedatafolder cmdline parameter
// and is required so that the physics material manager can load the game's material types in
char * g_pGameFolder = _strdup("X:\\gta\\build");

char g_LogFilePrefix[64];

// Need to implement this in order to compile..
//void CDebug::DebugMessage(const char * fmt, ...) { }

// This file is used to log compilation info to
FILE * g_pLogFile = NULL;
// NB : This shouldn't be hardcoded..
//float g_fSizeOfNavMesh = 50.0f;

// Define this to perform a sanity-check on each navmesh every time loaded
//#define CHECK_NAVMESH_INTEGRITY

// PURPOSE:	This string is what's used as the base name for navmeshes for aerial navigation.
const char* g_AerialMeshName = "heightmesh";

TNavMeshParams::~TNavMeshParams()
{
	if(m_pFileName)
		free(m_pFileName);
	if(m_pFileNameWithoutExtension)
		free(m_pFileNameWithoutExtension);
	if(m_pAuthoredMeshFileName)
		free(m_pAuthoredMeshFileName);
	if(m_pNonMapNavMeshName)
		free(m_pNonMapNavMeshName);
	if(m_pSearchName)
		free(m_pSearchName);
	if(m_pReplaceName)
		free(m_pReplaceName);
}

sysSpinLockToken outputTextToken;

void
CNavMeshMaker::OutputText(char * pText)
{
	sysSpinLock lock(outputTextToken);

	printf(pText);

	// Print out to g_pLogFile - but not the g_pBlankLine's..
	if(g_pLogFile)
	{
		if(*pText != '\r')
		{
			fprintf(g_pLogFile, pText);
		}
		else
		{
			fprintf(g_pLogFile, "\n");
		}
		fflush(g_pLogFile);
	}

	//OutputDebugString(pText);
}

void LogArgs()
{
	if(g_pLogFile)
	{
		int argc = sysParam::GetArgCount();
		char ** argv = sysParam::GetArgArray();

		fprintf(g_pLogFile, "--------------------------------------------------------------------\n");
		fprintf(g_pLogFile, "NumArgs : %i\n", argc);

		for(int a=0; a<argc; a++)
		{
			fprintf(g_pLogFile, "Args %i: %s\n", a, argv[a]);
		}

		fprintf(g_pLogFile, "--------------------------------------------------------------------\n");
	}
}

int
NavMeshMakerMain()
{
	printf("CNavGenNode = %i bytes\n", sizeof(CNavGenNode));
	printf("CNavGenTri = %i bytes\n", sizeof(CNavGenTri));
	printf("CNavSurfaceTri = %i bytes\n", sizeof(CNavSurfaceTri));
	printf("CNavTriEdge = %i bytes\n", sizeof(CNavTriEdge));

	int argc = sysParam::GetArgCount();
	char ** argv = sysParam::GetArgArray();

#if __DEV && !__OPTIMIZED
	// Enable the CRT to debug memory leak info at exit
	//_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	_CrtMemState navGenMemStart;
	_CrtMemCheckpoint(&navGenMemStart);
#endif

	// We have to make sure that there is a valid working directory.  When NavMeshMaker is run, it
	// should ideally have a full working directory specified in the args parsed below.
	_chdir(g_pGameFolder);

	char logFileName[512];

//	const fwNavGenConfig& cfg = PARAM_nocfg.Get() ?
//		GetDefaultConfig() : fwNavGenConfig::Get();

	if(PARAM_nocfg.Get())
	{
		fwNavGenConfigManagerDefaults<fwNavGenConfig>::InitClass(NULL);
	}
	const fwNavGenConfig& cfg = fwNavGenConfig::Get();

	// Initialize the global sample resolution variable from the configuration file.
	// It should be referenced the first time by ParseParams(), where it can get assigned
	// a different value.
	// Note: if -aerial is used, a different default value will be used, but this is too early
	// to know if that parameter is present.
	g_fSampleResolution = cfg.m_SampleResolutionDefault;

	g_fMaxHeightChangeUnderNavMeshTriEdge = cfg.m_MaxHeightChangeUnderTriangleEdge;

	g_fMinimumVerticalDistanceBetweenNodes = cfg.m_MinimumVerticalDistanceBetweenNodes;


	// Parse the parameters passed to the program
	if(!CNavMeshMaker::ParseParams(argc, argv))
	{
		return -1;
	}

	if(!g_OutputPath)
	{
		g_OutputPath = _strdup(g_InputPath);
	}

	if(g_Mode == DeleteIdenticalNavFiles)
	{
		CNavMeshMaker::CompareAndDeleteIdenticalNavFiles(g_CompareDirectory1, g_CompareDirectory2);
		return 1;
	}

	if(g_Mode == CreateIndexMappingFile)
	{
		char logShortName[256];
		sprintf(logShortName, "CreateIndexMapping_%s.txt", g_MappingOutputFilename);

		sprintf(logFileName, "%s/%s%s", g_InputPath, g_LogFilePrefix, logShortName);
		g_pLogFile = fopen(logFileName, "wt");
		CNavMeshMaker::OutputText(logFileName);
		LogArgs();

		char mappingFileName[MAX_PATH];
		sprintf_s(mappingFileName, "%s/%s", g_OutputPath, g_MappingOutputFilename);
		CNavMeshMaker::CreateMappingFile(g_MappingFileExtension, mappingFileName);
		free(g_MappingFileExtension);

		return 1;
	}

	if(g_Mode == CopyFilesIntoSubfoldersForRpf)
	{
		CNavMeshMaker::CopyFilesIntoSubfoldersForRpfCreation(g_CopyIntoSubFoldersExtension);
		return 1;
	}

	if(g_Mode == DisplayNavMeshInfo)
	{
		CNavMeshMaker::PrintNavMeshInfo();
		return 1;
	}

#if __DEV
	RAGE_LOG_DISABLE = 1;
#endif

	//CFileMgr::Initialise();

	CPathServerExtents::m_iNumSectorsPerNavMesh = 0;
	CNavMeshMaker::ReadDatFile();

	if(CPathServerExtents::m_iNumSectorsPerNavMesh==0)
	{
		printf("g_iNumSectorsPerNavMesh==0\n");
		return -1;
	}

	CNavGen::SetOutputTextFn(CNavMeshMaker::OutputText);

	CNavMeshMaker::OutputText("\n");

	if(!g_bOnlyNamedFiles && !g_bProcessingNonMapNavmeshes)
	{
		CNavMeshMaker::InitMeshFilenames();
	}

	if(g_CommandFileName)
	{
		if(!CNavMeshMaker::ReadCmdFile())
		{
			CNavMeshMaker::ClearData();
			return -1;
		}
	}

	if(g_Mode == CombineCollision)
	{
		CNavMeshMaker::CombineCollisionFiles();
		return 0;
	}

	if(g_Mode == GenerateMeshes)
	{
		sprintf(logFileName, "%s/%s%s", g_InputPath, g_LogFilePrefix, "sample_log.txt");
		g_pLogFile = fopen(logFileName, "wt");
		CNavMeshMaker::OutputText(logFileName);
		LogArgs();

		if(!CNavMeshMaker::GenerateNavMeshes())
		{
			CNavMeshMaker::ClearData();
			return -1;
		}

		if(g_pLogFile)
			fclose(g_pLogFile);
		g_pLogFile = NULL;
	}

	if(g_Mode == MergeMeshes)
	{
		sprintf(logFileName, "%s/%s%s", g_InputPath, g_LogFilePrefix, "merge_log.txt");
		g_pLogFile = fopen(logFileName, "wt");
		CNavMeshMaker::OutputText(logFileName);
		LogArgs();

		if(!CNavMeshMaker::MergeNavMeshes())
		{
			CNavMeshMaker::ClearData();
			return -1;
		}

		if(g_pLogFile)
			fclose(g_pLogFile);
		g_pLogFile = NULL;
	}

	if(g_Mode == StitchMeshes)
	{
		sprintf(logFileName, "%s/%s%s", g_InputPath, g_LogFilePrefix, "stitch_log.txt");
		g_pLogFile = fopen(logFileName, "wt");
		CNavMeshMaker::OutputText(logFileName);
		LogArgs();

		if(!CNavMeshMaker::StitchNavMeshes())
		{
			CNavMeshMaker::ClearData();
			return -1;
		}

		if(g_pLogFile)
			fclose(g_pLogFile);
		g_pLogFile = NULL;
	}

	if(g_Mode == FloodFillMeshes)
	{
		sprintf(logFileName, "%s/%s%s", g_InputPath, g_LogFilePrefix, "floodfill_log.txt");
		g_pLogFile = fopen(logFileName, "wt");
		CNavMeshMaker::OutputText(logFileName);
		LogArgs();

		CNavMeshMaker::OutputText("\nFloodFilling navmeshes..\n");

		if(!CNavMeshMaker::FloodFillNavMeshes())
		{
			CNavMeshMaker::ClearData();
			return -1;
		}

		if(g_pLogFile)
			fclose(g_pLogFile);
		g_pLogFile = NULL;
	}

	if(g_Mode == DisableBadClimbs)
	{
		FastAssert(false);
/*
		CNavMeshMaker::OutputText("\nDisabling bad climbs in navmeshes..\n");

		if(!CNavMeshMaker::DisableBadClimbsInNavMeshes())
		{
			CNavMeshMaker::ClearData();
			return -1;
		}
*/
	}

	if((g_Mode == AnalyseMainMapMeshes || g_Mode == AnalyseNonMapMeshes))
	{
		sprintf(logFileName, "%s/%s%s", g_InputPath, g_LogFilePrefix, "analyse_log.txt");
		g_pLogFile = fopen(logFileName, "wt");
		CNavMeshMaker::OutputText(logFileName);
		LogArgs();

		bool bAnalyse, bMainMap;
		if(g_Mode == AnalyseMainMapMeshes || g_Mode == AnalyseNonMapMeshes)
		{
			bAnalyse = true;
			bMainMap = (g_Mode == AnalyseMainMapMeshes);
		}
		else
		{
			bAnalyse = false;
			bMainMap = false;	// Not much point doing "-linkcover" for dynamic navmeshes, they're usually very quick to analyse
		}

		if(!CNavMeshMaker::AnalyseNavMeshes(bAnalyse, bMainMap))
		{
			CNavMeshMaker::ClearData();
			return -1;
		}

		if(g_pLogFile)
			fclose(g_pLogFile);
		g_pLogFile = NULL;
	}
	if(g_Mode == CreateHierarchical)
	{
		sprintf(logFileName, "%s/%s%s", g_InputPath, g_LogFilePrefix, "heirarchical_log.txt");
		g_pLogFile = fopen(logFileName, "wt");
		CNavMeshMaker::OutputText(logFileName);
		LogArgs();

		if(!CNavMeshMaker::CreateHierarchicalNavData())
		{
			CNavMeshMaker::ClearData();
			return -1;
		}

		if(g_pLogFile)
			fclose(g_pLogFile);
		g_pLogFile = NULL;
	}

	if(g_Mode == CleanMeshes)
	{
		if(!CNavMeshMaker::CleanNavMeshes())
		{
			CNavMeshMaker::ClearData();
			return -1;
		}
	}

	if(g_Mode == JoinDLC)
	{
		sprintf(logFileName, "%s/%s%s", g_InputPath, g_LogFilePrefix, "joinDLC_log.txt");
		g_pLogFile = fopen(logFileName, "wt");
		CNavMeshMaker::OutputText(logFileName);
		LogArgs();

		if(!CNavMeshMaker::JoinMeshesDLC(g_DLCPath, g_OutputPath))
		{
			CNavMeshMaker::ClearData();
			return -1;
		}

		if(g_pLogFile)
			fclose(g_pLogFile);
		g_pLogFile = NULL;
	}

	CNavMeshMaker::ClearData();

	OutputDebugString("Done!");

#if __DEV && !__OPTIMIZED
	//_CrtDumpMemoryLeaks();
	_CrtMemDumpAllObjectsSince(&navGenMemStart);
#endif

	return 1;
}

void
CNavMeshMaker::ClearData(void)
{
	u32 i;

	g_Mode = -1;

	free(g_CommandFileName);
	g_CommandFileName = NULL;

	free(g_InputPath);
	g_InputPath = NULL;

	free(g_OutputPath);
	g_OutputPath = NULL;

	if(g_DLCPath)
		free(g_DLCPath);

	for(i=0; i<g_SourceNavMeshes.size(); i++)
	{
		TNavMeshParams * pNavParams = g_SourceNavMeshes[i];
		delete pNavParams;
	}
	g_SourceNavMeshes.clear();
	g_SourceNavMeshes.resize(0);


	for(i=0; i<g_NavMeshes.size(); i++)
	{
		CNavMeshHolder * p = g_NavMeshes[i];
		delete p;
	}
	g_NavMeshes.clear();
	g_NavMeshes.resize(0);

	if(g_pLogFile)
	{
		fclose(g_pLogFile);
		g_pLogFile = NULL;
	}
}

//**************************************************************
// pGameDataPath will be something like "x:/gta5/build/dev"

void CNavMeshMaker::ReadDatFile()
{
	char pathForNavDatFile[512];
	sprintf(pathForNavDatFile, "%s\\common\\data\\nav.dat", g_pGameFolder);

	FILE * pFile = fopen(pathForNavDatFile, "rt");
	if(!pFile)
	{
		return;
	}

	char string[256];

	while(fgets(string, 256, pFile))
	{
		if(string[0] == '#')
			continue;

		int iNumMatched = 0;
		int iValue;

		// SECTORS_PER_NAVMESH defines the resolution which this game's navmesh system works at.
		iNumMatched = sscanf(string, "SECTORS_PER_NAVMESH = %i", &iValue);
		if(iNumMatched)
		{
			CPathServerExtents::m_iNumSectorsPerNavMesh = iValue;
			continue;
		}
	}

	fclose(pFile);
}

bool
CNavMeshMaker::ParseParams(int argc, char *argv[])
{
	// First argument is always the .exe itself
	int argIndex = 1;

	// Read in the arguments
	while(argIndex < argc)
	{
		char * pArg = argv[argIndex];
		if(!pArg)
		{
			break;
		}

		if(stricmp(pArg, "-deleteidentical")==0)
		{
			// Read the filename1
			pArg = argv[argIndex];
			if(!pArg || !*pArg)
			{
				OutputText("Error : -deleteidentical was used, but no path #1 was not found.\n");
				return false;
			}
			g_CompareDirectory1 = _strdup(pArg);

			// Read the filename2
			argIndex++;
			pArg = argv[argIndex];
			if(!pArg || !*pArg)
			{
				OutputText("Error : -deleteidentical was used, but no path #2 was not found.\n");
				return false;
			}
			g_CompareDirectory2 = _strdup(pArg);

			// Set mode appropriately
			g_Mode = DeleteIdenticalNavFiles;
			return true;
		}
		else if(stricmp(pArg, "-buildid")==0)
		{
			argIndex++;
			pArg = argv[argIndex];
			sscanf(pArg, "%u", &g_iForceBuildID);
		}
		// Specifying -? or /? or /h or -h will show the usage
		else if(stricmp(pArg, "-?")==0 || stricmp(pArg, "/?")==0 || stricmp(pArg, "-h")==0 || stricmp(pArg, "/?")==0)
		{
			ShowUsage();
			return false;
		}
		// If specified, an input path is expected to follow
		else if(stricmp(pArg, "-inputpath")==0)
		{
			// Read the path
			argIndex++;
			pArg = argv[argIndex];
			if(!pArg || !*pArg)
			{
				OutputText("Error : -inputpath was used, but no path was found.\n");
				return false;
			}
			free(g_InputPath);
			g_InputPath = _strdup(pArg);
		}
		// If specified, an output path is expected to follow
		else if(stricmp(pArg, "-outputpath")==0)
		{
			// Read the path
			argIndex++;
			pArg = argv[argIndex];
			if(!pArg || !*pArg)
			{
				OutputText("Error : -outputpath was used, but no path was found.\n");
				return false;
			}
			free(g_OutputPath);
			g_OutputPath = _strdup(pArg);
		}
		// If specified, a dlc path is expected to follow
		else if(stricmp(pArg, "-dlcpath")==0)
		{
			// Read the path
			argIndex++;
			pArg = argv[argIndex];
			if(!pArg || !*pArg)
			{
				OutputText("Error : -dlcpath was used, but no path was found.\n");
				return false;
			}
			free(g_DLCPath);
			g_DLCPath = _strdup(pArg);
		}
		// If specified, the path to the game's data folder is expected to follow
		else if(stricmp(pArg, "-gamepath")==0)
		{
			// Read the path
			argIndex++;
			pArg = argv[argIndex];
			if(!pArg || !*pArg)
			{
				OutputText("Error : -gamepath was used, but no path was found.\n");
				return false;
			}
			free(g_pGameFolder);
			g_pGameFolder = _strdup(pArg);

			// We HAVE to set the current working directory to equal this path.  This is because
			// the game now uses virtual relative devices to look for assert & this assumes that
			// they are all relative to the current working directory.
			_chdir(g_pGameFolder);
		}
		else if(stricmp(pArg, "-numthreads")==0)
		{
			argIndex++;
			pArg = argv[argIndex];
			sscanf(pArg, "%i", &g_iMaxNumThreads);

			if(g_iMaxNumThreads == 0)
			{
				g_iMaxNumThreads = sysIpcGetProcessorCount();
			}
		}
		/*
		else if(stricmp(pArg, "-cullbehindwalls")==0)
		{
			g_bGlobalRemoveTrisBehindWalls = true;
		}
		*/
		else if(stricmp(pArg, "-ignoresteepness")==0)
		{
			g_bGlobalIgnoreSteepness = true;
		}
		// Mode is set to combine collision subsections into navmesh block .tri files, ready for compilation
		else if(stricmp(pArg, "-combine")==0)
		{
			g_Mode = CombineCollision;
		}
		// Mode is set to sample navigation meshes from ".tri" data, and output ".srf" triangle meshes
		else if(stricmp(pArg, "-sample")==0)
		{
			g_Mode = GenerateMeshes;
		}
		// Mode is set to merge together polys within the ".srf" triangle meshes and generate ".inv" files
		else if(stricmp(pArg, "-merge")==0)
		{
			g_Mode = MergeMeshes;
		}
		// Mode is set to stitch together ".inv" NavMeshes and establish neighbour adjacency
		else if(stricmp(pArg, "-stitch")==0)
		{
			g_Mode = StitchMeshes;
		}
		// Mode is set to stitch together ".inv" NavMeshes and establish neighbour adjacency
		else if(stricmp(pArg, "-floodfill")==0)
		{
			g_Mode = FloodFillMeshes;
		}
		// Mode is set to load ".inv", ".oct" and ".tri" triangles, and analyze for climb-up's & drop-down's, etc.
		else if(stricmp(pArg, "-analyse")==0)
		{
			g_Mode = AnalyseMainMapMeshes;
		}
		// Mode is set to load ".lst" files detailing climbs which didn't work in the game engine, and to disable these in the ".inv" files in the output folder.
		else if(stricmp(pArg, "-disablebadclimbs")==0)
		{
			g_Mode = DisableBadClimbs;
		}
		// Mode is set to load ".inv", ".oct" and ".tri" triangles, and analyze for climb-up's & drop-down's, etc.
		else if(stricmp(pArg, "-analysedynamic")==0)
		{
			g_Mode = AnalyseNonMapMeshes;
		}
		// Mode is set to load navmeshes, and to generate heirarchical nav data
		else if(stricmp(pArg, "-hierarchical")==0)
		{
			g_Mode = CreateHierarchical;
		}
		// Mode is set to load up ".inv" files & to clean out bad data (this is a hack & will not be req'd in future)
		else if(stricmp(pArg, "-clean")==0)
		{
			g_Mode = CleanMeshes;
		}
		else if(stricmp(pArg, "-joindlc")==0)
		{
			g_Mode = JoinDLC;
		}
		// (sample resolution)
		// If specified, then is followed by a float which defines the spacing between samples if -sample mode is set
		else if(stricmp(pArg, "-res")==0)
		{
			argIndex++;
			pArg = argv[argIndex];
			sscanf(pArg, "%f", &g_fSampleResolution);
			g_fSampleResolutionExplicitlySet = true;	// Remember that we got an explicit override.
		}
		else if(stricmp(pArg, "-maxheightchange")==0)
		{
			argIndex++;
			pArg = argv[argIndex];
			sscanf(pArg, "%f", &g_fMaxHeightChangeUnderNavMeshTriEdge);
		}
		else if(stricmp(pArg, "-nodesminzdist")==0)
		{
			argIndex++;
			pArg = argv[argIndex];
			sscanf(pArg, "%f", &g_fMinimumVerticalDistanceBetweenNodes);
		}
		else if(stricmp(pArg, "-pedheighttest")==0)
		{
			argIndex++;
			pArg = argv[argIndex];
			sscanf(pArg, "%f", &g_fOverrideTestClearHeightInTriangulation);
		}
		// If specified, then a text file is being used to detail all the files to process
		else if(stricmp(pArg, "-cmdfile")==0)
		{
			// Read the command filename
			argIndex++;
			pArg = argv[argIndex];
			if(!pArg || !*pArg)
			{
				OutputText("Error : -cmdfile was used, but no command file was specified.\n");
				return false;
			}
			g_CommandFileName = _strdup(pArg);
		}
		// If we have specified an xml navmesh resolution areas file, then load this in
		// It will reset the default sampling resolution to 1.0, and will cause any -res
		// param or -res entries in a command file to be ignored, in favour of using the
		// sampling resolution multipliers in the areas file.
		else if(stricmp(pArg, "-areas")==0)
		{
			// Read the command filename
			argIndex++;
			pArg = argv[argIndex];
			if(!pArg || !*pArg)
			{
				OutputText("Error : -areas was used, but no file was specified.\n");
				return false;
			}
			g_ResolutionAreasFileName = _strdup(pArg);
		}
		else if(stricmp(pArg, "-coverpoints")==0)
		{
			// Read the filename
			argIndex++;
			pArg = argv[argIndex];
			if(!pArg || !*pArg)
			{
				OutputText("Error : -coverpoints was used, but no file was specified.\n");
				return false;
			}
			g_CustomCoverpointsFileName = _strdup(pArg);
		}
		// If we have specified an xml DLC areas file, then load this in
		else if(stricmp(pArg, "-dlcareas")==0)
		{
			// Read the command filename
			argIndex++;
			pArg = argv[argIndex];
			if(!pArg || !*pArg)
			{
				OutputText("Error : -dlcareas was used, but no file was specified.\n");
				return false;
			}
			g_DLCAreasFileName = _strdup(pArg);
		}
		// If -onlynamedfiles is specified, then ONLY the files listed in the cmd file (g_CommandFileName) will be processed
		else if(stricmp(pArg, "-onlynamedfiles")==0)
		{
			g_bOnlyNamedFiles = true;
		}
		// If -background is specified, then CNavGen will Sleep(0) frequently to allow the system to be more responsive
		else if(stricmp(pArg, "-background")==0)
		{
			g_bProcessInBackground = true;
			OutputText("Processing in background.\n");
		}
		// If -nonmap is specified, then we are processing non-map navmeshes
		else if(stricmp(pArg, "-nonmap")==0)
		{
			g_bProcessingNonMapNavmeshes = true;
			OutputText("Processing non-map navmeshes.\n");
		}
		else if(stricmp(pArg, "-createunderwater")==0)
		{
			g_bCreateNavMeshUnderwater = true;
		}
		else if(stricmp(pArg, "-nogeometryproximity")==0)
		{
			g_bGeometryProximity = false;
		}
		else if(stricmp(pArg, "-createmappingfile")==0)
		{
			g_Mode = CreateIndexMappingFile;	

			//*********************
			// Read the extension

			argIndex++;
			pArg = argv[argIndex];
			if(!pArg || !*pArg)
			{
				OutputText("Error : -createmappingfile was used, but no file extension was found.\n");
				return false;
			}
			g_MappingFileExtension = _strdup(pArg);

			//*****************************
			// Read the mapping file name

			argIndex++;
			pArg = argv[argIndex];
			if(!pArg || !*pArg)
			{
				OutputText("Error : -createmappingfile was used, but no mapping filename was found.\n");
				return false;
			}
			g_MappingOutputFilename = _strdup(pArg);
		}
		else if(stricmp(pArg, "-copyintosubfolders")==0)
		{
			g_Mode = CopyFilesIntoSubfoldersForRpf;	

			//*********************
			// Read the extension

			argIndex++;
			pArg = argv[argIndex];
			if(!pArg || !*pArg)
			{
				OutputText("Error : -copyintosubfolders was used, but no file extension was found.\n");
				return false;
			}
			g_CopyIntoSubFoldersExtension = _strdup(pArg);
		}
		else if(stricmp(pArg, "-info")==0)
		{
			g_Mode = DisplayNavMeshInfo;
		}
		else if(stricmp(pArg, "-aerial") == 0)
		{
			g_bAerialNavigation = true;

			// Switch to use the sampling resolution for heightmeshes. However, if we
			// already found a -res on the command line, keep it.
			if(!g_fSampleResolutionExplicitlySet)
			{
				const fwNavGenConfig& cfg = fwNavGenConfig::Get();
				Assert(g_fSampleResolution == cfg.m_SampleResolutionDefault);
				g_fSampleResolution = cfg.m_AerialSampleResolutionDefault;
			}
		}
		
		argIndex++;
	}

	return true;
}


void
CNavMeshMaker::ShowUsage(void)
{
	OutputText("NavMeshMaker\n");
	OutputText("A command-line tool for creating navigation meshes.\n");
	OutputText("(c) Rockstar North 2005, author James Broad.\n");
	OutputText("\n");
	OutputText("Usage :\n");
	OutputText("-sample [filename.tri] : samples the given file & saves as [filename.3ds]\n");
	OutputText("-sample [list.txt] : samples all .tri files in list & saves with .srf extensions\n");
	OutputText("\n");
	OutputText("-merge [list.txt] : loads .srf files and converts into .inv NavMesh, by merging faces\n");
	OutputText("\n");
	OutputText("-stitch [list.txt] : stitches all .inv files in list, and resaves with .nav extensions\n");
	OutputText("\n");
	OutputText("-floodfill [list.txt] : floodfills all .inv files in list, removing inaccessible polys - and resaves with .nav extensions\n");
	OutputText("\n");
	OutputText("-analyse [list.txt] : analyses all .inv files in list, and saves with .nav extensions (needs .oct and .tri files present)\n");
	OutputText("\n");
	OutputText("-disablebadclimbs [list.txt] : reads BadClimbs_X_Y.lst files produced by running game with \"-validateclimbs\" param, disables these climbs in \".inv\" files in the export folder.\n");
	OutputText("\n");
	OutputText("-precalc [list.txt] : loads all .inv files in list, and precalcs crossing times\n");
	OutputText("\n");
	OutputText("-inputpath [pathname] : specifies an input path for data\n");
	OutputText("-outputpath [pathname] : specifies an output path for data\n");
	OutputText("\n");
	OutputText("-comparecrc [full_filename1] [full_filename2]: compares file CRCs, returns 1 if identical, 0 otherwise\n");
	OutputText("\n");
	OutputText("Example usage:\n");
	OutputText("-sample MySectors.list -inputpath D:\\Data\n");
	OutputText("-stitch MyMeshes.list -inputpath D:\\Data\n");
}

char * CNavMeshMaker::GetFileExtensionForMode()
{
	// Depending upon what MODE we're currently in, replace/append file extension with appropriate type
	char * pExt = NULL;
	switch(g_Mode)
	{
		case GenerateMeshes:
			pExt = "tri";
			break;
		case MergeMeshes:
			pExt = "srf";
			break;
		case StitchMeshes:
		case AnalyseNonMapMeshes:
			pExt = "mrg";
			break;
		case AnalyseMainMapMeshes:
		case FloodFillMeshes:
		case CleanMeshes:
		case JoinDLC:
		default:
			pExt = g_bAerialNavigation ? "inh" : "inv";
			break;
	}
	return pExt;
}

TNavMeshParams * CNavMeshMaker::GetSourceFileByCoords(int iSectorX, int iSectorY)
{
	for(u32 m=0; m<g_SourceNavMeshes.size(); m++)
	{
		TNavMeshParams * pParams = g_SourceNavMeshes[m];
		if(pParams->m_iStartSectorX == iSectorX && pParams->m_iStartSectorY == iSectorY)
			return pParams;
	}
	return NULL;
}

TNavMeshParams * CNavMeshMaker::InitNavMeshParams(int iSectorX, int iSectorY, char * pNavMeshName)
{
	// note - using rage_new instead of new here, since this is called from the heightmap tool
	TNavMeshParams * pMeshParams = rage_new TNavMeshParams();

	pMeshParams->m_iStartSectorX = iSectorX;
	pMeshParams->m_iStartSectorY = iSectorY;

	// Depending upon what MODE we're currently in, replace/append file extension with appropriate type
	const char * pDefaultBaseName = "navmesh";

	// If we're building navmeshes for aerial navigation, use names like "heightmesh[..][..]" instead
	// of "navmesh[..][..]". The exception is when we first generate them, where we still store the name
	// as "navmesh[..][..]" here so we can find the regular .tri files, just changing the output name.
	if(g_bAerialNavigation && g_Mode != GenerateMeshes)
	{
		pDefaultBaseName = g_AerialMeshName;
	}

	const char * pExt = GetFileExtensionForMode();			
	char filenameNoExtension[256];

	char* filenameNoExtPtr = filenameNoExtension;
	size_t filenameNoExtLen = sizeof(filenameNoExtension);

	// If we have an input path, store it at the beginning of the text buffer, updating
	// the pointer and size accordingly.
	if(g_InputPath)
	{
		formatf(filenameNoExtPtr, filenameNoExtLen, "%s/", g_InputPath);
		const size_t written1 = strlen(filenameNoExtPtr);
		filenameNoExtPtr += written1;
		filenameNoExtLen -= written1;
	}

	formatf(filenameNoExtPtr, filenameNoExtLen, "%s", pNavMeshName ? pNavMeshName : pDefaultBaseName);
	const size_t written2 = strlen(filenameNoExtPtr);
	filenameNoExtPtr += written2;
	filenameNoExtLen -= written2;

	// Append the sector to the name.
	if(!pNavMeshName)
	{
		formatf(filenameNoExtPtr, filenameNoExtLen, "[%i][%i]", pMeshParams->m_iStartSectorX, pMeshParams->m_iStartSectorY);
	}

	// Add the extension, to a separate string.
	char filename[256];
	formatf(filename, "%s.%s", filenameNoExtension, pExt);

	Assert(g_fMaxHeightChangeUnderNavMeshTriEdge > 0.0f);	// Make sure this got set properly from the configuration file or some other way.
	Assert(g_fMinimumVerticalDistanceBetweenNodes > 0.0f);	// Same thing here.

	pMeshParams->m_pNonMapNavMeshName = _strdup(pNavMeshName);
	pMeshParams->m_pFileName = _strdup(filename);
	pMeshParams->m_pFileNameWithoutExtension = _strdup(filenameNoExtension);
	pMeshParams->m_fSamplingResolution = g_fSampleResolution;
	pMeshParams->m_fMaxHeightChangeUnderTriangleEdge = g_fMaxHeightChangeUnderNavMeshTriEdge;
	pMeshParams->m_fMinZDistBetweenSamples = g_fMinimumVerticalDistanceBetweenNodes;
	pMeshParams->m_fLowWallCoverPointStepSize = fwNavGenConfig::Get().m_CoverLowWallPointSpacingAlongEdges;
	pMeshParams->m_bCullBehindWalls = true;
	pMeshParams->m_bJitterSamples = false;

	return pMeshParams;
}

void CNavMeshMaker::InitMeshFilenames()
{
	int iNumX = CPathServerExtents::m_iNumNavMeshesInX;
	int iNumY = CPathServerExtents::m_iNumNavMeshesInY;

	for(int y=0; y<iNumY; y++)
	{
		for(int x=0; x<iNumX; x++)
		{
			TNavMeshParams * pMeshParams = InitNavMeshParams(x*CPathServerExtents::m_iNumSectorsPerNavMesh, y*CPathServerExtents::m_iNumSectorsPerNavMesh);
			g_SourceNavMeshes.push_back(pMeshParams);
		}
	}
}

// NAME : ReadCmdFile
// PURPOSE : Reads a command list file (.lst)
// This file lists names of navmeshes to be processed, accompanied by optional per-mesh parameters
bool CNavMeshMaker::ReadCmdFile()
{
	int iNonMapSectorIndex = 1;

	char textBuf[256];
	char fileNameBuf[256];
	char nonMapNavMeshName[256];

	FILE * filePtr = fopen(g_CommandFileName, "rt");

	if(!filePtr)
	{
		sprintf(textBuf, "Note : couldn't open command file \"%s\".\n", g_CommandFileName);
		OutputText(textBuf);
		return true;
	}

	do
	{
		char * pRet = fgets(fileNameBuf, 256, filePtr);
		if(!pRet)
			break;

		if(fileNameBuf[0]=='#' || fileNameBuf[0]=='\t' || fileNameBuf[0]==10)
			continue;

		// Find which navmesh this is
		int iX = -1, iY = -1;
		char * pIndexStart = strstr(fileNameBuf, "[");
		if(!pIndexStart)
			continue;

		int iNumScanned = sscanf(pIndexStart, "[%i][%i]", &iX, &iY);

		if(!g_bProcessingNonMapNavmeshes)
		{
			if(iNumScanned==2 && g_bOnlyNamedFiles)
			{
				u32 n;
				for(n=0; n<g_SourceNavMeshes.size(); n++)
				{
					TNavMeshParams * pNavParams = g_SourceNavMeshes[n];
					if(pNavParams->m_iStartSectorX==iX && pNavParams->m_iStartSectorY==iY)
						break;
				}
				// If we don't already have this entry, then create a new one.
				if(n==g_SourceNavMeshes.size())
				{
					TNavMeshParams * pMeshParams = InitNavMeshParams(iX, iY);
					g_SourceNavMeshes.push_back(pMeshParams);
				}
			}
		}
		else
		{
			int iNumScanned = sscanf(pIndexStart, "[%s]", &nonMapNavMeshName);

			if(iNumScanned==1)
			{
				if(nonMapNavMeshName[strlen(nonMapNavMeshName)-1]==']')
					nonMapNavMeshName[strlen(nonMapNavMeshName)-1] = 0;

				iX = iNonMapSectorIndex;
				iY = iNonMapSectorIndex;

				TNavMeshParams * pMeshParams = InitNavMeshParams(iX, iY, nonMapNavMeshName);
				g_SourceNavMeshes.push_back(pMeshParams);
			}
		}

		for(u32 n=0; n<g_SourceNavMeshes.size(); n++)
		{
			TNavMeshParams * pNavParams = g_SourceNavMeshes[n];

			bool bFoundNavMesh;
			if(g_bProcessingNonMapNavmeshes)
			{
				bFoundNavMesh = (strcmp(nonMapNavMeshName, pNavParams->m_pNonMapNavMeshName)==0);
			}
			else
			{
				bFoundNavMesh = false;
				if(pNavParams->m_iStartSectorX==iX && pNavParams->m_iStartSectorY==iY)
				{
					// If "heightmesh[..][..]" is used in the cmdfile, only use this line
					// if building navmeshes for aerial navigation. Conversely, if it's something
					// else, don't use it if building heightmeshes.
					if(strncmp(fileNameBuf, "heightmesh[", 11) == 0)
					{
						bFoundNavMesh = g_bAerialNavigation;
					}
					else
					{
						bFoundNavMesh = !g_bAerialNavigation;
					}
				}
			}

			if(bFoundNavMesh)
			{
				//**********************************************************************
				// Look across the line to see if there's a "-res F" token.
				// This can be specified to override the default m_fSamplingResolution
				// on a per-navmesh basis, allowing us to deal with intricate map areas
				// individually without forcing a higher (& therefore more costly)
				// sampling across the entire map.

				if(!g_ResolutionAreasFileName)
				{
					char * pSamplesResPtr = strstr(fileNameBuf, "-res");
					if(pSamplesResPtr)
					{
						float fRes = g_fSampleResolution;
						int iNumRead = sscanf(pSamplesResPtr, "-res %f", &fRes);
						if(iNumRead==1)
						{
							pNavParams->m_fSamplingResolution = fRes; //g_fSizeOfNavMesh / (float)iNumSamples;
						}
					}
				}

				//************************************************************************
				// Look across the line to see if there's a "-maxheightchange N" token.
				// This allows us to override the m_fMaxHeightChangeUnderEdge variable,
				// to let us be more tolerant on steps & stairs on a per-navmesh basis
				// without raising the tolerance value across the whole map & thereby
				// causing problems with low walls, benches and other features.
				// (remember the artists are meant to have a 25cm max step height, so its
				// best to fix the source of the problem generally)

				char * pMaxHeightChangePtr = strstr(fileNameBuf, "-maxheightchange");
				if(pMaxHeightChangePtr)
				{
					float fMaxHeightChange;
					int iNumRead = sscanf(pMaxHeightChangePtr, "-maxheightchange %f", &fMaxHeightChange);
					if(iNumRead==1)
					{
						pNavParams->m_fMaxHeightChangeUnderTriangleEdge = fMaxHeightChange;
					}
				}

				//************************************************************************
				//	Search for "-nodesminzdist".  This will override any value for this
				//	parameter which has been passed in on the command-line.
				//	This token controls the minimum Z height difference between sample-
				//	points with the same XY coordinate.

				char * pMinZDistPtr = strstr(fileNameBuf, "-nodesminzdist");
				if(pMinZDistPtr)
				{
					float fMinVertDist;
					int iNumRead = sscanf(pMinZDistPtr, "-nodesminzdist %f", &fMinVertDist);
					if(iNumRead==1)
					{
						pNavParams->m_fMinZDistBetweenSamples = fMinVertDist;
					}
				}

				//************************************************************************
				// Search for an occurance of "-nocullbehindwalls" which turns off the
				// navmesh poly culling for polys which appear to be behind walls.
				// Sometimes this can return false positives & then we end up with missing
				// polys.

				char * pNoCullPtr = strstr(fileNameBuf, "-nocullbehindwalls");
				if(pNoCullPtr)
				{
					pNavParams->m_bCullBehindWalls = false;
				}

				//*******************************************************************
				// Search for specification of a hand authored mesh for this entry
				char * pAuthoredMeshPtr = strstr(fileNameBuf, "-authoredmesh");
				if(pAuthoredMeshPtr)
				{
					char authoredMeshName[512];
					int iNumRead = sscanf(pAuthoredMeshPtr, "-authoredmesh %s", &authoredMeshName);
					if(iNumRead==1)
					{
						pNavParams->m_pAuthoredMeshFileName = _strdup(authoredMeshName);
					}
				}

				//***************************************************************************
				// Search for an occurance of "-jittersamples" which enables a mode for this
				// navmesh generation, in which a number of samples are taken to when placing
				// nodes before triangulation.  This helps to overcome problems caused by
				// artists leaving gaps between polygons - which most commonly happens on
				// stairs where the steps don't form a continuous surface when seen directly
				// from above.  Unfortunately, explaining why this is a problem to artists
				// doesn't stop them from doing it..

				char * pJitterPtr = strstr(fileNameBuf, "-jittersamples");
				if(pJitterPtr)
				{
					pNavParams->m_bJitterSamples = true;
				}

				//************************************************************************
				// Look across the line to see if there's a "-maxheightchange N" token.
				// This allows us to override the m_fMaxHeightChangeUnderEdge variable,
				// to let us be more tolerant on steps & stairs on a per-navmesh basis
				// without raising the tolerance value across the whole map & thereby
				// causing problems with low walls, benches and other features.
				// (remember the artists are meant to have a 25cm max step height, so its
				// best to fix the source of the problem generally)

				char * pCvrSpacingPtr = strstr(fileNameBuf, "-coverspacing");
				if(pCvrSpacingPtr)
				{
					float fLowWallCoverCheckSpacing;
					int iNumRead = sscanf(pCvrSpacingPtr, "-coverspacing %f", &fLowWallCoverCheckSpacing);
					if(!iNumRead)
					{
						pNavParams->m_fLowWallCoverPointStepSize = fLowWallCoverCheckSpacing;
					}
				}

				// TODO : allow us to have multiple slice-planes on the cmd line
				char * pSlicePlanePtr = strstr(fileNameBuf, "-sliceplane");
				if(pSlicePlanePtr)
				{
					float fSlicePlaneZ;
					int iNumRead = sscanf(pSlicePlanePtr, "-sliceplane %f", &fSlicePlaneZ);
					if(!iNumRead)
					{
						CSlicePlane * slicePlane = new CSlicePlane();
						slicePlane->m_vNormal = Vector3(0.0f,0.0f,1.0f);
						slicePlane->m_fDist = -fSlicePlaneZ;
						pNavParams->m_SlicePlanes.push_back(slicePlane);
					}
				}
			}
		}

		iNonMapSectorIndex++;

	} while(1);

	fclose(filePtr);
	return true;

}

void CNavMeshMaker::ReadTriFileFlags()
{
	u32 m;
	for(m=0; m<g_SourceNavMeshes.size(); m++)
	{
		TNavMeshParams * pNavParams = g_SourceNavMeshes[m];
		pNavParams->m_iTriFileFlags = CNavGen::ScanCollisionFileFlags(pNavParams->m_pFileName, pNavParams->m_bTriFileExists);
	}
}

// TODO: Finish this off
// It is supposed to clip & combine multiple collision files (each smaller than an actual navmesh)
void CNavMeshMaker::CombineCollisionFiles()
{
	/*
	int x,y;
	u32 s,p;

	const int iSectorStep = CPathServerExtents::m_iNumSectorsPerNavMesh;

	char sectionFileTitle[256];
	char sectionFileName[256];

	for(y=0; y<CPathServerExtents::GetWorldDepthInSectors(); y+=iSectorStep)
	{
		for(x=0; x<CPathServerExtents::GetWorldWidthInSectors(); x+=iSectorStep)
		{
			CNavGen * pNavGens = new CNavGen[4];

			Vector3 vSectorMins, vSectorMaxs, vWorldMin, vWorldMax;
			Vector2 vMapSectorsMin;
			int iSectorMin, iSectorMax, iNumSectorsX, iNumSectorsY;

			for(s=0; s<4; s++)
			{
				sprintf(sectionFileTitle, "navmesh[%i][%i]-%i.tri", x, y, s);
				sprintf(sectionFileName, "%s/%s", g_InputPath, sectionFileTitle);

				pNavGens[s].LoadCollisionTriangles(sectionFileName, vSectorMins, vSectorMaxs, iSectorMin, iSectorMax, iNumSectorsX, iNumSectorsY, vWorldMin, vWorldMax, vMapSectorsMin);
			}

			for(s=0; s<4; s++)
			{
				//pNavGens[s].ClipCollisionTriangles();
			}

			CNavGen * pCombinedNavGen = new CNavGen();

			for(s=0; s<4; s++)
			{
				for(p=0; p<pNavGens[s].m_CollisionTriangles.size(); p++)
				{
					CNavGenTri * pTri = pNavGens[s].m_CollisionTriangles[p];

				}

			}

		}
	}
	*/
}

// Given an input set of ".tri" files, we might not want to compile navmeshes for all of them
// since many could just consist of the water surface.  We want to compile meshes which are close
// to land, but not those which exist in the middle of the sea.
// This function dilates the set of navmeshes which are marked with "m_bGenerateSurfaceForThisFile".

void CNavMeshMaker::MarkWhichFilesToCompile()
{
	if(g_bProcessingNonMapNavmeshes)
	{
		for(u32 i = 0; i < g_SourceNavMeshes.size(); i++)
		{
			TNavMeshParams* pParams = g_SourceNavMeshes[i];
			if(pParams->m_bTriFileExists)
			{
				if(pParams->m_iTriFileFlags & NAVMESH_EXPORTFLAG_HAS_COLLISION_MESH)
				{
					pParams->m_bGenerateSurfaceForThisFile = true;
				}
			}
		}
	}
	else
	{
		const int iSectorStep = CPathServerExtents::m_iNumSectorsPerNavMesh;

		int x,y;

		//---------------------------------------------------------------------------------------------
		// Mark for generation, all navmesh cells which have collision or are adjacent to collision.

		for(y=0; y<CPathServerExtents::GetWorldDepthInSectors(); y+=iSectorStep)
		{
			for(x=0; x<CPathServerExtents::GetWorldWidthInSectors(); x+=iSectorStep)
			{
				TNavMeshParams * pCentralMesh = GetSourceFileByCoords(x, y);

				if(pCentralMesh)
				{
					bool bNearCollision = false;

					if(pCentralMesh->m_iTriFileFlags & NAVMESH_EXPORTFLAG_HAS_COLLISION_MESH)
					{
						bNearCollision = true;
					}
					else
					{
						TNavMeshParams * pNegX = GetSourceFileByCoords(x - iSectorStep, y);
						TNavMeshParams * pPosX = GetSourceFileByCoords(x + iSectorStep, y);
						TNavMeshParams * pNegY = GetSourceFileByCoords(x, y - iSectorStep);
						TNavMeshParams * pPosY = GetSourceFileByCoords(x, y + iSectorStep);

						if(pNegX && (pNegX->m_iTriFileFlags & NAVMESH_EXPORTFLAG_HAS_COLLISION_MESH))
							bNearCollision = true;
						else if(pPosX && (pPosX->m_iTriFileFlags & NAVMESH_EXPORTFLAG_HAS_COLLISION_MESH))
							bNearCollision = true;
						else if(pNegY && (pNegY->m_iTriFileFlags & NAVMESH_EXPORTFLAG_HAS_COLLISION_MESH))
							bNearCollision = true;
						else if(pPosY && (pPosY->m_iTriFileFlags & NAVMESH_EXPORTFLAG_HAS_COLLISION_MESH))
							bNearCollision = true;
					}

					if(bNearCollision)
					{
						pCentralMesh->m_bGenerateSurfaceForThisFile = true;
						pCentralMesh->m_bNearCollision = true;
					}
				}
			}
		}

		//----------------------------------------------------------------------------------------------------------------
		// Dilate the area by one:
		// For all navmesh cells which not marked for generation, and are adjacent to an area marked as 'near collision'
		// set them for generation of navmesh.

		/*

		for(y=0; y<CPathServerExtents::GetWorldDepthInSectors(); y+=iSectorStep)
		{
			for(x=0; x<CPathServerExtents::GetWorldWidthInSectors(); x+=iSectorStep)
			{
				TNavMeshParams * pCentralMesh = GetSourceFileByCoords(x, y);
				if(pCentralMesh && !pCentralMesh->m_bGenerateSurfaceForThisFile)
				{
					TNavMeshParams * pNegX = GetSourceFileByCoords(x - iSectorStep, y);
					TNavMeshParams * pPosX = GetSourceFileByCoords(x + iSectorStep, y);
					TNavMeshParams * pNegY = GetSourceFileByCoords(x, y - iSectorStep);
					TNavMeshParams * pPosY = GetSourceFileByCoords(x, y + iSectorStep);

					bool bNearCollision = false;

					if(pNegX && pNegX->m_bNearCollision)
						bNearCollision = true;
					else if(pPosX && pPosX->m_bNearCollision)
						bNearCollision = true;
					else if(pNegY && pNegY->m_bNearCollision)
						bNearCollision = true;
					else if(pPosY && pPosY->m_bNearCollision)
						bNearCollision = true;

					if(bNearCollision)
					{
						pCentralMesh->m_bGenerateSurfaceForThisFile = true;
					}
				}
			}
		}
		*/
	}

	OutputText("\n\n--- Summary of which navmeshes will be compiled/skipped ---\n\n");

	char tmp[512];
	int iCompileCount = 0;
	int iTotalCount = 0;
	for(u32 m=0; m<g_SourceNavMeshes.size(); m++)
	{
		TNavMeshParams * pParams = g_SourceNavMeshes[m];

		char * pStatus = pParams->m_bTriFileExists ?
			pParams->m_bGenerateSurfaceForThisFile ?
				"compiled" : "skipped" : "none";

		sprintf(tmp, "%s - %s\n", pParams->m_pFileName, pStatus);
			

		if(pParams->m_bGenerateSurfaceForThisFile)
			iCompileCount++;
		if(pParams->m_bTriFileExists)
			iTotalCount++;

		OutputText(tmp);
	}

	sprintf(tmp, "\n--- (%i / %i) navmeshes will be compiled ---\n\n", iCompileCount, iTotalCount);
	OutputText(tmp);
}


// Replace the last occurrence of pSearchStr in strInOut (max size strBuffSize), with pReplaceStr.
bool sReplaceStringLastOccurrence(char* strInOut, int strBuffSize, const char* pSearchStr, const char* pReplaceStr)
{
	char* pLastFound = NULL;
	char* pSearchPtr = strInOut;
	while(1)
	{
		char* pStrFound = strstr(pSearchPtr, pSearchStr);
		if(pStrFound)
		{
			pLastFound = pStrFound;
			pSearchPtr = pStrFound + strlen(pSearchStr);
		}
		else
		{
			break;
		}
	}

	if(pLastFound)
	{
		char temp[256];
		safecpy(temp, pLastFound + strlen(pSearchStr), sizeof(temp));

		formatf(pLastFound, strBuffSize - (pLastFound - strInOut), "%s%s", pReplaceStr, temp);
		return true;
	}
	return false;
}


//----------------------------------------------------------------------------
// FUNCTION : GenerateNavMeshes
// PURPOSE : Samples, triangulates & optimises all the navmeshes for the map
// Distributes work via a thread pool

bool CNavMeshMaker::GenerateNavMeshes()
{
	char textBuf[256];
//	char textProgress[256];

	time_t sysTime;
	tm * localTime;
	time(&sysTime);
	localTime = localtime(&sysTime);
	sprintf(textBuf, "STARTED AT : %s\n\n", asctime(localTime));
	OutputText(textBuf);

	CGenerationThreadPool * pThreadPool = new CGenerationThreadPool();
	pThreadPool->Init(g_iMaxNumThreads);

	sprintf(textBuf, "Utilising %i threads\n\n", g_iMaxNumThreads);
	OutputText(textBuf);

	float fTotalTimeTaken = 0.0f;
	float fTotalTimeToPlaceNodes = 0.0f;
	float fTotalTimeToTriangulate = 0.0f;
	float fTotalTimeToOptimize = 0.0f;


	if(g_ResolutionAreasFileName)
	{
		CNavGen::LoadCustomResolutionAreas(g_ResolutionAreasFileName);
	}

	if(g_bGeometryProximity)
	{
		// Read all the flags for the tri files
		ReadTriFileFlags();

		// Decide which ones we will compile.
		MarkWhichFilesToCompile();
	}

	u32 m;
	for(m=0; m<g_SourceNavMeshes.size(); m++)
	{
		TNavMeshParams * pNavParams = g_SourceNavMeshes[m];

		// Skip files which are not marked for compiling.
		// This is so that we can avoid compiling navmeshes for huge empty expanses of water far from any geometry.
		if(g_bGeometryProximity && !pNavParams->m_bGenerateSurfaceForThisFile)
		{
			continue;
		}

		// Hand authored meshes are not sampled/generated
		if(pNavParams->m_pAuthoredMeshFileName)
		{
			continue;
		}

		CNavGen * navGen = new CNavGen();

		Vector3 vSectorMin;
		Vector3 vSectorMax;

		char tmp[256];
		strcpy(tmp, pNavParams->m_pFileName);
		strlwr(tmp);

		if(!CNavGen::LoadCollisionFileExtents(pNavParams->m_pFileName, vSectorMin, vSectorMax))
		{
			continue;
		}

		const char* pSearchName = "navmesh";
		const char* pReplaceName = g_AerialMeshName;

		char srfFileName[256];
		strcpy(srfFileName, tmp);
		if(g_bAerialNavigation)
		{
			sReplaceStringLastOccurrence(srfFileName, sizeof(srfFileName), pSearchName, pReplaceName);
		}
		char * pExtPos = strstr(srfFileName, ".tri");

		pExtPos[1] = 's';
		pExtPos[2] = 'r';
		pExtPos[3] = 'f';
		pExtPos[4] = '\0';


		u32 iTriFileCRC = 0;
		u32 iResAreasCRC = 0;
		u32 iSrfFileCollisionCRC = 0;
		u32 iSrfFileResAreasCRC = 0;

		iTriFileCRC = CalcFileCRC(pNavParams->m_pFileName);
		iResAreasCRC = CalcResAreasCRC(CNavGen::m_ResolutionAreasWholeMap, vSectorMin, vSectorMax);

		if(ReadSrfFileCRCs(srfFileName, iSrfFileCollisionCRC, iSrfFileResAreasCRC))
		{
			if(iSrfFileCollisionCRC==iTriFileCRC && iSrfFileResAreasCRC==iResAreasCRC)
			{
				// It looks like the existing SRF file was made from the current TRI file (stored CRC matches the TRI file's CRC)
				// Also the res-areas must match.
				// We can skip processing this TRI file, and continue on until we find one which has changed.
				continue;
			}
		}


		pNavParams->m_iTriFileCRC = iTriFileCRC;
		pNavParams->m_iResAreasCRC = iResAreasCRC;
		if(g_bAerialNavigation)
		{
			pNavParams->m_pSearchName = _strdup(pSearchName);
			pNavParams->m_pReplaceName = _strdup(pReplaceName);
		}


		Vector3 vWorldMin;
		Vector3 vWorldMax;
		Vector2 vMapMin;
		int iSectorStartX;
		int iSectorStartY;
		int iSectorNumX;
		int iSectorNumY;

		bool bLoadedOk = navGen->LoadCollisionTriangles(
			pNavParams->m_pFileName,
			vSectorMin, vSectorMax,
			iSectorStartX, iSectorStartY,
			iSectorNumX, iSectorNumY,
			vWorldMin, vWorldMax,
			vMapMin);

		if(bLoadedOk)
		{
			CPathServerExtents::m_vWorldMin.x = vMapMin.x;
			CPathServerExtents::m_vWorldMin.y = vMapMin.y;

			CPathServerExtents::m_vWorldMax.x =
				CPathServerExtents::m_vWorldMin.x +
					(CPathServerExtents::m_iNumNavMeshesInX * CPathServerExtents::m_iNumSectorsPerNavMesh * CPathServerExtents::GetWorldWidthOfSector());

			CPathServerExtents::m_vWorldMax.y =
				CPathServerExtents::m_vWorldMin.y +
					(CPathServerExtents::m_iNumNavMeshesInY * CPathServerExtents::m_iNumSectorsPerNavMesh * CPathServerExtents::GetWorldWidthOfSector());
		}

		if(!bLoadedOk)
			continue;

		//----------------------------------------

		CNavGen* pNeighborNavGens[4] = { NULL, NULL, NULL, NULL };
		int numNeighborNavGens = 0;

		if(g_bAerialNavigation)
		{
			s32 meshX, meshY;
			/*int meshIndex =*/ CNavGen_NavMeshStore::GetNavMeshIndexFromFilename(pNavParams->m_pFileName, &meshX, &meshY);

			// TODO: Think more about how to load up the neighboring navmeshes. There is some code for it
			// already for the regular -analyze phase, but it would take some refactoring to be able to
			// reuse it, and things generally got messier when I tried it - should be faster though, by not
			// having to reload the same data repeatedly. /FF

			Vector3 vSectorMinTemp, vSectorMaxTemp;
			int iSectorStartXTemp, iSectorStartYTemp;
			int iSectorNumXTemp, iSectorNumYTemp;
			Vector3 vWorldMinTemp, vWorldMaxTemp;
			Vector2 vMapMinTemp;

			for(int y = -1; y <= 1; y++)
			{
				for(int x = -1; x <= 1; x++)
				{
					if(x == 0 && y == 0)
					{
						continue;
					}
					if(x != 0 && y != 0)
					{
						continue;
					}

					char neighborFileName[256];
					formatf(neighborFileName, "%s/navmesh[%d][%d].tri", g_InputPath, meshX + x*CPathServerExtents::m_iNumSectorsPerNavMesh, meshY + y*CPathServerExtents::m_iNumSectorsPerNavMesh);

					CNavGen* pNeighborNavGen = rage_new CNavGen;
					bool loaded = pNeighborNavGen->LoadCollisionTriangles(neighborFileName, vSectorMinTemp, vSectorMaxTemp, iSectorStartXTemp, iSectorStartYTemp,
							iSectorNumXTemp, iSectorNumYTemp, vWorldMinTemp, vWorldMaxTemp, vMapMinTemp);

					if(loaded)
					{
						// Set up generation params
						CNavGenParams params;
						params.m_vAreaMin = vWorldMinTemp;
						params.m_vAreaMax = vWorldMaxTemp;
						params.m_vUpVector = Vector3(0, 0, 1.0f);
						params.m_vSectorMins = vSectorMinTemp;
						params.m_vSectorMaxs = vSectorMaxTemp;
						params.m_fDistanceToInitiallySeedNodes = pNavParams->m_fSamplingResolution;
						params.m_MaxHeightChangeUnderTriangleEdge = pNavParams->m_fMaxHeightChangeUnderTriangleEdge;
						params.m_bAlsoStoreSplitPolysInOctreeLeaves = false;
						params.m_bJitterSamplesWhenCreatingNodes = pNavParams->m_bJitterSamples;
						params.m_bCreateNavmeshUnderwater = g_bCreateNavMeshUnderwater;
						params.m_bAerialNavigation = g_bAerialNavigation;
						pNeighborNavGen->Init(&params);
						pNeighborNavGen->InitOctree();

						Assert(numNeighborNavGens < NELEM(pNeighborNavGens));
						pNeighborNavGens[numNeighborNavGens++] = pNeighborNavGen;
					}
					else
					{
						delete pNeighborNavGen;
					}
				}
			}
		}

		if(!navGen->m_CollisionTriangles.size() && !navGen->m_bHasWater)
			continue;

		Assert(pNavParams->m_fMinZDistBetweenSamples > 0.0f);

		// Given the input sampling resolution, calculate the distance between each node..
		float fMeshSize = (float) ((int)(vSectorMax.x - vSectorMin.x));
		float fNumSamples = (float) ((int)(fMeshSize / pNavParams->m_fSamplingResolution));
		float fSampleSpacing = fMeshSize / fNumSamples;

		// ..but if we are using custom resolution areas, revert to the default 1m sampling frequency
		if(g_ResolutionAreasFileName)
		{
			fNumSamples = fMeshSize;
			fSampleSpacing = 1.0f;
		}

		// Set up generation params
		const fwNavGenConfig& cfg = fwNavGenConfig::Get();

		CNavGenParams params;
		params.m_vAreaMin = vWorldMin;
		params.m_vAreaMax = vWorldMax;
		params.m_vUpVector = Vector3(0, 0, 1.0f);
		params.m_vSectorMins = vSectorMin;
		params.m_vSectorMaxs = vSectorMax;
		params.m_fDistanceToInitiallySeedNodes = fSampleSpacing;
		params.m_MaxHeightChangeUnderTriangleEdge = pNavParams->m_fMaxHeightChangeUnderTriangleEdge;
		params.m_bAlsoStoreSplitPolysInOctreeLeaves = false;
		params.m_bJitterSamplesWhenCreatingNodes = pNavParams->m_bJitterSamples;
		params.m_bCreateNavmeshUnderwater = g_bCreateNavMeshUnderwater;
		params.m_bAerialNavigation = g_bAerialNavigation;
		params.m_fMinZDistBetweenSamples = pNavParams->m_fMinZDistBetweenSamples;
		params.m_TriangulationMaxHeightDiff = cfg.m_TriangulationMaxHeightDiff;
		params.m_OptimizeMaxQuadricErrorMetric = cfg.m_OptimizeMaxQuadricErrorMetric;

		if(g_bAerialNavigation)
		{
			params.m_fMaxTriangleSideLength = cfg.m_AerialMaxTriangleSideLength;
			params.m_fMaxTriangleArea = cfg.m_AerialMaxTriangleArea;
		}

		if(!navGen->m_CollisionTriangles.size())
		{
			params.m_fDistanceToInitiallySeedNodes = CPathServerExtents::GetSizeOfNavMesh() / 25.0f;
			params.m_fMaxTriangleSideLength = 32.0f;
			params.m_fMaxTriangleArea = 160.0f;
		}


		// Add slice planes
		for(u32 sp=0; sp<pNavParams->m_SlicePlanes.size(); sp++)
		{
			navGen->m_SlicePlanes.push_back(pNavParams->m_SlicePlanes[sp]);
		}

		//---------------------------
		// Init the navmesh gen class

		navGen->Init(&params);

		//---------------------------
		// Set global overrides

		if(g_bGlobalIgnoreSteepness)
			navGen->m_bIgnoreSteepness = true;

		if(g_fOverrideTestClearHeightInTriangulation >= 0.0f)
			navGen->m_fTestClearHeightInTriangulation = g_fOverrideTestClearHeightInTriangulation;

		//-----------------------------------------------------------------------------

		CreateDynamicResolutionAreasForNavMesh(navGen, iSectorStartX, iSectorStartY);

		//-----------------------------------------------------------------------------

		CGenerationWorkerThread * pFreeThread = (CGenerationWorkerThread*) pThreadPool->GetNextFree();

		while(pFreeThread == NULL)
		{
			CGenerationWorkerThread * pCompletedThread = (CGenerationWorkerThread*) pThreadPool->GetNextComplete();

			while(pCompletedThread != NULL)
			{
				pCompletedThread->SaveData();

				pCompletedThread->Shutdown();

				pCompletedThread = (CGenerationWorkerThread*) pThreadPool->GetNextComplete();
			}

			sysIpcYield(1000);

			pFreeThread = (CGenerationWorkerThread*) pThreadPool->GetNextFree();
		}

		Assert( !pFreeThread->IsRunning() );

		pFreeThread->Init();
		pFreeThread->Run(pNavParams, navGen, pNeighborNavGens, numNeighborNavGens, g_bAerialNavigation);
	}

	// Complete any remaining worker threads
	while(pThreadPool->GetNextRunning() || pThreadPool->GetNextComplete())
	{
		CGenerationWorkerThread * pCompletedThread = (CGenerationWorkerThread*) pThreadPool->GetNextComplete();

		while(pCompletedThread != NULL)
		{
			pCompletedThread->SaveData();

			pCompletedThread->Shutdown();

			pCompletedThread = (CGenerationWorkerThread*) pThreadPool->GetNextComplete();
		}

		sysIpcYield(0);
	}

	time(&sysTime);
	localTime = localtime(&sysTime);
	sprintf(textBuf, "ENDED AT : %s\n\n", asctime(localTime));
	OutputText(textBuf);

	sprintf(textBuf, "TOTAL TIME TAKEN : %.1f minutes\n", fTotalTimeTaken / 60.0f);
	OutputText(textBuf);
	sprintf(textBuf, "TOTAL TIME TO PLACE NODES : %.1f minutes\n", fTotalTimeToPlaceNodes / 60.0f);
	OutputText(textBuf);
	sprintf(textBuf, "TOTAL TIME TO TRIANGULATE : %.1f minutes\n", fTotalTimeToTriangulate / 60.0f);
	OutputText(textBuf);
	sprintf(textBuf, "TOTAL TIME TO OPTIMIZE : %.1f minutes\n", fTotalTimeToOptimize / 60.0f);
	OutputText(textBuf);

	OutputText("\n");


	pThreadPool->Shutdown();
	delete pThreadPool;

	return true;
}

bool CNavMeshMaker::CreateDynamicResolutionAreasForNavMesh(CNavGen * pNavGen, s32 iSectorStartX, s32 iSectorStartY)
{
	//-----------------------------------------------------------------------------------
	// Load the list of dynamic entities intersecting this navmesh
	// This is used to automatically create increased resolution areas for door objects

	const bool bLoadLadders = __AUTO_CREATE_RESOLUTION_AREAS_FOR_LADDERS;
	const bool bLoadEntities = __AUTO_CREATE_RESOLUTION_AREAS_FOR_DOORS;

	if(!bLoadLadders && !bLoadEntities)
		return false;

	CDynamicEntityBoundsArray dynamicEntities;
	vector<fwNavLadderInfo> laddersList;

	char extrasFileName[1024];
	sprintf(extrasFileName, "%s/Extras_%i_%i.dat", g_InputPath, iSectorStartX, iSectorStartY);
	CNavGen::ParseExtraInfoFile(extrasFileName, bLoadLadders ? &laddersList : NULL, NULL, NULL, bLoadEntities ? &dynamicEntities.m_Bounds : NULL);

#if __AUTO_CREATE_RESOLUTION_AREAS_FOR_DOORS

	for(u32 e=0; e<dynamicEntities.m_Bounds.size(); e++)
	{
		if((dynamicEntities.m_Bounds[e]->m_iFlags & fwDynamicEntityInfo::FLAG_DOOR)!=0)
		{
			CNavResolutionArea * pArea = new CNavResolutionArea();
			pArea->m_bDynamicallyCreated = true;
			dynamicEntities.m_Bounds[e]->m_MinMax.GetAsFloats(pArea->m_vMin, pArea->m_vMax);
			pArea->m_fMultiplier = 0.5f;
			pNavGen->m_ResolutionAreasCurrentNavMesh.push_back(pArea);
		}
	}
#endif // __AUTO_CREATE_RESOLUTION_AREAS_FOR_DOORS


#if __AUTO_CREATE_RESOLUTION_AREAS_FOR_LADDERS

	const Vector3 vLadderHalfResArea(2.0f, 2.0f, 1.0f);

	for(u32 l=0; l<laddersList.size(); l++)
	{
		fwNavLadderInfo * pLadderInfo = &laddersList[l];		

		CNavResolutionArea * pAreaTop = new CNavResolutionArea();
		pAreaTop->m_bDynamicallyCreated = true;
		pAreaTop->m_vMin = pLadderInfo->GetTop() - vLadderHalfResArea;
		pAreaTop->m_vMax = pLadderInfo->GetTop() + vLadderHalfResArea;
		pAreaTop->m_fMultiplier = 0.25f;
		pNavGen->m_ResolutionAreasCurrentNavMesh.push_back(pAreaTop);

		CNavResolutionArea * pAreaBottom = new CNavResolutionArea();
		pAreaBottom->m_bDynamicallyCreated = true;
		pAreaBottom->m_vMin = pLadderInfo->GetBase() - vLadderHalfResArea;
		pAreaBottom->m_vMax = pLadderInfo->GetBase() + vLadderHalfResArea;
		pAreaBottom->m_fMultiplier = 0.25f;
		pNavGen->m_ResolutionAreasCurrentNavMesh.push_back(pAreaBottom);

	}
#endif // __AUTO_CREATE_RESOLUTION_AREAS_FOR_LADDERS

	return true;
}

bool CNavMeshMaker::MergeNavMeshes()
{
	char textBuf[512];
	OutputText("Merging polys from .srf files, and creating .inv NavMeshes.\n");

	CMergeThreadPool * pThreadPool = new CMergeThreadPool();
	pThreadPool->Init(g_iMaxNumThreads);

	float fTotalTimeTaken = 0.0f;

	time_t sysTime;
	tm * localTime;

	time(&sysTime);
	localTime = localtime(&sysTime);
	sprintf(textBuf, "STARTED AT : %s\n\n", asctime(localTime));
	OutputText(textBuf);

	if(g_ResolutionAreasFileName)
	{
		CNavGen::LoadCustomResolutionAreas(g_ResolutionAreasFileName);
	}

	u32 m;
	char mrgFileName[1024];

	// Load all the .srf files as nav meshes
	for(m=0; m<g_SourceNavMeshes.size(); m++)
	{
		TNavMeshParams * pNavParams = g_SourceNavMeshes[m];
		//char * pFileName = pNavParams->m_pFileName;
		CNavMeshHolder * pNavMeshHolder = new CNavMeshHolder();

		pNavMeshHolder->m_pColFileName = _strdup(pNavParams->m_pFileName);
		ReplaceExtension(pNavMeshHolder->m_pColFileName, ".tri");

		/*
		See if we have a '.mrg' file for this navmesh already, whose collision-file CRC
		matches the CRC of the .tri collision file in the build directory.
		If so we can skip this stage for this navmesh, as it is already up-to-date.
		*/

		Vector3 vNavMeshMin, vNavMeshMax;

		u32 iCollisionFileCRC = CalcFileCRC(pNavMeshHolder->m_pColFileName);

		if( iCollisionFileCRC == 0)
		{
			continue;
		}

		u32 iResAreasCRC = 0;
		if( CNavGen::LoadCollisionFileExtents(pNavMeshHolder->m_pColFileName, vNavMeshMin, vNavMeshMax) )
		{
			iResAreasCRC = CalcResAreasCRC(CNavGen::m_ResolutionAreasWholeMap, vNavMeshMin, vNavMeshMax);
		}

		bool bCanSkipThisFile = false;
		u32 iSrfFileCollisionCRC = 0;
		u32 iSrfFileResAreasCRC = 0;
		if( ReadSrfFileCRCs(pNavParams->m_pFileName, iSrfFileCollisionCRC, iSrfFileResAreasCRC) )
		{
			sprintf(mrgFileName, "%s.mrg", pNavParams->m_pFileNameWithoutExtension);
			fiStream * pMrgFile = fiStream::Open(mrgFileName, true);
			if(pMrgFile)
			{
				u32 iMrgFileCollisionCRC = 0;
				u32 iMrgFileResAreasCRC = 0;

				pMrgFile->Seek( pMrgFile->Size()-8 );
				if( pMrgFile->ReadInt( &iMrgFileCollisionCRC, 1 ) == 1)
				{
					if( pMrgFile->ReadInt( &iMrgFileResAreasCRC, 1 ) == 1)
					{
						if( iCollisionFileCRC == iMrgFileCollisionCRC &&
							iCollisionFileCRC == iSrfFileCollisionCRC &&
							iResAreasCRC == iMrgFileResAreasCRC &&
							iResAreasCRC == iSrfFileResAreasCRC)
						{
							bCanSkipThisFile = true;
						}
					}
				}
				pMrgFile->Close();
			}

			if(bCanSkipThisFile)
			{
				sprintf(textBuf, "Skipping file \"%s\" .mrg file CRC matches with .srf file CRC", pNavParams->m_pFileName);
				OutputText(g_pBlankLine);
				OutputText(textBuf);

				continue;
			}
		}

		pNavParams->m_iTriFileCRC = pNavMeshHolder->m_iTriFileCRC = iCollisionFileCRC;
		pNavParams->m_iResAreasCRC = pNavMeshHolder->m_iResAreasCRC = iResAreasCRC;


		//-----------------------------------------------------------------------------
		// Assign the merging of this navmesh to one of our worker threads..

		CMergeWorkerThread * pFreeThread = (CMergeWorkerThread*) pThreadPool->GetNextFree();

		while(pFreeThread == NULL)
		{
			CMergeWorkerThread * pCompletedThread = (CMergeWorkerThread*) pThreadPool->GetNextComplete();

			while(pCompletedThread != NULL)
			{
				pCompletedThread->SaveData();

				pCompletedThread->Shutdown();

				pCompletedThread = (CMergeWorkerThread*) pThreadPool->GetNextComplete();
			}

			sysIpcYield(0);

			pFreeThread = (CMergeWorkerThread*) pThreadPool->GetNextFree();
		}

		Assert( !pFreeThread->IsRunning() );

		pFreeThread->Init();
		pFreeThread->Run(pNavParams, pNavMeshHolder->m_vBlockMin, pNavMeshHolder->m_vBlockMax, !g_bProcessingNonMapNavmeshes, g_bAerialNavigation);
	}


	// Complete any remaining worker threads
	while(pThreadPool->GetNextRunning() || pThreadPool->GetNextComplete())
	{
		CMergeWorkerThread * pCompletedThread = (CMergeWorkerThread*) pThreadPool->GetNextComplete();

		while(pCompletedThread != NULL)
		{
			pCompletedThread->SaveData();

			pCompletedThread->Shutdown();

			pCompletedThread = (CMergeWorkerThread*) pThreadPool->GetNextComplete();
		}

		sysIpcYield(0);
	}

	//-----------------------------------------------------------------------------------------------

	OutputText("Clearing up\n");

	// finally, delete them all...
	for(u32 c=0; c<g_NavMeshes.size(); c++)
	{
		CNavMeshHolder * pNavMeshHolder = g_NavMeshes[c];
		delete pNavMeshHolder;
	}
	g_NavMeshes.clear();


	time(&sysTime);
	localTime = localtime(&sysTime);
	sprintf(textBuf, "ENDED AT : %s\n\n", asctime(localTime));
	OutputText(textBuf);

	OutputText("\n");
	sprintf(textBuf, "TOTAL TIME TAKEN : %.1f minutes\n", fTotalTimeTaken / 60.0f);
	OutputText(textBuf);
	OutputText("\n");

	sprintf(textBuf, "MaxVertexPoolSize reached : %i\n", CMergeWorkerThread::ms_iMaxVertexPoolSize);
	OutputText(textBuf);
	sprintf(textBuf, "MaxVertexIndex reached : %i\n", CMergeWorkerThread::ms_iMaxVertexIndex);
	OutputText(textBuf);
	OutputText("\n");

	return true;
}


bool CNavMeshMaker::StitchNavMeshes(void)
{
	char textBuf[256];
	OutputText("Stitching NavMeshes.\n");

	time_t sysTime;
	tm * localTime;

	time(&sysTime);
	localTime = localtime(&sysTime);
	sprintf(textBuf, "STARTED AT : %s\n\n", asctime(localTime));
	OutputText(textBuf);

	float fTotalTimeTaken = 0.0f;
	u32 iStartTime = timeGetTime();

	u32 m,n,n1,n2;

	// Load all the nav meshes
	for(m=0; m<g_SourceNavMeshes.size(); m++)
	{
		TNavMeshParams * pNavParams = g_SourceNavMeshes[m];
		char * pFileName = pNavParams->m_pFileName;

		OutputText(g_pBlankLine);
		sprintf(textBuf, "Loading NavMesh \"%s\"", pFileName);
		OutputText(textBuf);

		CNavMeshHolder * pNavMeshHolder = new CNavMeshHolder();

		CNavMesh * pNavMesh = CNavMesh::LoadBinary(pFileName);

		if(!pNavMesh)
		{
			delete pNavMeshHolder;
			continue;
		}

#ifdef CHECK_NAVMESH_INTEGRITY
		CNavGen::CheckNavMeshIntegrity(pNavMesh);
#endif

		pNavMeshHolder->m_pNavMesh = pNavMesh;
		pNavMeshHolder->m_pNavMeshName = _strdup(pFileName);

		pNavMeshHolder->m_iNavMeshIndex = CNavGen_NavMeshStore::GetNavMeshIndexFromFilename(pNavMeshHolder->m_pNavMeshName, &pNavMeshHolder->m_iNavMeshXPos, &pNavMeshHolder->m_iNavMeshYPos);

		if(pNavMeshHolder->m_iNavMeshIndex==NAVMESH_NAVMESH_INDEX_NONE)
		{
			OutputText("bad filename format.");
		}
		g_NavMeshes.push_back(pNavMeshHolder);
	}

	OutputText("\nIdentifying edge polys\n");

	for(n=0; n<g_NavMeshes.size(); n++)
	{
		CNavMeshHolder * pNavMeshHolder = g_NavMeshes[n];

		if(pNavMeshHolder->m_pNavMeshName)
		{
			OutputText(g_pBlankLine);
			sprintf(textBuf, "> %s", pNavMeshHolder->m_pNavMeshName);
			OutputText(textBuf);
		}

		pNavMeshHolder->m_pNavMesh->IdentifyEdgePolys();
	}

	// Now go through all nav meshes, and perform the stitching operation on all other nav-meshes
	// which we know to be bordering upon them..
	for(n1=0; n1<g_NavMeshes.size(); n1++)
	{
		CNavMeshHolder * pNavMeshHolder1 = g_NavMeshes[n1];
		if(!pNavMeshHolder1->m_pNavMesh)
			continue;

		if(pNavMeshHolder1->m_pNavMeshName)
		{
			OutputText(g_pBlankLine);
			sprintf(textBuf, "(%i/%i) : %s", n1, g_NavMeshes.size(), pNavMeshHolder1->m_pNavMeshName);
			OutputText(textBuf);
		}

		u32 iNavMesh1Index = pNavMeshHolder1->m_pNavMesh->GetIndexOfMesh();

		// Clean out the adjacency for all poly adjacencies onto another nav-mesh.
		// This is so that old info doesn't get retained & we can re-stitch w/o having to resample everything.

		for(u32 p=0; p<pNavMeshHolder1->m_pNavMesh->GetNumPolys(); p++)
		{
			TNavMeshPoly * pPoly = pNavMeshHolder1->m_pNavMesh->GetPoly(p);
			for(u32 v=0; v<pPoly->GetNumVertices(); v++)
			{
				const TAdjPoly & adjPoly = pNavMeshHolder1->m_pNavMesh->GetAdjacentPoly(pPoly->GetFirstVertexIndex()+v);
				// Connecting to another navmesh ?
				const u32 iNavMesh = adjPoly.GetNavMeshIndex(pNavMeshHolder1->m_pNavMesh->GetAdjacentMeshes());
				if(iNavMesh != NAVMESH_NAVMESH_INDEX_NONE && iNavMesh != iNavMesh1Index)
				{
					pNavMeshHolder1->m_pNavMesh->GetAdjacentPolysArray().Get(pPoly->GetFirstVertexIndex() + v)->m_iAdjacentNavMeshIndex = NAVMESH_NAVMESH_INDEX_NONE;
					pNavMeshHolder1->m_pNavMesh->GetAdjacentPolysArray().Get(pPoly->GetFirstVertexIndex() + v)->SetNavMeshIndex(NAVMESH_NAVMESH_INDEX_NONE, pNavMeshHolder1->m_pNavMesh->GetAdjacentMeshes());
					pNavMeshHolder1->m_pNavMesh->GetAdjacentPolysArray().Get(pPoly->GetFirstVertexIndex() + v)->SetPolyIndex(NAVMESH_POLY_INDEX_NONE);
					pNavMeshHolder1->m_pNavMesh->GetAdjacentPolysArray().Get(pPoly->GetFirstVertexIndex() + v)->SetOriginalNavMeshIndex(NAVMESH_NAVMESH_INDEX_NONE, pNavMeshHolder1->m_pNavMesh->GetAdjacentMeshes());
					pNavMeshHolder1->m_pNavMesh->GetAdjacentPolysArray().Get(pPoly->GetFirstVertexIndex() + v)->SetOriginalPolyIndex(NAVMESH_POLY_INDEX_NONE);
				}
			}
		}
	}

	// Now go through all nav meshes, and perform the stitching operation on all other nav-meshes
	// which we know to be bordering upon them..
	// This step actually modifies each navmesh so that it's shared edges exactly match up with
	// polys in the neighbouring meshes.

	int iSectorsPerMesh = CPathServerExtents::m_iNumSectorsPerNavMesh;

	for(n1=0; n1<g_NavMeshes.size(); n1++)
	{
		CNavMeshHolder * pNavMeshHolder1 = g_NavMeshes[n1];
		if(!pNavMeshHolder1->m_pNavMesh)
			continue;

		if(pNavMeshHolder1->m_pNavMeshName)
		{
			OutputText(g_pBlankLine);
			sprintf(textBuf, "(%i/%i) : %s", n1, g_NavMeshes.size(), pNavMeshHolder1->m_pNavMeshName);
			OutputText(textBuf);
		}

		CNavMeshHolder * pNavMesh_MinusX = NULL;
		CNavMeshHolder * pNavMesh_PlusX = NULL;
		CNavMeshHolder * pNavMesh_MinusY = NULL;
		CNavMeshHolder * pNavMesh_PlusY = NULL;

		for(n2=0; n2<g_NavMeshes.size(); n2++)
		{
			if(n1 == n2)
				continue;

			CNavMeshHolder * pNavMeshHolder2 = g_NavMeshes[n2];

			if(!pNavMeshHolder2->m_pNavMesh)
				continue;

			if(pNavMeshHolder2->m_iNavMeshYPos == pNavMeshHolder1->m_iNavMeshYPos &&
				pNavMeshHolder2->m_iNavMeshXPos == pNavMeshHolder1->m_iNavMeshXPos-iSectorsPerMesh)
			{
				pNavMesh_MinusX = pNavMeshHolder2;
			}
			else if(pNavMeshHolder2->m_iNavMeshYPos == pNavMeshHolder1->m_iNavMeshYPos &&
				pNavMeshHolder2->m_iNavMeshXPos == pNavMeshHolder1->m_iNavMeshXPos+iSectorsPerMesh)
			{
				pNavMesh_PlusX = pNavMeshHolder2;
			}
			else if(pNavMeshHolder2->m_iNavMeshXPos == pNavMeshHolder1->m_iNavMeshXPos &&
				pNavMeshHolder2->m_iNavMeshYPos == pNavMeshHolder1->m_iNavMeshYPos-iSectorsPerMesh)
			{
				pNavMesh_MinusY = pNavMeshHolder2;
			}
			else if(pNavMeshHolder2->m_iNavMeshXPos == pNavMeshHolder1->m_iNavMeshXPos &&
				pNavMeshHolder2->m_iNavMeshYPos == pNavMeshHolder1->m_iNavMeshYPos+iSectorsPerMesh)
			{
				pNavMesh_PlusY = pNavMeshHolder2;
			}
		}

		CNavGen::StitchNavMesh(
			pNavMeshHolder1->m_pNavMesh,
			pNavMesh_MinusX ? pNavMesh_MinusX->m_pNavMesh : NULL,
			pNavMesh_PlusX ? pNavMesh_PlusX->m_pNavMesh : NULL,
			pNavMesh_MinusY ? pNavMesh_MinusY->m_pNavMesh : NULL,
			pNavMesh_PlusY ? pNavMesh_PlusY->m_pNavMesh : NULL);
	}



	// Now go through all nav meshes, and perform the simpler stitching operation on all other nav-meshes
	// which we know to be bordering upon them..  Since we've already modified all meshes to ensure they
	// fit nicely, all we have to do here is match polys up across each edge.
	// Note that this takes the poly with the greatest overlap.  This is done becuase *sometimes* we may
	// not be able to ensure polys fit nicely, if we run up against the max_num_vertices for example - we
	// will then not be able to remove a T-junction by inserting a new vertex & edge..

	OutputText("\nIdentifying edge polys again\n");

	for(n=0; n<g_NavMeshes.size(); n++)
	{
		CNavMeshHolder * pNavMeshHolder = g_NavMeshes[n];

		if(pNavMeshHolder->m_pNavMeshName)
		{
			OutputText(g_pBlankLine);
			sprintf(textBuf, "> %s", pNavMeshHolder->m_pNavMeshName);
			OutputText(textBuf);
		}

		pNavMeshHolder->m_pNavMesh->IdentifyEdgePolys();
	}


	for(n1=0; n1<g_NavMeshes.size(); n1++)
	{
		CNavMeshHolder * pNavMeshHolder1 = g_NavMeshes[n1];
		if(!pNavMeshHolder1->m_pNavMesh)
			continue;

		for(n2=0; n2<g_NavMeshes.size(); n2++)
		{
			if(n1 == n2)
				continue;

			CNavMeshHolder * pNavMeshHolder2 = g_NavMeshes[n2];

			if(!pNavMeshHolder2->m_pNavMesh)
				continue;

			// Adjacent in X ?
			if(pNavMeshHolder2->m_iNavMeshYPos == pNavMeshHolder1->m_iNavMeshYPos &&
				(pNavMeshHolder2->m_iNavMeshXPos == pNavMeshHolder1->m_iNavMeshXPos + iSectorsPerMesh ||
				pNavMeshHolder2->m_iNavMeshXPos == pNavMeshHolder1->m_iNavMeshXPos - iSectorsPerMesh))
			{
				pNavMeshHolder1->m_pNavMesh->StitchEdgePolysAcrossMeshes(*pNavMeshHolder2->m_pNavMesh);
			}
			// Adjacent in Y ?
			else if(pNavMeshHolder2->m_iNavMeshXPos == pNavMeshHolder1->m_iNavMeshXPos &&
				(pNavMeshHolder2->m_iNavMeshYPos == pNavMeshHolder1->m_iNavMeshYPos + iSectorsPerMesh ||
				pNavMeshHolder2->m_iNavMeshYPos == pNavMeshHolder1->m_iNavMeshYPos - iSectorsPerMesh))
			{
				pNavMeshHolder2->m_pNavMesh->StitchEdgePolysAcrossMeshes(*pNavMeshHolder1->m_pNavMesh);
			}
		}
	}


	OutputText("\nSaving NavMeshes\n");

	// Now go through and save all the nav meshes, with the ".nav" extension
	char navMeshFileName[256];

	for(n=0; n<g_NavMeshes.size(); n++)
	{
		CNavMeshHolder * pNavMeshHolder = g_NavMeshes[n];

		strcpy(navMeshFileName, pNavMeshHolder->m_pNavMeshName);

		size_t s = strlen(navMeshFileName)-1;
		while(navMeshFileName[s] != '.')
			s--;

		if(navMeshFileName[s] == '.')
		{
			s++;
			navMeshFileName[s++] = 'i';
			navMeshFileName[s++] = 'n';
			navMeshFileName[s++] = g_bAerialNavigation ? 'h' : 'v';

			CNavMesh::SaveBinary(pNavMeshHolder->m_pNavMesh, navMeshFileName);
		}
		else
		{
			OutputText("Error - bad filename\n");
		}
	}

	OutputText("Clearing up\n");

	// finally, delete them all...
	for(n=0; n<g_NavMeshes.size(); n++)
	{
		CNavMeshHolder * pNavMeshHolder = g_NavMeshes[n];
		if(pNavMeshHolder->m_pNavMesh)
		{
			CNavMeshAlloc<CNavMesh> navmeshAlloc;
			navmeshAlloc.Delete(pNavMeshHolder->m_pNavMesh);
		}
		delete pNavMeshHolder;
	}
	g_NavMeshes.clear();

	OutputText("Done\n");


	time(&sysTime);
	localTime = localtime(&sysTime);
	sprintf(textBuf, "ENDED AT : %s\n\n", asctime(localTime));
	OutputText(textBuf);


	u32 iEndTime = timeGetTime();
	float fSecs = ((float)(iEndTime - iStartTime) / 1000.0f);
	fTotalTimeTaken = fSecs;

	OutputText("\n");
	sprintf(textBuf, "TOTAL TIME TAKEN : %.1f minutes\n", fTotalTimeTaken / 60.0f);
	OutputText(textBuf);
	OutputText("\n");

	return true;
}

bool
CNavMeshMaker::FloodFillNavMeshes()
{
	char textBuf[256];
	OutputText("Flood-filling navmeshes.\n");

	char pathForNavDatFile[512];
	sprintf(pathForNavDatFile, "%s\\common\\data", g_pGameFolder);

	//************************************************
	// Load all the navmeshes on the entire map

	bool bInitOk = CNavGen_NavMeshStore::LoadAll(g_InputPath);

	if(!bInitOk)
	{
		OutputText(g_pBlankLine);
		sprintf(textBuf, "FloodFillNavMeshes - CNavGen_NavMeshStore::LoadAll() returned false\n");
		OutputText(textBuf);
		return false;
	}

	//***************************************************************************
	// Load the dynamic entity info from every extras.dat file on the entire map
/*
	CDynamicEntityBoundsPerNavMesh entityBoundsPerNavMesh;

	int iIndex = 0;
	for(int y=0; y<CPathServerExtents::GetWorldDepthInSectors(); y+=CPathServerExtents::m_iNumSectorsPerNavMesh)
	{
		for(int x=0; x<CPathServerExtents::GetWorldWidthInSectors(); x+=CPathServerExtents::m_iNumSectorsPerNavMesh)
		{
			char extrasFileName[1024];
			sprintf(extrasFileName, "%s/Extras_%i_%i.dat", g_InputPath, x, y);

			CNavGen::ParseExtraInfoFile(extrasFileName, NULL, NULL, NULL, &entityBoundsPerNavMesh.m_Bounds[iIndex]);

			iIndex++;
		}
	}
*/
	//***********************
	// Commence processing..

	//OutputText("Setting free space around polys..\n");
	//CNavGen::SetFreeSpaceAroundPolys();


	OutputText("Removing isolated areas of pavement..\n");
	CNavGen::RemoveIsolatedAreasOfPavement();


	OutputText("Removing isolated areas of ped-density..\n");
	CNavGen::RemoveIsolatedAreasOfPedDensity();


	OutputText("Marking isolated areas of navmesh..\n");
	CNavGen::MarkIsolatedAreasOfNavMesh();

	OutputText("Marking potential network spawn polys..\n");
	CNavGen::MarkPotentialNetworkSpawnPolys(g_InputPath); //entityBoundsPerNavMesh);

	OutputText("Setting navmesh flags..\n");
	CNavGen::SetAllNavMeshFlags();

	OutputText("Calculating free space around verts & edges..\n");
	for(int y=0; y<CPathServerExtents::m_iNumNavMeshesInY; y++)
	{
		for(int x=0; x<CPathServerExtents::m_iNumNavMeshesInX; x++)
		{
			int iNavMesh = (CPathServerExtents::m_iNumNavMeshesInX * y) + x;
			CNavMesh * pNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(iNavMesh);
			if(pNavMesh)
			{
				CNavMesh * pNegX = (x > 0) ? CNavGen_NavMeshStore::GetNavMeshFromIndex((CPathServerExtents::m_iNumNavMeshesInX * y) + x-1) : NULL;
				CNavMesh * pPosX = (x < CPathServerExtents::m_iNumNavMeshesInX) ? CNavGen_NavMeshStore::GetNavMeshFromIndex((CPathServerExtents::m_iNumNavMeshesInX * y) + x+1) : NULL;
				CNavMesh * pNegY = (y > 0) ? CNavGen_NavMeshStore::GetNavMeshFromIndex((CPathServerExtents::m_iNumNavMeshesInY * (y-1)) + x) : NULL;
				CNavMesh * pPosY = (y < CPathServerExtents::m_iNumNavMeshesInY) ? CNavGen_NavMeshStore::GetNavMeshFromIndex((CPathServerExtents::m_iNumNavMeshesInY * (y+1)) + x) : NULL;

				//CNavGen::CalcFreeSpaceBeyondPolyEdges(pNavMesh);
				CNavGen::CalcFreeSpaceAroundVertices(pNavMesh, pNegX, pPosX, pNegY, pPosY);
			}
		}
	}

	OutputText("All done!\n");

	//**************************************************************************************************
	// Now need to save out all the navmeshes again.
	//**************************************************************************************************

	const u32 iBuildID = g_iForceBuildID ? g_iForceBuildID : timeGetTime();
	
	OutputText("Saving navmeshes..\n");

	char navMeshShortFilename[512];
	char navMeshFilename[512];

	for(int y=0; y<CPathServerExtents::m_iNumNavMeshesInY; y++)
	{
		for(int x=0; x<CPathServerExtents::m_iNumNavMeshesInX; x++)
		{
			int iNavMesh = (CPathServerExtents::m_iNumNavMeshesInX * y) + x;

			CNavMesh * pNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(iNavMesh);
			if(!pNavMesh)
				continue;

			pNavMesh->IdentifyEdgePolys();

			pNavMesh->SetBuildID(iBuildID);

			sprintf(
				navMeshShortFilename,
				"navmesh[%i][%i].inv",
				x * CPathServerExtents::m_iNumSectorsPerNavMesh,
				y * CPathServerExtents::m_iNumSectorsPerNavMesh);

			sprintf(navMeshFilename, "%s/%s", g_OutputPath, navMeshShortFilename);

			CNavMesh::SaveBinary(pNavMesh, navMeshFilename);
		}
	}

	OutputText("Saved.\n");

	//**************************
	// Free allocated memory

	//NB: entityBoundsPerNavMesh will be cleared in its destructor

//	CPathServer::Shutdown();

	return true;
}


bool CNavMeshMaker::AnalyseNavMeshes(bool bAnalyse, bool bMainMapNavMeshes)
{
	// Prevent the navmesh loaded from loading cover points - as we will be creating new ones.
	CNavMesh::ms_bLoadCoverPoints = false;

	char textBuf[256];
	OutputText("Analysing NavMeshes for climb-up's / drop-down's / etc.\n");

	time_t sysTime;
	tm * localTime;

	time(&sysTime);
	localTime = localtime(&sysTime);
	sprintf(textBuf, "STARTED LOADING DATA AT : %s\n\n", asctime(localTime));
	OutputText(textBuf);

	float fTotalTimeTaken = 0.0f;
	u32 m,n,n2;
	char tmp[MAX_PATH];
	char octreeFileName[MAX_PATH];
	char collTriFileName[MAX_PATH];
	//char mrgFileName[MAX_PATH];
	char srfFileName[MAX_PATH];
	char cvrFileName[MAX_PATH];

	vector<CNavGen*> navGens;


	if(g_ResolutionAreasFileName)
	{
		CNavGen::LoadCustomResolutionAreas(g_ResolutionAreasFileName);
	}

	if(g_CustomCoverpointsFileName)
	{
		CNavGen::LoadCustomCoverpoints(g_CustomCoverpointsFileName);
	}

	//***********************************************************************************
	// For each CNavMesh on the main map, we will load it and the four surrounding it.
	// We will also load the octrees & collision triangles (This will require a CNavGen
	// class instance for each mesh).
	// For interiors we will just process the single CNavMesh (unless at a later date the
	// interiors get so large that they must themselves be split into blocks).
	//
	// We will go through and load ALL this data into memory up-front (probably 100+ megs)
	// Then we can call the analysis functions which find out which can polys connect to
	// each other in special ways (climb-ups, drop-downs, jump-overs, etc).  This data 
	// stored in each CNavMesh, and then all of the meshes are re-saved at the end.
	//***********************************************************************************

	for(m=0; m<g_SourceNavMeshes.size(); m++)
	{
		TNavMeshParams * pNavParams = g_SourceNavMeshes[m];
		char * pFileName = pNavParams->m_pFileName;
		if(!pFileName || !(*pFileName))
			continue;

		strcpy(tmp, pFileName);
		strlwr(tmp);

		strcpy(octreeFileName, pFileName);
		strcpy(collTriFileName, pFileName);
		strcpy(srfFileName, pFileName);
		strcpy(cvrFileName, pFileName);

		ReplaceExtension(octreeFileName, ".oct");
		ReplaceExtension(collTriFileName, ".tri");
		ReplaceExtension(srfFileName, ".srf");
		ReplaceExtension(cvrFileName, ".cvr");



		//**********************************************************
		//	Load NavMeshes
		//**********************************************************

		OutputText(g_pBlankLine);
		sprintf(textBuf, "Loading NavMesh \"%s\"", pFileName);
		OutputText(textBuf);

		CNavMesh * pNavMesh = CNavMesh::LoadBinary(pFileName);

		if(!pNavMesh)
		{
			continue;
		}

		CNavMeshHolder * pNavMeshHolder = new CNavMeshHolder();
		pNavMeshHolder->m_bIsInterior = false;


		#ifdef CHECK_NAVMESH_INTEGRITY
		CNavGen::CheckNavMeshIntegrity(pNavMesh);
		#endif

		//*****************************************************************
		//	Set up data in CNavMeshHolder

		pNavMeshHolder->m_pNavMesh = pNavMesh;
		pNavMeshHolder->m_pNavMeshName		= _strdup(pFileName);
		pNavMeshHolder->m_pColFileName		= _strdup(collTriFileName);
		pNavMeshHolder->m_pSrfFileName		= _strdup(srfFileName);
		pNavMeshHolder->m_pOctreeFileName	= _strdup(octreeFileName);
		pNavMeshHolder->m_pCvrFileName		= _strdup(cvrFileName);
		pNavMeshHolder->m_pParams = pNavParams;

		pNavMeshHolder->m_iNavMeshIndex = CNavGen_NavMeshStore::GetNavMeshIndexFromFilename(pNavMeshHolder->m_pNavMeshName, &pNavMeshHolder->m_iNavMeshXPos, &pNavMeshHolder->m_iNavMeshYPos);

		if(pNavMeshHolder->m_iNavMeshIndex==NAVMESH_NAVMESH_INDEX_NONE)
		{
			OutputText("bad filename format.");

			pNavMeshHolder->m_iNavMeshIndex = NAVMESH_INDEX_FIRST_DYNAMIC;
			pNavMeshHolder->m_iNavMeshXPos = 0;
			pNavMeshHolder->m_iNavMeshYPos = 0;
		}


		//***************************************************************************************
		//	See if we have a CVR file whose stored CRC value matches the TRI file's CRC for this
		//	navmesh.  If so then we might not need to analyse coverpoints for this navmesh - but
		//	only if the 4 neighbouring navmeshes have also not changed.

#if __USE_CACHED_COVERPOINTS
		u32 iCvrFileCollisionCRC = 0;
		u32 iCvrFileResAreasCRC = 0;
		CNavGen::ReadCvrFileCRCs(pNavMeshHolder->m_pCvrFileName, iCvrFileCollisionCRC, iCvrFileResAreasCRC);
#endif
		u32 iSrfFileCollisionCRC = 0;
		u32 iSrfFileResAreasCRC = 0;
		ReadSrfFileCRCs(pNavMeshHolder->m_pSrfFileName, iSrfFileCollisionCRC, iSrfFileResAreasCRC);

#if __USE_CACHED_COVERPOINTS
		// TODO : also check latest collision file / res areas CRCs?
		if(iCvrFileCollisionCRC == iSrfFileCollisionCRC && iCvrFileResAreasCRC == iSrfFileResAreasCRC)
		{
			pNavMeshHolder->m_bCanMaybeUseCachedCoverPoints = true;
		}
#endif
		pNavMeshHolder->m_iTriFileCRC = iSrfFileCollisionCRC;
		pNavMeshHolder->m_iResAreasCRC = iSrfFileResAreasCRC;

		//*******************************************************************
		//	Add to list.
		//	Create a NULL entry in the navgens list.  we'll load when needed
		//*******************************************************************

		pNavMeshHolder->m_fLowWallCoverPointStepSize = pNavParams->m_fLowWallCoverPointStepSize;

		g_NavMeshes.push_back(pNavMeshHolder);
		navGens.push_back(NULL);
	}




	//*****************************************************
	// Now do the actual analysis of each CNavMesh.
	//*****************************************************

	if(bAnalyse)
	{
		OutputText("\n\n");
		time(&sysTime);
		localTime = localtime(&sysTime);
		sprintf(textBuf, "STARTED ANALYSING NAVMESHES AT : %s\n\n", asctime(localTime));
		OutputText(textBuf);

		// List of all the special links
		vector<CSpecialLinkInfo *> allSpecialLinks;


		for(n=0; n<g_NavMeshes.size(); n++)
		{
			u32 iStartTime = timeGetTime();

			CNavMeshHolder * pNavMeshHolder = g_NavMeshes[n];

			//Identify the (up to) 4 neighbouring navmeshes
			CNavMeshHolder * pNeighbourMeshMinusX = NULL;
			CNavMeshHolder * pNeighbourMeshPlusX = NULL;
			CNavMeshHolder * pNeighbourMeshMinusY = NULL;
			CNavMeshHolder * pNeighbourMeshPlusY = NULL;

			for(u32 m1=0; m1<g_NavMeshes.size(); m1++)
			{
				CNavMeshHolder * pHolder = g_NavMeshes[m1];
				pHolder->m_bRequired = false;
			}

			s32 iNeighbourNavGenMinusX = -1;
			s32 iNeighbourNavGenPlusX = -1;
			s32 iNeighbourNavGenMinusY = -1;
			s32 iNeighbourNavGenPlusY = -1;

			pNavMeshHolder->m_bRequired = true;

			if(bMainMapNavMeshes)
			{
				for(n2=0; n2<g_NavMeshes.size(); n2++)
				{
					CNavMeshHolder * pNeighbour = g_NavMeshes[n2];
					if(pNeighbour->m_bIsInterior)
						continue;

					if(pNeighbour->m_iNavMeshYPos == pNavMeshHolder->m_iNavMeshYPos)
					{
						if(pNeighbour->m_iNavMeshXPos == pNavMeshHolder->m_iNavMeshXPos-1)
						{
							pNeighbour->m_bRequired = true;
							pNeighbourMeshMinusX = pNeighbour;
							iNeighbourNavGenMinusX = n2;
						}
						else if(pNeighbour->m_iNavMeshXPos == pNavMeshHolder->m_iNavMeshXPos+1)
						{
							pNeighbour->m_bRequired = true;
							pNeighbourMeshPlusX = pNeighbour;
							iNeighbourNavGenPlusX = n2;
						}
					}
					else if(pNeighbour->m_iNavMeshXPos == pNavMeshHolder->m_iNavMeshXPos)
					{
						if(pNeighbour->m_iNavMeshYPos == pNavMeshHolder->m_iNavMeshYPos-1)
						{
							pNeighbour->m_bRequired = true;
							pNeighbourMeshMinusY = pNeighbour;
							iNeighbourNavGenMinusY = n2;
						}
						else if(pNeighbour->m_iNavMeshYPos == pNavMeshHolder->m_iNavMeshYPos+1)
						{
							pNeighbour->m_bRequired = true;
							pNeighbourMeshPlusY = pNeighbour;
							iNeighbourNavGenPlusY = n2;
						}
					}
				}
			}

			//----------------------------------------------------------------------------

			LoadRequiredCollisionAndUnloadNotRequired(navGens);

			//----------------------------------------------------------------------------

			int iNumNeighbouringNavMeshes = 0;
			int iNumNeighboursCanUseCachedCoverPoints = 0;

			if(pNeighbourMeshMinusX) {
				iNumNeighbouringNavMeshes++;
				if(pNeighbourMeshMinusX->m_bCanMaybeUseCachedCoverPoints) iNumNeighboursCanUseCachedCoverPoints++;
			}
			if(pNeighbourMeshPlusX) {
				iNumNeighbouringNavMeshes++;
				if(pNeighbourMeshPlusX->m_bCanMaybeUseCachedCoverPoints) iNumNeighboursCanUseCachedCoverPoints++;
			}
			if(pNeighbourMeshMinusY) {
				iNumNeighbouringNavMeshes++;
				if(pNeighbourMeshMinusY->m_bCanMaybeUseCachedCoverPoints) iNumNeighboursCanUseCachedCoverPoints++;
			}
			if(pNeighbourMeshPlusY) {
				iNumNeighbouringNavMeshes++;
				if(pNeighbourMeshPlusY->m_bCanMaybeUseCachedCoverPoints) iNumNeighboursCanUseCachedCoverPoints++;
			}

			// If this navmesh & all the surrounding 4 navmeshes can used cached coverpoints, then go ahead
			char * pCvrFileToLoad =
				(iNumNeighbouringNavMeshes == iNumNeighboursCanUseCachedCoverPoints && pNavMeshHolder->m_bCanMaybeUseCachedCoverPoints) ?
					pNavMeshHolder->m_pCvrFileName : NULL;

			CNavGen * pNavGen = navGens[n];
			Assert(pNavGen);

			if(pNavGen)
			{
				CNavGen * pNeighbourNodeGenMinusX = (iNeighbourNavGenMinusX==-1) ? NULL : navGens[iNeighbourNavGenMinusX];
				CNavGen * pNeighbourNodeGenPlusX = (iNeighbourNavGenPlusX==-1) ? NULL : navGens[iNeighbourNavGenPlusX];
				CNavGen * pNeighbourNodeGenMinusY = (iNeighbourNavGenMinusY==-1) ? NULL : navGens[iNeighbourNavGenMinusY];
				CNavGen * pNeighbourNodeGenPlusY = (iNeighbourNavGenPlusY==-1) ? NULL : navGens[iNeighbourNavGenPlusY];

				// Possibly load authored adjacencies
				// For now this is only for authored navmeshes (and this means non-mainmap boats/trains/etc)
				atArray<CNavGen::TAuthoredAdjacencyHintPos> authoredAdjacencies;
				if(pNavMeshHolder->m_pParams->m_pAuthoredMeshFileName)
				{
					pNavGen->Load3dsMaxAsciiMarkup(pNavMeshHolder->m_pParams->m_pAuthoredMeshFileName, authoredAdjacencies);
				}

				OutputText(g_pBlankLine);
				sprintf(textBuf, "Analysing NavMesh \"%s\".\n", pNavMeshHolder->m_pNavMeshName);
				OutputText(textBuf);

	#if NAVGEN_OBSOLETE
				if(pNavMeshHolder->m_fLowWallCoverPointStepSize != g_fLowWallCoverPointSpacingAlongEdges)
				{
					CNavGen::ms_fLowCoverSpacingAlongEdges = pNavMeshHolder->m_fLowWallCoverPointStepSize;
					CNavGen::ms_fMinCoverPointSeparationWhenThinningOut = pNavMeshHolder->m_fLowWallCoverPointStepSize;
				}
				else
				{
					CNavGen::ms_fLowCoverSpacingAlongEdges = g_fLowWallCoverPointSpacingAlongEdges;
					CNavGen::ms_fMinCoverPointSeparationWhenThinningOut = g_fMinCoverPointSeperationWhenThinningOut;
				}
	#else	// NAVGEN_OBSOLETE
				CNavGen::ms_fLowCoverSpacingAlongEdges = pNavMeshHolder->m_fLowWallCoverPointStepSize;
	#endif	// NAVGEN_OBSOLETE

				CNavGen::AnalyseNavMesh(
					pNavMeshHolder->m_pNavMesh, pNavGen,
					pNeighbourMeshMinusX ? pNeighbourMeshMinusX->m_pNavMesh : NULL, pNeighbourNodeGenMinusX,
					pNeighbourMeshPlusX ? pNeighbourMeshPlusX->m_pNavMesh : NULL, pNeighbourNodeGenPlusX,
					pNeighbourMeshMinusY ? pNeighbourMeshMinusY->m_pNavMesh : NULL, pNeighbourNodeGenMinusY,
					pNeighbourMeshPlusY ? pNeighbourMeshPlusY->m_pNavMesh : NULL, pNeighbourNodeGenPlusY,
					pCvrFileToLoad,
					authoredAdjacencies,
					!bMainMapNavMeshes
				);

				OutputText(g_pBlankLine);
				sprintf(textBuf, "Analysing NavMesh \"%s\".\n", pNavMeshHolder->m_pNavMeshName);
				OutputText(textBuf);

				OutputText(g_pBlankLine);
				sprintf(tmp, "Low Climb-Overs : %i High Climb-Overs : %i Drop-Downs : %i Cover-Points : %i\n",
					g_iNumLowClimbOversFound,
					g_iNumHighClimbOversFound,
					g_iNumDropDownsFound,
					g_iNumCoverPointsFound);

				u32 iEndTime = timeGetTime();
				float fSecs = ((float)(iEndTime - iStartTime) / 1000.0f);
				fTotalTimeTaken += fSecs;

				//*******************************************************************
				// Analyse the ladders, etc in this navmesh if there are any
				// Look for the Extra.dat file for this navmesh.
				// Currently this is only working for main-map navmeshes, to make it
				// work for vehicle navmeshes too we just need to construct a valid
				// filename for the extras file for that vehicle

				// TODO: We probably shouldn't write out the extras data when building
				// aerial navmeshes.

				if(bMainMapNavMeshes)
				{
					static char extrasFileName[1024];
					sprintf(
						extrasFileName,
						"%s/Extras_%i_%i.dat",
						g_InputPath,
						pNavMeshHolder->m_iNavMeshXPos,
						pNavMeshHolder->m_iNavMeshYPos
					);

					vector<fwNavLadderInfo> ladderInfos;
					vector<TCarNode*> carNodes;
					vector<spdSphere*> portalBoundaries;
					vector<fwDynamicEntityInfo*> dynamicEntityBounds;

					CNavGen * pNavGens[ANALYSE_NUM_NAVMESHES] =
					{
						pNavGen, pNeighbourNodeGenMinusX, pNeighbourNodeGenPlusX, pNeighbourNodeGenMinusY, pNeighbourNodeGenPlusY
					};

					bool bReadInfoFileOk = CNavGen::ParseExtraInfoFile(extrasFileName, &ladderInfos, &carNodes, &portalBoundaries, &dynamicEntityBounds);

					if(bReadInfoFileOk)
					{
						CNavMesh * pNavMeshes[ANALYSE_NUM_NAVMESHES];

						pNavMeshes[0] = pNavMeshHolder->m_pNavMesh;
						pNavMeshes[1] = pNeighbourMeshMinusX ? pNeighbourMeshMinusX->m_pNavMesh : NULL;
						pNavMeshes[2] = pNeighbourMeshPlusX ? pNeighbourMeshPlusX->m_pNavMesh : NULL;
						pNavMeshes[3] = pNeighbourMeshMinusY ? pNeighbourMeshMinusY->m_pNavMesh : NULL;
						pNavMeshes[4] = pNeighbourMeshPlusY ? pNeighbourMeshPlusY->m_pNavMesh : NULL;

						CNavGen::CreateSpecialNavMeshLinks(pNavMeshes, pNavGens, ANALYSE_NUM_NAVMESHES, ladderInfos, dynamicEntityBounds, allSpecialLinks);
						CNavGen::MarkCarNodes(pNavMeshHolder->m_pNavMesh, carNodes);
						CNavGen::ZeroPedDensityAtPortalBoundaries(pNavMeshHolder->m_pNavMesh, portalBoundaries);
					}

					for(u32 n=0; n<carNodes.size(); n++)
						delete carNodes[n];
					for(u32 p=0; p<portalBoundaries.size(); p++)
						delete portalBoundaries[p];

					CNavGen::MarkShelteredPolys(pNavMeshHolder->m_pNavMesh, pNavGen);
				}

				pNavMeshHolder->m_bRequired = false;
			}
		}

		//***********************************************
		// Create special links sections in each navmesh.

		for(n=0; n<g_NavMeshes.size(); n++)
		{
			CNavMeshHolder * pNavMeshHolder = g_NavMeshes[n];
			if(!pNavMeshHolder->m_pNavMesh)
				continue;

			vector<CSpecialLinkInfo*> specialLinksFrom;
			vector<CSpecialLinkInfo*> specialLinksTo;
			for(u32 s=0; s<allSpecialLinks.size(); s++)
			{
				if(allSpecialLinks[s]->GetLinkFromNavMesh() == pNavMeshHolder->m_iNavMeshIndex)
					specialLinksFrom.push_back(allSpecialLinks[s]);

				if(allSpecialLinks[s]->GetLinkToNavMesh() == pNavMeshHolder->m_iNavMeshIndex)
					specialLinksTo.push_back(allSpecialLinks[s]);
			}

			//************************************************************************
			//	Go through all link info list & set the NAVMESHPOLY_HAS_SPECIAL_LINKS
			//	flag for all polys which are linked from or to

			CNavMesh * pNavMesh = pNavMeshHolder->m_pNavMesh;

			if(specialLinksFrom.size())
			{
				pNavMesh->SetFlags( pNavMesh->GetFlags() | NAVMESH_HAS_SPECIAL_LINKS_SECTION );
			}
			else
			{
				pNavMesh->SetFlags( pNavMesh->GetFlags() & ~NAVMESH_HAS_SPECIAL_LINKS_SECTION );
			}

			//***************************************************************************************
			//	Allocate the final array of special links, copy in members & set in the navmesh

			CSpecialLinkInfo * pLinksArray = new CSpecialLinkInfo[specialLinksFrom.size()];
			for(u32 l=0; l<specialLinksFrom.size(); l++)
			{
				CSpecialLinkInfo * pLink = specialLinksFrom[l];
				memcpy(&pLinksArray[l], pLink, sizeof(CSpecialLinkInfo));
			}

			if(specialLinksFrom.size())
			{
				pNavMesh->SetSpecialLinks( (u32)specialLinksFrom.size(), pLinksArray );
			}

			//******************************************************************************
			// Now also set the special link flag for all polys in this mesh which are
			// linked to from another navmesh.
			// NB: We may want a separate flag to distinguish this..

			atArray<s32> specialLinkIndices;

			for(u32 p=0; p<pNavMesh->GetNumPolys(); p++)
			{
				TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);
				pPoly->SetSpecialLinksStartIndex(specialLinkIndices.GetCount());

				s32 iNum = 0;

				for(u32 l=0; l<pNavMesh->GetNumSpecialLinks(); l++)
				{
					const CSpecialLinkInfo & link = pNavMesh->GetSpecialLinks()[l];

					if(  (link.GetLinkFromNavMesh()==pNavMesh->GetIndexOfMesh() && link.GetLinkFromPoly()==p) ||
						(link.GetLinkToNavMesh()==pNavMesh->GetIndexOfMesh() && link.GetLinkToPoly()==p) )
					{
						specialLinkIndices.PushAndGrow(l);
						iNum++;

						if(iNum >= NAVMESHPOLY_MAX_SPECIAL_LINKS)
							break;
					}
				}

				pPoly->SetNumSpecialLinks(iNum);
			}

			u16 * pSpecialLinkIndices = new u16[specialLinkIndices.GetCount()];
			for(s32 i=0; i<specialLinkIndices.GetCount(); i++)
				pSpecialLinkIndices[i] = (u16)specialLinkIndices[i];

			if(specialLinkIndices.GetCount())
			{
				pNavMesh->SetSpecialLinksIndices( specialLinkIndices.GetCount(), pSpecialLinkIndices );
			}
		}

		//*************************************
		// Finished... Unload everything

		for(u32 m1=0; m1<g_NavMeshes.size(); m1++)
		{
			CNavMeshHolder * pHolder = g_NavMeshes[m1];
			pHolder->m_bRequired = false;
		}
		LoadRequiredCollisionAndUnloadNotRequired(navGens);


		//*****************************************************
		// Save out all the CNavMeshes
		//*****************************************************

		OutputText("Saving NavMeshes\n");

		// Now go through and save all the nav meshes, with the ".inv" extension
		char navMeshFileName[256];

		for(n=0; n<g_NavMeshes.size(); n++)
		{
			CNavMeshHolder * pNavMeshHolder = g_NavMeshes[n];
			if(pNavMeshHolder->m_pNavMesh)
			{
				strcpy(navMeshFileName, pNavMeshHolder->m_pNavMeshName);

				size_t s = strlen(navMeshFileName)-1;
				while(navMeshFileName[s] != '.')
					s--;

				if(navMeshFileName[s] == '.')
				{
					s++;
					navMeshFileName[s++] = 'i';
					navMeshFileName[s++] = 'n';
					navMeshFileName[s++] = g_bAerialNavigation ? 'h' : 'v';

					CNavMesh::SaveBinary(pNavMeshHolder->m_pNavMesh, navMeshFileName);
				}
				else
				{
					OutputText("Error - bad filename\n");
				}

#if __USE_CACHED_COVERPOINTS

				//***********************************************
				//	Save out a .cvr file for every navmesh also

				CNavGen::SaveCvrFile(pNavMeshHolder->m_pCvrFileName, pNavMeshHolder->m_pNavMesh, pNavMeshHolder->m_iTriFileCRC, pNavMeshHolder->m_iResAreasCRC);
#endif
			}
		}
	}

	//*****************************************************
	// Delete all the data again
	//*****************************************************

	for(n=0; n<g_NavMeshes.size(); n++)
	{
		CNavMeshHolder * pNavMeshHolder = g_NavMeshes[n];
		if(pNavMeshHolder->m_pNavMesh)
		{
			CNavMeshAlloc<CNavMesh> navmeshAlloc;
			navmeshAlloc.Delete(pNavMeshHolder->m_pNavMesh);
		}
		delete pNavMeshHolder;
	}
	g_NavMeshes.clear();

	for(n=0; n<navGens.size(); n++)
	{
		CNavGen * pNavGen = navGens[n];
		if(pNavGen)
		{
			pNavGen->Shutdown();
			delete pNavGen;
		}
	}
	navGens.clear();


	time(&sysTime);
	localTime = localtime(&sysTime);
	sprintf(textBuf, "ENDED AT : %s\n\n", asctime(localTime));
	OutputText(textBuf);

	OutputText("\n");
	sprintf(textBuf, "TOTAL TIME TAKEN : %.1f minutes\n", fTotalTimeTaken / 60.0f);
	OutputText(textBuf);
	OutputText("\n");

	return true;
}


bool
CNavMeshMaker::LoadRequiredCollisionAndUnloadNotRequired(vector<CNavGen*> & navGens)
{
	char textBuf[512];

	for(u32 n=0; n<g_NavMeshes.size(); n++)
	{
		CNavMeshHolder * pHolder = g_NavMeshes[n];
		CNavGen * pNavGen = navGens[n];

		if(pHolder->m_bRequired)
		{
			// NavGen already loaded for a required navmesh ?
			if(pNavGen)
				continue;

			// Create new NavGen, and set in array
			CNavGen * pNavGen = new CNavGen();
			navGens[n] = pNavGen;

			OutputText(g_pBlankLine);
			sprintf(textBuf, "Loading triangles \"%s\".", pHolder->m_pColFileName);
			OutputText(textBuf);

			Vector3 vSectorMin, vSectorMax, vWorldMin, vWorldMax;
			Vector2 vMapMin;
			int iSectorStartX, iSectorStartY, iSectorNumX, iSectorNumY;

			// Collision triangles
			bool bLoadedOk = pNavGen->LoadCollisionTriangles(
				pHolder->m_pColFileName,
				vSectorMin, vSectorMax,
				iSectorStartX, iSectorStartY,
				iSectorNumX, iSectorNumY,
				vWorldMin, vWorldMax, vMapMin);

			if(bLoadedOk)
			{
				OutputText(g_pBlankLine);
				OutputText("Loaded collision ok");

				CPathServerExtents::m_vWorldMin.x = vMapMin.x;
				CPathServerExtents::m_vWorldMin.y = vMapMin.y;

				CPathServerExtents::m_vWorldMax.x =
					CPathServerExtents::m_vWorldMin.x +
					(CPathServerExtents::m_iNumNavMeshesInX * CPathServerExtents::m_iNumSectorsPerNavMesh * CPathServerExtents::GetWorldWidthOfSector());

				CPathServerExtents::m_vWorldMax.y =
					CPathServerExtents::m_vWorldMin.y +
					(CPathServerExtents::m_iNumNavMeshesInY * CPathServerExtents::m_iNumSectorsPerNavMesh * CPathServerExtents::GetWorldWidthOfSector());

				// Mark up triangles as walkable, pavement, see-through, shoot-through, etc
				pNavGen->FlagWalkableTris();

				// Octree
				bool bLoadedOctreeOk = pNavGen->m_Octree.LoadOctree(pHolder->m_pOctreeFileName, &pNavGen->m_CollisionTriangles, &pHolder->m_iTriFileCRC);

				if(!bLoadedOctreeOk)
				{
					sprintf(textBuf, "Couldn't load octree file \"%s\" - will generate instead\n", pHolder->m_pOctreeFileName);
					OutputText(textBuf);

					CNavGenParams params;
					params.Init(
						vWorldMin, vWorldMax,
						Vector3(0, 0, 1.0f),
						vSectorMin, vSectorMax,
						false,
						g_fSampleResolution
						);

					pNavGen->Init(&params);
					pNavGen->InitOctree();
				}
			}
			else
			{
				sprintf(textBuf, "Error : couldn't load triangle file \"%s\".\n\n", pHolder->m_pColFileName);
				
				pNavGen->Shutdown();
				delete pNavGen;
				navGens[n] = NULL;

				OutputText(textBuf);
			}
		}

		// !pHolder->m_bRequired
		else
		{
			if(pNavGen)
			{
				pNavGen->Shutdown();
				delete pNavGen;
			}

			navGens[n] = NULL;
		}
	}

	return true;
}

//*******************************************************************
//	CompareAndDeleteIdenticalNavFiles
//	Given a pFolder1 and pFolder2, this function will scan through
//	all files with the "inv" extension and will compare the CRCs
//	of files with the same filename.  If the CRCs are identical then
//	the "inv" file in the FIRST FOLDER will be deleted!
//	This is intended to be used for removing duplicate files from the
//	D/L content

bool CNavMeshMaker::CompareAndDeleteIdenticalNavFiles(char * pFolder1, char * pFolder2)
{
	char searchString[MAX_PATH];
	sprintf(searchString, "%s/*.inv", pFolder2);

	WIN32_FIND_DATA findData;
	ZeroMemory(&findData, sizeof(WIN32_FIND_DATA));

	HANDLE h1 = FindFirstFile(searchString, &findData);
	if(h1==INVALID_HANDLE_VALUE)
		return false;

	while(h1!=INVALID_HANDLE_VALUE)
	{
		// We have found an "inv" file in folder 2
		// See if it also exists in folder 1.

		char * pFilename2 = (char*)findData.cFileName;

		if(pFilename2)
		{
			char fileName1[MAX_PATH];
			sprintf(fileName1, "%s/%s", pFolder1, pFilename2);
			char fileName2[MAX_PATH];
			sprintf(fileName2, "%s/%s", pFolder2, pFilename2);

			u32 iCRC2 = CalcFileCRC(fileName2);
			u32 iCRC1 = CalcFileCRC(fileName1);

			// CRCs are valid and are also identical.  We can delete the 1st file.
			if(iCRC1 && iCRC2 && (iCRC1==iCRC2))
			{
				DeleteFileA(fileName1);
			}
		}

		if(!FindNextFile(h1, &findData))
			break;
	}

	return true;
}


bool
CNavMeshMaker::CreateHierarchicalNavData(void)
{
	bool ret = true;

	ret = CNavGen::CreateHierarchicalNavData(g_InputPath, g_pGameFolder);

	return ret;
}



bool
CNavMeshMaker::CleanNavMeshes(void)
{
	char textBuf[256];
	OutputText("Cleaning NavMeshes.\n");

	u32 m,n;

	// Load all the nav meshes
	for(m=0; m<g_SourceNavMeshes.size(); m++)
	{
		TNavMeshParams * pNavParams = g_SourceNavMeshes[m];
		char * pFileName = pNavParams->m_pFileName;

		OutputText(g_pBlankLine);
		sprintf(textBuf, "Loading NavMesh \"%s\"", pFileName);
		OutputText(textBuf);

		CNavMeshHolder * pNavMeshHolder = new CNavMeshHolder();

		CNavMesh * pNavMesh = CNavMesh::LoadBinary(pFileName);

		if(!pNavMesh)
		{
			delete pNavMeshHolder;
			continue;
		}

		pNavMeshHolder->m_pNavMesh = pNavMesh;
		pNavMeshHolder->m_pNavMeshName = _strdup(pFileName);

		pNavMeshHolder->m_iNavMeshIndex = CNavGen_NavMeshStore::GetNavMeshIndexFromFilename(pNavMeshHolder->m_pNavMeshName, &pNavMeshHolder->m_iNavMeshXPos, &pNavMeshHolder->m_iNavMeshYPos);

		if(pNavMeshHolder->m_iNavMeshIndex==NAVMESH_NAVMESH_INDEX_NONE)
		{
			OutputText("bad filename format.");
		}
		g_NavMeshes.push_back(pNavMeshHolder);
	}


	for(n=0; n<g_NavMeshes.size(); n++)
	{
		CNavMeshHolder * pNavMeshHolder = g_NavMeshes[n];

		if(pNavMeshHolder->m_pNavMeshName)
		{
			OutputText(g_pBlankLine);
			sprintf(textBuf, "> %s", pNavMeshHolder->m_pNavMeshName);
			OutputText(textBuf);
		}

		CNavMesh * pNavMesh = pNavMeshHolder->m_pNavMesh;

		for(u32 p=0; p<pNavMesh->GetNumPolys(); p++)
		{
			TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);

			for(u32 v=0; v<pPoly->GetNumVertices(); v++)
			{
				TAdjPoly & adjPoly = *pNavMesh->GetAdjacentPolysArray().Get(pPoly->GetFirstVertexIndex() + v);

				if(adjPoly.GetAdjacencyType() != ADJACENCY_TYPE_NORMAL)
				{
					adjPoly.m_iAdjacentNavMeshIndex = NAVMESH_NAVMESH_INDEX_NONE;
					adjPoly.SetNavMeshIndex(NAVMESH_NAVMESH_INDEX_NONE, pNavMesh->GetAdjacentMeshes());
					adjPoly.SetPolyIndex(NAVMESH_POLY_INDEX_NONE);
				}

				adjPoly.SetAdjacencyType(ADJACENCY_TYPE_NORMAL);
				adjPoly.SetEdgeProvidesCover(false);
			}
		}

		pNavMesh->EstablishPolyAdjacancy();
	}


	//*****************************************************
	// Save out all the CNavMeshes
	//*****************************************************

	OutputText("Saving NavMeshes\n");

	// Now go through and save all the nav meshes, with the ".inv" extension
	char navMeshFileName[256];

	for(n=0; n<g_NavMeshes.size(); n++)
	{
		CNavMeshHolder * pNavMeshHolder = g_NavMeshes[n];

		strcpy(navMeshFileName, pNavMeshHolder->m_pNavMeshName);

		size_t s = strlen(navMeshFileName)-1;
		while(navMeshFileName[s] != '.')
			s--;

		if(navMeshFileName[s] == '.')
		{
			s++;
			navMeshFileName[s++] = 'i';
			navMeshFileName[s++] = 'n';
			navMeshFileName[s++] = g_bAerialNavigation ? 'h' : 'v';

			CNavMesh::SaveBinary(pNavMeshHolder->m_pNavMesh, navMeshFileName);
		}
		else
		{
			OutputText("Error - bad filename\n");
		}
	}


	//*****************************************************
	// Delete all the data again
	//*****************************************************

	for(n=0; n<g_NavMeshes.size(); n++)
	{
		CNavMeshHolder * pNavMeshHolder = g_NavMeshes[n];
		if(pNavMeshHolder->m_pNavMesh)
		{
			CNavMeshAlloc<CNavMesh> navmeshAlloc;
			navmeshAlloc.Delete(pNavMeshHolder->m_pNavMesh);
		}
		delete pNavMeshHolder;
	}
	g_NavMeshes.clear();



	return true;
}



TNavMeshParams * GetByIndex(s32 iSectorX, s32 iSectorY, vector<TNavMeshParams*> & meshes)
{
	for(u32 i=0; i<meshes.size(); i++)
	{
		if(meshes[i]->m_iStartSectorX == iSectorX && meshes[i]->m_iStartSectorY == iSectorY)
		{
			return meshes[i];
		}
	}
	return NULL;
}

//---------------------------------------------------------------------------------------------------------
// NAME : StitchDLC
// PURPOSE : Stitches a set of DLC navmeshes, into an original set of navmeshes.
// INPUTS :
//		meshesDLC - array of every navmesh in DLC which contains any changed map assets
//		meshesOriginal - array of all the original navmeshes which shipped with the game

bool CNavMeshMaker::JoinMeshesDLC(char * pPathDLC, char * pOutputPathDLC)
{
	// Load the navmeshes from the DLC folder
	if(!CNavGen_NavMeshStore::LoadAll(pPathDLC))
	{
		CNavMeshMaker::OutputText("Error, couldn't load DLC navmeshes");
		return false;
	}

	int x,y;

	// Mark all DLC navmeshes as such
	for(y=0; y<CPathServerExtents::GetWorldDepthInSectors(); y+=CPathServerExtents::m_iNumSectorsPerNavMesh)
	{
		for(x=0; x<CPathServerExtents::GetWorldWidthInSectors(); x+=CPathServerExtents::m_iNumSectorsPerNavMesh)
		{
			const u32 index = CNavGen_NavMeshStore::GetMeshIndexFromSectorCoords(x, y);
			CNavMesh * pNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(index);
			if(pNavMesh)
			{
				pNavMesh->SetFlags( pNavMesh->GetFlags() | NAVMESH_DLC );
			}
		}
	}

	// Load the rest of the map from the main input folder
	if(!CNavGen_NavMeshStore::LoadAll(g_InputPath))
	{
		CNavMeshMaker::OutputText("Error, couldn't load original non-DLC navmeshes");
		return false;
	}

	// For each DLC navmesh, we need to process each edge which connects to the main map
	// Easiest way to structure this, is to visit every DLC navmesh and examine its neighbours
	// to see if any are non-DLC; if so then we copy off vertices/indices/polys into arrays, and
	// then visit each navmesh edge resolving t-junctions & connections.

	const s32 iOffsets[4][2] =
	{
		-CPathServerExtents::m_iNumSectorsPerNavMesh, 0,
		CPathServerExtents::m_iNumSectorsPerNavMesh, 0,
		0, -CPathServerExtents::m_iNumSectorsPerNavMesh,
		0, CPathServerExtents::m_iNumSectorsPerNavMesh
	};

	for(y=0; y<CPathServerExtents::GetWorldDepthInSectors(); y+=CPathServerExtents::m_iNumSectorsPerNavMesh)
	{
		for(x=0; x<CPathServerExtents::GetWorldWidthInSectors(); x+=CPathServerExtents::m_iNumSectorsPerNavMesh)
		{
			const u32 index = CNavGen_NavMeshStore::GetMeshIndexFromSectorCoords(x, y);
			CNavMesh * pNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(index);
			if(pNavMesh && ((pNavMesh->GetFlags()&NAVMESH_DLC)!=0))
			{
				bool bAdjoinsNonDLC = false;
				CNavMesh * pAdjacent[4] = { NULL, NULL, NULL, NULL };
				s32 iNumAdjacent = 0;

				for(s32 a=0; a<4; a++)
				{
					const s32 iX = x + iOffsets[a][0];
					const s32 iY = y + iOffsets[a][1];

					if(iX >= 0 && iX <= (CPathServerExtents::m_iNumNavMeshesInX*CPathServerExtents::m_iNumSectorsPerNavMesh)
						&& iY >= 0 && iY <= (CPathServerExtents::m_iNumNavMeshesInY*CPathServerExtents::m_iNumSectorsPerNavMesh))
					{
						const u32 adjIndex = CNavGen_NavMeshStore::GetMeshIndexFromSectorCoords(iX, iY);
						CNavMesh * pAdjNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(adjIndex);
						if(pAdjNavMesh && ((pAdjNavMesh->GetFlags()&NAVMESH_DLC)==0))
						{
							bAdjoinsNonDLC = true;
							pAdjacent[iNumAdjacent++] = pAdjNavMesh;
						}
					}
				}

				// If this DLC navmesh adjoins to any non-DLC, then we must process its edges
				if(bAdjoinsNonDLC)
				{
					CNavGen::JoinMeshesDLC(pNavMesh, pAdjacent);
				}
			}
		}
	}

	// Now save off all the DLC meshes to the DLC folder
	// Mark all DLC navmeshes as such

	char dlcFilename[1024];

	for(y=0; y<CPathServerExtents::GetWorldDepthInSectors(); y+=CPathServerExtents::m_iNumSectorsPerNavMesh)
	{
		for(x=0; x<CPathServerExtents::GetWorldWidthInSectors(); x+=CPathServerExtents::m_iNumSectorsPerNavMesh)
		{
			const u32 index = CNavGen_NavMeshStore::GetMeshIndexFromSectorCoords(x, y);
			CNavMesh * pNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(index);
			if(pNavMesh)
			{
				if( (pNavMesh->GetFlags() & NAVMESH_DLC) !=0 )
				{
					sprintf(dlcFilename, "%s/navmesh[%i][%i].inv", pOutputPathDLC, x, y);
					CNavMesh::SaveBinary(pNavMesh, dlcFilename);
				}
			}
		}
	}

	return true;
}


#if 0

bool CNavMeshMaker::DisableBadClimbsInNavMeshes()
{
	char textBuf[256];
	OutputText("Disabling climbs which don't work in the game engine.\n");

	char pathForNavDatFile[512];
	sprintf(pathForNavDatFile, "%s\\common\\data", g_pGameFolder);

	//**************************************************************************************************
	// Init the pathserver class.  This will load all the navmeshes in the world.
	//**************************************************************************************************
	bool bInitOk = CNavGen_NavMeshStore::LoadAll(g_InputPath);
	if(!bInitOk)
	{
		OutputText(g_pBlankLine);
		sprintf(textBuf, "DisableBadClimbs - CNavGen_NavMeshStore::LoadAll() returned false\n");
		OutputText(textBuf);
		return false;
	}

	//**************************************************************************************************
	// Load BadClimbsN_N.lst file for each navmesh, and disable the bad climbs within.
	// These list files are created by running the game with the newly resourced navmeshes, and
	// using the "-validateclimbs" parameter.
	//**************************************************************************************************

	char climbFileNameShort[512];
	char climbFileName[512];
	char navmeshFileNameShort[512];
	char navmeshFileName[512];

	for(int y=0; y<CPathServerExtents::m_iNumNavMeshesInY; y++)
	{
		for(int x=0; x<CPathServerExtents::m_iNumNavMeshesInX; x++)
		{
			int iNavMesh = (CPathServerExtents::m_iNumNavMeshesInX * y) + x;

			CNavMesh * pNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(iNavMesh);
			if(!pNavMesh)
				continue;

			sprintf(
				climbFileNameShort,
				"BadClimbs_%i_%i.lst",
				x * CPathServerExtents::m_iNumSectorsPerNavMesh,
				y * CPathServerExtents::m_iNumSectorsPerNavMesh
			);

			sprintf(climbFileName, "%s/%s", g_OutputPath, climbFileNameShort);

			fiStream * pFile = fiStream::Open(climbFileName);
			if(!pFile)
				continue;

			bool bDisabledClimb = false;

			int iNum;

			do 
			{
				Vector3 vClimbPos;
				iNum = fscanf(pFile, "%f %f %f", &vClimbPos.x, &vClimbPos.y, &vClimbPos.z);
				if(iNum==3)
				{
					TAdjPoly * pAdjPoly = NULL;
					if(pNavMesh->GetNonStandardAdjacancyAtPosition(vClimbPos, 0.1f, pAdjPoly) && pAdjPoly)
					{
						pAdjPoly->SetAdjacencyType(ADJACENCY_TYPE_NORMAL);
						pAdjPoly->m_iAdjacentNavMeshIndex = NAVMESH_NAVMESH_INDEX_NONE;
						pAdjPoly->SetNavMeshIndex(NAVMESH_NAVMESH_INDEX_NONE, pNavMesh->GetAdjacentMeshes());
						pAdjPoly->SetPolyIndex(NAVMESH_POLY_INDEX_NONE);
						pAdjPoly->SetOriginalNavMeshIndex(NAVMESH_NAVMESH_INDEX_NONE, pNavMesh->GetAdjacentMeshes());
						pAdjPoly->SetOriginalPolyIndex(NAVMESH_POLY_INDEX_NONE);

						bDisabledClimb = true;
					}
				}

			} while(iNum==3);

			pFile->Close();

			//**************************************************************************
			// If we found & disabled any climbs, then save out the navmesh file again

			if(bDisabledClimb)
			{
				sprintf(
					navmeshFileNameShort,
					"navmesh[%i][%i].inv",
					x * CPathServerExtents::m_iNumSectorsPerNavMesh,
					y * CPathServerExtents::m_iNumSectorsPerNavMesh
				);

				sprintf(navmeshFileName, "%s/%s", g_OutputPath, navmeshFileNameShort);

				CNavMesh::SaveBinary(pNavMesh, navmeshFileName);
			}
		}
	}

	return true;
}

#endif

//***********************************************************************************************************
// The purpose of this function is to create a mapping between a group of files with a sector-based naming
// convention based upon FILENAME[X][Y].EXT and a minimal zero-based indexing into these files.
//
// Previously, we had a number of possible navmesh/audmesh/navnode files whose 'index' in the streaming system,
// and in our internal arrays ranged from 0..10k, due to their subscript range of [0][0] to [200][200] and
// a navmesh size of 2x2 sectors (variable, it should be noted).

bool CNavMeshMaker::CreateMappingFile(char * pFileExtension, char * pFilenameForMappingFile)
{
	CNavMeshMaker::OutputText("CreateMappingFile\n");
	CNavMeshMaker::OutputText(pFileExtension);
	CNavMeshMaker::OutputText("\n");
	CNavMeshMaker::OutputText(pFilenameForMappingFile);
	CNavMeshMaker::OutputText("\n");

	vector<u32> mappingArray;

	CreateMappingArray(pFileExtension, mappingArray);

	// If we're doing navmeshes, then run again for navnodes.
	/*
	if(stricmp(pFileExtension, "inv")==0)
	{
		CreateMappingArray("ihn", mappingArray);
	}
	*/

	WriteMappingFile(pFilenameForMappingFile, mappingArray);

	CNavMeshMaker::OutputText("Done!\n");

	return true;
}

bool CNavMeshMaker::CreateMappingArray(char * pFileExtension, vector<u32> & mappingArray)
{
	CNavMeshMaker::OutputText("CreateMappingArray\n");

	//*****************************************************************************

	char tmp[256];

	char searchString[512];
	sprintf(searchString, "%s/*.%s", g_OutputPath, pFileExtension);

	WIN32_FIND_DATA findData;
	ZeroMemory(&findData, sizeof(WIN32_FIND_DATA));

	HANDLE h = FindFirstFile(searchString, &findData);

	if(h == INVALID_HANDLE_VALUE)
	{
		OutputText("No files - FindFirstFile returned INVALID_HANDLE_VALUE!\n");
	}
	else
	{
		BOOL bContinue = TRUE;
		while(bContinue)
		{
			OutputText(findData.cFileName);

			TNavMeshIndex iNavMesh;
			
			// NASTY SPECIAL-CASE HACK UNTIL NAVNODES HAVE THE SAME NAMING CONVENTION AS NAVMESHES & AUDMESHES
			if(stricmp(pFileExtension, "ihn")==0)
			{
				int iSectorX, iSectorY;
				sscanf(findData.cFileName, "%i_%i", &iSectorX, &iSectorY);

				static const int iNumSectorsPerNavNode =
					CPathServerExtents::m_iHierarchicalNodesBlockSize * CPathServerExtents::m_iNumSectorsPerNavMesh;

				// TODO: use new mesh store interface for all this
				int iNavMeshBlockX = (int) (iSectorX / iNumSectorsPerNavNode);
				int iNavMeshBlockY = (int) (iSectorY / iNumSectorsPerNavNode);

				if(iNavMeshBlockX < 0 || iNavMeshBlockX >= CPathServerExtents::m_iNumNavMeshesInX || iNavMeshBlockY < 0 || iNavMeshBlockY >= CPathServerExtents::m_iNumNavMeshesInY)
					return NAVMESH_NAVMESH_INDEX_NONE;

				const int iRatio = CPathServerExtents::m_iNumNavMeshesInX / CPathServerExtents::m_iHierarchicalNodesBlockSize;

				iNavMesh = (iNavMeshBlockY * iRatio) + iNavMeshBlockX;

				if(iNavMesh==NAVMESH_NODE_INDEX_NONE)
					iNavMesh = NAVMESH_NAVMESH_INDEX_NONE;
			}
			else
			{
				// Check the beginning of the filename to determine if it's an aerial navmesh, and if so,
				// include it only if that's what we want.
				if(IsAerialMeshFileName(findData.cFileName) == g_bAerialNavigation)
				{
					iNavMesh = CNavGen_NavMeshStore::GetNavMeshIndexFromFilename(findData.cFileName);
				}
				else
				{
					iNavMesh = NAVMESH_NAVMESH_INDEX_NONE;
				}
			}

			if(iNavMesh != NAVMESH_NAVMESH_INDEX_NONE)
			{
				sprintf(tmp, " = %i\n", iNavMesh);
				OutputText(tmp);

				// Only add this mapping if we've not already got it.
				// This can occur because we call this function twice - once for "inv" navmeshes,
				// and again for "ihn" navnodes, to ensure we have the whole map covered.

				u32 i;
				for(i=0; i<mappingArray.size(); i++)
				{
					if(mappingArray[i]==iNavMesh)
						break;
				}

				if(i==mappingArray.size())
				{
					mappingArray.push_back(iNavMesh);
				}
			}
			else
			{
				OutputText(" = NAVMESH_NAVMESH_INDEX_NONE\n");
			}

			bContinue = FindNextFile(h, &findData);
		}
	}

	return true;
}

bool CNavMeshMaker::WriteMappingFile(char * pFilenameForMappingFile, vector<u32> & mappingArray)
{
	CNavMeshMaker::OutputText("WriteMappingFile\n");

	//***********************************************************
	// Now we've generated the mappings, save them out to file

	if(mappingArray.size())
	{
		fiStream * pFile = fiStream::Create(pFilenameForMappingFile);
		if(pFile)
		{
			fprintf(pFile, "NUMENTRIES %i\n", mappingArray.size());

			for(u32 f=0; f<mappingArray.size(); f++)
			{
				const int iIndex = mappingArray[f];
				fprintf(pFile, "%i\n", iIndex);
			}
			pFile->Close();
		}
	}
	return true;
}


bool CNavMeshMaker::IsAerialMeshFileName(const char* pFilename)
{
	return strncmp(pFilename, g_AerialMeshName, strlen(g_AerialMeshName)) == 0;
}


bool CNavMeshMaker::ShouldCopyIntoSubfolder(const char* pFilename)
{
	return IsAerialMeshFileName(pFilename) == g_bAerialNavigation;
}


void CNavMeshMaker::CopyFilesIntoSubfoldersForRpfCreation(char * pFileExtension)
{
	static const int iNumSubFolders = 10;

	int s;

	//-----------------------------
	// Firstly create the folders

	for(s=0; s<iNumSubFolders; s++)
	{
		char folderName[512];
		sprintf(folderName, "%s/%i", g_OutputPath, s);

		BOOL bSuccess = CreateDirectory(folderName, NULL);
		if(!bSuccess)
			printf("Couldn't create directory \"%s\"\n", folderName);
	}

	//---------------------------------------------------------------------------------
	// Now count how many files of the given extension exist in the main output folder

	int iNumFiles = 0;

	char searchPath[512];
	sprintf(searchPath, "%s/*.%s", g_OutputPath, pFileExtension);
	WIN32_FIND_DATA findData;

	HANDLE hFile = FindFirstFile(searchPath, &findData);
	while(hFile != INVALID_HANDLE_VALUE)
	{
		// Only count and copy the files we care about, either the regular navmeshes
		// or the height meshes. Note that we could probably have used searchPath[] for
		// this, but having a separate function may be easier to maintain if we change
		// the naming convention.
		if(ShouldCopyIntoSubfolder(findData.cFileName))
		{
			iNumFiles++;
		}

		if(!FindNextFile(hFile, &findData))
			break;
	}

	//-------------------------------------------
	// Now copy files into the subfolders

	int iNumPerFolder = (iNumFiles / iNumSubFolders) +1;
	int iCurrentFolder = 0;
	int iNumInThisFolder = 0;

	hFile = FindFirstFile(searchPath, &findData);
	while(hFile != INVALID_HANDLE_VALUE)
	{
		if(ShouldCopyIntoSubfolder(findData.cFileName))
		{
			char oldFileName[512];
			sprintf(oldFileName, "%s/%s", g_OutputPath, findData.cFileName);
			char newFileName[512];
			sprintf(newFileName, "%s/%i/%s", g_OutputPath, iCurrentFolder, findData.cFileName);

			BOOL bMovedOk = CopyFile(oldFileName, newFileName, FALSE);
			if(!bMovedOk)
				printf("Couldn't copy file \"%s\" to \"%s\"\n", findData.cFileName, newFileName);

			iNumInThisFolder++;
			if(iNumInThisFolder >= iNumPerFolder)
			{
				iNumInThisFolder = 0;
				iCurrentFolder++;
				if(iCurrentFolder >= iNumSubFolders)
					iCurrentFolder = iNumSubFolders-1;
			}
		}

		if(!FindNextFile(hFile, &findData))
			break;
	}

}

int g_iTotalNumPolys = 0;
int g_iTotalNumVertices = 0;
int g_iTotalNumPoolSize = 0;
int g_iTotalNumCoverPoints = 0;
int g_iTotalNumSpecialLinks = 0;

void CNavMeshMaker::PrintNavMeshInfo()
{
	//---------------------------------------------------------------------------------
	// Now count how many files of the given extension exist in the main output folder

	char searchPath[512];
	sprintf(searchPath, "%s/*.%s", g_InputPath, "inv");
	WIN32_FIND_DATA findData;

	HANDLE hFile = FindFirstFile(searchPath, &findData);
	while(hFile != INVALID_HANDLE_VALUE)
	{
		PrintSingleNavMeshInfo(findData.cFileName);

		if(!FindNextFile(hFile, &findData))
			break;
	}

	char totals[512];
	sprintf(totals, "Total num polys : %i\n", g_iTotalNumPolys);
	Displayf(totals);
	sprintf(totals, "Total num vertices : %i\n", g_iTotalNumVertices);
	Displayf(totals);
	sprintf(totals, "Total num poolsize : %i\n", g_iTotalNumPoolSize);
	Displayf(totals);
	sprintf(totals, "Total num coverpoints : %i\n", g_iTotalNumCoverPoints);
	Displayf(totals);
	sprintf(totals, "Total num special links : %i\n", g_iTotalNumSpecialLinks);
	Displayf(totals);
}

void CNavMeshMaker::PrintSingleNavMeshInfo(char * pFilename)
{
	char fullFilename[512];
	sprintf(fullFilename, "%s\\%s", g_InputPath, pFilename);

	CNavMesh * pNavMesh = CNavMesh::LoadBinary(fullFilename);
	if(pNavMesh)
	{
		u32 iFullFileCRC = CalcFileCRC(fullFilename, 0);
		u32 iFileCRCExcludingBuildID = CalcFileCRC(fullFilename, 16);

		char tmp[256];
		
		Displayf("----------------------------------------------------------\n");

		sprintf(tmp, "Filename : %s\n", pFilename);
		Displayf(tmp);
		
		sprintf(tmp, "Full file CRC: %u\n", iFullFileCRC);
		Displayf(tmp);

		sprintf(tmp, "File CRC excluding BuildID: %u\n", iFileCRCExcludingBuildID);
		Displayf(tmp);

		sprintf(tmp, "BuildID: %u\n", pNavMesh->GetBuildID());
		Displayf(tmp);

		sprintf(tmp, "NumVerts: %i, NumPolys: %i, PoolSize: %i\n", pNavMesh->GetNumVertices(), pNavMesh->GetNumPolys(), pNavMesh->GetSizeOfPools());
		Displayf(tmp);

		sprintf(tmp, "NumCoverPoints: %i, NumSpecialLinks: %i", pNavMesh->GetNumCoverPoints(), pNavMesh->GetNumSpecialLinks());
		Displayf(tmp);

		g_iTotalNumPolys += pNavMesh->GetNumPolys();
		g_iTotalNumVertices += pNavMesh->GetNumVertices();
		g_iTotalNumPoolSize += pNavMesh->GetSizeOfPools();
		g_iTotalNumCoverPoints += pNavMesh->GetNumCoverPoints();
		g_iTotalNumSpecialLinks += pNavMesh->GetNumSpecialLinks();


		delete pNavMesh;
	}
}

}	// namespace rage
