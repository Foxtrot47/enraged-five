//
// fwnavgen/datatypes.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef FWNAVGEN_DATATYPES_H
#define FWNAVGEN_DATATYPES_H

#include "vector/vector3.h"
#include "vector/vector4.h"
#include "vector/matrix34.h"
#include "atl/array.h"

#include "ai/navmesh/shortminmax.h"

//-----------------------------------------------------------------------------

namespace rage
{

//-----------------------------------------------------------------------------

/*
PURPOSE
	fwNavLadderInfo contains information about a ladder in the game world,
	used when processing navigation data.
NOTES
	Adapted from TLadderInfo in GTA.
*/
struct fwNavLadderInfo
{
	void SetTop(const Vector3 & vTop) { fTopPos[0] = vTop.x; fTopPos[1] = vTop.y; fTopPos[2] = vTop.z; }
	void SetBase(const Vector3 & vBase) { fBasePos[0] = vBase.x; fBasePos[1] = vBase.y; fBasePos[2] = vBase.z; }
	void SetNormal(const Vector3 & vNormal) { fSurfaceNormal[0] = vNormal.x; fSurfaceNormal[1] = vNormal.y; fSurfaceNormal[2] = vNormal.z; }

	Vector3 GetTop() const { return Vector3(fTopPos[0], fTopPos[1], fTopPos[2]); }
	Vector3 GetBase() const { return Vector3(fBasePos[0], fBasePos[1], fBasePos[2]); }
	Vector3 GetNormal() const { return Vector3(fSurfaceNormal[0], fSurfaceNormal[1], fSurfaceNormal[2]); }

private:

	float fTopPos[3];
	float fBasePos[3];
	float fSurfaceNormal[3];
};

/*
PURPOSE
	fwDynamicEntityInfo contains information about a dynamic entity in the game world,
	used when processing navigation data.
NOTES
	
*/
class fwDynamicEntityInfo
{
public:
	enum
	{
		// This is an object which is fixed in place until knocked over by application of force above some threshold
		FLAG_UPROOTABLE_OBJECT	=	0x01,
		// This object needs to be climbable by AI when not uprooted, which will involve splitting navmesh polygons by its major axis
		FLAG_CLIMABABLE_BY_AI	=	0x02,
		// This object is a door
		FLAG_DOOR				=	0x04
	};

public:
	fwDynamicEntityInfo() { }
	~fwDynamicEntityInfo() { }

	Matrix34 m_Matrix;
	Vector4 m_Planes[4];
	Vector3 m_OOBB[2];
	Vector3 m_vCorners[4];
	TShortMinMax m_MinMax;
	float m_fTopZ;
	float m_fBottomZ;
	
	u32 m_iFlags;
};


//--------------------------------------------------------------
// CDLCArea
// Defines an area of DLC content, which has to be stitched into
// the shipped navmeshes without modification to them.

class CDLCArea
{
public:
	CDLCArea() { m_iFlags = 0; }
	~CDLCArea() { }

	Vector3 m_vMin;
	Vector3 m_vMax;
	u32 m_iFlags;

	static bool LoadDLCAreas(const char * pFilename, atArray<CDLCArea*> & areas);
};

//-----------------------------------------------------------------------------

}	// namespace rage

//-----------------------------------------------------------------------------

#endif	// FWNAVGEN_DATATYPES_H

// End of file fwnavgen/datatypes.h
