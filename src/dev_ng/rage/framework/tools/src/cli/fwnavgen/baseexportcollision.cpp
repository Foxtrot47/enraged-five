//
// fwnavgen/baseexportcollision.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "baseexportcollision.h"
#include "datatypes.h"
#include "helperfunc.h"
#include "materialinterface.h"
#include "ai/navmesh/navmesh.h"
#include "ai/navmesh/datatypes.h"
#include "entity/archetype.h"
#include "entity/entity.h"
#include "file/stream.h"
#include "fragment/type.h"
#include "fragment/typechild.h"
#include "spatialdata/sphere.h"
#include "phbound/boundbox.h"
#include "phbound/boundbvh.h"
#include "phbound/boundcapsule.h"
#include "phbound/boundcomposite.h"
#include "phbound/boundcylinder.h"
#include "phbound/boundgeom.h"
#include "phbound/boundsphere.h"
#include "phbound/OptimizedBvh.h"
#include "vector/vector2.h"
#include "vector/vector3.h"

#if __WIN32PC
#include "system/xtl.h"		// GetCurrentDirectory(), etc
#endif

using namespace rage;

using namespace fwNavToolHelperFuncs;
using namespace std;

#define __CHECK_FOR_DUPLICATE_TRIANGLES					0
#define __CHECK_FOR_DEGENERATE_TRIANGLES				0
#define __EXPORT_UNKNOWN_BOUNDS_AS_BOX					1
#define FLUSH_TRIANGLES_LIST_WHEN_THIS_MANY_VERTICES	16384

#define __PROMPT_FOR_OUTPUT_PATH						0

#define NUM_BOUND_PRIMITIVE_SLICES						(HEIGHTMAP_GENERATOR_TOOL ? 12 : 12) // TODO -- increase these for heightmap tool, but don't crash
#define NUM_CAPSULE_PRIMITIVE_CAP_STACKS				(HEIGHTMAP_GENERATOR_TOOL ? 4 : 1)
#define NUM_SPHERE_PRIMITIVE_STACKS						(HEIGHTMAP_GENERATOR_TOOL ? 12 : 8)

#define NUM_BIG_BOUND_PRIMITIVE_SLICES					36
#define NUM_BIG_SPHERE_PRIMITIVE_STACKS					36

#if HEIGHTMAP_GENERATOR_TOOL
	#define EXPORT_BOUND_NAME triData.m_pBoundName
#else
	#define EXPORT_BOUND_NAME "BOUND" // bound name is not available, but it could be .. it wouldn't be very difficult
#endif

// TODO: Maybe change how this thing works, it's not currently defined at the framework level.
XPARAM(exportFolder);

//-----------------------------------------------------------------------------

const float fwExportCollisionBaseTool::ms_fWaterSamplingStepSize = 0.25f; //1.0f;
const float fwExportCollisionBaseTool::ms_fSameVertexEps = 0.001f;

const float fwExportCollisionBaseTool::ms_fMinHeightToCoverBoxCylinder = 0.5f;
const float fwExportCollisionBaseTool::ms_fMinRadiusToCoverBoxCylinder = 0.25f;
const float fwExportCollisionBaseTool::ms_fMaxRadiusToCoverBoxCylinder = 1.25f;

fwNavMeshMaterialInterface * fwExportCollisionBaseTool::ms_pNavMeshStoredData	= NULL;

#if __CHECK_FOR_DUPLICATE_TRIANGLES

//bool IsThisTriangleDuplicated(Vector3 * verts, vector<Vector3*> & triangleVertices, CEntity * pEntity)
bool IsThisTriangleDuplicated(const Vector3 * verts, const vector<Vector3> & triangleVertices)
{
	static const float fEps = 0.01f;

	for(s32 t=0; t<(int)triangleVertices.size(); t+=3)
	{
		Vector3 vTriVerts[3];
		vTriVerts[0] = triangleVertices[t];
		vTriVerts[1] = triangleVertices[t+1];
		vTriVerts[2] = triangleVertices[t+2];

		if(vTriVerts[0].IsClose(verts[0], fEps) && vTriVerts[1].IsClose(verts[1], fEps) && vTriVerts[2].IsClose(verts[2], fEps))
		{
//			char * pModelName;
//			if(pEntity)
//			{
//				CBaseModelInfo * pModelInfo = CModelInfo::GetModelInfo(pEntity->GetModelIndex());
//				Displayf("ARTWORK ERROR : Model %s at (%.2f,%.2f,%.f2) has duplicated geometry.\n", pModelInfo->GetModelName(), pEntity->GetPosition().x, pEntity->GetPosition().y, pEntity->GetPosition().z);
//			}
//			else
//			{
				Displayf("ARTWORK ERROR : Static Collision Mesh at (%.2f,%.2f,%.f2) has duplicated geometry.\n", verts[0].x, verts[0].y, verts[0].z);
//			}
			return true;
		}
	}
	return false;
}

#endif	// __CHECK_FOR_DUPLICATE_TRIANGLES

fwExportCollisionBaseTool::fwExportCollisionBaseTool(fwLevelProcessTool * pTool) :
	fwLevelProcessToolExporterInterface(pTool)
		, m_vActualMinOfGeomInTriFile(0.0f, 0.0f, 0.0f)
		, m_vActualMaxOfGeomInTriFile(0.0f, 0.0f, 0.0f)
		, m_vNavMeshMins(0.0f, 0.0f, 0.0f)
		, m_vNavMeshMaxs(0.0f, 0.0f, 0.0f)
		, m_pCurrentTriFile(NULL)
		, m_pWaterLevels(NULL)
		, m_fAverageWaterLevelInCurrentNavMesh(0.0f)
		, m_iNumTrianglesThisTriFile(0)
#if HEIGHTMAP_GENERATOR_TOOL
		, m_iNumTrianglesExported(0)
		, m_iCombinedArchetypeFlags(0)
		, m_iExportedArchetypeFlags(0)
		, m_vExportBoundsMin(V_ZERO)
		, m_vExportBoundsMax(V_ZERO)
		, m_vIntersectRegionMin(V_ZERO)
		, m_vIntersectRegionMax(V_ZERO)
		, m_bIntersectRegionHit(false)
#endif
		, m_iHasWaterInCurrentNavMesh(0)
{
	m_OutputPath[0] = '\0';
	m_TriFileName[0] = '\0';
}


fwExportCollisionBaseTool::~fwExportCollisionBaseTool()
{
	// TODO: should probably make this one non-static, at least if we're deleting it
	// like this!
	if(ms_pNavMeshStoredData)
	{
		delete ms_pNavMeshStoredData;
		ms_pNavMeshStoredData = NULL;
	}
}


bool fwExportCollisionBaseTool::BaseOpenTriFile(const char *pNameToUse,
		const Vector2 &worldMin, const Vector3 &navMeshMins, const Vector3 &navMeshMaxs,
		int currentSectorX, int currentSectorY, int numSectorsPerNavMesh,
		float worldWidthOfSector)
{
	m_vActualMinOfGeomInTriFile = Vector3(FLT_MAX, FLT_MAX, FLT_MAX);
	m_vActualMaxOfGeomInTriFile = Vector3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	u32 iBlockSize = numSectorsPerNavMesh;	//CPathServerExtents::m_iNumSectorsPerNavMesh;

	Assert(!m_pCurrentTriFile);
	CloseTriFile();

	m_pCurrentTriFile = fiStream::Create(pNameToUse);
	if(!m_pCurrentTriFile)
		return false;

	//*****************
	// write a header
	//*****************

	m_pCurrentTriFile->Write(TColPolyData::m_TriFileHeader, 8);

	//**************************************************************************************
	// write an UID for this file.  This is used to make sure the .tri files and .oct files
	// match up properly.  We can just use the elapsed time for this
	//**************************************************************************************

	u32 iFlags = 0;
	m_pCurrentTriFile->WriteInt(&iFlags, 1);

	//*********************************************************************************************
	// write an empty integer, which will will later overwrite with the num triangles in the file

	u32 iNumTrianges = 0;
	m_pCurrentTriFile->WriteInt(&iNumTrianges, 1);

	//************************************************************************
	// Write the min of the world; this is where the sector numbering starts
	//************************************************************************

	m_pCurrentTriFile->WriteFloat(&worldMin.x, 1);
	m_pCurrentTriFile->WriteFloat(&worldMin.y, 1);

	//**************************************************
	// Write the extents of this block of sectors
	//**************************************************

	m_pCurrentTriFile->WriteFloat(&navMeshMins.x, 1);
	m_pCurrentTriFile->WriteFloat(&navMeshMins.y, 1);
	m_pCurrentTriFile->WriteFloat(&navMeshMins.z, 1);

	m_pCurrentTriFile->WriteFloat(&navMeshMaxs.x, 1);
	m_pCurrentTriFile->WriteFloat(&navMeshMaxs.y, 1);
	m_pCurrentTriFile->WriteFloat(&navMeshMaxs.z, 1);

	m_pCurrentTriFile->WriteInt(&currentSectorX, 1);
	m_pCurrentTriFile->WriteInt(&currentSectorY, 1);

	m_pCurrentTriFile->WriteInt(&iBlockSize, 1);
	m_pCurrentTriFile->WriteInt(&iBlockSize, 1);

	//********************************************************************************
	//	Write the water level, etc for this area.  We always write whether the navmesh
	//	has water + the average height.  We can then also optionally write out a 2d
	//	array of water heights which, if present, should be used in preference to the
	//	average water level

	m_pCurrentTriFile->WriteByte(&m_iHasWaterInCurrentNavMesh, 1);
	m_pCurrentTriFile->WriteFloat(&m_fAverageWaterLevelInCurrentNavMesh, 1);
	u8 bHasVariableWaterLevel = (m_pWaterLevels != NULL);
	m_pCurrentTriFile->WriteByte(&bHasVariableWaterLevel, 1);

	if(bHasVariableWaterLevel)
	{
		float fSampleSpacing = (worldWidthOfSector * ((float)iBlockSize)) / ms_fWaterSamplingStepSize;
		s32 iNumSamples = (s32)(fSampleSpacing * fSampleSpacing);

		m_pCurrentTriFile->WriteFloat(&ms_fWaterSamplingStepSize, 1);
		m_pCurrentTriFile->WriteInt(&iNumSamples, 1);

		for(int s=0; s<iNumSamples; s++)
		{
			s16 iWaterLevel = m_pWaterLevels[s];
			m_pCurrentTriFile->WriteShort(&iWaterLevel, 1);
		}
	}
	return true;
}


bool fwExportCollisionBaseTool::FlushTriListToTriFile(vector<Vector3> & triangleVertices, vector<phMaterialMgr::Id> & triangleMaterials, vector<u16> & colPolyFlags, vector<u32> & archetypeFlags, vector<fwNavTriData> & triDataArray)
{
	if(!m_pCurrentTriFile)
	{
		OpenTriFile(m_TriFileName);
		Assert(m_pCurrentTriFile);
	}

	Vector3 verts[3];
	phMaterialMgr::Id iMtl;
	u8 iPerPolyBitfield;

	Assert((triangleMaterials.size()*3) == triangleVertices.size());
	Assert((colPolyFlags.size()*3) == triangleVertices.size());
	Assert((archetypeFlags.size()*3) == triangleVertices.size());
	Assert((triDataArray.size()*3) == triangleVertices.size());

	s32 iTriIndex = 0;

	for(u32 v=0; v<triangleVertices.size(); v+=3)
	{
		verts[2] = triangleVertices[v];
		verts[1] = triangleVertices[v+1];
		verts[0] = triangleVertices[v+2];

		iMtl = triangleMaterials[iTriIndex];
		u16 iColPolyFlags = colPolyFlags[iTriIndex];
		u32 iArchetypeFlags = archetypeFlags[iTriIndex];
		const fwNavTriData & triData = triDataArray[iTriIndex];

		// Construct 8bit value to be baked into the poly.  These values are preserved in the navmesh.
		iPerPolyBitfield = GetBakedInAttributesForTriangle(verts, iMtl);

		TColPolyData colPolyData;
		colPolyData.SetIsStairs( ms_pNavMeshStoredData->IsThisSurfaceTypeStairs(iMtl) );
		colPolyData.SetIsPavement( ms_pNavMeshStoredData->IsThisSurfaceTypePavement(iMtl) );
		colPolyData.SetIsSeeThrough( ms_pNavMeshStoredData->IsThisSurfaceTypeSeeThrough(iMtl) );
		colPolyData.SetIsShootThrough( ms_pNavMeshStoredData->IsThisSurfaceTypeShootThrough(iMtl) );
		colPolyData.SetIsWalkable( ms_pNavMeshStoredData->IsThisSurfaceWalkable(verts, iMtl) );
		colPolyData.SetIsNotClimbableCollision( !ms_pNavMeshStoredData->IsThisSurfaceClimbable(iMtl) );
		colPolyData.SetIsFixedObject( ((iColPolyFlags & EXPORTPOLYFLAG_FIXEDOBJECT)!=0) );
		colPolyData.SetDontCreateNavMeshOn( ((iColPolyFlags & EXPORTPOLYFLAG_DONT_CREATE_NAVMESH_ON_THIS)!=0) );
		colPolyData.SetIsInterior( ((iColPolyFlags & EXPORTPOLYFLAG_INTERIOR)!=0) );
		colPolyData.SetDoesntProvideCover( ((iColPolyFlags & EXPORTPOLYFLAG_DOESNTPROVIDECOVER)!=0) );
		colPolyData.SetIsClimbableObject( ((iColPolyFlags & EXPORTPOLYFLAG_CLIMBABLEOBJECT)!=0) );
		colPolyData.SetIsExteriorPortal( ((iColPolyFlags & EXPORTPOLYFLAG_EXTERIOR_PORTAL)!=0) );

		// This call sets the TColPolyData's archetype bitfield based upon the game-specific interpretation of 'iArchetypeFlags'
		ms_pNavMeshStoredData->SetCollisionPolyDataFromArchetypeFlags(colPolyData, iArchetypeFlags);

		// Set colpoly data which is not valid for vehicle navmeshes
		if((iColPolyFlags & EXPORTPOLYFLAG_VEHICLE)==0)
		{
			colPolyData.SetIsRoad( ms_pNavMeshStoredData->IsThisSurfaceTypeRoad(verts, iMtl) );
			colPolyData.SetIsTrainTracks( ms_pNavMeshStoredData->IsThisSurfaceTypeTrainTracks(verts, iMtl) );
			colPolyData.SetIsNoNetworkSpawn( ms_pNavMeshStoredData->IsThisSurfaceAllowedForNetworkRespawning(iMtl)==false );
		}
		// Or set to sensible defaults
		else
		{
			colPolyData.SetIsMoverBound(true);
			colPolyData.SetIsRiverBound(false);
			colPolyData.SetIsWeaponBound(false);
			colPolyData.SetIsCoverBound(false);
			colPolyData.SetIsStairSlopeBound(false);
#if HEIGHTMAP_GENERATOR_TOOL
			colPolyData.SetIsVehicleBound(true);
			colPolyData.SetIsDefaultBound(true);
#endif
			colPolyData.SetIsNoNetworkSpawn( true );
		}

		s32 iPedDensity = (s32) ms_pNavMeshStoredData->GetPedDensityForThisSurfaceType(iMtl);
		Assert(iPedDensity >= 0 && iPedDensity <= MAX_PED_DENSITY);

		colPolyData.SetPedDensity( iPedDensity );
		colPolyData.m_triData = triData;
#if HEIGHTMAP_GENERATOR_TOOL
		const char * vfxGroupName = NULL;
		colPolyData.m_triData.m_iMaterialId = iMtl;
		colPolyData.m_triData.m_iMaterialVfxGroupId	= (u8)ms_pNavMeshStoredData->GetMaterialVfxGroup(iMtl, vfxGroupName);
		colPolyData.m_triData.m_iMaterialMaskIndex	= (u8)ms_pNavMeshStoredData->GetMaterialMaskIndex(iMtl);
		colPolyData.m_triData.m_iMaterialProcIndex	= (u8)ms_pNavMeshStoredData->GetMaterialProcIndex(iMtl);
#endif

		// Force fixed objects to have zero ped-density.  It seems that fixed objects have their per-poly flags set wrong.
		if(colPolyData.GetIsFixedObject())
		{
			colPolyData.SetPedDensity(0);
			colPolyData.SetIsPavement(false);
		}

		// Glass polygons should not be walkable
		//	bool bIsGlass = PGTAMATERIALMGR->GetIsGlass(iMtl);
		bool bIsGlass = ms_pNavMeshStoredData->IsThisSurfaceTypeGlass(iMtl);
		if(bIsGlass)
			colPolyData.SetIsWalkable(false);

		m_pCurrentTriFile->WriteByte(&iPerPolyBitfield, 1);

		colPolyData.Write(m_pCurrentTriFile);

		int p;
		for(p=0; p<3; p++)
		{
			m_pCurrentTriFile->WriteFloat(&verts[p].x, 1);
			m_pCurrentTriFile->WriteFloat(&verts[p].y, 1);
			m_pCurrentTriFile->WriteFloat(&verts[p].z, 1);

			if(verts[p].x < m_vActualMinOfGeomInTriFile.x) m_vActualMinOfGeomInTriFile.x = verts[p].x;
			if(verts[p].y < m_vActualMinOfGeomInTriFile.y) m_vActualMinOfGeomInTriFile.y = verts[p].y;
			if(verts[p].z < m_vActualMinOfGeomInTriFile.z) m_vActualMinOfGeomInTriFile.z = verts[p].z;
			if(verts[p].x > m_vActualMaxOfGeomInTriFile.x) m_vActualMaxOfGeomInTriFile.x = verts[p].x;
			if(verts[p].y > m_vActualMaxOfGeomInTriFile.y) m_vActualMaxOfGeomInTriFile.y = verts[p].y;
			if(verts[p].z > m_vActualMaxOfGeomInTriFile.z) m_vActualMaxOfGeomInTriFile.z = verts[p].z;
		}

		m_iNumTrianglesThisTriFile++;

		iTriIndex++;
	}

	triangleVertices.clear();
	triangleMaterials.clear();
	colPolyFlags.clear();
	archetypeFlags.clear();
	triDataArray.clear();

	return true;
}


void fwExportCollisionBaseTool::CloseTriFile(bool setExportFlags)
{
	if(m_pCurrentTriFile)
	{
		if(setExportFlags)
		{
			// Now we've finished writing the tri file, rewind to the start of the file
			// and update the tri-file flags to say whether there was any collision exported here.

			m_pCurrentTriFile->Seek(NAVMESH_EXPORT_TRIFILE_OFFSET_OF_FLAGS);

			u32 iFlags = 0;
			if(m_iNumTrianglesThisTriFile>0)
				iFlags |= NAVMESH_EXPORTFLAG_HAS_COLLISION_MESH;
			m_pCurrentTriFile->WriteInt(&iFlags, 1);

			// now write the number of vertices which were in the file

			m_pCurrentTriFile->Seek(NAVMESH_EXPORT_TRIFILE_OFFSET_OF_NUMVERTICES);
			m_pCurrentTriFile->WriteInt(&m_iNumTrianglesThisTriFile, 1);
		}

		m_pCurrentTriFile->Close();
		m_pCurrentTriFile = NULL;
	}
}


bool fwExportCollisionBaseTool::WriteExtrasFile(const char *pFilename, const vector<fwNavLadderInfo> & laddersList,
		const atArray<Vector3> & carNodesList, const vector<spdSphere> & portalBoundaries, const std::vector<fwEntity*> & dynamicEntities)
{
	if(!laddersList.size() && !carNodesList.size() && !portalBoundaries.size() && !dynamicEntities.size()) return false;

	fiStream * pExtrasFile = fiStream::Create(pFilename);
	if(!pExtrasFile)
		return false;

	//******************************************************************************
	//	Ladders

	fprintf(pExtrasFile, "NUM_LADDERS %i\n", laddersList.size());

	for(u32 l=0; l<laddersList.size(); l++)
	{
		const fwNavLadderInfo & ladderInfo = laddersList[l];

		Assert(ladderInfo.GetTop().x >= m_vNavMeshMins.x && ladderInfo.GetTop().x <= m_vNavMeshMaxs.x);
		Assert(ladderInfo.GetTop().y >= m_vNavMeshMins.y && ladderInfo.GetTop().y <= m_vNavMeshMaxs.y);

		fprintf(pExtrasFile, "TOP %.2f %.2f %.2f BOTTOM %.2f %.2f %.2f NORMAL %.2f %.2f %.2f\n",
			ladderInfo.GetTop().x, ladderInfo.GetTop().y, ladderInfo.GetTop().z,
			ladderInfo.GetBase().x, ladderInfo.GetBase().y, ladderInfo.GetBase().z,
			ladderInfo.GetNormal().x, ladderInfo.GetNormal().y, ladderInfo.GetNormal().z
			);
	}

	//******************************************************************************
	//	Car-Nodes

	fprintf(pExtrasFile, "NUM_CARNODES %i\n", carNodesList.size());

	for(s32 n=0; n<carNodesList.size(); n++)
	{
		const Vector3 & vNodePos = carNodesList[n];
		fprintf(pExtrasFile, "%.1f %.1f %.1f\n", vNodePos.x, vNodePos.y, vNodePos.z);
	}

	//******************************************************************************
	//	Portals onto main map

	fprintf(pExtrasFile, "NUM_PORTALS %i\n", portalBoundaries.size());

	for(u32 n=0; n<portalBoundaries.size(); n++)
	{
		const spdSphere & sphere = portalBoundaries[n];
		const Vec3V sphereCenter = sphere.GetCenter();
		fprintf(pExtrasFile, "%.1f %.1f %.1f %.1f\n", sphereCenter.GetXf(), sphereCenter.GetYf(), sphereCenter.GetZf(), sphere.GetRadiusf());
	}

	//***********************
	//	Dynamic entities

#if __DEV
	static bool bBreakOnName = false;
	static bool bBreakOnPosition = false;
	static char breakName[256];
	sprintf(breakName, "prop_fnclink_07d");
	static Vector3 vBreakPos(1987.78f, 3816.22f, 31.35f);
	static float fBreakEps = 0.2f;
#endif


	char entityName[256];

	fprintf(pExtrasFile, "NUM_DYNAMIC_ENTITIES %i\n", dynamicEntities.size());

	for(u32 n=0; n<dynamicEntities.size(); n++)
	{
		const fwEntity * pEntity = dynamicEntities[n];
		if(pEntity->GetModelId().IsValid())
		{
#if __DEV
			if(bBreakOnName && stricmp(breakName, pEntity->GetModelName())==0)
			{
				bBreakOnName = bBreakOnName;
			}
			if(bBreakOnPosition && vBreakPos.IsClose( VEC3V_TO_VECTOR3(pEntity->GetTransform().GetPosition()), fBreakEps ))
			{
				bBreakOnPosition = bBreakOnPosition;
			}
#endif

#if !__NO_OUTPUT
			strcpy(entityName, pEntity->GetModelName());
#else
			strcpy(entityName, "(unknown)");
#endif

			const fwArchetype * pArchetype = fwArchetypeManager::GetArchetype(pEntity->GetModelId());

			Matrix34 mEntity;
			pEntity->GetMatrixCopy(mEntity);

			Vector3 vMin = pArchetype->GetBoundingBoxMin();
			Vector3 vMax = pArchetype->GetBoundingBoxMax();

			u32 iFlags = ms_pNavMeshStoredData->GetDynamicEntityInfoFlags(pEntity);

			fprintf(pExtrasFile, "%.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %.3f %u %s\n",
				mEntity.a.x, mEntity.a.y, mEntity.a.z,
				mEntity.b.x, mEntity.b.y, mEntity.b.z,
				mEntity.c.x, mEntity.c.y, mEntity.c.z,
				mEntity.d.x, mEntity.d.y, mEntity.d.z,
				vMin.x, vMin.y, vMin.z, vMax.x, vMax.y, vMax.z,
				iFlags, entityName);
		}
	}

	fprintf(pExtrasFile, "\n");
	fprintf(pExtrasFile, "\n");

	pExtrasFile->Close();

	return true;
}


#if __WIN32PC

void fwExportCollisionBaseTool::GetOutputFolder()
{
#if __PROMPT_FOR_OUTPUT_PATH	// Prompt for an output path

	bool bOk = BANKMGR.OpenFile(m_OutputPath, 512, "*.*", false, "Select output folder..");
	if(!bOk)
	{
		return false;
	}
	// Remove the selected filename, leaving only the path
	char * ptr = &m_OutputPath[ strlen(m_OutputPath)-1 ];
	while(ptr && ptr != m_OutputPath && *ptr != '/' && *ptr != '\\') ptr--;
	if(*ptr == '\\' || *ptr == '/')
		*ptr = 0;

#else	// Automatically create output path

	const char * pExportFolder = NULL;
	PARAM_exportFolder.Get(pExportFolder);

	if(pExportFolder)
	{
		safecpy(m_OutputPath, pExportFolder);
	}
	else
	{
		char pCurrDir[256];
		GetCurrentDirectory(256, pCurrDir);
		strlwr(pCurrDir);

		char * pBuild = strstr(pCurrDir, "build");
		if(pBuild)
		{
			if(pBuild > pCurrDir) pBuild--;
			*pBuild = 0;
		}

		formatf(m_OutputPath, "%s/exported_navmeshes", pCurrDir);
	}

	WIN32_FIND_DATA findData;
	ZeroMemory(&findData, sizeof(WIN32_FIND_DATA));
	HANDLE hExistingFolder = FindFirstFile(m_OutputPath, &findData);

	// Folder not found
	if(hExistingFolder==INVALID_HANDLE_VALUE)
	{
		ASSERT_ONLY(BOOL bOk =) CreateDirectory(m_OutputPath, NULL);
		Assert(bOk);
	}
	// Folder or file found, make sure its a directory
	else
	{
		Assert((findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)!=0);
	}

#endif
}

#else	// __WIN32PC

void fwExportCollisionBaseTool::GetOutputFolder()
{
	const char * pExportFolder = NULL;
	PARAM_exportFolder.Get(pExportFolder);

	if(pExportFolder)
	{
		printf("Export Folder : %s", pExportFolder);
		safecpy(m_OutputPath, pExportFolder);
	}
	else
	{
		BANKMGR.OpenFile(m_OutputPath, 512, "*.*", true, "Select export path (type any filename above)..");

		// Remove the selected filename, leaving only the path
		char * ptr = &m_OutputPath[ strlen(m_OutputPath)-1 ];
		while(ptr && ptr != m_OutputPath && *ptr != '/' && *ptr != '\\') ptr--;
		if(*ptr == '\\' || *ptr == '/')
			*ptr = 0;
	}



}

#endif	// !__WIN32PC

#define __BREAK_ON_SPECIFIC_TRIANGLE	(__DEV && 0)
#define __USE_CULL_AREA					0
#define __USE_OMIT_AREA					0

bool fwExportCollisionBaseTool::CheckTriangleAgainstNavMeshExtentsAndAddToList(const Vector3 pVerts[3], phMaterialMgr::Id iMaterial, vector<Vector3> & triangleVertices, vector<phMaterialMgr::Id> & triangleMaterials, vector<u16> & colPolyFlags, u16 iColPolyFlag, vector<u32> & archetypeFlags, u32 iArchetypeFlags, vector<fwNavTriData> & triDataArray, const fwNavTriData & triData)
{
	if (!IsFiniteStable(RCC_VEC3V(pVerts[0])) ||
		!IsFiniteStable(RCC_VEC3V(pVerts[1])) ||
		!IsFiniteStable(RCC_VEC3V(pVerts[2])))
	{
		return false;
	}

#if HEIGHTMAP_GENERATOR_TOOL
	if(!IsZeroAll(m_vIntersectRegionMin) || !IsZeroAll(m_vIntersectRegionMax))
	{
		const Vec3V vTriBoundsMin = Min(RCC_VEC3V(pVerts[0]), RCC_VEC3V(pVerts[1]), RCC_VEC3V(pVerts[2]));
		const Vec3V vTriBoundsMax = Max(RCC_VEC3V(pVerts[0]), RCC_VEC3V(pVerts[1]), RCC_VEC3V(pVerts[2]));

		if(spdAABB(m_vIntersectRegionMin, m_vIntersectRegionMax).IntersectsAABB(spdAABB(vTriBoundsMin, vTriBoundsMax)))
			m_bIntersectRegionHit = true;
	}
#endif

#if __CHECK_TRIANGLES_AGAINST_NAVMESH_EXTENTS
	if(pVerts[0].x < m_vNavMeshMins.x && pVerts[1].x < m_vNavMeshMins.x && pVerts[2].x < m_vNavMeshMins.x)
		return false;
	if(pVerts[0].y < m_vNavMeshMins.y && pVerts[1].y < m_vNavMeshMins.y && pVerts[2].y < m_vNavMeshMins.y)
		return false;
	if(pVerts[0].z < m_vNavMeshMins.z && pVerts[1].z < m_vNavMeshMins.z && pVerts[2].z < m_vNavMeshMins.z)
		return false;

	if(pVerts[0].x > m_vNavMeshMaxs.x && pVerts[1].x > m_vNavMeshMaxs.x && pVerts[2].x > m_vNavMeshMaxs.x)
		return false;
	if(pVerts[0].y > m_vNavMeshMaxs.y && pVerts[1].y > m_vNavMeshMaxs.y && pVerts[2].y > m_vNavMeshMaxs.y)
		return false;
	if(pVerts[0].z > m_vNavMeshMaxs.z && pVerts[1].z > m_vNavMeshMaxs.z && pVerts[2].z > m_vNavMeshMaxs.z)
		return false;
#endif

#if __CHECK_FOR_DEGENERATE_TRIANGLES
	// Check that this isn't a degenerate tri
	if(AreVerticesEqual(pVerts[0], pVerts[1], ms_fSameVertexEps) || AreVerticesEqual(pVerts[1], pVerts[2], ms_fSameVertexEps) || AreVerticesEqual(pVerts[2], pVerts[0], ms_fSameVertexEps))
	{
		return false;
	}
#endif

#if __CHECK_FOR_DUPLICATE_TRIANGLES
	if(IsThisTriangleDuplicated(pVerts, triangleVertices))
	{
		printf("ART ERROR - duplicate triangle found.\n");
		return false;
	}
#endif

#if __BREAK_ON_SPECIFIC_TRIANGLE
	static Vector3 vTestVerts[3] =
	{
		Vector3(1656.37f, 2547.90f, 49.43f),
		Vector3(1648.30f, 2538.76f, 49.43f),
		Vector3(1656.14f, 2548.10f, 44.39f)
	};
	static bool bBreak = false;
	int iNumSame = 0;
	for(int p1=0; p1<3; p1++)
	{
		for(int p2=0; p2<3; p2++)
		{
			if(vTestVerts[p1].IsClose(pVerts[p2], 0.1f))
				iNumSame++;
		}
	}
	if(iNumSame == 3)
	{
		Assert(false);
		bBreak = true;
	}
#endif

#if __USE_CULL_AREA
	static const Vector3 vCullMin(-100.0f, -100.0f, -500.0f);
	static const Vector3 vCullMax(100.0f, 100.0f, 500.0f);

	Vector3 vTriMin(FLT_MAX, FLT_MAX, FLT_MAX);
	Vector3 vTriMax(-FLT_MAX, -FLT_MAX, -FLT_MAX);
	for(s32 v=0; v<3; v++)
	{
		vTriMin.x = Min(vTriMin.x, pVerts[v].x);
		vTriMin.y = Min(vTriMin.y, pVerts[v].y);
		vTriMin.z = Min(vTriMin.z, pVerts[v].z);
		vTriMax.x = Max(vTriMax.x, pVerts[v].x);
		vTriMax.y = Max(vTriMax.y, pVerts[v].y);
		vTriMax.z = Max(vTriMax.z, pVerts[v].z);
	}

	if(vTriMax.x < vCullMin.x || vTriMax.y < vCullMin.y || vTriMax.z < vCullMin.z || vCullMax.x < vTriMin.x || vCullMax.y < vTriMin.y || vCullMax.z < vTriMin.z)
	{
		return true;
	}
#endif

#if __USE_OMIT_AREA
	static const Vector3 vOmitOrigin( 2476.8f, 4949.7f, 45.61f );
	static const float fOmitSize = 30.0f;

	for(s32 v=0; v<3; v++)
	{
		if( (pVerts[v] - vOmitOrigin).Mag2() < fOmitSize*fOmitSize)
		{
			return true;
		}
	}
#endif

#if HEIGHTMAP_GENERATOR_TOOL
	m_iNumTrianglesExported++;
	m_iExportedArchetypeFlags |= iArchetypeFlags;
	m_vExportBoundsMin = Min(RCC_VEC3V(pVerts[0]), RCC_VEC3V(pVerts[1]), RCC_VEC3V(pVerts[2]), m_vExportBoundsMin);
	m_vExportBoundsMax = Max(RCC_VEC3V(pVerts[0]), RCC_VEC3V(pVerts[1]), RCC_VEC3V(pVerts[2]), m_vExportBoundsMax);
#endif

	triangleVertices.push_back(pVerts[0]);
	triangleVertices.push_back(pVerts[1]);
	triangleVertices.push_back(pVerts[2]);

	triangleMaterials.push_back(iMaterial);
	colPolyFlags.push_back(iColPolyFlag);
	archetypeFlags.push_back(iArchetypeFlags);
	triDataArray.push_back(triData);

	if(triangleVertices.size() >= FLUSH_TRIANGLES_LIST_WHEN_THIS_MANY_VERTICES)
	{
		FlushTriListToTriFile(triangleVertices, triangleMaterials, colPolyFlags, archetypeFlags, triDataArray);
	}

	return true;
}

void fwExportCollisionBaseTool::ExportBound(const phBound * pBound, const fragType * pFragType, const Matrix34 & mat, vector<Vector3> & triangleVertices, vector<phMaterialMgr::Id> & triangleMaterials, vector<u16> & colPolyFlags, u16 iColPolyFlag, vector<u32> & archetypeFlags, u32 iArchetypeFlags, vector<fwNavTriData> & triDataArray, const fwNavTriData & in_triData)
{
	if(!pBound)
		return;

	int iBoundType = pBound->GetType();

	fwNavTriData triData = in_triData;

#if HEIGHTMAP_GENERATOR_TOOL
	static bool bStop = false;

	if (bStop) // break here to stop the program and copy the console output
	{
		bStop = false;
		system("pause");
	}

	triData.m_iFlagsStruct.m_iBoundType = (u8)iBoundType;

	if (iBoundType == phBound::COMPOSITE)
	{
		triData.m_iFlagsStruct.m_bIsBoundCompositeChild = true;
	}
#endif

	switch(iBoundType)
	{
	case phBound::BVH:
		{
			const phBoundBVH * pBVH = static_cast<const phBoundBVH*>(pBound);
			AddBVHTrianglesToList(pBVH, mat, triangleVertices, triangleMaterials, colPolyFlags, iColPolyFlag, archetypeFlags, iArchetypeFlags, triDataArray, triData);
			break;
		}
	case phBound::BOX:
		{
			const phBoundBox * pBox = static_cast<const phBoundBox*>(pBound);
			AddBoxTrianglesToList(pBox, mat, triangleVertices, triangleMaterials, colPolyFlags, iColPolyFlag, archetypeFlags, iArchetypeFlags, triDataArray, triData);
			break;
		}
	case phBound::GEOMETRY:
		{
			const phBoundGeometry * pGeom = static_cast<const phBoundGeometry*>(pBound);
			AddGeometryTrianglesToList(pGeom, mat, triangleVertices, triangleMaterials, colPolyFlags, iColPolyFlag, archetypeFlags, iArchetypeFlags, triDataArray, triData);
			break;
		}
	case phBound::COMPOSITE:
		{
			const phBoundComposite * pBoundComposite = static_cast<const phBoundComposite*>(pBound);
			for (s32 i=0; i<pBoundComposite->GetNumBounds(); i++)
			{
				// check this bound is still valid (smashable code may have removed it)
				phBound * pChildBound = pBoundComposite->GetBound(i);
				if (pChildBound)
				{
					/*
					if(pFragType && pFragType->GetHasAnyArticulatedParts())
					{
						int nGroupIndex = pFragType->GetPhysics(0)->GetAllChildren()[i]->GetOwnerGroupPointerIndex();
						if(pFragType->HasMotionLimits(nGroupIndex))
							continue;
					}
					*/

					// calc the new matrix
					Matrix34 newMat = mat;
					Matrix34 thisMat = RCC_MATRIX34(pBoundComposite->GetCurrentMatrix(i));
					newMat.DotFromLeft(thisMat);	

					u32 iBoundArchetypeFlags = 0;

					if (pBoundComposite->GetTypeAndIncludeFlags())
					{
						iBoundArchetypeFlags = pBoundComposite->GetTypeFlags(i);
					}

					u32 iCombinedArchetypeFlags = iArchetypeFlags | iBoundArchetypeFlags;

					// render the bound
					ExportBound(pChildBound, pFragType, newMat, triangleVertices, triangleMaterials, colPolyFlags, iColPolyFlag, archetypeFlags, iCombinedArchetypeFlags, triDataArray, triData);
				}
			}
			break;
		}
	case phBound::CYLINDER:
	{
		// If we are exporting cover boxes around pillars
		if( m_pLevelProcessTool->GetExportCoverBoxesForPillarShapes() )
		{
			const phBoundCylinder * pBoundCylinder = static_cast<const phBoundCylinder*>(pBound);

			float fHeight = pBoundCylinder->GetHeight();
			float fRadius = pBoundCylinder->GetRadius();

			const Vec3V vCentreWorld = RCC_VEC3V(mat.d) + pBound->GetCentroidOffset();
			const Vec3V vhalfExtents(fRadius, fHeight/2.0f, fRadius);
			const phMaterialMgr::Id mtlID = pBound->GetMaterialId(0);

			MaybeAddCoverBoxForPillarShape(mat, vCentreWorld, vhalfExtents, fRadius, fHeight, ADD_POLY_TO_LIST_VARS);
		}

		const phBoundCylinder* pBoundCylinder = static_cast<const phBoundCylinder*>(pBound);
		AddCylinderTrianglesToList(pBoundCylinder, mat, triangleVertices, triangleMaterials, colPolyFlags, iColPolyFlag, archetypeFlags, iArchetypeFlags, triDataArray, triData);
		break;
	}

	case phBound::CAPSULE:
	{
		// If we are exporting cover boxes around pillars
		if( m_pLevelProcessTool->GetExportCoverBoxesForPillarShapes() )
		{
			const phBoundCapsule * pBoundCapsule = static_cast<const phBoundCapsule*>(pBound);

			float fHeight = pBoundCapsule->GetLength();
			float fRadius = pBoundCapsule->GetRadius();

			const Vec3V vCentreWorld = RCC_VEC3V(mat.d) + pBound->GetCentroidOffset();
			const Vec3V vhalfExtents(fRadius, fHeight/2.0f + fRadius, fRadius);
			const phMaterialMgr::Id mtlID = pBound->GetMaterialId(0);

			MaybeAddCoverBoxForPillarShape(mat, vCentreWorld, vhalfExtents, fRadius, fHeight, ADD_POLY_TO_LIST_VARS);
		}

		const phBoundCapsule * pBoundCapsule = static_cast<const phBoundCapsule*>(pBound);
		AddCapsuleTrianglesToList(pBoundCapsule, mat, triangleVertices, triangleMaterials, colPolyFlags, iColPolyFlag, archetypeFlags, iArchetypeFlags, triDataArray, triData);
		break;
	}

	case phBound::SPHERE:
	{
		const phBoundSphere * pBoundSphere = static_cast<const phBoundSphere*>(pBound);
		AddSphereTrianglesToList(pBoundSphere, mat, triangleVertices, triangleMaterials, colPolyFlags, iColPolyFlag, archetypeFlags, iArchetypeFlags, triDataArray, triData);
		break;
	}

	default:
		{
#if HEIGHTMAP_GENERATOR_TOOL
			if (iBoundType != phBound::CAPSULE && // James will support these types soon, let's not spam about it
				iBoundType != phBound::SPHERE)
#endif
			{
#if __DEV
				const char * pBoundsDesc = pBound->GetTypeString();
				Displayf("CNavMeshDataExporterTool : Error - bounds type %i ('%s') not supported.\n", iBoundType, pBoundsDesc);
#elif __BANK
				Displayf("CNavMeshDataExporterTool : Error - bounds not supported.\n");
#endif
			}

#if __EXPORT_UNKNOWN_BOUNDS_AS_BOX
			const Vec3V vCentre = RCC_VEC3V(mat.d) + pBound->GetCentroidOffset();
			const Vec3V vHalfExtent = pBound->GetBoundingBoxSize()*ScalarV(V_HALF);

			const Vec3V vBasisX = RCC_VEC3V(mat.a);
			const Vec3V vBasisY = RCC_VEC3V(mat.b);
			const Vec3V vBasisZ = RCC_VEC3V(mat.c);

			const phMaterialMgr::Id mtlID = pBound->GetMaterialId(0);

			AddBoxVolumeTrianglesToList(vCentre, vHalfExtent, vBasisX, vBasisY, vBasisZ, ADD_POLY_TO_LIST_VARS);
#endif
			break;
		}
	}
}

void fwExportCollisionBaseTool::AddBVHTrianglesToList(const rage::phBoundBVH * pBoundBVH, const Matrix34 & mat, vector<Vector3> & triangleVertices, vector<phMaterialMgr::Id> & triangleMaterials, vector<u16> & colPolyFlags, u16 iColPolyFlag, vector<u32> & archetypeFlags, u32 iArchetypeFlags, vector<fwNavTriData> & triDataArray, const fwNavTriData & in_triData)
{
	int iNumPolys = pBoundBVH->GetNumPolygons();
	int iNumTrianglesOutput = 0;

	Vector3 vPolyMin(FLT_MAX, FLT_MAX, FLT_MAX);
	Vector3 vPolyMax(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	int p,v;

	for(p=0; p<iNumPolys; p++)
	{
		const u16 boundMaterialID = (u16)pBoundBVH->GetPolygonMaterialIndex(p);
		const phMaterialMgr::Id	mtlID = pBoundBVH->GetMaterialId(boundMaterialID);
		const phPrimitive& bvhPrimitive = pBoundBVH->GetPrimitive(p);

		fwNavTriData triData = in_triData;

#if HEIGHTMAP_GENERATOR_TOOL
		if (m_pLevelProcessTool->GetExportPolyDensities())
		{
			if (bvhPrimitive.GetType() == PRIM_TYPE_POLYGON)
			{
				const int iNumRecursions = 0;
				triData.m_iPolyDensity = (u8)(0.5f + 255.0f*Clamp<float>(1.0f - pBoundBVH->RatePolygonRecursive((phPolygon::Index)p, iNumRecursions), 0.0f, 1.0f));
			}

			const float maxPrimitivesPerMeterSquared_MOVER = 4.0f; // PFD_MaxPrimitivesPerMeterSquared
			const float maxPrimitivesPerMeterSquared_WEAPON = 4.0f*5.0f;
			triData.m_iPrimDensity_MOVER = (u8)(0.5f + 255.0f*Clamp<float>(1.0f - pBoundBVH->RatePrimitive((phPolygon::Index)p, true, maxPrimitivesPerMeterSquared_MOVER), 0.0f, 1.0f));
			triData.m_iPrimDensity_WEAPON = (u8)(0.5f + 255.0f*Clamp<float>(1.0f - pBoundBVH->RatePrimitive((phPolygon::Index)p, true, maxPrimitivesPerMeterSquared_WEAPON), 0.0f, 1.0f));
		}

		triData.m_iFlagsStruct.m_iBvhPrimitiveType = (u8)bvhPrimitive.GetType();
#endif // HEIGHTMAP_GENERATOR_TOOL

		switch(bvhPrimitive.GetType())
		{
			case PRIM_TYPE_POLYGON:
			{
				const phPolygon & poly = bvhPrimitive.GetPolygon();

				// Get min/max of poly
				for(v=0; v<POLY_MAX_VERTICES; v++)
				{
					rage::phPolygon::Index vIndex = poly.GetVertexIndex(v);
					if(vIndex >= pBoundBVH->GetNumVertices())
						break;

					Vector3 vert = VEC3V_TO_VECTOR3(pBoundBVH->GetVertex(vIndex));
					mat.Transform(vert);

					if(vert.x < vPolyMin.x) vPolyMin.x = vert.x;
					if(vert.y < vPolyMin.y) vPolyMin.y = vert.y;
					if(vert.z < vPolyMin.z) vPolyMin.z = vert.z;
					if(vert.x > vPolyMax.x) vPolyMax.x = vert.x;
					if(vert.y > vPolyMax.y) vPolyMax.y = vert.y;
					if(vert.z > vPolyMax.z) vPolyMax.z = vert.z;
				}
				if (v == POLY_MAX_VERTICES)
				{
					// Split the poly into triangles, and add them to the input list..
					for(int v=1; v<POLY_MAX_VERTICES-1; v++)
					{
						rage::phPolygon::Index vIndex0 = poly.GetVertexIndex(0);
						rage::phPolygon::Index vIndex1 = poly.GetVertexIndex(v);
						rage::phPolygon::Index vIndex2 = poly.GetVertexIndex(v+1);

						Vector3 verts[3];
						verts[0] = VEC3V_TO_VECTOR3(pBoundBVH->GetVertex(vIndex0));
						verts[1] = VEC3V_TO_VECTOR3(pBoundBVH->GetVertex(vIndex1));
						verts[2] = VEC3V_TO_VECTOR3(pBoundBVH->GetVertex(vIndex2));

						mat.Transform(verts[0]);
						mat.Transform(verts[1]);
						mat.Transform(verts[2]);

						if( CheckTriangleAgainstNavMeshExtentsAndAddToList(verts, mtlID, triangleVertices, triangleMaterials, colPolyFlags, iColPolyFlag, archetypeFlags, iArchetypeFlags, triDataArray, triData) )
							iNumTrianglesOutput++;
					}
				}
				break;
			}
			case PRIM_TYPE_BOX:
			{
				const phPrimBox & bvhBoxPrim = bvhPrimitive.GetBox();

				//			0______5
				//		   /	  /|
				//		  /		 / |
				//		6/_____3/  |
				//		|		|  |
				//		|	4	|  1
				//		|		| /
				//		|		|/
				//		2-------7 

				const Vec3V vertex0 = Transform(RCC_MAT34V(mat), pBoundBVH->GetVertex(bvhBoxPrim.GetVertexIndex(0)));
				const Vec3V vertex1 = Transform(RCC_MAT34V(mat), pBoundBVH->GetVertex(bvhBoxPrim.GetVertexIndex(1)));
				const Vec3V vertex2 = Transform(RCC_MAT34V(mat), pBoundBVH->GetVertex(bvhBoxPrim.GetVertexIndex(2)));
				const Vec3V vertex3 = Transform(RCC_MAT34V(mat), pBoundBVH->GetVertex(bvhBoxPrim.GetVertexIndex(3)));

				const Vec3V vHalfExtentX = (vertex1 + vertex3 - vertex0 - vertex2)*ScalarV(V_QUARTER); // half extent right
				const Vec3V vHalfExtentY = (vertex0 + vertex3 - vertex1 - vertex2)*ScalarV(V_QUARTER); // half extent up
				const Vec3V vHalfExtentZ = (vertex2 + vertex3 - vertex0 - vertex1)*ScalarV(V_QUARTER);

				const Vec3V vCentre = (vertex0 + vertex1 + vertex2 + vertex3)*ScalarV(V_QUARTER);
				const Vec3V vHalfExtent(Mag(vHalfExtentX), Mag(vHalfExtentY), Mag(vHalfExtentZ));

				const Vec3V vBasisX = Normalize(vHalfExtentX);
				const Vec3V vBasisY = Normalize(vHalfExtentY);
				const Vec3V vBasisZ = Normalize(vHalfExtentZ);

				AddBoxVolumeTrianglesToList(vCentre, vHalfExtent, vBasisX, vBasisY, vBasisZ, ADD_POLY_TO_LIST_VARS);
				break;
			}
			case PRIM_TYPE_CYLINDER:
			{
				const phPrimCylinder & bvhCylinderPrim = bvhPrimitive.GetCylinder();

				// If we are exporting cover boxes around pillars
				if( m_pLevelProcessTool->GetExportCoverBoxesForPillarShapes() )
				{
					Vec3V v0_world = Transform(RCC_MAT34V(mat), pBoundBVH->GetVertex(bvhCylinderPrim.GetEndIndex0()));
					Vec3V v1_world = Transform(RCC_MAT34V(mat), pBoundBVH->GetVertex(bvhCylinderPrim.GetEndIndex1()));

					Vec3V vShaftAxis(v1_world - v0_world);
					float fHeight = Mag(vShaftAxis).Getf();
					float fRadius = bvhCylinderPrim.GetRadius();
					const Vec3V vCentreWorld = (v0_world + v1_world) * ScalarV(0.5f);
					const Vec3V vhalfExtents(fRadius, fHeight/2.0f, fRadius);

					// Create a matrix to orient the cylinder in the local space of the bound.
					ScalarV cylinderLength = MagFast(vShaftAxis); 
					Mat34V cylinderWorldMatrix(V_IDENTITY); 
					// Check if the shaft axis is parallel to the y-axis. 
					const VecBoolV maskY = VecBoolV(V_F_T_F_F); 
					const Vec3V result = SelectFT(maskY, vShaftAxis, Vec3V(V_ZERO)); 
					if(!IsZeroAll(result)) 
					{
						cylinderWorldMatrix.SetCol0(Normalize(Cross(vShaftAxis, Vec3V(V_Y_AXIS_WONE))));
						cylinderWorldMatrix.SetCol1(Normalize(vShaftAxis)); 
						cylinderWorldMatrix.SetCol2(Cross(cylinderWorldMatrix.GetCol0(), cylinderWorldMatrix.GetCol1()));
					} 
					cylinderWorldMatrix.SetCol3(Average(v0_world, v1_world)); 

					// This goes into the ADD_POLY_TO_LIST_VARS macro below
					const phMaterialMgr::Id mtlID = pBoundBVH->GetMaterialId(0);

					MaybeAddCoverBoxForPillarShape(MAT34V_TO_MATRIX34(cylinderWorldMatrix), vCentreWorld, vhalfExtents, fRadius, fHeight, ADD_POLY_TO_LIST_VARS);
				}

				const float fRadius = bvhCylinderPrim.GetRadius();
				const Vec3V vPoint0 = Transform(RCC_MAT34V(mat), pBoundBVH->GetVertex(bvhCylinderPrim.GetEndIndex0()));
				const Vec3V vPoint1 = Transform(RCC_MAT34V(mat), pBoundBVH->GetVertex(bvhCylinderPrim.GetEndIndex1()));

				if (fRadius <= 0.001f)
				{
					Errorf("%s: PRIM_TYPE_CYLINDER at %d=(%f,%f,%f),%d=(%f,%f,%f) has too small a radius (%f), skipping", EXPORT_BOUND_NAME, bvhCylinderPrim.GetEndIndex0(), VEC3V_ARGS(vPoint0), bvhCylinderPrim.GetEndIndex1(), VEC3V_ARGS(vPoint1), fRadius);
					break;
				}
				else if (MaxElement(Abs(vPoint1 - vPoint0)).Getf() < 0.001f)
				{
					Errorf("%s: PRIM_TYPE_CYLINDER at %d=(%f,%f,%f),%d=(%f,%f,%f) has degenerate points, skipping", EXPORT_BOUND_NAME, bvhCylinderPrim.GetEndIndex0(), VEC3V_ARGS(vPoint0), bvhCylinderPrim.GetEndIndex1(), VEC3V_ARGS(vPoint1));
					break;
				}

				// construct basis
				const Vec3V vAxis = Normalize(vPoint1 - vPoint0);
				const Vec3V vAxisAbs = Abs(vAxis);
				const float fAxisMinElement = MinElement(vAxisAbs).Getf();
				Vec3V vUp(V_Z_AXIS_WZERO);

				if      (fAxisMinElement == vAxisAbs.GetXf()) { vUp = Vec3V(V_X_AXIS_WZERO); }
				else if (fAxisMinElement == vAxisAbs.GetYf()) { vUp = Vec3V(V_Y_AXIS_WZERO); }

				const Vec3V vBasisX = Normalize(Cross(vAxis, vUp)); // vUp is the "most perpendicular" axis-aligned vector to vAxis
				const Vec3V vBasisY = Cross(vBasisX, vAxis); // vBasisX is already perpendicular to vAxis, no need to normalize

				const int iNumSlices = NUM_BOUND_PRIMITIVE_SLICES;

				AddCylinderVolumeTrianglesToList(vPoint0, vPoint1, vBasisX, vBasisY, fRadius, iNumSlices, ADD_POLY_TO_LIST_VARS);
				break;
			}
			case PRIM_TYPE_CAPSULE:
			{
				// We are for now treating BVH capsule primitives exactly the same as BVH cylinder primitives

				const phPrimCapsule & bvhCapsulePrim = bvhPrimitive.GetCapsule();

				// If we are exporting cover boxes around pillars
				if( m_pLevelProcessTool->GetExportCoverBoxesForPillarShapes() )
				{
					Vec3V v0_world = Transform(RCC_MAT34V(mat), pBoundBVH->GetVertex(bvhCapsulePrim.GetEndIndex0()));
					Vec3V v1_world = Transform(RCC_MAT34V(mat), pBoundBVH->GetVertex(bvhCapsulePrim.GetEndIndex1()));

					Vec3V vShaftAxis(v1_world - v0_world);
					float fHeight = Mag(vShaftAxis).Getf();
					float fRadius = bvhCapsulePrim.GetRadius();		
					const Vec3V vCentreWorld = (v0_world + v1_world) * ScalarV(0.5f);
					const Vec3V vhalfExtents(fRadius, fHeight/2.0f + fRadius, fRadius);

					// Create a matrix to orient the capsule in the local space of the bound.
					ScalarV capsuleLength = MagFast(vShaftAxis); 
					Mat34V capsuleWorldMatrix(V_IDENTITY); 
					// Check if the shaft axis is parallel to the y-axis. 
					const VecBoolV maskY = VecBoolV(V_F_T_F_F); 
					const Vec3V result = SelectFT(maskY, vShaftAxis, Vec3V(V_ZERO)); 
					if(!IsZeroAll(result)) 
					{ 
						capsuleWorldMatrix.SetCol0(Normalize(Cross(vShaftAxis, Vec3V(V_Y_AXIS_WONE))));
						capsuleWorldMatrix.SetCol1(Normalize(vShaftAxis)); 
						capsuleWorldMatrix.SetCol2(Cross(capsuleWorldMatrix.GetCol0(), capsuleWorldMatrix.GetCol1()));
					} 
					capsuleWorldMatrix.SetCol3(Average(v0_world, v1_world)); 

					// This goes into the ADD_POLY_TO_LIST_VARS macro below
					const phMaterialMgr::Id mtlID = pBoundBVH->GetMaterialId(0);

					MaybeAddCoverBoxForPillarShape(MAT34V_TO_MATRIX34(capsuleWorldMatrix), vCentreWorld, vhalfExtents, fRadius, fHeight, ADD_POLY_TO_LIST_VARS);
				}

				const float fRadius = bvhCapsulePrim.GetRadius();
				const Vec3V vPoint0 = Transform(RCC_MAT34V(mat), pBoundBVH->GetVertex(bvhCapsulePrim.GetEndIndex0()));
				const Vec3V vPoint1 = Transform(RCC_MAT34V(mat), pBoundBVH->GetVertex(bvhCapsulePrim.GetEndIndex1()));

				if (fRadius <= 0.001f)
				{
					Errorf("%s: PRIM_TYPE_CAPSULE at %d=(%f,%f,%f),%d=(%f,%f,%f) has too small a radius (%f), skipping", EXPORT_BOUND_NAME, bvhCapsulePrim.GetEndIndex0(), VEC3V_ARGS(vPoint0), bvhCapsulePrim.GetEndIndex1(), VEC3V_ARGS(vPoint1), fRadius);
					break;
				}
				else if (MaxElement(Abs(vPoint1 - vPoint0)).Getf() <= 0.001f)
				{
					//Errorf("%s: PRIM_TYPE_CAPSULE at %d=(%f,%f,%f),%d=(%f,%f,%f) has degenerate points, skipping", EXPORT_BOUND_NAME, bvhCapsulePrim.GetEndIndex0(), VEC3V_ARGS(vPoint0), bvhCapsulePrim.GetEndIndex1(), VEC3V_ARGS(vPoint1));

					const Vec3V vCentre = (vPoint1 + vPoint0)*ScalarV(V_HALF);

					const Vec3V vBasisX(V_X_AXIS_WZERO);
					const Vec3V vBasisY(V_Y_AXIS_WZERO);
					const Vec3V vBasisZ(V_Z_AXIS_WZERO);

					const int iNumSlices = (fRadius < 10.f ? NUM_BOUND_PRIMITIVE_SLICES : NUM_BIG_BOUND_PRIMITIVE_SLICES);
					const int iNumStacks = (fRadius < 10.f ? NUM_SPHERE_PRIMITIVE_STACKS : NUM_BIG_SPHERE_PRIMITIVE_STACKS);

					AddSphereVolumeTrianglesToList(vCentre, vBasisX, vBasisY, vBasisZ, fRadius, iNumSlices, iNumStacks, ADD_POLY_TO_LIST_VARS);
					break;
				}

				// construct basis
				const Vec3V vAxis = Normalize(vPoint1 - vPoint0);
				const Vec3V vAxisAbs = Abs(vAxis);
				const float fAxisMinElement = MinElement(vAxisAbs).Getf();
				Vec3V vUp(V_Z_AXIS_WZERO);

				if      (fAxisMinElement == vAxisAbs.GetXf()) { vUp = Vec3V(V_X_AXIS_WZERO); }
				else if (fAxisMinElement == vAxisAbs.GetYf()) { vUp = Vec3V(V_Y_AXIS_WZERO); }

				const Vec3V vBasisX = Normalize(Cross(vAxis, vUp)); // vUp is the "most perpendicular" axis-aligned vector to vAxis
				const Vec3V vBasisY = Cross(vBasisX, vAxis); // vBasisX is already perpendicular to vAxis, no need to normalize

				const int iNumSlices = NUM_BOUND_PRIMITIVE_SLICES;
				const int iNumCapStacks = NUM_CAPSULE_PRIMITIVE_CAP_STACKS;

				AddCapsuleVolumeTrianglesToList(vPoint0, vPoint1, vBasisX, vBasisY, fRadius, iNumSlices, iNumCapStacks, ADD_POLY_TO_LIST_VARS);
				break;
			}

			case PRIM_TYPE_SPHERE:
			{
				const phPrimSphere & bvhSpherePrim = bvhPrimitive.GetSphere();

				const float fRadius = bvhSpherePrim.GetRadius();
				const Vec3V vCentre = Transform(RCC_MAT34V(mat), pBoundBVH->GetVertex(bvhSpherePrim.GetCenterIndex()));

				if (fRadius <= 0.001f)
				{
					Errorf("%s: PRIM_TYPE_SPHERE at %d=(%f,%f,%f) has too small a radius (%f), skipping", EXPORT_BOUND_NAME, bvhSpherePrim.GetCenterIndex(), VEC3V_ARGS(vCentre), fRadius);
					break;
				}

				const Vec3V vBasisX(V_X_AXIS_WZERO);
				const Vec3V vBasisY(V_Y_AXIS_WZERO);
				const Vec3V vBasisZ(V_Z_AXIS_WZERO);

				const int iNumSlices = (fRadius < 10.f ? NUM_BOUND_PRIMITIVE_SLICES : NUM_BIG_BOUND_PRIMITIVE_SLICES);
				const int iNumStacks = (fRadius < 10.f ? NUM_SPHERE_PRIMITIVE_STACKS : NUM_BIG_SPHERE_PRIMITIVE_STACKS);

				AddSphereVolumeTrianglesToList(vCentre, vBasisX, vBasisY, vBasisZ, fRadius, iNumSlices, iNumStacks, ADD_POLY_TO_LIST_VARS);
				break;
			}

			default:
			{
#if HEIGHTMAP_GENERATOR_TOOL
				Warningf("### unsupported BVH primitive type %d", bvhPrimitive.GetType());
#endif
				break;
			}
		}
	}
}

void fwExportCollisionBaseTool::AddCylinderTrianglesToList(const rage::phBoundCylinder * pBoundCylinder, const Matrix34 & mat, vector<Vector3> & triangleVertices, vector<phMaterialMgr::Id> & triangleMaterials, vector<u16> & colPolyFlags, u16 iColPolyFlag, vector<u32> & archetypeFlags, u32 iArchetypeFlags, vector<fwNavTriData> & triDataArray, const fwNavTriData & triData)
{
	const Vec3V vPoint0 = RCC_VEC3V(mat.d) - RCC_VEC3V(mat.b)*pBoundCylinder->GetHalfHeightV();
	const Vec3V vPoint1 = RCC_VEC3V(mat.d) + RCC_VEC3V(mat.b)*pBoundCylinder->GetHalfHeightV();

	const Vec3V vBasisX = RCC_VEC3V(mat.a);
	const Vec3V vBasisY = RCC_VEC3V(mat.c);

	const float fRadius = pBoundCylinder->GetRadius();

	const int iNumSlices = NUM_BOUND_PRIMITIVE_SLICES;

	const phMaterialMgr::Id mtlID = pBoundCylinder->GetMaterialId(0);

	AddCylinderVolumeTrianglesToList(vPoint0, vPoint1, vBasisX, vBasisY, fRadius, iNumSlices, ADD_POLY_TO_LIST_VARS);
}

void fwExportCollisionBaseTool::AddCapsuleTrianglesToList(const rage::phBoundCapsule * pBoundCapsule, const Matrix34 & mat, vector<Vector3> & triangleVertices, vector<phMaterialMgr::Id> & triangleMaterials, vector<u16> & colPolyFlags, u16 iColPolyFlag, vector<u32> & archetypeFlags, u32 iArchetypeFlags, vector<fwNavTriData> & triDataArray, const fwNavTriData & triData)
{
	const Vec3V vPoint0 = RCC_VEC3V(mat.d) - RCC_VEC3V(mat.b)*pBoundCapsule->GetHalfLengthV();
	const Vec3V vPoint1 = RCC_VEC3V(mat.d) + RCC_VEC3V(mat.b)*pBoundCapsule->GetHalfLengthV();

	const Vec3V vBasisX = RCC_VEC3V(mat.a);
	const Vec3V vBasisY = RCC_VEC3V(mat.c);

	const float fRadius = pBoundCapsule->GetRadius();

	const int iNumSlices = NUM_BOUND_PRIMITIVE_SLICES;
	const int iNumCapStacks = NUM_CAPSULE_PRIMITIVE_CAP_STACKS;

	const phMaterialMgr::Id mtlID = pBoundCapsule->GetMaterialId(0);

	AddCapsuleVolumeTrianglesToList(vPoint0, vPoint1, vBasisX, vBasisY, fRadius, iNumSlices, iNumCapStacks, ADD_POLY_TO_LIST_VARS);
}

void fwExportCollisionBaseTool::AddSphereTrianglesToList(const rage::phBoundSphere * pBoundSphere, const Matrix34 & mat, vector<Vector3> & triangleVertices, vector<phMaterialMgr::Id> & triangleMaterials, vector<u16> & colPolyFlags, u16 iColPolyFlag, vector<u32> & archetypeFlags, u32 iArchetypeFlags, vector<fwNavTriData> & triDataArray, const fwNavTriData & triData)
{
	const Vec3V vCentre = RCC_VEC3V(mat.d);

	const Vec3V vBasisX(V_X_AXIS_WZERO);
	const Vec3V vBasisY(V_Y_AXIS_WZERO);
	const Vec3V vBasisZ(V_Z_AXIS_WZERO);

	const float fRadius = pBoundSphere->GetRadius();

	const int iNumSlices = (fRadius < 10.f ? NUM_BOUND_PRIMITIVE_SLICES : NUM_BIG_BOUND_PRIMITIVE_SLICES);
	const int iNumStacks = (fRadius < 10.f ? NUM_SPHERE_PRIMITIVE_STACKS : NUM_BIG_SPHERE_PRIMITIVE_STACKS);

	const phMaterialMgr::Id mtlID = pBoundSphere->GetMaterialId(0);

	AddSphereVolumeTrianglesToList(vCentre, vBasisX, vBasisY, vBasisZ, fRadius, iNumSlices, iNumStacks, ADD_POLY_TO_LIST_VARS);
}

void fwExportCollisionBaseTool::AddBoxTrianglesToList(const rage::phBoundBox * pBoundBox, const Matrix34 & mat, vector<Vector3> & triangleVertices, vector<phMaterialMgr::Id> & triangleMaterials, vector<u16> & colPolyFlags, u16 iColPolyFlag, vector<u32> & archetypeFlags, u32 iArchetypeFlags, vector<fwNavTriData> & triDataArray, const fwNavTriData & triData)
{
	const Vec3V vCentre = RCC_VEC3V(mat.d) + pBoundBox->GetCentroidOffset();
	const Vec3V vHalfExtent = pBoundBox->GetBoxSize()*ScalarV(V_HALF);

	const Vec3V vBasisX = RCC_VEC3V(mat.a);
	const Vec3V vBasisY = RCC_VEC3V(mat.b);
	const Vec3V vBasisZ = RCC_VEC3V(mat.c);

	const phMaterialMgr::Id mtlID = pBoundBox->GetMaterialId(0);

	AddBoxVolumeTrianglesToList(vCentre, vHalfExtent, vBasisX, vBasisY, vBasisZ, ADD_POLY_TO_LIST_VARS);
}

void fwExportCollisionBaseTool::AddGeometryTrianglesToList(const rage::phBoundGeometry * pBoundGeometry, const Matrix34 & mat, vector<Vector3> & triangleVertices, vector<phMaterialMgr::Id> & triangleMaterials, vector<u16> & colPolyFlags, u16 iColPolyFlag, vector<u32> & archetypeFlags, u32 iArchetypeFlags, vector<fwNavTriData> & triDataArray, const fwNavTriData & in_triData)
{
	int iNumPolys = pBoundGeometry->GetNumPolygons();

	Vector3 vPolyMin(FLT_MAX, FLT_MAX, FLT_MAX);
	Vector3 vPolyMax(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	for(int p=0; p<iNumPolys; p++)
	{
		const phPolygon & poly = pBoundGeometry->GetPolygon(p);
		fwNavTriData triData = in_triData;

#if __PFDRAW && HEIGHTMAP_GENERATOR_TOOL
		if (m_pLevelProcessTool->GetExportPolyDensities() && poly.GetPrimitive().GetType() == PRIM_TYPE_POLYGON)
		{
			const int iNumRecursions = 0;
			triData.m_iPolyDensity = (u8)(0.5f + 255.0f*Clamp<float>(1.0f - pBoundGeometry->RatePolygonRecursive((phPolygon::Index)p, iNumRecursions), 0.0f, 1.0f));
		}
#endif // __PFDRAW && HEIGHTMAP_GENERATOR_TOOL

		// Get min/max of poly
		for(int v=0; v<POLY_MAX_VERTICES; v++)
		{
			rage::phPolygon::Index vIndex = poly.GetVertexIndex(v);
			Vector3 vert = VEC3V_TO_VECTOR3(pBoundGeometry->GetVertex(vIndex));

			mat.Transform(vert);

			if(vert.x < vPolyMin.x) vPolyMin.x = vert.x;
			if(vert.y < vPolyMin.y) vPolyMin.y = vert.y;
			if(vert.z < vPolyMin.z) vPolyMin.z = vert.z;
			if(vert.x > vPolyMax.x) vPolyMax.x = vert.x;
			if(vert.y > vPolyMax.y) vPolyMax.y = vert.y;
			if(vert.z > vPolyMax.z) vPolyMax.z = vert.z;
		}

#if __CHECK_TRIANGLES_AGAINST_NAVMESH_EXTENTS
		// Test against the bounds of the current navmesh
		if(vPolyMax.x < m_vNavMeshMins.x || vPolyMax.y < m_vNavMeshMins.y || vPolyMax.z < m_vNavMeshMins.z ||
			vPolyMin.x > m_vNavMeshMaxs.x || vPolyMin.y > m_vNavMeshMaxs.y || vPolyMin.z > m_vNavMeshMaxs.z)
		{
			// No intersection possible
			continue;
		}
#endif

		// Split the poly into triangles, and add them to the input list..
		const u16 boundMaterialID = (u16)pBoundGeometry->GetPolygonMaterialIndex(p);
		const phMaterialMgr::Id	mtlID = pBoundGeometry->GetMaterialId(boundMaterialID);

		for(int v=1; v<POLY_MAX_VERTICES-1; v++)
		{
			rage::phPolygon::Index vIndex0 = poly.GetVertexIndex(0);
			rage::phPolygon::Index vIndex1 = poly.GetVertexIndex(v);
			rage::phPolygon::Index vIndex2 = poly.GetVertexIndex(v+1);

			Vector3 verts[3];
			verts[0] = VEC3V_TO_VECTOR3(pBoundGeometry->GetVertex(vIndex0));
			verts[1] = VEC3V_TO_VECTOR3(pBoundGeometry->GetVertex(vIndex1));
			verts[2] = VEC3V_TO_VECTOR3(pBoundGeometry->GetVertex(vIndex2));

			mat.Transform(verts[0]);
			mat.Transform(verts[1]);
			mat.Transform(verts[2]);

#if __CHECK_FOR_DEGENERATE_TRIANGLES
			// Check that this isn't a degenerate tri
			if((AreVerticesEqual(verts[0], verts[1], ms_fSameVertexEps) || AreVerticesEqual(verts[1], verts[2], ms_fSameVertexEps) || AreVerticesEqual(verts[2], verts[0], ms_fSameVertexEps)))
			{
				continue;
			}
#endif

#if __CHECK_FOR_DUPLICATE_TRIANGLES
			if(IsThisTriangleDuplicated(verts, triangleVertices))
			{
				printf("ART ERROR - duplicate triangle found.\n");
				continue;
			}
#endif

			CheckTriangleAgainstNavMeshExtentsAndAddToList(verts, mtlID, triangleVertices, triangleMaterials, colPolyFlags, iColPolyFlag, archetypeFlags, iArchetypeFlags, triDataArray, triData);
		}
	}
}

bool fwExportCollisionBaseTool::AddPolyToList(const Vec3V * points, int numPoints, bool bForce, ADD_POLY_TO_LIST_PARAMS)
{
	bool bAdded = false;

	for(int v=1; v<numPoints-1; v++)
	{
		Vec3V verts[3] =
		{
			points[0],
			points[v],
			points[v+1],
		};

		if (bForce)
		{
			triangleVertices.push_back(RCC_VECTOR3(verts[0]));
			triangleVertices.push_back(RCC_VECTOR3(verts[1]));
			triangleVertices.push_back(RCC_VECTOR3(verts[2]));

			triangleMaterials.push_back(mtlID);
			colPolyFlags.push_back(iColPolyFlag);
			archetypeFlags.push_back(iArchetypeFlags);
			triDataArray.push_back(triData);

			if(triangleVertices.size() >= FLUSH_TRIANGLES_LIST_WHEN_THIS_MANY_VERTICES)
			{
				FlushTriListToTriFile(triangleVertices, triangleMaterials, colPolyFlags, archetypeFlags, triDataArray);
			}

			bAdded = true;
		}
		else if (CheckTriangleAgainstNavMeshExtentsAndAddToList((Vector3*)verts, mtlID, triangleVertices, triangleMaterials, colPolyFlags, iColPolyFlag, archetypeFlags, iArchetypeFlags, triDataArray, triData))
		{
			bAdded = true;
		}
	}

	return bAdded;
}

void fwExportCollisionBaseTool::AddRadialVolumeTrianglesToList(Vec3V_In vPoint0, Vec3V_In vPoint1, Vec3V_In vBasisX, Vec3V_In vBasisY_in, const float* radii, const float* dists, int iNumSlices, int iNumStacks, ADD_POLY_TO_LIST_PARAMS)
{
	Vec3V vBasisY = vBasisY_in;

	// adjust basis so that the determinant is > 0
	if (Determinant(Mat33V(vBasisX, vBasisY, vPoint1 - vPoint0)).Getf() < 0.0f)
	{
		vBasisY = -vBasisY;
	}

	for (int iSlice = 0; iSlice < iNumSlices; iSlice++)
	{
		const float fAng0 = 2.0f*PI*(float)(iSlice + 0)/(float)iNumSlices; // [0..2PI]
		const float fAng1 = 2.0f*PI*(float)(iSlice + 1)/(float)iNumSlices;

		const Vec3V vBasis0 = vBasisX*ScalarV(cosf(fAng0)) + vBasisY*ScalarV(sinf(fAng0));
		const Vec3V vBasis1 = vBasisX*ScalarV(cosf(fAng1)) + vBasisY*ScalarV(sinf(fAng1));

		for (int iStack = 0; iStack < iNumStacks; iStack++)
		{
			const Vec3V vP0 = vPoint0 + (vPoint1 - vPoint0)*ScalarV(dists[iStack + 0]);
			const Vec3V vP1 = vPoint0 + (vPoint1 - vPoint0)*ScalarV(dists[iStack + 1]);

			const float fRadius0 = radii[iStack + 0];
			const float fRadius1 = radii[iStack + 1];

			if (fRadius0 != 0.0f)
			{
				const Vec3V vTriVerts[3] =
				{
					vP0 + vBasis0*ScalarV(fRadius0),
					vP0 + vBasis1*ScalarV(fRadius0),
					vP1 + vBasis1*ScalarV(fRadius1)
				};

				CheckTriangleAgainstNavMeshExtentsAndAddToList((const Vector3*)vTriVerts, mtlID, triangleVertices, triangleMaterials, colPolyFlags, iColPolyFlag, archetypeFlags, iArchetypeFlags, triDataArray, triData);
			}
			
			if (fRadius1 != 0.0f)
			{
				const Vec3V vTriVerts[3] =
				{
					vP0 + vBasis0*ScalarV(fRadius0),
					vP1 + vBasis1*ScalarV(fRadius1),
					vP1 + vBasis0*ScalarV(fRadius1)
				};

				CheckTriangleAgainstNavMeshExtentsAndAddToList((const Vector3*)vTriVerts, mtlID, triangleVertices, triangleMaterials, colPolyFlags, iColPolyFlag, archetypeFlags, iArchetypeFlags, triDataArray, triData);
			}
		}
	}
}

void fwExportCollisionBaseTool::AddCylinderVolumeTrianglesToList(Vec3V_In vPoint0, Vec3V_In vPoint1, Vec3V_In vBasisX, Vec3V_In vBasisY, float fRadius, int iNumSlices, ADD_POLY_TO_LIST_PARAMS)
{
	const int iNumStacks = 3;

	const float radii[iNumStacks + 1] = {0.0f, fRadius, fRadius, 0.0f};
	const float dists[iNumStacks + 1] = {0.0f, 0.0f, 1.0f, 1.0f};

	AddRadialVolumeTrianglesToList(vPoint0, vPoint1, vBasisX, vBasisY, radii, dists, iNumSlices, iNumStacks, ADD_POLY_TO_LIST_VARS);
}

void fwExportCollisionBaseTool::AddCapsuleVolumeTrianglesToList(Vec3V_In vPoint0, Vec3V_In vPoint1, Vec3V_In vBasisX, Vec3V_In vBasisY, float fRadius, int iNumSlices, int iNumCapStacks, ADD_POLY_TO_LIST_PARAMS)
{
	const int iNumStacks = iNumCapStacks*2 + 1;

	float* radii = rage_new float[iNumStacks + 1];
	float* dists = rage_new float[iNumStacks + 1];

	const float fDistScale = -fRadius/Mag(vPoint1 - vPoint0).Getf();

	for (int iStack = 0; iStack <= iNumCapStacks; iStack++)
	{
		const float fAng = 0.5f*PI*(float)iStack/(float)iNumCapStacks; // [0..PI/2]

		radii[iStack] = sinf(fAng)*fRadius;
		dists[iStack] = cosf(fAng)*fDistScale;

		radii[iNumStacks - iStack] =        radii[iStack];
		dists[iNumStacks - iStack] = 1.0f - dists[iStack];
	}

	radii[0] = 0.0f; // force radii at endpoints to zero, to prevent sliver triangles being created at the poles
	radii[iNumStacks] = 0.0f;

	AddRadialVolumeTrianglesToList(vPoint0, vPoint1, vBasisX, vBasisY, radii, dists, iNumSlices, iNumStacks, ADD_POLY_TO_LIST_VARS);

	delete[] radii;
	delete[] dists;
}

void fwExportCollisionBaseTool::AddSphereVolumeTrianglesToList(Vec3V_In vCentre, Vec3V_In vBasisX, Vec3V_In vBasisY, Vec3V_In vBasisZ, float fRadius, int iNumSlices, int iNumStacks, ADD_POLY_TO_LIST_PARAMS)
{
	const Vec3V vPoint0 = vCentre - vBasisZ*ScalarV(fRadius);
	const Vec3V vPoint1 = vCentre + vBasisZ*ScalarV(fRadius);

	float* radii = rage_new float[iNumStacks + 1];
	float* dists = rage_new float[iNumStacks + 1];

	for (int iStack = 0; iStack <= iNumStacks; iStack++)
	{
		const float fAng = PI*(float)iStack/(float)iNumStacks; // [0..PI]

		radii[iStack] = fRadius*sinf(fAng);
		dists[iStack] = (1.0f - cosf(fAng))*0.5f; // [0..1]
	}

	radii[0] = 0.0f; // force radii at endpoints to zero, to prevent sliver triangles being created at the poles
	radii[iNumStacks] = 0.0f;

	AddRadialVolumeTrianglesToList(vPoint0, vPoint1, vBasisX, vBasisY, radii, dists, iNumSlices, iNumStacks, ADD_POLY_TO_LIST_VARS);

	delete[] radii;
	delete[] dists;
}

void fwExportCollisionBaseTool::AddBoxVolumeTrianglesToList(Vec3V_In vCentre, Vec3V_In vHalfExtent, Vec3V_In vBasisX, Vec3V_In vBasisY, Vec3V_In vBasisZ, ADD_POLY_TO_LIST_PARAMS)
{
	const int NUM_TRIANGLES = 12;
	const int cornerFromFace[NUM_TRIANGLES][3]=
	{
		{4,0,3},
		{3,7,4},
		{2,1,5},
		{5,6,2},
		{1,0,4},
		{4,5,1},
		{7,3,2},
		{2,6,7},
		{3,0,1},
		{1,2,3},
		{5,4,7},
		{7,6,5}
	};

	const Mat34V vBasis(vBasisX, vBasisY, vBasisZ, vCentre);
	const Vec3V cornerVerts[8] =
	{
		Transform(vBasis, vHalfExtent*Vec3V(+1.0f, +1.0f, +1.0f)),
		Transform(vBasis, vHalfExtent*Vec3V(-1.0f, +1.0f, +1.0f)),
		Transform(vBasis, vHalfExtent*Vec3V(-1.0f, -1.0f, +1.0f)),
		Transform(vBasis, vHalfExtent*Vec3V(+1.0f, -1.0f, +1.0f)),
		Transform(vBasis, vHalfExtent*Vec3V(+1.0f, +1.0f, -1.0f)),
		Transform(vBasis, vHalfExtent*Vec3V(-1.0f, +1.0f, -1.0f)),
		Transform(vBasis, vHalfExtent*Vec3V(-1.0f, -1.0f, -1.0f)),
		Transform(vBasis, vHalfExtent*Vec3V(+1.0f, -1.0f, -1.0f))
	};

	for (int iTri = 0; iTri < NUM_TRIANGLES; iTri++)
	{
		const Vec3V verts[3] =
		{
			cornerVerts[cornerFromFace[iTri][0]],
			cornerVerts[cornerFromFace[iTri][1]],
			cornerVerts[cornerFromFace[iTri][2]]
		};

		CheckTriangleAgainstNavMeshExtentsAndAddToList((const Vector3*)verts, mtlID, triangleVertices, triangleMaterials, colPolyFlags, iColPolyFlag, archetypeFlags, iArchetypeFlags, triDataArray, triData);
	}
}

u8 fwExportCollisionBaseTool::GetBakedInAttributesForTriangle(Vector3 * pTriVerts, phMaterialMgr::Id iTriMaterial)
{
	Assertf(ms_pNavMeshStoredData, "A class needs to be derived from CNavMeshStoredData for this project, and set via the SetProjectSpecificDataFilter() function.");
	if(!ms_pNavMeshStoredData)
		return 0;

	u8 iByte=0;

	if(ms_pNavMeshStoredData->GetIsBakedInAttribute1(pTriVerts,iTriMaterial)) iByte |= (1<<0);
	if(ms_pNavMeshStoredData->GetIsBakedInAttribute2(pTriVerts,iTriMaterial)) iByte |= (1<<1);
	if(ms_pNavMeshStoredData->GetIsBakedInAttribute3(pTriVerts,iTriMaterial)) iByte |= (1<<2);
	if(ms_pNavMeshStoredData->GetIsBakedInAttribute4(pTriVerts,iTriMaterial)) iByte |= (1<<3);
	if(ms_pNavMeshStoredData->GetIsBakedInAttribute5(pTriVerts,iTriMaterial)) iByte |= (1<<4);
	if(ms_pNavMeshStoredData->GetIsBakedInAttribute6(pTriVerts,iTriMaterial)) iByte |= (1<<5);
	if(ms_pNavMeshStoredData->GetIsBakedInAttribute7(pTriVerts,iTriMaterial)) iByte |= (1<<6);
	if(ms_pNavMeshStoredData->GetIsBakedInAttribute8(pTriVerts,iTriMaterial)) iByte |= (1<<7);

	return iByte;
}


void fwExportCollisionBaseTool::SetProjectSpecificDataFilter(fwNavMeshMaterialInterface * pStoredData)
{
	if(ms_pNavMeshStoredData) delete ms_pNavMeshStoredData;
	ms_pNavMeshStoredData = pStoredData;
}

// Helper method that may add a COVER collision box circumscribing the given pillar shape
// This helps to generate corner cover around tree trunks or appropriate columns
void fwExportCollisionBaseTool::MaybeAddCoverBoxForPillarShape(const Matrix34& matrixLocalToWorld, Vec3V_In centerWorld, Vec3V_In halfExtents, float radius, float height, ADD_POLY_TO_LIST_PARAMS)
{
	// If the cylinder is appropriately sized to be boxed
	if( ms_fMinHeightToCoverBoxCylinder <= height && ms_fMinRadiusToCoverBoxCylinder <= radius && radius <= ms_fMaxRadiusToCoverBoxCylinder )
	{
		const Vec3V vBasisX = RCC_VEC3V(matrixLocalToWorld.a);
		const Vec3V vBasisY = RCC_VEC3V(matrixLocalToWorld.b);
		const Vec3V vBasisZ = RCC_VEC3V(matrixLocalToWorld.c);

		// temporarily set archetype flags, so we can use defined var list
		u32 iPrevArchetypeFlags = iArchetypeFlags;
		iArchetypeFlags = m_pLevelProcessTool->GetCoverBoxCollisionArchetypeFlags();

		AddBoxVolumeTrianglesToList(centerWorld, halfExtents, vBasisX, vBasisY, vBasisZ, ADD_POLY_TO_LIST_VARS);

		// restore flags to what they were
		iArchetypeFlags = iPrevArchetypeFlags;
	}
}

//-----------------------------------------------------------------------------

// End of file fwnavgen/baseexportcollision.cpp
