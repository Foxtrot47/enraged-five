#include "NavGen_Voxel.h"
#include "NavGen.h"

namespace rage
{

const float CVoxelMap::ms_fDefaultCellSize = 0.25f;

CVoxelMap::CElement::CElement()
{
	m_pSurface = NULL;
	m_fIntersectZ = 0.0f;
	m_iSpaceAbove = 0;
	m_pNextAbove = NULL;
	//m_pNextBelow = NULL;
}
CVoxelMap::CElement::~CElement()
{

}

CVoxelMap::CVoxelMap()
{
	m_fCellSize = ms_fDefaultCellSize;

	m_fHalfCellSize = m_fCellSize / 2.0f;
	m_vVoxelSize = Vector3(m_fCellSize, m_fCellSize, m_fCellSize);
	m_vHalfVoxelSize = Vector3(m_fHalfCellSize, m_fHalfCellSize, m_fHalfCellSize);
	m_vHalfVoxelSizeXY = Vector3(m_fHalfCellSize, m_fHalfCellSize, 0.0f);
	m_iNumCellsX = 0;
	m_iNumCellsY = 0;
	m_pBaseCells = NULL;
}

CVoxelMap::~CVoxelMap()
{

}

void CVoxelMap::Shutdown()
{
	delete[] m_pBaseCells;
	m_pBaseCells = NULL;
}

bool CVoxelMap::ConstructFromCollision(const Vector3 & vMin, const Vector3 & vMax, TColTriArray & collisionTriangles)
{
	// We expand the XY extents such that the center of each boundary voxel lies exactly on the edge of the region
	InitToSize(vMin - m_vHalfVoxelSizeXY, vMax + m_vHalfVoxelSizeXY);

	s32 t;
	for(t=0; t<collisionTriangles.size(); t++)
	{
		CNavGenTri * pTri = &collisionTriangles[t];

		VoxelizeTriangle(pTri);
	}

	return true;
}

// NAME : VoxeliseTriangle
// PURPOSE : Voxelise a single triangle
// For now visit every voxel in the AABB of the triangle, and test the voxel for tri intersection
// We may get performance wins by raserizing in 3d at a later date..
void CVoxelMap::VoxelizeTriangle(CNavGenTri * pTri)
{
	Vector3 vTriMin(FLT_MAX, FLT_MAX, FLT_MAX);
	Vector3 vTriMax(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	s32 p;
	for(p=0; p<3; p++)
	{
		vTriMin.x = Min(vTriMin.x, pTri->m_vPts[p].x);
		vTriMin.y = Min(vTriMin.y, pTri->m_vPts[p].y);
		vTriMin.z = Min(vTriMin.z, pTri->m_vPts[p].z);
		vTriMax.x = Max(vTriMax.x, pTri->m_vPts[p].x);
		vTriMax.y = Max(vTriMax.y, pTri->m_vPts[p].y);
		vTriMax.z = Max(vTriMax.z, pTri->m_vPts[p].z);
	}

	vTriMin.x = Max(vTriMin.x, m_vMin.x);
	vTriMin.y = Max(vTriMin.y, m_vMin.y);
	vTriMin.z = Max(vTriMin.z, m_vMin.z);
	vTriMax.x = Min(vTriMax.x, m_vMax.x);
	vTriMax.y = Min(vTriMax.y, m_vMax.y);
	vTriMax.z = Min(vTriMax.z, m_vMax.z);

	s32 iVoxelMin[3];
	s32 iVoxelMax[3];

	WorldCoordsToVoxelCoords(vTriMin, iVoxelMin[0], iVoxelMin[1], iVoxelMin[2]);
	WorldCoordsToVoxelCoords(vTriMax, iVoxelMax[0], iVoxelMax[1], iVoxelMax[2]);

	s32 x,y,z;
	Vector3 vVoxelMin, vVoxelMax;

	for(z=iVoxelMin[2]; z<iVoxelMax[2]; z++)
	{
		for(y=iVoxelMin[1]; y<iVoxelMax[1]; y++)
		{
			for(x=iVoxelMin[0]; x<iVoxelMax[0]; x++)
			{
				GetVoxelBounds(x, y, z, vVoxelMin, vVoxelMax);

				const bool bIntersects = geomBoxes::TestTriangleToAlignedBox(pTri->m_vPts[0], pTri->m_vPts[1], pTri->m_vPts[2], vVoxelMin, vVoxelMax);

				if(bIntersects)
				{
					//CElement * pNewElement = AllocVoxel(x, y, z, pTri);
				}
			}
		}
	}
}

CVoxelMap::CElement * CVoxelMap::AllocVoxel(const s32 iVx, const s32 iVy, const s32 iVz, CNavGenTri * pTri)
{
	const bool bValid = IsValidVoxelCoord(iVx, iVy);
	Assert(bValid);
	if(bValid)
	{
		s32 iBase = (iVy * m_iNumCellsY) + iVx;
		CElement * pBase = &m_pBaseCells[iBase];
		CElement * pLast = NULL;
		s32 z = 0;
		s32 lastz = 0;

		while(pBase)
		{
			// Already have a voxel allocated on exactly this Z height
			if( iVz == z )
			{
				// Add this triangle to the list of triangles passing through this voxel
				pBase->m_pSurface = pTri;

				return pBase;
			}
			// We should insert a new element along this RLE column
			else if( iVz < z+pBase->m_iSpaceAbove )
			{
				CElement * pElement = AllocElement();
				pElement->m_pNextAbove = pBase->m_pNextAbove;
				pBase->m_pNextAbove = pElement;

				pElement->m_iSpaceAbove = pBase->m_iSpaceAbove - (iVz-z);
				pBase->m_iSpaceAbove = (iVz-z);
				pElement->m_pSurface = pTri;

				return pElement;
			}
			// Keep iterating upwards
			lastz = z;
			z += pBase->m_iSpaceAbove;
			pLast = pBase;
			pBase = pBase->m_pNextAbove;
		}
		// We reached the end of the column - we'll need to allocate a new element at the end
		Assert(!pBase);
		Assert(pLast);

		CElement * pElement = AllocElement();
		pLast->m_pNextAbove = pElement;
		pElement->m_iSpaceAbove = pLast->m_iSpaceAbove - (iVz - lastz);
		pLast->m_iSpaceAbove = iVz - lastz;
		pElement->m_pSurface = pTri;

		return pElement;
	}

	return NULL;
}

void CVoxelMap::ForAllSolidVoxels(ForAllVoxelsCB callbackFn, void * data)
{
	s32 x,y;
	s32 iBaseCount=0;
	Vector3 vVoxelMin, vVoxelMax;

	for(y=0; y<m_iNumCellsY; y++)
	{
		for(x=0; x<m_iNumCellsX; x++)
		{
			s32 z=0;
			CElement * pElement = &m_pBaseCells[iBaseCount];
			while(pElement)
			{
				if(pElement->m_pSurface)
				{
					GetVoxelBounds(x, y, z, vVoxelMin, vVoxelMax);
					if( !callbackFn(pElement, vVoxelMin, vVoxelMax, data) )
					{
						return;
					}
				}

				z += pElement->m_iSpaceAbove;
				pElement = pElement->m_pNextAbove;
			}
			iBaseCount++;
		}
	}
}

void CVoxelMap::InitToSize(const Vector3 & vMin, const Vector3 & vMax)
{
	m_vMin = vMin;
	m_vMax = vMax;

	const Vector3 vSize = vMax - vMin;
	m_iNumCellsX = (s32) (vSize.x / m_fCellSize);
	m_iNumCellsY = (s32) (vSize.y / m_fCellSize);
	m_iMaxNumVoxelsZ = (s32) (2000.0f / m_fCellSize);

	s32 iNumBaseCells = m_iNumCellsX * m_iNumCellsY;

	m_pBaseCells = rage_new CElement[iNumBaseCells];

	s32 x,y;

	for(y=0; y<m_iNumCellsY; y++)
	{
		for(x=0; x<m_iNumCellsX; x++)
		{
			m_pBaseCells[(y*m_iNumCellsX)+x].m_iSpaceAbove = m_iMaxNumVoxelsZ;
		}
	}
}

void CVoxelMap::VoxelCoordsToWorldCoords(const s32 vx, const s32 vy, const s32 vz, Vector3 & vOutPos) const
{
	Assert(vx >= 0 && vx < m_iNumCellsX);
	Assert(vy >= 0 && vy < m_iNumCellsY);


	vOutPos.x = m_vMin.x + (((float)vx) * m_fCellSize) + m_fHalfCellSize;
	vOutPos.y = m_vMin.y + (((float)vy) * m_fCellSize) + m_fHalfCellSize;
	vOutPos.z = m_vMin.z + (((float)vz) * m_fCellSize);
}

void CVoxelMap::WorldCoordsToVoxelCoords(const Vector3 & vPos, s32 & iOutVx, s32 & iOutVy, s32 & iOutVz) const
{
	iOutVx = (s32) (((vPos.x-m_fHalfCellSize)-m_vMin.x) / m_fCellSize);
	iOutVy = (s32) (((vPos.y-m_fHalfCellSize)-m_vMin.y) / m_fCellSize);
	iOutVz = (s32) ((vPos.z-m_vMin.z) / m_fCellSize);
}

void CVoxelMap::GetVoxelBounds(const s32 iVx, const s32 iVy, const s32 iVz, Vector3 & vOutMin, Vector3 & vOutMax) const
{
	Vector3 vVoxelMid;
	VoxelCoordsToWorldCoords(iVx, iVy, iVz, vVoxelMid);

	vOutMin = vVoxelMid - m_vHalfVoxelSize;
	vOutMax = vVoxelMid + m_vHalfVoxelSize;
}

}


