#include "NavGen.h"
#include "NavGen_Utils.h"
//#include "SurfaceTable.h"


namespace rage
{

//***********************************************************************************
//	Title	: NavGen_Optimise.cpp
//	Author	: James Broad
//	Date	: Jan 2005
//
//	This file contains classes & functions for use in optimising the navmesh.
//
//	The navmesh surface will have been generated from sampling & triangulating the
//	collision geometry exported from the world.
//
//	The optimisation uses progressive-meshing to reduce the triangle count, whilst
//	trying to maintain the important features of the geometry.  Each edge in the mesh
//	is assigned a cost value for how much the surrounding topology would change if
//	one of it's vertices were to be moved on top of the other (thereby removing one
//	edge, one vertex, and two faces).  The cost value is in part derived from the
//	'quadric error metric' technique (see Heckbert & Garland for an excellent 
//	explaination of this).
//
//	The edge with the least cost value is chosen, and is collapsed.  This process
//	repeats until no more edges can be collapsed within some error threshold.
//
//	There are other restrictions in place based upon triangle area, maximum triangle
//	edge-length, and measures to ensure that triangles never flip themselves or their
//	neighbours upside-down or otherwise invalidate the mesh.  We also prevent vertices
//	from being moved if they are sitting on different surface-types, one (or both) of
//	which we are trying to preserve (eg. pavements).
//
//	Once the optimisation process is complete, the mesh is saved as an .asc file.
//	This is a simple ascii list of vertices & triangles similar to that used by
//	3ds max (I have however added some extra fields for surface-types).
//
//	By far the most complex parts of this optimisation process, is keeping track of
//	the topology of the mesh when triangles, edges & nodes are removed.  I've used
//	a winged-edge data structure with each CNavTriEdge having a pointer to two
//	CNavGenTri's, and each CNavGenNode having a list of CNavTriEdge's.  As triangles,
//	edges & nodes are removed they are NOT deleted as - but are marked with a flag in
//	the SetRemoved() function.  This is partly to avoid the problem of double-deleting
//	elements, and partly to avoid lots of runtime deallocations.  Instead, at the end
//	of the optimisation process everything is cleanly & safely deleted.
//
//***********************************************************************************

// Defining this will cause debug spew wrt the mesh optimisation
//#define DEBUG_MESHOPT

#define __PERFORM_OPTIMISATION		1

// Defining this will cause a lot of sanity-checks
//#define INDEPTH_OPTIMISER_SANITYCHECK
//#define CHECK_FOR_DUPLICATE_TRIANGLES_DURING_OPTIMIZATION

// This global defines whether we are going to encode surface information (pavements, etc) into the mesh
// It is needed here, since in this current implementation we encode this information simply by preventing
// the optimisation code from optimising it away.
//bool g_bDoWeCareAboutSurfaceTypes = true;


bool CNavGen::OptimiseNavSurface(void)
{

#if __PERFORM_OPTIMISATION

	if(!m_Octree.RootNode())
		return false;

	Vector3 vMin, vMax;
	m_Octree.GetOctreeExtents(vMin, vMax);

	//************************************************************************
	// Optimise the triangles in each leaf
	//************************************************************************

	m_iNumEdgesCollapsedThisPass = 0;

	CreateCollapseEdges();



	if(!m_bRandomisedEdgeListsBeforeOptimisation)
	{
		//RandomiseSomeDataBeforeOptimisation();
		m_bRandomisedEdgeListsBeforeOptimisation = true;
	}

	OptimiseNavSurfaces();

	m_iOptimisedTriangleCount -= (m_iNumEdgesCollapsedThisPass * 2);
	m_iOptimisedNodeCount -= m_iNumEdgesCollapsedThisPass;

#ifdef NAVGEN_SPEW_DEBUG
	sprintf(tmpBuf, "%i tris removed, num nodes = (%i / %i), num triangles = (%i / %i).\n", m_iNumEdgesCollapsedThisPass*2, m_iOptimisedNodeCount, m_iInitialPassNumNodesCreated, m_iOptimisedTriangleCount, m_iInitialPassNumTrianglesCreated);
	OutputDebugString(tmpBuf);
#endif

#endif	// __PERFORM_OPTIMISATION

	m_bNavSurfaceHasBeenOptimised = true;

	return true;
}



bool
CNavGen::RemoveTriFromTriList(vector<CNavSurfaceTri*> & list, CNavSurfaceTri * pTri)
{
	bool bFound = false;

	u32 i;
	for(i=0; i<list.size(); i++)
	{
		CNavSurfaceTri * pListTri = list[i];
		if(pListTri == pTri)
		{
			list.erase(list.begin()+i);
			i--;
			bFound = true;
		}
	}

	return bFound;
}


bool
RemoveEdgeFromEdgeList(vector<CNavTriEdge*> & list, CNavTriEdge * pEdge)
{
	bool bFound = false;

	u32 i;
	for(i=0; i<list.size(); i++)
	{
		CNavTriEdge * pListEdge = list[i];
		if(pListEdge == pEdge)
		{
			list.erase(list.begin()+i);
			i--;
			bFound = true;
		}
	}

	return bFound;
}


void
CNavGen::RemoveReferenceToTrisFromNodesOppositeEdgeToCollapse(CNavTriEdge * pEdge)
{
	CNavSurfaceTri * pTri1 = pEdge->m_pTri1;
	CNavSurfaceTri * pTri2 = pEdge->m_pTri2;

	CNavGenNode * pNode1 = pEdge->m_pNode1;
	CNavGenNode * pNode2 = pEdge->m_pNode2;

	CNavGenNode * pOppositeNode1 = NULL;
	CNavGenNode * pOppositeNode2 = NULL;

	int t;

	// Find the 'opposite' node in tri1.  This is the node which of the tri which isn't equal to either of the edge nodes
	for(t=0; t<3; t++)
	{
		if(pTri1->m_Nodes[t] != pNode1 && pTri1->m_Nodes[t] != pNode2)
		{
			pOppositeNode1 = pTri1->m_Nodes[t];
			break;
		}
	}
	if(t==3)
	{
		//_ASSERTE(0);
	}

	// Find the 'opposite' node in tri2.  This is the node which of the tri which isn't equal to either of the edge nodes
	for(t=0; t<3; t++)
	{
		if(pTri2->m_Nodes[t] != pNode1 && pTri2->m_Nodes[t] != pNode2)
		{
			pOppositeNode2 = pTri2->m_Nodes[t];
			break;
		}
	}
	if(t==3)
	{
		//_ASSERTE(0);
	}

	// NB : Also mark all edges around the two opposite nodes, as having had the topology changed.

	if(pOppositeNode1)
	{
		RemoveTriFromTriList(pOppositeNode1->m_TrianglesUsingThisNode, pEdge->m_pTri1);
		RemoveTriFromTriList(pOppositeNode1->m_TrianglesUsingThisNode, pEdge->m_pTri2);

		for(u32 e=0; e<pOppositeNode1->m_EdgesUsingThisNode.size(); e++)
		{
			CNavTriEdge * pOtherEdge = pOppositeNode1->m_EdgesUsingThisNode[e];
			if(pOtherEdge->m_pNode1 == pEdge->m_pNode1 || pOtherEdge->m_pNode2 == pEdge->m_pNode1 ||
				pOtherEdge->m_pNode1 == pEdge->m_pNode2 || pOtherEdge->m_pNode2 == pEdge->m_pNode2)
			{
				pOtherEdge->m_iFlags |= TRIEDGE_TOPOLOGYHASCHANGED;
			}
		}
	}

	if(pOppositeNode2)
	{
		RemoveTriFromTriList(pOppositeNode2->m_TrianglesUsingThisNode, pEdge->m_pTri1);
		RemoveTriFromTriList(pOppositeNode2->m_TrianglesUsingThisNode, pEdge->m_pTri2);

		for(u32 e=0; e<pOppositeNode2->m_EdgesUsingThisNode.size(); e++)
		{
			CNavTriEdge * pOtherEdge = pOppositeNode2->m_EdgesUsingThisNode[e];
			if(pOtherEdge->m_pNode1 == pEdge->m_pNode1 || pOtherEdge->m_pNode2 == pEdge->m_pNode1 ||
				pOtherEdge->m_pNode1 == pEdge->m_pNode2 || pOtherEdge->m_pNode2 == pEdge->m_pNode2)
			{
				pOtherEdge->m_iFlags |= TRIEDGE_TOPOLOGYHASCHANGED;
			}
		}
	}

	
}

// A node is deemed moveable, if there are no triangles surrounding it which
// have less than 3 adjacency
bool
IsNodeMoveable(CNavGenNode * pNode)
{
	if(pNode->m_iFlags & NAVGENNODE_ON_EDGE_OF_MESH)
		return false;

	if(pNode->IsRemoved())
		return false;

	int iNumTris = 0;
	int iNumEdges = 0;

	u32 e, t;

	for(t=0; t<pNode->m_TrianglesUsingThisNode.size(); t++)
	{
		CNavSurfaceTri * pTri = pNode->m_TrianglesUsingThisNode[t];
		if(pTri->IsRemoved())
			continue;
		iNumTris++;
	}
	for(e=0; e<pNode->m_EdgesUsingThisNode.size(); e++)
	{
		CNavTriEdge * pEdge = pNode->m_EdgesUsingThisNode[e];
		if(pEdge->IsRemoved())
			continue;
		iNumEdges++;
	}

	return (iNumTris == iNumEdges);
}


int
GetEdgeIndexFromNodes(CNavSurfaceTri * pTri, CNavGenNode * pNode1, CNavGenNode * pNode2)
{
	if((pTri->m_Nodes[0] == pNode1 && pTri->m_Nodes[1] == pNode2) || (pTri->m_Nodes[0] == pNode2 && pTri->m_Nodes[1] == pNode1))
	{
		return 0;
	}
	else if((pTri->m_Nodes[1] == pNode1 && pTri->m_Nodes[2] == pNode2) || (pTri->m_Nodes[1] == pNode2 && pTri->m_Nodes[2] == pNode1))
	{
		return 1;
	}
	else if((pTri->m_Nodes[2] == pNode1 && pTri->m_Nodes[0] == pNode2) || (pTri->m_Nodes[2] == pNode2 && pTri->m_Nodes[0] == pNode1))
	{
		return 2;
	}

	return -1;
}


void
RecomputeAdjacencyAfterVertexCollapse(CNavGenNode * pNodeToRemove, CNavGenNode * pNodeMovedOnto, CNavSurfaceTri * pTriRemoved1, CNavSurfaceTri * pTriRemoved2)
{
	u32 t, i, t2, i2;

	CNavSurfaceTri * pRemovedAjacentTris[8] = { NULL, NULL };
	CNavSurfaceTri * pMovedOntoAjacentTris[8] = { NULL, NULL };
	int iNumRemovedAdjacentTris = 0;
	int iNumMovedOntoAdjacentTris = 0;

	// Do for all triangles surrounding the node to remove :
	// If we find a triangle pointer to either of the just-removed tri's, then delete it from the list
	// If we find up to 2 triangles which were adjacent to either of the just-remove tri's, then make a note of them
	for(t=0; t<pNodeToRemove->m_TrianglesUsingThisNode.size(); t++)
	{
		CNavSurfaceTri * pTri = pNodeToRemove->m_TrianglesUsingThisNode[t];

		if(pTri->IsRemoved())
			continue;

		if(pTri == pTriRemoved1 || pTri == pTriRemoved2)
		{
			Assert(0);	// shouldn't get here anymore
			pNodeToRemove->m_TrianglesUsingThisNode.erase(pNodeToRemove->m_TrianglesUsingThisNode.begin() + t);
			t--;
			continue;
		}
		if(pTri->m_AdjacentTris[0] == pTriRemoved1 || pTri->m_AdjacentTris[1] == pTriRemoved1 || pTri->m_AdjacentTris[2] == pTriRemoved1 ||
			pTri->m_AdjacentTris[0] == pTriRemoved2 || pTri->m_AdjacentTris[1] == pTriRemoved2 || pTri->m_AdjacentTris[2] == pTriRemoved2)
		{
			pRemovedAjacentTris[iNumRemovedAdjacentTris++] = pTri;
		}
	}

	// Do for all triangles surrounding the node which has been moved onto :
	// If we find a triangle pointer to either of the just-removed tri's, then delete it from the list
	// If we find up to 2 triangles which were adjacent to either of the just-remove tri's, then make a note of them
	for(t=0; t<pNodeMovedOnto->m_TrianglesUsingThisNode.size(); t++)
	{
		CNavSurfaceTri * pTri = pNodeMovedOnto->m_TrianglesUsingThisNode[t];

		if(pTri->IsRemoved())
			continue;

		if(pTri == pTriRemoved1 || pTri == pTriRemoved2)
		{
			Assert(0);	// shouldn't get here anymore
			pNodeMovedOnto->m_TrianglesUsingThisNode.erase(pNodeMovedOnto->m_TrianglesUsingThisNode.begin() + t);
			t--;
			continue;
		}
		if(pTri->m_AdjacentTris[0] == pTriRemoved1 || pTri->m_AdjacentTris[1] == pTriRemoved1 || pTri->m_AdjacentTris[2] == pTriRemoved1 ||
			pTri->m_AdjacentTris[0] == pTriRemoved2 || pTri->m_AdjacentTris[1] == pTriRemoved2 || pTri->m_AdjacentTris[2] == pTriRemoved2)
		{
			pMovedOntoAjacentTris[iNumMovedOntoAdjacentTris++] = pTri;
		}
	}

	if(iNumRemovedAdjacentTris > 2 || iNumMovedOntoAdjacentTris > 2)
	{
		Assert(0);	// something is wrong
	}

	// Now go back through the triangles which we have identified as being adjacent to the
	// two triangles which have just been removed, and link them to each other appropriately.
	for(t=0; t<(u32)iNumRemovedAdjacentTris; t++)
	{
		for(i=0; i<3; i++)
		{
			if(pRemovedAjacentTris[t]->m_AdjacentTris[i] == pTriRemoved1)
			{
				for(t2=0; t2<(u32)iNumMovedOntoAdjacentTris; t2++)
				{
					for(i2=0; i2<3; i2++)
					{
						if(pMovedOntoAjacentTris[t2]->m_AdjacentTris[i2] == pTriRemoved1)
						{
							pRemovedAjacentTris[t]->m_AdjacentTris[i] = pMovedOntoAjacentTris[t2];
							pMovedOntoAjacentTris[t2]->m_AdjacentTris[i2] = pRemovedAjacentTris[t];
						}
					}
				}
			}
			if(pRemovedAjacentTris[t]->m_AdjacentTris[i] == pTriRemoved2)
			{
				for(t2=0; t2<(u32)iNumMovedOntoAdjacentTris; t2++)
				{
					for(i2=0; i2<3; i2++)
					{
						if(pMovedOntoAjacentTris[t2]->m_AdjacentTris[i2] == pTriRemoved2)
						{
							pRemovedAjacentTris[t]->m_AdjacentTris[i] = pMovedOntoAjacentTris[t2];
							pMovedOntoAjacentTris[t2]->m_AdjacentTris[i2] = pRemovedAjacentTris[t];
						}
					}
				}
			}
		}
	}
	
	for(t=0; t<(u32)iNumMovedOntoAdjacentTris; t++)
	{
		for(i=0; i<3; i++)
		{
			if(pMovedOntoAjacentTris[t]->m_AdjacentTris[i] == pTriRemoved1)
			{
				for(t2=0; t2<(u32)iNumRemovedAdjacentTris; t2++)
				{
					for(i2=0; i2<3; i2++)
					{
						if(pRemovedAjacentTris[t2]->m_AdjacentTris[i2] == pTriRemoved1)
						{
							pMovedOntoAjacentTris[t]->m_AdjacentTris[i] = pRemovedAjacentTris[t2];
							pRemovedAjacentTris[t2]->m_AdjacentTris[i2] = pMovedOntoAjacentTris[t];
						}
					}
				}
			}
			if(pMovedOntoAjacentTris[t]->m_AdjacentTris[i] == pTriRemoved2)
			{
				for(t2=0; t2<(u32)iNumRemovedAdjacentTris; t2++)
				{
					for(i2=0; i2<3; i2++)
					{
						if(pRemovedAjacentTris[t2]->m_AdjacentTris[i2] == pTriRemoved2)
						{
							pMovedOntoAjacentTris[t]->m_AdjacentTris[i] = pRemovedAjacentTris[t2];
							pRemovedAjacentTris[t2]->m_AdjacentTris[i2] = pMovedOntoAjacentTris[t];
						}
					}
				}
			}
		}
	}

}



// Calculates the vertex normal at a node by examining the plane eq's of the surrounding triangles.
// Can optionally also specify a node which, if found in any surrounding face, will be substituded for
// another node for the purposes of the calculation.  This is to help evaluate the 'cost' value of moving
// a node.  Need to be aware that when this is called using a node substitution, some of the triangles
// may become degenerate
#define MAX_SURROUNDING_TRIS	16

bool
CalculateNodeNormal(Vector3 & vAveragedNormal, CNavGenNode * pNode, CNavGenNode * pNodeToSubstitute, CNavGenNode * pNodeToSubstituteWith)
{
	Vector3 vPlaneNormals[MAX_SURROUNDING_TRIS];
	float fPlaneDists[MAX_SURROUNDING_TRIS];

	Vector3 vPlaneNormal;
	float fPlaneDist;
	int iNumUniquePlanes = 0;
	Vector3 edge1, edge2;
	Vector3 * v0, * v1, * v2;
	float dot;

	vAveragedNormal = Vector3(0,0,0);

	const float fSameNormalEps = 0.95f;

	u32 t, p;
	for(t=0; t<pNode->m_TrianglesUsingThisNode.size(); t++)
	{
		CNavSurfaceTri * pTri = pNode->m_TrianglesUsingThisNode[t];

		if(pTri->IsRemoved())
			continue;

		if(pNodeToSubstitute)
		{
			// Find which vertex is the node to move
			if(pTri->m_Nodes[0] == pNodeToSubstitute)
			{
				v0 = &pNodeToSubstituteWith->m_vBasePos;
				v1 = &pTri->m_Nodes[1]->m_vBasePos;
				v2 = &pTri->m_Nodes[2]->m_vBasePos;
			}
			else if(pTri->m_Nodes[1] == pNodeToSubstitute)
			{
				v0 = &pTri->m_Nodes[0]->m_vBasePos;
				v1 = &pNodeToSubstituteWith->m_vBasePos;
				v2 = &pTri->m_Nodes[2]->m_vBasePos;
			}
			else
			{
				v0 = &pTri->m_Nodes[0]->m_vBasePos;
				v1 = &pTri->m_Nodes[1]->m_vBasePos;
				v2 = &pNodeToSubstituteWith->m_vBasePos;
			}

			// Check that this triangle isn't degenerate due to the node substitution.
			if(v0 == v1 || v0 == v2 || v1 == v2)
				continue;
		}
		else
		{
			v0 = &pTri->m_Nodes[0]->m_vBasePos;
			v1 = &pTri->m_Nodes[1]->m_vBasePos;
			v2 = &pTri->m_Nodes[2]->m_vBasePos;
		}


		// Calculate the triangle plane
		edge1 = *v1 - *v0;
		edge2 = *v2 - *v0;
		vPlaneNormal = CrossProduct(edge2, edge1);
		vPlaneNormal.Normalize();
		fPlaneDist = - DotProduct(vPlaneNormal, *v0);

		// See if we already have a plane with this normal ?
		for(p=0; p<(u32)iNumUniquePlanes; p++)
		{
			dot = DotProduct(vPlaneNormals[p], vPlaneNormal);
			if(dot >= fSameNormalEps)
				break;
		}
		if(p == (u32)iNumUniquePlanes && iNumUniquePlanes<MAX_SURROUNDING_TRIS)
		{
			vPlaneNormals[iNumUniquePlanes] = vPlaneNormal;
			fPlaneDists[iNumUniquePlanes] = fPlaneDist;
			iNumUniquePlanes++;

			vAveragedNormal += vPlaneNormal;
		}
	}

	if(!iNumUniquePlanes)
	{
		return false;
	}
	else if(iNumUniquePlanes > 1)
	{
		vAveragedNormal /= ((float)iNumUniquePlanes);
	}

	return true;
}






// This function calculates a cost value associated with moving "pNode" onto "pNodeToMoveOnto"
// The cost is a function of the change in curvature of the surrounding mesh (quadric error metric method)
float
CNavGen::EvaluateVertexMove(CNavGenNode * pNode, CNavGenNode * pNodeToMoveOnto)
{
	if(pNode->IsRemoved() || pNodeToMoveOnto->IsRemoved())
		return FLT_MAX;

	// Try to preserve water edges
	if(pNode->m_bIsWater != pNodeToMoveOnto->m_bIsWater)
		return FLT_MAX;
	// Try to preserve water edges
	if(pNode->m_bIsUnderwater != pNodeToMoveOnto->m_bIsUnderwater)
		return FLT_MAX;

	const bool bBothNodesAreWater = (pNode->m_bIsWater || pNodeToMoveOnto->m_bIsWater);

	u32 t;

	if(m_bDoWeCareAboutSurfaceTypes && pNode->m_TrianglesUsingThisNode.size())
	{
		// Never move a node if any of the surrounding triangles have different IsPavement() flag settings
		CNavSurfaceTri * pTri0 = pNode->m_TrianglesUsingThisNode[0];
		bool bPavement = pTri0->m_colPolyData.GetIsPavement();
		bool bInterior = pTri0->m_colPolyData.GetIsInterior();
		bool bNoNetworkSpawn = pTri0->m_colPolyData.GetIsNoNetworkSpawn();
		//float fPedDensity = pTri0->m_colPolyData.GetPedDensity();

		for(t=1; t<pNode->m_TrianglesUsingThisNode.size(); t++)
		{
			CNavSurfaceTri * pTri = pNode->m_TrianglesUsingThisNode[t];
			if(pTri->m_colPolyData.GetIsPavement() != bPavement)
				return FLT_MAX;
			if(pTri->m_colPolyData.GetIsInterior() != bInterior)
				return FLT_MAX;
			if(pTri->m_colPolyData.GetIsNoNetworkSpawn() != bNoNetworkSpawn)
				return FLT_MAX;
//			if( (pTri->m_colPolyData.GetPedDensity() == 0.0f && fPedDensity > 0.0f) ||
//				(pTri->m_colPolyData.GetPedDensity() > 0.0f && fPedDensity == 0.0f) )
//				return FLT_MAX;
			if( pTri->m_bIsWater != pTri0->m_bIsWater )
				return FLT_MAX;
		}

		// Never move a node if any of the surrounding triangles have differing backed-in data bitfields
		u8 iBakedInData = pTri0->m_iBakedInBitField;

		for(t=1; t<pNode->m_TrianglesUsingThisNode.size(); t++)
		{
			CNavSurfaceTri * pTri = pNode->m_TrianglesUsingThisNode[t];
			if(pTri->m_iBakedInBitField != iBakedInData)
				return FLT_MAX;
		}
	}

	// Check steepness.  If the "from" node has a mixture of steep/non-steep polys surrounding it
	// then we cannot move it.
	int iNumTooSteep = 0;
	int iNumNotTooSteep = 0;
	for(t=0; t<pNode->m_TrianglesUsingThisNode.size(); t++)
	{
		CNavSurfaceTri * pTri = pNode->m_TrianglesUsingThisNode[t];
		if(pTri->IsRemoved())
			continue;
		if(pTri->m_bIsTooSteep)
			iNumTooSteep++;
		else
			iNumNotTooSteep++;
	}
	if(iNumTooSteep > 0 && iNumNotTooSteep > 0)
		return FLT_MAX;

	// Imagine that the vertex at "pNode" is in its new position at "pNodeToMoveOnto".
	// Calculate the plane equations for all the triangles which use "pNode", as if
    // the vertex is at it's new position.  Now sum up the distances from these planes
	// to the original "pNode" position.  This value (plus any special-case modifiers)
	// is the "cost" of moving this vertex.  The idea is to choose the vertex-move
	// with the least cost.

	const Vector3 vUpVector = ZAXIS;

	Vector3 vOriginalPlaneNormal;
	float fOriginalPlaneDist;

	Vector3 vPlaneNormal;
	float fPlaneDist;

	Vector3 * v0, * v1, * v2;
	Vector3 verts[3];
	Vector3 edge1, edge2, edge3;

	vector<CNavSurfaceTri*> triangleList;
	triangleList.reserve(32);
	
	float dot;
	u32 e;

	//**********************************************************
	// Firstly check that all of the triangles surrounding this
	// node have 3 neighbouring triangles.  At this stage we
	// don't want to be moving boundary triangles at all.
	//**********************************************************

	if(m_bDontMoveNonConnectedFaces)
	{
		for(t=0; t<pNode->m_TrianglesUsingThisNode.size(); t++)
		{
			CNavSurfaceTri * pTri = pNode->m_TrianglesUsingThisNode[t];
			if(pTri->IsRemoved())
				continue;

			if(!pTri->m_AdjacentTris[0] || !pTri->m_AdjacentTris[1] || !pTri->m_AdjacentTris[2])
			{
				return FLT_MAX;
			}
		}
	}
	else
	{
		if(!IsNodeMoveable(pNode))
		{
			return FLT_MAX;
		}
	}

	//*******************************************************************
	// Secondly, don't let one node become surrounded by loads of faces
	//*******************************************************************
	int iTooManyTriangles = m_Params.m_iMaxNumberOfTrianglesSurroundingAnyNode;

	int iNumActiveTriangles = 0;
	for(t=0; t<pNode->m_TrianglesUsingThisNode.size(); t++)
		if(!pNode->m_TrianglesUsingThisNode[t]->IsRemoved())
			iNumActiveTriangles++;
	for(t=0; t<pNodeToMoveOnto->m_TrianglesUsingThisNode.size(); t++)
		if(!pNodeToMoveOnto->m_TrianglesUsingThisNode[t]->IsRemoved())
			iNumActiveTriangles++;

	//int iNumTrianglesAroundNewNode = (pNode->m_TrianglesUsingThisNode.size() + pNodeToMoveOnto->m_TrianglesUsingThisNode.size()) - 4;
	//if(iNumTrianglesAroundNewNode > iTooManyTriangles)
	if(iNumActiveTriangles-4 > iTooManyTriangles)
	{
		return FLT_MAX;
	}

	//**************************************************************
	// Make sure that no triangles nearby the proposed edge-collapse
	// may be flipped or otherwise turned upside-down by this
	// operation.
	//**************************************************************

	for(t=0; t<pNode->m_TrianglesUsingThisNode.size(); t++)
	{
		CNavSurfaceTri * pTri = pNode->m_TrianglesUsingThisNode[t];
		if(pTri->IsRemoved())
			continue;

		triangleList.push_back(pTri);
	}
	for(t=0; t<pNodeToMoveOnto->m_TrianglesUsingThisNode.size(); t++)
	{
		CNavSurfaceTri * pTri = pNodeToMoveOnto->m_TrianglesUsingThisNode[t];
		if(pTri->IsRemoved())
			continue;

		triangleList.push_back(pTri);
	}
	for(e=0; e<pNode->m_EdgesUsingThisNode.size(); e++)
	{
		CNavTriEdge * pEdge = pNode->m_EdgesUsingThisNode[e];

		if(pEdge->IsRemoved())
			continue;

		if(pEdge->m_pTri1->IsRemoved())
			continue;

		triangleList.push_back(pEdge->m_pTri1);

		if(pEdge->m_pTri2->IsRemoved())
			continue;

		triangleList.push_back(pEdge->m_pTri2);
	}

	for(e=0; e<pNodeToMoveOnto->m_EdgesUsingThisNode.size(); e++)
	{
		CNavTriEdge * pEdge = pNodeToMoveOnto->m_EdgesUsingThisNode[e];

		if(pEdge->IsRemoved())
			continue;

		if(pEdge->m_pTri1->IsRemoved())
			continue;

		triangleList.push_back(pEdge->m_pTri1);

		if(pEdge->m_pTri2->IsRemoved())
			continue;

		triangleList.push_back(pEdge->m_pTri2);
	}


	for(t=0; t<triangleList.size(); t++)
	{
		CNavSurfaceTri * pTri = triangleList[t];

		if(pTri->IsRemoved())
			continue;

		// First we want to exclude the (up to) two triangles which will
		// disappear as a result of this operation.  These will be the tri's
		// which have both "pNode" and "pNodeToMoveOnto" amongst their vertices.
		if((pTri->m_Nodes[0] == pNode || pTri->m_Nodes[1] == pNode || pTri->m_Nodes[2] == pNode) &&
			(pTri->m_Nodes[0] == pNodeToMoveOnto || pTri->m_Nodes[1] == pNodeToMoveOnto || pTri->m_Nodes[2] == pNodeToMoveOnto))
		{
			continue;
		}

		// Find which vertex is the node to move
		if(pTri->m_Nodes[0] == pNode)
		{
			verts[0] = pNodeToMoveOnto->m_vBasePos;		verts[1] = pTri->m_Nodes[1]->m_vBasePos;	verts[2] = pTri->m_Nodes[2]->m_vBasePos;
		}
		else if(pTri->m_Nodes[1] == pNode)
		{
			verts[0] = pTri->m_Nodes[0]->m_vBasePos;	verts[1] = pNodeToMoveOnto->m_vBasePos;		verts[2] = pTri->m_Nodes[2]->m_vBasePos;
		}
		else if(pTri->m_Nodes[2] == pNode)
		{
			verts[0] = pTri->m_Nodes[0]->m_vBasePos;	verts[1] = pTri->m_Nodes[1]->m_vBasePos;	verts[2] = pNodeToMoveOnto->m_vBasePos;
		}
		else
		{
			continue;
		}

		PlaneFromPoints(verts, &vPlaneNormal, fPlaneDist);

		float fDot = DotProduct(vPlaneNormal, vUpVector);
		if(fDot <= 0.0f)
		{
			return FLT_MAX;
		}
	}

	//**********************************************************
	// Go through the triangles surrounding the node to move
	// and check that the proposed move wouldn't invalidate the
	// traingle by reducing it to a colinear or degenerate tri.
	// NB : This should probably be done for both the nodes on
	// this edge, and not just the one which is moving.
	//**********************************************************

	float fQuadricErrorMetric = 0.0f;

	triangleList.clear();

	for(t=0; t<pNode->m_TrianglesUsingThisNode.size(); t++)
	{
		CNavSurfaceTri * pTri = pNode->m_TrianglesUsingThisNode[t];
		if(pTri->IsRemoved())
			continue;

		// First we want to exclude the (up to) two triangles which will
		// disappear as a result of this operation.  These will be the tri's
		// which have both "pNode" and "pNodeToMoveOnto" amongst their vertices.
		if((pTri->m_Nodes[0] == pNode || pTri->m_Nodes[1] == pNode || pTri->m_Nodes[2] == pNode) &&
			(pTri->m_Nodes[0] == pNodeToMoveOnto || pTri->m_Nodes[1] == pNodeToMoveOnto || pTri->m_Nodes[2] == pNodeToMoveOnto))
		{
			continue;
		}

		triangleList.push_back(pTri);
	}

	for(t=0; t<triangleList.size(); t++)
	{
		CNavSurfaceTri * pTri = triangleList[t];

		if(pTri->IsRemoved())
			continue;

		bool bIsTriangleFromAroundMovingNode = true;//( t < iNumTrianglesFromMovingNode );

		// A quick sanity check
		Assert(pTri->m_Nodes[0] == pNode || pTri->m_Nodes[1] == pNode || pTri->m_Nodes[2] == pNode);

#ifdef INDEPTH_OPTIMISER_SANITYCHECK
		for(int s=0; s<3; s++)
		{
			if(pTri->m_Nodes[s] == pNode)
			{
				ASSERT(pTri->m_Nodes[(s+1)%3] != pNode && pTri->m_Nodes[(s+2)%3] != pNode);
			}
			if(pTri->m_Nodes[s] == pNodeToMoveOnto)
			{
				ASSERT(pTri->m_Nodes[(s+1)%3] != pNodeToMoveOnto && pTri->m_Nodes[(s+2)%3] != pNodeToMoveOnto);
			}
		}
#endif

		// Find which vertex is the node to move
		if(pTri->m_Nodes[0] == pNode)
		{
			v0 = &pNodeToMoveOnto->m_vBasePos;
			v1 = &pTri->m_Nodes[1]->m_vBasePos;
			v2 = &pTri->m_Nodes[2]->m_vBasePos;
		}
		else if(pTri->m_Nodes[1] == pNode)
		{
			v0 = &pTri->m_Nodes[0]->m_vBasePos;
			v1 = &pNodeToMoveOnto->m_vBasePos;
			v2 = &pTri->m_Nodes[2]->m_vBasePos;
		}
		else if(pTri->m_Nodes[2] == pNode)
		{
			v0 = &pTri->m_Nodes[0]->m_vBasePos;
			v1 = &pTri->m_Nodes[1]->m_vBasePos;
			v2 = &pNodeToMoveOnto->m_vBasePos;
		}
		else
		{
			continue;
		}

		//*********************************************************************
		// Calculate the triangle plane with the vertex in its new position
		// Get the edge lengths of the new tri
		//*********************************************************************
		
		edge1 = *v1 - *v0;
		edge2 = *v2 - *v0;
		edge3 = *v2 - *v1;

		const float l0 = NormalizeAndMag(edge1);
		const float l1 = NormalizeAndMag(edge2);
		const float l2 = NormalizeAndMag(edge3);

		vPlaneNormal = CrossProduct(edge2, edge1);
		vPlaneNormal.Normalize();
		fPlaneDist = - DotProduct(vPlaneNormal, *v0);


		//*****************************************************************
		// Apply penalties for forming triangles whose edges are too long,
		// or forming triangles which are slivers.
		//*****************************************************************

		static const float fWaterSizeMultiplier = 2.0f;
		const float fSizeMultiplier = bBothNodesAreWater ? fWaterSizeMultiplier : 1.0f;

		// Never allow any edge to get too large
		const float fMaxEdgeLength = m_Params.m_fMaxTriangleSideLength * fSizeMultiplier;
		if(l0 > fMaxEdgeLength || l1 > fMaxEdgeLength || l2 > fMaxEdgeLength)
		{
			return FLT_MAX;
		}

		// Never allow any edge to get too small
		static const float fSmallLength = m_Params.m_fMinTriangleSideLength;
		if(l0 < fSmallLength || l1 < fSmallLength || l2 < fSmallLength)
		{
			return FLT_MAX;
		}

		// Calculate the tri area from the edge lengths
		const float a = CalcTriArea(l0, l1, l2);

		// Can't move this vertex - it would make the triangle too small
		static const float fMinTriArea = m_Params.m_fMinTriangleArea;
		static const float fMaxTriArea = m_Params.m_fMaxTriangleArea * fSizeMultiplier;
		
		if(a <= fMinTriArea)
		{
			return FLT_MAX;
		}

		if(a > fMaxTriArea)
		{
			return FLT_MAX;
		}

		//******************************************************************
		// Make sure that no angles in the new triangle would be too small
		//******************************************************************

		const float fMinTriAngle = m_Params.m_fMinTriangleAngle * DtoR;

		float dot = DotProduct(edge1, edge2);
		dot = Clamp(dot, -1.0f, 1.0f);
		float angle1 = rage::Acosf(dot);

		// reverse edge 1 to take the dot product & get the 2nd triangle angle
		edge1 = *v0 - *v1;
		edge1.Normalize();

		dot = DotProduct(edge1, edge3);
		dot = Clamp(dot, -1.0f, 1.0f);
		float angle2 = Acosf(dot);
		//float angle3 = 180.0f - (angle1 + angle2);
		float angle3 = PI - (angle1 + angle2);

		if(angle1 < fMinTriAngle || angle2 < fMinTriAngle || angle3 < fMinTriAngle)
			return FLT_MAX;


		//******************************************************************************
		// Check that this move wouldn't make the tri degenerate, or flip it's normal,
		// or turn it upside down so that it't normal is facing downwards
		//******************************************************************************

		v0 = &pTri->m_Nodes[0]->m_vBasePos;
		v1 = &pTri->m_Nodes[1]->m_vBasePos;
		v2 = &pTri->m_Nodes[2]->m_vBasePos;

		edge1 = *v1 - *v0;
		edge2 = *v2 - *v0;

		edge1.Normalize();
		edge2.Normalize();

		vOriginalPlaneNormal = CrossProduct(edge2, edge1);
		vOriginalPlaneNormal.Normalize();

		fOriginalPlaneDist = - DotProduct(vOriginalPlaneNormal, pNode->m_vBasePos);

		// Can't move this vertex - it would flip the triangle
		if(DotProduct(vOriginalPlaneNormal, vPlaneNormal) <= 0.0f)
		{
			return FLT_MAX;
		}

		// Can't move this vertex - it would cause the triangle's normal to point down
		// so that the slope would exceed the maximum walkable angle

		const float fMaxAngleForPoly = bBothNodesAreWater ?
			m_Params.m_fMaxAngleForWaterPolyToBeInNavMesh : m_Params.m_fMaxAngleForPolyToBeInNavMesh;

		if(DotProduct(vPlaneNormal, vUpVector) <= Cosf(fMaxAngleForPoly * DtoR))
		{
			return FLT_MAX;
		}

		// Total up the error metric.  This value is this sum of distances from the
		// original planes surrounding this node, to the node's proposed new position.
		// This is a measure of how much the change will affect the local curvature of
		// the surface : for flat surfaces this is zero.

		if(bIsTriangleFromAroundMovingNode)
		{
			float fCost = (DotProduct(pNodeToMoveOnto->m_vBasePos, vOriginalPlaneNormal) + fOriginalPlaneDist);
			fCost = fCost*fCost;

			fQuadricErrorMetric += fCost;
		}
	}



	//***********************************************************************************************************
	// Calculate the cost value based upon the change of curvature to the surface surrounding the change.
	// Calculate the vertex normals at each node
	// NB : What this should *really* do is to visit all the nodes surrounding the node which is to move,
	// and calculate the before & after normals, and sum the differences.  This would be the best measure
	// of curvature change - but for this, we need to store a list of EDGES in each node (btw. this would also
	// make way for a big optimisation to replace the linear searching the edge-list after a vertex-move)..
	//***********************************************************************************************************

	Vector3 vMoveOntoNodeNormal_BeforeMove;
	Vector3 vMoveOntoNodeNormal_MinusMovingNode;
	Vector3 vMovingNodeNormal_AfterMove;
	Vector3 vMoveOntoNodeNormal_AfterMove;

	CalculateNodeNormal(vMoveOntoNodeNormal_BeforeMove, pNodeToMoveOnto, NULL, NULL);
	CalculateNodeNormal(vMoveOntoNodeNormal_MinusMovingNode, pNodeToMoveOnto, pNode, pNodeToMoveOnto);
	CalculateNodeNormal(vMovingNodeNormal_AfterMove, pNode, pNode, pNodeToMoveOnto);

	// add these two together & normalise for an approximate estimate of how the node normal will apprear after the move
	vMoveOntoNodeNormal_AfterMove = (vMoveOntoNodeNormal_MinusMovingNode + vMovingNodeNormal_AfterMove) * 0.5f;
	vMoveOntoNodeNormal_AfterMove.Normalize();

	dot = DotProduct(vMoveOntoNodeNormal_BeforeMove, vMoveOntoNodeNormal_AfterMove);

	// if the dot product of the before & after vectors is below this amount, then never allow the vertex move
	static const float fMinimumNormalDotAllowed = Cosf(45.0f * DtoR);
	static const float fMinimumNormalDotAllowedForWater = Cosf(70.0f * DtoR);
	const float fMinimumNormalDot = bBothNodesAreWater ? fMinimumNormalDotAllowedForWater : fMinimumNormalDotAllowed;

	if(dot < fMinimumNormalDot)
		return FLT_MAX;

	static const float fMaxErrorMetricForWater	= 20.0f;

	const float fMaxError = bBothNodesAreWater ? fMaxErrorMetricForWater : m_Params.m_OptimizeMaxQuadricErrorMetric;

	if(fQuadricErrorMetric > fMaxError)
		return FLT_MAX;

	return fQuadricErrorMetric;
}


bool CNavGen::MakeSureEdgeCollapseDoesntFlipAnyTriangles(CNavGenNode * pNode, CNavGenNode * pNodeToMoveOnto)
{
	Vector3 verts[3];
	Vector3 vPlaneNormal;
	float fPlaneDist;
	float fDot;
	Vector3 vUpVector(0, 0, 1.0f);

	for(u32 t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];

		if(pTri->IsRemoved())
			continue;

		// First we want to exclude the (up to) two triangles which will
		// disappear as a result of this operation.  These will be the tri's
		// which have both "pNode" and "pNodeToMoveOnto" amongst their vertices.
		if((pTri->m_Nodes[0] == pNode || pTri->m_Nodes[1] == pNode || pTri->m_Nodes[2] == pNode) &&
			(pTri->m_Nodes[0] == pNodeToMoveOnto || pTri->m_Nodes[1] == pNodeToMoveOnto || pTri->m_Nodes[2] == pNodeToMoveOnto))
		{
			continue;
		}

		// Find which vertex is the node to move
		if(pTri->m_Nodes[0] == pNode)
		{
			verts[0] = pNodeToMoveOnto->m_vBasePos;		verts[1] = pTri->m_Nodes[1]->m_vBasePos;	verts[2] = pTri->m_Nodes[2]->m_vBasePos;
		}
		else if(pTri->m_Nodes[1] == pNode)
		{
			verts[0] = pTri->m_Nodes[0]->m_vBasePos;	verts[1] = pNodeToMoveOnto->m_vBasePos;		verts[2] = pTri->m_Nodes[2]->m_vBasePos;
		}
		else if(pTri->m_Nodes[2] == pNode)
		{
			verts[0] = pTri->m_Nodes[0]->m_vBasePos;	verts[1] = pTri->m_Nodes[1]->m_vBasePos;	verts[2] = pNodeToMoveOnto->m_vBasePos;
		}
		else
		{
			continue;
		}

		PlaneFromPoints(verts, &vPlaneNormal, fPlaneDist);

		fDot = DotProduct(vPlaneNormal, vUpVector);
		if(fDot <= 0.0f)
		{
			return false;
		}
	}

	return true;
}


bool TestObjLos(fwDynamicEntityInfo * pObj, const Vector3 & vStart, const Vector3 & vEnd)
{
	Vector3 vClippedStart = vStart;
	Vector3 vClippedEnd = vEnd;
	float fStartDot, fEndDot, s;
	static const float fEps = 0.0f;
	int p;

	for(p=0; p<4; p++)
	{
		fStartDot = GetPlaneDist(pObj->m_Planes[p], vClippedStart);
		fEndDot = GetPlaneDist(pObj->m_Planes[p], vClippedEnd);

		// Both start & end are in front of a plane, so no intersection is possible
		if(fStartDot > fEps && fEndDot > fEps)
		{
			break;
		}
		else if(fStartDot > fEps && fEndDot < -fEps)
		{
			s = fStartDot / (fStartDot - fEndDot);
			vClippedStart = vClippedStart + ((vClippedEnd - vClippedStart) * s);
		}
		else if(fStartDot < -fEps)
		{
			if(fEndDot > fEps)
			{
				s = fStartDot / (fStartDot - fEndDot);
				vClippedEnd = vClippedStart + ((vClippedEnd - vClippedStart) * s);
			}
		}
	}

	if(p==4)
		return false;
	return true;
}


void CNavGen::CreateCollapseEdges()
{
	// This is the list of edge collapses we shall consider.  We will initially set this up
	// by going through the list of nav triangles in this leaf node & creating edges.  However,
	// after this initial set-up step, we can keep traversing the "triEdges" list repeatedly
	// collapsing edges without need to revisit the triangles list at all.
	// If we wish later to do a global optimisation (ie. without localising it to octree leaves)
	// then we should maintain a global list of edges, which we add the remaining edges too
	// after we've finished our local 'in-leaf' optimising.

	//********************************************************************
	// First phase - create a list of shared edges
	//********************************************************************

	u32 t;

	for(t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];

		if(pTri->IsRemoved())
		{
			continue;
		}
		if(pTri->m_bDoNotOptimise)
		{
			continue;
		}

		const bool bCareAboutEdgeOrdering = true; //false;	// true

		// If the edge defined by 'vertices' 0 and 1 has not yet been processed
		if(!(pTri->m_iFlags & NAVSURFTRI_EDGE_0_TO_1_CACHED))
		{
			// At this stage, we cannot collaspe an edge which has only one side
			if(pTri->m_AdjacentTris[0] && (!(pTri->m_AdjacentTris[0]->IsRemoved())))
			{
				CNavTriEdge * pEdge = rage_new CNavTriEdge();
				m_NavTriEdges.push_back(pEdge);
				m_iTotalNumEdgesInExistence++;
				pEdge->m_pTri1 = pTri;
				pEdge->m_pTri2 = pTri->m_AdjacentTris[0];
				pEdge->m_pNode1 = pTri->m_Nodes[0];
				pEdge->m_pNode2 = pTri->m_Nodes[1];
				m_fAverageEdgeLength += (pEdge->m_pNode1->m_vBasePos - pEdge->m_pNode2->m_vBasePos).Mag();
				m_fAverageEdgeLength *= 0.5f;

				pEdge->m_pNode1->m_EdgesUsingThisNode.push_back(pEdge);
				pEdge->m_pNode2->m_EdgesUsingThisNode.push_back(pEdge);

				m_TriEdgesForCollapsing.push_back(pEdge);
				pTri->m_iFlags |= NAVSURFTRI_EDGE_0_TO_1_CACHED;

				//int otherEdge = GetEdgeIndexFromNodes(pEdge->m_pTri2, pEdge->m_pNode1, pEdge->m_pNode2);
				//int otherEdge = pEdge->m_pTri2->GetAdjacencyEdge(pTri);
				int otherEdge = pEdge->m_pTri2->GetEdge(pEdge->m_pNode2, pEdge->m_pNode1, bCareAboutEdgeOrdering);
				Assert(otherEdge != -1 && pEdge->m_pTri2->m_AdjacentTris[otherEdge] != NULL);
				
				switch(otherEdge)
				{
					case 0:
						pEdge->m_pTri2->m_iFlags |= NAVSURFTRI_EDGE_0_TO_1_CACHED;
						break;
					case 1:
						pEdge->m_pTri2->m_iFlags |= NAVSURFTRI_EDGE_1_TO_2_CACHED;
						break;
					case 2:
						pEdge->m_pTri2->m_iFlags |= NAVSURFTRI_EDGE_2_TO_0_CACHED;
						break;
					default:
						printf("ERROR - adjacency not found");
						break;
				}
			}
		}

		// If the edge defined by 'vertices' 1 and 2 has not yet been processed
		if(!(pTri->m_iFlags & NAVSURFTRI_EDGE_1_TO_2_CACHED))
		{
			// At this stage, we cannot collaspe an edge which has only one side
			if(pTri->m_AdjacentTris[1] && (!(pTri->m_AdjacentTris[1]->IsRemoved())))
			{
				CNavTriEdge * pEdge = rage_new CNavTriEdge();
				m_NavTriEdges.push_back(pEdge);
				m_iTotalNumEdgesInExistence++;
				pEdge->m_pTri1 = pTri;
				pEdge->m_pTri2 = pTri->m_AdjacentTris[1];
				pEdge->m_pNode1 = pTri->m_Nodes[1];
				pEdge->m_pNode2 = pTri->m_Nodes[2];
				m_fAverageEdgeLength += (pEdge->m_pNode1->m_vBasePos - pEdge->m_pNode2->m_vBasePos).Mag();
				m_fAverageEdgeLength *= 0.5f;

				pEdge->m_pNode1->m_EdgesUsingThisNode.push_back(pEdge);
				pEdge->m_pNode2->m_EdgesUsingThisNode.push_back(pEdge);

				m_TriEdgesForCollapsing.push_back(pEdge);
				pTri->m_iFlags |= NAVSURFTRI_EDGE_1_TO_2_CACHED;

				//int otherEdge = GetEdgeIndexFromNodes(pEdge->m_pTri2, pEdge->m_pNode1, pEdge->m_pNode2);
				//int otherEdge = pEdge->m_pTri2->GetAdjacencyEdge(pTri);
				int otherEdge = pEdge->m_pTri2->GetEdge(pEdge->m_pNode2, pEdge->m_pNode1, bCareAboutEdgeOrdering);
				Assert(otherEdge != -1 && pEdge->m_pTri2->m_AdjacentTris[otherEdge] != NULL);
				
				switch(otherEdge)
				{
					case 0:
						pEdge->m_pTri2->m_iFlags |= NAVSURFTRI_EDGE_0_TO_1_CACHED;
						break;
					case 1:
						pEdge->m_pTri2->m_iFlags |= NAVSURFTRI_EDGE_1_TO_2_CACHED;
						break;
					case 2:
						pEdge->m_pTri2->m_iFlags |= NAVSURFTRI_EDGE_2_TO_0_CACHED;
						break;
					default:
						printf("ERROR - adjacency not found");
						break;
				}
			}
		}

		// If the edge defined by 'vertices' 2 and 0 has not yet been processed
		if(!(pTri->m_iFlags & NAVSURFTRI_EDGE_2_TO_0_CACHED))
		{
			// At this stage, we cannot collaspe an edge which has only one side
			if(pTri->m_AdjacentTris[2] && (!(pTri->m_AdjacentTris[2]->IsRemoved())))
			{
				CNavTriEdge * pEdge = rage_new CNavTriEdge();
				m_NavTriEdges.push_back(pEdge);
				m_iTotalNumEdgesInExistence++;
				pEdge->m_pTri1 = pTri;
				pEdge->m_pTri2 = pTri->m_AdjacentTris[2];
				pEdge->m_pNode1 = pTri->m_Nodes[2];
				pEdge->m_pNode2 = pTri->m_Nodes[0];
				m_fAverageEdgeLength += (pEdge->m_pNode1->m_vBasePos - pEdge->m_pNode2->m_vBasePos).Mag();
				m_fAverageEdgeLength *= 0.5f;

				pEdge->m_pNode1->m_EdgesUsingThisNode.push_back(pEdge);
				pEdge->m_pNode2->m_EdgesUsingThisNode.push_back(pEdge);

				m_TriEdgesForCollapsing.push_back(pEdge);
				pTri->m_iFlags |= NAVSURFTRI_EDGE_2_TO_0_CACHED;

				//int otherEdge = GetEdgeIndexFromNodes(pEdge->m_pTri2, pEdge->m_pNode1, pEdge->m_pNode2);
				//int otherEdge = pEdge->m_pTri2->GetAdjacencyEdge(pTri);
				int otherEdge = pEdge->m_pTri2->GetEdge(pEdge->m_pNode2, pEdge->m_pNode1, bCareAboutEdgeOrdering);
				Assert(otherEdge != -1 && pEdge->m_pTri2->m_AdjacentTris[otherEdge] != NULL);
				
				switch(otherEdge)
				{
					case 0:
						pEdge->m_pTri2->m_iFlags |= NAVSURFTRI_EDGE_0_TO_1_CACHED;
						break;
					case 1:
						pEdge->m_pTri2->m_iFlags |= NAVSURFTRI_EDGE_1_TO_2_CACHED;
						break;
					case 2:
						pEdge->m_pTri2->m_iFlags |= NAVSURFTRI_EDGE_2_TO_0_CACHED;
						break;
					default:
						printf("ERROR - adjacency not found");
						break;
				}
			}
		}
	}
}

//*****************************************************************************************
// When an edge is collapsing, and another edge has a reference to the node which is
// about to disappear, it is necessary to set the node pointer in the other edge to
// point to the node which is being collapsed onto.
// However, first we must ascertain whether the other edge actually borders either
// of the triangles which is about to also disappear.  If so, then the edge must
// be updated so that instead of pointing to a deceased triangle - it points to the
// triangle which it is about to become adjacent to as a result of the edge-collapse!
//*****************************************************************************************
bool UpdateAdjacency(CNavSurfaceTri * pTriToUpdate, CNavSurfaceTri * pRefToReplace, CNavSurfaceTri * pTriToReplaceItWith)
{
	if(!pTriToUpdate)
		return false;

	if(pTriToUpdate->IsRemoved())
		return false;

	if(pTriToUpdate->m_AdjacentTris[0] == pRefToReplace ||
		pTriToUpdate->m_AdjacentTris[1] == pRefToReplace ||
		 pTriToUpdate->m_AdjacentTris[2] == pRefToReplace)
	{
		if(pTriToUpdate->m_AdjacentTris[0] == pTriToReplaceItWith ||
			pTriToUpdate->m_AdjacentTris[1] == pTriToReplaceItWith ||
			 pTriToUpdate->m_AdjacentTris[2] == pTriToReplaceItWith)
		{
			Assertf(false, "was about to create a double adjacency in triangle 0x%p", pTriToUpdate);
			//pTriToUpdate->SetRemoved();
			return false;
		}
	}

	int t;
	for(t=0; t<3; t++)
	{
		if(pTriToUpdate->m_AdjacentTris[t] == pRefToReplace)
		{
			pTriToUpdate->m_AdjacentTris[t] = pTriToReplaceItWith;
			return true;
		}
	}
	return false;
}

// Connects the edges' triangle pointers for the edge which is moving, to bridge the gap
// Also updates the tri's adjacent[3] pointers to bridge the gap.

void 
CNavGen::MaybeStitchUpEdgeAcrossGap(CNavTriEdge * pCollapsingEdge, CNavTriEdge * pOtherEdge, CNavGenNode * pSharedNode)
{
	int last_v, v;

	if(pOtherEdge->m_pTri1 == pCollapsingEdge->m_pTri1)
	{
		last_v = 2;
		for(v=0; v<3; v++)
		{
			if(pCollapsingEdge->m_pTri1->m_Nodes[last_v] != pSharedNode && pCollapsingEdge->m_pTri1->m_Nodes[v] != pSharedNode)
			{
				pOtherEdge->m_pTri1 = pCollapsingEdge->m_pTri1->m_AdjacentTris[last_v];
				UpdateAdjacency(pOtherEdge->m_pTri1, pCollapsingEdge->m_pTri1, pOtherEdge->m_pTri2);
				UpdateAdjacency(pOtherEdge->m_pTri2, pCollapsingEdge->m_pTri1, pOtherEdge->m_pTri1);

				break;
			}
			last_v = v;
		}
	}
	if(pOtherEdge->m_pTri1 == pCollapsingEdge->m_pTri2)
	{
		last_v = 2;
		for(v=0; v<3; v++)
		{
			if(pCollapsingEdge->m_pTri2->m_Nodes[last_v] != pSharedNode && pCollapsingEdge->m_pTri2->m_Nodes[v] != pSharedNode)
			{
				pOtherEdge->m_pTri1 = pCollapsingEdge->m_pTri2->m_AdjacentTris[last_v];
				UpdateAdjacency(pOtherEdge->m_pTri1, pCollapsingEdge->m_pTri2, pOtherEdge->m_pTri2);
				UpdateAdjacency(pOtherEdge->m_pTri2, pCollapsingEdge->m_pTri2, pOtherEdge->m_pTri1);

				break;
			}
			last_v = v;
		}
	}

	if(pOtherEdge->m_pTri2 == pCollapsingEdge->m_pTri1)
	{
		last_v = 2;
		for(v=0; v<3; v++)
		{
			if(pCollapsingEdge->m_pTri1->m_Nodes[last_v] != pSharedNode && pCollapsingEdge->m_pTri1->m_Nodes[v] != pSharedNode)
			{
				pOtherEdge->m_pTri2 = pCollapsingEdge->m_pTri1->m_AdjacentTris[last_v];
				UpdateAdjacency(pOtherEdge->m_pTri1, pCollapsingEdge->m_pTri1, pOtherEdge->m_pTri2);
				UpdateAdjacency(pOtherEdge->m_pTri2, pCollapsingEdge->m_pTri1, pOtherEdge->m_pTri1);

				break;
			}
			last_v = v;
		}
	}
	if(pOtherEdge->m_pTri2 == pCollapsingEdge->m_pTri2)
	{
		last_v = 2;
		for(v=0; v<3; v++)
		{
			if(pCollapsingEdge->m_pTri2->m_Nodes[last_v] != pSharedNode && pCollapsingEdge->m_pTri2->m_Nodes[v] != pSharedNode)
			{
				pOtherEdge->m_pTri2 = pCollapsingEdge->m_pTri2->m_AdjacentTris[last_v];
				UpdateAdjacency(pOtherEdge->m_pTri1, pCollapsingEdge->m_pTri2, pOtherEdge->m_pTri2);
				UpdateAdjacency(pOtherEdge->m_pTri2, pCollapsingEdge->m_pTri2, pOtherEdge->m_pTri1);

				break;
			}
			last_v = v;
		}
	}

	// The other edge may have become invalid, in which case we'll need to remove it
	if(pOtherEdge->m_pTri1 == NULL || pOtherEdge->m_pTri2 == NULL)
	{
		pOtherEdge->SetRemoved();

		RemoveEdgeFromEdgeList(pOtherEdge->m_pNode1->m_EdgesUsingThisNode, pOtherEdge);
		RemoveEdgeFromEdgeList(pOtherEdge->m_pNode2->m_EdgesUsingThisNode, pOtherEdge);
	}
}



bool
CNavGen::OptimiseNavSurfaces(void)
{
	m_TriEdgesForCollapsing.clear();
	
	size_t iMaxPerIteration = m_NavSurfaceNodes.size() / 8;

	size_t iNumAdded = 0;

#if __DEV
	size_t iSizeBefore = m_NavSurfaceNodes.size();
#endif

	u32 n,e;
	for(n=0; n<m_NavSurfaceNodes.size(); n++)
	{
		int index = n;

		CNavGenNode * pNode = m_NavSurfaceNodes[index];	//[n];
		if(pNode->IsRemoved())
			continue;

		for(e=0; e<pNode->m_EdgesUsingThisNode.size(); e++)
		{
			CNavTriEdge * pEdge = pNode->m_EdgesUsingThisNode[e];
			if(pEdge->IsRemoved())
			{
				continue;
			}

			m_TriEdgesForCollapsing.push_back(pEdge);
			iNumAdded++;

			if(iNumAdded >= iMaxPerIteration)
			{
				iNumAdded = 0;

				DoOptimiseNavSurfaces();

				m_TriEdgesForCollapsing.clear();
			}
		}
	}

	DoOptimiseNavSurfaces();

	m_TriEdgesForCollapsing.clear();

#if __DEV
	size_t iSizeAfter = m_NavSurfaceNodes.size();
	Assert(iSizeBefore == iSizeAfter);
#endif

	return true;
}




bool
CNavGen::DoOptimiseNavSurfaces(void)
{
	//**********************************************************************
	// Iterate through our list of edges, repeatedly collapsing an edge.
	// Each time we collapse an edge we remove one vertex and two triangles,
	// but maintain connectivity.
	//**********************************************************************

	u32 v, t, e;
	float fLowestCost;
	int iLowestCostIndex;
	static const float fMaxErrorValue					= 100000;
	static const float fMinErrorValueToBeConsideredNone	= 0.001f;
	bool bQuit = false;

	int iNumIterations = m_EdgeCollapseNumIterations;

	while(!bQuit)
	{
		fLowestCost = fMaxErrorValue;
		iLowestCostIndex = -1;

		list<CNavTriEdge*>::iterator lowestCostIter;
		list<CNavTriEdge*>::iterator listIter;

		int iListIndex = 0;


		for(listIter = m_TriEdgesForCollapsing.begin(); listIter != m_TriEdgesForCollapsing.end(); listIter++)
		{
			if(g_bProcessInBackground) Sleep(0);

			CNavTriEdge * pEdge = *listIter;

			if(pEdge->IsRemoved())
			{
				//m_TriEdgesForCollapsing.erase(listIter,listIter);
				continue;
			}

			if(pEdge->m_pNode1->IsRemoved() || pEdge->m_pNode2->IsRemoved())
			{
				//m_TriEdgesForCollapsing.erase(listIter,listIter);
				continue;
			}

			if(pEdge->m_pTri1->IsRemoved() || pEdge->m_pTri2->IsRemoved())
			{
				//m_TriEdgesForCollapsing.erase(listIter,listIter);
				continue;
			}

			if(pEdge->m_iFlags & TRIEDGE_TOPOLOGYHASCHANGED)
			{
				// NB : We only need to re-evaluate the move cost when the local topology has changed!
				pEdge->m_fCostNode1ToNode2 = EvaluateVertexMove(pEdge->m_pNode1, pEdge->m_pNode2);
				pEdge->m_fCostNode2ToNode1 = EvaluateVertexMove(pEdge->m_pNode2, pEdge->m_pNode1);

				pEdge->m_iFlags &= ~TRIEDGE_TOPOLOGYHASCHANGED;
			}

			// Keep track of the lowest cost vertex-move so far
			if(pEdge->m_fCostNode1ToNode2 < fLowestCost)
			{
				fLowestCost = pEdge->m_fCostNode1ToNode2;
				iLowestCostIndex = iListIndex;
				lowestCostIter = listIter;
			}
			else if(pEdge->m_fCostNode2ToNode1 < fLowestCost)
			{
				fLowestCost = pEdge->m_fCostNode2ToNode1;
				iLowestCostIndex = iListIndex;
				lowestCostIter = listIter;
			}
			// If we've found a cost of zero (ie. moving the point in the plane) then quit now!
			if(Eq(pEdge->m_fCostNode1ToNode2, 0.0f, fMinErrorValueToBeConsideredNone) ||
				Eq(pEdge->m_fCostNode2ToNode1, 0.0f, fMinErrorValueToBeConsideredNone))
			{
				iLowestCostIndex = iListIndex;
				lowestCostIter = listIter;
				break;
			}

			iListIndex++;
		}

		// Collapse the lowest-costed vertex-move
		if(iLowestCostIndex != -1)
		{
			//CNavTriEdge * pEdge = m_TriEdgesForCollapsing[iLowestCostIndex];
			CNavTriEdge * pEdge = *lowestCostIter;
	
			m_iNumEdgesCollapsedThisPass++;

			CNavGenNode * pNodeToRemove = NULL;
			CNavGenNode * pNodeToMoveOnto = NULL;

			CNavSurfaceTri * pTriToRemove1 = pEdge->m_pTri1;
			CNavSurfaceTri * pTriToRemove2 = pEdge->m_pTri2;

			// We are going to move Node1 onto Node2, and remove Node1 in the process
			// Anything which refers to Node1, or to Tri1 or Tri2, must be updated
			if(pEdge->m_fCostNode1ToNode2 <= pEdge->m_fCostNode2ToNode1)
			{
				pNodeToRemove = pEdge->m_pNode1;
				pNodeToMoveOnto = pEdge->m_pNode2;

				#ifdef DEBUG_MESHOPT
				sprintf(tmpBuf, "Edge 0x%X to remove, cost = %.3f\n", ((u32)pEdge), pEdge->m_fCostNode1ToNode2);
				OutputDebugString(tmpBuf);
				#endif
			}
			else
			{
				pNodeToRemove = pEdge->m_pNode2;
				pNodeToMoveOnto = pEdge->m_pNode1;

				#ifdef DEBUG_MESHOPT
				sprintf(tmpBuf, "Edge 0x%X to remove, cost = %.3f\n", ((u32)pEdge), pEdge->m_fCostNode2ToNode1);
				OutputDebugString(tmpBuf);
				#endif
			}

			//************************************************************************************
			// As a failsafe, I am going to mark all the edges owned by all the vertices of the 2
			// triangle to move as changed in topology.
			//************************************************************************************
			u32 n;
			for(v=0; v<3; v++)
			{
				for(e=0; e<pTriToRemove1->m_Nodes[v]->m_EdgesUsingThisNode.size(); e++)
				{
					CNavTriEdge * pChangedEdge = pTriToRemove1->m_Nodes[v]->m_EdgesUsingThisNode[e];
					pChangedEdge->m_iFlags |= TRIEDGE_TOPOLOGYHASCHANGED;

					for(n=0; n<pChangedEdge->m_pNode1->m_EdgesUsingThisNode.size(); n++)
					{
						CNavTriEdge * pChangedEdge2 = pChangedEdge->m_pNode1->m_EdgesUsingThisNode[n];
						pChangedEdge2->m_iFlags |= TRIEDGE_TOPOLOGYHASCHANGED;
					}
					for(n=0; n<pChangedEdge->m_pNode2->m_EdgesUsingThisNode.size(); n++)
					{
						CNavTriEdge * pChangedEdge2 = pChangedEdge->m_pNode2->m_EdgesUsingThisNode[n];
						pChangedEdge2->m_iFlags |= TRIEDGE_TOPOLOGYHASCHANGED;
					}
				}
			}
			for(v=0; v<3; v++)
			{
				for(e=0; e<pTriToRemove2->m_Nodes[v]->m_EdgesUsingThisNode.size(); e++)
				{
					CNavTriEdge * pChangedEdge = pTriToRemove2->m_Nodes[v]->m_EdgesUsingThisNode[e];
					pChangedEdge->m_iFlags |= TRIEDGE_TOPOLOGYHASCHANGED;

					for(n=0; n<pChangedEdge->m_pNode1->m_EdgesUsingThisNode.size(); n++)
					{
						CNavTriEdge * pChangedEdge2 = pChangedEdge->m_pNode1->m_EdgesUsingThisNode[n];
						pChangedEdge2->m_iFlags |= TRIEDGE_TOPOLOGYHASCHANGED;
					}
					for(n=0; n<pChangedEdge->m_pNode2->m_EdgesUsingThisNode.size(); n++)
					{
						CNavTriEdge * pChangedEdge2 = pChangedEdge->m_pNode2->m_EdgesUsingThisNode[n];
						pChangedEdge2->m_iFlags |= TRIEDGE_TOPOLOGYHASCHANGED;
					}
				}
			}
			//*****************************************************************************
			//  Remove the edge to be deleted from the nodes' "m_EdgesUsingThisNode" lists.
			//	Delete instances of the collapsing edge in the pNodeToMoveOnto node.  These
			//	are now superseded by the identical edges in pNodeToRemove.
			//	Create a list of the surrounding edges which will be effected by the node
			//	move & will need processing to switch together the topology & recalculate
			//	edge-collpase costs.
			//*****************************************************************************

			vector<CNavTriEdge*> affectedEdges;
			affectedEdges.reserve(128);

			for(e=0; e<pNodeToMoveOnto->m_EdgesUsingThisNode.size(); e++)
			{
				CNavTriEdge * pOtherEdge = pNodeToMoveOnto->m_EdgesUsingThisNode[e];
				if(pOtherEdge->IsRemoved())
					continue;

				// Remove the edge from the surrounding-edges list
				if(pOtherEdge == pEdge)
				{
					pNodeToMoveOnto->m_EdgesUsingThisNode.erase(pNodeToMoveOnto->m_EdgesUsingThisNode.begin() + e);
					e--;
					continue;
				}
				// If this was not the edge itself - but was either of the edges which bordered the
				// two tris to be removed, then remove this edge - it is now duplicated in other node
				if(pOtherEdge->m_pTri1 == pTriToRemove1 || pOtherEdge->m_pTri2 == pTriToRemove1 ||
					pOtherEdge->m_pTri1 == pTriToRemove2 || pOtherEdge->m_pTri2 == pTriToRemove2)
				{
					CNavGenNode * pOtherNode = (pOtherEdge->m_pNode1 == pNodeToMoveOnto) ? pOtherEdge->m_pNode2 : pOtherEdge->m_pNode1;
					RemoveEdgeFromEdgeList(pOtherNode->m_EdgesUsingThisNode, pOtherEdge);

					pNodeToMoveOnto->m_EdgesUsingThisNode.erase(pNodeToMoveOnto->m_EdgesUsingThisNode.begin() + e);

					pOtherEdge->SetRemoved();
					e--;
					continue;
				}

				affectedEdges.push_back(pOtherEdge);
			}
			for(e=0; e<pNodeToRemove->m_EdgesUsingThisNode.size(); e++)
			{
				CNavTriEdge * pOtherEdge = pNodeToRemove->m_EdgesUsingThisNode[e];
				if(pOtherEdge->IsRemoved())
					continue;

				// Remove the edge from the surrounding-edges list
				if(pOtherEdge == pEdge)
				{
					pNodeToRemove->m_EdgesUsingThisNode.erase(pNodeToRemove->m_EdgesUsingThisNode.begin() + e);
					e--;
					continue;
				}
				affectedEdges.push_back(pOtherEdge);

				// Also add these edges to the node that this node is moving onto
				pNodeToMoveOnto->m_EdgesUsingThisNode.push_back(pOtherEdge);
			}

			//********************************************
			//	Do the edges
			//********************************************

			m_fAverageNumTrianglesAroundNode = 0.0f;


			// Set all edges which used node 1, to now use node 2
			for(e=0; e<affectedEdges.size(); e++)
			{
				CNavTriEdge * pOtherEdge = affectedEdges[e];

				pOtherEdge->m_iFlags &= ~TRIEDGE_NOTYETBEENMOVEDATALL;	// this will have been moved now..

				// Update nodes ptrs in edges1 
				if(pOtherEdge->m_pNode1 == pNodeToRemove)
				{
					// This adjoins the moving edge agross the gap, and also adjoinds the faces on either side of the gap
					MaybeStitchUpEdgeAcrossGap(pEdge, pOtherEdge, pNodeToRemove);
					pOtherEdge->m_pNode1 = pNodeToMoveOnto;

					// This edge will need to have its collapse value recalculated
					pOtherEdge->m_iFlags |= TRIEDGE_TOPOLOGYHASCHANGED;
				}
				else if(pOtherEdge->m_pNode2 == pNodeToRemove)
				{
					// This adjoins the moving edge agross the gap, and also adjoinds the faces on either side of the gap
					MaybeStitchUpEdgeAcrossGap(pEdge, pOtherEdge, pNodeToRemove);
					pOtherEdge->m_pNode2 = pNodeToMoveOnto;

					// This edge will need to have its collapse value recalculated
					pOtherEdge->m_iFlags |= TRIEDGE_TOPOLOGYHASCHANGED;
				}

				// edges using the "pNodeToMoveOnto" node will also need recalculating..
				else if(pOtherEdge->m_pNode1 == pNodeToMoveOnto || pOtherEdge->m_pNode2 == pNodeToMoveOnto)
				{
					// This edge will need to have its collapse value recalculated
					pOtherEdge->m_iFlags |= TRIEDGE_TOPOLOGYHASCHANGED;
				}

				// If this edge is now invalid (only one face on any side of it) then it will have
				// been flagged above in MaybeStitchUpEdge()..  We should make sure it gets removed
				// from the "pNodeToMoveOnto" edge list to which it was previoudly added
				if(pOtherEdge->IsRemoved())
				{
					RemoveEdgeFromEdgeList(pNodeToMoveOnto->m_EdgesUsingThisNode, pOtherEdge);
				}

				else if(pOtherEdge->m_iFlags & TRIEDGE_TOPOLOGYHASCHANGED)
				{
					// keep running average of edge lengths
					m_fAverageEdgeLength += (pOtherEdge->m_pNode1->m_vBasePos - pOtherEdge->m_pNode2->m_vBasePos).Mag();
					m_fAverageEdgeLength /= 2.0f;
				}
			}

			// This removes references to the removed triangles from the other 2 nodes in the triangle
			RemoveReferenceToTrisFromNodesOppositeEdgeToCollapse(pEdge);

			// Set all surrounding tri's using node 1, to now use node 2
			// and add the list of tris from node 1 into node 2's list
			for(t=0; t<pNodeToRemove->m_TrianglesUsingThisNode.size(); t++)
			{
				CNavSurfaceTri * pTri = pNodeToRemove->m_TrianglesUsingThisNode[t];
				if(pTri->IsRemoved())
					continue;

				// Remove the tri from the node's list, if this is one of the tris being removed
				if(pTri == pTriToRemove1 || pTri == pTriToRemove2)
				{
					pNodeToRemove->m_TrianglesUsingThisNode.erase(pNodeToRemove->m_TrianglesUsingThisNode.begin()+t);
					t--;
					continue;
				}

				pTri->m_iFlags |= NAVSURFTRI_MODIFIEDBYOPTIMISATION;

				if(pTri->m_Nodes[0] == pNodeToRemove)
					pTri->m_Nodes[0] = pNodeToMoveOnto;
				else if(pTri->m_Nodes[1] == pNodeToRemove)
					pTri->m_Nodes[1] = pNodeToMoveOnto;
				else if(pTri->m_Nodes[2] == pNodeToRemove)
					pTri->m_Nodes[2] = pNodeToMoveOnto;
			}

			// Now go through node 2's surrounding triangles, and erase the removed faces from there
			RemoveTriFromTriList(pNodeToMoveOnto->m_TrianglesUsingThisNode, pTriToRemove1);
			RemoveTriFromTriList(pNodeToMoveOnto->m_TrianglesUsingThisNode, pTriToRemove2);

			// Add node1's surrounding tris, to node2's surrounding tris list
			for(t=0; t<pNodeToRemove->m_TrianglesUsingThisNode.size(); t++)
			{
				CNavSurfaceTri * pTri = pNodeToRemove->m_TrianglesUsingThisNode[t];
				if(pTri->IsRemoved())
					continue;

				pNodeToMoveOnto->m_TrianglesUsingThisNode.push_back(pTri);
			}

			// This node gets removed
			pNodeToRemove->SetRemoved();

			// These two triangles get removed
			pTriToRemove1->SetRemoved();
			pTriToRemove2->SetRemoved();

			// Try to identify & fix any 'flaps' which may have occured in the mesh.
			// This is where two triangles become adjacent across two faces.
			FixFlaps(pNodeToRemove);
			FixFlaps(pNodeToMoveOnto);

			// Every time an edge-collapse occurs, go through all faces which use the node
			// which has moved and mark all nodes which are used by all these faces as
			// changed - so that the cost value must be re-evaluated for them

			// Delete the edge
			pEdge->SetRemoved();

			m_TriEdgesForCollapsing.erase(lowestCostIter);

			iNumIterations--;
			if(iNumIterations== 0)
				bQuit = true;
		}
		else
		{
			bQuit = true;

			list<CNavTriEdge*>::iterator listIter2;
			for(listIter2 = m_TriEdgesForCollapsing.begin(); listIter2 != m_TriEdgesForCollapsing.end(); listIter2++)
			{
				CNavTriEdge * pEdge = *listIter2;
				pEdge->m_iFlags |= TRIEDGE_TOPOLOGYHASCHANGED;
			}

#ifdef DEBUG_MESHOPT

			sprintf(tmpBuf, "Couldn't find any suitable edge to collapse.\n");
			OutputDebugString(tmpBuf);

			for(e=0; e<m_TriEdgesForCollapsing.size(); e++)
			{
				// Manually force TOPOLOGYHASCHANGED flag :) ?
				CNavTriEdge * pEdge = m_TriEdgesForCollapsing[e];

				if(pEdge->IsRemoved())
					continue;
				
				// Why hasn't this edge been touched at all yet??
				if(pEdge->m_iFlags & TRIEDGE_NOTYETBEENMOVEDATALL &&
					(pEdge->m_fCostNode1ToNode2 < fMaxErrorValue || pEdge->m_fCostNode2ToNode1 < fMaxErrorValue))
				{
					bool pPause = true;
					sprintf(tmpBuf, "Edge 0x%X hasn't been touched yet at all.\n", ((u32)pEdge));
					OutputDebugString(tmpBuf);
				}
			}
#endif

		}
	}

	return true;
}


// sometimes collapses cause 'flaps' in the mesh, where two triangles become
// adjoined to each other along TWO edges.  In this case we have to remove
// both triangles, and connect up the adjacencies over the gap

void
CNavGen::FixFlaps(CNavGenNode * pNode)
{
	u32 t;
	for(t=0; t<pNode->m_TrianglesUsingThisNode.size(); t++)
	{
		CNavSurfaceTri * pTri = pNode->m_TrianglesUsingThisNode[t];

		if(pTri->IsRemoved())
			continue;

		CNavSurfaceTri * pFlapTri = NULL;
		CNavSurfaceTri * pAdjTri1 = NULL;

		if(pTri->m_AdjacentTris[0] == pTri->m_AdjacentTris[1])
		{
			pFlapTri = pTri->m_AdjacentTris[0];
			pAdjTri1 = pTri->m_AdjacentTris[2];
		}
		else if(pTri->m_AdjacentTris[1] == pTri->m_AdjacentTris[2])
		{
			pFlapTri = pTri->m_AdjacentTris[1];
			pAdjTri1 = pTri->m_AdjacentTris[0];
		}
		else if(pTri->m_AdjacentTris[2] == pTri->m_AdjacentTris[0])
		{
			pFlapTri = pTri->m_AdjacentTris[2];
			pAdjTri1 = pTri->m_AdjacentTris[1];
		}

		// Okay, looks like we may have a 'flap'!
		if(pFlapTri)
		{
			CNavSurfaceTri * pAdjTri2 = NULL;
			CNavSurfaceTri * pLinkBackFlapTri = NULL;

			if(pFlapTri->m_AdjacentTris[0] == pFlapTri->m_AdjacentTris[1])
			{
				pAdjTri2 = pFlapTri->m_AdjacentTris[2];
				pLinkBackFlapTri = pFlapTri->m_AdjacentTris[0];
			}
			else if(pFlapTri->m_AdjacentTris[1] == pFlapTri->m_AdjacentTris[2])
			{
				pAdjTri2 = pFlapTri->m_AdjacentTris[0];
				pLinkBackFlapTri = pFlapTri->m_AdjacentTris[1];
			}
			else if(pFlapTri->m_AdjacentTris[2] == pFlapTri->m_AdjacentTris[0])
			{
				pAdjTri2 = pFlapTri->m_AdjacentTris[1];
				pLinkBackFlapTri = pFlapTri->m_AdjacentTris[2];
			}
			else
			{
#ifdef NAVGEN_SPEW_DEBUG
				printf("Warning : found a flap tri, without an adjacent flap tri.\n");
#endif
				continue;
			}
			if(pLinkBackFlapTri != pTri)
			{
#ifdef NAVGEN_SPEW_DEBUG
				printf("Warning : found a flap tri - but it doesn't link back to original.\n");
#endif
				continue;
			}

			// Right - we've gotta remove the two flap tris, and link the good tris adjacent to
			// them across the gap!

			pTri->SetRemoved();
			pFlapTri->SetRemoved();

			if(pAdjTri1->m_AdjacentTris[0] == pTri)
				pAdjTri1->m_AdjacentTris[0] = pAdjTri2;
			else if(pAdjTri1->m_AdjacentTris[1] == pTri)
				pAdjTri1->m_AdjacentTris[1] = pAdjTri2;
			else if(pAdjTri1->m_AdjacentTris[2] == pTri)
				pAdjTri1->m_AdjacentTris[2] = pAdjTri2;

			if(pAdjTri2->m_AdjacentTris[0] == pFlapTri)
				pAdjTri2->m_AdjacentTris[0] = pAdjTri1;
			else if(pAdjTri2->m_AdjacentTris[1] == pFlapTri)
				pAdjTri2->m_AdjacentTris[1] = pAdjTri1;
			else if(pAdjTri2->m_AdjacentTris[2] == pFlapTri)
				pAdjTri2->m_AdjacentTris[2] = pAdjTri1;

			// Now remove the two flap tris from all the nodes that may reference them
			for(int n=0; n<3; n++)
			{
				RemoveTriFromTriList(pTri->m_Nodes[n]->m_TrianglesUsingThisNode, pTri);
				RemoveTriFromTriList(pFlapTri->m_Nodes[n]->m_TrianglesUsingThisNode, pFlapTri);

				// Now modify the edge which was between them, to now be between
				// the two adjacent tris..
				for(u32 e=0; e<pTri->m_Nodes[n]->m_EdgesUsingThisNode.size(); e++)
				{
					CNavTriEdge * pEdge = pTri->m_Nodes[n]->m_EdgesUsingThisNode[e];
					if(pEdge->IsRemoved())
						continue;

					if(pEdge->m_pTri1 == pTri)
					{
						pEdge->m_pTri1 = pAdjTri1;
						pEdge->m_iFlags |= TRIEDGE_TOPOLOGYHASCHANGED;
					}
					else if(pEdge->m_pTri1 == pFlapTri)
					{
						pEdge->m_pTri1 = pAdjTri2;
						pEdge->m_iFlags |= TRIEDGE_TOPOLOGYHASCHANGED;
					}
					if(pEdge->m_pTri2 == pTri)
					{
						pEdge->m_pTri2 = pAdjTri1;
						pEdge->m_iFlags |= TRIEDGE_TOPOLOGYHASCHANGED;
					}
					else if(pEdge->m_pTri2 == pFlapTri)
					{
						pEdge->m_pTri2 = pAdjTri2;
						pEdge->m_iFlags |= TRIEDGE_TOPOLOGYHASCHANGED;
					}
				}
			}

		}
	}

}


void
CNavGen::RemoveEdgeTris(void)
{
	if(m_Octree.RootNode())
	{
		//********************************************************************
		// Do the first pass, which is to remove all tris with < 3 adjacency
		//********************************************************************
		MarkEdgeTris(true);
		ProcessRemoveEdgeTris();

#ifdef NAVGEN_SPEW_DEBUG
		sprintf(tmpBuf, "Remove Edge Tris (pass 1), num triangles = (%i / %i).\n", m_iOptimisedTriangleCount, m_iInitialPassNumTrianglesCreated);
		OutputDebugString(tmpBuf);
#endif
		//********************************************************************
		// Do the second pass, which is to remove all tris with < 2 adjacency
		// and which have not been optimised during edge-collapse
		//********************************************************************

		MarkEdgeTris(false);
		ProcessRemoveEdgeTris();

#ifdef NAVGEN_SPEW_DEBUG
		sprintf(tmpBuf, "Remove Edge Tris (pass 2), num triangles = (%i / %i).\n", m_iOptimisedTriangleCount, m_iInitialPassNumTrianglesCreated);
		OutputDebugString(tmpBuf);
#endif
	}
}




// In the first pass we mark ANY triangle which has less than 3 adjacent tris
// In the second pass we mark only those which have 1 or zero adjacent triangles,
// and which have not been a part of the edge-collapse process (that is a 'given' btw).
//
// NB : This function is a bit misleading - it is not dealing with triangles physically on
// the edge of the navmesh, but rather triangles which have not been optimised due to being
// on the outside boundary of a the triangulated area.
// We may also need somewhere to mark triangles which are physically on the navmesh edges..

void
CNavGen::MarkEdgeTris(bool bFirstPass)
{
	u32 t;
	int numAdj;

	for(t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];
		if(pTri->IsRemoved())
			continue;

		numAdj = NumAdjacentTris(pTri);

		if(bFirstPass)
		{
			if(numAdj == 3)
				continue;

			pTri->m_iFlags |= NAVSURFTRI_EXTERIOR;
		}
		else
		{
			if(numAdj > 1)
				continue;

			if(!(pTri->m_iFlags & NAVSURFTRI_MODIFIEDBYOPTIMISATION))
			{
				pTri->m_iFlags |= NAVSURFTRI_EXTERIOR;
			}
		}
	}
}


void
CNavGen::ProcessRemoveEdgeTris(void)
{
	u32 t, e, f, n;
	for(t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];

		if(pTri->IsRemoved())
			continue;

		if(!(pTri->m_iFlags & NAVSURFTRI_EXTERIOR))
			continue;

		// Flag this tri as removed
		pTri->SetRemoved();
		m_iOptimisedTriangleCount--;

		// Visit all the nodes which are the vertices of this tri
		for(n=0; n<3; n++)
		{
			CNavGenNode * pNode = pTri->m_Nodes[n];
			if(pNode->IsRemoved())
				continue;

			// Remove it from the list of surrounding faces
			for(f=0; f<pNode->m_TrianglesUsingThisNode.size(); f++)
			{
				CNavSurfaceTri * pSurTri = pNode->m_TrianglesUsingThisNode[f];
				if(pSurTri->IsRemoved())
					continue;

				if(pSurTri == pTri)
				{
					pNode->m_TrianglesUsingThisNode.erase(pNode->m_TrianglesUsingThisNode.begin() + f);
					f--;
				}
			}

			// Remove all edges which reference it
			for(e=0; e<pNode->m_EdgesUsingThisNode.size(); e++)
			{
				CNavTriEdge * pEdge = pNode->m_EdgesUsingThisNode[e];
				if(pEdge->IsRemoved())
					continue;

				if(pEdge->m_pTri1 == pTri || pEdge->m_pTri2 == pTri)
				{
					pNode->m_EdgesUsingThisNode.erase(pNode->m_EdgesUsingThisNode.begin() + e);
					e--;

					pEdge->SetRemoved();
				}
			}
		}

		// Remove adjacency link-back from adjacent tris
		for(f=0; f<3; f++)
		{
			CNavSurfaceTri * pAdjTri = pTri->m_AdjacentTris[f];
			if(!pAdjTri)
				continue;

			if(pAdjTri->IsRemoved())
				continue;

			for(e=0; e<3; e++)
			{
				if(pAdjTri->m_AdjacentTris[e] == pTri)
					pAdjTri->m_AdjacentTris[e] = NULL;
			}
		}
	
	}

	return;
}




//***********************************************************************************
//
//	After doing the edge-collapse optimisation, and then calling Mark/RemoveEdgeTris
//	we can then go through and merge the may triangle-fans which will be around the
//	edges of all the walkable areas.  This will have a major impact on the polycount!
//
//***********************************************************************************

bool
CNavGen::OptimiseEdges(void)
{
	if(!m_Octree.RootNode())
	{
		return false;
	}

	bool bQuit = false;
	int iNumTimesDidZero = 0;
	int iNumTrisRemoved = 0;

	while(!bQuit)
	{
		m_iNumEdgesCollapsedThisPass = 0;

		OptimiseEdges_OneIteration();

		if(m_iNumEdgesCollapsedThisPass)
		{
			iNumTimesDidZero = 0;
			iNumTrisRemoved += m_iNumEdgesCollapsedThisPass;
			m_iNumEdgesCollapsedThisPass = 0;
		}
		else
		{
			iNumTimesDidZero++;
		}
		if(iNumTimesDidZero==2)
		{
			bQuit = true;
		}
	}

	m_iOptimisedTriangleCount -= iNumTrisRemoved;
	m_iOptimisedNodeCount -= iNumTrisRemoved;

#ifdef NAVGEN_SPEW_DEBUG
	sprintf(tmpBuf, "%i tris removed, num nodes = (%i / %i), num triangles = (%i / %i).\n", iNumTrisRemoved, m_iOptimisedNodeCount, m_iInitialPassNumNodesCreated, m_iOptimisedTriangleCount, m_iInitialPassNumTrianglesCreated);
	OutputDebugString(tmpBuf);
#endif

	return true;

}

bool
CNavGen::OptimiseEdges_OneIteration(void)
{
	u32 n, t, e;
	int numAdj, a, emptyEdge, otherEmptyEdge, a2, n2;
	int sharedEdge, neighbourEdge;
	int otherSharedEdge, otherNeighbourEdge;

	Vector3 vTriEdge, vOtherTriEdge;
	CNavGenNode * pSharedNode1 = NULL;
	CNavGenNode * pSharedNode2 = NULL;
	CNavTriEdge * pEdgeToRemove = NULL;
	float dot;

	for(n=0; n<m_NavSurfaceNodes.size(); n++)
	{
		if(g_bProcessInBackground) Sleep(0);

		CNavGenNode * pNode = m_NavSurfaceNodes[n];

		if(pNode->IsRemoved())
			continue;

		if(pNode->m_EdgesUsingThisNode.size() != 1)
			continue;

		if(pNode->m_TrianglesUsingThisNode.size() != 2)
			continue;

		if(m_bDoWeCareAboutSurfaceTypes)
		{
			CNavSurfaceTri * pTri1 = pNode->m_TrianglesUsingThisNode[0];
			CNavSurfaceTri * pTri2 = pNode->m_TrianglesUsingThisNode[1];

			if(pTri1->m_colPolyData.GetIsPavement() != pTri2->m_colPolyData.GetIsPavement())
				continue;

			if(pTri1->m_iBakedInBitField != pTri2->m_iBakedInBitField)
				continue;

			if(pTri1->m_bDoNotOptimise || pTri2->m_bDoNotOptimise)
				continue;
		}

		if(pNode->m_iFlags & NAVGENNODE_ON_EDGE_OF_MESH)
			continue;

		for(t=0; t<pNode->m_TrianglesUsingThisNode.size(); t++)
		{
			CNavSurfaceTri * pTri = pNode->m_TrianglesUsingThisNode[t];
			if(pTri->IsRemoved())
				continue;

			numAdj = NumAdjacentTris(pTri);

			// This triangle has an adjacency of two, so lets try to
			// merge it with one (or both) of its adjacent triangles
			if(numAdj == 2)
			{
				// Find which edge is the one which has no adjacent triangle
				emptyEdge = -1;
				for(a=0; a<3; a++)
				{
					CNavSurfaceTri * pAdjTri = pTri->m_AdjacentTris[a];
					if(!pAdjTri)
						emptyEdge = a;
				}
				if(emptyEdge == -1)
					continue;

				// Get the edge vector & normalise
				CNavGenNode * pTriEdgeNode1 = pTri->m_Nodes[(emptyEdge+1)%3];
				CNavGenNode * pTriEdgeNode2 = pTri->m_Nodes[emptyEdge];

				vTriEdge = pTri->m_Nodes[(emptyEdge+1)%3]->m_vBasePos - pTri->m_Nodes[emptyEdge]->m_vBasePos;
				vTriEdge.Normalize();

				// Now find which of this triangle's two adjacent tris also has an adjacency of 2
				for(a=0; a<3; a++)
				{
					if(a == emptyEdge)
						continue;

					sharedEdge = a;

					CNavSurfaceTri * pAdjTri = pTri->m_AdjacentTris[a];

					if(NumAdjacentTris(pAdjTri)!=2)
						continue;

					// We've found an adjacent tri which also has an adjacency of 2.
					// Find which edge on the adjacent tri has no adjacent triangle

					otherEmptyEdge = -1;		// The edge index on "pAdjTri" which adjoins to nothing
					otherSharedEdge = -1;		// The edge index on "pAdjTri" which adjoins "pTri"
					otherNeighbourEdge = -1;	// The edge index on "pAdjTri" which adjoins to another triangle..

					for(a2=0; a2<3; a2++)
					{
						CNavSurfaceTri * pAdjTri2 = pAdjTri->m_AdjacentTris[a2];
						if(!pAdjTri2)
							otherEmptyEdge = a2;
						else if(pAdjTri2==pTri)
							otherSharedEdge = a2;
						else
							otherNeighbourEdge = a2;
					}
					// Make sure this data is correct
					if(otherEmptyEdge == -1 || otherSharedEdge == -1 || otherNeighbourEdge == -1)
						continue;

					// Now find which edge for "pTri"
					if((emptyEdge == 0 && a == 1) || (emptyEdge == 1 && a == 0))
						neighbourEdge = 2;
					else if((emptyEdge == 1 && a == 2) || (emptyEdge == 2 && a == 1))
						neighbourEdge = 0;
					else if((emptyEdge == 2 && a == 0) || (emptyEdge == 0 && a == 2))
						neighbourEdge = 1;
					else
						continue;

					CNavGenNode * pAdjTriEdgeNode1 = pAdjTri->m_Nodes[(otherEmptyEdge+1)%3];
					CNavGenNode * pAdjTriEdgeNode2 = pAdjTri->m_Nodes[otherEmptyEdge];

					// We have to make sure that this empty edge consists of one of the other nodes from the other pTri's edge
					// and that it therefore forms a continuous edge which we can test for being in a stright line with the other edge
					if(pTriEdgeNode1 == pAdjTriEdgeNode1 || pTriEdgeNode1 == pAdjTriEdgeNode2 ||
						pTriEdgeNode2 == pAdjTriEdgeNode1 || pTriEdgeNode2 == pAdjTriEdgeNode2)
					{

					}
					else
					{
						continue;
					}

					// Get the edge vector of the adjacent tri's empty edge, and normalise
					vOtherTriEdge = pAdjTri->m_Nodes[(otherEmptyEdge+1)%3]->m_vBasePos - pAdjTri->m_Nodes[otherEmptyEdge]->m_vBasePos;
					vOtherTriEdge.Normalize();

					// Now lets make sure that these two 'empty' edges are exactly aligned..
					dot = DotProduct(vTriEdge, vOtherTriEdge);
					dot = Abs(dot);
					if(dot < 0.999f)
					{
						continue;
					}

					// We can now try to merge these two tris.  We'll keep pTri, and discard pAdjTri.
					// Lets go through and find the two shared nodes along the shared edge.
					int v1,v2;
					for(v1=0; v1<3; v1++)
					{
						for(v2=0; v2<3; v2++)
						{
							if(pTri->m_Nodes[v1] == pAdjTri->m_Nodes[v2])
							{
								if(!pSharedNode1)
									pSharedNode1 = pTri->m_Nodes[v1];
								else
									pSharedNode2 = pTri->m_Nodes[v1];
							}
						}
					}

					// Something's wrong - these two tris should definately share two of their nodes..
					if(!pSharedNode1 || !pSharedNode2)
						continue;

					// "pNodeToChangeTo" is the node which "pTri" will be made to use, in order to implement the merge
					CNavGenNode * pNodeToChangeTo = NULL;
					CNavGenNode * pNodeToRemove = NULL;

					// Find out which of these nodes forming the continuous edge is the one that's in the middle
					// (ie. shared by both tris).  This is that one that we'll be removing.
					if(pTriEdgeNode1 == pAdjTriEdgeNode1)
						pNodeToRemove = pTriEdgeNode1;
					else if(pTriEdgeNode1 == pAdjTriEdgeNode2)
						pNodeToRemove = pTriEdgeNode1;
					else if(pTriEdgeNode2 == pAdjTriEdgeNode1)
						pNodeToRemove = pTriEdgeNode2;
					else if(pTriEdgeNode2 == pAdjTriEdgeNode2)
						pNodeToRemove = pTriEdgeNode2;

					Assert(pNodeToRemove);


					for(v1=0; v1<3; v1++)
					{
						// Find the node in the adjacent tri which ISN'T either of the shared nodes.
						if(pAdjTri->m_Nodes[v1] != pSharedNode1 && pAdjTri->m_Nodes[v1] != pSharedNode2)
						{
							pNodeToChangeTo = pAdjTri->m_Nodes[v1];
						}
					}
					// Make sure we got it - if not, then the mesh's data structure has become fucked
					if(!pNodeToChangeTo)
						continue;

					// remove the edge around this node, which defines the edge between the two faces being merged
					// Remove the other tri from all the nodes it is in

					// New bit (to account for the fact that we may have multiple edges
					// flagged as REMOVED, but not yet deleted)
					for(u32 ed=0; ed<pNodeToRemove->m_EdgesUsingThisNode.size(); ed++)
					{
						pEdgeToRemove = pNodeToRemove->m_EdgesUsingThisNode[ed];

						RemoveEdgeFromEdgeList(pEdgeToRemove->m_pNode1->m_EdgesUsingThisNode, pEdgeToRemove);
						RemoveEdgeFromEdgeList(pEdgeToRemove->m_pNode2->m_EdgesUsingThisNode, pEdgeToRemove);

						if(pEdgeToRemove->IsRemoved())
						{
							continue;
						}

						pEdgeToRemove->SetRemoved();

					}

					// Remove the other tri from all the nodes it is in
					for(n2=0; n2<3; n2++)
					{
						CNavGenNode * pTriNode = pAdjTri->m_Nodes[n2];
						if(pTriNode->IsRemoved())
							continue;

						RemoveTriFromTriList(pTriNode->m_TrianglesUsingThisNode, pAdjTri);
					}

					// Set the triangle adjacent to pAdjTri, to now be adjacent to pTri
					for(a2=0; a2<3; a2++)
					{
						if(pAdjTri->m_AdjacentTris[otherNeighbourEdge]->m_AdjacentTris[a2] == pAdjTri)
						{
							pAdjTri->m_AdjacentTris[otherNeighbourEdge]->m_AdjacentTris[a2] = pTri;
							break;
						}
					}

					// Set pTri's adjacency to now be adjacent to the tri which is adjacent to pAdjTri
					pTri->m_AdjacentTris[sharedEdge] = pAdjTri->m_AdjacentTris[otherNeighbourEdge];

					// Update the edge which was beween pAdjTri & it's adjacent tri (not pTri) to now
					// reference pTri instead of pAdjTri. Also change the edge's node pointer to the new node
					CNavTriEdge * pSharedEdge = NULL;
					for(n2=0; n2<3; n2++)
					{
						CNavGenNode * pTriNode = pAdjTri->m_Nodes[n2];

						if(pTriNode->IsRemoved())
							continue;

						for(e=0; e<pTriNode->m_EdgesUsingThisNode.size(); e++)
						{
							CNavTriEdge * pEdge = pTriNode->m_EdgesUsingThisNode[e];
							if(pEdge->IsRemoved())
								continue;

							// We'll just mark all edges around this node as having changed..
							pEdge->m_iFlags |= TRIEDGE_TOPOLOGYHASCHANGED;

							if(pEdge->m_pTri1 == pAdjTri && pEdge->m_pTri2 == pAdjTri->m_AdjacentTris[otherNeighbourEdge])
							{
								pSharedEdge = pEdge;
								pEdge->m_pTri1 = pTri;
								break;
							}
							else if(pEdge->m_pTri1 == pAdjTri->m_AdjacentTris[otherNeighbourEdge] && pEdge->m_pTri2 == pAdjTri)
							{
								pSharedEdge = pEdge;
								pEdge->m_pTri2 = pTri;
								break;
							}
						}
					}

					// Get the node in pAdjTri which is not one of our shared nodes.
					// This is the node which pTri will now become a surrounding tri of
					CNavGenNode * pAdjTriOtherNode = NULL;

					if((pAdjTri->m_Nodes[0] == pSharedNode1 && pAdjTri->m_Nodes[1] == pSharedNode2) ||
						(pAdjTri->m_Nodes[0] == pSharedNode2 && pAdjTri->m_Nodes[1] == pSharedNode1))
						pAdjTriOtherNode = pAdjTri->m_Nodes[2];
					else if((pAdjTri->m_Nodes[1] == pSharedNode1 && pAdjTri->m_Nodes[2] == pSharedNode2) ||
						(pAdjTri->m_Nodes[1] == pSharedNode2 && pAdjTri->m_Nodes[2] == pSharedNode1))
						pAdjTriOtherNode = pAdjTri->m_Nodes[0];
					else if((pAdjTri->m_Nodes[2] == pSharedNode1 && pAdjTri->m_Nodes[0] == pSharedNode2) ||
						(pAdjTri->m_Nodes[2] == pSharedNode2 && pAdjTri->m_Nodes[0] == pSharedNode1))
						pAdjTriOtherNode = pAdjTri->m_Nodes[1];
					else
					{
						Assert(0);
						continue;
					}

					// add pTri to surrounding tri's list
					pAdjTriOtherNode->m_TrianglesUsingThisNode.push_back(pTri);


					u32 f;
					for(f=0; f<pNodeToRemove->m_TrianglesUsingThisNode.size(); f++)
					{
						CNavSurfaceTri * pSurrTri = pNodeToRemove->m_TrianglesUsingThisNode[f];
						if(pSurrTri == pTri || pSurrTri == pAdjTri)
						{
							for(int n3=0; n3<3; n3++)
							{
								if(pSurrTri->m_Nodes[n3] == pNodeToRemove)
									pSurrTri->m_Nodes[n3] = pNodeToChangeTo;
							}
						}
					}
					for(e=0; e<pNodeToRemove->m_EdgesUsingThisNode.size(); e++)
					{
						CNavTriEdge * pMaybeAffectedEdge = pNodeToRemove->m_EdgesUsingThisNode[e];
						pMaybeAffectedEdge->m_iFlags |= TRIEDGE_TOPOLOGYHASCHANGED;
					}
					for(e=0; e<pNodeToChangeTo->m_EdgesUsingThisNode.size(); e++)
					{
						CNavTriEdge * pMaybeAffectedEdge = pNodeToChangeTo->m_EdgesUsingThisNode[e];
						pMaybeAffectedEdge->m_iFlags |= TRIEDGE_TOPOLOGYHASCHANGED;
					}


					RemoveTriFromTriList(pNodeToRemove->m_TrianglesUsingThisNode, pTri);
					RemoveTriFromTriList(pNodeToRemove->m_TrianglesUsingThisNode, pAdjTri);

					if(!pNodeToRemove->m_TrianglesUsingThisNode.size())
					{
						pNodeToRemove->SetRemoved();
					}

					pAdjTri->SetRemoved();

					// We've merged a tri, so don't want to do any more.  We'll do other adjoing ones in the next pass.
					m_iNumEdgesCollapsedThisPass++;

					return false;
				}
			}
		}
	}

	return true;
}


void
CNavGen::SetAllTrianglesAsAccessible(void)
{
	for(u32 t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];

		if(pTri->IsRemoved())
		{
			pTri->m_iFlags &= ~NAVSURFTRI_ACCESSIBLE;
		}
		else
		{
			pTri->m_iFlags |= NAVSURFTRI_ACCESSIBLE;
		}
	}
}


//***********************************************************************
// Removes all nodes and triangles which are not marked as accessible.
//***********************************************************************
void
CNavGen::PruneInaccessibleNodesAndTris(void)
{
	for(u32 t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];

		if(pTri->IsRemoved())
			continue;

		// Not accessible ?
		if(!(pTri->m_iFlags & NAVSURFTRI_ACCESSIBLE))
		{
			pTri->SetRemoved();

			for(int v=0; v<3; v++)
			{
				CNavGenNode * pNode = pTri->m_Nodes[v];

				if(pNode->IsRemoved())
					continue;

				// Remove from node's triangles list
				RemoveTriFromTriList(pNode->m_TrianglesUsingThisNode, pTri);

				// Remove all edges which use this node
				for(u32 e=0; e<pNode->m_EdgesUsingThisNode.size(); e++)
				{
					CNavTriEdge * pEdge = pNode->m_EdgesUsingThisNode[e];
					if(pEdge->IsRemoved())
						continue;

					if(pEdge->m_pTri1 == pTri || pEdge->m_pTri2 == pTri)
					{
						RemoveEdgeFromEdgeList(pNode->m_EdgesUsingThisNode, pEdge);

						pEdge->SetRemoved();

						CNavGenNode * pOtherNodeOnEdge = (pEdge->m_pNode1 == pNode) ? pEdge->m_pNode2 : pEdge->m_pNode1;
						RemoveEdgeFromEdgeList(pOtherNodeOnEdge->m_EdgesUsingThisNode, pEdge);
					}
				}

				// Can we remove this node also ?
				if(pNode->m_TrianglesUsingThisNode.size() == 0)
				{
					pNode->SetRemoved();
				}
			}
		}
	}
}

// Name :	RemoveUnimportantTriangles
// Desc :	If after optimization, a triangle has only one neighbout and is still little larger than
//			when it was first created, is is probably pretty useless to the pathfinding.  We can safely
//			remove it and save some memory in the navmesh.

void
CNavGen::RemoveUnimportantNodesAndTriangles(void)
{
	m_iNumSmallTrianglesRemoved = 0;

	float fInitialDist = m_Params.m_fDistanceToInitiallySeedNodes + 0.1f;
	float fSmallArea = (fInitialDist * fInitialDist) / 2.0f;
	
	u32 n, t;

	for(t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];

		if(pTri->IsRemoved())
			continue;

		// Don't remove polys on the edge of the mesh
		if(pTri->m_iFlags & NAVSURFTRI_EXTERIOR)
			continue;

		if(NumAdjacentTris(pTri) <= 1)
		{
			Vector3 pts[3] =
			{
				pTri->m_Nodes[0]->m_vBasePos,
				pTri->m_Nodes[1]->m_vBasePos,
				pTri->m_Nodes[2]->m_vBasePos
			};
			// We're only bothering about the XY area
			pts[0].z = 0.0f;
			pts[1].z = 0.0f;
			pts[2].z = 0.0f;

			float fArea = CalcTriArea(pts);
			if(fArea <= fSmallArea)
			{
				pTri->m_iFlags &= ~NAVSURFTRI_ACCESSIBLE;

				m_iNumSmallTrianglesRemoved++;

				// Remove reference back to this tri from adjacent tris
				for(int a=0; a<3; a++)
				{
					if(pTri->m_AdjacentTris[a])
					{
						if(pTri->m_AdjacentTris[a]->m_AdjacentTris[0] == pTri)
							pTri->m_AdjacentTris[a]->m_AdjacentTris[0] = NULL;
						if(pTri->m_AdjacentTris[a]->m_AdjacentTris[1] == pTri)
							pTri->m_AdjacentTris[a]->m_AdjacentTris[1] = NULL;
						if(pTri->m_AdjacentTris[a]->m_AdjacentTris[2] == pTri)
							pTri->m_AdjacentTris[a]->m_AdjacentTris[2] = NULL;
					}
				}
			}
		}
	}

	for(n=0; n<m_NavSurfaceNodes.size(); n++)
	{
		CNavGenNode * pNode = m_NavSurfaceNodes[n];

		for(t=0; t<pNode->m_TrianglesUsingThisNode.size(); t++)
		{
			CNavSurfaceTri * pTri = pNode->m_TrianglesUsingThisNode[t];

			if(pTri->IsRemoved())
				continue;

			break;
		}

		// If 't' reached the end of the list, then all the triangles surrounding the node are removed..
		if(t == pNode->m_TrianglesUsingThisNode.size())
		{
			pNode->SetRemoved();
		}
	}

}




void CNavGen::MarkWhetherNodesAreSurroundedByTriangles()
{
	u32 n,t;

	// Mark all the nodes which are on the interior of the navmesh
	for(n=0; n<m_NavSurfaceNodes.size(); n++)
	{
		CNavGenNode * pNode = m_NavSurfaceNodes[n];
		if(pNode->IsRemoved())
			continue;

		static const float fAngleEps = (4.0f * DtoR);
		float fAngleTotal = 0.0f;

		for(t=0; t<pNode->m_TrianglesUsingThisNode.size(); t++)
		{
			CNavSurfaceTri * pTri = pNode->m_TrianglesUsingThisNode[t];
			if(pTri->IsRemoved())
				continue;

			CNavGenNode * pOtherNode1, * pOtherNode2;

			if(pTri->m_Nodes[0]==pNode)
			{
				pOtherNode1 = pTri->m_Nodes[1];
				pOtherNode2 = pTri->m_Nodes[2];
			}
			else if(pTri->m_Nodes[1]==pNode)
			{
				pOtherNode1 = pTri->m_Nodes[2];
				pOtherNode2 = pTri->m_Nodes[0];
			}
			else if(pTri->m_Nodes[2]==pNode)
			{
				pOtherNode1 = pTri->m_Nodes[0];
				pOtherNode2 = pTri->m_Nodes[1];
			}
			else
			{
				Assert(0);
				continue;
			}

			Vector3 v1 = pOtherNode1->m_vBasePos - pNode->m_vBasePos;
			Vector3 v2 = pOtherNode2->m_vBasePos - pNode->m_vBasePos;

			v1.Normalize();
			v2.Normalize();

			const float fDot = Clamp( DotProduct(v1,v2), -1.0f, 1.0f);
			const float fAngle = acosf(fDot);

			fAngleTotal += fAngle;
		}

		if(Eq(fAngleTotal, PI*2.0f, fAngleEps))
		{
			pNode->m_bIsSurroundedByTriangles = true;
		}
		else
		{
			pNode->m_bIsSurroundedByTriangles = false;
		}
	}
}

//****************************************************************************************
//	RelexVertexPositions
//	This is to be called after the optimisation process.
//	The idea is to visit all the nodes which are completely surrounded by triangles, and
//	to adjust their positions such that the distance between all their surrounding nodes
//	is as even as possible.
//	It returns the largest distance that any vertex was moved, the idea being that we can
//	repeat until this dips under some threashold.
//****************************************************************************************

float CNavGen::RelaxVertexPositions()
{
	u32 n,s;
	float fGreatestMag = 0.0f;

#define MAX_SURROUNDING_NODES		32

	Vector3 vToOtherNodes[MAX_SURROUNDING_NODES];
	CNavGenNode * pOtherNodes[MAX_SURROUNDING_NODES];
	float fDistToOtherNodes[MAX_SURROUNDING_NODES];

	for(n=0; n<m_NavSurfaceNodes.size(); n++)
	{
		CNavGenNode * pNode = m_NavSurfaceNodes[n];
		if(pNode->IsRemoved())
			continue;

		if(!pNode->m_bIsSurroundedByTriangles)
			continue;

		Assert(pNode->m_EdgesUsingThisNode.size() < MAX_SURROUNDING_NODES);

		float fAverageDist = 0.0f;
		int iNumActiveSurrounding = 0;

		for(s=0; s<pNode->m_EdgesUsingThisNode.size(); s++)
		{
			CNavTriEdge * pEdge = pNode->m_EdgesUsingThisNode[s];
			if(pEdge->IsRemoved())
				continue;

			CNavGenNode * pOtherNode = (pEdge->m_pNode1==pNode) ? pEdge->m_pNode2 : pEdge->m_pNode1;
			if(pOtherNode->IsRemoved())
				continue;

			pOtherNodes[s] = pOtherNode;
			vToOtherNodes[s] = pOtherNode->m_vBasePos - pNode->m_vBasePos;
			fDistToOtherNodes[s] = vToOtherNodes[s].Mag();
			vToOtherNodes[s].Normalize();

			fAverageDist += fDistToOtherNodes[s];
			iNumActiveSurrounding++;
		}

		fAverageDist /= ((float)iNumActiveSurrounding);

		Vector3 vImpulse(0.0f,0.0f,0.0f);

		for(s=0; s<pNode->m_EdgesUsingThisNode.size(); s++)
		{
			CNavGenNode * pOtherNode = pOtherNodes[s];
			if(pOtherNode->IsRemoved())
				continue;

			float fDiff = fDistToOtherNodes[s] - fAverageDist;
			vImpulse += (vToOtherNodes[s] * fDiff);
		}

		vImpulse /= ((float)iNumActiveSurrounding);

		// Make sure that this doesn't make any nodes too close
		const Vector3 vNewNodePos = pNode->m_vBasePos + vImpulse;
		for(s=0; s<pNode->m_EdgesUsingThisNode.size(); s++)
		{
			CNavGenNode * pOtherNode = pOtherNodes[s];
			if(pOtherNode->IsRemoved())
				continue;

			const Vector3 vDiff = pOtherNode->m_vBasePos - vNewNodePos;
			if(vDiff.Mag() <= m_Params.m_fMinTriangleSideLength)
				break;
		}

		if(s == pNode->m_EdgesUsingThisNode.size())
		{
			pNode->m_vBasePos += vImpulse;

			const float fMag = vImpulse.Mag();
			if(fMag > fGreatestMag)
				fGreatestMag = fMag;
		}
	}

	return fGreatestMag;
}




//**************************************************************************
//	Because we mark triangles which are on slopes which are too steep to
//	walk upon, and these cause hard boundaries in the mesh which cannot be
//	optimized across - we perform a process after mesh triangulation & prior
//	to optimization which attempts to remove the jagged edges caused and
//	thereby allow better navmesh optimising & polygon merging.
//
//		.
//	   /|\
//	  / | \
//   |\ | /|
//   | \|/ |
//   | /|\ |
//	 |/ | \| ____ here
//    \ | /
//	   \|/
//      .
//
//	The top for triangles are TOO_STEEP, whereas the bottom two are not.. This is
//	a common occurance!  If we move the CENTRE vertex down to the position labeled
//	'here' then we will remove the jaggy edge.

void CNavGen::RemoveSteepSlopeJaggies()
{
	CNavSurfaceTri * pNavTris[6];
	static const float fTestDot = 0.707f;

	u32 n;
	u32 t,t2;


	//***********************************************************************************
	// Firstly go through all triangles and remove steep-sloped triangles which
	// have only one neighbour, and whose only neighbour is non steep.  And vice versa.
	// These triangles will never be optimised and are basically useless!

	for(t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];
		if(pTri->IsRemoved())
			continue;

		CNavSurfaceTri * pAdjacentTri = NULL;
		int iNumAdjacent = 0;
		for(s32 a=0; a<3 && iNumAdjacent<2; a++)
		{
			if(pTri->m_AdjacentTris[a] && !pTri->m_AdjacentTris[a]->IsRemoved())
			{
				pAdjacentTri = pTri->m_AdjacentTris[a];
				iNumAdjacent++;
			}
		}
		if(iNumAdjacent != 1)
			continue;

		if((pAdjacentTri->m_bIsTooSteep && !pTri->m_bIsTooSteep) || (!pAdjacentTri->m_bIsTooSteep && pTri->m_bIsTooSteep))
		{
			pTri->SetRemoved();	
		}
	}

	//*******************************************************************************************
	// Now go through all the nodes & try to fix jagged edges between steep & non-steep surfaces
	// This should allow optimisation to be a little more effective.

	for(n=0; n<m_NavSurfaceNodes.size(); n++)
	{
		CNavGenNode * pNode = m_NavSurfaceNodes[n];
		if(pNode->IsRemoved())
			continue;
		if(pNode->m_TrianglesUsingThisNode.size()!=6)
			continue;
		if(pNode->m_bHasBeenMovedToFixJaggies)
			continue;

#if __DEV
//		static bool bPause = false;
//		Vector3 vTestPos(119.08f, 819.74f, 28.91f);
//		if(CMaths::Eq(pNode->m_vBasePos.x, vTestPos.x, 0.1f) && CMaths::Eq(pNode->m_vBasePos.y, vTestPos.y, 0.1f))
//			bPause = true;
#endif

		s32 iSteepCount = 0;
		s32 iNonSteepCount = 0;

		for(t=0; t<6; t++)
		{
			CNavSurfaceTri * pTri = pNode->m_TrianglesUsingThisNode[t];

			// All triangles need to be more-or-less in the same plane
			const float fDot = DotProduct(pTri->m_vNormal, pNode->m_TrianglesUsingThisNode[0]->m_vNormal);
			if(fDot < fTestDot)
				break; 

			if(pTri->m_bIsTooSteep)
				iSteepCount++;
			else iNonSteepCount++;

			pNavTris[t] = pTri;
		}

		if(iSteepCount!=4 || iNonSteepCount!=2)
			continue;

		CNavGenNode * pHighestNode = NULL, * pLowestNode = NULL;
		for(t=0; t<6; t++)
		{
			for(u32 n2=0; n2<3; n2++)
			{
				if(!pHighestNode || pNavTris[t]->m_Nodes[n2]->m_vBasePos.z > pHighestNode->m_vBasePos.z)
					pHighestNode = pNavTris[t]->m_Nodes[n2];
				if(!pLowestNode || pNavTris[t]->m_Nodes[n2]->m_vBasePos.z < pLowestNode->m_vBasePos.z)
					pLowestNode = pNavTris[t]->m_Nodes[n2];
			}
		}

		for(t=0; t<6 && !pNode->m_bHasBeenMovedToFixJaggies; t++)
		{
			CNavSurfaceTri * pTri1 = pNavTris[t];

			for(t2=t+1; t2<6; t2++)
			{
				CNavSurfaceTri * pTri2 = pNavTris[t2];

				CNavGenNode * pSharedNode1 = NULL, * pSharedNode2 = NULL;

				if(GetSharedNodes(pTri1, pTri2, &pSharedNode1, &pSharedNode2, NULL)==2)
				{
					// Have we found the two non-steep triangles to be adjacent?
					if(!pTri1->m_bIsTooSteep && !pTri2->m_bIsTooSteep)// && (pSharedNode1==pLowestNode||pSharedNode2==pLowestNode))
					{
						CNavGenNode * pOtherNode1 = GetOtherNode(pTri1, pSharedNode1, pSharedNode2);
						CNavGenNode * pOtherNode2 = GetOtherNode(pTri2, pSharedNode1, pSharedNode2);

						const Vector3 vAverage = (pSharedNode1->m_vBasePos + pSharedNode2->m_vBasePos + pOtherNode1->m_vBasePos + pOtherNode2->m_vBasePos) / 4.0f;

						pNode->m_vBasePos = vAverage;
						pNode->m_bHasBeenMovedToFixJaggies = true;
						break;
					}
				}
			}
		}
	}
}

}	// namespace rage
