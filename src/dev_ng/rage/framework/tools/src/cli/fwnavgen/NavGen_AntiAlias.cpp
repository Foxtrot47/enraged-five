#include "NavGen_AntiAlias.h"
#include "NavGen.h"	// TODO: Move navgen types into their own header file, away from CNavGan

using namespace rage;

#define __AA_EDGES_USE_CLOSEST_POINT_ON_EDGE	1

namespace rage
{
	// Default FN, to identify open boundary edge of mesh
	bool DefaultIsBoundaryEdgeFN(const CNavSurfaceTri * pTri, const s32 iEdge)
	{
		return (pTri->m_AdjacentTris[iEdge] == NULL);
	}

	// FNs which identify pavement edges
	bool PavementIsBoundaryEdgeFN(const CNavSurfaceTri * pTri, const s32 iEdge)
	{
		return pTri->m_colPolyData.GetIsPavement() &&
				(pTri->m_AdjacentTris[iEdge] != NULL) &&
				 (!pTri->m_AdjacentTris[iEdge]->m_colPolyData.GetIsPavement());
	}
	bool InvPavementIsBoundaryEdgeFN(const CNavSurfaceTri * pTri, const s32 iEdge)
	{
		return !pTri->m_colPolyData.GetIsPavement() &&
			(pTri->m_AdjacentTris[iEdge] != NULL) &&
			(pTri->m_AdjacentTris[iEdge]->m_colPolyData.GetIsPavement());
	}

	// FNs which identify steep slope edges
	bool SteepSlopeIsBoundaryEdgeFN(const CNavSurfaceTri * pTri, const s32 iEdge)
	{
		return pTri->m_bIsTooSteep &&
			(pTri->m_AdjacentTris[iEdge] != NULL) &&
			(!pTri->m_AdjacentTris[iEdge]->m_bIsTooSteep);
	}
	bool InvSteepSlopeIsBoundaryEdgeFN(const CNavSurfaceTri * pTri, const s32 iEdge)
	{
		return !pTri->m_bIsTooSteep &&
			(pTri->m_AdjacentTris[iEdge] != NULL) &&
			(pTri->m_AdjacentTris[iEdge]->m_bIsTooSteep);
	}

	bool IsValidNodeMoveForExternalEdge(
		CNavGen * pNavGen, CNavGenNode * pNode, const Vector3 & vNewPosition,
		CNavGenNode * pExteriorNode1, CNavGenNode * pExteriorNode2,
		CNavSurfaceTri * pTri1, CNavSurfaceTri * pTri2)
	{
		static const float fHeightTestIncrement = 0.125f;

		const Vector3 vUp(0.0f, 0.0f, pNavGen->m_Params.m_HeightAboveNodeBaseAtWhichToTriangulate);

		if(pTri1->m_bIsWater != pTri2->m_bIsWater)
			return false;

		// Ensure that no triangles will get flipped
		for(u32 t=0; t<pNode->m_TrianglesUsingThisNode.size(); t++)
		{
			CNavSurfaceTri * pNodeTri = pNode->m_TrianglesUsingThisNode[t];
			if(!pNodeTri->IsRemoved())
			{
				Vector3 vNormalBefore = pNodeTri->CalcNormal();
				vNormalBefore.Normalize();
				const Vector3 vOldNodePos = pNode->m_vBasePos;
				pNode->m_vBasePos = vNewPosition;
				Vector3 vNormalAfter = pNodeTri->CalcNormal();
				vNormalAfter.Normalize();
				pNode->m_vBasePos = vOldNodePos;

				const float fDot = DotProduct(vNormalBefore, vNormalAfter);
				if(fDot < 0.707f)
					return false;
			}
		}

		// No ground underneath?
		if( pNavGen->m_Octree.TestLineOfSight(vNewPosition + vUp, vNewPosition - ZAXIS, NAVGEN_COLLISION_TYPES) )
		{
			return false;
		}

		// We will probably at least need to also do tests from both exterior edges to this new position
		// And quite possibly tests for all triangle edges attached to this node.
		bool bTooSteep1 = false;
		if ( !pNavGen->IsLineFreeOfSuddenHeightChanges(pNavGen->m_Octree, pNode->m_vBasePos + vUp, vNewPosition + vUp, NULL, NULL, NAVGEN_COLLISION_TYPES, bTooSteep1, fHeightTestIncrement, pNavGen->m_Params.m_MaxHeightChangeUnderTriangleEdge, false, true, !pTri1->m_bIsWater, pTri1->m_bIsWater))
		{
			return false;
		}

		// Now for both exterior nodes as well
		bool bTooSteep2 = false;
		if ( !pNavGen->IsLineFreeOfSuddenHeightChanges(pNavGen->m_Octree, pExteriorNode1->m_vBasePos + vUp, vNewPosition + vUp, NULL, NULL, NAVGEN_COLLISION_TYPES, bTooSteep2, fHeightTestIncrement, pNavGen->m_Params.m_MaxHeightChangeUnderTriangleEdge, false, true, !pTri1->m_bIsWater, pTri1->m_bIsWater))
		{
			return false;
		}

		bool bTooSteep3 = false;
		if ( !pNavGen->IsLineFreeOfSuddenHeightChanges(pNavGen->m_Octree, pExteriorNode2->m_vBasePos + vUp, vNewPosition + vUp, NULL, NULL, NAVGEN_COLLISION_TYPES, bTooSteep3, fHeightTestIncrement, pNavGen->m_Params.m_MaxHeightChangeUnderTriangleEdge, false, true, !pTri1->m_bIsWater, pTri1->m_bIsWater))
		{
			return false;
		}

		// Make sure no collision found between old & new position
		if( !pNavGen->m_Octree.TestLineOfSight(pNode->m_vBasePos + vUp, vNewPosition + vUp, NAVGEN_COLLISION_TYPES) )
		{
			return false;
		}

		// Make sure no collision from each exterior node to new position

		if( !pNavGen->m_Octree.TestLineOfSight(pExteriorNode1->m_vBasePos + vUp, vNewPosition + vUp, NAVGEN_COLLISION_TYPES) )
		{
			return false;
		}
		if( !pNavGen->m_Octree.TestLineOfSight(pExteriorNode2->m_vBasePos + vUp, vNewPosition + vUp, NAVGEN_COLLISION_TYPES) )
		{
			return false;
		}

		//---------------------------------------------------------------------------------------------------
		// Perform thorough tests for collision intersecting the volume above the two adjacent triangles..

		const Vector3 vPtsTri1[3] =
		{
			(pTri1->m_Nodes[0] == pNode) ? vNewPosition + vUp : pTri1->m_Nodes[0]->m_vBasePos + vUp,
			(pTri1->m_Nodes[1] == pNode) ? vNewPosition + vUp : pTri1->m_Nodes[1]->m_vBasePos + vUp,
			(pTri1->m_Nodes[2] == pNode) ? vNewPosition + vUp : pTri1->m_Nodes[2]->m_vBasePos + vUp
		};

		CClipVolume vol;

		if( !vol.InitExtrudedTri(vPtsTri1, pNavGen->m_Params.m_fTestClearHeightInTriangulation - pNavGen->m_Params.m_HeightAboveNodeBaseAtWhichToTriangulate, &ZAXIS) )
			return false;

		if( !pNavGen->m_Octree.TestVolumeIsClear(vol, NAVGEN_COLLISION_TYPES))
			return false;


		const Vector3 vPtsTri2[3] =
		{
			(pTri2->m_Nodes[0] == pNode) ? vNewPosition + vUp : pTri2->m_Nodes[0]->m_vBasePos + vUp,
			(pTri2->m_Nodes[1] == pNode) ? vNewPosition + vUp : pTri2->m_Nodes[1]->m_vBasePos + vUp,
			(pTri2->m_Nodes[2] == pNode) ? vNewPosition + vUp : pTri2->m_Nodes[2]->m_vBasePos + vUp
		};

		if( !vol.InitExtrudedTri(vPtsTri2, pNavGen->m_Params.m_fTestClearHeightInTriangulation - pNavGen->m_Params.m_HeightAboveNodeBaseAtWhichToTriangulate, &ZAXIS) )
			return false;

		if( !pNavGen->m_Octree.TestVolumeIsClear(vol, NAVGEN_COLLISION_TYPES))
			return false;

		return true;
	}


	bool IsValidNodeMoveForPavementEdge(
		CNavGen * pNavGen, CNavGenNode * pNode, const Vector3 & vNewPosition,
		CNavGenNode * pExteriorNode1, CNavGenNode * pExteriorNode2,
		CNavSurfaceTri * pTri1, CNavSurfaceTri * pTri2)
	{
		const Vector3 vUp(0.0f, 0.0f, pNavGen->m_Params.m_HeightAboveNodeBaseAtWhichToTriangulate);

		// No pavement underneath?
		Vector3 vHitPos;
		float fHitDist;
		CNavGenTri * pCollisionTri;
		u32 iRetFlags = 0;

		if( !pNavGen->m_Octree.ProcessLineOfSight(vNewPosition + vUp, vNewPosition - ZAXIS, iRetFlags, NAVGEN_COLLISION_TYPES, vHitPos, fHitDist, pCollisionTri) )
		{
			return false;
		}

		if( !pCollisionTri->m_colPolyData.GetIsPavement() )
		{
			return false;
		}

		return IsValidNodeMoveForExternalEdge(pNavGen, pNode, vNewPosition, pExteriorNode1, pExteriorNode2, pTri1, pTri2);
	}

}

CNavEdgeIterator::CNavEdgeIterator()
{

}
CNavEdgeIterator::~CNavEdgeIterator()
{

}

// FUNCTION : Init
// PURPOSE : Initialise the iterator to start with the given triangle & edge

bool CNavEdgeIterator::Init(CNavSurfaceTri * pTri, s32 iEdge, IsBoundaryEdgeFN boundaryEdgeFn)
{
	Assert(pTri);

	m_StartingEdge.m_pTri = pTri;
	m_StartingEdge.m_iEdge = iEdge;

	m_CurrentEdge.m_pTri = pTri;
	m_CurrentEdge.m_iEdge = iEdge;

	m_pIsBoundaryEdgeFunction = boundaryEdgeFn ? boundaryEdgeFn : DefaultIsBoundaryEdgeFN;

	Assertf(m_pIsBoundaryEdgeFunction( pTri, iEdge ), "This is not a boundary edge!");

	return true;
}

// FUNCTION : GetCurrent
// PURPOSE : Get the current edge
void CNavEdgeIterator::GetCurrent(CNavSurfaceTri ** ppTri, s32 * piEdge, CNavGenNode ** ppNode1, CNavGenNode ** ppNode2)
{
	*ppTri = m_CurrentEdge.m_pTri;
	*piEdge = m_CurrentEdge.m_iEdge;

	*ppNode1 = m_CurrentEdge.m_pTri->m_Nodes[ m_CurrentEdge.m_iEdge ];
	*ppNode2 = m_CurrentEdge.m_pTri->m_Nodes[ (m_CurrentEdge.m_iEdge+1) % 3 ];
}

// FUNCTION : Step
// PURPOSE : Iterate to the next triangle/edge on the boundary edge, or return false if iteration is complete
CNavEdgeIterator::EIterRet CNavEdgeIterator::Step(s32 iPass)
{
	// Our current edge is (i1 to i2)
	// We will be looking to walk around node i2, and find a match for edge (i2 to i3)

	s32 i2 = (m_CurrentEdge.m_iEdge+1) % 3;
	s32 i3 = (m_CurrentEdge.m_iEdge+2) % 3;

	// If the next edge on this triangle is empty, then we iterate straight there
	//if(m_CurrentEdge.m_pTri->m_AdjacentTris[i2] == NULL)
	if( m_pIsBoundaryEdgeFunction(m_CurrentEdge.m_pTri, i2) )
	{
		m_CurrentEdge.m_iEdge = i2;
		// Complete
		if(m_CurrentEdge.m_pTri == m_StartingEdge.m_pTri && m_CurrentEdge.m_iEdge == m_StartingEdge.m_iEdge)
			return ITER_COMPLETE;
		// Keep going
		return ITER_CONTINUE;
	}

	CNavSurfaceTri * pStepTri = m_CurrentEdge.m_pTri;
	CNavGenNode * pPivotNode = m_CurrentEdge.m_pTri->m_Nodes[i2];
	CNavGenNode * pMatchNode1 = pStepTri->m_Nodes[i2];
	CNavGenNode * pMatchNode2 = pStepTri->m_Nodes[i3];

	bool bNextEdgeFound = false;
	while(!bNextEdgeFound)
	{
		vector<CNavSurfaceTri*> & triangles = pPivotNode->m_TrianglesUsingThisNode;

RESTART:
		u32 t;
		for(t=0; t<triangles.size(); t++)
		{
			CNavSurfaceTri * pNextTri = triangles[t];
			if(pNextTri->IsRemoved())
				continue;
			if(pNextTri == m_CurrentEdge.m_pTri)
				continue;

			s32 iNextEdge = pNextTri->GetEdgeForAntiAliasPass(pMatchNode2, pMatchNode1, iPass);
			if(iNextEdge == -1)
				continue;

			// We've found this edge matched in an adjacent triangle.
			// Now iterate again on this triangle to find an open edge

			Assert(pNextTri->m_AntiAliasPass[iNextEdge] < iPass);
			pNextTri->m_AntiAliasPass[iNextEdge] = (u8)iPass;

			pStepTri = pNextTri;

			// Is the next edge on this triangle empty?
			// If so we can traverse it
			s32 iAdjacentEdge = (iNextEdge+1) % 3;

			if( m_pIsBoundaryEdgeFunction( pStepTri, iAdjacentEdge ) )
			{
				m_CurrentEdge.m_pTri = pStepTri;
				m_CurrentEdge.m_iEdge = iAdjacentEdge;

				// Complete
				if(m_CurrentEdge.m_pTri == m_StartingEdge.m_pTri && m_CurrentEdge.m_iEdge == m_StartingEdge.m_iEdge)
					return ITER_COMPLETE;
				// Keep going
				return ITER_CONTINUE;
			}

			pMatchNode1 = pStepTri->m_Nodes[ (iNextEdge+1) % 3 ];
			pMatchNode2 = pStepTri->m_Nodes[ (iNextEdge+2) % 3 ];

			goto RESTART;
		}

		// We found no adjacent candidate triangle to step onto, so we iterate across the current tri edges
		if(!bNextEdgeFound)
		{
			if(pStepTri == m_CurrentEdge.m_pTri)
			{
				s32 iNextEdge = (m_CurrentEdge.m_iEdge+1) % 3;

				// If the next edge on this triangle is empty we can traverse onto it
				//if(m_CurrentEdge.m_pTri->m_AdjacentTris[iNextEdge] == NULL)
				if( m_pIsBoundaryEdgeFunction( m_CurrentEdge.m_pTri, iNextEdge ) )
				{
					m_CurrentEdge.m_iEdge = iNextEdge;
					bNextEdgeFound = true;
				}
				// Otherwise we'll have to quit
				else
				{
					return ITER_ERROR;
				}
			}
			else
			{
				// We're really stuck..
				return ITER_ERROR;
			}
		}
	}

	// If we have arrived back at our starting triangle & edge, then we've finished
	if(m_CurrentEdge.m_pTri == m_StartingEdge.m_pTri && m_CurrentEdge.m_iEdge == m_StartingEdge.m_iEdge)
	{
		return ITER_COMPLETE;
	}

	// Keep going
	return ITER_CONTINUE;
}



CNavEdgeIterator::CEdge CNavAntiAlias::LocateBoundaryEdgeTriangle(IsBoundaryEdgeFN pFN, s32 iPass)
{
	CNavEdgeIterator::CEdge edge;

	// Locate a starting triangle
	for(u32 t=0; t<m_pNavGen->m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_pNavGen->m_NavSurfaceTriangles[t];
		if(pTri->IsRemoved())
			continue;

		for(s32 e=0; e<3; e++)
		{
			if( pFN(pTri, e) && pTri->m_AntiAliasPass[e] < iPass )
			{
				edge.m_pTri = pTri;
				edge.m_iEdge = e;
				return edge;
			}
		}
	}
	return edge;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

CNavAntiAlias::CNavAntiAlias(CNavGen * pNavGen)
{
	m_pNavGen = pNavGen;
}

void CNavAntiAlias::AntiAliasMeshEdges(IsBoundaryEdgeFN isEdgeFN, IsValidMoveFN isValidMoveFN)
{
	s32 p,e;

	// Reset AA flags on all polygon edges
	for(u32 t=0; t<m_pNavGen->m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_pNavGen->m_NavSurfaceTriangles[t];
		if(pTri->IsRemoved())
			continue;

		for(e=0; e<3; e++)
			pTri->m_AntiAliasPass[e] = 0;
	}

	// Now perform number of passes
	const int iNumPasses = 1;
	for(p=1; p<iNumPasses+1; p++)
	{
		CNavEdgeIterator::CEdge startingEdge = LocateBoundaryEdgeTriangle( isEdgeFN, p);

		while( startingEdge.m_pTri )
		{
			CNavEdgeIterator edgeIter;
			if( edgeIter.Init(startingEdge.m_pTri, startingEdge.m_iEdge, isEdgeFN) )
			{
				CNavSurfaceTri * pTris[2];
				int iEdges[2];
				CNavGenNode * pEdgeNode1[2];
				CNavGenNode * pEdgeNode2[2];

				edgeIter.GetCurrent( &pTris[0], &iEdges[0], &pEdgeNode1[0], &pEdgeNode2[0] );
				pTris[0]->m_AntiAliasPass[iEdges[0]] = (u8)p;

				Assert(pEdgeNode1[0]);
				Assert(pEdgeNode2[0]);

				do 
				{
					if(edgeIter.Step(p) != CNavEdgeIterator::ITER_CONTINUE)
						break;

					edgeIter.GetCurrent( &pTris[1], &iEdges[1], &pEdgeNode1[1], &pEdgeNode2[1] );

					// ERROR? Attempt to fix buildmachine infinite loop
 					Assert( pTris[1]->m_AntiAliasPass[iEdges[1]] != (u8)p );
					if( pTris[1]->m_AntiAliasPass[iEdges[1]] == (u8)p )
					{
 						break;
					}

					pTris[1]->m_AntiAliasPass[iEdges[1]] = (u8)p;

					Assert(pEdgeNode1[1]);
					Assert(pEdgeNode2[1]);

					//------------------------------
					// can we smooth this edge?

					if(pTris[0] != pTris[1])	// not if both edges originate from the same triangle
					{
						Vector3 vEdge1 = pEdgeNode2[0]->m_vBasePos - pEdgeNode1[0]->m_vBasePos;
						vEdge1.z = 0.0f;
						vEdge1.Normalize();

						Vector3 vEdge2 = pEdgeNode2[1]->m_vBasePos - pEdgeNode1[1]->m_vBasePos;
						vEdge2.z = 0.0f;
						vEdge2.Normalize();

						const float fDot = DotProduct(vEdge1, vEdge2);
						const Vector3 vCross = CrossProduct(vEdge1, vEdge2);

						static dev_float fDotThreshold = 1.0f; //0.99f; //707f;

						// Only if this is a convex edge
						if( fDot < fDotThreshold && vCross.z > 0.0f )
						{
							// Try to move the shared node to a position between the two exterior nodes,
							// or as close as we can get it.

							CNavGenNode * pExteriorNode1 = NULL;
							CNavGenNode * pExteriorNode2 = NULL;
							
							if(pEdgeNode1[0] == pEdgeNode1[1] || pEdgeNode1[0] == pEdgeNode2[1])
								pExteriorNode1 = pEdgeNode2[0];
							else if(pEdgeNode2[0] == pEdgeNode1[1] || pEdgeNode2[0] == pEdgeNode2[1])
								pExteriorNode1 = pEdgeNode1[0];

							if(pEdgeNode1[1] == pEdgeNode1[0] || pEdgeNode1[1] == pEdgeNode2[0])
								pExteriorNode2 = pEdgeNode2[1];
							else if(pEdgeNode2[1] == pEdgeNode1[0] || pEdgeNode2[1] == pEdgeNode2[0])
								pExteriorNode2 = pEdgeNode1[1];

							Assert(pExteriorNode1 && pExteriorNode2 && (pExteriorNode1 != pExteriorNode2));
							if(pExteriorNode1 && pExteriorNode2)
							{
								CNavGenNode * pCentralNode = (pExteriorNode1 == pEdgeNode1[0]) ? pEdgeNode2[0] : pEdgeNode1[0];

								const float fWeights[] = { 1.0f, 0.75f, 0.5f, 0.25f };

#if __AA_EDGES_USE_CLOSEST_POINT_ON_EDGE

								const Vector3 vSeg = pExteriorNode2->m_vBasePos - pExteriorNode1->m_vBasePos;
								Vector3 vDesiredPos;

								if(vSeg.Mag2() < SMALL_FLOAT)
								{
									vDesiredPos = pExteriorNode1->m_vBasePos;
								}
								else
								{
									const float T = geomTValues::FindTValueSegToPoint(pExteriorNode1->m_vBasePos, vSeg, pCentralNode->m_vBasePos);
									vDesiredPos = pExteriorNode1->m_vBasePos + (vSeg * T);
								}

								const float fTooNear2 = 0.1f*0.1f;
								const bool bTooClose =
									(vDesiredPos-pExteriorNode1->m_vBasePos).Mag2() < fTooNear2 ||
									(vDesiredPos-pExteriorNode2->m_vBasePos).Mag2() < fTooNear2;
#else

								const Vector3 vDesiredPos = (pExteriorNode1->m_vBasePos + pExteriorNode2->m_vBasePos) * 0.5f;
								const bool bTooClose = false;

#endif // __AA_EDGES_USE_CLOSEST_POINT_ON_EDGE

								if( !bTooClose )
								{
									for(s32 a=0; a<4; a++)
									{
										const Vector3 vNewPos = (vDesiredPos * fWeights[a]) + (pCentralNode->m_vBasePos * (1.0f - fWeights[a]));

										if( isValidMoveFN( m_pNavGen, pCentralNode, vNewPos, pExteriorNode1, pExteriorNode2, pTris[0], pTris[1] ) )
										{
											pCentralNode->m_vBasePos = vNewPos;

											/*
											// Hopefully not necessary..
											for(u32 st=0; st<pCentralNode->m_TrianglesUsingThisNode.size(); st++)
											{
												CNavSurfaceTri * pAffectedTri = pCentralNode->m_TrianglesUsingThisNode[st];
												if(!pAffectedTri->IsRemoved())
													pAffectedTri->m_iFlags |= NAVSURFTRI_WAS_MOVED;
											}
											*/

											break;
										}
									}
								}
							}
						}
					}

					//----------------------------------------
					// shuffle current into last, and repeat

					pTris[0] = pTris[1];
					iEdges[0] = iEdges[1];
					pEdgeNode1[0] = pEdgeNode1[1];
					pEdgeNode2[0] = pEdgeNode2[1];

				} while( true );
			}

			// Repeat for all boundary edges in this navmesh section
			startingEdge = LocateBoundaryEdgeTriangle( isEdgeFN, p);
		}
	}
}

//-------------------------------------------------------------------------------------------
// NAME : AntiAliasPavementEdges
// PURPOSE : Attempt to remove/reduce aliasing artifacts along pavement edges in the mesh


void CNavAntiAlias::AntiAliasPavementEdges()
{
	AntiAliasMeshEdges(PavementIsBoundaryEdgeFN, IsValidNodeMoveForPavementEdge);
	AntiAliasMeshEdges(InvPavementIsBoundaryEdgeFN, IsValidNodeMoveForExternalEdge);
}

//-------------------------------------------------------------------------------------------
// NAME : AntiAliasSteepSlopeEdges
// PURPOSE : Attempt to remove/reduce aliasing artifacts along steep slope edges in the mesh


void CNavAntiAlias::AntiAliasSteepSlopeEdges()
{
	AntiAliasMeshEdges(SteepSlopeIsBoundaryEdgeFN, IsValidNodeMoveForExternalEdge);
	AntiAliasMeshEdges(InvSteepSlopeIsBoundaryEdgeFN, IsValidNodeMoveForExternalEdge);
}

//-------------------------------------------------------------------------------------------
// NAME : AntiAliasBoundaryEdges
// PURPOSE : Attempt to remove/reduce aliasing artifacts along boundary edges in the mesh

void CNavAntiAlias::AntiAliasBoundaryEdges()
{
	AntiAliasMeshEdges(DefaultIsBoundaryEdgeFN, IsValidNodeMoveForExternalEdge);
}


