#include "toolnavmesh.h"

// The #includes below are basically what 'ai/navmesh/navmesh.cpp' would have normally included:

// Rage headers
#include "math/simpleMath.h"

// Framework headers
#include "fwmaths/vector.h"
#include "spatialdata/sphere.h"

// Game headers
#include "data/safestruct.h"
#include "data/struct.h"
#include "file/asset.h"
#include "file/device.h"
#include "file/stream.h"
#include "paging/rscbuilder.h"
#include "vector/geometry.h"
#include "vector/vector2.h"
#include "vector/color32.h"

// This makes sure that 'navmesh.cpp' doesn't include other files or forward declare
// any symbols that don't belong in the fwToolNavMesh namespace.
#define COMPILE_TOOL_MESH

// Define these symbols to build the nav mesh code in its tool configuration.
#define NAVGEN_TOOL
#define NAVMESHMAKER

namespace rage
{

namespace fwToolNavMesh
{

// This is ugly - we #include the whole .cpp file, so that all the nav mesh symbols
// built in tool configuration end up inside the fwToolNavMesh namespace.
#include "ai/navmesh/navmesh.cpp"
#include "ai/navmesh/shortminmax.cpp"

}	// namespace fwToolNavMesh

}	// namespace rage
