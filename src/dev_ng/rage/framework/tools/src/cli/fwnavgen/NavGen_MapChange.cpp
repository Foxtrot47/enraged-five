#include "NavGen.h"
#include "NavGen_Store.h"
#include "workerthreads.h"
#include "ai/navmesh/navmeshextents.h"

#include <algorithm>		// std::sort()

// FILE : NavGen_MapChange.cpp
// PURPOSE : Classes and functions concerned with local changes to navmesh for DLC.

namespace rage
{


int CompareEdgeMinusX(CNavGen::TStitchPoly::TAdjacentDLCPoly const * p1, CNavGen::TStitchPoly::TAdjacentDLCPoly const * p2)
{
	if(p1->m_vVert1.y < p2->m_vVert1.y)
		return -1;
	else
		return 1;
}
int CompareEdgePlusX(CNavGen::TStitchPoly::TAdjacentDLCPoly const * p1, CNavGen::TStitchPoly::TAdjacentDLCPoly const * p2)
{
	if(p1->m_vVert1.y > p2->m_vVert1.y)
		return -1;
	else
		return 1;
}
int CompareEdgeMinusY(CNavGen::TStitchPoly::TAdjacentDLCPoly const * p1, CNavGen::TStitchPoly::TAdjacentDLCPoly const * p2)
{
	if(p1->m_vVert1.x < p2->m_vVert1.x)
		return -1;
	else
		return 1;
}
int CompareEdgePlusY(CNavGen::TStitchPoly::TAdjacentDLCPoly const * p1, CNavGen::TStitchPoly::TAdjacentDLCPoly const * p2)
{
	if(p1->m_vVert1.x > p2->m_vVert1.x)
		return -1;
	else
		return 1;
}
CNavGen::TStitchPoly * GetStitchPolyRepresentingNavMeshPoly(atArray<CNavGen::TStitchPoly*> & polyArray, s32 iPolyIndex)
{
	for(s32 i=0; i<polyArray.GetCount(); i++)
	{
		CNavGen::TStitchPoly * pStitchPoly = polyArray[i];
		if(pStitchPoly->m_iIndexInNavMesh == iPolyIndex)
			return pStitchPoly;
	}
	return NULL;
}
s32 GetVertexIndex(atArray<Vector3> & vertexArray, const Vector3 & vert, const float fSameVertexEps=0.01f)
{
	for(s32 v=0; v<vertexArray.GetCount(); v++)
	{
		if( vertexArray[v].IsClose(vert, fSameVertexEps) )
			return v;
	}
	return -1;
}

//------------------------------------------------------------------------------------------------------------
// JoinMeshesDLC
// This occurs after all the DLC navmeshes have been fully generated
// 1) Load all the new .inv files for input DLC navmeshes
// 2) Load the remaining non-DLC navmesh .inv files which shipped with the game.
// 3) Insert vertices to DLC navmeshes, to fix T-junctions along the edges with main map navmeshes
// 4) Insert degenerate polygons to DLC navmeshes, fix 1-to-many connections form maain map to DLC navmeshes
//
// In this way the DLC navmeshes guarantee to match up exactly with the external adjacencies of the main map.
//
// pInputNavMesh - the central input DLC navmesh
// ppAdjacentNavmeshes - array of up to 4 adjacent navmeshes from the main map (non DLC);
//  these will be ordered corresponding to the enumeration CNavGen::SideOfNeighbour, with regarda to the central input mesh

void CNavGen::JoinMeshesDLC(CNavMesh * pDLCNavMesh, CNavMesh ** ppAdjacentMainMapNavmeshes)
{
	//const float fShortEdge = 0.05f; //0.1f;
	const float fSameVertexEps = 0.01f;

	//-----------------------------------------------------------------------------------------------------

	// The 'DLC_edgePolys' arrays are used to create stitch polys for the navmesh polygons along each exterior edge
	// of the DLC navmesh, which will need to be modified in order for them to match up with the degenerate polys we're
	// going to create in order that we can adjoin this DLC navmesh exactly to the main map navmesh.
	atArray<TStitchPoly*> DLC_edgePolys;

	// The 'stitchPolys' arrays are used to store the zero-area polygons used to bridge the connection between
	// the DLC and the non-DLC navmeshes along each edge.  They will live within the DLC navmesh.
	// The first adjacency in each of these is *always* outwards to the adjoining main map navmesh; the other
	// adjacencies will point internally to navmesh polygons in the DLC navmesh (which will be created from the
	// entries in the 'DLC_edgePolys' arrays above).
	atArray<TStitchPoly*> stitchPolys[4];

	s32 iDLCX, iDLCY;
	CNavGen_NavMeshStore::GetSectorFromNavMeshIndex(pDLCNavMesh->GetIndexOfMesh(), iDLCX, iDLCY);

	Vector2 vDLCMin, vDLCMax;
	CNavGen_NavMeshStore::GetNavMeshExtentsFromIndex(pDLCNavMesh->GetIndexOfMesh(), vDLCMin, vDLCMax);

	pDLCNavMesh->IdentifyEdgePolys();

	for(s32 e=0; e<4; e++)
	{
		CNavMesh * pAdjacent = ppAdjacentMainMapNavmeshes[e];
		if(!pAdjacent)
			continue;

		pAdjacent->IdentifyEdgePolys();

		// Determine which side of the central DLC navmesh, the adjacent neighbour is
		s32 iAdjX, iAdjY;
		CNavGen_NavMeshStore::GetSectorFromNavMeshIndex(pAdjacent->GetIndexOfMesh(), iAdjX, iAdjY);

		SideOfNeighbour iAdjacentSide;
		float fEdgeCoord;

		if(iAdjX == iDLCX-CPathServerExtents::GetNumSectorsPerNavMesh() && iAdjY==iDLCY)
		{
			iAdjacentSide = eMinusX;
			fEdgeCoord = vDLCMin.x;
		}
		else if(iAdjX == iDLCX+CPathServerExtents::GetNumSectorsPerNavMesh() && iAdjY==iDLCY)
		{
			iAdjacentSide = ePlusX;
			fEdgeCoord = vDLCMax.x;
		}
		else if(iAdjX == iDLCX && iAdjY==iDLCY-CPathServerExtents::GetNumSectorsPerNavMesh())
		{
			iAdjacentSide = eMinusY;
			fEdgeCoord = vDLCMin.y;
		}
		else if(iAdjX == iDLCX && iAdjY==iDLCY+CPathServerExtents::GetNumSectorsPerNavMesh())
		{
			iAdjacentSide = ePlusY;
			fEdgeCoord = vDLCMax.y;
		}
		else
		{
			Assert(false);
			continue;
		}

		// For every polygon in the pAdjacent main-map navmesh which connects to the central DLC navmesh index via
		// a normal adjacency, we will create a stitch poly with initially a single TAdjPoly which will adjoin to
		// the poly's external edge.
		// The stitch poly will then have adjacencies added to that it bridges the gap to the DLC navmesh.
		// It will always be a zero-area.
		// Eventually the stitch poly will become a TNavMeshPoly within the DLC navmesh.


		// Set up a navmesh adjacency in the DLC mesh
		pDLCNavMesh->GetAdjacentMeshes()->Set( pAdjacent->GetIndexOfMesh() );

		for(u32 p=0; p<pAdjacent->GetNumPolys(); p++)
		{
			TNavMeshPoly * pAdjPoly = pAdjacent->GetPoly(p);
			if(!pAdjPoly->GetLiesAlongEdgeOfMesh())
				continue;

			for(u32 v=0; v<pAdjPoly->GetNumVertices(); v++)
			{
				// We are filtering through only edges which have a normal adjacency, which are adjoined to
				// the main map navmesh where the DLC navmesh is to be placed (in this way we can guarantee we
				// are dealing with polygon edges which are right against the edge of the mesh).

				const TAdjPoly & adj = pAdjacent->GetAdjacentPoly( pAdjPoly->GetFirstVertexIndex() + v );
				if(adj.GetAdjacencyType() != ADJACENCY_TYPE_NORMAL)
					continue;
				if(adj.GetNavMeshIndex(pAdjacent->GetAdjacentMeshes()) != pDLCNavMesh->GetIndexOfMesh())
					continue;

				Vector3 v1, v2;
				pAdjacent->GetVertex( pAdjacent->GetPolyVertexIndex(pAdjPoly, v), v1 );
				pAdjacent->GetVertex( pAdjacent->GetPolyVertexIndex(pAdjPoly, (v+1)%pAdjPoly->GetNumVertices() ), v2 );

				if((v2 - v1).XYMag2() <= SMALL_FLOAT)
					continue;

				TStitchPoly * pStitchPoly = new TStitchPoly();
				
				stitchPolys[e].PushAndGrow(pStitchPoly);

				TAdjPoly adjFromDLC;
				adjFromDLC.SetAdjacencyType(ADJACENCY_TYPE_NORMAL);
				adjFromDLC.SetNavMeshIndex(pAdjacent->GetIndexOfMesh(), pDLCNavMesh->GetAdjacentMeshes() );
				adjFromDLC.SetOriginalNavMeshIndex(pAdjacent->GetIndexOfMesh(), pDLCNavMesh->GetAdjacentMeshes() );
				adjFromDLC.SetPolyIndex(p);
				adjFromDLC.SetOriginalPolyIndex(p);
				adjFromDLC.SetIsExternalEdge(true);

				pStitchPoly->m_NewEdges.PushAndGrow(adjFromDLC);
				pStitchPoly->m_NewVertices.PushAndGrow(v2);	// add in reverse order to give correct adjacent winding
				pStitchPoly->m_NewVertices.PushAndGrow(v1);
			}
		}

		// Now we have an array of TStitchPoly for this edge which adjoins pAdjacent, and within each poly there's exactly
		// one external edge and the two vertices which bound it.
		// We should now take each TStitchPoly's initial edge and determine which edge polys in pDLCNavmesh it overlaps, and
		// add these in sorted order.  this will be moderately complicated.

		for(s32 s=0; s<stitchPolys[e].size(); s++)
		{
			TStitchPoly * pStitchPoly = stitchPolys[e][s];

			const Vector3 & vExternalVec0 = pStitchPoly->m_NewVertices[0];
			const Vector3 & vExternalVec1 = pStitchPoly->m_NewVertices[1];
			Vector3 vExternalEdgeVecNonUnit = vExternalVec1 - vExternalVec0;
			Vector3 vExternalEdgeVec = vExternalEdgeVecNonUnit;
			Vector3 vExternalEdgeVec2d(vExternalEdgeVec.x, vExternalEdgeVec.y, 0.0f);
			vExternalEdgeVec.Normalize();
			vExternalEdgeVec2d.Normalize();

			// Iterate through all edge polygons in DLC navmesh, identify and process those who have edge(s) adjoining this external edge
			for(u32 p=0; p<pDLCNavMesh->GetNumPolys(); p++)
			{
				TNavMeshPoly * pDLCPoly = pDLCNavMesh->GetPoly(p);
				if(!pDLCPoly->GetLiesAlongEdgeOfMesh())
					continue;

				for(u32 a=0; a<pDLCPoly->GetNumVertices(); a++)
				{
					const TAdjPoly & dlcEdge = pDLCNavMesh->GetAdjacentPoly( pDLCPoly->GetFirstVertexIndex() + a );
					Vector3 vDLCVert0, vDLCVert1;
					pDLCNavMesh->GetVertex( pDLCNavMesh->GetPolyVertexIndex( pDLCPoly, a ), vDLCVert0 );
					pDLCNavMesh->GetVertex( pDLCNavMesh->GetPolyVertexIndex( pDLCPoly, (a+1) % pDLCPoly->GetNumVertices() ), vDLCVert1 );
					Vector3 vDLCEdge = vDLCVert1 - vDLCVert0;
					Vector3 vDLCEdge2d(vDLCEdge.x, vDLCEdge.y, 0.0f);
					if(vDLCEdge.Mag2() <= SMALL_FLOAT)
						continue;
					vDLCEdge.Normalize();
					vDLCEdge2d.Normalize();

					// NOTE! We swap the ordering of this edge, so it is pointing *towards* the DLC navmesh (oppsite to the external edge)
					SwapEm(vDLCVert0, vDLCVert1);

					// We want to filter through only edges which are right up at the edge of the navmesh

					// Immediately rule out any edges which are connected in any way.
					// We create DLC navmeshes without the rest of the map being present, so any connections will indicate either
					// an existing adjacency within the same navmesh, or to a neighbouring DLC navmesh.
					// Either way - it rules out this polygon being on the exterion edge of the mesh.

					if(dlcEdge.GetNavMeshIndex(pDLCNavMesh->GetAdjacentMeshes()) != NAVMESH_NAVMESH_INDEX_NONE)
						continue;

					// Ensure that this edge is colinear to the vExternalEdge in the XY, and its winding is opposite
					const float fDot = (vExternalEdgeVec2d.x * vDLCEdge2d.x) + (vExternalEdgeVec2d.y * vDLCEdge2d.y); 
					if(fDot < 0.99f)
						continue;

					// Ensure that both verts are along the vExternalEdgeVec; avoids locating adjacent edges at the wrong height (above or below)
					static dev_float fSegDistEps = 0.5f;
					if( geomDistances::Distance2LineToPoint(vExternalVec0, vExternalEdgeVecNonUnit, vDLCVert0) > fSegDistEps*fSegDistEps)
						continue;
					if( geomDistances::Distance2LineToPoint(vExternalVec0, vExternalEdgeVecNonUnit, vDLCVert1) > fSegDistEps*fSegDistEps)
						continue;

					// Ensure that this edge is close to the edge of the mesh
					static dev_float fMaxDistFromEdge = 0.125f;
					if(iAdjacentSide==eMinusX || iAdjacentSide==ePlusX)
					{
						if( !IsClose(vDLCVert0.x, fEdgeCoord, fMaxDistFromEdge) || !IsClose(vDLCVert1.x, fEdgeCoord, fMaxDistFromEdge) )
							continue;
					}
					else
					{
						if( !IsClose(vDLCVert0.y, fEdgeCoord, fMaxDistFromEdge) || !IsClose(vDLCVert1.y, fEdgeCoord, fMaxDistFromEdge) )
							continue;
					}

					float fOverlapInterval;
					if( ComputeOverlapInterval(vExternalVec0, vExternalVec1, vDLCVert0, vDLCVert1, fOverlapInterval) )
					{
						static dev_float fEps = 0.1f;
						if( fOverlapInterval < -fEps )
						{
							// Determine cases where the DLC edge extends outside of the non-DLC edge, either in
							// the positive/negative directions, or both.
							// Adjust edge vertices to fit the extents of the non-DLC edge (we will later insert new
							// vertices into the DLC navmesh to ensure adjacencies are exact)

							switch(iAdjacentSide)
							{
								case eMinusX:
								{
									vDLCVert0.y = Max(vDLCVert0.y, vExternalVec1.y);
									vDLCVert1.y = Min(vDLCVert1.y, vExternalVec0.y);
									break;
								}
								case ePlusX:
								{
									vDLCVert1.y = Max(vDLCVert1.y, vExternalVec0.y);
									vDLCVert0.y = Min(vDLCVert0.y, vExternalVec1.y);
									break;
								}
								case eMinusY:
								{
									vDLCVert0.x = Max(vDLCVert0.x, vExternalVec1.x);
									vDLCVert1.x = Min(vDLCVert1.x, vExternalVec0.x);
									break;
								}
								case ePlusY:
								{
									vDLCVert1.x = Max(vDLCVert1.x, vExternalVec0.x);
									vDLCVert0.x = Min(vDLCVert0.x, vExternalVec1.x);
									break;
								}
							}

							// Do we already have a stitch poly for this DLC edge poly?
							TStitchPoly * pExisting = GetStitchPolyRepresentingNavMeshPoly(DLC_edgePolys, p);

							if(!pExisting)
							{
								// If not, create one now
								pExisting = new TStitchPoly();
								DLC_edgePolys.PushAndGrow(pExisting);

								pExisting->m_iIndexInNavMesh = p;
								pExisting->m_pOriginalPoly = pDLCPoly;

								for(u32 w=0; w<pDLCPoly->GetNumVertices(); w++)
								{
									Vector3 vert;
									pDLCNavMesh->GetVertex( pDLCNavMesh->GetPolyVertexIndex(pDLCPoly, w), vert );
									pExisting->m_NewVertices.PushAndGrow(vert);

									const TAdjPoly & adj = pDLCNavMesh->GetAdjacentPoly( pDLCPoly->GetFirstVertexIndex() + w );
									pExisting->m_NewEdges.PushAndGrow(adj);
								}
							}

							TStitchPoly::TAdjacentDLCPoly adjDLCPoly(vDLCVert0, vDLCVert1, pDLCPoly, pExisting, a, fOverlapInterval);
							pStitchPoly->m_AdjacentDLCPolys.PushAndGrow(adjDLCPoly);
						}
					}
				}
			}
		}

		// We now have for each TStitchPoly, an external edge which will connect to the non-DLC navmesh, and we have identified
		// all the polygons in the DLC navmesh which we need to connect to.

		// The next step is to sort the polygon connections and create the adjacencies.  This includes creating NULL adjacencies
		// in the event of there being none.
		for(s32 s=0; s<stitchPolys[e].size(); s++)
		{
			TStitchPoly * pStitchPoly = stitchPolys[e][s];

			if(!pStitchPoly->m_AdjacentDLCPolys.GetCount())
			{
				stitchPolys[e].Delete(s);
				s--;
				continue;
			}

			switch(iAdjacentSide)
			{
				case eMinusX:
					pStitchPoly->m_AdjacentDLCPolys.QSort(0, -1, CompareEdgeMinusX);
					break;
				case ePlusX:
					pStitchPoly->m_AdjacentDLCPolys.QSort(0, -1, CompareEdgePlusX);
					break;
				case eMinusY:
					pStitchPoly->m_AdjacentDLCPolys.QSort(0, -1, CompareEdgeMinusY);
					break;
				case ePlusY:
					pStitchPoly->m_AdjacentDLCPolys.QSort(0, -1, CompareEdgePlusY);
					break;
			}

			// Now create the adjacencies
			// TODO: For now I am not going to bother about the case where there is a polygon gap in the DLC navmesh at the edge;

			for(u32 p=0; p<(u32)pStitchPoly->m_AdjacentDLCPolys.GetCount(); p++)
			{
				const TStitchPoly::TAdjacentDLCPoly & adjacentDLCPoly = pStitchPoly->m_AdjacentDLCPolys[p];
				u32 iPolyIndex = pDLCNavMesh->GetPolyIndex(adjacentDLCPoly.m_pPoly);

				TAdjPoly adjToDLC;	// I call it adjToDLC, but of course this poly will exist within the DLC navmesh - so its an internal adjacency really
				adjToDLC.SetAdjacencyType(ADJACENCY_TYPE_NORMAL);
				adjToDLC.SetNavMeshIndex(pDLCNavMesh->GetIndexOfMesh(), pDLCNavMesh->GetAdjacentMeshes() );
				adjToDLC.SetOriginalNavMeshIndex(pDLCNavMesh->GetIndexOfMesh(), pDLCNavMesh->GetAdjacentMeshes() );
				adjToDLC.SetPolyIndex(iPolyIndex);
				adjToDLC.SetOriginalPolyIndex(iPolyIndex);

				pStitchPoly->m_NewEdges.PushAndGrow(adjToDLC);

				pStitchPoly->m_NewVertices.PushAndGrow(adjacentDLCPoly.m_vVert2);
			}

			// Hmm.  Handle a special case where we created a polygon with exactly two adjacencies - one being the external edge to the main map,
			// and the other being a polygon in the DLC navmesh.  Obviously we can't have a polygon with 2 vertices, so handle this with a hack which
			// inserts a small unconnected edge.  We have to modify one of the vertices slightly to do this otherwise we'll recieve exceptions
			// due to the zero length edge.

			if( 0 ) //pStitchPoly->m_NewEdges.size() == 2)
			{
				//Assert(false);

				/*
				const float fModifyAmt = fShortEdge;

				// One caveat: the length of this polygon has to be greater than the amount we are going to modify the vertex by;
				// If not then just abandon this join altogether!
				Vector3 vEdgeLengthVec = pStitchPoly->m_NewVertices[1] - pStitchPoly->m_NewVertices[0];
				if(vEdgeLengthVec.Mag() < fModifyAmt * 2.0f)
				{
					stitchPolys[e].erase( stitchPolys[e].begin() + s);
					s--;
					continue;
				}

				// The vertex we're going to modify
				Vector3 & vModifyVert = pStitchPoly->m_NewVertices[ pStitchPoly->m_NewVertices.size()-1 ];
				// Using this vector
				Vector3 vModifyVec = pStitchPoly->m_NewVertices[2] - pStitchPoly->m_NewVertices[1];
				vModifyVec.Normalize();
				// Adjust slightly
				vModifyVert -= vModifyVec * fModifyAmt;

				// And insert a blank adjacency, so that we now have a triangle!
				TAdjPoly adjBlank;
				adjBlank.SetAdjacencyType(ADJACENCY_TYPE_NORMAL);
				adjBlank.SetNavMeshIndex(NAVMESH_NAVMESH_INDEX_NONE, pDLCNavMesh->GetAdjacentMeshes() );
				adjBlank.SetOriginalNavMeshIndex(NAVMESH_NAVMESH_INDEX_NONE, pDLCNavMesh->GetAdjacentMeshes() );
				adjBlank.SetPolyIndex(NAVMESH_POLY_INDEX_NONE);
				adjBlank.SetOriginalPolyIndex(NAVMESH_POLY_INDEX_NONE);

				pStitchPoly->m_NewEdges.PushAndGrow(adjBlank);
				*/
			}

			else
			{
				// We will have duplicated a vertex at the end of the new poly (because the vert2 of the final adj poly will have
				// identical to the first vertex of the external edge).
				// Remove this vertex.  (now, we might get an assert if there would have been a gap here - that's ok, see comment above)

				pStitchPoly->m_NewVertices.Delete( pStitchPoly->m_NewVertices.GetCount()-1 );
			}

#if __ASSERT
		//	const Vector3 vVertDiff = pStitchPoly->m_NewVertices[pStitchPoly->m_NewVertices.GetCount()-1] - pStitchPoly->m_NewVertices[0];
		//	Assert( vVertDiff.XYMag() < 0.1f );	// might assert - thats ok
		//	Assert( Abs(vVertDiff.z) < 0.5f );	// might assert - thats ok
			Assert( pStitchPoly->m_NewVertices.GetCount() == pStitchPoly->m_NewEdges.size()); // shouldn't ever assert!
#endif

#if __ASSERT
			float fArea = CalcPolyAreaXY( pStitchPoly->m_NewVertices );
			Assert(fArea <= 0.1f);
#endif
		}

		// Right!  Now we have the list of TStitchPoly's which connect the main-map navmesh to the DLC navmesh.  These will become the
		// zero-area triangles which ensure that the DLC navmesh's external adjacency will *exactly* connect with the main map.
		// However we now need to insert vertices & adjacencies all along the DLC navmesh's external polygons to ensure that they'll
		// line up perfectly with the stitch polys.

		// Iterate all the polygons at the edge of the DLC navmesh (these are not the zero-area polys)
		for(u32 p=0; p<(u32)DLC_edgePolys.GetCount(); p++)
		{
			TStitchPoly * pEdgePoly = DLC_edgePolys[p];

			Vector3 vPolyNormal = VEC3_ZERO;
			float fPolyNormalD = 0.0f;

			bool bGotNormal = pDLCNavMesh->GetPolyNormal(vPolyNormal, pEdgePoly->m_pOriginalPoly, 0.99f);
			Assert(bGotNormal);

			if(bGotNormal)
			{
				Vector3 vPolyVertex;
				pDLCNavMesh->GetVertex( pDLCNavMesh->GetPolyVertexIndex(pEdgePoly->m_pOriginalPoly, 0), vPolyVertex );
				fPolyNormalD = - DotProduct( vPolyNormal, vPolyVertex );
			}

RESTART_FOR_DLCPOLY:

			// If we reach the maximum number of vertices in a poly, we'll just have to live with a discontinuity..
			if(pEdgePoly->m_NewVertices.GetCount() >= NAVMESHPOLY_MAX_NUM_VERTICES-1)
				continue;

			for(u32 a=0; a<(u32)pEdgePoly->m_NewVertices.GetCount(); a++)
			{
				// If the DLC navmesh has a connection at this point, then it cannot be an external edge
				// TODO : Use the 'm_bExternalEdge' flag in TAdjPoly.
				const TAdjPoly & dlcEdge = pEdgePoly->m_NewEdges[a];
				if(dlcEdge.GetNavMeshIndex(pDLCNavMesh->GetAdjacentMeshes())!=NAVMESH_NAVMESH_INDEX_NONE)
					continue;

				// Visit each external edge
				const Vector3 & v1 = pEdgePoly->m_NewVertices[a];
				const Vector3 & v2 = pEdgePoly->m_NewVertices[(a+1)%pEdgePoly->m_NewVertices.GetCount()];
				Vector3 vEdgeNonUnit = v2-v1;
				Vector3 vEdge = vEdgeNonUnit;
				float fEdgeLen = vEdge.Mag();
				vEdge.Normalize();

				// Go through all our zero-area stitch polys
				// Identify all vertices which lie along vEdge
				for(s32 s=0; s<stitchPolys[e].size(); s++)
				{
					TStitchPoly * pStitchPoly = stitchPolys[e][s];
					for(u32 v=0; v<(u32)pStitchPoly->m_NewVertices.GetCount(); v++)
					{
						// Does this lie along the edge?
						Vector3 vTJuncVert = pStitchPoly->m_NewVertices[v];					

						// Test the distance along the edge of the point
						static dev_float fDistEps = 0.05f;
						const float fDotAlongEdge = DotProduct( vEdge, vTJuncVert - v1 );
						if( fDotAlongEdge < fDistEps || fDotAlongEdge > (fEdgeLen-fDistEps) )
							continue;

						// Ensure that this vertex is close to the edge of the mesh
						static dev_float fMaxDistFromEdge = 0.125f;
						if(iAdjacentSide==eMinusX || iAdjacentSide==ePlusX)
						{
							if( !IsClose(vTJuncVert.x, fEdgeCoord, fMaxDistFromEdge) )
								continue;
						}
						else
						{
							if( !IsClose(vTJuncVert.y, fEdgeCoord, fMaxDistFromEdge) )
								continue;
						}

						// Now test that it is exactly on the edge segment
						static dev_float fSegEps = 0.5f;
						const float fDistSqrFromEdgeSeg = geomDistances::Distance2LineToPoint(v1, vEdgeNonUnit, vTJuncVert);
						if(fDistSqrFromEdgeSeg > fSegEps*fSegEps)
							continue;

						// If so we have to add a new vertex here into pEdgePoly,
						// and also a new adjacency; We insert it after index 'a'

						// We need to make sure that the vertex lies exactly in the plane of the polygon it is being added to
						if(bGotNormal)
						{
							float fDistFromPlane = DotProduct( vPolyNormal, vTJuncVert) + fPolyNormalD;
							vTJuncVert -= vPolyNormal * fDistFromPlane;
						}
						

						TAdjPoly dupEdge = dlcEdge;
						pEdgePoly->m_NewVertices.Insert(a+1) = vTJuncVert;
						pEdgePoly->m_NewEdges.insert( pEdgePoly->m_NewEdges.begin()+a+1, dupEdge );

						goto RESTART_FOR_DLCPOLY;
					}
				}
			}
		}
	}

	//---------------------------------------------------------------------------------------------------

	// Now we are ready to recreate the DLC navmesh.
	// We need to do several things:
	//  1) replace all edge polygons with their replacements from the 'DLC_edgePolys' arrays
	//  2) append our zero-area polygons which stitch between the DLC and non-DLC navmeshes

	atArray<Vector3> vertices;
	atArray<TAdjPoly> adjacencies;
	atArray<TNavMeshPoly> polys;
	atArray<u16> indices;


	u32 iPoolIndex = 0;

	for(u32 p=0; p<pDLCNavMesh->GetNumPolys(); p++)
	{
		const TNavMeshPoly * pNavMeshPoly = pDLCNavMesh->GetPoly(p);
		TNavMeshPoly newPoly = *pNavMeshPoly;

		newPoly.SetLiesAlongEdgeOfMesh(false);

		TStitchPoly * pNewEdgePoly = GetStitchPolyRepresentingNavMeshPoly( DLC_edgePolys, p);
		if( pNewEdgePoly )
		{
			Assert(pNewEdgePoly->m_NewVertices.GetCount() == pNewEdgePoly->m_NewEdges.GetCount() && pNewEdgePoly->m_NewVertices.GetCount() >= 3);

			newPoly.SetFirstVertexIndex(iPoolIndex);
			newPoly.SetNumVertices(pNewEdgePoly->m_NewVertices.GetCount());

			// Add vertices and adjacencies..
			for(s32 i=0; i<pNewEdgePoly->m_NewVertices.GetCount(); i++)
			{
				// Vertex
				Vector3 vert = pNewEdgePoly->m_NewVertices[i];
				s32 index = GetVertexIndex( vertices, vert, fSameVertexEps );
				if(index == -1)
				{
					indices.PushAndGrow( (u16)vertices.GetCount() );
					vertices.PushAndGrow( vert );
				}
				else
				{
					indices.PushAndGrow( (u16)index );
				}

				// Adjacency
				adjacencies.PushAndGrow( pNewEdgePoly->m_NewEdges[i] );
			}
		}
		else
		{
			newPoly.SetFirstVertexIndex(iPoolIndex);

			// Add vertices and adjacencies..
			for(s32 i=0; i<(s32)pNavMeshPoly->GetNumVertices(); i++)
			{
				// Vertex
				Vector3 vert;
				pDLCNavMesh->GetVertex( pDLCNavMesh->GetPolyVertexIndex(pNavMeshPoly, i), vert);

				s32 index = GetVertexIndex( vertices, vert, fSameVertexEps );
				if(index == -1)
				{
					indices.PushAndGrow( (u16)vertices.GetCount() );
					vertices.PushAndGrow( vert );
				}
				else
				{
					indices.PushAndGrow( (u16)index );
				}

				// Adjacency
				const TAdjPoly & adj = pDLCNavMesh->GetAdjacentPoly(pNavMeshPoly->GetFirstVertexIndex()+i);
				adjacencies.PushAndGrow(adj);
			}
		}

		polys.PushAndGrow(newPoly);
		iPoolIndex += newPoly.GetNumVertices();
	}

	// Now add all the zero-area TStitchPolys which adjoin the DLC navmesh to the main-map navmesh

	for(u32 e=0; e<4; e++)
	{
		for(u32 p=0; p<(u32)stitchPolys[e].GetCount(); p++)
		{
			TStitchPoly * pStitchPoly = stitchPolys[e][p];

			Assert(pStitchPoly->m_NewVertices.GetCount() == pStitchPoly->m_NewEdges.GetCount()); // && pStitchPoly->m_NewVertices.GetCount() >= 3);

			const TAdjPoly & externalAdj = pStitchPoly->m_NewEdges[0];
			CNavMesh * pExternalAdjNavMesh = NULL;
			for(u32 n=0; n<4; n++)
			{
				if(ppAdjacentMainMapNavmeshes[n] && ppAdjacentMainMapNavmeshes[n]->GetIndexOfMesh()==externalAdj.GetNavMeshIndex(pDLCNavMesh->GetAdjacentMeshes()))
				{
					pExternalAdjNavMesh = ppAdjacentMainMapNavmeshes[n];
					break;
				}
			}
			Assert(pExternalAdjNavMesh);
			if(!pExternalAdjNavMesh)
				continue;

			TNavMeshPoly * pExternalAdjPoly = pExternalAdjNavMesh->GetPoly( externalAdj.GetPolyIndex() );

			TNavMeshPoly newPoly = *pExternalAdjPoly;
			newPoly.SetNavMeshIndex(pDLCNavMesh->GetIndexOfMesh());
			newPoly.SetFirstVertexIndex(iPoolIndex);
			newPoly.SetNumVertices(pStitchPoly->m_NewVertices.GetCount());
			newPoly.SetNumSpecialLinks(0);
			newPoly.SetSpecialLinksStartIndex(0);
			newPoly.SetIsSmall(true);
			newPoly.SetIsLarge(false);

			newPoly.m_Struct3.m_bZeroAreaStichPolyDLC = true;
			newPoly.SetLiesAlongEdgeOfMesh(true);

			// Add vertices and adjacencies..
			for(s32 i=0; i<pStitchPoly->m_NewVertices.GetCount(); i++)
			{
				// Vertex
				Vector3 vert = pStitchPoly->m_NewVertices[i];
				s32 index = GetVertexIndex( vertices, vert, fSameVertexEps );
				if(index == -1)
				{
					indices.PushAndGrow( (u16)vertices.GetCount() );
					vertices.PushAndGrow( vert );
				}
				else
				{
					indices.PushAndGrow( (u16)index );
				}

				// Adjacency
				const TAdjPoly & adjacency = pStitchPoly->m_NewEdges[i];

				adjacencies.PushAndGrow( adjacency );

				//---------------------------------------------------------------------------------------
				// We need to link back the adjacency from the adjacent polygon, to this stitch polygon

				// Internal link ?  Links to DLC polys should already be established *from* stitch poly inwards
				if( adjacency.GetNavMeshIndex( pDLCNavMesh->GetAdjacentMeshes() ) == pDLCNavMesh->GetIndexOfMesh() )
				{
					const Vector3 & v1 = pStitchPoly->m_NewVertices[i];
					const Vector3 & v2 = pStitchPoly->m_NewVertices[(i+1)%pStitchPoly->m_NewVertices.GetCount()];

					const u32 iAdjacentPoly = adjacency.GetPolyIndex();
					Assert( iAdjacentPoly < (u32)polys.GetCount() );
					const TNavMeshPoly & adjacentPoly = polys[iAdjacentPoly];

					// We've located the DLC poly; now iterate its edges to find the one which exactly matches
					for(u32 j=0; j<adjacentPoly.GetNumVertices(); j++)
					{
						TAdjPoly & linkBackAdjacency = adjacencies[ adjacentPoly.GetFirstVertexIndex() + j ];
						if(linkBackAdjacency.GetNavMeshIndex( pDLCNavMesh->GetAdjacentMeshes() ) == NAVMESH_NAVMESH_INDEX_NONE )
						{
							const Vector3 vLinkBackV1 = vertices[ indices[adjacentPoly.GetFirstVertexIndex() + j] ];
							const Vector3 vLinkBackV2 = vertices[ indices[adjacentPoly.GetFirstVertexIndex() + ((j+1)%adjacentPoly.GetNumVertices())] ];

							// Need to be a little more lenient on proximity tests; 5cm should do
							// If we find the wrong adjacencies are matched up, we'll need to tighten this up with an interval test or something.
							static dev_float fEps = 0.05f * 0.05f; //fSameVertexEps*fSameVertexEps;

							bool bCloseAB = ((v1 - vLinkBackV2).XYMag2() < fEps) && ((v2 - vLinkBackV1).XYMag2() < fEps);
							bool bCloseBA = ((v1 - vLinkBackV1).XYMag2() < fEps) && ((v2 - vLinkBackV2).XYMag2() < fEps);

							if( bCloseAB || bCloseBA )
							{
								linkBackAdjacency.SetAdjacencyType( ADJACENCY_TYPE_NORMAL );
								linkBackAdjacency.SetNavMeshIndex( pDLCNavMesh->GetIndexOfMesh(), pDLCNavMesh->GetAdjacentMeshes() );
								linkBackAdjacency.SetOriginalNavMeshIndex( pDLCNavMesh->GetIndexOfMesh(), pDLCNavMesh->GetAdjacentMeshes() );
								linkBackAdjacency.SetPolyIndex( polys.GetCount() );
								linkBackAdjacency.SetOriginalPolyIndex( polys.GetCount() );
								break;
							}
						}
					}
				}
			}

			polys.PushAndGrow(newPoly);
			iPoolIndex += newPoly.GetNumVertices();
		}
	}

	//-----------------------------------------------------------------------------------------------------------------------------

	Assert(indices.GetCount() == (s32)iPoolIndex);
	Assert(adjacencies.GetCount() == (s32)iPoolIndex);

	CNavMeshCompressedVertex * pCompressedVerts = new CNavMeshCompressedVertex[vertices.GetCount()];
	for(u32 v=0; v<(u32)vertices.GetCount(); v++)
	{
		const Vector3 & vert = vertices[v];
		CompressVertex(
			vert,
			pCompressedVerts[v].data[0], pCompressedVerts[v].data[1], pCompressedVerts[v].data[2],
			pDLCNavMesh->GetNonResourcedData()->m_vMinOfNavMesh,
			pDLCNavMesh->GetExtents()
		);
	}

	//-----------------------------------------------------------------------------------------------------------------------------

	// Make sure to mark this navmesh as DLC
	pDLCNavMesh->SetFlags( pDLCNavMesh->GetFlags() | NAVMESH_DLC );

	pDLCNavMesh->m_iNumPolys = polys.GetCount();
	pDLCNavMesh->m_iNumVertices = vertices.GetCount();
	pDLCNavMesh->m_iSizeOfPools = iPoolIndex;

	delete pDLCNavMesh->m_PolysArray;
	pDLCNavMesh->m_PolysArray = new aiSplitArray<TNavMeshPoly,NAVMESH_POLY_ARRAY_BLOCKSIZE>(pDLCNavMesh->m_iNumPolys, polys.GetElements());
	pDLCNavMesh->SetPolysSubArrayIndices();

	delete pDLCNavMesh->m_VertexIndexArray;
	pDLCNavMesh->m_VertexIndexArray = new aiSplitArray<u16,NAVMESH_VERTEXINDEX_ARRAY_BLOCKSIZE>(pDLCNavMesh->m_iSizeOfPools, indices.GetElements());

	delete pDLCNavMesh->m_AdjacentPolysArray;
	pDLCNavMesh->m_AdjacentPolysArray = new aiSplitArray<TAdjPoly,NAVMESH_ADJPOLY_ARRAY_BLOCKSIZE>(pDLCNavMesh->m_iSizeOfPools, adjacencies.GetElements());

	delete pDLCNavMesh->m_CompressedVertexArray;
	pDLCNavMesh->m_CompressedVertexArray = new aiSplitArray<CNavMeshCompressedVertex, NAVMESH_COMPRESSEDVERTEX_ARRAY_BLOCKSIZE>(pDLCNavMesh->m_iNumVertices, (CNavMeshCompressedVertex*)pCompressedVerts);

	// We need to destroy & recreate the quadtree
	// First of all through we need to extract all the coverpoints into a list
	vector<CNavMeshCoverPoint*> coverPoints;
	ExtractCoverPointsIntoList(pDLCNavMesh, pDLCNavMesh->GetQuadTree(), coverPoints, false);

	CNavGen::MakeNavMeshQuadTree(pDLCNavMesh, pDLCNavMesh->GetQuadTree()->m_Mins, pDLCNavMesh->GetQuadTree()->m_Maxs);

	// After recreating the quadtree, we add the coverpoints back in
	for(u32 c=0; c<coverPoints.size(); c++)
	{
		CNavMeshCoverPoint coverPoint = *coverPoints[c];

		Vector3 vPosition;
		DecompressVertex(vPosition, coverPoint.m_iX, coverPoint.m_iY, coverPoint.m_iZ, pDLCNavMesh->m_pQuadTree->m_Mins, pDLCNavMesh->m_vExtents);

		AddCoverPointToNavMeshQuadTree(pDLCNavMesh->m_pQuadTree, coverPoint, vPosition);
	}

	// We have to set a member in every quadtree leaf which tells us what the index (within the navmesh) of the 1st coverpoint in that leaf is.
	g_iTotalNumCoverPointsCount = 0;
	SetIndexOfFirstCoverPointInQuadtreeLeaves(pDLCNavMesh->m_pQuadTree);
}


bool PolyHasAdjacencyTo(TNavMeshPoly * pPoly, CNavMesh * pNavMesh, u32 iOtherNavMesh)
{
	for(u32 v=0; v<pPoly->GetNumVertices(); v++)
	{
		const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly( pPoly->GetFirstVertexIndex()+v );
		if(adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes())==iOtherNavMesh)
		{
			return true;
		}
	}
	return false;
}



bool IsVertexUsed(u32 iVertex, CNavMesh * pNavMesh)
{
	for(u32 p=0; p<pNavMesh->GetNumPolys(); p++)
	{
		TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);

		for(u32 v=0; v<pPoly->GetNumVertices(); v++)
		{
			u16 iPolyVertex = pNavMesh->GetPolyVertexIndex(pPoly, v);
			if(iVertex == iPolyVertex)
				return true;
		}
	}
	return false;
}



}	// namespace rage

