#include "NavGen_Octree.h"
#include "NavGen.h"
#include "NavGen_Utils.h"

namespace rage
{

// TEST

// This define will cause all the recursive LOS tests to expand each node's min/max extents by
// the given vector 'vOctreeMinMaxOverEstimate' before testing for intersection.  This combats
// rare cases where an early-out bbox cull fails because the intersecting extents are too thin.

#define OVERESTIMATE_OCTREE_NODES

//Vector3 vOctreeMinMaxOverEstimate(1.0f, 1.0f, 1.0f);
const Vector3 vOctreeMinMaxOverEstimate(0.05f, 0.05f, 0.05f);
//Vector3 vOctreeMinMaxOverEstimate(0.5f, 0.5f, 0.5f);


#define OCTREE_PREFETCH(offset, base) _mm_prefetch((char*)base+offset,_MM_HINT_T0)


void
CLosIntersections::Add(const Vector3 & vPos, const float fDist, CNavGenTri * pTri)
{
	if(!m_Intersections.GetCount())
	{
		TLosIsect isect(vPos, fDist, pTri);
		m_Intersections.PushAndGrow(isect);
	}
	else
	{
		for(int v=0; v<m_Intersections.GetCount(); v++)
		{
			const TLosIsect & isect = m_Intersections[v];

			if(fDist < isect.m_fDist)
			{
				if(m_Intersections.GetCount()==LOS_INTERSECTIONS_MAXNUM)
					m_Intersections.Delete(m_Intersections.GetCount()-1);

				TLosIsect newisect(vPos, fDist, pTri);
				m_Intersections.Insert(v) = newisect;
				return;
			}
		}
		if(m_Intersections.GetCount() < LOS_INTERSECTIONS_MAXNUM)
		{
			TLosIsect newisect(vPos, fDist, pTri);
			m_Intersections.PushAndGrow(newisect);
		}
	}
}

void MinMaxFromPts(Vector3 & vMin, Vector3 & vMax, const Vector3 * pPts, const int iNumPts)
{
	for(int v=0; v<iNumPts; v++)
	{
		vMin.x = Min(vMin.x, pPts[v].x);
		vMin.y = Min(vMin.y, pPts[v].y);
		vMin.z = Min(vMin.z, pPts[v].z);

		vMax.x = Max(vMax.x, pPts[v].x);
		vMax.y = Max(vMax.y, pPts[v].y);
		vMax.z = Max(vMax.z, pPts[v].z);
	}
}

bool CClipVolume::InitExtrudedTri(const Vector3 * pPts, const float fExtrudeDist, const Vector3 * pExtrudeNormal)
{
	Vector3 vNormal;
	CalcTriNormal(pPts, vNormal);

	// Make sure we're pointing in the right direction!
	// Although a user-specified extrusion vec can be supplied, it should
	// be facing in the same approx direction as the triangle normal.

	if(pExtrudeNormal)
	{
		const float fDot = DotProduct(*pExtrudeNormal, vNormal);
		Assert(DotProduct(*pExtrudeNormal, vNormal) > 0.0f);

		if( fDot <= 0.0f )
			return false;
	}


	if(pExtrudeNormal)
		vNormal = *pExtrudeNormal;

#if __DEV
	Vector3 vCentre(0.0f,0.0f,0.0f);
	for(int v=0; v<3; v++)
	{
		vCentre += pPts[v];
		vCentre += (pPts[v] + (vNormal * fExtrudeDist));
	}
	vCentre /= 6.0f;
#endif

	m_iNumPlanes = 5;
	m_vMin = Vector3(FLT_MAX, FLT_MAX, FLT_MAX);
	m_vMax = Vector3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	Vector3 vTmpPts[3];
	Vector3 vExtrusionVec = vNormal * fExtrudeDist;

	// First plane is opposite to input tri
	vTmpPts[0] = pPts[2];
	vTmpPts[1] = pPts[1];
	vTmpPts[2] = pPts[0];
	PlaneFromPoints(vTmpPts, &m_Planes[0], m_fPlaneDists[0]);
	MinMaxFromPts(m_vMin, m_vMax, vTmpPts, 3);

	// Second plane is same as collision tri, but offset by extrusion vec
	vTmpPts[0] = pPts[0] + vExtrusionVec;
	vTmpPts[1] = pPts[1] + vExtrusionVec;
	vTmpPts[2] = pPts[2] + vExtrusionVec;
	PlaneFromPoints(vTmpPts, &m_Planes[1], m_fPlaneDists[1]);
	MinMaxFromPts(m_vMin, m_vMax, vTmpPts, 3);

	// Third plane
	vTmpPts[0] = pPts[0];
	vTmpPts[1] = pPts[1];
	vTmpPts[2] = pPts[1] + vExtrusionVec;
	PlaneFromPoints(vTmpPts, &m_Planes[2], m_fPlaneDists[2]);
	MinMaxFromPts(m_vMin, m_vMax, vTmpPts, 3);

	// Fourth plane
	vTmpPts[0] = pPts[1];
	vTmpPts[1] = pPts[2];
	vTmpPts[2] = pPts[2] + vExtrusionVec;
	PlaneFromPoints(vTmpPts, &m_Planes[3], m_fPlaneDists[3]);
	MinMaxFromPts(m_vMin, m_vMax, vTmpPts, 3);

	// Fifth plane
	vTmpPts[0] = pPts[2];
	vTmpPts[1] = pPts[0];
	vTmpPts[2] = pPts[0] + vExtrusionVec;
	PlaneFromPoints(vTmpPts, &m_Planes[4], m_fPlaneDists[4]);
	MinMaxFromPts(m_vMin, m_vMax, vTmpPts, 3);

	m_MinMax.SetFloat(m_vMin.x - MINMAX_RESOLUTION, m_vMin.y - MINMAX_RESOLUTION, m_vMin.z - MINMAX_RESOLUTION, m_vMax.x + MINMAX_RESOLUTION, m_vMax.y + MINMAX_RESOLUTION, m_vMax.z + MINMAX_RESOLUTION);

#if __DEV
	// Make sure that the centre of the volume is BEHIND all 5 planes
	for(int p=0; p<5; p++)
	{
		float fDist = DotProduct(m_Planes[p], vCentre) + m_fPlaneDists[p];
		Assert(fDist < 0.0f);
	}
#endif

	return true;
}


void
CClipVolume::InitExtrudedQuad(const Vector3 * pPts, const float fExtrudeDist, const Vector3 * pExtrudeNormal)
{
	Vector3 vNormal;
	CalcTriNormal(pPts, vNormal);

	// Make sure we're pointing in the right direction!
	// Although a user-specified extrusion vec can be supplied, it should
	// be facing in the same approx direction as the triangle normal.
#if __DEV
	// Make sure this quad is completely flat.
	Vector3 vNormal2;
	CalcTriNormal(&pPts[1], vNormal2);
	Assert(DotProduct(vNormal, vNormal2) > 0.707f);

	if(pExtrudeNormal)
	{
		Assert(DotProduct(*pExtrudeNormal, vNormal) > 0.0f);
	}
#endif

	if(pExtrudeNormal)
		vNormal = *pExtrudeNormal;

#if __DEV
	Vector3 vCentre(0.0f,0.0f,0.0f);
	for(int v=0; v<4; v++)
	{
		vCentre += pPts[v];
		vCentre += (pPts[v] + vNormal);
	}
	vCentre /= 8.0f;
#endif

	m_iNumPlanes = 6;
	m_vMin = Vector3(FLT_MAX, FLT_MAX, FLT_MAX);
	m_vMax = Vector3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	Vector3 vTmpPts[4];
	Vector3 vExtrusionVec = vNormal * fExtrudeDist;

	// First plane is opposite to input quad
	vTmpPts[0] = pPts[3];
	vTmpPts[1] = pPts[2];
	vTmpPts[2] = pPts[1];
	vTmpPts[3] = pPts[0];
	PlaneFromPoints(vTmpPts, &m_Planes[0], m_fPlaneDists[0]);
	MinMaxFromPts(m_vMin, m_vMax, vTmpPts, 4);

	// Second plane is same as collision quad, but offset by extrusion vec
	vTmpPts[0] = pPts[0] + vExtrusionVec;
	vTmpPts[1] = pPts[1] + vExtrusionVec;
	vTmpPts[2] = pPts[2] + vExtrusionVec;
	vTmpPts[3] = pPts[3] + vExtrusionVec;
	PlaneFromPoints(vTmpPts, &m_Planes[1], m_fPlaneDists[1]);
	MinMaxFromPts(m_vMin, m_vMax, vTmpPts, 4);

	// Third plane
	vTmpPts[0] = pPts[0];
	vTmpPts[1] = pPts[1];
	vTmpPts[2] = pPts[1] + vExtrusionVec;
	vTmpPts[3] = pPts[0] + vExtrusionVec;
	PlaneFromPoints(vTmpPts, &m_Planes[2], m_fPlaneDists[2]);
	MinMaxFromPts(m_vMin, m_vMax, vTmpPts, 4);

	// Fourth plane
	vTmpPts[0] = pPts[1];
	vTmpPts[1] = pPts[2];
	vTmpPts[2] = pPts[2] + vExtrusionVec;
	vTmpPts[3] = pPts[1] + vExtrusionVec;
	PlaneFromPoints(vTmpPts, &m_Planes[3], m_fPlaneDists[3]);
	MinMaxFromPts(m_vMin, m_vMax, vTmpPts, 4);

	// Fifth plane
	vTmpPts[0] = pPts[2];
	vTmpPts[1] = pPts[3];
	vTmpPts[2] = pPts[3] + vExtrusionVec;
	vTmpPts[3] = pPts[2] + vExtrusionVec;
	PlaneFromPoints(vTmpPts, &m_Planes[4], m_fPlaneDists[4]);
	MinMaxFromPts(m_vMin, m_vMax, vTmpPts, 4);

	// Sixth plane
	vTmpPts[0] = pPts[3];
	vTmpPts[1] = pPts[0];
	vTmpPts[2] = pPts[0] + vExtrusionVec;
	vTmpPts[3] = pPts[3] + vExtrusionVec;
	PlaneFromPoints(vTmpPts, &m_Planes[5], m_fPlaneDists[5]);
	MinMaxFromPts(m_vMin, m_vMax, vTmpPts, 4);

	m_MinMax.SetFloat(m_vMin.x, m_vMin.y, m_vMin.z, m_vMax.x, m_vMax.y, m_vMax.z);

#if __DEV
	// Make sure that the centre of the volume is BEHIND all 6 planes
	for(int p=0; p<6; p++)
	{
		float fDist = DotProduct(m_Planes[p], vCentre) + m_fPlaneDists[p];
		Assert(fDist < 0.0f);
	}
#endif
}


// Test whether the given triangle intersects the volume at all
// Returns TRUE in the case of an intersection, and FALSE if none
// IF pClosestIntersection is non-null then it will be returned holding
// the closest intersection with m_Planes[0].  That is, the closest
// intersection to the triangle/quad used to initialise this volume.

#define CLIPVOLUME_MAX_POINTS		128

bool
CClipVolume::TestTriIntersection(const Vector3 * pTriPts, Vector3 * pClosestIntersection, float * fClosestIntersectionDist) const
{
	Vector3 vPtsBufferA[CLIPVOLUME_MAX_POINTS];
	Vector3 vPtsBufferB[CLIPVOLUME_MAX_POINTS];

	Vector3 * pInPts = vPtsBufferA;
	Vector3 * pOutPts = vPtsBufferB;
	int iNumInPts = 3;
	int iNumOutPts = 0;

	vPtsBufferA[0] = pTriPts[0];
	vPtsBufferA[1] = pTriPts[1];
	vPtsBufferA[2] = pTriPts[2];

	static const float fEps = 0.0f;
	for(int pl=0; pl<m_iNumPlanes; pl++)
	{
		const Vector3 & vClipPlane = m_Planes[pl];
		const float fPlaneDist = m_fPlaneDists[pl];

		int lastv = iNumInPts-1;
		float fLastDist = DotProduct(vClipPlane, pInPts[lastv]) + fPlaneDist;

		for(int v=0; v<iNumInPts; v++)
		{
			const float fDist = DotProduct(vClipPlane, pInPts[v]) + fPlaneDist;

			if(fLastDist < fEps)
			{
				pOutPts[iNumOutPts++] = pInPts[lastv];

				Assert(iNumOutPts < CLIPVOLUME_MAX_POINTS);
				if(iNumOutPts >= CLIPVOLUME_MAX_POINTS)
					return false;
			}

			if(SIGN(fLastDist) != SIGN(fDist))
			{
				float s = (fLastDist / (fLastDist - fDist));
				Vector3 vClipPos = pInPts[lastv] + ((pInPts[v] - pInPts[lastv]) * s);
				pOutPts[iNumOutPts++] = vClipPos;

				Assert(iNumOutPts < CLIPVOLUME_MAX_POINTS);
				if(iNumOutPts >= CLIPVOLUME_MAX_POINTS)
					return false;
			}

			fLastDist = fDist;
			lastv = v;
		}

		// Input triangle was clipped to nothing by one of the planes - there can be no intersection.
		if(!iNumOutPts)
			return false;

		// Swap inputs & outputs
		Vector3 * pTmp = pInPts;
		pInPts = pOutPts;
		pOutPts = pTmp;

		iNumInPts = iNumOutPts;
		iNumOutPts = 0;
	}

	if(pClosestIntersection || fClosestIntersectionDist)
	{
		Vector3 vClosestPos;
		float fClosestDist = FLT_MAX;
		for(int v=0; v<iNumInPts; v++)
		{
			float fTestDist = - (DotProduct(m_Planes[0], pInPts[v]) + m_fPlaneDists[0]);
			if(fTestDist < fClosestDist && fTestDist > 0.0f)	// test > 0.0f as a failsafe
			{
				vClosestPos = pInPts[v];
				fClosestDist = fTestDist;
			}
		}
		if(pClosestIntersection)
			*pClosestIntersection = vClosestPos;
		if(fClosestIntersectionDist)
			*fClosestIntersectionDist = fClosestDist;
	}

	return true;
}


//***************************************************************************************************
//	CNavGenOctreeNodeBase
//	Base class from which to derive octree nodes & leaves
//***************************************************************************************************

CNavGenOctreeNodeBase::CNavGenOctreeNodeBase(void)
{

}
CNavGenOctreeNodeBase::~CNavGenOctreeNodeBase(void)
{

}

//***************************************************************************************************
//	CNavGenOctreeNode
//	Octree node
//***************************************************************************************************

CNavGenOctreeNode::CNavGenOctreeNode(void)
	: CNavGenOctreeNodeBase()
{
	m_Flags = 0;

	int i;
	for(i=0; i<8; i++)
	{
		m_ChildNodes[i] = NULL;
	}
}

CNavGenOctreeNode::~CNavGenOctreeNode(void)
{

}

//***************************************************************************************************
//	CNavGenOctreeLeaf
//	Octree leaf
//***************************************************************************************************

CNavGenOctreeLeaf::CNavGenOctreeLeaf(void)
	: CNavGenOctreeNodeBase()
{
	m_Flags = NAVGEN_OCTREE_FLAG_ISLEAF;

//	m_pNodeMap = NULL;

#ifdef DEBUG_OCTREE_CONSTRUCTION
	m_pFragments = NULL;
#endif

#ifdef VISUALISE_NAVIGATION_SURFACE
	m_pNavSurfaceVertexBuffer = NULL;
	m_iNumTrianglesInVB = 0;
#endif
}

CNavGenOctreeLeaf::~CNavGenOctreeLeaf(void)
{
#ifdef DEBUG_OCTREE_CONSTRUCTION
	// Only delete the fragments (if they exist).  The actual triangles are stored/deleted centrally.
	if(m_pFragments)
	{
		for(int p=0; p<m_pFragments->size(); p++)
		{
			CSplitPoly * pPoly = (*m_pFragments)[p];
			delete pPoly;
		}

		delete m_pFragments;
		m_pFragments = NULL;
	}
#endif

#ifdef VISUALISE_NAVIGATION_SURFACE
	if(m_pNavSurfaceVertexBuffer)
	{
		m_pNavSurfaceVertexBuffer->Release();
		m_pNavSurfaceVertexBuffer = NULL;
	}
#endif
}



//***************************************************************************************************
//	CNavGenOctree
//	Octree main data structure
//***************************************************************************************************

const u32 CNavGenOctree::ms_iMajorFileVersion = 1;
const u32 CNavGenOctree::ms_iMinorFileVersion = 1;

CNavGenOctree::CNavGenOctree(void)
{
	m_pRootNode = NULL;
//	m_iDesiredNumTrisInEachLeaf		= 64;
//	m_fDesiredLeafNodeSize			= 64.0f;

	m_iDesiredNumTrisInEachLeaf		= 256;
	m_fDesiredLeafNodeSize			= 12.5f;	//50.0f;	//25.0f;

	m_iTotalNumInternalNodes = 0;
	m_iTotalNumLeaves = 0;
	m_iMaximumDepth = 0;
	m_bAlsoStoreSplitPolysInOctreeLeaves = false;

	m_iCurrentTimeStamp = 0;

	m_pHitTri = NULL;
	m_vHitPos = Vector3(0.0f,0.0f,0.0f);
	m_fClosestHitDistSqr = FLT_MAX;

	m_iLosFlagsToTest = 0;
	m_bIgnoreSeeThrough = false;
	m_bIgnoreFixed = false;

	m_iNumBatchedLosTests = 0;
}

CNavGenOctree::~CNavGenOctree(void)
{
	Shutdown();
}





// Saves out the octree
bool
CNavGenOctree::SaveOctree(char * pFilename, TColTriArray * pCollisionTriangles, u32 iCRCofTriFile)
{
	if(!m_pRootNode)
		return false;

	FILE * filePtr = fopen(pFilename, "wb");

	// Write a header
	char header[] = "NGENTREE";
	fwrite(header, 1, 8, filePtr);

	// Write the major file version
	fwrite(&ms_iMajorFileVersion, sizeof(u32), 1, filePtr);
	// Write the minor file version
	fwrite(&ms_iMinorFileVersion, sizeof(u32), 1, filePtr);

	// Write the CRC of tri file which this octree was generated from
	fwrite(&iCRCofTriFile, sizeof(u32), 1, filePtr);

	// Write the root node's min & max
	fwrite(&m_RootNodeMin.x, sizeof(float), 1, filePtr);
	fwrite(&m_RootNodeMin.y, sizeof(float), 1, filePtr);
	fwrite(&m_RootNodeMin.z, sizeof(float), 1, filePtr);
	fwrite(&m_RootNodeMax.x, sizeof(float), 1, filePtr);
	fwrite(&m_RootNodeMax.y, sizeof(float), 1, filePtr);
	fwrite(&m_RootNodeMax.z, sizeof(float), 1, filePtr);

	SaveOctreeR(m_pRootNode, pCollisionTriangles, filePtr);

	fflush(filePtr);
	fclose(filePtr);

	return true;
}

void
CNavGenOctree::SaveOctreeR(CNavGenOctreeNodeBase * pNodeBase, TColTriArray * pCollisionTriangles, FILE * filePtr)
{
	// Write the node flags (u32)
	fwrite(&pNodeBase->m_Flags, 4, 1, filePtr);

	//***********************************************************
	//
	//	Write a leaf node
	//
	//***********************************************************

	if(pNodeBase->m_Flags & NAVGEN_OCTREE_FLAG_ISLEAF)
	{
		CNavGenOctreeLeaf * pLeaf = (CNavGenOctreeLeaf*)pNodeBase;

		// Write the leaf node's min/max's
		fwrite(&pLeaf->m_NodeMin.x, sizeof(float), 1, filePtr);
		fwrite(&pLeaf->m_NodeMin.y, sizeof(float), 1, filePtr);
		fwrite(&pLeaf->m_NodeMin.z, sizeof(float), 1, filePtr);
		fwrite(&pLeaf->m_NodeMax.x, sizeof(float), 1, filePtr);
		fwrite(&pLeaf->m_NodeMax.y, sizeof(float), 1, filePtr);
		fwrite(&pLeaf->m_NodeMax.z, sizeof(float), 1, filePtr);

		// write number of triangles
		u32 numTris = (u32)pLeaf->m_Triangles.size();
		fwrite(&numTris, sizeof(u32), 1, filePtr);

		// write triangles as indices into the pCollisionTriangles list
		for(u32 t=0; t<pLeaf->m_Triangles.size(); t++)
		{
			CNavGenTri * pTri = pLeaf->m_Triangles[t];
			u32 index = pTri->m_iIndexInCollisionPolysList;
			fwrite(&index, sizeof(u32), 1, filePtr);
		}

		return;
	}

	//***********************************************************
	//
	//	Write an internal node
	//
	//***********************************************************

	CNavGenOctreeNode * pNode = (CNavGenOctreeNode*)pNodeBase;

	for(int o=0; o<8; o++)
	{
		SaveOctreeR(pNode->m_ChildNodes[o], pCollisionTriangles, filePtr);
	}
}


// Loads the octree
bool
CNavGenOctree::LoadOctree(char * pFilename, TColTriArray * pCollisionTriangles, u32 * iCRCtoMatch)
{
	FILE * filePtr = fopen(pFilename, "rb");

	if(!filePtr)
	{
		return false;
	}

	// Read the header
	char header[] = "NGENTREE";
	char buffer[9];

	fread(buffer, 1, 8, filePtr);
	buffer[8] = 0;

	if(strcmp(header, buffer)!=0)
	{
		fclose(filePtr);
		// Error, the header didn't match
		return false;
	}

	// Read the major file version
	u32 iMajorFileVersion;
	fread(&iMajorFileVersion, sizeof(u32), 1, filePtr);

	// Read the minor file version
	u32 iMinorFileVersion;
	fread(&iMinorFileVersion, sizeof(u32), 1, filePtr);

	// Can we read this version ?
	if(iMajorFileVersion != ms_iMajorFileVersion || iMinorFileVersion != ms_iMinorFileVersion)
	{
		fclose(filePtr);
		return false;
	}

	// Read the CRC of the tgri file which this octree was generated from.  This should match the optional CRC passed in to this function,
	// which can ensure that the .oct file has been generated from a specific matching .tri file!
	// If one is passed in, and found not to match - then we will abort loading the octree & the calling code can regenerate a new octree
	// to match the collision triangles.
	u32 iFileCRC;
	fread(&iFileCRC, sizeof(u32), 1, filePtr);
	if(iCRCtoMatch && (*iCRCtoMatch != iFileCRC))
	{
		fclose(filePtr);
		return false;
	}

	// Read the min/max's of the root node
	fread(&m_RootNodeMin.x, sizeof(float), 1, filePtr);
	fread(&m_RootNodeMin.y, sizeof(float), 1, filePtr);
	fread(&m_RootNodeMin.z, sizeof(float), 1, filePtr);
	fread(&m_RootNodeMax.x, sizeof(float), 1, filePtr);
	fread(&m_RootNodeMax.y, sizeof(float), 1, filePtr);
	fread(&m_RootNodeMax.z, sizeof(float), 1, filePtr);

	// Load the octree
	m_pRootNode = LoadOctreeR(pCollisionTriangles, filePtr);

	fclose(filePtr);

	return true;
}


CNavGenOctreeNodeBase *
CNavGenOctree::LoadOctreeR(TColTriArray * pCollisionTriangles, FILE * filePtr)
{
	// Read node flags
	u32 nodeFlags;
	fread(&nodeFlags, 4, 1, filePtr);

	// Is this a leaf
	if(nodeFlags & NAVGEN_OCTREE_FLAG_ISLEAF)
	{
		CNavGenOctreeLeaf * pLeaf = rage_new CNavGenOctreeLeaf();
		pLeaf->m_Flags = nodeFlags;

		// Read leaf node's min & max
		Vector3 vMin, vMax;
		fread(&vMin.x, sizeof(float), 1, filePtr);
		fread(&vMin.y, sizeof(float), 1, filePtr);
		fread(&vMin.z, sizeof(float), 1, filePtr);
		fread(&vMax.x, sizeof(float), 1, filePtr);
		fread(&vMax.y, sizeof(float), 1, filePtr);
		fread(&vMax.z, sizeof(float), 1, filePtr);

		pLeaf->m_NodeMin = vMin;
		pLeaf->m_NodeMax = vMax;

		// Read num tris
		u32 numTris;
		fread(&numTris, sizeof(u32), 1, filePtr);

		for(u32 t=0; t<numTris; t++)
		{
			u32 index;
			fread(&index, sizeof(u32), 1, filePtr);

			CNavGenTri * pTri = &(*pCollisionTriangles)[index];
			pLeaf->m_Triangles.push_back(pTri);
		}

		return pLeaf;
	}

	CNavGenOctreeNode * pNode = rage_new CNavGenOctreeNode();
	pNode->m_Flags = nodeFlags;

	for(int o=0; o<8; o++)
	{
		pNode->m_ChildNodes[o] = LoadOctreeR(pCollisionTriangles, filePtr);
	}

	return pNode;
}


// Helper function which will return one of the above enumeration values, from a combination of planeside flags
s32
OctantEnumFromPlaneSideFlags(u32 flags)
{
	if(flags & PLANESIDE_POSZ)
	{
		if(flags & PLANESIDE_POSY)
		{
			if(flags & PLANESIDE_POSX)
			{
				return POSX_POSY_POSZ;
			}
			else
			{
				return NEGX_POSY_POSZ;
			}
		}
		else
		{
			if(flags & PLANESIDE_POSX)
			{
				return POSX_NEGY_POSZ;
			}
			else
			{
				return NEGX_NEGY_POSZ;
			}
		}
	}
	else
	{
		if(flags & PLANESIDE_POSY)
		{
			if(flags & PLANESIDE_POSX)
			{
				return POSX_POSY_NEGZ;
			}
			else
			{
				return NEGX_POSY_NEGZ;
			}
		}
		else
		{
			if(flags & PLANESIDE_POSX)
			{
				return POSX_NEGY_NEGZ;
			}
			else
			{
				return NEGX_NEGY_NEGZ;
			}
		}
	}
}





// Creates the octree from the input list of triangles
bool CNavGenOctree::Init(const Vector3 & UNUSED_PARAM(vMeshMin), const Vector3 & UNUSED_PARAM(vMeshMax), TColTriArray * pTriangles)
{
	m_iDesiredNumTrisInEachLeaf		= pTriangles->size() / 64;
	m_fDesiredLeafNodeSize			= 12.5f;	//25.0f;	//25.0f;

//	float fSize = vMeshMax.x - vMeshMin.x;
//	fSize = max(fSize, vMeshMax.y - vMeshMin.y);
//	m_iDesiredNumTrisInEachLeaf		= 128;
//	m_fDesiredLeafNodeSize			= fSize / 10.0f;


	// For the octree construction we use the class CSplitPoly
	// Firstly then, we need to create a CSplitPoly for every triangle in the input list
	atDList<CSplitPoly*> Polys;
	TColTriArray & triList = *pTriangles;

	static const float fVeryLargeVal = 1000000.0f;
	Vector3 vMin(fVeryLargeVal, fVeryLargeVal, fVeryLargeVal);
	Vector3 vMax(-fVeryLargeVal, -fVeryLargeVal, -fVeryLargeVal);

	s32 t,p;
	for(t=0; t<pTriangles->size(); t++)
	{
		CNavGenTri * pTri = &triList[t];
		CSplitPoly * pPoly = rage_new CSplitPoly();

		if(!pPoly)
		{
			OutOfMemory("CNavGenOctree::Init");
			return false;
		}

		for(p=0; p<3; p++)
		{
			if(pTri->m_vPts[p].x < vMin.x) vMin.x = pTri->m_vPts[p].x;
			if(pTri->m_vPts[p].y < vMin.y) vMin.y = pTri->m_vPts[p].y;
			if(pTri->m_vPts[p].z < vMin.z) vMin.z = pTri->m_vPts[p].z;
			if(pTri->m_vPts[p].x > vMax.x) vMax.x = pTri->m_vPts[p].x;
			if(pTri->m_vPts[p].y > vMax.y) vMax.y = pTri->m_vPts[p].y;
			if(pTri->m_vPts[p].z > vMax.z) vMax.z = pTri->m_vPts[p].z;

			pPoly->m_Pts.PushAndGrow(pTri->m_vPts[p]);
		}

		pPoly->m_pParentTriangle = pTri;

		atDNode<CSplitPoly*> * pNode = rage_new atDNode<CSplitPoly*>(pPoly);
		Polys.Append(*pNode);
	}

	// Ensure that octree is not zero-thickness in any dimension
	Vector3 vSize = vMax - vMin;
	if(Eq(vSize.x, 0.0f, 0.1f))
	{
		vMin.x -= 0.5f;
		vMax.x += 0.5f;
	}
	if(Eq(vSize.y, 0.0f, 0.1f))
	{
		vMin.y -= 0.5f;
		vMax.y += 0.5f;
	}
	if(Eq(vSize.z, 0.0f, 0.1f))
	{
		vMin.z -= 0.5f;
		vMax.z += 0.5f;
	}

	m_RootNodeMin = vMin;
	m_RootNodeMax = vMax;

	// Now construct the octree
	m_pRootNode = ConstructR(NULL, -1, &Polys, vMin, vMax, 0);

	return true;
}

// Deletes the octree
bool
CNavGenOctree::Shutdown(void)
{
	if(m_pRootNode)
	{
		ShutdownR(m_pRootNode);
		m_pRootNode = NULL;
	}

	return true;
}

void
CNavGenOctree::ShutdownR(CNavGenOctreeNodeBase * pNodeBase)
{
	if(pNodeBase->m_Flags & NAVGEN_OCTREE_FLAG_ISLEAF)
	{
		CNavGenOctreeLeaf * pLeaf = (CNavGenOctreeLeaf*)pNodeBase;
		delete pLeaf;
		return;
	}

	CNavGenOctreeNode * pNode = (CNavGenOctreeNode*)pNodeBase;
	for(int o=0; o<8; o++)
	{
		if(pNode->m_ChildNodes[o])
		{
			ShutdownR(pNode->m_ChildNodes[o]);
		}
	}

	delete pNode;
}


CNavGenOctreeNodeBase *
CNavGenOctree::ConstructR(CNavGenOctreeNode * pParentNode, s32 iOctantThisIs, atDList<CSplitPoly*> * pPolys, const Vector3 & vNodeMin, const Vector3 & vNodeMax, u32 iDepth)
{
	atDList<CSplitPoly*> & polyList = *pPolys;
	atDNode<CSplitPoly*> * pListNode = polyList.GetHead();

	m_ConstructStruct.iNumPolys = 0;
	while(pListNode)
	{
		m_ConstructStruct.iNumPolys++;
		pListNode = pListNode->GetNext();
	}


	if(iDepth > m_iMaximumDepth)
		m_iMaximumDepth = iDepth;

	// Have we met the criteria to stop descending, and create a leaf ?
//	g_OctreeStatic.fMaxDimension = CMaths::Max(vNodeMax.x - vNodeMin.x, vNodeMax.y - vNodeMin.y);
//	g_OctreeStatic.fMaxDimension = CMaths::Max(g_OctreeStatic.fMaxDimension, vNodeMax.z - vNodeMin.z);

	Vector3 vSize = vNodeMax - vNodeMin;
	bool bSmallEnough = (vSize.x < m_fDesiredLeafNodeSize && vSize.y < m_fDesiredLeafNodeSize && vSize.z < m_fDesiredLeafNodeSize);

	if(m_ConstructStruct.iNumPolys <= m_iDesiredNumTrisInEachLeaf || bSmallEnough)	//g_OctreeStatic.fMaxDimension <= m_fDesiredLeafNodeSize)
	{
		CNavGenOctreeLeaf * pLeaf = rage_new CNavGenOctreeLeaf();
		if(!pLeaf)
		{
			OutOfMemory("CNavGenOctree::ConstructR");
			return NULL;
		}

		m_iTotalNumLeaves++;

		// Timestamp for this leaf!  Mark any polys which arrived in this leaf with this timestamp, so if
		// multiple fragments from one poly arrived here - we only store one reference to the original!
		m_iCurrentTimeStamp++;

		if(pParentNode)
		{
			pParentNode->m_ChildNodes[iOctantThisIs] = pLeaf;
		}

		pLeaf->m_NodeMin = Vector3(FLT_MAX, FLT_MAX, FLT_MAX);
		pLeaf->m_NodeMax = Vector3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

		// Go through all split polys which have arrived in this leaf, and add the original 'CNavGenTri' for each one.
		// It is possible that multiple fragments from the same triangle may arrive in one leaf, so check for uniqueness.
		// Calculate the leaf's extents based on the actual bounds of the fragments - not upon the min/max passed down the tree
#ifdef DEBUG_OCTREE_CONSTRUCTION
		if(m_bAlsoStoreSplitPolysInOctreeLeaves)
		{
			pLeaf->m_pFragments = rage_new vector<CSplitPoly*>;
		}
#endif

		s32 v;
		CNavGenTri * pTri;
		CSplitPoly * pPoly;
		
		pListNode = polyList.GetHead();
		while(pListNode)
		{
			pPoly = pListNode->Data;

			// Update leaf extents
			for(v=0; v<pPoly->m_Pts.GetCount(); v++)
			{
				Vector3 pt = pPoly->m_Pts[v];
				if(pt.x < pLeaf->m_NodeMin.x) pLeaf->m_NodeMin.x = pt.x;
				if(pt.y < pLeaf->m_NodeMin.y) pLeaf->m_NodeMin.y = pt.y;
				if(pt.z < pLeaf->m_NodeMin.z) pLeaf->m_NodeMin.z = pt.z;
				if(pt.x > pLeaf->m_NodeMax.x) pLeaf->m_NodeMax.x = pt.x;
				if(pt.y > pLeaf->m_NodeMax.y) pLeaf->m_NodeMax.y = pt.y;
				if(pt.z > pLeaf->m_NodeMax.z) pLeaf->m_NodeMax.z = pt.z;
			}

		#ifdef DEBUG_OCTREE_CONSTRUCTION
			if(m_bAlsoStoreSplitPolysInOctreeLeaves)
			{
				pLeaf->m_pFragments->push_back(pPoly);
			}
		#endif

			pTri = pPoly->m_pParentTriangle;

			// Only add this origial pTri collision triangle to polys list
			if(pTri->m_iTimeStamp != m_iCurrentTimeStamp)
			{
				pLeaf->m_Triangles.push_back(pTri);
				pTri->m_iTimeStamp = m_iCurrentTimeStamp;
			}
			
		#ifdef DEBUG_OCTREE_CONSTRUCTION
			if(!m_bAlsoStoreSplitPolysInOctreeLeaves)
			{
				delete pPoly;
			}
		#else

			delete pPoly;

		#endif

			atDNode<CSplitPoly*> * pThisNode = pListNode;
			pListNode = pListNode->GetNext();
			delete pThisNode;
		}

		return pLeaf;
	}

	// This will be a node rather than a leaf.  Create the node and calculate the seperating planes.  The input list
	// of polys will now be split against each plane, and each fragment classified in one of eight octants.
	// We then go through and recurse.

	m_ConstructStruct.vCenter = (vNodeMin + vNodeMax) * 0.5f;

	// Up plane
	m_ConstructStruct.fPlaneDists[0] = m_ConstructStruct.vCenter.z;
	// Right plane
	m_ConstructStruct.fPlaneDists[1] = m_ConstructStruct.vCenter.x;
	// Forwards plane
	m_ConstructStruct.fPlaneDists[2] = m_ConstructStruct.vCenter.y;

	// Create the node
	CNavGenOctreeNode * pNode = rage_new CNavGenOctreeNode();
	if(!pNode)
	{
		OutOfMemory("CNavGenOctree::ConstructR");
		return NULL;
	}

	m_iTotalNumInternalNodes++;

	if(pParentNode)
	{
		pParentNode->m_ChildNodes[iOctantThisIs] = pNode;
	}

	// Stack-allocated data
	atDList<CSplitPoly*> octantPolyLists[8];
	s32 o;

	// Firstly split all input polys against ALL of our 3 planes, and accumulate the fragments
	// whilst flagging which side of all 3 planes each poly fragment exists in
	// Afterwards we will go ahead and create each octant from the relevant polys
	{
		s32 pl, p;

		// Clear the side flags for all polys in the list
		int iCount=0;
		pListNode = polyList.GetHead();
		while(pListNode)
		{
			CSplitPoly * pPoly = pListNode->Data;
			pPoly->m_PlaneSideFlags = 0;
			pListNode = pListNode->GetNext();
			iCount++;
		}

		// Repeat for each plane
		for(pl=0; pl<3; pl++)
		{
			int iListSize = 0;
			pListNode = polyList.GetHead();
			while(pListNode)
			{
				pListNode = pListNode->GetNext();
				iListSize++;
			}

			pListNode = polyList.GetHead();

			for(p=0; p<iListSize; p++)
			{
				CSplitPoly * pPoly = pListNode->Data;
				CSplitPoly * pFrontFragment = NULL;
				CSplitPoly * pBackFragment = NULL;

				ePlaneSide planeSide = ClassifyPolyToAxialPlane(pPoly, m_ConstructStruct.iPlaneFlags[pl], m_ConstructStruct.fPlaneDists[pl]);

				if(planeSide == ePlaneFront)
				{
					pFrontFragment = pPoly;
				}
				else if(planeSide == ePlaneBack)
				{
					pBackFragment = pPoly;
				}
				else
				{
					SplitPolyToAxialPlane(pPoly, m_ConstructStruct.iPlaneFlags[pl], m_ConstructStruct.fPlaneDists[pl], 0.0f, &pFrontFragment, &pBackFragment);
					pPoly->m_bNeedsDeleted = true;
				}

				// Poly was split to front of this plane
				if(pFrontFragment)
				{
					// store sidedness flag
					pFrontFragment->m_PlaneSideFlags = pPoly->m_PlaneSideFlags;
					pFrontFragment->m_PlaneSideFlags |= m_ConstructStruct.iPlaneFlags[pl];
					pFrontFragment->m_pParentTriangle = pPoly->m_pParentTriangle;

					atDNode<CSplitPoly*> * pNewNode = rage_new atDNode<CSplitPoly*>(pFrontFragment);
					polyList.Append(*pNewNode);
				}
				// Poly was split to back of this plane
				if(pBackFragment)
				{
					// store sidedness flag, shifted up by one to indicate negative
					pBackFragment->m_PlaneSideFlags = pPoly->m_PlaneSideFlags;
					pBackFragment->m_PlaneSideFlags |= (m_ConstructStruct.iPlaneFlags[pl] << 1);
					pBackFragment->m_pParentTriangle = pPoly->m_pParentTriangle;

					atDNode<CSplitPoly*> * pNewNode = rage_new atDNode<CSplitPoly*>(pBackFragment);
					polyList.Append(*pNewNode);
				}
				// Poly was split to nothing.  This is an error, and can only happen if a poly
				// has become degenerate (coplanar polys are returned as front fragments).
				if(!pFrontFragment && !pBackFragment)
				{
					ErrorMsg("CNavGenOctree::ConstructR - split poly generated no fragments!");
					break;
				}

				pListNode = pListNode->GetNext();
			}

			// We've now split all polys to that plane.  Now go through list and delete all polys up until the original 'iListSize'
			while(iListSize)
			{
				//pListNode = polyList.GetHead();
				pListNode = polyList.PopHead();
				CSplitPoly * pPoly = pListNode->Data;
				if(pPoly->m_bNeedsDeleted)
					delete pPoly;
				delete pListNode;

				//atDNode<CSplitPoly*>* pListNodeToDelete = polyList.PopHead();
				//delete pListNodeToDelete;

				iListSize--;
			}
		}

		pListNode = polyList.GetHead();
		while(pListNode)
		{
			CSplitPoly * pPoly = pListNode->Data;

			// Do a quick sanity-check that no poly exists on both sides of any plane..
			Assert(!((pPoly->m_PlaneSideFlags & PLANESIDE_POSX)&&(pPoly->m_PlaneSideFlags & PLANESIDE_NEGX)));
			Assert(!((pPoly->m_PlaneSideFlags & PLANESIDE_POSY)&&(pPoly->m_PlaneSideFlags & PLANESIDE_NEGY)));
			Assert(!((pPoly->m_PlaneSideFlags & PLANESIDE_POSZ)&&(pPoly->m_PlaneSideFlags & PLANESIDE_NEGZ)));

			int oct = OctantEnumFromPlaneSideFlags(pPoly->m_PlaneSideFlags);

			atDNode<CSplitPoly*> * pNewNode = rage_new atDNode<CSplitPoly*>(pPoly);
			octantPolyLists[oct].Append(*pNewNode);

			atDNode<CSplitPoly*> * pThisNode = pListNode;
			pListNode = pListNode->GetNext();

			delete pThisNode;
		}

		polyList.Empty();
	}

	// now make the octants
	for(o=0; o<8; o++)
	{
		Vector3 vOctantMin, vOctantMax;
		GetOctantExtents(o, vNodeMin, vNodeMax, vOctantMin, vOctantMax);

		ConstructR(
			pNode,
			o,
			&octantPolyLists[o],
			vOctantMin,
			vOctantMax,
			iDepth+1
		);
	}

	return pNode;
}


void
CNavGenOctree::GetOctantExtents(u32 oct, const Vector3 & vParentMin, const Vector3 & vParentMax, Vector3 & vOctantMin, Vector3 & vOctantMax) const
{
	Vector3 vCenter = (vParentMin + vParentMax) * 0.5f;
	Vector3 vSize = vParentMax - vCenter;

	switch(oct)
	{
		case POSX_POSY_POSZ:
			vOctantMin = vCenter;
			vOctantMax = vParentMax;
			break;
		case POSX_NEGY_POSZ:
			vOctantMin = vCenter;
			vOctantMin.y -= vSize.y;
			vOctantMax = vParentMax;
			vOctantMax.y -= vSize.y; 
			break;
		case NEGX_NEGY_POSZ:
			vOctantMin = vParentMin;
			vOctantMin.z += vSize.z;
			vOctantMax = vCenter;
			vOctantMax.z += vSize.z;
			break;
		case NEGX_POSY_POSZ:
			vOctantMin = vCenter;
			vOctantMin.x -= vSize.x;
			vOctantMax = vParentMax;
			vOctantMax.x -= vSize.x;
			break;

		case POSX_POSY_NEGZ:
			vOctantMin = vCenter;
			vOctantMin.z -= vSize.z;
			vOctantMax = vParentMax;
			vOctantMax.z -= vSize.z;
			break;
		case POSX_NEGY_NEGZ:
			vOctantMin = vCenter;
			vOctantMin.y -= vSize.y;
			vOctantMin.z -= vSize.z;
			vOctantMax = vParentMax;
			vOctantMax.y -= vSize.y; 
			vOctantMax.z -= vSize.z;
			break;
		case NEGX_NEGY_NEGZ:
			vOctantMin = vParentMin;
			vOctantMax = vCenter;
			break;
		case NEGX_POSY_NEGZ:
			vOctantMin = vCenter;
			vOctantMin.x -= vSize.x;
			vOctantMin.z -= vSize.z;
			vOctantMax = vParentMax;
			vOctantMax.x -= vSize.x;
			vOctantMax.z -= vSize.z;
			break;
	}
}

//******************************************************************************************************

int CNavGenOctree::FindAllTrianglesInVolume(CNavGenTri** pTriArrayOut, int maxNumTris, const CClipVolume& volume)
{
	if(!m_pRootNode)
	{
		Assert(0);
		return 0;
	}

	m_iCurrentTimeStamp++;

	return FindAllTrianglesInVolumeR(pTriArrayOut, maxNumTris, m_pRootNode, volume, m_RootNodeMin, m_RootNodeMax);
}


int CNavGenOctree::FindAllTrianglesInVolumeR(CNavGenTri** pTriArrayOut, int maxNumTris,
		const CNavGenOctreeNodeBase* pNodeBase,
		const CClipVolume& volume, const Vector3& vNodeMin, const Vector3& vNodeMax) const
{
	int numFound = 0;

	// If we've reached a leaf, test the triangles within
	if(pNodeBase->m_Flags & NAVGEN_OCTREE_FLAG_ISLEAF)
	{
		CNavGenOctreeLeaf * pLeaf = (CNavGenOctreeLeaf*) pNodeBase;

#ifdef OVERESTIMATE_OCTREE_NODES
		if(BBoxIntersect(volume.GetMin(), volume.GetMax(), pLeaf->m_NodeMin - vOctreeMinMaxOverEstimate, pLeaf->m_NodeMax + vOctreeMinMaxOverEstimate))
#else
		if(BBoxIntersect(volume.GetMin(), volume.GetMax(), pLeaf->m_NodeMin, pLeaf->m_NodeMax))
#endif
		{
			u32 p;
			Vector3 vIntersect;

			for(p=0; p<pLeaf->m_Triangles.size(); p++)
			{
				CNavGenTri * pTri = pLeaf->m_Triangles[p];

				if(pTri->m_iTimeStamp == m_iCurrentTimeStamp)
					continue;

				if(!pTri->m_colPolyData.TestCollisionTypeFlags(m_iCollisionTypesToTest))
					continue;

				pTri->m_iTimeStamp = m_iCurrentTimeStamp;

				if(!volume.GetMinMax().Intersects(pTri->m_MinMax))
					continue;

				if(volume.TestTriIntersection(pTri->m_vPts))
				{
					if(Verifyf(numFound < maxNumTris, "Filled up array when finding triangles within volume."))
					{
						pTriArrayOut[numFound++] = pTri;
					}
				}
			}
		}
	}
	else
	{
		CNavGenOctreeNode * pNode = (CNavGenOctreeNode*) pNodeBase;

#ifdef OVERESTIMATE_OCTREE_NODES
		if(BBoxIntersect(volume.GetMin(), volume.GetMax(), vNodeMin - vOctreeMinMaxOverEstimate, vNodeMax + vOctreeMinMaxOverEstimate))
#else
		if(BBoxIntersect(volume.GetMin(), volume.GetMax(), vNodeMin, vNodeMax))
#endif
		{
			s32 o;
			for(o=0; o<8; o++)
			{
				// Get the min/max's for this octant based upon the parent node's extents
				Vector3 vOctantMin, vOctantMax;
				GetOctantExtents(o, vNodeMin, vNodeMax, vOctantMin, vOctantMax);

				CNavGenOctreeNodeBase * pChildNode = pNode->m_ChildNodes[o];

				if(pChildNode)
				{
					numFound += FindAllTrianglesInVolumeR(pTriArrayOut + numFound, maxNumTris - numFound, pChildNode, volume, vOctantMin, vOctantMax);
					Assert(numFound <= maxNumTris);		// This one is expected to hold up even if we fill up the array.
				}
			}
		}
	}

	return numFound;
}


//******************************************************************************************************

bool CNavGenOctree::TestVolumeIsClear(const CClipVolume & volume, const u32 iCollisionTypeFlags, bool bIgnoreClimbableObjects)
{
	if(!m_pRootNode)
	{
		Assert(0);
		return false;
	}

	m_iCurrentTimeStamp++;

	bool bIsClear = TestVolumeIsClearR(m_pRootNode, volume, m_RootNodeMin, m_RootNodeMax, iCollisionTypeFlags, bIgnoreClimbableObjects);

	return bIsClear;
}

bool
CNavGenOctree::TestVolumeIsClearR(
	CNavGenOctreeNodeBase * pNodeBase,
	const CClipVolume & volume,
	Vector3 & vNodeMin,
	Vector3 & vNodeMax,
	const u32 iCollisionTypeFlags,
	bool bIgnoreClimbableObjects)
{
	// If we've reached a leaf, test the triangles within
	if(pNodeBase->m_Flags & NAVGEN_OCTREE_FLAG_ISLEAF)
	{
		CNavGenOctreeLeaf * pLeaf = (CNavGenOctreeLeaf*) pNodeBase;

#ifdef OVERESTIMATE_OCTREE_NODES
		if(BBoxIntersect(volume.GetMin(), volume.GetMax(), pLeaf->m_NodeMin - vOctreeMinMaxOverEstimate, pLeaf->m_NodeMax + vOctreeMinMaxOverEstimate))
#else
		if(BBoxIntersect(volume.GetMin(), volume.GetMax(), pLeaf->m_NodeMin, pLeaf->m_NodeMax))
#endif
		{
			u32 p;
			Vector3 vIntersect;

			for(p=0; p<pLeaf->m_Triangles.size(); p++)
			{
				CNavGenTri * pTri = pLeaf->m_Triangles[p];

				if(pTri->m_iTimeStamp == m_iCurrentTimeStamp)
					continue;

				pTri->m_iTimeStamp = m_iCurrentTimeStamp;

				if(!pTri->m_colPolyData.TestCollisionTypeFlags(iCollisionTypeFlags))
					continue;

				if(bIgnoreClimbableObjects && pTri->m_colPolyData.GetIsClimbableObject())
					continue;

				if(!volume.GetMinMax().Intersects(pTri->m_MinMax))
					continue;

				if(volume.TestTriIntersection(pTri->m_vPts))
				{
					return false;
				}
			}
		}

		return true;
	}
	else
	{
		CNavGenOctreeNode * pNode = (CNavGenOctreeNode*) pNodeBase;

#ifdef OVERESTIMATE_OCTREE_NODES
		if(BBoxIntersect(volume.GetMin(), volume.GetMax(), vNodeMin - vOctreeMinMaxOverEstimate, vNodeMax + vOctreeMinMaxOverEstimate))
#else
		if(BBoxIntersect(volume.GetMin(), volume.GetMax(), vNodeMin, vNodeMax))
#endif
		{
			s32 o;
			for(o=0; o<8; o++)
			{
				// Get the min/max's for this octant based upon the parent node's extents
				Vector3 vOctantMin, vOctantMax;
				GetOctantExtents(o, vNodeMin, vNodeMax, vOctantMin, vOctantMax);

				CNavGenOctreeNodeBase * pChildNode = pNode->m_ChildNodes[o];

				if(pChildNode)
				{
					bool bLos = TestVolumeIsClearR(pChildNode, volume, vOctantMin, vOctantMax, iCollisionTypeFlags, bIgnoreClimbableObjects);

					// As soon as an intersection is found we may return false
					if(!bLos)
					{
						return false;
					}
				}
			}
		}

		return true;
	}
}

//******************************************************************************************************



// Tests a line of sight.  Returns true if LOS is clear, or false if there was an intersection
bool
CNavGenOctree::TestLineOfSight(const Vector3 & vStart, const Vector3 & vEnd, const u32 iCollisionTypeFlags, bool bVertical, u32 iFlagsToTest, bool bIgnoreSeeThrough, bool bIgnoreFixed, bool bIgnoreClimbableObjects, bool bIgnoreStairs, bool bIgnoreRiverBound)
{
	if(!m_pRootNode)
	{
		return false;
	}

	m_iLosFlagsToTest = iFlagsToTest;
	m_bIgnoreSeeThrough = bIgnoreSeeThrough;
	m_bIgnoreFixed = bIgnoreFixed;
	m_bIgnoreClimbableObjects = bIgnoreClimbableObjects;
	m_bIgnoreStairs = bIgnoreStairs;
	m_bIgnoreRiverBound = bIgnoreRiverBound;
	m_iCollisionTypesToTest = iCollisionTypeFlags;

	m_vRayMins.x = Min(vStart.x, vEnd.x);
	m_vRayMins.y = Min(vStart.y, vEnd.y);
	m_vRayMins.z = Min(vStart.z, vEnd.z);
	m_vRayMaxs.x = Max(vStart.x, vEnd.x);
	m_vRayMaxs.y = Max(vStart.y, vEnd.y);
	m_vRayMaxs.z = Max(vStart.z, vEnd.z);

	m_NavGenPolyLosMinMax.SetFloat(m_vRayMins.x, m_vRayMins.y, m_vRayMins.z, m_vRayMaxs.x, m_vRayMaxs.y, m_vRayMaxs.z);
	m_NavGenPolyLosMinMax.Expand(NAVGEN_MINMAX_EXPAND_AMT);

	m_iCurrentTimeStamp++;

	// Recurse into the tree
	if(bVertical)
	{
		return TestLineOfSightVerticalR(m_pRootNode, m_RootNodeMin, m_RootNodeMax, vStart, vEnd);
	}
	else
	{
		return TestLineOfSightR(m_pRootNode, m_RootNodeMin, m_RootNodeMax, vStart, vEnd);
	}
}


// Tests a line of sight, starting from the specified node.
// Returns true if LOS is clear, or false if there was an intersection
bool
CNavGenOctree::TestLineOfSight(CNavGenOctreeNodeBase * pNodeBase, const Vector3 & vNodeMin, const Vector3 & vNodeMax, const Vector3 & vStart, const Vector3 & vEnd, const u32 iCollisionTypeFlags, u32 iFlagsToTest, bool bIgnoreSeeThrough, bool bIgnoreFixed, bool bIgnoreClimbableObjects, bool bIgnoreStairs, bool bIgnoreRiverBound)
{
	Vector3 vMin = vNodeMin;
	Vector3 vMax = vNodeMax;

	m_vRayMins.x = Min(vStart.x, vEnd.x);
	m_vRayMins.y = Min(vStart.y, vEnd.y);
	m_vRayMins.z = Min(vStart.z, vEnd.z);
	m_vRayMaxs.x = Max(vStart.x, vEnd.x);
	m_vRayMaxs.y = Max(vStart.y, vEnd.y);
	m_vRayMaxs.z = Max(vStart.z, vEnd.z);

	m_iCollisionTypesToTest = iCollisionTypeFlags;
	m_iLosFlagsToTest = iFlagsToTest;
	m_bIgnoreSeeThrough = bIgnoreSeeThrough;
	m_bIgnoreFixed = bIgnoreFixed;
	m_bIgnoreClimbableObjects = bIgnoreClimbableObjects;
	m_bIgnoreStairs = bIgnoreStairs;
	m_bIgnoreRiverBound = bIgnoreRiverBound;

	m_NavGenPolyLosMinMax.SetFloat(m_vRayMins.x, m_vRayMins.y, m_vRayMins.z, m_vRayMaxs.x, m_vRayMaxs.y, m_vRayMaxs.z);
	m_NavGenPolyLosMinMax.Expand(NAVGEN_MINMAX_EXPAND_AMT);

	m_iCurrentTimeStamp++;

	// Recurse into the tree
	return TestLineOfSightR(pNodeBase, vMin, vMax, vStart, vEnd);
}

bool CNavGenOctree::TestLineOfSightR(CNavGenOctreeNodeBase * pNodeBase, Vector3 & vNodeMin, Vector3 & vNodeMax, const Vector3 & vStart, const Vector3 & vEnd)
{
	// If we've reached a leaf, test the triangles within
	if(pNodeBase->m_Flags & NAVGEN_OCTREE_FLAG_ISLEAF)
	{
		CNavGenOctreeLeaf * pLeaf = (CNavGenOctreeLeaf*) pNodeBase;

#ifdef OVERESTIMATE_OCTREE_NODES
			if(BBoxIntersect(m_vRayMins, m_vRayMaxs, pLeaf->m_NodeMin - vOctreeMinMaxOverEstimate, pLeaf->m_NodeMax + vOctreeMinMaxOverEstimate))
#else
			if(BBoxIntersect(m_vRayMins, m_vRayMaxs, pLeaf->m_NodeMin, pLeaf->m_NodeMax))
#endif
		{
			u32 p;
			Vector3 vIntersect;

			for(p=0; p<pLeaf->m_Triangles.size(); p++)
			{
				CNavGenTri * pTri = pLeaf->m_Triangles[p];

				if(pTri->m_iTimeStamp == m_iCurrentTimeStamp)
				{
					continue;
				}
				pTri->m_iTimeStamp = m_iCurrentTimeStamp;

				if(!pTri->m_colPolyData.TestCollisionTypeFlags(m_iCollisionTypesToTest))
					continue;

				if(m_iLosFlagsToTest && ((pTri->m_iFlags & m_iLosFlagsToTest)==0))
					continue;

				if(m_bIgnoreSeeThrough && (pTri->m_iFlags & NAVGENTRI_SEETHROUGH))
					continue;

				if(m_bIgnoreSeeThrough && pTri->m_colPolyData.GetIsShootThrough())
					continue;

				if(m_bIgnoreSeeThrough && pTri->m_colPolyData.GetDoesntProvideCover())
					continue;

				if(m_bIgnoreFixed && (pTri->m_colPolyData.GetIsFixedObject()))
					continue;

				if(m_bIgnoreClimbableObjects && (pTri->m_colPolyData.GetIsClimbableObject()))
					continue;

				if(m_bIgnoreStairs && (pTri->m_iFlags & NAVGENTRI_STAIRS))
					continue;

				if(m_bIgnoreStairs && pTri->m_colPolyData.GetIsStairs())
					continue;

				if(m_bIgnoreRiverBound && pTri->m_colPolyData.GetIsRiverBound())
					continue;

				if(!m_NavGenPolyLosMinMax.Intersects(pTri->m_MinMax))
					continue;

				// As soon as an intersection is found we may return false
				bool bHit = RayIntersectsTriangle(
					vStart,
					vEnd,
					&pTri->m_vPts[0],
					vIntersect
				);

				if(bHit)
				{
					return false;
				}
			}
		}

		return true;
	}
	else
	{
		CNavGenOctreeNode * pNode = (CNavGenOctreeNode*) pNodeBase;

		// We need only consider the child nodes if the ray intersects this node's volume

#ifdef OVERESTIMATE_OCTREE_NODES
		if(BBoxIntersect(m_vRayMins, m_vRayMaxs, vNodeMin - vOctreeMinMaxOverEstimate, vNodeMax + vOctreeMinMaxOverEstimate))
#else
		if(BBoxIntersect(m_vRayMins, m_vRayMaxs, vNodeMin, vNodeMax))
#endif
		{
			s32 o;
			for(o=0; o<8; o++)
			{
				// Get the min/max's for this octant based upon the parent node's extents
				Vector3 vOctantMin, vOctantMax;
				GetOctantExtents(o, vNodeMin, vNodeMax, vOctantMin, vOctantMax);

				CNavGenOctreeNodeBase * pChildNode = pNode->m_ChildNodes[o];

				if(pChildNode)
				{
					bool bLos = TestLineOfSightR(pChildNode, vOctantMin, vOctantMax, vStart, vEnd);

					// As soon as an intersection is found we may return false
					if(!bLos)
					{
						return false;
					}
				}
			}
		}

		return true;
	}
}

bool CNavGenOctree::TestLineOfSightVerticalR(CNavGenOctreeNodeBase * pNodeBase, Vector3 & vNodeMin, Vector3 & vNodeMax, const Vector3 & vStart, const Vector3 & vEnd)
{
	// If we've reached a leaf, test the triangles within
	if(pNodeBase->m_Flags & NAVGEN_OCTREE_FLAG_ISLEAF)
	{
		CNavGenOctreeLeaf * pLeaf = (CNavGenOctreeLeaf*) pNodeBase;

		if(vStart.x >= vNodeMin.x && vStart.x <= vNodeMax.x && vStart.y >= vNodeMin.y && vStart.y <= vNodeMax.y)
		{
			u32 p;
			Vector3 vIntersect;

			for(p=0; p<pLeaf->m_Triangles.size(); p++)
			{
				CNavGenTri * pTri = pLeaf->m_Triangles[p];

				if(pTri->m_iTimeStamp == m_iCurrentTimeStamp)
				{
					continue;
				}
				pTri->m_iTimeStamp = m_iCurrentTimeStamp;

				if(!pTri->m_colPolyData.TestCollisionTypeFlags(m_iCollisionTypesToTest))
					continue;

				if(m_iLosFlagsToTest && ((pTri->m_iFlags & m_iLosFlagsToTest)==0))
					continue;

				if(m_bIgnoreSeeThrough && (pTri->m_iFlags & NAVGENTRI_SEETHROUGH))
					continue;

				if(m_bIgnoreSeeThrough && pTri->m_colPolyData.GetIsShootThrough())
					continue;

				if(m_bIgnoreSeeThrough && pTri->m_colPolyData.GetDoesntProvideCover())
					continue;

				if(m_bIgnoreFixed && (pTri->m_colPolyData.GetIsFixedObject()))
					continue;

				if(m_bIgnoreClimbableObjects && (pTri->m_colPolyData.GetIsClimbableObject()))
					continue;

				if(m_bIgnoreStairs && (pTri->m_iFlags & NAVGENTRI_STAIRS))
					continue;

				if(m_bIgnoreStairs && pTri->m_colPolyData.GetIsStairs())
					continue;

				if(m_bIgnoreRiverBound && pTri->m_colPolyData.GetIsRiverBound())
					continue;

				if(!m_NavGenPolyLosMinMax.Intersects(pTri->m_MinMax))
					continue;

				// As soon as an intersection is found we may return false
				bool bHit = RayIntersectsTriangle(
					vStart,
					vEnd,
					&pTri->m_vPts[0],
					vIntersect
				);

				if(bHit)
				{
					return false;
				}
			}
		}

		return true;
	}
	else
	{
		CNavGenOctreeNode * pNode = (CNavGenOctreeNode*) pNodeBase;

		// We need only consider the child nodes if the ray intersects this node's volume
		if(vStart.x >= vNodeMin.x && vStart.x <= vNodeMax.x && vStart.y >= vNodeMin.y && vStart.y <= vNodeMax.y)
		{
			s32 o;
			for(o=0; o<8; o++)
			{
				// Get the min/max's for this octant based upon the parent node's extents
				Vector3 vOctantMin, vOctantMax;
				GetOctantExtents(o, vNodeMin, vNodeMax, vOctantMin, vOctantMax);

				CNavGenOctreeNodeBase * pChildNode = pNode->m_ChildNodes[o];

				if(pChildNode)
				{
					bool bLos = TestLineOfSightVerticalR(pChildNode, vOctantMin, vOctantMax, vStart, vEnd);

					// As soon as an intersection is found we may return false
					if(!bLos)
					{
						return false;
					}
				}
			}
		}

		return true;
	}
}


//*************************************************************************************************

// performs 5 jittered vertical linetests & returns the one with the highest Z
const Vector3 CProbeTester::m_vJitterOffsets[5] =
{
	Vector3(0.0f, 0.0f, 0.0f),
	Vector3(-1.0f, 0.0f, 0.0f),
	Vector3(1.0f, 0.0f, 0.0f),
	Vector3(0.0f, -1.0f, 0.0f),
	Vector3(0.0f, 1.0f, 0.0f)
};

bool
CNavGenOctree::ProcessLineOfSightVerticalJittered(
	const Vector3 & vStart,
	const Vector3 & vEnd,
	const float fJitterSize,
	const u32 iCollisionTypeFlags,
	Vector3 & vHitPoint,
	float & fHitDist,
	CNavGenTri *& hitTriangle,
	u32 iFlagsToTest,
	bool bIgnoreSeeThrough,
	bool bIgnoreFixed,
	bool bIgnoreClimbable,
	bool bIgnoreRiverBound)
{
	Assert(vStart.z > vEnd.z);	// linetests must be downwards

	Vector3 vJitteredStart, vJitteredEnd;
	Vector3 vScaledOffset;
	float fHighestZ = -9999.9f;
	bool bHitSomething = false;

	ResetBatchLOS();

	int j;
	for(j=0; j<5; j++)
	{
		vScaledOffset = m_vJitterOffsets[j] * fJitterSize;
		vJitteredStart = vStart + vScaledOffset;
		vJitteredEnd = vEnd + vScaledOffset;
		
		AddLosTestToBatch(vJitteredStart, vJitteredEnd);
	}

	int iNumHit, iNumClear;
	StartBatchLosTest(iNumHit, iNumClear, iCollisionTypeFlags, EContinueUntilEnd, true, iFlagsToTest, bIgnoreSeeThrough, bIgnoreFixed, bIgnoreClimbable, bIgnoreRiverBound);

	for(j=0; j<5; j++)
	{
		int iNumIsec = GetBatchNumIntersections(j);
		for(int i=0; i<iNumIsec; i++)
		{
			const Vector3 & vHitPos = GetBatchIntersection(j,i);

			if(vHitPos.z > fHighestZ)
			{
				fHighestZ = vHitPos.z;
				vHitPoint = vHitPos;
				fHitDist = GetBatchHitDistSqr(j,i);
				hitTriangle = GetBatchHitTri(j,i);
				bHitSomething = true;
			}
		}
	}

	if(bHitSomething)
	{
		if(fHitDist > 0.001f)
			fHitDist = Sqrtf(fHitDist);
	}

	return bHitSomething;
}

//*************************************************************************************************

bool
CNavGenOctree::ProcessLineOfSight(const Vector3 & vStart, const Vector3 & vEnd, u32 & iOut_ReturnFlags, const u32 iCollisionTypeFlags, Vector3 & vHitPoint, float & fHitDist, CNavGenTri *& hitTriangle, bool bVertical, u32 iFlagsToTest, bool bIgnoreSeeThrough, bool bIgnoreFixed, bool bIgnoreClimbableObjects, bool bIgnoreStairs, bool bIgnoreRiverBound)
{
	if(!m_pRootNode)
	{
		return false;
	}

	return ProcessLineOfSightFromNode(m_pRootNode, m_RootNodeMin, m_RootNodeMax, vStart, vEnd, iOut_ReturnFlags, iCollisionTypeFlags, vHitPoint, fHitDist, hitTriangle, bVertical, iFlagsToTest, bIgnoreSeeThrough, bIgnoreFixed, bIgnoreClimbableObjects, bIgnoreStairs, bIgnoreRiverBound);
}

bool
CNavGenOctree::ProcessLineOfSightFromNode(CNavGenOctreeNodeBase * pNodeToStartAt, const Vector3 & vNodeMin, const Vector3 & vNodeMax, const Vector3 & vStart, const Vector3 & vEnd, u32 & iOut_ReturnFlags, const u32 iCollisionTypeFlags, Vector3 & vHitPoint, float & fHitDist, CNavGenTri *& hitTriangle, bool bVertical, u32 iFlagsToTest, bool bIgnoreSeeThrough, bool bIgnoreFixed, bool bIgnoreClimbableObjects, bool bIgnoreStairs, bool bIgnoreRiverBound)
{
	m_pHitTri = NULL;
	m_fClosestHitDistSqr = FLT_MAX;
	m_iCollisionTypesToTest = iCollisionTypeFlags;
	m_iLosFlagsToTest = iFlagsToTest;
	m_bIgnoreSeeThrough = bIgnoreSeeThrough;
	m_bIgnoreFixed = bIgnoreFixed;
	m_bIgnoreClimbableObjects = bIgnoreClimbableObjects;
	m_bIgnoreStairs = bIgnoreStairs;
	m_bIgnoreRiverBound = bIgnoreRiverBound;
	m_iOutReturnFlags = 0;

	Vector3 vMin = vNodeMin;
	Vector3 vMax = vNodeMax;

	m_vRayMins.x = Min(vStart.x, vEnd.x);
	m_vRayMins.y = Min(vStart.y, vEnd.y);
	m_vRayMins.z = Min(vStart.z, vEnd.z);
	m_vRayMaxs.x = Max(vStart.x, vEnd.x);
	m_vRayMaxs.y = Max(vStart.y, vEnd.y);
	m_vRayMaxs.z = Max(vStart.z, vEnd.z);

	m_NavGenPolyLosMinMax.SetFloat(m_vRayMins.x, m_vRayMins.y, m_vRayMins.z, m_vRayMaxs.x, m_vRayMaxs.y, m_vRayMaxs.z);
	m_NavGenPolyLosMinMax.Expand( NAVGEN_MINMAX_EXPAND_AMT );

	m_iCurrentTimeStamp++;

	// Recurse into the tree, from the given node
	if(bVertical)
	{
		ProcessLineOfSightVerticalR(pNodeToStartAt, vMin, vMax, vStart, vEnd);
	}
	else
	{
		ProcessLineOfSightR(pNodeToStartAt, vMin, vMax, vStart, vEnd);
	}

	iOut_ReturnFlags = m_iOutReturnFlags;

	if(m_pHitTri != NULL)
	{
		hitTriangle = m_pHitTri;
		vHitPoint = m_vHitPos;

		fHitDist = Sqrtf(m_fClosestHitDistSqr);
		return true;
	}

	return false;
}


void
CNavGenOctree::ProcessLineOfSightR(CNavGenOctreeNodeBase * pNodeBase, Vector3 & vNodeMin, Vector3 & vNodeMax, const Vector3 & vStart, const Vector3 & vEnd)
{
	// If we've reached a leaf, test the triangles within
	if(pNodeBase->m_Flags & NAVGEN_OCTREE_FLAG_ISLEAF)
	{
		CNavGenOctreeLeaf * pLeaf = (CNavGenOctreeLeaf*) pNodeBase;

#ifdef OVERESTIMATE_OCTREE_NODES
		if(BBoxIntersect(m_vRayMins, m_vRayMaxs, pLeaf->m_NodeMin - vOctreeMinMaxOverEstimate, pLeaf->m_NodeMax + vOctreeMinMaxOverEstimate))
#else
		if(BBoxIntersect(m_vRayMins, m_vRayMaxs, pLeaf->m_NodeMin, pLeaf->m_NodeMax))
#endif
		{
			u32 p;
			Vector3 vIntersect;

			for(p=0; p<pLeaf->m_Triangles.size(); p++)
			{
				CNavGenTri * pTri = pLeaf->m_Triangles[p];

				if(pTri->m_iTimeStamp == m_iCurrentTimeStamp)
				{
					continue;
				}
				pTri->m_iTimeStamp = m_iCurrentTimeStamp;

				if(!pTri->m_colPolyData.TestCollisionTypeFlags(m_iCollisionTypesToTest))
					continue;

				if(m_iLosFlagsToTest && ((pTri->m_iFlags & m_iLosFlagsToTest)==0))
					continue;

				if(m_bIgnoreSeeThrough && (pTri->m_iFlags & NAVGENTRI_SEETHROUGH))
					continue;

				if(m_bIgnoreSeeThrough && pTri->m_colPolyData.GetIsShootThrough())
					continue;

				if(m_bIgnoreSeeThrough && (pTri->m_colPolyData.GetDoesntProvideCover()))
					continue;

				if(m_bIgnoreFixed && (pTri->m_colPolyData.GetIsFixedObject()))
					continue;

				if(m_bIgnoreClimbableObjects && (pTri->m_colPolyData.GetIsClimbableObject()))
					continue;

				if(m_bIgnoreStairs && (pTri->m_iFlags & NAVGENTRI_STAIRS))
					continue;

				if(m_bIgnoreStairs && pTri->m_colPolyData.GetIsStairs())
					continue;

				if(m_bIgnoreRiverBound && pTri->m_colPolyData.GetIsRiverBound())
					continue;

				if(!m_NavGenPolyLosMinMax.Intersects(pTri->m_MinMax))
					continue;

				// As soon as an intersection is found we may return false
				bool bHit = RayIntersectsTriangle(
					vStart,
					vEnd,
					&pTri->m_vPts[0],
					vIntersect
				);

				if(bHit)
				{
					// Fill out the return flags
					if(pTri->m_colPolyData.GetIsNotClimbableCollision())
						m_iOutReturnFlags |= RetFlag_HitNonClimbableCollision;


					Vector3 vDiff = vIntersect - vStart;
					float fDistSqr = vDiff.Mag2();
					if(fDistSqr < m_fClosestHitDistSqr)
					{
						m_fClosestHitDistSqr = fDistSqr;
						m_vHitPos = vIntersect;
						m_pHitTri = pTri;
					}
				}
			}
		}

		return;
	}
	else
	{
		CNavGenOctreeNode * pNode = (CNavGenOctreeNode*) pNodeBase;

		// We need only consider the child nodes if the ray intersects this node's volume

#ifdef OVERESTIMATE_OCTREE_NODES
		if(BBoxIntersect(m_vRayMins, m_vRayMaxs, vNodeMin - vOctreeMinMaxOverEstimate, vNodeMax + vOctreeMinMaxOverEstimate))
#else
		if(BBoxIntersect(m_vRayMins, m_vRayMaxs, vNodeMin, vNodeMax))
#endif
		{
			s32 o;
			for(o=0; o<8; o++)
			{
				// Get the min/max's for this octant based upon the parent node's extents
				Vector3 vOctantMin, vOctantMax;
				GetOctantExtents(o, vNodeMin, vNodeMax, vOctantMin, vOctantMax);

				CNavGenOctreeNodeBase * pChildNode = pNode->m_ChildNodes[o];

				if(pChildNode)
				{
					ProcessLineOfSightR(pChildNode, vOctantMin, vOctantMax, vStart, vEnd);
				}
			}
		}
	}
}


void
CNavGenOctree::ProcessLineOfSightVerticalR(CNavGenOctreeNodeBase * pNodeBase, Vector3 & vNodeMin, Vector3 & vNodeMax, const Vector3 & vStart, const Vector3 & vEnd)
{
	// If we've reached a leaf, test the triangles within
	if(pNodeBase->m_Flags & NAVGEN_OCTREE_FLAG_ISLEAF)
	{
		CNavGenOctreeLeaf * pLeaf = (CNavGenOctreeLeaf*) pNodeBase;

		//if(vStart.x >= vNodeMin.x && vStart.x <= vNodeMax.x && vStart.y >= vNodeMin.y && vStart.y <= vNodeMax.y)

#ifdef OVERESTIMATE_OCTREE_NODES
		if(BBoxIntersect2d(m_vRayMins, m_vRayMaxs, pLeaf->m_NodeMin - vOctreeMinMaxOverEstimate, pLeaf->m_NodeMax + vOctreeMinMaxOverEstimate))
#else
		if(BBoxIntersect2d(m_vRayMins, m_vRayMaxs, pLeaf->m_NodeMin, pLeaf->m_NodeMax))
#endif
		{
			u32 p;
			Vector3 vIntersect;

			for(p=0; p<pLeaf->m_Triangles.size(); p++)
			{
				CNavGenTri * pTri = pLeaf->m_Triangles[p];

				if(pTri->m_iTimeStamp == m_iCurrentTimeStamp)
				{
					continue;
				}
				pTri->m_iTimeStamp = m_iCurrentTimeStamp;

				if(!pTri->m_colPolyData.TestCollisionTypeFlags(m_iCollisionTypesToTest))
					continue;

				if(m_iLosFlagsToTest && ((pTri->m_iFlags & m_iLosFlagsToTest)==0))
					continue;

				if(m_bIgnoreSeeThrough && (pTri->m_iFlags & NAVGENTRI_SEETHROUGH))
					continue;

				if(m_bIgnoreSeeThrough && pTri->m_colPolyData.GetIsShootThrough())
					continue;

				if(m_bIgnoreSeeThrough && (pTri->m_colPolyData.GetDoesntProvideCover()))
					continue;

				if(m_bIgnoreFixed && (pTri->m_colPolyData.GetIsFixedObject()))
					continue;

				if(m_bIgnoreClimbableObjects && (pTri->m_colPolyData.GetIsClimbableObject()))
					continue;

				if(m_bIgnoreStairs && (pTri->m_iFlags & NAVGENTRI_STAIRS))
					continue;

				if(m_bIgnoreStairs && pTri->m_colPolyData.GetIsStairs())
					continue;

				if(m_bIgnoreRiverBound && pTri->m_colPolyData.GetIsRiverBound())
					continue;

				if(!m_NavGenPolyLosMinMax.Intersects(pTri->m_MinMax))
					continue;

				// As soon as an intersection is found we may return false
				bool bHit = RayIntersectsTriangle(
					vStart,
					vEnd,
					&pTri->m_vPts[0],
					vIntersect
				);

				if(bHit)
				{
					Vector3 vDiff = vIntersect - vStart;
					float fDistSqr = vDiff.Mag2();
					if(fDistSqr < m_fClosestHitDistSqr)
					{
						m_fClosestHitDistSqr = fDistSqr;
						m_vHitPos = vIntersect;
						m_pHitTri = pTri;
					}
				}
			}
		}

		return;
	}
	else
	{
		CNavGenOctreeNode * pNode = (CNavGenOctreeNode*) pNodeBase;

		// We need only consider the child nodes if the ray intersects this node's volume
		//if(1)	//vStart.x >= vNodeMin.x && vStart.x <= vNodeMax.x && vStart.y >= vNodeMin.y && vStart.y <= vNodeMax.y)

#ifdef OVERESTIMATE_OCTREE_NODES
		if(BBoxIntersect2d(m_vRayMins, m_vRayMaxs, vNodeMin - vOctreeMinMaxOverEstimate, vNodeMax + vOctreeMinMaxOverEstimate))
#else
		if(BBoxIntersect2d(m_vRayMins, m_vRayMaxs, vNodeMin, vNodeMax))
#endif
		{
			s32 o;
			for(o=0; o<8; o++)
			{
				// Get the min/max's for this octant based upon the parent node's extents
				Vector3 vOctantMin, vOctantMax;
				GetOctantExtents(o, vNodeMin, vNodeMax, vOctantMin, vOctantMax);

				CNavGenOctreeNodeBase * pChildNode = pNode->m_ChildNodes[o];

				if(pChildNode)
				{
					ProcessLineOfSightVerticalR(pChildNode, vOctantMin, vOctantMax, vStart, vEnd);
				}
			}
		}
	}
}



//*************************************************************************************************

//*************************************************************************************************


void
CNavGenOctree::ResetBatchLOS()
{
	m_iNumBatchedLosTests = 0;
	m_vRayMins = Vector3(FLT_MAX, FLT_MAX, FLT_MAX);
	m_vRayMaxs = Vector3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	s32 i;
	for(i=0; i<MAX_NUM_BATCHED_LOS_TESTS; i++)
	{
		m_iBatchLosTestNumPolyHits[i] = 0;
	}
}


bool
CNavGenOctree::AddLosTestToBatch(const Vector3 & vStart, const Vector3 & vEnd)
{
	if(m_iNumBatchedLosTests >= MAX_NUM_BATCHED_LOS_TESTS)
		return false;

	m_vBatchLosTestStarts[m_iNumBatchedLosTests] = vStart;
	m_vBatchLosTestEnds[m_iNumBatchedLosTests] = vEnd;

	m_MinMaxForBatchedLosTest[m_iNumBatchedLosTests].SetFloat(
		Min(vStart.x, vEnd.x) - 0.5f, Min(vStart.y, vEnd.y) - 0.5f, Min(vStart.z, vEnd.z) - 0.5f,
		Max(vStart.x, vEnd.x) + 0.5f, Max(vStart.y, vEnd.y) + 0.5f, Max(vStart.z, vEnd.z) + 0.5f
	);

	if(vStart.x < m_vRayMins.x) m_vRayMins.x = vStart.x;
	if(vStart.y < m_vRayMins.y) m_vRayMins.y = vStart.y;
	if(vStart.z < m_vRayMins.z) m_vRayMins.z = vStart.z;
	if(vStart.x > m_vRayMaxs.x) m_vRayMaxs.x = vStart.x;
	if(vStart.y > m_vRayMaxs.y) m_vRayMaxs.y = vStart.y;
	if(vStart.z > m_vRayMaxs.z) m_vRayMaxs.z = vStart.z;

	if(vEnd.x < m_vRayMins.x) m_vRayMins.x = vEnd.x;
	if(vEnd.y < m_vRayMins.y) m_vRayMins.y = vEnd.y;
	if(vEnd.z < m_vRayMins.z) m_vRayMins.z = vEnd.z;
	if(vEnd.x > m_vRayMaxs.x) m_vRayMaxs.x = vEnd.x;
	if(vEnd.y > m_vRayMaxs.y) m_vRayMaxs.y = vEnd.y;
	if(vEnd.z > m_vRayMaxs.z) m_vRayMaxs.z = vEnd.z;

	m_iNumBatchedLosTests++;

	return true;
}

void CNavGenOctree::StartBatchLosTest(int & iNumLosHit, int & iNumLosClear, const u32 iCollisionTypeFlags, EBatchTestLosMode eBatchLosTestMode, bool bRecordIntersections, u32 iFlagsToTest, bool bIgnoreSeeThrough, bool bIgnoreFixed, bool bIgnoreClimbable, bool bIgnoreStairs, bool bIgnoreRiverBound)
{
	iNumLosHit = 0;
	iNumLosClear = 0;

	if(!m_iNumBatchedLosTests)
		return;

	m_eBatchedLosTestMode = eBatchLosTestMode;
	m_bBatchRecordIntersections = bRecordIntersections;

	if(m_bBatchRecordIntersections)
	{
		memset(m_iBatchNumIntersections, 0, sizeof(int)*MAX_NUM_BATCHED_LOS_TESTS);
	}

	m_iCollisionTypesToTest = iCollisionTypeFlags;
	m_iLosFlagsToTest = iFlagsToTest;
	m_bIgnoreSeeThrough = bIgnoreSeeThrough;
	m_bIgnoreFixed = bIgnoreFixed;
	m_bIgnoreClimbableObjects = bIgnoreClimbable;
	m_bIgnoreStairs = bIgnoreStairs;
	m_bIgnoreRiverBound = bIgnoreRiverBound;

	m_iCurrentTimeStamp++;

	m_NavGenPolyLosMinMax.SetFloat(m_vRayMins.x, m_vRayMins.y, m_vRayMins.z, m_vRayMaxs.x, m_vRayMaxs.y, m_vRayMaxs.z);
	m_NavGenPolyLosMinMax.Expand( NAVGEN_MINMAX_EXPAND_AMT );

	TestBatchLineOfSightR(m_pRootNode, m_RootNodeMin, m_RootNodeMax);

	// Now count up how many were clear & how many intersected
	s32 i;
	for(i=0; i<m_iNumBatchedLosTests; i++)
	{
		if(m_iBatchLosTestNumPolyHits[i] > 0)
			iNumLosHit++;
		else
			iNumLosClear++;
	}
}




bool CNavGenOctree::TestBatchLineOfSightR(CNavGenOctreeNodeBase * pNodeBase, Vector3 & vNodeMin, Vector3 & vNodeMax)
{
	// If we've reached a leaf, test the triangles within
	if(pNodeBase->m_Flags & NAVGEN_OCTREE_FLAG_ISLEAF)
	{
		CNavGenOctreeLeaf * pLeaf = (CNavGenOctreeLeaf*) pNodeBase;

#ifdef OVERESTIMATE_OCTREE_NODES
		if(BBoxIntersect(m_vRayMins, m_vRayMaxs, pLeaf->m_NodeMin - vOctreeMinMaxOverEstimate, pLeaf->m_NodeMax + vOctreeMinMaxOverEstimate))
#else
		if(BBoxIntersect(m_vRayMins, m_vRayMaxs, pLeaf->m_NodeMin, pLeaf->m_NodeMax))
#endif
		{
			u32 p;
			Vector3 vIntersect;

			for(p=0; p<pLeaf->m_Triangles.size(); p++)
			{
				CNavGenTri * pTri = pLeaf->m_Triangles[p];

				if(pTri->m_iTimeStamp == m_iCurrentTimeStamp)
				{
					continue;
				}
				pTri->m_iTimeStamp = m_iCurrentTimeStamp;

				if(!m_NavGenPolyLosMinMax.Intersects(pTri->m_MinMax))
					continue;

				if(!pTri->m_colPolyData.TestCollisionTypeFlags(m_iCollisionTypesToTest))
					continue;

				if(m_iLosFlagsToTest && ((pTri->m_iFlags & m_iLosFlagsToTest)==0))
					continue;

				if(m_bIgnoreSeeThrough && (pTri->m_iFlags & NAVGENTRI_SEETHROUGH))
					continue;

				if(m_bIgnoreSeeThrough && pTri->m_colPolyData.GetIsShootThrough())
					continue;

				if(m_bIgnoreSeeThrough && (pTri->m_colPolyData.GetDoesntProvideCover()))
					continue;

				if(m_bIgnoreFixed && (pTri->m_colPolyData.GetIsFixedObject()))
					continue;

				if(m_bIgnoreClimbableObjects && (pTri->m_colPolyData.GetIsClimbableObject()))
					continue;

				if(m_bIgnoreStairs && (pTri->m_iFlags & NAVGENTRI_STAIRS))
					continue;

				if(m_bIgnoreStairs && pTri->m_colPolyData.GetIsStairs())
					continue;

				if(m_bIgnoreRiverBound && pTri->m_colPolyData.GetIsRiverBound())
					continue;

				int i;
				for(i=0; i<m_iNumBatchedLosTests; i++)
				{
					// As soon as an intersection is found we may return false
					bool bHit = RayIntersectsTriangle(
						m_vBatchLosTestStarts[i],
						m_vBatchLosTestEnds[i],
						&pTri->m_vPts[0],
						vIntersect
					);

					if(bHit)
					{
						if(m_bBatchRecordIntersections)
						{
							if(m_iBatchNumIntersections[i]<MAX_BATCH_ISECTS)
							{
								m_vBatchIntersections[i][m_iBatchNumIntersections[i]] = vIntersect;
								m_fBatchHitDistsSqr[i][m_iBatchNumIntersections[i]] = (vIntersect - m_vBatchLosTestStarts[i]).Mag2();
								m_pBatchHitTris[i][m_iBatchNumIntersections[i]] = pTri;

								m_iBatchNumIntersections[i]++;
							}
						}

						m_iBatchLosTestNumPolyHits[i]++;
						if(m_eBatchedLosTestMode==EQuitIfWeHitSomething)
							return false;
					}
				}
			}
		}

		return true;
	}
	else
	{
		CNavGenOctreeNode * pNode = (CNavGenOctreeNode*) pNodeBase;

#ifdef OVERESTIMATE_OCTREE_NODES
		if(BBoxIntersect(m_vRayMins, m_vRayMaxs, vNodeMin - vOctreeMinMaxOverEstimate, vNodeMax + vOctreeMinMaxOverEstimate))
#else
		if(BBoxIntersect(m_vRayMins, m_vRayMaxs, vNodeMin, vNodeMax))
#endif
		{
			s32 o;
			for(o=0; o<8; o++)
			{
				// Get the min/max's for this octant based upon the parent node's extents
				Vector3 vOctantMin, vOctantMax;
				GetOctantExtents(o, vNodeMin, vNodeMax, vOctantMin, vOctantMax);

				CNavGenOctreeNodeBase * pChildNode = pNode->m_ChildNodes[o];

				if(pChildNode)
				{
					bool bLos = TestBatchLineOfSightR(pChildNode, vOctantMin, vOctantMax);

					// If we are to quit on hitting/not-hitting a surface, then the test will return false.
					// We should continue to return false until we exit from the octree.
					if(!bLos)
						return false;
				}
			}
		}

		return true;
	}
}


//*************************************************************************************************

//*************************************************************************************************


bool CNavGenOctree::GetRayIntersectPolySide(const Vector3 & vStart, const Vector3 & vEnd, int & iSide, const u32 iCollisionTypeFlags, Vector3 * pHitPos)
{
	iSide=0;

	if(!m_pRootNode)
		return false;

	m_LosIntersections.Reset();

	m_pHitTri = NULL;
	m_fClosestHitDistSqr = FLT_MAX;
	m_iCollisionTypesToTest = iCollisionTypeFlags;
	m_iLosFlagsToTest = 0;
	m_bIgnoreSeeThrough = true;	// See-through objects are sometimes one-sided
	m_bIgnoreFixed = false;
	m_bIgnoreClimbableObjects = true;
	m_bIgnoreStairs = false;
	m_bIgnoreRiverBound = false;

	m_vRayMins.x = Min(vStart.x, vEnd.x);
	m_vRayMins.y = Min(vStart.y, vEnd.y);
	m_vRayMins.z = Min(vStart.z, vEnd.z);
	m_vRayMaxs.x = Max(vStart.x, vEnd.x);
	m_vRayMaxs.y = Max(vStart.y, vEnd.y);
	m_vRayMaxs.z = Max(vStart.z, vEnd.z);

	m_NavGenPolyLosMinMax.SetFloat( m_vRayMins.x, m_vRayMins.y, m_vRayMins.z, m_vRayMaxs.x, m_vRayMaxs.y, m_vRayMaxs.z );
	m_NavGenPolyLosMinMax.Expand( NAVGEN_MINMAX_EXPAND_AMT );

	m_iCurrentTimeStamp++;

	GetRayIntersectPolySideR(m_pRootNode, m_RootNodeMin, m_RootNodeMax, vStart, vEnd);

	if(!m_LosIntersections.GetNum())
		return false;

	Vector3 vRay = vEnd - vStart;
	vRay.Normalize();

	if(m_LosIntersections.GetNum() >= 2)
	{	
		// We have 2 or more intersections.  See if they're close.
		float fDist1 = m_LosIntersections.GetDist(0);
		float fDist2 = m_LosIntersections.GetDist(1);

		static const float fCloseIntersection = 0.25f;

		if(Abs(fDist1 - fDist2) < fCloseIntersection)
		{
			CNavGenTri * pTri1 = m_LosIntersections.GetTri(0);
			CNavGenTri * pTri2 = m_LosIntersections.GetTri(1);

			if(pHitPos)
			{
				if(fDist1 < fDist2)
					*pHitPos = m_LosIntersections.GetPos(0);
				else
					*pHitPos = m_LosIntersections.GetPos(1);
			}

			float fDot1 = - DotProduct(vRay, pTri1->m_vNormal);
			float fDot2 = - DotProduct(vRay, pTri2->m_vNormal);
			int iSide1 = (int) Sign(fDot1);
			int iSide2 = (int) Sign(fDot2);

			if(iSide1==iSide2)
			{
				iSide = iSide1;
			}
			else
			{
				iSide = 0;	// unable to reliably determine, we hit two coincident opposite-facing polys
			}

			return true;
		}
	}

	CNavGenTri * pTri = m_LosIntersections.GetTri(0);
	float fDot = - DotProduct(vRay, pTri->m_vNormal);
	iSide = (int) Sign(fDot);

	if(pHitPos)
	{
		*pHitPos = m_LosIntersections.GetPos(0);
	}

	return true;
}


void
CNavGenOctree::GetRayIntersectPolySideR(CNavGenOctreeNodeBase * pNodeBase, Vector3 & vNodeMin, Vector3 & vNodeMax, const Vector3 & vStart, const Vector3 & vEnd)
{
	// If we've reached a leaf, test the triangles within
	if(pNodeBase->m_Flags & NAVGEN_OCTREE_FLAG_ISLEAF)
	{
		CNavGenOctreeLeaf * pLeaf = (CNavGenOctreeLeaf*) pNodeBase;

#ifdef OVERESTIMATE_OCTREE_NODES
		if(BBoxIntersect(m_vRayMins, m_vRayMaxs, pLeaf->m_NodeMin - vOctreeMinMaxOverEstimate, pLeaf->m_NodeMax + vOctreeMinMaxOverEstimate))
#else
		if(BBoxIntersect(m_vRayMins, m_vRayMaxs, pLeaf->m_NodeMin, pLeaf->m_NodeMax))
#endif
		{
			u32 p;
			Vector3 vIntersect;

			for(p=0; p<pLeaf->m_Triangles.size(); p++)
			{
				CNavGenTri * pTri = pLeaf->m_Triangles[p];

				if(pTri->m_iTimeStamp == m_iCurrentTimeStamp)
				{
					continue;
				}
				pTri->m_iTimeStamp = m_iCurrentTimeStamp;

				if(!pTri->m_colPolyData.TestCollisionTypeFlags(m_iCollisionTypesToTest))
					continue;

				if(m_iLosFlagsToTest && ((pTri->m_iFlags & m_iLosFlagsToTest)==0))
					continue;

				if(m_bIgnoreSeeThrough && pTri->m_colPolyData.GetIsSeeThrough())
					continue;

				if(m_bIgnoreFixed && (pTri->m_colPolyData.GetIsFixedObject()))
					continue;

				if(m_bIgnoreClimbableObjects && (pTri->m_colPolyData.GetIsClimbableObject()))
					continue;

				if(m_bIgnoreStairs && (pTri->m_iFlags & NAVGENTRI_STAIRS))
					continue;

				if(m_bIgnoreStairs && pTri->m_colPolyData.GetIsStairs())
					continue;

				if(m_bIgnoreRiverBound && pTri->m_colPolyData.GetIsRiverBound())
					continue;

				if(!m_NavGenPolyLosMinMax.Intersects(pTri->m_MinMax))
					continue;

				bool bHit = RayIntersectsTriangle(
					vStart,
					vEnd,
					&pTri->m_vPts[0],
					vIntersect
				);

				if(bHit)
				{
					Vector3 vDiff = vIntersect - vStart;
					float fDist = vDiff.Mag();
					m_LosIntersections.Add(vIntersect, fDist, pTri);
				}
			}
		}

		return;
	}
	else
	{
		CNavGenOctreeNode * pNode = (CNavGenOctreeNode*) pNodeBase;

		// We need only consider the child nodes if the ray intersects this node's volume

#ifdef OVERESTIMATE_OCTREE_NODES
		if(BBoxIntersect(m_vRayMins, m_vRayMaxs, vNodeMin - vOctreeMinMaxOverEstimate, vNodeMax + vOctreeMinMaxOverEstimate))
#else
		if(BBoxIntersect(m_vRayMins, m_vRayMaxs, vNodeMin, vNodeMax))
#endif
		{
			s32 o;
			for(o=0; o<8; o++)
			{
				// Get the min/max's for this octant based upon the parent node's extents
				Vector3 vOctantMin, vOctantMax;
				GetOctantExtents(o, vNodeMin, vNodeMax, vOctantMin, vOctantMax);

				CNavGenOctreeNodeBase * pChildNode = pNode->m_ChildNodes[o];

				if(pChildNode)
				{
					GetRayIntersectPolySideR(pChildNode, vOctantMin, vOctantMax, vStart, vEnd);
				}
			}
		}
	}
}

void CNavGenOctree::AddTrianglesToCulledGeom(CCulledGeom & culledGeom, CNavGenOctreeNodeBase * pNode)
{
	if(!pNode)
	{
		if(m_pRootNode)
		{
			m_iCurrentTimeStamp++;
			AddTrianglesToCulledGeom(culledGeom, m_pRootNode);
		}
		return;
	}

	if(pNode->GetIsLeafNode())
	{
		CNavGenOctreeLeaf * pLeaf = (CNavGenOctreeLeaf*)pNode;
		if(BBoxIntersect(pLeaf->m_NodeMin, pLeaf->m_NodeMax, culledGeom.m_vMin, culledGeom.m_vMax))
		{
			for(int p=0; p<(int)pLeaf->m_Triangles.size(); p++)
			{
				CNavGenTri * pTri = pLeaf->m_Triangles[p];
				if(pTri->m_iTimeStamp != m_iCurrentTimeStamp)
				{
					pTri->m_iTimeStamp = m_iCurrentTimeStamp;

					if(pTri->m_MinMax.Intersects(culledGeom.m_MinMax))
					{
						culledGeom.m_Triangles.push_back(pTri);
					}
				}
			}
		}
	}
	else
	{
		for(int i=0; i<8; i++)
		{
			if(((CNavGenOctreeNode*)pNode)->m_ChildNodes[i])
			{
				AddTrianglesToCulledGeom(culledGeom, ((CNavGenOctreeNode*)pNode)->m_ChildNodes[i]);
			}
		}
	}
}

//---------------------------------------------------------------------------------

void CCulledGeom::Reset()
{
	m_Triangles.clear();
}

void CCulledGeom::Init(const Vector3 & vMin, const Vector3 & vMax)
{
	m_vMin = vMin;
	m_vMax = vMax;
	m_MinMax.Set(vMin, vMax);
}

bool CCulledGeom::TestLineOfSight(const Vector3 & vStart, const Vector3 & vEnd, const u32 iCollisionTypeFlags, bool /*bVertical*/, u32 iFlagsToTest, bool bIgnoreSeeThrough, bool bIgnoreFixed, bool bIgnoreClimableObjects, bool bIgnoreStairs, bool bIgnoreRiverBound)
{
	Assert(vStart.x >= m_vMin.x);
	Assert(vStart.y >= m_vMin.y);
	Assert(vStart.z >= m_vMin.z);
	Assert(vStart.x < m_vMax.x);
	Assert(vStart.y < m_vMax.y);
	Assert(vStart.z < m_vMax.z);
	Assert(vEnd.x >= m_vMin.x);
	Assert(vEnd.y >= m_vMin.y);
	Assert(vEnd.z >= m_vMin.z);
	Assert(vEnd.x < m_vMax.x);
	Assert(vEnd.y < m_vMax.y);
	Assert(vEnd.z < m_vMax.z);

	Vector3 vIntersect;
	TShortMinMax rayMinMax;
	rayMinMax.SetFloat( Min(vStart.x, vEnd.x), Min(vStart.y, vEnd.y), Min(vStart.z, vEnd.z), Max(vStart.x, vEnd.x), Max(vStart.y, vEnd.y), Max(vStart.z, vEnd.z) );
	rayMinMax.Expand(NAVGEN_MINMAX_EXPAND_AMT);

	// No intersection, there LOS is clear : return true
	if(!rayMinMax.Intersects(m_MinMax))
		return true;

	for(int p=0; p<(int)m_Triangles.size(); p++)
	{
		CNavGenTri * pTri = m_Triangles[p];

		if(!pTri->m_colPolyData.TestCollisionTypeFlags(iCollisionTypeFlags))
			continue;

		if(!pTri->m_MinMax.Intersects(rayMinMax))
			continue;

		if(iFlagsToTest && ((pTri->m_iFlags & iFlagsToTest)==0))
			continue;

		if(bIgnoreSeeThrough && (pTri->m_iFlags & NAVGENTRI_SEETHROUGH))
			continue;

		if(bIgnoreSeeThrough && pTri->m_colPolyData.GetIsShootThrough())
			continue;

		if(bIgnoreSeeThrough && pTri->m_colPolyData.GetDoesntProvideCover())
			continue;

		if(bIgnoreFixed && pTri->m_colPolyData.GetIsFixedObject())
			continue;

		if(bIgnoreClimableObjects && pTri->m_colPolyData.GetIsClimbableObject())
			continue;

		if(bIgnoreStairs && (pTri->m_iFlags & NAVGENTRI_STAIRS))
			continue;

		if(bIgnoreStairs && pTri->m_colPolyData.GetIsStairs())
			continue;

		if(bIgnoreRiverBound && pTri->m_colPolyData.GetIsRiverBound())
			continue;

		// As soon as an intersection is found we may return false
		const bool bHit = RayIntersectsTriangle(
			vStart,
			vEnd,
			&pTri->m_vPts[0],
			vIntersect);

		if(bHit)
			return false;
	}
	return true;
}

// As above, but returns data about the intersection
bool CCulledGeom::ProcessLineOfSight(
	const Vector3 & vStart,
	const Vector3 & vEnd,
	u32 & iOut_ReturnFlags,
	const u32 iCollisionTypeFlags,
	Vector3 & vHitPoint,
	float & fHitDist,
	CNavGenTri *& hitTriangle,
	bool /*bVertical*/,
	u32 iFlagsToTest,
	bool bIgnoreSeeThrough,
	bool bIgnoreFixed,
	bool bIgnoreClimableObjects,
	bool bIgnoreStairs,
	bool bIngoreRiverBound)
{
	Assert(vStart.x >= m_vMin.x);
	Assert(vStart.y >= m_vMin.y);
	Assert(vStart.z >= m_vMin.z);
	Assert(vStart.x < m_vMax.x);
	Assert(vStart.y < m_vMax.y);
	Assert(vStart.z < m_vMax.z);
	Assert(vEnd.x >= m_vMin.x);
	Assert(vEnd.y >= m_vMin.y);
	Assert(vEnd.z >= m_vMin.z);
	Assert(vEnd.x < m_vMax.x);
	Assert(vEnd.y < m_vMax.y);
	Assert(vEnd.z < m_vMax.z);

	Vector3 vIntersect;
	TShortMinMax rayMinMax;
	rayMinMax.SetFloat( Min(vStart.x, vEnd.x), Min(vStart.y, vEnd.y), Min(vStart.z, vEnd.z), Max(vStart.x, vEnd.x), Max(vStart.y, vEnd.y), Max(vStart.z, vEnd.z) );
	rayMinMax.Expand( NAVGEN_MINMAX_EXPAND_AMT );

	hitTriangle = NULL;
	fHitDist = FLT_MAX;

	// No intersection, there is no intersection : return false
	if(!rayMinMax.Intersects(m_MinMax))
		return false;

	for(int p=0; p<(int)m_Triangles.size(); p++)
	{
		CNavGenTri * pTri = m_Triangles[p];

		if(!pTri->m_colPolyData.TestCollisionTypeFlags(iCollisionTypeFlags))
			continue;

		if(!pTri->m_MinMax.Intersects(rayMinMax))
			continue;

		if(iFlagsToTest && ((pTri->m_iFlags & iFlagsToTest)==0))
			continue;

		if(bIgnoreSeeThrough && (pTri->m_iFlags & NAVGENTRI_SEETHROUGH))
			continue;

		if(bIgnoreSeeThrough && pTri->m_colPolyData.GetIsShootThrough())
			continue;

		if(bIgnoreSeeThrough && pTri->m_colPolyData.GetDoesntProvideCover())
			continue;

		if(bIgnoreFixed && pTri->m_colPolyData.GetIsFixedObject())
			continue;

		if(bIgnoreClimableObjects && pTri->m_colPolyData.GetIsClimbableObject())
			continue;

		if(bIgnoreStairs && (pTri->m_iFlags & NAVGENTRI_STAIRS))
			continue;

		if(bIgnoreStairs && pTri->m_colPolyData.GetIsStairs())
			continue;

		if(bIngoreRiverBound && pTri->m_colPolyData.GetIsRiverBound())
			continue;

		// As soon as an intersection is found we may return false
		const bool bHit = RayIntersectsTriangle(
			vStart,
			vEnd,
			&pTri->m_vPts[0],
			vIntersect);

		if(bHit)
		{
			// Fill out the return flags
			if(pTri->m_colPolyData.GetIsNotClimbableCollision())
				iOut_ReturnFlags |= RetFlag_HitNonClimbableCollision;

			Vector3 vDiff = vIntersect - vStart;
			float fDistSqr = vDiff.Mag2();
			if(fDistSqr < fHitDist)
			{
				fHitDist = fDistSqr;
				vHitPoint = vIntersect;
				hitTriangle = pTri;
			}
		}
	}

	if(hitTriangle)
	{	
		fHitDist = sqrtf(fHitDist);
		return true;
	}
	return false;
}

// performs 5 jittered vertical linetests & returns the one with the highest Z
bool CCulledGeom::ProcessLineOfSightVerticalJittered(
	const Vector3 & vStart,
	const Vector3 & vEnd,
	const float fJitterSize,
	const u32 iCollisionTypeFlags,
	Vector3 & vHitPoint,
	float & fHitDist,
	CNavGenTri *& hitTriangle,
	u32 iFlagsToTest,
	bool bIgnoreSeeThrough,
	bool bIgnoreFixed,
	bool bIgnoreClimableObjects,
	bool bIgnoreRiverBound)
{
	Assert(vStart.x >= m_vMin.x);
	Assert(vStart.y >= m_vMin.y);
	Assert(vStart.z >= m_vMin.z);
	Assert(vStart.x < m_vMax.x);
	Assert(vStart.y < m_vMax.y);
	Assert(vStart.z < m_vMax.z);
	Assert(vEnd.x >= m_vMin.x);
	Assert(vEnd.y >= m_vMin.y);
	Assert(vEnd.z >= m_vMin.z);
	Assert(vEnd.x < m_vMax.x);
	Assert(vEnd.y < m_vMax.y);
	Assert(vEnd.z < m_vMax.z);

	hitTriangle = NULL;

	CNavGenTri * pTestHitTri = NULL;
	float fTestHitDist = 0.0f;
	Vector3 vTestHitPoint;
	vHitPoint.z = -FLT_MAX;
	u32 iRetFlags = 0;

	for(int j=0; j<5; j++)
	{
		const bool bHit = ProcessLineOfSight(
			vStart + m_vJitterOffsets[j] * fJitterSize,
			vEnd + m_vJitterOffsets[j] * fJitterSize,
			iRetFlags,
			iCollisionTypeFlags,
			vTestHitPoint,
			fTestHitDist,
			pTestHitTri,
			true,
			iFlagsToTest,
			bIgnoreSeeThrough,
			bIgnoreFixed,
			bIgnoreClimableObjects,
			false,
			bIgnoreRiverBound);

		if(bHit && vTestHitPoint.z > vHitPoint.z)
		{
			vHitPoint = vTestHitPoint;
			fHitDist = fTestHitDist;
			hitTriangle = pTestHitTri;
		}
	}

	return (hitTriangle != NULL);
}

bool CCulledGeom::TestVolumeIsClear(const CClipVolume & volume, const u32 iCollisionTypeFlags, bool bIgnoreClimbableObjects)
{
	const TShortMinMax & volMinMax = volume.GetMinMax();
	if(!volMinMax.Intersects(m_MinMax))
		return false;

	u32 p;
	for(p=0; p<m_Triangles.size(); p++)
	{
		CNavGenTri * pTri = m_Triangles[p];

		if(!pTri->m_colPolyData.TestCollisionTypeFlags(iCollisionTypeFlags))
			continue;

		if(!volMinMax.Intersects(pTri->m_MinMax))
			continue;

		if(bIgnoreClimbableObjects && pTri->m_colPolyData.GetIsClimbableObject())
			continue;

		if(volume.TestTriIntersection(pTri->m_vPts))
			return false;
	}
	return true;
}

}	// namespace rage
