#include "NavGen.h"
#include "fwmaths/random.h"

#include <algorithm>		// std::sort()

namespace rage
{

//***********************************************************************************
//	Title	: NavGen_Optimise.cpp
//	Author	: James Broad
//	Date	: Jan 2005
//
//	This file contains class and functions which focus upon the problem of taking
//	a triangle mesh, and optimising it into an n-sided polygon mesh.
//
//	This is done by repeatedly merging triangles into polygons, where the resultant
//	polygon will be convex.  There are further restrictions upon the generation
//	of polygons - including that the maximum area must be below some threshold,
//	that the surface normals must be similar, that no vertices may deviate from
//	the plane of the poly over a certain epsilon, that a maximum vertex count must not
//	be exceeded, and that the angle at any vertex must be within a certain range (to
//	avoid 'sliver' polgons).
//
//	Once merging cannot proceed any further, a CNavMesh is created from the data.
//	This is saved out with the CNavMesh::SaveBinary() function.
//
//*********************************************************************************

#define __PERFORM_MERGE					1

#define __DEBUG_COMPARE_POLYS			0
#define __DBG_MERGING					0
#define __MERGE_REMOVE_COLINEAR_EDGES	1

#if __DBG_MERGING
#define DBG_MERGING_ONLY(x)				x
#else
#define DBG_MERGING_ONLY(x)
#endif

#if __DEV
void
DebugMergePoly(TMergePoly * pPoly)
{
	printf("pPoly = 0x%x\n", pPoly);

	printf("Vertices:\n");
	for(s32 d=0; d<pPoly->m_Vertices.size(); d++)
	{
		Vector3 vert = pPoly->m_Vertices[d]->m_Vertex;
		printf("%i) %.5f %.5f %.5f\n", d, vert.x, vert.x, vert.z);
	}

	printf("Normal: %.2f %.2f %.2f\n", pPoly->m_vPlaneNormal.x, pPoly->m_vPlaneNormal.y, pPoly->m_vPlaneNormal.z);
	printf("Area: %.2f\n", pPoly->m_fArea);
}
#endif

bool ComparePolyPedDensityFn(TMergePoly * pPoly1, TMergePoly * pPoly2)
{
	return(pPoly1->m_colPolyData.GetPedDensity() > pPoly2->m_colPolyData.GetPedDensity());
}

bool RandomPolyCompareFn(TMergePoly * UNUSED_PARAM(pPoly1), TMergePoly * UNUSED_PARAM(pPoly2))
{
	return (fwRandom::GetRandomTrueFalse()) ? -1 : 1;
}

//------------------------------------------------------------------------------------------------------------------------------

CNavMesh * CNavGen::CreateMergedPolyNavMesh(CNavMesh * pInputMesh, vector<fwDynamicEntityInfo*> * pSplitObjects)
{
#if __DBG_MERGING
	char tmpTxt[256];
#endif

	u32 v,p;
	vector<TVertexEntry*> vertexList;
	vector<TMergePoly*> mergePolys;

	mergePolys.reserve(pInputMesh->m_iNumPolys);

	list<TMergePoly*> sortedPolyList;

	list<TMergePoly*>::iterator checkIter;

	//************************************
	// Duplicate the vertices list
	//************************************
	for(v=0; v<pInputMesh->m_iNumVertices; v++)
	{
		Vector3 vert;
		pInputMesh->GetVertex(v, vert);
		TVertexEntry * pVert = rage_new TVertexEntry;
		pVert->m_iIndex = v;
		pVert->m_Vertex = vert;
		vertexList.push_back(pVert);
	}

	s32 iNumFinalNoNetworkSpawn = 0;
	s32 iNumInitialNoNetworkSpawn = 0;

	//************************************
	// Duplicate the triangles as polys
	//************************************
	for(p=0; p<pInputMesh->m_iNumPolys; p++)
	{
		TNavMeshPoly * pPoly = pInputMesh->GetPoly(p);
		TMergePoly * pMergePoly = rage_new TMergePoly();

		pMergePoly->m_colPolyData.SetIsPavement(pPoly->GetIsPavement());
		pMergePoly->m_colPolyData.SetIsInterior(pPoly->GetIsInterior());
		pMergePoly->m_colPolyData.SetPedDensity(pPoly->GetPedDensity());
		pMergePoly->m_bInShelter = pPoly->TestFlags(NAVMESHPOLY_IN_SHELTER);
		pMergePoly->m_bWater = pPoly->TestFlags(NAVMESHPOLY_IS_WATER);
		pMergePoly->m_bTooSteep = pPoly->TestFlags(NAVMESHPOLY_TOO_STEEP_TO_WALK_ON);
		pMergePoly->m_bIntersectsEntities = pPoly->m_NonResourced.m_bIntersectsEntities;
		pMergePoly->m_colPolyData.SetIsNoNetworkSpawn(pPoly->m_NonResourced.m_bNoNetworkSpawn);
		pMergePoly->m_bDoNotMerge = pPoly->m_bDoNotOptimise;

		if(pPoly->m_NonResourced.m_bNoNetworkSpawn)
			iNumInitialNoNetworkSpawn++;

		pMergePoly->m_iIndex = p;

		pMergePoly->m_iFreeSpaceTopZ = TPolyVolumeInfo::ms_iFreeSpaceNone;

		for(v=0; v<pPoly->GetNumVertices(); v++)
		{
			int vIndex = pInputMesh->GetPolyVertexIndex(pPoly, v);
			pMergePoly->m_Vertices.Grow() = vertexList[vIndex];
		}

		// Calculate the area
		pMergePoly->CalcAreaAndBounds();

		// initially add to this list, which mirrors the input mesh poly list
		mergePolys.push_back(pMergePoly);
	}

	//*************************************************************************
	// Establish adjacency
	// Use the adjacency information already calculated for the input CNavMesh
	//*************************************************************************

	for(p=0; p<pInputMesh->m_iNumPolys; p++)
	{
		TNavMeshPoly * pPoly = pInputMesh->GetPoly(p);
		TMergePoly * pMergePoly = mergePolys[p];

		for(u32 i=0; i<pPoly->GetNumVertices(); i++)
		{
			const TAdjPoly & adjPoly = pInputMesh->GetAdjacentPoly( pPoly->GetFirstVertexIndex()+i );

			if(adjPoly.GetPolyIndex() == NAVMESH_POLY_INDEX_NONE)
			{
				pMergePoly->m_AdjacentPolys.Grow() = NULL;
			}
			else
			{
				pMergePoly->m_AdjacentPolys.Grow() = mergePolys[adjPoly.GetPolyIndex()];
			}

		}
	}

	for(v=0; v<vertexList.size(); v++)
	{
		TVertexEntry * pVert = vertexList[v];

		for(u32 p2=0; p2<pInputMesh->m_iNumPolys; p2++)
		{
			if(mergePolys[p2]->UsesVertex(pVert))
				pVert->m_SurroundingPolys.Grow() = mergePolys[p2];
		}

		Merge_SortPolysSurroundingVertex(pVert);
	}

	//********************************************************************************
	// Any polys which have pavement on all sides - or all sides but one
	// will be set to pavement themselves
	//*************************************************************************
	for(p=0; p<pInputMesh->m_iNumPolys; p++)
	{
		TMergePoly * pMergePoly = mergePolys[p];
		int iNumPavement = 0;

		for(int t=0; t<3; t++)
		{
			TMergePoly * pAdjPoly = pMergePoly->m_AdjacentPolys[t];

			if(pAdjPoly && pAdjPoly->m_colPolyData.GetIsPavement())
			{
				iNumPavement++;
			}
		}

		if(iNumPavement >= 2)
		{
			pMergePoly->m_colPolyData.SetIsPavement(true);
		}
	}


	//**************************************************************
	// now sort polys into the sorted list, based upon polygon area
	//**************************************************************
	for(p=0; p<pInputMesh->m_iNumPolys; p++)
	{
		TMergePoly * pMergePoly = mergePolys[p];
		SortMergePolyIntoList(pMergePoly, &sortedPolyList);
	}

#if __DBG_MERGING
	int iplynum=0;
	list<TMergePoly*>::iterator listIter;
	for(listIter=sortedPolyList.begin(); listIter!=sortedPolyList.end(); listIter++)
	{
		TMergePoly * pDbgPoly = *listIter;
		sprintf(tmpTxt, "%i) poly %i (area %.3f)\n", iplynum++, pDbgPoly->m_iIndex, pDbgPoly->m_fArea);
		OutputText(tmpTxt);
	}
#endif

	// don't need this list anymore
	mergePolys.clear();


#if __DEBUG_COMPARE_POLYS
	vector<TVec3> dbgPolyVerts1;
	dbgPolyVerts1.push_back(TVec3(-924.00f, 790.67f, 3.46f));
	dbgPolyVerts1.push_back(TVec3(-923.33f, 792.00f, 3.46f));
	dbgPolyVerts1.push_back(TVec3(-925.33f, 792.00f, 3.46f));
	vector<TVec3> dbgPolyVerts2;
	dbgPolyVerts2.push_back(TVec3(-924.00f, 790.67f, 3.46f));
	dbgPolyVerts2.push_back(TVec3(-922.00f, 790.67f, 3.46f));
	dbgPolyVerts2.push_back(TVec3(-923.33f, 792.00f, 3.46f));
#endif

	//**********************************************************
	//	Start merging the polygons.
	//**********************************************************

#if __PERFORM_MERGE

	int iNumIterationsWithoutAnyMerge = 0;
	int iNumTries = 0;
	int iMaxNumTries = 4;
	int iIterationNum = 0;
	int iNumMergesDone = 0;

	while( iNumTries < iMaxNumTries )
	{
		bool bQuit = false;

		if(m_Params.m_bDontDoPolygonMerging)
			bQuit=true;

		while(!bQuit)
		{
			// Attempt to merge as many pairs of adjacent polygons as we can
			if(!sortedPolyList.size())
				break;

			iIterationNum++;

			bool bMerged;

			//bMerged = Merge_AttemptToMergeMultipleAdjacentPolys(vertexList, sortedPolyList);

			
			TMergePoly * pTestPoly = *(sortedPolyList.begin());

			bMerged = Merge_AttemptToMergeAdjacentPairs(pTestPoly, sortedPolyList, vertexList);

			if(bMerged)
			{
				iNumIterationsWithoutAnyMerge = 0;
				iNumMergesDone++;
			}
			
			else
			{
				// If we tried merging all adjacent polys of this poly unsuccessfully, then we'll increment this counter
				iNumIterationsWithoutAnyMerge++;

				// We've done all the merging that we currently can with this poly
				// Let's shove it at the back of the sorted list, so we get round to
				// trying it again later..
				sortedPolyList.erase(sortedPolyList.begin());
				sortedPolyList.push_back(pTestPoly);
			}

			// If we've gone right through the list without any merges, then it's time to quit..
			if((u32)iNumIterationsWithoutAnyMerge == sortedPolyList.size())
			{
				bQuit = true;
			}
		}	// while !bQuit

#if __DBG_MERGING
		OutputText("\n");
#endif

		iNumIterationsWithoutAnyMerge = 0;


		//*********************************************************************************************
		// Now we ought to go through all the polys and remove all colinear edges which do not border
		// any other polygons.  We cannot remove interior colinear edges as this will break the
		// triangulation & adjacency.  NB : will interior collinear edges cause any problems??
		//*********************************************************************************************

#if __MERGE_REMOVE_COLINEAR_EDGES

		Merge_RemoveColinearEdges(sortedPolyList, vertexList);

#endif	// __MERGE_REMOVE_COLINEAR_EDGES
	
		iNumTries++;

	}// iNumTries

#endif	// __PERFORM_MERGE

	//******************************************************************************
	//	Here we have the opportunity to split polygons in certain areas of the map
	//	in order to deal with specific pathfinding situations.  Here is where we
	//	ensure that uprootable objects such as fences, cause navmesh polygons under
	//	them to be bisected along their major axis.

#if __SPLIT_POLYGONS_AFTER_MERGE
	SplitPolysAfterMerging(pInputMesh, sortedPolyList, vertexList, pSplitObjects);
#endif
	
	//**********************************************************************
	//  Sort the merged polys by ped-density.  Its a preqequisite of the
	//  in-game ped generation code that all the polygons with ped-density
	//  are stored at the very start of the m_Polys array.

	sortedPolyList.sort(ComparePolyPedDensityFn);

	bool g_bRandomizeMergePolys = false;
	if(g_bRandomizeMergePolys)
	{
		sortedPolyList.sort(RandomPolyCompareFn);
		sortedPolyList.sort(RandomPolyCompareFn);
		sortedPolyList.sort(RandomPolyCompareFn);
		sortedPolyList.sort(RandomPolyCompareFn);
	}

	//***********************************************************
	// Now set the m_iNumReferences to zero for each vertex -
	// and then go through all faces and accumulate references
	// to each vertex.  After this we'll go through the vertex
	// list and remove all those with zero references..
	//***********************************************************

	list<TMergePoly*>::iterator iter;
	int index=0;

	for(iter = sortedPolyList.begin(); iter != sortedPolyList.end(); iter++)
	{
		TMergePoly * pPoly = *iter;
		pPoly->m_iIndex = index++;
	}
	
	for(v=0; v<vertexList.size(); v++)
	{
		TVertexEntry * pEntry = vertexList[v];
		pEntry->m_iNumReferences = 0;
	}

	for(iter = sortedPolyList.begin(); iter != sortedPolyList.end(); iter++)
	{
		TMergePoly * pPoly = *iter;
		for(int i=0; i<pPoly->m_Vertices.size(); i++)
		{
			TVertexEntry * pEntry = pPoly->m_Vertices[i];
			pEntry->m_iNumReferences++;
		}
	}

	for(v=0; v<vertexList.size(); v++)
	{
		TVertexEntry * pEntry = vertexList[v];
		if(pEntry->m_iNumReferences == 0)
		{
			delete pEntry;
			vertexList.erase( vertexList.begin() + v );
			v--;
		}
	}

	//****************************************************************
	//  Weld identical vertices

	const float fWeldEps = 0.0001f;

	for(v=0; v<vertexList.size(); v++)
	{
		TVertexEntry * pVert1 = vertexList[v];

RESTART_VERTEX_WELD:

		for(u32 v2=v+1; v2<vertexList.size(); v2++)
		{
			TVertexEntry * pVert2 = vertexList[v2];

			if(pVert1->m_Vertex.IsClose( pVert2->m_Vertex, fWeldEps ) )
			{
				// Delete the 2nd occurance
				vertexList.erase( vertexList.begin() + v2 );

				// Retarget any references to v2, with v1
				for(iter = sortedPolyList.begin(); iter != sortedPolyList.end(); iter++)
				{
					TMergePoly * pPoly = *iter;
					pPoly->ReplaceVertex(pVert2, pVert1);
				}

				delete pVert2;
				goto RESTART_VERTEX_WELD;
			}
		}
	}



	//*********************************************************
	//	Mark each vertex & face with it's index, so we don't
	//	have to do inefficient lookup for each one..
	//*********************************************************

	for(v=0; v<vertexList.size(); v++)
	{
		TVertexEntry * pEntry = vertexList[v];
		pEntry->m_iIndex = v;
	}

	//*********************************************************
	//	Now we need to create a new CNavMesh with the new data
	//*********************************************************

	int iTotalNumEdges = 0;
	for(iter = sortedPolyList.begin(); iter != sortedPolyList.end(); iter++)
	{
		TMergePoly * pPoly = *iter;
		iTotalNumEdges += pPoly->m_AdjacentPolys.size();
	}

	// Do the vertices, in compressed format
	u16 * pVertexData = rage_new u16[vertexList.size()*3];
	for(v=0; v<vertexList.size(); v++)
	{
		u16 x,y,z;
		CompressVertex(vertexList[v]->m_Vertex, x, y, z, pInputMesh->m_pQuadTree->m_Mins, pInputMesh->m_vExtents);
		pVertexData[v*3]		= x;
		pVertexData[(v*3)+1]	= y;
		pVertexData[(v*3)+2]	= z;
	}

	CNavMesh * pOutputMesh = rage_new CNavMesh();

	TNavMeshPoly * pPolys = rage_new TNavMeshPoly[sortedPolyList.size()];
	TAdjPoly * pAdjPolys = rage_new TAdjPoly[iTotalNumEdges];
	u16 * pVertexIndices = rage_new u16[iTotalNumEdges];

	ZeroMemory(pPolys, sizeof(TNavMeshPoly) * sortedPolyList.size());
	ZeroMemory(pAdjPolys, sizeof(TAdjPoly) * iTotalNumEdges);
	ZeroMemory(pVertexIndices, sizeof(u16) * iTotalNumEdges);

	int iCurrentPoolIndex = 0;
	bool bFirstVertexIndexHasOverflowed = false;

	p = 0;
	for(iter = sortedPolyList.begin(); iter != sortedPolyList.end(); iter++)
	{
		TMergePoly * pPoly = *iter;

		pPolys[p].SetFlags(0);
		pPolys[p].SetNavMeshIndex(pInputMesh->m_iIndexOfMesh);

		if(pPoly->m_colPolyData.GetIsPavement())
		{
			pPolys[p].OrFlags(NAVMESHPOLY_IS_PAVEMENT);
		}

		pPolys[p].SetIsInterior(pPoly->m_colPolyData.GetIsInterior());

		if(pPoly->m_bInShelter)
		{
			pPolys[p].OrFlags(NAVMESHPOLY_IN_SHELTER);
		}
		if(pPoly->m_bWater)
		{
			pPolys[p].OrFlags(NAVMESHPOLY_IS_WATER);
		}
		if(pPoly->m_bTooSteep)
		{
			pPolys[p].OrFlags(NAVMESHPOLY_TOO_STEEP_TO_WALK_ON);
		}

		const bool bNoNetworkSpawn = pPoly->m_colPolyData.GetIsNoNetworkSpawn();
		pPolys[p].m_NonResourced.m_bNoNetworkSpawn = bNoNetworkSpawn;

		if(bNoNetworkSpawn)
			iNumFinalNoNetworkSpawn++;
		s32 iPedDensity = pPoly->m_colPolyData.GetPedDensity();

		if(iPedDensity < 0) iPedDensity = 0;
		if(iPedDensity > MAX_PED_DENSITY) iPedDensity = MAX_PED_DENSITY;
		pPolys[p].SetPedDensity(iPedDensity);

		pPolys[p].SetFirstVertexIndex(iCurrentPoolIndex);
		pPolys[p].SetNumVertices(pPoly->m_Vertices.size());

		if(iCurrentPoolIndex >= NAVMESH_MAX_INGAME_1ST_VERTEX_INDEX)
		{
			bFirstVertexIndexHasOverflowed = true;
		}

		Vector3 vMinXYZ(FLT_MAX, FLT_MAX, FLT_MAX);
		Vector3 vMaxXYZ(-FLT_MAX, -FLT_MAX, -FLT_MAX);

		for(v=0; v<(u32)pPoly->m_Vertices.size(); v++)
		{
			if(pPoly->m_Vertices[v]->m_Vertex.x < vMinXYZ.x) vMinXYZ.x = pPoly->m_Vertices[v]->m_Vertex.x;
			if(pPoly->m_Vertices[v]->m_Vertex.y < vMinXYZ.y) vMinXYZ.y = pPoly->m_Vertices[v]->m_Vertex.y;
			if(pPoly->m_Vertices[v]->m_Vertex.z < vMinXYZ.z) vMinXYZ.z = pPoly->m_Vertices[v]->m_Vertex.z;

			if(pPoly->m_Vertices[v]->m_Vertex.x > vMaxXYZ.x) vMaxXYZ.x = pPoly->m_Vertices[v]->m_Vertex.x;
			if(pPoly->m_Vertices[v]->m_Vertex.y > vMaxXYZ.y) vMaxXYZ.y = pPoly->m_Vertices[v]->m_Vertex.y;
			if(pPoly->m_Vertices[v]->m_Vertex.z > vMaxXYZ.z) vMaxXYZ.z = pPoly->m_Vertices[v]->m_Vertex.z;

			pVertexIndices[iCurrentPoolIndex] = (u16)pPoly->m_Vertices[v]->m_iIndex;

			pAdjPolys[iCurrentPoolIndex].SetAdjacencyType(ADJACENCY_TYPE_NORMAL);
			pAdjPolys[iCurrentPoolIndex].SetEdgeProvidesCover(false);

			if(pPoly->m_AdjacentPolys[v])
			{
				pAdjPolys[iCurrentPoolIndex].m_iAdjacentNavMeshIndex = pInputMesh->m_iIndexOfMesh;

				// TODO! Here is where we will first need to construct a lookup table of adjacent
				// navmeshes.  During this merge phase, there will be just two entries -
				// [0] NAVMESH_INDEX_NONE
				// [1] the index of pInputMesh

				pAdjPolys[iCurrentPoolIndex].SetNavMeshIndex(pInputMesh->m_iIndexOfMesh, pOutputMesh->GetAdjacentMeshes());
				
				int index = pPoly->m_AdjacentPolys[v]->m_iIndex;

				if(index == -1)
				{
					printf("ERROR - poly index in list was -1\n");
					DebuggerBreak();
				}
				else
				{
					pAdjPolys[iCurrentPoolIndex].SetPolyIndex(pPoly->m_AdjacentPolys[v]->m_iIndex);
				}
			}
			else
			{
				pAdjPolys[iCurrentPoolIndex].SetNavMeshIndex(NAVMESH_NAVMESH_INDEX_NONE, pOutputMesh->GetAdjacentMeshes());
				pAdjPolys[iCurrentPoolIndex].SetPolyIndex(NAVMESH_POLY_INDEX_NONE);
			}

			iCurrentPoolIndex++;
		}

		vMinXYZ -= TShortMinMax::ms_vExtendMinMax;
		vMaxXYZ += TShortMinMax::ms_vExtendMinMax;

		pPolys[p].m_MinMax.SetFloat(vMinXYZ.x, vMinXYZ.y, vMinXYZ.z, vMaxXYZ.x, vMaxXYZ.y, vMaxXYZ.z);

		p++;
	}

	char txt[256];
	bool bError = false;

	if(bFirstVertexIndexHasOverflowed)
	{
		bError = true;

		sprintf(txt, "**************************************************************************\n");
		printf(txt);
		OutputDebugString(txt);
		OutputText(txt);

		sprintf(txt, "ERROR - iFirstVertexIndex %i has overflowed (max=%i)\n", iCurrentPoolIndex, NAVMESH_MAX_INGAME_1ST_VERTEX_INDEX);
		printf(txt);
		OutputDebugString(txt);
		OutputText(txt);		
	}

	if(vertexList.size() > NAVMESH_MAX_INGAME_POOLSIZE)
	{
		bError = true;

		sprintf(txt, "**************************************************************************\n");
		printf(txt);
		OutputDebugString(txt);
		OutputText(txt);

		sprintf(txt, "ERROR - iPoolIndex %i has overflowed (max=%i)\n", iCurrentPoolIndex, NAVMESH_MAX_INGAME_POOLSIZE);
		printf(txt);
		OutputDebugString(txt);
		OutputText(txt);
	}

	if(bError)
	{
		sprintf(txt, "  Too many polys in this navmesh, perhaps use a lower sampling resolution?\n");
		printf(txt);
		OutputDebugString(txt);
		OutputText(txt);

		sprintf(txt, "  Compilation will continue, but this navmesh will be broken...\n");
		printf(txt);
		OutputDebugString(txt);

		sprintf(txt, "**************************************************************************\n");
		printf(txt);
		OutputDebugString(txt);
		OutputText(txt);

		//DebuggerBreak();
	}

#if __ASSERT
	if( ms_iVerbosity > 0 )
	{
		sprintf(txt, "\n\nMERGING RESULTS (%i / %i)\n\n", sortedPolyList.size(), pInputMesh->m_iNumPolys);
		OutputText(txt);
		OutputDebugString("Merged Navmesh Stats:\n");
		sprintf(txt, "iNumVertices:%i iNumFaces:%i iSizeOfPools:%i\n", vertexList.size(), sortedPolyList.size(), iTotalNumEdges);
		OutputDebugString(txt);
	}
#endif

	pOutputMesh->m_iFlags = pInputMesh->m_iFlags | NAVMESH_COMPRESSED_VERTEX_DATA;
	pOutputMesh->m_iNumVertices = (u32)vertexList.size();
	pOutputMesh->m_iNumPolys = (u32)sortedPolyList.size();

	pOutputMesh->m_CompressedVertexArray = new aiSplitArray<CNavMeshCompressedVertex, NAVMESH_COMPRESSEDVERTEX_ARRAY_BLOCKSIZE>(pOutputMesh->m_iNumVertices, (CNavMeshCompressedVertex*)pVertexData);
	delete[] pVertexData;
	pVertexData = NULL;

	pOutputMesh->m_PolysArray = new aiSplitArray<TNavMeshPoly,NAVMESH_POLY_ARRAY_BLOCKSIZE>(pOutputMesh->m_iNumPolys, pPolys);
	pOutputMesh->SetPolysSubArrayIndices();
	delete[] pPolys;
	pPolys = NULL;

	pOutputMesh->m_iSizeOfPools = iTotalNumEdges;

	pOutputMesh->m_VertexIndexArray = new aiSplitArray<u16, NAVMESH_VERTEXINDEX_ARRAY_BLOCKSIZE>(pOutputMesh->m_iSizeOfPools, pVertexIndices);
	delete[] pVertexIndices;
	pVertexIndices = NULL;

	pOutputMesh->m_AdjacentPolysArray = new aiSplitArray<TAdjPoly,NAVMESH_ADJPOLY_ARRAY_BLOCKSIZE>(pOutputMesh->m_iSizeOfPools, pAdjPolys);
	delete[] pAdjPolys;
	pAdjPolys = NULL;

	pOutputMesh->m_iIndexOfMesh = pInputMesh->m_iIndexOfMesh;

	pOutputMesh->m_pQuadTree = rage_new CNavMeshQuadTree();
	pOutputMesh->m_pQuadTree->m_Mins = pInputMesh->m_pQuadTree->m_Mins;
	pOutputMesh->m_pQuadTree->m_Maxs = pInputMesh->m_pQuadTree->m_Maxs;

	pOutputMesh->m_pNonResourcedData->m_vMinOfNavMesh = pInputMesh->m_pQuadTree->m_Mins;

	pOutputMesh->m_vExtents.x = pInputMesh->m_vExtents.x;
	pOutputMesh->m_vExtents.y = pInputMesh->m_vExtents.y;
	pOutputMesh->m_vExtents.z = pInputMesh->m_vExtents.z;

	//**********************************************************
	// Now delete all the data we created during the processing
	//**********************************************************

	for(v=0; v<vertexList.size(); v++)
	{
		TVertexEntry * vec = vertexList[v];
		delete vec;
	}
	vertexList.clear();

	for(iter = sortedPolyList.begin(); iter != sortedPolyList.end(); iter++)
	{
		TMergePoly * poly = *iter;
		delete poly;
	}
	sortedPolyList.clear();

#if __NAVMESH_SANITY_CHECK_ADJACENCIES
	pInputMesh->SanityCheckAdjacencies();
#endif

	return pOutputMesh;
}

bool PolyListContains(atArray<TMergePoly*> & polys, TMergePoly * pPoly)
{
	for(s32 i=0; i<polys.GetCount(); i++)
		if(polys[i] == pPoly)
			return true;
	return false;
}
bool GetSharedEdgeVerts(TMergePoly * pPoly1, TMergePoly * pPoly2, TVertexEntry ** pVert1, TVertexEntry ** pVert2)
{
	
	*pVert1 = NULL;
	*pVert2 = NULL;
	s32 i,j;
	for(i=0; i<pPoly1->m_Vertices.size(); i++)
	{
		TVertexEntry * v1 = pPoly1->m_Vertices[i];
		for(j=0; j<pPoly2->m_Vertices.size(); j++)
		{
			TVertexEntry * v2 = pPoly2->m_Vertices[j];
			if(v1 == v2)
			{
				if(!*pVert1)
					*pVert1 = v1;
				else if(!*pVert2)
					*pVert2 = v2;
			}
		}
	}

	return(*pVert1 && *pVert2);
}

// Sort the list of surrounding polys in a clockwise order

bool CNavGen::TSortMergePoly::operator () (const TMergePoly * pPoly1, const TMergePoly * pPoly2) const
{
	Vector3 vEdge1 = VEC3_ZERO;
	Vector3 vEdge2 = VEC3_ZERO;

	s32 v;
	for(v=0; v<pPoly1->m_Vertices.size(); v++)
	{
		if(pPoly1->m_Vertices[v] == pCentralVert)
		{
			vEdge1 = pPoly1->m_Vertices[ (v+1)%pPoly1->m_Vertices.size() ]->m_Vertex - pPoly1->m_Vertices[v]->m_Vertex;
			vEdge1.Normalize();
			break;
		}
	}
	for(v=0; v<pPoly2->m_Vertices.size(); v++)
	{
		if(pPoly2->m_Vertices[v] == pCentralVert)
		{
			vEdge2 = pPoly2->m_Vertices[ (v+1)%pPoly2->m_Vertices.size() ]->m_Vertex - pPoly2->m_Vertices[v]->m_Vertex;
			vEdge2.Normalize();
			break;
		}
	}

	Assert(vEdge1.Mag2() > 0.001f && vEdge2.Mag2() > 0.001f);

	const float fAngle1 = atan2f(vEdge1.y, vEdge1.x) + PI;
	const float fAngle2 = atan2f(vEdge2.y, vEdge2.x) + PI;

	return (fAngle1 < fAngle2);
}

// Sort the list of surrounding polys in a clockwise order
void CNavGen::Merge_SortPolysSurroundingVertex(TVertexEntry * pVert)
{
	TSortMergePoly sorter;
	sorter.pCentralVert = pVert;
	std::sort( pVert->m_SurroundingPolys.begin(), pVert->m_SurroundingPolys.end(), sorter );
}


bool CNavGen::Merge_AttemptToMergeMultipleAdjacentPolys(vector<TVertexEntry*> & vertexList, list<TMergePoly*> & /*sortedPolyList*/)
{
	const float fMaxArea = m_Params.m_fMaxTriangleArea;
	const float fMaxAreaForWaterPoly = fMaxArea * 35.0f;

	u32 v,j;
	s32 p,i;
	for(v=0; v<vertexList.size(); v++)
	{
		TVertexEntry * pVert = vertexList[v];
		const int iNumPolys = pVert->m_SurroundingPolys.size();

		// Create a list of the unique vertices in all polygons surrounding pVert
		// NB: If pVert is completely enclosed by polygons, then pVert itself will be removed -
		// we should identify this case.. somehow!

		int iNumWater = 0;
		int iNumUnderwater = 0;
		int iNumTooSteep = 0;
		int iNumInterior = 0;
		int iNumPavement = 0;
		float fTotalArea = 0.0f;
		//int iNumFinalVerts = 0;
		vector<TVertexEntry*> finalVerts;

		for(p=0; p<pVert->m_SurroundingPolys.size(); p++)
		{
			TMergePoly * pPoly = pVert->m_SurroundingPolys[p];

			for(i=0; i<pPoly->m_Vertices.size(); i++)
			{
				for(j=0; j<finalVerts.size(); j++)
					if(finalVerts[j] == pPoly->m_Vertices[i])
						break;

				if(j==finalVerts.size())
					finalVerts.push_back(pPoly->m_Vertices[i]);
			}

			if(pPoly->m_bWater)
				iNumWater++;
			if(pPoly->m_bTooSteep)
				iNumTooSteep++;
			if(pPoly->m_colPolyData.GetIsPavement())
				iNumPavement++;
			if(pPoly->m_colPolyData.GetIsInterior())
				iNumInterior++;

			fTotalArea += pPoly->m_fArea;
		}

		if(finalVerts.size() >= NAVMESHPOLY_MAX_NUM_VERTICES)
		{
			continue;
		}
		if((iNumWater > 0 && iNumWater != iNumPolys) ||
			(iNumUnderwater > 0 && iNumUnderwater != iNumPolys) ||
			(iNumTooSteep > 0 && iNumTooSteep != iNumPolys) || 
			(iNumPavement > 0 && iNumPavement != iNumPolys) ||
			(iNumInterior > 0 && iNumInterior != iNumPolys))
		{
			continue;
		}

		TMergePoly * pFirstPoly = pVert->m_SurroundingPolys[0];

		const float fMaxAreaForThisPoly = pFirstPoly->m_bWater ? fMaxAreaForWaterPoly : fMaxArea;
		if(fTotalArea > fMaxAreaForThisPoly)
		{
			continue;
		}

		//--------------------------------------------------------
		// Are these polygons coplanar enough to merge together?

		const float fCoplanarPlaneTestEps = 0.25f;
		bool bCoplanar = true;

		for(p=0; p<pVert->m_SurroundingPolys.size() && bCoplanar; p++)
		{
			TMergePoly * pPoly = pVert->m_SurroundingPolys[p];

			for(i=0; i<(s32)finalVerts.size(); i++)
			{
				float d = DotProduct( finalVerts[i]->m_Vertex, pPoly->m_vPlaneNormal ) + pPoly->m_fPlaneDist;
				if( Abs(d) > fCoplanarPlaneTestEps )
				{
					bCoplanar = false;
					break;
				}
			}
		}
		if(!bCoplanar)
			continue;

		//---------------------------------------------------------------------
		// Now test whether merging these polys will create a convex polygon

		const float fConvexPlaneTestEps = 0.01f;
		bool bConvex = true;

		for(p=0; p<pVert->m_SurroundingPolys.size() && bConvex; p++)
		{
			TMergePoly * pPoly = pVert->m_SurroundingPolys[p];

			for(i=0; i<pPoly->m_AdjacentPolys.size() && bConvex; i++)
			{
				TMergePoly * pEdgePoly = pPoly->m_AdjacentPolys[i];
				if(!PolyListContains(pVert->m_SurroundingPolys, pEdgePoly))
				{
					// Ok, this is an external edge...
					TVertexEntry * pVert1, *pVert2;
					if( GetSharedEdgeVerts(pPoly, pEdgePoly, &pVert1, &pVert2) )
					{
						// create a perpendicular plane from this edge, and test all the other verts against it
						Vector3 v1 = pVert1->m_Vertex;
						v1.z = 0.0f;
						Vector3 v2 = pVert2->m_Vertex;
						v2.z = 0.0f;
						Vector3 vEdge = v1 - v2;
						vEdge.Normalize();

						Vector3 vEdgePlaneNormal = CrossProduct(vEdge, Vector3(0.0f,0.0f,1.0f) );
						float fEdgePlaneDist = - DotProduct(vEdgePlaneNormal, v2);

						for(j=0; j<finalVerts.size(); j++)
						{
							float d = DotProduct( finalVerts[j]->m_Vertex, vEdgePlaneNormal ) + fEdgePlaneDist;
							if( d < fConvexPlaneTestEps )
							{
								bConvex = false;
								break;
							}
						}
					}
				}
			}
		}

		if( !bConvex )
			continue;

		//------------------------------------------------------------------------------------------------------
		// Right.. Now create a single merged polygon from the surrounding polys - this is the complicated part
		
		
		return true;
	}

	return false;
}

bool CNavGen::Merge_AttemptToMergeAdjacentPairs(TMergePoly * pTestPoly, list<TMergePoly*> & sortedPolyList, vector<TVertexEntry*> & vertexList)
{
	if(pTestPoly->m_bDoNotMerge)
		return false;

	u32 p,e;
	const float fMaxArea = m_Params.m_fMaxTriangleArea;
	const float fMaxAreaForWaterPoly = fMaxArea * 35.0f;

	for(p=0; p<(u32)pTestPoly->m_AdjacentPolys.size(); p++)
	{
		if(g_bProcessInBackground) Sleep(0);

		TMergePoly * pAdjPoly = pTestPoly->m_AdjacentPolys[p];

		if(!pAdjPoly)
			continue;

		if(pAdjPoly->m_bDoNotMerge)
			continue;

#if __DBG_MERGING
		sprintf(tmpTxt, "(considering merging poly %i with %i)\n", pTestPoly->m_iIndex, pAdjPoly->m_iIndex);
		OutputText(tmpTxt);
#endif

#if __DEBUG_COMPARE_POLYS
		static bool bPause = false;
		if((pTestPoly->DoesPolyMatch(dbgPolyVerts1, 0.5f) && pAdjPoly->DoesPolyMatch(dbgPolyVerts2, 0.5f)) ||
			(pTestPoly->DoesPolyMatch(dbgPolyVerts2, 0.5f) && pAdjPoly->DoesPolyMatch(dbgPolyVerts1, 0.5f)))
		{
			bPause = true;
		}
#endif

		int iTestPolyNumVertices = pTestPoly->m_Vertices.size();
		int iAdjPolyNumVertices = pAdjPoly->m_Vertices.size();

		// Make sure this poly doesn't have too many vertices
		if((iTestPolyNumVertices + iAdjPolyNumVertices - 2) >= NAVMESHPOLY_MAX_NUM_VERTICES)
		{
			DBG_MERGING_ONLY(OutputText("(no - would exceed max verts)\n"));
			continue;
		}

		if(pTestPoly->m_bWater != pAdjPoly->m_bWater)
		{
			DBG_MERGING_ONLY(OutputText("(no - water & not water)\n"));
			continue;
		}

		// Only merge polys whose clear space above is reasonably similar
		static const s32 iMaxHeightAboveZDiff = 5;
		
		const s32 iZHeightAbove1 = pTestPoly->m_iFreeSpaceTopZ;
		const s32 iZHeightAbove2 = pAdjPoly->m_iFreeSpaceTopZ;
		const s32 iHeightAboveZDiff = Abs(iZHeightAbove1 - iZHeightAbove2);
		if(iHeightAboveZDiff > iMaxHeightAboveZDiff)
		{
			DBG_MERGING_ONLY(OutputText("(no - height diff mismatch)\n"));
			continue;
		}

		if(pTestPoly->m_bTooSteep != pAdjPoly->m_bTooSteep)
		{
			DBG_MERGING_ONLY(OutputText("(no - too steep & not too steep)\n"));
			continue;
		}

		if(pTestPoly->m_bIntersectsEntities != pAdjPoly->m_bIntersectsEntities)
		{
			DBG_MERGING_ONLY(OutputText("(no - intersects & not intersects entities)\n"));
			continue;
		}

		if(pTestPoly->m_colPolyData.GetIsNoNetworkSpawn() != pAdjPoly->m_colPolyData.GetIsNoNetworkSpawn())
		{
			DBG_MERGING_ONLY(OutputText("(no - networkspawn/no networkspawn)\n"));
			continue;
		}

		if(pTestPoly->m_colPolyData.GetIsInterior() != pAdjPoly->m_colPolyData.GetIsInterior())
		{
			DBG_MERGING_ONLY(OutputText("(no - interior & not interior)\n"));
			continue;
		}

		const float fMaxAreaForThisPoly = pTestPoly->m_bWater ? fMaxAreaForWaterPoly : fMaxArea;

		// NB : We should have a test here that the poly will not exceed a certain
		// size, either based on area or on max edge-length - or both.
		if((pTestPoly->m_fArea + pAdjPoly->m_fArea) > fMaxAreaForThisPoly)
		{
			DBG_MERGING_ONLY(OutputText("(no - would exceed max poly area)\n"));
			continue;
		}

		// We cannot merge polys of different surface types, if we care about either of the types
		if(m_bDoWeCareAboutSurfaceTypes)
		{
			if(!CanWeMergeThesePolys(pTestPoly, pAdjPoly))
			{
				DBG_MERGING_ONLY(OutputText("(no - CanWeMergeThesePolys() returned false)\n"));
				continue;
			}
		}

		// Make sure both polys are coplanar enough
		if(!ArePolysCoplanarEnoughToMerge(pTestPoly, pAdjPoly))
		{
			DBG_MERGING_ONLY(OutputText("(no - not coplanar enough)\n"));
			continue;
		}

		// Find which adjacent edge this is in the 2nd poly
		for(e=0; (s32)e<pAdjPoly->m_AdjacentPolys.size(); e++)
		{
			if(pAdjPoly->m_AdjacentPolys[e] == pTestPoly)
				break;
		}
		if((s32)e == pAdjPoly->m_AdjacentPolys.size())
		{
			// ERROR - we've found an adjacency which only links one way..
			printf("WARNING - we've found an adjacency which only links one way\n");
			//DebuggerBreak();
			continue;
		}

		// Make sure that the resultant poly will be convex
		if(!WillMergedPolyBeConvex(pTestPoly, p, pAdjPoly, e))
		{
			DBG_MERGING_ONLY(OutputText("(no - resultant poly will not be convex)\n"));
			continue;
		}


		//***************************************************
		// Okay, it looks like we can indeed merge these two
		// polys.. Let's do so then..
		//
		// Well merge pAdjPoly with pTestPoly.  pTestPoly will
		// become the new polygon, while pAdjPoly will be
		// destroyed.  We'll need to replace references to
		// pAdjPoly in all polys, with pTestPoly.
		//***************************************************

		// If this returns FALSE, then it means the resultant poly
		// was too large.  If polys are too large, then the pathfinding
		// suffers as a consequence.

#if __DBG_MERGING
		sprintf(tmpTxt, "%i) Merging poly %i & %i", iNumMergesDone, pTestPoly->m_iIndex, pAdjPoly->m_iIndex);
		OutputText(tmpTxt);
#endif

		if(!MergePolys(pTestPoly, p, pAdjPoly, e))
		{
#if __DBG_MERGING
			OutputText("  FAIL\n");
#endif
			continue;
		}

#if __DBG_MERGING
		OutputText("\n");
#endif

		//*******************************************************************************

		RemoveReferenceToMergedPoly(pAdjPoly, pTestPoly, &sortedPolyList, &vertexList);
		delete pAdjPoly;

		// Now we should remove the newly merged poly from its position in the
		// sorted list, and re-insert according to its new area.

		sortedPolyList.erase( sortedPolyList.begin() );
		SortMergePolyIntoList(pTestPoly, &sortedPolyList);

		return true;
	}

	return false;
}

bool CNavGen::Merge_RemoveColinearEdges(list<TMergePoly*> & sortedPolyList, vector<TVertexEntry*> & vertexList)
{
	u32 p;

	list<TMergePoly*>::iterator iter;

	static const float fColinearDotProduct = Cosf(2.0f * DtoR);

restart_for_all_polys:

	int iNumVerticesRemoved = 0;

	int count =0;
	for(iter = sortedPolyList.begin(); iter != sortedPolyList.end(); iter++)
	{
		TMergePoly * pPoly = *iter;
		Vector3 vLastEdge;
		Vector3 vThisEdge;

restart_for_this_poly:

		if(pPoly->m_Vertices.size() < 3)
		{
			RemoveReferenceToMergedPoly(pPoly, NULL, &sortedPolyList, &vertexList);
			delete pPoly;
			goto restart_for_all_polys;
		}

		int iPolyNumVerts = pPoly->m_Vertices.size();
		int lastp = iPolyNumVerts - 1;
		int nextp;

		for(p=0; p<(u32)iPolyNumVerts; p++)
		{
			nextp = (p+1) % iPolyNumVerts;

			vLastEdge = pPoly->m_Vertices[p]->m_Vertex - pPoly->m_Vertices[lastp]->m_Vertex;
			vThisEdge = pPoly->m_Vertices[nextp]->m_Vertex - pPoly->m_Vertices[p]->m_Vertex;

			vLastEdge.Normalize();
			vThisEdge.Normalize();
			float fDot = DotProduct(vLastEdge, vThisEdge);

			// Collinear edge.  If there is no adjacency on the other side
			// of both edges, then we can safely remove it.
			// Also if there IS adjacency on the other side, but it is the same
			// polygon - then we can remove it (in face this is necessary for the
			// pathfinding algorithm - there may only by one entry point from any
			// polygon to any other polygon).
			if(fDot >= fColinearDotProduct)
			{
				TMergePoly * pAdjacentLastP = pPoly->m_AdjacentPolys[lastp];
				TMergePoly * pAdjacentP = pPoly->m_AdjacentPolys[p];

				if(pAdjacentLastP == pAdjacentP)
				{
					pPoly->m_Vertices.erase( pPoly->m_Vertices.begin() + p );
					pPoly->m_AdjacentPolys.erase( pPoly->m_AdjacentPolys.begin() + p );

					if(pPoly->m_Vertices.size() < 3)
					{
						RemoveReferenceToMergedPoly(pPoly, NULL, &sortedPolyList, &vertexList);
						delete pPoly;
						goto restart_for_all_polys;
					}

					iNumVerticesRemoved++;

					pPoly->CalcAreaAndBounds();

					goto restart_for_this_poly;
				}
			}
			lastp = p;
		}

		count++;
	}

	return true;
}


void
CNavGen::CheckMergePolyAdjacency(TMergePoly * pPoly)
{
	if(pPoly->m_Vertices.size() != pPoly->m_AdjacentPolys.size())
	{
		DebuggerBreak();
	}

	s32 a, a2;
	for(a=0; a<pPoly->m_AdjacentPolys.size(); a++)
	{
		TMergePoly * pAdjPoly = pPoly->m_AdjacentPolys[a];
		if(!pAdjPoly)
			continue;

		for(a2=0; a2<pAdjPoly->m_AdjacentPolys.size(); a2++)
		{
			TMergePoly * pAdjPoly2 = pAdjPoly->m_AdjacentPolys[a2];
			if(pAdjPoly2 == pPoly)
				break;
		}
		if(a2 == pAdjPoly->m_AdjacentPolys.size())
		{
			char tmpBuf[256];
			s32 m;

			OutputDebugString("WARNING - adjacent poly doesn't link back to poly\n");
			printf("WARNING - adjacent poly doesn't link back to poly\n");

			OutputDebugString("Poly:\n");
			for(m=0; m<pPoly->m_AdjacentPolys.size(); m++)
			{
				TMergePoly * ply = pPoly->m_AdjacentPolys[m];
				sprintf(tmpBuf, "0x%X ", ply);
				OutputDebugString(tmpBuf);
				printf(tmpBuf);
			}
			OutputDebugString("\n");
			OutputDebugString("Adjacent Poly\n");
			for(m=0; m<pAdjPoly->m_AdjacentPolys.size(); m++)
			{
				TMergePoly * ply = pAdjPoly->m_AdjacentPolys[m];
				sprintf(tmpBuf, "0x%X ", ply);
				OutputDebugString(tmpBuf);
			}
			OutputDebugString("\n");

			//DebugBreak();
		}
	}
}







void
CheckForDuplicatePolys(TMergePoly * pPoly, list<TMergePoly*> & polyList)
{
	int iNumPolyVertices = pPoly->m_Vertices.size();
	int iNumListPolyVertices;
	int iNumSame;
	s32 v1,v2;

	list<TMergePoly*>::iterator iter;
	for(iter = polyList.begin(); iter != polyList.end(); iter++)
	{
		TMergePoly * pListPoly = *iter;
		if(pPoly == pListPoly)
			continue;

		iNumListPolyVertices = pListPoly->m_Vertices.size();
		iNumSame = 0;

		for(v1=0; v1<pPoly->m_Vertices.size(); v1++)
		{
			for(v2=0; v2<pListPoly->m_Vertices.size(); v2++)
			{
				if(pPoly->m_Vertices[v1]->m_iIndex == pListPoly->m_Vertices[v2]->m_iIndex)
				{
					iNumSame++;
				}
			}
		}

		if(iNumSame >= iNumPolyVertices || iNumSame >= iNumListPolyVertices)
		{
			DebuggerBreak();
		}
	}
}





void CNavGen::RemoveReferenceToBisectedPoly(TMergePoly * pRemovedPoly, list<TMergePoly*> * polyList, vector<TVertexEntry*> * vertexList)
{
	//u32 v;
	list<TMergePoly*>::iterator removedPolyIter;
	list<TMergePoly*>::iterator iter;
	vector<TVertexEntry*>::iterator vertexIter;

	for(iter = polyList->begin(); iter != polyList->end(); iter++)
	{
		TMergePoly * pPoly = *iter;

		if(pPoly == pRemovedPoly)
		{
			polyList->erase(iter);
			break;
		}
	}

	/*
	for(iter = polyList->begin(); iter != polyList->end(); iter++)
	{
		TMergePoly * pPoly = *iter;

		if(pPoly)
		{
			for(v=0; (s32)v<pPoly->m_AdjacentPolys.size(); v++)
			{
				TMergePoly * pAdjPoly = pPoly->m_AdjacentPolys[v];
				Assert(pAdjPoly != pRemovedPoly);

//				if(pAdjPoly && pAdjPoly == pRemovedPoly)
//				{
//					pPoly->m_AdjacentPolys[v] = pReplacedWithPoly;
//				}
			}
		}
	}
	*/

	for(vertexIter=vertexList->begin(); vertexIter!=vertexList->end(); vertexIter++)
	{
		TVertexEntry * pVert = *vertexIter;
		pVert->RemoveSurroundingPoly(pRemovedPoly);
	}
}

void CNavGen::RemoveReferenceToMergedPoly(TMergePoly * pRemovedPoly, TMergePoly * pReplacedWithPoly, list<TMergePoly*> * polyList, vector<TVertexEntry*> * vertexList)
{
	u32 v;
	list<TMergePoly*>::iterator removedPolyIter;
	list<TMergePoly*>::iterator iter;
	vector<TVertexEntry*>::iterator vertexIter;

	for(iter = polyList->begin(); iter != polyList->end(); iter++)
	{
		TMergePoly * pPoly = *iter;

		if(pPoly == pRemovedPoly)
		{
			polyList->erase(iter);
			break;
		}
	}

	for(iter = polyList->begin(); iter != polyList->end(); iter++)
	{
		TMergePoly * pPoly = *iter;

		if(pPoly)
		{
			for(v=0; (s32)v<pPoly->m_AdjacentPolys.size(); v++)
			{
				TMergePoly * pAdjPoly = pPoly->m_AdjacentPolys[v];
				if(pAdjPoly && pAdjPoly == pRemovedPoly)
				{
					pPoly->m_AdjacentPolys[v] = pReplacedWithPoly;
				}
			}
		}
	}

	for(vertexIter=vertexList->begin(); vertexIter!=vertexList->end(); vertexIter++)
	{
		TVertexEntry * pVert = *vertexIter;
		if(pVert->RemoveSurroundingPoly(pRemovedPoly))
		{
			if(!pVert->HasSurroundingPoly(pReplacedWithPoly))
			{
				pVert->m_SurroundingPolys.Grow() = pReplacedWithPoly;
			}
		}
	}
}


void
CNavGen::EstablishMergePolysAdjacency(vector<TMergePoly*> * pPolyList)
{
	u32 p1,p2;
	s32 v1,v2,lastv1,lastv2;

	TVertexEntry * poly1Vertex1, * poly1Vertex2, * poly2Vertex1, * poly2Vertex2;

	for(p1=0; p1<pPolyList->size(); p1++)
	{
		TMergePoly * pPoly1 = (*pPolyList)[p1];

		for(p2=0; p2<pPolyList->size(); p2++)
		{
			if(p1 == p2)
				continue;

			TMergePoly * pPoly2 = (*pPolyList)[p2];

			// We can never merge water & non-water polys
			if(pPoly1->m_bWater != pPoly2->m_bWater)
				continue;

			lastv1 = pPoly1->m_Vertices.size()-1;
			for(v1=0; v1<pPoly1->m_Vertices.size(); v1++)
			{
				poly1Vertex1 = pPoly1->m_Vertices[lastv1];
				poly1Vertex2 = pPoly1->m_Vertices[v1];

				lastv2 = pPoly2->m_Vertices.size()-1;
				for(v2=0; v2<pPoly2->m_Vertices.size(); v2++)
				{
					poly2Vertex1 = pPoly2->m_Vertices[lastv2];
					poly2Vertex2 = pPoly2->m_Vertices[v2];

					if((poly1Vertex1 == poly2Vertex1 && poly1Vertex2 == poly2Vertex2) ||
						(poly1Vertex1 == poly2Vertex1 && poly1Vertex2 == poly2Vertex2))
					{
						// We've found the adjacent poly
						pPoly1->m_AdjacentPolys.Grow() = pPoly2;
						break;
					}

					lastv2 = v2;
				}

				// If no adjacent poly found, we add a NULL
				if(v2 == pPoly2->m_Vertices.size())
				{
					pPoly1->m_AdjacentPolys.Grow() = NULL;
				}

				lastv1 = v1;
			}
		}
	}
}


// Sorts the TMergePoly into the list.  The list is maintained in ascending order
// of polygon area, so those with least area are first in the list.
void
CNavGen::SortMergePolyIntoList(TMergePoly * pPoly, list<TMergePoly*> * pPolyList)
{
	list<TMergePoly*>::iterator iter = pPolyList->begin();

	while(iter != pPolyList->end())
	{
		TMergePoly * pListPoly = *iter;

		if(pPoly->m_fArea < pListPoly->m_fArea)
		{
			pPolyList->insert(iter, pPoly);
			return;
		}

		iter++;
	}

	pPolyList->push_back(pPoly);
}


bool
CNavGen::ArePolysCoplanarEnoughToMerge(TMergePoly * pPoly1, TMergePoly * pPoly2)
{
	static const float fPlaneEps = 0.25f;	//0.1f;//0.05f;
	s32 v;

	// Classify poly 2's vertices against poly 1's plane eq
	for(v=0; v<pPoly2->m_Vertices.size(); v++)
	{
		float dist = DotProduct(pPoly1->m_vPlaneNormal, pPoly2->m_Vertices[v]->m_Vertex) + pPoly1->m_fPlaneDist;
		if(Abs(dist) > fPlaneEps)
			return false;
	}
	// Classify poly 1's vertices against poly 2's plane eq
	for(v=0; v<pPoly1->m_Vertices.size(); v++)
	{
		float dist = DotProduct(pPoly2->m_vPlaneNormal, pPoly1->m_Vertices[v]->m_Vertex) + pPoly2->m_fPlaneDist;
		if(Abs(dist) > fPlaneEps)
			return false;
	}

	return true;
}


bool CNavGen::WillMergedPolyBeConvex(TMergePoly * pPoly1, int pPoly1MergeEdge, TMergePoly * pPoly2, int pPoly2MergeEdge)
{
	Vector3 m_vPolyVertices[NAVMESHPOLY_MAX_NUM_VERTICES*2];
	Vector3 m_vPolyEdgePlaneNormals[NAVMESHPOLY_MAX_NUM_VERTICES*2];
	float m_vPolyEdgePlaneDists[NAVMESHPOLY_MAX_NUM_VERTICES*2];


	s32 v,lastv;

	int iNumPlanes = 0;
	int iNumVertices = 0;

	// Make the list of pPoly1's planes & vertices
	lastv = pPoly1->m_Vertices.size()-1;
	for(v=0; v<pPoly1->m_Vertices.size(); v++)
	{
		// Add vertex
		m_vPolyVertices[iNumVertices] = pPoly1->m_Vertices[lastv]->m_Vertex;
		m_vPolyVertices[iNumVertices].z = 0.0f;	// not interested in elevation
		iNumVertices++;

		// But not including the edge we're gonna remove
		if(lastv != pPoly1MergeEdge)
		{
			Vector3 v1 = pPoly1->m_Vertices[v]->m_Vertex;
			v1.z = 0.0f;
			Vector3 v2 = pPoly1->m_Vertices[lastv]->m_Vertex;
			v2.z = 0.0f;

			//Vector3 vEdge = pPoly1->m_Vertices[v]->m_Vertex - pPoly1->m_Vertices[lastv]->m_Vertex;
			Vector3 vEdge = v1 - v2;
			vEdge.Normalize();

			//m_vPolyEdgePlaneNormals[iNumPlanes] = CrossProduct(vEdge, pPoly1->m_vPlaneNormal);
			//m_vPolyEdgePlaneDists[iNumPlanes] = - DotProduct(m_vPolyEdgePlaneNormals[iNumPlanes], pPoly1->m_Vertices[lastv]->m_Vertex);

			m_vPolyEdgePlaneNormals[iNumPlanes] = CrossProduct(vEdge, Vector3(0.0f,0.0f,1.0f) );
			m_vPolyEdgePlaneDists[iNumPlanes] = - DotProduct(m_vPolyEdgePlaneNormals[iNumPlanes], v2);

			iNumPlanes++;
		}

		lastv = v;
	}

	// Make the list of pPoly2's planes & vertices
	lastv = pPoly2->m_Vertices.size()-1;
	for(v=0; v<pPoly2->m_Vertices.size(); v++)
	{
		// Add vertex, but not if the vertex either side of the edge to remove
		int prevv = lastv-1;
		if(prevv < 0)
			prevv += pPoly2->m_Vertices.size();

		if(lastv != pPoly2MergeEdge && prevv != pPoly2MergeEdge)
		{
			m_vPolyVertices[iNumVertices] = pPoly2->m_Vertices[lastv]->m_Vertex;
			m_vPolyVertices[iNumVertices].z = 0.0f;
			iNumVertices++;
		}

		// But not including the edge we're gonna remove
		if(lastv != pPoly2MergeEdge)
		{
			Vector3 v1 = pPoly2->m_Vertices[v]->m_Vertex;
			v1.z = 0.0f;
			Vector3 v2 = pPoly2->m_Vertices[lastv]->m_Vertex;
			v2.z = 0.0f;

			Vector3 vEdge = v1 - v2;
			vEdge.Normalize();

			m_vPolyEdgePlaneNormals[iNumPlanes] = CrossProduct(vEdge, Vector3(0.0f,0.0f,1.0f) );
			m_vPolyEdgePlaneDists[iNumPlanes] = - DotProduct(m_vPolyEdgePlaneNormals[iNumPlanes], v2);
			iNumPlanes++;
		}

		lastv = v;
	}

	if(iNumVertices >= NAVMESHPOLY_MAX_NUM_VERTICES || iNumPlanes >= NAVMESHPOLY_MAX_NUM_VERTICES)
	{
		char txt[256];

		sprintf(txt, "******************************************************************\n");
		printf(txt);
		OutputText(txt);

		sprintf(txt, "ERROR - Some poly has exceeded the max num vertices.\n");
		printf(txt);
		OutputText(txt);

		sprintf(txt, "******************************************************************\n");
		printf(txt);
		OutputText(txt);

		DebuggerBreak();
	}

	//*************************************************
	// Now go through all planes, and make sure that
	// no vertices are in front of any plane, which
	// would indicate a concavity.
	//*************************************************

	static const float fPlaneEps = 0.01f;

	int pl;
	for(pl=0; pl<iNumPlanes; pl++)
	{
		for(v=0; v<iNumVertices; v++)
		{
			float dist = DotProduct( m_vPolyEdgePlaneNormals[pl], m_vPolyVertices[v] ) + m_vPolyEdgePlaneDists[pl];
			if(dist > fPlaneEps)
			{
				return false;
			}
		}
	}

	return true;
}

//********************************************************************************
// Merge pPoly2 with pPoly1
// pPoly1 becomes the union of the two polygons, which pPoly2 remains unchanged -
// and will subsequently be deleted.
// If any edge will exceed the maximum edge length, then we return false.
//********************************************************************************

bool
CNavGen::MergePolys(TMergePoly * pPoly1, int pPoly1MergeEdge, TMergePoly * pPoly2, int pPoly2MergeEdge)
{
	if(!pPoly1 || !pPoly2)
		return false;

	vector<TVertexEntry*> vertexList;
	vector<TMergePoly*> adjacentPolys;

	// Start with the 2nd vertex on pPoly1MergeEdge.
	u32 v;
	int count;
	int iVertexOnPoly1ToContinueFrom = -1;

	v = pPoly1MergeEdge+1;
	while(v >= (u32)pPoly1->m_Vertices.size())
		v -= pPoly1->m_Vertices.size();

	// Add all the vertices & adjacencies from poly1
	count = pPoly1->m_Vertices.size();

	while(count > 0)
	{
		vertexList.push_back( pPoly1->m_Vertices[v] );
		TMergePoly * pAdjPoly = pPoly1->m_AdjacentPolys[v];

		// Quit out of this loop when we get to the shared edge.
		// We will continue with the remaining points in pPoly1
		// once we've dealt with pPoly2..
		if(pAdjPoly == pPoly2)
		{
			iVertexOnPoly1ToContinueFrom = (v + 1) % pPoly1->m_Vertices.size();
			break;
		}

		adjacentPolys.push_back(pAdjPoly);

		v++;
		if(v >= (u32)pPoly1->m_Vertices.size())
			v -= (u32)pPoly1->m_Vertices.size();
		count--;
	}

	// !!! something's gone wrong !!!
	if(iVertexOnPoly1ToContinueFrom == -1)
	{
		DebuggerBreak();
		return false;
	}

	// Continue with the 2nd vertex on pPoly2MergeEdge.
	int p2Sharedv1 = pPoly2MergeEdge;
	int p2Sharedv2 = pPoly2MergeEdge+1;

	while(p2Sharedv2 >= (int)pPoly2->m_Vertices.size())
		p2Sharedv2 -= (int)pPoly2->m_Vertices.size();

	v = p2Sharedv2;

	// Add the vertices & adjacencies from poly2, MINUS the vertices along the shared edge..
	count = pPoly2->m_Vertices.size();
	while(count>0)
	{
		if(v == (u32)p2Sharedv1)
			break;

		if(v != (u32)p2Sharedv2)
		{
			vertexList.push_back( pPoly2->m_Vertices[v] );
		}

		TMergePoly * pAdjPoly = pPoly2->m_AdjacentPolys[v];
		adjacentPolys.push_back(pAdjPoly);

		v++;
		if(v >= (u32)pPoly2->m_Vertices.size())
			v -= pPoly2->m_Vertices.size();
		count--;
	}

	// Now we continue with pPoly1 - continuing from where we left off previously
	int iEndIndex = (pPoly1MergeEdge+1) % pPoly1->m_Vertices.size();
	while(iVertexOnPoly1ToContinueFrom != iEndIndex)
	{
		vertexList.push_back( pPoly1->m_Vertices[ iVertexOnPoly1ToContinueFrom ] );
		adjacentPolys.push_back( pPoly1->m_AdjacentPolys[ iVertexOnPoly1ToContinueFrom ] );

		iVertexOnPoly1ToContinueFrom++;
		while(iVertexOnPoly1ToContinueFrom >= (int)pPoly1->m_Vertices.size())
			iVertexOnPoly1ToContinueFrom -= (int)pPoly1->m_Vertices.size();
	}

	//***********************************************************************
	// Make sure nothing has gone wrong wrt number of points, etc.
	//***********************************************************************

	if(vertexList.size() == 0)
	{
		DebuggerBreak();
		return false;
	}

	if(vertexList.size() >= NAVMESHPOLY_MAX_NUM_VERTICES)
	{
		char txt[256];

		sprintf(txt, "******************************************************************\n");
		printf(txt);
		OutputText(txt);

		sprintf(txt, "ERROR - MergePolys() Exceeded max num vertices.\n");
		printf(txt);
		OutputText(txt);

		sprintf(txt, "******************************************************************\n");
		printf(txt);
		OutputText(txt);

		DebuggerBreak();
		return false;
	}
	if(vertexList.size() != adjacentPolys.size())
	{
		printf("ERROR - vertexList and adjacentPolys are different sizes.\n");
		DebuggerBreak();
		return false;
	}

	//*****************************************************************************
	// Make sure that this new poly will not exceed the maximum edge length metric,
	// and also make sure that no angle will be smaller than the minimum allowed.
	//*****************************************************************************

	const float fIsWaterMaxEdgeLength = m_Params.m_fMaxTriangleSideLength * 5.0f;
	const float fMaxTriSideLength = pPoly1->m_bWater ? fIsWaterMaxEdgeLength : m_Params.m_fMaxTriangleSideLength;

	Vector3 vPolyMins(FLT_MAX, FLT_MAX, FLT_MAX);
	Vector3 vPolyMaxs(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	const float fMinTriAngle = (m_Params.m_fMinTriangleAngle * DtoR);
	const float fColinearTriAngle = (179.5f * DtoR);
	float fEdgeLength = 0.0f;
	bool bLastAngleWasColinear = false;

	size_t lastv = vertexList.size()-1;
	int nextv = 1;
	for(v=0; v<vertexList.size(); v++)
	{
		Vector3 vVec1 = vertexList[lastv]->m_Vertex - vertexList[v]->m_Vertex;
		vVec1.Normalize();
		Vector3 vVec2 = vertexList[nextv]->m_Vertex - vertexList[v]->m_Vertex;
		vVec2.Normalize();

		if(vertexList[v]->m_Vertex.x < vPolyMins.x) vPolyMins.x = vertexList[v]->m_Vertex.x;
		if(vertexList[v]->m_Vertex.y < vPolyMins.y) vPolyMins.y = vertexList[v]->m_Vertex.y;
		if(vertexList[v]->m_Vertex.z < vPolyMins.z) vPolyMins.z = vertexList[v]->m_Vertex.z;
		if(vertexList[v]->m_Vertex.x > vPolyMaxs.x) vPolyMaxs.x = vertexList[v]->m_Vertex.x;
		if(vertexList[v]->m_Vertex.y > vPolyMaxs.y) vPolyMaxs.y = vertexList[v]->m_Vertex.y;
		if(vertexList[v]->m_Vertex.z > vPolyMaxs.z) vPolyMaxs.z = vertexList[v]->m_Vertex.z;

		float dot = DotProduct(vVec1, vVec2);

		dot = Clamp(dot, -1.0f, 1.0f);
		float angle1 = Acosf(dot);

		if(angle1 < fMinTriAngle)
		{
			return false;
		}

		const bool bEdgeIsColinear = (angle1 >= fColinearTriAngle);

		if(bLastAngleWasColinear)
		{
			fEdgeLength += vVec1.Mag();
		}
		else
		{
			fEdgeLength = vVec1.Mag();
		}

		if(fEdgeLength >= fMaxTriSideLength)
		{
			return false;
		}

		bLastAngleWasColinear = bEdgeIsColinear;

		lastv = v;
		nextv++;
		if(nextv >= (int)vertexList.size())
			v -= (u32)vertexList.size();
	}
/*
	// 16/11/05 - JB : New bit.  In an attempt to correct the way that we're getting polygons
	// with really large edge lengths for both colinear edges, and single edges - let's put
	// a restriction on the total axis-orientated extents of each merged poly.
	if((vPolyMaxs.x - vPolyMins.x) > fMaxTriSideLength)
		return false;
	if((vPolyMaxs.y - vPolyMins.y) > fMaxTriSideLength)
		return false;
*/


	// HACK
	// Now do a check that no 2 vertices in the new poly are the same.
	u32 v1,v2;
	for(v1=0; v1<vertexList.size(); v1++)
	{
		Vector3 vert1 = vertexList[v1]->m_Vertex;

		for(v2=v1+1; v2<vertexList.size(); v2++)
		{
			Vector3 vert2 = vertexList[v2]->m_Vertex;

			if(vert1.IsClose(vert2, 0.001f))
			{
				return false;
			}
		}
	}
	// \HACK



	//***********************************************************************
	// Create the new poly in-place as pPoly1
	//***********************************************************************

	pPoly1->m_vPlaneNormal = pPoly1->m_vPlaneNormal + pPoly2->m_vPlaneNormal;
	pPoly1->m_vPlaneNormal.Normalize();
	Vector3 vert = vertexList[0]->m_Vertex;
	pPoly1->m_fPlaneDist = - DotProduct(pPoly1->m_vPlaneNormal, vert);

	pPoly1->m_Vertices.clear();
	pPoly1->m_AdjacentPolys.clear();

	if(vertexList.size() != adjacentPolys.size())
	{
		printf("ERROR - vertexList.size() & adjacentPolys.size() are different..\n");
		DebuggerBreak();
		return false;
	}

	for(v=0; v<(s32)vertexList.size(); v++)
	{
		pPoly1->m_Vertices.Grow() = vertexList[v];
		pPoly1->m_AdjacentPolys.Grow() = adjacentPolys[v];
	}

	const float fCombinedArea = pPoly1->m_fArea + pPoly2->m_fArea;

	pPoly1->CalcAreaAndBounds();

	// Ped density becomes the sum of poly's densities proportional to area

	float fNewPedDensity =
		(((float)pPoly1->m_colPolyData.GetPedDensity()) * (pPoly1->m_fArea / fCombinedArea)) +
		(((float)pPoly2->m_colPolyData.GetPedDensity()) * (pPoly2->m_fArea / fCombinedArea));

	pPoly1->m_colPolyData.SetPedDensity( (s32) (fNewPedDensity + 0.5f) );

	// If either poly is pavement, then both are marked as such
	if(pPoly1->m_colPolyData.GetIsPavement() || pPoly2->m_colPolyData.GetIsPavement())
	{
		pPoly1->m_colPolyData.SetIsPavement(true);
	}

	// If either poly is interior, then both are marked as such
	Assert(pPoly1->m_colPolyData.GetIsInterior() == pPoly2->m_colPolyData.GetIsInterior());
	if(pPoly1->m_colPolyData.GetIsInterior() || pPoly2->m_colPolyData.GetIsInterior())
	{
		pPoly1->m_colPolyData.SetIsInterior(true);
	}

	// If both polys are in shelter, then the merged poly will also be.. If not, then neither will be
	if(pPoly1->m_bInShelter && pPoly2->m_bInShelter)
	{
		pPoly1->m_bInShelter = true;
	}

	// Clear space above is the minimum of the two poly's clear space.
	// NB: We may want to prevent these two being merged in the first place if difference is too great?
	pPoly1->m_iFreeSpaceTopZ = Min(pPoly1->m_iFreeSpaceTopZ, pPoly2->m_iFreeSpaceTopZ);

	Assert(pPoly1->m_bWater == pPoly2->m_bWater);

	return true;
}



void CNavGen::SplitPolysAfterMerging(const CNavMesh * /*pInputNavMesh*/, list<TMergePoly*> & sortedPolyList, vector<TVertexEntry*> & vertexList, vector<fwDynamicEntityInfo*> * pSplitObjects)
{
	//const Vector3 & vPlaneNormal = pObj->m_Matrix.a;
	//const Vector3 vPointOnPlane = (pInputNavMesh->m_pQuadTree->m_Mins + pInputNavMesh->m_pQuadTree->m_Maxs) * 0.5f;
	//const Vector3 vPlaneNormal(1.0f, 0.0f, 0.0f);

	if(!pSplitObjects)
	{
		return;
	}

	static dev_float fEps = 0.15f;

	u32 o;
	for( o=0; o<pSplitObjects->size(); o++ )
	{
		fwDynamicEntityInfo * pObj = (*pSplitObjects)[o];

		// Only objects which have been marked as FLAG_CLIMABABLE_BY_AI
		if(!(pObj->m_iFlags & fwDynamicEntityInfo::FLAG_CLIMABABLE_BY_AI))
			continue;

		// Only work with objects which are Z-up for now..
		const float fDotUp = DotProduct( pObj->m_Matrix.c, ZAXIS );
		if(fDotUp < 0.707f)
			continue;

		const float fSizeX = pObj->m_OOBB[1].x - pObj->m_OOBB[0].x;
		const float fSizeY = pObj->m_OOBB[1].y - pObj->m_OOBB[0].y;
		Assert(fSizeX > 0.0f && fSizeY > 0.0f);

		const Vector3 vPointOnPlane = GetCenter(pObj->m_OOBB[0], pObj->m_OOBB[1], pObj->m_Matrix);

		const Vector3 & vPlaneNormal = (fSizeX > fSizeY) ? pObj->m_Matrix.b : pObj->m_Matrix.a;
		const float fDist = - DotProduct(vPointOnPlane, vPlaneNormal);
		const Vector4 vPlane(vPlaneNormal.x, vPlaneNormal.y, vPlaneNormal.z, fDist);

		list<TMergePoly*> newFragments;

LIST_RESTART:

		list<TMergePoly*>::iterator listIter;
		s32 iPolyNum = 0;
		for(listIter=sortedPolyList.begin(); listIter!=sortedPolyList.end(); listIter++, iPolyNum++)
		{
			TMergePoly * pTestPoly = *listIter;
			if(pTestPoly)
			{
				if( TestMergePolyObjectIntersection(pTestPoly, pObj) )
				{
					bool bRemoved = BisectMergePoly(pTestPoly, vPlane, fEps, sortedPolyList, vertexList, newFragments);
					if(bRemoved)
					{
						goto LIST_RESTART;
					}
				}
			}
		}

		list<TMergePoly*>::iterator fragListIter;
		for(fragListIter=newFragments.begin(); fragListIter!=newFragments.end(); fragListIter++)
		{
			TMergePoly * pFragPoly = *fragListIter;
			pFragPoly->m_iIndex = (u32)sortedPolyList.size();
			SortMergePolyIntoList(pFragPoly, &sortedPolyList);
		}
	}
}

float CalculateWindingArea(vector<Vector3*> & verts)
{
	if(verts.size() < 3)
		return 0.0f;

	float area = 0.0f;
	Vector3 * v0 = verts[0];
	for(u32 v=1; v<verts.size()-1; v++)
	{
		Vector3 * v1 = verts[v];
		Vector3 * v2 = verts[v+1];

		area += CalcTriArea(*v0, *v1, *v2);
	}
	return area;
}

bool CNavGen::BisectMergePoly(TMergePoly * pPoly, const Vector4 & vSplitPlane, const float fPlaneEps, list<TMergePoly*> & sortedPolyList, vector<TVertexEntry*> & vertexList, list<TMergePoly*> & newFragmentsPolyList)
{
	static const float fSmallArea = 0.1f;

	u32 v;

	//-----------------------------------------------------------------
	// First establish whether we actually need to perform this split

	vector<Vector3*> frontVerts;
	vector<Vector3*> backVerts;
	int lastv = pPoly->m_Vertices.GetCount()-1;
	for(v=0; v<(u32)pPoly->m_Vertices.GetCount(); v++)
	{
		Vector3 vert = pPoly->m_Vertices[v]->m_Vertex;
		Vector3 lastVert = pPoly->m_Vertices[lastv]->m_Vertex;

		const float f1 = GetPlaneDist(vSplitPlane, lastVert);
		const float f2 = GetPlaneDist(vSplitPlane, vert);

		// If any polygon edge is almost coincident with the split plane, then fail
		if(Abs(f1) <= fPlaneEps && Abs(f2) <= fPlaneEps)
			return false;

		// Abort if any adjacent polygon will be unable to accommodate the new
		// vertex being inserted along its edge.  A cop out, as really we should
		// ascertain exactly which neighbouring polys will need new verts (if any)
		TMergePoly * pAdjacentPoly = pPoly->m_AdjacentPolys[lastv];
		if(pAdjacentPoly && pAdjacentPoly->m_Vertices.size() >= NAVMESHPOLY_MAX_NUM_VERTICES-1)
			return false;

		const float fLastDist = GetPlaneDist(vSplitPlane, lastVert);
		const float fDist = GetPlaneDist(vSplitPlane, vert);

		const int iLastSide = (fLastDist < -fPlaneEps) ? -1 : (fLastDist >= fPlaneEps) ? 1 : 0;
		const int iSide = (fDist < -fPlaneEps) ? -1 : (fDist >= fPlaneEps) ? 1 : 0;
		Assert(!(iSide==0 && iLastSide==0));

		if(iLastSide == -1)
		{
			backVerts.push_back(new Vector3(lastVert));
		}
		else if(iLastSide == 1)
		{
			frontVerts.push_back(new Vector3(lastVert));
		}

		if(iLastSide != iSide)
		{
			if(iLastSide != 0 && iSide != 0)
			{
				const float s = (fLastDist / (fLastDist - fDist));
				const Vector3 vPoint = lastVert + ((vert - lastVert) * s);
				frontVerts.push_back(new Vector3(vPoint));
				backVerts.push_back(new Vector3(vPoint));
			}
			else if(iLastSide == 0)
			{
				frontVerts.push_back(new Vector3(lastVert));
				backVerts.push_back(new Vector3(lastVert));
				Assert(iSide!=0);
			}
		}

		//-----------------------

		lastv = v;
	}

	const size_t iNumBack = backVerts.size();
	const size_t iNumFront = frontVerts.size();

	if(iNumBack == (size_t)pPoly->m_Vertices.size() && iNumFront < 3)
		return false;
	if(iNumFront == (size_t)pPoly->m_Vertices.size() && iNumBack < 3)
		return false;

	const float fFrontArea = CalculateWindingArea(frontVerts);
	const float fBackArea = CalculateWindingArea(backVerts);
	CleanDeleteStlVector(frontVerts);
	CleanDeleteStlVector(backVerts);
	if(fFrontArea < fSmallArea || fBackArea < fSmallArea)
		return false;

	//------------------------------------
	// Ok so it looks like we can proceed

	TMergePoly * pFront = new TMergePoly();
	TMergePoly * pBack = new TMergePoly();

	pFront->m_bAddedDuringPolyBisect = true;
	pBack->m_bAddedDuringPolyBisect = true;

	lastv = pPoly->m_Vertices.GetCount()-1;
	for(v=0; v<(u32)pPoly->m_Vertices.GetCount(); v++)
	{
		Assert(pPoly->m_Vertices.GetCount() == pPoly->m_AdjacentPolys.GetCount());

		Vector3 vert = pPoly->m_Vertices[v]->m_Vertex;
		Vector3 lastVert = pPoly->m_Vertices[lastv]->m_Vertex;

		const float fLastDist = GetPlaneDist(vSplitPlane, lastVert);
		const float fDist = GetPlaneDist(vSplitPlane, vert);

		const int iLastSide = (fLastDist < -fPlaneEps) ? -1 : (fLastDist >= fPlaneEps) ? 1 : 0;
		const int iSide = (fDist < -fPlaneEps) ? -1 : (fDist >= fPlaneEps) ? 1 : 0;
		Assert(!(iSide==0 && iLastSide==0));

		TMergePoly * pAdjacentPoly = pPoly->m_AdjacentPolys[lastv];

		if(iLastSide == -1)
		{
			pBack->m_Vertices.Push(pPoly->m_Vertices[lastv]);
			pBack->m_AdjacentPolys.Push(pAdjacentPoly);
		}
		else if(iLastSide == 1)
		{
			pFront->m_Vertices.Push(pPoly->m_Vertices[lastv]);
			pFront->m_AdjacentPolys.Push(pAdjacentPoly);
		}

		if(iLastSide != iSide)
		{
			if(iLastSide != 0 && iSide != 0)
			{
				const float s = (fLastDist / (fLastDist - fDist));
				const Vector3 vPoint = lastVert + ((vert - lastVert) * s);
				TVertexEntry * pNewVert = new TVertexEntry(vPoint);
				pNewVert->m_iIndex = (u32)vertexList.size(); 
				vertexList.push_back(pNewVert);

				pNewVert->AddSurroundingPoly(pFront);
				pNewVert->AddSurroundingPoly(pBack);
				if(pAdjacentPoly)
					pNewVert->AddSurroundingPoly(pAdjacentPoly);

				pFront->m_Vertices.Push(pNewVert);
				pBack->m_Vertices.Push(pNewVert);

				pFront->m_AdjacentPolys.Push( (fLastDist < 0.0f) ? pAdjacentPoly : pBack );
				pBack->m_AdjacentPolys.Push( (fLastDist < 0.0f) ? pFront : pAdjacentPoly );

				if(pAdjacentPoly)
				{
					pAdjacentPoly->BisectAdjacentEdge(
						pPoly,
						(fLastDist < 0.0f) ? pFront : pBack, /*(fLastDist < 0.0f) ? pBack : pFront,*/
						pNewVert,
						(fLastDist < 0.0f) ? pBack : pFront /*(fLastDist < 0.0f) ? pFront : pBack*/
					);
				}
			}
			else if(iLastSide == 0)
			{
				TVertexEntry * pExistingVert = pPoly->m_Vertices[lastv];
				pExistingVert->AddSurroundingPoly(pFront);
				pExistingVert->AddSurroundingPoly(pBack);
				pExistingVert->RemoveSurroundingPoly(pPoly);

				pFront->m_Vertices.Push( pExistingVert );
				pBack->m_Vertices.Push( pExistingVert );

				pFront->m_AdjacentPolys.Push( (fDist > 0.0f) ? pAdjacentPoly : pBack );
				pBack->m_AdjacentPolys.Push( (fDist > 0.0f) ? pFront : pAdjacentPoly );

				//pFront->m_AdjacentPolys.Push( (fLastDist < 0.0f) ? pBack : pAdjacentPoly );
				//pBack->m_AdjacentPolys.Push( (fLastDist < 0.0f) ? pAdjacentPoly : pFront );

				Assert(iSide!=0);
				if(pAdjacentPoly)
					pAdjacentPoly->ReplaceAdjacency(pPoly, (iSide > 0) ? pFront : pBack);
			}
		}

		Assert(pFront->m_Vertices.GetCount() == pFront->m_AdjacentPolys.GetCount());
		Assert(pBack->m_Vertices.GetCount() == pBack->m_AdjacentPolys.GetCount());

		//-----------------------

		lastv = v;
	}

	//---------------------------------------------------------------------
	// Replace references to pPoly within adjacent polys of pFront & pBack

	for(v=0; v<(u32)pFront->m_AdjacentPolys.size(); v++)
	{
		TMergePoly * pAdjacent = pFront->m_AdjacentPolys[v];
		if(pAdjacent)
			pAdjacent->ReplaceAdjacency(pPoly, pFront);
	}
	for(v=0; v<(u32)pBack->m_AdjacentPolys.size(); v++)
	{
		TMergePoly * pAdjacent = pBack->m_AdjacentPolys[v];
		if(pAdjacent)
			pAdjacent->ReplaceAdjacency(pPoly, pBack);
	}

	//-----------------------------------------------------
	// Now remove all references to pPoly everywhere else

	list<TMergePoly*>::iterator polyIter;
	for(polyIter=sortedPolyList.begin(); polyIter!=sortedPolyList.end(); polyIter++)
	{
		(*polyIter)->ReplaceAdjacency(pPoly, NULL);
	}

	for(v=0; v<vertexList.size(); v++)
	{
		vertexList[v]->RemoveSurroundingPoly(pPoly);
	}

	Assert(pFront->m_Vertices.GetCount() == pFront->m_AdjacentPolys.GetCount());
	Assert(pBack->m_Vertices.GetCount() == pBack->m_AdjacentPolys.GetCount());

	//-------------------------------------------------------------
	// Finally, initialise pFront & pBack, and delete/remove pPoly
	
	pFront->CopyData(pPoly);
	pFront->CalcAreaAndBounds();
	pFront->m_vPlaneNormal = pPoly->m_vPlaneNormal;
	pFront->m_bFragment = true;
	newFragmentsPolyList.push_back(pFront);

	pBack->CopyData(pPoly);
	pBack->CalcAreaAndBounds();
	pBack->m_vPlaneNormal = pPoly->m_vPlaneNormal;
	pBack->m_bFragment = true;
	newFragmentsPolyList.push_back(pBack);

//	Assert( DotProduct(pFront->m_vPlaneNormal, pPoly->m_vPlaneNormal) > 0.9f );
//	Assert( DotProduct(pBack->m_vPlaneNormal, pPoly->m_vPlaneNormal) > 0.9f );

#if __DEV
	pFront->TestAdjacencies();
	pBack->TestAdjacencies();
#endif

	sortedPolyList.remove(pPoly);
	delete pPoly;

	return true;
}

#if __DEV
bool TMergePoly::TestAdjacencies()
{
	for(u32 a=0; a<(u32)m_AdjacentPolys.GetCount(); a++)
	{
		TMergePoly * pAdjacent = m_AdjacentPolys[a];
		if(pAdjacent)
		{
			bool bFoundA = false;
			for(u32 b=0; b<(u32)pAdjacent->m_AdjacentPolys.GetCount(); b++)
			{
				if(pAdjacent->m_AdjacentPolys[b] == this)
				{
					bFoundA = true;
					break;
				}
			}
			Assert(bFoundA);
		}
	}
	return true;
}
#endif

bool TMergePoly::BisectAdjacentEdge(TMergePoly * pOldPoly, TMergePoly * pNewA, TVertexEntry * pMiddleVertex, TMergePoly * pNewB)
{
	for(u32 a=0; a<(u32)m_AdjacentPolys.GetCount(); a++)
	{
		TMergePoly * pAdjacent = m_AdjacentPolys[a];
		if(pAdjacent == pOldPoly)
		{
			m_AdjacentPolys[a] = pNewA;
			m_Vertices.Insert(a+1) = pMiddleVertex;
			m_AdjacentPolys.Insert(a+1) = pNewB;

			return true;
		}
	}
	Assertf(false, "Adjacency to pOldPoly was not found.  Middle vertex is at (%.2f, %.2f, %.2f)", pMiddleVertex->m_Vertex.x, pMiddleVertex->m_Vertex.y, pMiddleVertex->m_Vertex.z);
	return false;
}

float TMergePoly::CalcAreaAndBounds()
{
	m_fBoundingRadius = 0.0f;
	m_vOrigin = Vector3(0,0,0);

	s32 v;
	for(v=0; v<m_Vertices.GetCount(); v++)
	{
		m_vOrigin += m_Vertices[v]->m_Vertex;
	}
	m_vOrigin /= ((float)m_Vertices.GetCount());
	for(v=0; v<m_Vertices.GetCount(); v++)
	{
		Vector3 vDiff = m_vOrigin - m_Vertices[v]->m_Vertex;
		m_fBoundingRadius = Max(m_fBoundingRadius, vDiff.Mag2());
	}
	m_fBoundingRadius = rage::Sqrtf(m_fBoundingRadius);

	float a=0;
	Vector3 e1,e2,e3;
	s32 v1,v2;

	v1 = 1;
	for(v2=2; v2<m_Vertices.GetCount(); v2++)
	{
		e1 = m_Vertices[v1]->m_Vertex - m_Vertices[0]->m_Vertex;
		e2 = m_Vertices[v2]->m_Vertex - m_Vertices[0]->m_Vertex;
		e3 = m_Vertices[v2]->m_Vertex - m_Vertices[v1]->m_Vertex;
		a += CalcTriArea(e1.Mag(), e2.Mag(), e3.Mag());
		v1=v2;
	}

	m_fArea = a;

	Assert(m_fArea > SMALL_FLOAT);
	Assert(m_fArea < LARGE_FLOAT);

	// Calc surface normal, etc.
	v1 = 1;
	v2 = 2;
	for(v=0; v<m_Vertices.GetCount(); v++)
	{
		const Vector3 & vec0 = m_Vertices[v]->m_Vertex;
		const Vector3 & vec1 = m_Vertices[v1]->m_Vertex;
		const Vector3 & vec2 = m_Vertices[v2]->m_Vertex;

		Vector3 vEdge1 = vec0 - vec1;
		Vector3 vEdge2 = vec2 - vec1;
		vEdge1.Normalize();
		vEdge2.Normalize();

		if(DotProduct(vEdge1, vEdge2) < 0.8f)
		{
			m_vPlaneNormal = CrossProduct(vEdge1, vEdge2);
			m_vPlaneNormal.Normalize();
			break;
		}

		v1 = (v1+1) % m_Vertices.GetCount();
		v2 = (v2+1) % m_Vertices.GetCount();
	}

	//Vector3 edge1 = m_Vertices[1]->m_Vertex - m_Vertices[0]->m_Vertex;
	//Vector3 edge2 = m_Vertices[2]->m_Vertex - m_Vertices[0]->m_Vertex;
	//m_vPlaneNormal = CrossProduct(edge1, edge2);
	//m_vPlaneNormal.Normalize();

	m_fPlaneDist = - DotProduct(m_vPlaneNormal, m_Vertices[0]->m_Vertex);

	return m_fArea;
}


}	// namespace rage
