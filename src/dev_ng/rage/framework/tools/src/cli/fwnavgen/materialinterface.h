//
// fwnavgen/materialinterface.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef FWNAVGEN_MATERIALINTERFACE_H
#define FWNAVGEN_MATERIALINTERFACE_H

#include "phcore/materialmgr.h"		// phMaterialMgr::Id
#include "ai/navmesh/datatypes.h"	// TColPolyData

namespace rage
{
	class fwEntity;

//-----------------------------------------------------------------------------

//***************************************************************************************************
//	fwNavMeshMaterialInterface
//	Abstract class used by fwExportCollisionBaseTool to define which attributes & surface-types should
//	be stored in the game's navmeshes.  Each project should derive from this class so that it can
//	bake in data which is relevant for it.
//***************************************************************************************************
class fwNavMeshMaterialInterface
{
public:
	virtual ~fwNavMeshMaterialInterface() {}
	virtual bool GetIsBakedInAttribute1(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType)) = 0;
	virtual bool GetIsBakedInAttribute2(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType)) = 0;
	virtual bool GetIsBakedInAttribute3(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType)) = 0;
	virtual bool GetIsBakedInAttribute4(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType)) = 0;
	virtual bool GetIsBakedInAttribute5(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType)) = 0;
	virtual bool GetIsBakedInAttribute6(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType)) = 0;
	virtual bool GetIsBakedInAttribute7(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType)) = 0;
	virtual bool GetIsBakedInAttribute8(Vector3 * UNUSED_PARAM(pTriVerts), phMaterialMgr::Id UNUSED_PARAM(iSurfaceType)) = 0;

	virtual bool IsThisSurfaceTypeGlass(phMaterialMgr::Id iSurfaceType) = 0;
	virtual bool IsThisSurfaceTypeStairs(phMaterialMgr::Id iSurfaceType) = 0;
	virtual bool IsThisSurfaceTypePavement(phMaterialMgr::Id iSurfaceType) = 0;
	virtual bool IsThisSurfaceTypeRoad(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType) = 0;
	virtual bool IsThisSurfaceTypeTrainTracks(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType) = 0;
	virtual bool IsThisSurfaceTypeSeeThrough(phMaterialMgr::Id iSurfaceType) = 0;
	virtual bool IsThisSurfaceTypeShootThrough(phMaterialMgr::Id iSurfaceType) = 0;
	virtual bool IsThisSurfaceAllowedForNetworkRespawning(phMaterialMgr::Id iSurfaceType) = 0;
	virtual bool IsThisSurfaceWalkable(Vector3 * pTriVerts, phMaterialMgr::Id iSurfaceType) = 0;
	virtual bool IsThisSurfaceClimbable(phMaterialMgr::Id iSurfaceType) = 0;
	virtual float GetPedDensityForThisSurfaceType(phMaterialMgr::Id iSurfaceType) = 0;

	virtual u32 GetDynamicEntityInfoFlags(const fwEntity * pEntity) = 0;

	virtual void SetCollisionPolyDataFromArchetypeFlags(TColPolyData & colPolyData, const u32 iArchetypeFlags) = 0;

#if HEIGHTMAP_GENERATOR_TOOL
	virtual int GetMaterialVfxGroup(phMaterialMgr::Id iSurfaceType, const char *& outName) = 0;
	virtual int GetMaterialMaskIndex(phMaterialMgr::Id iSurfaceType) = 0;
	virtual int GetMaterialProcIndex(phMaterialMgr::Id iSurfaceType) = 0;
#endif
};

//-----------------------------------------------------------------------------

}	// namespace rage

#endif	// FWNAVGEN_MATERIALINTERFACE_H

// End of file fwnavgen/materialinterface.h
