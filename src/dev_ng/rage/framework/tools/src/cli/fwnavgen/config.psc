<?xml version="1.0"?> 
<ParserSchema xmlns="http://www.rockstargames.com/RageParserSchema"
>

<structdef autoregister="true" type="fwNavGenConfig">
	<float init="2500.0f" name="m_AerialMaxTriangleArea"/>
	<float init="100.0f" name="m_AerialMaxTriangleSideLength"/>
	<float init="7.5f" name="m_AerialSampleResolutionDefault"/>
	<float init="2.0f" name="m_AerialVerticalQuantization"/>
	<float init="44.0f" name="m_AngleForNavMeshPolyToBeTooSteep"/>
	<s32 init="0" name="m_AntialiasEdgesNumPasses"/>
	<s32 init="0" name="m_AntialiasPavementEdgesNumPasses"/>
	<bool init="false" name="m_CreateResolutionAreasForPavementTriangles"/>
	<float init="0.3f" name="m_CoverLowFinalDistanceFromSurfaceOfCoverPoint"/>
	<float init="0.50f" name="m_CoverLowFinalProbeCheckDistanceLimit"/>
	<float init="1.0f" name="m_CoverLowMaxDistOfFloorBelow"/>
	<float init="0.5f" name="m_CoverLowPointSeparation"/>
	<float init="1.44f" name="m_CoverLowWallMidHeight"/>
	<float init="0.2f" name="m_CoverLowWallObstructionHeight"/>
	<float init="0.5f" name="m_CoverLowWallObstructionMaxHeight"/>
	<float init="2.0f" name="m_CoverLowWallPointSpacingAlongEdges"/>
	<float init="1.5f" name="m_CoverLowWallTestDistance"/>
	<float init="1000.0f" name="m_ExporterMaxZCutoff"/>
	<float init="-500.0f" name="m_ExporterMinZCutoff"/>
	<float init="60.0f" name="m_MaxAngleForPolyToBeInNavMesh"/>
	<float init="75.0f" name="m_MaxAngleForWaterPolyToBeInNavMesh"/>
	<float init="0.4f" name="m_MaxHeightChangeUnderTriangleEdge"/>
	<float init="2.0f" name="m_MinimumVerticalDistanceBetweenNodes"/>
	<float init="0.35f" name="m_PedRadius"/>
	<float init="1.0f" name="m_SampleResolutionDefault"/>
	<float init="1.8f" name="m_TestClearHeightInTriangulation"/>
	<float init="-6000.0f" name="m_WorldMinX"/>
	<float init="-6000.0f" name="m_WorldMinY"/>
	<float init="0.0f" name="m_MinPatchAreaBeforeCulling"/>
	<float init="0.4f" name="m_TriangulationMaxHeightDiff"/>
	<float init="5.0" name="m_OptimizeMaxQuadricErrorMetric"/>
</structdef>

</ParserSchema>