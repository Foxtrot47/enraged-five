#ifndef WORKERTHREADS_H
#define WORKERTHREADS_H

//-------------------------------------------------------------------------------
// FILENAME : WorkerThreads.cpp
// PURPOSE : Implementes a means of processing navmesh constructing in parallel

#include "system/ipc.h"
#include "atl/array.h"
#include "vector/vector3.h"

#include "NavGen_Types.h"

namespace rage
{
	struct TNavMeshParams;
	class CNavGen;

class CWorkerThreadBase
{
	friend class CGenerationThreadPool;

public:
	CWorkerThreadBase() { }
	virtual ~CWorkerThreadBase() { }

	virtual bool IsRunning() = 0;
	virtual bool IsComplete() = 0;
	virtual bool Process() = 0;

	DWORD m_iThreadId;
	int m_iThreadNum;
	sysMemAllocator * m_Allocator;
};

class CThreadPoolBase
{
public:

	CThreadPoolBase() { }
	virtual ~CThreadPoolBase() { }

	virtual void Init(int iNumThreads) = 0;
	virtual void Shutdown() = 0;

	CWorkerThreadBase * GetNextFree();
	CWorkerThreadBase * GetNextComplete();
	CWorkerThreadBase * GetNextRunning();

protected:

	atArray<CWorkerThreadBase*> m_ThreadPool;
};

///////////////////////////////////////////////////////////////////////////////////

class CGenerationWorkerThread : public CWorkerThreadBase
{
	friend class CGenerationThreadPool;

private:
	CGenerationWorkerThread();
public:

	void Init();
	bool Run(TNavMeshParams * pParams, CNavGen * pGenerator, CNavGen ** ppNeighborNavGens, int iNumNeighbours, bool bAerialNavigation);

	virtual bool Process();
	virtual bool IsRunning();
	virtual bool IsComplete();

	void SaveData();
	void Shutdown();

	static DWORD WINAPI ThreadFunction(LPVOID pData);

private:

	TNavMeshParams * m_pNavParams;
	CNavGen * m_pNavMeshGenerator;
	CNavGen * m_pNeighbours[4];	// Used for ariel nav
	int m_iNumNeighbours;
	bool m_bAerialNavigation;
	bool m_bComplete;

	float m_fTimeTakenToOptimize;
	float fTotalTimeToPlaceNodes;
	float fTotalTimeToTriangulate;
	float fTotalTimeToOptimize;
	float fTotalTimeTaken;

#pragma optimize( "", off )		// PRAGMA-OPTIMIZE-ALLOW
	char filename[1024];
#pragma optimize( "", on )		// PRAGMA-OPTIMIZE-ALLOW

};

class CMergeWorkerThread : public CWorkerThreadBase
{
	friend class CMergeThreadPool;

private:
	CMergeWorkerThread();
public:

	void Init();
	bool Run(TNavMeshParams * pParams, const Vector3 & vBlockMin, const Vector3 & vBlockMax, const bool bMainMap, const bool bAerialNavigation);

	virtual bool Process();
	virtual bool IsRunning();
	virtual bool IsComplete();

	void SaveData();
	void Shutdown();

	static DWORD WINAPI ThreadFunction(LPVOID pData);

	static s32 ms_iMaxVertexPoolSize;
	static s32 ms_iMaxVertexIndex;

private:

	TNavMeshParams * m_pNavParams;
	Vector3 m_vBlockMin;
	Vector3 m_vBlockMax;
	bool m_bRunning;
	bool m_bComplete;
	bool m_bMainMap;
	bool m_bAerialNavigation;
	CNavMesh * m_pMergedNavMesh;

//	bool m_bComplete;
//	float m_fTimeTakenToOptimize;
//	float fTotalTimeToPlaceNodes;
//	float fTotalTimeToTriangulate;
//	float fTotalTimeToOptimize;
//	float fTotalTimeTaken;
};


/*
class CAnalyseWorkerThread : public CWorkerThreadBase
{
	friend class CAnalyseThreadPool;

private:
	CAnalyseWorkerThread();
public:

	void Init();
	bool Run(TNavMeshParams * pParams, const Vector3 & vBlockMin, const Vector3 & vBlockMax, const bool bMainMap, const bool bAerialNavigation);

	virtual bool Process();
	virtual bool IsRunning();
	virtual bool IsComplete();

	void SaveData();
	void Shutdown();

	static void ThreadFunction(void * ptr);

private:

	TNavMeshParams * m_pNavParams;
	Vector3 m_vBlockMin;
	Vector3 m_vBlockMax;
	bool m_bRunning;
	bool m_bComplete;
	bool m_bMainMap;
	bool m_bAerialNavigation;
	CNavMesh * m_pMergedNavMesh;
};

*/

class CGenerationThreadPool : public CThreadPoolBase
{
public:

	virtual void Init(int iNumThreads);
	virtual void Shutdown();
};

class CMergeThreadPool : public CThreadPoolBase
{
public:

	virtual void Init(int iNumThreads);
	virtual void Shutdown();
};

/*
class CAnalyseThreadPool : public CThreadPoolBase
{
public:

	virtual void Init(int iNumThreads);
	virtual void Shutdown();
};
*/

} // rage

#endif // WORKERTHREADS_H