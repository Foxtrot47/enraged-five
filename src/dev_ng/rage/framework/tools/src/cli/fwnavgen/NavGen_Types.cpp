#include "NavGen_Types.h"
#include "NavGen.h"	// only for m_fTriangulationGetNodeEps

namespace rage
{
#if __DEV
u32 g_iDebugNodeIndex = 0;
#endif
s32 CNavSurfaceTri::ms_iGenerationId = 0;

CNavSurfaceTri::CNavSurfaceTri()
{
	m_Nodes[0] = m_Nodes[1] = m_Nodes[2] = NULL;
	m_AdjacentTris[0] = m_AdjacentTris[1] = m_AdjacentTris[2] = NULL;
	m_AntiAliasPass[0] = m_AntiAliasPass[1] = m_AntiAliasPass[2] = 0;
	m_iFlags = 0;
	m_bIsSheltered = false;
	m_bIsWater = false;
	m_bIsSlicePlane = false;
	m_bIsTooSteep = false;
	m_bCalcSpaceAbove = false;
	m_iFreeSpaceTopZ = TPolyVolumeInfo::ms_iFreeSpaceNone;
	m_bCreatedInFixJaggies = false;
	m_bIntersectsEntities = false;
	m_bAboveCullAreaThreshold = false;
	m_bDoNotOptimise = false;
	m_bDoNotCullSmallArea = false;
	m_iFloodFillTimeStamp = 0;
	m_iGenerationIndex = ms_iGenerationId;
	ms_iGenerationId++;
}

//***************************************************************************************************
//	CPlacedNodeMultiMap
//***************************************************************************************************

CPlacedNodeMultiMap::CPlacedNodeMultiMap(const Vector3 & vMin, const Vector3 & vMax, float fSpacingXY)
{
	//***********************************************************************************
	// We expand the boundaries by one space so that nodes lying slightly over either edge
	// can be successfully stored in this map.  Since the node-generation itself guarantees
	// that no duplicate nodes are ever created, this should be okay.
	//***********************************************************************************

	m_vMin = vMin;
	m_vMax = vMax;

	Vector3 vSize = m_vMax - m_vMin;

	m_iWidth = (u32) (vSize.x / fSpacingXY) + 1;
	m_iHeight = (u32) (vSize.y / fSpacingXY) + 1;
	m_fSpacingXY = fSpacingXY;
	m_fSpacingXYRecip = 1.0f / m_fSpacingXY;

	m_pNodeArrays = new vector<CNavGenNode*>[m_iWidth*m_iHeight];
	m_bProcessed = new u8[m_iWidth*m_iHeight];
	memset(m_bProcessed, 0, m_iWidth*m_iHeight);
}

CPlacedNodeMultiMap::~CPlacedNodeMultiMap(void)
{
	if(m_pNodeArrays)
	{
		delete[] m_pNodeArrays;
		m_pNodeArrays = NULL;
	}
	if(m_bProcessed)
	{
		delete[] m_bProcessed;
		m_bProcessed = NULL;
	}
}

bool CPlacedNodeMultiMap::IsFlaggedAsProcessed(const float x, const float y) const
{
	Vector2 vOffset(x - m_vMin.x, y - m_vMin.y);
	u32 iIndexX = (u32) ((vOffset.x * m_fSpacingXYRecip) + 0.5f);
	u32 iIndexY = (u32) ((vOffset.y * m_fSpacingXYRecip) + 0.5f);
	if(iIndexX < 0 || iIndexX >= m_iWidth || iIndexY < 0 || iIndexY >= m_iHeight)
	{
		return false;
	}
	u32 iIndex = (iIndexY * m_iWidth) + iIndexX;

	// Get the list
	return m_bProcessed[iIndex] ? true : false;
}

void CPlacedNodeMultiMap::FlagAsProcessed(const float x, const float y)
{
	Vector2 vOffset(x - m_vMin.x, y - m_vMin.y);
	u32 iIndexX = (u32) ((vOffset.x * m_fSpacingXYRecip) + 0.5f);
	u32 iIndexY = (u32) ((vOffset.y * m_fSpacingXYRecip) + 0.5f);
	if(iIndexX < 0 || iIndexX >= m_iWidth || iIndexY < 0 || iIndexY >= m_iHeight)
	{
		return ;
	}
	u32 iIndex = (iIndexY * m_iWidth) + iIndexX;

	// Get the list
	m_bProcessed[iIndex] = true;
}

int CPlacedNodeMultiMap::AddNode(CNavGenNode * pNode)
{
	Vector3 vOffset = pNode->m_vBasePos - m_vMin;

	u32 iIndexX = (u32) ((vOffset.x * m_fSpacingXYRecip) + 0.5f);
	u32 iIndexY = (u32) ((vOffset.y * m_fSpacingXYRecip) + 0.5f);

	if(iIndexX < 0 || iIndexX >= m_iWidth || iIndexY < 0 || iIndexY >= m_iHeight)
	{
		return false;
	}

	u32 iIndex = (iIndexY * m_iWidth) + iIndexX;
	
	// Get the list
	vector<CNavGenNode*> & nodeList = m_pNodeArrays[iIndex];

	// Add the node
	nodeList.push_back(pNode);

	return true;
}


CNavGenNode *
CPlacedNodeMultiMap::GetNode(
	const Vector3 & vPosition,
	float fZInterval,
	const float fTriangulationGetNodeEps,
	bool bOnStairs,
	bool bNotTriangulatedOnly,
	bool b2ndTriangulationPass,
	bool bClosestZ,
	bool bIncreaseZThresholdForStairs)
{
	//static const float fEpsilonXY = CNavGen::m_fTriangulationGetNodeEps;

	Vector3 vOffset = vPosition - m_vMin;

	u32 iIndexX = (u32) ((vOffset.x * m_fSpacingXYRecip)+0.5f);
	u32 iIndexY = (u32) ((vOffset.y * m_fSpacingXYRecip)+0.5f);

	if(iIndexX < 0 || iIndexX >= m_iWidth || iIndexY < 0 || iIndexY >= m_iHeight)
	{
		return NULL;
	}

	float fHeighestZSoFar = -1000000.0f;
	float fSmallestDZ = 1000000.0f;

	CNavGenNode * pBestNodeSoFar = NULL;

	u32 iIndex = (iIndexY * m_iWidth) + iIndexX;
	
	// Get the list
	vector<CNavGenNode*> & nodeList = m_pNodeArrays[iIndex];

	for(u32 n=0; n<nodeList.size(); n++)
	{
		CNavGenNode * pNode = nodeList[n];

		if(bNotTriangulatedOnly)
		{
			if(!b2ndTriangulationPass)
			{
				if(pNode->m_iFlags & NAVGENNODE_HASBEENTRIANGULATED_SO_SKIP)
				{
					continue;
				}
			}
			else
			{
				if(pNode->m_iFlags & NAVGENNODE_HASBEENTRIANGULATED2_SO_SKIP)
				{
					continue;
				}
			}
		}

		// NB : Shouldn't need to do this comparison on (x,y) at all - but in the interests of debugging it will remain in.
		// (It should be replaced with an assertion instead).

		if(!Eq(vPosition.x, pNode->m_vBasePos.x, fTriangulationGetNodeEps) || !Eq(vPosition.y, pNode->m_vBasePos.y, fTriangulationGetNodeEps))
		{

			continue;
		}

		// We've got a node at the specified (x,y).  Now if it is close enough in 'z', we will return it.
		// If the node is flagged as being on stairs, then be more lenient about height differences.
		float fIntervalToUse;
		if(bIncreaseZThresholdForStairs && (bOnStairs || (pNode->m_iFlags & NAVGENNODE_IS_ON_STAIRS_SURFACE)) )
		{
			fIntervalToUse = fZInterval * 2.0f;
		}
		else
		{
			fIntervalToUse = fZInterval;
		}

		if(Eq(vPosition.z, pNode->m_vBasePos.z, fIntervalToUse))
		{
			if(bClosestZ)
			{
				float fDZ = Abs(pNode->m_vBasePos.z - vPosition.z);
				if(fDZ < fSmallestDZ)
				{
					fSmallestDZ = fDZ;
					pBestNodeSoFar = pNode;
				}
			}
			else
			{
				if(pNode->m_vBasePos.z > fHeighestZSoFar)
				{
					fHeighestZSoFar = pNode->m_vBasePos.z;
					pBestNodeSoFar = pNode;
				}
			}
		}
	}

	// No entries at all, or no entries which match on the basis of the Z interval
	return pBestNodeSoFar;
}





CNavGenNode * CPlacedNodeMultiMap::GetFirstNodeBelowZVal(const Vector3 & vPosition, const float fTriangulationGetNodeEps)
{
	//static const float fEpsilonXY = CNavGen::m_fTriangulationGetNodeEps;

	Vector3 vOffset = vPosition - m_vMin;

	u32 iIndexX = (u32)(vOffset.x * m_fSpacingXYRecip);
	u32 iIndexY = (u32)(vOffset.y * m_fSpacingXYRecip);

	if(iIndexX < 0 || iIndexX >= m_iWidth || iIndexY < 0 || iIndexY >= m_iHeight)
	{
		return NULL;
	}

	u32 iIndex = (iIndexY * m_iWidth) + iIndexX;
	
	// Get the list
	vector<CNavGenNode*> & nodeList = m_pNodeArrays[iIndex];

	float fSmallestZDelta = FLT_MAX;
	CNavGenNode * pBestNode = NULL;

	for(u32 n=0; n<nodeList.size(); n++)
	{
		CNavGenNode * pNode = nodeList[n];

		// NB : Shouldn't need to do this comparison on (x,y) at all - but in the
		// interests of debugging it will remain in.  (It should be replaced with
		// an assertion instead).

		if(!Eq(vPosition.x, pNode->m_vBasePos.x, fTriangulationGetNodeEps) || !Eq(vPosition.y, pNode->m_vBasePos.y, fTriangulationGetNodeEps))
		{
			continue;
		}

		// We've got a node at the specified (x,y).  Is it less in Z ?
		if(pNode->m_vBasePos.z < vPosition.z)
		{
			// We're looking for the closest z difference LESS THAN the target value..
			float fDelta = vPosition.z - pNode->m_vBasePos.z;
			if(fDelta < fSmallestZDelta)
			{
				fSmallestZDelta = fDelta;
				pBestNode = pNode;
			}
		}
	}

	if(pBestNode)
	{
		return pBestNode;
	}

	// No entries at all, or no entries which match on the basis of the Z interval
	return NULL;
}

} // rage

