#include "common.h"
#include "config.h"
#include "NavGen_Octree.h"
#include "NavGen.h"
#include "NavGen_Utils.h"
//#include "pathserver\PathServer.h"

#define __USE_GEOM_CULLER_FOR_EDGE_TEST		1
#define __USE_GEOM_CULLER_FOR_CORNERS		1
#define __USE_GEOM_CULLER_FOR_LOW_WALL		1

namespace rage
{

//*********************************************************************************
//
//	Title	: NavGen_Cover.cpp
//	Author	: James Broad, Phil Hooker
//	Date	: Jan 2005
//***************************************************************************************

// Reserve the coverpoints arrays to this size during analysis, to avoid constantly redimensioning arrays
#define COVERPOINTS_RESERVE_SIZE				8192

// Whether to check & break when a certain coverpoint position is found
#define __BREAK_ON_COVERPOINT 0

// Let's define a location globally that we'll use for our "break_on_coverpoint" stuff.
// Mad Doctor I, Sept 24, 2012
#if __BREAK_ON_COVERPOINT
static Vector3 s_vTestVec(-798.40735f, 185.05695f, 71.60547f);
static float s_fTestDistance = 3.0f;
static float s_fTestDistance2 = s_fTestDistance*s_fTestDistance;
static Vector3 vEps(1.0f, 1.0f, 4.0f);
#endif // __BREAK_ON_COVERPOINT

// Whether to spew out detailed logging for a focus area
#define __OUTPUT_FOCUS_AREA 0

// Define location and radius for focus area output spew
#if __OUTPUT_FOCUS_AREA
static Vector3 s_vDebugOutputFocusPosition(0.0f, 0.0f, 0.0f);
static float s_fDebugOutputFocusRadius = 1.0f;
static bool s_bWatermarkMessagePrinted = false;
#endif // __OUTPUT_FOCUS_AREA

// Whether to only process cover within a focus area
#define __PROCESS_FOCUS_AREA_ONLY 0

// Define location and radius for focus area processing
#if __PROCESS_FOCUS_AREA_ONLY
static Vector3 s_vDebugProcessFocusPosition(0.0f, 0.0f, 0.0f);
static float s_fDebugProcessFocusRadius = 1.0f;
#endif // __PROCESS_FOCUS_AREA_ONLY

//***************************************************************************************

float CNavGen::ms_fLowCoverSpacingAlongEdges						= 0.0f;
#if NAVGEN_OBSOLETE
float CNavGen::ms_fMinCoverPointSeparationWhenThinningOut			= 1.0f;
#endif
const float CNavGen::ms_fCheckCoverBetweenPointsStepSize			= 0.4f;
const float CNavGen::ms_fMaxCoverLinkDistance						= 15.0f;

bool bCreateCoverFromFixedObjects = true;	//false;
bool bIgnoreStairsForLowWallCoverDirection = true;

// Global defaults for probe methods
const bool bVerticalDefault = false;
const bool bIgnoreClimbableDefault = true;
const int iFlagsToTestDefault = 0;

// For low wall cover, whether to ignore SeeThrough objects
const bool bLowWallIgnoreSeeThrough = true;


//***************************************************************************************

int g_iNumCoverPointsFound = 0;

bool
CNavGen::DoesCoverPointAlreadyExist(vector<CNavMeshCoverPoint> & coverPoints, const Vector3 & point, u8 * iCoverDir, u32 iCoverType, const Vector3 & vNavMeshMin, const Vector3 & vNavMeshSize, float fEps, float fRatioClearToBlocked)
{
	// cover dirs within 45degs of each other will be checked as possible duplicates or considered for merging
	static const float fSameAngleDot = 0.707f;

	// If the query coverpoint is a "wall to left/right" corner
	//  we may merge or adjust an existing corner coverpoint:
	static const float fLeftAndRightCombinationDot = 0.95f;	// cover dirs within about 75degs of each other may be considered
	static const float fLeftAndRightCombinationDist = 0.35f;	//0.2f;	// cover points within this dist of each other may be considered
	// otherwise if the point is this far away it is not a duplicate (so return false)
	static const float fLeftAndRightMinDist = 0.5f;

	Vector3 vCoverDir;
	float fEpsSqr = fEps*fEps;

	if(iCoverDir)
	{
		Vector3FromCoverDir(vCoverDir, *iCoverDir);
	}

	for(u32 c=0; c<coverPoints.size(); c++)
	{
		const CNavMeshCoverPoint & coverPoint = coverPoints[c];
		Vector3 vExistingVertex;
		DecompressVertex(vExistingVertex, coverPoint.m_iX, coverPoint.m_iY, coverPoint.m_iZ, vNavMeshMin, vNavMeshSize);

		// Is there a cover point already at this position, or nearby ?
		float fDistSqr = (vExistingVertex - point).Mag2();

		if(fDistSqr < fEpsSqr)
		{
			// If we've specified a direction, then only count this point as similar if it has roughly the same orientation
			if(iCoverDir)
			{
				Vector3 vExistingCoverDir;
				Vector3FromCoverDir(vExistingCoverDir, (u8)coverPoint.m_CoverPointFlags.GetCoverDir());
				float fDot = DotProduct(vCoverDir, vExistingCoverDir);

				if(fDot > fSameAngleDot)
				{
					// If the dot products here are pretty much exact this might be a candidate to consider combining with this coverpoint
					if( fDot > fLeftAndRightCombinationDot )
					{
						// If one coverpoint is left and the other right, then try to combine them together to create
						// a single coverpoint that can fire either to the left or the right
						if( ( iCoverType == NAVMESH_COVERPOINT_WALL_TO_LEFT && coverPoint.m_CoverPointFlags.GetCoverType() == NAVMESH_COVERPOINT_WALL_TO_RIGHT ) || 
							( iCoverType == NAVMESH_COVERPOINT_WALL_TO_RIGHT && coverPoint.m_CoverPointFlags.GetCoverType() == NAVMESH_COVERPOINT_WALL_TO_LEFT ) )
						{
							// Work out the horizontal directional vector
							Vector3 vHoriz;
							vHoriz.Cross(vExistingCoverDir, Vector3(0.0f, 0.0f, 1.0f));

							// Use this to work out the horizontal distance between the 2 points
							const float fHorizontalDistance = vHoriz.Dot(vExistingVertex) - vHoriz.Dot(point);
							const float fabsHorizontalDistance = fabsf( fHorizontalDistance );

							// If horizontally close, shift the point to be horizontally between the 2 points
							if( fabsHorizontalDistance < fLeftAndRightCombinationDist )
							{
								coverPoints[c].m_CoverPointFlags.SetCoverType( NAVMESH_COVERPOINT_WALL_TO_NEITHER );
								Vector3 vCombinedVetrtex;

								// Place the point at whichever point is furthest back
								if( vExistingVertex.Dot(vExistingCoverDir) > point.Dot(vExistingCoverDir) )
								{
									vCombinedVetrtex = point + ( vHoriz * fHorizontalDistance * 0.5f );
								}
								else
								{
									vCombinedVetrtex = vExistingVertex - ( vHoriz * fHorizontalDistance * 0.5f );
								}

								CompressVertex(vCombinedVetrtex, coverPoints[c].m_iX, coverPoints[c].m_iY, coverPoints[c].m_iZ, vNavMeshMin, vNavMeshSize);
							}
							// Allow left and right points to overlap within tighter distances than other points
							else if( fabsHorizontalDistance > fLeftAndRightMinDist )
							{
								return false;
							}
						}
						// JB : I'm not sure this bit below is doing the right thing. Am gonna rewrite it :
						// If these 2 coverpoints are at roughly the same position & are both LEFT or RIGHT
						// cover, then choose the one which is closest to the LEFT or RIGHT.
						else if( iCoverType == coverPoint.m_CoverPointFlags.GetCoverType() )
						{
							Vector3 vTangent;
							vTangent.Cross(vExistingCoverDir, Vector3(0.0f,0.0f,1.0f));
							Vector3 vToNewCP = point - vExistingVertex;
							vToNewCP.z = 0.0f;
							float fHorizDot = vTangent.Dot(vToNewCP);

							float fEps = 0.05f;
							if((iCoverType == NAVMESH_COVERPOINT_WALL_TO_LEFT && fHorizDot > fEps) ||
								(iCoverType == NAVMESH_COVERPOINT_WALL_TO_RIGHT && fHorizDot < -fEps))
							{
								CompressVertex( point, coverPoints[c].m_iX, coverPoints[c].m_iY, coverPoints[c].m_iZ, vNavMeshMin, vNavMeshSize);
								coverPoints[c].m_fRatioClearToBlocked = fRatioClearToBlocked;
								coverPoints[c].m_fPosOnNavMeshX = point.x;
								coverPoints[c].m_fPosOnNavMeshY = point.y;
								coverPoints[c].m_fPosOnNavMeshZ = point.z;
							}
						}
						/*
						// If the cover points are covering the same direction, use the one closest to the firing angle
						else if( iCoverType == coverPoint.m_CoverPointFlags.GetCoverType() )
						{
							// Work out the horizontal directional vector
							Vector3 vHoriz;
							vHoriz.Cross(vExistingCoverDir, Vector3(0.0f, 0.0f, 1.0f));
							Vector3 vFromTo = point - vExistingVertex;
							vFromTo.z = 0.0f;
							float fHorizDot = vHoriz.Dot(vFromTo);

							if( fabsf(fHorizDot) > 0.05f &&
								( ( iCoverType == NAVMESH_COVERPOINT_WALL_TO_LEFT 
								&& fHorizDot < 0.0f ) || 
								( iCoverType == NAVMESH_COVERPOINT_WALL_TO_RIGHT 
								&& fHorizDot > 0.0f  ) ) )
							{
								CompressVertex( point, coverPoints[c].m_iX, coverPoints[c].m_iY, coverPoints[c].m_iZ, vNavMeshMin, vNavMeshSize);
							}
						}
						*/
					}

					// Else - these corner-coverpoints in roughly the same direction, and are coincident,
					// Choose the one with the ratio of clear/blocked most equal to 1.0f
					else if((iCoverType == NAVMESH_COVERPOINT_WALL_TO_LEFT || iCoverType == NAVMESH_COVERPOINT_WALL_TO_LEFT)
						&& iCoverType == coverPoint.m_CoverPointFlags.GetCoverType())
					{
						float fCloseness = Abs(1.0f - fRatioClearToBlocked);
						float fExistingCloseness = Abs(1.0f - coverPoint.m_fRatioClearToBlocked);
						
						if(fCloseness < fExistingCloseness)
						{
							coverPoints[c].m_fRatioClearToBlocked = fRatioClearToBlocked;
							coverPoints[c].m_fPosOnNavMeshX = point.x;
							coverPoints[c].m_fPosOnNavMeshY = point.y;
							coverPoints[c].m_fPosOnNavMeshZ = point.z;

							CompressVertex( point, coverPoints[c].m_iX, coverPoints[c].m_iY, coverPoints[c].m_iZ, vNavMeshMin, vNavMeshSize);
						}
					}

					return true;
				}
			}
			else
			{
				return true;
			}
		}
	}

	return false;
}


//**************************************************************************
//
//	DoesEdgeProvideCover
//
//	Determines whether an edge provides adequate cover from the direction
//	of the edge normal.  This is *only* done for edges which have no
//	adjacent polygons over them (ie. the are on an edge of the mesh).
//	We determine this by stepping along the edge & performing at regularly
//	spaced line-tests in the direction of the edge normal, at chest height.
//	If more than 50% of the linetests hit obstructions, then this edge will
//	provide good cover for pathfinding purposes.  This will allow paths to
//	keep to low walls etc, whilst trying to get to some destination.
//
//	vEdgeVec is the normalized direction of the edge (v1 -> v2)
//
//**************************************************************************

bool
CNavGen::DoesEdgeProvideCover(
	const int iNumNavGens,
	CNavGen ** pNavGens,
	const Vector3 & vEdgeVertex1,
	const Vector3 & vEdgeVec,
	const float fEdgeLength,
	const Vector3 & vEdgeNormal,
	Vector3 & out_vAverageNormalOfSurfacesHit,	// This param is passed out as the averaged vector of the collision geometry we've hit with our ray-tests
	bool & out_bEdgeMightProvideLowCover)
{
	// We'll assume this edge may provide low cover (to check for later), unless we prove it not to below..
	out_bEdgeMightProvideLowCover = true;

	// Zero the average normal initially
	out_vAverageNormalOfSurfacesHit.Zero();

	// The distance along the edge axis between cover direction probes 
	static const float fEdgeStepSpacing = 0.125f;

	// The height of the cover direction probing that we want to hit protective cover
	static const float fTestHeight = 0.8f;	// was 0.7f, was 1.0f

	// The length of the cover direction probes
	static const float fTestLength = 4.0f;

	// The height of the cover direction probing that we want to be clear for firing
	static const float fTestHeight_High = 1.5f;

	// The max permitted ratio of blocked high height probes for firing clearance
	static const float fMaxRatioOfHighBlockedLOSForCover = 0.5f;
	
	// The max permitted ratio of clear low height probes for safe cover
	static const float fMaxRatioOfUnblockedToBlockedForCover = 0.5f;

	// The start point of the low height probes
	Vector3 vTestPos = vEdgeVertex1;
	vTestPos.z += fTestHeight;

	// The start point of the high height probes
	Vector3 vTestPos_High = vEdgeVertex1;
	vTestPos_High.z += fTestHeight_High;

	// The end point of the low and high height probes
	Vector3 vTestEndPos, vTestEndPos_High;

	// The cover direction offset defining the probe endpoints relative to the start points
	Vector3 vTestVec = vEdgeNormal * fTestLength;

	// The offset defining the next probe start relative to the previous probe start along the edge direction
	Vector3 vIncrement = vEdgeVec * fEdgeStepSpacing;

	// The number of probes to perform at each height as a function of the total edge length and the space between probes.
	float fNumTestsToDo = fEdgeLength / fEdgeStepSpacing;
	
	// The max number of blocked results at high height to consider this cover as having valid firing clearance
	int iMaxNumBlockedLOS_High = ((int)(fNumTestsToDo * fMaxRatioOfHighBlockedLOSForCover)) + 1;

	// The max number of clear results at low height to consider this cover as providing adequate protection
	int iMaxNumOfUnblockedToNotProvideCover = ((int)(fNumTestsToDo * fMaxRatioOfUnblockedToBlockedForCover)) + 1;

	// Local variables
	Vector3 vHitPos;
	Vector3 vNormalFromIntersections(0.0f,0.0f,0.0f);
	Vector3 vLastIntersectionPosition(0.0f,0.0f,0.0f);
	int iNumTests = 0;
	int iNumBlockedLOS = 0;
	int iNumUnBlockedLOS = 0;
	int iNumBlockedLOS_High = 0;
	float fProgress = 0.0f;
	bool bTestForHighLOS = true;

#if __BREAK_ON_COVERPOINT
	if( s_vTestVec.Dist2(vTestPos) < s_fTestDistance2 )
	{
		bool bPause = true; bPause;
	}
#endif

	// Define the extents of the geometry volume of interest
	const Vector3 vEndVec = vEdgeVertex1 + (vEdgeVec * fEdgeLength);
	const float fGeomExpand = fTestLength + Max(fTestHeight, fTestHeight_High);
	Vector3 vGeomMin, vGeomMax;
	vGeomMin.Min(vEdgeVertex1, vEndVec);
	vGeomMax.Max(vEdgeVertex1, vEndVec);
	vGeomMin -= Vector3(fGeomExpand, fGeomExpand, fGeomExpand);
	vGeomMax += Vector3(fGeomExpand, fGeomExpand, fGeomExpand);

	// Initialize the defined culled geometry of interest for probing tests
	CCulledGeom geomCuller;
	InitGeomCuller(geomCuller, vGeomMin, vGeomMax, iNumNavGens, pNavGens);

	// Traverse the edge distance
	while(fProgress < fEdgeLength)
	{
		// Define the probe endpoints in the cover direction
		vTestEndPos = vTestPos + vTestVec;
		vTestEndPos_High = vTestPos_High + vTestVec;

		// If we are still checking at high height
		// AND the LOS test hits an obstruction
		if(bTestForHighLOS && 
#if __USE_GEOM_CULLER_FOR_EDGE_TEST
			!geomCuller.TestLineOfSight(vTestPos_High, vTestEndPos_High, COVER_COLLISION_TYPES, bVerticalDefault, iFlagsToTestDefault, bLowWallIgnoreSeeThrough) )
#else
			!TestMultipleNavGenLos(vTestPos_High, vTestEndPos_High, iNumNavGens, pNavGens) )
#endif
		{
			// Increment the obstruction count
			iNumBlockedLOS_High++;

			// Check if the high height obstruction count exceeds the limit
			if(iNumBlockedLOS_High > iMaxNumBlockedLOS_High)
			{
				// Flag this edge so that we don't test it extensively for low-cover points later.  This should help eliminate
				// a lot of expensive line-tests like along enclosed corridors, etc.
				out_bEdgeMightProvideLowCover = false;
				bTestForHighLOS = false;

#if __OUTPUT_FOCUS_AREA
				const Vector3 vDebugPoint = vEdgeVertex1;
				const Vector3 vLOSA = vTestPos_High;
				const Vector3 vLOSB = vTestEndPos_High;
				if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
				{
					Displayf("FOCUS: DoesEdgeProvideCover [Check0] [%.2f, %.2f, %.2f] LOS [%.2f, %.2f, %.2f] to [%.2f, %.2f, %.2f] failed too many clear checks",
						vDebugPoint.x, vDebugPoint.y, vDebugPoint.z, vLOSA.x, vLOSA.y, vLOSA.z, vLOSB.x, vLOSB.y, vLOSB.z );
				}
#endif // __OUTPUT_FOCUS_AREA
			}
		}

		// If this edge may still be considered to maybe provide cover, then we have to do a full ProcessLOS - in order
		// to get the surface normal back for averaging
		if(out_bEdgeMightProvideLowCover)
		{
			CNavGenTri * pHitTri = NULL;
			float fClosestHitDist;
			u32 iRetFlags = 0;

#if __USE_GEOM_CULLER_FOR_EDGE_TEST
			if(geomCuller.ProcessLineOfSight(vTestPos, vTestEndPos, iRetFlags, COVER_COLLISION_TYPES, vHitPos, fClosestHitDist, pHitTri, bVerticalDefault, iFlagsToTestDefault, bLowWallIgnoreSeeThrough) )
#else
			if(!ProcessMultipleNavGenLos(vTestPos, vTestEndPos, iNumNavGens, pNavGens, &vHitPos, fClosestHitDist, pHitTri, false))
#endif		
			{
				// We hit something.  Increment our counter of how many times the LOS was blocked.
				if( iNumBlockedLOS > 0 )
				{
					vNormalFromIntersections = vHitPos - vLastIntersectionPosition;
				}

				vLastIntersectionPosition = vHitPos;
				iNumBlockedLOS++;
			}
			else
			{
				iNumUnBlockedLOS++;
			}
		}
		// ..Otherwise we can just do a TestLOS.
		else
		{
#if __USE_GEOM_CULLER_FOR_EDGE_TEST
			if(!geomCuller.TestLineOfSight(vTestPos, vTestEndPos, COVER_COLLISION_TYPES, bVerticalDefault, iFlagsToTestDefault, bLowWallIgnoreSeeThrough))
#else
			if(!TestMultipleNavGenLos(vTestPos, vTestEndPos, iNumNavGens, pNavGens))
#endif
			{
				// We hit something.  Increment our counter of how many times the LOS was blocked.
				iNumBlockedLOS++;
			}
			else
			{
				iNumUnBlockedLOS++;
			}
		}

		if(iNumUnBlockedLOS > iMaxNumOfUnblockedToNotProvideCover)
		{
#if __OUTPUT_FOCUS_AREA
			const Vector3 vDebugPoint = vEdgeVertex1;
			if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
			{
				Displayf("FOCUS: DoesEdgeProvideCover [Check1] [%.2f, %.2f, %.2f] failed too many covered checks", vDebugPoint.x, vDebugPoint.y, vDebugPoint.z);
			}
#endif // __OUTPUT_FOCUS_AREA

			out_bEdgeMightProvideLowCover = false;
			return false;
		}

		iNumTests++;
		vTestPos += vIncrement;
		vTestPos_High += vIncrement;
		fProgress += fEdgeStepSpacing;
	}

#ifdef __USE_CALCULATED_ADJPOLY_NORMALS
	if( iNumBlockedLOS > 1 )
	{
		vNormalFromIntersections.Normalize();
		out_vAverageNormalOfSurfacesHit.Cross(vNormalFromIntersections, Vector3(0.0f, 0.0f, 1.0f));
	}
#endif

	float fRatio = ((float)iNumBlockedLOS) / ((float)iNumTests);

	if(fRatio >= fMaxRatioOfUnblockedToBlockedForCover)
	{
		return true;
	}

#if __OUTPUT_FOCUS_AREA
	const Vector3 vDebugPoint = vEdgeVertex1;
	if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
	{
		Displayf("FOCUS: DoesEdgeProvideCover [Check2] [%.2f, %.2f, %.2f] failed clear/covered ratio check", vDebugPoint.x, vDebugPoint.y, vDebugPoint.z);
	}
#endif // __OUTPUT_FOCUS_AREA

	out_bEdgeMightProvideLowCover = false;
	return false;
}







bool
CNavGen::AddCoverPointToNavMeshQuadTree(CNavMeshQuadTree * pTree, CNavMeshCoverPoint & coverPoint, Vector3 & vPosition)
{
	if(pTree->m_pLeafData)
	{
		int iNumCover = pTree->m_pLeafData->m_iNumCoverPoints;
		CNavMeshCoverPoint * pCoverData = rage_new CNavMeshCoverPoint[iNumCover+1];

		if( pTree->m_pLeafData->m_CoverPoints )
		{
			memcpy(pCoverData, pTree->m_pLeafData->m_CoverPoints, sizeof(CNavMeshCoverPoint) * iNumCover);
			delete[] pTree->m_pLeafData->m_CoverPoints;
		}
		else
		{
			ASSERT( iNumCover == 0 );
		}

		memcpy(&pCoverData[iNumCover], &coverPoint, sizeof(CNavMeshCoverPoint));

		pTree->m_pLeafData->m_CoverPoints = pCoverData;
		pTree->m_pLeafData->m_iNumCoverPoints++;

		return true;
	}

	for(int q=0; q<4; q++)
	{
		if(vPosition.x >= pTree->m_pChildren[q]->m_Mins.x && vPosition.y >= pTree->m_pChildren[q]->m_Mins.y &&
			vPosition.x < pTree->m_pChildren[q]->m_Maxs.x && vPosition.y < pTree->m_pChildren[q]->m_Maxs.y)
		{
			if(AddCoverPointToNavMeshQuadTree(pTree->m_pChildren[q], coverPoint, vPosition))
			{
				return true;
			}
		}
	}

	return false;
}

void
CNavGen::ClearCoverPointsInNavMesh(CNavMeshQuadTree * pTree)
{
	if(pTree->m_pLeafData)
	{
		pTree->m_pLeafData->m_iNumCoverPoints = 0;

		if(pTree->m_pLeafData->m_CoverPoints)
		{
			delete[] pTree->m_pLeafData->m_CoverPoints;
			pTree->m_pLeafData->m_CoverPoints = NULL;
		}

		return;
	}

	for(int q=0; q<4; q++)
	{
		ClearCoverPointsInNavMesh(pTree->m_pChildren[q]);
	}
}



void
CNavGen::SetIndexOfFirstCoverPointInQuadtreeLeaves(CNavMeshQuadTree * pTree)
{
	if(pTree->m_pLeafData)
	{
		if(g_iTotalNumCoverPointsCount >= 65535) { printf("ERROR - coverpoint overflow!\n"); }

		pTree->m_pLeafData->m_iIndexOfFirstCoverPointInList = (u16)g_iTotalNumCoverPointsCount;
		g_iTotalNumCoverPointsCount += pTree->m_pLeafData->m_iNumCoverPoints;
		return;
	}

	for(int q=0; q<4; q++)
	{
		SetIndexOfFirstCoverPointInQuadtreeLeaves(pTree->m_pChildren[q]);
	}
}

//***************************************************************
//	FindCoverPoints
//***************************************************************

void
CNavGen::FindCoverPoints(CNavGen * pNavGen, CNavMesh * pNavMesh, int iNumNavGens, CNavGen ** pNavGens)
{
	vector<CNavMeshCoverPoint> coverPoints;
	coverPoints.reserve(COVERPOINTS_RESERVE_SIZE);

#if __OUTPUT_FOCUS_AREA
	if( !s_bWatermarkMessagePrinted )
	{
		// print proof that output focus area is in the executable
		Displayf("FOCUS: __OUTPUT_FOCUS_AREA enabled in CNavGen::FindCoverPoints");
		s_bWatermarkMessagePrinted = true;
	}
#endif // __OUTPUT_FOCUS_AREA

	//------------------------------------------------------------------------------------------------
	// First add any custom coverpoints specified in data
	u32 iCustomCoverPoint;
	for(iCustomCoverPoint=0; iCustomCoverPoint < pNavGen->m_CustomCoverpointsCurrentNavMesh.size(); iCustomCoverPoint++)
	{
		CNavCustomCoverpoint* pCustomCoverpoint = pNavGen->m_CustomCoverpointsCurrentNavMesh[iCustomCoverPoint];
		Vector3 vCustomCoverpointPos(pCustomCoverpoint->m_CoordsX,pCustomCoverpoint->m_CoordsY,pCustomCoverpoint->m_CoordsZ);

		CNavMeshCoverPoint coverPointToAdd;

		CompressVertex(vCustomCoverpointPos, coverPointToAdd.m_iX, coverPointToAdd.m_iY, coverPointToAdd.m_iZ, pNavMesh->m_pQuadTree->m_Mins, pNavMesh->m_vExtents);
		coverPointToAdd.m_CoverPointFlags.SetCoverType( pCustomCoverpoint->m_Type );
		coverPointToAdd.m_CoverPointFlags.SetCoverDir( pCustomCoverpoint->m_Direction );

		if(!(coverPoints.size() % COVERPOINTS_RESERVE_SIZE))
		{
			coverPoints.reserve(coverPoints.size() + COVERPOINTS_RESERVE_SIZE);
		}

		coverPoints.push_back(coverPointToAdd);
	}

	FindCornerCoverPoints(pNavGen, pNavMesh, iNumNavGens, pNavGens, coverPoints);

	FindLowWallCoverPoints(pNavMesh, iNumNavGens, pNavGens, coverPoints);	

	//------------------------------------------------------------------------------------------------
	// Now remove any coverpoints intersecting an area marked with CNavResolutionArea::FLAG_NO_COVER

	u32 c;
	for(c=0; c<coverPoints.size(); c++)
	{
		const CNavMeshCoverPoint & coverPoint = coverPoints[c];
		Vector3 vPosition;
		DecompressVertex(vPosition, coverPoint.m_iX, coverPoint.m_iY, coverPoint.m_iZ, pNavMesh->m_pQuadTree->m_Mins, pNavMesh->m_vExtents);

		if(pNavGen->IntersectsResolutionArea( CNavResolutionArea::FLAG_NO_COVER, vPosition, vPosition))
		{
			coverPoints.erase(coverPoints.begin() + c);
			c--;
		}
	}


	//**************************************************************************
	//	Now we need to actually add these cover-points to the navmesh class.
	//	We'll add them to the Quadtree structure.
	//	There is now a hard limit to the max num of coverpoints per navmesh,
	//	due to the need to have left & right coverpoint linking - and in an
	//	attempt to squeeze any link's info into 16bits.
	//**************************************************************************

	if(coverPoints.size() > MAX_NUM_COVERPOINTS_PER_NAVMESH)
	{
		char tmp[256];
		sprintf(tmp, "ERROR - This navmesh has too many coverpoints\n");
		OutputDebugString(tmp);
		printf(tmp);

		sprintf(tmp, "ERROR - This navmesh has %i coverpoints, the maximum allowed is %i\n", coverPoints.size(), MAX_NUM_COVERPOINTS_PER_NAVMESH);
		OutputDebugString(tmp);
		printf(tmp);
	}

	size_t iNumCoverPoints = coverPoints.size();
	if(iNumCoverPoints > MAX_NUM_COVERPOINTS_PER_NAVMESH)
		iNumCoverPoints = MAX_NUM_COVERPOINTS_PER_NAVMESH;

	for(c=0; c<iNumCoverPoints; c++)
	{
		CNavMeshCoverPoint coverPoint = coverPoints[c];

		Vector3 vPosition;
		DecompressVertex(vPosition, coverPoint.m_iX, coverPoint.m_iY, coverPoint.m_iZ, pNavMesh->m_pQuadTree->m_Mins, pNavMesh->m_vExtents);

#if __BREAK_ON_COVERPOINT
		if(s_vTestVec.IsClose(vPosition, vEps))
		{
			bool bBreak=true; bBreak;
		}
#endif

		AddCoverPointToNavMeshQuadTree(pNavMesh->m_pQuadTree, coverPoint, vPosition);
	}

	// We have to set a member in every quadtree leaf which tells us what the index (within the navmesh) of
	// the 1st coverpoint in that leaf is.
	g_iTotalNumCoverPointsCount = 0;
	SetIndexOfFirstCoverPointInQuadtreeLeaves(pNavMesh->m_pQuadTree);
}

void
CNavGen::FindLowWallCoverPoints(CNavMesh * pNavMesh, int iNumNavGens, CNavGen ** pNavGens, vector<CNavMeshCoverPoint> & coverPoints)
{
	u32 p;
	u32 lastv, v;
	Vector3 vGroundPos, vVec, vLastVec;

	//**********************************************************************************
	//	Firstly we will create cover points which have the NAVMESH_COVERPOINT_LOW_WALL
	//	flag set.  These are used by the game for hiding behind, and the flag is
	//	the equivalent of COVUSE_LOWCOVER in Cover.h
	//	These are easy to find, because we already know which edges potentially
	//	provide cover - we need only to perform a line-test to ascertain whether this
	//	cover is 'low' or not..
	//**********************************************************************************

	CCulledGeom geometry;

	const float fSpacingAlongEdge = ms_fLowCoverSpacingAlongEdges;
	Assert(fSpacingAlongEdge > 0.0f);	// Here to catch variable initialization problems.

	for(p=0; p<pNavMesh->m_iNumPolys; p++)
	{
		TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);

		// For certain navmesh surfaces we cannot make coverpoints.  eg. water
		if(pPoly->TestFlags(NAVMESHPOLY_IS_WATER))
			continue;
		if(pPoly->TestFlags(NAVMESHPOLY_TOO_STEEP_TO_WALK_ON))
			continue;

		lastv = pPoly->GetNumVertices()-1;
		for(v=0; v<pPoly->GetNumVertices(); v++)
		{
			const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly(pPoly->GetFirstVertexIndex()+lastv);

			// If we already know that this edge *won't* provide low cover OR this edge is a normal
			// connection between two polys, then don't look for low cover along this edge
			if(adjPoly.m_bEdgeMightProvideLowCover==false ||
				(adjPoly.GetAdjacencyType()==ADJACENCY_TYPE_NORMAL && adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes())!=NAVMESH_NAVMESH_INDEX_NONE && adjPoly.GetPolyIndex()!=NAVMESH_POLY_INDEX_NONE))
			{
#if __OUTPUT_FOCUS_AREA
				pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, v), vVec);
				const Vector3 vDebugPoint = vVec;
				if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
				{
					if( (adjPoly.GetAdjacencyType()==ADJACENCY_TYPE_NORMAL && adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes())!=NAVMESH_NAVMESH_INDEX_NONE && adjPoly.GetPolyIndex()!=NAVMESH_POLY_INDEX_NONE) )
					{
						Displayf("FOCUS: FindLowWallCoverPoints [Check0] [%.2f, %.2f, %.2f] Poly[0x%x] skipped, internal navmesh edge", vDebugPoint.x, vDebugPoint.y, vDebugPoint.z, &adjPoly);
						
					}
					else if( adjPoly.m_bEdgeMightProvideLowCover==false )
					{
						Displayf("FOCUS: FindLowWallCoverPoints [Check0] [%.2f, %.2f, %.2f] Poly[0x%x] skipped, won't provide low cover", vDebugPoint.x, vDebugPoint.y, vDebugPoint.z, &adjPoly);
					}
				}
#endif // __OUTPUT_FOCUS_AREA

				lastv = v;
				continue;
			}

			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, lastv), vLastVec);
			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, v), vVec);

			Vector3 vEdgeVec3d = vVec - vLastVec;
			float fEdgeLength = vEdgeVec3d.Mag();
			vEdgeVec3d.Normalize();

			Vector3 vTmp = CrossProduct(vEdgeVec3d, Vector3(0,0,1.0f));
			vTmp.Normalize();
			//vTmp.NormalizeFast();

			Vector3 vUp3d = CrossProduct(vEdgeVec3d, vTmp);
			vUp3d.Normalize();
			//vUp3d.NormalizeFast();

			Vector3 vEdgeNormal3d = CrossProduct(vUp3d, vEdgeVec3d);
			vEdgeNormal3d.Normalize();


#if __USE_GEOM_CULLER_FOR_LOW_WALL
			Vector3 vGeomMin, vGeomMax;
			vGeomMin.Min(vLastVec, vVec);
			vGeomMax.Max(vLastVec, vVec);
			const float fGeomExtend = fwNavGenConfig::Get().m_CoverLowWallTestDistance + 3.0f;
			vGeomMin -= Vector3(fGeomExtend,fGeomExtend,fGeomExtend);
			vGeomMax += Vector3(fGeomExtend,fGeomExtend,fGeomExtend);
			InitGeomCuller(geometry, vGeomMin, vGeomMax, iNumNavGens, pNavGens);
#endif
			//			static const float fSineZAngleTolerance = CMaths::Sin(DEGTORAD(45.0f));
			//			if( ABS(vEdgeNormal3d.z) < fSineZAngleTolerance )
			{
				//vEdgeNormal3d.NormalizeFast();
				Vector3 vIntersectPos;
				Vector3 vEdgeNormal2d = vEdgeNormal3d;
				vEdgeNormal2d.z =0.0f;
				vEdgeNormal2d.Normalize();
				// If this edge is too small to step along checking for cover - then just use the mid-point
				// and try to add a single coverpoint at this position.
				// JB : changed this : we now try 3 positions : middle, last-vert & next-vert
				if(fEdgeLength <= fSpacingAlongEdge)
				{
					Vector3 vPointsToTest[3] = { (vLastVec + vVec) * 0.5f, vLastVec, vVec };
					for(int s=0; s<3; s++)
					{
						Vector3 vPointOnEdge = vPointsToTest[s];
						vPointOnEdge.z += 0.3f;

						// Move back from any walls, etc
						// To left of center point
#if __USE_GEOM_CULLER_FOR_LOW_WALL
						bool bLos = geometry.TestLineOfSight( vPointOnEdge, vPointOnEdge - (vEdgeNormal2d * 0.3f), COVER_COLLISION_TYPES, bVerticalDefault, iFlagsToTestDefault, bLowWallIgnoreSeeThrough );
#else
						bool bLos = TestMultipleNavGenLos(
							vPointOnEdge,
							vPointOnEdge - (vEdgeNormal2d * 0.3f),
							iNumNavGens,
							pNavGens,
							&vIntersectPos);
#endif
						if(bLos)
						{
							vPointOnEdge -= (vEdgeNormal2d * 0.3f);
						}
						else
						{
							vPointOnEdge -= vEdgeNormal2d;
						}

						// Find exact intersection with floor ?
						CheckForLowWallCoverPoint(geometry, pNavMesh, adjPoly, vPointOnEdge, vEdgeVec3d, vEdgeNormal3d, iNumNavGens, pNavGens, coverPoints);
					}
				}
				// Otherwise, step along the extent of the edge - and add coverpoints all the way along
				else
				{
					// Start from either end and work inwards.
					float fHalfLength = fEdgeLength * 0.5f;
					for(float fEdgePos=0.0f; fEdgePos<=fHalfLength; fEdgePos+=fSpacingAlongEdge)
					{
						// search for cover from the left
						Vector3 vPointOnEdge = vLastVec + (vEdgeVec3d * fEdgePos);
						vPointOnEdge.z += 0.3f;

						// To left of center point
#if __USE_GEOM_CULLER_FOR_LOW_WALL
						float fTestDist;
						CNavGenTri * pTestTri;
						u32 iRetFlags = 0;
						bool bLos = !geometry.ProcessLineOfSight(vPointOnEdge, vPointOnEdge - (vEdgeNormal2d * 0.3f), iRetFlags, COVER_COLLISION_TYPES, vIntersectPos, fTestDist, pTestTri, bVerticalDefault, iFlagsToTestDefault, bLowWallIgnoreSeeThrough );
#else
						bool bLos = TestMultipleNavGenLos(
							vPointOnEdge,
							vPointOnEdge - (vEdgeNormal2d * 0.3f),
							iNumNavGens,
							pNavGens,
							&vIntersectPos);
#endif
						if(bLos)
						{
							vPointOnEdge -= (vEdgeNormal2d * 0.3f);
						}
						else
						{
							vPointOnEdge = vIntersectPos;
						}
						CheckForLowWallCoverPoint(geometry, pNavMesh, adjPoly, vPointOnEdge, vEdgeVec3d, vEdgeNormal3d, iNumNavGens, pNavGens, coverPoints);
						// Search for cover from the right
						Vector3 vPointOnEdge2 = vLastVec + (vEdgeVec3d * (fEdgeLength-fEdgePos));
						vPointOnEdge2.z += 0.3f;

						// To left of center point
#if __USE_GEOM_CULLER_FOR_LOW_WALL
						bLos = !geometry.ProcessLineOfSight(vPointOnEdge2, vPointOnEdge2 - (vEdgeNormal2d * 0.3f), iRetFlags, COVER_COLLISION_TYPES, vIntersectPos, fTestDist, pTestTri, bVerticalDefault, iFlagsToTestDefault, bLowWallIgnoreSeeThrough );
#else
						bLos = TestMultipleNavGenLos(
							vPointOnEdge2,
							vPointOnEdge2 - (vEdgeNormal2d * 0.3f),
							iNumNavGens,
							pNavGens,
							&vIntersectPos);
#endif
						if(bLos)
						{
							vPointOnEdge2 -= (vEdgeNormal2d * 0.3f);
						}
						else
						{
							vPointOnEdge2 = vIntersectPos;
						}

						CheckForLowWallCoverPoint(geometry, pNavMesh, adjPoly, vPointOnEdge2, vEdgeVec3d, vEdgeNormal3d, iNumNavGens, pNavGens, coverPoints);

					}
				}
			}

			lastv = v;
		}
	}
}


// vEdgeVec3d is the vector of the navmesh edge.  This is at 90degrees to the navmesh edge's normal.
// vEdgeNormal3d is the normal of the navmesh edge.  Note that the normal of the navmesh edge may be 'jagged' and not
// smoothly follow the edge of the collision geometry which it is up against.

#ifdef __USE_CALCULATED_ADJPOLY_NORMALS
bool CNavGen::CheckForLowWallCoverPoint(CCulledGeom & geometry, const CNavMesh * pNavMesh, const TAdjPoly & adjPoly, const Vector3 & vInputPointOnEdge, const Vector3 & vEdgeVec3d, const Vector3 & vEdgeNormal3d, int iNumNavGens, CNavGen ** pNavGens, vector<CNavMeshCoverPoint> & coverPoints)
#else
bool CNavGen::CheckForLowWallCoverPoint(CCulledGeom & geometry, const CNavMesh * pNavMesh, const TAdjPoly & UNUSED_PARAM(adjPoly), const Vector3 & vInputPointOnEdge, const Vector3 & vEdgeVec3d, const Vector3 & vEdgeNormal3d, int UNUSED_PARAM(iNumNavGens), CNavGen ** UNUSED_PARAM(pNavGens), vector<CNavMeshCoverPoint> & coverPoints)
#endif
{
	// This is the height at which a line-of-sight should be blocked, to affirm that there is a low wall here
	static const float fLowWallObstructionHeight = fwNavGenConfig::Get().m_CoverLowWallObstructionHeight;
	static const float fLowWallObstructionMaxHeight = fwNavGenConfig::Get().m_CoverLowWallObstructionMaxHeight;
	static const float fLowWallObstructionXOffset = fwNavGenConfig::Get().m_PedRadius;	// Note: it's a little bit awkward to do the function call and everything here, for assigning a static const variable.

	// This is the range of heights at which line-of-sights should be clear for this to be fireable over
	// Note: these were unused, but if we need them, should be moved to fwNavGenConfig. /FF
	//	static const float fLowWallMinHeight = 1.2f;
	//	static const float fLowWallMaxHeight = 1.6f;

	// This is the distance ahead of the potential coverpoint position which we shall test for a clear line-of-sight
	static const float fLowWallTestDistance = fwNavGenConfig::Get().m_CoverLowWallTestDistance;

	// The height difference of the ground to the left or right of the cover point mustn't exceed this value.
	// @TODO: Move this to fwNavGenConfig (dmorales)
	static const float fMaxHeightChangeEitherSideOfCoverPoint = 0.2f;

#if __PROCESS_FOCUS_AREA_ONLY
	if( vInputPointOnEdge.Dist2(s_vDebugProcessFocusPosition) > rage::square(s_fDebugProcessFocusRadius) )
	{
		return false;
	}
#endif // __PROCESS_FOCUS_AREA_ONLY

	//*********************************************************************************************
	// Affirm that a low line-of-sight is blocked & find out at what distance from the chosen
	// position the line-of-sight hits something.  Adjust the potential coverpoint position so
	// that it is a set distance away from the covering wall.  Discard this coverpoint if the
	// LOS is clear or if the intersection distance is too far away.
	//*********************************************************************************************

	Vector3 vEdgeNormal2d = vEdgeNormal3d;
	vEdgeNormal2d.z = 0.0f;

	// NEW : Add on the normal we we obtained from the adjacent collision geometry, when we called DoesEdgeProvideCover()
#ifdef __USE_CALCULATED_ADJPOLY_NORMALS
	vEdgeNormal2d.x = adjPoly.m_fEdgeNormalX;
	vEdgeNormal2d.y = adjPoly.m_fEdgeNormalY;
#endif

	vEdgeNormal2d.Normalize();

	u8 iCoverDir = FindCoverDirFromVector(vEdgeNormal2d);

	//*********************************************************************************
	// Make sure that there's actually some floor below this position, and not too far
	//

	Vector3 vPosAbove, vPosBelow, vHitPos;
	vPosAbove = vInputPointOnEdge;
	vPosAbove.z += 0.5f;
	vPosBelow = vInputPointOnEdge;
	vPosBelow.z -= fwNavGenConfig::Get().m_CoverLowMaxDistOfFloorBelow;

	// If there is a clear los then we can't create a coverpoint here
#if __USE_GEOM_CULLER_FOR_LOW_WALL
	float fHitDist;
	CNavGenTri * pTri;
	u32 iRetFlags = 0;
	if(!geometry.ProcessLineOfSight(vPosAbove, vPosBelow, iRetFlags, COVER_COLLISION_TYPES, vHitPos, fHitDist, pTri))
#else
	if(TestMultipleNavGenLos(vPosAbove, vPosBelow, iNumNavGens, pNavGens, &vHitPos, true))
#endif
	{
#if __OUTPUT_FOCUS_AREA
		const Vector3 vDebugPoint = vInputPointOnEdge;
		if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
		{
			Displayf("FOCUS: CheckForLowWallCoverPoint [Check0] [%.2f, %.2f, %.2f] failed floor below check", vDebugPoint.x, vDebugPoint.y, vDebugPoint.z);
		}
#endif // __OUTPUT_FOCUS_AREA
		return false;
	}

	Vector3 vPointOnEdge = vInputPointOnEdge;
	vPointOnEdge.z = vHitPos.z;
	Vector3 vCentralIntersectPos(0.0f, 0.0f, 0.0f);
	CNavGenTri * pHitTri= NULL;
	bool bLos;

#if __BREAK_ON_COVERPOINT
	if( s_vTestVec.Dist(vPointOnEdge) < s_fTestDistance )
	{
		bool bPause = true; bPause;
	}
#endif

	Vector3 vPointSlightlyAboveEdge = vPointOnEdge;
	vPointSlightlyAboveEdge.z += fLowWallObstructionHeight;

	Vector3 vEdgeVec2d = vEdgeVec3d;
	vEdgeVec2d.z = 0.0f;
	vEdgeVec2d.Normalize();

	//
	// Do a line-test to ensure that there is some low cover.  (NB : What if this is just a drop-down edge?)
	// Now do line tests at 2 height levels to make sure the point has:
	// central cover right in front, and at least 50% cover at the ped radius extents.
	//
	// 5  1  3  7
	//
	// 4  0  2  6
	//

	Vector3 vCoverRight;
	vCoverRight.Cross(vEdgeNormal2d, Vector3(0.0f,0.0f,1.0f));

	static const Vector3 ms_CoverCheckOffsets[8] = {
		Vector3(-fLowWallObstructionXOffset*0.5f, 0.0f,	fLowWallObstructionHeight		), // 0
		Vector3(-fLowWallObstructionXOffset*0.5f, 0.0f,	fLowWallObstructionMaxHeight	), // 1
		Vector3( fLowWallObstructionXOffset*0.5f, 0.0f,	fLowWallObstructionHeight		), // 2
		Vector3( fLowWallObstructionXOffset*0.5f, 0.0f,	fLowWallObstructionMaxHeight	), // 3
		Vector3(-fLowWallObstructionXOffset		, 0.0f,	fLowWallObstructionHeight		), // 4
		Vector3(-fLowWallObstructionXOffset		, 0.0f,	fLowWallObstructionMaxHeight	), // 5
		Vector3( fLowWallObstructionXOffset		, 0.0f,	fLowWallObstructionHeight		), // 6
		Vector3( fLowWallObstructionXOffset		, 0.0f,	fLowWallObstructionMaxHeight	)  // 7
	};
	int iNumOutsideSectionsInCover = 0;
	int iNumNormalsConsidered = 0;

	Vector3 avIntersections[8];
	bool	abIntesected[8] = {false, false, false, false, false, false, false, false };

	// Create an approximate 3d vector for the LOS checks
	Vector3 vApproximated3dNormal = vEdgeNormal2d;
	vApproximated3dNormal.z = vEdgeNormal3d.z;
	vApproximated3dNormal.Normalize();


	float fClosestLowObstructionDist = FLT_MAX;

	// Iterate over LOS checks
	for( int iNoIterations = 0; iNoIterations < 2; iNoIterations++ )
	{
		Vector3 vNewNormalFromLos(0.0f, 0.0f, 0.0f);
		float fDistanceToIntersection = 0.0f;
		iNumOutsideSectionsInCover = 0;
		iNumNormalsConsidered = 0;
		for( int iLineTest = 0; iLineTest < 8; iLineTest++ )
		{
			Vector3 vIntersectPos;
			float fClosestHitDist;
			u32 iRetFlags = 0;
			Vector3 vTestPos = vPointOnEdge;
			vTestPos.z	+= ms_CoverCheckOffsets[iLineTest].z;
			vTestPos	+= vCoverRight * ms_CoverCheckOffsets[iLineTest].x;

#if __USE_GEOM_CULLER_FOR_LOW_WALL
			bLos = !geometry.ProcessLineOfSight(
				vTestPos,
				vTestPos + (vApproximated3dNormal * fLowWallTestDistance),
				iRetFlags,
				COVER_COLLISION_TYPES,
				vIntersectPos,
				fClosestHitDist,
				pHitTri,
				bVerticalDefault,
				iFlagsToTestDefault,
				bLowWallIgnoreSeeThrough,		// Don't take cover behind see-thru surfaces
				!bCreateCoverFromFixedObjects,	// Don't take cover behind surfaces which are part of FIXED objects.  These will have coverpoints added in-game.
				bIgnoreClimbableDefault,
				bIgnoreStairsForLowWallCoverDirection
				);
#else
			bLos = ProcessMultipleNavGenLos(
				vTestPos,
				vTestPos + (vApproximated3dNormal * fLowWallTestDistance),
				iNumNavGens,
				pNavGens,
				&vIntersectPos,
				fClosestHitDist,
				pHitTri,
				false,
				true,							// Don't take cover behind see-thru surfaces
				!bCreateCoverFromFixedObjects	// Don't take cover behind surfaces which are part of FIXED objects.  These will have coverpoints added in-game.
			);
#endif
			if( !bLos )
			{
				if(fClosestHitDist < fClosestLowObstructionDist)
					fClosestLowObstructionDist = fClosestHitDist;

				// Make sure its a close distance to the central points
				if( (iLineTest <= 3) || ABS(fDistanceToIntersection - (vIntersectPos-vTestPos).Mag() ) < 0.25f )
				{
					vNewNormalFromLos += -pHitTri->m_vNormal;
					++iNumNormalsConsidered;
				}

				++iNumOutsideSectionsInCover;
				abIntesected[iLineTest] = true;
				avIntersections[iLineTest] = vIntersectPos;
			}

			// Los is required on the central section of cover around the player
			if( (iLineTest >= 0) && (iLineTest <= 3) )
			{
				if( bLos )
				{
#if __OUTPUT_FOCUS_AREA
					const Vector3 vDebugPoint = vPointOnEdge;
					const Vector3 vLOSA = vTestPos;
					const Vector3 vLOSB = vTestPos + (vApproximated3dNormal * fLowWallTestDistance);
					if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
					{
						Displayf("FOCUS: CheckForLowWallCoverPoint [Check1] [%.2f, %.2f, %.2f] LOS [%.2f, %.2f, %.2f] to [%.2f, %.2f, %.2f] failed required blocked check",
							vDebugPoint.x, vDebugPoint.y, vDebugPoint.z, vLOSA.x, vLOSA.y, vLOSA.z, vLOSB.x, vLOSB.y, vLOSB.z );
					}
#endif // __OUTPUT_FOCUS_AREA

					return false;
				}
				// Average central intersection positions 2 and 0
				if(iNoIterations == 0 && ( iLineTest == 2 || iLineTest == 0 ) )
				{
					vCentralIntersectPos += vIntersectPos;
					if( iLineTest == 2 )
					{
						vCentralIntersectPos.Scale(0.5f);
						fDistanceToIntersection = vCentralIntersectPos.Dist(vPointOnEdge);
					}
				}
			}
		}


		// 75% coverage of all coverpoints required
		if(iNumOutsideSectionsInCover < 6)
		{
#if __OUTPUT_FOCUS_AREA
			const Vector3 vDebugPoint = vPointOnEdge;
			if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
			{
				Displayf("FOCUS: CheckForLowWallCoverPoint [Check2] [%.2f, %.2f, %.2f] failed required number outside sections covered check", vDebugPoint.x, vDebugPoint.y, vDebugPoint.z );
			}
#endif // __OUTPUT_FOCUS_AREA

			return false;
		}

		// Recalculate the normal on the first pass
		if( iNoIterations == 0 )
		{
			Vector3 vAverageNormal(0.0f, 0.0f, 0.0f);
			ASSERT(abIntesected[0] && abIntesected[1] && abIntesected[2] && abIntesected[3]);
			// Try to build a normal from the intersection positions
			vAverageNormal += avIntersections[2] - avIntersections[0];
			vAverageNormal += avIntersections[3] - avIntersections[1];

			if( abIntesected[4] ) { vAverageNormal += avIntersections[0] - avIntersections[4]; }
			if( abIntesected[5] ) { vAverageNormal += avIntersections[1] - avIntersections[5]; }
			if( abIntesected[6] ) { vAverageNormal += avIntersections[6] - avIntersections[2]; }
			if( abIntesected[7] ) { vAverageNormal += avIntersections[7] - avIntersections[3]; }

			vAverageNormal.z = 0.0f;
			vAverageNormal.Normalize();
			vNewNormalFromLos.Cross(vAverageNormal, Vector3(0.0f, 0.0f, 1.0f));

			vEdgeNormal2d = -vNewNormalFromLos;
			vEdgeNormal2d.z = 0.0f;
			vEdgeNormal2d.Normalize();
			iCoverDir = FindCoverDirFromVector(vEdgeNormal2d);
			vApproximated3dNormal = vEdgeNormal2d;
			vApproximated3dNormal.z = vEdgeNormal3d.z;
			vApproximated3dNormal.Normalize();
			vCoverRight.Cross(vEdgeNormal2d, Vector3(0.0f,0.0f,1.0f));
		}
	}

	//***********************************************************************************
	// Make sure that this coverpoint is the correct distance away from the wall.
	//**********************************************************************************

	//if(fClosestLowObstructionDist < fFinalDistanceFromSurfaceOfCoverPoint)
	{
		vPointOnEdge -= (vApproximated3dNormal * (fwNavGenConfig::Get().m_CoverLowFinalDistanceFromSurfaceOfCoverPoint-fClosestLowObstructionDist));
	}

	// snap the point to the actual ground
	Vector3 vTestLosDown_Start = vPointOnEdge;
	vTestLosDown_Start.z += 0.2f;
	Vector3 vTestLosDown_End = vPointOnEdge;
	vTestLosDown_End.z -= 1.6f;

	// If there is an obstruction above the coverpoint, then a ped can't stand here
#if __USE_GEOM_CULLER_FOR_LOW_WALL
	if(geometry.ProcessLineOfSight(vTestLosDown_Start, vTestLosDown_End, iRetFlags, COVER_COLLISION_TYPES, vHitPos, fHitDist, pHitTri, true))
#else
	if(!TestMultipleNavGenLos(vTestLosDown_Start, vTestLosDown_End, iNumNavGens, pNavGens, &vHitPos, true))
#endif
	{
		vPointOnEdge.z = vHitPos.z;
	}
	else
	{
#if __OUTPUT_FOCUS_AREA
		const Vector3 vDebugPoint = vPointOnEdge;
		const Vector3 vLOSA = vTestLosDown_Start;
		const Vector3 vLOSB = vTestLosDown_End;
		if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
		{
			Displayf("FOCUS: CheckForLowWallCoverPoint [Check3] [%.2f, %.2f, %.2f] LOS [%.2f, %.2f, %.2f] to [%.2f, %.2f, %.2f] failed to hit the ground",
				vDebugPoint.x, vDebugPoint.y, vDebugPoint.z, vLOSA.x, vLOSA.y, vLOSA.z, vLOSB.x, vLOSB.y, vLOSB.z );
		}
#endif // __OUTPUT_FOCUS_AREA

		return false;
	}

	// Use a probe check from the final adjusted position in the final adjusted cover direction
	// It is possible we have a point that is now too far from the physical cover object.
	// This can happen because we began from the navmesh edge and not any physical object collision.
	static bool s_bDoFinalProbeCheck = true;
	if( s_bDoFinalProbeCheck )
	{
		Vector3 vTestPos = vPointOnEdge;
		vTestPos.z += fLowWallObstructionHeight;
		Vector3 vIntersectPos;
		float fClosestHitDist;
		u32 iRetVal = 0;
		bool bFinalCheckLos = !geometry.ProcessLineOfSight(
			vTestPos,
			vTestPos + (vEdgeNormal2d * fLowWallTestDistance),
			iRetVal,
			COVER_COLLISION_TYPES,
			vIntersectPos,
			fClosestHitDist,
			pHitTri,
			bVerticalDefault,
			iFlagsToTestDefault,
			bLowWallIgnoreSeeThrough,		// Don't take cover behind see-thru surfaces
			!bCreateCoverFromFixedObjects,	// Don't take cover behind surfaces which are part of FIXED objects.  These will have coverpoints added in-game.
			bIgnoreClimbableDefault,
			bIgnoreStairsForLowWallCoverDirection
			);

		// If nothing was hit at all
		if( bFinalCheckLos )
		{
#if __OUTPUT_FOCUS_AREA
			const Vector3 vDebugPoint = vPointOnEdge;
			const Vector3 vLOSA = vTestPos;
			const Vector3 vLOSB = vTestPos + (vEdgeNormal2d * fLowWallTestDistance);
			if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
			{
				Displayf("FOCUS: CheckForLowWallCoverPoint [Check3.1] [%.2f, %.2f, %.2f] LOS [%.2f, %.2f, %.2f] to [%.2f, %.2f, %.2f] failed to hit anything",
					vDebugPoint.x, vDebugPoint.y, vDebugPoint.z, vLOSA.x, vLOSA.y, vLOSA.z, vLOSB.x, vLOSB.y, vLOSB.z );
			}
#endif // __OUTPUT_FOCUS_AREA
			return false;
		}
		else // hit something
		{
			// Check if the distance to what was hit is too great
			if( fClosestHitDist > fwNavGenConfig::Get().m_CoverLowFinalProbeCheckDistanceLimit )
			{
#if __OUTPUT_FOCUS_AREA
				const Vector3 vDebugPoint = vPointOnEdge;
				const Vector3 vLOSA = vTestPos;
				const Vector3 vLOSB = vTestPos + (vEdgeNormal2d * fLowWallTestDistance);
				if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
				{
					Displayf("FOCUS: CheckForLowWallCoverPoint [Check3.1] [%.2f, %.2f, %.2f] LOS [%.2f, %.2f, %.2f] to [%.2f, %.2f, %.2f] hit at distance [%.2f], limit is [%.2f]",
						vDebugPoint.x, vDebugPoint.y, vDebugPoint.z, vLOSA.x, vLOSA.y, vLOSA.z, vLOSB.x, vLOSB.y, vLOSB.z, fClosestHitDist, fwNavGenConfig::Get().m_CoverLowFinalProbeCheckDistanceLimit );
				}
#endif // __OUTPUT_FOCUS_AREA
				return false;
			}
		}
	}

	// Looks like we don't adjust the vPointOnEdge position any more after this..
	// Disregard if we already have a point at more-or-less the same position
	if(DoesCoverPointAlreadyExist(coverPoints, vPointOnEdge, &iCoverDir, NAVMESH_COVERPOINT_LOW_WALL, pNavMesh->m_pQuadTree->m_Mins, pNavMesh->m_vExtents, fwNavGenConfig::Get().m_CoverLowPointSeparation, 9999.0f))
	{
#if __OUTPUT_FOCUS_AREA
		const Vector3 vDebugPoint = vPointOnEdge;
		if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
		{
			Displayf("FOCUS: CheckForLowWallCoverPoint [Check4] [%.2f, %.2f, %.2f] rejected as duplicate of existing cover", vDebugPoint.x, vDebugPoint.y, vDebugPoint.z );
		}
#endif // __OUTPUT_FOCUS_AREA

		return false;
	}

	// Get the ped radius from config data
	const float fPedRadius = fwNavGenConfig::Get().m_PedRadius;

	
	// Check the ground below is fairly even, by placing a line test to the left at ped radius distance
	vTestLosDown_Start = vPointOnEdge;
	vTestLosDown_Start.z += 0.4f;
	vTestLosDown_End = vPointOnEdge;
	vTestLosDown_End.z -= 1.6f;
#if __USE_GEOM_CULLER_FOR_LOW_WALL
	if(geometry.ProcessLineOfSight(vTestLosDown_Start-(vCoverRight*fPedRadius), vTestLosDown_End-(vCoverRight*fPedRadius), iRetFlags, COVER_COLLISION_TYPES, vHitPos, fHitDist, pHitTri, true ) )
#else
	if(!TestMultipleNavGenLos(vTestLosDown_Start-(vCoverRight*fPedRadius), vTestLosDown_End-(vCoverRight*fPedRadius), iNumNavGens, pNavGens, &vHitPos, true))
#endif
	{
		if( ABS(vPointOnEdge.z - vHitPos.z) > fMaxHeightChangeEitherSideOfCoverPoint )
		{
#if __OUTPUT_FOCUS_AREA
			const Vector3 vDebugPoint = vPointOnEdge;
			const Vector3 vLOSA = vTestLosDown_Start-(vCoverRight*fPedRadius);
			const Vector3 vLOSB = vTestLosDown_End-(vCoverRight*fPedRadius);
			if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
			{
				Displayf("FOCUS: CheckForLowWallCoverPoint [Check5] [%.2f, %.2f, %.2f] LOS [%.2f, %.2f, %.2f] to [%.2f, %.2f, %.2f] failed level ground front check: distance too great",
					vDebugPoint.x, vDebugPoint.y, vDebugPoint.z, vLOSA.x, vLOSA.y, vLOSA.z, vLOSB.x, vLOSB.y, vLOSB.z );
			}
#endif // __OUTPUT_FOCUS_AREA

			return false;
		}
	}
	else
	{
#if __OUTPUT_FOCUS_AREA
		const Vector3 vDebugPoint = vPointOnEdge;
		const Vector3 vLOSA = vTestLosDown_Start-(vCoverRight*fPedRadius);
		const Vector3 vLOSB = vTestLosDown_End-(vCoverRight*fPedRadius);
		if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
		{
			Displayf("FOCUS: CheckForLowWallCoverPoint [Check6] [%.2f, %.2f, %.2f] LOS [%.2f, %.2f, %.2f] to [%.2f, %.2f, %.2f] failed level ground front check: ground not hit",
				vDebugPoint.x, vDebugPoint.y, vDebugPoint.z, vLOSA.x, vLOSA.y, vLOSA.z, vLOSB.x, vLOSB.y, vLOSB.z );
		}
#endif // __OUTPUT_FOCUS_AREA

		return false;
	}

	// Check the ground below is fairly even, by placing a line test to the right at ped radius distance
#if __USE_GEOM_CULLER_FOR_LOW_WALL
	if(geometry.ProcessLineOfSight(vTestLosDown_Start+(vCoverRight*fPedRadius), vTestLosDown_End+(vCoverRight*fPedRadius), iRetFlags, COVER_COLLISION_TYPES, vHitPos, fHitDist, pHitTri, true ) )
#else
	if(!TestMultipleNavGenLos(vTestLosDown_Start+(vCoverRight*fPedRadius), vTestLosDown_End+(vCoverRight*fPedRadius), iNumNavGens, pNavGens, &vHitPos, true))
#endif
	{
		if( ABS(vPointOnEdge.z - vHitPos.z) > fMaxHeightChangeEitherSideOfCoverPoint )
		{
#if __OUTPUT_FOCUS_AREA
			const Vector3 vDebugPoint = vPointOnEdge;
			const Vector3 vLOSA = vTestLosDown_Start+(vCoverRight*fPedRadius);
			const Vector3 vLOSB = vTestLosDown_End+(vCoverRight*fPedRadius);
			if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
			{
				Displayf("FOCUS: CheckForLowWallCoverPoint [Check7] [%.2f, %.2f, %.2f] LOS [%.2f, %.2f, %.2f] to [%.2f, %.2f, %.2f] failed level ground back check: distance too great",
					vDebugPoint.x, vDebugPoint.y, vDebugPoint.z, vLOSA.x, vLOSA.y, vLOSA.z, vLOSB.x, vLOSB.y, vLOSB.z );
			}
#endif // __OUTPUT_FOCUS_AREA

			return false;
		}
	}
	else
	{
#if __OUTPUT_FOCUS_AREA
		const Vector3 vDebugPoint = vPointOnEdge;
		const Vector3 vLOSA = vTestLosDown_Start+(vCoverRight*fPedRadius);
		const Vector3 vLOSB = vTestLosDown_End+(vCoverRight*fPedRadius);
		if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
		{
			Displayf("FOCUS: CheckForLowWallCoverPoint [Check8] [%.2f, %.2f, %.2f] LOS [%.2f, %.2f, %.2f] to [%.2f, %.2f, %.2f] failed level ground back check: ground not hit",
				vDebugPoint.x, vDebugPoint.y, vDebugPoint.z, vLOSA.x, vLOSA.y, vLOSA.z, vLOSB.x, vLOSB.y, vLOSB.z );
		}
#endif // __OUTPUT_FOCUS_AREA

		return false;
	}

	//*******************************************************************************************************
	// Make sure that there is a clear LOS from the coverpoint to the height of a ped above the coverpoint
	//

	Vector3 vTestLosUp_Start = vPointOnEdge;
	vTestLosUp_Start.z += 0.1f;					// move up a little so ray doesn't penetrate the ground below
	Vector3 vTestLosUp_End = vPointOnEdge;
	vTestLosUp_End.z += 1.6f;

	// If there is an obstruction above the coverpoint, then a ped can't stand here
#if __USE_GEOM_CULLER_FOR_LOW_WALL
	if(!geometry.TestLineOfSight(vTestLosUp_Start, vTestLosUp_End, COVER_COLLISION_TYPES, true))
#else
	if(!TestMultipleNavGenLos(vTestLosUp_Start, vTestLosUp_End, iNumNavGens, pNavGens, &vHitPos, true))
#endif
	{
#if __OUTPUT_FOCUS_AREA
		const Vector3 vDebugPoint = vPointOnEdge;
		const Vector3 vLOSA = vTestLosUp_Start;
		const Vector3 vLOSB = vTestLosUp_End;
		if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
		{
			Displayf("FOCUS: CheckForLowWallCoverPoint [Check9] [%.2f, %.2f, %.2f] LOS [%.2f, %.2f, %.2f] to [%.2f, %.2f, %.2f] failed height clearance check",
				vDebugPoint.x, vDebugPoint.y, vDebugPoint.z, vLOSA.x, vLOSA.y, vLOSA.z, vLOSB.x, vLOSB.y, vLOSB.z );
		}
#endif // __OUTPUT_FOCUS_AREA

		return false;
	}


	//*********************************************************************************************
	// Now find a point a bit higher up above the (now adjusted) vPointOnEdge position.
	// This point will be the start of a line-of-sight test which should be clear of intersections
	// in order for this to be a low-wall which is blocked low down but can be fired over at this
	// height
	//*********************************************************************************************

	static const float fTestHeights[3] = { fwNavGenConfig::Get().m_CoverLowWallMidHeight };
	int iH;

	for(iH=0; iH<1; iH++)
	{
		s32 iNumberFailures = 0;
		Vector3 vPointAboveEdge = vPointOnEdge;
		vPointAboveEdge.z += fTestHeights[iH];

		// Now test for a line of sight
#if __USE_GEOM_CULLER_FOR_LOW_WALL
		bLos = geometry.TestLineOfSight( vPointAboveEdge, vPointAboveEdge + (vApproximated3dNormal * fLowWallTestDistance), COVER_COLLISION_TYPES, bVerticalDefault, iFlagsToTestDefault, bLowWallIgnoreSeeThrough );
#else
		bLos = TestMultipleNavGenLos(
			vPointAboveEdge,
			vPointAboveEdge + (vApproximated3dNormal * fLowWallTestDistance),
			iNumNavGens,
			pNavGens,
			NULL);		
#endif
		if(!bLos)
		{
			// Cant  handle highest point failure
			if( iH==0 )
			{
#if __OUTPUT_FOCUS_AREA
				const Vector3 vDebugPoint = vPointOnEdge;
				const Vector3 vLOSA = vPointAboveEdge;
				const Vector3 vLOSB = vPointAboveEdge + (vApproximated3dNormal * fLowWallTestDistance);
				if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
				{
					Displayf("FOCUS: CheckForLowWallCoverPoint [Check10] [%.2f, %.2f, %.2f] LOS [%.2f, %.2f, %.2f] to [%.2f, %.2f, %.2f] failed highest clearance check",
						vDebugPoint.x, vDebugPoint.y, vDebugPoint.z, vLOSA.x, vLOSA.y, vLOSA.z, vLOSB.x, vLOSB.y, vLOSB.z );
				}
#endif // __OUTPUT_FOCUS_AREA

				return false;
			}
			++iNumberFailures;
		}

		// To left of center point
#if __USE_GEOM_CULLER_FOR_LOW_WALL
		bLos = geometry.TestLineOfSight(
			vPointAboveEdge - (vCoverRight*fPedRadius),
			(vPointAboveEdge - (vCoverRight*fPedRadius)) + (vApproximated3dNormal * fLowWallTestDistance), COVER_COLLISION_TYPES, bVerticalDefault, iFlagsToTestDefault, bLowWallIgnoreSeeThrough );
#else
		bLos = TestMultipleNavGenLos(
			vPointAboveEdge - (vCoverRight*fPedRadius),
			(vPointAboveEdge - (vCoverRight*fPedRadius)) + (vApproximated3dNormal * fLowWallTestDistance),
			iNumNavGens,
			pNavGens,
			NULL
			);
#endif

		if(!bLos)
		{
			++iNumberFailures;

#if __OUTPUT_FOCUS_AREA
			const Vector3 vDebugPoint = vPointOnEdge;
			const Vector3 vLOSA = vPointAboveEdge - (vCoverRight*fPedRadius);
			const Vector3 vLOSB = (vPointAboveEdge - (vCoverRight*fPedRadius)) + (vApproximated3dNormal * fLowWallTestDistance);
			if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
			{
				Displayf("FOCUS: ProcessCornerCoverCandidate [Check11.1] [%.2f, %.2f, %.2f] LOS [%.2f, %.2f, %.2f] to [%.2f, %.2f, %.2f] failed clear check", 
					vDebugPoint.x, vDebugPoint.y, vDebugPoint.z, vLOSA.x, vLOSA.y, vLOSA.z, vLOSB.x, vLOSB.y, vLOSB.z);
			}
#endif // __OUTPUT_FOCUS_AREA
		}

		// To right of center point
#if __USE_GEOM_CULLER_FOR_LOW_WALL
		bLos = geometry.TestLineOfSight(
			vPointAboveEdge + (vCoverRight*fPedRadius),
			(vPointAboveEdge + (vCoverRight*fPedRadius)) + (vApproximated3dNormal * fLowWallTestDistance), COVER_COLLISION_TYPES, bVerticalDefault, iFlagsToTestDefault, bLowWallIgnoreSeeThrough );
#else
		bLos = TestMultipleNavGenLos(
			vPointAboveEdge + (vCoverRight*fPedRadius),
			(vPointAboveEdge + (vCoverRight*fPedRadius)) + (vApproximated3dNormal * fLowWallTestDistance),
			iNumNavGens,
			pNavGens,
			NULL
			);
#endif
		if(!bLos)
		{
			++iNumberFailures;

#if __OUTPUT_FOCUS_AREA
			const Vector3 vDebugPoint = vPointOnEdge;
			const Vector3 vLOSA = vPointAboveEdge + (vCoverRight*fPedRadius);
			const Vector3 vLOSB = (vPointAboveEdge + (vCoverRight*fPedRadius)) + (vApproximated3dNormal * fLowWallTestDistance);
			if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
			{
				Displayf("FOCUS: ProcessCornerCoverCandidate [Check11.2] [%.2f, %.2f, %.2f] LOS [%.2f, %.2f, %.2f] to [%.2f, %.2f, %.2f] failed clear check", 
					vDebugPoint.x, vDebugPoint.y, vDebugPoint.z, vLOSA.x, vLOSA.y, vLOSA.z, vLOSB.x, vLOSB.y, vLOSB.z);
			}
#endif // __OUTPUT_FOCUS_AREA
		}

		// To left of center point at an angle
#if __USE_GEOM_CULLER_FOR_LOW_WALL
		geometry.TestLineOfSight(
			vPointAboveEdge - (vCoverRight*fPedRadius),
			(vPointAboveEdge - (vCoverRight*fPedRadius*4.0f)) + (vApproximated3dNormal * fLowWallTestDistance), COVER_COLLISION_TYPES, bVerticalDefault, iFlagsToTestDefault, bLowWallIgnoreSeeThrough );
#else
		bLos = TestMultipleNavGenLos(
			vPointAboveEdge - (vCoverRight*fPedRadius),
			(vPointAboveEdge - (vCoverRight*fPedRadius*4.0f)) + (vApproximated3dNormal * fLowWallTestDistance),
			iNumNavGens,
			pNavGens,
			NULL);
#endif
		if(!bLos)
		{
			++iNumberFailures;

#if __OUTPUT_FOCUS_AREA
			const Vector3 vDebugPoint = vPointOnEdge;
			const Vector3 vLOSA = vPointAboveEdge - (vCoverRight*fPedRadius);
			const Vector3 vLOSB = (vPointAboveEdge - (vCoverRight*fPedRadius*4.0f)) + (vApproximated3dNormal * fLowWallTestDistance);
			if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
			{
				Displayf("FOCUS: ProcessCornerCoverCandidate [Check11.3] [%.2f, %.2f, %.2f] LOS [%.2f, %.2f, %.2f] to [%.2f, %.2f, %.2f] failed clear check", 
					vDebugPoint.x, vDebugPoint.y, vDebugPoint.z, vLOSA.x, vLOSA.y, vLOSA.z, vLOSB.x, vLOSB.y, vLOSB.z);
			}
#endif // __OUTPUT_FOCUS_AREA
		}

		// To right of center point at an angle
#if __USE_GEOM_CULLER_FOR_LOW_WALL
		geometry.TestLineOfSight(
			vPointAboveEdge + (vCoverRight*fPedRadius),
			(vPointAboveEdge + (vCoverRight*fPedRadius*4.0f)) + (vApproximated3dNormal * fLowWallTestDistance), COVER_COLLISION_TYPES, bVerticalDefault, iFlagsToTestDefault, bLowWallIgnoreSeeThrough );
#else
		bLos = TestMultipleNavGenLos(
			vPointAboveEdge + (vCoverRight*fPedRadius),
			(vPointAboveEdge + (vCoverRight*fPedRadius*4.0f)) + (vApproximated3dNormal * fLowWallTestDistance),
			iNumNavGens,
			pNavGens,
			NULL);
#endif

		if(!bLos)
		{
			++iNumberFailures;

#if __OUTPUT_FOCUS_AREA
			const Vector3 vDebugPoint = vPointOnEdge;
			const Vector3 vLOSA = vPointAboveEdge + (vCoverRight*fPedRadius);
			const Vector3 vLOSB = (vPointAboveEdge + (vCoverRight*fPedRadius*4.0f)) + (vApproximated3dNormal * fLowWallTestDistance);
			if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
			{
				Displayf("FOCUS: ProcessCornerCoverCandidate [Check11.4] [%.2f, %.2f, %.2f] LOS [%.2f, %.2f, %.2f] to [%.2f, %.2f, %.2f] failed clear check", 
					vDebugPoint.x, vDebugPoint.y, vDebugPoint.z, vLOSA.x, vLOSA.y, vLOSA.z, vLOSB.x, vLOSB.y, vLOSB.z);
			}
#endif // __OUTPUT_FOCUS_AREA
		}

		const int numAllowedFailures = 2;
		if( iNumberFailures <= numAllowedFailures )
		{
			// Success!
			continue;
		}

#if __OUTPUT_FOCUS_AREA
		const Vector3 vDebugPoint = vPointOnEdge;
		if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
		{
			Displayf("FOCUS: CheckForLowWallCoverPoint [Check11] [%.2f, %.2f, %.2f] failed %d line of sight checks (allowed %d)", vDebugPoint.x, vDebugPoint.y, vDebugPoint.z, iNumberFailures, numAllowedFailures );
		}
#endif // __OUTPUT_FOCUS_AREA

		// FAILURE!
		return false;
	}

#if __BREAK_ON_COVERPOINT
	if(s_vTestVec.IsClose(vPointOnEdge, vEps))
	{
		bool bBreak=true; bBreak;
	}
#endif

	CNavMeshCoverPoint coverPoint;
	CompressVertex(vPointOnEdge, coverPoint.m_iX, coverPoint.m_iY, coverPoint.m_iZ, pNavMesh->m_pQuadTree->m_Mins, pNavMesh->m_vExtents);
	coverPoint.m_CoverPointFlags.SetCoverType( NAVMESH_COVERPOINT_LOW_WALL );
	coverPoint.m_CoverPointFlags.SetCoverDir(iCoverDir);

	if(!(coverPoints.size() % COVERPOINTS_RESERVE_SIZE))
	{
		coverPoints.reserve(coverPoints.size() + COVERPOINTS_RESERVE_SIZE);
	}

	coverPoints.push_back(coverPoint);

	g_iNumCoverPointsFound++;

#if __OUTPUT_FOCUS_AREA
	const Vector3 vDebugPoint = vPointOnEdge;
	if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
	{
		Displayf("FOCUS: CheckForLowWallCoverPoint [%.2f, %.2f, %.2f] coverpoint added", vDebugPoint.x, vDebugPoint.y, vDebugPoint.z);
	}
#endif // __OUTPUT_FOCUS_AREA

	return true;
}


//	Name	: FindCornerCoverPoints
//	Purpose : Identify potential positions for cover-points where a wall poly offers protection.
//	This is a rewrite of the original 
//
//	i)		Find vertical triangles with 2 vertices (v1 & v2) almost coincident in the XY plane
//	ii)		Construct a matrix for the triangle, located at the base of these 2 points - and with the X component the opposite
//			of the poly's normal, the Y component in the plane of the poly, and the Z component the vector from v1 to v2
//	iii)	Project the lowest point forwards from the plane of the poly a fixed amount (1m?) - to find p1
//	iv)		Cast a (navmesh) ray downwards from p1 to find a navmesh poly underneath, and within some close distance (2m?)
//	v)		Do a series of series of collision raycasts in the X direction, along the face of the poly & beyond to determine whether
//			there is open space beyond the edge of the polygon - which will indicate a corner here.

void
CNavGen::FindCornerCoverPoints(CNavGen * pNavGen, CNavMesh * pNavMesh, int iNumNavGens, CNavGen ** pNavGens, vector<CNavMeshCoverPoint> & coverPoints)
{
	// There must be at least this separation between all tri points for us to process it
	static const float fDimensionsTooSmall2					= 0.05f;	// 0.1f; //2.0f;

	// The triangle must be at least this length
	static const float fMinTriLength2						= 0.01f;	// 0.5f; //4.0f;

	// This defines how close the XY coords of the two candidate points must be
	static const float fCoincidentVertexXY2					= 1.0f * 1.0f;

	// This defines the minimum vertical separation of the vertices along the edge we're considering for cover
	// It will prevent very small detail polys being analysed for providing cover.  (the only possible error
	// condition here, is if a corner is made up of many small triangles - this should be reported as a
	// graphics error in bugstar).
	static const float fMinHeightOfEdgeToConsider			= 0.5f;

	// This is how far in front of the triangle that we generate our 1st test point.
	// Currently we try NUM_ATTEMPTS_TO_FIND_NAVMESH_POLY = 3 attempts
	// starting at 1/3, then 2/3 then 3/3 this distance, preferring closer positions.
	static const float fDistanceFromTriSurfaceOfP1			= 1.5f;

	// These variables define how far above & below P1 we will create the start/endpoints for the navmesh raycast,
	// which is used to determine whether there is a navmesh poly under this potential corner position.
	static const float fDistanceAboveP1ForNavMeshRayTest	= 0.0f; //1.0f;
	static const float fDistanceBelowP1ForNavMeshRayTest	= 3.0f;

	// How far above v2 to begin scanPosition projecting down to find candidate corners
	static const float fInitialVerticalOffsetScanPosAboveV2 = 0.2f;

	// How far below candidate point on navmesh to push S1 for next scan down vertically
	static const float fVerticalScanIncrement = 0.5f;

	// How far below v1 to stop projecting down from scanPosition to find candidate corners
	static const float fVerticalScanLimitAboveV1 = 0.4f;

	// Local variables
	s32 cp;
	s32 iTri;
	CNavGenTri* pCollisionTri;
	TNavMeshPoly * pNavMeshPoly;
	float fDistSrq_01, fDistSrq_12, fDistSrq_20;
	float fDistV1vOther, fDistV2vOther;
	float fDistXY, fDistV1VOtherXY, fDistV2VOtherXY;
	Vector3 v1, v2, vOther;
	Vector3 vMatX, vMatY, vMatZ;
	Vector3 p1;
	Vector3 vStartPosForNavMeshRayTest, vEndPosForNavMeshRayTest, vNavMeshIntersectPos;

	//**********************************************************
	// First find a candidate collision poly
	//**********************************************************
	for(cp=0; cp<pNavGen->m_CollisionTriangles.size(); cp++)
	{
		pCollisionTri = &pNavGen->m_CollisionTriangles[cp];

		// We are looking for triangles which are nearly vertical.
		if(pNavGen->IsWalkable(pCollisionTri))
			continue;

		// We can't hide behind see-through triangles!
		if(pCollisionTri->m_iFlags & NAVGENTRI_SEETHROUGH)
			continue;

		// We don't want to create corner coverpoints on surfaces which came from FIXED objects.
		// These objects will have coverpoints created for them in-game
		if(!bCreateCoverFromFixedObjects && pCollisionTri->m_colPolyData.GetIsFixedObject())
			continue;

		// Don't bother with pissy little triangles
		fDistSrq_01 = (pCollisionTri->m_vPts[0] - pCollisionTri->m_vPts[1]).Mag2();
		if(fDistSrq_01 < fDimensionsTooSmall2)
			continue;

		fDistSrq_12 = (pCollisionTri->m_vPts[1] - pCollisionTri->m_vPts[2]).Mag2();
		if(fDistSrq_12 < fDimensionsTooSmall2)
			continue;

		fDistSrq_20 = (pCollisionTri->m_vPts[2] - pCollisionTri->m_vPts[0]).Mag2();
		if(fDistSrq_20 < fDimensionsTooSmall2)
			continue;

		// Go through all the triangle points, and see if any two are almost identical in the XY plane
		for(iTri = 0; iTri<3; iTri++)
		{
			v1 = pCollisionTri->m_vPts[iTri];
			v2 = pCollisionTri->m_vPts[(iTri+1)%3];
			vOther = pCollisionTri->m_vPts[(iTri+2)%3];

			fDistXY = (v1 - v2).XYMag2();

			// This "fDistXY" distance must smaller than the XY distances of the other edges of the triangle,
			// and also be smaller than the "fCoincidentVertexXY2" value
			fDistV1VOtherXY = (v1 - vOther).XYMag2();
			fDistV2VOtherXY = (v2 - vOther).XYMag2();

			if(fDistXY < fDistV1VOtherXY && fDistXY < fDistV2VOtherXY && fDistXY < fCoincidentVertexXY2)
			{
				// We have two vertices almost the same in the XY plane, but we must order them so that v2 is above v1
				if(v1.z > v2.z)
				{
					Vector3 vTmp = v1;
					v1 = v2;
					v2 = vTmp;
				}

				// The Z separation of these vertices must also be greater than some minimum value.
				// Otherwise we might find ourselves considering small details for cover.
				if(v2.z - v1.z > fMinHeightOfEdgeToConsider)
				{
					break;
				}
			}
		}
		// if iTri got to 3, then we didn't find any suitable (v1,v2) for this triangle..
		if(iTri == 3)
			continue;

		// If v1 is outside of the extents of this navmesh, then don't bother processing - it will be processed
		// in another navmesh's analysis.  (vertices may lie outside if we have not clipped the collision geometry).
		if(v1.x < pNavMesh->m_pQuadTree->m_Mins.x || v1.y < pNavMesh->m_pQuadTree->m_Mins.y ||
			v1.x > pNavMesh->m_pQuadTree->m_Maxs.x || v1.y > pNavMesh->m_pQuadTree->m_Maxs.y)
			continue;

#if __BREAK_ON_COVERPOINT
		if(Eq(v1.x, s_vTestVec.x, vEps.x) && Eq(v1.y, s_vTestVec.y, vEps.y))
		{
			bool bPause = true;	bPause;
		}
#endif // __BREAK_ON_COVERPOINT

		//**************************************************************************
		// Ensure that the length of the triangle is above some minimum.  Given that
		// v1 & v2 are almost coincident in the XY plane, we examine the min dist
		// between vOther and v1/v2.
		//**************************************************************************
		fDistV1vOther = (v1 - vOther).Mag2();
		fDistV2vOther = (v2 - vOther).Mag2();

		if(fDistV1vOther < fMinTriLength2 || fDistV2vOther < fMinTriLength2)
			continue;

		//**********************************************************
		// Find the cover dir, which is basically the opposite of
		// the triangle's surface normal.
		//**********************************************************

		Vector3 vCoverDir = pCollisionTri->m_vNormal * -1.0f;
		vCoverDir.z = 0.0f;

		//*************************************************************************************
		// Let the point scanPosition be a position projected slightly in front of the triangle's surface.
		// We will begin with scanPosition at a height above v2 and project down to find candidate coverpoint
		// positions on the navmesh. As we process candidate points, we will continue to project
		// down, considering candidate points that hit navmesh surfaces, until scanPosition 
		// reaches the height of v1.
		//*************************************************************************************
		
		// The scan position will initially be at the midpoint between the 2 edge vertices, but at a height above the top vertex
		Vector3 scanPosition = (v1 + v2) * 0.5f;
		scanPosition.z = v2.z + fInitialVerticalOffsetScanPosAboveV2;

		bool bActivelyScanningVertically = true;
		while( bActivelyScanningVertically )
		{
			//*********************************************************************************
			// Now we will try to find a navmesh poly some distance below the point 'p1'.
			// We will project the point out from the triangle surface by the normal of the
			// triangle scaled by some amount.  We will try multiple distances
			//*********************************************************************************

			#define NUM_ATTEMPTS_TO_FIND_NAVMESH_POLY	3

			float fDistancesFromTriSurface[NUM_ATTEMPTS_TO_FIND_NAVMESH_POLY] =
			{
				fDistanceFromTriSurfaceOfP1 * 0.33f,
				fDistanceFromTriSurfaceOfP1 * 0.66f,
				fDistanceFromTriSurfaceOfP1
			};

			u32 iNavMeshPoly = NAVMESH_POLY_INDEX_NONE;
			int iAttemptsToFindNavPoly;
			float fDistanceForwardsToTest=0.0f;

			for(iAttemptsToFindNavPoly=0; iAttemptsToFindNavPoly<NUM_ATTEMPTS_TO_FIND_NAVMESH_POLY; iAttemptsToFindNavPoly++)
			{
				// set the distance to test at the current attempt index
				fDistanceForwardsToTest = fDistancesFromTriSurface[iAttemptsToFindNavPoly];

				// P1 will initially be at the scan position
				p1 = scanPosition;

				// But we will then project the point P1 outwards by the triangle normal * some distance
				p1 += (pCollisionTri->m_vNormal * fDistanceForwardsToTest);

				//****************************************************************************
				// Shoot the ray down into the navmesh to determine whether there is any
				// navmesh underneath p1.  We only do this a short distance to avoid creating
				// corner coverpoints for overhanging geometry, etc.
				//****************************************************************************

				vStartPosForNavMeshRayTest = p1;
				vStartPosForNavMeshRayTest.z += fDistanceAboveP1ForNavMeshRayTest;

				vEndPosForNavMeshRayTest = p1;
				vEndPosForNavMeshRayTest.z -= fDistanceBelowP1ForNavMeshRayTest;

				Vector3 vIsectPos;
				iNavMeshPoly = pNavMesh->GetIntersectPoly(vStartPosForNavMeshRayTest, vEndPosForNavMeshRayTest, vIsectPos, true);

				// If we hit a navmesh poly under P1, then we consider this triangle for cover.
				// Otherwise we'll continue and try a point projected out to a different distance.
				if(iNavMeshPoly != NAVMESH_POLY_INDEX_NONE)
				{
					break;
				}

			}// END for(iAttemptsToFindNavPoly)

			// If we didn't find a navmesh poly..
			if(iNavMeshPoly == NAVMESH_POLY_INDEX_NONE)
			{
				// Increment the vertical scan position
				scanPosition.z = MAX( v1.z, scanPosition.z - fVerticalScanIncrement );

				// If the scan position has reached the limit, stop scanning vertically
				if( scanPosition.z <= v1.z + fVerticalScanLimitAboveV1 )
				{
					bActivelyScanningVertically = false;
				}

				// try the next attempt to find nav poly
				continue;
			}

			// Set the handle to the candidate cover position nav mesh poly
			pNavMeshPoly = pNavMesh->GetPoly(iNavMeshPoly);

			// For certain navmesh surfaces we cannot make coverpoints.  eg. water
			if(pNavMeshPoly->TestFlags(NAVMESHPOLY_IS_WATER) || pNavMeshPoly->TestFlags(NAVMESHPOLY_TOO_STEEP_TO_WALK_ON))
			{
				// Increment the vertical scan position
				scanPosition.z = MAX( v1.z, scanPosition.z - fVerticalScanIncrement );

				// If the scan position has reached the limit, stop scanning vertically
				if( scanPosition.z <= v1.z + fVerticalScanLimitAboveV1 )
				{
					bActivelyScanningVertically = false;
				}

				// try the next attempt to find nav poly
				continue;
			}

			// Find the exact point of intersection with the navmesh.  This is where the coverpoint will be made.
			pNavMesh->RayIntersectsPoly(vStartPosForNavMeshRayTest, vEndPosForNavMeshRayTest, pNavMeshPoly, vNavMeshIntersectPos);

#if __PROCESS_FOCUS_AREA_ONLY
			if( vNavMeshIntersectPos.Dist2(s_vDebugProcessFocusPosition) > rage::square(s_fDebugProcessFocusRadius) )
			{
				// try the next attempt to find nav poly
				continue;
			}
#endif // __PROCESS_FOCUS_AREA_ONLY

			// Setup the corner coverpoint candidate data
			CornerCoverCandidateData candidateData;
			candidateData.m_vertexAbove = v2;
			candidateData.m_vertexAbove.z = scanPosition.z;
			candidateData.m_vertexBelow = v1;
			candidateData.m_vertexBelow.z = vNavMeshIntersectPos.z;
			candidateData.m_vertexThirdOfTri = vOther;
			candidateData.m_vCoverDir = vCoverDir;
			candidateData.m_vNavMeshIntersectPos = vNavMeshIntersectPos;
			candidateData.m_fDistanceFromTriToP1 = fDistanceForwardsToTest;
			candidateData.m_fDistanceAboveP1ForRayTest = fDistanceAboveP1ForNavMeshRayTest;
			candidateData.m_fDistanceBelowP1ForRayTest = fDistanceBelowP1ForNavMeshRayTest;

			// Validate the candidate, possibly adding a new coverpoint
			ProcessCornerCoverCandidate(pNavMesh, iNumNavGens, pNavGens, pCollisionTri, candidateData, coverPoints);

			// Update the height of the scan position to be below the candidate point that was found
			// NOTE: Must force scanPosition to decrease in Z, as the intersect pos may hit above and cause infinite looping
			// This was happening when we were projecting from above p1. Currently this is at zero, but leaving this clamp for safety.
			scanPosition.z = MIN( scanPosition.z - fVerticalScanIncrement, vNavMeshIntersectPos.z - fVerticalScanIncrement );
			// NOTE: If we allow scanPosition below v1.z we can get errors accessing out of bounds in the navmesh/navgens data
			scanPosition.z = MAX( scanPosition.z, v1.z );

			// If the scan position has reached the limit, stop scanning vertically
			if( scanPosition.z <= v1.z + fVerticalScanLimitAboveV1 )
			{
				bActivelyScanningVertically = false;
			}

		}// END while(bActivelyScanningVertically)

	} // END for(m_CollisionTriangles)
}


bool
CNavGen::ProcessCornerCoverCandidate(CNavMesh* pNavMesh, int iNumNavGens, CNavGen** pNavGens, CNavGenTri* pCollisionTri, const CornerCoverCandidateData& candidateData, vector<CNavMeshCoverPoint>& out_coverPoints)
{
	// Existing cover checks are performed within this radius of a candidate point
	static const float fCoincidentCoverPointEps				= 1.0f;	// JB : We could try reducing this

	// These variables define the extents of the line of raytests which are done against the collision tris,
	// to determine whether there is a corner past the edge of the triangle being tested.
	static const float fDistBeforeTriEdgeToStartRayTests	= 0.35f;	//0.5f;
	static const float fDistAfterTriEdgeToEndRayTests		= 0.35f;	//0.5f;	// 1.5f; //1.0f;
	// These variables define the depths of the ray tests.
	// There must be this much empty space beyond the edge of the triangle, for it to be deemed as a corner worth providing cover.
	static const float fDistOfClearRayTests					= 2.25f;	//3.0f;	//5.0f;
	// There must be solid cover within this range of the cover position, for there to be enough protection.
	static const float fDistOfCoverRayTests					= 1.0f;

	// This distance is used to shoot a ray from 0.5m above the prospective cover-point position, in the
	// direction that this cover-point provides cover.  The ray is "fIsRoomToUseDistance" in length.
	// This test decides whether there is actually room to use this cover-points (eg. it rules out
	// cover-points which are generated right up next to walls, etc).
	static const float fIsRoomToUseDistance					= 0.2f;	//	0.25f;	0.3f;

	// Although the cover point we generate (vNavMeshIntersectPos) is right on the cover-edge, we need to move it
	// into cover by a small amount so that peds in the game take cover in the correct positions.  This is the
	// distance by which it needs to be moved into cover.
	static const float fFinalDistWithinCoverForCoverPoint		= 0.4f;	// was 0.5f;

	// The cover point we've generated (vNavMeshIntersectPos) is likely to be too far out from the surface of
	// the edge which we've identified for cover.  The correct distance is 0.5m.  We should nudge the point
	// back to this distance, along the reverse of the surface normal.  Since we've already nudged it outwards
	// by 'fDistanceFromTriSurfaceOfP1', we now only need to push it back by the difference between that
	// variable & the variable below..
	static const float fFinalDistanceFromSurfaceOfCoverPoint = 0.3f;		// was 0.5f;

	// The height difference of the ground to the left or right of the cover point mustn't exceed this value.
	// This is to stop cover points being generated which entail the ped stepping out to the left/right into
	// thin air, or up/down a flight of stairs, etc.
	static const float fMaxHeightChangeEitherSideOfCoverPoint	= 0.2f;

	// The bottom/top of the corner cover tests.
	static const float fHeightAboveGroundOfCoverTestBase = 0.5f;
	static const float fHeightAboveGroundOfCoverTestTop = 1.4f; // was 1.5f

	// If the cover is exposed at this height above ground, it is a low corner
	static const float fHeightForLowCorner = 1.7f;

	// How far to probe checking for low corner clearance
	static const float fLengthOfLowCornerProbe = 1.0f;

	// The bottom/top height for testing for low corner coverage/clearance.
	static const float fHeightAboveGroundOfLowCornerBase = 0.3f;
	static const float fHeightAboveGroundOfLowCornerTop = 1.2f;

	// Local variables
	Vector3 vHitPos;
	CNavGenTri * pHitTri= NULL;
	Vector3 vNavMeshMins = pNavMesh->m_pQuadTree->m_Mins;
	Vector3 vNavMeshSize = pNavMesh->m_pQuadTree->m_Maxs - pNavMesh->m_pQuadTree->m_Mins;

	//**************************************************************************************************************
	// Try to test a line-of-sight between halfway between the two edge-points, and the position P1 which we have
	// for the coverpoint.  We want to prevent coverpoints being created on rooftops above corners in the building
	// interior below - which could happen if we didn't establish that there was no LOS here.
	//**************************************************************************************************************

	Vector3 vOnRoofTestFromPos = (candidateData.m_vertexBelow + candidateData.m_vertexAbove) * 0.5f;
	vOnRoofTestFromPos += pCollisionTri->m_vNormal * 0.5f;

	if(!TestMultipleNavGenLos(vOnRoofTestFromPos, candidateData.m_vNavMeshIntersectPos + Vector3(0,0,0.25f), iNumNavGens, pNavGens, COVER_COLLISION_TYPES))
	{
#if __OUTPUT_FOCUS_AREA
		const Vector3 vDebugPoint = candidateData.m_vNavMeshIntersectPos;
		const Vector3 vLOSA = vOnRoofTestFromPos;
		const Vector3 vLOSB = candidateData.m_vNavMeshIntersectPos + Vector3(0.0f,0.0f,0.25f);
		if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
		{
			Displayf("FOCUS: ProcessCornerCoverCandidate [Check0] [%.2f, %.2f, %.2f] LOS [%.2f, %.2f, %.2f] to [%.2f, %.2f, %.2f] failed clear to mid-wall check", 
				vDebugPoint.x, vDebugPoint.y, vDebugPoint.z, vLOSA.x, vLOSA.y, vLOSA.z, vLOSB.x, vLOSB.y, vLOSB.z);
		}
#endif // __OUTPUT_FOCUS_AREA

		return false;
	}

	// NB : This is where unique cover-point test was previously

	//******************************************************************************
	// Figure out whether this triangle edge is going to provide us with a covering
	// wall to the left of us, or to the right.
	// Construct a plane from the tri normal, and the up vector.  If vOther is in
	// front, then it's NAVMESH_COVERPOINT_WALL_TO_LEFT else its RIGHT
	//******************************************************************************

	Vector3 vToOther = candidateData.m_vertexThirdOfTri - candidateData.m_vertexBelow;
	vToOther.NormalizeFast();

	Vector3 vNormal2d = pCollisionTri->m_vNormal;
	vNormal2d.z = 0.0f;
	vNormal2d.NormalizeFast();
	Vector3 vCross = CrossProduct(vNormal2d, Vector3(0.0f, 0.0f, 1.0f));

	float fDot = DotProduct(vToOther, vCross);
	u32 iCoverType = (fDot > 0.0f) ? NAVMESH_COVERPOINT_WALL_TO_LEFT : NAVMESH_COVERPOINT_WALL_TO_RIGHT;

	Vector3 vToOther2d = vToOther;
	vToOther2d.z = 0.0f;
	vToOther2d.NormalizeFast();

	//******************************************************************************
	// We now fire some rays downwards either side of the cover-point, to make sure
	// that the height of the ground to the left & right of the cover-point is not
	// over some threshold difference in height.  If it is, then we cannot make
	// this cover-point as coming out of cover left/right would entail us changing
	// height too much (eg. down a flight of stairs, over an edge)
	//******************************************************************************

	// Get the actual height of the ground underneath the candidate cover-point position :
	// we already have the position of intersection with the navmesh, but this can deviate from the
	// ACTUAL ground height.  If we are going to make a reliable/meaningful comparison, then we'll
	// nead the real ground height to compare against.
	CNavGenTri * pGroundTri;
	float fClosestHitDist;
	Vector3 vPosOnGround;
	u32 iRetFlags = 0;

	bool bHitGround = !ProcessMultipleNavGenLos(candidateData.m_vNavMeshIntersectPos + Vector3(0.0f,0.0f,1.0f), candidateData.m_vNavMeshIntersectPos - Vector3(0.0f,0.0f,1.0f), 5, pNavGens, iRetFlags, COVER_COLLISION_TYPES, &vPosOnGround, fClosestHitDist, pGroundTri);

	if(!bHitGround)
		vPosOnGround = candidateData.m_vNavMeshIntersectPos;


#if __USE_GEOM_CULLER_FOR_CORNERS
	CCulledGeom geometry;
	Vector3 vGeomMins, vGeomMaxs;
	float fGeomExtend;
	fGeomExtend = candidateData.m_fDistanceBelowP1ForRayTest + candidateData.m_fDistanceAboveP1ForRayTest + Max(fDistOfClearRayTests, fDistOfCoverRayTests);
	vGeomMins = candidateData.m_vNavMeshIntersectPos;
	vGeomMaxs = candidateData.m_vNavMeshIntersectPos;
	vGeomMins -= Vector3(fGeomExtend,fGeomExtend,fGeomExtend);
	vGeomMaxs += Vector3(fGeomExtend,fGeomExtend,fGeomExtend);
	InitGeomCuller(geometry, vGeomMins, vGeomMaxs, iNumNavGens, pNavGens);
#endif 


	bool bHeightChangeIsOk = true;

	// Step half a meter to the side of the cover (-vToOther2d) in small increments, firing rays down into the
	// collision geometry.  If at any time the height difference with the actual Z height of the coverpoint
	// is above the threshold 'fMaxHeightChangeEitherSideOfCoverPoint', then this cover-point is not valid.

	float fHeightTestInc = 0.125f;
	float fHeightTestDist = 0.5f;
	for(float fHeightTestOffset=fHeightTestInc; fHeightTestOffset<fHeightTestDist; fHeightTestOffset+=fHeightTestInc)
	{
		Vector3 vHeightTestStart = candidateData.m_vNavMeshIntersectPos;
		vHeightTestStart -= vToOther2d * fHeightTestOffset;
		vHeightTestStart.z += 1.0f;

		Vector3 vHeightTestEnd = candidateData.m_vNavMeshIntersectPos;
		vHeightTestEnd -= vToOther2d * fHeightTestOffset;
		vHeightTestEnd.z -= 2.0f;

		Vector3 vHeightTestHitPos;

#if __USE_GEOM_CULLER_FOR_CORNERS
		bool bGroundLosClear = !geometry.ProcessLineOfSight(vHeightTestStart, vHeightTestEnd, iRetFlags, COVER_COLLISION_TYPES, vHeightTestHitPos, fClosestHitDist, pGroundTri);
#else
		bool bGroundLosClear = ProcessMultipleNavGenLos(vHeightTestStart, vHeightTestEnd, 5, pNavGens, &vHeightTestHitPos, fClosestHitDist, pGroundTri);
#endif

		if(bGroundLosClear)
		{
			bHeightChangeIsOk = false;
			break;
		}

		if(Abs(vHeightTestHitPos.z - vPosOnGround.z) > fMaxHeightChangeEitherSideOfCoverPoint)
		{
			bHeightChangeIsOk = false;
			break;
		}
	}

	if(!bHeightChangeIsOk)
	{
#if __OUTPUT_FOCUS_AREA
		const Vector3 vDebugPoint = candidateData.m_vNavMeshIntersectPos;
		if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
		{
			Displayf("FOCUS: ProcessCornerCoverCandidate [Check1] [%.2f, %.2f, %.2f] failed level ground checks ", vDebugPoint.x, vDebugPoint.y, vDebugPoint.z);
		}
#endif // __OUTPUT_FOCUS_AREA

		return false;
	}

	//*********************************************************************************************************

	// We now need to adjust the coverpoint position slightly, so that it is in the correct placement for
	// peds to take cover.  Currently, the coverpoint is positioned exactly at the edge which we have identified
	// for cover.  We need to move it in the direction of the cover.
	Vector3 vAdjustedFinalNavMeshPos = candidateData.m_vNavMeshIntersectPos + vToOther2d * fFinalDistWithinCoverForCoverPoint;

	// We now need to adjust the coverpoint so that it is closer to the surface which is providing cover.
	// Since we've already pushed it outwards from the surface by 'fDistanceFromTriSurfaceOfP1', we will
	// need to find the difference between that value & the actual final distance we desire.
	float fDistToNudgeBackTowardsSurface = candidateData.m_fDistanceFromTriToP1 - fFinalDistanceFromSurfaceOfCoverPoint;
	vAdjustedFinalNavMeshPos -= (vNormal2d * fDistToNudgeBackTowardsSurface);

	//*********************************************************************************************************

	// Determine whether this corner cover is LOW:
	// Use a probe test at the cover position and low corner height.
	// If the probe does not hit the cover object, then this cover is a low corner

	// First prepare the culled geometry for the test
#if __USE_GEOM_CULLER_FOR_CORNERS
	CCulledGeom lowCornerGeometry;
	float fLowCornerGeomExtend = 1.5f*fHeightForLowCorner;
	Vector3 vLowCornerGeomMins = vAdjustedFinalNavMeshPos;
	Vector3 vLowCornerGeomMaxs = vAdjustedFinalNavMeshPos;
	vLowCornerGeomMins -= Vector3(fLowCornerGeomExtend,fLowCornerGeomExtend,fLowCornerGeomExtend);
	vLowCornerGeomMaxs += Vector3(fLowCornerGeomExtend,fLowCornerGeomExtend,fLowCornerGeomExtend);
	InitGeomCuller(lowCornerGeometry, vLowCornerGeomMins, vLowCornerGeomMaxs, iNumNavGens, pNavGens);
#endif // __USE_GEOM_CULLER_FOR_CORNERS

	// Now perform the collision probe - if nothing is hit, we have low corner conditions
	Vector3 vLowCornerTestStartPos = vAdjustedFinalNavMeshPos;
	vLowCornerTestStartPos.z += fHeightForLowCorner;
	Vector3 vLowCornerTestEndPos = vLowCornerTestStartPos + (candidateData.m_vCoverDir * fLengthOfLowCornerProbe);
#if __USE_GEOM_CULLER_FOR_CORNERS
	bool bIsLowCorner = !lowCornerGeometry.ProcessLineOfSight(vLowCornerTestStartPos, vLowCornerTestEndPos, iRetFlags, COVER_COLLISION_TYPES, vHitPos, fClosestHitDist, pHitTri);
#else
	bool bIsLowCorner = ProcessMultipleNavGenLos(vLowCornerTestStartPos, vLowCornerTestEndPos, iNumNavGens, pNavGens, &vHitPos, fClosestHitDist, pHitTri);
#endif // __USE_GEOM_CULLER_FOR_CORNERS

#if __OUTPUT_FOCUS_AREA
	const Vector3 vLowCornerDebugPoint = candidateData.m_vNavMeshIntersectPos;
	const Vector3 vLowCornerLOSA = vLowCornerTestStartPos;
	const Vector3 vLowCornerLOSB = vLowCornerTestEndPos;
	if( vLowCornerDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
	{
		if( bIsLowCorner )
		{
			Displayf("FOCUS: ProcessCornerCoverCandidate [LowCornerCheck] [%.2f, %.2f, %.2f] clear LOS from [%.2f, %.2f, %.2f] to [%.2f, %.2f, %.2f] LOW CORNER",
				vLowCornerDebugPoint.x, vLowCornerDebugPoint.y, vLowCornerDebugPoint.z, vLowCornerLOSA.x, vLowCornerLOSA.y, vLowCornerLOSA.z, vLowCornerLOSB.x, vLowCornerLOSB.y, vLowCornerLOSB.z);
		}
		else
		{
			Displayf("FOCUS: ProcessCornerCoverCandidate [LowCornerCheck] [%.2f, %.2f, %.2f] LOS hit [%.2f, %.2f, %.2f] to [%.2f, %.2f, %.2f] TALL CORNER",
				vLowCornerDebugPoint.x, vLowCornerDebugPoint.y, vLowCornerDebugPoint.z, vLowCornerLOSA.x, vLowCornerLOSA.y, vLowCornerLOSA.z, vLowCornerLOSB.x, vLowCornerLOSB.y, vLowCornerLOSB.z);
		}
	}
#endif // __OUTPUT_FOCUS_AREA


	//*********************************************************************************************************

	int iPercentLosBlockedOnCoverSide = 0;
	int iPercentLosClearOnNonCoverSide = 0;
	int iActualNumBlockedOnCoverSide = 0;
	int iActualNumClearOnNoCoverSide = 0;

	Vector3 vBaseOfEdge = candidateData.m_vertexBelow;
	vBaseOfEdge.z = candidateData.m_vNavMeshIntersectPos.z;
	//vBaseOfEdge.z = vPosOnGround.z;
	vBaseOfEdge -= candidateData.m_vCoverDir * 0.5f;

#if __BREAK_ON_COVERPOINT
	if(Eq(v1.x, s_vTestVec.x, vEps.x) && Eq(v1.y, s_vTestVec.y, vEps.y))
	{
		bool bPause=true; bPause;
	}
#endif

	const int iNumRows = 3;
	const int iNumColumns = 3;
	const float fHorizontalSpacing = 0.1f; // 0.1f // 0.2f
	const float fOffsetFromCorner = 0.44f; // 0.5f // 0.75f

	float fHeightAboveGroundBase = fHeightAboveGroundOfCoverTestBase;
	float fHeightAboveGroundTop = fHeightAboveGroundOfCoverTestTop;
	if(bIsLowCorner)
	{
		fHeightAboveGroundBase = fHeightAboveGroundOfLowCornerBase;
		fHeightAboveGroundTop = fHeightAboveGroundOfLowCornerTop;
	}

	TestForCoverAtCorner(
		geometry, // NOTE: this culled geo is valid for low and high corner checking
		iNumNavGens,
		pNavGens,
		vBaseOfEdge,
		candidateData.m_vCoverDir,
		vToOther2d,
		fDistOfClearRayTests,
		fDistOfCoverRayTests,
		candidateData.m_vNavMeshIntersectPos.z + fHeightAboveGroundBase,
		candidateData.m_vNavMeshIntersectPos.z + fHeightAboveGroundTop,
		iNumRows,
		fHorizontalSpacing,
		iNumColumns,
		fOffsetFromCorner,
		iPercentLosBlockedOnCoverSide,
		iPercentLosClearOnNonCoverSide,
		iActualNumBlockedOnCoverSide,
		iActualNumClearOnNoCoverSide
	);

	const int iMinPercentBlockedOnCoverSide = 90;
	const int iMinPercentClearOnNonCoverSide = 65;	// was 75;

	const bool bSufficientlyCoveredAndClear = iPercentLosBlockedOnCoverSide >= iMinPercentBlockedOnCoverSide &&	iPercentLosClearOnNonCoverSide >= iMinPercentClearOnNonCoverSide;

	if( !bSufficientlyCoveredAndClear )
	{
#if __OUTPUT_FOCUS_AREA
		const Vector3 vDebugPoint = candidateData.m_vNavMeshIntersectPos;
		if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
		{
			Displayf("FOCUS: ProcessCornerCoverCandidate [Check2] [%.2f, %.2f, %.2f] failed covered and clear checks ", vDebugPoint.x, vDebugPoint.y, vDebugPoint.z);
		}
#endif // __OUTPUT_FOCUS_AREA

		return false;
	}

	//*******************************************************************************************
	// Perform a line-of-sight test from the calculated final cover-point position in the
	// direction of the cover to ascertain whether this cover-point has room for a hiding ped.
	// This will rule out points which are too close to a wall, etc. such that a ped cannot fit.
	//*******************************************************************************************

	const float fCanBeUsedProbeHeight = 0.5f;
	Vector3 vCanBeUsedLosStartPos = vAdjustedFinalNavMeshPos;
	vCanBeUsedLosStartPos.z += fCanBeUsedProbeHeight;
	Vector3 vCanBeUsedLosEndPos = vCanBeUsedLosStartPos + (vToOther2d * fIsRoomToUseDistance);

#if __USE_GEOM_CULLER_FOR_CORNERS
	bool bCanBeUsedLos = !geometry.ProcessLineOfSight(vCanBeUsedLosStartPos, vCanBeUsedLosEndPos, iRetFlags, COVER_COLLISION_TYPES, vHitPos, fClosestHitDist, pHitTri);
#else
	bool bCanBeUsedLos = ProcessMultipleNavGenLos(vCanBeUsedLosStartPos, vCanBeUsedLosEndPos, iNumNavGens, pNavGens, &vHitPos, fClosestHitDist, pHitTri);
#endif
	if(!bCanBeUsedLos)
	{
#if __OUTPUT_FOCUS_AREA
		const Vector3 vDebugPoint = vAdjustedFinalNavMeshPos;
		const Vector3 vLOSA = vCanBeUsedLosStartPos;
		const Vector3 vLOSB = vCanBeUsedLosEndPos;
		if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
		{
			Displayf("FOCUS: ProcessCornerCoverCandidate [Check3] [%.2f, %.2f, %.2f] LOS [%.2f, %.2f, %.2f] to [%.2f, %.2f, %.2f] failed ped can fit check", 
				vDebugPoint.x, vDebugPoint.y, vDebugPoint.z, vLOSA.x, vLOSA.y, vLOSA.z, vLOSB.x, vLOSB.y, vLOSB.z);
		}
#endif // __OUTPUT_FOCUS_AREA

		return false;
	}

#if __BREAK_ON_COVERPOINT
	if(Eq(vAdjustedFinalNavMeshPos.x, s_vTestVec.x, vEps.x) && Eq(vAdjustedFinalNavMeshPos.y, s_vTestVec.y, vEps.y))
	{
		bool bPause=true; bPause;
	}
#endif // __BREAK_ON_COVERPOINT

	//*******************************************************************************************
	// Perform a few line-of-sight tests from the calculated final cover-point position in the
	// direction out from the cover to check for clearance in the vantage position area.
	// This will rule out points which are obstructed for when the ped wants to pop out and fire.
	//*******************************************************************************************

	// we know that the distance from vAdjustedFinalNavMeshPos to the cover object surface is fFinalDistanceFromSurfaceOfCoverPoint
	// so we will perform a series of probes from the hiding position out towards the vantage position
	const float fVantageProbeHeight = 0.5f;
	const float fVantageProbeLength = 1.2f;
	const float fVantageProbeDistToSurfaceIncrement = 0.15f;
	const float fVantageProbeDistToSurfaceLimit = 0.5f;

	// First prepare the culled geometry for the test
#if __USE_GEOM_CULLER_FOR_CORNERS
	CCulledGeom vantageGeometry;
	float fVantageGeomExtend = 1.5f * Max(fVantageProbeLength, fVantageProbeDistToSurfaceLimit);
	Vector3 vVantageGeomMins = vAdjustedFinalNavMeshPos;
	Vector3 vVantageGeomMaxs = vAdjustedFinalNavMeshPos;
	vVantageGeomMins -= Vector3(fVantageGeomExtend,fVantageGeomExtend,fVantageGeomExtend);
	vVantageGeomMaxs += Vector3(fVantageGeomExtend,fVantageGeomExtend,fVantageGeomExtend);
	InitGeomCuller(vantageGeometry, vVantageGeomMins, vVantageGeomMaxs, iNumNavGens, pNavGens);
#endif // __USE_GEOM_CULLER_FOR_CORNERS

	// Initialize the test distance from cover surface
	float fCurrentTestDistToSurface = fVantageProbeDistToSurfaceIncrement;

	// Initialize the LOS test start position
	Vector3 vVantageLOSStartPos = vAdjustedFinalNavMeshPos - (vNormal2d * (fFinalDistanceFromSurfaceOfCoverPoint));
	vVantageLOSStartPos += (vNormal2d * fVantageProbeDistToSurfaceIncrement);
	vVantageLOSStartPos.z += fVantageProbeHeight;

	// Initialize the LOS test end position
	Vector3 vVantageLOSEndPos = vVantageLOSStartPos - (vToOther2d * fVantageProbeLength);

	// Perform the series of LOS tests
	bool bVantageLOS = true;
	while( bVantageLOS && fCurrentTestDistToSurface < fVantageProbeDistToSurfaceLimit )
	{
#if __USE_GEOM_CULLER_FOR_CORNERS
		bVantageLOS = !vantageGeometry.ProcessLineOfSight(vVantageLOSStartPos, vVantageLOSEndPos, iRetFlags, COVER_COLLISION_TYPES, vHitPos, fClosestHitDist, pHitTri);
#else
		bVantageLOS = ProcessMultipleNavGenLos(vVantageLOSStartPos, vVantageLOSEndPos, iNumNavGens, pNavGens, &vHitPos, fClosestHitDist, pHitTri);
#endif
		// If the test didn't hit anything
		if( bVantageLOS )
		{
			// increment the test distance from the cover object surface
			fCurrentTestDistToSurface += fVantageProbeDistToSurfaceIncrement;

			// update the vantage LOS test positions
			vVantageLOSStartPos += (vNormal2d * fVantageProbeDistToSurfaceIncrement);
			vVantageLOSEndPos = vVantageLOSStartPos - (vToOther2d * fVantageProbeLength);
		}
	} // end while()
		 
	// If the vantage tests did hit something
	if(!bVantageLOS)
	{
#if __OUTPUT_FOCUS_AREA
		const Vector3 vDebugPoint = vAdjustedFinalNavMeshPos;
		const Vector3 vLOSA = vVantageLOSStartPos;
		const Vector3 vLOSB = vVantageLOSEndPos;
		if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
		{
			Displayf("FOCUS: ProcessCornerCoverCandidate [Check3] [%.2f, %.2f, %.2f] LOS [%.2f, %.2f, %.2f] to [%.2f, %.2f, %.2f] failed vantage point check", 
				vDebugPoint.x, vDebugPoint.y, vDebugPoint.z, vLOSA.x, vLOSA.y, vLOSA.z, vLOSB.x, vLOSB.y, vLOSB.z);
		}
#endif // __OUTPUT_FOCUS_AREA

		return false;
	}

#if __BREAK_ON_COVERPOINT
	if(Eq(vAdjustedFinalNavMeshPos.x, s_vTestVec.x, vEps.x) && Eq(vAdjustedFinalNavMeshPos.y, s_vTestVec.y, vEps.y))
	{
		bool bPause=true; bPause;
	}
#endif // __BREAK_ON_COVERPOINT

	//*******************************************************************************************
	// Make sure that we don't already have a cover point in this direction, and facing in the
	// direction opposite to the surface normal of the triangle.
	//*******************************************************************************************

	float fRatioClearToBlocked = ((float)iActualNumClearOnNoCoverSide) / ((float)iActualNumBlockedOnCoverSide);

	u8 iCoverDir = CoverDirFromVector3(candidateData.m_vCoverDir);
	bool bAlreadyExists = DoesCoverPointAlreadyExist(out_coverPoints, vAdjustedFinalNavMeshPos, &iCoverDir, iCoverType, vNavMeshMins, vNavMeshSize, fCoincidentCoverPointEps, fRatioClearToBlocked);
	if(bAlreadyExists)
	{
#if __OUTPUT_FOCUS_AREA
		const Vector3 vDebugPoint = vAdjustedFinalNavMeshPos;
		if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
		{
			Displayf("FOCUS: ProcessCornerCoverCandidate [Check4] [%.2f, %.2f, %.2f] rejected as duplicate", vDebugPoint.x, vDebugPoint.y, vDebugPoint.z);
		}
#endif // __OUTPUT_FOCUS_AREA

		return false;
	}


	//*******************************************************************************************
	// It is finally proven to be okay to add this position as a coverpoint!
	//*******************************************************************************************

	// Initialize the navmesh coverpoint data and append it to the list
	CNavMeshCoverPoint coverPoint;
	CompressVertex(vAdjustedFinalNavMeshPos, coverPoint.m_iX, coverPoint.m_iY, coverPoint.m_iZ, vNavMeshMins, vNavMeshSize);
	coverPoint.m_CoverPointFlags.SetCoverDir(iCoverDir);
	coverPoint.m_fRatioClearToBlocked = fRatioClearToBlocked;
	if(!bIsLowCorner)
	{
		coverPoint.m_CoverPointFlags.SetCoverType(iCoverType);
	}
	else // bIsLowCorner is true
	{ 
		if( iCoverType == NAVMESH_COVERPOINT_WALL_TO_LEFT )
		{
			coverPoint.m_CoverPointFlags.SetCoverType( NAVMESH_COVERPOINT_LOW_WALL_TO_LEFT );
		}
		else if( iCoverType == NAVMESH_COVERPOINT_WALL_TO_RIGHT )
		{
			coverPoint.m_CoverPointFlags.SetCoverType( NAVMESH_COVERPOINT_LOW_WALL_TO_RIGHT );
		}
		else
		{
			Assert(0);
		}
	}	

	if(!(out_coverPoints.size() % COVERPOINTS_RESERVE_SIZE))
	{
		out_coverPoints.reserve(out_coverPoints.size() + COVERPOINTS_RESERVE_SIZE);
	}

	out_coverPoints.push_back(coverPoint);

	g_iNumCoverPointsFound++;

#if __OUTPUT_FOCUS_AREA
	const Vector3 vDebugPoint = vAdjustedFinalNavMeshPos;
	if( vDebugPoint.Dist2(s_vDebugOutputFocusPosition) <= rage::square(s_fDebugOutputFocusRadius) )
	{
		Displayf("FOCUS: ProcessCornerCoverCandidate [%.2f, %.2f, %.2f] coverpoint ADDED ", vDebugPoint.x, vDebugPoint.y, vDebugPoint.z);
	}
#endif // __OUTPUT_FOCUS_AREA

	// report success
	return true;
}

#define CORNER_COVER_MAX_NUM_RAYS 64
Vector3 g_vCornerCoverLeft[CORNER_COVER_MAX_NUM_RAYS];
Vector3 g_vCornerCoverRight[CORNER_COVER_MAX_NUM_RAYS];

void
CNavGen::TestForCoverAtCorner(
	CCulledGeom & geometry,
	const int UNUSED_PARAM(iNumNavGens),
	CNavGen ** UNUSED_PARAM(ppNavGens),
	const Vector3 & vPositionOfCorner,						// Test probes will be done either side of this corner.  The z position should be at the base of the corner, close to floor.
	const Vector3 & vCoverDir,								// This is the unit vector direction of the cover
	const Vector3 & vTangentToRays,							// This always points towards the side of the corner which is in cover
	const float fClearTestRayLength,						// This is the length of the raytests for clear line of sight
	const float fCoveredTestRayLength,						// This is the length of the raytests for sufficient coverage
	const float fLowZ,										// Height of the bottom-most row of linetests
	const float fHighZ,										// Height of the top-most row of linetests
	const int iNumRows,										// Number of rows (vertical)
	const float fHorizontalSpacing,							// Spacing between columns (horizontal)
	const int iNumColumns,									// The number of columns
	const float fOffsetFromCorner,							// How far to the left & right of the corner that the origin of the raytests should be done
	int & iOut_PercentageLosTestsBlockedOnCoverSide,		// Output percentages.  The calling function will want both of these to be...
	int & iOut_PercentageLosTestsClearOnNoCoverSide,		// ...sufficiently high percentages to imply that cover exists at this edge.
	int & iOutActualNumBlockedOnCoverSide,
	int & iOutActualNumClearOnNoCoverSide)
{
	Assert(fOffsetFromCorner > fHorizontalSpacing);
	ASSERT(iNumRows > 1 );

	// When checking that an LOS exists from the cover-pos to the start of the raytest, we also do a 2nd LOS a bit
	// further in to make sure no obstacle is going to block a ped's ability to move out past the corner here
	static const float fDistTowardsCoverForSecondTest	= 0.3f;

	Vector3 vHitPos;

	float fVertInc = (fHighZ - fLowZ) / (float)(iNumRows - 1);
	float fHorizExtent = fHorizontalSpacing * (float)iNumColumns;
	float fHalfHorizExtent = fHorizExtent * 0.5f;
	Assert(fHalfHorizExtent < fOffsetFromCorner);

	Vector3 vCoverOrigin = vPositionOfCorner;
	vCoverOrigin.z += 0.25f;

	Vector3 vPos, vEndPos;
	Vector3 vOffsetTowardsCorner = vCoverDir * fDistTowardsCoverForSecondTest;

	Vector3 vClearTestRay = vCoverDir * fClearTestRayLength;
	Vector3 vCoveredTestRay = vCoverDir * fCoveredTestRayLength;

	int iTotalNumTestsInCover = 0;
	int iNumLosBlockedInCover = 0;

	int iTotalNumTestsOutOfCover = 0;
	int iNumLosClearOutOfCover = 0;

	iOutActualNumBlockedOnCoverSide = 0;
	iOutActualNumClearOnNoCoverSide = 0;

	// Set up the tests for the side supposed to be "in cover"
	for(int iRow=0; iRow<iNumRows; iRow++)
	{
		vPos = vPositionOfCorner;
		vPos.z = fLowZ;
		vPos += vTangentToRays * (fOffsetFromCorner - fHalfHorizExtent);
		vPos.z += fVertInc * (float)iRow;

		for(int iCol=0; iCol<iNumColumns; iCol++)
		{
			vEndPos = vPos + vCoveredTestRay;

#if __USE_GEOM_CULLER_FOR_CORNERS
			if(geometry.TestLineOfSight(vCoverOrigin, vPos, COVER_COLLISION_TYPES))
#else
			if(TestMultipleNavGenLos(vCoverOrigin, vPos, iNumNavGens, ppNavGens, &vHitPos, false))
#endif
			{

#if __USE_GEOM_CULLER_FOR_CORNERS
				if(geometry.TestLineOfSight(vCoverOrigin+vOffsetTowardsCorner, vPos+vOffsetTowardsCorner, COVER_COLLISION_TYPES))
#else
				if(TestMultipleNavGenLos(vCoverOrigin+vOffsetTowardsCorner, vPos+vOffsetTowardsCorner, iNumNavGens, ppNavGens, &vHitPos, false))
#endif
				{
					// Ignore see-through polys so that we won't get a high percentage of blocked los for hiding behind them
					// Same goes for fixed polys - we can create cover on fixed objects in the game
#if __USE_GEOM_CULLER_FOR_CORNERS
					bool bLos = geometry.TestLineOfSight(vPos, vEndPos, COVER_COLLISION_TYPES, false, 0, true, !bCreateCoverFromFixedObjects);
#else
					bool bLos = TestMultipleNavGenLos(vPos, vEndPos, iNumNavGens, ppNavGens, &vHitPos, false, true, !bCreateCoverFromFixedObjects);
#endif
					if(!bLos)
						iNumLosBlockedInCover++;
				}
			}

			iTotalNumTestsInCover++;
			vPos += vTangentToRays * fHorizontalSpacing;
		}
	}

	// Set up the tests for the side supposed to be "out of cover"
	for(int iRow=0; iRow<iNumRows; iRow++)
	{
		vPos = vPositionOfCorner;
		vPos.z = fLowZ;
		vPos -= vTangentToRays * (fOffsetFromCorner - fHalfHorizExtent);
		vPos.z += fVertInc * (float)iRow;

		for(int iCol=0; iCol<iNumColumns; iCol++)
		{
			vEndPos = vPos + vClearTestRay;

#if __USE_GEOM_CULLER_FOR_CORNERS
			if(geometry.TestLineOfSight(vCoverOrigin, vPos, COVER_COLLISION_TYPES))
#else
			if(TestMultipleNavGenLos(vCoverOrigin, vPos, iNumNavGens, ppNavGens, &vHitPos, false))
#endif
			{
#if __USE_GEOM_CULLER_FOR_CORNERS
				if(geometry.TestLineOfSight(vCoverOrigin+vOffsetTowardsCorner, vPos+vOffsetTowardsCorner, COVER_COLLISION_TYPES))
#else
				if(TestMultipleNavGenLos(vCoverOrigin+vOffsetTowardsCorner, vPos+vOffsetTowardsCorner, iNumNavGens, ppNavGens, &vHitPos, false))
#endif
				{

#if __USE_GEOM_CULLER_FOR_CORNERS
					bool bLos = geometry.TestLineOfSight(vPos, vEndPos, COVER_COLLISION_TYPES);
#else
					bool bLos = TestMultipleNavGenLos(vPos, vEndPos, iNumNavGens, ppNavGens, &vHitPos, false);
#endif
					if(bLos)
						iNumLosClearOutOfCover++;
				}
			}

			iTotalNumTestsOutOfCover++;
			vPos -= vTangentToRays * fHorizontalSpacing;
		}
	}

	// Now work out the percentage of LOS tests which hit cover on the "cover side"
	iOut_PercentageLosTestsBlockedOnCoverSide = (int) ((((float)iNumLosBlockedInCover) / ((float)iTotalNumTestsInCover)) * 100.0f);

	// Work out the percentage of LOS tests which hit empty space on the "not in cover side"
	iOut_PercentageLosTestsClearOnNoCoverSide = (int) ((((float)iNumLosClearOutOfCover) / ((float)iTotalNumTestsOutOfCover)) * 100.0f);

	iOutActualNumBlockedOnCoverSide = iNumLosBlockedInCover;
	iOutActualNumClearOnNoCoverSide = iNumLosClearOutOfCover;
}





//********************************************************************************
//	LinkUpCoverPoints
//	This function takes a central navmesh, and 4 adjacent navmeshes (which may
//	be NULL if they don't exist).  It then visits all the coverpoints in the
//	central navmesh, and attempts to find nearby coverpoints which can be moved
//	to in a straight line whilst staying in cover.
//	To do this we need two extract the coverpoints into a list (since they are
//	stored in the navmeshes' quad-trees).  Eventually we may need to load & pass in
//	collision geometry (in the form of CNavGen instances) to this function - but
//	for now we will just the "TAdjPoly::m_bEdgeProvidesCover" member of each
//	navmesh poly's edge to determine whether we are in cover or not.
//	To determine whether one coverpoint may link to another, we'll use the
//	CPathServer TestNavMeshLineOfSight() functions.
//********************************************************************************

u32 g_iIndexOfCoverPointWithinNavMesh = 0;
CNavMesh * g_pAdjacentNavMeshes[4];
bool g_bLinkLowWallCoverPoints = true;
int g_iNumLinkCoverNavGens = 0;
CNavGen ** g_ppLinkCoverNavGens = NULL;

#define NUM_NAVMESHES_IN_X		60
#define NUM_NAVMESHES_IN_Y		60

int
CNavGen::GetCodeForLinkedToNavMesh(CNavMesh * pLinkFromNavMesh, CNavMesh * pLinkToNavMesh)
{
	Assert(pLinkFromNavMesh && pLinkToNavMesh);

	s32 iFromIndex = pLinkFromNavMesh->m_iIndexOfMesh;
	s32 iToIndex = pLinkToNavMesh->m_iIndexOfMesh;

	// Same
	if(iFromIndex == iToIndex)
		return 1;
	// -X
	if(iToIndex == iFromIndex-1)
		return 5;
	// +X
	else if(iToIndex == iFromIndex+1)
		return 4;
	// -Y
	if(iToIndex == iFromIndex - NUM_NAVMESHES_IN_X)
		return 3;
	// +Y
	else if(iToIndex == iFromIndex + NUM_NAVMESHES_IN_X)
		return 2;

	Assert(0);
	return 0;
}



void
CNavGen::ExtractCoverPointsIntoList(CNavMesh * pNavMesh, CNavMeshQuadTree * pTree, vector<CNavMeshCoverPoint*> & coverPoints, bool bIgnoreLockedForLinking)
{
	Assert(pTree);
	if(pTree->m_pLeafData)
	{
		for(int c=0; c<pTree->m_pLeafData->m_iNumCoverPoints; c++)
		{
			CNavMeshCoverPoint * pCP = &pTree->m_pLeafData->m_CoverPoints[c];
			if(pCP->m_bRemoveMe)
				continue;

			pCP->m_pOwningNavMesh = pNavMesh;
			pCP->m_iCoverPointIndexWithinNavMesh = g_iIndexOfCoverPointWithinNavMesh++;
			coverPoints.push_back(pCP);
		}
		return;
	}
	for(int c=0; c<4; c++)
	{
		ExtractCoverPointsIntoList(pNavMesh, pTree->m_pChildren[c], coverPoints, bIgnoreLockedForLinking);
	}
}

int gINumCvrPts=0;

int
CNavGen::CountCoverPoints(CNavMesh * pNavMesh)
{
	gINumCvrPts=0;
	CountCoverPointsR(pNavMesh->m_pQuadTree);
	return gINumCvrPts;
}

void
CNavGen::CountCoverPointsR(CNavMeshQuadTree * pTree)
{
	if(pTree->m_pLeafData)
	{
		gINumCvrPts += pTree->m_pLeafData->m_iNumCoverPoints;
		return;
	}
	for(int c=0; c<4; c++)
	{
		CountCoverPointsR(pTree->m_pChildren[c]);
	}
}

void
CNavGen::RecalculateCoverPointIndices(CNavMesh * pNavMesh, CNavMeshQuadTree * pTree, s32 &iStartIndex )
{
	Assert(pTree);
	if(pTree->m_pLeafData)
	{
		for(int c=0; c<pTree->m_pLeafData->m_iNumCoverPoints; c++)
		{
			CNavMeshCoverPoint * pCP = &pTree->m_pLeafData->m_CoverPoints[c];
			if( !pCP->m_bRemoveMe )
			{
				pCP->m_pOwningNavMesh = pNavMesh;
				pCP->m_iCoverPointIndexWithinNavMesh = iStartIndex++;
			}
			else
			{
				//ASSERT(!pCP->m_pLeftLinkedCoverPoint && !pCP->m_pRightLinkedCoverPoint );
			}
		}
		return;
	}
	for(int c=0; c<4; c++)
	{
		RecalculateCoverPointIndices(pNavMesh, pTree->m_pChildren[c], iStartIndex);
	}
}
/*
void
CNavGen::ReLinkCoverPoints(CNavMesh * pNavMesh, CNavMeshQuadTree * pTree, s32 &iStartIndex )
{
	Assert(pTree);
	if(pTree->m_pLeafData)
	{
		for(int c=0; c<pTree->m_pLeafData->m_iNumCoverPoints; c++)
		{
			CNavMeshCoverPoint * pCP = &pTree->m_pLeafData->m_CoverPoints[c];
			if( !pCP->m_bRemoveMe )
			{
				if( pCP->m_pLeftLinkedCoverPoint )
				{
					pCP->m_LeftLinkingCoverPoint.SetAdjacentNavMeshCode( GetCodeForLinkedToNavMesh(pCP->m_pOwningNavMesh, pCP->m_pLeftLinkedCoverPoint->m_pOwningNavMesh) );
					pCP->m_LeftLinkingCoverPoint.SetCoverPointIndex( pCP->m_pLeftLinkedCoverPoint->m_iCoverPointIndexWithinNavMesh );
				}
				if( pCP->m_pRightLinkedCoverPoint)
				{
					pCP->m_RightLinkingCoverPoint.SetAdjacentNavMeshCode( GetCodeForLinkedToNavMesh(pCP->m_pOwningNavMesh, pCP->m_pRightLinkedCoverPoint->m_pOwningNavMesh) );
					pCP->m_RightLinkingCoverPoint.SetCoverPointIndex( pCP->m_pRightLinkedCoverPoint->m_iCoverPointIndexWithinNavMesh );
				}
			}
			else
			{
				ASSERT(!pCP->m_pLeftLinkedCoverPoint && !pCP->m_pRightLinkedCoverPoint );
			}
		}
		return;
	}
	for(int c=0; c<4; c++)
	{
		ReLinkCoverPoints(pNavMesh, pTree->m_pChildren[c], iStartIndex);
	}
}


void
CNavGen::ReCreateQuadTreeCoverPointsAfterLinking(CNavMeshQuadTree * pTree, int & iCountSoFar)
{
	if(pTree->m_pLeafData)
	{
		vector<CNavMeshCoverPoint> coverPts;

		Assert(iCountSoFar < 65536);
		pTree->m_pLeafData->m_iIndexOfFirstCoverPointInList = (u16)iCountSoFar;

		int iNumNew=0;
		int c;
		for(c=0; c<pTree->m_pLeafData->m_iNumCoverPoints; c++)
		{
			CNavMeshCoverPoint * pCP = &pTree->m_pLeafData->m_CoverPoints[c];
			if(!pCP->m_bRemoveMe) iNumNew++;

			// Clear out ptrs, they will be dodgy after this reallocation
			pCP->m_pLeftLinkedCoverPoint = NULL;
			pCP->m_pRightLinkedCoverPoint = NULL;
		}

		CNavMeshCoverPoint * pNewCpts = new CNavMeshCoverPoint[iNumNew];

		int iCount=0;
		for(c=0; c<pTree->m_pLeafData->m_iNumCoverPoints; c++)
		{
			CNavMeshCoverPoint * pCP = &pTree->m_pLeafData->m_CoverPoints[c];
			if(!pCP->m_bRemoveMe)
			{
				memcpy(&pNewCpts[iCount], pCP, sizeof(CNavMeshCoverPoint));

				pNewCpts[iCount].m_LeftLinkingCoverPoint.SetAdjacentNavMeshCode( pCP->m_LeftLinkingCoverPoint.GetAdjacentNavMeshCode() );
				pNewCpts[iCount].m_LeftLinkingCoverPoint.SetCoverPointIndex( pCP->m_LeftLinkingCoverPoint.GetCoverPointIndex() );
				pNewCpts[iCount].m_RightLinkingCoverPoint.SetAdjacentNavMeshCode( pCP->m_RightLinkingCoverPoint.GetAdjacentNavMeshCode() );
				pNewCpts[iCount].m_RightLinkingCoverPoint.SetCoverPointIndex( pCP->m_RightLinkingCoverPoint.GetCoverPointIndex() );

				iCount++;
			}
		}


		delete[] pTree->m_pLeafData->m_CoverPoints;
		pTree->m_pLeafData->m_CoverPoints = pNewCpts;
		pTree->m_pLeafData->m_iNumCoverPoints = (u16)iCount;

		iCountSoFar += iCount;
		return;
	}
	for(int c=0; c<4; c++)
	{
		ReCreateQuadTreeCoverPointsAfterLinking(pTree->m_pChildren[c], iCountSoFar);
	}
}
*/

char * g_pCvrFileHeader = "CVRPTS06";

bool CNavGen::SaveCvrFile(char * pFullFileName, CNavMesh * pNavMesh, u32 iTriFileCRC, u32 iResAreasCRC)
{
	FILE * pFile = fopen(pFullFileName, "wb");
	if(!pFile)
		return false;

	fwrite(g_pCvrFileHeader, 1, 8, pFile);

	fwrite(&iTriFileCRC, 4, 1, pFile);
	fwrite(&iResAreasCRC, 4, 1, pFile);
	

	// Write struct sizes
	s32 iCoverPointFlagsSize = sizeof(TCoverPointFlags);
	fwrite(&iCoverPointFlagsSize, 4, 1, pFile);

	vector<CNavMeshCoverPoint*> coverPoints;
	ExtractCoverPointsIntoList(pNavMesh, pNavMesh->GetQuadTree(), coverPoints, false);

	size_t iNumCvrPts = coverPoints.size();
	fwrite(&iNumCvrPts, 4, 1, pFile);

	for(u32 c=0; c<coverPoints.size(); c++)
	{
		CNavMeshCoverPoint * pCP = coverPoints[c];

		// coverpoint flags
		fwrite(&pCP->m_CoverPointFlags, sizeof(TCoverPointFlags), 1, pFile);

		// compressed u16 XYZs
		fwrite(&pCP->m_iX, 2, 1, pFile);
		fwrite(&pCP->m_iY, 2, 1, pFile);
		fwrite(&pCP->m_iZ, 2, 1, pFile);
	}

	fclose(pFile);
	return true;
}

bool CNavGen::ReadCvrFileCRCs(char * pFullFileName, u32 & iCollisionCRC, u32 & iResAreasCRC)
{
	FILE * pFile = fopen(pFullFileName, "rb");
	if(!pFile) return false;

	char fileHeader[8];

	fread(fileHeader, 1, 8, pFile);
	for(int c=0; c<8; c++)
	{
		if(g_pCvrFileHeader[c] != fileHeader[c])
		{
			Assert(0);
			fclose(pFile);
			return false;
		}
	}

	u32 iTmp;

	fread(&iCollisionCRC, 4, 1, pFile);
	fread(&iResAreasCRC, 4, 1, pFile);
	fread(&iTmp, 4, 1, pFile);

	fclose(pFile);

	return true;
}

bool
CNavGen::LoadCvrFileAndAddCoverPoints(char * pFullFileName, CNavMesh * pNavMesh)
{
	FILE * pFile = fopen(pFullFileName, "rb");
	if(!pFile)
		return false;

	char header[8];
	fread(header, 1, 8, pFile);

	// Check headers match
	for(int i=0; i<8; i++)
	{
		if(header[i] != g_pCvrFileHeader[i])
		{
			fclose(pFile);
			return false;
		}
	}

	u32 iCollisionCRC;
	fread(&iCollisionCRC, 4, 1, pFile);
	u32 iResAreasCRC;
	fread(&iResAreasCRC, 4, 1, pFile);

	// Check that struct sizes haven't changed
	s32 iCoverPointFlagsSize;
	fread(&iCoverPointFlagsSize, 4, 1, pFile);
	AssertMsg(iCoverPointFlagsSize==sizeof(TCoverPointFlags), "ERROR - TCoverPointFlags struct has changed size.  Partial rebuild not possible.");
	if(iCoverPointFlagsSize!=sizeof(TCoverPointFlags))
	{
		fclose(pFile);
		return false;
	}

	s32 iNumCvrPts;
	fread(&iNumCvrPts, 4, 1, pFile);

	for(int c=0; c<iNumCvrPts; c++)
	{
		CNavMeshCoverPoint newCP;

		fread(&newCP.m_CoverPointFlags, sizeof(TCoverPointFlags), 1, pFile);

		fread(&newCP.m_iX, 2, 1, pFile);
		fread(&newCP.m_iY, 2, 1, pFile);
		fread(&newCP.m_iZ, 2, 1, pFile);

		Vector3 vPosition;
		DecompressVertex(vPosition, newCP.m_iX, newCP.m_iY, newCP.m_iZ, pNavMesh->m_pQuadTree->m_Mins, pNavMesh->m_vExtents);

		newCP.m_iCoverPointIndexWithinNavMesh = c;
		newCP.m_pOwningNavMesh = pNavMesh;
		newCP.m_bRemoveMe = false;

		AddCoverPointToNavMeshQuadTree(pNavMesh->GetQuadTree(), newCP, vPosition);
	}

	// Set up the pointers between all *internal* linked coverpoints.
	// 'Lock' coverpoints which are internal to the navmesh & for which linking
	// will serve no purpose.  (ms_fMaxCoverLinkDistance)

	Vector3 vMeshInternalMin = pNavMesh->m_pQuadTree->m_Mins + Vector3(ms_fMaxCoverLinkDistance, ms_fMaxCoverLinkDistance, ms_fMaxCoverLinkDistance);
	Vector3 vMeshInternalMax = pNavMesh->m_pQuadTree->m_Maxs - Vector3(ms_fMaxCoverLinkDistance, ms_fMaxCoverLinkDistance, ms_fMaxCoverLinkDistance);

	vector<CNavMeshCoverPoint*> cpts;
	ExtractCoverPointsIntoList(pNavMesh, pNavMesh->GetQuadTree(), cpts, false);

	for(u32 v=0; v<cpts.size(); v++)
	{
		CNavMeshCoverPoint * cp = cpts[v];

		Vector3 vPosition;
		DecompressVertex(vPosition, cp->m_iX, cp->m_iY, cp->m_iZ, pNavMesh->m_pQuadTree->m_Mins, pNavMesh->m_vExtents);

		static const Vector3 vTestVec1(15.8f, 1699.0f, 0.0f);
		static const Vector3 vTestVec2(15.8f, 1702.1f, 0.0f);

		if((vPosition - vTestVec1).XYMag() < 0.3f) {
			bool bPause=true; bPause;
		}
		if((vPosition - vTestVec2).XYMag() < 0.3f) {
			bool bPause=true; bPause;
		}
	}

	g_iTotalNumCoverPointsCount = 0;
	SetIndexOfFirstCoverPointInQuadtreeLeaves(pNavMesh->m_pQuadTree);


	fclose(pFile);

	return true;
}




bool
CNavGen::CheckForCoverBetweenCoverPoints(const Vector3 & vCoverVertex, const Vector3 & vCoverDir, const Vector3 & vOtherCoverVertex, const Vector3 & vOtherCoverDir)
{
#if __BREAK_ON_COVERPOINT
	static Vector3 vTestVec1(775.5f, -59.75f, 5.1f);
	static Vector3 vTestVec2(764.0f, -62.8f, 5.1f);
	static Vector3 vEps(1.0f, 1.0f, 4.0f);
	if((vTestVec1.IsClose(vCoverVertex, vEps) && vTestVec2.IsClose(vOtherCoverVertex, vEps)) ||
		(vTestVec1.IsClose(vOtherCoverVertex, vEps) && vTestVec2.IsClose(vCoverVertex, vEps)))
	{
		bool bBreak=true; bBreak;
	}

#endif // __BREAK_ON_COVERPOINT

	// Check along the link every multiple of 'fStepSize'
	static const float fStepSize			= ms_fCheckCoverBetweenPointsStepSize;
	static const float fCoverDistTest		= 2.0f;
	static const float fCoverHeight			= 0.5f;
	static const int iMaxNumContiguousFails = 1;

	Vector3 vDiff = vOtherCoverVertex - vCoverVertex;
	float fDist = vDiff.Mag();
	if(fDist < 0.5f)	// so close as to be insignificant
		return true;

	int iNumSteps = (int) (fDist / fStepSize);

	vDiff.Normalize();
	Vector3 vInc = vDiff * fStepSize;

	Vector3 vTestVec = vCoverDir + vOtherCoverDir;
	vTestVec.Normalize();

	Vector3 vCoverTestVec = vTestVec * fCoverDistTest;
	Vector3 vStepPos = vCoverVertex;

	CNavGen & navGen = *g_ppLinkCoverNavGens[0];
	navGen.m_Octree.ResetBatchLOS();

	for(int s=0; s<iNumSteps; s++)
	{
		Vector3 vTestStart = vStepPos;
		vTestStart.z += fCoverHeight;

		Vector3 vTestEnd = vTestStart;
		vTestEnd += vCoverTestVec;

		bool bLos = navGen.m_Octree.TestLineOfSight(vTestStart, vTestEnd, COVER_COLLISION_TYPES);
		if(bLos)
			return false;

		vStepPos += vInc;
	}

	// Do final test
	Vector3 vTestStart = vOtherCoverVertex;
	vTestStart.z += fCoverHeight;

	Vector3 vTestEnd = vOtherCoverVertex;
	vTestEnd += vCoverTestVec;

	bool bLos = navGen.m_Octree.TestLineOfSight(vTestStart, vTestEnd, COVER_COLLISION_TYPES);
	if(bLos)
		return false;

	return true;
}




const float CDirectionalCoverInfo::ms_fTestHeights[DIRECTIONAL_COVER_NUM_HEIGHTS] = { 0.5f, 1.0f, 1.5f };

//********************************************************************************************
// TestCoverDirectionsAtPosition
// Fires rays in 8 directions to find how much cover exists at this position in the navmesh.
//

void CNavGen::TestCoverDirectionsAtPosition(CCulledGeom & geometry, const Vector3 & vPosition, CDirectionalCoverInfo & coverInfo, CNavGen ** UNUSED_PARAM(ppNavGens), const int UNUSED_PARAM(iNumNavGens))
{
	for(int d=0; d<DIRECTIONAL_COVER_NUM_DIRS; d++)
	{
		int iNumHits = 0;
		for(int h=0; h<DIRECTIONAL_COVER_NUM_HEIGHTS; h++)
		{
			const Vector3 vStart = vPosition + Vector3(0.0f, 0.0f, CDirectionalCoverInfo::ms_fTestHeights[h]);
			const Vector3 vEnd = vStart + (TNavMeshPoly::ms_vCoverDirections[d] * DIRECTIONAL_COVER_TEST_DISTANCE);

#if __USE_GEOMCULLER_FOR_DIRECTIONAL_COVER
			if(!geometry.TestLineOfSight(vStart, vEnd, COVER_COLLISION_TYPES))
#else
			if(!TestMultipleNavGenLos(vStart, vEnd, iNumNavGens, ppNavGens))
#endif
			{
				iNumHits++;
			}

		}

		coverInfo.m_fTotals[d] = ((float)iNumHits) / DIRECTIONAL_COVER_NUM_HEIGHTS;
	}
}

u8 CNavGen::TestDirectionalCoverNavMeshForPoly(CCulledGeom & geometry, CNavMesh * pNavMesh, TNavMeshPoly * pPoly, CNavGen ** ppNavGens, const int iNumNavGens)
{
	int p,d;
	u32 v;

	Vector3 vPolyVerts[NAVMESHPOLY_MAX_NUM_VERTICES];
	for(v=0; v<pPoly->GetNumVertices(); v++)
		pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, v), vPolyVerts[v] );

	Vector3 vPointsInPoly[NUM_CLOSE_PTS_IN_POLY];
	int iNumPtsInPoly = CreatePointsInPolyNormal(pNavMesh, pPoly, vPolyVerts, POINTSINPOLY_NORMAL, vPointsInPoly);

	CDirectionalCoverInfo coverInfo[NUM_CLOSE_PTS_IN_POLY];

	for(p=0; p<iNumPtsInPoly; p++)
	{
		TestCoverDirectionsAtPosition(geometry, vPointsInPoly[p], coverInfo[p], ppNavGens, iNumNavGens);
	}

	CDirectionalCoverInfo averageCoverInfo;

	for(p=0; p<iNumPtsInPoly; p++)
	{
		for(d=0; d<DIRECTIONAL_COVER_NUM_DIRS; d++)
		{
			averageCoverInfo.m_fTotals[d] += coverInfo[p].m_fTotals[d];
		}
	}

	u8 iCoverBitfield = 0;

	for(d=0; d<DIRECTIONAL_COVER_NUM_DIRS; d++)
	{
		averageCoverInfo.m_fTotals[d] /= ((float)iNumPtsInPoly);

		if(averageCoverInfo.m_fTotals[d] >= DIRECTIONAL_COVER_THRESHOLD)
		{
			iCoverBitfield |= (1 << d);
		}
	}
	return iCoverBitfield;
}

}	// namespace rage
