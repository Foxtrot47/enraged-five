#ifndef NAVMESHMAKER_H
#define NAVMESHMAKER_H

//********************************************************************************************************
//
//	Filename : NavMeshMaker.h
//	Author : James Broad
//	RockstarNorth (c) 2005
//
//	------------------------------------------------------------------------------------------------------
//	
//	Overview:
//	---------
//
//	This is a command-line utility which handles all the process of generating navigation
//	meshes automatically.  Navigation meshes are an alternative solution for pathfinding,
//	which is traditionally handled by a network of hand-placed or automatically-generated
//	nodes.
//
//	The advantage of navigation meshes, is that they can provide better paths than other
//	techniques and can require little or no manual mark-up.  In our system we hope to be
//	able to include useful cues in the path, to handle jumping & climbing as required.
//
//	The down-side is that this system requires a preprocess to generate the data, and that
//	the path searching is more expensive at run-time that a node-based approach.  It is
//	hoped that use of the 'CPathServer' class in a dedicated thread can offset the run-time
//	costs - and that the smarter AI behaviour as a result of the paths, will make this
//	a viable trade-off for certain in-game tasks...
//
//	------------------------------------------------------------------------------------------------------
//	The procedure for using NavMeshMaker is as follows :
//	------------------------------------------------------------------------------------------------------
//
//	1) Collision geometry is exported into ".tri" files, which are typically 2x2 block
//	of game sectors.  This equals about 3600 files for the whole extents of the world.
//
//	2) NavMeshMaker is called for each of these ".tri" files, specifying the "-sample" option
//	and the name of list file of the base filenames of the exported .tri files.
//	This step does the following :
//
//		i)   samples the height data of the terrain at a fine resolution
//		ii)  triangulates the height samples where possible, creating triangle adjacency data
//		iii) optimises the triangle mesh by a series of edge-collapses
//		iv)  saves out a ".asc" file of the resultant mesh in the same directory.
//
//	3)	NavMeshMaker is called with the "-merge" option, and the list of all the base filenames again.
//	files we have created.  This step loads the ".asc" files and merges any adjacent polygons it
//	can, and saves out the files in ".inv" (independent navmesh) format.
//
//	4)	NavMeshMaker is called with the "-stitch" option, specifying a list file of all
//		the ".3ds" files we have created.  This step  does the following :
//
//		i)   loads all the ".inv" files into memory.
//		ii)  analyses the border triangles on each mesh, and connects them together across
//			each file boundary.
//		iii) resaves out a the ".inv" files which are ready for use by the game's "CPathServer" class.
//
//	5)	NavMeshMaker is called with the "-analyse" option, specifyinf the list file again.
//		This step loads all the ".inv" files (and all the ".oct" files which were generated
//		earlier in the processing steps) and analyses all places where peds can climb, drop, jump
//		and generated cover positions for the AI.  It saves out the *final" ".inv" files at the end.
//
//********************************************************************************************************

#include "fwgeovis/geovis.h"
#include "fwmaths/vector.h"
#include "fwnavgen/config.h"
#include "fwnavgen/toolnavmesh.h"

#include <vector>
using namespace std;
//#include <crtdbg.h>	// for _ASSERTE & mem-leak alerts

#include "system\ipc.h"

namespace rage
{

class CNavGen;
class CNavMeshHolder;
class CSlicePlane;
struct TNavMeshParams;

class CNavMeshMaker
{
public:
	CNavMeshMaker() { }
	~CNavMeshMaker() { }

	static bool ParseParams(int argc, char *argv[]);
	static void ShowUsage();
	static void ReadDatFile();
	static void InitMeshFilenames();
	static void CombineCollisionFiles();
	static bool ReadCmdFile();
	static void ReadTriFileFlags();
	static void MarkWhichFilesToCompile();
	static bool GenerateNavMeshes();
	static bool StitchNavMeshes();
	static bool MergeNavMeshes();
	static bool MergeNavMeshes_Parallel();
	static bool FloodFillNavMeshes();
	static bool DisableBadClimbsInNavMeshes();
	static bool AnalyseNavMeshes(bool bAnalyse, bool bMainMapNavMeshes);
	static bool AnalyseNavMeshes_Parallel(bool bAnalyse, bool bMainMapNavMeshes);
	static bool LoadRequiredCollisionAndUnloadNotRequired(vector<CNavGen*> & navGens);
	static bool CreateDynamicResolutionAreasForNavMesh(CNavGen * pNavGen, s32 iSectorStartX, s32 iSectorStartY);
	static bool CullCoverpointsAlongLinks(vector<CNavGen*> & navGens);
	static bool CompareAndDeleteIdenticalNavFiles(char * pFolder1, char * pFolder2);
	static bool CreateHierarchicalNavData();
	static bool CleanNavMeshes();
	static void OutputText(char * pText);
	static void ClearData();
	static char * GetFileExtensionForMode();
	static TNavMeshParams * InitNavMeshParams(int iSectorX, int iSectorY, char * pNavMeshName = NULL);
	static TNavMeshParams * GetSourceFileByCoords(int iSectorX, int iSectorY);
	static bool CreateMappingFile(char * pFileExtension, char * pFilenameForMappingFile);
	static bool CreateMappingArray(char * pFileExtension, vector<u32> & mappingArray);
	static bool WriteMappingFile(char * pFilenameForMappingFile, vector<u32> & mappingArray);
	static bool IsAerialMeshFileName(const char* pFilename);
	static bool ShouldCopyIntoSubfolder(const char* pFilename);

	static bool JoinMeshesDLC(char * pPathDLC, char * pOutputPathDLC);

	// Divide files into subfolders, so we can build smaller RPFs from each folder.
	// Reason why: Rage RPF files some scale well with number of files (100 or so is ideal)
	// We may have 4000 navmesh files in our RPFs which takes over an hour to add to the RPF
	// NB: Somebody needs to fix this, winzip doesn't take that long even when compressing.

	static void CopyFilesIntoSubfoldersForRpfCreation(char * pFileExtension);

	static void PrintNavMeshInfo();
	static void PrintSingleNavMeshInfo(char * pFilename);
};

#define OUTPUT_TEXT	CNavMeshMaker::OutputText

bool sReplaceStringLastOccurrence(char* strInOut, int strBuffSize, const char* pSearchStr, const char* pReplaceStr);

// The two modes of operation
enum NavMeshMode
{
	// Read collision subsections (0..4) .tri files, and combine each into a single .tri file for the navmesh block
	CombineCollision,
	// Do the initial sampling of the .tri files & create the .asc ascii triangle NavMesh files
	GenerateMeshes,
	// Load the .srf files, and merge triangles into polys and save as .inv NavMesh files
	MergeMeshes,
	// Load the .inv files and compute adjacency between neighbouring NavMeshes
	StitchMeshes,
	// Flood-fills the navmeshes and removes any inaccessible polys
	FloodFillMeshes,
	// Reads list files produces by running the game in "-validateclimbs" mode, and disables the climbs in the .inv files
	DisableBadClimbs,
	// Load .inv files and .tri files & analyze climb-ups, drop-downs and other non-standard poly connections
	AnalyseMainMapMeshes,
	// Load .inv files and .tri files & analyze climb-ups, drop-downs and other non-standard poly connections
	AnalyseNonMapMeshes,
	// Load all .inv files via CPathServer, create a CHierarchicalNavData for each mesh
	CreateHierarchical,
	// Cleans up errors, etc
	CleanMeshes,
	// Compares the contents of two folders, removing "inv" files from the 1st folder which are present & idendical in the 2nd
	DeleteIdenticalNavFiles,
	// Create a mapping file for navmeshes/audmeshes which is used to minimise streaming overheads
	CreateIndexMappingFile,
	// RPF files don't handle more than a few hundred files well within our build pipeline - split into smaller RPFs as a fix
	CopyFilesIntoSubfoldersForRpf,
	// Scans the input folder for .inv files, and displays info about each found (CRC, build-ID, etc)
	DisplayNavMeshInfo,
	// Processes a completed set of DLC navmeshes and ensures that adjacencies will adjoin to the rest of the non DLC map
	JoinDLC
};

// TODO: Combine TNavMeshParams and CNavMeshHolder into one.
struct TNavMeshParams
{
	TNavMeshParams()
	{
		m_bNearCollision = false;
		m_bGenerateSurfaceForThisFile = false;
		m_bTriFileExists = false;
		m_pFileName = NULL;
		m_pFileNameWithoutExtension = NULL;
		m_pAuthoredMeshFileName = NULL;
		m_pNonMapNavMeshName = NULL;
		m_iTriFileFlags = 0;
		m_iStartSectorX = -1;
		m_iStartSectorY = -1;
		m_bUseFakeWaterLevel = false;
		m_fFakeWaterLevel = 0.0f;

		m_iTriFileCRC = 0;
		m_iResAreasCRC = 0;

		m_pSearchName = NULL;
		m_pReplaceName = NULL;
	}
	~TNavMeshParams();
	
	bool m_bNearCollision;
	bool m_bGenerateSurfaceForThisFile;
	bool m_bTriFileExists;
	char * m_pNonMapNavMeshName;
	char * m_pFileName;
	char * m_pFileNameWithoutExtension;
	char * m_pAuthoredMeshFileName;
	u32 m_iTriFileFlags;
	int m_iStartSectorX;
	int m_iStartSectorY;
	float m_fSamplingResolution;
	float m_fMaxHeightChangeUnderTriangleEdge;
	float m_fMinZDistBetweenSamples;
	float m_fLowWallCoverPointStepSize;
	float m_fFakeWaterLevel;
	bool m_bCullBehindWalls;
	bool m_bJitterSamples;
	bool m_bUseFakeWaterLevel;
	vector <CSlicePlane*> m_SlicePlanes;

	u32 m_iTriFileCRC;
	u32 m_iResAreasCRC;

	char * m_pSearchName;
	char * m_pReplaceName;
};	

class CFileMapping
{
public:
	CFileMapping()
	{ 
		m_pFileName = NULL;
		m_iIndex = -1;
	}
	~CFileMapping()
	{
		if(m_pFileName)
			delete(m_pFileName);
	}

	char * m_pFileName;
	s32 m_iIndex;
};

// Which mode this has been invoked as
extern int g_Mode;
// The name of a command file which will be used
extern char * g_CommandFileName;
// Name of a navmesh resolution areas XML file, which we may use to define custom areas of increased sampling resolution
extern char * g_ResolutionAreasFileName;
// Name of a navmesh custom coverpoints XML file, which we may use to define custom coverpoints
extern char * g_CustomCoverpointsFileName;
// Name of a DLC areas XML file, which defines a series of bounding boxes which contain DLC content for this navmesh build
extern char * g_DLCAreasFileName;
// The navmeshes we are processing
extern vector<TNavMeshParams*> g_SourceNavMeshes;

// An input path to be prepended to all input file-names
extern char * g_InputPath;
// An output path to be prepended to all output file-names
extern char * g_OutputPath;
// The resolution at which to sample the world
extern float g_fSampleResolution;
// The number of samples to take across the extent of the navmesh (g_fSampleResolution can be calculated from this)
extern int g_iNumSamples;
// The maximum height change allowed underneath a navmesh triangle edge (to avoid us going up onto low walls, etc).
extern float g_fMaxHeightChangeUnderNavMeshTriEdge;
// If this is true then we are processing non-map navmeshes
extern bool g_bProcessingNonMapNavmeshes;

enum NavMeshAdjacency
{
	PosY, NegY, PosX, NegX
};

class CNavMeshHolder
{
public:
	CNavMeshHolder(void)
	{
		m_pNavMesh = NULL;
		m_pNavMeshNameWithoutExtension = NULL;
		m_pNavMeshName = NULL;
		m_pColFileName = NULL;
		m_pOctreeFileName = NULL;
		m_pSrfFileName = NULL;
		m_pCvrFileName = NULL;
		m_bIsInterior = false;
		m_bRequired = false;
		m_bCanMaybeUseCachedCoverPoints = false;
		m_iTriFileCRC = 0;
		m_iResAreasCRC = 0;
		m_iNavMeshXPos = -1;
		m_iNavMeshYPos = -1;
		m_iNavMeshIndex = NAVMESH_NAVMESH_INDEX_NONE;
		m_pNeighbouringMeshes[0] = m_pNeighbouringMeshes[1] = m_pNeighbouringMeshes[2] = m_pNeighbouringMeshes[3] = NULL;
		m_iNumSamples = 0;
		m_fLowWallCoverPointStepSize = 0.0f;
		m_pParams = NULL;
	}
	~CNavMeshHolder(void)
	{
		free(m_pNavMeshNameWithoutExtension);
		free(m_pNavMeshName);
		free(m_pColFileName);
		free(m_pOctreeFileName);
		free(m_pSrfFileName);
		free(m_pCvrFileName);
	}

	CNavMesh * m_pNavMesh;
	char * m_pNavMeshNameWithoutExtension;		// fully qualified, with path
	char * m_pNavMeshName;		// fully qualified, with path
	char * m_pColFileName;		// fully qualified, with path
	char * m_pOctreeFileName;	// fully qualified, with path
	char * m_pSrfFileName;		// fully qualified, with path
	char * m_pCvrFileName;		// fully qualified, with path
	bool m_bIsInterior;
	bool m_bRequired;
	bool m_bCanMaybeUseCachedCoverPoints;	// If true then we can use the coverpoints we found & saved during the last compile
	u32 m_iTriFileCRC;		// The CRC of the tri file for this navmesh
	u32 m_iResAreasCRC;
	int m_iNavMeshXPos;
	int m_iNavMeshYPos;
	TNavMeshIndex m_iNavMeshIndex;
	Vector3 m_vBlockMin;
	Vector3 m_vBlockMax;
	int m_iNumSamples;
	float m_fLowWallCoverPointStepSize;
	TNavMeshParams * m_pParams;	// Point back to the params for this navmesh

	CNavMesh * m_pNeighbouringMeshes[4];
};

struct TPrevAnalysisCRC
{
	char * m_pNavMeshName;	// fully qualified, with path
	u32 m_iTriFileCRC;
};

struct TBadClimbInfo
{
	Vector3 m_vPosition;
};

// A list of containers which hold the navmeshes in the world
extern vector <CNavMeshHolder*> g_NavMeshes;

// The size of each block in sectors, usually 4x4 or 8x8..
extern int g_SectorBlockSizeX;
extern int g_SectorBlockSizeY;


/*
PURPOSE
This subclass template simply makes sure that it's easy to use fwNavGenConfigManager
with configuration classes specialized for the game.
*/
template<class _CfgData> class fwNavGenConfigManagerDefaults : public fwNavGenConfigManager
{
public:
	// PURPOSE:	Initialize fwNavGenConfigManager, by loading a given configuration file
	//			and resizing all fwPool objects that have been registered.
	// PARAMS:	configFilename		- File name of the configuration file to load,
	//								  or NULL if no file load is desired.
	static void InitClass(const char* configFilename)
	{
		// Should only have one instance.
		Assert(!sm_Instance);

		// Create the instance.
		sm_Instance = rage_new fwNavGenConfigManagerDefaults<_CfgData>;

		// Load the configuration file. Note that this couldn't just be done
		// by constructor of fwNavGenConfigManager, as we need to finish the construction
		// before the call to CreateConfig().
		sm_Instance->Load(configFilename);
	}

protected:
	virtual void fwNavGenConfigManagerDefaults<fwNavGenConfig>::Load(const char* UNUSED_PARAM(pConfigFilename))
	{
		delete m_Config;
		m_Config = CreateConfig();
		m_Config->m_AerialVerticalQuantization = 2.0f;
		m_Config->m_AngleForNavMeshPolyToBeTooSteep = 44.0f;
		m_Config->m_AntialiasEdgesNumPasses = 0;
		m_Config->m_AntialiasPavementEdgesNumPasses = 0;
		m_Config->m_CreateResolutionAreasForPavementTriangles = false;
		m_Config->m_CoverLowFinalDistanceFromSurfaceOfCoverPoint = 0.3f;
		m_Config->m_CoverLowFinalProbeCheckDistanceLimit = 0.50f;
		m_Config->m_CoverLowMaxDistOfFloorBelow = 1.0f;
		m_Config->m_CoverLowPointSeparation = 0.5f;
		m_Config->m_CoverLowWallMidHeight = 1.4f;
		m_Config->m_CoverLowWallObstructionHeight = 0.2f;
		m_Config->m_CoverLowWallObstructionMaxHeight = 0.5f;
		m_Config->m_CoverLowWallPointSpacingAlongEdges = 2.0f;
		m_Config->m_CoverLowWallTestDistance = 1.5f;
		m_Config->m_ExporterMaxZCutoff = 1000.0f;
		m_Config->m_ExporterMinZCutoff = -500.0f;
		m_Config->m_MaxAngleForPolyToBeInNavMesh = 60.0f;
		m_Config->m_MaxAngleForWaterPolyToBeInNavMesh = 75.0f;
		m_Config->m_MaxHeightChangeUnderTriangleEdge = 0.4f;
		m_Config->m_MinimumVerticalDistanceBetweenNodes = 2.0f;
		m_Config->m_PedRadius = 0.35f;
		m_Config->m_SampleResolutionDefault = 1.0f;
		m_Config->m_TestClearHeightInTriangulation = 1.8f;
#if HEIGHTMAP_GENERATOR_TOOL
		m_Config->m_WorldMinX = (float)gv::NAVMESH_BOUNDS_MIN_X;
		m_Config->m_WorldMinY = (float)gv::NAVMESH_BOUNDS_MIN_Y;
#else
		m_Config->m_WorldMinX = -6000.0f;
		m_Config->m_WorldMinY = -6000.0f;
#endif
	}

	virtual fwNavGenConfig *CreateConfig() const
	{	return rage_new _CfgData;	}
};



}	// namespace rage

#endif

