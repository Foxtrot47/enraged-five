//
// fwnavgen/helperfunc.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "helperfunc.h"
#include "math/simplemath.h"
#include "vector/geometry.h"
#include "vector/matrix34.h"

using namespace rage;

//-----------------------------------------------------------------------------

// l0,l1,l2 are the length of each triangle edge
float fwNavToolHelperFuncs::CalcTriArea(float l0, float l1, float l2)
{
	// Calculate the tri area from the edge lengths
	float p = l0 + l1 + l2;
	float a = 0.25f * rage::Sqrtf(p * (p - (2.0f * l0)) * (p - (2.0f * l1)) * (p - (2.0f * l2)));
	return a;
}


float fwNavToolHelperFuncs::CalcTriArea(Vector3 * pPts)
{
	Vector3 e1 = pPts[1] - pPts[0];
	Vector3 e2 = pPts[2] - pPts[1];
	Vector3 e3 = pPts[0] - pPts[2];

	return CalcTriArea(e1.Mag(), e2.Mag(), e3.Mag());
}


bool fwNavToolHelperFuncs::AreVerticesEqual(const Vector3 & v1, const Vector3 & v2, float fEps)
{
	if(rage::IsNearZero(v1.x - v2.x, fEps) && rage::IsNearZero(v1.y - v2.y, fEps) && rage::IsNearZero(v1.z - v2.z, fEps))
	{
		return true;
	}
	return false;
}


void fwNavToolHelperFuncs::GetRotatedMinMax(Vector3 & vMinInOut, Vector3 & vMaxInOut, const Matrix34 & mat)
{
	Vector3 vTmpMin = vMinInOut;
	Vector3 vTmpMax = vMaxInOut;

	vMinInOut = Vector3(FLT_MAX, FLT_MAX, FLT_MAX);
	vMaxInOut = Vector3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	Vector3 vCorners[4] = 
	{
		Vector3(vTmpMin.x, vTmpMin.y, 0),
		Vector3(vTmpMax.x, vTmpMin.y, 0),
		Vector3(vTmpMax.x, vTmpMax.y, 0),
		Vector3(vTmpMin.x, vTmpMax.y, 0)
	};
	Vector3 vRotated;

	for(int v=0; v<4; v++)
	{
		mat.Transform(vCorners[v], vRotated);
		vMinInOut.Min(vMinInOut, vRotated);
		vMaxInOut.Max(vMaxInOut, vRotated);
	}
}


bool fwNavToolHelperFuncs::BoundingBoxesOverlap(const Vector3 & vMin1, const Vector3 & vMax1, const Vector3 & vMin2, const Vector3 & vMax2)
{
	if(vMin1.x > vMax2.x || vMin1.y > vMax2.y || vMin1.z > vMax2.z || vMin2.x > vMax1.x || vMin2.y > vMax1.y || vMin2.z > vMax1.z)
		return false;
	return true;
}

bool fwNavToolHelperFuncs::BoundingBoxesOverlapXY(const Vector3 & vMin1, const Vector3 & vMax1, const Vector3 & vMin2, const Vector3 & vMax2)
{
	if(vMin1.x > vMax2.x || vMin1.y > vMax2.y || vMin2.x > vMax1.x || vMin2.y > vMax1.y)
		return false;
	return true;
}

void fwNavToolHelperFuncs::TransformObjectAABB(const Matrix34& objMtrx,
		const Vector3& vLocalBoundMin, const Vector3& vLocalBoundMax,
		Vector3& aabbMinOut, Vector3& aabbMaxOut)
{
	// Corner points transformed in local space
	Vector3 vCornerPoints[8] =
	{
		Vector3(vLocalBoundMin.x, vLocalBoundMin.y, vLocalBoundMin.z),
		Vector3(vLocalBoundMax.x, vLocalBoundMin.y, vLocalBoundMin.z),
		Vector3(vLocalBoundMin.x, vLocalBoundMax.y, vLocalBoundMin.z),
		Vector3(vLocalBoundMax.x, vLocalBoundMax.y, vLocalBoundMin.z),

		Vector3(vLocalBoundMin.x, vLocalBoundMin.y, vLocalBoundMax.z),
		Vector3(vLocalBoundMax.x, vLocalBoundMin.y, vLocalBoundMax.z),
		Vector3(vLocalBoundMin.x, vLocalBoundMax.y, vLocalBoundMax.z),
		Vector3(vLocalBoundMax.x, vLocalBoundMax.y, vLocalBoundMax.z),
	};

	// Transform corners into worldspace & take min/max
	Vector3 vBoundMin(FLT_MAX, FLT_MAX, FLT_MAX);
	Vector3 vBoundMax(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	for(int c=0; c<8; c++)
	{
		objMtrx.Transform(vCornerPoints[c]);

		vBoundMin.Min(vBoundMin, vCornerPoints[c]);
		vBoundMax.Max(vBoundMax, vCornerPoints[c]);
	}

	aabbMinOut = vBoundMin;
	aabbMaxOut = vBoundMax;
}

//-----------------------------------------------------------------------------

// End of file fwnavgen/helperfunc.cpp
