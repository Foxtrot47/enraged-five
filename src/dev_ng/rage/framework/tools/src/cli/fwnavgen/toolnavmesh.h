#ifndef FWNAVGEN_TOOLNAVMESH_H
#define FWNAVGEN_TOOLNAVMESH_H

#ifdef NAVGEN_TOOL

// If NAVGEN_TOOL is already set, we should be compiling NavGenTest, and we just include
// 'ai/navmesh/navmesh.h' like usual, which will be built in its regular namespace in
// tool configuration.
#include "ai/navmesh/navmesh.h"

#else	// NAVGEN_TOOL

// These #includes are basically what would have normally been included by 'ai/navmesh/navmesh.h'.

#ifdef GTA_ENGINE
#include "navmeshchannel.h"
#else
#define pathAssertf	Assertf
#endif

// Rage includes
#include "atl/array.h"
#include "data/base.h"
#include "data/struct.h"
#include "file/stream.h"
#include "math/amath.h"
#include "paging/base.h"
#include "spatialdata/sphere.h"
#include "system/cache.h"
#include "system/memory.h"
#include "system/performancetimer.h"
#include "vector/color32.h"
#include "vector/matrix34.h"
#include "vector/vector3.h"

namespace rage
{
	class CHierarchicalNode;
	class CNavGen;
	extern const bool g_bEndianSwap;
}

// Before, when this code was in 'GTA_tools/navgen', the .vcproj file disabled warning 4800.
// Now, we do it from this header file.
#pragma warning(disable:4800)

namespace rage
{

// This namespace will include the classes from 'ai/navmesh/navmesh.[h/cpp]', compiled
// in their tool configuration.
namespace fwToolNavMesh
{

// This makes sure that 'navmesh.h' doesn't include other files or forward declare
// any symbols that don't belong in the fwToolNavMesh namespace.
#define COMPILE_TOOL_MESH

// Define these symbols to build the nav mesh code in its tool configuration.
#define NAVGEN_TOOL
#define NAVMESHMAKER

#include "ai/navmesh/navmesh.h"

// Undefine the variables we just set, as they shouldn't be used outside of 'navmesh.[h/cpp]'.
#undef NAVGEN_TOOL
#undef NAVMESHMAKER
#undef COMPILE_TOOL_MESH

}	// namespace fwToolNavMesh

}	// namespace rage

// Make the user who included this header file transparently use the tool version
// of the navmesh classes.
using namespace rage::fwToolNavMesh;

#endif	// NAVGEN_TOOL

#endif	// FWNAVGEN_TOOLNAVMESH_H
