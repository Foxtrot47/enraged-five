//
// fwnavgen/baseexportcollisiongrid.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef FWNAVGEN_BASEEXPORTCOLLISIONGRID_H
#define FWNAVGEN_BASEEXPORTCOLLISIONGRID_H

#include "baseexportcollision.h"
#include "datatypes.h"

namespace rage
{

//-----------------------------------------------------------------------------

class fwExportCollisionGridTool : public fwExportCollisionBaseTool
{
public:
	fwExportCollisionGridTool(fwLevelProcessTool * pTool);

	// -- fwExportCollisionBaseTool interface --

	virtual bool OpenTriFile(const char *pNameToUse);

protected:

	// -- fwExportCollisionGridTool internal interface --

	virtual bool ParseArgumentsAndStartExportCollision();
	virtual void GenerateFileNames(char* triFileNameOut, int maxTriFileNameLen, char* extrasFileNameOut, int maxExtrasFileNameLen) const;
	virtual bool StartExportCollisionForSpecifiedNavMeshes(u32 iStartSectorX, u32 iStartSectorY, u32 iEndSectorX, u32 iEndSectorY);
	virtual bool StartExportCollisionForSpecifiedWorldCoords(float fStartX, float fStartY, float fEndX, float fEndY);
	virtual bool StartExportCollisionForAllNavMeshes();
	virtual bool StartExportCollisionForAllNavMeshesFrom(int iX, int iY);
	virtual bool StartCollisionExport();
	virtual bool StartCollisionExportFrom(int iX, int iY);
	virtual bool StartExportCollisionForNextRegion(int iAreaIndex);

	void CalcCurrentNavMeshExtents();

	bool NextCellOrSubSection();

	virtual void GridCellFinished() {}
	virtual void GridProcessFinished() {}
	virtual void UpdateStreamingPos(const Vector3 &/*pos*/) {}

	// -- fwExportCollisionGridTool data --

	Vector3		m_vCentreOfCurrentNavMesh;

	float		m_fWidthOfNavMesh;
	float		m_fDepthOfNavMesh;

	float		m_fCollisionExportHeight;

	// The sectors within the block which we are exporting
	s32			m_iStartSectorX;
	s32			m_iNumSectorsX;
	s32			m_iStartSectorY;
	s32			m_iNumSectorsY;

	s32			m_iEndSectorX;
	s32			m_iEndSectorY;

	s32			m_iCurrentSectorX;
	s32			m_iCurrentSectorY;

	bool		m_bExporting;

	// A single navmesh is divided into 4 subsections for the purpose of export, so we don't try to request large chunks of game world.
	// The navmeshmaker program combines these 4 smaller tri files into one large file prior to processing.
//	int			m_iNavMeshSubSection;	// obsolete
//	bool		m_bUseSubSections;		// obsolete

	atArray<CDLCArea*> m_DLCAreas;

	s32 m_iCurrentArea;
};

//-----------------------------------------------------------------------------

}	// namespace rage

#endif // FWNAVGEN_BASEEXPORTCOLLISIONGRID_H

// End of file fwnavgen/baseexportcollisiongrid.h
