#ifndef NAVGEN_VOXEL_H
#define NAVGEN_VOXEL_H

#include "vector/vector3.h"
#include "atl/array.h"
#include "NavGen_Types.h"

namespace rage
{

class CNavGenTri;

//---------------------------------------------------------------------------------------------------------
// CLASS: CVoxelMap
// PURPOSE : Voxelisation of collision for one navmesh region, and construction functions using voxel data

class CVoxelMap
{
public:

	static const float ms_fDefaultCellSize;

	//------------------------------------------------------------------------------------------------
	// CLASS : CVoxelMap::CElementRLE
	// PURPOSE : A run-length encoded element of voxel data.
	// Space is not solid, so this defines a surface followed by m_iSpaceAbove elements of free space

	class CElement
	{
	public:

		CElement();
		~CElement();

		// Pointer to the collision surface which this element was created from
		CNavGenTri * m_pSurface;
		// The exact Z height of the collision at the center of this cell
		float m_fIntersectZ;
		// Number of empty voxels above this before the next element
		s32 m_iSpaceAbove;

		CElement * m_pNextAbove;
		//CElement * m_pNextBelow;
	};

	typedef bool (*ForAllVoxelsCB) (CVoxelMap::CElement * pElement, Vector3::Vector3Param vVoxelMin, Vector3::Vector3Param vVoxelMax, void * data);

public:

	CVoxelMap();
	~CVoxelMap();

	bool ConstructFromCollision(const Vector3 & vMin, const Vector3 & vMax, TColTriArray & collisionTriangles);

	void Shutdown();

	void ForAllSolidVoxels(ForAllVoxelsCB callbackFn, void * data=NULL);

protected:

	void InitToSize(const Vector3 & vMin, const Vector3 & vMax);

	CElement * AllocVoxel(const s32 iVx, const s32 iVy, const s32 iVz, CNavGenTri * pTri);

	void VoxelCoordsToWorldCoords(const s32 iVx, const s32 iVy, const s32 iVz, Vector3 & vOutPos) const;
	void WorldCoordsToVoxelCoords(const Vector3 & vPos, s32 & iOutVx, s32 & iOutVy, s32 & iOutVz) const;
	void GetVoxelBounds(const s32 iVx, const s32 iVy, const s32 iVz, Vector3 & vOutMin, Vector3 & vOutMax) const;

	inline bool IsValidVoxelCoord(const s32 iVx, const s32 iVy) const
	{
		return ( iVx >= 0 && iVy >= 0 && iVx < m_iNumCellsX && iVy < m_iNumCellsY);
	}

	CElement * AllocElement() { return rage_new CElement(); }

	void VoxelizeTriangle(CNavGenTri * pTri);

protected:

	Vector3 m_vMin;
	Vector3 m_vMax;
	
	float m_fCellSize;
	float m_fHalfCellSize;
	Vector3 m_vVoxelSize;
	Vector3 m_vHalfVoxelSize;
	Vector3 m_vHalfVoxelSizeXY;

	s32 m_iNumCellsX;
	s32 m_iNumCellsY;
	s32 m_iMaxNumVoxelsZ;

	CElement * m_pBaseCells;
};






}

#endif // NAVGEN_VOXEL_H


