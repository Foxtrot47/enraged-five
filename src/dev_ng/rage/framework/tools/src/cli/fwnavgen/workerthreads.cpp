
//-------------------------------------------------------------------------------
// FILENAME : WorkerThreads.cpp
// PURPOSE : Implementes a means of processing navmesh constructing in parallel

#include "workerthreads.h"
#include "fwnavgen/config.h"
#include "fwnavgen/toolnavmesh.h"
#include "fwnavgen/navgen.h"
#include "fwnavgen/NavMeshMaker.h"
#include "fwnavgen/NavGen_Store.h"

#include "system/threadregistry.h"


using namespace rage;

/*
void SetThreadNameDebugger(const char * pName)
{
	// See "SetThreadName" in the Xenon help for discussion.
	typedef struct tagTHREADNAME_INFO {
		DWORD dwType;     // Must be 0x1000
		LPCSTR szName;    // Pointer to name (in user address space)
		DWORD dwThreadID; // Thread ID (-1 for caller thread)
		DWORD dwFlags;    // Reserved for future use; must be zero
	} THREADNAME_INFO;

	THREADNAME_INFO info;

	info.dwType = 0x1000;
	info.szName = pName;
	info.dwThreadID = (DWORD)-1;
	info.dwFlags = 0;

	__try
	{
		RaiseException( 0x406D1388, 0, sizeof(info)/sizeof(ULONG), (ULONG_PTR*)&info );
	}
	__except( EXCEPTION_CONTINUE_EXECUTION )
	{
	}
}
*/

void CGenerationThreadPool::Init(int iNumThreads)
{
	for(int i=0; i<iNumThreads; i++)
	{
		m_ThreadPool.PushAndGrow( new CGenerationWorkerThread() );
		m_ThreadPool[i]->m_iThreadNum = i;
	}
}
void CGenerationThreadPool::Shutdown()
{
	for(int i=0; i<m_ThreadPool.GetCount(); i++)
	{
		CGenerationWorkerThread * pThread = (CGenerationWorkerThread*) m_ThreadPool[i];
		pThread->Shutdown();
		delete pThread;
	}
}



void CMergeThreadPool::Init(int iNumThreads)
{
	for(int i=0; i<iNumThreads; i++)
	{
		m_ThreadPool.PushAndGrow( new CMergeWorkerThread() );
		m_ThreadPool[i]->m_iThreadNum = i;
	}
}
void CMergeThreadPool::Shutdown()
{
	for(int i=0; i<m_ThreadPool.GetCount(); i++)
	{
		CMergeWorkerThread * pThread = (CMergeWorkerThread*) m_ThreadPool[i];
		pThread->Shutdown();
		delete pThread;
	}
}


/*
void CAnalyseThreadPool::Init(int iNumThreads)
{
	for(int i=0; i<iNumThreads; i++)
	{
		m_ThreadPool.PushAndGrow( new CAnalyseWorkerThread() );
		m_ThreadPool[i]->m_iThreadNum = i;
	}
}
void CAnalyseThreadPool::Shutdown()
{
	for(int i=0; i<m_ThreadPool.GetCount(); i++)
	{
		CAnalyseWorkerThread * pThread = (CAnalyseWorkerThread*) m_ThreadPool[i];
		pThread->Shutdown();
		delete pThread;
	}
}
*/


//////////////////////////////////////////////////////////////////////////////////////////////

CWorkerThreadBase * CThreadPoolBase::GetNextFree()
{
	for(int i=0; i<m_ThreadPool.GetCount(); i++)
	{
		if( !m_ThreadPool[i]->IsRunning() && !m_ThreadPool[i]->IsComplete() )
		{
			return m_ThreadPool[i];
		}
	}
	return NULL;
}
CWorkerThreadBase * CThreadPoolBase::GetNextComplete()
{
	for(int i=0; i<m_ThreadPool.GetCount(); i++)
	{
		if( m_ThreadPool[i]->IsRunning() && m_ThreadPool[i]->IsComplete() )
		{
			return m_ThreadPool[i];
		}
	}
	return NULL;
}
CWorkerThreadBase * CThreadPoolBase::GetNextRunning()
{
	for(int i=0; i<m_ThreadPool.GetCount(); i++)
	{
		if( m_ThreadPool[i]->IsRunning() && !m_ThreadPool[i]->IsComplete() )
		{
			return m_ThreadPool[i];
		}
	}
	return NULL;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

CGenerationWorkerThread::CGenerationWorkerThread()
{
	Init();
}

void CGenerationWorkerThread::Init()
{
	m_pNavParams = NULL;
	m_pNavMeshGenerator = NULL;
	m_iNumNeighbours = 0;
	m_pNeighbours[0] = m_pNeighbours[1] = m_pNeighbours[2] = m_pNeighbours[3] = NULL;
	fTotalTimeToPlaceNodes = 0.0f;
	fTotalTimeToTriangulate = 0.0f;
	fTotalTimeToOptimize = 0.0f;
	fTotalTimeTaken = 0.0f;
	m_bComplete = false;
}

bool CGenerationWorkerThread::Run(TNavMeshParams * pParams, CNavGen * pGenerator, CNavGen ** ppNeighborNavGens, int iNumNeighbours, bool bAerialNavigation)
{
	m_pNavParams = pParams;
	m_pNavMeshGenerator = pGenerator;
	m_iNumNeighbours = iNumNeighbours;
	for(int i=0; i<iNumNeighbours; i++)
		m_pNeighbours[i] = ppNeighborNavGens[i];
	m_bAerialNavigation = bAerialNavigation;

	m_Allocator = &sysMemAllocator::GetCurrent();
	HANDLE hThread = CreateThread(NULL, 1024*1024, ThreadFunction, this, CREATE_SUSPENDED, &m_iThreadId);
	ResumeThread(hThread);

	return true;
}

void CGenerationWorkerThread::Shutdown()
{
	m_pNavParams = NULL;
	
	if(m_pNavMeshGenerator)
	{
		m_pNavMeshGenerator->Shutdown();
		delete m_pNavMeshGenerator;
	}
	m_pNavMeshGenerator = NULL;

	for(int i=0; i<4; i++)
	{
		if(m_pNeighbours[i])
		{
			m_pNeighbours[i]->Shutdown();
			delete m_pNeighbours[i];
			m_pNeighbours[i] = NULL;
		}
	}
}

void CGenerationWorkerThread::SaveData()
{
	// Save out the .srf file
//	OUTPUT_TEXT(g_pBlankLine);
//	OUTPUT_TEXT("Saving .srf file.");

//	char textBuf[256];

	char fileNameNoExtension[256];
	char fileNameOctree[256];
	char fileNameSrf[256];
	char fileTitleNoPathOrExtension[256];

	// Strip extension
	const char * pFileName = m_pNavParams->m_pFileName;
	const char * pPtr = &pFileName[strlen(pFileName)-1];
	int iNumStripped = 1;
	while(*pPtr && *pPtr != '.')
	{
		pPtr--;
		iNumStripped++;
	}
	strncpy(fileNameNoExtension, pFileName, strlen(pFileName) - iNumStripped);
	fileNameNoExtension[strlen(pFileName) - iNumStripped] = 0;

	// Append the .srf (nav surface) extension
	sprintf(fileNameSrf, "%s.srf", fileNameNoExtension);

	// Append the .oct extension
	sprintf(fileNameOctree, "%s.oct", fileNameNoExtension);

	if(m_bAerialNavigation)
	{
		sReplaceStringLastOccurrence(fileNameSrf, sizeof(fileNameSrf), m_pNavParams->m_pSearchName, m_pNavParams->m_pReplaceName);
		sReplaceStringLastOccurrence(fileNameOctree, sizeof(fileNameOctree), m_pNavParams->m_pSearchName, m_pNavParams->m_pReplaceName);
	}

	// Remove the path
	pPtr = &fileNameNoExtension[strlen(fileNameNoExtension)-1];
	while(*pPtr && *pPtr != '\\' && *pPtr != '/')
	{
		pPtr--;
	}
	if(*pPtr == '\\' || *pPtr == '/')
	{
		pPtr++;
	}

	// Make a copy of the file title with no path or extension
	strcpy(fileTitleNoPathOrExtension, pPtr);

	// Save..
	m_pNavMeshGenerator->m_Octree.SaveOctree(fileNameOctree, &m_pNavMeshGenerator->m_CollisionTriangles, m_pNavParams->m_iTriFileCRC);

	m_pNavMeshGenerator->SaveNavSurfaceBinary(fileNameSrf, fileTitleNoPathOrExtension, m_pNavParams->m_iTriFileCRC, m_pNavParams->m_iResAreasCRC);
}

DWORD WINAPI CGenerationWorkerThread::ThreadFunction(LPVOID pData)
{
	CGenerationWorkerThread * pWorker = (CGenerationWorkerThread*)pData;

	sysMemAllocator::SetCurrent(*pWorker->m_Allocator);
	sysMemAllocator::SetMaster(*pWorker->m_Allocator);
	sysMemAllocator::SetContainer(*pWorker->m_Allocator);

	sprintf(pWorker->filename, "%s", pWorker->m_pNavParams->m_pFileName);

	pWorker->Process();

	return 0;
}

bool CGenerationWorkerThread::IsRunning()
{
	return (m_pNavMeshGenerator != NULL);
}
bool CGenerationWorkerThread::IsComplete()
{
	return m_pNavMeshGenerator && m_bComplete;
}
bool CGenerationWorkerThread::Process()
{
	char tmp[1024];
	sprintf(tmp, "(%i) starting navmesh[%i][%i]\n", m_iThreadNum, m_pNavParams->m_iStartSectorX, m_pNavParams->m_iStartSectorY);
	OUTPUT_TEXT(tmp);

	// Mark which triangles are walkable, based only upon surface orientation
	m_pNavMeshGenerator->FlagWalkableTris();

	// Create the octree
//	OUTPUT_TEXT(g_pBlankLine);
//	OUTPUT_TEXT("Creating octree.");
//	u32 iOctreeStartTime = timeGetTime();

	m_pNavMeshGenerator->InitOctree();

//	u32 iOctreeEndTime = timeGetTime();
//	float fOctreeSecs = (((float)(iOctreeEndTime - iOctreeStartTime)) / 1000.0f);
//	OUTPUT_TEXT(g_pBlankLine);
//	sprintf(tmpBuf, "Octree took %.3f secs to create.", fOctreeSecs);
//	OUTPUT_TEXT(tmpBuf);

	// Generate the nav mesh surface
//	OUTPUT_TEXT(g_pBlankLine);
//	OUTPUT_TEXT("Generating nav mesh...");

	m_pNavMeshGenerator->GenerateNavSurface(m_pNeighbours, m_iNumNeighbours, true, !g_bProcessingNonMapNavmeshes);

	//navGen.RemoveSteepSlopeJaggies();

//	OUTPUT_TEXT(g_pBlankLine);
//	sprintf(tmpBuf, "Took %.3f secs to place nodes", m_pNavMeshGenerator->m_fTimeTakenToPlaceNodes);
//	OUTPUT_TEXT(tmpBuf);
//	OUTPUT_TEXT(g_pBlankLine);
//	sprintf(tmpBuf, "Took %.3f secs in total to triangulate nodes (using %i areas)", m_pNavMeshGenerator->m_fTimeTakenToTriangulate, m_pNavMeshGenerator->m_iNumResAreas);
//	OUTPUT_TEXT(tmpBuf);
//	OUTPUT_TEXT(g_pBlankLine);
/*				
	sprintf(tmpBuf, "(pass1: %.2f, pass2: %.2f, jaggies: %.2f, smooth: %.2f, cullwater: %.2f, adjacency: %.2f, walls: %.2f)",
		m_pNavMeshGenerator->m_fTimeTakenToTriangulatePass1,
		m_pNavMeshGenerator->m_fTimeTakenToTriangulatePass2,
		m_pNavMeshGenerator->m_fTimeTakenToFixJaggies,
		m_pNavMeshGenerator->m_fTimeTakenToSmooth,
		m_pNavMeshGenerator->m_fTimeTakenToCullWaterTriangles,
		m_pNavMeshGenerator->m_fTimeTakenToEstablishAdjacency,
		m_pNavMeshGenerator->m_fTimeTakenToMoveNodesAwayFromWalls);
	OUTPUT_TEXT(tmpBuf);
	OUTPUT_TEXT(g_pBlankLine);

	OUTPUT_TEXT("Optimising nav mesh...");
*/
	// Repeat the optimisation steps..
	// First optimise pmesh & edges until no longer possible
	// Secondly, flood fill from walkable surface types

	//u32 iOptimizeStartTime = timeGetTime();

	bool bQuit = false;
	bool bSinglePass = false;

	if( m_pNavMeshGenerator->m_Params.m_bDontDoOptimisation )
		bQuit=true;

	//int iInitialNumTris = m_pNavMeshGenerator->m_iOptimisedTriangleCount;

	while(!bQuit)
	{
		//OutputDebugString("//***********************************************\n");
		//sprintf(tmpBuf, "// Optimising Nav-Surface\n");
		//OutputDebugString(tmpBuf);
		//OutputDebugString("//***********************************************\n");

		m_pNavMeshGenerator->m_bDontMoveNonConnectedFaces = false;

		int numTimesZero = 0;

		do
		{
			m_pNavMeshGenerator->OptimiseNavSurface();

			if(m_pNavMeshGenerator->m_iNumEdgesCollapsedThisPass == 0)
			{
				numTimesZero++;
			}
			else
			{
				numTimesZero = 0;
			}

			if(bSinglePass)
			{
				bQuit = true;
				break;
			}

		} while(numTimesZero < 2);


		//OutputDebugString("//***********************************************\n");
		//sprintf(tmpBuf, "// Optimising edges (tri fans)\n");
		//OutputDebugString(tmpBuf);
		//OutputDebugString("//***********************************************\n");

		int iTrisBefore = m_pNavMeshGenerator->m_iOptimisedTriangleCount;

		m_pNavMeshGenerator->OptimiseEdges();

		int iTrisAfter = m_pNavMeshGenerator->m_iOptimisedTriangleCount;

		if(iTrisBefore == iTrisAfter)
			bQuit = true;
	}

	//u32 iOptimizeEndTime = timeGetTime();
	//m_fTimeTakenToOptimize = (float)(iOptimizeEndTime - iOptimizeStartTime) / 1000.0f;

	//sprintf(tmpBuf, "Took %.3f secs to optimize navmesh.  Triangles (%i / %i)", m_fTimeTakenToOptimize, m_pNavMeshGenerator->m_iOptimisedTriangleCount, iInitialNumTris);
	//OUTPUT_TEXT(tmpBuf);

	// Maybe remove polys which we think are behind walls as a post-process?
	if(m_pNavParams->m_bCullBehindWalls && m_pNavMeshGenerator->m_bRemoveTrisBehindWalls && !m_pNavMeshGenerator->m_bDoBehindWallTestDuringTriangulation)
	{
		m_pNavMeshGenerator->CullTrianglesBehindWallsAfterOptimisation();
	}

	m_pNavMeshGenerator->RemoveRiverBedTrianglesAfterOptimisation();

	// Clean out any bad data which might have been generated
	// (zero-area triangles, double adjacencies, etc.)
	m_pNavMeshGenerator->CleanBadData();

	for(int i = 0; i < 4; i++)
	{
		if(m_pNeighbours[i])
		{
			delete m_pNeighbours[i];
			m_pNeighbours[i] = NULL;
		}
	}

#ifdef NAVGEN_SPEW_DEBUG
	static bool bSanityCheck=false;
	if(bSanityCheck)
		m_pNavMeshGenerator->SanityCheckEverything();
#endif

	//----------------------------------------------------------------------------------
/*
	sprintf(textBuf, "Took %.2f secs (nodes : %.2f) (tris : %.2f) (optimize : %.2f)",
		m_pNavMeshGenerator->m_fTimeTakenToPlaceNodes + m_pNavMeshGenerator->m_fTimeTakenToTriangulate + m_fTimeTakenToOptimize,
		m_pNavMeshGenerator->m_fTimeTakenToPlaceNodes,
		m_pNavMeshGenerator->m_fTimeTakenToTriangulate,
		m_fTimeTakenToOptimize
		);

	fTotalTimeToPlaceNodes += m_pNavMeshGenerator->m_fTimeTakenToPlaceNodes;
	fTotalTimeToTriangulate += m_pNavMeshGenerator->m_fTimeTakenToTriangulate;
	fTotalTimeToOptimize += m_fTimeTakenToOptimize;
	fTotalTimeTaken += m_pNavMeshGenerator->m_fTimeTakenToPlaceNodes + m_pNavMeshGenerator->m_fTimeTakenToTriangulate + m_fTimeTakenToOptimize;
*/

	//	OUTPUT_TEXT(g_pBlankLine);
	//	OUTPUT_TEXT(textBuf);

	//	OUTPUT_TEXT("\n");

	sprintf(tmp, "(%i) completed navmesh[%i][%i]\n", m_iThreadNum, m_pNavParams->m_iStartSectorX, m_pNavParams->m_iStartSectorY);
	OUTPUT_TEXT(tmp);

	m_bComplete = true;

	return true;
}


////////////////////////////////////////////////////////////////////////////////////////////////

s32 CMergeWorkerThread::ms_iMaxVertexPoolSize = 0;
s32 CMergeWorkerThread::ms_iMaxVertexIndex = 0;

CMergeWorkerThread::CMergeWorkerThread()
{
	Init();
}

void CMergeWorkerThread::Init()
{
	m_pNavParams = NULL;
	m_vBlockMin = VEC3_ZERO;
	m_vBlockMax = VEC3_ZERO;
	m_bRunning = false;
	m_bComplete = false;
	m_bMainMap = true;
	m_bAerialNavigation = false;
	m_pMergedNavMesh = NULL;
}

bool CMergeWorkerThread::Run(TNavMeshParams * pParams, const Vector3 & vBlockMin, const Vector3 & vBlockMax, const bool bMainMap, const bool bAerialNavigation)
{
	m_bRunning = true;

	m_pNavParams = pParams;
	m_vBlockMin = vBlockMin;
	m_vBlockMax = vBlockMax;
	m_bMainMap = bMainMap;
	m_bAerialNavigation = bAerialNavigation;

	m_Allocator = &sysMemAllocator::GetCurrent();
	HANDLE hThread = CreateThread(NULL, 1024*1024, ThreadFunction, this, CREATE_SUSPENDED, &m_iThreadId);
	ResumeThread(hThread);

	return true;
}

void CMergeWorkerThread::Shutdown()
{
	m_pNavParams = NULL;
	m_bRunning = false;
	m_bComplete = false;
}

void CMergeWorkerThread::SaveData()
{
	if(!m_pMergedNavMesh)
		return;

	char navMeshFileName[1024];
	sprintf(navMeshFileName, "%s.%s", m_pNavParams->m_pFileNameWithoutExtension, "mrg");

	//----------------------------------------------------------------------------------------------------------
	// CNavMesh::LoadBinary() required NAVMESH_IS_DYNAMIC to be set if we don't have an index in the world map

	if(!m_bMainMap && m_pMergedNavMesh->GetIndexOfMesh() == NAVMESH_INDEX_FIRST_DYNAMIC)
	{
		m_pMergedNavMesh->SetFlags( m_pMergedNavMesh->GetFlags() | NAVMESH_IS_DYNAMIC );
	}

	m_pMergedNavMesh->SetBuildID(0);

	CNavMesh::SaveBinary(m_pMergedNavMesh, navMeshFileName);

	//---------------------------------------------------------------
	// Append the CRC of the source collision file.
	// This is the only difference between the .inv and .mrg files

	fiStream * stream = fiStream::Open(navMeshFileName, false);
	if(stream)
	{
		int iSeekOk = stream->Seek( stream->Size() );
		if(iSeekOk != -1)
		{
			stream->WriteInt( &m_pNavParams->m_iTriFileCRC, 1);
			stream->WriteInt( &m_pNavParams->m_iResAreasCRC, 1);
			stream->Close();
		}
	}

#ifdef CHECK_NAVMESH_INTEGRITY
	CNavGen::CheckNavMeshIntegrity(m_pMergedNavMesh);
#endif

	delete m_pMergedNavMesh;
	m_pMergedNavMesh = NULL;
}

DWORD WINAPI CMergeWorkerThread::ThreadFunction(LPVOID pData)
{
	CMergeWorkerThread * pWorker = (CMergeWorkerThread*)pData;

	sysMemAllocator::SetCurrent(*pWorker->m_Allocator);
	sysMemAllocator::SetMaster(*pWorker->m_Allocator);
	sysMemAllocator::SetContainer(*pWorker->m_Allocator);

	pWorker->Process();

	return 0;
}

bool CMergeWorkerThread::IsRunning()
{
	return m_bRunning;
}

bool CMergeWorkerThread::IsComplete()
{
	return m_bComplete;
}

bool CMergeWorkerThread::Process()
{
	char tmp[1024];
	sprintf(tmp, "(%i) starting navmesh[%i][%i]\n", m_iThreadNum, m_pNavParams->m_iStartSectorX, m_pNavParams->m_iStartSectorY);
	OUTPUT_TEXT(tmp);

	CNavGen navGen;
	CNavMesh * pNavMesh = NULL;
	
	// Hand-authored navmeshes are loaded here
	if(m_pNavParams->m_pAuthoredMeshFileName)
	{
		pNavMesh = navGen.Load3dsMaxAsciiAsNavMesh(m_pNavParams->m_pAuthoredMeshFileName);
	}
	// Otherwise we load the ".srf" file which was the output of the previous sampling stage
	else
	{
		pNavMesh = navGen.LoadNavSurfaceBinaryAsNavMesh(m_pNavParams->m_pFileName, m_vBlockMin, m_vBlockMax, m_bMainMap);
	}

	if(!pNavMesh)
	{
		m_bComplete = true;
		return false;
	}

	ms_iMaxVertexPoolSize = Max(ms_iMaxVertexPoolSize, navGen.m_iLoadNavSurfaceBinary_MaxVertexPoolSize);
	ms_iMaxVertexIndex = Max(ms_iMaxVertexIndex, navGen.m_iLoadNavSurfaceBinary_MaxVertexIndex);

#ifdef CHECK_NAVMESH_INTEGRITY
	CNavGen::CheckNavMeshIntegrity(pNavMesh);
#endif

	s32 iMeshX, iMeshY;
	TNavMeshIndex iMeshIndex = CNavGen_NavMeshStore::GetNavMeshIndexFromFilename(m_pNavParams->m_pFileName, &iMeshX, &iMeshY);

	if(iMeshIndex!=NAVMESH_NAVMESH_INDEX_NONE)
	{
		pNavMesh->SetIndexOfMesh(iMeshIndex);
	}
	else
	{
		pNavMesh->SetIndexOfMesh(NAVMESH_INDEX_FIRST_DYNAMIC);
		//OutputText("bad filename format.");
	}

	pNavMesh->EstablishPolyAdjacancy();

//	for(u32 p=0; p<pNavMesh->GetNumPolys(); p++)
//	{
//		TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);
//		for(u32 a=0; a<pPoly->GetNumVertices(); a++)
//		{
//			pNavMesh->GetAdjacentPolysArray().Get(pPoly->GetFirstVertexIndex()+a)->m_iAdjacentNavMeshIndex =
//				pNavMesh->GetAdjacentPolysArray().Get(pPoly->GetFirstVertexIndex()+a)->GetNavMeshIndex(pNavMesh->GetAdjacentMeshes());
//		}
//	}

	//OutputText(g_pBlankLine);
	//OutputText("Merging polys\n");

	if(m_pNavParams->m_pAuthoredMeshFileName)
	{
		// No merging performed for hand-authored navmeshes
		m_pMergedNavMesh = pNavMesh;

		// Classify poly sizes
		const float fSmallPolyArea = 2.0f;
		const float fLargePolyArea = 40.0f;
		m_pMergedNavMesh->SetPolysSizeFlags(fSmallPolyArea, fLargePolyArea);

		navGen.MakeNavMeshQuadTree(m_pMergedNavMesh, m_pMergedNavMesh->GetQuadTree()->m_Mins, m_pMergedNavMesh->GetQuadTree()->m_Maxs);
	}
	else
	{
#if __SPLIT_POLYGONS_AFTER_MERGE
		vector<fwDynamicEntityInfo*> entityBounds;
		char extrasFileName[1024];
		sprintf(extrasFileName, "%s/Extras_%i_%i.dat", g_InputPath, iMeshX, iMeshY);
		CNavGen::ParseExtraInfoFile(extrasFileName, NULL, NULL, NULL, &entityBounds);
#endif

		navGen.m_Params.m_bAerialNavigation = m_bAerialNavigation;

		u32 iStartTime = timeGetTime();
		int iNumVerticesBeforeMerge = pNavMesh->GetNumVertices();
		int iNumPolygonsBeforeMerge = pNavMesh->GetNumPolys();

		if(iNumPolygonsBeforeMerge)
		{
			// Create new navmesh by merging triangles into polygons
			m_pMergedNavMesh = navGen.CreateMergedPolyNavMesh(pNavMesh, &entityBounds);

			delete pNavMesh;
			pNavMesh = NULL;

			// Classify poly sizes
			const float fSmallPolyArea = 2.0f;
			const float fLargePolyArea = 40.0f;
			m_pMergedNavMesh->SetPolysSizeFlags(fSmallPolyArea, fLargePolyArea);

			// Establish adjacency again
			// Mainly to workaround a problem with polygon bisection
			m_pMergedNavMesh->EstablishPolyAdjacancy();

			u32 iEndTime = timeGetTime();
			float fSecs = ((float)(iEndTime - iStartTime) / 1000.0f);

			if(CNavGen::ms_iVerbosity > 0)
			{
				char textBuf[1024];
				sprintf(textBuf, "Verts: (%i/%i).  Polys: (%i/%i).  Took %.2f secs\n",
					m_pMergedNavMesh->GetNumVertices(),
					iNumVerticesBeforeMerge,
					m_pMergedNavMesh->GetNumPolys(),
					iNumPolygonsBeforeMerge,
					fSecs);
				OUTPUT_TEXT(textBuf);
			}
		}

		navGen.MakeNavMeshQuadTree(m_pMergedNavMesh, m_vBlockMin, m_vBlockMax);
	}

	m_bComplete = true;


	sprintf(tmp, "(%i) completed navmesh[%i][%i]\n", m_iThreadNum, m_pNavParams->m_iStartSectorX, m_pNavParams->m_iStartSectorY);
	OUTPUT_TEXT(tmp);

	return true;
}

