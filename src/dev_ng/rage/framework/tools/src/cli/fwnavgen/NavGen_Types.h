#ifndef NAVGEN_TYPES
#define NAVGEN_TYPES

#include "toolnavmesh.h"
#include "vector/vector3.h"
#include "atl/array.h"
#include "ai/navmesh/datatypes.h"
//#include "ai/navmesh/shortminmax.h"
#include "fwmaths/Vector.h"

#include "NavGen_Utils.h"

using namespace std;
#include <vector>
#include <list>

namespace rage
{
	class CNavSurfaceTri;
	class CNavGenNode;
	class CNavTriEdge;
	class CNavGenTri;

	typedef atArray<CNavGenTri, 0, u32> TColTriArray;

//-----------------------------------------------------------------------------------------
//	CLASS : CNavGenNode
//	PURPOSE : The node class used for generation of the navigation surface which defines
//	all areas which are walkable in the world


// Set when this node has been removed as a product of a vertex-collapse optimisation
#define NAVGENNODE_REMOVED									0x01
#define NAVGENNODE_HASBEENOPTIMISED							0x02
// Set when the node's position has changed.  When this occurs, cached cost values must be recalculated.
#define NAVGENNODE_HASCHANGEDPOSITION						0x04

// These flags are set whenever a triangle edge is formed between the node, and the adjacent nodes
// during triangulation.  By using these flags, we can do a 2nd triangulation pass and fill in
// gaps in the mesh left when triangulation wasn't possible for the initial configuration.
#define NAVGENNODE_EDGE_EXISTS_NEGX							0x08
#define NAVGENNODE_EDGE_EXISTS_POSX							0x10
#define NAVGENNODE_EDGE_EXISTS_NEGY							0x20
#define NAVGENNODE_EDGE_EXISTS_POSY							0x40
// These are set if a diagonal edge has been triangulated to/from this node
#define NAVGENNODE_EDGE_EXISTS_DIAGONAL_POSX_NEGY			0x80
#define NAVGENNODE_EDGE_EXISTS_DIAGONAL_NEGX_POSY			0x100

#define NAVGENNODE_HASBEENTRIANGULATED_SO_SKIP				0x200
#define NAVGENNODE_HASBEENTRIANGULATED2_SO_SKIP				0x400

#define NAVGENNODE_DEBUG_MARKED								0x800
#define NAVGENNODE_ON_EDGE_OF_MESH							0x1000
#define NAVGENNODE_ON_EDGE_OF_RESOLUTION_AREA				0x2000
#define NAVGENNODE_IS_ON_STAIRS_SURFACE						0x4000
#define NAVGENNODE_DO_NOT_OPTIMISE							0x8000

// These are the triangulation flags (stored in m_iTriangulationFlags)
#define NAVGENNODE_LOS_DONE_NEGX							0x1
#define NAVGENNODE_LOS_DONE_NEGY							0x2
#define NAVGENNODE_LOS_DONE_POSX							0x4
#define NAVGENNODE_LOS_DONE_POSY							0x8
#define NAVGENNODE_LOS_DONE_NEGXPOSY						0x10
#define NAVGENNODE_LOS_DONE_POSXNEGY						0x20
#define NAVGENNODE_LOS_DONE_POSXPOSY						0x40
#define NAVGENNODE_LOS_DONE_NEGXNEGY						0x80

#define NAVGENNODE_LOS_EXISTS_NEGX							0x100
#define NAVGENNODE_LOS_EXISTS_NEGY							0x200
#define NAVGENNODE_LOS_EXISTS_POSX							0x400
#define NAVGENNODE_LOS_EXISTS_POSY							0x800
#define NAVGENNODE_LOS_EXISTS_NEGXPOSY						0x1000
#define NAVGENNODE_LOS_EXISTS_POSXNEGY						0x2000
#define NAVGENNODE_LOS_EXISTS_POSXPOSY						0x4000
#define NAVGENNODE_LOS_EXISTS_NEGXNEGY						0x8000

// These are the height-test flags (also stored in m_iTriangulationFlags)
#define NAVGENNODE_HEIGHTTEST_DONE_NEGX						0x10000
#define NAVGENNODE_HEIGHTTEST_DONE_NEGY						0x20000
#define NAVGENNODE_HEIGHTTEST_DONE_POSX						0x40000
#define NAVGENNODE_HEIGHTTEST_DONE_POSY						0x80000
#define NAVGENNODE_HEIGHTTEST_DONE_NEGXPOSY					0x100000
#define NAVGENNODE_HEIGHTTEST_DONE_POSXNEGY					0x200000
#define NAVGENNODE_HEIGHTTEST_DONE_POSXPOSY					0x400000
#define NAVGENNODE_HEIGHTTEST_DONE_NEGXNEGY					0x800000

#define NAVGENNODE_HEIGHTTEST_OK_NEGX						0x1000000
#define NAVGENNODE_HEIGHTTEST_OK_NEGY						0x2000000
#define NAVGENNODE_HEIGHTTEST_OK_POSX						0x4000000
#define NAVGENNODE_HEIGHTTEST_OK_POSY						0x8000000
#define NAVGENNODE_HEIGHTTEST_OK_NEGXPOSY					0x10000000
#define NAVGENNODE_HEIGHTTEST_OK_POSXNEGY					0x20000000
#define NAVGENNODE_HEIGHTTEST_OK_POSXPOSY					0x40000000
#define NAVGENNODE_HEIGHTTEST_OK_NEGXNEGY					0x80000000


#define NODE_INDEX_UNASSIGNED								0xFFFFFFFF

#if __DEV
extern u32 g_iDebugNodeIndex;
#endif

class ALIGNAS(16) CNavGenNode
{
public:
	inline CNavGenNode(void)
	{
		m_iFlags = 0;
		m_iTriangulationFlags = 0;
		m_iIndexOfThisNode = NODE_INDEX_UNASSIGNED;
#if __DEV
		m_iDebugNodeIndex = g_iDebugNodeIndex++;
#endif
		m_pCollisionTriangleThisIsOn = NULL;
		m_bIsWater = false;
		m_bIsUnderwater = false;
		m_bIsSlicePlane = false;
		m_bHasBeenMovedToFixJaggies = false;
		m_iNumTimesMovedToSmoothEdges = 0;
		m_iSampleResBits = 0;

		m_TrianglesUsingThisNode.reserve(32);
		m_EdgesUsingThisNode.reserve(32);
	}
	inline ~CNavGenNode(void) { }

	// The position of the base of the node
	Vector3 m_vBasePos;
	// The index of the triangle which this node is sitting on
	u32 m_iFlags;
	u32 m_iTriangulationFlags;

	// All the triangles which surround this node
	vector<CNavSurfaceTri*> m_TrianglesUsingThisNode;

	// For mesh optimisation, a list of the edges surrounding this node
	vector<CNavTriEdge*> m_EdgesUsingThisNode;

	// This index is used when saving out the .3ds file of the navmesh.
	// It is assigned via an ordered walk of the octree just before saving.
	u32 m_iIndexOfThisNode;

	// For debugging
#if __DEV
	u32 m_iDebugNodeIndex;
#endif
	// This is the collision triangle which this node is sitting upon
	CNavGenTri * m_pCollisionTriangleThisIsOn;

	// Set if this node is on the water surface
	u32 m_bIsWater						:1;

	// Set if this node is underneath the water
	u32 m_bIsUnderwater					:1;

	// This node is sitting on an artificial surface designed to help wi 3d navigation
	u32 m_bIsSlicePlane					:1;

	// Set if this node is completely surrounded by triangles.
	// ie. If it is in the interior of the navmesh.
	u32 m_bIsSurroundedByTriangles		:1;
	u32 m_bHasBeenMovedToFixJaggies		:1;
	u32 m_iNumTimesMovedToSmoothEdges	:8;
	u32 m_iSampleResBits				:8;
	
	inline bool IsRemoved(void) { return (m_iFlags & NAVGENNODE_REMOVED); }
	inline void SetRemoved(void) { m_iFlags |= NAVGENNODE_REMOVED; }

	inline bool ContainsTriangle(CNavSurfaceTri * pTri)
	{
		for(u32 t=0; t<m_TrianglesUsingThisNode.size(); t++)
			if(m_TrianglesUsingThisNode[t] == pTri)
				return true;
		return false;
	}

};	// 56 bytes




//------------------------------------------------------------------------------------------------------
//	CLASS : CNavGenTri
//	PURPOSE : This class defines a collision triangle which makes up part of the geometry of the world


// Means that this triangle is orientated upwards, and deemed walkable
#define NAVGENTRI_WALKABLE				0x01
#define NAVGENTRI_PAVEMENT				0x02
#define NAVGENTRI_STAIRS				0x04
#define NAVGENTRI_SEETHROUGH			0x08

// Used by the -analyse functions, to say that the edge has already been check for corners
#define NAVGENTRI_EDGE_01_CHECKED		0x10
#define NAVGENTRI_EDGE_12_CHECKED		0x20
#define NAVGENTRI_EDGE_20_CHECKED		0x40

#define NAVGENTRI_SHOOTTHROUGH			0x80
#define NAVGENTRI_TOOSTEEP				0x100

#define TRI_IS_SHELTERED_DIST			30.0f

class ALIGNAS(16) CNavGenTri
{
public:
	inline CNavGenTri(void)
	{
		m_iIndexInCollisionPolysList = 0;
		m_iFlags = 0;
		m_iTimeStamp = 0xFFFFFFFF;
		m_fArea = 0.0f;
	}
	inline ~CNavGenTri(void) { }

	inline bool GetIsWalkable() const { return (m_iFlags & NAVGENTRI_WALKABLE)!=0; }
	inline bool GetIsTooSteep() const { return (m_iFlags & NAVGENTRI_TOOSTEEP)!=0; }

	// The triangle vertices
	Vector3 m_vPts[3];
	// The triangle plane normal
	Vector3 m_vNormal;
	// Packed min/max extents of the triangle
	TShortMinMax m_MinMax;
	// We timestamp the poly during the course of queries/rendering.
	// It is the same as the ScanCode() system in CWorld, and prevents items being visited twice
	// when they may referenced several times within a data structure.
	u32 m_iTimeStamp;
	u32 m_iFlags;
	TColPolyData m_colPolyData;
	u8 m_iBakedInDataBitField;
	// The distance along the normal to the origin
	float m_fPlaneDist;
	float m_fArea;
	// The index that this poly has in the m_CollisionTriangles list.
	u32 m_iIndexInCollisionPolysList;

	u8 m_iPadding[16];	// pad up to 128 bytes
};	// 64 bytes


//---------------------------------------------------------------------------------
//	CLASS : CNavSurfaceTri
//	PURPOSE : This class defines a single triangle in the navigation surface.


// This flag is set when the tri has been removed due to an edge collapse
#define NAVSURFTRI_REMOVED						0x01
// These flags are set when the cost tri edge-collapse has been calculated
// already during the processing of an adjoining tri
#define NAVSURFTRI_EDGE_0_TO_1_CACHED			0x02
#define NAVSURFTRI_EDGE_1_TO_2_CACHED			0x04
#define NAVSURFTRI_EDGE_2_TO_0_CACHED			0x08

#define NAVSURFTRI_ISMARKED_GREEN				0		// default col
#define NAVSURFTRI_ISMARKED_RED					0x10
#define NAVSURFTRI_ISMARKED_YELLOW				0x20
#define NAVSURFTRI_ISMARKED_WHITE				0x40

#define NAVSURFTRI_ISMARKED_MASK				(0x10 + 0x20 + 0x40)

#define NAVSURFTRI_PATHFIND_OPEN				0x80
#define NAVSURFTRI_PATHFIND_CLOSED				0x100
#define NAVSURFTRI_PATHFIND_PATHTRI				0x200

// If this flags is NOT set, and the triangle is NOT surrounded by adjacent tris
// then there's a good chance that we can remove it after the optimisation step..
#define NAVSURFTRI_MODIFIEDBYOPTIMISATION		0x400
#define NAVSURFTRI_EXTERIOR						0x800
#define NAVSURFTRI_ACCESSIBLE					0x1000
#define NAVSURFTRI_BUGRIDDEN					0x2000

#define NAVSURFTRI_ATEDGEOFMESH					0x4000

#define NAVSURFTRI_ADJACENCY_ESTABLISHED_1		0x8000
#define NAVSURFTRI_ADJACENCY_ESTABLISHED_2		0x10000

#define NAVSURFTRI_WAS_MOVED					0x20000

class ALIGNAS(16) CNavSurfaceTri
{
public:
	static s32 ms_iGenerationId;

	CNavSurfaceTri();
	~CNavSurfaceTri(void) { }

	inline Vector3 Centroid(void)
	{
		Vector3 vCentroid(0,0,0);
		for(int v=0; v<3; v++)
		{
			vCentroid.x += m_Nodes[v]->m_vBasePos.x;
			vCentroid.y += m_Nodes[v]->m_vBasePos.y;
			vCentroid.z += m_Nodes[v]->m_vBasePos.z + 1.0f;
		}
		return (vCentroid / 3.0f);
	}
	inline Vector3 CalcNormal()
	{
		const Vector3 & v0 = m_Nodes[0]->m_vBasePos;
		const Vector3 & v1 = m_Nodes[1]->m_vBasePos;
		const Vector3 & v2 = m_Nodes[2]->m_vBasePos;
		// Calculate the triangle plane, see if normal faces downwards (negative Z)
		Vector3 edge1 = v1 - v0;
		Vector3 edge2 = v2 - v0;
		edge1.Normalize();
		edge2.Normalize();
		const Vector3 vNormal = CrossProduct(edge2, edge1);
		return vNormal;
	}
	inline void InitMinMax()
	{
		Vector3 vMin(FLT_MAX, FLT_MAX, FLT_MAX);
		Vector3 vMax(-FLT_MAX, -FLT_MAX, -FLT_MAX);
		for(int p=0; p<3; p++)
		{
			vMin.x = Min(vMin.x, m_Nodes[p]->m_vBasePos.x);
			vMin.y = Min(vMin.y, m_Nodes[p]->m_vBasePos.y);
			vMin.z = Min(vMin.z, m_Nodes[p]->m_vBasePos.z);
			vMax.x = Max(vMax.x, m_Nodes[p]->m_vBasePos.x);
			vMax.y = Max(vMax.y, m_Nodes[p]->m_vBasePos.y);
			vMax.z = Max(vMax.z, m_Nodes[p]->m_vBasePos.z);
		}
		m_MinMax.Set(vMin, vMax);
	}
	inline bool IsRemoved(void) { return (m_iFlags & NAVSURFTRI_REMOVED); }
	inline void SetRemoved(void)
	{
		m_iFlags |= NAVSURFTRI_REMOVED;
	}

	inline void RemoveAdjacency(CNavSurfaceTri * pOther)
	{
		for(int p=0; p<3; p++)
			if(m_AdjacentTris[p] == pOther)
				m_AdjacentTris[p] = NULL;
	}
	inline int GetEdge(CNavGenNode * pNode1, CNavGenNode * pNode2, bool bCareAboutOrdering) const
	{
		if((m_Nodes[0]==pNode1 && m_Nodes[1]==pNode2) || (!bCareAboutOrdering && (m_Nodes[0]==pNode2 && m_Nodes[1]==pNode1)))
			return 0;
		if((m_Nodes[1]==pNode1 && m_Nodes[2]==pNode2) || (!bCareAboutOrdering && (m_Nodes[1]==pNode2 && m_Nodes[2]==pNode1)))
			return 1;
		if((m_Nodes[2]==pNode1 && m_Nodes[0]==pNode2) || (!bCareAboutOrdering && (m_Nodes[2]==pNode2 && m_Nodes[0]==pNode1)))
			return 2;
		return -1;
	}
	inline int GetEdgeForAntiAliasPass(CNavGenNode * pNode1, CNavGenNode * pNode2, s32 iPass) const
	{
		s32 iEdge = -1;
		if(m_Nodes[0]==pNode1 && m_Nodes[1]==pNode2)
			iEdge = 0;
		else if(m_Nodes[1]==pNode1 && m_Nodes[2]==pNode2)
			iEdge = 1;
		else if(m_Nodes[2]==pNode1 && m_Nodes[0]==pNode2)
			iEdge = 2;

		if(iEdge != -1 && m_AntiAliasPass[iEdge] >= iPass)
			iEdge = -1;

		return iEdge;
	}
	inline bool EdgeIntersectsObjects(int e) const
	{
		return (m_iEdgeObjLosFlags & (1<<e)) != 0;
	}
	inline int GetAdjacencyEdge(CNavSurfaceTri * pOtherTri) const
	{
		if(!pOtherTri)
			return -1;
		if(m_AdjacentTris[0]==pOtherTri) return 0;
		if(m_AdjacentTris[1]==pOtherTri) return 1;
		if(m_AdjacentTris[2]==pOtherTri) return 2;
		return -1;
	}
	inline void OrderNodesWithWinding(CNavGenNode ** ppNode1, CNavGenNode ** ppNode2)
	{
		int i1 = GetNodeIndex(m_Nodes, *ppNode1);
		int i2 = GetNodeIndex(m_Nodes, *ppNode2);
		Assert(i1 != -1 && i2 != -1);

		if( (i1==1 && i2==0) || (i1==2 && i2==1) || (i1==0 && i2==2) )
			SwapEm(*ppNode1, *ppNode2);
	}

	//---------------------------------------------------------------------------

	CNavGenNode * m_Nodes[3];
	CNavSurfaceTri * m_AdjacentTris[3];

	u32 m_iFlags;
	TColPolyData m_colPolyData;
	u8 m_iBakedInBitField;

	// Whether there is any collision within TRI_IS_SHELTERED_DIST above the triangle
	u32 m_bIsSheltered				:1;
	u32 m_bIsWater					:1;
	u32 m_bIsSlicePlane				:1;
	u32 m_bIsTooSteep				:1;
	u32 m_bCalcSpaceAbove			:1;
	u32 m_bIntersectsEntities		:1;
	u32 m_bCreatedInFixJaggies		:1;
	u32 m_bAboveCullAreaThreshold	:1;
	u32 m_bDoNotOptimise			:1;
	u32 m_bDoNotCullSmallArea		:1;

	u32 m_iFloodFillTimeStamp;

	u8 m_AntiAliasPass[3];

	// This is used to calculate the A* heuristic, and also as a guide for placing nodes when refining the path
	// NB : this needn't be stored btw - it can be calculated as-required on the fly..
//	Vector3 m_vClosestPointInTri;
	Vector3 m_vNormal;
	float m_fPlaneD;

	u8 m_iEdgeObjLosFlags;

	TShortMinMax m_MinMax;

	// The z value of the lowest navigable space above the triangle
	s16 m_iFreeSpaceTopZ;

	s32 m_iGenerationIndex;

};	// 44 bytes



//---------------------------------------------------------------------------
// CLASS : CNavTriEdge
// PURPOSE : Edge class used for mesh optimisation


#define TRIEDGE_REMOVED					0x01
#define TRIEDGE_TOPOLOGYHASCHANGED		0x02
#define TRIEDGE_ADDEDFROMLEAF			0x04
#define TRIEDGE_NOTYETBEENMOVEDATALL	0x08
#define TRIEDGE_JAGGIESREMOVED			0x10

class CNavTriEdge
{
public:
	inline CNavTriEdge(void)
	{
		m_iFlags = (TRIEDGE_TOPOLOGYHASCHANGED | TRIEDGE_NOTYETBEENMOVEDATALL);
		m_fCostNode1ToNode2 = 0;
		m_fCostNode2ToNode1 = 0;
	}
	inline ~CNavTriEdge(void) { }

	inline bool IsRemoved(void) { return (m_iFlags & TRIEDGE_REMOVED); }
	inline void SetRemoved(void) { m_iFlags |= TRIEDGE_REMOVED; }

	CNavSurfaceTri * m_pTri1;
	CNavSurfaceTri * m_pTri2;
	CNavGenNode * m_pNode1;
	CNavGenNode * m_pNode2;
	float m_fCostNode1ToNode2;
	float m_fCostNode2ToNode1;
	u32 m_iFlags;

	u32 m_iPad;	// pad to 32 bytes

};	// 28 bytes



//----------------------------------------------------------------------------------------------------
//	CLASS : CPlacedNodeMultiMap
//	PURPOSE : This is a container which simply stores an 2d array of arrays.
//	We need to be able to quickly find nodes in the world based upon their (x,y) coordinates,
//	and for each (x,y) we may have multiple nodes with differing Z values.

class CPlacedNodeMultiMap
{
public:

	enum
	{
		GETNODE_INPUT_IS_ON_STAIRS,
		GETNODE_INCREASE_Z_THRESHOLD_FOR_STAIRS,
		GETNODE_NOT_TRIANGULATED_ONLY,
		GETNODE_2ND_PASS_TRIANGULATION_ONLY,
		GETNODE_CLOSEST_Z_INSTEAD_OF_HIGHEST_Z
	};

	CPlacedNodeMultiMap(const Vector3 & vMin, const Vector3 & vMax, float fSpacingXY);
	~CPlacedNodeMultiMap(void);

	bool IsFlaggedAsProcessed(const float x, const float y) const;
	void FlagAsProcessed(const float x, const float y);

	// Adds a node to the data-structure.  It is stored for quick (x,y) retrieval.
	int AddNode(CNavGenNode * pNode);

	// Gets a node from the data-structure at the given position, and with the
	// specified range of valid Z-coordinate.  If bOnStairs is set, then we will be]
	// more forgiving about height differences..
	CNavGenNode * GetNode(const Vector3 & vPosition, float fZInterval, const float fTriangulationGetNodeEps, bool bOnStairs, bool bNotTriangulated = false, bool b2ndTriangulationPass = false, bool bClosestZ = false, bool bIncreaseZThresholdForStairs = true);


	// Gets the first node at the given (x,y) coords, whose z-value is LESS THAN the given value
	// This is used to step down to lower nodes which may have been generated.
	CNavGenNode * GetFirstNodeBelowZVal(const Vector3 & vPosition, const float fTriangulationGetNodeEps);

	inline CNavGenNode * GetHighestNodeWhichHasntBeenTriangulated(float fXPos, float fYPos, int b2ndTriangulationPass, const float fTriangulationGetNodeEps)
	{
		return GetNode(Vector3(fXPos, fYPos, -5000.0f), 10000.0f, fTriangulationGetNodeEps, false, true, b2ndTriangulationPass!=0);
	}

private:

	Vector3 m_vMin;
	Vector3 m_vMax;
	u32 m_iWidth;
	u32 m_iHeight;
	float m_fSpacingXY;
	float m_fSpacingXYRecip;

	vector<CNavGenNode*> * m_pNodeArrays;
	u8 * m_bProcessed;
};



//--------------------------------------------------------
// Misc Classes

class CNavGenTriForHeightMap
{
public:
	Vec3V m_vPts[3];
	TColPolyData m_colPolyData;
};

struct TNavGenTriPair
{
	CNavGenTri * m_pTri1;
	CNavGenTri * m_pTri2;
};

struct VECTOR_ALIGN TCarNode
{
	Vector3 vPos;
};

class CSlicePlane
{
public:
	Vector3 m_vNormal;
	float m_fDist;
};

class TPolygonToReplace
{
public:
	s32 m_iPolyIndex;
	vector<Vector3*> m_Verts;
	vector<TAdjPoly> m_Adjacencies;
	vector<u16> m_VertexIndices;

	~TPolygonToReplace()
	{
		for(u32 v=0; v<m_Verts.size(); v++)
			delete m_Verts[v];
	}
};

}

#endif // NAVGEN_TYPES



