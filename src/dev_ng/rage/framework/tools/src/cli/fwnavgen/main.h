//
// fwnavgen/main.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef FWNAVGEN_MAIN_H
#define FWNAVGEN_MAIN_H

/*
	This file is meant to be used as a replacement for 'system/main.h', i.e. #included
	from the .cpp file that contains the Main() function. It either sets up the
	game heaps like usual, if -exportsectors or -exportall is present on the command line,
	or a stock allocator. The stock allocator may be preferable when doing the actual
	nav mesh processing, because you don't need to specify a maximum heap size up front,
	and performance will be the same as it used to be in older versions of the tool.
*/

#ifndef NO_INIT_GAME_HEAP
#define NO_INIT_GAME_HEAP
#endif

static void InitGameHeap();

#include "diag/tracker.h"
#include "system/main.h"
#include "system/stockallocator.h"
#include "system/xtl.h"

#if __WIN32PC

static bool sFindCommandLineArg(const char *s, char **ppValue = NULL)
{
	const char *parg = GetCommandLine();
	size_t len = strlen(s);

	// Look for the argument while we still have chars
	while (*parg)
	{
		// Do case-insensitive string compare for match.
		if (strnicmp(parg, s, len) != 0)
		{
			++parg;
			continue;
		}
		// We think we have a match, but it might be partial
		const char *p1 = parg + len;
		if ((*p1 != 0) && (*p1 != '=') && (*p1 != ' '))
		{
			++parg;
			continue;
		}
		// Ok, we really have a value
		if (ppValue)
		{
			if (*p1 == '=')
				*ppValue = (char *)(p1 + 1);
			else
				*ppValue = NULL;
		}
		// Whether argument or not, we found it
		return true;
	}
	// No match
	return false;
}

static bool sExportGeomMode()
{
	static bool s_bComputed = false;
	static bool s_bRetVal = false;

	if(!s_bComputed)
	{
		char *pValue;
		s_bRetVal = sFindCommandLineArg("exportall", &pValue) || sFindCommandLineArg("exportcontinuefrom") || sFindCommandLineArg("exportsectors") || sFindCommandLineArg("exportregion") || sFindCommandLineArg("exportdlc");
		s_bComputed = true;
	}

	return s_bRetVal;
}

#endif	// __WIN32PC


#ifdef SIMPLE_HEAP_SIZE
#undef SIMPLE_HEAP_SIZE
#define SIMPLE_HEAP_SIZE					(2048 * 1024)
#endif

#ifdef SIMPLE_PHYSICAL_SIZE
#undef SIMPLE_PHYSICAL_SIZE
#define SIMPLE_PHYSICAL_SIZE				(1024 * 1024)
#endif

static sysMemAllocator &sSetupHeaps()
{

#ifdef SIMPLE_HEAP_SIZE

	const size_t gameVirtHeapSize = (static_cast<size_t>(SIMPLE_HEAP_SIZE) << 10) - FRAG_HEAP_SIZE;

#if USE_SPARSE_MEMORY
	static sysMemSparseAllocator gameVirtualAllocator(gameVirtHeapSize, sysMemSimpleAllocator::HEAP_MAIN);

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// [FRAG CACHE]
	//
	void* pFragMemory = sysMemPhysicalAllocate(FRAG_HEAP_SIZE);
	sysMemManager::GetInstance().SetFragMemory(pFragMemory);
#else
	static sysMemSimpleAllocator gameVirtualAllocator(gameVirtHeapSize, sysMemSimpleAllocator::HEAP_MAIN);

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// [FRAG CACHE]
	//
	void* pFragMemory = sysMemPhysicalAllocate(FRAG_HEAP_SIZE);
	sysMemManager::GetInstance().SetFragMemory(pFragMemory);
#endif

	size_t resVirtHeapSize = static_cast<size_t>(SIMPLE_PHYSICAL_SIZE) << 10;

#if USE_SPARSE_MEMORY
	size_t resVirtWorkSize = COMPUTE_SPARSEBUDDYHEAP_WORKSPACE_SIZE(resVirtHeapSize / g_rscPhysicalLeafSize);
	resVirtWorkSize = (resVirtWorkSize + 1023) & ~1023;
	void* const resVirtWork = sysMemVirtualAllocate(resVirtWorkSize);
	static sysMemSparseBuddyAllocator physicalAllocator(resVirtHeapSize, g_rscPhysicalLeafSize, (resVirtHeapSize / g_rscPhysicalLeafSize), resVirtWork);

#elif RSG_PC
	static sysMemGrowBuddyAllocator physicalAllocator(g_rscPhysicalLeafSize, resVirtHeapSize);
#else
	size_t resVirtWorkSize = COMPUTE_BUDDYHEAP_WORKSPACE_SIZE(resVirtHeapSize / g_rscPhysicalLeafSize);	
	void* const resVirtWork = gameVirtualAllocator.Allocate(resVirtWorkSize, 16);
	void* const resVirtHeap = sysMemPhysicalAllocate(resVirtHeapSize);
	static sysMemBuddyAllocator physicalAllocator(resVirtHeap, g_rscPhysicalLeafSize, (resVirtHeapSize / g_rscPhysicalLeafSize), resVirtWork);
#endif


	static sysMemMultiAllocator theAllocator;
	theAllocator.AddAllocator(gameVirtualAllocator);

#if RSG_PC && defined(SIMPLE_VIRTUAL_SIZE) && (SIMPLE_VIRTUAL_SIZE > 0)
	static sysMemGrowBuddyAllocator resourceVirtualAllocator(g_rscVirtualLeafSize, (SIMPLE_VIRTUAL_SIZE) << 10);
	theAllocator.AddAllocator(resourceVirtualAllocator);
#else
	theAllocator.AddAllocator(physicalAllocator);
#endif

	theAllocator.AddAllocator(physicalAllocator);
	theAllocator.AddAllocator(physicalAllocator);

#if ENABLE_KNOWN_REFERENCES
	pgBase::SetTrackedHeap(*theAllocator.GetAllocator(MEMTYPE_RESOURCE_VIRTUAL));
#endif

	// If there's no debug heap, just point at the game allocator.
	theAllocator.AddAllocator(gameVirtualAllocator);

#else	// SIMPLE_HEAP_SIZE
	// This bit of terror prevents the stock allocator from ever being destroyed
	// on shutdown, so we are immune to object cleanup order.
	static char theAllocatorBuffer[sizeof(stockAllocator)];
	sysMemAllocator& theAllocator = *(::new (theAllocatorBuffer) stockAllocator);
#endif	// SIMPLE_HEAP_SIZE

	sysMemAllocator::SetCurrent(theAllocator);
	sysMemAllocator::SetMaster(theAllocator);
	sysMemAllocator::SetContainer(theAllocator);

	return theAllocator;
}



#define NAVGEN_SIMPLE_HEAP_SIZE			(1024 * 1024)	// 768 // originally 128 * 1024
#define NAVGEN_SIMPLE_PHYSICAL_SIZE		(1024 * 1024)	// 768 // originally 128 * 1024

static sysMemAllocator &sSetupHeapsOld()
{
	// Note: adapted from the InitGameHeap() function in 'main.h'. Would be nice to simplify
	// or refactor further, to reduce code duplication. /FF

	// SIMPLE_HEAP_SIZE
	// SIMPLE_PHYSICAL_SIZE

#ifdef USE_SPARSE_ALLOCATOR
	static sysMemSparseAllocator gameAllocator(NAVGEN_SIMPLE_HEAP_SIZE << 10, sysMemSimpleAllocator::HEAP_MAIN);
#else
	static sysMemSimpleAllocator gameAllocator(NAVGEN_SIMPLE_HEAP_SIZE << 10, sysMemSimpleAllocator::HEAP_MAIN);
#endif

	// Physical size is in kilobytes; page size is 8k.
	XENON_ONLY(simplePhysicalSize -= g_sysExtraMemory);

	size_t resVirtHeapSize = static_cast<size_t>(NAVGEN_SIMPLE_PHYSICAL_SIZE) << 10;
#if USE_SPARSE_MEMORY
	size_t resVirtWorkSize = COMPUTE_SPARSEBUDDYHEAP_WORKSPACE_SIZE(resVirtHeapSize / g_rscPhysicalLeafSize);
	resVirtWorkSize = (resVirtWorkSize + 1023) & ~1023;
	void* const resVirtWork = sysMemVirtualAllocate(resVirtWorkSize);
	static sysMemSparseBuddyAllocator physicalAllocator(resVirtHeapSize, g_rscPhysicalLeafSize, (resVirtHeapSize / g_rscPhysicalLeafSize), resVirtWork);
#else
	static sysMemGrowBuddyAllocator physicalAllocator(g_rscPhysicalLeafSize, resVirtHeapSize);
#endif // USE_SPARSE_BUDDY_ALLOCATOR

	static sysMemMultiAllocator theAllocator;
	theAllocator.AddAllocator(gameAllocator);
	theAllocator.AddAllocator(physicalAllocator);
	theAllocator.AddAllocator(physicalAllocator);
	theAllocator.AddAllocator(physicalAllocator);	// Add physical allocator thrice, there's no difference on !__PPU platforms.

#if ENABLE_KNOWN_REFERENCES
	// Tell game systems where the defragmentable heap lives.
	pgBase::SetTrackedHeap(*theAllocator.GetAllocator(MEMTYPE_RESOURCE_VIRTUAL));
#endif

#if ENABLE_DEBUG_HEAP
	void *debugHeapMemory = NULL;
	const u32 debugHeapSize = (DEBUG_HEAP_SIZE + (2*1024) ) << 10;

	debugHeapMemory = VirtualAlloc(NULL, debugHeapSize, MEM_COMMIT, PAGE_READWRITE);
	if (debugHeapMemory) {
		printf("[STARTUP] Allocating %uk debug heap...\n",debugHeapSize>>10);
		static sysMemSimpleAllocator debugAllocator(debugHeapMemory,debugHeapSize,sysMemSimpleAllocator::HEAP_DEBUG);
		theAllocator.AddAllocator(debugAllocator);
		g_sysHasDebugHeap = true;
	}
#endif	// ENABLE_DEBUG_HEAP

	return theAllocator;
}


static sysMemAllocator &sSetupStockAllocator()
{
	// This bit of terror prevents the stock allocator from ever being destroyed
	// on shutdown, so we are immune to object cleanup order.
	static char theAllocatorBuffer[sizeof(stockAllocator)];
	sysMemAllocator& theAllocator = *(::new (theAllocatorBuffer) stockAllocator);

	return theAllocator;
}


static void InitGameHeap()
{
	// Depending on if we're exporting level geometry or performing other nav mesh
	// build operations, we either set up the game heaps (so the regular data load code
	// from the game can work), or a stock allocator (which is what the old NavMeshMaker
	// tool used, so we don't have to specify an upper limit to the amount of memory we can
	// use).
	sysMemAllocator* theAllocator;

	if(sExportGeomMode())
	{
		theAllocator = &sSetupHeaps();
	}
	else
	{
		theAllocator = &sSetupStockAllocator();
	}

	sysMemAllocator::SetCurrent(*theAllocator);
	sysMemAllocator::SetMaster(*theAllocator);
	sysMemAllocator::SetContainer(*theAllocator);

#ifdef __MFC
	s_pTheAllocator = theAllocator;
#endif

	// JB: Here's a lot of stuff which I don't really understand, but without which
	// the navmesh generator will no longer run.  Copied from base\src\main.h

#if !__FINAL && __WIN32 && !__RESOURCECOMPILER // support for eeaddin
	if (sysBootManager::IsDebuggerPresent())
	{
#if __XENON
		void * const DEBUG_DATA_PTR = (void *)0x00070000;
#elif __WIN32PC
		void * const DEBUG_DATA_PTR = (void *)0x70070000;
#endif
		g_DebugData  =(size_t*)VirtualAlloc(DEBUG_DATA_PTR,4096,MEM_RESERVE,PAGE_READWRITE);
		if (!g_DebugData)
		{
			printf("Couldn't allocate space for debug data. 0x%x\n", GetLastError());
		}
		else
		{
			VirtualAlloc(DEBUG_DATA_PTR,4096,MEM_COMMIT,PAGE_READWRITE);
			g_DebugData[0] = MAKEFOURCC('d', '3', 'b', 'g');
			g_DebugData[1] = 2;
		}
	}
#endif

#if RAGE_TRACKING
	bool	memvisualizeEnabled = false;

	if (sysBootManager::IsDebuggerPresent())
	{
		LPSTR cmd = GetCommandLine();
		memvisualizeEnabled = strstr(cmd, "@memvisualize") != NULL;
	}

	if ( memvisualizeEnabled )
	{
		USE_DEBUG_MEMORY();
		++g_TrackerDepth;
		diagTracker* t = rage_new diagTrackerRemote2( false );
		sysMemAllocator& master = sysMemAllocator::GetMaster();
		t->InitHeap("Game Virtual", master.GetAllocator(MEMTYPE_GAME_VIRTUAL)->GetHeapBase(), master.GetAllocator(MEMTYPE_GAME_VIRTUAL)->GetHeapSize());
		t->InitHeap("Resource Virtual", master.GetAllocator(MEMTYPE_RESOURCE_VIRTUAL)->GetHeapBase(), master.GetAllocator(MEMTYPE_RESOURCE_VIRTUAL)->GetHeapSize());
#if __XENON
		if (g_pXenonPoolAllocator)
			t->InitHeap("Pool Allocator", g_pXenonPoolAllocator->GetHeapBase(), g_pXenonPoolAllocator->GetHeapSize());
#endif // __XENON
#if __PPU
		t->InitHeap("Flex Allocator", flexAllocator.GetHeapBase(), flexAllocator.GetHeapSize());
		// InitHeap() for g_pResidualAllocator will be performed on allocator creation, in grcDevice::InitClass
#endif
		// If there really is a distinct separate debug heap, add a tracker for it.
#if ENABLE_DEBUG_HEAP
		if (g_sysHasDebugHeap)
			t->InitHeap("Debug Heap", master.GetAllocator(MEMTYPE_DEBUG_VIRTUAL)->GetHeapBase(), master.GetAllocator(MEMTYPE_DEBUG_VIRTUAL)->GetHeapSize());
#endif
		t->Push("Tracker Send Buffer");
		t->Tally(t,sizeof(diagTrackerRemote2),0);
		t->Pop();
		diagTracker::SetCurrent(t);
		--g_TrackerDepth;

	}
#endif

	CHECKPOINT_SYSTEM_MEMORY;
}

#endif	// FWNAVGEN_MAIN_H

// End of file 'fwnavgen/main.h'
