#include "NavGen_Octree.h"
#include "NavGen.h"
#include "NavGen_Utils.h"
#include "NavGen_Store.h"
#include "ai/navmesh/navmeshextents.h"

#include <algorithm>

namespace rage
{

//*********************************************************************************
//
//	Title	: NavGen_Hierarchical.cpp
//	Author	: James Broad
//	Date	: Jan 2005
//
//	This file contains classes and functions whose purpose are to precalculate the
//	paths across all navmeshes, in order to provide a hierarchical top-layer to the
//	pathfinding.
//	Each navmesh is broken down into a set of significant extrances/exits along its
//	edge.  For each of these, a bit is set to indicate whether each one can get to
//	each of the others within that navmesh.  At runtime, this information allows us
//	to quickly find paths across the entire game map at a coarse level.
//
//*********************************************************************************

#define __OPTIMISE_NODES					1
#define __NEW_POSITION_CALCULATION			1
#define __LINK_NEARBY_NODES					0
#define __INSERT_NODES_TO_FOLLOW_TERRAIN	1
#define __ANALYSE_LINK_WIDTHS				1
#define __USE_BLOCKSIZE						1

#define MIN_AREA_POLY_WITH_ONE_LINK			5.0f
#define MAX_AREA_DURING_OPTIMIZE			1000.0f	//75.0f

u32 CNavGen::g_iHierarchicalNodeTimestamp = 1;

bool CanHierarchicalNodesBeMerged(TNavMeshPoly & polyNode1WasOn, TNavMeshPoly & polyNode2WasOn)
{
	if(polyNode1WasOn.GetIsWater() != polyNode2WasOn.GetIsWater())
		return false;
	if(polyNode1WasOn.GetIsPavement() != polyNode2WasOn.GetIsPavement())
		return false;
	if(polyNode1WasOn.GetPedDensity() != polyNode2WasOn.GetPedDensity())
		return false;
	if(polyNode1WasOn.GetIsInterior() != polyNode2WasOn.GetIsInterior())
		return false;
	if(polyNode1WasOn.GetNavMeshIndex() != polyNode2WasOn.GetNavMeshIndex())
		return false;

	return true;
}

bool CanMakeNodeOnPoly(CNavMesh * pNavMesh, TNavMeshPoly * pPoly)
{
	if(pPoly->GetIsIsolated())
		return false;

	const float fArea = pNavMesh->GetPolyArea(pNavMesh->GetPolyIndex(pPoly));
	if(fArea < MIN_AREA_POLY_WITH_ONE_LINK && pPoly->GetNumVertices()==1)
		return false;

	return true;
}

//**********************************************************************************
// Hierarchical nav nodes are combined into NxN blocks in order to be less wasteful
// on the small allocation memory heap, and also to aid streaming.

bool AreInSameBlock(const TNavMeshIndex iHierNav1, const TNavMeshIndex iHierNav2)
{
#if __USE_BLOCKSIZE
	const int iY1 = iHierNav1 / CPathServerExtents::m_iNumNavMeshesInY;
	const int iX1 = iHierNav1 - (iY1 * CPathServerExtents::m_iNumNavMeshesInY);
	const int iY2 = iHierNav2 / CPathServerExtents::m_iNumNavMeshesInY;
	const int iX2 = iHierNav2 - (iY2 * CPathServerExtents::m_iNumNavMeshesInY);

	const int iBlockX1 = iX1 / CPathServerExtents::m_iHierarchicalNodesBlockSize;
	const int iBlockY1 = iY1 / CPathServerExtents::m_iHierarchicalNodesBlockSize;
	const int iBlockX2 = iX2 / CPathServerExtents::m_iHierarchicalNodesBlockSize;
	const int iBlockY2 = iY2 / CPathServerExtents::m_iHierarchicalNodesBlockSize;

	return (iBlockX1==iBlockX2 && iBlockY1==iBlockY2);
#else
	return (iHierNav1==iHierNav2);
#endif
}

//*************************************************************************************
//	CreateHierarchicalNavData
//	This function loads all the map navmeshes, and creates a CHierarchicalNavData
//	for each one.  This is a compacted representation of the navmesh, which can be
//	streamed in at distance and rudimentary node-level pathfinding performed with it.
//
//	Every poly in the navmesh equates to a CHierarchicalNavNode in this class, and each
//	poly link is a CHierarchicalNavLink.
//	The CPathServer should handle the streaming & storing of these alongside CNavMesh's.
//

int CompareNodes(CHierarchicalNode * const * n1, CHierarchicalNode * const * n2)
{
	return (int)Sign((*n1)->m_fArea - (*n2)->m_fArea);
}


CHierarchicalNode * g_pOriginNode = NULL;

int CompareNearbyNodesDistance(CHierarchicalNode * const * n1, CHierarchicalNode * const * n2)
{
	const Vector3 vDiff1 = (*n1)->m_vPosition - g_pOriginNode->m_vPosition;
	const Vector3 vDiff2 = (*n2)->m_vPosition - g_pOriginNode->m_vPosition;

	return (int)Sign(vDiff1.Mag2() - vDiff2.Mag2());
}

bool IsLinked(CHierarchicalNode * pFromNode, CHierarchicalNode * pToNode)
{
	for(s32 l=0; l<pFromNode->m_Links.GetCount(); l++)
		if(pFromNode->m_Links[l].m_pNode==pToNode)
			return true;
	return false;
}
bool IsLinked(atArray<CHierarchicalLink> & linksList, CHierarchicalNode * pToNode)
{
	for(s32 l=0; l<linksList.GetCount(); l++)
		if(linksList[l].m_pNode==pToNode)
			return true;
	return false;
}
void Link(CHierarchicalNode * pFromNode, CHierarchicalNode * pToNode)
{
	Assert(!IsLinked(pFromNode, pToNode));
	Assert(pFromNode->m_Links.size()<MAX_NUM_HIERACHICAL_LINKS);
	CHierarchicalLink link;
	link.m_pNode = pToNode;
	link.m_iNavMeshIndex = pToNode->m_pNavMesh->GetIndexOfMesh();
	link.m_iPolyIndex = pToNode->m_pNavMesh->GetPolyIndex(pToNode->m_pPoly);
	pFromNode->m_Links.PushAndGrow(link);
}
void Unlink(CHierarchicalNode * pFromNode, CHierarchicalNode * pToNode)
{
	for(s32 l=0; l<pFromNode->m_Links.GetCount(); l++)
	{
		if(pFromNode->m_Links[l].m_pNode==pToNode)
		{
			pFromNode->m_Links.Delete(l);
			return;
		}
	}
}
int GetLinkIndex(atArray<CHierarchicalLink> & linksList, CHierarchicalNode * pToNode)
{
	for(s32 l=0; l<linksList.GetCount(); l++)
		if(linksList[l].m_pNode==pToNode)
			return l;
	return -1;
}
bool IsLinkedToAtAll(CHierarchicalNode * pNode, vector<CHierarchical*> & hierarchicals)
{
	for(u32 h=0; h<hierarchicals.size(); h++)
	{
		CHierarchical * pHierarchical = hierarchicals[h];
		for(s32 n=0; n<pHierarchical->m_Nodes.GetCount(); n++)
		{
			CHierarchicalNode * pOtherNode = pHierarchical->m_Nodes[n];
			if(pNode==pOtherNode)
				continue;

			for(s32 l=0; l<pOtherNode->m_Links.GetCount(); l++)
			{
				CHierarchicalLink link = pOtherNode->m_Links[l];
				if(link.m_pNode == pNode)
					return true;
			}
		}
	}
	return false;
}

void EnsureAllLinksWorkInBothDirections(CHierarchical * pHierarchical)
{
	atArray<CHierarchicalNode*> & nodes = pHierarchical->m_Nodes;

	for(int n=0; n<nodes.GetCount(); n++)
	{
		CHierarchicalNode * pNode = nodes[n];

		for(int l=0; l<pNode->m_Links.GetCount(); l++)
		{
			CHierarchicalLink & link = pNode->m_Links[l];
			Assert(link.m_pNode);
			Assert(IsLinked(link.m_pNode, pNode));

			if(!IsLinked(link.m_pNode, pNode) && link.m_pNode->m_Links.GetCount() < MAX_NUM_HIERACHICAL_LINKS)
			{
				// We need a link to go back from 'link.m_pNode' to pNode
				Link(link.m_pNode, pNode);
			}
		}
	}
}

static const u32 MAX_HIERARCHICAL_LINKS_DURING_OPTIMISATION = (MAX_NUM_HIERACHICAL_LINKS/2);

bool CNavGen::CreateHierarchicalNavData(char * pNavMeshesPath, char * pGameDataPath)
{
	char pathForNavDatFile[512];
	sprintf(pathForNavDatFile, "%s\\common\\data", pGameDataPath);

	/*
	CPathServer::ms_bLoadAllHierarchicalData = false;

	if(!CPathServer::Init(CPathServer::EMultiThreaded, 0, pNavMeshesPath, pathForNavDatFile, pNavMeshesPath))
	{
		return false;
	}
	*/

	if(!CNavGen_NavMeshStore::LoadAll(pNavMeshesPath))
	{
		return false;
	}

	s32 x,y,n,l;
	u32 p,v,h;
	Vector3 vPolyCentroid;

	DEV_ONLY(char dbgTxt[256];);

	//*******************************************************************************************
	//	Create the initial node network.  There is a node at the centre of every navmesh poly.

	vector<CHierarchical*> hierarchical;

	for(y=0; y<CPathServerExtents::m_iNumNavMeshesInY; y++)
	{
		for(x=0; x<CPathServerExtents::m_iNumNavMeshesInX; x++)
		{
			const u32 iIndex = (y*CPathServerExtents::m_iNumNavMeshesInX)+x;
			CNavMesh * pNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(iIndex);

			if(pNavMesh)
			{
				CHierarchical * pHierarchical = rage_new CHierarchical();
				hierarchical.push_back(pHierarchical);

				Printf("Creating hierarchical %i for NavMesh[%i,%i].\n", hierarchical.size(), x*CPathServerExtents::m_iNumSectorsPerNavMesh, y*CPathServerExtents::m_iNumSectorsPerNavMesh);

				pHierarchical->m_pNavMesh = pNavMesh;

				for(p=0; p<pNavMesh->m_iNumPolys; p++)
				{
					TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);

					if(CanMakeNodeOnPoly(pNavMesh, pPoly))
					{
						CHierarchicalNode * pNode = rage_new CHierarchicalNode();
						pNode->m_pPoly = pPoly;

						pNavMesh->GetPolyCentroid(p, vPolyCentroid);
						pNode->m_vPosition = vPolyCentroid;
						pNode->m_pNavMesh = pNavMesh;
						pNode->m_fArea = pNavMesh->GetPolyArea(p);
						pNode->m_iEdgeFlags = CNavMesh::GetPolyEdgeFlags(pNavMesh, p);
						pNode->m_bOnEdgeOfMesh = (pNode->m_iEdgeFlags != 0);

						for(v=0; v<pPoly->GetNumVertices(); v++)
						{
							const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly(pPoly->GetFirstVertexIndex()+v);
							const u32 iNavMesh = adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes());

							if(adjPoly.GetAdjacencyType()==ADJACENCY_TYPE_NORMAL && iNavMesh!=NAVMESH_NAVMESH_INDEX_NONE)
							{
								CNavMesh * pAdjNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(iNavMesh);
								if(pAdjNavMesh)
								{
									TNavMeshPoly * pAdjPoly = pAdjNavMesh->GetPoly( adjPoly.GetPolyIndex() );

									if(CanMakeNodeOnPoly(pAdjNavMesh, pAdjPoly) && pNode->m_Links.GetCount() < MAX_NUM_HIERACHICAL_LINKS)
									{
										CHierarchicalLink link;
										link.m_iNavMeshIndex = iNavMesh;
										link.m_iPolyIndex = adjPoly.GetPolyIndex();
										link.m_pNode = NULL;

										Assert(pNode->m_Links.size()<MAX_NUM_HIERACHICAL_LINKS);
										pNode->m_Links.PushAndGrow(link);
									}
								}
							}
						}

						if(pNode->m_Links.GetCount())
						{
							pHierarchical->m_Nodes.PushAndGrow(pNode);
							pPoly->m_pHierarchicalNode = pNode;
						}
						else
						{
							delete pNode;
						}
					}
				}

				pHierarchical->m_iNumNodesActive = pHierarchical->m_Nodes.GetCount();
			}
		}
	}

	//******************************************************************
	//	Link up nodes' links, so we get a fully connected network

	for(h=0; h<hierarchical.size(); h++)
	{
		CHierarchical * pHierarchical = hierarchical[h];

		for(n=0; n<pHierarchical->m_Nodes.GetCount(); n++)
		{
			CHierarchicalNode * pNode = pHierarchical->m_Nodes[n];
			Assert(pNode->m_Links.GetCount() > 0);

			for(l=0; l<pNode->m_Links.GetCount(); l++)
			{
				CHierarchicalLink & link = pNode->m_Links[l];
				CNavMesh * pLinkedNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(link.m_iNavMeshIndex);
				if(pLinkedNavMesh)
				{
					TNavMeshPoly * pLinkedPoly = pLinkedNavMesh->GetPoly(link.m_iPolyIndex);
					Assert(pLinkedPoly->m_pHierarchicalNode);
					if(!IsLinked(pNode, pLinkedPoly->m_pHierarchicalNode))
					{
						link.m_pNode = pLinkedPoly->m_pHierarchicalNode;
					}
					else
					{
						pNode->m_Links.Delete(l);
						l--;
					}
				}
			}
		}
	}

	//*****************************************************
	//	Ensure that all links work in both directions!

	for(h=0; h<hierarchical.size(); h++)
	{
		CHierarchical * pHierarchical = hierarchical[h];
		atArray<CHierarchicalNode*> & nodes = pHierarchical->m_Nodes;

		for(n=0; n<nodes.GetCount(); n++)
		{
			CHierarchicalNode * pNode = nodes[n];

			for(l=0; l<pNode->m_Links.GetCount(); l++)
			{
				CHierarchicalLink & link = pNode->m_Links[l];
				Assert(link.m_pNode);
				Assert(IsLinked(link.m_pNode, pNode));

				if(!IsLinked(link.m_pNode, pNode))
				{
					// We need a link to go back from 'link.m_pNode' to pNode
					if(link.m_pNode->m_Links.size()<MAX_NUM_HIERACHICAL_LINKS)
					{
						Link(link.m_pNode, pNode);
					}
					// And if we haven't got enough links to spare on the node - then remove the link to this node!
					// Better to be missing a link, than to have a one-way link which may fuck things up later..
					else
					{
						pNode->m_Links.Delete(l);
						l--;
					}
				}
			}
		}
	}

	//***************************************************************************
	//	Now do the optimisation work which will allow this to be a significantly
	//	smaller pathfinding solution than the full navmesh.

#if __OPTIMISE_NODES

	const Vector3 vRaise(0.0f,0.0f,1.0f);
	static const float fMaximumArea = MAX_AREA_DURING_OPTIMIZE;
	static const float fMaximumDistanceBetweenNodesSqr = (20.0f * 20.0f);

	for(h=0; h<hierarchical.size(); h++)
	{
		CHierarchical * pHierarchical = hierarchical[h];
		atArray<CHierarchicalNode*> & nodes = pHierarchical->m_Nodes;
		const int iInitialNumNodes = nodes.GetCount();

		bool bOptimisationComplete = false;
		while(!bOptimisationComplete)
		{
ReSort:
			// Sort nodes in ascending order of size, so that the smallest are first
			nodes.QSort(0, -1, CompareNodes);

			EnsureAllLinksWorkInBothDirections(pHierarchical);

			// Go through nodes from smallest first, and try to pair a node with its smallest
			// neighbour - only where surface types are compatible.
			for(n=0; n<nodes.GetCount(); n++)
			{
				CHierarchicalNode * pNode = nodes[n];

				float fSmallestArea = FLT_MAX;
				CHierarchicalNode * pSmallestAreaNode = NULL;
				int iSmallestArea = -1;

				for(l=0; l<pNode->m_Links.GetCount(); l++)
				{
					CHierarchicalLink & link = pNode->m_Links[l];
					Assert(link.m_pNode);
					if(!link.m_pNode)
						continue;

					CHierarchicalNode * pNode2 = link.m_pNode;

					const bool bIsEdge = pNode->m_bOnEdgeOfMesh || pNode2->m_bOnEdgeOfMesh;
					const bool bCanMergeEdge = bIsEdge && (pNode->m_iEdgeFlags == pNode2->m_iEdgeFlags);

					if(AreInSameBlock(pNode->m_pNavMesh->m_iIndexOfMesh, pNode2->m_pNavMesh->m_iIndexOfMesh) &&
						//(pNode->m_pNavMesh == pNode2->m_pNavMesh) &&
						(!bIsEdge || bCanMergeEdge) &&
						(pNode2->m_fArea < fSmallestArea) &&
						(pNode->m_fArea + pNode2->m_fArea < fMaximumArea) &&
						CanHierarchicalNodesBeMerged(*pNode->m_pPoly, *pNode2->m_pPoly))
					{
						fSmallestArea = pNode2->m_fArea;
						pSmallestAreaNode = pNode2;
						iSmallestArea = l;
					}
				}
				if(pSmallestAreaNode)
				{
					//*********************************************************************************
					// pNode will now become merged with pSmallestAreaNode, and will inherit all of its
					// (unique) linked nodes.  We need to check whether the new position for the node
					// is valid - we need to make sure that there is a navmesh line-of-sight from all
					// the surrounding nodes to the new position.

					Assert(pNode->m_pNavMesh == pSmallestAreaNode->m_pNavMesh);

#if __NEW_POSITION_CALCULATION
					Vector3 vNewPosition;
					const float fCombinedArea = pNode->m_fArea + pSmallestAreaNode->m_fArea;
					if(fCombinedArea > 0.0f)
					{
						// NB: This could be off the navmesh!  We could have two irregular polygons, where the linear
						// interpolation of their positions takes us off the polys.
						// It may be better to interpolate from vCentroid1->vMidedge->vCentroid2 and work out some funky
						// function for how this would happen
						vNewPosition =
							(pNode->m_vPosition * (pNode->m_fArea / fCombinedArea)) +
							(pSmallestAreaNode->m_vPosition * (pSmallestAreaNode->m_fArea / fCombinedArea));
					}
					else
					{
						vNewPosition = (pNode->m_fArea > pSmallestAreaNode->m_fArea) ? pNode->m_vPosition : pSmallestAreaNode->m_vPosition;
					}
#else
					const Vector3 vNewPosition = (pNode->m_fArea > pSmallestAreaNode->m_fArea) ? pNode->m_vPosition : pSmallestAreaNode->m_vPosition;
#endif

					CHierarchicalNode * pParentNodes[2] = { pNode, pSmallestAreaNode };
					bool bLosOk = true;
					bool bNodeDistanceOk = true;
					for(s32 p=0; p<2 && bLosOk; p++)
					{
						CHierarchicalNode * pParentNode = pParentNodes[p];
						for(l=0; l<pParentNode->m_Links.GetCount(); l++)
						{
							CHierarchicalNode * pLinkedNode = pParentNode->m_Links[l].m_pNode;
							if(!pLinkedNode)
								continue;
							if(pLinkedNode==pNode || pLinkedNode==pSmallestAreaNode)
								continue;

							const Vector3 vBetweenNodes = pLinkedNode->m_vPosition - vNewPosition;
							if(vBetweenNodes.Mag2() > fMaximumDistanceBetweenNodesSqr)
							{
								bNodeDistanceOk = false;
								break;
							}
							if(!CNavGen_NavMeshStore::TestNavMeshLOS(pLinkedNode->m_vPosition + vRaise, vNewPosition + vRaise))
							{
								bLosOk = false;
								break;
							}
						}
					}

					if(bLosOk && bNodeDistanceOk)
					{
						vector<CHierarchicalLink> newLinks;

						// Accumulate list of unique links, not including the links to each other
						for(l=0; l<pSmallestAreaNode->m_Links.GetCount(); l++)
						{
							CHierarchicalLink & link = pSmallestAreaNode->m_Links[l];
							Assert(link.m_pNode);
							Assert(IsLinked(link.m_pNode, pSmallestAreaNode));

							if(link.m_pNode!=pNode)
								newLinks.push_back(link);
						}
						// Accumulate list of unique links, not including the links to each other
						for(l=0; l<pNode->m_Links.GetCount(); l++)
						{
							CHierarchicalLink & link = pNode->m_Links[l];
							Assert(link.m_pNode);
							Assert(IsLinked(link.m_pNode, pNode));

							if(link.m_pNode!=pSmallestAreaNode && !IsLinked(pSmallestAreaNode, link.m_pNode))
							{
								newLinks.push_back(link);
							}
						}

						if(newLinks.size()==0 || newLinks.size() > MAX_HIERARCHICAL_LINKS_DURING_OPTIMISATION)
						{

						}
						else
						{
							#if __DEV
							sprintf(dbgTxt, "Merging node %p (area %.1f) and node %p (area %.1f).  Num active nodes = %i/%i\n", pNode, pNode->m_fArea, pSmallestAreaNode, pSmallestAreaNode->m_fArea, pHierarchical->m_iNumNodesActive, iInitialNumNodes);
							OutputDebugString(dbgTxt);
							#endif

							pNode->m_fArea += pSmallestAreaNode->m_fArea;
							pNode->m_vPosition = vNewPosition;
							pNode->m_bOnEdgeOfMesh = (pNode->m_bOnEdgeOfMesh || pSmallestAreaNode->m_bOnEdgeOfMesh);
							//pNode->m_iEdgeFlags |= pSmallestAreaNode->m_iEdgeFlags;

							pHierarchical->m_iNumNodesActive--;

							pNode->m_Links.Reset();

							// Add all the new links to the pNode
							for(l=0; l<(int)newLinks.size(); l++)
							{
								pNode->m_Links.PushAndGrow(newLinks[l]);

								// Ensure that all the new links are reciprocal
								if(!IsLinked(newLinks[l].m_pNode, pNode))
								{
									Link(newLinks[l].m_pNode, pNode);
								}
							}

							// Remove all links in all CHeirarchicals which refer to the removed node pSmallestAreaNode
							for(u32 rH=0; rH<hierarchical.size(); rH++)
							{
								CHierarchical * pTestHeir = hierarchical[rH];
								if(pTestHeir==pHierarchical || CNavGen_NavMeshStore::GetDoNavMeshesAdjoin(pHierarchical->m_pNavMesh->m_iIndexOfMesh, pTestHeir->m_pNavMesh->m_iIndexOfMesh))
								{
									for(int nH=0; nH<pTestHeir->m_Nodes.GetCount(); nH++)
									{
										CHierarchicalNode * pNode = pTestHeir->m_Nodes[nH];
										Unlink(pNode, pSmallestAreaNode);

										if(pNode==pSmallestAreaNode)
										{
											pTestHeir->m_Nodes.Delete(nH);
											nH--;
										}
									}
								}
							}

							delete pSmallestAreaNode;

							// Break out of the loop, so we go back to the QSort()
							goto ReSort;
						}
					}
				}
			}

			// If we get this far, then we found no further suitable nodes to merge
			bOptimisationComplete = true;
		}

		int iX,iY;
		CNavGen_NavMeshStore::GetSectorFromNavMeshIndex(pHierarchical->m_pNavMesh->m_iIndexOfMesh,iX,iY);

		Printf("NavMesh[%i,%i] Num Hier-Nodes : %i/%i\n", iX, iY, pHierarchical->m_iNumNodesActive, iInitialNumNodes);
	}

	//*********************************************************
	//	Ensure that all links work in both directions! (again)

	for(h=0; h<hierarchical.size(); h++)
	{
		CHierarchical * pHierarchical = hierarchical[h];
		EnsureAllLinksWorkInBothDirections(pHierarchical);
	}

#endif	// __OPTIMISE_NODES


#if __INSERT_NODES_TO_FOLLOW_TERRAIN

	for(h=0; h<hierarchical.size(); h++)
	{
		CHierarchical * pHierarchical = hierarchical[h];

		int iX,iY;
		CNavGen_NavMeshStore::GetSectorFromNavMeshIndex(pHierarchical->m_pNavMesh->m_iIndexOfMesh,iX,iY);

		Printf("Inserting Nodes for NavMesh[%i,%i] (h==%i).  Initial Nodes : %i\n", iX, iY, h, pHierarchical->m_Nodes.GetCount());

		InsertHierarchicalNodesToFollowTerrain(pHierarchical);

		Printf("NavMesh[%i,%i].  Final Nodes : %i\n", iX, iY, pHierarchical->m_Nodes.GetCount());
	}

#endif	// __INSERT_NODES_TO_FOLLOW_TERRAIN


#if __LINK_NEARBY_NODES

	//************************************************************************************************************
	// Now go through the nodes and see if there are any nearby which are unlinked, and are suitable for linking

 	for(h=0; h<hierarchical.size(); h++)
	{
		CHierarchical * pHierarchical = hierarchical[h];
		atArray<CHierarchicalNode*> & nodes = pHierarchical->m_Nodes;

		atArray<CHierarchicalNode*> nearbyNodes;
		static const float fMaxDist = 30.0f;	//100.0f;	//20.0f;

		for(s32 n=0; n<nodes.GetCount(); n++)
		{
			CHierarchicalNode * pOriginNode = nodes[n];
			if(pOriginNode->m_bAddedToFollowTerrain)
				continue;

			nearbyNodes.Reset();
			nearbyNodes.Reserve(MAX_NUM_HIERACHICAL_LINKS);

//			static bool bPause = false;
//			static Vector3 vTest(-819.5f, 1019.9f, 6.9f);
//			if(vTest.IsClose(pOriginNode->m_vPosition, 1.0f))
//				bPause = true;

			g_iHierarchicalNodeTimestamp++;

			GetClosestNodes(pOriginNode, pOriginNode, nearbyNodes, MAX_NUM_HIERACHICAL_LINKS, fMaxDist);

			// Sort nodes in ascending order of distance from pOriginNode
			g_pOriginNode = pOriginNode;
			nearbyNodes.QSort(0, -1, CompareNearbyNodesDistance);

			for(s32 c=0; c<nearbyNodes.GetCount(); c++)
			{
				CHierarchicalNode * pNearbyNode = nearbyNodes[c];

				Assert(!IsLinked(pOriginNode, pNearbyNode));
				Assert(!IsLinked(pNearbyNode, pOriginNode));

				if(CPathServer::m_PathServerThread.TestNavMeshLOS(pOriginNode->m_vPosition + vRaise, pNearbyNode->m_vPosition + vRaise, 0))
				{
					// Is there space for the link AND the link back?
					if(pOriginNode->m_Links.GetCount() < MAX_NUM_HIERACHICAL_LINKS && pNearbyNode->m_Links.GetCount() < MAX_NUM_HIERACHICAL_LINKS)
					{
						Link(pOriginNode, pNearbyNode);
						Link(pNearbyNode, pOriginNode);
					}
				}
			}
		}
	}

#endif	// __LINK_NEARBY_NODES

	//************************************************************************************
	// Remove all links which join to an invalid navmesh index
	// This is a safeguard for what I suspect is an error causes by bad assets after
	// a Rage update which broke the resource format.  Can take this out later hopefully.

 	for(h=0; h<hierarchical.size(); h++)
	{
		CHierarchical * pHierarchical = hierarchical[h];
		atArray<CHierarchicalNode*> & nodes = pHierarchical->m_Nodes;

		for(n=0; n<nodes.size(); n++)
		{
			CHierarchicalNode * pNode = nodes[n];

			for(l=0; l<pNode->m_Links.size(); l++)
			{
				const CHierarchicalLink & link = pNode->m_Links[l];
				if(link.m_iNavMeshIndex > NAVMESH_MAX_MAP_INDEX)
				{
					pNode->m_Links.Delete(l);
					l--;
				}
			}
		}
	}

#if __ANALYSE_LINK_WIDTHS

 	for(h=0; h<hierarchical.size(); h++)
	{
		CHierarchical * pHierarchical = hierarchical[h];
		atArray<CHierarchicalNode*> & nodes = pHierarchical->m_Nodes;

		for(n=0; n<nodes.size(); n++)
		{
			CHierarchicalNode * pNode = nodes[n];

			for(l=0; l<pNode->m_Links.size(); l++)
			{
				CHierarchicalLink & link = pNode->m_Links[l];
				AnalyseLinkWidth(pNode, &link);
			}
		}
	}

#endif

	//****************************************
	// Save out all the hierarchical nav data

	SaveHierarchicalNavData(pNavMeshesPath, hierarchical);

	for(h=0; h<hierarchical.size(); h++)
	{
		CHierarchical * pHier = hierarchical[h];
		delete pHier;
	}

	return true;
}


void CNavGen::GetClosestNodes(CHierarchicalNode * pOriginNode, CHierarchicalNode * pCurrentNode, atArray<CHierarchicalNode*> & closestNodes, const int iMaxNum, const float fMaxDist)
{
	if(closestNodes.GetCount() >= iMaxNum)
		return;

	Assert(pCurrentNode->m_iTimestamp != g_iHierarchicalNodeTimestamp);
	if(pCurrentNode->m_iTimestamp != g_iHierarchicalNodeTimestamp)
	{
		pCurrentNode->m_iTimestamp = g_iHierarchicalNodeTimestamp;

		if(pCurrentNode != pOriginNode &&
			!pCurrentNode->m_bAddedToFollowTerrain &&
			!IsLinked(pOriginNode, pCurrentNode) &&
			!IsLinked(pCurrentNode, pOriginNode) &&
			pCurrentNode->m_Links.GetCount() > 0)
		{
			const Vector3 vDiff = pCurrentNode->m_vPosition - pOriginNode->m_vPosition;
			const float fThisDistSqr = vDiff.Mag2();
			if(fThisDistSqr < fMaxDist*fMaxDist)
			{
				closestNodes.PushAndGrow(pCurrentNode);
			}
		}

		// We only add nodes within fMaxDist - however we may visit nodes up to 4x a greater distance.
		const float fSearchDistSqr = (fMaxDist*4.0f)*(fMaxDist*4.0f);

		for(int l=0; l<pCurrentNode->m_Links.GetCount(); l++)
		{
			CHierarchicalLink link = pCurrentNode->m_Links[l];
			if(link.m_pNode)
			{
				if(link.m_pNode->m_iTimestamp != g_iHierarchicalNodeTimestamp)
				{
					Vector3 vDiff = link.m_pNode->m_vPosition - pOriginNode->m_vPosition;
					if(vDiff.Mag2() < fSearchDistSqr)
					{
						GetClosestNodes(pOriginNode, link.m_pNode, closestNodes, iMaxNum, fMaxDist);
					}
				}
			}
		}
	}
}

#if __USE_BLOCKSIZE

bool CNavGen::SaveHierarchicalNavData(const char * pSavePath, vector<CHierarchical*> & hierarchical)
{
	s32 l;
	u32 n;
	int x,y;

	vector<CHierarchicalBlock*> blocks;

	for(y=0; y<CPathServerExtents::m_iNumNavMeshesInY; y+=CPathServerExtents::m_iHierarchicalNodesBlockSize)
	{
		for(x=0; x<CPathServerExtents::m_iNumNavMeshesInX; x+=CPathServerExtents::m_iHierarchicalNodesBlockSize)
		{
			//*******************************************************************
			// Build a list of the hierarchical navs which belong in this block.

			vector<CHierarchical*> hierarchicalsInThisBlock;
			Vector3 vBlockMin(FLT_MAX, FLT_MAX, FLT_MAX);
			Vector3 vBlockMax(-FLT_MAX, -FLT_MAX, -FLT_MAX);

			int iBX, iBY;
			for(iBY=0; iBY<CPathServerExtents::m_iHierarchicalNodesBlockSize; iBY++)
			{
				for(iBX=0; iBX<CPathServerExtents::m_iHierarchicalNodesBlockSize; iBX++)
				{
					int iBlockX = x + iBX;
					int iBlockY = y + iBY;

					TNavMeshIndex iNavMesh = (iBlockY * CPathServerExtents::m_iNumNavMeshesInX) + iBlockX;

					for(u32 h=0; h<hierarchical.size(); h++)
					{
						CHierarchical * pHier = hierarchical[h];
						if(pHier->m_pNavMesh->m_iIndexOfMesh == iNavMesh)
						{
							hierarchicalsInThisBlock.push_back(pHier);

							vBlockMin.x = Min(vBlockMin.x, pHier->m_pNavMesh->m_pQuadTree->m_Mins.x);
							vBlockMin.y = Min(vBlockMin.y, pHier->m_pNavMesh->m_pQuadTree->m_Mins.y);
							vBlockMax.x = Max(vBlockMax.x, pHier->m_pNavMesh->m_pQuadTree->m_Maxs.x);
							vBlockMax.y = Max(vBlockMax.y, pHier->m_pNavMesh->m_pQuadTree->m_Maxs.y);
							vBlockMin.z = Min(vBlockMin.z, pHier->m_pNavMesh->m_pQuadTree->m_Mins.z);
							vBlockMax.z = Max(vBlockMax.z, pHier->m_pNavMesh->m_pQuadTree->m_Maxs.z);
						}
					}
				}
			}

			Vector3 vBlockSize = vBlockMax - vBlockMin;

			//***************************************************
			// Construct the in-game hierarchical nav for (x,y)
			
			CHierarchicalBlock * pBlock = new CHierarchicalBlock();

			pBlock->m_iStartX = x;
			pBlock->m_iStartY = y;
			pBlock->m_pHierarchical = new CHierarchicalNavData();
			pBlock->m_pHierarchical->m_iNumNodes = 0;
			pBlock->m_vBlockMin = vBlockMin;
			pBlock->m_vBlockMax = vBlockMax;

			//******************************
			// Get a list of all the nodes

			vector<CHierarchicalNode*> & nodes = pBlock->m_Nodes;
			for(u32 h=0; h<hierarchicalsInThisBlock.size(); h++)
			{
				CHierarchical * pHier = hierarchicalsInThisBlock[h];
				for(int n=0; n<pHier->m_Nodes.size(); n++)
				{
					if(pHier->m_Nodes[n]->m_Links.GetCount()==0 || pHier->m_Nodes[n]->m_Links.GetCount()>MAX_NUM_HIERACHICAL_LINKS)
					{

					}
					else
					{
						pBlock->m_pHierarchical->m_iNumLinks += pHier->m_Nodes[n]->m_Links.GetCount();
						pHier->m_Nodes[n]->m_iIndexOfThisNode = pBlock->m_pHierarchical->m_iNumNodes++;
						nodes.push_back(pHier->m_Nodes[n]);
					}
				}
			}

			blocks.push_back(pBlock);
		}
	}

	for(u32 b=0; b<blocks.size(); b++)
	{
		CHierarchicalBlock * pBlock = blocks[b];
		CHierarchicalNavData * pNavData = pBlock->m_pHierarchical;
		vector<CHierarchicalNode*> & nodes = pBlock->m_Nodes;
		const Vector3 & vBlockMin = pBlock->m_vBlockMin;
		const Vector3 & vBlockMax = pBlock->m_vBlockMax;
		const Vector3 vBlockSize = vBlockMax - vBlockMin;

		pNavData->m_vMins = vBlockMin;
		pNavData->m_vMaxs = vBlockMax;
		pNavData->m_vSize = vBlockSize;

		if(pNavData->m_iNumNodes)
		{
			pNavData->m_pNodes = rage_new CHierarchicalNavNode[pNavData->m_iNumNodes];
			ZeroMemory(pNavData->m_pNodes, sizeof(CHierarchicalNavNode) * pNavData->m_iNumNodes);

			pNavData->m_pLinks = rage_new CHierarchicalNavLink[pNavData->m_iNumLinks];
			ZeroMemory(pNavData->m_pLinks, sizeof(CHierarchicalNavLink) * pNavData->m_iNumLinks);


			int iLinkPtr = 0;
			int iNodeCounter = 0;

			for(n=0; n<nodes.size(); n++)
			{
				CHierarchicalNode * pNode = nodes[n];
				CHierarchicalNavNode & node = pNavData->m_pNodes[iNodeCounter++];

				pNode->m_vPosition.z = Clamp(pNode->m_vPosition.z, vBlockMin.z, vBlockMax.z);

				node.m_Struct1.m_iX = CompressFixed8(pNode->m_vPosition.x, vBlockMin.x, vBlockSize.x);
				node.m_Struct1.m_iY = CompressFixed8(pNode->m_vPosition.y, vBlockMin.y, vBlockSize.y);
				node.m_Struct1.m_iZ = CompressFixed12(pNode->m_vPosition.z, vBlockMin.z, vBlockSize.z);

				int iNumLinks = pNode->m_Links.GetCount();
				Assert(iNumLinks > 0 && iNumLinks <= MAX_NUM_HIERACHICAL_LINKS);

				node.m_Struct3.m_iNumLinks = MAX(0, iNumLinks-1);
				node.m_Struct2.m_iStartOfLinkData = iLinkPtr;
				node.m_Struct2.m_iNavMeshIndex = CNavGen_NavMeshStore::NavMeshIndexToNavNodesIndex(pNode->m_pNavMesh->m_iIndexOfMesh);

				// Keep within maximum node area
				node.m_Struct3.m_iAreaRepresented = MIN((int)pNode->m_fArea, CHierarchicalNavNode::ms_iMaxAreaRepresented);
				// Impose an arbirary minimum limit to the area size to prevent the pedgen from spamming the same nodes repeatedly.
				// 8x8 = 64m sq
				node.m_Struct3.m_iAreaRepresented = MAX(node.m_Struct3.m_iAreaRepresented, CHierarchicalNavNode::ms_iMinAreaRepresented);

				Assert(pNode->m_pPoly);
				node.m_Struct3.m_iPedDensity = MIN(pNode->m_pPoly->GetPedDensity(), CHierarchicalNavNode::ms_iMaxPedDensity);

				// Set these initially 
				node.m_Struct4.m_iParentNavMeshIndex = NAVMESH_NAVMESH_INDEX_NONE;
				node.m_Struct4.m_iParentNodeIndex = NAVMESH_NODE_INDEX_NONE;

				node.SetPedSpawningEnabled(true);

				for(l=0; l<pNode->m_Links.GetCount(); l++)
				{
					CHierarchicalLink & srcLink = pNode->m_Links[l];
					CHierarchicalNavLink & dstLink = pNavData->m_pLinks[iLinkPtr];

					const TNavMeshIndex iNodesIndex = CNavGen_NavMeshStore::NavMeshIndexToNavNodesIndex(srcLink.m_iNavMeshIndex);
					dstLink.SetNavMeshIndex(srcLink.m_iNavMeshIndex==NAVMESH_NAVMESH_INDEX_NONE ? NAVMESH_NAVMESH_INDEX_NONE : iNodesIndex);

					Assert(srcLink.m_pNode->m_iIndexOfThisNode != -1);
					dstLink.SetNodeIndex(srcLink.m_pNode->m_iIndexOfThisNode);

					dstLink.SetWidthToLeft((int) Min(srcLink.m_fWidthToLeft, HIERARCHICAL_LINK_MAX_WIDTH) );
					dstLink.SetWidthToRight((int) Min(srcLink.m_fWidthToRight, HIERARCHICAL_LINK_MAX_WIDTH) );

					iLinkPtr++;
				}
			}

			const s32 iMem = pNavData->CalcSize();

			int iSectorX = pBlock->m_iStartX * CPathServerExtents::m_iNumSectorsPerNavMesh;
			int iSectorY = pBlock->m_iStartY * CPathServerExtents::m_iNumSectorsPerNavMesh;

			printf("NavMesh [%i][%i] CHeirarchicalNavData = %ik\n", iSectorX, iSectorY, iMem/1024);

			char filename[256];
			sprintf(filename, "%s/%i_%i.ihn", pSavePath, iSectorX, iSectorY);
			CHierarchicalNavData::Save(filename, pNavData);
		}

		delete pNavData;
		delete pBlock;
	}

	return true;
}

#else // __USE_BLOCKSIZE

bool CNavGen::SaveHierarchicalNavData(const char * pSavePath, vector<CHierarchical*> & hierarchical)
{
	s32 n,l;
	u32 h;

	for(h=0; h<hierarchical.size(); h++)
	{
		CHierarchical * pHierarchical = hierarchical[h];
		CNavMesh * pNavMesh = pHierarchical->m_pNavMesh;

		if(pHierarchical->m_Nodes.GetCount())
		{
			pHierarchical->m_pInGameNavData = rage_new CHierarchicalNavData();

			pHierarchical->m_pInGameNavData->m_iNumNodes = pHierarchical->m_iNumNodesActive;
			pHierarchical->m_pInGameNavData->m_iNumLinks = 0;

			pHierarchical->m_pInGameNavData->m_vMins = pNavMesh->m_pQuadTree->m_Mins;
			pHierarchical->m_pInGameNavData->m_vMaxs = pNavMesh->m_pQuadTree->m_Maxs;
			pHierarchical->m_pInGameNavData->m_vSize = pNavMesh->m_pQuadTree->m_Maxs - pNavMesh->m_pQuadTree->m_Mins;
			
			// Count links, and assign node indices
			int iNodeCount = 0;
			for(n=0; n<pHierarchical->m_Nodes.GetCount(); n++)
			{
				CHierarchicalNode * pNode = pHierarchical->m_Nodes[n];
				Assert(pNode->m_Links.GetCount() > 0 && pNode->m_Links.GetCount() <= MAX_NUM_HIERACHICAL_LINKS);

				if(pNode->m_Links.GetCount()==0 || pNode->m_Links.GetCount()>MAX_NUM_HIERACHICAL_LINKS)
				{
					delete pNode;
					pHierarchical->m_Nodes.Delete(n);
					n--;
				}
				else
				{
					pNode->m_iIndexOfThisNode = iNodeCount++;
					pHierarchical->m_pInGameNavData->m_iNumLinks += pNode->m_Links.GetCount();
				}
			}

			Assert(pHierarchical->m_pInGameNavData->m_iNumNodes == (u32)iNodeCount);
		}
	}

	for(h=0; h<hierarchical.size(); h++)
	{
		CHierarchical * pHierarchical = hierarchical[h];
		CNavMesh * pNavMesh = pHierarchical->m_pNavMesh;
		CHierarchicalNavData * pHierNavData = pHierarchical->m_pInGameNavData;

		if(pHierNavData)
		{
			pHierNavData->m_pNodes = rage_new CHierarchicalNavNode[pHierNavData->m_iNumNodes];
			ZeroMemory(pHierNavData->m_pNodes, sizeof(CHierarchicalNavNode) * pHierNavData->m_iNumNodes);

			pHierNavData->m_pLinks = rage_new CHierarchicalNavLink[pHierNavData->m_iNumLinks];
			ZeroMemory(pHierNavData->m_pLinks, sizeof(CHierarchicalNavLink) * pHierNavData->m_iNumLinks);

			const Vector3 & vNavMeshMin = pHierNavData->GetMin();
			const Vector3 & vNavMeshSize = pHierNavData->GetSize();

			int iLinkPtr = 0;
			int iNodeCounter = 0;

			for(n=0; n<pHierarchical->m_Nodes.GetCount(); n++)
			{
				CHierarchicalNode * pNode = pHierarchical->m_Nodes[n];
				CHierarchicalNavNode & node = pHierNavData->m_pNodes[iNodeCounter++];

				//AssertMsg(pNode->m_vPosition.z >= vNavMeshMin.z && pNode->m_vPosition.z <= pHierNavData->GetMax().z, ("%f, %f, %f\n", pNode->m_vPosition.z, vNavMeshMin.z, pHierNavData->GetMax().z));

				pNode->m_vPosition.z = Clamp(pNode->m_vPosition.z, vNavMeshMin.z, pHierNavData->GetMax().z);

				node.m_Struct1.m_iX = CompressFloat8(pNode->m_vPosition.x, vNavMeshMin.x, vNavMeshSize.x);
				node.m_Struct1.m_iY = CompressFloat8(pNode->m_vPosition.y, vNavMeshMin.y, vNavMeshSize.y);
				node.m_Struct1.m_iZ = CompressFloat12(pNode->m_vPosition.z, vNavMeshMin.z, vNavMeshSize.z);

				//ASSERT_ONLY(float fTestZ = DecompressFloat12(node.m_Struct1.m_iZ, vNavMeshMin.z, vNavMeshSize.z);)
				//Assert(CMaths::Eq(fTestZ, pNode->m_vPosition.z, 0.2f));

				int iNumLinks = pNode->m_Links.GetCount();
				Assert(iNumLinks > 0 && iNumLinks <= MAX_NUM_HIERACHICAL_LINKS);

				node.m_Struct3.m_iNumLinks = MAX(0, iNumLinks-1);
				node.m_Struct2.m_iStartOfLinkData = iLinkPtr;
				node.m_Struct2.m_iNavMeshIndex = pNavMesh->m_iIndexOfMesh;

				// Keep within maximum node area
				node.m_Struct3.m_iAreaRepresented = MIN((int)pNode->m_fArea, CHierarchicalNavNode::ms_iMaxAreaRepresented);
				// Impose an arbirary minimum limit to the area size to prevent the pedgen from spamming the same nodes repeatedly.
				// 8x8 = 64m sq
				node.m_Struct3.m_iAreaRepresented = MAX(node.m_Struct3.m_iAreaRepresented, CHierarchicalNavNode::ms_iMinAreaRepresented);

				Assert(pNode->m_pPoly);
				node.m_Struct3.m_iPedDensity = MIN(pNode->m_pPoly->GetPedDensity(), CHierarchicalNavNode::ms_iMaxPedDensity);

				// Set these initially 
				node.m_Struct4.m_iParentNavMeshIndex = NAVMESH_NAVMESH_INDEX_NONE;
				node.m_Struct4.m_iParentNodeIndex = NAVMESH_NODE_INDEX_NONE;

				node.SetPedSpawningEnabled(true);

				for(l=0; l<pNode->m_Links.GetCount(); l++)
				{
					CHierarchicalLink & srcLink = pNode->m_Links[l];
					CHierarchicalNavLink & dstLink = pHierNavData->m_pLinks[iLinkPtr];

					dstLink.SetNavMeshIndex(srcLink.m_iNavMeshIndex);

					Assert(srcLink.m_pNode->m_iIndexOfThisNode != -1);
					dstLink.SetNodeIndex(srcLink.m_pNode->m_iIndexOfThisNode);

					dstLink.SetWidthToLeft((int) Min(srcLink.m_fWidthToLeft, HIERARCHICAL_LINK_MAX_WIDTH) );
					dstLink.SetWidthToRight((int) Min(srcLink.m_fWidthToRight, HIERARCHICAL_LINK_MAX_WIDTH) );

					iLinkPtr++;
				}
			}

			const s32 iMem = pHierNavData->CalcSize();

			int iX,iY;
			CPathServer::GetSectorFromNavMeshIndex(pNavMesh->m_iIndexOfMesh,iX,iY);

			printf("NavMesh [%i][%i] CHeirarchicalNavData = %ik\n", iX, iY, iMem/1024);

			char filename[256];
			sprintf(filename, "%s/%i_%i.ihn", pSavePath, iX, iY);
			CHierarchicalNavData::Save(filename, pHierNavData);
		}
	}

	return true;
}
#endif // __USE_BLOCKSIZE

bool
CNavGen::InsertHierarchicalNodesToFollowTerrain(CHierarchical * pHierarchical)
{
	static const float fMinLinkDist					= 4.0f;
	static const float fMaxHeightError				= 2.0f;	//1.0f
	static const float fStepSize					= 4.0f;	//2.0f;
	static const float fMaxHeightCorrectionDelta	= 20.0f;

	s32 n,l;

	for(n=0; n<pHierarchical->m_Nodes.GetCount(); n++)
	{
		CHierarchicalNode * pNode = pHierarchical->m_Nodes[n];

		for(l=0; l<pNode->m_Links.GetCount(); l++)
		{
			CHierarchicalLink link = pNode->m_Links[l];
			if(link.m_bCheckedForHeightDifferences)
				continue;

			link.m_bCheckedForHeightDifferences = true;

			const int iLinkBackIndex = GetLinkIndex(link.m_pNode->m_Links, pNode);
			Assert(iLinkBackIndex != -1);
			if(iLinkBackIndex != -1)
			{
				CHierarchicalLink & linkBack = link.m_pNode->m_Links[iLinkBackIndex];
				Assert(!linkBack.m_bCheckedForHeightDifferences);
				linkBack.m_bCheckedForHeightDifferences = true;
			}

			CHierarchicalLink & linkRef = pNode->m_Links[l];
			bool bFinished = false;

#if __DEV
			const Vector3 vTestVec1(2750.76f, 183.6f, 23.52f);
			const Vector3 vTestVec2(2737.89f, 188.29f, 26.99f);
			const float fEps = 0.2f;

			static bool bPause = false;
			if(pNode->m_vPosition.IsClose(vTestVec1, fEps) && link.m_pNode->m_vPosition.IsClose(vTestVec2, fEps))
			{
				bPause = true;
			}
			if(pNode->m_vPosition.IsClose(vTestVec2, fEps) && link.m_pNode->m_vPosition.IsClose(vTestVec1, fEps))
			{
				bPause = true;
			}
#endif

			while(!bFinished)
			{
				const Vector3 & vStart = pNode->m_vPosition;
				const Vector3 & vEnd = link.m_pNode->m_vPosition;
				Vector3 vVec = vEnd - vStart;
				if(vVec.XYMag2() < fMinLinkDist)
				{
					bFinished = true;
					break;
				}
				const float fLen = NormalizeAndMag(vVec);
				vVec *= fStepSize;

				if(fLen > fMinLinkDist)
				{
					int iNumSteps = (int) (fLen / fStepSize) -1;
					Vector3 vTestPos = vStart + vVec;

					while(iNumSteps > 0)
					{
						const u32 iNavMesh = CNavGen_NavMeshStore::GetNavMeshIndexFromPosition(vTestPos);
						CNavMesh * pNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(iNavMesh);
						if(pNavMesh)
						{
							Vector3 vIntersectBelow, vIntersectAbove;
							float fClosestDistBelow = FLT_MAX, fClosestDistAbove = FLT_MAX;

							const u32 iPolyBelow = pNavMesh->GetPolyBelowPoint(vTestPos, vIntersectBelow, fMaxHeightCorrectionDelta);
							const u32 iPolyAbove = pNavMesh->GetPolyAbovePoint(vTestPos, vIntersectAbove, fMaxHeightCorrectionDelta);

							if(iPolyBelow != NAVMESH_POLY_INDEX_NONE)
								fClosestDistBelow = Abs(vTestPos.z - vIntersectBelow.z);
							if(iPolyAbove != NAVMESH_POLY_INDEX_NONE)
								fClosestDistAbove = Abs(vIntersectAbove.z - vTestPos.z);

							if(iPolyBelow != NAVMESH_POLY_INDEX_NONE || iPolyAbove != NAVMESH_POLY_INDEX_NONE)
							{
								const float fClosestDist = (fClosestDistAbove < fClosestDistBelow) ? fClosestDistAbove : fClosestDistBelow;

								if(fClosestDist > fMaxHeightError && fClosestDist < fMaxHeightCorrectionDelta)
								{
									const Vector3 & vClosestIntersect = (fClosestDistAbove < fClosestDistBelow) ? vIntersectAbove : vIntersectBelow;
									const u32 iClosestPoly = (fClosestDistAbove < fClosestDistBelow) ? iPolyAbove : iPolyBelow;

									// Create new node at intersection & initialise
									CHierarchicalNode * pNewNode = rage_new CHierarchicalNode();
									pNewNode->m_bAddedToFollowTerrain = true;
									pNewNode->m_pNavMesh = pNavMesh;
									pNewNode->m_pPoly = pNavMesh->GetPoly(iClosestPoly);
									pNewNode->m_vPosition = vClosestIntersect;
									pHierarchical->m_Nodes.PushAndGrow(pNewNode);
									pHierarchical->m_iNumNodesActive++;

									// Link back from new node to prev node
									CHierarchicalLink linkBack;
									linkBack.m_iNavMeshIndex = pNode->m_pNavMesh->m_iIndexOfMesh;
									linkBack.m_pNode = pNode;
									linkBack.m_bCheckedForHeightDifferences = true;
									pNewNode->m_Links.PushAndGrow(linkBack);

									// Link forwards from the new node to the next node
									CHierarchicalLink linkForward;
									linkForward.m_iNavMeshIndex = link.m_pNode->m_pNavMesh->m_iIndexOfMesh;
									linkForward.m_pNode = link.m_pNode;
									linkForward.m_bCheckedForHeightDifferences = false;
									pNewNode->m_Links.PushAndGrow(linkForward);


									// Find the link-back from the destination node, and modify it
									// to point to the new node
									Assert(iLinkBackIndex!=-1);
									if(iLinkBackIndex!=-1)
									{
										CHierarchicalLink & lbRef = link.m_pNode->m_Links[iLinkBackIndex];
										lbRef.m_pNode = pNewNode;
										lbRef.m_iNavMeshIndex = pNavMesh->m_iIndexOfMesh;
										lbRef.m_bCheckedForHeightDifferences = false;
									}

									// Modify the link from the last node to point to the new one
									linkRef.m_iNavMeshIndex = pNavMesh->m_iIndexOfMesh;
									linkRef.m_pNode = pNewNode;
									linkRef.m_bCheckedForHeightDifferences = true;

									bFinished = true;
									break;
								}
							}
						}

						vTestPos += vVec;
						iNumSteps--;
					}
				}
				bFinished = true;
			}
		}
	}
	return true;
}

//*********************************************************************
// AnalyseLinkWidth
// Give a link this will work out how much free navigatable space
// lies on the left and right sides of it.  This is done by querying
// line-of-sight in the navmesh at increasing distances from the
// link's central line.

void CNavGen::AnalyseLinkWidth(CHierarchicalNode * UNUSED_PARAM(pFromNode), CHierarchicalLink * pLink)
{
	pLink->m_fWidthToLeft = 0;
	pLink->m_fWidthToRight = 0;

	// TODO : FIX THIS BY IMPLELEMTING LOS FUNCTION IN CNAVGEN_NAVMESHSTORE!!

#if 0
	Assert(!pLink->m_bCheckedForWidth);
	CHierarchicalNode * pToNode = pLink->m_pNode;

	static const Vector3 vTestPos1(0.78f, 1725.4f, 1199.9f);
	static const Vector3 vTestPos2(1.95f, 1744.14f, 1199.9f);
	static const float fTestEps = 2.0f;
	static bool bPause = false;

	if( (pFromNode->m_vPosition.IsClose(vTestPos1, fTestEps) && pToNode->m_vPosition.IsClose(vTestPos2, fTestEps)) ||
		(pFromNode->m_vPosition.IsClose(vTestPos2, fTestEps) && pToNode->m_vPosition.IsClose(vTestPos1, fTestEps)) )
	{
		bPause = true;
	}

	// If the link back to the pFromNode has already been analysed, then use it's values
	for(int l=0; l<pToNode->m_Links.size(); l++)
	{
		CHierarchicalLink & linkBack = pToNode->m_Links[l];
		if(linkBack.m_pNode==pFromNode && linkBack.m_bCheckedForWidth)
		{
			pLink->m_fWidthToLeft = linkBack.m_fWidthToRight;
			pLink->m_fWidthToRight = linkBack.m_fWidthToLeft;
			pLink->m_bCheckedForWidth = true;
			return;
		}
	}

	pLink->m_bCheckedForWidth = true;

	static const float fTestStep = 0.25f;
	static const float fMaxTestWidth = HIERARCHICAL_LINK_MAX_WIDTH;
	static const float fTestSides[2] = { 1.0f, -1.0f };
	static const float fPolyLookDownDist = 4.0f;
	static const Vector3 vPolyLookOffset(0.0f,0.0f,0.5f);

	Vector3 vLinkVec = pToNode->m_vPosition - pFromNode->m_vPosition;
	vLinkVec.Normalize();
	Vector3 vTangent = CrossProduct(vLinkVec, ZAXIS);
	vTangent.Normalize();

	Vector3 vIntersect;

	// Get the navmesh polygons underneath the start node
	CNavMesh * pStartNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex( CNavGen_NavMeshStore::GetNavMeshIndexFromPosition(pFromNode->m_vPosition) );
	Assert(pStartNavMesh);
	if(!pStartNavMesh)
		return;
	u32 iStartPolyIndex = pStartNavMesh->GetPolyBelowPoint(pFromNode->m_vPosition + vPolyLookOffset, vIntersect, fPolyLookDownDist);
	if(iStartPolyIndex == NAVMESH_POLY_INDEX_NONE)
		iStartPolyIndex = pStartNavMesh->GetPolyAbovePoint(pFromNode->m_vPosition - vPolyLookOffset, vIntersect, fPolyLookDownDist);
	Assert(iStartPolyIndex != NAVMESH_POLY_INDEX_NONE);
	if(iStartPolyIndex == NAVMESH_POLY_INDEX_NONE)
		return;
	TNavMeshPoly * pStartPoly = &pStartNavMesh->m_Polys[iStartPolyIndex];

	// Get the navmesh polygons underneath the end node
	CNavMesh * pEndNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex( CNavGen_NavMeshStore::GetNavMeshIndexFromPosition(pToNode->m_vPosition) );
	Assert(pEndNavMesh);
	if(!pEndNavMesh)
		return;
	u32 iEndPolyIndex = pEndNavMesh->GetPolyBelowPoint(pToNode->m_vPosition + vPolyLookOffset, vIntersect, fPolyLookDownDist);
	if(iEndPolyIndex == NAVMESH_POLY_INDEX_NONE)
		iEndPolyIndex = pEndNavMesh->GetPolyAbovePoint(pToNode->m_vPosition - vPolyLookOffset, vIntersect, fPolyLookDownDist);
	Assert(iEndPolyIndex != NAVMESH_POLY_INDEX_NONE);
	if(iEndPolyIndex == NAVMESH_POLY_INDEX_NONE)
		return;
	TNavMeshPoly * pEndPoly = &pEndNavMesh->m_Polys[iEndPolyIndex];

	for(int s=0; s<2; s++)
	{
		const float fSide = fTestSides[s];

		float fWidth;
		for(fWidth=fTestStep ; fWidth<fMaxTestWidth; fWidth+=fTestStep)
		{
			const Vector3 vLosTangentVec = (vTangent * fSide) * fWidth;

			// Use LOS to get the start poly offset by the current test width
			CPathServer::m_PathServerThread.IncTimeStamp();
			TNavMeshPoly * pWidthTestStartPoly = CPathServer::m_PathServerThread.TestNavMeshLOS(
				pFromNode->m_vPosition,
				Vector2(vLosTangentVec.x, vLosTangentVec.y),
				NULL,
				pStartPoly,
				0
			);
			if(!pWidthTestStartPoly)
				break;
			
			// Use LOS to get the end poly offset by the current test width
			CPathServer::m_PathServerThread.IncTimeStamp();
			TNavMeshPoly * pWidthTestEndPoly = CPathServer::m_PathServerThread.TestNavMeshLOS(
				pToNode->m_vPosition,
				Vector2(vLosTangentVec.x, vLosTangentVec.y),
				NULL,
				pEndPoly,
				0
			);
			if(!pWidthTestEndPoly)
				break;

			CPathServer::m_PathServerThread.IncTimeStamp();
			if(!CPathServer::m_PathServerThread.TestNavMeshLOS(
				pFromNode->m_vPosition + vLosTangentVec,
				pToNode->m_vPosition + vLosTangentVec,
				pWidthTestEndPoly,
				pWidthTestStartPoly,
				NULL,
				0))
			{
				break;
			}
		}

		if(s==0)
			pLink->m_fWidthToRight = fWidth;
		else
			pLink->m_fWidthToLeft = fWidth;			
	}
#endif
}

}	// namespace rage
