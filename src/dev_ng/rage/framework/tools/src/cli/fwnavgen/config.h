//
// fwnavgen/config.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef FWNAVGEN_CONFIG_H
#define FWNAVGEN_CONFIG_H

#include "parser/macros.h"		// PAR_PARSABLE
#include "system/new.h"
#include "fwgeovis/geovis.h"

//-----------------------------------------------------------------------------

namespace rage
{

struct fwNavGenConfig;

//-----------------------------------------------------------------------------

class fwNavGenConfigManager
{
public:
	static void ShutdownClass();

	static fwNavGenConfigManager& GetInstance()
	{	Assertf(sm_Instance, "Expected navgen configuration manager object.");
		return *sm_Instance;
	}

	// PURPOSE:	Load a configuration file with a given name. Normally called internally
	//			by fwNavGenConfigManagerImpl<>::InitClass().
	// PARAMS:	pConfigFilename		- Name of the configuration file to load.
	virtual void Load(const char* pConfigFilename);

	// PURPOSE:	Get the configuration.
	// RETURNS:	Read-only reference to the configuration data.
	const fwNavGenConfig& GetConfig() const
	{	Assert(m_Config);
		return *m_Config;
	}

protected:
	fwNavGenConfigManager();

	virtual ~fwNavGenConfigManager();

	virtual fwNavGenConfig *CreateConfig() const = 0;

	fwNavGenConfig*					m_Config;

	// PURPOSE:	Pointer to the manager instance, or NULL.
	static fwNavGenConfigManager*	sm_Instance;
};

//-----------------------------------------------------------------------------

/*
PURPOSE
	This subclass template simply makes sure that it's easy to use fwNavGenConfigManager
	with configuration classes specialized for the game.
*/
template<class _CfgData> class fwNavGenConfigManagerImpl : public fwNavGenConfigManager
{
public:
	// PURPOSE:	Initialize fwNavGenConfigManager, by loading a given configuration file
	//			and resizing all fwPool objects that have been registered.
	// PARAMS:	configFilename		- File name of the configuration file to load,
	//								  or NULL if no file load is desired.
	static void InitClass(const char* configFilename)
	{
		// Should only have one instance.
		Assert(!sm_Instance);

		// Create the instance.
		sm_Instance = rage_new fwNavGenConfigManagerImpl<_CfgData>;

		// Load the configuration file. Note that this couldn't just be done
		// by constructor of fwNavGenConfigManager, as we need to finish the construction
		// before the call to CreateConfig().
		sm_Instance->Load(configFilename);
#if HEIGHTMAP_GENERATOR_TOOL
		const_cast<fwNavGenConfig&>(sm_Instance->GetConfig()).m_WorldMinX = (float)gv::NAVMESH_BOUNDS_MIN_X;
		const_cast<fwNavGenConfig&>(sm_Instance->GetConfig()).m_WorldMinY = (float)gv::NAVMESH_BOUNDS_MIN_Y;
#endif
	}

protected:
	virtual fwNavGenConfig *CreateConfig() const
	{	return rage_new _CfgData;	}
};

//-----------------------------------------------------------------------------

struct fwNavGenConfig
{
	// Convenience function
	static const fwNavGenConfig& Get()
	{	return fwNavGenConfigManager::GetInstance().GetConfig();	}

	// PURPOSE:	When building navmeshes for aerial navigation, this is the max area a polygon
	//			is allowed to have when merging. This is separate from the on-ground value as
	//			we likely want to merge to larger polygons to save memory in the aerial mesh.
	float	m_AerialMaxTriangleArea;

	// PURPOSE:	Like m_AerialMaxTriangleArea, but used to limit the sides of polygons rather than
	//			their area.
	float	m_AerialMaxTriangleSideLength;

	// PURPOSE:	For heightmeshes, this is the default sampling resolution to use.
	float	m_AerialSampleResolutionDefault;

	// PURPOSE:	When building navmeshes for aerial navigation, sampled points get quantized upwards
	//			with the resolution specified here. A higher value will tend to produce a mesh
	//			with larger flat pieces, thus with fewer polygons, but less vertical accuracy.
	float	m_AerialVerticalQuantization;

	// Any polys whose surface normal deviates from the up-vector by over this angle, will be marked as "steep" and normal paths will not traverse them.
	// Note: in degrees.
	float	m_AngleForNavMeshPolyToBeTooSteep;				// from CNavGenParams::CNavGenParams(), m_fAngleForNavMeshPolyToBeTooSteep (44.0).

	s32		m_AntialiasEdgesNumPasses;						// Number of iterations at smoothing navmesh edges after triangulation

	s32		m_AntialiasPavementEdgesNumPasses;				// Number of iterations at smoothing navmesh pavement edges after triangulation

	bool	m_CreateResolutionAreasForPavementTriangles;		// Automatically create resolution areas for the AABB of pavement triangles

	// This is the distance away from the low-wall at which the cover point should be positioned for the anims to work
	float	m_CoverLowFinalDistanceFromSurfaceOfCoverPoint;	// from CheckForLowWallCoverPoint(), was fFinalDistanceFromSurfaceOfCoverPoint (0.3, formerly 0.5).

	float	m_CoverLowFinalProbeCheckDistanceLimit;			// The final adjusted coverpoint position and direction should not be more than this distance to cover object.

	float	m_CoverLowMaxDistOfFloorBelow;					// from CheckForLawWallCoverPoint(), was fMaxDistOfFloorBelow (1.0).

	// This is the minimum distance from any other coverpoint at which a new low-wall coverpoint can be generated
	float	m_CoverLowPointSeparation;						// from CheckForLawWallCoverPoint(), was fCoverPointSeparation (0.5, formerly 0.2).

	// This is the range of heights at which line-of-sights should be clear for this to be fireable over
	float	m_CoverLowWallMidHeight;						// from CheckForLawWallCoverPoint(), was fLowWallMidHeight (1.4).

	// This is the height at which a line-of-sight should be blocked, to affirm that there is a low wall here
	float	m_CoverLowWallObstructionHeight;				// from CheckForLawWallCoverPoint(), was fLowWallObstructionHeight (0.2).

	float	m_CoverLowWallObstructionMaxHeight;				// from CheckForLawWallCoverPoint(), was fLowWallObstructionMaxHeight (0.5).

	// This is changed to 2.0f now that cover linking is turned off by default.
	// In the event that it is switched back on again, we will need to put it back down to 0.4f
	float	m_CoverLowWallPointSpacingAlongEdges;			// was g_fLowWallCoverPointSpacingAlongEdges (2.0)

	// This is the distance ahead of the potential coverpoint position which we shall test for a clear line-of-sight
	float	m_CoverLowWallTestDistance;						// from CheckForLawWallCoverPoint(), was fLowWallTestDistance (1.5).

	float	m_ExporterMaxZCutoff;							// was fwExportCollisionGridTool::fExporterMaxZCutoff.
	float	m_ExporterMinZCutoff;							// was fwExportCollisionGridTool::fExporterMinZCutoff.

	// Any polys whose surface normal deviates from the up-vector by over this angle, will not become part of the navmesh.
	// Note: in degrees.
	float	m_MaxAngleForPolyToBeInNavMesh;					// from CNavGenParams::CNavGenParams(), m_fMaxAngleForPolyToBeInNavMesh (60.0).

	// Any water polys whose surface normal deviates from the up-vector by over this angle, will not become part of the navmesh.
	// Note: in degrees.
	float	m_MaxAngleForWaterPolyToBeInNavMesh;			// from CNavGenParams::CNavGenParams(), m_fMaxAngleForWaterPolyToBeInNavMesh (75.0).

	// The maximum sudden change in height of the collision mesh underneath any two vertices of
	// a triangle which is about to be triangulated.
	float	m_MaxHeightChangeUnderTriangleEdge;				// was CNavGen::ms_MaxHeightChangeUnderTriangleEdge (0.4).

	// Defines how far to descend beneath a node, before creating the next one.
	float	m_MinimumVerticalDistanceBetweenNodes;			// was CNavGen::m_fMinimumVerticalDistanceBetweenNodes (2.0).

	float	m_PedRadius;									// was PED_RADIUS.

	float	m_SampleResolutionDefault;						// was g_fSampleResolution, in NavMeshMaker.cpp.

	// This is the height which must be clear of obstructions above the triangulated navmesh
	// Basically it is the height of a ped, except under special circumstances (dynamic navmeshes)
	// where we may with to lower it to allow peds to pass under low obstructions on boats, etc.
	float	m_TestClearHeightInTriangulation;				// was CNavGen::m_fTestClearHeightInTriangulation (1.0).

	// The minimum XY coordinate of the world
	// Sector number proceeds from this location
	// It should be a multiple of the navmesh mesh size
	float	m_WorldMinX;									// was fwExportCollisionGridTool::m_vWorldMin.x.
	float	m_WorldMinY;									// was fwExportCollisionGridTool::m_vWorldMin.y.

	float	m_MinPatchAreaBeforeCulling;					// Navmesh patches whose area is smaller that this, will be culled

	// Maximum Z interval to search for nodes to triangulate; is multiplied by sample spacing.
	// Is increased automatically when nodes are are on stairs
	// Will need to be enough to detect nodes on sloping surfaces
	float	m_TriangulationMaxHeightDiff;

	// Quadric error metric is sum of squared distances from surrounding planes of new point during optimisation (mesh collapse)
	// Lower value gives a mesh which conforms better to original shape, but consists of more triangles
	float m_OptimizeMaxQuadricErrorMetric;


	PAR_PARSABLE;
};

//-----------------------------------------------------------------------------

}	// namespace rage

//-----------------------------------------------------------------------------

#endif	// FWNAVGEN_CONFIG_H

/* End of file fwnavgen/config.h */
