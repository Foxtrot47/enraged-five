#include "navgen.h"

using namespace rage;

//**************************************************************************************
//	Filename : NavGen_Audio
//	Desc : This file contains functions used to query the local collision geometry for
//	audio prioerties.  It is called during the analysis phase of navmesh creation - that
//	is the final stage, after NavMeshMaker has already been invoked ro create the basic
//	navmesh geometry.
//**************************************************************************************


//*******************************************************************************************************
//	CalculateAudioProperties
//	This function is called for every navmesh poly in every navmesh in turn.

//	iNumVerts		= the number of verts in the poly
//	pPolyVerts		= the vertices of the poly (raised up slightly to reduce chance of intersecing the floor)
//	vPolyCentroid	= the centroid of the poly (again raised up)
//	ppNavGens		= pointers to the navmesh generator for the current & up to 4 surrounding navmeshes
//	iNumNavGens		= the number of navgens pointed to by ppNavGens (*)
//
//	(*) the ppNavGens can be used to perform linetests into the local collision mesh of the world, use
//	the functions TestMultipleNavGenLos() and ProcessMultipleNavGenLos() to do this (example below).
//	The TestMultipleNavGenLos() function is potentially faster, since it can early-out as soon as any
//	intersection is found.
//*******************************************************************************************************


u32
CNavGen::CalculateAudioPropertiesForPoly(const int UNUSED_PARAM(iNumVerts), const Vector3 * UNUSED_PARAM(pPolyVerts), const Vector3 & vPolyCentroid, CNavGen ** ppNavGens, int iNumNavGens)
{
//	u32 iAudioProperties=0;

	// Just a bit of experimental code, and an example of the linetest calls!

	// Excuse the comedy code - will make it readable/flexible when I've a good idea of the basic structure

	Vector3 vPolyUpABit = vPolyCentroid;
	vPolyUpABit.Add(Vector3(0.0f, 0.0f, 0.75f));

	static const int numMainTestRays=8;
	static const Vector3 vMainTestVecs[numMainTestRays] =
	{
		Vector3(1.0f, 0.0f, 0.0f),
		Vector3(0.0f, 1.0f, 0.0f),
		Vector3(-1.0f, 0.0f, 0.0f),
		Vector3(0.0f, -1.0f, 0.0f),
		Vector3(1.0f, 1.0f, 0.0f),
		Vector3(-1.0f, 1.0f, 0.0f),
		Vector3(-1.0f, -1.0f, 0.0f),
		Vector3(1.0f, -1.0f, 0.0f),
	};
	static const float fTestDist = 40.0f;

	Vector3 vHitPos;
	float fHitDist;
	CNavGenTri * pHitTri;
	Vector3 vStart;
	Vector3 vEnd;
	bool noIntersect = false;
	u32 iRetFlags = 0;

	f32 hitDistance[numMainTestRays];
	for (int i=0; i<numMainTestRays; i++)
	{
		vStart = vPolyUpABit;
		vEnd   = vPolyUpABit + vMainTestVecs[i] * fTestDist;
		hitDistance[i] = -1;
		noIntersect = CNavGen::ProcessMultipleNavGenLos(vStart, vEnd, iNumNavGens, ppNavGens, iRetFlags, NAVGEN_COLLISION_TYPES, &vHitPos, fHitDist, pHitTri);
		if (!noIntersect)
		{
			hitDistance[i] = fHitDist;
		}
	}

	static f32 diagonalDistance = 0.75f;
	f32 diagonalHitDistance[4][2];

	for (int i=0; i<4; i++)
	{
		if (hitDistance[i+4]<diagonalDistance && hitDistance[i+4]>-0.5f)
		{
			// it hits something close, so we don't bother scanning from it.
			diagonalHitDistance[i][0] = hitDistance[i];
			diagonalHitDistance[i][1] = hitDistance[(i+1)%4];
		}
		else
		{
			diagonalHitDistance[i][0] = diagonalHitDistance[i][1] = -1.0f;
			vStart = vPolyUpABit + vMainTestVecs[i+4] * diagonalDistance;
			vEnd = vStart + vMainTestVecs[i] * fTestDist;
			noIntersect = CNavGen::ProcessMultipleNavGenLos(vStart, vEnd, iNumNavGens, ppNavGens, iRetFlags, NAVGEN_COLLISION_TYPES, &vHitPos, fHitDist, pHitTri);
			if (!noIntersect)
			{
				diagonalHitDistance[i][0] = fHitDist;
			}

			vEnd = vStart + vMainTestVecs[(i+1)%4] * fTestDist;
			noIntersect = CNavGen::ProcessMultipleNavGenLos(vStart, vEnd, iNumNavGens, ppNavGens, iRetFlags, NAVGEN_COLLISION_TYPES, &vHitPos, fHitDist, pHitTri);
			if (!noIntersect)
			{
				diagonalHitDistance[i][1] = fHitDist;
			}
		}
	}

	// Now look up
	static Vector3 up = Vector3(0.0f, 0.0f, 1.0f);
	static f32 upTestDistance = 30.0f;
	f32 upHitDistances[9];

	vStart = vPolyUpABit;
	vEnd   = vPolyUpABit + up * upTestDistance;
	upHitDistances[0] = -1.0f;
	noIntersect = CNavGen::ProcessMultipleNavGenLos(vStart, vEnd, iNumNavGens, ppNavGens, iRetFlags, NAVGEN_COLLISION_TYPES, &vHitPos, fHitDist, pHitTri);
	if (!noIntersect)
	{
		upHitDistances[0] = fHitDist;
	}

	for (int i=1; i<5; i++)
	{
		if (hitDistance[i-1]<diagonalDistance && hitDistance[i-1]>-0.5f)
		{
			// it hits something close, so we don't bother scanning from it.
			upHitDistances[i] = upHitDistances[0];
			upHitDistances[i+4] = upHitDistances[0];
		}
		else
		{
			upHitDistances[i] = -1.0f;
			vStart = vPolyUpABit + vMainTestVecs[i-1] * diagonalDistance;
			vEnd  = vPolyUpABit + vMainTestVecs[i-1] * diagonalDistance + up * upTestDistance;
			noIntersect = CNavGen::ProcessMultipleNavGenLos(vStart, vEnd, iNumNavGens, ppNavGens, iRetFlags, NAVGEN_COLLISION_TYPES, &vHitPos, fHitDist, pHitTri);
			if (!noIntersect)
			{
				upHitDistances[i] = fHitDist;
			}
			// Now do the further along ones
			upHitDistances[i+4] = -1.0f;
			f32 upFarTestDistance = Max(diagonalDistance, Min(7.5f, (hitDistance[i-1] - 2.5f)));
			vStart = vPolyUpABit + vMainTestVecs[i-1] * upFarTestDistance;
			vEnd  = vPolyUpABit + vMainTestVecs[i-1] * upFarTestDistance + up * upTestDistance;
			noIntersect = CNavGen::ProcessMultipleNavGenLos(vStart, vEnd, iNumNavGens, ppNavGens, iRetFlags, NAVGEN_COLLISION_TYPES, &vHitPos, fHitDist, pHitTri);
			if (!noIntersect)
			{
				upHitDistances[i+4] = fHitDist;
			}
		}
	}

	// Now look up and out
	f32 upAndOutHitDistances[4];
	f32 maxClearHeight = -1.0f;
	int maxClearHeightIndex = -1;

	for (int i=0; i<5; i++)
	{
		if (upHitDistances[i] < -0.5f)
		{
			// los up, so use that one
			maxClearHeightIndex = i;
			maxClearHeight = -1.0f;
			break;
		}
		if (upHitDistances[i]>maxClearHeight)
		{
			maxClearHeight = upHitDistances[i];
			maxClearHeightIndex = i;
		}
	}
	if (maxClearHeight<-0.5)
	{
		// Can pick any height we like
		maxClearHeight = 5.0f;
	}
	else
	{
		maxClearHeight -= 1.5f; // come down from the roof a bit
		maxClearHeight = Max(0.0f, maxClearHeight - 1.5f); // come down from the roof a bit
	}

	Vector3 lookUpHorizonalOffset = Vector3(0.0f, 0.0f, 0.0f);
	if (maxClearHeightIndex!=0)
	{
		lookUpHorizonalOffset = vMainTestVecs[maxClearHeightIndex-1];
	}

	vStart = vPolyUpABit + lookUpHorizonalOffset * diagonalDistance + up * maxClearHeight;
	for (int i=0; i<4; i++)
	{
		upAndOutHitDistances[i] = -1.0f;
		vEnd = vPolyUpABit + lookUpHorizonalOffset * diagonalDistance + up * maxClearHeight + vMainTestVecs[i] * fTestDist;
		noIntersect = CNavGen::ProcessMultipleNavGenLos(vStart, vEnd, iNumNavGens, ppNavGens, iRetFlags, NAVGEN_COLLISION_TYPES, &vHitPos, fHitDist, pHitTri);
		if (!noIntersect)
		{
			upAndOutHitDistances[i] = fHitDist;
		}
	}

	// Also look diagonally up, which'll make under narrow things less reverby
	static f32 upDiagonalTestDistance = 30;
	f32 diagonallyUpHitDistances[4];
	for (int i=0; i<4; i++)
	{
		diagonallyUpHitDistances[i] = -1.0f;
		vStart = vPolyUpABit;
		vEnd   = vPolyUpABit + (vMainTestVecs[i] * 1.0f + up) * upDiagonalTestDistance;
		noIntersect = CNavGen::ProcessMultipleNavGenLos(vStart, vEnd, iNumNavGens, ppNavGens, iRetFlags, NAVGEN_COLLISION_TYPES, &vHitPos, fHitDist, pHitTri);
		if (!noIntersect)
		{
			diagonallyUpHitDistances[i] = fHitDist;
		}
	}

	// Cheap and cheerful first go
	// How many of our ups hit something?
	int upsBlocked = 0;
	f32 averageHeight = 0.0f;
	for (int i=0; i<5; i++)
	{
		if (upHitDistances[i]>-0.5f)
		{
			upsBlocked++;
			averageHeight += upHitDistances[i];
		}
	}
//	f32 reverbWet = ((f32)upsBlocked) / 5.0f;
//	f32 reverbSize = 0.0f;
	u32 reverbSizeQuantised = 0; // means small, but also none, if totally dry
	if (upsBlocked>0)
	{
		averageHeight = averageHeight/upsBlocked;
		if (averageHeight < 3.0f)
		{
			reverbSizeQuantised = 0;
		}
		else if (averageHeight < 5.0f)
		{ 
			reverbSizeQuantised = 1;
		}
		else if (averageHeight < 7.0f)
		{
			reverbSizeQuantised = 2;
		}
		else
		{
			reverbSizeQuantised = 3;
		}
	}

	int diagonalUpsBlocked = 0;
	for (int i=0; i<4; i++)
	{
		if (diagonallyUpHitDistances[i]>-0.5f)
		{
			diagonalUpsBlocked++;
		}
	}
	// Arbitrary, but we have 5 looks up. Let's remove one blocked for each 2 diagonals that can see out? Ignore distance for now, smaller's safer.
	int normalisedUpsBlocked = upsBlocked - (int)((5-diagonalUpsBlocked) / 2);
	// got between 0 and 5 blocked ups - scale that into 0 to 3.
	// Squeeze extra info in here - if we're 0 blocked, we're totally clean, so null out the reverb size stuff.
	// Otherwise, 0 means 1 blocked, and use the reverb size. Combine 2 and 3 blocked
	u32 reverbWetQuantised = 0;
	if (normalisedUpsBlocked<=0)
	{
		reverbWetQuantised = 0;
		reverbSizeQuantised = 0;
	}
	else if (normalisedUpsBlocked<3)
	{
		reverbWetQuantised = normalisedUpsBlocked-1;
	}
	else
	{
		reverbWetQuantised = Min(3, normalisedUpsBlocked-2);
	}

	u32 output = reverbSizeQuantised + 4 * reverbWetQuantised;

	// Now lets see if we're in an alleyway
	// Look for 2/3 perpendicular horiz lines being blocked < 5m, and for that being true on >=2/4 of the directions.
	// Then look up and out to see if it's a high alleyway, to impact wetness
	f32 hitDistancesPerp[4][3];
	for (int i=0; i<4; i++)
	{
		hitDistancesPerp[i][1] = hitDistance[i]; // that's the straight out one
		hitDistancesPerp[i][2] = diagonalHitDistance[i][0]; // the one after
		hitDistancesPerp[i][0] = diagonalHitDistance[(i+3)%4][1]; // the one before
	}
	int hitDistancesShort[4];
	f32 hitDistancesShortTotal[4];
//	int totalShortDirections = 0;
	// For each of the four directions, see how many short distances we have
	for (int i=0; i<4; i++)
	{
		hitDistancesShort[i] = 0;
		hitDistancesShortTotal[i] = 0.0f;
		for (int j=0; j<3; j++)
		{
			if ((hitDistancesPerp[i][j] > -0.5f) && (hitDistancesPerp[i][j] < 5.0f))
			{
				// This one of the three in this direction is short
				hitDistancesShort[i]++;
				hitDistancesShortTotal[i] += hitDistancesPerp[i][j];
			}
		}
		if (hitDistancesShort[i]>1)
		{
			// work out the average short distance for these three 
			hitDistancesShortTotal[i] = hitDistancesShortTotal[i] / hitDistancesShort[i];
		}
	}
	// Compare both opposite pairs, and see if they're both short, and the total distance is < 5
	bool alleyway = false;
	bool highAlleyway = false;
	for (int i=0; i<2; i++)
	{
		if ((hitDistancesShort[i]>1) && (hitDistancesShort[i+2]>1))
		{
			// They're both short, are they short enough combined?
			if ((hitDistancesShortTotal[i] + hitDistancesShortTotal[i+2]) < 6.0f)
			{
				alleyway = true;
				// Now see if we're in a high alleyway
				if ((upAndOutHitDistances[i]>-0.5f) && (upAndOutHitDistances[i+2]>-0.5f) &&
					((upAndOutHitDistances[i] + upAndOutHitDistances[i+2]) < 8.0f))
				{
					// we're in a high alleyway
					highAlleyway = true;
				}
			}
		}
	}

	// For now, only even look at this is we're otherwise dry.
	if (output == 0)
	{
		if (alleyway)
		{
			output = 5;
			if (highAlleyway)
			{
				output = 6;
			}
		}
	}

	Assert(output<16);

/*
for(int t=0; t<NumRays; t++)
	{
		Vector3 vEnd = vPolyCentroid + (vTestVecs[t]*fTestDist); 

		//************************************************************************************************************
		//	This finds the first intersection from vStart to vEnd, returning info in vHitPos, fHitDist & pHitTri
		//	It returns 'true' if there *is* an intersection (whereas TestMultipleNavGenLos return 'true' if there is
		//	a clear line-of-sight).

		bool bHit = CNavGen::ProcessMultipleNavGenLos(vPolyCentroid, vEnd, iNumNavGens, ppNavGens, &vHitPos, fHitDist, pHitTri);
		if(bHit)
		{
			iNumIntersections++;
		}
	}

	float fRatio = (float)iNumIntersections / (float)NumRays;
	iAudioProperties = (int) (fRatio * 15.0f);
*/

//	AssertMsg(iAudioProperties<16, "iAudioProperties needs to fit into 4 bits if poss.");

	return output;
//	return iAudioProperties;
}