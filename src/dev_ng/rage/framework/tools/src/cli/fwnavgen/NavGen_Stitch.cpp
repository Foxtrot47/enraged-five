#if 1

#include "NavGen.h"
//#include "pathserver\PathServer.h"

namespace rage
{

//******************************************************************************************
//	Title	: NavGen_Stitch.cpp
//	Author	: James Broad
//	Date	: Jan 2005
//
//	For each poly along each navmesh edge, find out how many other polys are abutting it
//	in the adjacent navmesh.  We'll make a note of the modification which is needed to
//	be done to each poly in order to join it perfectly to its neighbour, and finally
//	we will implement these changes and modify the navmesh accordingly.
//	In the final stitching step, all the navmeshes are exactly joined up with this new
//	adjacency..
//
//******************************************************************************************

bool g_bLimitStitchPolySize = false;

int FindVertexIndex(vector<TVec3> & vertexList, const Vector3 & vec, const float fEps)
{
	// Get an index for this vertex.  If not found, add as a new vertex at end of vertices list.
	//int iIndex = -1;
	for(u32 vi=0; vi<vertexList.size(); vi++)
	{
		const TVec3 & listvec = vertexList[vi];
		if(Eq(vec.x, listvec.x, fEps) && Eq(vec.y, listvec.y, fEps) && Eq(vec.z, listvec.z, fEps))
		{
			return vi;
		}
	}
	return -1;
}
int CNavGen::SelectStitchPolyStartingVertex(TStitchPoly * pStitchPoly, CNavMesh * pNavMesh, SideOfNeighbour sideOfNeighbour)
{
	float fGreatestDist = -1000.0f;
	int iBestVertex = -1;

	for(int i=0; i<pStitchPoly->m_NewVertices.size(); i++)
	{
		const Vector3 & vPos = pStitchPoly->m_NewVertices[i];
		float fDiff = 0.0f;

		switch(sideOfNeighbour)
		{
			case ePlusX:
				fDiff = Abs(vPos.x - pNavMesh->m_pQuadTree->m_Maxs.x);
				break;
			case eMinusX:
				fDiff = Abs(vPos.x - pNavMesh->m_pQuadTree->m_Mins.x);
				break;
			case ePlusY:
				fDiff = Abs(vPos.y - pNavMesh->m_pQuadTree->m_Maxs.y);
				break;
			case eMinusY:
				fDiff = Abs(vPos.y - pNavMesh->m_pQuadTree->m_Mins.y);
				break;
			default:
				Assert(0);
				break;
		}

		if(fDiff > fGreatestDist)
		{
			fGreatestDist = fDiff;
			iBestVertex = i;
		}
	}
	Assert(iBestVertex != -1);
	return iBestVertex;
}


bool CNavGen::StitchNavMesh(CNavMesh * pNavMesh, CNavMesh * pNavMeshMinusX, CNavMesh * pNavMeshPlusX, CNavMesh * pNavMeshMinusY, CNavMesh * pNavMeshPlusY)
{
	if(!pNavMesh)
		return true;

	if(pNavMeshMinusX)
		pNavMesh = StitchNavMesh(pNavMesh, pNavMeshMinusX, eMinusX);

	if(pNavMeshPlusX)
		pNavMesh = StitchNavMesh(pNavMesh, pNavMeshPlusX, ePlusX);

	if(pNavMeshMinusY)
		pNavMesh = StitchNavMesh(pNavMesh, pNavMeshMinusY, eMinusY);

	if(pNavMeshPlusY)
		pNavMesh = StitchNavMesh(pNavMesh, pNavMeshPlusY, ePlusY);

	return true;
}



//******************************************************************
//
// Add a vertex to this list, sorted in ascending/descending X/Y
//
//******************************************************************

void AddVertexSorted(Vector3 & vec, atArray<Vector3> & vertices, bool bCompareX, bool bGreatestFirst)
{
	if(!vertices.GetCount())
	{
		vertices.PushAndGrow(vec);
		return;
	}

	Vector3 listVec;

	if(bCompareX)
	{
		if(bGreatestFirst)
		{
			for(s32 v=0; v<vertices.GetCount(); v++)
			{
				listVec = vertices[v];
				if(vec.x > listVec.x)
				{
					atArrayInsert(vertices, vec, v);
					//vertices.insert(vertices.begin()+v, vec);
					return;
				}
			}
		}
		else
		{
			for(s32 v=0; v<vertices.GetCount(); v++)
			{
				listVec = vertices[v];
				if(vec.x < listVec.x)
				{
					atArrayInsert(vertices, vec, v);
					//vertices.insert(vertices.begin()+v, vec);
					return;
				}
			}
		}
	}
	else
	{
		if(bGreatestFirst)
		{
			for(s32 v=0; v<vertices.GetCount(); v++)
			{
				listVec = vertices[v];
				if(vec.y > listVec.y)
				{
					atArrayInsert(vertices, vec, v);
					//vertices.insert(vertices.begin()+v, vec);
					return;
				}
			}
		}
		else
		{
			for(s32 v=0; v<vertices.GetCount(); v++)
			{
				listVec = vertices[v];
				if(vec.y < listVec.y)
				{
					atArrayInsert(vertices, vec, v);
					//vertices.insert(vertices.begin()+v, vec);
					return;
				}
			}
		}
	}

	vertices.PushAndGrow(vec);
}


CNavMesh *
CNavGen::StitchNavMesh(CNavMesh * pNavMesh, CNavMesh * pOtherNavMesh, SideOfNeighbour sideOfNeighbour)
{
	u32 t1, t2;
	u32 iPoly1EdgeFlags, iPoly2EdgeFlags;
	TNavMeshPoly * pPoly1, * pPoly2;

	static const float fSameVectorDotProduct = rage::Cosf( 1.0f * DtoR );

	static const Vector3 vXAxis(1.0f, 0.0f, 0.0f);
	static const Vector3 vYAxis(0.0f, 1.0f, 0.0f);

	vector<TStitchPoly*> stitchPolys;

	for(t1=0; t1<pNavMesh->m_iNumPolys; t1++)
	{
		pPoly1 = pNavMesh->GetPoly(t1);

		Assert(pPoly1->GetNumVertices() >= 3);

		if(!pPoly1->GetLiesAlongEdgeOfMesh())
			continue;

		TStitchPoly * pStitchPoly = new TStitchPoly();
		pStitchPoly->m_iIndexInNavMesh = t1;

		iPoly1EdgeFlags = pNavMesh->GetPolyEdgeFlags(pNavMesh, t1);

		Vector3 vPoly1_Normal, vPoly1_Vertex;
		pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly1, 0), vPoly1_Vertex);
		const bool bGotNormal = pNavMesh->GetPolyNormal(vPoly1_Normal, pPoly1);
		//const float fPoly1_PlaneDist = - DotProduct(vPoly1_Normal, vPoly1_Vertex);

		for(t2=0; t2<pOtherNavMesh->m_iNumPolys; t2++)
		{
			pPoly2 = pOtherNavMesh->GetPoly(t2);

			if(!pPoly2->GetLiesAlongEdgeOfMesh())
				continue;

			iPoly2EdgeFlags = pOtherNavMesh->GetPolyEdgeFlags(pOtherNavMesh, t2);

			// See whether the map edges which these triangles are on present the
			// possibility that they may match up..
			int iNumPossibleSharedEdges = 0;

			if((iPoly1EdgeFlags & EDGEFLAG_NEG_X) && (iPoly2EdgeFlags & EDGEFLAG_POS_X))
				iNumPossibleSharedEdges++;

			if((iPoly1EdgeFlags & EDGEFLAG_POS_X) && (iPoly2EdgeFlags & EDGEFLAG_NEG_X))
				iNumPossibleSharedEdges++;

			if((iPoly1EdgeFlags & EDGEFLAG_NEG_Y) && (iPoly2EdgeFlags & EDGEFLAG_POS_Y))
				iNumPossibleSharedEdges++;

			if((iPoly1EdgeFlags & EDGEFLAG_POS_Y) && (iPoly2EdgeFlags & EDGEFLAG_NEG_Y))
				iNumPossibleSharedEdges++;

			if(!iNumPossibleSharedEdges)
				continue;

			// Okay, these triangles are at least bordering a shared nav-mesh edge
			// now lets do a comprehensive test to find the shared vertices..
			// If we find a shared edge, we'll only fill in the adjacency for pTri1..

			u32 v1, lastv1, v2, lastv2;
			Vector3 pVertex1, pLastVertex1, pVertex2, pLastVertex2;
			Vector3 vEdge1Vec, vEdge2Vec;
			Vector3 vEdge1VecNonUnit3d;
			float fTri1_MinExtent, fTri1_MaxExtent;
			float fTri2_MinExtent, fTri2_MaxExtent;
			float fTri1_MinHeight, fTri1_MaxHeight;
			float fTri2_MinHeight, fTri2_MaxHeight;
			float fTri1_ClosePoint, fTri2_ClosePoint;
			float fDot;
			int iCardinalAxis = 0;

			enum
			{
				XAxis = 1,
				YAxis = 2
			};

			lastv1 = pPoly1->GetNumVertices()-1;
			for(v1=0; v1<pPoly1->GetNumVertices(); v1++)
			{
				pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly1, lastv1), pLastVertex1);
				const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly( pPoly1->GetFirstVertexIndex() + lastv1 );

				// We want to find any open edges of poly1, which are along the shared edge of its navmesh
				if(adjPoly.GetPolyIndex() != NAVMESH_POLY_INDEX_NONE)
				{
					lastv1 = v1;
					continue;
				}

				pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly1, v1), pVertex1);

				vEdge1VecNonUnit3d = pVertex1 - pLastVertex1;
				vEdge1Vec = vEdge1VecNonUnit3d;
				vEdge1Vec.z = 0;
				vEdge1Vec.Normalize();

				// This edge must be along a cardinal axis..
				iCardinalAxis = 0;

				fDot = rage::Abs(DotProduct(vEdge1Vec, vXAxis));
				if(fDot >= fSameVectorDotProduct) iCardinalAxis = XAxis;
				fDot = rage::Abs(DotProduct(vEdge1Vec, vYAxis));
				if(fDot >= fSameVectorDotProduct) iCardinalAxis = YAxis;

				if(iCardinalAxis == XAxis && (sideOfNeighbour == ePlusX || sideOfNeighbour == eMinusX))
				{
					lastv1 = v1;
					continue;
				}
				if(iCardinalAxis == YAxis && (sideOfNeighbour == ePlusY || sideOfNeighbour == eMinusY))
				{
					lastv1 = v1;
					continue;
				}

				if(iCardinalAxis)
				{
					lastv2 = pPoly2->GetNumVertices()-1;
					for(v2=0; v2<pPoly2->GetNumVertices(); v2++)
					{
						pOtherNavMesh->GetVertex( pOtherNavMesh->GetPolyVertexIndex(pPoly2, v2), pVertex2);
						pOtherNavMesh->GetVertex( pOtherNavMesh->GetPolyVertexIndex(pPoly2, lastv2), pLastVertex2);

						vEdge2Vec = pVertex2 - pLastVertex2;
						vEdge2Vec.z = 0;

						vEdge2Vec.Normalize();

						// Are the 2 edge vectors very similar ?
						fDot = DotProduct(vEdge1Vec, vEdge2Vec);
						if(rage::Abs(fDot) < fSameVectorDotProduct)
						{
							lastv2 = v2;
							continue;
						}

						// Now lets test that the triangle edge overlaps in the cardinal axis
						// with a simple extents test.  If so then at least we can walk from
						// one triangle to another (assuming they're at the same height).
						// We'll also test whether the edge points overlap in Z.

						switch(iCardinalAxis)
						{
							case XAxis:
								fTri1_MinExtent = rage::Min(pVertex1.x, pLastVertex1.x);
								fTri1_MaxExtent = rage::Max(pVertex1.x, pLastVertex1.x);
								fTri2_MinExtent = rage::Min(pVertex2.x, pLastVertex2.x);
								fTri2_MaxExtent = rage::Max(pVertex2.x, pLastVertex2.x);
								fTri1_ClosePoint = pVertex1.y;
								fTri2_ClosePoint = pVertex2.y;
								break;
							case YAxis:
								fTri1_MinExtent = rage::Min(pVertex1.y, pLastVertex1.y);
								fTri1_MaxExtent = rage::Max(pVertex1.y, pLastVertex1.y);
								fTri2_MinExtent = rage::Min(pVertex2.y, pLastVertex2.y);
								fTri2_MaxExtent = rage::Max(pVertex2.y, pLastVertex2.y);
								fTri1_ClosePoint = pVertex1.x;
								fTri2_ClosePoint = pVertex2.x;
								break;
							default:
								lastv2 = v2;
								continue;
						}

						// No overlap is possible along the adjoining axis ?
						if(fTri1_MaxExtent <= fTri2_MinExtent || fTri1_MinExtent >= fTri2_MaxExtent)
						{
							lastv2 = v2;
							continue;
						}

						// Edges are too far apart laterally ?
						static const float fLateralProximity = 0.2f;
						if(!Eq(fTri1_ClosePoint, fTri2_ClosePoint, fLateralProximity))
						{
							lastv2 = v2;
							continue;
						}

						fTri1_MinHeight = rage::Min(pVertex1.z, pLastVertex1.z);
						fTri1_MaxHeight = rage::Max(pVertex1.z, pLastVertex1.z);
						fTri2_MinHeight = rage::Min(pVertex2.z, pLastVertex2.z);
						fTri2_MaxHeight = rage::Max(pVertex2.z, pLastVertex2.z);

						static const float fHeightTolerance = 1.0f;

						// Is no overlap is possible vertically ?
						if(fTri1_MaxHeight < (fTri2_MinHeight - fHeightTolerance) || (fTri1_MinHeight - fHeightTolerance) > fTri2_MaxHeight)
						{
							lastv2 = v2;
							continue;
						}
						
						// Ensure that the two vertices from poly2 lie close to the plane of poly1
						if(bGotNormal)
						{
							static const float fMaxDist = 0.25f;

							/*
							const float fD1 = DotProduct(vPoly1_Normal, pVertex2) + fPoly1_PlaneDist;
							const float fD2 = DotProduct(vPoly1_Normal, pLastVertex2) + fPoly1_PlaneDist;

							if(rage::Abs(fD1) > fMaxDist || rage::Abs(fD2) > fMaxDist)
							{
								lastv2 = v2;
								continue;
							}
							*/

							// Previously we checked the edgepoint pVertex2 and pLastVertex2 against the plane of poly1
							// Now this is changed to check the distance from a line formed along pLastVerte1 to pVertex1

							if(geomDistances::Distance2LineToPoint(pLastVertex1, vEdge1VecNonUnit3d, pLastVertex2) > fMaxDist*fMaxDist)
							{
								lastv2 = v2;
								continue;
							}
							if(geomDistances::Distance2LineToPoint(pLastVertex1, vEdge1VecNonUnit3d, pVertex2) > fMaxDist*fMaxDist)
							{
								lastv2 = v2;
								continue;
							}
						}

						// Make sure we've not got any almost identical vertices in our offending-verts list
						static const float fSameEps = 0.01f;
						bool bAlreadyGotLastV2 = false;
						bool bAlreadyGotV2 = false;

						for(s32 oi=0; oi<pStitchPoly->m_vOffendingVertices.GetCount(); oi++)
						{
							Vector3 vOffVert = pStitchPoly->m_vOffendingVertices[oi];

							if(Eq(vOffVert.x, pLastVertex2.x, fSameEps) && Eq(vOffVert.y, pLastVertex2.y, fSameEps))
							{
								bAlreadyGotLastV2 = true;
							}
							if(Eq(vOffVert.x, pVertex2.x, fSameEps) && Eq(vOffVert.y, pVertex2.y, fSameEps))
							{
								bAlreadyGotV2 = true;
							}
						}

						// Store these vertices.  Once we've identified all the other polys which are overlapping
						// with this poly, we will go about breaking all edges by offending vertices..
						// Add them sorted into the offending list.  This may need to be rewritten depending upon
						// the winding order & +/-Y direction of the world.  Best to debug thru & check out the
						// logic, along with the way in which the edge normal is pointing!

						switch(iCardinalAxis)
						{
							case XAxis:
							{
								if(!bAlreadyGotLastV2 && pLastVertex2.x > fTri1_MinExtent && pLastVertex2.x < fTri1_MaxExtent)
								{
									AddVertexSorted(
										pLastVertex2,
										pStitchPoly->m_vOffendingVertices,
										true, (sideOfNeighbour == eMinusX || sideOfNeighbour == ePlusY)
									);
								}
								if(!bAlreadyGotV2 && pVertex2.x > fTri1_MinExtent && pVertex2.x < fTri1_MaxExtent)
								{
									AddVertexSorted(
										pVertex2,
										pStitchPoly->m_vOffendingVertices,
										true, (sideOfNeighbour == eMinusX || sideOfNeighbour == ePlusY)
									);
								}
								break;
							}
							case YAxis:
							{
								if(!bAlreadyGotLastV2 && pLastVertex2.y > fTri1_MinExtent && pLastVertex2.y < fTri1_MaxExtent)
								{
									AddVertexSorted(
										pLastVertex2,
										pStitchPoly->m_vOffendingVertices,
										false, (sideOfNeighbour == eMinusX || sideOfNeighbour == ePlusY)
									);
								}
								if(!bAlreadyGotV2 && pVertex2.y > fTri1_MinExtent && pVertex2.y < fTri1_MaxExtent)
								{
									AddVertexSorted(
										pVertex2,
										pStitchPoly->m_vOffendingVertices,
										false, (sideOfNeighbour == eMinusX || sideOfNeighbour == ePlusY)
									);
								}
								break;
							}
							default:
								Assert(0);
						}

						lastv2 = v2;
					}

				} // iCardinalAxis

				lastv1 = v1;
			}
		}

		if(pStitchPoly->m_vOffendingVertices.GetCount())
		{
			stitchPolys.push_back(pStitchPoly);

			// Mark this poly with a temporary flag, so we know it is one of the ones which needs changing
			pPoly1->OrFlags(NAVMESHPOLY_NAVGEN_POLY_STITCHED);
		}
		else
		{
			pPoly1->AndFlags(~NAVMESHPOLY_NAVGEN_POLY_STITCHED);

			delete pStitchPoly;
		}
	}

	float fSharedEdgeVal;
	switch(sideOfNeighbour)
	{
		case ePlusX:
			fSharedEdgeVal = (pNavMesh->m_pQuadTree->m_Maxs.x + pOtherNavMesh->m_pQuadTree->m_Mins.x) * 0.5f;
			break;
		case eMinusX:
			fSharedEdgeVal = (pNavMesh->m_pQuadTree->m_Mins.x + pOtherNavMesh->m_pQuadTree->m_Maxs.x) * 0.5f;
			break;
		case ePlusY:
			fSharedEdgeVal = (pNavMesh->m_pQuadTree->m_Maxs.y + pOtherNavMesh->m_pQuadTree->m_Mins.y) * 0.5f;
			break;
		case eMinusY:
			fSharedEdgeVal = (pNavMesh->m_pQuadTree->m_Mins.y + pOtherNavMesh->m_pQuadTree->m_Maxs.y) * 0.5f;
			break;
		default:
			fSharedEdgeVal = 0;
			Assert(0);
			return NULL;
	}

	CNavMesh * pNewNavMesh = DoStitchingAlongEdge(pNavMesh, stitchPolys, fSharedEdgeVal, sideOfNeighbour);

	return pNewNavMesh;
}



CNavMesh *
CNavGen::DoStitchingAlongEdge(CNavMesh * pNavMesh, vector<TStitchPoly*> & stitchPolys, float fSharedEdgeVal, SideOfNeighbour sideOfNeighbour)
{
	const float fEdgeEps = 0.5f;

	Vector3 vVertex, vLastVertex;
	int iNumNewVertices = 0;

	for(u32 p=0; p<stitchPolys.size(); p++)
	{
		TStitchPoly * pStitchPoly = stitchPolys[p];
		TNavMeshPoly * pPoly = pNavMesh->GetPoly( pStitchPoly->m_iIndexInNavMesh );

		Assert(pStitchPoly->m_vOffendingVertices.GetCount() > 0);
		Assert(pPoly->GetNumVertices() >= 3);

		int iNumNewVertsInThisPoly = 0;

		int lastv = pPoly->GetNumVertices()-1;
		for(u32 v=0; v<pPoly->GetNumVertices(); v++)
		{
			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, lastv), vLastVertex );
			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, v), vVertex );

			const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly(pPoly->GetFirstVertexIndex() + lastv);

			pStitchPoly->m_NewVertices.PushAndGrow(vLastVertex);
			pStitchPoly->m_NewEdges.PushAndGrow(adjPoly);

			// Is this an edge which is along the boundary with the neighbouring navmesh ?
			float fComp1, fComp2;
			switch(sideOfNeighbour)
			{
				case eMinusX:
				case ePlusX:
					fComp1 = vLastVertex.x;
					fComp2 = vVertex.x;
					break;
				case eMinusY:
				case ePlusY:
					fComp1 = vLastVertex.y;
					fComp2 = vVertex.y;
					break;
				default:
					fComp1 = 0;
					fComp2 = 0;
					Assert(0);
					return false;
			}

			// Not this edge ?
			if(!(Eq(fComp1, fSharedEdgeVal, fEdgeEps) && Eq(fComp2, fSharedEdgeVal, fEdgeEps)))
			{
				lastv = v;
				continue;
			}

			// Step through the array of 'offending' vertices (ie. those which may lie along this edge)
			// and add vertices & edges to our new poly if necessary.  We needn't worry about ordering
			// here - since the array of offending vertices is already guatanteed to be sorted.

			static const float fSameValEps = 0.05f;

			for(s32 o=0; o<pStitchPoly->m_vOffendingVertices.GetCount(); o++)
			{
				Vector3 vOffVert = pStitchPoly->m_vOffendingVertices[o];

				// See if this vertex is extremely close to either of the existing vertices on this edge.
				// If so then we won't bother with it - we don't want to fuck things up by creating
				// degenerate edges, etc.
				switch(sideOfNeighbour)
				{
					case eMinusX:
					case ePlusX:
						if(Eq(vOffVert.y, vLastVertex.y, fSameValEps) || Eq(vOffVert.y, vVertex.y, fSameValEps))
						{
							//pStitchPoly->m_vOffendingVertices.erase(pStitchPoly->m_vOffendingVertices.begin() + o);
							pStitchPoly->m_vOffendingVertices.Delete(o);
							o--;
							continue;
						}
						break;
					case eMinusY:
					case ePlusY:
						if(Eq(vOffVert.x, vLastVertex.x, fSameValEps) || Eq(vOffVert.x, vVertex.x, fSameValEps))
						{
							//pStitchPoly->m_vOffendingVertices.erase(pStitchPoly->m_vOffendingVertices.begin() + o);
							pStitchPoly->m_vOffendingVertices.Delete(o);
							o--;
							continue;
						}
						break;
				}

				// Now see if this vertex is indeed between the two existing vertices on this edge.
				// If so, then we'll add it to our new poly.  If not then we may use it along another edge.
				// NB : We may need to interpolate over the edge to generate the exact vertex position.
				bool bAddVertex = false;

				switch(sideOfNeighbour)
				{
					case eMinusX:
						if(vOffVert.y > vVertex.y && vOffVert.y < vLastVertex.y)
						{
							bAddVertex = true;
						}
						break;
					case ePlusX:
						if(vOffVert.y > vLastVertex.y && vOffVert.y < vVertex.y)
						{
							bAddVertex = true;
						}
						break;
					case eMinusY:
						if(vOffVert.x > vLastVertex.x && vOffVert.x < vVertex.x)
						{
							bAddVertex = true;
						}
						break;
					case ePlusY:
						if(vOffVert.x > vVertex.x && vOffVert.x < vLastVertex.x)
						{
							bAddVertex = true;
						}
						break;
					default:
						Assert(0);
				}

				if(bAddVertex)
				{
					// Must make sure we've got space in the poly to add the new vertex.
					// Remember, we have an upper limit of NAVMESHPOLY_MAX_NUM_VERTICES.
					// If we have hit this limit, then we'll just have to avoid adding
					// any further vertices - resulting in a discontinuity between the
					// navmeshes at this poly..
					if(g_bLimitStitchPolySize && pPoly->GetNumVertices() + iNumNewVertsInThisPoly + 1 == NAVMESHPOLY_MAX_NUM_VERTICES)
					{
						char tmpDbg[256];				
						sprintf(tmpDbg, "WARNING : Couldn't add new vertex during navmesh stitching - max verts reached (side=%i)\n", sideOfNeighbour);
						printf(tmpDbg);
						OutputText(tmpDbg);
						sprintf(tmpDbg, "  poly %i, vertex is at (%.2f, %.2f, %.2f)\n", p, vOffVert.x, vOffVert.y, vOffVert.z);
						printf(tmpDbg);
						OutputText(tmpDbg);
					}
					else
					{
						iNumNewVertices++;

						TAdjPoly blankAdjPoly;

						pStitchPoly->m_NewVertices.PushAndGrow(vOffVert);
						pStitchPoly->m_NewEdges.PushAndGrow(adjPoly);

						pStitchPoly->m_vOffendingVertices.Delete(o);
						o--;

						iNumNewVertsInThisPoly++;
					}
				}
			}

			// next..
			lastv = v;
		}

		Assert(pStitchPoly->m_NewVertices.GetCount() >= 3);
		Assert((u32)pStitchPoly->m_NewVertices.GetCount() >= pPoly->GetNumVertices());
	}

	//******************************************************************************
	//
	//	Now we've got a big load of TStitchPoly's with which we need to replace the
	//	original polys in the NavMesh..
	//	The first things to do is make a new vertex pool, and then we can go thru
	//	each poly and (re)create it from scratch into a new buffer.
	//	We then free-up & copy over the old data members, and return the navmesh
	//	converted in-place.
	//
	//******************************************************************************

	// We'll decompress all the vertices into this pool, and then we'll add any new ones we need..
	
	static const float fVertexEqEps = 0.05f;

	vector<TVec3> newVertices;
	vector<u16> newVertexIndices;
	vector<TAdjPoly> newAdjPolys;
	vector<TNavMeshPoly> newPolys;
	vector<TPolyVolumeInfo> newVolumeInfo;

	s32 iPoolIndex = 0;

	for(u32 p=0; p<pNavMesh->GetNumPolys(); p++)
	{
		TNavMeshPoly & poly = *pNavMesh->GetPoly(p);

		TStitchPoly * pStitchPoly = NULL;
		if(poly.TestFlags(NAVMESHPOLY_NAVGEN_POLY_STITCHED))
		{
			for(u32 sp=0; sp<stitchPolys.size(); sp++)
			{
				if(stitchPolys[sp]->m_iIndexInNavMesh == (int)p)
				{
					pStitchPoly = stitchPolys[sp];
					break;
				}
			}
			Assert(pStitchPoly);
		}

		// If this polygon wasn't stitched, then simply add it to our lists
		if(!pStitchPoly)
		{
			TNavMeshPoly newPoly;
			memcpy(&newPoly, &poly, sizeof(TNavMeshPoly));
			newPoly.SetNumVertices(poly.GetNumVertices());
			newPoly.SetFirstVertexIndex(iPoolIndex);
			newPolys.push_back(newPoly);

			for(u32 a=0; a<poly.GetNumVertices(); a++)
			{
				TAdjPoly adjPoly = pNavMesh->GetAdjacentPoly(poly.GetFirstVertexIndex() + a);
				newAdjPolys.push_back(adjPoly);

				u32 iVertex = (u32)pNavMesh->GetPolyVertexIndex(&poly, a);
				pNavMesh->GetVertex(iVertex, vVertex);

				iVertex = FindVertexIndex(newVertices, vVertex, fVertexEqEps);
				if(iVertex == -1)
				{
					iVertex = (u32)newVertices.size();
					newVertices.push_back(TVec3(vVertex));
				}
				
				newVertexIndices.push_back((u16)iVertex);
			}

			iPoolIndex += poly.GetNumVertices();
		}

		//------------------------------------------------------------------------------------------
		// If this polygon was stitched, but had less than NAVMESHPOLY_MAX_NUM_VERTICES vertices,
		// then we can add it simply to the list as another poly

		else
		{
			//--------------------------------------------------------------------------

			poly.SetFlags(poly.GetFlags() & ~NAVMESHPOLY_NAVGEN_POLY_STITCHED);

			if (pStitchPoly->m_NewVertices.size() < NAVMESHPOLY_MAX_NUM_VERTICES)
			{
				TNavMeshPoly newPoly;
				memcpy(&newPoly, &poly, sizeof(TNavMeshPoly));
				newPoly.SetNumVertices(pStitchPoly->m_NewVertices.size());
				newPoly.SetFirstVertexIndex(iPoolIndex);
				newPolys.push_back(newPoly);

				for(s32 a=0; a<pStitchPoly->m_NewVertices.size(); a++)
				{
					TAdjPoly adjPoly = pStitchPoly->m_NewEdges[a];
					newAdjPolys.push_back(adjPoly);

					size_t iVertex = FindVertexIndex(newVertices, pStitchPoly->m_NewVertices[a], fVertexEqEps);
					if(iVertex==-1)
					{
						iVertex = newVertices.size();
						newVertices.push_back(TVec3(pStitchPoly->m_NewVertices[a]));
					}
					newVertexIndices.push_back((u16)iVertex);
				}

				iPoolIndex += pStitchPoly->m_NewVertices.size();
			}

			//------------------------------------------------------------------------------------------
			// If this polygon was stitched, and it ended up with more vertices that is possible for
			// a TNavMeshPoly then we must split into smaller polygons.

			else
			{
				//------------------------------------------------------------------------
				// First we should mark all surrounding polygons that their adjacencies
				// will need recalculating - because it will no longer be valid.

				/*
				for(u32 p2=0; p2<newPolys.size(); p2++)
				{
					if(p2==p)
						continue;
					const TNavMeshPoly & poly2 = newPolys[p2];
					for(u32 a=0; a<poly2.GetNumVertices(); a++)
					{
						TAdjPoly & adjPoly2 = newAdjPolys[poly2.GetFirstVertexIndex()+a];

						Assert(adjPoly2.GetOriginalNavMeshIndex() == adjPoly2.GetNavMeshIndex());
						Assert(adjPoly2.GetOriginalPolyIndex() == adjPoly2.GetPolyIndex());

						if(adjPoly2.GetNavMeshIndex()==pNavMesh->GetIndexOfMesh() && adjPoly2.GetPolyIndex()==p)
						{
							//-----------------------------------------------------------------------------------------
							// Later we will identify these edges which need their adjacency recalculated by the fact
							// that they have a valid navmesh index, but their poly index is NAVMESH_POLY_INDEX_NONE

							adjPoly2.SetOriginalPolyIndex(NAVMESH_POLY_INDEX_NONE);
							adjPoly2.SetPolyIndex(NAVMESH_POLY_INDEX_NONE);
						}
					}
				}
				*/

				//--------------------------------------------------------------------------
				// Now we create multiple fragment polygons from the original poly.
				// Each one will start at vertex zero.
				// Every internal adjacency which was previously valid, will have to be set
				// to NAVMESH_POLY_INDEX_NONE, whilst retaining a valid navmesh index.

				// TODO: select iStartingVertex to be the polygon vertex which is the furthest
				// distance away from the stitch-edge we are currently working on.
				// This should eliminate the problem of degenerate polygon fragments.

				int iStartingVertex = SelectStitchPolyStartingVertex(pStitchPoly, pNavMesh, sideOfNeighbour);
				int iCurrentVert = iStartingVertex+2;
				if(iCurrentVert >= pStitchPoly->m_NewVertices.size())
					iCurrentVert -= pStitchPoly->m_NewVertices.size();

				TAdjPoly blankAdjacency;

				while(iCurrentVert != iStartingVertex)
				{
					TNavMeshPoly polyFragment;
					memcpy(&polyFragment, &poly, sizeof(TNavMeshPoly));
					polyFragment.SetFirstVertexIndex(iPoolIndex);

					int iNumVerts = 0;

					//-----------------------------------------
					// Add the initial vertex (iStartingVertex)
					
					Vector3 vStartVert = pStitchPoly->m_NewVertices[iStartingVertex];
					size_t iStartVertex = FindVertexIndex(newVertices, vStartVert, fVertexEqEps);
					if(iStartVertex==-1)
					{
						iStartVertex = newVertices.size();
						newVertices.push_back(TVec3(vStartVert));
					}
					newVertexIndices.push_back((u16)iStartVertex);
					newAdjPolys.push_back(blankAdjacency);
					iPoolIndex++;
					iNumVerts++;

					//-----------------------------------------
					// Add the next vertex (iCurrentVert-1)

					s32 iNextVertex = iCurrentVert-1;
					if(iNextVertex < 0)
						iNextVertex += pStitchPoly->m_NewVertices.size();

					Vector3 vNextVert = pStitchPoly->m_NewVertices[iNextVertex];
					iNextVertex = FindVertexIndex(newVertices, vNextVert, fVertexEqEps);
					if(iNextVertex==-1)
					{
						iNextVertex = (u32)newVertices.size();
						newVertices.push_back(TVec3(vNextVert));
					}
					newVertexIndices.push_back((u16)iNextVertex);
					newAdjPolys.push_back(blankAdjacency);
					iPoolIndex++;
					iNumVerts++;

					//------------------------------------------------------
					// Loop across the vertices in the current pStitchPoly

					do
					{
						Vector3 vCurrVert = pStitchPoly->m_NewVertices[iCurrentVert];
						size_t iVertex = FindVertexIndex(newVertices, vCurrVert, fVertexEqEps);
						if(iVertex==-1)
						{
							iVertex = newVertices.size();
							newVertices.push_back(TVec3(vCurrVert));
						}
						newVertexIndices.push_back((u16)iVertex);
						newAdjPolys.push_back(blankAdjacency);

						iPoolIndex++;
						iNumVerts++;

						iCurrentVert++;
						if(iCurrentVert >= pStitchPoly->m_NewVertices.size())
							iCurrentVert -= pStitchPoly->m_NewVertices.size();
					}
					while(iNumVerts < NAVMESHPOLY_MAX_NUM_VERTICES-1 && iCurrentVert != iStartingVertex);

					polyFragment.SetNumVertices(iNumVerts);

					newPolys.push_back(polyFragment);
				}
			}
		}
	}

	//--------------------------------------------------
	// Now reconstruct the navmesh from the new arrays

	Assert(newVertexIndices.size() == newAdjPolys.size());

	bool bCreatedNewPolys = (pNavMesh->m_iNumPolys != newPolys.size());

	// Now set all this data in the navmesh..
	pNavMesh->m_iSizeOfPools = (int)newVertexIndices.size();
	pNavMesh->m_iNumVertices = (int)newVertices.size();
	pNavMesh->m_iNumPolys = (int)newPolys.size();

	delete pNavMesh->m_CompressedVertexArray;
	delete pNavMesh->m_VertexIndexArray;
	delete[] pNavMesh->m_VertexPool;
	delete pNavMesh->m_PolysArray;
	delete pNavMesh->m_AdjacentPolysArray;

	pNavMesh->m_VertexIndexArray = new aiSplitArray<u16, NAVMESH_VERTEXINDEX_ARRAY_BLOCKSIZE>((u32)newVertexIndices.size());

	pNavMesh->m_AdjacentPolysArray = new aiSplitArray<TAdjPoly, NAVMESH_ADJPOLY_ARRAY_BLOCKSIZE>((u32)newAdjPolys.size());

	pNavMesh->m_PolysArray = new aiSplitArray<TNavMeshPoly, NAVMESH_POLY_ARRAY_BLOCKSIZE>((u32)newPolys.size());
	pNavMesh->SetPolysSubArrayIndices();

	for(u32 p=0; p<newPolys.size(); p++)
	{
		TNavMeshPoly * pDestPoly = pNavMesh->GetPoly(p);
		memcpy(pDestPoly, &newPolys[p], sizeof(TNavMeshPoly));
	}

	for(u32 i=0; i<newVertexIndices.size(); i++)
	{
		*pNavMesh->GetVertexIndexArray().Get(i) = newVertexIndices[i];
		*pNavMesh->GetAdjacentPolysArray().Get(i) = newAdjPolys[i];
	}

	// Allocate & copy vertices.  Complicated slightly because they can be compressed or not..
	if(pNavMesh->m_iFlags & NAVMESH_COMPRESSED_VERTEX_DATA)
	{
		int iNumNewVertices = (int)newVertices.size();
		pNavMesh->m_CompressedVertexArray = new aiSplitArray<CNavMeshCompressedVertex, NAVMESH_COMPRESSEDVERTEX_ARRAY_BLOCKSIZE>(iNumNewVertices);
	}
	else
	{
		pNavMesh->m_VertexPool = rage_new Vector3[newVertices.size()];
	}

	for(u32 v=0; v<newVertices.size(); v++)
	{
		const TVec3 vert = newVertices[v];
		Vector3 vertex(vert.x, vert.y, vert.z);

		if(pNavMesh->m_iFlags & NAVMESH_COMPRESSED_VERTEX_DATA)
		{
			CNavMeshCompressedVertex * cVert = pNavMesh->GetCompressedVertexArray().Get(v);
			CompressVertex(
				vertex,
				cVert->data[0],
				cVert->data[1],
				cVert->data[2],
				pNavMesh->m_pQuadTree->m_Mins,
				pNavMesh->m_vExtents
			);
		}
		else
		{
			pNavMesh->m_VertexPool[v].x = vertex.x;
			pNavMesh->m_VertexPool[v].y = vertex.y;
			pNavMesh->m_VertexPool[v].z = vertex.z;
		}
	}

	for(u32 p=0; p<stitchPolys.size(); p++)
	{
		TStitchPoly * pStitchPoly = stitchPolys[p];
		delete pStitchPoly;
	}

	//------------------------------------------
	// Now we recalculate each poly's min/max

	if(bCreatedNewPolys)
	{
		for(u32 p=0; p<pNavMesh->GetNumPolys(); p++)
		{
			TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);
			Vector3 vMinXYZ(FLT_MAX, FLT_MAX, FLT_MAX);
			Vector3 vMaxXYZ(-FLT_MAX, -FLT_MAX, -FLT_MAX);

			for(u32 v=0; v<pPoly->GetNumVertices(); v++)
			{
				Vector3 vVertex;
				pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, v), vVertex);
				if(vVertex.x < vMinXYZ.x) vMinXYZ.x = vVertex.x;
				if(vVertex.y < vMinXYZ.y) vMinXYZ.y = vVertex.y;
				if(vVertex.z < vMinXYZ.z) vMinXYZ.z = vVertex.z;
				if(vVertex.x > vMaxXYZ.x) vMaxXYZ.x = vVertex.x;
				if(vVertex.y > vMaxXYZ.y) vMaxXYZ.y = vVertex.y;
				if(vVertex.z > vMaxXYZ.z) vMaxXYZ.z = vVertex.z;
			}
			vMinXYZ -= TShortMinMax::ms_vExtendMinMax;
			vMaxXYZ += TShortMinMax::ms_vExtendMinMax;
			pPoly->m_MinMax.SetFloat(vMinXYZ.x, vMinXYZ.y, vMinXYZ.z, vMaxXYZ.x, vMaxXYZ.y, vMaxXYZ.z);
		}
	}

	//-----------------------------------------------------------------------
	// Finally we shall have to re-establish the adjacencies within the mesh

	pNavMesh->EstablishPolyAdjacancy();

	if(bCreatedNewPolys && pNavMesh->m_pQuadTree)
	{
		// Recreate the quadtree for this navmesh
		const Vector3 vQuadTreeMin = pNavMesh->m_pQuadTree->m_Mins;
		const Vector3 vQuadTreeMax = pNavMesh->m_pQuadTree->m_Maxs;

		delete pNavMesh->m_pQuadTree;
		pNavMesh->m_pQuadTree = NULL;

		MakeNavMeshQuadTree(pNavMesh, vQuadTreeMin, vQuadTreeMax);
	}


	// NB : conversion was done in-place
	return pNavMesh;
}



}	// namespace rage

#endif
