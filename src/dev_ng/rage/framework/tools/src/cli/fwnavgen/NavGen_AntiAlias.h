#ifndef NAVGEN_ANTIALIAS_H
#define NAVGEN_ANTIALIAS_H

#include "vector\vector3.h"

namespace rage
{

// NAME : NavGen_AntiAlias
// PURPOSE :
// Classes & algorithms involved with reducing the aliasing artifacts of the navmesh geometry
// at its boundary edge.

class CNavGen;
class CNavGenNode;
class CNavSurfaceTri;

// Function type for detecting whether an edge is considered a boundary
typedef bool (*IsBoundaryEdgeFN)(const CNavSurfaceTri *, const s32 iEdge);
// Implementations
bool DefaultIsBoundaryEdgeFN(const CNavSurfaceTri * pTri, const s32 iEdge);
bool PavementIsBoundaryEdgeFN(const CNavSurfaceTri * pTri, const s32 iEdge);

// Function type for detecting whether a vertex move is valid
typedef bool (*IsValidMoveFN)(CNavGen * pNavGen, CNavGenNode * pNode, const Vector3 & vNewPosition,
							  CNavGenNode * pExteriorNode1, CNavGenNode * pExteriorNode2,
							  CNavSurfaceTri * pTri1, CNavSurfaceTri * pTri2);
// Implementations
bool IsValidNodeMoveForExternalEdge(
	CNavGen * pNavGen, CNavGenNode * pNode, const Vector3 & vNewPosition,
	CNavGenNode * pExteriorNode1, CNavGenNode * pExteriorNode2,
	CNavSurfaceTri * pTri1, CNavSurfaceTri * pTri2);
bool IsValidNodeMoveForPavementEdge(
	CNavGen * pNavGen, CNavGenNode * pNode, const Vector3 & vNewPosition,
	CNavGenNode * pExteriorNode1, CNavGenNode * pExteriorNode2,
	CNavSurfaceTri * pTri1, CNavSurfaceTri * pTri2);

// CLASS : CNavEdgeIterator
// PURPOSE : Simplifies/encapsulates the process of iterating across the navmesh's boundary edge
// Operates upon the in-construction mesh (CNavGenNode, CNavSurfaceTri, etc)


class CNavEdgeIterator
{
public:
	class CEdge
	{
	public:
		CEdge()
		{
			m_pTri = NULL;
			m_iEdge = -1;
		}
		CNavSurfaceTri * m_pTri;
		s32 m_iEdge;
	};
	enum EIterRet
	{
		ITER_ERROR		=	-1,
		ITER_CONTINUE	=	0,
		ITER_COMPLETE	=	1
	};
public:

	CNavEdgeIterator();
	~CNavEdgeIterator();

	// Initialise the iterator to start with the given triangle & edge
	bool Init(CNavSurfaceTri * pTri, s32 iEdge, IsBoundaryEdgeFN boundaryEdgeFn);

	// Get the current edge
	void GetCurrent(CNavSurfaceTri ** ppTri, s32 * piEdge, CNavGenNode ** ppNode1, CNavGenNode ** ppNode2);

	const CEdge & GetStarting() { return m_StartingEdge; }

	// Iterate to the next triangle & edge on the boundary edge, or return false if iteration is complete
	EIterRet Step(s32 iPass);

	void Reset()
	{
		m_StartingEdge.m_pTri = NULL;
		m_StartingEdge.m_iEdge = -1;
	}

protected:

	// The edge which the algorithm started on
	CEdge m_StartingEdge;

	// The current edge
	CEdge m_CurrentEdge;

	// Callback function to determine whether this is a boundary edge
	IsBoundaryEdgeFN m_pIsBoundaryEdgeFunction;

};


class CNavAntiAlias
{
public:

	CNavAntiAlias(CNavGen * pNavGen);

	void AntiAliasBoundaryEdges();
	void AntiAliasPavementEdges();
	void AntiAliasSteepSlopeEdges();

protected:

	CNavEdgeIterator::CEdge LocateBoundaryEdgeTriangle(IsBoundaryEdgeFN pFN, s32 iPass);
	void AntiAliasMeshEdges(IsBoundaryEdgeFN isEdgeFN, IsValidMoveFN isValidMoveFN);

	CNavGen * m_pNavGen;
};

} // rage

#endif // NAVGEN_ANTIALIAS_H
