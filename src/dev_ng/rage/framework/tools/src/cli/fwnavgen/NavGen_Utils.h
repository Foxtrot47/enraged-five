#ifndef NAVGENUTILS_H
#define NAVGENUTILS_H

//*********************************************************************************
//	Title	: NavGen_Stitch.cpp
//	Author	: James Broad
//	Date	: Jan 2005
//
//	A bunch of useful functions.
//
//*********************************************************************************

#include <vector>
using namespace std;

namespace rage
{

class CSplitPoly;
class CNavGenNode;
class CNavSurfaceTri;
class CNavGenTri;
class CNavResolutionArea;

}

#include "toolnavmesh.h"

// Note: this was copied from 'pathserver/pathserver_datatypes.h', but I don't think it's important that they match or anything. /FF
#define MAX_PATH_STACK_SIZE				2048	//1024

#include "fwnavgen/datatypes.h"

#include "system\xtl.h"	// Solves masses of compile errors (warning C4668). Read the RAGE 0.4 "readme.txt" for why this is needed.
#include "atl\array.h"
//#include "basetypes.h"
//#include "maths\vector.h"

// The macros below were copied from 'game/datatypes.h', so we don't have to pull that in.
// We may want to just swap them out with some RAGE equivalent. /FF

/*
 * Useful macros. All four macros assume nothing about type. The only macro 
 * to return a defined type is SIGN which returns an 's32' 
 */
#undef ABS
#undef MAX
#undef MIN
#undef SIGN

#define ABS(a)		(((a) < 0) ? (-(a)) : (a))
#define MAX(a, b)	(((a) > (b)) ? (a) : (b))
#define MIN(a, b)	(((a) < (b)) ? (a) : (b))
#define SIGN(a)		(s32)(((a) < 0) ? (-1) : (1))

namespace rage
{

void ReplaceExtension(char * pString, char * pExt);
void RemoveExtension(char * pString);
void StripFilename(char * pString);

bool IsNumericallyValid(const float f);

inline void atArrayInsert(atArray<Vector3> & vertices, const Vector3 & vert, int iPosition)
{
	vertices.PushAndGrow(vert);
	vertices.Pop();
	Vector3 & vEmptySlot = vertices.Insert(iPosition);
	vEmptySlot = vert;
}


// Returns whether the ray intersects the axis-aligned bounding-box
bool RayIntersectsAABB(const Vector3 & vRayStart, const Vector3 & vRayEnd, const Vector3 & vBoxMin, const Vector3 & vBoxMax);

// Returns whether the ray intersects the triangle, the intersection point is returned also
bool RayIntersectsTriangle(
	const Vector3 & vRayStart,
	const Vector3 & vRayEnd,
	Vector3 * pVerts,
	Vector3 & vecIntersect
);

bool TestRayTriangle(
	const Vector3 & vRayStart,
	const Vector3 & vRayEnd,
	const Vector3 & vTriNormal,
	Vector3 * pVerts
);

inline bool BBoxIntersect(const Vector3 & vMin1, const Vector3 & vMax1, const Vector3 & vMin2, const Vector3 & vMax2)
{
	if(vMin1.x > vMax2.x || vMin1.y > vMax2.y || vMin1.z > vMax2.z ||
		vMin2.x > vMax1.x || vMin2.y > vMax1.y || vMin2.z > vMax1.z)
	{
		return false;
	}

	return true;
}

inline bool BBoxIntersect2d(const Vector3 & vMin1, const Vector3 & vMax1, const Vector3 & vMin2, const Vector3 & vMax2)
{
	if(vMin1.x > vMax2.x || vMin1.y > vMax2.y ||
		vMin2.x > vMax1.x || vMin2.y > vMax1.y)
	{
		return false;
	}

	return true;
}

bool ComputeOverlapInterval(const Vector3 & vEdgeA_V1, const Vector3 & vEdgeA_V2, const Vector3 & vEdgeB_V1, const Vector3 & vEdgeB_V2, float & fOut_OverlapInterval);

void ProjectBoundingBoxOntoZPlane(
	const Vector3 & vBoxMinIn,
	const Vector3 & vBoxMaxIn,
	const Matrix34 & orientationMat,
	const float fPlaneZ,
	float & fTopZOut,
	float & fBottomZOut,
	Vector3 * pCornersOut,	 // array of 4 Vector3's
	Vector4 * pEdgePlanes,
	TShortMinMax & minMax);	// array of 4 Vector4's

bool DynamicObjectIntersectsPolygon(fwDynamicEntityInfo * pEntityInfo, int iNumPolyVerts, Vector3 * pPolyVerts, Vector4 * vPolyEdgePlanes);

enum ePlaneSide
{
	ePlaneFront,
	ePlaneBack,
	ePlaneOn,
	ePlaneStraddles
};

float GetPlaneDist(const Vector4 & vPlane, const Vector3 & vPos);

// Classify the given poly to the plane.
ePlaneSide ClassifyPolyToAxialPlane(CSplitPoly * pInputPoly, const u32 iPlaneAlignmentFlag, const float & fPlaneDist);

// Splits the given triangle to the plane.  Front & back fragments are created as applicable.
bool SplitPolyToPlane(CSplitPoly * pInputPoly, const Vector3 & vPlaneNormal, const float & fPlaneDist, const float fSplitPlaneEps, CSplitPoly ** frontPoly, CSplitPoly ** backPoly);

// Splits the given triangle to the plane.  Front & back fragments are created as applicable.
bool SplitPolyToAxialPlane(CSplitPoly * pInputPoly, const u32 iPlaneAlignmentFlag, const float & fPlaneDist, const float fSplitPlaneEps, CSplitPoly ** frontPoly, CSplitPoly ** backPoly);

static const float ONE_THIRD = (float) (1.0 / 3.0);

// Find the closest point in a triangle, to another given point.  This may be at a vertex, along an edge, or in the interior
float ClosestPointInTriangleToPoint(Vector3 * triPts, const Vector3 & pTargetPoint, Vector3 & outPoint);

void RandomPointInTriangle(Vector3 * triPts, Vector3 & vRandom);

int NumAdjacentTris(CNavSurfaceTri * pTri);

void TriangleCentroid(CNavSurfaceTri * pTri, Vector3 & vec);

void CalcTriNormal(const Vector3 * pPts, Vector3 & vNormal);

void PlaneFromPoints(Vector3 * pPts, Vector3 * vPlaneNormal, float & fPlaneDist);

int GetSharedNodes(CNavSurfaceTri * pTri1, CNavSurfaceTri * pTri2, CNavGenNode ** pNode1, CNavGenNode ** pNode2, CNavGenNode ** pNode3);
int GetSharedNodes(CNavGenNode ** pNodesList1, CNavGenNode ** pNodesList2, CNavGenNode ** pNode1, CNavGenNode ** pNode2, CNavGenNode ** pNode3);

CNavGenNode * GetOtherNode(CNavSurfaceTri * pTri1, CNavGenNode * pNode1, CNavGenNode * pNode2);

// ----- ENUMERATIONS & CONSTANTS -----
enum POINT_CLASSIFICATION
{
	ON_LINE,		// The point is on, or very near, the line
	LEFT_SIDE,		// looking from endpoint A to B, the test point is on the left
	RIGHT_SIDE		// looking from endpoint A to B, the test point is on the right
};

int LineSegsIntersect2D(const Vector3 & A, const Vector3 & B, const Vector3 & C, const Vector3 & D);

inline bool Equal(const Vector3 & v1, const Vector3 & v2, float fEps)
{
	return(Eq(v1.x, v2.x, fEps) && Eq(v1.y, v2.y, fEps) && Eq(v1.z, v2.z, fEps));
}

// l0,l1,l2 are the length of each triangle edge
inline float CalcTriArea(float l0, float l1, float l2)
{
	// Calculate the tri area from the edge lengths
	float p = l0 + l1 + l2;
	float val = p * (p - (2.0f * l0)) * (p - (2.0f * l1)) * (p - (2.0f * l2));
	static const float fminval = 0.0001f;
	if(val < fminval)
		return 0.0f;

	float a = 0.25f * rage::Sqrtf(val);
	return a;
}
inline float CalcTriArea(Vector3 * pPts)
{
	Vector3 e1 = pPts[1] - pPts[0];
	Vector3 e2 = pPts[2] - pPts[1];
	Vector3 e3 = pPts[0] - pPts[2];
	return CalcTriArea(e1.Mag(), e2.Mag(), e3.Mag());
}
inline float CalcTriArea(Vector3 & p0, Vector3 & p1, Vector3 & p2)
{
	Vector3 e1 = p1 - p0;
	Vector3 e2 = p2 - p1;
	Vector3 e3 = p0 - p2;
	return CalcTriArea(e1.Mag(), e2.Mag(), e3.Mag());
}
float CalcPolyAreaXY(atArray<Vector3> & pts);

void GetRandomPointInTriangle(const Vector3 & v1, const Vector3 & v2, const Vector3 & v3, Vector3 & vOut);

int GetNodeIndex(CNavGenNode ** pNodes, CNavGenNode * pNode);
int GetWindingOrder(CNavGenNode ** pNodes, CNavGenNode * pNode1, CNavGenNode * pNode2);

void CompressVertex(const Vector3 & vertex, u16 & iX, u16 & iY, u16 & iZ, const Vector3 & vMin, const Vector3 & vSize);
void DecompressVertex(Vector3 & vertex, const u16 & iX, const u16 & iY, const u16 & iZ, const Vector3 & vMin, const Vector3 & vSize);

// Reports if we've run out of memory
void OutOfMemory(char * szReason);

// Reports a general error
void ErrorMsg(char * szMessage);




TNavMeshPoly * UtilGetClosestNavMeshPolyEdge(CNavMesh * pNavMesh, const Vector3 & vPos, const float fMaxDistToLook, Vector3 & vOutClosestPosOnEdge, bool bOnlyWater=false, bool bNotWater=false);
void UtilGetClosestNavMeshPolyEdgeR(CNavMesh * pNavMesh, const TShortMinMax & minMax, CNavMeshQuadTree * pTree, bool bOnlyWater, bool bNotWater);

extern const u32 g_CRCTable[256];
inline void CalcCrc(const u8 byte, u32 & dwCrc32)
{
	dwCrc32 = ((dwCrc32) >> 8) ^ g_CRCTable[(byte) ^ ((dwCrc32) & 0x000000FF)];
}
inline void CalcCrc(const float f, u32 & dwCrc32)
{
	u8 * ptr = (u8*)&f;
	CalcCrc(ptr[0], dwCrc32);
	CalcCrc(ptr[1], dwCrc32);
	CalcCrc(ptr[2], dwCrc32);
	CalcCrc(ptr[3], dwCrc32);
}
inline void CalcCrc(const u32 i, u32 & dwCrc32)
{
	u8 * ptr = (u8*)&i;
	CalcCrc(ptr[0], dwCrc32);
	CalcCrc(ptr[1], dwCrc32);
	CalcCrc(ptr[2], dwCrc32);
	CalcCrc(ptr[3], dwCrc32);
}
inline void CalcCrc(const Vector3 & v, u32 & dwCrc32)
{
	CalcCrc(v.x, dwCrc32);
	CalcCrc(v.y, dwCrc32);
	CalcCrc(v.z, dwCrc32);
}

// Calculates the CRC for a file, first skipping the specified num bytes
u32 CalcFileCRC(char * pFullFileName, s32 iSkipThisManyLeadingBytes=0);

// Reads the CRC value stored in the following file types.
// NOTE THAT THIS IS THE CRC OF THE "TRI" FILE WHICH THESE FILES ARE ASSOCIATED WITH.
bool ReadSrfFileCRCs(char * pFullFileName, u32 & iCollisionCRC, u32 & iResAreasCRC);

// Calculate the CRC of the navmesh resolution areas which intersect the given area
u32 CalcResAreasCRC(vector<CNavResolutionArea*> & areas, const Vector3 & vAreaMin, const Vector3 & vAreaMax);

struct TMergePoly;

bool DoWeCareAboutThisSurfaceType(u16 iSurfaceType);
bool CanWeMergeThesePolys(TMergePoly * pPoly1, TMergePoly * pPoly2);

bool TestMergePolyObjectIntersection(TMergePoly * pPoly, fwDynamicEntityInfo * pObj);

inline void
ScanFloat(char * str, float & val) 
{
	// get sgn
	float sgn;
	if(*str == '-') 
  {
		sgn = -1.0f;
		str++;
	}
	else 
		sgn = 1.0;

	// read left side
	double left = 0;
	while(*str != '.' && *str >= '0' && *str <= '9')
		left = (left * 10.0f) + (*str++ - 48);

	// read right side
	double right = 0;
	int pts = 0;

	if(*str == '.') 
  {
		str++;
		while(*str != ' ' && *str != '\t' && *str != '\n' && *str != '\r' && *str != '\0') 
    {
			right = (right * 10.0f) + (*str++ - 48);
			pts++;
		}
	}

	val = (float) (left + (right / (pow(10.0f,pts))) );

	val *= sgn;
}


inline void
ScanInt(char * str, int & val) 
{
	// get sgn
	int sgn;
	if(*str == '-') 
  {
		sgn = -1;
		str++;
	}
	else 
		sgn = 1;

	// read
	val = 0;
	while(*str != ' ' && *str != '\t' && *str != '\n' && *str != '\r' && *str != '\0') 
  {
		val = (val * 10) + (*str++ - 48);
	}

	val *= sgn;
}


u8 FindCoverDirFromVector(const Vector3 & Dir);
Vector3 FindVectorFromCoverDir(u8 Dir);


template<class T> class CNavGenPriorityQueue
{
	struct CNode
	{
		T * m_pItem;
		CNode * m_pGreaterChild;
		CNode * m_pSmallerChild;
		CNode * m_pParent;
	};

public:

	inline CNavGenPriorityQueue(void)
	{
		m_pRootNode = NULL;
		m_pGreatestElement = NULL;
		m_pSmallestElement = NULL;
		m_iNumElements = NULL;
	}

	inline ~CNavGenPriorityQueue(void)
	{
		Clear();
	}

	inline void Clear(void)
	{
		Clear(m_pRootNode);
		m_pRootNode = NULL;
		m_pGreatestElement = NULL;
		m_pSmallestElement = NULL;
		m_iNumElements = NULL;
	}

	inline void Clear(CNode * pNode)
	{
		if(pNode)
		{
			if(pNode->m_pGreaterChild)
			{
				Clear(pNode->m_pGreaterChild);
			}
			if(pNode->m_pSmallerChild)
			{
				Clear(pNode->m_pSmallerChild);
			}

			delete pNode;
		}
	}

	inline void Insert(T * pItem)
	{
		if(m_pRootNode)
		{
			Insert(m_pRootNode, pItem);
		}
		else
		{
			m_pRootNode = rage_new CNode;
			m_pRootNode->m_pGreaterChild = NULL;
			m_pRootNode->m_pSmallerChild = NULL;
			m_pRootNode->m_pParent = NULL;
			m_pRootNode->m_pItem = pItem;

			m_pGreatestElement = m_pRootNode;
			m_pSmallestElement = m_pRootNode;

			m_iNumElements++;
		}
	}

	inline void Insert(CNode * pNode, T * pItem)
	{
		// Greater than node item ?
		if((*pItem) > (*pNode->m_pItem))
		{
			if(pNode->m_pGreaterChild)
			{
				Insert(pNode->m_pGreaterChild, pItem);
			}
			else
			{
				pNode->m_pGreaterChild = rage_new CNode;
				pNode->m_pGreaterChild->m_pGreaterChild = NULL;
				pNode->m_pGreaterChild->m_pSmallerChild = NULL;
				pNode->m_pGreaterChild->m_pParent = pNode;
				pNode->m_pGreaterChild->m_pItem = pItem;

				if(*pItem > *(m_pGreatestElement->m_pItem))
				{
					m_pGreatestElement = pNode->m_pGreaterChild;
				}

				m_iNumElements++;
			}
		}
		// Smaller or equal to node item
		else
		{
			if(pNode->m_pSmallerChild)
			{
				Insert(pNode->m_pSmallerChild, pItem);
			}
			else
			{
				pNode->m_pSmallerChild = rage_new CNode;
				pNode->m_pSmallerChild->m_pGreaterChild = NULL;
				pNode->m_pSmallerChild->m_pSmallerChild = NULL;
				pNode->m_pSmallerChild->m_pParent = pNode;
				pNode->m_pSmallerChild->m_pItem = pItem;

				if(*pItem < *(m_pSmallestElement->m_pItem))
				{
					m_pSmallestElement = pNode->m_pSmallerChild;
				}

				m_iNumElements++;
			}
		}
	}

	inline T* PopGreatest(void)
	{
		if(m_pGreatestElement)
		{
			T * pItem = m_pGreatestElement->m_pItem;
			CNode * pTmp = m_pGreatestElement;

			if(m_pGreatestElement->m_pParent)
			{
				if(m_pGreatestElement->m_pSmallerChild)
				{
					m_pGreatestElement->m_pParent->m_pGreaterChild = m_pGreatestElement->m_pSmallerChild;
					m_pGreatestElement->m_pSmallerChild->m_pParent = m_pGreatestElement->m_pParent;
					m_pGreatestElement = m_pGreatestElement->m_pSmallerChild;
				}
				else
				{
					m_pGreatestElement->m_pParent->m_pGreaterChild = NULL;
					m_pGreatestElement = m_pGreatestElement->m_pParent;
				}
			}
			else
			{
				m_pGreatestElement = m_pGreatestElement->m_pSmallerChild;
				m_pGreatestElement->m_pParent = NULL;
				m_pRootNode = m_pGreatestElement;
			}

			delete pTmp;
			m_iNumElements--;
			return pItem;
		}
		else
		{
			return NULL;
		}
	}



	inline T* PopSmallest(void)
	{
		if(m_pSmallestElement)
		{
			T * pItem = m_pSmallestElement->m_pItem;
			CNode * pTmp = m_pSmallestElement;

			if(m_pSmallestElement->m_pParent)
			{
				if(m_pSmallestElement->m_pGreaterChild)
				{
					m_pSmallestElement->m_pParent->m_pSmallerChild = m_pSmallestElement->m_pGreaterChild;
					m_pSmallestElement->m_pGreaterChild->m_pParent = m_pSmallestElement->m_pParent;
					m_pSmallestElement = m_pSmallestElement->m_pGreaterChild;
				}
				else
				{
					m_pSmallestElement->m_pParent->m_pSmallerChild = NULL;
					m_pSmallestElement = m_pSmallestElement->m_pParent;
				}
			}
			else
			{
				m_pSmallestElement = m_pSmallestElement->m_pGreaterChild;
				m_pSmallestElement->m_pParent = NULL;
				m_pRootNode = m_pSmallestElement;
			}

			delete pTmp;
			m_iNumElements--;
			return pItem;
		}
		else
		{
			return NULL;
		}
	}

	inline T* GetGreatest(void)
	{
		if(m_pGreatestElement)
		{
			return m_pGreatestElement->m_pItem;
		}
		else
		{
			return NULL;
		}
	}

	inline T* GetSmallest(void)
	{
		if(m_pSmallestElement)
		{
			return m_pSmallestElement->m_pItem;
		}
		else
		{
			return NULL;
		}
	}

	inline void AddAsGreatest(T * pItem)
	{
		CNode * pNode = rage_new CNode;
		pNode->m_pGreaterChild = NULL;
		pNode->m_pSmallerChild = NULL;
		pNode->m_pItem = pItem;

		if(m_pGreatestElement)
		{
			pNode->m_pParent = m_pGreatestElement;
		}
		else
		{
			m_pRootNode = pNode;
		}

		m_pGreatestElement = pNode;
		m_iNumElements++;
	}

	inline void AddAsSmallest(T * pItem)
	{
		CNode * pNode = rage_new CNode;
		pNode->m_pGreaterChild = NULL;
		pNode->m_pSmallerChild = NULL;
		pNode->m_pItem = pItem;

		if(m_pSmallestElement)
		{
			pNode->m_pParent = m_pSmallestElement;
		}
		else
		{
			m_pRootNode = pNode;
		}

		m_pSmallestElement = pNode;
		m_iNumElements++;
	}

	inline int Size(void) { return m_iNumElements; }

private:

	CNode * m_pRootNode;
	CNode * m_pGreatestElement;
	CNode * m_pSmallestElement;

	int m_iNumElements;

};

// Name			:	GetAngleBetweenPoints
// Purpose		:	Returns the angle between 2 points. 
// Parameters	:	fX1, fY1 - point 1 x and y coords
//					fX2, fY2 - point 2 x and y coords
// Returns		:	The angle FROM point 2, TO point 1
inline float GetAngleBetweenPoints(float fX1, float fY1, float fX2, float fY2)
{
	static const float fHalfPI = PI / 2.0f;
	static const float f2PI = PI*2.0f;

	float fReturnAngle;
	float fXdiff, fYdiff;

	// Get XY differences
	fXdiff = fX2 - fX1; 
	fYdiff = fY2 - fY1; 

	// Stop divide by zero
	if (fYdiff == 0.0f)
	{
		fYdiff = 0.0001f;
	}

	if (fXdiff > 0.0f)
	{
		if (fYdiff > 0.0f)
		{
			// Lower left quad
			fReturnAngle = fHalfPI + (fHalfPI - (atan(fXdiff / fYdiff)));
		}
		else
		{
			// Upper Left quad
			fReturnAngle = fHalfPI - (fHalfPI + (atan(fXdiff / fYdiff)));
		}
	}
	else
	{
		if(fYdiff > 0.0f)
		{
			// Lower right quad
			fReturnAngle = -fHalfPI - (fHalfPI + (atan(fXdiff / fYdiff)));
		}
		else
		{
			// Upper right quad
			fReturnAngle = -fHalfPI + (fHalfPI - (atan(fXdiff / fYdiff)));
		}
	}

	while(fReturnAngle < 0.0f) fReturnAngle += f2PI;
	while(fReturnAngle > f2PI) fReturnAngle -= f2PI;

	return (fReturnAngle);
} // end - CAngle::GetAngleBetweenPoints

Vector3 GetCenter(const Vector3 & vMinAABB, const Vector3 & vMaxAABB, const Matrix34 & mat);

inline int ReadLine(fiStream * pFile, char * pBuffer, int iBufferSize)
{
	int i = 0;
	char c;
	int iNum = pFile->ReadByte(&c, 1);
	
	while(iNum > 0 && c != 13 && c != 10 && i < iBufferSize-1)
	{
		pBuffer[i++] = c;
		iNum = pFile->ReadByte(&c, 1);
	}
	pBuffer[i] = 0;

	return i;
}

//*************************************************************************************************8

class CBitArray 
{
	public:

		CBitArray();
		~CBitArray();

		bool Alloc(int num_bits);
  
		inline void Set(int index)
		{
			int indexByte = index / 8;
			int bit = index % 8;
			Assert(m_Bits);
			m_Bits[indexByte] |= (1<<bit);
  		}
		inline void Reset(int index)
		{
			int indexByte = index / 8;
			int bit = index % 8;
			Assert(m_Bits);
			m_Bits[indexByte] &= ~(1<<bit);
		}
		inline bool IsSet(int index)
		{
			int indexByte = index / 8;
			int bit = index % 8;
			Assert(m_Bits);
			if(m_Bits[indexByte] & (1<<bit)) return TRUE;
			else return FALSE;
		}
		inline bool IsClear(int index) { return !IsSet(index); }

		void Clear();
		void SetAll();

		char * m_Bits;       //lint !e1925 // This is a public data member! Naughty!  
		int    m_NumBytes;   //lint !e1925 // This is a public data member! Naughty!
		int    m_NumBits;    //lint !e1925 // This is a public data member! Naughty!
};

}	// namespace rage

//*************************************************************************************************8
#endif