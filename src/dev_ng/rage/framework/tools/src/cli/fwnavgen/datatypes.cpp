//
// fwnavgen/datatypes.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "datatypes.h"
#include "parser/manager.h"

namespace rage
{

// Parse DLC area XML files in input strin
// The input string may be a single file, or multiple files separated by semicolons (no whitespace)
bool CDLCArea::LoadDLCAreas(const char * pInputFilenameString, atArray<CDLCArea*> & areas)
{
	Assert(pInputFilenameString);

	areas.clear();

	char buffer[2048];
	atArray<const char*> filenames;

	const char * pStart = pInputFilenameString;
	const char * pCurr = pInputFilenameString;

	while(1)
	{
		if(*pCurr == ';' || *pCurr == 0)
		{
			strncpy(buffer, pStart, (pCurr-pStart));
			buffer[(pCurr-pStart)] = 0;

			filenames.PushAndGrow(StringDuplicate(buffer));

			if(!*pCurr)
				break;

			pCurr++;
			pStart=pCurr;
		}
		pCurr++;
	}

	INIT_PARSER;

	for(s32 s=0; s<filenames.GetCount(); s++)
	{
		const char * pFilename = filenames[s];

		parTree * pTree = PARSER.LoadTree(pFilename, "");
		Assert(pTree);
		if(!pTree)
			continue;

		const parTreeNode* pRootNode = pTree->GetRoot();
		Assert(pRootNode);
		Assert(stricmp(pRootNode->GetElement().GetName(), "DLCAreas") == 0);

		// Make sure we have the correct version.
		ASSERT_ONLY(int iNumAreas = pRootNode->GetElement().FindAttributeIntValue("NumAreas", 0);)

		parTreeNode::ChildNodeIterator i = pRootNode->BeginChildren();
		for(; i != pRootNode->EndChildren(); ++i)
		{
			if(stricmp((*i)->GetElement().GetName(), "Areas") == 0)
			{
				// Go over any and all the child DOM trees and DOM nodes off of the list.
				int areaIndex = 0;
				parTreeNode::ChildNodeIterator j = (*i)->BeginChildren();
				for(; j != (*i)->EndChildren(); ++j)
				{
					Assert(stricmp((*j)->GetElement().GetName(), "Area") == 0);
					Assertf(areaIndex < iNumAreas, "More areas than expected encountered.");

					CDLCArea * pArea = rage_new CDLCArea();
					areas.PushAndGrow(pArea);

					pArea->m_vMin.x = (*j)->GetElement().FindAttributeFloatValue("MinX", 0.0f);
					pArea->m_vMin.y = (*j)->GetElement().FindAttributeFloatValue("MinY", 0.0f);
					pArea->m_vMin.z = (*j)->GetElement().FindAttributeFloatValue("MinZ", 0.0f);
					pArea->m_vMax.x = (*j)->GetElement().FindAttributeFloatValue("MaxX", 0.0f);
					pArea->m_vMax.y = (*j)->GetElement().FindAttributeFloatValue("MaxY", 0.0f);
					pArea->m_vMax.z = (*j)->GetElement().FindAttributeFloatValue("MaxZ", 0.0f);

					pArea->m_iFlags = (*j)->GetElement().FindAttributeIntValue("Flags", 0);

					areaIndex++;
				}
			}
		}

		delete pTree;
		StringFree(pFilename);
	}

	return true;
}

}

// End of file fwnavgen/datatypes.cpp
