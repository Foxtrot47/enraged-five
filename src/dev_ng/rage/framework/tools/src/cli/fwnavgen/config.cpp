//
// fwnavgen/config.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "fwnavgen/config.h"
#include "parser/manager.h"

using namespace rage;

#include "fwnavgen/config_parser.h"

//-----------------------------------------------------------------------------

fwNavGenConfigManager* fwNavGenConfigManager::sm_Instance = NULL;

void fwNavGenConfigManager::ShutdownClass()
{
	// ShutdownClass() should be matched with InitClass() calls, so if this
	// fails, something may have gone wrong.
	Assert(sm_Instance);

	delete sm_Instance;
	sm_Instance = NULL;
}


void fwNavGenConfigManager::Load(const char* pConfigFileName)
{
	// Delete any previous configuration object, and create a new one.
	delete m_Config;
	m_Config = CreateConfig();

	bool loaded = false;
	if(pConfigFileName && pConfigFileName[0])
	{
		// Try to load the configuration file.
		if(PARSER.InitAndLoadObject(pConfigFileName, "", *m_Config))
		{
			loaded = true;
		}
		else
		{
			Errorf("Failed to load configuration file '%s'.", pConfigFileName);
		}
	}

	if(!loaded)
	{
		// If it didn't load, we still need to make sure we get the default values in there.
		PARSER.InitObject(*m_Config);
	}
}


fwNavGenConfigManager::fwNavGenConfigManager()
		: m_Config(NULL)
{
}


fwNavGenConfigManager::~fwNavGenConfigManager()
{
	delete m_Config;
}

//-----------------------------------------------------------------------------

/* End of file fwnavgen/config.cpp */
