//
// fwnavgen/baseexportcollision.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef FWNAVGEN_BASEEXPORTCOLLISION_H
#define FWNAVGEN_BASEEXPORTCOLLISION_H

#include "basetool.h"

#include "phcore/materialmgr.h"		// phMaterialMgr::Id
#include "vector/vector3.h"

#include "fwgeovis/geovis.h"

#include <vector>					// vector<>

namespace rage
{

class fiStream;
class fragType;
class fwNavMeshMaterialInterface;
class fwNavTriData;
class fwEntity;
class phBound;
class phBoundBox;
class phBoundBVH;
class phBoundCapsule;
class phBoundCylinder;
class phBoundGeometry;
class phBoundSphere;
class spdSphere;
class Vector2;
class Vector3;
struct fwNavLadderInfo;

//-----------------------------------------------------------------------------

// TODO: Maybe move these back to the .cpp file if possible later.
#define __CHECK_TRIANGLES_AGAINST_NAVMESH_EXTENTS	1

//-----------------------------------------------------------------------------

enum eExportPolyFlags
{
	EXPORTPOLYFLAG_FIXEDOBJECT						=	1,
	EXPORTPOLYFLAG_DONT_CREATE_NAVMESH_ON_THIS		=	2,
	EXPORTPOLYFLAG_INTERIOR							=	4,
	EXPORTPOLYFLAG_DOESNTPROVIDECOVER				=	8,
	EXPORTPOLYFLAG_UNUSED1							=	16,
	EXPORTPOLYFLAG_VEHICLE							=	32,
	EXPORTPOLYFLAG_CLIMBABLEOBJECT					=	64,
	EXPORTPOLYFLAG_EXTERIOR_PORTAL					=	128,
	EXPORTPOLYFLAG_NOT_CLIMBABLE_COLLISION			=	256
};

//-----------------------------------------------------------------------------

class fwExportCollisionBaseTool : public fwLevelProcessToolExporterInterface
{
public:

	fwExportCollisionBaseTool(fwLevelProcessTool * pTool);
	virtual ~fwExportCollisionBaseTool();

	virtual bool OpenTriFile(const char *pNameToUse) = 0;

	//************************************************************************************************
	// This writes a ".tri" file containing the collision geometry.  Bound files cannot have more
	// vertex indices greater than 65535, which is often too small for for us here..
	//************************************************************************************************

	bool BaseOpenTriFile(const char *pNameToUse, const Vector2 &worldMin,
			const Vector3 &navMeshMins, const Vector3 &navMeshMaxs,
			s32 currentSectorX, s32 currentSectorY,
			int numSectorsPerNavMesh, float worldWidthOfSector);

	bool FlushTriListToTriFile(std::vector<Vector3> & triangleVertices, std::vector<phMaterialMgr::Id> & triangleMaterials, std::vector<u16> & colPolyFlags, std::vector<u32> & archetypeFlags, std::vector<fwNavTriData> & triDataArray);

	void CloseTriFile(bool setExportFlags = false);

	bool WriteExtrasFile(const char *pFilename, const std::vector<fwNavLadderInfo> & laddersList, const atArray<Vector3> & carNodesList, const std::vector<spdSphere> & portalBoundaries, const std::vector<fwEntity*> & dynamicEntities);

	void GetOutputFolder();

	// Given an input triangle, this function chops it to the extents of the navmesh(*) and adds the fragments to the list.
	// (*) The extents are the ACTUAL min/max of the navmesh & not the extended min/max - although we may make it *fractionally* larger so that height-samples
	// don't fall down the cracks between navmeshes.
	bool CheckTriangleAgainstNavMeshExtentsAndAddToList(const Vector3 pVerts[3], phMaterialMgr::Id iMaterial, std::vector<Vector3> & triangleVertices, std::vector<phMaterialMgr::Id> & triangleMaterials, std::vector<u16> & colPolyFlags, u16 iColPolyFlag, std::vector<u32> & archetypeFlags, u32 iArchetypeFlags, std::vector<fwNavTriData> & triDataArray, const fwNavTriData & triData);

	void ExportBound(const phBound * pBound, const fragType * pFragType, const Matrix34 & mat, std::vector<Vector3> & triangleVertices, std::vector<phMaterialMgr::Id> & triangleMaterials, std::vector<u16> & colPolyFlags, u16 iColPolyFlag, std::vector<u32> & archetypeFlags, u32 iArchetypeFlags, std::vector<fwNavTriData> & triDataArray, const fwNavTriData & triData);

	//**********************************************************************************
	// Given a phBoundOctree, this will add its triangles to the given pNavMeshBound
	//**********************************************************************************

	void AddBVHTrianglesToList(const rage::phBoundBVH * pBoundBVH,                    const Matrix34 & mat, std::vector<Vector3> & triangleVertices, std::vector<phMaterialMgr::Id> & triangleMaterials, std::vector<u16> & colPolyFlags, u16 iColPolyFlag, std::vector<u32> & archetypeFlags, u32 iArchetypeFlags, std::vector<fwNavTriData> & triDataArray, const fwNavTriData & triData);
	void AddCylinderTrianglesToList(const rage::phBoundCylinder * pBoundCylinder,     const Matrix34 & mat, std::vector<Vector3> & triangleVertices, std::vector<phMaterialMgr::Id> & triangleMaterials, std::vector<u16> & colPolyFlags, u16 iColPolyFlag, std::vector<u32> & archetypeFlags, u32 iArchetypeFlags, std::vector<fwNavTriData> & triDataArray, const fwNavTriData & triData);
	void AddCapsuleTrianglesToList(const rage::phBoundCapsule * pBoundCapsule,        const Matrix34 & mat, std::vector<Vector3> & triangleVertices, std::vector<phMaterialMgr::Id> & triangleMaterials, std::vector<u16> & colPolyFlags, u16 iColPolyFlag, std::vector<u32> & archetypeFlags, u32 iArchetypeFlags, std::vector<fwNavTriData> & triDataArray, const fwNavTriData & triData);
	void AddSphereTrianglesToList(const rage::phBoundSphere * pBoundSphere,           const Matrix34 & mat, std::vector<Vector3> & triangleVertices, std::vector<phMaterialMgr::Id> & triangleMaterials, std::vector<u16> & colPolyFlags, u16 iColPolyFlag, std::vector<u32> & archetypeFlags, u32 iArchetypeFlags, std::vector<fwNavTriData> & triDataArray, const fwNavTriData & triData);
	void AddBoxTrianglesToList(const rage::phBoundBox * pBoundBox,                    const Matrix34 & mat, std::vector<Vector3> & triangleVertices, std::vector<phMaterialMgr::Id> & triangleMaterials, std::vector<u16> & colPolyFlags, u16 iColPolyFlag, std::vector<u32> & archetypeFlags, u32 iArchetypeFlags, std::vector<fwNavTriData> & triDataArray, const fwNavTriData & triData);
	void AddGeometryTrianglesToList(const rage::phBoundGeometry * phBoundGeometry,    const Matrix34 & mat, std::vector<Vector3> & triangleVertices, std::vector<phMaterialMgr::Id> & triangleMaterials, std::vector<u16> & colPolyFlags, u16 iColPolyFlag, std::vector<u32> & archetypeFlags, u32 iArchetypeFlags, std::vector<fwNavTriData> & triDataArray, const fwNavTriData & triData);

	#define ADD_POLY_TO_LIST_PARAMS \
		std::vector<Vector3>			& triangleVertices, \
		std::vector<phMaterialMgr::Id>	& triangleMaterials,	phMaterialMgr::Id	mtlID, \
		std::vector<u16>				& colPolyFlags,			u16					iColPolyFlag, \
		std::vector<u32>				& archetypeFlags,		u32					iArchetypeFlags, \
		std::vector<fwNavTriData>		& triDataArray,			const fwNavTriData	& triData

	#define ADD_POLY_TO_LIST_VARS \
		triangleVertices, \
		triangleMaterials,	mtlID, \
		colPolyFlags,		iColPolyFlag, \
		archetypeFlags,		iArchetypeFlags, \
		triDataArray,		triData

	bool AddPolyToList(const Vec3V * points, int numPoints, bool bForce, ADD_POLY_TO_LIST_PARAMS);

	void AddRadialVolumeTrianglesToList(Vec3V_In vPoint0, Vec3V_In vPoint1, Vec3V_In vBasisX, Vec3V_In vBasisY, const float* radii, const float* dists, int iNumSlices, int iNumStacks, ADD_POLY_TO_LIST_PARAMS);
	void AddCylinderVolumeTrianglesToList(Vec3V_In vPoint0, Vec3V_In vPoint1, Vec3V_In vBasisX, Vec3V_In vBasisY, float fRadius, int iNumSlices, ADD_POLY_TO_LIST_PARAMS);
	void AddCapsuleVolumeTrianglesToList(Vec3V_In vPoint0, Vec3V_In vPoint1, Vec3V_In vBasisX, Vec3V_In vBasisY, float fRadius, int iNumSlices, int iNumCapStacks, ADD_POLY_TO_LIST_PARAMS);
	void AddSphereVolumeTrianglesToList(Vec3V_In vCentre, Vec3V_In vBasisX, Vec3V_In vBasisY, Vec3V_In vBasisZ, float fRadius, int iNumSlices, int iNumStacks, ADD_POLY_TO_LIST_PARAMS);
	void AddBoxVolumeTrianglesToList(Vec3V_In vCentre, Vec3V_In vHalfExtent, Vec3V_In vBasisX, Vec3V_In vBasisY, Vec3V_In vBasisZ, ADD_POLY_TO_LIST_PARAMS);

	//*******************************************************************************************************
	// Given a collision triangle, this function constructs an 8-bit value where each bit represents
	// a baked-in attribute to preserve in the navmesh.  The virtual functions GetIsBakenInAttributeN()
	// are called to build up the bitfield.  Each project should derive a class from CNavMeshDataExporter,
	// and should implement these functions for whatever attributes are of use to them
	//*******************************************************************************************************
	u8 GetBakedInAttributesForTriangle(Vector3 * pTriVerts, phMaterialMgr::Id iTriMaterial);

	// This function sets an instance of the "fwNavMeshMaterialInterface" class, whose purpose is to filter out
	// which types of data we wish to store in the navmeshes for each project.  The class's functions are
	// called upon export for each triangle in the world.
	static void SetProjectSpecificDataFilter(fwNavMeshMaterialInterface * pStoredData);

	void ResetNumTrianglesThisTriFile() { m_iNumTrianglesThisTriFile = 0; }

#if HEIGHTMAP_GENERATOR_TOOL
	void ExportEntityBegin(u32 iArchFlags, Vec3V_In vMin, Vec3V_In vMax) { m_iCombinedArchetypeFlags = iArchFlags; m_vExportBoundsMin = Vec3V(V_FLT_MAX); m_vExportBoundsMax = Vec3V(V_NEG_FLT_MAX); m_vIntersectRegionMin = vMin, m_vIntersectRegionMax = vMax; m_bIntersectRegionHit = false; m_iNumTrianglesExported = 0; m_iExportedArchetypeFlags = 0; }
	bool WasInterestRegionHit() const { return m_bIntersectRegionHit; }
	bool WereAnyTrianglesExported() const { return m_iNumTrianglesExported > 0; }
#endif

protected:

	// Helper method that may add a COVER collision box circumscribing the given pillar shape
	// This helps to generate corner cover around tree trunks or appropriate columns
	void MaybeAddCoverBoxForPillarShape(const Matrix34& matrixLocalToWorld, Vec3V_In centerWorld, Vec3V_In halfExtentsLocal, float radius, float height, ADD_POLY_TO_LIST_PARAMS);

	// The actual min/max of the geometry we wrote to file
	Vector3					m_vActualMinOfGeomInTriFile;
	Vector3					m_vActualMaxOfGeomInTriFile;

	// The extents of the block which we are exporting
	Vector3					m_vNavMeshMins;
	Vector3					m_vNavMeshMaxs;

	fiStream*				m_pCurrentTriFile;
	s16*					m_pWaterLevels;

	float					m_fAverageWaterLevelInCurrentNavMesh;

	s32						m_iNumTrianglesThisTriFile;

#if HEIGHTMAP_GENERATOR_TOOL
	s32						m_iNumTrianglesExported;
	u32						m_iExportedArchetypeFlags;
	u32						m_iCombinedArchetypeFlags;
	Vec3V					m_vExportBoundsMin;
	Vec3V					m_vExportBoundsMax;
	Vec3V					m_vIntersectRegionMin;
	Vec3V					m_vIntersectRegionMax;
	bool					m_bIntersectRegionHit;
#endif

	u8						m_iHasWaterInCurrentNavMesh;

	char					m_OutputPath[512];
	char					m_TriFileName[1024];

	static const float		ms_fSameVertexEps;
	static const float		ms_fWaterSamplingStepSize;

	static const float		ms_fMinHeightToCoverBoxCylinder;
	static const float		ms_fMinRadiusToCoverBoxCylinder;
	static const float		ms_fMaxRadiusToCoverBoxCylinder;

	// This static instance is set by each project to point to a project-specific class derived
	// from CNavMeshExportData.  (eg. CNavMeshExportDataGta, CNavMeshExportDataJimmy).  The virtual
	// functions in this class are called for each collision triangle, so that important attributes
	// can be baked into the generated navmeshes.
	static fwNavMeshMaterialInterface * ms_pNavMeshStoredData;
};

//-----------------------------------------------------------------------------

}	// namespace rage

#endif // FWNAVGEN_BASEEXPORTCOLLISION_H

// End of file fwnavgen/baseexportcollision.h
