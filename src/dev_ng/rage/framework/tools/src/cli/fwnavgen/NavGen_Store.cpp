#include "NavGen_Store.h"
#include "ai/navmesh/navmeshextents.h"
#include "system/xtl.h"		// For OutputDebugString()

using namespace rage;

CNavGen_NavMeshStore::CEntry CNavGen_NavMeshStore::m_Entries[NAVMESH_MAX_MAP_INDEX+1];

s32 CNavGen_NavMeshStore::GetNavMeshIndexFromFilename(const char * pFilename, s32 * pSectorX, s32 * pSectorY)
{
	//******************************************************************
	// NavMesh filenames are currently in the following format:
	// NB : This is the *independent* format (".inv")
	//
	// "navmesh[X][Y].inv"		(eg. "navmesh[108][24].inv")
	//	X = Starting Sector X
	//	Y = Starting Sector Y
	//
	// All other files in the "navmeshes.img" file are assumed to be
	// dynamic navmeshes belonging to vehicles, etc.
	//
	// Note that these dynamic navmeshes also need to have a naming
	// convention worked out at some point.  Note also that the main map
	// navmeshes naming format above should at some point soon be
	// simplified to something like "M_108_24" to save space.
	//******************************************************************

	char tmpFilename[256];
	strcpy(tmpFilename, pFilename);
	strlwr(tmpFilename);

	u32 iIndex;

	//char * ptr = strstr(tmpFilename, "navmesh[");
	//if(ptr)
	{
		// Skip to first '[' character
		char * ptr = strstr(tmpFilename, "[");
		//Assertf(ptr, "NavMesh filename was in wrong format");
		if(!ptr)
			return NAVMESH_NAVMESH_INDEX_NONE;

		// The character after this is the X position
		ptr++;
		char * pX = ptr;

		// Skip to first ']' character
		ptr = strstr(pX, "]");
		Assertf(ptr, "NavMesh exterior filename was in wrong format");
		// Terminate the XPos string here
		*ptr = 0;

		// Skip to the second '[' character
		ptr++;
		if(*ptr=='[')
		{
			ptr++;
			char * pY = ptr;

			// Skip to second ']' character
			ptr = strstr(pY, "]");

			// Terminate the YPos string here
			*ptr = 0;

			// Read in X & Y
			s32 iXPos, iYPos;
			sscanf(pX, "%i", &iXPos);
			sscanf(pY, "%i", &iYPos);

			if(pSectorX)
				*pSectorX = iXPos;
			if(pSectorY)
				*pSectorY = iYPos;

			iXPos /= CPathServerExtents::m_iNumSectorsPerNavMesh;
			iYPos /= CPathServerExtents::m_iNumSectorsPerNavMesh;

			// Return the navmesh index
			//iIndex = (iYPos * m_pNavMeshStore->GetNumMeshesInX()) + iXPos;
			iIndex = (iYPos * CPathServerExtents::m_iNumNavMeshesInX) + iXPos;
			Assert(iIndex >=0 && iIndex <= NAVMESH_MAX_MAP_INDEX);
			return iIndex;
		}
	}

	// We currently assume that all other files in the image are dynamic navmeshes for vehicles
	Assert(false);
	return NAVMESH_NAVMESH_INDEX_NONE;
}

CNavMesh * CNavGen_NavMeshStore::GetNavMeshFromIndex(const s32 iMesh)
{
	Assert(iMesh >= 0 && iMesh <= NAVMESH_MAX_MAP_INDEX);
	return m_Entries[iMesh].pNavMesh;
}

CNavMesh *CNavGen_NavMeshStore::GetNavMeshFromSectorCoords(const s32 iSectorX, const s32 iSectorY)	// TODO: phase out sector usage when we get time
{
	TNavMeshIndex iMesh = GetMeshIndexFromSectorCoords(iSectorX, iSectorY);
	if(iMesh == NAVMESH_NAVMESH_INDEX_NONE)
		return NULL;
	return GetNavMeshFromIndex(iMesh);
}

s32 CNavGen_NavMeshStore::GetMeshIndexFromSectorCoords(const s32 iSectorX, const s32 iSectorY)	// TODO: phase out sector usage when we get time
{
	const int iNavMeshBlockX = (int) (iSectorX / (float) CPathServerExtents::m_iNumSectorsPerNavMesh);
	const int iNavMeshBlockY = (int) (iSectorY / (float) CPathServerExtents::m_iNumSectorsPerNavMesh);

	if(iNavMeshBlockX < 0 || iNavMeshBlockX >= CPathServerExtents::m_iNumNavMeshesInX || iNavMeshBlockY < 0 || iNavMeshBlockY >= CPathServerExtents::m_iNumNavMeshesInY)
	{
		Assertf(false, "GetMeshIndexFromSectorCoords : sectors coords out of range");
		return NAVMESH_NAVMESH_INDEX_NONE;
	}
	const u32 iIndex = (iNavMeshBlockY * CPathServerExtents::m_iNumNavMeshesInX) + iNavMeshBlockX;
	return iIndex;
}

void CNavGen_NavMeshStore::GetNavMeshExtentsFromIndex(const TNavMeshIndex iMeshIndex, Vector2 & vMin, Vector2 & vMax)
{
	const int iY = iMeshIndex / CPathServerExtents::m_iNumNavMeshesInY;
	const int iX = iMeshIndex - (iY * CPathServerExtents::m_iNumNavMeshesInY);
	const float fMeshSize = CPathServerExtents::GetSizeOfNavMesh();

	vMin.x = ((((float)iX) * fMeshSize) + CPathServerExtents::m_vWorldMin.x);
	vMin.y = ((((float)iY) * fMeshSize) + CPathServerExtents::m_vWorldMin.y);

	vMax.x = vMin.x + fMeshSize;
	vMax.y = vMin.y + fMeshSize;
}

bool CNavGen_NavMeshStore::TestNavMeshLOS(const Vector3 & UNUSED_PARAM(vStart), const Vector3 & UNUSED_PARAM(vEnd))
{
	return true;
}

void CNavGen_NavMeshStore::GetSectorFromNavMeshIndex(const s32 iNavMesh, s32 & iSectorXOut, s32 & iSectorYOut)
{
	iSectorYOut = (iNavMesh / CPathServerExtents::m_iNumNavMeshesInX);
	iSectorXOut = iNavMesh - (iSectorYOut*CPathServerExtents::m_iNumNavMeshesInX);

	iSectorXOut *= CPathServerExtents::m_iNumSectorsPerNavMesh;
	iSectorYOut *= CPathServerExtents::m_iNumSectorsPerNavMesh;
}

bool CNavGen_NavMeshStore::GetDoNavMeshesAdjoin(const s32 iNavMesh1, const s32 iNavMesh2)
{
	const int iNumSectorsPerNavMesh = CPathServerExtents::m_iNumSectorsPerNavMesh;

	s32 iX1, iY1, iX2, iY2;
	GetSectorFromNavMeshIndex(iNavMesh1, iX1, iY1);
	GetSectorFromNavMeshIndex(iNavMesh2, iX2, iY2);

	if(iX1==iX2 && (iY1==iY2-iNumSectorsPerNavMesh || iY1==iY2+iNumSectorsPerNavMesh))
		return true;
	if(iY1==iY2 && (iX1==iX2-iNumSectorsPerNavMesh || iX1==iX2+iNumSectorsPerNavMesh))
		return true;
	return false;
}

s32 CNavGen_NavMeshStore::NavMeshIndexToNavNodesIndex(const TNavMeshIndex iNavMesh)
{
	Assert(iNavMesh != NAVMESH_NAVMESH_INDEX_NONE);

	s32 iSectorX, iSectorY;

	GetSectorFromNavMeshIndex(iNavMesh, iSectorX, iSectorY);

	iSectorX /= CPathServerExtents::m_iHierarchicalNodesBlockSize;
	iSectorY /= CPathServerExtents::m_iHierarchicalNodesBlockSize;

	iSectorX *= CPathServerExtents::m_iHierarchicalNodesBlockSize;
	iSectorY *= CPathServerExtents::m_iHierarchicalNodesBlockSize;

	const u32 iNodesIndex = GetMeshIndexFromSectorCoords(iSectorX, iSectorY);
	return iNodesIndex;
}

s32 CNavGen_NavMeshStore::GetNavMeshIndexFromPosition(const Vector3 & vPos)
{
	const int iSectorX = CPathServerExtents::GetWorldToSectorX(vPos.x);
	const int iSectorY = CPathServerExtents::GetWorldToSectorY(vPos.y);

	const u32 iNodesIndex = GetMeshIndexFromSectorCoords(iSectorX, iSectorY);
	return iNodesIndex;
}

bool CNavGen_NavMeshStore::LoadAll(char * pPath)
{
	int x,y;
	int iSectorStep = CPathServerExtents::m_iNumSectorsPerNavMesh;

	u32 iTotalNumVerticesInWorld = 0;
	u32 iTotalNumPolysInWorld = 0;
	u32 iTotalNumCoverPointsInWorld = 0;

	u32 iGreatestNumCoverPointsInAnyNavMesh = 0;

	char sectorName[256];
	char sectorFilename[256];

	for(y=0; y<CPathServerExtents::GetWorldDepthInSectors(); y+=iSectorStep)
	{
		for(x=0; x<CPathServerExtents::GetWorldWidthInSectors(); x+=iSectorStep)
		{
			const u32 index = GetMeshIndexFromSectorCoords(x, y);
			if(GetNavMeshFromIndex(index))
			{
				Assertf(false, "CNavGen_NavMeshStore::LoadAll - already loaded");
			}

			sprintf(sectorName, "navmesh[%i][%i].inv", x, y);
			sprintf(sectorFilename, "%s\\navmesh[%i][%i].inv", pPath, x, y);


			CNavMesh * pNavMesh = CNavMesh::LoadBinary(sectorFilename);
			if(!pNavMesh)
				continue;

#if __ENSURE_THAT_POLY_QUICK_CENTROIDS_ARE_REALLY_WITHIN_POLYS
			pNavMesh->CheckAllQuickPolyCentroidsAreWithinPolys();
#endif

			Assert(!m_Entries[index].pNavMesh);
			if(!m_Entries[index].pNavMesh)
			{
				m_Entries[index].pNavMesh = pNavMesh;

				// total up
				iTotalNumVerticesInWorld += pNavMesh->GetNumVertices();
				iTotalNumPolysInWorld += pNavMesh->GetNumPolys();
				iTotalNumCoverPointsInWorld += pNavMesh->GetNumCoverPoints();

				if(pNavMesh->GetNumCoverPoints() > iGreatestNumCoverPointsInAnyNavMesh)
					iGreatestNumCoverPointsInAnyNavMesh = pNavMesh->GetNumCoverPoints();
			}
		}
	}

#if __DEV
#if __WIN32PC

	char tmp[256];
	Printf("*****************************************************************\n");
	Printf("*****************************************************************\n");

	sprintf(tmp, "Total num vertices in all navmeshes : %i\n", iTotalNumVerticesInWorld);
	Printf(tmp);
	OutputDebugString(tmp);
	sprintf(tmp, "Total num polys in all navmeshes : %i\n", iTotalNumPolysInWorld);
	Printf(tmp);
	OutputDebugString(tmp);
	sprintf(tmp, "Total num cover-points in all navmeshes : %i\n", iTotalNumCoverPointsInWorld);
	Printf(tmp);
	OutputDebugString(tmp);
	sprintf(tmp, "Greatest num coverpoints in any navmesh : %i\n(max is 8191 stored in 13 bits in TLinkedCoverPoint)\n", iGreatestNumCoverPointsInAnyNavMesh);
	Printf(tmp);
	OutputDebugString(tmp);

	Printf("*****************************************************************\n");
	Printf("*****************************************************************\n");

#endif
#endif

	return true;
}

void CNavGen_NavMeshStore::UnloadAll()
{
	s32 x,y;
	int iSectorStep = CPathServerExtents::m_iNumSectorsPerNavMesh;

	for(y=0; y<CPathServerExtents::GetWorldDepthInSectors(); y+=iSectorStep)
	{
		for(x=0; x<CPathServerExtents::GetWorldWidthInSectors(); x+=iSectorStep)
		{
			const u32 index = GetMeshIndexFromSectorCoords(x, y);
			if(GetNavMeshFromIndex(index))
			{
				if(m_Entries[index].pNavMesh)
				{
					delete m_Entries[index].pNavMesh;
					m_Entries[index].pNavMesh = NULL;
				}
			}
		}
	}
}