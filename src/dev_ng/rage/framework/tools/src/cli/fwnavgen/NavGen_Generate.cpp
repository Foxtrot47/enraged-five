#include "common.h"
#include "config.h"
#include "NavGen.h"
#include "NavGen_Utils.h"
#include "vector/geometry.h"
#include "ai/navmesh/datatypes.h"
#include "ai/navmesh/navmeshextents.h"

//***********************************************************************************
//	Title	: NavGen_Generate.cpp
//	Author	: James Broad
//	Date	: Jan 2005
//
//	This file contains the main functions for generating the navmesh.
//	The collision geometry will have been loaded with LoadCollisionTris(), and an
//	octree made of this geometry.  Any geometry outside the bounds of this navmesh
//	will have been sliced away.
//
//	Height samples are made across the collision geometry at regular intervals, using
//	the octree and the function PlaceNavSurfaceNodesInOctreeLeaf().  These samples
//	are stored in the CPlacedNodeMultiMap container - one for each XY location, and
//	possibly having multiple Z samples in each position (for when geometry is on
//	multiple levels in the world).
//
//	The height samples are triangulated.  This is done quite easily, because we know
//	that all the samples are evenly spaced.  For any height sample (CNavGenNode) we
//	triangulate it with the 4 adjacent height samples.
//	There is a height tolerance value for how close adjacent samples must be vertically
//	to allow triangulation to take place.  There is also a series of line-tests to
//	ensure that the triangulation isn't occuring over any low walls or similar.
//	(see TestForSuddenHeightChangesAlongLine()).
//
//	If all nodes of any triangle end up on the same type of collision surface (and we
//	are interested in preserving this surface info - eg. pavements) then we mark the
//	triangle as being of that surface type.
//
//	Once the generation & triangulation is done, the surface will be optimised.
//
//***********************************************************************************

namespace rage
{


bool IsPavement(CNavSurfaceTri * pTri) { return pTri->m_colPolyData.GetIsPavement(); }
bool IsCollisionTriPavement(CNavGenTri * pTri) { return pTri->m_colPolyData.GetIsPavement(); }

// This #define causes flags to be set so that triangulation calls m_Octree::LineOfSight() less often
//#define REDUCE_LOS_CALLS_IN_TRIANGULATION
//#define REDUCE_LOS_CALLS_IN_HEIGHTCHECKS
#define __USE_VOLUME_TESTS									1
#define __DEBUG_SAMPLE_POINTS								0
#define __MOVE_VERTS_TO_SMOOTH_PAVEMENT_EDGES				1
#define __CUT_PAVEMENTS_INTO_NAVMESH						0

bool CNavGen::GenerateNavSurface(CNavGen** pNeighborNavGens, int numNeighborNavGens, bool bTriangulate, bool bMainMap)
{
	const fwNavGenConfig& cfg = fwNavGenConfig::Get();

	if(!m_Octree.RootNode())
	{
		return false;
	}

	OutputDebugString("\n//***********************************************\n");

	sprintf(tmpBuf, "// Generating Path Nodes\n");
	OutputDebugString(tmpBuf);
	sprintf(tmpBuf, "// m_Params.m_fDistanceToInitiallySeedNodes = %.2f\n", m_Params.m_fDistanceToInitiallySeedNodes);
	OutputDebugString(tmpBuf);
	sprintf(tmpBuf, "// m_Params.m_TriangulationMaxHeightDiff = %.2f\n", m_Params.m_TriangulationMaxHeightDiff);
	OutputDebugString(tmpBuf);
	sprintf(tmpBuf, "// m_fMinZDistBetweenSamples = %.2f\n", m_Params.m_fMinZDistBetweenSamples);
	OutputDebugString(tmpBuf);
	sprintf(tmpBuf, "// m_Params.m_HeightAboveNodeBaseAtWhichToTriangulate = %.2f\n", m_Params.m_HeightAboveNodeBaseAtWhichToTriangulate);
	OutputDebugString(tmpBuf);
	sprintf(tmpBuf, "// m_Params.m_MaxHeightChangeUnderTriangleEdge = %.2f\n", m_Params.m_MaxHeightChangeUnderTriangleEdge);
	OutputDebugString(tmpBuf);
	sprintf(tmpBuf, "// m_vAreaMin = (%.2f, %.2f, %.2f)\n", m_Params.m_vAreaMin.x, m_Params.m_vAreaMin.y, m_Params.m_vAreaMin.z);
	OutputDebugString(tmpBuf);
	sprintf(tmpBuf, "// m_vAreaMax = (%.2f, %.2f, %.2f)\n", m_Params.m_vAreaMax.x, m_Params.m_vAreaMax.y, m_Params.m_vAreaMax.z);
	OutputDebugString(tmpBuf);
	sprintf(tmpBuf, "// m_vSectorMins = (%.2f, %.2f, %.2f)\n", m_Params.m_vSectorMins.x, m_Params.m_vSectorMins.y, m_Params.m_vSectorMins.z);
	OutputDebugString(tmpBuf);
	sprintf(tmpBuf, "// m_vSectorMaxs = (%.2f, %.2f, %.2f)\n", m_Params.m_vSectorMaxs.x, m_Params.m_vSectorMaxs.y, m_Params.m_vSectorMaxs.z);
	OutputDebugString(tmpBuf);

	OutputDebugString("//***********************************************\n");

	m_iInitialPassNumNodesCreated = 0;

	//************************************************************************
	// Classify all CNavResolutionArea's wrt the current navmesh extents
	// We don't clear the array first, since it may already contain areas
	// placed there by the calling code.
	//************************************************************************

	u32 r;
	for(r=0; r<m_ResolutionAreasWholeMap.size(); r++)
	{
		CNavResolutionArea * pArea = m_ResolutionAreasWholeMap[r];
		if(BBoxIntersect2d(m_vSectorMins, m_vSectorMaxs, pArea->m_vMin, pArea->m_vMax))
			m_ResolutionAreasCurrentNavMesh.push_back(pArea);
	}

	//************************************************************************
	// Place the initial pathnodes into the octree, at a very fine resolution
	//************************************************************************
#if __DEV
	m_vMinOfPlacedNodes = Vector3(FLT_MAX, FLT_MAX, FLT_MAX);
	m_vMaxOfPlacedNodes = Vector3(-FLT_MAX, -FLT_MAX, -FLT_MAX);
#endif

	u32 iNodesStartTime = timeGetTime();

	if(m_Params.m_bAerialNavigation)
	{
		PlaceNavAerialNodes(pNeighborNavGens, numNeighborNavGens, m_vSectorMins, m_vSectorMaxs);
	}
	else
	{
		PlaceNavSurfaceNodes(m_vSectorMins, m_vSectorMaxs, cfg.m_CreateResolutionAreasForPavementTriangles);
	}

	u32 iNodesEndTime = timeGetTime();
	m_fTimeTakenToPlaceNodes = (float)(iNodesEndTime - iNodesStartTime) / 1000.0f;

	sprintf(tmpBuf, "m_iInitialPassNumNodesCreated : %i\n", m_iInitialPassNumNodesCreated);
	OutputDebugString(tmpBuf);

#if __DEV
	sprintf(tmpBuf, "Min of placed nodes : (%.2f, %.2f, %.2f)\n", m_vMinOfPlacedNodes.x, m_vMinOfPlacedNodes.y, m_vMinOfPlacedNodes.z);
	OutputDebugString(tmpBuf);
	sprintf(tmpBuf, "Max of placed nodes : (%.2f, %.2f, %.2f)\n", m_vMaxOfPlacedNodes.x, m_vMaxOfPlacedNodes.y, m_vMaxOfPlacedNodes.z);
	OutputDebugString(tmpBuf);
#endif

	if(!bTriangulate)
		return true;

	OutputDebugString("\n//***********************************************\n");
	sprintf(tmpBuf, "// Triangulating Path Nodes (pass 1)\n");
	OutputDebugString(tmpBuf);
	OutputDebugString("//***********************************************\n");
	

	//************************************************************************
	// Triangulate the initial pathnodes to form a surface
	//************************************************************************

	u32 iTrisStartTime = timeGetTime();

	m_iTriangulationPass = 0;
	
	TriangulateNavSurface(m_vSectorMins, m_vSectorMaxs, m_iTriangulationPass);

	u32 iTrisPass1EndTime = timeGetTime();
	m_fTimeTakenToTriangulatePass1 = (float)(iTrisPass1EndTime - iTrisStartTime) / 1000.0f;


	u32 iTrisPass2EndTime = timeGetTime();
	m_fTimeTakenToTriangulatePass2 = (float)(iTrisPass2EndTime - iTrisPass1EndTime) / 1000.0f;


	//****************************************************************
	// Cut properties into the mesh surface
	//*****************************************************************

#if __CUT_PAVEMENTS_INTO_NAVMESH
	
	OutputDebugString("\n//***********************************************\n");
	sprintf(tmpBuf, "// Cut properties into surface\n");
	OutputDebugString(tmpBuf);
	OutputDebugString("//***********************************************\n");

	CutPavementsIntoMesh();

#endif


	// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

	if(m_Params.m_bTagPavementNavMesh)
	{
		TagPavementNavMesh();
	}

	// <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<


	OutputDebugString("\n//***********************************************\n");
	sprintf(tmpBuf, "// Establishing Triangle Adjacency\n");
	OutputDebugString(tmpBuf);
	OutputDebugString("//***********************************************\n");


	//****************************************************************
	// Create & triangulate the water surface
	//*****************************************************************

	// Remove water triangles which are inaccessible
	CullWaterTrianglesNotAcessible();

	u32 iCallWaterTrisEndTime = timeGetTime();
	m_fTimeTakenToCullWaterTriangles = (float)(iCallWaterTrisEndTime - iTrisPass2EndTime) / 1000.0f;

	//************************************************************************
	// Connect all the triangles in the surface together
	//************************************************************************

	EstablishNavSurfacesAdjacency();
	EstablishNavSurfacesAdjacency2();

	u32 iEstablishAdjacencyEndTime = timeGetTime();
	m_fTimeTakenToEstablishAdjacency = (float)(iEstablishAdjacencyEndTime - iCallWaterTrisEndTime) / 1000.0f;


#if __MOVE_VERTS_TO_SMOOTH_PAVEMENT_EDGES

	u32 iSmoothStartTime = timeGetTime();

	OutputDebugString("\n//***********************************************\n");
	sprintf(tmpBuf, "// Smoothing pavement edges\n");
	OutputDebugString(tmpBuf);
	OutputDebugString("//***********************************************\n");

	MoveVertsToSmoothPavementEdges();

	m_fTimeTakenToSmooth = (float)(timeGetTime() - iSmoothStartTime) / 1000.0f;

#endif


	//************************************************************************
	// Nudge nodes away from walls
	//************************************************************************

	MarkWhetherNodesAreSurroundedByTriangles();

	u32 iMoveNodesStartTime = timeGetTime();

	if( m_Params.m_bMoveNodesAwayFromWalls )
	{
		MoveNodesAwayFromWalls();
	}

	u32 iMoveNodesEndTime = timeGetTime();
	m_fTimeTakenToMoveNodesAwayFromWalls = ((float)(iMoveNodesEndTime-iMoveNodesStartTime) / 1000.0f);

	//--------------------------------------------------------------------------------
	// Deal with jagged edges on the navmesh.
	// We have two techniques
	//  (i) "Fix Jaggies" by creating new triangles to fill gaps
	//  (ii) "Anti Alias Edges" by shifting vertices to form straight lines
	// We always perform (i) since this helps connect disparate mesh areas..

	//*****************************************************************************************
	// Fix jaggies by walking the boundary edge of the mesh & joining up gaps where possible
	//*****************************************************************************************

	u32 iFixJaggiesStartTime = timeGetTime();
	if(m_Params.m_bFixJaggiesAfterTriangulation)
	{
		FixJaggiesAfterTriangulation();

		// Ensure adjacencies are all set up for new triangles
		EstablishNavSurfacesAdjacency();
		EstablishNavSurfacesAdjacency2();
	}

	u32 iFixJaggiesEndTime = timeGetTime();
	m_fTimeTakenToFixJaggies = ((float)(iFixJaggiesEndTime-iFixJaggiesStartTime) / 1000.0f);

	m_iInitialPassNumTrianglesCreated += m_iFixJaggiesNumTrianglesCreated;



	//******************************************************************************
	// Perform a number of antialiasing passes on the boundary-edges of the navmesh
	//******************************************************************************

	u32 iAntiAliasStartTime = timeGetTime();

	for(s32 ap1=0; ap1<cfg.m_AntialiasEdgesNumPasses; ap1++)
	{
		CNavAntiAlias navAA(this);
		navAA.AntiAliasBoundaryEdges();

		MoveNodesAwayFromWalls();
	}

	//*****************************************************
	// Perform antialiasing on boundary pavement edges
	//*****************************************************
	for(s32 ap2=0; ap2<cfg.m_AntialiasPavementEdgesNumPasses; ap2++)
	{
		CNavAntiAlias navAA(this);
		navAA.AntiAliasPavementEdges();
	}
	//*****************************************************
	// Perform antialiasing on steep slope edges
	//*****************************************************
/*
	for(s32 ap2=0; ap2<4; ap2++)
	{
		CNavAntiAlias navAA(this);
		navAA.AntiAliasSteepSlopeEdges();
	}
*/
	m_fTimeTakenToAntiAliasEdges = (float)(timeGetTime() - iAntiAliasStartTime) / 1000.0f;


	//***********************************************************************
	// Remove triangles which are of no use to us
	// Remove degenerate triangles
	//************************************************************************

	s32 iNumCulled_NoNeighbours = 0;
	s32 iNumCulled_ZeroArea = 0;
	s32 iNumCulled_PatchArea = 0;

	if(bMainMap)
	{
		iNumCulled_NoNeighbours = RemoveTrianglesWithNoNeighbours();
		iNumCulled_ZeroArea = RemoveTrianglesWithZeroArea();
		if( cfg.m_MinPatchAreaBeforeCulling > 0.0f)
		{
			iNumCulled_PatchArea = RemoveSmallPatchesOfNavMesh(cfg.m_MinPatchAreaBeforeCulling);
		}
	}
	else
	{
		iNumCulled_NoNeighbours = RemoveTrianglesWithNoNeighbours();
		iNumCulled_ZeroArea = RemoveTrianglesWithZeroArea();
	}

	
	


	//***********************************************************************

	RecordSteepnessUnderMovedTriangles();

	//***********************************************************************


	u32 iTrisEndTime = timeGetTime();
	m_fTimeTakenToTriangulate = (float)(iTrisEndTime - iTrisStartTime) / 1000.0f;

	float fNodesMemInKb = ((float)(m_iInitialPassNumNodesCreated * sizeof(CNavGenNode))) / 1024.0f;
	float fTrisMemInKb = ((float)(m_iInitialPassNumTrianglesCreated * sizeof(CNavSurfaceTri))) / 1024.0f;
	float fNodesMemInMb = fNodesMemInKb / 1024.0f;
	float fTrisMemInMb = fTrisMemInKb / 1024.0f;

	OutputDebugString("\n\n");
	OutputDebugString("//*************************************************************\n");

	sprintf(tmpBuf, "// Created %i nodes in initial pass (%.1fMb)\n", m_iInitialPassNumNodesCreated, fNodesMemInMb);
	OutputDebugString(tmpBuf);
	sprintf(tmpBuf, "// Created %i triangles in unoptimised surface mesh (%.1fMb)\n", m_iInitialPassNumTrianglesCreated, fTrisMemInMb);
	OutputDebugString(tmpBuf);
	sprintf(tmpBuf, "// %i triangles removed (%i no neighbours, %i zero area, %i min patch area)\n",
		iNumCulled_NoNeighbours + iNumCulled_ZeroArea + iNumCulled_PatchArea,
		iNumCulled_NoNeighbours, iNumCulled_ZeroArea, iNumCulled_PatchArea);
	OutputDebugString(tmpBuf);
	sprintf(tmpBuf, "// Created %i triangles fixing jaggies\n", m_iFixJaggiesNumTrianglesCreated);
	OutputDebugString(tmpBuf);
	sprintf(tmpBuf, "// Took %.1f seconds to antialias boundary edges\n", m_fTimeTakenToAntiAliasEdges);
	OutputDebugString(tmpBuf);

	OutputDebugString("//*************************************************************\n");
	OutputDebugString("\n\n");

	m_iOptimisedTriangleCount = 0;
	for(u32 t=0; t<m_NavSurfaceTriangles.size(); t++)
		if(!m_NavSurfaceTriangles[t]->IsRemoved())
			m_iOptimisedTriangleCount++;

	m_iOptimisedNodeCount = 0;
	for(u32 n=0; n<m_NavSurfaceNodes.size(); n++)
		if(!m_NavSurfaceNodes[n]->IsRemoved())
			m_iOptimisedNodeCount++;


	//********************************************************************************
	// Remove resolution areas for current navmesh, deleting only dynamically created
	//********************************************************************************

	for(r=0; r<m_ResolutionAreasCurrentNavMesh.size(); r++)
	{
		if(m_ResolutionAreasCurrentNavMesh[r]->m_bDynamicallyCreated)
			delete m_ResolutionAreasCurrentNavMesh[r];
	}
	m_ResolutionAreasCurrentNavMesh.clear();

	//************************************************************************
	// Clean up any data which we no longer need
	//************************************************************************

	CleanUpDataAfterGeneratingNavMesh();


	OutputDebugString("// Finished clearing construction data\n");

	return true;
}


void CNavGen::GenerateNavSurface_VoxelMap()
{
	m_VoxelMap.ConstructFromCollision(m_vSectorMins, m_vSectorMaxs, m_CollisionTriangles);
}

//***********************************************************************************
// EstablishTriAdjacency
// This function fills in the adjacency information of the CNavSurfaceTri
// It does this by examining the CNavGenNode's which the triangle uses, and in
// turn examining these nodes' lists of triangles which use them
// Therefore this data must have been set up properly before calling this function.
//***********************************************************************************

void EstablishTriAdjacency(CNavSurfaceTri * pTri)
{
	if((pTri->m_iFlags & NAVSURFTRI_ADJACENCY_ESTABLISHED_1)!=0)
		return;

	u32 i;
	int v, last_v, other_v, other_last_v;
	CNavGenNode * pNode, * pLastNode;

//	static const float fSnapVertexEps = 0.2f;
//	static const float fSnapVertexEpsSqr = fSnapVertexEps*fSnapVertexEps;

	last_v = 2;
	pLastNode = pTri->m_Nodes[last_v];

	for(v=0; v<3; v++)
	{
		// deal with the edge from 'last_v' to 'v'
		pNode = pTri->m_Nodes[v];

		//***********************************************************************************
		// By going through the list of surrounding tris within pLastNode,
		// we should be able to find a triangle which shares the same nodes
		// along one edge as this triangle does.  If is is not found, then
		// no adjacent triangle exists.
		// We should not have to do the same process for 'pNode' during this
		// iteration, because by definition the triangle will appear in both
		// 'pLastNode' and 'pNode'
		//***********************************************************************************

		for(i=0; i<pLastNode->m_TrianglesUsingThisNode.size(); i++)
		{
			CNavSurfaceTri * pOtherTri = pLastNode->m_TrianglesUsingThisNode[i];
			if(pTri == pOtherTri)
				continue;

			// NB : we should check that it's not already set here - that is only
			// if we decide to link both ways each time we establish an adjacent face..
			other_last_v = 2;
			for(other_v=0; other_v<3; other_v++)
			{
				if((pOtherTri->m_Nodes[other_v] == pLastNode && pOtherTri->m_Nodes[other_last_v] == pNode) ||
					(pOtherTri->m_Nodes[other_v] == pNode && pOtherTri->m_Nodes[other_last_v] == pLastNode))
				{
					// We have found an adjacent triangle along the edge defined by (last_v -> v);
					pTri->m_AdjacentTris[last_v] = pOtherTri;
					break;
				}

				other_last_v = other_v;
			}
		}

		pLastNode = pNode;
		last_v = v;
	}

	pTri->m_iFlags |= NAVSURFTRI_ADJACENCY_ESTABLISHED_1;
}

//***********************************************************************************
// Since some adjacency is being set up wrongly : triangles are connected one way,
// but not the other - this function puts it to rights.
//***********************************************************************************
void EstablishTriAdjacency2(CNavSurfaceTri * pTri)
{
	if((pTri->m_iFlags & NAVSURFTRI_ADJACENCY_ESTABLISHED_2)!=0)
		return;

	int e;
	for(e=0; e<3; e++)
	{
		if(pTri->m_AdjacentTris[e])
		{
			CNavSurfaceTri * pAdjacentTri = pTri->m_AdjacentTris[e];
			CNavGenNode * pNode1 = pTri->m_Nodes[e];
			CNavGenNode * pNode2 = pTri->m_Nodes[(e+1)%3];

			if((pNode1 == pAdjacentTri->m_Nodes[0] && pNode2 == pAdjacentTri->m_Nodes[1]) || (pNode2 == pAdjacentTri->m_Nodes[0] && pNode1 == pAdjacentTri->m_Nodes[1]))
			{
				if(!pAdjacentTri->m_AdjacentTris[0])
				{
					pAdjacentTri->m_AdjacentTris[0] = pTri;
				}
			}
			else if((pNode1 == pAdjacentTri->m_Nodes[1] && pNode2 == pAdjacentTri->m_Nodes[2]) || (pNode2 == pAdjacentTri->m_Nodes[1] && pNode1 == pAdjacentTri->m_Nodes[2]))
			{
				if(!pAdjacentTri->m_AdjacentTris[1])
				{
					pAdjacentTri->m_AdjacentTris[1] = pTri;
				}
			}
			else if((pNode1 == pAdjacentTri->m_Nodes[2] && pNode2 == pAdjacentTri->m_Nodes[0]) || (pNode2 == pAdjacentTri->m_Nodes[2] && pNode1 == pAdjacentTri->m_Nodes[0]))
			{
				if(!pAdjacentTri->m_AdjacentTris[2])
				{
					pAdjacentTri->m_AdjacentTris[2] = pTri;
				}
			}
		}
	}

	pTri->m_iFlags |= NAVSURFTRI_ADJACENCY_ESTABLISHED_2;
}



// This function establishes the adjacency info between every triangle in the nav-surface
void
CNavGen::EstablishNavSurfacesAdjacency(void)
{
	u32 t;
	for(t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];
		EstablishTriAdjacency(pTri);
	}
}


void
CNavGen::EstablishNavSurfacesAdjacency2(void)
{
	u32 t;
	for(t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];
		if(pTri->IsRemoved())
			continue;

		EstablishTriAdjacency2(pTri);
	}
}

//********************************************************************************************
// RemoveTrianglesWithNoNeighbours
// This should be called after the nav-surface has been first triangulated, and before any
// optimisation is done.  Small disjoint triangles are rarely of any use, and this may also
// help fix the problem of small unconnected triangles appearing on some steep surfaces.
//*******************************************************************************************

s32 CNavGen::RemoveTrianglesWithNoNeighbours(void)
{
	s32 iNumRemoved = 0;

	u32 t;
	for(t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];
		if(pTri->IsRemoved())
			continue;

		if(!pTri->m_AdjacentTris[0] && !pTri->m_AdjacentTris[1] && !pTri->m_AdjacentTris[2])
		{
			pTri->SetRemoved();
			RemoveAllReferencesToTriangle(pTri);
			iNumRemoved++;
		}
	}
	return iNumRemoved;
}


s32 CNavGen::RemoveTrianglesWithZeroArea()
{
	static const float fMinArea = SMALL_FLOAT;
	Vector3 vPts[3];

	s32 iNumRemoved = 0;

	u32 t;
	for(t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];
		if(pTri->IsRemoved())
			continue;

		vPts[0] = pTri->m_Nodes[0]->m_vBasePos;
		vPts[1] = pTri->m_Nodes[1]->m_vBasePos;
		vPts[2] = pTri->m_Nodes[2]->m_vBasePos;

		float fArea = CalcTriArea(vPts);

		if(fArea < fMinArea)
		{
			pTri->SetRemoved();
			RemoveAllReferencesToTriangle(pTri);
			iNumRemoved++;
		}
	}

	return iNumRemoved;
}

s32 CNavGen::RemoveSmallPatchesOfNavMesh(const float fThreshold)
{
	u32 t;
	const float fMeshEdgeEps = 0.2f;
	for(t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];
		if(pTri->IsRemoved())
			continue;

		s32 iNodesAtEdge = 0;
		for(u32 v=0; v<3; v++)
		{
			if( IsClose(pTri->m_Nodes[v]->m_vBasePos.x, m_vSectorMins.x, fMeshEdgeEps) || 
				IsClose(pTri->m_Nodes[v]->m_vBasePos.y, m_vSectorMins.y, fMeshEdgeEps) ||
				IsClose(pTri->m_Nodes[v]->m_vBasePos.x, m_vSectorMaxs.x, fMeshEdgeEps) ||
				IsClose(pTri->m_Nodes[v]->m_vBasePos.y, m_vSectorMaxs.y, fMeshEdgeEps))
			{
				iNodesAtEdge++;
			}
		}
		if(iNodesAtEdge >= 2)
		{
			pTri->m_iFlags |= NAVSURFTRI_ATEDGEOFMESH;
		}

		// See if this poly intersects with any areas marked with the 'FLAG_NO_CULL_SMALL_AREAS' flag
		for(u32 a=0; a<m_ResolutionAreasCurrentNavMesh.size(); a++)
		{
			CNavResolutionArea * pArea = m_ResolutionAreasCurrentNavMesh[a];
			if((pArea->m_iFlags & CNavResolutionArea::FLAG_NO_CULL_SMALL_AREAS)!=0)
			{
				TShortMinMax minMax;
				minMax.Set(pArea->m_vMin, pArea->m_vMax);
				if(minMax.Intersects(pTri->m_MinMax))
				{
					pTri->m_bDoNotCullSmallArea = true;
					break;
				}
			}
		}
	}
	

	const s32 iMaxNumIters = 30;
	s32 iNumRemoved = 0;

	u32 iTimestamp = 0;
	for(u32 t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];
		if(pTri->IsRemoved())
			continue;
		if((pTri->m_iFlags & NAVSURFTRI_ATEDGEOFMESH)!=0)
			continue;
		if(pTri->m_bIsWater)	// Preserve small pools of water, etc
			continue;
		if(pTri->m_bDoNotCullSmallArea)
			continue;

		if(!pTri->m_bAboveCullAreaThreshold)
		{
			iTimestamp++;
			pTri->m_iFloodFillTimeStamp = iTimestamp;

			float fTotalArea=0.0f;
			s32 iIters=0;

			RemoveSmallPatchesOfNavMeshR(pTri, fTotalArea, fThreshold, iTimestamp, iIters, iMaxNumIters);

			if(fTotalArea < fThreshold)
			{
				pTri->SetRemoved();
				RemoveAllReferencesToTriangle(pTri);
				iNumRemoved++;
			}
			else
			{
				pTri->m_bAboveCullAreaThreshold = true;
			}
		}
	}
	return iNumRemoved;
}

bool CNavGen::RemoveSmallPatchesOfNavMeshR(CNavSurfaceTri * pTri, float & fTotalArea, const float fThreshold, const u32 iTimestamp, s32 & iIters, const s32 iMaxNumIters)
{
	const float fArea = CalcTriArea(pTri->m_Nodes[0]->m_vBasePos, pTri->m_Nodes[1]->m_vBasePos, pTri->m_Nodes[2]->m_vBasePos);
	
	fTotalArea += fArea;
	iIters++;

	// If we've exceeded target area size, then quit recursion
	if(fTotalArea > fThreshold)
	{
		return false;
	}

	// If we've exceeded max num iterations, then force patch area to pass test - and quit recursion
	if(iIters > iMaxNumIters)
	{
		fTotalArea = Max(fTotalArea, fThreshold);
		return false;
	}

	// If we've hit a triangle on the edge of the navmesh, then force patch area to pass test - and quit recursion
	if((pTri->m_iFlags & NAVSURFTRI_ATEDGEOFMESH)!=0)
	{
		fTotalArea = Max(fTotalArea, fThreshold);
		return false;
	}

	for(u32 a=0; a<3; a++)
	{
		if(pTri->m_AdjacentTris[a] && !pTri->m_AdjacentTris[a]->IsRemoved())
		{
			CNavSurfaceTri * pAdjTri = pTri->m_AdjacentTris[a];

			// If we touch any polygons which have already been found to be on a patch of
			// navmesh above the area threshold, then we can quit instantly!
			if(pAdjTri->m_bAboveCullAreaThreshold)
			{
				fTotalArea = Max(fTotalArea, fThreshold);
				return false;
			}

			if(pAdjTri->m_iFloodFillTimeStamp != iTimestamp)
			{
				pAdjTri->m_iFloodFillTimeStamp = iTimestamp;

				if(!RemoveSmallPatchesOfNavMeshR(pAdjTri, fTotalArea, fThreshold, iTimestamp, iIters, iMaxNumIters))
				{
					pAdjTri->m_bAboveCullAreaThreshold = true;

					return false;
				}
			}
		}
	}

	return true;
}


//#define NUM_WATER_SAMPLES_PER_NAVMESH		100

// Wrapper function for getting the water level at a specified position (has to be within current navmesh)
bool
CNavGen::GetWaterLevel(const float fX, const float fY, float & fWaterHeight)
{
	// If this navmesh section doesn't have water, then return false
	if(!m_bHasWater)
		return false;

	// If this navmesh section doesn't have a waterlevel file, then return the constant water height
	if(!m_pWaterSamples)
	{
		fWaterHeight = m_fWaterLevel;
		return true;
	}

	// TODO: Define 'ms_fWaterSamplingStepSize' in the tri file
	int NUM_WATER_SAMPLES_PER_NAVMESH = (int)
		((CPathServerExtents::GetWorldWidthOfSector() * ((float)CPathServerExtents::m_iNumSectorsPerNavMesh)) / m_fWaterSamplingStepSize);

	// Otherwise index into the waterheights array
	float fSampleX = Max(fX, m_vSectorMins.x);
	fSampleX = Min(fX, m_vSectorMaxs.x);
	float fSampleY = Max(fY, m_vSectorMins.y);
	fSampleY = Min(fY, m_vSectorMaxs.y);

	int iX = (int) ((fSampleX - m_vSectorMins.x) / m_fWaterSamplingStepSize);
	int iY = (int) ((fSampleY - m_vSectorMins.y) / m_fWaterSamplingStepSize);

	if(iX < 0) iX = 0;
	if(iX >= NUM_WATER_SAMPLES_PER_NAVMESH) iX = NUM_WATER_SAMPLES_PER_NAVMESH-1;
	if(iY < 0) iY = 0;
	if(iY >= NUM_WATER_SAMPLES_PER_NAVMESH) iY = NUM_WATER_SAMPLES_PER_NAVMESH-1;

	int iOffset = (iY * NUM_WATER_SAMPLES_PER_NAVMESH) + iX;
	if(iOffset < 0) iOffset = 0;
	if(iOffset >= m_iNumWaterSamples) iOffset = m_iNumWaterSamples-1;

	s16 iWaterHeight = m_pWaterSamples[iOffset];

	// A value of -32000 is a special-case which indicated NO water here.
	if(iWaterHeight == -32000)
		return false;

	fWaterHeight = (float)iWaterHeight;

	return true;
}

// A slice plane is a 3d plane which creates an artificial navmesh surface upon it.
// For now I will assume that each slice plane operates across the entire map - however in
// future it might be better to have a collection of triangles/quads which can allow these
// slice regions to have XY extents.
bool CNavGen::GetHitSlicePlane(const float fX, const float fY, const float fZ1, const float fZ2, float & fHitZ)
{
	const Vector3 vStart(fX, fY, fZ1);
	const Vector3 vEnd(fX, fY, fZ2);

	for(u32 p=0; p<m_SlicePlanes.size(); p++)
	{
		CSlicePlane * pSlicePlane = m_SlicePlanes[p];

		const float fDist1 = DotProduct(pSlicePlane->m_vNormal, vStart) + pSlicePlane->m_fDist;
		const float fDist2 = DotProduct(pSlicePlane->m_vNormal, vEnd) + pSlicePlane->m_fDist;

		if(fDist1 < 0.0f && fDist2 >= 0.0f)
		{
			const Vector3 vPosOnPlane = vStart - (fDist1 * pSlicePlane->m_vNormal);
			fHitZ = vPosOnPlane.z;
			return true;
		}
		else if(fDist2 < 0.0f && fDist1 >= 0.0f)
		{
			const Vector3 vPosOnPlane = vStart - (fDist1 * pSlicePlane->m_vNormal);
			fHitZ = vPosOnPlane.z;
			return true;
		}
	}
	return false;
}


bool sClip2DSegmentAgainstLine(
		float &segX1InOut, float &segY1InOut, float &segZ1InOut,
		float &segX2InOut, float &segY2InOut, float &segZ2InOut,
		float lineA, float lineB, float lineC)
{
	//	lineA*x + lineB*y + lineC = 0;

	const float segX1 = segX1InOut;
	const float segY1 = segY1InOut;
	const float segZ1 = segZ1InOut;
	const float segX2 = segX2InOut;
	const float segY2 = segY2InOut;
	const float segZ2 = segZ2InOut;

	const float signedDist1 = segX1*lineA + segY1*lineB + lineC;
	const float signedDist2 = segX2*lineA + segY2*lineB + lineC;

	bool outside1 = signedDist1 >= 0.0f;
	bool outside2 = signedDist2 >= 0.0f;

	if(outside1 && outside2)
	{
		// Both segment endpoints are "outside" of the line.
		return false;
	}
	else if(!outside1 && !outside2)
	{
		// Both segment endpoints are "inside" of the line, no need to clip.
		return true;
	}

	const float dist1 = fabs(signedDist1);
	const float dist2 = fabs(signedDist2);
	const float distSum = dist1 + dist2;
	if(distSum < SMALL_FLOAT)
	{
		// Both segment endpoints are very close to the line, clipping would be inaccurate.
		// Treating as if the segment was inside is probably safest in our case.
		return true;
	}

	const float t = dist1/distSum;
	const float isectX = (segX2 - segX1)*t + segX1;
	const float isectY = (segY2 - segY1)*t + segY1;
	const float isectZ = (segZ2 - segZ1)*t + segZ1;

	if(outside1)
	{
		segX1InOut = isectX;
		segY1InOut = isectY;
		segZ1InOut = isectZ;
	}
	else
	{
		segX2InOut = isectX;
		segY2InOut = isectY;
		segZ2InOut = isectZ;
	}

	return true;
}


void CNavGen::PlaceNavAerialNodes(CNavGen** pNeighborNavGens, int numNeighborNavGens, const Vector3 & vMinForPlacement, const Vector3 & vMaxForPlacement)
{
	Vector3 vMin = vMinForPlacement;
	Vector3 vMax = vMaxForPlacement;

	const int numOctrees = numNeighborNavGens + 1;
	CNavGenOctree** pOctrees = rage_new CNavGenOctree*[numOctrees];

	pOctrees[0] = &m_Octree;
	for(int i = 0; i < numNeighborNavGens; i++)
	{
		pOctrees[i + 1] = &pNeighborNavGens[i]->m_Octree;
	}

	// Not sure....
	vMax += Vector3(1.0f, 1.0f, 0.0f);

	// Do we need something like this?
	//	float fEdgeEps = m_Params.m_fDistanceToInitiallySeedNodes;

	// Using a min/max passed in (which is taken from the octant calculated by descending the octree)
	// This is because the actual min/max of leaf nodes may be less than the area they logically cover
	// in the octree, being taken as it is from the bounding box of the geometry fragments which
	// ended up in the leaf during octree construction.

	m_pNodeMap = rage_new CPlacedNodeMultiMap(vMin, vMax, m_Params.m_fDistanceToInitiallySeedNodes);

	float fStartX = 0.0f, fStartY = 0.0f;
	int xInt = 0, yInt = 0;
	Vector3 vStart(0,0,0);
	Vector3 vEnd(0,0,0);
//	float fCoincidentDistSqr = 0.1f * 0.1f;

	// Work out where to start in the X & Y directions.
	// We want to place nodes upon a regular grid, regardless of the mins/maxs of the leaf node.
	// We will always round down the start position to be a multiple of "m_fDistanceToInitiallySeedNodes"

	float xFloat = (vMin.x / m_Params.m_fDistanceToInitiallySeedNodes) - 0.5f;
	xInt = (int)xFloat;
	fStartX = ((float)xInt) * m_Params.m_fDistanceToInitiallySeedNodes;
	if(fStartX < vMin.x)
		fStartX = vMin.x;

	float yFloat = (vMin.y / m_Params.m_fDistanceToInitiallySeedNodes) - 0.5f;
	yInt = (int)yFloat;
	fStartY = ((float)yInt) * m_Params.m_fDistanceToInitiallySeedNodes;
	if(fStartY < vMin.y)
		fStartY = vMin.y;

	float fXPos = 0.0f, fYPos = 0.0f;

	const float halfSide = m_Params.m_fDistanceToInitiallySeedNodes*0.5f;
	fStartX -= halfSide;
	fStartY -= halfSide;


	int numPtsX = (int)floorf((vMax.x - fStartX)/m_Params.m_fDistanceToInitiallySeedNodes) + 1;
	int numPtsY = (int)floorf((vMax.y - fStartY)/m_Params.m_fDistanceToInitiallySeedNodes) + 1;

	float *vertexHeightArray = rage_new float[(numPtsX + 2)*(numPtsY + 2)];

	for(int ptY = 0; ptY < numPtsY + 2; ptY++)
	{
		for(int ptX = 0; ptX < numPtsX + 2; ptX++)
		{
			vertexHeightArray[ptY*(numPtsX + 2) + ptX] = vMin.z;
		}
	}


	// Step through in y
	fYPos = fStartY;
	for(int ptY = 0; ptY <= numPtsY; ptY++)
	{
		vStart.y = fYPos;
		vEnd.y = fYPos;

		// Step through in x
		fXPos = fStartX;
//		while(fXPos < vMax.x)
		for(int ptX = 0; ptX <= numPtsX; ptX++)
		{
			if(g_bProcessInBackground) Sleep(0);

			const float fColX1 = fXPos - halfSide;
			const float fColY1 = fYPos - halfSide;
			const float fColX2 = fXPos + halfSide;
			const float fColY2 = fYPos + halfSide;

			Vector3 pts[4];
			pts[0].Set(fColX1, fColY1, vMax.z);
			pts[1].Set(fColX2, fColY1, vMax.z);
			pts[2].Set(fColX2, fColY2, vMax.z);
			pts[3].Set(fColX1, fColY2, vMax.z);

			CClipVolume column;
			column.InitExtrudedQuad(pts, vMax.z - vMin.z, NULL);

			static const int kMaxTris = 256*1024;
			CNavGenTri** triArray = rage_new CNavGenTri*[kMaxTris];


			float maxPtZ = vMin.z;
			for(int k = 0; k < numOctrees; k++)
			{
				int numTris = pOctrees[k]->FindAllTrianglesInVolume(triArray, kMaxTris, column);

				for(int i = 0; i < numTris; i++)
				{
					const CNavGenTri &tri = *triArray[i];

					float planeA = tri.m_vNormal.x;
					float planeB = tri.m_vNormal.y;
					float planeC = tri.m_vNormal.z;
					float planeD = tri.m_fPlaneDist;

					if(planeC < 0.0f)
					{
						planeA = -planeA;
						planeB = -planeB;
						planeC = -planeC;
						planeD = -planeD;
					}


					const float x1 = fXPos - halfSide;
					const float y1 = fYPos - halfSide;
					const float x2 = fXPos + halfSide;
					const float y2 = fYPos + halfSide;

					bool found = false;
					if(planeC > SMALL_FLOAT)
					{
						float xx, yy;
						if(planeA < 0.0f)
						{
							xx = x2;
						}
						else
						{
							xx = x1;
						}
						if(planeB < 0.0f)
						{
							yy = y2;
						}
						else
						{
							yy = y1;
						}

						if(geom2D::Test2DPointVsTri(xx, yy,
								tri.m_vPts[0].x, tri.m_vPts[0].y, 
								tri.m_vPts[1].x, tri.m_vPts[1].y, 
								tri.m_vPts[2].x, tri.m_vPts[2].y))
						{
							const float z = -(xx*planeA + yy*planeB + planeD)/planeC;
							maxPtZ = Max(z, maxPtZ);

							found = true;
						}
					}


					if(!found)
					{
						Vector3 lastPt = tri.m_vPts[2];
						for(int i = 0; i < 3; i++)
						{
							Vector3 pt = tri.m_vPts[i];

							float segXA = lastPt.x;
							float segYA = lastPt.y;
							float segZA = lastPt.z;
							float segXB = pt.x;
							float segYB = pt.y;
							float segZB = pt.z;

							lastPt = pt;

							if(!sClip2DSegmentAgainstLine(segXA, segYA, segZA, segXB, segYB, segZB, 1.0f, 0.0f, -x2))
							{
								continue;
							}
							if(!sClip2DSegmentAgainstLine(segXA, segYA, segZA, segXB, segYB, segZB, -1.0f, 0.0f, x1))
							{
								continue;
							}
							if(!sClip2DSegmentAgainstLine(segXA, segYA, segZA, segXB, segYB, segZB, 0.0f, 1.0f, -y2))
							{
								continue;
							}
							if(!sClip2DSegmentAgainstLine(segXA, segYA, segZA, segXB, segYB, segZB, 0.0f, -1.0f, y1))
							{
								continue;
							}

							maxPtZ = Max(segZA, maxPtZ);
							maxPtZ = Max(segZB, maxPtZ);
						}
					}
				}
			}

			delete []triArray;

			maxPtZ = Min(maxPtZ, vMax.z);

			// Due to the limited sampling resolution, the output mesh is not expected to be
			// all that accurate anyway, and by adding extra quantization in the vertical
			// dimension, we may end up with a piecewise flatter mesh which can be merged
			// more aggressively, reducing the number of polygons. If a quantization value
			// is set in the configuration object, we apply it here.
			const float quantize = fwNavGenConfig::Get().m_AerialVerticalQuantization;
			if(quantize > 0.0f)
			{
				maxPtZ = ceilf(maxPtZ/quantize)*quantize;
			}

			int index1 = (ptY + 0)*(numPtsX + 2) + (ptX + 0);
			int index2 = (ptY + 0)*(numPtsX + 2) + (ptX + 1);
			int index3 = (ptY + 1)*(numPtsX + 2) + (ptX + 1);
			int index4 = (ptY + 1)*(numPtsX + 2) + (ptX + 0);

			vertexHeightArray[index1] = Max(vertexHeightArray[index1], maxPtZ);
			vertexHeightArray[index2] = Max(vertexHeightArray[index2], maxPtZ);
			vertexHeightArray[index3] = Max(vertexHeightArray[index3], maxPtZ);
			vertexHeightArray[index4] = Max(vertexHeightArray[index4], maxPtZ);
			fXPos += m_Params.m_fDistanceToInitiallySeedNodes;
		}

		fYPos += m_Params.m_fDistanceToInitiallySeedNodes;
	}

	fYPos = fStartY + m_Params.m_fDistanceToInitiallySeedNodes;
	for(int ptY = 0; ptY < numPtsY; ptY++)
	{
		fXPos = fStartX + m_Params.m_fDistanceToInitiallySeedNodes;
		for(int ptX = 0; ptX < numPtsX; ptX++)
		{
			// Create a pathnode here
			CNavGenNode * pPathNode = rage_new CNavGenNode();
			pPathNode->m_vBasePos.Set(fXPos - halfSide, fYPos - halfSide, vertexHeightArray[(ptY + 1)*(numPtsX + 2) + ptX + 1]);
//			pPathNode->m_pCollisionTriangleThisIsOn = pTri;
//			pPathNode->m_pCollisionTriangleThisIsOn = NULL;		// Not sure, does it make sense to try to set this?
//			pPathNode->m_bIsWater = false;
//			pPathNode->m_bIsUnderwater = false;
//			pPathNode->m_bIsSlicePlane = false;

			#define NODES_RESERVE_SIZE	16384
			if(!(m_NavSurfaceNodes.size() % NODES_RESERVE_SIZE))
			{
				m_NavSurfaceNodes.reserve(m_NavSurfaceNodes.size() + NODES_RESERVE_SIZE);
			}

			m_NavSurfaceNodes.push_back(pPathNode);

			// Add to node map structure
			m_pNodeMap->AddNode(pPathNode);

#if 0
			if(m_bDontOptimiseMeshEdges)
			{
				const float fEdgeEps = 1.5f*m_Params.m_fDistanceToInitiallySeedNodes;
				const Vector3 &vHitPoint = pPathNode->m_vBasePos;
				if(Eq(m_vSectorMins.x, vHitPoint.x, fEdgeEps) ||
					Eq(m_vSectorMaxs.x, vHitPoint.x, fEdgeEps) ||
					Eq(m_vSectorMins.y, vHitPoint.y, fEdgeEps) ||
					Eq(m_vSectorMaxs.y, vHitPoint.y, fEdgeEps))
				{
					pPathNode->m_iFlags |= NAVGENNODE_ON_EDGE_OF_MESH;
				}
			}
#endif
			m_iInitialPassNumNodesCreated++;

			fXPos += m_Params.m_fDistanceToInitiallySeedNodes;
		}

		fYPos += m_Params.m_fDistanceToInitiallySeedNodes;
	}


	delete []pOctrees;
	delete []vertexHeightArray;

}


int NearestSuperiorPowerOf2( float n ){	return (int) pow( 2, ceil( log( n ) / log( 2.0f ) ) );} 

// Ensure that all the custom resolution areas are kosher
void CorrectCustomResolutionAreas(vector<CNavResolutionArea*> & areas, const float fDistanceToPlaceNodes)
{
	u32 r1;
	for(r1=0; r1<areas.size(); r1++)
	{
		CNavResolutionArea * pArea = areas[r1];

		// min & max must be multiples of fDistanceToPlaceNodes
		pArea->m_vMin.x = ((float)round(pArea->m_vMin.x / fDistanceToPlaceNodes)) * fDistanceToPlaceNodes;
		pArea->m_vMin.y = ((float)round(pArea->m_vMin.y / fDistanceToPlaceNodes)) * fDistanceToPlaceNodes;
		pArea->m_vMax.x = ((float)round(pArea->m_vMax.x / fDistanceToPlaceNodes)) * fDistanceToPlaceNodes;
		pArea->m_vMax.y = ((float)round(pArea->m_vMax.y / fDistanceToPlaceNodes)) * fDistanceToPlaceNodes;

		// 1/resolution must be a power of two
		float fInvRes = 1.0f / pArea->m_fMultiplier;
		fInvRes = (float)NearestSuperiorPowerOf2(fInvRes);
		pArea->m_fMultiplier = 1.0f / fInvRes;

		pArea->m_fMultiplier = Max(pArea->m_fMultiplier, CNavResolutionArea::ms_fMaxResolutionMultiplier);
	}
}

bool IsInCustomResolutionArea(Vector3 & vPos, vector<CNavResolutionArea*> & areas, float & fResMultiplier)
{
	fResMultiplier = 1.0f;
	bool bFoundArea = false;
	for(u32 r=0; r<areas.size(); r++)
	{
		CNavResolutionArea * pArea = areas[r];

		if(vPos.x >= pArea->m_vMin.x && vPos.y >= pArea->m_vMin.y && vPos.z >= pArea->m_vMin.z &&
			vPos.x < pArea->m_vMax.x && vPos.y < pArea->m_vMax.y && vPos.z < pArea->m_vMax.z)
		{
			if(pArea->m_fMultiplier < fResMultiplier)
			{
				fResMultiplier = pArea->m_fMultiplier;
				bFoundArea = true;
			}
		}
	}
	return bFoundArea;
}
bool IsInCustomResolutionAreaXY(Vector3 & vPos, vector<CNavResolutionArea*> & areas, float & fSmallestResMultiplier)
{
	fSmallestResMultiplier = 1.0f;
	bool bFoundArea = false;
	for(u32 r=0; r<areas.size(); r++)
	{
		CNavResolutionArea * pArea = areas[r];

		if(vPos.x >= pArea->m_vMin.x && vPos.y >= pArea->m_vMin.y && 
			vPos.x < pArea->m_vMax.x && vPos.y < pArea->m_vMax.y)
		{
			if(pArea->m_fMultiplier < fSmallestResMultiplier)
				fSmallestResMultiplier = pArea->m_fMultiplier;
			bFoundArea = true;
		}
	}
	return bFoundArea;
}

float GetSmallestResolutionMultiplier(vector<CNavResolutionArea*> & areas)
{
	float fMultiplier = 1.0f;
	for(u32 r=0; r<areas.size(); r++)
	{
		CNavResolutionArea * pArea = areas[r];

		if(pArea->m_fMultiplier < fMultiplier)
			fMultiplier = pArea->m_fMultiplier;
	}
	return fMultiplier;
}

void CNavGen::CreateResolutionAreasForCollisionTriangles(vector<CNavResolutionArea*> * areas)
{
	for(s32 t=0; t<m_CollisionTriangles.size(); t++)
	{
		CNavGenTri * pTri = &m_CollisionTriangles[t];
		/*
		if(pTri->m_colPolyData.GetIsPavement())
		{
			Vector3 vMin, vMax;
			pTri->m_MinMax.GetAsFloats(vMin, vMax);
			CNavResolutionArea * pArea = new CNavResolutionArea();
			pArea->m_vMin = vMin;
			pArea->m_vMax = vMax;
			pArea->m_vMin.z -= 0.5f;
			pArea->m_vMax.z += 1.0f;
			pArea->m_fMultiplier = 0.5f;
			pArea->m_bDynamicallyCreated = true;

			areas->push_back(pArea);
		}
		else */ if(pTri->m_colPolyData.GetIsStairSlopeBound())
		{
			Vector3 vMin, vMax;
			pTri->m_MinMax.GetAsFloats(vMin, vMax);
			CNavResolutionArea * pArea = new CNavResolutionArea();
			pArea->m_vMin = vMin;
			pArea->m_vMax = vMax;
			pArea->m_vMin.z -= 0.5f;
			pArea->m_vMax.z += 1.0f;
			pArea->m_fMultiplier = 0.5f;
			pArea->m_bDynamicallyCreated = true;

			areas->push_back(pArea);
		}
	}
}

//******************************************************************************************
// Places temporary nodes in the bounding area defined by the octree leaf.  Nodes placed
// must be added to the leaf node's m_PathNodes list.  We can greatly speed up our raycasts
// here by using the 2nd version of the ProcessLineOfSight function, and passsing in
// the octree leaf in which to start the algorithm - thus removing the need to traverse
// through the data-structure for each line-test.
//******************************************************************************************


void CNavGen::PlaceNavSurfaceNodes(const Vector3 & vMinForPlacement, const Vector3 & vMaxForPlacement, const bool UNUSED_PARAM(bCreateResAreasForPavement))
{
	char tmpBuf[256];

	Vector3 vMaxExtend(1.0f, 1.0f, 0.0f);

	//if(bCreateResAreasForPavement)
	{
		CreateResolutionAreasForCollisionTriangles(&m_ResolutionAreasCurrentNavMesh);
	}

	CorrectCustomResolutionAreas(m_ResolutionAreasCurrentNavMesh, m_Params.m_fDistanceToInitiallySeedNodes);

	const float fSmallestSamplingDistance = GetSmallestResolutionMultiplier(m_ResolutionAreasCurrentNavMesh) * m_Params.m_fDistanceToInitiallySeedNodes;
	m_pNodeMap = rage_new CPlacedNodeMultiMap(vMinForPlacement, vMaxForPlacement + vMaxExtend, fSmallestSamplingDistance);

	sprintf(tmpBuf, "navmesh (%.1f,%.1f,%.1f) to (%.1f,%.1f,%.1f) at resolution %.3f\n", vMinForPlacement.x, vMinForPlacement.y, vMinForPlacement.z, vMaxForPlacement.x, vMaxForPlacement.y, vMaxForPlacement.z, m_Params.m_fDistanceToInitiallySeedNodes);
	OutputDebugString(tmpBuf);

	PlaceNavSurfaceNodesInArea(vMinForPlacement, vMaxForPlacement, m_Params.m_fDistanceToInitiallySeedNodes);

	m_iNumResAreas = 1;

	for(u32 r=0; r<m_ResolutionAreasCurrentNavMesh.size(); r++)
	{
		CNavResolutionArea * pArea = m_ResolutionAreasCurrentNavMesh[r];
		if(pArea->m_fMultiplier == 1.0f)
			continue;

		const float fRes = pArea->m_fMultiplier * m_Params.m_fDistanceToInitiallySeedNodes;

		sprintf(tmpBuf, "area %i (%.1f,%.1f,%.1f) to (%.1f,%.1f,%.1f) at resolution %.3f\n", r, pArea->m_vMin.x, pArea->m_vMin.y, pArea->m_vMin.z, pArea->m_vMax.x, pArea->m_vMax.y, pArea->m_vMax.z, fRes);
		OutputDebugString(tmpBuf);

		PlaceNavSurfaceNodesInArea(pArea->m_vMin, pArea->m_vMax, fRes);

		m_iNumResAreas++;
	}

	//--------------------------------------------------
	// Mark all nodes which are in "no optimise" areas

	for(u32 r=0; r<m_ResolutionAreasCurrentNavMesh.size(); r++)
	{
		CNavResolutionArea * pArea = m_ResolutionAreasCurrentNavMesh[r];
		if((pArea->m_iFlags & CNavResolutionArea::FLAG_DO_NOT_OPTIMISE)==0)
			continue;

		for(u32 n=0; n<m_NavSurfaceNodes.size(); n++)
		{
			CNavGenNode * pNode = m_NavSurfaceNodes[n];
			if(pNode->IsRemoved())
				continue;
			if(pNode->m_vBasePos.IsGreaterOrEqualThan(pArea->m_vMin) && pArea->m_vMax.IsGreaterThan(pNode->m_vBasePos))
			{
				pNode->m_iFlags |= NAVGENNODE_DO_NOT_OPTIMISE;
			}
		}
	}
}



void CNavGen::PlaceNavSurfaceNodesInArea(const Vector3 & vMinForPlacement, const Vector3 & vMaxForPlacement, const float fSamplingResolution)
{
	Vector3 vMin = vMinForPlacement;
	Vector3 vMax = vMaxForPlacement;

	const bool bWholeMeshArea = (vMin - m_vSectorMins).XYMag2() < 0.1f && (vMax - m_vSectorMaxs).XYMag2() < 0.1f;

	vMax += Vector3(fSamplingResolution, fSamplingResolution, 0.0f);

	const float fEdgeEps = fSamplingResolution;
	const float fResEdgeEps = m_Params.m_fDistanceToInitiallySeedNodes + CNavResolutionArea::ms_fMaxResolutionMultiplier;

	const int iSampleResBit = (int)(1.0f / fSamplingResolution);

	// Using a min/max passed in (which is taken from the octant calculated by descending the octree)
	// This is because the actual min/max of leaf nodes may be less than the area they logically cover
	// in the octree, being taken as it is from the bounding box of the geometry fragments which
	// ended up in the leaf during octree construction.

	float fStartX = 0.0f, fStartY = 0.0f;
	int xInt = 0, yInt = 0;
	Vector3 vStart(0,0,0);
	Vector3 vEnd(0,0,0);
	float fCoincidentDistSqr = 0.1f * 0.1f;

	// Work out where to start in the X & Y directions.
	// We want to place nodes upon a regular grid, regardless of the mins/maxs of the leaf node.
	// We will always round down the start position to be a multiple of "m_fDistanceToInitiallySeedNodes"

	float xFloat = (vMin.x / m_Params.m_fDistanceToInitiallySeedNodes) - 0.5f;
	xInt = (int)xFloat;
	fStartX = ((float)xInt) * m_Params.m_fDistanceToInitiallySeedNodes;
	if(fStartX < vMin.x)
		fStartX = vMin.x;

	float yFloat = (vMin.y / m_Params.m_fDistanceToInitiallySeedNodes) - 0.5f;
	yInt = (int)yFloat;
	fStartY = ((float)yInt) * m_Params.m_fDistanceToInitiallySeedNodes;
	if(fStartY < vMin.y)
		fStartY = vMin.y;

	float fXPos = 0.0f, fYPos = 0.0f;
	fYPos = fStartY;

	Vector3 vHitPoint(0,0,0);
	float fHitDist = 0.0f;
	CNavGenTri * pTri = NULL;
	u32 iRetFlags = 0;

	bool bHit = false;
	bool bHitWater = false;
	bool bUnderwater = false;
	bool bHitSlicePlane = false;

	// count how many we did, for later..
	u32 xNum = 0;
	u32 yNum = 0;

	// Step through in y
	while(fYPos < vMax.y)
	{
		vStart.y = fYPos;
		vEnd.y = fYPos;

		// Step through in x
		fXPos = fStartX;
		while(fXPos < vMax.x)
		{
			if(g_bProcessInBackground) Sleep(0);

#if __DEV
			if(Eq(fYPos, -3450.0f, 0.1f))
			{
				bool bPause=true;	bPause;
			}
#endif

			//if(!m_pNodeMap->IsFlaggedAsProcessed(fXPos, fYPos))
			{
				//m_pNodeMap->FlagAsProcessed(fXPos, fYPos);

				vStart.x = fXPos;
				vEnd.x = fXPos;

				// Repeatedly fire rays down until no more hits are made.
				// Each time nudge the z pos to slightly below the previous intersection to ensure
				// that we get all triangles.  Don't bother with triangles which aren't walkable.
				vStart.z = vMax.z + 10.0f;
				vEnd.z = vMin.z - 100.0f;

				// Start this off with a value outside the bbox
				Vector3 vLastHitPoint(vMin.x - 100.0f, vMin.y - 100.0f, vMin.z - 100.0f);

				// Coords should always fall within this octree leaf
				Assert(vStart.x >= vMin.x && vStart.x <= vMax.x && vStart.y >= vMin.y && vStart.y <= vMax.y);

				//bool bAtEdgeOfNavMesh = IsPositionNearEdgeOfNavMesh(vStart, m_vSectorMins, m_vSectorMaxs, m_Params.m_fDistanceToInitiallySeedNodes);

				const u32 iEdgeFlags = GetPositionNavMeshEdgeFlags(vStart, m_vSectorMins, m_vSectorMaxs, fSamplingResolution); //m_Params.m_fDistanceToInitiallySeedNodes);

				do
				{
					bHitWater = false;
					bUnderwater = false;
					bHitSlicePlane = false;

					if(m_bJitterSamplesForPlacement && iEdgeFlags==0)
					{
						bHit = m_Octree.ProcessLineOfSightVerticalJittered(vStart, vEnd, 0.125f, NAVGEN_COLLISION_TYPES, vHitPoint, fHitDist, pTri, NAVGENTRI_WALKABLE);
						vHitPoint.x = vStart.x;
						vHitPoint.y = vStart.y;
					}
					else
					{
						static const float fEdgeOfMeshAdjustment = 0.1f;
						Vector3 vTmpStart = vStart;
						Vector3 vTmpEnd = vEnd;

						if(iEdgeFlags & EDGEFLAG_NEG_X)
						{
							vTmpStart.x += fEdgeOfMeshAdjustment;
							vTmpEnd.x += fEdgeOfMeshAdjustment;
						}
						if(iEdgeFlags & EDGEFLAG_POS_X)
						{
							vTmpStart.x -= fEdgeOfMeshAdjustment;
							vTmpEnd.x -= fEdgeOfMeshAdjustment;
						}
						if(iEdgeFlags & EDGEFLAG_NEG_Y)
						{
							vTmpStart.y += fEdgeOfMeshAdjustment;
							vTmpEnd.y += fEdgeOfMeshAdjustment;
						}
						if(iEdgeFlags & EDGEFLAG_POS_Y)
						{
							vTmpStart.y -= fEdgeOfMeshAdjustment;
							vTmpEnd.y -= fEdgeOfMeshAdjustment;
						}

						bHit = m_Octree.ProcessLineOfSight(vTmpStart, vTmpEnd, iRetFlags, NAVGEN_COLLISION_TYPES, vHitPoint, fHitDist, pTri, true, NAVGENTRI_WALKABLE);

						vHitPoint.x = vStart.x;
						vHitPoint.y = vStart.y;
					}


					float fWaterLevel;
					if(GetWaterLevel(vStart.x, vStart.y, fWaterLevel))
					{
						if((!bHit && vStart.z > fWaterLevel && vEnd.z < fWaterLevel) ||
							(bHit && vStart.z > fWaterLevel && vHitPoint.z < fWaterLevel))
						{
							bHit = true;
							vHitPoint = vStart;
							vHitPoint.z = fWaterLevel;
							pTri = NULL;
							bHitWater = true;
						}
						else if(bHit && vStart.z < fWaterLevel && vHitPoint.z < fWaterLevel)
						{
							if(m_Params.m_bCreateNavmeshUnderwater)
							{
								bHit = true;
								bUnderwater = true;
								pTri = NULL;	// this is so that surface angle is not considered for underwater navgen polys
							}
							else
							{
								bHit = false;
							}
						}
					}

					float fSlicePlaneZ;
					if(GetHitSlicePlane(vStart.x, vStart.y, vStart.z, vEnd.z, fSlicePlaneZ))
					{
						if(!bHit || fSlicePlaneZ > vHitPoint.z)
						{
							bHit = true;
							bHitSlicePlane = true;
							vHitPoint = vStart;
							vHitPoint.z = fSlicePlaneZ;
							pTri = NULL;
						}
					}

					// Set the water flag if we hit water.
					if(pTri && bHit && pTri->m_colPolyData.GetIsRiverBound())
						bHitWater = true;

					// If hit...
					if(bHit)
					{
	#if __DEBUG_SAMPLE_POINTS
						static bool bPause = false;
						static Vector3 vTestPos(-609.87f, -1612.89f, 26.10f);
						if(vHitPoint.IsClose(vTestPos, 0.75f))
						{
							bPause = true;
						}
	#endif

						Vector3 vDiff = vLastHitPoint - vHitPoint;

						if(vHitPoint.z < vMin.z || vHitPoint.z > vMax.z)
						{
							//  If not within the Z bounds of this leaf..
							vStart = vHitPoint;
							vStart.z -= m_Params.m_fMinZDistBetweenSamples;
						}
						else if(vDiff.Mag2() <= fCoincidentDistSqr)
						{
							// The hit position was identical to the previous hit position.
							// We may have hit a polygon side-on or something.
							// Move the z value by a significant amount.
							vStart = vHitPoint;
							vStart.z -= m_Params.m_fMinZDistBetweenSamples;
						}
						else if(Abs(vDiff.z) < m_Params.m_fMinZDistBetweenSamples)
						{
							// The hit position was too close vertically to the previous node position.
							vStart = vHitPoint;
							vStart.z -= m_Params.m_fMinZDistBetweenSamples;
						}
						else
						{
							bool bCanPlaceNodesOnThisSurface;
							if(pTri)
							{
								if(((pTri->m_iFlags & NAVGENTRI_WALKABLE)!=0) &&
									!pTri->m_colPolyData.GetDontCreateNavMeshOn() )
									bCanPlaceNodesOnThisSurface = true;
								else
									bCanPlaceNodesOnThisSurface = false;
							}
							else
							{
								bCanPlaceNodesOnThisSurface = true;
							}

							if(!pTri || bCanPlaceNodesOnThisSurface)
							{
								// Looks like we can place a node here, the poly is walkable.
								// However, first lets make sure there are no other nodes in the same position.
								// NB : This can be removed once we're sussed what is causing mesh discrepancies & overlaps.
								// NB : Probably only needed on octree boundaries
	#if __DEV
								if(vHitPoint.x < m_vMinOfPlacedNodes.x) m_vMinOfPlacedNodes.x = vHitPoint.x;
								if(vHitPoint.y < m_vMinOfPlacedNodes.y) m_vMinOfPlacedNodes.y = vHitPoint.y;
								if(vHitPoint.z < m_vMinOfPlacedNodes.z) m_vMinOfPlacedNodes.z = vHitPoint.z;
								if(vHitPoint.x > m_vMaxOfPlacedNodes.x) m_vMaxOfPlacedNodes.x = vHitPoint.x;
								if(vHitPoint.y > m_vMaxOfPlacedNodes.y) m_vMaxOfPlacedNodes.y = vHitPoint.y;
								if(vHitPoint.z > m_vMaxOfPlacedNodes.z) m_vMaxOfPlacedNodes.z = vHitPoint.z;
	#endif

								CNavGenNode * pPathNode = m_pNodeMap->GetNode(
									vHitPoint, m_Params.m_fMinZDistBetweenSamples, m_fTriangulationGetNodeEps,
									false, false, false, false, false);

								if(!pPathNode)
								{
									// Create a pathnode here
									pPathNode = rage_new CNavGenNode();
									pPathNode->m_vBasePos = vHitPoint;
									pPathNode->m_pCollisionTriangleThisIsOn = pTri;
									pPathNode->m_bIsWater = bHitWater;
									pPathNode->m_bIsUnderwater = bUnderwater;
									pPathNode->m_bIsSlicePlane = bHitSlicePlane;

									if(pTri && pTri->m_colPolyData.GetIsStairs())
									{
										pPathNode->m_iFlags |= NAVGENNODE_IS_ON_STAIRS_SURFACE;
									}

	#define NODES_RESERVE_SIZE	16384
									if(!(m_NavSurfaceNodes.size() % NODES_RESERVE_SIZE))
									{
										m_NavSurfaceNodes.reserve(m_NavSurfaceNodes.size() + NODES_RESERVE_SIZE);
									}

									m_NavSurfaceNodes.push_back(pPathNode);

									// Add to node map structure
									m_pNodeMap->AddNode(pPathNode);

									m_iInitialPassNumNodesCreated++;

									if(m_bDontOptimiseMeshEdges)
									{
										if(Eq(m_vSectorMins.x, vHitPoint.x, fEdgeEps) ||
											Eq(m_vSectorMaxs.x, vHitPoint.x, fEdgeEps) ||
											Eq(m_vSectorMins.y, vHitPoint.y, fEdgeEps) ||
											Eq(m_vSectorMaxs.y, vHitPoint.y, fEdgeEps))
										{
											pPathNode->m_iFlags |= NAVGENNODE_ON_EDGE_OF_MESH;
										}
									}

									pPathNode->m_iSampleResBits |= iSampleResBit;

								}	// !pExistingNode

								if(!bWholeMeshArea &&
									(Eq(vMinForPlacement.x, vHitPoint.x, fResEdgeEps) ||
									Eq(vMaxForPlacement.x, vHitPoint.x, fResEdgeEps) ||
									Eq(vMinForPlacement.y, vHitPoint.y, fResEdgeEps) ||
									Eq(vMaxForPlacement.y, vHitPoint.y, fResEdgeEps)))
								{
									pPathNode->m_iFlags |= NAVGENNODE_ON_EDGE_OF_RESOLUTION_AREA;
								}

								pPathNode->m_iSampleResBits |= iSampleResBit;

								vLastHitPoint = vHitPoint;

							}	// NAVGENTRI_WALKABLE

							vStart = vHitPoint;
							vStart.z -= m_Params.m_fMinZDistBetweenSamples;
						}

					}	// bHit
					else
					{
						// No vertical LOS intersection here..
						break;
					}

				} while(bHit && vStart.z >= vMin.z);

			} // if !already processed

			fXPos += fSamplingResolution;
			xNum++;
		}

		fYPos += fSamplingResolution;
		yNum++;
	}
}




//***************************************************************************
//	IsLineFreeOfSuddenHeightChanges
//	This function is used to make sure that the height underneath a tri
//	edge does not change by a large amount - which might indicate that
//	this navmesh triangle 'flows up' onto a low wall or some similar
//	piece of geometry.  The only situation where we *should* allow a sudden
//	height change is where either the start/end points of the triangle
//	are sitting on a piece of geometry marked as SURFACE_TYPE_STAIRS
//***************************************************************************

#define COMPARE_WITH_OCTREE 0

bool CNavGen::IsLineFreeOfSuddenHeightChanges(
	CProbeTester & probeTester,
	const Vector3 & vPt1,
	const Vector3 & vPt2,
	CNavGenNode * pNode1,
	CNavGenNode * pNode2,
	const u32 iCollisionTypes,
	bool & bExceededSteepnessThreshold,
	float fIncrement,
	float fMaxHeightChangeValue,
	bool bIgnoreEqualZ,
	bool bQuitIfGroundNotHit,
	bool bQuitIfHitWater,
	bool bQuitIfHitNonWater, 
	bool bInReverse)
{
	if(!ms_bCheckForSuddenHeightChangesUnderTriEdges)
		return true;

	static const float fIdenticalHeightValue = 0.1f;
	const float fMaxHeightChange = fMaxHeightChangeValue;

	const float fExceedSteepnessDistMax = 0.35f;	// 0.25f

	// If input values are more of less identical then don't bother
	if(bIgnoreEqualZ && Eq(vPt1.z, vPt2.z, fIdenticalHeightValue))
	{
		return true;
	}

	Vector3 vVec = vPt2 - vPt1;

	float fLength = vVec.Mag();
	vVec.Normalize();

	float fPosAlongLine = 0.0f;
	Vector3 vStepVec = vVec * fIncrement;

	Vector3 vPos = vPt1;
	Vector3 vEnd = vPos;
	vEnd.z = Min(vPt1.z, vPt2.z) - ms_fHeightChangesUnderTriEdges_HeightToTest;

	CNavGenTri * pHitTri;
	Vector3 vHitPos;
	u32 iRetFlags = 0;

	bool bHit;
	float fHitDist;
	float fHeight;
	float fLastHeight;
	
	if(pNode1 && pNode1->m_bIsWater)
	{
		Vector3 vWaterSurface = vPt1;
		vWaterSurface.z -= m_Params.m_HeightAboveNodeBaseAtWhichToTriangulate;
		bHit = probeTester.ProcessLineOfSight(vWaterSurface, vWaterSurface - (ZAXIS * 10.0f), iRetFlags, iCollisionTypes, vHitPos, fHitDist, pHitTri, true, 0, false, false, true, false, true);
		if(bHit)
			fLastHeight = vWaterSurface.z - fHitDist;
		else
			fLastHeight = vPt1.z - m_Params.m_HeightAboveNodeBaseAtWhichToTriangulate;
	}
	else
	{
		fLastHeight = vPt1.z - m_Params.m_HeightAboveNodeBaseAtWhichToTriangulate;
	}


	int iIterations=0;
	float fDistanceOverSteepnessThreshold = 0.0f;

	while(fPosAlongLine <= fLength)
	{
		if(m_bJitterTestsUnderLine)
		{
			bHit = probeTester.ProcessLineOfSightVerticalJittered(vPos, vEnd, 0.001f, iCollisionTypes, vHitPos, fHitDist, pHitTri, 0, false, false, true, true);

#if COMPARE_WITH_OCTREE
			float fTestDist;
			Vector3 vTestPos;
			CNavGenTri * pTestTri;
			bool bHitB = m_Octree.ProcessLineOfSightVerticalJittered(vPos, vEnd, 0.001f, vTestPos, fTestDist, pTestTri, true);
			Assert(bHit==bHitB);
			Assert(!bHitB || (fTestDist==fHitDist && pTestTri==pHitTri && vTestPos.IsClose(vHitPos, 0.01f)) );
#endif
		}
		else
		{
			bHit = probeTester.ProcessLineOfSight(vPos, vEnd, iRetFlags, iCollisionTypes, vHitPos, fHitDist, pHitTri, true, 0, false, false, true, false, true);
		}

		if(bHit)
		{
			fHeight = vPos.z - fHitDist;

			float fHeightDiff = Abs(fHeight - fLastHeight);
			if(fHeightDiff > fMaxHeightChange)
			{
				return false;
			}

			fLastHeight = fHeight;

			if(pHitTri)
			{
				if(pHitTri->GetIsTooSteep() && pHitTri->m_vNormal.z > 0.003f)
					fDistanceOverSteepnessThreshold += fIncrement;

				if(bQuitIfHitWater && pHitTri->m_colPolyData.GetIsRiverBound())
					return false;
				if(bQuitIfHitNonWater && !pHitTri->m_colPolyData.GetIsRiverBound())
					return false;
			}
			else
			{
				fDistanceOverSteepnessThreshold = 0.0f;
			}
		}
		else
		{
			fDistanceOverSteepnessThreshold = 0.0f;

			if(bQuitIfGroundNotHit)
				return false;
		}

		fPosAlongLine += fIncrement;
		vPos += vStepVec;
		vEnd.x = vPos.x;
		vEnd.y = vPos.y;
		
		// If we're going in reverse, then only bother doing the first 2 iterations.  All we want to achieve is
		// to stop the far end of these triangle edges from riding up onto a piece of wall.  (If this code were
		// smarter it would achieve this in a single pass, without doing all this bInReverse stuff)..
		if(bInReverse)
		{
			iIterations++;
			if( ((pNode1 && pNode1->m_bIsWater) == (pNode2 && pNode2->m_bIsWater)) && iIterations >= 2)
				break;
		}
	}

	if(fDistanceOverSteepnessThreshold > fExceedSteepnessDistMax)
		bExceededSteepnessThreshold = true;

	if(!bInReverse)
	{
		// Now also do the same test in reverse.  This is mainly to prevent the final point from riding
		// up onto low walls - and could be fixed with a single further test - but for now (and to
		// improve accuracy of this test a bit) we'll do the whole line test in reverse along the edge.
		return IsLineFreeOfSuddenHeightChanges(probeTester, vPt2, vPt1, pNode2, pNode1, iCollisionTypes, bExceededSteepnessThreshold, fIncrement, fMaxHeightChangeValue, bIgnoreEqualZ, bQuitIfGroundNotHit, bQuitIfHitWater, bQuitIfHitNonWater, true);
	}

	return true;
}

bool EdgeAlreadyExists(CNavGenNode * pNode1, CNavGenNode * pNode2)
{
	for(u32 t=0; t<pNode1->m_TrianglesUsingThisNode.size(); t++)
	{
		CNavSurfaceTri * pTri = pNode1->m_TrianglesUsingThisNode[t];
		if(!pTri->IsRemoved())
		{
			if( (pNode1 == pTri->m_Nodes[0] && pNode2 == pTri->m_Nodes[1]) ||
				(pNode1 == pTri->m_Nodes[1] && pNode2 == pTri->m_Nodes[2]) ||
				(pNode1 == pTri->m_Nodes[2] && pNode2 == pTri->m_Nodes[0]) )
				return true;
		}
	}
	return false;
}

bool PassesOverlapTest(CNavGenNode ** pNodes, CNavGenNode ** pTestNodes, int iNumTestNodes)
{
	int v,lastv;
	int e,laste;
	int n;
	u32 t;
	float fTVal;

	lastv = 2;
	for(v=0; v<3; v++)
	{
		if(pNodes[v] && pNodes[lastv])
		{
			const float x1 = pNodes[v]->m_vBasePos.x;
			const float y1 = pNodes[v]->m_vBasePos.y;
			const float x2 = pNodes[lastv]->m_vBasePos.x;
			const float y2 = pNodes[lastv]->m_vBasePos.y;

			for(n=0; n<iNumTestNodes; n++)
			{
				if(pTestNodes[n])
				{
					for(t=0; t<pTestNodes[n]->m_TrianglesUsingThisNode.size(); t++)
					{
						CNavSurfaceTri * pTri = pTestNodes[n]->m_TrianglesUsingThisNode[t];
						if(!pTri->IsRemoved())
						{
							laste = 2;
							for(e=0; e<3; e++)
							{
								if( CNavMesh::LineSegsIntersect2D(
									x1, y1, x2, y2,
									pTri->m_Nodes[laste]->m_vBasePos.x, pTri->m_Nodes[laste]->m_vBasePos.y,
									pTri->m_Nodes[e]->m_vBasePos.x, pTri->m_Nodes[e]->m_vBasePos.y, fTVal) )
								{
									if(fTVal > 0.0f && fTVal < 1.0f)
										return false;
								}

								laste = e;
							}
						}
					}
				}
			}
		}

		lastv = v;
	}

	return true;
}


//******************************************************************************
// This function triangulates path nodes into a surface.  For every triangle
// a line-test is done for every edge - if there is no intersection, then the
// triangle is created.
// NB : there is much room for optimisation of these line-tests, since all
// internal triangle edges are shared.
//
// Much of the speed of this function is down to the fact that most of the
// processing is localised to the octree leaf.  However, for nodes along the
// boundary edges of each leaf we will have to do the more laborious process
// of obtaining surrounding nodes via a whole octree query, and similarly for
// the subsequent line-tests.  Bear in mind that at the end of the day we will
// want a single mesh which we can optimize, unconstrained by the octree leaves
// that each section of the mesh originated in.
//******************************************************************************

void CNavGen::TriangulateNavSurface(const Vector3 & vNodeMin, const Vector3 & vNodeMax, int b2ndTriangulationPass)
{
	const float fSmallestResolution = GetSmallestResolutionMultiplier(m_ResolutionAreasCurrentNavMesh) * m_Params.m_fDistanceToInitiallySeedNodes;

	const Vector3 vMin = vNodeMin;
	const Vector3 vMax = vNodeMax + Vector3(1.0f, 1.0f, 0.0f);;

#if __BANK
	static int iNextTri = 0;
#endif
	// This is the value which defines how far apart points may be vertically and
	// yet still be able to be triangulated into the navigation surface.
	// This should really be a function of the m_Params.m_fMaxWalkableAngle, and
	// the m_Params.m_fDistanceToInitiallySeedNodes..
	//
	// NB : Also, if all 3 nodes are sitting on a triangle of surface-type STAIRS (or similar)
	// then we should INCREASE the fValidZInterval, so that we always triangulate up
	// stairs - no matter how steep...

/*	const*/ float fValidZInterval = m_Params.m_TriangulationMaxHeightDiff;

	if(m_Params.m_bAerialNavigation)
	{
		// For aerial meshes, we should probably never strip out any polygon because
		// it covers too much height, as we want them on the sides of skyscrapers and
		// stuff. To achieve this, we simply set this Z interval to be large enough
		// to cover the whole vertical range.
		fValidZInterval = vNodeMax.z - vNodeMin.z;
	}

	// This is the height above each node at which to generate the collision tris.
	const float fHeightAboveNode = m_Params.m_HeightAboveNodeBaseAtWhichToTriangulate;


	int xInt, yInt;
	float fStartX, fStartY, fHighestZ;
	float fXPos, fYPos;
	Vector3 vPos;
	
	CNavGenNode * pNodes[3] = { NULL, NULL, NULL };
	Vector3 vNodeDirections[3] = { VEC3_ZERO, VEC3_ZERO, VEC3_ZERO };
	Vector3 vNodeOffsets[3] = { VEC3_ZERO, VEC3_ZERO, VEC3_ZERO };
	Vector3 vNodePositions[3];

	CNavGenNode * pNearbyNodes[9][9];

	bool bLosResultAbove_v1v2, bLosResultAbove_v2v3, bLosResultAbove_v3v1;

	static const Vector3 vExtrudeNormal(0.0f,0.0f,1.0f);
	
	// This is the spacing along the line at which to test the height-change
	static const float fHeightTestIncrement = 0.125f;
	// This is the maximum height-change for normal surfaces.  NB : If start & end nodes are on stairs, this value could be more ?
	const float fDefaultMaxHeightChangeValue = m_Params.m_MaxHeightChangeUnderTriangleEdge;

	// Every other row in Y, we place triangle differently.
	int n, tri;

	const float xFloat = (vMin.x / m_Params.m_fDistanceToInitiallySeedNodes) - 0.5f;
	xInt = (int)xFloat;
	fStartX = ((float)xInt) * m_Params.m_fDistanceToInitiallySeedNodes;
	if(fStartX < vMin.x)
		fStartX = vMin.x;

	const float yFloat = (vMin.y / m_Params.m_fDistanceToInitiallySeedNodes) - 0.5f;
	yInt = (int)yFloat;
	fStartY = ((float)yInt) * m_Params.m_fDistanceToInitiallySeedNodes;
	if(fStartY < vMin.y)
		fStartY = vMin.y;

	fHighestZ = vNodeMax.z + 10.0f;	//0.1f;

	const float fHeightOfVolumeTests = m_Params.m_fTestClearHeightInTriangulation;

	CCulledGeom culledGeom;
	CProbeTester * pProbeTester;
	if(m_bUseGeometryCuller)
		pProbeTester = &culledGeom;
	else
		pProbeTester = &m_Octree;

	Vector3 vCullGeomExtraSize(
		m_Params.m_fDistanceToInitiallySeedNodes,
		m_Params.m_fDistanceToInitiallySeedNodes,
		fDefaultMaxHeightChangeValue + fValidZInterval);

	bool bQuitLoop = false;

	while(!bQuitLoop)
	{
		bool bWeFoundAnyNodes = false;

		fYPos = fStartY;

		// Step through in X and Y
		while(fYPos < vMax.y)
		{
			fXPos = fStartX;

			while(fXPos < vMax.x)
			{
				if(g_bProcessInBackground) Sleep(0);

				// Start with the highest Z-coordinate node at this position which has not yet been processed.
				// Attempt to make a triangle from this node, and the two to the
				// +X and +Y directions.  Then, continue stepping across in X&Y.  We will come back to this
				// node later, and process new next lower layer of nodes, until all are done.
				
				//ASSERT(fXPos >= vMin.x && fXPos < vNodeMax.x && fYPos >= vMin.y && fYPos < vNodeMax.y);

				CNavGenNode * pNode = m_pNodeMap->GetHighestNodeWhichHasntBeenTriangulated(fXPos, fYPos, b2ndTriangulationPass, m_fTriangulationGetNodeEps);

				if(pNode)
				{
#if __DEV
					static bool bPause = false;
					static Vector3 vTestPos( 648.0f, -1803.0f, 8.51f );
					if(Eq(pNode->m_vBasePos.x, vTestPos.x, 0.1f) && Eq(pNode->m_vBasePos.y, vTestPos.y, 0.1f))
					{
						bPause = true;
					}
#endif
					bWeFoundAnyNodes = true;

					if(!b2ndTriangulationPass)
					{
						pNode->m_iFlags |= NAVGENNODE_HASBEENTRIANGULATED_SO_SKIP;
					}
					else
					{
						pNode->m_iFlags |= NAVGENNODE_HASBEENTRIANGULATED2_SO_SKIP;
					}

					bool bStartNodeIsOnStairs =
						((pNode->m_iFlags & NAVGENNODE_IS_ON_STAIRS_SURFACE)!=0) ?
						true :
						false;

					vPos = pNode->m_vBasePos;

					bool bTrianglesFound;
					do
					{
						bTrianglesFound = false;

						for(tri=0; tri<2; tri++)
						{
							//***************************************************************************
							//	Do the triangles defined by this node, dependent upon the value of 'tri'.
							//	When (tri==0) we do the triangle defined by (node, node-x, node-y)
							//	When (tri==1) we do the triangle defined by (node, node+x, node+y)
							//*****************************************************************

							pNodes[1] = pNodes[2] = NULL;

							// central node
							vNodePositions[0] = vPos;
							vNodeDirections[0].Zero();

							if(!b2ndTriangulationPass)
							{
								if(tri==0)
								{
									vNodeDirections[1].x = -1.0f;
									vNodeDirections[1].y = 0.0f;
									vNodeDirections[2].x = 0.0f;
									vNodeDirections[2].y = -1.0f;

									vNodeOffsets[1].x = 0.0f;
									vNodeOffsets[1].y = 1.0f;
									vNodeOffsets[2].x = 1.0f;
									vNodeOffsets[2].y = 0.0f;
								}
								else 
								{
									vNodeDirections[1].x = 1.0f;
									vNodeDirections[1].y = 0.0f;
									vNodeDirections[2].x = 0.0f;
									vNodeDirections[2].y = 1.0f;

									vNodeOffsets[1].x = 0.0f;
									vNodeOffsets[1].y = -1.0f;
									vNodeOffsets[2].x = -1.0f;
									vNodeOffsets[2].y = 0.0f;
								}
							}
							else
							{
								if(tri==0)
								{
									vNodeDirections[1].x = 0.0f;
									vNodeDirections[1].y = 1.0f;
									vNodeDirections[2].x = -1.0f;
									vNodeDirections[2].y = 0.0f;

									vNodeOffsets[1].x = -1.0f;
									vNodeOffsets[1].y = 0.0f;
									vNodeOffsets[2].x = 0.0f;
									vNodeOffsets[2].y = 1.0f;
								}
								else
								{
									vNodeDirections[1].x = 0.0f;
									vNodeDirections[1].y = -1.0f;
									vNodeDirections[2].x = 0.0f;
									vNodeDirections[2].y = 1.0f;

									vNodeOffsets[1].x = 1.0f;
									vNodeOffsets[1].y = 0.0f;
									vNodeOffsets[2].x = -1.0f;
									vNodeOffsets[2].y = 0.0f;
								}
							}

							pNodes[0] = pNode;

							//*********************************************************************************
							// The 'GetNode' function returns the node within the given interval, which has
							// the highest Z value.  If bStartNodeIsOnStairs is set, the interval is doubled
							// (this is on top of the x2 which we are using here).
							//*********************************************************************************

							u32 iNearbyNodesResBits = 0;
							//const bool bResAreaEdgeNode = true; //(pNode->m_iFlags & NAVGENNODE_ON_EDGE_OF_RESOLUTION_AREA)!=0;
							if(1)
							{
								const Vector3 vNearbyNodesMin(
									pNode->m_vBasePos.x - (CNavResolutionArea::ms_fMaxResolutionMultiplier * 4.0f),
									pNode->m_vBasePos.y - (CNavResolutionArea::ms_fMaxResolutionMultiplier * 4.0f),
									pNode->m_vBasePos.z);

								for(int q=0; q<9; q++)
								{
									for(int r=0; r<9; r++)
									{
										Vector3 vNearPos(
											vNearbyNodesMin.x + ((float)q)*CNavResolutionArea::ms_fMaxResolutionMultiplier,
											vNearbyNodesMin.y + ((float)r)*CNavResolutionArea::ms_fMaxResolutionMultiplier,
											vNearbyNodesMin.z);

										pNearbyNodes[q][r] = m_pNodeMap->GetNode(
											vNearPos,
											fValidZInterval,
											m_fTriangulationGetNodeEps,
											bStartNodeIsOnStairs,
											false,
											false,
											true
										);

										if(pNearbyNodes[q][r])
											iNearbyNodesResBits |= pNearbyNodes[q][r]->m_iSampleResBits;
									}
								}
							}

							// if any mixture of sampling resolutions
							const bool bResAreaEdgeNode =
								iNearbyNodesResBits != 0 &&
								iNearbyNodesResBits != 1 &&
								iNearbyNodesResBits != 2 &&
								iNearbyNodesResBits != 4;

							// Obtain closest node in each direction

							for(n=1; n<3; n++)
							{
								pNodes[n] = NULL;

								float fOffsetSide = 0.0f;
								while(fOffsetSide < m_Params.m_fDistanceToInitiallySeedNodes)
								{
									float fOffset = fSmallestResolution;
									while(fOffset <= m_Params.m_fDistanceToInitiallySeedNodes)
									{
										vNodePositions[n] = vNodePositions[0] + (vNodeDirections[n] * fOffset);
										vNodePositions[n] += (vNodeOffsets[n] * fOffsetSide);
										Vector3 vNodeObtainPos = vNodePositions[n];
										vNodeObtainPos.z += 0.5f;

										pNodes[n] = m_pNodeMap->GetNode(
											vNodeObtainPos,
											fValidZInterval,
											m_fTriangulationGetNodeEps,
											bStartNodeIsOnStairs,
											false,
											false,
											true
										);

										if(pNodes[n] && pNodes[n] != pNodes[0])
										{
											const bool bPassesNearbyNodesTest = bResAreaEdgeNode ?
												PassesOverlapTest(pNodes, (CNavGenNode**)pNearbyNodes, 9*9) : true;

											if(bPassesNearbyNodesTest)
											{
												const bool bEdgeAlreadyExists = bResAreaEdgeNode ?
													(n == 2) ?
														EdgeAlreadyExists(pNodes[0], pNodes[n]) :
														EdgeAlreadyExists(pNodes[n], pNodes[0])
													: false;

												if(!bEdgeAlreadyExists)
												{
													break;

													//const bool bPassesOvelapTest = bResAreaEdgeNode ? PassesOverlapTest(pNodes, pNodes, 3) : true;
													//if(bPassesOvelapTest)
													//	break;
												}
											}
										}
					 					pNodes[n] = NULL;
										fOffset += fSmallestResolution;
									}
									if(pNodes[n])
										break;
									fOffsetSide += fSmallestResolution;
								}
								if(!pNodes[n])
									break;
							}

							if(n != 3)
								continue;

							// If any of the 3 positions that will define this triangle, are outside the extents
							// of the navmesh min/max, then don't create this triangle.  This is an early out which
							// stops us having to do the GetNode() calls later - which are expensive, and will in
							// this case fail anyway.
							for(n=1; n<3; n++)
							{
								if(vNodePositions[n].x < vNodeMin.x || vNodePositions[n].y < vNodeMin.y)
									break;
							}
							if(n != 3)
								continue;

							// If this the 2nd triangulation pass, we need to check that the 1st pass didn't
							// create triangles which are gonna conflict with the ones we are trying to create now..
							if(b2ndTriangulationPass)
							{
								if(tri==0)
								{
									if(pNodes[0]->m_iFlags & NAVGENNODE_EDGE_EXISTS_DIAGONAL_NEGX_POSY)
									{
										continue;
									}
								}
								else
								{
									if(pNodes[0]->m_iFlags & NAVGENNODE_EDGE_EXISTS_DIAGONAL_POSX_NEGY)
									{ 
										continue;
									}
								}
							}

							int iNumWater = 0;
							if(pNodes[0]->m_bIsWater) iNumWater++;
							if(pNodes[1]->m_bIsWater) iNumWater++;
							if(pNodes[2]->m_bIsWater) iNumWater++;


							int iNumSlicePlane = 0;
							if(pNodes[0]->m_bIsSlicePlane) iNumSlicePlane++;
							if(pNodes[1]->m_bIsSlicePlane) iNumSlicePlane++;
							if(pNodes[2]->m_bIsSlicePlane) iNumSlicePlane++;

							// If all surfaces are water, then this poly is marked as such also
							//bool bWater = (iNumWater==3);

							// If any nodes are water, then this poly is marked as such also
							bool bWater = (iNumWater!=0);
							const bool bSlicePlane = (iNumSlicePlane==3);


							const bool bNodesAreOnStairs =
								((pNodes[0]->m_iFlags & NAVGENNODE_IS_ON_STAIRS_SURFACE) ||
								(pNodes[1]->m_iFlags & NAVGENNODE_IS_ON_STAIRS_SURFACE) ||
								(pNodes[2]->m_iFlags & NAVGENNODE_IS_ON_STAIRS_SURFACE));

							const float fTestZInterval = (bNodesAreOnStairs) ? fValidZInterval * 2.0f : fValidZInterval;

							// Make sure all nodes are within fValidZInterval
							const float fMinZ = Min(pNodes[0]->m_vBasePos.z, Min(pNodes[1]->m_vBasePos.z, pNodes[2]->m_vBasePos.z));
							const float fMaxZ = Max(pNodes[0]->m_vBasePos.z, Max(pNodes[1]->m_vBasePos.z, pNodes[2]->m_vBasePos.z));
							const float fMidZ = (fMinZ + fMaxZ) * 0.5f;

							if(!m_Params.m_bAerialNavigation)
							{
								if(!Eq(pNodes[0]->m_vBasePos.z, fMidZ, fTestZInterval) ||
									!Eq(pNodes[1]->m_vBasePos.z, fMidZ, fTestZInterval) ||
									!Eq(pNodes[2]->m_vBasePos.z, fMidZ, fTestZInterval))
								{
									// Although we found 2 nodes, they are not close enough vertically to triangulate.
									continue;
								}
							}

							//********************************************************************************
							// Check the normal of the triangle which will be created.
							// There is a nasty situation where points may be sampled outside of the valid
							// area for this navmesh - and if these points end up getting triangulated
							// we get vertical polygons.  Make sure the surface normal of this new triangle
							// makes is walkable..

							Vector3 vClampedNodes[3];
							int c;

							for(c=0; c<3; c++)
							{
								vClampedNodes[c] = pNodes[c]->m_vBasePos;
								vClampedNodes[c].x = Clamp(vClampedNodes[c].x, vNodeMin.x, vNodeMax.x);
								vClampedNodes[c].y = Clamp(vClampedNodes[c].y, vNodeMin.y, vNodeMax.y);
								vClampedNodes[c].z = Clamp(vClampedNodes[c].z, vNodeMin.z, vNodeMax.z);
							}

							Vector3 vEdge1 = vClampedNodes[0] - vClampedNodes[1];
							Vector3 vEdge2 = vClampedNodes[2] - vClampedNodes[1];
							vEdge1.Normalize();
							vEdge2.Normalize();
							Vector3 vNormal = CrossProduct(vEdge2, vEdge1);
							vNormal.Normalize();

							bool bTooSteep[3] = { false, false, false };

							if(!m_Params.m_bAerialNavigation)
							{
								if(DotProduct(vNormal, Vector3(0.0f,0.0f,1.0f)) < 0.2f)
									continue;

								//*************************************************************************
								// Check all 3 nodes' flags.  There may be some LOS flags set, which will
								// allow us to avoid repeating the costly LOS tests multiple times.
								//*************************************************************************

								bool bLos_v1v2 = false, bLos_v2v3 = false, bLos_v3v1 = false;
								bool bCheckResult_v1v2 = false, bCheckResult_v2v3 = false, bCheckResult_v3v1 = false;

								//*************************************************************************
								// Check that all 3 points can see each other, via 3 line-tests
								//*************************************************************************

								Vector3 v1 = pNodes[0]->m_vBasePos;
								v1.z += fHeightAboveNode;
								Vector3 v2 = pNodes[1]->m_vBasePos;
								v2.z += fHeightAboveNode;
								Vector3 v3 = pNodes[2]->m_vBasePos;
								v3.z += fHeightAboveNode;

								/*
								bLos_v1v2 = m_Octree.TestLineOfSight(v1, v2);
								bLos_v2v3 = m_Octree.TestLineOfSight(v2, v3);
								bLos_v3v1 = m_Octree.TestLineOfSight(v3, v1);
								*/

								bLos_v1v2 = true; //m_Octree.TestLineOfSight(v1, v2);
								bLos_v2v3 = true; //m_Octree.TestLineOfSight(v2, v3);
								bLos_v3v1 = true; //m_Octree.TestLineOfSight(v3, v1);

#if __DEV
								static Vector3 vTest0( 648.0f, -1802.0f, 8.51f );
								static Vector3 vTest1( 649.0f, -1803.0f, 8.51f );
								static bool bDbgBreak = false;

								if(pNodes[0]->m_vBasePos.IsClose(vTest0, 0.2f) && pNodes[1]->m_vBasePos.IsClose(vTest1, 0.2f))
								{
									bDbgBreak = true;
								}
#endif

								bool bIsClear = (bLos_v1v2 && bLos_v2v3 && bLos_v3v1);
								if(bIsClear)
								{
									Assert(fHeightAboveNode < fHeightOfVolumeTests);

									CClipVolume vol;
									const Vector3 vPts[3] = { v3, v2, v1 };

									vol.InitExtrudedTri(vPts, fHeightOfVolumeTests - fHeightAboveNode, &vExtrudeNormal);

									bIsClear = m_Octree.TestVolumeIsClear(vol, NAVGEN_COLLISION_TYPES);
								}

								bLos_v1v2 = bLos_v2v3 = bLos_v3v1 = bIsClear;
								bLosResultAbove_v1v2 = bLosResultAbove_v2v3 = bLosResultAbove_v3v1 = bIsClear;

								const float fMaxHeightChange = fDefaultMaxHeightChangeValue;

								if(bIsClear)
								{
									if(iNumSlicePlane)	// We don't bother with height-change test for slice-plane polys
									{
										bCheckResult_v1v2 = bCheckResult_v2v3 = bCheckResult_v3v1 = true;
									}
									else
									{
										if(m_bUseGeometryCuller)
										{
											culledGeom.Reset();
											Vector3 vTriMin, vTriMax;
											vTriMin.Min(v1, v2);
											vTriMin.Min(vTriMin, v3);
											vTriMax.Max(v1, v2);
											vTriMax.Max(vTriMax, v3);
											vTriMin.z -= ms_fHeightChangesUnderTriEdges_HeightToTest;
											//vTriMax.z += (fHeightOfVolumeTests - fHeightAboveNode);

											culledGeom.Init(vTriMin - vCullGeomExtraSize, vTriMax + vCullGeomExtraSize);
											m_Octree.AddTrianglesToCulledGeom(culledGeom);
										}

										if(pNodes[0]->m_bIsWater && pNodes[1]->m_bIsWater)
										{
											bCheckResult_v1v2 = true;
										}
										else
										{
											bool bHitIfNoGroundHit = (pNodes[0]->m_bIsWater != pNodes[1]->m_bIsWater);
											bCheckResult_v1v2 = IsLineFreeOfSuddenHeightChanges(*pProbeTester, v1, v2, pNodes[0], pNodes[1], NAVGEN_COLLISION_TYPES_EXCEPT_RIVER, bTooSteep[0], fHeightTestIncrement, fMaxHeightChange, false, bHitIfNoGroundHit, false, false);
										}
										if(!bCheckResult_v1v2)
											continue;

										if(pNodes[1]->m_bIsWater && pNodes[2]->m_bIsWater)
										{
											bCheckResult_v2v3 = true;
										}
										else
										{
											bool bHitIfNoGroundHit = (pNodes[1]->m_bIsWater != pNodes[2]->m_bIsWater);
											bCheckResult_v2v3 = IsLineFreeOfSuddenHeightChanges(*pProbeTester, v2, v3, pNodes[1], pNodes[2], NAVGEN_COLLISION_TYPES_EXCEPT_RIVER, bTooSteep[1], fHeightTestIncrement, fMaxHeightChange, false, bHitIfNoGroundHit, false, false);
										}
										if(!bCheckResult_v2v3)
											continue;

										if(pNodes[2]->m_bIsWater && pNodes[0]->m_bIsWater)
										{
											bCheckResult_v3v1 = true;
										}
										else
										{
											bool bHitIfNoGroundHit = (pNodes[2]->m_bIsWater != pNodes[0]->m_bIsWater);
											bCheckResult_v3v1 = IsLineFreeOfSuddenHeightChanges(*pProbeTester, v3, v1, pNodes[2], pNodes[0], NAVGEN_COLLISION_TYPES_EXCEPT_RIVER, bTooSteep[2], fHeightTestIncrement, fMaxHeightChange, false, bHitIfNoGroundHit, false, false);
										}
										if(!bCheckResult_v3v1)
											continue;
									}
								}
								else
								{
									bCheckResult_v1v2 = bCheckResult_v2v3 = bCheckResult_v3v1 = false;
								}

								// Set flags in these 3 nodes, to indicate the result of this line-test.
								// We can check these next time we come to do a line-test between any of
								// the same nodes.

								// If there's no LOS on any of the triangle edges, then we can't triangulate these points
								if(!bLos_v1v2 || !bLos_v2v3 || !bLos_v3v1)
									continue;

								// If the height-change check failed underneath the triangle edges, then we can't triangulate
								if(!bCheckResult_v1v2 || !bCheckResult_v2v3 || !bCheckResult_v3v1)
									continue;

								// There was no LOS for the triangle created directly above the actual triangle.  Therefore we
								// can assume that there was some blocking collision geometry here, just above the navmesh surface.
								if(!bLosResultAbove_v1v2 || !bLosResultAbove_v2v3 || !bLosResultAbove_v3v1)
									continue;
							}

							//************************************************************
							// Now create a triangle from these 3 nodes

							CNavSurfaceTri * pNavTri = rage_new CNavSurfaceTri();

							InitNavSurfaceTri(pNavTri, pNodes, bWater, bSlicePlane);
							pNavTri->InitMinMax();
							
							if(bTooSteep[0] || bTooSteep[1] || bTooSteep[2])
							{
								pNavTri->m_bIsTooSteep = true;
							}

							//************************************************************


							#define TRIANGLES_RESERVE_SIZE		16384
							if(!(m_NavSurfaceTriangles.size() % TRIANGLES_RESERVE_SIZE))
							{
								m_NavSurfaceTriangles.reserve(m_NavSurfaceTriangles.size() + TRIANGLES_RESERVE_SIZE);
							}

							// Add the triangle to the list in the octree leaf

							m_NavSurfaceTriangles.push_back(pNavTri);

							// Add associations, which will be used later in optimising the mesh
							AssociateTriAndNode(pNavTri, pNodes[0]);
							AssociateTriAndNode(pNavTri, pNodes[1]);
							AssociateTriAndNode(pNavTri, pNodes[2]);

							// Set the flags in each of the 3 nodes, to indicate which triangle edges
							// have been formed.  In the 2nd triangulation passes we'll examine these
							// to figure out what additional triangles we can make..
							if(!b2ndTriangulationPass)
							{
								if(tri==0)
								{
									pNodes[0]->m_iFlags |= NAVGENNODE_EDGE_EXISTS_NEGX;
									pNodes[0]->m_iFlags |= NAVGENNODE_EDGE_EXISTS_NEGY;
									pNodes[1]->m_iFlags |= NAVGENNODE_EDGE_EXISTS_POSX;
									pNodes[1]->m_iFlags |= NAVGENNODE_EDGE_EXISTS_DIAGONAL_POSX_NEGY;
									pNodes[2]->m_iFlags |= NAVGENNODE_EDGE_EXISTS_POSY;
									pNodes[2]->m_iFlags |= NAVGENNODE_EDGE_EXISTS_DIAGONAL_NEGX_POSY;
								}
								else
								{
									pNodes[0]->m_iFlags |= NAVGENNODE_EDGE_EXISTS_POSX;
									pNodes[0]->m_iFlags |= NAVGENNODE_EDGE_EXISTS_POSY;
									pNodes[1]->m_iFlags |= NAVGENNODE_EDGE_EXISTS_NEGX;
									pNodes[1]->m_iFlags |= NAVGENNODE_EDGE_EXISTS_DIAGONAL_NEGX_POSY;
									pNodes[2]->m_iFlags |= NAVGENNODE_EDGE_EXISTS_NEGY;
									pNodes[2]->m_iFlags |= NAVGENNODE_EDGE_EXISTS_DIAGONAL_POSX_NEGY;
								}
							}
							else
							{

							}

							m_iInitialPassNumTrianglesCreated++;
							bTrianglesFound = true;
						}

					} while(0); //bTrianglesFound);
				}

				fXPos += fSmallestResolution;
			}

			fYPos += fSmallestResolution;
		}

		if(!bWeFoundAnyNodes)
		{
			bQuitLoop = true;
		}
	}
}

void CNavGen::InitNavSurfaceTri(CNavSurfaceTri * pNavTri, CNavGenNode ** ppNodes, bool bWater, bool bSlicePlane)
{
	pNavTri->m_Nodes[0] = ppNodes[2];
	pNavTri->m_Nodes[1] = ppNodes[1];
	pNavTri->m_Nodes[2] = ppNodes[0];

	pNavTri->m_vNormal = pNavTri->CalcNormal();
	pNavTri->m_vNormal.Normalize();
	pNavTri->m_fPlaneD = - pNavTri->m_vNormal.Dot(pNavTri->m_Nodes[0]->m_vBasePos);

	const CNavGenTri * pColTri1 = pNavTri->m_Nodes[0]->m_pCollisionTriangleThisIsOn;
	const CNavGenTri * pColTri2 = pNavTri->m_Nodes[1]->m_pCollisionTriangleThisIsOn;
	const CNavGenTri * pColTri3 = pNavTri->m_Nodes[2]->m_pCollisionTriangleThisIsOn;

	// If all surfaces are pavement, then this poly is marked as such also
	const bool bAllPavement = (pColTri1 && pColTri2 && pColTri3) &&
		pColTri1->m_colPolyData.GetIsPavement() &&
		pColTri2->m_colPolyData.GetIsPavement() &&
		pColTri3->m_colPolyData.GetIsPavement();

	s32 iNumPavement = 0;
	if(pColTri1 && pColTri2 && pColTri3)
	{
		if(pColTri1->m_colPolyData.GetIsPavement()) iNumPavement++;
		if(pColTri2->m_colPolyData.GetIsPavement()) iNumPavement++;
		if(pColTri3->m_colPolyData.GetIsPavement()) iNumPavement++;
	}


	const bool bInterior = (pColTri1 && pColTri2 && pColTri3) &&
		pColTri1->m_colPolyData.GetIsInterior() &&
		pColTri2->m_colPolyData.GetIsInterior() &&
		pColTri3->m_colPolyData.GetIsInterior();

	const bool bTooSteep = (pColTri1 && pColTri2 && pColTri3) &&
		(pColTri1->m_iFlags & NAVGENTRI_TOOSTEEP) &&
		(pColTri2->m_iFlags & NAVGENTRI_TOOSTEEP) &&
		(pColTri3->m_iFlags & NAVGENTRI_TOOSTEEP);

	const bool bNoNetworkSpawn = (pColTri1 && pColTri2 && pColTri3) &&
		(pColTri1->m_colPolyData.GetIsNoNetworkSpawn() &&
		pColTri2->m_colPolyData.GetIsNoNetworkSpawn() &&
		pColTri3->m_colPolyData.GetIsNoNetworkSpawn() );

	// Bitwise OR the baked-in data bitfield
	u8 iBitfield;
	if(pColTri1 && pColTri2 && pColTri3)
	{
		iBitfield = 
			pColTri1->m_iBakedInDataBitField |
			pColTri2->m_iBakedInDataBitField |
			pColTri3->m_iBakedInDataBitField;
	}
	else
	{
		iBitfield = 0;
	}

	/*
	float fPedDensity = 0.0f;
	if(pColTri1 && pColTri2 && pColTri3)
	{
		if( iNumPavement==0 || iNumPavement==3 )
		{
			fPedDensity = (float)Max( pColTri1->m_colPolyData.GetPedDensity(), Max(pColTri2->m_colPolyData.GetPedDensity(), pColTri3->m_colPolyData.GetPedDensity() ) );
		}
	}
	pNavTri->m_colPolyData.SetPedDensity((s32)fPedDensity);
	*/

	pNavTri->m_colPolyData.SetPedDensity(0); // ped density now set exclusively in CorrectPedDensity() during analysis phase
	pNavTri->m_colPolyData.SetIsPavement(bAllPavement);
	
	pNavTri->m_colPolyData.SetIsInterior(bInterior);
	pNavTri->m_colPolyData.SetIsNoNetworkSpawn(bNoNetworkSpawn);

	pNavTri->m_bIsTooSteep = bTooSteep;

	pNavTri->m_iBakedInBitField = iBitfield;

	if(bWater)
		pNavTri->m_bIsWater = true;
	if(bSlicePlane)
		pNavTri->m_bIsSlicePlane = true;

	pNavTri->m_iFreeSpaceTopZ = TPolyVolumeInfo::ms_iFreeSpaceNone;

	if(m_bCalculateSpaceAboveAllPolys)
		pNavTri->m_bCalcSpaceAbove = true;

	if((ppNodes[0]->m_iFlags & NAVGENNODE_DO_NOT_OPTIMISE)!=0 &&
		(ppNodes[1]->m_iFlags & NAVGENNODE_DO_NOT_OPTIMISE)!=0 &&
		(ppNodes[2]->m_iFlags & NAVGENNODE_DO_NOT_OPTIMISE)!=0)
	{
		pNavTri->m_bDoNotOptimise = true;
	}
}


//*******************************************************************************
//	FindDepthClearAboveAndBelow
//	This function performs linetests to determine how much free space exists
//	both above and/or below the input triangle.
//	Water surface polygons track the clear space below them down to the seabed.
//	In the future all polygons might track the free space above them also, so we
//	can do proper aerial navigation.

void
CNavGen::FindDepthClearAboveAndBelow(CNavGenNode ** pNodes, s32 * pFirstIntersectionAboveZ, s32 * pFirstIntersectionBelowZ, const bool bTestUpToWaterLevel)
{
	Vector3 vHitPos;
	float fHitDist;
	CNavGenTri * pHitTri;
	const Vector3 vRaiseVec(0.0f, 0.0f, 0.25f);
	u32 iRetFlags = 0;

	//**********************************************************************
	// See how much space is above, if we have an integer pointer passed in

	if(pFirstIntersectionAboveZ)
	{
		float fLowestIntersectionPos = FLT_MAX;
		float fTopZ;

		for(int s=0; s<3; s++)
		{
			if(bTestUpToWaterLevel)
			{
				const bool bGotWaterLevel = GetWaterLevel(pNodes[s]->m_vBasePos.x, pNodes[s]->m_vBasePos.y, fTopZ);
				Assert(bGotWaterLevel);
				if(!bGotWaterLevel)
					continue;
			}
			else
			{
				fTopZ = pNodes[s]->m_vBasePos.z + (float)MAX_WATER_DEPTH_CHECK;
			}

			const Vector3 vTestToPos(pNodes[s]->m_vBasePos.x, pNodes[s]->m_vBasePos.y, fTopZ);
			const bool bHit = m_Octree.ProcessLineOfSight(pNodes[s]->m_vBasePos + vRaiseVec, vTestToPos, iRetFlags, NAVGEN_COLLISION_TYPES, vHitPos, fHitDist, pHitTri, true);

			if(bHit)
			{
				fLowestIntersectionPos = Min(fLowestIntersectionPos, vHitPos.z);
			}
			else
			{
				fLowestIntersectionPos = Min(fLowestIntersectionPos, fTopZ);
			}
		}

		Assert(fLowestIntersectionPos < FLT_MAX);
		*pFirstIntersectionAboveZ = (s32)fLowestIntersectionPos;
	}

	//**********************************************************************
	// See how much space is below, if we have an integer pointer passed in

	if(pFirstIntersectionBelowZ)
	{
		float fHighestIntersectionPos = -FLT_MAX;
		float fBottomZ;

		for(int s=0; s<3; s++)
		{
			fBottomZ = pNodes[s]->m_vBasePos.z - (float)MAX_WATER_DEPTH_CHECK;
			const Vector3 vTestToPos(pNodes[s]->m_vBasePos.x, pNodes[s]->m_vBasePos.y, fBottomZ);
			const bool bHit = m_Octree.ProcessLineOfSight(pNodes[s]->m_vBasePos, vTestToPos, iRetFlags, NAVGEN_COLLISION_TYPES, vHitPos, fHitDist, pHitTri, true);

			if(bHit)
			{
				fHighestIntersectionPos = Max(fHighestIntersectionPos, vHitPos.z);
			}
			else
			{
				fHighestIntersectionPos = Max(fHighestIntersectionPos, fBottomZ);
			}
		}

		Assert(fHighestIntersectionPos > -FLT_MAX);
		*pFirstIntersectionBelowZ = (s32)fHighestIntersectionPos;
	}
}

void CNavGen::RemoveRiverBedTrianglesAfterOptimisation()
{
	static const float fTestDist = 50.0f;

	for(u32 t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pNavTri = m_NavSurfaceTriangles[t];
		if(pNavTri->IsRemoved())
			continue;

		Vector3 vCenter = (pNavTri->m_Nodes[0]->m_vBasePos + pNavTri->m_Nodes[0]->m_vBasePos + pNavTri->m_Nodes[0]->m_vBasePos) / 3.0f;

		Vector3 vHitPos;
		float fHitDist = 0.0f;
		CNavGenTri * pHitTri = NULL;
		u32 iRetFlags = 0;

		if( m_Octree.ProcessLineOfSight(vCenter + (ZAXIS * 0.1f), vCenter + (ZAXIS * fTestDist), iRetFlags, NAVGEN_COLLISION_TYPES, vHitPos, fHitDist, pHitTri, true) )
		{
			if(pHitTri && pHitTri->m_colPolyData.GetIsRiverBound())
			{
				pNavTri->SetRemoved();
			}
		}
	}
}

void CNavGen::CullTrianglesBehindWallsAfterOptimisation()
{
	static const float fSmallAmt = 0.25f;

	for(u32 t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pNavTri = m_NavSurfaceTriangles[t];
		if(pNavTri->IsRemoved())
			continue;

		Vector3 v1 = pNavTri->m_Nodes[0]->m_vBasePos;
		Vector3 v2 = pNavTri->m_Nodes[1]->m_vBasePos;
		Vector3 v3 = pNavTri->m_Nodes[2]->m_vBasePos;

		v1.z += fSmallAmt;
		v2.z += fSmallAmt;
		v3.z += fSmallAmt;

		if(IsTriangleBehindWall(v1, v2, v3))
		{
			pNavTri->SetRemoved();
		}
	}
}

#define WALL_TEST_NUM_ABOVE		4
#define WALL_TEST_NUM_PROBES	9
#define WALL_TEST_NUM_ORIGINS	1	//4


bool
CNavGen::IsTriangleBehindWall(const Vector3 & vPt1, const Vector3 & vPt2, const Vector3 & vPt3)
{
	static const float fTestDist = 50.0f;
	static const float fTestAboveHeight = 50.0f;
	static const float fSmallDistanceAbovePoly = 2.0f;
	static const float fSmallDistAbove = 0.5f;
	static const float fOriginRaiseUpDist = 0.5f;
	static const float fPercentageOfReverseIntersections = 50.0f;

	static bool bInitialised = false;
	static Vector3 vTestVectors[WALL_TEST_NUM_PROBES];
	
	if(!bInitialised)
	{
		bInitialised = true;

		vTestVectors[0] = Vector3(0.0f,0.0f,fTestDist);
		
		vTestVectors[1] = Vector3(0.0f,fTestDist,0.0f);
		vTestVectors[2] = Vector3(0.0f,-fTestDist,0.0f);
		vTestVectors[3] = Vector3(fTestDist,0.0f,0.0f);
		vTestVectors[4] = Vector3(-fTestDist,0.0f,0.0f);

		vTestVectors[5] = Vector3(0.0f,fTestDist*0.707f,fTestDist*0.707f);
		vTestVectors[6] = Vector3(0.0f,-fTestDist*0.707f,fTestDist*0.707f);
		vTestVectors[7] = Vector3(fTestDist*0.707f,0.0f,fTestDist*0.707f);
		vTestVectors[8] = Vector3(-fTestDist*0.707f,0.0f,fTestDist*0.707f);
	}

	Vector3 vHitPos;
	bool bHit;
	int iSide;

	//*************************************************************************************
	//	Fire rays upwards to determine whether this triangle is potentially enclosed.
	//	We will only perform these tests on triangles which have collision above them.
	//	If we hit the reverse of a collision poly then we can quickly cull this poly too..

	Vector3 vVertOffset(0.0f, 0.0f, fSmallDistAbove);
	Vector3 vVertTestVec(0.0f, 0.0f, fTestAboveHeight);

	Vector3 vCentroid = (vPt1 + vPt2 + vPt3) / 3.0f;
	Vector3 vUpTestOrigins[WALL_TEST_NUM_ABOVE] =
	{
		vCentroid + vVertOffset,
		vPt1 + vVertOffset,
		vPt2 + vVertOffset,
		vPt3 + vVertOffset
	};

	int iNumWithColAbove=0;
	int iNumHitReverse=0;
	int t;

	for(t=0; t<WALL_TEST_NUM_ABOVE; t++)
	{
		bHit = m_Octree.GetRayIntersectPolySide(vUpTestOrigins[t], vUpTestOrigins[t] + vVertTestVec, iSide, NAVGEN_COLLISION_TYPES, &vHitPos);
		if(bHit)
		{
			iNumWithColAbove++;

			// If this poly has collision right above & very close, then don't consider this intersection
			// towards the culling of the poly.  It is highly likely that the navmesh poly is in some
			// way intersecting the scene geometry (quite often happens due to the way that the mesh
			// optimization operates on the geometry, ie. progressive mesh collapsing)
			if(Abs(vHitPos.z - vUpTestOrigins[t].z) <= fSmallDistanceAbovePoly)
			{

			}
			else
			{
				if(iSide==-1)
					iNumHitReverse++;
			}
		}
	}

	// If there was no collision found above whatsoever, then we need to leave this poly alone
	if(!iNumWithColAbove)
		return false;

	//************************************************************
	//	Run the test proper for this triangle

	int iNumTestsDone = 0;
	int iNumTestsThatHitReverse = 0;

	//***************************************************************************************************

	Vector3 vTestOrigin = vCentroid;
	vTestOrigin.z += fOriginRaiseUpDist;

	for(t=0; t<WALL_TEST_NUM_PROBES; t++)
	{
		Vector3 vStart = vTestOrigin;
		Vector3 vEnd = vTestOrigin + vTestVectors[t];

		vStart.x = Clamp(vStart.x, m_vSectorMins.x, m_vSectorMaxs.x);
		vStart.y = Clamp(vStart.y, m_vSectorMins.y, m_vSectorMaxs.y);
		vStart.z = Clamp(vStart.z, m_vSectorMins.z, m_vSectorMaxs.z);

		vEnd.x = Clamp(vEnd.x, m_vSectorMins.x, m_vSectorMaxs.x);
		vEnd.y = Clamp(vEnd.y, m_vSectorMins.y, m_vSectorMaxs.y);
		vEnd.z = Clamp(vEnd.z, m_vSectorMins.z, m_vSectorMaxs.z);

		bHit = m_Octree.GetRayIntersectPolySide(vStart, vEnd, iSide, NAVGEN_COLLISION_TYPES);

		iNumTestsDone++;

		//*************************************************************************************
		// See if the ray hit the back of a wall.  If normals are pointing in same direction,
		// we hit the back. (Use a minimum dot which ensures that a glancing hit doesn't count?)

		if(bHit && iSide==-1)
		{
			iNumTestsThatHitReverse++;
		}
	}

	//***************************************************************************************************

	float fPercentageHitReverse = ((float)iNumTestsThatHitReverse / (float)iNumTestsDone) * 100.0f;
	if(fPercentageHitReverse > fPercentageOfReverseIntersections)
		return true;

	return false;
}


bool CNavGen::IsPositionNearEdgeOfNavMesh(const Vector3 & vPos, const Vector3 & vMin, const Vector3 & vMax, const float fEps)
{
	return (GetPositionNavMeshEdgeFlags(vPos, vMin, vMax, fEps)!=0);
}

u32 CNavGen::GetPositionNavMeshEdgeFlags(const Vector3 & vPos, const Vector3 & vMin, const Vector3 & vMax, const float fEps)
{
	u32 iEdgeFlags = 0;

	if(Abs(vPos.x - vMin.x) < fEps)
		iEdgeFlags |= EDGEFLAG_NEG_X;
	if(Abs(vPos.x - vMax.x) < fEps)
		iEdgeFlags |= EDGEFLAG_POS_X;
	if(Abs(vPos.y - vMin.y) < fEps)
		iEdgeFlags |= EDGEFLAG_NEG_Y;
	if(Abs(vPos.y - vMax.y) < fEps)
		iEdgeFlags |= EDGEFLAG_POS_Y;

	return iEdgeFlags;
}

void CNavGen::MoveNodesAwayFromWalls()
{
	CCulledGeom geometry;
	Vector3 vTriMin, vTriMax;
	const Vector3 vGeomExtend(1.5f,1.5f,1.5f);
	
	u32 t, s;
	s32 p, n;

	for(t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];

		if(pTri->IsRemoved())
			continue;

		if(pTri->m_Nodes[0]->m_bIsSurroundedByTriangles && pTri->m_Nodes[1]->m_bIsSurroundedByTriangles && pTri->m_Nodes[2]->m_bIsSurroundedByTriangles)
			continue;

		Vector3 vOriginalNormal = pTri->CalcNormal();
		vOriginalNormal.Normalize();

		pTri->m_MinMax.GetAsFloats(vTriMin, vTriMax);
		geometry.Reset();
		geometry.Init(vTriMin-vGeomExtend, vTriMax+vGeomExtend);
		m_Octree.AddTrianglesToCulledGeom(geometry);

		Vector3 vEdgeNormals[3];
		float fEdgePlaneDists[3];
		
		for(p=0; p<3; p++)
		{
			if( !pTri->m_Nodes[p]->m_bIsSurroundedByTriangles || !pTri->m_Nodes[(p+1)%3]->m_bIsSurroundedByTriangles )
			{
				CNavGenNode * pNode1 = pTri->m_Nodes[p];
				CNavGenNode * pNode2 = pTri->m_Nodes[(p+1)%3];

				Vector3 vEdge = pNode2->m_vBasePos - pNode1->m_vBasePos;
				vEdge.Normalize();
				vEdgeNormals[p] = CrossProduct(ZAXIS, vEdge);
				vEdgeNormals[p].Normalize();

				fEdgePlaneDists[p] = - DotProduct(vEdgeNormals[p], pNode1->m_vBasePos);
			}
		}

		//---------------------------------------------------------------------
		// Calculate the closest distance to any obstruction along this edge

		const float fMaxDist = PATHSERVER_PED_RADIUS;
		float fObstructionDists[3] = { fMaxDist, fMaxDist, fMaxDist };

		for(p=0; p<3; p++)
		{
			if( pTri->m_Nodes[p]->m_bIsSurroundedByTriangles && pTri->m_Nodes[(p+1)%3]->m_bIsSurroundedByTriangles )
				continue;

			CNavGenNode * pNode1 = pTri->m_Nodes[p];
			CNavGenNode * pNode2 = pTri->m_Nodes[(p+1)%3];

			const float fTestStep = 0.1f;
			float fTestDistance = fTestStep;

			while(fTestDistance < PATHSERVER_PED_RADIUS)
			{
				Vector3 vert1 = pNode1->m_vBasePos;
				Vector3 vert2 = pNode2->m_vBasePos;
				vert1.z += m_Params.m_HeightAboveNodeBaseAtWhichToTriangulate;
				vert2.z += m_Params.m_HeightAboveNodeBaseAtWhichToTriangulate;

				vert1 += vEdgeNormals[p] * fTestDistance;
				vert2 += vEdgeNormals[p] * fTestDistance;

				Vector3 vHitPos1to2;
				float fHitDist1to2;
				CNavGenTri * pHitTri1to2 = NULL;
				u32 iRetFlags = 0;

				if(geometry.ProcessLineOfSight(vert1, vert2, iRetFlags, NAVGEN_COLLISION_TYPES, vHitPos1to2, fHitDist1to2, pHitTri1to2))
				{
					fObstructionDists[p] = fTestDistance;

					break;
				}

				fTestDistance += fTestStep;
			}
		}

		// Now the tricky part.  We must slide the vertices along the triangle edge vectors
		// so that we move them the required distance from each edge.
		for(p=0; p<3; p++)
		{
			if( pTri->m_Nodes[p]->m_bIsSurroundedByTriangles && pTri->m_Nodes[(p+1)%3]->m_bIsSurroundedByTriangles )
				continue;

			if(fObstructionDists[p] == fMaxDist)
				continue;

			float fPlanarMoveDist = fMaxDist - fObstructionDists[p];
			Assert(fPlanarMoveDist > 0.0f && fPlanarMoveDist <= fMaxDist);

			CNavGenNode * pNode1 = pTri->m_Nodes[p];
			CNavGenNode * pNode2 = pTri->m_Nodes[(p+1)%3];
			CNavGenNode * pOtherNode = pTri->m_Nodes[(p+2)%3];

			// We can only move verts 1 & 2 by the desired amount, if the other vertex
			// if further away than the planar distance (otherwise we'll flip the triangle)
			// NB: Edge planes point outside of the triangle, so we test against the negative distance.

			static dev_float fMinEps = 0.1f;		// prevent any triangle edge from becoming too short
			float fOtherPlaneDist = - (DotProduct(vEdgeNormals[p], pOtherNode->m_vBasePos) + fEdgePlaneDists[p]);
			fPlanarMoveDist = Min(fPlanarMoveDist, fOtherPlaneDist-fMinEps);
			if( fPlanarMoveDist <= 0.0f )
				continue;
			fOtherPlaneDist = -fOtherPlaneDist;



			// Work out the new vertices, slid along the 2 other edge vectors to sit exactly fMaxDist from the obstruction
			Vector3 v1ToOther = pOtherNode->m_vBasePos - pNode1->m_vBasePos;
			Vector3 v2ToOther = pOtherNode->m_vBasePos - pNode2->m_vBasePos;

			const float S = fPlanarMoveDist / (fPlanarMoveDist - (fOtherPlaneDist+fPlanarMoveDist));

			Vector3 vNewVert1 = pNode1->m_vBasePos + (S * v1ToOther);
			Vector3 vNewVert2 = pNode2->m_vBasePos + (S * v2ToOther);

			// Modify node positions, and then determine whether this is a valid move.
			// If not we will revert the node positions once again
			Vector3 vPrevVert1 = pNode1->m_vBasePos;
			Vector3 vPrevVert2 = pNode2->m_vBasePos;

			// Make the proposed changes, provided each node is not at the edge of the mesh
			// TODO: Instead of prohibiting moves to nodes on the edge of the mesh, we should instead
			// restrict the movements to be in the direction of the X or Y world axes.

			if(!IsPositionNearEdgeOfNavMesh(pNode1->m_vBasePos, m_vSectorMins, m_vSectorMaxs, 0.25f))
				pNode1->m_vBasePos = vNewVert1;

			if(!IsPositionNearEdgeOfNavMesh(pNode2->m_vBasePos, m_vSectorMins, m_vSectorMaxs, 0.25f))
				pNode2->m_vBasePos = vNewVert2;

			CNavGenNode * pEdgeNodes[2] = { pNode1, pNode2 };

			bool bRevertChange = false;

			// Check #1 : Paranoia-based second test for whether this move will flip the triangle
			Vector3 vNewNormal = pTri->CalcNormal();
			if(DotProduct(vOriginalNormal, vNewNormal) < 0.1f)
			{
				bRevertChange = true;
			}

			if(!bRevertChange)
			{
				// Check #2 : has this flipped any surrounding triangles ?
				for(n=0; n<2 && !bRevertChange; n++)
				{
					for(s=0; s<pEdgeNodes[n]->m_TrianglesUsingThisNode.size(); s++)
					{
						CNavSurfaceTri * pNodeTri = pEdgeNodes[n]->m_TrianglesUsingThisNode[s];
						if(pNodeTri->IsRemoved())
							continue;

						static const float fMinDot = Cosf(80.0f * DtoR);
						Vector3 vPlaneNormal = pNodeTri->CalcNormal();
						vPlaneNormal.Normalize();
						const float fDot = DotProduct(vPlaneNormal, pNodeTri->m_vNormal);

						if(vPlaneNormal.z < 0.0f || fDot <= fMinDot)
						{
							bRevertChange = true;
							break;
						}
					}
				}
			}

			if(0) //!bRevertChange)
			{
				// Check #3 : nasty; ensure that node N has no attached triangles with open edges whose edge normal is in same direction as node movement
				for(n=0; n<2 && !bRevertChange; n++)
				{
					//Vector3 vDirectionOfMovement = (n==0) ? v1ToOther : v2ToOther;
					//vDirectionOfMovement.z = 0.0f;
					//vDirectionOfMovement.Normalize();

					for(s=0; s<pEdgeNodes[n]->m_TrianglesUsingThisNode.size() && !bRevertChange; s++)
					{
						CNavSurfaceTri * pNodeTri = pEdgeNodes[n]->m_TrianglesUsingThisNode[s];
						if(pNodeTri->IsRemoved())
							continue;
						if(pNodeTri == pTri)
							continue;
						for(s32 ei=0; ei<3; ei++)
						{
							if(!pNodeTri->m_AdjacentTris[ei])
							{
								if(! geometry.TestLineOfSight(pNodeTri->m_Nodes[(ei+1)%3]->m_vBasePos, pNodeTri->m_Nodes[ei]->m_vBasePos,NAVGEN_COLLISION_TYPES) )
								{
									bRevertChange = true;
									break;
								}

								/*
								Vector3 vAdjTriEdge = pNodeTri->m_Nodes[(ei+1)%3]->m_vBasePos - pNodeTri->m_Nodes[ei]->m_vBasePos;
								vAdjTriEdge.z = 0.0f;
								vAdjTriEdge.Normalize();

								SwapEm(vAdjTriEdge.x, vAdjTriEdge.y);
								vAdjTriEdge.y = -vAdjTriEdge.y;

								if( DotProduct(vDirectionOfMovement, vAdjTriEdge) > 0.0f )
								{
									bRevertChange = true;
									break;
								}
								*/
							}
						}
					}
				}
			}

			// Failed : Revert changes
			if(bRevertChange)
			{
				pNode1->m_vBasePos = vPrevVert1;
				pNode2->m_vBasePos = vPrevVert2;

				// TODO: Should we try decreasing the move distances for this edge in steps until we succeed?
				continue;
			}
			// Succeeded : Recalculate the normals of all triangles surrounding these two nodes
			// Flag all triangles which were modified
			else
			{
				for(n=0; n<2; n++)
				{
					for(s=0; s<pEdgeNodes[n]->m_TrianglesUsingThisNode.size(); s++)
					{
						CNavSurfaceTri * pNodeTri = pEdgeNodes[n]->m_TrianglesUsingThisNode[s];
						if(pNodeTri->IsRemoved())
							continue;
						pNodeTri->m_vNormal = pNodeTri->CalcNormal();
						pNodeTri->m_vNormal.Normalize();

						// Flag as modified
						pNodeTri->m_iFlags |= NAVSURFTRI_WAS_MOVED;
					}
				}
			}
		}
	}
}


// NAME : RecordSteepnessUnderTrianglesAfterMovedAwayFromWalls
// PURPOSE : We initially record the steepness when the triangles are created;
// However if any triangle is modified by the MoveNodesAwayFromWalls() logic, then
// we should re-establish their steepness - since that process may mean that they
// should no longer be classified as steep.
void CNavGen::RecordSteepnessUnderMovedTriangles()
{
	Vector3 vHitPos;
	float fHitDist;
	CNavGenTri * pHitTri;
	u32 iRetFlags = 0;
	Vector3 vRaise(0.0f, 0.0f, m_Params.m_HeightAboveNodeBaseAtWhichToTriangulate);
	static const float fHeightTestIncrement = 0.125f;

	u32 t,n,lastn;

	// save
	const bool bOldJitter = m_bJitterTestsUnderLine;
	m_bJitterTestsUnderLine = false;

	for(t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];

		if(pTri->IsRemoved())
			continue;

		if((pTri->m_iFlags & NAVSURFTRI_WAS_MOVED)==0)
			continue;
		if(!pTri->m_bIsTooSteep)
			continue;

		// Initially turn off steepness
		pTri->m_bIsTooSteep = false;

		s32 iNumSteep = 0;

		// See if all 3 nodes are on steep triangles..
		for(n=0; n<3; n++)
		{
			if(m_Octree.ProcessLineOfSight(pTri->m_Nodes[n]->m_vBasePos + ZAXIS, pTri->m_Nodes[n]->m_vBasePos - ZAXIS, iRetFlags, NAVGEN_COLLISION_TYPES, vHitPos, fHitDist, pHitTri, true, NAVGENTRI_WALKABLE))
			{
				if(pHitTri && (pHitTri->m_iFlags & NAVGENTRI_TOOSTEEP)!=0)
				{
					iNumSteep++;
				}
			}
		}
		// ..if so we reclassify as steep, and move onto the next one
		if(iNumSteep == 3)
		{
			pTri->m_bIsTooSteep = true;
			continue;
		}

		// Perform test along triangle edges to detect steepness
		lastn = 2;
		for(n=0; n<3; n++)
		{
			bool bSteep = false;

			// This is the same test as performed during triangulation & elsewhere, but this time we don't care about the height change
			// We pass in a sentinel value (100.0f) which will never be reached
			IsLineFreeOfSuddenHeightChanges(m_Octree, pTri->m_Nodes[lastn]->m_vBasePos + vRaise, pTri->m_Nodes[n]->m_vBasePos + vRaise, pTri->m_Nodes[lastn], pTri->m_Nodes[n], NAVGEN_COLLISION_TYPES, bSteep, fHeightTestIncrement, 100.0f, false, false, false, false);

			if(bSteep)
			{
				pTri->m_bIsTooSteep = true;
				break;
			}

			lastn = n;
		}
	}

	// restore
	m_bJitterTestsUnderLine = bOldJitter;
}

// This function triangulates path nodes into a surface
void CNavGen::CleanUpDataAfterGeneratingNavMesh(void)
{
	if(m_pNodeMap)
	{
		delete m_pNodeMap;
		m_pNodeMap = NULL;
	}
}


//*****************************************************************************************************
//	Make an attempt to remove water triangles which not accessible.
//	Works in a similar way to IsTriangleBNehindWall().  For 'sheltered' water polys, we cast a ray up
//	and if we hit the backface of a collision poly, then this water poly is likely to inaccessible.
//	Of course this fails if anyone has modelled the underside surface of a piece of shoreline, etc.
//	But anyway, in the general case it should remove a lot of triangles.

void
CNavGen::CullWaterTrianglesNotAcessible()
{
	Vector3 vOffset(0.0f, 0.0f, TRI_IS_SHELTERED_DIST);

	u32 t;
	for(t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];
		if(pTri->IsRemoved() || !pTri->m_bIsWater)
			continue;

		Vector3 vVerts[3] = { pTri->m_Nodes[0]->m_vBasePos, pTri->m_Nodes[1]->m_vBasePos, pTri->m_Nodes[2]->m_vBasePos };
		vVerts[0].z += 0.1f;
		vVerts[1].z += 0.1f;
		vVerts[2].z += 0.1f;

		Vector3 vCentroid = pTri->Centroid();
		vCentroid.z += 0.1f;

		int iSide=0;
		bool bHit;
		
		bHit = m_Octree.GetRayIntersectPolySide(vCentroid, vCentroid+vOffset, iSide, NAVGEN_COLLISION_TYPES);
		if(!bHit || iSide!=-1) continue;
		bHit = m_Octree.GetRayIntersectPolySide(vVerts[0], vVerts[0]+vOffset, iSide, NAVGEN_COLLISION_TYPES);
		if(!bHit || iSide!=-1) continue;
		bHit = m_Octree.GetRayIntersectPolySide(vVerts[1], vVerts[1]+vOffset, iSide, NAVGEN_COLLISION_TYPES);
		if(!bHit || iSide!=-1) continue;
		bHit = m_Octree.GetRayIntersectPolySide(vVerts[2], vVerts[2]+vOffset, iSide, NAVGEN_COLLISION_TYPES);
		if(!bHit || iSide!=-1) continue;

		pTri->SetRemoved();

		RemoveTriFromTriList(pTri->m_Nodes[0]->m_TrianglesUsingThisNode, pTri);
		RemoveTriFromTriList(pTri->m_Nodes[1]->m_TrianglesUsingThisNode, pTri);
		RemoveTriFromTriList(pTri->m_Nodes[2]->m_TrianglesUsingThisNode, pTri);
	}
}

// Fix for artifacts caused by the axis aligned sampling grid.
// For every external edge of the navmesh (this is, a polygon edge which has no adjacent poly) -
// idenfity the next and prev edges sharing that node, and attempt to triangulate.


void CNavGen::FixJaggiesAfterTriangulation()
{
	u32 t;

	m_iFixJaggiesNumTrianglesCreated = 0;
	int iNumPasses = 10;

	while(iNumPasses)
	{
		int iNumTrisThisPass = 0;

		vector<CNavSurfaceTri*> triangles;

		// Make a list of all triangles which have at least one open edge
		for(t=0; t<m_NavSurfaceTriangles.size(); t++)
		{
			CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];
			if(pTri->IsRemoved())
				continue;
			if(!pTri->m_AdjacentTris[0] || !pTri->m_AdjacentTris[1] || !pTri->m_AdjacentTris[2])
				triangles.push_back(pTri);
		}

		while(triangles.size())
		{
			CNavSurfaceTri * pTri = triangles[triangles.size()-1];
			triangles.pop_back();

			for(int e=0; e<3; e++)
			{
				if(!pTri->m_AdjacentTris[e])
				{
					CNavGenNode * edgeNodes[2] = { pTri->m_Nodes[e], pTri->m_Nodes[(e+1)%3] };
					CNavGenNode * pOtherNode = NULL;
					CNavSurfaceTri * pOtherTri = NULL;
					int iOtherEdge;

					if( FindTriangulateEdge(e, edgeNodes[0], edgeNodes[1], pTri, &pOtherNode, &pOtherTri, &iOtherEdge) )
					{
						iNumTrisThisPass++;
					}
				}
			}
		}

		m_iFixJaggiesNumTrianglesCreated += iNumTrisThisPass;

		if(!iNumTrisThisPass)
			break;

		iNumPasses--;
	}
}


bool CNavGen::FindTriangulateEdge(int iInputTriEdge, CNavGenNode * pNode1, CNavGenNode * pNode2, CNavSurfaceTri * pInputTri, CNavGenNode ** ppOutNode, CNavSurfaceTri ** ppOutTri, int * pOutEdge)
{
	static dev_bool bPerformCrossingsTest = true;
	static dev_bool bPerformVolumeTest = true;
	static dev_bool bPerformHeightChangeTest = true;
	static dev_bool bPerformCollisionProximityTest = true;
	static dev_bool bCheckForExistingTriangleEdges = true;

	// This is the height above each node at which to generate the collision tris.
	const float fHeightAboveNode = m_Params.m_HeightAboveNodeBaseAtWhichToTriangulate;
	const float fHeightOfVolumeTests = m_Params.m_fTestClearHeightInTriangulation;
	// This is the spacing along the line at which to test the height-change
	static const float fHeightTestIncrement = 0.125f;
	// This is the maximum height-change for normal surfaces.  NB : If start & end nodes are on stairs, this value could be more ?
	const float fDefaultMaxHeightChangeValue = m_Params.m_MaxHeightChangeUnderTriangleEdge;

	static const float fStandardPedRadius = fwNavGenConfig::Get().m_PedRadius;

	CNavGenNode * inputNodes[] = { pNode1, pNode2 };

	int n=0;

	{
		CNavGenNode * pInputNode = inputNodes[n];

		Vector3 vThisEdge = pInputNode->m_vBasePos - inputNodes[!n]->m_vBasePos;
		vThisEdge.z = 0.0f;
		vThisEdge.Normalize();

		for(u32 t=0; t<pInputNode->m_TrianglesUsingThisNode.size(); t++)
		{
			CNavSurfaceTri * pOtherTri = pInputNode->m_TrianglesUsingThisNode[t];
			if(pOtherTri->IsRemoved())
				continue;
			if(pOtherTri==pInputTri)
				continue;

			// Find an adjacent edge on the other triangle which is empty, and which shares pInputNode
			for(int a=0; a<3; a++)
			{
				if( pOtherTri->m_AdjacentTris[a]==NULL && pOtherTri->m_Nodes[(a+1)%3] == pInputNode )
				{
					CNavGenNode * pOtherNode = pOtherTri->m_Nodes[a];

					//-----------------------------------------------------------------------
					// Ensure this isn't the opposite side of the same edge that was input!

					if(pOtherNode != inputNodes[!n])
					{

#if __DEV
						static bool bBreak = false;
						static Vector3 vTestVec1(118.0f, -2801.0f, 0.0f);
						static Vector3 vTestVec2(118.0f, -2800.0f, 0.0f);
						static Vector3 vTestVec3(119.0f, -2800.0f, 0.0f);
						bool bEdgeMatched =
							(pNode1->m_vBasePos.IsClose(vTestVec1, 0.25f) && pNode2->m_vBasePos.IsClose(vTestVec2, 0.25f)) ||
							(pNode1->m_vBasePos.IsClose(vTestVec2, 0.25f) && pNode2->m_vBasePos.IsClose(vTestVec1, 0.25f));
						if(bEdgeMatched && pOtherNode->m_vBasePos.IsClose(vTestVec3, 0.25f))
							bBreak = true;
#endif


						Vector3 vOtherEdge = pOtherNode->m_vBasePos - pInputNode->m_vBasePos;
						const float fOtherEdgeLengthSqr = vOtherEdge.Mag2();

						//--------------------------------------
						// Ensure edge length is not too great

						if(fOtherEdgeLengthSqr > 20.0f*20.0f)
							continue;

						vOtherEdge.z = 0.0f;
						vOtherEdge.Normalize();

						//----------------------------------------------------------------------------------------------
						// Ensure that this is a valid combination of edges, resulting in an interior angle of <= 180

						Vector3 vCross = CrossProduct(vOtherEdge, vThisEdge);
						if(vCross.z > 0.0f)
						{
							// The new edge is defined by (pTriNodes[1] to pTriNodes[2])
							CNavGenNode * pTriNodes[3] = 
							{
								pInputNode, pOtherNode, inputNodes[!n]
							};

							// We can't add a triangle across a water boundary
							int iNumWater=0;
							for(int w=0; w<3; w++)
								if(pTriNodes[w]->m_bIsWater)
									iNumWater++;

							if(iNumWater != 0 && iNumWater != 3)
								continue;


							//--------------------------------------------------------------------
							// Test that the area of a resultant triangle will be > some minimum

							const float fArea = CalcTriArea(pTriNodes[0]->m_vBasePos, pTriNodes[1]->m_vBasePos, pTriNodes[2]->m_vBasePos);
							if(fArea < 0.1f)
								continue;

							// Construct edge planes
							Vector3 vTriPlanes2d[3];
							for(int i=0; i<3; i++)
							{
								vTriPlanes2d[i] = pTriNodes[(i+1)%3]->m_vBasePos - pTriNodes[i]->m_vBasePos;
								SwapEm(vTriPlanes2d[i].x, vTriPlanes2d[i].y);
								vTriPlanes2d[i].x = -vTriPlanes2d[i].x;
								vTriPlanes2d[i].z = 0.0f;
								vTriPlanes2d[i].Normalize();
								const float d = - DotProduct(vTriPlanes2d[i], pTriNodes[i]->m_vBasePos);
								vTriPlanes2d[i].z = d;
							}

							//-----------------------------------------------------------------------------------------------
							// Ensure the the height of the triangle from the new edge is greater than some threshold value.
							// This prevents us creating multiple sliver triangles which may not be optimised away.

							const float fDistVert =
								(pTriNodes[0]->m_vBasePos.x * vTriPlanes2d[1].x) + (pTriNodes[0]->m_vBasePos.y * vTriPlanes2d[1].y) + vTriPlanes2d[1].z;
							if(Abs(fDistVert) < 0.1f)
								continue;

							//------------------------------------------------------------------------------------------
							// Test all surrounding triangles to ensure that the new edge will not cross any of them

							if(bPerformCrossingsTest)
							{
								bool bNoCrossings = true;

								for(int m=0; m<3; m++)
								{
									for(u32 s=0; (s<pTriNodes[m]->m_TrianglesUsingThisNode.size()) && bNoCrossings; s++)
									{
										CNavSurfaceTri * pTestTri = pTriNodes[m]->m_TrianglesUsingThisNode[s];

										int iNumNodesSame = 0;
										for(int n1=0; n1<3; n1++)
										{
											if(pTestTri->m_Nodes[n1] == pTriNodes[0] ||
												pTestTri->m_Nodes[n1] == pTriNodes[1] ||
												 pTestTri->m_Nodes[n1] == pTriNodes[2])
												 iNumNodesSame++;
										}
										if(iNumNodesSame >= 3)
										{
											bNoCrossings = false;
											break;
										}

										Vector3 vNode1( inputNodes[!n]->m_vBasePos );
										vNode1.z = 0.0f;
										Vector3 vNode2( pOtherNode->m_vBasePos );
										vNode2.z = 0.0f;
										Vector2 v2dNode1( inputNodes[!n]->m_vBasePos, Vector2::kXY );
										Vector2 v2dNode2( pOtherNode->m_vBasePos, Vector2::kXY );

										for(int e=0; e<3; e++)
										{
											Vector3 vEdge1( pTestTri->m_Nodes[e]->m_vBasePos );
											vEdge1.z = 0.0f;
											Vector3 vEdge2( pTestTri->m_Nodes[(e+1)%3]->m_vBasePos );
											vEdge2.z = 0.0f;
											Vector2 v2dEdge1( pTestTri->m_Nodes[e]->m_vBasePos, Vector2::kXY);
											Vector2 v2dEdge2( pTestTri->m_Nodes[(e+1)%3]->m_vBasePos, Vector2::kXY);

											float t1,t2;
											bool bIntersects = geom2D::Test2DLineVsLine(
												t1, t2,
												v2dEdge1, v2dEdge2,
												v2dNode1, v2dNode2);

											if( bIntersects && t1 > 0.0f && t1 < 1.0f && t2 > 0.0f && t2 < 1.0f )
											{
												bNoCrossings = false;
												break;
											}

											// Need to solve the final case here, where we have an edge exactly along an edge
											// but contained within the length of it..

											//-----------------------------------------------------------------------------
											// Ensure that other than the 3 nodes which are the new triangle vertices, no
											// surrounding polygon's vertices are within the new triangle OR are contained
											// within the new triangle.

											const float fEps = -0.05f;

											if(pTestTri->m_Nodes[e] != pTriNodes[0] && pTestTri->m_Nodes[e] != pTriNodes[1] && pTestTri->m_Nodes[e] != pTriNodes[2])
											{
												int pl;
												for(pl=0; pl<3; pl++)
												{
													const Vector3 & vec = pTestTri->m_Nodes[e]->m_vBasePos;
													const float fPlaneDist = (vec.x * vTriPlanes2d[pl].x) + (vec.y * vTriPlanes2d[pl].y) + vTriPlanes2d[pl].z;
													if(fPlaneDist < fEps)
														break;
												}
												// Point was ON or INSIDE all 3 triangle edge planes
												if(pl == 3)
												{
													bNoCrossings = false;
												}
											}
										}
									}
								}

								if(!bNoCrossings)
									continue;
							}

							Vector3 vPts[3] = { pTriNodes[0]->m_vBasePos, pTriNodes[1]->m_vBasePos, pTriNodes[2]->m_vBasePos };

							//-----------------------------------------------------------------------------------------------------
							// Perform a volume test above the candidate triangle, to ensure this doesn't intersect any collision

							if(bPerformVolumeTest)
							{
								CClipVolume vol;
								Vector3 vExtrudeNormal = ZAXIS;
								vPts[0].z += fHeightAboveNode;
								vPts[1].z += fHeightAboveNode;
								vPts[2].z += fHeightAboveNode;
								vol.InitExtrudedTri(vPts, fHeightOfVolumeTests - fHeightAboveNode, &vExtrudeNormal);

								if(!m_Octree.TestVolumeIsClear(vol, NAVGEN_COLLISION_TYPES))
									continue;
							}

							//------------------------------------------------------------------------------------
							// Perform a 'height change' test under the new edge to ensure this is a valid edge

							bool bTooSteep[3] = { false, false, false };

							if(bPerformHeightChangeTest)
							{
								bool bOldJitter = m_bJitterTestsUnderLine;
								m_bJitterTestsUnderLine = false;

								int e;
								for(e=0; e<3; e++)
								{
									if( !IsLineFreeOfSuddenHeightChanges(m_Octree, vPts[e], vPts[(e+1)%3], NULL, NULL, NAVGEN_COLLISION_TYPES, bTooSteep[e], fHeightTestIncrement, fDefaultMaxHeightChangeValue, false, true, (iNumWater==0), (iNumWater==3) ))
										break;
								}

								m_bJitterTestsUnderLine = bOldJitter;

								if(e != 3)
									continue;
							}

							//---------------------------------------------------------------------------------------------
							// Ensure that the new candidate edge is no closer than PED_RADIUS to any collision geometry

							if(bPerformCollisionProximityTest)
							{
								Vector3 vEdge1 = pOtherNode->m_vBasePos;
								Vector3 vEdge2 = inputNodes[!n]->m_vBasePos;
								vEdge1.z += fHeightAboveNode;
								vEdge2.z += fHeightAboveNode;

								Vector3 vEdge = vEdge2 - vEdge1;
								vEdge.Normalize();
								Vector3 vTriNormal, vEdgeNormal;
								CalcTriNormal(vPts, vTriNormal);

								// Resultant triangle must be facing upwards
								if(vTriNormal.z < 0.0f)
									continue;

								//vEdgeNormal = CrossProduct(vTriNormal, vEdge);
								vEdgeNormal.x = - vEdge.y;
								vEdgeNormal.y = vEdge.x;
								vEdgeNormal.z = 0.0f;
								vEdgeNormal.Normalize();

								Vector3 vEdge1Extend = vEdge1 + (vEdgeNormal * fStandardPedRadius);
								Vector3 vEdge2Extend = vEdge2 + (vEdgeNormal * fStandardPedRadius);

								if(!m_Octree.TestLineOfSight(vEdge1Extend, vEdge2Extend, NAVGEN_COLLISION_TYPES))
									continue;

//								if(!m_Octree.TestLineOfSight(vEdge1, vEdge1Extend) ||
//									!m_Octree.TestLineOfSight(vEdge2, vEdge2Extend)
//									continue;
							}

							if(bCheckForExistingTriangleEdges)
							{
								bool bEdgeExists = false;
								for(s32 n1=0; n1<3; n1++)
								{
									CNavGenNode * pTestNode1 = pTriNodes[n1];
									CNavGenNode * pTestNode2 = pTriNodes[(n1+1)%3];

									u32 et;
									s32 iNumExistingEdges = 0;
									for(et=0; et<m_NavSurfaceTriangles.size(); et++)
									{
										CNavSurfaceTri * pExistingTri = m_NavSurfaceTriangles[et];
										s32 iSharedNodes=0;
										if(pExistingTri->m_Nodes[0]==pTestNode1 || pExistingTri->m_Nodes[1]==pTestNode1 || pExistingTri->m_Nodes[2]==pTestNode1)
											iSharedNodes++;
										if(pExistingTri->m_Nodes[0]==pTestNode2 || pExistingTri->m_Nodes[1]==pTestNode2 || pExistingTri->m_Nodes[2]==pTestNode2)
											iSharedNodes++;

										if(iSharedNodes == 2)
											iNumExistingEdges++;
									}
									if(iNumExistingEdges > 1)
									{
										Assertf(false, "Found existing edge!");
										bEdgeExists = true;
										break;
									}
								}
								if(bEdgeExists)
									continue;
							}

							//-------------------------------------------------------------
							// Ok, it looks like this candidate triangle can be created

							CNavSurfaceTri * pNavTri = rage_new CNavSurfaceTri();
							pNavTri->m_bCreatedInFixJaggies = true;

							bool bWater = pInputTri->m_bIsWater || pOtherTri->m_bIsWater;

							SwapEm(pTriNodes[0], pTriNodes[2]);
							InitNavSurfaceTri(pNavTri, pTriNodes, bWater, false);
							pNavTri->InitMinMax();

							if(bTooSteep[0] || bTooSteep[1] || bTooSteep[2])
							{
								pNavTri->m_bIsTooSteep = true;
							}

							// Add the triangle to the list
							m_NavSurfaceTriangles.push_back(pNavTri);

							// Add associations, which will be used later in optimising the mesh
							AssociateTriAndNode(pNavTri, pTriNodes[0]);
							AssociateTriAndNode(pNavTri, pTriNodes[1]);
							AssociateTriAndNode(pNavTri, pTriNodes[2]);

							pOtherTri->m_AdjacentTris[a] = pNavTri;
							pInputTri->m_AdjacentTris[iInputTriEdge] = pNavTri;

							pNavTri->m_AdjacentTris[0] = pOtherTri;
							pNavTri->m_AdjacentTris[1] = NULL;
							pNavTri->m_AdjacentTris[2] = pInputTri;


							*ppOutNode = pOtherNode;
							*ppOutTri = pOtherTri;
							*pOutEdge = a;

							return true;
						}
					}
				}
			}
		}
	}

	return false;
}

bool GetIsCollisionTriPavement(CNavGenTri * pTri) { return pTri->m_colPolyData.GetIsPavement(); }
void SetIsNavTriPavement(CNavSurfaceTri * pTri) { pTri->m_colPolyData.SetIsPavement(true); }

bool CNavGen::MakeSureNodeMoveDoesntFlipAnyTriangles(CNavGenNode * pNode, const Vector3 & vNewPosition)
{
	Vector3 verts[3];
	Vector3 vPlaneNormal;
	float fPlaneDist;
	float fDot;
	Vector3 vUpVector(0, 0, 1.0f);

	for(u32 t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];

		if(pTri->IsRemoved())
			continue;

		// Find which vertex is the node to move
		if(pTri->m_Nodes[0] == pNode)
		{
			verts[0] = vNewPosition;
			verts[1] = pTri->m_Nodes[1]->m_vBasePos;
			verts[2] = pTri->m_Nodes[2]->m_vBasePos;
		}
		else if(pTri->m_Nodes[1] == pNode)
		{
			verts[0] = pTri->m_Nodes[0]->m_vBasePos;
			verts[1] = vNewPosition;
			verts[2] = pTri->m_Nodes[2]->m_vBasePos;
		}
		else if(pTri->m_Nodes[2] == pNode)
		{
			verts[0] = pTri->m_Nodes[0]->m_vBasePos;
			verts[1] = pTri->m_Nodes[1]->m_vBasePos;
			verts[2] = vNewPosition;
		}
		else
		{
			continue;
		}

		PlaneFromPoints(verts, &vPlaneNormal, fPlaneDist);

		fDot = DotProduct(vPlaneNormal, vUpVector);
		if(fDot <= 0.0f)
		{
			return false;
		}
	}

	return true;
}


void CNavGen::MoveVertsToSmoothEdges( bool (HasSurfaceProperty)(CNavSurfaceTri *), bool (CollisionHasSurfaceProperty)(CNavGenTri *) )
{
	int iNumChanged;

	do 
	{
		iNumChanged = 0;

		size_t t;
		const size_t iInitialNumTris = m_NavSurfaceTriangles.size();
		for(t = 0; t < iInitialNumTris; t++)
		{
			CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];
			if(pTri->IsRemoved())
				continue;

			if(MoveVertsToSmoothEdges(pTri, HasSurfaceProperty, CollisionHasSurfaceProperty))
			{
				iNumChanged++;
			}
		}

	} while (iNumChanged > 0);
}

bool CNavGen::MoveVertsToSmoothEdges(CNavSurfaceTri * pPavementTri, bool (HasSurfaceProperty)(CNavSurfaceTri *), bool (CollisionHasSurfaceProperty)(CNavGenTri *))
{
	const float fSmallestMoveSqr = 0.1f*0.1f;
	bool bMovedAnyNodes = false;

	if( HasSurfaceProperty(pPavementTri) )
	{
		for(int e1=0; e1<3; e1++)
		{
			// Find an adjacent triangle which is not pavemnt
			if(pPavementTri->m_AdjacentTris[e1] && ! HasSurfaceProperty(pPavementTri->m_AdjacentTris[e1]) )
			{
				CNavSurfaceTri * pNonPavementTri = pPavementTri->m_AdjacentTris[e1];

				CNavGenNode * triSharedNodes[2] = { NULL, NULL };
				int iNumShared = GetSharedNodes(pPavementTri, pNonPavementTri, &triSharedNodes[0], &triSharedNodes[1], NULL);
				Assert(iNumShared==2);

				if(iNumShared == 2)
				{
					for(int n=0; n<2; n++)
					{
						CNavGenNode * pNode = triSharedNodes[n];
						for(u32 t=0; t<pNode->m_TrianglesUsingThisNode.size(); t++)
						{
							CNavSurfaceTri * pOppositePavementTri = pNode->m_TrianglesUsingThisNode[t];
							if(pOppositePavementTri==pPavementTri || pOppositePavementTri==pNonPavementTri || !HasSurfaceProperty(pOppositePavementTri))
								continue;

							for(int e2=0; e2<3; e2++)
							{
								if(pOppositePavementTri->m_AdjacentTris[e2] && !HasSurfaceProperty(pOppositePavementTri->m_AdjacentTris[e2])
									&& pOppositePavementTri != pPavementTri && pOppositePavementTri != pNonPavementTri)
								{
									CNavSurfaceTri * pOppositeNonPavementTri = pOppositePavementTri->m_AdjacentTris[e2];

									CNavGenNode * oppositeTriSharedNodes[2] = { NULL, NULL };
									int iOppositeNumShared = GetSharedNodes(pOppositePavementTri, pOppositeNonPavementTri, &oppositeTriSharedNodes[0], &oppositeTriSharedNodes[1], NULL);
									Assert(iOppositeNumShared==2);

									if(iOppositeNumShared == 2)
									{
										// Okay...
										// See if the two non-pavement triangles have a shared edge
										CNavGenNode * nonPavementSharedNodes[2] = { NULL, NULL };
										int iNumNonPavementShared = GetSharedNodes(pNonPavementTri, pOppositeNonPavementTri, &nonPavementSharedNodes[0], &nonPavementSharedNodes[1], NULL);
										if(iNumNonPavementShared == 2)
										{
											// I think we can now be sure that this satisfies our requirements
											// we should have a single node which is shared between pPavementTri & pOppositeNonPavementTri

											CNavGenNode * pPivotSharedNode = NULL;
											int iNumPivotShared = GetSharedNodes(pPavementTri, pOppositeNonPavementTri, &pPivotSharedNode, NULL, NULL);
											if(iNumPivotShared == 1 && (pPivotSharedNode->m_iNumTimesMovedToSmoothEdges < MAX_SMOOTHING_ITERS_ON_NAVGENNODE))
											{
												Assert(pPivotSharedNode == triSharedNodes[0] || pPivotSharedNode == triSharedNodes[1]);
												Assert(pPivotSharedNode == nonPavementSharedNodes[0] || pPivotSharedNode == nonPavementSharedNodes[1]);

												CNavGenNode * pEdgeNode1 = (pPivotSharedNode == triSharedNodes[0]) ? triSharedNodes[1] : triSharedNodes[0];
												CNavGenNode * pEdgeNode2 = (pPivotSharedNode == oppositeTriSharedNodes[0]) ? oppositeTriSharedNodes[1] : oppositeTriSharedNodes[0];

												// We want to move the pivot node to exactly between the two edge nodes

												const Vector3 vNewBasePos = (pEdgeNode1->m_vBasePos + pEdgeNode2->m_vBasePos) * 0.5f;

												if((vNewBasePos - pPivotSharedNode->m_vBasePos).XYMag2() > fSmallestMoveSqr)
												{
													Vector3 vHitPos;
													float fHitDist;
													CNavGenTri * pHitPoly = NULL;
													u32 iRetFlags = 0;

													if( m_Octree.ProcessLineOfSight(vNewBasePos + ZAXIS, vNewBasePos - ZAXIS, iRetFlags, NAVGEN_COLLISION_TYPES, vHitPos, fHitDist, pHitPoly, true) && pHitPoly)
													{
														if(CollisionHasSurfaceProperty(pHitPoly))
														{
															if(MakeSureNodeMoveDoesntFlipAnyTriangles(pPivotSharedNode, vNewBasePos))
															{
																pPivotSharedNode->m_vBasePos = vNewBasePos;
																bMovedAnyNodes = true;

																pPivotSharedNode->m_iNumTimesMovedToSmoothEdges++;
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	return bMovedAnyNodes;
}


//-------------------------------------------------------------------

void CNavGen::MoveVertsToSmoothPavementEdges()
{
	int iNumChanged;

	do 
	{
		iNumChanged = 0;

		//OutputDebugString("\n Next iteration..\n");

		size_t t;
		const size_t iInitialNumTris = m_NavSurfaceTriangles.size();
		for(t=0; t<iInitialNumTris; t++)
		{
			CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];
			if(pTri->IsRemoved())
				continue;

			if(MoveVertsToSmoothPavementEdges(pTri))
			{
				iNumChanged++;
			}
		}

	} while (iNumChanged > 0);
}

bool CNavGen::MoveVertsToSmoothPavementEdges(CNavSurfaceTri * pPavementTri)
{
	const float fSmallestMoveSqr = 0.1f*0.1f;
	bool bMovedAnyNodes = false;

	if(pPavementTri->m_colPolyData.GetIsPavement())
	{
		for(int e1=0; e1<3; e1++)
		{
			// Find an adjacent triangle which is not pavemnt
			if(pPavementTri->m_AdjacentTris[e1] && !pPavementTri->m_AdjacentTris[e1]->m_colPolyData.GetIsPavement())
			{
				CNavSurfaceTri * pNonPavementTri = pPavementTri->m_AdjacentTris[e1];

				CNavGenNode * triSharedNodes[2] = { NULL, NULL };
				int iNumShared = GetSharedNodes(pPavementTri, pNonPavementTri, &triSharedNodes[0], &triSharedNodes[1], NULL);
				Assert(iNumShared==2);

				if(iNumShared == 2)
				{
					for(int n=0; n<2; n++)
					{
						CNavGenNode * pNode = triSharedNodes[n];
						for(u32 t=0; t<pNode->m_TrianglesUsingThisNode.size(); t++)
						{
							CNavSurfaceTri * pOppositePavementTri = pNode->m_TrianglesUsingThisNode[t];
							if(pOppositePavementTri==pPavementTri || pOppositePavementTri==pNonPavementTri || !pOppositePavementTri->m_colPolyData.GetIsPavement())
								continue;

							for(int e2=0; e2<3; e2++)
							{
								if(pOppositePavementTri->m_AdjacentTris[e2] && !pOppositePavementTri->m_AdjacentTris[e2]->m_colPolyData.GetIsPavement()
									&& pOppositePavementTri != pPavementTri && pOppositePavementTri != pNonPavementTri)
								{
									CNavSurfaceTri * pOppositeNonPavementTri = pOppositePavementTri->m_AdjacentTris[e2];

									CNavGenNode * oppositeTriSharedNodes[2] = { NULL, NULL };
									int iOppositeNumShared = GetSharedNodes(pOppositePavementTri, pOppositeNonPavementTri, &oppositeTriSharedNodes[0], &oppositeTriSharedNodes[1], NULL);
									Assert(iOppositeNumShared==2);

									if(iOppositeNumShared == 2)
									{
										// Okay...
										// See if the two non-pavement triangles have a shared edge
										CNavGenNode * nonPavementSharedNodes[2] = { NULL, NULL };
										int iNumNonPavementShared = GetSharedNodes(pNonPavementTri, pOppositeNonPavementTri, &nonPavementSharedNodes[0], &nonPavementSharedNodes[1], NULL);
										if(iNumNonPavementShared == 2)
										{
											// I think we can now be sure that this satisfies our requirements
											// we should have a single node which is shared between pPavementTri & pOppositeNonPavementTri

											CNavGenNode * pPivotSharedNode = NULL;
											int iNumPivotShared = GetSharedNodes(pPavementTri, pOppositeNonPavementTri, &pPivotSharedNode, NULL, NULL);
											if(iNumPivotShared == 1 && (pPivotSharedNode->m_iNumTimesMovedToSmoothEdges < MAX_SMOOTHING_ITERS_ON_NAVGENNODE))
											{
												Assert(pPivotSharedNode == triSharedNodes[0] || pPivotSharedNode == triSharedNodes[1]);
												Assert(pPivotSharedNode == nonPavementSharedNodes[0] || pPivotSharedNode == nonPavementSharedNodes[1]);

												CNavGenNode * pEdgeNode1 = (pPivotSharedNode == triSharedNodes[0]) ? triSharedNodes[1] : triSharedNodes[0];
												CNavGenNode * pEdgeNode2 = (pPivotSharedNode == oppositeTriSharedNodes[0]) ? oppositeTriSharedNodes[1] : oppositeTriSharedNodes[0];

												// We want to move the pivot node to exactly between the two edge nodes

												const Vector3 vNewBasePos = (pEdgeNode1->m_vBasePos + pEdgeNode2->m_vBasePos) * 0.5f;

												if((vNewBasePos - pPivotSharedNode->m_vBasePos).XYMag2() > fSmallestMoveSqr)
												{
													Vector3 vHitPos;
													float fHitDist;
													CNavGenTri * pHitPoly = NULL;
													u32 iRetFlags = 0;

													if( m_Octree.ProcessLineOfSight(vNewBasePos + ZAXIS, vNewBasePos - ZAXIS, iRetFlags, NAVGEN_COLLISION_TYPES, vHitPos, fHitDist, pHitPoly, true) && pHitPoly)
													{
														if(pHitPoly->m_colPolyData.GetIsPavement())
														{
															if(MakeSureNodeMoveDoesntFlipAnyTriangles(pPivotSharedNode, vNewBasePos))
															{
																pPivotSharedNode->m_vBasePos = vNewBasePos;
																bMovedAnyNodes = true;

																pPivotSharedNode->m_iNumTimesMovedToSmoothEdges++;
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	return bMovedAnyNodes;
}


void CNavGen::MoveVertsToSmoothJaggies()
{
	/*
	int iNumChanged;

	do 
	{
		iNumChanged = 0;

		//OutputDebugString("\n Next iteration..\n");

		int t;
		const int iInitialNumTris = m_NavSurfaceTriangles.size();
		for(t=0; t<iInitialNumTris; t++)
		{
			CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];
			if(pTri->IsRemoved())
				continue;

			if(MoveVertsToSmoothJaggies(pTri))
			{
				iNumChanged++;
			}
		}

	} while (iNumChanged > 0);
	*/
}

bool CNavGen::MoveVertsToSmoothJaggies(CNavSurfaceTri * UNUSED_PARAM(pInputTri))
{
	/*
	const float fSmallestMoveSqr = 0.1f*0.1f;
	bool bMovedAnyNodes = false;

	for(int e1=0; e1<3; e1++)
	{
		// Find an adjacency, which is followed by an empty edge
		if(pInputTri->m_AdjacentTris[e1] && !pInputTri->m_AdjacentTris[(e1+1)%3])
		{
			CNavSurfaceTri * pOtherTri = pInputTri->m_AdjacentTris[e1];

			for(int e2=0; e2<3; e2++)
			{
				if(pOtherTri->m_AdjacentTris[e2] == 
			}


			CNavGenNode * triSharedNodes[2] = { NULL, NULL };
			int iNumShared = GetSharedNodes(pInputTri, pOtherTri, &triSharedNodes[0], &triSharedNodes[1], NULL);
			Assert(iNumShared==2);

			if(iNumShared == 2)
			{
				for(int n=0; n<2; n++)
				{
					CNavGenNode * pNode = triSharedNodes[n];
					for(u32 t=0; t<pNode->m_TrianglesUsingThisNode.size(); t++)
					{
						CNavSurfaceTri * pOppositePavementTri = pNode->m_TrianglesUsingThisNode[t];
						if(pOppositePavementTri==pInputTri || pOppositePavementTri==pOtherTri || !pOppositePavementTri->m_colPolyData.GetIsPavement())
							continue;

						for(int e2=0; e2<3; e2++)
						{
							if(pOppositePavementTri->m_AdjacentTris[e2] && !pOppositePavementTri->m_AdjacentTris[e2]->m_colPolyData.GetIsPavement()
								&& pOppositePavementTri != pInputTri && pOppositePavementTri != pOtherTri)
							{
								CNavSurfaceTri * pOppositeNonPavementTri = pOppositePavementTri->m_AdjacentTris[e2];

								CNavGenNode * oppositeTriSharedNodes[2] = { NULL, NULL };
								int iOppositeNumShared = GetSharedNodes(pOppositePavementTri, pOppositeNonPavementTri, &oppositeTriSharedNodes[0], &oppositeTriSharedNodes[1], NULL);
								Assert(iOppositeNumShared==2);

								if(iOppositeNumShared == 2)
								{
									// Okay...
									// See if the two non-pavement triangles have a shared edge
									CNavGenNode * nonPavementSharedNodes[2] = { NULL, NULL };
									int iNumNonPavementShared = GetSharedNodes(pOtherTri, pOppositeNonPavementTri, &nonPavementSharedNodes[0], &nonPavementSharedNodes[1], NULL);
									if(iNumNonPavementShared == 2)
									{
										// I think we can now be sure that this satisfies our requirements
										// we should have a single node which is shared between pInputTri & pOppositeNonPavementTri

										CNavGenNode * pPivotSharedNode = NULL;
										int iNumPivotShared = GetSharedNodes(pInputTri, pOppositeNonPavementTri, &pPivotSharedNode, NULL, NULL);
										if(iNumPivotShared == 1 && (pPivotSharedNode->m_iNumTimesMovedToSmoothEdges < MAX_SMOOTHING_ITERS_ON_NAVGENNODE))
										{
											Assert(pPivotSharedNode == triSharedNodes[0] || pPivotSharedNode == triSharedNodes[1]);
											Assert(pPivotSharedNode == nonPavementSharedNodes[0] || pPivotSharedNode == nonPavementSharedNodes[1]);

											CNavGenNode * pEdgeNode1 = (pPivotSharedNode == triSharedNodes[0]) ? triSharedNodes[1] : triSharedNodes[0];
											CNavGenNode * pEdgeNode2 = (pPivotSharedNode == oppositeTriSharedNodes[0]) ? oppositeTriSharedNodes[1] : oppositeTriSharedNodes[0];

											// We want to move the pivot node to exactly between the two edge nodes

											const Vector3 vNewBasePos = (pEdgeNode1->m_vBasePos + pEdgeNode2->m_vBasePos) * 0.5f;

											if((vNewBasePos - pPivotSharedNode->m_vBasePos).XYMag2() > fSmallestMoveSqr)
											{
												Vector3 vHitPos;
												float fHitDist;
												CNavGenTri * pHitPoly = NULL;
												u32 iRetFlags = 0;

												if( m_Octree.ProcessLineOfSight(vNewBasePos + ZAXIS, vNewBasePos - ZAXIS, iRetFlags, vHitPos, fHitDist, pHitPoly, true) && pHitPoly)
												{
													if(pHitPoly->m_colPolyData.GetIsPavement())
													{
														if(MakeSureNodeMoveDoesntFlipAnyTriangles(pPivotSharedNode, vNewBasePos))
														{
															pPivotSharedNode->m_vBasePos = vNewBasePos;
															bMovedAnyNodes = true;

															pPivotSharedNode->m_iNumTimesMovedToSmoothEdges++;
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	return bMovedAnyNodes;
	*/

	return false;
}

// FUNCTION : TagPavementNavMesh
// PURPOSE : Another attempt at fixing errors caused by navmesh not picking up pavement polyons

void CNavGen::TagPavementNavMesh()
{
	const Vector3 vUp(0.0f, 0.0f, 1.0f);
	CClipVolume vol;

	for(int c=0; c<m_CollisionTriangles.size(); c++)
	{
		CNavGenTri * pCollisionTri = &m_CollisionTriangles[c];
		if(!pCollisionTri->m_colPolyData.GetIsPavement())
			continue;

		TShortMinMax minMax = pCollisionTri->m_MinMax;
		//minMax.m_iMinZ -= MINMAX_ONE;
		minMax.m_iMaxZ += MINMAX_ONE;

		bool bClipVolInitialised = false;
		for(u32 t=0; t<m_NavSurfaceTriangles.size(); t++)
		{
			CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];
			if(pTri->IsRemoved() || pTri->m_colPolyData.GetIsPavement())
				continue;

			if(!minMax.Intersects(pTri->m_MinMax))
				continue;

			// Triangles are in proximity, and collision triangle is pavement whereas
			// the navigation triangle is not pavement.

			if(!bClipVolInitialised)
			{
				vol.InitExtrudedTri(pCollisionTri->m_vPts, 1.0f, &vUp);
				bClipVolInitialised = true;
			}

			Vector3 vNodes[3] = { pTri->m_Nodes[0]->m_vBasePos, pTri->m_Nodes[1]->m_vBasePos, pTri->m_Nodes[2]->m_vBasePos };
			vNodes[0].z += m_Params.m_HeightAboveNodeBaseAtWhichToTriangulate;
			vNodes[1].z += m_Params.m_HeightAboveNodeBaseAtWhichToTriangulate;
			vNodes[2].z += m_Params.m_HeightAboveNodeBaseAtWhichToTriangulate;

			if(vol.TestTriIntersection(vNodes))
			{
				pTri->m_colPolyData.SetIsPavement(true);
			}
		}
	}
}

void CNavGen::CutPavementsIntoMesh()
{
	CutPropertiesIntoMesh(GetIsCollisionTriPavement, SetIsNavTriPavement);
}


// FUNCTION : CutPropertiesIntoMesh
// PURPOSE : Cuts navmesh polygons by the perpendicular edges of all collision polygons whose properties
//           we wish to preserve in the navmesh.  After this we tag all the cut polygon fragments  based
//           upon the properties of the collision beneath them.
//           This function is called prior to mesh optimisation, so that fragments can be optimised/merged.

#define MAX_DISTINCT_PLANES 64

void CNavGen::CutPropertiesIntoMesh( bool(*CollisionTriHasProperty)(CNavGenTri*), void(* /*SetNavTriProperty*/)(CNavSurfaceTri*) )
{
	static dev_float fSamePlaneEps = 0.1f; //0.01f;
	static dev_float fPlaneEps = 0.0f; //0.01f;
	static dev_float fAreaEps = 0.0f;
	static dev_float fNodeEpsSqrXY = 0.1f*0.1f;
	static dev_float fNodeEpsZ = 1.0f;

	// store initial num tris, so that we don't process fragments from this plane
	const size_t iNumTris = m_NavSurfaceTriangles.size();
	for(size_t t=0; t<iNumTris; t++)
	{
		CNavSurfaceTri * pNavTri = m_NavSurfaceTriangles[t];
		if(pNavTri->IsRemoved())
			continue;

		Vector4 vDistinctPlanes[MAX_DISTINCT_PLANES];
		int iNumPlanes = 0;

		for(int c=0; c<m_CollisionTriangles.size() && iNumPlanes < MAX_DISTINCT_PLANES; c++)
		{
			CNavGenTri * pCollisionTri = &m_CollisionTriangles[c];
			if(pCollisionTri->m_vNormal.z < 0.3f)
				continue;

			if(!CollisionTriHasProperty(pCollisionTri))
				continue;

			// Early out with a TShortMinMax cached in CNavSurfaceTri
			TShortMinMax colPolyMinMax = pCollisionTri->m_MinMax;
			colPolyMinMax.m_iMaxZ += MINMAX_FIXEDPT_FROM_FLOAT(1.0f);
			if(!colPolyMinMax.Intersects(pNavTri->m_MinMax))
				continue;

			for(int ce=0; ce<3; ce++)
			{
				const Vector3 & vColVert1 = pCollisionTri->m_vPts[ce];
				const Vector3 & vColVert2 = pCollisionTri->m_vPts[(ce+1)%3];

				if(LineSegsIntersect2D(vColVert1, vColVert2, pNavTri->m_Nodes[0]->m_vBasePos, pNavTri->m_Nodes[1]->m_vBasePos)!=SEGMENTS_INTERSECT &&
					LineSegsIntersect2D(vColVert1, vColVert2, pNavTri->m_Nodes[1]->m_vBasePos, pNavTri->m_Nodes[2]->m_vBasePos)!=SEGMENTS_INTERSECT &&
					LineSegsIntersect2D(vColVert1, vColVert2, pNavTri->m_Nodes[2]->m_vBasePos, pNavTri->m_Nodes[0]->m_vBasePos)!=SEGMENTS_INTERSECT)
				{
					continue;
				}

				int d;
				for(d=0; d<iNumPlanes; d++)
				{
					if( Abs(vDistinctPlanes[d].Dot3(vColVert1)+vDistinctPlanes[d].w) < fSamePlaneEps &&
						Abs(vDistinctPlanes[d].Dot3(vColVert2)+vDistinctPlanes[d].w) < fSamePlaneEps)
						break;
				}
				if(d == iNumPlanes)
				{
					// 2d edge plane
					Vector4 vColEdgePlane( vColVert2.x - vColVert1.x, vColVert2.y - vColVert1.y, 0.0f, 0.0f);
					vColEdgePlane.Normalize3();
					// perpendicular vector
					SwapEm(vColEdgePlane.x, vColEdgePlane.y);
					vColEdgePlane.y = -vColEdgePlane.y;
					// plane eq D
					vColEdgePlane.w = -DotProduct(vColEdgePlane.GetVector3(), vColVert1);

					vDistinctPlanes[iNumPlanes++] = vColEdgePlane;

					if(iNumPlanes == MAX_DISTINCT_PLANES)
						break;
				}
			}
		}

		if(!iNumPlanes)
			continue;

		atArray<CSplitPoly*> polyListA;
		atArray<CSplitPoly*> polyListB;

		atArray<CSplitPoly*> * inputPolys = &polyListA;
		atArray<CSplitPoly*> * outputPolys = &polyListB;

		CSplitPoly * pInitialPoly = new CSplitPoly();
		pInitialPoly->m_Pts.PushAndGrow(pNavTri->m_Nodes[0]->m_vBasePos);
		pInitialPoly->m_Pts.PushAndGrow(pNavTri->m_Nodes[1]->m_vBasePos);
		pInitialPoly->m_Pts.PushAndGrow(pNavTri->m_Nodes[2]->m_vBasePos);
		pInitialPoly->m_pParentTriangle = NULL;
		pInitialPoly->m_PlaneSideFlags = 0;

		inputPolys->PushAndGrow(pInitialPoly);

		// Now we have a list of planes by which to split this navmesh polygon;
		for(int pl=0; pl<iNumPlanes; pl++)
		{
			for(int p=0; p<inputPolys->size(); p++)
			{
				CSplitPoly * pSplitPoly = (*inputPolys)[p];
				CSplitPoly * pFrontFragment = NULL;
				CSplitPoly * pBackFragment = NULL;

				bool bSplit = SplitPolyToPlane(pSplitPoly, vDistinctPlanes[pl].GetVector3(), vDistinctPlanes[pl].w, fPlaneEps, &pFrontFragment, &pBackFragment);
				if(bSplit)
				{
					if(pFrontFragment)
					{
						Assert(pFrontFragment->m_Pts.size() >= 3);
						outputPolys->PushAndGrow(pFrontFragment);
					}
					if(pBackFragment)
					{
						Assert(pBackFragment->m_Pts.size() >= 3);
						outputPolys->PushAndGrow(pBackFragment);
					}
					delete pSplitPoly;
				}
				else
				{
					outputPolys->PushAndGrow(pSplitPoly);
				}
			}

			SwapEm(inputPolys, outputPolys);
			outputPolys->clear();
		}

		// We've split all the polygons to the planes
		// Now we need to construct CNavSurfaceTri's from the output fragments

		pNavTri->SetRemoved();
		RemoveAllReferencesToTriangle(pNavTri);

		for(int p=0; p<inputPolys->GetCount(); p++)
		{
			CSplitPoly * pPoly = (*inputPolys)[p];
			for(int i=1; i<pPoly->m_Pts.GetCount()-1; i++)
			{
				Vector3 vTriPts[3] = { pPoly->m_Pts[0], pPoly->m_Pts[i], pPoly->m_Pts[i+1] };
				CNavGenNode * pNodes[3] =
				{
					LocateExisingNode(vTriPts[2], fNodeEpsSqrXY, fNodeEpsZ, true, NULL, NULL),
					LocateExisingNode(vTriPts[1], fNodeEpsSqrXY, fNodeEpsZ, true, pNodes[0], NULL),
					LocateExisingNode(vTriPts[0], fNodeEpsSqrXY, fNodeEpsZ, true, pNodes[0], pNodes[1])
				};
				//CNavGenNode * pNodes[3] = { pFrontNodes[0], pFrontNodes[f], pFrontNodes[f+1] };
				const float fArea = CalcTriArea(vTriPts);
				if(fArea > fAreaEps)
				{
					CNavSurfaceTri * pNewTri = rage_new CNavSurfaceTri();
					InitNavSurfaceTri(pNewTri, pNodes, false, false);
					pNewTri->InitMinMax();

					m_NavSurfaceTriangles.push_back(pNewTri);

					// Add associations, which will be used later in optimising the mesh
					AssociateTriAndNode(pNewTri, pNewTri->m_Nodes[0]);
					AssociateTriAndNode(pNewTri, pNewTri->m_Nodes[1]);
					AssociateTriAndNode(pNewTri, pNewTri->m_Nodes[2]);
				}
			}
			delete pPoly;
		}
	}
}
CNavGenNode * CNavGen::LocateExisingNode(const Vector3 & vPos, const float fEpsSqrXY, const float fEpsZ, const bool bAllocIfNotFound, CNavGenNode * pExclude1, CNavGenNode * pExclude2)
{
	CNavGenNode * pClosestNode = NULL;
	float fClosestDistSqr = fEpsSqrXY;

	for(u32 n=0; n<m_NavSurfaceNodes.size(); n++)
	{
		CNavGenNode * pNode = m_NavSurfaceNodes[n];
		if(pNode->IsRemoved() || pNode==pExclude1 || pNode==pExclude2)
			continue;
		if(IsClose(pNode->m_vBasePos.z, vPos.z, fEpsZ))
		{
			const float fMag2 = (pNode->m_vBasePos - vPos).XYMag2();
			if(fMag2 <= fEpsSqrXY && fMag2 < fClosestDistSqr)
			{
				fClosestDistSqr = fMag2;
				pClosestNode = pNode;
			}
		}
	}
	if(!pClosestNode && bAllocIfNotFound)
	{
		pClosestNode = rage_new CNavGenNode();
		m_NavSurfaceNodes.push_back(pClosestNode);
		pClosestNode->m_vBasePos = vPos;
	}
	return pClosestNode; 
}



}	// namespace rage
