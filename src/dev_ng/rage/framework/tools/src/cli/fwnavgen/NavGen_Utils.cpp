#include "NavGen_Utils.h"
#include "common.h"
#include "NavGen.h"
#include "NavGen_Octree.h"
#include "fwmaths\Random.h"
#include "fwnavgen/datatypes.h"
#include "math/simplemath.h"
#include "vector/geometry.h"


namespace rage
{

void ReplaceExtension(char * pString, char * pExt)
{
	char * ptr = &pString[strlen(pString)-1];
	while(*ptr && *ptr != '.' && ptr != pString)
	{
		ptr--;
	}
	if(*ptr == '.')
	{
		int e;
		for(e=0; e<4; e++)
			if(*pExt)
				ptr[e] = *pExt++;
			else
				break;
		ptr[e] = 0;
	}
}

void RemoveExtension(char * pString)
{
	ReplaceExtension(pString, NULL);
}

void StripFilename(char * pString)
{
	if(!pString || !*pString)
		return;

	// rewind from end of string until we hit path separator, or string start
	char * p = &pString[ strlen(pString)-1 ];
	while(*p && p!=pString && *p != '\\' && *p != '/') p--;
	// if we hit a separator, then zero terminate
	if( p>pString && (*p=='\\' || *p=='/') )
		*p = 0;
}

bool IsNumericallyValid(const float f)
{
	if(_FPCLASS_QNAN==_fpclass(f))
	{
		return false;
	}
	if(_FPCLASS_SNAN==_fpclass(f))
	{
		return false;
	}
	if(_FPCLASS_NINF==_fpclass(f))
	{
		return false;
	}
	if(_FPCLASS_PINF==_fpclass(f))
	{
		return false;
	}
	if(false==_finite(f))
	{
		return false;
	}
	if(_isnan(f))
	{
		return false;
	}
	return true;
}

bool
RayIntersectsAABB(const Vector3 & vRayStart, const Vector3 & vRayEnd, const Vector3 & vBoxMinOriginal, const Vector3 & vBoxMaxOriginal)
{
	// Expand the bbox slightly, do deal with rays which go straight along a cardinal axis & may be missed.
	Vector3 vBoxMin = vBoxMinOriginal - Vector3(0.1f, 0.1f, 0.1f);
	Vector3 vBoxMax = vBoxMaxOriginal + Vector3(0.1f, 0.1f, 0.1f);

	// Trivial quickout cases first!	
	// Build outcodes as in clipping 	
	Vector3 d[4] =
	{
		vBoxMax - vRayStart,
		vRayStart - vBoxMin,
		vBoxMax - vRayEnd,
		vRayEnd - vBoxMin
	};

	u32 *p = (u32*)d;
	s32 s(((p[ 0]>>31)<<1) | ((p[ 1]>>31)<<3) | ((p[ 2]>>31)<<5) | ((p[ 3]>>31)) | ((p[ 4]>>31)<<2) | ((p[ 5]>>31)<<4));
	s32 e(((p[ 6]>>31)<<1) | ((p[ 7]>>31)<<3) | ((p[ 8]>>31)<<5) | ((p[ 9]>>31)) | ((p[10]>>31)<<2) | ((p[11]>>31)<<4));	
	if (s&e) 		return false; 	// both points same side of exterior of box	
	if (!(s*e))		return true;    //	if (!s || !e) 	return true; 	// trivial acceptance - either point inside ( DW - might be a faster way than these two tests since we know we are testing for zero in both and that when anded together they return 0 )
	s32 b = s|e;					// compute crossed planes with codes 
	Vector3 rd(vRayEnd - vRayStart);
	float r,x,y,z,recip;
	
	if (b&3)	
	{
		recip = 1.0f/rd.x;
		if (b&1) // is it the MIN PLANE (X)
		{	
			r = d[1].x * -recip;
			y = rd.y * r + vRayStart.y;
			z = rd.z * r + vRayStart.z;
			if ((y > vBoxMin.y) && (y < vBoxMax.y) && (z > vBoxMin.z) && (z < vBoxMax.z))
				return true;
		}
		if (b&2) // is it the MAX PLANE (X)
		{
			r = d[0].x * recip;
			y = rd.y * r + vRayStart.y;
			z = rd.z * r + vRayStart.z;
			if ((y > vBoxMin.y) && (y < vBoxMax.y) && (z > vBoxMin.z) && (z < vBoxMax.z))
				return true;
		}
	}	

	if (b&(3<<2))	
	{
		recip = 1.0f/rd.y;
		if (b&4) // is it the MIN PLANE (Y)
		{
			r = d[1].y * -recip;
			x = rd.x * r + vRayStart.x;
			z = rd.z * r + vRayStart.z;
			if ((x > vBoxMin.x) && (x < vBoxMax.x) && (z > vBoxMin.z) && (z < vBoxMax.z))
				return true;
		}
		if (b&8) // is it the MAX PLANE (Y)
		{
			r = d[0].y * recip;
			x = rd.x * r + vRayStart.x;
			z = rd.z * r + vRayStart.z;
			if ((x > vBoxMin.x) && (x < vBoxMax.x) && (z > vBoxMin.z) && (z < vBoxMax.z))
				return true;
		}
	}
	
	if (b&(3<<4))	
	{	
		recip = 1.0f/rd.z;
		if (b&16) // is it the MIN PLANE (Z)
		{
			r = d[1].z * -recip;
			x = rd.x * r + vRayStart.x;
			y = rd.y * r + vRayStart.y;
			if ((x > vBoxMin.x) && (x < vBoxMax.x) && (y > vBoxMin.y) && (y < vBoxMax.y))
				return true;
		}
		if (b&32) // is it the MAX PLANE (Z)
		{
			r = d[0].z * recip;
			x = rd.x * r + vRayStart.x;
			y = rd.y * r + vRayStart.y;
			if ((x > vBoxMin.x) && (x < vBoxMax.x) && (y > vBoxMin.y) && (y < vBoxMax.y))
				return true;
		}
	}
	
	return false;
}

// http://softsurfer.com/Archive/algorithm_0105/algorithm_0105.htm#intersect_RayTriangle()
// Returns whether the ray intersects the triangle
#define SMALL_NUM		0.00000001f

bool
RayIntersectsTriangle(
	const Vector3 & vRayStart,
	const Vector3 & vRayEnd,
	Vector3 * pVerts,
	Vector3 & vecIntersect)
{
	Vector3 u, v, n;
	Vector3 dir, w0, w;
	float r, a, b;

	u = pVerts[1] - pVerts[0];
	v = pVerts[2] - pVerts[0];
	n = CrossProduct(u, v);
	if(n.Mag2() < SMALL_NUM)
	{
		return false;
	}

	dir = vRayEnd - vRayStart;
	w0 = vRayStart - pVerts[0];
	a = - DotProduct(n, w0);
	b = DotProduct(n, dir);
	if(rage::Abs(b) < SMALL_NUM)
	{
		// NB : Could be parallel
		return false;
	}

	r = a / b;
	if(r < 0.0f || r > 1.0f)
	{
		return false;
	}

	vecIntersect = vRayStart + r * dir;

	float uu,uv,vv,wu,wv,D;
	uu = DotProduct(u,u);
	uv = DotProduct(u,v);
	vv = DotProduct(v,v);
	w = vecIntersect - pVerts[0];
	wu = DotProduct(w,u);
	wv = DotProduct(w,v);
	D = uv * uv - uu * vv;

	float s,t;
	s = (uv * wv - vv * wu) / D;
	if(s < 0.0f || s > 1.0f)
		return false;
	
	t = (uv * wu - uu * wv) / D;
	if(t < 0.0f || (s+t) > 1.0f)
		return false;

	return true;
}



bool SegEdgeCheckUndirectedPreCalc(
	const Vector3 &p0, const Vector3 &p1,
	const Vector4 &RaySeg, const Vector4 &edgeNormalCross, const Vector4 &dispEdges)
{
	Vector4 edge, crossEdges;
	edge.Subtract(p1,p0);
	crossEdges.Cross(edge,RaySeg);		// From the collide edge to the polygon edge (or other direction)
	float segDir = dispEdges.Dot3(crossEdges);
	if(segDir==0.0f) return false;
	float faceDir = edgeNormalCross.Dot3(crossEdges);
	return (!SameSign(faceDir,segDir));
}
bool SegEdgeCheckUndirected(
	const Vector3 &p0, const Vector3 &p1,
	const Vector3 &segA, const Vector4 &RaySeg, const Vector4 &edgeNormalCross)
{
	Vector4 edge, crossEdges, dispEdges;
	edge.Subtract(p1,p0);
	crossEdges.Cross(edge,RaySeg);		// From the collide edge to the polygon edge (or other direction)
	dispEdges.Subtract(segA,p0);		
	float segDir = dispEdges.Dot3(crossEdges);
	if(segDir==0.0f) return false;
	float faceDir = edgeNormalCross.Dot3(crossEdges);
	return (!SameSign(faceDir,segDir));
}

bool
TestRayTriangle(
	const Vector3 & vRayStart,
	const Vector3 & vRayEnd,
	const Vector3 & vTriNormal,
	Vector3 * pVerts)
{
	Vector3 vRayDir = vRayEnd - vRayStart;
	ScalarV fFraction;

	int ret = geomSegments::SegmentTriangleIntersectUndirected(
		VECTOR3_TO_VEC3V(vRayStart),
		VECTOR3_TO_VEC3V(vRayDir),
		VECTOR3_TO_VEC3V(vTriNormal),
		VECTOR3_TO_VEC3V(pVerts[0]), VECTOR3_TO_VEC3V(pVerts[1]), VECTOR3_TO_VEC3V(pVerts[2]),
		fFraction
	);

	return (ret == 0);
}

float CalcPolyAreaXY(atArray<Vector3> & pts)
{
	Vector3 v0 = pts[0];
	v0.z = 0.0f;

	float fArea = 0.0f;

	for(s32 i=1; i<pts.GetCount()-1; i++)
	{
		Vector3 v1 = pts[i];
		Vector3 v2 = pts[i+1];

		v1.z = v2.z = 0.0f;

		fArea += CalcTriArea(v0, v1, v2);
	}
	return fArea;
}

void GetRandomPointInTriangle(const Vector3 & v1, const Vector3 & v2, const Vector3 & v3, Vector3 & vOut)
{
	// Adam Croston wrote this, & I pinched it!
	vOut = Vector3(0.0f, 0.0f, 0.0f);

	Vector3 vAB = v2 - v1;
	Vector3 vAC = v3 - v1;

	float u = fwRandom::GetRandomNumberInRange(0.0f, 1.0f);
	float v = fwRandom::GetRandomNumberInRange(0.0f, 1.0f);
	if((u + v) > 1.0f)
	{
		u = 1.0f - u;
		v = 1.0f - v;
	}

	vOut = (vAB * u) + (vAC * v);
	vOut += v1;
}

// NAME : ComputeOverlapInterval
// PURPOSE : Given two colinear edges, this computes the overlap interval (ie. the length in which they run parallel)

bool ComputeOverlapInterval(const Vector3 & vEdgeA_V1, const Vector3 & vEdgeA_V2, const Vector3 & vEdgeB_V1, const Vector3 & vEdgeB_V2, float & fOut_OverlapInterval)
{
	fOut_OverlapInterval = 0.0f;

	Vector3 vUnitEdgeA = vEdgeA_V2 - vEdgeA_V1;
	if(vUnitEdgeA.Mag2() < SMALL_FLOAT)
		return false;
	Vector3 vUnitEdgeB = vEdgeB_V2 - vEdgeB_V1;
	if(vUnitEdgeB.Mag2() < SMALL_FLOAT)
		return false;

	vUnitEdgeA.Normalize();
	vUnitEdgeB.Normalize();

	const float fDot = (vUnitEdgeA.x * vUnitEdgeB.x) + (vUnitEdgeA.y * vUnitEdgeB.y); 

	if(fDot < 0.0f)
	{
		// Test for interval overlap
		float fEdgeBMin = vUnitEdgeA.Dot( vEdgeB_V1 );
		float fEdgeBMax = vUnitEdgeA.Dot( vEdgeB_V2 );
		if(fEdgeBMin > fEdgeBMax)
			SwapEm(fEdgeBMin, fEdgeBMax);

		float fEdgeAMin = vUnitEdgeA.Dot( vEdgeA_V1 );
		float fEdgeAMax = vUnitEdgeA.Dot( vEdgeA_V2 );
		if(fEdgeAMin > fEdgeAMax)
			SwapEm(fEdgeAMin, fEdgeAMax);

		fOut_OverlapInterval = (fEdgeBMin < fEdgeAMin) ?
			fEdgeAMin - fEdgeBMax : fEdgeBMin - fEdgeAMax;

		return true;
	}

	return false;
}



#define SPLIT_POLY_MAX_NUM_PTS	64

// Splits the given triangle to the plane.  Front & back fragments are created as applicable.
bool SplitPolyToPlane(CSplitPoly * pInputPoly, const Vector3 & vPlaneNormal, const float & fPlaneDist, const float fSplitPlaneEps, CSplitPoly ** frontPoly, CSplitPoly ** backPoly)
{
	Vector3 vFrontPts[SPLIT_POLY_MAX_NUM_PTS];
	Vector3 vBackPts[SPLIT_POLY_MAX_NUM_PTS];

	s32 iNumFrontPts = 0;
	s32 iNumBackPts = 0;

	s32 lastv = pInputPoly->m_Pts.GetCount()-1;
	u32 i;
	s32 v;
	Vector3 vec, lastvec;

	lastvec = pInputPoly->m_Pts[lastv];
	float fLastDot = DotProduct(lastvec, vPlaneNormal) + fPlaneDist;
	

	for(v=0; v<pInputPoly->m_Pts.GetCount(); v++)
	{
		vec = pInputPoly->m_Pts[v];

		float fDot = DotProduct(vec, vPlaneNormal) + fPlaneDist;

		if(fLastDot < fSplitPlaneEps)
		{
			vBackPts[iNumBackPts++] = lastvec;
		}
		else if(fLastDot >= fSplitPlaneEps)
		{
			vFrontPts[iNumFrontPts++] = lastvec;
		}

		if(SIGN(fDot) != SIGN(fLastDot))
		{
			// split edge at plane
			float s = (fLastDot / (fLastDot - fDot));
			Vector3 iPoint = lastvec + ((vec - lastvec) * s);
			vBackPts[iNumBackPts++] = iPoint;
			vFrontPts[iNumFrontPts++] = iPoint;
		}

		FastAssert(iNumFrontPts < SPLIT_POLY_MAX_NUM_PTS);
		FastAssert(iNumBackPts < SPLIT_POLY_MAX_NUM_PTS);

		fLastDot = fDot;
		lastv = v;
		lastvec = vec;
	}
	
	if(iNumFrontPts >= 3)
	{
		// NB : need to check for degenerate polys here
		*frontPoly = rage_new CSplitPoly();
		for(i=0; i<(u32)iNumFrontPts; i++)
		{
			(*frontPoly)->m_Pts.PushAndGrow(vFrontPts[i]);
		}
		//(*frontPoly)->m_iSurfaceType = pInputPoly->m_iSurfaceType;
		(*frontPoly)->m_colPolyData = pInputPoly->m_colPolyData;
		(*frontPoly)->m_iBakedInDataBitField = pInputPoly->m_iBakedInDataBitField;
	}
	if(iNumBackPts >= 3)
	{
		// NB : need to check for degenerate polys here
		*backPoly = rage_new CSplitPoly();
		for(i=0; i<(u32)iNumBackPts; i++)
		{
			(*backPoly)->m_Pts.PushAndGrow(vBackPts[i]);
		}
		//(*backPoly)->m_iSurfaceType = pInputPoly->m_iSurfaceType;
		(*backPoly)->m_colPolyData = pInputPoly->m_colPolyData;
		(*backPoly)->m_iBakedInDataBitField = pInputPoly->m_iBakedInDataBitField;
	}

	return true;
}

float GetPlaneDist(const Vector4 & vPlane, const Vector3 & vPos)
{
	return vPlane.Dot3(vPos) + vPlane.w;
}

// Classifies the given triangle to the plane.
ePlaneSide ClassifyPolyToAxialPlane(CSplitPoly * pInputPoly, const u32 iPlaneAlignmentFlag, const float & fPlaneDist)
{
	int iNumPts = pInputPoly->m_Pts.size();
	int iNumInFront = 0;
	int iNumBehind = 0;

	for(int v=0; v<iNumPts; v++)
	{
		const Vector3 & vec = pInputPoly->m_Pts[v];
		float fDot;

		// Do this instead of a dot-product
		switch(iPlaneAlignmentFlag)
		{
			case PLANESIDE_POSX:
				fDot = vec.x - fPlaneDist;
				break;
			case PLANESIDE_NEGX:
				fDot = vec.x + fPlaneDist;
				break;
			case PLANESIDE_POSY:
				fDot = vec.y - fPlaneDist;
				break;
			case PLANESIDE_NEGY:
				fDot = vec.y + fPlaneDist;
				break;
			case PLANESIDE_POSZ:
				fDot = vec.z - fPlaneDist;
				break;
			case PLANESIDE_NEGZ:
				fDot = vec.z + fPlaneDist;
				break;
			default:
				fDot = 0.0f;
				break;
		}

		if(fDot >= 0.0f)
			iNumInFront++;
		else
			iNumBehind++;
	}

	if(iNumInFront == iNumPts)
		return ePlaneFront;

	if(iNumBehind == iNumPts)
		return ePlaneBack;

	return ePlaneStraddles;
}

// Splits the given triangle to the plane.  Front & back fragments are created as applicable.
bool SplitPolyToAxialPlane(CSplitPoly * pInputPoly, const u32 iPlaneAlignmentFlag, const float & fPlaneDist, const float fSplitPlaneEps, CSplitPoly ** frontPoly, CSplitPoly ** backPoly)
{
	Vector3 vFrontPts[SPLIT_POLY_MAX_NUM_PTS];
	Vector3 vBackPts[SPLIT_POLY_MAX_NUM_PTS];

	s32 iNumFrontPts = 0;
	s32 iNumBackPts = 0;

	s32 lastv = pInputPoly->m_Pts.GetCount()-1;
	u32 i;
	s32 v;
	Vector3 vec, lastvec;

	lastvec = pInputPoly->m_Pts[lastv];

	float fLastDot, fDot;

	// Do this instead of a dot-product
	switch(iPlaneAlignmentFlag)
	{
		case PLANESIDE_POSX:
			fLastDot = lastvec.x - fPlaneDist;
			break;
		case PLANESIDE_NEGX:
			fLastDot = lastvec.x + fPlaneDist;
			break;
		case PLANESIDE_POSY:
			fLastDot = lastvec.y - fPlaneDist;
			break;
		case PLANESIDE_NEGY:
			fLastDot = lastvec.y + fPlaneDist;
			break;
		case PLANESIDE_POSZ:
			fLastDot = lastvec.z - fPlaneDist;
			break;
		case PLANESIDE_NEGZ:
			fLastDot = lastvec.z + fPlaneDist;
			break;
		default:
			fLastDot = 0.0f;
			break;
	}	

	for(v=0; v<pInputPoly->m_Pts.GetCount(); v++)
	{
		vec = pInputPoly->m_Pts[v];

		// Do this instead of a dot-product
		switch(iPlaneAlignmentFlag)
		{
			case PLANESIDE_POSX:
				fDot = vec.x - fPlaneDist;
				break;
			case PLANESIDE_NEGX:
				fDot = vec.x + fPlaneDist;
				break;
			case PLANESIDE_POSY:
				fDot = vec.y - fPlaneDist;
				break;
			case PLANESIDE_NEGY:
				fDot = vec.y + fPlaneDist;
				break;
			case PLANESIDE_POSZ:
				fDot = vec.z - fPlaneDist;
				break;
			case PLANESIDE_NEGZ:
				fDot = vec.z + fPlaneDist;
				break;
			default:
				fDot = 0.0f;
				break;
		}	

		if(fLastDot < fSplitPlaneEps)
		{
			vBackPts[iNumBackPts++] = lastvec;
		}
		else if(fLastDot >= fSplitPlaneEps)
		{
			vFrontPts[iNumFrontPts++] = lastvec;
		}

		if(SIGN(fDot) != SIGN(fLastDot))
		{
			// split edge at plane
			float s = (fLastDot / (fLastDot - fDot));
			Vector3 iPoint = lastvec + ((vec - lastvec) * s);
			vBackPts[iNumBackPts++] = iPoint;
			vFrontPts[iNumFrontPts++] = iPoint;
		}

		fLastDot = fDot;
		lastv = v;
		lastvec = vec;
	}
	
	if(iNumFrontPts >= 3)
	{
		// NB : need to check for degenerate polys here
		*frontPoly = rage_new CSplitPoly();
		for(i=0; i<(u32)iNumFrontPts; i++)
		{
			(*frontPoly)->m_Pts.PushAndGrow(vFrontPts[i]);
		}
		(*frontPoly)->m_colPolyData = pInputPoly->m_colPolyData;
		(*frontPoly)->m_iBakedInDataBitField = pInputPoly->m_iBakedInDataBitField;
	}
	if(iNumBackPts >= 3)
	{
		// NB : need to check for degenerate polys here
		*backPoly = rage_new CSplitPoly();
		for(i=0; i<(u32)iNumBackPts; i++)
		{
			(*backPoly)->m_Pts.PushAndGrow(vBackPts[i]);
		}
		(*backPoly)->m_colPolyData = pInputPoly->m_colPolyData;
		(*backPoly)->m_iBakedInDataBitField = pInputPoly->m_iBakedInDataBitField;
	}

	return true;
}

#define NUM_CLOSE_PTS_IN_TRI	6

// Find the closest point in a triangle, to another given point.  This may be at a vertex, along an edge, or in the interior
float ClosestPointInTriangleToPoint(Vector3 * triPts, const Vector3 & pTargetPoint, Vector3 & outPoint)
{
	Vector3 vPts[NUM_CLOSE_PTS_IN_TRI];

	Vector3 vCentroid = (triPts[0] + triPts[1] + triPts[2]) * ONE_THIRD;

	vPts[0] = (((triPts[0] + triPts[1]) * 0.5f) + vCentroid) * 0.5f;
	vPts[1] = (((triPts[1] + triPts[2]) * 0.5f) + vCentroid) * 0.5f;
	vPts[2] = (((triPts[2] + triPts[0]) * 0.5f) + vCentroid) * 0.5f;

	vPts[3] = (triPts[0] * 0.5f) + (vCentroid * 0.5f);
	vPts[4] = (triPts[1] * 0.5f) + (vCentroid * 0.5f);
	vPts[5] = (triPts[2] * 0.5f) + (vCentroid * 0.5f);

	Vector3 vDiff;
	int iLeastDistIndex = -1;
	float fLeastDistSqr = FLT_MAX;
	float fDistSqr;

	for(int t=0; t<NUM_CLOSE_PTS_IN_TRI; t++)
	{
		//vDiff.FromSubtract(pTargetPoint, vPts[t]);
		vDiff = pTargetPoint - vPts[t];
		fDistSqr = vDiff.Mag2();
		if(fDistSqr < fLeastDistSqr)
		{
			fLeastDistSqr = fDistSqr;
			iLeastDistIndex = t;
		}
	}

	outPoint = vPts[iLeastDistIndex];

	return fLeastDistSqr;
}

void RandomPointInTriangle(Vector3 * triPts, Vector3 & vRandom)
{
	Vector3 vWeights;

	vWeights.x = (((float)rand()) / ((float)RAND_MAX));
	vWeights.y = (((float)rand()) / ((float)RAND_MAX));
	vWeights.z = (((float)rand()) / ((float)RAND_MAX));
	vWeights.Normalize();

	vRandom = (triPts[0] * vWeights.x) + (triPts[1] * vWeights.y) + (triPts[2] * vWeights.z);
}


int NumAdjacentTris(CNavSurfaceTri * pTri)
{
	int n=0;
	if(pTri->m_AdjacentTris[0]) n++;
	if(pTri->m_AdjacentTris[1]) n++;
	if(pTri->m_AdjacentTris[2]) n++;
	return n;
}

void TriangleCentroid(CNavSurfaceTri * pTri, Vector3 & vec)
{
	vec = (pTri->m_Nodes[0]->m_vBasePos + pTri->m_Nodes[1]->m_vBasePos + pTri->m_Nodes[2]->m_vBasePos) * ONE_THIRD;
}

void CalcTriNormal(const Vector3 * pPts, Vector3 & vNormal)
{
	Vector3 edge1, edge2;

	edge1 = pPts[1] - pPts[0];
	edge2 = pPts[2] - pPts[0];

	edge1.Normalize();
	edge2.Normalize();

	vNormal = CrossProduct(edge2, edge1);
	vNormal.Normalize();
}

int GetNodeIndex(CNavGenNode ** pNodes, CNavGenNode * pNode)
{
	if(pNodes[0] == pNode) return 0;
	if(pNodes[1] == pNode) return 1;
	if(pNodes[2] == pNode) return 2;
	return -1;
}

int GetWindingOrder(CNavGenNode ** pNodes, CNavGenNode * pNode1, CNavGenNode * pNode2)
{
	int iNode1 = GetNodeIndex(pNodes, pNode1);
	int iNode2 = GetNodeIndex(pNodes, pNode2);
	Assert(iNode1 != -1 && iNode2 != -1);

	if(iNode2 == ((iNode1+1)%3))
		return 1;
	return -1;
}

void PlaneFromPoints(Vector3 * pPts, Vector3 * vPlaneNormal, float & fPlaneDist)
{
	Vector3 edge1, edge2;

	edge1 = pPts[1] - pPts[0];
	edge2 = pPts[2] - pPts[0];

	edge1.Normalize();
	edge2.Normalize();

	*vPlaneNormal = CrossProduct(edge2, edge1);
	vPlaneNormal->Normalize();

	fPlaneDist = - DotProduct(*vPlaneNormal, pPts[0]);
}

int GetSharedNodes(CNavSurfaceTri * pTri1, CNavSurfaceTri * pTri2, CNavGenNode ** pNode1, CNavGenNode ** pNode2, CNavGenNode ** pNode3)
{
	CNavGenNode * pSharedNodes[3] = { NULL, NULL, NULL };
	int iNumSharedNodes = 0;

	for(s32 n1=0; n1<3; n1++)
	{
		for(s32 n2=0; n2<3; n2++)
		{
			if(pTri1->m_Nodes[n1] == pTri2->m_Nodes[n2])
			{
				Assert(iNumSharedNodes < 3);
				pSharedNodes[iNumSharedNodes++] = pTri1->m_Nodes[n1];
			}
		}
	}

	if(pNode1) *pNode1 = pSharedNodes[0];
	if(pNode2) *pNode2 = pSharedNodes[1];
	if(pNode3) *pNode3 = pSharedNodes[2];

	return iNumSharedNodes;
}

// It is assumed that pNodesList1 and pNodesList2 are each an array of 3 CNavGenNode pointers, as may found in a CNavSurfaceTri
int GetSharedNodes(CNavGenNode ** pNodesList1, CNavGenNode ** pNodesList2, CNavGenNode ** pNode1, CNavGenNode ** pNode2, CNavGenNode ** pNode3)
{
	CNavGenNode * pSharedNodes[3] = { NULL, NULL, NULL };
	int iNumSharedNodes = 0;

	for(s32 n1=0; n1<3; n1++)
	{
		for(s32 n2=0; n2<3; n2++)
		{
			if(pNodesList1[n1] == pNodesList2[n2])
			{
				Assert(iNumSharedNodes < 3);
				pSharedNodes[iNumSharedNodes++] = pNodesList1[n1];
			}
		}
	}

	if(pNode1) *pNode1 = pSharedNodes[0];
	if(pNode2) *pNode2 = pSharedNodes[1];
	if(pNode3) *pNode3 = pSharedNodes[2];

	return iNumSharedNodes;
}

CNavGenNode * GetOtherNode(CNavSurfaceTri * pTri1, CNavGenNode * pNode1, CNavGenNode * pNode2)
{
	if( (pTri1->m_Nodes[0]==pNode1 && pTri1->m_Nodes[1]==pNode2) || (pTri1->m_Nodes[1]==pNode1 && pTri1->m_Nodes[0]==pNode2))
		return pTri1->m_Nodes[2];
	if( (pTri1->m_Nodes[1]==pNode1 && pTri1->m_Nodes[2]==pNode2) || (pTri1->m_Nodes[2]==pNode1 && pTri1->m_Nodes[1]==pNode2))
		return pTri1->m_Nodes[0];
	if( (pTri1->m_Nodes[0]==pNode1 && pTri1->m_Nodes[2]==pNode2) || (pTri1->m_Nodes[2]==pNode1 && pTri1->m_Nodes[0]==pNode2))
		return pTri1->m_Nodes[1];
	return NULL;
}

/*	SegmentIntersection
------------------------------------------------------------------------------------------
	
	Determines if two segments intersect, and if so the point of intersection. The current
	member line is considered line AB and the incomming parameter is considered line CD for
	the purpose of the utilized equations.

	A = PointA of the member line
	B = PointB of the member line
	C = PointA of the provided line	
	D = PointB of the provided line	

------------------------------------------------------------------------------------------
*/

int LineSegsIntersect2D(const Vector3 & A, const Vector3 & B, const Vector3 & C, const Vector3 & D)
{
	Vector3 vA = A;
	Vector3 vB = B;
	Vector3 vC = C;
	Vector3 vD = D;

	vA.z = 0;
	vB.z = 0;
	vC.z = 0;
	vD.z = 0;

	Vector3 vABEdge = vB - vA;
	vABEdge.Normalize();
	Vector3 vCDEdge = vD - vC;
	vCDEdge.Normalize();

	Vector3 vABNormal;
	vABNormal.y = -vABEdge.x;
	vABNormal.x = vABEdge.y;
	vABNormal.z = 0.0f;

	Vector3 vCDNormal;
	vCDNormal.y = -vCDEdge.x;
	vCDNormal.x = vCDEdge.y;
	vCDNormal.z = 0.0f;

	float ABPlaneDist = - DotProduct(vABNormal, vA);
	float CDPlaneDist = - DotProduct(vCDNormal, vC);

	// classify A wrt CD
	float ADist = DotProduct(vA, vCDNormal) + CDPlaneDist;

	// classify B wrt CD
	float BDist = DotProduct(vB, vCDNormal) + CDPlaneDist;

	// classify C wrt AB
	float CDist = DotProduct(vC, vABNormal) + ABPlaneDist;

	// classify D wrt AB
	float DDist = DotProduct(vD, vABNormal) + ABPlaneDist;

	// get signs
	int iSignA = SIGN(ADist);
	int iSignB = SIGN(BDist);
	int iSignC = SIGN(CDist);
	int iSignD = SIGN(DDist);

	if(iSignA == 0) iSignA = -1;
	if(iSignB == 0) iSignB = -1;
	if(iSignC == 0) iSignC = -1;
	if(iSignD == 0) iSignD = -1;

	// Have an epsilon test for points A & B, which are always the 'waypoints-in-triangle'.
	// If signs are the same, and either is very close to the triangle edge - then force a result.
	static const float fOnLineEps = 0.05f;

	// If both points are on same side of line & are very close, then force an intersection
	if(iSignA != 0 && (iSignA == iSignB) && ((rage::Abs(ADist) < fOnLineEps) || (rage::Abs(BDist) < fOnLineEps)))
	{
		iSignB = -iSignA;
	}

	if(iSignA == 0 || iSignB == 0 || iSignC == 0 || iSignD == 0)
	{
		// dont really care if they don't actually intersect, all I want is to know if SEGMENTS_INTERSECT
		return LINES_INTERSECT;
	}

	if(iSignA != iSignB && iSignC != iSignD)
	{
		return SEGMENTS_INTERSECT;
	}

	return LINES_INTERSECT;
}



void OutOfMemory(char * szReason)
{
	szReason;
}

void ErrorMsg(char * szMessage)
{
	szMessage;
}

// This function parses the NavMesh name (eg. "Sectors8x8_80_96", "Sectors4x4_12_16") and
// returns the actual index that the mesh will take in the game.  It does this by knowing
// the maximum number of X&Y sectors in the game engine, and reading in the mesh size and
// coordinates.
/*
bool
GetDetailsFromMeshName(char * pName, int & iNavMeshIndex, int & iNavMeshXPos, int & iNavMeshYPos)
{
	// rewind a pointer from the end, to the beginning of the file title
	char * ptr = &pName[strlen(pName)-1];
	while(ptr != pName && *ptr != '\\' && *ptr != '/')
		ptr--;

	if(ptr != pName)
		ptr++;

	char tmpName[256];
	strcpy(tmpName, ptr);
	ptr = &tmpName[0];

	// The first 7 chars should be "navmesh"
	if(strstr(ptr, "navmesh")!=ptr) return false;

	ptr += 7;
	if(!*ptr) return false;

	char * pBlockWidthStr = ptr;

	while(*ptr != 'x')
		ptr++;
	if(!*ptr) return false;
	*ptr = 0;
	ptr++;
	if(!*ptr) return false;

	char * pBlockHeightStr = ptr;

	while(*ptr != '_')
		ptr++;
	if(!*ptr) return false;
	*ptr = 0;
	ptr++;
	if(!*ptr) return false;

	char * pMeshXPosStr = ptr;

	while(*ptr != '_')
		ptr++;
	if(!*ptr) return false;
	*ptr = 0;
	ptr++;
	if(!*ptr) return false;

	char * pMeshYPosStr = ptr;

	while(*ptr != '.')
		ptr++;
	if(!*ptr) return false;
	*ptr = 0;
	ptr++;
	if(!*ptr) return false;

	if(!pBlockWidthStr || !pBlockHeightStr || !pMeshXPosStr || !pMeshYPosStr)
		return false;

	int iBlockWidth, iBlockHeight, iMeshXPos, iMeshYPos;
		
	sscanf(pBlockWidthStr, "%i", &iBlockWidth);
	sscanf(pBlockHeightStr, "%i", &iBlockHeight);
	sscanf(pMeshXPosStr, "%i", &iMeshXPos);
	sscanf(pMeshYPosStr, "%i", &iMeshYPos);

	// blocksize must be exactly divisible into world size in sectors
	Assert((WORLD_WIDTHINSECTORS % iBlockWidth) == 0);
	Assert((WORLD_DEPTHINSECTORS % iBlockHeight) == 0);

	int iNumBlocksInMapWidth = WORLD_WIDTHINSECTORS / iBlockWidth;
//	int iNumBlocksInMapHeight = WORLD_DEPTHINSECTORS / iBlockHeight;

	iNavMeshXPos = iMeshXPos / iBlockWidth;
	iNavMeshYPos = iMeshYPos / iBlockHeight;
	iNavMeshIndex = (iNavMeshYPos * iNumBlocksInMapWidth) + iNavMeshXPos;
	iNavMeshBlockWidth = iBlockWidth;
	iNavMeshBlockHeight = iBlockHeight;

	return true;
}
*/

/*
bool IsSurfaceTypeAllowedToBeSteep(u16 iType)
{
	return PGTAMATERIALMGR->GetIsStairs(iType);
}

bool IsSurfaceTypeSeeThrough(u16 iType)
{
	return PGTAMATERIALMGR->GetIsSeeThrough(iType);
}

bool IsSurfaceTypeShootThrough(u16 iType)
{
	return PGTAMATERIALMGR->GetIsShootThrough(iType);
}
*/

/*
bool DoWeCareAboutThisSurfaceType(u16 iSurfaceType)
{
	if(IsSurfaceTypePavement(iSurfaceType))
		return true;
//	if(PGTAMATERIALMGR->GetIsPavement(iSurfaceType))
//		return true;

	return false;
}
*/

bool CanWeMergeThesePolys(TMergePoly * pPoly1, TMergePoly * pPoly2)
{
	bool bPoly1HasPedDensity = pPoly1->m_colPolyData.GetPedDensity() > 0;
	bool bPoly2HasPedDensity = pPoly2->m_colPolyData.GetPedDensity() > 0;

	bool bSamePedDensity = (bPoly1HasPedDensity == bPoly2HasPedDensity);

	bool bSamePavementFlags = pPoly1->m_colPolyData.GetIsPavement() == pPoly2->m_colPolyData.GetIsPavement();
	bool bSameInteriorFlags = pPoly1->m_colPolyData.GetIsInterior() == pPoly2->m_colPolyData.GetIsInterior();
	//bool bCanMergeBakedInData = pPoly1->m_iBakedInBitField == pPoly2->m_iBakedInBitField;
	bool bCanMerge = (bSamePedDensity && bSamePavementFlags && bSameInteriorFlags); // && bCanMergeBakedInData);
	return bCanMerge;
}

//---------------------------------------------------------------
// 1st pass solution : a simplistic object position test vs poly

bool TestMergePolyObjectIntersection(TMergePoly * pPoly, fwDynamicEntityInfo * pObj)
{
	u32 v;

	Vector3 vMin(FLT_MAX, FLT_MAX, FLT_MAX);
	Vector3 vMax(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	for(v=0; v<(u32)pPoly->m_Vertices.GetCount(); v++)
	{
		vMin.x = Min(vMin.x, pPoly->m_Vertices[v]->m_Vertex.x);
		vMin.y = Min(vMin.y, pPoly->m_Vertices[v]->m_Vertex.y);
		vMin.z = Min(vMin.z, pPoly->m_Vertices[v]->m_Vertex.z);
		vMax.x = Max(vMax.x, pPoly->m_Vertices[v]->m_Vertex.x);
		vMax.y = Max(vMax.y, pPoly->m_Vertices[v]->m_Vertex.y);
		vMax.z = Max(vMax.z, pPoly->m_Vertices[v]->m_Vertex.z);
	}
	vMin.z -= 1.5f;
	vMax.z += 1.0f;

	TShortMinMax tmpMinMax;
	tmpMinMax.Set(vMin, vMax);

	if(!pObj->m_MinMax.Intersects(tmpMinMax))
	{
		return false;
	}

	//--------------------------------------------------------------
	// Test all object points vs all edge planes of the polygon
	// If all are outside any plane, then there is no intersection

	u32 lastv = pPoly->m_Vertices.GetCount()-1;
	for(v=0; v<(u32)pPoly->m_Vertices.GetCount(); v++)
	{
		Vector3 vEdgeVec = pPoly->m_Vertices[v]->m_Vertex - pPoly->m_Vertices[lastv]->m_Vertex;
		if(vEdgeVec.Mag2() > SMALL_FLOAT)
		{
			vEdgeVec.Normalize();

			Vector3 vTmp;
			vTmp.Cross(vEdgeVec, ZAXIS);
			vTmp.Normalize();

			Vector3 vUp3d;
			vUp3d.Cross(vEdgeVec, vTmp);
			vUp3d.Normalize();

			Vector3 vEdgeNormal;
			vEdgeNormal.Cross(vUp3d, vEdgeVec);
			vEdgeNormal.Normalize();

			const float fPlaneD = - DotProduct(vEdgeNormal, pPoly->m_Vertices[v]->m_Vertex);

			u32 p;
			for(p=0; p<4; p++)
			{
				if( DotProduct(pObj->m_vCorners[p], vEdgeNormal) + fPlaneD < 0.0f )
					break;
			}
			// All 4 corner points are outside of a polygon edge: no intersection here
			if(p == 4)
			{
				return false;
			}
		}

		lastv = v;
	}

	//------------------------------------------------------------------
	// Test all polygon points vs the edge planes of the dynamic object

	for(v=0; v<4; v++)
	{
		u32 p;
		for(p=0; p<(u32)pPoly->m_Vertices.GetCount(); p++)
		{
			if( GetPlaneDist( pObj->m_Planes[v], pPoly->m_Vertices[p]->m_Vertex ) < 0.0f )
				break;
		}
		// All polygon vertices are outside of an object edge plane: no intersection here
		if(p == 4)
		{
			return false;
		}
	}

	return true;
}


void CompressVertex(const Vector3 & vertex, u16 & iX, u16 & iY, u16 & iZ, const Vector3 & vMin, const Vector3 & vSize)
{
	float fTmp;
	s32 iTmp;

	// X Component
	fTmp = vertex.x - vMin.x;
	fTmp /= vSize.x;
	iTmp = (s32)(fTmp * 65536.0f);

	if(iTmp < 0)
		iTmp = 0;
	else if(iTmp > 65535)
		iTmp = 65535;
	iX = (u16)iTmp;

	// Y Component
	fTmp = vertex.y - vMin.y;
	fTmp /= vSize.y;
	iTmp = (s32)(fTmp * 65536.0f);

	if(iTmp < 0)
		iTmp = 0;
	else if(iTmp > 65535)
		iTmp = 65535;
	iY = (u16)iTmp;

	// Z Component
	fTmp = vertex.z - vMin.z;
	fTmp /= vSize.z;
	iTmp = (s32)(fTmp * 65536.0f);

	if(iTmp < 0)
		iTmp = 0;
	else if(iTmp > 65535)
		iTmp = 65535;
	iZ = (u16)iTmp;
}


void
DecompressVertex(Vector3 & vertex, const u16 & iX, const u16 & iY, const u16 & iZ, const Vector3 & vMin, const Vector3 & vSize)
{
	static const float fRecip65536 = ((float)(1.0 / 65536.0));

	// unpack into 0..1 range
	vertex.x = (((float)iX) * fRecip65536);
	vertex.y = (((float)iY) * fRecip65536);
	vertex.z = (((float)iZ) * fRecip65536);

	// scale by the total extents of the block of sectors
	vertex.x *= vSize.x;
	vertex.y *= vSize.y;
	vertex.z *= vSize.z;

	// offset by mins of entire block
	vertex.x += vMin.x;
	vertex.y += vMin.y;
	vertex.z += vMin.z;
}

// NOTES: The direction is in range 0..254 inclusive.  255 is reserved to indicate 360 degree cover.
u8
FindCoverDirFromVector(const Vector3 & Dir)
{
	u8 iDir = (u8) (atan2(-Dir.x, Dir.y) * (255.0f / (2.0f * PI)));
	if(iDir >= 255)
	{
		iDir = 0;
	}
	return iDir;
}

//////////////////////////////////////////////////////
// FUNCTION: FindDirFromVector
// PURPOSE:  Given a direction this function will return the vector to go with it.
// NOTES: The direction is in range 0..254 inclusive.  255 is reserved to indicate 360 degree cover.
//////////////////////////////////////////////////////

Vector3
FindVectorFromCoverDir(u8 Dir)
{
	Assert(Dir != 255);

	Vector3	Result;
	
	Result.z = 0;
	float	Angle = Dir * (2.0f * PI / 255.0f);

	Result.x = -rage::Sinf(Angle);
	Result.y = rage::Cosf(Angle);

	return Result;
}







//******************************************************************************************************
//	Find the closeset navmesh edgepoly satisfying the conditions

Vector3 g_vNavGenGetPolyEdgeMins(0.0f,0.0f,0.0f);
Vector3 g_vNavGenGetPolyEdgeMaxs(0.0f,0.0f,0.0f);
Vector3 g_vNavGenGetPolyEdgeInputPos(0.0f,0.0f,0.0f);
Vector3 g_vNavGenGetPolyEdge[NAVMESHPOLY_MAX_NUM_VERTICES];
float g_fNavGenGetPolyEdgeClosestDistSqr = FLT_MAX;
float g_fNavGenGetPolyEdgeMaxDistSqr = 0.0f;
u32 g_iNavGenGetPolyEdgeBestPoly = NAVMESH_POLY_INDEX_NONE;

TNavMeshPoly * UtilGetClosestNavMeshPolyEdge(
	CNavMesh * pNavMesh, const Vector3 & vPos, const float fMaxDistToLook,
	Vector3 & UNUSED_PARAM(vOutClosestPosOnEdge),
	bool bOnlyWater, bool bNotWater)
{
	g_vNavGenGetPolyEdgeMins = Vector3(vPos.x-fMaxDistToLook, vPos.y-fMaxDistToLook, vPos.z-fMaxDistToLook);
	g_vNavGenGetPolyEdgeMaxs = Vector3(vPos.x+fMaxDistToLook, vPos.y+fMaxDistToLook, vPos.z+fMaxDistToLook);

	TShortMinMax minMax;
	minMax.SetFloat(g_vNavGenGetPolyEdgeMins.x, g_vNavGenGetPolyEdgeMins.y, g_vNavGenGetPolyEdgeMins.z, g_vNavGenGetPolyEdgeMaxs.x, g_vNavGenGetPolyEdgeMaxs.y, g_vNavGenGetPolyEdgeMaxs.z);

	g_vNavGenGetPolyEdgeInputPos = vPos;
	g_fNavGenGetPolyEdgeClosestDistSqr = FLT_MAX;
	g_fNavGenGetPolyEdgeMaxDistSqr = fMaxDistToLook*fMaxDistToLook;
	g_iNavGenGetPolyEdgeBestPoly = NAVMESH_POLY_INDEX_NONE;

	UtilGetClosestNavMeshPolyEdgeR(pNavMesh, minMax, pNavMesh->GetQuadTree(), bOnlyWater, bNotWater);

	if(g_iNavGenGetPolyEdgeBestPoly == NAVMESH_POLY_INDEX_NONE)
	{
		return NULL;
	}

	TNavMeshPoly * pPoly = pNavMesh->GetPoly(g_iNavGenGetPolyEdgeBestPoly);
	return pPoly;
}

void UtilGetClosestNavMeshPolyEdgeR(CNavMesh * pNavMesh, const TShortMinMax & minMax, CNavMeshQuadTree * pTree, bool bOnlyWater, bool bNotWater)
{
	if(pTree->m_pLeafData)
	{
		s32 iNumPolys=pTree->m_pLeafData->m_iNumPolys;
		u16 * pPolyIndices=pTree->m_pLeafData->m_Polys;
		float fDistSqr;

		for(s32 p=0; p<iNumPolys; p++)
		{
			u16 iIndex = pPolyIndices[p];
			TNavMeshPoly * pPoly = pNavMesh->GetPoly(iIndex);

			if(bOnlyWater && !pPoly->GetIsWater())
				continue;

			if(bNotWater && pPoly->GetIsWater())
				continue;

			if(!pPoly->m_MinMax.Intersects(minMax))
				continue;

			u32 v;
			for(v=0; v<pPoly->GetNumVertices(); v++)
			{
				pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, v), g_vNavGenGetPolyEdge[v] );
			}

			s32 lastv = pPoly->GetNumVertices()-1;
			for(v=0; v<pPoly->GetNumVertices(); v++)
			{
				fDistSqr = geomDistances::Distance2SegToPoint(g_vNavGenGetPolyEdge[lastv], g_vNavGenGetPolyEdge[v] - g_vNavGenGetPolyEdge[lastv], g_vNavGenGetPolyEdgeInputPos);

				if(fDistSqr < g_fNavGenGetPolyEdgeMaxDistSqr && fDistSqr < g_fNavGenGetPolyEdgeClosestDistSqr)
				{
					g_fNavGenGetPolyEdgeClosestDistSqr = fDistSqr;
					g_iNavGenGetPolyEdgeBestPoly = iIndex;
				}

				lastv = v;
			}

		}

		return;
	}
	else
	{
		for(s32 c=0; c<4; c++)
		{
			CNavMeshQuadTree * pChild = pTree->m_pChildren[c];
			if(g_vNavGenGetPolyEdgeMins.x > pChild->m_Maxs.x || g_vNavGenGetPolyEdgeMins.y > pChild->m_Maxs.y ||
				pChild->m_Mins.x > g_vNavGenGetPolyEdgeMaxs.x || pChild->m_Mins.y > g_vNavGenGetPolyEdgeMaxs.y)
			{

			}
			else
			{
				UtilGetClosestNavMeshPolyEdgeR(pNavMesh, minMax, pChild, bOnlyWater, bNotWater);
			}
		}
	}
}


// crc table
const u32 g_CRCTable[256] =
{
	0x00000000, 0x77073096, 0xEE0E612C, 0x990951BA,
	0x076DC419, 0x706AF48F, 0xE963A535, 0x9E6495A3,
	0x0EDB8832, 0x79DCB8A4, 0xE0D5E91E, 0x97D2D988,
	0x09B64C2B, 0x7EB17CBD, 0xE7B82D07, 0x90BF1D91,
	0x1DB71064, 0x6AB020F2, 0xF3B97148, 0x84BE41DE,
	0x1ADAD47D, 0x6DDDE4EB, 0xF4D4B551, 0x83D385C7,
	0x136C9856, 0x646BA8C0, 0xFD62F97A, 0x8A65C9EC,
	0x14015C4F, 0x63066CD9, 0xFA0F3D63, 0x8D080DF5,
	0x3B6E20C8, 0x4C69105E, 0xD56041E4, 0xA2677172,
	0x3C03E4D1, 0x4B04D447, 0xD20D85FD, 0xA50AB56B,
	0x35B5A8FA, 0x42B2986C, 0xDBBBC9D6, 0xACBCF940,
	0x32D86CE3, 0x45DF5C75, 0xDCD60DCF, 0xABD13D59,
	0x26D930AC, 0x51DE003A, 0xC8D75180, 0xBFD06116,
	0x21B4F4B5, 0x56B3C423, 0xCFBA9599, 0xB8BDA50F,
	0x2802B89E, 0x5F058808, 0xC60CD9B2, 0xB10BE924,
	0x2F6F7C87, 0x58684C11, 0xC1611DAB, 0xB6662D3D,

	0x76DC4190, 0x01DB7106, 0x98D220BC, 0xEFD5102A,
	0x71B18589, 0x06B6B51F, 0x9FBFE4A5, 0xE8B8D433,
	0x7807C9A2, 0x0F00F934, 0x9609A88E, 0xE10E9818,
	0x7F6A0DBB, 0x086D3D2D, 0x91646C97, 0xE6635C01,
	0x6B6B51F4, 0x1C6C6162, 0x856530D8, 0xF262004E,
	0x6C0695ED, 0x1B01A57B, 0x8208F4C1, 0xF50FC457,
	0x65B0D9C6, 0x12B7E950, 0x8BBEB8EA, 0xFCB9887C,
	0x62DD1DDF, 0x15DA2D49, 0x8CD37CF3, 0xFBD44C65,
	0x4DB26158, 0x3AB551CE, 0xA3BC0074, 0xD4BB30E2,
	0x4ADFA541, 0x3DD895D7, 0xA4D1C46D, 0xD3D6F4FB,
	0x4369E96A, 0x346ED9FC, 0xAD678846, 0xDA60B8D0,
	0x44042D73, 0x33031DE5, 0xAA0A4C5F, 0xDD0D7CC9,
	0x5005713C, 0x270241AA, 0xBE0B1010, 0xC90C2086,
	0x5768B525, 0x206F85B3, 0xB966D409, 0xCE61E49F,
	0x5EDEF90E, 0x29D9C998, 0xB0D09822, 0xC7D7A8B4,
	0x59B33D17, 0x2EB40D81, 0xB7BD5C3B, 0xC0BA6CAD,

	0xEDB88320, 0x9ABFB3B6, 0x03B6E20C, 0x74B1D29A,
	0xEAD54739, 0x9DD277AF, 0x04DB2615, 0x73DC1683,
	0xE3630B12, 0x94643B84, 0x0D6D6A3E, 0x7A6A5AA8,
	0xE40ECF0B, 0x9309FF9D, 0x0A00AE27, 0x7D079EB1,
	0xF00F9344, 0x8708A3D2, 0x1E01F268, 0x6906C2FE,
	0xF762575D, 0x806567CB, 0x196C3671, 0x6E6B06E7,
	0xFED41B76, 0x89D32BE0, 0x10DA7A5A, 0x67DD4ACC,
	0xF9B9DF6F, 0x8EBEEFF9, 0x17B7BE43, 0x60B08ED5,
	0xD6D6A3E8, 0xA1D1937E, 0x38D8C2C4, 0x4FDFF252,
	0xD1BB67F1, 0xA6BC5767, 0x3FB506DD, 0x48B2364B,
	0xD80D2BDA, 0xAF0A1B4C, 0x36034AF6, 0x41047A60,
	0xDF60EFC3, 0xA867DF55, 0x316E8EEF, 0x4669BE79,
	0xCB61B38C, 0xBC66831A, 0x256FD2A0, 0x5268E236,
	0xCC0C7795, 0xBB0B4703, 0x220216B9, 0x5505262F,
	0xC5BA3BBE, 0xB2BD0B28, 0x2BB45A92, 0x5CB36A04,
	0xC2D7FFA7, 0xB5D0CF31, 0x2CD99E8B, 0x5BDEAE1D,

	0x9B64C2B0, 0xEC63F226, 0x756AA39C, 0x026D930A,
	0x9C0906A9, 0xEB0E363F, 0x72076785, 0x05005713,
	0x95BF4A82, 0xE2B87A14, 0x7BB12BAE, 0x0CB61B38,
	0x92D28E9B, 0xE5D5BE0D, 0x7CDCEFB7, 0x0BDBDF21,
	0x86D3D2D4, 0xF1D4E242, 0x68DDB3F8, 0x1FDA836E,
	0x81BE16CD, 0xF6B9265B, 0x6FB077E1, 0x18B74777,
	0x88085AE6, 0xFF0F6A70, 0x66063BCA, 0x11010B5C,
	0x8F659EFF, 0xF862AE69, 0x616BFFD3, 0x166CCF45,
	0xA00AE278, 0xD70DD2EE, 0x4E048354, 0x3903B3C2,
	0xA7672661, 0xD06016F7, 0x4969474D, 0x3E6E77DB,
	0xAED16A4A, 0xD9D65ADC, 0x40DF0B66, 0x37D83BF0,
	0xA9BCAE53, 0xDEBB9EC5, 0x47B2CF7F, 0x30B5FFE9,
	0xBDBDF21C, 0xCABAC28A, 0x53B39330, 0x24B4A3A6,
	0xBAD03605, 0xCDD70693, 0x54DE5729, 0x23D967BF,
	0xB3667A2E, 0xC4614AB8, 0x5D681B02, 0x2A6F2B94,
	0xB40BBE37, 0xC30C8EA1, 0x5A05DF1B, 0x2D02EF8D,
};

// Calculates the CRC for a .tri file - this is the CRC of the file, excluding the header & the timestamp.
//u32 CalcTriFileCRC(char * pFullFileName)/
//{
//	return CalcFileCRC(pFullFileName, 12);	// skip 8-byte header "NAVTRI3\0" or similar, and then the 4-byte timestamp
//}

// Calculates the CRC for a file, first skipping the specified num bytes
u32 CalcFileCRC(char * pFullFileName, s32 iSkipThisManyLeadingBytes)
{
	FILE * pFile = fopen(pFullFileName, "rb");	// File opened in binary mode
	if(!pFile) return 0;

	fseek(pFile, 0, SEEK_END);
	int iNumBytes = ftell(pFile);
	fseek(pFile, 0, SEEK_SET);
	u8 * pBuffer = rage_new u8[iNumBytes];
	size_t iBytesRead = fread(pBuffer, 1, iNumBytes, pFile);
	fclose(pFile);

	if(iBytesRead < (size_t)iSkipThisManyLeadingBytes)
	{
		delete[] pBuffer;
		return 0;
	}

	u8 * pData = &pBuffer[iSkipThisManyLeadingBytes];

	//*******************************************************************************

	u32 iCrc = 0xFFFFFFFF;

	while(iBytesRead)
	{
		CalcCrc(*pData, iCrc);
		pData++;

		iBytesRead--;
	}

	iCrc = ~iCrc;

	delete[] pBuffer;

	return iCrc;
}


bool ReadSrfFileCRCs(char * pFullFileName, u32 & iCollisionCRC, u32 & iResAreasCRC)
{
	iCollisionCRC = 0;
	iResAreasCRC = 0;

	FILE * pFile = fopen(pFullFileName, "rb");
	if(!pFile)
		return false;

	char header[] = "NAVSRF2\0";
	char fileHeader[8];

	fread(fileHeader, 1, 8, pFile);
	for(int c=0; c<8; c++)
	{
		if(header[c] != fileHeader[c])
		{
			Assert(0);
			fclose(pFile);
			return false;
		}
	}

	fread(&iCollisionCRC, 4, 1, pFile);
	fread(&iResAreasCRC, 4, 1, pFile);

	fclose(pFile);

	return true;
}

// Calculate the CRC of the navmesh resolution areas which intersect the given area
u32 CalcResAreasCRC(vector<CNavResolutionArea*> & areas, const Vector3 & vAreaMin, const Vector3 & vAreaMax)
{
	u32 iCRC=0;

	for(u32 r=0; r<areas.size(); r++)
	{
		CNavResolutionArea * pArea = areas[r];
		if(BBoxIntersect(pArea->m_vMin, pArea->m_vMax, vAreaMin, vAreaMax))
		{
			CalcCrc(pArea->m_vMin, iCRC);
			CalcCrc(pArea->m_vMax, iCRC);
			CalcCrc(pArea->m_fMultiplier, iCRC);
			CalcCrc(pArea->m_iFlags, iCRC);
		}
	}
	return iCRC;
}

//****************************************************************************8

CBitArray::CBitArray() : m_Bits(NULL), m_NumBytes(0), m_NumBits(0)
{

}
CBitArray::~CBitArray()
{
    if(m_Bits)
	{
		delete[] m_Bits;
		m_Bits = NULL;
	}
}
bool CBitArray::Alloc(int iNumBits)
{
	if(iNumBits> m_NumBits)
	{
		if(m_Bits)
		{
			delete[] m_Bits;
			m_Bits = NULL;
		}
		m_NumBits = iNumBits;
		int iBytes = (iNumBits / 8) +1;
		m_Bits = new char[iBytes];
		m_NumBytes = iBytes;
		Clear();
		return true;
	}
	return false;
}
void CBitArray::Clear()
{
	Assert(m_Bits);
	Assert(m_NumBytes>=0);
	memset(m_Bits, 0, (unsigned int)m_NumBytes);
}
void CBitArray::SetAll()
{
	Assert(m_Bits);
	Assert(m_NumBytes>=0);
	memset(m_Bits, 255, (unsigned int)m_NumBytes);
}

void ProjectBoundingBoxOntoZPlane(
	const Vector3 & vBoxMinIn,
	const Vector3 & vBoxMaxIn,
	const Matrix34 & orientationMat,
	const float fPlaneZ,
	float & fTopZOut,
	float & fBottomZOut,
	Vector3 * pCornersOut,	 // array of 4 Vector3's
	Vector4 * pEdgePlanes,	// array of 4 Vector4's
	TShortMinMax & minMax)
{
	//********************************************************	   
	// Project onto z=fPlaneZ plane

	int v;

	Vector3 vBoxMin = vBoxMinIn;
	Vector3 vBoxMax = vBoxMaxIn;

	float	LengthXProj = (orientationMat.a.x * orientationMat.a.x + orientationMat.a.y * orientationMat.a.y);
	float	LengthYProj = (orientationMat.b.x * orientationMat.b.x + orientationMat.b.y * orientationMat.b.y);
	float	LengthZProj = (orientationMat.c.x * orientationMat.c.x + orientationMat.c.y * orientationMat.c.y);
			
	float	SizeBBX = (vBoxMax.x - vBoxMin.x) * 0.5f;
	float	SizeBBY = (vBoxMax.y - vBoxMin.y) * 0.5f;
	float	SizeBBZ = (vBoxMax.z - vBoxMin.z) * 0.5f;
	
	// Get the centre of the bbox in local space.
	Vector3 vLocalCentre = (vBoxMax + vBoxMin) * 0.5f;
	// Adjust min/max so that centre is at the origin (in case the model is off-centre)
	vBoxMin -= vLocalCentre;
	vBoxMax -= vLocalCentre;
	// Transform to entity's origin
	Vector3 vWorldCentre = orientationMat * vLocalCentre;
	// Find the offset from the matrix's origin to the local-origin in worldspace
	// This offset will be non-zero, if the entity's origin was off-centre
	Vector3 vCentreOffset = vWorldCentre - orientationMat.d;
	// Rotate the offset.  We'll add this onto the final corner points at the end.
	orientationMat.Transform3x3(vLocalCentre);


	// Bounding-box is now at entity's origin, and is symmetrical around this.  The code below relies upon the
	// symmetry to calculate the 2d projection to 4 points.  After this is done, we can move the projected
	// points back by the rotated offset which the bbmin/bbmax was from (0,0,0)
	float	TotalSizeX = LengthXProj * SizeBBX * 2.0f;
	float	TotalSizeY = LengthYProj * SizeBBY * 2.0f;
	float	TotalSizeZ = LengthZProj * SizeBBZ * 2.0f;

	float		OffsetLength1, OffsetLength2, OffsetLength;
	float		OffsetWidth1, OffsetWidth2, OffsetWidth;
	Vector3		MainVec, MainVecPerp;
	Vector3		HigherPoint, LowerPoint;

	if (TotalSizeX > TotalSizeY && TotalSizeX > TotalSizeZ)
	{
		// x-axis is the main one
		MainVec = Vector3(orientationMat.a.x, orientationMat.a.y, 0.0f);

		HigherPoint = vWorldCentre + SizeBBX * MainVec;
		LowerPoint = vWorldCentre - SizeBBX * MainVec;
		MainVec.Normalize();

		OffsetLength1 = SizeBBY * (MainVec.y * orientationMat.b.y   +   MainVec.x * orientationMat.b.x);
		OffsetWidth1 =  SizeBBY * (MainVec.x * orientationMat.b.y   -   MainVec.y * orientationMat.b.x);
		OffsetLength2 = SizeBBZ * (MainVec.y * orientationMat.c.y   +   MainVec.x * orientationMat.c.x);
		OffsetWidth2 =  SizeBBZ * (MainVec.x * orientationMat.c.y   -   MainVec.y * orientationMat.c.x);
	}
	else if (TotalSizeY > TotalSizeZ)
	{
		// y-axis is the main one
		MainVec = Vector3(orientationMat.b.x, orientationMat.b.y, 0.0f);

		HigherPoint = vWorldCentre + SizeBBY * MainVec;
		LowerPoint = vWorldCentre - SizeBBY * MainVec;
		MainVec.Normalize();

		OffsetLength1 = SizeBBX * (MainVec.y * orientationMat.a.y   +   MainVec.x * orientationMat.a.x);
		OffsetWidth1 =  SizeBBX * (MainVec.x * orientationMat.a.y   -   MainVec.y * orientationMat.a.x);
		OffsetLength2 = SizeBBZ * (MainVec.y * orientationMat.c.y   +   MainVec.x * orientationMat.c.x);
		OffsetWidth2 =  SizeBBZ * (MainVec.x * orientationMat.c.y   -   MainVec.y * orientationMat.c.x);
	}
	else
	{
		// z-axis is the main one
		MainVec = Vector3(orientationMat.c.x, orientationMat.c.y, 0.0f);

		HigherPoint = vWorldCentre + SizeBBZ * MainVec;
		LowerPoint = vWorldCentre - SizeBBZ * MainVec;
		MainVec.Normalize();

		OffsetLength1 = SizeBBX * (MainVec.y * orientationMat.a.y   +   MainVec.x * orientationMat.a.x);
		OffsetWidth1 =  SizeBBX * (MainVec.x * orientationMat.a.y   -   MainVec.y * orientationMat.a.x);
		OffsetLength2 = SizeBBY * (MainVec.y * orientationMat.b.y   +   MainVec.x * orientationMat.b.x);
		OffsetWidth2 =  SizeBBY * (MainVec.x * orientationMat.b.y   -   MainVec.y * orientationMat.b.x);
	}

	MainVecPerp = Vector3(MainVec.y, -MainVec.x, 0.0f);

	OffsetLength = ABS(OffsetLength1) + ABS(OffsetLength2);
	OffsetWidth = ABS(OffsetWidth1) + ABS(OffsetWidth2);

	// JB : correct ordering now - front, left, rear & right sides of the entity
	pCornersOut[0] = HigherPoint + OffsetLength * MainVec - OffsetWidth * MainVecPerp;
	pCornersOut[1] = LowerPoint - OffsetLength * MainVec - OffsetWidth * MainVecPerp;
	pCornersOut[2] = LowerPoint - OffsetLength * MainVec + OffsetWidth * MainVecPerp;
	pCornersOut[3] = HigherPoint + OffsetLength * MainVec + OffsetWidth * MainVecPerp;				

	pCornersOut[0].z = pCornersOut[1].z = pCornersOut[2].z = pCornersOut[3].z = fPlaneZ;

	// Find the top-most & bottom-most Z values

	Vector3 vCorners[8] =
	{
		Vector3(vBoxMin.x, vBoxMin.y, vBoxMin.z),
		Vector3(vBoxMax.x, vBoxMin.y, vBoxMin.z),
		Vector3(vBoxMax.x, vBoxMax.y, vBoxMin.z),
		Vector3(vBoxMin.x, vBoxMax.y, vBoxMin.z),
		Vector3(vBoxMin.x, vBoxMin.y, vBoxMax.z),
		Vector3(vBoxMax.x, vBoxMin.y, vBoxMax.z),
		Vector3(vBoxMax.x, vBoxMax.y, vBoxMax.z),
		Vector3(vBoxMin.x, vBoxMax.y, vBoxMax.z)
	};
	fTopZOut = -99999.0f;
	fBottomZOut = 99999.0f;
	for(v=0; v<8; v++)
	{
		orientationMat.Transform(vCorners[v]);
		vCorners[v] += vCentreOffset;

		if(vCorners[v].z < fBottomZOut) fBottomZOut = vCorners[v].z;
		if(vCorners[v].z > fTopZOut) fTopZOut = vCorners[v].z;
	}

	Vector3 vCenter = (vCorners[0] + vCorners[1] + vCorners[2] + vCorners[3]) * 0.25f;

	if(fTopZOut < fBottomZOut)
		fTopZOut = fBottomZOut;

	int lastv = 3;
	for(v=0; v<4; v++)
	{
		Vector3 vEdge = pCornersOut[v] - pCornersOut[lastv];
		vEdge.Normalize();
		Vector3 vNormal = CrossProduct(vEdge, ZAXIS);
		vNormal.z = 0.0f;
		vNormal.Normalize();
		pEdgePlanes[v].SetVector3(vNormal);
		pEdgePlanes[v].w = - pEdgePlanes[v].Dot3(pCornersOut[v]);

		ASSERT_ONLY( const float d = pEdgePlanes[v].Dot3(vCenter) + pEdgePlanes[v].w; )
		Assert( d <= 0.0f );
		Assert( pEdgePlanes[v].Mag32() > SMALL_FLOAT );

		lastv = v;
	}

	//-----------------------------------------------------------


	Vector3 vWldMin(FLT_MAX, FLT_MAX, FLT_MAX);
	Vector3 vWldMax(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	fBottomZOut -= 0.2f;
	vWldMin.z = Min(fTopZOut, fBottomZOut);
	vWldMax.z = Max(fTopZOut, fBottomZOut);
	for(int v=0; v<4; v++)
	{
		vWldMin.x = Min(vWldMin.x, pCornersOut[v].x);
		vWldMin.y = Min(vWldMin.y, pCornersOut[v].y);
		vWldMax.x = Max(vWldMax.x, pCornersOut[v].x);
		vWldMax.y = Max(vWldMax.y, pCornersOut[v].y);
	}

	vWldMin.x = Max(vWldMin.x, -MINMAX_MAX_FLOAT_VAL);
	vWldMin.y = Max(vWldMin.y, -MINMAX_MAX_FLOAT_VAL);
	vWldMin.z = Max(vWldMin.z, -MINMAX_MAX_FLOAT_VAL);
	vWldMax.x = Min(vWldMax.x, MINMAX_MAX_FLOAT_VAL);
	vWldMax.y = Min(vWldMax.y, MINMAX_MAX_FLOAT_VAL);
	vWldMax.z = Min(vWldMax.z, MINMAX_MAX_FLOAT_VAL);

	minMax.Set(vWldMin, vWldMax);
}


bool DynamicObjectIntersectsPolygon(fwDynamicEntityInfo * pEntityInfo, int iNumPolyVerts, Vector3 * pPolyVerts, Vector4 * vPolyEdgePlanes)
{
	int v, p;

	// Classify all polygon points to the object
	// Iff all points are outside any plane we early out
	for(p=0; p<4; p++)
	{
		for(v=0; v<iNumPolyVerts; v++)
		{
			if(GetPlaneDist(pEntityInfo->m_Planes[p], pPolyVerts[v]) < 0.0f)
				break;
		}
		if(v == iNumPolyVerts)
			return false;
	}

	// Classify all object corners to the polygon planes
	// Iff all points are outside any plane we early out
	for(p=0; p<iNumPolyVerts; p++)
	{
		for(v=0; v<4; v++)
		{
			if(GetPlaneDist(vPolyEdgePlanes[p], pEntityInfo->m_vCorners[v]) < 0.0f)
				break;
		}
		if(v == 4)
			return false;
	}

	return true;
}


Vector3 GetCenter(const Vector3 & vMinAABB, const Vector3 & vMaxAABB, const Matrix34 & mat)
{
	Vector3 vCorners[] =
	{
		Vector3( vMinAABB.x, vMinAABB.y, vMinAABB.z ),
		Vector3( vMaxAABB.x, vMinAABB.y, vMinAABB.z ),
		Vector3( vMinAABB.x, vMaxAABB.y, vMinAABB.z ),
		Vector3( vMaxAABB.x, vMaxAABB.y, vMinAABB.z ),
		Vector3( vMinAABB.x, vMinAABB.y, vMaxAABB.z ),
		Vector3( vMaxAABB.x, vMinAABB.y, vMaxAABB.z ),
		Vector3( vMinAABB.x, vMaxAABB.y, vMaxAABB.z ),
		Vector3( vMaxAABB.x, vMaxAABB.y, vMaxAABB.z )
	};
	Vector3 vOrigin(0.0f, 0.0f, 0.0f);
	for(s32 i=0; i<8; i++)
	{
		mat.Transform(vCorners[i]);
		vOrigin += vCorners[i];
	}
	vOrigin /= 8.0f;
	return vOrigin;
}

}	// namespace rage
