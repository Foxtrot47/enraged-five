#include "NavGen.h"
#include "config.h"
#include "NavGen_Utils.h"
#include "NavGen_Store.h"
#include "ai/navmesh/navmeshextents.h"
#include "parser/manager.h"
#include "baseexportcollision.h"

namespace rage
{

extern bool g_bAerialNavigation;

char tmpBuf[256];

const float CNavGen::m_fTriangulationGetNodeEps = 0.25f;	// was 0.125f
const bool CNavGen::ms_bCheckForSuddenHeightChangesUnderTriEdges = true;
const float CNavGen::ms_fHeightChangesUnderTriEdges_HeightToTest = 10.0f;	// could be less!

vector<CNavResolutionArea*> CNavGen::m_ResolutionAreasWholeMap;
const float CNavResolutionArea::ms_fMaxResolutionMultiplier = 0.25f;

vector<CNavCustomCoverpoint*> CNavGen::m_CustomCoverpointsWholeMap;

//int g_iTotalNumEdgesInExistence = 0;
bool g_bProcessInBackground = false;
bool g_bOnlyNamedFiles = false;
char * g_pBlankLine("\r                                                                               \r");

#define ERROR_CHECK_NODE_TRI_ASSOCIATIONS

//***********************************************************************************
// AssociateTriAndNode
// This function adds a CNavSurfaceTri to the node's list if triangles which use it
//***********************************************************************************

void
AssociateTriAndNode(CNavSurfaceTri * pTri, CNavGenNode * pNode)
{
#ifdef ERROR_CHECK_NODE_TRI_ASSOCIATIONS
	u32 i;
	for(i=0; i<pNode->m_TrianglesUsingThisNode.size(); i++)
	{
		CNavSurfaceTri * pExistingTri = pNode->m_TrianglesUsingThisNode[i];
		if(pExistingTri == pTri)
		{
			return;
		}
	}
#endif

	pNode->m_TrianglesUsingThisNode.push_back(pTri);
}

//***********************************************************************************
// AssociateTriAndNode
// This function removes a CNavSurfaceTri to the node's list if triangles which use it
//***********************************************************************************

void
DisassociateTriAndNode(CNavSurfaceTri * pTri, CNavGenNode * pNode)
{
	u32 i;
	for(i=0; i<pNode->m_TrianglesUsingThisNode.size(); i++)
	{
		CNavSurfaceTri * pExistingTri = pNode->m_TrianglesUsingThisNode[i];
		if(pExistingTri == pTri)
		{
			pNode->m_TrianglesUsingThisNode.erase(pNode->m_TrianglesUsingThisNode.begin()+i);

#ifndef ERROR_CHECK_NODE_TRI_ASSOCIATIONS
			return;
#endif
			i--;
		}
	}
}





//***************************************************************************************************
//	CNavGenParams
//	This class holds the parameters for the generation process
//***************************************************************************************************

CNavGenParams::CNavGenParams()
{
	const fwNavGenConfig& cfg = fwNavGenConfig::Get();

	m_fMaxAngleForPolyToBeInNavMesh				= cfg.m_MaxAngleForPolyToBeInNavMesh;
	m_fMaxAngleForWaterPolyToBeInNavMesh		= cfg.m_MaxAngleForWaterPolyToBeInNavMesh;
	m_fAngleForNavMeshPolyToBeTooSteep			= cfg.m_AngleForNavMeshPolyToBeTooSteep;

	m_vUpVector = Vector3(0, 0, 1.0f);
	m_bAlsoStoreSplitPolysInOctreeLeaves = false;

	m_iDesiredNumTrisPerLeaf					= 64;
	m_fMinimumLeafNodeSize						= 32.0f;
	m_fDistanceToInitiallySeedNodes				= 1.0f;

	m_fMaxTriangleSideLength =	20.0f;	//12.0f; //8.0f;
	m_fMinTriangleAngle =		8.0f;	//12.0f;
	m_fMinTriangleArea =		0.02f;	//0.05f;
	m_fMaxTriangleArea =		120.0f;	//80.0f;

	m_iMaxNumberOfTrianglesSurroundingAnyNode = 15;	//12;

	//***********************************************************************************
	// m_TriangulationMaxHeightDiff				= 0.35f;
	// This initial value of 0.35 worked great for GTA:SA
	// The only reason I may be tempted to increase it, is to help level-designers with
	// stairs, until the artists get round to marking them up with SURFACE_TYPE_STAIRS.
	//***********************************************************************************
	
	//********************************************************************************************
	// m_TriangulationMaxHeightDiff
	// The maximum z-difference (+ or -) between any adjacent nodes for them to be triangulatable
	//********************************************************************************************

	m_TriangulationMaxHeightDiff				= 0.4f;		//0.32f;	//0.3f;		//0.25f;	// Was 0.35f

	m_fMinZDistBetweenSamples = 2.0f;

	//********************************************************************************************
	// m_HeightAboveNodeBaseAtWhichToTriangulate
	// How high above the sampled position should we triangulate the navmesh surface
	//********************************************************************************************

	m_HeightAboveNodeBaseAtWhichToTriangulate	= 0.35f;

	//********************************************************************************************
	// This is the height which must be clear of obstructions above the triangulated navmesh
	// Basically it is the height of a ped, except under special circumstances (dynamic navmeshes)
	// where we may with to lower it to allow peds to pass under low obstructions on boats, etc.
	//********************************************************************************************

	m_fTestClearHeightInTriangulation			= cfg.m_TestClearHeightInTriangulation;
	Assert(m_fTestClearHeightInTriangulation >= 0.0f);

	//********************************************************************************************
	// m_MaxHeightChangeUnderTriangleEdge
	// The maximum sudden change in height of the collision mesh underneath any two vertices of
	// a triangle which is about to be triangulated
	//********************************************************************************************

	m_MaxHeightChangeUnderTriangleEdge			= cfg.m_MaxHeightChangeUnderTriangleEdge;	//CNavGen::ms_MaxHeightChangeUnderTriangleEdge;		//0.25f;	// Was 0.35f

	m_OptimizeMaxQuadricErrorMetric			= cfg.m_OptimizeMaxQuadricErrorMetric;

	//********************************************************************************************
	//	Whether to try a number of jittered samples when placing surface points, helps combat gaps
	//********************************************************************************************
	m_bJitterSamplesWhenCreatingNodes = false;
	
	m_bCreateNavmeshUnderwater = false;

	m_fDistToExtrudeCollisionTris				= 1.0f;

	m_bAerialNavigation = false;

	m_bDontDoOptimisation = false;
	m_bDontDoPolygonMerging = false;
	m_bMoveNodesAwayFromWalls = true;
	m_bFixJaggiesAfterTriangulation = true;

	m_bTagPavementNavMesh = false;
}

CNavGenParams::~CNavGenParams(void)
{

}


void CNavGenParams::Init(
	const Vector3 & vWorldMin, const Vector3 & vWorldMax,
	const Vector3 & vUpVector,
	const Vector3 & vNavMeshMin, const Vector3 & vNavMeshMax,
	bool bStoreSplitPolysInOctreeLeaves,	// should usually be false for efficiency
	float fSampleResolution)
{
	*this = CNavGenParams();

	m_vAreaMin = vWorldMin;
	m_vAreaMax = vWorldMax;
	m_vUpVector = vUpVector;
	m_vSectorMins = vNavMeshMin;
	m_vSectorMaxs = vNavMeshMax;
	m_bAlsoStoreSplitPolysInOctreeLeaves = bStoreSplitPolysInOctreeLeaves;
	m_fDistanceToInitiallySeedNodes = fSampleResolution;
	m_TriangulationMaxHeightDiff *= fSampleResolution;
}


//***************************************************************************************************
//	CNavGen
//***************************************************************************************************

s32 CNavGen::ms_iVerbosity = 0;
TDbgOutFnPtr CNavGen::m_pOutputTextFn = NULL;

CNavGen::CNavGen(void)
{
	m_bUseGeometryCuller = true;
	m_iInitialPassNumNodesCreated = 0;
	m_iInitialPassNumTrianglesCreated = 0;
	m_iFixJaggiesNumTrianglesCreated = 0;

	m_iTriangulationPass = 0;

	m_bTriEdgesForCollapsingHaveBeenRandomised = false;
	m_bRandomisedEdgeListsBeforeOptimisation = false;
	m_iNumEdgesCollapsedThisPass = 0;
	m_bNavSurfaceHasBeenOptimised = false;

	m_bDontMoveNonConnectedFaces = true;
	m_bDontOptimiseMeshEdges = false;//true;

	m_iCurrentNodeIndexToAssign = 0;

	m_EdgeCollapseNumIterations = 100;

	m_pNodeMap = NULL;

	m_bHasWater = false;
	m_fWaterLevel = 0.0f;
	m_pWaterSamples = NULL;
	m_fWaterSamplingStepSize = 0;
	m_iNumWaterSamples = 0;

	m_iNumResAreas = 0;

	m_iTotalNumEdgesInExistence = false;
	m_bDoWeCareAboutSurfaceTypes = true;

	m_iLoadNavSurfaceBinary_MaxVertexIndex = 0;
	m_iLoadNavSurfaceBinary_MaxVertexPoolSize = 0;

	m_bRemoveTrisBehindWalls = false;
	m_bIgnoreSteepness = false;
	m_bDoBehindWallTestDuringTriangulation = false;
	m_bJitterSamplesForPlacement = true;
	m_bJitterTestsUnderLine = true;
	m_bCalculateSpaceAboveAllPolys = false;

	m_fTestClearHeightInTriangulation = 0.0f;
}

CNavGen::~CNavGen(void)
{
	Shutdown();
}


bool CNavGen::Init(CNavGenParams * pParams)
{
	memcpy(&m_Params, pParams, sizeof(CNavGenParams));

	m_Params.m_fMinTriangleSideLength = 0.2f;
//	static const float fTriSideAreaMult = 1.75f;
//	float fMinArea = CalcTriArea(m_Params.m_fMinTriangleSideLength * fTriSideAreaMult, m_Params.m_fMinTriangleSideLength * fTriSideAreaMult, m_Params.m_fMinTriangleSideLength * fTriSideAreaMult);
//	m_Params.m_fMinTriangleArea = Min(fMinArea, 0.05f);

	m_Octree.m_bAlsoStoreSplitPolysInOctreeLeaves = pParams->m_bAlsoStoreSplitPolysInOctreeLeaves;
	m_Octree.m_fDesiredLeafNodeSize = pParams->m_fMinimumLeafNodeSize;
	m_Octree.m_iDesiredNumTrisInEachLeaf = pParams->m_iDesiredNumTrisPerLeaf;

	m_bNavSurfaceHasBeenOptimised = false;
	m_bTriEdgesForCollapsingHaveBeenRandomised = false;

	return true;
}

bool CNavGen::InitOctree()
{
	// Firstly create an octree of the collision polygons
	return m_Octree.Init(m_Params.m_vSectorMins, m_Params.m_vSectorMaxs, &m_CollisionTriangles);
}

// Close the generator
bool
CNavGen::Shutdown(void)
{
	// Clear all data associated with the nav-mesh.  Needs the octree to operate.
//	CleanUpDataAfterOptimisingNavMesh();

	// Clear all the triangles
	m_CollisionTriangles.clear();

	
	// Shutdown the octree.  Also deletes pathnodes, and nav-tris in the leaves. (not any more)
	m_Octree.Shutdown();


	u32 n;
	for(n=0; n<m_NavSurfaceNodes.size(); n++)
	{
		CNavGenNode * pNode = m_NavSurfaceNodes[n];
		delete pNode;
	}
	m_NavSurfaceNodes.clear();

	for(u32 t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];
		delete pTri;
	}
	m_NavSurfaceTriangles.clear();

	u32 e;
	for(e=0; e<m_NavTriEdges.size(); e++)
	{
		CNavTriEdge * pEdge = m_NavTriEdges[e];
		delete pEdge;
	}
	m_NavTriEdges.clear();


	if(m_pNodeMap)
	{
		delete m_pNodeMap;
		m_pNodeMap = NULL;
	}

	if(m_pWaterSamples)
	{
		delete[] m_pWaterSamples;
		m_pWaterSamples = NULL;
	}
	for(u32 sp=0; sp<m_SlicePlanes.size(); sp++)
	{
		delete m_SlicePlanes[sp];
	}

	return true;
}






bool
CNavGen::IsWalkable(CNavGenTri * pTri)
{
	if(!pTri->m_colPolyData.GetIsWalkable())
		return false;

	if(pTri->m_colPolyData.GetIsFixedObject())
		return false;

	float fDot = DotProduct(m_Params.m_vUpVector, pTri->m_vNormal);
	float fRads = m_Params.m_fMaxAngleForPolyToBeInNavMesh * DtoR;

	// If surface is downwards-facing then it is NEVER walkable!
	// Most other surfaces are only walkable if withing some 'steepness' angle of the
	// up vector, unless IsSurfaceTypeAllowedToBeSteep() returns true (stairs, escalators
	// etc are allowed to be steeped than other surfaces).

	bool bSurfCanBeSteep = pTri->m_colPolyData.GetIsStairs();

	if(fDot < 0.0f || ((!bSurfCanBeSteep) && (fDot < Cosf(fRads))))
	{
		return false;
	}

	return true;
}


bool CNavGen::IsTooSteepForNormalMovement(const Vector3 & vNormal)
{
	if(m_bIgnoreSteepness)
		return false;

	float fDot = DotProduct(m_Params.m_vUpVector, vNormal);
	float fRads = m_Params.m_fAngleForNavMeshPolyToBeTooSteep * DtoR;

	if(fDot < 0.0f || (fDot < Cosf(fRads)) )
	{
		return true;
	}

	return false;
}


void
CNavGen::FlagWalkableTris(void)
{
	if(g_bAerialNavigation)
	{
		// Probably don't need/want any of this, if building aerial navmeshes.
		return;
	}

	s32 t;
	for(t=0; t<m_CollisionTriangles.size(); t++)
	{
		CNavGenTri * pTri = &m_CollisionTriangles[t];

		if(IsWalkable(pTri) || pTri->m_colPolyData.GetIsRiverBound())
		{
			pTri->m_iFlags |= NAVGENTRI_WALKABLE;
		}
		else
		{
			pTri->m_iFlags &= ~NAVGENTRI_WALKABLE;
		}

		if(IsTooSteepForNormalMovement(pTri->m_vNormal) && !pTri->m_colPolyData.GetIsStairs() && !pTri->m_colPolyData.GetIsStairSlopeBound())
		{
			pTri->m_iFlags |= NAVGENTRI_TOOSTEEP;
		}
		else
		{
			pTri->m_iFlags &= ~NAVGENTRI_TOOSTEEP;
		}

		if(pTri->m_colPolyData.GetIsPavement())
		{
			pTri->m_iFlags |= NAVGENTRI_PAVEMENT;
		}

		if(pTri->m_colPolyData.GetIsStairs())
		{
			pTri->m_iFlags |= NAVGENTRI_STAIRS;
		}

		if(pTri->m_colPolyData.GetIsSeeThrough())
		{
			pTri->m_iFlags |= NAVGENTRI_SEETHROUGH;
		}

		if(pTri->m_colPolyData.GetIsShootThrough())
		{
			pTri->m_iFlags |= NAVGENTRI_SHOOTTHROUGH;
		}
	}
}


void CNavGen::CheckForDuplicateNavSurfaceTriangle(CNavSurfaceTri * pTri)
{
	for(u32 t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pLeafTri = m_NavSurfaceTriangles[t];
		if(pTri == pLeafTri)
			continue;

		int iNumSame = 0;
		u32 v1,v2;

		for(v1=0; v1<3; v1++)
		{
			for(v2=0; v2<3; v2++)
			{
				if(pTri->m_Nodes[v1] == pLeafTri->m_Nodes[v2])
					iNumSame++;
			}
		}

		if(iNumSame >= 3)
		{
			printf("ERROR - found duplicate triangles!\n");
			OutputDebugString("ERROR - found duplicate triangles!\n");

			sprintf(tmpBuf, "Tri1 0x%p : (%.2f, %.2f, %.2f) (%.2f, %.2f, %.2f) (%.2f, %.2f, %.2f)\n",
				pTri,
				pTri->m_Nodes[0]->m_vBasePos.x, pTri->m_Nodes[0]->m_vBasePos.y, pTri->m_Nodes[0]->m_vBasePos.z,
				pTri->m_Nodes[1]->m_vBasePos.x, pTri->m_Nodes[1]->m_vBasePos.y, pTri->m_Nodes[1]->m_vBasePos.z,
				pTri->m_Nodes[2]->m_vBasePos.x, pTri->m_Nodes[2]->m_vBasePos.y, pTri->m_Nodes[2]->m_vBasePos.z
			);
			printf(tmpBuf);
			OutputDebugString(tmpBuf);

			sprintf(tmpBuf, "Adjacencies : 0x%p, 0x%p, 0x%p\n", pTri->m_AdjacentTris[0], pTri->m_AdjacentTris[1], pTri->m_AdjacentTris[2]);
			printf(tmpBuf);
			OutputDebugString(tmpBuf);

			sprintf(tmpBuf, "Tri2 0x%p : (%.2f, %.2f, %.2f) (%.2f, %.2f, %.2f) (%.2f, %.2f, %.2f)\n",
				pLeafTri,
				pLeafTri->m_Nodes[0]->m_vBasePos.x, pLeafTri->m_Nodes[0]->m_vBasePos.y, pLeafTri->m_Nodes[0]->m_vBasePos.z,
				pLeafTri->m_Nodes[1]->m_vBasePos.x, pLeafTri->m_Nodes[1]->m_vBasePos.y, pLeafTri->m_Nodes[1]->m_vBasePos.z,
				pLeafTri->m_Nodes[2]->m_vBasePos.x, pLeafTri->m_Nodes[2]->m_vBasePos.y, pLeafTri->m_Nodes[2]->m_vBasePos.z
			);
			printf(tmpBuf);
			OutputDebugString(tmpBuf);

			sprintf(tmpBuf, "Adjacencies : 0x%p, 0x%p, 0x%p\n", pLeafTri->m_AdjacentTris[0], pLeafTri->m_AdjacentTris[1], pLeafTri->m_AdjacentTris[2]);
			printf(tmpBuf);
			OutputDebugString(tmpBuf);

			pLeafTri->m_iFlags |= NAVSURFTRI_BUGRIDDEN;

//				DebugBreak();
		}
	}

	return;
}












CNavSurfaceTri *
CNavGen::PickNavSurfaceTriangle(const Vector3 & vRayStart, const Vector3 & vRayEnd)
{
	Vector3 vTriPts[3];
	Vector3 vPlaneNormal;
	float fPlaneDist;
	Vector3 vEdge1, vEdge2;
	Vector3 vIntersectPos;
	m_pPickedNavSurfaceTri = NULL;
	m_fClosestNavSurfaceTriDist = FLT_MAX;
	
	u32 t;
	for(t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];
		if(pTri->IsRemoved())
			continue;

		vTriPts[0] = pTri->m_Nodes[0]->m_vBasePos;
		vTriPts[1] = pTri->m_Nodes[1]->m_vBasePos;
		vTriPts[2] = pTri->m_Nodes[2]->m_vBasePos;

		vTriPts[0].z += m_Params.m_HeightAboveNodeBaseAtWhichToTriangulate;
		vTriPts[1].z += m_Params.m_HeightAboveNodeBaseAtWhichToTriangulate;
		vTriPts[2].z += m_Params.m_HeightAboveNodeBaseAtWhichToTriangulate;

		// Calculate the triangle plane
		vEdge1 = vTriPts[1] - vTriPts[0];
		vEdge1.Normalize();
		vEdge2 = vTriPts[2] - vTriPts[0];
		vEdge2.Normalize();

		vPlaneNormal = CrossProduct(vEdge2, vEdge1);
		vPlaneNormal.Normalize();
		fPlaneDist = - DotProduct(vPlaneNormal, vTriPts[0]);

		bool bHit = RayIntersectsTriangle(
			vRayStart,
			vRayEnd,
			&vTriPts[0],
			vIntersectPos
		);

		if(bHit)
		{
			pTri->m_iFlags |= NAVSURFTRI_PATHFIND_PATHTRI;

			m_iNumNavSurfaceTrisHit++;

			float fDist = (vIntersectPos - vRayStart).Mag();
			if(fDist < m_fClosestNavSurfaceTriDist)
			{
				m_fClosestNavSurfaceTriDist = fDist;
				m_pPickedNavSurfaceTri = pTri;
			}
		}
	}

	return m_pPickedNavSurfaceTri;
}



//*****************************************************************************
//	This function chops away all geometry outside of the m_vWorldChopMin and
//	m_vWorldChopMax.  This is done to the collision triangles, before the
//	octree is made of them.  This is to ensure that no two blocks of game
//	sectors ever overlaps, and that they abutt exactly.  This is very important
//	for later we will come to connect adjacent block together, and they must
//	line up exactly and never have duplicate nodes/surfaces in two blocks..
//*****************************************************************************
/*
void CNavGen::SliceAwayGeometry(const Vector3 & vMins, const Vector3 & vMaxs)
{
	int p;
	u32 t;

	vector<CSplitPoly*> polyList1;
	vector<CSplitPoly*> polyList2;

	vector<CSplitPoly*> * inputTriList = &polyList1;
	vector<CSplitPoly*> * outputTriList = &polyList2;

	for(t=0; t<m_CollisionTriangles.size(); t++)
	{
		CNavGenTri * pTri = &m_CollisionTriangles[t];
		CSplitPoly * pPoly = rage_new CSplitPoly();

		pPoly->m_Pts.PushAndGrow(pTri->m_vPts[0]);
		pPoly->m_Pts.PushAndGrow(pTri->m_vPts[1]);
		pPoly->m_Pts.PushAndGrow(pTri->m_vPts[2]);

		pPoly->m_pParentTriangle = NULL;
		pPoly->m_PlaneSideFlags = 0;

		pPoly->m_colPolyData = pTri->m_colPolyData;
		pPoly->m_iBakedInDataBitField = pTri->m_iBakedInDataBitField;

//		pPoly->m_iSurfaceType = pTri->m_iSurfaceType;

		inputTriList->push_back(pPoly);

		delete pTri;
	}

	m_CollisionTriangles.clear();
	
	//*********************************************************************
	// The cutting planes
	// Each of these planes points outwards away from the sector geometry
	// which we wish to preseve
	//*********************************************************************
	Vector3 vPlaneNormals[4] =
	{
		Vector3(1.0f, 0, 0),		// left edge
		Vector3(-1.0f, 0, 0),		// right edge
		Vector3(0, -1.0f, 0),		// top edge
		Vector3(0, 1.0f, 0)		// bottom edge
	};

	float fPlaneDists[4];
	fPlaneDists[0] = - DotProduct(vPlaneNormals[0], vMins);
	fPlaneDists[1] = - DotProduct(vPlaneNormals[1], vMaxs);
	fPlaneDists[2] = - DotProduct(vPlaneNormals[2], vMaxs);
	fPlaneDists[3] = - DotProduct(vPlaneNormals[3], vMins);
	
	for(p=0; p<4; p++)
	{
		for(t=0; t<inputTriList->size(); t++)
		{
			CSplitPoly * pPoly = (*inputTriList)[t];
			CSplitPoly * pFrontFragment = NULL;
			CSplitPoly * pBackFragment = NULL;

			//SplitPolyToAxialPlane(pPoly, iPlaneFlags[p], fPlaneDists[p], &pFrontFragment, &pBackFragment);
			SplitPolyToPlane(pPoly, vPlaneNormals[p], fPlaneDists[p], 0.0f, &pFrontFragment, &pBackFragment);

			if(pFrontFragment)
			{
				outputTriList->push_back(pFrontFragment);
			}
			if(pBackFragment)
			{
				delete pBackFragment;
			}

			delete pPoly;
		}

		// If this isn't the final plane, then swap our lists
		if(p != 3)
		{
			if(inputTriList == &polyList1)
			{
				inputTriList = &polyList2;
				outputTriList = &polyList1;
			}
			else
			{
				inputTriList = &polyList1;
				outputTriList = &polyList2;
			}
			outputTriList->clear();
		}
	}

	// Now we can go through our output list's fragments, and create
	// a new set of CNavGenTri's with which to start the main procssing.

	s32 v, lastv;
	Vector3 edge1, edge2;

	for(t=0; t<outputTriList->size(); t++)
	{
		CSplitPoly * pPoly = (*outputTriList)[t];

		lastv = 1;

		for(v=2; v<pPoly->m_Pts.GetCount(); v++)
		{
			CNavGenTri * pTri = rage_new CNavGenTri;

			pTri->m_vPts[0] = pPoly->m_Pts[0];
			pTri->m_vPts[1] = pPoly->m_Pts[lastv];
			pTri->m_vPts[2] = pPoly->m_Pts[v];

			pTri->m_iIndexInCollisionPolysList = m_CollisionTriangles.size();
			pTri->m_iFlags = 0;
			pTri->m_iTimeStamp = 0;

			//pTri->m_iSurfaceType = pPoly->m_iSurfaceType;
			pTri->m_colPolyData = pPoly->m_colPolyData;
			pTri->m_iBakedInDataBitField = pPoly->m_iBakedInDataBitField;

			edge1 = pTri->m_vPts[1] - pTri->m_vPts[0];
			edge2 = pTri->m_vPts[2] - pTri->m_vPts[0];
			pTri->m_vNormal = CrossProduct(edge2, edge1);
			pTri->m_vNormal.Normalize();

			pTri->m_fPlaneDist = - DotProduct(pTri->m_vNormal, pTri->m_vPts[0]);

			m_CollisionTriangles.push_back(pTri);

			lastv = v;
		}

		delete pPoly;
	}
}
*/






//*************************************************************
//	Sanity-check as much as possible.
//	This needs to have been called after data with the REMOVED
//	flag has actually been removed.
//*************************************************************


#ifdef NAVGEN_SPEW_DEBUG

char sanityCheckTmpBuf[256];

void
CNavGen::SanityCheckEverything(void)
{
	u32 t,v;
	u32 e,n;
	float l0, l1, l2, fPlaneDist, fDot;
	Vector3 edge1, edge2, edge3, vPlaneNormal;

	for(t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];

		// Make sure that this triangle has no exactly same triangle anywhere
		// NB : this is gonna take AGES but is really necessary...
//			SanityCheck_MakeSureNoDuplicateTris(pTri, m_Octree.RootNode());

		// Check it's not been removed
		if(pTri->IsRemoved())
		{
			continue;

//				sprintf(sanityCheckTmpBuf, "ERROR - NavSurfaceTri 0x%p was removed\n", pTri);
//				OutputDebugString(sanityCheckTmpBuf);
//				DebuggerBreak();
			//.._ASSERTE(0);
		}

		// Mark all tris
		pTri->m_iFlags |= NAVSURFTRI_ISMARKED_YELLOW;

		if(m_bNavSurfaceHasBeenOptimised)
		{
			// Check that every triangle has had every edge processed for edge-creation
			if((pTri->m_AdjacentTris[0] && !(pTri->m_iFlags & NAVSURFTRI_EDGE_0_TO_1_CACHED)) ||
				(pTri->m_AdjacentTris[1] && !(pTri->m_iFlags & NAVSURFTRI_EDGE_1_TO_2_CACHED)) ||
				(pTri->m_AdjacentTris[2] && !(pTri->m_iFlags & NAVSURFTRI_EDGE_2_TO_0_CACHED)))
			{
				sprintf(sanityCheckTmpBuf, "ERROR - not all edges of NavSurfaceTri 0x%p were processed for creating edges\n", pTri);
				OutputDebugString(sanityCheckTmpBuf);
				DebuggerBreak();
				//_ASSERTE(0);
			}
		}
		// Make sure we've got 3 nodes !
		if(!pTri->m_Nodes[0] || !pTri->m_Nodes[1] || !pTri->m_Nodes[2])
		{
			pTri->m_iFlags |= NAVSURFTRI_BUGRIDDEN;
			sprintf(sanityCheckTmpBuf, "ERROR - tri 0x%p doesn't have 3 nodes!\n", pTri);
			OutputDebugString(sanityCheckTmpBuf);
			DebuggerBreak();
			//_ASSERTE(0);
		}

		if(pTri->m_Nodes[0] == pTri->m_Nodes[1] || pTri->m_Nodes[1] == pTri->m_Nodes[2] || pTri->m_Nodes[2] == pTri->m_Nodes[0])
		{
			pTri->m_iFlags |= NAVSURFTRI_BUGRIDDEN;
			sprintf(sanityCheckTmpBuf, "ERROR - tri 0x%p is using a node more than once|!\n", pTri);
			OutputDebugString(sanityCheckTmpBuf);
			DebuggerBreak();
			//_ASSERTE(0);
		}

		// Make sure that none of our nodes are removed
		for(v=0; v<3; v++)
		{
			if(pTri->m_Nodes[v]->IsRemoved())
			{
				pTri->m_iFlags |= NAVSURFTRI_BUGRIDDEN;
				sprintf(sanityCheckTmpBuf, "ERROR - node %i of tri 0x%p was removed\n", v, pTri);
				OutputDebugString(sanityCheckTmpBuf);
				DebuggerBreak();
			}
		}
		// Make sure the surface is pointing upwards
		edge1 = pTri->m_Nodes[1]->m_vBasePos - pTri->m_Nodes[0]->m_vBasePos;//*v1 - *v0;
		edge2 = pTri->m_Nodes[2]->m_vBasePos - pTri->m_Nodes[0]->m_vBasePos;//*v2 - *v0;
		edge3 = pTri->m_Nodes[1]->m_vBasePos - pTri->m_Nodes[2]->m_vBasePos;//*v2 - *v0;

		l0 = edge1.Mag();
		edge1.Normalize();

		l1 = edge2.Mag();
		edge2.Normalize();

		l2 = edge3.Mag();
		edge3.Normalize();

		vPlaneNormal = CrossProduct(edge2, edge1);
		vPlaneNormal.Normalize();

		fPlaneDist = - DotProduct(vPlaneNormal, pTri->m_Nodes[0]->m_vBasePos);
		fDot = DotProduct(vPlaneNormal, Vector3(0, 0, 1.0f));

		if(fDot < 0.0f)
		{
			pTri->m_iFlags |= NAVSURFTRI_BUGRIDDEN;
			sprintf(sanityCheckTmpBuf, "ERROR - tri 0x%p normal isn't pointing upwards.\n", pTri);
			OutputDebugString(sanityCheckTmpBuf);
			DebuggerBreak();

			pTri->m_iFlags |= NAVSURFTRI_ISMARKED_RED;
		}

		// Check that we don't link to an adjacent triangle more than once..
		if(pTri->m_AdjacentTris[0] != NULL && pTri->m_AdjacentTris[1] != NULL)
		{
			if(pTri->m_AdjacentTris[0] == pTri->m_AdjacentTris[1])
			{
				pTri->m_iFlags |= NAVSURFTRI_BUGRIDDEN;
				sprintf(sanityCheckTmpBuf, "ERROR - tri 0x%p adjacent tri 0x%p linked to more than once (0 and 1)..\n", pTri, pTri->m_AdjacentTris[0]);
				OutputDebugString(sanityCheckTmpBuf);
				DebuggerBreak();
			}
		}
		// Check that we don't link to an adjacent triangle more than once..
		if(pTri->m_AdjacentTris[1] != NULL && pTri->m_AdjacentTris[2] != NULL)
		{
			if(pTri->m_AdjacentTris[1] == pTri->m_AdjacentTris[2])
			{
				pTri->m_iFlags |= NAVSURFTRI_BUGRIDDEN;
				sprintf(sanityCheckTmpBuf, "ERROR - tri 0x%p adjacent tri 0x%p linked to more than once (1 and 2)..\n", pTri, pTri->m_AdjacentTris[1]);
				OutputDebugString(sanityCheckTmpBuf);
				DebuggerBreak();
			}
		}
		// Check that we don't link to an adjacent triangle more than once..
		if(pTri->m_AdjacentTris[2] != NULL && pTri->m_AdjacentTris[0] != NULL)
		{
			if(pTri->m_AdjacentTris[2] == pTri->m_AdjacentTris[0])
			{
				pTri->m_iFlags |= NAVSURFTRI_BUGRIDDEN;
				sprintf(sanityCheckTmpBuf, "ERROR - tri 0x%p adjacent tri 0x%p linked to more than once (2 and 0)..\n", pTri, pTri->m_AdjacentTris[2]);
				OutputDebugString(sanityCheckTmpBuf);
				DebuggerBreak();
			}
		}

		// Check the adjacent triangles
		for(e=0; e<3; e++)
		{
			if(pTri->m_AdjacentTris[e])
			{
				// Make sure that none of the adjacent triangles are removed!
				if(pTri->m_AdjacentTris[e]->IsRemoved())
				{
					pTri->m_iFlags |= NAVSURFTRI_BUGRIDDEN;
					sprintf(tmpBuf, "ERROR - adjacent tri %i was removed\n", e);
					OutputDebugString(tmpBuf);
					DebuggerBreak();
					//_ASSERTE(0);
				}
				// Make sure that the adjacent face links back to this face!!
				if(pTri->m_AdjacentTris[e]->m_AdjacentTris[0] != pTri &&
					pTri->m_AdjacentTris[e]->m_AdjacentTris[1] != pTri &&
					pTri->m_AdjacentTris[e]->m_AdjacentTris[2] != pTri)
				{
					pTri->m_iFlags |= NAVSURFTRI_BUGRIDDEN;
					OutputDebugString("ERROR - adjacent tris don't link back to tri\n");
					DebuggerBreak();
					//_ASSERT(0);

					pTri->m_AdjacentTris[e]->m_iFlags |= NAVSURFTRI_ISMARKED_RED;
					//pTri->m_iFlags
				}
			}
		}
	}

	for(n=0; n<m_NavSurfaceNodes.size(); n++)
	{
		CNavGenNode * pNode = m_NavSurfaceNodes[n];
		if(pNode->IsRemoved())
		{
			continue;

//				OutputDebugString("ERROR - node was removed\n");
//				DebuggerBreak();
			//_ASSERTE(0);
		}
		// Make sure this node is within this leaf (nodes never move)
		/*
		if(pNode->m_vBasePos.x < vNodeMin.x || pNode->m_vBasePos.x > vNodeMax.x ||
			pNode->m_vBasePos.y < vNodeMin.y || pNode->m_vBasePos.y > vNodeMax.y ||
			pNode->m_vBasePos.z < vNodeMin.z || pNode->m_vBasePos.z > vNodeMax.z)
		{
			OutputDebugString("ERROR - node is outside of leaf bounds.\n");

			sprintf(tmpBuf, "node pos (%.2f, %.2f, %.2f)  leaf min (%.2f, %.2f, %.2f)  leaf max (%.2f, %.2f, %.2f)\n",
				pNode->m_vBasePos.x, pNode->m_vBasePos.y, pNode->m_vBasePos.z,
				vNodeMin.x, vNodeMin.y, vNodeMin.z,
				vNodeMax.x, vNodeMax.y, vNodeMax.z
			);
			OutputDebugString(tmpBuf);

			DebuggerBreak();
			//_ASSERTE(0);
		}
		*/
		// Check neighbouring faces

		// Check neighbouring edges
		for(e=0; e<pNode->m_EdgesUsingThisNode.size(); e++)
		{
			CNavTriEdge * pEdge = pNode->m_EdgesUsingThisNode[e];
			if(pEdge->IsRemoved())
			{
				continue;

//					OutputDebugString("ERROR - edge was removed.\n");
//					DebuggerBreak();
			}
		}
	}
	return;
}




void
CNavGen::SanityCheck_MakeSureNoDuplicateTris(CNavSurfaceTri * pTri)
{
	u32 t;
	int v1,v2,iNumSame;
	Vector3 * triVerts[3];
	Vector3 * leafTriVerts[3];

	triVerts[0] = &pTri->m_Nodes[0]->m_vBasePos;
	triVerts[1] = &pTri->m_Nodes[1]->m_vBasePos;
	triVerts[2] = &pTri->m_Nodes[2]->m_vBasePos;

	for(t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pLeafTri = m_NavSurfaceTriangles[t];
		if(pTri == pLeafTri)
			continue;

		leafTriVerts[0] = &pLeafTri->m_Nodes[0]->m_vBasePos;
		leafTriVerts[1] = &pLeafTri->m_Nodes[1]->m_vBasePos;
		leafTriVerts[2] = &pLeafTri->m_Nodes[2]->m_vBasePos;

		iNumSame = 0;
		float fEqEps = 0.1f;

		for(v1=0; v1<3; v1++)
		{
			for(v2=0; v2<3; v2++)
			{
//					if(triVerts[v1]->Eq(*leafTriVerts[v2], 0.1f))

				if(Eq(triVerts[v1]->x, leafTriVerts[v2]->x, fEqEps) &&
					Eq(triVerts[v1]->y, leafTriVerts[v2]->y, fEqEps) &&
					Eq(triVerts[v1]->z, leafTriVerts[v2]->z, fEqEps))
				{
					iNumSame++;
				}
			}
		}

		if(iNumSame >= 3)
		{
			OutputDebugString("Shite! Found a duplicate nav-tri!\n");
			DebuggerBreak();
		}
	}
}

#endif





void
CNavGen::CleanBadData(void)
{
	// Firstly mark them
	MarkBadTrianglesForRemoval();

	// then remove them
	//CleanUpRemovedEdgesNodesAndTris();
}


//******************************************************************
//
//	This function analyses the triangles, and if they're bad it
//	marks them with NAVSURFTRI_REMOVED.  It then calls tha function
//	RemoveAllReferencesToTriangle() which cleans up all traces of
//	the triangle.
//
//******************************************************************

void
CNavGen::MarkBadTrianglesForRemoval(void)
{
	for(u32 t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];

		if(pTri->IsRemoved())
		{
			continue;
		}

		//*****************************************************
		// If this triangle has zero area then we remove it
		//*****************************************************
		float fArea = CalcTriArea(
			(pTri->m_Nodes[0]->m_vBasePos - pTri->m_Nodes[1]->m_vBasePos).Mag(),
			(pTri->m_Nodes[1]->m_vBasePos - pTri->m_Nodes[2]->m_vBasePos).Mag(),
			(pTri->m_Nodes[2]->m_vBasePos - pTri->m_Nodes[0]->m_vBasePos).Mag()
		);
		if(fArea == 0.0f)
		{
			pTri->SetRemoved();
		}

		//******************************************************
		// If this triangle is adjacent twice to the same tri
		// then we remove it
		//*******************************************************
		if(pTri->m_AdjacentTris[0] != NULL && pTri->m_AdjacentTris[1] != NULL && (pTri->m_AdjacentTris[0] == pTri->m_AdjacentTris[1]))
		{
			pTri->SetRemoved();
		}
		else if(pTri->m_AdjacentTris[1] != NULL && pTri->m_AdjacentTris[2] != NULL && (pTri->m_AdjacentTris[1] == pTri->m_AdjacentTris[2]))
		{
			pTri->SetRemoved();
		}
		else if(pTri->m_AdjacentTris[2] != NULL && pTri->m_AdjacentTris[0] != NULL && (pTri->m_AdjacentTris[2] == pTri->m_AdjacentTris[0]))
		{
			pTri->SetRemoved();
		}


		//**********************************************************
		// If removed, then kill all references to it
		//**********************************************************
		if(pTri->IsRemoved())
		{
			RemoveAllReferencesToTriangle(pTri);
		}
	}
}


void
CNavGen::RemoveAllReferencesToTriangle(CNavSurfaceTri * pTri)
{
	for(u32 n=0; n<m_NavSurfaceNodes.size(); n++)
	{
		CNavGenNode * pNode = m_NavSurfaceNodes[n];
		RemoveTriFromTriList(pNode->m_TrianglesUsingThisNode, pTri);

		// We mark all edges using the triangle.  Later we will
		// remove those edges which are marked for removal.
		for(u32 e=0; e<pNode->m_EdgesUsingThisNode.size(); e++)
		{
			CNavTriEdge * pEdge = pNode->m_EdgesUsingThisNode[e];
			if(pEdge->m_pTri1 == pTri || pEdge->m_pTri2 == pTri)
			{
				pEdge->SetRemoved();
			}
		}
	}

	for(u32 t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pListTri = m_NavSurfaceTriangles[t];
		if(pListTri->m_AdjacentTris[0] == pTri)
			pListTri->m_AdjacentTris[0] = NULL;
		if(pListTri->m_AdjacentTris[1] == pTri)
			pListTri->m_AdjacentTris[1] = NULL;
		if(pListTri->m_AdjacentTris[2] == pTri)
			pListTri->m_AdjacentTris[2] = NULL;
	}

}


void
CNavGen::PrepareForSave(void)
{
	int numTris = 0;

	for(u32 t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pTri = m_NavSurfaceTriangles[t];
		if(!pTri->IsRemoved())
		{
			numTris++;

			for(int n=0; n<3; n++)
			{
				CNavGenNode * pNode = pTri->m_Nodes[n];
				if(pNode->m_iIndexOfThisNode == NODE_INDEX_UNASSIGNED)
				{
					pNode->m_iIndexOfThisNode =  m_iCurrentNodeIndexToAssign++;

					m_NodesForSaving.push_back(pNode);
				}
			}
		}
	}

	u32 iTotal = m_iTotalNumTris;
	iTotal += numTris;

	if(iTotal > 65535)
	{
		Assert("ERROR - 65535 tris exceeded.");
	}

	m_iTotalNumTris = (u16)iTotal;
}

//**********************************************************
//
//	This function saves the nav surface as a 3ds .asc file
//	The 3ds format used here is a 'bare-minimum' for our
//	needs.  The file format was taken from www.wotsit.org
//
//	The data in the 3ds format is in little-endian mode,
//	ie. the LSB of a data word/long-word comes first.
//	Since this will be running on little-endian PCs, we can
//	achieve this by using fwrite() to always write bytes
//
//**********************************************************

#define _3DS_MAIN3DS				0x4D4D
#define _3DS_EDIT3DS				0x3D3D
#define _3DS_OBJECTDEF				0x4000
#define _3DS_TRIANGLELISTOBJ		0x4100
#define _3DS_VERTEXLIST				0x4110
#define _3DS_TRANSLATIONMATRIX		0x4160
#define _3DS_FACELIST				0x4120

#define _3DS_CHUNK_HEADER_SIZE		6

void
WriteChunkID(FILE * pFile, u16 chunkID, u32 chunkSize)
{
	fwrite(&chunkID, 1, 2, pFile);
	fwrite(&chunkSize, 1, 4, pFile);
}

bool
ReadChunkID(FILE * pFile, u16 & chunkID, u32 & chunkSize)
{
	fread(&chunkID, 1, 2, pFile);
	if(feof(pFile))
		return false;

	fread(&chunkSize, 1, 4, pFile);
	if(feof(pFile))
		return false;

	return true;
}

#define NSB_FLAG_PAVEMENT				0x0001
#define NSB_FLAG_SHELTER				0x0002
#define NSB_FLAG_WATER					0x0004
#define NSB_FLAG_UNUSED1				0x0008
#define NSB_FLAG_INTERIOR				0x0010
#define NSB_FLAG_TOOSTEEP				0x0020
#define NSB_FLAG_INTERSECTING_ENTITIES	0x0040
#define NSB_FLAG_NO_NETWORK_SPAWN		0x0080
#define NSB_FLAG_DO_NOT_OPTIMISE		0x0100

bool CNavGen::SaveNavSurfaceBinary(char * pFilename, char * pMeshName, u32 iTriFileCRC, u32 iResAreasCRC)
{
	if(!pFilename || !*pFilename || !pMeshName || !*pMeshName)
		return false;

	if(!m_Octree.RootNode())
		return false;

	//**************************************************************************
	// Assign indices to all nodes in mesh - count how many vertices we'll have
	m_iCurrentNodeIndexToAssign = 0;
	m_iTotalNumTris = (u16) 0;

	PrepareForSave();

	Assert(m_iCurrentNodeIndexToAssign <= 65533);
	u32 iTotalNumVertices = (s32) m_iCurrentNodeIndexToAssign;


	// Open the file
	FILE * filePtr = fopen(pFilename, "wb");
	if(!filePtr)
		return false;

	// Write file header
	char header[] = "NAVSRF2\0";
	fwrite(header, 1, 8, filePtr);

	// Write the CRC of the tri file from which this nav surface was generated.
	// We'll use this to enable us to do partial rebuilds of navmeshes.
	fwrite(&iTriFileCRC, 4, 1, filePtr);

	// Write the CRC of the tri file from which this nav surface was generated.
	// We'll use this to enable us to do partial rebuilds of navmeshes.
	fwrite(&iResAreasCRC, 4, 1, filePtr);

	// Write a flags field
	u32 iFlags = 0;
	fwrite(&iFlags, 4, 1, filePtr);

	// Write num vertices
	fwrite(&iTotalNumVertices, 4, 1, filePtr);

	// Write num triangles
	fwrite(&m_iTotalNumTris, 4, 1, filePtr);

	u32 t,n;

	// Write the vertices
	Assert(iTotalNumVertices == m_NodesForSaving.size());
	for(n=0; n<m_NodesForSaving.size(); n++)
	{
		CNavGenNode * pNode = m_NodesForSaving[n];
		float x = pNode->m_vBasePos.x;
		float y = pNode->m_vBasePos.y;
		float z = pNode->m_vBasePos.z;

		fwrite(&x, 4, 1, filePtr);
		fwrite(&y, 4, 1, filePtr);
		fwrite(&z, 4, 1, filePtr);
	}

	// Write the triangles & the associated data
	int iNumTrisWritten=0;
	for(t=0; t<m_NavSurfaceTriangles.size(); t++)
	{
		CNavSurfaceTri * pNavTri = m_NavSurfaceTriangles[t];
		if(pNavTri->IsRemoved())
			continue;

		iNumTrisWritten++;

		u32 a = pNavTri->m_Nodes[0]->m_iIndexOfThisNode;
		u32 b = pNavTri->m_Nodes[1]->m_iIndexOfThisNode;
		u32 c = pNavTri->m_Nodes[2]->m_iIndexOfThisNode;

		// Write out in reverse winding order
		fwrite(&c, 4, 1, filePtr);
		fwrite(&b, 4, 1, filePtr);
		fwrite(&a, 4, 1, filePtr);

		// Get col-poly data
		u8 iPedDensity = (u8)pNavTri->m_colPolyData.GetPedDensity();

		// Ped density
		fwrite(&iPedDensity, 1, 1, filePtr);

		// Write baked-in data
		fwrite(&pNavTri->m_iBakedInBitField, 1, 1, filePtr);

		// clear space above tri
		fwrite(&pNavTri->m_iFreeSpaceTopZ, 2, 1, filePtr);

		// Write other flags :
		u32 iTriFlags = 0;

		if(pNavTri->m_colPolyData.GetIsPavement())
			iTriFlags |= NSB_FLAG_PAVEMENT;
		if(pNavTri->m_bIsSheltered)
			iTriFlags |= NSB_FLAG_SHELTER;
		if(pNavTri->m_bIsWater)
			iTriFlags |= NSB_FLAG_WATER;
		if(pNavTri->m_colPolyData.GetIsInterior())
			iTriFlags |= NSB_FLAG_INTERIOR;
		if(pNavTri->m_bIsTooSteep)
			iTriFlags |= NSB_FLAG_TOOSTEEP;
		if(pNavTri->m_bIntersectsEntities)
			iTriFlags |= NSB_FLAG_INTERSECTING_ENTITIES;
		if(pNavTri->m_colPolyData.GetIsNoNetworkSpawn())
			iTriFlags |= NSB_FLAG_NO_NETWORK_SPAWN;
		if(pNavTri->m_bDoNotOptimise)
			iTriFlags |= NSB_FLAG_DO_NOT_OPTIMISE;


		fwrite(&iTriFlags, 4, 1, filePtr);
	}
	Assert(iNumTrisWritten==(int)m_iTotalNumTris);

	fclose(filePtr);

	return true;
}

// NAME : Load3dsMaxAsciiMarkup
// PURPOSE : Reads in a 3dsMax ASE file and extracts markup for climbs, drops, etc
//
// This is in no way an attempt to make a good ASE file loader, it is
// just the minimal code to load in the data which we need.
//
// The name of each helper dummy object in the file is checked for starting with "climb" or "drop"
// The position, and the direction of the dummy's Z axis is used to generate adjacencies.
//

bool CNavGen::Load3dsMaxAsciiMarkup(char * pFilename, atArray<TAuthoredAdjacencyHintPos> & adjacencyPositions)
{
	char * pAseBuffer = NULL;

	struct CAseBuffer
	{
		CAseBuffer(char * p) { pBuffer = p; }
		~CAseBuffer() { delete[] pBuffer; }
		char * pBuffer;
	};

	// Open the file
	FILE * pFile = fopen(pFilename, "rt");
	if(!pFile) return false;

	// Read the contents
	fseek(pFile, 0, SEEK_END);
	int iNumBytes = ftell(pFile);
	fseek(pFile, 0, SEEK_SET);
	pAseBuffer = rage_new char[iNumBytes];
	/*size_t iBytesRead =*/ fread(pAseBuffer, 1, iNumBytes, pFile);
	fclose(pFile);

	// instance this struct, so as to delete the buffer when we leave this function
	CAseBuffer scoped(pAseBuffer);

	Vector3 vPos, vAxis;

	Matrix34 rotMat;
	rotMat.Identity();
	rotMat.RotateFullX(-PI/2.0f);

	//------------------------
	// Scan for climb objects

	char * pObjectPos = pAseBuffer;

	bool bQuit = false;
	while(!bQuit)
	{
		// Scan for the item named "climb"
		pObjectPos = strstr(pObjectPos, "*NODE_NAME \"climb");
		if(!pObjectPos)
			break;

		char * pAxisPos = strstr(pObjectPos, "*TM_ROW1");
		if(!pAxisPos)
			break;

		if( sscanf(pAxisPos, "*TM_ROW1 %f %f %f", &vAxis.x, &vAxis.y, &vAxis.z) != 3)
			break;

		char * pVecPos = strstr(pObjectPos, "*TM_POS");
		if(!pVecPos)
			break;

		if( sscanf(pVecPos, "*TM_POS %f %f %f", &vPos.x, &vPos.y, &vPos.z) != 3)
			break;

		pObjectPos = pVecPos;

		rotMat.Transform(vAxis);
		rotMat.Transform(vPos);

		TAuthoredAdjacencyHintPos climb;
		climb.m_vStartPosition = vPos;
		climb.m_vHeading = vAxis;
		climb.m_iType = TAuthoredAdjacencyHintPos::TYPE_CLIMB;

		adjacencyPositions.PushAndGrow(climb);
	}

	//------------------------
	// Scan for drop objects

	pObjectPos = pAseBuffer;

	bQuit = false;
	while(!bQuit)
	{
		// Scan for the item named "drop"
		pObjectPos = strstr(pObjectPos, "*NODE_NAME \"drop");
		if(!pObjectPos)
			break;

		char * pAxisPos = strstr(pObjectPos, "*TM_ROW1");
		if(!pAxisPos)
			break;

		if( sscanf(pAxisPos, "*TM_ROW1 %f %f %f", &vAxis.x, &vAxis.y, &vAxis.z) != 3)
			break;

		char * pVecPos = strstr(pObjectPos, "*TM_POS");
		if(!pVecPos)
			break;

		if( sscanf(pVecPos, "*TM_POS %f %f %f", &vPos.x, &vPos.y, &vPos.z) != 3)
			break;

		pObjectPos = pVecPos;

		rotMat.Transform(vAxis);
		rotMat.Transform(vPos);

		TAuthoredAdjacencyHintPos drop;
		drop.m_vStartPosition = vPos;
		drop.m_vHeading = vAxis;
		drop.m_iType = TAuthoredAdjacencyHintPos::TYPE_DROP;

		adjacencyPositions.PushAndGrow(drop);
	}

	return true;
}

// NAME : Load3dsMaxAsciiAsNavMesh
// PURPOSE : Reads in a 3dsMax ASE file as a navmesh
//
// This is in no way an attempt to make a good ASE file loader, it is
// just the minimal code to load in the data which we need.
//
// It is assumed that a geom object named "navmesh" exists in the file.
//

CNavMesh * CNavGen::Load3dsMaxAsciiAsNavMesh(char * pFilename)
{
	char * pAseBuffer = NULL;

	struct CAseBuffer
	{
		CAseBuffer(char * p) { pBuffer = p; }
		~CAseBuffer() { delete[] pBuffer; }
		char * pBuffer;
	};

	// Open the file
	FILE * pFile = fopen(pFilename, "rt");
	if(!pFile) return NULL;

	// Read the contents
	fseek(pFile, 0, SEEK_END);
	int iNumBytes = ftell(pFile);
	fseek(pFile, 0, SEEK_SET);
	pAseBuffer = rage_new char[iNumBytes];
	/*size_t iBytesRead =*/ fread(pAseBuffer, 1, iNumBytes, pFile);
	fclose(pFile);

	// instance this struct, so as to delete the buffer when we leave this function
	CAseBuffer scoped(pAseBuffer);

	// Scan for the item named "navmesh"
	char * pObjectPos = strstr(pAseBuffer, "*NODE_NAME \"navmesh\"");
	if(!pObjectPos)
		return NULL;

	char * pMeshPos = strstr(pObjectPos, "*MESH");
	if(!pMeshPos)
		return NULL;

	char * pNumVertices = strstr(pMeshPos, "*MESH_NUMVERTEX");
	if(!pNumVertices)
		return NULL;

	s32 iNumVertices;
	if( sscanf(pNumVertices, "*MESH_NUMVERTEX %i", &iNumVertices) != 1)
		return NULL;

	char * pNumFaces = strstr(pNumVertices, "*MESH_NUMFACES");
	if(!pNumFaces)
		return NULL;

	u32 iNumFaces;
	if( sscanf(pNumFaces, "*MESH_NUMFACES %i", &iNumFaces) != 1)
		return NULL;

	char * pStartVertices = pNumFaces;

	Matrix34 rotMat;
	rotMat.Identity();
	rotMat.RotateFullX(-PI/2.0f);

	Vector3 vMin(FLT_MAX, FLT_MAX, FLT_MAX);
	Vector3 vMax(-FLT_MAX, -FLT_MAX, -FLT_MAX);
	atArray<Vector3, 16> vertices;	// atArray?
	for(s32 v=0; v<iNumVertices; v++)
	{
		pStartVertices = strstr(pStartVertices, "*MESH_VERTEX ");
		if(!pStartVertices)
			return NULL;

		s32 iVert;
		Vector3 vert;
		if( sscanf(pStartVertices, "*MESH_VERTEX %i %f %f %f", &iVert, &vert.x, &vert.y, &vert.z) != 4)
			return NULL;

		rotMat.Transform(vert);

		vertices.PushAndGrow(vert);

		pStartVertices++;

		vMin.x = Min(vMin.x, vert.x);
		vMin.y = Min(vMin.y, vert.y);
		vMin.z = Min(vMin.z, vert.z);
		vMax.x = Max(vMax.x, vert.x);
		vMax.y = Max(vMax.y, vert.y);
		vMax.z = Max(vMax.z, vert.z);
	}

	char * pStartFaces = pStartVertices;
	pStartFaces = strstr(pStartFaces, "*MESH_FACE_LIST ");
	pStartFaces++;

	vector<s32> indices;
	for(u32 f=0; f<iNumFaces; f++)
	{
		pStartFaces = strstr(pStartFaces, "*MESH_FACE");
		if(!pStartFaces)
			return NULL;

		s32 iFace;
		s32 iFaceIndices[3];
		if( sscanf(pStartFaces, "*MESH_FACE %i: A: %i B: %i C: %i", &iFace, &iFaceIndices[0], &iFaceIndices[1], &iFaceIndices[2]) != 4)
			return NULL;

		indices.push_back(iFaceIndices[0]);
		indices.push_back(iFaceIndices[1]);
		indices.push_back(iFaceIndices[2]);

		pStartFaces++;
	}


	//----------------------------------------------------------------------

	u16 * pVertexData = rage_new u16[iNumVertices*3];
	TNavMeshPoly * pPolys = rage_new TNavMeshPoly[indices.size()/3];
	TAdjPoly * pAdjPolys = rage_new TAdjPoly[indices.size()];
	u16 * pVertexIndices = rage_new u16[indices.size()];

	u32 i;

	//------------------------
	// Create polygons array

	int iPoolIndex=0;
	//u32 iMaxVertexIndex=0;

	for(i=0; i<iNumFaces; i++)
	{
		pPolys[i].SetFlags(0);
		pPolys[i].SetNumVertices(3);
		pPolys[i].SetFirstVertexIndex(iPoolIndex);

		s32 a = indices[ (i*3) ];
		s32 b = indices[ (i*3) +1 ];
		s32 c = indices[ (i*3) +2 ];

		// Set up the poly
		pVertexIndices[iPoolIndex] = (u16)a;
		pAdjPolys[iPoolIndex].m_iAdjacentNavMeshIndex = NAVMESH_NAVMESH_INDEX_NONE;
		pAdjPolys[iPoolIndex].SetPolyIndex(NAVMESH_POLY_INDEX_NONE);
		pAdjPolys[iPoolIndex].SetAdjacencyType(ADJACENCY_TYPE_NORMAL);
		pAdjPolys[iPoolIndex].SetEdgeProvidesCover(false);
		iPoolIndex++;

		pVertexIndices[iPoolIndex] = (u16)b;
		pAdjPolys[iPoolIndex].m_iAdjacentNavMeshIndex = NAVMESH_NAVMESH_INDEX_NONE;
		pAdjPolys[iPoolIndex].SetPolyIndex(NAVMESH_POLY_INDEX_NONE);
		pAdjPolys[iPoolIndex].SetAdjacencyType(ADJACENCY_TYPE_NORMAL);
		pAdjPolys[iPoolIndex].SetEdgeProvidesCover(false);
		iPoolIndex++;

		pVertexIndices[iPoolIndex] = (u16)c;
		pAdjPolys[iPoolIndex].m_iAdjacentNavMeshIndex = NAVMESH_NAVMESH_INDEX_NONE;
		pAdjPolys[iPoolIndex].SetPolyIndex(NAVMESH_POLY_INDEX_NONE);
		pAdjPolys[iPoolIndex].SetAdjacencyType(ADJACENCY_TYPE_NORMAL);
		pAdjPolys[iPoolIndex].SetEdgeProvidesCover(false);
		iPoolIndex++;
	}

	//-------------------------
	// Now set up the navmesh

	CNavMesh * pNavMesh = rage_new CNavMesh();

	pNavMesh->m_iFlags = NAVMESH_COMPRESSED_VERTEX_DATA;
	pNavMesh->m_iNumVertices = iNumVertices;
	pNavMesh->m_iNumPolys = iNumFaces;

	pNavMesh->m_iIndexOfMesh = NAVMESH_INDEX_FIRST_DYNAMIC;
	pNavMesh->SetBuildID(0);


	s32 iVert=0;
	for(s32 v=0; v<iNumVertices; v++)
	{
		CompressVertex(vertices[v], pVertexData[iVert], pVertexData[iVert+1], pVertexData[iVert+2], vMin, vMax-vMin);
		iVert+=3;
	}

	pNavMesh->m_CompressedVertexArray = new aiSplitArray<CNavMeshCompressedVertex, NAVMESH_COMPRESSEDVERTEX_ARRAY_BLOCKSIZE>(iNumVertices, (CNavMeshCompressedVertex*)pVertexData);
	delete[] pVertexData;
	pVertexData = NULL;

	pNavMesh->m_PolysArray = rage_new aiSplitArray<TNavMeshPoly, NAVMESH_POLY_ARRAY_BLOCKSIZE>(iNumFaces, pPolys);
	pNavMesh->SetPolysSubArrayIndices();
	delete[] pPolys;
	pPolys = NULL;

	pNavMesh->m_iSizeOfPools = iNumFaces * 3;

	pNavMesh->m_VertexIndexArray = rage_new aiSplitArray<u16, NAVMESH_VERTEXINDEX_ARRAY_BLOCKSIZE>(pNavMesh->m_iSizeOfPools, pVertexIndices);
	delete[] pVertexIndices;
	pVertexIndices = NULL;

	pNavMesh->m_AdjacentPolysArray = rage_new aiSplitArray<TAdjPoly, NAVMESH_ADJPOLY_ARRAY_BLOCKSIZE>(pNavMesh->m_iSizeOfPools, pAdjPolys);
	delete[] pAdjPolys;
	pAdjPolys = NULL;

	pNavMesh->m_pQuadTree = rage_new CNavMeshQuadTree();
	pNavMesh->m_pQuadTree->m_Mins = vMin;
	pNavMesh->m_pQuadTree->m_Maxs = vMax;
	pNavMesh->m_pNonResourcedData->m_vMinOfNavMesh = pNavMesh->m_pQuadTree->m_Mins;

	pNavMesh->m_pQuadTree->m_pLeafData = rage_new TNavMeshQuadTreeLeafData;
	pNavMesh->m_pQuadTree->m_pLeafData->m_iNumPolys = 0;
	pNavMesh->m_pQuadTree->m_pLeafData->m_Polys = NULL;
	pNavMesh->m_pQuadTree->m_pLeafData->m_iNumCoverPoints = 0;
	pNavMesh->m_pQuadTree->m_pLeafData->m_CoverPoints = NULL;

	//vBlockMins = vMin;
	//vBlockMaxs = vMax;

	pNavMesh->m_vExtents.x = vMax.x - vMin.x;
	pNavMesh->m_vExtents.y = vMax.y - vMin.y;
	pNavMesh->m_vExtents.z = vMax.z - vMin.z;

	return pNavMesh;
}

//************************************************************************
//
// vBlockMins & vBlockMaxs are passed back by parameter
//
//************************************************************************

CNavMesh * CNavGen::LoadNavSurfaceBinaryAsNavMesh(char * pFileName, Vector3 & vBlockMins, Vector3 & vBlockMaxs, bool bMainMapNavMesh)
{
	Vector3 * pVertices;
	TNavMeshPoly * pPolys;
	TAdjPoly * pAdjPolys;
	u16 * pVertexIndices;
	int iNumVertices = 0;
	int iNumFaces = 0;
	int v;

	s32 iNavMeshXPos, iNavMeshYPos;
	TNavMeshIndex iNavMeshIndex;
	
	if(bMainMapNavMesh)
	{
		iNavMeshIndex = CNavGen_NavMeshStore::GetNavMeshIndexFromFilename(pFileName, &iNavMeshXPos, &iNavMeshYPos);
		if(iNavMeshIndex==NAVMESH_NAVMESH_INDEX_NONE)
			return NULL;
	}
	else
	{
		iNavMeshIndex = NAVMESH_INDEX_FIRST_DYNAMIC;
		iNavMeshXPos = 0;
		iNavMeshYPos = 0;
	}

	if(!LoadNavSurfaceBinary(pFileName, pVertices, pPolys, pAdjPolys, pVertexIndices, iNumVertices, iNumFaces))
	{
		return NULL;
	}
	if(!iNumVertices || !iNumFaces)
	{
		return NULL;
	}

	const float fCloseEps = SMALL_FLOAT; //0.05f;

	//***********************************************
	// Check that we don't have duplicated vertices

	s32 iNumIdentical=0;
	s32 v1,v2;
	for(v1=0; v1<iNumVertices; v1++)
	{
		for(v2=v1+1; v2<iNumVertices; v2++)
		{
			if(pVertices[v1].IsClose(pVertices[v2], fCloseEps))
				iNumIdentical++;
		}
	}
	//printf("Num Vertices Identical : %i\n", iNumIdentical);

	//*****************************************
	// Check that every single vertex is used

	CBitArray bits;
	bits.Alloc(iNumVertices);

	s32 f;
	for(f=0; f<iNumFaces; f++)
	{
		int i1 = pVertexIndices[f*3];
		int i2 = pVertexIndices[(f*3)+1];
		int i3 = pVertexIndices[(f*3)+2];
		bits.Set(i1);
		bits.Set(i2);
		bits.Set(i3);

		pPolys[f].SetNavMeshIndex(iNavMeshIndex);
	}
	for(v=0; v<iNumVertices; v++)
	{
		Assert(!bits.IsClear(v));
	}

	//************************************
	// Check that no faces are duplicates

	s32 f1,f2;

	for(f1=0; f1<iNumFaces; f1++)
	{
		for(f2=f1+1; f2<iNumFaces; f2++)
		{
			int f1_indices[3] = { pVertexIndices[f1*3], pVertexIndices[(f1*3)+1], pVertexIndices[(f1*3)+2] };
			int f2_indices[3] = { pVertexIndices[f2*3], pVertexIndices[(f2*3)+1], pVertexIndices[(f2*3)+2] };

			int iNumVerticesIdentical=0;
			int iNumIndicesIdentical=0;

			for(v1=0; v1<3; v1++)
			{
				int iIndex1 = f1_indices[v1];
				Vector3 vVertex1 = pVertices[iIndex1];

				for(v2=0; v2<3; v2++)
				{
					int iIndex2 = f2_indices[v2];
					Vector3 vVertex2 = pVertices[iIndex2];

					if(iIndex1==iIndex2)
						iNumIndicesIdentical++;

					if(vVertex1.IsClose(vVertex2, fCloseEps))
						iNumVerticesIdentical++;
				}
			}

			Assert(iNumVerticesIdentical < 3);
		}
	}

	//*************************
	// Calc the mins & maxs
	//*************************

	Vector3 vMin, vMax;
	vMin.x = FLT_MAX;
	vMin.y = FLT_MAX;
	vMin.z = FLT_MAX;
	vMax.x = -FLT_MAX;
	vMax.y = -FLT_MAX;
	vMax.z = -FLT_MAX;

	// For main map (outside), we get the min/max from the sectors & sector-size (extracted from the filename)
	if(iNavMeshIndex!=NAVMESH_INDEX_FIRST_DYNAMIC)
	{
		vMin.x = CPathServerExtents::GetSectorToWorldX(iNavMeshXPos);
		vMin.y = CPathServerExtents::GetSectorToWorldY(iNavMeshYPos);

		vMax.x = CPathServerExtents::GetSectorToWorldX(iNavMeshXPos) + CPathServerExtents::GetSizeOfNavMesh();
		vMax.y = CPathServerExtents::GetSectorToWorldY(iNavMeshYPos) + CPathServerExtents::GetSizeOfNavMesh();
	}
	else
	{
		// For non-map navmeshes which exist in local space, get the minmax from the vertex data
		for(v=0; v<iNumVertices; v++)
		{
			if(pVertices[v].x < vMin.x) vMin.x = pVertices[v].x;
			if(pVertices[v].x > vMax.x) vMax.x = pVertices[v].x;
			if(pVertices[v].y < vMin.y) vMin.y = pVertices[v].y;
			if(pVertices[v].y > vMax.y) vMax.y = pVertices[v].y;
		}
	}

	// Always find the z-extents from the vertices
	for(v=0; v<iNumVertices; v++)
	{
		if(pVertices[v].z < vMin.z) vMin.z = pVertices[v].z;
		if(pVertices[v].z > vMax.z) vMax.z = pVertices[v].z;
	}

	float fSizeX = vMax.x - vMin.x;
	float fSizeY = vMax.y - vMin.y;
	float fSizeZ = vMax.z - vMin.z;

	//*********************************
	// Create the quantized vertex data
	//*********************************
	u16 * pVertexData = rage_new u16[iNumVertices*3];

	float fTmp;
	s32 iTmp;

	for(int v=0; v<iNumVertices; v++)
	{
		// X Component
		fTmp = pVertices[v].x - vMin.x;
		fTmp /= fSizeX;
		iTmp = (s32)(fTmp * 65536.0f);

		if(iTmp < 0)
			iTmp = 0;
		else if(iTmp > 65535)
			iTmp = 65535;
		pVertexData[v*3] = (u16)iTmp;

		// Y Component
		fTmp = pVertices[v].y - vMin.y;
		fTmp /= fSizeY;
		iTmp = (s32)(fTmp * 65536.0f);

		if(iTmp < 0)
			iTmp = 0;
		else if(iTmp > 65535)
			iTmp = 65535;
		pVertexData[(v*3)+1] = (u16)iTmp;

		// Z Component
		fTmp = pVertices[v].z - vMin.z;
		fTmp /= fSizeZ;
		iTmp = (s32)(fTmp * 65536.0f);

		if(iTmp < 0)
			iTmp = 0;
		else if(iTmp > 65535)
			iTmp = 65535;
		pVertexData[(v*3)+2] = (u16)iTmp;
	}

	delete[] pVertices;

	//**************************************************************
	// Now we can create the nav mesh class
	//**************************************************************

#if __ASSERT
	if(ms_iVerbosity > 0)
	{
		char txt[256];
		OutputDebugString("LoadNavSurfaceBinary Navmesh Stats:\n");
		sprintf(txt, "iNumVertices:%i iNumFaces:%i iSizeOfPools:%i\n", iNumVertices, iNumFaces, iNumFaces*3);
		OutputDebugString(txt);
	}
#endif

	CNavMesh * pNavMesh = rage_new CNavMesh();
	pNavMesh->m_iFlags = NAVMESH_COMPRESSED_VERTEX_DATA;
	pNavMesh->m_iNumVertices = iNumVertices;
	pNavMesh->m_iNumPolys = iNumFaces;

	pNavMesh->m_CompressedVertexArray = new aiSplitArray<CNavMeshCompressedVertex, NAVMESH_COMPRESSEDVERTEX_ARRAY_BLOCKSIZE>(iNumVertices, (CNavMeshCompressedVertex*)pVertexData);
	delete[] pVertexData;
	pVertexData = NULL;

	pNavMesh->m_PolysArray = new aiSplitArray<TNavMeshPoly, NAVMESH_POLY_ARRAY_BLOCKSIZE>(iNumFaces, pPolys);
	pNavMesh->SetPolysSubArrayIndices();
	delete[] pPolys;
	pPolys = NULL;

	pNavMesh->m_iSizeOfPools = iNumFaces * 3;

	pNavMesh->m_VertexIndexArray = new aiSplitArray<u16, NAVMESH_VERTEXINDEX_ARRAY_BLOCKSIZE>(pNavMesh->m_iSizeOfPools, pVertexIndices);
	delete[] pVertexIndices;
	pVertexIndices = NULL;

	pNavMesh->m_AdjacentPolysArray = new aiSplitArray<TAdjPoly, NAVMESH_ADJPOLY_ARRAY_BLOCKSIZE>(pNavMesh->m_iSizeOfPools, pAdjPolys);
	delete[] pAdjPolys;
	pAdjPolys = NULL;

	pNavMesh->m_pQuadTree = rage_new CNavMeshQuadTree();
	pNavMesh->m_pQuadTree->m_Mins = vMin;
	pNavMesh->m_pQuadTree->m_Maxs = vMax;
	pNavMesh->m_pNonResourcedData->m_vMinOfNavMesh = pNavMesh->m_pQuadTree->m_Mins;

	pNavMesh->m_pQuadTree->m_pLeafData = rage_new TNavMeshQuadTreeLeafData;
	pNavMesh->m_pQuadTree->m_pLeafData->m_iNumPolys = 0;
	pNavMesh->m_pQuadTree->m_pLeafData->m_Polys = NULL;
	pNavMesh->m_pQuadTree->m_pLeafData->m_iNumCoverPoints = 0;
	pNavMesh->m_pQuadTree->m_pLeafData->m_CoverPoints = NULL;

	vBlockMins = vMin;
	vBlockMaxs = vMax;

	pNavMesh->m_vExtents.x = fSizeX;
	pNavMesh->m_vExtents.y = fSizeY;
	pNavMesh->m_vExtents.z = fSizeZ;

	//*******************************************************************
	// Figure out if this navmesh has variable height-above-poly values.
	// If so then set the navmesh's flags appropriately and then allocate
	// and fill in the array of height values.

	/*
	Assert(!pNavMesh->m_PolyVolumeInfo);

	u32 h;
	bool bHasVolumeData = false;
	for(h=0; h<pNavMesh->m_iNumPolys; h++)
	{
		if(pNavMesh->GetPoly(h)->m_iFreeSpaceTopZ != TPolyVolumeInfo::ms_iFreeSpaceNone)
		{
			if(m_Params.m_bCreateNavmeshUnderwater)
			{
				bHasVolumeData = true;
				break;
			}
			else
			{
				pNavMesh->GetPoly(h)->m_iFreeSpaceTopZ = TPolyVolumeInfo::ms_iFreeSpaceNone;
			}
		}
	}

	if(bHasVolumeData && m_Params.m_bCreateNavmeshUnderwater)
	{
		pNavMesh->m_iFlags |= NAVMESH_HAS_POLY_VOLUME_SECTION;
		pNavMesh->m_PolyVolumeInfo = rage_new TPolyVolumeInfo[pNavMesh->m_iNumPolys];
		memset(pNavMesh->m_PolyVolumeInfo, 0, sizeof(TPolyVolumeInfo)*pNavMesh->m_iNumPolys);

		for(h=0; h<pNavMesh->m_iNumPolys; h++)
		{
			pNavMesh->m_PolyVolumeInfo[h].m_iFreeSpaceTopZ = pNavMesh->GetPoly(h)->m_iFreeSpaceTopZ;
			pNavMesh->m_PolyVolumeInfo[h].m_iPolyLinkedToAbove = NAVMESH_POLY_INDEX_NONE;
			pNavMesh->m_PolyVolumeInfo[h].m_iPolyLinkedToBelow = NAVMESH_POLY_INDEX_NONE;
		}
	}
	*/

	//CNavMesh::SaveBinary(pNavMesh, "x:/gta5/exported_navmeshes/test.inv");

	return pNavMesh;
}


static const int gMaxPoolValue = (1<<NUM_BITS_FOR_1ST_VERTEX_INDEX)-1;

bool
CNavGen::LoadNavSurfaceBinary(
	char * pFileName,
	Vector3 *& pVertices,
	TNavMeshPoly *& pPolys,
	TAdjPoly *& pAdjPolys,
	u16 *& pVertexIndices,
	int & iNumVertices,
	int & iNumFaces)
{
	if(!pFileName || !*pFileName)
		return false;

	FILE * pFilePtr = fopen(pFileName, "rb");
	if(!pFilePtr)
		return false;

	//*****************************************************************
	// Load into memory

	// Get filesize
	fseek(pFilePtr, 0, SEEK_END);
	int iFileSize = ftell(pFilePtr);
	fseek(pFilePtr, 0, SEEK_SET);
	// Alloc buffer
	char * pBuffer = rage_new char[iFileSize];
	// Read in & terminate properly
	size_t iNumBytes = fread(pBuffer, 1, iFileSize, pFilePtr);
	fclose(pFilePtr);

	//*****************************************************************

	char * pDataPtr = pBuffer;

	char header[] = "NAVSRF2\0";
	for(int c=0; c<8; c++)
	{
		if(header[c] != pDataPtr[c])
		{
			Assert(0);
			delete[] pDataPtr;
			return false;
		}
	}
	pDataPtr += 8;

	// Read the CRC of the tri file from which this nav surface was generated
	u32 iCollisionCRC = *((u32*)pDataPtr);
	iCollisionCRC=iCollisionCRC;

	pDataPtr+=4;

	// Read the CRC of the nav res areas which intersected this navmesh
	u32 iResAreasCRC = *((u32*)pDataPtr);
	iResAreasCRC=iResAreasCRC;

	pDataPtr+=4;

	// Read flags
	u32 iFlags = *((u32*)pDataPtr);
	pDataPtr+=4;

	// Read num vertices
	u32 iNumVerts = *((u32*)pDataPtr);
	pDataPtr+=4;

	// Read num triangles
	u32 iNumTris = *((u32*)pDataPtr);
	pDataPtr+=4;

	// Keep compiler quiet in Release/ToolRelease builds
	iFlags; iNumBytes;

	//**********************************************************************
	// Allocate arrays

	iNumVertices = iNumVerts;
	iNumFaces = iNumTris;

	pVertices = rage_new Vector3[iNumVerts];

	pPolys = rage_new TNavMeshPoly[iNumTris];
	pAdjPolys = rage_new TAdjPoly[iNumFaces*3];
	pVertexIndices = rage_new u16[iNumFaces*3];

	ZeroMemory(pPolys, sizeof(TNavMeshPoly) * iNumTris);
	ZeroMemory(pAdjPolys, sizeof(TAdjPoly) * (iNumTris*3));
	ZeroMemory(pVertexIndices, sizeof(u16) * (iNumTris*3));

	//**********************************************************************

	// Read all the vertices
	for(u32 v=0; v<iNumVerts; v++)
	{
		float x,y,z;

		x = *((float*)pDataPtr);
		pDataPtr+=4;
		y = *((float*)pDataPtr);
		pDataPtr+=4;
		z = *((float*)pDataPtr);
		pDataPtr+=4;

		pVertices[v].x	= x;
		pVertices[v].y	= y;
		pVertices[v].z	= z;
	}

	// Read all the tris & associated data
	int iPoolIndex=0;
	u32 iMaxVertexIndex=0;

	for(u32 t=0; t<iNumTris; t++)
	{
		pPolys[t].SetFlags(0);
		pPolys[t].SetNumVertices(3);
		pPolys[t].SetFirstVertexIndex(iPoolIndex);

		// Read indices
		u32 a = *((u32*)pDataPtr);
		pDataPtr+=4;
		u32 b = *((u32*)pDataPtr);
		pDataPtr+=4;
		u32 c = *((u32*)pDataPtr);
		pDataPtr+=4;

		// Read ped density
		u8 iPedDensity = *((u8*)pDataPtr);
		pDataPtr++;

		// Read the baked-in-data bitfield
		u8 iBakedInBitfield = *((u8*)pDataPtr);
		pDataPtr++;

		// clear space Zheight above tri
		s16 iClearSpaceZHeight = *((s16*)pDataPtr);
		pDataPtr+=2;

		u32 iTriFlags = *((u32*)pDataPtr);
		pDataPtr+=4;



		// Set up the poly
		Assert(a <= 65535);
		pVertexIndices[iPoolIndex] = (u16)a;
		pAdjPolys[iPoolIndex].m_iAdjacentNavMeshIndex = NAVMESH_NAVMESH_INDEX_NONE;
		//pAdjPolys[iPoolIndex].SetNavMeshIndex(NAVMESH_NAVMESH_INDEX_NONE);
		pAdjPolys[iPoolIndex].SetPolyIndex(NAVMESH_POLY_INDEX_NONE);
		pAdjPolys[iPoolIndex].SetAdjacencyType(ADJACENCY_TYPE_NORMAL);
		pAdjPolys[iPoolIndex].SetEdgeProvidesCover(false);
		iPoolIndex++;

		Assert(b <= 65535);
		pVertexIndices[iPoolIndex] = (u16)b;
		pAdjPolys[iPoolIndex].m_iAdjacentNavMeshIndex = NAVMESH_NAVMESH_INDEX_NONE;
		//pAdjPolys[iPoolIndex].SetNavMeshIndex(NAVMESH_NAVMESH_INDEX_NONE);
		pAdjPolys[iPoolIndex].SetPolyIndex(NAVMESH_POLY_INDEX_NONE);
		pAdjPolys[iPoolIndex].SetAdjacencyType(ADJACENCY_TYPE_NORMAL);
		pAdjPolys[iPoolIndex].SetEdgeProvidesCover(false);
		iPoolIndex++;

		Assert(c <= 65535);
		pVertexIndices[iPoolIndex] = (u16)c;
		pAdjPolys[iPoolIndex].m_iAdjacentNavMeshIndex = NAVMESH_NAVMESH_INDEX_NONE;
		//pAdjPolys[iPoolIndex].SetNavMeshIndex(NAVMESH_NAVMESH_INDEX_NONE);
		pAdjPolys[iPoolIndex].SetPolyIndex(NAVMESH_POLY_INDEX_NONE);
		pAdjPolys[iPoolIndex].SetAdjacencyType(ADJACENCY_TYPE_NORMAL);
		pAdjPolys[iPoolIndex].SetEdgeProvidesCover(false);
		iPoolIndex++;

		iMaxVertexIndex = max(iMaxVertexIndex, a);
		iMaxVertexIndex = max(iMaxVertexIndex, b);
		iMaxVertexIndex = max(iMaxVertexIndex, c);

		m_iLoadNavSurfaceBinary_MaxVertexPoolSize = max(m_iLoadNavSurfaceBinary_MaxVertexPoolSize, iPoolIndex);

		if(iPoolIndex > gMaxPoolValue)
		{
			char txt[256];
			sprintf(txt, "ERROR in CNavGen::LoadNavSurfaceBinary() : iPoolIndex has overflowed > %i..\n", gMaxPoolValue);
			printf(txt);
			OutputText(txt);
			OutputDebugString(txt);
			AssertMsg(false, txt);
			return false;
		}

		pPolys[t].SetPedDensity(iPedDensity);

		if((iTriFlags&NSB_FLAG_PAVEMENT)!=0)
			pPolys[t].OrFlags(NAVMESHPOLY_IS_PAVEMENT);

		if((iTriFlags&NSB_FLAG_SHELTER)!=0)
			pPolys[t].OrFlags(NAVMESHPOLY_IN_SHELTER);

		if((iTriFlags&NSB_FLAG_WATER)!=0)
			pPolys[t].OrFlags(NAVMESHPOLY_IS_WATER);

		if((iTriFlags&NSB_FLAG_TOOSTEEP)!=0)
			pPolys[t].OrFlags(NAVMESHPOLY_TOO_STEEP_TO_WALK_ON);

		pPolys[t].SetIsInterior((iTriFlags&NSB_FLAG_INTERIOR)!=0);

		pPolys[t].m_iFreeSpaceTopZ = iClearSpaceZHeight;
		pPolys[t].m_bDoNotOptimise = ((iTriFlags&NSB_FLAG_DO_NOT_OPTIMISE)!=0);


		// Save off data which is not a part of CNavMesh resourcing, but which we store in the ".inv" independent asset
		pPolys[t].m_NonResourced.m_bIntersectsEntities = ((iTriFlags&NSB_FLAG_INTERSECTING_ENTITIES)!=0);
		pPolys[t].m_NonResourced.m_bNoNetworkSpawn = ((iTriFlags&NSB_FLAG_NO_NETWORK_SPAWN)!=0);

		
			

		((void*)iBakedInBitfield);

		
	}

	delete[] pBuffer;

	if(ms_iVerbosity > 0)
	{
		char txt[256];
		sprintf(txt, "LoadNavSurfaceBinary() - iMaxVertexIndex:(%i/65535), iMaxPoolIndex:(%i/%i)\n", iMaxVertexIndex, iPoolIndex, gMaxPoolValue);
		OutputText(txt);
		OutputDebugString(txt);
	}

	m_iLoadNavSurfaceBinary_MaxVertexIndex = max(m_iLoadNavSurfaceBinary_MaxVertexIndex, (int)iMaxVertexIndex);
	return true;
}


u32 CNavGen::ScanCollisionFileFlags(char * pInputFilename, bool & bFileExists)
{
	bFileExists = false;

	fiStream * pStream = fiStream::Open(pInputFilename);
	if(!pStream)
		return 0;

	char header[8];
	pStream->Read(header, 8);
	if(strcmp(header, TColPolyData::m_TriFileHeader)!=0)
	{
		pStream->Close();
		return 0;
	}

	bFileExists = true;

	pStream->Seek(NAVMESH_EXPORT_TRIFILE_OFFSET_OF_FLAGS);
	u32 iFlags;
	pStream->ReadInt(&iFlags, 1);


	pStream->Close();
	return iFlags;
}

//****************************************************************************
//	LoadCollisionTrianglesAndMergeIntersectingInteriors
//	This function loads the specified collision "tri" file, which is expected
//	to be a main-map navmesh file.  It then also scans through the list of
//	interiors (m_InteriorInfos) and loads in those which are intersecting the
//	extents of the main-map navmesh.
//	The triangles are all added into the collision triangles list.

 

bool CNavGen::LoadCollisionFileExtents(char * pInputFilename, Vector3 & vMin, Vector3 & vMax)
{
	fiStream * pStream = fiStream::Open(pInputFilename);
	if(!pStream)
		return false;

	char header[8];
	pStream->Read(header, 8);
	if(strcmp(header, TColPolyData::m_TriFileHeader)!=0)
	{
		pStream->Close();
		return false;
	}

	u32 iReserved;
	pStream->ReadInt(&iReserved, 1);

	float tmp[2];
	pStream->ReadFloat(&tmp[0], 1);
	pStream->ReadFloat(&tmp[1], 1);

	pStream->ReadFloat(&vMin.x, 1);
	pStream->ReadFloat(&vMin.y, 1);
	pStream->ReadFloat(&vMin.z, 1);

	pStream->ReadFloat(&vMax.x, 1);
	pStream->ReadFloat(&vMax.y, 1);
	pStream->ReadFloat(&vMax.z, 1);

	pStream->Close();
	return true;
}


// This define is for debugging - we can cull out triangles outside of a specified region
#define __USE_CULL_REGION_WHEN_LOADING_TRIANGLES		0
#define __REMOVE_DEGENERATE_TRIS_ON_LOAD				0
#define __NO_WATER										0

#if HEIGHTMAP_GENERATOR_TOOL
PARAM(importRDR3,"");
#endif

bool CNavGen::LoadCollisionTriangles(
	char * pInputFilename,
	// The following parameters are use purely to return info about the loaded file
	Vector3 & vSectorMins, Vector3 & vSectorMaxs,
	int & iSectorStartX, int & iSectorStartY,
	int & iNumSectorsX, int & iNumSectorsY,
	Vector3 & vWorldMin, Vector3 & vWorldMax,
	Vector2 & vMapSectorsMin,	// The min XY of the map, from which the sector numbering starts
	// These optional params specify a min/max outside of which to discard polygons
	Vector3 * pCullToThisMin, Vector3 * pCullToThisMax
#if HEIGHTMAP_GENERATOR_TOOL
	, CNavGenTriForHeightMap ** out_tris
	, u32 * out_iNumTris
#endif
	)
{
#if HEIGHTMAP_GENERATOR_TOOL
	if (out_tris == NULL || out_iNumTris == NULL)
	{
		return 0;
	}
#endif

#if __USE_CULL_REGION_WHEN_LOADING_TRIANGLES
	Vector3 vCullSize( 4, 4, 4 );
	Vector3 vCullMin( -779, 183, 73 );
	Vector3 vCullMax = vCullMin;
	vCullMin -= vCullSize;
	vCullMax += vCullSize;
#endif

	//*******************************************************************
	//	See whether this input file is a ".tri" file (as exported from
	//	the old GTA codebase) or a ".bnd" file using the Rage physics
	//	bounds from the new codebase.
	//*******************************************************************

	char filename[1024];
	strcpy(filename, pInputFilename);
	strlwr(filename);

	//*******************************************************************
	//	Old-style ".tri" file
	//*******************************************************************
	if(strstr(filename, ".tri"))
	{
		fiStream * pStream = fiStream::Open(filename);
		if(!pStream)
			return false;

		char header[8];
		pStream->Read(header, 8);
		if(strcmp(header, TColPolyData::m_TriFileHeader)!=0)
		{
			pStream->Close();
			return false;
		}

		u32 iReserved;
		pStream->ReadInt(&iReserved, 1);

		u32 iNumTriangles;
		pStream->ReadInt(&iNumTriangles, 1);

		pStream->ReadFloat(&vMapSectorsMin.x, 1);
		pStream->ReadFloat(&vMapSectorsMin.y, 1);

		pStream->ReadFloat(&vSectorMins.x, 1);
		pStream->ReadFloat(&vSectorMins.y, 1);
		pStream->ReadFloat(&vSectorMins.z, 1);

		pStream->ReadFloat(&vSectorMaxs.x, 1);
		pStream->ReadFloat(&vSectorMaxs.y, 1);
		pStream->ReadFloat(&vSectorMaxs.z, 1);

		pStream->ReadInt(&iSectorStartX, 1);
		pStream->ReadInt(&iSectorStartY, 1);
		pStream->ReadInt(&iNumSectorsX, 1);
		pStream->ReadInt(&iNumSectorsY, 1);

#if HEIGHTMAP_GENERATOR_TOOL
		CNavGenTriForHeightMap *& triData = *out_tris;

		if (triData)
		{
			delete[] triData;
		}

		triData = rage_new CNavGenTriForHeightMap[iNumTriangles];
#else
		m_CollisionTriangles.clear();
		m_CollisionTriangles.Resize(iNumTriangles);
		CNavGenTri * triData = m_CollisionTriangles.GetElements();
#endif

		u8 bHasWater=false;
		float fWaterLevel=0.0f;
		u8 bHasVariableWaterLevel=false;

		pStream->ReadByte(&bHasWater, 1);
		pStream->ReadFloat(&fWaterLevel, 1);
		pStream->ReadByte(&bHasVariableWaterLevel, 1);

		m_bHasWater = bHasWater ? true : false;
		m_fWaterLevel = fWaterLevel;
		m_bHasVariableWaterLevel = bHasVariableWaterLevel ? true : false;

		Assert(!(m_pWaterSamples && m_bHasVariableWaterLevel));

		// If there is a variable water level in this navmesh area (this includes navmeshes
		// which have gaps in the water coverage) then read in sampled water heights.
		if(m_bHasVariableWaterLevel)
		{
			pStream->ReadFloat(&m_fWaterSamplingStepSize, 1);
			pStream->ReadInt(&m_iNumWaterSamples, 1);

			Assert(m_iNumWaterSamples > 0);

			m_pWaterSamples = rage_new s16[m_iNumWaterSamples];

			for(int s=0; s<m_iNumWaterSamples; s++)
			{
				pStream->ReadShort(&m_pWaterSamples[s], 1);
			}
		}

#if __NO_WATER
	m_bHasWater = false;
#endif

		//--------------------------------------------------------------------------

		float x,y,z;
		TColPolyData colPolyData;
		u8 iBakedInBitField;
		Vector3 pts[3];
		Vector3 vEdge1, vEdge2, vEdge3;
		int p,n;

		s32 iNumTris = 0;

		for(;;)
		{
			n = pStream->ReadByte(&iBakedInBitField, 1);
			if(!n)
				break;

			colPolyData.Read(pStream);
			colPolyData.SetDontCreateNavMeshOn(false);

			for(p=0; p<3; p++)
			{
				pStream->ReadFloat(&x, 1);
				pStream->ReadFloat(&y, 1);
				pStream->ReadFloat(&z, 1);

				pts[p].x = x;
				pts[p].y = y;
				pts[p].z = z;
			}

#if HEIGHTMAP_GENERATOR_TOOL
			triData[iNumTris].m_vPts[0] = RCC_VEC3V(pts[0]);
			triData[iNumTris].m_vPts[1] = RCC_VEC3V(pts[1]);
			triData[iNumTris].m_vPts[2] = RCC_VEC3V(pts[2]);
			triData[iNumTris].m_colPolyData = colPolyData;
#else
			// Make sure triangle's not degenerate
			vEdge1 = pts[1] - pts[0];
			vEdge2 = pts[2] - pts[0];
			vEdge3 = pts[2] - pts[1];

			static const float fMinTriArea = 0.0001f;
			static const float fMinEdgeLength = 0.001f;

			float fEdgeLength1 = vEdge1.Mag();
			float fEdgeLength2 = vEdge2.Mag();
			float fEdgeLength3 = vEdge3.Mag();
			float fArea = CalcTriArea(fEdgeLength1, fEdgeLength2, fEdgeLength3);

			// Add tri
			CNavGenTri * pTri = &triData[iNumTris];
			pTri->m_vPts[0] = pts[0];
			pTri->m_vPts[1] = pts[1];
			pTri->m_vPts[2] = pts[2];

			pTri->m_iBakedInDataBitField = iBakedInBitField;
			pTri->m_colPolyData = colPolyData;
			pTri->m_fArea = fArea;

			// Calculate normal
			pTri->m_vNormal = CrossProduct(vEdge2, vEdge1);
			pTri->m_vNormal.Normalize();
#endif
			Vector3 vTriMin, vTriMax;
			vTriMin.x = Min( Min(pts[0].x, pts[1].x), pts[2].x);
			vTriMin.y = Min( Min(pts[0].y, pts[1].y), pts[2].y);
			vTriMin.z = Min( Min(pts[0].z, pts[1].z), pts[2].z);
			vTriMax.x = Max( Max(pts[0].x, pts[1].x), pts[2].x);
			vTriMax.y = Max( Max(pts[0].y, pts[1].y), pts[2].y);
			vTriMax.z = Max( Max(pts[0].z, pts[1].z), pts[2].z);

			// Test triangle against the optional min/max supplied.  This is to cull interior polys
			// to the extents of the main-map navmesh which they are in.
			if(pCullToThisMin && pCullToThisMax)
			{
				if(vTriMin.x > pCullToThisMax->x || vTriMin.y > pCullToThisMax->y || vTriMin.z > pCullToThisMax->z ||
					pCullToThisMin->x > vTriMax.x || pCullToThisMin->y > vTriMax.y || pCullToThisMin->z > vTriMax.z)
				{
					continue;
				}
			}

			static const float fExtendTriMinMax = MINMAX_RESOLUTION;
			vTriMin.x -= fExtendTriMinMax;
			vTriMin.y -= fExtendTriMinMax;
			vTriMin.z -= fExtendTriMinMax;
			vTriMax.x += fExtendTriMinMax;
			vTriMax.y += fExtendTriMinMax;
			vTriMax.z += fExtendTriMinMax;

#if !HEIGHTMAP_GENERATOR_TOOL
			pTri->m_MinMax.SetFloat(vTriMin.x, vTriMin.y, vTriMin.z, vTriMax.x, vTriMax.y, vTriMax.z);
#endif
#if __USE_CULL_REGION_WHEN_LOADING_TRIANGLES
			if(vTriMin.x > vCullMax.x || vTriMin.y > vCullMax.y || vTriMin.z > vCullMax.z ||
				vTriMax.x < vCullMin.x || vTriMax.y < vCullMin.y || vTriMax.z < vCullMin.z)
			{
				continue;
			}
#endif

#if !HEIGHTMAP_GENERATOR_TOOL
			pTri->m_fPlaneDist = - DotProduct(pTri->m_vNormal, pts[0]);
			pTri->m_iIndexInCollisionPolysList = iNumTris;
			Assert(iNumTris < m_CollisionTriangles.size());
#endif
			iNumTris++;
		}

#if HEIGHTMAP_GENERATOR_TOOL
		*out_iNumTris = iNumTris;
#else
		m_CollisionTriangles.Resize(iNumTris);
#endif
		pStream->Close();
	}

	//*******************************************************************
	//	Unrecognised file extension
	//*******************************************************************
	else
	{
		return false;
	}


	//**********************************************************************
	//	Slice the triangles to the exact extents of the area which we
	//	are creating the navmesh for.  For ".tri" files, the dimensions are
	//	held in the file format.  For ".bnd" files we may have to examine
	//	the filename to extract the area dimensions.
	//	For example, "Sectors6x6_108_24" means a block of 6x6 sectors
	//	starting at sector(108,24).  By using the conversion functions in
	//	"World.h" we can find out the exact min & max in worldspace of
	//	this block of sectors.
	//********************************************************************* 
	vWorldMin = Vector3(FLT_MAX, FLT_MAX, FLT_MAX);
	vWorldMax = Vector3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	//*****************************************************************************
	//	Slice away any triangle pieces which are outside of this block of sectors.
	//	This ensures that we don't process anything which is outside of block
	//	which we are concerned with.
	//*****************************************************************************

	Vector3 vSliceMins = vSectorMins - Vector3(0.1f, 0.1f, 0.1f);
	Vector3 vSliceMaxs = vSectorMaxs + Vector3(0.1f, 0.1f, 0.1f);

	// Expand the min/max which we slice the geometry buy ever-so-slightly, so that linetests right
	// on the edge won't fail due to small numerical errors.
	//	SliceAwayGeometry(vSliceMins, vSliceMaxs);	// NO NEED TO DO THIS - GEOMETRY IS NOW CLIPPED AT EXPORT

#if !HEIGHTMAP_GENERATOR_TOOL
	s32 t;
	for(t=0; t<m_CollisionTriangles.size(); t++)
	{
		CNavGenTri * pTri = &m_CollisionTriangles[t];

		// Update world extents
		for(int p=0; p<3; p++)
		{
			if(pTri->m_vPts[p].x < vWorldMin.x) vWorldMin.x = pTri->m_vPts[p].x;
			if(pTri->m_vPts[p].y < vWorldMin.y) vWorldMin.y = pTri->m_vPts[p].y;
			if(pTri->m_vPts[p].z < vWorldMin.z) vWorldMin.z = pTri->m_vPts[p].z;

			if(pTri->m_vPts[p].x > vWorldMax.x) vWorldMax.x = pTri->m_vPts[p].x;
			if(pTri->m_vPts[p].y > vWorldMax.y) vWorldMax.y = pTri->m_vPts[p].y;
			if(pTri->m_vPts[p].z > vWorldMax.z) vWorldMax.z = pTri->m_vPts[p].z;
		}
	}
#endif

	m_vSectorMins = vSectorMins;
	m_vSectorMaxs = vSectorMaxs;

	return true;
}

#if HEIGHTMAP_GENERATOR_TOOL
int CNavGen::LoadCollisionTrianglesForHeightMap(
	CNavGenTriForHeightMap *& tris,
	int &,// iMaxTris,
	char * pInputFilename)
{
	Vector3 vSectorMins;
	Vector3 vSectorMaxs;
	int iSectorStartX;
	int iSectorStartY;
	int iNumSectorsX;
	int iNumSectorsY;
	Vector3 vWorldMin;
	Vector3 vWorldMax;
	Vector2 vMapSectorsMin;

	u32 iNumTris = 0;

	if (LoadCollisionTriangles(pInputFilename, vSectorMins, vSectorMaxs, iSectorStartX, iSectorStartY, iNumSectorsX, iNumSectorsY, vWorldMin, vWorldMax, vMapSectorsMin, NULL, NULL, &tris, &iNumTris))
	{
		return iNumTris;
	}

	return 0;
}
#endif

void CNavGen::CreateGroundPlane(const Vector2 & vAreaMin, const Vector2 & vAreaMax, const Vector2 & vPlaneMin, const Vector2 & vPlaneMax, const float z)
{
	m_CollisionTriangles.clear();
	m_CollisionTriangles.Resize(2);
	CNavGenTri * triArray = m_CollisionTriangles.GetElements();

	m_vSectorMins = Vector3(vAreaMin.x, vAreaMin.y, z-100.0f);
	m_vSectorMaxs = Vector3(vAreaMax.x, vAreaMax.y, z+100.0f);

	TColPolyData colPolyData;
	memset(&colPolyData, 0, sizeof(TColPolyData));
	colPolyData.SetIsWalkable(true);

	Vector3 vEdge1, vEdge2, vEdge3;
	float fEdgeLength1, fEdgeLength2, fEdgeLength3;

	CNavGenTri * pTris[2];

	CNavGenTri * pTri1 = &triArray[0];
	pTri1->m_vPts[2] = Vector3(vPlaneMin.x, vPlaneMin.y, z);
	pTri1->m_vPts[1] = Vector3(vPlaneMax.x, vPlaneMin.y, z);
	pTri1->m_vPts[0] = Vector3(vPlaneMin.x, vPlaneMax.y, z);
	CNavGenTri * pTri2 = &triArray[1];
	pTri2->m_vPts[2] = Vector3(vPlaneMin.x, vPlaneMax.y, z);
	pTri2->m_vPts[1] = Vector3(vPlaneMax.x, vPlaneMin.y, z);
	pTri2->m_vPts[0] = Vector3(vPlaneMax.x, vPlaneMax.y, z);

	pTris[0] = pTri1;
	pTris[1] = pTri2;

	static const float fExtendTriMinMax = 0.1f;

	for(int t=0; t<2; t++)
	{
		CNavGenTri * pTri = pTris[t];
		pTri->m_iBakedInDataBitField = 0;
		pTri->m_colPolyData = colPolyData;

		vEdge1 = pTri->m_vPts[1] - pTri->m_vPts[0];
		vEdge2 = pTri->m_vPts[2] - pTri->m_vPts[0];
		vEdge3 = pTri->m_vPts[2] - pTri->m_vPts[1];
		fEdgeLength1 = vEdge1.Mag();
		fEdgeLength2 = vEdge2.Mag();
		fEdgeLength3 = vEdge3.Mag();
		pTri->m_fArea = CalcTriArea(fEdgeLength1, fEdgeLength2, fEdgeLength3);
		pTri->m_vNormal = CrossProduct(vEdge2, vEdge1);
		pTri->m_vNormal.Normalize();

		Vector3 vTriMin, vTriMax;
		vTriMin.x = Min( Min(pTri->m_vPts[0].x, pTri->m_vPts[1].x), pTri->m_vPts[2].x) - fExtendTriMinMax;
		vTriMin.y = Min( Min(pTri->m_vPts[0].y, pTri->m_vPts[1].y), pTri->m_vPts[2].y) - fExtendTriMinMax;
		vTriMin.z = Min( Min(pTri->m_vPts[0].z, pTri->m_vPts[1].z), pTri->m_vPts[2].z) - fExtendTriMinMax;
		vTriMax.x = Max( Max(pTri->m_vPts[0].x, pTri->m_vPts[1].x), pTri->m_vPts[2].x) + fExtendTriMinMax;
		vTriMax.y = Max( Max(pTri->m_vPts[0].y, pTri->m_vPts[1].y), pTri->m_vPts[2].y) + fExtendTriMinMax;
		vTriMax.z = Max( Max(pTri->m_vPts[0].z, pTri->m_vPts[1].z), pTri->m_vPts[2].z) + fExtendTriMinMax;
		pTri->m_MinMax.SetFloat(vTriMin.x, vTriMin.y, vTriMin.z, vTriMax.x, vTriMax.y, vTriMax.z);

		pTri->m_fPlaneDist = - DotProduct(pTri->m_vNormal, pTri->m_vPts[0]);
		pTri->m_iIndexInCollisionPolysList = m_CollisionTriangles.size();
		m_CollisionTriangles.Push(*pTri);
	}
}

bool CNavGen::LoadCustomResolutionAreas(char * pFilename)
{
	m_ResolutionAreasWholeMap.clear();

	INIT_PARSER;
	parTree * pTree = PARSER.LoadTree(pFilename, "");
	Assert(pTree);

	const parTreeNode* pRootNode = pTree->GetRoot();
	Assert(pRootNode);
	Assert(stricmp(pRootNode->GetElement().GetName(), "NavResAreas") == 0);

	parTreeNode::ChildNodeIterator i = pRootNode->BeginChildren();
	for(; i != pRootNode->EndChildren(); ++i)
	{
		if(stricmp((*i)->GetElement().GetName(), "Areas") == 0)
		{
			// Go over any and all the child DOM trees and DOM nodes off of the list.
			int areaIndex = 0;
			parTreeNode::ChildNodeIterator j = (*i)->BeginChildren();
			for(; j != (*i)->EndChildren(); ++j)
			{
				Assert(stricmp((*j)->GetElement().GetName(), "Area") == 0);

				CNavResolutionArea * pArea = rage_new CNavResolutionArea();
				pArea->m_bDynamicallyCreated = false;
				m_ResolutionAreasWholeMap.push_back(pArea);

				pArea->m_vMin.x = (*j)->GetElement().FindAttributeFloatValue("MinX", 0.0f);
				pArea->m_vMin.y = (*j)->GetElement().FindAttributeFloatValue("MinY", 0.0f);
				pArea->m_vMin.z = (*j)->GetElement().FindAttributeFloatValue("MinZ", 0.0f);
				pArea->m_vMax.x = (*j)->GetElement().FindAttributeFloatValue("MaxX", 0.0f);
				pArea->m_vMax.y = (*j)->GetElement().FindAttributeFloatValue("MaxY", 0.0f);
				pArea->m_vMax.z = (*j)->GetElement().FindAttributeFloatValue("MaxZ", 0.0f);
				int iResMult = (*j)->GetElement().FindAttributeIntValue("ResMult", 0);
				pArea->m_fMultiplier = 1.0f / (float)( 1<<(iResMult+1) );

				pArea->m_iFlags = (*j)->GetElement().FindAttributeIntValue("Flags", 0);

				areaIndex++;
			}
		}
	}

	delete pTree;

	return true;
}

bool CNavGen::LoadCustomCoverpoints(char * pFilename)
{
	m_CustomCoverpointsWholeMap.clear();

	INIT_PARSER;
	parTree * pTree = PARSER.LoadTree(pFilename, "");
	Assert(pTree);

	const parTreeNode* pRootNode = pTree->GetRoot();
	Assert(pRootNode);
	Assert(stricmp(pRootNode->GetElement().GetName(), "NavCustomCoverpoints") == 0);

	parTreeNode::ChildNodeIterator i = pRootNode->BeginChildren();
	for(; i != pRootNode->EndChildren(); ++i)
	{
		if(stricmp((*i)->GetElement().GetName(), "CustomCoverpoints") == 0)
		{
			// Go over any and all the child DOM trees and DOM nodes off of the list.
			parTreeNode::ChildNodeIterator j = (*i)->BeginChildren();
			for(; j != (*i)->EndChildren(); ++j)
			{
				Assert(stricmp((*j)->GetElement().GetName(), "Coverpoint") == 0);

				CNavCustomCoverpoint * pCustomCoverpoint = rage_new CNavCustomCoverpoint();
				m_CustomCoverpointsWholeMap.push_back(pCustomCoverpoint);

				pCustomCoverpoint->m_Type		= (u8)(*j)->GetElement().FindAttributeIntValue("Type", 0);
				pCustomCoverpoint->m_Direction	= (u8)(*j)->GetElement().FindAttributeIntValue("Direction", 0);
				pCustomCoverpoint->m_CoordsX	= (*j)->GetElement().FindAttributeFloatValue("CoordsX", 0.0f);
				pCustomCoverpoint->m_CoordsY	= (*j)->GetElement().FindAttributeFloatValue("CoordsY", 0.0f);
				pCustomCoverpoint->m_CoordsZ	= (*j)->GetElement().FindAttributeFloatValue("CoordsZ", 0.0f);
			}
		}
	}

	delete pTree;

	return true;
}

bool
CNavGen::ProcessLineOfSightNoOctree(const Vector3 & vStart, const Vector3 & vEnd, const u32 iCollisionTypes, Vector3 & vHitPoint, float & fHitDist, CNavGenTri *& hitTriangle, bool UNUSED_PARAM(bVertical))
{
	float fClosestHitDist = 99999999.0f;
	Vector3 vIntersect;
	bool bIntersected = false;

	for(s32 t=0; t<m_CollisionTriangles.size(); t++)
	{
		CNavGenTri * pTri = &m_CollisionTriangles[t];

		if(!pTri->m_colPolyData.TestCollisionTypeFlags(iCollisionTypes))
			continue;

		// As soon as an intersection is found we may return false
		bool bHit = RayIntersectsTriangle(
			vStart,
			vEnd,
			&pTri->m_vPts[0],
			vIntersect
		);

		if(bHit)
		{
			Vector3 vDiff = vIntersect - vStart;
			float fDistSqr = vDiff.Mag2();
			if(fDistSqr < fClosestHitDist)
			{
				bIntersected = true;
				fClosestHitDist = fDistSqr;
				vHitPoint = vIntersect;
				hitTriangle = pTri;
			}
		}
	}
	if(bIntersected)
	{
		fHitDist = fClosestHitDist;
	}
	return bIntersected;
}



int CNavGen::ms_BytesUsedInQuadtree = 0;
int CNavGen::ms_iNumNavMeshQuadtreeLeaves = 0;

// The extents will have already been set in the Quadtree.  All we have to do
// is construct it.
void
CNavGen::MakeNavMeshQuadTree(CNavMesh * pNavMesh, const Vector3 & vMin, const Vector3 & vMax)
{
	u32 p,v;
	Vector3 vert;

	vector<CSplitPoly*> splitPolyList;

	for(p=0; p<pNavMesh->m_iNumPolys; p++)
	{
		TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);
		CSplitPoly * pSplitPoly = rage_new CSplitPoly();

		for(v=0; v<pPoly->GetNumVertices(); v++)
		{
			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, v), vert);
			pSplitPoly->m_Pts.PushAndGrow(vert);
		}

		//*******************************************
		// Here we are casting the int *index* of the
		// poly, to a pointer!! beware !!
		//*******************************************
		pSplitPoly->m_pParentTriangle = (CNavGenTri*)p;

		splitPolyList.push_back(pSplitPoly);
	}

	ms_BytesUsedInQuadtree = 0;
	ms_iNumNavMeshQuadtreeLeaves = 0;

	CNavMeshQuadTree * pQuadTree = MakeNavMeshQuadTree(
		vMin,
		vMax,
		splitPolyList,
		0
	);

	// Delete the existing (minimal) quadtree, ready for the proper one
	delete pNavMesh->m_pQuadTree;
	pNavMesh->m_pQuadTree = NULL;

	pNavMesh->m_pQuadTree = pQuadTree;
}

int g_fNavMeshQuadtreeMaxDepth = 100;
int g_fNavMeshQuadtreeMinPolysInLeaf = 32;
float g_fNavMeshQuadtreeMinLeafSize = CPathServerExtents::GetWorldWidthOfSector();

CNavMeshQuadTree *
CNavGen::MakeNavMeshQuadTree(const Vector3 & vMin, const Vector3 & vMax, vector<CSplitPoly*> & splitPolyList, int iDepth)
{
	u32 p;

	// Termination conditions met ? Create a leaf here ?
	Vector3 vSize = vMax - vMin;
	if(iDepth > g_fNavMeshQuadtreeMaxDepth || splitPolyList.size() < (u32)g_fNavMeshQuadtreeMinPolysInLeaf ||
		(vSize.x <= g_fNavMeshQuadtreeMinLeafSize && vSize.y <= g_fNavMeshQuadtreeMinLeafSize))
	{
		CNavMeshQuadTree * pNewNode = rage_new CNavMeshQuadTree();
		pNewNode->m_Mins = vMin;
		pNewNode->m_Maxs = vMax;
		pNewNode->m_pChildren[0] = pNewNode->m_pChildren[1] = pNewNode->m_pChildren[2] = pNewNode->m_pChildren[3] = NULL;
		pNewNode->m_pLeafData = rage_new TNavMeshQuadTreeLeafData();
		pNewNode->m_pLeafData->m_iNumPolys = (u16) splitPolyList.size();
		pNewNode->m_pLeafData->m_Polys = rage_new u16[splitPolyList.size()];
		pNewNode->m_pLeafData->m_iNumCoverPoints = 0;
		pNewNode->m_pLeafData->m_CoverPoints = NULL;

		ms_BytesUsedInQuadtree += sizeof(CNavMeshQuadTree);
		ms_BytesUsedInQuadtree += sizeof(TNavMeshQuadTreeLeafData);
		ms_BytesUsedInQuadtree += sizeof(u16) * (int)splitPolyList.size();

		ms_iNumNavMeshQuadtreeLeaves++;

		for(p=0; p<splitPolyList.size(); p++)
		{
			CSplitPoly * pSplitPoly = splitPolyList[p];
			size_t index = (size_t)pSplitPoly->m_pParentTriangle;
			pNewNode->m_pLeafData->m_Polys[p] = (u16)index;

			delete pSplitPoly;
		}
		return pNewNode;
	}

	vector<CSplitPoly*> quadrantLists[4];
	Vector3 vMid = (vMin + vMax) / 2.0f;

	Vector3 vSplitPlaneNormalUp(0, 1.0f, 0);
	float fSplitPlaneDistUp = - DotProduct(vSplitPlaneNormalUp, vMid);

	Vector3 vSplitPlaneNormalRight(1.0f, 0, 0);
	float fSplitPlaneDistRight = - DotProduct(vSplitPlaneNormalRight, vMid);

	for(p=0; p<splitPolyList.size(); p++)
	{
		CSplitPoly * pSplitPoly = splitPolyList[p];
		CSplitPoly * pFrontPolyTmp = NULL, * pBackPolyTmp = NULL;
		CSplitPoly * pPolyPosYPosX = NULL, * pPolyPosYNegX = NULL, * pPolyNegYPosX = NULL, * pPolyNegYNegX = NULL;

		size_t triIndex = (size_t)pSplitPoly->m_pParentTriangle;

		SplitPolyToPlane(pSplitPoly, vSplitPlaneNormalUp, fSplitPlaneDistUp, 0.0f, &pFrontPolyTmp, &pBackPolyTmp);
		delete pSplitPoly;

		if(pFrontPolyTmp)
		{
			SplitPolyToPlane(pFrontPolyTmp, vSplitPlaneNormalRight, fSplitPlaneDistRight, 0.0f, &pPolyPosYPosX, &pPolyPosYNegX);
			//SplitPolyToAxialPlane(pFrontPolyTmp, PLANESIDE_POSX, fSplitPlaneDistRight, &pPolyPosYPosX, &pPolyPosYNegX);
			delete pFrontPolyTmp;
			pFrontPolyTmp = NULL;
		}
		if(pBackPolyTmp)
		{
			SplitPolyToPlane(pBackPolyTmp, vSplitPlaneNormalRight, fSplitPlaneDistRight, 0.0f, &pPolyNegYPosX, &pPolyNegYNegX);
			//SplitPolyToAxialPlane(pBackPolyTmp, PLANESIDE_POSX, fSplitPlaneDistRight, &pPolyNegYPosX, &pPolyNegYNegX);
			delete pBackPolyTmp;
			pBackPolyTmp = NULL;
		}

		// Add polys to their respective quadrant lists
		if(pPolyPosYPosX)
		{
			pPolyPosYPosX->m_pParentTriangle = (CNavGenTri*)triIndex;
			quadrantLists[0].push_back(pPolyPosYPosX);
		}
		if(pPolyNegYPosX)
		{
			pPolyNegYPosX->m_pParentTriangle = (CNavGenTri*)triIndex;
			quadrantLists[1].push_back(pPolyNegYPosX);
		}
		if(pPolyNegYNegX)
		{
			pPolyNegYNegX->m_pParentTriangle = (CNavGenTri*)triIndex;
			quadrantLists[2].push_back(pPolyNegYNegX);
		}
		if(pPolyPosYNegX)
		{
			pPolyPosYNegX->m_pParentTriangle = (CNavGenTri*)triIndex;
			quadrantLists[3].push_back(pPolyPosYNegX);
		}
	}

	CNavMeshQuadTree * pNewNode = rage_new CNavMeshQuadTree();
	pNewNode->m_Mins = vMin;
	pNewNode->m_Maxs = vMax;
	pNewNode->m_pLeafData = NULL;

	ms_BytesUsedInQuadtree += sizeof(CNavMeshQuadTree);
	
	// +x +y
	pNewNode->m_pChildren[0] = MakeNavMeshQuadTree(
		vMid, vMax,
		quadrantLists[0],
		iDepth+1
	);

	// +x -y
	pNewNode->m_pChildren[1] = MakeNavMeshQuadTree(
		Vector3(vMid.x, vMin.y, 0), Vector3(vMax.x, vMid.y, 0),
		quadrantLists[1],
		iDepth+1
	);

	// -x -y
	pNewNode->m_pChildren[2] = MakeNavMeshQuadTree(
		vMin, vMid,
		quadrantLists[2],
		iDepth+1
	);

	// -x +y
	pNewNode->m_pChildren[3] = MakeNavMeshQuadTree(
		Vector3(vMin.x, vMid.y, 0), Vector3(vMid.x, vMax.y, 0),
		quadrantLists[3],
		iDepth+1
	);

	return pNewNode;
}


void
CNavGen::CheckNavMeshIntegrity(CNavMesh * pNavMesh)
{
	// Check whether any vertices in the navmesh are duplicated -
	// ie. more than one vertex exists with the same position (or very close)
	
	char tmpText[256];
	static const float fCoincidentVertexEps = 0.1f;

	u32 v1,v2,p;

	for(v1=0; v1<pNavMesh->m_iNumVertices; v1++)
	{
		Vector3 vert1;
		pNavMesh->GetVertex(v1, vert1);

		for(v2=v1+1; v2<pNavMesh->m_iNumVertices; v2++)
		{
			Vector3 vert2;
			pNavMesh->GetVertex(v2, vert2);

			if(vert1.IsClose(vert2, fCoincidentVertexEps))
			{
				sprintf(tmpText, "ERROR : Coincident vertices found (%i and %i)\n", v1, v2);

				printf(tmpText);
				OutputDebugString(tmpText);
			}
		}
	}

	// Check whether any vertices in the navmesh are unused.  ie. no polygons
	// in the entire mesh use them.

	for(v1=0; v1<pNavMesh->m_iNumVertices; v1++)
	{
		Vector3 vert1;
		pNavMesh->GetVertex(v1, vert1);

		bool bVertexUsed = false;

		for(p=0; (p<pNavMesh->m_iNumPolys && !bVertexUsed); p++)
		{
			TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);

			for(v2=0; v2<pPoly->GetNumVertices(); v2++)
			{
				int index = pNavMesh->GetPolyVertexIndex(pPoly, v2);

				if(index == (int) v1)
				{
					bVertexUsed = true;
					break;
				}
			}
		}

		if(!bVertexUsed)
		{
			sprintf(tmpText, "ERROR : Unused vertex found (%i)\n", v1);

			printf(tmpText);
			OutputDebugString(tmpText);
		}
	}

}

}	// namespace rage
