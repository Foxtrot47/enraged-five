#include "NavGen.h"
#include "NavGen_Store.h"
//#include "pathserver/pathserver.h"

using namespace rage;

//****************************************************************************
//	FloodFillNavMeshes
//	This function uses the CPathServer's store of navmeshes to flood-fill
//	through the meshes.  Each poly which is visited is marked, and then after
//	completion those which were never visited can be removed.
//****************************************************************************

bool
CNavGen::FloodFillNavMeshes()
{
	//Assert(CPathServer::ms_bInitialised);

	int iNumPolysBefore = CountPolysInAllNavMeshes();

	do 
	{
		TNavMeshPoly * pPoly = FindSuitablePolyForFloodFill();
		if(!pPoly)
		{
			break;
		}

		FloodFillNavMeshesFromPoly(pPoly);

	} while(1);

	RecreateAllNavMeshesAfterFloodFill();

	int iNumPolysAfter = CountPolysInAllNavMeshes();
	char tmpTxt[256];
	sprintf(tmpTxt, "\nFloodFill removed %i polys (%i/%i)\n", iNumPolysBefore - iNumPolysAfter, iNumPolysBefore, iNumPolysAfter);
	OutputText(tmpTxt);

	return true;
}

//****************************************************************************
//	FindSuitablePolyForFloodFill
//	This function goes through all the navmeshes, and finds a poly which may
//	be used as the starting-point for a flood fill.
//	It might also be useful to have a list of the ped-nodes in the game, and
//	attempt flood-filling from these too.
//****************************************************************************

TNavMeshPoly *
CNavGen::FindSuitablePolyForFloodFill()
{
//	Assert(CPathServer::ms_bInitialised);

	s32 n;
	u32 p;
	for(n=0; n<CNavGen_NavMeshStore::GetTotalNumNavMeshes(); n++)
	{
		CNavMesh * pNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(n);
		if(!pNavMesh)
			continue;

		for(p=0; p<pNavMesh->m_iNumPolys; p++)
		{
			TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);

			// We don't want to flood-fill from any poly which has already been flood-filled from
			if(pPoly->TestFlags(NAVMESHPOLY_NAVGEN_VISITED_BY_FLOODFILL))
				continue;
			// We *only* want to flood-fill from polys which are open to the air above.  In this way we
			// should be able exclude polys which are trapped inside buildings, etc.
			if(pPoly->TestFlags(NAVMESHPOLY_IN_SHELTER))
				continue;

			return pPoly;
		}
	}

	return NULL;
}

int
CNavGen::CountPolysInAllNavMeshes()
{
	//Assert(CPathServer::ms_bInitialised);

	int iNum=0;
	s32 n;
	for(n=0; n<CNavGen_NavMeshStore::GetTotalNumNavMeshes(); n++)
	{
		CNavMesh * pNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(n);
		if(pNavMesh)
		{
			iNum += pNavMesh->m_iNumPolys;
		}
	}

	return iNum;
}

void
CNavGen::RecreateAllNavMeshesAfterFloodFill()
{
	//Assert(CPathServer::ms_bInitialised);

	RemapPolyIndicesInAllNavMeshes();

	s32 n;
	for(n=0; n<CNavGen_NavMeshStore::GetTotalNumNavMeshes(); n++)
	{
		CNavMesh * pNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(n);
		if(pNavMesh)
		{
			ReconstructNavMeshAfterFloodFill(pNavMesh);
		}
	}
}


//****************************************************************************
//	FloodFillNavMeshesFromPoly
//	Recursively visits polygons in all the navmeshes in the world.
//	This will likely eat up a lot of stack space, and if the exe isn't linked
//	with a really large stack-size then expect crashes!
//	NB: This could easily be rewritten to use a queue/list, and then we won't
//	need the stack space for recursion.
//****************************************************************************

void
CNavGen::FloodFillNavMeshesFromPoly(TNavMeshPoly * pPoly)
{
	Assert(!(pPoly->TestFlags(NAVMESHPOLY_NAVGEN_VISITED_BY_FLOODFILL)));

	pPoly->OrFlags(NAVMESHPOLY_NAVGEN_VISITED_BY_FLOODFILL);

	CNavMesh * pNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(pPoly->GetNavMeshIndex());

	for(u32 a=0; a<pPoly->GetNumVertices(); a++)
	{
		const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly(pPoly->GetFirstVertexIndex() + a);
		const u32 iAdjNavMesh = adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes());
		if(iAdjNavMesh != NAVMESH_NAVMESH_INDEX_NONE &&
			iAdjNavMesh != NAVMESH_INDEX_TESSELLATION &&
			adjPoly.GetPolyIndex() != NAVMESH_POLY_INDEX_NONE)
		{
			CNavMesh * pAdjNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(iAdjNavMesh);
			TNavMeshPoly * pAdjPoly = pAdjNavMesh->GetPoly(adjPoly.GetPolyIndex());

			// We don't want to flood-fill into any poly which has already been visited
			if((pAdjPoly->TestFlags(NAVMESHPOLY_NAVGEN_VISITED_BY_FLOODFILL)==0))
			{
				FloodFillNavMeshesFromPoly(pAdjPoly);
			}
		}
	}
}

//****************************************************************************
//	ReconstructNavMeshAfterFloodFill
//	After doing the flood-fill across the navmeshes, we will need to re-create
//	each navmesh removing the polygons which were not visited by the fill &
//	are therefore deemed to be inaccessible.
//	This will involve recreating all the data stores, etc.
//****************************************************************************

void
CNavGen::RemapPolyIndicesInAllNavMeshes()
{
	s32 n;
	u32 p;

	// Firstly calculate what the new indices of each poly will be (we use the m_iRemappedPolyIndex present in TNavMeshPoly when NAVGEN_TOOL is #defined)
	for(n=0; n<CNavGen_NavMeshStore::GetTotalNumNavMeshes(); n++)
	{
		CNavMesh * pNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(n);
		if(pNavMesh)
		{
			int iCount=0;

			for(p=0; p<pNavMesh->m_iNumPolys; p++)
			{
				TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);

				// If this poly was visited, then it will need a new index
				if(pPoly->TestFlags(NAVMESHPOLY_NAVGEN_VISITED_BY_FLOODFILL))
				{
					pPoly->m_iRemappedPolyIndex = (u16)iCount;
					iCount++;
				}
				else
				{
					pPoly->m_iRemappedPolyIndex = 0xFFFF;
				}
			}
		}
	}

	u32 a;
	// Now go through all adjacencies, and remap the adjacent poly indices
	for(n=0; n<CNavGen_NavMeshStore::GetTotalNumNavMeshes(); n++)
	{
		CNavMesh * pNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(n);
		if(pNavMesh)
		{
			for(p=0; p<pNavMesh->m_iNumPolys; p++)
			{
				TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);

				// If this poly was visited, then it will need remapping
				if(pPoly->TestFlags(NAVMESHPOLY_NAVGEN_VISITED_BY_FLOODFILL))
				{
					for(a=0; a<pPoly->GetNumVertices(); a++)
					{
						const TAdjPoly & adjPoly = pNavMesh->GetAdjacentPoly( pPoly->GetFirstVertexIndex() + a );
						const u32 iAdjNavMesh = adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes());
						if(iAdjNavMesh != NAVMESH_NAVMESH_INDEX_NONE && adjPoly.GetPolyIndex() != NAVMESH_POLY_INDEX_NONE)
						{
							CNavMesh * pAdjNavMesh = CNavGen_NavMeshStore::GetNavMeshFromIndex(iAdjNavMesh);
							Assert(pAdjNavMesh);
							//TNavMeshPoly * pAdjPoly = pAdjNavMesh->GetPoly(adjPoly.m_PolyIndex);
							TNavMeshPoly * pAdjPoly = pAdjNavMesh->GetPoly(adjPoly.GetPolyIndex());
							Assert(pAdjPoly->TestFlags(NAVMESHPOLY_NAVGEN_VISITED_BY_FLOODFILL));
							Assert(pAdjPoly->m_iRemappedPolyIndex != 0xFFFF);

							
							pNavMesh->GetAdjacentPolysArray().Get(pPoly->GetFirstVertexIndex() + a)->SetPolyIndex(pAdjPoly->m_iRemappedPolyIndex);
							pNavMesh->GetAdjacentPolysArray().Get(pPoly->GetFirstVertexIndex() + a)->SetOriginalPolyIndex(pAdjPoly->m_iRemappedPolyIndex);
						}
					}
				}
				else
				{
					for(a=0; a<pPoly->GetNumVertices(); a++)
					{
						TAdjPoly & adjPoly = *pNavMesh->GetAdjacentPolysArray().Get( pPoly->GetFirstVertexIndex() + a );
						adjPoly.m_iAdjacentNavMeshIndex = NAVMESH_NAVMESH_INDEX_NONE;
						adjPoly.SetNavMeshIndex(NAVMESH_NAVMESH_INDEX_NONE, pNavMesh->GetAdjacentMeshes());
						adjPoly.SetOriginalNavMeshIndex(NAVMESH_NAVMESH_INDEX_NONE, pNavMesh->GetAdjacentMeshes());
						adjPoly.SetPolyIndex(NAVMESH_POLY_INDEX_NONE);
						adjPoly.SetOriginalPolyIndex(NAVMESH_POLY_INDEX_NONE);
					}
				}
			}
		}
	}

}

void
CNavGen::ReconstructNavMeshAfterFloodFill(CNavMesh * pNavMesh)
{
	u32 p,vi;
	u32 v;

	static const float fSameVertEps = 0.005f;

	vector<TNavMeshPoly*> polysToKeep;
	for(p=0; p<pNavMesh->m_iNumPolys; p++)
	{
		TNavMeshPoly * pPoly = pNavMesh->GetPoly(p);
		if(pPoly->TestFlags(NAVMESHPOLY_NAVGEN_VISITED_BY_FLOODFILL))
		{
			Assert(pPoly->m_iRemappedPolyIndex != 0xFFFF);
			Assert(pPoly->m_iRemappedPolyIndex == polysToKeep.size());

			polysToKeep.push_back(pPoly);
		}
	}

	vector<Vector3*> verts;
	vector<u16> indices;
	vector<TAdjPoly> adjPolys;

	for(p=0; p<polysToKeep.size(); p++)
	{
		TNavMeshPoly * pPoly = polysToKeep[p];

		size_t iFirstVertexIndex = indices.size();

		for(v=0; v<pPoly->GetNumVertices(); v++)
		{
			Vector3 vVert;
			pNavMesh->GetVertex( pNavMesh->GetPolyVertexIndex(pPoly, v), vVert );

			for(vi=0; vi<verts.size(); vi++)
			{
				const Vector3 * vExistingVert = verts[vi];
				if(vExistingVert->IsClose(vVert, fSameVertEps))
				{
					indices.push_back( (u16) vi);
					break;
				}
			}
			if(vi==verts.size())
			{
				indices.push_back( (u16) verts.size());

				Vector3 * pNewVert = rage_new Vector3;
				*pNewVert = vVert;
				verts.push_back(pNewVert);
			}

			TAdjPoly adjPoly = pNavMesh->GetAdjacentPoly(pPoly->GetFirstVertexIndex() + v);

			if(adjPoly.GetNavMeshIndex(pNavMesh->GetAdjacentMeshes())==pNavMesh->m_iIndexOfMesh)
			{
				Assert(adjPoly.GetPolyIndex() < polysToKeep.size());
			}

			adjPolys.push_back(adjPoly);
		}

		pPoly->SetFirstVertexIndex((u32)iFirstVertexIndex);
	}

	//****************************************************************************
	// Polys

	pNavMesh->m_iNumPolys = (u32)polysToKeep.size();

	TNavMeshPoly * pNewPolys = rage_new TNavMeshPoly[polysToKeep.size()];
	for(p=0; p<polysToKeep.size(); p++)
	{
		TNavMeshPoly * pPoly = polysToKeep[p];
		TNavMeshPoly * pNewPoly = &pNewPolys[p];

		memcpy(pNewPoly, pPoly, sizeof(TNavMeshPoly));
	}

	// Reallocate polys
	delete pNavMesh->m_PolysArray;
	pNavMesh->m_PolysArray = new aiSplitArray<TNavMeshPoly, NAVMESH_POLY_ARRAY_BLOCKSIZE>(pNavMesh->m_iNumPolys, pNewPolys);
	pNavMesh->SetPolysSubArrayIndices();

	delete[] pNewPolys;
	pNewPolys = NULL;

	//****************************************************************************
	// Vertices

	pNavMesh->m_iNumVertices = (u32)verts.size();

	u16 * pVerts = rage_new u16[ verts.size() * 3 ];
	int iVCount=0;
	for(vi=0; vi<verts.size(); vi++)
	{
		const Vector3 & vVert = *verts[vi];
		u16 x,y,z;
		CompressVertex(vVert, x, y, z, pNavMesh->m_pQuadTree->m_Mins, pNavMesh->m_vExtents);

		pVerts[iVCount++] = x;
		pVerts[iVCount++] = y;
		pVerts[iVCount++] = z;

		delete verts[vi];
	}

	//-----------------------
	// Reallocate vertices

	delete pNavMesh->m_CompressedVertexArray;
	pNavMesh->m_CompressedVertexArray = new aiSplitArray<CNavMeshCompressedVertex, NAVMESH_COMPRESSEDVERTEX_ARRAY_BLOCKSIZE>(pNavMesh->m_iNumVertices, (CNavMeshCompressedVertex*)pVerts);

	delete[] pVerts;
	pVerts = NULL;

	//****************************************************************************
	// Indices

	u16 * pVertexIndices = rage_new u16[indices.size()];
	for(vi=0; vi<indices.size(); vi++)
	{
		pVertexIndices[vi] = indices[vi];
	}

	delete pNavMesh->m_VertexIndexArray;
	pNavMesh->m_VertexIndexArray = new aiSplitArray<u16, NAVMESH_VERTEXINDEX_ARRAY_BLOCKSIZE>(pNavMesh->m_iSizeOfPools, pVertexIndices);
	delete[] pVertexIndices;
	pVertexIndices = NULL;

	//****************************************************************************
	// Adjacent polys

	Assert(adjPolys.size() == indices.size());

	pNavMesh->m_iSizeOfPools = (u32)adjPolys.size();

	TAdjPoly * pAdjPolys = rage_new TAdjPoly[adjPolys.size()];
	for(vi=0; vi<adjPolys.size(); vi++)
	{
		TAdjPoly & pSrcAdjPoly = adjPolys[vi];
		TAdjPoly & destAdjPoly = pAdjPolys[vi];

		pSrcAdjPoly.MakeCopy(destAdjPoly);
	}

	// Reallocate adjpolys
	delete pNavMesh->m_AdjacentPolysArray;
	pNavMesh->m_AdjacentPolysArray = new aiSplitArray<TAdjPoly, NAVMESH_ADJPOLY_ARRAY_BLOCKSIZE>(pNavMesh->m_iSizeOfPools, pAdjPolys);

	delete[] pAdjPolys;
	pAdjPolys = NULL;



	//****************************************************************************
	// Quadtree

	Vector3 vMin = pNavMesh->m_pQuadTree->m_Mins;
	Vector3 vMax = pNavMesh->m_pQuadTree->m_Maxs;

	// This will delete all children as well
	delete pNavMesh->m_pQuadTree;
	// We have to create a small one-level-deep quadtree just for the min/max extents of the navmesh
	// (this is silly, we should just be able to use a min/max stored in the navmesh)
	pNavMesh->m_pQuadTree = rage_new CNavMeshQuadTree;
	pNavMesh->m_pQuadTree->m_Mins = vMin;
	pNavMesh->m_pQuadTree->m_Mins = vMax;

	CNavGen::MakeNavMeshQuadTree(pNavMesh, vMin, vMax);

}










