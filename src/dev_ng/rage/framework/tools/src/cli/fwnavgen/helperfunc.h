//
// fwnavgen/helperfunc.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef FWNAVGEN_HELPERFUNC_H
#define FWNAVGEN_HELPERFUNC_H

#include "vector/vector3.h"

//-----------------------------------------------------------------------------

namespace rage
{

//-----------------------------------------------------------------------------

/*
PURPOSE
	This namespace contains various helper functions used by tools exporting
	navigation data.
NOTES
	Originally, many of these functions were defined in GTA's 'pathserver/ExportCollision.cpp'.
*/
namespace fwNavToolHelperFuncs
{

	// Util funcs for detecting degenerate triangles
	float CalcTriArea(Vector3 * pPts);
	float CalcTriArea(float l0, float l1, float l2);

	bool AreVerticesEqual(const Vector3 & v1, const Vector3 & v2, float fEps);

	void GetRotatedMinMax(Vector3 & vMinInOut, Vector3 & vMaxInOut, const Matrix34 & mat);

	bool BoundingBoxesOverlap(const Vector3 & vMin1, const Vector3 & vMax1, const Vector3 & vMin2, const Vector3 & vMax2);
	bool BoundingBoxesOverlapXY(const Vector3 & vMin1, const Vector3 & vMax1, const Vector3 & vMin2, const Vector3 & vMax2);

	void TransformObjectAABB(const Matrix34& objMtrx,
			const Vector3& vLocalBoundMin, const Vector3& vLocalBoundMax,
			Vector3& aabbMinOut, Vector3& aabbMaxOut);

}	// namespace fwNavToolHelperFuncs

//-----------------------------------------------------------------------------

}	// namespace rage

//-----------------------------------------------------------------------------

#endif	// FWNAVGEN_HELPERFUNC_H

// End of file fwnavgen/helperfunc.h
