//
// fwnavgen/baseexportcollisiongrid.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "baseexportcollisiongrid.h"
#include "config.h"
#include "ai/navmesh/navmesh.h"
#include "ai/navmesh/navmeshextents.h"
#include "system/nelem.h"
#include "system/param.h"

using namespace rage;

XPARAM(exportregion);
XPARAM(exportsectors);
XPARAM(exportcontinuefrom);
XPARAM(exportdlc);

//-----------------------------------------------------------------------------

fwExportCollisionGridTool::fwExportCollisionGridTool(fwLevelProcessTool * pTool)
	: fwExportCollisionBaseTool(pTool)
		, m_vCentreOfCurrentNavMesh(0,0,0)
		, m_fWidthOfNavMesh(0.0f)
		, m_fDepthOfNavMesh(0.0f)
		, m_fCollisionExportHeight(0.0f)
		, m_iStartSectorX(0)
		, m_iNumSectorsX(0)
		, m_iStartSectorY(0)
		, m_iNumSectorsY(0)
		, m_iEndSectorX(0)
		, m_iEndSectorY(0)
		, m_iCurrentSectorX(0)
		, m_iCurrentSectorY(0)
		, m_bExporting(false)
		, m_iCurrentArea(-1)
{
}


//***************************************************************************
//	OpenTriFile
//	Opens the ".tri" file which contains the collision to be used to create
//	this navmesh.  If the pNameOfModel is supplied, then this will be used
//	to name the triangle file (appended with .tri") - otherwise the name will
//	be created from the currently exporting navmesh map section.

bool fwExportCollisionGridTool::OpenTriFile(const char *pNameToUse)
{
	const fwNavGenConfig &cfg = fwNavGenConfigManager::GetInstance().GetConfig();
	const Vector2 worldMin(cfg.m_WorldMinX, cfg.m_WorldMinY);
	return BaseOpenTriFile(pNameToUse,
			worldMin,
			m_vNavMeshMins, m_vNavMeshMaxs,
			m_iCurrentSectorX, m_iCurrentSectorY,
			CPathServerExtents::m_iNumSectorsPerNavMesh,
			CPathServerExtents::GetWorldWidthOfSector()
			);
}


static int GetIntegerParams(int* params, int paramCountMax, const char* pParamTxt) // returns number of params
{
	int paramCount = 0;
	char temp[256] = "";
	strcpy(temp, pParamTxt);
	const char* pParam = strtok(temp, ",\t ");

	while (pParam && paramCount < paramCountMax)
	{
		params[paramCount++] = atoi(pParam);
		pParam = strtok(NULL, ",\t ");
	}

	return paramCount;
}

bool fwExportCollisionGridTool::ParseArgumentsAndStartExportCollision()
{
	const char * pParamTxt = NULL;
	bool success = false;

	//-----------------------------------------
	// Export navmeshes in world-coords region

	if(PARAM_exportregion.Get(pParamTxt))
	{
		Displayf("CNavMeshDataExporterTool::NotifyMainGameLoopHasStarted() - exportregion\n");

		int params[4];
		int paramCount = GetIntegerParams(params, NELEM(params), pParamTxt);
		int iStartX = 0;
		int iStartY = 0;
		int iEndX = -1;
		int iEndY = -1;

		if (paramCount == 4)
		{
			iStartX = params[0];
			iStartY = params[1];
			iEndX = params[2];
			iEndY = params[3];
		}
		else if (paramCount == 3)
		{
			int iCentreX = params[0];
			int iCentreY = params[1];
			int iRadius = params[2];

			if (iRadius < 0)
			{
				// pixel coordinates on 1-resolution heightmap (assuming -res=1)
				// ...
				Quitf("pixel region not implemented");
			}

			iStartX = iCentreX - iRadius;
			iStartY = iCentreY - iRadius;
			iEndX = iCentreX + iRadius;
			iEndY = iCentreY + iRadius;
		}

		if (iStartX <= iEndX &&
			iStartY <= iEndY)
		{
			success = StartExportCollisionForSpecifiedWorldCoords((float)iStartX, (float)iStartY, (float)iEndX, (float)iEndY);
		}
	}

	//---------------------------------------------------
	// Export navmeshes specified in sector coordinates

	else if(PARAM_exportsectors.Get(pParamTxt))
	{
		Displayf("CNavMeshDataExporterTool::NotifyMainGameLoopHasStarted() - exportsectors\n");

		int params[4];
		int paramCount = GetIntegerParams(params, NELEM(params), pParamTxt);
		int iStartX = 0;
		int iStartY = 0;
		int iEndX = -1;
		int iEndY = -1;

		if (paramCount == 4)
		{
			iStartX = params[0];
			iStartY = params[1];
			iEndX = params[2];
			iEndY = params[3];
		}
		else if (paramCount == 3)
		{
			int iCentreX = params[0];
			int iCentreY = params[1];
			int iRadius = params[2];

			iStartX = iCentreX - iRadius;
			iStartY = iCentreY - iRadius;
			iEndX = iCentreX + iRadius;
			iEndY = iCentreY + iRadius;
		}
		else if (paramCount == 2)
		{
			iStartX = params[0];
			iStartY = params[1];
			iEndX = params[0] + 1;
			iEndY = params[1] + 1;
		}

		if (iStartX <= iEndX &&
			iStartY <= iEndY)
		{
			success = StartExportCollisionForSpecifiedNavMeshes(iStartX, iStartY, iEndX, iEndY);
		}
	}

	else if(PARAM_exportcontinuefrom.Get(pParamTxt))
	{
		Displayf("CNavMeshDataExporterTool::NotifyMainGameLoopHasStarted() - exportcontinuefrom\n");

		int params[4];
		int paramCount = GetIntegerParams(params, NELEM(params), pParamTxt);
		int iStartX = -1;
		int iStartY = -1;

		if (paramCount == 2)
		{
			iStartX = params[0];
			iStartY = params[1];
			if (iStartX >= 0 && iStartY >= 0)
			{
				success = StartExportCollisionForAllNavMeshesFrom(iStartX, iStartY);
			}
		}
	}

	else if(PARAM_exportdlc.Get(pParamTxt))
	{
		Displayf("CNavMeshDataExporterTool::NotifyMainGameLoopHasStarted() - exportdlc\n");
		Displayf("Parsing DLC areas file \"%s\"", pParamTxt);

		bool bLoadedOk = CDLCArea::LoadDLCAreas(pParamTxt, m_DLCAreas);
		Displayf("Loaded ok : %s", bLoadedOk ? "true":"false");

		if(!bLoadedOk || m_DLCAreas.GetCount()==0)
			return false;

		m_iCurrentArea = 0;

		success = StartExportCollisionForNextRegion(m_iCurrentArea);
	}

	//-------------------------------------
	// Export all navmeshes in the map

	else
	{
		Displayf("CNavMeshDataExporterTool::NotifyMainGameLoopHasStarted() - exportall\n");
		success = StartExportCollisionForAllNavMeshes();
	}

	return success;
}


void fwExportCollisionGridTool::GenerateFileNames(char* triFileNameOut, int maxTriFileNameLen,
		char* extrasFileNameOut, int maxExtrasFileNameLen) const
{
	// Create the filename for our ".tri" file
	char triName[256];
	char extrasName[256];

	// create tri files
	formatf(triName, "navmesh[%i][%i].tri", m_iCurrentSectorX, m_iCurrentSectorY);
	formatf(triFileNameOut, maxTriFileNameLen, "%s\\%s", m_OutputPath, triName);

	// Create the filename for the "extras.dat" file.  We may or may not use this, depending upon what entities are in this navmesh area.
	formatf(extrasName, "Extras_%i_%i.dat", m_iCurrentSectorX, m_iCurrentSectorY);
	formatf(extrasFileNameOut, maxExtrasFileNameLen, "%s\\%s", m_OutputPath, extrasName);
}


bool fwExportCollisionGridTool::StartExportCollisionForSpecifiedNavMeshes(u32 iStartSectorX, u32 iStartSectorY, u32 iEndSectorX, u32 iEndSectorY)
{
	const u32 iNumSectorsPerNavMesh = CPathServerExtents::m_iNumSectorsPerNavMesh;

	// The number of sectors in the specified block
	m_iStartSectorX = (iStartSectorX / iNumSectorsPerNavMesh) * iNumSectorsPerNavMesh; // round down
	m_iStartSectorY = (iStartSectorY / iNumSectorsPerNavMesh) * iNumSectorsPerNavMesh;
	m_iEndSectorX = ((iEndSectorX + iNumSectorsPerNavMesh - 1) / iNumSectorsPerNavMesh) * iNumSectorsPerNavMesh; // round up
	m_iEndSectorY = ((iEndSectorY + iNumSectorsPerNavMesh - 1) / iNumSectorsPerNavMesh) * iNumSectorsPerNavMesh;

	return StartCollisionExport();
}

bool fwExportCollisionGridTool::StartExportCollisionForSpecifiedWorldCoords(float fStartX, float fStartY, float fEndX, float fEndY)
{
	u32 iSectorStartX = (u32)Max<int>(0, CPathServerExtents::GetWorldToSectorX(fStartX));
	u32 iSectorStartY = (u32)Max<int>(0, CPathServerExtents::GetWorldToSectorY(fStartY));
	u32 iSectorEndX = (u32)Max<int>(0, CPathServerExtents::GetWorldToSectorXRoundUp(fEndX));
	u32 iSectorEndY = (u32)Max<int>(0, CPathServerExtents::GetWorldToSectorYRoundUp(fEndY));

	return StartExportCollisionForSpecifiedNavMeshes(iSectorStartX, iSectorStartY, iSectorEndX, iSectorEndY);
}

bool fwExportCollisionGridTool::StartExportCollisionForAllNavMeshes()
{
	// The number of sectors in the world
	m_iStartSectorX = 0;
	m_iStartSectorY = 0;
	m_iEndSectorX = CPathServerExtents::m_iNumNavMeshesInX * CPathServerExtents::m_iNumSectorsPerNavMesh;
	m_iEndSectorY = CPathServerExtents::m_iNumNavMeshesInY * CPathServerExtents::m_iNumSectorsPerNavMesh;

	return StartCollisionExport();
}

bool fwExportCollisionGridTool::StartExportCollisionForAllNavMeshesFrom(int iX, int iY)
{
	// The number of sectors in the world
	m_iStartSectorX = 0;
	m_iStartSectorY = 0;
	m_iEndSectorX = CPathServerExtents::m_iNumNavMeshesInX * CPathServerExtents::m_iNumSectorsPerNavMesh;
	m_iEndSectorY = CPathServerExtents::m_iNumNavMeshesInY * CPathServerExtents::m_iNumSectorsPerNavMesh;

	return (StartCollisionExport() && StartCollisionExportFrom(iX, iY));
}

bool fwExportCollisionGridTool::StartCollisionExport()
{
	Assert(!m_bExporting);

	m_fWidthOfNavMesh = CPathServerExtents::GetWorldWidthOfSector() * CPathServerExtents::m_iNumSectorsPerNavMesh;
	m_fDepthOfNavMesh = CPathServerExtents::GetWorldWidthOfSector() * CPathServerExtents::m_iNumSectorsPerNavMesh;

	m_bExporting = true;

	m_iCurrentSectorX = m_iStartSectorX;
	m_iCurrentSectorY = m_iStartSectorY;
	m_iNumSectorsX = CPathServerExtents::m_iNumSectorsPerNavMesh;
	m_iNumSectorsY = CPathServerExtents::m_iNumSectorsPerNavMesh;

	CalcCurrentNavMeshExtents();

	UpdateStreamingPos(m_vCentreOfCurrentNavMesh);

	return true;
}

bool fwExportCollisionGridTool::StartCollisionExportFrom(int iX, int iY)
{
	m_fWidthOfNavMesh = CPathServerExtents::GetWorldWidthOfSector() * CPathServerExtents::m_iNumSectorsPerNavMesh;
	m_fDepthOfNavMesh = CPathServerExtents::GetWorldWidthOfSector() * CPathServerExtents::m_iNumSectorsPerNavMesh;

	m_bExporting = true;

	m_iCurrentSectorX = m_iStartSectorX;
	m_iCurrentSectorY = m_iStartSectorY;
	m_iNumSectorsX = CPathServerExtents::m_iNumSectorsPerNavMesh;
	m_iNumSectorsY = CPathServerExtents::m_iNumSectorsPerNavMesh;

	CalcCurrentNavMeshExtents();

	UpdateStreamingPos(m_vCentreOfCurrentNavMesh);

	m_iCurrentSectorX = iX;
	m_iCurrentSectorY = iY;
		
	return true;
}
bool fwExportCollisionGridTool::StartExportCollisionForNextRegion(int iAreaIndex)
{
	if(iAreaIndex >= m_DLCAreas.GetCount())
		return false;

	CDLCArea * pArea = m_DLCAreas[iAreaIndex];

	return StartExportCollisionForSpecifiedWorldCoords(pArea->m_vMin.x, pArea->m_vMin.y, pArea->m_vMax.x, pArea->m_vMax.y);
}
void fwExportCollisionGridTool::CalcCurrentNavMeshExtents()
{
	if(!m_bExporting)
	{
		return;
	}

	int iX = m_iCurrentSectorX / CPathServerExtents::m_iNumSectorsPerNavMesh;
	m_iCurrentSectorX = iX * CPathServerExtents::m_iNumSectorsPerNavMesh;
	int iY = m_iCurrentSectorY / CPathServerExtents::m_iNumSectorsPerNavMesh;
	m_iCurrentSectorY = iY * CPathServerExtents::m_iNumSectorsPerNavMesh;

	float fSectorX = (float)m_iCurrentSectorX;
	float fSectorY = (float)m_iCurrentSectorY;

	Vector3 vEntireMins, vEntireMaxs, vEntireCentre;

	const fwNavGenConfig &cfg = fwNavGenConfigManager::GetInstance().GetConfig();

	vEntireMins.x = cfg.m_WorldMinX + (fSectorX * CPathServerExtents::GetWorldWidthOfSector());
	vEntireMins.y = cfg.m_WorldMinY + (fSectorY * CPathServerExtents::GetWorldWidthOfSector());
	vEntireMins.z = cfg.m_ExporterMinZCutoff;

	vEntireMaxs.x = vEntireMins.x + m_fWidthOfNavMesh;
	vEntireMaxs.y = vEntireMins.y + m_fDepthOfNavMesh;
	vEntireMaxs.z = cfg.m_ExporterMaxZCutoff;

	vEntireCentre = (vEntireMins + vEntireMaxs) / 2.0f;
	vEntireCentre.z = m_fCollisionExportHeight;

	m_vNavMeshMins = vEntireMins;
	m_vNavMeshMaxs = vEntireMaxs;
	m_vCentreOfCurrentNavMesh = vEntireCentre;
}


bool fwExportCollisionGridTool::NextCellOrSubSection()
{
	GridCellFinished();

	// Step across map..
	m_iCurrentSectorX += CPathServerExtents::m_iNumSectorsPerNavMesh;

	if(m_iCurrentSectorX >= m_iEndSectorX)
	{
		m_iCurrentSectorX = m_iStartSectorX;
		m_iCurrentSectorY += CPathServerExtents::m_iNumSectorsPerNavMesh;

		if(m_iCurrentSectorY >= m_iEndSectorY)
		{
			if(m_iCurrentArea != -1)
			{
				m_iCurrentArea++;
				if(m_iCurrentArea < m_DLCAreas.GetCount())
				{
					StartExportCollisionForNextRegion(m_iCurrentArea);
					return true;
				}
			}

			m_bExporting = false;

			GridProcessFinished();

			return false;
		}
	}

#if !HEIGHTMAP_GENERATOR_TOOL
	printf("Now Starting to export the next navmesh [%i][%i]..\n", m_iCurrentSectorX, m_iCurrentSectorY);
#endif

	CalcCurrentNavMeshExtents();

	UpdateStreamingPos(m_vCentreOfCurrentNavMesh);

	return true;
}

//-----------------------------------------------------------------------------

// End of file fwnavgen/baseexportcollisiongrid.cpp
