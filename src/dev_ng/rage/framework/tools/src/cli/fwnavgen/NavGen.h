#ifndef NAVGEN_H
#define NAVGEN_H


//*********************************************************************************
//
//	Title	: NavGen.h
//	Author	: James Broad
//	Date	: Jan 2005
//
//	The classes in this file are used to automatically generate a navigation mesh
//	for the AI & pathfinding systems.
//
//	Algorithm:
//	----------
//
//	Sampling:
//
//	The approach is to create a large densely packed grid of nodes by repeatedly
//	sampling heights from the world's collision geometry with vertical LOS tests.
//  For any given (x,y) coordinate we may have multiple sample plots at different
//	height.
//	We will then triangulate the samples in a regular manner.  For each sampled
//	node we will try to find two nodes above & right, and left & down - with which
//	to create two triangles.  These nodes must all be within a certain height
//	interval of each other to be considered for triangulation.  Additionally, there
//	must exist a clear LOS between all 3 nodes to form a triangle (this prevents
//	the surface from intersecting the world's collision geometry).
//	We now have a very fine mesh across the entire world.  The next step is to
//	simplify it using progressive-meshing techniques, by collapsing edges where
//	the surface curvature is not too high (using quadric error metric method).
//	We may also mark certain nodes as 'non-collapsible' (for example - pavements)
//	which means that their approximate extents are preserved in the mesh.
//	The simplified triangle mesh is then saved out as a 3ds .asc file.
//
//
//	Merging :
//
//	The next phase is to load the .asc files, and to repeatedly merge triangles
//	into convex polys - and polys into larger convex polys, restricted by a
//	limit on polygon size and maximum edge length.  We mark each poly with a
//	flag (small, normal, large) depending upon the final area of the poly.
//	We can now eliminate any redundant vertices, and create a CNavMesh class
//	which we save out.
//
//
//	Stitching :
//
//	Given a large amount of CNavMeshes covering the whole game world, the next
//	step is to load them all in and to establish polygon adjacency across the
//	borders of each mesh.  This is done for each polygon, by finding the
//	adjacent polygon in the neighbouring navmesh which has the largest edge
//	overlap in a cardinal (x,y) axis.
//	Once this is done for all meshes, they are saved out again.
//
//
//	Analysis:
//
//	This phase identifies polygon adjacencies which depend upon an action such
//	as climbing, jumping, dropping, etc.  All the nav meshes are loaded in,
//	along with their respective collision geometry.  We then find every polygon
//	edge which doesn't have an adjacent poly.  For each of these polygon edges,
//	we perform a series of LOS tests to nearby polygons which also have non-
//	adjacent edges in an attempt to identify places where a character could
//	jump over an obstacle, or drop down to another polygon, etc.
//	Once this is done for all meshes, we save them out again.
//
//*********************************************************************************

//#include <crtdbg.h>
#include "NavGen_Octree.h"
#include "NavGen_Utils.h"
#include "NavGen_Voxel.h"
#include "NavGen_AntiAlias.h"
#include "NavGen_Types.h"

#include "atl\array.h"
#include "vector\matrix34.h"

#include "fwgeovis/geovis.h"

using namespace std;

#include <vector>
#include <list>

namespace rage
{

class CNavGen;
class CNavGenParams;
class CNavSurfaceTri;
class CNavGenNode;
class CNavTriEdge;
class CNavGenDynObject;



class CNavGenTri;


#if __DEV
#define NAVGEN_SPEW_DEBUG
#endif

extern char tmpBuf[256];
extern char * g_pBlankLine;

#define ID_NODEGRAPH_INVALID		0xFFFF

void AssociateTriAndNode(CNavSurfaceTri * pTri, CNavGenNode * pNode);
void DisassociateTriAndNode(CNavSurfaceTri * pTri, CNavGenNode * pNode);
void EstablishTriAdjacency(CNavSurfaceTri * pTri);

// If set, this will cause Sleep(0) to be called frequently
extern bool g_bProcessInBackground;

// If this is set, then ONLY the navmesh files in the command-file will be processed.
extern bool g_bOnlyNamedFiles;

#define NAVGEN_OBSOLETE 0
#define MAX_SMOOTHING_ITERS_ON_NAVGENNODE					16
#define __USE_GEOMCULLER_FOR_DIRECTIONAL_COVER				1
#define __SPLIT_POLYGONS_AFTER_MERGE						1


#define NAVGEN_COLLISION_TYPES					(TColPolyData::COLLISION_TYPE_MOVER | TColPolyData::COLLISION_TYPE_RIVER | TColPolyData::COLLISION_TYPE_STAIRSLOPE)
#define COVER_COLLISION_TYPES					(TColPolyData::COLLISION_TYPE_MOVER | TColPolyData::COLLISION_TYPE_COVER)
#define NAVGEN_COLLISION_TYPES_EXCEPT_RIVER		(TColPolyData::COLLISION_TYPE_MOVER | TColPolyData::COLLISION_TYPE_STAIRSLOPE)

//***************************************************************************************************
//	CNavGenParams
//	This class hold the parameters to the node generator
//***************************************************************************************************

class CNavGenParams
{
public:
	CNavGenParams(void);
	~CNavGenParams(void);

	void Init(
		const Vector3 & vWorldMin, const Vector3 & vWorldMax,
		const Vector3 & vUpVector,
		const Vector3 & vNavMeshMin, const Vector3 & vNavMeshMax,
		bool bStoreSplitPolysInOctreeLeaves,	// should usually be false for efficiency
		float fSampleResolution
	);

	// The maximum permitted distance between any two nodes
	float m_fMaxTriangleSideLength;
	float m_fMinTriangleSideLength;
	float m_fMinTriangleAngle;

	float m_fMinTriangleArea;
	float m_fMaxTriangleArea;

	int m_iMaxNumberOfTrianglesSurroundingAnyNode;

	// Any polys whose surface normal deviates from the up-vector by over this angle, will not become part of the navmesh.
	float m_fMaxAngleForPolyToBeInNavMesh;
	float m_fMaxAngleForWaterPolyToBeInNavMesh;
	// Any polys whose surface normal deviates from the up-vector by over this angle, will be marked as "steep" and normal paths will not traverse them.
	float m_fAngleForNavMeshPolyToBeTooSteep;

	// The up vector, defaults at (0, 0, 1.0f)
	Vector3 m_vUpVector;

	// These two members describe the leaf-creation conditions.  If either are met, then a leaf node is created.
	float m_fMinimumLeafNodeSize;
	u32 m_iDesiredNumTrisPerLeaf;

	// This defines how closely initially placed nodes are to be packed together.
	// Smaller values will lead to more accurate geometry analysis, but will take longer to process.
	float m_fDistanceToInitiallySeedNodes;

	// This is the maximum height difference allowable between any two adjacent nodes, after
	// which triangulation between them may not occur
	float m_TriangulationMaxHeightDiff;

	float m_fMinZDistBetweenSamples;

	// This is the height above the node's base (ground level) at which the triangulation
	// is performed.  Line-of-sight tests and the nav surface creation will occur at this height.
	float m_HeightAboveNodeBaseAtWhichToTriangulate;

	// This is the height which must be clear of obstructions above the triangulated navmesh
	float m_fTestClearHeightInTriangulation;

	// This value is the maximum sudden height-change allowable underneath any triangle edge during
	// navmesh generation.  This stops the navmesh riding up onto low walls, etc - but provides enough
	// tolerance for it to go up over pavement curbs, etc.
	float m_MaxHeightChangeUnderTriangleEdge;
	bool m_bJitterSamplesWhenCreatingNodes;
	float m_OptimizeMaxQuadricErrorMetric;

	// The total extents that the node graph must cover
	Vector3 m_vAreaMin;
	Vector3 m_vAreaMax;

	// All geometry outside of these extents will be chopped away
	Vector3 m_vSectorMins;
	Vector3 m_vSectorMaxs;
	u32 m_iSectorStartX;
	u32 m_iSectorStartY;
	u32 m_iNumSectorsX;
	u32 m_iNumSectorsY;

	// PURPOSE:	True if we're building a navmesh intended for aerial navigation.
	bool m_bAerialNavigation;

	// This is turned off by default.  Creating navmesh underwater will result in larger navmeshes because the sea-bed will
	// generate navmesh, and we will have TVolumePolyInfo created for each polygon.
	// Underwater navigation has problems currently - it may well be better to adopt a different waypoint-based pathfinder.
	bool m_bCreateNavmeshUnderwater;

	// Fix aliasing by connecting gaps along exterior edge of navmesh
	bool m_bFixJaggiesAfterTriangulation;

	// This is useful for debugging the octree, and allows viewing of the split polys
	bool m_bAlsoStoreSplitPolysInOctreeLeaves;

	// This is the distance by which collision triangles are extruded for cutting surface-types into the mesh
	float m_fDistToExtrudeCollisionTris;

	bool m_bDontDoOptimisation;
	bool m_bDontDoPolygonMerging;
	bool m_bMoveNodesAwayFromWalls;
	bool m_bTagPavementNavMesh;
};


struct TPathStackNode
{
	CNavSurfaceTri * m_pTri;
	TPathStackNode * m_pNext;

	float m_fCost;
};

//#define	MAX_PATH_STACK_SIZE		1024

#define MAX_WATER_DEPTH_CHECK	750

class TVec3
{
public:
	TVec3() { }
	inline TVec3(const float fX, const float fY, const float fZ)
	{
		x = fX; y = fY; z = fZ;
	}
	inline TVec3(const Vector3 & v)
	{
		x = v.x; y = v.y; z = v.z;
	}
	float x,y,z;
};

//*********************************************************************
//	TVertexEntry & TMergePoly
//	These are used when merging triangles to create an optimal
//	CNavMesh class.
//*********************************************************************
struct TVertexEntry
{
	TVertexEntry()
	{
		m_iIndex = -1;
		m_iNumReferences = 0;
	}
	TVertexEntry(const Vector3 & vec)
	{
		m_iIndex = -1;
		m_iNumReferences = 0;
		m_Vertex = vec;
	}

	s32 m_iIndex;			// the index in the list
	u32 m_iNumReferences;	// at some point we'll total up the num references - and remove if zero
	Vector3 m_Vertex;

	atArray<TMergePoly*> m_SurroundingPolys;

	inline void AddSurroundingPoly(TMergePoly * pPoly)
	{
		Assert(!HasSurroundingPoly(pPoly));
		m_SurroundingPolys.Grow() = pPoly;
	}
	inline bool RemoveSurroundingPoly(TMergePoly * pPoly)
	{
		atArray<TMergePoly*>::iterator iter;
		for(iter=m_SurroundingPolys.begin(); iter!=m_SurroundingPolys.end(); iter++)
		{
			if(*iter==pPoly)
			{
				m_SurroundingPolys.erase(iter);
				return true;
			}
		}
		return false;
	}

	inline bool HasSurroundingPoly(TMergePoly * pPoly)
	{
		atArray<TMergePoly*>::iterator iter;
		for(iter=m_SurroundingPolys.begin(); iter!=m_SurroundingPolys.end(); iter++)
		{
			if(*iter==pPoly)
			{
				return true;
			}
		}
		return false;
	}
	inline void CopySurroundingPolys(const TVertexEntry * pOther)
	{
		atArray<TMergePoly*>::const_iterator iter;
		for(iter=pOther->m_SurroundingPolys.begin(); iter!=pOther->m_SurroundingPolys.end(); iter++)
		{
			m_SurroundingPolys.Grow() = *iter;
		}
	}
};

#define CleanDeleteStlVector(n) { for(u32 TMPI=0; TMPI<n.size(); TMPI++) delete n[TMPI]; }

struct TMergePoly
{
public:
	atArray<TVertexEntry*> m_Vertices;
	atArray<TMergePoly*> m_AdjacentPolys;

	float m_fArea;
	u32 m_iIndex;	// the index in the list
	Vector3 m_vPlaneNormal;
	float m_fPlaneDist;
	Vector3 m_vOrigin;
	float m_fBoundingRadius;
	//float m_fPedDensity;
	TColPolyData m_colPolyData;
	u8 m_iBakedInBitField;
	int m_iFreeSpaceTopZ;

	u32 m_bInShelter				:1;
	u32 m_bWater					:1;
	u32 m_bTooSteep					:1;
	u32 m_bFragment					:1;
	u32 m_bIntersectsEntities		:1;
	u32 m_bAddedDuringPolyBisect	:1;
	u32 m_bDoNotMerge				:1;

	TMergePoly()
	{
		m_Vertices.Reserve(16);
		m_AdjacentPolys.Reserve(16);

		m_fArea = 0.0f;
		m_iIndex = 0;
		m_vPlaneNormal.Zero();
		m_fPlaneDist = 0.0f;
		m_vOrigin.Zero();
		m_fBoundingRadius = 0.0f;
		TColPolyData m_colPolyData;
		m_iBakedInBitField = 0;
		m_iFreeSpaceTopZ = 0;

		m_bInShelter = false;
		m_bWater = false;
		m_bTooSteep = false;
		m_bFragment = false;
		m_bIntersectsEntities = false;
		m_bAddedDuringPolyBisect = false;
	}

	inline void CopyData(const TMergePoly * pSrc)
	{
		m_vPlaneNormal = pSrc->m_vPlaneNormal;
		m_fPlaneDist = pSrc->m_fPlaneDist;
		//m_fPedDensity = pSrc->m_fPedDensity;
		m_colPolyData = pSrc->m_colPolyData;
		m_iBakedInBitField = pSrc->m_iBakedInBitField;
		m_iFreeSpaceTopZ = pSrc->m_iFreeSpaceTopZ;
		m_bInShelter = pSrc->m_bInShelter;
		m_bWater = pSrc->m_bWater;
		m_bTooSteep = pSrc->m_bTooSteep;
	}

	float CalcAreaAndBounds();

	inline bool operator > (const TMergePoly & p2)
	{
		return m_fArea > p2.m_fArea;
	}
	inline bool operator < (const TMergePoly & p2)
	{
		return m_fArea < p2.m_fArea;
	}

	inline bool UsesVertex(TVertexEntry * pVert)
	{
		for(s32 v=0; v<m_Vertices.GetCount(); v++)
			if(m_Vertices[v]==pVert)
				return true;
		return false;
	}

	inline bool HasVertex(const Vector3 & vec, const float fEps=SMALL_FLOAT)
	{
		for(s32 v=0; v<m_Vertices.GetCount(); v++)
		{
			if(m_Vertices[v]->m_Vertex.IsClose(vec, fEps))
				return true;
		}
		return false;
	}

	inline bool InsertVertexBetween(TVertexEntry * pNew, TMergePoly * pAdjPoly, const TVertexEntry * pPrev, const TVertexEntry * pNext)
	{
		Assert(m_AdjacentPolys.size()==m_Vertices.size());

		TVertexEntry * pLastVert = m_Vertices[ m_Vertices.GetCount()-1 ];
		for(s32 v=0; v<m_Vertices.GetCount(); v++)
		{
			TVertexEntry * pVert = m_Vertices[ v ];

			if( (pPrev == pLastVert && pNext == pVert) ||
				(pPrev == pVert && pNext == pLastVert) )
			{
				m_Vertices.insert( m_Vertices.begin()+v, pNew );
				m_AdjacentPolys.insert( m_AdjacentPolys.begin()+v, pAdjPoly );
				return true;
			}

			pLastVert = pVert;
		}
		Assert(false);
		return false;
	}

	// NAME : BisectAdjacentEdge
	// PURPOSE : When bisecting a TMergePoly we have to also bisect the adjacencies which link back to it in neighbouring polygons
	// This function serves that purpose, and is called on an adjacent polygon to removed references to the old (bisected) poly
	// and replace with two new edges, and the new vertex in the middle.
	bool BisectAdjacentEdge(TMergePoly * pOldPoly, TMergePoly * pNewA, TVertexEntry * pMiddleVertex, TMergePoly * pNewB);

	void ReplaceAdjacency(TMergePoly * pOld, TMergePoly * pNew)
	{
		for(s32 a=0; a<m_AdjacentPolys.GetCount(); a++)
			if(m_AdjacentPolys[a] == pOld)
				m_AdjacentPolys[a] = pNew;
	}

	void ReplaceVertex(TVertexEntry * pOld, TVertexEntry * pNew)
	{
		for(s32 v=0; v<m_Vertices.size(); v++)
		{
			if(m_Vertices[v] == pOld)
				m_Vertices[v] = pNew;
		}
	}

	inline bool DoesPolyMatch(vector<TVec3> & verts, const float fEps=0.1f)
	{
		if((s32)verts.size()!=m_Vertices.size())
			return false;

		s32 iNumMatching = 0;
		for(s32 v1=0; v1<m_Vertices.size(); v1++)
		{
			for(u32 v2=0; v2<verts.size(); v2++)
			{
				const TVec3 & vec = verts[v2];
				if(m_Vertices[v1]->m_Vertex.IsClose(Vector3(vec.x, vec.y, vec.z), fEps))
				{
					iNumMatching++;
					break;
				}
			}
		}
		return (iNumMatching==m_Vertices.size());
	}

	static inline int GetAdjacentPolys(const TVertexEntry * pVert1, const TVertexEntry * pVert2, TMergePoly ** ppPoly1, TMergePoly ** ppPoly2)
	{
		Assert(ppPoly1 && ppPoly2);

		int iNum = 0;
		TMergePoly * pSharedPolys[2];

		for(s32 i=0; i<pVert1->m_SurroundingPolys.size(); i++)
		{
			TMergePoly * pAdj1 = pVert1->m_SurroundingPolys[i];
			for(s32 j=0; j<pVert2->m_SurroundingPolys.size(); j++)
			{
				TMergePoly * pAdj2 = pVert2->m_SurroundingPolys[j];

				if(pAdj1==pAdj2)
				{
					Assert(iNum < 2);
					pSharedPolys[iNum++] = pAdj1;
					break;
				}
			}
			if(iNum==2)
				break;
		}

		*ppPoly1 = NULL;
		*ppPoly2 = NULL;
		if(iNum==1)
			*ppPoly1 = pSharedPolys[0];
		if(iNum==2)
			*ppPoly1 = pSharedPolys[1];
		return iNum;
	}

	bool TestAdjacencies();
};

class CHierarchicalNode;


class CHierarchicalLink
{
public:
	CHierarchicalLink()
	{
		m_iNavMeshIndex = NAVMESH_NAVMESH_INDEX_NONE;
		m_iPolyIndex = NAVMESH_POLY_INDEX_NONE;
		m_pNode = NULL;
		m_bCheckedForHeightDifferences = false;
		m_bCheckedForWidth = false;
		m_fWidthToLeft = 0.0f;
		m_fWidthToRight = 0.0f;
	}
	u32 m_iNavMeshIndex;
	u32 m_iPolyIndex;
	CHierarchicalNode * m_pNode;
	bool m_bCheckedForHeightDifferences;
	bool m_bCheckedForWidth;
	float m_fWidthToLeft;
	float m_fWidthToRight;
};

class CHierarchicalNode
{
public:
	CHierarchicalNode()
	{
		m_bOnEdgeOfMesh = false;
		m_bAddedToFollowTerrain = false;
		m_vPosition = Vector3(0.0f,0.0f,0.0f);
		m_pPoly = NULL;
		m_pNavMesh = NULL;
		m_fArea = 0.0f;
		m_iIndexOfThisNode = -1;
		m_iTimestamp = 0;
	}
	bool m_bOnEdgeOfMesh;
	bool m_bAddedToFollowTerrain;
	u32 m_iEdgeFlags;
	Vector3 m_vPosition;
	atArray<CHierarchicalLink> m_Links;
	CNavMesh * m_pNavMesh;
	TNavMeshPoly * m_pPoly;
	float m_fArea;
	s32 m_iIndexOfThisNode;
	u32 m_iTimestamp;
};

class CHierarchical
{
public:
	CHierarchical()
	{
		m_pNavMesh = NULL;
		m_iNumNodesActive = 0;
		m_pInGameNavData = NULL;
	}
	~CHierarchical()
	{
		for(int n=0; n<m_Nodes.size(); n++)
			delete m_Nodes[n];
		delete m_pInGameNavData;
	}
	CNavMesh * m_pNavMesh;
	atArray<CHierarchicalNode*> m_Nodes;
	s32 m_iNumNodesActive;
	CHierarchicalNavData * m_pInGameNavData;
};

class CHierarchicalBlock
{
public:
	CHierarchicalBlock()
	{
		m_iStartX = -1;
		m_iStartY = -1;
		m_vBlockMin = Vector3(0.0f, 0.0f, 0.0f);
		m_vBlockMax = Vector3(0.0f, 0.0f, 0.0f);
		m_pHierarchical = NULL;
	}
	int m_iStartX;
	int m_iStartY;
	Vector3 m_vBlockMin;
	Vector3 m_vBlockMax;
	CHierarchicalNavData * m_pHierarchical;
	vector<CHierarchicalNode*> m_Nodes;
};

//*******************************************************************************************
//	NavMesh Analysis
//*******************************************************************************************

struct TCoverPointLinkUp
{
	// The direction from the src coverpoint
	int m_iDirFromSrcCoverPoint;
	// The coverpoint
	CNavMeshCoverPoint * m_pDestCoverPoint;
	// How suitable this coverpoint is.  The lower the value, the better.
	float m_fSuitability;
};


struct TNavGenEdgeGroup
{
	TNavGenEdgeGroup(void)
	{
		m_iIndexOfGroup = -1;
		m_iEdge = -1;
		m_vEdgeCentroid = Vector3(0,0,0);
		m_iIndexOfNeighbouringGroup = -1;
	}

	s32 m_iIndexOfGroup;
	s32 m_iEdge;
	vector<TNavMeshPoly *> m_Polys;
	Vector3 m_vEdgeCentroid;
	s32 m_iIndexOfNeighbouringGroup;
};

struct TPrecalcPath
{
	TPrecalcPath(void)
	{
		m_fPathLength = 0.0f;
		iStartEdge = -1;
		iEndEdge = -1;
	}

	int iStartEdge;
	int iEndEdge;
	float m_fPathLength;
	atArray<Vector3> m_PathPoints;
};

class CNavGenEdgeGroups
{
public:
	CNavGenEdgeGroups(void)
	{
		pNavMesh = NULL;
	}
	~CNavGenEdgeGroups(void)
	{
		for(u32 e=0; e<m_EdgeGroups.size(); e++)
		{
			TNavGenEdgeGroup * pEdgeGroup = m_EdgeGroups[e];
			delete pEdgeGroup;
		}
	}

	inline TPrecalcPath * GetPrecalcPath(u32 iStartEdge, u32 iEndEdge)
	{
		Assert(iStartEdge >= 0 && iStartEdge < m_EdgeGroups.size() && iEndEdge >= 0 && iEndEdge < m_EdgeGroups.size());

		for(u32 p=0; p<m_PrecalcPaths.size(); p++)
		{
			TPrecalcPath * pPath = m_PrecalcPaths[p];
			if(pPath->iStartEdge == (int)iStartEdge && pPath->iEndEdge == (int)iEndEdge)
			{
				return pPath;
			}
		}
		Assert(0);
		return NULL;
	}

	CNavMesh * pNavMesh;
	vector<TNavGenEdgeGroup*> m_EdgeGroups;
	vector<TPrecalcPath*> m_PrecalcPaths;
};

#define ANALYSE_NUM_NAVMESHES					5

extern int g_iTotalNumCoverPointsCount;
extern int g_iNumCoverPointsFound;

//************************************************************************************
// Directional Cover
// This is cover analysed in 8 directions at several points in a navmesh poly.
// A single average cover in each direction is calculated for the entire poly, and
// then this is compared with threshold values to create an 8bit bitfield of whether
// cover exists in a given direction.  This can then be factored into pathfinding
// heuristic to create paths which favour protection from a given direction.

#define DIRECTIONAL_COVER_NUM_HEIGHTS		3
#define DIRECTIONAL_COVER_TEST_DISTANCE		10.0f
#define DIRECTIONAL_COVER_THRESHOLD			0.5f

class CDirectionalCoverInfo
{
public:
	CDirectionalCoverInfo()
	{
		for(int d=0; d<DIRECTIONAL_COVER_NUM_DIRS; d++)
		{
			m_fTotals[d] = 0.0f;
		}
	}

	float m_fTotals[DIRECTIONAL_COVER_NUM_DIRS];

	static const float ms_fTestHeights[DIRECTIONAL_COVER_NUM_HEIGHTS];
};

//********************************************************************
//	Debug fn

typedef void (*TDbgOutFnPtr)(char * pTxt);


//--------------------------------------------------------------
// CNavResolutionArea
// Defines an localised area of customised resolution sampling

class CNavResolutionArea
{
public:
	CNavResolutionArea() { m_bDynamicallyCreated = false; m_iFlags = 0; }
	~CNavResolutionArea() { }

	// 4x sampling resolution is the maximum supported currently
	// beyond this the mesh becomes so fine there's no point proceeding further
	static const float ms_fMaxResolutionMultiplier;

	// bounds of area
	Vector3 m_vMin;
	Vector3 m_vMax;

	// multiplier to resolution, lower values gives finer resolution
	// should be a multiple of 2x (eg. 0.5, 0.25, 0.125)
	float m_fMultiplier;

	// Whether this area was dynamically created (and therefore should be deleted after use)
	bool m_bDynamicallyCreated;

	u32 m_iFlags;

	enum
	{
		FLAG_DO_NOT_OPTIMISE		=	0x1,		// Do not optimise navmesh within this box
		FLAG_NO_COVER				=	0x2,		// Do not generate cover within this box
		FLAG_NO_DROPS				=	0x4,		// Do not generate dropdowns from polygons within this box
		FLAG_NO_CLIMBS				=	0x8,		// Do not generate climbs from polygons within this box
		FLAG_NO_CULL_SMALL_AREAS	=	0x10,		// Do not cull small areas of navmesh within this box
	};

};


//--------------------------------------------------------------
// CNavCustomCoverpoint
// Defines a coverpoint specified in data
class CNavCustomCoverpoint // this corresponds with the game class CNavDataCoverpoint (see NavEdit.h)
{
public:

	CNavCustomCoverpoint() {}
	~CNavCustomCoverpoint() {}

	float m_CoordsX, m_CoordsY, m_CoordsZ;
	u8 m_Type; // See navmesh.h, example: NAVMESH_COVERPOINT_LOW_WALL
	u8 m_Direction; // 0-255 gets mapped to world direction vector
};

class CDynamicEntityBoundsArray
{
public:
	CDynamicEntityBoundsArray() { }
	~CDynamicEntityBoundsArray() { CleanDeleteStlVector(m_Bounds); }
	vector<fwDynamicEntityInfo*> m_Bounds;
};

class CDynamicEntityBoundsPerNavMesh
{
public:
	CDynamicEntityBoundsPerNavMesh() { }
	~CDynamicEntityBoundsPerNavMesh()
	{
		for(u32 n=0; n<NAVMESH_MAX_MAP_INDEX; n++)
			for(u32 b=0; b<m_Bounds[n].size(); b++)
				delete m_Bounds[n][b];
	}

	vector<fwDynamicEntityInfo*> m_Bounds[ NAVMESH_MAX_MAP_INDEX ];
};



//***************************************************************************************************
//	CNavGen
//	The navmesh generator class
//***************************************************************************************************

class CNavGen
{
	friend class CNavAntiAlias;

public:
	CNavGen(void);
	~CNavGen(void);

	// Initialise the class with the given params, and builds an octree from the collision triangles
	bool Init(CNavGenParams * pParams);

	// Close the generator
	bool Shutdown(void);

	bool InitOctree();

	// Main function which samples the navmesh from the collision triangles.
	bool GenerateNavSurface(CNavGen** pNeighborNavGens, int numNeighborNavGens, bool bTriangulate = true, bool bMainMap = true);

	// Work-in-progress
	void GenerateNavSurface_VoxelMap();

	bool FlagTrianglesBlockedByDynamicObjects(CDynamicEntityBoundsArray & dynamicEntities);
	bool SplitTrianglesAfterOptimise(CDynamicEntityBoundsArray & dynamicEntities);
	bool SplitTriangle(const Vector4 & vPlane, CNavSurfaceTri * pTri);

	// Optimise the 'Nav-Surface' to reduce the triangle count
	bool OptimiseNavSurface(void);


public:

	// Load only the flags for the specified collision file.  Quick - only opens file, verifies header & reads subsequent 4 bytes.
	// Can be used to ascertain whether the ".tri" file really contains any collision geometry, etc.
	static u32 ScanCollisionFileFlags(char * pInputFilename, bool & bFileExists);

	// This function loads a .tri collision triangles file.  All parameters but the first, are for *returning* data from the function
	bool LoadCollisionTriangles(
		char * pFilename,
		Vector3 & vSectorMins, Vector3 & vSectorMaxs,
		int & iSectorStartX, int & iSectorStartY,
		int & iNumSectorsX, int & iNumSectorsY,
		Vector3 & vWorldMins, Vector3 & vWorldMaxs,
		Vector2 & vMapSectorsMin,
		// These optional params specify a min/max outside of which to discard polygons
		Vector3 * pCullToThisMin = NULL, Vector3 * pCullToThisMax = NULL
#if HEIGHTMAP_GENERATOR_TOOL
		, CNavGenTriForHeightMap ** out_tris = NULL
		, u32 * out_iNumTris = NULL
#endif
	);
#if HEIGHTMAP_GENERATOR_TOOL
	int LoadCollisionTrianglesForHeightMap(
		CNavGenTriForHeightMap *& tris,
		int & iMaxTris,
		char * pInputFilename
	);
#endif

	// This function loads a Rage .bnd file & obtains the triangles from it.
	bool LoadCollisionBound(
		char * pInputFilename,
		// The following parameters are use purely to return info about the loaded file
		Vector3 & vSectorMins, Vector3 & vSectorMaxs,
		int & iSectorStartX, int & iSectorStartY,
		int & iNumSectorsX, int & iNumSectorsY,
		Vector3 & vWorldMin, Vector3 & vWorldMax
	);
	static bool LoadCollisionFileExtents(char * pInputFilename, Vector3 & vMin, Vector3 & vMax);

	bool ReadWaterLevel(char * pWaterFileName);

	// This function slices away all geometry which is outside of m_vWorldChopMin & m_vWorldChopMax bounds
	//void SliceAwayGeometry(const Vector3 & vMins, const Vector3 & vMaxs);

	// Test function to set up a simple ground plane for navmesh generation
	void CreateGroundPlane(const Vector2 & vAreaMin, const Vector2 & vAreaMax, const Vector2 & vPlaneMin, const Vector2 & vPlaneMax, const float z);

	static bool LoadCustomResolutionAreas(char * pFilename);

	static bool LoadCustomCoverpoints(char * pFilename);

	static bool LoadDLCAreas(char * pFilename);

	// This function creates an octree from the world triangles.
	bool MakeOctree(void);

	// Goes through triangle list, and flags tris as walkable or not
	void FlagWalkableTris(void);

	// Returns whether this triangle is walkable.  This is based upon the triangle's normal,
	// and maybe also upon the surface type..
	bool IsWalkable(CNavGenTri * pTri);
	bool IsTooSteepForNormalMovement(const Vector3 & vNormal);

	//*****************************************************************
	//
	//	These functions are concerned with the placing of the initial
	//	pathnodes, and the triangulation of the resultant surface.
	//
	//*****************************************************************

	void PlaceNavAerialNodes(CNavGen** pNeighborNavGens, int numNeighborNavGens, const Vector3 & vMinForPlacement, const Vector3 & vMaxForPlacement);

	// This function places nodes throughout the octree area on a regular axis aligned grid
	void PlaceNavSurfaceNodes(const Vector3 & vMinForPlacement, const Vector3 & vMaxForPlacement, const bool bCreateResAreasForPavement);
	void PlaceNavSurfaceNodesInArea(const Vector3 & vMinForPlacement, const Vector3 & vMaxForPlacement, const float fSamplingResolution);

	// This function triangulates path nodes into a surface
	void TriangulateNavSurface(const Vector3 & vMin, const Vector3 & vMax, int b2ndTriangulationPass = 0);

	// Initialised a passed in CNavSurfaceTri
	void InitNavSurfaceTri(CNavSurfaceTri * pNavTri, CNavGenNode ** ppNodes, bool bWater, bool bSlicePlane);

	// Finds & triangulates a gap at the edge of the navmesh, after main triangulation has been done (to fix jagged edges causes by aliasing of axis-aligned sample grid)
	bool FindTriangulateEdge(int iInputTriEdge, CNavGenNode * pNode1, CNavGenNode * pNode2, CNavSurfaceTri * pInputTri, CNavGenNode ** ppOutNode, CNavSurfaceTri ** ppOutTri, int * pOutEdge);

	void CorrectAdjacencyOrderingAfterTriangulation();

	void CreateResolutionAreasForCollisionTriangles(vector<CNavResolutionArea*> * areas);

	void TagPavementNavMesh();

	void CutPavementsIntoMesh();
	void CutPropertiesIntoMesh( bool(*CollisionTriHasProperty)(CNavGenTri*), void(*SetNavTriProperty)(CNavSurfaceTri*) );
	CNavGenNode * LocateExisingNode(const Vector3 & vPos, const float fEpsSqrXY, const float fEpsZ, const bool bAllocIfNotFound, CNavGenNode * pExclude1, CNavGenNode * pExclude2);
	CNavGenNode * CreateNewNode(const Vector3 & vPos);

	void MoveVertsToSmoothPavementEdges();
	bool MoveVertsToSmoothPavementEdges(CNavSurfaceTri * pTri);

	void MoveVertsToSmoothJaggies();
	bool MoveVertsToSmoothJaggies(CNavSurfaceTri * pPavementTri);

	void MoveVertsToSmoothEdges( bool (HasSurfaceProperty)(CNavSurfaceTri *), bool (CollisionHasSurfaceProperty)(CNavGenTri *) );
	bool MoveVertsToSmoothEdges( CNavSurfaceTri * pPavementTri, bool (HasSurfaceProperty)(CNavSurfaceTri *), bool (CollisionHasSurfaceProperty)(CNavGenTri *) );

/*
	void AntiAliasBoundaryEdges();
	CNavEdgeIterator::CEdge LocateBoundaryEdgeTriangle(IsBoundaryEdgeFN pFN, s32 iPass);

	void AntiAliasMeshEdges(IsBoundaryEdgeFN isEdgeFN);
	void AntiAliasPavementEdges();

	bool IsValidNodeMoveForExternalEdge(CNavGenNode * pNode, const Vector3 & vNewPosition, CNavGenNode * pExteriorNode1, CNavGenNode * pExteriorNode2, CNavSurfaceTri * pTri1, CNavSurfaceTri * pTri2);
*/

	void FindDepthClearAboveAndBelow(CNavGenNode ** pNodes, s32 * pFirstIntersectionAboveZ, s32 * pFirstIntersectionBelowZ, const bool bTestUpToWaterLevel);

	// This function attempts to remove water polys which are not accessible
	void CullWaterTrianglesNotAcessible();
	// This helper function fires a ray from each edge of triangle to see if it hits the reverse side of a wall
	bool IsTriangleBehindWall(const Vector3 & pPt1, const Vector3 & pPt2, const Vector3 & pPt3);
	// This may be called after optimisation to remove inaccessible triangles.  It may be more optimal than the
	// alternative approach of culling them during initial triangulation (trade off of line-tests vs optimisation cost)
	void CullTrianglesBehindWallsAfterOptimisation();
	void RemoveRiverBedTrianglesAfterOptimisation();

	bool IsPositionNearEdgeOfNavMesh(const Vector3 & vPos, const Vector3 & vMin, const Vector3 & vMax, const float fEps);
	u32 GetPositionNavMeshEdgeFlags(const Vector3 & vPos, const Vector3 & vMin, const Vector3 & vMax, const float fEps);

	void MarkWhetherNodesAreSurroundedByTriangles();

	// Nudges nodes away from walls
	void MoveNodesAwayFromWalls();

	void RecordSteepnessUnderMovedTriangles();

	// This debug function processes a line-of-sight brute-force against ALL collision triangles (no octree culling - very slow)
	bool ProcessLineOfSightNoOctree(const Vector3 & vStart, const Vector3 & vEnd, const u32 iCollisionTypes, Vector3 & vHitPoint, float & fHitDist, CNavGenTri *& hitTriangle, bool bVertical);

	// Given two points (typically vertices in a potential NavSurfaceTri) we step along the ray at fIncrement steps,
	// and do vertical line tests downwards.  If we encounter a sudden change in height over a tolerance value,
	// then we return false - other we return true indicating that we can safely triangulate this triangle edge.
	// This function is called by the triangulation functions above, to try to eliminate the navmesh surface 'flowing'
	// over low walls, etc.
	bool IsLineFreeOfSuddenHeightChanges(CProbeTester & probeTester, const Vector3 & vPt1, const Vector3 & vPt2, CNavGenNode * pNode1, CNavGenNode * pNode2, const u32 iCollisionTypes, bool & bExceededSteepnessThreshold, float fIncrement, float fMaxHeightChangeValue, bool bIgnoreEqualZ, bool bQuitIfGroundNotHit, bool bQuitIfHitWater, bool bQuitIfHitNonWater, bool bInReverse = false);

	// A debugging function to make sure that no triangles are being duplicated during the creation/optimization process
	void CheckForDuplicateNavSurfaceTriangle(CNavSurfaceTri * pTri);

	// This function establishes the adjacency info between every triangle in the nav-surface
	void EstablishNavSurfacesAdjacency();
	void EstablishNavSurfacesAdjacency2();

	// This function removes disjoint triangles (before optimisation)
	s32 RemoveTrianglesWithNoNeighbours();
	// Remove triangles with very small areas
	s32 RemoveTrianglesWithZeroArea();
	
	// Remove patches of triangles below some area in total size
	s32 RemoveSmallPatchesOfNavMesh(const float fThreshold);
	bool RemoveSmallPatchesOfNavMeshR(CNavSurfaceTri * pTri, float & fTotalArea, const float fBreakOutSize, const u32 iTimestamp, s32 & iIters, const s32 iMaxNumIters);

	// Fix jagged edges cause by aliasing, by inserting new triangles to fill the gaps
	void FixJaggiesAfterTriangulation();

	void CleanUpDataAfterGeneratingNavMesh();

	CNavSurfaceTri * PickNavSurfaceTriangle(const Vector3 & vRayStart, const Vector3 & vRayEnd);



	void CreateWaterNavSurfaceTris();


	//***********************************************************************
	//
	//	These functions are concerned with the optimisation of the
	//	navigation surface we've created, via a process of vertex-collapsing
	//
	//***********************************************************************

	// This function calculates a cost value associated with moving "pNode" onto "pNodeToMoveOnto"
	// The cost is a function of the change in curvature of the surrounding mesh (quadric error metric method)
	float EvaluateVertexMove(CNavGenNode * pNode, CNavGenNode * pNodeToMoveOnto);

	bool MakeSureEdgeCollapseDoesntFlipAnyTriangles(CNavGenNode * pNode, CNavGenNode * pNodeToMoveOnto);
	bool MakeSureNodeMoveDoesntFlipAnyTriangles(CNavGenNode * pNode, const Vector3 & vNewPosition);

	// These functions recurse down to each octree leaf, and performs optimisation by edge collapse within the
	// leaf.  As a by-product of this process, triangles and nodes may move out of the leaf although this will
	// not be identified at this time.  At a later point we may reclassify them.
	void CreateCollapseEdges(void);

	void RandomiseSomeDataBeforeOptimisation(void);

	bool GatherAllEdgesIntoEdgeList(void);

	// All the above calls, call this function
	bool OptimiseNavSurfaces(void);

	bool DoOptimiseNavSurfaces(void);

	void RemoveReferenceToTrisFromNodesOppositeEdgeToCollapse(CNavTriEdge * pEdge);

	// This is to fix the nasty problem where flaps are created in the mesh, when two triangles
	// become adjacent across two edges.
	void FixFlaps(CNavGenNode * pNode);

	// This function sees if it is necessary to stick up edges and does so if found necessary
	void MaybeStitchUpEdgeAcrossGap(CNavTriEdge * pCollapsingEdge, CNavTriEdge * pOtherEdge, CNavGenNode * pSharedNode);

	// This function cleans up all allocated data associated with optimising the nav-mesh
	void CleanUpDataAfterOptimisingNavMesh(void);


	// Removes all the triangles which are not interior to the mesh.  These are the triangles
	// which define the immovable edges of the mesh, and are always unoptimised (we cannot
	// move them).  After the mesh interior has been optimised, we can remove these triangles
	// and them optimise the mesh borders nicely.
	void RemoveEdgeTris(void);
	void MarkEdgeTris(bool bFirstPass);
	void ProcessRemoveEdgeTris(void);

	// These functions are to be called after the normal optimisation has completed, and no more
	// edges can be collapsed.  The purpose is to go through areas of the surface which could not
	// be collapsed (ie. areas bordering upon the edge of the mesh) - and optimise here.
	bool OptimiseEdges(void);
	bool OptimiseEdges_OneIteration(void);


	void GatherTrisOfSurfaceType(u32 iSurfaceType, vector<CNavGenTri*> & triList);
	void SetAllTrianglesAsAccessible(void);

	// Removes all nodes and triangles which are not marked as accessible.
	void PruneInaccessibleNodesAndTris(void);


	// Attempt to make the mesh more regular by moving nodes which are completely surrounded by
	// triangles, such that the distance between each surrounding node is more similar.
	float RelaxVertexPositions();
	
	// 
	void RemoveSteepSlopeJaggies();

	// Removes nav surface triangles which have not been optimised, and which are at the edge
	// of the mesh with only one adjacent triangle.  These are fairly pointless and just consume
	// memory.  This must ONLY be done after the optimisation step can do no more.
	// Removing these triangles means that 'nooks & crannies' may not get any nav triangle in them
	// but that's okay, since we have a tri-in-volume test for grabbing the NavTri underfoot anyhow.
	// Removing these triangles will also allow the poly-merging phase to be more productive..
	int m_iNumSmallTrianglesRemoved;

	void RemoveUnimportantNodesAndTriangles(void);

	//**************************************************************************************

	static bool FloodFillNavMeshes();
	static TNavMeshPoly * FindSuitablePolyForFloodFill();
	static void FloodFillNavMeshesFromPoly(TNavMeshPoly * pPoly);
	static void ReconstructNavMeshAfterFloodFill(CNavMesh * pNavMesh);
	static int CountPolysInAllNavMeshes();
	static void RecreateAllNavMeshesAfterFloodFill();
	static void RemapPolyIndicesInAllNavMeshes();

	//**************************************************************************************
	//
	//	These functions are concerned with stitching adjacent navmeshes together, so that
	//	they know which ones is adjacent.  It also ensures that the polygons along the
	//	adjoining edges are well-connected with no T-junctions (by inserting new vertices
	//	and edges into them where necessary).
	//
	//**************************************************************************************

	enum SideOfNeighbour
	{
		eMinusX,
		ePlusX,
		eMinusY,
		ePlusY
	};

	// Intermediary data structure to store information about a navmesh poly which is in the process
	// of being stitched with polys in a neighbouring navmesh.
	struct TStitchPoly
	{
		TStitchPoly()
		{
			m_iIndexInNavMesh = NAVMESH_POLY_INDEX_NONE;
			m_pOriginalPoly = NULL;
		}
		// The index of the original poly in the navmesh being stitched
		int m_iIndexInNavMesh;

		// a list of the vertices which may be causing T-junctions for this poly, along a particular NavMesh edge
		atArray<Vector3> m_vOffendingVertices;

		// The array of vertices
		atArray<Vector3> m_NewVertices;
		// The array of edges
		atArray<TAdjPoly> m_NewEdges;

		//--------------------------------------------------------------------------
		// These members below are only used when joining DLC navmeshes to main map

		struct TAdjacentDLCPoly
		{
			TAdjacentDLCPoly() { }
			TAdjacentDLCPoly(const Vector3 & vVert1, const Vector3 & vVert2, TNavMeshPoly*pPoly, TStitchPoly*pStitchPoly, s32 iEdge, float fOverlap)
			{
				m_pPoly = pPoly;
				m_pStitchPoly = pStitchPoly;
				m_iDLCEdge = iEdge;
				m_fOverlapInterval = fOverlap;
				m_vVert1 = vVert1;
				m_vVert2 = vVert2;
			}

			Vector3 m_vVert1;
			Vector3 m_vVert2;
			TNavMeshPoly * m_pPoly;			// the polygon in the DLC navmesh
			TStitchPoly * m_pStitchPoly;	// the stitch poly corresponding
			s32 m_iDLCEdge;					// the edge in the polygon which adjoins to the non DLC navmesh
			float m_fOverlapInterval;		// the overlap interval
		};

		TNavMeshPoly * m_pOriginalPoly;
		atArray<TAdjacentDLCPoly> m_AdjacentDLCPolys;	// Adjacent polygons in DLC navmesh
	};

	static bool StitchNavMesh(
		CNavMesh * pNavMesh,
		CNavMesh * pNavMeshMinusX,
		CNavMesh * pNavMeshPlusX,
		CNavMesh * pNavMeshMinusY,
		CNavMesh * pNavMeshPlusY
	);

	// Stitches up a particular boundary.  The new mesh is returned.
	static CNavMesh * StitchNavMesh(CNavMesh * pNavMesh, CNavMesh * pOtherNavMesh, SideOfNeighbour sideOfNeighbour);

	static CNavMesh * DoStitchingAlongEdge(CNavMesh * pNavMesh, vector<TStitchPoly*> & stitchPolys, float fSharedEdgeVal, SideOfNeighbour sideOfNeighbour);

	static int SelectStitchPolyStartingVertex(TStitchPoly * pStitchPoly, CNavMesh * pNavMesh, SideOfNeighbour sideOfNeighbour);


	// Joins a central DLC navmesh to the surrounding main-map navmeshes
	static void JoinMeshesDLC(CNavMesh * pInputNavMesh, CNavMesh ** ppAdjacentNavmeshes);


	//**************************************************************************************
	//
	//	These functions are concerned with the analysis of CNavMeshes for the purpose of
	//	finding places where peds can climb up, drop down, jump over things in order to
	//	get to a nearby nav surface poly.
	//
	//**************************************************************************************

	// This defines an authored position/direction in the navmesh at which an adjacency should be scanned for
	struct TAuthoredAdjacencyHintPos
	{
		Vector3 m_vStartPosition;
		Vector3 m_vHeading;
		s32 m_iType;

		enum Type
		{
			TYPE_NONE,
			TYPE_CLIMB,
			TYPE_DROP
		};
	};

	static float ms_fLowCoverSpacingAlongEdges;
#if NAVGEN_OBSOLETE
	static float ms_fMinCoverPointSeparationWhenThinningOut;
#endif
	static const float ms_fCheckCoverBetweenPointsStepSize;
	static const float ms_fMaxCoverLinkDistance;
	static u32 ms_iIndexOfCoverPointWithinNavMesh;

	static bool AnalyseNavMesh(
		CNavMesh * pNavMesh, CNavGen * pNodeGen,
		CNavMesh * pNavMeshMinusX, CNavGen * pNodeGenMinusX,
		CNavMesh * pNavMeshPlusX, CNavGen * pNodeGenPlusX,
		CNavMesh * pNavMeshMinusY, CNavGen * pNodeGenMinusY,
		CNavMesh * pNavMeshPlusY, CNavGen * pNodeGenPlusY,
		char * pLoadThisCvrFileInsteadOfAnalysingForCoverPoints,
		const atArray<CNavGen::TAuthoredAdjacencyHintPos> & authoredAdjacencies,
		bool bNonMapNavMesh
	);

	static int ms_BytesUsedInQuadtree;
	static int ms_iNumNavMeshQuadtreeLeaves;

#if NAVGEN_OBSOLETE
	static int GatherPolysFromNavMesh(CNavMesh * pNavMesh, TAdjPoly * polyMem, int iMaxToGather, Vector3 & vMin, Vector3 & vMax);
	static void GatherPolysFromNavMeshR(CNavMeshQuadTree * pTree, TAdjPoly * pPolyMem, CNavMesh * pNavMesh);
#endif

	static bool TestNavMeshLOS(const Vector3 & vStartPos, const Vector3 & vEndPos, const TNavMeshPoly * pToPoly, TNavMeshPoly * pTestPoly, TNavMeshPoly * pLastPoly, const u32 iLosFlags);
	static TNavMeshPoly * TestNavMeshLOS(const Vector3 & vStartPos, const Vector2 & vLosDir, Vector3 * pvOut_IntersectionPos, TNavMeshPoly * pStartPoly, const u32 iLosFlags);
	static bool TestNavMeshLOS_R(TNavMeshPoly * pTestPoly, TNavMeshPoly * pLastPoly);

	static void InitGeomCuller(CCulledGeom & geometry, const Vector3 & vMins, const Vector3 & vMaxs, const int iNumNavGens, CNavGen ** pNavGens);

	// TestMultipleNavGenLos - returns true is the LOS is clear, or false if an intersection occurred
	static bool TestMultipleNavGenLos(const Vector3 & vStart, const Vector3 & vEnd, int iNumNavGens, CNavGen ** pNavGens, const u32 iCollisionTypes, Vector3 * pHitPos = NULL, bool bVertical = false, bool bIgnoreSeeThrough = false, bool bIgnoreFixed = false);
	static bool TestMultipleNavMeshLos(const Vector3 & vStart, const Vector3 & vEnd, int iNumNavMeshes, CNavMesh ** pNavMeshes, CNavMesh *& pHitNavMesh, u16 & iHitPoly, Vector3 & vHitPos);

	static bool ProcessMultipleNavGenLos(const Vector3 & vStart, const Vector3 & vEnd, int iNumNavGens, CNavGen ** pNavGens, u32 & iOut_ReturnFlags, const u32 iCollisionTypes, Vector3 * pHitPos, float & fClosestHitDist, CNavGenTri *& pOutputHitTri, bool bVertical = false, bool bIgnoreSeeThrough = false, bool bIgnoreFixed = false);

	static bool IsMultipleNavGenVolumeClear(const CClipVolume & volume, int iNumNavGens, CNavGen ** pNavGens, bool bConsiderClimbableObjects, const u32 iCollisionTypes);

	CNavResolutionArea * IntersectsResolutionArea(const u32 iResAreaFlags, const Vector3 & vMin, const Vector3 & vMax);

	// (This function will be the replacement for the FindClimbOver fn & others)
	bool FindNonStandardAdjacency(CNavMesh * pNavMesh, TNavMeshPoly * pPoly, const Vector3 & vPointOnEdge, const Vector3 & vEdgeNormal, const Vector3 & vEdgeVec, CNavGen ** ppNavGens);

	static bool ScanForAdjacentNavMeshPoly(
		CNavMesh * pNavMesh,
		TNavMeshPoly * pStartPoly,
		TAdjPoly & adjPoly,
		const Vector3 & vStartPos,
		const Vector3 & vScanDir,
		const Vector3 & vRightVector,
		CNavGen ** ppNavGens,
		CNavMesh ** ppNavMeshes,
		Vector3 & vOut_ToPos,
		const bool bConsiderClimbableObjects,
		const bool bQuitWithoutPerformingTests,
		const float fScanStartDistAhead = 0.5f);

	static bool IsGroundEven(const Vector3 & vPos, const int iNumProbes, const Vector3 * pOffsets, CNavGen ** ppNavGens, const float fMaxAllowedHeightDiff);
	static bool IsClearAbove(const Vector3 & vPos, const float fDistAboveToTest, const float fRadius, CNavGen ** ppNavGens, bool bConsiderClimbableObjects);
	static bool IsAdjacencyClear(const Vector3 & vStart, const Vector3 & vEnd, const float fWidth, const float fHeightAboveStart, const float fTestHeight, CNavGen ** ppNavGens, bool bConsiderClimbableObjects);
	static bool FindLowestClearPassage(const Vector3 & vStart, const Vector3 & vEnd, CNavGen ** ppNavGens, const float fMaxHeightAboveStart, float & fLowestClearHeight, bool bConsiderClimbableObjects, bool bTestForNotClimbableCollision);

	bool CheckForClimbOver(
		const Vector3 & vPosition,				// The position on the floor
		const Vector3 & vDirection,				// The direction the climb is to be analysed in
		const float fMaxHeightForClimb,
		const float fMaxDistMayFallOtherSide,
		const bool bAllowLandInWater,
		const int iNumNavGens,
		CNavGen ** ppNavGens,
		CNavMesh ** ppNavMeshes
	);

#if NAVGEN_OBSOLETE
	static bool FindClimbOver(int iNumNavGens, CNavMesh ** pNavMeshes, CNavGen ** pNavGens, const Vector3 & vHalfwayAlongEdge, const Vector3 & vEdgeVectorUnit, const Vector3 & vEdgeNormal, TAdjPoly startPoly, TAdjPoly & edgeData);
#endif

	static bool DoesEdgeProvideCover(const int iNumNavGens, CNavGen ** pNavGens, const Vector3 & vEdgeVertex1, const Vector3 & vEdgeVec, const float fEdgeLength, const Vector3 & vEdgeNormal, Vector3 & out_vAverageNormalOfSurfacesHit, bool & out_bEdgeMightProvideLowCover);

	static void FindCoverPoints(CNavGen * pNavGen, CNavMesh * pNavMesh, int iNumNavGens, CNavGen ** pNavGens);
	static void FindLowWallCoverPoints(CNavMesh * pNavMesh, int iNumNavGens, CNavGen ** pNavGens, vector<CNavMeshCoverPoint> & coverPoints);
	static bool CheckForLowWallCoverPoint(CCulledGeom & geometry, const CNavMesh * pNavMesh, const TAdjPoly & adjPoly, const Vector3 & vInputPointOnEdge, const Vector3 & vEdgeVec3d, const Vector3 & vEdgeNormal3d, int iNumNavGens, CNavGen ** pNavGens, vector<CNavMeshCoverPoint> & coverPoints);
	static void FindCornerCoverPoints(CNavGen * pNavGen, CNavMesh * pNavMesh, int iNumNavGens, CNavGen ** pNavGens, vector<CNavMeshCoverPoint> & coverPoints);

	// Data for candidate corner coverpoint, to alleviate passing so many parameters around
	struct CornerCoverCandidateData
	{
		Vector3 m_vertexAbove;
		Vector3 m_vertexBelow;
		Vector3 m_vertexThirdOfTri;
		Vector3 m_vCoverDir;
		Vector3 m_vNavMeshIntersectPos;
		float m_fDistanceFromTriToP1;
		float m_fDistanceAboveP1ForRayTest;
		float m_fDistanceBelowP1ForRayTest;
	};

	// Perform validation checks and if all pass, add the candidate coverpoint
	static bool ProcessCornerCoverCandidate(CNavMesh* pNavMesh, int iNumNavGens, CNavGen** pNavGens, CNavGenTri* pCollisionTri, const CornerCoverCandidateData& candidateData, vector<CNavMeshCoverPoint>& out_coverPoints);

	// Helper method to perform coverage and clearance probes for candidate corner cover
	static void TestForCoverAtCorner(
		CCulledGeom & geometry,
		const int iNumNavGens,
		CNavGen ** ppNavGens,
		const Vector3 & vPositionOfCorner,						// Test probes will be done either side of this corner.  The z position should be at the base of the corner, close to floor.
		const Vector3 & vCoverDir,								// This is the unit vector direction of the cover
		const Vector3 & vTangentToRays,							// This always points towards the side of the corner which is in cover
		const float fClearTestRayLength,						// This is the length of the raytests for clear line of sight
		const float fCoveredTestRayLength,						// This is the length of the raytests for sufficient coverage
		const float fLowZ,										// Height of the bottom-most row of linetests
		const float fHighZ,										// Height of the top-most row of linetests
		const int iNumRows,										// Number of rows (vertical)
		const float fHorizontalSpacing,							// Spacing between columns (horizontal)
		const int iNumColumns,									// The number of columns
		const float fOffsetFromCorner,							// How far to the left & right of the corner that the origin of the raytests should be done
		int & iOut_PercentageLosTestsBlockedOnCoverSide,		// Output percentages.  The calling function will want both of these to be...
		int & iOut_PercentageLosTestsClearOnNoCoverSide,			// ...sufficiently high percentages to imply that cover exists at this edge.
		int & iOutActualNumBlockedOnCoverSide,
		int & iOutActualNumClearOnNoCoverSide
	);

	static bool DoesCoverPointAlreadyExist(vector<CNavMeshCoverPoint> & coverPoints, const Vector3 & point, u8 * iCoverDir, u32 iCoverType, const Vector3 & vNavMeshMin, const Vector3 & vNavMeshSize, float fEps, float fRatioClearToBlocked);
	static bool AddCoverPointToNavMeshQuadTree(CNavMeshQuadTree * pTree, CNavMeshCoverPoint & coverPoint, Vector3 & vPosition);
	static void ClearCoverPointsInNavMesh(CNavMeshQuadTree * pTree);

	static bool SaveCvrFile(char * pFullFileName, CNavMesh * pNavMesh, u32 iTriFileCRC, u32 iResAreasCRC);
	static bool ReadCvrFileCRCs(char * pFullFileName, u32 & iCollisionCRC, u32 & iResAreasCRC);
	bool LoadCvrFileAndAddCoverPoints(char * pFullFileName, CNavMesh * pNavMesh);

	static int LinkUpCoverPoints(CNavMesh * pNavMesh, CNavMesh * pAdjacentNavMeshes[4], int iNumNavGen, CNavGen ** ppNavGens);
	static void ClearAllLinkingCoverPoints(CNavMeshQuadTree * pTree);
	static void ClearAllExternalLinkingCoverPoints(CNavMeshQuadTree * pTree);
	static void ClearAllCoverPointsLinkingToAdjNavMesh(CNavMeshQuadTree * pTree, eAdjNavMeshCode iAdjNav);
	static void GetNearbyCoverPoints(vector<CNavMeshCoverPoint*> & coverPoints);
	static void ExtractCoverPointsIntoList(CNavMesh * pNavMesh, CNavMeshQuadTree * pTree, vector<CNavMeshCoverPoint*> & coverPoints, bool bIgnoreLockedForLinking);
	static void	RecalculateCoverPointIndices(CNavMesh * pNavMesh, CNavMeshQuadTree * pTree, s32 &iStartIndex );
	static void	ReLinkCoverPoints(CNavMesh * pNavMesh, CNavMeshQuadTree * pTree, s32 &iStartIndex );
	static void ReCreateQuadTreeCoverPointsAfterLinking(CNavMeshQuadTree * pTree, int & iCountSoFar);

	static bool SearchForCoverPointLinkUp(
		int iLinkDir,								// -1 for left, +1 for right
		CNavMeshCoverPoint * pCP,
		const Vector3 & vCoverVertex,
		const Vector3 & vCoverDir,
		const Vector3 & vLinkUpDir,
		vector<CNavMeshCoverPoint*> & coverPoints,
		int iIndexToStart
	);

	static bool CheckForCoverBetweenCoverPoints(const Vector3 & vCoverVertex, const Vector3 & vCoverDir, const Vector3 & vOtherCoverVertex, const Vector3 & vOtherCoverDir);
	static int GetCodeForLinkedToNavMesh(CNavMesh * pLinkFromNavMesh, CNavMesh * pLinkToNavMesh);
	static void SetIndexOfFirstCoverPointInQuadtreeLeaves(CNavMeshQuadTree * pTree);

	static int CountCoverPoints(CNavMesh * pNavMesh);
	static void CountCoverPointsR(CNavMeshQuadTree * pTree);

	//****************************
	// Directional cover scanning

	static void TestCoverDirectionsAtPosition(CCulledGeom & geometry, const Vector3 & vPosition, CDirectionalCoverInfo & coverInfo, CNavGen ** ppNavGens, const int iNumNavGens);
	static u8 TestDirectionalCoverNavMeshForPoly(CCulledGeom & geometry, CNavMesh * pNavMesh, TNavMeshPoly * pPoly, CNavGen ** ppNavGens, const int iNumNavGens);


	// Corrects the smearing out of ped density values by resampling the values from the collision mesh
	static void CorrectPedDensity(CNavMesh * pNavMesh, CNavGen * pNavGen);
	
	void SetRoadFlags(CNavMesh * pNavmesh);
	void SetTrainTrackFlags(CNavMesh * pNavmesh);

	static void SetAllPolyTimestamps(const u16 iTimestamp);

	//static void SetFreeSpaceAroundPolys();

	static void CalcFreeSpaceBeyondPolyEdges(CNavMesh * pNavMesh);
	static void CalcFreeSpaceBeyondPolyEdge(TNavMeshPoly * pPoly, CNavMesh * pNavMesh, s32 iEdge);

	static void CalcFreeSpaceAroundVertices(CNavMesh * pNavMesh, CNavMesh * pNegX, CNavMesh * pPosX, CNavMesh * pNegY, CNavMesh * pPosY);
	static float CalcFreeSpaceAroundVertex(CNavMesh * pNavMesh, CNavMesh * pNegX, CNavMesh * pPosX, CNavMesh * pNegY, CNavMesh * pPosY, const s32 iVertex);

	// Remove small patches of pavement which will cause spawned peds to be isolated & unable to wander anywhere
	static void RemoveIsolatedAreasOfPavement();
	static void RemoveIsolatedAreasOfPavementR(CNavMesh * pNavMesh, TNavMeshPoly & poly, u16 iTimeStamp, float & fAreaSoFar);

	// Same functionality, but for patches of ped-density
	static void RemoveIsolatedAreasOfPedDensity();
	static void RemoveIsolatedAreasOfPedDensityR(CNavMesh * pNavMesh, TNavMeshPoly & poly, u16 iTimeStamp, float & fAreaSoFar);

	// Marks small patches of navmesh which are not connected to the main navigation surface
	static void MarkIsolatedAreasOfNavMesh();
	static void MarkIsolatedAreasOfNavMeshR(CNavMesh * pNavMesh, TNavMeshPoly & poly, u16 iTimeStamp, float & fAreaSoFar);

	// Identifies polygons which may be potential candidates for respawning the player during network games
	static void MarkPotentialNetworkSpawnPolys(const char * pInputPath); //CDynamicEntityBoundsPerNavMesh & entityBoundsPerNavMesh);
	static void MarkPotentialNetworkSpawnPolysR(CNavMesh * pNavMesh, TNavMeshPoly & poly, u16 iTimeStamp, float & fAreaSoFar);

	static void SetAllNavMeshFlags();

	// Functions to link water polys to land polys, and vice versa
	static void LinkShorePolys(CNavMesh * pNavMesh, CNavGen * pNavGen);

	// Calculate the water depth under all water polygons; currently stored as a single bit for all polys failing a certain depth threshold
	static void CalculateWaterDepths(CNavMesh * pNavMesh, CNavGen * pNavGen);

	// Load in the "Extra_x_y.dat" text file which holds some extra information about the world (ladder, etc)
	static bool ParseExtraInfoFile(char * pFileName, vector<fwNavLadderInfo> * laddersList, vector<TCarNode*> * carNodesList, vector<spdSphere*> * portalBoundaries, vector<fwDynamicEntityInfo*> * dynamicEntityBounds);

	// Create the special navmesh links.
	// pNavMeshes is an array of ANALYSE_NUM_NAVMESHES navmeshes.  Element zero is always present.
	// The next 4 elements may or may not be valid, or may be NULL.
	static void CreateSpecialNavMeshLinks(CNavMesh ** ppNavMeshes, CNavGen ** ppNavGens, int iNumNavMeshes, vector<fwNavLadderInfo> & ladderInfoList, vector<fwDynamicEntityInfo*> & dynamicEntityBounds, vector<CSpecialLinkInfo*> & allSpecialLinks);

	// Functions to analyse links relating to special things in the world.
	static void AnalyseSpecialLink_Ladder(CNavMesh ** ppNavMeshes, CNavGen ** ppNavGens, int iNumNavmeshes, fwNavLadderInfo & ladderInfo, vector<CSpecialLinkInfo*> & specialLinks);

	static void AnalyseSpecialLink_ClimbableObject(CNavMesh ** ppNavMeshes, CNavGen ** ppNavGens, int iNumNavmeshes, fwDynamicEntityInfo * pObjectInfo, vector<CSpecialLinkInfo*> & specialLinks);

	// Marks which polys are near car nodes
	static void MarkCarNodes(CNavMesh * pNavMesh, vector<TCarNode*> & carNodes);

	// Removes all ped-density from navmesh polys which intersect portal boundaries
	static void ZeroPedDensityAtPortalBoundaries(CNavMesh * pNavMesh, vector<spdSphere*> & portalBoundaries);

	// Marks which polys are considered sheltered from above, and may be used by peds to shelter from rain, etc
	static void MarkShelteredPolys(CNavMesh * pNavMesh, CNavGen * pNavGen);

	//************************************************************************************************
	//	Audio analysis stuff

	static u32 CalculateAudioPropertiesForPoly(const int iNumVerts, const Vector3 * pPolyVerts, const Vector3 & vPolyCentroid, CNavGen ** ppNavGens, int iNumNavGens);

	//************************************************************************************************
	//	Hierarchical pathfinding

	static u32 g_iHierarchicalNodeTimestamp;

	static bool CreateHierarchicalNavData(char * pNavMeshesPath, char * pGameDataPath);
	static bool SaveHierarchicalNavData(const char * pSavePath, vector<CHierarchical*> & hierarchical);

	static void GetClosestNodes(CHierarchicalNode * pOriginNode, CHierarchicalNode * pCurrentNode, atArray<CHierarchicalNode*> & closestNodes, const int iMaxNum, const float fMaxDistSqr);
	static bool InsertHierarchicalNodesToFollowTerrain(CHierarchical * pHierarchical);
	static void AnalyseLinkWidth(CHierarchicalNode * pFromNode, CHierarchicalLink * pLink);


	//*******************************************************************************************
	//	These functions are to do with connecting a DLC subsection into the main map navmeshes

	void StitchAdjacentConnectionsForMapSwap(CNavMesh * pOrginalNavMesh, CNavMesh * pNewNeighbourNavmesh, CNavMesh * pOriginalNeighbourNavMesh);
	CNavMesh * ReplacePolygonsInMesh(CNavMesh * pNavMesh, vector<TPolygonToReplace> & polygonsToReplace);
	void FindNewVertsAlongEdge(vector<Vector4> & newVertsAlongEdge, const Vector3 & p1v1, const Vector3 & p1v2, const Vector3 & vEdgeNormal2d, CNavMesh * pNewNeighbourNavmesh);

	//**************************************************************************************
	//
	//	These functions are concerned with pathfinding, and with the generation of pathnodes
	//
	//**************************************************************************************

	void ClearOpenClosedFlags(VOID);
	void AutomaticallyGeneratePathnodesViaLineOfSightMethod(void);

	// Finds a path from the start to the end, via the navigation surface's triangles
	bool FindTrianglePath(
		CNavSurfaceTri * pStartTri,
		CNavSurfaceTri * pEndTri,
		const Vector3 & vEndPos,
		vector<CNavSurfaceTri*> & pathTris
	);

	// One a triangle path has been found, this function refines it into a minimal set of waypoints
	// by testing lines of sight from the start to the target.
	void RefineTrianglePathAndCreatePathNodes(const Vector3 & vStartPos, const Vector3 & vEndPos, vector<CNavSurfaceTri*> & pathTris, atArray<Vector3> & pathPoints);

	// This function performs quick linetests between the points on "pFromTri" and "pToTri".
	// It does this recursively by doing 2d line-seg intersection tests against the triangle edges, and
	// walking into the adjacent triangle each time an intersection is found.  If no triangle exists to
	// walk into in the line direction - then we've departed from the nav mesh, and should quit..
	bool TestNavSurfaceLOS(CNavSurfaceTri * pFromTri, CNavSurfaceTri * pToTri, CNavSurfaceTri * pTestTri, CNavSurfaceTri * pLastTri);

	//*************************************************************
	//
	//	General purpose stuff
	//
	//*************************************************************

	bool RemoveTriFromTriList(vector<CNavSurfaceTri*> & list, CNavSurfaceTri * pTri);

	// This does a big sanity-check on all the data.  Somethings going awry during
	// the mesh optimisation, and we are being left with orphaned triangles which
	// apparently *do* connect up.  Also some triangles appear upside-down (although
	// a new constraint in the EvaluateVertexMove() should have fixed that).
	void SanityCheckEverything(void);
	void SanityCheck_MakeSureNoDuplicateTris(CNavSurfaceTri * pTri);

	static void CheckNavMeshIntegrity(CNavMesh * pNavMesh);

	bool SaveNavSurfaceBinary(char * pFilename, char * pMeshName, u32 iTriFileCRC, u32 iResAreasCRC);

	void CleanBadData(void);
	void MarkBadTrianglesForRemoval(void);
	void RemoveAllReferencesToTriangle(CNavSurfaceTri * pTri);

	void PrepareForSave(void);
	void Save3ds(FILE * pFilePtr, int saveMode);

	vector<CNavGenNode*> m_NodesForSaving;

	// Load a 3ds file, and return a CNavMesh class.  Triangles adjacency will need to
	// be recomputed, as will mesh-mesh adjacency.
	CNavMesh * LoadNavSurfaceBinaryAsNavMesh(char * pFileName, Vector3 & vBlockMins, Vector3 & vBlockMaxs, bool bMainMapNavMesh);

	bool LoadNavSurfaceBinary(
		char * pFileName,
		Vector3 *& pVertices,
		TNavMeshPoly *& pPolys,
		TAdjPoly *& pAdjPolys,
		u16 *& pVertexIndices,
		int & iNumVertices,
		int & iNumFaces
	);

	int m_iLoadNavSurfaceBinary_MaxVertexIndex;
	int m_iLoadNavSurfaceBinary_MaxVertexPoolSize;

	CNavMesh * Load3dsMaxAsciiAsNavMesh(char * pFilename);
	static bool Load3dsMaxAsciiMarkup(char * pFilename, atArray<TAuthoredAdjacencyHintPos> & adjacencyPositions);

	//*****************************************************************
	//
	//	These functions are concerned with repeatedly merging
	//	triangles into polys, and then poly into more complex polys.
	//	The idea is to start with those with least area.   The overall
	//	aim is that of reducing the primitive count, and memory size.
	//
	//****************************************************************

	// Creates a new simplified nav-mesh based on the input one, by merging
	// adjacent triangles into polygons, and then polygons into larger polygons.
	// Removes vertices from collinear edges.

	CNavMesh * CreateMergedPolyNavMesh(CNavMesh * pInputMesh, vector<fwDynamicEntityInfo*> * pSplitObjects);
	void CheckMergePolyAdjacency(TMergePoly * pPoly);
	void EstablishMergePolysAdjacency(vector<TMergePoly*> * pPolyList);
	void SortMergePolyIntoList(TMergePoly * pPoly, list<TMergePoly*> * pPolyList);
	bool WillMergedPolyBeConvex(TMergePoly * pPoly1, int pPoly1MergeEdge, TMergePoly * pPoly2, int pPoly2MergeEdge);
	bool ArePolysCoplanarEnoughToMerge(TMergePoly * pPoly1, TMergePoly * pPoly2);
	bool MergePolys(TMergePoly * pTestPoly, int p, TMergePoly * pAdjPoly, int e);
	void RemoveReferenceToMergedPoly(TMergePoly * pRemovedPoly, TMergePoly * pReplacedWithPoly, list<TMergePoly*> * polyList, vector<TVertexEntry*> * vertexList);
	void RemoveReferenceToBisectedPoly(TMergePoly * pRemovedPoly, list<TMergePoly*> * polyList, vector<TVertexEntry*> * vertexList);

	bool Merge_AttemptToMergeAdjacentPairs(TMergePoly * pTestPoly, list<TMergePoly*> & sortedPolyList, vector<TVertexEntry*> & vertexList);
	bool Merge_RemoveColinearEdges(list<TMergePoly*> & sortedPolyList, vector<TVertexEntry*> & vertexList);

	bool Merge_AttemptToMergeMultipleAdjacentPolys(vector<TVertexEntry*> & vertexList, list<TMergePoly*> & sortedPolyList);

	void Merge_SortPolysSurroundingVertex(TVertexEntry * pVert);
	//bool Merge_SurroundSortPredicate(const TMergePoly * pPoly1, const TMergePoly * pPoly2, TVertexEntry * pCentralVert);

	class TSortMergePoly
	{
	public:
		TVertexEntry * pCentralVert;
		bool operator () (const TMergePoly * pPoly1, const TMergePoly * pPoly2) const;
	};

	static void MakeNavMeshQuadTree(CNavMesh * pNavMesh, const Vector3 & vMin, const Vector3 & vMax);
	static CNavMeshQuadTree * MakeNavMeshQuadTree(const Vector3 & vMin, const Vector3 & vMax, vector<CSplitPoly*> & splitPolyList, int iDepth);

	// Postprocess after merging polygons, to split certain polygons in half
	void SplitPolysAfterMerging(const CNavMesh * pInputNavMesh, list<TMergePoly*> & sortedPolyList, vector<TVertexEntry*> & vertexList, vector<fwDynamicEntityInfo*> * pSplitObjects);
	// Bisect the TMergePoly by the given plane
	bool BisectMergePoly(TMergePoly * pPoly, const Vector4 & vSplitPlane, const float fPlaneEps, list<TMergePoly*> & sortedPolyList, vector<TVertexEntry*> & vertexList, list<TMergePoly*> & newFragmentsPolyList);

public:

	// A copy of the parameters which were passed in
	CNavGenParams m_Params;

	float m_fTestClearHeightInTriangulation;

	int m_iInitialPassNumNodesCreated;
	int m_iInitialPassNumTrianglesCreated;
	int m_iFixJaggiesNumTrianglesCreated;

	int m_iTriangulationPass;

	bool m_bRemoveTrisBehindWalls;
	bool m_bIgnoreSteepness;
	bool m_bDoBehindWallTestDuringTriangulation;
	bool m_bJitterSamplesForPlacement;
	bool m_bJitterTestsUnderLine;
	bool m_bCalculateSpaceAboveAllPolys;

	bool m_bUseGeometryCuller;

	int m_iOptimisedNodeCount;
	int m_iOptimisedTriangleCount;

	// How many times to edge-collapse, before quitting for that pass (default=100)
	int m_EdgeCollapseNumIterations;
	
	// Defines how far to descend beneath a node, before creating the next one.
	//float m_fMinimumVerticalDistanceBetweenNodes;

	// The octree created from the collision triangles
	CNavGenOctree m_Octree;

	// The extents of currently-generated navmesh
	Vector3 m_vSectorMins;
	Vector3 m_vSectorMaxs;

	// If currently generated navmesh has water at all
	bool m_bHasWater;
	bool m_bHasVariableWaterLevel;
	// If the water level is constant across the whole navmesh area, then we can use m_fWaterLevel
	float m_fWaterLevel;
	// Otherwise, "m_pWaterSamples" is loaded from tri file
	float m_fWaterSamplingStepSize;
	s32 m_iNumWaterSamples;
	s16 * m_pWaterSamples;

	// Areas within the current navmesh which require custom resolution sampling
	static vector<CNavResolutionArea*> m_ResolutionAreasWholeMap;
	vector<CNavResolutionArea*> m_ResolutionAreasCurrentNavMesh;

	// Custom coverpoints within the current navmesh
	static vector<CNavCustomCoverpoint*> m_CustomCoverpointsWholeMap;
	vector<CNavCustomCoverpoint*> m_CustomCoverpointsCurrentNavMesh;

	// List of areas which contain DLC content during this navmesh build
	atArray<CDLCArea*> m_DLCAreas;

	// Slice planes - 3d planes which create an artificial navmesh surface
	vector<CSlicePlane*> m_SlicePlanes;

	static const float m_fTriangulationGetNodeEps;
	static const bool ms_bCheckForSuddenHeightChangesUnderTriEdges;
	static const float ms_fHeightChangesUnderTriEdges_HeightToTest;

	// Wrapper function for getting the water level at a specified position (has to be within current navmesh)
	bool GetWaterLevel(const float fX, const float fY, float & fWaterHeight);
	bool GetHitSlicePlane(const float fX, const float fY, const float fZ1, const float fZ2, float & fHitZ);

	// The list of world collision triangles
	TColTriArray m_CollisionTriangles;

	CVoxelMap m_VoxelMap;

	// No longer keeping this data in the octree leaves - it's too cumbersome & only marginally faster
	CPlacedNodeMultiMap * m_pNodeMap;
	vector<CNavGenNode*> m_NavSurfaceNodes;
	vector<CNavSurfaceTri*> m_NavSurfaceTriangles;
	vector<CNavTriEdge*> m_NavTriEdges;

	

	// The list of triangle edges, used for optimising the mesh via edge-collapses
	list<CNavTriEdge*> m_TriEdgesForCollapsing;

	// These are used to debug exactly the extents of our nodes
	Vector3 m_vMinOfPlacedNodes;
	Vector3 m_vMaxOfPlacedNodes;

	// Whether this list of tri-edges has been randomised yet (randomising helps the optimising
	// since edges will initially be strictly ordered)
	bool m_bTriEdgesForCollapsingHaveBeenRandomised;
	bool m_bRandomisedEdgeListsBeforeOptimisation;
	bool m_bNavSurfaceHasBeenOptimised;

	int m_iTotalNumEdgesInExistence;
	bool m_bDoWeCareAboutSurfaceTypes;

	// When this flag is set, then face with an adjacency < 3 will NEVER take part in an edge collapse
	// If it is not set, then the IsNodeMoveable() will be called instead.
	bool m_bDontMoveNonConnectedFaces;

	// When this flag is set, the the very edges of the nav mesh will never be optimised - which will
	// guarantee that neighbouring meshes will connecte together *exactly*
	bool m_bDontOptimiseMeshEdges;

	// These members are maintained so that the optimisation algorithm can try to maintain uniformity
	float m_fAverageEdgeLength;
	float m_fAverageNumTrianglesAroundNode;

	u32 m_iNumEdgesCollapsedThisPass;

	u32 m_iTriPathNumTrisVisited;

	CNavSurfaceTri * m_pPickedNavSurfaceTri;
	float m_fClosestNavSurfaceTriDist;
	int m_iNumNavSurfaceTrisHit;

	TPathStackNode * m_PathfindingStack;
	TPathStackNode m_PathStackNodes[MAX_PATH_STACK_SIZE];
	int m_iCurrentPathStackNode;

	Vector3 m_vPathfindingTarget;

	// Keep a store of all the triangles we've visited.  We can then iterate over them after
	// each path search, and clear the open/closed flags ready for next time.
	vector<CNavSurfaceTri*> m_PathfindingVisitedTris;


	u32 m_iCurrentNodeIndexToAssign;
	u32 m_iTotalNumTris;
	u32 m_iFileSize;

	u32 m_iVertexSavingProgress;
	u32 m_iFaceSavingProgress;
	u32 m_iNumResAreas;

	float m_fTimeTakenToPlaceNodes;
	float m_fTimeTakenToTriangulate;
	float m_fTimeTakenToMoveNodesAwayFromWalls;
	float m_fTimeTakenToFixJaggies;
	float m_fTimeTakenToAntiAliasEdges;
	float m_fTimeTakenToTriangulatePass1;
	float m_fTimeTakenToTriangulatePass2;
	float m_fTimeTakenToSmooth;
	float m_fTimeTakenToCullWaterTriangles;
	float m_fTimeTakenToEstablishAdjacency;

	static TDbgOutFnPtr m_pOutputTextFn;
	
	static void SetOutputTextFn(TDbgOutFnPtr pFn) { m_pOutputTextFn = pFn; }

	static s32 ms_iVerbosity;

	static void OutputText(char * pText)
	{
		if(m_pOutputTextFn)
			m_pOutputTextFn(pText);
	}
};

inline void DebuggerBreak()
{
	__debugbreak();
}

extern int g_iNumLowClimbOversFound;
extern int g_iNumHighClimbOversFound;
extern int g_iNumDropDownsFound;
extern int g_iNumCoverPointsFound;

}	// namespace rage


#endif
