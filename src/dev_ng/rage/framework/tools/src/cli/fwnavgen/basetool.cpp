//
// fwnavgen/basetool.cpp
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#include "basetool.h"

#include "grprofile/timebars.h"

using namespace rage;

//------------------------------------------------------------------------------

fwLevelProcessTool::fwLevelProcessTool()
		: m_ExporterInterface(NULL)
		, m_GameInterface(NULL)
		, m_iCollisionArchetypeFlags(0xffffffff)
		, m_iCoverBoxCollisionArchetypeFlags(0)
		, m_bExportExteriorPortals(false)
		, m_bExportPolyDensities(false)
		, m_bExportCoverBoxesForPillarShapes(false)
{
}


fwLevelProcessTool::~fwLevelProcessTool()
{
	delete m_ExporterInterface;
	delete m_GameInterface;
}


fwFsm::Return fwLevelProcessTool::UpdateState(const s32 state, const s32, const fwFsm::Event event)
{
	fwFsmBegin

		fwFsmState(State_Init)
			fwFsmOnUpdate
				return Init();

		fwFsmState(State_Run)
			fwFsmOnUpdate
				return Run_OnUpdate();

		fwFsmState(State_Shutdown)
			fwFsmOnUpdate
				return Shutdown();

	fwFsmEnd
}


fwFsm::Return fwLevelProcessTool::Init()
{
	PF_INIT_STARTUPBAR("Startup", 100);
	PF_START_STARTUPBAR("System Init");

	PF_INIT_TIMEBARS("Main", 1260, 99999999.9f);

	m_ExporterInterface = CreateExporterInterface(this);
	m_GameInterface = CreateGameInterface(this);

	m_ExporterInterface->PreInit();

	if(!m_GameInterface->InitGame())
	{
		SetState(State_Shutdown);
		return fwFsm::Continue;
	}

	m_ExporterInterface->Init();

	SetState(State_Run);

	return fwFsm::Continue;
}


fwFsm::Return fwLevelProcessTool::Run_OnUpdate()
{
	Assertf(GetCollisionArchetypeFlags()!=0, "No collision archetypes specified - the app will export nothing!  You need to call SetCollisionArchetypeFlags() in main.cpp");

	m_GameInterface->Update1();

	if(m_ExporterInterface->UpdateExport())
	{
		SetState(State_Shutdown);
	}

	m_GameInterface->Update2();

	return fwFsm::Continue;
}


fwFsm::Return fwLevelProcessTool::Shutdown()
{
	m_ExporterInterface->Shutdown();
	m_GameInterface->ShutdownGame();
	m_ExporterInterface->PostShutdown();

	return fwFsm::Quit;
}

//------------------------------------------------------------------------------

// End of file fwnavgen/basetool.cpp
