//
// fwnavgen/basetool.h
//
// Copyright (C) 1999-2011 Rockstar Games.  All Rights Reserved.
//

#ifndef FWNAVGEN_BASETOOL_H
#define FWNAVGEN_BASETOOL_H

#include "fwsys/app.h"

//------------------------------------------------------------------------------

namespace rage
{

class fwLevelProcessTool;

//-----------------------------------------------------------------------------

/*
PURPOSE
	Interface for tools that process level data loaded in game, for example by
	writing out geometry to intermediate files to build navigation data from.
*/
class fwLevelProcessToolExporterInterface
{
public:
	fwLevelProcessToolExporterInterface(fwLevelProcessTool * pTool) { m_pLevelProcessTool = pTool; }

	// PURPOSE:	Virtual destructor.
	virtual ~fwLevelProcessToolExporterInterface() { }

	// PURPOSE:	This gets called before fwLevelProcessToolGameInterface
	//			initializes the game, for example giving the exporter a chance
	//			to configure the game's initialization depending on the
	//			exporter's needs. 
	virtual void PreInit() = 0;

	virtual void Init() {}

	virtual void Shutdown() {}

	virtual void PostShutdown() {}


	// PURPOSE:	This gets called after the game has been loaded, and is where
	//			the actual code for doing the export processing is expected
	//			to be called from.
	// RETURNS:	True if done.
	virtual bool UpdateExport() = 0;

protected:

	fwLevelProcessTool * m_pLevelProcessTool;
};

//-----------------------------------------------------------------------------

/*
PURPOSE
	Interface for initializing various game systems and loading level data.
NOTES
	Separate from fwLevelProcessToolExporterInterface as we may want to have
	multiple tools that process the data in various ways, but the code in
	classes derived from fwLevelProcessToolGameInterface is meant to be shared
	across tools for a specific game.
*/
class fwLevelProcessToolGameInterface
{
public:

	fwLevelProcessToolGameInterface(fwLevelProcessTool * pTool) { m_pLevelProcessTool = pTool; }

	// PURPOSE:	Virtual destructor.
	virtual ~fwLevelProcessToolGameInterface() { }

	// PURPOSE:	Initialize the relevant parts of the game.
	virtual bool InitGame() = 0;

	// PURPOSE:	Shut down the relevant parts of the game.
	virtual void ShutdownGame() = 0;

	// PURPOSE:	Part one of the simplified update loop when running the tool,
	//			called before fwLevelProcessToolExporterInterface::UpdateExport().
	virtual void Update1() = 0;

	// PURPOSE:	Part two of the simplified update loop when running the tool,
	//			called after fwLevelProcessToolExporterInterface::UpdateExport().
	virtual void Update2() = 0;

protected:

	fwLevelProcessTool * m_pLevelProcessTool;
};

//------------------------------------------------------------------------------

/*
PURPOSE
	Base class for tool applications that operate by initializing parts of game,
	loading a level, and processing or exporting data about the game world. To
	achieve this, two interfaces (fwLevelProcessToolGameInterface and
	fwLevelProcessToolExporterInterface) are used, containing the code for
	initializing the game and for processing data.
*/
class fwLevelProcessTool : public fwApp
{
public:
	enum
	{
		State_Init = 0,
		State_Run,
		State_Shutdown
	};

	// PURPOSE:	Constructor.
	fwLevelProcessTool();

	// PURPOSE:	Virtual destructor.
	virtual ~fwLevelProcessTool();

	// PURPOSE: Accessors for the exporter interface
	fwLevelProcessToolExporterInterface * GetExporterInterface() { return m_ExporterInterface; }

	// PURPOSE: Accessors for the game interface
	fwLevelProcessToolGameInterface * GetGameInterface() { return m_GameInterface; }

	// PURPOSE : Get/Set which physics archetypes are to be exported
	// 
	u32 GetCollisionArchetypeFlags() const { return m_iCollisionArchetypeFlags; }
	void SetCollisionArchetypeFlags(const u32 t) { m_iCollisionArchetypeFlags = t; }

	bool GetExportExteriorPortals() const { return m_bExportExteriorPortals; }
	void SetExportExteriorPortals(bool b) { m_bExportExteriorPortals = b; }

	bool GetExportPolyDensities() const { return m_bExportPolyDensities; }
	void SetExportPolyDensities(bool b) { m_bExportPolyDensities = b; }

	bool GetExportCoverBoxesForPillarShapes() const { return m_bExportCoverBoxesForPillarShapes; }
	void SetExportCoverBoxesForPillarShapes(bool b) { m_bExportCoverBoxesForPillarShapes = b; }

	u32 GetCoverBoxCollisionArchetypeFlags() const { return m_iCoverBoxCollisionArchetypeFlags; }
	void SetCoverBoxCollisionArchetypeFlags(u32 flags) { m_iCoverBoxCollisionArchetypeFlags = flags; }

protected:
	// -- fwFsm internal interface --

	virtual fwFsm::Return UpdateState(const s32 state, const s32 iMessage, const fwFsm::Event event);

	// -- fwLevelProcessTool internal interface --

	// PURPOSE:	Initialize the tool, including creating the game/exporter interfaces,
	//			and running their initialization functions.
	virtual fwFsm::Return Init();

	// PURPOSE:	Update function while running the tool, after initialization is complete.
	virtual fwFsm::Return Run_OnUpdate();

	// PURPOSE:	Shut down the tool.
	virtual fwFsm::Return Shutdown();

	// PURPOSE:	Virtual function for creating the exporter interface, for subclasses
	//			to override.
	// RETURNS:	Should return a pointer to a valid fwLevelProcessToolExporterInterface
	//			subclass, allocated using rage_new with the fwLevelProcessTool object
	//			assuming ownership. 
	virtual fwLevelProcessToolExporterInterface* CreateExporterInterface(fwLevelProcessTool * pTool) = 0;

	// PURPOSE:	Virtual function for creating the game interface, for subclasses
	//			to override.
	// RETURNS:	Should return a pointer to a valid fwLevelProcessToolGameInterface
	//			subclass, allocated using rage_new with the fwLevelProcessTool object
	//			assuming ownership.
 	virtual fwLevelProcessToolGameInterface* CreateGameInterface(fwLevelProcessTool * pTool) = 0;

	// -- fwLevelProcessTool data --

	// PURPOSE:	Pointer to the exporter interface, created by CreateExporterInterface()
	//			and to be destroyed when fwLevelProcessTool shuts down.
	fwLevelProcessToolExporterInterface*	m_ExporterInterface;

	// PURPOSE:	Pointer to the game interface, created by CreateGameInterface()
	//			and to be destroyed when fwLevelProcessTool shuts down.
	fwLevelProcessToolGameInterface*		m_GameInterface;

	// Which physics types this app will export (defaults to 0)
	u32						m_iCollisionArchetypeFlags;
	u32						m_iCoverBoxCollisionArchetypeFlags;
	bool					m_bExportExteriorPortals;
	bool					m_bExportPolyDensities;
	bool					m_bExportCoverBoxesForPillarShapes;
};

//-----------------------------------------------------------------------------

/*
PURPOSE
	Template for a subclass of fwLevelProcessTool, where the template parameters
	are the game and exporter interface subclasses to use. This template just
	makes it a bit more convenient for the user to create the interfaces.
*/
template<class _GameInterface, class _ExporterInterface> class fwLevelProcessToolImpl : public fwLevelProcessTool
{
public:

protected:
	// -- fwLevelProcessTool internal interface --

	virtual fwLevelProcessToolExporterInterface* CreateExporterInterface(fwLevelProcessTool * pTool)
	{	return rage_new _ExporterInterface(pTool);	}

	virtual fwLevelProcessToolGameInterface* CreateGameInterface(fwLevelProcessTool * pTool)
	{	return rage_new _GameInterface(pTool);	}
};

//------------------------------------------------------------------------------

}

//------------------------------------------------------------------------------

#endif	// FWNAVGEN_BASETOOL_H

// End of file fwnavgen/basetool.h
