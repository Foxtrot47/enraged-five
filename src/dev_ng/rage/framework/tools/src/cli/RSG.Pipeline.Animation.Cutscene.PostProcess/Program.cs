﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.Animation.Cutscene.PostProcess
{
    class Program
    {
        #region Constants
        private static readonly String OPTION_INPUTDIR = "inputdir";
        private static readonly String OPTION_OUTPUTDIR = "outputdir";
        private static readonly String OPTION_SCENENAME = "scenename";
        private static readonly String OPTION_FINALISEDIR = "finalisedir";
        private static readonly String OPTION_TASK_NAME = "taskname";
        private static readonly String LOG_CTX = "Post Process";
        #endregion // Constants

        [STAThread]
        static int Main(string[] args)
        {
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.CreateUniversalLog("PostProcess");
            try
            {
                // Define our executable options.  Stick with the long options
                // (e.g. --opt) rather than the single character versions.
                Getopt options = new Getopt(args, new LongOption[] {
                new LongOption(OPTION_INPUTDIR, LongOption.ArgType.Required,
                    "Input directory."),
                new LongOption(OPTION_OUTPUTDIR, LongOption.ArgType.Required,
                    "Output directory."),
                new LongOption(OPTION_SCENENAME, LongOption.ArgType.Required,
                    ""),
                new LongOption(OPTION_FINALISEDIR, LongOption.ArgType.Required,
                    ""),
                new LongOption(OPTION_TASK_NAME, LongOption.ArgType.Optional,
                    "Asset Pipeline 3 Task Name."),
                });
                // Turn args into vars
                String inputDir = options[OPTION_INPUTDIR] as String;
                String outputDir = options[OPTION_OUTPUTDIR] as String;
                String sceneName = options[OPTION_SCENENAME] as String;
                String finaliseDir = options[OPTION_FINALISEDIR] as String;
                String taskName = options[OPTION_TASK_NAME] == null ? String.Empty : options[OPTION_TASK_NAME] as String;

                if (String.Empty != taskName)
                {
                    Console.WriteLine("TASK: {0}", taskName);
                    log.MessageCtx(LOG_CTX, "TASK: {0}", taskName);
                }

                if (!Directory.Exists(inputDir))
                {
                    log.ErrorCtx(LOG_CTX, "Directory '{0}' does not exist", inputDir);
                    return RSG.Pipeline.Core.Constants.Exit_Failure;
                }

                //log.MessageCtx(LOG_CTX, "Creating directory '{0}'", outputDir);
                //if (!RSG.Pipeline.Services.Animation.DirectoryServices.CreateDirectory(outputDir))
                //{
                //    log.ErrorCtx(LOG_CTX, "Failed to create directory: '{0}'.  Aborting.", outputDir);
                //    return (Constants.Exit_Failure);
                //}

                // Work out how many sections we have, we use the camera as an indication as this should always exist

                string[] cameraAnims = Directory.GetFiles(inputDir, "*Camera*.anim");

                int numSections = cameraAnims.Length;

                if (numSections == 0)
                {
                    log.ErrorCtx(LOG_CTX, "Scene '{0}' contains 0 sections. Must have at least 1. (0 Camera animations)", sceneName);
                    if (Directory.Exists(outputDir))
                        Directory.Delete(outputDir, true);
                    return RSG.Pipeline.Core.Constants.Exit_Failure;
                }

                for (int i = 0; i < numSections; ++i)
                {
                    string[] sectionFiles = Directory.GetFiles(inputDir, String.Format("*-{0}.*", i));

                    using (Ionic.Zip.ZipFile zip = new Ionic.Zip.ZipFile())
                    {
                        foreach (string file in sectionFiles)
                            zip.AddFile(file, "");

                        log.MessageCtx(LOG_CTX, "Saved '{0}'.", Path.Combine(outputDir, String.Format("{0}-{1}.icd.zip", sceneName, i)));
                        zip.Save(Path.Combine(outputDir, String.Format("{0}-{1}.icd.zip", sceneName, i)));
                    }
                }

                if (Directory.Exists(finaliseDir))
                {
                    // Copy the cutxml as this is needed for PSO
                    string cutxmlFile = Path.Combine(finaliseDir, sceneName + ".cutxml");
                    if (File.Exists(cutxmlFile))
                    {
                        File.Copy(cutxmlFile, Path.Combine(outputDir, sceneName + ".cutxml"), true);
                    }
                    else
                    {
                        log.ErrorCtx(LOG_CTX, "File '{0}' does not exist.", cutxmlFile);
                        if (Directory.Exists(outputDir))
                            Directory.Delete(outputDir, true);
                        return RSG.Pipeline.Core.Constants.Exit_Failure;
                    }
                }
                else
                {
                    log.ErrorCtx(LOG_CTX, "Directory '{0}' does not exist.", finaliseDir);
                    if (Directory.Exists(outputDir))
                        Directory.Delete(outputDir, true);
                    return RSG.Pipeline.Core.Constants.Exit_Failure;
                }

                return RSG.Pipeline.Core.Constants.Exit_Success;
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Unhandled Exception during Post Process");
                return RSG.Pipeline.Core.Constants.Exit_Failure;
            }
            finally
            {
                LogFactory.ApplicationShutdown();
            }
        }
    }
}
