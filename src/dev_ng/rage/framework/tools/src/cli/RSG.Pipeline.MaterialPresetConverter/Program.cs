﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

using RSG.Base.Collections;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Model.MaterialPresets;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services;

namespace RSG.Pipeline.MaterialPresetConverter
{
    class Program
    {
        #region Constants
        private static readonly String LOG_CTX = "Material Preset Converter";
        private static readonly String OPT_INPUT = "input";
        private static readonly String OPT_OUTPUT = "output";
        private static readonly String OPT_FORCE = "force";
        private static readonly String OPT_SUBDIRS = "subdirs";
        private static readonly String OPT_DRAWABLE = "inputDrawable";
        #endregion // Constants

        #region Entry Point
        static int Main(String[] args)
        {
            int result = Constants.Exit_Success;
            LongOption[] lopts = new LongOption[] {
                    new LongOption(OPT_INPUT, LongOption.ArgType.Required, "Input file or directory."),
                    new LongOption(OPT_OUTPUT, LongOption.ArgType.Required, "Output .zip file."),
                    new LongOption(OPT_FORCE, LongOption.ArgType.Optional, "Force converts; circumvents dependencies."),
                    new LongOption(OPT_SUBDIRS, LongOption.ArgType.Optional, "Input directory has subdirs that need to be scanned."),
                    new LongOption(OPT_DRAWABLE, LongOption.ArgType.Optional, "Input drawable to be extracted."),
                };

            CommandOptions options = new CommandOptions(args, lopts);
            IUniversalLog log = LogFactory.CreateUniversalLog("Material Preset Converter");
            IUniversalLogTarget logFile = LogFactory.CreateUniversalLogFile(log);
            LogFactory.CreateApplicationConsoleLogTarget();
            try
            {
                Debug.Assert(options.ContainsOption(OPT_INPUT), "No input option defined.");
                Debug.Assert(options.ContainsOption(OPT_OUTPUT), "No input option defined.");
                if (options.ShowHelp || !options.ContainsOption(OPT_INPUT) || !options.ContainsOption(OPT_OUTPUT))
                {
                    log.MessageCtx(LOG_CTX, options.GetUsage());
                    return (result);
                }

                //Find any template files and convert them.
                String input = options[OPT_INPUT] as String;
                String output = options[OPT_OUTPUT] as String;
                List<string> templateFiles = new List<string>();
                bool searchSubDirs = options.ContainsOption(OPT_SUBDIRS);

                // Extract the drawable before processing the data.
                if (options.ContainsOption(OPT_DRAWABLE))
                {
                    String inputDrawable = options[OPT_DRAWABLE] as String;
                    Zip.ExtractNewer(inputDrawable, input);
                }

                if (Directory.Exists(input))
                {
                    if (searchSubDirs)
                    {
                        foreach (string subDir in Directory.GetDirectories(input))
                        {
                            templateFiles.AddRange(System.IO.Directory.GetFiles(subDir, "*" + MaterialPreset.MaterialObjectExtension));
                            templateFiles.AddRange(System.IO.Directory.GetFiles(subDir, "*" + MaterialPreset.MaterialTemplateExportExtension));
                        }
                    }
                    else
                    {
                        templateFiles.AddRange(System.IO.Directory.GetFiles(input, "*" + MaterialPreset.MaterialObjectExtension));
                        templateFiles.AddRange(System.IO.Directory.GetFiles(input, "*" + MaterialPreset.MaterialTemplateExportExtension));
                    }
                }
                else
                {
                    templateFiles.Add(input);
                }

                foreach (string template in templateFiles)
                {
                    MaterialPreset preset;
                    if (MaterialPreset.LoadPreset(options.Branch, log, template, out preset, options.Config) == false)
                    {
                        String drawableName = Path.GetFileName(output);
                        log.ErrorCtx(LOG_CTX, "Unable to parse preset {0} within drawable {1}.", template, output);
                        continue;
                    }
                
                    //Expand the preset to include all values from its parents.
                    preset.ExpandPreset();

                    preset.FixShaderIndices();

                    //Save the shader variable file.
                    string svaFilename = Path.Combine(Path.GetDirectoryName(template), Path.GetFileNameWithoutExtension(template) + ".sva");
                    if (!MaterialPreset.WriteSvaFile(svaFilename, preset))
                    {
                        log.ErrorCtx(LOG_CTX, "Could not create sva file for template: {0} - Shader variables are probably mismatched.", template);
                        continue;
                    }
                    else
                    {
                        System.IO.File.Delete(template);
                    }
                }

                //Regenerate the drawable .zip file.
                if (File.Exists(output))
                    System.IO.File.Delete(output);

                if (searchSubDirs)
                {
                    List<Pair<String, String>> file_list = new List<Pair<String, String>>();

                    foreach (string subDir in System.IO.Directory.GetDirectories(input))
                    {
                        foreach (string subFile in System.IO.Directory.GetFiles(subDir))
                        {
                            // Because we'll be stiching an .idd back together, we need to know the "directory" inside the zip structure
                            // it goes into. The dirName variable needs to be a qualified path otherwise the Path.GetDirectoryName on the zip
                            // side resolves to nothing.
                            string directory = Path.GetDirectoryName(subFile);
                            string relativeDir = Path.GetFileName(directory);
                            string dirName = "\\" + relativeDir + "\\";

                            file_list.Add(new Pair<String, String>(subFile, dirName));
                        }
                    }

                    Zip.Create(options.Branch, output, file_list, true);
                }
                else
                {
                    string[] drawableFiles;
                    drawableFiles = System.IO.Directory.GetFiles(input, "*", SearchOption.AllDirectories);
                    Zip.Create(options.Branch, output, drawableFiles);
                }

                if (log.HasErrors || Log.StaticLog.HasErrors)
                    result = Constants.Exit_Failure;
            }
            catch (Exception ex)
            {
                log.ExceptionCtx(LOG_CTX, ex, "Unhandled exception during Material Preset Converter.");
                result = Constants.Exit_Failure;
            }
            return (result);
        }
        #endregion
    }
}
