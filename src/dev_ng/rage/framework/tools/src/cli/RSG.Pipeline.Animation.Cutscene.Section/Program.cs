﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Xml;
using System.IO;
using System.Threading;
using RSG.Base.OS;
using RSG.Pipeline.Core;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace RSG.Pipeline.Animation.Cutscene.Section
{
    class Program
    {
        #region Constants
        private static readonly String OPTION_INPUTDIR = "inputdir";
        private static readonly String OPTION_INPUTFACEDIR = "inputfacedir";
        private static readonly String OPTION_INPUTLIGHTDIR = "inputlightdir";
        private static readonly String OPTION_OUTPUTDIR = "outputdir";
        private static readonly String OPTION_ANIMSECTION = "cutfanimsection";
        private static readonly String OPTION_ANIMEDIT = "animedit";
        private static readonly String OPTION_CLIPEDIT = "clipedit";
        private static readonly String OPTION_TASK_NAME = "taskname";
        private static readonly String OPTION_MULTITHREADED = "multithreaded";
        private static readonly String OPTION_USEOLDMINTIME = "useOldMinSectionTime";
        private static readonly String LOG_CTX = "Section";
        #endregion // Constants

        private static List<Thread> ActiveThreads;
        private static int MaximumThreads;
        private static bool ProcessRunsSuccess;
        private static bool MultiThreaded;

        public class AnimCropEntry
        {
            public AnimCropEntry(string animedit, string infile, string outfile, string cropin, string cropout)
            {
                _animedit = animedit;
                _infile = infile;
                _outfile = outfile;
                _cropin = cropin;
                _cropout = cropout;
            }

            public string _animedit;
            public string _infile;
            public string _outfile;
            public string _cropin;
            public string _cropout;
        }

        public class ClipCropEntry
        {
            public ClipCropEntry(string clipedit, string infile, string outfile, string cropin, string cropout, string animfile, string outputpath)
            {
                _clipedit = clipedit;
                _infile = infile;
                _outfile = outfile;
                _cropin = cropin;
                _cropout = cropout;
                _animfile = animfile;
                _outputpath = outputpath;
            }

            public string _clipedit;
            public string _infile;
            public string _outfile;
            public string _cropin;
            public string _cropout;
            public string _animfile;
            public string _outputpath;
        }
        private static IUniversalLog log = null;

        static int RunProcess(string strExe, string strArgs)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.Arguments = strArgs;
            startInfo.FileName = strExe;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.CreateNoWindow = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false;

            log.MessageCtx(LOG_CTX, strExe + " " + strArgs);

            Process proc = Process.Start(startInfo);
            String output = proc.StandardOutput.ReadToEnd();
            proc.WaitForExit();

            if(output != String.Empty)
                log.MessageCtx(LOG_CTX, output);

            return proc.ExitCode;
        }

        //Threading
        public static void ProcessSectionAnimationThread(object run)
        {
            AnimCropEntry c = (AnimCropEntry)run;

            string animEditArgs = String.Format("-anim \"{0}\" -out \"{1}\" -op crop={2},{3} -nopopups", c._infile, c._outfile, c._cropin, c._cropout);

            int processResult = RunProcess(c._animedit, animEditArgs);
            if (processResult != 0)
            {
                ProcessRunsSuccess = false;
            }
        }

        public static void ProcessSectionClipThread(object run)
        {
            ClipCropEntry c = (ClipCropEntry)run;

            string clipEditArgs = String.Format("-clip \"{0}\" -out \"{1}\" -clipanimation \"{2},{3},,{4}\" -path \"{5}\" -nopopups", c._infile, c._outfile, c._cropin, c._cropout, c._animfile, c._outputpath);

            int processResult = RunProcess(c._clipedit, clipEditArgs);
            if (processResult != 0)
            {
                ProcessRunsSuccess = false;
            }
        }

        public static int SectionAnimations(List<AnimCropEntry> cropEntries)
        {
            if (MultiThreaded)
            {
                int currentThreadIndex = 0;
                ProcessRunsSuccess = true;
                ActiveThreads.Clear();

                while (currentThreadIndex < cropEntries.Count)
                {
                    for (int threadIndex = 0; threadIndex < ActiveThreads.Count; ++threadIndex)
                    {
                        Thread currentThread = ActiveThreads[threadIndex];
                        if (currentThread.IsAlive == false)
                        {
                            //Remove this from the list.
                            ActiveThreads.RemoveAt(threadIndex);
                            threadIndex--;
                        }
                    }

                    while (ActiveThreads.Count < MaximumThreads && currentThreadIndex < cropEntries.Count)
                    {
                        Thread newRunThread = new Thread(ProcessSectionAnimationThread);
                        //newRunThread.SetApartmentState(ApartmentState.STA);
                        newRunThread.Name = "Process Run Thread " + currentThreadIndex;
                        ActiveThreads.Add(newRunThread);

                        AnimCropEntry comp = (AnimCropEntry)cropEntries[currentThreadIndex];
                        
                        newRunThread.Start(comp);
                        currentThreadIndex++;
                    }
                }

                foreach (Thread cropThread in ActiveThreads)
                {
                    cropThread.Join();
                }

                if (!ProcessRunsSuccess)
                {
                    return -1;
                }
            }
            else
            {
                foreach (AnimCropEntry entry in cropEntries)
                {
                    string animEditArgs = String.Format("-anim \"{0}\" -out \"{1}\" -op crop={2},{3} -nopopups", entry._infile, entry._outfile, entry._cropin, entry._cropout);

                    if (RunProcess(entry._animedit, animEditArgs) != 0) return -1;
                }
            }

            return 0;
        }

        public static int SectionClips(List<ClipCropEntry> cropEntries)
        {
            if (MultiThreaded)
            {
                int currentThreadIndex = 0;
                ProcessRunsSuccess = true;
                ActiveThreads.Clear();

                while (currentThreadIndex < cropEntries.Count)
                {
                    for (int threadIndex = 0; threadIndex < ActiveThreads.Count; ++threadIndex)
                    {
                        Thread currentThread = ActiveThreads[threadIndex];
                        if (currentThread.IsAlive == false)
                        {
                            //Remove this from the list.
                            ActiveThreads.RemoveAt(threadIndex);
                            threadIndex--;
                        }
                    }

                    while (ActiveThreads.Count < MaximumThreads && currentThreadIndex < cropEntries.Count)
                    {
                        Thread newRunThread = new Thread(ProcessSectionClipThread);
                        //newRunThread.SetApartmentState(ApartmentState.STA);
                        newRunThread.Name = "Process Run Thread " + currentThreadIndex;
                        ActiveThreads.Add(newRunThread);

                        ClipCropEntry crop = (ClipCropEntry)cropEntries[currentThreadIndex];

                        newRunThread.Start(crop);
                        currentThreadIndex++;
                    }
                }

                foreach (Thread cropThread in ActiveThreads)
                {
                    cropThread.Join();
                }

                if (!ProcessRunsSuccess)
                {
                    return -1;
                }
            }
            else
            {
                foreach (ClipCropEntry entry in cropEntries)
                {
                    string clipEditArgs = String.Format("-clip \"{0}\" -out \"{1}\" -clipanimation \"{2},{3},,{4}\" -path \"{5}\" -nopopups", entry._infile, entry._outfile, entry._cropin, entry._cropout, entry._animfile, entry._outputpath);

                    if (RunProcess(entry._clipedit, clipEditArgs) != 0) return -1;
                }
            }

            return 0;
        }

        static bool SectionAnim(string animEditExe, string inputDir, string outputDir)
        {
            try
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(Path.Combine(outputDir, "anim_section.targets"));

                List<string> alInputAnimArray = new List<string>();
                List<string> alOutputAnimArray = new List<string>();
                List<string> alOpsStartArray = new List<string>();
                List<string> alOpsEndArray = new List<string>();

                System.Xml.XmlNamespaceManager xmlnsManager = new System.Xml.XmlNamespaceManager(xmlDoc.NameTable);
                xmlnsManager.AddNamespace("ns", xmlDoc.DocumentElement.NamespaceURI);

                XmlNodeList cropItemNodes = xmlDoc.SelectNodes("/ns:Project/ns:ItemGroup/ns:CropAnimItem", xmlnsManager);
                XmlNodeList cropOutputAnimFileNodes = xmlDoc.SelectNodes("/ns:Project/ns:ItemGroup/ns:CropAnimItem/ns:outputFile", xmlnsManager);
                XmlNodeList cropOpStartNodes = xmlDoc.SelectNodes("/ns:Project/ns:ItemGroup/ns:CropAnimItem/ns:start", xmlnsManager);
                XmlNodeList cropOpEndNodes = xmlDoc.SelectNodes("/ns:Project/ns:ItemGroup/ns:CropAnimItem/ns:end", xmlnsManager);
                foreach (XmlNode n in cropItemNodes) alInputAnimArray.Add(n.Attributes["Include"].InnerText);
                foreach (XmlNode n in cropOutputAnimFileNodes) alOutputAnimArray.Add(n.InnerText);
                foreach (XmlNode n in cropOpStartNodes) alOpsStartArray.Add(n.InnerText);
                foreach (XmlNode n in cropOpEndNodes) alOpsEndArray.Add(n.InnerText);

                SubstEnv(alInputAnimArray, inputDir, outputDir);
                SubstEnv(alOutputAnimArray, inputDir, outputDir);
                SubstEnv(alOpsStartArray, inputDir, outputDir);
                SubstEnv(alOpsEndArray, inputDir, outputDir);

                List<AnimCropEntry> animEntries = new List<AnimCropEntry>();

                for (int i = 0; i < alInputAnimArray.Count; ++i)
                {
                    AnimCropEntry crop = new AnimCropEntry(animEditExe, alInputAnimArray[i], alOutputAnimArray[i], alOpsStartArray[i], alOpsEndArray[i]);
                    animEntries.Add(crop);

                    if (!File.Exists(alInputAnimArray[i]))
                    {
                        log.ErrorCtx(LOG_CTX, "File '{0}' does not exist.", alInputAnimArray[i]);
                        return false;
                    }
                }

                if (SectionAnimations(animEntries) != 0) return false;
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Unhandled Exception during Cutscene Section Process in SectionAnim function");
                return false;
            }

            return true;
        }

        static bool SectionClip(string clipEditExe, string inputDir, string outputDir)
        {
            try
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(Path.Combine(outputDir, "anim_section.targets"));

                List<string> alInputClipArray = new List<string>();
                List<string> alOutputClipArray = new List<string>();
                List<string> alOpsStartArray = new List<string>();
                List<string> alOpsEndArray = new List<string>();
                List<string> alAnimFileArray = new List<string>();

                System.Xml.XmlNamespaceManager xmlnsManager = new System.Xml.XmlNamespaceManager(xmlDoc.NameTable);
                xmlnsManager.AddNamespace("ns", xmlDoc.DocumentElement.NamespaceURI);

                XmlNodeList cropItemNodes = xmlDoc.SelectNodes("/ns:Project/ns:ItemGroup/ns:CropClipItem", xmlnsManager);
                XmlNodeList cropOutputAnimFileNodes = xmlDoc.SelectNodes("/ns:Project/ns:ItemGroup/ns:CropClipItem/ns:outputFile", xmlnsManager);
                XmlNodeList cropOpStartNodes = xmlDoc.SelectNodes("/ns:Project/ns:ItemGroup/ns:CropClipItem/ns:start", xmlnsManager);
                XmlNodeList cropOpEndNodes = xmlDoc.SelectNodes("/ns:Project/ns:ItemGroup/ns:CropClipItem/ns:end", xmlnsManager);
                XmlNodeList animFileNodes = xmlDoc.SelectNodes("/ns:Project/ns:ItemGroup/ns:CropClipItem/ns:animFile", xmlnsManager);
                foreach (XmlNode n in cropItemNodes) alInputClipArray.Add(n.Attributes["Include"].InnerText);
                foreach (XmlNode n in cropOutputAnimFileNodes) alOutputClipArray.Add(n.InnerText);
                foreach (XmlNode n in cropOpStartNodes) alOpsStartArray.Add(n.InnerText);
                foreach (XmlNode n in cropOpEndNodes) alOpsEndArray.Add(n.InnerText);
                foreach (XmlNode n in animFileNodes) alAnimFileArray.Add(n.InnerText);

                SubstEnv(alInputClipArray, inputDir, outputDir);
                SubstEnv(alOutputClipArray, inputDir, outputDir);
                SubstEnv(alOpsStartArray, inputDir, outputDir);
                SubstEnv(alOpsEndArray, inputDir, outputDir);
                SubstEnv(alAnimFileArray, inputDir, outputDir);

                List<ClipCropEntry> clipEntries = new List<ClipCropEntry>();

                for (int i = 0; i < alInputClipArray.Count; ++i)
                {
                    ClipCropEntry clipEntry = new ClipCropEntry(clipEditExe, alInputClipArray[i], alOutputClipArray[i], alOpsStartArray[i], alOpsEndArray[i], alAnimFileArray[i], outputDir);
                    clipEntries.Add(clipEntry);

                    if (!File.Exists(alInputClipArray[i]))
                    {
                       log.ErrorCtx(LOG_CTX, "File '{0}' does not exist.", alInputClipArray[i]);
                        return false;
                    }
                }

                if (SectionClips(clipEntries) != 0) return false;
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Unhandled Exception during Cutscene Section Process in SectionClip function");
                return false;
            }

            return true;
        }

        static void SubstEnv(List<string> alArray, string inputDir, string outputDir)
        {
            for (int i = 0; i < alArray.Count; ++i)
            {
                alArray[i] = alArray[i].Replace("$(inputFaceDir)", inputDir);
                alArray[i] = alArray[i].Replace("$(inputDir)", inputDir);
                alArray[i] = alArray[i].Replace("$(intermediateDir)", inputDir);
                alArray[i] = alArray[i].Replace("$(outputDir)", outputDir);
            }
        }

        [STAThread]
        static int Main(string[] args)
        {
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            log = LogFactory.CreateUniversalLog("Section");
            try
            {
                // Define our executable options.  Stick with the long options
                // (e.g. --opt) rather than the single character versions.
                Getopt options = new Getopt(args, new LongOption[] {
                    new LongOption(OPTION_INPUTDIR, LongOption.ArgType.Required,
                        "Input directory."),
                    new LongOption(OPTION_INPUTFACEDIR, LongOption.ArgType.Required,
                        ""),
                    new LongOption(OPTION_INPUTLIGHTDIR, LongOption.ArgType.Required,
                        ""),
                    new LongOption(OPTION_OUTPUTDIR, LongOption.ArgType.Required,
                        "Output directory."),
                    new LongOption(OPTION_ANIMSECTION, LongOption.ArgType.Required,
                        ""),
                    new LongOption(OPTION_ANIMEDIT, LongOption.ArgType.Required,
                        "animedit filename."),
                    new LongOption(OPTION_CLIPEDIT, LongOption.ArgType.Required,
                        "clipedit filename."),
                    new LongOption(OPTION_TASK_NAME, LongOption.ArgType.Optional,
                        "Asset Pipeline 3 Task Name."),
                    new LongOption(OPTION_MULTITHREADED, LongOption.ArgType.Required,
                        ""),
                    new LongOption(OPTION_USEOLDMINTIME, LongOption.ArgType.None,
                        "If specified, will section the animations using the old minimum time (that shipped assets were built with)"),
                });

                // Turn args into vars
                String inputDir = options[OPTION_INPUTDIR] as String;
                String inputFaceDir = options[OPTION_INPUTFACEDIR] as String;
                String inputLightDir = options[OPTION_INPUTLIGHTDIR] as String;
                String outputDir = options[OPTION_OUTPUTDIR] as String;
                String cutfAnimSectionExe = options[OPTION_ANIMSECTION] as String;
                String animEditExe = options[OPTION_ANIMEDIT] as String;
                String clipEditExe = options[OPTION_CLIPEDIT] as String;
                String taskName = options[OPTION_TASK_NAME] == null ? String.Empty : options[OPTION_TASK_NAME] as String;
                MultiThreaded = (String.Compare(options[OPTION_MULTITHREADED] as String, "true", true) == 0);

                if (String.Empty != taskName)
                {
                    Console.WriteLine("TASK: {0}", taskName);
                    log.MessageCtx(LOG_CTX, "TASK: {0}", taskName);
                }

                ActiveThreads = new List<Thread>();
                MaximumThreads = Environment.ProcessorCount + 2;

                log.MessageCtx(LOG_CTX, "MultiThreading : {0} ({1} max threads)", options[OPTION_MULTITHREADED] as String, MaximumThreads.ToString());

                if (!Directory.Exists(inputDir))
                {
                    log.ErrorCtx(LOG_CTX, "Directory '{0}' does not exist", inputDir);
                    return RSG.Pipeline.Core.Constants.Exit_Failure;
                }

                //log.MessageCtx(LOG_CTX, "Creating directory '{0}'", outputDir);
                //if (!RSG.Pipeline.Services.Animation.DirectoryServices.CreateDirectory(outputDir))
                //{
                //    log.ErrorCtx(LOG_CTX, "Failed to create directory: '{0}'.  Aborting.", outputDir);
                //    return (Constants.Exit_Failure);
                //}


                // Generate the anim_section.targets file using cutfanimcombine
                String cutfAnimSectionArgs = String.Format("-inputDir \"{0}\" -cutsceneFile \"{1}\" -intermediateDir \"{2}\" -inputFaceDir \"{3}\" -hideAlloc -nopopups", inputDir, Path.Combine(inputLightDir, "data_stream.cutxml"), outputDir, Path.Combine(inputDir, "faces"));
                if (options.HasOption(OPTION_USEOLDMINTIME))
                {
                    cutfAnimSectionArgs += String.Format(" -{0}", OPTION_USEOLDMINTIME);
                }

                if (RunProcess(cutfAnimSectionExe, cutfAnimSectionArgs) != 0)
                {
                    if (Directory.Exists(outputDir))
                        Directory.Delete(outputDir, true);
                    return RSG.Pipeline.Core.Constants.Exit_Failure;
                }

                if (!SectionAnim(animEditExe, inputFaceDir, outputDir))
                {
                    if (Directory.Exists(outputDir))
                        Directory.Delete(outputDir, true);
                    return RSG.Pipeline.Core.Constants.Exit_Failure;
                }

                if (!SectionClip(clipEditExe, inputFaceDir, outputDir))
                {
                    if (Directory.Exists(outputDir))
                        Directory.Delete(outputDir, true);
                    return RSG.Pipeline.Core.Constants.Exit_Failure;
                }

                return RSG.Pipeline.Core.Constants.Exit_Success;
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Unhandled Exception during Cutscene Section Process");
                return RSG.Pipeline.Core.Constants.Exit_Failure;
            }
            finally
            {
                LogFactory.ApplicationShutdown();
            }
        }
    }
}
