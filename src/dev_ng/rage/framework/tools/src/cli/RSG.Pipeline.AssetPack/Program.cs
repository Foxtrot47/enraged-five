﻿using System;
using System.Collections.Generic;
using SIO = System.IO;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;
using RSG.Pipeline.Engine;
using RSG.Pipeline.Services;
using RSG.Pipeline.Core.Build;

namespace RSG.Pipeline.AssetPack
{

    /// <summary>
    /// Pipeline Asset Packaging executable.
    /// </summary>
    /// This wrapper replaces IronRuby data_asset_pack.rb script and was
    /// implemented because of issues in IR64.exe.
    class Program
    {
        #region Constants
        private static readonly String AUTHOR = "RSGEDI Tools";
        private static readonly String EMAIL = "*tools@rockstarnorth.com";
        private static readonly String ASSETPACK_PROCESSOR = "RSG.Pipeline.Processor.Common.AssetPackProcessor";
        
        private static readonly String LOG_CTX = "Asset Pack";
        private static readonly String OPTION_REBUILD = "rebuild";
        private static readonly String OPTION_FLAG_NO_SYNC = "no-sync";
        private static readonly String OPTION_FLAG_NO_XGE = "no-xge";
        #endregion // Constants

        /// <summary>
        /// Asset packaging entry-point.
        /// </summary>
        /// <param name="args"></param>
        [STAThread]
        static int Main(String[] args)
        {
            // Merge our options with the default pipeline command options.
            // Define our executable options.  Stick with the long options
            // (e.g. --opt) rather than the single character versions.
            LongOption[] opts = new LongOption[] {
                new LongOption(OPTION_REBUILD, LongOption.ArgType.None,
                    "Rebuild data; ignoring timestamp information."), 
                new LongOption(OPTION_FLAG_NO_SYNC, LongOption.ArgType.Required,
                    "Disable engine Perforce dependency sync."),
                new LongOption(OPTION_FLAG_NO_XGE, LongOption.ArgType.None,
                    "Disable Xoreax XGE parallel processing engine.")
            };
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.CreateUniversalLog(LOG_CTX);
            CommandOptions options = new CommandOptions(args, opts);
            int exit_code = Constants.Exit_Success;

            try
            {
                // Display help is requested.
                if (options.ShowHelp)
                {
                    String processName = System.Diagnostics.Process.GetCurrentProcess().ProcessName;
                    ShowUsage(processName, options);
                    return (Constants.Exit_Success);
                }
                else if (!Pack(log, options))
                {
                    log.ErrorCtx(LOG_CTX, "Asset packing failed.  See previous errors.");
                    exit_code = Constants.Exit_Failure;
                }
            }
            catch (Exception ex)
            {
                exit_code = Constants.Exit_Failure;
                RSG.Base.Windows.ExceptionStackTraceDlg dlg =
                    new RSG.Base.Windows.ExceptionStackTraceDlg(ex, EMAIL, AUTHOR);
                dlg.ShowDialog();
            }
            finally
            {
                LogFactory.ApplicationShutdown();
            }
            return (exit_code);
        }

        #region Private Methods
        /// <summary>
        /// Print usage information to console output.
        /// </summary>
        /// <param name="program"></param>
        /// <param name="options"></param>
        private static void ShowUsage(String program, CommandOptions options)
        {
            Console.WriteLine(program);
            Console.WriteLine("Usage:");
            Console.WriteLine(options.GetUsage());
        }

        /// <summary>
        /// Run the packaging.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        private static bool Pack(IUniversalLog log, CommandOptions options)
        {
            // Initialise engine flags.
            EngineFlags flags = EngineFlags.Default;
            if (options.ContainsOption(OPTION_REBUILD))
                flags |= EngineFlags.Rebuild;
            if (options.ContainsOption(OPTION_FLAG_NO_SYNC))
                flags &= ~EngineFlags.SyncDependencies;
            if (options.ContainsOption(OPTION_FLAG_NO_XGE))
                flags &= ~EngineFlags.XGE;
            if (options.NoPopups)
                flags &= ~EngineFlags.Popups;

            // Initialise engine and content-tree.
            IEngine engine = new RSG.Pipeline.Engine.Engine(options, options.Config, options.Project, options.Branch, flags);
            IContentTree tree = Factory.CreateEmptyTree(options.Branch);
            IProcessor processor = engine.Processors.Where(p => p.Name.Equals(ASSETPACK_PROCESSOR)).FirstOrDefault();
            if (null == processor)
            {
                log.ErrorCtx(LOG_CTX, "Asset Pack Processor not available: {0}.  Aborting.",
                    ASSETPACK_PROCESSOR);
                return (false);
            }

            // Construct an Asset Pack Process.
            IInput buildInput = new BuildInput(options.Branch);
            ProcessBuilder pb = new ProcessBuilder(processor, tree);
            foreach (String filename in options.TrailingArguments)
            {
                pb.Inputs.Add(tree.CreateFile(filename));
            }
            buildInput.RawProcesses.Add(pb.ToProcess());

            TimeSpan ts;
            IEnumerable<IOutput> outputs;
            bool buildResult = engine.Build(ref buildInput, out outputs, out ts);
            log.MessageCtx(LOG_CTX, "Build Took: {0}.", ts.ToString());

            return (buildResult);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.AssetPack namespace
