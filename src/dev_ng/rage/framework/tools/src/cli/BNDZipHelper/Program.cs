﻿namespace BNDZipHelper
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.IO.Compression;
    using RSG.Base.OS;
    using RSG.Bounds;
    using RSG.ManagedRage;

    /// <summary>
    /// 
    /// </summary>
    class Program
    {
        #region Fields
        /// <summary>
        /// The name for the commandline argument that is used to pass in the input filename.
        /// </summary>
        private const string InputOption = "input";
        #endregion Fields

        static void Main(string[] args)
        {
            Getopt options = new Getopt(args, new LongOption[]
            {
                new LongOption(InputOption, LongOption.ArgType.Required,
                    "")
            });

            string filename = options.Options[InputOption] as string;
            FileMode mode = FileMode.Open;
            FileAccess access = FileAccess.Read;
            ZipArchiveMode archiveMode = ZipArchiveMode.Read;
            using (FileStream zipToOpen = new FileStream(filename, mode, access))
            {
                using (ZipArchive archive = new ZipArchive(zipToOpen, archiveMode))
                {
                    foreach (ZipArchiveEntry entry in archive.Entries)
                    {
                        if (!entry.FullName.EndsWith("collision.zip"))
                        {
                            continue;
                        }

                        using (Stream collisionStream = entry.Open())
                        {
                            foreach (BNDFile file in LoadBoundsFromZipFile(collisionStream))
                            {
                                foreach (string type in ExtractProceduralTypes(file))
                                {
                                    Console.Out.WriteLine(type);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Loads the single bounds data file found in a particular .ibn.zip file
        /// </summary>
        /// <param name="boundFileName"></param>
        /// <param name="zipStream"></param>
        /// <returns></returns>
        private static IEnumerable<BNDFile> LoadBoundsFromZipFile(Stream zipStream)
        {
            using (ZipArchive archive = new ZipArchive(zipStream, ZipArchiveMode.Read))
            {
                foreach (ZipArchiveEntry entry in archive.Entries)
                {
                    if (!entry.FullName.EndsWith(".bnd"))
                    {
                        continue;
                    }

                    using (Stream bndStream = entry.Open())
                    {
                        yield return BNDFile.Load(bndStream);
                    }
                }
            }
        }

        /// <summary>
        /// Recursively processes bound object's to extract all the procedural types attached
        /// to the materials that aren't set to default.
        /// </summary>
        private static IEnumerable<string> ExtractProceduralTypes(BNDFile boundFile)
        {
            if (boundFile == null || boundFile.BoundObject == null)
            {
                yield break;
            }

            foreach (string proceduralType in ExtractProceduralTypes(boundFile.BoundObject))
            {
                yield return proceduralType;
            }
        }

        /// <summary>
        /// Recursively processes bound object's to extract all the procedural types attached
        /// to the materials that aren't set to default.
        /// </summary>
        private static IEnumerable<string> ExtractProceduralTypes(BoundObject boundObject)
        {
            if (boundObject == null)
            {
                yield break;
            }

            Composite composite = boundObject as Composite;
            if (composite != null)
            {
                foreach (CompositeChild child in composite.Children)
                {
                    foreach (string proceduralType in ExtractProceduralTypes(child.BoundObject))
                    {
                        yield return proceduralType;
                    }
                }
            }
            else
            {
                BVH bvh = BoundObjectConverter.ToBVH(boundObject);
                foreach (string mat in bvh.Materials)
                {
                    BVHMaterial material = new BVHMaterial(mat);
                    if (material.ProceduralModifier != "0")
                    {
                        yield return material.ProceduralModifier;
                    }
                }
            }
        }
    } // BNDZipHelper.Program {Class}
} // BNDZipHelper {Namespace}
