﻿using System;
using System.Collections.Generic;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Build;
using RSG.Pipeline.Engine;
using RSG.Pipeline.Services;
using RSG.SourceControl.Perforce;

namespace RSG.Pipeline.LeakTest
{

    /// <summary>
    /// 
    /// </summary>
    class Program
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        static int Main(String[] args)
        {
            LongOption[] opts = new LongOption[] {
            };
            CommandOptions options = new CommandOptions(args, opts);

            LogFactory.Initialize();
            IUniversalLog log = LogFactory.CreateUniversalLog("LeakTest");
            LogFactory.CreateApplicationConsoleLogTarget();

            log.Message("Press key to continue...");
            Console.ReadKey();
            P4 p4 = new P4();

            int it = 0;
            while (true)
            {
#if PERFORCE_CONNECT_TEST
                log.Message("Iteration: {0}", it);
                if (it % 100 == 0)
                {
                    log.Message("[start] Press key to continue...");
                    Console.ReadKey();
                }

                p4.Connect();
                System.Threading.Thread.Sleep(1000);
               // p4.Disconnect();

                ++it;
#endif // PERFORCE_CONNECT_TEST

#if true // ENGINE_BUILD_TEST
                log.Message("Iteration: {0}", it++);
                if (it % 100 == 0)
                {
                    log.Message("[start] Press key to continue...");
                    Console.ReadKey();
                }

                ICollection<String> inputs = new List<String>();
                IEnumerable<IOutput> outputs;
                TimeSpan buildTime;
                inputs.Add(@"x:\gta5\assets\export\anim\ingame\ak_test");

                IEngineDelegate engineDelegates = new Engine.EngineDelegate();
                Engine.Engine engine = new Engine.Engine(options, options.Config, options.Project, options.Branch, EngineFlags.Default | EngineFlags.Rebuild);
                engine.Build(inputs, out outputs, out buildTime);

                //log.Message("[end] Press key to continue...");
                //Console.ReadKey();
#endif // ENGINE_BUILD_TEST

            }
            
            return (Constants.Exit_Success);
        }
    }

} // RSG.Pipeline.LeakTest namespace
