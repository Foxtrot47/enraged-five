﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace GameTextLabelExtractor
{
    /// <summary>
    /// 
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// Program entry point.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static int Main(string[] args)
        {
            // Make sure the 
            if (args.Length != 4)
            {
                Console.WriteLine("Incorrect argument count.  Expected 4 got {0}.", args.Length);
                ShowUsage();
                return 1;
            }

            String inputFiles = null;
            String outputFile = null;
            if (args[0] == "--input")
            {
                inputFiles = args[1];
            }
            else if (args[0] == "--output")
            {
                outputFile = args[1];
            }

            if (args[2] == "--input")
            {
                inputFiles = args[3];
            }
            else if (args[2] == "--output")
            {
                outputFile = args[3];
            }

            if (inputFiles == null || outputFile == null)
            {
                Console.WriteLine("Missing either the --input or --output commandline parameter.");
                ShowUsage();
                return 1;
            }

            // Process the input files.
            IList<String> allTextLabels = new List<String>();

            String inputDirectory = Path.GetDirectoryName(inputFiles);
            String inputPattern = Path.GetFileName(inputFiles);
            foreach (String file in Directory.EnumerateFiles(inputDirectory, inputPattern))
            {
                foreach (String label in GetLabelsInFile(file))
                {
                    allTextLabels.Add(label);
                }
            }

            // Generate the output file.
            using (TextWriter writer = new StreamWriter(outputFile))
            {
                foreach (String label in allTextLabels)
                {
                    writer.WriteLine(label);
                }
            }

            return 0;
        }

        /// <summary>
        /// Outputs information on how to use the application.
        /// </summary>
        private static void ShowUsage()
        {
            Console.WriteLine("Example Usage:");
            Console.WriteLine(@"GameTextLabelExtractor.exe --input x:\gta5\assets_ng\GameText\American\american*.txt --output x:\gta5\assets_ng\GameText\American\allAmericanLabels.txt");
        }

        /// <summary>
        /// Parses a text file extracting all the text labels.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private static IEnumerable<String> GetLabelsInFile(String filename)
        {
            IList<String> labels = new List<String>();

            // Flag to indicate that we are parsing sections.
            bool parsingSections = false;

            // Key(s) for the current text entry.
            String label = null;
            String currentText = null;

            // Regex for parsing the labels.
            Regex labelRegex = new Regex(@"^\[(?'label'\w+)(:(?'section'\w+))?(!(?'platform'\w+))?\]");

            using (StreamReader reader = new StreamReader(filename))
            {
                String line;
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.StartsWith("{") || String.IsNullOrWhiteSpace(line))
                    {
                        continue;
                    }
                    else if (line == "start")
                    {
                        parsingSections = true;
                    }
                    else if (parsingSections)
                    {
                        if (line == "end")
                        {
                            parsingSections = false;
                        }
                    }
                    else if (line.StartsWith("["))
                    {
                        // Finish off the previous entry.
                        if (label != null && !String.IsNullOrEmpty(currentText))
                        {
                            labels.Add(label);
                        }

                        // Parse the label, extracting the optional section/platform.
                        Match firstMatch = labelRegex.Match(line);
                        if (firstMatch.Success)
                        {
                            Group labelGroup = firstMatch.Groups["label"];
                            label = labelGroup.Value;
                            currentText = "";
                        }
                        else
                        {
                            label = null;
                        }
                    }
                    else if (label != null)
                    {
                        currentText += line;
                    }
                }
            }

            // See if there is a left over label to finish off.
            if (label != null && !String.IsNullOrEmpty(currentText))
            {
                labels.Add(label);
            }

            return labels;
        }
    }
}
