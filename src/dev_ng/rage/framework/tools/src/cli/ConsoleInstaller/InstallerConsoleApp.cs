﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using RSG.Editor;
using RSG.Editor.Controls;
using System.Threading;
using RSG.Base.Configuration;
using RSG.Installer;
using RSG.Base.Configuration.Installer;
using RSG.Base.OS;

namespace ConsoleInstaller
{
    /// <summary>
    /// 
    /// </summary>
    public class InstallerConsoleApp : RsConsoleApplication
    {
        #region Program Entry Point
        /// <summary>
        /// Program entry point.
        /// </summary>
        /// <param name="args"></param>
        [STAThread]
        static void Main(string[] args)
        {
            InstallerConsoleApp app = new InstallerConsoleApp();
            app.Run();
        }
        #endregion // Program Entry Point
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public InstallerConsoleApp()
            : base()
        {
            RegisterCommandlineArg("default_branch", LongOption.ArgType.Required, "Optional default branch to change the installation to.");
        }
        #endregion // Constructor(s)

        #region Overrides
        /// <summary>
        /// 
        /// </summary>
        protected override int ConsoleMain()
        {
            // Check for running explorer windows.
            bool cancel = false;
            IList<IntPtr> explorerWindows = RSG.Interop.Microsoft.WindowsExplorer.GetAllTopLevelWindows();
            if (explorerWindows.Any() && !this.CommandOptions.NoPopups)
            {
                if (MessageBoxResult.No == RsMessageBox.Show(Installer.Resources.StringTable.InstallerSummary_RunningProcesses,
                    Installer.Resources.StringTable.InstallerWindow_Title, MessageBoxButton.YesNo, MessageBoxImage.Question, MessageBoxResult.No))
                {
                    cancel = true;
                }
            }

            if (cancel)
            {
                return 2;
            }

            IWritableConfig config = ConfigFactory.CreateWritableConfig();
            IInstallerConfig installConfig = ConfigFactory.CreateInstallerConfig(config.Project);
            ToolsInstaller installer = new ToolsInstaller(config);

            // Determine the default branch we should use.
            String defaultBranch = config.Project.DefaultBranchName;
            if (CommandLineOptions.HasOption("default_branch"))
            {
                String commandlineBranch = (String)CommandLineOptions["default_branch"];
                if (config.Project.Branches.ContainsKey(commandlineBranch))
                {
                    defaultBranch = commandlineBranch;
                }
            }

            // Determine which dcc toolsets we should install.
            DCCToolSet maxToolSet = DCCToolSet.DoNotInstall;
            DCCToolSet mbToolSet = DCCToolSet.DoNotInstall;
            DCCToolSet mudboxToolSet = DCCToolSet.DoNotInstall;
            bool mbInstallPython = false;

            try
            {
                maxToolSet = installer.GetInstalledAutodesk3dsmaxToolSet(Log, installConfig);
            }
            catch (Exception ex)
            {
                Log.Error("Error reading Autodesk 3dsmax toolset setup.");
            }
            
            try
            {
                mbToolSet = installer.GetInstalledAutodeskMotionBuilderToolSet(Log, installConfig);
                mbInstallPython = installer.GetInstalledAutodeskMotionBuilderScripts(Log, installConfig);
            }
            catch (Exception ex)
            {
                Log.Error("Error reading Autodesk MotionBuilder toolset setup.");
            }
            
            try
            {
                mudboxToolSet = installer.GetInstalledAutodeskMudboxToolSet(Log, installConfig);
            }
            catch (Exception ex)
            {
                Log.Error("Error reading Autodesk Mudbox toolset setup.");
            }

            bool success;
            // Kick off the install.
            try
            { 
            success = installer.Install(Log, defaultBranch, maxToolSet, mbToolSet, mudboxToolSet, mbInstallPython);
            }
            catch (Exception ex)
            {
                Log.ToolException(ex, "Exception performing the installation");

                success = false;
            }

            return (success ? 0 : 1);
        }
        #endregion // Overrides
    }
}
