#
# RSG.Pipeline.Automation.Database.ClientConsole.makefile
#
 
Project RSG.Pipeline.Automation.Database.ClientConsole

BuildTemplate $(toolsroot)/etc/projgen/RSG.Pipeline.Executable.build
 
ConfigurationType exe
 
FrameworkVersion 4.5
 
OutputPath $(toolsroot)\ironlib\lib\
 
Files {
	Program.cs
	Directory Properties {
			AssemblyInfo.cs
	}
	rockstar.ico
}
 
ProjectReferences {
	..\..\..\..\..\..\..\..\..\3rdParty\dev\cli\P4.Net\src\P4API\P4API.csproj
	..\..\..\..\..\..\..\..\..\3rdparty\dev\cli\P4.Net\src\p4dn\p4dn.vcxproj
	..\..\..\..\..\base\tools\libs\RSG.Base.Configuration\RSG.Base.Configuration.csproj
	..\..\..\..\..\base\tools\libs\RSG.Base\RSG.Base.csproj
	..\..\..\..\..\base\tools\libs\RSG.SourceControl.Perforce\RSG.SourceControl.Perforce.csproj
	..\..\Libs\RSG.Pipeline.Automation.Database.Common\RSG.Pipeline.Automation.Database.Common.csproj
	..\..\Libs\RSG.Pipeline.Automation.Database.Consumers\RSG.Pipeline.Automation.Database.Consumers.csproj
	..\..\Libs\RSG.Pipeline.Automation.Database.Domain\RSG.Pipeline.Automation.Database.Domain.csproj
	..\..\Libs\RSG.Pipeline.Content\RSG.Pipeline.Content.csproj
	..\..\Libs\RSG.Pipeline.Core\RSG.Pipeline.Core.csproj
	..\..\Libs\RSG.Services.Common\RSG.Services.Common.csproj
}
References {
	System
	System.ComponentModel.Composition
	System.Core
	System.ServiceModel
	System.Xml.Linq
	System.Data.DataSetExtensions
	Microsoft.CSharp
	System.Data
	System.Xml
}
