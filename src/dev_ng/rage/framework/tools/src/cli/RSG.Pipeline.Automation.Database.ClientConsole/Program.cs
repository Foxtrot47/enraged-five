﻿using RSG.Base.Configuration;
using RSG.Base.Configuration.Services;
using RSG.Pipeline.Core;
using RSG.Pipeline.Automation.Database.Common;
using RSG.Pipeline.Automation.Database.Consumers;
using RSG.SourceControl.Perforce;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using RSG.SourceControl.Perforce.Extensions;
using RSG.Pipeline.Automation.Database.Domain.Entities.Jobs;
using RSG.Pipeline.Automation.Database.Domain.Entities;
using RSG.Pipeline.Automation.Database.Domain.Entities.Stats;

namespace RSG.Pipeline.Automation.Database.ClientConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            IJobConfig config = new JobConfig();
            IServer serverConfig = config.Servers["local"];

            Thread.Sleep(1000 * 10);
       

            CommandOptions Options = new CommandOptions();
            using (P4 p4 = Options.Project.SCMConnect())
            {

                JobDatabaseConsumer client = new JobDatabaseConsumer(serverConfig);

               /* IEnumerable<P4API.P4RecordSet> recordSets = p4.ChangesChunked(new List<string>() { "X:\\rdr3\\tools\\etc\\..." }, 10);

                if (recordSets.Count() == 0)
                    return;

                IEnumerable<Changelist> changelists = Changelist.CreateFromChangesRecords(p4, recordSets);

                foreach (Changelist cl in changelists)
                {
                    AssetBuilderJob job = new AssetBuilderJob(cl);
                    job.Trigger.TriggerId = Guid.NewGuid();
                    client.CreateJob(job);
                }
                */
                //IList<Job> jobs = client.GetAllJobs();
                //jobs.Count();

                /*
                 * JobModel job = client.GetJob("002e868d-da62-40b6-a2fb-bf98f9f73425");
                client.UpdateJob(job);
                */
                /*IList<JobModel> jobs = client.GetAllJobsByDateAndType(typeof(JobModel), DateTime.UtcNow.AddHours(-5), DateTime.UtcNow, "rsgediscr2", 100);
                jobs.Count();

                IEnumerable<Change> changes = client.GetAllChangesForJob(jobs.First().ID);
                changes.Count();*/

                ServerSummary summary = client.GetJobStats("rsgediabld5", typeof(AssetBuilderJobModel));
                summary.ToString();

            }


        }
    }
}
