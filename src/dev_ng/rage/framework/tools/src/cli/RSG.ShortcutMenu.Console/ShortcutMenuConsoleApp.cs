﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.OS;
using RSG.Editor.Controls;
using RSG.ShortcutMenu.Clients;

namespace RSG.ShortcutMenu.Console
{
    /// <summary>
    /// 
    /// </summary>
    public class ShortcutMenuConsoleApp : RsInteractiveConsoleApplication
    {
        #region Program Entry Point
        /// <summary>
        /// Program entry point.
        /// </summary>
        /// <param name="args"></param>
        [STAThread]
        static void Main(string[] args)
        {
            ShortcutMenuConsoleApp app = new ShortcutMenuConsoleApp();
            app.Run();
        }
        #endregion // Program Entry Point

        #region Fields
        /// <summary>
        /// Host to connect to when sending queries.
        /// </summary>
        private String _hostName = "localhost";
        #endregion // Fields

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ShortcutMenuConsoleApp()
            : base()
        {
            // Register the commands we wish to support.
            RegisterConsoleCommand("invoke", InvokeItemCommandHandler, "<GUID> Invokes the specified shortcut menu item.");
            RegisterConsoleCommand("connect", ConnectCommandHandler, "<IP> Connects the console to the specified ip.");
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// Gets a semi-colon separate list of the application authors email addresses. These
        /// are the addresses which are used by the unhandled exception dialog to determine
        /// where the mail should be sent by default.
        /// e.g.
        /// *tools@rockstarnorth.com
        /// michael.taschler@rockstarnorth.com;dave.evans@rockstarnorth.com
        /// </summary>
        public override string AuthorEmailAddress
        {
            get { return "RDR3Tools@rockstarsandiego.com"; }
        }

        /// <summary>
        /// Text to display in the console's caret.
        /// </summary>
        protected override string CaretText
        {
            get { return String.Format("{0}@{1}", Environment.UserName, _hostName); }
        }
        #endregion // Properties

        #region Console Command Handlers
        /// <summary>
        /// Command which invokes a particular shortcut item.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        private void InvokeItemCommandHandler(String command, String[] arguments)
        {
            if (arguments.Length != 1)
            {
                Log.ErrorCtx(CommandLogCtx, "Invalid number of arguments ({0}); expecting 1: <GUID>.",
                    arguments.Length);
                HelpCommandHandler(command, arguments);
                return;
            }

            // Try and convert the first argument to a guid.
            Guid shortcutMenuItemGuid;
            if (!Guid.TryParse(arguments[0], out shortcutMenuItemGuid))
            {
                Log.ErrorCtx(CommandLogCtx, "First argument must be a valid guid.");
                HelpCommandHandler(command, arguments);
                return;
            }
            
            // Establish a connection to the service.
            EndpointAddress address = new EndpointAddress(String.Format("net.tcp://{0}:7020/ShortcutMenuService.svc", _hostName));
            NetTcpBinding binding = new NetTcpBinding();

            ShortcutMenuClient client = null;
            try
            {
                client = new ShortcutMenuClient(binding, address);
                client.Open();
                bool result = client.InvokeShortcutItem(shortcutMenuItemGuid);

                if (result)
                {
                    Log.MessageCtx(CommandLogCtx, "Item successfully invoked.");
                }
                else
                {
                    Log.ErrorCtx(CommandLogCtx, "Item with guid '{0}' doesn't exist.", shortcutMenuItemGuid);
                }
            }
            catch (Exception ex)
            {
                Log.ToolExceptionCtx(CommandLogCtx, ex, "Error connecting to shortcut menu service.");
            }
            finally
            {
                if (client != null)
                {
                    client.Close();
                }
            }
        }
        
        /// <summary>
        /// Used to change the host to connect to.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        private void ConnectCommandHandler(String command, String[] arguments)
        {
            if (arguments.Length != 1)
            {
                Log.ErrorCtx(CommandLogCtx, "Invalid number of arguments ({0}); expecting 1: <IP>.",
                    arguments.Length);
                HelpCommandHandler(command, arguments);
                return;
            }

            //TODO: Support machine names.
            IPAddress address;
            if (!IPAddress.TryParse(arguments[0], out address) && arguments[0].ToLower() != "localhost")
            {
                Log.ErrorCtx(CommandLogCtx, "First argument must be a valid ip address.");
                HelpCommandHandler(command, arguments);
                return;
            }

            _hostName = arguments[0];
        }
        #endregion // Console Command Handlers
    } // ShortcutMenuConsoleApp
}
