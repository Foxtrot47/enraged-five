﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.SourceControl.Perforce;
using P4API;
using System.Text.RegularExpressions;

namespace RSG.Pipeline.ProjectRebuildHelper
{
    static class Program
    {
        #region Constants
        private static readonly String LOG_CTX = "Project Rebuild Helper";

        private static readonly String OPTION_CHANGELIST = "changelist";
        private static readonly String OPTION_INTEGRATING_CHANGELIST = "integratingChangelist";
        private static readonly String BATCH_FILENAME = "rebuild_vcproj.bat";

        private static readonly String PROJECT_GENERATOR_DIR = "projectgenerator";
        private static readonly String PROJECT_GENERATOR_EXE = "projectgenerator.exe";

        #endregion

        #region Properties
        private static IUniversalLog Log = null;
        #endregion

        #region Controller Methods
        static int Main(String[] args)
        {
            int returnCode = 0;

            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            Log = LogFactory.ApplicationLog;

            Log.MessageCtx(LOG_CTX, "Working directory {0}", Directory.GetCurrentDirectory());

            Log.MessageCtx(LOG_CTX, "Looking for modified project files...");

            LongOption[] longOpts = new LongOption[]
                        {
                            new LongOption(OPTION_CHANGELIST, LongOption.ArgType.Required, string.Empty),
                            new LongOption(OPTION_INTEGRATING_CHANGELIST, LongOption.ArgType.Required, string.Empty)
                        };
            CommandOptions options = new CommandOptions(args, longOpts);

            uint changelist = 0;
            if (!UInt32.TryParse(options[OPTION_CHANGELIST].ToString(), out changelist))
            {
                Log.ErrorCtx(LOG_CTX, "Couldn't parse changelist from arguments");
                return -1;
            }

            uint integratingChangelist = 0;
            if (!UInt32.TryParse(options[OPTION_INTEGRATING_CHANGELIST].ToString(), out integratingChangelist))
            {
                Log.ErrorCtx(LOG_CTX, "Couldn't parse integratingChangelist from arguments");
                return -1;
            }

            using (P4 p4 = options.Config.Project.SCMConnect())
            {
                // First, get a list of all the files that have been integrated
                P4Record describe = p4.Run("describe", changelist.ToString())[0];
                List<String> fileList = new List<String>();

                if (describe.ArrayFields.ContainsKey("depotFile"))
                {
                    int depotFiles = describe.ArrayFields["depotFile"].Length;

                    for (int i = 0; i < depotFiles; i++)
                    {
                        fileList.Add(describe.ArrayFields["depotFile"][i] as String);
                    }
                }
                FileMapping[] files = FileMapping.Create(p4, fileList.ToArray());

                // Now, check to see if any of those files are vcproj files
                List<String> vcprojPaths = new List<String>();
                foreach (FileMapping fileMap in files)
                {
                    String localPath = fileMap.LocalFilename.ToLower();
                    String extension = Path.GetExtension(localPath);

                    if (extension == ".vcproj" || extension == ".vcxproj")
                    {
                        // The project has been changed, so it must be rebuilt
                        if (!vcprojPaths.Contains(localPath))
                        {
                            vcprojPaths.Add(localPath);
                        }
                    }
                }

                if (!vcprojPaths.Any())
                {
                    // No projects have been changed in the integration, so we don't need to do anything
                    Log.MessageCtx(LOG_CTX, "No vcproj files have been changed. Nothing needs to be done.");
                    return 0;
                }

                // For all the vcproj files that have been edited, get the batches in the same folder that will rebuild them
                Log.MessageCtx(LOG_CTX, "Changed vcproj files found. Running project rebuild.");
                ICollection<String> batchBuilderPaths = new List<String>();
                ICollection<String> makefileFilenamesForProjectGeneration = new List<String>();
             
                foreach (String path in vcprojPaths)
                {
                    // Two ways of building the project 
                    //  1. by passing the project generator the makefile ( this is preferred but you must have the template specified in the makefile )
                    //  2. by calling the batch file setup in this folder. ( not neccessarily the best )
                    String directoryToSearchForMakefile = Path.GetDirectoryName(path);
                    String makefileFilenameForProjectGeneration = null;

                    if (Directory.Exists(directoryToSearchForMakefile))
                    {
                        ICollection<String> patterns = new List<String>() { "*.txt", "*.makefile" };
                        Regex regex = new Regex(@"BuildTemplate\s.+", RegexOptions.IgnoreCase);                        
                        foreach (String pattern in patterns)
                        {
                            Log.MessageCtx(LOG_CTX, "Checking pattern {0}", pattern);
                            IEnumerable<String> makefileFilenames = Directory.GetFiles(directoryToSearchForMakefile, pattern, SearchOption.TopDirectoryOnly).ToList();
                            Log.MessageCtx(LOG_CTX, "makefileFilenames : {0}", String.Join(",",makefileFilenames));
                            // read the makefile and only include it if if contains a line matching the regex to identify it as the correct makefile.( validate )
                            foreach (String makefileFilename in makefileFilenames)
                            {
                                Log.MessageCtx(LOG_CTX, "makefileFilename : {0}", makefileFilename);
                                using (StreamReader strReader = new StreamReader(makefileFilename))
                                {
                                    String line;
                                    while ((line = strReader.ReadLine()) != null)
                                    {
                                        Log.MessageCtx(LOG_CTX, "line : {0}", line);
                                        if (regex.IsMatch(line))
                                        {
                                            makefileFilenameForProjectGeneration = makefileFilename;
                                            Log.MessageCtx(LOG_CTX, "Found makefile {0} for project {1}", makefileFilename, path);
                                            break;
                                        }
                                    }
                                }

                                // there can only be one - we choose the first one.
                                if (makefileFilenameForProjectGeneration != null)
                                    break;
                            }
                            // there can only be one - we choose the first one.
                            if (makefileFilenameForProjectGeneration != null)
                                break;
                        }
                    }

                    if (makefileFilenameForProjectGeneration != null)
                    {
                        Log.MessageCtx(LOG_CTX, "Suitable for direct project generation", makefileFilenameForProjectGeneration);
                        makefileFilenamesForProjectGeneration.Add(makefileFilenameForProjectGeneration);
                    }
                    else
                    {
                        Log.MessageCtx(LOG_CTX, "Was not suitable for direct project generation");
                        String batchPath = Path.Combine(Path.GetDirectoryName(path), BATCH_FILENAME);
                        if (File.Exists(batchPath))
                        {
                            if (!batchBuilderPaths.Contains(batchPath))
                            {
                                batchBuilderPaths.Add(batchPath);
                            }
                        }
                        else
                        {
                            Log.MessageCtx(LOG_CTX, "No rebuild batch found at this location: {0}", batchPath);
                        }
                    }
                }

                Log.MessageCtx(LOG_CTX, "there are {0} makefiles to generate", makefileFilenamesForProjectGeneration.Count());
                foreach (String makefileFilenameForProjectGeneration in makefileFilenamesForProjectGeneration)
                {
                    Log.MessageCtx(LOG_CTX, "makefileFilenameForProjectGeneration {0}", makefileFilenameForProjectGeneration);
                    
                    Environment.SetEnvironmentVariable("changelist_num", changelist.ToString());

                    SyncProjectGeneratorInSitueFiles(integratingChangelist, p4, makefileFilenameForProjectGeneration);
                    SyncProjectGeneratorBinaries(options, p4);
                    SyncProjectGeneratorConfigFiles(args, longOpts, integratingChangelist, p4, makefileFilenameForProjectGeneration);                
                    
                    Process process = new Process();
                    process.StartInfo.FileName = Path.Combine(options.Config.ToolsBin, PROJECT_GENERATOR_DIR, PROJECT_GENERATOR_EXE);
                    process.StartInfo.WorkingDirectory = Path.GetDirectoryName(makefileFilenameForProjectGeneration);
                    process.StartInfo.Arguments = String.Format("--changelist_num {0} {1}", changelist, makefileFilenameForProjectGeneration); 
                    process.StartInfo.UseShellExecute = false;

                    Log.MessageCtx(LOG_CTX, "Calling project generator : {0} {1} in working dir {2} : the working dir will be used to decide upon the config used in project generation.", process.StartInfo.FileName, process.StartInfo.Arguments, process.StartInfo.WorkingDirectory);
                    process.Start();
                    process.WaitForExit();
                    if (process.ExitCode != 0)
                    {
                        Log.ErrorCtx(LOG_CTX, "Project Generator ret code : {0}", process.ExitCode);
                        return process.ExitCode;
                    }
                    else
                    {
                        Log.MessageCtx(LOG_CTX, "Project Generator ret code : {0}", process.ExitCode);
                    }
                }

                Log.MessageCtx(LOG_CTX, "there are {0} batchBuilderPaths to generate", batchBuilderPaths.Count());
                foreach (String path in batchBuilderPaths)
                {
                    Environment.SetEnvironmentVariable("changelist_num", changelist.ToString());

                    Process process = new Process();
                    process.StartInfo.FileName = path;
                    process.StartInfo.WorkingDirectory = Path.GetDirectoryName(path);
                    process.StartInfo.UseShellExecute = false;
                    process.Start();
                    process.WaitForExit(300000);

                    if (!process.HasExited)
                    {
                        process.Kill();
                    }

                    //If it failed for some reason, try again with the VS2010 argument
                    if (process.HasExited && process.ExitCode != 0)
                    {
                        process.StartInfo.Arguments = "VS2010";
                        process.Start();
                        process.WaitForExit();
                        if (process.ExitCode != 0)
                        {
                            return process.ExitCode;
                        }
                    }
                    else if (!process.HasExited)
                    {
                        return -1;
                    }
                }
            }

            LogFactory.ApplicationShutdown();

            return returnCode;
        }



        private static void SyncProjectGeneratorConfigFiles(String[] args, LongOption[] longOpts, uint changelist, P4 p4, String makefileFilenameForProjectGeneration)
        {
            // Get the tools config of the path we are generating in
            String curDir = Directory.GetCurrentDirectory();
            try
            {
                String dirForGeneration = Path.GetDirectoryName(makefileFilenameForProjectGeneration);
                Directory.SetCurrentDirectory(dirForGeneration);
                CommandOptions optionsDest = new CommandOptions(args, longOpts);

                String projgenConfig = Path.Combine(optionsDest.Config.ToolsConfig, "projgen");
                String pathToSync = String.Format(@"{0}/...@{1}", Path.GetDirectoryName(projgenConfig).TrimEnd(new char[] { '/', '\\' }), changelist);
                Log.MessageCtx(LOG_CTX, "\tSyncing to project generation files for project file. {0} as of CL {1}", pathToSync, changelist);
                P4API.P4RecordSet recordSet = p4.Run("sync", pathToSync);
                p4.ParseAndLogRecordSetForDepotFilename("sync", recordSet);
                p4.LogP4RecordSetMessages(recordSet);
            }
            finally
            {
                Directory.SetCurrentDirectory(curDir);
            }
        }

        private static void SyncProjectGeneratorBinaries(CommandOptions options, P4 p4)
        {
            try
            {
                // We should also ensure we are on the *latest* binaries of the project generator.
                String pg3Dir = Path.Combine(options.Config.ToolsBin, PROJECT_GENERATOR_DIR).TrimEnd(new char[] { '/', '\\' });
                String pathToSync = String.Format(@"{0}/...", pg3Dir);
                Log.MessageCtx(LOG_CTX, "\tSyncing to project generator binaries : {0}", pathToSync);
                P4API.P4RecordSet recordSet = p4.Run("sync", pathToSync);
                p4.ParseAndLogRecordSetForDepotFilename("sync", recordSet);
                p4.LogP4RecordSetMessages(recordSet);
            }
            catch (Exception ex)
            {
                Log.ExceptionCtx(LOG_CTX, ex, "issus syncing project generation binaries");
            }
        }

        private static void SyncProjectGeneratorInSitueFiles(uint changelist, P4 p4, String makefileFilenameForProjectGeneration)
        {
            try
            {
                // Because we now use a virtual integrate we have to ensure we have the project generation files are synced.
                String pathToSync = String.Format(@"{0}/...@{1}", Path.GetDirectoryName(makefileFilenameForProjectGeneration).TrimEnd(new char[] { '/', '\\' }), changelist);
                Log.MessageCtx(LOG_CTX, "\tSyncing to folder of project file {0} as of CL {1}", pathToSync, changelist);
                P4API.P4RecordSet recordSet = p4.Run("sync", pathToSync);
                p4.ParseAndLogRecordSetForDepotFilename("sync", recordSet);
                p4.LogP4RecordSetMessages(recordSet);
            }
            catch (Exception ex)
            {
                Log.ExceptionCtx(LOG_CTX, ex, "issus syncing folder for project generation");
            }
        }
        #endregion
    }
}
