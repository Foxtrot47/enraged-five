﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ProjectGenerator
{
    /// <summary>
    /// Rules class that can run on a solution
    /// - The rule uses a regex to match *solution configs* and fires in a value into the SolutionProjectSettings for a solution project
    /// </summary>
    public class SolutionRules
    {
        #region Public Properties
        /// <summary>
        /// a number of rexes that if matching the solution config will be able to associate a ProjectConfig with that solution project
        /// </summary>
        public List<KeyValuePair<string, Regex>> ProjectConfig;
        /// <summary>
        /// a number of rexes that if matching the solution config will be able to associate a 'build' flag with that solution project
        /// </summary>
        public List<KeyValuePair<bool,Regex>> Build;
        /// <summary>
        /// a number of rexes that if matching the solution config will be able to associate a 'deploy' flag with that solution project
        /// </summary>
        public List<KeyValuePair<bool, Regex>> Deploy;
        #endregion // Public Properties

        #region Constructors
        /// <summary>
        /// Default Constructor
        /// </summary>
        public SolutionRules()
        {
            ProjectConfig = new List<KeyValuePair<string, Regex>>();
            Build = new List<KeyValuePair<bool,Regex>>();
            Deploy = new List<KeyValuePair<bool,Regex>>();
        }
        #endregion // Constructors
    } // public class SolutionRules
} // namespace ProjectGenerator
