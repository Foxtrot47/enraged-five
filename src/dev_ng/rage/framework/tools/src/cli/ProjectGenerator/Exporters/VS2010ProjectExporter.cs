﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Reflection;
using System.Collections;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using RSG.Base.Configuration;
// DW: TODO : Would be nice to utilise any msbuild filewriting functionality if it exists.
//using Microsoft.Build.Framework;
//using Microsoft.Build.BuildEngine;


namespace ProjectGenerator.Exporters
{
    /// <summary>
    /// Exporter for Visual Studio 2010 Project Files
    /// </summary>
    public sealed class VS2010ProjectExporter : 
                        ProjExporter,
                        Independent.IProjectExporter
    {
        #region Public Constants
        // Visual Studio version
        public static readonly string VS_VERSION = "2010";
        #endregion // Public Constants

        #region Constuctor(s)
        /// <summary>
        /// Static constructor
        /// </summary>
        static VS2010ProjectExporter()
        {
        }

        /// <summary>
        /// Parameterised contructor
        /// </summary>
        /// <param name="mangle"></param>
        public VS2010ProjectExporter(IUniversalLog log, CommandOptions options, string mangle, bool unity)
            : base(log, options, mangle, unity)
        {
        }
        #endregion // Constuctor(s)

        #region Public Properties
        /// <summary>
        /// Return the exporter type
        /// (Independent.IProjectExporter)
        /// </summary>
        /// <returns>The Exporter Enum</returns>
        public override Exporter ExporterType
        {
            get { return Unity ? Exporter.VS2010_UNITY : Exporter.VS2010; }
        }
        #endregion // Public Properties

        #region Protected Methods
        /// <summary>
        /// Overrides the abstract method in its superclass
        /// </summary>
        protected override string VsVersion
        {
            get { return VS_VERSION; }
        }

        protected override void CreateGenerateManifest(XmlDocument doc, XmlElement element)
        {
            if (IsExecutableProject())
            {
                AppendElementWithTextNode(doc, element, "GenerateManifest", "false");
            }
        }

        protected override void SetFileLtcg(XmlDocument doc, Independent.ProjectFile file, XmlElement clCompileElement)
        {
            if (!file.SuitableForLtcg)
            {
                XmlElement elem = AppendElementWithTextNode(doc, clCompileElement, "AdditionalOptions", "-/GL %(AdditionalOptions)");
            }
        }

        #endregion // Protected Methods

        #region Private Methods
        private bool IsExecutableProject()
        {
            return (Project.Targets.FirstOrDefault() != null &&
                    Project.Targets.FirstOrDefault().ProjectType.Standard == Independent.ProjectType.StandardValues.Exe);
        }
        #endregion // Private Methods
    } // class VS2010ProjectExporter
} // namespace ProjectGenerator.Exporters
    