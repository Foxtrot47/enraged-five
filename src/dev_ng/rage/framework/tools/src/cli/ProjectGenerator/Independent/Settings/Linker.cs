﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;


namespace ProjectGenerator.Independent
{
    /// <summary>
    /// Linker settings
    /// </summary>
    [Serializable]
    public class Linker : SettingExtender
    {
        #region Properties
        public bool GenerateDebugInfo { get; set; }
        public bool GenerateMapFile { get; set; }
        public string ProgramDatabaseFilename { get; set; }
        public string MapFilename { get; set; }
        public string OutputFilename { get; set; }
        public List<string> Libraries { get; set; }
        public List<string> AdditionalLibraryDirectories { get; set; }        
        public List<string> AdditionalDependencies { get; set; }
        public List<string> AdditionalOptions { get; set; }
        public List<string> IgnoreLibraries { get; set; }
        public bool RemoveUnrefed { get; set; }
        public bool LTCG { get; set; }
        public bool LinkIncremental { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Linker settings
        /// </summary>
        public Linker()
        {
            AdditionalLibraryDirectories = new List<string>();
            AdditionalDependencies = new List<string>();
            Libraries = new List<string>();
            AdditionalOptions = new List<string>();
            IgnoreLibraries = new List<string>();
            OutputFilename = String.Empty;
            MapFilename = String.Empty;
            GenerateMapFile = false;
            GenerateDebugInfo = false;
            RemoveUnrefed = false;
            LTCG = false;
            LinkIncremental = false;
        }
        #endregion // Constructor(s)
    } // class SettingsLinker
} // namespace ProjectGenerator.Independent
