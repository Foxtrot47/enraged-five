﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace ProjectGenerator
{
    /// <summary>
    /// A Make condition 
    /// - A single condition that can be evaluated using reflection into the Independent.Target class
    /// </summary>
    public class MakeCondition
    {
        #region Properties
        /// <summary>
        /// The . seperated Independent.Target class member string that expresses the setting to evaluate
        /// </summary>
        public string Setting { get; set; }
        /// <summary>
        /// The enum of the operator to use to evaluate a relationship between Setting and Value
        /// </summary>
        MakeRelationalOperator Operator { get; set; }
        /// <summary>
        /// The value to compare against
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Universal log : no choice but to make this static since the SimpleObjectCloner would want to clone this. A difficult decision.
        /// </summary>
        private static IUniversalLog Log { get; set; }
        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public MakeCondition()
        {
        }
        /// <summary>
        /// Parameterised constructor
        /// </summary>
        /// <param name="setting"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        public MakeCondition(IUniversalLog log, string setting, MakeRelationalOperator op, string value)
        {
            Setting = setting;
            Operator = op;
            Value = value;
            Log = log;
        }

        /// <summary>
        /// Chained constructor
        /// </summary>
        /// <param name="setting"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        public MakeCondition(IUniversalLog log, string setting, string op, string value)
            : this(log, setting, MakeRelationalOperatorExtensions.Create(log, op), value)
        {
        }
        #endregion // constructor(s)

        #region Public Methods
        /// <summary>
        /// Evaluate the condition for the target
        /// </summary>
        /// <param name="target">the target in which to evaluate the condition</param>
        /// <returns>bool</returns>
        public bool Eval(Independent.Target target)
        {
            object currrentPropertyValue = target.GetProperty(Setting);

            if (currrentPropertyValue != null)
            {
                bool matched = false;
                bool contains = false;
                string curr = String.Empty;

                if (currrentPropertyValue is string)
                {
                    curr = currrentPropertyValue as string;
                    matched = (0 == string.Compare(curr, Value, true));
                }
                else if (currrentPropertyValue is Independent.IEnum)
                {
                    matched = ((Independent.IEnum)currrentPropertyValue).Match(Value);
                    curr = ((Independent.IEnum)currrentPropertyValue).EnumToString();
                }
                else if (currrentPropertyValue is Platform)
                {
                    matched = ((Platform)currrentPropertyValue).Match(Value);
                    curr = ((Platform)currrentPropertyValue).EnumToString();
                }
                else if (currrentPropertyValue is Exporter)
                {
                    matched = ((Exporter)currrentPropertyValue).Match(Value);
                    curr = ((Exporter)currrentPropertyValue).EnumToString();
                }
                else if (currrentPropertyValue is Language)
                {
                    matched = ((Language)currrentPropertyValue).Match(Value);
                    curr = ((Language)currrentPropertyValue).EnumToString();
                }
                else
                {
                    Log.Error("Error: Non supported MakeCondition compared against. {0}", currrentPropertyValue.GetType().ToString());
                }

                // If it wasn't matched but our intetnion is to see if it merely contains OR DOESN'T CONTAIN a string - check that now.
                if (!matched && (Operator == MakeRelationalOperator.Contains || Operator == MakeRelationalOperator.NotContains))
                {
                    contains = curr.ToLower().Contains(Value.ToLower());
                }

                switch (Operator)
                {
                    case MakeRelationalOperator.Equals:
                        return (matched);
                    case MakeRelationalOperator.NotEquals:
                        return (!matched);
                    case MakeRelationalOperator.Contains:                        
                        return (matched||contains);
                    case MakeRelationalOperator.NotContains:
                        return !(matched||contains);
                    default:
                        Log.Error("Error: invalid MakeEqualityOperator");
                        return false;
                }
            }
            return false;
        }
        #endregion // Public Methods
    } // class MakeCondition
} // namespace ProjectGenerator
