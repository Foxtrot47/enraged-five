﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ProjectGenerator.Independent
{
    /// <summary>
    /// A file in the independent project
    /// </summary>
    [Serializable]
    public class ProjectFile
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public ProjectFile()
            : this(String.Empty)
        {
        }
            
        /// Constructor that sets the fullpath of the file            
        public ProjectFile(string fullPath, string pch = "")
        {
            CustomCommand = new Dictionary<Platform, CustomCommand>();

            ChildNodes = new List<KeyValuePair<String, String>>();
            bool matched = false;
            String filename = fullPath;

            // A file is assumed to be fit for LTCG by default as we only blacklist ones that aren't
            SuitableForLtcg = true;    

            do
            {
                Match match = Regex.Match(filename, "(.*)\\(([^\\)]*)=([^\\)]*)\\)");
                matched = match.Success && match.Groups.Count == 4;
                if (matched)
                {
                    filename = match.Groups[1].Value; // whittle it down
                    String key = match.Groups[2].Value;
                    String value = match.Groups[3].Value;

                    // Blacklist files for no ltcg
                    if (key.Equals("noltcg", StringComparison.OrdinalIgnoreCase))
                    {
                        if (value.Equals("true", StringComparison.OrdinalIgnoreCase))
                        {
                            SuitableForLtcg = false;
                        }
                        // don't pollute childnodes.
                        continue;
                    }

                    ChildNodes.Add(new KeyValuePair<string, string>(key, value));                   
                }
            } while (matched);
            Path = filename;
            Excluded = false;
            Parse = false;
            Custom = false;
            UnityDesignation = null;
            Unity = false;        

            // Handle PCH file overrides.
            if (pch == Makefile.PCH_CREATE)
            {
                ForcedIncludeFiles = String.Empty;
                AdditionalIncludeDirectories = String.Empty;
                PrecompiledHeader = pch;
            }
            else if (pch == Makefile.PCH_USE)
            {
                ForcedIncludeFiles = null;
                AdditionalIncludeDirectories = null;
                PrecompiledHeader = pch;
            }
            else
            {
                ForcedIncludeFiles = null;
                AdditionalIncludeDirectories = null;
                PrecompiledHeader = null;
            }
        }
        #endregion // Constructor(s)

        #region Properties
        /// <summary>
        /// The path ( full ) to the file.
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        ///  The unity designation name.
        /// </summary>
        public string UnityDesignation { get; set; }
        /// <summary>
        ///  The file is a unity file ( and is therefore not a file that is included in a unity file )
        /// </summary>
        public bool Unity { get; set; }
        /// <summary>
        /// Is excluded from the build.
        /// </summary>        
        public bool Excluded { get; set; }
        /// <summary>
        /// Is a parser file.
        /// </summary>
        public bool Parse { get; set; }
        /// <summary>
        /// Has custom build rules
        /// </summary>
        public bool Custom { get; set; }
        /// <summary>
        /// Link time compile generation
        /// </summary>
        public bool SuitableForLtcg { get; set; }
        /// <summary>
        /// Dictionary of custom commands that may vary from platform to platform
        /// </summary>
        public Dictionary<Platform,CustomCommand> CustomCommand { get; set; }
        /// <summary>
        /// Nullable string for force include files ( required for pch cpp )
        /// </summary>
        public string ForcedIncludeFiles { get; set; }
        /// <summary>
        /// Nullable string for additional include directories files ( required for pch cpp )
        /// </summary>
        public string AdditionalIncludeDirectories { get; set; }
        /// <summary>
        /// Nullable string for precompiled header setting ( required for pch cpp )
        /// </summary>
        public string PrecompiledHeader { get; set; }
        /// <summary>
        /// Optional child nodes
        /// </summary>
        public List<KeyValuePair<String, String>> ChildNodes { get; set; } 
        #endregion // Properties
    } // class ProjectFile
} // namespace ProjectGenerator.Independent
