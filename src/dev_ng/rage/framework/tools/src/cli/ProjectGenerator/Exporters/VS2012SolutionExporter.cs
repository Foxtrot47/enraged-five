﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Reflection;
using System.Collections;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using RSG.Base.Configuration;

namespace ProjectGenerator.Exporters
{
    /// <summary>
    /// Exporter for Visual Studio 2012 Solution File
    /// </summary>
    public sealed class VS2012SolutionExporter :
                        VsSlnExporter,
                        Independent.IProjectExporter
    {
        #region Private Constants
        // Visual Studio version
        private static readonly string VS_VERSION = "2012";
        #endregion //Constants

        #region Constuctor(s)
        /// <summary>
        /// Static constructor
        /// </summary>
        static VS2012SolutionExporter()
        {
        }

        /// <summary>
        /// Parameterised contructor
        /// </summary>
        /// <param name="mangle"></param>
        public VS2012SolutionExporter(IUniversalLog log, CommandOptions options, string mangle, bool unity)
            : base(log, options, mangle, unity)
        {
        }
        #endregion // Constuctor(s)

        #region Public Properties
        /// <summary>
        /// Return the exporter type
        /// (Independent.IProjectExporter)
        /// </summary>
        /// <returns>The Exporter Enum</returns>
        public override Exporter ExporterType
        {
            get { return Unity ? Exporter.VS2012_SLN_UNITY : Exporter.VS2012_SLN; }
        }
        #endregion // Public Properties

        #region Protected Methods
        /// <summary>
        /// Overrides the abstract method in its superclass
        /// </summary>
        protected override string VsVersion
        {
            get { return VS_VERSION; }
        }

        protected override void WriteHeader(TextWriter tw)
        {
            tw.WriteLine("Microsoft Visual Studio Solution File, Format Version 12.00");
            tw.WriteLine("# Visual Studio 11");
        }
        #endregion // Protected Methods
    } // class VS2012SolutionExporter
} // namespace ProjectGenerator.Exporters
