﻿using System;

namespace ProjectGenerator
{
    /// <summary>
    /// the attibute that is set on an enum that contains the value of the valid values  
    /// of a setting within the makefile.
    /// </summary>
    public class MakefilePlatformSuffixAttribute : Attribute
    {
        /// <summary>
        /// The string associated with the attribute
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Parameterised Constructor
        /// </summary>
        public MakefilePlatformSuffixAttribute(string text)
        {
            Text = text;
        }
    } // class TextAttribute
} // namespace ProjectGenerator
