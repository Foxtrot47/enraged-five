﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using LitJson;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace ProjectGenerator.Independent
{
    /// <summary>
    /// An independent solution 
    /// <remarks>the container of the 'internal' / 'independent' solution format</remarks>
    /// </summary>
    [Serializable]
    public class Solution
    {
        #region Public Constants
        /// <summary>
        /// this string if found in solution & project names is substituted for the independent Project.Name ( no reflection used )
        /// </summary>
        public static readonly string PROJECT_NAME = "Project.Name";
        #endregion // Public Constants
        #region Private Constants
        // It's not necessary to complicate solution files with guid filenames at the moment.
        // Where possible use a default, until of course any sign of a guid collision, which there hopefully will never be.
        private static readonly string DEFAULT_SLN_GUID = "8BC9CEB8-8B4A-11D0-8D11-00A0C91BC942";
        private static readonly char SEPERATOR  = ':';
        #endregion // Private Constants

        #region Public Properties
        /// <summary>
        /// The name of the solution
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The path to the solution
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// The GUID of the solution
        /// </summary>
        public string Guid { get; set; }
        /// <summary>
        /// Target SOLUTION config names 
        /// </summary>
        public List<string> Configs { get; set; }
        /// <summary>
        /// Target SOLUTION platform names
        /// </summary>
        public List<string> Platforms { get; set; }
        /// <summary>
        /// A collection of Projects in the solution, keyed by project id taken from the slndef file.
        /// </summary>
        public Dictionary<string, SolutionProject> Projects { get; set; }
        #endregion // Public Properties

        #region Private Properties
        /// <summary>
        /// Universal log : no choice but to make this static since the SimpleObjectCloner would want to clone this. A difficult decision.
        /// </summary>
        private static IUniversalLog Log { get; set; }
        #endregion // Private Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public Solution(IUniversalLog log)
        {
            Name = String.Empty;
            Path = String.Empty;
            Guid = DEFAULT_SLN_GUID;
            Configs = new List<string>();
            Platforms = new List<string>();
            Projects = new Dictionary<string, SolutionProject>();
            Log = log;
        }
        #endregion // Constructor(s)

        #region Static Public Methods
        /// <summary>
        /// Deserialise the JSON filename to an independent Solution
        /// <remarks>this allows for offline editing of the independent Solution and allowing it to reenter the conversion pipeline</remarks>
        /// <remarks>purely a debugging tool.</remarks>
        /// <remarks>I recommend using oxygen XML editor.</remarks>
        /// </summary>
        /// <param name="filename">the filename to interpret</param>
        /// <returns>the independent Solution</returns>
        static public Independent.Solution DeserialiseJSON(string filename)
        {
            string jsonString = File.ReadAllText(filename);
            return JsonMapper.ToObject<Independent.Solution>(jsonString);
        }
        #endregion // Static Public Methods

        #region Public Methods

        /// <summary>
        /// Is this a valid solution
        /// - solutions that don't have a name are not valid
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            return (Name != null && Name != String.Empty);
        }

        /// <summary>
        /// Expands variables for easier description of solutions
        /// </summary>
        /// <param name="project"></param>
        public void ExpandVariables(Project project)
        {
            if (Name == PROJECT_NAME)
            {
                Name = project.Name;
            }

            Name = Environment.ExpandEnvironmentVariables(Name);
        }

        /// <summary>
        /// Process 'rules' on the solution
        /// </summary>
        /// <param name="rules">The list of rules to process</param>
        public void ProcessRules(SolutionRules rules)
        {
            EstablishDependencyReferencesFromNames();

            // Get GUIDs for solution?
            // Curently this is fine being defaulted.

            ReadProjectGuids();

            BuildProjectSettingDictionary();

            ProcessProjectConfigTransformRules(rules);

            ProcessBuildRules(rules);

            ProcessDeployRules(rules);
        }

        /// <summary>
        /// Process exporter 'rules' on the solution
        /// </summary>
        /// <param name="rules"></param>
        /// <param name="exporter"></param>
        public void ProcessExporterRules(SolutionRules rules, Exporter exporter)
        {
            string exporterString = exporter.EnumToString();

            ProcessProjectConfigTransformRules(rules, exporterString);

            ProcessBuildRules(rules, exporterString);

            ProcessDeployRules(rules, exporterString);
        }

        /// <summary>
        /// JSON Serialisation
        /// <remarks>very handy, possible to manipulate, verify objects.</remarks>
        /// </summary>
        /// <param name="filename">The JSON filename serialise out.</param>
        public void SerialiseJSON(string filename)
        {
            string jsonString = JsonMapper.ToJson(this);
            File.WriteAllText(filename, jsonString);
        }
        #endregion // Public Methods

        #region Private Methods
        private void ProcessDeployRules(SolutionRules rules, string exporter = null)
        {
            // Match & replace deploy flags
            foreach (KeyValuePair<bool, Regex> regexKVP in rules.Deploy)
            {
                Regex ruleRegex = regexKVP.Value;
                bool ruleVal = regexKVP.Key;

                foreach (KeyValuePair<string, SolutionProject> projKVP in Projects)
                {
                    string projId = projKVP.Key;
                    SolutionProject slnProj = projKVP.Value;

                    foreach (KeyValuePair<string, SolutionProjectSetting> setting in slnProj.SolutionProjectSettingDict)
                    {
                        string fullProjectString = FullProjectString(exporter, slnProj, setting);

                        Match match = ruleRegex.Match(fullProjectString);
                        if (match.Success)
                        {
                            setting.Value.Deploy = ruleVal;
                        }
                    }
                }
            }
        }

        private void ProcessBuildRules(SolutionRules rules, string exporter = null)
        {
            // Match & replace build flags
            foreach (KeyValuePair<bool, Regex> regexKVP in rules.Build)
            {
                Regex ruleRegex = regexKVP.Value;
                bool ruleVal = regexKVP.Key;

                foreach (KeyValuePair<string, SolutionProject> projKVP in Projects)
                {
                    string projId = projKVP.Key;
                    SolutionProject slnProj = projKVP.Value;

                    foreach (KeyValuePair<string, SolutionProjectSetting> setting in slnProj.SolutionProjectSettingDict)
                    {
                        string fullProjectString = FullProjectString(exporter, slnProj, setting);

                        Match match = ruleRegex.Match(fullProjectString);
                        if (match.Success)
                        {
                            setting.Value.Build = ruleVal;
                        }
                    }
                }
            }
        }

        private void ProcessProjectConfigTransformRules(SolutionRules rules, string exporter = null)
        {
            // Match and replace Project configs.
            foreach (KeyValuePair<string, Regex> regexKVP in rules.ProjectConfig)
            {
                Regex ruleRegex = regexKVP.Value;
                string ruleVal = regexKVP.Key;

                foreach (KeyValuePair<string, SolutionProject> projKVP in Projects)
                {
                    string projId = projKVP.Key;
                    SolutionProject slnProj = projKVP.Value;

                    foreach (KeyValuePair<string, SolutionProjectSetting> setting in slnProj.SolutionProjectSettingDict)
                    {
                        string fullProjectString = FullProjectString(exporter, slnProj, setting); 
                        string destVal = ruleVal;

                        Match match = ruleRegex.Match(fullProjectString);
                        if (match.Success)
                        {
                            int i = 0;
                            foreach (Group g in match.Groups)
                            {
                                string val = g.Value.Trim();
                                destVal = destVal.Replace("(" + i + ")", val);
                                i++;
                            }

                            setting.Value.ProjectConfig = destVal;
                        }
                    }
                }
            }
        }

        private void BuildProjectSettingDictionary()
        {
            // First create all the configurations
            foreach (KeyValuePair<string, SolutionProject> projKVP in Projects)
            {
                foreach (string config in Configs)
                {
                    foreach (string platform in Platforms)
                    {
                        // They don't have default mappings : intentional design consideration. ( except what the constructor implements ) 
                        projKVP.Value.SolutionProjectSettingDict[config + "|" + platform] = new SolutionProjectSetting();
                    }
                }
            }
        }

        private void ReadProjectGuids()
        {
            // Get GUIDs for all projects
            foreach (KeyValuePair<string, SolutionProject> projKVP in Projects)
            {
                string projId = projKVP.Key;
                SolutionProject slnProj = projKVP.Value;

                string guidFilename = System.IO.Path.GetFullPath(Environment.ExpandEnvironmentVariables(slnProj.UnSuffixedPath + Exporters.ExporterBase.GUID_FILETYPE));

                // choose a local guid filename first - this way we can support guid overrides & missing guids.
                string localGuidFilename = System.IO.Path.GetFileName(Environment.ExpandEnvironmentVariables(slnProj.UnSuffixedPath + Exporters.ExporterBase.GUID_FILETYPE));

                if (File.Exists(localGuidFilename))
                {
                    if (File.Exists(guidFilename))
                    {
                        // Log.Warning("A local guid file is chosen as an override, yet it exists elsewhere {0} this is not common practice, but is supported, check this is desired.", guidFilename);
                    }
                    slnProj.Guid = GuidUtils.ReadFile(localGuidFilename);
                }
                else if (File.Exists(guidFilename))
                {                                     
                    slnProj.Guid = GuidUtils.ReadFile(guidFilename);
                }
                else
                {
                    Log.Error("No local or remote Guid file found for solution: {0} : 2 courses of action; 1) create this guid file. 2) create this guid file locally.", guidFilename);
                }
            }
        }

        private void EstablishDependencyReferencesFromNames()
        {
            // Establish references to dependencies
            foreach (KeyValuePair<string, SolutionProject> projKVP in Projects)
            {
                string projId = projKVP.Key;
                SolutionProject slnProj = projKVP.Value;

                foreach (string depName in slnProj.DependencyNames)
                {
                    if (Projects.ContainsKey(depName))
                    {
                        slnProj.Dependencies.Add(Projects[depName]);
                    }
                    else
                    {
                        Log.Error("Missing dependency ({1}) in solution project {0}", projId, depName);
                    }
                }
            }
        }
        #endregion // Private Methods

        #region Private Static Methods
        /// <summary>
        /// Builds a string that represents the form of proj_id[sln cfg|sln platform|exporter] OR proj_id[sln cfg|sln platform]
        /// - as used in a slnrules file.
        /// </summary>
        /// <param name="exporter"></param>
        /// <param name="slnProj"></param>
        /// <param name="setting"></param>
        /// <returns></returns>
        private static string FullProjectString(string exporter, SolutionProject slnProj, KeyValuePair<string, SolutionProjectSetting> setting)
        {
            string settingSubst = setting.Key.Replace('|', SEPERATOR);
            string result;
            if (exporter != null)
            {
                result = slnProj.Name + SEPERATOR + settingSubst + SEPERATOR + exporter;
            }
            else
            {
                result = slnProj.Name + SEPERATOR + settingSubst; 
            }
            return result;
        }
        #endregion // Private Static Methods

    } // class Solution
} // namespace ProjectGenerator.Independent
