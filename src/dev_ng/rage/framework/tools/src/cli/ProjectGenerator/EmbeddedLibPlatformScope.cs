﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGenerator
{
    /// <summary>
    /// Struct for containing EmbeddedLibPlatform keyval pair.
    /// TODO : review...!
    /// </summary>
    public class EmbeddedLibPlatformScope
    {
        public EmbeddedLibPlatformScope()
        {
            EmbeddedLibPlatform = new KeyValuePair<string, string>();
        }

        public KeyValuePair<string, string> EmbeddedLibPlatform { get; set; }
    } // class EmbeddedLibPlatformScope
} // namespace ProjectGenerator
