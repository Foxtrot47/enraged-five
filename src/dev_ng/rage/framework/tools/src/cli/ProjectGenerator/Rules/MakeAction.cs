﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using System.Text.RegularExpressions;

namespace ProjectGenerator
{
    /// <summary>
    /// A Make Action
    /// - an action that runs on the independent project format changing it
    /// - as defined by a .rules file (a makerules file).
    /// </summary>
    public class MakeAction
    {
        #region Properties
        /// <summary>
        /// The setting that the actions 'acts' on.
        /// - a . seperated Independent Target class member name ( uses reflection )
        /// </summary>
        public string Setting { get; set; }
        /// <summary>
        /// The enum of the operation to perform
        /// - eg. Add / Append etc.
        /// </summary>
        public MakeAssignmentOperator Operator { get; set; }
        /// <summary>
        /// The value to use when 'acting' on the setting.
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// Universal log : no choice but to make this static since the SimpleObjectCloner would want to clone this. A difficult decision.
        /// </summary>
        private static IUniversalLog Log { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public MakeAction()
        {
        }

        /// <summary>
        /// Parameterised constructor 
        /// </summary>
        /// <param name="setting"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        public MakeAction(IUniversalLog log, string setting, MakeAssignmentOperator op, string value)
        {
            Setting = setting;
            Operator = op;
            Value = value;
            Log = log;
        }

        /// <summary>
        /// Chained constructor
        /// </summary>
        /// <param name="setting"></param>
        /// <param name="op"></param>
        /// <param name="value"></param>
        public MakeAction(IUniversalLog log, string setting, string op, string value)
            : this(log, setting, MakeAssignmentOperatorExtensions.Create(log, op), value)
        {
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Run the action
        /// </summary>
        /// <param name="target"></param>
        public void Run(Independent.Target target)
        {
            try
            {
                string expandedValue = ExpandValue(target);

                object currentPropertyValue = target.GetProperty(Setting);

                switch (Operator)
                {
                    case MakeAssignmentOperator.Add:
                        if (currentPropertyValue is string)
                        {
                            target.SetPropertyValue(Setting, currentPropertyValue + expandedValue); // DW could be faster, but immutable strings make for complications
                        }
                        else if (currentPropertyValue is List<string>)
                        {
                            (currentPropertyValue as List<string>).Add(expandedValue);
                        }
                        else
                        {
                            Log.Warning("Unhandled Rule {0}", Setting);
                        }
                        break;
                    case MakeAssignmentOperator.Append:
                        if (currentPropertyValue is string)
                        {
                            target.SetPropertyValue(Setting, currentPropertyValue + expandedValue); // DW could be faster, but immutable strings make for complications
                        }
                        else if (currentPropertyValue is List<string>)
                        {
                            if ((currentPropertyValue as List<string>).Count()==0)
                            {
                                Log.Error("Cannot append to an empty list {0} {1}", Setting, expandedValue);
                            }
                            string last = (currentPropertyValue as List<string>).Last();
                            string newValue = last + expandedValue;
                            (currentPropertyValue as List<string>).RemoveAt((currentPropertyValue as List<string>).Count - 1);
                            (currentPropertyValue as List<string>).Add(newValue);
                        }
                        else
                        {
                            Log.Warning("Unhandled Rule {0}", Setting);
                        }
                        break;
                    case MakeAssignmentOperator.Prepend:
                        if (currentPropertyValue is string)
                        {
                            target.SetPropertyValue(Setting, expandedValue + currentPropertyValue); // DW could be faster, but immutable strings make for complications
                        }
                        else if (currentPropertyValue is List<string>)
                        {
                            if ((currentPropertyValue as List<string>).Count() == 0)
                            {
                                Log.Error("Cannot prepend to an empty list {0} {1}", Setting, expandedValue);
                            }
                            string last = (currentPropertyValue as List<string>).Last();
                            string newValue = expandedValue + last;
                            (currentPropertyValue as List<string>).RemoveAt((currentPropertyValue as List<string>).Count - 1);
                            (currentPropertyValue as List<string>).Add(newValue);
                        }
                        else
                        {
                            Log.Warning("Unhandled Rule {0}", Setting);
                        }
                        break;
                    case MakeAssignmentOperator.Set:
                        if (currentPropertyValue is string)
                        {
                            target.SetPropertyValue(Setting, expandedValue); // DW could be faster, but immutable strings make for complications
                        }
                        else if (currentPropertyValue is bool)
                        {
                            target.SetPropertyValue(Setting, bool.Parse(expandedValue));
                        }
                        else if (currentPropertyValue is List<string>)
                        {
                            List<string> newList = new List<string>();
                            newList.Add(expandedValue);
                            target.SetPropertyValue(Setting, newList);
                        }
                        else
                        {
                            Type objectType = currentPropertyValue.GetType();
                            object instance = Activator.CreateInstance(objectType, new object[] { expandedValue, false } );
                            target.SetPropertyValue(Setting, instance);
                        }
                        break;
                    // DW : TODO - handle null - removing the setting
                    default:
                        Log.Error("An invalid setting MakeAssignmentOperator is being used for a makeaction {0}", ToString());
                        break;
                }
            }
            catch (Exception ex)
            {
                Log.ToolException(ex, "*** Make Action Exception ***\n===Msg===\n{1}\n===Stack trace===\n{2}", ex.Message, ex.StackTrace);
            }
        }
        #endregion // Public Methods

        #region Private Methods

        /// <summary>
        /// Substitute [] delimited entries in string for lower case target Property values
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        private string ExpandValue(Independent.Target target)
        {
            string val = Value;
            bool matched = true;
            
            do
            {
                Match match = Makefile.BRACKETS_REGEX.Match(val);
                if (match.Success)
                {
                    string preString = match.Groups[1].Value;
                    string eval = match.Groups[2].Value;
                    string postString = match.Groups[3].Value;

                    object currentPropertyValue = target.GetProperty(eval);

                    if (currentPropertyValue != null && currentPropertyValue is string)
                    {
                        string evaluated = ((string)currentPropertyValue).ToLower();
                        evaluated = evaluated.Replace(" ", "_"); // may one day make this optional.
                        val = preString + evaluated + postString;
                    }
                    else
                    {
                        Log.Error("Make Action {0} did not expand {1}", Value, eval);
                    }
                }
                else
                {
                    matched = false;
                }
            } while(matched);

            return val;
        }
        #endregion // Private Methods
    } // class MakeAction
} // namespace ProjectGenerator
