﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using LitJson;
using System.Text.RegularExpressions;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using ProjectGenerator.Independent;
using RSG.Base.Configuration;

namespace ProjectGenerator
{
    /// <summary>
    /// The project builder original 'makefile' format. 
    /// <remarks>albeit extended.</remarks>
    /// <remarks>Implements the IProjectImporter interface to load this into an Independent.Project.</remarks>
    /// <remarks>handles parsing of the makefile and it's extensions</remarks>
    /// </summary>
    class Makefile : Independent.IProjectImporter
    {
        #region Constants
        // Console tracing options.
        private static readonly bool TRACE_MAKE                     = (ProjectGenerator.TRACE && true);
        private static readonly bool TRACE_MAKEFILENAMES            = (TRACE_MAKE && true);
        private static readonly bool TRACE_MAKELINES                = (TRACE_MAKE && true);
        private static readonly bool TRACE_PREPROCESS_MAKELINES     = (TRACE_MAKE && false);
        private static readonly bool TRACE_SOURCE_FILENAMES         = (TRACE_MAKE && false);
        private static readonly bool TRACE_SCOPESTACK               = (TRACE_MAKE && false);

        // Makefile preprocess file indentation
        private static readonly int PREPROCESS_INDENT               = 2;

        // Tracing indentation
        private static readonly int TRACE_INDENT                    = 2;

        // Issue warnings for missing src files 
        private static readonly bool WARN_IF_SRC_FILES_DONT_EXIST   = false;

        // Write a debug file that preprocesses makefiles.
        private static readonly bool WRITE_PREPROCESSED_FILES       = false;

        // The comment character of the makefile
        public static readonly char MAKEFILE_COMMENT_CHAR           = '#';

        // The filter name for parcodegen created parser hdr files
        private static readonly string PARSER_DIR                   = "[Parser Files]";
        // The parser hdr suffix
        private static readonly string PARSER_HDR                   = "_parser.h";

        // A forceinclude that must come first.
        private static readonly string PRIORITY_FORCEINCLUDE        = @"\forceinclude\";

        // The filenames of which to search for when building mega projects
        private static readonly string MEGA_PROJECT_MAKEFILE        = "makefile.txt";

        // A preprocessed makefile filetype
        private static readonly string PREPROCESSED_FILETYPE        = ".preprocessed";

        // Makefile syntax for all platforms.
        private static readonly string PLATFORM_ALL                 = "All";

        // Recognised file extensions.
        private static readonly string BUILD_FILE_EXTENSION         = "build";
        private static readonly string UNITY_CFG_FILE_EXTENSION     = ".unity";

        // Makefile file format tokens. 
        private static readonly string MAKEFILE_INCLUDE             = "Include";
        private static readonly string MAKEFILE_ROOT_DIRECTORY      = "RootDirectory";
        private static readonly string MAKEFILE_INCLUDE_PATH        = "IncludePath";
        private static readonly string MAKEFILE_FORCE_INCLUDE       = "ForceInclude";
        private static readonly string MAKEFILE_PROJECT             = "Project";
        private static readonly string MAKEFILE_MAKEFILE            = "Makefile";
        private static readonly string MAKEFILE_PROJECTTYPE         = "ProjectType|ConfigurationType";
        private static readonly string MAKEFILE_TITLEID             = "TitleId";
        private static readonly string MAKEFILE_ADDITIONAL_SECTIONS = "AdditionalSections";
        private static readonly string MAKEFILE_FRAMEWORK_VERSION   = "FrameworkVersion";
        private static readonly string MAKEFILE_OUTPUT_PATH         = "OutputPath";
        private static readonly string MAKEFILE_PLATFORM_TOOLSET    = "PlatformToolset";
        private static readonly string MAKEFILE_TEMPLATE            = "Template";
        public static readonly string MAKEFILE_BUILD_TEMPLATE      = "BuildTemplate";

        // Makefile parsing regular expressions.
        public static readonly RegexOptions REGEX_OPTIONS           = RegexOptions.IgnoreCase | RegexOptions.Compiled;

        // MakeAction regex
        public static readonly Regex BRACKETS_REGEX                 = new Regex(@"^\s*([^<^>]*)<([^<^>]*)>(.*)$", REGEX_OPTIONS);

        // Unity REGEXes
        private static readonly Regex UNITY_REGEX_ENABLED           = new Regex(@"^\s*set\s*enabled\s*(.*)$", REGEX_OPTIONS);
        private static readonly Regex UNITY_REGEX_EXCLUDE           = new Regex(@"^\s*set\s*exclude\s*(.*)$", REGEX_OPTIONS);
        private static readonly Regex UNITY_REGEX_COMPILATION_UNIT  = new Regex(@"^\s*(\S*)\s*(\S*)\s*(\S*)\s*(.*)$", REGEX_OPTIONS);

        private static readonly string UNITY_FOLDER_TOKEN           = "FOLDER_TOKEN";
        private static readonly string UNITY_EMPTY_REGEXP           = @"^(((\.*|\w:)\/)*)(FOLDER_TOKEN)\/(.*)(\.)(cpp|c)$";

        public static readonly Regex BUILD_SUITE_REGEX              = new Regex(@"\s*build (.+)\s*", REGEX_OPTIONS);


        // LTcg regex

        private static readonly Regex LTCG_REGEX_DISABLE = new Regex(@"LTCG_DISABLE", REGEX_OPTIONS);

        // DW : TODO : All these regexes should be tolerant of the { for scope starting on a new line.

        private static readonly Regex MAKEFILE_ACTION_REGEX                            = new Regex(@"^\s*Action\s+(.*){\s*$", REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_ACTIONLESS_RULE_REGEX                   = new Regex(@"^\s*\[([^\]]*)\]\s*(\+=|\<\<|\>\>|=)\s*'*([^']*)'*.*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_ASSIGNMENT_RULE_REGEX                   = new Regex(@"^\s*(\+=|\<\<|\>\>|=)\s*'*([^']*)'*.*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_BLANK_LINE                              = new Regex(@"^\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_BUILD_CONFIG_REGEX                      = new Regex(@"^\s*(.*)\s*$",REGEX_OPTIONS);
        public static readonly Regex MAKEFILE_CONDITION_REGEX                          = new Regex(@"^\s*Condition\s+\[([^\]]*)\]\s*{\s*$", REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_EXPORTER_RULES_REGEX                    = new Regex(@"^\s*ExporterRules\s*\[([^\]]*)\]\s*{\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_CONFIG_REGEX                            = new Regex(@"^\s*Config\s+(.*)$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_CUSTOM_FILE_COMMAND_REGEX               = new Regex(@"^\s*Command(.*)\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_CUSTOM_FILE_DEPS_REGEX                  = new Regex(@"^\s*Dependencies(.*)\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_CUSTOM_FILE_DESC_REGEX                  = new Regex(@"^\s*Description(.*)\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_CUSTOM_FILE_OUTPUTS_REGEX               = new Regex(@"^\s*Outputs(.*)\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_CUSTOM_FILENAME_PLATFORM_SCOPE_REGEX    = new Regex(@"^\s*(\S*)\s+{\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_CUSTOM_FILENAME_REGEX                   = new Regex(@"^\s*(.*)\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_CUSTOM_FILENAME_SCOPE_REGEX             = new Regex(@"^\s*(\S*)\s+{\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_CUSTOM_REGEX                            = new Regex(@"^\s*Custom\s+{\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_DEFINE_REGEX                            = new Regex(@"^\s*Define\s+(.*)\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_DEFINE_VAL_REGEX                        = new Regex(@"^\s*Define\s+(.*)\s*=\s*(.*)\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_DIRECTORIES_REGEX                       = new Regex(@"^\s*Directory\s*\{\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_DIRECTORY_DIR_REGEX                     = new Regex(@"^\s*(.*)\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_DIRECTORY_REGEX                         = new Regex(@"^\s*(Directory)\s+(\S+)(.*)$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_EMBEDDED_LIB_PLATFORM_REGEX             = new Regex(@"^\s*EmbeddedLib(\S*)\s*(.*)\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_EMBEDDED_LIB_PLATFORM_SCOPE_REGEX       = new Regex(@"^\s*EmbeddedLib(\S*)\s*(.*)\s*\{\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_ENDSCOPE_REGEX                          = new Regex(@"^\s*\}\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_EXPORTER_REGEX                          = new Regex(@"^\s*(.*)\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_EXPORTERS_REGEX                         = new Regex(@"^\s*Exporters\s+(.*){\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_FILE_REGEX                              = new Regex(@"^\s*Files\s*\{*\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_REFERENCES_REGEX                        = new Regex(@"^\s*References\s*\{*\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_PROJECTREFERENCES_REGEX                 = new Regex(@"^\s*ProjectReferences\s*\{*\s*$", REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_FILENAME_REGEX                          = new Regex(@"^\s*(\S*)\s*$", REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_REFERENCE_REGEX                         = new Regex(@"^\s*""?([^""]*)""?\s*$", REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_PROJECTREFERENCE_REGEX                  = new Regex(@"^\s*""?([^""]*)""?\s*$", REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_PROJECTREFERENCE_NOCOPYLOCAL_REGEX      = new Regex(@"^\s*""?([^""]*)""?\s*NOCOPYLOCAL\s*$", REGEX_OPTIONS);           
        private static readonly Regex MAKEFILE_FOLDER_REGEX                            = new Regex(@"^\s*(Folder)\s+(\S+)(.*)$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_INCLUDEFILE_REGEX                       = new Regex(@"^\s*Include\s+(.*)\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_INCLUDEPATH_RULED_REGEX                 = new Regex(@"^\s*IncludePath\[(\S*)\]\s*(.*)\s*$", REGEX_OPTIONS);
        private static readonly string MAKEFILE_KEYVAL_REGEX                           = @"^\s*{0}\s+(.*)$";
        private static readonly Regex MAKEFILE_LIBRARIES_REGEX                         = new Regex(@"^\s*Libraries\s*{\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_LIBRARY_REGEX                           = new Regex(@"^\s*(.*)\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_MAKEFILE_REGEX                          = new Regex(@"^\s*Makefile\s*\{\s*$",REGEX_OPTIONS);        
        private static readonly Regex MAKEFILE_NEW_PLATFORM_REGEX                      = new Regex(@"^\s*NewPlatform\s+(.*){\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_OPEN_SCOPE                              = new Regex(@"^\s*{\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_PARSE_REGEX                             = new Regex(@"^\s*Parse\s*\{*\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_PLATFORM_REGEX                          = new Regex(@"^\s*Platform\s+(.*){\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_PROJECT_SCOPE_REGEX                     = new Regex(@"^\s*Project\s+(.*){\s*$", REGEX_OPTIONS);
        public static readonly Regex MAKEFILE_INTERNAL_CONDITION_REGEX                 = new Regex(@"^\s*(.*)\s*(==|!=|=~|!~)\s*'*([^']*)'*\s*$", REGEX_OPTIONS);
        
        //@"^\s*([^\[]+)\[(.*)\s*(==|!=)\s*'*([^']*)'*\]\s*(\+=|\<\<|\>\>|=)\s*'*([^']*)'*.*$",REGEX_OPTIONS); // <- the bad boy
        private static readonly Regex MAKEFILE_RULE_REGEX                              = new Regex(@"^\s*([^\[]+)\[([^\]]*)\]\s*(\+=|\<\<|\>\>|=)\s*'*([^']*)'*.*\s*$", REGEX_OPTIONS); // <- not so bad

        private static readonly Regex MAKEFILE_SOLUTION_SCOPE_REGEX                         = new Regex(@"^\s*Solution\s+(.*)\s*\{\s*$", REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_SOLUTION_PROJECTS_SCOPE_REGEX                = new Regex(@"^\s*Projects\s+(.*)\s*(.*)\s*\{\s*$", REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_SOLUTION_PROJECT_DEPS_SCOPE_REGEX            = new Regex(@"^\s*Dependencies\s+\{\s*$", REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_SOLUTION_PROJECT_SCOPE_REGEX                 = new Regex(@"^\s*(\S+)\s+(.+)\s*\{\s*$", REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_SOLUTION_PROJECT_REGEX                       = new Regex(@"^\s*(\S*)\s+([^\{]*)\s*$", REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_SOLUTION_PROJECT_DEP_REGEX                   = new Regex(@"^\s*(.*)\s*$", REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_SOLUTION_CONFIGS_SCOPE_REGEX                 = new Regex(@"^\s*Solution_Configs\s+\{\s*$", REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_SOLUTION_CONFIG_REGEX                        = new Regex(@"^\s*(.*)\s*$", REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_SOLUTION_PLATFORMS_SCOPE_REGEX               = new Regex(@"^\s*Solution_Platforms\s+\{\s*$", REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_SOLUTION_PLATFORM_REGEX                      = new Regex(@"^\s*(.*)\s*$", REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_SOLUTION_PROJECT_CONFIG_RULES_SCOPE_REGEX    = new Regex(@"^\s*Project_Config_Rules\s+\{\s*$", REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_SOLUTION_BUILD_RULES_SCOPE_REGEX             = new Regex(@"^\s*Build_Rules\s+\{\s*$", REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_SOLUTION_DEPLOY_RULES_SCOPE_REGEX            = new Regex(@"^\s*Deploy_Rules\s+\{\s*$", REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_SOLUTION_REGEX_RULE_REGEX                    = new Regex(@"^\s*(.*)\s*=\s*'(.*)'\s*$", REGEX_OPTIONS);

        private static readonly Regex MAKEFILE_RULE_REGEX_UNCONDITIONAL                     = new Regex(@"^\s*([^\+]*)\s*(\+=|=|\<\<|\>\>)\s*'(.*)'\s*$",REGEX_OPTIONS);
        private static readonly Regex MAKEFILE_RULES_REGEX                                  = new Regex(@"^\s*Rules\s+(.*){\s*$", REGEX_OPTIONS);
        
        private static readonly string  PCH_FILENAME         = "pch";
        private static readonly string  PCH_FILETYPE        = ".cpp";
        public static readonly string   PCH_CREATE          = "Create";
        public static readonly string   PCH_USE             = "Use";

        // The name of the folder created within project files for storing projgen related files.
        private static readonly String PROJGEN_FOLDER = "_projgen";

        /// <summary>
        /// Strips trailing comments.
        /// </summary>
        public static readonly Regex MAKEFILE_COMMENT_REGEX = new Regex(@"^(.*)#(.*)$", REGEX_OPTIONS);
        
        #endregion // constants

        #region Constructors

        /// <summary>
        /// Parameterised constructor
        /// </summary>
        public Makefile(IUniversalLog log, CommandOptions options, string mangle)
        {
            Log = log;
            Options = options;
            Project = new Independent.Project();
            Rules = new List<MakeRule>();
            ExporterRules = new List<MakeRule>();
            Exporters = new List<Independent.IProjectExporter>();
            MakefilesDiscovered = new List<string>();
            Mangle = mangle;
            Solution = new Independent.Solution(Log);
            SolutionRules = new SolutionRules();
        }
        #endregion // constructors

        #region Public Properties
        /// <summary>
        /// An independent project to populate when reading a makefile.
        /// </summary>
        public Independent.Project Project { get; set; }

        /// <summary>
        /// An independent solution to populate when reading a makefile.
        /// </summary>
        public Independent.Solution Solution { get; set; }

        #endregion // Public Properties
        #region Private Properties
        private List<MakeRule> Rules { get; set; }
        private List<MakeRule> ExporterRules { get; set; }
        private SolutionRules SolutionRules { get; set; }
        private List<Independent.IProjectExporter> Exporters { get; set; }
        private List<string> MakefilesDiscovered { get; set; }
        private string Mangle { get; set; }
        
        /// <summary>
        /// Universal log
        /// </summary>
        private static IUniversalLog Log { get; set; }
        
        /// <summary>
        /// Command options - cached
        /// </summary>
        private CommandOptions Options { get; set; }
        #endregion // Private Properties

        #region Public Methods

        /// <summary>
        /// Import of a makefile filename
        /// (IProjectImporter)
        /// </summary>
        /// <param name="makefileFilename">The makefile filename to import</param>
        /// <returns>true upon success in parsing makefile</returns>
        public bool Import(string makefileFilename)
        {
            string sourceFilename = Path.GetFileName(makefileFilename);
            string workingDirectory = Path.GetDirectoryName(makefileFilename);

            string path = Path.Combine(workingDirectory, makefileFilename);
            if (!File.Exists(path))
            {
                Log.Error("Makefile {0} does not exist", path);
                return false;
            }
            
            Trace("Working Directory {0}", workingDirectory);

            // At this time we don't know what we are going to import, we set the path for convenience in case it is required.
            Solution.Path = path;
            Project.Path = path;

            // this is setup as a default since some makefiles don't specify a root directory, that can lead to problems.
            Project.RootDirectory = workingDirectory;

            LoadUnityConfig();

            return Parse(sourceFilename, workingDirectory);
        }   
     
        /// <summary>
        /// Set the language of the Project's targets
        /// </summary> 
        public void InferLanguagesOfTargets()
        {
            foreach (Independent.Target target in Project.Targets)
            {
                if (Project.IsValid())
                {
                    target.Language = LanguageExtensions.InferLanguage(Log, Project);
                    if (target.Language == Language.Invalid)
                    {
                        Log.Error("Could not infer the language for a target. Project name = {0}", Project.Name);
                    }
                    }
            }
        }

        /// <summary>
        /// Run all rules on the makefile. 
        /// </summary>
        public void ProcessRules()
        {
            if (Project.IsValid())
            {
                Project.ProcessRules(Rules);
            }
            
            if (Solution.IsValid())
            {
                Solution.ProcessRules(SolutionRules);
            }
        }

        /// <summary>
        /// Runs AFTER all rules have been processed
        /// <remarks>Does generalised fixup</remarks>
        /// <remarks>Do NOT abuse, code here generally indicates that some feature is required to be incorporated into the rules file.</remarks>
        /// </summary>
        public void PostProcessRules()
        {
            RemoveDuplicates();
            OrderForceIncludes();
        }

        /// <summary>
        /// Run exporter specific rules
        /// </summary>
        /// <returns></returns>
        public void RunExporterRules()
        {
            // DW: I used to call exporter.Project = SimpleObjectCloner.Clone(Project);
            // for a reliable deep copy. That was quite slow since it naively serialises the Project each time.
            // This only needs done once per project, each exporter can deserialise from the same memory stream.

            if (Project.IsValid())
            {
                MemoryStream projectStream = SimpleObjectCloner.PreSerialise(Project);

                foreach (Independent.IProjectExporter exporter in Exporters.Where(n => n.ExportsProjects))
                {
                    // clone the object since the exporter rules will possibly make changes to it.
                    exporter.Project = SimpleObjectCloner.Clone(Project, projectStream);
                    ProcessExporterRules(exporter);
                }
            }

            if (Solution.IsValid())
            {
                MemoryStream solutionStream = SimpleObjectCloner.PreSerialise(Solution);

                foreach (Independent.IProjectExporter exporter in Exporters.Where( n => n.ExportsSolutions))
                {
                    // clone the object since the exporter rules will possibly make changes to it.
                    exporter.Solution = SimpleObjectCloner.Clone(Solution, solutionStream);
                    ProcessSolutionExporterRules(exporter);
                }
            }
        }

        /// <summary>
        /// Interprets unity setup
        /// </summary>
        /// <returns></returns>
        public void DesignateUnity()
        {
            foreach (Independent.IProjectExporter exporter in Exporters)
            {
                exporter.DesignateUnity();
            }
        }

        /// <summary>
        /// Export the makefile for each exporter known to it. 
        /// <remarks>populate the exportedFilenames with the files created as a result of running each exporter.</remarks>
        /// </summary>
        /// <param name="exportedFilenames">A list of exported filenames to populate</param>
        /// <param name="verify">verify generated files</param>
        /// <returns>bool indicating export success</returns>
        public bool Export(ICollection<string> exportedFilenames, bool verify)
        {           
            bool ok = true;
            foreach (Independent.IProjectExporter exporter in Exporters)
            {
                if (exporter.Export(exportedFilenames, verify))
                {
                    Trace("exporter {0} exported ok", exporter.ExporterType.EnumToString());
                }
                else
                {
                    Error("exporter {0} export failed", exporter.ExporterType.EnumToString());
                    ok = false;
                }
            }
            return ok;
        }

        /// <summary>
        /// Get the list of files that will be exported
        /// </summary>
        /// <param name="filenames">The list of filenames to populate</param>
        public void PreDetermineExportedFiles(ICollection<string> filenames)
        {
            foreach (Independent.IProjectExporter exporter in Exporters)
            {
                exporter.Language = this.Project.Targets.First().Language;
                exporter.PreDetermineExportedFiles(filenames);
            }
        }

        /// <summary>
        /// for each top level directory discover 'parser' files
        /// upon discovery create an independent file entry
        /// a new _parser.h filename for each 'parser' file.
        /// this is in a folder [parser files] under this directory.
        /// although the filenames are alongside their .cpp / .h / .psc equivalent.
        /// </summary>
        public void PopulateParserFileArtefacts()
        {
            foreach (Independent.ProjectDirectory containerDir in Project.Directories)
            {
                // containerdir is just a container
                foreach (Independent.ProjectDirectory topDir in containerDir.Directories)
                {
                    string virtualDirectoryName = Path.Combine(topDir.Path, PARSER_DIR);
                    Independent.ProjectDirectory addDir = new Independent.ProjectDirectory(virtualDirectoryName, true);

                    if (CreateParserArtefacts(addDir, topDir) > 0)
                    {
                        topDir.Add(addDir);
                    }
                }
            }
        }

        #endregion // Public Methods
        #region Private Static Methods

        /// <summary>
        /// Helper to get the first 2 matched values out the match as a keyval pair
        /// - trims them too.
        /// </summary>
        /// <param name="match"></param>
        /// <returns></returns>
        private static KeyValuePair<string, string> KeyVal(Match match)
        {
            return new KeyValuePair<string, string>(match.Groups[1].Value.Trim(), match.Groups[2].Value.Trim());
        }

        private void WarnOnNonExistentFile(Independent.ProjectFile file)
        {
            if (WARN_IF_SRC_FILES_DONT_EXIST)
            {
                if (!File.Exists(file.Path))
                {
                    if (!File.Exists(Path.ChangeExtension(file.Path, FileType.Psc.EnumToString())))
                    {
                        if (!file.Path.Contains(PARSER_HDR))
                        {
                            string msg = string.Format("Src file {0} doesn't exist ( Is this a parCodeGen created file? )", Path.GetFullPath(file.Path));
                            FileType fileType = FileTypeExtensions.Create(Log, Path.GetExtension(file.Path));
                            if (fileType == FileType.Sch || fileType == FileType.H)
                            {
                                // a lesser warning since the missing header shouldn't impact the compilation
                                // these warnings should be followed up by programmers.
                                Warning(msg + "[Hdrs are lazily maintained]");
                            }
                            else
                            {
                                Warning(msg);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Recursive method to remove duplicate filenames
        /// </summary>
        /// <param name="dir"></param>
        private static void RemoveDuplicatesFilenames(Independent.ProjectDirectory dir)
        {
            dir.Files = dir.Files.Distinct().ToList();
            foreach (Independent.ProjectDirectory d in dir.Directories)
            {
                RemoveDuplicatesFilenames(d);
            }
        }

        #region match methods

        #region match helpers

        private static bool Match(string s, ref string val, Regex regex, int matchIdx = 1)
        {
            Match match = regex.Match(s);
            if (match.Success)
            {
                val = match.Groups[matchIdx].Value.Trim();
            }
            return match.Success;
        }

        private static bool Match(string s, ref string val, string regex, int matchIdx = 1)
        {
            Match match = Regex.Match(s, regex, REGEX_OPTIONS);
            if (match.Success)
            {
                val = match.Groups[matchIdx].Value.Trim();
            }
            return match.Success;
        }

        private static bool MatchKeyVal(string s, string key, ref string result)
        {
            string regex = String.Format(MAKEFILE_KEYVAL_REGEX, key);
            return Match(s, ref result, regex);
        }

        private static bool MatchKeyValOut(string s, string key, out string result)
        {
            result = String.Empty;
            return MatchKeyVal(s, key, ref result);
        }

        private static bool MatchRegex(string s, string regex)
        {
            return Regex.Match(s, regex, RegexOptions.IgnoreCase).Success;
        }

        private static bool MatchRegex(string s, Regex regex)
        {
            return regex.Match(s).Success;
        }

        #endregion // match helpers

        #region each match
        // 'one-linerised' to keep it easy to read for now.
        private static bool MatchSolutionScope(string s, ref string solutionName) { return Match(s, ref solutionName, MAKEFILE_SOLUTION_SCOPE_REGEX); }
        private static bool MatchSolutionProjectsScope(string s) { return MatchRegex(s, MAKEFILE_SOLUTION_PROJECTS_SCOPE_REGEX); }
        private static bool MatchSolutionProjectDepsScope(string s) { return MatchRegex(s, MAKEFILE_SOLUTION_PROJECT_DEPS_SCOPE_REGEX); }
        private static bool MatchSolutionProjectDep(string s, ref string dependencyProjectId) { return Match(s, ref dependencyProjectId, MAKEFILE_SOLUTION_PROJECT_DEP_REGEX); }
        private static bool MatchSolutionConfigsScope(string s) { return MatchRegex(s, MAKEFILE_SOLUTION_CONFIGS_SCOPE_REGEX); }
        private static bool MatchSolutionConfig(string s, ref string configName) { return Match(s, ref configName, MAKEFILE_SOLUTION_CONFIG_REGEX); }
        private static bool MatchSolutionPlatformsScope(string s) { return MatchRegex(s, MAKEFILE_SOLUTION_PLATFORMS_SCOPE_REGEX); }
        private static bool MatchSolutionPlatform(string s, ref string platformName) { return Match(s, ref platformName, MAKEFILE_SOLUTION_PLATFORM_REGEX); }
        private static bool MatchSolutionProjectConfigRulesScope(string s) { return MatchRegex(s, MAKEFILE_SOLUTION_PROJECT_CONFIG_RULES_SCOPE_REGEX); }
        private static bool MatchSolutionBuildRulesScope(string s) { return MatchRegex(s, MAKEFILE_SOLUTION_BUILD_RULES_SCOPE_REGEX); }
        private static bool MatchSolutionDeployRulesScope(string s) { return MatchRegex(s, MAKEFILE_SOLUTION_DEPLOY_RULES_SCOPE_REGEX); }
        private static bool MatchExporters(string s) { return MatchRegex(s, MAKEFILE_EXPORTERS_REGEX); }
        private static bool MatchRules(string s) { return MatchRegex(s, MAKEFILE_RULES_REGEX); }
        private static bool MatchFiles(string s) { return MatchRegex(s, MAKEFILE_FILE_REGEX); }
        private static bool MatchReferences(string s) { return MatchRegex(s, MAKEFILE_REFERENCES_REGEX); }
        private static bool MatchProjectReferences(string s) { return MatchRegex(s, MAKEFILE_PROJECTREFERENCES_REGEX); }
        private static bool MatchParse(string s) { return MatchRegex(s, MAKEFILE_PARSE_REGEX); }
        private static bool MatchMakefile(string s) { return MatchRegex(s, MAKEFILE_MAKEFILE_REGEX); }
        private static bool MatchEndScope(string s) { return MatchRegex(s, MAKEFILE_ENDSCOPE_REGEX); }
        private static bool MatchBlankLine(string s) { return MatchRegex(s, MAKEFILE_BLANK_LINE); }
        private static bool MatchCustom(string s) { return MatchRegex(s, MAKEFILE_CUSTOM_REGEX); }
        private static bool MatchDirectories(string s) { return MatchRegex(s, MAKEFILE_DIRECTORIES_REGEX); }
        private static bool MatchLibraries(string s) { return MatchRegex(s, MAKEFILE_LIBRARIES_REGEX); }

        private static bool MatchBuildConfig(string s, ref string config) { return Match(s, ref config, MAKEFILE_BUILD_CONFIG_REGEX); }
        private static bool MatchDirectoriesDir(string s, ref string dir) { return Match(s, ref dir, MAKEFILE_DIRECTORY_DIR_REGEX); }
        private static bool MatchIncludeFile(string s, ref string includefile) { return Match(s, ref includefile, MAKEFILE_INCLUDEFILE_REGEX); }
        private static bool MatchExporter(string s, ref string exporter) { return Match(s, ref exporter, MAKEFILE_EXPORTER_REGEX); }
        private static bool MatchLibrary(string s, ref string library) { return Match(s, ref library, MAKEFILE_LIBRARY_REGEX); }
        private static bool MatchFilename(string s, ref string filename) { return Match(s, ref filename, MAKEFILE_FILENAME_REGEX); }
        private static bool MatchReference(string s, ref string reference) { return Match(s, ref reference, MAKEFILE_REFERENCE_REGEX); }
        private static bool MatchProjectReference(string s, ref string projReference) { return Match(s, ref projReference, MAKEFILE_PROJECTREFERENCE_REGEX); }
        private static bool MatchProjectReferenceNoCopyLocal(string s, ref string projReference) { return Match(s, ref projReference, MAKEFILE_PROJECTREFERENCE_NOCOPYLOCAL_REGEX); }
        
        private static bool MatchNewPlatform(string s, ref string platformName) { return Match(s, ref platformName, MAKEFILE_NEW_PLATFORM_REGEX); }
        private static bool MatchPlatform(string s, ref string platformName) { return Match(s, ref platformName, MAKEFILE_PLATFORM_REGEX); }
        private static bool MatchCustomFilename(string s, ref string customFileName) { return Match(s, ref customFileName, MAKEFILE_CUSTOM_FILENAME_REGEX); }
        private static bool MatchCustomFilenameScope(string s, ref string customFileName) { return Match(s, ref customFileName, MAKEFILE_CUSTOM_FILENAME_SCOPE_REGEX); }
        private static bool MatchCustomFilenamePlatformScope(string s, ref string customFileName) { return Match(s, ref customFileName, MAKEFILE_CUSTOM_FILENAME_PLATFORM_SCOPE_REGEX); }
        private static bool MatchConfig(string s, ref string config) { return Match(s, ref config, MAKEFILE_CONFIG_REGEX); }
        private static bool MatchDirectory(string s, ref string directory) { return Match(s, ref directory, MAKEFILE_DIRECTORY_REGEX, 2); }
        private static bool MatchFolder(string s, ref string folder) { return Match(s, ref folder, MAKEFILE_FOLDER_REGEX, 2); }
        private static bool MatchAction(string s, ref string action) { return Match(s, ref action, MAKEFILE_ACTION_REGEX); }

        private static bool MatchCustomFileCommand(string s, ref string command) { return Match(s, ref command, MAKEFILE_CUSTOM_FILE_COMMAND_REGEX); }
        private static bool MatchCustomFileDescription(string s, ref string desc) { return Match(s, ref desc, MAKEFILE_CUSTOM_FILE_DESC_REGEX); }
        private static bool MatchCustomFileDependencies(string s, ref string deps) { return Match(s, ref deps, MAKEFILE_CUSTOM_FILE_DEPS_REGEX); }
        private static bool MatchCustomFileOutputs(string s, ref string outputs) { return Match(s, ref outputs, MAKEFILE_CUSTOM_FILE_OUTPUTS_REGEX); }

        private static bool MatchInclude(string s, ref string include) { return MatchKeyVal(s, MAKEFILE_INCLUDE, ref include); }
        private static bool MatchRootDirectory(string s, ref string rootdir) { return MatchKeyVal(s, MAKEFILE_ROOT_DIRECTORY, ref rootdir); }
        private static bool MatchTemplate(string s, ref string templateName) { return MatchKeyVal(s, MAKEFILE_TEMPLATE, ref templateName); }
        private static bool MatchBuildTemplate(string s, ref string buildTemplateName) { return MatchKeyVal(s, MAKEFILE_BUILD_TEMPLATE, ref buildTemplateName); }

        private bool MatchSolutionProjectScope(string s, ref Independent.SolutionProject project)
        {
            Match match = MAKEFILE_SOLUTION_PROJECT_SCOPE_REGEX.Match(s);
            if (match.Success && match.Groups.Count >= 3)
            {               
                string projectName = match.Groups[1].Value.Trim();
                string projectUnSuffixedPath = match.Groups[2].Value.Trim();
                project = new Independent.SolutionProject(Log, projectName, projectUnSuffixedPath);
            }
            return match.Success;
        }

        private bool MatchSolutionProject(string s, ref Independent.SolutionProject project)
        {
            Match match = MAKEFILE_SOLUTION_PROJECT_REGEX.Match(s);
            if (match.Success && match.Groups.Count >= 3)
            {
                string projectName = match.Groups[1].Value.Trim();
                string projectUnSuffixedPath = match.Groups[2].Value.Trim();
                project = new Independent.SolutionProject(Log, projectName, projectUnSuffixedPath);
            }
            return match.Success;
        }

        private bool MatchSolutionRegexRule(string s, out KeyValuePair<string, Regex> projectsToMatch)
        {
            Match match = MAKEFILE_SOLUTION_REGEX_RULE_REGEX.Match(s);
            if (match.Success && match.Groups.Count >= 3)
            {
                string projectMatch = match.Groups[1].Value.Trim();
                string slnConfigMatch = match.Groups[2].Value.Trim();
                Regex regex = new Regex(projectMatch);
                projectsToMatch = new KeyValuePair<string, Regex>(slnConfigMatch, regex);
            }
            else
            {
                projectsToMatch = new KeyValuePair<string, Regex>(String.Empty, null);
                Log.Error("Invalid Solution Regex Rule");
            }
            return match.Success;
        }

        private bool MatchSolutionBoolRegexRule(string s, out KeyValuePair<bool, Regex> projectsToMatch)
        {
            Match match = MAKEFILE_SOLUTION_REGEX_RULE_REGEX.Match(s);
            if (match.Success && match.Groups.Count >= 3)
            {
                string projectMatch = match.Groups[1].Value.Trim();
                string slnConfigMatch = match.Groups[2].Value.Trim();
                Regex regex = new Regex(projectMatch);
                bool set = bool.Parse(slnConfigMatch);
                projectsToMatch = new KeyValuePair<bool, Regex>(set, regex);
            }
            else
            {
                projectsToMatch = new KeyValuePair<bool, Regex>(false, null);
                Log.Error("Invalid Solution Boolean Regex Rule");
            }
            return match.Success;
        }

        private static bool MatchEmbeddedLibPlatform(string s, out KeyValuePair<string, string> lib)
        {
            Match match = MAKEFILE_EMBEDDED_LIB_PLATFORM_REGEX.Match(s);
            if (match.Success && match.Groups.Count >= 3)
            {
                lib = KeyVal(match);
                return true;
            }
            lib = new KeyValuePair<string, string>();
            return false;
        }

        private static bool MatchIncludePathRuled(string s, out KeyValuePair<string, string> includePath)
        {
            Match match = MAKEFILE_INCLUDEPATH_RULED_REGEX.Match(s);
            if (match.Success && match.Groups.Count >= 3)
            {
                includePath = KeyVal(match);
                return true;
            }
            includePath = new KeyValuePair<string, string>();
            return false;
        }

        private static bool MatchEmbeddedLibPlatformScope(string s, out EmbeddedLibPlatformScope scope)
        {
            Match match = MAKEFILE_EMBEDDED_LIB_PLATFORM_SCOPE_REGEX.Match(s);
            if (match.Success && match.Groups.Count >= 3)
            {
                scope = new EmbeddedLibPlatformScope();
                scope.EmbeddedLibPlatform = KeyVal(match);
                return true;
            }
            scope = new EmbeddedLibPlatformScope();
            return false;
        }

        private static bool MatchDefine(string s, out KeyValuePair<string, string> define)
        {
            Match match = MAKEFILE_DEFINE_VAL_REGEX.Match(s);
            if (match.Success && match.Groups.Count >= 3)
            {
                define = KeyVal(match);
                return true;
            }
            else
            {
                match = MAKEFILE_DEFINE_REGEX.Match(s);
                if (match.Success && match.Groups.Count >= 2)
                {
                    define = new KeyValuePair<string, string>(match.Groups[1].Value.Trim(), null);
                    return true;
                }
            }

            define = new KeyValuePair<string, string>();
            return false;
        }

        private static string MatchCondition(string s)
        {
            Match match = MAKEFILE_CONDITION_REGEX.Match(s);
            return (match.Success) ? match.Groups[1].Value.Trim() : null;
        }

        private static string MatchExporterRules(string s)
        {
            Match match = MAKEFILE_EXPORTER_RULES_REGEX.Match(s);
            return (match.Success) ? match.Groups[1].Value.Trim() : null;
        }

        private MakeRule MatchRule(string s)
        {
            Match match = MAKEFILE_RULE_REGEX.Match(s);
            if (match.Success && match.Groups.Count >= 5)
            {
                MakeAction action = new MakeAction(Log, match.Groups[1].Value.Trim(), match.Groups[3].Value.Trim(), match.Groups[4].Value.Trim());
                MakeRule rule = new MakeRule(Log);
                rule.Actions.Add(action);
                rule.ConditionGroup.AddConditions(match.Groups[2].Value.Trim());
                return rule;
            }
            return null;
        }

        private MakeRule MatchUnconditionalRule(string s)
        {
            Match match = MAKEFILE_RULE_REGEX_UNCONDITIONAL.Match(s);
            if (match.Success && match.Groups.Count >= 4)
            {
                MakeAction action = new MakeAction(Log, match.Groups[1].Value.Trim(), match.Groups[2].Value.Trim(), match.Groups[3].Value.Trim());
                return new MakeRule(Log, action, null);
            }
            return null;
        }

        private MakeRule MatchAssignmentRule(string s, string actionSetting)
        {
            Match match = MAKEFILE_ASSIGNMENT_RULE_REGEX.Match(s);
            if (match.Success && match.Groups.Count >= 3)
            {
                MakeAction action = new MakeAction(Log, actionSetting, match.Groups[1].Value.Trim(), match.Groups[2].Value.Trim());
                return new MakeRule(Log, action, null);
            }
            return null;
        }

        private MakeRule MatchActionlessRule(string s, string actionSetting)
        {
            Match match = MAKEFILE_ACTIONLESS_RULE_REGEX.Match(s);
            if (match.Success && match.Groups.Count >= 4)
            {
                string conditions = match.Groups[1].Value.Trim();
                MakeAction action = new MakeAction(Log, actionSetting, match.Groups[2].Value.Trim(), match.Groups[3].Value.Trim());
                MakeRule rule = new MakeRule(Log);
                rule.Actions.Add(action);
                rule.ConditionGroup.AddConditions(conditions);
                return rule;
            }
            return null;
        }
        #endregion // each match

        #endregion // match methods
        #region misc methods
        private void Trace(String message, params Object[] args)
        {
            if (TRACE_MAKE)
            {
                Log.Message(message, args);
            }
        }

        private void Error(String message, params Object[] args)
        {
            Log.Error(message, args);
        }

        private void Warning(String message, params Object[] args)
        {
            Log.Warning(message, args);
        }

        private static string OpenScope(string str)
        {
            return string.Format("{0} {{ {1} ", str, MAKEFILE_COMMENT_CHAR);
        }

        private static string CloseScope()
        {
            return string.Format("}} {0} END ", MAKEFILE_COMMENT_CHAR);
        }

        private static string DirectoryOpenScope(string newWorkingDirectory)
        {
            return string.Format("Directory {0} {{", newWorkingDirectory);
        }
        private static string DirectoryCloseScope(string dir)
        {
            return string.Format("}} {0} END Directory {1}", MAKEFILE_COMMENT_CHAR, dir);
        }

        // 
        // search through stack for an object type in scope.
        // 
        private static bool IsInScope(LinkedList<object> scope, Type search)
        {
            foreach (Object o in scope)
            {
                if (o.GetType() == search)
                {
                    return true;
                }
            }
            return false;
        }

        // Overkill - but there was a development reason for this - I think it was to catch unbalanced scopes.
        private static bool ExitScope(Object last)
        {
            return last is IPoppableScope;
        }

        // helper for printing makefile text out.
        // String.format doesn't like squigglies
        private static string IndentString(string s, int depth)
        {
            return ("".PadLeft(depth * TRACE_INDENT) + s.Replace("{", "{{").Replace("}", "}}"));
        }

        //
        // Pretty formats a preprocessed line of text in a makefile.
        // - indents it
        //
        private string AddPreprocessedLine(string s, int depth, List<string> lines)
        {
            s = String.Empty.PadLeft(depth * PREPROCESS_INDENT) + s;
            lines.Add(s);

            if (TRACE_PREPROCESS_MAKELINES)
                Log.Message(IndentString(s, depth)); // String.format doesn't like squigglies

            return s;
        }

        //
        // Removes a trailing comment from a makefile line.
        //
        private static string RemoveComment(string s)
        {
            // remove comments
            Match match = MAKEFILE_COMMENT_REGEX.Match(s);
            return match.Success ? match.Groups[1].Value : s;
        }

        #endregion // misc methods
        #endregion // Private Static Methods
        #region Private Methods

        /// <summary>
        /// Run all solution exporter rules.
        /// </summary>
        private void ProcessSolutionExporterRules(Independent.IProjectExporter exporter)
        {
            exporter.Solution.ProcessExporterRules(SolutionRules, exporter.ExporterType);
        }
    
        /// <summary>
        /// Run all exporter rules on the makefile. 
        /// </summary>
        private void ProcessExporterRules(Independent.IProjectExporter exporter)
        {
            // Set the exporter being used for exporter rules to utilise.
            foreach (Independent.Target target in exporter.Project.Targets)
            {
                target.Exporter = exporter.ExporterType;
                target.Language = exporter.Language;
            }

            exporter.Project.ProcessRules(ExporterRules);
        }

        /// <summary>
        /// Loads the unity config file into the independent representation
        /// </summary>
        /// <param name="unityFilename"></param>
        private void LoadUnityConfig()
        {
            string ext = Path.GetExtension(Project.Path);
            if (!ext.ToLower().Contains(BUILD_FILE_EXTENSION))                                                                
            {
                string unityFilename = Path.ChangeExtension(Project.Path, UNITY_CFG_FILE_EXTENSION);
                if (File.Exists(unityFilename))
                {
                    String[] readlines = File.ReadAllLines(unityFilename);
                    foreach (string curStr in readlines)
                    {
                        string s = RemoveComment(curStr.Trim());

                        if (s.Length == 0 || s[0] == MAKEFILE_COMMENT_CHAR)
                            continue;

                        Match match = UNITY_REGEX_EXCLUDE.Match(s);
                        if (match.Success)
                        {
                            string excludes = match.Groups[1].Value.Trim().ToLower();
                            if (excludes.Length > 0)
                            {
                                string[] excludeList = excludes.Split(' ');
                                Project.Unity.Exclusions.AddRange(excludeList);
                            }
                            continue;
                        }

                        match = UNITY_REGEX_ENABLED.Match(s);
                        if (match.Success)
                        {
                            string enabled = match.Groups[1].Value.Trim();
                            if (enabled.ToLower().Contains("false"))
                            {
                                Project.Unity.Enabled = false;
                            }
                            continue;
                        }

                        match = UNITY_REGEX_COMPILATION_UNIT.Match(s);
                        if (match.Success)
                        {
                            string unityDesignation = match.Groups[1].Value.Trim();
                            string unityRegex = match.Groups[2].Value.Trim();
                            String options = match.Groups[4].Value;

                            Match matchLtcg = LTCG_REGEX_DISABLE.Match(options);
                            bool ltcg = true;

                            if (matchLtcg.Success && matchLtcg.Groups.Count == 1)
                            {
                                ltcg = false;
                            }
                            else
                            {
                                // short form of unity directory
                                matchLtcg = LTCG_REGEX_DISABLE.Match(match.Groups[2].Value.Trim());
                                if (matchLtcg.Success && matchLtcg.Groups.Count == 1)
                                {
                                    ltcg = false;
                                    unityRegex = null;
                                }
                            }

                            if (unityRegex == null || unityRegex.Length == 0)
                            {
                                unityRegex = UNITY_EMPTY_REGEXP;
                                unityRegex = unityRegex.Replace(UNITY_FOLDER_TOKEN, unityDesignation);
                            }

                            bool ignoreCase = true;
                            // DW: all regexes ignore case now irresepctive of setting in .unity file TODO : tidy up files.
                            /*if (match.Groups.Count >= 3 && match.Groups[3].Value.ToLower().Trim() != "i")
                            {
                                ignoreCase = false;
                            }*/

                            Regex regex = new Regex(unityRegex, ignoreCase ? RegexOptions.IgnoreCase : RegexOptions.None );
                            Project.Unity.AddCompilationUnitSetup(unityDesignation, regex, ltcg);
                            continue;
                        }

                        Log.Warning("Unprocessed line {0} in unity file {1}", s, unityFilename);
                    }
                }
                else
                {
                    // It's ok for a unity file not to exists - just nothing to do.
                    //Log.Message("         ( Soft warning, No unity file found {0} )", unityFilename);
                }
            }
        }

        private void OrderForceIncludes()
        {
            foreach (Independent.Target target in Project.Targets)
            {
                OrderForceIncludes(target.Settings.Compiler);
            }
        }

        /// <summary>
        /// this is a temporary measure to reorder force includes 
        /// ideally there will be some way to express insertion order in rules file.
        /// </summary>
        /// <param name="compiler"></param>
        private void OrderForceIncludes(Independent.Compiler compiler)
        {            
            string forceInclude = compiler.ForceIncludes.FirstOrDefault(s => s.Contains(PRIORITY_FORCEINCLUDE));
            int idx = compiler.ForceIncludes.IndexOf(forceInclude);

            if (idx > 0)
            {
                compiler.ForceIncludes.RemoveAt(idx);
                compiler.ForceIncludes.Insert(0, forceInclude);
            }
        }

        /// <summary>
        /// For particular settings it's possible for the makefile to be incorrectly constructed
        /// here the duplicates will be removed rather that generate a warning or error.
        /// </summary>
        private void RemoveDuplicates()
        {
            foreach (Independent.ProjectDirectory dir in Project.Directories)
            {
                RemoveDuplicatesFilenames(dir);
            }

            foreach (Independent.Target target in Project.Targets)
            {
                target.Settings.Compiler.ForceIncludes = target.Settings.Compiler.ForceIncludes.Distinct().ToList();
                target.Settings.Compiler.AdditionalIncludeDirectories = target.Settings.Compiler.AdditionalIncludeDirectories.Distinct().ToList();
            }
        }

        /// <summary>
        /// Recursive function which adds new _parser.h filenames for each 'parser' file.
        /// </summary>
        /// <param name="addDir"></param>
        /// <param name="searchDir"></param>
        /// <returns></returns>
        private int CreateParserArtefacts(Independent.ProjectDirectory addDir, Independent.ProjectDirectory searchDir)
        {
            int numArtefacts = 0;
            foreach (Independent.ProjectFile file in searchDir.Files)
            {
                if (file.Parse)
                {
                    addDir.Add(new Independent.ProjectFile(file.Path + PARSER_HDR));
                    numArtefacts++;
                }
            }

            foreach (Independent.ProjectDirectory subDir in searchDir.Directories)
            {
                numArtefacts += CreateParserArtefacts(addDir, subDir);
            }

            return numArtefacts;
        }

        /// <summary>
        /// Search directory preprocessing makefiles (*.txt) found.
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="lines"></param>
        /// <param name="depth"></param>
        private void PreProcessDirSearch(string dir, List<string> lines, int depth)
        {
            try
            {                                                
                foreach (string file in Directory.GetFiles(dir, MEGA_PROJECT_MAKEFILE))
                {
                    Trace("Preprocessing {0}", file);
                    
                    // Make sure we can't discover the same makefile twice : this was added due to Natural motion makefile milarky.
                    // - it could be that I don't fully understand the makefile directory directive though..
                    if (!MakefilesDiscovered.Contains(file))
                    {
                        MakefilesDiscovered.Add(file);
                        if (!PreProcess(Path.GetFileName(file), dir, lines, depth + 1))
                        {
                            Error("Error PreProcessing {0}", file);
                        }
                    }                    
                }
               
                foreach (string d in Directory.GetDirectories(dir))
                {                  
                    PreProcessDirSearch(d, lines, depth+1);
                }
            }
            catch (Exception ex)
            {
                Log.ToolException(ex,"Error recursing directory {0}", dir);
            }
        }

        /// <summary>
        /// Preprocess - recursive preprocess makefile. 
        /// <remarks>Results one big monster makefile that thereafter be parsed or saved for debugging.</remarks>
        /// </summary>
        /// <param name="makefileFilename"></param>
        /// <param name="workingDirectory"></param>
        /// <param name="lines"></param>
        /// <param name="depth"></param>
        /// <returns></returns>
        private bool PreProcess(string makefileFilename, string workingDirectory, List<string> lines, int depth = 0)
        {
            string originalWorkingDirectory = Directory.GetCurrentDirectory();
            Directory.SetCurrentDirectory(workingDirectory);

            bool ok = true;
            bool isInDirectories = false; // megaproject scope flag

            {
                bool fileExists = File.Exists(makefileFilename);
                if (!fileExists)
                {
                    Log.Error("File not found during makefile preprocess {0}", makefileFilename);
                }

                List<string> readlines = fileExists ? File.ReadAllLines(makefileFilename).ToList<string>() : new List<string>();

                // All files need scoped to determine where where they start and end
                AddPreprocessedLine(OpenScope(MAKEFILE_MAKEFILE) + Path.Combine(workingDirectory, makefileFilename), depth, lines);
                depth += 1;

                foreach (string curStr in readlines)
                {
                    string s = RemoveComment(curStr.Trim());
                    
                    if (s.Length == 0 || s[0] == MAKEFILE_COMMENT_CHAR)
                        continue;

                    String templateName = null;
                    if (MatchTemplate(s, ref templateName))
                    {
                        continue; // ignore - for PG2 only
                    }

                    String buildTemplateName = null;
                    if (MatchBuildTemplate(s, ref buildTemplateName))
                    {
                        continue; // ignore - would already be handled at a higher level.
                    }

                    // Root Dir - needs added to project so that we can set the current working directory correctly.
                    string rootDirectory = String.Empty;
                    if (MatchRootDirectory(s, ref rootDirectory))
                    {
                        Project.RootDirectory = Path.GetFullPath(Path.Combine(workingDirectory, rootDirectory));
                        AddPreprocessedLine(s, depth, lines);
                        continue;
                    }

                    if (MatchDirectories(s))
                    {
                        isInDirectories = true;
                        // Long story - because we are in a directories scope it implicitly means we should be ignoring the root directory set by the makefile - don't ask me I didn't write these makefiles.
                        workingDirectory = Path.GetFullPath(originalWorkingDirectory);
                        Directory.SetCurrentDirectory(workingDirectory);
                        AddPreprocessedLine(DirectoryOpenScope(workingDirectory) + "# mega project directory - not relative to root directory!", depth, lines);
                        continue;
                    }

                    // Mega project support - when in this scope we only identify directories to process.
                    if (isInDirectories)
                    {
                        if (MatchEndScope(s))
                        {
                            isInDirectories = false;                           
                            AddPreprocessedLine(DirectoryCloseScope(Path.GetFullPath(originalWorkingDirectory)) + " - mega project directory", depth, lines);
                            if (Project.RootDirectory != null) // DW: I discovered a null root directory with natural motion, protecting this for now, the make file for this possibly needs reviewed. X:\gta5\src\dev\rage\naturalmotion\dump\include\makefile.txt
                            {
                                workingDirectory = Project.RootDirectory; // technically this looks wrong...
                                Directory.SetCurrentDirectory(workingDirectory);
                            }
                            else
                            {
                                Log.Warning("A root directory was not found for this makefile, please review the makefile");
                            }
                            continue;
                        }

                        string dir = null;
                        if (MatchDirectoriesDir(s, ref dir))
                        {
                            if (!Directory.Exists(dir))
                            {
                                Warning("Directory {0} doesn't exist.", dir);
                            }

                            AddPreprocessedLine(DirectoryOpenScope(dir), depth, lines);
                            string fullDir = Path.GetFullPath(Environment.ExpandEnvironmentVariables(Path.Combine(workingDirectory, dir)));
                            if (!Directory.Exists(fullDir))
                            {
                               Warning("Directory {0} doesn't exist.", fullDir);
                            }

                            string tempdir = Directory.GetCurrentDirectory();
                            Directory.SetCurrentDirectory(fullDir);
                            PreProcessDirSearch(fullDir, lines, depth + 1);
                            Directory.SetCurrentDirectory(tempdir);
                            AddPreprocessedLine(DirectoryCloseScope(dir), depth, lines);
                            continue;
                        }
                    }

                    // Include a file
                    string includeFilename = String.Empty;
                    if (MatchIncludeFile(s, ref includeFilename))
                    {
                        string includeFilenameOnly = Path.GetFileName(includeFilename);
                        string directory = Path.GetDirectoryName(Environment.ExpandEnvironmentVariables(includeFilename));
                        string cwd = Directory.GetCurrentDirectory();
                        string newWorkingDirectory = Project.RootDirectory == null ? cwd : Path.Combine(cwd, Project.RootDirectory, directory);

                        AddPreprocessedLine(DirectoryOpenScope(newWorkingDirectory), depth, lines);
                        if (!PreProcess(includeFilenameOnly, newWorkingDirectory, lines, depth + 1))
                        {
                            ok = false;
                        }
                        AddPreprocessedLine(DirectoryCloseScope(newWorkingDirectory), depth, lines);
                        continue;
                    }

                    // Project
                    string project;
                    if (MatchKeyValOut(s, MAKEFILE_PROJECT, out project))
                    {
                        if (depth <= 1) // if not the root project
                        {
                            AddPreprocessedLine(s, depth, lines); // add the root project - this isnt transformed to be a directory
                        }
                        depth += 1;
                        continue;
                    }

                    // Just add the line
                    AddPreprocessedLine(s, depth, lines);
                }
            }

            Directory.SetCurrentDirectory(originalWorkingDirectory); // reinstate working directory

            // file needs unscoped
            depth -= 1;
            AddPreprocessedLine(CloseScope() + Path.Combine(workingDirectory, makefileFilename), depth, lines);

            return ok;
        }

        /// <summary>
        /// Makefile Parse Method.
        /// <remarks>Initiates parsing.</remarks>
        /// </summary>
        /// <param name="makefileFilename"></param>
        /// <param name="workingDirectory"></param>
        /// <param name="scopeStack"></param>
        /// <param name="curDir"></param>
        /// <param name="depth"></param>
        /// <returns></returns>
        private bool Parse(string makefileFilename, string workingDirectory)
        {
            // 1. Set directory
            int depth = 0;
            string originalWorkingDirectory = workingDirectory;
            Directory.SetCurrentDirectory(workingDirectory); // was removed - my bad!!!!!!
            List<string> lines = new List<string>();
            Independent.ProjectDirectory curDir = new Independent.ProjectDirectory(originalWorkingDirectory);
            LinkedList<Object> scopeStack = new LinkedList<object>();
            PushScope(curDir, scopeStack);
            if (TRACE_MAKEFILENAMES) { Log.Message(String.Empty.PadLeft(depth) + "Makefile: Parse {0}/{1}", workingDirectory, makefileFilename); }

            // 2. Preprocess - expands the inclusions in the makefile into a monster makefile. 
            bool ok = PreProcess(makefileFilename, workingDirectory, lines);

            // 3. Write a Debug preprocessed file
            if (ok)
            {
                if (WRITE_PREPROCESSED_FILES)
                {
                    // Write a handy debug file to see what was preprocessed.                    
                    File.WriteAllText(makefileFilename + PREPROCESSED_FILETYPE, String.Join("\n", lines.ToArray()));
                }
            }
            else
            {
                Error("Makefile: Preprocess indicated there was a fault.");
            }

            // 4. Parse the preprocessed file.
            ParseEachLine(makefileFilename, workingDirectory, scopeStack, curDir, depth, lines);

            // 5. Reinstate directory
            Directory.SetCurrentDirectory(originalWorkingDirectory);

            PopScope(scopeStack);
            if (scopeStack.Count > 0)
            {
                Log.Warning("ScopeStack depth is {0} - there must be unbalanced scope in your makefiles.", scopeStack.Count);
            }

            return ok;
        }

        private void PushScope(Object obj, LinkedList<Object> scopeStack)
        {
            if (TRACE_SCOPESTACK)
            {
                string s = string.Format("Push {0} {1}", obj.GetType().ToString(), obj.GetHashCode());
                s = s.PadLeft(scopeStack.Count + s.Length, ' ');
                Log.Message(s);
            }
            scopeStack.AddLast(obj);
        }
        
        private void PopScope(LinkedList<Object> scopeStack)
        {
            Object last = scopeStack.Last.Value;
            scopeStack.RemoveLast();

            if (TRACE_SCOPESTACK)
            {
                string s = string.Format("Pop {0} {1}", last.GetType(), last.GetHashCode());
                s = s.PadLeft(scopeStack.Count + s.Length, ' ');
                Log.Message(s);
            }
        }

        /// <summary>
        /// for each line passed parse it.
        /// <remarks>creates the independent representation.</remarks>
        /// </summary>
        /// <param name="makefileFilename"></param>
        /// <param name="workingDirectory"></param>
        /// <param name="scopeStack"></param>
        /// <param name="curDir"></param>
        /// <param name="depth"></param>
        /// <param name="lines"></param>
        private void ParseEachLine(string makefileFilename, string workingDirectory, LinkedList<Object> scopeStack, Independent.ProjectDirectory curDir, int depth, List<string> lines)
        {
            string str = String.Empty; // temp str.
            
            // persists a previous setting for future use in a scope.
            Platform customFilePlatform = Platform.Invalid;
            Independent.CustomCommand customCommand = new Independent.CustomCommand();
            string platform = String.Empty;
            string action = String.Empty;
            string conditions = String.Empty;
            string exporterConditions = String.Empty;
            string platformScope = String.Empty;
            Independent.SolutionProject currSolutionProject = new Independent.SolutionProject(Log);
            Independent.ProjectFile customFile = null;
            EmbeddedLibPlatformScope embeddedLibPlatformScope;
            bool matchedRootDirectory = false;
            bool precompiledHeadersInUse = false;

            //
            // DW: Total hack to get around missing root directory issues TODO : review and improve, this root directory shenannigans is all cover the code base!
            bool foundRootDir = false;
            string rootDir = String.Empty;
            foreach (string curStr in lines)
            {
                string s = RemoveComment(curStr);
                if (MatchRootDirectory(s, ref rootDir))
                {
                    foundRootDir = true;
                    break;
                }
            }

            if (!foundRootDir)
            {
                lines.Insert(0, "RootDirectory .");
            }
            // End Hack.

            // Parse lines            
            foreach (string curStr in lines)
            {
                string s = RemoveComment(curStr);

                if (TRACE_MAKELINES)
                    Log.Message(IndentString(s, depth));

                // Blank Line
                if (MatchBlankLine(s))
                    continue;

                // Open Scope
                if (MatchRegex(s, MAKEFILE_OPEN_SCOPE))
                    continue;

                // ExporterRules
                string newExporterConditions = MatchExporterRules(s);
                if (newExporterConditions != null)
                {
                    PushScope(new ExporterRulesScope(), scopeStack);
                    exporterConditions += newExporterConditions;
                    continue;
                }

                if (MatchProjectType(s))
                    continue;
                
                if (MatchFrameworkVersion(s))
                    continue;

                if (MatchPlatformToolset(s))
                    continue;

                if (MatchOutputPath(s))
                    continue;

                if (MatchTitleId(s))
                    continue;

                if (MatchAdditionalSections(s))
                    continue;

                if (MatchForceInclude(s))
                    continue;

                if (MatchIncludePath(s))
                    continue;

                if (MatchDefine(s))
                    continue;

                if (MatchEmbeddedLibPlatformScope(scopeStack, s, out embeddedLibPlatformScope))
                    continue;

                if (MatchEmbeddedLibPlatform(s))
                    continue;

                if (MatchIncludePathRuled(s))
                    continue;
                
                // Root Dir
                string rootDirectory = String.Empty;
                if (MatchRootDirectory(s, ref rootDirectory))
                {
                    workingDirectory = Path.GetFullPath(Path.Combine(GetCurrentDirectory(scopeStack), rootDirectory));
                    curDir = NewDirectory(workingDirectory, scopeStack, curDir);
                    Project.Directories.Add(curDir);
                    matchedRootDirectory = true;
                    continue;
                }

                if (MatchProject(s))
                    continue;

                // Enter scope
                if (ParseNewScopes(scopeStack, ref platform, ref platformScope, ref currSolutionProject, s))
                    continue;

                // Build Config/ target
                if (MatchConfig(s, ref str))
                {
                    Independent.Target target = new Independent.Target(Log, platform, str);
                    Project.Targets.Add(target);
                    // maintain a list of platforms so we can know what platforms we are supporting - handy for custom rules that are per platform.
                    if (!Project.Platforms.Contains(target.Platform))
                    {
                        Project.Platforms.Add(target.Platform);
                    }
                    continue;
                }

                Object lastScope = scopeStack.Last();

                // Endscope
                if (MatchEndScope(s))
                {
                    if (lastScope is Independent.ProjectDirectory)
                    {
                        curDir = PopDirectory(scopeStack);
                        if (curDir != null)
                        {
                            if (!curDir.IsVirtual)
                            {
                                workingDirectory = curDir.Path;
                            }
                        }
                    }
                    else if (lastScope is ConditionScope)
                    {
                        conditions = String.Empty;
                        PopScope(scopeStack);
                    }
                    else if (lastScope is ExporterRulesScope)
                    {
                        exporterConditions = String.Empty;
                        PopScope(scopeStack);
                    }
                    else if (lastScope is CustomFilePlatformScope)
                    {
                        customFilePlatform = Platform.Invalid;
                        PopScope(scopeStack);
                    }
                    else if (lastScope is ProjectScope)
                    {
                        PopScope(scopeStack);
                    }
                    else if (lastScope is EmbeddedLibPlatformScope)
                    {
                        PopScope(scopeStack);
                    }
                    else if (lastScope is ReferencesScope)
                    {
                        PopScope(scopeStack);
                    }
                    else if (lastScope is ProjectReferencesScope)
                    {
                        PopScope(scopeStack);
                    }
                    else if (ExitScope(lastScope))
                    {
                        PopScope(scopeStack);
                    }
                    else
                    {
                        Error("Makefile: Exiting unknown scope");
                    }
                    continue;
                }

                // Directory
                if (MatchDirectory(s, ref str))
                {
                    if (Path.IsPathRooted(str))
                    {
                        workingDirectory = str;
                    }
                    else
                    {
                        workingDirectory = Path.Combine(workingDirectory, str);
                    }
                    curDir = NewDirectory(workingDirectory, scopeStack, curDir);
                    continue;
                }

                // Folder - virtual folder in project
                //  - total headache if this is nested with further directories!? remains to be seen. - this is the case with PS3 - see physics frags - should be accounted for now.
                if (MatchFolder(s, ref str))
                {
                    string fullPath = curDir.IsVirtual? Path.Combine(curDir.Path, str) : Path.Combine(workingDirectory, str);
                    curDir = NewFolder(fullPath, scopeStack, curDir);
                    continue;
                }

                // Mmmm cant switch on class.
                // Add rule
                if (lastScope is RulesScope || lastScope is ExporterRulesScope || lastScope is ConditionScope)
                {
                    if (ParseRulesAndConditionScope(scopeStack, ref action, ref conditions, ref exporterConditions, s))
                        continue;
                }
                // Add rule for an action scope                
                else if (lastScope is ActionScope)
                {
                    if (ParseActionScope(action, s))
                        continue;
                }
                // Add custom details for a custom scope                
                else if (IsInScope(scopeStack, typeof(CustomScope)))
                {
                    if (MatchFolder(s, ref str))
                    {
                        string fullPath = Path.Combine(workingDirectory, str);
                        curDir = NewFolder(fullPath, scopeStack, curDir);
                        continue;
                    }

                    if (MatchCustomFilenameScope(s, ref str))
                    {
                        PushScope(new CustomFileScope(), scopeStack);
                        customFile = new Independent.ProjectFile(Path.Combine(workingDirectory,str));
                        curDir.AddCustomFile(customFile);
                        continue;
                    }
                    
                    if (MatchPlatform(s, ref str))
                    {
                        customFilePlatform = PlatformExtensions.Create(Log, str);
                        customCommand = new Independent.CustomCommand();
                        customFile.CustomCommand.Add(customFilePlatform, customCommand);                        
                        PushScope(new CustomFilePlatformScope(), scopeStack);
                        continue;
                    }

                    if (MatchCustomFileCommand(s, ref str))
                    {
                        customCommand.Command = str;                        
                        continue;
                    }
                        
                    if (MatchCustomFileDescription(s, ref str))
                    {
                        customCommand.Description = str;
                        continue;
                    }
                        
                    if (MatchCustomFileDependencies(s, ref str))
                    {
                        customCommand.Dependencies = str;
                        continue;
                    }
                        
                    if (MatchCustomFileOutputs(s, ref str))
                    {
                        customCommand.Outputs = str;
                        continue;
                    }

                    if (MatchCustomFilename(s, ref str))
                    {
                        customFile = new Independent.ProjectFile(Path.Combine(workingDirectory, str));
                        curDir.AddCustomFile(customFile);
                        continue;
                    }

                    Error("Error : During the CustomScope a line was not parsed.");
                    continue;
                }
                // Add exporter
                else if (lastScope is ExportersScope)
                {
                    if (ParseExporterScope(makefileFilename, s))
                        continue;
                }
                // Add exporter
                else if (lastScope is LibrariesScope)
                {
                    if (ParseLibrariesScope(makefileFilename, s))
                        continue;
                }
                // Add parser file
                else if (lastScope is ParserScope)
                {
                    if (MatchFilename(s, ref str))
                    {
                        curDir.AddParserFile(new Independent.ProjectFile(Path.Combine(workingDirectory, str)));
                        continue;
                    }
                }
                // inside Embedded lib platform scope
                else if (lastScope is EmbeddedLibPlatformScope)
                {
                    if (MatchBuildConfig(s, ref str))
                    {
                        AddEmbeddedLibPlatform(lastScope as EmbeddedLibPlatformScope, str);
                        continue;
                    }
                }
                // inside references scope
                else if (lastScope is ReferencesScope)
                {
                    if (MatchReference(s, ref str))
                    {
                        AddReference(str);
                        continue;
                    }
                }
                // inside references scope
                else if (lastScope is ProjectReferencesScope)
                {
                    if (MatchProjectReferenceNoCopyLocal(s, ref str))
                    {
                        AddProjectReference(str, false);
                        continue;
                    }

                    if (MatchProjectReference(s, ref str))
                    {
                        AddProjectReference(str);
                        continue;
                    }
                }
                else if (IsInScope(scopeStack, typeof(SolutionScope)))
                {
                    // probably due to using makefile Include keyword
                    string tempDir = String.Empty;
                    if (MatchDirectory(s, ref tempDir))
                    {
                        continue; // Ignore direcotry
                    }

                    if (IsInScope(scopeStack, typeof(SolutionProjectsScope)))
                    {
                        if (IsInScope(scopeStack, typeof(SolutionProjectScope)))
                        {
                            if (lastScope is SolutionProjectDepsScope)
                            {
                                Independent.SolutionProject solProject = new Independent.SolutionProject(Log);
                                if (MatchSolutionProject(s, ref solProject))
                                {
                                    solProject.ExpandVariables(Project);                                    

                                    // Add dependency AND a project ( just a shorter hand way of setting up solutions )
                                    currSolutionProject.DependencyNames.Add(solProject.Name);

                                    if (!Solution.Projects.ContainsKey(solProject.Name))
                                    {
                                        Solution.Projects.Add(solProject.Name, solProject);
                                        continue;
                                    }
                                    else
                                    {
                                        //Log.Warning("Solution Project specified twice, it is only neccessary to do so once. {0}", solProject.Name);
                                    }

                                    continue;
                                }
                                else if (MatchSolutionProjectDep(s, ref str))
                                {
                                    currSolutionProject.DependencyNames.Add(str);
                                    continue;
                                }
                            }
                        }
                        else
                        {
                            Independent.SolutionProject solProject = new Independent.SolutionProject(Log);
                            if (MatchSolutionProject(s, ref solProject))
                            {
                                solProject.ExpandVariables(Project);

                                if (!Solution.Projects.ContainsKey(solProject.Name))
                                {
                                    Solution.Projects.Add(solProject.Name, solProject);
                                    continue;
                                }
                                else
                                {
                                    Log.Error("Solution Project added twice");
                                }
                            }
                        }
                    }

                    if (lastScope is SolutionConfigsScope)
                    {
                        if (MatchSolutionConfig(s, ref str))
                        {
                            Solution.Configs.Add(str);
                            continue;
                        }
                    }

                    if (lastScope is SolutionPlatformsScope)
                    {
                        if (MatchSolutionPlatform(s, ref str))
                        {
                            Solution.Platforms.Add(str);
                            continue;
                        }
                    }

                    if (lastScope is SolutionProjectConfigRulesScope)
                    {
                        KeyValuePair<string, Regex> projectsToMatch;
                        if (MatchSolutionRegexRule(s, out projectsToMatch))
                        {
                            SolutionRules.ProjectConfig.Add(projectsToMatch);
                            continue;
                        }
                    }

                    if (lastScope is SolutionBuildRulesScope)
                    {
                        KeyValuePair<bool, Regex> projectsToMatch;
                        if (MatchSolutionBoolRegexRule(s, out projectsToMatch))
                        {
                            SolutionRules.Build.Add(projectsToMatch);
                            continue;
                        }
                    }

                    if (lastScope is SolutionDeployRulesScope)
                    {
                        KeyValuePair<bool, Regex> projectsToMatch;
                        if (MatchSolutionBoolRegexRule(s, out projectsToMatch))
                        {
                            SolutionRules.Deploy.Add(projectsToMatch);
                            continue;
                        }
                    }
                }

                // This happens last because the matching of a filename isn't very strong and *may* not be differentiated with other settings
                // This is because the current makefile format is pretty loose where & when filenames can be added in the makefile.

                // Add source file
                if (MatchFilename(s, ref str))
                {
                    string pch = String.Empty;

                    if (System.IO.Path.GetFileNameWithoutExtension(str) == PCH_FILENAME &&
                        System.IO.Path.GetExtension(str) == PCH_FILETYPE)
                    {
                        pch = PCH_CREATE;
                        precompiledHeadersInUse = true;
                    }
                    else if (precompiledHeadersInUse)
                    {
                        pch = PCH_USE;
                    }

                    Independent.ProjectFile file = new Independent.ProjectFile(Path.Combine(GetCurrentDirectory(scopeStack), str), pch);
                    if (Path.GetExtension(file.Path) == null || Path.GetExtension(file.Path) == "")
                    {
                        Warning("Filename with no extension {0}", str);
                    }
                    else
                    {
                        if (TRACE_SOURCE_FILENAMES) { Log.Message("Filename {0}", file.Path); }
                    }

                    WarnOnNonExistentFile(file);

                    if (Project.Directories.Count()==0) // fixes stlport missing a RootDirectory in it's makefile
                    {
                        Project.Directories.Add(curDir);
                    }
                    curDir.Add(file);
                    continue;
                }

                if (s.Contains("\""))
                {
                    Warning("Makefile: {0} : Not parsed it contains a quote - did you mean for this, the makefile format only supports single quotes.", s);
                }
                else
                {
                    Warning("Makefile: {0} : Not parsed", s);
                }
            }

            if (matchedRootDirectory)
            {
                PopScope(scopeStack);
            }

            String ext = Path.GetExtension(makefileFilename);
            if (!ext.Contains("slndef"))
            {
                AddProjectGeneratorFiles(makefileFilename, workingDirectory);
            }            
        }

        /// <summary>
        /// Adds project generator files into a virtual directory in the project
        /// </summary>
        /// <param name="makefileFilename"></param>
        /// <param name="workingDirectory"></param>
        private void AddProjectGeneratorFiles(String makefileFilename, String workingDirectory)
        {            
            // add the makefile filename into the files in the project 
            ProjectDirectory progGenDir = Project.Directories[0].Directories.Where(d => d.Path == PROJGEN_FOLDER).FirstOrDefault();

            if (progGenDir == default(ProjectDirectory))
            {
                progGenDir = new ProjectDirectory(PROJGEN_FOLDER, true);
                Project.Directories[0].Directories.Add(progGenDir);
            }

            String projectGeneratorFilename;
            FileType fileType = FileTypeExtensions.Create(Log, Path.GetExtension(makefileFilename));

            if (fileType == FileType.Txt || fileType == FileType.Makefile)
            {
                projectGeneratorFilename = makefileFilename;
            }
            else if (fileType == FileType.Build)
            {
                projectGeneratorFilename = Path.Combine(workingDirectory, makefileFilename);
            }
            else
            {
                projectGeneratorFilename = makefileFilename;
                Log.ErrorCtx("Usual makefile filename {0}", makefileFilename);
            }
            progGenDir.Add(new ProjectFile(projectGeneratorFilename));
        }

        private void AddProjectReference(String path, bool copyLocal = true)
        {
            foreach (Independent.Target target in Project.Targets)
            {
                ProjectReference projRef = new ProjectReference();
                projRef.Path = path;
                projRef.CopyLocal = copyLocal;
                target.Settings.General.ProjectReferences.Add(projRef);
            }
        }

        private void AddReference(string str)
        {
            foreach (Independent.Target target in Project.Targets)
            {
                target.Settings.General.References.Add(str);
            }
        }

        private bool MatchProject(string s)
        {
            string str = String.Empty;
            // Project
            if (MatchKeyVal(s, MAKEFILE_PROJECT, ref str))
            {
                Project.Name = str;
                return true;
            }
            return false;
        }

        private bool MatchEmbeddedLibPlatformScope(LinkedList<Object> scopeStack, string s, out EmbeddedLibPlatformScope embeddedLibPlatformScope)
        {
            // Embedded Lib scope 
            if (MatchEmbeddedLibPlatformScope(s, out embeddedLibPlatformScope))
            {
                PushScope(embeddedLibPlatformScope, scopeStack);
                return true;
            }
            return false;
        }

        private bool MatchIncludePath(string s)
        {
            // Include Path
            string str = String.Empty;
            if (MatchKeyVal(s, MAKEFILE_INCLUDE_PATH, ref str))
            {
                AddIncludePath(str);
                return true;
            }
            return false;
        }

        private bool MatchForceInclude(string s)
        {
            // Force Include
            string str = String.Empty;
            if (MatchKeyVal(s, MAKEFILE_FORCE_INCLUDE, ref str))
            {
                AddForceInclude(str);
                return true;
            }
            return false;
        }

        private bool MatchAdditionalSections(string s)
        {
            string str = String.Empty;
            if (MatchKeyVal(s, MAKEFILE_ADDITIONAL_SECTIONS, ref str))
            {
                SetAdditionalSections(str);
                return true;
            }
            return false;
        }

        private bool MatchTitleId(string s)
        {
            string str = String.Empty;
            if (MatchKeyVal(s, MAKEFILE_TITLEID, ref str))
            {
                SetTitleId(str);
                return true;
            }
            return false;
        }

        private bool MatchProjectType(string s)
        {
            // Project Type       
            string str = String.Empty;
            if (MatchKeyVal(s, MAKEFILE_PROJECTTYPE, ref str))
            {
                SetProjectType(str);
                return true;
            }
            return false;
        }

        private bool MatchFrameworkVersion(string s)
        {
            // Framework version     
            string str = String.Empty;
            if (MatchKeyVal(s, MAKEFILE_FRAMEWORK_VERSION, ref str))
            {
                SetFrameworkVersion(str);
                return true;
            }
            return false;
        }

        private bool MatchPlatformToolset(string s)
        {
            // Framework version     
            string str = String.Empty;
            if (MatchKeyVal(s, MAKEFILE_PLATFORM_TOOLSET, ref str))
            {
                SetPlatformToolset(str);
                return true;
            }
            return false;
        }

        private bool MatchOutputPath(string s)
        {
            // Output directory    
            string str = String.Empty;
            if (MatchKeyVal(s, MAKEFILE_OUTPUT_PATH, ref str))
            {
                SetOutputPath(str);
                return true;
            }
            return false;
        }
        

        private bool MatchDefine(string s)
        {
            // Define
            KeyValuePair<string, string> define;
            if (MatchDefine(s, out define))
            {
                AddDefine(define);
                return true;
            }
            return false;
        }

        private bool MatchEmbeddedLibPlatform(string s)
        {
            // Embedded Lib
            KeyValuePair<string, string> embeddedLibPlatform;
            if (MatchEmbeddedLibPlatform(s, out embeddedLibPlatform))
            {
                AddEmbeddedLibPlatform(embeddedLibPlatform);
                return true;
            }
            return false;
        }

        private bool MatchIncludePathRuled(string s)
        {
            // Include Path (ruled)
            KeyValuePair<string, string> includePathRuled;
            if (MatchIncludePathRuled(s, out includePathRuled))
            {
                AddIncludePathRuled(includePathRuled);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Search through the scope stack to find the real working directory ( not a virtual folder )
        /// </summary>
        /// <param name="scopeStack"></param>
        /// <returns></returns>
        private string GetCurrentDirectory(LinkedList<Object> scopeStack)
        {
            if (scopeStack.Count > 0)
            {
                // Get the last directory an set this as the current directory object - so that files may be added to it.
                foreach (Object o in scopeStack.Reverse())
                {
                    if (o is Independent.ProjectDirectory && !((Independent.ProjectDirectory)o).IsVirtual)
                    {
                        return ((Independent.ProjectDirectory)o).Path;
                    }
                }
            }
            
            Error("The currenty directory is not known");
            return Directory.GetCurrentDirectory();
        }

        #region Adding recognised settings to independent format
        private void SetProjectType(string projectType)
        {
            foreach (Independent.Target target in Project.Targets)
            {
                target.ProjectType = new Independent.ProjectType(Log, projectType);
            }
        }

        private void SetFrameworkVersion(string frameworkVersion)
        {
            foreach (Independent.Target target in Project.Targets)
            {
                target.Settings.General.DotNetFrameworkVersion = frameworkVersion;
            }
        }

        private void SetOutputPath(string outputDirectory)
        {
            foreach (Independent.Target target in Project.Targets)
            {
                target.Settings.General.OutputDirectoryName = outputDirectory;
            }
        }

        private void SetPlatformToolset(string platformToolset)
        {
            foreach (Independent.Target target in Project.Targets)
            {
                target.Settings.General.PlatformToolset = platformToolset;
            }
        }        

        private void SetTitleId(string titleId)
        {
            foreach (Independent.Target target in Project.Targets)
            {
                if (target.Platform == Platform.Xbox360)
                    target.Settings.Image.TitleId = titleId;
            }
        }

        private void SetAdditionalSections(string additionalSections)
        {
            foreach (Independent.Target target in Project.Targets)
            {
                if ( target.Platform == Platform.Xbox360 )
                    target.Settings.Image.AdditionalSections = additionalSections;
            }
        }
        
        private void AddForceInclude(string forceInclude)
        {
            foreach (Independent.Target target in Project.Targets)
            {
                target.Settings.Compiler.ForceIncludes.Insert(0, forceInclude);                
            }
        }

        private void AddIncludePath(string includePath)
        {
            foreach (Independent.Target target in Project.Targets)
            {
                target.Settings.Compiler.AdditionalIncludeDirectories.Insert(0,includePath);
            }
        }

        private void AddDefine(KeyValuePair<string, string> define)
        {
            foreach (Independent.Target target in Project.Targets)
            {
                if (define.Value != null)
                {
                    target.Settings.Compiler.PreprocessorDefinitions.Add(define.Key + "=" + define.Value);
                }
                else
                {
                    target.Settings.Compiler.PreprocessorDefinitions.Add(define.Key);
                }
            }
        }

        private void AddEmbeddedLibPlatform(KeyValuePair<string, string> embeddedLibPlatform, string config = null)
        {
            string platform = embeddedLibPlatform.Key;
            string lib = embeddedLibPlatform.Value;

            bool processed = false;

            foreach (Independent.Target target in Project.Targets)
            {                
                if ( (platform == PLATFORM_ALL) || target.Platform.MakefilePlatformSuffix() == platform)
                {
                    if (config != null && !string.Equals(config, target.Config, StringComparison.CurrentCultureIgnoreCase))
                    {
                        continue;
                    }

                    // For executables add the dependency to the linker - otherwise to the librarian
                    if (target.ProjectType.Standard == Independent.ProjectType.StandardValues.Exe)
                    {
                        target.Settings.Linker.AdditionalDependencies.Add(lib);
                    }
                    else
                    {
                        target.Settings.Librarian.AdditionalDependencies.Add(lib);
                    }
                    processed = true;
                }
            }

            if (!processed)
            {
                Warning("MAKEFILE : Embedded Lib platform did not get used in any target : \n\t{0} : {1} ", config, embeddedLibPlatform);
            }
        }

        private void AddIncludePathRuled(KeyValuePair<string, string> includePath, string config = null)
        {
            string condition = string.Format("[{0}]+='{1}'", includePath.Key, includePath.Value);

            // the makefile setting is now added as a rule to the END of the rules list ( that should be ok in most scenarios )
            if (!ParseActionScope("Settings.General.IncludePath", condition))
            {
                Warning("MAKEFILE : Include Path (with rule) issue? : \n\t{0} : {1} {2} ", config, includePath.Key, includePath.Value);
            }
        }

        private void AddEmbeddedLibPlatform(EmbeddedLibPlatformScope scope, string config)
        {
            AddEmbeddedLibPlatform(scope.EmbeddedLibPlatform, config);
        }
        
        #endregion // Adding recognised settings to independent format

        #region Scope Sensitive Parsing

        //
        // parsing extension for discovering new scopes
        //
        private bool ParseNewScopes(LinkedList<Object> scopeStack, ref string newPlatform, ref string platform, ref Independent.SolutionProject solutionProject,  string s)
        {
            string platformName = String.Empty;
            string solutionName = String.Empty;

            if (MatchFiles(s))
            {
                PushScope(new FileScope(), scopeStack);
            }
            else if (MatchReferences(s))
            {
                PushScope(new ReferencesScope(), scopeStack);
            }
            else if (MatchProjectReferences(s))
            {
                PushScope(new ProjectReferencesScope(), scopeStack);
            }
            else if (MatchParse(s))
            {
                PushScope(new ParserScope(), scopeStack);
            }
            else if (MatchMakefile(s))
            {
                PushScope(new MakefileScope(), scopeStack);
            }
            else if (MatchExporters(s))
            {
                PushScope(new ExportersScope(), scopeStack);
            }
            else if (MatchNewPlatform(s, ref platformName))
            {
                PushScope(new NewPlatformScope(), scopeStack);
                newPlatform = platformName;
            }
/*            else if (MatchPlatform(s, ref platformName))
            {
                scopeStack.AddLast(new PlatformScope());
                platform = platformName;
            }
 * */
            else if (MatchCustom(s))
            {
                PushScope(new CustomScope(), scopeStack);
            }
            else if (MatchRules(s))
            {
                PushScope(new RulesScope(), scopeStack);
            }               
            else if (MatchDirectories(s))
            {
                PushScope(new DirectoriesScope(), scopeStack);
                scopeStack.AddLast(new DirectoriesScope());
            }
            else if (MatchLibraries(s))
            {
                PushScope(new LibrariesScope(), scopeStack);
            }            
            else if (MatchSolutionScope(s, ref solutionName))
            {
                Solution.Name = solutionName;
                Solution.ExpandVariables(Project);
                PushScope(new SolutionScope(), scopeStack);
            }
            else if (IsInScope(scopeStack, typeof(SolutionScope)))
            {
                string tempDir = String.Empty;
                if (MatchDirectory(s, ref tempDir)) //  Adirectory creted as a result of Include makefile keyword
                {
                    PushScope(new DirectoriesScope(), scopeStack);
                }
                else
                {
                    if (MatchSolutionProjectsScope(s))
                    {
                        PushScope(new SolutionProjectsScope(), scopeStack);
                    }
                    else if (MatchSolutionProjectScope(s,ref solutionProject))
                    {
                        solutionProject.ExpandVariables(Project);

                        if (!Solution.Projects.ContainsKey(solutionProject.Name))
                        {
                            Solution.Projects.Add(solutionProject.Name, solutionProject);
                        }
                        else
                        {
                            Log.Error("Solution Project added twice {0}", solutionProject.Name);
                        }
                        PushScope(new SolutionProjectScope(), scopeStack);
                    }
                    else if (MatchSolutionProjectDepsScope(s))
                    {
                        PushScope(new SolutionProjectDepsScope(), scopeStack);
                    }
                    else if (MatchSolutionPlatformsScope(s))
                    {
                        PushScope(new SolutionPlatformsScope(), scopeStack);
                    }
                    else if (MatchSolutionConfigsScope(s))
                    {
                        PushScope(new SolutionConfigsScope(), scopeStack);
                    }
                    else if (MatchSolutionProjectConfigRulesScope(s))
                    {
                        PushScope(new SolutionProjectConfigRulesScope(), scopeStack);
                    }
                    else if (MatchSolutionBuildRulesScope(s))
                    {
                        PushScope(new SolutionBuildRulesScope(), scopeStack);
                    }
                    else if (MatchSolutionDeployRulesScope(s))
                    {
                        PushScope(new SolutionDeployRulesScope(), scopeStack);
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            else
            {
                return false;
            }

            return true;
        }

        // whilst in the scope of rules or conditions - perform parsing.
        private bool ParseRulesAndConditionScope(LinkedList<Object> scopeStack, ref string action, ref string conditions, ref string exporterConditions, string s)
        {
            // Is it a FULL rule of some sort?
            MakeRule rule = MatchRule(s);
            if (rule == null)
            {
                // Rule with no conditon?
                rule = MatchUnconditionalRule(s);
                
                if (rule == null)
                {
                    // Is it an Action?
                    if (MatchAction(s, ref action))
                    {
                        PushScope(new ActionScope(), scopeStack);
                        return true;
                    }

                    // Is it a condition?
                    string newConditions = MatchCondition(s);
                    if (newConditions != null)
                    {
                        PushScope(new ConditionScope(), scopeStack);
                        conditions += newConditions;
                        return true;
                    }
                }
            }

            // We have a rule now, add it.
            if (rule != null)
            {
                rule.ConditionGroup.AddConditions(conditions);
                rule.ConditionGroup.AddConditions(exporterConditions);
                if (exporterConditions != String.Empty)
                {
                    ExporterRules.Add(rule);
                }
                else
                {
                    Rules.Add(rule);
                }
                return true;
            }

            return false;
        }        

        private bool ParseExporterScope(string makefileFilename, string s)
        {
            string exporterName = String.Empty;
            if (MatchExporter(s, ref exporterName))
            {
                Exporter exporter = ExporterExtensions.Create(Log, s);

                switch (exporter)
                {
                    case Exporter.VS2010:
                        Exporters.Add(new Exporters.VS2010ProjectExporter(Log, Options, Mangle, false));
                        break;
                    case Exporter.VS2010_UNITY:
                        Exporters.Add(new Exporters.VS2010ProjectExporter(Log, Options, Mangle, true));
                        break;
                    case Exporter.VS2010_SLN:
                        Exporters.Add(new Exporters.VS2010SolutionExporter(Log, Options, Mangle, false));
                        break;
                    case Exporter.VS2010_SLN_UNITY:
                        Exporters.Add(new Exporters.VS2010SolutionExporter(Log, Options, Mangle, true));
                        break;
                    case Exporter.VS2012:
                        Exporters.Add(new Exporters.VS2012ProjectExporter(Log, Options, Mangle, false));
                        break;
                    case Exporter.VS2012_UNITY:
                        Exporters.Add(new Exporters.VS2012ProjectExporter(Log, Options, Mangle, true));
                        break;
                    case Exporter.VS2012_SLN:
                        Exporters.Add(new Exporters.VS2012SolutionExporter(Log, Options, Mangle, false));
                        break;
                    case Exporter.VS2012_SLN_UNITY:
                        Exporters.Add(new Exporters.VS2012SolutionExporter(Log, Options, Mangle, true));
                        break;
                    default:
                        Error("Error: Exporter name {0} is not recognised in file {1}", exporterName, makefileFilename);
                        break;
                }
                return true;
            }
            return false;
        }

        private bool ParseLibrariesScope(string makefileFilename, string s)
        {
            string libraryName = String.Empty;
            if (MatchLibrary(s, ref libraryName))
            {
                foreach (Independent.Target target in Project.Targets)
                {
                    target.Settings.Linker.Libraries.Add(libraryName);
                }
                return true;
            }
            return false;
        }

        private bool ParseActionScope(string action, string s)
        {
            MakeRule rule = MatchActionlessRule(s, action);

            if (rule == null)
            {
                rule = MatchAssignmentRule(s, action);
            }

            if (rule != null)
            {
                Rules.Add(rule);
                return true;
            }

            return false;
        }

        #endregion // scope sensitive parsing        

        #region directory handling
        private Independent.ProjectDirectory PushDirectory(LinkedList<object> scopeStack, string directoryPath, bool isFolder = false)
        {
            Independent.ProjectDirectory newDir = new Independent.ProjectDirectory(directoryPath, isFolder);
            PushScope(newDir, scopeStack);
            return newDir;
        }

        private Independent.ProjectDirectory PopDirectory(LinkedList<object> scope)
        {
            PopScope(scope);
            if (scope.Count > 0)
            {
                // Get the last directory an set this as the current directory object - so that files may be added to it.
                foreach (Object o in scope.Reverse())
                    if (o is Independent.ProjectDirectory)
                        return o as Independent.ProjectDirectory;

                //Log.Error("Makefile: Directory pop failed.");
            }

            return null;
        }

        private Independent.ProjectDirectory NewDirectory(string dir, LinkedList<object> scope, Independent.ProjectDirectory curDir)
        {
            Independent.ProjectDirectory newDir = PushDirectory(scope, dir);
            if (curDir != null)
            {
                curDir.Add(newDir);
            }
            return newDir;
        }

        private Independent.ProjectDirectory NewFolder(string folder, LinkedList<object> scope, Independent.ProjectDirectory curDir)
        {
            Independent.ProjectDirectory newFolder = PushDirectory(scope, folder, true);
            if (curDir != null)
            {
                curDir.Add(newFolder);
            }
            return newFolder;
        }
        #endregion // directory handling

        #region Private Classes
        // Nested empty class objects - for context stack
        // Objects are placed on a stack to define the scope when parsing a makefile
        // These empty classes used define a context/scope for which there is no meaningful associated data
        // These can be interspersed with objects that hold data hence the approach.
        private interface IPoppableScope { } // property for each of these classes for the scope stack.
        private class ParserScope : IPoppableScope { }
        private class FileScope : IPoppableScope { }
        private class MakefileScope : IPoppableScope { }
        private class PlatformScope : IPoppableScope { }
        private class NewPlatformScope : IPoppableScope { }
        private class RulesScope : IPoppableScope { }
        private class ExporterRulesScope : IPoppableScope { }
        private class ActionScope : IPoppableScope { }
        private class ConditionScope : IPoppableScope { }
        private class ExportersScope : IPoppableScope { }
        private class CustomScope : IPoppableScope { }
        private class CustomFileScope : IPoppableScope { }
        private class CustomFilePlatformScope : IPoppableScope { }
        private class DirectoriesScope : IPoppableScope { }
        private class LibrariesScope : IPoppableScope { }
        private class ProjectScope : IPoppableScope { }
        private class SolutionScope : IPoppableScope { }
        private class SolutionProjectsScope : IPoppableScope { }
        private class SolutionProjectScope : IPoppableScope { }
        private class SolutionProjectDepsScope : IPoppableScope { }
        private class SolutionPlatformsScope : IPoppableScope { }
        private class SolutionConfigsScope : IPoppableScope { }
        private class SolutionProjectConfigRulesScope : IPoppableScope { }
        private class SolutionBuildRulesScope : IPoppableScope { }
        private class SolutionDeployRulesScope : IPoppableScope { }
        private class ReferencesScope : IPoppableScope { }
        private class ProjectReferencesScope : IPoppableScope { } 

        #endregion // Private Classes

        #region Makefile Documentation
        /// <summary>
        /// https://devstar.rockstargames.com/wiki/index.php/Editing_Makefiles
        ///'#'Comments (supported)         
        ///     Comments are marked by the '#' symbol. Anything after this symbol will be ignored by the parser. Block comments are not supported.  
        ///
        ///Project  (supported)            
        ///     The name of the project that will be created. If there is no project name, then the system will assume that no project will be generated.   
        ///
        ///Template  (not to be supported)           
        ///     This is a file name rooted within %RS_TOOLSROOT%\etc\projbuild that designates the configurations to be created within the project. 
        ///     This is used to create a wide variety of configurations for very specific uses. 
        ///     For example, the MotionBuilder plug-ins use their own configuration file, which allows us to better support new versions of MotionBuilder as quickly deprecating old ones. 
        ///     Example: Template motionbuilder_configurations.xml  
        ///
        ///Files { ... } (supported)       
        ///     A grouping to list all of the files to be listed within the project.  
        ///
        ///Include (supported, but needs more attention)             
        ///     The Include keyword may only be used within the Files directive. 
        ///     Users may specify a directory (relative to the root directory) or a specific project definition file that will be processed to include all files. 
        ///     All other information such as IncludePath and Library includes will be ignored. 
        ///     If no project definition file is specified directly, "makefile.txt" will be appended to the directory name.  
        ///
        ///Custom { ... }  (supported)     
        ///     A grouping to list all of the files with custom steps in the project. 
        ///     The type of custom steps have deduced by the extension of the file, such as .spu, .frag and .geo. 
        ///     Within this list can be full custom build steps on a per-file basis.
        ///
        ///Parse{ ... }   (supported)      
        ///     A grouping to list all of the files with parser custom steps in the project.  
        ///
        ///EmbeddedLibAll (supported)      
        ///     Add library dependencies to the all platforms in the solution. Used for non-platform-specific libraries (e.g. 3D Studio Max libraries).  
        //
        ///EmbeddedLibWin32 (supported)     
        ///     Add library dependencies to the Win32 platform.  
        ///
        ///EmbeddedLibWin64 (supported)   
        ///     Add library dependencies to the Win64 platform.  
        ///
        ///EmbeddedLibPsn (supported)      
        ///     Add library dependencies to the PS3 platform.  
        ///
        ///EmbeddedLibXenon (supported)    
        ///     Add library dependencies to the Xenon platform.  
        ///
        ///EmbeddedLib* { ... } 
        ///     The above EmbeddedLibPlatformName keywords can have an associated bracket. 
        ///     This bracket contains a list of configurations that will include this library (e.g. Debug, ToolRelease, Beta, BankRelease, RscBeta, etc.).
        ///     This is how libraries can be specified on a per-platform, per-configuration basis. 
        ///     Multiple configurations can be specified within the brackets.  
        ///
        ///LibraryDirectory         
        ///     Add additional library directories to the all platforms in the solution.  
        ///
        ///LibraryDirectoryWin32    
        ///     Add additional library directories to the Win32 platform.  
        ///
        ///LibraryDirectoryWin64    
        ///     Add additional library directories to the Win64 platform.  
        ///
        ///LibraryDirectoryPsn      
        ///     Add additional library directories to the PS3 platform.  
        ///
        ///LibraryDirectoryXenon    
        ///     Add additional library directories to the Xenon platform.  
        ///
        ///LibraryDirectory* { ... }  
        ///     The above LibraryDirectoryPlatformName keywords can have an associated bracket. This bracket contains a list of configurations that will include this additional library directory (e.g. Debug, ToolRelease, Beta, BankRelease, RscBeta, etc.). This is how library directories can be specified on a per-platform, per-configuration basis. Multiple configurations can be specified within the brackets.  
        ///
        ///IncludePath  (supported)
        ///     Accepted a single path or a set of semicolon-adjoined paths to be added to the AdditionalIncludeDirectories attribute of every configuration. 
        ///     Multiple IncludePath attributes can be in the same file. Using $(RAGE_DIR), $(RS_TOOLSROOT) and other environment variables is encouraged. 
        ///     Using full paths is mandated. Example: IncludePath $(RAGE_DIR)\base\tools\dcc\motionbuilder  
        ///
        ///IncludePathWin32, IncludePathWin64, IncludePathPsn, IncludePathXenon  
        ///     Platform-specific identifiers for include paths.  
        ///
        ///ForceInclude  (supported)
        ///     Accepted a single path or a set of semicolon-adjoined paths to be added under the ForceInclude attribute of every configuration.  
        ///
        ///Define (supported)
        ///     Accepted a single preprocessor definition or a set of semicolon-adjoined definitions to be added under the Preprocessor Definitions attribute of every configuration. Multiple Define commands cna be added to this file.  
        ///
        ///RootDirectory (supported)
        ///     A working root directory from which to read all other projects.  
        ///
        ///Folder (supported)
        ///     A virtual directory.
        /// 
        ///Directory (supported - partly)
        ///     A relative or an absolute path to another directory to be included in this project. 
        ///     The generation utility will search for a project definition file in that directory to then process, recursively. 
        ///     This is used to create "megaprojects" which is a collection of smaller projects.  
        ///
        ///ConfigurationType
        ///     An option to set the output type of project. 
        ///     The available options are "dll", "library" and "exe". 
        ///     This is useful for having very similar projects where some are DLLs and others are static libraries. 
        ///     Keep in mind that different Visual Studio tools nodes (e.g. the LinkerTool) does not exist for libraries and the LibrarianTool for the DLLs and executables. 
        ///     One can account for both in the configuration XMLs.  
        ///
        ///OutputFileName 
        ///     In certain cases, the output file name may not be the default of "$(ProjectName).$(OutputType). 
        ///     Instead specialized extensions will be necessary to be added on a per-project basis. 
        ///     For example, 3D Studio Max plug-ins require particular extensions for load order, so an OutputFileName option is employed. 
        ///     This option is intended to act solely as a file name, not a full path, but can by the way the internal logic of the project generation utility works. 
        ///     The outputted file path should be specified on a per-configuration basis. 
        ///
        ///WarningLevel 
        ///     A number from 1 to 4 that designates the project's warning level.  
        ///
        ///WarningsAsErrors 
        ///     For most configurations, this is set to "true". Set to "true" or "false" to turn warnings as errors on or off, respectively.  
        ///
        ///ProjectReference 
        ///     [Used by Visual Studio 2010 generator] 
        ///     Either a relative or absolute path to a project name to be included as a Project Reference in the project.  
        ///</summary>
        ///

        #region Makefile file format example
        /// <summary>
        ///  Makefile file format examples
        /// 
        /// Project game1_lib
        ///
        /// RootDirectory ..
        ///
        /// IncludePath $(ProjectDir)\..
        /// IncludePath $(RAGE_DIR)\base\src
        ///
        /// ForceInclude basetypes.h
        /// ForceInclude game_config.h
        ///
        /// Files {
        ///     Include ai/makefile.txt
        ///     Include animation/makefile.txt
        /// }
        ///
        /// Project ai 
        ///
        /// Files {
        ///    Directory ambient {
        ///        AmbientModelSet.cpp
        ///        AmbientModelSet.h
        ///        Parse {
        ///	        AmbientModelSet
        ///        }
        ///    }
        ///    AITarget.cpp
        /// } 
        /// </summary>
        #endregion // Makefile file format example
        #endregion //  makefile documentation
        #endregion // Private Methods
    } // class Makefile
} // namespace ProjectGenerator
