﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace ProjectGenerator.Independent
{
    /// <summary>
    /// All the build settings as used by a target
    /// </summary>
    [Serializable]
    public class Settings
    {
        #region Properties
        /// <summary>
        /// Generalised settings
        /// </summary>
        public General General { get; set; }
        /// <summary>
        /// Debugger settings
        /// </summary>
        public Debugger Debugger { get; set; }
        /// <summary>
        /// compiler settings
        /// </summary>
        public Compiler Compiler { get; set; }
        /// <summary>
        /// Linker settings
        /// </summary>
        public Linker Linker { get; set; }
        /// <summary>
        /// Librarian settings
        /// </summary>
        public Librarian Librarian { get; set; }
        /// <summary>
        /// Build events settings
        /// </summary>
        public BuildEvents BuildEvents { get; set; }
        /// <summary>
        /// Deploy settings
        /// </summary>
        public Deploy Deploy { get; set; }
        /// <summary>
        /// Image settings
        /// </summary>
        public Image Image { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// default constructor
        /// </summary>
        public Settings()
        {
            General = new General();
            Debugger = new Debugger();
            Compiler = new Compiler();
            Linker = new Linker();
            Librarian = new Librarian();
            BuildEvents = new BuildEvents();
            Deploy = new Deploy();
            Image = new Image();
        }
        #endregion // Constructor(s)
    } // class Settings
} // namespace ProjectGenerator.Independent
