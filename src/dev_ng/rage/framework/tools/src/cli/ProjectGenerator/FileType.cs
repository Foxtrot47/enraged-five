﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace ProjectGenerator
{
    #region FileType enum
    /// <summary>
    /// Enum of FileTypes
    /// <remarks>replace with a general tools filetype once I know where it is?</remarks>
    /// </summary>
    public enum FileType
    {
        [FileTypeStringAttribute(".h")]
        H,
        [FileTypeStringAttribute(".cpp")]
        Cpp,
        [FileTypeStringAttribute(".c")]
        C,
        [FileTypeStringAttribute(".frag")]
        Frag,
        [FileTypeStringAttribute(".job")]
        Job,
        [FileTypeStringAttribute(".txt")]
        Txt,
        [FileTypeStringAttribute(".pmjob")]
        PmJob,
        [FileTypeStringAttribute(".task")]
        Task,
        [FileTypeStringAttribute(".inl")]
        Inl,
        [FileTypeStringAttribute(".sch")]
        Sch,
        [FileTypeStringAttribute(".psc")]
        Psc,
        [FileTypeStringAttribute(".dtx")]
        Dtx,
        [FileTypeStringAttribute(".spa")]
        Spa,
        [FileTypeStringAttribute(".inc")]
        Inc,
        [FileTypeStringAttribute(".fxh")]
        Fxh,
        [FileTypeStringAttribute(".fx")]
        Fx,
		[FileTypeStringAttribute(".xsd")]
		Xsd,
        [FileTypeStringAttribute("")]
        None, 
        Invalid
    } // enum FileType
    #endregion //FileType enum

    #region FileTypeExtensions
    /// <summary>
    /// Static extensions methods to the FileType enum
    /// <remarks>Extends the enum class so we can reflect on it's attributes.</remarks>
    /// </summary>
    public static class FileTypeExtensions
    {
        /// <summary>
        /// Create a FileType
        /// </summary>
        /// <param name="fileType"></param>
        /// <param name="fileTypeName"></param>
        public static FileType Create(this FileType fileType, string fileTypeName)
        {
            fileTypeName = fileTypeName.TrimStart('.');
            if (fileTypeName == "")
            {
                return FileType.None;
            }

            if (Enum.TryParse(fileTypeName, true, out fileType))
            {
                return fileType;
            }
            else
            {
                Log.Log__Error("Invalid fileType {0}", fileTypeName);
                return FileType.Invalid;
            }
        }

        /// <summary>
        /// Get the string (FileTypeStringAttribute) of a filetype enum
        /// </summary>
        /// <param name="fileType"></param>
        /// <returns></returns>
        public static string ToFileTypeString(this FileType fileType)
        {
            Type type = fileType.GetType();
            FieldInfo fi = type.GetField(fileType.ToString());
            FileTypeStringAttribute[] attributes = fi.GetCustomAttributes(typeof(FileTypeStringAttribute), false) as FileTypeStringAttribute[];
            return (attributes.Length > 0 ? attributes[0].Text : null);
        }

        /// <summary>
        /// Returns true if this enum is of a source file type
        /// </summary>
        /// <param name="fileType"></param>
        /// <returns></returns>
        public static bool IsSourceFile(this FileType fileType)
        {
            return (fileType != FileType.None && fileType != FileType.Invalid);
        }

    } // class FileTypeExtensions
    #endregion // FileTypeExtensions
} // namespace ProjectGenerator
