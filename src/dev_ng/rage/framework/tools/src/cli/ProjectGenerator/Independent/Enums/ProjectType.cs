﻿using System;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using System.Collections.Generic;

namespace ProjectGenerator.Independent
{
    /// <summary>
    /// The projectType setting enum
    /// </summary>
    [Serializable]
    public class ProjectType : ICustomEnum, IEnum
    {
        #region Enum StandardValues
        /// <summary>
        /// Enum of Standard ProjectTypes.
        /// </summary>
        public enum StandardValues
        {
            Exe,
            Lib,
            Dll,
            Invalid,
            Custom,
        } // enum StandardValues
        #endregion // Enum StandardValues

        #region Constructors
        /// <summary>Default Constructor - the default is an invalid ProjectType</summary>
        public ProjectType(IUniversalLog log)
        {
            Log = log;
            Standard = StandardValues.Invalid;
        }

        /// <summary>Instantiate an projectType using a string</summary>
        /// <param name="projectType">
        /// A string to translate into an enum if recognised ( through reflection ) ( case insensitive ).
        /// If it not recognised it will allow the custom value to be used directly in export OR translated at export. ( case sensitive )</param>
        /// <param name="isCustom">
        /// Enforces the string used to be a custom setting - despite it potentially being a recognised enum.
        /// </param>
        public ProjectType(IUniversalLog log, string projectType, bool isCustom = false)
        {
            Log = log;
            StandardValues std;
            if (!isCustom && Enum.TryParse(projectType, true, out std))
            {
                Standard = std;
            }
            else
            {
                Custom = projectType;
            }
        }

        /// <summary>Instantiate an ProjectType using a StandardValues enum.</summary>
        /// <param name="projectType">The StandardValues enum. Do not use StandardValues.Custom, instead use another constructor.</param>
        public ProjectType(StandardValues projectType)
        {
            Standard = projectType;
        }
        #endregion Constructors

        #region Public Properties

        /// <summary>The 'standard' enum setting</summary>
        public StandardValues Standard
        {
            get { return m_Standard; }
            set
            {
                if (value != StandardValues.Custom)
                {
                    m_Custom = null;
                }
                else
                {
                    Log.Error("Error: A custom ProjectType should only be set via the Custom property or constructor");
                }
                m_Standard = value;
            }
        }
        private StandardValues m_Standard;

        /// <summary>The custom string - NOT a standard enum.</summary>
        public string Custom
        {
            get { return m_Custom; }
            set
            {
                m_Standard = StandardValues.Custom;
                m_Custom = value;
            }
        }
        private String m_Custom;
        #endregion // Public Properties

        #region Private Properties
        /// <summary>
        /// Caches for (much) quicker reflection
        /// </summary>
        private static Dictionary<StandardValues, string> StandardValuesCache = new Dictionary<StandardValues, string>();

        /// <summary>
        /// Universal log : no choice but to make this static since the SimpleObjectCloner would want to clone this. A difficult decision.
        /// </summary>
        private static IUniversalLog Log { get; set; }
        #endregion // Private Properties

        #region Public Methods
        /// <summary>
        /// Get as a string
        /// </summary>
        /// <returns></returns>
        public string EnumToString()
        {
            if (m_Standard == StandardValues.Custom)
            {
                return m_Custom;
            }

            // Is it Cached?
            if (StandardValuesCache.ContainsKey(Standard))
            {
                return StandardValuesCache[Standard];
            }

            string s = m_Standard.ToString();

            // Cache it for quicker reflection next time.
            if (s != null)
            {
                StandardValuesCache.Add(Standard, s);
            }

            return s;
        }

        /// <summary>
        /// Compare against a string representation of the enum ( case insensitive )
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        public bool Match(string val)
        {
            string currVal = Standard.ToString();
            return (0 == string.Compare(currVal, val, true));
        }

        #endregion // Public Methods
    } // class ProjectType
} // namespace ProjectGenerator.Independent
