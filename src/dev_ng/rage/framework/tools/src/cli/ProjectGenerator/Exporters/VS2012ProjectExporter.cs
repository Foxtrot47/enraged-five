﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Reflection;
using System.Collections;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.Configuration;

namespace ProjectGenerator.Exporters
{
    /// <summary>
    /// Exporter for Visual Studio 2010 Project Files
    /// </summary>
    public sealed class VS2012ProjectExporter :
                        ProjExporter,
                        Independent.IProjectExporter
    {
        #region Public Constants
        // Visual Studio version
        public static readonly string VS_VERSION = "2012";
        #endregion // Public Constants

        #region Constuctor(s)
        /// <summary>
        /// Static constructor
        /// </summary>
        static VS2012ProjectExporter()
        {
        }

        /// <summary>
        /// Parameterised contructor
        /// </summary>
        /// <param name="mangle"></param>
        public VS2012ProjectExporter(IUniversalLog log, CommandOptions options, string mangle, bool unity)
            : base(log, options, mangle, unity)
        {
        }
        #endregion // Constuctor(s)

        #region Public Properties
        /// <summary>
        /// Return the exporter type
        /// (Independent.IProjectExporter)
        /// </summary>
        /// <returns>The Exporter Enum</returns>
        public override Exporter ExporterType
        {
            get { return Unity ? Exporter.VS2012_UNITY : Exporter.VS2012; }
        }
        #endregion // Public Properties

        #region Protected Methods

        /// <summary>
        /// Overrides the abstract method in its superclass
        /// </summary>
        protected override string VsVersion
        {
            get { return VS_VERSION; }
        }

        protected override XmlElement CreateVCTargetsPath(XmlDocument doc, XmlElement element)
        {
            XmlElement vcTargetsPath = AppendElementWithTextNode(doc, element, "VCTargetsPath", "$(VCTargetsPath11)");
            AppendAttribute(doc, vcTargetsPath, "Condition", "'$(VCTargetsPath11)' != '' and '$(VSVersion)' == '' and $(VisualStudioVersion) == ''");
            return vcTargetsPath;
        }

        protected override void SetFileLtcg(XmlDocument doc, Independent.ProjectFile file, XmlElement clCompileElement)
        {
            if (!file.SuitableForLtcg)
            {
                XmlElement elem = AppendElementWithTextNode(doc, clCompileElement, "WholeProgramOptimization", "false");
            }
        }

        //  <ItemGroup>
        //      <AppxManifest Include="Package.appxmanifest" />
        //  </ItemGroup>
        protected override void CreateItemGroupManifests(XmlDocument doc, XmlElement root)
        {
            if (IsExecutableProject())
            {
                // supported in makefile
               // XmlElement element = AppendElement(doc, root, MSBUILD_ITEMGROUP);
               // AppendElementWithAttribute(doc, element, "AppxManifest", "Include", "Package.appxmanifest");
            }
        }

        protected override void CreateRootNamespace(XmlDocument doc, XmlElement element)
        {
            if (IsExecutableProject())
            {
                AppendElementWithTextNode(doc, element, "RootNamespace", "$(SolutionName)");
            }
        }

        protected override void CreateDefaultLanguage(XmlDocument doc, XmlElement element)
        {
            //if (IsExecutableProject())
            {
                AppendElementWithTextNode(doc, element, "DefaultLanguage", "en-US");
            }
        }

        protected override void CreateKeyword(XmlDocument doc, XmlElement element)
        {
            //if (IsExecutableProject())
            {
                AppendElementWithTextNode(doc, element, "Keyword", "Win32Proj");
            }
        }

        protected override void CreateMinimalVisualStudioVersion(XmlDocument doc, XmlElement element)
        {
            if (IsExecutableProject())
            {
                AppendElementWithTextNode(doc, element, "MinimumVisualStudioVersion", "11.0");
            }
        }

        protected override void CreateApplicationEnvironment(XmlDocument doc, XmlElement element)
        {
            //if (IsExecutableProject())
            {
                AppendElementWithTextNode(doc, element, "ApplicationEnvironment", "title");
            }
        }

        protected override void CreateUseDebugLibraries(XmlDocument doc, XmlElement element)
        {
            if (IsExecutableProject())
            {
                AppendElementWithTextNode(doc, element, "UseDebugLibraries", "true");
            }
        }

        protected override void CreateEmbedManifest(XmlDocument doc, XmlElement element)
        {
            if (IsExecutableProject())
            {
                AppendElementWithTextNode(doc, element, "EmbedManifest", "false");
            }
        }

        protected override void CreateGenerateManifest(XmlDocument doc, XmlElement element)
        {
            if (IsExecutableProject())
            {
                AppendElementWithTextNode(doc, element, "GenerateManifest", "false");
            }
        }

        // DW I *think* this is possibly VS2012 specific.
        protected override void CreateMetadataPath(XmlDocument doc, XmlElement element, Independent.Target target)
        {
            AppendElementWithTextNode(doc, element, "LibraryWPath", Join(target.Settings.General.MetadataPath, "$(LibraryWPath)"));
        }     

        #endregion // Protected Methods

        #region Private Methods
        private bool IsExecutableProject()
        {
            return (Project.Targets.FirstOrDefault() != null &&
                    Project.Targets.FirstOrDefault().ProjectType.Standard == Independent.ProjectType.StandardValues.Exe);
        }
        #endregion // Private Methods

    } // class VS2012ProjectExporter
} // namespace ProjectGenerator.Exporters
