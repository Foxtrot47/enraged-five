﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGenerator.Independent
{
    /// <summary>
    /// A project directory
    /// <remarks>A container of further directories and files</remarks>
    /// </summary>
    [Serializable]
    public class ProjectDirectory
    {
        #region Properties
        /// <summary>
        /// The path to the directory
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// the list of files within the directory
        /// </summary>
        public List<ProjectFile> Files { get; set; }
        /// <summary>
        /// The list of directories within this directory
        /// </summary> 
        public List<ProjectDirectory> Directories { get; set; }
        /// <summary>
        /// A 'virtual' flag
        /// - when true it indicates thsi directory does not invoke a change of directory in the filesystem
        /// - rather the directory is a virtual folder for organising further files and directories in the IDE.
        /// </summary>
        public bool IsVirtual { get; set; }
        #endregion // Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor
        /// </summary>
        public ProjectDirectory()
        {
        }

        /// <summary>
        /// Chained constructor that allows setting of directory
        /// </summary>
        /// <param name="workingDirectory"></param>
        public ProjectDirectory(string workingDirectory)
            : this(workingDirectory, false)
        {
        }

        /// <summary>
        /// Constructor that allows setting of directory & virtual flag
        /// </summary>
        /// <param name="workingDirectory"></param>
        /// <param name="isVirtual"></param>
        public ProjectDirectory(string workingDirectory, bool isVirtual)
        {
            Path = workingDirectory;
            Files = new List<ProjectFile>();
            Directories = new List<ProjectDirectory>();
            IsVirtual = isVirtual;
        }

        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Adds a directory into the directory's collection of directories.
        /// </summary>
        /// <param name="dir"></param>
        public void Add(ProjectDirectory dir)
        {
            Directories.Add(dir);
        }

        /// <summary>
        /// Adds a file into the directory's collection of files.
        /// </summary>
        /// <param name="file"></param>
        public void Add(ProjectFile file)
        {
            Files.Add(file);
        }

        /// <summary>
        /// Adds a parser file into the directory's collection of files.
        /// - ensures the parser file flag is set.
        /// </summary>
        /// <param name="file"></param>
        public void AddParserFile(ProjectFile file)
        {
            file.Parse = true;
            Files.Add(file);
        }

        /// <summary>
        /// Adds a custom file into the directory's collection of files.
        /// - ensures the custom flag is set.
        /// </summary>
        /// <param name="file"></param>
        public void AddCustomFile(ProjectFile file)
        {
            file.Custom = true;
            Files.Add(file);
        }

        #endregion Public Methods
    } // class ProjectDirectory
} // namespace ProjectGenerator.Independent
