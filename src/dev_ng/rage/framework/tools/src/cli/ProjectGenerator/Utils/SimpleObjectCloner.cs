﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

namespace ProjectGenerator
{
    /// <summary>
    /// Template method for cloning [Serialisable] objects
    /// THIS HAS THE EFFECT OF A DEEP COPY / CLONE.
    /// writes the object as binary to a memory stream, as binary it will be the fastest 
    /// it then deserialises the object and returns it as a new object.
    /// - It will serialise BOTH public and private properties & generic collections.
    /// NB. metadata about the object is saved in the binary stream.
    /// * Use [NonSerialized] to mark fields for non-serialisation
    /// * Types must be marked with [Serializable] to be serialised.
    /// </summary>
    public static class SimpleObjectCloner
    {
        public static MemoryStream PreSerialise<T>(T objectToClone)
        {
            BinaryFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            formatter.Serialize(stream, objectToClone);
            return stream;
        }

        /// <summary>
        /// Template clone method.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objectToClone"></param>
        /// <param name="preSerialisedStream"></param>
        /// <returns></returns>
        public static T Clone<T>(T objectToClone, MemoryStream preSerialisedStream = null)
        {
            BinaryFormatter formatter = new BinaryFormatter();

            if (preSerialisedStream != null)
            {
                preSerialisedStream.Seek(0, SeekOrigin.Begin);
                T newObj = (T)formatter.Deserialize(preSerialisedStream);
                return newObj;
            }
            else
            {
                using (Stream stream = new MemoryStream())
                {                    
                    formatter.Serialize(stream, objectToClone);
                    stream.Seek(0, SeekOrigin.Begin);
                    T newObj = (T)formatter.Deserialize(stream);
                    return newObj;
                }
            }
        }
    } 
}
