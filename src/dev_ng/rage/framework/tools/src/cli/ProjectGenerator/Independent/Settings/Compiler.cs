﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;


namespace ProjectGenerator.Independent
{
    /// <summary>
    /// Compiler settings
    /// </summary>
    [Serializable]
    public class Compiler : SettingExtender
    {
        #region Properties        
        public List<string> AdditionalIncludeDirectories { get; set; }
        public List<string> AdditionalUsingDirectories { get; set; }
   
        public List<string> PreprocessorDefinitions { get; set; }
        public List<string> ForceIncludes { get; set; }
        public List<string> ForcedUsingFiles { get; set; }
        public List<string> DisableSpecificWarnings { get; set; }
        public List<string> AdditionalOptions { get; set; }
        public string ProgramDatabaseFilename;
        
        public bool GenerateDebugInformation { get; set; }
        public bool FunctionLevelLink { get; set; }        
        public bool FavourSize { get; set; }
        public bool FavourSpeed { get; set; }
        public bool StringPooling { get; set; }
        public bool IntrinsicFunctions { get; set; }
        public bool TreatWarningsAsErrors { get; set; }

        public string FilenamePatternOverride { get; set; }
        public List<string> FileOverride_AdditionalIncludeDirectories { get; set; }
        public bool FileOverride_CompileAsWinRt { get; set; }
        public string FileOverride_ExceptionHandling { get; set; }        

        // DW: added to support CS
        public string DebugSymbols { get; set; }
        public string DebugType  { get; set; }
        public string Optimization  { get; set; }
        public string ErrorReport  { get; set; }
        public string WarningLevel { get; set; }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// default constructor
        /// </summary>
        public Compiler()
        {  
            AdditionalIncludeDirectories = new List<string>();
            PreprocessorDefinitions = new List<string>();
            DisableSpecificWarnings = new List<string>(); 
            ForceIncludes = new List<string>();

            ForcedUsingFiles = new List<string>();
            AdditionalUsingDirectories = new List<string>();

            AdditionalOptions = new List<string>();
            ProgramDatabaseFilename = String.Empty;

            GenerateDebugInformation = false;
            FunctionLevelLink = false;            
            FavourSize = false;
            FavourSpeed = false;
            IntrinsicFunctions = false;
            StringPooling = false;
            TreatWarningsAsErrors = false;

            FilenamePatternOverride = String.Empty;
            FileOverride_AdditionalIncludeDirectories = new List<string>();
            FileOverride_CompileAsWinRt = false;
            FileOverride_ExceptionHandling = String.Empty;

            DebugSymbols = String.Empty;
            DebugType = String.Empty;
            Optimization = String.Empty; 
            ErrorReport = String.Empty;
            WarningLevel = String.Empty;
        }
        #endregion Constructor(s)
    } // class SettingsCompiler
} // namespace ProjectGenerator.Independent
 