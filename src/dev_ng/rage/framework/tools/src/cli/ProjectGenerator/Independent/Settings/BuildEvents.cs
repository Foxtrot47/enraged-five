﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace ProjectGenerator.Independent
{
    /// <summary>
    /// The Build events collection of settings
    /// </summary>
    [Serializable]
    public class BuildEvents
    {
        #region Properties
        /// <summary>
        /// prebuild event settings
        /// </summary>
        public ProjectBuildEvent PreBuild { get; set; }
        /// <summary>
        /// prelink event settings
        /// </summary>
        public ProjectBuildEvent PreLink { get; set; }
        /// <summary>
        /// postbuild event settings
        /// </summary>
        public ProjectBuildEvent PostBuild { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public BuildEvents()
        {
            PreBuild = new ProjectBuildEvent();
            PreLink = new ProjectBuildEvent();
            PostBuild = new ProjectBuildEvent();
        }
        #endregion Constructor(s)
    } // class SettingsBuildEvents
} // namespace ProjectGenerator.Independent
