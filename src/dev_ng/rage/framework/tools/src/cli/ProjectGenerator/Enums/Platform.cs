﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace ProjectGenerator
{
    #region Enum
    /// <summary>
    /// Enum of platforms
    /// <remarks>replace with a general tools platform once I know where it is?</remarks>
    /// </summary>
    public enum Platform
    {
        [MakeFileAttribute("PS3")]
        [MakefilePlatformSuffixAttribute("Psn")]
        Ps3,
        [MakeFileAttribute("ORBIS")]
        [MakefilePlatformSuffixAttribute("Orbis")]
        Orbis,
        [MakeFileAttribute("Xbox 360")]
        [MakefilePlatformSuffixAttribute("Xenon")]
        Xbox360,
        [MakeFileAttribute("Durango")]
        [MakefilePlatformSuffixAttribute("Durango")]
        Durango,
        [MakeFileAttribute("Win32")]
        [MakefilePlatformSuffixAttribute("Win32")]
        Win32,
        [MakeFileAttribute("x86")]
        [MakefilePlatformSuffixAttribute("x86")]
        x86,
        [MakeFileAttribute("AnyCPU")]
        [MakefilePlatformSuffixAttribute("AnyCPU")]
        AnyCPU,
        [MakeFileAttribute("x64")]
        [MakefilePlatformSuffixAttribute("x64")]
        x64,
        [MakeFileAttribute("x64")]
        [MakefilePlatformSuffixAttribute("Win64")]
        Win64,
        [MakeFileAttribute("All")]
        [MakefilePlatformSuffixAttribute("All")]
        All,
        Invalid
    } // enum Exporter
    #endregion // Enum

    #region Extensions
    /// <summary>
    /// Static extensions methods to the Platform enum
    /// <remarks>Extends the enum class so we can reflect on it's attributes.</remarks>
    /// </summary>
    public static class PlatformExtensions
    {
        /// <summary>
        /// Caches for (much) quicker reflection
        /// </summary>
        private static Dictionary<Platform, string> MakeFileAttributeCache = new Dictionary<Platform, string>();
        private static Dictionary<Platform, string> MakefilePlatformSuffixAttributeCache = new Dictionary<Platform, string>();

        /// <summary>
        /// Create a platform
        /// </summary>
        /// <param name="platform"></param>
        /// <param name="platformName"></param>
        public static Platform Create(IUniversalLog log, string platformName)
        {
            Platform platform;
            if (Enum.TryParse(platformName, true, out platform))
            {
                return platform;
            }
            else
            {
                log.Error("Invalid platform {0}", platformName);
                return Platform.Invalid;
            }
        }       

        /// <summary>
        /// Get the MakefileSuffix (MakefileSuffixAttribute) of a platform enum
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        public static string MakefilePlatformSuffix(this Platform platform)
        {
            // Is it cached?
            if (MakefilePlatformSuffixAttributeCache.ContainsKey(platform))
            {
                return MakefilePlatformSuffixAttributeCache[platform];
            }

            Type type = platform.GetType();
            FieldInfo fi = type.GetField(platform.ToString());
            MakefilePlatformSuffixAttribute[] attributes = fi.GetCustomAttributes(typeof(MakefilePlatformSuffixAttribute), false) as MakefilePlatformSuffixAttribute[];
            string s =  (attributes.Length > 0 ? attributes[0].Text : null);

            // Cache it for quicker reflection next time.
            if (s != null)
            {
                MakefilePlatformSuffixAttributeCache.Add(platform, s);
            }

            return s;
        }

        /// <summary>
        /// Get the string (MakeFileAttribute) of a platform enum
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        public static string EnumToString(this Platform platform)
        {
            // Is it cached?
            if (MakeFileAttributeCache.ContainsKey(platform))
            {
                return MakeFileAttributeCache[platform];
            }

            Type type = platform.GetType();
            FieldInfo fi = type.GetField(platform.ToString());
            MakeFileAttribute[] attributes = fi.GetCustomAttributes(typeof(MakeFileAttribute), false) as MakeFileAttribute[];
            string s = (attributes.Length > 0 ? attributes[0].Text : null);
            
            // Cache it for quicker reflection next time.
            if (s != null)
            {
                MakeFileAttributeCache.Add(platform, s);
            }

            return s;
        }

        /// <summary>
        /// Extension method to compare against a string representation of the enum ( case insensitive )
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        public static bool Match(this Platform obj, string val)
        {
            string currVal = obj.EnumToString();
            return (0 == string.Compare(currVal, val, true));
        }

        /// <summary>
        /// extension method - does the platform support PCH?
        /// </summary>
        /// <param name="obj"></param>
        public static bool SupportsPch(this Platform obj)
        {
            if (obj == Platform.Durango)
            {
                return true;
            }
            return false;
        }

    } // class PlatformExtensions
    #endregion // Extensions
} // namespace ProjectGenerator