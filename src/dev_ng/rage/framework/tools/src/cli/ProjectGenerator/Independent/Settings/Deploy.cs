﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace ProjectGenerator.Independent
{
    /// <summary>
    /// Deploy settings
    /// </summary>
    [Serializable]
    public class Deploy : SettingExtender
    {
        #region Properties
        public string RemoteRoot { get; set; }
        public List<string> DeployFiles { get; set; }
        public string DeployType { get; set; }

        #endregion //Properties

        #region Constructor(s)
        /// <summary>
        /// default constructor
        /// </summary>
        public Deploy()
        {
            RemoteRoot = String.Empty;
            DeployType = String.Empty;
            DeployFiles = new List<string>();
        }
        #endregion // Constructor(s)
    } // class SettingsDeploy
}
