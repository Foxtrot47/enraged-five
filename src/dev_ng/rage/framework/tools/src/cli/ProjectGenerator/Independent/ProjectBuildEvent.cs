﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGenerator.Independent
{
    /// <summary>
    /// A Project build event
    /// </summary>
    [Serializable]
    public class ProjectBuildEvent
    {
        #region Properties
        /// <summary>
        /// the command to run for the build event
        /// </summary>
        public string Command { get; set; }
        /// <summary>
        /// The description of the command running that will be optionally emitted in the IDE.
        /// </summary>
        public string Description { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default Constructor
        /// </summary>
        public ProjectBuildEvent()
        {
            Command = String.Empty;
            Description = String.Empty;
        }
        #endregion // constructor(s)
    } // class ProjectBuildEvent
} // namespace ProjectGenerator.Independent
