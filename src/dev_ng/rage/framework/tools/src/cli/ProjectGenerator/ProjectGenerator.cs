﻿using System;
using System.IO;
using System.Diagnostics;
using System.Reflection;
using System.Collections.Generic;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using LitJson;
using System.Linq;
using RSG.Base.Configuration;

namespace ProjectGenerator
{
    /// <summary>
    /// Project Generator class
    /// </summary>
    class ProjectGenerator
    {
        #region Public Constants
        /// <summary>
        /// Enable all tracing of project generator events.
        /// </summary>
        public static readonly bool TRACE = false;
        #endregion Public Constants
        #region Private Constants
        private static readonly string JSON_FILETYPE = ".json";
        #endregion // Private Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public ProjectGenerator()
        {
        }
        /// <summary>
        /// Parameterised constructor
        /// </summary>
        /// <param name="makefileFilenames">the makefilenames ; seperated</param>
        /// <param name="verify">after generation verify the filename in a number of ways</param>
        /// <param name="doJsonDebug">does json debugging</param>
        /// <param name="mangle">the string to use to mangle filenames</param>
        public ProjectGenerator(IUniversalLog log, CommandOptions options, bool verify, bool doJsonDebug, string mangle, IList<string> makefileFilenames)
        {
            Log = log;
            MakefileFilenames = makefileFilenames;
            Verify = verify; 
            DoJsonDebug = doJsonDebug;
            Makefiles = new List<Makefile>();
            ExportedFilenames = new List<string>();
            Mangle = mangle;
            Options = options;
        }
        #endregion // Constructor(s)

        #region Public Properties
        /// <summary>
        /// The makefile filenames
        /// </summary>
        public IList<string> MakefileFilenames { get; set; }
        /// <summary>
        /// A list of makefiles to use
        /// </summary>
        public ICollection<Makefile> Makefiles { get; set; }
        /// <summary>
        /// A list of the filenames ultimately created / 'exported'.
        /// </summary>
        public ICollection<string> ExportedFilenames { get; set; }
        /// <summary>
        /// Optional flag to do JSON debugging
        /// </summary>
        public bool DoJsonDebug { get; set; }
        /// <summary>
        /// Perform a post generation verify step on the exported files
        /// </summary>
        public bool Verify { get; set; }
        /// <summary>
        /// The mangling of the output filenames 
        /// </summary>
        public string Mangle { get; set; }
        #endregion // Public Properties
        #region Private Properties
        /// <summary>
        /// Universal log 
        /// </summary>
        private IUniversalLog Log { get; set; }
        /// <summary>
        /// Options cached.
        /// </summary>
        private CommandOptions Options { get; set; }
        #endregion

        #region Public Methods
        /// <summary>
        /// Load makefiles and export 
        /// </summary>
        /// <returns></returns>
        public bool LoadMakefiles()
        {
            Trace("Project Generator Run @ {0}", System.DateTime.Now);
            Makefile makefile = new Makefile(Log, Options, Mangle);            
            Makefiles.Add(makefile);
            
            int numMakeFiles = MakefileFilenames.Count;
            Trace("LoadMakefiles {0} ({1})", MakefileFilenames, numMakeFiles);
           
            bool ok = true;
            int makefileId = 1;
            foreach (string makefileFilename in MakefileFilenames)
            {
                if (makefile.Import(makefileFilename))
                {
                    Trace("Makefile ({0}/{1}) {2}: Imported OK.", makefileId, numMakeFiles, makefileFilename);
                }
                else
                {
                    ok = false;
                    Log.Warning("Makefile {0}: Import failed.", makefile);
                }
                makefileId++;
            }

            // If this is somehow not a real project then there is nothing to do.
            if (makefile.Project.Name == null && makefile.Solution.Name == null)
            {
                Log.Warning("This is not a Project or Solution ( invalid makefile ) no files will be generated.");
                ok = false;
            }

            if (ok)
            {
                // For each parser file in the project create the 'artefacts' - resultant source files it creates 
                // and add to the project.
                makefile.PopulateParserFileArtefacts();

                // Set the language of the Project's targets
                makefile.InferLanguagesOfTargets();

                // Process the rules loaded
                Trace("Project Generator : Process makerules @ {0}", System.DateTime.Now);
                makefile.ProcessRules();
                makefile.PostProcessRules();
                Trace("Project Generator : Process makerules completed @ {0}", System.DateTime.Now);

                if (DoJsonDebug)
                {
                    JsonDebug(MakefileFilenames.Last(), makefile);
                }
            }

            return ok;
        }

        /// <summary>
        /// Runs exporter rules
        /// </summary>
        public void RunExporterRules()
        {
            foreach (Makefile makefile in Makefiles)
            {
                makefile.RunExporterRules();
            }
        }

        /// <summary>
        /// Designate unity files.
        /// </summary>
        public void DesignateUnity()
        {
            foreach (Makefile makefile in Makefiles)
            {
                makefile.DesignateUnity();
            }
        }
        
        /// <summary>
        /// Get the list of files that will be exported
        /// </summary>
        /// <param name="filenames">the filenames list to populate</param>
        public ICollection<string> PreDetermineExportedFiles()
        {
            ICollection<string> exportFilenamesKnownPreExport = new List<string>();    
            foreach (Makefile makefile in Makefiles)
            {
                makefile.PreDetermineExportedFiles(exportFilenamesKnownPreExport);
            }
            return exportFilenamesKnownPreExport;
        }

        /// <summary>
        /// Export
        /// </summary>
        /// <returns>boolean for success</returns>
        public bool RunExport()
        {
            Trace("Project Generator : Export {0}", System.DateTime.Now);
            bool exported = Export();
            Trace("Exporting finished with result {0} @ {1}", exported, System.DateTime.Now);

            return exported;
        }

        /// <summary>
        /// Display ( Log message ) the files exported by this project generator.
        /// </summary>
        public void DisplayExportedFiles()
        {
            foreach (string file in ExportedFilenames)
            {
                Trace("Exported file: {0}", file);
            }
        }

        #endregion // Public methods
        #region Private Methods
        //
        // Makefile serialisation - for debugging.
        //
        private void JsonDebug(string makefileFilename, Makefile makefile)
        {
            Trace("Saving JSON");
            string jsonFilename = makefileFilename + JSON_FILETYPE;
            makefile.Project.SerialiseJSON(jsonFilename);

            // TODO: DW. whack a breakpoint in here or fire up editor ( oxygen ) to edit the file and continue, 
            // Oxygen will keep your place in the grid view for JSON allowing you to very settings coming through easily.
            // Also useful for rapid prototyping features / testing without having written the neccessary Makefile changes.
            Process.Start(jsonFilename);

            // Load from json
            Independent.Project project_reloaded = Independent.Project.DeserialiseJSON(jsonFilename);

            Trace("Reloaded JSON");
        }
        //
        // Export
        //
        private bool Export()
        {
            bool ok = true;
            foreach (Makefile makefile in Makefiles)
            {
                ICollection<string> filenamesExported = new List<string>();
                if (makefile.Export(filenamesExported, Verify))
                {
                    foreach (string s in filenamesExported)
                    {
                        Trace("Exporting OK as {0}", s);
                        ExportedFilenames.Add(s);
                    }
                }
                else
                {
                    Log.Error("Error: Exporting {0} failed", makefile.Project.Name);
                    ok = false;
                }
            }                        
            return ok;
        }

        private void Trace(String message, params Object[] args)
        {
            if (TRACE)
            {
                Log.Message(message, args);
            }
        }

        #endregion // Private Methods
    } // class ProjectGenerator
} // namespace ProjectGenerator
