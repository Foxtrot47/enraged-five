﻿using System;

namespace ProjectGenerator
{
    /// <summary>
    /// the attibute of a Filetype that contains the dot prefixed extension name 
    /// </summary>
    public class FileTypeStringAttribute : Attribute
    {
        public string Text { get; set; }
        public FileTypeStringAttribute(string text)
        {
            Text = text;
        }
    } // class FileTypeStringAttribute
} // namespace ProjectGenerator
