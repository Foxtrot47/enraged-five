﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ProjectGenerator.Independent
{
    /// <summary>
    /// The configuration of a single unity file
    /// </summary>
    [Serializable]
    public class UnityCompilationUnitDescriptor
    {
        #region Properties
        /// <summary>
        /// Link time compile generation config|platform csv filter.
        /// </summary>
        public bool Ltcg { get; private set; }

        /// <summary>
        /// Regex of the unity compilation unit.
        /// </summary>
        public Regex Regex { get; private set; }
        #endregion // Properties

        #region Constructors
        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="regex"></param>
        /// <param name="ltcg"></param>
        public UnityCompilationUnitDescriptor(Regex regex, bool ltcg)
        {
            Regex = regex;
            Ltcg = ltcg;
        }
        #endregion // Constructors
    }
}
