﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using LitJson;

using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Base.SCM;
using RSG.Pipeline.Core;
using RSG.SourceControl.Perforce;
using RSG.Base.Configuration;

namespace ProjectGenerator
{
    /// <summary>
    /// Main entry point class
    /// </summary>
    class Program
    {
        #region Constants
        private static readonly String AUTHOR                       = "Derek Ward";
        private static readonly String EMAIL                        = "derek.ward@rockstarnorth.com";
        private static readonly String APP_NAME                     = "ProjectGenerator";
        private static readonly string HELP_URL                     = "https://devstar.rockstargames.com/wiki/index.php/Dev:Project_builder3";

        private static readonly String OPTION_VERBOSE               = "verbose";
        private static readonly String OPTION_BUILDSUITE            = "buildsuite";
        private static readonly String OPTION_DEBUG                 = "debug";
        private static readonly String OPTION_LOG                   = "log";
        private static readonly String OPTION_UNILOG                = "unilog";
        private static readonly String OPTION_NEW_UNILOG            = "new_unilog";
        private static readonly String OPTION_NOPOPUP               = "nopopups";
        private static readonly String OPTION_JSONDEBUG             = "jsondebug";
        private static readonly String OPTION_VERIFY                = "verify";
        private static readonly String OPTION_CHANGELIST            = "changelist";
        private static readonly String OPTION_MANGLE                = "mangle";
        private static readonly String OPTION_SCM_DISABLE           = "scm_disable";
        private static readonly String OPTION_SCM_DISABLE_PREEDIT   = "scm_disable_preedit";
        private static readonly String OPTION_SCM_DISABLE_BATCH     = "scm_disable_batch";
        private static readonly String OPTION_HELP                  = "help";        

        private static readonly String LOG_CTX                      = "PG3";
        #endregion // Constants

        #region Properties
        private static bool Verbose { get; set; }
        #endregion //Properties

        #region Main
        /// <summary>
        /// Main entry point
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        static int Main(string[] args)
        {
            int result = Constants.Exit_Success;

            LongOption[] lopts = new LongOption[] {
                    new LongOption(OPTION_HELP,                 LongOption.ArgType.Optional,   "Show help/usage"),         
                    new LongOption(OPTION_CHANGELIST,           LongOption.ArgType.Optional,   "Construct a changelist with all generated files ( if not specified default CL is used )"),         
                    new LongOption(OPTION_DEBUG,                LongOption.ArgType.None,       "Allow Visual Studio debugger to be attached."),
                    new LongOption(OPTION_JSONDEBUG,            LongOption.ArgType.None,       "Export independent representation to a .json format for offline inspection."),
                    new LongOption(OPTION_LOG,                  LongOption.ArgType.Required,   "Usage of log files."),
                    new LongOption(OPTION_BUILDSUITE,           LongOption.ArgType.Optional,   "Specify the 'build suite' filename."), 
                    new LongOption(OPTION_MANGLE,               LongOption.ArgType.Optional,   "Mangle the destination files with this suffix."),
                    new LongOption(OPTION_NEW_UNILOG,           LongOption.ArgType.None,       "Use new unilog."),
                    new LongOption(OPTION_NOPOPUP,              LongOption.ArgType.None,       "Disable popups."),
                    new LongOption(OPTION_SCM_DISABLE,          LongOption.ArgType.Optional,   "Disable most interaction with your SCM. No files will be edited or added."),
                    new LongOption(OPTION_VERBOSE,              LongOption.ArgType.Optional,   "Output Verbose to the console."),
                    new LongOption(OPTION_UNILOG,               LongOption.ArgType.Required,   "Use the universal log."),
                    new LongOption(OPTION_VERIFY,               LongOption.ArgType.None,       "Verify generated files."),                               
                    new LongOption(OPTION_SCM_DISABLE_PREEDIT,  LongOption.ArgType.Optional,   "Disable pre edit files, if used the files generated must be writable, this prevents having to SCM edit files for each makefile, instead they are batch edited once created."),                               
                    new LongOption(OPTION_SCM_DISABLE_BATCH,    LongOption.ArgType.Optional,   "Disable batching of p4 commands."),                               
                };

            CommandOptions options = new CommandOptions(args, lopts);

            bool changelist = options.ContainsOption(OPTION_CHANGELIST);
            bool jsonDebug = options.ContainsOption(OPTION_JSONDEBUG);
            bool nopopups = options.ContainsOption(OPTION_NOPOPUP);
            bool scm = options.ContainsOption(OPTION_SCM_DISABLE) ? false : true;
            bool scmPreEdit = options.ContainsOption(OPTION_SCM_DISABLE_PREEDIT) ? false : true;
            bool batchScm = options.ContainsOption(OPTION_SCM_DISABLE_BATCH) ? false : true;
            bool verify = options.ContainsOption(OPTION_VERIFY);
            bool help = options.ContainsOption(OPTION_HELP);
            string mangle = options.ContainsOption(OPTION_MANGLE) ? (options[OPTION_MANGLE] as String) : String.Empty;
            string buildSuite = options.ContainsOption(OPTION_BUILDSUITE) ? (options[OPTION_BUILDSUITE] as String) : null;

            Verbose = options.ContainsOption(OPTION_VERBOSE);

            IUniversalLog log = LogFactory.CreateUniversalLog(LOG_CTX);
            IUniversalLogTarget logFile = LogFactory.CreateUniversalLogFile(log);
            LogFactory.CreateApplicationConsoleLogTarget();

            if (options.ShowHelp)
            {
                log.MessageCtx(LOG_CTX, options.GetUsage());
                return result;
            }
  
            try
            {
                // If debug option was set and there is no debugger currently 
                // attached to our process then wait for it or for user input.
                bool debugger = options.ContainsOption(OPTION_DEBUG);
                if (debugger && !Debugger.IsAttached)
                {
                    Console.WriteLine("Attach debugger or press any key to continue...");
                    Console.ReadKey();
                }

                IList<string> makefiles = GetMakefiles(log, options, ref buildSuite);

                string version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                string helpLink = HELP_URL;
                string appAndVersion = string.Format("{0} v{1}", APP_NAME, version);
                string startupMsg = string.Format("=== {0} ===\n\n  {5}\n\n  Command Line: {1}.exe {2}\n\n\t{3}\n\n\t{4}\n", appAndVersion, APP_NAME, string.Join(" ", args), buildSuite, string.Join("\n\t", makefiles), helpLink);
                VerboseMsg(log, startupMsg);

                if (help || 0 == options.TrailingArguments.Count())
                {
                    if (!Verbose)
                    {
                        Console.WriteLine("\n{0}", startupMsg);
                        Console.WriteLine("\n{0} {1}", AUTHOR, EMAIL);
                    }
                    Console.WriteLine("\tTrailing arguments are used.\n\tThey specify list of config & makefiles files you want to process, order is important.\n\tConfiguration and scope of work should be set before makefiles.\n\teg.%RS_TOOLSROOT%/bin/ProjectGenerator/ProjectGenerator.exe %RS_TOOLSROOT%/etc/projgen/game.makefiles\n");
                    Console.WriteLine(options.GetUsage());
                    return 0;
                }

                if (makefiles.Count() == 0)
                {
                    log.ErrorCtx(LOG_CTX, "No makefiles specified.");
                    return Constants.Exit_Failure;
                }

                if (buildSuite == null)
                {
                    if (makefiles.Count() == 1)
                    {
                        // Read the first file if the makefile contains the buildsuite we will use this.
                        String buildSuiteExpanded = Environment.ExpandEnvironmentVariables(options.Branch.Environment.Subst(makefiles[0]));                       

                        if (File.Exists(buildSuiteExpanded))
                        {
                            String fileContents = File.ReadAllText(buildSuiteExpanded);
                            Match match = Regex.Match(fileContents, String.Format(@"{0}\s*(.*)\r\n", Makefile.MAKEFILE_BUILD_TEMPLATE));
                            if (match.Success && match.Groups.Count == 2)
                            {
                                buildSuite = match.Groups[1].Value;
                            }
                        }
                    }

                    if (buildSuite == null)
                    {
                        // assume the buildsuite is the first passed in makefile.
                        buildSuite = makefiles[0];
                        makefiles.RemoveAt(0);
                    }
                    else
                    {
                        log.WarningCtx(LOG_CTX, "No build suite specified, proceeding in the hope that it is contained in {0}", makefiles[0]);
                    }
                }

                if (makefiles.Count() == 0)
                {
                    log.WarningCtx(LOG_CTX, "Warning: No makefiles specfied = no work to do.");
                }
                else
                {
                    // ==========================================================
                    // ================   MAIN CONTROL LOGIC  ===================
                    // ==========================================================
                    VerboseProfile(log, "=== {1} Started @ {0} ===", System.DateTime.Now, APP_NAME);

                    int makefileId = 1;
                    int numMakefiles = makefiles.Count;
                    List<string> totalExportedFilenames = new List<string>(); // A collection to store all filenames exported.

                    scm = CheckScmServiceAvaliable(scm, log);

                    string buildSuiteExpanded = Environment.ExpandEnvironmentVariables(buildSuite);
                    buildSuiteExpanded = options.Branch.Environment.Subst(buildSuiteExpanded);
                    String toolsroot = Environment.GetEnvironmentVariable("RS_TOOLSROOT");
                    string buildSuiteCompressed = buildSuiteExpanded.Replace(toolsroot, "%RS_TOOLSROOT%");

                    log.MessageCtx(LOG_CTX, "Configuration Settings = {0}", buildSuiteCompressed);
                    log.MessageCtx(LOG_CTX, " ");
                    // ============ 1. FOR EACH MAKEFILE ==============
                    foreach (string makefileFilename in makefiles)
                    {
                        string makefileFilenameExpanded =  Environment.ExpandEnvironmentVariables(makefileFilename);                        
                        IList<string> allMakefiles = new List<string>() { buildSuiteExpanded, makefileFilenameExpanded };

                        Directory.SetCurrentDirectory(Path.GetDirectoryName(makefileFilenameExpanded));

                        String rageDir = Environment.GetEnvironmentVariable("RAGE_DIR");
                        makefileFilenameExpanded = makefileFilenameExpanded.Replace(rageDir, "%RAGE_DIR%");
                        VerboseSectionStart(log);
                        log.MessageCtx(LOG_CTX, "    ({0}/{1}) {2}", makefileId.ToString().PadLeft(2, ' '), numMakefiles, makefileFilenameExpanded);

                        // ============== 2. CREATE THE PROJECT GENERATOR ==============
                        ProjectGenerator projGen = new ProjectGenerator(log, options, verify, jsonDebug, mangle, allMakefiles);

                        // ============== 3. LOAD MAKEFILES  ==============
                        if (projGen.LoadMakefiles())
                        {
                            // ============== 4. RUN EXPORTER RULES  ==============
                            projGen.RunExporterRules();

                            // ============== 5. PROCESS UNITY  ==============
                            projGen.DesignateUnity();

                            // ============== 6. PRECHECKOUT FILES  ==============                        
                            ICollection<string> exportFilenamesKnownPreExport = PreCheckoutExportedFiles(scm, scmPreEdit, batchScm, log, projGen);

                            // ============== 7. EXPORT FOR ALL EXPORTERS ==============
                            if (projGen.RunExport())
                            {
                                if (ProjectGenerator.TRACE) 
                                    log.MessageCtx(LOG_CTX, "--- {0} ({1}/{2}) : Generated OK. ---", makefileFilenameExpanded, makefileId, numMakefiles);
                            }
                            else
                            {
                                result = Constants.Exit_Failure;
                                log.ErrorCtx(LOG_CTX, "*** ({1}/{2}) {0} : ERROR Generation Failed - no further makefiles are processed. ***", makefileFilenameExpanded, makefileId, numMakefiles);
                                break;
                            }

                            // ============== 8. CHECK EXPORTED OK ==============
                            CompareExportedVsExpected(log, projGen, exportFilenamesKnownPreExport);
                        }

                        // ============== 9. COLLECT THE NEWLY ADDED EXPORTED FILES ==============
                        totalExportedFilenames.AddRange(projGen.ExportedFilenames);

                        projGen.DisplayExportedFiles();

                        VerboseSectionEnd(log);

                        makefileId++;
                    }

                    // ============== 10. SCM CHECKIN ==============
                    if (scm)
                    {
                        ScmCheckin(changelist, batchScm, log, appAndVersion, totalExportedFilenames);
                    }
                    else
                    {
                        log.WarningCtx(LOG_CTX, "SCM was disabled.");
                    }

                    if (log.HasWarnings)
                    {
                        VerboseWarning(log, "*** {0} : Warnings encountered - please review logs & console output. ***", APP_NAME);
                    }

                    if (log.HasErrors)
                    {
                        VerboseWarning(log, "*** {0} : Errors encountered - please review logs & console output. ***", APP_NAME);
                        result = Constants.Exit_Failure;
                    }

                    VerboseProfile(log, "=== {1} Finished @ {0} with exit code {2} ===", System.DateTime.Now, APP_NAME, result);
                }
            }
            catch (Exception ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "*** Top Level Exception in {0} ***\n===Msg===\n{1}\n===Stack trace===\n{2}", APP_NAME, ex.Message, ex.StackTrace);
            }

            if (log.HasErrors)
                result = Constants.Exit_Failure;

            Environment.ExitCode = result;
            VerboseMsg(log, "Exiting with {0}", result);

            LogFactory.ApplicationShutdown(LogFactory.ShutdownMode.DisplayOnError);

            return (result);
        } // Main
        #endregion // main

        #region Private Methods

        private static bool CheckScmServiceAvaliable(bool scm, IUniversalLog log)
        {
            if (scm)
            {
                using (P4 p4 = new P4())
                {
                    try
                    {
                        if (p4 == null)
                        {
                            log.WarningCtx(LOG_CTX, "Perforce service was not available, the files have been exported and will be modified, yet won't be edited in perforce");
                            scm = false;
                        }
                        else
                        {
                            log.DebugCtx(LOG_CTX, "P4 Connect");
                            bool enabled = P4.Log.Enabled;
                            P4.Log.Enabled = false;
                            p4.Connect();
                            P4.Log.Enabled = enabled;
                            log.DebugCtx(LOG_CTX, "..P4 Connected");
                        }
                    }
                    catch (P4API.Exceptions.P4APIExceptions ex)
                    {
                        log.ToolExceptionCtx(LOG_CTX, ex, "Unhandled p4 exception ({0} [{1}]).", p4.Port, p4.User);
                    }
                }
            }
            return scm;
        }

        private static void ScmCheckin(bool changelist, bool batchScm, IUniversalLog log, string appAndVersion, List<string> totalExportedFilenames)
        {
            using (P4 p4 = new P4())
            {
                try
                {
                    p4.CallingProgram = Assembly.GetExecutingAssembly().FullName;

                    bool enabled = P4.Log.Enabled;
                    P4.Log.Enabled = false;
                    p4.Connect();
                    P4.Log.Enabled = enabled;                    
                    
                    VerboseSectionStart(log);
                    VerboseMsg(log, "=== SCM add or edit files");
                    log.DebugCtx(LOG_CTX, "P4 Finalise");

                    ScmFinalise(log, totalExportedFilenames, p4, changelist, batchScm, appAndVersion);// LogFactory.ApplicationConsoleLog);

                    log.DebugCtx(LOG_CTX, "..P4 Finalised");

                    VerboseSectionEnd(log);
                }
                catch (P4API.Exceptions.P4APIExceptions ex)
                {
                    log.ToolException(ex, "Unhandled p4 exception ({0} [{1}]).", p4.Port, p4.User);
                }
            }
        }

        private static ICollection<string> PreCheckoutExportedFiles(bool scm, bool scmPreEdit, bool batchScm, IUniversalLog log, ProjectGenerator projGen)
        {
            ICollection<string> exportFilenamesKnownPreExport = projGen.PreDetermineExportedFiles();

            if (scm && exportFilenamesKnownPreExport.Count > 0)
            {
                using (P4 p4 = new P4())
                {
                    p4.CallingProgram = Assembly.GetExecutingAssembly().FullName;

                    bool enabled = P4.Log.Enabled;
                    P4.Log.Enabled = false;                                        
                    p4.Connect();
                    P4.Log.Enabled = enabled;

                    try
                    {
                        log.DebugCtx(LOG_CTX, "P4 pre-edit {0} files", exportFilenamesKnownPreExport.Count);

                        if (scmPreEdit)
                            ScmCheckout(log, exportFilenamesKnownPreExport, p4, batchScm);

                        log.DebugCtx(LOG_CTX, "..P4 pre-edited");
                    }
                    catch (P4API.Exceptions.P4APIExceptions ex)
                    {
                        log.ToolExceptionCtx(LOG_CTX, ex, "Unhandled p4 exception ({0} [{1}]).", p4.Port, p4.User);
                    }
                }
            }
            return exportFilenamesKnownPreExport;
        }

        private static void VerboseMsg(IUniversalLog log, String message, params Object[] args)
        {
            if (Verbose)
                log.MessageCtx(LOG_CTX, message, args);
        }

        private static void VerboseWarning(IUniversalLog log, String message, params Object[] args)
        {
            if (Verbose)
                log.WarningCtx(LOG_CTX, message, args);
        }

        private static void VerboseProfile(IUniversalLog log, String message, params Object[] args)
        {
            if (Verbose)
                log.ProfileCtx(LOG_CTX, message, args);
        }

        private static void VerboseSectionStart(IUniversalLog log)
        {
            VerboseMsg(log, " ");
            VerboseMsg(log, "--------------------------------------------------------------------------------");
        }

        private static void VerboseSectionEnd(IUniversalLog log)
        {
            VerboseMsg(log, "--------------------------------------------------------------------------------");
            VerboseMsg(log, " ");
        }

        /// <summary>
        /// Load the .makefiles file
        /// </summary>
        /// <param name="build">the build set</param>
        /// <param name="makefiles">a list of makefile filenames to add to</param>
        private static void ParseMakefilesFile(IUniversalLog log, String filename, ICollection<string> makefiles, ref string buildSuite)
        {
            filename = Path.GetFullPath(Environment.ExpandEnvironmentVariables(filename));
            if (!File.Exists(filename))
            {
                log.ErrorCtx(LOG_CTX, "Makefiles file {0} doesn't exist", filename);
                return;
            }

            string[] lines = File.ReadAllLines(filename);
            foreach (string line in lines)
            {
                // handle comments
                string fn = line.Trim();
                if (fn.Length == 0 || fn.StartsWith(Makefile.MAKEFILE_COMMENT_CHAR.ToString()))
                    continue;

                // Match the build suite
                {
                    Match match = Makefile.BUILD_SUITE_REGEX.Match(fn);
                    if (match.Success && match.Groups.Count > 1)
                    {
                        buildSuite = match.Groups[1].Value;
                        continue;
                    }
                }

                {
                    // All we expect in this file is a list of files, which we add to the makefiles list.
                    Match match = Makefile.MAKEFILE_COMMENT_REGEX.Match(fn); // removes trailing comment
                    if (match.Success && match.Groups.Count > 1)
                    {
                        makefiles.Add(match.Groups[1].Value);
                    }
                    else
                    {
                        makefiles.Add(fn);
                    }
                }
            }
        }

        /// <summary>
        /// Run the FINAL source control stage of processing. 
        /// adding files 
        /// moving exported files into a CL
        /// reverting if unchanged
        /// Remember: files will have been pre checked out 
        /// </summary>
        /// <param name="allExportedFilenames"></param>
        /// <param name="useNonDefaultChangelist"></param>
        /// <param name="changelistDescription"></param>
        /// <param name="consoleLog"></param>
        private static void ScmFinalise(IUniversalLog log, IEnumerable<string> allExportedFilenames, P4 p4, bool useNonDefaultChangelist, bool batch = true, string changelistDescription = "Project Generated files.", ILogTarget consoleLog = null)
        {
            bool doLogMessage = false;

            try
            {
                // Check all these filenames actually exist
                ICollection<string> allExportedFilenamesThatExist = new List<string>();
                foreach (string filename in allExportedFilenames)
                {
                    if (File.Exists(filename))
                    {
                        allExportedFilenamesThatExist.Add(filename);
                    }
                    else
                    {
                        log.ErrorCtx(LOG_CTX, "A filename could not be found {0}... yet was supposedly exported?!", filename);
                    }
                }

                P4API.P4PendingChangelist changeList = useNonDefaultChangelist ? p4.CreatePendingChangelist(changelistDescription) : null;

                if (changeList != null)
                {
                    log.MessageCtx(LOG_CTX, "As requested, a changelist has been created {0}", changeList.Number);
                }

                FileMapping[] fileMappings = FileMapping.Create(p4, allExportedFilenamesThatExist.ToArray());
                IEnumerable<String> depotPaths = fileMappings.Where(map => map.Mapped == true).Select(map => map.DepotFilename);
                FileState[] fileStates = FileState.Create(p4, depotPaths.ToArray());

                ICollection<string> addDepotPaths = new List<string>();
                ICollection<string> editDepotPaths = new List<string>();
                ICollection<string> allDepotPaths = new List<string>();

                // DW : TODO : Iterate over filestates when there is 1:1 mapping with FileState.Create
                foreach (string depotPath in depotPaths)
                {
                    string action = "add";
                    foreach (FileState fileState in fileStates) // remove later! 
                    {
                        if (string.Compare(fileState.DepotFilename, depotPath, true) == 0)
                        {
                            if (fileState.HeadAction != FileAction.Unknown &&
                                fileState.HeadAction != FileAction.Delete)
                            {
                                editDepotPaths.Add(depotPath);
                                allDepotPaths.Add(depotPath);
                                action = "edit";
                            }
                            break;
                        }
                    }

                    if (batch)
                    {
                        if (action == "add")
                        {
                            addDepotPaths.Add(depotPath);
                            allDepotPaths.Add(depotPath);
                        }
                    }
                    else
                    {
                        P4API.P4RecordSet recordSet = null;
                        if (changeList != null)
                        {
                            //p4.Run("reopen", doLogMessage, "-c", changeList.Number.ToString(), "-t", "+w", depotPath);
                            p4.Run("reopen", doLogMessage, "-c", changeList.Number.ToString(), depotPath);
                            recordSet = p4.Run(action, doLogMessage, "-c", changeList.Number.ToString(), depotPath);

                        }
                        else
                        {
                            if (action == "add")
                            {
                                //p4.Run("reopen", doLogMessage, "-t", "+w", depotPath);
                                recordSet = p4.Run(action, doLogMessage, depotPath); // a guid file? ***** must not be +w! *****
                            }
                        }

                        if (recordSet != null && recordSet.Errors.Length > 0)
                        {
                            log.ErrorCtx(LOG_CTX, "An error occurred during p4 {0} {1} ", action, depotPath);
                            foreach (string error in recordSet.Errors)
                            {
                                log.ErrorCtx(LOG_CTX, error);
                            }
                        }
                        else
                        {
                            if (doLogMessage) { log.MessageCtx(LOG_CTX, "p4 {0} {1} ok", action, depotPath); }
                        }

                        // Revert unchanged files
                        p4.Run("revert", doLogMessage, "-a", depotPath);
                    }
                }

                // Batched
                if (batch)
                {
                    P4API.P4RecordSet recordSet = null;
                    if (changeList != null)
                    {
                        if (addDepotPaths.Count > 0)
                        {
                            //p4.Run("reopen", doLogMessage, "-c", changeList.Number.ToString(), "-t", "+w", depotPath);           // DW: NO!!!                            

                            // JUMPING THRU HOOPS!!! overload Params with an array 
                            string[] temp = { "-c", changeList.Number.ToString() };
                            string[] allArgs = temp.Concat(addDepotPaths).ToArray();

                            log.DebugCtx(LOG_CTX, "P4 Reopen & Add {0}", string.Join(",", allArgs));

                            p4.Run("reopen", doLogMessage, allArgs);
                            recordSet = p4.Run("add", doLogMessage, allArgs);
                        }

                        if (editDepotPaths.Count > 0)
                        {
                            //p4.Run("reopen", doLogMessage, "-c", changeList.Number.ToString(), "-t", "+w", depotPath);             // DW: NO!!!            

                            // JUMPING THRU HOOPS!!! overload Params with an array 
                            string[] temp = { "-c", changeList.Number.ToString() };
                            string[] allArgs = temp.Concat(editDepotPaths).ToArray();

                            log.DebugCtx(LOG_CTX, "P4 Reopen & Edit {0}", string.Join(",", allArgs));

                            p4.Run("reopen", doLogMessage, allArgs);
                            recordSet = p4.Run("edit", doLogMessage, allArgs);
                        }
                    }
                    else
                    {
                        if (addDepotPaths.Count > 0)
                        {
                            //p4.Run("reopen", doLogMessage, "-t", "+w", depotPath);        // DW: NO!!!

                            log.DebugCtx(LOG_CTX, "P4 add {0}", string.Join(",", addDepotPaths));

                            recordSet = p4.Run("add", doLogMessage, addDepotPaths.ToArray()); // a guid file? must not be +w!
                        }
                    }

                    if (recordSet != null && recordSet.Errors.Length > 0)
                    {
                        log.ErrorCtx(LOG_CTX, "An error occurred during p4 {0} ", string.Join(",", allDepotPaths));
                        foreach (string error in recordSet.Errors)
                        {
                            log.ErrorCtx(LOG_CTX, error);
                        }
                    }
                    else
                    {
                        if (doLogMessage) { log.MessageCtx(LOG_CTX, "p4 {0} ok", string.Join(",", allDepotPaths)); }
                    }

                    if (allDepotPaths.Count > 0)
                    {
                        // Revert unchanged files
                        string[] tempStr = { "-a" };
                        string[] allArguments = tempStr.Concat(allDepotPaths).ToArray();
                        
                        log.DebugCtx(LOG_CTX, "P4 Revert unchanged {0}", string.Join(",", allArguments));

                        p4.Run("revert", doLogMessage, allArguments);
                    }
                }

            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Unhandled p4 exception during ScmFinalise ({0} [{1}]) {2}.", p4.Port, p4.User, ex.Message);
            }
        }

        /// <summary>
        /// Pre checkout files so they can be written to.
        /// </summary>
        /// <param name="filenamesKnownPreExport"></param>
        private static void ScmCheckout(IUniversalLog log, ICollection<string> filenamesKnownPreExport, P4 p4, bool batch = true)
        {
            ICollection<string> editDepotPaths = new List<string>();

            try
            {
                FileMapping[] fileMappings = FileMapping.Create(p4, filenamesKnownPreExport.ToArray());
                IEnumerable<String> depotPaths = fileMappings.Where(map => map.Mapped == true).Select(map => map.DepotFilename);
                FileState[] fileStates = FileState.Create(p4, depotPaths.ToArray());

                foreach (string depotPath in depotPaths)
                {
                    foreach (FileState fileState in fileStates) // remove later! 
                    {
                        if (string.Compare(fileState.DepotFilename, depotPath, true) == 0)
                        {
                            if (fileState.HeadAction != FileAction.Unknown &&
                                fileState.HeadAction != FileAction.Delete)
                            {
                                if (batch)
                                {
                                    editDepotPaths.Add(depotPath);
                                }
                                else
                                {
                                    if (ProjectGenerator.TRACE)
                                    {
                                        log.MessageCtx(LOG_CTX, "Edit {0}", depotPath);
                                    }

                                    P4API.P4RecordSet recordSet = p4.Run("edit", false, depotPath);

                                    if (recordSet != null && recordSet.Errors.Length > 0)
                                    {
                                        log.ErrorCtx(LOG_CTX, "An error occurred during p4 {0} ", depotPath);
                                        foreach (string error in recordSet.Errors)
                                        {
                                            log.ErrorCtx(LOG_CTX, error);
                                        }
                                    }
                                }
                            }
                            break;
                        }
                    }
                }

                if (batch && editDepotPaths.Count > 0)
                {
                    if (ProjectGenerator.TRACE)
                    {
                        log.DebugCtx(LOG_CTX, "Edit {0}", string.Join(",", editDepotPaths));
                    }

                    P4API.P4RecordSet recordSetBatch = p4.Run("edit", false, editDepotPaths.ToArray());

                    if (recordSetBatch != null && recordSetBatch.Errors.Length > 0)
                    {
                        log.ErrorCtx(LOG_CTX, "An error occurred during p4 edit {0} ", string.Join(",", editDepotPaths));
                        foreach (string error in recordSetBatch.Errors)
                        {
                            log.ErrorCtx(LOG_CTX, error);
                        }
                    }
                }
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Unhandled p4 exception during ScmCheckout ({0} [{1}]) {2}.", p4.Port, p4.User, ex.Message);
            }
        }

        private static IList<string> GetMakefiles(IUniversalLog log, CommandOptions options, ref string buildSuite)
        {
            IList<string> makefiles = new List<string>();

            foreach (string filename in options.TrailingArguments)
            {
                string expandedFilename = Path.GetFullPath(Environment.ExpandEnvironmentVariables(filename));
                string filenameRooted = Path.IsPathRooted(expandedFilename) ? expandedFilename : Path.Combine(Directory.GetCurrentDirectory(), expandedFilename);
                if (Path.GetExtension(filename).ToLower().Contains("makefiles"))
                {
                    ParseMakefilesFile(log, filenameRooted, makefiles, ref buildSuite);
                }
                else
                {
                    makefiles.Add(filenameRooted);
                }
            }
            return makefiles;
        }

        private static void CompareExportedVsExpected(IUniversalLog log, ProjectGenerator projGen, IEnumerable<string> exportFilenamesKnownPreExport)
        {
            foreach (string preExportFilename in exportFilenamesKnownPreExport)
            {
                bool matched = false;
                foreach (string exportedFilename in projGen.ExportedFilenames)
                {
                    if (0 == string.Compare(preExportFilename, exportedFilename, false))
                    {
                        matched = true;
                        break;
                    }
                }

                if (!matched)
                {
                    log.ErrorCtx(LOG_CTX, "Filename {0} was expected to be exported but was not.", preExportFilename);
                }
            }

            foreach (string exportedFilename in projGen.ExportedFilenames)
            {
                bool matched = false;
                foreach (string preExportFilename in exportFilenamesKnownPreExport)
                {
                    if (0 == string.Compare(preExportFilename, exportedFilename, false))
                    {
                        matched = true;
                        break;
                    }
                }

                if ((!matched) && (!exportedFilename.ToLower().Contains("guid")))
                {
                    log.WarningCtx(LOG_CTX, "Filename {0} was exported but was not expected.", exportedFilename);
                }
            }
        }

        #endregion // Private Methods
    } // class Program
} // namespace ProjectGenerator
