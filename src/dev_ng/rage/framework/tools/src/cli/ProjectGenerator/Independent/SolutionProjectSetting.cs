﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGenerator.Independent
{
    /// <summary>
    /// A store of the configurable setting of a solution project
    /// - this is used to hold the settings for a instance of a project in a solution for a particular solution config
    /// </summary>
    [Serializable]
    public class SolutionProjectSetting
    {
        #region Public Properties 
        /// <summary>
        /// The PROJECT configuration of a SOLUTION project 
        /// </summary>
        public string ProjectConfig { get; set; }
        /// <summary>
        /// Flag to build this project as part of the SOLUTION
        /// </summary>
        public bool Build { get; set; }
        /// <summary>
        /// Flag to deploy this project as part of the SOLUTION
        /// </summary>
        public bool Deploy { get; set; }
        #endregion // Public Properties

        #region Constructors
        /// <summary>
        /// Default Constructor
        /// </summary>
        public SolutionProjectSetting()
        {
            ProjectConfig = String.Empty;
            Build = false;
            Deploy = false;
        }
        #endregion // Constructors
    } // class SolutionProjectSetting
} // namespace ProjectGenerator.Independent
