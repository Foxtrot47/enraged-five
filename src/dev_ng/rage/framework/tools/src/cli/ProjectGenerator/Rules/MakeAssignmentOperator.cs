﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace ProjectGenerator
{
    #region Enum
    /// <summary>
    /// Enum of recognised assignment operators for make actions
    /// </summary>
    public enum MakeAssignmentOperator
    {
        [MakeFileAttribute("<<")]
        Append,
        [MakeFileAttribute(">>")]
        Prepend,
        [MakeFileAttribute("+=")]
        Add,
        [MakeFileAttribute("=")]
        Set,
        [MakeFileAttribute("INVALID")]
        Invalid
    } // enum MakeAssignmentOperator
    #endregion //Enum

    #region Extensions
    /// <summary>
    /// Enum extensions of MakeAssignmentOperator
    /// </summary>
    public static class MakeAssignmentOperatorExtensions
    {
        /// <summary>
        /// Caches for (much) quicker reflection
        /// </summary>
        private static Dictionary<MakeAssignmentOperator, string> MakeFileAttributeCache = new Dictionary<MakeAssignmentOperator, string>();

        /// <summary>
        /// Converts a string to a MakeAssignmentOperator enum
        /// </summary>
        /// <param name="op"></param>
        /// <returns></returns>
        public static MakeAssignmentOperator Create(IUniversalLog log, string op)
        {
            MakeAssignmentOperator[] ops = (MakeAssignmentOperator[])Enum.GetValues(typeof(MakeAssignmentOperator));

            foreach (MakeAssignmentOperator o in ops)
            {
                if (0 == String.Compare(op, EnumToString(o), true))
                    return (o);
            }
            log.Error("Invalid MakeAssignmentOperator {0}", op);
            return MakeAssignmentOperator.Invalid;
        }

        /// <summary>
        /// Converts a MakeAssignmentOperator enum to a string
        /// </summary>
        /// <param name="makeEqualityOperator"></param>
        /// <returns></returns>
        public static string EnumToString(this MakeAssignmentOperator makeRelationalOperator)
        {
            // Is it cached?
            if (MakeFileAttributeCache.ContainsKey(makeRelationalOperator))
            {
                return MakeFileAttributeCache[makeRelationalOperator];
            }

            Type type = makeRelationalOperator.GetType();
            FieldInfo fi = type.GetField(makeRelationalOperator.ToString());
            MakeFileAttribute[] attributes = fi.GetCustomAttributes(typeof(MakeFileAttribute), false) as MakeFileAttribute[];
            string s = (attributes.Length > 0 ? attributes[0].Text : null);

            // Cache it for quicker reflection next time.
            if (s != null)
            {
                MakeFileAttributeCache.Add(makeRelationalOperator, s);
            }

            return s;
        }
    } // class MakeRelationalOperatorExtensions
    #endregion //Extensions
} // namespace ProjectGenerator
