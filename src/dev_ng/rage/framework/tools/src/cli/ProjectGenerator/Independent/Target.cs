﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace ProjectGenerator.Independent
{
    /// <summary>
    /// An Independent target
    /// </summary>
    [Serializable]
    public class Target
    {
        #region Public Properties
        /// <summary>
        /// The full name of a target
        /// <remarks>typically the joining of 'Config|Platform'</remarks>
        /// </summary>
        public string Name { get { return Config + "|" + Platform.EnumToString(); } }
        /// <summary>
        /// The platform of the target
        /// <example>Win32, PS3 etc.</example>
        /// </summary>
        public Platform Platform { get; set; }
        /// <summary>
        /// The config of the target
        ///  <example>Beta, Final etc.</example>
        /// </summary>
        public string Config { get; set; }
        /// <summary>
        /// The lanugage of the target
        ///  <example>C#, C++</example>
        /// </summary>
        public Language Language { get; set; }
        /// <summary>
        /// The settings of the target
        /// <remarks>compiler linker settings etc.</remarks>
        /// </summary>
        public Settings Settings { get; set; }
        /// <summary>
        /// ProjectType - the type of project lib, exe, dll...
        /// </summary>
        public ProjectType ProjectType { get; set; }
        /// <summary>
        /// Scratch area for rules to write temporary values to.
        /// thereatfer they can be reused in the rules file, preventing bloat.
        /// </summary>
        public Scratch Scratch { get; set; }
        /// <summary>
        /// A temporary Exporter enum - this is used in exporter rules to know what sort of project we are exporting for at export time
        /// It is not part of the independent project format.
        /// </summary>
        public Exporter Exporter { get; set; }
        /// <summary>
        /// A target can be flagged inactive
        /// - something an exporter rule can do to disable targets
        /// </summary>
        public bool Active { get; set; }
        #endregion // Public Properties

        #region Private Properties
        /// <summary>
        /// Universal log : no choice but to make this static since the SimpleObjectCloner would want to clone this. A difficult decision.
        /// </summary>
        private static IUniversalLog Log { get; set; }
        #endregion // Private Properties

        #region Private Constants
        private static readonly string SCRATCH = "Scratch";
        #endregion // Private Constants

        #region Constructor(s)
        /// <summary>
        /// Parameterised Constructor that sets the platform and config.
        /// </summary>
        /// <param name="platform"></param>
        /// <param name="config"></param>
        public Target(IUniversalLog log, string platform, string config)
        {
            // By default all projects are libs since it's not currently setup in makefiles.
            // The new executable makefile format specifies that it;s an exe though.
            Log = log;
            ProjectType = new ProjectType(ProjectType.StandardValues.Lib);
            Platform = PlatformExtensions.Create(Log, platform);
            Config = config;
            Language = Language.None; // needs to be established by scanning the source files in the project
            Settings = new Settings();
            Scratch = new Scratch();
            Active = true;            
        }
        #endregion // Constructor(s)

        #region Public Methods : Rule processing

        /// <summary>
        /// Sets the . separated property value in this class to the passed value. 
        /// <remarks>NB. Because strings are immutable and thus we have to assign a new object, this makes this slightly counter intuitive.</remarks>
        /// <remarks>the general policy at the moment is to create new objects as required.</remarks>
        /// </summary>
        /// <param name="fullname">The name of the property in the target ( . seperated )</param>
        /// <param name="value">the value to set the property to</param>
        public void SetPropertyValue(string fullname, object value)
        {
            Object obj = this;
            Object prevObj = null; 

            IList<String> propertyNames = fullname.Split('.').ToList<string>();
            int propertyNamesLength = propertyNames.Count;
            int propertyId = 0;

            foreach (string name in propertyNames)
            {
                PropertyInfo propertyInfo = obj.GetType().GetProperty(name);
                if (propertyInfo == null)
                {
                    // Handle Scratch Dictionary
                    if (propertyNames[0] == SCRATCH)
                    {
                        string[] propertyName = propertyNames.Skip(1).ToArray();
                        string key = String.Join(".", propertyName);
                        Scratch.Add(key, (string)value);
                        return;
                    }

                    // Whack into bucket if object has a (is a) SettingExtender
                    if (obj is SettingExtender)
                    {
                        SettingExtender extender = (SettingExtender)obj;
                        extender.SetBucket(name,(string)value);
                        return;
                    }

                    Log.Error("Get MakeRule property value couldn't resolve {0} in {1}", name, fullname);
                    return;
                }
                prevObj = obj;
                obj = propertyInfo.GetValue(obj, null);
                if (obj == null)
                {
                    Log.Error("Get MakeRule property didn't manage to retrieve an object for {0} in {1}", name, fullname);
                }
                else
                {
                    if (propertyId == propertyNamesLength-1)
                    {
                        propertyInfo.SetValue(prevObj, value, null);
                    }
                }
                propertyId += 1;
            }
        }

        /// <summary>
        /// Returns a . separated string into the relevent property value if it exists within this class.
        /// </summary>
        /// <param name="fullname">the . seperated string that relates to the members of the Independent.Target class</param>
        /// <returns>the object within the target class.</returns>
        public Object GetProperty(string fullname)
        {
            Object obj = this;
            IList<string> propertyNames = fullname.Split('.').ToList<string>();

            foreach (string name in propertyNames)
            {
                PropertyInfo propertyInfo = obj.GetType().GetProperty(name);
                if (propertyInfo == null)
                {
                    // Handle Scratch Dictionary
                    if (propertyNames.First() == SCRATCH)
                    {
                        string[] propertyName = propertyNames.Skip(1).ToArray();
                        string key = String.Join(".", propertyName);
                        return Scratch.Get(key);
                    }

                    // Get setting if in SettingExtender
                    if (obj is SettingExtender)
                    {
                        SettingExtender extender = (SettingExtender)obj;
                        return extender.GetBucket(name);
                    }

                    Log.Error("Get MakeRule property value couldn't resolve {0} in {1}", name, fullname);
                    return null;
                }
                obj = propertyInfo.GetValue(obj, null);
                if (obj == null)
                {
                    Log.Error("Get MakeRule property didn't manage to retrieve an object for {0} in {1} (it was null)", name, fullname);
                }
            }

            return obj;
        }
        #endregion // Public Methods : Rule processing
    } // class Target
} // namespace ProjectGenerator.Independent
