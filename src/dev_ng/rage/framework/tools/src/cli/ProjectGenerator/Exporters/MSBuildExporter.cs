﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using RSG.Base.Logging.Universal;
using RSG.Base.Configuration;

namespace ProjectGenerator.Exporters
{
    /// <summary>
    /// MSBuild base class
    /// </summary>
    public class MSBuildExporter :
        ExporterBase
    {
        #region Private Constants
        /// <summary>
        /// Msbuild nodes 
        /// http://msdn.microsoft.com/en-us/library/5dy88c2e.aspx
        /// http://msdn.microsoft.com/en-us/library/dd393574.aspx
        /// </summary>
        protected static readonly string MSBUILD_ITEMGROUP = "ItemGroup"; // Contains a set of user-defined Item elements. Every item used in a MSBuild project must be specified as a child of an ItemGroup element.
        protected static readonly string MSBUILD_PROPERTYGROUP = "PropertyGroup"; // Contains a set of user-defined Property elements. Every Property element used in an MSBuild project must be a child of a PropertyGroup element.
        protected static readonly string MSBUILD_IMPORT = "Import"; // Imports the contents of one project file into another project file.
        protected static readonly string MSBUILD_IMPORTGROUP = "ImportGroup"; // Contains a collection of Import elements that are grouped under an optional condition. For more information, see Import Element (MSBuild).
        protected static readonly string MSBUILD_ITEMDEFINITIONGROUP = "ItemDefinitionGroup"; // The ItemDefinitionGroup element lets you define a set of Item Definitions, which are metadata values that are applied to all items in the project, by default. ItemDefinitionGroup supersedes the need to use the CreateItem Task and the CreateProperty Task. For more information, see Item Definitions.
        #endregion //Constants

        #region Constructor(s)
        /// <summary>
        /// Parameterised constructor
        /// </summary>
        /// <param name="mangle"></param>
        /// <param name="unity"></param>
        public MSBuildExporter(IUniversalLog log, CommandOptions options, string mangle, bool unity, bool exportsProjects, bool exportsSolutions)
            : base(log, options, mangle, unity, exportsProjects, exportsSolutions)
        {
        }
        #endregion // Constructor(s)

        #region Protected Static Methods
        #region Xml helpers

        // supports the conditional msbuild node ( optional )
        protected static XmlElement CreateElement(XmlDocument doc, string elementName, Independent.Target target = null)
        {
            XmlElement element = doc.CreateElement(elementName);
            if (target != null)
            {
                AppendAttribute(doc, element, "Condition", string.Format("'$(Configuration)|$(Platform)'=='{0}'", target.Name));
            }
            return element;
        }

        protected static XmlElement CreateElement(XmlDocument doc, string elementName, Platform platform)
        {
            XmlElement element = doc.CreateElement(elementName);
            if (platform != Platform.All)
            {
                AppendAttribute(doc, element, "Condition", string.Format("'$(Platform)'=='{0}'", platform.EnumToString()));
            }
            return element;
        }

        protected static XmlElement AppendElementWithAttribute(XmlDocument doc, XmlElement element, string elementName, string attributeName, string attributeValue, Platform platform, bool sort = true)
        {
            XmlElement newElement = CreateElement(doc, elementName, platform);
            AppendAttribute(doc, newElement, attributeName, attributeValue);
            AppendChild(element, newElement, sort);
            return newElement;
        }

        protected static XmlElement AppendElementWithAttribute(XmlDocument doc, XmlElement element, string elementName, string attributeName, string attributeValue, Independent.Target target = null, bool sort = true)
        {
            XmlElement newElement = CreateElement(doc, elementName, target);
            AppendAttribute(doc, newElement, attributeName, attributeValue);
            AppendChild(element, newElement, sort);
            return newElement;
        }

        protected static XmlElement AppendElementWithAttributes(XmlDocument doc, XmlElement element, string elementName, Dictionary<string, string> attributes, Independent.Target target = null, bool sort = true)
        {
            XmlElement newElement = CreateElement(doc, elementName, target);
            AppendAttributes(doc, newElement, attributes);
            AppendChild(element, newElement, sort);
            return newElement;
        }

        protected static XmlElement AppendElementWithTextNode(XmlDocument doc, XmlElement element, KeyValuePair<string, string> keyVal, Platform platform, bool sort = true)
        {
            return AppendElementWithTextNode(doc, element, keyVal.Key, keyVal.Value, platform, sort);
        }

        protected static XmlElement AppendElementWithTextNode(XmlDocument doc, XmlElement element, KeyValuePair<string, string> keyVal, Independent.Target target = null, bool sort = true)
        {
            return AppendElementWithTextNode(doc, element, keyVal.Key, keyVal.Value, target, sort);
        }

        protected static XmlElement AppendElementWithTextNode(XmlDocument doc, XmlElement element, string elementName, string text, Platform platform, bool sort = true)
        {
            if (text == null)
            {
                // this is legitamate and not an error 
                // - the intent is to not create a node if the text element resolves to null 
                // - this would happen if there was no valid conversion for a target and no thus need to set a value 
                // - which will mean that the project file will default
                return null;
            }

            XmlElement newElement = CreateElement(doc, elementName, platform);
            XmlText textNode = doc.CreateTextNode(text);
            newElement.InnerText = text;
            AppendChild(element, newElement, sort);
            return newElement;
        }

        /// <summary>
        /// Linear insertion sort of element
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="newElement"></param>
        /// <param name="insertionSort"></param>
        protected static void AppendChild( XmlElement parent,  XmlElement newElement, bool insertionSort = true)
        {
            // DW: ---Abandoned--- DO NOT sort elements, I have discovered that order matters in a VCXproj, seems like some macro expansion quirk of VS, 
            // So basically I'm not prepared to mess with the unknown here unless absolutely required, which it isn't. 
            // sorted xml files were just an idea to assist debugging.

/*          if (insertionSort)
            {
                foreach (XmlNode n in parent.ChildNodes)
                {
                    if (string.Compare(n.Name, newElement.Name) > 0)
                    {
                        parent.InsertBefore(newElement, n);
                        return;
                    }
                }
            }*/
            
            parent.AppendChild(newElement);
        }

        protected static XmlElement AppendElementWithTextNode(XmlDocument doc, XmlElement element, string elementName, string text, Independent.Target target = null, bool sort = true)
        {
            if (text == null)
            {
                // this is legitamate and not an error 
                // - the intent is to not create a node if the text element resolves to null 
                // - this would happen if there was no valid conversion for a target and no thus need to set a value 
                // - which will mean that the project file will default
                return null;
            }

            XmlElement newElement = CreateElement(doc, elementName, target);
            XmlText textNode = doc.CreateTextNode(text);
            newElement.InnerText = text;
            AppendChild(element, newElement, sort);
            return newElement;
        }

        protected static XmlElement AppendElement(XmlDocument doc, XmlElement element, string elementName, Independent.Target target = null, bool sort = true)
        {
            XmlElement newElement = CreateElement(doc, elementName, target);
            AppendChild(element, newElement, sort);
            return newElement;
        }

        protected static void AppendAttributes(XmlDocument doc, XmlElement element, Dictionary<string, string> attributes)
        {
            foreach (KeyValuePair<String, String> attribute in attributes)
            {
                AppendAttribute(doc, element, attribute.Key, attribute.Value);
            }
        }

        protected static void AppendAttribute(XmlDocument doc, XmlElement element, string attributeName, string attributeValue)
        {
            XmlAttribute attribute = doc.CreateAttribute(attributeName);
            attribute.Value = attributeValue;
            element.Attributes.Append(attribute);
        }

        #endregion // xml helpers
        #endregion // Protected Static Methods        
        #region Protected Methods
        // Xml Declaration
        protected void CreateXmlDeclaration(XmlDocument doc)
        {
            XmlDeclaration decl = doc.CreateXmlDeclaration("1.0", "UTF-8", string.Empty);
            doc.AppendChild(decl);
        }

        // This is the root node. It specifies the MSBuild version to use
        // and also the default target to be executed when this file is passed
        // to MSBuild.exe
        //
        // <Project DefaultTargets="Build" ToolsVersion="4.0" xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
        //
        protected XmlElement CreateXmlRoot(XmlDocument doc)
        {
            // Document root.            
            XmlElement root = doc.CreateElement("Project");
            doc.AppendChild(root);

            AppendAttributes(doc, root, new Dictionary<string, string> { { "DefaultTargets", "Build" }, 
                                                                         { "ToolsVersion", "4.0" }, 
                                                                         { "xmlns", @"http://schemas.microsoft.com/developer/msbuild/2003" } });

            return root;
        }
        #endregion // Protected Methods
    } // class MSBuildExporter
} // namespace ProjectGenerator.Exporters
