﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGenerator.Independent
{
    /// <summary>
    /// A project reference
    /// </summary
    [Serializable]
    public class ProjectReference
    {
        #region Properties
        public String Path { get; set; }
        public bool CopyLocal { get; set; }
        #endregion // Properties

        #region Constructors
        public ProjectReference()
        {
            Path = String.Empty;
            CopyLocal = true;
        }
        #endregion // Constructors
    }
}
