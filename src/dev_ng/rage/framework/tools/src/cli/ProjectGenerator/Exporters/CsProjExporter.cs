﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using RSG.Base.Logging;
using RSG.Base.Configuration;
using System.Text.RegularExpressions;

namespace ProjectGenerator.Exporters
{
    /// <summary>
    /// A Proj exporter ( CsProj )
    /// </summary>
    public abstract partial class ProjExporter
    {
        #region Public Constants
        /// <summary>
        /// Visual Studio fileType
        /// </summary> 
        public static readonly string VS_FILETYPE_CSPROJ = ".csproj";
        #endregion // Public Constants

        #region Private Constants
        public static readonly string CSPROJ_PRODUCT_VERSION = "8.0.30703";
        public static readonly string CSPROJ_SCHEMA_VERSION = "2.0";
        public static readonly string CSPROJ_FILE_ALIGNMENT = "512";
        #endregion // Private Constants

        #region Private Properties
        private string SuffixCsProj
        {
            get { return VS_FILETYPE_CSPROJ; }
        }

        private string StandardSuffixCsProj
        {
            get { return VsVersion + VS_FILETYPE_CSPROJ; }
        }

        private string MangledSuffixCsProj
        {
            get { return MangledSuffixNoExtension + VS_FILETYPE_CSPROJ; }
        }
        #endregion // Private Properties

        #region Private methods
        private bool ExportCsProj(ICollection<string> filenamesExported, bool verify)
        {
            bool ok = CreateCsProj(Project, filenamesExported, verify);
            return ok;
        }

        /// <summary>
        /// create the csproj file
        /// </summary>
        /// <param name="project"></param>
        /// <param name="filenamesExported"></param>
        /// <param name="verify"></param>
        /// <returns></returns>
        private bool CreateCsProj(Independent.Project project, ICollection<string> filenamesExported, bool verify)
        {
            string outputFilenameFullPath = GetCsProjOutputFilename(project);
            XmlDocument doc = CreateCsProjXmlDoc(outputFilenameFullPath, filenamesExported);

            FileStream stream = new FileStream(outputFilenameFullPath, System.IO.FileMode.Create, FileAccess.Write, FileShare.Read);
            doc.Save(stream);
            stream.Close();

            Unescape(outputFilenameFullPath);

            bool ok = true;

            if (verify)
            {
                ok = Verify(outputFilenameFullPath);
            }

            if (ok)
            {
                filenamesExported.Add(outputFilenameFullPath);
            }

            return ok;
        }

        /// <summary>
        /// 
        /// </summary>
        /*
          <PropertyGroup>
            <Configuration Condition=" '$(Configuration)' == '' ">Debug</Configuration>
            <Platform Condition=" '$(Platform)' == '' ">AnyCPU</Platform>
            <ProductVersion>8.0.30703</ProductVersion>
            <SchemaVersion>2.0</SchemaVersion>
            <ProjectGuid>{29E6A1C6-857C-4D03-AE65-FBC3232ABD5F}</ProjectGuid>
            <OutputType>Library</OutputType>
            <AppDesignerFolder>Properties</AppDesignerFolder>
            <RootNamespace>RSG.Pipeline.Automation.Common</RootNamespace>
            <AssemblyName>RSG.Pipeline.Automation.Common</AssemblyName>
            <TargetFrameworkVersion>v4.0</TargetFrameworkVersion>
            <FileAlignment>512</FileAlignment>
         */
        private void CreatePropertyGroupForBaseConfigurationCs(XmlDocument doc, XmlElement root, String outputFilename, ICollection<String> filenamesExported)
        {
            Independent.Target target = Project.Targets.First(n => n.Active);

            XmlElement propertyGroupElement = AppendElement(doc, root, MSBUILD_PROPERTYGROUP);
            root.AppendChild(propertyGroupElement);

            XmlElement configurationElement = AppendElementWithTextNode(doc, propertyGroupElement, "Configuration", target.Config);
            AppendAttribute(doc,configurationElement, "Condition", " '$(Configuration)' == '' ");

            XmlElement platformElement = AppendElementWithTextNode(doc, propertyGroupElement, "Platform", target.Platform.EnumToString());
            AppendAttribute(doc, platformElement, "Condition", " '$(Platform)' == '' ");

            AppendElementWithTextNode(doc, propertyGroupElement, "ProductVersion", CSPROJ_PRODUCT_VERSION);
            AppendElementWithTextNode(doc, propertyGroupElement, "SchemaVersion", CSPROJ_SCHEMA_VERSION);

            AppendElementWithTextNode(doc, propertyGroupElement, "ProjectGuid", GuidToXmlString(ProjectGuid(outputFilename, VsVersion, filenamesExported)));

            AppendElementWithTextNode(doc, propertyGroupElement, OutputTypeCs(target));

            AppendElementWithTextNode(doc, propertyGroupElement, "AppDesignerFolder", "Properties");

            String projectName = Path.GetFileNameWithoutExtension(outputFilename);

            AppendElementWithTextNode(doc, propertyGroupElement, "RootNamespace", projectName);
            AppendElementWithTextNode(doc, propertyGroupElement, "AssemblyName", projectName);

            if (String.IsNullOrEmpty(target.Settings.General.DotNetFrameworkVersion))
            {
                AppendElementWithTextNode(doc, propertyGroupElement, "TargetFrameworkVersion", "v4.0");
            }
            else
            {
                AppendElementWithTextNode(doc, propertyGroupElement, "TargetFrameworkVersion", String.Format("v{0}", target.Settings.General.DotNetFrameworkVersion));
            }
            
            AppendElementWithTextNode(doc, propertyGroupElement, "FileAlignment", CSPROJ_FILE_ALIGNMENT);
        }

        /// <summary>
        /// 
        /// </summary>
        /*
          <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Debug|AnyCPU' ">
            <DebugSymbols>true</DebugSymbols>
            <DebugType>full</DebugType>
            <Optimize>false</Optimize>
            <OutputPath>bin\Debug\</OutputPath>
            <DefineConstants>DEBUG;TRACE</DefineConstants>
            <ErrorReport>prompt</ErrorReport>
            <WarningLevel>4</WarningLevel>
        */
        private void CreatePropertyGroupForEachConfigurationCs(XmlDocument doc, XmlElement root)
        {
            foreach (Independent.Target target in Project.Targets.Where(n => n.Active))
            {
                XmlElement element = AppendElement(doc, root, MSBUILD_PROPERTYGROUP, target, false);
                {
                    AppendElementWithTextNode(doc, element, "PlatformTarget", target.Platform.EnumToString());
                    
                    if (!String.IsNullOrEmpty(target.Settings.Compiler.DebugSymbols)) 
                        AppendElementWithTextNode(doc, element, "DebugSymbols", target.Settings.Compiler.DebugSymbols);
                    if (!String.IsNullOrEmpty(target.Settings.Compiler.DebugType))
                        AppendElementWithTextNode(doc, element, "DebugType", target.Settings.Compiler.DebugType);
                    if (!String.IsNullOrEmpty(target.Settings.Compiler.Optimization))
                        AppendElementWithTextNode(doc, element, "Optimize", target.Settings.Compiler.Optimization);
                    if (!String.IsNullOrEmpty(target.Settings.General.OutputDirectoryName))
                    {
                        String relPath = target.Settings.General.OutputDirectoryName;
                        try
                        {
                            if (!target.Settings.General.OutputDirectoryName.StartsWith("."))
                            {
                                relPath = RelativeExpandedProjectPath(target.Settings.General.OutputDirectoryName);
                            }
                        }
                        catch
                        {
                            // Deliberately ignored - RelativeExpandedProjectPath can have problems with URI exceptions for which we ignore.
                        }
                        AppendElementWithTextNode(doc, element, "OutputPath", relPath);
                    }
                    
                    AppendElementWithTextNode(doc, element, "DefineConstants", Join(target.Settings.Compiler.PreprocessorDefinitions, "__VS" + VsVersion));
                    
                    if (!String.IsNullOrEmpty(target.Settings.Compiler.ErrorReport)) 
                        AppendElementWithTextNode(doc, element, "ErrorReport", target.Settings.Compiler.ErrorReport);
                    if (!String.IsNullOrEmpty(target.Settings.Compiler.WarningLevel)) 
                        AppendElementWithTextNode(doc, element, "WarningLevel", target.Settings.Compiler.WarningLevel);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /*
            <PropertyGroup>
                <ApplicationIcon>rockstar.ico</ApplicationIcon>
            </PropertyGroup>
         */
        private void CreatePropertyGroupAppIconCs(XmlDocument doc, XmlElement root)
        {
            ICollection<Independent.ProjectFile> sourceFiles = Project.AllSourceFiles();

            String icoExt = FileType.Ico.ToString();
            foreach (Independent.ProjectFile sourceFile in sourceFiles)
            {
                String ext = Path.GetExtension(sourceFile.Path).Replace(".","");
                if (0 == String.Compare(ext, icoExt, true))
                {
                    XmlElement element = AppendElement(doc, root, MSBUILD_PROPERTYGROUP);
                    AppendElementWithTextNode(doc, element, "ApplicationIcon", Path.GetFileName(sourceFile.Path));
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /*
            <ItemGroup>
                <EmbeddedResource Include="Resources\StringTable.resx">
                  <Generator>ResXFileCodeGenerator</Generator>
                  <LastGenOutput>StringTable.Designer.cs</LastGenOutput>
                </EmbeddedResource>
                <EmbeddedResource Include="Resources\CommandStringTable.resx" />
            </ItemGroup>
         */
        private void CreateItemGroupResx(XmlDocument doc, XmlElement root)
        {
            ICollection<Independent.ProjectFile> sourceFiles = Project.AllSourceFiles();

            String resxExt = FileType.Resx.ToString();
            foreach (Independent.ProjectFile sourceFile in sourceFiles)
            {
                String ext = Path.GetExtension(sourceFile.Path).Replace(".","");
                if (0 == String.Compare(ext, resxExt, true))
                {
                    XmlElement element = AppendElement(doc, root, MSBUILD_ITEMGROUP);
                    XmlElement newElement = AppendElementWithAttribute(doc, element, "EmbeddedResource", "Include", FilenameToRelFilterPath(sourceFile.Path));
                    foreach (KeyValuePair<String, String> kvp in sourceFile.ChildNodes)
                    {
                        AppendElementWithTextNode(doc, newElement, kvp.Key, kvp.Value);
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /*
           <ItemGroup>
            <Reference Include="System.Core" />
        */
        private void CreateItemGroupReferencesCs(XmlDocument doc, XmlElement root)
        {
            Independent.Target target = Project.Targets.First(n => n.Active);

            XmlElement element = AppendElement(doc, root, MSBUILD_ITEMGROUP);
            foreach (String reference in target.Settings.General.References)
            {
                AppendElementWithAttribute(doc, element, "Reference", "Include",reference);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /*
          <ItemGroup>
            <Compile Include="Batch\ProjectBatcher.cs" />
        */
        private void CreateItemGroupSourceFilesCs(XmlDocument doc, XmlElement root, FileType fileType)
        {          
            XmlElement element = AppendElement(doc, root, MSBUILD_ITEMGROUP);
            {
                foreach (Independent.ProjectDirectory dir in Project.Directories)
                {
                    RecurseDirectoryCs(doc, element, dir, fileType);
                }
            }      
        }

        /// <summary>
        /// 
        /// </summary>
        private void CreateItemGroupSourceFilesXaml(XmlDocument doc, XmlElement root, FileType fileType)
        {
            XmlElement element = AppendElement(doc, root, MSBUILD_ITEMGROUP);
            {
                foreach (Independent.ProjectDirectory dir in Project.Directories)
                {
                    RecurseDirectoryXaml(doc, element, dir, fileType);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /*
        <ItemGroup>
            <ProjectReference Include="..\..\..\..\..\..\..\..\..\3rdparty\dev\cli\P4.Net\src\P4API\P4API.csproj">
              <Project>{4706B526-42F0-420E-9CF2-B0AB775C8E47}</Project>
              <Name>P4API</Name>
        */
        private void CreateItemGroupProjectReferencesCs(XmlDocument doc, XmlElement root)
        {
            Independent.Target target = Project.Targets.First(n => n.Active);

            XmlElement element = AppendElement(doc, root, MSBUILD_ITEMGROUP);
            foreach (Independent.ProjectReference projectReference in target.Settings.General.ProjectReferences)
            {
                // convert project reference to relative path
                if (!File.Exists(projectReference.Path))
                {
                    Log.Error("Error: Project reference does not exist {0}", projectReference);
                }

                XmlElement projectRefElement = AppendElementWithAttribute(doc, element, "ProjectReference", "Include", projectReference.Path);

                String guidFilename = Path.GetFullPath(Path.ChangeExtension(projectReference.Path, "guid"));
                String guid = GuidUtils.UNKNOWN_GUID;

                if (File.Exists(guidFilename))
                {
                    guid = GuidUtils.ReadFile(guidFilename);
                }
                else
                {
                    Log.Error("Error: no Guid file found at {0}", guidFilename);
                }

                AppendElementWithTextNode(doc, projectRefElement, "Project", GuidToXmlString(guid));

                String projectName = Path.GetFileNameWithoutExtension(projectReference.Path);
                AppendElementWithTextNode(doc, projectRefElement, "Name", projectName);

                if (projectReference.CopyLocal == false)
                {
                    AppendElementWithTextNode(doc, projectRefElement, "Private", "False");
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /*
         * <Import Project="$(MSBuildToolsPath)\Microsoft.CSharp.targets" />
         */         
        private void CreateImportProjectTargetCs(XmlDocument doc, XmlElement root)
        {
            AppendElementWithAttribute(doc, root, MSBUILD_IMPORT, "Project", @"$(MSBuildToolsPath)\Microsoft.CSharp.targets", null, false);            
        }

        /// <summary>
        /// 
        /// </summary>
        /// 
        /*  <PropertyGroup>
            <PostBuildEvent>CALL $(RS_TOOLSROOT)\wildwest\script\dos\CopyMaxAssemblies.bat $(SolutionName) $(TargetPath) x64 $(MAX_DEPLOY) $(TargetFileName)</PostBuildEvent>
        */ 
        private void CreatePropertyGroupPostBuildEventCs(XmlDocument doc, XmlElement root)
        {
            Independent.Target target = Project.Targets.First(n => n.Active);

            XmlElement element = AppendElement(doc, root, MSBUILD_PROPERTYGROUP);
            Independent.ProjectBuildEvent postBuildEvent = target.Settings.BuildEvents.PostBuild;
            AppendElementWithTextNode(doc, element, "PostBuildEvent", postBuildEvent.Command);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /*
         *   <!-- To modify your build process, add your task inside one of the targets below and uncomment it. 
               Other similar extension points exist, see Microsoft.Common.targets.
          <Target Name="BeforeBuild">
          </Target>
          <Target Name="AfterBuild">
          </Target>
          -->
         * */
        private void CreateStandardProjectTaskCommentCs(XmlDocument doc, XmlElement root)
        { 
            // TODO : not important.
        }

        /// <summary>
        /// Create the entire xml doc - control logic
        /// </summary>
        private XmlDocument CreateCsProjXmlDoc(string outputFilename, ICollection<string> filenamesExported)
        {
            XmlDocument doc = new XmlDocument();
            CreateXmlDeclaration(doc);
            XmlElement root = CreateXmlRoot(doc);
            doc.InsertBefore(doc.CreateComment("PG3 generated (at some point in time - it *may* have been handedited since)."), root);
            
            CreatePropertyGroupForBaseConfigurationCs(doc, root, outputFilename, filenamesExported);
            CreatePropertyGroupAppIconCs(doc, root);            
            CreatePropertyGroupForEachConfigurationCs(doc, root);
            CreateItemGroupReferencesCs(doc, root);
            CreateItemGroupSourceFilesCs(doc, root, FileType.Cs);
            CreateItemGroupSourceFilesXaml(doc, root, FileType.Xaml);
            CreateItemGroupSourceFilesCs(doc, root, FileType.Datasource);            
            CreateItemGroupProjectReferencesCs(doc, root);
            CreateItemGroupSourceFilesCs(doc, root, FileType.Config);
            CreateItemGroupSourceFilesCs(doc, root, FileType.Ico);
            CreateItemGroupSourceFilesCs(doc, root, FileType.Png);
            // not added for now at least
            //CreateItemGroupSourceFilesCs(doc, root, FileType.Build);
            CreateItemGroupSourceFilesCs(doc, root, FileType.Txt);
            CreateItemGroupSourceFilesCs(doc, root, FileType.Makefile);
            CreateItemGroupResx(doc, root);
            CreateImportProjectTargetCs(doc, root);
            CreatePropertyGroupPostBuildEventCs(doc, root);
            CreateStandardProjectTaskCommentCs(doc, root);
            return doc;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        private string GetCsProjOutputFilename(Independent.Project project)
        {
            string outputFilename = project.Name + SuffixCsProj;//MangledSuffixCsProj;
            string outputFilenameFullPath = Path.Combine(Directory.GetCurrentDirectory(), outputFilename);
            return outputFilenameFullPath;
        }

        /// <summary>
        ///  Interprets independent format to predict the output filenames that will be generated.
        /// </summary>
        /// <param name="filenames">The filenames that will be generated</param>
        public void PreDetermineExportedFilesCsProj(ICollection<string> filenames)
        {
            if (IsProjectValid())
            {
                // Project files
                string csProjFullPath = GetCsProjOutputFilename(Project);
                filenames.Add(csProjFullPath);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        private KeyValuePair<String, String> OutputTypeCs(Independent.Target target)
        {
            string nodeName = "OutputType";
            switch (target.ProjectType.Standard)
            {
                case Independent.ProjectType.StandardValues.Lib:
                    return new KeyValuePair<String, String>(nodeName, "Library");
                case Independent.ProjectType.StandardValues.Exe:
                    return new KeyValuePair<String, String>(nodeName, "Exe");
                case Independent.ProjectType.StandardValues.Dll:
                    return new KeyValuePair<String, String>(nodeName, "????");
                case Independent.ProjectType.StandardValues.Custom:
                    return new KeyValuePair<String, String>(nodeName, target.ProjectType.Custom);
                case Independent.ProjectType.StandardValues.Invalid:
                    Log.Error("Error: an invalid ProjectType was encountered");
                    return new KeyValuePair<String, String>(nodeName, null);
                default:
                    Log.Error("Error: an unknown/unsupported ProjectType was encountered");
                    return new KeyValuePair<String, String>(nodeName, null);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="element"></param>
        /// <param name="directory"></param>
        private void RecurseDirectoryCs(XmlDocument doc, XmlElement element, Independent.ProjectDirectory directory, FileType extension = FileType.None)
        {
            foreach (Independent.ProjectDirectory dir in directory.Directories)
            {
                RecurseDirectoryCs(doc, element, dir, extension);
            }

            foreach (Independent.ProjectFile file in directory.Files)
            {
                string ext = Path.GetExtension(file.Path);
                FileType fileType = FileTypeExtensions.Create(Log, ext);
                
                if (extension == fileType)
                {                    
                    String filename = file.Path;
                    XmlElement elementAdded = null;

                    switch (fileType)
                    {
                        case FileType.Cs:
                            String relPath = FilenameToRelFilterPath(filename);
                            elementAdded = AppendElementWithAttribute(doc, element, "Compile", "Include", relPath);
                            
                            if (!file.ChildNodes.Any() && relPath.EndsWith(".xaml.cs", StringComparison.OrdinalIgnoreCase))
                            {
                                String stripXaml = relPath.Replace(".xaml.cs", String.Empty);
    
                                String xamlFilename = Path.ChangeExtension(stripXaml, FileType.Xaml.ToString());
                                AppendElementWithTextNode(doc, elementAdded, "DependentUpon", xamlFilename);                                
                            }
                            break;
                        case FileType.Config:
                        case FileType.Datasource:
                        case FileType.Makefile:
                        case FileType.Txt:
                        case FileType.Build:
                            elementAdded = AppendElementWithAttribute(doc, element, "None", "Include", FilenameToRelFilterPath(filename));
                            break;
                        case FileType.Ico:
                            elementAdded = AppendElementWithAttribute(doc, element, "Content", "Include", FilenameToRelFilterPath(filename));
                            break;
                        case FileType.Png:
                            elementAdded = AppendElementWithAttribute(doc, element, "Resource", "Include", FilenameToRelFilterPath(filename));
                            break;
                        default:
                            Log.Warning("File Extension {0} is not explicity handled @ {1} ( it is assumed to be a 'None'", ext, filename);
                            elementAdded = AppendElementWithAttribute(doc, element, "None", "Include", FilenameToRelFilterPath(filename));
                            break;
                    }

                    if (elementAdded != null)
                    {
                        foreach (KeyValuePair<String, String> kvp in file.ChildNodes)
                        {
                            AppendElementWithTextNode(doc, elementAdded, kvp.Key, kvp.Value);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="element"></param>
        /// <param name="directory"></param>
        private void RecurseDirectoryXaml(XmlDocument doc, XmlElement element, Independent.ProjectDirectory directory, FileType extension = FileType.None)
        {
            foreach (Independent.ProjectDirectory dir in directory.Directories)
            {
                RecurseDirectoryXaml(doc, element, dir, extension);
            }

            foreach (Independent.ProjectFile file in directory.Files)
            {
                string ext = Path.GetExtension(file.Path);
                FileType fileType = FileTypeExtensions.Create(Log, ext);

                if (extension == fileType)
                {
                    String filename = file.Path;
                    XmlElement elementAdded = null;

                    switch (fileType)
                    {
                        case FileType.Xaml:
                            // The build action is Page
                            // http://stackoverflow.com/questions/145752/what-are-the-various-build-action-settings-in-vs-net-project-properties-and-wh
                            // Page (wpf only): Used to compile a xaml file into baml. The baml is then embedded with the same technique as Resource (i.e. available as `AppName.g.resources)
                            // when other types of build action and custom tools are required we will be required to specify that on a file by file basis.
                            // since that defeats the purpose of the project generator we trust this won't be required - when if ever it is required a long hard think is required
                            // as the makefile will become 'exporter' non-agnostic.

                            elementAdded = AppendElementWithAttribute(doc, element, "Page", "Include", FilenameToRelFilterPath(filename));
                            if (!file.ChildNodes.Any())
                            {
                                // 
                                AppendElementWithTextNode(doc, elementAdded, "SubType", "Designer");
                                // The default custom tool
                                AppendElementWithTextNode(doc, elementAdded, "Generator", "MSBuild:Compile");
                            }
                            break;
                        default:
                            Log.Warning("File Extension {0} is not explicity handled @ {1} ( it is assumed to be a 'None'", ext, filename);
                            elementAdded = AppendElementWithAttribute(doc, element, "None", "Include", FilenameToRelFilterPath(filename));
                            break;
                    }

                    if (elementAdded != null)
                    {
                        foreach (KeyValuePair<String, String> kvp in file.ChildNodes)
                        {
                            AppendElementWithTextNode(doc, elementAdded, kvp.Key, kvp.Value);
                        }
                    }
                }
            }
        }
        #endregion // private methods
    }
}
