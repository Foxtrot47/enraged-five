﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;


namespace ProjectGenerator.Independent
{
    /// <summary>
    /// Debugger settings
    /// </summary>
    [Serializable]
    public class Debugger
    {
        #region Properties
        /// <summary>
        /// the command to run
        /// </summary>
        public string Command { get; set; }
        /// <summary>
        /// the arguments to the command
        /// </summary>
        public string CommandArguments { get; set; }
        /// <summary>
        /// the working direcotry in which to run
        /// </summary>
        public string WorkingDirectory { get; set; }
        #endregion // Properties
    } // class SettingsDebugger
} // namespace ProjectGenerator.Independent
