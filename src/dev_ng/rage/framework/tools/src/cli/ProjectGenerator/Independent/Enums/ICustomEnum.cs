﻿using System;

namespace ProjectGenerator.Independent
{
    /// <summary>
    /// Custom enum interface
    /// <remarks>allows inheriting class to supply custom enumarations that 'pass through' the enum definitions to the exporter</remarks>
    /// </summary>
    public interface ICustomEnum
    {
        #region Public Properties
        /// <summary>
        /// The custom string - NOT a standard enum.
        /// </summary>
        string Custom { get; set; }        
        #endregion // Public Properties        
    } // interface ICustomEnum
} // ProjectGenerator.Independent
