﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGenerator.Independent
{
    /// <summary>
    /// The independent setup of a custom command that can be attached to files
    /// </summary>
    [Serializable]
    public class CustomCommand
    {
        #region Public Properties
        /// <summary>
        /// The custom command for a custom file
        /// - DW: TODO improve & review
        /// </summary>
        public string Command { get; set; }
        /// <summary>
        /// The custom command description for a custom file
        /// - DW: TODO improve & review
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// The custom command outputs for a custom file
        /// - DW: TODO improve & review
        /// </summary>
        public string Outputs { get; set; }
        /// <summary>
        /// The custom command dependencies for a custom file
        /// - DW: TODO improve & review
        /// </summary>   
        public string Dependencies { get; set; }
        #endregion // Public Properties
    } // public class CustomCommand
} // namespace ProjectGenerator.Independent
