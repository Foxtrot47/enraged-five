﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using LitJson;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace ProjectGenerator.Independent
{
    /// <summary>
    /// An independent project 
    /// <remarks>the container of the 'internal' / 'independent' project format</remarks>
    /// </summary>
    [Serializable]
    public class Project
    {
        #region Properties
        /// <summary>
        /// The name of the project
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The path to the project
        /// </summary>
        public string Path { get; set; }
        /// <summary>
        /// The Root Directory of the project
        /// </summary>
        public string RootDirectory { get; set; }
        /// <summary>
        /// A collection of Project Directories
        /// </summary>
        public List<ProjectDirectory> Directories { get; set; }
        /// <summary>
        /// A collection of targets for the project
        /// </summary>
        public List<Target> Targets { get; set; }
        /// <summary>
        /// A collection of platforms
        /// </summary>
        public List<Platform> Platforms { get; set; }
        /// <summary>
        /// A description of how a unity build would behave if required.
        /// </summary>
        public UnityConfig Unity { get; set; }

        #endregion // properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public Project()
        {
            Directories = new List<ProjectDirectory>();
            Targets = new List<Target>();
            Platforms = new List<Platform>();
            Unity = new UnityConfig();
        }
        #endregion // Constructor(s)

        #region Static Public Methods
        /// <summary>
        /// Deserialise the JSON filename to an independent project
        /// <remarks>this allows for offline editing of the independent project and allowing it to reenter the conversion pipeline</remarks>
        /// <remarks>purely a debugging tool.</remarks>
        /// <remarks>I recommend using oxygen XML editor.</remarks>
        /// </summary>
        /// <param name="filename">the filename to interpret</param>
        /// <returns>the independent project</returns>
        static public Independent.Project DeserialiseJSON(string filename)
        {
            string jsonString = File.ReadAllText(filename);
            return JsonMapper.ToObject<Independent.Project>(jsonString);
        }
        #endregion // Static Public Methods
        #region Public Methods

        /// <summary>
        /// Flatten the project directory structure to get all source files.
        /// </summary>
        public ICollection<Independent.ProjectFile> AllSourceFiles()
        {
            List<Independent.ProjectFile> sourceFiles = new List<Independent.ProjectFile>();
            foreach (Independent.ProjectDirectory dir in Directories)
            {
                ICollection<Independent.ProjectFile> moreSourceFiles = AllSourceFilesRecurse(dir);
                sourceFiles.AddRange(moreSourceFiles);
            }
            return sourceFiles;
        }

        /// <summary>
        /// See AllSourceFiles - this is the recursive method it uses.
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        public List<Independent.ProjectFile> AllSourceFilesRecurse(Independent.ProjectDirectory directory)
        {
            List<Independent.ProjectFile> allSourceFiles = new List<Independent.ProjectFile>();
            foreach (Independent.ProjectDirectory dir in directory.Directories)
            {
                List<Independent.ProjectFile> files = AllSourceFilesRecurse(dir);
                allSourceFiles.AddRange(files);
            }

            foreach (Independent.ProjectFile file in directory.Files)
            {
                allSourceFiles.Add(file);
            }
            return allSourceFiles;
        }

        /// <summary>
        /// Is this a valid project
        /// - projects that don't have a name are not valid
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            return (Name != null && Name != String.Empty);
        }

        /// <summary>
        /// Process rules on the project
        /// <remarks>this is run per target transforming the independent settings that are stored in each target.</remarks>
        /// </summary>
        /// <param name="rules">The list of rules to process</param>
        public void ProcessRules(List<MakeRule> rules)
        {
            foreach (MakeRule rule in rules)
            {
                foreach (Target target in Targets)
                {
                    rule.Process(target);
                }
            }
        }

        /// <summary>
        /// JSON Serialisation
        /// <remarks>very handy, possible to manipulate, verify objects.</remarks>
        /// </summary>
        /// <param name="filename">The JSON filename serialise out.</param>
        public void SerialiseJSON(string filename)
        {
            string jsonString = JsonMapper.ToJson(this);
            File.WriteAllText(filename, jsonString);
        }
        #endregion // Public Methods
    } // class Project
} // namespace ProjectGenerator.Independent
