﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Reflection;
using System.Collections;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using Microsoft.Win32;

namespace ProjectGenerator.Exporters
{
    /// <summary>
    /// A Proj exporter ( Vcxproj )
    /// - This contains the common functionality for VS2010 & VS2012 etc.
    /// - Where necessary we can virtualise methods and override in the subclass.
    /// </summary>
    public abstract partial class ProjExporter
    {
        #region Public Constants
        /// <summary>
        /// Visual Studio fileType
        /// </summary> 
        public static readonly string VS_FILETYPE_VCXPROJ = ".vcxproj";
        #endregion // Public Constants

        #region Protected Constants
        // Internal version of visual studio project
        protected static readonly string PROJECT_FILE_VERSION = "10.0.30319.1";
        // Visual Studio filters filetype
        protected static readonly string FILTERS_FILETYPE = ".filters";

        protected static readonly string SDK_REF = "SDKReference";
        protected static readonly string XBOX_CONDITION = "'$(Platform)'=='Durango'"; 
        protected static readonly string XBOX_SERVICES_API = "Xbox Services API, Version=8.0";
        protected static readonly string XBOX_GAMECHAT_API = "Xbox GameChat API, Version=8.0";
        #endregion // Protected Constants

        #region Private Properties
        
        private string StandardSuffixVcxProj
        {
            get { return Unity ? "_" + VsVersion + UNITY + VS_FILETYPE_VCXPROJ : VsVersion + VS_FILETYPE_VCXPROJ; }
        }

        private string MangledSuffixVcxProj
        {
            get { return MangledSuffixNoExtension + VS_FILETYPE_VCXPROJ; }
        }
        #endregion // Private Properties

        #region Public Methods

        /// <summary>
        ///  Interprets independent format to predict the output filenames that will be generated.
        /// </summary>
        /// <param name="filenames">The filenames that will be generated</param>
        public void PreDetermineExportedFilesVcxProj(ICollection<string> filenames)
        {
            if (IsProjectValid())
            {
                // Unity files
                PreDetermineExportedUnityFiles(filenames);

                // Project files
                string vcxProjFullPath = GetVcxProjOutputFilename(Project);
                filenames.Add(vcxProjFullPath);

                // Filters
                string filtersFilenameFullPath = GetFiltersFilenameFullPath(GetFiltersFilename(Project));
                filenames.Add(filtersFilenameFullPath);
            }
        }

        #endregion // Public Methods
        #region Protected Methods
        // This contains project level settings such as ProjectGuid, 
        // RootNamespace, etc. These properties are not normally overridden
        // elsewhere in the project file. This group is not configuration 
        // dependent and so only one Globals group generally exists in the
        // project file.
        //
        //<PropertyGroup Label="Globals" />
        //		<ProjectName>game1_lib</ProjectName>
        //      <ProjectGuid>{C83FF5FE-FFE5-486B-BD32-C17F5A6EF073}</ProjectGuid>
        //
        protected virtual XmlElement CreateGlobals(XmlDocument doc, XmlElement root, string outputFilename, ICollection<string> filenamesExported)
        {
            XmlElement element = AppendElementWithAttribute(doc, root, MSBUILD_PROPERTYGROUP, "Label", "Globals", null, false);
            {
                AppendElementWithTextNode(doc, element, "ProjectName", Project.Name);
                AppendElementWithTextNode(doc, element, "ProjectGuid", GuidToXmlString(ProjectGuid(outputFilename, VsVersion, filenamesExported)));
                CreateVCTargetsPath(doc, element);

                // These have been added as a result of settings that occured when using the durango project wizard
                // It is important to note they are not really understood yet, so it is likely these elements will be for review.
                CreateRootNamespace(doc, element);
                CreateDefaultLanguage(doc, element);
                CreateKeyword(doc, element);
                CreateMinimalVisualStudioVersion(doc, element);
                CreateApplicationEnvironment(doc, element);
            }
            return element;
        }

        protected virtual XmlElement CreateVCTargetsPath(XmlDocument doc, XmlElement element)
        {
            return null;
        }

        protected virtual void CreateRootNamespace(XmlDocument doc, XmlElement element)
        {
        }

        protected virtual void CreateDefaultLanguage(XmlDocument doc, XmlElement element)
        {
        }

        protected virtual void CreateKeyword(XmlDocument doc, XmlElement element)
        {
        }

        protected virtual void CreateMinimalVisualStudioVersion(XmlDocument doc, XmlElement element)
        {
        }

        protected virtual void CreateApplicationEnvironment(XmlDocument doc, XmlElement element)
        {
        }  

        protected virtual void CreateUseDebugLibraries(XmlDocument doc, XmlElement element)
        {
        }
  
        protected virtual void CreateEmbedManifest(XmlDocument doc, XmlElement element)
        {
        }  

        protected virtual void CreateGenerateManifest(XmlDocument doc, XmlElement element)
        {
        }  

       // DW I *think* this is possibly VS2012 specific.
        protected virtual void CreateMetadataPath(XmlDocument doc, XmlElement element, Independent.Target target)
        {
        }

        #endregion // Protected Methods
        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filenamesExported"></param>
        /// <param name="verify"></param>
        /// <returns></returns>
        private bool ExportVcxProj(ICollection<string> filenamesExported, bool verify)
        {
            bool ok = true;

            // Before export transform the independent format for unity purposes.
            // This will exclude some files from the build.
            // Files excluded will be added to new unity files
            // These new unity files will be created.
            if (Unity && Project.Unity.Enabled)
            {
                UnityTransform(filenamesExported);
            }

            // Create the vcxproj file
            if (!CreateVcxProj(Project, filenamesExported, verify))
            {
                ok = false;
            }

            // Create the filter file
            CreateFilters(Project, filenamesExported);

            return ok;
        }

        /// <summary>
        /// create the vcxproj file
        /// </summary>
        /// <param name="project"></param>
        /// <param name="filenamesExported"></param>
        /// <param name="verify"></param>
        /// <returns></returns>
        private bool CreateVcxProj(Independent.Project project, ICollection<string> filenamesExported, bool verify)
        {
            string outputFilenameFullPath = GetVcxProjOutputFilename(project);
            XmlDocument doc = CreateVcxprojXmlDoc(project, outputFilenameFullPath, filenamesExported);

            FileStream stream = new FileStream(outputFilenameFullPath, System.IO.FileMode.Create, FileAccess.Write, FileShare.Read);
            doc.Save(stream);
            stream.Close();

            Unescape(outputFilenameFullPath);

            bool ok = true;

            if (verify)
            {
                ok = Verify(outputFilenameFullPath);
            }

            if (ok)
            {
                filenamesExported.Add(outputFilenameFullPath);
            }

            return ok;
        }

        // Create the entire xml doc, section by section of the horrid vcxproj format.
        private XmlDocument CreateVcxprojXmlDoc(Independent.Project project, string outputFilename, ICollection<string> filenamesExported)
        {
            XmlDocument doc = new XmlDocument();
            CreateXmlDeclaration(doc);
            XmlElement root = CreateXmlRoot(doc);
            CreateProjectConfigurations(doc, root);
            CreateGlobals(doc, root, outputFilename, filenamesExported);
            CreateImportPropertySheet(doc, root);
            CreatePropertyGroupForEachTarget(doc, root);
            CreateImportPropertySheetTools(doc, root);
            CreateImportGroupExtensionSettings(doc, root);
            CreateImportGroupPropertySheetsForEachTarget(doc, root);
            CreatePropertyGroupUserMacros(doc, root);
            CreatePropertyGroupProjectFileVersion(doc, root, project);
            CreatePropertyGroupEmptyLabelForEachTarget(doc, root);
            CreateItemDefinitionGroupEmptyLabelForEachTarget(doc, root);
            CreateItemGroupManifests(doc, root);
            if (IsExecutableProject(project))
            {
                CreateItemGroupXboxServicesApi(doc, root);
            }
            CreateItemGroupSourceFiles(doc, root);
            CreateImportTargets(doc, root);
            CreateItemGroupProjectReferences(doc, root, filenamesExported);
            CreateImportGroupExtensionTargets(doc, root);
            return doc;
        }

        private bool IsExecutableProject(Independent.Project project)
        {
            return (project.Targets.FirstOrDefault() != null &&
                    project.Targets.FirstOrDefault().ProjectType.Standard == Independent.ProjectType.StandardValues.Exe);
        }

        private string GetVcxProjOutputFilename(Independent.Project project)
        {
            string outputFilename = project.Name + MangledSuffixVcxProj;
            string outputFilenameFullPath = Path.Combine(Directory.GetCurrentDirectory(), outputFilename);
            return outputFilenameFullPath;
        }

        #region Filters

        private void CreateFiltersItemGroupFilters(XmlDocument doc, XmlElement root)
        {
            List<string> filtersAdded = new List<string>();

            XmlElement element = AppendElement(doc, root, MSBUILD_ITEMGROUP);
            {
                foreach (Independent.ProjectDirectory dir in Project.Directories) // top level directory to be ignored, its a container.
                {
                    RecurseDirectoryFilter(doc, element, dir, filtersAdded);
                }
            }
        }

        /// <summary>
        /// Creates a filters file that maps files to folders
        /// </summary>
        /// <param name="project"></param>
        /// <param name="filenamesExported"></param>
        private void CreateFilters(Independent.Project project, ICollection<string> filenamesExported)
        {
            XmlDocument doc = CreateFiltersXmlDoc();
            string outputFilename = GetFiltersFilename(project);
            string outputFilenameFullPath = GetFiltersFilenameFullPath(outputFilename);
            FileStream stream = new FileStream(outputFilename, System.IO.FileMode.Create, FileAccess.Write, FileShare.Read);
            doc.Save(stream);
            stream.Close();
            filenamesExported.Add(outputFilenameFullPath);
        }

        private string GetFiltersFilename(Independent.Project project)
        {
            string outputFilename = project.Name + MangledSuffixVcxProj + FILTERS_FILETYPE;
            return outputFilename;
        }

        private void RecurseDirectoryFilter(XmlDocument doc, XmlElement element, Independent.ProjectDirectory directory, List<string> filtersAdded, string remove = null)
        {
            bool setRemove = (remove == null);

            foreach (Independent.ProjectDirectory dir in directory.Directories)
            {
                if (setRemove)
                {
                    // Any 'back path' as this is required to be removed from the filters for mega projects
                    remove = BackPath(Path.GetDirectoryName(dir.Path));
                }

                RecurseDirectoryFilter(doc, element, dir, filtersAdded, remove);

                string filterDir = PathToFilterPath(dir.Path, remove);

                if (!filtersAdded.Contains(filterDir)) // no duplicates
                {
                    if (filterDir != ".")
                    {
                        XmlElement filterElement = AppendElementWithAttribute(doc, element, "Filter", "Include", filterDir);
                        string guid = PseudoGuid(dir.Path); // this will differ if other users use a different project directory root
                        AppendElementWithTextNode(doc, filterElement, "UniqueIdentifier", GuidToXmlString(guid));
                        filtersAdded.Add(filterDir);
                    }
                }
            }
        }

        private void CreateFiltersItemGroupByExtensionFilters(XmlDocument doc, XmlElement root, FileType extension)
        {
            XmlElement newElement = CreateElement(doc, "ItemGroup");
            bool added = false; // so to add the element to the root only once! - this prevents unnecessary nodes.
            int depth = 0;
            foreach (Independent.ProjectDirectory dir in Project.Directories) // top directory is a container
            {
                if (FiltersRecurseDirectoryByExtentionFilters(doc, newElement, dir, extension, depth) && !added)
                {
                    added = true;
                    root.AppendChild(newElement);
                }
            }
        }

        private bool FiltersRecurseDirectoryByExtentionFilters(XmlDocument doc, XmlElement element, Independent.ProjectDirectory directory, FileType extension, int depth, string remove = null)
        {
            bool matched = false;

            foreach (Independent.ProjectFile file in directory.Files)
            {
                string ext = Path.GetExtension(file.Path);
                FileType fileType = FileTypeExtensions.Create(Log, ext);

                // If a virtual directory be sure to know this so that we set the appropriate filter for the file
                string virtDir = directory.IsVirtual ? PathToFilterPath(directory.Path, remove) : null;
                XmlElement newElement = null;

                if (file.Parse && extension == FileType.Psc)
                {
                    newElement = AppendElementWithAttribute(doc, element, "CustomBuild", "Include", FilenameToRelFilterPath(file.Path + extension.EnumToString()));
                }
                else if (file.Custom)
                {
                    if (extension == FileType.None || extension == fileType)
                    {
                        if ((file.CustomCommand != null && file.CustomCommand.Count() > 0) ||
                            fileType == FileType.Job || fileType == FileType.Frag || fileType == FileType.Task) // DW :seems like the enum needs a new attribute
                        {
                            newElement = AppendElementWithAttribute(doc, element, "CustomBuild", "Include", FilenameToRelFilterPath(file.Path));
                        }
                        else
                        {
                            newElement = AppendElementWithAttribute(doc, element, "None", "Include", FilenameToRelFilterPath(file.Path));
                        }
                    }
                }
                else
                {
                    if (extension == FileType.None || extension == fileType)
                    {
                        switch (fileType)
                        {
                            case FileType.Cpp:
                            case FileType.C:
                                newElement = AppendElementWithAttribute(doc, element, "ClCompile", "Include", FilenameToRelFilterPath(file.Path));
                                break;
                            case FileType.Rc:
                                newElement = AppendElementWithAttribute(doc, element, "ResourceCompile", "Include", FilenameToRelFilterPath(file.Path));                                
                                break;
                            case FileType.Inl:
                            case FileType.H:
                                newElement = AppendElementWithAttribute(doc, element, "ClInclude", "Include", FilenameToRelFilterPath(file.Path));
                                break;
                            case FileType.Sch:
                            case FileType.Txt:
                            case FileType.Build:
                            case FileType.Makefile:                                
                            case FileType.Spa:
                            case FileType.Dtx:
                            case FileType.Inc:
                            case FileType.Fxh:
                            case FileType.Bat:
							case FileType.Xsd:
                                newElement = AppendElementWithAttribute(doc, element, "None", "Include", FilenameToRelFilterPath(file.Path));
                                break;
                            case FileType.Appxmanifest:
                                newElement = AppendElementWithAttribute(doc, element, "AppxManifest", "Include", FilenameToRelFilterPath(file.Path));
                                break;
                            case FileType.Png:
                                newElement = AppendElementWithAttribute(doc, element, "Image", "Include", FilenameToRelFilterPath(file.Path));
                                break;
                            default:
                                //if (!file.Parse)
                                {
                                    Log.Error("File Extension {0} is unhandled @ {1}", ext, file.Path);
                                }
                                break;
                        }
                    }
                }

                if (newElement != null)
                {
                    AddFilterForFileExtension(doc, depth, remove, file, virtDir, newElement);
                    matched = true;
                }
            }

            // mega project support - removes the backpath at the first tier of directories only. ( this is getting complicated ) 
            bool setRemove = (remove == null);

            foreach (Independent.ProjectDirectory dir in directory.Directories)
            {
                if (setRemove)
                {
                    remove = BackPath(Path.GetDirectoryName(dir.Path));
                }

                if (FiltersRecurseDirectoryByExtentionFilters(doc, element, dir, extension, depth + 1, remove))
                {
                    matched = true;
                }
            }

            return matched;
        }

        private void AddFilterForFileExtension(XmlDocument doc, int depth, string remove, Independent.ProjectFile file, string virtDir, XmlElement clCompileElement)
        {
            if (depth != 0)
            {
                AppendElementWithTextNode(doc, clCompileElement, "Filter", virtDir != null ? virtDir : PathToFilterPath(Path.GetDirectoryName(file.Path), remove));
            }
        }
        #endregion // filters    

        #region Xmldoc creation

        //This contains the project configurations known to the project
        //     (such as Debug|Win32 and Release|Win32).
        //
        //<ItemGroup Label="ProjectConfigurations">
        //  <ProjectConfiguration Include="Debug|PS3">
        //	    <Configuration>Debug</Configuration>
        //	    <Platform>PS3</Platform>
        //
        private XmlElement CreateProjectConfigurations(XmlDocument doc, XmlElement root)
        {
            XmlElement element = AppendElementWithAttribute(doc, root, MSBUILD_ITEMGROUP, "Label", "ProjectConfigurations", null, false);
            {
                foreach (Independent.Target target in Project.Targets.Where(n => n.Active))
                {
                    XmlElement projectConfiguration = AppendElementWithAttribute(doc, element, "ProjectConfiguration", "Include", target.Name);
                    {
                        AppendElementWithTextNode(doc, projectConfiguration, "Configuration", target.Config);
                        AppendElementWithTextNode(doc, projectConfiguration, "Platform", target.Platform.EnumToString());
                    }
                }
            }
            return element;
        }

        // This property sheet contains the default settings for a VC++
        // project. It contains definitions of all the project settings such
        // as Platform, PlatformToolset, OutputPath, TargetName, UseOfAtl, 
        // etc. and also all the item definition group defaults for each known
        // item group. In general, properties in this file are not tool-specific.
        //
        // <Import Project="$(VCTargetsPath)\Microsoft.Cpp.default.props" />
        //
        private XmlElement CreateImportPropertySheet(XmlDocument doc, XmlElement root)
        {
            return AppendElementWithAttribute(doc, root, MSBUILD_IMPORT, "Project", @"$(VCTargetsPath)\Microsoft.Cpp.default.props", null, false);
        }

        /// <summary>
        /// Fix symbol file getting overwritten on x64 Debug. mspdbsrv.exe generates a valid PDB file but it's later overwritten by MSBuild.exe. 
        /// We avoid it by enabling variable "SkipCopyingSymbolsToOutputDirectory" inside the project file.
        /// - Not exposed to rules - this is quirk of C:\Windows\Microsoft.NET\Framework\v3.5\Microsoft.Common.targets for the Debug target only!
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="element"></param>
        /// <param name="target"></param>
        private void CreateSkipCopyingSymbolsToOutputDirectory(XmlDocument doc, XmlElement element, Independent.Target target)
        {
            //  C:\Windows\Microsoft.NET\Framework\v3.5\Microsoft.Common.targets
            //          <DebugSymbols Condition=" '$(ConfigurationName)' == 'Debug' and '$(DebugSymbols)' == '' and '$(DebugType)'==''">true</DebugSymbols>
            //          <_DebugSymbolsProduced>false</_DebugSymbolsProduced>
            //          <_DebugSymbolsProduced Condition="'$(DebugSymbols)'=='true'">true</_DebugSymbolsProduced>
            //
            //  C:\Windows\Microsoft.NET\Framework\v3.5\Microsoft.Common.targets uses this...
            //          <!-- Copy the debug information file (.pdb), if any -->
            //          <Copy
            //              SourceFiles="@(_DebugSymbolsIntermediatePath)"
            //              DestinationFiles="@(_DebugSymbolsOutputPath)"
            //              SkipUnchangedFiles="true"
            //              OverwriteReadOnlyFiles="$(OverwriteReadOnlyFiles)"
            //              Condition="'$(_DebugSymbolsProduced)'=='true' and '$(SkipCopyingSymbolsToOutputDirectory)' != 'true'">
            //              <Output TaskParameter="DestinationFiles" ItemName="FileWrites"/>
            //          </Copy>


            // DW: This needs reviewed, it's not good.
            if ((target.Platform == Platform.Win32 || target.Platform == Platform.x64 || target.Platform == Platform.Win64 || target.Platform == Platform.Durango) &&
                (target.Config == "Debug" || target.Config == "Beta" || target.Config == "Release") && 
                target.ProjectType.Standard == Independent.ProjectType.StandardValues.Exe)
            {
                AppendElementWithTextNode(doc, element, "SkipCopyingSymbolsToOutputDirectory", "true");
            }
        }

        // This property group has an attached configuration condition 
        // (such as Condition="'$(Configuration)|$(Platform)'=='Debug|Win32'")
        // and comes in multiple copies, one per configuration. This property
        // group hosts configuration-wide properties. These properties control
        // the inclusion of system property sheets in Microsoft.Cpp.props.
        // E.g. if we define the property <CharacterSet>Unicode</CharacterSet>,
        // then the system property sheet microsoft.Cpp.unicodesupport.props 
        // will be included (as can be seen in the Property Manager). Indeed,
        // in one of the imports of Microsoft.Cpp.props, we can see the line:
        //
        // <Import Condition="'$(CharacterSet)' == 'Unicode'"   Project="$(VCTargetsPath)\microsoft.Cpp.unicodesupport.props"/> -->
        //      <PropertyGroup Label="Configuration" />
        //
        //<PropertyGroup Label="Configuration" Condition="'$(Configuration)|$(Platform)'=='Debug|PS3'">
        //    <ConfigurationType>StaticLibrary</ConfigurationType>
        //    <CharacterSet>MultiByte</CharacterSet>
        //    <PlatformToolset>SNC</PlatformToolset>
        //    <ExceptionsAndRtti>NoExceptsWithRtti</ExceptionsAndRtti>
        //</PropertyGroup>
        //
        private void CreatePropertyGroupForEachTarget(XmlDocument doc, XmlElement root)
        {
            foreach (Independent.Target target in Project.Targets.Where(n => n.Active))
            {
                XmlElement element = AppendElementWithAttribute(doc, root, MSBUILD_PROPERTYGROUP, "Label", "Configuration", target, false);
                {
                    AppendElementWithTextNode(doc, element, ProjectType(target));
                    AppendElementWithTextNode(doc, element, CharacterSet(target));
                    AppendElementWithTextNode(doc, element, PlatformToolset(target));

                    // DW : I'm fairly sure this node is only required for PS3
                    if (target.Platform == Platform.Ps3)
                    {
                        AppendElementWithTextNode(doc, element, ExceptionsAndRtti(target));
                    }

                    CreateUseDebugLibraries(doc, element);
                    CreateEmbedManifest(doc, element);
					CreateGenerateManifest(doc, element);
					AppendElementWithTextNode(doc, element, WholeProgramOptimization(target));

                    CreateSkipCopyingSymbolsToOutputDirectory(doc, element, target);
                }
            }
        }

        // This property sheet (directly or via imports) defines the 
        // default values for many tool-specific properties such as the 
        // compiler’s Optimization, WarningLevel properties, Midl tool’s
        // TypeLibraryName property, etc. In addition, it imports various
        // system property sheets based on configuration properties defined
        // in the property group immediately above.  
        //
        // <Import Project="$(VCTargetsPath)\Microsoft.Cpp.props" />
        //
        private XmlElement CreateImportPropertySheetTools(XmlDocument doc, XmlElement root)
        {
            return AppendElementWithAttribute(doc, root, MSBUILD_IMPORT, "Project", @"$(VCTargetsPath)\Microsoft.Cpp.props", null, false);
        }

        // This group contains imports for the property sheets that
        // are part of Build Customizations (or Custom Build Rules as this
        // feature was called in earlier editions). A Build Customization
        // is defined by up to three files – a .targets file, a .props file
        // and .xml file. This import group contains the imports for the
        // .props file.  
        //
        // <ImportGroup Label="ExtensionSettings" />
        //
        private XmlElement CreateImportGroupExtensionSettings(XmlDocument doc, XmlElement root)
        {
            return AppendElementWithAttribute(doc, root, MSBUILD_IMPORTGROUP, "Label", "ExtensionSettings", null, false);
        }

        // This group contains the imports for user property sheets.
        // These are the property sheets you add through the Property 
        // Manager view in VS. The order in which these imports are listed
        // is relevant and is reflected in the Property Manager. The project
        // file normally contains multiple instances of this kind of import
        // group, one for each project configuration.  
        //
        // 	<ImportGroup Label="PropertySheets" Condition="'$(Configuration)|$(Platform)'=='Debug|PS3'">
        //      <Import Label="LocalAppDataPlatform" Project="$(UserRootDir)\Microsoft.Cpp.$(Platform).user.props" Condition="exists('$(UserRootDir)\Microsoft.Cpp.$(Platform).user.props')"/>
        //      <Import Project="$(VCTargetsPath)Microsoft.CPP.UpgradeFromVC71.props"/>
        //  </ImportGroup>
        private void CreateImportGroupPropertySheetsForEachTarget(XmlDocument doc, XmlElement root)
        {
            foreach (Independent.Target target in Project.Targets.Where(n => n.Active))
            {
                XmlElement element = AppendElementWithAttribute(doc, root, MSBUILD_IMPORTGROUP, "Label", "PropertySheets", target, false);
                {
                    AppendElementWithAttributes(doc, element, MSBUILD_IMPORT, new Dictionary<string, string> {      { "Label", "LocalAppDataPlatform" }, 
                                                                                                                    { "Project", @"$(UserRootDir)\Microsoft.Cpp.$(Platform).user.props" }, 
                                                                                                                    { "Condition", @"exists('$(UserRootDir)\Microsoft.Cpp.$(Platform).user.props')" } });


                    string customPropFilename = Project.Name + MangledSuffixNoExtension + ".props";

                    AppendElementWithAttributes(doc, element, MSBUILD_IMPORT, new Dictionary<string, string> {      { "Project", customPropFilename }, 
                                                                                                                    { "Condition", @"exists('" + customPropFilename + "')" } });

                    //       AppendElementWithAttribute(doc, element, MSBUILD_IMPORT, "Project", @"$(VCTargetsPath)Microsoft.CPP.UpgradeFromVC71.props");
                }
            }
        }

        // UserMacros are as variables used to customize your build 
        // process. E.g. you can define a user macro to define your custom
        // output path as $(CustomOutpuPath) and use it to define other 
        // variables. This property group houses such properties. Note that
        // in VS20xx, this group is not populated by the IDE since we do not
        // support user macros for configurations (we do for property sheets
        // though).  
        //
        // <PropertyGroup Label="UserMacros" />
        // 
        private XmlElement CreatePropertyGroupUserMacros(XmlDocument doc, XmlElement root)
        {
            return AppendElementWithAttribute(doc, root, MSBUILD_PROPERTYGROUP, "Label", "UserMacros", null, false);
        }

        // I've forgotten what this was all about
        // <_ProjectFileVersion>10.0.30319.1</_ProjectFileVersion>
        private XmlElement CreatePropertyGroupProjectFileVersion(XmlDocument doc, XmlElement root, Independent.Project project)
        {
            XmlElement propertyGroupElement = AppendElement(doc, root, MSBUILD_PROPERTYGROUP, null, false);
            {
                AppendElementWithTextNode(doc, propertyGroupElement, "_ProjectFileVersion", PROJECT_FILE_VERSION);

                Independent.Target target = project.Targets.First();
                if (target != null && target.Settings.General.IsolateConfigurationsOnDeploy == true && IsExecutableProject(project))
                {
                    AppendElementWithTextNode(doc, propertyGroupElement, "IsolateConfigurationsOnDeploy", target.Settings.General.IsolateConfigurationsOnDeploy.ToString());
                }
            }
            return propertyGroupElement;
        }

        // This property group normally comes with a configuration 
        // condition attached. You will also see multiple instances of the 
        // property group, one per configuration. It differs in its identity
        // from the other property groups above by the fact that it does 
        // not have a label (to look at it in another way, it has a label 
        // that is equal to the empty string). This group contains project 
        // configuration level settings. These settings apply to all files
        // that are part of the specified item group. Build Customization
        // item definition metadata also gets initialized here.
        //
        // <PropertyGroup />
        //  <_ProjectFileVersion>10.0.30319.1</_ProjectFileVersion>
        //  <OutDir Condition="'$(Configuration)|$(Platform)'=='Debug|PS3'">psn_debug_unity_2010\</OutDir>
        //  <IntDir Condition="'$(Configuration)|$(Platform)'=='Debug|PS3'">$(OutDir)</IntDir>
        //  <CustomBuildBeforeTargets Condition="'$(Configuration)|$(Platform)'=='Debug|PS3'">InitializeBuildStatus</CustomBuildBeforeTargets>
        //  <OutputFile Condition="'$(Configuration)|$(Platform)'=='Beta|Xbox 360'">$(OutDir)$(TargetName)$(TargetExt)</OutputFile>
        private void CreatePropertyGroupEmptyLabelForEachTarget(XmlDocument doc, XmlElement root)
        {
            foreach (Independent.Target target in Project.Targets.Where(n => n.Active))
            {
                XmlElement element = AppendElement(doc, root, MSBUILD_PROPERTYGROUP, target, false);
                {
                    if (target.Settings.General.OutputDirectoryName != null && target.Settings.General.OutputDirectoryName.Length > 0)
                    {
                        AppendElementWithTextNode(doc, element, "OutDir", target.Settings.General.OutputDirectoryName);
                    }
                    if (target.Settings.General.IntermediateDirectoryName != null && target.Settings.General.IntermediateDirectoryName.Length > 0)
                    {
                        AppendElementWithTextNode(doc, element, "IntDir", target.Settings.General.IntermediateDirectoryName);
                    }
                    if (target.Settings.General.IncludePath.Count > 0)
                    {
                        AppendElementWithTextNode(doc, element, "IncludePath", Join(target.Settings.General.IncludePath, "$(IncludePath)"));
                    }
                    if (target.Settings.General.LibraryPath.Count > 0)
                    {
                        AppendElementWithTextNode(doc, element, "LibraryPath", Join(target.Settings.General.LibraryPath, "$(LibraryPath)"));
                    }
                    if (target.Settings.General.ReferencePath.Count > 0)
                    {
                        AppendElementWithTextNode(doc, element, "ReferencePath", Join(target.Settings.General.ReferencePath, "$(ReferencePath)"));
                    }
                    if (target.Settings.General.ExecutablePath.Count > 0)
                    {
                        AppendElementWithTextNode(doc, element, "ExecutablePath", Join(target.Settings.General.ExecutablePath, "$(ExecutablePath)"));
                    }
                    if (target.Settings.General.MetadataPath.Count > 0)
                    {
                        CreateMetadataPath(doc, element, target);
                    }
                    AppendElementWithTextNode(doc, element, "CustomBuildBeforeTargets", "InitializeBuildStatus");
                    if (target.Settings.General.TargetName != null && target.Settings.General.TargetName.Length > 0)
                    {
                        AppendElementWithTextNode(doc, element, "TargetName", target.Settings.General.TargetName);//"$(ProjectName)"); // may only be required for PS3?
                    }

                    AppendElementWithTextNode(doc, element, "OutputFile", "$(OutDir)$(TargetName)$(TargetExt)");
                    AppendElementWithTextNode(doc, element, "LinkIncremental", "false");

                    if (target.Platform == Platform.Xbox360)
                    {
                        AppendElementWithTextNode(doc, element, "ImageXexOutput", target.Settings.Image.OutputFilename);
                        AppendElementWithTextNode(doc, element, "RemoteRoot", target.Settings.Deploy.RemoteRoot);
                    }
                }
            }
        }

        // Similar to the property group immediately above, but it 
        // contains item definitions and item definition metadata instead
        // of properties.
        //
        // <ItemDefinitionGroup Condition="'$(Configuration)|$(Platform)'=='Debug|PS3'">
        // <PreBuildEvent>
        //      <Command>pushd ..\game
        // </PreBuildEvent>
        //
        // <PreLinkEvent>
        //      <Command>$(RS_TOOLSROOT)/bin/coding/python/stripobjects.exe %(FullPath) $(OutDir)</Command>
        //      <Message>Stripping debug symbols</Message>
        // </PreLinkEvent>
        //
        // <Lib>
        // 		<ModuleDefinitionFile>3</ModuleDefinitionFile>
        // 		<AdditionalDependencies>%(AdditionalDependencies)</AdditionalDependencies>
        // 	    <AdditionalOptions>%(AdditionalOptions)</AdditionalOptions>
        // </Lib>
        //
        // <ClCompile>
        // 		<ProgramDataBaseFileName>$(IntDir)$(TargetName).pdb</ProgramDataBaseFileName>
        // 		<DebugInformationFormat>ProgramDatabase</DebugInformationFormat>
        // 	    <CompileAs>Default</CompileAs>
        //      <AdditionalIncludeDirectories>$(RAGE_DIR)\stlport\STLport-5.0RC5\stlport;$(RAGE_DIR)\base\src;$(RAGE_DIR)\suite\src;$(ProjectDir)\..;$(RAGE_DIR)\naturalmotion\include;$(RAGE_DIR)\naturalmotion\src;$(RAGE_DIR)\script\src;$(RAGE_DIR)\framework\src;%(AdditionalIncludeDirectories)</AdditionalIncludeDirectories>
        //      <PreprocessorDefinitions>SN_TARGET_PS3;NDEBUG;__SNC__;__VS20xx;%(PreprocessorDefinitions)</PreprocessorDefinitions>
        // 		<ForcedIncludeFiles>$(RAGE_DIR)\base\src\forceinclude\psn_debug.h;game_config.h;basetypes.h;%(ForcedIncludeFiles)</ForcedIncludeFiles>
        // 	    <PrecompiledHeader>NotUsing</PrecompiledHeader>
        // 	    <PrecompiledHeaderFile/>
        // 		<DisableSpecificWarnings>552;178</DisableSpecificWarnings>
        // 	    <AdditionalOptions> %(AdditionalOptions)</AdditionalOptions>
        // 	</ClCompile>

        /// <summary>
        /// Get a list of all the parser files that are not PSC.
        /// recurses and construct a pre build commandline.
        /// </summary>
        /// <param name="dir"></param>
        /// <param name="parserFiles"></param>
        private void BuildPrebuildCommand(Independent.ProjectDirectory dir, Independent.ProjectDirectory lastNonVirtualDir, ref string prebuildString)
        {
            if (lastNonVirtualDir.IsVirtual)
            {
                Log.Error("A Virtual Directory is thought of as non virtual.");
            }

            List<Independent.ProjectFile> parserFiles = new List<Independent.ProjectFile>();
            foreach (Independent.ProjectFile file in dir.Files.Where(n => n.Parse))
            {
                string pscFilename = file.Path + FileType.Psc.EnumToString();

                // ONLY IF A .PSC DOES NOT EXIST DO WE NEED TO ADD PARCODEGEN PROCESSING AS A PREBUILD EVENT.
                // IDEALLY THE DEPENDENCIES / OUTPUTS WOULD BE CAREFULLY CRAFTED TO UNDERSTAND THIS AND WE
                // COULD DO WITHOUT SUCH A STEP?
                //
                // as discussed ith Russ Schaff below...
                //
                // Yep that’s why we needed to use the prebuild event. Dependency checking is probably a lot simpler with PSC files, but there are still a good number of structdefs that aren't in PSC files yet. 
                // That said - I'm not sure which is less work - getting proper dependency checking when not using PSC files, or moving all the existing <structdef>s into PSC files and just standardizing on that as the way to do reflection info from now on. 
                // Looks like there are 115 files that have a <structdef> in them right now.
                //
                // As I remember the rules we follow right now, it's something like this:
                //
                // For each entry 'f' in the Parse {} section of a makefile.txt:
                //    If there is an 'f.psc':
                //            Add the psc to the project as a custom build step
                //            If 'f.psc' contains a generate="class" attribue:
                //                            Set the outputs to f_parser.h, f.cpp, f.h
                //            Else:
                //                            Set the outputs to f_parser.h
                //   Else:
                //          Add a parCodeGen invocation to the prebuild step for the project
                //          (Not sure if we set outputs here? It would be just f_parser.h at any rate)
                //
                // The --depends argument is for doing some additional dependency checking _within_ parCodeGen because I couldn't get it to happen in a better way in VS. 
                // Basically whenever parCodeGen.zip is out of date, we want to regenerate all the project files. 
                // parCodeGen internally does a time stamp check comparing the input file (the .cpp or .psc) vs. the output files and only regenerates the output files if they are too old. 
                // With --depends we also check the .zip. If we could do this within MSBuild that'd probably be faster, because then we could save on a bunch of python invocations.

                if (!File.Exists(pscFilename))
                {
                    parserFiles.Add(file);
                }
            }

            // DW: TODO : This should really be replaced by a script that hides the complexity from the vcxproj file and other exporters
            if (parserFiles.Count() > 0)
            {
                string relPath = PathToFilterPath2(lastNonVirtualDir.Path);

                if (!Directory.Exists(lastNonVirtualDir.Path))
                {
                    Log.Error("A directory {0} doesnt exists for which we are using this for parcodegen prebuild", lastNonVirtualDir.Path);
                }

                prebuildString += "\npushd " + relPath + "\n";
                prebuildString += PARCODEGEN_EXECUTABLE;
                prebuildString += " " + "--depends " + PARCODEGEN_DEPENDENCY;
                foreach (Independent.ProjectFile file in parserFiles)
                {
                    prebuildString += " " + Path.GetFileNameWithoutExtension(file.Path);
                }
                prebuildString += "\npopd";
            }




            foreach (Independent.ProjectDirectory subdir in dir.Directories)
            {
                if (!subdir.IsVirtual)
                {
                    lastNonVirtualDir = subdir;
                }

                BuildPrebuildCommand(subdir, lastNonVirtualDir, ref prebuildString);
            }
        }

        private void CreateItemDefinitionGroupEmptyLabelForEachTarget(XmlDocument doc, XmlElement root)
        {
            foreach (Independent.Target target in Project.Targets.Where(n => n.Active))
            {
                XmlElement element = AppendElement(doc, root, MSBUILD_ITEMDEFINITIONGROUP, target, false);
                {
                    AddPreBuildEvent(doc, element);
                    AddPreLinkEvent(doc, target, element);
                    AddLink(doc, target, element);
                    AddLibrarian(doc, target, element);
                    AddCompile(doc, target, element);
                    AddImage(doc, target, element);
                    AddDeploy(doc, target, element);
                    AddPostBuildEvent(doc, target, element);
                }
            }
        }

        //		<ImageXex>
        //          <OutputFileName>$(RS_BUILDBRANCH)/game_xenon_profile.xex</OutputFileName>
        //          <AdditionalSections>54540840=..\..\..\..\xlast\Fuzzy.spa</AdditionalSections>
        //          <TitleID>0x54540840</TitleID>
        //          <ProjectDefaults/>
        //      </ImageXex>
        private void AddImage(XmlDocument doc, Independent.Target target, XmlElement element)
        {
            XmlElement imageElement = AppendElement(doc, element, "ImageXex");
            Independent.Image image = target.Settings.Image;
            {
                AppendElementWithTextNode(doc, imageElement, "OutputFileName", image.OutputFilename);
                AppendElementWithTextNode(doc, imageElement, "AdditionalSections", image.AdditionalSections);
                AppendElementWithTextNode(doc, imageElement, "TitleID", image.TitleId);

                AppendBuckets(doc, imageElement, (Independent.SettingExtender)image);
            }
        }

        //<Deploy>
        //	<ExecutionBucket>85313600</ExecutionBucket>
        //	<RemoteRoot>xe:\$(ProjectName)</RemoteRoot>
        //	<DeploymentFiles>$(RemoteRoot)=$(ImagePath);$(RemoteRoot)=$(TEMP)\rfs.dat;$(RemoteRoot)=$(TargetDir)$(TargetName).cmp;</DeploymentFiles>
        //	<DeploymentType>CopyToHardDrive</DeploymentType>
        //</Deploy>
        //
        private void AddDeploy(XmlDocument doc, Independent.Target target, XmlElement element)
        {
            if (target.Platform == Platform.Xbox360 || target.Platform == Platform.Durango)
            {
                XmlElement deployElement = AppendElementWithAttribute(doc, element, "Deploy", "Condition", string.Format("'$(XBECOPY_SUPPRESS_COPY)'!='1'"));
                Independent.Deploy deploy = target.Settings.Deploy;
                {
                    AppendElementWithTextNode(doc, deployElement, "RemoteRoot", deploy.RemoteRoot);
                    AppendElementWithTextNode(doc, deployElement, "DeploymentFiles", Join(deploy.DeployFiles));
                    AppendElementWithTextNode(doc, deployElement, "DeploymentType", deploy.DeployType);

                    AppendBuckets(doc, deployElement, (Independent.SettingExtender)deploy);
                }
            }
        }

        private void AddCompileFileOverride(XmlDocument doc, Independent.Target target, XmlElement clCompile, Independent.ProjectFile file)
        {
            if (!String.IsNullOrEmpty(target.Settings.Compiler.FilenamePatternOverride))
            {
                Regex regex = new Regex(target.Settings.Compiler.FilenamePatternOverride, RegexOptions.IgnoreCase);
                if (regex.IsMatch(file.Path))
                {
                    AppendElementWithTextNode(doc, clCompile, "AdditionalIncludeDirectories", Join(target.Settings.Compiler.FileOverride_AdditionalIncludeDirectories), target);
                    AppendElementWithTextNode(doc, clCompile, "CompileAsWinRT", target.Settings.Compiler.FileOverride_CompileAsWinRt.ToString(), target);
                    AppendElementWithTextNode(doc, clCompile, "ExceptionHandling", target.Settings.Compiler.FileOverride_ExceptionHandling, target);
                }
            }
        }

        private void AddCompile(XmlDocument doc, Independent.Target target, XmlElement element)
        {
            XmlElement clCompile = AppendElement(doc, element, "ClCompile");
            Independent.Compiler compiler = target.Settings.Compiler;
            {
                AppendElementWithTextNode(doc, clCompile, "AdditionalOptions", JoinSpace(compiler.AdditionalOptions, "%(AdditionalOptions)"));
                AppendElementWithTextNode(doc, clCompile, "AdditionalIncludeDirectories", Join(compiler.AdditionalIncludeDirectories, "%(AdditionalIncludeDirectories)"));
                AppendElementWithTextNode(doc, clCompile, "AdditionalUsingDirectories", Join(compiler.AdditionalUsingDirectories, "%(AdditionalUsingDirectories)"));
                AppendElementWithTextNode(doc, clCompile, "DisableSpecificWarnings", Join(compiler.DisableSpecificWarnings));
                if (target.Settings.General.Exceptions.Length>0)
                {
                    AppendElementWithTextNode(doc, clCompile, "ExceptionHandling", target.Settings.General.Exceptions.ToString());
                }
                AppendFavourSizeOrSpeed(doc, clCompile, compiler);
                AppendElementWithTextNode(doc, clCompile, "ForcedIncludeFiles", Join(compiler.ForceIncludes, "%(ForcedIncludeFiles)"));
                AppendElementWithTextNode(doc, clCompile, "ForcedUsingFiles", Join(compiler.ForcedUsingFiles)); // "%(ForcedUsingFiles)" 
                AppendElementWithTextNode(doc, clCompile, "FunctionLevelLinking", compiler.FunctionLevelLink.ToString());
                AppendElementWithTextNode(doc, clCompile, "IntrinsicFunctions", compiler.IntrinsicFunctions.ToString());
                AppendElementWithTextNode(doc, clCompile, "PreprocessorDefinitions", Join(compiler.PreprocessorDefinitions, "__VS"+VsVersion, "%(PreprocessorDefinitions)"));
                AppendElementWithTextNode(doc, clCompile, "RuntimeTypeInfo", target.Settings.General.Rtti.ToString());
                AppendElementWithTextNode(doc, clCompile, "StringPooling", compiler.StringPooling.ToString());

                if (!String.IsNullOrEmpty(target.Settings.Compiler.Optimization))
                    AppendElementWithTextNode(doc, clCompile, "Optimization", target.Settings.Compiler.Optimization);

                AppendBuckets(doc, clCompile, (Independent.SettingExtender)compiler);

                if (!String.IsNullOrEmpty(target.Settings.Compiler.WarningLevel))
                    AppendElementWithTextNode(doc, clCompile, "WarningLevel", target.Settings.Compiler.WarningLevel);

                if (target.Platform == Platform.Ps3)
                {
                    if (compiler.TreatWarningsAsErrors)
                    {
                        AppendElementWithTextNode(doc, clCompile, "TreatMessagesAsErrors", "WarningsAsErrors");
                    }
                    AppendElementWithTextNode(doc, clCompile, "GenerateDebugInformation", compiler.GenerateDebugInformation.ToString());
                }
                else if (target.Platform == Platform.Orbis)
                {
                    AppendElementWithTextNode(doc, clCompile, "WarningsAsErrors", compiler.TreatWarningsAsErrors.ToString());
                    AppendElementWithTextNode(doc, clCompile, "GenerateDebugInformation", compiler.GenerateDebugInformation.ToString());
                }
                else
                {
                    AppendElementWithTextNode(doc, clCompile, "TreatWarningAsError", compiler.TreatWarningsAsErrors.ToString());
                }
            }
        }

        private void AddLibrarian(XmlDocument doc, Independent.Target target, XmlElement element)
        {

            if (target.ProjectType.Standard == Independent.ProjectType.StandardValues.Lib)
            {
                Independent.Librarian librarian = target.Settings.Librarian;
                XmlElement lib = AppendElement(doc, element, "Lib");
                {
                    if (librarian.OutputFilename != "")
                    {
                        AppendElementWithTextNode(doc, lib, "OutputFile", librarian.OutputFilename);
                    }

                    AppendElementWithTextNode(doc, lib, "AdditionalDependencies", Join(librarian.AdditionalDependencies, "%(AdditionalDependencies)"));
                    AppendElementWithTextNode(doc, lib, "AdditionalOptions", JoinSpace(librarian.AdditionalOptions, "%(AdditionalOptions)"));

                    AppendBuckets(doc, lib, (Independent.SettingExtender)librarian);
                }
            }
        }

        private void AddLink(XmlDocument doc, Independent.Target target, XmlElement element)
        {
            if (target.ProjectType.Standard == Independent.ProjectType.StandardValues.Exe)
            {
                Independent.Linker linker = target.Settings.Linker;
                XmlElement link = AppendElement(doc, element, "Link");
                {
                    if (target.Platform == Platform.Durango)
                    {
                        // see B* 1634327
                        AppendElementWithTextNode(doc, link, "AdditionalDependencies", Join(linker.AdditionalDependencies));
                    }
                    else
                    {
                        AppendElementWithTextNode(doc, link, "AdditionalDependencies", Join(linker.AdditionalDependencies, "%(AdditionalDependencies)"));
                    }
                    AppendElementWithTextNode(doc, link, "AdditionalOptions", JoinSpace(linker.AdditionalOptions, "%(AdditionalOptions)"));
                    AppendElementWithTextNode(doc, link, "AdditionalLibraryDirectories", Join(linker.AdditionalLibraryDirectories, "%(AdditionalLibraryDirectories)"));
                    AppendElementWithTextNode(doc, link, "GenerateDebugInformation", linker.GenerateDebugInfo.ToString());
                    CreateGenerateMapFileElement(doc, target, linker, link);
                    AppendElementWithTextNode(doc, link, "IgnoreSpecificDefaultLibraries", Join(linker.IgnoreLibraries, "%(IgnoreSpecificDefaultLibraries)"));

                    AppendElementWithTextNode(doc, link, "LinkIncremental", linker.LinkIncremental.ToString());
                    AppendElementWithTextNode(doc, link, "MapFilename", linker.MapFilename);
                    AppendElementWithTextNode(doc, link, "OutputFile", linker.OutputFilename);

                    AppendBuckets(doc, link, (Independent.SettingExtender)linker);
                }
            }
        }

        private void AddPreBuildEvent(XmlDocument doc, XmlElement element)
        {
            string preBuildCommand = CreatePreBuildCommand();

            if (preBuildCommand != "")
            {
                XmlElement preBuildEventElement = AppendElement(doc, element, "PreBuildEvent");
                {
                    AppendElementWithTextNode(doc, preBuildEventElement, "Command", preBuildCommand + "\n");
                    AppendElementWithTextNode(doc, preBuildEventElement, "Message", "Pre Build Event : Running ParCodeGen on source files.");
                }
            }
        }

        private void AddPreLinkEvent(XmlDocument doc, Independent.Target target, XmlElement element)
        {
            Independent.ProjectBuildEvent preLinkEvent = target.Settings.BuildEvents.PreLink;
            XmlElement preLinkEventElement = AppendElement(doc, element, "PreLinkEvent");
            {
                AppendElementWithTextNode(doc, preLinkEventElement, "Command", preLinkEvent.Command);
                AppendElementWithTextNode(doc, preLinkEventElement, "Message", preLinkEvent.Description);
            }
        }

        private void AddPostBuildEvent(XmlDocument doc, Independent.Target target, XmlElement element)
        {
            Independent.ProjectBuildEvent postBuildEvent = target.Settings.BuildEvents.PostBuild;
            XmlElement postBuildEventElement = AppendElement(doc, element, "PostBuildEvent");
            {
                AppendElementWithTextNode(doc, postBuildEventElement, "Command", postBuildEvent.Command);
                AppendElementWithTextNode(doc, postBuildEventElement, "Message", postBuildEvent.Description);
            }
        }

        /// <summary>
        /// iterate over all files in project for each directory containing parse files that NOT of extension psc
        /// pushd into this directory
        /// then parcodegen them
        /// and popd
        /// </summary>
        /// <returns></returns>
        private string CreatePreBuildCommand()
        {
            string preBuildCommand = "";
            foreach (Independent.ProjectDirectory dir in Project.Directories)
            {
                // Since the first directory can be virtual we have to work out the first non virtual directory - pain in the arse.
                Independent.ProjectDirectory nonVirtDir = dir;
                if (nonVirtDir.IsVirtual)
                {
                    nonVirtDir.IsVirtual = false;
                    nonVirtDir.Path = System.IO.Path.GetDirectoryName(nonVirtDir.Path);
                }

                BuildPrebuildCommand(dir, nonVirtDir, ref preBuildCommand);
            }
            return preBuildCommand;
        }

        // Contains the items (source files, etc.) in the project.
        // You will generally have multiple item groups – one per item
        // type.  
        //
        // <ItemGroup>
        //      <ClInclude Include="..\ai\AITarget.h"/>
        //      <ClInclude Include="..\ai\BlockingBounds.h"/>
        //      <ClInclude Include="..\ai\EntityScanner.h"/>
        // 
        private void CreateItemGroupSourceFiles(XmlDocument doc, XmlElement root)
        {            
            // here we try to group related intems together - as does VS20xx by default.            
            IEnumerable<FileType> fileTypes = Enum.GetValues(typeof(FileType)).Cast<FileType>();

            foreach (FileType fileType in fileTypes.Where(ft => ft.IsSourceFile()))
            {
                XmlElement element = AppendElement(doc, root, MSBUILD_ITEMGROUP);
                {
                    foreach (Independent.ProjectDirectory dir in Project.Directories)
                    {
                        RecurseDirectory(doc, element, dir, fileType);
                    }
                }
            }
        }

        /// See url:bugstar:1680054 
        //   <ItemGroup Condition="$(Platform)'=='Durango'">
        //      <SDKReference Include="Xbox Services API, Version=8.0" />
        // </ItemGroup>
        private void CreateItemGroupXboxServicesApi(XmlDocument doc, XmlElement root)
        {
            XmlElement element = CreateElement(doc, MSBUILD_ITEMGROUP);
            AppendAttribute(doc, element, "Condition", XBOX_CONDITION);
            AppendElementWithAttribute(doc, element, SDK_REF, "Include", XBOX_SERVICES_API);
            AppendElementWithAttribute(doc, element, SDK_REF, "Include", XBOX_GAMECHAT_API);
            AppendChild(root, element);
        }

        //  <ItemGroup>
        //      <AppxManifest Include="Package.appxmanifest" />
        //  </ItemGroup>
        protected virtual void CreateItemGroupManifests(XmlDocument doc, XmlElement root)
        {
        }

        // Contains the project references
        //
        //<ItemGroup>
        //  <ProjectReference Include="X:/gta5/src/dev/game/VS_Project1_lib/game1_lib_2010_unity.vcxproj">
        //      <Project>{C83FF5FE-FFE5-486B-BD32-C17F5A6EF073}</Project>
        //      <ReferenceOutputAssembly>false</ReferenceOutputAssembly>
        //  </ProjectReference>
        // 
        private void CreateItemGroupProjectReferences(XmlDocument doc, XmlElement root, ICollection<string> filenamesExported)
        {
            XmlElement element = AppendElement(doc, root, MSBUILD_ITEMGROUP, null, false);
            {
                // The vcxproj model of project references is restrictive - hence doesn't look right or fit well with the independent format which basically allows each target to have different 
                // libraries linked for each target.
                Independent.Target target = Project.Targets[0]; // choose any target, they should all have the same libraries.
                foreach (string lib in target.Settings.Linker.Libraries)
                {
                    // Handle missing and non-mangled libraries
                    // ALWAYS USE THE FULL PATH - PseudoGuids may break otherwise.
                    string libraryPath = Path.GetFullPath(Environment.ExpandEnvironmentVariables(lib + MangledSuffixVcxProj));

                    // we have the filetype already specified - this means non name mangling will take place.
                    if (lib.Contains(VS_FILETYPE_VCXPROJ))
                    {
                        libraryPath = Path.GetFullPath(Environment.ExpandEnvironmentVariables(lib));
                        if (!File.Exists(libraryPath))
                        {
                            Log.Warning("Library with no mangling {0} could not be found", libraryPath);
                        }
                    }
                    else if (!File.Exists(libraryPath))
                    {
                        if (MANGLE_SUFFIXES)
                        {
                            string nonMangledLibraryPath = Path.GetFullPath(Environment.ExpandEnvironmentVariables(lib + StandardSuffixVcxProj));

                            if (File.Exists(nonMangledLibraryPath))
                            {
                                Log.Message("Using non mangled library path {0}", nonMangledLibraryPath);
                                libraryPath = nonMangledLibraryPath;
                            }
                            else
                            {
                                Log.Warning("Neither library path {0} or {1} could be found", libraryPath, nonMangledLibraryPath);
                            }
                        }
                        else
                        {
                            Log.Warning("Library path {0} could not be found", libraryPath);
                        }
                    }

                    XmlElement projectReference = AppendElementWithAttribute(doc, element, "ProjectReference", "Include", FilenameToRelFilterPath(libraryPath));
                    {
                        string projectGuid = ProjectGuid(libraryPath, VsVersion, filenamesExported);//PseudoGuid(libraryPath);// System.Guid.NewGuid().ToString();
                        AppendElementWithTextNode(doc, projectReference, "Project", GuidToXmlString(projectGuid));
                        AppendElementWithTextNode(doc, projectReference, "ReferenceOutputAssembly", "false");
                    }
                }
            }
        }

        private void RecurseDirectory(XmlDocument doc, XmlElement element, Independent.ProjectDirectory directory, FileType extension = FileType.None)
        {
            foreach (Independent.ProjectDirectory dir in directory.Directories)
            {
                RecurseDirectory(doc, element, dir, extension);
            }

            foreach (Independent.ProjectFile file in directory.Files)
            {
                string ext = Path.GetExtension(file.Path);
                FileType fileType = FileTypeExtensions.Create(Log, ext);

                if (file.Parse && extension == FileType.Psc)
                {
                    string pscFilename = file.Path + extension.EnumToString();
                    if (File.Exists(pscFilename))
                    {
                        XmlElement customBuild = AppendElementWithAttribute(doc, element, "CustomBuild", "Include", FilenameToRelFilterPath(pscFilename));
                        // In the old project builder this is setup per build config - not sure if that is required yet...
                        string commandLine = string.Format(@"pushd %(RootDir)%(Directory) && {0} %(filename)%(extension) && popd", PARCODEGEN_EXECUTABLE);
                        string filenameNoExtension = @"%(RootDir)%(Directory)%(Filename)";

                        List<string> outputs = new List<string>();
                        outputs.Add(filenameNoExtension + PARSER_HDR);
                        ParsePscForClassGeneration(pscFilename, filenameNoExtension, outputs);

                        outputs.Add("%(Outputs)");

                        string joinedOutputs = Join(outputs);
                        XmlElement command = AppendElementWithTextNode(doc, customBuild, "Command", commandLine);
                        XmlElement output = AppendElementWithTextNode(doc, customBuild, "Outputs", joinedOutputs);
                        XmlElement additionalInputs = AppendElementWithTextNode(doc, customBuild, "AdditionalInputs", PARCODEGEN_DEPENDENCY + ";%(AdditionalInputs)");
                        XmlElement message = AppendElementWithTextNode(doc, customBuild, "Message", string.Format("Generating parser code... Command {0}", commandLine.Replace("&&", " ")));
                    }
                }
                else if (file.Custom && extension == fileType)
                {
                    if (file.CustomCommand != null && file.CustomCommand.Count() > 0)
                    {
                        if (File.Exists(file.Path))
                        {
                            foreach (KeyValuePair<Platform, Independent.CustomCommand> customCommand in file.CustomCommand)
                            {
                                Platform platform = customCommand.Key;
                                Independent.CustomCommand command = customCommand.Value;

                                XmlElement customBuildElement = AppendElementWithAttribute(doc, element, "CustomBuild", "Include", FilenameToRelFilterPath(file.Path));

                                if (customBuildElement != null)
                                {
                                    // In the old project builder this is setup per build config - not sure if that is required yet...
                                    XmlElement commandElement = AppendElementWithTextNode(doc, customBuildElement, "Command", FixVcxProjMacros(command.Command), platform);

                                    List<string> outputs = new List<string>();
                                    outputs.Add(FixVcxProjMacros(command.Outputs));
                                    outputs.Add("%(Outputs)");
                                    XmlElement output = AppendElementWithTextNode(doc, customBuildElement, "Outputs", Join(outputs), platform);

                                    List<string> deps = new List<string>();
                                    deps.Add(FixVcxProjMacros(command.Dependencies.Replace(" ", String.Empty)));
                                    deps.Add("%(AdditionalInputs)"); // DW for some reason VCxproj doesn't like this                            
                                    XmlElement AdditionalInputs = AppendElementWithTextNode(doc, customBuildElement, "AdditionalInputs", Join(deps), platform);

                                    XmlElement Message = AppendElementWithTextNode(doc, customBuildElement, "Message", string.Format("Custom build... {0}", FixVcxProjMacros(command.Description)), platform);
                                }
                                else
                                {
                                    Log.Error("A custombuild could not identify which platform it should be added to.");
                                }
                            }
                        }
                    }
                    else
                    {
                        if (extension == FileType.None || extension == fileType)
                        {
                            switch (fileType)
                            {
                                case FileType.Cpp:
                                case FileType.C:
                                case FileType.PmJob:
                                case FileType.Rc:
                                    Log.Warning("I dont know how to handle this extension yet... {0} is unhandled @ {1}", ext, file.Path);
                                    AppendElementWithAttribute(doc, element, "None", "Include", FilenameToRelFilterPath(file.Path));
                                    break;
                                case FileType.H:   // nothing to do
                                case FileType.Dtx: // nothing to do - documentation file.                                    
                                case FileType.Spa: // nothing to do 
                                case FileType.Fxh: // nothing to do 
                                case FileType.Bat: // nothing to do 
								case FileType.Xsd: // nothing to do
                                case FileType.Txt: // nothing to do
                                case FileType.Build: // nothing to do
                                case FileType.Makefile: // nothing to do
                                    AppendElementWithAttribute(doc, element, "None", "Include", FilenameToRelFilterPath(file.Path));
                                    break;
                                case FileType.Job:
                                    // DW: TODO : ideal for some LINQ here
                                    if (Project.Platforms.Contains(Platform.Ps3))
                                    {
                                        if (!(Unity && Project.Unity.Enabled))
                                        {
                                            if (Unity && Project.Unity.Enabled)
                                            {
                                                CustomBuild(doc, element, file, Platform.Ps3, String.Empty, String.Empty);
                                            }
                                            else
                                            {
                                                CustomBuild(doc, element, file, Platform.Ps3, CustomStepCommand("make_spurs_job_pb"), CustomStepOutput("job"));
                                            }
                                        }
                                    }
                                    break;
                                case FileType.Frag:
                                    if (Project.Platforms.Contains(Platform.Ps3))
                                    {
                                        if (Unity && Project.Unity.Enabled)
                                        {
                                            CustomBuild(doc, element, file, Platform.Ps3, String.Empty, String.Empty);
                                        }
                                        else
                                        {
                                            CustomBuild(doc, element, file, Platform.Ps3, CustomStepCommand("make_spu_frag_pb"), CustomStepOutput("frag"));
                                        }
                                    }
                                    break;
                                case FileType.Task:
                                    if (Project.Platforms.Contains(Platform.Ps3))
                                    {
                                        if (Unity && Project.Unity.Enabled)
                                        {
                                            CustomBuild(doc, element, file, Platform.Ps3, String.Empty, String.Empty);
                                        }
                                        else
                                        {
                                            CustomBuild(doc, element, file, Platform.Ps3, CustomStepCommand("make_spurs_task_pb"), CustomStepOutput("task"));
                                        }
                                    }
                                    break;
                                case FileType.Png:
                                    AppendElementWithAttribute(doc, element, "Image", "Include", FilenameToRelFilterPath(file.Path));
                                    break;
                                case FileType.Appxmanifest:
                                    AppendElementWithAttribute(doc, element, "AppxManifest", "Include", FilenameToRelFilterPath(file.Path));
                                    break;
                                case FileType.Fx:
                                    Log.Warning("Custom File Extension {0} is not supported @ {1}", ext, file.Path);
                                    break;
                                case FileType.Ico:
                                    AppendElementWithAttribute(doc, element, "Content", "Include", FilenameToRelFilterPath(file.Path));
                                    break;
                                default:
                                    Log.Error("Error: Custom File Extension {0} is unhandled @ {1}", ext, file.Path);
                                    break;
                            }
                        }
                    }
                }
                else
                {
                    if (extension == FileType.None || extension == fileType)
                    {
                        switch (fileType)
                        {
                            case FileType.Cpp:
                            case FileType.C:
                                XmlElement clCompileElement = AppendElementWithAttribute(doc, element, "ClCompile", "Include", FilenameToRelFilterPath(file.Path));
                                if (file.Excluded)
                                {
                                    AppendElementWithTextNode(doc, clCompileElement, "ExcludedFromBuild", "true");
                                }
                                SetFileLtcg(doc, file, clCompileElement);

                                // Additional ClCompile settings
                                foreach (Platform platform in Project.Platforms)
                                {
                                    if (platform.SupportsPch())
                                    {
                                        if (file.ForcedIncludeFiles != null)
                                        {
                                            AppendElementWithTextNode(doc, clCompileElement, "ForcedIncludeFiles", file.ForcedIncludeFiles, platform);
                                        }

                                        if (file.AdditionalIncludeDirectories != null)
                                        {
                                            AppendElementWithTextNode(doc, clCompileElement, "AdditionalIncludeDirectories", file.AdditionalIncludeDirectories, platform);
                                        }

                                        if (file.PrecompiledHeader != null)
                                        {
                                            AppendElementWithTextNode(doc, clCompileElement, "PrecompiledHeader", file.PrecompiledHeader, platform);
                                        }
                                    }
                                }

                                // Search for file targets of this file name
                                foreach (Independent.Target target in this.Project.Targets)
                                {
                                    AddCompileFileOverride(doc, target, clCompileElement, file);
                                }

                                break;
                            case FileType.Rc:
                                foreach (Independent.Target target in Project.Targets.Where(n => n.Active))
                                {
                                    XmlElement clResCompileElement = AppendElementWithAttribute(doc, element, "ResourceCompile", "Include", FilenameToRelFilterPath(file.Path), target);

                                    if (target.Platform != Platform.Win32 &&
                                        target.Platform != Platform.Win64 &&
                                        target.Platform != Platform.x86)
                                    {
                                        AppendElementWithTextNode(doc, clResCompileElement, "ExcludedFromBuild", "true");
                                    }
                                }
                                break;
                            case FileType.Inl:
                            case FileType.H:
                                AppendElementWithAttribute(doc, element, "ClInclude", "Include", FilenameToRelFilterPath(file.Path));
                                break;
                            case FileType.Sch:
                            case FileType.Txt:
                            case FileType.Build:
                            case FileType.Makefile:
                            case FileType.Dtx:
                            case FileType.Spa:
                            case FileType.Fxh:
                            case FileType.Inc:
                            case FileType.Bat:
							case FileType.Xsd:
                                AppendElementWithAttribute(doc, element, "None", "Include", FilenameToRelFilterPath(file.Path));
                                break;
                            case FileType.Appxmanifest:
                                AppendElementWithAttribute(doc, element, "Appxmanifest", "Include", FilenameToRelFilterPath(file.Path));
                                break;
                            case FileType.Png:
                                AppendElementWithAttribute(doc, element, "Image", "Include", FilenameToRelFilterPath(file.Path));
                                break;
                            default:
                                if (!file.Parse)
                                {
                                    Log.Error("File Extension {0} is unhandled @ {1}", ext, file.Path);
                                }
                                break;
                        }
                    }
                }
            }
        }

        protected virtual void SetFileLtcg(XmlDocument doc, Independent.ProjectFile file, XmlElement clCompileElement)
        {
        }

        private void CustomBuild(XmlDocument doc, XmlElement element, Independent.ProjectFile file, Platform platform, string command, string output)
        {
            XmlElement customBuild = AppendElementWithAttribute(doc, element, "CustomBuild", "Include", FilenameToRelFilterPath(file.Path));
            XmlElement commandElement = AppendElementWithTextNode(doc, customBuild, "Command", command, platform);
            XmlElement outputElement = AppendElementWithTextNode(doc, customBuild, "Outputs", output + ";%(Outputs)", platform);
            XmlElement messageElement = AppendElementWithTextNode(doc, customBuild, "Message", string.Format("{0} [Outputs: {1} ]", command, output), platform);
        }

        // Defines (directly or via imports) VC++ targets such as
        // build, clean, etc.
        //
        // <Import Project="$(VCTargetsPath)\Microsoft.Cpp.targets" />
        //
        private XmlElement CreateImportTargets(XmlDocument doc, XmlElement root)
        {
            return AppendElementWithAttribute(doc, root, MSBUILD_IMPORT, "Project", @"$(VCTargetsPath)\Microsoft.Cpp.targets", null, false);
        }

        /// <summary>
        /// This group contains imports for the Build Customization
        /// target files.
        /// <ImportGroup Label="ExtensionTargets" /> fuuuck how do I do nice xml node in xml documentation...
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="root"></param>
        /// <returns></returns>
        private XmlElement CreateImportGroupExtensionTargets(XmlDocument doc, XmlElement root)
        {
            return AppendElementWithAttribute(doc, root, MSBUILD_IMPORTGROUP, "Label", "ExtensionTargets", null, false);
        }
        #endregion // xmldoc creation

        // Create the entire xml doc, section by section of the horrid vcxproj format.
        private XmlDocument CreateFiltersXmlDoc()
        {
            XmlDocument doc = new XmlDocument();
            CreateXmlDeclaration(doc);
            CreateComment(doc);
            XmlElement root = CreateXmlRoot(doc);
            CreateFiltersItemGroupFilters(doc, root);

            List<FileType> fileTypes = Enum.GetValues(typeof(FileType)).Cast<FileType>().ToList();

            foreach (FileType extension in fileTypes)
            {
                if (extension.IsSourceFile())
                {
                    CreateFiltersItemGroupByExtensionFilters(doc, root, extension);
                }
            }

            return doc;
        }

        #region VCXPROJ Independent settings translation
        /// <summary>
        /// The general approach here is that these methods take an Independent.Target as input and return a translated keyVal<string,string> pair where;
        /// - the key is the name of the element 
        /// - the value is the text of the setting.
        /// - This was done out of mild paranoia for some flexibility since it's plausable that the element name may differ based upon the target or toolset.
        /// - It also couples settings this relationship better for ease of maintenance & readibility etc.       
        /// - When returning a null value in the keyval pair the intent is to prevent a node from being created, if you want an empty node use string.empty.
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        private KeyValuePair<String, String> ProjectType(Independent.Target target)
        {
            string nodeName = "ConfigurationType";
            switch (target.ProjectType.Standard)
            {
                case Independent.ProjectType.StandardValues.Lib:
                    return new KeyValuePair<String, String>(nodeName, "StaticLibrary");
                case Independent.ProjectType.StandardValues.Exe:
                    return new KeyValuePair<String, String>(nodeName, "Application");
                case Independent.ProjectType.StandardValues.Dll:
                    return new KeyValuePair<String, String>(nodeName, "DynamicLibrary");
                case Independent.ProjectType.StandardValues.Custom:
                    return new KeyValuePair<String, String>(nodeName, target.ProjectType.Custom);
                case Independent.ProjectType.StandardValues.Invalid:
                    Log.Error("Error: an invalid ProjectType was encountered");
                    return new KeyValuePair<String, String>(nodeName, null);
                default:
                    Log.Error("Error: an unknown/unsupported ProjectType was encountered");
                    return new KeyValuePair<String, String>(nodeName, null);
            }
        }

		private KeyValuePair<String, String> CharacterSet(Independent.Target target)
		{
			return new KeyValuePair<String, String>("CharacterSet", target.Settings.General.CharacterSet);
		}

		private KeyValuePair<String, String> WholeProgramOptimization(Independent.Target target)
		{
			string value = target.Settings.General.WholeProgramOptimization;
			if(value != null && value.Length == 0)
			{
				// Don't include an empty tag
				value = null;
			}
			return new KeyValuePair<String, String>("WholeProgramOptimization", value);
		}

        private KeyValuePair<String, String> PlatformToolset(Independent.Target target)
        {
            string platformToolset = target.Settings.General.PlatformToolset;
            string nodeName = "PlatformToolset";
  
            if (!String.IsNullOrEmpty(platformToolset))
            {
                return new KeyValuePair<String, String>(nodeName, platformToolset);
            }

            switch (target.Platform)
            {
                case Platform.Ps3:
                    return new KeyValuePair<String, String>(nodeName, "SNC");
                case Platform.Durango:
                    return new KeyValuePair<String, String>(nodeName, "v110");
                case Platform.x64:
                case Platform.Win64:
                    if(target.Exporter==Exporter.VS2012 || target.Exporter==Exporter.VS2012_UNITY)
                        return new KeyValuePair<String, String>(nodeName, "v110_xp");
                    else
                        return new KeyValuePair<String, String>(nodeName, null);
                default:
                    return new KeyValuePair<String, String>(nodeName, null); // when returning a null value the intent is to prevent a node from being created
            }
        }

        private KeyValuePair<String, String> ExceptionsAndRtti(Independent.Target target)
        {
            string nodeName = "ExceptionsAndRtti";
            if (target.Settings.General.Exceptions.ToLower() != "false")
            {
                if (target.Settings.General.Rtti)
                {
                    return new KeyValuePair<String, String>(nodeName, "WithExceptsWithRtti");
                }
                else
                {
                    return new KeyValuePair<String, String>(nodeName, "WithExceptsNoRtti");
                }
            }
            else
            {
                if (target.Settings.General.Rtti)
                {
                    return new KeyValuePair<String, String>(nodeName, "NoExceptsWithRtti");
                }
                else
                {
                    return new KeyValuePair<String, String>(nodeName, "NoExceptsNoRtti");
                }
            }
        }
        #endregion // VCXPROJ Independent settings translation 

        #endregion //  Private Methods

        #region Private Static Methods
        private static void CreateGenerateMapFileElement(XmlDocument doc, Independent.Target target, Independent.Linker linker, XmlElement link)
        {
            if (target.Platform == Platform.Ps3)
            {
                if (linker.GenerateMapFile)
                {
                    AppendElementWithTextNode(doc, link, "GenerateSnMapFile", "NormalMapFile");
                }
                else
                {
                    AppendElementWithTextNode(doc, link, "GenerateSnMapFile", "None");
                }
            }
            else
            {
                AppendElementWithTextNode(doc, link, "GenerateMapFile", linker.GenerateMapFile.ToString());
            }
        }

        private void ParsePscForClassGeneration(string pscFilename, string filenameNoExtension, List<string> outputs)
        {

            //<ParserSchema xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            //	xsi:noNamespaceSchemaLocation="schemas/parsermetadata.xsd"
            //	generate="class, psoChecks">

            // old school parse the psc file - should be using linq when I get time
            XmlDocument xml = new XmlDocument();
            using (XmlTextReader tr = new XmlTextReader(pscFilename))
            {
                tr.Namespaces = false;
                xml.Load(tr);
            }

            XmlNode schema = xml.SelectSingleNode("//ParserSchema");

            if (schema != null)
            {
                XmlAttribute attr = schema.Attributes["generate"];
                if (attr != null && attr.InnerText.Contains("class"))
                {
                    outputs.Add(filenameNoExtension + ".cpp");
                    outputs.Add(filenameNoExtension + ".h");
                }
            }
            else
            {
                Log.Error("Invalid PSC filename {0}", pscFilename);
            }
        }

        private static string GetFiltersFilenameFullPath(string outputFilename)
        {
            string outputFilenameFullPath = Path.Combine(Directory.GetCurrentDirectory(), outputFilename);
            return outputFilenameFullPath;
        }

        private static void AppendFavourSizeOrSpeed(XmlDocument doc, XmlElement clCompile, Independent.Compiler compiler)
        {
            if (compiler.FavourSize == compiler.FavourSpeed)
            {
                AppendElementWithTextNode(doc, clCompile, "FavorSizeOrSpeed", "Neither");
            }
            else if (compiler.FavourSize)
            {
                AppendElementWithTextNode(doc, clCompile, "FavorSizeOrSpeed", "Size");
            }
            else if (compiler.FavourSpeed)
            {
                AppendElementWithTextNode(doc, clCompile, "FavorSizeOrSpeed", "Speed");
            }
        }

        private static void CreateComment(XmlDocument doc)
        {
            string version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            string appAndVersion = string.Format("{0} v{1}", Assembly.GetExecutingAssembly().GetName(), version);

            //            XmlComment comment = doc.CreateComment(string.Format("Built by Project Generator : {0}", appAndVersion));
            //            doc.AppendChild(comment);
        }

        #region String joining helpers

        private static string JoinSpace(List<string> list, params string[] strings)
        {
            List<string> newList = new List<string>(list);
            newList.AddRange(strings);
            return Join(newList.ToArray(), " ");
        }

        protected static string Join(List<string> list, params string[] strings)
        {
            List<string> newList = new List<string>(list);
            newList.AddRange(strings);
            return Join(newList.ToArray(), ";");
        }

        private static string Join(string[] list, string seperator)
        {
            return string.Join(seperator, list);
        }

        #endregion // string joining helpers

        /// <summary>
        /// For a complex (annoying) path return the relative part
        /// <example>'x:\example\..\example2\file.txt' => '\example2\file.txt'</example>
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private static string BackPath(string path)
        {
            Match match = Regex.Match(path, @"[^\.]*(\.\.\\.*)$", RegexOptions.IgnoreCase);
            string result = match.Success ? match.Groups[1].Value.Trim() + @"\" : null;
            return result;
        }

        /// <summary>
        /// Escapes string for XML 
        /// Also fixes some Macros that are used in makefiles
        /// <example>X:\gta5\src\dev\game\renderer\makefile.txt  : call "..\renderer\SpuPM\GtaBuildSPUPM.bat" debug $(InputDir) $(InputName) $(IntDir)</example>
        /// These are VS2008 macros, hence the translation - ideally should be exporter agnostic!!
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static string FixVcxProjMacros(string str)
        {
            str = System.Security.SecurityElement.Escape(str);
            //str = str.Replace("\"", "&quot;"); // should be &quot;
            str = str.Replace("$(InputDir)", "%(RootDir)%(Directory)");
            str = str.Replace("$(InputName)", "%(Filename)");
            // str = str.Replace("&amp;amp;", "&amp;");
            return str;
        }

        private static string CustomStepCommand(string command)
        {
            return string.Format(@"call $(RS_TOOLSROOT)\script\coding\custom_steps\{0} %(RootDir)%(Directory) $(IntDir) %(Filename)  &quot;$(ConfigurationName)&quot;", command);
        }

        private static string CustomStepOutput(string type)
        {
            return string.Format(@"$(IntDir)%(Filename).{0}.obj", type);
        }

        /// <summary>
        /// Adds bucketed settings to element
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="element"></param>
        /// <param name="settingExtender"></param>
        private static void AppendBuckets(XmlDocument doc, XmlElement element, Independent.SettingExtender settingExtender)
        {
            foreach (KeyValuePair<String, String> entry in settingExtender.Bucket)
            {
                AppendElementWithTextNode(doc, element, entry.Key, entry.Value);
            }
        }

        private static void Unescape(string outputFilename)
        {
            // Unfortunate VCXPROJ complication - in some circumstances it's required to use " instead of &apos;
            // Haven't decided upon the best way to do this yet - this is temporary, obviously better to control the escaping
            // question is how exactly?
            //
            // further I require to be able to put in carriage return line feeds into the xml ( but not escaped! ) VCXPROJ is quirky about the xml it wants to read.
            //
            string fileContents = File.ReadAllText(outputFilename);
            fileContents = fileContents.Replace("&quot;", "\"");
            fileContents = fileContents.Replace("&amp;quot;", "\"");
            fileContents = fileContents.Replace("&amp;amp;", "&amp;");
            fileContents = fileContents.Replace("&amp;#13;", "&#13;");
            fileContents = fileContents.Replace("&amp;gt;", ">"); // this was failing on post build steps
            fileContents = fileContents.Replace("&gt;", ">"); // this was failing on post build steps
            fileContents = fileContents.Replace("_NEWLINE_", "\n"); // pain in the bum <- I don;t think I'll ever get rid of this step now.

            //fileContents = fileContents.Replace(" &amp;&amp;", "&&");
            File.WriteAllText(outputFilename, fileContents);
        }
        #endregion // Private Static Methods

        #region .vcxproj format explained - msbuild experience required.
        /// <summary>
        ///
        ///    This is the root node. It specifies the MSBuild version to use
        ///    and also the default target to be executed when this file is passed
        ///    to MSBuild.exe
        ///    &lt;Project DefaultTargets="Build" ToolsVersion="4.0" xmlns='http://schemas.microsoft.com/developer/msbuild/2003' &gt; 
        ///
        ///    This contains the project configurations known to the project
        ///    (such as Debug|Win32 and Release|Win32).
        ///    &lt;ItemGroup Label="ProjectConfigurations" /&gt;
        ///
        ///    This contains project level settings such as ProjectGuid, 
        ///    RootNamespace, etc. These properties are not normally overridden
        ///    elsewhere in the project file. This group is not configuration 
        ///    dependent and so only one Globals group generally exists in the
        ///    project file.
        ///    &lt;PropertyGroup Label="Globals" /&gt;
        ///
        ///    This property sheet contains the default settings for a VC++
        ///    project. It contains definitions of all the project settings such
        ///    as Platform, PlatformToolset, OutputPath, TargetName, UseOfAtl, 
        ///    etc. and also all the item definition group defaults for each known
        ///    item group. In general, properties in this file are not tool-specific.
        ///    &lt;Import Project="$(VCTargetsPath)\Microsoft.Cpp.default.props" /&gt;
        ///
        ///    This property group has an attached configuration condition 
        ///    (such as Condition="'$(Configuration)|$(Platform)'=='Debug|Win32'")
        ///    and comes in multiple copies, one per configuration. This property
        ///    group hosts configuration-wide properties. These properties control
        ///    the inclusion of system property sheets in Microsoft.Cpp.props.
        ///    E.g. if we define the property &lt;CharacterSet&gt;Unicode&lt;/CharacterSet&gt;,
        ///    then the system property sheet microsoft.Cpp.unicodesupport.props 
        ///    will be included (as can be seen in the Property Manager). Indeed,
        ///    in one of the imports of Microsoft.Cpp.props, we can see the line:
        ///    &lt;Import Condition="'$(CharacterSet)' == 'Unicode'"   Project="$(VCTargetsPath)\microsoft.Cpp.unicodesupport.props"/&gt;
        ///    &lt;PropertyGroup Label="Configuration" /&gt;
        ///
        ///    This property sheet (directly or via imports) defines the 
        ///    default values for many tool-specific properties such as the 
        ///    compiler’s Optimization, WarningLevel properties, Midl tool’s
        ///    TypeLibraryName property, etc. In addition, it imports various
        ///    system property sheets based on configuration properties defined
        ///    in the property group immediately above.  
        ///    &lt;Import Project="$(VCTargetsPath)\Microsoft.Cpp.props" /&gt;
        ///
        ///    This group contains imports for the property sheets that
        ///    are part of Build Customizations (or Custom Build Rules as this
        ///    feature was called in earlier editions). A Build Customization
        ///    is defined by up to three files – a .targets file, a .props file
        ///    and .xml file. This import group contains the imports for the
        ///    .props file.  
        ///    &lt;ImportGroup Label="ExtensionSettings" /&gt;
        ///
        ///    This group contains the imports for user property sheets.
        ///    These are the property sheets you add through the Property 
        ///    Manager view in VS. The order in which these imports are listed
        ///    is relevant and is reflected in the Property Manager. The project
        ///    file normally contains multiple instances of this kind of import
        ///    group, one for each project configuration.  
        ///    &lt;ImportGroup Label="PropertySheets" /&gt;
        ///
        ///    UserMacros are as variables used to customize your build 
        ///    process. E.g. you can define a user macro to define your custom
        ///    output path as $(CustomOutpuPath) and use it to define other 
        ///    variables. This property group houses such properties. Note that
        ///    in VCXPROJ, this group is not populated by the IDE since we do not
        ///    support user macros for configurations (we do for property sheets
        ///    though).  
        ///    &lt;PropertyGroup Label="UserMacros" /&gt;
        ///
        ///    This property group normally comes with a configuration 
        ///    condition attached. You will also see multiple instances of the 
        ///    property group, one per configuration. It differs in its identity
        ///    from the other property groups above by the fact that it does 
        ///    not have a label (to look at it in another way, it has a label 
        ///    that is equal to the empty string). This group contains project 
        ///    configuration level settings. These settings apply to all files
        ///    that are part of the specified item group. Build Customization
        ///    item definition metadata also gets initialized here.
        ///    &lt;PropertyGroup /&gt;
        ///
        ///    Similar to the property group immediately above, but it 
        ///    contains item definitions and item definition metadata instead
        ///    of properties.
        ///    &lt;ItemDefinitionGroup /&gt;
        /// 
        ///    Contains the items (source files, etc.) in the project.
        ///    You will generally have multiple item groups – one per item
        ///    type.  
        ///    &lt;ItemGroup /&gt;
        ///
        ///    Defines (directly or via imports) VC++ targets such as
        ///    build, clean, etc.
        ///    &lt;Import Project="$(VCTargetsPath)\Microsoft.Cpp.targets" /&gt;
        ///
        ///    This group contains imports for the Build Customization
        ///    target files.
        ///    &lt;ImportGroup Label="ExtensionTargets" /&gt;
        ///
        ///    &lt;/Project&gt;
        ///    
        ///    &lt;ItemGroup&gt;
        ///      &lt;ProjectReference 
        ///     This group is only for applications, it defines the project references.     
        /// 
        /// 
        /// </summary>
        #endregion // vcxproj format explained
    }
}
