﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGenerator
{
    #region OperatorAttribute
    public class OperatorAttribute : Attribute
    {
        public string Op { get; set; }
        public OperatorAttribute(string op)
        {
            Op = op;
        }
    }
    #endregion // OperatorAttribute
}
