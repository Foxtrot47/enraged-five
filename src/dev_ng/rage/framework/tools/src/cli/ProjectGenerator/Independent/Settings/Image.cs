﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace ProjectGenerator.Independent
{
    /// <summary>
    /// Image settings
    /// <remarks>Possibly a 360 thing only</remarks>
    /// </summary>
    [Serializable]
    public class Image : SettingExtender
    {
        #region Properties
        public string OutputFilename { get; set; }
        public string AdditionalSections { get; set; }
        public string TitleId { get; set; }

        #endregion //Properties

        #region Constructor(s)
        /// <summary>
        /// default constructor
        /// </summary>
        public Image()
        {
            OutputFilename = String.Empty;
            AdditionalSections = String.Empty;
            TitleId = String.Empty;
        }
        #endregion // Constructor(s)
    } // class SettingsImage
}
