﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace ProjectGenerator.Independent
{
    /// <summary>
    /// A project in a solution
    /// </summary>
    [Serializable]
    public class SolutionProject
    {
        #region Private Constants
        private static readonly string UNITY_ONLY_SOLUTION_PROJECT_TOKEN = "(Unity)";
        private static readonly string NONUNITY_ONLY_SOLUTION_PROJECT_TOKEN = "(NonUnity)";
        #endregion // Private Constants

        #region Public Properties
        /// <summary>
        /// The name of the project
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// The path to the project file ( unsuffixed )
        /// </summary>
        public string UnSuffixedPath { get; set; }
        /// <summary>
        /// The guid used by the project
        /// </summary>
        public string Guid { get; set; }
        /// <summary>
        /// The names of the projects the solution project is dependent on.
        /// </summary>
        public List<string> DependencyNames { get; set; }
        /// <summary>
        /// References to the project depenendencies.
        /// </summary>
        public List<SolutionProject> Dependencies { get; set; }
        /// <summary>
        /// The settings of the solution project stored in a dictionary keyed by solution config
        /// </summary>
        public Dictionary<string, SolutionProjectSetting> SolutionProjectSettingDict { get; set; }
        /// <summary>
        /// Quirky Tristate bool 
        /// - null = Use this project in Unity builds&Non-Unity Builds
        /// - true = Use this project only in Unity builds
        /// - false = Use this project only in Non-Unity Builds
        /// </summary>
        public Nullable<bool> Unity { get; private set; }
        /// <summary>
        /// The Exporter this project is setup to use, when set to Exporter.ALL it will export for all projects
        /// otherwise it will only export for the one exporter specified.
        /// </summary>
        public ICollection<Exporter> Exporters { get; private set; }
        #endregion // Public Properties

        #region Private Properties
        /// <summary>
        /// Universal log : no choice but to make this static since the SimpleObjectCloner would want to clone this. A difficult decision.
        /// </summary>
        private static IUniversalLog Log { get; set; }
        #endregion // Private Properties

        #region Constructors
        /// <summary>
        /// Paramterised Constructor
        /// </summary>
        public SolutionProject(IUniversalLog log, string name = "", string path = "")
        {
            Log = log;
            Exporters = new List<Exporter>();
            Exporters.Add(Exporter.ALL);

            UnSuffixedPath = path;
            Name = name;
            Unity = null;

            if (Name.Contains(UNITY_ONLY_SOLUTION_PROJECT_TOKEN))
            {
                if (Name.Contains(NONUNITY_ONLY_SOLUTION_PROJECT_TOKEN))
                {
                    Log.Error("Do not mark projects as {0} *AND* {0}, this is a tristate boolean", UNITY_ONLY_SOLUTION_PROJECT_TOKEN, NONUNITY_ONLY_SOLUTION_PROJECT_TOKEN);
                }

                Unity = true;
                Name = Name.Replace(UNITY_ONLY_SOLUTION_PROJECT_TOKEN, String.Empty);
            }

            if (Name.Contains(NONUNITY_ONLY_SOLUTION_PROJECT_TOKEN))
            {
                Unity = false;
                Name = Name.Replace(NONUNITY_ONLY_SOLUTION_PROJECT_TOKEN, String.Empty);
            }
                                  
            string[] exporters = Exporter.GetNames(typeof(Exporter));
            
            Match m = Regex.Match(Name, @"^.*\((.*)\).*$", RegexOptions.IgnoreCase);
            if (m.Success)
            {
                string exportersDesc = m.Groups[1].Value.Trim();

                foreach (string exporter in exporters)
                {
                    // Note how the string incased in the parenthesis is actually powered by a regex
                    Match m2 = Regex.Match(exporter, exportersDesc, RegexOptions.IgnoreCase);
                    if (m2.Success)
                    {
                        string exporterStringReplace = String.Format("({0})", exportersDesc);
                        Name = Name.Replace(exporterStringReplace, String.Empty);
                        
                        // Now we are getting choosy about exporters lets remove the default for Exporter.All.
                        Exporter firstExporter = Exporters.First();
                        if (firstExporter == Exporter.ALL)
                        {
                            Exporters.Remove(firstExporter);
                        }
                        
                        Exporters.Add(ExporterExtensions.Create(Log, exporter));
                    }
                }
            }

            Guid = String.Empty;
            DependencyNames = new List<string>();
            Dependencies = new List<SolutionProject>();
            SolutionProjectSettingDict = new Dictionary<string, SolutionProjectSetting>();
        }

        #endregion // Constructors

        #region Public Methods

        /// <summary>
        /// Do we have any valid mapping for this solution, cos if not it's not meant for serialisation
        /// - this would likely be achieved through a rule in the slnrules that maps to '' empty string for all projects.
        /// </summary>
        /// <returns>true if a valid mapping has been found - the project if for serialisation</returns>
        public bool HasValidMapping()
        {
            foreach (KeyValuePair<string, SolutionProjectSetting> kvp in SolutionProjectSettingDict)
            {
                if (kvp.Value.ProjectConfig != null && kvp.Value.ProjectConfig.Length > 0)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Expands variables for easier description of solutions
        /// </summary>
        /// <param name="project">A Project that we can use the Project Name from</param>
        public void ExpandVariables(Project project)
        {
            if (0 == string.Compare(Name,Solution.PROJECT_NAME,true))
            {
                Name = project.Name;
            }

            if (0 == string.Compare(UnSuffixedPath, Solution.PROJECT_NAME, true))
            {
                UnSuffixedPath = project.Name;
            }

            Name = Environment.ExpandEnvironmentVariables(Name);
            UnSuffixedPath = Environment.ExpandEnvironmentVariables(UnSuffixedPath);
        }
        #endregion // Public Methods

    } //  class SolutionProject
} // namespace ProjectGenerator.Independent
