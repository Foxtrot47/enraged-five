﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGenerator.Independent
{
    public enum ProjectType
    {
        [TextAttribute("exe")]
        exe,
        [TextAttribute("lib")]
        lib,
        [TextAttribute("dll")]
        dll,
        [TextAttribute("invalid")]
        invalid
    } // enum ProjectType
} // namespace ProjectGenerator.Independent
