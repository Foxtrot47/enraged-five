﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace ProjectGenerator
{
    #region Enum
    /// <summary>
    /// MakeRelationalOperator
    /// Enum of recognised equality operators
    /// - eg equals, notEquals etc.
    /// </summary>
    public enum MakeRelationalOperator
    {
        [MakeFileAttribute("==")]
        Equals,
        [MakeFileAttribute("!=")]
        NotEquals,
        [MakeFileAttribute("=~")] // DW: TODO: One day actually make this match regex, it's just implemented as a simple case insensitive contains for now.
        Contains,
        [MakeFileAttribute("!~")]
        NotContains,
        [MakeFileAttribute("&&")]
        And,
        [MakeFileAttribute("||")]
        Or,
        [MakeFileAttribute("INVALID")]
        Invalid
    } //  enum MakeRelationalOperator
    #endregion // Enum

    #region Extensions
    /// <summary>
    /// Enum extensions of MakeRelationalOperator
    /// </summary>
    public static class MakeRelationalOperatorExtensions
    {
        /// <summary>
        /// Caches for (much) quicker reflection
        /// </summary>
        private static Dictionary<MakeRelationalOperator, string> MakeFileAttributeCache = new Dictionary<MakeRelationalOperator, string>();

        /// <summary>
        /// Converts a string to a MakeEqualityOperator enum
        /// </summary>
        /// <param name="op"></param>
        /// <returns></returns>
        public static MakeRelationalOperator Create(IUniversalLog log, string op)
        {
            MakeRelationalOperator[] ops = (MakeRelationalOperator[])Enum.GetValues(typeof(MakeRelationalOperator));

            foreach (MakeRelationalOperator o in ops)
            {
                if (0 == String.Compare(op, EnumToString(o), true))
                    return (o);
            }
            log.Error("Invalid MakeRelationalOperator {0}", op);
            return MakeRelationalOperator.Invalid;
        }

        /// <summary>
        /// Converts a MakeEqualityOperator enum to a string
        /// </summary>
        /// <param name="makeEqualityOperator"></param>
        /// <returns></returns>
        public static string EnumToString(this MakeRelationalOperator makeRelationalOperator)
        {
            // Is it cached?
            if (MakeFileAttributeCache.ContainsKey(makeRelationalOperator))
            {
                return MakeFileAttributeCache[makeRelationalOperator];
            }

            Type type = makeRelationalOperator.GetType();
            FieldInfo fi = type.GetField(makeRelationalOperator.ToString());
            MakeFileAttribute[] attributes = fi.GetCustomAttributes(typeof(MakeFileAttribute), false) as MakeFileAttribute[];
            string s = (attributes.Length > 0 ? attributes[0].Text : null);

            // Cache it for quicker reflection next time.
            if (s != null)
            {
                MakeFileAttributeCache.Add(makeRelationalOperator, s);
            }

            return s;
        }
    } // class MakeRelationalOperatorExtensions
    #endregion Extensions
} // namespace ProjectGenerator
