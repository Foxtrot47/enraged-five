﻿using System;

namespace ProjectGenerator
{
    /// <summary>
    /// the attribute that is set on an enum that contains the value of the valid values  
    /// of a setting within the makefile.
    /// </summary>
    public class MakeFileAttribute : Attribute
    {
        /// <summary>
        /// The string associated with the attribute
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Parameterised Constructor
        /// </summary>
        public MakeFileAttribute(string text)
        {
            Text = text;
        }
    } // class TextAttribute
} // namespace ProjectGenerator
