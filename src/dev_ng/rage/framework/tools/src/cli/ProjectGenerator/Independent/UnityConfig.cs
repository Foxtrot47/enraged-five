﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace ProjectGenerator.Independent
{
    /// <summary>
    /// Unity configuration
    /// <remarks>Stores what is described in project_name.unity file. </project_name></remarks>
    /// </summary>
    [Serializable]
    public class UnityConfig
    {
        #region Properties 
        /// <summary>
        /// Filenames that are not party to the unity build.
        /// </summary>
        public List<string> Exclusions { get; set; }
        
        /// <summary>
        /// A collection of unity designation & regex pairs.
        /// </summary>
        public Dictionary<string, UnityCompilationUnitDescriptor> CompilationUnitSetup { get; set; }

        /// <summary>
        /// A Conventient way to enable / disable a unity build
        /// </summary>
        public bool Enabled { get; set; }

        /// <summary>
        /// A unity file 
        /// - A file that is known to be designated to build unity files.
        /// </summary>
        public Dictionary<string,List<Independent.ProjectFile>> CompilationUnits { get; set; }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// default constructor
        /// </summary>
        public UnityConfig()
        {
            Enabled = true;
            Exclusions = new List<string>();
            CompilationUnitSetup = new Dictionary<string, UnityCompilationUnitDescriptor>();
            CompilationUnits = new Dictionary<string, List<Independent.ProjectFile>>();
        }
        #endregion Constructor(s)

        #region Public Methods
        /// <summary>
        /// Add a Compilation unit dictionary entry
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void AddCompilationUnitSetup(string key, Regex regex, bool ltcg = true)
        {
            UnityCompilationUnitDescriptor unityFileConfig = new UnityCompilationUnitDescriptor(regex, ltcg);

            if (CompilationUnitSetup.ContainsKey(key))
            {
                CompilationUnitSetup[key] = unityFileConfig;
            }
            else
            {
                CompilationUnitSetup.Add(key, unityFileConfig);
            }
        }

        /// <summary>
        /// For the given file add it to the compilation unit of name passed.
        /// </summary>
        /// <param name="file">the file to add to the compilation unit.</param>
        /// <param name="designation">the name of the compilation unit to add it to.</param>
        /// <param name="ltcg">LTCG flag for the unity file itself</param>
        public void AddFileToCompilationUnit(Independent.ProjectFile file, string designation)
        {
            if (file.UnityDesignation != null)
                return; // the file has already been designated - do not designate to another unity file.

            // The file now knows of its unity designation.
            file.UnityDesignation = designation;
            file.Excluded = true;

            // We setup a dictionary of unity files
            if (!CompilationUnits.ContainsKey(designation))
            {
                // Add the first file to this designation.
                List<Independent.ProjectFile> newFiles = new List<ProjectFile>() { file };
                CompilationUnits[designation] = newFiles;
            }
            else
            {
                // append another unity file to this designation.
                CompilationUnits[designation].Add(file);
            }            
        }

        #endregion // public methods
    } // class UnityConfig
} // namespace ProjectGenerator.Independent
