﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGenerator.Independent
{
    /// <summary>
    /// SettingExtender : a base class for independent settings
    /// allows new key/value pair to be introduced without having an associated independent setting.
    /// </summary>
    [Serializable]
    public class SettingExtender
    {
        #region Public Properties
        /// <summary>
        /// A 'bucket' for key val pairs where the setting is not explicitly recognised
        /// this allows us to add new settings without a new release of the project generator binaries
        /// over time such settings can migrate to recognised settings.
        /// </summary>
        public Dictionary<string, string> Bucket { get; set; }
        #endregion // Public Properties

        #region Constructor(s)
        /// <summary>
        /// default constructor
        /// </summary>
        public SettingExtender()
        {
            Bucket = new Dictionary<string, string>();
        }
        #endregion //Constructor(s)

        #region Public Methods
        /// <summary>
        /// Set a setting
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public void SetBucket(string name, string value)
        {
            if (Bucket.ContainsKey(name))
            {
                Bucket[name] = value;
            }
            else
            {
                Bucket.Add(name, value);
            }
        }

        /// <summary>
        /// Get a setting
        /// </summary>
        /// <param name="name"></param>
        /// <returns>the setting if it has it, or empty string if not</returns>
        public string GetBucket(string name)
        {
            if (Bucket.ContainsKey(name))
            {
                return Bucket[name];
            }
            else
            {
                return String.Empty;
            }
        }
        #endregion // Public Methods
    } // class SettingExtender
} // namespace ProjectGenerator.Independent.Settings
