﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Reflection;
using System.Collections;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using RSG.Base.Configuration;

namespace ProjectGenerator.Exporters
{
    /// <summary>
    /// A Proj exporter ( Vcxproj & CsProj )
    /// - This contains the common functionality for VS2010 & VS2012 etc.
    /// - Where necessary we can virtualise methods and override in the subclass.
    /// </summary>
    public abstract partial class ProjExporter :
        MSBuildExporter
    {
        #region Protected Constants
        // enable Trace output
        protected static readonly bool TRACE = (ProjectGenerator.TRACE && true);
        // Indicates Mangling is on, and when so important project reference considerations are made.
        protected static readonly bool MANGLE_SUFFIXES = true; // set to true to mangle filenames so that they don't collide with existing files.
        #endregion // Protected Constants

        #region Constructors
        /// <summary>
        /// Parameterised constructor
        /// </summary>
        /// <param name="mangle"></param>
        /// <param name="unity"></param>
        protected ProjExporter(IUniversalLog log, CommandOptions options, string mangle, bool unity)
            : base(log, options, mangle, unity, true, false)
        {
        }
        #endregion // Constructor(s)

        #region Protected Properties
        /// <summary>
        /// The version of visual studio eg. "VS2010", "VS2012" etc.
        /// - Implemented by the sub class
        /// </summary>        
        protected abstract string VsVersion { get; }
        #endregion // Protected Properties

        #region Private Properties

        private string MangledSuffixNoExtension
        {
            get { return Unity ? "_" + VsVersion + UNITY + Mangler : "_" + VsVersion + Mangler; }
        }
        #endregion // Private Properties

        #region public methods
        /// <summary>
        /// Export the independent project
        /// (Independent.IProjectExporter)
        /// </summary>
        /// <param name="project">the project to export</param>
        /// <param name="filenamesExported">a list to populate of all the files created / exported.</param>
        /// <returns></returns>
        public bool Export(ICollection<string> filenamesExported, bool verify = false)
        {
            bool ok = true;

            if (IsProjectValid())
            {
                if (Project.RootDirectory == null)
                {
                    Log.Warning("Project does not have a root Directory!, fix the makefile.");
                }

                Language = LanguageExtensions.InferLanguage(Log, Project);
                if (Language == Language.Invalid)
                {
                    Log.Error("Could not infer the language.");
                    return false;
                }

                if (Language == Language.Cs)
                {
                    ok = ExportCsProj(filenamesExported, verify);
                }
                else if (Language == Language.Cpp)
                {
                    ok = ExportVcxProj(filenamesExported, verify);
                }
                else
                {
                    Log.Error("Unknown language");
                }
            }

            return ok;
        }

        /// <summary>
        /// The type of exporter : DW TODO: I've now got a mix of interface / abstract methods, that isn't ideal
        /// </summary>
        public abstract Exporter ExporterType { get; }

        /// <summary>
        /// Verify that the project exists.
        /// </summary>
        /// <param name="filename">the filename to verify</param>
        /// <returns>true if all ok</returns>
        public bool Verify(string filename)
        {
            if (!File.Exists(filename))
            {
                return false;
            }

            return true;
        }

        public void PreDetermineExportedFiles(ICollection<string> filenames)
        {
            if (Language == Language.None)
                return; // Not all exporters have a language per se.

            if (Language == Language.Cpp)
            {
                PreDetermineExportedFilesVcxProj(filenames);
            }
            else if (Language == Language.Cs)
            {
                PreDetermineExportedFilesCsProj(filenames);
            }
            else
            {
                Log.Error("Unhandled Language.");
            }
        }

        #endregion // public methods
    }
}
