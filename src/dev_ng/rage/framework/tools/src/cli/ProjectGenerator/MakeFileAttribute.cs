﻿using System;

namespace ProjectGenerator
{
    /// <summary>
    /// the attibute that is set on an enum that contains the value of the valid values  
    /// of a setting within the makefile.
    /// </summary>
    public class MakeFileAttribute : Attribute
    {
        public string Text { get; set; }
        public MakeFileAttribute(string text)
        {
            Text = text;
        }
    } // class TextAttribute
}
