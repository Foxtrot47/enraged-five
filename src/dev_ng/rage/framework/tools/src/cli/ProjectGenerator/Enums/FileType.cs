using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace ProjectGenerator
{
    #region Enum
    /// <summary>
    /// Enum of FileTypes
    /// <remarks>replace with a general tools filetype once I know where it is?</remarks>
    /// </summary>
    public enum FileType
    {
        [FileTypeExtensionAttribute(".cs")]
        Cs,
        [FileTypeExtensionAttribute(".h")]
        H,
        [FileTypeExtensionAttribute(".cpp")]
        Cpp,
        [FileTypeExtensionAttribute(".c")]
        C,
        [FileTypeExtensionAttribute(".frag")]
        Frag,
        [FileTypeExtensionAttribute(".job")]
        Job,
        [FileTypeExtensionAttribute(".txt")]
        Txt,
        [FileTypeExtensionAttribute(".build")]
        Build,
        [FileTypeExtensionAttribute(".makefile")]
        Makefile,
        [FileTypeExtensionAttribute(".pmjob")]
        PmJob,
        [FileTypeExtensionAttribute(".task")]
        Task,
        [FileTypeExtensionAttribute(".inl")]
        Inl,
        [FileTypeExtensionAttribute(".sch")]
        Sch,
        [FileTypeExtensionAttribute(".psc")]
        Psc,
        [FileTypeExtensionAttribute(".dtx")]
        Dtx,
        [FileTypeExtensionAttribute(".spa")]
        Spa,
        [FileTypeExtensionAttribute(".inc")]
        Inc,
        [FileTypeExtensionAttribute(".fxh")]
        Fxh,
        [FileTypeExtensionAttribute(".fx")]
        Fx,
        [FileTypeExtensionAttribute(".rc")]
        Rc,
        [FileTypeExtensionAttribute(".png")]
        Png,
        [FileTypeExtensionAttribute(".appxmanifest")]
        Appxmanifest,
        [FileTypeExtensionAttribute(".bat")]
        Bat,
        [FileTypeExtensionAttribute(".ico")]
        Ico,
        [FileTypeExtensionAttribute(".config")]
        Config,  
	    [FileTypeExtensionAttribute(".xsd")]
	    Xsd,
        [FileTypeExtensionAttribute(".datasource")]
        Datasource,
        [FileTypeExtensionAttribute(".resx")]
        Resx,
        [FileTypeExtensionAttribute(".xaml")]
        Xaml,  
        [FileTypeExtensionAttribute("")]
        None, 
        Invalid
    } // enum FileType
    #endregion //Enum

    #region Extensions
    /// <summary>
    /// Static extensions methods to the FileType enum
    /// <remarks>Extends the enum class so we can reflect on it's attributes.</remarks>
    /// </summary>
    public static class FileTypeExtensions
    {
        /// <summary>
        /// Caches for (much) quicker reflection
        /// </summary>
        private static Dictionary<FileType, string> FileTypeExtensionAttributeCache = new Dictionary<FileType, string>();

        /// <summary>
        /// Create a FileType
        /// </summary>
        /// <param name="fileType"></param>
        /// <param name="fileTypeName"></param>
        public static FileType Create(IUniversalLog log, string fileTypeName)
        {
            fileTypeName = fileTypeName.TrimStart('.');
            if (fileTypeName == String.Empty)
            {
                return FileType.None;
            }

            FileType filetype;
            if (Enum.TryParse(fileTypeName, true, out filetype))
            {
                return filetype;
            }
            else
            {
                log.Error("Invalid FileType {0}", fileTypeName);
                return FileType.Invalid;
            }
        }

        /// <summary>
        /// Get the string (FileTypeStringAttribute) of a filetype enum
        /// </summary>
        /// <param name="fileType"></param>
        /// <returns></returns>
        public static string EnumToString(this FileType fileType)
        {
            // Is it Cached?
            if (FileTypeExtensionAttributeCache.ContainsKey(fileType))
            {
                return FileTypeExtensionAttributeCache[fileType];
            }

            Type type = fileType.GetType();
            FieldInfo fi = type.GetField(fileType.ToString());
            FileTypeExtensionAttribute[] attributes = fi.GetCustomAttributes(typeof(FileTypeExtensionAttribute), false) as FileTypeExtensionAttribute[];
            string s = (attributes.Length > 0 ? attributes[0].Text : null);

            // Cache it for quicker reflection next time.
            if (s != null)
            {
                FileTypeExtensionAttributeCache.Add(fileType, s);
            }

            return s;
        }

        /// <summary>
        /// Returns true if this enum is of a source file type
        /// </summary>
        /// <param name="fileType"></param>
        /// <returns></returns>
        public static bool IsSourceFile(this FileType fileType)
        {
            return (fileType != FileType.None && fileType != FileType.Invalid);
        }

    } // class FileTypeExtensions
    #endregion // Extensions

} // namespace ProjectGenerator
