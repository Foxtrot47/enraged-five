﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGenerator.Independent
{
    /// <summary>
    /// Scratch area
    /// <remarks>A place to store user defined rule variables on demand.</remarks>
    /// </summary>
    [Serializable]
    public class Scratch
    {
        #region Properties
        public Dictionary<string, string> Dict { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// default constructor
        /// </summary>
        public Scratch()
        {
            Dict = new Dictionary<string, string>();            
        }
        #endregion Constructor(s)

        #region Public Methods
        /// <summary>
        /// Add a dictionary entry
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Add(string key, string value)
        {
            if (Dict.ContainsKey(key))
            {
                Dict[key] = value;
            }
            else
            {
                Dict.Add(key, value);
            }
        }

        /// <summary>
        /// Get a value from the dictionary
        /// <remarks>If an entry doesn't exist create an empty string entry for it</remarks>
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string Get(string key)
        {
            if (Dict.ContainsKey(key))
            {
                return Dict[key];
            }
            else
            {
                Dict.Add(key, String.Empty);
                return Dict[key];
            }
        }
    
        #endregion // public methods
    } // class Scratch
} // namespace ProjectGenerator.Independent
