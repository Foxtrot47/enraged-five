﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace ProjectGenerator
{
    #region Enum
    /// <summary>
    /// Enum of recognised exporters
    /// </summary>
    public enum Exporter
    {
        [MakeFileAttribute("VS2010")]
        VS2010,
        [MakeFileAttribute("VS2010_UNITY")]
        VS2010_UNITY,
        [MakeFileAttribute("VS2010_SLN")]
        VS2010_SLN,
        [MakeFileAttribute("VS2010_SLN_UNITY")]
        VS2010_SLN_UNITY,

        [MakeFileAttribute("VS2012")]
        VS2012,
        [MakeFileAttribute("VS2012_UNITY")]
        VS2012_UNITY,
        [MakeFileAttribute("VS2012_SLN")]
        VS2012_SLN,
        [MakeFileAttribute("VS2012_SLN_UNITY")]
        VS2012_SLN_UNITY,

        [MakeFileAttribute("ALL")]
        ALL,

        [MakeFileAttribute("INVALID")]
        INVALID
    } // enum Exporter
    #endregion // Enum

    #region Extensions
    /// <summary>
    /// Static extensions methods to the Exporter enum
    /// <remarks>Extends the enum class so we can reflect on it's attributes.</remarks>
    /// </summary>
    public static class ExporterExtensions
    {
        /// <summary>
        /// Caches for (much) quicker reflection
        /// </summary>
        private static Dictionary<Exporter, string> MakeFileAttributeCache = new Dictionary<Exporter, string>();

        /// <summary>
        /// Create an Exporter Enum fro the passed string
        /// </summary>
        /// <param name="exporter">as part of the extensions</param>
        /// <param name="exporterName">the string representing the exporter ( case insensitive )</param>
        /// <returns>the exporter enum</returns>
        public static Exporter Create(IUniversalLog log, string exporterName)
        {
            Exporter exporter;

            if (Enum.TryParse(exporterName, true, out exporter))
            {
                return exporter;
            }
            else
            {
                log.Error("Invalid Exporter {0}", exporterName);
                return Exporter.INVALID;
            }
        }

        /// <summary>
        /// Get the MakefileExporter (MakeFileAttribute) of a exporter enum
        /// </summary>
        /// <param name="exporter"></param>
        /// <returns></returns>
        public static string EnumToString(this Exporter exporter)
        {
            // Is it cached?
            if (MakeFileAttributeCache.ContainsKey(exporter))
            {
                return MakeFileAttributeCache[exporter];
            }

            Type type = exporter.GetType();
            FieldInfo fi = type.GetField(exporter.ToString());
            MakeFileAttribute[] attributes = fi.GetCustomAttributes(typeof(MakeFileAttribute), false) as MakeFileAttribute[];
            string s = (attributes.Length > 0 ? attributes[0].Text : null);

            // Cache it for quicker reflection next time.
            if (s != null)
            {
                MakeFileAttributeCache.Add(exporter, s);
            }

            return s;
        }

        /// <summary>
        /// Extension method to compare against a string representation of the enum ( case insensitive )
        /// </summary>
        /// <param name="exporter"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        public static bool Match(this Exporter exporter, string val)
        {
            string currVal = exporter.EnumToString();
            return (0 == string.Compare(currVal, val, true));
        }

    } // class ExporterExtensions
    #endregion // Extensions
} // namespace ProjectGenerator
