﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ProjectGenerator
{
    /// <summary>
    /// Guid I/O helper
    /// </summary>
    public static class GuidUtils
    {
        /// <summary>
        /// Reads a guid file
        /// </summary>
        /// <param name="guidFilename">the filename to read - it should already be checked to exist</param>
        /// <returns>the guid as a string</returns>
        public static string ReadFile(string guidFilename)
        {
            StreamReader streamReader = new StreamReader(guidFilename);
            string guid = streamReader.ReadToEnd();
            guid = guid.TrimEnd('\r', '\n');
            guid = guid.ToUpper();
            streamReader.Close();
            return guid;
        }

        /// <summary>
        /// Writes a guid stringto file
        /// </summary>
        /// <param name="guid">the guid as a string</param>
        /// <param name="guidFilename">the filename</param>
        public static void WriteFile(string guid, string guidFilename)
        {
            StreamWriter streamWriter = new StreamWriter(guidFilename);
            streamWriter.Write(guid);
            streamWriter.Close();
        }

        /// <summary>
        /// Invalid guid
        /// </summary>
        public static readonly String UNKNOWN_GUID = "????????-????-????-????-????????????";

    } // GuidUtils
} // namespace ProjectGenerator
