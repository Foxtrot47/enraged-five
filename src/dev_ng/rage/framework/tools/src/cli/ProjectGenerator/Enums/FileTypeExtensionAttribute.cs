﻿using System;

namespace ProjectGenerator
{
    /// <summary>
    /// the attibute of a Filetype that contains the dot prefixed extension name 
    /// </summary>
    public class FileTypeExtensionAttribute : Attribute
    {
        /// <summary>
        /// The string associated with the attribute
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Parameterised Constructor
        /// </summary>
        public FileTypeExtensionAttribute(string text)
        {
            Text = text;
        }
    } // class FileTypeStringAttribute
} // namespace ProjectGenerator
