﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using System.IO;

namespace ProjectGenerator
{
    #region Enum
    /// <summary>
    /// Enum of Languages
    /// <remarks></remarks>
    /// </summary>
    public enum Language
    {
        // None
        [MakeFileAttribute("None")]
        None,
        // C++
        [MakeFileAttribute("Cpp")]
        Cpp,
        // C#
        [MakeFileAttribute("Cs")]
        Cs,
        [MakeFileAttribute("All")]
        All,
        Invalid
    } // enum Language
    #endregion // Enum

    #region Extensions
    /// <summary>
    /// Static extensions methods to the Language enum
    /// <remarks>Extends the enum class so we can reflect on it's attributes.</remarks>
    /// </summary>
    public static class LanguageExtensions
    {
        /// <summary>
        /// Caches for (much) quicker reflection
        /// </summary>
        private static Dictionary<Language, string> MakefileAttributeCache = new Dictionary<Language, string>();

        /// <summary>
        /// Create a language
        /// </summary>
        /// <param name="platform"></param>
        /// <param name="platformName"></param>
        public static Language Create(IUniversalLog log, string languageName)
        {
            Language language;
            if (Enum.TryParse(languageName, true, out language))
            {
                return language;          
            }
            else
            {
                log.Error("Invalid language {0}", languageName);
                return Language.Invalid;
            }
        }

        /// <summary>
        /// Get the string (MakeFileAttribute) of a language enum
        /// </summary>
        /// <param name="platform"></param>
        /// <returns></returns>
        public static string EnumToString(this Language language)
        {
            // Is it cached?
            if (MakefileAttributeCache.ContainsKey(language))
            {
                return MakefileAttributeCache[language];
            }

            Type type = language.GetType();
            FieldInfo fi = type.GetField(language.ToString());
            MakeFileAttribute[] attributes = fi.GetCustomAttributes(typeof(MakeFileAttribute), false) as MakeFileAttribute[];
            string s = (attributes.Length > 0 ? attributes[0].Text : null);

            // Cache it for quicker reflection next time.
            if (s != null)
            {
                MakefileAttributeCache.Add(language, s);
            }

            return s;
        }

        /// <summary>
        /// Extension method to compare against a string representation of the enum ( case insensitive )
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="val"></param>
        /// <returns></returns>
        public static bool Match(this Language obj, string val)
        {
            string currVal = obj.EnumToString();
            return (0 == string.Compare(currVal, val, true));
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Language InferLanguage(IUniversalLog log, Independent.Project project)
        {
            Language language = Language.None;
            foreach (Independent.ProjectDirectory dir in project.Directories)
            {
                language = RecurseDirectoryForLanguage(log, dir);
                if (language != Language.None)
                    break;
            }
            return language;
        }

        /// <summary>
        /// Recurse through directories to find the filetype of source files in the project
        /// from this we infer the language used.
        /// </summary>
        /// <param name="doc"></param>
        /// <param name="element"></param>
        /// <param name="directory"></param>
        /// <returns></returns>
        private static Language RecurseDirectoryForLanguage(IUniversalLog log, Independent.ProjectDirectory directory)
        {
            foreach (Independent.ProjectDirectory dir in directory.Directories)
            {
                Language language = RecurseDirectoryForLanguage(log, dir);
                if (language != Language.None)
                    return language;
            }

            foreach (Independent.ProjectFile file in directory.Files)
            {
                string ext = Path.GetExtension(file.Path);
                FileType fileType = FileTypeExtensions.Create(log, ext);

                switch (fileType)
                {
                    case FileType.Cpp:
                    case FileType.C:
                        return Language.Cpp;
                    case FileType.Cs:
                        return Language.Cs;
                    default:
                        // deliberately left blank
                        break;
                }
            }

            return Language.None;
        }
    } // class LanguageExtensions
    #endregion // Extensions
} // namespace ProjectGenerator