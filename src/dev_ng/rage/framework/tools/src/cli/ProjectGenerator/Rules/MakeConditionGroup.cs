﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace ProjectGenerator
{
    /// <summary>
    /// a collection of conditions and further groups
    /// with a logical operator to combine them.
    /// </summary>
    public class MakeConditionGroup
    {
        #region Public Properties
        /// <summary>
        /// the conditions
        /// </summary>
        public List<MakeCondition> Conditions;
        /// <summary>
        /// further conditions groups
        /// </summary>
        public List<MakeConditionGroup> ConditionGroups;
        /// <summary>
        /// How to combine these conditions || or && - both are not supported
        /// </summary>
        public bool Or;
        #endregion // Public Properties

        #region Private Properties
        /// <summary>
        /// Universal log : no choice but to make this static since the SimpleObjectCloner would want to clone this. A difficult decision.
        /// </summary>
        private static IUniversalLog Log { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public MakeConditionGroup(IUniversalLog log)
        {
            ConditionGroups = new List<MakeConditionGroup>();
            Conditions = new List<MakeCondition>();
            Or = false;
            Log = log;
        }
        #endregion // Constructor(s)

        #region Public methods
        /// <summary>
        /// Evaluate all conditions on the target
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public bool MeetsConditions(Independent.Target target)
        {
            if (Or)
            {
                foreach (MakeCondition condition in Conditions)
                {
                    if (condition.Eval(target))
                    {
                        return true; // return true as soon as evaluted to true
                    }
                }

                foreach (MakeConditionGroup conditionGroup in ConditionGroups)
                {
                    if (conditionGroup.MeetsConditions(target))
                    {
                        return true;
                    }
                }

                return false; // otherwise false
            }
            else
            {
                foreach (MakeCondition condition in Conditions)
                {
                    if (!condition.Eval(target))
                    {
                        return false; // return false as soon as evaluted to false
                    }
                }

                foreach (MakeConditionGroup conditionGroup in ConditionGroups)
                {
                    if (!conditionGroup.MeetsConditions(target))
                    {
                        return false;
                    }
                }

                return true; // otherwise true
            }
        }

        /// <summary>
        /// For the given string split it on && and || and add each condition to rule.
        /// </summary>
        /// <param name="compositeConditions">the composite string containing conditions</param>
        public int AddConditions(string compositeConditions, bool or = false)
        {
            if (compositeConditions == String.Empty)
            {
                return 0;
            }

            int numConditions = 0;
            string andString = MakeRelationalOperator.And.EnumToString();
            string orString = MakeRelationalOperator.Or.EnumToString();
            string[] stringSeparatorAnd = new string[] { andString };
            string[] stringSeparatorsOr = new string[] { orString };

            MakeConditionGroup newGroup = new MakeConditionGroup(Log);
            ConditionGroups.Add(newGroup);

            // split by && then by || 
            // NB. no support for brackets

            if (compositeConditions.Contains(andString) && compositeConditions.Contains(orString))
            {
                Log.Error("Both {0} and {1} are not supported in condition {2}", andString, orString, compositeConditions);
            }

            if (compositeConditions.Contains("(") || compositeConditions.Contains(")"))
            {
                Log.Error("() is not supported in condition {0}", compositeConditions);
            }

            string[] conditions = compositeConditions.Split(stringSeparatorAnd, StringSplitOptions.RemoveEmptyEntries);

            foreach (string condition in conditions)
            {
                string[] furtherConditions = condition.Split(stringSeparatorsOr, StringSplitOptions.RemoveEmptyEntries);

                if (furtherConditions.Count() > 1)
                {
                    newGroup.Or = true;
                }

                foreach (string furtherCondition in furtherConditions)
                {
                    Match matchCondition = Makefile.MAKEFILE_INTERNAL_CONDITION_REGEX.Match(furtherCondition);
                    if (matchCondition.Success)
                    {
                        MakeCondition newCondition = new MakeCondition(Log, matchCondition.Groups[1].Value.Trim(), matchCondition.Groups[2].Value.Trim(), matchCondition.Groups[3].Value.Trim());
                        newGroup.Conditions.Add(newCondition);
                        numConditions++;
                    }
                    else
                    {
                        Log.Error("Invalid Condition {0} in {1}", condition, conditions);
                    }
                }
            }
            return numConditions;
        }

        #endregion // public methods
    } // class MakeConditionGroup
} // ProjectGenerator
