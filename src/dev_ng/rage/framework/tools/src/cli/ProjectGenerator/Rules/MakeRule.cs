﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using System.Text.RegularExpressions;

namespace ProjectGenerator
{
    /// <summary>
    /// A Make rule
    /// - a collection of conditions to evalute on an Independent.Target
    /// - upon all evaluating to true the collection of actions will run on the target
    /// - thus changing the targets settings.
    /// </summary>
    public class MakeRule
    {
        #region Properties
        /// <summary>
        /// The collection of actions to run
        /// </summary>
        public List<MakeAction> Actions { get; set; }
        /// <summary>
        /// The collection of conditions to evaluate
        /// </summary>
        public MakeConditionGroup ConditionGroup { get; set; }

        private static IUniversalLog Log { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public MakeRule(IUniversalLog log)
        {
            Log = log;
            Actions = new List<MakeAction>();
            ConditionGroup = new MakeConditionGroup(Log);
        }

        /// <summary>
        /// Parameterised constructor
        /// </summary>
        /// <param name="action"></param>
        /// <param name="condition"></param>
        public MakeRule(IUniversalLog log, MakeAction action, MakeCondition condition)
        {
            Log = log;
            Actions = new List<MakeAction>();
            ConditionGroup = new MakeConditionGroup(Log);

            if (action != null)
            {
                Actions.Add(action);
            }

            if (condition != null)
            {
                ConditionGroup.Conditions.Add(condition);
            }
        }
        #endregion // Constructor(s)

        #region Public methods
        /// <summary>
        /// Process the rule on the target
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public bool Process(Independent.Target target)
        {
            if (ConditionGroup.MeetsConditions(target))
            {
                RunActions(target);
                return true;
            }
            return false;
        }

        #endregion Public methods

        #region Private methods
        private void RunActions(Independent.Target target)
        {
            foreach (MakeAction action in Actions)
            {
                action.Run(target);
            }
        }
        #endregion // Private methods
    } // class Makerule
} // namesapce ProjectGenerator
