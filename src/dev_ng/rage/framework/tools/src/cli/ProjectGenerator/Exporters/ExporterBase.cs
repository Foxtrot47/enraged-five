﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Reflection;
using System.Collections;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using RSG.Base.Configuration;
using ProjectGenerator.Independent;

namespace ProjectGenerator.Exporters
{
    /// <summary>
    /// Base class used by all exporters
    /// - Processes unity transformations.
    /// TODO : Tidies p4 of old unity files as it goes?
    /// </summary>
    public class ExporterBase
    {
        #region Public Constant(s)
        // Guid filetype
        public static readonly string GUID_FILETYPE = ".guid";
        #endregion // Public Constant(s)
        #region Protected Constant(s)
        protected static readonly string UNITY = "_unity";

        protected static readonly string UNITY_PREFIX = "_unity_";

        // ParcodeGen Executable
        protected static readonly string PARCODEGEN_EXECUTABLE = @"$(RS_TOOLSROOT)\bin\coding\python\parCodeGen.exe";
        // Dependency for parcodegen custom build step
        protected static readonly string PARCODEGEN_DEPENDENCY = @"$(RS_TOOLSROOT)\bin\coding\python\parCodeGen.zip";
        // A parcodgen created parser suffix
        protected static readonly string PARSER_HDR = "_parser.h";
        // Guid tool
        protected static readonly string UUID_TOOL = "%RS_TOOLSROOT%\\bin\\coding\\uuidgen.exe";
        #endregion // Protected Constant(s)
        #region Private Constant(s)

        private static readonly string UNITY_DEFINE = "__UNITYBUILD";
        private static readonly string UNITY_PROLOGUE = "forceinclude/_unity_prologue.h";
        private static readonly string UNITY_EPILOGUE = "forceinclude/_unity_epilogue.h";
        protected static readonly string UNITY_CONFIG_FILETYPE = ".unity";
        protected static readonly string UNITY_FOLDERNAME = "_Unity";
        #endregion // Private Constant(s)

        #region Constructor(s)
        /// <summary>
        /// Parameterised Constructor
        /// </summary>
        /// <param name="mangle">A string that mangles output filenames</param>
        /// <param name="unity">Set true if a unity exporter</param>
        public ExporterBase(IUniversalLog log, CommandOptions options, string mangle, bool unity, bool exportsProjects, bool exportsSolutions)
        {
            Log = log;
            Options = options;
            GuidsUsed = new Dictionary<string, string>();
            Mangler = mangle;
            Unity = unity;
            ExportsProjects = exportsProjects;
            ExportsSolutions = exportsSolutions;
            Language = Language.None;            
        }
        #endregion Constructor(s)

        #region Public Properties
        /// <summary>
        /// The independent project format - the export from this object.
        /// since the exporter exporter will change this object for exporter based rules and unity transformations 
        /// it is prudent to hold a copy.
        /// (As per IProjectExporter)
        /// </summary>
        public Independent.Project Project { get; set; }

        /// <summary>
        /// The independent solution format - the export from this object.
        /// since the exporter exporter will change this object for exporter based rules and unity transformations 
        /// it is prudent to hold a copy.
        /// (As per IProjectExporter)
        /// </summary>
        public Independent.Solution Solution { get; set; }

        /// <summary>
        /// Boolean to indicate if this exporter can export a project.
        /// </summary>
        public bool ExportsProjects { get; set; }

        /// <summary>
        /// Boolean to indicate if this exporter can export a solution.
        /// </summary>
        public bool ExportsSolutions { get; set; }

        /// <summary>
        /// Convenience Property for the language being exported.
        /// </summary>
        public Language Language { get; set; }

        #endregion // Public Properties
        #region Protected Properties
        /// <summary>
        /// A dictionary of all the guids used.
        /// </summary>
        protected Dictionary<string, string> GuidsUsed { get; set; }
        
        /// <summary>
        /// A mangle string.
        /// </summary>
        protected string Mangler { get; set; }

        /// <summary>
        /// Is this exporter a unity exporter?
        /// </summary>
        protected bool Unity { get; set; }

        /// <summary>
        /// The universal log
        /// </summary>
        protected IUniversalLog Log { get; set; }

        /// <summary>
        /// Command options - cached.
        /// </summary>
        protected CommandOptions Options { get; set; }
        #endregion // Protected Properties
        #region Private Properties
        /// <summary>
        /// Constructs the filename where a unity file would be found alongside the makefile.
        /// </summary>
        private string UnityConfigFilename
        {
            get { return Project.Name + UNITY_CONFIG_FILETYPE; }
        }
        #endregion // Private Properties

        #region Public Methods
        
        /// <summary>
        /// Since we have both a Project and a Solution in this base class
        /// we provide a way of knowing what it is we have to export.
        /// </summary>
        /// <returns></returns> 
        public bool IsSolutionValid()
        {
            return (Solution != null && Solution.IsValid());
        }

        /// <summary>
        /// Since we have both a Project and a Solution in this base class
        /// we provide a way of knowing what it is we have to export.
        /// </summary>
        /// <returns></returns> 
        public bool IsProjectValid()
        {
            return (Project != null && Project.IsValid());
        }

        /// <summary>
        ///  Interprets independent format to predict the output filenames that will be generated.
        /// </summary>
        /// <param name="filenames">The filenames that will be generated</param>
        public void PreDetermineExportedUnityFiles(ICollection<string> filenames)
        {
            if (IsProjectValid())
            {
                // Unity files
                if (Unity && Project.Unity.Enabled)
                {
                    if (Project.Unity.CompilationUnits.Count > 0)
                    {
                        string unityFolder = Path.GetDirectoryName(Project.Path);
                        unityFolder = Path.Combine(unityFolder, UNITY_FOLDERNAME);

                        foreach (KeyValuePair<String, List<Independent.ProjectFile>> compilationUnit in Project.Unity.CompilationUnits)
                        {
                            string filename = string.Format("{0}{1}_{2}.cpp", UNITY_PREFIX, Project.Name, compilationUnit.Key);
                            string unityPath = Path.Combine(unityFolder, filename);
                            filenames.Add(unityPath);
                        }
                    }
                }
            }
        }

        #endregion // Public methods
        #region Protected Methods

        /// <summary>
        /// A guid for a project file.
        /// </summary>
        /// <param name="libraryPath">the path to the library we are creating a guid for</param>
        /// <param name="filenamesExported">a list of filenames exported, the guid creation will create a new .guid file.</param>
        /// <returns>the guid as a string</returns>
        protected string ProjectGuid(string libraryPath, string vs_version, ICollection<string> filenamesExported = null)
        {
            string dir = Path.GetDirectoryName(libraryPath);
            string projectName = Path.GetFileNameWithoutExtension(libraryPath);

            // We use one guid filename for all projects
            int idx = projectName.LastIndexOf("_" + vs_version);
            if (idx > 0)
            {
                projectName = projectName.Substring(0, idx);
            }
            else
            {
                if (this.Language == Language.Cpp)
                    Log.Warning("An unexpected projectName {0} is in use, this can lead to hard to track down GUID issues.", projectName);
            }


            string guidFilename = Path.Combine(dir, projectName + GUID_FILETYPE);
            string guid;

            if (this.Language != Language.Cs) // because we pregenerate the guid with the current Cs projects.
            {
                try
                {
                    if ((!filenamesExported.Contains(guidFilename)) && File.Exists(guidFilename))
                    {
                        File.Delete(guidFilename);
                        Log.Warning("{0} was deleted - this is likely safe to ignore.", Path.GetFileName(guidFilename));
                        Log.Warning("Did a previous conversion fail and/or have you not submitted the generated guid files yet?");
                    }
                }
                catch
                {
                    // Not a problem, we only try to delete a guid file if it exists, 
                    // there is a possibility that a guid was created by a previous conversion but not checked in.
                    // if that was to occur we would not know to check this file in.                
                }

                if (!File.Exists(guidFilename) && File.Exists(UUID_TOOL))
                {
                    string command = string.Format("\"{0}\" -o{1}", UUID_TOOL, guidFilename);
                    System.Diagnostics.ProcessStartInfo ProcessInfo;
                    System.Diagnostics.Process process;
                    ProcessInfo = new System.Diagnostics.ProcessStartInfo("cmd.exe", "/c " + command);
                    ProcessInfo.CreateNoWindow = true;
                    ProcessInfo.UseShellExecute = false;
                    ProcessInfo.RedirectStandardError = true;
                    ProcessInfo.RedirectStandardOutput = true;
                    process = System.Diagnostics.Process.Start(ProcessInfo);
                    process.WaitForExit();
                    string output = process.StandardOutput.ReadToEnd();
                    string errors = process.StandardError.ReadToEnd();
                    int exitCode = process.ExitCode;

                    if (errors.Count() > 0)
                    {
                        Log.Error("{0} : Errors : {1}", command, errors);
                    }

                    if (exitCode != 0)
                    {
                        Log.Error("{0} returned non zero ret code : {1}", command, exitCode);
                    }

                    process.Close();

                    Log.Message("Creating new guid in {0}", guidFilename);

                    if (filenamesExported != null)
                    {
                        filenamesExported.Add(guidFilename);
                    }
                }
            }

            if (File.Exists(guidFilename))
            {
                guid = GuidUtils.ReadFile(guidFilename);
            }
            else
            {
                //Log.Warning("A guid has not been created - I'll create one on the fly but it may be unsafe.");
                guid = System.Guid.NewGuid().ToString();
                // - Not fit for saving... would mask issues.
                //StreamWriter streamWriter = new StreamWriter(guidFilename);
                //streamWriter.Write(guid);
                //streamWriter.Close();

                // DW : 29-08-12 I can't remember why I was reluctant to do this.
                Log.Message("Creating new guid in {0}", guidFilename);
                GuidUtils.WriteFile(guid, guidFilename);
                if (filenamesExported != null)
                {
                    filenamesExported.Add(guidFilename);
                }
            }

            return guid.ToUpper();
        }


        /// <summary>
        /// Given the independent unity information format held by the exporter project, transform this representation.
        /// so that;
        /// 1) We add a new unity directory
        /// 2) We create & populate new unity files.
        /// 3) We exclude unity files from the build.
        /// </summary>
        protected void UnityTransform(ICollection<string> filenamesExported)
        {
            MakeUnityFiles(filenamesExported);
        }

        //**************************************************************************************************************************************************************************
        // DW TODO: In retrospect the work here needs a jolly good review and rewrite, subtle pathing variants with the makefile system have made it VERY difficult to understand
        // the intent and these methods have grown to cater for this as it's become apparent how the pathing is expected to work from the terse makefile format.
        // Now everything works, this could really be tidied. There is a case for a Path object, something along the lines of every filename when interpreted form a makefile
        // is transformed to a FULL & expanded path, but keeps its definition, it's unexpanded path, it's relative path at read time. 
        // A LOT OF WORK, but pathing issues are the root of all evil.
        //**************************************************************************************************************************************************************************

        protected string FilenameToRelFilterPath(string path)
        {
            // DW: TODO : mm I might need to ponder this some more... getting it *just* working to start with though...
            // it probably should compute paths relative to the root directory ? Project.RootDirectory                                 
            string evaluatedPath = EvaluateRelativePath(Path.GetDirectoryName(Project.Path), Path.GetFullPath(path));

            string trim = ".\\";
            if (evaluatedPath.StartsWith(trim))
            {
                evaluatedPath = evaluatedPath.Substring(trim.Length);
            }

            return evaluatedPath;
            //Match match = Regex.Match(path, @".*(\.\.\\.*)$", RegexOptions.IgnoreCase);
            //if (match.Success)
            //    return match.Groups[1].Value.Trim();
            //Log.Error("Error: A filter path was not converted");
            //return "invalid path";
        }

        // Cleary thsi needs to be reviewed - pathing has become very complex, partly because of the previous project generator and its makefiles.
        protected string PathToFilterPath2(string path, string remove = null)
        {
            string evaluatedPath = EvaluateRelativePath(Path.GetDirectoryName(Project.Path), Path.GetFullPath(path));
            if (remove != null)
            {
                evaluatedPath = evaluatedPath.Replace(remove, String.Empty);
            }

            return evaluatedPath;
            // DW: TODO : mm I might need to ponder this some more... getting it *just* working to start with though...
            // it probably should compute paths relative to the root directory ? Project.RootDirectory
            //Match match = Regex.Match(path, @".*\.\.\\(.*)$", RegexOptions.IgnoreCase);
            //if (match.Success)
            //    return match.Groups[1].Value.Trim();
            //Log.Warning("Warning: A filter path was not converted {0}", path);
            //return "";
        }

        /// <summary>
        /// For a given path calculate the appropriate filter path it should use.
        /// </summary>
        /// <param name="path">The path to calculate for</param>
        /// <param name="remove">an optional string to remove from the result</param>
        /// <returns>The resultant path to use as the filter directory in the project</returns>
        protected string PathToFilterPath(string path, string remove = null)
        {
            // Observe the reluctant fuzzy logic here - its been quite hard to find a rule to suit all forms of makefile pathing.
            // Pathing is the bane of the project generator, there are many exceptional makefiles that include files / directories in many different ways.
            // This *seems* to be a pragmatic solution for now.
            string relativeToPath1 = Path.GetDirectoryName(Project.Path);
            string evaluatedPath1 = MakeRelativeFilterPath(path, remove, relativeToPath1);

            if (Project.RootDirectory == String.Empty || Project.RootDirectory == null)
            {                
                return evaluatedPath1;
            }
            else
            {
                string relativeToPath2 = Project.RootDirectory;
                string evaluatedPath2 = MakeRelativeFilterPath(path, remove, relativeToPath2);

                // Basically there are two rules in play, we choose the shortest path.
                if (evaluatedPath1.Length <= evaluatedPath2.Length)
                {
                    return evaluatedPath1;
                }

                return evaluatedPath2;
            }
        }


        /// <summary>
        /// Create a psuedo guid
        /// <remarks>for the purposes fo the project builder the likelihood of a collision is pretty remote, I'll eat my hat if it ever falls over.</remarks>
        /// <remarks>I do know that guid are by definition non deterministic, but I'm taking a shortdut for now at least. and it saves having to save out files.</remarks>
        /// <remarks>I may end up regretting this.</remarks>
        /// </summary>
        /// <param name="uniqueFileName">the filename the guid is created from - can be any string but will always create the same guid for the same string across machines.</param>
        /// <returns>a guid</returns>      
        protected string PseudoGuid(string uniqueFileName)
        {
            uniqueFileName = uniqueFileName.ToLower();
            byte[] stringbytes = Encoding.UTF8.GetBytes(uniqueFileName.ToLower());
            byte[] hashedBytes = new System.Security.Cryptography.SHA1CryptoServiceProvider().ComputeHash(stringbytes);
            Array.Resize(ref hashedBytes, 16);
            Guid guid = new Guid(hashedBytes);
            if (GuidsUsed.ContainsKey(guid.ToString()))
            {
                string guidString = GuidsUsed[guid.ToString()];
                if (guidString != uniqueFileName)
                {
                    Log.Error("Guid collision!!!!!");
                }
            }
            else
            {
                GuidsUsed.Add(guid.ToString(), uniqueFileName);
            }

            //Log.Message("Pseudo guid {0} {1}", uniqueFileName, guid.ToString());
            return guid.ToString().ToUpper();
        }    

        protected string PathToRelPath(string path, string remove = null)
        {
            string evaluatedPath = EvaluateRelativePath(Path.GetDirectoryName(Project.Path), Path.GetFullPath(path));
            if (remove != null)
            {
                evaluatedPath = evaluatedPath.Replace(remove, String.Empty);
            }
            evaluatedPath = evaluatedPath.Replace("..\\", String.Empty);
            evaluatedPath = evaluatedPath.Replace(".\\", String.Empty);

            return Path.GetDirectoryName(evaluatedPath);
            // DW: TODO : mm I might need to ponder this some more... getting it *just* working to start with though...
            // it probably should compute paths relative to the root directory ? Project.RootDirectory
            //Match match = Regex.Match(path, @"^.*\.\.\\(.*)\\.*$", RegexOptions.IgnoreCase);
            //if (match.Success)
            //    return match.Groups[1].Value.Trim();
            //Log.Error("Error: A relative path was not converted {0}", path);
            //return "invalid path";
        }

        protected string RelativeExpandedProjectPath(string path)
        {
            String expanded1 = Environment.ExpandEnvironmentVariables(path);
            String expanded2 = Options.Config.Environment.Subst(expanded1);
            System.Uri uri1 = new Uri(Project.RootDirectory + "\\");
            System.Uri uri2 = new Uri(expanded2);
            Uri relativeUri = uri1.MakeRelativeUri(uri2);
            String result = relativeUri.ToString().Replace("/", "\\");
            return result;
        }

        #endregion // Protected Methods
        #region Private Methods

        private void MakeUnityFiles(ICollection<string> filenamesExported)
        {
            // The independent format also becomes aware of the list of files in each compilation unit so we can now create the unity files.
            // we create the unity files everytime and they should be writable, revert unchanged prevents them from being checked in if unchanged.
            if (Project.Unity.CompilationUnits.Count > 0)
            {
                string unityFolder;
                Independent.ProjectDirectory unityDir;
                CreateUnityFilesFolder(out unityFolder, out unityDir);

                foreach (KeyValuePair<String, List<Independent.ProjectFile>> compilationUnit in Project.Unity.CompilationUnits)
                {
                    string filename = string.Format("{0}{1}_{2}.cpp", UNITY_PREFIX, Project.Name, compilationUnit.Key);
                    string unityPath = Path.Combine(unityFolder, filename);
                    Independent.ProjectFile unityFile = new Independent.ProjectFile(unityPath);

                    // Decide if this unity file is for LTCG - that is decided by checking if any of its sub files are non LTCG.
                    // doing so does not guarantee that is will be set for every build config|platform, that can only be decided last minute.
                    unityFile.SuitableForLtcg = compilationUnit.Value.All(f => f.SuitableForLtcg);

                    /*if (unityFile.SuitableForLtcg)
                    {
                        this.Log.Message("Unity file {0} marked for LTCG for configs {1}", unityFile.Path, unityFile.SuitableForLtcg);
                    }
                    */
                    unityDir.Add(unityFile);
                    filenamesExported.Add(unityPath);

                    WarnReadOnlyUnityFile(unityPath);

                    CreateUnityFile(compilationUnit, unityPath);
                }
            }
        }

        /// <summary>
        /// Designates unity files 
        /// As per IProjectExporter
        /// </summary>
        public void DesignateUnity()
        {
            if (Unity)
            {
                // Populate each file with a unity designation if it needs one, and build the unity dictionary so that new unity files may be created.           
                if (IsProjectValid())
                {
                    foreach (Independent.ProjectDirectory dir in Project.Directories)
                    {
                        RecurseDirectoryUnity(dir);
                    }
                }
            }
        }

        private void RecurseDirectoryUnity(Independent.ProjectDirectory directory)
        {
            foreach (Independent.ProjectFile file in directory.Files)
            {
                if (!file.Parse && !file.Custom)
                {
                    string filename = Path.GetFileName(file.Path).ToLower();

                    if (!Project.Unity.Exclusions.Contains(filename))
                    {
                        string relFilename = FilenameToRelFilterPath(file.Path);
                        string path = relFilename.Replace('\\', '/');
                        
                        foreach (KeyValuePair<String, UnityCompilationUnitDescriptor> unit in Project.Unity.CompilationUnitSetup)
                        {
                            Match match = unit.Value.Regex.Match(path);
                            if (match.Success)
                            {
                                Project.Unity.AddFileToCompilationUnit(file, unit.Key);
                                break;// cant be matched in more than one regex if it is maybe raise an error or warning? DW: TODO ?
                            }
                        }
                    }
                }
            }

            foreach (Independent.ProjectDirectory dir in directory.Directories)
            {
                RecurseDirectoryUnity(dir);
            }
        }

        private KeyValuePair<String, List<Independent.ProjectFile>> CreateUnityFile(KeyValuePair<String, List<Independent.ProjectFile>> compilationUnit, string unityPath)
        {
            try
            {
                TextWriter tw = new StreamWriter(unityPath);
                tw.WriteLine(String.Empty);
                tw.WriteLine("#define {0}", UNITY_DEFINE);
                foreach (Independent.ProjectFile file in compilationUnit.Value)
                {
                    string relFilename = Path.Combine("..\\", FilenameToRelFilterPath(file.Path));
                    relFilename = relFilename.Replace("\\", "/");

                    tw.WriteLine("#include \"{0}\"", UNITY_PROLOGUE);
                    tw.WriteLine("#include \"{0}\"", relFilename);
                    tw.WriteLine("#include \"{0}\"", UNITY_EPILOGUE);
                }
                tw.Close();
            }
            catch
            {
                Log.Error("Unity compilation unit error :  {0}", unityPath);
            }
            return compilationUnit;
        }

        private void CreateUnityFilesFolder(out string unityFolder, out Independent.ProjectDirectory unityDir)
        {
            unityFolder = Path.GetDirectoryName(Project.Path);
            unityFolder = Path.Combine(unityFolder, UNITY_FOLDERNAME);
            unityDir = new Independent.ProjectDirectory(unityFolder);

            Independent.ProjectDirectory rootDir = Project.Directories[0];
            rootDir.Directories.Add(unityDir);
        }

        #endregion // Private Methods

        #region Protected Static Methods

        /// <summary>
        /// Evaluates / 'rebases' a path
        /// </summary>
        /// <param name="mainDirPath">the directory in which to express the other path relative to.</param>
        /// <param name="absoluteFilePath">the path to evaluate</param>
        /// <returns></returns>
        protected static string EvaluateRelativePath(string mainDirPath, string absoluteFilePath)
        {
            string[] firstPathParts = mainDirPath.Trim(Path.DirectorySeparatorChar).Split(Path.DirectorySeparatorChar);
            string[] secondPathParts = absoluteFilePath.Trim(Path.DirectorySeparatorChar).Split(Path.DirectorySeparatorChar);

            int sameCounter = 0;
            for (int i = 0; i < Math.Min(firstPathParts.Length, secondPathParts.Length); i++)
            {
                if (!firstPathParts[i].ToLower().Equals(secondPathParts[i].ToLower()))
                {
                    break;
                }
                sameCounter++;
            }

            if (sameCounter == 0)
            {
                return absoluteFilePath;
            }

            string newPath = String.Empty;
            for (int i = sameCounter; i < firstPathParts.Length; i++)
            {
                if (i > sameCounter)
                {
                    newPath += Path.DirectorySeparatorChar;
                }
                newPath += "..";
            }

            if (newPath.Length == 0)
            {
                newPath = ".";
            }

            for (int i = sameCounter; i < secondPathParts.Length; i++)
            {
                newPath += Path.DirectorySeparatorChar;
                newPath += secondPathParts[i];
            }

            return newPath;
        }

        /// <summary>
        /// ensures a guid is enclosed in squiggles
        /// <remarks>By not having this it led to guid regeneration across all files, including filters - a bad bug to find, hence this method.</remarks>
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        protected static string GuidToXmlString(string guid)
        {
            return "{" + guid + "}";
        }

        #endregion // Protected Static Methods
        #region Private Static Methods

        private static string MakeRelativeFilterPath(string path, string remove, string relativeToPath)
        {
            string evaluatedPath = EvaluateRelativePath(relativeToPath, Path.GetFullPath(path));
            if (remove != null)
            {
                evaluatedPath = evaluatedPath.Replace(remove, String.Empty);
            }
            evaluatedPath = evaluatedPath.Replace("..\\", String.Empty);
            evaluatedPath = evaluatedPath.Replace(".\\", String.Empty);
            return evaluatedPath;
        }    

        private void WarnReadOnlyUnityFile(string unityPath)
        {
            if (File.Exists(unityPath))
            {
                FileAttributes fileAttributes = File.GetAttributes(unityPath);
                if ((fileAttributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                {
                    Log.Warning("Readonly File {0} should be writable.", unityPath);
                }
            }
        }
        #endregion // Private Static Methods
    } // class ExporterBase
} // namespace ProjectGenerator.Exporters
