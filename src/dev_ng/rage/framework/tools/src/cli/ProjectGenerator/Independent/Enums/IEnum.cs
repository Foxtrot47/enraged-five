﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectGenerator.Independent
{
    /// <summary>
    /// Enum interface 
    /// - enforces EnumToString & Match
    /// </summary>
    public interface IEnum
    {
        #region Public Properties
        /// <summary>
        /// Converts the enum to a string
        /// </summary>
        /// <returns>the string</returns>
        string EnumToString();

        /// <summary>
        /// returns true if enum matches the passed string, otherwise returns false.
        /// </summary>
        /// <param name="val">the string to compare</param>
        /// <returns>true if matched</returns>
        bool Match(string val);
        #endregion // Public Properties
    } // interface ICustomEnum
}
