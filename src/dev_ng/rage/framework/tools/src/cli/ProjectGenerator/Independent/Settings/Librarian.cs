﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;


namespace ProjectGenerator.Independent
{
    /// <summary>
    /// Librarian settings
    /// </summary>
    [Serializable]
    public class Librarian : SettingExtender
    {
        #region Properties
        public string OutputFilename { get; set; }
        public List<string> AdditionalDependencies { get; set; }
        public List<string> AdditionalOptions { get; set; }
        public bool LinkLibraryDependencies { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor
        /// </summary>
        public Librarian()
        {
            OutputFilename = String.Empty;
            AdditionalDependencies = new List<string>();
            AdditionalOptions = new List<string>();
            LinkLibraryDependencies = false;
        }
        #endregion // Constructor(s)
    } // class SettingsLibrarian
} // namespace ProjectGenerator.Independent
