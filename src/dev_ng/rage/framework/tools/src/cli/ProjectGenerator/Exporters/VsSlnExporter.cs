﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Reflection;
using System.Collections;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using RSG.Base.Configuration;

namespace ProjectGenerator.Exporters
{
    /// <summary>
    /// Exporter for Visual Studio Solution Files
    /// - base class functionality used by VS2010 & VS2012.
    /// </summary>
    public abstract class VsSlnExporter :
                 ExporterBase                    
    {
        #region Private Constants
        // Visual Studio fileType
        private static readonly string VS_FILETYPE = ".sln";
        #endregion //Constants

        #region Constuctor(s)
        /// <summary>
        /// Static constructor
        /// </summary>
        static VsSlnExporter()
        {
        }

        /// <summary>
        /// Parameterised contructor
        /// </summary>
        /// <param name="mangle"></param>
        /// <param name="unity"></param>
        public VsSlnExporter(IUniversalLog log, CommandOptions options, string mangle, bool unity)
            : base(log, options, mangle, unity, false, true)
        {
        }
        #endregion // Constuctor(s)

        #region Protected Properties
        /// <summary>
        /// The version of visual studio eg. "VS2010", "VS2012" etc.
        /// - Implemented by the sub class
        /// </summary>        
        protected abstract string VsVersion { get; }
        #endregion // Protected Properties
        #region Private Properties
        private string StandardSuffix
        {
            get { return Unity ? "_" + VsVersion + UNITY + VS_FILETYPE : "_" + VsVersion + VS_FILETYPE; }
        }

        private string MangledSuffix
        {
            get { return Unity ? "_" + VsVersion + UNITY + Mangler + VS_FILETYPE : "_" + VsVersion + Mangler + VS_FILETYPE; }
        }

        private string MangledProjectSuffixVcxProj
        {
            get { return Unity ? "_" + VsVersion + UNITY + Mangler + ProjExporter.VS_FILETYPE_VCXPROJ : "_" + VsVersion + Mangler + ProjExporter.VS_FILETYPE_VCXPROJ; }
        }

        private string MangledProjectSuffixCsProj
        {
            get { return Unity ? "_" + VsVersion + UNITY + Mangler + ProjExporter.VS_FILETYPE_CSPROJ : "_" + VsVersion + Mangler + ProjExporter.VS_FILETYPE_CSPROJ; }
        }
        #endregion // Private Properties

        #region Public Methods
        /// <summary>
        /// The type of exporter : DW TODO: I've now got a mix of interface / abstract methods, that isn't ideal
        /// </summary>
        public abstract Exporter ExporterType { get; }

        /// <summary>
        ///  Interprets independent format to predict the output filenames that will be generated.
        /// </summary>
        /// <param name="filenames">The filenames that will be generated</param>
        public void PreDetermineExportedFiles(ICollection<string> filenames)
        {
            if (IsSolutionValid())
            {
                string slnFullPath = GetSlnOutputFilename(Solution);
                filenames.Add(slnFullPath);
            }
        }

        /// <summary>
        /// Export the independent project
        /// (Independent.IProjectExporter)
        /// </summary>
        /// <param name="project">the project to export</param>
        /// <param name="filenamesExported">a list to populate of all the files created / exported.</param>
        /// <returns></returns>
        public bool Export(ICollection<string> filenamesExported, bool verify = false)
        {
            if (IsSolutionValid())
            {
                return ExportSln(Solution, filenamesExported, verify);
            }

            return true;
        }

        /// <summary>
        /// Verify that the solution exists, loads into VS & builds ok.
        /// </summary>
        /// <param name="filename">the filename to verify</param>
        /// <returns>true if all ok</returns>
        public bool Verify(string filename)
        {
            if (!File.Exists(filename))
            {
                return false;
            }

            return true;
        }
        #endregion // Public Methods
        #region Private methods
        /// <summary>
        /// create the solution file
        /// </summary>
        /// <param name="solution"></param>
        /// <param name="filenamesExported"></param>
        /// <param name="verify"></param>
        /// <returns></returns>
        private bool ExportSln(Independent.Solution solution, ICollection<string> filenamesExported, bool verify)
        {
            string outputFilenameFullPath = GetSlnOutputFilename(solution);

            try
            {
                TextWriter tw = new StreamWriter(outputFilenameFullPath);
                WriteHeader(tw);

                WriteProjects(solution, tw);

                tw.WriteLine("Global");
                {
                    WriteAllConfigs(solution, tw);
                    WriteSolutionProjectMappingBuildDeployToProject(solution, tw);
                    WritePreSolutionSection(tw);
                }
                tw.WriteLine("EndGlobal");

                tw.Close();
            }
            catch (Exception ex)
            {
                Log.ToolException(ex, "Solution Export Exception {0}", outputFilenameFullPath);
            }

            bool ok = true;

            if (verify)
            {
                ok = Verify(outputFilenameFullPath);
            }

            if (ok)
            {
                filenamesExported.Add(outputFilenameFullPath);
            }

            return ok;
        }

        private bool IsSolutionProjectValid(Independent.SolutionProject slnProj)
        {
            if (slnProj.Unity == null ||    // TriState bool : Valid for both unity and non unity exports
                slnProj.Unity == Unity)     // Valid for this type of export if the same, don't freak out!, tri-state 'bool' is cool!
            {
                foreach (Exporter exporter in slnProj.Exporters)
                {
                    if (exporter == Exporter.ALL || exporter == Exporter.INVALID)
                    {
                        return true;
                    }

                    if (exporter == ExporterType)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private void WriteProjects(Independent.Solution solution, TextWriter tw)
        {
            foreach (KeyValuePair<string, Independent.SolutionProject> slnProjKVP in solution.Projects.Where( n => n.Value.HasValidMapping()))
            {
                Independent.SolutionProject slnProj = slnProjKVP.Value;

                if (IsSolutionProjectValid(slnProj))
                {
                    string projectFilename = GetSolutionProjectFilename(slnProj, MangledProjectSuffixVcxProj);

                    if (String.IsNullOrEmpty(projectFilename))
                    {
                        projectFilename = GetSolutionProjectFilename(slnProj, MangledProjectSuffixCsProj);
                    }

                    if (String.IsNullOrEmpty(projectFilename))
                    {
                        Log.Error("A filename in the solution cannot be found : {0}", slnProj.Name);
                    }

                    string line = string.Format("Project(\"{{{0}}}\") = \"{1}\", \"{2}\", \"{{{3}}}\"", Solution.Guid, slnProj.Name, projectFilename, slnProj.Guid);
                    tw.WriteLine(line);
                    if (slnProj.Dependencies.Count > 0)
                    {
                        tw.WriteLine("\tProjectSection(ProjectDependencies) = postProject");
                        foreach (Independent.SolutionProject depProj in slnProj.Dependencies.Where( n => n.HasValidMapping()))
                        {
                            string guid = depProj.Guid;
                            string guidLine = string.Format("\t\t{{{0}}} = {{{1}}}", guid, guid);
                            tw.WriteLine(guidLine);
                        }
                        tw.WriteLine("\tEndProjectSection");
                    }
                    tw.WriteLine("EndProject");
                }
            }
        }

        private string GetSolutionProjectFilename(Independent.SolutionProject slnProj, String suffix)
        {
            string projectFilename = slnProj.UnSuffixedPath + suffix;
           // Uri uri = new Uri(Path.Combine(Directory.GetCurrentDirectory(),projectFilename));
           // string fullProjectFilename = uri.LocalPath;
            
            // Handle filenames that do not exist : usually a bad sign of naming convention
            if (!File.Exists(projectFilename))
            {
                string[] replacements = { UNITY, Mangler, UNITY + Mangler, VsVersion + UNITY, VsVersion + UNITY + Mangler };

                foreach (string replace in replacements)
                {
                    if (replace != null && replace.Length > 0)
                    {
                        string newProjectFilename = projectFilename.Replace(replace, String.Empty);

                        if (File.Exists(newProjectFilename))
                        {
                            projectFilename = newProjectFilename;
                            return projectFilename;
                        }
                    }
                }
            }
            else
            {
                return projectFilename;
            }

            return null;            
        }

        private string GetSlnOutputFilename(Independent.Solution solution)
        {
            string outputFilename = solution.Name + MangledSuffix;
            string outputFilenameFullPath = Path.Combine(Directory.GetCurrentDirectory(), outputFilename);
            return outputFilenameFullPath;
        }
        #endregion // Private Methods
        #region Protected Methods
        protected abstract void WriteHeader(TextWriter tw);
        #endregion // Protected Methods
        #region Private Static Methods
        private static void WritePreSolutionSection(TextWriter tw)
        {
            tw.WriteLine("\tGlobalSection(SolutionProperties) = preSolution");
            tw.WriteLine("\t\tHideSolutionNode = FALSE");
            tw.WriteLine("\tEndGlobalSection");
        }

        private void WriteSolutionProjectMappingBuildDeployToProject(Independent.Solution solution, TextWriter tw)
        {
            tw.WriteLine("\tGlobalSection(ProjectConfigurationPlatforms) = postSolution");
            foreach (KeyValuePair<string, Independent.SolutionProject> slnProjKVP in solution.Projects)
            {
                Independent.SolutionProject slnProj = slnProjKVP.Value;
                foreach (string config in solution.Configs)
                {
                    foreach (string platform in solution.Platforms)
                    {
                        string slnconfig = string.Format("{0}|{1}", config, platform);
                        if (slnProj.SolutionProjectSettingDict.ContainsKey(slnconfig))
                        {
                            Independent.SolutionProjectSetting slnProjSetting = slnProj.SolutionProjectSettingDict[slnconfig];
                            if (slnProjSetting.ProjectConfig != null && slnProjSetting.ProjectConfig.Length > 0)
                            {
                                string line = string.Format("\t\t{{{0}}}.{1}.ActiveCfg = {2}", slnProj.Guid, slnconfig, slnProjSetting.ProjectConfig);
                                tw.WriteLine(line);
                                if (slnProjSetting.Build)
                                {
                                    string buildLine = string.Format("\t\t{{{0}}}.{1}.Build.0 = {2}", slnProj.Guid, slnconfig, slnProjSetting.ProjectConfig);
                                    tw.WriteLine(buildLine);
                                }
                                if (slnProjSetting.Deploy)
                                {
                                    string configLine = string.Format("\t\t{{{0}}}.{1}.Deploy.0 = {2}", slnProj.Guid, slnconfig, slnProjSetting.ProjectConfig);
                                    tw.WriteLine(configLine);
                                }
                            }
                            else
                            {
                                //Log.Warning("{0} {1} skipped", slnconfig, slnProj.Name);
                            }
                        }
                        else
                        {
                            Log.Error("{0} is not setup for project {1}", slnconfig, slnProj.Name);
                        }
                    }
                }
            }
            tw.WriteLine("\tEndGlobalSection");
        }

        private static void WriteAllConfigs(Independent.Solution solution, TextWriter tw)
        {
            List<string> writtenConfigPlatforms = new List<string>();

            tw.WriteLine("\tGlobalSection(SolutionConfigurationPlatforms) = preSolution");
            foreach (string config in solution.Configs)
            {
                foreach (string platform in solution.Platforms)
                {
                    foreach (KeyValuePair<string, Independent.SolutionProject> slnProjKVP in solution.Projects)
                    {
                        string slnconfig = string.Format("{0}|{1}", config, platform);
                        Independent.SolutionProject slnProj = slnProjKVP.Value;
                        if (slnProj.SolutionProjectSettingDict.ContainsKey(slnconfig))
                        {
                            Independent.SolutionProjectSetting slnProjSetting = slnProj.SolutionProjectSettingDict[slnconfig];
                            if (slnProjSetting.ProjectConfig != null && slnProjSetting.ProjectConfig.Length > 0)
                            {
                                string configPlatform = config + "|" + platform;
                                if (!writtenConfigPlatforms.Contains(configPlatform))
                                {
                                    writtenConfigPlatforms.Add(configPlatform);
                                    string line = string.Format("\t\t{0} = {0}", configPlatform);
                                    tw.WriteLine(line);
                                }
                            }
                        }
                    }
                }
            }
            tw.WriteLine("\tEndGlobalSection");
        }
        #endregion // Private Static Methods
    } // class VsSlnExporter
} // namespace ProjectGenerator.Exporters
