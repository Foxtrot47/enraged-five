﻿
namespace ProjectGenerator.Independent
{
    /// <summary>
    /// Interface for importing a format into the independent project format.
    /// </summary>
    interface IProjectImporter
    {
        /// <summary>
        /// Import method.
        /// </summary>
        /// <param name="makefileFilename">the makefilename to import</param>
        /// <returns>true if succeeded</returns>
        bool Import(string makefileFilename);

        /// <summary>
        /// The independent project format - the importer Can import into this object.
        /// </summary>
        Project Project { get; set; }

        /// <summary>
        /// The independent solution format - the importer can import into this object.
        /// </summary>
        Solution Solution { get; set; }

    } // interface IProjectImporter
} // namespace ProjectGenerator.Independent
