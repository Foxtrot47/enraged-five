﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace ProjectGenerator.Independent
{
    /// <summary>
    /// Generalised settings
    /// </summary>
    [Serializable]
    public class General
    {
        #region Properties
        public string OutputDirectoryName { get; set; }
        public string IntermediateDirectoryName { get; set; }
        public string TargetName { get; set; }
        public List<string> ExtenstionsToDeleteOnClean { get; set; }
        public List<string> IncludePath { get; set; }
        public List<string> LibraryPath { get; set; }
        public List<string> ReferencePath { get; set; }
        public List<string> ExecutablePath { get; set; }
        public List<string> MetadataPath { get; set; }
        public string BuildLogFilename { get; set; }        
        public string CharacterSet { get; set; } // TODO - make enum
		public string WholeProgramOptimization { get; set; }
        public string Exceptions { get; set; }
        public bool Rtti { get; set; }
        public bool IsolateConfigurationsOnDeploy { get; set; }
        public String PlatformToolset { get; set; }

        public List<string> References { get; set; }
        public List<ProjectReference> ProjectReferences { get; set; }
        public string DotNetFrameworkVersion { get; set; }
        
        #endregion //Properties

        #region Constructor(s)
        /// <summary>
        /// default constructor
        /// </summary>
        public General()
        {
            IsolateConfigurationsOnDeploy = false;
            ExtenstionsToDeleteOnClean = new List<string>();
            OutputDirectoryName = String.Empty;
            TargetName = String.Empty;
            IntermediateDirectoryName = String.Empty;
			CharacterSet = String.Empty;
			WholeProgramOptimization = String.Empty;
            PlatformToolset = String.Empty;
			Exceptions = "false";
            Rtti = false;
            IncludePath = new List<string>();
            LibraryPath = new List<string>();
            ExecutablePath = new List<string>();
            ReferencePath = new List<string>(); 
            MetadataPath = new List<string>();

            References = new List<string>();
            ProjectReferences = new List<ProjectReference>();
            DotNetFrameworkVersion = String.Empty;
        }
        #endregion // Constructor(s)
    } // class SettingsGeneral
} // namespace ProjectGenerator.Independent
