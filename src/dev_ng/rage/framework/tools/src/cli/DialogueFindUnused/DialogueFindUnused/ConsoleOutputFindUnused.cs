﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using RSG.Model.Dialogue.Export;

namespace DialogueFindUnused
{
    class ConsoleOutputFindUnused
    {
        private TextWriter m_writer;

        /// <summary>
        /// Process the data.
        /// </summary>
        public void ProcessData(string scriptLiteralFile, string dstarFileFolder, string outputFolder)
        {
            string keyListFile = "keys.txt";
            string unusedKeys = "unusedKeys.txt";
            keyListFile = Path.Combine(outputFolder, keyListFile);
            unusedKeys = Path.Combine(outputFolder, unusedKeys);

            if (!Directory.Exists(outputFolder)) { Directory.CreateDirectory(outputFolder); }

            KeyExtractor extractor = new KeyExtractor(keyListFile, dstarFileFolder);
            extractor.Run();

            if (File.Exists(unusedKeys))
            {
                File.Delete(unusedKeys);
            }

            using (Stream s = File.Open(unusedKeys, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read))
            {
                using (m_writer = new StreamWriter(s))
                {
                    CompareFiles compare = new CompareFiles();
                    compare.KeyNotFound += new KeyExtractorEventHandler(Compare_KeyNotFound);
                    compare.Compare(keyListFile, scriptLiteralFile);
                    compare.KeyNotFound -= Compare_KeyNotFound;
                }
            }
        }

        void Compare_KeyNotFound(object sender, KeyExtractorEventArgs e)
        {
            Console.WriteLine(e.ConversationKey);
            m_writer.WriteLine(e.ConversationKey);
        }
    }
}
