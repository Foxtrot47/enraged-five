﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.OS;

namespace DialogueFindUnused
{
    /// <summary>
    /// Dialogue Star helper program to find unused conversations.
    /// </summary>
    class Program
    {
        #region Private constants

        static readonly string OPTION_OUTPUT_FOLDER = "out";
        static readonly string OPTION_SCRIPT_LITERAL_FILE = "literal";
        static readonly string OPTION_DSTAR_FOLDER = "dstar";
        static readonly string OPTION_SHOW_HELP = "help";

        #endregion

        #region Program entry point

        /// <summary>
        /// Entry point to application.
        /// </summary>
        /// <param name="args">Command line arguments. See ShowHelp() for details.</param>
        static void Main(string[] args)
        {
            Getopt options = new Getopt(args,
                new LongOption[] 
                { 
                    new LongOption(OPTION_SHOW_HELP, LongOption.ArgType.None, 'h', "Show help"),
                    new LongOption(OPTION_OUTPUT_FOLDER, LongOption.ArgType.Required, 'e', "The output folder."),
                    new LongOption(OPTION_DSTAR_FOLDER, LongOption.ArgType.Required, 'r', "The folder that contains the *.dstar files."),
                    new LongOption(OPTION_SCRIPT_LITERAL_FILE, LongOption.ArgType.Required, 'f', "The script literals file.")
                }
            );

            string outputFolder = options.HasOption(OPTION_OUTPUT_FOLDER) ? options.Options[OPTION_OUTPUT_FOLDER].ToString() : "";
            string dstarFolder = options.HasOption(OPTION_DSTAR_FOLDER) ? options.Options[OPTION_DSTAR_FOLDER].ToString() : "";
            string scriptLiteral = options.HasOption(OPTION_SCRIPT_LITERAL_FILE) ? options.Options[OPTION_SCRIPT_LITERAL_FILE].ToString() : "";

            if (options.HasOption(OPTION_SHOW_HELP))
            {
                ShowHelp();
            }
            else
            {
                if (String.IsNullOrEmpty(outputFolder) || String.IsNullOrEmpty(dstarFolder) || String.IsNullOrEmpty(scriptLiteral))
                {
                    Console.WriteLine("You must specify a value for each of the script literal, output folder and dstar conversation folder.");
                    return;
                }

                ConsoleOutputFindUnused findUnused = new ConsoleOutputFindUnused();
                findUnused.ProcessData(scriptLiteral, dstarFolder, outputFolder);
            }
        }

        #endregion

        /// <summary>
        /// Show the help page.
        /// </summary>
        private static void ShowHelp()
        {
            Console.WriteLine("Dialogue Star Find Unused Conversations");
            Console.WriteLine();
            Console.WriteLine("Extract key values from .dstar files:");
            Console.WriteLine("\tdsref --literal X:\\scriptliteral.txt --dstar X:\\assets\\dstarfolder --out X:\\outputfolder");
            Console.WriteLine();
        }
    }
}
