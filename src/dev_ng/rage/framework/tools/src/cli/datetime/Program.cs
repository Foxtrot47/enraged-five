//
// File: Program.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Program.cs class
//

using System;
using System.Collections.Generic;
using System.Text;
using RsBase;

namespace datetime
{
    
    /// <summary>
    /// 
    /// </summary>
    class Program
    {
        #region Constants
        // Return codes
        private static readonly int EXIT_SUCCESS = 0;
        // Help/Usage options
        private static readonly String sC_s_str_Help = "help";
        private static readonly String sC_s_str_HelpShort = "h";
        // Version options
        private static readonly String sC_s_str_Version = "version";
        private static readonly String sC_s_str_VersionShort = "v";
        #endregion

        /// <summary>
        /// datetime console application entry-point
        /// </summary>
        /// <param name="args"></param>
        static int Main(String[] args)
        {
            cCommandLineParser parser = new cCommandLineParser(args);
            ParseOptions(parser);

            return (EXIT_SUCCESS);
        }

        /// <summary>
        /// Parse any command line options we have been specified
        /// </summary>
        /// <param name="parser"></param>
        static void ParseOptions(cCommandLineParser parser)
        {
            if (null == parser)
                throw new ArgumentNullException();
            bool bResult = false;

            if (null != parser.Parameters[sC_s_str_Version] || null != parser.Parameters[sC_s_str_VersionShort])
            {
                version();
                bResult = true;
            } if (null != parser.Parameters[sC_s_str_Help] || null != parser.Parameters[sC_s_str_HelpShort])
            {
                usage();
                bResult = true;
            }

            if (!bResult && parser.TrailingArguments.Count > 0)
                printDateTime(parser.TrailingArguments[0]);
            else if (!bResult)
                printDateTime(String.Empty);
        }

        /// <summary>
        /// Print out usage information to stdout
        /// </summary>
        static void usage()
        {
            Console.WriteLine("datetime [options] format_string");
            Console.WriteLine("Formatting date/time strings for use with DOS Batch Scripts.\n");
            Console.WriteLine("See http://msdn2.microsoft.com/en-us/library/az4se3k1.aspx.\n");
            Console.WriteLine(String.Format(" --h|--help\tDisplay this usage information (then quit)\n" +
                                            " --v|--version\tDisplay version information (then quit)\n" +
                                            " format_string\tDate and time format string in .Net format\n") );
        }

        /// <summary>
        /// Print out version information to stdout
        /// </summary>
        static void version()
        {
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            
            Console.WriteLine("datetime version " + assembly.GetName().Version.ToString());
            Console.WriteLine("Copyright � 2007 Rockstar Games");
            Console.WriteLine("by David Muir <david.muir@rockstarnorth.com>\n");
            Console.WriteLine("Formatting date/time strings for use with DOS Batch Scripts.\n");
        }

        /// <summary>
        /// Print out current date/time string using specified formatting string to stdout
        /// </summary>
        static void printDateTime(String sFormat)
        {
            if (0 == sFormat.Length)
                Console.WriteLine(DateTime.Now.ToString());
            else
                Console.WriteLine(DateTime.Now.ToString(sFormat));
        }
    }

} // End of datetime namespace

// End of file
