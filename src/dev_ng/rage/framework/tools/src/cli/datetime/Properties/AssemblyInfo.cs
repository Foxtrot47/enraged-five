using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("datetime")]
[assembly: AssemblyDescription("Print date/time strings using .Net compatible format strings")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Rockstar North")]
[assembly: AssemblyProduct("datetime")]
[assembly: AssemblyCopyright("Copyright © Rockstar Games 2007")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("b8534e63-63f7-4807-91b5-5868adc6daa5")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("1.0.0.41")]
[assembly: AssemblyFileVersion("1.0.0.41")]
