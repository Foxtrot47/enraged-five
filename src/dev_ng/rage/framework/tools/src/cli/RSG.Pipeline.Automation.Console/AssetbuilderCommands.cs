using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using P4API;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Client;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;
using RSG.Pipeline.Content.Algorithm;
using RSG.Pipeline.Services;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Engine;
using RSG.Pipeline.Core.Build;
using RSG.Pipeline.Automation.Common.Notifications.Alerts;
using RSG.Pipeline.Automation.Common.Notifications;
using System.Text.RegularExpressions;
using RSG.Pipeline.Automation.Common.Batch;

namespace RSG.Pipeline.Automation.Console
{
    /// <summary>
    /// Asset Builder targeted commands implementation.
    /// </summary>
    public class AssetbuilderCommands
    {
        #region Member Data
        private static readonly String LOG_CTX = "Console";
        private static IUniversalLog Log;
        private static AutomationServiceConsumer AutomationConsumer;
        private static AutomationAdminConsumer AdminConsumer;
        private static FileTransferServiceConsumer FileTransferConsumer;
        #endregion

        #region Initialization
        /// <summary>
        /// Initializes the static structure.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="automationConsumer"></param>
        /// <param name="adminConsumer"></param>
        /// <param name="fileTransferConsumer"></param>
        public static void Initialize(IUniversalLog log, AutomationServiceConsumer automationConsumer,
            AutomationAdminConsumer adminConsumer, FileTransferServiceConsumer fileTransferConsumer)
        {
            Log = log;
            AutomationConsumer = automationConsumer;
            AdminConsumer = adminConsumer;
            FileTransferConsumer = fileTransferConsumer;
        }
        #endregion

        #region Command Handlers
        /// <summary>
        /// asset_build, asset_rebuild [files/changelist/id] "description" command handler.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        /// <param name="rebuild"></param>
        public static void CommandHandlerBuild(CommandOptions options, String[] arguments, bool rebuild)
        {
            if (0 == arguments.Length)
            {
                Log.Error("No arguments specified for Asset Build; specify file(s), changelist or Job ID.");
                return;
            }

            // We may construct one or more jobs here; depending on the arguments.
            // Multiple files will go into a single job, multiple changelist or
            // other Job IDs will get separated into multiple jobs.
            ISet<int> changelistIds = new HashSet<int>();
            ISet<String> filenames = new HashSet<String>();
            ICollection<AssetBuilderJob> jobs = new List<AssetBuilderJob>();

            String descriptionInCommand = String.Empty;

            ICollection<String> branchFilters = new List<String>();

            using (P4 p4 = options.Config.Project.SCMConnect())
            {
                foreach (String argument in arguments)
                {
                    int changelistNumber = 0;

                    if (int.TryParse(argument, out changelistNumber))
                    {
                        Log.MessageCtx(LOG_CTX, String.Format("Changelist {0} queued for processing.", changelistNumber));
                        changelistIds.Add(changelistNumber);
                    }
                    else if (IsBranchFilter(options, argument))
                    {
                        branchFilters.Add(argument);
                    }
                    else
                    {
                        // Get paths that are in existance in the depot.
                        IEnumerable<String> files = DepotVerifiedPaths(null, p4, options.Branch, argument, false);

                        if (!files.Any())
                        {
                            if (!String.IsNullOrEmpty(descriptionInCommand))
                            {
                                Log.WarningCtx(LOG_CTX, "The job cannot be enqueued. No paths were found in perforce.");
                                return;
                            }
                            else
                            {
                                // If it isn't a CL num or a file it is considered the description.
                                descriptionInCommand = argument;
                                Log.MessageCtx(LOG_CTX, "Description (as derived from command) : \"{0}\"", descriptionInCommand);
                                continue;
                            }
                        }
                        else
                        {
                            Log.MessageCtx(LOG_CTX, "{0} files found in perforce.", files.Count());
                        }

                        // Ensure those files are within the branches.
                        ICollection<String> validFiles = FilterFilesInProjectsBranches(options, p4, files);

                        if (!validFiles.Any())
                        {
                            Log.ErrorCtx(LOG_CTX, "The job cannot be enqueued. No paths are under the project branches.");
                            return;
                        }
                        else
                        {
                            Log.MessageCtx(LOG_CTX, "Of {0} valid depot paths, {1} files found in branches.", files.Count(), validFiles.Count());
                        }

                        filenames.AddRange(validFiles);
                    }
                }

                if (String.IsNullOrEmpty(descriptionInCommand))
                {
                    Log.ErrorCtx(LOG_CTX, "You must supply a reason for this command, enter a string enclosed in quotes as the last argument using this command.");
                    return;                    
                }

                EngineFlags flags = EngineFlags.Default;
                if (rebuild)
                    flags |= EngineFlags.Rebuild;

                // Process Changelists : we simply add all the files out of them - otherwise if they were CL triggered 
                // they would sort corrrectly in portal reports and show the CL more than once - that would only confuse users.
                Changelist[] changelists = Changelist.Create(p4, changelistIds.ToArray());
                foreach (Changelist changelist in changelists)
                {
                    IEnumerable<FileMapping> changeListFiles = FileMapping.Create(p4, changelist.Files.ToArray());
                    IDictionary<ProjectBatchKey, IEnumerable<FileMapping>> changeListFilesBatched = ProjectBatcher.Batch(changeListFiles, options, Log, branchFilters);
                    ProjectBatcher.DisplayBatched(changeListFiles, changeListFilesBatched, Log);

                    foreach (KeyValuePair<ProjectBatchKey, IEnumerable<FileMapping>> kvp in changeListFilesBatched)
                    {
                        ProjectBatchKey projectBatchKey = kvp.Key;
                        IEnumerable<FileMapping> filemappings = kvp.Value;

                        AssetBuilderJob job = new AssetBuilderJob(projectBatchKey.Branch);
                        job.EngineFlags = flags;
                        String description = String.Format("Asset build command (using CL {0}) : {1} : {2}", changelist.Number, descriptionInCommand, changelist.Description);

                        //The server expects files to be in local format to check against dispatch groups.
                        IEnumerable<String> files = filemappings.Select(fm => fm.LocalFilename.Replace("/", "\\"));

                        job.Trigger = new UserRequestTrigger(Environment.UserName, files, Guid.NewGuid(), DateTime.UtcNow, description);
                        jobs.Add(job);
                    }
                }

                // Process all files specified together; as a single unit of work ( this may get split into more than one job )
                if (filenames.Count > 0)
                {
                    IEnumerable<FileMapping> otherFiles = FileMapping.Create(p4, filenames.ToArray());
                    IDictionary<ProjectBatchKey, IEnumerable<FileMapping>> changeListFilesBatched = ProjectBatcher.Batch(otherFiles, options, Log);
                    ProjectBatcher.DisplayBatched(otherFiles, changeListFilesBatched, Log);

                    foreach (KeyValuePair<ProjectBatchKey, IEnumerable<FileMapping>> kvp in changeListFilesBatched)
                    {
                        ProjectBatchKey projectBatchKey = kvp.Key;
                        IEnumerable<FileMapping> filemappings = kvp.Value;

                        AssetBuilderJob job = new AssetBuilderJob(projectBatchKey.Branch);
                        job.EngineFlags = flags;
                        String description = String.Format("Asset build command (files) : {0}", descriptionInCommand);

                        //The server expects files to be in local format to check against dispatch groups.
                        IEnumerable<String> files = otherFiles.Select(fm => fm.LocalFilename.Replace("/", "\\"));

                        job.Trigger = new UserRequestTrigger(Environment.UserName, filenames, Guid.NewGuid(), DateTime.UtcNow, description);
                        jobs.Add(job);
                    }
                }

                if (!jobs.Any())
                {
                    Log.ErrorCtx(LOG_CTX, "No jobs created.");
                    return;
                }

                // Enqueue jobs.
                Log.Message("{0} jobs created.", jobs.Count);
                foreach (IJob job in jobs)
                {
                    Log.Message("Enqueuing Job {0}", job.ID);
                    AutomationConsumer.EnqueueJob(job);
                }
            }
        }

        /// <summary>
        /// Sends rebuild commands for all resources within RSEXPORT
        /// - safe version that requires user prompt.
        /// </summary>
        /// <param name="options"></param>
        public static void CommandHandlerRebuildAllSafe(CommandOptions options, String[] arguments, IUniversalLog log)
        {
            Log.WarningCtx(LOG_CTX, " ");
            Log.WarningCtx(LOG_CTX, "**********************************************************************************************************");
            Log.WarningCtx(LOG_CTX, "NOW READ ALL OF THIS.");
            Log.WarningCtx(LOG_CTX, "This command will rebuild all assets on this branch. Please be certain this is intended before continuing.");
            Log.WarningCtx(LOG_CTX, "The project is {0}", options.Config.CoreProject.Name);
            Log.WarningCtx(LOG_CTX, "The branch is {0}", options.Branch.Name);
            Log.WarningCtx(LOG_CTX, "Press 'y' to proceed... any other key will abort.");
            Log.WarningCtx(LOG_CTX, "**********************************************************************************************************");
            Log.WarningCtx(LOG_CTX, " ");

            if (System.Console.ReadKey().KeyChar != 'y')
            {
                Log.MessageCtx(LOG_CTX, "Asset rebuild was aborted.");
                return;
            }

            Log.WarningCtx(LOG_CTX, " ");
            Log.WarningCtx(LOG_CTX, "*****************************************************************************************************************");
            Log.WarningCtx(LOG_CTX, "ASSET REBUILD ALL is now triggered, should you wish to prevent this now you will need to remove the jobs created.");
            Log.WarningCtx(LOG_CTX, "*****************************************************************************************************************");
            Log.WarningCtx(LOG_CTX, " ");

            // The last safety measure - a 10 second sleep.
            int sleep = 10;
            Log.WarningCtx(LOG_CTX, "...Sleep {0}s", sleep);
            System.Threading.Thread.Sleep(sleep*1000);

            CommandHandlerRebuildAll(options, arguments, log);
        }

        /// <summary>
        /// helper method that writes to log and to a temp file.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="sw"></param>
        /// <param name="message"></param>
        /// <param name="args"></param>
        public static void LogMsg(IUniversalLog log, StreamWriter streamWriter, String message, params Object[] args)
        {
            log.Message(message, args);
            streamWriter.WriteLine(message, args);
        }

        /// <summary>
        /// Sends rebuild commands for all resources within RSEXPORT
        /// - grouped by processor.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="arguments">regex filters of client path</param>
        /// <param name="log"></param>
        public static void CommandHandlerRebuildAll(CommandOptions options, String[] arguments, IUniversalLog log)
        {
            try
            {
                // Check for mandatory description
                if (arguments.Length <= 0)
                {
                    Log.ErrorCtx(LOG_CTX, "You must supply a reason, enter a string enclosed in quotes at the end of all your arguments passed.");
                    return;
                }

                String description = arguments.Last();
                Log.MessageCtx(LOG_CTX, "Reason for build interpreted as : {0}", description);

                if (String.IsNullOrEmpty(description) || String.IsNullOrWhiteSpace(description))
                {
                    Log.ErrorCtx(LOG_CTX, "You must supply a PROPER reason, enter a string ENCLOSED IN QUOTES at the end of all your arguments passed.");
                    return;
                }

                // remove the description.
                arguments = arguments.Take(arguments.Length - 1).ToArray();

                IConfig config = options.Config;
                Dictionary<KeyValuePair<Guid, String>, int> jobsCreatedDict = new Dictionary<KeyValuePair<Guid, String>, int>(); // DW: Probably could be a tuple.
                List<String> localFiles;

                // Temp file to detail the job guids and files what we have created  - this is handy for debugging the enormous amount of jobs the rebuild can create.
                String tempFilename = System.IO.Path.Combine(config.ToolsTemp, String.Format("asset_rebuild_jobs_and_files_{0}.txt", DateTime.Now.ToString("yyyy_MM_dd-HH_mm_ss")));
                System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(tempFilename);

                // extract from the command args the desired projects to rebuild.
                List<IProject> projects = new List<IProject>();
                List<String> args = arguments.ToList();
                int dlc = args.RemoveAll(a => String.Equals(a, "dlc", StringComparison.CurrentCultureIgnoreCase));
                int core = args.RemoveAll(a => String.Equals(a, "core", StringComparison.CurrentCultureIgnoreCase));

                if (dlc > 0)
                {
                    projects.AddRange(config.DLCProjects.Select(p => p.Value));
                }
                else
                {
                    // you meant core if you did not specify dlc ( default )
                    core = 1;
                }

                if (core > 0)
                {
                    projects.Add(options.CoreProject);
                }

                using (P4 p4 = options.CoreProject.SCMConnect())
                {
                    foreach (IProject project in projects)
                    {
                        try
                        {
                            IBranch branch;

                            log.MessageCtx(LOG_CTX, "\tRebuild : Handling project {0}", project.Name);

                            // Get all files in RSEXPORT in perforce

                            // for all projects we use the same branch : this may not exist for each branch so we check.
                            KeyValuePair<String, IBranch> branchKvp = project.Branches.Where(b => options.Branch.Name.Equals(b.Key, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();

                            if (branchKvp.Equals(default(KeyValuePair<String, IBranch>)))
                                continue;

                            branch = branchKvp.Value;

                            log.MessageCtx(LOG_CTX, "\t\tRebuild : Handling branch {0}", branch.Name);

                            P4RecordSet fstatRecords = p4.Run("fstat", Path.Combine(branch.Export, "..."));

                            HashSet<String> excludedExtensions = new HashSet<String>() { ".xml" };

                            localFiles = fstatRecords.Records
                                            .Where(fs => fs["headAction"] != "delete")
                                .Where(fs => !excludedExtensions.Contains(Path.GetExtension(fs["clientFile"])))
                                .Select(fs => System.IO.Path.GetFullPath(fs["clientFile"].ToLower())).ToList();

                            // Filter files based upon client path
                            foreach (String arg in args)
                            {
                                int numfiles = localFiles.Count();
                                Regex filter = new Regex(arg);
                                localFiles.RemoveAll(lf => !filter.IsMatch(lf));
                                LogMsg(log, streamWriter, "Filtered {0} files using Regex '{1}' ({2} => {3})", numfiles - localFiles.Count(), arg, numfiles, localFiles.Count());
                            }

                            // Create content tree
                            IContentTree tree = Factory.CreateTree(branch);

                            // Get all processes used by these local files.
                            IEngineParameters engineParams = new EngineParameters(branch, EngineFlags.Default, null, BuildType.All, log);
                            LogMsg(log, streamWriter, "Resolving processes for {0} files in {1}...", localFiles.Count(), branch.Export);
                            IEnumerable<IProcess> resolvedProcesses = ProcessResolver.Resolve(engineParams, tree, localFiles);
                            LogMsg(log, streamWriter, "...Resolved {0} processes.", resolvedProcesses.Count());

                            // Group the processes by name.
                            IEnumerable<IEnumerable<IProcess>> groupedProcessLists = resolvedProcesses.
                                GroupBy(p => p.ProcessorClassName).
                                Select(grp => grp.ToList()).ToList();
                            LogMsg(log, streamWriter, "Grouped {0} processes.", groupedProcessLists.Count());

                            // Show them
                            groupedProcessLists.ForEach(gp => LogMsg(log, streamWriter, "\t{0}", gp.ElementAt(0).ProcessorClassName));

                            // Now handle each processor group
                            foreach (IEnumerable<IProcess> groupedProcesses in groupedProcessLists)
                            {
                                String processName = groupedProcesses.ElementAt(0).ProcessorClassName;

                                LogMsg(log, streamWriter, " ");
                                LogMsg(log, streamWriter, "=================================================================================");
                                LogMsg(log, streamWriter, "Handling processor group {0}", processName);

                                ICollection<String> groupedFiles = new List<String>();

                                // Get all the grouped files.
                                foreach (IProcess process in groupedProcesses)
                                {
                                    // Match with local files and remove on match
                                    foreach (Content.File inputFileNode in process.Inputs.OfType<Content.File>())
                                    {
                                        String inputFileNodeFilename = System.IO.Path.GetFullPath(inputFileNode.AbsolutePath);
                                        int idx = localFiles.IndexOf(inputFileNodeFilename);
                                        if (idx >= 0)
                                        {
                                            groupedFiles.Add(localFiles[idx]);
                                            localFiles.RemoveAt(idx);
                                        }
                                    }

                                    // Match with directory and remove on match
                                    foreach (Content.Directory inputDirectoryNode in process.Inputs.OfType<Content.Directory>())
                                    {
                                        String inputFileNodeDirectoryName = System.IO.Path.GetFullPath(inputDirectoryNode.AbsolutePath);
                                        if (localFiles.Any(lf => lf.Contains(inputFileNodeDirectoryName)))
                                        {
                                            foreach (String localfile in localFiles.Where(lf => lf.Contains(inputFileNodeDirectoryName)))
                                            {
                                                groupedFiles.Add(localfile);
                                            }
                                            localFiles.RemoveAll(lf => lf.Contains(inputFileNodeDirectoryName));
                                        }
                                    }
                                }

                                // Switch on process name and handle as required.
                                // - Typically (and by default) we split further by directory into jobs.
                                switch (processName)
                                {
                                    /* Kept as an example.
                                case "dummy":
                                    // whatever?
                                    break;
                                case "RSG.Pipeline.Processor.Map.InstancePlacement.InstancePlacement":
                                    // TODO ?
                                    break;
                                                case "RSG.Pipeline.Processor.Animation.InGame.PreProcess" :
                                                case "RSG.Pipeline.Processor.Animation.Cutscene.PreProcess" : 
                                case "RSG.Pipeline.Processor.Platform.Rage" : 
                                case "RSG.Pipeline.Processor.Animation.Networks.NetworkPreProcess" : 
                                case "RSG.Pipeline.Processor.Map.PreProcess" : 
                                case "RSG.Pipeline.Processor.Map.DummyCombineProcessor" : 
                                case "RSG.Pipeline.Processor.Map.InstancePlacement.PreProcess" : 
                                                case "RSG.Pipeline.Processor.Common.CharacterProcessor" : 
                                                case "RSG.Pipeline.Processor.Vehicle.PreProcess" : 
                                                     */

                                    default:
                                        // Now group by directory.
                                        LogMsg(log, streamWriter, "Grouping process {0} by directory", processName);
                                        IEnumerable<IEnumerable<String>> groupedPathsToConvert = groupedFiles.
                                            GroupBy(p => System.IO.Path.GetDirectoryName(p)).
                                            Select(grp => grp.ToList()).ToList();

                                        LogMsg(log, streamWriter, "Found {0} directories in processor group", groupedPathsToConvert.Count());
                                        LogMsg(log, streamWriter, "=================================================================================");

                                        // Kick off a job for each directory in group.
                                        foreach (IEnumerable<String> groupedPaths in groupedPathsToConvert)
                                        {
                                            RebuildFiles(groupedPaths, options, log, streamWriter, jobsCreatedDict, branch, description);
                                        }
                                        break;
                                }
                            }

                            // Now handle any remaining files. ( stragglers not in content tree )
                            LogMsg(log, streamWriter, " ");
                            LogMsg(log, streamWriter, "=================================================================================");
                            LogMsg(log, streamWriter, "*** Stragglers not in content tree : {0} files ***", localFiles.Count());

                            IEnumerable<IEnumerable<String>> groupedStragglersToConvert = localFiles.
                                 GroupBy(p => System.IO.Path.GetDirectoryName(p)).
                                 Select(grp => grp.ToList()).ToList();

                            LogMsg(log, streamWriter, "Found {0} directories in stragglers", groupedStragglersToConvert.Count());
                            LogMsg(log, streamWriter, "=================================================================================");

                            // Kick off a job for each directory in stragglers
                            foreach (IEnumerable<String> groupedPaths in groupedStragglersToConvert)
                            {
                                RebuildFiles(groupedPaths, options, log, streamWriter, jobsCreatedDict, branch, description);
                            }
                        }
                        catch (Exception e)
                        {
                            // because DLC is not always properly synced, yet is known as a project
                            log.WarningCtx(LOG_CTX, "Exception hit trying to rebuild project {0} (it will not be rebuilt) : {1}", project.Name, e);
                        }
                    }
                }

                // Show a simple summary
                int numJobs = jobsCreatedDict.Count();
                LogMsg(log, streamWriter, " ");
                LogMsg(log, streamWriter, "Summary");
                LogMsg(log, streamWriter, "=======\n");
                LogMsg(log, streamWriter, "Created {0} jobs.", numJobs);
                int i = 0;
                String prettyArgs = String.Join(" ", arguments);
                ICollection<String> summary = new List<String>() { String.Format("Rebuild command arguments: {0}", prettyArgs) };
                jobsCreatedDict.ForEach(kvp => summary.Add(String.Format("\t({0,4}/{1}) \tJob: {2} \t: ({3,5} files) \t: {4}", ++i, numJobs, kvp.Key.Key, kvp.Value, kvp.Key.Value)));
                summary.ForEach(s => LogMsg(log, streamWriter, s));

                // Send a ( hardcoded ) notification 
#warning DW: TODO : for now this is hardcoded - will take work to get this data driven through the notifications system in TaskBase. url:bugstar:1735792
                List<IEmailRecipient> notificationRecipients = new List<IEmailRecipient>() { new NotificationRecipient("AssetRebuildAlert", "*tools@rockstarnorth.com", NotificationRule.OnAll) };
                GeneralAlertNotification notifier = new GeneralAlertNotification(log, options, String.Format("Asset Rebuild created {0} jobs", numJobs), summary);
                notifier.Send(notificationRecipients, null);

                // Close the temp file
                streamWriter.Close();

                // We are finished - all that remains is for the jobs to complete now.
                log.Message("--- Asset Rebuild All job creation is complete ---");
                log.Message("You can check a report of the rebuild in {0}", tempFilename);
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Exception handling rebuild");
            }
        }

        /// <summary>
        /// Rebuild some file by creating an assetbuilder job
        /// - user request
        /// </summary>
        /// <param name="filenames"></param>
        public static void RebuildFiles(IEnumerable<string> filenames, CommandOptions options, IUniversalLog log, StreamWriter streamWriter, IDictionary<KeyValuePair<Guid, String>, int> jobsCreatedDict, IBranch branch, String description)
        {
            if (filenames.Any())
            {
                // Verify the paths are valid - only vlaid paths will be added to the rebuild.
                char[] invalidPathChars = Path.GetInvalidPathChars();

                ICollection<String> validFilenames = new List<String>();
                foreach (String filename in filenames)
                {
                    if (String.IsNullOrEmpty(filename))
                    {
                        log.Error("RebuildFiles : Empty filename");
                        continue;
                    }

                    // Check if there are any invalid characters.
                    int firstInvalidChar = filename.IndexOfAny(invalidPathChars);
                    if (firstInvalidChar >= 0)
                    {
                        log.Error("RebuildFiles : Invalid character at index {0} '{1}' in path '{2}' : it will not be built.", firstInvalidChar, filename[firstInvalidChar], filename);
                        log.Error("Invalid chars are '{0}'", String.Join(" ", invalidPathChars));
                        continue;
                    }

                    validFilenames.Add(filename);
                }

                // DW: this is just an interim fix until we can properly address all the issues of @ character.
                validFilenames = validFilenames.Select(vf => vf.Replace("@", "%40")).ToList();

                using (P4 p4 = options.Config.Project.SCMConnect())
                {
                    // Process all files specified together; as a single job (single job? - unless of course the files split over projects and branches)
                    IEnumerable<FileMapping> changeListFiles = FileMapping.Create(p4, validFilenames.ToArray());
                    IDictionary<ProjectBatchKey, IEnumerable<FileMapping>> changeListFilesBatched = ProjectBatcher.Batch(changeListFiles, options, Log);
                    ProjectBatcher.DisplayBatched(changeListFiles, changeListFilesBatched, Log);

                    foreach (KeyValuePair<ProjectBatchKey, IEnumerable<FileMapping>> kvp in changeListFilesBatched)
                    {
                        ProjectBatchKey projectBatchKey = kvp.Key;
                        IEnumerable<FileMapping> filemappings = kvp.Value;

                        AssetBuilderJob job = new AssetBuilderJob(branch);
                        job.EngineFlags = EngineFlags.Default | EngineFlags.Rebuild;
                        description = String.Format("Asset Rebuild All : {0}", description);
                        job.Trigger = new UserRequestTrigger(Environment.UserName, filemappings.Select(fm => fm.LocalFilename) , Guid.NewGuid(), DateTime.UtcNow, description);
                        AutomationConsumer.EnqueueJob(job);

                        // Write to log about what we created here
                        LogMsg(log, streamWriter, " ");
                        LogMsg(log, streamWriter, "\t(#{0}) Enqueuing Rebuild Job {1} : {2} files", jobsCreatedDict.Count() + 1, job.ID, filemappings.Count());
                        LogMsg(log, streamWriter, "\t------------------------------------------------------------------------------------------------");
                        filemappings.ForEach(f => LogMsg(log, streamWriter, "\t\t{0}", f));

                        // Record a stat on what we have created
                        jobsCreatedDict.Add(new KeyValuePair<Guid, String>(job.ID, System.IO.Path.GetDirectoryName(filenames.First())), filemappings.Count());
                    }
                }
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Helper function to processs a content node for the asset rebuild all command.
        /// - recursive
        /// - processes the localfiles and for paths which it wants to convert as a group it add them to pathsToConvert
        /// - 
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="process"></param>
        /// <param name="branch"></param>
        /// <param name="localFiles"></param>
        /// <param name="pathsToConvert">Can contain filenames and directories.</param>
        private static void ProcessContentNode(IContentTree tree, IProcess process, IBranch branch, ref List<String> localFiles, ref List<String> pathsToConvert)
        {
            foreach (IContentNode inputNode in process.Inputs)
            {
                if (inputNode is Content.File)
                {
                    Content.File inputFileNode = inputNode as Content.File;

                    String inputFileNodeAbsolutePath = System.IO.Path.GetFullPath(inputFileNode.AbsolutePath);
                    String branchExport = System.IO.Path.GetFullPath(branch.Export);

                    if (inputFileNodeAbsolutePath.StartsWith(branchExport))
                    {
                        // It's in export so add it to the paths to convert.
                        localFiles.Remove(inputFileNode.AbsolutePath);
                        pathsToConvert.Add(inputFileNode.AbsolutePath); 
                    }
                    else
                    {
                        IEnumerable<IProcess> parentProcesses = tree.FindProcessesWithOutput(inputFileNode);
                        foreach (Process parentProcess in parentProcesses)
                        {
                            ProcessContentNode(tree, parentProcess, branch, ref localFiles, ref pathsToConvert);
                        }
                    }
                }
                else if (inputNode is Content.Directory)
                {
                    Content.Directory inputDirectoryNode = inputNode as Content.Directory;

                    for (int index = 0; index < localFiles.Count(); index++)
                    {
                        String localFile = localFiles.ElementAt(index);

                        localFile = System.IO.Path.GetFullPath(localFile);
                        String inputDirPath = inputDirectoryNode.AbsolutePath + "\\";
                        inputDirPath = System.IO.Path.GetFullPath(inputDirPath);

                        if (localFile.StartsWith(inputDirPath))
                        {
                            localFiles.RemoveAt(index);
                            index--;
                        }
                    }

                    pathsToConvert.Add(inputDirectoryNode.AbsolutePath + "\\*");
                }
            }
        }

        /// <summary>
        /// Expand RS_EXPORT path in perforce. ( as opposed to what is on local disk )
        /// - If it ain't in perforce this will not return it.
        /// - If the head revision in not buildable ( purged, marked for delete ) it will not be returned.
        /// </summary>
        /// <param name="branch">The branch object.</param>
        /// <param name="pathname">            
        ///  - wildcarded
        ///  - depot
        ///  - local filesystem 
        ///  - containing environment variables
        ///  - using characters such as @, *</param>
        /// <returns>The valid depot paths discovered in p4 at this time.</returns>
        private static IEnumerable<String> DepotVerifiedPaths(IUniversalLog log, P4 p4, IBranch branch, String origPath, bool depot = true)
        {
            ICollection<String> resultPaths = new List<String>();

            try
            {
                if (log != null)
                    log.Message("DepotVerifiedPaths: origPath: {0}", origPath);

                // 1. Expanded Branch Env variables.
                String expandedPath = branch.Environment.Subst(origPath);
                if (log != null)
                    log.Message("DepotVerifiedPaths: expandedPath: {0}", expandedPath);

                // 2. Handle the @ character ( as used for animations ).
                String escapedPath = expandedPath.Replace("@", "%40");
                if (log != null)
                    log.Message("DepotVerifiedPaths: escapedPath: {0}", escapedPath);

                // Root the path and get the full path.
                String normalisedPath = escapedPath;
                if (!normalisedPath.StartsWith("//"))
                {
                    // 4. Env expand the path.
                    String envExpandedPath = System.Environment.ExpandEnvironmentVariables(normalisedPath);
                    if (log != null)
                        log.Message("DepotVerifiedPaths: envExpandedPath: {0}", envExpandedPath);

                    // 5. Root the path if required.
                    String rootedPath = Path.IsPathRooted(escapedPath) ? envExpandedPath : Path.Combine(branch.Export, envExpandedPath);
                    if (log != null)
                        log.Message("DepotVerifiedPaths: rootedPath: {0}", rootedPath);

                    // 6. Normalise the path.
                    normalisedPath = rootedPath.Replace(Path.AltDirectorySeparatorChar.ToString(), Path.DirectorySeparatorChar.ToString());
                    if (log != null)
                        log.Message("DepotVerifiedPaths: normalisedPath: {0}", normalisedPath);
                }

                // 7. Get the depot state of these files, what matters is the head revision, that's what we intend to build.
                // NO local state is of relevence, we should not assume that the console issuing the build has a good local filesystem and/or workspace mapping.
                FileState[] filestates = FileState.Create(p4, normalisedPath);
                if (log != null)
                    filestates.ForEach(fs => log.Message("DepotVerifiedPaths: Filestates: {0} rev {1} action {2}", fs.DepotFilename, fs.HaveRevision, fs.HeadAction));

                // 8. Query the head action - does this file exist in perforce in such a way that we could build it?
                // - What matters is what the state of the head revision is, that's what we want to build.
                bool warned = false;
                foreach (FileState filestate in filestates)
                {
                    if (filestate.HeadRevision > 0 &&
                        filestate.HeadAction != FileAction.Delete &&
                        filestate.HeadAction != FileAction.MoveAndDelete &&
                        filestate.HeadAction != FileAction.Purge)
                    {
                        String filenameToAdd = depot ? filestate.DepotFilename : Path.GetFullPath(filestate.ClientFilename).Replace("@", "%40");
                        resultPaths.Add(filenameToAdd);

                        if (log != null)
                            log.Message("DepotVerifiedPaths: resultPath: {0} : {1}", filenameToAdd, filestate.HeadAction);
                    }
                    else
                    {
                        // Too spammy
                        if (!warned)
                        {
                            if (log != null)
                                log.Warning("DepotVerifiedPaths: Path will not be built: {0} rev {1} action {2}. You are only warned about the first file that will not be build by your file search criteria.", filestate.DepotFilename, filestate.HeadRevision, filestate.HeadAction);
                            warned = true;
                        }
                    }
                }

                if (log != null)
                    log.Message("DepotVerifiedPaths: {0} files to build", resultPaths.Count);
            }
            catch (Exception ex)
            {
                if (log != null)
                    log.ToolException(ex, "DepotVerifiedPaths: Path {0} could not be evaluated in p4. Check logs to see how it was evaluted.", origPath);
            }
            // 9. Return paths in Depot or filesystem
            return resultPaths;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="options"></param>
        /// <param name="files"></param>
        /// <returns></returns>
        private static ICollection<string> FilterFilesInProjectsBranches(CommandOptions options, P4 p4, IEnumerable<String> files)
        {
            ICollection<String> validFiles = new List<String>();

            foreach (KeyValuePair<String, IProject> kvpProject in options.Config.AllProjects())
            {
                IProject project = kvpProject.Value;
                foreach (KeyValuePair<String, IBranch> kvpBranch in project.Branches)
                {
                    IBranch branch = kvpBranch.Value;

                    FileMapping export = FileMapping.Create(p4, branch.Export).FirstOrDefault();
                    if (export != null)
                    {
                        foreach (String filename in files)
                        {
                            if (filename.StartsWith(export.DepotFilename, StringComparison.OrdinalIgnoreCase) ||
                                Path.GetFullPath(filename).StartsWith(Path.GetFullPath(export.ExpandedLocalFilename), StringComparison.OrdinalIgnoreCase))
                            {
                                if (!validFiles.Contains(filename))
                                {
                                    validFiles.Add(filename.Replace("/", "\\"));
                                }
                            }
                        }
                    }
                }
            }
            return validFiles;
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Returns true if the string passed is a recognised branch name.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="branchName"></param>
        /// <returns></returns>
        private static bool IsBranchFilter(CommandOptions options, String branchName)
        {
            IDictionary<String, IProject> allProjects = options.Config.AllProjects();
            foreach (KeyValuePair<String, IProject> projectKvp in allProjects)
            {
                IProject project = projectKvp.Value;

                foreach (KeyValuePair<String, IBranch> branchKvp in project.Branches)
                {
                    IBranch branch = branchKvp.Value;
                    if (branchName.Equals(branch.Name, StringComparison.CurrentCultureIgnoreCase))
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        #endregion // Private methods
    }
} // RSG.Pipeline.Automation.Console namespace
