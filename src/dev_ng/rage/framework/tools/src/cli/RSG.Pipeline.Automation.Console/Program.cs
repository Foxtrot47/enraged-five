using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Client;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Automation.Common.Services.Messages;

namespace RSG.Pipeline.Automation.Console
{
    
    /// <summary>
    /// Automation Console command-line application main class.
    /// </summary>
    class Program
    {
        #region Constants
        private static readonly String LOG_CTX = "Console";
        private static readonly String OPT_HOST = "host";
        private static readonly String OPT_SCRIPT = "script";

        private const String CMD_HELP = "help";
        private const String CMD_EXIT = "exit";
        private const String CMD_CLIENTS = "clients";
        private const String CMD_CLIENT = "client";
        private const String CMD_JOBS = "jobs";
        private const String CMD_JOBSMATRIX = "jobsmatrix";
        private const String CMD_JOBSMATRIXGUID = "jobsmatrixguid";
        private const String CMD_JOBSMATRIXTIME = "jobsmatrixtime";     
        private const String CMD_JOB = "job";
        private const String CMD_UPLOAD = "upload";
        private const String CMD_DOWNLOAD = "download";
        private const String CMD_FILEAVAILABILITY = "files";
        private const String CMD_FILEAVAILABILITY2 = "files2";
        private const String CMD_ASSETBUILD = "asset_build";
        private const String CMD_ASSETREBUILD = "asset_rebuild";
        private const String CMD_ASSETREBUILD_ALL_SAFE = "asset_rebuild_all";
        private const String CMD_ASSETREBUILD_ALL = "asset_rebuild_all_unsafe";        
        private const String CMD_STATS = "stats";
        private const String CMD_CODEBUILD = "code_build";
        private const String CMD_STATUS = "status";
        private const String CMD_SHUTDOWN = "shutdown";
        private const String CMD_NETWORKEXPORT = "network_export";
        private const String CMD_INTEGRATIONS = "integrations";
        private const String CMD_UNREGISTER_WORKER = "unregister_worker";
        private const String CMD_SET_TASK_PARAM = "set_task_param";
        private const String CMD_GET_TASK_PARAMS = "get_task_params";
        private const String CMD_CLEAR_CACHE = "clear_cache";
        private const String CMD_SET_CLIENT_TICK_RATE = "set_client_tick_rate";
        private const String CMD_DISPATCH = "dispatch";
        private const String CMD_STOP_TASK = "stoptask";
        private const String CMD_START_TASK = "starttask";
        private const String CMD_FRAME_CAPTURE = "frame_capture";
        
        private static readonly KeyValuePair<String, String>[] CMD_INFORMATION = new KeyValuePair<String, String>[] { 
            new KeyValuePair<String, String>(CMD_HELP, "Display this command/help information."),
            new KeyValuePair<String, String>(CMD_EXIT, "Exit console."),
            new KeyValuePair<String, String>(CMD_CLIENTS, "Display table of connected clients."),
            new KeyValuePair<String, String>(CMD_JOBSMATRIX, "Displays table of jobs status organised in a CL matrix."),
            new KeyValuePair<String, String>(CMD_JOBSMATRIXGUID, "Displays table of jobs guid organised in a CL matrix."),
            new KeyValuePair<String, String>(CMD_JOBSMATRIXTIME, "Displays table of jobs processing time organised in a CL matrix."),
            new KeyValuePair<String, String>(CMD_JOBS, "[<GUID>] Display table of jobs."),
            new KeyValuePair<String, String>(CMD_JOB, "[<GUID>] Display table of jobs."),
            new KeyValuePair<String, String>(CMD_UPLOAD, "<GUID> <FILENAME> Upload file for job."),
            new KeyValuePair<String, String>(CMD_DOWNLOAD, "<GUID> <FILENAME> Download file for job."),
            new KeyValuePair<String, String>(CMD_FILEAVAILABILITY, "<GUID> Display list of files for a job."),
            new KeyValuePair<String, String>(CMD_FILEAVAILABILITY2, "<GUID> Display list of files (and sizes) for a job."),
            new KeyValuePair<String, String>(CMD_ASSETBUILD, "[<FILENAMES>] [<CHANGELISTS>] [<BRANCH NAME FILTER>] <\"DESC\"> : Invoke asset *build*."),
            new KeyValuePair<String, String>(CMD_ASSETREBUILD, "[<FILENAMES>] [<CHANGELISTS>] [<BRANCH NAME FILTER>] <\"DESC\"> : Invoke asset rebuild."),
            new KeyValuePair<String, String>(CMD_ASSETREBUILD_ALL_SAFE, "[<REGEX FILTER> : use \\ for paths] [<DLC>] [<CORE>] <\"DESC\"> Invoke rebuild of all assets. eg. asset_rebuild_all dlc \\mpapartment\\ \"your manditory description here\""),
            new KeyValuePair<String, String>(CMD_STATS, "Create a performance statistic report."),
            new KeyValuePair<String, String>(CMD_STATUS, "Display status information."),
            new KeyValuePair<String, String>(CMD_CODEBUILD, "<FILENAME> Invoke code build based on jobdef xml."),
            new KeyValuePair<String, String>(CMD_INTEGRATIONS, "Show Integrations."),
            new KeyValuePair<String, String>(CMD_SHUTDOWN, "[SYNCLABEL|SYNCHEAD] [RESUME] Shutdown workers and host."),
            new KeyValuePair<String, String>(CMD_NETWORKEXPORT, "Invoke network export for your local max and scene xml file(s)"),
            new KeyValuePair<String, String>(CMD_UNREGISTER_WORKER, "Unregister a worker from the server."),
            new KeyValuePair<String, String>(CMD_SET_TASK_PARAM, "[TASKNAME] [PARAM NAME] [PARAM VALUE] Sets a local task parameter."),
            new KeyValuePair<String, String>(CMD_GET_TASK_PARAMS, "[TASKNAME] Display all task parameters."),
            new KeyValuePair<String, String>(CMD_CLEAR_CACHE, "Clear Cache."),            
            new KeyValuePair<String, String>(CMD_SET_CLIENT_TICK_RATE, "<TIME_MS> Sets the tick rate between client updates"),
            new KeyValuePair<String, String>(CMD_DISPATCH, "<GUID> <HOST> Assign a job guid to a host."),
            new KeyValuePair<String, String>(CMD_START_TASK, "<TASKNAME> Starts a task."),
            new KeyValuePair<String, String>(CMD_STOP_TASK, "<TASKNAME> Stops a task."),
            new KeyValuePair<String, String>(CMD_FRAME_CAPTURE, "<FBX> Triggers the frame capture chain")            
        };
        #endregion // Constants

        #region Enumerations and Structures
        /// <summary>
        /// 
        /// </summary>
        private enum CommandResult
        {
            Ok,
            Ignored,
            Exit,
        }
        #endregion // Enumerations and Structures

        #region Member Data
        private static IUniversalLog log;
        private static bool isRunning;
        private static AutomationServiceConsumer AutomationConsumer;
        private static AutomationAdminConsumer AdminConsumer;
        private static FileTransferServiceConsumer FileTransferConsumer;
        #endregion // Member Data

        /// <summary>
        /// Main entry-point.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        [STAThread]
        static int Main(String[] args)
        {
            try
            {
                LogFactory.Initialize();
                LongOption[] lopts = new LongOption[] {
                    new LongOption(OPT_HOST, LongOption.ArgType.Required,
                        "Host for Automation and File Transfer Services."),
                    new LongOption(OPT_SCRIPT, LongOption.ArgType.Required,
                        "Command script filename to execute (e.g. for batching jobs).")
                };
                CommandOptions options = new CommandOptions(args, lopts);
                InitialiseConsole();
                isRunning = InitialiseConsumers(options);

                AssetbuilderCommands.Initialize(log, AutomationConsumer, AdminConsumer, FileTransferConsumer);

                // Determine if we have a command-line script to execute.
                if (options.ContainsOption(OPT_SCRIPT))
                {
                    String scriptFilename = (String)options[OPT_SCRIPT];
                    if (File.Exists(scriptFilename))
                    {
                        using (StreamReader sr = new StreamReader(scriptFilename))
                        {
                            String commandLine = String.Empty;
                            while (null != (commandLine = sr.ReadLine()))
                            {
                                CommandResult result = HandleCommand(options, commandLine);
                                if (CommandResult.Exit == result)
                                {
                                    log.Message("Exiting...");
                                    isRunning = false;
                                }
                            }
                        }
                    }
                    else
                    {
                        log.ErrorCtx(LOG_CTX, "Script file: {0} does not exist.  Ignoring.",
                            scriptFilename);
                    }
                }

                // Regular standard input command input loop.
                while (isRunning)
                {
                    PrettyPrintCaret();
                    String commandString = System.Console.ReadLine();
                    CommandResult result = HandleCommand(options, commandString);

                    if (CommandResult.Exit == result)
                    {
                        log.Message("Exiting...");
                        isRunning = false;
                    }
                }
            }
            catch (Exception ex)
            {
                System.Console.WriteLine(ex.StackTrace);

                log.ToolException(ex, "Automation Console exception");
                return (Constants.Exit_Failure);
            }
            finally
            {
                Shutdown();
            }
            return (log.HasErrors ? Constants.Exit_Failure : Constants.Exit_Success);
        }

        /// <summary>
        /// Handle processing of a unparsed command string.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="commandString"></param>
        /// <returns>Command result object.</returns>
        static CommandResult HandleCommand(CommandOptions options, String commandString)
        {
            if (String.IsNullOrEmpty(commandString))
                return (CommandResult.Ignored); // Skip.

            // Split the options so that options can be enclosed in quotes
            Regex regex = new Regex( @"((""((?<token>.*?)(?<!\\)"")|(?<token>[\S]+))(\s)*)", RegexOptions.None );
            IEnumerable<String> matches = ( from Match m in regex.Matches(commandString) 
                                            where m.Groups[ "token" ].Success
                                            select m.Groups[ "token" ].Value).ToList();

            String[] tokens = matches.ToArray();
            if (0 == tokens.Length)
                return (CommandResult.Ignored); // Skip.

            String command = tokens[0].ToLower();
            String[] arguments = new String[0];
            if (tokens.Length > 1)
            {
                arguments = new String[tokens.Length - 1];
                Array.Copy(tokens, 1, arguments, 0, tokens.Length - 1);
            }

            CommandResult result = ParseCommand(options, command, arguments);
            return (result);
        }

        /// <summary>
        /// Initialise console display.
        /// </summary>
        static void InitialiseConsole()
        {
            var descriptionAttribute = Assembly.GetEntryAssembly()
                    .GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false)
                    .OfType<AssemblyDescriptionAttribute>()
                    .FirstOrDefault();

            if (descriptionAttribute != null)
                System.Console.Title = descriptionAttribute.Description;
            log = LogFactory.ApplicationLog;
            LogFactory.CreateApplicationConsoleLogTarget();
            System.Console.CancelKeyPress += ShutdownKill;
        }

        /// <summary>
        /// Initialise Automation consumers.
        /// </summary>
        static bool InitialiseConsumers(CommandOptions options)
        {
            bool showHelp = options.ShowHelp;
            Debug.Assert(options.ContainsOption(OPT_HOST), "No host option specified.");
            if (!options.ContainsOption(OPT_HOST))
            {
                log.Error("No host specified.");
                ShowHelp(options);
                return (false);
            }

            String host = options[OPT_HOST] as String;
            log.Message("Connecting to: {0}...", host);

            Uri automationService = new Uri(String.Format("net.tcp://{0}:7010/automation.svc", host));
            AutomationConsumer = new AutomationServiceConsumer(automationService);
            AdminConsumer = new AutomationAdminConsumer(automationService);
            log.Message("Connected to {0}.", automationService);

            Uri fileTransferService = new Uri(String.Format("net.tcp://{0}:7010/FileTransfer.svc", host));
            FileTransferConsumer = new FileTransferServiceConsumer(options.Config, fileTransferService);
            log.Message("Connected to {0}.", fileTransferService);

            return (true);
        }

        /// <summary>
        /// Shutdown cleanly.
        /// </summary>
        static void Shutdown()
        {
            log.Message("Shutting down Automation Console");
            LogFactory.ApplicationShutdown();
        }

        /// <summary>
        /// Show help usage information.
        /// </summary>
        /// <param name="options"></param>
        static void ShowHelp(CommandOptions options)
        {
            System.Console.WriteLine(options.GetUsage());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="options"></param>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        /// <returns></returns>
        static CommandResult ParseCommand(CommandOptions options, String command,
            String[] arguments)
        {
            System.Console.WriteLine();
            CommandResult result = CommandResult.Ok;
            switch (command)
            {
                case CMD_EXIT:
                    result = CommandResult.Exit;
                    break;
                case CMD_HELP:
                    CommandHelpHandler(options, command, arguments);
                    break;
                case CMD_UPLOAD:
                    CommandUploadHandler(options, command, arguments);
                    break;
                case CMD_DOWNLOAD:
                    CommandDownloadHandler(options, command, arguments);
                    break;
                case CMD_FILEAVAILABILITY:
                    CommandFileAvailabilityHandler(options, command, arguments);                            
                    break;    
                case CMD_FILEAVAILABILITY2:
                    CommandFileAvailability2Handler(options, command, arguments);
                    break;
                case CMD_JOBSMATRIX:
                    CommandJobHandler(options, command, arguments, true);
                    break;
                case CMD_JOBSMATRIXGUID:
                    CommandJobHandler(options, command, arguments, true, "guid");
                    break;
                case CMD_JOBSMATRIXTIME:
                    CommandJobHandler(options, command, arguments, true, "time");
                    break;
                case CMD_JOBS:
                case CMD_JOB:
                    CommandJobHandler(options, command, arguments);
                    break;
                case CMD_CLIENTS:
                case CMD_CLIENT:
                    CommandClientsHandler(options, command, arguments);
                    break;
                case CMD_ASSETBUILD:
                    AssetbuilderCommands.CommandHandlerBuild(options, arguments, false);
                    break;
                case CMD_ASSETREBUILD:
                    AssetbuilderCommands.CommandHandlerBuild(options, arguments, true);
                    break;
                case CMD_ASSETREBUILD_ALL:
                    AssetbuilderCommands.CommandHandlerRebuildAll(options, arguments, log);
                    break;
                case CMD_ASSETREBUILD_ALL_SAFE:
                    AssetbuilderCommands.CommandHandlerRebuildAllSafe(options, arguments, log);
                    break;
                case CMD_STATS:
                    CommandStatsHandler(options, command, arguments);
                    break;
                case CMD_CODEBUILD:
                    CommandCodeBuildHandler(options, command, arguments);
                    break;
                case CMD_STATUS:
                    CommandStatusHandler(options, command, arguments);
                    break;
                case CMD_SHUTDOWN:
                    CommandShutdownHandler(options, command, arguments);
                    break;
                case CMD_NETWORKEXPORT:
                    CommandNetworkExport(options, arguments);
                    break;
                case CMD_INTEGRATIONS:
                    CommandIntegrations(options, arguments);
                    break;
                case CMD_UNREGISTER_WORKER:
                    CommandUnregisterWorker(options, arguments);
                    break;
                case CMD_SET_TASK_PARAM:
                    CommandSetTaskParam(options, arguments);
                    break;
                case CMD_GET_TASK_PARAMS:
                    CommandGetTaskParams(options, arguments);
                    break;
                case CMD_CLEAR_CACHE:
                    CommandClearAllWorkersCacheHandler(options);
                    break;
                case CMD_SET_CLIENT_TICK_RATE:
                    CommandSetClientTickRate(options, arguments);
                    break;
                case CMD_DISPATCH:
                    CommandDispatch(options, arguments);
                    break;   
                case CMD_START_TASK:
                    CommandStartTask(options, arguments);
                    break;
                case CMD_STOP_TASK:
                    CommandStopTask(options, arguments);
                    break;
                case CMD_FRAME_CAPTURE:
                    CommandFrameCapture(options, arguments);
                    break;
                default:
                    CommandNotImplementedHandler(options, command, arguments);
                    break;
            }
            System.Console.WriteLine();
            return (result);
        }

        #region Command Handler Methods

        /// <summary>
        /// Set the dispatch of the job guid to a particular host.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="arguments"></param>
        public static void CommandDispatch(CommandOptions options, String[] arguments)
        {
            if (arguments.Length == 2)
            {
                Guid guid;
                if (Guid.TryParse(arguments[0], out guid))
                {
                    String host = arguments[1];
                    AdminConsumer.SetJobDispatch(guid, host);
                }
                else
                {
                    log.Error("Invalid guid {0}", arguments[0]);
                }
            }
            else
            {
                log.Error("Invalid dispatch command, require 2 arguments [GUID] [HOST]");
            }
        }

        /// <summary>
        /// Command not implemented handler.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        private static void CommandNotImplementedHandler(CommandOptions options, String command, String[] arguments)
        {
            log.ErrorCtx(LOG_CTX, "Command '{0}' ({1} arguments) not implemented.  Ignoring.",
                command, arguments.Length);
        }

        /// <summary>
        /// Command not implemented handler.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
#warning DHM FIX ME: only print out command's info if not "help"?
        private static void CommandHelpHandler(CommandOptions options, String command, String[] arguments)
        {
            log.MessageCtx(LOG_CTX, "Available console commands:");

            int longestCommandLen = CMD_INFORMATION.Max(cmd => cmd.Key.Length);
            foreach (KeyValuePair<String, String> cmdInfo in CMD_INFORMATION)
            {
                log.MessageCtx(LOG_CTX, "{0}\t{1}", cmdInfo.Key.PadRight(longestCommandLen), cmdInfo.Value);
            }
        }

        /// <summary>
        /// jobs, job [id] command handler.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        private static void CommandJobHandler(CommandOptions options, String command,
            String[] arguments, bool displayAsMatrix = false, string matrixField = "")
        {
            string jobFormat = "{0,-36} {1,-12} {2,-10} {3,-17} {4,-9} {5,21} {6,-17} {7,-40} {8,-17} {9,-17} {10, -15} {11,-8} {12,-10} {13,-16}";
            string[] headers = new string[] { "Job ID", "SkipConsume", "ConsumedBy", "State", "Priority", "Changelist", "Usernames", "Triggered At", "Created At", "Processed At", "Processing Host", "Duration", "TriggerId", "Job Results" };
            string header = String.Format(jobFormat, headers);

            // Extract any search filters found (strings enclosed in quotes)... removing these from the arguments list.            
            IEnumerable<string> searchStrings = PluckOutArgumentSearchStrings(ref arguments);

            // DW - see if arguments match the optional parameters to Monitor
            // If they do then we need not consider these arguments as jobids
            CapabilityType role = CapabilityType.None;
            int start = 0;
            int count = int.MaxValue;
            int monitorArgs = 0;

            if (arguments.Length >= 1 && CapabilityType.TryParse(arguments[0], true, out role))
            {
                IEnumerable<String> enumStrings = Enum.GetNames(typeof(CapabilityType)).Select(s => s.ToLower());

                if (enumStrings.Contains(arguments[0].ToLower()))
                {
                    monitorArgs++;
                }
            }

            if (arguments.Length >= 2 && int.TryParse(arguments[1], out start))
            {
                monitorArgs++;
            }

            if (arguments.Length >= 3 && int.TryParse(arguments[2], out count))
            {
                monitorArgs++;
            }

            IEnumerable<TaskStatus> status = AdminConsumer.Monitor();
            IDictionary<String, CodeBuilder2Job> codebuilderJobTargets = new Dictionary<String, CodeBuilder2Job>();
            ISet<String> codebuilderJobCLs = new HashSet<String>();
            IDictionary<String, ScriptBuilderJob> scriptbuilderJobTargets = new Dictionary<String, ScriptBuilderJob>();
            ISet<String> scriptbuilderJobCLs = new HashSet<String>();

            // Changelist, buildname, state
            IDictionary<String, IDictionary<String,String>> codebuilderJobState = new Dictionary<String, IDictionary<String,String>>();
            IDictionary<String, IDictionary<String, String>> scriptbuilderJobState = new Dictionary<String, IDictionary<String, String>>();
            if (0 == arguments.Length || monitorArgs > 0)
            {
                foreach (TaskStatus taskStatus in status)
                {
                    System.Console.WriteLine("Task: {0}", taskStatus.Name);
                    PrettyPrintTable(header, taskStatus.Jobs, delegate(IJob job)
                    {
                        IEnumerable<uint> changelists = job.Trigger.ChangelistNumbers();
                        IEnumerable<String> usernames = job.Trigger.Usernames();
                        IEnumerable<DateTime> triggeredAtDateTimes = job.Trigger.TriggeredAt();

                        String changelist = PrettyChangelists(changelists);
                        String triggeredAt = PrettyTriggeredAt(triggeredAtDateTimes);
                        String username = PrettyUserNames(usernames);

                        object[] jobFieldsDisplay = JobFields(taskStatus, job, changelist, username, triggeredAt);

                        if (SearchFields(searchStrings, jobFieldsDisplay))
                        {
                            if (displayAsMatrix)
                            {
                                // Record the codebuilder jobs for display later in a form of a matrix
                                if (job is CodeBuilder2Job)
                                {
                                    DisplayCodeBuilderJobInMatrix(matrixField, codebuilderJobTargets, codebuilderJobCLs, codebuilderJobState, job, changelist);
                                }
                                else if (job is ScriptBuilderJob)
                                {
                                    DisplayScriptBuilderJobInMatrix(matrixField, scriptbuilderJobTargets, scriptbuilderJobCLs, scriptbuilderJobState, job, changelist);
                                }
                                else
                                {
                                    // Expand as you require for other jobs - since they require some form of header you'll need to handle that
                                    // - this might be something we can abstract - just haven't got time now.
                                }
                            }
                            else
                            {
                                System.Console.WriteLine(jobFormat, jobFieldsDisplay);
                            }
                        }
                    });
                    System.Console.WriteLine("\n\n");
                }

                if (displayAsMatrix)
                {
                    DisplayCodebuilderJobMatrix(codebuilderJobTargets, codebuilderJobCLs, codebuilderJobState);
                }
            }
            else
            {
                // Loop through all Job IDs.             
                foreach (String argument in arguments)
                {
                    Guid jobId = Guid.Empty;
                    if (!Guid.TryParse(argument, out jobId))
                    {
                        log.Error("Invalid Job ID: {0}.  Ignoring.", argument);
                        continue;
                    }

#warning DHM FIX ME: could filter this better.
                    foreach (TaskStatus taskStatus in status)
                    {
                        IJob job = taskStatus.Jobs.Where(j => j.ID.Equals(jobId)).FirstOrDefault();
                        if (job == null)
                            continue;

                        System.Console.WriteLine("Task: {0}", taskStatus.Name);

                        IEnumerable<uint> changelists = job.Trigger.ChangelistNumbers();
                        IEnumerable<String> usernames = job.Trigger.Usernames();
                        IEnumerable<DateTime> submittedAtDateTimes = job.Trigger.TriggeredAt();
                        IEnumerable<String> files = job.Trigger.TriggeringFiles();

                        String changelist = PrettyChangelists(changelists);
                        String triggeredAt = PrettyTriggeredAt(submittedAtDateTimes);
                        String username = PrettyUserNames(usernames);

                        System.Console.WriteLine(header);

                        object[] jobFieldsDisplay = JobFields(taskStatus, job, changelist, username, triggeredAt);

                        if (SearchFields(searchStrings, jobFieldsDisplay))
                        {
                            System.Console.WriteLine(jobFormat, jobFieldsDisplay);
                        }

                        System.Console.WriteLine("Files:");
                        foreach (String filename in files)
                            System.Console.WriteLine("\t{0}", filename);
                        break;
                    }
                }
            }
        }


        private static void DisplayScriptBuilderJobInMatrix(string matrixField, IDictionary<String, ScriptBuilderJob> scriptbuilderJobTargets, ISet<String> scriptbuilderJobCLs, IDictionary<String, IDictionary<String, String>> scriptbuilderJobState, IJob job, String changelist)
        {
            ScriptBuilderJob sbJob = (ScriptBuilderJob)job;
            string buildName = String.Format("{0}|{1}", Path.GetFileNameWithoutExtension(sbJob.ScriptProjectFilename), sbJob.BuildConfig);
            if (!scriptbuilderJobTargets.ContainsKey(buildName))
                scriptbuilderJobTargets.Add(buildName, sbJob);
            scriptbuilderJobCLs.Add(changelist);

            if (!scriptbuilderJobState.ContainsKey(changelist))
                scriptbuilderJobState.Add(changelist, new Dictionary<String, String>());

            // Display various fields in the matrix
            if (0 == String.Compare(matrixField, "guid", true))
            {
                if (sbJob.State == JobState.SkippedConsumed)
                {
                    scriptbuilderJobState[changelist].Add(buildName, String.Format("{0}=>{1}", sbJob.ID.ToString().Substring(0, 8), sbJob.ConsumedByJobID.ToString().Substring(0, 8)));
                }
                else
                {
                    scriptbuilderJobState[changelist].Add(buildName, sbJob.ID.ToString().Substring(0, 8));
                }
            }
            else if (0 == String.Compare(matrixField, "time", true))
                scriptbuilderJobState[changelist].Add(buildName, sbJob.ProcessingTime.ToString(@"hh\:mm\:ss"));
            else
            {
                String state = sbJob.State.ToString();
                if (sbJob.State == JobState.Assigned)
                {
                    foreach (WorkerStatus ws in AdminConsumer.Clients().Where(ws => ws.AssignedJob == sbJob.ID))
                    {
                        state = String.Format("* {0} *", ws.ServiceConnection.Host);
                        break;
                    }
                }

                if (scriptbuilderJobState[changelist].ContainsKey(buildName))
                {
                    log.Error("{0} is being built twice, it can't display in the jobsmatrix correctly.", buildName);
                }
                else
                {
                    scriptbuilderJobState[changelist].Add(buildName, state);
                }
            }
        }

        private static void DisplayCodeBuilderJobInMatrix(string matrixField, IDictionary<String, CodeBuilder2Job> codebuilderJobTargets, ISet<String> codebuilderJobCLs, IDictionary<String, IDictionary<String, String>> codebuilderJobState, IJob job, String changelist)
        {
            CodeBuilder2Job cb2Job = (CodeBuilder2Job)job;
            string buildName = String.Format("{0}|{1}|{2}", Path.GetFileNameWithoutExtension(cb2Job.SolutionFilename), cb2Job.BuildConfig, cb2Job.Platform);
            if (!codebuilderJobTargets.ContainsKey(buildName))
                codebuilderJobTargets.Add(buildName, cb2Job);
            codebuilderJobCLs.Add(changelist);

            if (!codebuilderJobState.ContainsKey(changelist))
                codebuilderJobState.Add(changelist, new Dictionary<String, String>());

            // Display various fields in the matrix
            if (0 == String.Compare(matrixField, "guid", true))
            {
                if (cb2Job.State == JobState.SkippedConsumed)
                {
                    codebuilderJobState[changelist].Add(buildName, String.Format("{0}=>{1}", cb2Job.ID.ToString().Substring(0, 8), cb2Job.ConsumedByJobID.ToString().Substring(0, 8)));
                }
                else
                {
                    codebuilderJobState[changelist].Add(buildName, cb2Job.ID.ToString().Substring(0, 8));
                }
            }
            else if (0 == String.Compare(matrixField, "time", true))
                codebuilderJobState[changelist].Add(buildName, cb2Job.ProcessingTime.ToString(@"hh\:mm\:ss"));
            else
            {
                String state = cb2Job.State.ToString();
                if (cb2Job.State == JobState.Assigned)
                {
                    foreach (WorkerStatus ws in AdminConsumer.Clients().Where(ws => ws.AssignedJob == cb2Job.ID))
                    {
                        state = String.Format("* {0} *", ws.ServiceConnection.Host);
                        break;
                    }
                }

                if (codebuilderJobState[changelist].ContainsKey(buildName))
                {
                    log.Error("{0} is being built twice, it can't display in the jobsmatrix correctly.", buildName);
                }
                else
                {
                    codebuilderJobState[changelist].Add(buildName, state);
                }
            }
        }

        private static string PrettyTriggeredAt(IEnumerable<DateTime> triggeredAtDateTimes)
        {
            String triggeredAt;
            if (triggeredAtDateTimes.Any())
            {
                triggeredAt = triggeredAtDateTimes.HasAtLeast(2) ? String.Format("{0}->{1}", PrettyPrint(triggeredAtDateTimes.First()), PrettyPrint(triggeredAtDateTimes.Last())) : PrettyPrint(triggeredAtDateTimes.Last());
            }
            else
            {
                triggeredAt = PrettyPrint(DateTime.MinValue);
            }
            return triggeredAt;
        }

        private static string PrettyUserNames(IEnumerable<String> usernames)
        {
            String username;
            if (usernames.Any())
            {
                username = usernames.HasAtLeast(2) ? String.Format("({0})Last={1}", usernames.Count(), usernames.Last()) : usernames.Last();
            }
            else
            {
                username = String.Empty;
            }
            return username;
        }

        private static string PrettyChangelists(IEnumerable<uint> changelists)
        {
            String changelist;
            if (changelists.HasAtLeast(2))
            {
                changelist = String.Format("{0}-({2})->{1}", changelists.Min(), changelists.Max(), changelists.Count());
            }
            else if (changelists.Any()) // has one
            {
                changelist = changelists.First().ToString();
            }
            else
            {
                changelist = String.Empty;
            }
            return changelist;
        }

        /// <summary>
        /// Displays the matrix of codebuilder jobs
        /// </summary>
        /// <param name="codebuilderJobTargets"></param>
        /// <param name="codebuilderJobCLs"></param>
        /// <param name="codebuilderJobState"></param>
        private static void DisplayCodebuilderJobMatrix(IDictionary<String, CodeBuilder2Job> codebuilderJobTargets, ISet<String> codebuilderJobCLs, IDictionary<String, IDictionary<String, String>> codebuilderJobState)
        {
            int minColWidth = 20;

            // Write header 
            StringBuilder slnHdr =     new StringBuilder("|                        |");
            StringBuilder toolHdr =    new StringBuilder("|                        |");
            StringBuilder rebuildHdr = new StringBuilder("| Changelists            |");
            StringBuilder cfgHdr =     new StringBuilder("|                        |");
            StringBuilder border =     new StringBuilder("+------------------------+");
            foreach (KeyValuePair<String, CodeBuilder2Job> target in codebuilderJobTargets)
            {
                String sln = Path.GetFileName(target.Value.SolutionFilename);
                String tool = target.Value.Tool;
                String rebuild = target.Value.Rebuild ? "rebuild" : "incremental";
                String cfg = String.Format("{0}|{1}", target.Value.BuildConfig, target.Value.Platform);
                int len = Math.Max(Math.Max(sln.Length, cfg.Length), minColWidth);
                slnHdr.AppendFormat("{0} |", sln.PadBoth(len));
                toolHdr.AppendFormat("{0} |", tool.PadBoth(len));
                rebuildHdr.AppendFormat("{0} |", rebuild.PadBoth(len));
                cfgHdr.AppendFormat("{0} |", cfg.PadBoth(len));
                border.Insert(border.Length, "-", len + 1);
                border.Append("+");
            }
            System.Console.WriteLine(border);
            System.Console.WriteLine(slnHdr);
            System.Console.WriteLine(cfgHdr);
            System.Console.WriteLine(border);
            System.Console.WriteLine(toolHdr);
            System.Console.WriteLine(rebuildHdr);
            System.Console.WriteLine(border);

            // Write matrix
            foreach (String clNum in codebuilderJobCLs.Reverse())
            {
                String line = String.Format("| {0} |", clNum.PadLeft(22));
                foreach (KeyValuePair<String, CodeBuilder2Job> target in codebuilderJobTargets)
                {
                    CodeBuilder2Job job = target.Value;
                    String sln = Path.GetFileName(job.SolutionFilename);
                    String cfg = String.Format("{0}|{1}", job.BuildConfig, job.Platform);
                    String slnAndCfg = String.Format("{0}|{1}", Path.GetFileNameWithoutExtension(job.SolutionFilename), cfg);
                    int len = Math.Max(Math.Max(sln.Length, cfg.Length), minColWidth);
                    foreach (KeyValuePair<String, IDictionary<String, String>> kvp in codebuilderJobState.Where(kvp => kvp.Key == clNum))
                    {
                        IDictionary<String, String> foundCfgDict = kvp.Value;
                        String state = foundCfgDict.ContainsKey(slnAndCfg) ? foundCfgDict[slnAndCfg] : "unknown";
                        if (state.Equals("SkippedConsumed", StringComparison.CurrentCultureIgnoreCase))
                        {
                            state = String.Format("SkipConsum");
                        }
                        line += String.Format("{0}|", state.PadBoth(len + 1));
                    }
                }
                System.Console.WriteLine(line);
            }

            System.Console.WriteLine(border);
        }

        /// <summary>
        /// Get the jobs fields as an object array
        /// </summary>
        /// <param name="taskStatus"></param>
        /// <param name="job"></param>
        /// <param name="changelist"></param>
        /// <param name="username"></param>
        /// <param name="submittedAt"></param>
        /// <returns></returns>
        private static object[] JobFields(TaskStatus taskStatus, IJob job, String changelist, String username, String triggeredAt)
        {
            String allJobResults = JoinJobResults(taskStatus, job);

            String prettyProcessingTime = PrettyPrint(job.ProcessingTime);
            String prettyProcessedAt = PrettyPrint(job.ProcessedAt.ToLocalTime());
            String prettyCreatedAt = PrettyPrint(job.CreatedAt.ToLocalTime());
            String prettyUsername = username.Substring(0, Math.Min(username.Length, 16));
            String triggerId = job.Trigger.TriggerId.ToString().Take(8).ToSystemString();

            String skipConsume = job is CodeBuilder2Job ? ((CodeBuilder2Job)job).SkipConsume.ToString() : "N/A";
            object[] jobFieldsDisplay = { job.ID, skipConsume, job.ConsumedByJobID.ToString().Take(8).ToSystemString(), job.State, job.Priority, changelist, prettyUsername, triggeredAt, prettyCreatedAt, prettyProcessedAt, job.ProcessingHost, prettyProcessingTime, triggerId, allJobResults };
            return jobFieldsDisplay;
        }

        /// <summary>
        /// Helper for Datetime
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        private static String PrettyPrint(DateTime dateTime)
        {
            if (dateTime != DateTime.MinValue && dateTime != DateTime.MaxValue)
            {
                String monthDayPattern = CultureInfo.CurrentCulture.DateTimeFormat.MonthDayPattern;

                String pretty;
                if (monthDayPattern.IndexOf('M') < monthDayPattern.IndexOf('d'))
                {
                    pretty = String.Format("{0:MM/dd/yy HH:mm:ss}", dateTime);
                }
                else
                {
                    pretty = String.Format("{0:dd/MM/yy HH:mm:ss}", dateTime);
                }

                return pretty;
            }
            return String.Empty;
        }

        /// <summary>
        /// Helper for Timespan
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        private static String PrettyPrint(TimeSpan timeSpan)
        {
            String pretty = String.Format("{0:hh\\:mm\\:ss}", timeSpan);
            return pretty;
        }

        /// <summary>
        /// Concatenate all job results 
        /// - there may be ( unlikely ) more than one.
        /// </summary>
        /// <param name="taskStatus"></param>
        /// <param name="job"></param>
        /// <returns></returns>
        private static String JoinJobResults(TaskStatus taskStatus, IJob job)
        {
            StringBuilder jobResultString = new StringBuilder();
            IJobResult[] jobResults = taskStatus.JobResults.Where(jobResult => jobResult.JobId == job.ID).ToArray();
            foreach (IJobResult jobResult in jobResults)
            {
                jobResultString.Append(jobResult.ToString());
                jobResultString.Append(" ");
            }
            String allJobResults = jobResultString.ToString();
            return allJobResults;
        }

        /// Pluck out all search strings ( remove & recreate new lists ) on passed string array of arguments.
        /// - a search string is enclosed in double quotes "".
        /// - the search strings are 'removed' from the arguments for easy future parsing. 
        /// </summary>
        /// <param name="arguments">the arguments to search in</param>
        /// <returns>search strings found</returns>
        private static IEnumerable<String> PluckOutArgumentSearchStrings(ref String[] arguments)
        {
            ICollection<String> remainingArguments = new List<String>();
            ICollection<String> searchStrings = new List<String>();

            int idx = 0;

            foreach (String arg in arguments)
            {
                Guid guid;
                int integer;

                IEnumerable<String> enumStrings = Enum.GetNames(typeof(CapabilityType)).Select(s => s.ToLower());

                // If you wish to search for a number ( changelist ) it must be the first argument.
                // Here we recognise arguments that are NOT search arguments.
                if (enumStrings.Contains(arguments[0].ToLower()) || 
                    Guid.TryParse(arg, out guid) || 
                    (idx>0 && int.TryParse(arg, out integer)))
                {
                    remainingArguments.Add(arg);
                }
                else
                {
                    // otherwise pluck it out of the argument list.
                    searchStrings.Add(arg);
                }

                idx++;
            }

            arguments = remainingArguments.ToArray<String>();

            return searchStrings;
        }

        /// <summary>
        /// Search strings not CONTAINED in the jobFields, won't be displayed, so return false
        /// </summary>
        /// <param name="searchStrings">the strings that need to be found</param>
        /// <param name="fieldsDisplay">the job fields to search in - they are converted ToString()</param>
        /// <returns>false if any search string was not contained in any fields.</returns>
        private static bool SearchFields(IEnumerable<String> searchStrings, object[] fieldsDisplay)
        {
            foreach (String s in searchStrings)
            {
                // case insensitive compare - for all the fields are there any that do NOT have the case insenitive search string?
                if (!fieldsDisplay.Any(x => x.ToString().IndexOf(s, StringComparison.OrdinalIgnoreCase) >= 0))
                {
                    return false; // NOT found in any of the job fields
                }
            }
            return true; // all the search strings were found in the job fields
        }

        /// <summary>
        /// clients, client [id] command handler.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        private static void CommandClientsHandler(CommandOptions options,
            String command, String[] arguments)
        {
            String clientsFormat = "{0,-36} {1,-10} {2,-50} {3,-36} {4}";
            String[] headers = new String[] { "Client ID", "State", "Host", "Assigned Job", "Capability" };
            String header = String.Format(clientsFormat, headers);

            // Extract any search filters found (strings enclosed in quotes)... removing these from the arguments list.            
            IEnumerable<String> searchStrings = PluckOutArgumentSearchStrings(ref arguments);

            if (0 == arguments.Length)
            {
                // Get clients sorted by host.
                IEnumerable<WorkerStatus> clients = AdminConsumer.Clients().OrderBy(ws => ws.ServiceConnection.Host);

                PrettyPrintTable(header, clients, delegate(WorkerStatus client)
                {
                    object[] clientsFieldsDisplay = { client.ID, client.State, client.ServiceConnection.Host, client.AssignedJob, client.Capability.Type };

                    if (SearchFields(searchStrings, clientsFieldsDisplay))
                    {
                        System.Console.WriteLine(clientsFormat, clientsFieldsDisplay);
                    }
                });
            }
            else
            {
                // Loop through all Client IDs.
                IEnumerable<WorkerStatus> clients = AdminConsumer.Clients();
                foreach (String argument in arguments)
                {
                    Guid clientId = Guid.Empty;
                    if (!Guid.TryParse(argument, out clientId))
                    {
                        log.Error("Invalid Client ID: {0}.  Ignoring.", argument);
                        continue;
                    }

                    foreach (WorkerStatus client in clients)
                    {
                        if (!clientId.Equals(client.ID))
                            continue;

                        System.Console.WriteLine(header);
                        object[] clientsFieldsDisplay = { client.ID, client.State, client.ServiceConnection.Host, client.AssignedJob, client.Capability.Type };

                        if (SearchFields(searchStrings, clientsFieldsDisplay))
                        {
                            System.Console.WriteLine(clientsFormat, clientsFieldsDisplay);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Shows integrations running and their status
        /// </summary>
        /// <param name="options"></param>
        /// <param name="command"></param>
        private static void CommandIntegrations(CommandOptions options, String[] arguments)
        {
            IEnumerable<TaskStatus> status = AdminConsumer.Monitor();

            log.Message("Integrations");
            foreach (IntegratorTaskStatus integratorTaskStatus in status.Where(t => t is IntegratorTaskStatus))
            {
                foreach (Integration integration in integratorTaskStatus.Integrations)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(String.Format("\t{0}", integration));
                    bool blocked = false;

                    foreach (IntegratorJobResult integratorJobResult in integratorTaskStatus.JobResults.Cast<IntegratorJobResult>().Where(jr => jr.PendingIntegrationChangelistNumber >= 0))
                    {
                        foreach (IntegratorJob searchedJob in integratorTaskStatus.Jobs.Where(j => j.ID == integratorJobResult.JobId))
                        {
                            if (0 == String.Compare(integration.BranchSpec, searchedJob.Integration.BranchSpec))
                            {
                                sb.Append(String.Format(" : Blocked by {0}\n", integratorJobResult.PendingIntegrationChangelistNumber));
                                blocked = true;
                            }
                        }
                    }

                    log.Message(sb.ToString());

                    // Now display all the jobs associated with each integration
                    foreach (IntegratorJob integratorJob in integratorTaskStatus.Jobs.Cast<IntegratorJob>())
                    {
                        if (0 == String.Compare(integration.BranchSpec, integratorJob.Integration.BranchSpec))
                        {
                            IEnumerable<uint> changelists = integratorJob.Trigger.ChangelistNumbers();
                            IEnumerable<String> usernames = integratorJob.Trigger.Usernames();

                            String changelist = changelists.Any() ? String.Join(",", changelists) : String.Empty;
                            String username = usernames.Any() ? usernames.Last() : String.Empty;

                            bool foundResult = false;
                            foreach (IntegratorJobResult jobResult in integratorTaskStatus.JobResults.Where(jr => jr.JobId == integratorJob.ID))
                            {
                                if (jobResult.PendingIntegrationChangelistNumber >= 0)
                                    log.Warning(String.Format("\t\t{0}({1}) => {2}", changelist, username, jobResult));
                                else
                                    log.Message(String.Format("\t\t{0}({1}) => {2}", changelist, username, jobResult));
                                foundResult = true;
                            }

                            if (!foundResult)
                            {
                                log.Message(String.Format("\t\t{0}({1}) => {2}", changelist, username, blocked ? "Blocked" : "Queued"));
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// unregisters a worker
        /// </summary>
        private static void CommandUnregisterWorker(CommandOptions options, String[] arguments)
        {
            if (0 == arguments.Length)
            {
                log.Error("No arguments specified for Unregister Worker; specify worker ID.");
                return;
            }

            String workerIDText = arguments[0];
            Guid workerID = Guid.Empty;

            if (!Guid.TryParse(workerIDText, out workerID))
            {
                log.Error("Unable to parse worker ID from argument '{0}'", workerIDText);
                return;
            }

            AutomationConsumer.UnregisterWorker(workerID);
        }

        /// <summary>
        /// set task param
        /// </summary>
        private static void CommandSetTaskParam(CommandOptions options, String[] arguments)
        {
            if (3 != arguments.Length)
            {
                log.Error("Insufficient arguments specified for CommandSetTaskParam; specify task, parameter and new value.");
                return;
            }

            String taskName = arguments[0];
            String paramName = arguments[1];
            String paramValue = arguments[2];

            log.Message("Setting task param {0} {1} {2}", taskName, paramName, paramValue);

            AdminConsumer.SetTaskParam(taskName, paramName, paramValue);
        }

        /// <summary>
        /// Start task
        /// </summary>
        private static void CommandStartTask(CommandOptions options, String[] arguments)
        {
            if (0 == arguments.Length)
            {
                log.Message("Starting ALL tasks");
                AdminConsumer.StartTask(null);
            }

            foreach (String taskName in arguments)
            {
                log.Message("Starting task {0}", taskName);
                AdminConsumer.StartTask(taskName);
            }
        }

        /// <summary>
        /// Stop task
        /// </summary>
        private static void CommandStopTask(CommandOptions options, String[] arguments)
        {
            if (0 == arguments.Length)
            {
                log.Message("Stopping ALL tasks");
                AdminConsumer.StopTask(null);
            }

            foreach (String taskName in arguments)
            {
                log.Message("Stopping task {0}", taskName);
                AdminConsumer.StopTask(taskName);
            }
        }        

        /// <summary>
        /// get all task params
        /// </summary>
        private static void CommandGetTaskParams(CommandOptions options, String[] arguments)
        {
            if (1 != arguments.Length)
            {
                log.Error("Insufficient arguments specified for CommandGetTaskParams; specify task name.");
                return;
            }

            String taskName = arguments[0];

            IDictionary<String, Object> taskParams = AdminConsumer.GetTaskParams(taskName);

            foreach (KeyValuePair<String, Object> kvp in taskParams)
            {
                log.Message("Task param : {0} {1}", kvp.Key, kvp.Value);
            }
        }

        private class HourlyStat
        {
            internal HourlyStat(DateTime hourBeginning, int jobsIn, int jobsOut, int jobsBalance)
            {
                HourBeginning = hourBeginning;
                JobsIn = jobsIn;
                JobsOut = jobsOut;
                JobsBalance = jobsBalance;
            }

            internal DateTime HourBeginning { get; private set; }
            internal int JobsIn { get; private set; }
            internal int JobsOut { get; private set; }
            internal int JobsBalance { get; private set; } 
        }

        private static void CommandStatsHandler(CommandOptions options, String command, String[] arguments)
        {
            IEnumerable<TaskStatus> taskStatuses = AdminConsumer.Monitor();

            log.Message("Stats Report");
            log.Message("");
            foreach (TaskStatus taskStatus in taskStatuses)
                CreateStatsReport(options.Config, taskStatus);
            log.Message("");
        }

        private static void CreateStatsReport(IConfig config, TaskStatus taskStatus)
        {
            IJob[] allJobs = taskStatus.Jobs.Where(job => job.State != JobState.Aborted && job.State != JobState.Skipped && job.State != JobState.SkippedConsumed).ToArray();
            IJob[] completedJobs = taskStatus.Jobs.Where(job => job.State == JobState.Completed).ToArray();

            if (completedJobs.Length == 0)
            {
                log.Warning("There are no completed jobs from which to generate stats");
                return;
            }

            DateTime firstJob = allJobs.OrderBy(job => job.CreatedAt).Select(job => job.CreatedAt).FirstOrDefault();
            int jobBalance = 0;
            List<HourlyStat> hourlyStats = new List<HourlyStat>();
            DateTime startTime = new DateTime(firstJob.Year, firstJob.Month, firstJob.Day, firstJob.Hour, 0, 0);
            while (startTime < DateTime.UtcNow)
            {
                DateTime endTime = startTime.AddHours(1);

                IJob[] jobsCreatedInThisPeriod = allJobs.Where(job => job.CreatedAt >= startTime && job.CreatedAt < endTime).ToArray();
                IJob[] jobsCompletedInThisPeriod = allJobs.Where(job => job.CompletedAt >= startTime && job.CompletedAt < endTime).ToArray();
                jobBalance = jobBalance + jobsCreatedInThisPeriod.Length - jobsCompletedInThisPeriod.Length;
                hourlyStats.Add(
                    new HourlyStat(
                        startTime,
                        jobsCreatedInThisPeriod.Length,
                        jobsCompletedInThisPeriod.Length,
                        Math.Max(jobBalance, 0)));

                startTime = startTime.AddHours(1);
            }

            // build a dictionary of completed jobs time spans per directory (ignore mixed files lists)
            Dictionary<String, List<IJob>> directoryJobMap = new Dictionary<String, List<IJob>>();

            // build a dictionary of completed job time spans per dispatch group
            Dictionary<String, String> directoryDispatchGroupMap = LoadDirectoryDispatchGroupMap(config);
            Dictionary<String, List<IJob>> dispatchGroupJobMap = new Dictionary<String, List<IJob>>();

            foreach (IJob completedJob in completedJobs)
            {
                SortedSet<String> jobFileDirectories = new SortedSet<String>();
                IEnumerable<String> files = completedJob.Trigger.TriggeringFiles();
                if (files.Any())
                {
                    foreach (String directoryName in files.Select(pathname => Path.GetDirectoryName(pathname).ToLower()))
                    {
                        jobFileDirectories.Add(directoryName);
                    }
                }

                if (jobFileDirectories.Count == 1)
                {
                    String directoryName = jobFileDirectories.First();
                    if (!directoryJobMap.ContainsKey(directoryName))
                    {
                        directoryJobMap.Add(jobFileDirectories.First(), new List<IJob>());
                    }
                    directoryJobMap[jobFileDirectories.First()].Add(completedJob);

                    directoryName = Path.GetFullPath(directoryName);
                    if (directoryDispatchGroupMap.ContainsKey(directoryName))
                    {
                        String dispatchGroupName = directoryDispatchGroupMap[directoryName];
                        if (!dispatchGroupJobMap.ContainsKey(dispatchGroupName))
                            dispatchGroupJobMap.Add(dispatchGroupName, new List<IJob>());
                        dispatchGroupJobMap[dispatchGroupName].Add(completedJob);
                    }
                }
            }

            // Print overview stats to the screen
            log.Message("{0} stats", taskStatus.Name);
            log.Message("+--------------------------------------------------------------+--------+----------+----------+");
            log.Message("| Directory                                                    | # Jobs | AVG time | MAX time |");
            log.Message("+--------------------------------------------------------------+--------+----------+----------+");
            foreach (KeyValuePair<String, List<IJob>> directoryJobPair in directoryJobMap.OrderBy(kvp => kvp.Key))
            {
                TimeSpan[] jobProcessingTimes = directoryJobPair.Value.Select(job => job.ProcessingTime).ToArray();
                TimeSpan averageTimeSpan = TimeSpan.FromSeconds(jobProcessingTimes.Sum(ts => ts.TotalSeconds) / (double)directoryJobPair.Value.Count());
                log.Message("| {0,-60} | {1,6} | {2,8} | {3,8} |",
                    directoryJobPair.Key, 
                    directoryJobPair.Value.Count(), 
                    averageTimeSpan.ToString(@"hh\:mm\:ss"),
                    jobProcessingTimes.Max().ToString(@"hh\:mm\:ss"));
            }
            log.Message("+--------------------------------------------------------------+--------+----------+----------+");

            // Create a csv report
            String myDocumentsDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            String reportPathname = Path.Combine(myDocumentsDirectory, taskStatus.Name + " Stats.csv");
            using (StreamWriter writer = File.CreateText(reportPathname))
            {
                writer.WriteLine("Hourly Performance Stats");
                writer.WriteLine();
                writer.Write("Hours Elapsed,");
                foreach (HourlyStat hourlyStat in hourlyStats)
                    writer.Write("{0},", hourlyStat.HourBeginning.ToString());
                writer.Write("\nJobs In,");
                foreach (HourlyStat hourlyStat in hourlyStats)
                    writer.Write("{0},", hourlyStat.JobsIn);
                writer.Write("\nJobs Out,");
                foreach (HourlyStat hourlyStat in hourlyStats)
                    writer.Write("{0},", hourlyStat.JobsOut);
                writer.Write("\nJobs Balance,");
                foreach (HourlyStat hourlyStat in hourlyStats)
                    writer.Write("{0},", hourlyStat.JobsBalance);

                writer.WriteLine();
                writer.WriteLine();
                writer.WriteLine("Directory Performance Stats");
                writer.WriteLine();

                foreach (KeyValuePair<String, List<IJob>> directoryJobPair in directoryJobMap.OrderBy(kvp => kvp.Key))
                {
                    writer.WriteLine(directoryJobPair.Key);
                    StringBuilder processedAtRowBuilder = new StringBuilder("Processed At");
                    StringBuilder processingTimeRowBuilder = new StringBuilder("Processed Time");
                    foreach (IJob job in directoryJobPair.Value)
                    {
                        processedAtRowBuilder.AppendFormat(",{0}", job.ProcessedAt);
                        processingTimeRowBuilder.AppendFormat(",{0}", job.ProcessingTime);
                    }
                    writer.WriteLine(processedAtRowBuilder.ToString());
                    writer.WriteLine(processingTimeRowBuilder.ToString());
                    writer.WriteLine();
                }

                foreach (KeyValuePair<String, List<IJob>> dispatchGroupJobPair in dispatchGroupJobMap.OrderBy(kvp => kvp.Key))
                {
                    writer.WriteLine(dispatchGroupJobPair.Key);
                    StringBuilder processedAtRowBuilder = new StringBuilder("Processed At");
                    StringBuilder processingTimeRowBuilder = new StringBuilder("Processed Time");
                    foreach (IJob job in dispatchGroupJobPair.Value)
                    {
                        processedAtRowBuilder.AppendFormat(",{0}", job.ProcessedAt);
                        processingTimeRowBuilder.AppendFormat(",{0}", job.ProcessingTime);
                    }
                    writer.WriteLine(processedAtRowBuilder.ToString());
                    writer.WriteLine(processingTimeRowBuilder.ToString());
                    writer.WriteLine();
                }
            }

            log.Message("Created detailed report at '{0}'", reportPathname);
        }

        private static Dictionary<String, String> LoadDirectoryDispatchGroupMap(IConfig config)
        {
            String xmlPathname = Path.Combine(config.ToolsConfig, "automation", "tasks", "jobdispatch", "AssetBuilderJobDispatch.xml");
            Dictionary<String, String> directoryDispatchGroupMap = new Dictionary<String, String>();

            if (File.Exists(xmlPathname))
            {
                try
                {
                    XDocument doc = XDocument.Load(xmlPathname);
                    foreach (XElement groupElement in doc.Root.Elements("group"))
                    {
                        String groupName = groupElement.Attribute("name").Value;
                        foreach (XElement itemElement in groupElement.Elements("item"))
                        {
                            String itemPath = itemElement.Attribute("value").Value;
                            String expandedDirectoryName = config.CoreProject.DefaultBranch.Environment.Subst(itemPath).ToLower();
                            directoryDispatchGroupMap.Add(Path.GetFullPath(expandedDirectoryName), groupName);
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.ToolExceptionCtx(LOG_CTX, ex, "Unable to load dispatch group data");
                }
            }
            

            return directoryDispatchGroupMap;
        }

        /// <summary>
        /// code_build [code_build.xml] command handler.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        /// <param name="rebuild"></param>
        private static void CommandCodeBuildHandler(CommandOptions options,
            String command, String[] arguments)
        {
            if (0 == arguments.Length)
            {
                log.Error("No arguments specified for Code Build; specify task xml file(s) or Changelist.");
                return;
            }

            // We may construct one or more jobs here; depending on the arguments.
            // Multiple files will go into a single job, multiple changelist or
            // other Job IDs will get separated into multiple jobs.
            ICollection<int> changelistIds = new List<int>();
            ICollection<String> filepaths = new List<String>();
            ICollection<CodeBuilderJob> jobs = new List<CodeBuilderJob>();

            // Parse arguments collection sets of changelist, files, existing Job IDs.
            foreach (String argument in arguments)
            {
                int changelistNumber = 0;
                if (int.TryParse(argument, out changelistNumber))
                {
                    changelistIds.Add(changelistNumber);
                }
                else
                {
                    log.WarningCtx(LOG_CTX, "'codebuild' of argument {0} was not processed.", argument);
                }
            }

            String description = String.Format("Code build command {0}", String.Join(" ", arguments));
            UserRequestTrigger trigger = new UserRequestTrigger(Environment.GetEnvironmentVariable("USERNAME"), filepaths, Guid.NewGuid(), DateTime.UtcNow, description); 
            foreach (String taskSettingsXML in filepaths)
            { 
                //process each task
                XDocument xmlDoc = XDocument.Load(taskSettingsXML);

                IEnumerable<XElement> xmlJobs = xmlDoc.XPathSelectElements("/Jobs/Job");
                foreach (XElement xmlJob in xmlJobs)
                {
                    CodeBuilderJob job = new CodeBuilderJob(xmlJob, CapabilityType.CodeBuilder, options.Branch);
                    job.Trigger = trigger;
                    jobs.Add(job);
                }
            }

            // Enqueue jobs.
            log.Message("{0} jobs created.", jobs.Count);
            foreach (IJob job in jobs)
            {
                log.Message("Enqueuing Job {0}", job.ID);
                AutomationConsumer.EnqueueJob(job);
            }
        }

        private static void CommandStatusHandler(CommandOptions options, String command, String[] arguments)
        {
            IEnumerable<TaskStatus> taskStatuses = AdminConsumer.Monitor();
            IEnumerable<WorkerStatus> workerStatuses = AdminConsumer.Clients();

            log.Message("Status Report");
            log.Message("");
            foreach (TaskStatus taskStatus in taskStatuses)
                ReportTaskStatus(taskStatus, workerStatuses);
            log.Message("");
        }

        /// <summary>
        /// shutdown command handler
        /// </summary>
        /// <param name="options"></param>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        private static void CommandShutdownHandler(CommandOptions options,
            String command, String[] arguments)
        {
            ShutdownType shutdownType = ShutdownType.None;
            string reason = null;
            foreach (String argument in arguments)
            {
                if (String.Equals("resume", argument, StringComparison.CurrentCultureIgnoreCase) ||
                            String.Equals("restart", argument, StringComparison.CurrentCultureIgnoreCase))
                {
                    shutdownType |= ShutdownType.Resume;
                }
                else if (String.Equals("synchead", argument, StringComparison.CurrentCultureIgnoreCase))
                {
                    shutdownType |= ShutdownType.ScmSyncHead;
                    log.Warning("You are syncing to HEAD of tools please check this is what you want to do");
                }
                else if (String.Equals("synclabel", argument, StringComparison.CurrentCultureIgnoreCase))
                {
                    shutdownType |= ShutdownType.ScmSyncLabel;
                    log.Warning("HEY! You are syncing to LABELLED tools please check this is what you want to do");
                }
                else if (String.Equals("syncconfig", argument, StringComparison.CurrentCultureIgnoreCase))
                {
                    shutdownType |= ShutdownType.ScmSyncConfig;
                    log.Warning("Syncing to tools configuration data ( requires restart )");
                }
                else if (String.Equals("synccontent", argument, StringComparison.CurrentCultureIgnoreCase))
                {
                    shutdownType |= ShutdownType.ScmSyncContent;
                    log.Warning("Syncing to tools content tree data ( requires restart )");
                }
                else if (String.Equals("cleancache", argument, StringComparison.CurrentCultureIgnoreCase))
                {
                    shutdownType |= ShutdownType.CleanCache;
                    log.Warning("Cleaning cache");
                }
                else if (reason == null)
                {
                    reason = String.Format("Reason for shutdown ({0}) : {1}\n", options.Config.Username, argument);
                }
                else
                {
                    log.Warning("Unsupported option passed to 'shutdown' : {0}, * no shutdown will take place *", argument);
                    return;
                }
            }

            if (reason == null)
            {
                log.Warning("No reason supplied for the shutdown, supply a string enclosed in quotes. * no shutdown will take place *");
                return;
            }

            shutdownType |= ShutdownType.WorkersAndHost;

            LogShutdownDirectives(shutdownType, reason);

            AdminConsumer.Shutdown(shutdownType, reason);
        }

        private static void CommandClearAllWorkersCacheHandler(CommandOptions options)
        {
            AdminConsumer.ClearAllWorkersCache();
        }

        private static void CommandSetClientTickRate(CommandOptions options, String[] arguments)
        {
            Int32 timeMs;
            if (Int32.TryParse(arguments[0], out timeMs))
            {
                AdminConsumer.SetClientUpdateTick(timeMs);
            }
            else
            {
                log.WarningCtx(LOG_CTX, "Invalid parameter value {0}, should be in format set_client_tick_rate <Int32>", arguments[0]);
            }
        }

        private static void LogShutdownDirectives(ShutdownType shutdownType, string reason)
        {
            log.Message("Shutdown directives...");
            log.Message("Shutdown reason={0}", reason);
            if (shutdownType.HasFlag(ShutdownType.WorkersAndHost))
            {
                log.Message(" - Shutting down WORKERS.");
                log.Message(" - Shutting down HOST.");
                log.Message(" - Shutdown will WAIT for jobs / tasks to finish.");
            }
            if (shutdownType.HasFlag(ShutdownType.ScmSyncHead))
            {
                log.Message(" - Shutdown will SYNC to *head* of tools after shutdown.");
            }
            if (shutdownType.HasFlag(ShutdownType.ScmSyncLabel))
            {
                log.Message(" - Shutdown will SYNC to *labelled* tools after shutdown.");
            }
            if (shutdownType.HasFlag(ShutdownType.Resume))
            {
                log.Message(" - Shutdown will RESUME processes afterwards.");
            }
            log.Message(" - Use 'clients' command to monitor shutdown.");
        }

        private static void ReportTaskStatus(TaskStatus taskStatus, IEnumerable<WorkerStatus> workerStatuses)
        {
            // gather data required for status report
            IJob[] completedJobs = taskStatus.Jobs.Where(job => job.State == JobState.Completed).ToArray();
            IJob[] pendingJobs = taskStatus.Jobs.Where(job => job.State == JobState.Pending).ToArray();

            TimeSpan[] completeJobProcessingTimes = completedJobs.Where(completedJob => completedJob.ProcessingTime.TotalMilliseconds > 0)
                                                                 .Select(completedJob => completedJob.ProcessingTime).ToArray();

            // display status information
            log.Message("Task '{0}' ({1})", taskStatus.Name, taskStatus.TaskState);

            try
            {
                if (completeJobProcessingTimes.Length == 0)
                    return;
                double averageProcessingTimeSeconds = completeJobProcessingTimes.Average(timeSpan => timeSpan.TotalSeconds);
                TimeSpan averageProcessingTime = TimeSpan.FromSeconds(averageProcessingTimeSeconds);

                // display processing time
                log.Message("{0} jobs were completed with an average processing time of {1}",
                    completeJobProcessingTimes.Length, averageProcessingTime.ToString());

                // try to estimate how long it'll take to clear the queue (if there is one)
                int numBusyWorkers = workerStatuses.Count(workerStatus => workerStatus.State == WorkerState.Busy);
                if (numBusyWorkers > 0)
                {
                    double estimatedTotalProcessingTimeRemainingSeconds = averageProcessingTimeSeconds * pendingJobs.Length;
                    double estimatedTotalProcessingTimeRemainingPerBusyWorkerSeconds = estimatedTotalProcessingTimeRemainingSeconds / (double)numBusyWorkers;
                    TimeSpan estimatedTotalProcessingTimeRemainingPerBusyWorker = TimeSpan.FromSeconds(estimatedTotalProcessingTimeRemainingPerBusyWorkerSeconds);

                    log.Message("{0} jobs are pending.  With {1} workers currently busy, these jobs should be clear in approximately {2}.",
                        pendingJobs.Length, numBusyWorkers, estimatedTotalProcessingTimeRemainingPerBusyWorker.ToString());
                }
                else
                {
                    if (pendingJobs.Length > 0)
                    {
                        log.Message("{0} jobs are pending.  However, all workers are currently idle so no end time can be estimated.", pendingJobs.Length);
                    }
                    else
                    {
                        log.Message("There are no pending jobs.  All clients are idle.");
                    }
                }

                // try to estimate the worker utilisation based on completed job information
                if (workerStatuses.Count() > 0)
                {
                    DateTime timeFirstJobWasProcessed = completedJobs.Where(completedJob => completedJob.ProcessingTime.TotalMilliseconds > 0)
                                                                     .Select(completedJob => completedJob.ProcessedAt)
                                                                     .OrderBy(processedAt => processedAt)
                                                                     .First();
                    TimeSpan timeSinceFirstJobWasProcessed = DateTime.UtcNow - timeFirstJobWasProcessed;
                    double timeSinceFirstJobWasProcessedSeconds = timeSinceFirstJobWasProcessed.TotalSeconds;
                    double sumProcessingTimeSeconds = completeJobProcessingTimes.Sum(timeSpan => timeSpan.TotalSeconds);
                    double serverLoadRatio = sumProcessingTimeSeconds / workerStatuses.Count() / timeSinceFirstJobWasProcessedSeconds;
                    log.Message("{0} of total processing was performed by {1} workers in the last {2}",
                        TimeSpan.FromSeconds(sumProcessingTimeSeconds),
                        workerStatuses.Count(),
                        TimeSpan.FromSeconds(timeSinceFirstJobWasProcessedSeconds));
                    log.Message("The worker utilisation is estimated to be {0}%", (serverLoadRatio * 100).ToString("0.00"));
                }
                else
                {
                    log.Message("There are no workers connected.  Unable to determine worker utilisation.");
                }
            }
            catch (Exception ex)
            {
                log.ToolException(ex, "Status is wonky");
            }
        }

        /// <summary>
        /// Upload file to File Transfer Service command.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        private static void CommandUploadHandler(CommandOptions options,
            String command, String[] arguments)
        {
            if (2 != arguments.Length)
            {
                log.ErrorCtx(LOG_CTX, "Invalid number of arguments ({0}); expecting 2: <GUID> <FILENAME>.",
                    arguments.Length);
                CommandHelpHandler(options, command, arguments);
                return;
            }

            String filename = arguments[1];
            if (!File.Exists(filename))
            {
                log.ErrorCtx(LOG_CTX, "File '{0}' does not exist.  Aborting.", filename);
                CommandHelpHandler(options, command, arguments);
                return;
            }

            Guid jobId = Guid.Empty;
            if (!Guid.TryParse(arguments[0], out jobId))
            {
                log.ErrorCtx(LOG_CTX, "Invalid job Guid; read: '{0}'.", arguments[0]);
                CommandHelpHandler(options, command, arguments);
                return;
            }

            IEnumerable<TaskStatus> monitor = AdminConsumer.Monitor();
            IEnumerable<IJob> jobs = monitor.SelectMany(task => task.Jobs.Where(j => j.ID == jobId));
            IJob job = jobs.FirstOrDefault();
            if (null == job)
            {
                log.ErrorCtx(LOG_CTX, "Job ID '{0}' does not exist on Automation Service.", jobId);
                CommandHelpHandler(options, command, arguments);
                return;
            }

            if (!FileTransferConsumer.UploadFile(job, filename))
                log.ErrorCtx(LOG_CTX, "File upload failed; '{0}' => '{1}'.", jobId, filename);
            else
                log.MessageCtx(LOG_CTX, "File upload finished; '{0}' => '{1}'.", jobId, filename);
        }

        /// <summary>
        /// Download file from File Transfer Service command.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        private static void CommandDownloadHandler(CommandOptions options,
            String command, String[] arguments)
        {
            if (2 != arguments.Length)
            {
                log.ErrorCtx(LOG_CTX, "Invalid number of arguments ({0}); expecting 2: <GUID> <FILENAME>.",
                    arguments.Length);
                CommandHelpHandler(options, command, arguments);
                return;
            }

            String filename = arguments[1];
            String destinationFilename = Path.GetFileName(filename);
            if (File.Exists(destinationFilename))
            {
                log.ErrorCtx(LOG_CTX, "Destination file '{0}' already exists!  Aborting.",
                    destinationFilename);
                CommandHelpHandler(options, command, arguments);
                return;
            }

            Guid jobId = Guid.Empty;
            if (!Guid.TryParse(arguments[0], out jobId))
            {
                log.ErrorCtx(LOG_CTX, "Invalid job Guid; read: '{0}'.", arguments[0]);
                CommandHelpHandler(options, command, arguments);
                return;
            }

            IEnumerable<TaskStatus> monitor = AdminConsumer.Monitor();
            IEnumerable<IJob> jobs = monitor.SelectMany(task => task.Jobs.Where(j => j.ID == jobId));
            IJob job = jobs.FirstOrDefault();
            if (null == job)
            {
                log.ErrorCtx(LOG_CTX, "Job ID '{0}' does not exist on Automation Service.", jobId);
                CommandHelpHandler(options, command, arguments);
                return;
            }

            Byte[] md5;
            if (!FileTransferConsumer.DownloadFile(job, filename, destinationFilename, out md5))
                log.ErrorCtx(LOG_CTX, "File download failed; '{0}': {1} => '{2}'.  MD5: {3}.",
                    jobId, filename, destinationFilename, RSG.Base.IO.FileMD5.Format(md5));
            else
                log.MessageCtx(LOG_CTX, "File download complete; '{0}': {1} => '{2}'.  MD5: {3}.",
                    jobId, filename, destinationFilename, RSG.Base.IO.FileMD5.Format(md5));
        }

        /// <summary>
        /// File availability from File Transfer Service command (Job ID).
        /// </summary>
        /// <param name="options"></param>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        private static void CommandFileAvailabilityHandler(CommandOptions options,
            String command, String[] arguments)
        {
            if (1 != arguments.Length)
            {
                log.ErrorCtx(LOG_CTX, "Invalid number of arguments ({0}); expecting 1: <GUID>.",
                    arguments.Length);
                CommandHelpHandler(options, command, arguments);
                return;
            }

            Guid jobId = Guid.Empty;
            if (!Guid.TryParse(arguments[0], out jobId))
            {
                log.ErrorCtx(LOG_CTX, "Invalid job Guid; read: '{0}'.", arguments[0]);
                CommandHelpHandler(options, command, arguments);
                return;
            }

            IEnumerable<TaskStatus> monitor = AdminConsumer.Monitor();
            IEnumerable<IJob> jobs = monitor.SelectMany(task => task.Jobs.Where(j => j.ID == jobId));
            IJob job = jobs.FirstOrDefault();
            if (null == job)
            {
                log.ErrorCtx(LOG_CTX, "Job ID '{0}' does not exist on Automation Service.", jobId);
                CommandHelpHandler(options, command, arguments);
                return;
            }

            IEnumerable<String> jobFiles = FileTransferConsumer.FileAvailability(job);
            log.MessageCtx(LOG_CTX, "File availability for Job '{0}':", jobId);
            foreach (String jobFile in jobFiles)
                log.MessageCtx(LOG_CTX, "\t{0}", jobFile);
        }        

        /// <summary>
        /// File availability from File Transfer Service command (Job ID).
        /// </summary>
        /// <param name="options"></param>
        /// <param name="command"></param>
        /// <param name="arguments"></param>
        private static void CommandFileAvailability2Handler(CommandOptions options,
            String command, String[] arguments)
        {
            if (1 != arguments.Length)
            {
                log.ErrorCtx(LOG_CTX, "Invalid number of arguments ({0}); expecting 1: <GUID>.",
                    arguments.Length);
                CommandHelpHandler(options, command, arguments);
                return;
            }

            Guid jobId = Guid.Empty;
            if (!Guid.TryParse(arguments[0], out jobId))
            {
                log.ErrorCtx(LOG_CTX, "Invalid job Guid; read: '{0}'.", arguments[0]);
                CommandHelpHandler(options, command, arguments);
                return;
            }

            IEnumerable<TaskStatus> monitor = AdminConsumer.Monitor();
            IEnumerable<IJob> jobs = monitor.SelectMany(task => task.Jobs.Where(j => j.ID == jobId));
            IJob job = jobs.FirstOrDefault();
            if (null == job)
            {
                log.ErrorCtx(LOG_CTX, "Job ID '{0}' does not exist on Automation Service.", jobId);
                CommandHelpHandler(options, command, arguments);
                return;
            }

            IEnumerable<RemoteFileInfo> jobFiles = FileTransferConsumer.FileAvailability2(job);
            log.MessageCtx(LOG_CTX, "File availability for Job '{0}':", jobId);
            foreach (RemoteFileInfo jobFile in jobFiles)
                log.MessageCtx(LOG_CTX, "\t{0} ({1})", jobFile.Filename, ((RSG.Base.FileSize)jobFile.FileSize));
        }

        /// <summary>
        /// Send off a map section for network export.  Text file can be specified which contains
        /// multiple sections to export.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="arguments"></param>
        private static void CommandNetworkExport(CommandOptions options, String[] arguments)
        {
            if (0 == arguments.Length)
            {
                log.Error("No arguments specified for Network Export - specify a map file or text file to batch network export.");
                return;
            }

            ICollection<String> filenames = new List<String>();
            IEnumerable<MapExportJob> jobs;

            // Parse arguments - map section or text file giving a list of sections to set up jobs for
            foreach (String argument in arguments)
            {
                // If a text file, read it to get multiple map sections in
                if (argument.EndsWith(".txt"))
                {
                    if (File.Exists(argument))
                    {
                        try
                        {
                            log.MessageCtx(LOG_CTX, "Reading in map sections from {0}", argument);
                            filenames.AddRange(File.ReadAllLines(argument));
                        }
                        catch (System.Exception ex)
                        {
                            log.ErrorCtx(LOG_CTX, "Exception swallowed while trying to read map files from {0} - {1}", argument, ex.ToString());
                        }
                    }
                }
                else
                {
                    filenames.Add(argument);
                }
            }

            if (filenames.Count > 0)
            {
                IProcessorCollection processors = new ProcessorCollection(options.Config);
                IContentTree tree = Content.Factory.CreateTree(options.Branch);

                using (P4 p4 = options.Config.Project.SCMConnect())
                {
                    jobs = MapExportJobFactory.Create(options.Branch, tree, p4, filenames, true);
                    log.Message("{0} jobs created.", jobs.Count());

                    // Enqueue jobs. 
                    foreach (MapExportJob job in jobs)
                    {
#warning AJM:Hacky shite just to get this working right now
                        String maxFile = job.ExportProcesses.First().DCCSourceFilename;
                        log.Message("Uploading Job File: {0}", maxFile);
                        FileTransferConsumer.UploadFile(job, maxFile);
                        String xmlFile = job.ExportProcesses.First().ExportFiles.Where(f => f.EndsWith(".xml")).FirstOrDefault();
                        log.Message("Uploading Job File: {0}", xmlFile);
                        FileTransferConsumer.UploadFile(job, xmlFile);


                        log.Message("Enqueuing Job {0}", job.ID);
                        AutomationConsumer.EnqueueJob(job);
                    }
                }
            }
        }

        /// <summary>
        /// Helper to strop off description from frame capture commands
        /// </summary>
        /// <param name="arguments"></param>
        /// <param name="description"></param>
        /// <param name="args"></param>
        private static void StripFrameCapDescription(String[] arguments, out String description, out List<String> args)
        {
            if (!arguments.Last().EndsWith(".fbx"))
            {
                description = arguments.Last();
                args = arguments.ToList();
                args.RemoveAt(args.Count - 1);
            }
            else
            {
                description = String.Format("Automated Frame Capture for {0}", Path.GetFileName(arguments.Last()));
                args = arguments.ToList();
                log.WarningCtx(LOG_CTX, "No Description provided for job, generated one automatically");
            }
        }

        /// <summary>
        /// Send off a cutscene build to the cutscene scripter. 
        /// multiple sections to export.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="arguments"></param>
        private static void CommandFrameCapture(CommandOptions options, String[] arguments)
        {
            if (0 == arguments.Length)
            {
                log.Error("No arguments specified for Network Export - specify a map file or text file to batch network export.");
                return;
            }
            String description;
            List<String> args;
            StripFrameCapDescription(arguments, out description, out args);
            foreach (String argument in args)
            {
                if (argument.EndsWith(".fbx"))
                {
                    if (File.Exists(argument))
                    {
                        try
                        {
                            log.MessageCtx(LOG_CTX, "Adding to CutSceneScripter {0}", argument);
                            UserRequestTrigger trigger = new UserRequestTrigger(new List<String>() { argument }, Guid.NewGuid(), DateTime.UtcNow, description);
                            Job job = new Job(options.Branch, CapabilityType.CutsceneScripter, trigger);
                            AutomationConsumer.EnqueueJob(job);
                        }
                        catch (System.Exception ex)
                        {
                            log.ErrorCtx(LOG_CTX, "Exception while trying to add job {0} - {1}", argument, ex.ToString());
                        }
                    }
                    else
                    {
                        log.WarningCtx(LOG_CTX, "File {0} doesn't exist on disk, so won't be processed", argument);
                    }
                }
                else
                {
                    log.WarningCtx(LOG_CTX, "File {0} isn't a FBX so won't be processed", argument);
                }
            }
        }
        #endregion // Command Handler Methods

        #region Utility Helper Method


        /// <summary>
        /// Pretty-print cursor/caret for user-input.
        /// </summary>
        private static void PrettyPrintCaret()
        {
            Uri connection = AutomationConsumer.ServiceConnection;
            System.Console.Write("{0}@{1} > ", Environment.UserName, connection.Host);
        }

        /// <summary>
        /// Pretty-print a table of information; alternating console background
        /// and foreground colours.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="header"></param>
        /// <param name="formatter"></param>
        private static void PrettyPrintTable<T>(String header, IEnumerable<T> objects,
            Action<T> formatter)
        {
            ConsoleColor startFgCol = System.Console.ForegroundColor;
            ConsoleColor startBgCol = System.Console.BackgroundColor;
            try
            {
                ConsoleColor bgColour = ConsoleColor.DarkGray;
                ConsoleColor fgColour = ConsoleColor.Gray;
                System.Console.WriteLine(header);
                int n = 0;
                foreach (T o in objects)
                {
                    System.Console.ForegroundColor = fgColour;
                    System.Console.BackgroundColor = bgColour;
                    formatter(o);

                    if (0 == n % 2)
                    {
                        fgColour = ConsoleColor.DarkGray;
                        bgColour = ConsoleColor.Gray;
                    }
                    else
                    {
                        bgColour = ConsoleColor.DarkGray;
                        fgColour = ConsoleColor.Gray;
                    }
                    ++n;
                }
            }
            finally
            {
                // Restore.
                System.Console.ForegroundColor = startFgCol;
                System.Console.BackgroundColor = startBgCol;
            }
        }
        #endregion // Utility Helper Methods

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void ShutdownKill(Object sender, EventArgs e)
        {
            log.Warning("Automation Console being killed.  Attempting to clean up.");
            Shutdown();
        }
        #endregion // Event Handlers
    }
} // RSG.Pipeline.Automation.Console namespace
