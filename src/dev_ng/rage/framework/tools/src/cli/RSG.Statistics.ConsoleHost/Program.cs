﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Configuration;
using System.Threading;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.Net;
using RSG.Base.OS;
using RSG.Pipeline.Services;
using RSG.Services.Common.ServiceModelExtensions;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Server;
using RSG.Statistics.Server.Extensions;
using RSG.Statistics.Server.Services;

namespace RSG.Statistics.ConsoleHost
{
    class Program
    {
        #region Constants
        // Options
        private static readonly String OPT_SERVER = "server";
        private static readonly String OPTION_UPDATE_SCHEMA = "updateschema";        // On starting the server update the schema
        #endregion // Constants

        #region Member Data
        private static IUniversalLog m_log;
        #endregion // Member Data

        #region Program Entry Point
        /// <summary>
        /// The main entry point for the application.
        /// Attribute [STAThread] for invoking the universal log from the right thread
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            // Create the log
            LogFactory.Initialize();
            m_log = LogFactory.CreateUniversalLog("Statistics_ServerHost");
            LogFactory.CreateApplicationConsoleLogTarget();
            UniversalLogFile logfile = LogFactory.CreateUniversalLogFile(m_log) as UniversalLogFile;

            //ConsoleLog consoleOutput = new ConsoleLog();
            //VSOutputLogTarget vsOutput = new VSOutputLogTarget();

            // Define our executable options.  Stick with the long options
            // (e.g. --opt) rather than the single character versions.
            LongOption[] lopts = new LongOption[] {
                new LongOption(OPT_SERVER, LongOption.ArgType.Required, "Server to connect to."),
                new LongOption(OPTION_UPDATE_SCHEMA, LongOption.ArgType.None, "Optionally update the database schema.")
            };
            CommandOptions options = new CommandOptions(args, lopts);

            bool updateSchema = options.ContainsOption(OPTION_UPDATE_SCHEMA);

            // Initialise the server to communicate with.
            IStatisticsConfig config = new StatisticsConfig(options.Branch);
            IStatsServer server;
            if (!options.ContainsOption(OPT_SERVER))
            {
                server = config.DefaultServer;
            }
            else
            {
                String serverName = (String)options[OPT_SERVER];
                Debug.Assert(config.Servers.ContainsKey(serverName), "Requested an unknown server");
                server = config.Servers[serverName];
            }

            // Initialise the server
            ServerBootStrapper.Initialise(server, updateSchema);

            // Keep track of the service hosts we opened so that we can close them later on
            List<ServiceHost> hosts = new List<ServiceHost>();
            Thread webUpdateThread = null;

            // All services need to be in the same assembly as the ServiceBase class
            Assembly ass = typeof(ServiceBase).Assembly;

            try
            {
                // For each service section in the configuration, create and open a host
                m_log.Message("Starting WCF services.");
                m_log.Profile("Starting WCF services.");

                ServicesSection section = ConfigurationManager.GetSection("system.serviceModel/services") as ServicesSection;
                if (section != null)
                {
                    foreach (ServiceElement element in section.Services)
                    {
                        m_log.Debug("Creating host for the '{0}' service.", element.Name);
                        Type serviceType = ass.GetType(element.Name);
                        Debug.Assert(serviceType != null, "Unable to determine the type of the service from the config.");
                        if (serviceType == null)
                        {
                            throw new ArgumentNullException(String.Format("Unable to determine the type of the service '{0}' from the config.", element.Name));
                        }

                        // Determine what kind of service we are trying to create.
                        ServiceBehaviorAttribute att =
                           serviceType.GetCustomAttributes(typeof(ServiceBehaviorAttribute), true).FirstOrDefault() as ServiceBehaviorAttribute;
                        Debug.Assert(att != null, "Trying to instantiate a service instance that doesn't have a ServiceBehaviorAttribute associated with it.");
                        if (att == null)
                        {
                            throw new ArgumentNullException("Trying to instantiate a service instance that doesn't have a ServiceBehaviorAttribute associated with it.");
                        }

                        // Create the appropriate host for the service's instance context mode.
                        ServiceHost host;
                        if (att.InstanceContextMode == InstanceContextMode.Single)
                        {
                            object serviceInstance = Activator.CreateInstance(serviceType, server);
                            host = new ConfigServiceHost(serviceInstance);
                        }
                        else
                        {
                            host = new ConfigServiceHost(server, serviceType);
                        }

                        // Keep track of this host for later and open it.
                        hosts.Add(host);
                        host.Open();
                    }

                    m_log.ProfileEnd();
                }

                // Start up the web service
                m_log.Profile("Starting web server.");

                WebService.WebRootDir = server.WebRootDir;
                WebService.WebServerPort = server.HttpPort;
                WebService.SetConfigDir(options.Branch.Project.Config.ToolsConfig);

                webUpdateThread = new Thread(new ThreadStart(WebService.Start));
                webUpdateThread.IsBackground = true;
                webUpdateThread.Start();
                m_log.ProfileEnd();

                Console.WriteLine("Server successfully started.  Press <enter> to stop.");
                Console.ReadLine();
                Console.ReadLine();
            }
            catch (ConfigurationVersionException ex)
            {
                m_log.ToolException(ex, "Configuration version error: run tools installer.  Installed version: {0}, expected version: {1}.",
                    ex.ActualVersion, ex.ExpectedVersion);
                m_log.Message("An important update has been made to the tool chain.  Install version: {0}, expected version: {1}.{2}{2}Sync latest or labelled tools, run {3} and then restart the application.",
                    ex.ActualVersion, ex.ExpectedVersion, Environment.NewLine,
                    Path.Combine(Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "install.bat"));
                Environment.Exit(1);
            }
            catch (RSG.Base.Configuration.ConfigurationException ex)
            {
                m_log.ToolException(ex, "Configuration parse error.");
                m_log.Message("There was an error initialising configuration data.{0}{0}Sync latest or labelled tools and restart the application.", Environment.NewLine);
                Environment.Exit(1);
            }
            catch (System.Exception ex)
            {
                m_log.ToolException(ex, "Unhandled Exception.");
            }
            finally
            {
                m_log.Message("Stopping WCF services.");
                foreach (ServiceHost host in hosts)
                {
                    if (host.State == CommunicationState.Opened)
                    {
                        host.Close();
                    }
                    else
                    {
                        host.Abort();
                    }
                }

                m_log.Message("Stopping web server.");
                WebService.Stop();
                if (webUpdateThread != null)
                {
                    webUpdateThread.Abort();
                }
            }

            LogFactory.ApplicationShutdown();
        }
        #endregion // Program Entry Point
    }
}
