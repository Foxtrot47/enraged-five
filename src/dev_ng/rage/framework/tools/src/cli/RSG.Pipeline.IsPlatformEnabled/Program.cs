﻿using System;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.IsPlatformEnabled
{

    /// <summary>
    /// 
    /// </summary>
    class Program
    {

        /// <summary>
        /// Entry-point.
        /// </summary>
        /// <param name="args"></param>
        static int Main(String[] args)
        {
            LogFactory.Initialize();
            IUniversalLog log = LogFactory.ApplicationLog;
            LogFactory.CreateApplicationConsoleLogTarget();

            CommandOptions options = new CommandOptions();
            if (0 == options.TrailingArguments.Count())
            {
                log.Error("No platforms specified.  Aborting.");
                return (Constants.Exit_Failure);
            }

            // Loop through our targets determining whether they are all 
            // enabled from our configuration data.
            bool enabled = true;
            foreach (String trailing in options.TrailingArguments)
            {
                String platform = trailing.ToLower();
                foreach (ITarget target in options.Branch.Targets.Values)
                {
                    if (!platform.Equals(target.Platform.ToString(), StringComparison.OrdinalIgnoreCase))
                        continue;

                    enabled &= target.Enabled;
                }
            }

            return (enabled ? Constants.Exit_Success : Constants.Exit_Failure);
        }
    }

} // RSG.Pipeline.IsPlatformEnabled namespace
