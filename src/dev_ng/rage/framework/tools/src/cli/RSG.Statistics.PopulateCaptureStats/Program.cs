﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml;
using RSG.Base.Logging;
using RSG.Base.OS;
using RSG.Statistics.Common;
using RSG.Statistics.Client.Common;
using System.Diagnostics;
using RSG.Statistics.Common.Config;
using RSG.Pipeline.Services;
using RSG.Base.Logging.Universal;
using RSG.Base.Configuration;

namespace RSG.Statistics.PopulateCaptureStats
{
    class Program
    {
        #region Constants
        // Return codes
        private static readonly int EXIT_SUCCESS = 0;
        private static readonly int EXIT_WARNING = 1;
        private static readonly int EXIT_ERROR = 2;
        private static readonly int EXIT_CRITIC = 3;

        // Options
        private static readonly String OPT_SERVER = "server";
        private static readonly String OPTION_DEV = "dev";                      // Flag indicating that this is running in dev mode
        private static readonly String OPTION_FILE = "file";                    // stats.xml file containing to process.
        private static readonly String OPTION_NO_SYNC = "nosync";               // Flag to skip the syncing of assets
        private static readonly String OPTION_BUILD_CONFIG = "buildconfig";     // Build config that this stats file is for.
        private static readonly String OPTION_LEVEL = "level";                  // Level that the stats have come from.
        private static readonly String OPTION_PERFTESTER = "perftester";        // Indicates this is the perftester running.
    
        private static readonly String OPTION_CHANGELIST = "cl";                // Changelist these stats are associated with.
        private static readonly String OPTION_MODS_FILE = "mods";               // File containing the modifications information.
        #endregion // Constants

        #region Member Data
        private static IUniversalLog m_log;
        private static IStatsServer s_server;
        #endregion // Member Data

        #region Program Entry Point
        /// <summary>
        /// The main entry point for the application.
        /// Attribute [STAThread] for invoking the universal log from the right thread
        /// </summary>
        [STAThread]
        static int Main(string[] args)
        {
            int exit_code = EXIT_SUCCESS;

            // Create the log
            LogFactory.Initialize();
            m_log = LogFactory.CreateUniversalLog("Statistics_PopulateCaptureStats");
            LogFactory.CreateApplicationConsoleLogTarget();
            UniversalLogFile logfile = LogFactory.CreateUniversalLogFile(m_log) as UniversalLogFile;

            // Define our executable options.  Stick with the long options
            // (e.g. --opt) rather than the single character versions.
            LongOption[] lopts = new LongOption[] {
                new LongOption(OPT_SERVER, LongOption.ArgType.Required, "Server to connect to."),
                new LongOption(OPTION_DEV, LongOption.ArgType.None, "Dev mode."),
                new LongOption(OPTION_FILE, LongOption.ArgType.Required, "File to parse."),
                new LongOption(OPTION_NO_SYNC, LongOption.ArgType.None, "Skip asset sync step."),
                new LongOption(OPTION_BUILD_CONFIG, LongOption.ArgType.Required, "Build configuration."),
                new LongOption(OPTION_LEVEL, LongOption.ArgType.Required, "Level."),
                new LongOption(OPTION_CHANGELIST, LongOption.ArgType.Required, "Optional changelist number."),
                new LongOption(OPTION_MODS_FILE, LongOption.ArgType.Required, "Optional modifications file."),
                new LongOption(OPTION_PERFTESTER, LongOption.ArgType.None, "Flag indicating the file is a perf tester file.")
            };
            CommandOptions options = new CommandOptions(args, lopts);

            bool dev = options.ContainsOption(OPTION_DEV);
            String file = (options.ContainsOption(OPTION_FILE) ? options[OPTION_FILE] as String : null);
            bool syncAssets = !options.ContainsOption(OPTION_NO_SYNC);
            bool perfTester = options.ContainsOption(OPTION_PERFTESTER);
            BuildConfig? buildConfig = (options.ContainsOption(OPTION_BUILD_CONFIG) ? (BuildConfig?)BuildConfigUtils.ParseFromRuntimeString(options[OPTION_BUILD_CONFIG] as String) : null);
            String level = (options.ContainsOption(OPTION_LEVEL) ? options[OPTION_LEVEL] as String : null);
            uint? changelist = (options.ContainsOption(OPTION_CHANGELIST) ? (uint?)UInt32.Parse(options[OPTION_CHANGELIST] as String) : null);
            String modsFile = (options.ContainsOption(OPTION_MODS_FILE) ? options[OPTION_MODS_FILE] as String : null);

            // Initialise the server to communicate with.
            IStatisticsConfig config = new StatisticsConfig(options.Branch);
            if (!options.ContainsOption(OPT_SERVER))
            {
                s_server = config.DefaultServer;
            }
            else
            {
                String serverName = (String)options[OPT_SERVER];
                Debug.Assert(config.Servers.ContainsKey(serverName), "Requested an unknown server");
                s_server = config.Servers[serverName];
            }

            // Validate the command-line arguments
            if (changelist != null || modsFile != null || perfTester)
            {
                Debug.Assert(buildConfig.HasValue && level != null, "Build config and level need to be provided if submitting per changelist or perftester capture stats.");
                if (!buildConfig.HasValue || level == null)
                {
                    throw new ArgumentException("Build config and level need to be provided if submitting per changelist or perftester capture stats.");
                }
            }
            else 
            {
                throw new ArgumentException("Changelist not supplied nor is perftester!");
            }

            // Preamble...
            if (dev)
            {
                Console.WriteLine("Press <enter> to start client.");
                Console.ReadLine();
                Console.WriteLine();
                Console.WriteLine("Client is now running...");
            }

            try
            {
                // Do we want to sync assets?
                if (syncAssets)
                {
                    AssetSyncService assetSyncService = new AssetSyncService(options.Branch, s_server);
                    assetSyncService.SyncLevels();
                    assetSyncService.SyncDrawLists();
                    assetSyncService.SyncCaptureRelatedStrings();
                }

                // Sync the stats.
                CaptureStatSyncService syncService = new CaptureStatSyncService(s_server);
                
                if (changelist != null)
                {
                    syncService.SyncChangelistStats(file, changelist.Value, level, buildConfig.Value);
                }
                else if (modsFile != null)
                {
                    syncService.SyncChangelistStats(file, modsFile, level, buildConfig.Value);
                }
                else if (perfTester)
                {
                    syncService.SyncPerfStats(file, level, buildConfig.Value);
                }
            }
            catch (Exception ex)
            {
                Log.Log__Exception(ex, "Unexpected exception occurred while populating capture stats.");
                exit_code = EXIT_CRITIC;
            }

            if (dev)
            {
                Console.WriteLine("Press <enter> to stop client.");
                Console.ReadLine();
            }

            LogFactory.ApplicationShutdown();
            Environment.ExitCode = exit_code;
            return (exit_code);
        }
        #endregion // Program Entry Point
    } // Program
}
