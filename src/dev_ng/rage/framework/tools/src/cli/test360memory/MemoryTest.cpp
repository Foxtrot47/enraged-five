//--------------------------------------------------------------------------------------
// MemoryViews.cpp
//
// This sample demonstrates the usage of the Xbox 360 memory view macros to treat one
// type of memory like another type.  In particular, write-combined memory can be viewed
// as cached read-only, which greatly improves read speed from that memory.
//
// This sample also demonstrates a CPU hardware hang that can occur when the cached
// read-only memory views are used on large page size allocations.  Take note of the
// text in the InvokeCPUHardwareBug function to learn how to avoid this CPU hang.
//
// Game Technology Group.
// Copyright (C) Microsoft Corporation. All rights reserved.
//--------------------------------------------------------------------------------------
#include <xtl.h>
#include <xboxmath.h>
#include <AtgApp.h>
#include <AtgFont.h>
#include <AtgHelp.h>
#include <AtgInput.h>
#include <AtgResource.h>
#include <AtgUtil.h>
#include "AtgConsole.h"

//--------------------------------------------------------------------------------------
// Globals
//--------------------------------------------------------------------------------------

// Console for output
ATG::Console g_Console;

// Global variable to stop the optimizer from removing calculations.
__int64 g_sum;



void FoundMemoryError(DWORD * pMemAddr, DWORD iDesiredVal, DWORD iActualVal, int iFailedAfter)
{
	g_Console.Format("\n\nDetected memory error at %x - should have been %x, but was %x\n", pMemAddr, iDesiredVal, iActualVal);
	g_Console.Format("Failed after %i tests\n", iFailedAfter);
	g_Console.Format("\n\nPress any button to quit\n");

	bool bButtonPressed = false;
	while(!bButtonPressed)
	{
		ATG::GAMEPAD* pGamepad = ATG::Input::GetMergedInput();
		if( pGamepad->wPressedButtons != 0 )
			bButtonPressed = true;
	}	
}

bool TestMemory(DWORD * pMem, int iNumBytes, DWORD iExpectedVal, int iTestIteration)
{
	int iNumDwords = iNumBytes / sizeof(DWORD);
	for(int i=0; i<iNumDwords; i++)
	{
		if(pMem[i] != iExpectedVal)
		{
			FoundMemoryError(&pMem[i], iExpectedVal, pMem[i], iTestIteration);
			XPhysicalFree(pMem);
			return false;
		}
	}
	return true;
}

//--------------------------------------------------------------------------------------
// Name: main
// Desc: Entry point to the program
//--------------------------------------------------------------------------------------

#define NUM_TESTS	400	//000

#define BK_COL		0xFF1F005F
#define TXT_COL		0xFFFFFFFF


VOID __cdecl main()
{
    // Initialize the console window
    g_Console.Create( "game:\\media\\fonts\\Courier_New_11.xpr", BK_COL, TXT_COL );
    g_Console.SendOutputToDebugChannel( TRUE );

	// Find out how much physical memory we can allocate
	MEMORYSTATUS memStat;
	GlobalMemoryStatus(&memStat);
	int iFreeMem = memStat.dwAvailPhys;

	g_Console.Format("Available physical memory : %dMb\n", iFreeMem/(1024*1024));

	int iAllocStepSize = 1024*1024;
	int iMemToAlloc = iFreeMem - iAllocStepSize;
	DWORD * pBuffer = NULL;

	while(iMemToAlloc > 0)
	{
		// Allocate as much of main memory as we can.
		pBuffer = (DWORD*) XPhysicalAlloc(
			iMemToAlloc,
			MAXULONG_PTR,
			0, 
			PAGE_READWRITE | MEM_LARGE_PAGES
		);

		if(pBuffer)
			break;

		iMemToAlloc -= iAllocStepSize;
	}

	if(!pBuffer)
	{
		g_Console.Format("Failed to allocate memory\n");
		return;
	}

	g_Console.Format("Allocated : %dMb\n", iMemToAlloc/(1024*1024));


	bool bQuit = false;

	while(!bQuit)
	{
		for(unsigned int i=0; i<256; i++)
		{
			unsigned int testValue = i;
			unsigned int testValue32;

			testValue32 = (testValue << 24) | (testValue<<16) | (testValue<<8) | testValue;

			XMemSet(pBuffer, testValue, iMemToAlloc);
			
			if(!TestMemory(pBuffer, iMemToAlloc, testValue32, i))
			{
				XPhysicalFree(pBuffer);
				return;
			}

			testValue = i ^ 0xff;
			testValue32;

			testValue32 = (testValue << 24) | (testValue<<16) | (testValue<<8) | testValue;

			XMemSet(pBuffer, testValue, iMemToAlloc);

			if(!TestMemory(pBuffer, iMemToAlloc, testValue32, i))
			{
				XPhysicalFree(pBuffer);
				return;
			}

			g_Console.Format(".");

			if(((i+1)&0xf)==0)
				g_Console.Format("\n");
		}
		//*********************************************************************
		//	Test has completed successfully.  Your 360 devkit is probably ok

		g_Console.Format("\n\nMemory appears to be ok.\n");
		g_Console.Format("\n\nPress A to run test again, or any other button to quit.");


		bool bButtonPressed = false;

		while (!bButtonPressed)
		{
			ATG::GAMEPAD* pGamepad = ATG::Input::GetMergedInput();
			if( pGamepad->wPressedButtons != 0 )
			{
				if(pGamepad->wPressedButtons & XINPUT_GAMEPAD_A)
				{

				}
				else
				{
					bQuit = true;
				}
				
				g_Console.Format( "\n" );
				break;  
			}
		}

	}

	XPhysicalFree(pBuffer);


}
