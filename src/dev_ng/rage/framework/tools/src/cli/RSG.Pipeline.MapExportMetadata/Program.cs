﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Base.Profiling;
using RSG.Base.Windows;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Metadata;
using RSG.SceneXml;
using RSG.SceneXml.MapExport.Framework;
using RSG.SceneXml.MapExport.Util;

namespace RSG.Pipeline.MapExportMetadata
{

    /// <summary>
    /// Map Metadata Serialiser application class.
    /// </summary>
    class Program
    {
        #region Constants
        private static readonly String LOG_CTX = "Map Metadata";
        private static readonly String OPT_CONFIG = "config";
        #endregion // Constants

        /// <summary>
        /// Entry-point.
        /// </summary>
        /// <param name="args"></param>
        [STAThread]
        static int Main(String[] args)
        {
            int result = Constants.Exit_Success;
            LongOption[] lopts = new LongOption[] {
                    new LongOption(OPT_CONFIG, LongOption.ArgType.Required,
                        "Map metadata serialiser configuration XML filename.")
                };
            CommandOptions options = new CommandOptions(args, lopts);
            LogFactory.Initialize();
            IUniversalLog log = LogFactory.CreateUniversalLog("Map Metadata Serialiser");
            IUniversalLogTarget logFile = LogFactory.CreateUniversalLogFile(log);
            LogFactory.CreateApplicationConsoleLogTarget();
            try
            {
                Debug.Assert(options.ContainsOption(OPT_CONFIG),
                    "No config option defined.");
                if (options.ShowHelp || !options.ContainsOption(OPT_CONFIG))
                {
                    log.MessageCtx(LOG_CTX, options.GetUsage());
                    return (result);
                }
                String configFilename = (String)options[OPT_CONFIG];
                MapMetadataSerialiserConfig config = new MapMetadataSerialiserConfig(configFilename);

                // Load Content-Tree.  This is required for the SceneCollection.
                IContentTree tree = RSG.Pipeline.Content.Factory.CreateTree(options.Branch);
                ContentTreeHelper contentHelper = new ContentTreeHelper(tree);

                // DHM TODO: DLC hack.  See Archetype.Branch and Entity.Branch.
                RSG.SceneXml.MapExport.Archetype.Branch = options.Branch;
                RSG.SceneXml.MapExport.Entity.Branch = options.Branch;

                // This is the real reason we allow batch processing; so
                // we don't pay for this overhead again and again.
                log.ProfileCtx(LOG_CTX, "Loading Generic Scene Data.");
                SceneCollection sceneCollection = new SceneCollection(options.Branch,
                    tree, SceneCollectionLoadOption.All);
                log.ProfileEnd();

                // Initialise our Scene override integrator component.
                SceneOverrideIntegrator.Instance.Initialise(options.Branch, contentHelper, config.Options.SceneOverridesDirectory);
                InterContainerLODManager.Instance.Initialise(sceneCollection, contentHelper);
                    
                // Loop through our tasks.
                config.Tasks.ForEachWithIndex((task, index) =>
                {
                    if (task is MapMetadataSerialiserTask)
                    {
                        log.ProfileCtx(LOG_CTX, "Processing Task [{0}/{1}]: {2}",
                            index + 1, config.Tasks.Count(), task.Name);

                        MapMetadataSerialiserTask t = (MapMetadataSerialiserTask)task;
                        if (!MetadataSerialiser.ProcessMap(log, options.Branch, tree, config.Options, t, sceneCollection))
                            result = Constants.Exit_Failure;

                        log.ProfileEnd();
                    }
                    else if (task is MapMetadataSerialiserMergeTask)
                    {
                        log.ProfileCtx(LOG_CTX, "Processing Merge Task [{0}/{1}]: {2}",
                            index + 1, config.Tasks.Count(), task.Name);

                        MapMetadataSerialiserMergeTask t = (MapMetadataSerialiserMergeTask)task;
                        if (!MetadataSerialiser.ProcessMapMerge(log, options.Branch, tree, config.Options, t, sceneCollection))
                            result = Constants.Exit_Failure;

                        log.ProfileEnd();
                    }
                }
                );
                
                if (LogFactory.HasError())
                    result = Constants.Exit_Failure;
            }
            catch (Exception ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex,
                    "Unhandled exception during Map Metadata Serialisation.");
                result = Constants.Exit_Failure;
            }
            return (result);
        }
    }

} // RSG.Pipeline.MapExportMetadata namespace
