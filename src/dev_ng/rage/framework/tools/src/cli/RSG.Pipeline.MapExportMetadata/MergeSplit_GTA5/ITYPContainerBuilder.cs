﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.SceneXml.MapExport;
using RSG.SceneXml.MapExport.AssetCombine;
using RSG.SceneXml.MapExport.Project.GTA5;
using RSG.SceneXml.MapExport.Project.GTA5.Collections;

namespace RSG.Pipeline.MapExportMetadata.MergeSplit_GTA5
{
    /// <summary>
    /// This class manages the the creation of new ITYP containers from existing parts (Archetypes, other containers, etc).
    /// The internal workings are encapsulated.  We should only ever provide methods to add data and to serialise
    /// complete containers.
    /// </summary>
    internal class ITYPContainerBuilder
    {
        internal ITYPContainerBuilder(
            IBranch branch, 
            AssetFile assetFile, 
            ModelAudioCollisionCollection modelAudioCollisionCollection, 
            String outputDirectory,
            String outputITYPBasename,
            IDictionary<String, IEnumerable<ArchetypeLite>> coreArchetypeObjects)
        {
            branch_ = branch;
            assetFile_ = assetFile;
            modelAudioCollisionCollection_ = modelAudioCollisionCollection;
            outputDirectory_ = outputDirectory;
            outputITYPBasename_ = outputITYPBasename;
            coreArchetypeObjects_ = coreArchetypeObjects;

            partialStandardContainerNode_ = null;
            partialStreamedContainerNode_ = null;
            partialPermanentContainerNode_ = null;
            standardContainerIndex_ = 1;
            streamedContainerIndex_ = 1;
            completeStandardContainerNodes_ = new List<ITYPContainerNode>();
            completeStreamedContainerNodes_ = new List<ITYPContainerNode>();
            completePermanentContainerNodes_ = new List<ITYPContainerNode>();
            interiorContainerMap_ = new Dictionary<string, ITYPContainer>();
        }

        internal int StandardContainerCount
        {
            get
            {
                return completeStandardContainerNodes_.Count;
            }
        }

        internal int StreamedContainerCount
        {
            get
            {
                return completeStreamedContainerNodes_.Count;
            }
        }

        internal int PermanentContainerCount
        {
            get { return completePermanentContainerNodes_.Count; }
        }

        internal double PendingStandardContainerSize
        {
            get
            {
                if (partialStandardContainerNode_ == null)
                    return 0;

                return ITYPCostEstimator.EstimateContainerContentCost(partialStandardContainerNode_.Container);
            }
        }

        internal double PendingStreamedContainerSize
        {
            get
            {
                if (partialStreamedContainerNode_ == null)
                    return 0;

                return ITYPCostEstimator.EstimateContainerContentCost(partialStreamedContainerNode_.Container);
            }
        }

        internal double PendingPermanentContainerSize
        {
            get
            {
                if (partialPermanentContainerNode_ == null)
                    return 0;

                return ITYPCostEstimator.EstimateContainerContentCost(partialPermanentContainerNode_.Container);
            }
        }

        /// <summary>
        /// Append a standard container
        /// </summary>
        internal void AppendStandardContainer(MergeSplitInputNode inputNode)
        {
            // Append standard container data
            if (partialStandardContainerNode_ == null)
                partialStandardContainerNode_ = NewStandardContainerNode();

            partialStandardContainerNode_.Container.MergeWith(inputNode.ITYPSerialiserNode.Container);

            inputNode.IMAPSerialiserNode.ReplaceDependency(
                inputNode.ITYPSerialiserNode.Container.Name,
                partialStandardContainerNode_.Container.Name);
        }

        /// <summary>
        /// Append a standard container that is too big.
        /// Here we split the container based on the limit provided by a higher level of the algortihm.
        /// </summary>
        internal void AppendOversizeStandardContainer(MergeSplitInputNode inputNode, double standardITYPLimit)
        {
            String originalITYPName = inputNode.ITYPSerialiserNode.Container.Name;
            List<String> splitITYPNames = new List<String>();

            // There is some special setup for the first split cotnainer
            ITYPContainerNode firstSplitContainerNode = NewStandardContainerNode();
            splitITYPNames.Add(firstSplitContainerNode.Container.Name);
            firstSplitContainerNode.Container.AddDependencyRange(inputNode.ITYPSerialiserNode.Container.Dependencies);
            firstSplitContainerNode.Container.AddCompositeEntityTypeRange(inputNode.ITYPSerialiserNode.Container.CompositeEntityTypes.Values);

            ITYPContainerNode currentContainerNode = firstSplitContainerNode;
            foreach (Archetype archetype in inputNode.ITYPSerialiserNode.Container.Archetypes)
            {
                if (currentContainerNode == null)
                {
                    currentContainerNode = NewStandardContainerNode();
                    splitITYPNames.Add(currentContainerNode.Container.Name);
                    currentContainerNode.Container.AddDependencyRange(inputNode.ITYPSerialiserNode.Container.Dependencies);
                }

                currentContainerNode.Container.AddArchetype(archetype);

                if (ITYPCostEstimator.EstimateContainerContentCost(currentContainerNode.Container) >= standardITYPLimit)
                {
                    completeStandardContainerNodes_.Add(currentContainerNode);
                    currentContainerNode = null;
                }
            }

            // Collect any straggler
            if (currentContainerNode != null)
            {
                completeStandardContainerNodes_.Add(currentContainerNode);
                currentContainerNode = null;
            }

            inputNode.IMAPSerialiserNode.ReplaceStaticDependency(originalITYPName, splitITYPNames);
        }

        /// <summary>
        /// Append a streamed container
        /// </summary>
        internal void AppendStreamedContainer(MergeSplitInputNode inputNode)
        {
            // Append streamed container data
            if (partialStreamedContainerNode_ == null)
                partialStreamedContainerNode_ = NewStreamedContainerNode();

            partialStreamedContainerNode_.Container.MergeWith(inputNode.ITYPSerialiserNode.StreamedContainer);

            inputNode.IMAPSerialiserNode.ReplaceDependency(
                inputNode.ITYPSerialiserNode.StreamedContainer.Name,
                partialStreamedContainerNode_.Container.Name);
        }

        /// <summary>
        /// Append a permanent container.
        /// </summary>
        /// <param name="inputNode"></param>
        internal void AppendPermanentContainer(MergeSplitInputNode inputNode)
        {
            // Append streamed container data
            if (partialPermanentContainerNode_ == null)
                partialPermanentContainerNode_ = NewPermanentContainerNode();

            partialPermanentContainerNode_.Container.MergeWith(inputNode.ITYPSerialiserNode.PermanentContainer);

            inputNode.IMAPSerialiserNode.ReplaceDependency(
                inputNode.ITYPSerialiserNode.PermanentContainer.Name,
                partialPermanentContainerNode_.Container.Name);
        }

        /// <summary>
        /// Append an in-situ interior container
        /// </summary>
        internal void AppendInteriorContainer(ITYPContainer itypContainer)
        {
            interiorContainerMap_.Add(itypContainer.Name, itypContainer);
        }

        internal void FinaliseStandardContainer()
        {
            if (partialStandardContainerNode_ != null)
            {
                completeStandardContainerNodes_.Add(partialStandardContainerNode_);
                partialStandardContainerNode_ = null;
            }
        }

        internal void FinaliseStreamedContainer()
        {
            if (partialStreamedContainerNode_ != null)
            {
                completeStreamedContainerNodes_.Add(partialStreamedContainerNode_);
                partialStreamedContainerNode_ = null;
            }
        }

        internal void FinalisePermanentContainer()
        {
            if (partialPermanentContainerNode_ != null)
            {
                completePermanentContainerNodes_.Add(partialPermanentContainerNode_);
                partialPermanentContainerNode_ = null;
            }
        }

        internal void Serialise()
        {
            foreach (ITYPContainerNode completeContainerNode in completeStandardContainerNodes_)
            {
                Log.Log__Message("Serialising standard ITYP to '{0}'", completeContainerNode.Pathname);
                SceneSerialiserITYP.WriteContainerToDisc(
                    completeContainerNode.Container,
                    completeContainerNode.Pathname,
                    new SceneXml.Scene[0],
                    branch_,
                    assetFile_,
                    modelAudioCollisionCollection_,
                    coreArchetypeObjects_);
            }

            System.Diagnostics.Debug.Assert(completePermanentContainerNodes_.Count <= 1);
            foreach (ITYPContainerNode permanentContainerNode in completePermanentContainerNodes_)
            {
                Log.Log__Message("Serialising permanent ITYP to '{0}'", permanentContainerNode.Pathname);
                SceneSerialiserITYP.WriteContainerToDisc(
                    permanentContainerNode.Container,
                    permanentContainerNode.Pathname,
                    new SceneXml.Scene[0],
                    branch_,
                    assetFile_,
                    modelAudioCollisionCollection_,
                    coreArchetypeObjects_);
            }

            foreach (ITYPContainerNode completeContainerNode in completeStreamedContainerNodes_)
            {
                Log.Log__Message("Serialising streamed ITYP to '{0}'", completeContainerNode.Pathname);
                SceneSerialiserITYP.WriteContainerToDisc(
                    completeContainerNode.Container,
                    completeContainerNode.Pathname,
                    new SceneXml.Scene[0],
                    branch_,
                    assetFile_,
                    modelAudioCollisionCollection_,
                    coreArchetypeObjects_);
            }

            foreach (KeyValuePair<String, ITYPContainer> interiorContainerPair in interiorContainerMap_)
            {
                String pathname = System.IO.Path.Combine(outputDirectory_, interiorContainerPair.Key + ".ityp");
                Log.Log__Message("Serialising in-situ interior ITYP to '{0}'", pathname);
                SceneSerialiserITYP.WriteContainerToDisc(
                    interiorContainerPair.Value,
                    pathname,
                    new SceneXml.Scene[0],
                    branch_,
                    assetFile_,
                    modelAudioCollisionCollection_,
                    coreArchetypeObjects_);
            }
        }

        internal void WriteManifestData(XDocument xmlManifestAdditionsDocument)
        {
            foreach (ITYPContainerNode completeContainerNode in completeStandardContainerNodes_)
            {
                XElement itypDep2Element = SceneSerialiserITYP.AsManifestITYPDependency2(completeContainerNode.Container);
                if (null != itypDep2Element)
                    xmlManifestAdditionsDocument.Root.Add(itypDep2Element);
            }

            foreach (KeyValuePair<String, ITYPContainer> interiorContainerPair in interiorContainerMap_)
            {
                XElement itypDep2Element = SceneSerialiserITYP.AsManifestITYPDependency2(interiorContainerPair.Value);
                if (null != itypDep2Element)
                    xmlManifestAdditionsDocument.Root.Add(itypDep2Element);
            }
        }

        /// <summary>
        /// Helper method to manage the unique naming requirement
        /// </summary>
        private ITYPContainerNode NewStandardContainerNode()
        {
            String containerName = String.Format("{0}_{1}", outputITYPBasename_, standardContainerIndex_.ToString("000"));
            ++standardContainerIndex_;
            ITYPContainer container = new ITYPContainer(containerName);
            String pathname = System.IO.Path.Combine(outputDirectory_, containerName + ".ityp");

            return new ITYPContainerNode(container, pathname);
        }

        /// <summary>
        /// Helper method to manage the unique naming requirement
        /// </summary>
        private ITYPContainerNode NewStreamedContainerNode()
        {
            String containerName = String.Format("{0}_{1}_strm", outputITYPBasename_, streamedContainerIndex_.ToString("000"));
            ++streamedContainerIndex_;
            ITYPContainer container = new ITYPContainer(containerName);
            String pathname = System.IO.Path.Combine(outputDirectory_, containerName + ".ityp");

            return new ITYPContainerNode(container, pathname);
        }

        /// <summary>
        /// Helper method to manage the unique naming requirement
        /// </summary>
        private ITYPContainerNode NewPermanentContainerNode()
        {
            String containerName = String.Format("{0}_permanent", outputITYPBasename_);
            ITYPContainer container = new ITYPContainer(containerName);
            String pathname = System.IO.Path.Combine(outputDirectory_, containerName + ".ityp");

            return new ITYPContainerNode(container, pathname);
        }

        // Construction time data
        private IBranch branch_;
        private AssetFile assetFile_;
        private ModelAudioCollisionCollection modelAudioCollisionCollection_;
        private String outputDirectory_;
        private String outputITYPBasename_;
        private IDictionary<String, IEnumerable<ArchetypeLite>> coreArchetypeObjects_;

        // Working data
        private ITYPContainerNode partialStandardContainerNode_;
        private ITYPContainerNode partialStreamedContainerNode_;
        private ITYPContainerNode partialPermanentContainerNode_;
        private int standardContainerIndex_;
        private int streamedContainerIndex_;

        // Complete container node data
        private List<ITYPContainerNode> completeStandardContainerNodes_;
        private List<ITYPContainerNode> completeStreamedContainerNodes_;
        private List<ITYPContainerNode> completePermanentContainerNodes_;
        private Dictionary<String, ITYPContainer> interiorContainerMap_;
    }
}
