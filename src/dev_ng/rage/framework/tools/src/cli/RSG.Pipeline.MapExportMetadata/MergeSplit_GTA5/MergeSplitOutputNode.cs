﻿using System.Collections.Generic;
using System.Linq;

namespace RSG.Pipeline.MapExportMetadata.MergeSplit_GTA5
{
    /// <summary>
    /// This is an abstraction of a the output from a metadata merge/split.
    /// </summary>
    internal class MergeSplitOutputNode
    {
        internal MergeSplitOutputNode(ITYPContainerBuilder itypContainerBuilder, 
                                      IEnumerable<IMAPSerialiserNode> imapSerialiserNodes,
                                      IEnumerable<ITYPSerialiserNode> itypSerialiserNodes)
        {
            ITYPContainerBuilder = itypContainerBuilder;
            IMAPSerialiserNodes = imapSerialiserNodes.ToArray();
            ITYPSerialiserNodes = itypSerialiserNodes.ToArray();
        }

        internal ITYPContainerBuilder ITYPContainerBuilder { get; private set; }
        internal IMAPSerialiserNode[] IMAPSerialiserNodes { get; private set; }
        internal ITYPSerialiserNode[] ITYPSerialiserNodes { get; private set; }
    }
}
