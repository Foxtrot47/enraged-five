﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;
using RSG.SceneXml.MapExport.Project.GTA5;
using RSG.SceneXml.MapExport.Project.GTA5.Collections;

namespace RSG.Pipeline.MapExportMetadata.MergeSplit_GTA5
{
    /// <summary>
    /// This class acts as a composite of a SceneSerialiserITYP object and merge/split related data.
    /// It encapsulates the complexity of SceneSerialiserITYP.
    /// </summary>
    internal class ITYPSerialiserNode
    {
        internal ITYPSerialiserNode(SceneSerialiserITYP serialiser, String pathname)
        {
            serialiser_ = serialiser;
            pathname_ = pathname;
        }

        internal ITYPContainer Container
        {
            get
            {
                return serialiser_.Container;
            }
        }

        internal ITYPContainer StreamedContainer
        {
            get
            {
                return serialiser_.StreamedContainer;
            }
        }

        internal ITYPContainer PermanentContainer
        {
            get
            {
                return serialiser_.PermanentContainer;
            }
        }

        internal Dictionary<String, ITYPContainer> InteriorContainerMap
        {
            get
            {
                return serialiser_.InteriorContainerMap;
            }
        }

        internal RSG.SceneXml.MapExport.AssetCombine.AssetFile AssetFile
        {
            get
            {
                return serialiser_.AssetFile;
            }
        }

        internal RSG.SceneXml.MapExport.ModelAudioCollisionCollection ModelAudioCollisionCollection
        {
            get
            {
                return serialiser_.ModelAudioCollision;
            }
        }

        internal String Name
        {
            get
            {
                return System.IO.Path.GetFileNameWithoutExtension(pathname_);
            }
        }

        internal void Serialise()
        {
            serialiser_.Serialise(pathname_);
        }

        internal void WriteManifestData(XDocument manifestAdditionsDocument)
        {
            serialiser_.WriteManifestData(manifestAdditionsDocument);
        }

        internal void MergeWith(ITYPSerialiserNode serialiserNode)
        {
            serialiser_.MergeWith(serialiserNode.serialiser_);
        }

        private SceneSerialiserITYP serialiser_;
        private String pathname_;
    }
}
