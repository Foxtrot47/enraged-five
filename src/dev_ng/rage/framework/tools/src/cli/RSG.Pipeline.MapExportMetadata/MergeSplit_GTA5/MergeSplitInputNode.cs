﻿using RSG.Base.Math;

namespace RSG.Pipeline.MapExportMetadata.MergeSplit_GTA5
{
    /// <summary>
    /// This is an abstraction of a single input to a metadata merge/split.
    /// The goal here is to encapsulate the underlying serialiser objects, which are quite unwieldy, so that
    /// the high level algorithm is easier to follow
    /// </summary>
    internal class MergeSplitInputNode
    {
        internal MergeSplitInputNode(ITYPSerialiserNode itypSerialiserNode, IMAPSerialiserNode imapSerialiserNode)
        {
            ITYPSerialiserNode = itypSerialiserNode;
            IMAPSerialiserNode = imapSerialiserNode;
        }

        // First class properties
        internal ITYPSerialiserNode ITYPSerialiserNode { get; private set; }
        internal IMAPSerialiserNode IMAPSerialiserNode { get; private set; }

        // Derived properties
        internal BoundingBox2f BoundsXY
        {
            get
            {
                return IMAPSerialiserNode.BoundsXY;
            }
        }

        internal bool IsLOD
        {
            get
            {
                return ITYPSerialiserNode.Name.ToLower().Contains("_lod");
            }
        }
    }
}
