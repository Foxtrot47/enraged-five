﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Core;
using RSG.Pipeline.MapExportMetadata.MergeSplit_GTA5;
using RSG.Pipeline.Services.Metadata;
using RSG.Pipeline.Services.Platform;
using RSG.SceneXml;
using RSG.SceneXml.MapExport;
using RSG.SceneXml.MapExport.AssetCombine;

using RSG.SceneXml.MapExport.Project.GTA5;
using RSG.SceneXml.MapExport.Project.GTA5.Collections;
using RSG.SceneXml.MapExport.Util;
using SIO = System.IO;

namespace RSG.Pipeline.MapExportMetadata
{
    internal static partial class MetadataSerialiser
    {
        private static Dictionary<InteriorContainerNameMap.InteriorKey, string> CoreInteriorsNamesMap;

        private static bool ProcessMap_GTA5(IUniversalLog log, IBranch branch, IContentTree tree, MapMetadataSerialiserOptions options, MapMetadataSerialiserTask task, SceneCollection sceneCollection)
        {
            bool result = true;
            log.ProfileCtx(LOG_CTX, "Loading task scene data: {0} inputs.", task.Inputs.Count());

            // Load scene collection
            string[] scenePathnames = task.Inputs.Select(input => input.SceneFilename).ToArray();
            ICollection<Scene> scenes = LoadScenes(scenePathnames, sceneCollection, log);

            AssetFile assetCollection = CreateAssetFile(task.AssetCombineFilename, log, branch);
            log.ProfileEnd();

            // Core Game Archetype Cache.
            IDictionary<string, string> coreArchetypeCache = new Dictionary<string, string>();
            IDictionary<String, IEnumerable<ArchetypeLite>> coreArchetypeObjects = 
                new Dictionary<String, IEnumerable<ArchetypeLite>>();
            if (branch.Project.IsDLC)
            {
                coreArchetypeCache = LoadCoreGameArchetypes_GTA5(log, task.CoreGameMetadataZipDependencies,
                    out coreArchetypeObjects);
                log.Message("Core Archetypes Loaded: {0}.", coreArchetypeCache.Count);
            }

            foreach (MapMetadataSerialiserOutput output in task.Output)
            {
                if (!result)
                    break;

                // Set the scene platform before any attributes will be queried.
                ITarget target = PlatformPathConversion.GetTargetFromRagebuilderPlatform(branch, output.Platform);
                foreach (Scene scene in scenes)
                    scene.SetSceneTarget(target);

                if (!SIO.Directory.Exists(output.OutputDirectory))
                    SIO.Directory.CreateDirectory(output.OutputDirectory);

                // Map Metadata
                InteriorContainerNameMap interiorNameMap = new InteriorContainerNameMap(CoreInteriorsNamesMap);
                scenePathnames = scenes.Select(scene => scene.Filename).ToArray();
                XDocument xmlManifestAdditions = Manifest.CreateManifestFile(scenePathnames);
                SceneSerialiserITYP itypSerialiser = null;

                if (output.ExportArchetypes)
                {
                    log.ProfileCtx(LOG_CTX, "Generate ITYP data.");
                    if (SIO.Directory.Exists(output.OutputDirectory))
                    {
                        foreach (string existingItypFile in SIO.Directory.EnumerateFiles(
                            output.OutputDirectory, "*.ityp", SIO.SearchOption.TopDirectoryOnly))
                        {
                            SIO.File.Delete(existingItypFile);
                        }
                    }

                    string itypFilename = MapAsset.AppendMapPrefixIfRequired(branch.Project, string.Format("{0}.ityp", output.Prefix)); 
                    itypFilename = SIO.Path.Combine(output.OutputDirectory, itypFilename);

                    itypSerialiser = new SceneSerialiserITYP(branch, scenes.ToArray(), output.ExportEntities, 
                        assetCollection, interiorNameMap, options.AudioCollisionMapFilename,
                        coreArchetypeObjects);

                    result &= itypSerialiser.Process(itypFilename);
                    result &= itypSerialiser.Serialise(itypFilename);
                    result &= output.ForcedDependencies.Any() ? itypSerialiser.WriteManifestData(xmlManifestAdditions, output.ForcedDependencies) : itypSerialiser.WriteManifestData(xmlManifestAdditions);

                    log.ProfileEnd();
                }

                if (output.ExportEntities)
                {
                    log.ProfileCtx(LOG_CTX, "Generate IMAP data.");
                    if (SIO.Directory.Exists(output.OutputDirectory))
                    {
                        foreach (string existingImapFile in SIO.Directory.EnumerateFiles(
                            output.OutputDirectory, "*.imap", SIO.SearchOption.TopDirectoryOnly))
                        {
                            SIO.File.Delete(existingImapFile);
                        }
                    }

                    string imapFilename = MapAsset.AppendMapPrefixIfRequired(branch.Project, string.Format("{0}.imap", output.Prefix));
                    imapFilename = SIO.Path.Combine(output.OutputDirectory, imapFilename);

                    ITYPContainer itypContainer = null;
                    ITYPContainer itypStreamedContainer = null;
                    if (output.ExportArchetypes)
                    {
                        itypContainer = itypSerialiser.Container;
                        if (itypSerialiser.StreamedContainer.ArchetypeCount > 0)
                            itypStreamedContainer = itypSerialiser.StreamedContainer;
                        // We need to avoid dependencies on data that won't be generated - B* 628077
                    }
                    SceneSerialiserIMAP imapSerialiser = new SceneSerialiserIMAP(branch, scenes.ToArray(), itypContainer, itypStreamedContainer, output.OutputDirectory, branch.Build, interiorNameMap, options.NonSerialisedTimeCycleTypes);

                    //Convert the metadata serialiser's manifest dependency to the native 
                    //class for manifest serialisation.
                    List<ManifestDependency> manifestDependencies = new List<ManifestDependency>();
                    foreach (MapMetadataSerialiserManifestDependency additionalDep in task.AdditionalManifestDependencies)
                    {
                        if (string.IsNullOrWhiteSpace(additionalDep.InputWildcard) == false)
                        {
                            string[] imapFiles = SIO.Directory.GetFiles(additionalDep.IMAPPath, additionalDep.InputWildcard);
                            foreach (string imapFile in imapFiles)
                            {
                                ManifestDependency dependency = new ManifestDependency(imapFile, additionalDep.ITYPDependencies, additionalDep.IsInterior);
                                manifestDependencies.Add(dependency);
                            }
                        }
                        else
                        {
                            ManifestDependency dependency = new ManifestDependency(additionalDep.IMAPPath, additionalDep.ITYPDependencies, additionalDep.IsInterior);
                            manifestDependencies.Add(dependency);
                        }
                    }

                    foreach (ManifestDependency manifestDependency in manifestDependencies)
                    {
                        if (manifestDependency.ITYPDependencies.Length == 0)
                        {
                            IMAPFile imapFile = IMAPFile.Load(manifestDependency.IMAPFile, null);
                            manifestDependency.ITYPDependencies = imapFile.PhysicsDictionaries.ToArray();
                        }
                    }

                    result &= imapSerialiser.Process(imapFilename);
                    result &= imapSerialiser.Serialise(imapFilename);
                    result &= imapSerialiser.WriteManifestData(xmlManifestAdditions, manifestDependencies);

                    if (output.Preview)
                    {
                        IMAPPreview.GenerateIMAPsForPreview_GTA5(branch, output.OutputDirectory, tree, scenes.First(), imapSerialiser);
                    }

                    log.ProfileEnd();
                }

                Manifest.SaveManifestFile(xmlManifestAdditions, task.MapExportAdditionsFilename);

                // Reset the scenes back to an "independent" platform.
                foreach (Scene scene in scenes)
                    scene.SetSceneTarget(null);
            }

            return result;
        }

        private static bool ProcessMapMerge_GTA5(IUniversalLog log, IBranch branch, IContentTree tree, MapMetadataSerialiserOptions options, MapMetadataSerialiserMergeTask mergeTask, SceneCollection sceneCollection)
        {
            log.MessageCtx(LOG_CTX, "Processing metadata merge '{0}'", mergeTask.Name);

            string[] allInputScenePathnames = mergeTask.InputGroups.SelectMany(inputGroup => inputGroup.SceneFilenames).ToArray();
            XDocument xmlManifestAdditions = Manifest.CreateManifestFile(allInputScenePathnames);

            // Load the asset collection
            AssetFile assetCollection = CreateAssetFile(mergeTask.AssetCombineFilename, log, branch);

            bool result = true;

            // Core Game Archetype Cache.
            IDictionary<string, string> coreArchetypeCache = new Dictionary<string, string>();
            IDictionary<String, IEnumerable<ArchetypeLite>> coreArchetypeObjects = 
                new Dictionary<String, IEnumerable<ArchetypeLite>>();
            if (branch.Project.IsDLC)
            {
                coreArchetypeCache = LoadCoreGameArchetypes_GTA5(log, mergeTask.CoreGameMetadataZipDependencies,
                    out coreArchetypeObjects);
                log.Message("Core Archetypes Loaded: {0}.", coreArchetypeCache.Count);
            }

            foreach (MapMetadataSerialiserOutput output in mergeTask.Output)
            {
                // Bail out if we have any errors.
                if (!result)
                    break;

                ITarget target = PlatformPathConversion.GetTargetFromRagebuilderPlatform(branch, output.Platform);

                // Prepare output
                if (!SIO.Directory.Exists(output.OutputDirectory))
                    SIO.Directory.CreateDirectory(output.OutputDirectory);

                if (SIO.Directory.Exists(output.OutputDirectory))
                {
                    foreach (string existingItypFile in SIO.Directory.EnumerateFiles(
                        output.OutputDirectory, "*.ityp", SIO.SearchOption.TopDirectoryOnly))
                    {
                        SIO.File.Delete(existingItypFile);
                    }

                    foreach (string existingImapFile in SIO.Directory.EnumerateFiles(
                        output.OutputDirectory, "*.imap", SIO.SearchOption.TopDirectoryOnly))
                    {
                        SIO.File.Delete(existingImapFile);
                    }
                }

                // Process all input groups.  Here do the usual processing to produce ITYPs and IMAPs,
                // but we don't serialise anything.

                log.MessageCtx(LOG_CTX, "Processing {0} input groups", mergeTask.InputGroups.Count());
                Dictionary<MapMetadataSerialiserMergeInputGroup, MergeSplitInputNode> inputGroupMap =
                    new Dictionary<MapMetadataSerialiserMergeInputGroup, MergeSplitInputNode>();
                foreach (MapMetadataSerialiserMergeInputGroup inputGroup in mergeTask.InputGroups)
                {
                    // Load scene collection
                    ICollection<Scene> inputScenes = LoadScenes(inputGroup.SceneFilenames, sceneCollection, log);

                    // Set the scene platform before any attributes will be queried.
                    foreach (Scene scene in inputScenes)
                        scene.SetSceneTarget(target);

                    // Map Metadata
                    InteriorContainerNameMap interiorNameMap = new InteriorContainerNameMap(CoreInteriorsNamesMap);
                    SceneSerialiserITYP itypSerialiser = null;
                    log.ProfileCtx(LOG_CTX, "Generate ITYP data.");

                    string itypFilename = MapAsset.AppendMapPrefixIfRequired(branch.Project, string.Format("{0}.ityp", inputGroup.Prefix));
                    itypFilename = SIO.Path.Combine(output.OutputDirectory, itypFilename);

                    itypSerialiser = new SceneSerialiserITYP(branch, inputScenes.ToArray(), output.ExportEntities, 
                        assetCollection, interiorNameMap, options.AudioCollisionMapFilename,
                        coreArchetypeObjects);
                    result &= itypSerialiser.Process(itypFilename);
                    log.ProfileEnd();

                    log.ProfileCtx(LOG_CTX, "Generate IMAP data.");

                    string imapFilename = MapAsset.AppendMapPrefixIfRequired(branch.Project, string.Format("{0}.imap", inputGroup.Prefix));
                    imapFilename = SIO.Path.Combine(output.OutputDirectory, imapFilename);

                    ITYPContainer itypContainer = null;
                    ITYPContainer itypPermanentContainer = null;
                    ITYPContainer itypStreamedContainer = null;
                    if (output.ExportArchetypes)
                    {
                        itypContainer = itypSerialiser.Container;
                        if (itypSerialiser.PermanentContainer.ArchetypeCount > 0)
                            itypPermanentContainer = itypSerialiser.PermanentContainer;
                        if (itypSerialiser.StreamedContainer.ArchetypeCount > 0)
                            itypStreamedContainer = itypSerialiser.StreamedContainer;
                        // We need to avoid dependencies on data that won't be generated - B* 628077
                    }

                    SceneSerialiserIMAP imapSerialiser = new SceneSerialiserIMAP(branch,
                        inputScenes.ToArray(), itypContainer, itypStreamedContainer,
                        output.OutputDirectory, branch.Build, interiorNameMap, options.NonSerialisedTimeCycleTypes);

                    result &= imapSerialiser.Process(imapFilename);

                    if (output.Preview)
                    {
                        IMAPPreview.GenerateIMAPsForPreview_GTA5(branch, output.OutputDirectory, tree, inputScenes.First(), imapSerialiser);
                    }

                    log.ProfileEnd();

                    MergeSplitInputNode inputGroupData = new MergeSplitInputNode(
                        new ITYPSerialiserNode(itypSerialiser, itypFilename),
                        new IMAPSerialiserNode(imapSerialiser, imapFilename));
                    inputGroupMap.Add(inputGroup, inputGroupData);

                    // Reset the scenes back to an "independent" platform.
                    foreach (Scene scene in inputScenes)
                        scene.SetSceneTarget(null);
                }

                // Merge the ITYP data
                log.MessageCtx(LOG_CTX, "Carrying out Merge/Split on ITYP data");
                MergeSplitOutputNode mergeOutputNode = ITYPMergeSplitController.MergeSplit(
                    log, branch, mergeTask.Name, output.OutputDirectory, inputGroupMap.Values, coreArchetypeObjects);

                // Write out the data
                log.MessageCtx(LOG_CTX, "Serialising Merged/Split ITYP data");
                mergeOutputNode.ITYPContainerBuilder.Serialise();
                mergeOutputNode.ITYPContainerBuilder.WriteManifestData(xmlManifestAdditions);

                log.MessageCtx(LOG_CTX, "Serialising unmerged ITYP data");
                foreach (ITYPSerialiserNode itypSerialiserNode in mergeOutputNode.ITYPSerialiserNodes)
                {
                    itypSerialiserNode.Serialise();
                    itypSerialiserNode.WriteManifestData(xmlManifestAdditions);
                }

                log.MessageCtx(LOG_CTX, "Serialising IMAP data");
                foreach (IMAPSerialiserNode imapSerialiserNode in mergeOutputNode.IMAPSerialiserNodes)
                {
                    if (branch.Project.IsDLC)
                        imapSerialiserNode.AddDependenciesForCoreGame(coreArchetypeCache);
                    imapSerialiserNode.Serialise();
                    imapSerialiserNode.WriteManifestData(xmlManifestAdditions);
                }

                log.MessageCtx(LOG_CTX, "Serialising manifest data");
                Manifest.SaveManifestFile(xmlManifestAdditions, mergeTask.MapExportAdditionsFilename);
            }

            return result;
        }

        /// <summary>
        /// Load the core game metadata zip files and cache archetype information; this is used
        /// for specifying ITYP dependencies.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="coreMetadataZipDependencies"></param>
        /// <param name="coreGameArchetypeLites"></param>
        /// <returns></returns>
        private static IDictionary<string, string> LoadCoreGameArchetypes_GTA5(IUniversalLog log,
            IEnumerable<MapMetadataSerialiserCoreGameMetadataZipDependency> coreMetadataZipDependencies,
            out IDictionary<String, IEnumerable<ArchetypeLite>> coreGameArchetypeLites)
        {
            IDictionary<string, string> coreGameArchetypeCache = new Dictionary<string, string>();
            coreGameArchetypeLites = new Dictionary<String, IEnumerable<ArchetypeLite>>();

            foreach (MapMetadataSerialiserCoreGameMetadataZipDependency metadataZipFile in coreMetadataZipDependencies)
            {
                string metadataZipFilename = metadataZipFile.CoreGameMetadataZipFilename;
                log.Message("Loading Core Metadata Zip data: {0}.", metadataZipFilename);
                if (!SIO.File.Exists(metadataZipFilename))
                {
                    log.Warning("Core Metadata Zip file does not exist locally: {0}.", metadataZipFilename);
                    continue;
                }

                using (ZipArchive zf = ZipFile.OpenRead(metadataZipFilename))
                {
                    // initializing metadata names for name mapping of interiors from core game
                    if (CoreInteriorsNamesMap == null)
                        CoreInteriorsNamesMap = new Dictionary<InteriorContainerNameMap.InteriorKey, string>();

                    foreach (ZipArchiveEntry ze in zf.Entries)
                    {
                        PopulateCoreInteriorsNames(ze);

                        if (!ze.FullName.EndsWith(".ityp"))
                            continue; // Skip non-ITYP files.

                        log.Message("\tLoading ITYP: {0}", ze.FullName);
                        using (SIO.Stream itypStream = ze.Open())
                        {
                            string itypBasename = SIO.Path.GetFileNameWithoutExtension(ze.FullName);
                            SceneDeserialiserITYP deserialiser = new SceneDeserialiserITYP(itypStream);
                            coreGameArchetypeLites.Add(itypBasename, deserialiser.ArchetypeObjects);
                            
                            foreach (string archetype in deserialiser.Archetypes)
                            {
                                string archetypeName = archetype.ToLower();
                                if (coreGameArchetypeCache.ContainsKey(archetypeName))
                                {
                                    log.Error("Duplicate archetype name found: '{0}' in '{1}' and '{2}'.",
                                        archetypeName, coreGameArchetypeCache[archetypeName], itypBasename);
                                }
                                else
                                {
                                    coreGameArchetypeCache.Add(archetypeName, itypBasename);
                                }
                            }

                            foreach (string interiorArchetype in deserialiser.InteriorArchetypes)
                            {
                                string archetypeName = interiorArchetype.ToLower();
                                if (coreGameArchetypeCache.ContainsKey(archetypeName))
                                {
                                    log.Error("Duplicate interior archetype name found: '{0}' in '{1}' and '{2}'.",
                                        archetypeName, coreGameArchetypeCache[archetypeName], itypBasename);
                                }
                                else
                                {
                                    coreGameArchetypeCache.Add(archetypeName, itypBasename);
                                }
                            }

                            foreach (string timeArchetype in deserialiser.TimeArchetypes)
                            {
                                string archetypeName = timeArchetype.ToLower();
                                if (coreGameArchetypeCache.ContainsKey(archetypeName))
                                {
                                    log.Error("Duplicate time archetype name found: '{0}' in '{1}' and '{2}'.",
                                        archetypeName, coreGameArchetypeCache[archetypeName], itypBasename);
                                }
                                else
                                {
                                    coreGameArchetypeCache.Add(archetypeName, itypBasename);
                                }
                            }
                        }
                    }
                }
            }

            return (coreGameArchetypeCache);
        }

        private static readonly Regex interiorRegex = new Regex(@"interior_[0-9]*_*(\w+)+");//(\w)*");

        private static void PopulateCoreInteriorsNames(ZipArchiveEntry ze)
        {
            string metadataName = SIO.Path.GetFileNameWithoutExtension(ze.FullName);
            if(string.IsNullOrEmpty(metadataName))
                return;

            Match match = interiorRegex.Match(metadataName);
            if (match.Success)
            {
                string objectName = match.Groups[1].Value;
                float x = 0f, y = 0f, z = 0f;

                ExtractPosition(ze, ref x, ref y, ref z);

                InteriorContainerNameMap.InteriorKey interiorKey = new InteriorContainerNameMap.InteriorKey(objectName, x, y, z);

                if (CoreInteriorsNamesMap.ContainsKey(interiorKey))
                    return;
                CoreInteriorsNamesMap.Add(interiorKey, match.Groups[0].Value);
            }
        }

        /// <summary>
        /// opens the imap as a stream
        /// </summary>
        /// <param name="ze"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        private static void ExtractPosition(ZipArchiveEntry ze, ref float x, ref float y, ref float z)
        {
            // open the imap and lookup the Milo instance definition
            using (SIO.Stream imapStream = ze.Open())
            {
                XDocument imap = XDocument.Load(imapStream);
                XElement cmapData = imap.Element("CMapData");
                if (cmapData == null)
                {
                    // log error
                    return;
                }

                XElement entities = cmapData.Element("entities");
                if (entities == null)
                {
                    // log error
                    return;
                }

                XElement item = entities.Element("Item");
                if (item == null)
                {
                    // log error
                    return;
                }

                if (item.Attribute("type").Value != "CMloInstanceDef")
                {
                    // log error
                    return;
                }

                XElement position = item.Element("position");
                if (position == null)
                {
                    // log error
                    return;
                }

                float.TryParse(position.Attribute("x").Value, out x);
                float.TryParse(position.Attribute("y").Value, out y);
                float.TryParse(position.Attribute("z").Value, out z);
            }
        }
    }
}
