﻿using System;
using System.Collections.Generic;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;
using RSG.SceneXml.MapExport;
using RSG.SceneXml.MapExport.AssetCombine;
using RSG.SceneXml.MapExport.Project.GTA5_NG.Collections;

namespace RSG.Pipeline.MapExportMetadata.MergeSplit_GTA5_NG
{
    /// <summary>
    /// Static class responsible for coordinating the ITYP Merge/Split
    /// </summary>
    internal static class ITYPMergeSplitController
    {
        internal static MergeSplitOutputNode MergeSplit(IUniversalLog log, 
                                                        IBranch branch, 
                                                        String baseITYPName, 
                                                        String outputDirectory, 
                                                        IEnumerable<MergeSplitInputNode> inputNodes,
            IDictionary<String, IEnumerable<ArchetypeLite>> coreArchetypeObjects)
        {
            // The assetFile and modelAudioCollisionCollection are required during serialisation (which is bad).
            // It would be ideal to move these requirements to the processing stage.
            AssetFile assetFile = inputNodes.First().ITYPSerialiserNode.AssetFile;
            ModelAudioCollisionCollection modelAudioCollisionCollection = inputNodes.First().ITYPSerialiserNode.ModelAudioCollisionCollection;

            ITYPContainerBuilder itypContainerBuilder = new ITYPContainerBuilder(
                branch, assetFile, modelAudioCollisionCollection, outputDirectory, baseITYPName,
                coreArchetypeObjects);

            MergeSplitInputNode[] lodInputNodes = inputNodes.Where(inputNode => inputNode.IsLOD).ToArray();
            MergeSplitInputNode[] regularInputNodes = inputNodes.Where(inputNode => !inputNode.IsLOD).ToArray();
            MergeStandardContainers(log, regularInputNodes, itypContainerBuilder);
            MergeStreamedContainers(log, regularInputNodes, itypContainerBuilder);
            MergePermanentContainers(log, regularInputNodes, itypContainerBuilder);
            MergeInSituInteriorContainers(log, regularInputNodes, itypContainerBuilder);

            log.Message("ITYP merge created {0} standard ITYP files from {1} inputs",
                itypContainerBuilder.StandardContainerCount, inputNodes.Count());
            log.Message("ITYP merge created {0} streamed ITYP files from {1} inputs",
                itypContainerBuilder.StreamedContainerCount, inputNodes.Count());
            log.Message("ITYP merge created {0} permanent ITYP files from {1} inputs",
                itypContainerBuilder.PermanentContainerCount, inputNodes.Count());

            return new MergeSplitOutputNode(itypContainerBuilder, 
                                            inputNodes.Select(mergeInputNode => mergeInputNode.IMAPSerialiserNode),
                                            lodInputNodes.Select(lodInputNode => lodInputNode.ITYPSerialiserNode));
        }

        /// <summary>
        /// Merges the standard ITYP containers from MergeSplitInputNode objects into a ITYPContainerBuilder object.
        /// This duplicates some code from MergeStreamedContainers() but is also subly different.
        /// Refactoring has been avoided to keep the code clear at the cost of maintenance problems.
        /// </summary>
        private static void MergeStandardContainers(IUniversalLog log, 
                                                    IEnumerable<MergeSplitInputNode> inputNodes, 
                                                    ITYPContainerBuilder itypContainerBuilder)
        {
            log.Message("Merging standard containers");

            List<MergeSplitInputNode> remainingInputNodes = new List<MergeSplitInputNode>(inputNodes);
            while (remainingInputNodes.Count > 0)
            {
                log.Message("{0} inputs remain", remainingInputNodes.Count);

                // 1) Add the largest input
                MergeSplitInputNode initialInputNode = remainingInputNodes.OrderByDescending(
                    inputNode => ITYPCostEstimator.EstimateContainerContentCost(inputNode.ITYPSerialiserNode.Container)).First();
                double initialInputSize = ITYPCostEstimator.EstimateContainerContentCost(initialInputNode.ITYPSerialiserNode.Container);
                if (initialInputSize <= standardITYPSplitLimit_)
                {
                    log.Message("Adding '{0}' to new merged container", initialInputNode.ITYPSerialiserNode.Container.Name);
                    itypContainerBuilder.AppendStandardContainer(initialInputNode);
                    remainingInputNodes.Remove(initialInputNode);

                    // 2) Sort what's left by how close it is to our initial input (to keep ITYP containers that have as low a range as possible)
                    Queue<MergeSplitInputNode> sortedRemainingMergeInputQueue = new Queue<MergeSplitInputNode>(
                        remainingInputNodes.OrderBy(mergeInputNode => (initialInputNode.BoundsXY.Centre() - mergeInputNode.BoundsXY.Centre()).Magnitude()));

                    // 3) Merge till it's full
                    bool containerFull = false;
                    while (!containerFull && sortedRemainingMergeInputQueue.Count > 0)
                    {
                        // Is a merge ok?
                        MergeSplitInputNode inputNode = sortedRemainingMergeInputQueue.First();
                        double sizeAfterMerge = itypContainerBuilder.PendingStandardContainerSize +
                            ITYPCostEstimator.EstimateContainerContentCost(inputNode.ITYPSerialiserNode.Container);
                        if (sizeAfterMerge < standardITYPMergeLimit_)
                        {
                            log.Message("Adding '{0}' to existing merged container", inputNode.ITYPSerialiserNode.Container.Name);
                            itypContainerBuilder.AppendStandardContainer(sortedRemainingMergeInputQueue.Dequeue());
                            remainingInputNodes.Remove(inputNode);
                        }
                        else
                        {
                            log.Message("Not adding '{0}' to existing merged container would break the size limit ({1} bytes).",
                                inputNode.ITYPSerialiserNode.Container.Name, standardITYPMergeLimit_);
                            itypContainerBuilder.FinaliseStandardContainer();
                            containerFull = true;
                        }
                    }

                    // Finalise any 'straggler'
                    itypContainerBuilder.FinaliseStandardContainer();
                }
                else
                {
                    log.Message("{0} is already greater than the size limit ({1} bytes).  This container will be split.",
                        initialInputNode.ITYPSerialiserNode.Container.Name, standardITYPSplitLimit_);
                    itypContainerBuilder.AppendOversizeStandardContainer(initialInputNode, standardITYPSplitLimit_);
                    remainingInputNodes.Remove(initialInputNode);
                }
            }
        }

        /// <summary>
        /// Merges the streamed ITYP containers from MergeSplitInputNode objects into a ITYPContainerBuilder object.
        /// This duplicates some code from MergeStandardContainers() but is also subly different.
        /// Refactoring has been avoided to keep the code clear at the cost of maintenance problems.
        /// </summary>
        private static void MergeStreamedContainers(IUniversalLog log, 
                                                    IEnumerable<MergeSplitInputNode> inputNodes, 
                                                    ITYPContainerBuilder itypContainerBuilder)
        {
            log.Message("Merging streamed containers");

            List<MergeSplitInputNode> remainingInputNodes = new List<MergeSplitInputNode>(inputNodes);
            while (remainingInputNodes.Count > 0)
            {
                log.Message("{0} inputs remain", remainingInputNodes.Count);

                // 1) Add the largest input
                MergeSplitInputNode initialInputNode = remainingInputNodes.OrderByDescending(
                    inputNode => ITYPCostEstimator.EstimateContainerContentCost(inputNode.ITYPSerialiserNode.StreamedContainer)).First();
                double initialInputSize = ITYPCostEstimator.EstimateContainerContentCost(
                    initialInputNode.ITYPSerialiserNode.StreamedContainer);
                log.Message("Adding '{0}' to new merged container", initialInputNode.ITYPSerialiserNode.StreamedContainer.Name);
                itypContainerBuilder.AppendStreamedContainer(initialInputNode);
                remainingInputNodes.Remove(initialInputNode);

                // 2) Sort what's left by how close it is to our initial input (to keep ITYP containers that have as low a range as possible)
                Queue<MergeSplitInputNode> sortedRemainingMergeInputQueue = new Queue<MergeSplitInputNode>(
                    remainingInputNodes.OrderBy(inputNode => (initialInputNode.BoundsXY.Centre() - inputNode.BoundsXY.Centre()).Magnitude()));

                // 3) Merge till it's full
                bool containerFull = false;
                while (!containerFull && sortedRemainingMergeInputQueue.Count > 0)
                {
                    // Is a merge ok?
                    MergeSplitInputNode inputNode = sortedRemainingMergeInputQueue.Peek();
                    double sizeAfterMerge = itypContainerBuilder.PendingStreamedContainerSize +
                        ITYPCostEstimator.EstimateContainerContentCost(inputNode.ITYPSerialiserNode.StreamedContainer);
                    if (sizeAfterMerge < streamedITYPMergeLimit_)
                    {
                        log.Message("Adding '{0}' to existing merged container", inputNode.ITYPSerialiserNode.StreamedContainer.Name);

                        MergeSplitInputNode next = sortedRemainingMergeInputQueue.Dequeue();

                        itypContainerBuilder.AppendStreamedContainer(next);
                        remainingInputNodes.Remove(next);
                    }
                    else
                    {
                        log.Message("Not adding '{0}' to existing merged container would break the size limit ({1} bytes).",
                            inputNode.ITYPSerialiserNode.StreamedContainer.Name, streamedITYPMergeLimit_);
                        itypContainerBuilder.FinaliseStreamedContainer();
                        containerFull = true;
                    }
                }

                // Finalise any 'straggler'
                itypContainerBuilder.FinaliseStreamedContainer();
            }
        }
        
        /// <summary>
        /// Merges the permanent ITYP containers from MergeSplitInputNode objects into a ITYPContainerBuilder object.
        /// This just takes all permanent archetypes into a single ITYP file.
        /// </summary>
        private static void MergePermanentContainers(IUniversalLog log,
                                                    IEnumerable<MergeSplitInputNode> inputNodes,
                                                    ITYPContainerBuilder itypContainerBuilder)
        {
            log.Message("Merging permanent containers");

            foreach (MergeSplitInputNode inputNode in inputNodes)
            {
                itypContainerBuilder.AppendPermanentContainer(inputNode);
            }
            itypContainerBuilder.FinalisePermanentContainer();
        }

        private static void MergeInSituInteriorContainers(IUniversalLog log, 
                                                          IEnumerable<MergeSplitInputNode> inputNodes, 
                                                          ITYPContainerBuilder itypContainerBuilder)
        {
            foreach (MergeSplitInputNode inputNode in inputNodes)
            {
                foreach (KeyValuePair<String, ITYPContainer> interiorContainerPair in inputNode.ITYPSerialiserNode.InteriorContainerMap)
                {
                    itypContainerBuilder.AppendInteriorContainer(interiorContainerPair.Value);
                }
            }
        }

        private static readonly double standardITYPSplitLimit_ = 128 * 1024;
        private static readonly double standardITYPMergeLimit_ = 32 * 1024;
        private static readonly double streamedITYPMergeLimit_ = 32 * 1024;
        private static readonly double permanentITYPMergeLimit_ = 128 * 1024;
    }
}
