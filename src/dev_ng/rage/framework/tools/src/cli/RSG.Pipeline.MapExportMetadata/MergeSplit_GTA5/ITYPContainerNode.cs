﻿using System;
using RSG.SceneXml.MapExport.Project.GTA5.Collections;

namespace RSG.Pipeline.MapExportMetadata.MergeSplit_GTA5
{
    /// <summary>
    /// This class acts as a composite of a ITYPContainer object and merge/split related data
    /// </summary>
    internal class ITYPContainerNode
    {
        internal ITYPContainerNode(ITYPContainer container, String pathname)
        {
            Container = container;
            Pathname = pathname;
        }

        internal ITYPContainer Container { get; private set; }
        internal String Pathname { get; private set; }
    }
}
