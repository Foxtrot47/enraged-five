﻿using System;
using System.Collections.Generic;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services.Metadata;
using RSG.SceneXml;
using RSG.SceneXml.MapExport.AssetCombine;
using RSG.SceneXml.MapExport.Util;
using SIO = System.IO;

namespace RSG.Pipeline.MapExportMetadata
{

    /// <summary>
    /// Map metadata serialiser; providing a single static method that does 
    /// executes the metadata serialisation task.
    /// </summary>
    internal static partial class MetadataSerialiser
    {
        #region Constants
        private static readonly String LOG_CTX = "Map Metadata";
        #endregion // Constants
        
        /// <summary>
        /// Process a metadata serialisation task.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="branch"></param>
        /// <param name="tree"></param>
        /// <param name="task"></param>
        /// <param name="sceneCollection"></param>
        /// <returns></returns>
        public static bool ProcessMap(IUniversalLog log, IBranch branch, IContentTree tree,
            MapMetadataSerialiserOptions options, MapMetadataSerialiserTask task, SceneCollection sceneCollection)
        {
            // Todo Flo: comments
            string serialiserName = branch.GetPipelineOption("RSG.Pipeline.MapExportMetadata.MetadataSerialiser", "Serialiser Selection", "GTA5_NG");

            if (serialiserName == "GTA5")
            {
                return ProcessMap_GTA5(log, branch, tree, options, task, sceneCollection);
            }

            return ProcessMap_GTA5_NG(log, branch, tree, options, task, sceneCollection);
        }

        /// <summary>
        /// This is the top level method for processing a map metadata merge
        /// </summary>
        public static bool ProcessMapMerge(IUniversalLog log, IBranch branch, IContentTree tree,
            MapMetadataSerialiserOptions options, MapMetadataSerialiserMergeTask mergeTask, SceneCollection sceneCollection)
        {
            // Todo Flo: comments
            string serialiserName = branch.GetPipelineOption("RSG.Pipeline.MapExportMetadata.MetadataSerialiser", "Serialiser Selection", "GTA5_NG");

            if (serialiserName == "GTA5")
            {
                return ProcessMapMerge_GTA5(log, branch, tree,options, mergeTask, sceneCollection);
            }

            return ProcessMapMerge_GTA5_NG(log, branch, tree, options, mergeTask, sceneCollection);
        }
        
        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="assetCombinePathname"></param>
        /// <param name="log"></param>
        /// <param name="branch"></param>
        /// <returns></returns>
        private static AssetFile CreateAssetFile(String assetCombinePathname, IUniversalLog log, IBranch branch)
        {
            AssetFile assetFile = null;

            if (!String.IsNullOrEmpty(assetCombinePathname) && SIO.File.Exists(assetCombinePathname))
            {
                log.MessageCtx(LOG_CTX, "Loading Asset Combine data from '{0}'.", assetCombinePathname);
                assetFile = new AssetFile(branch, assetCombinePathname);
            }
            else if (!String.IsNullOrWhiteSpace(assetCombinePathname))
            {
                log.WarningCtx(LOG_CTX, "Asset Combine input file does not exist: '{0}'.", assetCombinePathname);
            }

            return assetFile;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scenePathnames"></param>
        /// <param name="sceneCollection"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        private static ICollection<Scene> LoadScenes(IEnumerable<String> scenePathnames, SceneCollection sceneCollection, IUniversalLog log)
        {
            ICollection<Scene> scenes = new List<Scene>();
            foreach (String scenePathname in scenePathnames)
            {
                Scene inputScene = sceneCollection.LoadScene(scenePathname);
                if (null != inputScene)
                {
                    ApplySceneOverrides(inputScene, log);

                    scenes.Add(inputScene);
                    log.MessageCtx(LOG_CTX, "Loaded scene at '{0}'", scenePathname);
                }
                else
                {
                    log.ErrorCtx(LOG_CTX, "Failed to load scene at '{0}'.", scenePathname);
                }
            }

            return scenes;
        }

        /// <summary>
        /// Apply any scene overrides.  We currently only apply attribute changes here.
        /// However, we may want to move towards applying LOD overrides and deletes here as it's cleaner
        /// to have all this wickedness in one place.
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="log"></param>
        private static void ApplySceneOverrides(Scene scene, IUniversalLog log)
        {
            foreach (TargetObjectDef objectDef in scene.Walk(Scene.WalkMode.DepthFirst))
            {
                if (objectDef.IsObject())
                {
                    if (SceneOverrideIntegrator.Instance.HasAttributeOverridesForObject(objectDef))
                    {
                        bool dontCastShadows = SceneOverrideIntegrator.Instance.GetDontCastShadowsAttributeValue(objectDef);
                        bool dontRenderInShadows = SceneOverrideIntegrator.Instance.GetDontRenderInShadowsAttributeValue(objectDef);
                        bool dontRenderInReflections = SceneOverrideIntegrator.Instance.GetDontRenderInReflectionsAttributeValue(objectDef);
                        bool onlyRenderInReflections = SceneOverrideIntegrator.Instance.GetOnlyRenderInReflectionsAttributeValue(objectDef);
                        bool dontRenderInWaterReflections = SceneOverrideIntegrator.Instance.GetDontRenderInWaterReflectionsAttributeValue(objectDef);
                        bool onlyRenderInWaterReflections = SceneOverrideIntegrator.Instance.GetOnlyRenderInWaterReflectionsAttributeValue(objectDef);
                        bool dontRenderInMirrorReflections = SceneOverrideIntegrator.Instance.GetDontRenderInMirrorReflectionsAttributeValue(objectDef);
                        bool onlyRenderInMirrorReflections = SceneOverrideIntegrator.Instance.GetOnlyRenderInMirrorReflectionsAttributeValue(objectDef);
                        bool streamingPriorityLow = SceneOverrideIntegrator.Instance.GetStreamingPriorityLowAttributeValue(objectDef);
                        int priority = SceneOverrideIntegrator.Instance.GetPriorityAttributeValue(objectDef);

                        objectDef.SetAttribute(AttrNames.OBJ_DONT_CAST_SHADOWS, dontCastShadows);
                        objectDef.SetAttribute(AttrNames.OBJ_DONT_RENDER_IN_SHADOWS, dontRenderInShadows);
                        objectDef.SetAttribute(AttrNames.OBJ_DONT_RENDER_IN_REFLECTIONS, dontRenderInReflections);
                        objectDef.SetAttribute(AttrNames.OBJ_ONLY_RENDER_IN_REFLECTIONS, onlyRenderInReflections);
                        objectDef.SetAttribute(AttrNames.OBJ_DONT_RENDER_IN_WATER_REFLECTIONS, dontRenderInWaterReflections);
                        objectDef.SetAttribute(AttrNames.OBJ_ONLY_RENDER_IN_WATER_REFLECTIONS, onlyRenderInWaterReflections);
                        objectDef.SetAttribute(AttrNames.OBJ_DONT_RENDER_IN_MIRROR_REFLECTIONS, dontRenderInMirrorReflections);
                        objectDef.SetAttribute(AttrNames.OBJ_ONLY_RENDER_IN_MIRROR_REFLECTIONS, onlyRenderInMirrorReflections);
                        objectDef.SetAttribute(AttrNames.OBJ_LOW_PRIORITY, streamingPriorityLow);
                        objectDef.SetAttribute(AttrNames.OBJ_PRIORITY, priority);

                        log.MessageCtx(LOG_CTX, "Overriding attributes for '{0}':", objectDef.Name);
                        log.MessageCtx(LOG_CTX, "   dontCastShadows: {0}", dontCastShadows);
                        log.MessageCtx(LOG_CTX, "   dontRenderInShadows: {0}", dontRenderInShadows);
                        log.MessageCtx(LOG_CTX, "   dontRenderInReflections: {0}", dontRenderInReflections);
                        log.MessageCtx(LOG_CTX, "   onlyRenderInReflections: {0}", onlyRenderInReflections);
                        log.MessageCtx(LOG_CTX, "   dontRenderInWaterReflections: {0}", dontRenderInWaterReflections);
                        log.MessageCtx(LOG_CTX, "   onlyRenderInWaterReflections: {0}", onlyRenderInWaterReflections);
                        log.MessageCtx(LOG_CTX, "   dontRenderInMirrorReflections: {0}", dontRenderInMirrorReflections);
                        log.MessageCtx(LOG_CTX, "   onlyRenderInMirrorReflections: {0}", onlyRenderInMirrorReflections);
                        log.MessageCtx(LOG_CTX, "   streamingPriorityLow: {0}", streamingPriorityLow);
                        log.MessageCtx(LOG_CTX, "   priority: {0}", priority);
                    }
                }
            }
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.MapExportMetadata namespace
