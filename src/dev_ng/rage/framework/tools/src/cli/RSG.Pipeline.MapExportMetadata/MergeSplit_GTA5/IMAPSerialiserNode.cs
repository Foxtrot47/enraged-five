﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.SceneXml.MapExport.Project.GTA5;

namespace RSG.Pipeline.MapExportMetadata.MergeSplit_GTA5
{
    /// <summary>
    /// This class acts as a composite of a SceneSerialiserIMAP object and merge/split related data.
    /// It encapsulates the complexity of SceneSerialiserIMAP.
    /// </summary>
    /// DHM TODO: not much of an encapsulation!  Add comments too.
    /// 
    internal class IMAPSerialiserNode
    {
        #region Properties
        /// <summary>
        /// 
        /// </summary>
        internal BoundingBox2f BoundsXY
        {
            get
            {
                return serialiser_.BoundsXY;
            }
        }
        #endregion // Properties

        #region Member Data
        private SceneSerialiserIMAP serialiser_;
        private String pathname_;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serialiser"></param>
        /// <param name="pathname"></param>
        public IMAPSerialiserNode(SceneSerialiserIMAP serialiser, String pathname)
        {
            serialiser_ = serialiser;
            pathname_ = pathname;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        public void ReplaceDependency(String sourceName, String targetName)
        {
            if (String.IsNullOrEmpty(sourceName) || String.IsNullOrEmpty(targetName))
                return;
            
            serialiser_.ReplaceDependency(sourceName, targetName);
        }

        public void ReplaceStaticDependency(String sourceName, IEnumerable<String> targetNames)
        {
            if (String.IsNullOrEmpty(sourceName) || !targetNames.Any())
                return;

            serialiser_.ReplaceStaticDependency(sourceName, targetNames);
        }

        /// <summary>
        /// Add any core-game dependencies into IMAP Containers (forwards
        /// call into SceneSerialiserIMAP).
        /// </summary>
        /// <param name="archetypeToITYPMap"></param>
        public void AddDependenciesForCoreGame(IDictionary<String, String> archetypeToITYPMap)
        {
            serialiser_.AddDependenciesForCoreGame(archetypeToITYPMap);
        }

        /// <summary>
        /// Serialise out our IMAP containers.
        /// </summary>
        public void Serialise()
        {
            Log.Log__Message("Serialising IMAP to '{0}'", pathname_);
            serialiser_.Serialise(pathname_);
        }

        /// <summary>
        /// Serialise manifest data into XDocument.
        /// </summary>
        /// <param name="manifestAdditionsDocument"></param>
        public void WriteManifestData(XDocument manifestAdditionsDocument)
        {
            serialiser_.WriteManifestData(manifestAdditionsDocument, null);
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.MapExportMetadata.MergeSplit namespace
