﻿using System.Linq;
using RSG.SceneXml.MapExport;
using RSG.SceneXml.MapExport.Project.GTA5_NG.Collections;

namespace RSG.Pipeline.MapExportMetadata.MergeSplit_GTA5_NG
{
    /// <summary>
    /// This static class provides a heuristic for estimating the platform size of metadata containers
    /// </summary>
    internal static class ITYPCostEstimator
    {
        internal static double EstimateContainerContentCost(ITYPContainer container)
        {
            int numArchetypes = container.Archetypes.Length;
            Extension[] archetypeExtensions = container.Archetypes.SelectMany(archetype => archetype.Extensions).ToArray();

            // Light extension serialisation is disabled
            // (//rage/gta5/dev/rage/framework/tools/src/Libs/RSG.SceneXml.MapExport/Project/GTA/SceneSerialiserITYP.cs)
            int numArchetypeExtensions = archetypeExtensions.Count(extension => !extension.Object.Is2dfxLightEffect());

            double totalArchetypeCost = (double)numArchetypes * archetypeItemCost_;
            double totalArchetypeExtensionCost = (double)numArchetypeExtensions * archetypeExtensionItemCost_;

            return totalArchetypeCost + totalArchetypeExtensionCost;
        }

        // JWR - this was measured from the existing data:
        // cs1_11       : 1597 items => 337,908 bytes (212 bytes per item)
        // cs1_roads_pb : 1245 items => 261,996 bytes (210 bytes per item)
        // cs1_11       : 1315 items => 275,972 bytes (210 bytes per item)
        // cs1_11       : 1220 items => 258,548 bytes (212 bytes per item)
        private static readonly double archetypeItemCost_ = 214;
        private static readonly double archetypeExtensionItemCost_ = 160;
    }
}
