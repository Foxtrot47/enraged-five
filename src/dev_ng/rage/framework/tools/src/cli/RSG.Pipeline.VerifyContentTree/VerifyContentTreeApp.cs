﻿//---------------------------------------------------------------------------------------------
// <copyright file="VerifyContentTree.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.VerifyContentTree
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using RSG.Base.Configuration;
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;
    using RSG.Base.OS;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Pipeline.Core;
    using RSG.Pipeline.Content;
    
    class VerifyContentTreeApp : RsConsoleApplication
    {
        #region Properties
        /// <summary>
        /// Gets a semi-colon separate list of the application authors email addresses. These
        /// are the addresses which are used by the unhandled exception dialog to determine
        /// where the mail should be sent by default.
        /// e.g.
        /// *tools@rockstarnorth.com
        /// michael.taschler@rockstarnorth.com;dave.evans@rockstarnorth.com
        /// </summary>
        public override string AuthorEmailAddress
        {
            get { return "*tools@rockstarnorth.com"; }
        }
        #endregion // Properties

        #region Entry-Point
        [STAThread]
        static int Main(String[] args)
        {
            VerifyContentTreeApp app = new VerifyContentTreeApp();
            return (app.Run());
        }
        #endregion // Entry-Point

        #region RsConsoleApplication Overridden Methods
        /// <summary>
        /// Program Entry-Point.
        /// </summary>
        /// <returns></returns>
        protected override int ConsoleMain()
        {
            // Construct our CommandOptions object; this isn't yet integrated into the Editor
            // Framework RsConsoleApplication stuff.
            String[] args = Environment.GetCommandLineArgs().Skip(1).ToArray(); // HACK: skip app name.
            CommandOptions options = new CommandOptions(args, new LongOption[0]);

            int exit_code = RSG.Pipeline.Core.Constants.Exit_Success;
            try
            {
                if (options.ShowHelp)
                {
                    ShowUsage();
                }

                try
                {
                    IContentTree contentTree = Factory.CreateTree(options.Branch);
                    ValidateContentTree(this.Log, options, contentTree);
                }
                catch (Exception)
                {
                    this.Log.Error("Failed to load Content-Tree.  See previous errors.");
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Unhandled exception.");
            }
            finally
            {
                if (this.Log.HasErrors)
                    this.Log.Error("Content-Tree validation failed.");
                else
                    this.Log.Message("Content-Tree validation was successful.");

                exit_code = this.Log.HasErrors ? Constants.Exit_Failure : Constants.Exit_Success;
                LogFactory.ApplicationShutdown();
            }
            return (exit_code);
        }
        #endregion // RsConsoleApplication Overridden Methods

        #region Private Methods
        /// <summary>
        /// Validate the on-disk content-tree.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="options"></param>
        /// <param name="contentTree"></param>
        /// <returns></returns>
        private void ValidateContentTree(IUniversalLog log, CommandOptions options, IContentTree contentTree)
        {
            // Load the processors.
            IProcessorCollection processors = new ProcessorCollection(options.Config);

            // Loop through all Content-Tree IProcess Processor Names.
            IEnumerable<String> uniqueProcessors = contentTree.Processes.Select(p => p.ProcessorClassName).Distinct();
            foreach (String processorClassName in uniqueProcessors)
            {
                try
                {
                    IProcessor processor = processors.GetProcessor(processorClassName);
                }
                catch (ArgumentException ex)
                {
                    log.Error(ex.Message);
                }
            }
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.VerifyContentTree namespace
