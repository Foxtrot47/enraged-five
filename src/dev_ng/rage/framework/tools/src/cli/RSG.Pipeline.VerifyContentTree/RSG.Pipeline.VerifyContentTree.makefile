#
# RSG.Pipeline.VerifyContentTree.makefile
#
 
Project RSG.Pipeline.VerifyContentTree

BuildTemplate $(toolsroot)/etc/projgen/RSG.Pipeline.Executable.build
 
ConfigurationType exe
 
FrameworkVersion 4.5
 
OutputPath $(toolsroot)\ironlib\lib\
 
Files {
	VerifyContentTreeApp.cs
	Directory Properties {
			AssemblyInfo.cs
	}
	app.config
	rockstar.ico
}
 
ProjectReferences {
	..\..\..\..\..\base\tools\libs\RSG.Base.Configuration\RSG.Base.Configuration.csproj
	..\..\..\..\..\base\tools\libs\RSG.Base\RSG.Base.csproj
	..\..\..\..\..\base\tools\libs\RSG.Platform\RSG.Platform.csproj
	..\..\..\..\..\base\tools\libs\RSG.Interop.Microsoft\RSG.Interop.Microsoft.csproj
	..\..\ui\Libs\RSG.Editor\RSG.Editor.csproj
	..\..\ui\Libs\RSG.Editor.Controls\RSG.Editor.Controls.csproj
	..\..\Libs\RSG.Pipeline.Core\RSG.Pipeline.Core.csproj
	..\..\Libs\RSG.Pipeline.Content\RSG.Pipeline.Content.csproj
}
References {
	System
	System.Core
	System.Xml.Linq
	System.Data.DataSetExtensions
	System.ComponentModel.Composition
	Microsoft.CSharp
	System.Data
	System.Xml
}
