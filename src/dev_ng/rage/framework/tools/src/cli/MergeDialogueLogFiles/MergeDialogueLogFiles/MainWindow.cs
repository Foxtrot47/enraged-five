﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;

namespace MergeDialogueLogFiles
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnBrowseFolder_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderDialog = new FolderBrowserDialog();
            if (folderDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtInputFolder.Text = folderDialog.SelectedPath;
            }
        }

        private void btnBrowseOutput_Click(object sender, EventArgs e)
        {
            SaveFileDialog fileDialog = new SaveFileDialog();
            fileDialog.DefaultExt = ".csv";
            fileDialog.Filter = "CSV Documents (*.csv)|*.csv";
            if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtOutputFile.Text = fileDialog.FileName;
            }
        }

        private void btnBrowseIgnore_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.DefaultExt = ".csv";
            fileDialog.Filter = "CSV Documents (*.csv)|*.csv";
            if (fileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtIgnoreFile.Text = fileDialog.FileName;
            }
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            // Validate the input fields
            if (String.IsNullOrWhiteSpace(txtInputFolder.Text))
            {
                MessageBox.Show("Must specify an input folder.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (String.IsNullOrWhiteSpace(txtOutputFile.Text))
            {
                MessageBox.Show("Must specify an output file.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            if (!String.IsNullOrWhiteSpace(txtIgnoreFile.Text) && !File.Exists(txtIgnoreFile.Text))
            {
                MessageBox.Show("Ignore file doesn't exist.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            uint minCL = 0;
            if (!String.IsNullOrWhiteSpace(txtMinCL.Text))
            {
                if (!UInt32.TryParse(txtMinCL.Text.Trim(), out minCL))
                {
                    MessageBox.Show("Min CL# doesn't contain a valid number.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }

            // Get the data
            ISet<String> allEntries = GetAllEntries(minCL);
            ISet<String> ignoreEntries = GetEntriesToIgnore();

            // Output the resulting file.
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(txtOutputFile.Text, false))
            {
                file.WriteLine("VoiceName,FileName,SpeakerID");
                foreach (String line in allEntries.Except(ignoreEntries))
                {
                    file.WriteLine(line.ToUpper());
                }
            }

            MessageBox.Show("The merge completed successfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        /// <summary>
        /// 
        /// </summary>
        private ISet<String> GetAllEntries(uint minCL)
        {
            String[] files = Directory.GetFiles(txtInputFolder.Text, "*.csv",
                (chkIncludeSubDirs.CheckState == CheckState.Checked) ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);

            Regex filenameRegex = new Regex(@"^(.*)_(?'cl'\d+)_(.*)$");

            ISet<String> entries = new HashSet<String>();
            foreach (String file in files)
            {
                // Try and extract the changelist number out of the file name.
                uint cl = UInt32.MaxValue;
                Match match = filenameRegex.Match(file);
                if (match.Success)
                {
                    Group clGroup = match.Groups["cl"];
                    if (clGroup.Success)
                    {
                        cl = UInt32.Parse(clGroup.Value);
                    }
                }

                // Check whether we should include these results.
                if (cl >= minCL)
                {
                    foreach (String line in File.ReadAllLines(file).Skip(1))
                    {
                        entries.Add(line.Replace(" ", "").ToLower());
                    }
                }
            }
            return entries;
        }

        /// <summary>
        /// 
        /// </summary>
        private ISet<String> GetEntriesToIgnore()
        {
            ISet<String> entries = new HashSet<String>();
            if (!String.IsNullOrWhiteSpace(txtIgnoreFile.Text))
            {
                foreach (String line in File.ReadAllLines(txtIgnoreFile.Text).Skip(1))
                {
                    entries.Add(line.Replace(" ", "").ToLower());
                }
            }
            return entries;
        }
    }
}
