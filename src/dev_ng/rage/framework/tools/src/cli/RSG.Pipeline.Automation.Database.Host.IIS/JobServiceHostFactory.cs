﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Web;
using RSG.Base.Configuration.Services;
using RSG.Base.Logging;
using RSG.Services.Common;
using RSG.Pipeline.Automation.Database.Common;
using RSG.Pipeline.Automation.Database.Server;

namespace RSG.Pipeline.Automation.Database.Host.IIS
{
    /// <summary>
    /// Service host factory which injects the service configuration object into the service.
    /// </summary>
    public class JobServiceHostFactory : ConfigServiceHostFactory
    {
        /// <summary>
        /// Private field for the <see cref="ServiceHostConfig"/> property.
        /// </summary>
        private static readonly IServiceHostConfig _serviceHostConfig;

        /// <summary>
        /// Service host configuration object to pass into the service.
        /// </summary>
        protected override IServiceHostConfig ServiceHostConfig
        {
            get { return _serviceHostConfig; }
        }

        /// <summary>
        /// Static constructor.
        /// </summary>
        static JobServiceHostFactory()
        {
            // Initialise the server we are using.
            LogFactory.Initialize();

            // Determine the correct server config item to use.
            IJobConfig config = new JobConfig();
            IServer serverConfig = null;

            String serverName = ConfigurationManager.AppSettings["server"];
            if (serverName != null)
            {
                serverConfig = config.Servers[serverName];
            }
            else
            {
                serverConfig = config.DefaultServer;
            }

            // Make sure the config is set up to use IIS.
            Debug.Assert(serverConfig.UsingIIS, "Trying to host a server in IIS that isn't flagged as using IIS.");
            if (!serverConfig.UsingIIS)
            {
                throw new ArgumentException("Trying to host a server in IIS that isn't flagged as using IIS.");
            }

            _serviceHostConfig = serverConfig;
            ServerBootStrapper.Initialise(serverConfig);
        } // DeploymentServiceHostFactory
    }
}