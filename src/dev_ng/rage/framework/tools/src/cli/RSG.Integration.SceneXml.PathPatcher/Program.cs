﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.OS;
using RSG.Pipeline.Core;

namespace RSG.Integration.SceneXml.PathPatcher
{
    internal sealed class Program
    {
        private const string OptFromPath = "from";
        private const string OptToPath = "to";
        private const string OptFilenameList = "files";
        private const string OptDirectory = "dir";

        private static CommandOptions Options;
        private static string From = "";
        private static string To = "";

        [STAThread]
        private static int Main(string[] args)
        {
            LongOption[] options =
            {
                new LongOption(OptFromPath, LongOption.ArgType.Required, "Remap the RsRef from this Path"),
                new LongOption(OptToPath, LongOption.ArgType.Required, "Remap the RsRef to this Path"),
                new LongOption(OptFilenameList, LongOption.ArgType.Optional, "Applied on file(s) - semicolon separated values"),
                new LongOption(OptDirectory, LongOption.ArgType.Optional, "Applied on directory, gets the files from it."),
            };

            Options = new CommandOptions(args, options);

            try
            {
                List<string> files;

                if (Options.ContainsOption(OptFilenameList))
                {
                    string filelist = Options[OptFilenameList].ToString();
                    if (string.IsNullOrWhiteSpace(filelist))
                        return Constants.Exit_Success;

                    // make sure we operate on xml (remove non xml)
                    files = filelist.Split(new[] {';'}, StringSplitOptions.RemoveEmptyEntries).Where(f => f.EndsWith(".xml", StringComparison.OrdinalIgnoreCase)).ToList();
                }
                else
                {
                    // assume it's a directory if no filelist - return success if no file to process
                    if (!Options.ContainsOption(OptDirectory))
                        return Constants.Exit_Success;

                    string dir = Path.GetDirectoryName((string)Options[OptDirectory]);
                    if (string.IsNullOrWhiteSpace(dir))
                        return Constants.Exit_Success;

                    files = Directory.EnumerateFiles(dir, "*.xml", SearchOption.AllDirectories).ToList();
                }

                From = Path.GetFullPath(Options[OptFromPath].ToString());
                To = Path.GetFullPath(Options[OptToPath].ToString());

                // open said file(s)
                foreach (string filepath in files)
                {
                    XDocument sceneXml = XDocument.Load(filepath);

                    // if not found, it's not a scene xml, just bail out
                    XElement scene = sceneXml.Element("scene");
                    if (scene == null)
                        continue;

                    PatchSceneExportFilename(scene);

                    // otherwise, yay, select the actual root objects
                    IEnumerable<XElement> objects = scene.Elements("objects").Elements("object");

                    foreach (var o in objects)
                    {
                        // recursively processes those root objects
                        ProcessObject(o);
                    }
                    sceneXml.Save(filepath);
                }
            }
            catch (Exception)
            {
                return Constants.Exit_Failure;
            }

            return Constants.Exit_Success;
        }

        private static void PatchSceneExportFilename(XElement scene)
        {
            XAttribute filenameAttribute = scene.Attribute("filename");
            if(string.IsNullOrWhiteSpace(filenameAttribute.Value))
                return;

            string filenameValue = Path.GetFullPath(filenameAttribute.Value);
            string changedValue = filenameValue.Replace(From, To, StringComparison.OrdinalIgnoreCase);
            filenameAttribute.SetValue(changedValue);
        }

        private static void ProcessObject(XElement xElement)
        {
            // if the current object is not a ref object, we dont need to remap it
            XAttribute classAttribute = xElement.Attribute("class");

            if(classAttribute != null && classAttribute.Value == "rsrefobject")
            {
                // else, look for the paramblocks, where the actual attributes from the RsRef System are
                XElement paramBlocks = xElement.Element("paramblocks");
                
                if(paramBlocks != null)
                { 
                    IEnumerable<XElement> blocks = paramBlocks.Elements("paramblock");

                    foreach (var block in blocks)
                    {
                        // should contain the parameter element with the filename where the RsRef is pointing to
                        XElement parameter = block.Elements("parameter").FirstOrDefault(p => p.Attribute("name").Value == "filename");

                        if (parameter != null)
                        {
                            XAttribute filenameAttribute = parameter.Attribute("value");
                            if(string.IsNullOrWhiteSpace(filenameAttribute.Value))
                                continue;

                            string filenameValue = Path.GetFullPath(filenameAttribute.Value);
                            string changedValue = filenameValue.Replace(From, To, StringComparison.OrdinalIgnoreCase);
                            filenameAttribute.SetValue(changedValue);
                        }
                    }
                }
            }

            // let's get the children of that object, and let's process them too
            XElement children = xElement.Element("children");
            if (children != null)
            {
                IEnumerable<XElement> childObjects = children.Elements("object");
                foreach (var child in childObjects)
                {
                    ProcessObject(child);
                }
            }
        }
    }
}