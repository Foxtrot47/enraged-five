﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.Animation.Cutscene.CutsceneConcatenation
{
    class Program
    {
        #region Constants
        private static readonly String OPTION_INPUTDIR = "inputdir";
        private static readonly String OPTION_OUTPUTDIR = "outputdir";
        private static readonly String OPTION_AUDIO = "audio";
        private static readonly String OPTION_SECTIONMETHOD = "sectionmethod";
        private static readonly String OPTION_SECTIONDURATION = "sectionduration";
        private static readonly String OPTION_ANIMCONCAT = "animconcat";
        private static readonly String OPTION_CLIPCONCAT = "clipconcat";
        private static readonly String OPTION_CUTCONCAT = "cutconcat";
        private static readonly String OPTION_TASK_NAME = "taskname";
        private static readonly String LOG_CTX = "Cutscene Concatenation";
        #endregion // Constants

        [STAThread]
        static int Main(string[] args)
        {
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.CreateUniversalLog("CutsceneConcatenation");
            try
            {
                // Define our executable options.  Stick with the long options
                // (e.g. --opt) rather than the single character versions.
                Getopt options = new Getopt(args, new LongOption[] {
                    new LongOption(OPTION_INPUTDIR, LongOption.ArgType.Required,
                        "Input directory."),
                    new LongOption(OPTION_OUTPUTDIR, LongOption.ArgType.Required,
                        "Output directory."),
                    new LongOption(OPTION_AUDIO, LongOption.ArgType.Optional,
                        ""),
                    new LongOption(OPTION_SECTIONMETHOD, LongOption.ArgType.Required,
                        ""),
                    new LongOption(OPTION_SECTIONDURATION, LongOption.ArgType.Required,
                        ""),
                    new LongOption(OPTION_ANIMCONCAT, LongOption.ArgType.Required,
                        ""),
                    new LongOption(OPTION_CLIPCONCAT, LongOption.ArgType.Required,
                        ""),
                    new LongOption(OPTION_CUTCONCAT, LongOption.ArgType.Required,
                        ""),
                    new LongOption(OPTION_TASK_NAME, LongOption.ArgType.Required,
                        "")
                });
                // Turn args into vars
                String inputDir = options[OPTION_INPUTDIR] as String;
                String outputDir = options[OPTION_OUTPUTDIR] as String;
                String audioFile = options[OPTION_AUDIO] as String;
                String sectionMethod = options[OPTION_SECTIONMETHOD] as String;
                String sectionDuration = options[OPTION_SECTIONDURATION] as String;
                String animConcatExe = options[OPTION_ANIMCONCAT] as String;
                String clipConcatExe = options[OPTION_CLIPCONCAT] as String;
                String cutConcatExe = options[OPTION_CUTCONCAT] as String;
                String taskName = options[OPTION_TASK_NAME] == null ? String.Empty : options[OPTION_TASK_NAME] as String;

                if (String.Empty != taskName)
                {
                    Console.WriteLine("TASK: {0}", taskName);
                    log.MessageCtx(LOG_CTX, "TASK: {0}", taskName);
                }

                //log.MessageCtx(LOG_CTX, "Creating directory '{0}'", outputDir);
                //if (!RSG.Pipeline.Services.Animation.DirectoryServices.CreateDirectory(outputDir))
                //{
                //    log.ErrorCtx(LOG_CTX, "Failed to create directory: '{0}'.  Aborting.", outputDir);
                //    return (Constants.Exit_Failure);
                //}

                CutsceneConcat c = new CutsceneConcat(cutConcatExe, clipConcatExe, animConcatExe, log);

                string[] folderList = inputDir.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                int iSectionMethod = 0;
                Int32.TryParse(sectionMethod, out iSectionMethod);

                int iSectionDuration = 0;
                Int32.TryParse(sectionDuration, out iSectionDuration);

                if (c.Process(folderList, outputDir, audioFile, iSectionMethod, iSectionDuration) != 0)
                {
                    if (Directory.Exists(outputDir))
                        Directory.Delete(outputDir, true);
                    return (Constants.Exit_Failure);
                }

                return (Constants.Exit_Success);
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Unhandled exception during cutscene concatenation process.");
                return (Constants.Exit_Failure);
            }
            finally
            {
                LogFactory.ApplicationShutdown();
            }
        }
    }
}
