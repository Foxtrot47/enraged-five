﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RSG.Base.Collections;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.ManagedRage;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services.Platform;

namespace RSG.Pipeline.RpfMerge
{
    class Program
    {
        #region Constants

        private const string LoggingContext = "RSG.Pipeline.RpfMerge";
        private const string OptionTaskname = "taskname";
        private const string OptionOutput   = "output";
        private const string OptionReverse  = "reverse";

        #endregion

        /// <summary>
        /// RpfMerge entry point.
        /// </summary>
        /// <param name="args"></param>
        private static int Main(string[] args)
        {
            LongOption[] longOptions =
            {
                new LongOption(OptionOutput, LongOption.ArgType.Required, "Full file path of the resulting merge."),
                new LongOption(OptionTaskname, LongOption.ArgType.Required, "XGE taskname; echoed to TTY for XGE error log parsing in AP3.") 
            };

            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();

            IUniversalLog log = LogFactory.CreateUniversalLog(LoggingContext);
            CommandOptions options = new CommandOptions(args, longOptions);

            // Output task name for AP3 XGE logging compatibility.
            if (options.ContainsOption(OptionTaskname))
            {
                Console.WriteLine("TASK: {0}", options[OptionTaskname]);
                Console.Out.Flush();
            }

            // Verify options.
            if (options.ShowHelp)
            {
                Console.WriteLine(options.GetUsage());
                return Constants.Exit_Success;
            }

            if (!options.ContainsOption(OptionOutput))
            {
                Console.Error.WriteLine("'{0}' is a required option.", OptionOutput);
                Console.WriteLine(options.GetUsage());
                return Constants.Exit_Failure;
            }

            if (options.TrailingArguments.Count() < 2)
            {
                Console.Error.WriteLine("Not enough arguments supplied. You have to supply two or more rpf source.");
                return Constants.Exit_Failure;
            }

            bool reverse = false;
            if (options.ContainsOption(OptionReverse))
                reverse = (bool)options[OptionReverse];

            // Declaring the collection of sourceFiles here as we might want to filter it platform wise beforehand
            IEnumerable<string> sourceFiles = reverse ? options.TrailingArguments.Reverse().ToList() : options.TrailingArguments.ToList();
            foreach (string file in sourceFiles)
            {
                if (File.Exists(file))
                {
                    if(!file.EndsWith(".rpf", StringComparison.OrdinalIgnoreCase))
                        log.ToolErrorCtx(LoggingContext, "File '{0}' is not an RPFs.", file);
                }

                // At least one source file is missing. Logging errors so we can abort the whole operation later on.    
                log.ToolErrorCtx(LoggingContext, "File at {0} is missing.", file);
            }

            if (log.HasErrors)
            {
                return Constants.Exit_Failure;
            }

            // Actual merge. The stuff before was only to validate options and init.
            IEnumerable<Tuple<string, Packfile>> inputRpfs = sourceFiles.Select(filename => new Tuple<string, Packfile>(filename, new Packfile())).ToList();
            foreach (Tuple<string, Packfile> rpf in inputRpfs)
            {
                rpf.Item2.Load(rpf.Item1);
            }

            // Check all inputs platforms against the first one.
            // If there's a mismatch, we abort
            Packfile.etPlatform platform = inputRpfs.First().Item2.Platform;
            foreach (var inputRpf in inputRpfs)
            {
                if (inputRpf.Item2.Platform != platform)
                {
                    log.ErrorCtx(LoggingContext, "Some of the inputs supplied have different platforms. We can't merge RPFs from different platforms.\n"+
                        "\tReference RPF is {0}, while {1} is a {2} rpf.", platform, inputRpf.Item1, inputRpf.Item2.Platform);
                }

                if (inputRpf.Item2.Entries.Any(e => e.ShortName() == "_manifest"))
                {
                    log.ErrorCtx(LoggingContext, "You're trying to merge {0} which contains a manifest. We can't support merging of RPFs with manifest. Aborting.", inputRpf.Item1);
                }
            }

            if (log.HasErrors)
            {
                // Don't forget to close the source RPF because, well, that's uncool to keep that open
                foreach (Tuple<string, Packfile> rpf in inputRpfs)
                {
                    rpf.Item2.Close();
                }

                return Constants.Exit_Failure;
            }
            
            // create temp directory in cache based on output provided
            string outputFullFileName = (string)options[OptionOutput];
            string cacheFileName = Path.GetFileNameWithoutExtension(outputFullFileName);
            if (cacheFileName == null)
            {
                // todo log error
            }
            string cacheDirectory = Path.Combine(options.Project.Cache, "raw", "rpf_merge", cacheFileName);

            // file list is needed for sorting before calling the Rpf.Create later on (setting capacity to an arbitrary to avoid multiple resize)
            List<Pair<string, string>> fileList = new List<Pair<string, string>>(inputRpfs.Count()*20);
            foreach (Tuple<string, Packfile> rpf in inputRpfs)
            {
                string rpfFilename = Path.GetFileNameWithoutExtension(rpf.Item1);
                // extract
                if (Rpf.ExtractAll(log, rpf.Item2, cacheDirectory, rpfFilename, false))
                {
                    // grab all stuff extacted with full file system path
                    string filesPath = Path.Combine(cacheDirectory, rpfFilename);

                    var files = Directory.GetFiles(filesPath, "*.*", SearchOption.AllDirectories);
                    fileList.AddRange(files.Select(file => new Pair<String, String>(file, Path.GetFileName(file))));
                }
            }

            // Don't forget to close the Packfile stream from RPF because, well, that's uncool to keep that open
            foreach (Tuple<string, Packfile> rpf in inputRpfs)
            {
                rpf.Item2.Close();
            }

            ITarget target = options.Branch.Targets[MergeUtils.ManagedRagePlaformToPipelinePlatform(platform)];
            bool createSuccessfull = Rpf.Create(log, outputFullFileName, fileList, target, metadataDirectory: "", manifestFilename: "");

            // clean cache ?

            return createSuccessfull ? Constants.Exit_Success : Constants.Exit_Failure;
        }
    }

    //TODO Rename 
    static class MergeUtils
    {
        public static string ShortName(this PackEntry entry)
        {
            return Path.GetFileNameWithoutExtension(entry.Name);
        }

        // TODO REMOVE WHEN url:bugstar:1824649 
        public static Platform.Platform ManagedRagePlaformToPipelinePlatform(Packfile.etPlatform managedRagePlatform)
        {
            return Platform.PlatformUtils.RagebuilderPlatformToPlatform(managedRagePlatform.ToString());//STUFF;
        }
    }
}
