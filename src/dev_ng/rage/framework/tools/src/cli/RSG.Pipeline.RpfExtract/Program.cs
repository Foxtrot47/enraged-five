﻿using System;
using System.IO;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.ManagedRage;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.RpfExtract
{

    /// <summary>
    /// 
    /// </summary>
    class Program
    {
        #region Constants
        private static readonly String OPTION_TASK_NAME = "taskname";
        private static readonly String OPTION_OUTPUTDIR = "output";
        private static readonly String OPTION_OVERWRITE = "overwrite";
        #endregion // Constants

        /// <summary>
        /// Entry-point.
        /// </summary>
        /// <param name="args"></param>
        [STAThread]
        static int Main(String[] args)
        {
            // Define our executable options.  Stick with the long options
            // (e.g. --opt) rather than the single character versions.
            LongOption[] lopts = new LongOption[] {
                new LongOption(OPTION_OUTPUTDIR, LongOption.ArgType.Required,
                    "Output directory."),
                new LongOption(OPTION_TASK_NAME, LongOption.ArgType.Required,
                    "Task name (for XGE error parsing)."),
                new LongOption(OPTION_OVERWRITE, LongOption.ArgType.None,
                    "Overwrite destination files.")
            };
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.ApplicationLog;
            CommandOptions options = new CommandOptions(args, lopts);
            if (options.ShowHelp)
            {
                ShowHelp(log, options);
                return (Constants.Exit_Failure);
            }
            if (!options.ContainsOption(OPTION_OUTPUTDIR))
            {
                log.Error("No output directory specified.");
                ShowHelp(log, options);
                return (Constants.Exit_Failure);
            }

            String outputDirectory = (String)options[OPTION_OUTPUTDIR];
            bool overwrite = options.ContainsOption(OPTION_OVERWRITE);

            String taskName = options[OPTION_TASK_NAME] == null ? String.Empty : options[OPTION_TASK_NAME] as String;
            if (!String.IsNullOrEmpty(taskName))
            {
                Console.WriteLine("TASK: {0}", taskName);
            }
            
            if (0 == options.TrailingArguments.Count())
            {
                Console.Error.WriteLine("No RPF files specified.");
                ShowHelp(log, options);
                return (Constants.Exit_Failure);
            }

            if (!Directory.Exists(outputDirectory))
                Directory.CreateDirectory(outputDirectory);

            // Loop through trailing arguments.
            int result = Constants.Exit_Success;
            foreach (String filename in options.TrailingArguments)
            {
                if (!File.Exists(filename))
                {
                    log.Error("RPF file {0} does not exist.  Skipping.");
                    result = Constants.Exit_Failure;
                    continue;
                }

                ExtractAll(log, filename, outputDirectory, overwrite);
            }

            return (result);
        }

        #region Private Methods
        /// <summary>
        /// Print help usage information.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="options"></param>
        private static void ShowHelp(IUniversalLog log, CommandOptions options)
        {
            log.Message(options.GetUsage("files", "RPF filenames to extract."));
        }

        /// <summary>
        /// Extract all files from RPF.
        /// </summary>
        /// <param name="log">Log.</param>
        /// <param name="filename">RPF filename.</param>
        /// <param name="outputDir">Output directory.</param>
        /// <param name="overwrite">Overwrite force flag.</param>
        /// <returns></returns>
        private static bool ExtractAll(IUniversalLog log, String filename, 
            String outputDir, bool overwrite)
        {
            bool result = true;
            Packfile pf = new Packfile();
            pf.Load(filename);
            foreach (PackEntry pe in pf.Entries)
            {
                String entryName = Path.Combine(pe.Path, pe.Name);
                String outputDirectory = Path.Combine(outputDir, pe.Path);
                if (!Directory.Exists(outputDirectory))
                    Directory.CreateDirectory(outputDirectory);

                String outputFilename = Path.Combine(outputDirectory, pe.Name);
                try
                {
                    if (!overwrite && File.Exists(outputFilename))
                    {
                        log.Error("File already exists: {0}.  Skipping.",
                            outputFilename);
                        continue; // Skip.
                    }

                    Byte[] buffer = pf.Extract(entryName);
                    using (FileStream fs = File.OpenWrite(outputFilename))
                    {
                        log.Message("Extracting: {0}.", outputFilename);
                        fs.Write(buffer, 0, buffer.Length);
                    }
                }
                catch
                {
                    log.Error("Failed to create {0}.", outputFilename);
                    result = false;
                }
            }
            pf.Close();
            return (result);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.RpfExtract namespace
