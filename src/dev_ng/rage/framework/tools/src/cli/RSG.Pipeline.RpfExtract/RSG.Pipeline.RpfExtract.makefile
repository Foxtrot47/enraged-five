#
# RSG.Pipeline.RpfExtract.makefile
#
 
Project RSG.Pipeline.RpfExtract

BuildTemplate $(toolsroot)/etc/projgen/RSG.Pipeline.Executable.build
 
ConfigurationType exe
 
FrameworkVersion 4.5
 
OutputPath $(toolsroot)\ironlib\lib\
 
Files {
	Program.cs
	Directory Properties {
			AssemblyInfo.cs
	}
	app.config
	rockstar.ico
}
 
ProjectReferences {
	..\..\..\..\..\base\tools\libs\RSG.Base.Configuration\RSG.Base.Configuration.csproj
	..\..\..\..\..\base\tools\libs\RSG.Base\RSG.Base.csproj
	..\..\..\..\..\base\tools\libs\RSG.Platform\RSG.Platform.csproj
	..\..\..\..\..\base\tools\libs\RSG.ManagedRage\RSG.ManagedRage_2010.vcxproj
	..\..\Libs\RSG.Pipeline.Core\RSG.Pipeline.Core.csproj
	..\..\Libs\RSG.Pipeline.Services\RSG.Pipeline.Services.csproj
}
References {
	System
	System.Core
	System.Xml.Linq
	System.Data.DataSetExtensions
	Microsoft.CSharp
	System.Data
	System.Xml
}
