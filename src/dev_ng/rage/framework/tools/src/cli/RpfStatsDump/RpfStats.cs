﻿//---------------------------------------------------------------------------------------------
// <copyright file="RpfStats.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RpfStatsDump
{
    
    /// <summary>
    /// Statistic counts collected per RPF file.
    /// </summary>
    internal struct RpfStats
    {
        public int numDrawable;
        public int numFragment;
        public int numTXD;
        public int numDrawDict;
        public int numResource;
        public int numFile;
    }

} // RpfStatsDump namespace
