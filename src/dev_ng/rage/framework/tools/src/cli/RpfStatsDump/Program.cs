﻿//---------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RpfStatsDump
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.IO;
    using System.Threading.Tasks;
    using RSG.Base.Logging;
    using RSG.Base.OS;
    using RSG.ManagedRage;

    class Program
    {
        private const String OPT_OUTPUT = "output";
        private const String OPT_COMPARE_RPFS = "compare";
        private const String OPT_COMPARE_DIRECTORY_1 = "directory1";
        private const String OPT_COMPARE_DIRECTORY_2 = "directory2";

        static void Main(string[] args)
        {
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            LongOption[] opts = new LongOption[]
            { 
                new LongOption(OPT_OUTPUT, LongOption.ArgType.Required, "Output CSV filename."),
                new LongOption(OPT_COMPARE_RPFS, LongOption.ArgType.None, "Compare RPFs."),
                new LongOption(OPT_COMPARE_DIRECTORY_1, LongOption.ArgType.Required, "First directory to compare."),
                new LongOption(OPT_COMPARE_DIRECTORY_2, LongOption.ArgType.Required, "Second directory to compare."),
            };
            Getopt options = new Getopt(args, opts);

            String outputFilename = @"x:\rpf_stats.csv";
            if (options.HasOption(OPT_OUTPUT))
                outputFilename = (String)options[OPT_OUTPUT];

            if (File.Exists(outputFilename))
            {
                try
                {
                    File.Delete(outputFilename);
                }
                catch (System.Exception ex)
                {
                    LogFactory.ApplicationLog.Exception(ex,"Exception while trying to delete output file: {0}", outputFilename);
                }                
            }

            if (!options.HasOption(OPT_COMPARE_RPFS))
            {
                // Get all RPF files listed in trailing directories.
                IDictionary<String, RpfStats> stats = new Dictionary<String, RpfStats>();
                List<String> allRpfFiles = new List<String>();
                foreach (String directory in options.TrailingOptions)
                {
                    LogFactory.ApplicationLog.Message("Finding all RPFs in {0}", directory);
                    String fullDirectory = Path.GetFullPath(directory);
                    String[] rpfFiles = Directory.GetFiles(fullDirectory, "*.rpf", SearchOption.AllDirectories);
                    allRpfFiles.AddRange(rpfFiles);
                }
                LogFactory.ApplicationLog.Message("{0} found.", allRpfFiles.Count);

                try
                {
                    Packfile pf = new Packfile();
                    foreach (String filename in allRpfFiles)
                    {
                        try
                        {
                            LogFactory.ApplicationLog.Message("Loading {0}", filename);
                            if (pf.Load(filename))
                            {
                                RpfStats stat = new RpfStats();
                                foreach (PackEntry pe in pf.Entries)
                                {
                                    if (pe.IsResource)
                                        ++stat.numResource;
                                    if (pe.IsFile)
                                        ++stat.numFile;
                                    if (IsDrawable(pe.Name))
                                        ++stat.numDrawable;
                                    else if (IsFragment(pe.Name))
                                        ++stat.numFragment;
                                    else if (IsDrawDict(pe.Name))
                                        ++stat.numDrawDict;
                                    else if (IsTXD(pe.Name))
                                        ++stat.numTXD;
                                }
                                stats.Add(filename, stat);
                            }
                            else
                            {
                                LogFactory.ApplicationLog.Error("Failed to load RPF: {0}", filename);
                                stats.Add(filename, new RpfStats());
                            }
                        }
                        catch (Exception ex)
                        {
                            LogFactory.ApplicationLog.Exception(ex, "Error reading RPF: {0}", filename);
                        }
                    }

                    // Write out CSV.
                    using (System.IO.StreamWriter sw = new System.IO.StreamWriter(outputFilename))
                    {
                        sw.WriteLine("Filename,Resources,Files,Drawables,Fragments,TXD,DrawDict");
                        foreach (KeyValuePair<String, RpfStats> stat in stats)
                        {
                            RpfStats s = stat.Value;
                            sw.WriteLine("{0},{1},{2},{3},{4},{5},{6}", stat.Key, s.numResource,
                                s.numFile, s.numDrawable, s.numFragment, s.numTXD, s.numDrawDict);
                        }
                    }
                }
                catch (Exception ex)
                {
                    LogFactory.ApplicationLog.Exception(ex, "Unhandled exception");
                }
            }
            else
            {
                if (options.HasOption(OPT_COMPARE_DIRECTORY_1) && options.HasOption(OPT_COMPARE_DIRECTORY_2))
                {
                    // Comparing RPFs in directories.
                    String directory1 = options[OPT_COMPARE_DIRECTORY_1] as String;
                    String directory2 = options[OPT_COMPARE_DIRECTORY_2] as String;

                    List<String> directory1Files = new List<String>();
                    List<String> directory2Files = new List<String>();

                    LogFactory.ApplicationLog.Message("Finding all RPFs in {0}", directory1);
                    String fullDirectory1 = Path.GetFullPath(directory1);
                    String[] directory1RpfFiles = Directory.GetFiles(fullDirectory1, "*.rpf", SearchOption.AllDirectories);
                    directory1Files.AddRange(directory1RpfFiles);

                    LogFactory.ApplicationLog.Message("Finding all RPFs in {0}", directory2);
                    String fullDirectory2 = Path.GetFullPath(directory2);
                    String[] directory2RpfFiles = Directory.GetFiles(fullDirectory2, "*.rpf", SearchOption.AllDirectories);
                    directory2Files.AddRange(directory2RpfFiles);

                    try
                    {
                        using (System.IO.StreamWriter sw = new System.IO.StreamWriter(outputFilename, true))
                        {
                            sw.WriteLine("RPF Name,Unique Files");
                        }

                        foreach (String dir1Filename in directory1Files)
                        {
                            String relFilename = Path.GetFileName(dir1Filename);
                            List<String> matchingFiles = directory2Files.Where(f => Path.GetFileName(f).Equals(relFilename)).ToList();

                            // Only check files that exists in both directories
                            if (matchingFiles.Any())
                            {
                                String dir2Filename = matchingFiles.First();

                                List<String> pf1UniqueFiles = new List<String>();
                                List<String> pf2UniqueFiles = new List<String>();

                                try
                                {
                                    LogFactory.ApplicationLog.Message("Loading {0}", dir1Filename);
                                    LogFactory.ApplicationLog.Message("Loading {0}", dir2Filename);
                                    Packfile pf1 = new Packfile();
                                    Packfile pf2 = new Packfile();

                                    if (pf1.Load(dir1Filename) && pf2.Load(dir2Filename))
                                    {
                                        pf1UniqueFiles = pf1.Entries.Select(f => f.Name).Except(pf2.Entries.Select(f => f.Name)).ToList();
                                        pf2UniqueFiles = pf2.Entries.Select(f => f.Name).Except(pf1.Entries.Select(f => f.Name)).ToList();

                                        // Write out CSV.
                                        using (System.IO.StreamWriter sw = new System.IO.StreamWriter(outputFilename, true))
                                        {
                                            //sw.WriteLine("{0},{1}", dir1Filename, pf1UniqueFiles.Count);
                                            sw.Write("{0},{1}", dir2Filename, pf2UniqueFiles.Count);
                                            foreach (String uniqueFile in pf2UniqueFiles)
                                            {
                                                sw.Write(",{0}", uniqueFile);
                                            }
                                            sw.Write("\n");
                                        }
                                    }
                                    else
                                    {
                                        LogFactory.ApplicationLog.Error("Failed to load RPF(s): {0} | {1}", dir1Filename, dir2Filename);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    LogFactory.ApplicationLog.Exception(ex, "Error reading RPF(s): {0} | {1}", dir1Filename, dir2Filename);
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogFactory.ApplicationLog.Exception(ex, "Unhandled exception");
                    }
                }
                else
                {
                    LogFactory.ApplicationLog.Message("Directory1 and Directory2 must both be specified for compare to work");
                }
            }

            LogFactory.ApplicationShutdown();
        }

        #region Private Data and Methods
        static RSG.Base.IO.Wildcard drawableWc = new RSG.Base.IO.Wildcard("*.?dr");
        static RSG.Base.IO.Wildcard fragmentWc = new RSG.Base.IO.Wildcard("*.?ft");
        static RSG.Base.IO.Wildcard drawDictWc = new RSG.Base.IO.Wildcard("*.?dd");
        static RSG.Base.IO.Wildcard txdWc = new RSG.Base.IO.Wildcard("*.?td");

        static bool IsDrawable(String filename)
        {
            return (drawableWc.IsMatch(filename));
        }

        static bool IsFragment(String filename)
        {
            return (fragmentWc.IsMatch(filename));
        }

        static bool IsDrawDict(String filename)
        {
            return (drawDictWc.IsMatch(filename));
        }

        static bool IsTXD(String filename)
        {
            return (txdWc.IsMatch(filename));
        }
        #endregion // Private Data and Methods
    }

} // RpfStatsDump namespace
