﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
//using RSG.Model.MaterialPresets;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services;
using RSG.SourceControl.Perforce;
using RSG.SourceControl;
using System.Text.RegularExpressions;
using RSG.Rage.TypeFile;
using RSG.Rage.Entity;
using RSG.Rage.Shader;

namespace CompareSvaFiles
{
    class Program
    {
        #region Constants
        private static readonly String OPT_INPUT = "input";
        private static readonly String OPT_OUTPUT = "output";
        private static readonly String OPT_SELECTOR = "selectorFile";
        private static readonly String START_DATE = "start_date";
        private static readonly String END_DATE = "end_date";

        private static readonly String CSV_EXTENSION = ".csv";
        #endregion // Constants

        #region variables
        static string outputFilePath;
        #endregion

        static void Main(string[] args)
        {
            int result = Constants.Exit_Success;
            LongOption[] lopts = new LongOption[] 
            {
                new LongOption(OPT_INPUT, LongOption.ArgType.Required, 
                    "Input file or directory."),
                new LongOption(OPT_OUTPUT, LongOption.ArgType.Required, 
                    "Output .zip file."),
                new LongOption(OPT_SELECTOR, LongOption.ArgType.Required, 
                    "Txt file with container zips to check."),
                new LongOption(START_DATE, LongOption.ArgType.Required, 
                    "Date for earlier comparison point."),
                new LongOption(END_DATE, LongOption.ArgType.Required, 
                    "Date for later comparison point.")
            };

            CommandOptions options = new CommandOptions(args, lopts);
            IUniversalLog log = LogFactory.CreateUniversalLog("Material Preset Converter");
            IUniversalLogTarget logFile = LogFactory.CreateUniversalLogFile(log);
            LogFactory.CreateApplicationConsoleLogTarget();
            P4 p4 = new P4();
            p4.Connect();
            if (!p4.IsValidConnection(true, true))
            {
                log.Error("Couldn't connect to perforce.");
                return;
            }
            
            string seachroot = options.Branch.Export;
            if(options.ContainsOption(OPT_INPUT) && Directory.Exists(options[OPT_INPUT] as string))
                seachroot = options[OPT_INPUT] as string;

            if(!options.ContainsOption(START_DATE))
            {
                log.Error("Have to define start date.");
                return;
            }

            if(!options.ContainsOption(OPT_OUTPUT))
            {
                log.Error("Have to define an output file path.");
                return;
            }
            string outputPath = Path.GetDirectoryName(options[OPT_OUTPUT] as string);
            if (outputPath[outputPath.Length - 1] != '/' || outputPath[outputPath.Length - 1] != '\\')
                outputPath += "/";

            bool checkAllFiles = true;
            List<string> selectorFiles = new List<string>();
            if(options.ContainsOption(OPT_SELECTOR))
            {
                checkAllFiles = false;
                using (StreamReader sr = new StreamReader(options[OPT_SELECTOR] as string)) 
                {
                    string line = sr.ReadLine();
                    while(null!=line)
                    {
                        selectorFiles.Add(line.ToLower());
                        line = sr.ReadLine();
                    }
                }
            }

#warning DHM FIX ME: use DateTime.TryParse
            string startDate = options[START_DATE] as string;
            if(!Regex.IsMatch(startDate, @"\d{4}/\d{2}/\d{2}:\d{2}:\d{2}:\d{2}"))
            {
                log.Error("Start date needs to be defined in yyyy/mm/dd:hh:mm:ss");
                return;
            }
            string endDate = "now";
            if(options.ContainsOption(END_DATE))
            {
                endDate = options[END_DATE] as string;
                if (!Regex.IsMatch(endDate, @"\d{4}/\d{2}/\d{2}:\d{2}:\d{2}:\d{2}"))
                {
                    log.Error("End date needs to be defined in yyyy/mm/dd:hh:mm:ss");
                    return;
                }
            }

            List<char> invalidPathChars = new List<char>(Path.GetInvalidPathChars());
            invalidPathChars.AddRange(new char[]{'\\', '/', ':'});
            string validStartPathName = Regex.Replace(startDate, "[" + String.Join(",", invalidPathChars) + "]", "_");
            string validEndPathName = Regex.Replace(endDate, "[" + String.Join(",", invalidPathChars) + "]", "_");

            string dumpDir = Path.Combine(options.Project.Cache, "/svaFileCompare");

            log.Message("SvaFileCompare");
            log.Message("\tUsing root path " + seachroot);
            log.Message("\tUsing dump path " + dumpDir);

            try
            {
                Directory.Delete(dumpDir, true);
            }catch(Exception ex){}
            Directory.CreateDirectory(dumpDir);
            string[] containerZips = Directory.GetFiles(seachroot, "*.zip", SearchOption.AllDirectories);
            foreach (string containerZipPath in containerZips)
            {
                string containerName = Path.GetFileNameWithoutExtension(containerZipPath).ToLower();
                if (!checkAllFiles && !selectorFiles.Contains(containerName))
                    continue;

                outputFilePath = outputPath + containerName + CSV_EXTENSION;
                if (!Directory.Exists(outputPath))
                    Directory.CreateDirectory(outputPath);

                using (StreamWriter sw = new StreamWriter(outputFilePath))
                {
                    sw.WriteLine("containername, drawableName, material_index, shader_preset, wetnessOld, wetnessNew");
                }

                string containerDumpDir = Path.Combine(dumpDir, containerName);
                Directory.CreateDirectory(containerDumpDir);

                string drDumpDirEnd = Path.Combine(containerDumpDir, validEndPathName);
                Directory.CreateDirectory(drDumpDirEnd);
                string[] p4paramsEnd = { containerZipPath + "@" + endDate };
                P4API.P4RecordSet res = p4.Run("sync", p4paramsEnd);
                foreach (string p4error in res.Errors)
                    log.Error("Perforce error:" + p4error);
                foreach (string p4warn in res.Warnings)
                    log.Warning("Perforce warning:" + p4warn);
                if (!File.Exists(containerZipPath))
                    continue;
                Zip.ExtractAll(containerZipPath, drDumpDirEnd, true);

                string drDumpDirStart = Path.Combine(containerDumpDir, validStartPathName);
                Directory.CreateDirectory(drDumpDirStart);
                string[] p4paramsStart = { containerZipPath + "@"+ startDate };
                res = p4.Run("sync", p4paramsStart);
                foreach (string p4error in res.Errors)
                    log.Error("Perforce error:" + p4error);
                foreach (string p4warn in res.Warnings)
                    log.Warning("Perforce warning:" + p4warn);
                if (!File.Exists(containerZipPath))
                    continue;
                Zip.ExtractAll(containerZipPath, drDumpDirStart, true);

                string[] drawableZips = Directory.GetFiles(drDumpDirEnd, "*.id*.zip");
                foreach (string drZipPath in drawableZips)
                {
                    string drName = Path.GetFileNameWithoutExtension(Path.GetFileNameWithoutExtension(drZipPath));
                    string drZipName = Path.GetFileName(drZipPath);

                    string[] matchedOlderZips = Directory.GetFiles(drDumpDirStart, "*"+drZipName);
                    if (matchedOlderZips.Count() < 1)
                    {
                        log.WarningCtx(drName, "Didn't find exported drawable in older data for object {0}.", drName);
                        continue;
                    }
                    if (matchedOlderZips.Count() > 1)
                    {
                        log.WarningCtx(drName, "Found multiple exported drawables in older data for object {0}.", drName);
                        continue;
                    }

                    string drDumpDir = Path.Combine(drDumpDirEnd, drName);
                    Directory.CreateDirectory(drDumpDir);
                    Zip.ExtractAll(drZipPath, drDumpDir, true);

                    string olderDrDumpDir = Path.Combine(drDumpDirStart, drName);
                    string olderZip = matchedOlderZips[0];
                    Directory.CreateDirectory(olderDrDumpDir);
                    Zip.ExtractAll(olderZip, olderDrDumpDir, true);

                    ProcessDrawable(log, drDumpDir, olderDrDumpDir, containerName, drName);
                    try
                    {
                        Directory.Delete(drDumpDir, true);
                        Directory.Delete(olderDrDumpDir, true);
                    }
                    catch (Exception ex) { }
                }
                try
                {
                    Directory.Delete(containerDumpDir, true);
                }
                catch (Exception ex) { }
            }
        }


        public static void ProcessDrawable(IUniversalLog log, string laterDir, string oldDir, string containerName, string drawableName)
        {
            string[] entityFiles = Directory.GetFiles(laterDir, "entity.type");
            string[] oldEntityFiles = Directory.GetFiles(oldDir, "entity.type");
            if( entityFiles.Count()<1 ||
                entityFiles.Count()>1 ||
                oldEntityFiles.Count()<1 ||
                oldEntityFiles.Count() > 1)
            {
                log.Error("none or multiple entity.type files found in {0} or {1}!", laterDir, oldDir);
                return;
            }

            string entityFile = entityFiles[0];
            string oldEntityFile = oldEntityFiles[0];

            FragmentType newType = new FragmentType(log, true, laterDir);
            FragmentType oldType = new FragmentType(log, true, oldDir);

            if (!newType.LoadFromFile(entityFile, "type") || !oldType.LoadFromFile(oldEntityFile, "type"))
            {
                log.Error("Couldn't load drawables {0} and/or {1}.");
            }

            if (newType.ShadingGroup.Count != oldType.ShadingGroup.Count)
            {
                log.Warning("Shading group has differnt amount of shaders in new and old data.");
            }

            List<Shader> newShaders = newType.ShadingGroup.Shaders as List<Shader>;
            List<Shader> oldShaders = oldType.ShadingGroup.Shaders as List<Shader>;
            for (int shaderindex = 0; shaderindex < newShaders.Count() && shaderindex < oldShaders.Count(); shaderindex++)
            {
                Shader newShader = newShaders[shaderindex];
                Shader oldShader = oldShaders[shaderindex];
                if (newShader.PresetName != oldShader.PresetName)
                {
                    log.Warning("Presets between old and new data don't match on drawable {0}'s material {1} on index {2}.", drawableName, newShader.PresetName, shaderindex);
                }

                IShaderVariable newWetness = newShader.ShaderVars.Variables.FirstOrDefault(v => v.Name == "WetnessMultiplier");
                IShaderVariable oldWetness = oldShader.ShaderVars.Variables.FirstOrDefault(v => v.Name == "WetnessMultiplier");
                if (null == newWetness || null == oldWetness)
                    continue;

                FloatVariable newWetnessFloat = newWetness as FloatVariable;
                FloatVariable oldWetnessFloat = oldWetness as FloatVariable;

                if (newWetnessFloat.Value != oldWetnessFloat.Value)
                {
                    using (StreamWriter sw = new StreamWriter(outputFilePath, true))
                    {
                        sw.WriteLine("{0}, {1}, {2}, {3}, {4}, {5}", 
                               containerName, 
                               drawableName, 
                               shaderindex,
                               newShader.PresetName, 
                               oldWetnessFloat.Value, 
                               newWetnessFloat.Value);
                    }
                }
            }
        }
    }
}
