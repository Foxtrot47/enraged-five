﻿using System;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.WhatIsMyStudio
{

    /// <summary>
    /// 
    /// </summary>
    class Program
    {

        /// <summary>
        /// Entry-point.
        /// </summary>
        /// <param name="args"></param>
        static int Main(String[] args)
        {
            LogFactory.Initialize();
            IUniversalLog log = LogFactory.ApplicationLog;
            int result = Constants.Exit_Success;
            try
            {
                IConfig config = ConfigFactory.CreateConfig();
                Console.WriteLine(config.Studios.ThisStudio.Name);
            }
            catch (Exception ex)
            {
                log.ToolException(ex, "Unhandled exception.");
                result = Constants.Exit_Failure;
            }

            if (LogFactory.HasError())
                result = Constants.Exit_Failure;

            return (result);
        }

    }

} // RSG.Pipeline.WhatIsMyStudio namespace
