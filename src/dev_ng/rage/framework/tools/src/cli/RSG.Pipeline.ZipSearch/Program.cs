﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.ZipSearch
{

    /// <summary>
    /// ZipFile Search Program; wildcard and regular expression searching
    /// for file entries in ZipFiles.
    /// </summary>
    class Program
    {
        #region Constants
        private const String OPTION_WILDCARD = "wildcard";
        private const String OPTION_REGEX = "regex";
        private const String OPTION_RECURSIVE = "recursive";
        private const String OPTION_INZIP = "inzip";

        private const String WILDCARD_ZIP = "*.zip";
        #endregion // Constants

        /// <summary>
        /// Entry-point.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        [STAThread]
        static int Main(String[] args)
        {
            // Merge our options with the default pipeline command options.
            LongOption[] opts = new LongOption[] {
                new LongOption(OPTION_WILDCARD, LongOption.ArgType.Required,
                    "Specify a wildcard to search zip files."),
                new LongOption(OPTION_REGEX, LongOption.ArgType.Required,
                    "Specify a regular expression to search zip files."),
                new LongOption(OPTION_RECURSIVE, LongOption.ArgType.None,
                    "Specify whether directories are searched recursively."),
                new LongOption(OPTION_INZIP, LongOption.ArgType.None,
                    "Specify whether zips-within-zips are searched (recursive).")
            };
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.ApplicationLog;

            CommandOptions options = new CommandOptions(args, opts);
            if (options.ShowHelp)
            {
                ShowHelp(log, options);
                return (Constants.Exit_Failure);
            }

            // Verify options.
            if (!(options.ContainsOption(OPTION_WILDCARD) || options.ContainsOption(OPTION_REGEX)))
            {
                log.Error("Either {0} or {1} options are required to define search.", 
                    OPTION_WILDCARD, OPTION_REGEX);
                ShowHelp(log, options);
                return (Constants.Exit_Failure);
            }
            if (options.ContainsOption(OPTION_WILDCARD) && options.ContainsOption(OPTION_REGEX))
            {
                log.Error("Either {0} or {1} options are required to define search but not both.", 
                    OPTION_WILDCARD, OPTION_REGEX);
                ShowHelp(log, options);
                return (Constants.Exit_Failure);
            }

            bool recursive = options.ContainsOption(OPTION_RECURSIVE);
            bool inzip = options.ContainsOption(OPTION_INZIP);
            List<String> filenames = new List<String>();
            foreach (String trailing in options.TrailingArguments)
            {
                String filename = Path.GetFullPath(trailing);

                if (File.Exists(filename))
                    filenames.Add(filename);
                else if (Directory.Exists(filename))
                {
                    filenames.AddRange(Directory.GetFiles(filename, WILDCARD_ZIP,
                        recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly));
                }
                else
                    log.Warning("Trailing argument '{0}' not file or directory.  Ignoring.", filename);
            }

            // Run search.
            if (options.ContainsOption(OPTION_WILDCARD))
            {
                String wildcard = (String)options[OPTION_WILDCARD];
                RunWildcardSearch(log, wildcard, inzip, filenames);
            }
            else if (options.ContainsOption(OPTION_REGEX))
            {
                String regex = (String)options[OPTION_REGEX];
                RunRegexSearch(log, regex, inzip, filenames);
            }
            else
            {
                log.Error("Invalid search. {0} or {1} options are required to define search but not both.",
                    OPTION_WILDCARD, OPTION_REGEX);
                ShowHelp(log, options);
            }

            return (LogFactory.HasError() ? Constants.Exit_Failure : Constants.Exit_Success);
        }

        #region Private Methods
        /// <summary>
        /// Run wildcard string search.
        /// </summary>
        /// <param name="wildcard"></param>
        /// <param name="filenames"></param>
        private static void RunWildcardSearch(IUniversalLog log, String wildcard, bool inzip, IEnumerable<String> filenames)
        {
            RSG.Base.IO.Wildcard wc = new RSG.Base.IO.Wildcard(wildcard, RegexOptions.IgnoreCase);
            RunRegexSearch(log, wc, inzip, filenames);
        }

        /// <summary>
        /// Run regex string search.
        /// </summary>
        /// <param name="regex"></param>
        /// <param name="filenames"></param>
        private static void RunRegexSearch(IUniversalLog log, String regex, bool inzip, IEnumerable<String> filenames)
        {
            Regex re = new Regex(regex);
            RunRegexSearch(log, re, inzip, filenames);
        }

        /// <summary>
        /// Run Regex search (base search implementation).
        /// </summary>
        /// <param name="log"></param>
        /// <param name="regex"></param>
        /// <param name="inzip"></param>
        /// <param name="filenames"></param>
        private static void RunRegexSearch(IUniversalLog log, Regex regex, bool inzip, IEnumerable<String> filenames)
        {
            List<String> matches = new List<String>();
            foreach (String filename in filenames)
            {
                matches.Clear();
                using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
                {
                    RunRegexSearch(log, filename, regex, inzip, fs, matches);
                }
                if (matches.Count > 0)
                {
                    log.Message("{0} contains matches:", filename);
                    foreach (String match in matches)
                        log.Message("\t{0}", match);
                }
            }
        }

        /// <summary>
        /// Run Regex search in Stream.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="regex"></param>
        /// <param name="inzip"></param>
        /// <param name="stream"></param>
        /// <param name="matches"></param>
        private static void RunRegexSearch(IUniversalLog log, String context, Regex regex, bool inzip, Stream stream, ICollection<String> matches)
        {
            using (ZipArchive za = new ZipArchive(stream, ZipArchiveMode.Read))
            {
                // Loop through entries to see if it matches our regex.
                foreach (ZipArchiveEntry ze in za.Entries)
                {
                    if (regex.IsMatch(ze.Name))
                    {
                        if (Path.IsPathRooted(context))
                            matches.Add(ze.Name);
                        else
                            matches.Add(String.Format("{0}: {1}", context, ze.Name));
                    }

                    if (inzip && ze.Name.EndsWith(".zip"))
                    {
                        using (Stream s = ze.Open())
                        {
                            RunRegexSearch(log, ze.Name, regex, inzip, s, matches);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Display help info to log.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="options"></param>
        private static void ShowHelp(IUniversalLog log, CommandOptions options)
        {
            log.Message(options.GetUsage("files", "Zip files or directories to search."));
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.ZipSearch namespace
