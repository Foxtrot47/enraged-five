@echo off
pushd %~dp0

call setenv > NUL

set build_script=%RS_TOOLSROOT%\script\util\projgen\rebuildCsharpProject.bat
set cli=%RAGE_DIR%\framework\tools\src\cli
set libs=%RAGE_DIR%\framework\tools\src\libs
set projgen_config=%RS_TOOLSCONFIG%\projgen

@echo ====================================================================================== 
@echo ============================ PROJECT GENERATOR 3 STARTED =============================
@echo = https://devstar.rockstargames.com/wiki/index.php/Dev:Project_builder3#C.23_support = 
set started=%time% 
@echo STARTED : %started%
echo.

echo ********
echo * SYNC *
echo ********
echo.
call %RS_TOOLSROOT%\script\util\projGen\sync.bat
echo.

echo *****************************************
echo * BUILDING PIPELINE EXECUTABLE PROJECTS *
echo *****************************************
call %build_script% %cli%/RSG.Pipeline.ZipSearch/RSG.Pipeline.ZipSearch.makefile					
:END
@echo STARTED  : %started% 
@echo FINISHED : %time% 
@echo.
@echo * Default changelist contains project generated files. *
@echo.
@echo ===================== PROJECT GENERATOR 3 COMPLETE ======================
@echo =========================================================================

pause
