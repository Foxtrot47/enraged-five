#
# RSG.Pipeline.ZipSearch.makefile
#
 
Project RSG.Pipeline.ZipSearch
 
ConfigurationType exe

BuildTemplate $(toolsroot)/etc/projgen/RSG.Pipeline.Executable.build
 
FrameworkVersion 4.5
 
OutputPath $(toolsroot)\ironlib\lib\
 
Files {
	Program.cs
	Directory Properties {
			AssemblyInfo.cs
	}
	app.config
	rockstar.ico
}
 
ProjectReferences {
	..\..\..\..\..\base\tools\libs\RSG.Base\RSG.Base.csproj
	..\..\..\..\..\base\tools\libs\RSG.Base.Configuration\RSG.Base.Configuration.csproj
	..\..\Libs\RSG.Pipeline.Core\RSG.Pipeline.Core.csproj
}
References {
	System
	System.Core
	System.Xml.Linq
	System.Data.DataSetExtensions
	Microsoft.CSharp
	System.Data
	System.IO.Compression
	System.Xml
}
