﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Base.OS;
using System.ServiceModel;
using System.Reflection;
using System.ServiceModel.Configuration;
using System.Configuration;
using System.Threading;
using System.IO;
using RSG.Base.Net;
using RSG.Base.Configuration;
using RSG.Statistics.VerticaServices.Service;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Services;
using RSG.Statistics.Common.Config;
using System.Diagnostics;

namespace RSG.Statistics.VerticaConsoleHost
{
    class Program
    {
        #region Constants
        // Options
        private static readonly String OPT_SERVER = "server";
        #endregion // Constants

        #region Member Data
        private static IUniversalLog m_log;
        #endregion // Member Data

        #region Program Entry Point
        /// <summary>
        /// The main entry point for the application.
        /// Attribute [STAThread] for invoking the universal log from the right thread
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            // Create the log
            LogFactory.Initialize();
            m_log = LogFactory.CreateUniversalLog("Statistics_ServerHost");
            LogFactory.CreateApplicationConsoleLogTarget();
            UniversalLogFile logfile = LogFactory.CreateUniversalLogFile(m_log) as UniversalLogFile;

            // Define our executable options.  Stick with the long options
            // (e.g. --opt) rather than the single character versions.
            LongOption[] lopts = new LongOption[] {
                new LongOption(OPT_SERVER, LongOption.ArgType.Required, "Server to connect to.")
            };
            CommandOptions options = new CommandOptions(args, lopts);

            IStatisticsConfig config = new StatisticsConfig(options.Branch);
            IVerticaServer server;
            if (options.ContainsOption(OPT_SERVER))
            {
                String serverName = (String)options[OPT_SERVER];
                Debug.Assert(config.VerticaServers.ContainsKey(serverName), "Requested an unknown server");
                server = config.VerticaServers[serverName];
            }
            else
            {
                throw new ArgumentNullException("Server commandline parameter is not optional.");
            }

            // Keep track of the service hosts we opened so that we can close them later on
            List<ServiceHost> hosts = new List<ServiceHost>();

            // All services need to be in the same assembly as the ServiceBase class
            try
            {
                // All services need to be in the same assembly as the ServiceBase class
                Assembly ass = typeof(ServiceBase).Assembly;

                // For each service section in the configuration, create and open a host
                m_log.Profile("Starting WCF services.");

                ServicesSection section = ConfigurationManager.GetSection("system.serviceModel/services") as ServicesSection;
                if (section != null)
                {
                    foreach (ServiceElement element in section.Services)
                    {
                        m_log.Debug("Creating host for the '{0}' service.", element.Name);
                        Type serviceType = ass.GetType(element.Name);
                        Debug.Assert(serviceType != null, "Unable to determine the type of the service from the config.");
                        if (serviceType == null)
                        {
                            throw new ArgumentNullException(String.Format("Unable to determine the type of the service '{0}' from the config.", element.Name));
                        }

                        // Determine what kind of service we are trying to create.
                        ServiceBehaviorAttribute att =
                           serviceType.GetCustomAttributes(typeof(ServiceBehaviorAttribute), true).FirstOrDefault() as ServiceBehaviorAttribute;
                        Debug.Assert(att != null, "Trying to instantiate a service instance that doesn't have a ServiceBehaviorAttribute associated with it.");
                        if (att == null)
                        {
                            throw new ArgumentNullException("Trying to instantiate a service instance that doesn't have a ServiceBehaviorAttribute associated with it.");
                        }

                        // Create the appropriate host for the service's instance context mode.
                        ServiceHost host;
                        if (att.InstanceContextMode == InstanceContextMode.Single)
                        {
                            object serviceInstance = Activator.CreateInstance(serviceType, server);
                            host = new VerticaServiceHost(serviceInstance);
                        }
                        else
                        {
                            host = new VerticaServiceHost(server, serviceType);
                        }

                        // Keep track of this host for later and open it.
                        hosts.Add(host);
                        host.Open();
                    }
                }
                m_log.ProfileEnd();
                
                Console.WriteLine("Server successfully started.  Press <enter> to stop.");
                Console.ReadLine();
            }
            catch (ConfigurationVersionException ex)
            {
                m_log.ToolException(ex, "Configuration version error: run tools installer.  Installed version: {0}, expected version: {1}.",
                    ex.ActualVersion, ex.ExpectedVersion);
                m_log.Message("An important update has been made to the tool chain.  Install version: {0}, expected version: {1}.{2}{2}Sync latest or labelled tools, run {3} and then restart the application.",
                    ex.ActualVersion, ex.ExpectedVersion, Environment.NewLine,
                    Path.Combine(Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "install.bat"));
                Environment.Exit(1);
            }
            catch (RSG.Base.Configuration.ConfigurationException ex)
            {
                m_log.ToolException(ex, "Configuration parse error.");
                m_log.Message("There was an error initialising configuration data.{0}{0}Sync latest or labelled tools and restart the application.", Environment.NewLine);
                Environment.Exit(1);
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex);

                if (ex.InnerException != null)
                {
                    Console.WriteLine("");
                    Console.WriteLine("Inner Exception");
                    Console.WriteLine(ex.InnerException);
                }
                Console.ReadLine();
            }
            finally
            {
                m_log.Message("Stopping WCF services.");
                foreach (ServiceHost host in hosts)
                {
                    if (host.State == CommunicationState.Opened)
                    {
                        host.Close();
                    }
                    else
                    {
                        host.Abort();
                    }
                }
            }

            LogFactory.ApplicationShutdown();
        }
        #endregion // Program Entry Point
    }
}
