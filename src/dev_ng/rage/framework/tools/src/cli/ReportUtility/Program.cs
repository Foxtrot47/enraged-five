﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Configuration;
using RSG.Base.Configuration.Bugstar;
using RSG.Base.OS;
using RSG.Model.Asset;
using RSG.Model.Asset.Level;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using RSG.Model.Map3;
using RSG.Interop.Bugstar;

namespace ReportUtility
{
#warning DHM FIX ME: this tool is dead and needs to be integrated into ReportGenerator!
    class Program
    {
        #region Constants
        private static readonly String OPTION_REFERENCES = "references";
        private static readonly String OPTION_LEVEL = "level";
        #endregion

        static int Main(string[] args)
        {
            Getopt options = new Getopt(args, new LongOption[] {
#warning DHM FIX ME: LongOption.ArgType.Optional is probably not what was meant.
                new LongOption(OPTION_REFERENCES, LongOption.ArgType.Optional,
                    ""),
                new LongOption(OPTION_LEVEL, LongOption.ArgType.Required,
                    "Level name to load."),
            });

            IConfig config = ConfigFactory.CreateConfig();

            if (options.HasOption(OPTION_LEVEL) == false)
            {
                Console.WriteLine("No level specified.  Aborting...");
                return -1;
            }
            string levelName = (string) options.Options[OPTION_LEVEL];
            bool findReferences = options.HasOption(OPTION_REFERENCES);

            if (options.TrailingOptions.Length == 0 )
            {
                Console.WriteLine("No files specified.  Aborting...");
                return -1;
            }

            string[] filesArray = options.TrailingOptions;
            List<String> files = new List<String>();
            for (int index = 0; index < filesArray.Length; ++index)
            {
                if ( Directory.Exists(filesArray[index]) == true )
                {
                    string[] dirFiles = Directory.GetFiles(filesArray[index], "*.*");
                    foreach(string dirFile in dirFiles)
                    {
                        string normalizedDirFile = dirFile.Replace("\\", "/");
                        files.Add(normalizedDirFile);
                    }
                }
                else
                {
                    filesArray[index] = filesArray[index].Replace("\\", "/");
                    files.Add(filesArray[index]);
                }
            }
            
            if ( files.Count() == 1 )
                Console.WriteLine(String.Format("Searching for references to {0}.", files[0]));
            else
                Console.WriteLine(String.Format("Searching for references for {0} files.", files.Count()));

            string tempFile = Path.GetTempFileName();
            StreamWriter writer = new StreamWriter(tempFile);

            if (findReferences)
            {
                Dictionary<String, List<IMapArchetype>> referencedArchetypes = new Dictionary<String, List<IMapArchetype>>();
                foreach(string file in files) referencedArchetypes.Add(file, new List<IMapArchetype>());
                ILevel level = LoadLevel(levelName, config);

                level.MapHierarchy.AllSections.AsParallel().ForAll(section =>
                {
                    foreach(IMapArchetype archetype in section.Archetypes)
                    {
                        foreach(ITexture texture in archetype.Textures)
                        {
                            foreach (string file in files)
                            {
                                if ( String.Compare(file, texture.Filename, true) == 0)
                                {
                                    lock(referencedArchetypes)
                                    {
                                        referencedArchetypes[file].Add(archetype);
                                    }
                                }
                                else if(texture.AlphaFilename != null && String.Compare(file, texture.AlphaFilename, true) == 0)
                                {
                                    lock(referencedArchetypes)
                                    {
                                        referencedArchetypes[file].Add(archetype);
                                    }
                                }
                            }
                        }
                    }
                });

                files.Sort( (x, y) => String.Compare(x, y, true));

                foreach(string file in files)
                {
                    writer.WriteLine(String.Format("{0} is referenced by {1} object definition(s).", file, referencedArchetypes[file].Count));
                    foreach(IMapArchetype archetype in referencedArchetypes[file].OrderBy(x => x.ParentSection.Name))
                    {
                        writer.WriteLine(String.Format("\t{0} - {1}", archetype.ParentSection.Name, archetype.Name ));
                    }
                    writer.WriteLine();
                }

                writer.Close();

                Process process = new Process();
                process.StartInfo.FileName = "notepad";
                process.StartInfo.Arguments = tempFile;
                process.Start();
                process.WaitForExit();

                try { File.Delete(tempFile); }
                catch (Exception) { }
            }

            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="levelName"></param>
        /// <returns></returns>
        private static ILevel LoadLevel(string levelName, IConfig config)
        {
            // Get the level we are after
            IBugstarConfig bugstarConfig = ConfigFactory.CreateBugstarConfig(config);
            ILevelFactory levelFactory = new LevelFactory(config, bugstarConfig);
            ILevelCollection levelCollection = levelFactory.CreateCollection(DataSource.SceneXml);
            levelCollection.LoadStats(new StreamableStat[] { StreamableLevelStat.ExportDataPath, StreamableLevelStat.ProcessedDataPath, StreamableLevelStat.ImageBounds, StreamableLevelStat.BackgroundImage });

            ILevel level = levelCollection.FirstOrDefault(item => item.Name == levelName);

            // Check that we found the requested level
            if (level != null)
            {
                // Map hierarchy
                BugstarConnection bugstarConnection = new BugstarConnection(bugstarConfig.RESTService, bugstarConfig.AttachmentService);
                IMapFactory mapFactory = new MapFactory(config, bugstarConfig, bugstarConnection);
                level.MapHierarchy = mapFactory.CreateHierarchy(DataSource.SceneXml, level);

                object objectLock = new object();
                float progress = 0.0f;
                float increment = 1.0f / (float) level.MapHierarchy.AllSections.Count();

                level.MapHierarchy.AllSections.AsParallel().ForAll(section =>
                {
                    section.LoadStats(new StreamableStat[] { StreamableMapSectionStat.Archetypes, StreamableMapSectionStat.Entities });

                    lock (objectLock)
                    {
                        progress += increment;
                        Console.Write("\rLoading level data...{0}%   ", (int)(progress*100));
                    }
                });

                progress = 1.0f;
                Console.Write("\rLoading level data...{0}%   ", (int)(progress * 100));
            }

            return level;
        }
    }
}
