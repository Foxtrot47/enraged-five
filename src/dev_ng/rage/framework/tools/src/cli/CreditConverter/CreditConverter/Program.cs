﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using Microsoft.Office.Interop.Word;

namespace CreditConverter
{
    class Program
    {
        private static List<string> inputs = new List<string>();
        private static string output;
        private static string jobCodes;
        private static Dictionary<string, string> jobTitles;

        static void Main(string[] args)
        {
            bool valid = true;
            Console.ForegroundColor = ConsoleColor.Red;
            foreach (string arg in args)
            {
                if (arg.StartsWith("--output="))
                {
                    if (output == null)
                    {
                        output = arg.Remove(0, "--output=".Length);
                    }
                    else
                    {
                        valid = false;
                        Console.WriteLine("Multiple 'output' arguments specified on the commandline");
                    }
                }
                else if (arg.StartsWith("--jobCodes="))
                {
                    if (jobCodes == null)
                    {
                        jobCodes = arg.Remove(0, "--jobCodes=".Length);
                    }
                    else
                    {
                        valid = false;
                        Console.WriteLine("Multiple 'jobCodes' arguments specified on the commandline");
                    }
                }
                else
                {
                    inputs.Add(arg);
                    if (!File.Exists(arg))
                    {
                        valid = false;
                        Console.WriteLine("Specified input file '{0}' doesn't exist", arg);
                    }
                    else
                    {
                        string extension = Path.GetExtension(arg);
                        if (extension != ".doc" && extension != ".docx")
                        {
                            valid = false;
                            Console.WriteLine("Specified input file '{0}' has incorrect extension only supports .doc & .docx", arg);
                        }
                    }
                }
            }

            if (inputs.Count == 0)
            {
                valid = false;
                Console.WriteLine("No input files specified.");
            }

            if (output == null)
            {
                valid = false;
                Console.WriteLine("No output path specified.");
            }

            if (jobCodes == null)
            {
                valid = false;
                Console.WriteLine("No job codes path specified.");
            }

            if (valid == false)
            {
                Console.WriteLine("Unable to processed incorrect arguments on the commandline");
                Console.WriteLine("Press any key to exit");
                Console.ReadKey();
                Environment.Exit(1);
            }

            Console.ResetColor();
            Console.WriteLine("Processing the following files:");
            foreach (string input in inputs)
            {
                Console.WriteLine(input);
            }

            Console.WriteLine("Parameters:");
            Console.WriteLine("--output : {0}", output);
            Console.WriteLine("--jobcodes : {0}", jobCodes);

            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("------------------------------------------------------");
            Console.WriteLine("Processing Job List");

            jobTitles = new Dictionary<string, string>();
            using (StreamReader jobs = new StreamReader(jobCodes))
            {
                string currentCode = null;
                string line = String.Empty;
                while((line = jobs.ReadLine()) != null)
                {
                    if (line == String.Empty)
                    {
                        continue;
                    }

                    if (line[0] == '[' && line.EndsWith(":CREDIT]"))
                    {
                        currentCode = line.Substring(1, line.Length - 9);
                        continue;
                    }

                    if (currentCode != null)
                    {
                        string text = line.Trim().ToLower();
                        if (!jobTitles.ContainsKey(text))
                        {
                            jobTitles.Add(text, currentCode);
                        }

                        currentCode = null;
                    }
                }
            }

            Console.WriteLine("Found {0} unique job codes", jobTitles.Count.ToString());

            ProcessDocuments();
            Console.WriteLine("Press any key to exit");
            Console.ReadKey();
        }

        static void ProcessDocuments()
        {
            _Application wordApplication = null;
            _Document document = null;
            string fullText = String.Empty;
            object missing = System.Reflection.Missing.Value;
            object readOnly = true;
            object falseValue = false;

            try
            {
                Dictionary<string, string> addedCodes = new Dictionary<string, string>();
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;
                settings.Encoding = Encoding.UTF8;
                using (XmlWriter writer = XmlWriter.Create(output, settings))
                {
                    writer.WriteStartDocument();
                    writer.WriteStartElement("CCreditArray");
                    writer.WriteStartElement("CreditItems");

                    foreach (string input in inputs)
                    {
                        Console.WriteLine();
                        Console.WriteLine();
                        Console.WriteLine();
                        Console.WriteLine("------------------------------------------------------");
                        Console.WriteLine("Processing {0}", input);

                        object fileName = input;
                        wordApplication = new Application();
                        wordApplication.Visible = false;
                        document = wordApplication.Documents.Open(
                            ref fileName,
                            ref missing,
                            ref readOnly,
                            ref missing,
                            ref missing,
                            ref missing,
                            ref missing,
                            ref missing,
                            ref missing,
                            ref missing,
                            ref missing,
                            ref falseValue);

                        int nodeCount = 0;
                        int idAddedCount = 0;
                        List<string> warnings = new List<string>();
                        foreach (Tuple<string, LineType> line in GetLinesWithType(document, warnings))
                        {
                            nodeCount++;
                            Console.WriteLine("Handling {0} - {1}", line.Item2, line.Item1);
                            writer.WriteStartElement("Item");

                            switch (line.Item2)
                            {
                                case LineType.BigName:
                                    writer.WriteElementString("LineType", "NAME_BIG");
                                    writer.WriteElementString("cTextId1", line.Item1.Trim());
                                    break;
                                case LineType.BigSpace:
                                    writer.WriteElementString("LineType", "SPACE_BIG");
                                    writer.WriteElementString("cTextId1", null);
                                    break;
                                case LineType.JobTitle:
                                    writer.WriteElementString("LineType", "JOB_MED");
                                    break;
                                case LineType.MediumSpace:
                                    writer.WriteElementString("LineType", "SPACE_MED");
                                    writer.WriteElementString("cTextId1", null);
                                    break;
                                case LineType.SmallName:
                                    writer.WriteElementString("LineType", "NAME_SMALL");
                                    writer.WriteElementString("cTextId1", line.Item1.Trim());
                                    break;
                                case LineType.BigJob:
                                    writer.WriteElementString("LineType", "JOB_BIG");
                                    break;
                                case LineType.StudioName:
                                    writer.WriteElementString("LineType", "JOB_BIG");
                                    break;
                            }

                            if (line.Item2 == LineType.StudioName || line.Item2 == LineType.JobTitle)
                            {
                                string code = null;
                                string job = line.Item1.Trim().ToLower();
                                if (!jobTitles.TryGetValue(job, out code))
                                {
                                    idAddedCount++;
                                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                                    Console.WriteLine("Creating unique job id for {0}", job);
                                    Console.ResetColor();
                                    code = CreateUniqueJobCode(job, new HashSet<string>(jobTitles.Values));
                                    jobTitles.Add(job, code);
                                    addedCodes.Add(code, line.Item1);
                                }

                                writer.WriteElementString("cTextId1", code);
                            }

                            writer.WriteElementString("cTextId2", null);
                            writer.WriteEndElement();
                        }

                        document.Close(falseValue, missing, missing);
                        document = null;
                        Console.WriteLine("Created {0} xml items and {1} unique jobs", nodeCount, idAddedCount);
                    }

                    writer.WriteEndElement();
                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                }

                Console.WriteLine();
                Console.WriteLine();
                Console.WriteLine();

                if (addedCodes.Count > 0)
                {
                    string currentDirectory = Directory.GetCurrentDirectory();
                    string path = Path.Combine(currentDirectory, "creditconverteraddedcodes.txt");
                    using (StreamWriter writer = new StreamWriter(path))
                    {
                        foreach (KeyValuePair<string, string> addedCode in addedCodes)
                        {
                            writer.WriteLine("[{0}:CREDIT]", addedCode.Key);
                            writer.WriteLine(addedCode.Value);
                        }
                    }

                    Process.Start(path);
                    Console.WriteLine("Successfully converted with {0} codes having been created. File saved '{1}'", addedCodes.Count, path);
                }
                else
                {
                    Console.WriteLine("Successfully converted.");
                }
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Error occured: {0}", ex.Message);
                Console.ResetColor();
            }
            finally
            {
                if (document != null)
                {
                    document.Close(falseValue, missing, missing);
                }

                if (wordApplication != null)
                {
                    wordApplication.Quit(falseValue, missing, missing);
                }
            }
        }

        private static string CreateUniqueJobCode(string job, HashSet<string> existing)
        {
            string[] words = job.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            StringBuilder baseCodeBuilder = new StringBuilder();
            foreach (string word in words)
            {
                baseCodeBuilder.Append(word.Take(3).ToArray());
            }

            string code = baseCodeBuilder.ToString().ToUpper();
            int index = 1;
            while (existing.Contains(code))
            {
                code = code + index.ToString();
                index++;
            }

            return code;
        }

        private enum LineType
        {
            None,
            StudioName,
            BigJob,
            JobTitle,
            BigName,
            SmallName,
            BigSpace,
            MediumSpace,
        }

        private struct LineDescription
        {
            public string Text { get; set; }
            public bool Bold { get; set; }
            public bool Italic { get; set; }
            public bool Underlined { get; set; }
            public int LineNumber { get; set; }
        }

        private static List<Tuple<string, LineType>> GetLinesWithType(_Document document, List<string> warnings)
        {
            List<LineDescription> lines = GetLines(document);
            List<Tuple<string, LineType>> typeLines = new List<Tuple<string, LineType>>();
            if (lines.Count <= 1)
            {
                return typeLines;
            }

            Regex space =  new Regex(@"\A\z", RegexOptions.Compiled);
            Regex bigSpace = new Regex(@"\A[=]+\z", RegexOptions.Compiled);
            Regex studio = new Regex(@"\A(R\* |Rockstar |RAGE )[\w\s.]+\z", RegexOptions.Compiled | RegexOptions.IgnoreCase);

            LineType previousType = LineType.None;
            bool insideJobBlock = false;
            for (int lineIndex = 0; lineIndex < lines.Count; lineIndex++)
            {
                LineDescription currentLine = lines[lineIndex];
                string text = currentLine.Text;
                bool bold = currentLine.Bold;
                bool italic = currentLine.Italic;

                if (space.IsMatch(text))
                {
                    if (previousType != LineType.StudioName)
                    {
                        insideJobBlock = false;
                        previousType = LineType.MediumSpace;
                    }

                    typeLines.Add(new Tuple<string, LineType>(text, LineType.MediumSpace));
                    continue;
                }

                if (bigSpace.IsMatch(text))
                {
                    if (previousType != LineType.StudioName)
                    {
                        insideJobBlock = false;
                        previousType = LineType.BigSpace;
                    }

                    typeLines.Add(new Tuple<string, LineType>("", LineType.BigSpace));
                    continue;
                }

                if (italic || (bold && text.EndsWith(":")))
                {
                    typeLines.Add(new Tuple<string, LineType>(text, LineType.JobTitle));
                    previousType = LineType.JobTitle;
                    insideJobBlock = true;
                    continue;
                }

                if (studio.IsMatch(text))
                {
                    typeLines.Add(new Tuple<string, LineType>(text, LineType.StudioName));
                    previousType = LineType.StudioName;
                    insideJobBlock = true;
                    continue;
                }

                if (insideJobBlock)
                {
                    if (bold)
                    {
                        if (previousType != LineType.SmallName)
                        {
                            typeLines.Add(new Tuple<string, LineType>(text, LineType.BigName));
                            previousType = LineType.BigName;
                            continue;
                        }
                    }
                    else
                    {
                        typeLines.Add(new Tuple<string, LineType>(text, LineType.SmallName));
                        previousType = LineType.SmallName;
                        continue;
                    }
                }

                if (bold)
                {
                    typeLines.Add(new Tuple<string, LineType>(text, LineType.BigJob));
                    previousType = LineType.BigJob;
                }
                else
                {
                    typeLines.Add(new Tuple<string, LineType>(text, LineType.SmallName));
                    previousType = LineType.SmallName;
                }
            }
            
            return typeLines;
        }

        private static List<LineDescription> GetLines(_Document document)
        {
            List<LineDescription> lines = new List<LineDescription>();
            if (document == null)
            {
                return lines;
            }

            Range range = document.Content.FormattedText;
            string lineText = String.Empty;
            bool bold = true;
            bool italic = true;
            bool centered = true;
            bool underlined = true;
            int lineNumber = 0;
            double words = range.Words.Count;
            double wordIndex = 0;

            List<Range> wordCollection = new List<Range>();
            foreach (Range word in range.Words)
            {
                double percentage = (wordIndex++ / words) * 0.5;
                ShowProgress(percentage);
                wordCollection.Add(word);
            }

            for (int i = 0; i < wordCollection.Count; i++)
            {
                double percentage = (wordIndex++ / words) * 0.5;
                ShowProgress(percentage);

                Range word = wordCollection[i];
                string wordText = word.Text;

                char last = wordText[wordText.Length - 1];
                char first = wordText[0];
                if (last == '\r' || last == '\v')
                {
                    lineNumber++;
                    if (!centered || (lineText == "" && word.ParagraphFormat.Alignment != WdParagraphAlignment.wdAlignParagraphCenter))
                    {
                        if (wordText.Length != 1)
                        {
                            lineText += wordText.Substring(0, wordText.Length - 1);
                        }

                        lines.Add(
                            new LineDescription()
                            {
                                Text = lineText,
                                Bold = bold,
                                Italic = italic,
                                Underlined = underlined,
                                LineNumber = lineNumber,
                            });
                    }

                    lineText = String.Empty;
                    bold = true;
                    italic = true;
                    underlined = true;
                    centered = true;
                }
                else
                {
                    if (word.Bold == 0)
                    {
                        bold = false;
                    }

                    if (word.Italic == 0)
                    {
                        italic = false;
                    }

                    if (word.Underline == WdUnderline.wdUnderlineNone)
                    {
                        underlined = false;
                    }

                    if (word.ParagraphFormat.Alignment != WdParagraphAlignment.wdAlignParagraphCenter)
                    {
                        centered = false;
                    }

                    lineText += wordText;
                }
            }

            return lines;
        }

        private static void ShowProgress(double percentage)
        {
            string percent = String.Format("{0:000%}", percentage);
            Console.Write(percent);
            Console.Write("\b");
            Console.Write("\b");
            Console.Write("\b");
            Console.Write("\b");
        }
    }
}
