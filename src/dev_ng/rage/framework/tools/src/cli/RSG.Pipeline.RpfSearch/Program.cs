﻿//---------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.RpfSearch
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using RSG.Base.Configuration;
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;
    using RSG.Base.OS;
    using RSG.Pipeline.Core;
    using RSG.Platform;

    /// <summary>
    /// RPF file Search Program; wildcard and regular expression searching for
    /// file entries in RPF files.
    /// </summary>
    class Program
    {
        #region Constants
        private const String OPTION_WILDCARD = "wildcard";
        private const String OPTION_REGEX = "regex";
        private const String OPTION_RECURSIVE = "recursive";
        private const String OPTION_DUMPKEYS = "dumpkeys";
        #endregion // Constants

        #region Enumerations
        /// <summary>
        /// Search flags.
        /// </summary>
        [Flags]
        private enum SearchFlags
        {
            /// <summary>
            /// No flags.
            /// </summary>
            None,

            /// <summary>
            /// "Dump RPF encryption keys for opened RPFs to a named CSV file."
            /// </summary>
            DumpKeys
        }
        #endregion // Enumerations

        /// <summary>
        /// Entry-point.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        [STAThread]
        static int Main(String[] args)
        {
            // Merge our options with the default pipeline command options.
            LongOption[] opts = new LongOption[] {
                new LongOption(OPTION_WILDCARD, LongOption.ArgType.Required,
                    "Specify a wildcard to search zip files."),
                new LongOption(OPTION_REGEX, LongOption.ArgType.Required,
                    "Specify a regular expression to search zip files."),
                new LongOption(OPTION_RECURSIVE, LongOption.ArgType.None,
                    "Specify whether directories are searched recursively."),
                new LongOption(OPTION_DUMPKEYS, LongOption.ArgType.Required,
                    "Dump RPF encryption keys for opened RPFs to a named CSV file.")
            };
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.ApplicationLog;

            CommandOptions options = new CommandOptions(args, opts);
            if (options.ShowHelp)
            {
                ShowHelp(log, options);
                return (Constants.Exit_Failure);
            }

            // Verify options.
            if (!(options.ContainsOption(OPTION_WILDCARD) || options.ContainsOption(OPTION_REGEX) ||
                  options.ContainsOption(OPTION_DUMPKEYS)))
            {
                log.Error("Either {0}, {1} or {2} options are required to define search.",
                    OPTION_WILDCARD, OPTION_REGEX, OPTION_DUMPKEYS);
                ShowHelp(log, options);
                return (Constants.Exit_Failure);
            }
            if (options.ContainsOption(OPTION_WILDCARD) && options.ContainsOption(OPTION_REGEX))
            {
                log.Error("Either {0} or {1} options are required to define search but not both.",
                    OPTION_WILDCARD, OPTION_REGEX);
                ShowHelp(log, options);
                return (Constants.Exit_Failure);
            }

            bool recursive = options.ContainsOption(OPTION_RECURSIVE);
            List<String> filenames = new List<String>();
            foreach (String trailing in options.TrailingArguments)
            {
                String filename = Path.GetFullPath(trailing);

                if (File.Exists(filename))
                    filenames.Add(filename);
                else if (Directory.Exists(filename))
                {
                    filenames.AddRange(Directory.GetFiles(filename, "*.rpf",
                        recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly));
                }
                else
                    log.Warning("Trailing argument '{0}' not file or directory.  Ignoring.", filename);
            }
            
            // Determine search flags.
            SearchFlags searchFlags = SearchFlags.None;
            if (options.ContainsOption(OPTION_DUMPKEYS))
                searchFlags |= SearchFlags.DumpKeys;

            // Run search.
            ICollection<Tuple<String, UInt32>> encryptionKeys = new List<Tuple<String, UInt32>>();
            if (options.ContainsOption(OPTION_WILDCARD))
            {
                String wildcard = (String)options[OPTION_WILDCARD];
                RunWildcardSearch(log, wildcard, searchFlags, filenames, encryptionKeys);
            }
            else if (options.ContainsOption(OPTION_REGEX))
            {
                String regex = (String)options[OPTION_REGEX];
                RunRegexSearch(log, regex, searchFlags, filenames, encryptionKeys);
            }
            else if (options.ContainsOption(OPTION_DUMPKEYS))
            {
                // If we have only specified dump keys then we iterate through
                // all RPFs with a null Regex.
                RunRegexSearch(log, (Regex)null, searchFlags, filenames, encryptionKeys);
            }
            else
            {
                log.Error("Invalid search. {0} or {1} options are required to define search but not both.",
                    OPTION_WILDCARD, OPTION_REGEX);
                ShowHelp(log, options);
            }
            
            // Optionally dump the encryption keys into a CSV file.
            if (options.ContainsOption(OPTION_DUMPKEYS))
            {
                String filename = (String)options[OPTION_DUMPKEYS];
                filename = Path.GetFullPath(filename);

                log.Message("Dumping RPF encryption keys for {0} RPFs to: {1}.", encryptionKeys.Count, filename);
                using (StreamWriter sw = new StreamWriter(filename))
                {
                    // CSV header.
                    sw.WriteLine("RPF Filename,RPF Encryption Key");
                    foreach (Tuple<String, uint> key in encryptionKeys)
                        sw.WriteLine(String.Format("{0},0x{1:X8}", key.Item1, key.Item2));
                }
            }

            return (LogFactory.HasError() ? Constants.Exit_Failure : Constants.Exit_Success);
        }


        #region Private Methods
        /// <summary>
        /// Run wildcard string search.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="wildcard"></param>
        /// <param name="flags"></param>
        /// <param name="filenames"></param>
        /// <param name="keys"></param>
        private static void RunWildcardSearch(IUniversalLog log, String wildcard, SearchFlags flags,
            IEnumerable<String> filenames, ICollection<Tuple<String, UInt32>> keys)
        {
            RSG.Base.IO.Wildcard wc = new RSG.Base.IO.Wildcard(wildcard, RegexOptions.IgnoreCase);
            RunRegexSearch(log, wc, flags, filenames, keys);
        }

        /// <summary>
        /// Run regex string search.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="regex"></param>
        /// <param name="flags"></param>
        /// <param name="filenames"></param>
        /// <param name="keys"></param>
        private static void RunRegexSearch(IUniversalLog log, String regex, SearchFlags flags,
            IEnumerable<String> filenames, ICollection<Tuple<String, UInt32>> keys)
        {
            Regex re = new Regex(regex);
            RunRegexSearch(log, re, flags, filenames, keys);
        }

        /// <summary>
        /// Run Regex search (base search implementation).
        /// </summary>
        /// <param name="log"></param>
        /// <param name="regex"></param>
        /// <param name="flags"></param>
        /// <param name="filenames"></param>
        /// <param name="keys"></param>
        private static void RunRegexSearch(IUniversalLog log, Regex regex, SearchFlags flags,
            IEnumerable<String> filenames, ICollection<Tuple<String, UInt32>> keys)
        {
            List<String> matches = new List<String>();
            foreach (String filename in filenames)
            {
                matches.Clear();
                using (FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read))
                {
                    using (RSG.ManagedRage.Packfile pf = new RSG.ManagedRage.Packfile())
                    {
                        if (pf.Load(filename))
                        {
                            keys.Add(new Tuple<String, UInt32>(filename, pf.EncryptionKey));

                            foreach (RSG.ManagedRage.PackEntry pe in pf.Entries)
                            {
                                if (null != regex && regex.IsMatch(pe.Name))
                                    matches.Add(pe.Name);

                                try
                                {
                                    String extension = Path.GetExtension(pe.Name);
                                    if (extension.StartsWith(".") && extension.Length > 1)
                                    {
                                        FileType ft = FileTypeUtils.ConvertExtensionToFileType(extension);
                                        if (FileType.RagePackFile == ft)
                                        {
                                            Byte[] data = pf.Extract(pe.Name);
                                            using (MemoryStream ms = new MemoryStream(data))
                                            {
                                                String nestedFilename = String.Format("{0}:{1}", filename, pe.Name);
                                                RunRegexSearch(log, regex, flags, nestedFilename, ms, keys);
                                            }
                                        }
                                    }
                                }
                                catch (Exception)
                                {
                                    log.Error("Unable to read entry {0} in {1}.", pe.Index, filename);
                                }
                            }
                        }
                        else
                        {
                            log.Error("Failed to load RPF {0}.", filename);
                        }
                    }
                }
                if (matches.Count > 0)
                {
                    log.Message("{0} contains matches:", filename);
                    foreach (String match in matches)
                        log.Message("\t{0}", match);
                }
            }
        }

        /// <summary>
        /// Run Regex search (from stream, for nested RPF files).
        /// </summary>
        /// <param name="log"></param>
        /// <param name="regex"></param>
        /// <param name="flags"></param>
        /// <param name="filename"></param>
        /// <param name="stream"></param>
        /// <param name="keys"></param>
        private static void RunRegexSearch(IUniversalLog log, Regex regex, SearchFlags flags,
            String filename, Stream stream, ICollection<Tuple<String, UInt32>> keys)
        {
            List<String> matches = new List<String>();

            using (RSG.ManagedRage.Packfile pf = new RSG.ManagedRage.Packfile())
            {
                if (pf.Load(stream))
                {
                    keys.Add(new Tuple<String, UInt32>(filename, pf.EncryptionKey));

                    foreach (RSG.ManagedRage.PackEntry pe in pf.Entries)
                    {
                        if (null != regex && regex.IsMatch(pe.Name))
                            matches.Add(pe.Name);

                        try
                        {
                            String extension = Path.GetExtension(pe.Name);
                            if (extension.StartsWith(".") && extension.Length > 1)
                            {
                                FileType ft = FileTypeUtils.ConvertExtensionToFileType(extension);
                                if (FileType.RagePackFile == ft)
                                {
                                    Byte[] data = pf.Extract(pe.Name);
                                    using (MemoryStream ms = new MemoryStream(data))
                                    {
                                        String nestedFilename = String.Format("{0}:{1}", filename, pe.Name);
                                        RunRegexSearch(log, regex, flags, nestedFilename, ms, keys);
                                    }
                                }
                            }
                        }
                        catch (Exception)
                        {
                            log.Error("Unable to read entry {0} in {1}.", pe.Index, filename);
                        }
                    }
                }
                else
                {
                    log.Error("Failed to load RPF {0}.", filename);
                }
            }
        }

        /// <summary>
        /// Display help info to log.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="options"></param>
        private static void ShowHelp(IUniversalLog log, CommandOptions options)
        {
            log.Message(options.GetUsage("files", "RPF files or directories to search."));
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.RpfSearch namespace
