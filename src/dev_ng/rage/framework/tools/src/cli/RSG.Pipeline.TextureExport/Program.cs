﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using RSG.Base.Configuration;
using RSG.Base.OS;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Model.Asset;
using RSG.Model.Asset.Level;
using RSG.Model.Common;
using RSG.Model.Common.Map;
using RSG.Model.Map3;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;
using P4API;

namespace RSG.Pipeline.TextureExport
{
    class Program
    {
        #region Constants
        private static readonly String LOG_CTX = "Build Textures";
        private static readonly String OPTION_REFERENCES = "references";
        private static readonly String OPTION_LEVEL = "level";
        private static readonly String OPTION_CONVERT_LOCAL = "local";
        private static readonly String TEXTUREEXPORTPATH = "$(toolsroot)/ironlib/bin/TextureExport.exe";
        private static readonly String PROMPTPATH = "$(toolsroot)/ironlib/prompt.bat";
        private static readonly String PACKAGEBUILDCMD = "$(toolsbin)/ironruby/bin/ir.exe $(tools)/ironlib/util/data_convert_file.rb --rebuild ";
        private static String ARTCACHEDIRECTORY = "$(art)/cache/textureexport";

        #endregion

        #region Properties
        //Used to checkout texture export output prior to exporting. 
        private static P4 PerforceConnection;

        private static IUniversalLog Log;
        private static RSG.Base.ConfigParser.ConfigGameView gv;
        private static ContentTreeHelper contentHelper;
        private static IConfig config;
        #endregion

        [STAThread]
        static int Main(string[] args)
        {
            int result = Constants.Exit_Success;
            Getopt options = new Getopt(args, new LongOption[] {
                new LongOption(OPTION_REFERENCES, LongOption.ArgType.Optional, 'r'),
                new LongOption(OPTION_CONVERT_LOCAL, LongOption.ArgType.Optional, 'c'),
                new LongOption(OPTION_LEVEL, LongOption.ArgType.Required, 'l')
            });


            config = ConfigFactory.CreateConfig();

            if (options.HasOption(OPTION_LEVEL) == false)
            {
                Debug.Assert(true, "No level specified.  Aborting...");
                result = Constants.Exit_Failure;
                return result;
            }

            ARTCACHEDIRECTORY = config.Project.DefaultBranch.Environment.Subst(ARTCACHEDIRECTORY);
            string levelName = config.Project.DefaultBranch.Environment.Subst((string)options.Options[OPTION_LEVEL]);
            bool buildReferences = options.HasOption(OPTION_REFERENCES);
            bool buildLocally = options.HasOption(OPTION_CONVERT_LOCAL);

            if (buildLocally && buildReferences)
            {
                Debug.Assert(true, "Cannot have both building references and build locally! Doesn't make sense.");
                result = Constants.Exit_Failure;
                return result;
            }

            if (options.TrailingOptions.Length == 0 )
            {
                Debug.Assert(true, "No texture specified.  Aborting...");
                result = Constants.Exit_Failure;
                return result;
            }

            string[] filesArray = options.TrailingOptions;
            List<String> files = new List<String>();
            for (int index = 0; index < filesArray.Length; ++index)
            {
                if ( System.IO.Directory.Exists(filesArray[index]) == true )
                {
                    string[] dirFiles = System.IO.Directory.GetFiles(filesArray[index], "*.*");
                    foreach(string dirFile in dirFiles)
                    {
                        string normalizedDirFile = dirFile.Replace("\\", "/");
                        files.Add(normalizedDirFile);
                    }
                }
                else
                {
                    filesArray[index] = filesArray[index].Replace("\\", "/");
                    files.Add(filesArray[index]);
                }
            }
            files.Sort((x, y) => String.Compare(x, y, true));

            //Init logger
            LogFactory.Initialize();
            Log = LogFactory.CreateUniversalLog("RSG.Pipeline.TextureExport");
            IUniversalLogTarget LogFile = LogFactory.CreateUniversalLogFile(Log);
            LogFactory.CreateApplicationConsoleLogTarget();


            foreach (string file in files)
            {

                if (!buildLocally)
                {
                    PerforceConnection = new P4();
                    PerforceConnection.Connect();
                    // Load GameView
                    gv = new RSG.Base.ConfigParser.ConfigGameView();  
                    //Handle updating the pacakages
                    Dictionary<String, List<IMapArchetype>> referencedArchetypes = new Dictionary<String, List<IMapArchetype>>();
                    referencedArchetypes.Add(file, new List<IMapArchetype>());
                    ILevel level = LoadLevel(levelName);
              
                    // Load Content-Tree.  This is required for the SceneCollection. 
                    IProcessorCollection processors = new ProcessorCollection(config);
                    IContentTree tree = Factory.CreateTree(config.Project.DefaultBranch, processors);
                    contentHelper = new ContentTreeHelper(tree, processors);

                    level.MapHierarchy.AllSections.AsParallel().ForAll(section =>
                    {
                        foreach (IMapArchetype archetype in section.Archetypes)
                        {
                            foreach (ITexture texture in archetype.Textures)
                            {
                                if (String.Compare(file, texture.Filename, true) == 0)
                                {
                                    lock (referencedArchetypes)
                                    {
                                        referencedArchetypes[file].Add(archetype);
                                    }
                                }
                                else if (texture.AlphaFilename != null && String.Compare(file, texture.AlphaFilename, true) == 0)
                                {
                                    lock (referencedArchetypes)
                                    {
                                        referencedArchetypes[file].Add(archetype);
                                    }
                                }

                            }
                        }
                    });

                    //Getting list of all subdirectories and going to check if the file exists in each subdirectory
                    //at the exact name of the file instead of comparing it against every file
                    Dictionary<String, IMapArchetype> expandedParentPackagePaths = new Dictionary<String, IMapArchetype>();
                    string[] subdirectoryPaths = System.IO.Directory.GetDirectories(config.Project.DefaultBranch.Export, "*", SearchOption.AllDirectories);
                    string fullPathToTest = String.Empty;

                    if (referencedArchetypes[file].Count == 0)
                    {
                        Log.ErrorCtx(LOG_CTX, "\nNo references found for {0}. Must be used to generate formatted output properly.", file);
                        return Constants.Exit_Failure;
                    }

                    foreach (IMapArchetype archetype in referencedArchetypes[file].OrderBy(x => x.ParentSection.Name))
                    {
                        CheckForFileLocation(file, subdirectoryPaths, archetype, ref expandedParentPackagePaths);
                    }

                    //Export the texture
                    Log.MessageCtx(LOG_CTX, "\nExporting Texture: {0}", file);
                    if (!ExportManagedTextureFile(file, (MapArchetypeBase)expandedParentPackagePaths[expandedParentPackagePaths.Keys.First()], expandedParentPackagePaths.Keys.First()))
                    {
                        result = Constants.Exit_Failure;
                        continue;
                    }

                    if (buildReferences)
                    {
                        //rebuild all file paths in expandedParentPackagePaths
                        /*
                         * Parallize this section in order to get some good speed up
                         * as each .zip will be built in order at the moment
                         * 
                         * */
                        foreach (string package in expandedParentPackagePaths.Keys)
                        {
                            Log.MessageCtx(LOG_CTX, "About to build .zip: {0}", package);
                            RebuildZipFile(package);
                        }
                    }
                }
                else //build locally, fast conversion
                {
                    if (!System.IO.Directory.Exists(ARTCACHEDIRECTORY))
                    {
                        System.IO.Directory.CreateDirectory(ARTCACHEDIRECTORY);
                    }
                    ExportTextureFile(file, String.Format("{0}\\{1}", ARTCACHEDIRECTORY, Path.GetFileName(file)), false);
                }
            }
            //Cleanup
            if (PerforceConnection != null)
            {
                PerforceConnection.Disconnect();
            }

            return result;
        }

        #region Helper Functions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <param name="subdirectoryPaths"></param>
        /// <param name="archetype"></param>
        /// <param name="expandedParentPackagePaths"></param>
        /// <returns></returns>
        private static void CheckForFileLocation(string file, string[] subdirectoryPaths, IMapArchetype archetype, ref Dictionary<String, IMapArchetype> expandedParentPackagePaths)
        {
            //Function to help jump out of checking through all subdirectories if a unique file exists on disk and in expandedParentPackagePaths
            IContentNode[] pathNodes = contentHelper.GetAllMapSceneXmlNodes();

            foreach (Content.File path in pathNodes)
            {
                //Searching all scene xml
                if (String.Compare(path.Basename, archetype.ParentSection.Name) == 0)
                {
                    //Found a Map content node that matches the found reference
                    foreach (Content.Process process in contentHelper.GetAllMapExportProcesses())
                    {
                        //Find all export processes in order to find output mapping
                        foreach (IContentNode input in process.Inputs)
                        {
                            //Search every process's input
                            if (String.Compare(input.Name, path.Basename) == 0)
                            {
                                //found a processor that uses this input
                                foreach (Content.File output in process.Outputs)
                                {
                                    if(!expandedParentPackagePaths.ContainsKey(output.AbsolutePath))
                                    {
                                        expandedParentPackagePaths.Add(output.AbsolutePath, archetype);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private static bool ExportTextureFile(String srcPath, String destPath, bool useP4)
        {
            bool bExportSuccess = true; 
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo.FileName = config.Project.DefaultBranch.Environment.Subst(TEXTUREEXPORTPATH);
            bool bTexOutputExistsOnDisk = System.IO.File.Exists(destPath);

            if (bTexOutputExistsOnDisk && useP4)
            {
                //Checkout file in order to make destination writeable
                try
                {
                    PerforceConnection.Run("edit", destPath);
                }
                catch (System.InvalidOperationException e)
                {
                    //The background thread that is set to run P4 connection failed
                    //  writing an error to inform the user and making the file writeable to
                    //  not hold up process
                    FileInfo fileInfo = new FileInfo(destPath);
                    fileInfo.IsReadOnly = false;
                    fileInfo.Refresh();
                    Log.ErrorCtx(LOG_CTX, "Perforce has failed to checkout file while exporting: {0}", destPath);
                    bExportSuccess = false;
                }
                catch (P4API.Exceptions.PerforceInitializationError e)
                {
                    //The background thread that is set to run P4 connection failed
                    //  writing an error to inform the user and making the file writeable to
                    //  not hold up process
                    FileInfo fileInfo = new FileInfo(destPath);
                    fileInfo.IsReadOnly = false;
                    fileInfo.Refresh();
                    Log.ErrorCtx(LOG_CTX, "Perforce has failed to checkout file while exporting: {0}", destPath);
                    bExportSuccess = false;
                }
                catch
                {
                    Log.WarningCtx(LOG_CTX, "P4 error while exporting: {0}.\nUser will need to checkout", destPath);
                }
                //Handle the different cases of exceptions.
                //   it doesn't exist -> mark for add
                //PerforceConncection.Run("add", xmlEle.InnerText);                 
                //   it's already checked out -> leave alone?
            }


            process.StartInfo.Arguments = "-force -input " + srcPath + " -output " + destPath;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.Start();
            Log.MessageCtx(LOG_CTX, process.StandardOutput.ReadToEnd());
            process.WaitForExit();

            if (!bTexOutputExistsOnDisk && useP4)
            {
                try
                {
                    PerforceConnection.Run("add", destPath);
                }
                catch
                {
                    Log.ErrorCtx(LOG_CTX, "Perforce has failed to add new output file: {0} to changelist.", destPath);
                    bExportSuccess = false;
                }
            }
            return bExportSuccess;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filepath"></param>
        /// <param name="archetype"></param>
        /// <param name="exportZipFilePath"></param>
        /// <returns></returns>
        private static bool ExportManagedTextureFile(string filepath, MapArchetypeBase archetype, string exportZipFilePath)
        {
            //Function to re-export specified source texture file

            //Figure out how many texture outputs there are and where to put/name them
            Texture textureToExport = new Texture(filepath);
            string tcsFilePath = archetype.GetTcsFilenameForTexture(textureToExport, gv, exportZipFilePath);
            bool bExportSuccess = true;
            if (tcsFilePath == string.Empty)
            {
                Log.ErrorCtx(LOG_CTX, "TCS File not found locally. Make sure you have it in assets/metadata/textures/...\n Aborting this file...");
                return false;
            }

            //Parse tcs file for output per output location
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(tcsFilePath);
            XmlNodeList pathnameElements = xmlDoc.GetElementsByTagName("pathname");

            foreach (XmlElement xmlEle in pathnameElements)
            {
                ExportTextureFile(filepath, xmlEle.InnerText, true);
            }
            return bExportSuccess;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="zipPackagePath"></param>
        /// <returns></returns>
        private static void RebuildZipFile(string zipPackagePath)
        {
            //Rebuilding associated zipPackage
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo.FileName = config.Project.DefaultBranch.Environment.Subst(PROMPTPATH);
            process.StartInfo.Arguments = config.Project.DefaultBranch.Environment.Subst(PACKAGEBUILDCMD + zipPackagePath);
            process.Start();
            process.WaitForExit();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="levelName"></param>
        /// <returns></returns>
        private static ILevel LoadLevel(string levelName)
        {
            // Get the level we are after
            ILevelFactory levelFactory = new LevelFactory();
            ILevelCollection levelCollection = levelFactory.CreateCollection(DataSource.SceneXml);
            levelCollection.LoadStats(new StreamableStat[] { StreamableLevelStat.ExportDataPath, StreamableLevelStat.ProcessedDataPath});

            ILevel level = levelCollection.FirstOrDefault(item => item.Name == config.Project.Name);

            // Check that we found the requested level
            if (level != null)
            {
                // Map hierarchy
                IMapFactory mapFactory = new MapFactory();
                level.MapHierarchy = mapFactory.CreateHierarchy(DataSource.SceneXml, level);

                object objectLock = new object();
                float progress = 0.0f;
                float increment = 1.0f / (float) level.MapHierarchy.AllSections.Count();

                level.MapHierarchy.AllSections.AsParallel().ForAll(section =>
                {
                    section.LoadStats(new StreamableStat[] { StreamableMapSectionStat.Archetypes, StreamableMapSectionStat.Entities });

                    lock (objectLock)
                    {
                        progress += increment;
                        Log.MessageCtx(LOG_CTX, "\rLoading level data...{0}%   ", (int)(progress*100));
                    }
                });

                progress = 1.0f;
                Log.MessageCtx(LOG_CTX, "\rLoading level data...{0}%   ", (int)(progress * 100));
            }

            return level;
        }
    }
        #endregion
}
