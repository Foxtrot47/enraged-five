﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.IO.Compression;
using System.Diagnostics;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Services.AssetProcessor;
using System.IO;
using RSG.Platform;
using System.Text.RegularExpressions;
using RSG.ManagedRage.ClipAnimation;
using RSG.Export;

namespace RSG.Pipeline.AssetProcessor.Operations
{
    /// <summary>
    /// Process to suffix incoming UV animation and clip files with the shader index
    /// The shaderindex is taken from a map exported from rex, with the key being the material (shader) name
    /// </summary>
    internal class RenameAnimation : IAssetProcessorOperation
    {
        #region Constants
        const string PROPERTY_SUFFIX = "_DO_NOT_RESOURCE";
        const string PROPERTY_MATERIAL_NAME = "MaterialIndex" + PROPERTY_SUFFIX;
        #endregion //Constants

        #region Properties
        public Operation Operation
        {
            get { return Operation.RenameAnimation; }
        }
        #endregion


        #region IAssetProcessorOperation Interface Methods
        /// <summary>
        /// Rename an animation clip based on something defined somewhere BECAUSE RAGEBUILDER
        /// </summary>
        /// <param name="task"></param>
        /// <param name="options"></param>
        /// <param name="log"></param>
        /// <param name="inputs"></param>
        /// <param name="output"></param>
        /// <returns></returns>
        public bool Run(AssetProcessorTask task,
            CommandOptions options,
            IUniversalLog log,
            IEnumerable<Tuple<string, ZipArchive>> inputs,
            ZipArchive output)
        {
            ZipArchive containerIcd = null;
            Dictionary<string, ZipArchive> drawables = new Dictionary<string, ZipArchive>();

            if(!task.Elements.Any())
            {
                log.ErrorCtx("Assetprocessor RenameAnimation", "No animation cache directory was given to processor elements.");
                return false;
            }

            // Loop through out drawable inputs; 
            foreach (Tuple<string, ZipArchive> input in inputs)
            {
                string inputFilename = input.Item1;
                ZipArchive inputArchive = input.Item2;
                string assetName = Path.GetFileNameWithoutExtension(inputFilename);
                FileType filetype = FileTypeUtils.ConvertExtensionToFileType(Path.GetExtension(assetName));
                switch (filetype)
                {
                    case FileType.ClipDictionary:
                        {
                            Debug.Assert(null == containerIcd, "[Assetprocessor RenameAnimation] Multiple clip dictionaries found in container!");
                            containerIcd = inputArchive;
                            break;
                        }
                    case FileType.Drawable:
                    case FileType.Fragment:
                        {
                            drawables.Add(inputFilename, inputArchive);
                            break;
                        }
                    default:
                        log.DebugCtx("Assetprocessor RenameAnimation", "Skipping input file: {0}", inputFilename);
                        break;
                }
            }

            Debug.Assert(null != containerIcd, "[Assetprocessor RenameAnimation] No icd found in container.");
            Debug.Assert(drawables.Count <= 0, "[Assetprocessor RenameAnimation] No input drawables/fragments for container.");

            if (null == containerIcd)
            {
                return false;
            }

            foreach (ZipArchiveEntry inputEntry in containerIcd.Entries)
            {
                if(!Regex.IsMatch(inputEntry.FullName, "_UV")) ;
                {
                    CopyZipEntry(output, inputEntry, inputEntry.Name);
                    continue;
                }

                FileType filetype = FileTypeUtils.ConvertExtensionToFileType(Path.GetExtension(inputEntry.FullName));
                if (filetype != FileType.AnimFile)
                {
                    continue;
                }

                string animNameFull = Path.GetFileNameWithoutExtension(inputEntry.FullName);

                string animNameCropped = animNameFull.Substring(0, animNameFull.IndexOf("_UV"));
                if (!drawables.ContainsKey(animNameCropped))
                {
                    log.ErrorCtx(animNameCropped, "Couldn't find drawable for UV animation renaming.");
                    continue;
                }

                // find and load clip file
                string clipName = Path.Combine(animNameFull, ".clip");
                ZipArchiveEntry clipCounterPart = containerIcd.Entries.First(entry => entry.FullName == clipName);
                Uri animCache = task.Elements.First();
                string cachedClipPath = Path.Combine(animCache.ToString(), clipName);
                if (!File.Exists(cachedClipPath))
                {
                    log.ErrorCtx(animNameCropped, "Couldn't find cached version of clip file {0}.", cachedClipPath);
                    continue;
                }

                // Extract clip property information required to do the actual clip grouping
                MClip.Init();
                string mtlName = "";
                using (MClip rageClip = new MClip(MClip.ClipType.Normal))
                {
                    MemoryStream stream = new MemoryStream();
                    bool clipLoaded = (bool)rageClip.Load(cachedClipPath);

                    if (clipLoaded)
                    {
                        MProperty propMtlName = rageClip.FindProperty(PROPERTY_MATERIAL_NAME);
                        mtlName = (propMtlName.GetPropertyAttributes()[0] as MPropertyAttributeString).GetString();
                    }
                }

                // find and load rex report file
                ZipArchiveEntry rexReportEntry = drawables[animNameCropped].GetEntry("rexReport.xml");
                if (null == rexReportEntry)
                {
                    log.ErrorCtx(animNameCropped, "Couldn't find rexReport file in drawable for UV animation renaming.");
                    continue;
                }

                UInt32 shaderIndex = 0;

                using(Stream rexReportStream = rexReportEntry.Open())
                {
                    RexReport rexReport = RexReport.LoadMeta(rexReportStream, options.Branch, log);

                    if (!rexReport.MtlNameShaderMap.ContainsKey(mtlName))
                    {
                        log.Error("Materialname \"{0}\" from clip property not found in rexReport material-to-shaderindex map.", mtlName);
                        continue;
                    }

                    shaderIndex = rexReport.MtlNameShaderMap[mtlName];
                }

                string newAnimName = animNameFull + "_" + shaderIndex + "." + FileTypeUtils.GetPlatformExtension(FileType.AnimFile);
                CopyZipEntry(output, inputEntry, newAnimName);
                string newClipName = animNameFull + "_" + shaderIndex + "." + FileTypeUtils.GetPlatformExtension(FileType.ClipFile);
                CopyZipEntry(output, clipCounterPart, newClipName);
            }
            return true;
        }
        #endregion

        #region private methods
        ZipArchiveEntry CopyZipEntry(ZipArchive output, ZipArchiveEntry inputEntry, string newName)
        {
            ZipArchiveEntry outputEntry = output.CreateEntry(newName);
            using (Stream outputStream = outputEntry.Open())
            {
                using (Stream inputStream = inputEntry.Open())
                {
                    inputStream.CopyTo(outputStream);
                }
            }
            return outputEntry;
        }
        #endregion //private methods
    }
}
