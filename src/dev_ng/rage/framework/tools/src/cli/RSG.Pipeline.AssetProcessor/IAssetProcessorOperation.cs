﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Services.AssetProcessor;

namespace RSG.Pipeline.AssetProcessor
{

    /// <summary>
    /// Asset Processor Operation interface.
    /// </summary>
    internal interface IAssetProcessorOperation
    {
        /// <summary>
        /// Operation handled by this class.
        /// </summary>
        Operation Operation { get; }

        /// <summary>
        /// Run the operation over the input asset collection.
        /// </summary>
        /// <param name="task"></param>
        /// <param name="options"></param>
        /// <param name="log">Log</param>
        /// <param name="inputs">Input assets.</param>
        /// <param name="output">Output asset.</param>
        /// <returns>true iff successful; false otherwise.</returns>
        bool Run(AssetProcessorTask task,
            CommandOptions options,
            IUniversalLog log,
            IEnumerable<Tuple<string, ZipArchive>> inputs, 
            ZipArchive output);
    }

} // RSG.Pipeline.AssetProcessor namespace
