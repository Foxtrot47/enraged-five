﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Services.AssetProcessor;

namespace RSG.Pipeline.AssetProcessor.Operations
{

    /// <summary>
    /// Drawable Merge operation.
    /// </summary>
    internal class DrawableMergeOperation : IAssetProcessorOperation
    {
        #region Properties
        /// <summary>
        /// Operation handled by this class.
        /// </summary>
        public Operation Operation
        {
            get { return Operation.DrawableMerge; }
        }
        #endregion // Properties

        #region IAssetProcessorOperation Interface Methods
        /// <summary>
        /// Run the operation over the input asset collection.
        /// </summary>
        /// <param name="task"></param>
        /// <param name="options"></param>
        /// <param name="log">Log</param>
        /// <param name="inputs">Input assets.</param>
        /// <param name="output">Output asset.</param>
        /// <returns>true iff successful; false otherwise.</returns>
        public bool Run(AssetProcessorTask task, 
            CommandOptions options,
            IUniversalLog log,
            IEnumerable<Tuple<string, ZipArchive>> inputs,
            ZipArchive output)
        {
            // Loop through out drawable inputs; 
            foreach (Tuple<string, ZipArchive> input in inputs)
            {
                string inputFilename = input.Item1;
                ZipArchive inputArchive = input.Item2;
                string drawableName = Path.GetFileNameWithoutExtension(inputFilename);

                // Loop through all entries packing them into output
                // with a prefix of "drawableName" folder.
                foreach (ZipArchiveEntry inputEntry in inputArchive.Entries)
                {
                    Uri outputEntryName =  new Uri(string.Format(@"{0}/{1}",drawableName, inputEntry.FullName), UriKind.Relative);
                    
                    ZipArchiveEntry outputEntry = output.CreateEntry(outputEntryName.ToString());
                    using (Stream outputStream = outputEntry.Open())
                    {
                        using (Stream inputStream = inputEntry.Open())
                        {
                            inputStream.CopyTo(outputStream);
                        }
                    }
                }
            }

            return true;
        }
        #endregion // IAssetProcessorOperation Interface Methods
    }

} // RSG.Pipeline.AssetProcessor.DrawableMerge namespace
