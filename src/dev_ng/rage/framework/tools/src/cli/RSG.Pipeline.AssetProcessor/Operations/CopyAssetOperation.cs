﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Services.AssetProcessor;

namespace RSG.Pipeline.AssetProcessor.Operations
{
    /// <summary>
    /// Copy asset operation.
    /// </summary>
    internal class CopyAssetOperation : IAssetProcessorOperation
    {
        #region Properties
        /// <summary>
        /// Operation handled by this class.
        /// </summary>
        public Operation Operation
        {
            get { return Operation.Copy; }
        }
        #endregion // Properties

        #region IAssetProcessorOperation Interface Methods
        /// <summary>
        /// Run the operation over the input asset collection.
        /// </summary>
        /// <param name="task"></param>
        /// <param name="options"></param>
        /// <param name="log">Log</param>
        /// <param name="inputs">Input assets.</param>
        /// <param name="output">Output asset.</param>
        /// <returns>true iff successful; false otherwise.</returns>
        public bool Run(AssetProcessorTask task, CommandOptions options, IUniversalLog log, IEnumerable<Tuple<string, ZipArchive>> inputs, ZipArchive output)
        {
            // Loop through out drawable inputs; 
            foreach (Tuple<string, ZipArchive> input in inputs)
            {
                ZipArchive inputArchive = input.Item2;

                // Loop through all entries packing them into output.
                foreach (ZipArchiveEntry inputEntry in inputArchive.Entries)
                {
                    string outputEntryName = inputEntry.FullName;
                    ZipArchiveEntry outputEntry = output.CreateEntry(outputEntryName);
                    using (Stream outputStream = outputEntry.Open())
                    {
                        using (Stream inputStream = inputEntry.Open())
                        {
                            inputStream.CopyTo(outputStream);
                        }
                    }
                }
            }

            return true;
        }
        #endregion // IAssetProcessorOperation Interface Methods
    }

} // RSG.Pipeline.AssetProcessor.CopyAsset namespace
