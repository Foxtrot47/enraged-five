﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Services.AssetProcessor;

namespace RSG.Pipeline.AssetProcessor.Operations
{
    internal class MergeFilesOperation : IAssetProcessorOperation
    {
        public Operation Operation
        {
            get { return Operation.MergeFiles; }
        }

        public bool Run(AssetProcessorTask task, 
            CommandOptions options, 
            IUniversalLog log, 
            IEnumerable<Tuple<string, ZipArchive>> inputs, 
            ZipArchive output)
        {
            // entriesFullName keeps track of previously packed files
            // each time we add one entry, we try to see if it already exists in this hashset
            HashSet<string> entriesFullName = new HashSet<string>();

            foreach (Tuple<string, ZipArchive> input in inputs)
            {
                ZipArchive inputArchive = input.Item2;

                foreach (ZipArchiveEntry inputEntry in inputArchive.Entries)
                {
                    if (entriesFullName.Add(inputEntry.FullName))
                    {
                        ZipArchiveEntry outputEntry = output.CreateEntry(inputEntry.FullName);
                        using (Stream outputStream = outputEntry.Open())
                        {
                            using (Stream inputStream = inputEntry.Open())
                            {
                                inputStream.CopyTo(outputStream);
                            }
                        }
                    }
                    else
                    {
                        log.ToolErrorCtx("MergeFilesOperations", "An entry with the same name is already present in the merged file:\n{0}", inputEntry.FullName);
                    }
                }
            }

            return true;
        }
    }
}
