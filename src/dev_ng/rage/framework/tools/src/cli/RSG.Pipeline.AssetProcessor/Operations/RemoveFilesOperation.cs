﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Services.AssetProcessor;

namespace RSG.Pipeline.AssetProcessor.Operations
{
    /// <summary>
    /// This operation takes an input asset and removes .
    /// </summary>
    internal class RemoveFilesOperation : IAssetProcessorOperation
    {
        public Operation Operation
        {
            get { return Operation.RemoveFiles; }
        }

        public bool Run(AssetProcessorTask task, 
            CommandOptions options, 
            IUniversalLog log, 
            IEnumerable<Tuple<string, ZipArchive>> inputs, 
            ZipArchive output)
        {
            var entriesNames = output.Entries.Select(entry => entry.FullName).ToList();

            foreach (Uri element in task.Elements)
            {
                if (entriesNames.Contains(element.ToString()))
                {
                    var entry = output.GetEntry(element.ToString());
                    entry.Delete();
                }
            }

            return true;
        }
    }
}