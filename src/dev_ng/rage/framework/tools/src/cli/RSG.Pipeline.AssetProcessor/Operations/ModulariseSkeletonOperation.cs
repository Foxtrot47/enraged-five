﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Services.AssetProcessor;

namespace RSG.Pipeline.AssetProcessor.Operations
{
    /// <summary>
    /// This Operation takes the skeleton meta and modularises it into skel files.
    /// </summary>
    internal class ModulariseSkeletonOperation : IAssetProcessorOperation
    {
        public Operation Operation
        {
            get { return Operation.ModulariseSkeleton; }
        }

        public bool Run(AssetProcessorTask task, CommandOptions options, IUniversalLog log, IEnumerable<Tuple<string, ZipArchive>> inputs, ZipArchive output)
        {
            return true;
        }
    }
}
