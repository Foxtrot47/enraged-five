﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;
using RSG.Model.MaterialPresets;
using RSG.Pipeline.Services.AssetProcessor;

namespace RSG.Pipeline.AssetProcessor.Operations
{
    /// <summary>
    /// Material Preset operation.
    /// </summary>
    internal class MaterialPresetOperation : IAssetProcessorOperation
    {
        #region Constants
        private const string LoggingContext = "Material Preset Converter";
        private const string OPT_INPUT = "input";
        private const string OPT_OUTPUT = "output";
        private const string OPT_FORCE = "force";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Operation handled by this class.
        /// </summary>
        public Operation Operation 
        {
            get { return Operation.MaterialPreset; } 
        }
        #endregion // Properties

        #region IAssetProcessorOperation Interface Methods
        /// <summary>
        /// Run the operation over the input asset collection.
        /// </summary>
        /// <param name="task"></param>
        /// <param name="options"></param>
        /// <param name="log">Log</param>
        /// <param name="inputs">Input assets.</param>
        /// <param name="output">Output asset.</param>
        /// <returns>true iff successful; false otherwise.</returns>
        public bool Run(AssetProcessorTask task, CommandOptions options, IUniversalLog log, IEnumerable<Tuple<string, ZipArchive>> inputs, ZipArchive output)
        {
            // Material Preset only supports ift as input and output
            IEnumerable<Tuple<string, ZipArchive>> tupleInputs = inputs as IList<Tuple<string, ZipArchive>> ?? inputs.ToList();
            IEnumerable<Tuple<string, ZipArchive>> nonFragmentOrDrawable = tupleInputs.Where(input => !input.Item1.EndsWith("ift.zip") && !input.Item1.EndsWith("idr.zip"));

            if (nonFragmentOrDrawable.Any())
            {
                foreach (var tuple in nonFragmentOrDrawable)
                {
                    log.ErrorCtx(LoggingContext, "Input {0} is not a drawable or a fragment.", tuple.Item1);
                }

                return false;
            }

            foreach (var input in tupleInputs)
            {
                //Find any template files and convert them.
                IEnumerable<ZipArchiveEntry> entries = input.Item2.Entries.Where(entry => IsMaterialPreset(entry.Name));

                foreach (ZipArchiveEntry entry in entries)
                {
                    using (Stream entryStream = entry.Open())
                    {
                        using (MemoryStream ms = new MemoryStream())
                        {
                            entryStream.CopyTo(ms);

                            MaterialPreset preset;
                            bool presetLoading = MaterialPreset.LoadPreset(options.Branch, log, ms, out preset, options.Config);

                            if (presetLoading == false)
                            {
                                log.ErrorCtx(LoggingContext, "Unable to parse preset {0} within drawable/fragment {1}.", entry.Name, input.Item1);
                                continue;
                            }

                            //Expand the preset to include all values from its parents.
                            preset.ExpandPreset();

#warning Flo: temporary : write out as a file, reopen, grab the stream, put in zip
                            //Save the shader variable file.
                            string svaFilename = Path.GetTempFileName(); // Path.Combine(Path.GetDirectoryName(entry), Path.GetFileNameWithoutExtension(entry.Name) + ".sva");
                            preset.WriteSvaFile(svaFilename);

                            using (FileStream fs = File.OpenRead(svaFilename))
                            {
                                ZipArchiveEntry outputEntry = output.CreateEntry(entry.FullName);
                                using (Stream outputStream = outputEntry.Open())
                                {
                                    //preset.WriteSvaFile();.CopyTo(outputStream);
                                    fs.CopyTo(outputStream);
                                }
                            }

#warning Flo: remove when we got a stream available for material preset rage tokenizer thingies
                            // delete temporary created file
                            File.Delete(svaFilename);
                        }
                    }
                }
            }
            return true;
        }

        private static bool IsMaterialPreset(string filename)
        {
            string extension = Path.GetExtension(filename);
            if (string.IsNullOrEmpty(extension))
                return false;

            return extension.Equals(MaterialPreset.MaterialObjectExtension, StringComparison.OrdinalIgnoreCase) || extension.Equals(MaterialPreset.MaterialTemplateExportExtension, StringComparison.OrdinalIgnoreCase);
        }
        #endregion // IAssetProcessorOperation Interface Methods
    }

} // RSG.Pipeline.AssetProcessor.MaterialPreset namespace
