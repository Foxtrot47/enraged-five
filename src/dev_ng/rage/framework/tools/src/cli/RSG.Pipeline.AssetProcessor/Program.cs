﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.AssetProcessor.Operations;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services.AssetProcessor;

namespace RSG.Pipeline.AssetProcessor
{
    /// <summary>
    /// Asset Processor main class.
    /// </summary>
    class Program
    {
        #region Constants
        /// <summary>
        /// Log context.
        /// </summary>
        private const string LOG_CTX = "Asset Processor";
        private const string OPT_CONFIG = "config";
        private const string OPT_TASKNAME = "taskname";
        #endregion // Constants

        /// <summary>
        /// Asset Processor entry-point.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        [STAThread]
        private static int Main(string[] args)
        {
            // Merge our options with the default pipeline command options.
            LongOption[] opts =
            {
                new LongOption(OPT_CONFIG, LongOption.ArgType.Required, "Asset Processor configuration filename."),
                new LongOption(OPT_TASKNAME, LongOption.ArgType.Required, "XGE taskname; echoed to TTY for XGE error log parsing in AP3."),
            };

            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.ApplicationLog;
            CommandOptions options = new CommandOptions(args, opts);

            IEnumerable<AssetProcessorConfig> configs = null;
            try
            {
                // Output task name for AP3 XGE logging compatibility.
                if (options.ContainsOption(OPT_TASKNAME))
                {
                    Console.WriteLine("TASK: {0}", options[OPT_TASKNAME]);
                    Console.Out.Flush();
                }

                // Verify options.
                if (options.ShowHelp)
                {
                    Console.WriteLine(options.GetUsage());
                    return Constants.Exit_Success;
                }

                if (!options.ContainsOption(OPT_CONFIG))
                {
                    Console.Error.WriteLine("'{0}' is a required option.", OPT_CONFIG);
                    Console.WriteLine(options.GetUsage());
                    return Constants.Exit_Failure;
                }

                // try to load config file. The config file is responsible for its state validation.
                string configFilename = (string)options[OPT_CONFIG];
                configs = AssetProcessorConfig.Load(configFilename, log);

                if (configs == null)
                {
                    log.ToolErrorCtx(LOG_CTX, "Error while loading Asset Processor's config file. Aborting.");
                    return Constants.Exit_Failure;
                }

                if (!configs.Any())
                {
                    log.ToolErrorCtx(LOG_CTX, "No AssetProcessorConfig parsed in the config file. Aborting.");
                    return Constants.Exit_Failure;
                }
            }
            catch (Exception exception)
            {
                log.ToolExceptionCtx(LOG_CTX, exception, "The AssetProcessor throw an exception. Aborting.");
            }

            // init the exitCode with success
            // the finally{} clause is not here to change this state, just ensure that the streams are closed and flushed.
            // do not change the exitCode within finally{}
            int exitCode = Constants.Exit_Success;
            foreach (AssetProcessorConfig config in configs)
            {
                // We compress an in memory stream, so that we perform everything in mem, and avoid reading and writing from/to disk
                Stream outputStream = new MemoryStream();

                List<Tuple<string, ZipArchive>> inputAssets = null;
                try
                {
                    // enforcing at least an empty inputAssets collection
                    // prevents a null ref exception in the finally{} clause if an exception is raised
                    inputAssets = new List<Tuple<string, ZipArchive>>();

                    // load inputs from config and open every archive
                    IEnumerable<string> filenames = config.Inputs.Select(filename => filename.AbsolutePath);

                    bool fileErrors = false;
                    foreach (string filename in filenames)
                    {
                        if (File.Exists(filename))
                        {
                            inputAssets.Add(new Tuple<string, ZipArchive>(filename, ZipFile.Open(filename, ZipArchiveMode.Read)));
                        }
                        else
                        {
                            fileErrors = true;
                            log.ErrorCtx(LOG_CTX, "Input file not found: {0}.", filename);
                        }
                    }
                    
                    if (fileErrors)
                    {
                        log.ErrorCtx(LOG_CTX, "Skipping AssetProcessor for {0}.", config.Output.AbsolutePath);
                        // if we had an error in the previous iteration, just skip the current asset processor, and carry on with the next one
                        continue;
                    }

                    IEnumerable<AssetProcessorTask> tasks = config.Operations;

                    // Magic of ZipArchive we can new it in Update mode and it works anyway. Yay
                    using (ZipArchive outputAsset = new ZipArchive(outputStream, ZipArchiveMode.Update, true))
                    {
                        // Run operations in sequence.
                        foreach (AssetProcessorTask task in tasks)
                        {
                            bool operationResult = RunOperation(task, options, log, inputAssets, outputAsset);

                            if (operationResult == false)
                            {
                                exitCode = Constants.Exit_Failure;
                                break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.ToolExceptionCtx(LOG_CTX, ex, "Unhandled exception during Asset Processing.");
                    exitCode = Constants.Exit_Failure;
                }
                finally
                {
                    // Cleanup inputs.
                    foreach (Tuple<string, ZipArchive> inputAsset in inputAssets)
                    {
                        inputAsset.Item2.Dispose();
                    }

                    string outputPath = Path.GetDirectoryName(config.Output.AbsolutePath);
                    // check for output path existence and create the directory if not
                    if (!Directory.Exists(outputPath))
                        Directory.CreateDirectory(outputPath);

                    // TODO Flo: if (exit_code == fail) do not write ?
                    if (log.HasErrors || Log.StaticLog.HasErrors)
                        exitCode = Constants.Exit_Failure;

                    // Since output ZipArchive has been disposed, we can write the memorystream to a file
                    log.MessageCtx(LOG_CTX, "Writing output file: {0}", config.Output.AbsolutePath);
                    using (FileStream fileStream = new FileStream(config.Output.AbsolutePath, FileMode.Create))
                    {
                        outputStream.Seek(0, SeekOrigin.Begin);
                        outputStream.CopyTo(fileStream);
                    }

                    outputStream.Close();
                }
            }
            
            return exitCode;
        }

        /// <summary>
        /// Execute an Asset Processor operation.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="log"></param>
        /// <param name="task"></param>
        /// <param name="inputAssets"></param>
        /// <param name="outputAsset"></param>
        /// <returns></returns>
        private static bool RunOperation(AssetProcessorTask task,
            CommandOptions options,
            IUniversalLog log, 
            IEnumerable<Tuple<string, ZipArchive>> inputAssets, 
            ZipArchive outputAsset)
        {
            IAssetProcessorOperation operation;

            switch (task.Operation)
            {
                case Operation.Copy:
                    operation = new CopyAssetOperation();
                    break;

                case Operation.DrawableMerge:
                    operation = new DrawableMergeOperation();
                    break;

                case Operation.RenameAnimation:
                    operation = new RenameAnimation();
                    break;

                case Operation.MergeFiles:
                    operation = new MergeFilesOperation();
                    break;

                case Operation.AddFiles:
                    operation = new AddFilesOperation();
                    break;

                case Operation.RemoveFiles:
                    operation = new RemoveFilesOperation();
                    break;

                case Operation.MaterialPreset:
                    operation = new MaterialPresetOperation();
                    break;

                case Operation.ModulariseSkeleton:
                    operation = new ModulariseSkeletonOperation();
                    break;

                default:
                    log.ToolErrorCtx(LOG_CTX, "Invalid operation: {0}.  Internal tools error.", task.Operation);
                    return false;
            }

            return operation.Run(task, options, log, inputAssets, outputAsset);
        }
    }

} // RSG.Pipeline.AssetProcessor namespace
