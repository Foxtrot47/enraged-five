﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using RSG.Base.Configuration;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Services.AssetProcessor;

namespace RSG.Pipeline.AssetProcessor.Operations
{
    /// <summary>
    /// This Operation takes bound data and packs it into the output drawable
    /// </summary>
    internal class AddFilesOperation : IAssetProcessorOperation
    {
        public Operation Operation
        {
            get { return Operation.AddFiles; }
        }

        public bool Run(AssetProcessorTask task, CommandOptions options, IUniversalLog log, IEnumerable<Tuple<string, ZipArchive>> inputs, ZipArchive output)
        {
            foreach (Uri fileUri in task.Elements)
            {
                // Flo: AddFiles only support full path to files
                // we might want to improve that by providing a folder and list every directory/file in it
                // if that's the case, i recommend working on AssetProcessorConfig.Elements population instead
                string filepath = fileUri.LocalPath;

                if (!File.Exists(filepath))
                {
                    // TODO Flo: ILOG warning/error ?
                    continue;
                }

                string filename = Path.GetFileName(filepath);

                ZipArchiveEntry outputEntry = output.CreateEntry(filename);
                using (Stream outputStream = outputEntry.Open())
                {
                    using (FileStream fs = File.OpenRead(filepath))
                    {
                        fs.CopyTo(outputStream);
                    }
                }
            }

            return true;
        }
    }
}
