﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.OS;
using RSG.Base.Collections;
using RSG.Base.Configuration;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services.AssetPack;
using RSG.Pipeline.Services.Animation;
using RSG.Base.Logging.Universal;
using RSG.Base.Logging;

namespace RSG.Pipeline.Animation.ClipDictionary
{
    sealed class Program
    {

        #region Constants
        private static readonly int EXIT_SUCCESS = 0;
        private static readonly int EXIT_ERROR = 1;

        private static readonly String OPTION_CLIPXMLEDIT_EXE = "clipxmledit";
        private static readonly String OPTION_INPUTZIP = "inputzip";
        private static readonly String OPTION_OUTPUTDIR = "outputdir";
        private static readonly String OPTION_OUTPUTZIP = "outputzip";
        private static readonly String OPTION_GROUPDATA = "group";
        private static readonly String OPTION_MODDIR = "moddir";
        private static readonly String OPTION_TEMPLATEDIR = "templatedir";
        private static readonly String OPTION_SKELDIR = "skeldir";
        private static readonly String OPTION_CLIP_FIXUP = "cliprename";
        private static readonly String OPTION_DEFAULTSKEL = "defaultskeleton";
        private static readonly String OPTION_DLCPREFIX = "dlcprefix";
        private static readonly String OPTION_TASK_NAME = "taskname";
        private static readonly String LOG_CTX = "Clip Dictionary";

        #endregion // Constants

        static int RunProcess(string strExe, string strArgs, IUniversalLog log)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.Arguments = strArgs;
            startInfo.FileName = strExe;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.CreateNoWindow = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false;

            log.MessageCtx(LOG_CTX, strExe + " " + strArgs);

            Process proc = Process.Start(startInfo);
            String output = proc.StandardOutput.ReadToEnd();
            proc.WaitForExit();
            log.MessageCtx(LOG_CTX, output);

            return proc.ExitCode;
        }

        static void RecurseFolder(string folder, string parentFolder, List<Pair<String, String>> lst)
        {
            string[] files = Directory.GetFiles(folder);
            foreach (string file in files)
            {
                String destination = String.Format("{0}/{1}", parentFolder, Path.GetFileName(file));
                lst.Add(new Pair<String, String>(file, destination));
            }

            string[] directories = Directory.GetDirectories(folder);
            foreach (string dir in directories)
            {
                parentFolder += "/" + Path.GetFileName(dir);
                RecurseFolder(dir, parentFolder, lst);

                parentFolder = String.Empty;
            }
        }

        static void ExtractData(String inputZip, String outputDirectory)
        {
            if (!GroupFactory.IsDirectory(inputZip))
            {
                // Extract the zip 
                RSG.Pipeline.Services.Zip.ExtractAll(inputZip, outputDirectory, true);
            }
            else
            {
                // Copy all files from input dir to output dir
                string[] files = Directory.GetFiles(inputZip, "*.*", SearchOption.AllDirectories);
                foreach (string file in files)
                {
                    if (!Directory.Exists(Path.GetDirectoryName(file)))
                        Directory.CreateDirectory(Path.GetDirectoryName(file));
                    File.Copy(file, Path.Combine(outputDirectory, Path.GetFileName(file)));
                }
            }
        }

        static void MakeFilesInDirectoryWritable(String directory)
        {
            string[] files = Directory.GetFiles(directory, "*.*", SearchOption.AllDirectories);

            foreach (string file in files)
            {
                File.SetAttributes(file, FileAttributes.Normal);
            }
        }

        [STAThread]
        static int Main(string[] args)
        {
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.CreateUniversalLog("ClipDictionary");
            try
            {
                // Define our executable options.  Stick with the long options
                // (e.g. --opt) rather than the single character versions.
                Getopt options = new Getopt(args, new LongOption[] {
                    new LongOption(OPTION_CLIPXMLEDIT_EXE, LongOption.ArgType.Required, 
                        ""),
                    new LongOption(OPTION_INPUTZIP, LongOption.ArgType.Required,
                        "Input ZIP file."),
                    new LongOption(OPTION_OUTPUTDIR, LongOption.ArgType.Required,
                        "Output directory."),
                    new LongOption(OPTION_OUTPUTZIP, LongOption.ArgType.Required,
                        "Output ZIP file."),
                    new LongOption(OPTION_TASK_NAME, LongOption.ArgType.Required,
                        "Asset Pipeline 3 Task Name"),
                    new LongOption(OPTION_GROUPDATA, LongOption.ArgType.Optional,
                        ""),
                    new LongOption(OPTION_MODDIR, LongOption.ArgType.Required,
                        ""),
                    new LongOption(OPTION_TEMPLATEDIR, LongOption.ArgType.Required,
                        "Template directory."),
                    new LongOption(OPTION_SKELDIR, LongOption.ArgType.Required,
                        "Skeleton directory."),
                    new LongOption(OPTION_CLIP_FIXUP, LongOption.ArgType.Required,
                        ""),
                    new LongOption(OPTION_DEFAULTSKEL, LongOption.ArgType.Required,
                        "Default skeleton file."),
                    new LongOption(OPTION_DLCPREFIX, LongOption.ArgType.Required,
                        "DLC prefix for animations/clips."),
                });
                // Turn args into vars
                String clipXmlEditExe = options[OPTION_CLIPXMLEDIT_EXE] as String;
                String inputZip = options[OPTION_INPUTZIP] as String;
                String outputDirectory = options[OPTION_OUTPUTDIR] as String;
                String outputZip = options[OPTION_OUTPUTZIP] == null ? String.Empty : options[OPTION_OUTPUTZIP] as String;
                bool groupIt = options.HasOption(OPTION_GROUPDATA);
                String modDirectory = options[OPTION_MODDIR] as String;
                String templateDirectory = options[OPTION_TEMPLATEDIR] as String;
                String skelDirectory = options[OPTION_SKELDIR] as String;
                String clipFixupExe = options[OPTION_CLIP_FIXUP] as String;
                String defaultSkeleton = options[OPTION_DEFAULTSKEL] == null ? String.Empty : options[OPTION_DEFAULTSKEL] as String;
                String dlcPrefix = options[OPTION_DLCPREFIX] == null ? String.Empty : options[OPTION_DLCPREFIX] as String;
                String taskName = options[OPTION_TASK_NAME] == null ? String.Empty : options[OPTION_TASK_NAME] as String;

                if (String.Empty != taskName)
                {
                    Console.WriteLine("TASK: {0}", taskName);
                    log.MessageCtx(LOG_CTX, "TASK: {0}", taskName);
                }

                if (!GroupFactory.IsDirectory(inputZip) && !File.Exists(inputZip))
                {
                    log.ErrorCtx(LOG_CTX, "Zip file '%s' does not exist", inputZip);
                    return EXIT_ERROR;
                }

                //log.MessageCtx(LOG_CTX, "Creating directory '{0}'", outputDirectory);
                //if (!RSG.Pipeline.Services.Animation.DirectoryServices.CreateDirectory(outputDirectory))
                //{
                //    log.ErrorCtx(LOG_CTX, "Failed to create directory: '{0}'.  Aborting.", outputDirectory);
                //    return (Constants.Exit_Failure);
                //}

                System.Collections.Generic.IEnumerable<string> sourceFiles = new List<string>();

                if (!GroupFactory.IsDirectory(inputZip))
                    RSG.Pipeline.Services.Zip.GetFileList(inputZip, out sourceFiles);
                else
                    sourceFiles = Directory.GetFiles(inputZip, "*.*", SearchOption.AllDirectories).ToList();

                log.MessageCtx(LOG_CTX,"Checking '{0}' for '*.clipxml' files", inputZip);

                bool bNeedsProcessing = false;
                foreach (String file in sourceFiles)
                {
                    if (file.EndsWith(".clipxml"))
                    {
                        bNeedsProcessing = true;
                        break;
                    }
                }
            
               log.MessageCtx(LOG_CTX, "Processing '{0}'", inputZip);

                if (bNeedsProcessing)
                {
                    log.MessageCtx(LOG_CTX, "Processing '*.clipxml' files");
                    if (groupIt)
                    {
                       log.MessageCtx(LOG_CTX, "Performing grouping");
                       if (!GroupFactory.ExtractAndCreateClipGroups(log, inputZip, outputDirectory, modDirectory, templateDirectory, skelDirectory, clipFixupExe, defaultSkeleton, dlcPrefix, true)) return EXIT_ERROR;
                    }
                    else
                    {
                        ExtractData(inputZip, outputDirectory);
                    }

                    // Convert clips if any
                    string[] clipXmlFiles = Directory.GetFiles(outputDirectory, "*.clipxml", SearchOption.AllDirectories);

                    string clipXmlEditArgs = String.Format("-toclip -clips={0}", String.Join(",", clipXmlFiles));
                    if (RunProcess(clipXmlEditExe, clipXmlEditArgs, log) != 0)
                    {
                        if (Directory.Exists(outputDirectory))
                            Directory.Delete(outputDirectory, true);
                        return EXIT_ERROR;
                    }
                
                    log.MessageCtx(LOG_CTX, "Deleting '*.clipxml' files");

                    // Delete all clipxml files
                    for (int i = 0; i < clipXmlFiles.Length; ++i)
                    {
                        File.Delete(clipXmlFiles[i]);
                    }

                    if (outputZip != String.Empty)
                    {
                        log.MessageCtx(LOG_CTX, "Creating output zip file '{0}'", outputZip);

                        List<Pair<String, String>> lstFiles = new List<Pair<String, String>>();
                        RecurseFolder(outputDirectory, String.Empty, lstFiles);

                        IConfig config = ConfigFactory.CreateConfig();
                        IBranch branch = config.Project.DefaultBranch;

                        if (!RSG.Pipeline.Services.Zip.Create(branch, outputZip, lstFiles))
                        {
                            log.ErrorCtx(LOG_CTX, "Unable to create '{0}'", outputZip);
                            if (Directory.Exists(outputDirectory))
                                Directory.Delete(outputDirectory, true);
                            return EXIT_ERROR;
                        }
                    }
                }
                else
                {
                    log.MessageCtx(LOG_CTX, "Processing '*.clip' files");

                    if (groupIt)
                    {
                        log.MessageCtx(LOG_CTX, "Performing grouping");
                        if (!GroupFactory.ExtractAndCreateClipGroups(log, inputZip, outputDirectory, modDirectory, templateDirectory, skelDirectory, clipFixupExe, defaultSkeleton, dlcPrefix, false)) return EXIT_ERROR;
                    }
                    else
                    {
                        ExtractData(inputZip, outputDirectory);
                    }
                }

                MakeFilesInDirectoryWritable(outputDirectory);

                return EXIT_SUCCESS;
            }
            catch (Exception ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Unhandled Exception during animation Clip Dictionary Process");
                return EXIT_ERROR;
            }
            finally
            {
                LogFactory.ApplicationShutdown();
            }
        }
    }
}
