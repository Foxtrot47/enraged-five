﻿using System;
using SIO = System.IO;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Text;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Platform;
using RSG.UniversalLog.Util;
using RSG.Pipeline.Automation.ClientWorker.Jobs;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{
    abstract class IDBData
    {
        public String Name { get; set; }
        public String DBColumn { get; set; }
        public IDBData(String name, String dbColumn)
        {
            Name = name;
            DBColumn = dbColumn;
        }
    }

    class DBBigIntData : IDBData
    {
        public UInt64 Value { get; set; }

        public DBBigIntData(String name, String dbColumn)
              : base(name, dbColumn)
        {
            Value = 0;
        }
    }

    class DBFloatData : IDBData
    {
        public float Value { get; set; }
        public DBFloatData(String name, String dbColumn)
            : base(name, dbColumn)
        {
            Value = 0;
        }
    }

    abstract class IDBTables
    {
        protected static readonly String LOG_CTX = "IDBTables";

        public SmokeTestDBTable DBTable;
        public SmokeTestDBLookUpTable DBLookUpTable;
    }

    class SmokeTestDBTable
    {
        protected static readonly String LOG_CTX = "Smoke Test DB Table";
        protected static readonly bool DumpAllSQL = false;
        public String TableName;

        public SmokeTestDBTable(String tableName)
        {
            TableName = tableName;
        }

        public bool CreateTable(SqlConnection sqlConnection, IUniversalLog log, IList<String> columnDescs)
        {
            try
            {
                String createTableStatement = "CREATE TABLE " + TableName + "\n";
                createTableStatement += "(\n";

                foreach (String desc in columnDescs)
                {
                    String newLine = String.Format("{0} ,\n", desc);
                    createTableStatement += newLine;
                }
                createTableStatement += ");\n";

                SqlCommand createTableQuery = new SqlCommand(createTableStatement, sqlConnection);
                log.MessageCtx(LOG_CTX, "SQL: {0}", ToReadableString(createTableQuery));
                createTableQuery.ExecuteNonQuery();

                return true;
            }
            catch(System.Data.SqlClient.SqlException ex)
            {
                log.ErrorCtx(LOG_CTX, "SQL fail: " + ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Inserts one row into TableName.  Column names and values are passed via sqlParams.  Returns the 
        /// identity column for the row just inserted.  Best used to insert 1 row of data.
        /// </summary>
        /// <param name="">sqlConnection</param>
        /// <param name="">sqlParams</param>
        /// <param name="">log</param>
        /// <returns>int</returns>
        /// 
        public int DumpToDB(SqlConnection sqlConnection, IList<SqlParameter> sqlParams, IUniversalLog log)
        {
            String[] cnames = new String[sqlParams.Count];
            String[] vnames = new String[sqlParams.Count];

            int i = 0;
            foreach (SqlParameter param in sqlParams)
            {
                cnames[i] = param.ParameterName.Replace("@", "");
                vnames[i] = param.ParameterName;
                i++;
            }
            String columns = String.Format("({0})", String.Join(",", cnames));
            String values = String.Format("({0})", String.Join(",", vnames));

            String insertString = String.Format("INSERT INTO {0} {1} VALUES {2};SELECT CAST(scope_identity() AS int)", 
                                        TableName, columns.ToString(), values.ToString());
            SqlCommand insertCommand = new SqlCommand(insertString, sqlConnection);

            foreach (SqlParameter param in sqlParams)
            {
                insertCommand.Parameters.Add(param);
            }

            LogSQL(log, insertCommand);

            int newID = (int)insertCommand.ExecuteScalar();

            return newID;
        }

        protected static void LogSQL(IUniversalLog log, SqlCommand sqlCommand)
        {
            if (DumpAllSQL)
            {
                log.MessageCtx(LOG_CTX, "SQL: {0}", ToReadableString(sqlCommand));
            }
            else
            {
                //log.MessageCtx(LOG_CTX, ".");
            }
        }

        /// <summary>
        /// Returns a String based on the input SqlCommand.  Great for debugging.
        /// </summary>
        /// <param name="">command</param>
        /// <returns>String</returns>
        /// 
        protected static String ToReadableString(SqlCommand command)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(command.CommandText);
            if (command.Parameters.Count > 0)
                builder.AppendLine("params:");
            foreach (SqlParameter param in command.Parameters)
            {
                builder.AppendFormat("     Paramater {0}: {1}", param.ParameterName,
                    (param.Value == null ? "NULL" : param.Value.ToString())).AppendLine();
            }
            return builder.ToString();
        }
        
        /// <summary>
        /// Returns true if TableName exists in the DB.  A table that exists also has rows
        /// </summary>
        /// <param name="">sqlConnection</param>
        /// <param name="">log</param>
        /// <returns>bool</returns>
        /// 
        public bool Exists(SqlConnection sqlConnection, IUniversalLog log)
        {
            String existQuery = String.Format("SELECT 1 as IsExists FROM dbo.sysobjects where id = object_id('{0}')", TableName);

            SqlCommand createTableQuery = new SqlCommand(existQuery, sqlConnection);
            LogSQL(log, createTableQuery);
            SqlDataReader reader = createTableQuery.ExecuteReader();

            bool exists = reader.HasRows;

            reader.Close();
            return exists;
        }

        /// <summary>
        /// Returns the number of total rows in the input table
        /// </summary>
        /// <param name="">sqlConnection</param>
        /// <param name="">tableName</param>
        /// <param name="">log</param>
        /// <returns>int</returns>
        /// 
        public int GetNumberRows(SqlConnection sqlConnection, String tableName, IUniversalLog log)
        {
            String rowsQuery = String.Format("SELECT * FROM {0}", tableName);

            SqlCommand rowsSqlQuery = new SqlCommand(rowsQuery, sqlConnection);
            LogSQL(log, rowsSqlQuery);
            SqlDataReader reader = rowsSqlQuery.ExecuteReader();

            int rows = 0;
            while (reader.Read())
            {
                rows++;
            }
            reader.Close();

            return rows;
        }
    }

    class SmokeTestDBLookUpTable : SmokeTestDBTable
    {
        public IDictionary<String, int> LookUpTable = new Dictionary<String, int>();
        public String LookUpColumn { get; set; }

        public SmokeTestDBLookUpTable(String tableName, String lookUpColumn)
            : base(tableName)
        {
            LookUpColumn = lookUpColumn;
        }

        public bool CreateTable(SqlConnection sqlConnection, IUniversalLog log)
        {
            log.WarningCtx(LOG_CTX, "{0} table NOT found.  Must create...", TableName);
            IList<String> columnDescs = new List<String>();
            columnDescs.Add(String.Format("{0} int NOT NULL PRIMARY KEY IDENTITY(1,1)", LookUpColumn));
            columnDescs.Add("Name varchar(200) NOT NULL");
            return CreateTable(sqlConnection, log, columnDescs);
        }

        /// <summary>
        /// Returns the unique int that matches key.  This is our lookup table.  Logs and error and returns -1 if
        /// key isn't found
        /// </summary>
        /// <param name="">key</param>
        /// <param name="">log</param>
        /// <returns>int</returns>
        /// 
        public int LookUpValue(String key, IUniversalLog log)
        {
            if (LookUpTable.ContainsKey(key))
            {
                return LookUpTable[key];
            }
            log.ErrorCtx(LOG_CTX, "LookUpValue({0}) NOT found", key);
            
            return -1;
        }

        /// <summary>
        /// The highest level of building the LookupTable dictionary based on columns in TableName.  Inserts new
        /// ID/Name pairs that do not yet appear in the DB.  Adds newly inserted pairs to the Dictionary.
        /// </summary>
        /// <param name="">sqlConnection</param>
        /// <param name="">log</param>
        /// <param name="">columnNames</param>
        /// <returns></returns>
        /// 
        public void CreateAndCacheDBColumnHeaders(SqlConnection sqlConnection, IUniversalLog log, String[] columnNames)
        {
            //cache our existing values
            CacheTable(sqlConnection, log, columnNames);

            int inserted = 0;
            String columns = "(Name)";
            String values = "(@Name)";
            String insertQuery = String.Format("INSERT INTO {0} {1} VALUES {2};", TableName, columns, values);
            SqlCommand insertQueryCommand = new SqlCommand(insertQuery, sqlConnection);

            // if we don't already have these values, insert them
            foreach (String column in columnNames)
            {
                if (!LookUpTable.ContainsKey(column))
                {
                    insertQueryCommand.Parameters.Add(new SqlParameter("@Name", column));
                    LogSQL(log, insertQueryCommand);
                    insertQueryCommand.ExecuteNonQuery();
                    inserted++;
                }
            }

            if (inserted > 0)
            {
                //cache our existing values
                CacheTable(sqlConnection, log, columnNames);
            }
            else
            {
                //log.MessageCtx(LOG_CTX, "{0} table found.  Keys already cached", TableName);
            }
        }

        /// <summary>
        /// Dumps all column_ID/column_Name pairs
        /// </summary>
        /// <param name="">log</param>
        /// <returns></returns>
        /// 
        protected void Dump(IUniversalLog log)
        {
            foreach (String key in LookUpTable.Keys)
            {
                log.MessageCtx(LOG_CTX, "cached: {0} {1}", key, LookUpTable[key]);
            }
        }

        /// <summary>
        /// Builds the LookupTable dictionary based on columns in TableName.  If columnName has not
        /// yet been cached, query the DB and add those that are not yet cached.
        /// </summary>
        /// <param name="">sqlConnection</param>
        /// <param name="">log</param>
        /// <param name="">columnNames</param>
        /// <returns></returns>
        /// 
        protected void CacheTable(SqlConnection sqlConnection, IUniversalLog log, String[] columnNames)
        {
            //log.MessageCtx(LOG_CTX, "CacheTable {0}", columnNames[0]);
            if (!LookUpTable.ContainsKey(columnNames[0]))
            {
                log.DebugCtx(LOG_CTX, "caching {0}", TableName);
                String selectQuery = String.Format("SELECT * FROM {0}", TableName);
                SqlCommand selectQueryCommand = new SqlCommand(selectQuery, sqlConnection);
                SqlDataReader reader = selectQueryCommand.ExecuteReader();
                LogSQL(log, selectQueryCommand);

                while (reader.Read())
                {
                    int column_ID = reader.GetInt32(0);
                    String column_Name = reader.GetString(1);
                    //log.DebugCtx(LOG_CTX, "SQL results: {0} {1}", column_ID, column_Name);

                    if (!LookUpTable.ContainsKey(column_Name))
                    {
                        //log.DebugCtx(LOG_CTX, "LookupTable add: {0} {1}", column_Name, column_ID);
                        LookUpTable.Add(column_Name, column_ID);
                    }
                }
                reader.Close();

                //Dump(log);
            }
            else
            {
                //log.MessageCtx(LOG_CTX, "{0} already cached: {1} elements", TableName, LookUpTable.Count);
            }
        }

        /// <summary>
        /// Inserts 1 row into TableName.  Assumes insertQueryCommand is mostly complete.  Best used
        /// to insert multiple rows into a table as part of a loop.  Works on floats.
        /// </summary>
        /// <param name="">insertQueryCommand</param>
        /// <param name="">log</param>
        /// <param name="">name</param>
        /// <param name="">metric</param>
        /// <returns></returns>
        /// 
        public void InsertRow(SqlCommand insertQueryCommand, IUniversalLog log, 
                              String name, DBFloatData metric)
        {
            String columnName = String.Format("{0}{1}", name, metric.Name);
            insertQueryCommand.Parameters["@" + LookUpColumn].Value = LookUpValue(columnName, log);
            insertQueryCommand.Parameters["@" + metric.DBColumn].Value = metric.Value;

            LogSQL(log, insertQueryCommand);
            insertQueryCommand.ExecuteNonQuery();
        }

        /// <summary>
        /// Inserts 1 row into TableName.  Assumes insertQueryCommand is mostly complete.  Best used
        /// to insert multiple rows into a table as part of a loop.  Works on bigints.
        /// </summary>
        /// <param name="">insertQueryCommand</param>
        /// <param name="">log</param>
        /// <param name="">name</param>
        /// <param name="">metric</param>
        /// <returns></returns>
        /// 
        public void InsertRow(SqlCommand insertQueryCommand, IUniversalLog log, 
                              String name, DBBigIntData metric)
        {
            String columnName = String.Format("{0}{1}", name, metric.Name);
            insertQueryCommand.Parameters["@" + LookUpColumn].Value = LookUpValue(columnName, log);
            insertQueryCommand.Parameters["@" + metric.DBColumn].Value = (Int64)metric.Value;

            LogSQL(log, insertQueryCommand);
            insertQueryCommand.ExecuteNonQuery();
        }
    }

    /// <summary>
    /// Derive one of these per unique lookup table to take advantage of the static Dictionary
    /// </summary>
    /// 
    class SmokeTestMemoryHeapDBLookUpTable : SmokeTestDBLookUpTable
    {
        public SmokeTestMemoryHeapDBLookUpTable(String tableName, String lookUpColumn)
            : base(tableName, lookUpColumn)
        {

        }
    }

    class SmokeTestStreamingModuleDBLookUpTable : SmokeTestDBLookUpTable
    {
        public SmokeTestStreamingModuleDBLookUpTable(String tableName, String lookUpColumn)
            : base(tableName, lookUpColumn)
        {

        }
    }

    class SmokeTestMemoryBucketDBLookUpTable : SmokeTestDBLookUpTable
    {
        public SmokeTestMemoryBucketDBLookUpTable(String tableName, String lookUpColumn)
            : base(tableName, lookUpColumn)
        {

        }
    }

    class SmokeTestPerformanceResultsDBLookUpTable : SmokeTestDBLookUpTable
    {
        public SmokeTestPerformanceResultsDBLookUpTable(String tableName, String lookUpColumn)
            : base(tableName, lookUpColumn)
        {

        }
    }

    class SmokeTestTimeBarDBLookUpTable : SmokeTestDBLookUpTable
    {
        public SmokeTestTimeBarDBLookUpTable(String tableName, String lookUpColumn)
            : base(tableName, lookUpColumn)
        {

        }
    }

    class SmokeTestTimeBarSetDBLookUpTable : SmokeTestDBLookUpTable
    {
         public SmokeTestTimeBarSetDBLookUpTable(String tableName, String lookUpColumn)
            : base(tableName, lookUpColumn)
        {

        }
    }

    class SmokeTestLocationDBLookUpTable : SmokeTestDBLookUpTable
    {
        public SmokeTestLocationDBLookUpTable(String tableName, String lookUpColumn)
            : base(tableName, lookUpColumn)
        {

        }
    }

    class SmokeTestPlatformDBLookUpTable : SmokeTestDBLookUpTable
    {
        public SmokeTestPlatformDBLookUpTable(String tableName, String lookUpColumn)
            : base(tableName, lookUpColumn)
        {

        }
    }

    class SmokeTestPlatformPCDBLookUpTable : SmokeTestDBLookUpTable
    {
        public SmokeTestPlatformPCDBLookUpTable(String tableName, String lookUpColumn)
            : base(tableName, lookUpColumn)
        {

        }
    }
}