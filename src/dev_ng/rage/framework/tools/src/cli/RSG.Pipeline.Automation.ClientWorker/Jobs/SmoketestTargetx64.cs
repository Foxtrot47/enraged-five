﻿using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.Reflection;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;


namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{
    class SmoketestTargetx64Parameters : SmoketestTargetConsoleParameters
    {
        private String m_RootDir;
        public String RootDir
        {
            get { return m_RootDir; }
            set { m_RootDir = value; }
        }
    }

    class SmoketestTargetx64 : 
        SmoketestTargetConsole
    {
        protected static readonly String LOG_CTX64 = "Smoke Test Target (x64)";

        public SystemInfo MachineSpecs;

        public SmoketestTargetx64(IUniversalLog log, String ipAddress, CommandOptions options)
            : base(log, ipAddress, options)
        {
            MachineSpecs = new SystemInfo();
            //ProcessorInfo procInfo = MachineSpecs.Processor;
            //MemoryInfo memInfo = MachineSpecs.Memory;
            //GraphicsInfo graphicsInfo = MachineSpecs.Graphics;

            //log.MessageCtx(LOG_CTX64, "Target {0} proc:{1} cores:{2} mem:{3} GPU:{4} VRAM:{5} Driver:{6}", 
            //    this.IpAddress, procInfo.Name, procInfo.NumberOfCores,
            //    memInfo.Capacity, graphicsInfo.Name, graphicsInfo.Memory, graphicsInfo.Driver);
        }

        public override bool IsRunning(String executable)
        {
            foreach (System.Diagnostics.Process process in System.Diagnostics.Process.GetProcessesByName(executable))
            {
                if (process.HasExited)
                    return false;
                else return true;
            }
            return false;
        }

        protected override bool DoLaunch(String notused)
        {            
            SmoketestTargetx64Parameters param = this.Parameters as SmoketestTargetx64Parameters;
            Debug.Assert(param != null);

            String logfilePath = param.LogFile;
            logfilePath = logfilePath.Replace("[TargetIP]", this.IpAddress != null ? this.IpAddress : null);
            logfilePath = logfilePath.Replace("[RunIndex]", RunIndex.ToString());

            String args = param.CommandLineArguments;
            args = args.Replace("[TargetIP]", this.IpAddress != null ? this.IpAddress : null);
            args = args.Replace("[RunIndex]", RunIndex.ToString());

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "cmd.exe";
            System.Diagnostics.Process launchGameProcess = new System.Diagnostics.Process();

            launchGameProcess.StartInfo = startInfo;
            launchGameProcess.StartInfo.UseShellExecute = false;
            launchGameProcess.StartInfo.RedirectStandardOutput = true;
            launchGameProcess.StartInfo.RedirectStandardError = true;
            launchGameProcess.StartInfo.RedirectStandardInput = true;
            launchGameProcess.StartInfo.CreateNoWindow = false;

            launchGameProcess.Start();

            launchGameProcess.OutputDataReceived += OutputDataReceived;
            launchGameProcess.ErrorDataReceived += ErrorDataReceived;

            launchGameProcess.BeginOutputReadLine();
            launchGameProcess.BeginErrorReadLine();

            String changeDrive = @"x:";

            this.Log.MessageCtx(LOG_CTX64, "Changing PC drive...{0}", changeDrive);
            launchGameProcess.StandardInput.WriteLine(changeDrive);

            String changeDirectory = String.Join(" ", "cd", @"/d", param.RootDir);
            this.Log.MessageCtx(LOG_CTX64, "Changing PC directory...{0}", changeDirectory);
            launchGameProcess.StandardInput.WriteLine(changeDirectory);

            String setenvPath = String.Format("{0}{1}",
                                    Environment.ExpandEnvironmentVariables("call \"%RS_TOOLSROOT%\"\\bin\\"),
                                    "setenv.bat");
            this.Log.MessageCtx(LOG_CTX64, "Setting PC env vars...{0}", setenvPath);
            launchGameProcess.StandardInput.WriteLine(setenvPath);

            String lineToExecute = String.Format("{0} {1}/{2} {3}{4}/ -logfile={5} {6}",
                                                 "start",
                                                 param.RootDir,
                                                 System.IO.Path.GetFileName(param.ExePath),
                                                 @"-rootdir=",
                                                 param.RootDir, 
                                                 logfilePath,
                                                 args);
            this.Log.MessageCtx(LOG_CTX64, "Launching win64...{0}", lineToExecute);
            launchGameProcess.StandardInput.WriteLine(lineToExecute);

            this.LogFilePath = logfilePath;

            this.Log.MessageCtx(LOG_CTX64, "Log file will be at [[{0}]]", this.LogFilePath);

            Debug.Assert(this.IsMonitorNull());
            this.Monitor = new LogMonitor(this.LogFilePath);
            
            TimeSpan waitTime = new TimeSpan(0, 0, 10);
            
            // Short wait as the observers will handle after
            DateTime deployStartTime = DateTime.Now;
            while (waitTime > (DateTime.Now - deployStartTime))
            {
                System.Threading.Thread.Sleep(1000);
            }

            return true;
        }

        public override void Shutdown(String exePath)
        {
            String killArg = String.Format(@"/F /IM {0}", exePath);
            this.Log.MessageCtx(LOG_CTX, "Killing x64 {0} {1}", "taskkill.exe", killArg);
            System.Diagnostics.Process.Start("taskkill.exe", killArg);
        }
    }
}
