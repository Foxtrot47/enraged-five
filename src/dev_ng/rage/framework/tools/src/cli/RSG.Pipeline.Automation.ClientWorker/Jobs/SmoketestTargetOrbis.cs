﻿using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{
    class SmoketestTargetOrbisParameters : SmoketestTargetConsoleParameters
    {
        private String m_RootDir;
        public String RootDir
        {
            get { return m_RootDir; }
            set { m_RootDir = value; }
        }
    }

    class SmoketestTargetOrbis : 
        SmoketestTargetConsole
    {
        protected static readonly String LOG_CTXO = "Smoke Test Target (Orbis)";

        public SmoketestTargetOrbis(IUniversalLog log, String ipAddress, CommandOptions options)
            : base(log, ipAddress, options)
        {

        }

        protected override bool DoLaunch(String notused)
        {
            SmoketestTargetOrbisParameters param = this.Parameters as SmoketestTargetOrbisParameters;
            Debug.Assert(param != null);

            String logfilePath = param.LogFile;
            logfilePath = logfilePath.Replace("[TargetIP]", this.IpAddress != null ? this.IpAddress : null);
            logfilePath = logfilePath.Replace("[RunIndex]", RunIndex.ToString());

            String args = param.CommandLineArguments;
            args = args.Replace("[TargetIP]", this.IpAddress != null ? this.IpAddress : null);
            args = args.Replace("[RunIndex]", RunIndex.ToString());

            String runTool = Environment.ExpandEnvironmentVariables("\"%SCE_ROOT_DIR%\\ORBIS\\Tools\\Target Manager Server\\bin\\orbis-run.exe\"");

            //"%runtool%" /fsroot %cd% /c:all /elf "%cd%\game_orbis_%exetype%.elf" %cmdline% 
            String runCommandStr = String.Format(@"/t:{0} /fsroot {1} /c:all /elf {2} {3} -logfile={4}",
                                            this.IpAddress,
                                            param.RootDir,
                                            param.ExePath,
                                            args,
                                            logfilePath);

            String runCommandArgs = Environment.ExpandEnvironmentVariables(runCommandStr);

            this.Log.MessageCtx(LOG_CTXO, "Launching orbis-run.exe {0}", runCommandArgs);

            this.LogFilePath = logfilePath;

            this.Log.MessageCtx(LOG_CTXO, "Log file will be at [[{0}]]", this.LogFilePath);

            Debug.Assert(this.IsMonitorNull());
            this.Monitor = new LogMonitor(this.LogFilePath);

            TimeSpan waitTime = new TimeSpan(0, 1, 0);
            return RunExternalTool(runTool, runCommandArgs, waitTime);
        }

        public override void Shutdown(String param)
        {
            const String tool = "orbis-run";
            foreach (System.Diagnostics.Process process in System.Diagnostics.Process.GetProcessesByName(tool))
            {
                process.Kill();
            }
            this.Log.MessageCtx(LOG_CTX, "ShutdownTheGame: Process.Kill() {0}", tool);
        }
    }
}
