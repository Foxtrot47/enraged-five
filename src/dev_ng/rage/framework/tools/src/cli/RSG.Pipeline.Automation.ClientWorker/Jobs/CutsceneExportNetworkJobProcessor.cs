﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Export;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Common.Services.Messages;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Services;
using RSG.SourceControl.Perforce;
using Microsoft.Win32;
using Ionic.Zip;
using System.Xml;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{

    /// <summary>
    /// Map Export Network Job Processing class.
    /// </summary>
    internal class CutsceneExportNetworkJobProcessor : 
        JobProcessorBase,
        IJobProcessor
    {
        #region Properties
        /// <summary>
        /// Branch object.
        /// </summary>
        IBranch Branch { get; set; }
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="fileTransferConsumer"></param>
        /// 
        public CutsceneExportNetworkJobProcessor(CommandOptions options, FileTransferServiceConsumer fileTransferConsumer)
            : base(options, CapabilityType.CutsceneExportNetwork, fileTransferConsumer)
        {
        }
        #endregion // Constructor(s)

        #region Constants
        protected static readonly String LOG_CTX = "CutsceneExport Job Processor";

        #endregion

        #region Controller Methods
        /// <summary>
        /// Process requested job.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public override IJobResult Process(IJob job)
        {
            Debug.Assert(job.Role == CapabilityType.CutsceneExportNetwork, String.Format("Invalid job for processor: {0}.",
                job.GetType().Name));
            if (job.Role != CapabilityType.CutsceneExportNetwork)
            {
                return null;
            }
            Init(job);

            CleanCacheAndLogFiles();

            IUniversalLog log = LogFactory.CreateUniversalLog("process");
            ILogTarget logFile = LogFactory.CreateUniversalLogFile("process",
                log, "automation", job.ID.ToString()) as ILogTarget;

            IEnumerable<String> jobAvailableFilenames = FileTransferServiceConsumer.FileAvailability(job);

            bool exportCutFile = false;
            bool importCutFile = false;
            bool runConcat = false;
            bool useSourceControlFiles = false;

            string[] lstFilesFromServer = GetFilenamesFromServer(jobAvailableFilenames);
            string[] lstFilesFromJob = GetFilenamesFromJob(job, out exportCutFile, out importCutFile, out runConcat, out useSourceControlFiles);

            SyncDlcContents();

            IConfig config = ConfigFactory.CreateConfig();
            
            this.Branch = IsDlc(config, GetFBXFileFromJob(lstFilesFromJob));
            if(this.Branch == null)
                this.Branch = this.Options.Branch;

            log.Message("Project: {0}", this.Branch.Project.Name);

            SyncDependancies();

            string configFile = Path.Combine(Environment.GetEnvironmentVariable("RS_TOOLSROOT"), "etc", "processors", "RSG.Pipeline.Processor.Animation.Cutscene.PreProcess.local.xml");

            GenerateLocalCutscenePreProcessConfig(configFile, runConcat);

            JobResult jobResult = new JobResult(job.ID, false);

            string strMotionBuilderExe = String.Empty;
            if (!VerifyAutodeskMotionbuilderInstall(log, out strMotionBuilderExe))
            {
                log.Error("Job failed: VerifyAutodeskMotionbuilderInstall check failed.");
                MergeLogsScanErrorAndUpload(log, logFile, job);
                return jobResult;
            }

            ForceGetCutsceneData(job);

            if (!DownloadAndCopy(log, job, lstFilesFromServer, lstFilesFromJob, useSourceControlFiles))
            {
                log.ErrorCtx(LOG_CTX, "Source file(s) download to worker failed.");
                MergeLogsScanErrorAndUpload(log, logFile, job);
                return jobResult;
            }

            if (!MakeCutsceneDataWritable(log, job))
            {
                log.ErrorCtx(LOG_CTX, "Unable to prepare cutscene data for export.");
                MergeLogsScanErrorAndUpload(log, logFile, job);
                return jobResult;
            }

            string strPreviewDir = Branch.Preview;
            TryDeleteDirectory(strPreviewDir, log);
            Directory.CreateDirectory(strPreviewDir);

            string strPythonFilename = GeneratePythonFile(log, GetFBXFileFromJob(lstFilesFromJob), GetCutPartFileFromJob(lstFilesFromJob), exportCutFile, importCutFile);
            if ( strPythonFilename == null )
            {
                log.ErrorCtx(LOG_CTX, "Unable to generate Python export script.");
                MergeLogsScanErrorAndUpload(log, logFile, job);
                return jobResult;
            }

            //Ignore the exit code from the MotionBuilder process.  Instead rely on a success signal in the Universal Log.
            ExportAndBuild(log, strMotionBuilderExe, strPythonFilename);

            bool result = MergeLogsScanErrorAndUpload(log, logFile, job);
            result = result && UploadExportAndBuildData(GetFBXFileFromJob(lstFilesFromJob), GetCutPartFileFromJob(lstFilesFromJob), log, job);
            
            ForceGetCutsceneData(job);
            log.Message("Final Process result returning: {0}", result);
            TryDeleteFile(strPythonFilename, log);
            DeleteClientFiles(job, lstFilesFromJob, log);

            jobResult.Succeeded = result;
            return jobResult;
        }
        #endregion // Controller Methods

        #region Private Methods

        private void SyncDlcContents()
        {
            using (P4 p4 = this.Options.Config.Project.SCMConnect())
            {
                List<string> args = new List<string>();

                args.Add(Path.Combine("x:\\gta5_dlc", "....xml"));
                
                p4.Run("sync", args.ToArray());
            }
        }

        private void SyncDependancies()
        {
            using (P4 p4 = this.Options.Config.Project.SCMConnect())
            {
                List<string> args = new List<string>();

                args.Add(Path.Combine(this.Branch.Common, "..."));
                args.Add(Path.Combine(this.Options.Config.ToolsConfig, "..."));
                args.Add(Path.Combine(this.Branch.Assets, "anim", "expressions", "..."));
                args.Add(Path.Combine(this.Branch.Art, "animation", "resources", "characters", "Expressions", "Binary", "..."));
                args.Add(Path.Combine(this.Branch.Art, "VFX", "rmptfx", "..."));
                args.Add(Path.Combine(this.Branch.Assets, "export", "anim", "cutscene", "..."));

                //args.Add(Path.Combine(this.Options.Config.ToolsRoot, String.Format("...@{0}", this.Options.Project.Labels[RSG.Base.Configuration.Label.ToolsCurrent])));

                p4.Run("sync", args.ToArray());
            }
        }

        /// <summary>
        ///   Sorts the data from the server with the data in the job. These should match and are verified by filename. 
        /// </summary>
        /// <param name="log"></param>
        /// <param name="lstFilesFromServer"></param>
        /// <param name="lstFilesFromJob"></param>
        ///
        private bool SortAndVerifyData(IUniversalLog log, string[] lstFilesFromServer, string[] lstFilesFromJob)
        {
            if (lstFilesFromServer.Length != lstFilesFromJob.Length)
            {
                log.ErrorCtx(LOG_CTX, "Mismatched number of files between server and job.  Server expects {0}.  Job expects {1}.", lstFilesFromServer.Length, lstFilesFromJob.Length);
                return false;
            }

            // Sort these so they should be in the same order
            Array.Sort(lstFilesFromJob, delegate(string user1, string user2)
            {
                return Path.GetFileName(user1).CompareTo(Path.GetFileName(user2)); 
            });

            Array.Sort<string>(lstFilesFromServer);

            Console.WriteLine("*************DEBUG*************");
            Console.WriteLine("Server has {0} entries", lstFilesFromServer.Length);
            Console.WriteLine("Job has {0} entries", lstFilesFromJob.Length);
            Console.WriteLine("SERVER:");
            for (int i = 0; i < lstFilesFromServer.Length; ++i)
            {
                Console.WriteLine(lstFilesFromServer[i].ToString());
            }
            Console.WriteLine("JOB:");
            for (int i = 0; i < lstFilesFromJob.Length; ++i)
            {
                Console.WriteLine(lstFilesFromJob[i].ToString());
            }
            Console.WriteLine("*************DEBUG*************");

            // Verify that they are correct 
            for (int i = 0; i < lstFilesFromJob.Length; ++i)
            {
                if (Path.GetFileName(lstFilesFromJob[i]) != Path.GetFileName(lstFilesFromServer[i]))
                {
                    log.ErrorCtx(LOG_CTX, "File(s) {0} | {1} do not match.", lstFilesFromJob[i], lstFilesFromServer[i]);
                    return false;
                }
            }

            return true;
        }

        IBranch GetBranch(IConfig config, IProject project)
        {
            if (project.Branches.ContainsKey(config.CoreProject.DefaultBranchName))
            {
                return project.Branches[config.CoreProject.DefaultBranchName];
            }

            return project.Branches[project.DefaultBranchName];
        }

        private IBranch IsDlc(IConfig config, String fbx)
        {
            fbx = fbx.Replace("/", "\\");

            foreach (KeyValuePair<String, IProject> pair in config.DLCProjects)
            {
                string artDir = GetBranch(config, pair.Value).Art;
                artDir = artDir.Replace("/", "\\");

                if (fbx.StartsWith(artDir, StringComparison.OrdinalIgnoreCase))
                {
                    return GetBranch(config, pair.Value);
                }
            }

            return null;
        }

        /// <summary>
        ///   Nuke the cache folders and their contents. This means we dont scoop up any old shit
        /// </summary>
        ///
        private void CleanCacheAndLogFiles()
        {
            try
            {
#warning DHM FIX ME: these paths are old AP2 conversion paths.  This is now defined in data...
                string strResourcedCacheDir = Environment.GetEnvironmentVariable("RS_PROJROOT") + @"\cache\convert\dev\resourcing\anim\cuts";
                string strRawCacheDir = Environment.GetEnvironmentVariable("RS_PROJROOT") + @"\cache\raw\cuts";
                string strLogDir = Environment.GetEnvironmentVariable("RS_TOOLSROOT") + @"\logs";
                List<String> directoriesToDelete = new List<String>(){strResourcedCacheDir, strRawCacheDir,strLogDir};
                TryDeleteDirectories(directoriesToDelete);

                Directory.CreateDirectory(Environment.GetEnvironmentVariable("RS_TOOLSROOT") + @"\logs\cutscene");
            }
            catch (Exception /*e*/)
            {
            }
        }

        /// <summary>
        ///   Revert the data back to what it was before we exported/built
        /// </summary>
        /// <param name="job"></param>
        ///
        private void ForceGetCutsceneData(IJob job)
        {
            using (P4 p4 = this.Options.Config.Project.SCMConnect())
            {
                string strCutsceneDir = GetCutsceneDir(job);

                List<String> args = new List<String>();
                args.Add("-f");
                args.Add(strCutsceneDir + "\\...");
                p4.Run("sync", args.ToArray());
            }
        }

        public static void TimeItOut(object o)
        {
            Debug.Assert(o is IUniversalLog, String.Format("State object expected as universal log type."));

            IUniversalLog log = (IUniversalLog)o;

            Process[] proc = System.Diagnostics.Process.GetProcessesByName("motionbuilder");
            if (proc.Length > 0)
            {
                if (log != null)
                {
                    log.ErrorCtx(LOG_CTX, "Autodesk Motionbuilder has being killed due to timeout.  Aborting job.");
                }

                proc[0].Kill();
            }

            proc = System.Diagnostics.Process.GetProcessesByName("senddmp");
            if (proc.Length > 0)
                proc[0].Kill();
        }

        /// <summary>
        ///   Execute an export/build from motionbuilder using the python script
        /// </summary>
        /// <param name="log"></param>
        /// <param name="strMotionBuilderExe"></param>
        /// <param name="strPythonFilename"></param>
        ///
        private bool ExportAndBuild(IUniversalLog log, string strMotionBuilderExe, string strPythonFilename)
        {
            ProcessStartInfo info = new ProcessStartInfo();
            info.UseShellExecute = false;
            info.FileName = strMotionBuilderExe;
            info.Arguments = strPythonFilename + " -suspendMessages";

            //1800000 == 30mins
            //2700000 == 45mins
            // Run a timer, this will kill motionbuilder if it has crashed, currently set to 45minutes
            System.Threading.Timer tmr = new System.Threading.Timer(new TimerCallback(TimeItOut), log, 0, 2700000);

            Thread.Sleep(5000); // 5 seconds

            Process process = new Process();
            process.StartInfo = info;
            if ((null == process) || (!process.Start()))
            {
                log.ErrorCtx(LOG_CTX, "Failed to invoke Autodesk Motionbuilder for cutscene export.  Aborting job.");
                log.ErrorCtx(LOG_CTX, "Command: '{0}'.", process.StartInfo.FileName);
                log.ErrorCtx(LOG_CTX, "Arguments: '{0}'", String.Join(" ", process.StartInfo.Arguments));
                return (false);
            }

            log.MessageCtx(LOG_CTX, "Autodesk Motionbuilder launched;");
            log.MessageCtx(LOG_CTX, "Command: {0}.", process.StartInfo.FileName);
            log.MessageCtx(LOG_CTX, "Arguments: {0}.", String.Join(" ", process.StartInfo.Arguments));
            process.WaitForExit();

            tmr.Dispose();

            log.MessageCtx(LOG_CTX, "Process Exit Code: {0}.", process.ExitCode);

            if (0 != process.ExitCode) // Since we are nuking MB if it crashes this means we cant return an error on the exit code, just warn
            {
                log.WarningCtx(LOG_CTX, "Autodesk Motionbuilder crashed on exit.");
                log.WarningCtx(LOG_CTX, "Command: '{0}'.", process.StartInfo.FileName);
                log.WarningCtx(LOG_CTX, "Arguments: '{0}'", String.Join(" ", process.StartInfo.Arguments));
                return (false);
            }

            return (true);
        }

        /// <summary>
        ///   Generate the local config file for ap3 build system
        /// </summary>
        /// <param name="log"></param>
        /// <param name="exeFilename"></param>
        ///
        private static void GenerateLocalCutscenePreProcessConfig(string strConfig, bool concat)
        {
            if (File.Exists(strConfig))
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(strConfig);

                bool bFoundEntry = false;
                XmlNodeList nodes = doc.SelectNodes("/Parameters/Parameter");
                foreach (XmlNode node in nodes)
                {
                    if (node.Attributes["name"].Value.ToLower() == "automaticallybuildconcat")
                    {
                        node.Attributes["value"].Value = (concat == true) ? "true" : "false";
                        bFoundEntry = true;
                    }
                }

                if (!bFoundEntry)
                {
                    XmlNode node = doc.CreateNode(XmlNodeType.Element, "Parameter", null);
                    XmlAttribute attrName = doc.CreateAttribute("name");
                    attrName.Value = "AutomaticallyBuildConcat";
                    XmlAttribute attrType = doc.CreateAttribute("type");
                    attrType.Value = "bool";
                    XmlAttribute attrValue = doc.CreateAttribute("value");
                    attrValue.Value = (concat == true) ? "true" : "false";
                    node.Attributes.Append(attrName);
                    node.Attributes.Append(attrType);
                    node.Attributes.Append(attrValue);
                    doc.DocumentElement.AppendChild(node);
                }

                doc.Save(strConfig);
            }
            else
            {
                using (XmlWriter writer = XmlWriter.Create(strConfig))
                {
                    writer.WriteStartDocument();
                    writer.WriteStartElement("Parameters");

                    writer.WriteStartElement("Parameter");
                    writer.WriteAttributeString("name", "AutomaticallyBuildConcat");
                    writer.WriteAttributeString("type", "bool");
                    writer.WriteAttributeString("value", "true");
                    writer.WriteEndElement();

                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                }
            }
        }

        /// <summary>
        ///   Verify if motionbuilder is installed. This does a check in the registry for motionbuilder 2012 x64
        /// </summary>
        /// <param name="log"></param>
        /// <param name="exeFilename"></param>
        ///
        private bool VerifyAutodeskMotionbuilderInstall(IUniversalLog log, out String exeFilename)
        {
            RegistryKey hklm64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
            RegistryKey key = hklm64.OpenSubKey(@"SOFTWARE\Autodesk\MotionBuilder\2014");
            if(key != null)
            {
                string InstallPath = (string)key.GetValue("InstallPath", null);
                exeFilename = InstallPath + "bin\\x64\\motionbuilder.exe";

                if(File.Exists(exeFilename))
                    return (true);
            }

            log.ErrorCtx(LOG_CTX, "Motionbuilder executable not found.  Aborting job.");
            exeFilename = String.Empty;
            return (false);
        }

        /// <summary>
        ///   Delete the source files on the client, these are not needed anymore once processed and just take up space.
        /// </summary>
        /// <param name="job"></param>
        /// <param name="lstFilesFromServer"></param>
        /// <returns></returns>
        ///
        private void DeleteClientFiles(IJob job, string[] lstFilesFromJob, IUniversalLog log)
        {
            String downloadCacheDir = FileTransferServiceConsumer.GetClientFileDirectory(job);

            if (Directory.Exists(downloadCacheDir))
            {
                string[] files = Directory.GetFiles(downloadCacheDir);
                TryDeleteFiles(files, log);
            }

            // Deletes the fbx and cut
            TryDeleteFiles(lstFilesFromJob, log);
        }

        /// <summary>
        ///   Download the files from the server and copy them to the correct location on the client.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="job"></param>
        /// <param name="lstFilesFromServer"></param>
        /// <param name="lstFilesFromJob"></param>
        /// <returns></returns>
        ///
        private bool DownloadAndCopy(IUniversalLog log, IJob job, string[] lstFilesFromServer, string[] lstFilesFromJob, bool useSourceControlFiles)
        {
            bool result = true;
            if (useSourceControlFiles)
            {
                using (P4 p4 = this.Options.Config.Project.SCMConnect())
                {
                    string fbxFile = lstFilesFromJob[0];
                    //NOTE:  Don't need to worry about the files in assets\cuts; that's handled in ForceGetCutsceneData(job);

                    List<string> args = new List<string>();
                    args.Add("-f");
                    FileMapping[] mappings = FileMapping.Create(p4, fbxFile);
                    foreach (FileMapping mapping in mappings)
                    {
                        log.MessageCtx(LOG_CTX, "Synchronizing files {0}.", mapping.DepotFilename);
                        args.Add(mapping.DepotFilename);
                    }

                    p4.Run("sync", args.ToArray());

                    //Log which revision to be exported.
                    P4API.P4RecordSet recordSet = p4.Run("fstat", args.ToArray());
                    if ( recordSet != null )
                    {
                        foreach (P4API.P4Record record in recordSet.Records)
                        {
                            log.MessageCtx(LOG_CTX, String.Format("{0}#{1}", record["depotFile"], record["haveRev"]));
                        }
                    }
                }
            }
            else
            {
                if (!SortAndVerifyData(log, lstFilesFromServer, lstFilesFromJob))
                {
                    log.ErrorCtx(LOG_CTX, "Server / Job file(s) do not match.");
                    return false;
                }

                for (int iFile = 0; iFile < lstFilesFromServer.Length; iFile++)
                {
                    Byte[] md5 = RSG.Base.IO.FileMD5.Empty;
                    result &= this.FileTransferServiceConsumer.DownloadFile(job, 
                        lstFilesFromServer[iFile], Path.GetFileName(lstFilesFromServer[iFile]),
                        out md5);
                    if (!result)
                    {
                        log.ErrorCtx(LOG_CTX, "File {0} download failed.  MD5: {1}.",
                            RSG.Base.IO.FileMD5.Format(md5));
                        return (result);
                    }

                    String downloadCacheDir = FileTransferServiceConsumer.GetClientFileDirectory(job);
                    String source = Path.Combine(downloadCacheDir, lstFilesFromServer[iFile]);
                    if (!File.Exists(source))
                    {
                        log.ErrorCtx(LOG_CTX, "Source file {0} not available on server.", source);
                        result &= false;
                        return result;
                    }

                    log.MessageCtx(LOG_CTX, "Copying source file {0} to {1}.", source, lstFilesFromJob[iFile]);

                    if (File.Exists(lstFilesFromJob[iFile]))
                    {
                        FileAttributes attributes = File.GetAttributes(lstFilesFromJob[iFile]);
                        File.SetAttributes(lstFilesFromJob[iFile], attributes & ~FileAttributes.ReadOnly);
                    }

                    if (!Directory.Exists(Path.GetDirectoryName(lstFilesFromJob[iFile])))
                        Directory.CreateDirectory(Path.GetDirectoryName(lstFilesFromJob[iFile]));

                    File.Copy(source, lstFilesFromJob[iFile], true);

                    // Delete file from the job directory to save some space on the client.
                    log.MessageCtx(LOG_CTX, "Deleting source file {0}.", source);
                    TryDeleteFile(source);
                }
            }

            return result;
        }

        /// <summary>
        ///   Generate a python script to process the scene, this will be sent to motionbuilder
        /// </summary>
        /// <param name="strFBX"></param>
        /// <param name="strCutPart"></param>
        /// <param name="bExportCutfile"></param>
        /// <returns></returns>
        ///
        private string GeneratePythonFile(IUniversalLog log, string strFBX, string strCutPart, bool bExportCutfile, bool bImportCutfile)
        {
            string strTempFilename = Path.GetTempFileName();
            File.Delete(strTempFilename); // Delete the temp file created by GetTempFileName, we just want the filename
            strTempFilename = Path.ChangeExtension(strTempFilename, "py"); // Motionbuilder only accepts py extension

            try
            {
                using (TextWriter tw = new StreamWriter(strTempFilename))
                {
                    tw.WriteLine("from ctypes import *");
                    tw.WriteLine("from pyfbsdk import *");
                    tw.WriteLine("from pyfbsdk_additions import *");
                    tw.WriteLine("import os");
                    tw.WriteLine("");
                    tw.WriteLine("def dummyCallback( source, event ):");
                    tw.WriteLine("\tpass");
                    tw.WriteLine("");
                    tw.WriteLine("FBSystem().OnConnectionNotify.Add( dummyCallback )");
                    tw.WriteLine("FBApplication().OnFileExit.Remove( dummyCallback )");
                    tw.WriteLine("");
                    tw.WriteLine("try:");
                    tw.WriteLine("\tlogPath = os.getenv(\"RS_TOOLSROOT\") + \"\\logs\\cutscene\\workbench_batch.log\"");
                    tw.WriteLine("\td = os.path.dirname(logPath)");
                    tw.WriteLine("\tif not os.path.exists(d):");
                    tw.WriteLine("\t\tos.makedirs(d)");
                    tw.WriteLine("\tLOGFILE = open(logPath,\"a\")");
                    tw.WriteLine("");
                    tw.WriteLine("\tif FBApplication().FileOpen( \"" + strFBX.Replace("\\", "/") + "\" ) == True:");
                    tw.WriteLine("\t\tif cdll.rexMBRage.ExportBuildCutscene_Py( \"" + strCutPart.Replace("\\", "/") + "\"," + ((bExportCutfile) ? "True" : "False") + "," + ((bImportCutfile) ? "True" : "False") + ",False,True ) == False:");
                    tw.WriteLine("\t\t\tLOGFILE.write(\"" + strFBX.Replace("\\", "/") + " - " + strCutPart.Replace("\\", "/") + " failed\\n\")");
                    tw.WriteLine("\t\telse:");
                    tw.WriteLine("\t\t\tLOGFILE.write(\"" + strFBX.Replace("\\", "/") + " - " + strCutPart.Replace("\\", "/") + " success\\n\")");
                    tw.WriteLine("\telse:");
                    tw.WriteLine("\t\tLOGFILE.write(\"" + strFBX.Replace("\\", "/") + " - Unable to open file\\n\")");
                    tw.WriteLine("");
                    tw.WriteLine("\tLOGFILE.close()");
                    tw.WriteLine("except IOError:");
                    tw.WriteLine("  print \"An error has occurred within the Python script.\"");
                    tw.WriteLine("FBApplication().FileExit()");

                    tw.Close();
                }
            }
            catch (Exception e)
            {
                log.Error("Job failed: Motionbuilder python script error - {0}", e.Message);
                return null;
            }

            return strTempFilename;
        }

        /// <summary>
        ///   Get the filenames from the server that have being uploaded
        /// </summary>
        /// <param name="lstFiles"></param>
        /// <returns></returns>
        ///
        private string[] GetFilenamesFromServer(IEnumerable<String> lstFiles)
        {
            ArrayList tmp = new ArrayList();

            var enumerator = lstFiles.GetEnumerator();
            while (enumerator.MoveNext())
            {
                if (enumerator.Current.ToString().Contains("job.ulog")) continue;
                tmp.Add(enumerator.Current);
            }

            return (string[])tmp.ToArray(typeof(string));
        }

        /// <summary>
        ///   Parse the job user data into an array, comma delimited string
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        ///
        private string[] GetFilenamesFromJob(IJob job, out bool exportCutFile, out bool importCutFile, out bool runConcat, out bool useSourceControlFiles)
        {
            exportCutFile = false;
            importCutFile = false;
            runConcat = false;
            useSourceControlFiles = false;

            string userData = (string)job.UserData;

            string exportCutFileMarker = ":exportCutFile:";
            int tokenIndex = userData.IndexOf(exportCutFileMarker) + exportCutFileMarker.Length;
            string data = userData.Substring(tokenIndex, userData.IndexOf(":", tokenIndex) - tokenIndex);
            if ( data.StartsWith("true") == true )
                exportCutFile = true;

            string importCutFileMarker = ":importCutFile:";
            tokenIndex = userData.IndexOf(importCutFileMarker) + importCutFileMarker.Length;
            data = userData.Substring(tokenIndex, userData.IndexOf(":", tokenIndex) - tokenIndex);
            if ( data.StartsWith("true") == true )
                importCutFile = true;

            string useSourceControlFilesMarker = ":useSourceControlFiles:";
            tokenIndex = userData.IndexOf(useSourceControlFilesMarker) + useSourceControlFilesMarker.Length;
            data = userData.Substring(tokenIndex, userData.IndexOf(":", tokenIndex) - tokenIndex);
            if ( data.StartsWith("true") == true )
                useSourceControlFiles = true;
            
            string concatMarker = ":concat:";
            tokenIndex = userData.IndexOf(concatMarker) + concatMarker.Length;
            data = userData.Substring(tokenIndex, userData.IndexOf(":", tokenIndex) - tokenIndex);
            if (data.StartsWith("true") == true)
                runConcat = true;

            string filesMarker = ":files:";
            tokenIndex = userData.IndexOf(filesMarker) + filesMarker.Length;
            data = userData.Substring(tokenIndex, userData.Length - tokenIndex);
            return data.Split(',');
        }

        /// <summary>
        ///   Recursively set every file within the tree of folders to writable
        /// </summary>
        /// <param name="directory"></param>
        /// <returns></returns>
        ///
        void MakeDirectoryAndContentsWritable(string directory)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(directory);
            foreach (FileSystemInfo fileSystemInfo in dirInfo.GetFileSystemInfos())
            {
                if (fileSystemInfo.Attributes.HasFlag(FileAttributes.ReadOnly))
                    fileSystemInfo.Attributes = fileSystemInfo.Attributes & (~FileAttributes.ReadOnly);
            }

            foreach (DirectoryInfo subDirectoryInfo in dirInfo.GetDirectories())
                MakeDirectoryAndContentsWritable(subDirectoryInfo.FullName);
        }

        /// <summary>
        ///   Get the cutscene directory from the job. This uses the scenes data.cutpart within the userdata to work out what the scene path is
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        ///
        private string GetCutsceneDir(IJob job)
        {
            string strFiles = (string)job.UserData;
            string[] files = strFiles.Split(',');

            for(int i=0; i < files.Length; ++i)
            {
                if(files[i].Contains("data.cutpart"))
                {
                    return Path.GetDirectoryName(files[i]);
                }
            }

            return String.Empty;
        }

        /// <summary>
        ///   Get the cutscene scene name, we cannot assume this will be the fbx name so we pull it from the cutpart file
        /// </summary>
        /// <param name="strPart"></param>
        /// <returns></returns>
        ///
        private string GetCutsceneSceneName(string strPart)
        {
            if (File.Exists(strPart))
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(strPart);

                XmlNode pathNode = xmlDoc.SelectSingleNode("/RexRageCutscenePartFile/path");
                if (pathNode != null)
                {
                    return Path.GetFileName(pathNode.InnerText);
                }
            }

            return String.Empty;
        }

        /// <summary>
        ///   Get the fbx file from the job list, since we cannot guarentee a specific index
        /// </summary>
        /// <param name="lstFilesFromJob"></param>
        /// <returns></returns>
        ///
        private string GetFBXFileFromJob(string[] lstFilesFromJob)
        {
            foreach (string file in lstFilesFromJob)
            {
                if (file.ToLower().EndsWith(".fbx"))
                {
                    return file;
                }
            }

            return String.Empty;
        }

        /// <summary>
        ///   Get the cutpart file from the job list, since we cannot guarentee a specific index
        /// </summary>
        /// <param name="lstFilesFromJob"></param>
        /// <returns></returns>
        ///
        private string GetCutPartFileFromJob(string[] lstFilesFromJob)
        {
            foreach (string file in lstFilesFromJob)
            {
                if (file.ToLower().EndsWith(".cutpart"))
                {
                    return file;
                }
            }

            return String.Empty;
        }

        /// <summary>
        ///   Make all cutscene data writable - all data within X:\gta5\assets\cuts\(scene) and X:\gta5\assets\cuts\(scene).cutbin/cutxml
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        ///
        private bool MakeCutsceneDataWritable(IUniversalLog log, IJob job)
        {
            // Get the cutscene directory - X:\gta5\assets\cuts\(scene)
            string strCutsceneDir = GetCutsceneDir(job);
            if (Directory.Exists(strCutsceneDir) == false)
            {
                log.ErrorCtx(LOG_CTX, String.Format("Missing cutscene data.  Directory {0} does not exist.  Unable to continue.", strCutsceneDir));
                return false;
            }

            MakeDirectoryAndContentsWritable(strCutsceneDir);
            return true;
        }

        /// <summary>
        ///   Merge all the cutscene logs and upload them to the server.
        ///   Logs from X:\gta5\tools\logs and X:\gta5\tools\logs\cutscene and X:\gta5\tools\logs\cutscene\process
        /// </summary>
        /// <param name="log"></param>
        /// <param name="job"></param>
        /// <returns></returns>
        ///
        protected override bool MergeLogsScanErrorAndUpload(IUniversalLog log, ILogTarget logTarget, IJob job)
        {
            String destinationDir = this.FileTransferServiceConsumer.GetClientFileDirectory(job);
            if (!Directory.Exists(destinationDir))
                Directory.CreateDirectory(destinationDir);

            Directory.CreateDirectory(Environment.GetEnvironmentVariable("RS_TOOLSROOT") + @"\logs\cutscene\process");

            string[] uLogFiles = Directory.GetFiles(Environment.GetEnvironmentVariable("RS_TOOLSROOT") + @"\logs", "*.ulog", SearchOption.AllDirectories);
            //string[] uLogFilesToMerge2 = Directory.GetFiles(Environment.GetEnvironmentVariable("RS_TOOLSROOT") + @"\logs\cutscene", "*.ulog");
            //string[] uLogFilesToMerge3 = Directory.GetFiles(Environment.GetEnvironmentVariable("RS_TOOLSROOT") + @"\logs\cutscene\process", "*.ulog");

            //string[] uLogFiles = new string[uLogFilesToMerge1.Length + uLogFilesToMerge2.Length + uLogFilesToMerge3.Length];
            //Array.Copy(uLogFilesToMerge1, uLogFiles, uLogFilesToMerge1.Length);
            //Array.Copy(uLogFilesToMerge2, 0, uLogFiles, uLogFilesToMerge1.Length, uLogFilesToMerge2.Length);
            //Array.Copy(uLogFilesToMerge3, 0, uLogFiles, uLogFilesToMerge1.Length + uLogFilesToMerge2.Length, uLogFilesToMerge3.Length);

            String destinationFilename = Path.Combine(destinationDir, "job.ulog");
            UniversalLogFile.MergeLogs(destinationFilename, uLogFiles);

            bool result = false;
            if (UniversalLogFile.ContainsErrors(destinationFilename))
            {
                log.Error("Job failed: Merged log contains errors - {0}", destinationFilename);
            }
            else
            {
                //Search for a specific message in the log to denote successful exporting.
                const string exportSuccess = "Exporting cutscene has succeeded.";
                IEnumerable<Base.Collections.Pair<String, String>> messages = UniversalLogFile.GetMessages(destinationFilename);
                foreach (Base.Collections.Pair<String, String> message in messages)
                {
                    if (message.Second.CompareTo(exportSuccess) == 0)
                    {
                        result = true;
                        break;
                    }
                }
            }

            if (!this.FileTransferServiceConsumer.UploadFile(job, destinationFilename))
            {
                result = false;
                log.Error("Job failed: Upload of merged log failed - {0}", destinationFilename);
            }

            return result;
        }

        private string[] BuildUniqueList(string[] dirList)
        {
            List<string> fileList = new List<string>();

            foreach (string dir in dirList)
            {
                if(!Directory.Exists(dir)) continue;

                string[] files = Directory.GetFiles(dir);
                
                foreach(string currentFile in files)
                {
                    bool found = false;

                    foreach (string file in fileList)
                    {
                        if(Path.GetFileName(file) == Path.GetFileName(currentFile))
                        {
                            found=true;
                        }
                    }

                    if(!found)
                    {
                        fileList.Add(currentFile);
                    }
                }
            }

            return fileList.ToArray();
        }

        /// <summary>
        /// Upload export and build data back to the automation service.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="logFile"></param>
        /// <param name="job"></param>
        /// <returns></returns>
        private bool UploadExportAndBuildData(string strFBXFile, string strPartFile, IUniversalLog log, IJob job)
        {
            bool result = true;

            String downloadCacheDir = FileTransferServiceConsumer.GetClientFileDirectory(job);
            if (Directory.Exists(downloadCacheDir) == false)
            {
                Directory.CreateDirectory(downloadCacheDir);
            }

            string strPreviewDir = Branch.Preview; 
            string strCutsceneDir = GetCutsceneDir(job);

            using (ZipFile zip = new ZipFile())
            {
                if (Directory.Exists(strCutsceneDir))
                    zip.AddDirectory(strCutsceneDir);

                zip.Save(Path.Combine(downloadCacheDir, "export.zip"));
            };

            result &= FileTransferServiceConsumer.UploadFile(job, Path.Combine(downloadCacheDir, "export.zip"), "export.zip");

            // Create temp dirs and copy the files there, zip the dir and then delete.
            // This gets around the adding files and the zip having file paths.

            using (ZipFile zip = new ZipFile())
            {
                zip.AddDirectory(strPreviewDir);
                zip.Save(Path.Combine(downloadCacheDir, "resourced.zip"));
            }

            result &= FileTransferServiceConsumer.UploadFile(job, Path.Combine(downloadCacheDir, "resourced.zip"), "resourced.zip");

            // Work out the icd zip file and upload it.

            strFBXFile = strFBXFile.Replace("/","\\");
            string[] outputPathtokens = strFBXFile.Split(new string[] {"\\"}, StringSplitOptions.RemoveEmptyEntries); 

            string strScene = Path.Combine(this.Branch.Art, "animation", "cutscene", "!!scenes");
            string[] assetPathTokens = strScene.Split(new string[] { "\\" }, StringSplitOptions.RemoveEmptyEntries);

            string strZip = Path.Combine(this.Branch.Export, "anim", "cutscene", outputPathtokens[assetPathTokens.Length], GetCutsceneSceneName(strPartFile) + ".icd.zip");

            result &= FileTransferServiceConsumer.UploadFile(job, strZip, GetCutsceneSceneName(strPartFile) + ".icd.zip");

            if (!result)
            {
                log.ErrorCtx(LOG_CTX, "Export and build data upload failed.");
            }

            return result;
        }

        #endregion // Private Methods

    }

} // RSG.Pipeline.Automation.ClientWorker.Jobs namespace
