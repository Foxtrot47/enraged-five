﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Threading;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Content;
using RSG.Pipeline.Content.Algorithm;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Platform;
using RSG.SourceControl.Perforce;
using Ionic.Zip;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{
    class AnimationPackageJobProcessor :
        IcdBasedPackageJobProcessor
    {

        #region Constants
        protected readonly new String LOG_CTX = "Animation Package Job Processor";
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="fileTransferConsumer"></param>
        /// 
        public AnimationPackageJobProcessor(CommandOptions options, FileTransferServiceConsumer fileTransferConsumer)
            : base(options, CapabilityType.AnimationPackage, fileTransferConsumer)
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Get zip files that are going to be uploaded
        /// </summary>
        /// <param name="AllZips"></param>
        /// <returns></returns>

        protected override void GetZipFiles(ref List<String> AllZips)
        {
            if (String.IsNullOrEmpty(this.ZipRootDir))
            {
                Log.ErrorCtx(LOG_CTX, "Input Zip Directory must be defined for Animation Packaging. See Processor settings xml");
            }
            foreach (String subdir in System.IO.Directory.GetDirectories(this.ZipRootDir))
            {
                foreach (String zipFile in System.IO.Directory.GetFiles(subdir))
                {
                    AllZips.Add(zipFile);
                }
            }
        }
        #endregion // Controller Methods
    }
}
