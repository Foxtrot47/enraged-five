﻿using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{
    class SmoketestTargetDurangoParameters : SmoketestTargetConsoleParameters
    {
        private String m_AumID;
	    public String AumID
	    {
		    get { return m_AumID; }
		    set { m_AumID = value; }
	    }
    }

    class SmoketestTargetDurango : 
        SmoketestTargetConsole
    {
        protected static readonly String LOG_CTXD = "Smoke Test Target (Durango)";

        public SmoketestTargetDurango(IUniversalLog log, String ipAddress, CommandOptions options) 
            : base(log, ipAddress, options)
        {

        }

        protected override void LaunchTheThread()
        {
            String gameExecutable = Environment.ExpandEnvironmentVariables(this.Parameters.ExePath);
            SmoketestTargetDurangoParameters param = this.Parameters as SmoketestTargetDurangoParameters;
            Debug.Assert(param != null);
            if (param != null)
            {
                String AUMID = param.AumID;

                if (DoDeploy(gameExecutable))
                {
                    DoLaunch(AUMID);
                }

                this.LaunchThread = null;
                this.HasAttemptedLaunch = true;
            }
        }

        public override void Launch(SmoketestTargetConsoleParameters param)
        {
            base.Launch(param);
        }

        protected override bool DoDeploy(String gameExecutable)
        {
            FileInfo exeFileInfo = new FileInfo(gameExecutable);
            TimeSpan waitTime = new TimeSpan(0, 1, 0);

            //Making sure we can connect to the devkit
            String connectExe = Environment.ExpandEnvironmentVariables("\"%DurangoXDK%\\bin\\xbconnect.exe\"");
            if (!RunExternalTool(connectExe, IpAddress, waitTime))
            {
                this.Log.ErrorCtx(LOG_CTXD, "Failed to connect to Durango devkit at IP: {0}. Canceling test!", IpAddress);
                return false; 
            }
            String runtool = String.Format("{0}\\xbo_scripts\\deploy_execs.bat", Options.Branch.Build);

            this.Log.MessageCtx(LOG_CTXD, "Deploy package...");

            return RunExternalTool(runtool, "", waitTime);
        }

        protected override bool DoLaunch(String AUMID)
        {
            String logfilePath = this.Parameters.LogFile;
            logfilePath = logfilePath.Replace("[TargetIP]", this.IpAddress != null ? this.IpAddress : null);
            logfilePath = logfilePath.Replace("[RunIndex]", RunIndex.ToString());

            String args = this.Parameters.CommandLineArguments;
            args = args.Replace("[TargetIP]", this.IpAddress != null ? this.IpAddress : null);
            args = args.Replace("[RunIndex]", RunIndex.ToString());

            String runtool = Environment.ExpandEnvironmentVariables("\"%DurangoXDK%\\bin\\xbapp.exe\"");
            String launchCommandArgs = String.Format("launch {0} +{1} -logfile={2} {3}",
                                                        AUMID,
                                                        Environment.ExpandEnvironmentVariables(this.Parameters.ExePath),
                                                        logfilePath,
                                                        args
                                                    );

            this.Log.MessageCtx(LOG_CTXD, "Launch package...");

            this.LogFilePath = logfilePath;

            this.Log.MessageCtx(LOG_CTXD, "Log file will be at [[{0}]]", this.LogFilePath);

            Debug.Assert(this.IsMonitorNull());
            this.Monitor = new LogMonitor(this.LogFilePath);
            
            TimeSpan waitTime = new TimeSpan(0, 1, 0);
            return RunExternalTool(runtool, launchCommandArgs, waitTime);
        }

        public override void Shutdown(string param)
        {
            System.Diagnostics.Process.Start(Environment.ExpandEnvironmentVariables("\"%DurangoXDK%\\bin\\xbreboot.exe\""));
        }
    }
}
