﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Web;
using System.Threading;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Client;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Services;
using RSG.Pipeline.Core;
using RSG.Pipeline.Automation.Common.Services.Messages;

namespace RSG.Pipeline.Automation.ClientWorker
{

    /// <summary>
    /// 
    /// </summary>
    class Program
    {
        #region Constants
        private static readonly String OPT_SERVICE_AUTOMATION = "automation";
        private static readonly String OPT_SERVICE_FILETRANSFER = "filetransfer";
        private static readonly String OPT_SERVICE_DEFAULT = "";
        private static readonly String OPT_ROLE = "role";
        private static readonly String OPT_ROLE_DEFAULT = "none";
        private static readonly String OPT_ROLE_MAPEXPORT = "mapexport";
        private static readonly String OPT_ROLE_MAPEXPORT_NETWORK = "mapexport_network";
        private static readonly String OPT_ROLE_ASSETBUILDER = "assetbuilder";
        private static readonly String OPT_ROLE_CUTSCENEEXPORT = "cutsceneexport";
        private static readonly String OPT_ROLE_CUTSCENEEXPORT_NETWORK = "cutsceneexport_network";
        private static readonly String OPT_ROLE_CUTSCENEPACKAGE = "cutscenepackage";
        private static readonly String OPT_ROLE_CODEBUILDER = "codebuilder";
        private static readonly String OPT_ROLE_TEST = "test";
        private static readonly String OPT_ROLE_INTEGRATOR = "integrator";
        private static readonly String OPT_ROLE_CODEBUILDER2 = "codebuilder2";
        private static readonly String OPT_ROLE_SMOKETESTER = "smoketester";
        private static readonly String OPT_ROLE_SCRIPTBUILDER = "scriptbuilder";
        private static readonly String OPT_ROLE_CUTSCENESCRIPTER = "cutscenescripter";
        private static readonly String OPT_ROLE_COMMANDRUNNER = "commandrunner";
        private static readonly String OPT_ROLE_DATABASEMANAGER = "databasemanager";
        private static readonly String OPT_ROLE_CUTSCENERPFBUILDER = "cutscenerpfbuilder";
        private static readonly String OPT_ROLE_FRAMECAPTURE = "framecapture";
        
        #endregion // Constants

        #region Member Data
        internal static IUniversalLog Log;
        internal static Guid ClientGuid = Guid.Empty;
        
        // Worker Service
        private static Services.AutomationWorkerService m_AutomationWorker;
        #endregion // Member Data

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        [STAThread]
        static int Main(String[] args)
        {
            try
            {
                // Initialise the logs.
                LogFactory.Initialize();
                Program.Log = LogFactory.CreateUniversalLog("Automation_ClientWorker");
                LogFactory.CreateApplicationConsoleLogTarget();
                UniversalLogFile logfile = LogFactory.CreateUniversalLogFile(Program.Log) as UniversalLogFile;
                Console.CancelKeyPress += ShutdownKill;

                LongOption[] opts = new LongOption[] {
                    new LongOption(OPT_SERVICE_AUTOMATION, LongOption.ArgType.Required,
                        "Automation Service URI to connect to."),
                    new LongOption(OPT_SERVICE_FILETRANSFER, LongOption.ArgType.Required,
                        "File Transfer Service URI to connect to."),
                    new LongOption(OPT_ROLE, LongOption.ArgType.Required,
                        "Automation role.")
                };
                CommandOptions options = new CommandOptions(args, opts);
                options.NoPopups = true; // Force override user option.

                String serviceAutomation = options.ContainsOption(OPT_SERVICE_AUTOMATION) ?
                    (String)options[OPT_SERVICE_AUTOMATION] : OPT_SERVICE_DEFAULT;
                String serviceFileTransfer = options.ContainsOption(OPT_SERVICE_FILETRANSFER) ?
                    (String)options[OPT_SERVICE_FILETRANSFER] : OPT_SERVICE_DEFAULT;

                String roleArg = options.ContainsOption(OPT_ROLE) ?
                    (String)options[OPT_ROLE] : OPT_ROLE_DEFAULT;
                String[] roles = roleArg.Split(new char[] { ',', ';', '.' });
                CapabilityType capability = DeriveCapabilitiesFromRoles(roles);

                InitialiseConsole(capability);

                bool IsRunning = InitialiseWorker(options, serviceAutomation, 
                    serviceFileTransfer, capability);

                while (IsRunning && m_AutomationWorker.IsRunning)
                {
                    System.Threading.Thread.Sleep(100);
                }

                int exitCode = CalculateReturnCode();

                Shutdown();
                Log.Message("Shutting down with exit code {0}", exitCode);

                // ***************************************************************************
                // IMPORTANT (DO NOT REMOVE) : Main thread sleeps before exiting process. 
                // - This is required for the shutdown command.
                // - 1000ms is just an arbitrary value plucked out the air.
                // - Other remote processes throw a connection exception as they
                // won't be able to dispose completely if the process terminated immediately.
                // with the sleep we give them a chance.
                Thread.Sleep(1000);
                // ***************************************************************************

                return (exitCode);
            }
            catch (Exception ex)
            {
                Log.ToolException(ex, "Unhandled exception");
                return (Constants.Exit_Failure);
            }
        }

        /// <summary>
        /// For a list of roles derive the capabilities.
        /// </summary>
        /// <param name="roles"></param>
        /// <returns></returns>
        private static CapabilityType DeriveCapabilitiesFromRoles(String[] roles)
        {
            CapabilityType capability = CapabilityType.None;

            foreach (String role in roles)
            {
                if (0 == String.Compare(role, OPT_ROLE_MAPEXPORT))
                    capability |= CapabilityType.MapExport;
                else if (0 == String.Compare(role, OPT_ROLE_MAPEXPORT_NETWORK))
                    capability |= CapabilityType.MapExportNetwork;
                else if (0 == String.Compare(role, OPT_ROLE_ASSETBUILDER))
                {
                    capability |= CapabilityType.AssetBuilder;
                    capability |= CapabilityType.AssetBuilderRebuilds;
                }
                else if (0 == String.Compare(role, OPT_ROLE_CUTSCENEEXPORT))
                    capability |= CapabilityType.CutsceneExport;
                else if (0 == String.Compare(role, OPT_ROLE_CUTSCENEEXPORT_NETWORK))
                    capability |= CapabilityType.CutsceneExportNetwork;
                else if (0 == String.Compare(role, OPT_ROLE_CUTSCENEPACKAGE))
                    capability |= CapabilityType.CutscenePackage;
                else if (0 == String.Compare(role, OPT_ROLE_CODEBUILDER))
                    capability |= CapabilityType.CodeBuilder;
                else if (0 == String.Compare(role, OPT_ROLE_TEST))
                {
                    capability |= CapabilityType.SchedulingUnitTesting;
                }
                else if (0 == String.Compare(role, OPT_ROLE_INTEGRATOR))
                {
                    capability |= CapabilityType.Integrator;
                }
                else if (0 == String.Compare(role, OPT_ROLE_CODEBUILDER2))
                {
                    capability |= CapabilityType.CodeBuilder2;
                }
                else if (0 == String.Compare(role, OPT_ROLE_SCRIPTBUILDER))
                {
                    capability |= CapabilityType.ScriptBuilder;
                }
				else if (0 == String.Compare(role, OPT_ROLE_SMOKETESTER))
				{
                    capability |= CapabilityType.SmokeTester;
				}
                else if (0 == String.Compare(role, OPT_ROLE_COMMANDRUNNER))
                {
                    capability |= CapabilityType.CommandRunner;
                }
                else if (0 == String.Compare(role, OPT_ROLE_CUTSCENESCRIPTER))
                {
                    capability |= CapabilityType.CutsceneScripter;
                }
                else if (0 == String.Compare(role, OPT_ROLE_DATABASEMANAGER))
                {
                    capability |= CapabilityType.PtfxPackage;
                }
                else if (0 == String.Compare(role, OPT_ROLE_CUTSCENERPFBUILDER))
                {
                    capability |= CapabilityType.CutSceneRPFBuilder;
                }
                else if (0 == String.Compare(role, OPT_ROLE_FRAMECAPTURE))
                {
                    capability |= CapabilityType.FrameCapture;
                }
            }
            return capability;
        }

        /// <summary>
        /// Calculate exit codes : this enables a calling batch script to restart it.
        /// </summary>
        /// <returns></returns>
        private static int CalculateReturnCode()
        {
            int exitCode = Constants.Exit_Success;

            if (m_AutomationWorker.ShutDownType.HasFlag(ShutdownType.Resume))
            {
                exitCode |= Constants.Exit_Resume;
            }

            if (m_AutomationWorker.ShutDownType.HasFlag(ShutdownType.ScmSyncLabel))
            {
                exitCode |= Constants.Exit_SyncLabel;
            }

            if (m_AutomationWorker.ShutDownType.HasFlag(ShutdownType.ScmSyncHead))
            {
                exitCode |= Constants.Exit_SyncHead;
            }

            if (m_AutomationWorker.ShutDownType.HasFlag(ShutdownType.ScmSyncConfig))
            {
                exitCode |= Constants.Exit_SyncConfig;
            }

            if (m_AutomationWorker.ShutDownType.HasFlag(ShutdownType.ScmSyncContent))
            {
                exitCode |= Constants.Exit_SyncContent;
            }

            if (m_AutomationWorker.ShutDownType.HasFlag(ShutdownType.CleanCache))
            {
                exitCode |= Constants.Exit_CleanCache;
            }

            return exitCode;
        }

        /// <summary>
        /// Initialise console display and logging.
        /// </summary>
        /// <returns></returns>
        static void InitialiseConsole(CapabilityType role)
        {
            AssemblyDescriptionAttribute descriptionAttribute = Assembly.GetEntryAssembly()
                    .GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false)
                    .OfType<AssemblyDescriptionAttribute>()
                    .FirstOrDefault();

            if (descriptionAttribute != null)
                Console.Title = String.Format("{0} Role: {1}",
                    descriptionAttribute.Description, role);
        }

        /// <summary>
        /// Initialise automation client worker.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="serviceAutomation"></param>
        /// <param name="serviceFileTransfer"></param>
        /// <param name="role"></param>
        static bool InitialiseWorker(CommandOptions options, 
            String serviceAutomation, String serviceFileTransfer, CapabilityType role)
        {
            Log.Message("Worker initialisation.");

            // Create Worker Service
            Log.Message("WCF Host Initialisation.");
            m_AutomationWorker = new Services.AutomationWorkerService(
                options, serviceAutomation, serviceFileTransfer, role);

            // Start processing.
            m_AutomationWorker.Start();

            return (true);
        }

        /// <summary>
        /// 
        /// </summary>
        static void Shutdown()
        {
            if (null != m_AutomationWorker)
            {
                if (m_AutomationWorker.IsRunning)
                    m_AutomationWorker.Stop();
            }
            Log.Message("Automated service client closed.");
        }

        #region Event Handlers
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void ShutdownKill(Object sender, EventArgs e)
        {
            Log.Warning("Job worker being killed.  Attempting to clean up.");
            Shutdown();
        }
        #endregion // Event Handlers
    }

} // RSG.Pipeline.Automation.ClientWorker namespace
