﻿using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Jobs.JobData;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Consumers;
using RSG.SourceControl.Perforce;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{

    /// <summary>
    /// Enum signifying the stage of processing for a codebuild
    /// </summary>
    internal enum CommandStages
    {
        Invalid = -1,
        PreBuild,       // Prebuild commands.
        MainBuild,      // Actually compiling and linking.
        PostBuild,      // Postbuild commands.
        Publish,        // Upload, Copy and Submit.
        Finalise,       // Final fluff
        NumStages
    };

    internal sealed class CommandRunnerJobProcessor :        
        JobProcessorBase,
        IJobProcessor
    {
        #region Constants
        private const String LOG_CTX = "Command Runner Job Processor";

        /// <summary>
        /// Paremater that indicates this is not a live command runner : some operations will not be performed.
        /// </summary>
        private const String PARAM_DEV = "dev";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Makes a common cast of the BuildStage
        /// </summary>
        /// <returns></returns>
        private int CurrentCommandStage
        {
            get { return (int)CommandStage; }
        }

        private CommandStages CommandStage;
        private Regex ErrorRegex;
        private Regex WarningRegex;

         /// <summary>
        /// Num warnings parsed
        /// </summary>
        private int[] NumWarnings { get; set; }

        /// <summary>
        /// Num errors parsed
        /// </summary>
        private int[] NumErrors { get; set; }

        private string BuildContext { get; set; }
        #endregion //Properties

        #region Constructors
        public CommandRunnerJobProcessor(CommandOptions options, FileTransferServiceConsumer uploadConsumer)
            : base(options, CapabilityType.CommandRunner, uploadConsumer)
        {
        }
        #endregion // Constructors
        #region Controller Methods
        /// <summary>
        /// Process requested job.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public override IJobResult Process(IJob job)
        {
            JobResult jobResult = new JobResult(job.ID, false);

            Debug.Assert(job is CommandRunnerJob, String.Format("Invalid job for processor excepts CommandRunnerJob received {0}.", job.GetType()));
            if (!(job is CommandRunnerJob))
                return null;

            CommandRunnerJob currentJob = job as CommandRunnerJob;

            Init(job);

            BuildContext = String.Format("{0}.{1}", currentJob.CommandName, currentJob.BranchName);


            this.NumErrors = new int[(int)CommandStages.NumStages];
            this.NumWarnings = new int[(int)CommandStages.NumStages];

            IUniversalLog log = LogFactory.CreateUniversalLog("command_runner_process");
            ILogFileTarget logFile = LogFactory.CreateUniversalLogFile(
                String.Format("process_{0}", currentJob.ID), log) as ILogFileTarget;

            bool isDev = this.Parameters.ContainsKey(PARAM_DEV);

            using (P4 p4 = this.Options.Project.SCMConnect())
            {
                CommandStage = CommandStages.PreBuild;

                // Sync to the CL triggering this job.
                SyncToCL(log, p4, currentJob, isDev);

                // *******************************************
                // *** COMMAND RUNNER CONTROL LOGIC FOLLOWS *****
                // *******************************************

                bool[] stageSucceeded = new bool[(int)BuildStage.NumStages];
                stageSucceeded[CurrentCommandStage] = Prebuild(log, job, null, ReadStandardOutput, ReadStandardError);
                if (!stageSucceeded[(int)CommandStage])
                {
                    jobResult.Succeeded = false;
                }

                CommandStage = CommandStages.MainBuild;

                // Edit the upload files
                EnableWriteInArtefactDirectories(log, p4, currentJob.TargetDir);

                IEnumerable<FileInfo> fileInfosStart = null;

                // Take a note of all the file timestamps in the targetDir before the build starts
                if (!String.IsNullOrEmpty(currentJob.TargetDir))
                {
                    DirectoryInfo dirInfoStart = new DirectoryInfo(currentJob.TargetDir);
                    fileInfosStart = dirInfoStart.GetFiles();
                }

                stageSucceeded[CurrentCommandStage] = RunCommands(log, job, ReadStandardOutput, ReadStandardError);
                if (!stageSucceeded[(int)CommandStage])
                {
                    jobResult.Succeeded = false;
                }

                CommandStage = CommandStages.Publish;

                String regex = currentJob.TargetRegex;

                if (!String.IsNullOrEmpty(currentJob.TargetDir) && !String.IsNullOrEmpty(currentJob.PublishDir))
                {

                    // ets the files that were modified in the upload directory.
                    IEnumerable<String> filesModified = FilesModified(log, currentJob.TargetDir, regex, fileInfosStart);

                    // ********************************************************
                    // upload artefacts.
                    // ********************************************************
                    Upload(log, currentJob, currentJob.TargetDir, filesModified);
                
                    String descriptionContext = String.Format("{0}.{1}", BuildContext, currentJob.ID);
                    int submittedCL = 0;
                    IEnumerable<String> filesPublished = Publish(log, currentJob, currentJob.TargetDir, currentJob.PublishDir, filesModified, p4, BuildContext, descriptionContext,out submittedCL, isDev);
                }
                
                stageSucceeded[CurrentCommandStage] = true;

                CommandStage = CommandStages.PostBuild;
                stageSucceeded[CurrentCommandStage] = Postbuild(log, job, null, ReadStandardOutput, ReadStandardError);
                if (!stageSucceeded[(int)CommandStage])
                {
                    jobResult.Succeeded = false;
                }

                jobResult.Succeeded = true;
            }

            // Flush job log; ensure its closed and ready for uploading.
            logFile.Flush();
            LogFactory.CloseUniversalLogFile(logFile);
            LogFactory.CloseUniversalLog(log);

            //  Upload the log file
            UploadLog(job, log, logFile);

            return jobResult;
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Sync to a CL across all the monitoring paths that the job monitored.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="isDev">indicates this is not a builder server - sync may not be desired in development</param>
        private void SyncToCL(IUniversalLog log, P4 p4, CommandRunnerJob currentJob, bool isDev)
        {
            log.MessageCtx(BuildContext, " ");
            log.MessageCtx(BuildContext, "--- SyncToCL ---");

            IEnumerable<uint> clNums = currentJob.Trigger.ChangelistNumbers();

            // Sync to CL if required.
            if (clNums.Any())
            {
                uint maxCl = clNums.Max();
                log.MessageCtx(BuildContext, "Syncing to CL{0}", maxCl);
                ICollection<String> syncFiles = new List<String>();

                // Prevents sync from actually running, yet runs the command.
                if (isDev)
                {
                    log.MessageCtx(BuildContext, "syncing with '-n' option, this will not actually sync these files but shows the results if it were to.");
                    syncFiles.Add("-n");
                }

                foreach (String clFile in currentJob.Trigger.MonitoredPaths())
                {
                    String pathtoSync = String.Empty;
                    if (clFile.Contains("..."))
                    {
                        pathtoSync = String.Format("{0}@{1}", clFile, maxCl);
                    }
                    else if (clFile.Contains("."))
                    {
                        pathtoSync = String.Format("{0}@{1}", clFile, maxCl);
                    }
                    else
                    {
                       pathtoSync = String.Format("{0}/...@{1}", clFile, maxCl);
                    }
                    syncFiles.Add(pathtoSync);
                }

                P4API.P4RecordSet recordSet = p4.Run("sync", syncFiles.ToArray());
                p4.ParseAndLogRecordSetForDepotFilename("sync", recordSet);
                p4.LogP4RecordSetMessages(recordSet);
            }
        }

        private bool RunCommands(IUniversalLog log, IJob job, Action<object, object> ReadStandardOutput, Action<object, object> ReadStandardError)
        {
            CommandRunnerJob commandJob = job as CommandRunnerJob;

            int i = 1;
            foreach (CommandRunnerJobInfo command in commandJob.Commands)
            {
                // Create build process     
                ProcessStartInfo processStartInfo = CreateProcess(log, job, null, command.Command, command.Args);
                if (processStartInfo == null)
                {
                    log.ErrorCtx(LOG_CTX, "Cannot create process");
                    return false;
                }

                // Execute process
                log.MessageCtx(BuildContext, " ");
                log.MessageCtx(BuildContext, "---- Running build command {0}/{1} -----", i, commandJob.Commands.Count());

                ErrorRegex = new Regex(command.ErrorRegex);
                WarningRegex = new Regex(command.WarningRegex);
                Process proc = RunProcess(log, processStartInfo, ReadStandardOutput, ReadStandardError);

                // Handle exit code 
                if (proc.ExitCode != 0)
                {
                    if (i != commandJob.Commands.Count())
                    {
                        log.ErrorCtx(BuildContext, "Build commands is aborting on command {0}/{1} as non zero return code was encountered.", i, commandJob.Commands.Count());
                    }
                    else
                    {
                        log.ErrorCtx(BuildContext, "All build commands completed but non zero return code was encountered.");
                    }
                    return false;
                }

                i++;
            }
            return true;
        }

        /// <summary>
        /// Parses the line of output from the build process.
        /// - counts errors and warnings.
        /// - redirects to log.
        /// </summary>
        /// <param name="line"></param>
        private void ParseLine(IUniversalLog log, String line)
        {
            String context = String.Format("{0}:{1}",BuildContext, CommandStage);

            try
            {
                // Protect against interpretting '{ something other than a number }'
                line = line.Replace("{", "{{").Replace("}", "}}");

                if (ErrorRegex.IsMatch(line))
                {
                    log.ErrorCtx(context, line);
                    this.NumErrors[CurrentCommandStage]++;
                }
                else if (WarningRegex.IsMatch(line))
                {
                    log.WarningCtx(context, line);
                    this.NumWarnings[CurrentCommandStage]++;
                }
                else
                {
                    log.MessageCtx(context, line);
                }
            }
            catch (System.FormatException ex)
            {
                log.WarningCtx(context, "A parsed line was not in the correct format to display.");
            }
        }

        /// <summary>
        /// Helper thread to read standard output while a process is running.
        /// </summary>
        /// <param name="obj"></param>
        private void ReadStandardOutput(object logObj, object obj)
        {
            IUniversalLog log = (IUniversalLog)logObj;
            String line = obj as String;
            if (line == null)
            {
                log.ToolErrorCtx(BuildContext, "Non String object send to ReadStandardError");
                return;
            }

            try
            {
                ParseLine(log, line);
            }
            catch (FormatException fe) // Used to catch issues with formatting log messages
            {
                log.WarningCtx(BuildContext, String.Format("Error Occured while logging from stream stdout {0}", fe.ToString()));
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(BuildContext, e, "Exception parsing stdout stream.");
            }
        }

        /// <summary>
        /// Helper thread to read standard error while a processing is running.
        /// </summary>
        /// <param name="obj"></param>
        private void ReadStandardError(object logObj, object obj)
        {
              IUniversalLog log = (IUniversalLog)logObj;
            String line = obj as String;
            if (line == null)
            {
                log.ToolErrorCtx(BuildContext, "Non String object send to ReadStandardError");
                return;
            }

            try
            {
                if (!String.IsNullOrWhiteSpace(line))
                {
                    ParseLine(log, line);
                }
            }
            catch (FormatException fe) // Used to catch issues with formatting log messages
            {
                log.WarningCtx(BuildContext, String.Format("Error Occured while logging from stream stderr {0}", fe.ToString()));
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(BuildContext, e, "Exception parsing stderr stream.");
            }
        }

    

        #endregion // Private Methods
    }
}
