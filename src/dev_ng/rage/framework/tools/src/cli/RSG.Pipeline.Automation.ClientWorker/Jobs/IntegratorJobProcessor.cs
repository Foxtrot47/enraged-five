﻿using System;
using SIO = System.IO;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Engine;
using RSG.Pipeline.Content;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Build;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Platform;
using System.Text.RegularExpressions;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{

    /// <summary>
    /// Integrator job processor.
    /// </summary>
    internal sealed class IntegratorJobProcessor :
        JobProcessorBase,
        IJobProcessor
    {
        #region Constants
        /// <summary>
        /// Logging context
        /// </summary>
        private static readonly String LOG_CTX = "Integrator Job Processor";

        /// <summary>
        /// Parameter for toggling Perforce submission.
        /// </summary>
        private static readonly String PARAM_SUBMIT = "Submit";

        /// <summary>
        /// Parameter for additional opton into the p4 integ command .. 
        /// eg. -Di or -3
        /// </summary>
        private static readonly String PARAM_P4_INTEG_OPTIONS = "P4 Integ Options";

        /// <summary>
        /// Enable running of integrator executables
        /// </summary>
        private static readonly String PARAM_ENABLE_INTEGRATOR_EXECUTABLES = "Integrator Executables";

        /// <summary>
        /// sync head integrator executables
        /// </summary>
        private static readonly String PARAM_SYNC_INTEGRATOR_EXECUTABLES = "Sync Integrator Executables";        

        /// <summary>
        /// Params for the regex matching on build output
        /// </summary>
        private const String PARAM_REGEX_ERROR = "error regex";
        private const String PARAM_REGEX_WARNING = "warning regex";

        #endregion // Constants

        #region Properties
        /// <summary>
        /// Perforce submit flag; read from configuration XML.
        /// </summary>
        bool Submit { get; set; }

        /// <summary>
        /// The Integrator job that is currently being processed.
        /// </summary>
        IntegratorJob CurrentIntegratorJob { get; set; }

        /// <summary>
        /// Changelist object containing files relevant to the current job.  
        /// </summary>
        P4API.P4PendingChangelist CurrentPendingChangelist { get; set; }

        /// <summary>
        /// The changelist number submitted
        /// </summary>
        private int SubmittedChangelistNumber { get; set; }

        /// <summary>
        /// Integrator Executable output error regex.
        /// - keep these efficient
        /// </summary>
        private Regex ErrorRegex { get; set; }

        /// <summary>
        /// Integrator Executable output warning regex.
        /// - keep these efficient
        /// </summary>
        private Regex WarningRegex { get; set; }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="uploadConsumer"></param>
        /// 
        public IntegratorJobProcessor(CommandOptions options, FileTransferServiceConsumer uploadConsumer)
            : base(options, CapabilityType.Integrator , uploadConsumer)
        {
            this.Submit = true;

            if (this.Parameters.ContainsKey(PARAM_SUBMIT))
                this.Submit = (bool)this.Parameters[PARAM_SUBMIT];

            this.SubmittedChangelistNumber = (int)JobSubmissionState.NotSubmitted;

            this.ErrorRegex = null;
            this.WarningRegex = null;
        }

        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Process requested job.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public override IJobResult Process(IJob job)
        {
            bool result = true;

            IntegratorJobResult jobResult = new IntegratorJobResult(job.ID, false);

            using (P4 p4 = this.Options.Config.Project.SCMConnect())
            {
                Debug.Assert(job is IntegratorJob, "Invalid job for processor.");
                if (!(job is IntegratorJob))
                {
                    // return null in order that it doesn't appear like this was processed
                    return null;
                }

                Init(job);

                CurrentIntegratorJob = (IntegratorJob)(job);

                IUniversalLog log = LogFactory.CreateUniversalLog("process");
                ILogTarget logFile = LogFactory.CreateUniversalLogFile(
                    String.Format("process_{0}", CurrentIntegratorJob.ID), log) as ILogTarget;

                LogStateAtJobProcess(log);

                if (!(CurrentIntegratorJob.Trigger is IFilesTrigger))
                {
                    log.ErrorCtx(LOG_CTX, "Unsupported trigger attached to job.  Must be a IFilesTrigger");
                    return null;
                }

                log.ProfileCtx(LOG_CTX, "Integration Starting.");
                CurrentIntegratorJob.ProcessedAt = DateTime.UtcNow;
                try
                {
                    bool integrated = Integrate(p4, log, logFile);

                    // since a small perforce outage would cause the integrate to fail, we will try once more.
                    if (!integrated)
                    {
                        log.WarningCtx(LOG_CTX, "The integration failed. Since a small perforce outage would cause the integrate to fail, we will try once more.");
                        integrated = Integrate(p4, log, logFile);
                    }

                    result &= integrated;  
                }
                catch (Exception ex)
                {
                    log.ToolExceptionCtx(LOG_CTX, ex, "An unhandled exception occured during integrate: {0}");
                    result = false;
                }

                // Flush this jobs log to disk and merge with other logs for upload to our server host.
                log.ProfileEnd();
                LogFactory.FlushApplicationLog();
                logFile.Flush();

                String mergedLogFilename = CopyMergeAndCleanLogFiles(log, job);

                if (!String.IsNullOrEmpty(mergedLogFilename))
                {
                    result &= !UniversalLogFile.ContainsErrors(mergedLogFilename);
                    this.FileTransferServiceConsumer.UploadFile(job, mergedLogFilename);
                }

                jobResult.SubmittedChangelistNumber = SubmittedChangelistNumber;
                if (!result)
                {
                    // The job result is told of the pending CL that was used when this job failed. 
                    jobResult.PendingIntegrationChangelistNumber = CurrentIntegratorJob.SourceChangelist;
                }
                jobResult.Succeeded = result;

                if (jobResult.Succeeded)
                {
                    log.MessageCtx(LOG_CTX, "***");
                    log.MessageCtx(LOG_CTX, "*** Integration Succeeded : {0} ***", CurrentIntegratorJob.SourceChangelist);
                    log.MessageCtx(LOG_CTX, "***");
                }

                CurrentIntegratorJob = null;
            }

            return jobResult;
        }
        #endregion // Controller Methods

        #region Private Methods

        /// <summary>
        /// Control logic for integrating
        /// </summary>
        /// <returns></returns>
        private bool Integrate(P4 p4, IUniversalLog log, ILogTarget logFile)
        {
            bool createdCL = CreateChangelist(p4, log);
            if (!createdCL)
            {
                log.ErrorCtx(LOG_CTX, "P4 failed to Create CL");
                return false;
            }

            try
            {
                bool ranIntegration = RunIntegration(p4, log, logFile);
                if (!ranIntegration)
                {
                    log.ErrorCtx(LOG_CTX, "P4 failed to Integrate.");
                    return false;
                }

                bool resolved = ResolveIntegration(p4, log);

                if (!resolved)
                {
                    if (SubmittedChangelistNumber == (int)JobSubmissionState.NoFiles)
                    {
                        log.WarningCtx(LOG_CTX, "No files ( possibly already integrated ) the CL will be deleted.");
                        RevertIntegration(p4, log);
                        return true;
                    }
                    else
                    {
                        log.ErrorCtx(LOG_CTX, "P4 failed to Resolve.");
                        bool reverted = this.RevertIntegration(p4, log);
                        if (!reverted)
                        {
                            log.ErrorCtx(LOG_CTX, "P4 failed to Revert/Delete, please visit the integrator machine and tidy its pending CLs.");
                        }
                        return false;
                    }                
                }

                bool enableIntegratorExecutables = this.Parameters.ContainsKey(PARAM_ENABLE_INTEGRATOR_EXECUTABLES) && (bool)this.Parameters[PARAM_ENABLE_INTEGRATOR_EXECUTABLES];
                if (enableIntegratorExecutables)
                {
                    if (!this.HandleIntegratorExecutables(p4, log))
                    {
                        log.ErrorCtx(LOG_CTX, "There was a problem running integrator executables - no submit will occur.");

                        //aaagh one day I'll do this properly and put a finally block in so we don't have to handle revert so frequently, that's a fight for another day though.
                        bool reverted = this.RevertIntegration(p4, log);
                        if (!reverted)
                        {
                            log.ErrorCtx(LOG_CTX, "P4 failed to Revert/Delete.");
                        }
                        return false;
                    }
                }

                bool submitted = this.SubmitIntegration(p4, log);
                if (!submitted)
                {
                    log.ErrorCtx(LOG_CTX, "P4 failed to Submit.");

                    bool reverted = this.RevertIntegration(p4, log);
                    if (!reverted)
                    {
                        log.ErrorCtx(LOG_CTX, "P4 failed to Revert/Delete.");
                    }

                    return false;
                }
            }
            catch (Exception ex)
            {
                // Must revert files - if we don't files with exclusive checkout will remain open on the integrator machine
                log.ExceptionCtx(LOG_CTX, ex, "Exception occured during a stage of integration : revert of files will now occur");

                bool reverted = this.RevertIntegration(p4, log);
                if (!reverted)
                {
                    log.ErrorCtx(LOG_CTX, "P4 failed to Revert/Delete. Files are remaining open on integrator that need to be reverted by hand");
                }
            }

            return true;
        }

        /// <summary>
        /// Actually run 'p4 integ'
        /// http://www.perforce.com/perforce/doc.current/manuals/cmdref/integrate.html
        /// </summary>
        /// <returns></returns>
        private bool RunIntegration(P4 p4, IUniversalLog log, ILogTarget logFile)
        {
            List<string> args = new List<string>();
            if (this.Parameters.ContainsKey(PARAM_P4_INTEG_OPTIONS))
            {
                String extraArgs = this.Parameters[PARAM_P4_INTEG_OPTIONS].ToString();
                IEnumerable<String> splitArgs = extraArgs.Split(null);
                args.AddRange(splitArgs);
            }

            args.Add("-c");
            args.Add(this.CurrentPendingChangelist.Number.ToString());
            args.Add("-b");
            args.Add(this.CurrentIntegratorJob.Integration.BranchSpec);

            // Historically this used be of the form //...@=xxxxxx until I realised it can be more concise. P4V finds the lowest common path, unusual and unexpected!
            args.Add(String.Format("@={0}", this.CurrentIntegratorJob.SourceChangelist));

            log.MessageCtx(LOG_CTX, "Integrate");

            if (!this.P4Run(p4, log, logFile, "integ", args.ToArray()))
            {
                log.ErrorCtx(LOG_CTX, "P4 failed to Integrate.");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Creates a CL for the integration
        /// </summary>
        /// <returns></returns>
        private bool CreateChangelist(P4 p4, IUniversalLog log)
        {
            IFilesTrigger filesTrigger = (CurrentIntegratorJob.Trigger as IFilesTrigger);

            // Create a new changelist and checkout the files, adding them to our enwly created changelist.
            String message = String.Empty;

            if (filesTrigger is IChangelistTrigger)
                message = String.Format("Automated Integration for changelist #{0} - Branchspec ({1}) - {2}\n{3}", ((IChangelistTrigger)filesTrigger).Changelist, this.CurrentIntegratorJob.Integration.BranchSpec, ((IChangelistTrigger)filesTrigger).Description, message);
            else if (filesTrigger is IUserRequestTrigger)
                message = String.Format("User triggered request - {0}", message);

            this.CurrentPendingChangelist = p4.CreatePendingChangelist(message);
    
            if (this.CurrentPendingChangelist != null)
            {
                log.MessageCtx(LOG_CTX, " ");
                log.MessageCtx(LOG_CTX, "*** CL {0} created. ***", this.CurrentPendingChangelist.Number);
                log.MessageCtx(LOG_CTX, " ");
                return true;
            }
            return false;
        }

        /// <summary>
        /// Resolves the jobs current pending changelist
        /// </summary>
        private bool ResolveIntegration(P4 p4, IUniversalLog log)
        {
            string CL = this.CurrentPendingChangelist.Number.ToString();

            log.MessageCtx(LOG_CTX, "Opened files : {0}", CL);
            // --------------------------------------------------------------------
            // - OPENED
            // - Get initial files in changelist
            log.MessageCtx(LOG_CTX, "Get initial files in changelist");
            P4API.P4RecordSet parsedRecordSetInitialOpened = p4.Run("opened", true, "-c", CL);
            IEnumerable<String> intialOpenedFiles = p4.ParseAndLogRecordSetForDepotFilename("opened", parsedRecordSetInitialOpened);
            if (!intialOpenedFiles.Any())
            {
                log.WarningCtx(LOG_CTX, "No files INITIALLY exist in CL, no submit will take place.");
                SubmittedChangelistNumber = (int)JobSubmissionState.NoFiles;
                return false; // this is an error, the integrate NEEDS to produce results
            }

            log.MessageCtx(LOG_CTX, "there are {0} opened files in CL {1}", intialOpenedFiles.Count(), CL);

            // Transform to a collection of depot paths
            ICollection<String> openedDepotPaths = new List<string>();
            foreach (String depotFilename in intialOpenedFiles)
            {
                openedDepotPaths.Add(depotFilename);
            }

            // --------------------------------------------------------------------
            // - SYNC TO HEAD
            log.MessageCtx(LOG_CTX, "Sync head", CL);
            P4API.P4RecordSet parsedRecordSetSync = p4.Run("sync", true,  openedDepotPaths.ToArray());
            p4.LogP4RecordSetMessages(parsedRecordSetSync);

            List<String> allFiles = new List<String>(openedDepotPaths);
            // --------------------------------------------------------------------
            // - RESOLVE SPECIFIC FILES AS FOUND WITH THE P4REGEXRESOLVE OPTIONS
            foreach (KeyValuePair<String, String> kvp in this.CurrentIntegratorJob.Integration.P4RegexResolveOptions)
            {
                String regex = kvp.Key;
                String resolveOptions = kvp.Value;
                ICollection<String> resolveArgs = new List<String>() { resolveOptions };
                ICollection<String> filesToResolve = new List<String>();

                foreach (String filename in allFiles.Where(f => Regex.IsMatch(f, regex, RegexOptions.IgnoreCase)))
                {
                    filesToResolve.Add(filename);
                    resolveArgs.Add(filename);
                    log.MessageCtx(LOG_CTX, "{0} will be resolved using {1} as it matches regex {2}", filename, resolveOptions, regex);
                }

                allFiles.RemoveAll(f => filesToResolve.Contains(f));

                log.MessageCtx(LOG_CTX, "Resolving {0} Files", filesToResolve.Count);

                if (filesToResolve.Any())
                {
                    P4API.P4RecordSet parsedRecordSetResolve = p4.Run("resolve", true, resolveArgs.ToArray());
                    p4.LogP4RecordSetMessages(parsedRecordSetResolve);
                    if (parsedRecordSetResolve.HasErrors())
                    {
                        log.ErrorCtx(LOG_CTX, "p4 resolve : errors were encountered, no files will be submitted.");
                        SubmittedChangelistNumber = (int)JobSubmissionState.Errored;
                    }
                    else
                    {
                        foreach (String filename in filesToResolve)
                        {
                            log.MessageCtx(LOG_CTX, "\tResolved {0}", filename);
                        }
                    }
                }
            }
            
            // --------------------------------------------------------------------
            // - RESOLVE REMAINING FILES
            if (allFiles.Any())
            {
                log.MessageCtx(LOG_CTX, "Resolve Remaining Files");

                List<String> args = new List<String>() { this.CurrentIntegratorJob.Integration.P4ResolveOptions };
                args.AddRange(allFiles);

                P4API.P4RecordSet parsedRecordSetFinalResolve = p4.Run("resolve", true, args.ToArray());
                p4.LogP4RecordSetMessages(parsedRecordSetFinalResolve);
                if (parsedRecordSetFinalResolve.HasErrors())
                {
                    log.ErrorCtx(LOG_CTX, "p4 resolve : errors were encountered, no files will be submitted.");
                    SubmittedChangelistNumber = (int)JobSubmissionState.Errored;
                }
            }

            // After trying to resolve all files ( so we log all the issues ) we return now if there was any resolve error at any time.
            if (SubmittedChangelistNumber == (int)JobSubmissionState.Errored)
            {
                return false;
            }

            // --------------------------------------------------------------------
            // CHECK RESULT OF RESOLVE
            P4API.P4RecordSet parsedRecordPostResolve = p4.Run("opened", true, "-c", CL);
            IEnumerable<String> resolvedFiles = p4.ParseAndLogRecordSetForDepotFilename("opened", parsedRecordPostResolve);

            if (resolvedFiles.Any())
            {
                FileState[] filestates = FileState.Create(p4, resolvedFiles.ToArray());
                bool resolveFailed = false;
                foreach (FileState filestate in filestates)
                {
                    if (filestate.IsUnResolved)
                    {
                        log.ErrorCtx(LOG_CTX, "p4 resolve error : {0} is unresolved", filestate.DepotFilename);
                        resolveFailed = true;
                    }
                    else if (!filestate.IsResolved)
                    {
                        log.ErrorCtx(LOG_CTX, "p4 resolve error : {0} is not resolved ( this should not be possible as unresolved flag should be set)", filestate.DepotFilename);
                        resolveFailed = true;
                    }
                }

                if (resolveFailed)
                {
                    SubmittedChangelistNumber = (int)JobSubmissionState.Errored;
                    return false;
                }
            }

            log.MessageCtx(LOG_CTX, "ResolveFiles : {0} : Success!", CL);
            return true;
        }

        /// <summary>
        /// Submits the jobs current pending changelist
        /// </summary>
        private bool SubmitIntegration(P4 p4, IUniversalLog log)
        {
            string CL = this.CurrentPendingChangelist.Number.ToString();
            log.MessageCtx(LOG_CTX, "SubmitPendingFiles : {0} : {1}", CL, this.CurrentPendingChangelist.Description);

            try
            {
                // --------------------------------------------------------------------
                // NOT SUBMITTING?
                if (!this.Submit)
                {
                    log.MessageCtx(LOG_CTX, "CL {0} not submitted. (IntegratorJobProcessor.SubmitConvertedFiles is false)", CL);
                    SubmittedChangelistNumber = (int)JobSubmissionState.SubmitDisabled;
                    return true;
                }

                // --------------------------------------------------------------------
                // - OPENED
                // - Get initial files in changelist
                log.MessageCtx(LOG_CTX, "Getting opened files in CL {0}", CL);
                P4API.P4RecordSet parsedRecordSetInitialOpened = p4.Run("opened", true, "-c", CL);
                IEnumerable<String> intialOpenedFiles = p4.ParseAndLogRecordSetForDepotFilename("opened", parsedRecordSetInitialOpened);

                log.MessageCtx(LOG_CTX, "There are {0} opened files in CL {0}", intialOpenedFiles.Count(), CL);                

                if (!intialOpenedFiles.Any())
                {
                    log.WarningCtx(LOG_CTX, "No files INITIALLY exist in CL, no submit will take place.");
                    SubmittedChangelistNumber = (int)JobSubmissionState.NoFiles;
                    this.CurrentPendingChangelist.Delete();
                    return false; // this is an error, the integrate NEEDS to produce results
                }

                // --------------------------------------------------------------------
                // SUBMIT
                P4API.P4UnParsedRecordSet unparsedRecordSetSubmit = this.CurrentPendingChangelist.Submit();
                p4.LogP4RecordSetMessages(unparsedRecordSetSubmit);

                if (unparsedRecordSetSubmit.HasErrors())
                {
                    log.ErrorCtx(LOG_CTX, "p4 submit : errors were encountered, no files will be submitted.");
                    SubmittedChangelistNumber = (int)JobSubmissionState.Errored;
                    return false;
                }

                GetSubmittedChanglelistNumber(p4, unparsedRecordSetSubmit);
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Exception in submit stage of CL {0} : This CL will remain unsubmitted. Check if files have been locked?", CL);
                SubmittedChangelistNumber = (int)JobSubmissionState.Errored;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Reverts current pending changelist
        /// </summary>
        private bool RevertIntegration(P4 p4, IUniversalLog log)
        {
            string CL = this.CurrentPendingChangelist.Number.ToString();
            log.MessageCtx(LOG_CTX, "RevertPendingFiles : {0} : {1}", CL, this.CurrentPendingChangelist.Description);

            try
            {
                log.MessageCtx(LOG_CTX, "Reverting");
                P4API.P4RecordSet parsedRecordSetRevert = p4.Run("revert", true, "-c", CL, "//...");
                p4.ParseAndLogRecordSetForDepotFilename("revert", parsedRecordSetRevert);

                log.MessageCtx(LOG_CTX, "Deleting");
                this.CurrentPendingChangelist.Delete();

                SubmittedChangelistNumber = (int)JobSubmissionState.Reverted;
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Exception in revert/delete stage of CL {0} : This CL will remain unsubmitted.", CL);
                SubmittedChangelistNumber = (int)JobSubmissionState.Errored;
                return false;
            }

            return true;
        }

        /// <summary>
        /// Handle integrator executables
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        private bool HandleIntegratorExecutables(P4 p4, IUniversalLog log)
        {
            bool ok = true;
            try
            {
                String CL = this.CurrentPendingChangelist.Number.ToString();
                log.MessageCtx(LOG_CTX, "Handling Integrator Executables on pending CL {0}", CL);

                log.MessageCtx(LOG_CTX, "There are {0} Integrator Executables to handle", this.CurrentIntegratorJob.Integration.IntegratorExecutables.Count());
                if (!this.CurrentIntegratorJob.Integration.IntegratorExecutables.Any())
                {
                    return true;
                }

                foreach (String integratorExecutableFilename in this.CurrentIntegratorJob.Integration.IntegratorExecutables)
                {
                    log.MessageCtx(LOG_CTX, "Handling Integrator Executable {0} on pending CL {1}", integratorExecutableFilename, CL);
                    String fullIntegratorExecutableFilename = System.IO.Path.GetFullPath(this.Options.Branch.Environment.Subst(integratorExecutableFilename));

                    // Sync to the integrator executable filename
                    SyncIntegratorExecutableControlFilename(p4, log, fullIntegratorExecutableFilename);

                    if (!System.IO.File.Exists(fullIntegratorExecutableFilename))
                    {
                        log.ErrorCtx(LOG_CTX, "Integrator Executable filename {0} does not exist, it will not run.", fullIntegratorExecutableFilename);
                        continue;
                    }

                    // For simplicity each 'integratorExecutable' filename is deserialised on the spot as we run.
                    try
                    {
                        XDocument xmlDoc = XDocument.Load(fullIntegratorExecutableFilename);
                        IEnumerable<XElement> xmlElems = xmlDoc.XPathSelectElements("/integrationExecutables/integrationExecutable");
                        log.MessageCtx(LOG_CTX, "Loading {0} integrationExecutables from {1}", xmlElems.Count(), fullIntegratorExecutableFilename);

                        foreach (XElement integrationExecutableElem in xmlElems)
                        {
                            // Load high level settings relating to the integrator executable.
                            IDictionary<String, String> settings = LoadIntegratorExecutableSettings(integrationExecutableElem);

                            // Load exec element.
                            XElement execElem = integrationExecutableElem.XPathSelectElement("exec");
                            String execPath = execElem.XPathSelectElement("path").Value;
                            String execPathExpanded = Options.Branch.Environment.Subst(Environment.ExpandEnvironmentVariables(execPath));

                            // Load arguments and transform.
                            IEnumerable<String> filenamesPassed;
                            IDictionary<String, String> args = LoadIntegratorExecutableArgs(p4, log, CL, fullIntegratorExecutableFilename, execElem, execPath, out filenamesPassed);

                            // there's no file to run on, skip this task.
                            if (!filenamesPassed.Any())
                            {
                                log.WarningCtx(LOG_CTX, "No files eligible for integrationExecutable {0}. Skipping.", fullIntegratorExecutableFilename);
                                continue;
                            }

                            // Potentially edit some files.
                            EditFiles(p4, log, CL, settings, filenamesPassed);

                            // Sync to the actual exe.
                            SyncIntegratorExecutableExecutable(p4, log, execPathExpanded);

                            // Finally we run the executable.
                            int retCode = RunIntegrationExecutable(log, this.CurrentIntegratorJob, settings["name"], execPathExpanded, args);

                            // Handle return code - non zero is an showstopper for the integration.
                            if (retCode != 0)
                            {
                                log.ErrorCtx(LOG_CTX, "integrationExecutable {0}: {1} returned non zero ret code {2}", settings["name"], fullIntegratorExecutableFilename, retCode);
                                ok = false;
                            }
                        }
                    }
                    catch (XmlException xmlException)
                    {
                        log.ToolExceptionCtx(LOG_CTX, xmlException, "Invalid xml file {0}.", fullIntegratorExecutableFilename);
                        ok = false;
                    }
                    catch (Exception ex)
                    {
                        log.ToolExceptionCtx(LOG_CTX, ex, "Unknown issue encountered during integrator executable handling of control file {0}", fullIntegratorExecutableFilename);
                        ok = false;
                    }
                }
            }
            catch (Exception ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Unknown issue encountered during integrator executable handling.");
                ok = false;
            }
            return ok;
        }

        /// <summary>
        /// Load high level settings relating to the integrator executable.
        /// - these should mostly consist of handy key value pair settings for anything special we might want to do.
        /// - doesn't load the actual 'integratorexecutable' executable & arguments.
        /// </summary>
        /// <param name="integrationExecutableElem"></param>
        /// <returns></returns>
        private static IDictionary<String, String> LoadIntegratorExecutableSettings(XElement integrationExecutableElem)
        {
            // Load up immediate elements in the integrator executable and insert in dictionary.
            IEnumerable<String> keys = new List<String> { "name", "edit" };
            IDictionary<String, String> dict = new Dictionary<String, String>();
            foreach (String key in keys)
            {
                XElement elem = integrationExecutableElem.XPathSelectElement(key);
                if (elem != null)
                {
                    dict.Add(key, elem.Value);
                }
            }
            return dict;
        }

        /// <summary>
        /// Potentially edit some files if they match the regex in the xml element 'edit'.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="log"></param>
        /// <param name="CL"></param>
        /// <param name="dict"></param>
        /// <param name="filenamesPassed"></param>
        private static void EditFiles(P4 p4, IUniversalLog log, String CL, IDictionary<String, String> dict, IEnumerable<String> filenamesPassed)
        {
            // Now, what do we want to do with these filenames - do we want to open them for edit in the CL?
            if (dict.ContainsKey("edit"))
            {
                String regex = dict["edit"];
                Regex editRegex = new Regex(regex, RegexOptions.IgnoreCase);
                IEnumerable<String> editFiles = filenamesPassed.Where(f => editRegex.IsMatch(f));
                if (editFiles.Any())
                {
                    log.MessageCtx(LOG_CTX, "Editing files {0}", String.Join(" ", editFiles));
                    List<String> editArgs = new List<String> { "-c", CL };
                    editArgs.AddRange(filenamesPassed);
                    P4API.P4RecordSet recordSet = p4.Run("edit", editArgs.ToArray());
                    p4.LogP4RecordSetMessages(recordSet);
                }
            }
        }

        /// <summary>
        /// Sync to the 'IntegratorExecutable' control file.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="log"></param>
        /// <param name="fullIntegratorExecutableFilename"></param>
        private void SyncIntegratorExecutableControlFilename(P4 p4, IUniversalLog log, String fullIntegratorExecutableFilename)
        {
            if (this.Parameters.ContainsKey(PARAM_SYNC_INTEGRATOR_EXECUTABLES) && (bool)this.Parameters[PARAM_SYNC_INTEGRATOR_EXECUTABLES])
            {
                log.MessageCtx(LOG_CTX, "Syncing Integrator Executable Control File {0}", fullIntegratorExecutableFilename);
                P4API.P4RecordSet recordSet = p4.Run("sync", fullIntegratorExecutableFilename);
                p4.ParseAndLogRecordSetForDepotFilename("sync", recordSet);
                p4.LogP4RecordSetMessages(recordSet);
            }
        }

        /// <summary>
        /// Sync to the 'integratorExecutable' executable file.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="log"></param>
        /// <param name="execPathExpanded"></param>
        private void SyncIntegratorExecutableExecutable(P4 p4, IUniversalLog log, String execPathExpanded)
        {
            if (this.Parameters.ContainsKey(PARAM_SYNC_INTEGRATOR_EXECUTABLES) && (bool)this.Parameters[PARAM_SYNC_INTEGRATOR_EXECUTABLES])
            {
                log.MessageCtx(LOG_CTX, "Syncing Integrator Executable {0}", execPathExpanded);
                P4API.P4RecordSet recordSet = p4.Run("sync", execPathExpanded);
                p4.ParseAndLogRecordSetForDepotFilename("sync", recordSet);
                p4.LogP4RecordSetMessages(recordSet);
            }
        }

        /// <summary>
        /// Load the arguments
        /// - required special handling, including special case for file matching and branch environment substitution.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="log"></param>
        /// <param name="CL"></param>
        /// <param name="fullIntegratorExecutableFilename"></param>
        /// <param name="execElem"></param>
        /// <param name="execPath"></param>
        /// <param name="filenamesPassed">the filenames that will be passed to the executable</param>
        /// <returns></returns>
        private IDictionary<String, String> LoadIntegratorExecutableArgs(P4 p4, IUniversalLog log, String CL, String fullIntegratorExecutableFilename, XElement execElem, String execPath, out IEnumerable<String> filenamesPassed)
        {
            filenamesPassed = new List<String>();

            IDictionary<String, String> args = new Dictionary<String, String>();
            IEnumerable<XElement> xmlElemArgs = execElem.XPathSelectElements("arguments/argument");

            foreach (XElement xmlElemArg in xmlElemArgs)
            {
                IBranch branch = this.Options.Branch;
                XElement nameElem = xmlElemArg.Element("name");
                XElement valueElem = xmlElemArg.Element("value");

                if (nameElem == null)
                {
                    log.ErrorCtx(LOG_CTX, "Missing 'name' element for exec argument {0} : {1}", execPath, fullIntegratorExecutableFilename);
                    continue;
                }

                if (valueElem == null)
                {
                    log.ErrorCtx(LOG_CTX, "Missing 'value' element for exec argument {0} : {1}", execPath, fullIntegratorExecutableFilename);
                    continue;
                }

                // Yes I use attributes, arguably because I consider it 'metadata' for the underlying configuration.
                // that's about the only time they really should be used.
                if (valueElem.Attribute("project") != null && valueElem.Attribute("branch") != null)
                {
                    // Get the project and branch desired - this can be used to subst the environment of the values used here.
                    String projectName = valueElem.Attribute("project").Value;
                    String branchName = valueElem.Attribute("branch").Value;

                    KeyValuePair<String, IProject> projectKvp = Options.Config.AllProjects().Where(p => p.Value.Name.Equals(projectName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                    if (projectKvp.Equals(default(KeyValuePair<String, IProject>)))
                    {
                        log.ErrorCtx(LOG_CTX, "Project {0} is unknown : cannot derive project and branch.", projectName);
                        continue;
                    }
                    IProject project = projectKvp.Value;

                    KeyValuePair<String, IBranch> branchKvp = project.Branches.Where(b => b.Key.Equals(branchName, StringComparison.CurrentCultureIgnoreCase)).FirstOrDefault();
                    if (branchKvp.Equals(default(KeyValuePair<String, IBranch>)))
                    {
                        log.ErrorCtx(LOG_CTX, "Branch {0} within project {1} is unknown : cannot derive project and branch.", branchName, projectName);
                        continue;
                    }
                    branch = branchKvp.Value;
                }

                String argName = nameElem.Value;
                String argValue = valueElem.Value;

                argValue = branch.Environment.Subst(argValue);

                // special case handling of argument 'files'
                // - concatenate the files into a ';' separated list.
                // - where the files are required to match the directory in the 'value' element.
                // - and the files belong to the CL.
                if (argName.Equals("files", StringComparison.CurrentCultureIgnoreCase))
                {
                    // Now get the files that are in the CL that would match on the criteria for 
                    ICollection<String> filesToPass = new List<String>();

                    Changelist changelist = new Changelist(p4, int.Parse(CL));
                    FileMapping[] fms = FileMapping.Create(p4, changelist.Files.ToArray());

                    String fullPath = System.IO.Path.GetFullPath(argValue) + @"/";
                    String pathFilter = System.IO.Path.GetDirectoryName(fullPath);

                    foreach (FileMapping fm in fms)
                    {
                        // check it starts with the directory from the 'value' element.
                        String localDir = System.IO.Path.GetDirectoryName(fm.LocalFilename);
                        if (localDir.StartsWith(pathFilter, StringComparison.CurrentCultureIgnoreCase))
                        {
                            filesToPass.Add(fm.LocalFilename);
                        }
                    }

                    // For returning - in case of any special handling of files before the executable is run.
                    filenamesPassed = filesToPass;

                    if (!filesToPass.Any())
                    {
                        log.MessageCtx(LOG_CTX, "No files to pass to integrator executable - no work to do I presume.");
                        continue;
                    }

                    String allFiles = String.Join(";", filesToPass);
                    argValue = allFiles;
                }
                args.Add(argName, argValue);
            }
            return args;
        }

        /// <summary>
        /// Run Integrator executable
        /// </summary>
        /// <param name="log"></param>
        /// <param name="job"></param>
        /// <param name="name"></param>
        /// <param name="execPath"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        private int RunIntegrationExecutable(IUniversalLog log, IJob job, String name, String execPath, IEnumerable<KeyValuePair<String, String>> args)
        {
            // Build arguments as string.
            StringBuilder arguments = new StringBuilder();
            foreach (KeyValuePair<String,String> kvp in args)
            {
                String argName = kvp.Key;
                String argVal = kvp.Value;

                arguments.AppendFormat(" -{0} {1}", argName, argVal);
            }

            // Create build process   
            IDictionary<String,String> environment = new Dictionary<String, String>();
            ProcessStartInfo processStartInfo = CreateProcess(log, job, environment, execPath, arguments.ToString());
            if (processStartInfo == null)
            {
                log.ErrorCtx(LOG_CTX, "Cannot create process");
                return -1;
            }

            // Setup regexes for parsing errors and warnings, these are used by the stdio, sterr eventhandlers (ReadStandardOutput, ReadStandardError)
            if (this.Parameters.ContainsKey(PARAM_REGEX_ERROR))
            {
                String regex = (String)this.Parameters[PARAM_REGEX_ERROR];
                ErrorRegex = new Regex(regex, RegexOptions.IgnoreCase);
            }

            if (this.Parameters.ContainsKey(PARAM_REGEX_WARNING))
            {
                String regex = (String)this.Parameters[PARAM_REGEX_WARNING];
                WarningRegex = new Regex(regex, RegexOptions.IgnoreCase);
            }

            // Execute process
            log.MessageCtx(LOG_CTX, " ");
            log.MessageCtx(LOG_CTX, "Running integrator executable command {0} {1}", execPath, arguments);

            System.Diagnostics.Process proc = RunProcess(log, processStartInfo, ReadStandardOutput, ReadStandardError);

            // Handle exit code 
            log.MessageCtx(LOG_CTX, " ");
            if (proc.ExitCode != 0)
            {                
                log.ErrorCtx(LOG_CTX, "Integrator executable command {0} {1} returned exitcode {2}", execPath, arguments, proc.ExitCode);
            }
            else
            {
                log.MessageCtx(LOG_CTX, "Integrator executable command {0} {1} ran successfully and returned exitcode {2}", execPath, arguments, proc.ExitCode);
            }
            log.MessageCtx(LOG_CTX, " ");

            return proc.ExitCode;
        }

        /// <summary>
        /// Handle stdout while a processing is running.
        /// </summary>
        /// <param name="logObj">the log</param>
        /// <param name="obj">the string</param>
        private void ReadStandardOutput(object logObj, object obj)
        {
            IUniversalLog log = (IUniversalLog)logObj;
            String line = obj as String;
            if (line == null)
            {
                log.ToolErrorCtx(LOG_CTX, "Non String object sent to ReadStandardError");
                return;
            }

            try
            {
                ParseLine(log, line, ErrorRegex, WarningRegex);
            }
            catch (FormatException fe) // Used to catch issues with formatting log messages
            {
                log.WarningCtx(LOG_CTX, String.Format("Error Occured while logging from stream stdout {0}", fe.ToString()));
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Exception parsing stdout stream.");
            }
        }

        /// <summary>
        /// Handle stderr while a processing is running.
        /// </summary>
        /// <param name="logObj">the log</param>
        /// <param name="obj">the string</param>
        private void ReadStandardError(object logObj, object obj)
        {
            IUniversalLog log = (IUniversalLog)logObj;
            String line = obj as String;
            if (line == null)
            {
                log.ToolErrorCtx(LOG_CTX, "Non String object sent to ReadStandardError");
                return;
            }

            try
            {
                if (!String.IsNullOrWhiteSpace(line))
                {
                    log.ErrorCtx(LOG_CTX, line);
                }
            }
            catch (FormatException fe) // Used to catch issues with formatting log messages
            {
                log.WarningCtx(LOG_CTX, String.Format("Error Occured while logging from stream stderr {0}", fe.ToString()));
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Exception parsing stderr stream.");
            }
        }

        /// <summary>
        /// Parses the line of output from the process output.
        /// - redirects to log.
        /// - protects against unusual output
        /// </summary>
        /// <param name="log"></param>
        /// <param name="line"></param>
        /// <param name="errorRegex">regex used in parsing errors</param>
        /// <param name="warningRegex">regex used in parsing warnings</param>
        private static void ParseLine(IUniversalLog log, String line, Regex errorRegex, Regex warningRegex)
        {
            try
            {
                // Protect against interpretting '{ something other than a number }'
                line = line.Replace("{", "{{").Replace("}", "}}");

                if (errorRegex.IsMatch(line))
                {
                    log.ErrorCtx(LOG_CTX, line);
                }
                else if (warningRegex.IsMatch(line))
                {
                    log.WarningCtx(LOG_CTX, line);
                }
                else
                {
                    log.MessageCtx(LOG_CTX, line);
                }
            }
            catch (System.FormatException ex)
            {
                log.WarningCtx(LOG_CTX, "A parsed line was not in the correct format to display.");
            }
        }

        /// <summary>
        /// gets the changelist number from the unparsed recordset
        /// - otherwise assumes the CL remained the same.
        /// </summary>
        /// <param name="unparsedRecordSetSubmit"></param>
        private void GetSubmittedChanglelistNumber(P4 p4, P4API.P4UnParsedRecordSet unparsedRecordSetSubmit)
        {
            int submittedCLNum = p4.GetSubmittedChangelistNumber(unparsedRecordSetSubmit);
            if (submittedCLNum >= 0)
            {
                SubmittedChangelistNumber = submittedCLNum;
            }
            else
            {
                SubmittedChangelistNumber = CurrentPendingChangelist.Number;
            }
        }

        /// <summary>
        /// P4.Run wrapper with exception handler and additional logging.
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        private bool P4Run(P4 p4, IUniversalLog log, ILogTarget logFile, String operation, params String[] args)
        {
            bool success = true;
            P4API.P4RecordSet ret = null;

            try
            {
                ret = p4.Run(operation, true, args);

                // Log messages
                foreach (String message in ret.Messages)
                {
                    // Handle this issue; we can;t fix it but we must error upon it.
                    // 'can't delete exclusive file already opened'
                    if (message.IndexOf("exclusive file already opened", StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        log.ErrorCtx(LOG_CTX, "{0}", message);
                        success = false;
                    }
                    else
                    {
                        log.MessageCtx(LOG_CTX, "{0}", message);
                    }
                }

                // Log warnings
                foreach (String warning in ret.Warnings)
                {
                    log.WarningCtx(LOG_CTX, "{0}", warning);
                }

                // Log errors
                foreach (String error in ret.Errors)
                {
                    log.ErrorCtx(LOG_CTX, "{0}", error);
                    success = false;
                }
            }
            catch (Exception ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Exception running {0}: {1}", operation, ex.Message);
                UploadLog(this.CurrentIntegratorJob, log, logFile);
                return (false);
            }

            return (success);
        }

        /// <summary>
        /// It's nice to get a bit of log output in the console
        /// that makes it clear when jobs start & end.
        /// </summary>
        private void LogStateAtJobProcess(IUniversalLog log)
        {
            log.MessageCtx(LOG_CTX, " ");
            log.MessageCtx(LOG_CTX, "******************************************************");
            log.MessageCtx(LOG_CTX, "Preparing integrator for job: {0}", CurrentIntegratorJob.ID);

            if (!this.Submit)
            {
                log.MessageCtx(LOG_CTX, "*** Submit is disabled. ***");
            }
        }

        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.ClientWorker.Jobs namespace
