﻿using System;
using SIO = System.IO;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal; 
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Engine;
using RSG.Pipeline.Content;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Build;
using RSG.Pipeline.Services;
using RSG.Pipeline.Content.Algorithm;
using RSG.Platform;
using Ionic.Zip;
using P4API;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{
    class SmokeTestAnalyzerMP : 
        ISmokeTestAnalyzer
    {

        public SmokeTestAnalyzerMP(IDictionary<String, object> parameters, IBranch branch, BaseSmokeTestJob job,
                                  IUniversalLog log, ILogTarget logFile, CommandOptions options)
            : base(parameters, branch, job, log, logFile, options)
        {

        }

        public override bool RunAnalysis()
        {
            // sync data in preparation of running the game
            String syncTo = String.Format(@"@{0}", this.CurrentJob.Changelist.ToString());

            if (this.P4CurrentLabel.Contains("latest"))
            {
                //skip the label; latest on server
                syncTo = String.Format(@"#head");
            }

            SyncP4(syncTo);
            RunTheGame();
            String notUsed;

            foreach (SmoketestTargetObserver observer in TargetObservers)
            {
                observer.OnRunFinished += new SmoketestTargetObserver.RunFinishedDelegate(OnRunFinished);
            }

            WatchForCrashes(out notUsed);

            return true;
        }

        void OnRunFinished(LogMonitor log, ISmoketestTarget target)
        {
            SaveOutputFromRun(log, target);
            SendMail(log, target);
        }

        protected virtual bool WatchForCrashes(out String outCrashMsg)
        {
            int runtime = 0;
            outCrashMsg = "Not set";

            while (runtime < this.GameRunTimeMS)
            {
                foreach (SmoketestTargetObserver observer in TargetObservers)
                {
                    observer.Tick();

                    if (!observer.IsTargetRunningGame() && this.CurrentJob.RebootOnFailure)
                    {
                        observer.Target.Relaunch();
                    }
                }

                System.Threading.Thread.Sleep(1000);
                runtime += 1000;
            }

            return true;
        }


        protected void SendMail(LogMonitor log, ISmoketestTarget target)
        {
            StringBuilder body = new StringBuilder();

            if (log.HasFatalMessage)
            {
                body.Append("========================Fatal Message========================\n");
                body.Append("\n");
                body.Append(log.HasFatalMessage);
                body.Append("\n");
            }
            body.Append("========================All Logged Messages=======================\n");
            body.Append("\n");

            foreach (LogMessage msg in log.Messages)
            {
                body.AppendFormat("{0} - {1}", msg.Severity, msg.Message);    
            }

            String outcome = "Complete";

            if (log.HasCrash)
            {
                outcome = "CRASH";
            }
            else if (log.HasFatalMessage)
            {
                outcome = "FATAL";
            }
            else if (log.Messages.Count > 0)
            {
                outcome = "HANG";
            }

            String[] attachment = new String[0];

            String subject = String.Format("[MP Smoketest] {0} {1} CL{2} Failure: {3} Error Count: {4}",
                                        Branch.Project.Name, this.Platform.PlatformToRagebuilderPlatform(),
                                        this.CurrentJob.Changelist, outcome, log.Messages.Count);

            RSG.UniversalLog.Util.Email.SendEmail(Branch.Project.Config, MailRecipients.ToArray(),
                                                  subject, body.ToString(), attachment);

            this.Log.MessageCtx(LOG_CTX, "Mailing failure results...");
        }
    }
}