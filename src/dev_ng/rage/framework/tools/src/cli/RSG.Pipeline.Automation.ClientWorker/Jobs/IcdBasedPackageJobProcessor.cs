﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Threading;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Content;
using RSG.Pipeline.Content.Algorithm;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Platform;
using RSG.SourceControl.Perforce;
using Ionic.Zip;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{
    abstract class IcdBasedPackageJobProcessor :
        BasePackageProcessor
    {
        #region Constants
        /// <summary>
        /// Filename for filelist
        /// </summary>
        protected readonly String MASTER_ICD_LIST_NAME = "master_icd_list.xml";
        #endregion

        #region Properties
        /// <summary>
        /// Full path to master icd file.
        /// </summary>
        protected String MasterIcdListPath { get; set; }
        #endregion

        #region Constructors
        public IcdBasedPackageJobProcessor(CommandOptions options, CapabilityType type, FileTransferServiceConsumer fileTransferConsumer)
            : base(options, type, fileTransferConsumer)
        {
            MasterIcdListPath = this.ZipRootDir + "\\" + MASTER_ICD_LIST_NAME;
        }
        #endregion

        #region Controller Methods
        /// <summary>
        /// This function is implemented due to BasePackageProcessor having this function being abstract.
        /// This should never get called
        /// </summary>
        protected override void GetZipFiles(ref Dictionary<String, List<String>> AllZips)
        {
            Log.ErrorCtx(LOG_CTX, "Calling GetZipFiles() directly in IcdBasedPackageJobProcessor.cs. This needs to be overwritten!");
            throw new Exception();
        }

        /// <summary>
        /// Uploads all data associated with the job.
        /// </summary>
        /// <param name="job"></param>
        /// <param name="zipFiles"></param>
        /// <returns>result</returns>
        protected override bool UploadData(IJob job, Dictionary<String, List<String>> zipFiles)
        {
            bool result = base.UploadData(job, zipFiles);

            //Upload the master_icd_list.xml
            String zipPathBase = this.Parameters.ContainsKey(PARAM_INPUT_ZIP_DIR) ? (String)this.Parameters[PARAM_INPUT_ZIP_DIR] : "";

            String branchName = this.Options.Branch.Name;

            if (SupportCore)
            {
                result &= UploadMasterIcdList(job, this.Options.Project, Path.Combine(this.Options.Branch.Environment.Subst(zipPathBase), MASTER_ICD_LIST_NAME));
            }

            if (SupportDLC)
            {
                foreach (KeyValuePair<String, IProject> project in this.Options.Config.DLCProjects)
                {
                    IBranch dlcBranch = project.Value.Branches[branchName];
                    result &= UploadMasterIcdList(job, project.Value, Path.Combine(dlcBranch.Environment.Subst(zipPathBase), MASTER_ICD_LIST_NAME));
                }
            }

            return result;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Uploads the icd list
        /// </summary>
        /// <param name="job"></param>
        /// <param name="project"></param>
        /// <param name="icdPath"></param>
        /// <returns></returns>
        private bool UploadMasterIcdList(IJob job, IProject project, String icdPath)
        {
            bool result = true;

            String icdRelativePath = Path.Combine(project.Name, MASTER_ICD_LIST_NAME);
            if (!String.IsNullOrEmpty(icdPath))
            {
                result &= FileTransferServiceConsumer.UploadFile(job, icdPath, icdRelativePath);
                if (result == false)
                {
                    Log.ErrorCtx(LOG_CTX, String.Format("Unable to upload {0} to the server.", icdRelativePath));
                    return false;
                }
            }

            return result;
        }
        #endregion
    }
}
