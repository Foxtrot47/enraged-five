﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Threading;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Content;
using RSG.Pipeline.Content.Algorithm;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Platform;
using RSG.SourceControl.Perforce;
using Ionic.Zip;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{
    class CutscenePackageJobProcessor :
        IcdBasedPackageJobProcessor
    {
        #region Constants
        protected readonly new String LOG_CTX = "Cutscene Package Job Processor";
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="fileTransferConsumer"></param>
        /// 
        public CutscenePackageJobProcessor(CommandOptions options, FileTransferServiceConsumer fileTransferConsumer)
            : base(options, CapabilityType.CutscenePackage, fileTransferConsumer)
        {
        }
        #endregion // Constructor(s)


        #region Controller Methods
        /// <summary>
        /// Get zip files that are going to be uploaded
        /// </summary>
        /// <param name="AllZips"></param>
        /// <returns></returns>
        protected override void GetZipFiles(ref Dictionary<String, List<String>> AllZips)
		{
            if (!this.Parameters.ContainsKey(PARAM_INPUT_ZIP_DIR) || (String)this.Parameters[PARAM_INPUT_ZIP_DIR] == String.Empty)
            {
                Log.ErrorCtx(LOG_CTX, "Input Zip Directory must be defined for Cutscene Packaging. See Processor settings .xml");
            }

            String zipPathBase = (String)this.Parameters[PARAM_INPUT_ZIP_DIR];

            String branchName = this.Options.Branch.Name;

            if (SupportCore)
            {
                GetZipFilesForPath(this.Options.Branch.Environment.Subst(zipPathBase), ref AllZips, this.Options.Project.Name);
            }

            if (SupportDLC)
            {
                foreach (KeyValuePair<String, IProject> project in this.Options.Config.DLCProjects)
                {
                    IBranch dlcBranch = project.Value.Branches[branchName];
                    GetZipFilesForPath(dlcBranch.Environment.Subst(zipPathBase), ref AllZips, project.Value.Name);
                }
            }
        }

        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Stores zips under a certain path in a dictionary
        /// </summary>
        /// <param name="path"></param>
        /// <param name="AllZips"></param>
        /// <param name="projectName"></param>
        private void GetZipFilesForPath(String path, ref Dictionary<String, List<String>> AllZips, String projectName)
        {
            List<String> zipFiles = new List<string>();

            foreach (String subdir in System.IO.Directory.GetDirectories(path))
            {
                foreach (String zipFile in System.IO.Directory.GetFiles(subdir))
                {
                    zipFiles.Add(zipFile);
                }
            }

            AllZips.Add(projectName, zipFiles);
        }
        #endregion
    }
}
