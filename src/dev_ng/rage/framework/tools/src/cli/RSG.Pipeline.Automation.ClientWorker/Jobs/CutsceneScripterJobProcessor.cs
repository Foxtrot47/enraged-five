﻿using System;
using SIO = System.IO;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Engine;
using RSG.Pipeline.Content;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Build;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Platform;
using RSG.Pipeline.Automation.Common.Batch;
using System.Text.RegularExpressions;
using Microsoft.Win32;
using System.Threading;
using System.IO;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{

    /// <summary>
    /// Asset Builder job processor.
    /// </summary>
    internal sealed class CutsceneScripterJobProcessor :
        JobProcessorBase,
        IJobProcessor
    {
        #region Constants
        /// <summary>
        /// Logging context
        /// </summary>
        private static readonly String LOG_CTX = "Cutscene Scripter Job Processor";

        /// <summary>
        /// Paramater that can change the statistics folder for it's binaries from statistics to statistics_dev or whatever
        /// </summary>
        private static readonly String DCC_SOURCE_EXTENSION = ".fbx";

        /// <summary>
        /// Paramater that can change the statistics folder for it's binaries from statistics to statistics_dev or whatever
        /// </summary>
        private static readonly String PARAM_MOTIONBUILDER_PYTHON_SCRIPT = "Motionbuilder Python Script";

        /// <summary>
        /// Paramater that can change the statistics folder for it's binaries from statistics to statistics_dev or whatever
        /// </summary>
        private static readonly String PARAM_MOTIONBUILDER_PYTHON_LIGHT_SCRIPT = "Motionbuilder Python Light Script";

        /// <summary>
        /// Paramater that can change the statistics folder for it's binaries from statistics to statistics_dev or whatever
        /// </summary>
        private static readonly String PARAM_CAMERA_PLOT_SCRIPT = "Camera Plot Script";

        /// <summary>
        /// Paramater that can change the statistics folder for it's binaries from statistics to statistics_dev or whatever
        /// </summary>
        private static readonly String PARAM_PUBLISH_DIR = "Publish Dir";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Location that the output files get published to.
        /// </summary>
        private String PublisDir { get; set; }

        /// <summary>
        /// The asset builder job that is currently being processed.
        /// </summary>
        String MotionbuilderPythonScript { get; set; }

        /// <summary>
        /// The asset builder job that is currently being processed.
        /// </summary>
        String LightingPythonScript { get; set; }
        
        /// <summary>
        /// The asset builder job that is currently being processed.
        /// </summary>
        String CameraPlotScript { get; set; }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="uploadConsumer"></param>
        /// 
        public CutsceneScripterJobProcessor(CommandOptions options, FileTransferServiceConsumer uploadConsumer)
            : base(options, CapabilityType.CutsceneScripter, uploadConsumer, null, null, new List<BuildToken>() {BuildToken.ANIMATION, BuildToken.CHANGELIST, BuildToken.POSITION, 
                                                                                                                 BuildToken.RESOLUTION, BuildToken.RANGE, BuildToken.SOURCECHANGELIST,
                                                                                                                 BuildToken.SOURCEFILE, BuildToken.SOURCEREVISION, BuildToken.BAKECAMERA, BuildToken.LIGHTING})
        {
            if (this.Parameters.ContainsKey(PARAM_MOTIONBUILDER_PYTHON_SCRIPT))            
                this.MotionbuilderPythonScript = (string)this.Parameters[PARAM_MOTIONBUILDER_PYTHON_SCRIPT];

            if (this.Parameters.ContainsKey(PARAM_CAMERA_PLOT_SCRIPT))
                this.CameraPlotScript = (string)this.Parameters[PARAM_CAMERA_PLOT_SCRIPT]; 

            if(this.Parameters.ContainsKey(PARAM_PUBLISH_DIR))
                this.PublisDir = (string) this.Parameters[PARAM_PUBLISH_DIR];

            if (this.Parameters.ContainsKey(PARAM_MOTIONBUILDER_PYTHON_LIGHT_SCRIPT))
                this.LightingPythonScript = (string)this.Parameters[PARAM_MOTIONBUILDER_PYTHON_LIGHT_SCRIPT];
            
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Process requested job.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public override IJobResult Process(IJob job)
        {
            IUniversalLog log = LogFactory.CreateUniversalLog("process");
            IUniversalLogTarget logFile = LogFactory.CreateUniversalLogFile(
                String.Format("process_{0}", job.ID), log);

            Debug.Assert(job.Role == CapabilityType.CutsceneScripter, String.Format("Invalid job for processor: {0}.",
                job.GetType().Name));
            if (job.Role != CapabilityType.CutsceneScripter)
            {
                return null;
            }

            bool result = true;
            SubmittedChangelistJobResult jobResult = new SubmittedChangelistJobResult(job.ID, false);


            using (P4 p4 = this.Options.Config.Project.SCMConnect())
            {
                Init(job);

                // Sync the source files we will be passing to the asset processing engine.
                List<String> inputs = new List<String>();
                ISet<String> syncFiles = new HashSet<String>();
                ISet<String> revertFiles = new HashSet<String>();

                if (!(job.Trigger is FilesTrigger))
                {
                    log.ErrorCtx(LOG_CTX, "Unsupported trigger attached to job.  Must be a FilesTrigger");
                    return null;
                }
                FilesTrigger filesTrigger = (job.Trigger as FilesTrigger);
                IEnumerable<FileMapping> files = FileMapping.Create(p4, filesTrigger.Files);

                foreach (FileMapping filemapping in files)
                {
                    if (SIO.Path.GetExtension(filemapping.LocalFilename).ToLower() != DCC_SOURCE_EXTENSION) continue;

                    if (!syncFiles.Any())
                        syncFiles.Add("-f");

                    inputs.Add(filemapping.LocalFilename);
                    String perforcePath = filemapping.DepotFilename;
                    
                    syncFiles.Add(perforcePath);
                    revertFiles.Add(perforcePath);

                    IProject project = null;
                    IBranch branch = null;
                    string normalisedInput = SIO.Path.GetFullPath(filemapping.LocalFilename);
                    GetConfigurationFromFile(normalisedInput, out project, out branch);
                    if (project == null || branch == null)
                    {
                        log.ErrorCtx(LOG_CTX, "Can't find a project or branch which matches this file {0}", normalisedInput);
                        continue;
                    }
                    String path = branch.Environment.Subst(Path.Combine("$(assets)", "cuts", Path.GetFileNameWithoutExtension(normalisedInput), "..."));
                    
                    syncFiles.Add(path);
                }

                if (!this.SyncFiles(log, p4, syncFiles))
                {
                    log.ErrorCtx(LOG_CTX, "Failed to sync 1 or more source files: ");
                    foreach (String syncFile in syncFiles)
                        log.ErrorCtx(LOG_CTX, "\t{0}", syncFile);

                    return jobResult;
                }
                
                // Edit the upload files

                try
                {
                    log.ProfileCtx(LOG_CTX, "Cutscene Script Processing Starting.");
                    job.ProcessedAt = DateTime.UtcNow;
                    string strMotionBuilderExe = String.Empty;
                    if (!VerifyAutodeskMotionbuilderInstall(log, out strMotionBuilderExe))
                    {
                        log.Error("Job failed: VerifyAutodeskMotionbuilderInstall check failed.");
                        return null;
                    }

                    foreach (string input in inputs)
                    {
                        IProject project = null;
                        IBranch branch = null;
                        string normalisedInput = SIO.Path.GetFullPath(input);

                        GetConfigurationFromFile(normalisedInput, out project, out branch);

                        if (project == null || branch == null)
                        {
                            log.ErrorCtx(LOG_CTX, "Can't find a project or branch which matches this file {0}", normalisedInput);
                            continue;
                        }

                        // Now we have the project and branch lets make sure this file belongs in this config.
                        String scenesFolder = new DirectoryInfo(Path.Combine(branch.Environment.Subst(@"$(art)"), "animation", "cutscene", "!!scenes")).FullName;
                        if (!normalisedInput.ToLower().StartsWith(scenesFolder.ToLower()))
                        {
                            log.ErrorCtx(LOG_CTX, "File isn't in current default branch {0}.\nShould be under {1}", input, scenesFolder);
                            continue;
                        }

                        // Get changelist info for input file
                        P4API.P4RecordSet recordSet = this.P4Run(log, p4, "changes", normalisedInput);
                        P4API.FieldDictionary recordDict = recordSet.Records[0].Fields;
                        string changelist = (recordDict.ContainsKey("change")) ? recordDict["change"] : String.Empty;
                        int fileRevision = recordSet.Records.Count();

                        log.MessageCtx(LOG_CTX, "Processing file '{0}'", normalisedInput);

                        // Find range token if provided
                        String rangeStart = String.Empty;
                        String rangeEnd = String.Empty;
                        
                        String alteredPythonScript = null;
                        IEnumerable<String> rangeTokens = this.FindBuildTokens(log, job, BuildToken.RANGE);
                        if (rangeTokens != null && rangeTokens.Any())
                        {
                            String range = rangeTokens.FirstOrDefault();
                            String[] rangeArray = range.Split('-');
                            if (rangeArray.Length == 2)
                            {
                                rangeStart = rangeArray[0];
                                rangeEnd = rangeArray[1];
                                alteredPythonScript = SetPythonFileRanges(log, this.MotionbuilderPythonScript, rangeStart, rangeEnd);
                            }
                        }
                        bool lightingPass = false;
                        IEnumerable<String> lightTokens = this.FindBuildTokens(log, job, BuildToken.LIGHTING);
                        if (lightTokens != null && lightTokens.Any())
                        {
                            lightingPass =  (lightTokens.FirstOrDefault().ToLower() == "true");
                            alteredPythonScript = (lightingPass) ? this.LightingPythonScript : alteredPythonScript;
                            if (lightingPass && rangeTokens != null && rangeTokens.Any())
                            {
                                log.WarningCtx(LOG_CTX, "Cannot specify a range AND a lighting pass, ignoring range");
                                rangeTokens = null;
                            }
                        }

                        // Find token to see if we run 
                        bool bakeScript = false;
                        IEnumerable<String> bakeTokens = this.FindBuildTokens(log, job, BuildToken.BAKECAMERA);
                        if (bakeTokens != null && bakeTokens.Any())
                        {
                            bakeScript = (bakeTokens.FirstOrDefault().ToLower() == "true");
                        }

                        string strTempPythonFilename = GeneratePythonFile(log, input, (String.IsNullOrEmpty(alteredPythonScript))?this.MotionbuilderPythonScript:alteredPythonScript, 
                                                                         (bakeScript)?this.CameraPlotScript:null);

                        // Ensure Cut folder is in a good state before export
                        String path = branch.Environment.Subst(Path.Combine("$(assets)", "cuts", Path.GetFileNameWithoutExtension(normalisedInput)));

                        if (!System.IO.Directory.Exists(path))
                        {
                            log.ErrorCtx(LOG_CTX, "Cuts Folder doesn't exist {0}", path);
                            continue;
                        }

                        // Make files in cuts folders writeable
                        foreach (string file in System.IO.Directory.GetFiles(path, "*.*", SearchOption.AllDirectories))
                        {
                            FileInfo fileinf = new FileInfo(file);
                            fileinf.IsReadOnly = false;
                            if (!lightingPass && Path.GetExtension(file) == ".lightxml")
                                fileinf.Delete();
                        }

                        //Ignore the exit code from the MotionBuilder process.  Instead rely on a success signal in the Universal Log.
                        int PID = 0;
                        ExportAndBuild(log, strMotionBuilderExe, strTempPythonFilename, out PID);

                        result = MergeMobuLogs(log, job, PID);

                        IEnumerable<String> exportedFiles = SIO.Directory.GetFiles(path, "*", SearchOption.AllDirectories);

                        if (result && !log.HasErrors)
                        {

                            bool isDev = false;
                            String targetContext = String.Format("{0}.{1}.{2}.{3}", project.Name, branch.Name, System.IO.Path.GetFileNameWithoutExtension(path), job.ID);

                            if (isDev)
                            {
                                targetContext = String.Format("dev.{0}", targetContext);
                            }

                            String tempZipFilename = ZipDirectory(log, System.IO.Path.GetFileNameWithoutExtension(path), path);
                            log.MessageCtx(LOG_CTX, "Zipped file with name {0}", tempZipFilename);

                            log.MessageCtx(LOG_CTX, "Input {0}", normalisedInput);
                            log.MessageCtx(LOG_CTX, "Scenes Folder {0}", scenesFolder);
                            log.ProfileEnd();


                            string animationZip = ZipDirectory(log, System.IO.Path.GetFileNameWithoutExtension(path), path);
                            log.MessageCtx(LOG_CTX, "Zipped up file {0}", animationZip);


                            String cutFile = System.IO.Path.Combine(path, "data.cutxml");
                            XDocument cutFileDoc = XDocument.Load(cutFile);
                            XElement offsetElement = cutFileDoc.Root.Element("vOffset");

                            String position = String.Empty;
                            if (offsetElement != null)
                            {
                                position = String.Format("{0},{1},{2}", offsetElement.Attribute("x").Value, offsetElement.Attribute("y").Value, offsetElement.Attribute("z").Value);
                            }

                            String resolution = String.Empty;
                            String capturerange = String.Empty;
                            IEnumerable<String> resolutionTokens = this.FindBuildTokens(log, job, BuildToken.RESOLUTION);
                            if (resolutionTokens != null && resolutionTokens.Any())
                            {
                                resolution = resolutionTokens.FirstOrDefault();
                            }
                            if (rangeTokens != null && rangeTokens.Any())
                            {
                                capturerange = rangeTokens.FirstOrDefault();
                            }

                            // Build a normalised version of the scenes folder.
                            String parent = normalisedInput.ToLower().Replace(scenesFolder.ToLower(), String.Empty).Split(new char[] { System.IO.Path.DirectorySeparatorChar }, StringSplitOptions.RemoveEmptyEntries).First();
                            log.MessageCtx(LOG_CTX, "Parent Folder : {0}", parent);
                            String publish = branch.Environment.Subst(this.PublisDir);
                            String description = String.Format("#{0}={1}", BuildToken.PARENT.ToString(), parent);
                            description += String.Format("{0} #{1}={2}", Environment.NewLine, BuildToken.ANIMATION.ToString(), System.IO.Path.GetFileNameWithoutExtension(path));
                            description += String.Format("{0} #{1}={2}", Environment.NewLine, BuildToken.POSITION.ToString(), position);
                            if (resolution != String.Empty)
                                description += String.Format("{0} #{1}={2}", Environment.NewLine, BuildToken.RESOLUTION.ToString(), resolution);
                            description += String.Format("{0} #{1}={2}", Environment.NewLine, BuildToken.SOURCEFILE.ToString(), normalisedInput);
                            if (changelist != String.Empty)
                                description += String.Format("{0} #{1}={2}", Environment.NewLine, BuildToken.SOURCECHANGELIST.ToString(), changelist);
                            if (fileRevision != 0)
                                description += String.Format("{0} #{1}={2}", Environment.NewLine, BuildToken.SOURCEREVISION.ToString(), fileRevision.ToString());
                            if (capturerange != String.Empty)
                                description += String.Format("{0} #{1}={2}", Environment.NewLine, BuildToken.RANGE.ToString(), capturerange);

                            int submittedCL = 0;
                            Publish(log, job, SIO.Directory.GetParent(path).ToString(), publish, /*exportedFiles*/ new List<String>() { animationZip }, p4, "CutsceneContext", description, out submittedCL, isDev, false, false);
                            jobResult.SubmittedChangelistNumber = submittedCL;
                        }
                        else
                        {
                            log.ErrorCtx(LOG_CTX, "Motionbuilder Export Failed - Not submitting changes");
                        }
                        RevertFiles(log, p4, exportedFiles);
                    } 

                }
                catch (Exception ex)
                {
                    log.ToolException(ex, "An unhandled exception occured during export: {0}", ex.Message);
                    result = false;
                }
                finally
                {
                    // delete fbx to save space
                    UnloadFiles(log, p4, revertFiles);
                    // Flush this jobs log to disk and merge with other logs for upload to our server host.
                    logFile.Flush();
                    LogFactory.FlushApplicationLog();
                    LogFactory.CloseUniversalLogFile(logFile);
                    LogFactory.CloseUniversalLog(log);

                    String mergedLogFilename = CopyMergeAndCleanLogFiles(log, job);

                    if (!String.IsNullOrEmpty(mergedLogFilename))
                    {
                        result &= !UniversalLogFile.ContainsErrors(mergedLogFilename);
                        this.FileTransferServiceConsumer.UploadFile(job, mergedLogFilename);
                    }

                    jobResult.Succeeded = result;
                }
            }

            jobResult.Succeeded = result;
            return jobResult;
        }

        /// <summary>
        ///   Merge all the cutscene logs
        ///   Logs from X:\gta5\tools\logs\cutscene\process\...
        /// </summary>
        /// <param name="log"></param>
        /// <param name="job"></param>
        /// <returns></returns>
        ///
        protected bool MergeMobuLogs(IUniversalLog log, IJob job, int PID)
        {
            String destinationDir = this.FileTransferServiceConsumer.GetClientFileDirectory(job);
            if (!System.IO.Directory.Exists(destinationDir))
                System.IO.Directory.CreateDirectory(destinationDir);

            System.IO.Directory.CreateDirectory(Environment.GetEnvironmentVariable("RS_TOOLSROOT") + @"\logs\cutscene\process");

            string[] uLogFiles = System.IO.Directory.GetFiles(Environment.GetEnvironmentVariable("RS_TOOLSROOT") + @"\logs\cutscene\process", "*.ulog", SearchOption.TopDirectoryOnly);

            if (PID != 0)
            {
                string dir = Environment.GetEnvironmentVariable("RS_TOOLSROOT") + @"\logs\motionbuilder." + PID.ToString();
                if (System.IO.Directory.Exists(dir))
                {
                    string[] uLogMobuFiles = System.IO.Directory.GetFiles(dir, "*.ulog", SearchOption.TopDirectoryOnly);
                    uLogFiles = uLogFiles.Concat(uLogMobuFiles).ToArray();
                }
            }

            String destinationFilename = Path.Combine(destinationDir, "mobu.ulog");
            UniversalLogFile.MergeLogs(destinationFilename, uLogFiles);

            bool result = false;
            if (UniversalLogFile.ContainsErrors(destinationFilename))
            {
                log.Error("Job failed: Merged log contains errors - {0}", destinationFilename);
            }
            else
            {
                //Search for a specific message in the log to denote successful exporting.
                const string exportSuccess = "Exporting cutscene has succeeded.";
                IEnumerable<Base.Collections.Pair<String, String>> messages = UniversalLogFile.GetMessages(destinationFilename);
                foreach (Base.Collections.Pair<String, String> message in messages)
                {
                    if (message.Second.CompareTo(exportSuccess) == 0)
                    {
                        result = true;
                        break;
                    }
                }
                if (result == false)
                {
                    log.Error("Job failed: Merged log contained no errors but script failed to successfully export - check fbx is setup correctly.");
                }
            }

            return result;
        }

        private string SetPythonFileRanges(IUniversalLog log, string strScript, string rangeStart, string rangeEnd)
        {
            string strTempFilename = Path.Combine(Path.GetTempPath(),"AlteredPythonScript.py");

            try
            {
                string alteredScript = System.IO.File.ReadAllText(strScript);
                alteredScript = alteredScript.Replace("SetCutsceneShotStartRange_Py(0, 0)", String.Format("SetCutsceneShotStartRange_Py(0, {0})", rangeStart));
                alteredScript = alteredScript.Replace("SetCutsceneShotEndRange_Py(0, end_range)", String.Format("SetCutsceneShotEndRange_Py(0, {0})", rangeEnd));
                System.IO.File.WriteAllText(strTempFilename, alteredScript);
            }
            catch (Exception e)
            {
                log.Error("Job failed: Exception while writing altered python file - {0}", e.Message);
                return null;
            }

            return strTempFilename;
        }

        private string GeneratePythonFile(IUniversalLog log, string strFBX, string strScript, string additionalScript = null)
        {
            string strTempFilename = Path.GetTempFileName();
            SIO.File.Delete(strTempFilename); // Delete the temp file created by GetTempFileName, we just want the filename
            strTempFilename = Path.ChangeExtension(strTempFilename, "py"); // Motionbuilder only accepts py extension

            try
            {
                using (TextWriter tw = new StreamWriter(strTempFilename))
                {
                    tw.WriteLine("from ctypes import *");
                    tw.WriteLine("from pyfbsdk import *");
                    tw.WriteLine("from pyfbsdk_additions import *");
                    tw.WriteLine("import os");
                    tw.WriteLine("");
                    tw.WriteLine("def dummyCallback( source, event ):");
                    tw.WriteLine("\tpass");
                    tw.WriteLine("");
                    tw.WriteLine("FBSystem().OnConnectionNotify.Add( dummyCallback )");
                    tw.WriteLine("FBApplication().OnFileExit.Remove( dummyCallback )");
                    tw.WriteLine("");
                    tw.WriteLine("");
                    tw.WriteLine("FBApplication().FileOpen( \"" + strFBX.Replace("\\", "/") + "\" )");
                    if (additionalScript!=null)
                        tw.WriteLine("FBApplication().ExecuteScript( \"" + additionalScript.Replace("\\", "/") + "\" )");
                    tw.WriteLine("FBApplication().ExecuteScript( \"" + strScript.Replace("\\", "/") + "\" )");
                    tw.WriteLine("FBApplication().FileExit()");
                    tw.Close();
                }
            }
            catch (Exception e)
            {
                log.Error("Job failed: Failed to generate python file - {0}", e.Message);
                return null;
            }

            return strTempFilename;
        }

         /// <summary>
        ///   Get the project and branch for supplied filename
        /// </summary>
        /// <param name="normalisedFilename"></param>
        /// <param name="project"></param>
        /// <param name="branch"></param>
        ///
        private void GetConfigurationFromFile(string filename, out IProject project, out IBranch branch)
        {
            project = null;
            branch = null;
            foreach (KeyValuePair<string, IProject> proj in this.Options.Config.AllProjects())
            {
                if (proj.Value.Branches.ContainsKey(Options.Branch.Name) && filename.StartsWith(proj.Value.Branches[Options.Branch.Name].Art, StringComparison.InvariantCultureIgnoreCase))
                {
                    project = proj.Value;
                    branch = project.Branches[Options.Branch.Name];
                    break;
                }
            }
        }

        /// <summary>
        ///   Execute an export/build from motionbuilder using the python script
        /// </summary>
        /// <param name="log"></param>
        /// <param name="strMotionBuilderExe"></param>
        /// <param name="strPythonFilename"></param>
        ///
        private bool ExportAndBuild(IUniversalLog log, string strMotionBuilderExe, string strPythonFilename, out int PID)
        {
            PID = 0;
            ProcessStartInfo info = new ProcessStartInfo();
            info.UseShellExecute = false;
            info.FileName = strMotionBuilderExe;
            info.Arguments = strPythonFilename + " -suspendMessages";

            //1800000 == 30mins
            //2700000 == 45mins
            // Run a timer, this will kill motionbuilder if it has crashed, currently set to 45minutes
            System.Threading.Timer tmr = new System.Threading.Timer(new TimerCallback(TimeItOut), log, 0, 2700000);

            Thread.Sleep(5000); // 5 seconds

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo = info;
            if ((null == process) || (!process.Start()))
            {
                log.ErrorCtx(LOG_CTX, "Failed to invoke Autodesk Motionbuilder for cutscene export.  Aborting job.");
                log.ErrorCtx(LOG_CTX, "Command: '{0}'.", process.StartInfo.FileName);
                log.ErrorCtx(LOG_CTX, "Arguments: '{0}'", String.Join(" ", process.StartInfo.Arguments));
                return (false);
            }
            PID = process.Id;
            log.MessageCtx(LOG_CTX, "Autodesk Motionbuilder launched;");
            log.MessageCtx(LOG_CTX, "Command: {0}.", process.StartInfo.FileName);
            log.MessageCtx(LOG_CTX, "Arguments: {0}.", String.Join(" ", process.StartInfo.Arguments));
            process.WaitForExit();
            tmr.Dispose();

            log.MessageCtx(LOG_CTX, "Process Exit Code: {0}.", process.ExitCode);

            if (0 != process.ExitCode) // Since we are nuking MB if it crashes this means we cant return an error on the exit code, just warn
            {
                log.WarningCtx(LOG_CTX, "Autodesk Motionbuilder crashed on exit.");
                log.WarningCtx(LOG_CTX, "Command: '{0}'.", process.StartInfo.FileName);
                log.WarningCtx(LOG_CTX, "Arguments: '{0}'", String.Join(" ", process.StartInfo.Arguments));
                return (false);
            }
            return (true);
        }

        public static void TimeItOut(object o)
        {
            Debug.Assert(o is IUniversalLog, String.Format("State object expected as universal log type."));

            IUniversalLog log = (IUniversalLog)o;

            System.Diagnostics.Process[] proc = System.Diagnostics.Process.GetProcessesByName("motionbuilder");
            if (proc.Length > 0)
            {
                if (log != null)
                {
                    log.ErrorCtx(LOG_CTX, "Autodesk Motionbuilder has being killed due to timeout.  Aborting job.");
                }

                proc[0].Kill();
            }

            proc = System.Diagnostics.Process.GetProcessesByName("senddmp");
            if (proc.Length > 0)
                proc[0].Kill();
        }

        /// <summary>
        ///   Verify if motionbuilder is installed. This does a check in the registry for motionbuilder 2012 x64
        /// </summary>
        /// <param name="log"></param>
        /// <param name="exeFilename"></param>
        ///
        private bool VerifyAutodeskMotionbuilderInstall(IUniversalLog log, out String exeFilename)
        {
            RegistryKey hklm64 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
            RegistryKey key = hklm64.OpenSubKey(@"SOFTWARE\Autodesk\MotionBuilder\2014");
            if (key != null)
            {
                string InstallPath = (string)key.GetValue("InstallPath", null);
                exeFilename = InstallPath + "bin\\x64\\motionbuilder.exe";

                if (SIO.File.Exists(exeFilename))
                    return (true);
            }

            log.ErrorCtx(LOG_CTX, "Motionbuilder executable not found.  Aborting job.");
            exeFilename = String.Empty;
            return (false);
        }

        private void RevertFiles(IUniversalLog log, P4 p4, IEnumerable<String> files)
        {
            this.P4SilentRun(log, p4, "revert", files.ToArray());
        }

        private void UnloadFiles(IUniversalLog log, P4 p4, IEnumerable<String> files)
        {
            String[] fileArray = files.ToArray();
            for (int i = 0; i < fileArray.Count(); ++i)
            {
                fileArray[i] = String.Format("{0}#none", fileArray[i]);
            }
            this.P4SilentRun(log, p4, "sync", fileArray);
        }

        #endregion // Controller Methods

        #region Private Methods

        /// <summary>
        /// Validates the batched files
        /// - There should be only one batch.
        /// </summary>
        /// <param name="changeListFilesBatched"></param>
        /// <param name="fileMappings">the files the batches were created from</param>
        /// /// <returns></returns>
        private bool ValidateBatchedFiles(IUniversalLog log, P4 p4, IDictionary<ProjectBatchKey, IEnumerable<FileMapping>> filesBatched, IEnumerable<FileMapping> fileMappings)
        {
            bool validated = true;
            if (filesBatched.Count > 1)
            {
                log.ErrorCtx(LOG_CTX, "Error Assetbuilder job processor cannot process files across branches & projects");
                validated = false;
            }
            else if (filesBatched.Count == 0)
            {
                log.ErrorCtx(LOG_CTX, "Error Assetbuilder job processor was not able to determine which branch ALL files belong to ( files for creating batch now follow )");
                int i = 0;
                int numFileMappings = fileMappings.Count();
                foreach (FileMapping filemapping in fileMappings)
                {
                    log.ErrorCtx(LOG_CTX, "({0}/{1}) File could not be batched: Mapped?: '{2}' Depot: '{3}' Client: '{4}' Local: '{5}'", i, numFileMappings, filemapping.Mapped, filemapping.DepotFilename, filemapping.ClientFilename, filemapping.LocalFilename);
                    i++;
                }
                validated = false;
            }
            return validated;
        }

        /// <summary>
        /// Sync files wrapper.Wrapper for syncing files</summary>
        /// <param name="files"></param>
        /// <returns></returns>
        private bool SyncFiles(IUniversalLog log, P4 p4, IEnumerable<String> files)
        {

            if (files.Count() != 0)
            {
                P4API.P4RecordSet ret = this.P4SilentRun(log, p4, "sync", files.ToArray());
                return (ret != null && !ret.HasErrors());
            }
            else
            {
                //Log.WarningCtx(LOG_CTX, "Attempted to sync an empty list of files for job {0}", CurrentAssetBuilderJob.ID);
                return false;
            }
        }

        /// <summary>
        /// P4.Run wrapper with exception handler and minimal logging.
        /// - was created to silence warnings from 'opened' command.
        /// - it only reports on errors.
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        private P4API.P4RecordSet P4SilentRun(IUniversalLog log, P4 p4, String operation, params String[] args)
        {
            P4API.P4RecordSet ret = null;

            try
            {
                ret = p4.Run(operation, args);

                // Log errors
                foreach (String error in ret.Errors)
                {
                    log.ErrorCtx(LOG_CTX, "{0}", error);
                }
            }
            catch (Exception ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Exception running {0}: {1}", operation, ex.Message);
                return null;
            }
            return ret;
        }

        /// <summary>
        /// P4.Run wrapper with exception handler and additional logging.
        /// </summary>
        /// <param name="files"></param>
        /// <returns></returns>
        private P4API.P4RecordSet P4Run(IUniversalLog log, P4 p4, String operation, params String[] args)
        {
            P4API.P4RecordSet ret = null;

            try
            {
                ret = p4.Run(operation, args);
                // Log messages
                foreach (String message in ret.Messages)
                {
                    log.MessageCtx(LOG_CTX, "{0}", message);
                }

                // Log warnings
                foreach (String warning in ret.Warnings)
                {
                    log.WarningCtx(LOG_CTX, "{0}", warning);
                }

                // Log errors
                foreach (String error in ret.Errors)
                {
                    log.ErrorCtx(LOG_CTX, "{0}", error);
                }
            }
            catch (Exception ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Exception running {0}: {1}", operation, ex.Message);
                return null;
            }
            return ret;
        }

        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.ClientWorker.Jobs namespace
