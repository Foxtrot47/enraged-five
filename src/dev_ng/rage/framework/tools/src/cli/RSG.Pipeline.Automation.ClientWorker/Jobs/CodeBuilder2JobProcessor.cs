﻿using System;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Xml.Linq;
using System.Xml;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Engine;
using RSG.Pipeline.Content;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Build;
using RSG.Pipeline.Services;
using P4API;
using System.Text.RegularExpressions;
using System.IO;
using Ionic.Zip;
using System.Data.SqlClient;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{
    /// <summary>
    /// Enum signifying the stage of processing for a codebuild
    /// </summary>
    internal enum BuildStage : int 
    { 
        Invalid = -1, 
        PreBuild,       // Prebuild commands.
        MainBuild,      // Actually compiling and linking.
        Publish,        // Upload, Copy and Submit.
        PostBuild,      // Postbuild commands.
        Finalise,       // Final fluff
        NumStages       
    };

    /// <summary>
    /// Code Builder 2 - job processor.
    /// </summary>
    internal sealed class CodeBuilder2JobProcessor :
        JobProcessorBase,
        IJobProcessor
    {
        #region Constants
        private const String LOG_CTX = "Code Builder 2 Job Processor";

        /// <summary>
        /// Paremater that indicates this is not a live codebuilder : some operations will not be performed.
        /// </summary>
        private const String PARAM_DEV = "dev";

        /// <summary>
        /// Params specifying the executable tool to use for builds.
        /// </summary>
        private const String PARAM_INCREDIBUILD = "incredibuild";
        private const String PARAM_INCREDIBUILD_NEW = "incredibuild_new";
        private const String PARAM_VSI2008 = "vsi2008";
        private const String PARAM_VSI2010 = "vsi2010";
        private const String PARAM_VSI2011 = "vsi2011";
        private const String PARAM_VS2008 = "vs2008";
        private const String PARAM_VS2010 = "vs2010";
        private const String PARAM_VS2012 = "vs2012";
        private const String PARAM_VS2015 = "vs2015";
        private const String PARAM_VS2017 = "vs2017";
        private const String PARAM_VS2019 = "vs2019";
        private const String PARAM_BUILD_ENVIRONMENT = "BuildEnvironment";

        /// <summary>
        /// Params for the regex matching on build output
        /// </summary>
        private const String PARAM_REGEX_ERROR = "error regex";
        private const String PARAM_REGEX_WARNING = "warning regex";

        /// <summary>
        /// Param for a regex that needs to be matched and acts a filter for taarget files.
        /// this has an effect on publishing, and uploading
        /// </summary>
        private const String PARAM_REGEX_TARGET = "target filter regex";

        /// <summary>
        /// Enviroment variables names exposed to build processes at prebuild
        /// </summary>
        private const String EnvironmentSrcCL = "RSG_AUTOMATION_CODEBUILDER_SRC_CL";
        private const String EnvironmentContext = "RSG_AUTOMATION_CODEBUILDER_CONTEXT";
       
        /// <summary>
        /// Further enviroment variables names exposed to build processes at postbuild
        /// </summary>        
        private const String EnvironmentNumErrors = "RSG_AUTOMATION_CODEBUILDER_NUM_ERRORS";
        private const String EnvironmentNumWarnings = "RSG_AUTOMATION_CODEBUILDER_NUM_WARNINGS";
        private const String EnvironmentBuildSuceeded = "RSG_AUTOMATION_CODEBUILDER_BUILD_SUCCEEDED";

        /// <summary>
        /// CSV of the files published ( local paths ) 
        /// </summary>
        private const String EnvironmentPublishedFiles = "RSG_AUTOMATION_CODEBUILDER_FILES_PUBLISHED";

        /// <summary>
        /// Find the DB for shelveQA
        /// </summary>
        private const String PARAM_SHELVE_QA_DB_SERVER = "Shelve QA DB Server";
        private const String PARAM_SHELVE_QA_DB = "Shelve QA Database";
        private const String PARAM_SHELVE_QA_DB_MAIN_TABLE = "Shelve QA DB Main Table";

        #endregion // Constants

        #region Properties

        /// <summary>
        /// A context string that shows the build we are building
        /// - we splatter this everywhere so it's possible to track the many flavours of builds easily.
        /// - this is because builds can rapidly flood with errors and warnings.
        /// </summary>
        private string BuildContext { get; set; }

        /// <summary>
        /// Environment to set during build processing.
        /// </summary>
        private IDictionary<String, String> BuildEnvironment { get; set; }

        /// <summary>
        /// The stage of processing within the job processing - see enum.
        /// </summary>
        private BuildStage BuildStage { get; set; }

        /// <summary>
        /// Num warnings parsed ( prebuild )
        /// </summary>
        private int[] NumWarnings { get; set; }

        /// <summary>
        /// Num errors parsed ( postbuild ) 
        /// </summary>
        private int[] NumErrors { get; set; }

        /// <summary>
        /// Build output error regex.
        /// - keep these efficient
        /// </summary>
        private Regex ErrorRegex { get; set; }

        /// <summary>
        /// Build output warning regex.
        /// - keep these efficient
        /// </summary>
        private Regex WarningRegex { get; set; }

        /// <summary>
        /// Makes a common cast of the BuildStage
        /// </summary>
        /// <returns></returns>
        private int CurrentBuildStage 
        { 
            get { return (int)BuildStage; }
        }

        #endregion //  Properties

        #region Delegates
        private delegate void LogDelegate(String context, String message, params Object[] args);
        #endregion // Delegates

        #region Constructors
        /// <summary>
        /// Parameterized Constructor
        /// </summary>
        /// <param name="options"></param>
        /// <param name="uploadConsumer"></param>
        public CodeBuilder2JobProcessor(CommandOptions options, FileTransferServiceConsumer uploadConsumer)
            : base(options, CapabilityType.CodeBuilder2, uploadConsumer)
        {
            ResetErrorsAndWarnings();
            BuildStage = BuildStage.Invalid;

            //Log = LogFactory.CreateUniversalLog("codebuilder2");

            if (this.Parameters.ContainsKey(PARAM_REGEX_ERROR))
            {
                String regex = this.Parameters[PARAM_REGEX_ERROR].ToString();
                ErrorRegex = new Regex(regex, RegexOptions.IgnoreCase);
            }
            else
            {
                //Log.Error("Missing parameter {0}", PARAM_REGEX_ERROR);
            }

            if (this.Parameters.ContainsKey(PARAM_REGEX_WARNING))
            {
                String regex = this.Parameters[PARAM_REGEX_WARNING].ToString();
                WarningRegex = new Regex(regex, RegexOptions.IgnoreCase);
            }
            else
            {
                //Log.Error("Missing parameter {0}", PARAM_REGEX_WARNING);
            }

            if (this.Parameters.ContainsKey(PARAM_BUILD_ENVIRONMENT))
            {
                InitialiseBuildEnvironment(null);
            }
        }

        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// Process requested job.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public override IJobResult Process(IJob job)
        {
            BuildStage = BuildStage.Invalid;

            ResetErrorsAndWarnings();
            JobResult jobResult = new JobResult(job.ID, false);            

            Debug.Assert(job is CodeBuilder2Job, "Invalid job for processor.");
            if (!(job is CodeBuilder2Job))
                return null;

            CodeBuilder2Job currentJob = (CodeBuilder2Job)job;

            Init(job);

            IUniversalLog log = LogFactory.CreateUniversalLog("codebuilder_process");
            ILogFileTarget logFile = LogFactory.CreateUniversalLogFile(
                String.Format("process_{0}", currentJob.ID), log) as ILogFileTarget;

            String targetContext = String.Format("{0}.{1}.{2}.{3}.{4}", System.IO.Path.GetFileNameWithoutExtension(currentJob.SolutionFilename), currentJob.Platform, currentJob.BuildConfig, currentJob.Tool, currentJob.Rebuild ? "reb" : "inc");
            log.MessageCtx(LOG_CTX, "*** LOG HAS BEEN CREATED : {0} : {1}", logFile.Filename, targetContext);

            if (!System.IO.File.Exists(currentJob.SolutionFilename))
            {
                log.ErrorCtx(LOG_CTX, "Unable to find solution {0}.", currentJob.SolutionFilename);
                return jobResult;
            }

            // Ensure that the context is set to <context>.dev so that we never interfere with the live builds in development.
            // - the context is used to set the p4 directory for publishing.
            bool isDev = this.Parameters.ContainsKey(PARAM_DEV);      
            if (isDev)
            {
                targetContext = String.Format("dev.{0}", targetContext);
            }

            uint clNumMax = 0;
            if (!(job.Trigger is IUserRequestTrigger))
            {
                IEnumerable<uint> clNums = job.Trigger.ChangelistNumbers();
                clNumMax = clNums.Max();
                if (clNums.HasAtLeast(2))
                {
                    BuildContext = String.Format("{0}.CL{1}-{2}", targetContext, clNums.Min(), clNumMax);
                }
                else if (clNums.Any()) // has one CL
                {
                    BuildContext = String.Format("{0}.CL{1}", targetContext, clNumMax);
                }
            }
            else
            {
                //[TODO] Figure out something better for this
                //The only indication of this being a Label'ed build is the single prebuild command
                BuildContext = String.Format("UserRequest_{0}_{1}", ((IUserRequestTrigger)job.Trigger).Username, ((IUserRequestTrigger)job.Trigger).Command);
            }

            TestProcessBuildToken(job, jobResult, log);

            // *******************************************
            // *** CODEBUILDER CONTROL LOGIC FOLLOWS *****
            // *******************************************

            // Periodically flush the log so it gets saved out
            LogFactory.FlushApplicationLog();
            bool[] stageSucceeded = new bool[(int)BuildStage.NumStages];

            // Clear the build environment each build.
            InitialiseBuildEnvironment(log);

            using (P4 p4 = this.Options.Project.SCMConnect())
            {
                BuildStage = BuildStage.PreBuild;

                // 1. Sync to the CL triggering this job.
                SyncToCL(log, p4, currentJob, isDev);

                SyncToShelveQACLs(log, p4, currentJob);

                // Set RSG_ environment variables that prebuild processes can utilise
                SetPrebuildEnvironment(log, clNumMax, currentJob.Trigger);

                // *********************
                // 2. Run prebuild steps
                // *********************
                int origNumErrors = NumErrors[CurrentBuildStage];
                stageSucceeded[CurrentBuildStage] = Prebuild(log, currentJob, BuildEnvironment, ReadStandardOutput, ReadStandardError);
                ShowStageSuccess(log, stageSucceeded);
                int newNumErrors = NumErrors[CurrentBuildStage];
                if (newNumErrors > origNumErrors && stageSucceeded[CurrentBuildStage])
                {
                    log.WarningCtx(LOG_CTX, "Prebuild process(es) returned true giving a notion of success. However, {0} errors were encountered, one of the prebuild process(es) should be reviewed.", newNumErrors - origNumErrors);
                }

                // Periodically flush the log so it gets saved out
                LogFactory.FlushApplicationLog();
                // **********
                // Main build
                // **********
                if (stageSucceeded[CurrentBuildStage])
                {
                    BuildStage = BuildStage.MainBuild;

                    // 3. Edit the upload files
                    EnableWriteInArtefactDirectories(log, p4, currentJob.TargetDir);

                    IEnumerable<FileInfo> fileInfosStart = null;

                    // 4. Take a note of all the file timestamps in the targetDir before the build starts
                    if (!String.IsNullOrEmpty(currentJob.TargetDir))
                    {
                        DirectoryInfo dirInfoStart = new DirectoryInfo(currentJob.TargetDir);
                        fileInfosStart = dirInfoStart.GetFiles();
                    }

                    // 5. Actually build the code.  
                    stageSucceeded[CurrentBuildStage] = BuildCode(log, p4, currentJob);
                    ShowStageSuccess(log, stageSucceeded);

                    // Set RSG_ environment variables that postbuild processes can utilise
                    SetPostBuildEnvironment(log, stageSucceeded[CurrentBuildStage]);

                    // Periodically flush the log so it gets saved out
                    LogFactory.FlushApplicationLog();

                    if (stageSucceeded[CurrentBuildStage])
                    {
                        BuildStage = BuildStage.Publish;

                        String regex = this.Parameters.ContainsKey(PARAM_REGEX_TARGET) ? this.Parameters[PARAM_REGEX_TARGET].ToString() : String.Empty;
                        
                        // 6. Gets the files that were modified in the upload directory.
                        IEnumerable<String> filesModified = FilesModified(log, currentJob.TargetDir, regex, fileInfosStart);
                        
                        // ********************************************************
                        // 7. upload artefacts.
                        // ********************************************************
                        Upload(log, currentJob, currentJob.TargetDir, filesModified);

                        // ******************************************
                        // 8. Publish ( copy and optionally checkin )
                        // ******************************************
                        String descriptionContext = String.Format("{0}.{1}", BuildContext, currentJob.ID);
                        int submittedCL = 0;
                        IEnumerable<String> filesPublished = Publish(log, currentJob, currentJob.TargetDir, currentJob.PublishDir, filesModified, p4, targetContext, descriptionContext, out submittedCL, isDev);

                        // Ensure environment knows the filesnames that were published.
                        SetPublishEnvironment(log, filesPublished);

                        stageSucceeded[CurrentBuildStage] = true;

                        // Periodically flush the log so it gets saved out
                        LogFactory.FlushApplicationLog();
                    }
                }
            }

            // **********************
            // 9. Run postbuild steps
            // **********************
            if (stageSucceeded[CurrentBuildStage])
            {
                BuildStage = BuildStage.PostBuild;

                // Run the postbuild commmands
                stageSucceeded[CurrentBuildStage] = Postbuild(log, currentJob, BuildEnvironment, ReadStandardOutput, ReadStandardError);
                ShowStageSuccess(log, stageSucceeded);

                // Periodically flush the log so it gets saved out
                LogFactory.FlushApplicationLog();
            }

            BuildStage = BuildStage.Finalise;

            // ...um nothing to do
            stageSucceeded[CurrentBuildStage] = true;

            // 10. React to errors and warnings.
            HandleErrorsAndWarnings(log, jobResult, stageSucceeded);

            // Flush job log; ensure its closed and ready for uploading.
            logFile.Flush();
            LogFactory.FlushApplicationLog();
            LogFactory.CloseUniversalLogFile(logFile);
            LogFactory.CloseUniversalLog(log);

            // 11. Upload the log file
            UploadLog(job, log, logFile, false);

            return jobResult;
        }

        /// <summary>
        /// An example of how to use build tokens
        /// </summary>
        /// <param name="job"></param>
        /// <param name="jobResult"></param>
        /// <param name="log"></param>
        private void TestProcessBuildToken(IJob job, JobResult jobResult, IUniversalLog log)
        {
            // Find a particular build token and value(s) associated with it.
            BuildToken buildToken = BuildToken.CODEBUILDER_CONTEXT;
            IEnumerable<String> tokenValues = this.FindBuildTokens(log, job, buildToken);
            if (tokenValues != null)
            {
                // Now (as an example ) use the build token to change the log context : a simple and verifiable test that this is working.
                BuildContext = String.Format("{0}.BuildToken", BuildContext);

                foreach (String tokenValue in tokenValues.Where(bt => !String.IsNullOrEmpty(bt)))
                {
                    BuildContext = String.Format("{0}.{1}", BuildContext, tokenValue);

                    // Now register the token as processed in the job results.
                    jobResult.RegisterBuildTokenProcessed(buildToken, tokenValue);
                }
            }
        }

        private void Publish(IUniversalLog log, CodeBuilder2Job currentJob, String publishContext, bool isDev, bool[] stageSucceeded, P4 p4, IEnumerable<String> filesModified)
        {
            log.ProfileCtx(LOG_CTX, "--- Publish ---");

            String descriptionContext = String.Format("{0}.{1}", BuildContext, currentJob.ID);
            IEnumerable<String> filesPublished = null;

            if (String.IsNullOrEmpty(currentJob.PublishDir))
            {
                log.MessageCtx(LOG_CTX, "Publishing stage is not enabled as there is no publish dir: {0}", descriptionContext);
                stageSucceeded[CurrentBuildStage] = true;
            }
            else
            {
                if (String.IsNullOrEmpty(currentJob.TargetDir))
                {
                    // Thsi is indicitive of a badly organised configuration data.
                    log.WarningCtx(LOG_CTX, "Publishing stage is not enabled as there is no target dir: {0}", descriptionContext);
                    stageSucceeded[CurrentBuildStage] = true;
                }
                else
                {
                    // Substitute special string to keep all builds unique
                    String publishDir = currentJob.PublishDir.Replace("$(codebuild_context)", publishContext);
                    String targetDir = currentJob.TargetDir.Replace("$(codebuild_context)", publishContext);
                    int submittedCL = 0;
                    filesPublished = base.Publish(log, currentJob, targetDir, publishDir, filesModified, p4, publishContext, descriptionContext, out submittedCL, isDev);
                    stageSucceeded[CurrentBuildStage] = filesPublished != null ? true : false;
                }
            }

            // Ensure environment knows the filesnames that were published.
            SetPublishEnvironment(log, filesPublished);

            log.ProfileEnd();
        }

        private void ShowStageSuccess(IUniversalLog log, bool[] stageSucceeded)
        {
            if (stageSucceeded[CurrentBuildStage])
                log.MessageCtx(LOG_CTX, "{0} succeeded.", BuildStage);
            else
                log.ToolErrorCtx(LOG_CTX, "{0} failed.", BuildStage);
        }
        #endregion // Controller Methods

        #region Private Methods

        private void ResetErrorsAndWarnings()
        {
            NumWarnings = new int[(int)BuildStage.NumStages];
            NumErrors = new int[(int)BuildStage.NumStages];
        }

        private int TotalErrors()
        {
            int totalErrors = NumErrors.Sum();
            return totalErrors;
        }

        private int TotalWarnings()
        {
            int totalWarnings = NumWarnings.Sum();
            return totalWarnings;
        }

        private void InitialiseBuildEnvironment(IUniversalLog log)
        {
            this.BuildEnvironment = new Dictionary<String, String>();
            IEnumerable<String> envList = ((IEnumerable<Object>)this.Parameters[PARAM_BUILD_ENVIRONMENT]).OfType<String>();
            foreach (string envString in envList)
            {
#warning DW : TODO : make more defensive, use some fancy linq
                IEnumerable<String> kvp = envString.Split('=').Select(e => e.Trim());
                if (kvp.Count() == 2)
                {
                    if (log != null)
                        log.MessageCtx(LOG_CTX, "Read param driven build environment ({0}={1})", kvp.ElementAt(0), kvp.ElementAt(1));
                    this.BuildEnvironment.Add(kvp.ElementAt(0), kvp.ElementAt(1));
                }
                else
                {
                    if (log != null)
                        log.WarningCtx(LOG_CTX, "Unusual build environment ignored : {0}", envString);
                }
            }
        }

        /// <summary>
        /// Set RSG_ environment variables that prebuild processes can utilise
        /// </summary>
        /// <param name="CL"></param>
        private void SetPrebuildEnvironment(IUniversalLog log, uint changelistNumber, ITrigger trigger)
        {
            // Expose inital job details to environment.
            BuildEnvironment.Add(EnvironmentSrcCL, changelistNumber.ToString());
            BuildEnvironment.Add(EnvironmentContext, BuildContext);
            if (trigger is IHasCommand)
            {
                if (!String.IsNullOrWhiteSpace(((IHasCommand)trigger).Command))
                {
                    //Check for build environment settings in the string
                    //EX: SYNC_P4_LABEL=MyLabelNameHere CLEAR_FILES_WITH_EXT=.pdb
                    //This format should be verified at Trigger/Job creation
                    String[] envSettings = ((IHasCommand)trigger).Command.Split(' ');
                    foreach (String envVarKvp in envSettings)
                    {
                        Regex regex = new Regex("(^[^=]+)=(.*)", RegexOptions.IgnoreCase);
                        Match match = regex.Match(envVarKvp);
                        if (match.Success && match.Groups.Count == 3)
                        {
                            String envVarName = match.Groups[1].Value;
                            String envVarValue = match.Groups[2].Value;
                            BuildEnvironment.Add(envVarName, envVarValue);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Set RSG_ environment variables that postbuild processes can utilise
        /// </summary>
        /// <param name="buildSucceeded"></param>
        private void SetPostBuildEnvironment(IUniversalLog log, bool buildSucceeded)
        {
            // Expose processed job details to environment that can be used in post build scripts.
            BuildEnvironment.Add(EnvironmentNumErrors, TotalErrors().ToString());
            BuildEnvironment.Add(EnvironmentNumWarnings, TotalWarnings().ToString());
            BuildEnvironment.Add(EnvironmentBuildSuceeded, buildSucceeded.ToString());
        }

        private void SetPublishEnvironment(IUniversalLog log, IEnumerable<String> filesPublish)
        {
            if (filesPublish != null && filesPublish.Count() > 0)
            {
                // Expose inital job details to environment.
                String publishedFilesAsCSV = String.Join(",", filesPublish);
                BuildEnvironment.Add(EnvironmentPublishedFiles, publishedFilesAsCSV);
            }
            else
            {
                BuildEnvironment.Add(EnvironmentPublishedFiles, "");
            }
        }

        /// <summary>
        /// If we have errors or warning do something and set the jobresult as required.
        /// </summary>
        /// <param name="jobResult"></param>
        /// <returns></returns>
        private bool HandleErrorsAndWarnings(IUniversalLog log, JobResult jobResult, bool[] stageSucceeded)
        {
            // Overall notion of success = all phases of build must have succeeded.
            // there is scope to make this fuzzier if required, 
            // eg. prebuild OK, main build OK, post build fails, but mark build as success, because the post build isn't mission critical.
            bool allSucceeded = stageSucceeded[(int)BuildStage.PreBuild] &&
                                stageSucceeded[(int)BuildStage.MainBuild] &&
                                stageSucceeded[(int)BuildStage.Publish] &&
                                stageSucceeded[(int)BuildStage.PostBuild] &&
                                stageSucceeded[(int)BuildStage.Finalise];

            if (TotalErrors() > 0 || !allSucceeded)
            {
                bool showAsError = true;
                ShowBuildStageResultTable(log, stageSucceeded, showAsError);
                log.ToolErrorCtx(LOG_CTX, "Build Failed: {0} errors : {1} warnings", TotalErrors(), TotalWarnings());
                jobResult.Succeeded = false;
            }
            else if (TotalWarnings() > 0)
            {
                ShowBuildStageResultTable(log, stageSucceeded);
#warning DW: TODO  Add optional param to support 'treat warning as error'.
                log.WarningCtx(LOG_CTX, "Build Succeeded: {0} errors : {1} warnings", TotalErrors(), TotalWarnings());
                jobResult.Succeeded = true;
            }
            else
            {
                ShowBuildStageResultTable(log, stageSucceeded);
                log.MessageCtx(LOG_CTX, "Build Succeeded: {0} errors : {1} warnings", TotalErrors(), TotalWarnings());
                jobResult.Succeeded = true;
            }

            return jobResult.Succeeded;
        }

        /// <summary>
        /// Pretty table/matrix of results.
        /// </summary>
        /// <param name="stageSucceeded"></param>
        private void ShowBuildStageResultTable(IUniversalLog log, bool[] stageSucceeded, bool showAsError = false)
        {
            LogDelegate logDelegate = showAsError ? new LogDelegate(log.ToolErrorCtx) : new LogDelegate(log.MessageCtx);

            logDelegate(LOG_CTX, " ");
            logDelegate(LOG_CTX, "============================================");
            logDelegate(LOG_CTX, "===       BUILD STAGE RESULT TABLE       ===");
            logDelegate(LOG_CTX, "============================================");
            logDelegate(LOG_CTX, "=    STAGE    |  Status      |  Err | Warn =");
            logDelegate(LOG_CTX, "============================================");

            for (int bs = 0; bs < (int)BuildStage.NumStages; bs++)
            {
                logDelegate(LOG_CTX, "= {0} | {1} | {2} | {3} =", (((BuildStage)bs).ToString()).PadLeft(11), stageSucceeded[bs] ? "   Succeeded" : "      Failed", NumErrors[bs].ToString().PadLeft(4), NumWarnings[bs].ToString().PadLeft(4));
            }
            logDelegate(LOG_CTX, "============================================");
            logDelegate(LOG_CTX, " ");
        }

        /// <summary>
        /// Build the code
        /// - kicks off processes
        /// </summary>
        /// <returns></returns>
        private bool BuildCode(IUniversalLog log, P4 p4, CodeBuilder2Job currentJob)
        {
            log.MessageCtx(LOG_CTX, " ");
            log.MessageCtx(LOG_CTX, "--- BuildCode ---");

            // Create build process            
            ProcessStartInfo processStartInfo = CreateProcess(log, currentJob);
            if (processStartInfo == null)
            {
                log.ErrorCtx(LOG_CTX, "Cannot create build tool process");
                return false;
            }

            // Log the start  - make it clear in the logs / console.
            BuildStart(log, processStartInfo);

            log.ProfileCtx(LOG_CTX, "Build Code");
            System.Diagnostics.Process buildProcess = RunProcess(log, processStartInfo, ReadStandardOutput, ReadStandardError);
            log.ProfileEnd();

            // Log the end  - make it clear in the logs / console.
            BuildEnd(log, p4, buildProcess, currentJob);

            if (buildProcess.ExitCode != 0)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Sync to a CL across all the monitoring paths that the job monitored.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="isDev">indicates this is not a builder server - sync may not be desired in development</param>
        private void SyncToCL(IUniversalLog log, P4 p4, CodeBuilder2Job currentJob, bool isDev)
        {
            log.MessageCtx(LOG_CTX, " ");
            log.MessageCtx(LOG_CTX, "--- SyncToCL ---");

            IEnumerable<uint> clNums = currentJob.Trigger.ChangelistNumbers();

            // Sync to CL if required.
            if (clNums.Any())
            {
                uint maxCl = clNums.Max();
                log.MessageCtx(LOG_CTX, "Syncing to CL{0}", maxCl);
                ICollection<String> syncFiles = new List<String>();
                
                // Prevents sync from actually running, yet runs the command.
                if (isDev)
                {
                    log.MessageCtx(LOG_CTX, "syncing with '-n' option, this will not actually sync these files but shows the results if it were to.");
                    syncFiles.Add("-n");
                }

                foreach (String clFile in currentJob.Trigger.MonitoredPaths())
                {
                    String pathtoSync = String.Format("{0}/...@{1}", clFile, maxCl);
                    syncFiles.Add(pathtoSync);
                }

                P4API.P4RecordSet recordSet = p4.Run("sync", syncFiles.ToArray());
                p4.ParseAndLogRecordSetForDepotFilename("sync", recordSet);
                p4.LogP4RecordSetMessages(recordSet);
            }
        }

        /// <summary>
        ///  revert files in CL
        ///  - delete the CL also.
        /// </summary>
        /// <param name="pendingCL"></param>
        private void RevertTargetFiles(IUniversalLog log, P4PendingChangelist pendingCL)
         {
            if (pendingCL == null)
                return;

            log.MessageCtx(LOG_CTX, " ");
            log.MessageCtx(LOG_CTX, "--- RevertTargetFiles ---");

            log.MessageCtx(LOG_CTX, "Reverting CL{0}", pendingCL.Number);
            pendingCL.Revert();

            log.MessageCtx(LOG_CTX, "Deleting CL{0}", pendingCL.Number);
            pendingCL.Delete();
            }
        
        /// <summary>
        /// Determine CL for head.  Sync the build directories to headCL.  Unshelve the ShelveQACL
        /// and resolve.
        /// </summary>
        /// <returns></returns>
        private void SyncToShelveQACLs(IUniversalLog log, P4 p4, CodeBuilder2Job job)
         { 
            log.MessageCtx(LOG_CTX, " ");   
            log.MessageCtx(LOG_CTX, "--- SyncToShelveQACLs ---");

            IUserRequestTrigger shelveTrigger = job.Trigger as IUserRequestTrigger;
            if (shelveTrigger != null)
            {
                String[] envVars = shelveTrigger.Command.Split('.');

                //env vars:
                // 		[0]	"PREBUILD_SHELVEQA_CL=5343135"	string
                //		[1]	"PREBUILD_SHELVEQA_DBID=76"	string

                foreach (String envVar in envVars)
                {
                    if (envVar.ToLower().Contains("shelveqa"))
                    {
                        String[] envBits = envVar.Split('=');

                        if (envBits.Length == 2)
                        {
                            int clToUnshelve = Int32.Parse(envBits[1]);

                            String code = this.Options.Branch.Environment.Subst("$(code)/");
                            String rage = this.Options.Branch.Environment.Subst("$(ragecode)/");

                            P4API.P4RecordSet latestChangeResults = p4.Run("changes", "-s", "submitted", "-m","1", code + "...", rage + "...");
                            if (latestChangeResults.Records.Length > 1)
                            {
                                int gameChange = Int32.Parse(latestChangeResults.Records[0]["change"]);
                                int rageChange = Int32.Parse(latestChangeResults.Records[1]["change"]);
                                int latestChange = Math.Max(gameChange, rageChange);

                                String syncGame = String.Format("{0}...@{1}", code, latestChange);
                                String syncRage = String.Format("{0}...@{1}", rage, latestChange);
                                
                                DateTime start = DateTime.Now;
                                log.MessageCtx(LOG_CTX, "P4: {0} {1}", syncGame, syncRage);
                                P4API.P4RecordSet syncResults = p4.Run("sync", syncGame, syncRage);

                                // do we already have the latest version?
                                if (syncResults.Records.Count() == 0)
                                {
                                    log.MessageCtx(LOG_CTX, "SyncP4 {0} {1} complete. File(s) up-to-date", syncGame, syncRage);
                                }
                                else
                                {
                                    int fileCount = Convert.ToInt32(syncResults.Records[0]["totalFileCount"]);
                                    log.MessageCtx(LOG_CTX, "SyncP4 {0} {1} complete. {2} files(s) updated in {3} minutes",
                                                        syncGame, syncRage, fileCount, (DateTime.Now - start).Minutes);
                                }

                                BuildContext = String.Format("{0}.ShelveCL{1}.BuildCL{2}", BuildContext, clToUnshelve, latestChange);
                            }

                            int localCL = 0;
                            if (CreateChangelist(log, p4, clToUnshelve, out localCL))
                            {
                                BuildContext = String.Format("{0}.LocalCL{1}", BuildContext, localCL);
                                if (RunUnshelve(log, p4, clToUnshelve, localCL))
                                {
                                    if (ResolveIntegration(log, p4, localCL))
                                    {
                                        log.MessageCtx(LOG_CTX, "SyncP4 resolve {0} complete", localCL);
                                    }
                                }
                            }
                        }
                        break;
                    }
                } 
            }
         }


        /// <summary>
        /// </summary>
        /// <returns></returns>
        private bool RunUnshelve(IUniversalLog log, P4 p4, int clToUnshelve, int createdCL)
        {
            List<string> args = new List<string>();

            //p4 unshelve -s 5325116 -c default //rdr3/src/dev/game/companion/Action/CompanionAnalysisActionDialog.cpp
            args.Add("-s");
            args.Add(clToUnshelve.ToString());
            args.Add("-c");
            args.Add(createdCL.ToString());

            P4RecordSet records = p4.Run("unshelve", args.ToArray());

            if (records.HasErrors())
            {
                log.ErrorCtx(LOG_CTX, "P4 failed to Integrate.");
                return false;
            }

            return true;
        }

        /// <summary>
        /// Creates a CL for the integration
        /// </summary>
        /// <returns></returns>
        private bool CreateChangelist(IUniversalLog log, P4 p4, int clToUnshelve, out int outCL)
        {
            String message = String.Format("Shelve QA Automated Integration for changelist #{0}", clToUnshelve);

            P4PendingChangelist pendingCL = p4.CreatePendingChangelist(message);
            outCL = pendingCL.Number;

            if (pendingCL != null)
            {
                log.MessageCtx(LOG_CTX, "*** CL {0} created. ***", outCL);
                return true;
            }
            return false;
        }

        //@TODO do we have a R* layer on top of P4?
        private bool ResolveIntegration(IUniversalLog log, P4 p4, int localCL)
        {
            ICollection<string> args = new List<string>();
            args.Add("-am"); 
            args.Add("-c");
            args.Add(localCL.ToString());

            P4API.P4RecordSet parsedRecordSetResolve = p4.Run("resolve", true, args.ToArray());
            p4.LogP4RecordSetMessages(parsedRecordSetResolve);
            if (parsedRecordSetResolve.HasErrors())
            {
                log.ErrorCtx(LOG_CTX, "p4 resolve : {0} errors were encountered.  Please cleanup your files and re-shelve.",
                    parsedRecordSetResolve.Errors.Length);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Delete our local CL for unshelving stuff for shelveQA
        /// </summary>
        /// <param name="log"></param>
        /// <param name="p4"></param>
        /// <param name="localCL"></param>
        /// <returns></returns>
        private bool RevertChangelist(IUniversalLog log, P4 p4)
        {
            String[] context = BuildContext.Split('.');
            int localCL = -1;
            foreach (String block in context)
            {
                if (block.StartsWith("localCL"))
                {
                    String withoutCL = block.Replace("localCL", "");
                    localCL = Int32.Parse(withoutCL);
                    break;
                }
            }

            if (localCL > 0)
            {
                String revertCmd = String.Format("-c {0}", localCL);
                P4API.P4RecordSet revertResults = p4.Run("revert", revertCmd); 

                if(revertResults.HasErrors())
                {
                    log.ErrorCtx(LOG_CTX, "p4 revert : {0} errors were encountered.", revertResults.Errors.Length);
                    return false;
                }
                
                String deleteCL = String.Format("-d {0}", localCL);
                P4API.P4RecordSet delResult = p4.Run("change", deleteCL);

                if (delResult.HasErrors())
                {
                    log.ErrorCtx(LOG_CTX, "p4 del : {0} errors were encountered.", delResult.Errors.Length);
                    return false;
                }
                return true;
            }

            return false;
        }

       /// <summary>
        /// so that they can be written to ensure the upload files are writable.
        /// - currently syncs to revision 0 ( an experimental technique ) 
        /// </summary>
        /// <returns></returns>
        private void EnableWriteInArtefactDirectories(IUniversalLog log, P4 p4, CodeBuilder2Job currentJob)
        {
            log.MessageCtx(LOG_CTX, " ");
            log.MessageCtx(LOG_CTX, "--- EnableWriteInArtefactDirectories ---");

            if (String.IsNullOrEmpty(currentJob.TargetDir))
            {
                log.MessageCtx(LOG_CTX, "No target directory; no files to enable write.");
                return;
            }

            try
            {
                String path = Path.Combine(currentJob.TargetDir, "*");

                // Revert these files - could be already edited
                P4API.P4RecordSet recordSetRevert = p4.Run("revert", path);
                p4.ParseAndLogRecordSetForDepotFilename("revert", recordSetRevert);

                // Sync to revision 0 / none - yes this is a delete, but it enables to detect when files are truly modified in concrete way.
                // - otherwise you would be reliant of builds executed previously and/or what is checked into perforce ( by the builder or users ) 
                // - this would open up too many variables and we wish to be able to have a deterministic upload, it will pay dividends later.
                // DW: this is experimental, we may still have to qualify other files for upload - this is the easy mechanism, but there could be perf implications?
                P4API.P4RecordSet recordSetSync = p4.Run("sync", "-f", String.Format("{0}#none", path));
                p4.ParseAndLogRecordSetForDepotFilename("sync", recordSetSync);

                string[] filePaths = System.IO.Directory.GetFiles(currentJob.TargetDir);
                foreach (string filePath in filePaths)
                {
                    // due to what I think is a p4 bug with force syncing to revision 0 we have to take extra steps to clear this folder
                    log.WarningCtx(LOG_CTX, "Deleting {0}", filePath);
                    System.IO.File.Delete(filePath);
                }
            }
            catch (Exception ex)
            {
                log.ErrorCtx(LOG_CTX, "An exception occurred in checking out the upload dir before the codebuilder job built {0}", ex);
            }
        }

        /// <summary>
        /// Start of build
        /// </summary>
        /// <param name="startInfo"></param>
        private void BuildStart(IUniversalLog log, ProcessStartInfo startInfo)
        {           
            log.MessageCtx(BuildContext, " ");
            log.MessageCtx(BuildContext, "<*****************************> ");
            log.MessageCtx(BuildContext, "< CODEBUILDER2 BUILD STARTED  >");
            log.MessageCtx(BuildContext, "< {0} ", BuildContext);            
            log.MessageCtx(BuildContext, "< {0} {1}", startInfo.FileName, startInfo.Arguments);
            log.MessageCtx(BuildContext, "<*****************************> ");
            log.MessageCtx(BuildContext, " ");
        }

        /// <summary>
        /// End of build
        /// </summary>
        /// <param name="buildProcess"></param>
        private void BuildEnd(IUniversalLog log, P4 p4, System.Diagnostics.Process buildProcess, CodeBuilder2Job currentJob)
        {
            log.MessageCtx(BuildContext, " ");
            log.MessageCtx(BuildContext, "<******************************> ");
            bool success = true;
            if (NumErrors[CurrentBuildStage] == 0 && buildProcess.ExitCode == 0)
                log.MessageCtx(BuildContext, "< CODEBUILDER2 BUILD SUCCEEDED >");
            else
            {
                log.ToolErrorCtx(BuildContext, "< CODEBUILDER2 BUILD FAILED    >");
                success = false;
            }
            log.MessageCtx(BuildContext, "< {0}", BuildContext);
            log.MessageCtx(BuildContext, "< {0} {1}", buildProcess.StartInfo.FileName, buildProcess.StartInfo.Arguments);
            log.MessageCtx(BuildContext, "< Exit Code : {0}", buildProcess.ExitCode);
            log.MessageCtx(BuildContext, "< Errors: {0} Warnings: {1}", NumErrors[CurrentBuildStage], NumWarnings[CurrentBuildStage]);
            log.MessageCtx(BuildContext, "<******************************> ");
            log.MessageCtx(BuildContext, " ");

            PublishDBResults(log, p4, success, currentJob);
        }
        /// <summary>
        /// Publish any need results to a DB; only used for shelveQA atm
        /// </summary>
        /// <param name="log"></param>
        /// <param name="p4"></param>
        /// <param name="success"></param>
        private void PublishDBResults(IUniversalLog log, P4 p4, bool success, CodeBuilder2Job currentJob)
        {
            if (BuildContext.ToLower().Contains("shelveqa"))
            {
                RevertChangelist(log, p4);

                String DBServerName = (String)Parameters[PARAM_SHELVE_QA_DB_SERVER];
                String DBName = (String)Parameters[PARAM_SHELVE_QA_DB];

                String[] context = BuildContext.Split('.');
                int dbID = -1;
                foreach(String block in context)
                {
                    if (block.ToUpper().StartsWith("PREBUILD_SHELVEQA_DBID"))
                    {
                        //"UserRequest_sshoemaker_PREBUILD_SHELVEQA_CL=5343135.DBID=45.ShelveCL5343135.BuildCL5344720"	
                        String[] ids = block.Split('=');
                        if (ids.Length == 2)
                        {
                            dbID = Int32.Parse(ids[1]);
                            break;
                        }
                    }
                }
                if (dbID >= 0)
                {
                    String connectionString = String.Format("Initial Catalog={0};Data Source={1};Integrated Security=SSPI;", DBName, DBServerName);
                    log.MessageCtx(LOG_CTX, "Establishing sqlConnection to {0} {1}", DBServerName, DBName);

                    using (SqlConnection sqlConnection = new SqlConnection(connectionString))
                    {
                        try
                        {
                            sqlConnection.Open();

                            String shelveTableName = (String)Parameters[PARAM_SHELVE_QA_DB_MAIN_TABLE];
                            String sqlUpdate = String.Format("Update {0} SET BuildStatus=@BuildStatus, BuildLocation=@BuildLocation WHERE ID={1}", shelveTableName, dbID);
                            SqlCommand updateCommand = new SqlCommand(sqlUpdate, sqlConnection);
                            updateCommand.Parameters.AddWithValue("@BuildStatus", success ? "Build Complete" : "Build Failed");
                            updateCommand.Parameters.AddWithValue("@BuildLocation", currentJob.PublishDir);
                            int rowsUpdated = updateCommand.ExecuteNonQuery();

                            log.MessageCtx(LOG_CTX, String.Format("Updated row {0} to {1}.{2}.{3}", dbID, DBServerName, DBName, shelveTableName));
                        }

                        catch (System.Data.SqlClient.SqlException ex)
                        {
                            log.Error("SQL fail: {0}", ex.Message);
                        }
                        finally
                        {
                            log.MessageCtx(LOG_CTX, "Closing sqlConnection to {0} {1}", DBServerName, DBName);
                            sqlConnection.Close();
                        }
                    }
                }
			}
		}

        /// <summary>
        /// Creates the executable and arguments required to build the specified job.  
        /// </summary>
        /// <param name="job"></param>
        /// <returns>A populated ProcessStartInfo object</returns>
        private ProcessStartInfo CreateProcess(IUniversalLog log, CodeBuilder2Job job)
        {
            // Switch the tool and build the commandline to execuate in the build process
            if (!this.Parameters.ContainsKey(job.Tool))
            {
                log.ErrorCtx(LOG_CTX, "Unknown Tool {0} : it is case sensitive and should be lowercase.", job.Tool);
                return null;
            }

            String executable = Environment.ExpandEnvironmentVariables((string)this.Parameters[job.Tool]);
            
            String args = String.Empty;
            switch (job.Tool.ToLower())
            {
                case PARAM_INCREDIBUILD:
                case PARAM_INCREDIBUILD_NEW:
                    args = String.Format("\"{0}\" {3} /cfg=\"{1}|{2}\"", job.SolutionFilename, job.BuildConfig, job.Platform, job.Rebuild ? "/rebuild" : "/build"); 
                    break;
                case PARAM_VS2008:
                case PARAM_VS2010:
                case PARAM_VS2012:
                case PARAM_VS2015:
                case PARAM_VS2017:
                case PARAM_VS2019:
                    args = String.Format("\"{0}\" {3} \"{1}|{2}\"", job.SolutionFilename, job.BuildConfig, job.Platform, job.Rebuild ? "/rebuild" : "/build");
                    break;
                case PARAM_VSI2008:
                case PARAM_VSI2010:
                case PARAM_VSI2011:
                    args = String.Format("\"{0}\" \"{1}|{2}\" {3}", job.SolutionFilename, job.BuildConfig, job.Platform, job.Rebuild ? "/incredi /clean" : "/incredi /build");
                    break;
                default: 
                    log.WarningCtx(LOG_CTX, "Unknown Tool {0}", job.Tool);
                    return null;
            }            

            ProcessStartInfo startInfo = base.CreateProcess(log, job, BuildEnvironment, executable, args);

            return startInfo;
        }

        /// <summary>
        /// Parses the line of output from the build process.
        /// - counts errors and warnings.
        /// - redirects to log.
        /// </summary>
        /// <param name="line"></param>
        private void ParseLine(IUniversalLog log, String line)
        {
            String context = String.Format("{0}.{1}", BuildStage, BuildContext);

            try
            {
                // Protect against interpretting '{ something other than a number }'
                line = line.Replace("{", "{{").Replace("}", "}}");

                if (ErrorRegex.IsMatch(line))
                {
                    log.ErrorCtx(context, line);
                    this.NumErrors[CurrentBuildStage]++;
                }
                else if (WarningRegex.IsMatch(line))
                {
                    log.WarningCtx(context, line);
                    this.NumWarnings[CurrentBuildStage]++;
                }
                else
                {
                    log.MessageCtx(context, line);
                }
            }
            catch (System.FormatException ex)
            {
                //Log.WarningCtx(context, "A parsed line was not in the correct format to display.");
            }
        }

        /// <summary>
        /// Handle stdout while a processing is running.
        /// </summary>
        /// <param name="logObj">the log</param>
        /// <param name="obj">the string</param>
        private void ReadStandardOutput(object logObj, object obj)
		{
            IUniversalLog log = (IUniversalLog)logObj;
            String line = obj as String;
            if (line == null)
            {
                log.ToolErrorCtx(LOG_CTX,"Non String object send to ReadStandardError");
                return;
            }

            try
            {
                ParseLine(log, line);
            }
            catch (FormatException fe) // Used to catch issues with formatting log messages
            {
                log.WarningCtx(LOG_CTX, String.Format("Error Occured while logging from stream stdout {0}", fe.ToString()));
            }
            catch (Exception e) 
            { 
                log.ToolExceptionCtx(LOG_CTX, e, "Exception parsing stdout stream.");
            }
        }

        /// <summary>
        /// Handle stderr while a processing is running.
        /// </summary>
        /// <param name="logObj">the log</param>
        /// <param name="obj">the string</param>
        private void ReadStandardError(object logObj, object obj)
        {
            IUniversalLog log = (IUniversalLog)logObj;
            String line = obj as String;
            if (line == null)
            {
                log.ToolErrorCtx(LOG_CTX,"Non String object send to ReadStandardError");
                return;
            }

            try
            {
                if (!String.IsNullOrWhiteSpace(line))
                {
                    log.ErrorCtx(BuildContext, line);
                    this.NumErrors[CurrentBuildStage]++;
                }
            }
            catch (FormatException fe) // Used to catch issues with formatting log messages
            {
                log.WarningCtx(LOG_CTX, String.Format("Error Occured while logging from stream stderr {0}", fe.ToString()));
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Exception parsing stderr stream.");
            }
        }
        #endregion // Private Methods
    }
}
