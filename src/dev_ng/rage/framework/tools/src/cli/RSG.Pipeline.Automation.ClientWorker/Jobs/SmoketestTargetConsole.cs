﻿using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{
    class SmoketestTargetConsoleParameters  
    {
        private String m_LogFile;
        public String LogFile
        {
            get { return m_LogFile; }
            set { m_LogFile = value; }
        }

        private String m_ExePath;
	    public String ExePath
	    {
		    get { return m_ExePath; }
		    set { m_ExePath = value; }
	    }

        private String m_CommandLineArguments;
        public String CommandLineArguments
        {
            get { return m_CommandLineArguments; }
            set { m_CommandLineArguments = value; }
        }
    }

    abstract class SmoketestTargetConsole : ISmoketestTarget
    {
        private String m_IpAddress;
        public String IpAddress
        {
            get { return m_IpAddress; }
            set { m_IpAddress = value; }
        }

        private String m_LogFilePath;
        public String LogFilePath
        {
            get { return m_LogFilePath; }
            set { m_LogFilePath = value; }
        }

        private LogMonitor m_Monitor;
        public LogMonitor Monitor
        {
            set { m_Monitor = value; }
        }
        public bool IsMonitorNull() { return m_Monitor == null; }
        public LogMonitor TakeMonitor() { LogMonitor m = m_Monitor; m_Monitor = null; return m; }

        private IUniversalLog m_Log;
        public IUniversalLog Log
        {
            get { return m_Log; }
            set { m_Log = value; }
        }

        SmoketestTargetConsoleParameters m_Parameters;
        public SmoketestTargetConsoleParameters Parameters
        {
            get { return m_Parameters; }
            set { m_Parameters = value; }
        }

        int m_RunIndex = -1;
        public int RunIndex
        {
            get { return m_RunIndex; }
            set { m_RunIndex = value; }
        }

        bool m_HasAttemptedLaunch;
        public bool HasAttemptedLaunch
        {
            get { return m_HasAttemptedLaunch; }
            set { m_HasAttemptedLaunch = value;  }
        }

        public bool IsLaunching
        {
            get { return m_LaunchThread != null; }
        }

        Thread m_LaunchThread;
        public Thread LaunchThread
        {
            get { return m_LaunchThread; }
            set { m_LaunchThread = value; }
        }

        protected CommandOptions Options;

        protected static readonly String LOG_CTX = "Smoke Test Target (Console)";

        public SmoketestTargetConsole(IUniversalLog log, String ipAddress, CommandOptions options)
        {
            m_Log = log;
            m_IpAddress = ipAddress;
            Options = options;
        }

        public virtual bool IsRunning(String executable)
        {
            return true;
        }
        
        public bool CanLaunch()
        {
            return m_LaunchThread == null;
        }

        public virtual void Launch(SmoketestTargetConsoleParameters parameters)
        {
            if (m_LaunchThread != null)
            {
                throw new InvalidOperationException();
            }

            m_LogFilePath = null;
            if (m_Monitor != null)
            {
                m_Monitor.Dispose();
                m_Monitor = null;
            }
            m_Parameters = parameters;
            m_LaunchThread = new Thread(new ThreadStart(LaunchTheThread))
            {
                Name = "Smoketest : LaunchTheThread"
            };
            m_LaunchThread.Start();
        }

        /// <summary>
        /// This function needs to be implmeneted by inheriting classes, but cannot make abstract due to it being declared
        /// and used in ISmoketestTarget
        /// </summary>
        /// <param name="param"></param>
        /// <returns></returns>
        public virtual void Shutdown(String param)
        {
            throw new NotImplementedException();
        }

        public void Relaunch()
        {
            Launch(m_Parameters);
        }

        virtual protected void LaunchTheThread()
        {
            String gameExecutable = Environment.ExpandEnvironmentVariables(m_Parameters.ExePath);

            if (DoDeploy(gameExecutable))
            {
                ++RunIndex;
                DoLaunch("");
            }
            
            m_LaunchThread = null;
            m_HasAttemptedLaunch = true;
        }

        protected virtual bool DoDeploy(String gameExecutable)
        {
            return true;
        }

        protected abstract bool DoLaunch(String param);

        protected bool RunExternalTool(String executable, String args, TimeSpan waitTime)
        {
            this.Log.MessageCtx(LOG_CTX, "Executing: {0} {1}", executable, args);

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = executable;
            startInfo.Arguments = args;

            System.Diagnostics.Process launchGameProcess = new System.Diagnostics.Process();

            launchGameProcess.StartInfo = startInfo;
            launchGameProcess.StartInfo.UseShellExecute = false;
            launchGameProcess.StartInfo.RedirectStandardOutput = true;
            launchGameProcess.StartInfo.RedirectStandardError = true;
            launchGameProcess.StartInfo.RedirectStandardInput = true;
            launchGameProcess.StartInfo.CreateNoWindow = false;

            launchGameProcess.Start();

            launchGameProcess.OutputDataReceived += OutputDataReceived;
            launchGameProcess.ErrorDataReceived += ErrorDataReceived;

            launchGameProcess.BeginOutputReadLine();
            launchGameProcess.BeginErrorReadLine();

            // Wait up to a minute
            DateTime deployStartTime = DateTime.Now;
            while (waitTime > DateTime.Now - deployStartTime && !launchGameProcess.HasExited)
            {
                System.Threading.Thread.Sleep(1000);
            }

            bool success = false;

            //String stdOutput = launchGameProcess.StandardOutput.ReadToEnd();
            //String stdError = launchGameProcess.StandardError.ReadToEnd();

            if (launchGameProcess.HasExited)
            {
                this.Log.MessageCtx(LOG_CTX, "Exit code was {0}", launchGameProcess.ExitCode);

                if (launchGameProcess.ExitCode == 0)
                {
                    success = true;
                }
            }

            if (!launchGameProcess.HasExited)
            {
                launchGameProcess.Kill();
            }

            return success;
        }

        protected void OutputDataReceived(object sender, DataReceivedEventArgs dataIn)
        {  
            String blah = dataIn.Data as String;
            if (blah != null)
            {
                try
                {
                    this.Log.MessageCtx(LOG_CTX, "Deploy STDOUT:{0}", dataIn.Data);
                }
                catch (System.Exception ex)
                {
                    this.Log.MessageCtx(LOG_CTX, "Deploy STDOUT: exception: {0}", ex);
                }
            
            }
        }

        public void ErrorDataReceived(object sender, DataReceivedEventArgs dataIn)
        {
            String blah = dataIn.Data as String;
            if (blah != null)
            {
                try
                {
                    this.Log.MessageCtx(LOG_CTX, "Deploy STDERR:{0}", dataIn.Data);
                }
                catch (System.Exception ex)
                {
                    this.Log.MessageCtx(LOG_CTX, "Deploy STDERR: exception: {0}", ex);
                }
            }
        }
    }
}
