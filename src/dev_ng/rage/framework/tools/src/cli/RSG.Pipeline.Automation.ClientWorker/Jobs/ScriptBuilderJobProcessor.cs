﻿using Ionic.Zip;
using P4API;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Build;
using RSG.Pipeline.Engine;
using RSG.Pipeline.Services;
using RSG.SourceControl.Perforce;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;
using System.Xml.Linq;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{
    /// <summary>
    /// Enum signifying the stage of processing for a ScriptBuilder
    /// </summary>
    internal enum ScriptBuilderBuildStage
    {
        Invalid = -1,
        PreBuild,       // Prebuild commands.
        MainBuild,      // Compilation
        PostBuild,      // Postbuild commands.
        Finalise,       // Final fluff
        NumStages
    };

    /// <summary>
    /// ScriptBuilder - job processor.
    /// </summary>
    internal sealed class ScriptBuilderJobProcessor :
        JobProcessorBase,
        IJobProcessor
    {
        #region Constants
        private const String LOG_CTX = "Script Builder Job Processor";

        /// <summary>
        /// Parameter that indicates this is not a live ScriptBuilder : some operations will not be performed.
        /// </summary>
        private const String PARAM_DEV = "dev";

        /// <summary>
        /// Params specifying the executable tool to use for builds.
        /// </summary>
        private const String PARAM_XGE = "xge";
        private const String PARAM_BUILD_ENVIRONMENT = "BuildEnvironment";

        /// <summary>
        /// Params for the regex matching on build output
        /// </summary>
        private const String PARAM_REGEX_ERROR = "error regex";
        private const String PARAM_REGEX_WARNING = "warning regex";

        /// <summary>
        /// Param for a regex that needs to be matched and acts a filter for target files.
        /// this has an effect on publishing, and uploading
        /// </summary>
        private const String PARAM_REGEX_TARGET = "target filter regex";

        /// <summary>
        /// Enviroment variables names exposed to build processes at prebuild
        /// </summary>
        private const String EnvironmentSrcCL = "RSG_AUTOMATION_SCRIPTBUILDER_SRC_CL";
        private const String EnvironmentContext = "RSG_AUTOMATION_SCRIPTBUILDER_CONTEXT";

        /// <summary>
        /// Further enviroment variables names exposed to build processes at postbuild
        /// </summary>        
        private const String EnvironmentNumErrors = "RSG_AUTOMATION_SCRIPTBUILDER_NUM_ERRORS";
        private const String EnvironmentNumWarnings = "RSG_AUTOMATION_SCRIPTBUILDER_NUM_WARNINGS";
        private const String EnvironmentBuildSuceeded = "RSG_AUTOMATION_SCRIPTBUILDER_BUILD_SUCCEEDED";

        #endregion // Constants

        #region Properties

        /// <summary>
        /// A context string that shows the build we are building
        /// - we splatter this everywhere so it's possible to track the many flavours of builds easily.
        /// - this is because builds can rapidly flood with errors and warnings.
        /// </summary>
        private string BuildContext { get; set; }

        /// <summary>
        /// Environment to set during build processing.
        /// </summary>
        private IDictionary<String, String> BuildEnvironment { get; set; }

        /// <summary>
        /// Current job that is being processed.
        /// </summary>
        private ScriptBuilderJob CurrentJob { get; set; }

        /// <summary>
        /// The stage of processing within the job processing - see enum.
        /// </summary>
        private ScriptBuilderBuildStage BuildStage { get; set; }

        /// <summary>
        /// Process Log - this is used to make it easy to set the log for the readstandardoutput and readstandarderror callbacks
        /// </summary>
        private IUniversalLog ProcessLog { get; set; }

        /// <summary>
        /// Num warnings parsed ( prebuild )
        /// </summary>
        private int[] NumWarnings { get; set; }

        /// <summary>
        /// Num errors parsed ( postbuild ) 
        /// </summary>
        private int[] NumErrors { get; set; }

        /// <summary>
        /// Build output error regex.
        /// - keep these efficient
        /// </summary>
        private Regex ErrorRegex { get; set; }

        /// <summary>
        /// Build output warning regex.
        /// - keep these efficient
        /// </summary>
        private Regex WarningRegex { get; set; }

        /// <summary>
        /// Makes a common cast of the BuildStage
        /// </summary>
        /// <returns></returns>
        private int CurrentBuildStage
        {
            get { return (int)BuildStage; }
        }

        #endregion //  Properties

        #region Delegates
        private delegate void LogDelegate(String context, String message, params Object[] args);
        #endregion // Delegates

        #region Constructors
        /// <summary>
        /// Parameterized Constructor
        /// </summary>
        /// <param name="options"></param>
        /// <param name="uploadConsumer"></param>
        public ScriptBuilderJobProcessor(CommandOptions options, FileTransferServiceConsumer uploadConsumer)
            : base(options, CapabilityType.ScriptBuilder, uploadConsumer)
        {
            ResetErrorsAndWarnings();
            BuildStage = ScriptBuilderBuildStage.Invalid;

            //Log = LogFactory.CreateUniversalLog("ScriptBuilder");

            if (this.Parameters.ContainsKey(PARAM_REGEX_ERROR))
            {
                String regex = this.Parameters[PARAM_REGEX_ERROR].ToString();
                ErrorRegex = new Regex(regex, RegexOptions.IgnoreCase);
            }
            else
            {
                //Log.Error("Missing parameter {0}", PARAM_REGEX_ERROR);
            }

            if (this.Parameters.ContainsKey(PARAM_REGEX_WARNING))
            {
                String regex = this.Parameters[PARAM_REGEX_WARNING].ToString();
                WarningRegex = new Regex(regex, RegexOptions.IgnoreCase);
            }
            else
            {
                //Log.Error("Missing parameter {0}", PARAM_REGEX_WARNING);
            }

            if (this.Parameters.ContainsKey(PARAM_BUILD_ENVIRONMENT))
            {
                InitialiseBuildEnvironment(null);
            }
        }

        #endregion // Constructors

        #region Controller Methods
        /// <summary>
        /// Process requested job.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public override IJobResult Process(IJob job)
        {
            BuildStage = ScriptBuilderBuildStage.Invalid;

            ResetErrorsAndWarnings();
            JobResult jobResult = new JobResult(job.ID, false);

            Debug.Assert(job is ScriptBuilderJob, "Invalid job for processor.");
            if (!(job is ScriptBuilderJob))
                return null;

            CurrentJob = (ScriptBuilderJob)job;

            Init(job);

            IUniversalLog log = LogFactory.CreateUniversalLog("scriptbuilder_process");
            ILogTarget logFile = LogFactory.CreateUniversalLogFile(
                String.Format("process_{0}", CurrentJob.ID), log) as ILogTarget;

            String targetContext = String.Format("{0}.{1}.{2}", System.IO.Path.GetFileNameWithoutExtension(CurrentJob.ScriptProjectFilename), CurrentJob.BuildConfig, CurrentJob.Rebuild ? "reb" : "inc");
            log.MessageCtx(LOG_CTX, "*** LOG HAS BEEN CREATED : {0}", targetContext);

            if (!System.IO.File.Exists(CurrentJob.ScriptProjectFilename))
            {
                log.ErrorCtx(LOG_CTX, "Unable to find ScriptProject Filename {0}.", CurrentJob.ScriptProjectFilename);
                return jobResult;
            }

            // Ensure that the context is set to <context>.dev so that we never interfere with the live builds in development.
            bool isDev = this.Parameters.ContainsKey(PARAM_DEV);
            if (isDev)
            {
                targetContext = String.Format("dev.{0}", targetContext);
            }

            uint clNumMax = 0;
            if (!(job.Trigger is IUserRequestTrigger))
            {
                IEnumerable<uint> clNums = job.Trigger.ChangelistNumbers();
                clNumMax = clNums.Max();
                if (clNums.HasAtLeast(2))
                {
                    BuildContext = String.Format("{0}.CL{1}-{2}", targetContext, clNums.Min(), clNumMax);
                }
                else if (clNums.Any()) // has one CL
                {
                    BuildContext = String.Format("{0}.CL{1}", targetContext, clNumMax);
                }
            }
            else
            {
                //[TODO] Figure out something better for this
                //The only indication of this being a Label'ed build is the single prebuild command
                BuildContext = String.Format("UserRequest_{0}_{1}", ((IUserRequestTrigger)job.Trigger).Username, ((IUserRequestTrigger)job.Trigger).Command);
            }

            // *********************************************
            // *** SCRIPTBUILDER CONTROL LOGIC FOLLOWS *****
            // *********************************************

            bool[] stageSucceeded = new bool[(int)ScriptBuilderBuildStage.NumStages];

            // Clear the build environment each build.
            InitialiseBuildEnvironment(log);

            using (P4 p4 = this.Options.Project.SCMConnect())
            {
                BuildStage = ScriptBuilderBuildStage.PreBuild;

                // 1. Sync to the CL triggering this job.
                SyncToCL(log, p4, isDev);

                // Set RSG_ environment variables that prebuild processes can utilise
                SetPrebuildEnvironment(log, clNumMax, CurrentJob.Trigger);

                // *********************
                // 2. Run prebuild steps
                // *********************
                int origNumErrors = NumErrors[CurrentBuildStage];
                this.ProcessLog = log;
                stageSucceeded[CurrentBuildStage] = Prebuild(log, CurrentJob, BuildEnvironment, ReadStandardOutput, ReadStandardError);
                ShowStageSuccess(log, stageSucceeded);
                int newNumErrors = NumErrors[CurrentBuildStage];
                if (newNumErrors > origNumErrors && stageSucceeded[CurrentBuildStage])
                {
                    log.WarningCtx(LOG_CTX, "Prebuild process(es) returned true giving a notion of success. However, {0} errors were encountered, one of the prebuild process(es) should be reviewed.", newNumErrors - origNumErrors);
                }

                // **********
                // Main build
                // **********
                if (stageSucceeded[CurrentBuildStage])
                {
                    BuildStage = ScriptBuilderBuildStage.MainBuild;

                    // 3. Actually build the scripts.  
                    stageSucceeded[CurrentBuildStage] = BuildScript(log);
                    ShowStageSuccess(log, stageSucceeded);

                    // Set RSG_ environment variables that postbuild processes can utilise
                    SetPostBuildEnvironment(log, stageSucceeded[CurrentBuildStage]);
                }

                // **********************
                // 4. Run postbuild steps
                // **********************
                if (stageSucceeded[CurrentBuildStage])
                {
                    BuildStage = ScriptBuilderBuildStage.PostBuild;

                    // Run the postbuild commmands
                    stageSucceeded[CurrentBuildStage] = Postbuild(log, CurrentJob, BuildEnvironment, ReadStandardOutput, ReadStandardError);
                    ShowStageSuccess(log, stageSucceeded);
                }

                BuildStage = ScriptBuilderBuildStage.Finalise;

                // ...um nothing to do
                stageSucceeded[CurrentBuildStage] = true;

                // 5. React to errors and warnings.
                bool succeeded = HandleErrorsAndWarnings(log, jobResult, stageSucceeded);

                // 6. Upload the log file
                UploadLog(job, log, logFile, targetContext);

            } // end p4

            return jobResult;
        }

        private void ShowStageSuccess(IUniversalLog log, bool[] stageSucceeded)
        {
            if (stageSucceeded[CurrentBuildStage])
                log.MessageCtx(LOG_CTX, "{0} succeeded.", BuildStage);
            else
                log.ToolErrorCtx(LOG_CTX, "{0} failed.", BuildStage);
        }
        #endregion // Controller Methods

        #region Private Methods

        private void ResetErrorsAndWarnings()
        {
            NumWarnings = new int[(int)ScriptBuilderBuildStage.NumStages];
            NumErrors = new int[(int)ScriptBuilderBuildStage.NumStages];
        }

        private int TotalErrors()
        {
            int totalErrors = NumErrors.Sum();
            return totalErrors;
        }

        private int TotalWarnings()
        {
            int totalWarnings = NumWarnings.Sum();
            return totalWarnings;
        }

        private void InitialiseBuildEnvironment(IUniversalLog log)
        {
            this.BuildEnvironment = new Dictionary<String, String>();
            IEnumerable<String> envList = ((IEnumerable<Object>)this.Parameters[PARAM_BUILD_ENVIRONMENT]).OfType<String>();
            foreach (string envString in envList)
            {
#warning DW : TODO : make more defensive, use some fancy linq
                IEnumerable<String> kvp = envString.Split('=').Select(e => e.Trim());
                if (kvp.Count() == 2)
                {
                    if (log != null)
                        log.MessageCtx(LOG_CTX, "Read param driven build environment ({0}={1})", kvp.ElementAt(0), kvp.ElementAt(1));
                    this.BuildEnvironment.Add(kvp.ElementAt(0), kvp.ElementAt(1));
                }
                else
                {
                    if (log != null)
                        log.WarningCtx(LOG_CTX, "Unusual build environment ignored : {0}", envString);
                }
            }
        }

        /// <summary>
        /// Set RSG_ environment variables that prebuild processes can utilise
        /// </summary>
        /// <param name="CL"></param>
        private void SetPrebuildEnvironment(IUniversalLog log, uint changelistNumber, ITrigger trigger)
        {
            // Expose inital job details to environment.
            BuildEnvironment.Add(EnvironmentSrcCL, changelistNumber.ToString());
            BuildEnvironment.Add(EnvironmentContext, BuildContext);
            if (trigger is IHasCommand)
            {
                if (!String.IsNullOrWhiteSpace(((IUserRequestTrigger)trigger).Command))
                {
                    //Check for build environment settings in the string
                    //EX: SYNC_P4_LABEL=MyLabelNameHere CLEAR_FILES_WITH_EXT=.pdb
                    //This format should be verified at Trigger/Job creation
                    String[] envSettings = ((IUserRequestTrigger)trigger).Command.Split(' ');
                    foreach (String envVarKvp in envSettings)
                    {
                        Regex regex = new Regex("(^[^=]+)=(.*)", RegexOptions.IgnoreCase);
                        Match match = regex.Match(envVarKvp);
                        if (match.Success && match.Groups.Count == 3)
                        {
                            String envVarName = match.Groups[1].Value;
                            String envVarValue = match.Groups[2].Value;
                            BuildEnvironment.Add(envVarName, envVarValue);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Set RSG_ environment variables that postbuild processes can utilise
        /// </summary>
        /// <param name="buildSucceeded"></param>
        private void SetPostBuildEnvironment(IUniversalLog log, bool buildSucceeded)
        {
            // Expose processed job details to environment that can be used in post build scripts.
            BuildEnvironment.Add(EnvironmentNumErrors, TotalErrors().ToString());
            BuildEnvironment.Add(EnvironmentNumWarnings, TotalWarnings().ToString());
            BuildEnvironment.Add(EnvironmentBuildSuceeded, buildSucceeded.ToString());
        }

        /// <summary>
        /// If we have errors or warning do something and set the jobresult as required.
        /// </summary>
        /// <param name="jobResult"></param>
        /// <returns></returns>
        private bool HandleErrorsAndWarnings(IUniversalLog log, JobResult jobResult, bool[] stageSucceeded)
        {
            // Overall notion of success = all phases of build must have succeeded.
            // there is scope to make this fuzzier if required, 
            // eg. prebuild OK, main build OK, post build fails, but mark build as success, because the post build isn't mission critical.
            bool allSucceeded = stageSucceeded[(int)ScriptBuilderBuildStage.PreBuild] &&
                                stageSucceeded[(int)ScriptBuilderBuildStage.MainBuild] &&
                                stageSucceeded[(int)ScriptBuilderBuildStage.PostBuild] &&
                                stageSucceeded[(int)ScriptBuilderBuildStage.Finalise];

            if (!allSucceeded)
            {
                bool showAsError = true;
                ShowBuildStageResultTable(log, stageSucceeded, showAsError);
                log.ToolErrorCtx(LOG_CTX, "Build Failed: {0} errors : {1} warnings", TotalErrors(), TotalWarnings());
                jobResult.Succeeded = false;
            }
            else if (TotalErrors() > 0)
            {
                bool showAsError = true;
                ShowBuildStageResultTable(log, stageSucceeded, showAsError);
                log.ToolErrorCtx(LOG_CTX, "Build Failed: {0} errors : {1} warnings", TotalErrors(), TotalWarnings());
                jobResult.Succeeded = true; // there are errors but we should let the build be successful.
            }
            else if (TotalWarnings() > 0)
            {
                ShowBuildStageResultTable(log, stageSucceeded);
#warning DW: TODO  Add optional param to support 'treat warning as error'.
                log.WarningCtx(LOG_CTX, "Build Succeeded: {0} errors : {1} warnings", TotalErrors(), TotalWarnings());
                jobResult.Succeeded = true;
            }
            else
            {
                ShowBuildStageResultTable(log, stageSucceeded);
                log.MessageCtx(LOG_CTX, "Build Succeeded: {0} errors : {1} warnings", TotalErrors(), TotalWarnings());
                jobResult.Succeeded = true;
            }

            return jobResult.Succeeded;
        }

        /// <summary>
        /// Pretty table/matrix of results.
        /// </summary>
        /// <param name="stageSucceeded"></param>
        private void ShowBuildStageResultTable(IUniversalLog log, bool[] stageSucceeded, bool showAsError = false)
        {
            LogDelegate logDelegate = showAsError ? new LogDelegate(log.ToolErrorCtx) : new LogDelegate(log.MessageCtx);

            logDelegate(LOG_CTX, " ");
            logDelegate(LOG_CTX, "============================================");
            logDelegate(LOG_CTX, "===       BUILD STAGE RESULT TABLE       ===");
            logDelegate(LOG_CTX, "============================================");
            logDelegate(LOG_CTX, "=    STAGE    |  Status      |  Err | Warn =");
            logDelegate(LOG_CTX, "============================================");

            for (int bs = 0; bs < (int)ScriptBuilderBuildStage.NumStages; bs++)
            {
                logDelegate(LOG_CTX, "= {0} | {1} | {2} | {3} =", (((ScriptBuilderBuildStage)bs).ToString()).PadLeft(11), stageSucceeded[bs] ? "   Succeeded" : "      Failed", NumErrors[bs].ToString().PadLeft(4), NumWarnings[bs].ToString().PadLeft(4));
            }
            logDelegate(LOG_CTX, "============================================");
            logDelegate(LOG_CTX, " ");
        }

        /// <summary>
        /// Build the scripts
        /// - kicks off processes
        /// </summary>
        /// <returns></returns>
        private bool BuildScript(IUniversalLog log)
        {
            log.MessageCtx(LOG_CTX, " ");
            log.MessageCtx(LOG_CTX, "--- BuildScript ---");

            // Create build process            
            ProcessStartInfo processStartInfo = CreateProcess(log, CurrentJob);
            if (processStartInfo == null)
            {
                log.ErrorCtx(LOG_CTX, "Cannot create build tool process");
                return false;
            }

            // Log the start  - make it clear in the logs / console.
            BuildStart(log, processStartInfo);

            log.ProfileCtx(LOG_CTX, "Build Scripts");
            System.Diagnostics.Process buildProcess = RunProcess(log, processStartInfo, ReadStandardOutput, ReadStandardError);
            log.ProfileEnd();

            // Log the end  - make it clear in the logs / console.
            BuildEnd(log, buildProcess);

            if (buildProcess.ExitCode != 0)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Sync to a CL across all the monitoring paths that the job monitored.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="isDev">indicates this is not a builder server - sync may not be desired in development</param>
        private void SyncToCL(IUniversalLog log, P4 p4, bool isDev)
        {
            log.MessageCtx(LOG_CTX, " ");
            log.MessageCtx(LOG_CTX, "--- SyncToCL ---");

            IEnumerable<uint> clNums = CurrentJob.Trigger.ChangelistNumbers();

            // Sync to CL if required.
            if (clNums.Any())
            {
                uint maxCl = clNums.Max();
                log.MessageCtx(LOG_CTX, "Syncing to CL{0}", maxCl);
                ICollection<String> syncFiles = new List<String>();

                // Prevents sync from actually running, yet runs the command.
                if (isDev)
                {
                    log.MessageCtx(LOG_CTX, "syncing with '-n' option, this will not actually sync these files but shows the results if it were to.");
                    syncFiles.Add("-n");
                }

                foreach (String clFile in CurrentJob.Trigger.MonitoredPaths())
                {
                    String pathtoSync = String.Format("{0}/...@{1}", clFile, maxCl);
                    syncFiles.Add(pathtoSync);
                }

                P4API.P4RecordSet recordSet = p4.Run("sync", syncFiles.ToArray());
                p4.ParseAndLogRecordSetForDepotFilename("sync", recordSet);
                p4.LogP4RecordSetMessages(recordSet);
            }
        }

        /// <summary>
        /// Upload the log seperately
        /// - we wish to upload this as the very last thing we do, since build artefacts are uploaded immediately after build.
        /// - this upload will include info regarding the postbuild commands.
        /// </summary>
        /// <param name="job"></param>
        /// <param name="?"></param>
        private void UploadLog(IJob job, IUniversalLog log, ILogTarget logFile, string targetContext)
        {
            log.MessageCtx(LOG_CTX, " ");
            log.MessageCtx(LOG_CTX, "--- Merge and upload logfile ---");
            log.MessageCtx(LOG_CTX, "*** LOG IS NOW CLOSING : {0} : {1} ", targetContext, job.ID);
            logFile.Flush();
            LogFactory.CloseUniversalLogFile(logFile);
            LogFactory.CloseUniversalLog(log);

            //Log.MessageCtx(LOG_CTX, "*** LOG IS NOW CLOSED : {0} : {1} : {2}", logFile.Filename, targetContext, job.ID);

            bool cleanLogs = false;
            String mergedLogFilename = CopyMergeAndCleanLogFiles(log, job, cleanLogs);
            //Log.MessageCtx(LOG_CTX, "*** LOG IS NOW MERGED : {0} => {1} : {2} : {3}", logFile.Filename, mergedLogFilename, targetContext, job.ID);

            if (!String.IsNullOrEmpty(mergedLogFilename))
            {
                if (!FileTransferServiceConsumer.UploadFile(job, mergedLogFilename))
                {
                    //Log.ErrorCtx(LOG_CTX, String.Format("ScriptBuilder could not upload Log file {0}", mergedLogFilename));
                }
            }
            //Log.MessageCtx(LOG_CTX, "*** LOG IS NOW UPLOADED : {0} : {1} : {2}", mergedLogFilename, targetContext, job.ID);
        }

        /// <summary>
        /// Start of build
        /// </summary>
        /// <param name="startInfo"></param>
        private void BuildStart(IUniversalLog log, ProcessStartInfo startInfo)
        {
            log.MessageCtx(BuildContext, " ");
            log.MessageCtx(BuildContext, "<*****************************> ");
            log.MessageCtx(BuildContext, "< ScriptBuilder BUILD STARTED  >");
            log.MessageCtx(BuildContext, "< {0} ", BuildContext);
            log.MessageCtx(BuildContext, "< {0} {1}", startInfo.FileName, startInfo.Arguments);
            log.MessageCtx(BuildContext, "<*****************************> ");
            log.MessageCtx(BuildContext, " ");
        }

        /// <summary>
        /// End of build
        /// </summary>
        /// <param name="buildProcess"></param>
        private void BuildEnd(IUniversalLog log, System.Diagnostics.Process buildProcess)
        {
            log.MessageCtx(BuildContext, " ");
            log.MessageCtx(BuildContext, "<******************************> ");
            if (NumErrors[CurrentBuildStage] == 0 && buildProcess.ExitCode == 0)
                log.MessageCtx(BuildContext, "< ScriptBuilder BUILD SUCCEEDED >");
            else
                log.ToolErrorCtx(BuildContext, "< ScriptBuilder BUILD FAILED    >");
            log.MessageCtx(BuildContext, "< {0}", BuildContext);
            log.MessageCtx(BuildContext, "< {0} {1}", buildProcess.StartInfo.FileName, buildProcess.StartInfo.Arguments);
            log.MessageCtx(BuildContext, "< Exit Code : {0}", buildProcess.ExitCode);
            log.MessageCtx(BuildContext, "< Errors: {0} Warnings: {1}", NumErrors[CurrentBuildStage], NumWarnings[CurrentBuildStage]);
            log.MessageCtx(BuildContext, "<******************************> ");
            log.MessageCtx(BuildContext, " ");
        }

        /// <summary>
        /// Creates the executable and arguments required to build the specified job.  
        /// </summary>
        /// <param name="job"></param>
        /// <returns>A populated ProcessStartInfo object</returns>
        private ProcessStartInfo CreateProcess(IUniversalLog log, ScriptBuilderJob job)
        {
            // Switch the tool and build the commandline to execuate in the build process
            if (!this.Parameters.ContainsKey(job.Tool))
            {
                log.ErrorCtx(LOG_CTX, "Unknown Tool {0} : it is case sensitive and should be lowercase.", job.Tool);
                return null;
            }

            String executable = Environment.ExpandEnvironmentVariables((string)this.Parameters[job.Tool]);

            String args = String.Empty;
            switch (job.Tool.ToLower())
            {
                case PARAM_XGE:
                    {
                        String filename = String.Format("{0}.xml", Path.GetFileNameWithoutExtension(job.ScriptProjectFilename));
                        // Now derive the temp file... eg. C:\Users\svcrsgsanvmbuilder\Documents\ragScriptEditor\XGE\rdr3.xml                        
                        filename = Path.Combine(Environment.ExpandEnvironmentVariables("%userprofile%"), "Documents", "ragScriptEditor", "XGE", filename);
                        if (!System.IO.File.Exists(filename))
                        {
                            throw new FileNotFoundException("XGE file not found", filename);
                        }
                        args = String.Format("\"{0}\" {1}", filename, job.Rebuild ? "/Rebuild" : "/Build");
                    }
                    break;
                default:
                    log.WarningCtx(LOG_CTX, "Unknown Tool {0}", job.Tool);
                    return null;
            }

            ProcessStartInfo startInfo = base.CreateProcess(log, job, BuildEnvironment, executable, args);

            return startInfo;
        }

        /// <summary>
        /// Parses the line of output from the build process.
        /// - counts errors and warnings.
        /// - redirects to log.
        /// </summary>
        /// <param name="line"></param>
        private void ParseLine(IUniversalLog log, String line)
        {
            String context = String.Format("{0}.{1}", BuildStage, BuildContext);

            try
            {
                // Protect against interpretting '{ something other than a number }'
                line = line.Replace("{", "{{").Replace("}", "}}");

                if (ErrorRegex.IsMatch(line))
                {
                    log.ErrorCtx(context, line);
                    this.NumErrors[CurrentBuildStage]++;
                }
                else if (WarningRegex.IsMatch(line))
                {
                    log.WarningCtx(context, line);
                    this.NumWarnings[CurrentBuildStage]++;
                }
                else
                {
                    log.MessageCtx(context, line);
                }
            }
            catch (System.FormatException)
            {
                log.WarningCtx(context, "A parsed line was not in the correct format to display.");
            }
        }

        /// <summary>
        /// Handle stdout while a processing is running.
        /// </summary>
        /// <param name="logObj">the log</param>
        /// <param name="obj">the string</param>
        private void ReadStandardOutput(object logObj, object obj)
        {
            IUniversalLog log = (IUniversalLog)logObj;
            String line = obj as String;
            if (line == null)
            {
                log.ToolErrorCtx(LOG_CTX, "Non String object send to ReadStandardError");
                return;
            }

            try
            {
                ParseLine(log, line);
            }
            catch (FormatException fe) // Used to catch issues with formatting log messages
            {
                log.WarningCtx(LOG_CTX, String.Format("Error Occured while logging from stream stdout {0}", fe.ToString()));
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Exception parsing stdout stream.");
            }
        }

        /// <summary>
        /// Handle stderr while a processing is running.
        /// </summary>
        /// <param name="logObj">the log</param>
        /// <param name="obj">the string</param>
        private void ReadStandardError(object logObj, object obj)
        {
            IUniversalLog log = (IUniversalLog)logObj;
            String line = obj as String;
            if (line == null)
            {
                log.ToolErrorCtx(LOG_CTX, "Non String object send to ReadStandardError");
                return;
            }

            try
            {
                if (!String.IsNullOrWhiteSpace(line))
                {
                    log.ErrorCtx(BuildContext, line);
                    this.NumErrors[CurrentBuildStage]++;
                }
            }
            catch (FormatException fe) // Used to catch issues with formatting log messages
            {
                log.WarningCtx(LOG_CTX, String.Format("Error Occured while logging from stream stderr {0}", fe.ToString()));
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Exception parsing stderr stream.");
            }
        }
        #endregion // Private Methods
    }
}
