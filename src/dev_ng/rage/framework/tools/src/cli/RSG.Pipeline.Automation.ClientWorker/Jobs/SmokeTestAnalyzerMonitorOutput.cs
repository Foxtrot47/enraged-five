﻿using System;
using SIO = System.IO;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Engine;
using RSG.Pipeline.Content;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Build;
using RSG.Pipeline.Services;
using RSG.Pipeline.Content.Algorithm;
using RSG.Platform;
using Ionic.Zip;
using P4API;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{
    class SmokeTestAnalyzerMonitorOutput :
        ISmokeTestAnalyzer
    {
        #region     Properties
        /// <summary>
        /// Function called to begin the process of starting the game, and the main observer loop
        /// </summary>
        private SmokeTesMonitorOutputLJob Job;

        /// <summary>
        /// Variable designating whether the game is currently running or has finished
        /// </summary>
        private bool TestHasFinished;

        /// <summary>
        /// Designates if the test finished in success or failure. Should only be used/set after the game has been run.
        /// </summary>
        private bool TestSucceeded;
        #endregion  Properties

        public SmokeTestAnalyzerMonitorOutput(IDictionary<String, object> parameters, IBranch branch, BaseSmokeTestJob job,
                                  IUniversalLog log, ILogTarget logFile, CommandOptions options)
            : base(parameters, branch, job, log, logFile, options)
        {
            Job = (SmokeTesMonitorOutputLJob)job;
            TestHasFinished = false;
        }

        /// <summary>
        /// Function called to begin the process of starting the game, and the main observer loop
        /// </summary>
        /// <returns></returns>
        public override bool RunAnalysis()
        {
            using (P4 p4 = Options.Project.SCMConnect())
            {
                P4PendingChangelist pendingCL = p4.CreatePendingChangelist(String.Format("Updating IPL from SmokeTest. Job based on CL: {0}. GUID: {1}", Job.Changelist.ToString(), Job.ID.ToString()));
                SyncBuildExecutables(p4);

                if (!SyncEditFilesToOutput(p4, pendingCL.Number.ToString()))
                {
                    Log.ErrorCtx(LOG_CTX, "Errors occurred during the sync of Output files. The SmokeTest will be skipped");
                    return false;
                }

                RunTheGame();
                foreach (SmoketestTargetObserver observer in TargetObservers)
                {
                    observer.OnRunFinished += new SmoketestTargetObserver.RunFinishedDelegate(OnRunFinished);
                }
                while (!TestHasFinished)
                {
                    foreach (SmoketestTargetObserver observer in TargetObservers)
                    {
                        observer.Tick();
                    }
                    System.Threading.Thread.Sleep(10000);
                }
                ShutdownTheGame();
                return UpdateOutputFiles(p4, pendingCL.Number.ToString());
            }
        }

        /// <summary>
        /// Serialise job representation to XML.
        /// </summary>
        /// <returns></returns>
        protected bool UpdateOutputFiles(P4 p4, String pendingCLNumber)
        {
            Log.MessageCtx(LOG_CTX, "Monitoring run has finished. Handling updating files in perforce.");
            if (!TestSucceeded)
            {
                Log.MessageCtx(LOG_CTX, "Because the test errored, revert all updating files and deleting CL.");
                p4.Run("revert", "-c", pendingCLNumber);
                p4.Run("change", "-d", pendingCLNumber);
            }
            else
            {
                p4.Run("revert", "-a", "-c", pendingCLNumber);
                P4API.P4RecordSet recordSet = p4.Run("submit", "-c", pendingCLNumber);
                if (recordSet.HasErrors())
                {
                    recordSet = p4.Run("change", "-d", pendingCLNumber);
                    if (recordSet.HasErrors())
                    {
                        Log.ErrorCtx(LOG_CTX, "Errors ocurred during p4 submit command: -c {0}", pendingCLNumber);
                        return false;
                    }
                    else
                    {
                        Log.MessageCtx(LOG_CTX, "No changes to the files to upload were made. No update will be submitted");
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Sync FilesToOutput
        /// </summary>
        /// <returns></returns>
        protected bool SyncEditFilesToOutput(P4 p4, String pendingCLNum)
        {
            if (Job.FilesToUpdate.Any())
            {
                String syncArg = "";
                foreach (String file in Job.FilesToUpdate)
                {
                    syncArg += Options.Branch.Environment.Subst(file) + " ";
                }
                syncArg = syncArg.Replace("/", "\\").Trim();
                p4.Run("sync", syncArg);
                P4API.P4RecordSet recordSet = p4.Run("edit", "-c", pendingCLNum, syncArg);
                if (recordSet.HasErrors())
                {
                    Log.ErrorCtx(LOG_CTX, "Errors ocurred during p4 edit command: -c {0} {1}", pendingCLNum, syncArg);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Sync $(build)/$(branch)/... to the lastDeployedBuild
        /// </summary>
        /// <returns></returns>
        protected void SyncBuildExecutables(P4 p4)
        {
            String syncArg = String.Format("{0}\\...@{1}", Branch.Build, Job.LabelName);
            Log.MessageCtx(LOG_CTX, "Syncing {0}", syncArg);
            p4.Run("sync", syncArg);
        }

        /// <summary>
        /// Function that is called by the observer to designate that the game has come to a completed state
        /// </summary>
        /// <returns></returns>
        void OnRunFinished(LogMonitor log, ISmoketestTarget target)
        {
            if (log.HasSucceeded.HasValue)
            {
                TestSucceeded = (bool)log.HasSucceeded;
            }
            else
            {
                TestSucceeded = false;
            }
            SaveOutputFromRun(log, target);
            SendMail(log, target);
            TestHasFinished = true;
        }

        /// <summary>
        /// E-mail function called on the processor when job is finished
        /// </summary>
        /// <returns></returns>
        protected void SendMail(LogMonitor log, ISmoketestTarget target)
        {
            StringBuilder body = new StringBuilder();
            body.Append(String.Format("This test was completed by finding a {0} message.\n\n", TestSucceeded ? "SUCCESS" : "FAILURE"));
            if (log.HasFatalMessage)
            {
                body.Append("========================Fatal Message========================\n");
                body.Append("\n");
                body.Append(log.HasFatalMessage);
                body.Append("\n");
            }
            body.Append("========================All Logged Messages=======================\n");
            body.Append("\n");

            foreach (LogMessage msg in log.Messages)
            {
                body.AppendFormat("{0} - {1}", msg.Severity, msg.Message);    
            }

            String outcome = "Complete";

            if (log.HasCrash)
            {
                outcome = "CRASH";
            }
            else if (log.HasFatalMessage)
            {
                outcome = "FATAL";
            }
            else if (log.Messages.Count > 0)
            {
                outcome = "HANG";
            }

            String[] attachment = new String[0];

            String subject = String.Format("[{5} SmokeTest] {0} {1} CL{2} Failure: {3} Error Count: {4}",
                                        Branch.Project.Name, this.Platform.PlatformToRagebuilderPlatform(),
                                        this.CurrentJob.Changelist, outcome, log.Messages.Count, Job.FriendlyName);

            RSG.UniversalLog.Util.Email.SendEmail(Branch.Project.Config, MailRecipients.ToArray(),
                                                  subject, body.ToString(), attachment);

            this.Log.MessageCtx(LOG_CTX, "Mailing failure results...");
        }
    }
}