﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Services;
using System.Diagnostics;
using System.Threading;
using System.Text.RegularExpressions;
using RSG.SourceControl.Perforce;
using P4API;
using Ionic.Zip;
using RSG.Pipeline.Automation.Common.Triggers;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{

    /// <summary>
    /// Abstract base class for job processors.
    /// </summary>
    /// There are a set of helper protected methods that job processors must
    /// use to upload/merge log data.
    /// 
    internal abstract class JobProcessorBase : IJobProcessor
    {
        #region Constants
        /// <summary>
        /// Log context string.
        /// </summary>
        private static readonly String LOG_CTX = "Job Processor";

        /// <summary>
        /// Supported Build Tokens
        /// </summary>
        private static readonly String PARAM_SUPPORTED_BUILD_TOKENS = "Supported Build Tokens";

        #endregion // Constants

        #region Properties
        /// <summary>
        /// Job processor supported roles and job types.
        /// </summary>
        public CapabilityType Role
        {
            get;
            private set;
        }

        /// <summary>
        /// Configuration data object.
        /// </summary>
        public CommandOptions Options
        {
            get;
            private set;
        }

        /// <summary>
        /// File Transfer Service consumer.
        /// </summary>
        public FileTransferServiceConsumer FileTransferServiceConsumer
        {
            get;
            private set;
        }

        /// <summary>
        /// Job processor parameters (automatically loaded from %RS_TOOLSCONFIG%/automation/!name!.xml).
        /// </summary>
        public IDictionary<String, Object> Parameters
        {
            get { return m_Parameters; }
            private set { m_Parameters = value; }
        }
        protected IDictionary<String, Object> m_Parameters;

        /// <summary>
        /// Prebuild commands
        /// </summary>
        public IEnumerable<String> PrebuildCommands
        {
            get;
            private set;
        }

        /// <summary>
        /// Postbuild commands
        /// </summary>
        public IEnumerable<String> PostbuildCommands
        {
            get;
            private set;
        }

        /// <summary>
        /// For running processes via event handlers this is the stdout handling method that we wish to call.
        /// </summary>
        public Action<object, object> StdoutHandler 
        { 
            get; 
            private set; 
        }

        /// <summary>
        /// For running processes via event handlers this is the stderr handling method that we wish to call.
        /// </summary>
        public Action<object, object> StderrHandler 
        { 
            get; 
            private set; 
        }

        /// <summary>
        /// The log object that is made available to the stream handling in the event handler.
        /// </summary>
        public IUniversalLog LogForStreamProcessing 
        { 
            get; 
            private set; 
        }

        /// <summary>
        /// Build Tokens supported by the job processor
        /// </summary>
        public IEnumerable<BuildToken> SupportedBuildTokens
        {
            get;
            private set;
        }

        /// <summary>
        /// Flag to indicate the use of verbose logging
        /// </summary>
        public bool VerboseLogging
        {
            get;
            private set;
        }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="role"></param>
        /// <param name="fileTransferConsumer"></param>
        public JobProcessorBase(CommandOptions options, CapabilityType role,
            FileTransferServiceConsumer uploadConsumer, IEnumerable<String> prebuildCommands = null, IEnumerable<String> postbuildCommands = null, IEnumerable<BuildToken> supportedBuildToken = null, bool bVerboseLogging = true)
        {
            this.Options = options;
            this.Role = role;
            this.FileTransferServiceConsumer = uploadConsumer;
            this.m_Parameters = new Dictionary<String, Object>();
            this.PrebuildCommands = prebuildCommands;
            this.PostbuildCommands = postbuildCommands;
            this.VerboseLogging = bVerboseLogging;
            String filename = GetSettingsFilename();
            RSG.Base.Xml.Parameters.Load(filename, ref this.m_Parameters);

            // Store the tags that the job processor supports.
            this.SupportedBuildTokens = (supportedBuildToken != null) ? new HashSet<BuildToken>(supportedBuildToken) : new HashSet<BuildToken>();

            if (this.Parameters.ContainsKey(PARAM_SUPPORTED_BUILD_TOKENS))
            {
                IEnumerable<String> tokens = ((IEnumerable<Object>)this.Parameters[PARAM_SUPPORTED_BUILD_TOKENS]).OfType<String>();
                ICollection<BuildToken> buildTokens = new List<BuildToken>();
                foreach (String token in tokens)
                {
                    BuildToken buildToken;
                    if (Enum.TryParse<BuildToken>(token, out buildToken))
                    {
                        buildTokens.Add(buildToken);
                    }
                    else
                    {
                        Log.Log__WarningCtx(LOG_CTX, "unknown build token it attempted to be supported in {0} role : {1}", role, token);
        }
                }

                ((HashSet<BuildToken>)this.SupportedBuildTokens).UnionWith(buildTokens);
            }
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Process requested job.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public abstract IJobResult Process(IJob job);

        /// <summary>
        /// Default implementation of client suitability to run a job processor.  
        /// </summary>
        /// <returns></returns>
        public virtual bool CheckClientSuitability()
        {
            return (true);
        }

        /// <summary>
        /// Deletes a file.  log is optional
        /// </summary>
        /// <param name="log"></param>
        /// <param name="pathname"></param>
        public static void TryDeleteFile(String pathname, IUniversalLog log = null)
        {
            try
            {
                if (File.Exists(pathname))
                {
                    File.SetAttributes(pathname, FileAttributes.Normal);
                    File.Delete(pathname);
                }
            }
            catch (Exception ex)
            {
                if(log != null)
                    log.WarningCtx(LOG_CTX, "Exception whilst trying to delete {0}:{1}", pathname, ex.ToString());
            }
        }

        /// <summary>
        /// Deletes files
        /// </summary>
        /// <param name="log"></param>
        /// <param name="pathnames"></param>
        public static void TryDeleteFiles(IEnumerable<String> pathnames, IUniversalLog log = null)
        {
            foreach (String pathname in pathnames)
            {
                TryDeleteFile(pathname, log);
            }
        }

        /// <summary>
        /// Recursive deletion of directory.
        /// </summary>
        /// <param name="directory"></param>
        public static void TryDeleteDirectory(string directory, IUniversalLog log = null)
        {
            try
            {
                if (Directory.Exists(directory))
                    Directory.Delete(directory, true);
            }
            catch (System.IO.IOException)
            {
                if(log != null)
                    log.WarningCtx(LOG_CTX, String.Format("Unable to delete directory {0}.", directory));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="log"></param>
        /// <param name="directories"></param>
        public static void TryDeleteDirectories(IEnumerable<String> directories, IUniversalLog log = null)
        {
            foreach (String directory in directories)
            {
                TryDeleteDirectory(directory, log);
            }
        }
        #endregion // Controller Methods

        #region Protected Methods
        /// <summary>
        /// Copy, merge, scan for errors and upload the log data to our automation server.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="job"></param>
        /// <returns>true if there are no errors discovered in the merged logs</returns>
        protected virtual bool MergeLogsScanErrorAndUpload(IUniversalLog log, ILogTarget logTarget, IJob job)
        {
            bool result = true;
            String mergedLogFilename = CopyMergeAndCleanLogFiles(log, job);
            if (!String.IsNullOrEmpty(mergedLogFilename))
            {
                result &= !UniversalLogFile.ContainsErrors(mergedLogFilename);
                this.FileTransferServiceConsumer.UploadFile(job, mergedLogFilename);
            }
            return (result);
        }

        /// <summary>
        /// Init function for jobs.  Clears the parent process log directory currently.
        /// </summary>
        /// <param name="Job"></param>
        protected static void Init(IJob Job)
        {
            // Clears the parent process log directory.  Note that when using bat files to start up
            // automation servers and clients, that results in one top level cmd log folder which then 
            // can have multiple logs from different processes if we restart using the shutdown commands,
            // which is why this is being added.
            LogFactory.ClearLogDirectory(true);
        }

        /// <summary>
        /// Return settings XML filename.
        /// </summary>
        /// <returns></returns>
        /// This is virtual allowing you to override it if required; although
        /// it's not recommended.
        /// 
        protected virtual String GetSettingsFilename()
        {
            return (Path.Combine(this.Options.Config.ToolsConfig, "automation",
                "processors", String.Format("{0}.xml", this.GetType().Name)));
        }

        /// <summary>
        /// Copy log files into Job ID folder and merge; clean them afterwards.
        /// Return merge log filename.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="job"></param>
        /// <returns></returns>
        protected virtual String CopyMergeAndCleanLogFiles(IUniversalLog log, IJob job, bool clean = true)
        {
            String destinationDir = this.FileTransferServiceConsumer.GetClientFileDirectory(job);
            if (!Directory.Exists(destinationDir))
                Directory.CreateDirectory(destinationDir);

            String parentProcessLogDirectory = LogFactory.ParentProcessLogDirectory;
            List<String> ulogFiles = new List<String>();
            log.MessageCtx(LOG_CTX, "Parent Process Log Directory: {0}", parentProcessLogDirectory);

            if (Directory.Exists(parentProcessLogDirectory))
            {
                ulogFiles.AddRange(Directory.GetFiles(parentProcessLogDirectory, "*.ulog"));
            }

            // Copy log files to job ID directory.
            foreach (String ulogFile in ulogFiles)
            {
                try
                {
                    String srcFileName = Path.GetFileName(ulogFile);
                    log.MessageCtx(LOG_CTX, "Copying log file: {0}.", srcFileName);
                    String destinationFilename = Path.Combine(destinationDir, srcFileName);
                    if(File.Exists(destinationFilename))
                        log.WarningCtx(LOG_CTX, "Log file already exists at {0}.  Overwriting", destinationFilename);
                    File.Copy(ulogFile, destinationFilename, true);
                }
                catch (Exception ex)
                {
                    log.ToolExceptionCtx(LOG_CTX, ex, "Error copying log file: {0}.", ulogFile);
                }
            }

            // Clean log files.
            if (clean)
            {
                TryDeleteFiles(ulogFiles,log);
            }

            // Merge
            String[] uLogFilesToMerge = Directory.GetFiles(destinationDir, "*.ulog");
            String mergedLogDestinationFilename = Path.Combine(destinationDir, "job.ulog");
            UniversalLogFile.MergeLogs(mergedLogDestinationFilename, uLogFilesToMerge);

            if (!File.Exists(mergedLogDestinationFilename))
            {
                log.WarningCtx(LOG_CTX, "Merged result of {0} ulog files : {1} does not exist.", uLogFilesToMerge.Length, mergedLogDestinationFilename);
                return null;
            }

            return (mergedLogDestinationFilename);
        }

        /// <summary>
        /// Deletes files in the job folder with matching extensions, if no array is passed
        /// then all files are deleted.  Deletes the folder afterwards if it's empty, by default.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="job"></param>
        /// <param name="extensions"></param>
        /// <param name="deleteAllFiles"></param>
        protected virtual void DeleteJobFiles(IUniversalLog log, IJob job,  String[] extensions = null, bool deleteEmptyFolder = true)
        {
            String clientJobFolder = this.FileTransferServiceConsumer.GetClientFileDirectory(job);            
            String[] filesInFolder = Directory.GetFiles(clientJobFolder);
            if (extensions != null && extensions.Any())
            {
                // Match files with the specified extensions to get deleted
                String[] filesToDelete = filesInFolder.Where(file => extensions.Any(ext => file.EndsWith(ext, true, null))).ToArray();
                TryDeleteFiles(filesToDelete, log);
            }
            else
            {
                // Delete all files
                TryDeleteFiles(filesInFolder, log);
            }

            if (deleteEmptyFolder && !Directory.GetFiles(clientJobFolder).Any())
                TryDeleteDirectory(clientJobFolder, log);

        }

        /// <summary>
        /// Event handler for stdout capture
        /// - calls the handler method for stdout
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CaptureOutputEventHandler(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null && StdoutHandler != null)
            {
                StdoutHandler(LogForStreamProcessing, e.Data);
            }
        }

        /// <summary>
        /// Event handler for stderr capture
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CaptureErrorEventHandler(object sender, DataReceivedEventArgs e)
        {
            if (e.Data != null && StderrHandler != null)
            {
                StderrHandler(LogForStreamProcessing, e.Data);
            }
        }

        /// <summary>
        /// Creates a process
        /// - supports environment
        /// </summary>
        /// <param name="log"></param>
        /// <param name="job"></param>
        /// <param name="environment"></param>
        /// <param name="executable"></param>
        /// <param name="args"></param>
        /// <returns></returns>
        protected virtual ProcessStartInfo CreateProcess(IUniversalLog log, IJob job, IDictionary<String, String> environment, String executable, String args)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            
            // Set any environment settings required.
            if (environment != null)
            {
                foreach (KeyValuePair<String, String> kvp in environment)
                {
                    if (VerboseLogging)
                        log.MessageCtx(LOG_CTX, "Process Environment += '{0} = {1}'", kvp.Key, kvp.Value);
                    if (startInfo.EnvironmentVariables.ContainsKey(kvp.Key))
                    {
                        startInfo.EnvironmentVariables.Remove(kvp.Value);
                    }
                    startInfo.EnvironmentVariables.Add(kvp.Key, kvp.Value);
                }
            }

            // Ensures that build environment can know about build tokens.
            AddBuildTokensToProcessEnvironment(log, job, startInfo);

            String normalisedExecutablePath = Environment.ExpandEnvironmentVariables(this.Options.Branch.Environment.Subst(executable));
            startInfo.FileName = normalisedExecutablePath;

            // Check the build tool executable exists 
            if (!System.IO.File.Exists(startInfo.FileName))
            {
                log.ErrorCtx(LOG_CTX, "{0} : Does not exist : please review the toolchain installed on worker", startInfo.FileName);
                return null;
            }

            if (args != null)
            {
                startInfo.Arguments = args;
            }

            startInfo.UseShellExecute = false;

            return startInfo;
        }

        /// <summary>
        /// Runs a process
        /// - blocks on process completion
        /// - runs optional stdout and stderr threads to capture output streams
        /// </summary>
        /// <param name="processStartInfo"></param>
        /// <param name="stdoutFunc"></param>
        /// <param name="stderrFunc"></param>
        /// <returns></returns>
        /// 
        protected virtual System.Diagnostics.Process RunProcess(IUniversalLog log, ProcessStartInfo processStartInfo, Action<object, object> stdoutFunc, Action<object, object> stderrFunc)
        {
            log.MessageCtx(LOG_CTX, "---- Running {0} {1}", processStartInfo.FileName, processStartInfo.Arguments);

            bool origShowLogContext = LogFactory.ApplicationConsoleLog.ShowContext;
            LogFactory.ApplicationConsoleLog.ShowContext = true;

            // Set these properties so that they can be available to event handlers.
            LogForStreamProcessing = log;
            StdoutHandler = stdoutFunc;
            StderrHandler = stderrFunc;

            Process process = new Process();
            process.StartInfo = processStartInfo;

            // Add these class methods to the event handlers.
            if (stdoutFunc != null)
            {
                processStartInfo.RedirectStandardOutput = true; 
                process.OutputDataReceived += CaptureOutputEventHandler;
            }

            if (stderrFunc != null)
            {
                processStartInfo.RedirectStandardError = true;
                process.ErrorDataReceived += CaptureErrorEventHandler;                
            }

            // Start the process
                process.Start();

            // Begin Async reads of streams.
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            
            // Wait for process
            process.WaitForExit();

            // reinstate log factory settings.
            LogFactory.ApplicationConsoleLog.ShowContext = origShowLogContext;

            // Handle exitcode 
            if (process.ExitCode == 0)
            {
                log.MessageCtx(LOG_CTX, "Process {0} exited with return code {1}", processStartInfo.FileName, process.ExitCode);
            }
            else
            {
                log.ToolErrorCtx(LOG_CTX, "Process {0} exited with return code {1}", processStartInfo.FileName, process.ExitCode);
            }

            return process;
        }

        /// <summary>
        /// Prebuild commands
        /// </summary>
        /// <param name="log"></param>
        /// <param name="job"></param>
        /// <param name="environment"></param>
        /// <param name="stdout"></param>
        /// <param name="stderr"></param>
        /// <param name="origShowLogContext"></param>
        /// /// <returns>boolean if prebuild succeeded</returns>
        protected virtual bool Prebuild(IUniversalLog log, IJob job, IDictionary<String, String> environment, Action<object, object> stdout, Action<object, object> stderr)
        {
            //DW : TODO : establish environment setting during the processing of prebuild events.
            if (job.PrebuildCommands== null || !job.PrebuildCommands.Any())
            {
                log.MessageCtx(LOG_CTX, "--- No prebuild commands ---");
                return true;
            }

            log.MessageCtx(LOG_CTX, " ");
            log.MessageCtx(LOG_CTX, "<*******************************> ");
            log.MessageCtx(LOG_CTX, "<* Prebuild running {0} commands *>", job.PrebuildCommands.Count());
            log.MessageCtx(LOG_CTX, "<*******************************> ");
            log.ProfileCtx(LOG_CTX, "Prebuild");
            bool result = RunBuildCommands(log, job, environment, stdout, stderr, job.PrebuildCommands);
            log.ProfileEnd();

            if (result)
            {
                log.MessageCtx(LOG_CTX, "<*******************> ");               
                log.MessageCtx(LOG_CTX, "< Prebuild Succeded >", result ? "succeeded" : "failed");
                log.MessageCtx(LOG_CTX, "<*******************> ");
                log.MessageCtx(LOG_CTX, " ");
            }
            else
            {
                log.MessageCtx(LOG_CTX, "<*****************> ");
                log.ErrorCtx(LOG_CTX,   "< Prebuild Failed >", result ? "succeeded" : "failed");
                log.MessageCtx(LOG_CTX, "<*****************> ");
                log.MessageCtx(LOG_CTX, " ");
            }

            return result;
        }

        /// <summary>
        /// Postbuild commands
        /// </summary>
        /// <param name="log"></param>
        /// <param name="job"></param>
        /// <param name="environment"></param>
        /// <param name="stdout"></param>
        /// <param name="stderr"></param>
        /// <param name="origShowLogContext"></param>
        /// <returns></returns>
        protected virtual bool Postbuild(IUniversalLog log, IJob job, IDictionary<String, String> environment, Action<object, object> stdout, Action<object, object> stderr)
        {
            //DW : TODO : establish environment setting during the processing of postbuild events. - eg. success status etc.
            if (job.PostbuildCommands == null || !job.PostbuildCommands.Any())
            {
                log.MessageCtx(LOG_CTX, "--- No postbuild commands ---");
                return true;
            }

            log.MessageCtx(LOG_CTX, " ");
            log.MessageCtx(LOG_CTX, "<***********************************> ");
            log.MessageCtx(LOG_CTX, "< Postbuild running {0} commands *>", job.PostbuildCommands.Count());
            log.MessageCtx(LOG_CTX, "<***********************************> ");

            log.ProfileCtx(LOG_CTX, "Postbuild");
            bool result = RunBuildCommands(log, job, environment, stdout, stderr, job.PostbuildCommands);
            log.ProfileEnd();

            if (result)
            {
                log.MessageCtx(LOG_CTX, "<********************> ");
                log.MessageCtx(LOG_CTX, "< Postbuild Succeded >", result ? "succeeded" : "failed");
                log.MessageCtx(LOG_CTX, "<********************> ");
                log.MessageCtx(LOG_CTX, " ");
            }
            else
            {
                log.MessageCtx(LOG_CTX, "<******************> ");
                log.ErrorCtx(LOG_CTX,   "< Postbuild Failed >", result ? "succeeded" : "failed");
                log.MessageCtx(LOG_CTX, "<******************> ");
                log.MessageCtx(LOG_CTX, " ");
            }

            return result;
        }

        /// <summary>
        /// Runs an enumerable of commands ( called by prebuild and postbuild )
        /// - stderr and stdout actions can be used to understand what occurred.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="job"></param>
        /// <param name="environment"></param>
        /// <param name="stdout"></param>
        /// <param name="stderr"></param>
        /// <param name="commands"></param>
        /// <returns>false if the commands failed to run.</returns>
        private bool RunBuildCommands(IUniversalLog log, IJob job, IDictionary<String, String> environment, Action<object, object> stdout, Action<object, object> stderr, IEnumerable<String> commands)
        {            
            int i=1;
            foreach (String command in commands)
            {
                String commandParsed = command;
                String args = String.Empty;

                // extract executable and args from single string.
                Match match = new Regex("'(.*)'\\s(.*)", RegexOptions.IgnoreCase).Match(command);
                if (match.Groups.Count == 3)
                {
                    commandParsed = match.Groups[1].Value;
                    args = match.Groups[2].Value;
                }

                // Create build process     
                ProcessStartInfo processStartInfo = CreateProcess(log, job, environment, commandParsed, args);
                if (processStartInfo == null)
                {
                    log.ErrorCtx(LOG_CTX, "Cannot create process");
                    return false;
                }

                // Execute process
                log.MessageCtx(LOG_CTX, " ");
                log.MessageCtx(LOG_CTX, "---- Running build command {0}/{1} -----", i, commands.Count());

                Process proc = RunProcess(log, processStartInfo, stdout, stderr);

                // Handle exit code 
                if (proc.ExitCode != 0)                    
                {
                    if (i != commands.Count())
                    {
                        log.ErrorCtx(LOG_CTX, "Build commands is aborting on command {0}/{1} as non zero return code was encountered.", i, commands.Count());
                    }
                    else
                    {
                        log.ErrorCtx(LOG_CTX, "All build commands completed but non zero return code was encountered.");
                    }
                    return false;
                }

                i++;
            }
            return true;
        }

        /// <summary>
        /// Publishing is the process of copying files & optionally submitting.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="job"></param>
        /// <param name="targetDir"></param>
        /// <param name="publishDir"></param>
        /// <param name="files"></param>
        /// <param name="p4"></param>
        /// <param name="context"></param>
        /// <param name="contextDesc">The context for writing into the CL desc</param>
        /// <param name="isDev"></param>
        /// <returns></returns>
        protected virtual IEnumerable<String> Publish(IUniversalLog log, IJob job, String targetDir, String publishDir, IEnumerable<String> modifiedFiles, P4 p4, String context, String contextDesc, out int submittedCL, bool isDev = false, bool perserveFolderStructure = false, bool revertUnchanged = true)
        {
            submittedCL = (int)JobSubmissionState.NoFiles;
            // No files to publish?
            if (modifiedFiles == null || !modifiedFiles.Any())
            {
                log.MessageCtx(LOG_CTX, "No files to publish");
                return new List<String>();
            }

            // substitute special string to keep all builds unique
            publishDir = publishDir.Replace("$(publish_context)", context).TrimEnd(new[] { '/', '\\' });
            // Copy modified files to a temp directory.
            String tempDir = String.Empty;
            ICollection<String> tempFiles;

            log.MessageCtx("Target Dir: {0}", targetDir);
            string rootDir = Path.GetFullPath(new Uri(targetDir).LocalPath)
                    .TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)
                    .ToLowerInvariant();

            CopyModifiedFilesToToolsTemp(log, job, context, modifiedFiles, out tempDir, out tempFiles, perserveFolderStructure, rootDir);

            // Build the destination filenames.
            ICollection<String> publishFiles = new List<String>();
            foreach (String srcFilename in modifiedFiles)
            {
                String dstTempFilename;
                if (perserveFolderStructure)
                {
                    // Normalise these so we can do the replace
                    String srcFile = Path.GetFullPath(new Uri(srcFilename).LocalPath)
                     .TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)
                     .ToLowerInvariant();

                    targetDir = Path.GetFullPath(new Uri(targetDir).LocalPath)
                     .TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)
                     .ToLowerInvariant();

                    String srcStructure = srcFile.Replace(targetDir, String.Empty);
                    dstTempFilename = String.Format("{0}{1}", publishDir, srcStructure);
                    String parentFolder = Directory.GetParent(dstTempFilename).ToString();
                    if (!Directory.Exists(parentFolder))
                    {
                        Directory.CreateDirectory(parentFolder);
                    }
                }
                else
                {
                    dstTempFilename = Path.Combine(publishDir, Path.GetFileName(srcFilename));
                }

                publishFiles.Add(dstTempFilename);
            }

            // Create publish folder if it doesn't exist
            if (!System.IO.Directory.Exists(publishDir))
            {
                log.MessageCtx(LOG_CTX, "Creating publish directory : {0}", publishDir);
                System.IO.Directory.CreateDirectory(publishDir);
            }

            ICollection<String> addFiles = new List<String>();
            ICollection<String> editFiles = new List<String>();
            P4PendingChangelist pendingCL = null;

            // Check if mapped to workspace?
            P4API.P4RecordSet recordSet = p4.Run("where", publishDir.TrimEnd(new[] { '/', '\\' }));
            IEnumerable<String> depotFiles = p4.ParseAndLogRecordSetForDepotFilename("where", recordSet, VerboseLogging);

            // Pre-edit the depotfiles into a new CL so they can be copied onto.
            // If we have files we decide to interact with the SCM - otherwise all we do is copy without any need to submit files.
            if (depotFiles.Any())
            {
                // Create CL
                String desc = String.Format("{0} job publish : {1}", job.Role, contextDesc);
                pendingCL = p4.CreatePendingChangelist(desc);

                if (pendingCL != null)
                {
                    log.MessageCtx(LOG_CTX, " ");
                    log.MessageCtx(LOG_CTX, "*** CL {0} created. ***", pendingCL.Number);
                    log.MessageCtx(LOG_CTX, " ");
                    editFiles.Add(String.Format("-c{0}", pendingCL.Number));
                    addFiles.Add(String.Format("-c{0}", pendingCL.Number));
                }

                // Get status of files
                FileMapping[] filemappings = FileMapping.Create(p4, publishFiles.ToArray());
                String[] depotFilenames = filemappings.Select(f => f.DepotFilename).ToArray();
                FileState[] filestates = FileState.Create(p4, depotFilenames);

                // Sync to head of publish files ( needs to be forced since we can be at revison 0 and may have modified these files )
                ICollection<String> publishFilesSyncArgs = new List<String>() { "-f" };
                foreach (FileState filestate in filestates.Where(fs => fs.IsMapped && fs.HaveRevision != fs.HeadRevision))
                {
                    publishFilesSyncArgs.Add(filestate.DepotFilename);
                }

                if (publishFilesSyncArgs.Count() > 1)
                {
                    P4API.P4RecordSet recordSetSyncPublish = p4.Run("sync", publishFilesSyncArgs.ToArray());
                    p4.ParseAndLogRecordSetForDepotFilename("sync", recordSetSyncPublish, VerboseLogging);
                }

                // Compute files that are for Add or Edit
                foreach (FileMapping filemapping in filemappings)
                {
                    // If the file is being added for the first time then filestate will not return any details. 
                    // Mark the file for add.
                    if (!filestates.ToList().Any(f => f.DepotFilename.Equals(filemapping.DepotFilename,StringComparison.CurrentCultureIgnoreCase)))
                    {
                        addFiles.Add(filemapping.ExpandedLocalFilename);
                    }
                    else
                    {
                        // SCM knows about the file, so given the Filestate details determine if it should be added or edited.
                        try
                        {
                            FileState fs = filestates.First(filestate => filestate.DepotFilename.Equals(filemapping.DepotFilename, StringComparison.CurrentCultureIgnoreCase));

                            // Previously deleted or on revision 0 - mark for add - otherwise it's for edit.
                            if (fs.HeadAction == FileAction.Delete || fs.HeadRevision <= 0)
                                addFiles.Add(fs.ClientFilename);
                            else
                                editFiles.Add(fs.DepotFilename);
                        }
                        catch (P4API.Exceptions.P4APIExceptions ex)
                        {
                            log.ToolExceptionCtx(LOG_CTX, ex, "Exception : The file {0} may have not fstated as expected.", filemapping.DepotFilename); 
                        }
                    }
                }          

                if (editFiles.Count > 1)
                {
                    List<string> editedFilesWithTypes = new List<string>();
                    editedFilesWithTypes.Add("-t");
                    editedFilesWithTypes.Add("+w");
                    editedFilesWithTypes.AddRange(editFiles);

                    P4API.P4RecordSet recordSetEdit = p4.Run("edit", editedFilesWithTypes.ToArray());
                    p4.ParseAndLogRecordSetForDepotFilename("edit", recordSetEdit, VerboseLogging);

                    P4API.P4RecordSet recordSetReopen = p4.Run("reopen", editedFilesWithTypes.ToArray());
                    p4.ParseAndLogRecordSetForDepotFilename("reopen", recordSetReopen, VerboseLogging);
                }        
            }

            ICollection<String> publishedFiles = new List<String>();

            // Copy modified files into publish folder ( if that differs from the upload folder )
            int numCopied = 0;
            /*foreach (String srcFilename in tempFiles)
            {
                String dstFilename = Path.Combine(publishDir, Path.GetFileName(srcFilename));
                if (!srcFilename.Equals(dstFilename, StringComparison.CurrentCultureIgnoreCase))
                {
                    log.MessageCtx(LOG_CTX, "Copy {0} => {1}", srcFilename, dstFilename);
                    try
                    {
                        System.IO.File.Copy(srcFilename, dstFilename, true);
                        if (File.Exists(dstFilename))
                            publishedFiles.Add(dstFilename);
                        else
                            log.WarningCtx(LOG_CTX, "Publish Copy of {0} -> {1} failed", srcFilename, dstFilename);
                        numCopied++;
                    }
                    catch (Exception ex)
                    {
                        log.ToolExceptionCtx(LOG_CTX, ex, "Copying file failed {0} => {1}", srcFilename, dstFilename);
                    }
                }
            }*/

            CopyFiles(log, job, context, tempFiles, publishDir, out publishedFiles, out numCopied, perserveFolderStructure, tempDir);

            log.MessageCtx(LOG_CTX, "Publish: Copied {0} files.", numCopied);

            try
            {            
                // Now that the files have been copied form source to publish we can remove the temp dir
                if (tempDir != null)
                {
                    log.MessageCtx(LOG_CTX, "Publish: Deleting tempdir {0}.", tempDir);
                    TryDeleteDirectory(tempDir, log);
                    try
                    {
                        log.MessageCtx(LOG_CTX, "Publish: Deleting tempdir.", tempDir);
                        TryDeleteDirectory(tempDir, log);
                    }
                    catch (UnauthorizedAccessException e)
                    {
                        log.ToolExceptionCtx(LOG_CTX, e, "Failed to delete temp directory");
                    }
                
                    log.MessageCtx(LOG_CTX, "Publish: Deleting tempdir.", tempDir);
                    TryDeleteDirectory(tempDir, log);
                }
            }
            catch (Exception e)
            {
                log.ErrorCtx(LOG_CTX, "Failed to clean up temp directory {0}", tempDir);
            }

            // Construct CL 
            if (pendingCL != null)
            {
                // Now add any new depotfiles into the new CL.
                if (addFiles.Count > 1)
                {
                    List<string> addedFilesWithTypes = new List<string>();
                    addedFilesWithTypes.Add("-t");
                    addedFilesWithTypes.Add("+w");
                    addedFilesWithTypes.AddRange(addFiles);

                    P4API.P4RecordSet recordSetAdd = p4.Run("add", addedFilesWithTypes.ToArray());
                    p4.ParseAndLogRecordSetForDepotFilename("add", recordSetAdd, VerboseLogging);

                    // Some exports add the files to default. Making sure all our files are in the CL we require them to be.
                    P4API.P4RecordSet recordSetReopen = p4.Run("reopen", addedFilesWithTypes.ToArray());
                    p4.ParseAndLogRecordSetForDepotFilename("reopen", recordSetReopen, VerboseLogging);
                }

                // - Get files in changelist
                P4API.P4RecordSet parsedRecordSetOpened = p4.Run("opened", "-c", pendingCL.Number.ToString());
                IEnumerable<String> intialOpenedFiles = p4.ParseAndLogRecordSetForDepotFilename("opened", parsedRecordSetOpened, VerboseLogging);

                if (revertUnchanged)
                {
                    // Revert unchanged files.
                    log.MessageCtx(LOG_CTX, "Reverting Unchanged");
                    P4API.P4RecordSet parsedRecordSetRevert = p4.Run("revert", "-a", "-c", pendingCL.Number.ToString());
                    p4.ParseAndLogRecordSetForDepotFilename("revert", parsedRecordSetRevert, VerboseLogging);
                }

                // - Get files in changelist
                P4API.P4RecordSet parsedRecordSetOpened2 = p4.Run("opened", "-c", pendingCL.Number.ToString());
                IEnumerable<String> openedFiles = p4.ParseAndLogRecordSetForDepotFilename("opened", parsedRecordSetOpened2, VerboseLogging);

                int numRevertedFiles = intialOpenedFiles.Count() - openedFiles.Count();
                log.MessageCtx(LOG_CTX, "Revert Unchanged : Reverted {0} files. ( {1} => {2} files )", numRevertedFiles, intialOpenedFiles.Count(), openedFiles.Count());

                // - If no modified files remaining in CL no submit to do, 
                // - delete CL & return.
                if (!openedFiles.Any())
                {
                    log.MessageCtx(LOG_CTX, "No files exist in CL, no submit will take place.");
                    submittedCL = (int)JobSubmissionState.RevertUnchanged;
                    pendingCL.Delete();
                }
                else
                {
                    // Submit files.
                    if (!isDev)
                    {
                        P4API.P4UnParsedRecordSet unparsedRecordSetSubmit = pendingCL.Submit();
                        p4.LogP4RecordSetMessages(unparsedRecordSetSubmit);

                        if (unparsedRecordSetSubmit.HasErrors())
                        {
                            log.ErrorCtx(LOG_CTX, "p4 submit : errors were encountered, no files will be submitted.");
                        }
                        else
                        {
                            int submittedCLNum = p4.GetSubmittedChangelistNumber(unparsedRecordSetSubmit);
                            if (submittedCLNum <= 0)
                            {
                                submittedCLNum = pendingCL.Number;
                            }

                            submittedCL = submittedCLNum;
                            log.MessageCtx(LOG_CTX, "Publish: Submitted {0} files in CL {1}", openedFiles.Count(), submittedCLNum);
                        }
                    }
                    else
                    {
                        log.MessageCtx(LOG_CTX, "No submit will take place as we are in dev mode, CL {0} is left pending", pendingCL.Number);
                    }
                }
            }

            return publishedFiles;
        }

        /// <summary>
        /// Is a build token set - irrespecive of whether it has value(s) associated with it.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="job"></param>
        /// <param name="buildTokenName"></param>
        /// <returns>true if the tag is found</returns>
        protected bool HasBuildToken(IUniversalLog log, IJob job, BuildToken buildToken)
        {
            // Has to be supported by the job processor.
            if (!this.SupportedBuildTokens.Contains<BuildToken>(buildToken))
            {
                log.WarningCtx(LOG_CTX, "Build Token {0} is not supported by {1} job processor.", buildToken, this.Role);
                return false;
            }

            bool hasBuildToken = job.Trigger.Descriptions().Any(d => BuildTokenHelper.HasBuildToken(log,d,buildToken));
            return hasBuildToken;
        }

        /// <summary>
        /// Get the build token metadata from the job
        /// </summary>
        /// <param name="log"></param>
        /// <param name="job"></param>
        /// <param name="buildTokenName"></param>
        /// <returns>a list of the tags found, null if not found, empty if found but no values associated</returns>        
        protected IEnumerable<String> FindBuildTokens(IUniversalLog log, IJob job, BuildToken buildToken)
        {
            if (!HasBuildToken(log, job, buildToken))
            {
                return null;
            }

            List<String> buildTokenResults = new List<String>();

            IEnumerable<String> descriptions = job.Trigger.Descriptions();
            foreach (String description in descriptions)
            {
                IEnumerable<String> results = BuildTokenHelper.GetBuildTokens(log, description, buildToken);
                if (results != null)
                {
                    buildTokenResults.AddRange(results);
                }
            }

            return buildTokenResults;
        }

        /// <summary>
        /// Upload the log seperately
        /// - we wish to upload this as the very last thing we do, since build artefacts are uploaded immediately after build.
        /// - this upload will include info regarding the postbuild commands.
        /// </summary>
        /// <param name="job"></param>
        /// <param name="?"></param>
        protected virtual bool UploadLog(IJob job, IUniversalLog log, ILogTarget logFile, bool cleanLogs = false)
        {
            log.MessageCtx(LOG_CTX, " ");
            log.MessageCtx(LOG_CTX, "--- Merge and upload logfile ---");
            log.MessageCtx(LOG_CTX, "*** LOG IS NOW CLOSING : {0}", job.ID);
            logFile.Flush();
            LogFactory.CloseUniversalLogFile(logFile);
            LogFactory.CloseUniversalLog(log);

            String mergedLogFilename = CopyMergeAndCleanLogFiles(log, job, cleanLogs);
           
            if (!String.IsNullOrEmpty(mergedLogFilename))
            {
                if(FileTransferServiceConsumer.UploadFile(job, mergedLogFilename))
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Zip up the files modified
        /// </summary>
        /// <param name="zipName"></param>
        /// <param name="filesModified"></param>
        /// <returns>the zip filename generated</returns>
        protected String ZipFiles(IUniversalLog log, String zipName, IEnumerable<String> filesModified, bool directory = false)
        {
            log.MessageCtx(LOG_CTX, " ");
            log.MessageCtx(LOG_CTX, "--- ZipFiles ---");

            String fullZipFileName = Path.Combine(this.Options.Config.ToolsTemp, zipName + ".zip");

            log.MessageCtx(LOG_CTX, "Creating zip : {0}", fullZipFileName);
            using (ZipFile zip = new ZipFile())
            {
                zip.AddFiles(filesModified, @"\");
                zip.Save(fullZipFileName);
            }

            if (!System.IO.File.Exists(fullZipFileName))
            {
                log.ErrorCtx(LOG_CTX, "Zip filename does not exist {0}", fullZipFileName);
            }

            return fullZipFileName;
        }

        /// <summary>
        /// Zip up a directory
        /// </summary>
        /// <param name="zipName"></param>
        /// <param name="filesModified"></param>
        /// <returns>the zip filename generated</returns>
        protected String ZipDirectory(IUniversalLog log, String zipName, String directory)
        {
            log.MessageCtx(LOG_CTX, " ");
            log.MessageCtx(LOG_CTX, "--- ZipDirectory ---");

            if (!Directory.Exists(directory))
            {
                log.ToolErrorCtx(LOG_CTX, "Trying to add a directory that doesn't exist: {0}", directory);
                return String.Empty;
            }
            String fullZipFileName = Path.Combine(this.Options.Config.ToolsTemp, zipName + ".zip");

            log.MessageCtx(LOG_CTX, "Creating zip : {0}", fullZipFileName);
            using (ZipFile zip = new ZipFile())
            {
                zip.AddDirectory(directory);
                zip.Save(fullZipFileName);
            }

            if (!System.IO.File.Exists(fullZipFileName))
            {
                log.ErrorCtx(LOG_CTX, "Zip filename does not exist {0}", fullZipFileName);
            }

            return fullZipFileName;
        }

        /// <summary>
        /// Get the files found to be modified since the build
        /// </summary>
        /// <param name="fileInfosStart">The fileinfos of the files as found before the build</param>
        protected IEnumerable<String> FilesModified(IUniversalLog log, String targetDir, String regexTarget, IEnumerable<FileInfo> fileInfosStart)
        {
            ICollection<String> filesModified = new List<String>();

            log.MessageCtx(LOG_CTX, " ");
            log.MessageCtx(LOG_CTX, "--- FilesModified ---");

            if (String.IsNullOrEmpty(targetDir))
            {
                log.MessageCtx(LOG_CTX, "No target directory specified; no files modified.");
                return filesModified;
            }

            // Take a note of all the file timestamps
            DirectoryInfo dirInfoEnd = new DirectoryInfo(targetDir);
            IEnumerable<FileInfo> fileInfosEnd = dirInfoEnd.GetFiles();

            // now output all files that have changed
            log.MessageCtx(LOG_CTX, " ");
            log.MessageCtx(LOG_CTX, "Get modified files in {0}...", targetDir);
            log.MessageCtx(LOG_CTX, " ");
            foreach (FileInfo fiStart in fileInfosStart)
            {
                foreach (FileInfo fiEnd in fileInfosEnd.
                        Where(fi => fi.FullName == fiStart.FullName).
                        Where(fi => fi.LastWriteTimeUtc != fiStart.LastWriteTimeUtc))
                {
                    if (fiStart.Length > 0)
                    {
                        filesModified.Add(fiStart.FullName);
                        if (VerboseLogging)
                            log.MessageCtx(LOG_CTX, "File {0} was modified", fiStart);
                    }
                    else
                    {
                        log.WarningCtx(LOG_CTX, "File {0} was modified but is now empty : it will be ignored.", fiStart);
                    }
                }
            }

            foreach (FileInfo fiEnd in fileInfosEnd)
            {
                bool found = false;
                foreach (FileInfo fiStart in fileInfosStart)
                {
                    if (fiStart.FullName == fiEnd.FullName)
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    if (fiEnd.Length > 0)
                    {
                        filesModified.Add(fiEnd.FullName);
                        if (VerboseLogging)
                            log.MessageCtx(LOG_CTX, "File {0} was added", fiEnd);
                    }
                    else
                    {
                        log.WarningCtx(LOG_CTX, "File {0} was added but it is empty : it will be ignored.", fiEnd);
                    }
                }
            }
            log.MessageCtx(LOG_CTX, "{0} total files added or modified", filesModified.Count);


            String regex = regexTarget;
            if (!String.IsNullOrEmpty(regex))
            {
                Regex filter = new Regex(regex, RegexOptions.IgnoreCase);
                IEnumerable<String> filteredFilesModified = filesModified.Where(f => !filter.IsMatch(f));

                // DW: Extension methods to the rescue for no intersect!?
                IEnumerable<String> removedFiles = filteredFilesModified.Except(filesModified).Union(filesModified.Except(filteredFilesModified));

                if (removedFiles.Count() > 0)
                {
                    foreach (String removedFile in removedFiles)
                    {
                        if (VerboseLogging)
                            log.MessageCtx(LOG_CTX, "{0} was filtered out using job param '{1}'", removedFile, regexTarget);
                    }

                    log.MessageCtx(LOG_CTX, "There are now {0} files for upload.", filteredFilesModified.Count());
                    foreach (string filteredfile in filteredFilesModified)
                    {
                        if (VerboseLogging)
                            log.MessageCtx(LOG_CTX, "{0}", filteredfile);
                    }
                }

                return filteredFilesModified;
            }

            return filesModified;
        }

        /// <summary>
        /// - Determines files modified
        /// - Zips up files
        /// - Uploads files.
        /// </summary>
        /// <param name="filesModified"></param>
        protected void Upload(IUniversalLog log, IJob currentJob, String targetDir, IEnumerable<String> filesModified)
        {
            if (String.IsNullOrEmpty(targetDir))
            {
                log.MessageCtx(LOG_CTX, "No target dir; no upload");
                return;
            }

            log.ProfileCtx(LOG_CTX, "--- Upload ---");

            try
            {
                if (!filesModified.Any())
                {
                    log.WarningCtx(LOG_CTX, "No files were modified during build - no upload will take place");
                    return;
                }

                // Zips the files into a temporary zip file.
                String zipFilename = ZipFiles(log, currentJob.Role.ToString().ToLower(), filesModified);

                // Uploads files.
                if (!FileTransferServiceConsumer.UploadFile(currentJob, zipFilename))
                {
                    log.ErrorCtx(LOG_CTX, String.Format("{0} could not upload {1}",currentJob.Role.ToString(), zipFilename));
                }

                // Delete the zip filename created
                TryDeleteFile(zipFilename, log);
            }
            catch (Exception ex)
            {
                log.ErrorCtx(LOG_CTX, "Exception during codebuilder upload :  {0}", ex.ToString());
            }

            log.ProfileEnd();
        }

        /// <summary>
        /// so that they can be written to ensure the upload files are writable.
        /// - currently sycns to revision 0 ( an experimental technique ) 
        /// </summary>
        /// <returns></returns>
        protected void EnableWriteInArtefactDirectories(IUniversalLog log, P4 p4, String targetDir)
        {
            log.MessageCtx(LOG_CTX, " ");
            log.MessageCtx(LOG_CTX, "--- EnableWriteOfUploadFiles ---");

            if (String.IsNullOrEmpty(targetDir))
            {
                log.MessageCtx(LOG_CTX, "No target directory; no files to enable write.");
                return;
            }

            if (!System.IO.Directory.Exists(targetDir))
            {
                log.WarningCtx(LOG_CTX, "EnableWriteInArtefactDirectories : Target directory does not exist; no files to enable write.");
                log.WarningCtx(LOG_CTX, "Creating directory so it can be used as a target : {0}", targetDir);
                System.IO.Directory.CreateDirectory(targetDir);
            }

            try
            {
                String path = Path.Combine(targetDir, "*");

                // Revert these files - could be already edited
                P4API.P4RecordSet recordSetRevert = p4.Run("revert", path);
                p4.ParseAndLogRecordSetForDepotFilename("revert", recordSetRevert, VerboseLogging);

                // Sync to revision 0 / none - yes this is a delete, but it enables to detect when files are truly modified in concrete way.
                // - otherwise you would be reliant of builds executed previously and/or what is checked into perforce ( by the builder or users ) 
                // - this would open up too many variables and we wish to be able to have a deterministic upload, it will pay dividends later.
#warning DW: this is experimental, we may still have to qualify other files for upload - this is the easy mechanism, but there could be perf implications?
                P4API.P4RecordSet recordSetSync = p4.Run("sync", "-f", String.Format("{0}#none", path));
                p4.ParseAndLogRecordSetForDepotFilename("sync", recordSetSync, VerboseLogging);

                String[] filePaths = System.IO.Directory.GetFiles(targetDir);
                foreach (String filePath in filePaths)
                {
                    // due to what I think is a p4 bug with force syncing to revision 0 we have to take extra steps to clear this folder
                    log.WarningCtx(LOG_CTX, "Deleting {0}", filePath);
                    System.IO.File.Delete(filePath);
                }
            }
            catch (Exception ex)
            {
                log.ErrorCtx(LOG_CTX, "An exception occurred in checking out the upload dir before the codebuilder job built {0}", ex);
            }
        }

        private void CopyFiles(IUniversalLog log, IJob job, String context, IEnumerable<String> sourceFiles, String targetDir, out ICollection<String> tempFiles, out int numCopied, bool perserveFolderStructure, String rootDir)
        {
            tempFiles = new List<String>();
            numCopied = 0;
            log.MessageCtx(LOG_CTX, "Creating directory : {0}", targetDir);
            if(System.IO.Directory.Exists(targetDir))
            {
                System.IO.Directory.CreateDirectory(targetDir);
            }

            foreach (String srcFilename in sourceFiles)
            {
                String dstTempFilename;
                if (perserveFolderStructure)
                {
                    // Normalise these so we can do the replace
                    String srcFile = Path.GetFullPath(new Uri(srcFilename).LocalPath)
                     .TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)
                     .ToLowerInvariant();

                    rootDir = Path.GetFullPath(new Uri(rootDir).LocalPath)
                     .TrimEnd(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar)
                     .ToLowerInvariant();

                    if (VerboseLogging)
                    {
                        log.MessageCtx(LOG_CTX, "Root Dir {0}", rootDir);
                        log.MessageCtx(LOG_CTX, "SrcFile {0}", srcFile);
                    }

                    String srcStructure = srcFile.Replace(rootDir, String.Empty);
                    if (VerboseLogging)
                        log.MessageCtx(LOG_CTX, "Src Structure {0}", srcStructure);

                    dstTempFilename = String.Format("{0}{1}", targetDir, srcStructure);
                    if (VerboseLogging)
                        log.MessageCtx(LOG_CTX, "Destination temp folder {0}", dstTempFilename);

                    String parentFolder = Directory.GetParent(dstTempFilename).ToString();
                    if (!Directory.Exists(parentFolder))
                    {
                        Directory.CreateDirectory(parentFolder);
                    }
                }
                else
                {
                    dstTempFilename = Path.Combine(targetDir, Path.GetFileName(srcFilename));
                }

                if (VerboseLogging)
                    log.MessageCtx(LOG_CTX, "Copying file {0} => {1}", srcFilename, dstTempFilename);
                try
                {
                    System.IO.File.Copy(srcFilename, dstTempFilename, true);
                    tempFiles.Add(dstTempFilename);
                    numCopied++;
                }
                catch (Exception ex)
                {
                    log.ToolExceptionCtx(LOG_CTX, ex, "Copying file failed {0} => {1}", srcFilename, dstTempFilename);
                }
            }
        }
        #endregion // Protected Methods

        #region Private Methods
         /// Copies files to a temp folder for use a source directory from which to copy during publish.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="job"></param>
        /// <param name="context"></param>
        /// <param name="sourceDir"></param>
        /// <param name="sourceFiles"></param>
        private void CopyModifiedFilesToToolsTemp(IUniversalLog log, IJob job, String context, IEnumerable<String> sourceFiles, out String tempDir, out ICollection<String> tempFiles, bool perserveFolderStructure = false, String rootDir = "")
        {
            Guid guid = Guid.NewGuid();
            tempDir = Path.Combine(this.Options.Config.ToolsTemp, job.Role.ToString(), context, guid.ToString());
            tempFiles = new List<String>();
            log.MessageCtx(LOG_CTX, "Creating temp directory : {0}", tempDir);
            System.IO.Directory.CreateDirectory(tempDir);
            /*
            foreach (String srcFilename in sourceFiles)
            {
                String dstTempFilename = Path.Combine(tempDir, Path.GetFileName(srcFilename));
                log.MessageCtx(LOG_CTX, "Copy temp file {0} => {1}", srcFilename, dstTempFilename);
                try
                {
                    System.IO.File.Copy(srcFilename, dstTempFilename, true);
                    tempFiles.Add(dstTempFilename);
                }
                catch (Exception ex)
                {
                    log.ToolExceptionCtx(LOG_CTX, ex, "Copying temp file failed {0} => {1}", srcFilename, dstTempFilename);
                }
            }*/

            int numCopied = 0;
            CopyFiles(log, job, context, sourceFiles, tempDir, out tempFiles, out numCopied, perserveFolderStructure, rootDir);
        }
        /// <summary>
        /// Ensures that any external process's build environment can know about the build tokens.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="job"></param>
        /// <param name="startInfo"></param>
        private void AddBuildTokensToProcessEnvironment(IUniversalLog log, IJob job, ProcessStartInfo startInfo)
        {
            // Set any build tokens in the environment            
            foreach (BuildToken supportedBuildToken in this.SupportedBuildTokens)
            {
                IEnumerable<String> buildTokens = this.FindBuildTokens(log, job, supportedBuildToken);
                if (buildTokens != null && buildTokens.Any())
                {
                    // messy comma seperated values, but a script could know about whether a build token is set multiple times and handle this condition.
                    String joinedBuildTokenValues = String.Join(",", buildTokens);
                    if (VerboseLogging)
                        log.MessageCtx(LOG_CTX, "Process Environment (build token) += '{0} = {1}'", supportedBuildToken, joinedBuildTokenValues);
                    startInfo.EnvironmentVariables.Add(supportedBuildToken.ToString(), joinedBuildTokenValues);
                }
            }
        }
        #endregion  // Private Methods
    }

} // RSG.Pipeline.Automation.ClientWorker.Jobs namespace
