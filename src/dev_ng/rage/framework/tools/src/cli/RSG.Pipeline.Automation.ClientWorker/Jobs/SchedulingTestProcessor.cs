﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Interop.Autodesk3dsmax;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Services;
using RSG.SourceControl.Perforce;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{

    /// <summary>
    /// Scheduling test job processor.
    /// </summary>
    internal class SchedulingTestProcessor :
        JobProcessorBase,
        IJobProcessor
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public SchedulingTestProcessor(CommandOptions options, FileTransferServiceConsumer uploadConsumer)
            : base(options, CapabilityType.SchedulingUnitTesting, uploadConsumer)
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Process requested job.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public override IJobResult Process(IJob job)
        {
            Debug.Assert(job is Job, "Invalid job for processor.");
            if (!(job is Job))
            {
                return null;
            }

            Debug.Assert(job.Trigger is IChangelistTrigger, "Invalid Changelist Trigger for job.");
            if (!(job.Trigger is IChangelistTrigger))
            {
                return null;
            }

            Init(job);

            JobResult jobResult = new JobResult(job.ID, false);

            IUniversalLog log = LogFactory.CreateUniversalLog("process");
            ILogTarget logFile = LogFactory.CreateUniversalLogFile("process",
                log, "automation", job.ID.ToString()) as ILogTarget;

#warning DW: this looks wrong did you mean to check if this is a filestrigger?
            ChangelistTrigger trigger = (ChangelistTrigger)job.Trigger;
            foreach (String file in trigger.Files)
                log.Message("File: {0} changed.", file);
            System.Threading.Thread.Sleep(1000);
            logFile.Flush();

            String mergedLogFilename = CopyMergeAndCleanLogFiles(log, job);

            bool hasErrors = false;
            if (String.IsNullOrEmpty(mergedLogFilename))
            {
                hasErrors = UniversalLogFile.ContainsErrors(mergedLogFilename);

                // UploadFile log.
                this.FileTransferServiceConsumer.UploadFile(job, mergedLogFilename);
            }

            jobResult.Succeeded = !hasErrors;
            return jobResult;
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Automation.ClientWorker.Jobs namespace
