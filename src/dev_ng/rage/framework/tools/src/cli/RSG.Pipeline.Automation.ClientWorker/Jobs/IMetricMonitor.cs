﻿using System;
using SIO = System.IO;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Platform;
using RSG.UniversalLog.Util;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{
    abstract class IMetricMonitor
    {
        const int NEWBASELEVEL = 100;

        #region Constants
        protected static readonly String LOG_CTX = "Metric Monitor";
        private static readonly String METRICS_SEARCH_WILDCARD = "Metrics Search Wildcard";
        private static readonly String LOCATION_NAMES = "Location Names";
        private static readonly String METRICS_CSV_FILENAME = "Metrics CSV Filename";
        private static readonly String DB_NAME = "Database Name";
        private static readonly String DB_SERVER_NAME = "Database Server Name";
        private static readonly String DUMP_TO_DB = "Dump to Database";
        #endregion

        #region Properties

        protected bool DumpToDB;
        protected String MetricName;
        protected String MetricsSearchWildcard;
        protected String MetricsCSVFilename;
        protected String MetricsDirectory;
        protected String DBName;
        protected String DBServerName;
        protected RSG.Platform.Platform Platform;

        protected bool[] LocationProcessed;
        protected int[] LocationMetricsScore;
       
        protected String[] LocationLastProcessed;
        protected DateTime[] LocationLastProcessedDataTime;
        protected IPerfMetrics[] LocationPerfMetrics;
        protected IEnumerable<String> LocationNames = null;
        protected IList<String> MailRecipients = null;
      
        protected StringBuilder Subject;
        protected StringBuilder MetricsSubject;
        protected StringBuilder Body;

        protected IPerfMetrics Thresholds;
        protected IUniversalLog Log;
        protected IBranch Branch;

        protected IList<ISmoketestTarget> Targets;
        protected CommandOptions Options;

        #endregion

        #region Constructors
        /// <summary>
        /// Constructor.
        /// </summary>
        /// 
        public IMetricMonitor(CommandOptions options, IDictionary<String, Object> parameters, RSG.Platform.Platform platform, 
                              String exePath, IUniversalLog log, IBranch branch, 
                              IList<String> mailRecipients, IList<ISmoketestTarget> targets)
        {
            MetricName = "IMetricMonitor";
            Log = log;
            Branch = branch;
            MailRecipients = mailRecipients;
            Targets = targets;
            Options = options;

            if (parameters.ContainsKey(METRICS_SEARCH_WILDCARD))
            {
                this.MetricsSearchWildcard = (String)parameters[METRICS_SEARCH_WILDCARD];
            }

            if (parameters.ContainsKey(METRICS_CSV_FILENAME))
            {
                this.MetricsCSVFilename = (String)parameters[METRICS_CSV_FILENAME];
            }

            if (parameters.ContainsKey(LOCATION_NAMES))
            {
                String theDir = (String)parameters[LOCATION_NAMES];
                this.LocationNames = new List<String>(theDir.Split(';'));
            }          

            if (parameters.ContainsKey(DB_NAME))
            {
                this.DBName = (String)parameters[DB_NAME];
            }

            if (parameters.ContainsKey(DB_SERVER_NAME))
            {
                this.DBServerName = (String)parameters[DB_SERVER_NAME];
            }

            if (parameters.ContainsKey(DUMP_TO_DB))
            {
                this.DumpToDB = (bool)parameters[DUMP_TO_DB];
            }
            else
            {
                this.DumpToDB = false;
            }

            int numLocations = LocationNames.Count();

            this.Platform = platform;

            //should give us x:\rdr\build\dev\smoke\win64 etc
            MetricsDirectory = System.IO.Path.Combine(Branch.Build,
                                                      "smoke", //@TODO this should come from .xml file, also used as commandline
                                                      this.Platform.PlatformToRagebuilderPlatform());

            LocationProcessed = new bool[numLocations];
            LocationMetricsScore = new int[numLocations];
            LocationLastProcessed = new String[numLocations];
            LocationLastProcessedDataTime = new DateTime[numLocations];
            LocationPerfMetrics = new PerfMetrics[numLocations];
        }
        #endregion

        public abstract void AllocatePerfMetrics();

        protected abstract String AppendMailSubject(int locationIndex);
        public abstract String FailMailFirstCrash(StringBuilder emailBody);
        public abstract void FailMailRemainders(StringBuilder emailBody);

        /// <summary>
        /// Scan for existing perf files to establish base numbers to diff subsequent runs off of
        /// </summary>
        /// <returns></returns>
        public bool DetermineBaseline()
        {
            //init'd to false
            bool[] baseline = new bool[this.LocationNames.Count()];

            for (int location = 0; location < LocationNames.Count(); location++)
            {
                DateTime mostRecentDateTime;
                String mostRecent;
                String locationWildcard = LocationNames.ElementAt(location) + MetricsSearchWildcard;
                if (FindMostRecentFile(MetricsDirectory, locationWildcard, out mostRecent, out mostRecentDateTime))
                {
                    // if the filename of the most recent has changed, or the timestamp of the most 
                    // recent has changed
                    if (mostRecent != LocationLastProcessed[location] ||
                        mostRecentDateTime != LocationLastProcessedDataTime[location])
                    {
                        PerfMetrics newPerf = new PerfMetrics(DBServerName, DBName, Log);

                        // load the file and extract the perf summary metrics
                        if (ProcessFile(mostRecent, newPerf))
                        {
                            // compare the new file's metrics to the last metrics found for this town
                            int score = CompareMetrics(LocationPerfMetrics[location], newPerf,
                                                        LocationNames.ElementAt(location));

                            Debug.Assert(score == NEWBASELEVEL);
                            baseline[location] = true;

                            // update our latest metrics with the new one
                            LocationPerfMetrics[location] = newPerf;

                            // remember that we just processed this file with this datetime
                            LocationLastProcessed[location] = mostRecent;
                            LocationLastProcessedDataTime[location] = mostRecentDateTime;

                            LocationMetricsScore[location] = score;
                        }
                    }
                }
            }
            for (int location = 0; location < LocationNames.Count(); location++)
            {
                if (!baseline[location])
                {
                    this.Log.MessageCtx(LOG_CTX, "Failed to determine baseline values for {0}...",
                                        LocationNames.ElementAt(location));
                    return false;
                }
            }
            this.Log.MessageCtx(LOG_CTX, "Baseline values complete.");

            return true;
        }

        /// <summary>
        /// Search for metric files that the game has just output.  Return true if all location metrics are found.
        /// </summary>
        /// <returns></returns>
        public bool SearchForMetrics()
        {
            //@TODO debug only switch
            bool forceNewFilesFound = false;

            for (int location = 0; location < LocationNames.Count(); location++)
            {
                DateTime mostRecentDateTime;
                String mostRecent;
                String locationWildcard = LocationNames.ElementAt(location) + MetricsSearchWildcard;

                if (FindMostRecentFile(MetricsDirectory, locationWildcard, out mostRecent, out mostRecentDateTime))
                {
                    // if the filename of the most recent has changed, or the timestamp of the most 
                    // recent has changed
                    if (mostRecent != LocationLastProcessed[location] ||
                        mostRecentDateTime != LocationLastProcessedDataTime[location] ||
                        forceNewFilesFound)
                    {
                        PerfMetrics newPerf = new PerfMetrics(DBServerName, DBName, Log);

                        // load the file and extract the perf summary metrics
                        if (ProcessFile(mostRecent, newPerf))
                        {
                            // compare the new file's metrics to the last metrics found for this town
                            int score = CompareMetrics(LocationPerfMetrics[location], newPerf,
                                                        LocationNames.ElementAt(location));

                            Debug.Assert(score != NEWBASELEVEL);
                            LocationProcessed[location] = true;

                            // update our latest metrics with the new one
                            LocationPerfMetrics[location] = newPerf;

                            // remember that we just processed this file with this datetime
                            LocationLastProcessed[location] = mostRecent;
                            LocationLastProcessedDataTime[location] = mostRecentDateTime;

                            LocationMetricsScore[location] = score;
                        }
                    }
                }
            }

            for (int location = 0; location < LocationNames.Count(); location++)
            {
                if (!LocationProcessed[location])
                    break;

                if (location == (LocationNames.Count() - 1))
                {
                    return true;
                 
                }
            }
            return false;
        }

        /// <summary>
        /// Assuming we've found all metrics, publish them to the DB and mail out the results.
        /// </summary>
        /// <returns></returns>
        public void PublishResults()
        {
            Subject.Append(AppendMailSubject(0));

            SaveMetricsCSV();
            SaveMetricsDB();

            MailReport(LocationMetricsScore, LocationNames.Count(), LocationPerfMetrics[0].CL);
        }

        /// <summary>
        /// Assigns crashed and test complete to each location based on new metrics being found
        /// </summary>
        /// <returns></returns>
        public int MeasureSuccess()
        {
            int numSuccess = 0;
            bool crashFound = false;
            for (int location = 0; location < LocationNames.Count(); location++)
            {
                if (!LocationProcessed[location])
                {
                    //identify the first crash
                    if (!crashFound)
                    {
                        LocationPerfMetrics[location].Crashed = true;
                        LocationPerfMetrics[location].TestComplete = false;
                        crashFound = true;
                    }
                    else
                    {
                        //not tested
                        LocationPerfMetrics[location].TestComplete = false;
                    }
                }
                else
                {
                    numSuccess++;
                    LocationPerfMetrics[location].Crashed = false;
                    LocationPerfMetrics[location].TestComplete = true;
                }
            }
            return numSuccess;
        }

        private void SaveMetricsDB()
        {
            if (this.DumpToDB)
            {
                this.Log.MessageCtx(LOG_CTX, "Dumping metrics to DB");

                String pcMachineName = "";
                for (int location = 0; location < LocationNames.Count(); location++)
                {
                    //@TODO assume the 1st target machine here; should have a target index for this run; only an issue for mptest
                    String insertResult = LocationPerfMetrics[location].DumpToDB(LocationNames,
                                                                                 LocationNames.ElementAt(location),
                                                                                 Targets[0]);
                    Body.AppendLine(insertResult + "\n");

                    if (location == 0 && Targets[0] is SmoketestTargetx64)
                    {
                        String[] results = insertResult.Split(' ');
                        pcMachineName = results[results.Length - 1];
                    }
                }

                //for pc testing, add the flavor of PC to the subject line
                if (Targets[0] is SmoketestTargetx64)
                {
					//String namePlatform = String.Format("{0}-{1}", Targets[0].GetMachineName(), pcMachineName);
                    Subject.Replace(this.Platform.PlatformToRagebuilderPlatform(), pcMachineName);
                }
            }
            else
            {
                this.Log.WarningCtx(LOG_CTX, "No metrics dumped to DB.  DB insertion prevented by 'Dump to DB' config.");
            }
        }

        private void SaveMetricsCSV()
        {
            String filename = String.Format("{0}_{1}.csv", this.MetricsCSVFilename, this.Platform.PlatformToRagebuilderPlatform());

            bool writeHeader = false;
            if (System.IO.File.Exists(filename))
            {
                if (new FileInfo(filename).Length == 0)
                {
                    // file is empty
                    writeHeader = true;
                }
            }
            else
            {
                writeHeader = true;
            }
            this.Log.MessageCtx(LOG_CTX, "Dumping metrics to CSV {0} at {1} header: {2}.",
                                    filename, DateTime.Now.ToString(), writeHeader);

            using (FileStream fs = new FileStream(filename, FileMode.Append, FileAccess.Write))
            using (StreamWriter fileStream = new StreamWriter(fs))
            {
                for (int location = 0; location < LocationNames.Count(); location++)
                {
                    if (writeHeader)
                    {
                        LocationPerfMetrics[location].DumpHeaderToCSV(fileStream);
                        writeHeader = false;
                    }
                    LocationPerfMetrics[location].DumpToCSV(LocationNames.ElementAt(location), fileStream);
                }

                fileStream.Close();
            }
        }

        private bool FindMostRecentFile(String searchPath, String searchWilecard, out String outFilename, out DateTime outDateTime)
        {
            // this.Log.MessageCtx(LOG_CTX, "FindMostRecentFile looking for a new {0} file", searchPath);
            String[] filePaths = null;

            //if our path doesn't exist, try to create it once; bail if that doesn't work
            if (!System.IO.Directory.Exists(searchPath))
            {
                this.Log.WarningCtx(LOG_CTX, "Directory {0} not found. Trying to CreateDirectory...", searchPath);

                try
                {
                    System.IO.Directory.CreateDirectory(searchPath);
                }
                catch (System.Exception ex)
                {
                    this.Log.ErrorCtx(LOG_CTX, "System.IO.Directory.CreateDirectory exception {2} trying to create {0}{1}",
                                      searchPath, ex.Message.ToString());

                    outFilename = "not found";
                    outDateTime = DateTime.MinValue;
                    return false;
                }
                this.Log.WarningCtx(LOG_CTX, "Succeeded in CreateDirectory {0}.  Continuing...", searchPath);
            }

            try
            {
                filePaths = System.IO.Directory.GetFiles(searchPath, searchWilecard);
            }
            catch (System.Exception ex2)
            {
                //this.Log.ToolException();
                this.Log.ErrorCtx(LOG_CTX, "System.IO.Directory.GetFiles exception {2}.  Can\'t find {0}{1}",
                                            searchPath, searchWilecard, ex2.Message.ToString());
                outFilename = "not found";
                outDateTime = DateTime.MinValue;
                return false;
            }

            DateTime lastWritten = DateTime.MinValue;
            String lastWrittenFile = null;
            foreach (String fn in filePaths)
            {
                DateTime lastWriteTime = System.IO.File.GetLastWriteTime(fn);
                if (lastWriteTime > lastWritten)
                {
                    // this.Log.MessageCtx(LOG_CTX, "FindMostRecentFile found {0} @ {1}", fn, lastWriteTime);
                    lastWritten = lastWriteTime;
                    lastWrittenFile = fn;
                }
            }

            outFilename = lastWrittenFile;
            outDateTime = lastWritten;

            if (lastWritten != DateTime.MinValue)
            {
                this.Log.MessageCtx(LOG_CTX, "FindMostRecentFile found {0} @ {1}", outFilename, outDateTime);
                return true;
            }
            return false;
        }

        protected abstract bool ProcessFile(String filename, PerfMetrics outPerf);

        public static UInt64 GetNodeValueAverage(XElement node)
        {
            foreach (XElement child in node.Nodes())
            {
                if (String.Equals(child.Name.ToString(), "average", StringComparison.CurrentCultureIgnoreCase))
                {
                    // we end up with value=\"32482134.7324\"
                    String[] valueEquals = child.Attribute("value").ToString().Split('=');
                    UInt64 actualValue = Convert.ToUInt64(valueEquals[1].Replace("\"", ""));
                    return actualValue;
                }
            }
            return 0;
        }

        public static UInt64 GetNodeValue(XElement node)
        {
            String[] valueEquals = node.Attribute("value").ToString().Split('=');
            UInt64 actualValue = Convert.ToUInt64(valueEquals[1].Replace("\"", ""));
            return actualValue;
        }

        public static int GetNodeValueInt(XElement node)
        {
            String[] valueEquals = node.Attribute("value").ToString().Split('=');
            int actualValue = Convert.ToInt32(valueEquals[1].Replace("\"", ""));
            return actualValue;
        }

        protected abstract int CompareMetrics(IPerfMetrics existingPerf, IPerfMetrics newPerf, String locationName);

        protected int CompareMetric64(UInt64 existing, UInt64 newData, UInt64 threshold, String metricName, 
                                      String subType, bool biggerIsBetter = false)
        {
            Int64 sExisting = Convert.ToInt64(existing);
            Int64 sNewData = Convert.ToInt64(newData);
            Int64 delta = sExisting - sNewData;
            UInt64 absDelta = Convert.ToUInt64(Math.Abs((delta)));
            String name = String.Format("{0} {1}", metricName, subType);
            String line;
            if (absDelta > threshold)
            {
                if (!biggerIsBetter)
                    delta *= -1;

                if (delta < 0)
                {
                    line = String.Format("++++{0} improved by {1}.\n", name, absDelta);
                    Body.Append(line);
                    return 1;
                }
                else
                {
                    line = String.Format("----{0} declined by {1}.\n", name, absDelta);
                    Body.Append(line);
                    return -10;
                }
            }
            return 0;
        }

        protected int CompareMetric64(DBBigIntData existing, DBBigIntData newData, UInt64 threshold, String metricName,
                                    String subType, bool biggerIsBetter = false)
        {
            return CompareMetric64(existing.Value, newData.Value, threshold, metricName,
                                    subType, biggerIsBetter);
        }

        protected int CompareMetric(float existing, float newData, float threshold, String metricName, 
                                    String subType, bool biggerIsBetter = false)
        {
            float delta = existing - newData;
            float absDelta = Math.Abs((delta));
            String name = String.Format("{0} {1}", metricName, subType);
            String line;
            if (absDelta > threshold)
            {
                if (!biggerIsBetter)
                    delta *= -1;

                if (delta < 0)
                {
                    line = String.Format("++++{0} improved by {1}.\n", name, absDelta);
                    Body.Append(line);
                    return 1;
                }
                else
                {
                    line = String.Format("----{0} declined by {1}.\n", name, absDelta);
                    Body.Append(line);
                    return -10;
                }
            }
            return 0;
        }

        private void MailReport(int[] metricsScore, int numLocations, int CL)
        {
            int numFails = 0;
            for (int location = 0; location < numLocations; location++)
            {
                if (metricsScore[location] < 0)
                {
                    numFails++;
                }
            }

			//@TODO need new scoring system
            Subject.Append(MetricsSubject);

            MailResults(Subject.ToString(), Body.ToString());

            this.Log.MessageCtx(LOG_CTX, Subject.ToString());
            this.Log.MessageCtx(LOG_CTX, Body.ToString());
        }

        private void MailResults(String subject, String body)
        {
            // TODO might be best to use the PostJobNotification system though that would require a refactor
            // as it gives less control over when and what content is sent
            // TODO mail comes from the current user; might be better to be from "[project_name] Performance Metrics"
            String[] attachment = new String[0];
            
            //exception here for our calling thread not being sta if tools install doesn't have an email addr
            RSG.Base.Configuration.Email.Email.SendEmail(Options.Config, MailRecipients.ToArray(), 
                                                            subject, body, attachment);

            this.Log.MessageCtx(LOG_CTX, "Mailing results...");
        }
    } 
}
