﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{
    interface ISmoketestTarget
    {
        String IpAddress
        {
            get;
        }

        String LogFilePath
        {
            get;
        }

        // Only valid after you finish the launch thread. Starting a new launch invalidates this.
        LogMonitor TakeMonitor();

        bool IsLaunching
        {
            get;
        }

        bool HasAttemptedLaunch
        {
            get;
        }

        int RunIndex
        {
            get;
        }

        bool CanLaunch();
        bool IsRunning(String executable);
        void Relaunch();
        void Launch(SmoketestTargetConsoleParameters param);
        void Shutdown(String param);
    }
}
