﻿using System;
using SIO = System.IO;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Automation.Common;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{
    /// <summary>
    /// Smoke Test job processor.
    /// </summary>
    internal class SmokeTestJobProcessor :
        JobProcessorBase,
        IJobProcessor
    {
        #region Constants
      
        protected static readonly String LOG_CTX = "Smoke Test Job Processor";
           
        #endregion

        #region Properties

        private IConfig Config;

        /// <summary>
        /// Universal log object
        /// </summary>
        IUniversalLog Log { get; set; }

        /// <summary>
        /// Universal log file
        /// </summary>
        ILogTarget LogFile { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="uploadConsumer"></param>
        /// 
        public SmokeTestJobProcessor(CommandOptions options, FileTransferServiceConsumer uploadConsumer)
            : base(options, CapabilityType.SmokeTester, uploadConsumer)
        {
            Config = options.Config;
        }

        #endregion

        #region Controller Methods
        /// <summary>
        /// Process requested job.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public override IJobResult Process(IJob job)
        {
            //@TODO need our own job result??
            JobResult jobResult = new JobResult(job.ID, false);

            Debug.Assert((job.Role & this.Role) != CapabilityType.None, String.Format("Invalid job for processor: {0}.", job.GetType().Name));
            if ((job.Role & this.Role) == CapabilityType.None)
            {
                return null;
            }

            this.Log = LogFactory.CreateUniversalLog("process");
            this.LogFile = LogFactory.CreateUniversalLogFile(
                String.Format("process_{0}", job.ID), Log) as ILogTarget;

            BaseSmokeTestJob smoketestJob = (BaseSmokeTestJob)job;

            this.Log.MessageCtx(LOG_CTX,
                               "Smoke Test job found analysisType:{0} branch:{1} file:{2} cl:{3} cbcl:{4}",
                                smoketestJob.AnalysisType, smoketestJob.BranchName, smoketestJob.ExePath,
                                smoketestJob.Changelist, smoketestJob.CodeBuilderChangelist);

            ISmokeTestAnalyzer analyzer = null;
            IBranch branch = Config.Project.Branches[smoketestJob.BranchName];
            switch (smoketestJob.AnalysisType)
            {
                case SmokeTesterAnalysisType.QuickMetrics:
                    analyzer = new SmokeTestAnalyzerMetrics(Parameters, branch, smoketestJob, Log, LogFile, Options);
                    break;

                case SmokeTesterAnalysisType.MPSmoketest:
                    analyzer = new SmokeTestAnalyzerMP(Parameters, branch, smoketestJob, Log, LogFile, Options);
                    break;

                case SmokeTesterAnalysisType.MonitorOutput:
                    analyzer = new SmokeTestAnalyzerMonitorOutput(Parameters, branch, smoketestJob, Log, LogFile, Options);
                    break;
            }
            jobResult.Succeeded = analyzer.RunAnalysis();

            this.Log.MessageCtx(LOG_CTX, "End of Smoke Test job.  Success:{0}", jobResult.Succeeded);

            return jobResult; 
        }
        #endregion
    }
}
