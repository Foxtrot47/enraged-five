﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Threading;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Client;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Services;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Services;
using RSG.Pipeline.Automation.Common.Services.Messages;
using System.IO;

namespace RSG.Pipeline.Automation.ClientWorker.Services
{

	/// <summary>
	/// Automation Worker Service.
	/// </summary>
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
	internal class AutomationWorkerService : 
		IAutomationWorkerService,
        IAutomationWorkerAdminService
	{
		#region Constants
		/// <summary>
		/// Update thread tick in milliseconds.
		/// </summary>
		private static readonly int UPDATE_TICK = 100;

        /// <summary>
        /// Rate at which a work will attempt to retry to register in seconds
        /// </summary>
        private static readonly int WORKER_REGISTER_RETRY_RATE_SECONDS = 10;

        /// <summary>
        /// Period between server checks (when not processing a job)
        /// </summary>
        private static readonly int SERVER_CONNECTION_CHECK_PERIOD_SECONDS = 60;

		/// <summary>
		/// Log context.
		/// </summary>
		private static readonly String LOG_CTX = "Worker";
		#endregion // Constants

		#region Properties
		/// <summary>
		/// Command options object.
		/// </summary>
		private CommandOptions Options
		{
			get;
			set;
		}

		/// <summary>
		/// Current worker state.
		/// </summary>
		private WorkerState State
		{
			get;
			set;
		}

		/// <summary>
		/// Worker capabilities.
		/// </summary>
		private CapabilityType Role
		{
			get;
			set;
		}

		/// <summary>
		/// Current job queue (should only really have one entry).
		/// </summary>
		private Queue<IJob> Jobs
		{
			get;
			set;
		}

		/// <summary>
		/// Job processors (matching the role flags).
		/// </summary>
		private ICollection<IJobProcessor> JobProcessors
		{
			get;
			set;
		}

		/// <summary>
		/// Automation Service consumer.
		/// </summary>
		private AutomationServiceConsumer AutomationConsumer
		{
			get;
			set;
		}

		/// <summary>
		/// UploadFile Service AutomationConsumer.
		/// </summary>
		private FileTransferServiceConsumer FileTransferConsumer
		{
			get;
			set;
		}

		/// <summary>
		/// 
		/// </summary>
		private ServiceHost WorkerHost
		{
			get;
			set;
		}

        private Thread JobProcessThread
        {
            get;
            set;
        }

		/// <summary>
		/// Flag whether the worker service is running 
        /// - public & shared by main thread ( hence volatile bool & lock )
        /// could be thread safety overkill. 
        /// there must be more elegant ways... safety first though.
		/// </summary>
        private volatile bool m_isRunning;
        private object m_isRunningLockObject = new Object();
        public bool IsRunning
		{
            get
            {
                lock (m_isRunningLockObject) 
                { 
                    return m_isRunning; 
                }  
            }
            set 
            {
                lock (m_isRunningLockObject) 
                { 
                    m_isRunning = value; 
                }  
            }
		}

		/// <summary>
		/// Log object.
		/// </summary>
		private IUniversalLog Log
		{
			get;
			set;
		}

        /// <summary>
        /// The type of Shutdown if in progress.
        /// </summary>
        public ShutdownType ShutDownType 
        { 
            get; 
            private set; 
        }

        /// <summary>
        /// Flags the cache for clear at next available opportunity
        /// </summary>
        private bool ClearCacheFlagged { get; set; }

		#endregion // Properties

		#region Constructor(s)
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="config"></param>
		/// <param name="serviceAutomation"></param>
		/// <param name="serviceFileTransfer"></param>
		/// <param name="role"></param>
		public AutomationWorkerService(CommandOptions options, 
			String serviceAutomation, String serviceFileTransfer, CapabilityType role)
		{
            this.ClearCacheFlagged = false;
            this.ShutDownType = ShutdownType.None;
			this.Options = options;
			this.State = WorkerState.Idle;
			this.Role = role;
            this.JobProcessThread = null;
			this.Jobs = new Queue<IJob>();
			this.JobProcessors = new List<IJobProcessor>();
			if (!String.IsNullOrEmpty(serviceAutomation))
				this.AutomationConsumer = new AutomationServiceConsumer(new Uri(serviceAutomation));
			if (!String.IsNullOrEmpty(serviceFileTransfer))
				this.FileTransferConsumer = new FileTransferServiceConsumer(this.Options.Config, new Uri(serviceFileTransfer));
			this.IsRunning = false;
			this.Log = LogFactory.CreateUniversalLog("Automation Worker");

			// Create Service Host.
			Log.Message("WCF Host Startup.");
            try
            {
                // Port/binding definition for the service is defined in app.config. 
                this.WorkerHost = new ServiceHost(this);
                this.WorkerHost.Open();
            }
            catch(AddressAlreadyInUseException e)
            {
                // This is to handle the case were there are 2 instances of the worker trying to run. There is
                // and edge case which the port doesn't get released and you have to restart the machine.
                Log.ToolExceptionCtx(LOG_CTX, e, "Port already in use : please close any other instances of the worker process. If this persists a machine reboot will be required.");
                Log.WarningCtx(LOG_CTX, "Press Enter to terminate the worker");
                Console.ReadLine();
                throw e;
            }

			// Initialise job processors.
			Assembly assm = Assembly.GetEntryAssembly();
			foreach (Type type in assm.GetTypes())
			{
				if (type.IsAbstract)
					continue;

				if (typeof(IJobProcessor).IsAssignableFrom(type))
				{
					IJobProcessor processor = (IJobProcessor)Activator.CreateInstance(                        
						type, options, this.FileTransferConsumer);
					if (role.HasFlag(processor.Role) && processor.CheckClientSuitability())
					{
						Log.Message("Job Processor: {0} activated.", type.ToString());
						this.JobProcessors.Add(processor);
					}
				}
			}

            if (this.JobProcessors.Count == 0)
            {
                Log.Warning("No job processors have been initialized with this worker.");
            }

		}
		#endregion // Constructor(s)

		#region IAutomationWorkerService Methods
		/// <summary>
		/// Request current client state.
		/// </summary>
		/// <returns></returns>
		public WorkerState GetState()
		{
			this.Log.DebugCtx(LOG_CTX, "AutomationWorkerService::GetState()");
			return (this.State);
		}

		/// <summary>
		/// Request to process a particular job.
		/// </summary>
		/// <param name="job"></param>
		/// <returns></returns>
		public bool Process(IJob job)
		{
			this.Log.DebugCtx(LOG_CTX, "AutomationWorkerService::Process()");
			if (WorkerState.Busy == this.State)
			{
				this.Log.WarningCtx(LOG_CTX, "Ignoring job request {0} as worker is busy.",
					job.ID);
				return (false);
			}

			// Validate we have a processor for the job.
			IEnumerable<IJobProcessor> processors =
				this.JobProcessors.Where(p => p.Role.HasFlag(job.Role));
			Debug.Assert(processors.Count() > 0,
				"No job processor found.",
				"No job processor for job type: {0} {1}.", job.GetType(), job.Role);
			if (0 == processors.Count())
			{
				this.Log.Error("No job processor for job type: {0} {1}.",
					job.GetType(), job.Role);
				return (false);
			}

			// Enqueue job; seems valid.
			this.Log.MessageCtx(LOG_CTX, "Enqueuing Job {0}.", job.ID);
			lock (this.Jobs)
			{
				this.State = WorkerState.Busy;
				this.Jobs.Enqueue(job);
			}
			return (true);
		}

		/// <summary>
		/// Request to abort particular job.
		/// </summary>
		/// <param name="job"></param>
		/// <returns></returns>
		public bool Abort(IJob job)
		{
			this.Log.DebugCtx(LOG_CTX, "AutomationWorkerService::Abort()");
			throw new NotImplementedException();
		}
		#endregion // IAutomationWorkerService Methods

        #region IAutomationWorkerAdminService Methods

        /// <summary>
        /// Request to shutdown the worker.
        /// </summary>
        /// <param name="shutdownType"></param>
        /// <returns></returns>
        public void Shutdown(ShutdownType shutdownType)
        {
            // Ignore shutdown if not directed at workers.
            if (!shutdownType.HasFlag(ShutdownType.WorkersAndHost))
            {
                return;
            }

            this.Log.DebugCtx(LOG_CTX, "AutomationWorkerAdminService::Shutdown()");

            // already shutting down - change shutdowntype if it differs - otherwise wait...
            if (this.State == WorkerState.ShuttingDown)
            {
                if (ShutDownType == shutdownType)
                {
                    this.Log.WarningCtx(LOG_CTX, "Ignoring shutdown request as shutdown is already in progress.");
                    return;
                }
                this.Log.DebugCtx(LOG_CTX, "A different type of shutdown is requested, permitted to shutdown in a different manner, despite shutdown already in progress.");
            }

            // Initiate shutdown
            this.Log.MessageCtx(LOG_CTX, " ");
            this.Log.MessageCtx(LOG_CTX, "****************************");
            this.Log.MessageCtx(LOG_CTX, "*** Shutting Down Worker ***");
            this.Log.MessageCtx(LOG_CTX, "****************************");
            this.Log.MessageCtx(LOG_CTX, " ");

            // Store the shutdown type requested.
            ShutDownType = shutdownType;

            // Change the state of the worker.
            this.State = WorkerState.ShuttingDown;
        }

        /// <summary>
        /// Clears the cache.
        /// </summary>
        public void FlagClearCache()
        {
            this.Log.DebugCtx(LOG_CTX, "AutomationWorkerAdminService::ClearCache()");

            // Initiate clear cache
            this.Log.MessageCtx(LOG_CTX, " ");
            this.Log.MessageCtx(LOG_CTX, "******************************");
            this.Log.MessageCtx(LOG_CTX, "*** Clear Cache is Flagged ***");
            this.Log.MessageCtx(LOG_CTX, "******************************");
            this.Log.MessageCtx(LOG_CTX, " ");

            // Flags the cache for clear at next available opportunity
            this.ClearCacheFlagged = true;
        }

        #endregion // IAutomationWorkerAdminService Methods

		#region Controller Methods
		/// <summary>
		/// Start automation worker service internal processing.
		/// </summary>
		public void Start()
		{
			// Register with automation service we're available for work.
            if (null != this.AutomationConsumer)
            {
                PollUntilRegistered();
                lastServerConnectionCheckTime_ = DateTime.UtcNow;
            }

            this.JobProcessThread = new Thread(new ThreadStart(this.JobProcess))
            {
                Name = String.Format("AutomationWorkerService : JobProcess role=({0})", this.Role)
            };
            this.JobProcessThread.Start();
		}

        /// <summary>
        /// Loops until the worker can be registered
        /// </summary>
        public void PollUntilRegistered()
        {
            // No need to register if shutting down
            if (this.ShutDownType.HasFlag(ShutdownType.WorkersAndHost))
                return;

            while (true)
            {
                // DW: TODO do not offer up a guid to the client - clients want to be as dumb as possible. Instead use a bool.
                Guid guid = this.AutomationConsumer.RegisterWorker(new Capability(this.Role));
                if (guid != Guid.Empty)
                {
                    this.IsRunning = true;
                    break;
                }
                this.Log.MessageCtx(LOG_CTX, "Worker not registered, retry in {0}s", WORKER_REGISTER_RETRY_RATE_SECONDS);
                Thread.Sleep(WORKER_REGISTER_RETRY_RATE_SECONDS * 1000);
            }
            this.Log.MessageCtx(LOG_CTX, "Worker registered");
        }

		/// <summary>
		/// Stop automation worker service internal processing.
		/// </summary>
		public void Stop()
		{
            this.Log.MessageCtx(LOG_CTX, "Worker Stopping");

			// Notify automation service we're stopping.
            if (null != this.AutomationConsumer)
            {
                this.Log.MessageCtx(LOG_CTX, "Worker Unregistering");
                this.AutomationConsumer.UnregisterWorker();
            }

			this.IsRunning = false;

			Thread.Sleep(UPDATE_TICK);
		}
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
		/// Job processing thread method.
		/// </summary>
		private void JobProcess()
		{
			while (this.IsRunning)
			{
				// Get next job.
				IJob job = null;
				lock (this.Jobs)
				{
                    if (this.Jobs.Any())
					{
						job = this.Jobs.Dequeue();
					}
				}

				if (null != job)
				{
					IEnumerable<IJobProcessor> processors =
						this.JobProcessors.Where(p => p.Role.HasFlag(job.Role));

                    ICollection<IJobResult> jobResults = new List<IJobResult>();

					// Loop through all valid processors.
					foreach (IJobProcessor processor in processors)
					{
                        IJobResult jobResult = processor.Process(job);
                        if (jobResult != null)
                        {
                            jobResults.Add(jobResult);
                        }
					}

                    this.AutomationConsumer.JobCompleted(job, jobResults);

                    // As long as you are not shutting down you may enter idle state from busy
                    if (this.State != WorkerState.ShuttingDown)
                    {
                        this.State = WorkerState.Idle;
                    }
				}
                
                // This is the time to shutdown cleanly if requested                
                if (this.State == WorkerState.ShuttingDown)
                {
                    Stop();
                }
                else
                {
                    TimeSpan timeSinceLastServerConnectionCheck = DateTime.UtcNow - lastServerConnectionCheckTime_;
                    if (timeSinceLastServerConnectionCheck.TotalSeconds > SERVER_CONNECTION_CHECK_PERIOD_SECONDS)
                    {
                        VerifyServerConnection();
                    }
                }

                // Clear the cache if requested
                if (ClearCacheFlagged)
                {
                    ClearCache();

                    // unflag.
                    ClearCacheFlagged = false;
                }

				Thread.Sleep(UPDATE_TICK);
			}

            this.Log.MessageCtx(LOG_CTX, "Job Process Thread Finished");
		}

        /// <summary>
        /// Actually delete the cache.
        /// </summary>
        private void ClearCache()
        {
            String cacheDirectory = this.Options.Project.Cache;

            this.Log.Message(" ");
            this.Log.Message("**************************");
            this.Log.Message("*** Clearing cache now ***");
            this.Log.Message(cacheDirectory);
            this.Log.Message("**************************");
            this.Log.Message(" ");

            if (!Directory.Exists(cacheDirectory))
            {
                this.Log.Warning("Project cache directory: {0} does not exist.", 
                    this.Options.Project.Cache);
            }

            bool cacheCleared = Cache.ClearDirectory(this.Log, cacheDirectory);

            this.Log.Message(" ");
            this.Log.Message("*********************************");
            this.Log.Message("*** Clear Cache Completed {0} ***", cacheCleared ? "SUCCESS" : " FAILED");
            this.Log.Message("*********************************");
            this.Log.Message(" ");
        }

        private void VerifyServerConnection()
        {
            lastServerConnectionCheckTime_ = DateTime.UtcNow;

            if (null != this.AutomationConsumer)
            {
                if (!this.AutomationConsumer.IsWorkerRegistered())
                {
                    this.Log.WarningCtx(LOG_CTX, "Worker registration has lapsed.  Re-registering worker.");
                    PollUntilRegistered();
                }
                else
                {
                    this.Log.MessageCtx(LOG_CTX, "Worker registration check successful.");
                }
            }
            else
            {
                this.Log.WarningCtx(LOG_CTX, "Unable to check worker registration as Automation Consumer isn't setup.");
            }
        }
		#endregion // Private Methods

        #region Private Data
        DateTime lastServerConnectionCheckTime_;
        #endregion
    }

} // RSG.Pipeline.Automation.ClientWorker.Services namespace
