﻿using System;
using SIO = System.IO;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Engine;
using RSG.Pipeline.Content;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Core;
using RSG.Pipeline.Core.Build;
using RSG.Pipeline.Services;
using RSG.Pipeline.Content.Algorithm;
using RSG.Platform;
using Ionic.Zip;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{
    abstract class ISmokeTestAnalyzer
    {
        #region Constants

        protected static readonly String LOG_CTX = "Smoke Test Analyzer";

        private static readonly String TOOLS_TO_KILL = "Tools To Kill";
        private static readonly String TOOLS_TO_LAUNCH = "Tools To Launch";
        private static readonly String P4_CURRENT_LABEL = "P4 Current Label";
        private static readonly String APPLICATION_USER_MODEL_ID = "Durango Application User Model ID";
        private static readonly String MAIL_NOTIFICATION_ADDRESSES = "Mail Notification Addresses";
        private static readonly String METRICS_SEARCH_TIMEOUT = "Metrics Search Timeout";
        #endregion

        #region Properties

        protected String P4CurrentLabel;
        protected RSG.Platform.Platform Platform;
        protected int GameRunTimeMS;
        protected BaseSmokeTestJob CurrentJob;
        protected IBranch Branch;
        protected int MetricsSearchTimeout;
        protected CommandOptions Options;
        protected IList<String> ToolsToKill = null;
        protected IList<String> ToolsToLaunch = null;
        protected IDictionary<String, object> Parameters = null;
        protected IList<String> MailRecipients = null;

        List<ISmoketestTarget> m_Targets = null;
        protected List<ISmoketestTarget> Targets
        {
            get { return m_Targets; }
            set { m_Targets = value; }
        }

        List<SmoketestTargetObserver> m_TargetObservers = null;
        protected List<SmoketestTargetObserver> TargetObservers
        {
            get { return m_TargetObservers; }
            set { m_TargetObservers = value; }
        }

        /// <summary>
        /// Universal log object
        /// </summary>
        protected IUniversalLog Log { get; set; }

        /// <summary>
        /// Universal log file
        /// </summary>
        protected ILogTarget LogFile { get; set; }

        #endregion
        public ISmokeTestAnalyzer(IDictionary<String, object> parameters, IBranch branch, BaseSmokeTestJob job,
                                  IUniversalLog log, ILogTarget logFile, CommandOptions options)
        {
            CurrentJob = job;
            Log = log;
            LogFile = logFile;
            Parameters = parameters;
            Branch = branch;
            Options = options;

            //convert minutes to ms
            this.GameRunTimeMS = 60 * 1000 * job.Runtime;

            if (parameters.ContainsKey(TOOLS_TO_KILL))
            {
                String theTools = (String)parameters[TOOLS_TO_KILL];
                this.ToolsToKill = new List<String>(theTools.Split(new char[] { ';' }));
            }
            else
            {
                this.ToolsToKill = new List<String>();
            }

            if (parameters.ContainsKey(METRICS_SEARCH_TIMEOUT))
            {
                this.MetricsSearchTimeout = Convert.ToInt32(parameters[METRICS_SEARCH_TIMEOUT]);
            }

            if (parameters.ContainsKey(TOOLS_TO_LAUNCH))
            {
                String theTools = (String)parameters[TOOLS_TO_LAUNCH];
                this.ToolsToLaunch = new List<String>(theTools.Split(new char[] { ';' }));
            }
            else
            {
                this.ToolsToLaunch = new List<String>();
            }

            if (parameters.ContainsKey(MAIL_NOTIFICATION_ADDRESSES))
            {
                String theAddr = (String)parameters[MAIL_NOTIFICATION_ADDRESSES];
                this.MailRecipients = new List<String>(theAddr.Split(';'));
            }

            if (parameters.ContainsKey(P4_CURRENT_LABEL))
            {
                this.P4CurrentLabel = (String)parameters[P4_CURRENT_LABEL];
            }
            else
            {
                this.P4CurrentLabel = "Latest";
            }

            this.Platform = DetectPlatform(CurrentJob.ExePath);

            m_Targets = new List<ISmoketestTarget>();
            m_TargetObservers = new List<SmoketestTargetObserver>();
        }

        public abstract bool RunAnalysis();

        protected virtual bool WatchForCrashes(out String outCrashMsg)
        {
            int runtime = 0;
            outCrashMsg = "Not set";

            while (runtime < this.GameRunTimeMS)
            {
                foreach (SmoketestTargetObserver observer in m_TargetObservers)
                {
                    observer.Tick();
                }

                System.Threading.Thread.Sleep(1000);
                runtime += 1000;
            }

            return true;
        }

        protected void KillListedTools()
        {
            foreach (String tool in ToolsToKill)
            {
                foreach (System.Diagnostics.Process process in System.Diagnostics.Process.GetProcessesByName(tool))
                {
                    process.Kill();
                }
                this.Log.MessageCtx(LOG_CTX, "KillListedTools: Process.Kill() {0}", tool);
            }
            int sleepTime = 5000;
            this.Log.MessageCtx(LOG_CTX, "Waiting {0} ms for commands...", sleepTime);
            System.Threading.Thread.Sleep(sleepTime);
        }

        protected void LaunchListedTools()
        {
            foreach (String tool in ToolsToLaunch)
            {
                String toolWithEnv = Environment.ExpandEnvironmentVariables(tool);
                String toolNoDoubleSlash = toolWithEnv.Replace(@"\\", @"\");
                String launchTool = toolNoDoubleSlash.ToLower();

                //split on whitespace
                String[] launchParams = launchTool.Split(null);
                if (launchParams.Count() > 0)
                {
                    String fileName = System.IO.Path.GetFileName(launchParams[0]);

                    if (System.Diagnostics.Process.GetProcessesByName(fileName).Length == 0)
                    {
                        this.Log.MessageCtx(LOG_CTX, "LaunchListedTools {0}", launchTool);

                        StringBuilder processParams = new StringBuilder();
                        for (int paramIndex = 1; paramIndex < launchParams.Count(); paramIndex++)
                        {
                            processParams.Append(launchParams[paramIndex]);
                        }

                        try
                        {
                            //create a non-child process
                            System.Diagnostics.Process.Start(launchParams[0], processParams.ToString());
                        }
                        catch (System.Exception ex)
                        {
                            this.Log.MessageCtx(LOG_CTX, "LaunchListedTools exception process.start {0} {1}", processParams.ToString(), ex.ToString());
                        }
                       
                    }
                }
                else
                {
                    this.Log.WarningCtx(LOG_CTX, "LaunchListedTools: bad tool to launch {0}.  skipping...", launchTool);
                }
            }
            int sleepTime = 5000;
            this.Log.MessageCtx(LOG_CTX, "Waiting {0} ms for commands...", sleepTime);
            System.Threading.Thread.Sleep(sleepTime);
        }

        //@TODO: Durango and other platforms do not work similarly enough to be used with all smoketests.. factor out
        //other platforms to have their own ISmoketestTarget implementation
        protected void RunTheGame()
        {
            this.Log.MessageCtx(LOG_CTX, "Launching game on {0} with {1}", this.Platform.PlatformToRagebuilderPlatform(), this.CurrentJob.ExePath);

            ShutdownTheGame();

            KillListedTools();

            LaunchListedTools();

            String rootDir = Branch.Build;
            String gameExecutable = String.Format("{0}/{1}", rootDir, System.IO.Path.GetFileName(this.CurrentJob.ExePath));

            switch (this.Platform)
            {
                case RSG.Platform.Platform.PS3:
                    Log.ErrorCtx(LOG_CTX, "PS3 support removed.  Still in Perforce if needed.");
                    break;
                case RSG.Platform.Platform.Xbox360:
                    Log.ErrorCtx(LOG_CTX, "360 support removed.  Still in Perforce if needed.");
                    break;

                case RSG.Platform.Platform.Win64:
                    {
                        SmoketestTargetx64Parameters parameters = new SmoketestTargetx64Parameters();
                        parameters.CommandLineArguments = this.CurrentJob.CommandLine;
                        parameters.ExePath = gameExecutable;
                        parameters.LogFile = CurrentJob.LogFile;
                        parameters.RootDir = rootDir;

                        if (this.CurrentJob.Targets != null)
                        {
                            LaunchOnAllTargets(parameters);
                        }

                        break;
                    }

                case RSG.Platform.Platform.PS4:
                    {
                        SmoketestTargetOrbisParameters parameters = new SmoketestTargetOrbisParameters();
                        parameters.CommandLineArguments = this.CurrentJob.CommandLine;
                        parameters.ExePath = gameExecutable;
                        parameters.LogFile = CurrentJob.LogFile;
                        parameters.RootDir = rootDir;

                        if (this.CurrentJob.Targets != null)
                        {
                            LaunchOnAllTargets(parameters);
                        }

                        break;
                    }

                case RSG.Platform.Platform.XboxOne:
                    {
                        if (!this.Parameters.ContainsKey(APPLICATION_USER_MODEL_ID))
                        {
                            this.Log.ErrorCtx(LOG_CTX, "Durango Application User Model ID must be configured");
                            break;
                        }

                        //note that this executable is something like
                        //x:\rdr3/assets/automation/codebuilder/game_2012_unity.x64.bankrelease.incredibuild.inc/game_win64_bankrelease.exe
                        String gameXboneExecutable = Options.Branch.Environment.Subst(this.CurrentJob.ExePath);

                        SmoketestTargetDurangoParameters parameters = new SmoketestTargetDurangoParameters();
                        parameters.AumID = (String)Parameters[APPLICATION_USER_MODEL_ID];
                        parameters.CommandLineArguments = this.CurrentJob.CommandLine;
                        parameters.ExePath = gameXboneExecutable;
                        parameters.LogFile = CurrentJob.LogFile;

                        if (this.CurrentJob.Targets != null)
                        {
                            LaunchOnAllTargets(parameters);
                        }
                        break;
                    }
            }
        }

        /// <summary>
        /// Build shaders based on input CL for whichever platform we care about
        /// </summary>
        /// <param name="syncTo"></param>
        /// <returns></returns>
        protected bool BuildShaders(String cl)
        {
            this.Log.MessageCtx(LOG_CTX, "Building Shaders...");
            using (P4 p4 = new RSG.SourceControl.Perforce.P4())
            {
                this.Log.MessageCtx(LOG_CTX, "Synching src to {0}", cl);

                String srcCmd = String.Format("{0}{1}{2} {3}", Branch.Code,
                                                              "\\",
                                                              @"%%1",
                                                              cl);
                SyncCommand(p4, srcCmd, "src");

                String rageCmd = String.Format("{0}{1}{2} {3}", Branch.RageCode,
                                                            "\\",
                                                            @"%%1",
                                                            cl);
                SyncCommand(p4, rageCmd, "rage src");
            }

            //build shader code, based on platform
            String buildShadersCmd = "";
            switch (this.Platform)
            {
                case RSG.Platform.Platform.Win64:
                    buildShadersCmd = "rsm_rebuild_win32_40.bat";
                    break;
                case RSG.Platform.Platform.PS4:
                    buildShadersCmd = "Rsm_rebuild_orbis.bat";
                    break;
                case RSG.Platform.Platform.XboxOne:
                    buildShadersCmd = "Rsm_rebuild_durango.bat";
                    break;
            }

            String buildCmd = String.Format("{0}\\game\\shader_source\\VS_Project\\batch\\{1}", Branch.Code, buildShadersCmd);

            this.Log.MessageCtx(LOG_CTX, "Executing: {0}", buildCmd);

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = buildCmd;

            TimeSpan waitTime = new TimeSpan(0, 20, 0);

            System.Diagnostics.Process buildProcess = new System.Diagnostics.Process();

            buildProcess.StartInfo = startInfo;
            buildProcess.StartInfo.UseShellExecute = false;
            //buildProcess.StartInfo.RedirectStandardOutput = true;
            buildProcess.StartInfo.RedirectStandardError = true;
            buildProcess.StartInfo.RedirectStandardInput = true;
            buildProcess.StartInfo.CreateNoWindow = false;

            buildProcess.Start();

            //buildProcess.OutputDataReceived += CmdPromptOutputDataReceived;
            buildProcess.ErrorDataReceived += CmdPromptErrorDataReceived;

            //buildProcess.BeginOutputReadLine();
            buildProcess.BeginErrorReadLine();

            // Wait up to a minute
            DateTime deployStartTime = DateTime.Now;
            while (waitTime > DateTime.Now - deployStartTime && !buildProcess.HasExited)
            {
                System.Threading.Thread.Sleep(1000);
            }

            bool success = false;

            //String stdOutput = launchGameProcess.StandardOutput.ReadToEnd();
            //String stdError = launchGameProcess.StandardError.ReadToEnd();

            if (buildProcess.HasExited)
            {
                this.Log.MessageCtx(LOG_CTX, "Shader Build process exited with code {0}", buildProcess.ExitCode);

                if (buildProcess.ExitCode == 0)
                { 
                    this.Log.MessageCtx(LOG_CTX, "Shader Build successful");
                    success = true;
                }
            }

            if (!buildProcess.HasExited)
            {
                this.Log.ErrorCtx(LOG_CTX, "Shader Build process did not complete in {0} minutes. Killing...", waitTime.TotalMinutes);
                buildProcess.Kill();
            }

            return success;
        }

        /// <summary>
        /// Returns the last submitted CL
        /// </summary>
        /// <returns></returns>
        protected UInt32 GetP4LatestCL()
        {
            UInt32 cl = 0;
            using (P4 p4 = new RSG.SourceControl.Perforce.P4())
            {
                P4API.P4RecordSet changesResult = p4.Run("changes", "-s", "submitted", "-m", "1");

                if (changesResult.Records.Count() >= 1)
                {
                    cl = uint.Parse(changesResult.Records[0]["change"]);
                }
                else
                {
                    this.Log.WarningCtx(LOG_CTX, "GetP4LatestCL: p4 changes failed.  {0} records returned", changesResult.Records.Count());
                }
            }
            return cl;
        }

        private void LaunchOnAllTargets(SmoketestTargetConsoleParameters param)
        {
            foreach (String ipAddress in this.CurrentJob.Targets)
            {
                this.Log.MessageCtx(LOG_CTX, "Launching {0} smoketest client {1}", 
                                    this.Platform.PlatformToFriendlyName(), ipAddress);

                ISmoketestTarget target = null;
                switch (this.Platform)
                {
                    case RSG.Platform.Platform.Win64:
                        target = new SmoketestTargetx64(this.Log, ipAddress, Options);
                        break;
                    case RSG.Platform.Platform.PS4:
                        target = new SmoketestTargetOrbis(this.Log, ipAddress, Options);
                        break;
                    case RSG.Platform.Platform.XboxOne:
                        target = new SmoketestTargetDurango(this.Log, ipAddress, Options);
                        break;
                }
               
                target.Launch(param);

                List<String> successMessages = new List<String>();
                List<String> failureMessages = new List<String>();
                if (CurrentJob is ISmokeTestCompletedMessages)
                {
                    ISmokeTestCompletedMessages msgBasedJob = (ISmokeTestCompletedMessages)CurrentJob;
                    successMessages.AddRange(msgBasedJob.SuccessMessages);
                    failureMessages.AddRange(msgBasedJob.FailureMessages);
                }

                SmoketestTargetObserver observer = new SmoketestTargetObserver(this.Log,
                                                                               target,
                                                                               this.CurrentJob.OutputDir,
                                                                               successMessages,
                                                                               failureMessages,
                                                                               this.CurrentJob.HangTimeout,
                                                                               this.CurrentJob.LogNotFoundTimeout,
                                                                               this.CurrentJob.FatalTimeout,
                                                                               this.CurrentJob.CrashWaitForStartCoreDumpTimeout,
                                                                               this.CurrentJob.CrashWaitForEndCoreDumpTimeout);
                m_Targets.Add(target);
                m_TargetObservers.Add(observer);
            }
        }

        protected void ShutdownTheGame()
        {
            foreach (ISmoketestTarget target in m_Targets)
            {
                target.Shutdown( Environment.ExpandEnvironmentVariables(System.IO.Path.GetFileName(this.CurrentJob.ExePath)));
            }
            int sleepTime = 5000;
            this.Log.MessageCtx(LOG_CTX, "Waiting {0} ms for commands...", sleepTime);
            System.Threading.Thread.Sleep(sleepTime);
        }

        /// <summary>
        /// What CL are currently on? we don't want to sync backwards
        /// </summary>
        /// <param name="p4"></param>
        /// <returns></returns>
        protected UInt32 GetP4CurrentSync(P4 p4)
        {
            UInt32 currentSync = 0;
            P4API.P4RecordSet syncResults = p4.Run("info");
            if (syncResults.Records.Count() > 0)
            {
                String client = syncResults.Records[0]["clientName"];
                P4API.P4RecordSet changesResults = p4.Run("changes", "-m1", "@"+client);
                if (changesResults.Records.Count() > 0)
                {
                    currentSync = Convert.ToUInt32(changesResults.Records[0]["change"]);
                    this.Log.MessageCtx(LOG_CTX, "p4 changes {0} => CL{1}", client, currentSync);
                }
            }
            return currentSync;
        }


        /// <summary>
        /// p4 sync to the input cl with options; desc just used for logging info
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="syncCommand"></param>
        /// <param name="desc"></param>
        /// <returns></returns>
        protected int SyncCommand(P4 p4, String syncCommand, String desc)
        {
            this.Log.MessageCtx(LOG_CTX, "SyncP4 {0} cmd: {1}", desc, syncCommand);
            int fileCount = 0;

            if (this.CurrentJob.SyncFiles)
            {
                Stopwatch timer = new Stopwatch();
                timer.Start();
                P4API.P4RecordSet syncResults = p4.Run("sync", syncCommand);
           
                // do we already have the latest version?
                if (syncResults.Records.Count() == 0)
                {
                    this.Log.MessageCtx(LOG_CTX, "SyncP4 {0} complete. File(s) up-to-date", desc);
                }
                else
                {
                    fileCount = Convert.ToInt32(syncResults.Records[0]["totalFileCount"]);
                    this.Log.MessageCtx(LOG_CTX, "SyncP4 {0} complete. {1} files(s) updated in {2} minutes",
                                        desc, fileCount, timer.Elapsed.TotalMinutes);
                }
            }
            else
            {
                this.Log.WarningCtx(LOG_CTX, "No files updated.  P4 sync prevented by Sync Files config var");
            }
            
            return fileCount;
        }

        /// <summary>
        /// sync platform-data @ CL
        /// </summary>
        protected void SyncP4(String SyncTo, bool includeExecutable = true)
        {
            if (String.Equals(SyncTo, "0", StringComparison.CurrentCultureIgnoreCase))
            {
                this.Log.WarningCtx(LOG_CTX, "No files updated.  Trying to sync to CL0 {0}", SyncTo);
            }
            else
            {
                using (P4 p4 = new RSG.SourceControl.Perforce.P4())
                {
                    this.Log.MessageCtx(LOG_CTX, "SyncP4 {0}", SyncTo);

                    String rootDir = Branch.Build;

                    String looseCmd = String.Format("{0}{1}{2} {3}", rootDir,
                                                                  "\\",
                                                                  @"%%1",
                                                                  SyncTo);
                    SyncCommand(p4, looseCmd, "loose");

                    //@TODO force? 
                    String commonCmd = String.Format("{0}{1}{2}{3}",
                                                     rootDir,
                                                     "\\common",
                                                     "\\...",
                                                     SyncTo);
                    SyncCommand(p4, commonCmd, "common");

                    String platformCmd = String.Format("{0}{1}{2}{3}{4}", rootDir,
                                                                       "\\",
                                                                       this.Platform.PlatformToOutputDirectory(),
                                                                       "\\...",
                                                                       SyncTo);
                    SyncCommand(p4, platformCmd, "platform");
                    
                    String updateCmd = String.Format("{0}\\update\\...{1}", rootDir, SyncTo);
                    SyncCommand(p4, updateCmd, "update");

                    if (includeExecutable)
                    {
                        //copy over the latest automated codebuilder executable as we may have just overwritten it
                        //this is only necessary if the server and client are separate machines
                        String syncExeCmd = String.Format("{0}{1}{2}", this.CurrentJob.ExePath, @"@", this.CurrentJob.CodeBuilderChangelist.ToString());

                        SyncCommand(p4, syncExeCmd, "exe");

                        //sync our debug files
                        IList<String> debugFiles = GetDebugFilesForPlatform(this.CurrentJob.ExePath);

                        foreach (String debugFile in debugFiles)
                        {
                            String syncDebugFilesCmd = String.Format("{0}@{1}", debugFile, this.CurrentJob.CodeBuilderChangelist.ToString());
                            SyncCommand(p4, syncDebugFilesCmd, debugFile);
                        }

                        // only copy over the exe if we're sync files
                        if (this.CurrentJob.SyncFiles)
                        {
                            //can't launch the game from that folder; copy it to build
                            CopyFilesToBuild(this.CurrentJob.ExePath);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Copies all associate executable files to the build folder
        /// </summary>
        /// <param name="path"></param>
        private void CopyFilesToBuild(String exePath)
        {
            CopyFileToBuild(exePath);

            IList<String> debugFiles = GetDebugFilesForPlatform(exePath);

            foreach (String debugFile in debugFiles)
            {
                CopyFileToBuild(debugFile);
            }
            this.Log.MessageCtx(LOG_CTX, "Copied {0} debug files", debugFiles.Count());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="exePath"></param>
        /// <returns></returns>
        private IList<String> GetDebugFilesForPlatform(String exePath)
        {
            IList<String> files = new List<String>();
            RSG.Platform.Platform exePlatform = DetectPlatform(exePath);
            int i = 0;

            switch (exePlatform)
            {
                case RSG.Platform.Platform.Win64:
                    files.Add(System.IO.Path.ChangeExtension(exePath, ".cmp"));
                    files.Add(System.IO.Path.ChangeExtension(exePath, ".map"));
                    break;
                case RSG.Platform.Platform.PS4:
                    files.Add(System.IO.Path.ChangeExtension(exePath, ".cmp"));
                    files.Add(System.IO.Path.ChangeExtension(exePath, ".map"));
                    break;
                case RSG.Platform.Platform.XboxOne:
                    files.Add(System.IO.Path.ChangeExtension(exePath, ".cmp"));
                    break;
            }
            return files;
        }

        /// <summary>
        /// CopyFileToBuild - meh, can't launch the game from that folder; copy it to build
        /// </summary>
        /// <param name="path"></param>
        private void CopyFileToBuild(String path)
        {
            if (this.CurrentJob.SyncFiles)
            {
                String rootDir = this.Branch.Build;

                String filename = System.IO.Path.GetFileName(path);
                String destination = System.IO.Path.Combine(rootDir, filename);

                //remove read-only if its there as that will make the copy barf
                if (System.IO.File.Exists(destination))
                {
                    FileInfo fileInfo = new FileInfo(destination);
                    if (fileInfo.IsReadOnly)
                    {
                        fileInfo.IsReadOnly = false;
                        fileInfo.Refresh();
                    }
                }

                if (System.Diagnostics.Process.GetProcessesByName(destination).Length == 0)
                {
                    this.Log.MessageCtx(LOG_CTX, "Copying files: {0} {1}", path, destination);

                    try
                    {
                        System.IO.File.Copy(path, destination, true);
                    }
                    catch (System.Exception ex)
                    {
                        this.Log.ErrorCtx(LOG_CTX, "Can't Copy files: {0} {1}; {2}", path, destination, ex.ToString());
                    }
                }
                else
                {
                    this.Log.ErrorCtx(LOG_CTX, "Can't Copy files: {0} {1}; destination file in use", path, destination);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="exePath"></param>
        /// <returns></returns>
        private RSG.Platform.Platform DetectPlatform(String exePath)
        {
            RSG.Platform.Platform[] platforms = (RSG.Platform.Platform[])Enum.GetValues(typeof(RSG.Platform.Platform));

            foreach (RSG.Platform.Platform plat in platforms)
            {
                //platform values need to match those ingame in AutoGPUCapture.cpp
                if (exePath.Contains(plat.PlatformToRagebuilderPlatform()))
                {
                    return plat;
                }
            }
            this.Log.ErrorCtx(LOG_CTX, "Platform not found in exePath {0}; defaulting to Independent", exePath);
            return RSG.Platform.Platform.Independent;
        }

        private void CmdPromptOutputDataReceived(object sender, DataReceivedEventArgs dataIn)
        {
            this.Log.MessageCtx(LOG_CTX, "STDOUT: " + dataIn.Data);
        }

        private void CmdPromptErrorDataReceived(object sender, DataReceivedEventArgs dataIn)
        {
            this.Log.MessageCtx(LOG_CTX, "STDERR: " + dataIn.Data);
        }

        protected void CleanupObservers()
        {
            foreach (SmoketestTargetObserver observer in m_TargetObservers)
            {
                observer.CleanupLogMonitor();
            }
            m_TargetObservers.Clear();
        }

        protected void CleanupTargets()
        {
            m_Targets.Clear();
        }

        protected void SaveOutputFromRun(LogMonitor monitor, ISmoketestTarget target)
        {
            Log.MessageCtx(LOG_CTX, "Completed run {0}", monitor.LogFilePath);
            Log.MessageCtx(LOG_CTX, "Error Count: {0}", monitor.Messages.Count);

            String outputDirRoot = String.Format(@"{0}\{1}_{2}\", 
                this.CurrentJob.OutputDir, 
                Path.GetFileNameWithoutExtension(monitor.LogFilePath), 
                DateTime.Now.ToString("yyyyMMddHHmmss"));

            String newLogFilePath = outputDirRoot + Path.GetFileName(monitor.LogFilePath);

            Log.MessageCtx(LOG_CTX, "Moving logfile: {0} to {1}", monitor.LogFilePath, newLogFilePath);
            AttemptMoveOrCopyFile(monitor.LogFilePath, newLogFilePath);

            if (monitor.CoreDumpSuceeded)
            {
                String newDumpPath = outputDirRoot + Path.GetFileName(monitor.CoreDumpPath);

                Log.MessageCtx(LOG_CTX, "Moving dump: {0} to {1}", monitor.CoreDumpPath, newDumpPath);
                AttemptMoveOrCopyFile(monitor.CoreDumpPath, newDumpPath);
            }

            foreach (String artifact in this.CurrentJob.Artifacts)
            {
                String artifactSearchName = artifact;
                artifactSearchName = artifactSearchName.Replace("[TargetIP]", target.IpAddress);
                artifactSearchName = artifactSearchName.Replace("[Changelist]", this.CurrentJob.Changelist.ToString());
                artifactSearchName = artifactSearchName.Replace("[RunIndex]", target.RunIndex.ToString());

                string[] files = System.IO.Directory.GetFiles(Branch.Build, artifactSearchName, SearchOption.TopDirectoryOnly);
                
                foreach (String file in files)
                {
                    String newArtifactPath = outputDirRoot + Path.GetFileName(file);

                    Log.MessageCtx(LOG_CTX, "Copying artifact {0}: {1} to {2}",
                        artifact,
                        file,
                        newArtifactPath);

                    if (System.IO.File.Exists(newArtifactPath))
                    {
                        Log.MessageCtx(LOG_CTX, "  SKIPPING - {0} already exists", newArtifactPath);
                    }
                    else
                    {
                        AttemptMoveOrCopyFile(file, newArtifactPath);
                    }
                }
            }
        }

        // A more bullet-proofed version of moving a file than System.IO.File.Move.
        // First attempt to move the file. If that fails, try to copy it it. Do not
        // throw exceptions.
        protected void AttemptMoveOrCopyFile(String from, String to)
        {
            try
            {
                String dir = Path.GetDirectoryName(to);
                System.IO.Directory.CreateDirectory(dir);
                Log.MessageCtx(LOG_CTX, "Creating directory {0}", from);
            }
            catch (System.Exception e)
            {
                Log.MessageCtx(LOG_CTX, "Exception in AttemptMoveFile CreateDirectory: {0}", e.Message);
            }

            try
            {
                System.IO.File.Move(from, to);
            }
            catch (System.Exception)
            {
                Log.DebugCtx(LOG_CTX, "Could not move file {0}.. may still be in use. Attempting to copy.", from);
                try
                {
                    System.IO.File.Copy(from, to);
                }
                catch (System.Exception e2)
                {
                    Log.MessageCtx(LOG_CTX, "Could not move or copy file {0} to {1}", from, to);
                    Log.MessageCtx(LOG_CTX, "Exception in AttemptMoveFile Move/Copy: {0}", e2.Message);
                }
            }
        }
    }
}
