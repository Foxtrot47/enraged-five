﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{
    // A simple data object for messages we have read in from the log
    class LogMessage
    {
        String m_Message;
        public String Message
        {
            get { return m_Message; }
            set { m_Message = value; }
        }

        // This is something like "Error" or "Fatal Error". It will be the string after the timestamp (i.e. [XXXXXXXX:XXXXXXXX] Error: blah blah blah)
        String m_Severity;
        public String Severity
        {
            get { return m_Severity; }
            set { m_Severity = value; }
        }

        // Each string is a stack entry, in order of being found in the file. Generally stacks print BEFORE
        // the relevant assert so the stack will come from the previous entry in the log. In some cases though, the stack might be part of the
        // error message itself (will be present in Message)
        List<String> m_StackTrace = new List<string>();
        public List<String> StackTrace
        {
            get { return m_StackTrace; }
            set { m_StackTrace = value; }
        }
    }

    // This class is intended to watch a given log file for changes. From this, we can gather asserts, errors, and detect crashes and hangs.
    // It may be given the file that is expected to be used by a process before it is launched. It is safe for the file to already exist
    // as long as it is not currently being modified. As soon as we detect the presence or change of the file, we read everything in it from
    // beginning to end and continuously look for additional information to be written. Eventually, you may terminate monitoring of the
    // file, either because it appears to be stalled or because you no longer care.
    //
    // In the future, support for detecting crashes or successful termination of the game would be helpful
    //
    // You can use something like this to watch the file:

    /*
            LogMonitor monitor = new LogMonitor(@"X:\10.0.22.226_smoketest.log");
            
            TimeSpan stallTime = new TimeSpan(0, 0, 60); // 60 seconds

            // Bail if the file has been touched, but it's been more than 15 seconds since it was last touched
            while (monitor.IsWaitingForFile || DateTime.Now - monitor.FileChangeLastObserved < stallTime)
            {
                monitor.Tick();
            }

            monitor.EndReadFile();
     
            // Do stuff..
    */

    class LogMonitor : IDisposable
    {
        private List<String> m_SeveritiesToMonitor = new List<string>(new string[] { "Error" });
        public List<String> SeveritiesToMonitor
        {
            get { return m_SeveritiesToMonitor; }
            set { m_SeveritiesToMonitor = value; }
        }

        private List<String> m_FatalSeverities = new List<string>(new string[] { "Fatal Error" });
        public List<String> FatalSeverities
        {
            get { return m_FatalSeverities; }
            set { m_FatalSeverities = value; }
        }

        //Regex interpretation of m_SuccessMessages
        private List<Regex> m_SuccessRegexs = new List<Regex>();
        //Regex interpretation of m_FailureMessages
        private List<Regex> m_FailureRegexs = new List<Regex>();

        // Full path to the file we will monitor
        private String m_LogFilePath;
        public String LogFilePath
        {
            get { return m_LogFilePath; }
        }

        // All monitored messages in log found so far (presumably errors)
        private List<LogMessage> m_Messages = new List<LogMessage>();
        public List<LogMessage> Messages
        {
            get { return m_Messages; }
            set { m_Messages = value; }
        }

        // Last observed modified date
        private DateTime m_FileLastModified = DateTime.MinValue;
        public DateTime FileLastModified
        {
            get { return m_FileLastModified; }
        }

        // Last observed file size
        private long m_FileSize = -1;
        public long FileSize
        {
            get { return m_FileSize; }
        }

        // Wall-clock time we observed the last change to the file (size or modification date count)
        private DateTime m_FileChangeLastObserved = DateTime.MinValue;
        public DateTime FileChangeLastObserved
        {
            get { return m_FileChangeLastObserved; }
        }

        // True if we see evidence of a finished SmokeTest Message
        bool? m_HasSucceeded = null;
        public bool? HasSucceeded
        {
            get { return m_HasSucceeded; }
            set { m_HasSucceeded = value; }
        }

        // True if we see evidence of a finished SmokeTest Message
        bool m_HasFailed = false;
        public bool HasFailed
        {
            get { return m_HasFailed; }
            set { m_HasFailed = value; }
        }

        // True if we have seen a message with a severity listed in FatalSeverities
        bool m_HasFatalMessage = false;
        public bool HasFatalMessage
        {
            get { return m_HasFatalMessage; }
            set { m_HasFatalMessage = value; }
        }

        // True if we see evidence of a crash in the log
        bool m_HasCrash = false;
        public bool HasCrash
        {
            get { return m_HasCrash; }
            set { m_HasCrash = value; }
        }

        // True if the log indicates a core dump was successful
        bool m_CoreDumpAttempted;
        public bool CoreDumpAttempted
        {
            get { return m_CoreDumpAttempted; }
        }

        // True if the log indicates a core dump was successful
        bool m_CoreDumpSucceeded;
        public bool CoreDumpSuceeded
        {
            get { return m_CoreDumpSucceeded; }
        }

        // True if the log indicates a core dump was attempted but failed
        bool m_CoreDumpFailed;
        public bool CoreDumpFailed
        {
            get { return m_CoreDumpFailed; }
        }

        // If we attempted a core dump, will be the path to the file on PC. Otherwise, null.
        String m_CoreDumpPath;
        public System.String CoreDumpPath
        {
            get { return m_CoreDumpPath; }
        }

        enum State
        {
            WAIT_FOR_FILE, // We are waiting for the file currently existing to change modification date, OR for the file to show up if it doesn't currently exist
            READ_FILE, // We are parsing the file as it comes in and will continue to do so
            FINALIZED // We made a final read and will now ignore the file. It is necessary to finalize to process the last entry in the log
        }

        // If we are waiting for the file to show up or be touched
        public bool IsWaitingForFile
        {
            get { return m_State == State.WAIT_FOR_FILE; }
        }

        private State m_State = State.WAIT_FOR_FILE;

        // Count of bytes we have processed
        private int m_Progress = 0;

        // Stream that is open while in state READ_FILE
        private FileStream m_Stream;

        // We are looking for the begining of a new log line with a severity. Newlines can get the prefix again
        // so we need to look specifically for:
        //   [XXXXXXXX:XXXXXXXX] [Severity]:
        //     - Where Severity is alphanumeric and may contain spaces
        static readonly Regex LogLineBeginRegex = new Regex(@"\[\d{8}\:\d{8}\] (?<severity>[A-Za-z-0-9 ]+)\:");

        // We are looking for the escape codes we put around our stack traces:
        //   ASCII CHAR 33 (0x1B), [, 35m to start
        //   The stack trace line itself. [^\f\n\r\t\v] forces the regex to find this on a single line (NOT carriage returns)
        //   ASCII CHAR 33 (0x1B), [, 0m to finish
        static readonly Regex StackTraceRegex = new Regex(@"\x1B\[35m(?<line>[^\f\n\r\t\v]+)\x1B\[0m");
        static readonly int CHUNK_SIZE = 1024 * 1024; // Read file in 1MB chunks

        static readonly Regex CrashRegex = new Regex(@"\x1B\[35m\*\*\* EXCEPTION \w+ CAUGHT at address \w+\x1B\[0m");

        //static readonly Regex StartCoreDumpRegex = new Regex(@"Generating dump file");
        static readonly Regex StartCoreDumpRegex = new Regex(@"\x1B\[35mGenerating dump file");
        static readonly Regex TransferCoreDumpRegex = new Regex("Copying dump to host PC \"" + @"(?<path>[^\f\n\r\t\v]+)" + "\"");
        static readonly Regex FailCoreDumpRegex = new Regex("Failed.  Please manually copy the coredump off the console.");
        static readonly Regex CompleteCoreDumpRegex = new Regex("core dump complete");

        // Constructs a LogMonitor
        // - filePath - Full path to the file to observe. It may or may not exist yet.
        public LogMonitor(String filePath)
        {
            m_LogFilePath = filePath;

            // Cache the last modified time so that we can know if it is bumped
            if (System.IO.File.Exists(m_LogFilePath))
            {
                m_FileLastModified = File.GetLastWriteTime(m_LogFilePath);
            }
        }

        //For SmokeTest jobs that want to end based on a printed output, pass in messages
        //and monitor the m_HasSucceeded bool.
        public void SetSuccessMessages(IEnumerable<String> successMessages)
        {
            //Adding Regex's to check for parsing output
            foreach (String successMsg in successMessages)
            {
                m_SuccessRegexs.Add(new Regex(successMsg));
            }
        }

        //For SmokeTest jobs that want to end based on a printed output, pass in messages
        //and monitor the m_HasFailed bool.
        public void SetFailureMessages(IEnumerable<String> failureMessages)
        {
            //Adding Regex's to check for parsing output
            foreach (String failureMsg in failureMessages)
            {
                m_FailureRegexs.Add(new Regex(failureMsg));
            }
        }

        // Call this frequently
        // - 
        public void Tick()
        {
            switch (m_State)
            {
                case State.WAIT_FOR_FILE:

                    // If the file exists now but didn't before, OR the modified date changed, start monitoring
                    if (System.IO.File.Exists(m_LogFilePath) &&
                        (m_FileLastModified == DateTime.MinValue || File.GetLastWriteTime(m_LogFilePath) > m_FileLastModified))
                    {
                        StartReadingFile();
                    }

                    break;

                case State.READ_FILE:
                    Read(false);
                    break;

                case State.FINALIZED:
                    break;
            }
        }

        public void EndReadFile()
        {
            // Do our final read
            Read(true);

            m_State = State.FINALIZED;
            if(m_Stream != null)
            {
                m_Stream.Dispose();
            }
            m_Stream = null;
        }

        private void StartReadingFile()
        {
            m_State = State.READ_FILE;
            m_FileChangeLastObserved = DateTime.Now;

            m_Stream = File.Open(m_LogFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

            // Save the file length for later detection of modification
            FileInfo fileInfo = new FileInfo(m_LogFilePath);
            m_FileSize = fileInfo.Length;

            // Our first tick with a good file should read
            Read(false);

            // Read should set this value to something reasonable.
            Debug.Assert(m_FileLastModified != DateTime.MinValue);
        }

        public void Read(bool isFinalRead)
        {
            Debug.Assert(m_State == State.READ_FILE);
            Debug.Assert(m_Stream != null);
            Debug.Assert(m_FileSize >= 0);

            String previousLine = null;
            FileInfo fileInfo = new FileInfo(m_LogFilePath);

            // If the file has not been modified, return immediately
            DateTime fileLastModified = File.GetLastWriteTime(m_LogFilePath);
            if ((!isFinalRead && fileLastModified <= m_FileLastModified && m_FileSize == fileInfo.Length)
                || !fileInfo.Exists || m_Stream == null)
            {
                return;
            }

            //Console.Out.WriteLine("File changed");

            // File has been changed.. update state
            m_FileLastModified = fileLastModified;
            m_FileSize = fileInfo.Length;
            m_FileChangeLastObserved = DateTime.Now;

            // Keep reading new chunks from the file as long as we have entries
            bool entriesFoundInChunk = true;
            while (entriesFoundInChunk)
            {
                entriesFoundInChunk = false;

                // Read the next chunk
                m_Stream.Seek(m_Progress, SeekOrigin.Begin);
                byte[] bytes = new byte[CHUNK_SIZE];
                int bytesRead = m_Stream.Read(bytes, 0, CHUNK_SIZE);

                // This is the new chunk we will operate on
                string s = Encoding.Default.GetString(bytes);

                // Find the first match
                Match m1 = LogLineBeginRegex.Match(s, 0);
                int progressWithinChunk = 0;

                if (m1.Success)
                {
                    // Keep reading entries in this chunk as long as we have entries
                    bool chunkReadUnfinished = true;
                    while (chunkReadUnfinished)
                    {
                        chunkReadUnfinished = false;

                        String line = null;
                        String severity = null;
                        bool hasEntry = false;
                        bool peekIncompleteEntry = false; // Read ahead to detect crashes in a final line without having to call EndReadFile()

                        // Find where the current match ends
                        Match m2 = m1.NextMatch();
                        if (m2.Success)
                        {
                            // The current entry has an end, so we should process it
                            line = s.Substring(m1.Index, m2.Index - m1.Index);
                            hasEntry = true;
                            chunkReadUnfinished = true;
                            entriesFoundInChunk = true;
                            progressWithinChunk = m2.Index;
                        }
                        else
                        {
                            // This is the final read from the file so assume the current entry ends at end of file
                            if (isFinalRead)
                            {
                                hasEntry = true;
                                progressWithinChunk = s.Length;
                            }
                            // We processed a chunk and had leftovers. So we moved our next chunk to the start of the leftovers.
                            // Checking progressWithinChunk == 0 ensures that we only run through here after we moved the chunk
                            // to get as many characters in the partial line as possible. If we didn't do this, we'd be doing
                            // a peek on the end of every chunk, even though we could move the chunk and get the full message, and
                            // that would be dumb. Doing this lets us know about a crash in the final message of the log without
                            // having to timeout and call EndReadFile()
                            else if (progressWithinChunk == 0)
                            {
                                peekIncompleteEntry = true;
                            }

                            line = s.Substring(m1.Index);
                        }

                        // Process this entry
                        if (hasEntry || peekIncompleteEntry)
                        {
                            // We will enter here for some messages potentially multiple times so everything in here must be idempotent
                            severity = m1.Groups["severity"].Value;

                            if (m_FatalSeverities.Contains(severity))
                            {
                                m_HasFatalMessage = true;
                            }

                            if (CrashRegex.Match(line).Success)
                            {
                                m_HasCrash = true;
                            }

                            foreach (Regex successRegex in m_SuccessRegexs)
                            {
                                if (successRegex.Match(line).Success)
                                {
                                    m_HasSucceeded = true;
                                }
                            }
                            foreach (Regex failureRegex in m_FailureRegexs)
                            {
                                if (failureRegex.Match(line).Success)
                                {
                                    m_HasSucceeded = false;
                                }
                            }
                            if (m_HasCrash)
                            {
                                Match hasCoreDumpMatch = TransferCoreDumpRegex.Match(line);
                                if (hasCoreDumpMatch.Success)
                                {
                                    m_CoreDumpPath = hasCoreDumpMatch.Groups["path"].Value;
                                }

                                if (StartCoreDumpRegex.Match(line).Success)
                                {
                                    m_CoreDumpAttempted = true;
                                }

                                if (CompleteCoreDumpRegex.Match(line).Success)
                                {
                                    m_CoreDumpSucceeded = true;
                                }

                                if (FailCoreDumpRegex.Match(line).Success)
                                {
                                    m_CoreDumpFailed = true;
                                }
                            }

                            // We will only enter this block once per message
                            if (hasEntry)
                            {
                                if (m_SeveritiesToMonitor.Contains(severity) || m_FatalSeverities.Contains(severity))
                                {
                                    LogMessage message = new LogMessage();

                                    message.Message = line;
                                    message.Severity = severity;

                                    //Console.Out.WriteLine(message.Message);

                                    if (previousLine != null)
                                    {
                                        MatchCollection matches = StackTraceRegex.Matches(previousLine);
                                        foreach (Match m in matches)
                                        {
                                            message.StackTrace.Add(m.Groups["line"].Value);

                                            //Console.Out.WriteLine(m.Groups["line"].Value);
                                        }
                                    }

                                    m_Messages.Add(message);
                                }
                            }
                        }

                        // Advance to the next entry
                        previousLine = line;
                        m1 = m2;
                    }
                }

                // Normally, we should advance as far as we parsed
                m_Progress += progressWithinChunk;

                // If we read through a full-sized chunk and couldn't progress, our chunk size is too small
                if (progressWithinChunk == 0 && bytesRead == CHUNK_SIZE)
                {
                    Console.Out.WriteLine("*** WARNING: CHUNK SIZE TOO SMALL. MONITORED MESSAGES MAY GET MISSED! ***");
                    m_Progress += CHUNK_SIZE;
                }
            }
        }

        public void Dispose()
        {
            // This will make sure any system resources we have are released
            if (m_State != State.READ_FILE && m_State != State.WAIT_FOR_FILE)
            {
                EndReadFile();
            }

            m_State = State.FINALIZED;
        }
    }
}
