﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Threading;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Content;
using RSG.Pipeline.Content.Algorithm;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Platform;
using RSG.SourceControl.Perforce;
using Ionic.Zip;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{
    abstract class BasePackageProcessor :
        JobProcessorBase,
        IJobProcessor
    {
        #region Properties
        /// <summary>
        /// Branch object.
        /// </summary>
        protected IBranch Branch { get; set; }

        /// <summary>
        /// Configuration object.
        /// </summary>
        protected IConfig Config { get; set; }

        /// <summary>
        /// Universal Log.
        /// </summary>
        protected IUniversalLog Log { get; set; }

        /// <summary>
        /// Content tree.
        /// </summary>
        protected IContentTree Content { get; set; }

        /// <summary>
        /// Package Command to execute.
        /// </summary>
        protected String PackageCommand { get; set; }

        /// <summary>
        /// Package Command to execute.
        /// </summary>
        protected String PackageCommandArguments { get; set; }

        /// <summary>
        /// Manifest file containing changes dependencies.
        /// </summary>
        protected String ManifestFile { get; set; }

        /// <summary>
        /// Root of dependency used if going to upload based on a root dir
        /// </summary>
        protected String ZipRootDir { get; set; }

        /// <summary>
        /// Directory containing dependencies specific to processor
        /// </summary>
        protected List<String> SyncAssetDepDirs { get; set; }

        /// <summary>
        /// Wildcard by which the content tree knows to find processes by
        /// </summary>
        protected String ContentWildcard { get; set; }

        /// <summary>
        /// Container of Build Errors
        /// </summary>
        protected List<String> BuildErrorMessages { get; set; }

        /// <summary>
        /// Whether or not to output build output. For long processes, this causes massive slowdowns
        /// printing msgs no one is reading. 
        /// </summary>
        protected bool PrintBuildOutput { get; set; }

        /// <summary>
        /// Toggle to sync to label.
        /// </summary>
        protected bool SyncToLabel { get; set; }

        /// <summary>
        /// Allows processes to upload files and return success in order to let users grab uploaded files.
        /// </summary>
        protected bool UploadWithErrors { get; set; }

        /// <summary>
        /// Supports core project data
        /// </summary>
        protected bool SupportCore { get; set; }

        /// <summary>
        /// Supports dlc project data
        /// </summary>
        protected bool SupportDLC { get; set; }

        #endregion

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="fileTransferConsumer"></param>
        /// 
        public BasePackageProcessor(CommandOptions options, CapabilityType role, FileTransferServiceConsumer fileTransferConsumer)
            : base(options, role, fileTransferConsumer)
        {
            if (this.Parameters.ContainsKey(PARAM_PACKAGE_COMMAND))
                this.PackageCommand = options.Branch.Environment.Subst((string)this.Parameters[PARAM_PACKAGE_COMMAND]);

            if (this.Parameters.ContainsKey(PARAM_PACKAGE_COMMAND_ARGUMENTS))
                this.PackageCommandArguments = options.Branch.Environment.Subst((string)this.Parameters[PARAM_PACKAGE_COMMAND_ARGUMENTS]);

            if (this.Parameters.ContainsKey(PARAM_INPUT_ZIP_DIR))
                this.ZipRootDir = options.Branch.Environment.Subst((string)this.Parameters[PARAM_INPUT_ZIP_DIR]);

            SupportCore = (this.Parameters.ContainsKey(PARAM_MONITOR_CORE_PROJECT) ? Boolean.Parse(this.Parameters[PARAM_MONITOR_CORE_PROJECT].ToString()) : true);
            SupportDLC = (this.Parameters.ContainsKey(PARAM_MONITOR_DLC_PROJECTS) ? Boolean.Parse(this.Parameters[PARAM_MONITOR_DLC_PROJECTS].ToString()) : true);

            this.SyncAssetDepDirs = new List<String>();
            if (this.Parameters.ContainsKey(PARAM_ASSET_DEP_DIR))
            {
                String[] rawSyncAssetDepDirs = ((string)this.Parameters[PARAM_ASSET_DEP_DIR]).Split(';');
                foreach (String rawDirectory in rawSyncAssetDepDirs)
                {
                    String branchName = this.Options.Branch.Name;
                    if (SupportCore)
                    {
                        Debug.Assert(this.Options.CoreProject.Branches.ContainsKey(branchName),
                        "Core project does not have branch '{0}' defined.", branchName);
                        if (!this.Options.CoreProject.Branches.ContainsKey(branchName))
                            throw (new ArgumentException(String.Format("Core project does not have branch '{0}' defined.", branchName)));

                        string rootProjectLocation = this.Options.CoreProject.Branches[branchName].Environment.Subst(rawDirectory);
                        this.SyncAssetDepDirs.Add(rootProjectLocation);
                    }

                    if (SupportDLC)
                    {
                        Debug.Assert(this.Options.Project.Branches.ContainsKey(branchName),
                        "DLC project does not have branch '{0}' defined.", branchName);
                        if (!this.Options.Project.Branches.ContainsKey(branchName))
                            throw (new ArgumentException(String.Format("DLC project does not have branch '{0}' defined.", branchName)));

                        foreach (KeyValuePair<string, IProject> kvp in this.Options.Config.DLCProjects)
                        {
                            string dlcProjectName = kvp.Key;
                            IProject dlcProject = kvp.Value;
                            if (dlcProject.Branches.ContainsKey(branchName))
                            {
                                String dlcProjectLocation = dlcProject.Branches[branchName].Environment.Subst(rawDirectory);
                                this.SyncAssetDepDirs.Add(dlcProjectLocation);
                            }
                            else
                            {
                                if (Log != null)
                                {
                                    this.Log.WarningCtx(LOG_CTX, "DLC project branch does not exist: {0}", branchName);
                                }
                            }
                        }
                    }
                }
            }

            if (this.Parameters.ContainsKey(PARAM_MANIFEST_FILE))
                this.ManifestFile = options.Branch.Environment.Subst((string)this.Parameters[PARAM_MANIFEST_FILE]);

            if (this.Parameters.ContainsKey(PARAM_SYNC_TO_LABEL))
                this.SyncToLabel = (bool)this.Parameters[PARAM_SYNC_TO_LABEL];

            if (this.Parameters.ContainsKey(PARAM_CONTENT_WILDCARD))
                this.ContentWildcard = (String)this.Parameters[PARAM_CONTENT_WILDCARD];

            if (this.Parameters.ContainsKey(PARAM_UPLOAD_WITH_ERRORS))
                this.UploadWithErrors = (bool)this.Parameters[PARAM_UPLOAD_WITH_ERRORS];

            if (this.Parameters.ContainsKey(PARAM_NO_BUILD_PROC_OUTPUT))
                this.PrintBuildOutput = (bool)this.Parameters[PARAM_NO_BUILD_PROC_OUTPUT];

            if (String.IsNullOrEmpty(this.ZipRootDir))
            {
                if (Log != null)
                {
                    Log.ErrorCtx(LOG_CTX, "Input Zip Directory must be defined for Packaging Processor. See Processor settings xml");
                }
            }

            BuildErrorMessages = new List<String>();
        }
        #endregion // Constructor(s)

        #region Constants
        protected readonly String LOG_CTX = "Base Package Job Processor";
        protected static readonly String PARAM_PACKAGE_COMMAND = "Package Command";
        protected static readonly String PARAM_PACKAGE_COMMAND_ARGUMENTS = "Package Command Arguments";
        protected static readonly String PARAM_MANIFEST_FILE = "Manifest File";
        protected static readonly String PARAM_INPUT_ZIP_DIR = "Input Zip Directory";
        protected static readonly String PARAM_ASSET_DEP_DIR = "Asset Dependency Directory";
        protected static readonly String PARAM_SYNC_TO_LABEL = "Sync to Label";
        protected static readonly String PARAM_NO_BUILD_PROC_OUTPUT = "Print Build Output";
        protected static readonly String PARAM_CONTENT_WILDCARD = "Content Wildcard";
        protected static readonly String PARAM_UPLOAD_WITH_ERRORS = "Upload With Errors";
        protected static readonly String CLEAN_COMAMND = "clean";

        /// <summary>
        /// Parameter to allow DLC projects to be monitored
        /// </summary>
        protected static readonly String PARAM_MONITOR_DLC_PROJECTS = "Monitor DLC Projects";

        /// <summary>
        /// Parameter to allow NON-DLC (CORE) projects to be monitored
        /// </summary>
        protected static readonly String PARAM_MONITOR_CORE_PROJECT = "Monitor Core Project";
        #endregion


        #region Abstract Methods
        protected abstract void GetZipFiles(ref Dictionary<String, List<String>> AllZips);
        #endregion


        #region Controller Methods

        /// <summary>
        /// Synchronizes dependency specific assets.
        /// </summary>
        /// <param name="changelist"></param>
        /// <returns></returns>
        protected bool SyncProcessorSpecificAssets(uint changelist)
        {
            if (!SyncAssetDepDirs.Any())
            {
                Log.MessageCtx(LOG_CTX, "No item \"Asset Dependency Directory\" defined in processor .xml. Using basic sync");

                return true;
            }
            else
            {
                Log.MessageCtx(LOG_CTX, "Syncing xml dependent assets...");

                using (P4 p4 = this.Options.Config.Project.SCMConnect())
                {
                    List<String> args = new List<String>();
                    foreach (String directory in SyncAssetDepDirs)
                    {
                        args.Add(Path.Combine(directory, "...@" + changelist.ToString()));
                    }
                    P4API.P4RecordSet syncRecord = p4.Run("sync", args.ToArray());

                    foreach (String error in syncRecord.Errors)
                    {
                        Log.ErrorCtx(LOG_CTX, error);
                    }

                    return !syncRecord.HasErrors();
                }
            }
        }

        /// <summary>
        /// Recursive deletion of directory.
        /// </summary>
        /// <param name="directory"></param>
        protected void DeleteDirectory(string directory)
        {
            try
            {
                String[] subdirs = System.IO.Directory.GetDirectories(directory);
                foreach (String subdir in subdirs)
                    DeleteDirectory(subdir);

                // If a process has generated sub directories we need to make sure 
                // all files are cleaned up via TryCleanFile.
                String[] files = System.IO.Directory.GetFiles(directory, "*.*");
                foreach (String file in files)
                    System.IO.File.Delete(file);

                if (System.IO.Directory.Exists(directory))
                    System.IO.Directory.Delete(directory);
            }
            catch (System.IO.IOException)
            {
                Log.WarningCtx(LOG_CTX, String.Format("Unable to clean directory {0}.", directory));
            }
        }

        /// <summary>
        /// Syncs all dependencies associated with the job.
        /// </summary>
        /// <returns></returns>
        protected bool SyncDependencies()
        {
            Log.MessageCtx(LOG_CTX, "Syncing dependencies...");

            using (P4 p4 = this.Options.Config.Project.SCMConnect())
            {
                List<string> args = new List<string>();

                //rdr3/tools/bin/...
                if (SyncToLabel == true)
                    args.Add(Path.Combine(this.Options.Config.ToolsBin, String.Format("...@{0}", this.Options.Project.Labels[RSG.Base.Configuration.Label.ToolsCurrent])));
                else
                    args.Add(Path.Combine(this.Options.Config.ToolsBin, "..."));

                //rdr3/tools/etc/content/...
                args.Add(Path.Combine(Options.Config.ToolsConfig, "content", "..."));

                //cutscene specific decals
                args.Add(Path.Combine(this.Options.Branch.Build, "common", "data", "effects", "decals_cs.dat"));
                P4API.P4RecordSet syncRecord = p4.Run("sync", args.ToArray());

                foreach (string error in syncRecord.Errors)
                {
                    Log.ErrorCtx(LOG_CTX, error);
                }

                return !syncRecord.HasErrors();
            }
        }

        /// <summary>
        /// Uploads directory as a single .zip
        /// </summary>
        /// <param name="job"></param>
        /// <returns>result</returns>
        protected bool UploadDirectory(String dir, IEnumerable<String> zipFiles, String downloadCacheDir, String exportZipName, IJob job, bool bSameVMClientAsServer)
        {
            bool result = true;
            using (ZipFile zip = new ZipFile())
            {
                string tmp = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());
                System.IO.Directory.CreateDirectory(tmp);

                foreach (String zipFile in zipFiles)
                {
                    String zipDirectory = Path.GetDirectoryName(zipFile);
                    String zipSubDirectory = zipFile.Replace("/", "\\").Replace(dir, "");
                    System.IO.Directory.CreateDirectory(Path.Combine(tmp, Path.GetDirectoryName(zipSubDirectory)));


                    string destFile = Path.Combine(tmp, Path.GetFileName(zipFile));
                    System.IO.File.Copy(zipFile, destFile, true);
                    System.IO.File.SetAttributes(destFile, FileAttributes.Normal);
                }
                zip.AddDirectory(tmp);
                zip.Save(Path.Combine(downloadCacheDir, exportZipName));
                System.IO.Directory.Delete(tmp, true);
            }
            if (bSameVMClientAsServer)
            {
                try
                {
                    String serverCacheDir = downloadCacheDir.Replace("client", "server");
                    if (System.IO.Directory.Exists(serverCacheDir) == false)
                    {
                        System.IO.Directory.CreateDirectory(serverCacheDir);
                    }
                    System.IO.File.Copy(Path.Combine(downloadCacheDir, exportZipName), Path.Combine(serverCacheDir, exportZipName), true);
                    result &= true;
                }
                catch (System.Exception ex)
                {
                    Log.ErrorCtx(LOG_CTX, "Recieved error while copying files directly: {0}", ex.Message);
                    result = false;
                }
            }
            else
            {
                result &= FileTransferServiceConsumer.UploadFile(job, Path.Combine(downloadCacheDir, exportZipName), exportZipName);
            }
            return result;
        }

        /// <summary>
        /// Uploads all data associated with the job.
        /// </summary>
        /// <param name="job"></param>
        /// <param name="zipFiles"></param>
        /// <returns>result</returns>
        protected virtual bool UploadData(IJob job, Dictionary<String, List<String>> zipFiles)
        {
            //Check for if the client is on the same machine as the server.
            bool bSameVMAsServer = false;
            if (String.Equals(this.FileTransferServiceConsumer.GetClientHostName(), Dns.GetHostName()))
            {
                Log.MessageCtx(LOG_CTX, "This client has the same hostname as the FileTransferClient. Will use a direct copy instead of uploading for speed-up.");
                bSameVMAsServer = true; 
            }

            //Create the zip files that will be uploaded/downloaded
            String downloadCacheDir = FileTransferServiceConsumer.GetClientFileDirectory(job);
            if (System.IO.Directory.Exists(downloadCacheDir) == false)
                System.IO.Directory.CreateDirectory(downloadCacheDir);
            bool result = true;
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            //Must use multiple zip files due to directory being over 2GB in size. 

            // create unique list of directories from input zip files. No need to call GetDirectories(...) again.
            foreach(KeyValuePair<String, List<String>> kvp in zipFiles)
            {
                String projectCacheDir = Path.Combine(downloadCacheDir, kvp.Key);
                if (System.IO.Directory.Exists(projectCacheDir) == false)
                    System.IO.Directory.CreateDirectory(projectCacheDir);

                List<String> projectZipFiles = kvp.Value;

                String projectName = kvp.Key;

                IBranch branch = this.Options.Branch;
				IProject project = branch.Project;

                if (this.Options.Config.AllProjects().ContainsKey(projectName))
                {
                    project = this.Options.Config.AllProjects()[projectName];
                    branch = project.Branches[this.Options.Branch.Name];
                }

                if (project.IsDLC)
                {
                    RSG.GraphML.GraphCollection dlcGraph = new GraphML.GraphCollection(Path.Combine(project.Root, "dlc_content.xml"), branch);

                    this.Content = Factory.CreateTree(branch, dlcGraph);
                }
                else
                {
                    this.Content = Factory.CreateTree(Branch);
                }
				
				List<String> directoriesFromZips = kvp.Value.Select(zipPath => Path.GetDirectoryName(zipPath)).Distinct().ToList();

                foreach (String dir in directoriesFromZips)
                {
                    Log.MessageCtx(LOG_CTX, String.Format("Beginning upload of Dir: {0}", dir));
                    String lastDirectoryName = Path.GetFileName(dir);
                    String[] localZipFiles = System.IO.Directory.GetFiles(dir);
                    if (localZipFiles.Any())
                    {
                        result &= UploadDirectory(dir, localZipFiles, projectCacheDir, String.Format("export{0}.zip", lastDirectoryName), job, bSameVMAsServer);
                        if (result == false)
                        {
                            Log.ErrorCtx(LOG_CTX, "Error uploading files in {0} to the server.", dir);
                        }
                    }
                }

                Log.MessageCtx(LOG_CTX, "Done uploading .zip's"); 

                if (result == false)
                {
                    Log.ErrorCtx(LOG_CTX, "Unable to upload asset/export/... *.zips to the server.");
                    return false;
                }

                foreach (ITarget target in branch.Targets.Values)
                {
                    if (target.Enabled == false)
                        continue;

                    String resourcedZipName = target.Platform.ToString() + "resourced.zip";

                    using (ZipFile zip = new ZipFile())
                    {
                        string tmp = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());
                        System.IO.Directory.CreateDirectory(tmp);

                        //Acquire the anim build directory from the content system; have to use the 
                        string animBuildDirectory = null;
                        foreach (String zipFile in projectZipFiles)
                        {
                            String zipDirectory = Path.GetDirectoryName(zipFile);
                            IContentNode zipFileParentNode = this.Content.GetParentDirectoryFromWildcard(zipFile);

                            IContentNode zipFileDirectoryNode;
                            if (zipFileParentNode == null)
                            {
                                zipFileDirectoryNode = Content.CreateDirectory(zipDirectory, this.ContentWildcard);
                            }
                            else
                            {
                                zipFileDirectoryNode = zipFileParentNode;
                            }

                            IEnumerable<IProcess> zipProcesses = Content.FindProcessesWithInput(zipFileDirectoryNode);
                            if (zipProcesses.Count() == 0)
                            {
                                Log.WarningCtx(LOG_CTX, String.Format("Unable to find outputs for {0}.  Data for this scene will not be uploaded.", Path.GetFileName(zipFile)));
                            }
                            else
                            {
                                IProcess process = null;
                                try
                                {
                                    process = zipProcesses.First();
                                }
                                catch (AggregateException e)
                                {
                                    foreach (var ex in e.InnerExceptions)
                                    {
                                        Log.ErrorCtx(LOG_CTX, ex.Message);
                                        if (ex is IndexOutOfRangeException)
                                            Log.ErrorCtx(LOG_CTX, "The data source is corrupt. Query stopped.");
                                    }
                                    process = zipProcesses.First();
                                }
                                IContentNode outputNode = process.Outputs.First();

                                if (outputNode is IFilesystemNode)
                                {
                                    string rpfFileName = (outputNode as IFilesystemNode).AbsolutePath;
                                    string rawFileName = Path.GetFileName(zipFile);
                                    rawFileName = rawFileName.Substring(0, rawFileName.IndexOf('.'));
                                    rpfFileName = rpfFileName.Replace("$(filename)", rawFileName);

                                    IContentNode assetNode = outputNode.Owner.CreateAsset(rpfFileName, target.Platform);

                                    string rpfFile = target.Environment.Subst((assetNode as IFilesystemNode).AbsolutePath);
									animBuildDirectory = Path.GetDirectoryName(rpfFile);

                                    break;
                                }
                            }
                        }
                        if (animBuildDirectory != null)
                        {
                            //Create the directory.
                            System.IO.Directory.CreateDirectory(animBuildDirectory);

                            //uploading all local .rpfs
                            string[] rpfFiles = System.IO.Directory.GetFiles(animBuildDirectory, "*.rpf");
                            foreach (string rpfFile in rpfFiles)
                            {
                                string destFile = Path.Combine(tmp, Path.GetFileName(rpfFile));
                                System.IO.File.Copy(rpfFile, destFile, true);
                                System.IO.File.SetAttributes(destFile, FileAttributes.Normal);
                            }
                        }
                        else
                        {
                            Log.ErrorCtx(LOG_CTX, "Could not find the rpf directory. No rpf files will be uploaded.");
                        }

                        zip.AddDirectory(tmp);
                        zip.Save(Path.Combine(projectCacheDir, resourcedZipName));
                        System.IO.Directory.Delete(tmp, true);
                    }
                    if (bSameVMAsServer)
                    {
                        try
                        {
                            System.IO.File.Copy(Path.Combine(projectCacheDir, resourcedZipName), Path.Combine(projectCacheDir.Replace("client", "server"), resourcedZipName));
                            result &= true;
                        }
                        catch (System.Exception ex)
                        {
                            Log.ErrorCtx(LOG_CTX, "Got exception when directly copying to server directory:\n{0}", ex.Message);
                            result = false;
                        }
                    }
                    else
                    {
                        result &= FileTransferServiceConsumer.UploadFile(job, Path.Combine(projectCacheDir, resourcedZipName), resourcedZipName);
                    }
                    if (result == false)
                    {
                        Log.ErrorCtx(LOG_CTX, String.Format("Unable to upload {0} to the server.", resourcedZipName));
                        return false;
					}
                }
            }
            stopwatch.Stop();
            Log.MessageCtx(LOG_CTX, String.Format("Elapsed Time Uploading Files: {0}.", stopwatch.Elapsed));
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public override IJobResult Process(IJob job)
        {
            Debug.Assert(job.Role == CapabilityType.CutscenePackage || job.Role == CapabilityType.AnimationPackage || job.Role == CapabilityType.PtfxPackage,
                        String.Format("Invalid job for processor: {0}.", job.GetType().Name));
            if (job.Role != CapabilityType.CutscenePackage && job.Role != CapabilityType.AnimationPackage && job.Role != CapabilityType.PtfxPackage)
            {
                return null;
            }

            Log = LogFactory.CreateUniversalLog("process");

            Log.MessageCtx(LOG_CTX, "Starting packaging process...");

            this.Branch = this.Options.Branch;
            this.Config = this.Options.Config;

            if (SyncDependencies() == false)
            {
                Log.ErrorCtx(LOG_CTX, "Syncing dependencies failed.  Unable to process job.");
                return null;
            }

            IEnumerable<uint> clNums = job.Trigger.ChangelistNumbers();

            if (clNums.Any())
            {
                uint clMax = clNums.Max();
                SyncProcessorSpecificAssets(clMax);
            }
            else if (job.Trigger is TimedTrigger)
            {
                Log.MessageCtx(LOG_CTX, "Processing TimedTrigger ----- Going to begin process with files currently on disk.");
            }
            else
            {
                Log.ErrorCtx(LOG_CTX, String.Format("Unknown trigger type: {0}.  Unable to process job.", job.Trigger.GetType().ToString()));
                return null;
            }

            JobResult jobResult = new JobResult(job.ID, false);

            bool result = Package(job);

            if (result == false)
            {
                if (!this.UploadWithErrors)
                {
                    Log.ErrorCtx(LOG_CTX, "Packaging has failed.  Aborting job...");
                    return jobResult;
                }
            }

            Dictionary<String, List<String>> zipFiles = new Dictionary<String, List<String>>();
            GetZipFiles(ref zipFiles);

            if (zipFiles.Count == 0)
            {
                Log.WarningCtx(LOG_CTX, String.Format("No zip files were changed.  No files will be uploaded."));
            }
            else
            {
                result &= UploadData(job, zipFiles);

                String downloadCacheDir = FileTransferServiceConsumer.GetClientFileDirectory(job);
                if (System.IO.Directory.Exists(downloadCacheDir))
                {
                    DeleteDirectory(downloadCacheDir);
                }
            }

            LogFactory.CloseUniversalLog(Log);

            //Acquire all of the logs.
            MergeAndUploadLogs(job);
			
            jobResult.Succeeded = result;
            return jobResult;
        }


        /// <summary>
        /// Handler for Prebuild commands
        /// </summary>
        /// <returns></returns>
        protected bool Prebuild(IJob job, ref bool bIsClean)
        {
            if (job.PrebuildCommands == null || !job.PrebuildCommands.Any())
            {
                Log.MessageCtx(LOG_CTX, "--- No prebuild commands ---");
                return true;
            }

            Log.MessageCtx(LOG_CTX, " ");
            Log.MessageCtx(LOG_CTX, "<*******************************> ");
            Log.MessageCtx(LOG_CTX, "<* Prebuild running {0} commands *>", job.PrebuildCommands.Count());
            Log.MessageCtx(LOG_CTX, "<*******************************> ");
            Log.ProfileCtx(LOG_CTX, "Prebuild");
            return ExecutePrebuild(job, ref bIsClean);
        }


        /// <summary>
        /// Handler for Postbuild commands
        /// </summary>
        /// <returns></returns>
        protected bool Postbuild(IJob job)
        {
            if (job.PostbuildCommands == null || !job.PostbuildCommands.Any())
            {
                Log.MessageCtx(LOG_CTX, "--- No postbuild commands ---");
                return true;
            }

            Log.MessageCtx(LOG_CTX, " ");
            Log.MessageCtx(LOG_CTX, "<*******************************> ");
            Log.MessageCtx(LOG_CTX, "<* Postbuild running {0} commands *>", job.PostbuildCommands.Count());
            Log.MessageCtx(LOG_CTX, "<*******************************> ");
            Log.ProfileCtx(LOG_CTX, "Postbuild");
            return ExecutePostbuild(job);
        }


        /// <summary>
        /// Executes Prebuild command work
        /// </summary>
        /// <returns></returns>
        protected bool ExecutePrebuild(IJob job, ref bool bIsClean)
        {
            foreach (String command in job.PrebuildCommands)
            {
                if (String.Equals(command, CLEAN_COMAMND, StringComparison.OrdinalIgnoreCase))
                {
                    bIsClean = true; 
                    try
                    {
                        //Got a rebuild command. Dump out this.ExportSubDirectory
                        Log.MessageCtx(LOG_CTX, "Got a clean command. Deleting all of {0}.", this.ZipRootDir);
                        foreach (String file in System.IO.Directory.GetFiles(this.ZipRootDir, "*", SearchOption.AllDirectories))
                        {
                            System.IO.File.SetAttributes(file, FileAttributes.Normal);
                        }
                        String[] prevDirectories = System.IO.Directory.GetDirectories(this.ZipRootDir);
                        System.IO.Directory.Delete(this.ZipRootDir, true);
                        System.IO.Directory.CreateDirectory(this.ZipRootDir);
                        foreach (String oldDir in prevDirectories)
                        {
                            System.IO.Directory.CreateDirectory(oldDir);
                        }
                    }
                    catch (System.IO.DirectoryNotFoundException)
                    {
                        Log.WarningCtx(LOG_CTX, "Couldn't find directory: {0}.", this.ZipRootDir);
                        //This is backup in case the directory didn't exist originally. 
                        if (!System.IO.Directory.Exists(this.ZipRootDir))
                        {
                            System.IO.Directory.CreateDirectory(this.ZipRootDir);
                        }
                    }
                }
            }
            return true;
        }


        /// <summary>
        /// Executes Postbuild command work
        /// </summary>
        /// <returns></returns>
        protected bool ExecutePostbuild(IJob job)
        {
            //Nothing to handle for this specifically in rebuilds
            return true;
        }

        /// <summary>
        /// Launches the packaging process.
        /// </summary>
        /// <returns></returns>
        protected virtual bool Package(IJob job)
        {
            if (String.IsNullOrWhiteSpace(this.PackageCommand) == true)
            {
                Log.ErrorCtx(LOG_CTX, "The Package Command processor setting is not defined.  Unable to process job.");
                return false;
            }

            //Clear out any log files so we can track status of the called process.
            ClearLogFiles();

            BuildErrorMessages = new List<String>();

            //Handle Prebuild Commands
            bool bIsClean = false;
            Prebuild(job, ref bIsClean);

            //The command exists within the processor.
            System.Diagnostics.ProcessStartInfo processStartInfo = new System.Diagnostics.ProcessStartInfo();
            processStartInfo.FileName = Options.Branch.Environment.Subst(this.PackageCommand);
            List<String> cmdArgs = new List<String>();

            if (SupportCore)
            {
                cmdArgs.Add("--supportcore");
            }

            if (SupportDLC)
            {
                cmdArgs.Add("--supportdlc");
            }

            if (bIsClean)
            {
				//Tell the .zip script to rebuild since we just cleaned out the packager.
                cmdArgs.Add("--rebuild");
            }

            if (String.IsNullOrWhiteSpace(this.PackageCommandArguments) == false)
            {
                string arguments = this.PackageCommandArguments.Replace("$(manifest_file)", ManifestFile);
                arguments = Options.Branch.Environment.Subst(arguments);  //special replacement so to avoid duplicating data in the XML file.
                cmdArgs.Add(arguments);
            }
            processStartInfo.Arguments = String.Join(" ", cmdArgs);

            Log.MessageCtx(LOG_CTX, "Executing " + processStartInfo.FileName + " " + processStartInfo.Arguments + "...");

            processStartInfo.UseShellExecute = false;
            if (PrintBuildOutput)
            {
                processStartInfo.RedirectStandardOutput = true;
                processStartInfo.RedirectStandardError = true;
            }

            System.Diagnostics.Process buildProcess = RunProcess(Log, processStartInfo, ReadStandardOutput, ReadStandardError);

            if (buildProcess.ExitCode != 0 || BuildErrorMessages.Any())
            {
                Log.ErrorCtx(LOG_CTX, String.Format("Error has occurred executing the command: {0} {1}.  Exit code: {2}. Error Message Count: {3}.",
                        processStartInfo.FileName, processStartInfo.Arguments, buildProcess.ExitCode, BuildErrorMessages.Count()));
                if (buildProcess.ExitCode == 0)
                {
                    //Repeat any messages that are being flagged as errors
                    Log.ErrorCtx(LOG_CTX, "Repeating all errors as the exit code was 0. This is to help debug what the problem was.");
                    foreach (String errorMsg in BuildErrorMessages)
                    {
                        Log.ErrorCtx(LOG_CTX, errorMsg);
                    }
                }
                return false;
            }

            //Handle Postbuild commands
            Postbuild(job);
            return true;
        }

        /// <summary>
        /// Handle stdout while a processing is running.
        /// </summary>
        /// <param name="logObj">the log</param>
        /// <param name="obj">the string</param>
        protected virtual void ReadStandardOutput(object logObj, object obj)
        {
            IUniversalLog log = (IUniversalLog)logObj;
            String line = obj as String;
            if (line == null)
            {
                log.ToolErrorCtx(LOG_CTX, "Non String object send to ReadStandardError");
                return;
            }

            try
            {
                ParseLine(line);
            }
            catch (FormatException fe) // Used to catch issues with formatting log messages
            {
                log.WarningCtx(LOG_CTX, String.Format("Error Occured while logging from stream stdout {0}", fe.ToString()));
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Exception parsing stdout stream.");
            }
        }

        /// <summary>
        /// Handle stderr while a processing is running.
        /// </summary>
        /// <param name="logObj">the log</param>
        /// <param name="obj">the string</param>
        protected virtual void ReadStandardError(object logObj, object obj)
        {
            IUniversalLog log = (IUniversalLog)logObj;
            String line = obj as String;
            if (line == null)
            {
                log.ToolErrorCtx(LOG_CTX, "Non String object send to ReadStandardError");
                return;
            }

            try
            {
                if (!String.IsNullOrWhiteSpace(line))
                {
                    log.ErrorCtx(LOG_CTX, line);
                    BuildErrorMessages.Add(line);
                }
            }
            catch (FormatException fe) // Used to catch issues with formatting log messages
            {
                log.WarningCtx(LOG_CTX, String.Format("Error Occured while logging from stream stderr {0}", fe.ToString()));
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Exception parsing stderr stream.");
            }
        }

        /// <summary>
        /// Parses the line of output from the build process.
        /// </summary>
        /// <param name="line"></param>
        protected virtual void ParseLine(string line)
        {
            if (line.Contains(": error") == true || line.Contains(": fatal error") == true || line.Contains("Build FAILED"))
            {
                Log.ErrorCtx(LOG_CTX, line);
                BuildErrorMessages.Add(LOG_CTX + line);
            }
            else if (line.Contains(": warning") == true)
            {
                Log.WarningCtx(LOG_CTX, line);
            }
            else
            {
                Log.MessageCtx(LOG_CTX, line);
            }
        }

        /// <summary>
        /// Clears all log files from the local client.
        /// </summary>
        /// <returns></returns>
        protected bool ClearLogFiles()
        {
            try
            {
                if (System.IO.Directory.Exists(Config.ToolsLogs))
                    System.IO.Directory.Delete(Config.ToolsLogs, true);
            }
            catch (Exception /*e*/)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Scoops up all local logs to determine if there's been an error.  Creates a single log.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="job"></param>
        /// <returns></returns>
        protected bool MergeAndUploadLogs(IJob job)
        {
            String destinationDir = this.FileTransferServiceConsumer.GetClientFileDirectory(job);
            if (!System.IO.Directory.Exists(destinationDir))
                System.IO.Directory.CreateDirectory(destinationDir);

            string[] uLogFilesToMerge = System.IO.Directory.GetFiles(Config.ToolsLogs, "*.ulog", SearchOption.AllDirectories);
            String destinationFilename = Path.Combine(destinationDir, "job.ulog");
            UniversalLogFile.MergeLogs(destinationFilename, uLogFilesToMerge);

            bool result = true;

            if (UniversalLogFile.ContainsErrors(destinationFilename))
            {
                result = false;
                Log.Error("Job failed: Merged log contains errors - {0}", destinationFilename);
            }

            if (!this.FileTransferServiceConsumer.UploadFile(job, destinationFilename))
            {
                result = false;
                Log.Error("Job failed: Upload of merged log failed - {0}", destinationFilename);
            }

            return result;
        }
        #endregion // Controller Methods
    }
}
