﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Interop.Autodesk3dsmax;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Export;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Services;
using RSG.SourceControl.Perforce;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{

    /// <summary>
    /// Map Export Base Job Processor; shared by Map Export and Map Network
    /// Export processors as there is a lot of common functionality.
    /// </summary>
    internal abstract class MapExportJobProcessorBase : 
        JobProcessorBase,
        IJobProcessor
    {
        #region Constants
#warning DHM FIX ME: data-drive this from our project configuration data
        protected static readonly MaxVersion TARGET_MAX_VERSION = MaxVersion.Autodesk3dsmax2012;
        protected static readonly String MXS_BATCH_SCRIPT = "pipeline/export/maps/mapBatchExport.ms";
        protected static readonly String LOG_CTX = "MapExport Job Processor";

        /// <summary>
        /// Parameter to tell system whether to check out the map export data.
        /// </summary>
        protected static readonly String PARAM_PERFORCE_CHECKOUT = "Perforce Checkout";

        /// <summary>
        /// Parameter to tell system whether to use the map content-node's
        /// auto_export flag.
        /// </summary>
        protected static readonly String PARAM_USE_AUTOEXPORT_FLAG = "Use AutoExport Content Flag";
        #endregion // Constants

        #region Enumerations
        /// <summary>
        /// 
        /// </summary>
        internal enum FileMode
        {
            ForceWritable,
            Checkout,
        }
        #endregion // Enumerations

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="capability"></param>
        /// <param name="fileTransferConsumer"></param>
        public MapExportJobProcessorBase(CommandOptions options, 
            CapabilityType capability, FileTransferServiceConsumer fileTransferConsumer)
            : base(options, capability, fileTransferConsumer)
        {
        }
        #endregion // Constructor(s)

        #region Protected Methods (Shared Map Verification and Export Features)
        /// <summary>
        /// Verify map job has valid export processes defined.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="logFile"></param>
        /// <param name="job"></param>
        /// <returns></returns>
        protected virtual bool VerifyMapJobProcess(IUniversalLog log, MapExportJob job)
        {
            if ((null == job.ExportProcesses) || (0 == job.ExportProcesses.Count))
            {
                log.ErrorCtx(LOG_CTX, "Map job export information is empty.  No map sections.  Aborting job.");
                return (false);
            }
                
            return (true);
        }

        /// <summary>
        /// Verify Autodesk 3dsmax install and return absolute executable filename.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="logFile"></param>
        /// <param name="job"></param>
        /// <param name="exeFilename"></param>
        /// <returns></returns>
        protected virtual bool VerifyAutodesk3dsmaxInstall(IUniversalLog log, MapExportJob job, out String exeFilename)
        {
            if (!Autodesk3dsmax.IsVersionInstalled(TARGET_MAX_VERSION, true))
            {
                log.ErrorCtx(LOG_CTX, "3dsmax expected version {0} not available.  Aborting job.",
                    TARGET_MAX_VERSION);
                exeFilename = String.Empty;
                return (false);
            }

            String maxExe = Autodesk3dsmax.GetExecutablePath(TARGET_MAX_VERSION, true);
            if (!File.Exists(maxExe))
            {
                log.ErrorCtx(LOG_CTX, "3dsmax executable '{0}' not found.  Aborting job.",
                    maxExe);
                exeFilename = String.Empty;
                return (false);
            }

            exeFilename = maxExe;
            return (true);
        }

        /// <summary>
        /// Handle single ExportProcess as part of a job.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="logFile"></param>
        /// <param name="exeFilename">Autodesk 3dsmax Executable Filename</param>
        /// <param name="mapJob"></param>
        /// <param name="exportProcess"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        protected virtual bool Export(IUniversalLog log, ILogTarget logFile, String exeFilename, MapExportJob mapJob, ExportProcess exportProcess, FileMode mode)
        {
            bool result = true;
            log.MessageCtx(LOG_CTX, "Job {0} exporting: {1}.", mapJob.ID,
                exportProcess.DCCSourceFilename);

            foreach (String exportFile in exportProcess.ExportFiles)
            {
                if (FileMode.Checkout == mode)
                {
                    log.MessageCtx(LOG_CTX, "Check out: {0}.", exportFile);
#warning DHM FIX ME: handle checkout?
                    throw new NotImplementedException();
                }
                else if (File.Exists(exportFile))
                {
                    log.WarningCtx(LOG_CTX, "Marking writable: {0}.", exportFile);
                    FileAttributes attributes = File.GetAttributes(exportFile);
                    attributes = attributes & ~FileAttributes.ReadOnly;
                    File.SetAttributes(exportFile, attributes);
                }
            }

            StringBuilder arguments = new StringBuilder();
            arguments.AppendFormat("-silent -mxs \"global RsMapBatchExport_newExport = True;global RsAutoSubmitChangelists = false;");
            if (exportProcess.ExportPlatforms != null && exportProcess.ExportPlatforms.Any())
            {
                String platformOverrides = String.Join(";", exportProcess.ExportPlatforms.Select(p => p.ToString()));
                arguments.AppendFormat("global RsOverridePlatforms = \\\"{0}\\\";", platformOverrides);
            }
            if (mapJob.BranchName != null && mapJob.BranchName != String.Empty)
            {
                arguments.AppendFormat("global RsMapBranchRequested = \\\"{0}\\\";", mapJob.BranchName);
            }
            arguments.AppendFormat("fileIn \\\"{0}\\\"; RsMapBatchExportStart #(", MXS_BATCH_SCRIPT);
            arguments.AppendFormat("\\\"{0}\\\"", exportProcess.DCCSourceFilename);
            arguments.Append(") CloseMaxOnFinish:true perforceIntegration:true\"");

            exportProcess.ExportAt = DateTime.UtcNow;
            log.ProfileCtx(LOG_CTX, "3dsmax Export Start: {0}.", exportProcess.DCCSourceFilename);

            ProcessStartInfo info = new ProcessStartInfo();
            info.UseShellExecute = false;
            info.FileName = exeFilename;
            info.Arguments = arguments.ToString();
            Process process = new Process();
            process.StartInfo = info;
            if ((null == process) || (!process.Start()))
            {
                log.ErrorCtx(LOG_CTX, "Failed to invoke Autodesk 3dsmax {0} for map export.  Aborting job.", exeFilename);
                log.ErrorCtx(LOG_CTX, "Command: '{0}'.", process.StartInfo.FileName);
                log.ErrorCtx(LOG_CTX, "Arguments: '{0}'", String.Join(" ", process.StartInfo.Arguments));
                return (false);
            }

            log.MessageCtx(LOG_CTX, "Autodesk 3dsmax launched; export will happen in background.");
            log.MessageCtx(LOG_CTX, "Command: {0}.", process.StartInfo.FileName);
            log.MessageCtx(LOG_CTX, "Arguments: {0}.", String.Join(" ", process.StartInfo.Arguments));
            process.WaitForExit();

            log.MessageCtx(LOG_CTX, "Process Exit Code: {0}.", process.ExitCode);

#if false
            // AJM: There were some occasions where MAX would crash when exiting, even though the export had
            // actually went through fine, but this being set to false would scupper the entire export.
            // Removing this for now to see how we do without this.
            if (0 != process.ExitCode)
                result &= false;
#endif 
            log.ProfileEnd();
            logFile.Flush();
            exportProcess.ExportTime = DateTime.UtcNow - exportProcess.ExportAt;

            return (result);
        }

        /// <summary>
        /// Merge and upload logs for this job; this is the resultant Universal 
        /// Log for the job.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="logFile"></param>
        /// <param name="job"></param>
        /// <returns></returns>
        protected virtual bool MergeLogsScanErrorAndUpload(IUniversalLog log, ILogTarget logFile, IJob job)
        {
            logFile.Flush();
            String mergedLogFilename = CopyMergeAndCleanLogFiles(log, job);

            // Load and parse log;
            bool result = true;

            if (!String.IsNullOrEmpty(mergedLogFilename))
            {
                if (UniversalLogFile.ContainsErrors(mergedLogFilename))
                {
                    result = false;
                    log.Error("Job failed: Merged log contains errors - {0}", mergedLogFilename);
                }

                if (!this.FileTransferServiceConsumer.UploadFile(job, mergedLogFilename))
                {
                    result = false;
                    log.Error("Job failed: Upload of merged log failed - {0}", mergedLogFilename);
                }
                else
                {
                    // Delete files if the uLog upload was successful
                    DeleteJobFiles(log, job);
                }
            }
            else
            {
                log.Error("No merged uLog was created for the job!");
            }

            return result;
        }

        /// <summary>
        /// Default MapExport implementation of client suitability to run a job processor.  
        /// </summary>
        /// <returns></returns>
        public override bool CheckClientSuitability()
        {
            if (!Autodesk3dsmax.IsVersionInstalled(TARGET_MAX_VERSION, true))
            {
                Log.Log__Error("3dsmax is not installed!");
                return (false); 
            }
            return (true);
        }
        #endregion // Protected Methods (Shared Map Verification and Export Features)
    }

} // RSG.Pipeline.Automation.ClientWorker.Jobs namespace
