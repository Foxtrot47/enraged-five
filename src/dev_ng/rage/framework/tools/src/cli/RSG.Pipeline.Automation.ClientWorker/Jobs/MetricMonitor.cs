﻿using System;
using SIO = System.IO;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Threading;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Platform;
using RSG.UniversalLog.Util;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{
    class MetricMonitor : IMetricMonitor
    {
        const int NEWBASELEVEL = 100;

        #region Constants
        protected static readonly String LOG_CTXMM = "Metric Monitor";
        private static readonly String FPS_THRESHOLD = "FPS Threshold";
        private static readonly String MS_PER_FRAME_THRESHOLD = "MS Per Frame Threshold";
        private static readonly String EXE_SIZE_THRESHOLD = "Exe Size Threshold";
        private static readonly String VIRT_SLOSH_THRESHOLD = "Virt Slosh Threshold";
        private static readonly String PHYS_SLOSH_THRESHOLD = "Phys Slosh Threshold";
        private static readonly String MEM_BUCKET_THRESHOLD = "Mem Bucket Threshold";
        private static readonly String MEM_HEAP_THRESHOLD = "Mem Heap Threshold";
        private static readonly String STREAMING_MODULE_THRESHOLD = "Streaming Module Threshold";
        private static readonly String MEMORY_BUCKET_NAMES = "Memory Bucket Names";

        #endregion

        #region Properties

        private float FPSThreshold;
        private float MSPerFrameThreshold;
        private UInt64 ExeSizeThreshold;
        private UInt64 VirtSloshThreshold;
        private UInt64 PhysSloshThreshold;
        private UInt64 MemBucketThreshold;
        private List<MemoryHeap> MemHeapThreshold;
        private List<StreamingModule> StreamingModuleThreshold;
        private IList<String> MemBucketNames = null;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructor.
        /// </summary>
        /// 
        public MetricMonitor(CommandOptions options, IDictionary<String, Object> parameters, RSG.Platform.Platform platform, 
                             String exePath, IUniversalLog log, IBranch branch, IList<String> mailRecipients, 
                             IList<ISmoketestTarget> targets)
            : base(options, parameters, platform, exePath, log, branch, mailRecipients, targets)
        {
            MetricName = "Perf";

            if (parameters.ContainsKey(FPS_THRESHOLD))
            {
                this.FPSThreshold = Convert.ToSingle(parameters[FPS_THRESHOLD]);
            }

            if (parameters.ContainsKey(MS_PER_FRAME_THRESHOLD))
            {
                this.MSPerFrameThreshold = Convert.ToSingle(parameters[MS_PER_FRAME_THRESHOLD]);
            }

            if (parameters.ContainsKey(EXE_SIZE_THRESHOLD))
            {
                this.ExeSizeThreshold = Convert.ToUInt64(parameters[EXE_SIZE_THRESHOLD]);
            }
            if (parameters.ContainsKey(VIRT_SLOSH_THRESHOLD))
            {
                this.VirtSloshThreshold = Convert.ToUInt64(parameters[VIRT_SLOSH_THRESHOLD]);
            }
            if (parameters.ContainsKey(PHYS_SLOSH_THRESHOLD))
            {
                this.PhysSloshThreshold = Convert.ToUInt64(parameters[PHYS_SLOSH_THRESHOLD]);
            }
            if (parameters.ContainsKey(MEM_BUCKET_THRESHOLD))
            {
                this.MemBucketThreshold = Convert.ToUInt64(parameters[MEM_BUCKET_THRESHOLD]);
            }
            if (parameters.ContainsKey(STREAMING_MODULE_THRESHOLD))
            {
                this.StreamingModuleThreshold = new List<StreamingModule>();

                foreach (Object strObj in (List<Object>)parameters[STREAMING_MODULE_THRESHOLD])
                {
                    StreamingModule threshold = new StreamingModule();
                    String[] values = ((String)strObj).Split(';');

                    if (values.Length != 5)
                    {
                        log.ErrorCtx(LOG_CTXMM, "STREAMING_MODULE_THRESHOLD mismatch.  Expecting 6 parameters per module.  Found {0}", values.Length);
                    }

                    threshold.Name = values[0];
                    threshold.LoadedMemVirt.Value = Convert.ToUInt64(values[1]);
                    threshold.LoadedMemPhys.Value = Convert.ToUInt64(values[2]);
                    threshold.RequiredMemVirt.Value = Convert.ToUInt64(values[3]);
                    threshold.RequiredMemPhys.Value = Convert.ToUInt64(values[4]);

                    StreamingModuleThreshold.Add(threshold);
                }
            }

            if (parameters.ContainsKey(MEM_HEAP_THRESHOLD))
            {
                this.MemHeapThreshold = new List<MemoryHeap>();

                foreach(Object strObj in (List<Object>)parameters[MEM_HEAP_THRESHOLD])
                {
                    MemoryHeap threshold = new MemoryHeap();
                    String[] values = ((String)strObj).Split(';');
                    if (values.Length != 6)
                    {
                        log.ErrorCtx(LOG_CTXMM, "MEM_HEAP_THRESHOLD mismatch.  Expecting 6 parameters per heap.  Found {0}", values.Length);
                    }
                    threshold.Name = values[0];
                    threshold.Used.Value = Convert.ToUInt64(values[1]);
                    threshold.Free.Value = Convert.ToUInt64(values[2]);
                    threshold.Total.Value = Convert.ToUInt64(values[3]);
                    threshold.Peak.Value = Convert.ToUInt64(values[4]);
                    threshold.Frag.Value = Convert.ToUInt64(values[5]);

                    MemHeapThreshold.Add(threshold);
                }
            }

            if (parameters.ContainsKey(MEMORY_BUCKET_NAMES))
            {
                String theBuckets = (String)parameters[MEMORY_BUCKET_NAMES];
                this.MemBucketNames = new List<String>(theBuckets.Split(new char[] { ';' }));
            }
        }
        #endregion

        public override void AllocatePerfMetrics()
        {
            for (int local = 0; local < LocationNames.Count(); local++)
            {
                LocationPerfMetrics[local] = new PerfMetrics(DBServerName, DBName, Log);

                //much of this is unnecessary
                LocationPerfMetrics[local].Set(0.0f);
                LocationProcessed[local] = false;
                LocationMetricsScore[local] = 0;
                LocationLastProcessed[local] = null;
                LocationLastProcessedDataTime[local] = DateTime.MinValue;
            }

            Thresholds = new PerfMetrics(Log, FPSThreshold, MSPerFrameThreshold, ExeSizeThreshold, PhysSloshThreshold, VirtSloshThreshold);
        }

        protected override String AppendMailSubject(int locationIndex)
        {
            String subjectAddition = String.Format(" v{0} CL{1}",
                                    LocationPerfMetrics[locationIndex].BuildVersion, LocationPerfMetrics[locationIndex].CL);
            return subjectAddition;
        }

        public override String FailMailFirstCrash(StringBuilder emailBody)
        {
            bool bFirstCrash = true;
            String crashLocation = "";
            for (int local = 0; local < LocationNames.Count(); local++)
            {
                emailBody.AppendLine("\n========= " + LocationNames.ElementAt(local) + " ========");
                if (LocationPerfMetrics[local].TestComplete)
                {
                    emailBody.AppendLine("PASSED");
                }
                else if (LocationPerfMetrics[local].Crashed)
                {
                    emailBody.AppendLine("CRASHED");
                    if (bFirstCrash)
                    {
                        crashLocation = LocationNames.ElementAt(local);
                        bFirstCrash = false;
                        return crashLocation;
                    }
                }
            }
            return crashLocation;
        }

        public override void FailMailRemainders(StringBuilder emailBody)
        {
            bool bFirstCrash = true;
            for (int local = 0; local < LocationNames.Count(); local++)
            {
                if (LocationPerfMetrics[local].Crashed)
                {
                    if (bFirstCrash)
                    {
                        bFirstCrash = false;
                    }
                }
                else if(!bFirstCrash)
                {
                    emailBody.AppendLine("\n========= " + LocationNames.ElementAt(local) + " ========");
                    emailBody.AppendLine("DID NOT RUN");
                }
            }
        }

        protected override bool ProcessFile(String filename, PerfMetrics outPerf)
        {
            //<fpsResult type="framesPerSecondResult">
            //  <min value="17.506994" />
            //  <max value="33.350777" />
            //  <average value="29.823919" />
            //  <std value="0.876562" />
            //</fpsResult>
            //Element has only one attribute, framesPerSecondResult.  Has nodes with min,max,ave,std elements
            this.Log.MessageCtx(LOG_CTXMM, "Processing file {0}", filename);

            try
            {

                XDocument doc = XDocument.Load(filename);

                //@TODO tried a few slick ways to do this, but none have worked
                IEnumerable<XElement> platformResults = doc.XPathSelectElements("/debugLocationMetricsList/platform");
                foreach (XElement attribute in platformResults)
                {
                    if (String.Equals(attribute.Name.ToString(), "platform", StringComparison.CurrentCultureIgnoreCase))
                    {
                        //this.Log.MessageCtx(LOG_CTXMMM "Found platform: {0}", attribute.Value);
                        outPerf.Platform = RSG.Platform.PlatformUtils.PlatformFromString(attribute.Value);
                        break;
                    }
                }

            IEnumerable<XElement> buildResults = doc.XPathSelectElements("/debugLocationMetricsList/buildversion");
            foreach (XElement attribute in buildResults)
            {
                if (String.Equals(attribute.Name.ToString(), "buildversion", StringComparison.CurrentCultureIgnoreCase))
                {
                    //Build version on NG is a string
                    //float actualValue = Convert.ToSingle(attribute.Value);
                    //this.Log.MessageCtx(LOG_CTXMM, "Found buildversion: {0}", actualValue);
                    outPerf.BuildVersion = attribute.Value.ToString();
                    break;
                }
            }

                IEnumerable<XElement> clResults = doc.XPathSelectElements("/debugLocationMetricsList/changeList");
                foreach (XElement attribute in clResults)
                {
                    if (String.Equals(attribute.Name.ToString(), "changeList", StringComparison.CurrentCultureIgnoreCase))
                    {
                        int actualValue = Convert.ToInt32(attribute.Value);
                        //this.Log.MessageCtx(LOG_CTXM, "Found CL{0}", actualValue);
                        outPerf.CL = actualValue;
                        break;
                    }
                }

                IEnumerable<XElement> exeResults = doc.XPathSelectElements("/debugLocationMetricsList/exeSize");
                foreach (XElement attribute in exeResults)
                {
                    String[] valueEquals = attribute.Attribute("value").ToString().Split('=');
                    UInt64 actualValue = Convert.ToUInt64(valueEquals[1].Replace("\"", ""));
                    //this.Log.MessageCtx(LOG_CTXMM, "Found exeSize: {0} KB", actualValue);
                    outPerf.ExeSize = actualValue;
                }

                ///////////////////// MemBuckets

                bool foundMemBucket = false;
                IEnumerable<XElement> memBuckets = doc.XPathSelectElements("/debugLocationMetricsList/results/Item/memoryResults");

                int memBucketIndex = 0;

                //yeah, this is gross
                const int SLOSH_MEMBUCKET_INDEX = 16;

                List<MemoryBucket> theMemBuckets = new List<MemoryBucket>();
                MemoryBucket theMemBucket = new MemoryBucket();

                foreach (XElement elem in memBuckets)
                {
                    foreach (XElement node in elem.Nodes())
                    {
                        foreach (XElement node2 in node.Nodes())
                        {
                            if (!foundMemBucket)
                            {
                                if (String.Equals(node2.Name.ToString(), "name", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    if (node2.Value == MemBucketNames[memBucketIndex])
                                    {
                                        foundMemBucket = true;
                                        theMemBucket.Name = node2.Value;
                                    }
                                }
                            }
                            else if (String.Equals(node2.Name.ToString(), "gameVirtual", StringComparison.CurrentCultureIgnoreCase))
                            {
                                theMemBucket.GameVirt.Value = GetNodeValueAverage(node2);
                            }
                            else if (String.Equals(node2.Name.ToString(), "gamePhysical", StringComparison.CurrentCultureIgnoreCase))
                            {
                                theMemBucket.GamePhys.Value = GetNodeValueAverage(node2);
                            }
                            else if (String.Equals(node2.Name.ToString(), "resourceVirtual", StringComparison.CurrentCultureIgnoreCase))
                            {
                                theMemBucket.ResVirt.Value = GetNodeValueAverage(node2);
                            }
                            else if (String.Equals(node2.Name.ToString(), "resourcePhysical", StringComparison.CurrentCultureIgnoreCase))
                            {
                                theMemBucket.ResPhys.Value = GetNodeValueAverage(node2);
                                theMemBuckets.Add(theMemBucket);
                                memBucketIndex++;

                                if (memBucketIndex == MemBucketNames.Count())
                                {
                                    break;
                                }

                                theMemBucket = new MemoryBucket();
                                foundMemBucket = false;
                            }
                        }
                    }
                }

                outPerf.MemoryBuckets = theMemBuckets;

                outPerf.VirtualSlosh = theMemBuckets[SLOSH_MEMBUCKET_INDEX].ResVirt.Value;
                outPerf.PhysicalSlosh = theMemBuckets[SLOSH_MEMBUCKET_INDEX].ResPhys.Value;

                ////////////////////// MemHeaps
                {
                    bool foundMemHeaps = false;
                    IEnumerable<XElement> memHeaps = doc.XPathSelectElements("/debugLocationMetricsList/results/Item/memoryHeapResults");

                    int memHeapIndex = 0;

                    IList<MemoryHeap> theMemHeaps = new List<MemoryHeap>();
                    MemoryHeap theMemHeap = new MemoryHeap();

                    foreach (XElement elem in memHeaps)
                    {
                        foreach (XElement node in elem.Nodes())
                        {
                            foreach (XElement node2 in node.Nodes())
                            {
                                if (!foundMemHeaps)
                                {
                                    if (String.Equals(node2.Name.ToString(), "idx", StringComparison.CurrentCultureIgnoreCase))
                                    {
                                        int heapIndex = GetNodeValueInt(node2);
                                        if (heapIndex == memHeapIndex)
                                        {
                                            foundMemHeaps = true;
                                            theMemHeap.Idx = memHeapIndex;
                                        }
                                    }
                                }
                                else if (String.Equals(node2.Name.ToString(), "name", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    theMemHeap.Name = node2.Value;
                                }
                                else if (String.Equals(node2.Name.ToString(), "used", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    theMemHeap.Used.Value = GetNodeValue(node2);
                                }
                                else if (String.Equals(node2.Name.ToString(), "free", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    theMemHeap.Free.Value = GetNodeValue(node2);
                                }
                                else if (String.Equals(node2.Name.ToString(), "total", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    theMemHeap.Total.Value = GetNodeValue(node2);
                                }
                                else if (String.Equals(node2.Name.ToString(), "peak", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    theMemHeap.Peak.Value = GetNodeValue(node2);
                                }
                                else if (String.Equals(node2.Name.ToString(), "fragmentation", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    theMemHeap.Frag.Value = GetNodeValue(node2);
                                    theMemHeaps.Add(theMemHeap);
                                    memHeapIndex++;

                                    theMemHeap = new MemoryHeap();
                                    foundMemHeaps = false;
                                }
                            }
                        }
                    }

                    outPerf.MemoryHeaps = theMemHeaps;
                }

                /////////////////////////////// Streaming Modules
                {
                    bool foundStreamingModules = false;
                    IEnumerable<XElement> streamingModules = doc.XPathSelectElements("/debugLocationMetricsList/results/Item/streamingModuleResults");

                    int streamingModuleIndex = 0;

                    IList<StreamingModule> theStreamingModules = new List<StreamingModule>();
                    StreamingModule theStreamingModule = new StreamingModule();

                    foreach (XElement elem in streamingModules)
                    {
                        foreach (XElement node in elem.Nodes())
                        {
                            foreach (XElement node2 in node.Nodes())
                            {
                                if (!foundStreamingModules)
                                {
                                    if (String.Equals(node2.Name.ToString(), "moduleId", StringComparison.CurrentCultureIgnoreCase))
                                    {
                                        int moduleId = GetNodeValueInt(node2);
                                        if (moduleId == streamingModuleIndex)
                                        {
                                            foundStreamingModules = true;
                                            theStreamingModule.ID = streamingModuleIndex;
                                        }
                                    }
                                }
                                else if (String.Equals(node2.Name.ToString(), "moduleName", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    theStreamingModule.Name = node2.Value;
                                }
                                else if (String.Equals(node2.Name.ToString(), "loadedMemVirt", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    theStreamingModule.LoadedMemVirt.Value = GetNodeValue(node2);
                                }
                                else if (String.Equals(node2.Name.ToString(), "loadedMemPhys", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    theStreamingModule.LoadedMemPhys.Value = GetNodeValue(node2);
                                }
                                else if (String.Equals(node2.Name.ToString(), "requiredMemVirt", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    theStreamingModule.RequiredMemVirt.Value = GetNodeValue(node2);
                                }
                                else if (String.Equals(node2.Name.ToString(), "requiredMemPhys", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    theStreamingModule.RequiredMemPhys.Value = GetNodeValue(node2);
                                    theStreamingModules.Add(theStreamingModule);
                                    //theStreamingModule.Dump(Log);
                                    streamingModuleIndex++;

                                    theStreamingModule = new StreamingModule();
                                    foundStreamingModules = false;
                                }
                            }
                        }
                    }

                    outPerf.StreamingModules = theStreamingModules;
                }

                ////////////////////// Timebars
                {
                    bool foundTimeBars = false;
                    IEnumerable<XElement> timeBars = doc.XPathSelectElements("/debugLocationMetricsList/results/Item/cpuResults");

                    int timeBarIndex = 0;

                    IList<TimeBar> theTimeBars = new List<TimeBar>();
                    TimeBar theTimeBar = new TimeBar();

                    foreach (XElement elem in timeBars)
                    {
                        foreach (XElement node in elem.Nodes())
                        {
                            foreach (XElement node2 in node.Nodes())
                            {
                                if (!foundTimeBars)
                                {
                                    if (String.Equals(node2.Name.ToString(), "idx", StringComparison.CurrentCultureIgnoreCase))
                                    {
                                        int barIndex = GetNodeValueInt(node2);
                                        if (barIndex == timeBarIndex)
                                        {
                                            foundTimeBars = true;
                                            //could store this if we needed to                                        
                                        }
                                    }
                                }
                                else if (String.Equals(node2.Name.ToString(), "name", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    theTimeBar.Name = node2.Value;
                                }
                                else if (String.Equals(node2.Name.ToString(), "set", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    theTimeBar.SetName = node2.Value;
                                }
                                else if (String.Equals(node2.Name.ToString(), "min", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    String[] valueEquals = node2.Attribute("value").ToString().Split('=');
                                    theTimeBar.TimeBarValues.Min.Value = Convert.ToSingle(valueEquals[1].Replace("\"", ""));
                                }
                                else if (String.Equals(node2.Name.ToString(), "max", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    String[] valueEquals = node2.Attribute("value").ToString().Split('=');
                                    theTimeBar.TimeBarValues.Max.Value = Convert.ToSingle(valueEquals[1].Replace("\"", ""));
                                }
                                else if (String.Equals(node2.Name.ToString(), "average", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    String[] valueEquals = node2.Attribute("value").ToString().Split('=');
                                    theTimeBar.TimeBarValues.Average.Value = Convert.ToSingle(valueEquals[1].Replace("\"", ""));
                                }
                                else if (String.Equals(node2.Name.ToString(), "std", StringComparison.CurrentCultureIgnoreCase))
                                {
                                    String[] valueEquals = node2.Attribute("value").ToString().Split('=');
                                    theTimeBar.TimeBarValues.Std.Value = Convert.ToSingle(valueEquals[1].Replace("\"", ""));

                                    theTimeBars.Add(theTimeBar);
                                    timeBarIndex++;

                                    theTimeBar = new TimeBar();
                                    foundTimeBars = false;
                                }
                            }
                        }
                    }

                    outPerf.TimeBars = theTimeBars;
                }

                /////////////////////////////// FPS

                IEnumerable<XElement> fpsResults = doc.XPathSelectElements("/debugLocationMetricsList/results/Item/fpsResult");
                foreach (XElement elem in fpsResults)
                {
                    foreach (XElement node in elem.Nodes())
                    {
                        // we end up with value=\"32482134.7324\"
                        String[] valueEquals = node.Attribute("value").ToString().Split('=');
                        float actualValue = Convert.ToSingle(valueEquals[1].Replace("\"", ""));
                        if (String.Equals(node.Name.ToString(), "min", StringComparison.CurrentCultureIgnoreCase))
                        {
                            outPerf.PerfResults.FPS.Min.Value = actualValue;
                        }
                        else if (String.Equals(node.Name.ToString(), "max", StringComparison.CurrentCultureIgnoreCase))
                        {
                            outPerf.PerfResults.FPS.Max.Value = actualValue;
                        }
                        else if (String.Equals(node.Name.ToString(), "average", StringComparison.CurrentCultureIgnoreCase))
                        {
                            outPerf.PerfResults.FPS.Average.Value = actualValue;
                        }
                        else if (String.Equals(node.Name.ToString(), "std", StringComparison.CurrentCultureIgnoreCase))
                        {
                            outPerf.PerfResults.FPS.Std.Value = actualValue;
                        }
                    }
                    //this.Log.MessageCtx(LOG_CTXMM, "Found fpsResults");
                }

                /////////////////////////////// ms per frame

                IEnumerable<XElement> msPFResults = doc.XPathSelectElements("/debugLocationMetricsList/results/Item/msPFResult");
                foreach (XElement elem in msPFResults)
                {
                    foreach (XElement node in elem.Nodes())
                    {
                        // we end up with value=\"32482134.7324\"
                        String[] valueEquals = node.Attribute("value").ToString().Split('=');
                        float actualValue = Convert.ToSingle(valueEquals[1].Replace("\"", ""));
                        if (String.Equals(node.Name.ToString(), "min", StringComparison.CurrentCultureIgnoreCase))
                        {
                            outPerf.PerfResults.MSPFrame.Min.Value = actualValue;
                        }
                        else if (String.Equals(node.Name.ToString(), "max", StringComparison.CurrentCultureIgnoreCase))
                        {
                            outPerf.PerfResults.MSPFrame.Max.Value = actualValue;
                        }
                        else if (String.Equals(node.Name.ToString(), "average", StringComparison.CurrentCultureIgnoreCase))
                        {
                            outPerf.PerfResults.MSPFrame.Average.Value = actualValue;
                        }
                        else if (String.Equals(node.Name.ToString(), "std", StringComparison.CurrentCultureIgnoreCase))
                        {
                            outPerf.PerfResults.MSPFrame.Std.Value = actualValue;
                        }
                    }
                    //this.Log.MessageCtx(LOG_CTXMM, "Found msPFResults");
                }
                //outPerf.Dump(Log);

                outPerf.Platform = this.Platform;
                return true;
            }
            catch (System.Exception ex)
            {
                this.Log.MessageCtx(LOG_CTXMM, "Exception in ProcessFile:{0}", ex.ToString());
                return false;
            }
        }

        protected override int CompareMetrics(IPerfMetrics existingPerf, IPerfMetrics newPerf, String locationName)
        {
            //this.Log.MessageCtx(LOG_CTXMM, "CompareMetrics: old:");
            //existingPerf.Dump();
            //this.Log.MessageCtx(LOG_CTXMM, "CompareMetrics: new:");
            //newPerf.Dump();

            PerfMetrics existingMetrics = existingPerf as PerfMetrics;
            PerfMetrics newMetrics = newPerf as PerfMetrics;
            PerfMetrics thresholdMetrics = Thresholds as PerfMetrics;

            int totalSuccess = 0;
            this.Log.MessageCtx(LOG_CTXMM, "CompareMetrics {1} for {0}", locationName, existingMetrics.PerfResults.FPS.Average.Value);
            if (existingMetrics.PerfResults.FPS.Average.Value == 0.0f)
            {
                totalSuccess = NEWBASELEVEL;
                this.Log.MessageCtx(LOG_CTXMM, "Establishing a new baseline for {0}", locationName);
                //newMetrics.Dump(this.Log);
            }
            else
            {
                Body.Append("========= " + locationName + " ========\n");

                totalSuccess += CompareMetric(existingMetrics.PerfResults.FPS.Average.Value, newMetrics.PerfResults.FPS.Average.Value,
                                              thresholdMetrics.PerfResults.FPS.Average.Value, "FPS", "", true);
                totalSuccess += CompareMetric(existingMetrics.PerfResults.MSPFrame.Average.Value, newMetrics.PerfResults.MSPFrame.Average.Value,
                                              thresholdMetrics.PerfResults.MSPFrame.Average.Value, "MS", "");
                totalSuccess += CompareMetric64(existingMetrics.ExeSize, newMetrics.ExeSize,
                                                thresholdMetrics.ExeSize, "exeSize", "");

                totalSuccess += CompareMemHeaps(existingMetrics, newMetrics);
                totalSuccess += CompareMemBuckets(existingMetrics, newMetrics);
                totalSuccess += CompareStreamingModules(existingMetrics, newMetrics);

                if (totalSuccess == 0)
                {
                    String perf = String.Format("No large changes detected.  Average FPS:{0} MS:{1}",
                                    newMetrics.PerfResults.FPS.Average.Value, newMetrics.PerfResults.MSPFrame.Average.Value);
                    Body.Append(perf);
                }
                Body.Append("\n\n");
            }
            return totalSuccess;
        }

        private int CompareMemHeaps(PerfMetrics existingPerf, PerfMetrics newPerf)
        {
            int success = 0;

            Debug.Assert(existingPerf.MemoryHeaps.Count() == this.MemHeapThreshold.Count(),
                   "Memory Heap threshold size mismatch. Check thresholds in SmokeTestJobProcessor.xml");
            for (int heap = 0; heap < existingPerf.MemoryHeaps.Count(); heap++)
            {
                Debug.Assert(existingPerf.MemoryHeaps.ElementAt(heap).Name == this.MemHeapThreshold.ElementAt(heap).Name,
                    "Memory Heap name mismatch. Check thresholds in SmokeTestJobProcessor.xml");
                if (heap < newPerf.MemoryHeaps.Count() && heap < this.MemHeapThreshold.Count())
                {
                    // no real reason to compare free and used
                    //success += CompareMetric64(existingPerf.MemoryHeaps.ElementAt(heap).Free,
                    //                           newPerf.MemoryHeaps.ElementAt(heap).Free,
                    //                           this.MemHeapThreshold.ElementAt(heap).Free.Value,
                    //                           existingPerf.MemoryHeaps.ElementAt(heap).Name,
                    //                           "Free");

                    success += CompareMetric64(existingPerf.MemoryHeaps.ElementAt(heap).Used,
                                                newPerf.MemoryHeaps.ElementAt(heap).Used,
                                                this.MemHeapThreshold.ElementAt(heap).Used.Value,
                                                existingPerf.MemoryHeaps.ElementAt(heap).Name,
                                                "Used");

                    success += CompareMetric64(existingPerf.MemoryHeaps.ElementAt(heap).Total,
                                               newPerf.MemoryHeaps.ElementAt(heap).Total,
                                               this.MemHeapThreshold.ElementAt(heap).Total.Value,
                                               existingPerf.MemoryHeaps.ElementAt(heap).Name,
                                               "Total");

                    success += CompareMetric64(existingPerf.MemoryHeaps.ElementAt(heap).Frag,
                                               newPerf.MemoryHeaps.ElementAt(heap).Frag,
                                               this.MemHeapThreshold.ElementAt(heap).Frag.Value,
                                               existingPerf.MemoryHeaps.ElementAt(heap).Name,
                                               "Fragmentation");
                }
            }

            return success;
        }

        private int CompareMemBuckets(PerfMetrics existingPerf, PerfMetrics newPerf)
        {
            PerfMetrics thesholds = Thresholds as PerfMetrics;
            int success = 0;
            for (int bucket = 0; bucket < existingPerf.MemoryBuckets.Count(); bucket++)
            {
                // slosh bucket is the only unique one
                if (!String.Equals(existingPerf.MemoryBuckets.ElementAt(bucket).Name, "slosh", StringComparison.CurrentCultureIgnoreCase))
                {
                    if (bucket < newPerf.MemoryBuckets.Count())
                    {
                        success += CompareMetric64(existingPerf.MemoryBuckets.ElementAt(bucket).GamePhys,
                                                   newPerf.MemoryBuckets.ElementAt(bucket).GamePhys,
                                                   this.MemBucketThreshold,
                                                   existingPerf.MemoryBuckets.ElementAt(bucket).Name,
                                                   "gamePhys");
                        success += CompareMetric64(existingPerf.MemoryBuckets.ElementAt(bucket).GameVirt,
                                                   newPerf.MemoryBuckets.ElementAt(bucket).GameVirt,
                                                   this.MemBucketThreshold,
                                                   existingPerf.MemoryBuckets.ElementAt(bucket).Name,
                                                   "gameVirt");
                        success += CompareMetric64(existingPerf.MemoryBuckets.ElementAt(bucket).ResPhys,
                                                   newPerf.MemoryBuckets.ElementAt(bucket).ResPhys,
                                                   this.MemBucketThreshold,
                                                   existingPerf.MemoryBuckets.ElementAt(bucket).Name,
                                                   "resPhys");
                        success += CompareMetric64(existingPerf.MemoryBuckets.ElementAt(bucket).ResVirt,
                                                   newPerf.MemoryBuckets.ElementAt(bucket).ResVirt,
                                                   this.MemBucketThreshold,
                                                   existingPerf.MemoryBuckets.ElementAt(bucket).Name,
                                                   "resVirt");
                    }
                }
                //only consider slosh for consoles
                else if (this.Platform != RSG.Platform.Platform.Win64)
                {
                    success += CompareMetric64(existingPerf.VirtualSlosh, newPerf.VirtualSlosh,
                                               thesholds.VirtualSlosh,
                                               existingPerf.MemoryBuckets.ElementAt(bucket).Name, "virt");
                    success += CompareMetric64(existingPerf.PhysicalSlosh, newPerf.PhysicalSlosh,
                                               thesholds.PhysicalSlosh,
                                               existingPerf.MemoryBuckets.ElementAt(bucket).Name, "phys");
                }
            }
            return success;
        }

        private int CompareStreamingModules(PerfMetrics existingPerf, PerfMetrics newPerf)
        {
            int success = 0;
            Debug.Assert(existingPerf.StreamingModules.Count() == this.StreamingModuleThreshold.Count(),
                    "Streaming Module threshold size mismatch. Check thresholds in SmokeTestJobProcessor.xml");
            for (int module = 0; module < existingPerf.StreamingModules.Count(); module++)
            {
                if (module < newPerf.StreamingModules.Count() && module < this.StreamingModuleThreshold.Count())
                {
                    Debug.Assert(existingPerf.StreamingModules.ElementAt(module).Name == this.StreamingModuleThreshold.ElementAt(module).Name,
                        "Streaming Module name mismatch. Check thresholds in SmokeTestJobProcessor.xml");

                    success += CompareMetric64(existingPerf.StreamingModules.ElementAt(module).LoadedMemVirt,
                                             newPerf.StreamingModules.ElementAt(module).LoadedMemVirt,
                                             this.StreamingModuleThreshold.ElementAt(module).LoadedMemVirt.Value,
                                             existingPerf.StreamingModules.ElementAt(module).Name,
                                             "LoadedMemVirt");
                    success += CompareMetric64(existingPerf.StreamingModules.ElementAt(module).LoadedMemPhys,
                                             newPerf.StreamingModules.ElementAt(module).LoadedMemPhys,
                                             this.StreamingModuleThreshold.ElementAt(module).LoadedMemPhys.Value,
                                             existingPerf.StreamingModules.ElementAt(module).Name,
                                             "LoadedMemPhys");
                    success += CompareMetric64(existingPerf.StreamingModules.ElementAt(module).RequiredMemVirt,
                                             newPerf.StreamingModules.ElementAt(module).RequiredMemVirt,
                                              this.StreamingModuleThreshold.ElementAt(module).RequiredMemVirt.Value,
                                             existingPerf.StreamingModules.ElementAt(module).Name,
                                             "RequiredMemVirt");
                    success += CompareMetric64(existingPerf.StreamingModules.ElementAt(module).RequiredMemPhys,
                                             newPerf.StreamingModules.ElementAt(module).RequiredMemPhys,
                                             this.StreamingModuleThreshold.ElementAt(module).RequiredMemPhys.Value,
                                             existingPerf.StreamingModules.ElementAt(module).Name,
                                             "RequiredMemPhys");
                }
            }
            return success;
        }
    } 
}
