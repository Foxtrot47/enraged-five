﻿using RSG.Base.Logging.Universal;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{
    // This watches a smoketest
    class SmoketestTargetObserver
    {
        ISmoketestTarget m_Target;
        public ISmoketestTarget Target
        {
            get { return m_Target; }
        }

        LogMonitor m_LogMonitor;
        protected static readonly String LOG_CTX = "Smoke Test Target Observer";
        IUniversalLog m_Log;
        String m_OutputDir;
        IEnumerable<String> m_SuccessMessages;
        IEnumerable<String> m_FailureMessages;
        bool m_Running;

        TimeSpan m_LogNotFoundTimeout;// = new TimeSpan(0, 2, 0); // It's taking way too long to reboot so we should try again.. reboot
        TimeSpan m_HangTimeout;// = new TimeSpan(0, 2, 0); // The logfile has no indication of problems but has been idle for too long.. reboot
        //TimeSpan m_HangTimeout = new TimeSpan(0, 0, 15); // The logfile has no indication of problems but has been idle for too long.. reboot
        TimeSpan m_FatalTimeout;// = new TimeSpan(0, 2, 0); // The logfile shows a fatal error and no further log updates are being made.. reboot
        TimeSpan m_CrashWaitForStartCoreDumpTimeout;// = new TimeSpan(0, 3, 0); // The log file shows a crash and no indication of a core dump coming.. reboot
        TimeSpan m_CrashWaitForEndCoreDumpTimeout;// = new TimeSpan(0, 5, 0); // The log file shows a crash and a core dump coming but no success/fail reported.. reboot

        public LogMonitor LogMonitor
        {
            get { return m_LogMonitor; }
            set { m_LogMonitor = value; }
        }

        public delegate void RunFinishedDelegate(LogMonitor monitor, ISmoketestTarget target);
        public RunFinishedDelegate OnRunFinished;

        DateTime m_TimeMonitorCreated;

        public SmoketestTargetObserver(
            IUniversalLog log, 
            ISmoketestTarget target, 
            String outputDir,
            IEnumerable<String> successMessages,
            IEnumerable<String> failureMessages,
            TimeSpan hangTimeout, 
            TimeSpan logNotFoundTimeout, 
            TimeSpan fatalTimeout, 
            TimeSpan crashStartCoreDumpTimeout, 
            TimeSpan crashEndCoreDumpTimeout)
        {
            m_Log = log;
            m_Target = target;
            m_OutputDir = outputDir;
            m_Running = false;
            m_SuccessMessages = successMessages;
            m_FailureMessages = failureMessages;

            //@TODO improve this
            m_HangTimeout = hangTimeout;
            m_LogNotFoundTimeout = logNotFoundTimeout;
            m_FatalTimeout = fatalTimeout;
            m_CrashWaitForStartCoreDumpTimeout = crashStartCoreDumpTimeout;
            m_CrashWaitForEndCoreDumpTimeout = crashEndCoreDumpTimeout;
        }

        public bool IsTargetRunningGame()
        {
            return (m_Running || m_Target.IsLaunching);
        }

        public String GetRunTitle()
        {
            return String.Format("{0}[{1}]", m_Target.IpAddress, m_Target.RunIndex);
        }

        public void Tick()
        {
            if (!m_Running && !m_Target.IsLaunching && m_Target.HasAttemptedLaunch)
            {
                LogMonitor newMonitor = m_Target.TakeMonitor();
                if (newMonitor != null)
                {
                    CleanupLogMonitor();

                    m_LogMonitor = newMonitor;
                    m_Running = true;
                    m_TimeMonitorCreated = DateTime.Now;
                    m_LogMonitor.SetSuccessMessages(m_SuccessMessages);
                    m_LogMonitor.SetFailureMessages(m_FailureMessages);
                }
            }

            if (m_Running)
            {
                Debug.Assert(m_LogMonitor != null);
                m_LogMonitor.Tick();
            }

            if (m_Running && m_LogMonitor.HasSucceeded.HasValue)
            {
                m_Log.MessageCtx(LOG_CTX, "{0} - Completed with {1}.", GetRunTitle(), ((bool)m_LogMonitor.HasSucceeded) ? "SUCCESS" : "FAILURE");
                NotifyRunFinished();
            }

            if (!m_Running)
            {
                m_Log.DebugCtx(LOG_CTX, "{0} - Waiting for game to launch", GetRunTitle());
            }
            else if (m_LogMonitor.IsWaitingForFile)
            {
                int secondsLeftToTimeout = (int)(m_LogNotFoundTimeout.TotalSeconds - (DateTime.Now - m_TimeMonitorCreated).TotalSeconds);
                if (DateTime.Now - m_TimeMonitorCreated > m_LogNotFoundTimeout)
                {
                    m_Log.DebugCtx(LOG_CTX, "{0} - Timed out while waiting for log", GetRunTitle());

                    NotifyRunFinished();
                }
                else
                {
                    m_Log.DebugCtx(LOG_CTX, "{0} - Waiting for log {1}.. {2} seconds left to timeout", GetRunTitle(), m_LogMonitor.LogFilePath, secondsLeftToTimeout);
                }

            }
            else if (m_LogMonitor.HasCrash)
            {
                bool runComplete = false;

                if (m_LogMonitor.CoreDumpAttempted)
                {
                    if (m_LogMonitor.CoreDumpSuceeded || m_LogMonitor.CoreDumpFailed)
                    {
                        if (m_LogMonitor.CoreDumpSuceeded)
                        {
                            m_Log.MessageCtx(LOG_CTX, "{0} - Crashed.. core dump complete.", GetRunTitle());
                        }
                        else
                        {
                            m_Log.MessageCtx(LOG_CTX, "{0} - Crashed.. core dump failed.", GetRunTitle());
                        }

                        runComplete = true;
                    }

                    int secondsLeftToTimeout = (int)(m_CrashWaitForEndCoreDumpTimeout.TotalSeconds - (DateTime.Now - m_LogMonitor.FileChangeLastObserved).TotalSeconds);

                    if (DateTime.Now - m_LogMonitor.FileChangeLastObserved > m_CrashWaitForEndCoreDumpTimeout)
                    {
                        m_Log.MessageCtx(LOG_CTX, "{0} - Crashed.. core dump timed out while waiting to finish.", GetRunTitle());
                        runComplete = true;
                    }
                    else
                    {
                        m_Log.DebugCtx(LOG_CTX, "{0} - Crashed.. waiting for core dump to finish. {1} seconds left to timeout", GetRunTitle(), secondsLeftToTimeout);
                    }
                }
                else
                {
                    int secondsLeftToTimeout = (int)(m_CrashWaitForStartCoreDumpTimeout.TotalSeconds - (DateTime.Now - m_LogMonitor.FileChangeLastObserved).TotalSeconds);
                    if (DateTime.Now - m_LogMonitor.FileChangeLastObserved > m_CrashWaitForStartCoreDumpTimeout)
                    {
                        m_Log.MessageCtx(LOG_CTX, "{0} - Crashed.. core dump timed out while waiting to start.", GetRunTitle());
                        runComplete = true;
                    }
                    else
                    {
                        m_Log.DebugCtx(LOG_CTX, "{0} - Crashed.. waiting for core dump to start. {1} seconds left to timeout", GetRunTitle(), secondsLeftToTimeout);
                    }
                }

                if (runComplete)
                {
                    NotifyRunFinished();
                }

            }
            else if (m_LogMonitor.HasFatalMessage)
            {
                int secondsLeftToTimeout = (int)(m_FatalTimeout.TotalSeconds - (DateTime.Now - m_LogMonitor.FileChangeLastObserved).TotalSeconds);
                if (DateTime.Now - m_LogMonitor.FileChangeLastObserved > m_FatalTimeout)
                {
                    m_Log.MessageCtx(LOG_CTX, "{0} - Fatal message observed.", GetRunTitle());
                    NotifyRunFinished();
                }
                else
                {
                    m_Log.DebugCtx(LOG_CTX, "{0} - Fatal message observed. {1} seconds left to timeout", GetRunTitle(), secondsLeftToTimeout);
                }
            }
            else if (DateTime.Now - m_LogMonitor.FileChangeLastObserved > m_HangTimeout)
            {
                m_Log.MessageCtx(LOG_CTX, "{0} - Hang detected.", GetRunTitle());
                NotifyRunFinished();
            }
            else
            {
                m_Log.DebugCtx(LOG_CTX, "{0} - {1} errors", GetRunTitle(), m_LogMonitor.Messages.Count);
            }
        }

        public void NotifyRunFinished()
        {
            if (m_Running)
            {
                if (OnRunFinished != null)
                {
                    OnRunFinished(m_LogMonitor, m_Target);
                }
            }

            m_Running = false;
        }

        public void CleanupLogMonitor()
        {
            //Debug.Assert(!m_Running);
            if (m_LogMonitor != null)
            {
                m_LogMonitor.Dispose();
                m_LogMonitor = null;
            }
        }
    }
}

