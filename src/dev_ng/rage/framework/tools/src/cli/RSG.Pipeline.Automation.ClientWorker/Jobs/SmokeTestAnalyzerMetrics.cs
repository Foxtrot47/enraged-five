﻿using System;
using SIO = System.IO;
using System.Text;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading;
using P4API;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Platform;


namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{
    class SmokeTestAnalyzerMetrics :
        ISmokeTestAnalyzer
    {

        public String DataCL;

        public SmokeTestAnalyzerMetrics(IDictionary<String, object> parameters, IBranch branch, BaseSmokeTestJob job,
                                        IUniversalLog log, ILogTarget logFile, CommandOptions options)
            : base(parameters, branch, job, log, logFile, options)
        {
        }

        public override bool RunAnalysis()
        {
            MetricMonitor metrics = new MetricMonitor(this.Options, this.Parameters, this.Platform, this.CurrentJob.ExePath,
                                                      this.Log, this.Branch, this.MailRecipients, this.Targets);
            metrics.AllocatePerfMetrics();
            bool metricsComplete = false;
            bool gameFailsToRun = false;
            String crashString = "";

            int numRetries = 0;
            do
            {
                switch (numRetries)
                {
                    case 0:
                        {
                            //determine baseline stats
                            metrics.DetermineBaseline();

                            CleanupObservers();
                            CleanupTargets();
                            ShutdownTheGame();

                            // sync data in preparation of running the game
                            String syncTo = String.Format(@"@{0}", this.P4CurrentLabel);

                            if (this.P4CurrentLabel.Contains("latest"))
                            {
                                //skip the label; latest as of this submit
                                syncTo = String.Format(@"@{0}", this.CurrentJob.Changelist.ToString());
                            }

                            DataCL = syncTo;
                            SyncP4(syncTo, true);
                            break;
                        }
                    case 1:
                        {
                            //just try running the game again
                            ShutdownTheGame();
                            CleanupObservers();
                            CleanupTargets();
                            break;
                        }
                    case 2:
                        {
                            ShutdownTheGame();
                            CleanupObservers();
                            CleanupTargets();


                            // maybe a code/data dependency; go to data from this submit
                            String syncTo = String.Format(@"@{0}", this.CurrentJob.Changelist.ToString());

                            // actually grab head at this point
                            if (this.P4CurrentLabel.Contains("latest"))
                            {
                                UInt32 cl = GetP4LatestCL();
                                if (cl > 0)
                                {
                                    syncTo = String.Format("{0}", cl);
                                }
                            }

                            DataCL = syncTo;
                            SyncP4(syncTo, false);
                            break;
                        }

                    case 3:
                        {
                            ShutdownTheGame();
                            CleanupObservers();
                            CleanupTargets();

                            if (!BuildShaders(this.CurrentJob.Changelist.ToString()))
                            {
                                this.Log.WarningCtx(LOG_CTX,
                                          "Failure: could not build shaders. analysisType:{0} platform:{1} file:{2} cl:{3}.",
                                          this.CurrentJob.AnalysisType, this.Platform, this.CurrentJob.ExePath, this.CurrentJob.Changelist);
                            }
                            break;
                        }

                    case 4:
                        {
                            // mail failure message 
                            // game won't run
                            this.Log.ErrorCtx(LOG_CTX,
                                            "Failure: SmokeTest cannot run the game. analysisType:{0} platform:{1} file:{2} cl:{3} error:{4}.",
                                            this.CurrentJob.AnalysisType, this.Platform, this.CurrentJob.ExePath, this.CurrentJob.Changelist,
                                            crashString);

                            gameFailsToRun = true;

                            SendFailMail(metrics);
                            break;
                        }
                }

                if (!gameFailsToRun)
                {
                    //@TODO better method to detect crashes?
                    RunTheGame();

                    bool gameCrashed = false;
                    String gameCrashMsg;
                    //bool metricsComplete = false;
                    WaitForMetricsOrCrash(metrics, out gameCrashed, out metricsComplete, out gameCrashMsg);

                    if (metricsComplete)
                    {
                        if (gameCrashed)
                        {
                            this.Log.MessageCtx(LOG_CTX, "Game crashed: {0}, but metrics are complete", gameCrashMsg);
                            crashString = gameCrashMsg;
                        }
                        else
                        {
                            this.Log.MessageCtx(LOG_CTX, "All metrics complete");
                        }
                    }
                    else
                    {
                        this.Log.MessageCtx(LOG_CTX, "Game crashed: {0}", gameCrashMsg);
                        crashString = gameCrashMsg;
                    }

                    if (gameCrashed || !metricsComplete)
                    {
                        numRetries++;
                    }
                }

            } while (!metricsComplete && !gameFailsToRun);

            ShutdownTheGame();
            CleanupObservers();
            CleanupTargets();

            return metricsComplete;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameCrashed"></param>
        /// <param name="metricsComplete"></param>
        /// <param name="gameCrashMsg"></param>
        public void WaitForMetricsOrCrash(MetricMonitor metrics, out bool gameCrashed, out bool metricsComplete, out String gameCrashMsg)
        {
            this.Log.MessageCtx(LOG_CTX, "WaitForMetricsOrCrash Waiting {0} ms for the game to finish...", this.GameRunTimeMS);

            gameCrashed = false;
            metricsComplete = false;
            gameCrashMsg = String.Format("No Messages");

            int runTime = 0;

            // let the game boot and start up
            while (runTime < this.GameRunTimeMS && !gameCrashed)
            {
                gameCrashed = TickEachObserver(out gameCrashMsg);

                System.Threading.Thread.Sleep(1000);
                runTime += 1000;
            }

            if (!gameCrashed)
            {
                // watch for metrics files as well as crashes
                int monitorTime = 0;
                while (monitorTime < this.MetricsSearchTimeout && !gameCrashed && !metricsComplete)
                {
                    gameCrashed = TickEachObserver(out gameCrashMsg);

                    if (monitorTime > 0 && monitorTime % 5000 == 0)
                    {
                        metricsComplete = metrics.SearchForMetrics();
                    }

                    System.Threading.Thread.Sleep(1000);
                    monitorTime += 1000;
                }
            }

            if (metricsComplete)
            {
                metrics.PublishResults();
            }
            else
            {
                int numSuccess = metrics.MeasureSuccess();
                if (numSuccess == 0)
                {
                    //game crashed in first town or startup or startup failed; try again and sync data and try again
                    this.Log.MessageCtx(LOG_CTX,
                                        "Metrics search timeout.  Game failed to launch or first location crashed.  Retrying");
                }
                else
                {
                    this.Log.MessageCtx(LOG_CTX,
                                        "Metrics search timeout.  {0} location metrics found but there was a crash.",
                                        numSuccess);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="gameCrashMsg"></param>
        /// <returns></returns>
        protected bool TickEachObserver(out String gameCrashMsg)
        {
            gameCrashMsg = String.Format("No Messages");
            foreach (SmoketestTargetObserver observer in TargetObservers)
            {
                observer.Tick();

                if (observer.LogMonitor != null &&
                   (observer.LogMonitor.HasFatalMessage ||
                    observer.LogMonitor.HasCrash))
                {
                    if (observer.LogMonitor.Messages.Count > 0)
                    {
                        gameCrashMsg = observer.LogMonitor.Messages.Last().Message;
                    }

                    this.Log.ErrorCtx(LOG_CTX, "{0} crash(s) detected: {1}", observer.LogMonitor.Messages.Count, gameCrashMsg);
                    return true;
                }
            }
            return false;
        }

        protected void SendFailMail(MetricMonitor metrics)
        {
            StringBuilder body = new StringBuilder();
            StringBuilder crashes = new StringBuilder();
            int numCrashes = 0;
            String crashMsg = "";

            body.AppendFormat("Failed Attempts To Run CL {0} (codebuilder CL {1}):\n", this.CurrentJob.Changelist.ToString(),
                                                                                this.CurrentJob.CodeBuilderChangelist.ToString());
            body.AppendFormat("1. Data sync to CL {0}.\n", this.CurrentJob.Changelist.ToString());
            body.AppendFormat("2. Additional run to cover data cache.\n");
            body.AppendFormat("3. Data sync to CL {0}. (latest)\n", DataCL);
            body.AppendFormat("4. Src sync to CL {0} and rebuilt shaders.\n", this.CurrentJob.Changelist.ToString());

            String logFilename = String.Format(@"x:\Smoketest\InProgress\{0}_0_smoketest.log", Targets[0].IpAddress);
            body.AppendFormat("\nAttached log {0}.\n", logFilename);

            String crashLocation = metrics.FailMailFirstCrash(body);

            foreach (SmoketestTargetObserver observer in TargetObservers)
            {
                if (observer.LogMonitor != null &&
                   (observer.LogMonitor.HasFatalMessage ||
                    observer.LogMonitor.HasCrash))
                {
                    observer.LogMonitor.EndReadFile();

                    if (observer.LogMonitor.Messages.Count > 0)
                    {
                        crashMsg = observer.LogMonitor.Messages.Last().Message.Trim();
                        numCrashes++;

                        foreach (String stack in observer.LogMonitor.Messages.Last().StackTrace)
                        {
                            crashes.AppendLine(stack.Trim());
                        }
                        crashes.Append(crashMsg.Trim());
                    }
                    
                    observer.LogMonitor = null;
                }
            }

            StringBuilder completeCrashes = new StringBuilder(crashes.ToString().Trim());
            RemoveEscapeCharacters(completeCrashes);
            String crash = completeCrashes.ToString().Trim();

            CleanupObservers();

            ShutdownTheGame();

            // we can log Quitf crashes now, but we'll miss other crashes on PS4 until we use R*TM
            if (numCrashes == 0)
                numCrashes = 1;

            StringBuilder failRemainders = new StringBuilder();
            metrics.FailMailRemainders(failRemainders);

            String[] attachment = new String[1] { logFilename };
            //@TODO look up the pc platform name

            String platform = this.Platform.PlatformToRagebuilderPlatform(); 
            if (this.Platform == RSG.Platform.Platform.Win64)
            {
                platform = String.Format("{0} {1}", platform, System.Environment.MachineName);
            }

            String formattedCrashMsg = crashMsg;

            //only show the first 80 characters, remove the mem addr
            if(crashMsg.Length > 80)
            {
                int firstBracket = crashMsg.IndexOf('[');
                int closeBracket = crashMsg.IndexOf(']');
                if (closeBracket > 0)
                {
                    String noAddr = crashMsg.Replace(crashMsg.Substring(firstBracket, closeBracket - firstBracket + 1), "");
                    formattedCrashMsg = noAddr.Substring(0, 80);
                }
                else
                {
                    formattedCrashMsg = crashMsg.Substring(0, 80);
                }
            }
            String subject = String.Format("{0} {1} CL{2} Failure: {3} crashes: {4} {5}.",
                                            Branch.Project.Name,
                                            platform,           
                                            this.CurrentJob.Changelist, numCrashes, crashLocation, formattedCrashMsg);

            String emailBody = String.Format("{0}\n{1}\n{2}", body.ToString(), completeCrashes.ToString().Trim(), failRemainders.ToString());

            RSG.UniversalLog.Util.Email.SendEmail(Branch.Project.Config, MailRecipients.ToArray(),
                                                  subject, emailBody, attachment);

            this.Log.MessageCtx(LOG_CTX, "Mailing failure results...");
        }

        private void RemoveEscapeCharacters(StringBuilder str)
        {
            for (int index = 0; index < str.Length; index++)
            {
                char c = str[index];
                if (Char.IsControl(c) && c != '\n')
                {
                    str[index] = ' ';
                }
            }
        }
    }
}
