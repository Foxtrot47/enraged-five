﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Interop.Autodesk3dsmax;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Export;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Common.Services.Messages;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Platform;
using RSG.Platform;
using RSG.SourceControl.Perforce;
using RSG.SceneXml;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{

	/// <summary>
	/// Map Export Network Job Processing class.
	/// </summary>
	internal class MapExportNetworkJobProcessor : 
		MapExportJobProcessorBase,
		IJobProcessor
    {
        #region Constants
        private readonly int processWaitForExitTimeout_ = 60000;
        #endregion

        #region Constructor(s)
        /// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="options"></param>
		/// <param name="fileTransferConsumer"></param>
		/// 
		public MapExportNetworkJobProcessor(CommandOptions options, FileTransferServiceConsumer fileTransferConsumer)
			: base(options, CapabilityType.MapExportNetwork, fileTransferConsumer)
		{
		}
		#endregion // Constructor(s)

		#region Controller Methods
		/// <summary>
		/// Process requested job.
		/// </summary>
		/// <param name="job"></param>
		/// <returns></returns>
        public override IJobResult Process(IJob job)
		{
			Debug.Assert(job is MapExportJob, String.Format("Invalid job for processor: {0}.",
				job.GetType().Name));
			if (!(job is MapExportJob))
            {
                return null;
            }
			Debug.Assert(job.Trigger is IUserRequestTrigger, 
				String.Format("Invalid job trigger for processor: {0}.", job.Trigger.GetType().Name));
            if (!(job.Trigger is IUserRequestTrigger))
            {
                return null;
            }

            Init(job);
            JobResult jobResult = new JobResult(job.ID, false);            

			MapExportJob mapJob = (MapExportJob)job;
			IUniversalLog log = LogFactory.CreateUniversalLog("process");
            ILogTarget logFile = LogFactory.CreateUniversalLogFile("process",
                log, "automation", mapJob.ID.ToString()) as ILogTarget;

			bool result = true;
            
			try
			{
				if (!VerifyMapJobProcess(log, mapJob))
				{
                    // Flush job log; ensure its closed and ready for uploading.
                    logFile.Flush();
                    LogFactory.CloseUniversalLogFile(logFile);
                    LogFactory.CloseUniversalLog(log);

                    MergeLogsScanErrorAndUpload(log, logFile, job);
                    return jobResult;
				}

				String maxExe = String.Empty;
				if (!VerifyAutodesk3dsmaxInstall(log, mapJob, out maxExe))
				{
                    // Flush job log; ensure its closed and ready for uploading.
                    logFile.Flush();
                    LogFactory.CloseUniversalLogFile(logFile);
                    LogFactory.CloseUniversalLog(log);

                    MergeLogsScanErrorAndUpload(log, logFile, job);
					log.Error("Job failed: VerifyAutodesk3dsmaxInstall check failed, exiting Process function.");
                    return jobResult;
				}

				SyncDependencies(log);

				// Download data from automation service.
				if (!DownloadAndCopySourceFiles(log, mapJob))
				{
                    // Flush job log; ensure its closed and ready for uploading.
                    logFile.Flush();
                    LogFactory.CloseUniversalLogFile(logFile);
                    LogFactory.CloseUniversalLog(log);

                    MergeLogsScanErrorAndUpload(log, logFile, job);
					log.Error("Job failed: DownloadAndCopySourceFiles failed, exiting Process function.");
					CleanupDCCSourceAndExportData(log, logFile, mapJob);
                    return jobResult;
				}

				// Loop through each of our export processes; exporting...
				foreach (ExportProcess exportProcess in mapJob.ExportProcesses)
				{
                    result &= this.Export(log, logFile, maxExe, mapJob, exportProcess,
						FileMode.ForceWritable);

                    log.Message("Export result {0}", result);
				}

				if (!UploadExportAndBuildData(log, mapJob))
				{
                    // Flush job log; ensure its closed and ready for uploading.
                    logFile.Flush();
                    LogFactory.CloseUniversalLogFile(logFile);
                    LogFactory.CloseUniversalLog(log);

                    MergeLogsScanErrorAndUpload(log, logFile, job);
					log.Message("UploadExportAndBuildData failed, exiting Process function");
					CleanupDCCSourceAndExportData(log, logFile, mapJob);
                    return jobResult;
				}
			}
			catch (Exception ex)
			{
                log.ToolExceptionCtx(LOG_CTX, ex, "Exception processing map network export job {0}: {1}", job.ID, ex.Message);
                result = false;
			}

            // Flush job log; ensure its closed and ready for uploading.
            logFile.Flush();
            LogFactory.CloseUniversalLogFile(logFile);
            LogFactory.CloseUniversalLog(log);

			// Merge and upload resultant job log.
            result &= MergeLogsScanErrorAndUpload(log, logFile, job);            
            
			// Cleanup data overwritten during export.
			CleanupDCCSourceAndExportData(log, logFile, mapJob);
            log.Message("Final Process result returning: {0}", result);

            jobResult.Succeeded = result;
            return jobResult;
		}
		#endregion // Controller Methods

		#region Private Methods
		/// <summary>
		/// Download DCC and texture source files onto this client worker.
		/// </summary>
		/// <param name="log"></param>
		/// <param name="logFile"></param>
		/// <param name="job"></param>
		/// <returns></returns>
		private bool DownloadAndCopySourceFiles(IUniversalLog log, MapExportJob job)
		{
			bool result = true;
			IEnumerable<String> jobAvailableFilenames = FileTransferServiceConsumer.FileAvailability(job);

			foreach (ExportProcess exportProcess in job.ExportProcesses)
			{
				// Get the depot paths and force sync the source textures
				// Get the scene xml from the server which we need to find out source textures to sync
				String sceneXMLPathname = exportProcess.ExportFiles.Where(f => Path.GetExtension(f).Equals(".xml")).FirstOrDefault();
				if (sceneXMLPathname != null)
				{
					String sceneXMLFileName = Path.GetFileName(sceneXMLPathname);
					if (sceneXMLFileName != null)
					{
						String sceneXMLServerFilename = jobAvailableFilenames.Where(f => f.Equals(sceneXMLFileName)).FirstOrDefault();
						if (sceneXMLServerFilename != null)
							result &= DownloadAndCopySourceFilesAction(log, job, sceneXMLServerFilename, sceneXMLPathname, true);
					}                    
				} 

				// We've failed to get the scene xml so would otherwise have to use the checked in version of the scene xml file to
				// get source textures.
				if (!result)
					return result;

				List<String> sourceTextures = new List<String>();
				if (File.Exists(sceneXMLPathname))
				{
					Scene scene = Scene.Load(sceneXMLPathname, SceneXml.LoadOptions.Materials, false);
					if (scene != null)
					{
						scene.GetSceneTextureFilenames(ref sourceTextures, true);
					}
					else
					{
						log.ErrorCtx(LOG_CTX, "Failed to load scene xml to get source textures from: {0}.", sceneXMLPathname);
						return false;
					}
				}

				if (sourceTextures.Any())
				{
					using (P4 p4 = this.Options.Config.Project.SCMConnect())
					{
						FileMapping[] fms = FileMapping.Create(p4, sourceTextures.ToArray());
						List<String> args = new List<String>();
						args.Add("-f");
						args.AddRange(fms.Select(fm => fm.DepotFilename));
						log.MessageCtx(LOG_CTX, "\tSyncing source textures.");
						p4.Run("sync", args.ToArray());
					}
				}

				// Download and copy the DCC file
				String dccSourceFileBasename = Path.GetFileName(exportProcess.DCCSourceFilename);
				String serverDCCFilename = jobAvailableFilenames.Where(f =>
						Path.GetFileNameWithoutExtension(f).Equals(Path.GetFileNameWithoutExtension(dccSourceFileBasename))).FirstOrDefault();

				result &= DownloadAndCopySourceFilesAction(log, job, serverDCCFilename, exportProcess.DCCSourceFilename, true);

				// Download the source textures that have been passed to the server with the job request
				foreach (String editedSourceTexture in exportProcess.EditedSourceTextures)
				{
#warning AJM: Need to tidy up why the max file has no extension at this point to avoid having to do 'GetFileNameWithoutExtension' on both parts below
					String sourceFileBasename = Path.GetFileName(editedSourceTexture);
					String serverFilename = jobAvailableFilenames.Where(f =>
						Path.GetFileNameWithoutExtension(f).Equals(Path.GetFileNameWithoutExtension(sourceFileBasename))).FirstOrDefault();

					result &= DownloadAndCopySourceFilesAction(log, job, serverFilename, editedSourceTexture, false);
				}
			}

			// Upload and merge logs.
			if (!result)
			{
				log.ErrorCtx(LOG_CTX, "Source file(s) download to worker failed.");
			}

			return (result);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="log"></param>
		/// <param name="file"></param>
		/// <returns></returns>
		private bool DownloadAndCopySourceFilesAction(IUniversalLog log, MapExportJob job, String serverFilename, String fullFilename, bool errorIfNotFound)
		{
			bool result = true;
			log.MessageCtx(LOG_CTX, "Checking for file on server that will get copied to {0}...", fullFilename);

			Debug.Assert(!String.IsNullOrEmpty(serverFilename) && errorIfNotFound,
				String.Format("Source file {0} not available on server.",
					serverFilename));

			// If source textures aren't on server that's most likely because they didn't get sent with the job as they
			// contain no local edits, so our sync to the textures will have got the texture we need.
			if (String.IsNullOrEmpty(serverFilename))
			{
				// Only error if the max file isn't there
				if (errorIfNotFound)
				{
					log.ErrorCtx(LOG_CTX, "Source file {0} not available on server.",
						serverFilename);
					result &= false;
					return result;
				}
			}
			else
			{
				// Download the source file as its available on the server.
                Byte[] md5 = RSG.Base.IO.FileMD5.Empty;
				result &= this.FileTransferServiceConsumer.DownloadFile(job,
                    serverFilename, Path.GetFileName(serverFilename), out md5);
                if (!result)
                {
                    log.ErrorCtx(LOG_CTX, "File {0} download failed.  MD5: {1}.",
                        RSG.Base.IO.FileMD5.Format(md5));
                    return (result);
                }

                String jobSourceFilename = Path.Combine(this.FileTransferServiceConsumer.GetClientFileDirectory(job), 
                    Path.GetFileName(serverFilename));
                if (!File.Exists(jobSourceFilename))
				{
					if (errorIfNotFound)
					{
						log.ErrorCtx(LOG_CTX, "Source file {0} did not download correctly; cannot copy to {1}.",
                            Path.GetFileName(serverFilename), jobSourceFilename);
						result &= false;
					}
					else
					{
						log.MessageCtx(LOG_CTX, "Source texture file {0} doesn't exist on server (likely an unedited texture in perforce which will be sync'd).",
                            Path.GetFileName(serverFilename), jobSourceFilename);
					}
				}
				else
				{
					String sourceFileDirectory = Path.GetDirectoryName(fullFilename);
					if (!Directory.Exists(sourceFileDirectory))
						Directory.CreateDirectory(sourceFileDirectory);
					String dest = Path.Combine(sourceFileDirectory, serverFilename);

					try
					{
                        FileInfo fI = new FileInfo(jobSourceFilename);
						if (fI.Length == 0)
						{
                            log.ErrorCtx(LOG_CTX, "Source file {0} is 0 bytes in size!", jobSourceFilename);
							result &= false;
							return result;
						}
					}
					catch (Exception ex)
					{
                        log.ErrorCtx(LOG_CTX, "Exception while checking file size of {0}: {1}", jobSourceFilename, ex.Message);
						result &= false;
						return result;
                    }

                    // Determine correct file paths.
                    String downloadCacheDir = FileTransferServiceConsumer.GetClientFileDirectory(job);
                    String sourceFilename = Path.Combine(downloadCacheDir, serverFilename);

					log.MessageCtx(LOG_CTX, "Copying source file {0} to {1}.",
						jobSourceFilename, dest);
					if (File.Exists(dest))
					{
						FileAttributes attributes = File.GetAttributes(dest);
						File.SetAttributes(dest, attributes & ~FileAttributes.ReadOnly);
					}

                    File.Copy(jobSourceFilename, dest, true);
					// Delete texture from the job directory to save some space on the client.
                    TryDeleteFile(jobSourceFilename, log);
				}
			}
			return result;
		}

		/// <summary>
		/// Upload export and build data back to the automation service.
		/// </summary>
		/// <param name="log"></param>
		/// <param name="logFile"></param>
		/// <param name="job"></param>
		/// <returns></returns>
		private bool UploadExportAndBuildData(IUniversalLog log, MapExportJob job)
		{
			bool result = true;
			
            IProject project;
            IBranch branch;

            // Find the project and branch for this network export so later we can 
            if (this.Options.Config.CoreProject.Name.Equals(job.ProjectName, StringComparison.OrdinalIgnoreCase))
            {
                project = this.Options.Config.CoreProject;
            }
            else if (this.Options.Config.AllProjects().ContainsKey(job.ProjectName))
            {
                project = this.Options.Config.AllProjects()[job.ProjectName];
            }
            else
            {
                log.ErrorCtx(LOG_CTX, "Project is null in UploadExportAndBuildData - does the job have a project name?");
                return false;
            }

            if (project != null && project.Branches.ContainsKey(job.BranchName))
            {
                branch = project.Branches[job.BranchName];
            }
            else
            {
                log.ErrorCtx(LOG_CTX, "Branch is not in UploadExportAndBuildData - does the job have a branch name?");
                return false;
            }

            String mapCacheDirectory = Path.Combine(project.Cache, "raw", "maps");

            if (!Directory.Exists(mapCacheDirectory))
                Directory.CreateDirectory(mapCacheDirectory);

            IContentTree tree = RSG.Pipeline.Content.Factory.CreateTree(branch);
            RSG.Pipeline.Content.ContentTreeHelper helper = new RSG.Pipeline.Content.ContentTreeHelper(tree);

			foreach (ExportProcess exportProcess in job.ExportProcesses)
			{
				log.MessageCtx(LOG_CTX, "Uploading export and build data for {0} ({1} export files)...",
					exportProcess.DCCSourceFilename, exportProcess.ExportFiles.Count());
				String dccSourceBasename = Path.GetFileName(exportProcess.DCCSourceFilename);
				String mapName = Path.GetFileNameWithoutExtension(dccSourceBasename);

				// Upload export files (ZIP/XML).
				foreach (String filename in exportProcess.ExportFiles)
				{
					String destFilename = Path.GetFileName(filename);
					log.MessageCtx(LOG_CTX, "  Uploading {0}", filename);
					result &= this.FileTransferServiceConsumer.UploadFile(job, filename, destFilename);
				}
                    
#warning DHM FIX ME: Asset Pipeline 3 will be far easier to get this information.
				// Upload build platform data (RPFs).                
                IContentNode mapZip = tree.CreateFile(exportProcess.DCCSourceFilename);
				if ((null == mapZip) || !(mapZip is IContentNode))
				{
					log.ErrorCtx(LOG_CTX, "Failed to find build data for map: {0}.", mapName);
					result &= false;
				}
				else
				{
                    // Find out if the job has platform overrides it built for.
                    // Make sure not null - which will happen if people haven't got the 3ds max
                    // assemblies when they get updated.
                    List<ITarget> requestedTargets = new List<ITarget>();
                    if (exportProcess.ExportPlatforms != null)
                    {
                        foreach (Platform.Platform platform in exportProcess.ExportPlatforms)
                        {
                            if (branch.Targets.ContainsKey(platform))
                                requestedTargets.Add(branch.Targets[platform]);
                        }
                    }

                    if (!requestedTargets.Any())
                    {
                        requestedTargets = branch.Targets.Values.ToList();
                        log.Warning("No platforms specified in MapExportJob - using all enabled platforms on this network exporter");
                    }

                    IContentNode mapZipNode = helper.GetExportZipNodeFromMaxFileNode(mapZip);
					// Get correct processed zip output
                    IContentNode mapProcZipNode = helper.GetProcessedZipNodeFromExportZipNode(mapZipNode);
                    bool containerExport = helper.GetSceneTypeForExportNode(mapZipNode).Equals("map_container");
                    String mapZipFilename = (mapZipNode as Content.File).AbsolutePath;

                    // Go through enabled platforms and upload
                    foreach (ITarget target in requestedTargets)
                    {
                        if (!target.Enabled)
                        {
                            log.WarningCtx("A requested target for the network export job, isn't enabled on this client: {0}",
                                target.Platform.ToString());
                            continue;
                        }
                        if (containerExport)
                        {
                            // Get the RPF output for this platform and area
                            String platformFilenameCombined = PlatformPathConversion.ConvertAndRemapFilenameToPlatform(
                                target, mapZipFilename);
                            // Get the area directory as we just scoop up all the area's RPFs now and zip them up for each platform
                            String mapAreaDirectory = Path.GetDirectoryName(platformFilenameCombined);

                            // Put the platform name at the start, so it comes out as ps3downtown_01.zip for example
                            String mapAreaZipFilename = target.Platform.ToString() + Path.GetFileName(mapAreaDirectory) + ".zip";
                            String platformAreaZipPathname = Path.Combine(mapCacheDirectory, mapAreaZipFilename);
                            log.MessageCtx(LOG_CTX, "Map area zip file to be created: {0}", platformAreaZipPathname);

                            if (File.Exists(platformAreaZipPathname))
                            {
                                log.MessageCtx(LOG_CTX, "Existing mapAreaZipFilename found, deleting: {0}", platformAreaZipPathname);
                                TryDeleteFile(platformAreaZipPathname, log);
                            }

                            log.MessageCtx(LOG_CTX, "Checking {0} for {1} RPFs", mapAreaDirectory, target.Platform.ToString());
                            String[] platformFileList = Directory.GetFiles(mapAreaDirectory);
                            if (platformFileList.Any())
                            {
                                if (!Zip.Create(branch, platformAreaZipPathname, platformFileList, true))
                                {
                                    result = false;
                                    log.ErrorCtx(LOG_CTX, "Error creating map area zip file: {0}", platformAreaZipPathname);
                                }
                                else
                                {
                                    log.MessageCtx(LOG_CTX, "  Uploading {0}", platformAreaZipPathname);
                                    result &= this.FileTransferServiceConsumer.UploadFile(job, platformAreaZipPathname, mapAreaZipFilename);

                                    if (result)
                                    {
                                        TryDeleteFile(platformAreaZipPathname, log);
                                        TryDeleteDirectory(mapAreaDirectory, log);
                                    }
                                }
                            }
                            else
                            {
                                log.ErrorCtx(LOG_CTX, "No platform files found at {0}!", mapAreaDirectory);
                            }
                        }
                        else
                        {
                            String platformFilename = PlatformPathConversion.ConvertAndRemapFilenameToPlatform(
                                target, Path.GetFullPath(mapZipFilename));
                            String destFilename = target.Platform.ToString() + Path.GetFileName(platformFilename);
                            log.MessageCtx(LOG_CTX, "  Uploading {0}", platformFilename);
                            result &= this.FileTransferServiceConsumer.UploadFile(job, platformFilename, destFilename);
                        }
					}
				}
				// See if there's a textures/tcs zip file to upload.  It's 1 zip file for the whole job
				String zipFilename = Path.Combine(mapCacheDirectory, Common.Utils.CommonFuncs._networkExportChangelistFilesZip);
				result &= CreateAutoSubmitChangelistZipIfRequired(log, zipFilename, mapCacheDirectory);
				if (File.Exists(zipFilename))
				{
					String destFilename = Path.GetFileName(zipFilename);
					log.MessageCtx(LOG_CTX, "  Uploading {0}", zipFilename);
					result &= this.FileTransferServiceConsumer.UploadFile(job, zipFilename, destFilename);
				}
			}

			// Upload and merge logs.
			if (!result)
			{
				log.ErrorCtx(LOG_CTX, "Export and build data upload failed.");
			}

			return (result);
		}

		/// <summary>
		/// Checks changelists in a 'changelistNumber.txt' file from the export cache to see if any
		/// textures, dds, tif, clip or anim files are new/edited and need returned.
		/// </summary>
		/// <param name="log"></param>
		/// <param name="zipFilename">Pathname String if a file was created</param>
		/// <param name="mapName"></param>
		/// <returns></returns>
		private bool CreateAutoSubmitChangelistZipIfRequired(IUniversalLog log, String zipFilename, String mapCacheDirectory)
		{
			bool result = true;

			try
			{
                // Delete the MapNetworkExport_ChangelistFiles.zip file if it exists - so that it doesn't get returned later if there isn't
                // a new one created!
                TryDeleteFile(zipFilename);

				// Check the changelist number file if it exists, for this map section
				String changelistPathName = mapCacheDirectory + "\\networkExportChangelistNumber.txt";
				if (File.Exists(changelistPathName))
				{
					using (P4 p4 = this.Options.Config.Project.SCMConnect())
					{
						String[] changelists = File.ReadLines(changelistPathName).ToArray();
						P4API.P4RecordSet recordSet = p4.Run("describe", changelists);

						try
						{
							foreach (String changelist in changelists)
							{
								p4.Run("revert", "-a", "-c", changelist);
							}

							//Don't need this file anymore so nuke it
							TryDeleteFile(changelistPathName, log);
						}
						catch (Exception ex)
						{
							log.Warning("Perforce exception trying to revert unchanged files in map export changelist or deleting changelist txt file: {0}", ex.Message);
						}

						List<String> allDepotPathnames = new List<String>();
						// Collect all the depotfiles from each changelist
						foreach (P4API.P4Record record in recordSet)
						{
							if (record.ArrayFields.ContainsKey("depotFile"))
							{
								allDepotPathnames.AddRange(record.ArrayFields["depotFile"]);
							}
						}

						// Get the client pathnames
						if (allDepotPathnames.Any())
						{
							FileMapping[] fms = FileMapping.Create(p4, allDepotPathnames.ToArray());
                            // We don't want to take back source textures as it could overwrite any local changes made since 
                            // the network export was sent - bugstar:1464384
                            String artDirectory = Path.GetFullPath(this.Options.Branch.Environment.Subst("$(art)"));
							IEnumerable<String> clientPathnames = fms.Select(fileMap => Path.GetFullPath(fileMap.LocalFilename).ToLower());
                            IEnumerable<String> filteredClientPathnames = clientPathnames.Where(fileMap => !fileMap.Contains(artDirectory));

							// Create zip file in the cache folder, preserving the directory structure so it can be unzipped 
							// on the person's machine into the right places.
                            if (filteredClientPathnames.Any())
							{
                                if (!Zip.Create(this.Options.Branch, zipFilename, filteredClientPathnames, true))
								{
									result = false;
									log.ErrorCtx(LOG_CTX, "Error creating texture/TCS/anim zip data: {0}", zipFilename);
								}
							}
						}

						try
						{
							// Revert the files and delete changelists now
							foreach (String changelist in changelists)
							{
								p4.Run("revert", "-c", changelist, "//...");
								p4.Run("change", "-d", changelist);
							}
						}
						catch (Exception ex)
						{
							log.Warning("Perforce exception trying to revert unchanged files in map export changelist: {0}", ex.Message);
						}
					}
				}
			}
			catch (Exception ex)
			{
				log.Error("Exception in CreateTexturesZipIfRequired: {0}", ex.Message);
				result = false;
			}
			return (result);
		}

		/// <summary>
		/// Cleanup any files overwritten during the current job.
		/// </summary>
		/// <param name="log"></param>
		/// <param name="logFile"></param>
		/// <param name="job"></param>
		/// <returns></returns>
        private bool CleanupDCCSourceAndExportData(IUniversalLog log, ILogTarget logFile, MapExportJob job)
		{
            log.MessageCtx(LOG_CTX, "Cleaning up for job {0}...", job.ID);

            // Kill any running max instances - they should be closed by now...
            List<MaxInstance> maxInstances = Autodesk3dsmax.GetRunningInstances().ToList();
            foreach (MaxInstance maxInst in maxInstances)
            {
                try
                {
                    log.MessageCtx(LOG_CTX, "Trying to close 3dsMax process ID: {0}", maxInst.Process.Id);
                    if(maxInst.Process.CloseMainWindow())
                        maxInst.Process.WaitForExit(processWaitForExitTimeout_);

                    if (!maxInst.Process.HasExited)
                    {
                        log.MessageCtx(LOG_CTX, "Killing 3dsMax process ID: {0}", maxInst.Process.Id);
                        maxInst.Process.Kill();
                    }                 
                }
                catch (Exception ex)
                {
                    log.WarningCtx(LOG_CTX, "Swallowed exception while trying to close/kill max process - {0}", ex.ToString());
                }
            }

            // Delete the cache folder for the map section exported, so that next export on the machine won't have any cache files left
            // from previous exports.
            foreach (ExportProcess expProc in job.ExportProcesses)
            {
                String exportFolderCacheName = this.Options.Branch.Environment.Subst(Path.Combine("$(cache)", "maps", "dev", 
                                                Path.GetFileNameWithoutExtension(expProc.DCCSourceFilename)));
                TryDeleteDirectory(exportFolderCacheName, log);
            }

			// Get all the source and export files used in this job
			List<String> dccSourceFiles = new List<String>();
			dccSourceFiles.AddRange(job.ExportProcesses.Select(p => p.DCCSourceFilename));
			dccSourceFiles.AddRange(job.ExportProcesses.SelectMany(p => p.EditedSourceTextures));

            using (P4 p4 = this.Options.Config.Project.SCMConnect())
            {
                FileMapping[] fmsDccSource = FileMapping.Create(p4, (dccSourceFiles.ToArray()));
                // #none is used to remove from workspace
                List<String> sourceFiles = new List<String>();
                sourceFiles.AddRange(fmsDccSource.Select(f => f.DepotFilename + "#none"));

                // Now get fileMapping for files we want to sync latest to (zip/xml)
                FileMapping[] fmsExport = FileMapping.Create(p4, job.ExportProcesses.SelectMany(p => p.ExportFiles).ToArray());
                IEnumerable<string> exportFiles = fmsExport.Select(f => f.DepotFilename);
                sourceFiles.AddRange(exportFiles);

                // Try a revert of the export files as they could be checked out if an artist who submitted the job
                // doesn't have them checked out.
                if (exportFiles.Any())
                {
                    try
                    {
                        p4.Run("revert", exportFiles.ToArray());
                    }
                    catch (System.Exception ex)
                    {
                        log.Warning("Exception while trying to revert export files from perforce (they might not have been checked out): {0}", ex.Message);
                    }
                }                

                if (sourceFiles.Any())
                {
                    // Force sync
                    List<String> args = new List<String>();
                    args.Add("-f");
                    args.AddRange(sourceFiles);

                    log.MessageCtx(LOG_CTX, "\tRemoving {0} source and export files from perforce workspace.",
                                args.Count - 1);
                    try
                    {
                        p4.Run("sync", args.ToArray());
                        // maxc files can exist as they get copied across rather than sync'd
                        TryDeleteFiles(dccSourceFiles, log);
                    }
                    catch (Exception ex)
                    {
                        log.Warning("Exception while removing source and export files from perforce workspace!  {0}", ex.Message);
                    }
                }
            }
            
			return (true);
		}

		/// <summary>
		/// Syncs the metadata/definitions, assets/levels, build/dev/common
		/// </summary>
		private void SyncDependencies(IUniversalLog log)
		{
			String parentTXDxml = this.Options.Branch.Environment.Subst(
				Path.Combine("$(assets)", "maps", "ParentTxds.xml"));

            String vfxData = this.Options.Branch.Environment.Subst(
                Path.Combine("$(art)", "VFX", "rmptfx", "..."));

            String interiorXmls = this.Options.Branch.Environment.Subst(
                Path.Combine("$(assets)", "export", "levels", this.Options.Branch.Project.Name, "interiors", "....xml"));

            String propXmls = this.Options.Branch.Environment.Subst(
                Path.Combine("$(assets)", "export", "levels", this.Options.Branch.Project.Name, "props", "....xml"));

			List<String> paths = new List<String>();
			paths.Add(parentTXDxml);
            paths.Add(vfxData);
            paths.Add(interiorXmls);
            paths.Add(propXmls);

			try
			{
				using (P4 p4 = this.Options.Config.Project.SCMConnect())
				{
                    // Gets the list of DLC project files and their dependencies, otherwise the exporter machine might
                    // not know about the DLC project (if it is DLC) and not be able to look up that DLC project later.
                    List<String> DLCConfigAndProjectFiles = ConfigSync.DepotFilesTriggeringRestartResync(p4, this.Options.Branch, this.Options.Config, true, true).ToList();
                    paths.AddRange(DLCConfigAndProjectFiles);

					// Get the depot paths
					FileMapping[] fms = FileMapping.Create(p4, (paths.ToArray()));
					List<String> depotPaths = fms.Select(fm => fm.DepotFilename).ToList();

                    // Add the common dir - we sync this to the game label version of the folder.
                    String[] commonDirPath = new String[] { this.Options.Branch.Environment.Subst(Path.Combine("$(build)", "common", "...")) };
                    FileMapping[] commonDirFileMapping = FileMapping.Create(p4, commonDirPath);
                    // Commenting this out until a tools label is setup for NG
                    //String commonDirLabelPathArg = String.Format("{0}@{1}", commonDirFileMapping.Select(fm => fm.DepotFilename).First(), this.Options.Project.Labels[RSG.Base.Configuration.Label.GameCurrent]);
                    //depotPaths.Add(commonDirLabelPathArg);
                    depotPaths.Add(commonDirFileMapping.Select(fm => fm.DepotFilename).First());
                    
                    log.MessageCtx(LOG_CTX, "\tSyncing files.");
					if (depotPaths.Any())
					{
						p4.Run("sync", depotPaths.ToArray());
					}
				}
			}
			catch (Exception ex)
			{
				log.WarningCtx(LOG_CTX, "Dependency sync had an exception: {0}", ex.Message);
			}
		}
		#endregion // Private Methods
	}

} // RSG.Pipeline.Automation.ClientWorker.Jobs namespace
