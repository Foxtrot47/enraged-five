﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Net;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Threading;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Content;
using RSG.Pipeline.Content.Algorithm;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.Platform;
using RSG.SourceControl.Perforce;
using Ionic.Zip;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{
    class PtfxPackageJobProcessor :
        IcdBasedPackageJobProcessor
    {
        #region Constants
        protected readonly new String LOG_CTX = "Ptfx Package Job Processor";
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="fileTransferConsumer"></param>
        /// 
        public PtfxPackageJobProcessor(CommandOptions options, FileTransferServiceConsumer fileTransferConsumer)
            : base(options, CapabilityType.PtfxPackage, fileTransferConsumer)
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Handle stderr while a processing is running.
        /// </summary>
        /// <param name="logObj">the log</param>
        /// <param name="obj">the string</param>
        protected override void ReadStandardError(object logObj, object obj)
        {
            IUniversalLog log = (IUniversalLog)logObj;
            String line = obj as String;
            if (line == null)
            {
                log.ToolErrorCtx(LOG_CTX, "Non String object send to ReadStandardError");
                return;
            }

            try
            {
                if (!String.IsNullOrWhiteSpace(line))
                {
                    log.WarningCtx(LOG_CTX, line);
                }
            }
            catch (FormatException fe) // Used to catch issues with formatting log messages
            {
                log.WarningCtx(LOG_CTX, String.Format("Error Occured while logging from stream stderr {0}", fe.ToString()));
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Exception parsing stderr stream.");
            }
        }

        /// <summary>
        /// Parses the line of output from the build process.
        /// </summary>
        /// <param name="line"></param>
        protected override void ParseLine(string line)
        {
            if (line.Contains("fatal error") == true || line.Contains("Build FAILED"))
            {
                Log.ErrorCtx(LOG_CTX, line);
                BuildErrorMessages.Add(LOG_CTX + line);
            }
            else if (line.Contains("warn") == true || line.Contains("warning") == true || line.Contains("error") == true)
            {
                Log.WarningCtx(LOG_CTX, line);
            }
            else
            {
                Log.MessageCtx(LOG_CTX, line);
            }
        }

        /// <summary>
        /// Get zip files that are going to be uploaded
        /// </summary>
        /// <param name="AllZips"></param>
        /// <returns></returns>
        protected override void GetZipFiles(ref List<String> AllZips)
        {
            if (String.IsNullOrEmpty(this.ZipRootDir))
            {
                Log.ErrorCtx(LOG_CTX, "Input Zip Directory must be defined for Ptfx Packaging. See Processor settings xml.");
            }
            AllZips.AddRange(System.IO.Directory.GetFiles(this.ZipRootDir, "ptfx.zip"));
            AllZips.AddRange(System.IO.Directory.GetFiles(this.ZipRootDir, "*.meta"));
        }
        #endregion // Controller Methods

        /// <summary>
        /// Uploads all data associated with the job.
        /// </summary>
        /// <param name="job"></param>
        /// <param name="zipFiles"></param>
        /// <returns>result</returns>
        protected override bool UploadData(IJob job, IEnumerable<String> zipFiles)
        {
            //Check for if the client is on the same machine as the server.
            bool bSameVMAsServer = false;
            if (String.Equals(this.FileTransferServiceConsumer.GetClientHostName(), Dns.GetHostName()))
            {
                Log.MessageCtx(LOG_CTX, "This client has the same hostname as the FileTransferClient. Will use a direct copy instead of uploading for speed-up.");
                bSameVMAsServer = true;
            }

            //Create the zip files that will be uploaded/downloaded
            String downloadCacheDir = FileTransferServiceConsumer.GetClientFileDirectory(job);
            if (System.IO.Directory.Exists(downloadCacheDir) == false)
                System.IO.Directory.CreateDirectory(downloadCacheDir);
            bool result = true;
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            //Must use multiple zip files due to directory being over 2GB in size. 
            Log.MessageCtx(LOG_CTX, String.Format("Beginning upload of files from Dir: {0}", this.ZipRootDir));
            String lastDirectoryName = Path.GetFileName(this.ZipRootDir);
            if (zipFiles.Any())
            {
                result &= UploadDirectory(this.ZipRootDir, zipFiles, downloadCacheDir, String.Format("export{0}.zip", lastDirectoryName), job, bSameVMAsServer);
                if (result == false)
                {
                    Log.ErrorCtx(LOG_CTX, "Error uploading files in {0} to the server.", this.ZipRootDir);
                }
            }

            Log.MessageCtx(LOG_CTX, "Done uploading .zip's");

            if (result == false)
            {
                Log.ErrorCtx(LOG_CTX, "Unable to upload asset/export/... *.zips to the server.");
                return false;
            }

            foreach (ITarget target in Branch.Targets.Values)
            {
                if (target.Enabled == false)
                    continue;

                String resourcedZipName = target.Platform.ToString() + "resourced.zip";

                using (ZipFile zip = new ZipFile())
                {
                    string tmp = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());
                    System.IO.Directory.CreateDirectory(tmp);

                    string ptfxBuildDirectory = System.IO.Path.Combine(target.ResourcePath, "data", "effects");

                    //uploading all local .rpfs
                    string[] rpfFiles = System.IO.Directory.GetFiles(ptfxBuildDirectory, "*.rpf");
                    foreach (string rpfFile in rpfFiles)
                    {
                        string destFile = Path.Combine(tmp, Path.GetFileName(rpfFile));
                        System.IO.File.Copy(rpfFile, destFile, true);
                        System.IO.File.SetAttributes(destFile, FileAttributes.Normal);
                    }

                    zip.AddDirectory(tmp);
                    zip.Save(Path.Combine(downloadCacheDir, resourcedZipName));
                    System.IO.Directory.Delete(tmp, true);
                }
                if (bSameVMAsServer)
                {
                    try
                    {
                        System.IO.File.Copy(Path.Combine(downloadCacheDir, resourcedZipName), Path.Combine(downloadCacheDir.Replace("client", "server"), resourcedZipName));
                        result &= true;
                    }
                    catch (System.Exception ex)
                    {
                        Log.ErrorCtx(LOG_CTX, "Got exception when directly copying to server directory:\n{0}", ex.Message);
                        result = false;
                    }
                }
                else
                {
                    result &= FileTransferServiceConsumer.UploadFile(job, Path.Combine(downloadCacheDir, resourcedZipName), resourcedZipName);
                }
                if (result == false)
                {
                    Log.ErrorCtx(LOG_CTX, String.Format("Unable to upload {0} to the server.", resourcedZipName));
                    return false;
                }
            }
            stopwatch.Stop();
            Log.MessageCtx(LOG_CTX, String.Format("Elapsed Time Uploading Files: {0}.", stopwatch.Elapsed));
            return true;
        }
    }
}
