﻿using System;
using SIO = System.IO;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Platform;
using RSG.UniversalLog.Util;

namespace RSG.Pipeline.Automation.ClientWorker.Jobs
{
    abstract class IPerfMetrics
    {
        public IPerfMetrics(IUniversalLog log)
        {
            Log = log;
        }

        public bool Crashed;
        public bool TestComplete;
        public String BuildVersion;
        public int CL;
        public RSG.Platform.Platform Platform;
        public IUniversalLog Log;
        public abstract void DumpHeaderToCSV(StreamWriter fileStream);
        public abstract void DumpToCSV(String locationName, StreamWriter fileStream);
        public abstract String DumpToDB(IEnumerable<String> locationNames, String locationName, ISmoketestTarget target);
        public abstract void DumpDBColumnHeaders(SqlConnection sqlConnection);
        public abstract void Set(float theNum);
    }

    class PerfMetrics : IPerfMetrics
    {
        protected static readonly String LOG_CTX = "Perf Metrics";

        public IEnumerable<MemoryBucket> MemoryBuckets;
        public IEnumerable<MemoryHeap> MemoryHeaps;
        public IEnumerable<StreamingModule> StreamingModules;
        public IEnumerable<TimeBar> TimeBars;
        public UInt64 PhysicalSlosh;
        public UInt64 VirtualSlosh;
        public UInt64 ExeSize;
        public PerformanceResults PerfResults;
        public String DBServerName;
        public String DBName;
        static public int NumDBInserts;

        static public SmokeTestDBTable DBTable = new SmokeTestDBTable("dbo.Perf_Metric");
        static public SmokeTestLocationDBLookUpTable DBLocationLookup = new SmokeTestLocationDBLookUpTable("dbo.Location_Lookup", "Location_ID");
        static public SmokeTestPlatformDBLookUpTable DBPlatformLookup = new SmokeTestPlatformDBLookUpTable("dbo.Platform_Lookup", "Platform_ID");
        static public SmokeTestPlatformPCDBLookUpTable DBPlatformPCLookup = new SmokeTestPlatformPCDBLookUpTable("dbo.Platform_PC_Lookup", "Platform_ID");

        public PerfMetrics(String dbServerName, String dbName, IUniversalLog log)
            : base(log)
        {
            PerfResults = new PerformanceResults();
            Set(0.0f);
            MemoryBuckets = new List<MemoryBucket>();
            MemoryHeaps = new List<MemoryHeap>();
            StreamingModules = new List<StreamingModule>();
            TimeBars = new List<TimeBar>();
            DBServerName = dbServerName;
            DBName = dbName;
        }

        public PerfMetrics(IUniversalLog log, float averageFPS, float averageMSPFrame, UInt64 exeSize, UInt64 physSlosh, UInt64 virtSlosh)
            : base(log)
        {
            PerfResults = new PerformanceResults();
            PerfResults.FPS.Average.Value = averageFPS;
            PerfResults.MSPFrame.Average.Value = averageMSPFrame;
            ExeSize = exeSize;
            CL = -1;
            BuildVersion = "-1";
            Platform = RSG.Platform.Platform.Independent;
            PhysicalSlosh = physSlosh;
            VirtualSlosh = virtSlosh;
            MemoryBuckets = new List<MemoryBucket>();
            MemoryHeaps = new List<MemoryHeap>();
            StreamingModules = new List<StreamingModule>();
            TimeBars = new List<TimeBar>();
            Crashed = false;
            TestComplete = false;
        }

        /// <summary>
        /// Dump the entire PerfMetric to a CSV file.  After the immediate data, the heaps, buckets, and modules are dumped
        /// </summary>
        /// <param name="">locationName</param>
        /// <param name="">fileStream</param>
        /// <returns></returns>
        /// 
        public override void DumpToCSV(String locationName, StreamWriter fileStream)
        {
            String values = String.Join(",", locationName,
                                        DateTime.Now.ToString(), CL, BuildVersion, PerfResults.FPS.Average.Value, PerfResults.MSPFrame.Average.Value,
                                        ExeSize, "");
            fileStream.Write(values);

            foreach (MemoryHeap heap in MemoryHeaps)
            {
                heap.DumpToCSV(fileStream);
            }

            foreach (MemoryBucket bucket in MemoryBuckets)
            {
                bucket.DumpToCSV(fileStream);
            }

            foreach (StreamingModule module in StreamingModules)
            {
                module.DumpToCSV(fileStream);
            }

            foreach (TimeBar timeBar in TimeBars)
            {
                timeBar.DumpToCSV(fileStream);
            }
            fileStream.Write("\n");
        }

        /// <summary>
        /// Dump the entire PerfMetric headers to a CSV file.  After the immediate data, the heaps, buckets, and modules are dumped
        /// </summary>
        /// <param name="">fileStream</param>
        /// <returns></returns>
        /// 
        public override void DumpHeaderToCSV(StreamWriter fileStream)
        {
            String headers = String.Join(",", "Location", "Date", "CL", "BuildVersion", "FPS", "MSpFrame", "exeSize", "");
            fileStream.Write(headers);

            foreach (MemoryHeap heap in MemoryHeaps)
            {
                heap.DumpHeaderToCSV(fileStream);
            }

            foreach (MemoryBucket bucket in MemoryBuckets)
            {
                bucket.DumpHeaderToCSV(fileStream);
            }

            foreach (StreamingModule module in StreamingModules)
            {
                module.DumpHeaderToCSV(fileStream);
            }

            foreach (TimeBar timeBar in TimeBars)
            {
                timeBar.DumpHeaderToCSV(fileStream);
            }
            fileStream.Write("\n");
        }

        /// <summary>
        /// Dump the entire PerfMetric to DBServerName/DBName, creating tables as neeeded.  After the immediate data, the heaps, buckets, 
        /// the modules, and the timebars are dumped.  Also populates and caches all the DBLookupTables.  14 tables here are involved.  
        /// Catches all SQL exceptions.  Opens and closes the SQL connection.
        /// </summary>
        /// <param name="">locationNames</param>
        /// <param name="">locationName</param>
        /// <returns></returns>
        /// 
        public override String DumpToDB(IEnumerable<String> locationNames, String locationName, ISmoketestTarget target)
        {
            String connectionString = String.Format("Initial Catalog={0};Data Source={1};Integrated Security=SSPI;",
                                                    DBName, DBServerName);

            String insertResults = "";

            Log.MessageCtx(LOG_CTX, "Establishing sqlConnection to {0} {1}", DBServerName, DBName);
            DateTime start = DateTime.Now;
            NumDBInserts = 0;

            using (SqlConnection sqlConnection = new SqlConnection(connectionString))
            {
                try
                {                   
                    sqlConnection.Open();

                    String ourPlatform = CreatePlatformLookupTable(sqlConnection, target);
                    CreateLocationLookupTable(sqlConnection, locationNames);
                    CreateDBTable(sqlConnection);

                    int runID = 0;
                    int platformID = DBPlatformLookup.LookUpValue(ourPlatform, Log);
                    int locationID = DBLocationLookup.LookUpValue(locationName, Log);

                    IList<SqlParameter> sqlParams = new List<SqlParameter>();

                    sqlParams.Add(new SqlParameter("@Platform_ID", platformID));
                    sqlParams.Add(new SqlParameter("@Location_ID", locationID));
                    sqlParams.Add(new SqlParameter("@CL", CL));
                    sqlParams.Add(new SqlParameter("@ExeSize", (Int64)ExeSize));
                    sqlParams.Add(new SqlParameter("@BuildVersion", BuildVersion));
                    sqlParams.Add(new SqlParameter("@Date", DateTime.UtcNow));
                    runID = DBTable.DumpToDB(sqlConnection, sqlParams, Log);
                    insertResults = String.Format("Inserted row {0} to {1}.{2}.{3} for {4}", 
                                                    runID, DBServerName, DBName, DBTable.TableName, 
                                                    ourPlatform);
                    Log.MessageCtx(LOG_CTX, insertResults);

                    CreateAndDumpPerformanceTable(sqlConnection, runID, locationID);

                    CreateAndDumpHeapTable(sqlConnection, runID, locationID);

                    CreateAndDumpMemoryBucketTable(sqlConnection, runID, locationID);

                    CreateAndDumpStreamingModuleTable(sqlConnection, runID, locationID);

                    CreateAndDumpTimeBarTable(sqlConnection, runID, locationID);
                }

                catch (System.Data.SqlClient.SqlException ex)
                {
                    Log.ErrorCtx(LOG_CTX,"SQL fail: " + ex.Message);
                }
                finally
                {
                    Log.MessageCtx(LOG_CTX, "Closing sqlConnection to {0} {1}.  {2} DB Inserts in {3} minutes", 
                                            DBServerName, DBName, NumDBInserts, (DateTime.Now - start).Minutes);
                    sqlConnection.Close();
                }
            }
            return insertResults;
        }

        /// <summary>
        /// Creates the PerfMetrics DB table, containing the TestRun_ID key used by all other tables
        /// </summary>
        /// <param name="sqlConnection"></param>
        private void CreateDBTable(SqlConnection sqlConnection)
        {
            if (!DBTable.Exists(sqlConnection, Log))
            {
                Log.ErrorCtx(LOG_CTX, "{0} table NOT found.  Must create...", DBTable.TableName);
                IList<String> columnDescs = new List<String>();

                columnDescs.Add("TestRun_ID int NOT NULL PRIMARY KEY IDENTITY(1,1) ");
                columnDescs.Add("Platform_ID int NOT NULL");
                columnDescs.Add("Location_ID int NOT NULL");
                columnDescs.Add("CL int NOT NULL");
                columnDescs.Add("ExeSize bigint NOT NULL");
                columnDescs.Add("BuildVersion varchar(50) NOT NULL");
                columnDescs.Add("Date datetime NOT NULL");

                columnDescs.Add(String.Format("FOREIGN KEY (Platform_ID) REFERENCES {0} ({1})",
                                            DBPlatformLookup.TableName, DBPlatformLookup.LookUpColumn));
                columnDescs.Add(String.Format("FOREIGN KEY (Location_ID) REFERENCES {0} ({1})",
                                          DBLocationLookup.TableName, DBLocationLookup.LookUpColumn));

                DBTable.CreateTable(sqlConnection, Log, columnDescs);
            }
        }

        /// <summary>
        /// Handles creation and dumping of performance data to Performance and Performance_Lookup tables.  Creates the C#
        /// side lookup as well.  Creates the 2 DB tables if needed.
        /// </summary>
        /// <param name="">sqlConnection</param>
        /// <param name="">runID</param>
        /// <param name="">locationID</param>
        /// <returns></returns>
        /// 
        protected void CreateAndDumpPerformanceTable(SqlConnection sqlConnection, int runID, int locationID)
        {
            PerfResults.CreateTables(sqlConnection, Log, DBTable, DBLocationLookup);
               
            PerfResults.DumpToLookUpDB(sqlConnection, Log);
            PerfResults.DumpToDB(sqlConnection, runID, locationID, Log);
        }

        /// <summary>
        /// Handles creation and dumping of memory heap data to Heap and Heap_Lookup tables.  Creates the C#
        /// side lookup as well.  Creates the 2 DB tables if needed.
        /// </summary>
        /// <param name="">sqlConnection</param>
        /// <param name="">runID</param>
        /// <param name="">locationID</param>
        /// <returns></returns>
        /// 
        protected void CreateAndDumpHeapTable(SqlConnection sqlConnection, int runID, int locationID)
        {
            bool bTablesCreated = false;
            foreach (MemoryHeap heap in MemoryHeaps)
            {
                if (!bTablesCreated)
                {
                    bTablesCreated = heap.CreateTables(sqlConnection, Log, DBTable, DBLocationLookup);
                }
                heap.DumpToLookUpDB(sqlConnection, Log);
                heap.DumpToDB(sqlConnection, runID, locationID, Log);
            }
        }

        /// <summary>
        /// Handles creation and dumping of memory bucket data to Memory_Bucket and Memory_Bucket_Lookup tables.  Creates the C#
        /// side lookup as well.  Creates the 2 DB tables if needed.
        /// </summary>
        /// <param name="">sqlConnection</param>
        /// <param name="">runID</param>
        /// <param name="">locationID</param>
        /// <returns></returns>
        /// 
        protected void CreateAndDumpMemoryBucketTable(SqlConnection sqlConnection, int runID, int locationID)
        {
            bool bTablesCreated = false;
            foreach (MemoryBucket bucket in MemoryBuckets)
            {
                if (!bTablesCreated)
                {
                    bTablesCreated = bucket.CreateTables(sqlConnection, Log, DBTable, DBLocationLookup);
                }
                bucket.DumpToLookUpDB(sqlConnection, Log);
                bucket.DumpToDB(sqlConnection, runID, locationID, Log);
            }
        }

        /// <summary>
        /// Handles creation and dumping of streaming module data to Streaming_Module and Streaming_Module_Lookup tables.  Creates the C#
        /// side lookup as well.  Creates the 2 DB tables if needed.
        /// </summary>
        /// <param name="">sqlConnection</param>
        /// <param name="">runID</param>
        /// <param name="">locationID</param>
        /// <returns></returns>
        /// 
        protected void CreateAndDumpStreamingModuleTable(SqlConnection sqlConnection, int runID, int locationID)
        {
            bool bTablesCreated = false;
            foreach (StreamingModule module in StreamingModules)
            {
                if (!bTablesCreated)
                {
                    bTablesCreated = module.CreateTables(sqlConnection, Log, DBTable, DBLocationLookup);
                }
                module.DumpToLookUpDB(sqlConnection, Log);
                module.DumpToDB(sqlConnection, runID, locationID, Log);
            }
        }

        /// <summary>
        /// Handles creation and dumping of timebar data to Time_Bar and Time_Bar_Lookup tables.  Creates the C#
        /// side lookup as well.  Creates the 3 DB tables if needed.
        /// </summary>
        /// <param name="">sqlConnection</param>
        /// <param name="">runID</param>
        /// <param name="">locationID</param>
        /// <returns></returns>
        /// 
        protected void CreateAndDumpTimeBarTable(SqlConnection sqlConnection, int runID, int locationID)
        {
            bool bTablesCreated = false;
            foreach (TimeBar timeBar in TimeBars)
            {
                if (!bTablesCreated)
                {
                    bTablesCreated = timeBar.CreateTables(sqlConnection, Log, DBTable, DBLocationLookup);
                }
                timeBar.DumpToLookUpDB(sqlConnection, Log);
                timeBar.DumpToDB(sqlConnection, runID, locationID, Log);
            }
        }

        /// <summary>
        /// Handles creation and dumping of Platform data to the Platform_Lookup table.  Creates the C#
        /// side lookup as well.  Also creates the detailed Platform_PC_Lookup.  Returns a string matching
        /// our actual platform name (which is only special for win64)
        /// </summary>
        /// <param name="">sqlConnection</param>
        /// <returns></returns>
        /// 
        protected String CreatePlatformLookupTable(SqlConnection sqlConnection, ISmoketestTarget target)
        {
            if (!DBPlatformLookup.Exists(sqlConnection, Log))
            {
                DBPlatformLookup.CreateTable(sqlConnection, Log);
            }

            if (!DBPlatformPCLookup.Exists(sqlConnection, Log))
            {
                DBPlatformPCLookup.CreateTable(sqlConnection, Log);
            }
          
            RSG.Platform.Platform[] platforms = (RSG.Platform.Platform[])Enum.GetValues(typeof(RSG.Platform.Platform));
            String[] platformNames;

            if (target is SmoketestTargetx64)
            {
                platformNames = new String[platforms.Length];
            }
            else
            {
                platformNames = new String[platforms.Length-1];
            }

            int i = 0;
            foreach (RSG.Platform.Platform plat in platforms)
            {
                //add pc configs after
                if (plat != RSG.Platform.Platform.Win64)
                {
                    platformNames[i] = plat.PlatformToRagebuilderPlatform();
                    i++;
                }
            }

            String outPlatform = Platform.PlatformToRagebuilderPlatform();

            if (target is SmoketestTargetx64)
            {
                SmoketestTargetx64 pcTarget = target as SmoketestTargetx64;
                String[] pcNames = new String[1];

                pcNames[0] = String.Join(".",   pcTarget.MachineSpecs.GetMachineName(), 
                                                Regex.Replace(pcTarget.MachineSpecs.Processor.Name, @"\s+", " "),
                                                pcTarget.MachineSpecs.Processor.NumberOfCores,
                                                pcTarget.MachineSpecs.Memory.Capacity,
                                                Regex.Replace(pcTarget.MachineSpecs.Graphics.Name, @"\s+", " "),
                                                pcTarget.MachineSpecs.Graphics.Memory,
                                                pcTarget.MachineSpecs.Graphics.Driver);

                DBPlatformPCLookup.CreateAndCacheDBColumnHeaders(sqlConnection, Log, pcNames);

                //foreach pc config, add another entry, x64A, etc.
                int platExt = DBPlatformPCLookup.LookUpValue(pcNames[0], Log) + (int)'@';
                String pcPlatform = String.Format("{0}_{1}", RSG.Platform.Platform.Win64.PlatformToRagebuilderPlatform(), (char)platExt);

                platformNames[i] = pcPlatform;
                outPlatform = pcPlatform;
            }

            DBPlatformLookup.CreateAndCacheDBColumnHeaders(sqlConnection, Log, platformNames);

            return outPlatform;
        }

        /// <summary>
        /// Handles creation and dumping of Location data to the Location_Lookup table.  Creates the C#
        /// side lookup as well.
        /// </summary>
        /// <param name="">sqlConnection</param>
        /// <param name="">locationNames</param>
        /// <returns></returns>
        /// 
        protected void CreateLocationLookupTable(SqlConnection sqlConnection, IEnumerable<String> locationNames)
        {
            IList<String> lNames = locationNames as IList<String>;
            if (!DBLocationLookup.Exists(sqlConnection, Log))
            {
                DBLocationLookup.CreateTable(sqlConnection, Log);
            }
            String[] names = new String[lNames.Count];

            int i = 0;
            foreach (String name in locationNames)
            {
                names[i] = name;
                i++;
            }

            DBLocationLookup.CreateAndCacheDBColumnHeaders(sqlConnection, Log, names); //locationNames.ToArray());
        }

        public override void DumpDBColumnHeaders(SqlConnection sqlConnection)
        {
            String headers = String.Join(",", "Location", "Date", "CL", "BuildVersion", "FPS", "MSpFrame", "exeSize", "");
        }

        public override void Set(float theNum)
        {
            PerfResults.Set(theNum);            
            CL = Convert.ToInt32(theNum);
            ExeSize = VirtualSlosh = PhysicalSlosh = Convert.ToUInt64(theNum);
            Crashed = false;
            TestComplete = false;
        }

        public void Dump(IUniversalLog log)
        {
            log.MessageCtx(LOG_CTX, "CL{0} FPS:{1} MSpFrame:{2} exe:{3} KB virtSlosh:{4} KB physSlosh:{5} KB",
                    CL, PerfResults.FPS.Average.Value, PerfResults.MSPFrame.Average.Value, ExeSize, VirtualSlosh, PhysicalSlosh);
            foreach (MemoryHeap heap in MemoryHeaps)
            {
                heap.Dump(log);
            }
            foreach (MemoryBucket bucket in MemoryBuckets)
            {
                bucket.Dump(log);
            }
            foreach (StreamingModule module in StreamingModules)
            {
                module.Dump(log);
            }
            foreach (TimeBar timebar in TimeBars)
            {
                timebar.Dump(log);
            }
        }
   }

   class MeasuredResult
   {
       protected static readonly String LOG_CTX = "Measured Result";

       public DBFloatData Min = new DBFloatData("Min", "FloatMetric");
       public DBFloatData Max = new DBFloatData("Max", "FloatMetric");
       public DBFloatData Average = new DBFloatData("Average", "FloatMetric");
       public DBFloatData Std = new DBFloatData("Std", "FloatMetric");

       public void Dump(IUniversalLog log)
       {
           log.MessageCtx(LOG_CTX, "MeasuredResult Ave:{0} Min:{1} Max:{2} Std:{3}", Average.Value, Min.Value, Max.Value, Std.Value);
       }
   }

    class PerformanceResults : IDBTables
    {   
        public String FPSName = "FPS";
        public String MSPFrameName = "MSpFrame";
        public MeasuredResult FPS;
        public MeasuredResult MSPFrame;

        public PerformanceResults()
        {
            DBTable = new SmokeTestDBTable("dbo.Performance");
            DBLookUpTable = new SmokeTestPerformanceResultsDBLookUpTable("dbo.Performance_Lookup", "Performance_ID");
            FPS = new MeasuredResult();
            MSPFrame = new MeasuredResult();
        }

        public bool CreateTables(SqlConnection sqlConnection, IUniversalLog log, SmokeTestDBTable perfMetrics, SmokeTestDBLookUpTable locationLookup)
        {
            bool lookupExists = true;
            bool tableExists = true;
            if (!DBLookUpTable.Exists(sqlConnection, log))
            {
                lookupExists = DBLookUpTable.CreateTable(sqlConnection, log);
            }

            if (!DBTable.Exists(sqlConnection, log))
            {
                log.ErrorCtx(LOG_CTX, "{0} table NOT found.  Must create...", DBTable.TableName);
                IList<String> columnDescs = new List<String>();
                columnDescs.Add("ID int NOT NULL PRIMARY KEY IDENTITY(1,1)");
                columnDescs.Add("TestRun_ID int NOT NULL");
                columnDescs.Add("Location_ID int NOT NULL");
                columnDescs.Add("Performance_ID int NOT NULL");
                columnDescs.Add("FloatMetric float");
                columnDescs.Add("IntMetric int");
                columnDescs.Add(String.Format("FOREIGN KEY (TestRun_ID) REFERENCES {0} ({1})",
                                          perfMetrics.TableName, "TestRun_ID"));
                columnDescs.Add(String.Format("FOREIGN KEY (Location_ID) REFERENCES {0} ({1})",
                                          locationLookup.TableName, locationLookup.LookUpColumn));
                columnDescs.Add(String.Format("FOREIGN KEY (Performance_ID) REFERENCES {0} ({1})",
                                          DBLookUpTable.TableName, DBLookUpTable.LookUpColumn));
                tableExists = DBTable.CreateTable(sqlConnection, log, columnDescs);
            }
            return tableExists && lookupExists;
        }

        public void Set(float theNum)
        {
            FPS.Min.Value = FPS.Max.Value = FPS.Average.Value = FPS.Std.Value = MSPFrame.Min.Value = 
                MSPFrame.Max.Value = MSPFrame.Average.Value = MSPFrame.Std.Value = theNum;
        }

        public void DumpToDB(SqlConnection sqlConnection, int testRunID, int locationID, IUniversalLog log)
        {
            String columns = "(Performance_ID,TestRun_ID,Location_ID,FloatMetric)";
            String values = "(@Performance_ID,@TestRun_ID,@Location_ID,@FloatMetric)";
            String insertString = String.Format("INSERT INTO {0} {1} VALUES {2};", DBTable.TableName, columns, values);
            SqlCommand insertCommand = new SqlCommand(insertString, sqlConnection);

            insertCommand.Parameters.Add(new SqlParameter("@TestRun_ID", testRunID));
            insertCommand.Parameters.Add(new SqlParameter("@Location_ID", locationID));
            
            //we'll overwrite these
            insertCommand.Parameters.Add(new SqlParameter("@" + DBLookUpTable.LookUpColumn, 
                                                               DBLookUpTable.LookUpValue(FPSName + FPS.Min.Name, log)));
            insertCommand.Parameters.Add(new SqlParameter("@" + FPS.Min.DBColumn, FPS.Min.Value));

            DBLookUpTable.InsertRow(insertCommand, log, FPSName, FPS.Min);
            DBLookUpTable.InsertRow(insertCommand, log, FPSName, FPS.Max);
            DBLookUpTable.InsertRow(insertCommand, log, FPSName, FPS.Average);
            DBLookUpTable.InsertRow(insertCommand, log, FPSName, FPS.Std);
            DBLookUpTable.InsertRow(insertCommand, log, MSPFrameName, MSPFrame.Min);
            DBLookUpTable.InsertRow(insertCommand, log, MSPFrameName, MSPFrame.Max);
            DBLookUpTable.InsertRow(insertCommand, log, MSPFrameName, MSPFrame.Average);
            DBLookUpTable.InsertRow(insertCommand, log, MSPFrameName, MSPFrame.Std);
            PerfMetrics.NumDBInserts += 8;
        }

        public void DumpToLookUpDB(SqlConnection sqlConnection, IUniversalLog log)
        {
            String heaps = String.Format("{0}{1},{0}{2},{0}{3},{0}{4},{5}{6},{5}{7},{5}{8},{5}{9}",
                                    FPSName, FPS.Min.Name, FPS.Max.Name, FPS.Average.Name, FPS.Std.Name,
                                    MSPFrameName, MSPFrame.Min.Name, MSPFrame.Max.Name, MSPFrame.Average.Name, MSPFrame.Std.Name);
            String[] columnNames = heaps.Split(',');

            DBLookUpTable.CreateAndCacheDBColumnHeaders(sqlConnection, log, columnNames);
        }
   }

   class MemoryHeap : IDBTables
   {
       public int Idx;
       public String Name;
       public DBBigIntData Used = new DBBigIntData("Used", "IntMetric");
       public DBBigIntData Free = new DBBigIntData("Free", "IntMetric");
       public DBBigIntData Total = new DBBigIntData("Total", "IntMetric");
       public DBBigIntData Peak = new DBBigIntData("Peak", "IntMetric");
       public DBBigIntData Frag = new DBBigIntData("Frag", "IntMetric");

       public MemoryHeap()
       {
           DBTable = new SmokeTestDBTable("dbo.Memory_Heap");
           DBLookUpTable = new SmokeTestMemoryHeapDBLookUpTable("dbo.Memory_Heap_Lookup", "Heap_ID");
       }

       public bool CreateTables(SqlConnection sqlConnection, IUniversalLog log, SmokeTestDBTable perfMetrics, SmokeTestDBLookUpTable locationLookup)
       {
           bool lookupExists = true;
           bool tableExists = true;
          
           if (!DBLookUpTable.Exists(sqlConnection, log))
           {
               lookupExists = DBLookUpTable.CreateTable(sqlConnection, log);
           }

           if (!DBTable.Exists(sqlConnection, log))
           {
               log.ErrorCtx(LOG_CTX, "{0} table NOT found.  Must create...", DBTable.TableName);
               IList<String> columnDescs = new List<String>();
               columnDescs.Add("ID int NOT NULL PRIMARY KEY IDENTITY(1,1)");
               columnDescs.Add("TestRun_ID int NOT NULL");
               columnDescs.Add("Location_ID int NOT NULL");
               columnDescs.Add("Heap_ID int NOT NULL");
               columnDescs.Add("IntMetric bigint NOT NULL");
               columnDescs.Add(String.Format("FOREIGN KEY (TestRun_ID) REFERENCES {0} ({1})",
                                         perfMetrics.TableName, "TestRun_ID"));
               columnDescs.Add(String.Format("FOREIGN KEY (Location_ID) REFERENCES {0} ({1})",
                                         locationLookup.TableName, locationLookup.LookUpColumn));
               columnDescs.Add(String.Format("FOREIGN KEY (Heap_ID) REFERENCES {0} ({1})",
                                         DBLookUpTable.TableName, DBLookUpTable.LookUpColumn));
               tableExists = DBTable.CreateTable(sqlConnection, log, columnDescs);
           }
           return tableExists && lookupExists;
       }

       public void Dump(IUniversalLog log)
       {
           log.MessageCtx(LOG_CTX, "heapName:{0} Used:{1} Free:{2} Total:{3} Peak:{4} Frag:{5}",
                                   Name, Used.Value, Free.Value, Total.Value, Peak.Value, Frag.Value);
       }

       public void DumpHeaderToCSV(StreamWriter fileStream)
       {
           String heap = String.Format("{0}{1},{0}{2},{0}{3},{0}{4},{0}{5},", 
                                   Name, Used.Name, Free.Name, Total.Name, Peak.Name, Frag.Name);
           fileStream.Write(heap);
       }

       public void DumpToCSV(StreamWriter fileStream)
       {
           String heap = String.Join(",", Used.Value, Free.Value, Total.Value, Peak.Value, Frag.Value, "");
           fileStream.Write(heap);
       }

       public void DumpToDB(SqlConnection sqlConnection, int testRunID, int locationID, IUniversalLog log)
       {
           String columns = "(Heap_ID,TestRun_ID,Location_ID,IntMetric)";
           String values = "(@Heap_ID,@TestRun_ID,@Location_ID,@IntMetric)";
           String insertString = String.Format("INSERT INTO {0} {1} VALUES {2};", DBTable.TableName, columns, values);
           SqlCommand insertCommand = new SqlCommand(insertString, sqlConnection);

           insertCommand.Parameters.Add(new SqlParameter("@TestRun_ID", testRunID));
           insertCommand.Parameters.Add(new SqlParameter("@Location_ID", locationID));
           
           //we'll overwrite these
           insertCommand.Parameters.Add(new SqlParameter("@" + DBLookUpTable.LookUpColumn, 
                                                              DBLookUpTable.LookUpValue(Name + Used.Name, log)));
           insertCommand.Parameters.Add(new SqlParameter("@" + Used.DBColumn, (Int64)Used.Value));

           DBLookUpTable.InsertRow(insertCommand, log, Name, Used);
           DBLookUpTable.InsertRow(insertCommand, log, Name, Free);
           DBLookUpTable.InsertRow(insertCommand, log, Name, Total);
           DBLookUpTable.InsertRow(insertCommand, log, Name, Peak);
           DBLookUpTable.InsertRow(insertCommand, log, Name, Frag);

           PerfMetrics.NumDBInserts += 5;
       }

       public void DumpToLookUpDB(SqlConnection sqlConnection, IUniversalLog log)
       {
           String heaps = String.Format("{0}{1},{0}{2},{0}{3},{0}{4},{0}{5}", Name, Used.Name, Free.Name, Total.Name, Peak.Name, Frag.Name);
           String[] columnNames = heaps.Split(',');

           DBLookUpTable.CreateAndCacheDBColumnHeaders(sqlConnection, log, columnNames);
       }
   }

   class StreamingModule : IDBTables
   {
       public String Name;
       public int ID;
       public DBBigIntData LoadedMemVirt = new DBBigIntData("LoadedVirt", "IntMetric");
       public DBBigIntData LoadedMemPhys = new DBBigIntData("LoadedPhys", "IntMetric");
       public DBBigIntData RequiredMemVirt = new DBBigIntData("ReqVirt", "IntMetric");
       public DBBigIntData RequiredMemPhys = new DBBigIntData("ReqPhys", "IntMetric");

       public StreamingModule()
       {
           DBTable = new SmokeTestDBTable("dbo.Streaming_Module");
           DBLookUpTable = new SmokeTestStreamingModuleDBLookUpTable("dbo.Streaming_Module_Lookup", "Module_ID");
       }

       public bool CreateTables(SqlConnection sqlConnection, IUniversalLog log, SmokeTestDBTable perfMetrics, SmokeTestDBLookUpTable locationLookup)
       {
           bool lookupExists = true;
           bool tableExists = true;
           
           if (!DBLookUpTable.Exists(sqlConnection, log))
           {
               lookupExists = DBLookUpTable.CreateTable(sqlConnection, log);
           }

           if (!DBTable.Exists(sqlConnection, log))
           {
               log.ErrorCtx(LOG_CTX, "{0} table NOT found.  Must create...", DBTable.TableName);
               IList<String> columnDescs = new List<String>();
               columnDescs.Add("ID int NOT NULL PRIMARY KEY IDENTITY(1,1)");
               columnDescs.Add("TestRun_ID int NOT NULL");
               columnDescs.Add("Location_ID int NOT NULL");
               columnDescs.Add("Module_ID int NOT NULL");
               columnDescs.Add("IntMetric bigint NOT NULL");
               columnDescs.Add(String.Format("FOREIGN KEY (TestRun_ID) REFERENCES {0} ({1})",
                                         perfMetrics.TableName, "TestRun_ID"));
               columnDescs.Add(String.Format("FOREIGN KEY (Location_ID) REFERENCES {0} ({1})",
                                         locationLookup.TableName, locationLookup.LookUpColumn));
               columnDescs.Add(String.Format("FOREIGN KEY (Module_ID) REFERENCES {0} ({1})",
                                         DBLookUpTable.TableName, DBLookUpTable.LookUpColumn));
               tableExists = DBTable.CreateTable(sqlConnection, log, columnDescs);
           }
           return tableExists && lookupExists;
       }

       public void Dump(IUniversalLog log)
       {
           log.MessageCtx(LOG_CTX, "moduleName:{0} loadedVirt:{1} loadedPhys:{2} ReqVirt:{3} ReqPhys:{4}",
               Name, LoadedMemVirt.Value, LoadedMemPhys.Value, RequiredMemVirt.Value, RequiredMemPhys.Value);
       }
       public void DumpHeaderToCSV(StreamWriter fileStream)
       {
           String module = String.Format("{0}{1},{0}{2},{0}{3},{0}{4},",
                                          Name, LoadedMemVirt.Name, LoadedMemPhys.Name, RequiredMemVirt.Name, RequiredMemPhys.Name);
           fileStream.Write(module);
       }
       public void DumpToCSV(StreamWriter fileStream)
       {
           String module = String.Join(",", LoadedMemVirt.Value, LoadedMemPhys.Value, RequiredMemVirt.Value, RequiredMemPhys.Value, "");
           fileStream.Write(module);
       }

       public void DumpToDB(SqlConnection sqlConnection, int testRunID, int locationID, IUniversalLog log)
       {
           String columns = "(Module_ID,TestRun_ID,Location_ID,IntMetric)";
           String values = "(@Module_ID,@TestRun_ID,@Location_ID,@IntMetric)";
           String insertString = String.Format("INSERT INTO {0} {1} VALUES {2};", DBTable.TableName, columns, values);
           SqlCommand insertCommand = new SqlCommand(insertString, sqlConnection);

           insertCommand.Parameters.Add(new SqlParameter("@TestRun_ID", testRunID));
           insertCommand.Parameters.Add(new SqlParameter("@Location_ID", locationID));

           //we'll overwrite these
           insertCommand.Parameters.Add(new SqlParameter("@" + DBLookUpTable.LookUpColumn, 
                                                               DBLookUpTable.LookUpValue(Name + "LoadedVirt", log)));
           insertCommand.Parameters.Add(new SqlParameter("@" + LoadedMemVirt.DBColumn, (Int64)LoadedMemVirt.Value));

           DBLookUpTable.InsertRow(insertCommand, log, Name, LoadedMemVirt);
           DBLookUpTable.InsertRow(insertCommand, log, Name, LoadedMemPhys);
           DBLookUpTable.InsertRow(insertCommand, log, Name, RequiredMemVirt);
           DBLookUpTable.InsertRow(insertCommand, log, Name, RequiredMemPhys);

           PerfMetrics.NumDBInserts += 4;
       }

       public void DumpToLookUpDB(SqlConnection sqlConnection, IUniversalLog log)
       {
           String heaps = String.Format("{0}{1},{0}{2},{0}{3},{0}{4}",
                                         Name, LoadedMemVirt.Name, LoadedMemPhys.Name, RequiredMemVirt.Name, RequiredMemPhys.Name);
           String[] columnNames = heaps.Split(',');

           DBLookUpTable.CreateAndCacheDBColumnHeaders(sqlConnection, log, columnNames);
       }
   }

   class MemoryBucket : IDBTables
   {
       public String Name;
       public DBBigIntData GameVirt = new DBBigIntData("GameVirt", "IntMetric");
       public DBBigIntData GamePhys = new DBBigIntData("GamePhys", "IntMetric");
       public DBBigIntData ResVirt = new DBBigIntData("ResVirt", "IntMetric");
       public DBBigIntData ResPhys = new DBBigIntData("ResPhys", "IntMetric");

       public MemoryBucket()
       {
           DBTable = new SmokeTestDBTable("dbo.Memory_Bucket");
           DBLookUpTable = new SmokeTestMemoryBucketDBLookUpTable("dbo.Memory_Bucket_Lookup", "Bucket_ID");
       }

       public bool CreateTables(SqlConnection sqlConnection, IUniversalLog log, SmokeTestDBTable perfMetrics, SmokeTestDBLookUpTable locationLookup)
       {
           bool lookupExists = true;
           bool tableExists = true;
          
           if (!DBLookUpTable.Exists(sqlConnection, log))
           {
               lookupExists = DBLookUpTable.CreateTable(sqlConnection, log);
           }

           if (!DBTable.Exists(sqlConnection, log))
           {
               log.ErrorCtx(LOG_CTX, "{0} table NOT found.  Must create...", DBTable.TableName);
               IList<String> columnDescs = new List<String>();
               columnDescs.Add("ID int NOT NULL PRIMARY KEY IDENTITY(1,1)");
               columnDescs.Add("TestRun_ID int NOT NULL");
               columnDescs.Add("Location_ID int NOT NULL");
               columnDescs.Add("Bucket_ID int NOT NULL");
               columnDescs.Add("IntMetric bigint NOT NULL");
               columnDescs.Add(String.Format("FOREIGN KEY (TestRun_ID) REFERENCES {0} ({1})",
                                         perfMetrics.TableName, "TestRun_ID"));
               columnDescs.Add(String.Format("FOREIGN KEY (Location_ID) REFERENCES {0} ({1})",
                                         locationLookup.TableName, locationLookup.LookUpColumn));
               columnDescs.Add(String.Format("FOREIGN KEY (Bucket_ID) REFERENCES {0} ({1})",
                                         DBLookUpTable.TableName, DBLookUpTable.LookUpColumn));
               tableExists = DBTable.CreateTable(sqlConnection, log, columnDescs);
           }
           return tableExists && lookupExists;
       }

       public void Dump(IUniversalLog log)
       {
           log.MessageCtx(LOG_CTX, "memBucket:{0} gameVirt:{1} gamePhys:{2} resVirt:{3} resPhys:{4}",
               Name, GameVirt.Value, GamePhys.Value, ResVirt.Value, ResPhys.Value);
       }

       public void DumpHeaderToCSV(StreamWriter fileStream)
       {
           String bucket = String.Format("{0}{1},{0}{2},{0}{3},{0}{4},",
                                          Name, GameVirt.Name, GamePhys.Name, ResVirt.Name, ResPhys.Name);
           fileStream.Write(bucket);
       }

       public void DumpToCSV(StreamWriter fileStream)
       {
           String bucket = String.Join(",", GameVirt.Value, GamePhys.Value, ResVirt.Value, ResPhys.Value, "");
           fileStream.Write(bucket);
       }

       public void DumpToDB(SqlConnection sqlConnection, int testRunID, int locationID, IUniversalLog log)
       {
           String columns = "(Bucket_ID,TestRun_ID,Location_ID,IntMetric)";
           String values = "(@Bucket_ID,@TestRun_ID,@Location_ID,@IntMetric)";
           String insertString = String.Format("INSERT INTO {0} {1} VALUES {2};", DBTable.TableName, columns, values);
           SqlCommand insertCommand = new SqlCommand(insertString, sqlConnection);

           insertCommand.Parameters.Add(new SqlParameter("@TestRun_ID", testRunID));
           insertCommand.Parameters.Add(new SqlParameter("@Location_ID", locationID));

           //we'll overwrite these
           insertCommand.Parameters.Add(new SqlParameter("@" + DBLookUpTable.LookUpColumn, 
                                                               DBLookUpTable.LookUpValue(Name + "GameVirt", log)));
           insertCommand.Parameters.Add(new SqlParameter("@" + GameVirt.DBColumn, (Int64)GameVirt.Value));

           DBLookUpTable.InsertRow(insertCommand, log, Name, GameVirt);
           DBLookUpTable.InsertRow(insertCommand, log, Name, GamePhys);
           DBLookUpTable.InsertRow(insertCommand, log, Name, ResVirt);
           DBLookUpTable.InsertRow(insertCommand, log, Name, ResPhys);

           PerfMetrics.NumDBInserts += 4;
       }

       public void DumpToLookUpDB(SqlConnection sqlConnection, IUniversalLog log)
       {
           String heaps = String.Format("{0}{1},{0}{2},{0}{3},{0}{4}",
                                         Name, GameVirt.Name, GamePhys.Name, ResVirt.Name, ResPhys.Name);
           String[] columnNames = heaps.Split(',');

           DBLookUpTable.CreateAndCacheDBColumnHeaders(sqlConnection, log, columnNames);
       }
   }

   class TimeBar : IDBTables
   {
       public String Name;

	   //timebars have 2 lookup tables, 1 for timebar name, 1 for set name
       public String SetName;
       public SmokeTestDBLookUpTable DBSetLookUpTable;

       public String TimeBarName = "TimeBar";
       public MeasuredResult TimeBarValues;

       public TimeBar()
       {
           TimeBarValues = new MeasuredResult();
           DBTable = new SmokeTestDBTable("dbo.Time_Bar");
           DBLookUpTable = new SmokeTestTimeBarDBLookUpTable("dbo.Time_Bar_Lookup", "TimeBar_ID");
           DBSetLookUpTable = new SmokeTestTimeBarSetDBLookUpTable("dbo.Time_Bar_Set_Lookup", "TimeBarSet_ID");
       }

       public bool CreateTables(SqlConnection sqlConnection, IUniversalLog log, SmokeTestDBTable perfMetrics, SmokeTestDBLookUpTable locationLookup)
       {
           bool lookupExists = true;
           bool lookupSetExists = true;
           bool tableExists = true;
          
           if (!DBLookUpTable.Exists(sqlConnection, log))
           {
               lookupExists = DBLookUpTable.CreateTable(sqlConnection, log);
           }

           if (!DBSetLookUpTable.Exists(sqlConnection, log))
           {
               lookupSetExists = DBSetLookUpTable.CreateTable(sqlConnection, log);
           }

           if (!DBTable.Exists(sqlConnection, log))
           {
               log.ErrorCtx(LOG_CTX, "{0} table NOT found.  Must create...", DBTable.TableName);
               IList<String> columnDescs = new List<String>();
               columnDescs.Add("ID int NOT NULL PRIMARY KEY IDENTITY(1,1)");
               columnDescs.Add("TestRun_ID int NOT NULL");
               columnDescs.Add("Location_ID int NOT NULL");
               columnDescs.Add("TimeBar_ID int NOT NULL");
               columnDescs.Add("TimeBarSet_ID int NOT NULL");
               columnDescs.Add("FloatMetric float NOT NULL");
               columnDescs.Add(String.Format("FOREIGN KEY (TestRun_ID) REFERENCES {0} ({1})",
                                         perfMetrics.TableName, "TestRun_ID"));
               columnDescs.Add(String.Format("FOREIGN KEY (Location_ID) REFERENCES {0} ({1})",
                                         locationLookup.TableName, locationLookup.LookUpColumn));
               columnDescs.Add(String.Format("FOREIGN KEY (TimeBar_ID) REFERENCES {0} ({1})",
                                         DBLookUpTable.TableName, DBLookUpTable.LookUpColumn));
               columnDescs.Add(String.Format("FOREIGN KEY (TimeBarSet_ID) REFERENCES {0} ({1})",
                                        DBSetLookUpTable.TableName, DBSetLookUpTable.LookUpColumn));
               tableExists = DBTable.CreateTable(sqlConnection, log, columnDescs);
           }

           return tableExists && lookupExists && lookupSetExists;
       }

       public void Dump(IUniversalLog log)
       {
           log.MessageCtx(LOG_CTX, "timebar:{0} set:{1} min:{2} max:{3} ave:{4} std:{5}",
               Name, SetName, TimeBarValues.Min.Value, TimeBarValues.Max.Value, TimeBarValues.Average.Value, 
               TimeBarValues.Std.Value);
       }

       public void DumpHeaderToCSV(StreamWriter fileStream)
       {
           String timebar = String.Format("{0}:{1}{2},{0}:{1}{3},{0}:{1}{4},{0}:{1}{5},",
                                          SetName, Name, TimeBarValues.Min.Name, TimeBarValues.Max.Name, 
                                          TimeBarValues.Average.Name, TimeBarValues.Std.Name);
           fileStream.Write(timebar);
       }

       public void DumpToCSV(StreamWriter fileStream)
       {
           String timebar = String.Join(",", TimeBarValues.Min.Value, TimeBarValues.Max.Value, 
                                            TimeBarValues.Average.Value, TimeBarValues.Std.Value, "");
           fileStream.Write(timebar);
       }

       public void DumpToDB(SqlConnection sqlConnection, int testRunID, int locationID, IUniversalLog log)
       {
           String columns = "(TimeBar_ID,TimeBarSet_ID, TestRun_ID,Location_ID,FloatMetric)";
           String values = "(@TimeBar_ID,@TimeBarSet_ID, @TestRun_ID,@Location_ID,@FloatMetric)";
           String insertString = String.Format("INSERT INTO {0} {1} VALUES {2};", DBTable.TableName, columns, values);
           SqlCommand insertCommand = new SqlCommand(insertString, sqlConnection);

           insertCommand.Parameters.Add(new SqlParameter("@TestRun_ID", testRunID));
           insertCommand.Parameters.Add(new SqlParameter("@Location_ID", locationID));
           insertCommand.Parameters.Add(new SqlParameter("@" + DBSetLookUpTable.LookUpColumn,
                                                               DBSetLookUpTable.LookUpValue(SetName, log)));

           //we'll overwrite these
           insertCommand.Parameters.Add(new SqlParameter("@" + DBLookUpTable.LookUpColumn,
                                                               DBLookUpTable.LookUpValue(Name + TimeBarValues.Min.Name, log)));
           insertCommand.Parameters.Add(new SqlParameter("@" + TimeBarValues.Min.DBColumn, TimeBarValues.Min.Value));

           DBLookUpTable.InsertRow(insertCommand, log, Name, TimeBarValues.Min);
           DBLookUpTable.InsertRow(insertCommand, log, Name, TimeBarValues.Max);
           DBLookUpTable.InsertRow(insertCommand, log, Name, TimeBarValues.Average);
           DBLookUpTable.InsertRow(insertCommand, log, Name, TimeBarValues.Std);

           PerfMetrics.NumDBInserts += 4;
       }

       public void DumpToLookUpDB(SqlConnection sqlConnection, IUniversalLog log)
       {
           String timebar = String.Format("{0}{1},{0}{2},{0}{3},{0}{4}",
                                         Name, TimeBarValues.Min.Name, TimeBarValues.Max.Name, 
                                               TimeBarValues.Average.Name, TimeBarValues.Std.Name);
           String[] columnNames = timebar.Split(',');

           DBLookUpTable.CreateAndCacheDBColumnHeaders(sqlConnection, log, columnNames);

           String sets = String.Format("{0}", SetName);
           String[] setColumnNames = sets.Split(',');

           DBSetLookUpTable.CreateAndCacheDBColumnHeaders(sqlConnection, log, setColumnNames);
       }
   }
}
