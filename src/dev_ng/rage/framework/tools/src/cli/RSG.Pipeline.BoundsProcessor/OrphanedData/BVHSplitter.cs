﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;
using RSG.Bounds;

namespace RSG.Pipeline.BoundsProcessor.OrphanedData
{
    /// <summary>
    /// Class responsible for splitting a collection of primitives that is greater than the BVH limit up into child BVHs
    /// We want as few splits as possible
    /// We want splits that are as flat at the edges as possible (as requested by EF)
    /// We want BVHs that are as close to the size limit without going over as possible
    /// </summary>
    internal class BVHSplitter
    {
        internal BVHSplitter(int maxBVHDataSize, IEnumerable<Primitive> primitives)
        {
            maxBVHDataSize_ = maxBVHDataSize;
            primitives_ = primitives;
        }

        internal void Split(BNDFile.CollisionType collisionType)
        {
            if (collisionType.HasFlag(BNDFile.CollisionType.Mover))
            {
                SplitMover();
            }
            else
            {
                SplitNonMover();
            }
        }

        private void SplitMover()
        {
            PrimitiveBlock masterBlock = new PrimitiveBlock(primitives_);

            // The split is based on how far over the limit we are
            // We try 2x1, 2x2, 3x2, 3x3 and 4x4
            double overloadFactor = (double)masterBlock.EstimatedSize / (double)maxBVHDataSize_;
            if (overloadFactor < splitTwoThreshold_)
            {
                // Split into two
                if (masterBlock.BoundsXY.Width > masterBlock.BoundsXY.Height)
                    Split(2, 1, masterBlock.BoundsXY);
                else
                    Split(1, 2, masterBlock.BoundsXY);
            }
            else if (overloadFactor < splitFourThreshold_)
            {
                // Split into four
                Split(2, 2, masterBlock.BoundsXY);
            }
            else if (overloadFactor < splitSixThreshold_)
            {
                // Split into six
                if (masterBlock.BoundsXY.Width > masterBlock.BoundsXY.Height)
                    Split(3, 2, masterBlock.BoundsXY);
                else
                    Split(2, 3, masterBlock.BoundsXY);
            }
            else if (overloadFactor < splitNineThreshold_)
            {
                // Split into nine
                Split(3, 3, masterBlock.BoundsXY);
            }
            else
            {
                // Split into sixteen
                Split(4, 4, masterBlock.BoundsXY);
            }
        }

        private void SplitNonMover()
        {
            List<List<Primitive>> splitPrimitives = new List<List<Primitive>>();

            List<Primitive> currentBlock = new List<Primitive>();
            int currentBucketEstimatedSize = 0;

            foreach (Primitive primitive in primitives_)
            {
                if (currentBucketEstimatedSize + primitive.EstimatedSize > maxBVHDataSize_)
                {
                    splitPrimitives.Add(currentBlock);
                    currentBlock = new List<Primitive>();
                    currentBucketEstimatedSize = 0;
                }

                currentBlock.Add(primitive);
                currentBucketEstimatedSize += primitive.EstimatedSize;
            }

            if (currentBlock.Count > 0)
            {
                splitPrimitives.Add(currentBlock);
            }

            SplitPrimitives = splitPrimitives;
        }

        private void Split(int divisionsX, int divisionsY, BoundingBox2f boundsXY)
        {
            float startX = boundsXY.Min.X;
            float startY = boundsXY.Min.Y;
            float sectionSizeX = (boundsXY.Max.X - boundsXY.Min.X) / (float)divisionsX;
            float sectionSizeY = (boundsXY.Max.Y - boundsXY.Min.Y) / (float)divisionsY;

            List<float> xSplits = new List<float>();// the Y axis positions to split in X on
            for (int xSplitIndex = 1; xSplitIndex < divisionsX; ++xSplitIndex)
            {
                float splitPosition = startX + (sectionSizeX * (float)xSplitIndex);
                BestSplitFinder.FindBestSplit(primitives_, ref splitPosition, Primitive.CardinalAxis.Y, sectionSizeX);
                xSplits.Add(splitPosition);
            }

            List<float> ySplits = new List<float>();// the X axis positions to split in Y on
            for (int ySplitIndex = 1; ySplitIndex < divisionsY; ++ySplitIndex)
            {
                float splitPosition = startY + (sectionSizeY * (float)ySplitIndex);
                BestSplitFinder.FindBestSplit(primitives_, ref splitPosition, Primitive.CardinalAxis.X, sectionSizeY);
                ySplits.Add(splitPosition);
            }

            // Split the primitives up into a 2D grid
            Dictionary<Vector2i, List<Primitive>> splitPrimitivesMap = new Dictionary<Vector2i, List<Primitive>>();
            foreach (Primitive primitive in primitives_)
            {
                int xPos = divisionsX - 1;
                for (int splitIndex = 0; splitIndex < xSplits.Count && xPos != divisionsX; ++splitIndex)
                {
                    if (primitive.CentreXY.X <= xSplits[splitIndex])
                    {
                        xPos = splitIndex;
                        break;
                    }
                }

                int yPos = divisionsY - 1;
                for (int splitIndex = 0; splitIndex < ySplits.Count && yPos != divisionsY; ++splitIndex)
                {
                    if (primitive.CentreXY.Y <= ySplits[splitIndex])
                    {
                        yPos = splitIndex;
                        break;
                    }
                }

                Vector2i sectionIndex = new Vector2i(xPos, yPos);

                if (!splitPrimitivesMap.ContainsKey(sectionIndex))
                {
                    splitPrimitivesMap.Add(sectionIndex, new List<Primitive>());
                }
                splitPrimitivesMap[sectionIndex].Add(primitive);
            }

            List<List<Primitive>> splitPrimitives = new List<List<Primitive>>();

            // B* 949514 - quick addition of a second pass
            List<Vector2i> secondPassKeys = new List<Vector2i>();
            foreach (KeyValuePair<Vector2i, List<Primitive>> splitPrimitivesPair in splitPrimitivesMap)
            {
                PrimitiveBlock splitBlock = new PrimitiveBlock(splitPrimitivesPair.Value);
                if (splitBlock.EstimatedSize > maxBVHDataSize_)
                    secondPassKeys.Add(splitPrimitivesPair.Key);
            }

            foreach (Vector2i secondPassKey in secondPassKeys)
            {
                List<Primitive> primitivesToSplit = splitPrimitivesMap[secondPassKey];

                // Split in largest dimension
                IEnumerable<Primitive> primitivesOrderedForSplit = null;
                if (sectionSizeX >= sectionSizeY)
                    primitivesOrderedForSplit = primitivesToSplit.OrderBy(prim => prim.MinX);
                else
                    primitivesOrderedForSplit = primitivesToSplit.OrderBy(prim => prim.MinY);

                List<Primitive> currentBlock = new List<Primitive>();
                int currentBlockSize = 0;

                foreach (Primitive primitive in primitivesOrderedForSplit)
                {
                    if (currentBlockSize + primitive.EstimatedSize >= maxBVHDataSize_)
                    {
                        splitPrimitives.Add(currentBlock);
                        currentBlock = new List<Primitive>();
                        currentBlockSize = 0;
                    }

                    currentBlock.Add(primitive);
                    currentBlockSize += primitive.EstimatedSize;
                }

                if (currentBlock.Count > 0)
                    splitPrimitives.Add(currentBlock);// Add the straggler

                splitPrimitivesMap.Remove(secondPassKey);
            }

            // The last step is just to flatten
            foreach (KeyValuePair<Vector2i, List<Primitive>> splitPrimitivesPair in splitPrimitivesMap)
                splitPrimitives.Add(splitPrimitivesPair.Value);

            SplitPrimitives = splitPrimitives;
        }

        internal IEnumerable<IEnumerable<Primitive>> SplitPrimitives { get; private set; }

        // As we are less likely to get even splits the more we split, we reduce the thresholds appropriately
        // (splitDepreciationFactor_ ^ numSplits) * numRegions
        private static readonly double splitDepreciationFactor_ = 0.9;// Increase this is we're happy to have higher concentrations in some regions than others, else reduce it.
        private static readonly double splitTwoThreshold_ = Math.Pow(splitDepreciationFactor_, 1) * 2;
        private static readonly double splitFourThreshold_ = Math.Pow(splitDepreciationFactor_, 2) * 4;
        private static readonly double splitSixThreshold_ = Math.Pow(splitDepreciationFactor_, 3) * 6;
        private static readonly double splitNineThreshold_ = Math.Pow(splitDepreciationFactor_, 4) * 9;

        private int maxBVHDataSize_;
        private IEnumerable<Primitive> primitives_;
    }
}
