﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;
using RSG.Bounds;

namespace RSG.Pipeline.BoundsProcessor.OrphanedData
{
    /// <summary>
    /// A self contained triangle primitive
    /// </summary>
    internal class Triangle : Primitive
    {
        internal Triangle(BNDFile.CollisionType collisionType,
                          string material,
                          Vector4i materialColour,
                          float[] secondSurfaceDisplacements,
                          Vector3f[] shrunkVerts,
                          VertexData vert0,
                          VertexData vert1,
                          VertexData vert2,
                          uint numPerVertAttribs)
            : base(collisionType, material, materialColour, secondSurfaceDisplacements, shrunkVerts, numPerVertAttribs)
        {
            Vert0 = vert0;
            Vert1 = vert1;
            Vert2 = vert2;

            float centreX = (vert0.Position.X + vert1.Position.X + vert2.Position.X) / 3.0f;
            float centreY = (vert0.Position.Y + vert1.Position.Y + vert2.Position.Y) / 3.0f;
            CentreXY = new Vector2f(centreX, centreY);
            MinX = Math.Min(vert0.Position.X, Math.Min(vert1.Position.X, vert2.Position.X));
            MaxX = Math.Max(vert0.Position.X, Math.Max(vert1.Position.X, vert2.Position.X));
            MinY = Math.Min(vert0.Position.Y, Math.Min(vert1.Position.Y, vert2.Position.Y));
            MaxY = Math.Max(vert0.Position.Y, Math.Max(vert1.Position.Y, vert2.Position.Y));
        }

        internal VertexData Vert0 { get; private set; }
        internal VertexData Vert1 { get; private set; }
        internal VertexData Vert2 { get; private set; }

        // We lazy evaluate normals to avoid soring data we don't need (the vast majority will never be checked)
        internal Vector3f Normal
        {
            get
            {
                if (normal_ == null)
                {
                    normal_ = Vector3f.Cross(Vert1.Position - Vert0.Position, Vert2.Position - Vert0.Position);
                    normal_.Normalise();
                }

                return normal_;
            }
        }

        internal override int EstimatedSize
        {
            get { return 27; }// one primitive plus 11/6 verts
        }

        internal bool IsWithinAxiallyAlignedArea(Primitive.CardinalAxis axis, float min, float max)
        {
            if (axis == Primitive.CardinalAxis.X)
            {
                return CentreXY.Y >= min && CentreXY.Y <= max;
            }
            else if (axis == Primitive.CardinalAxis.Y)
            {
                return CentreXY.X >= min && CentreXY.X <= max;
            }

            return false;
        }

        internal bool IsIntersectedByAxiallyAlignedRay(Primitive.CardinalAxis axis, float pos)
        {
            if (axis == Primitive.CardinalAxis.X)
            {
                return (pos >= MinY && pos <= MaxY);
            }
            else if (axis == Primitive.CardinalAxis.Y)
            {
                return (pos >= MinX && pos <= MaxX);
            }

            return false;// We don't cache minZ and maxZ atm
        }

        internal bool IsAboveAxiallyAlignedRay(Primitive.CardinalAxis axis, float pos)
        {
            if (axis == Primitive.CardinalAxis.X)
            {
                return (CentreXY.Y > pos);
            }
            else if (axis == Primitive.CardinalAxis.Y)
            {
                return (CentreXY.X > pos);
            }

            return false;// We don't cache centreZ atm
        }

        internal IEnumerable<TriangleEdge> GetEdges()
        {
            if (edges_ == null)
            {
                edges_ = new TriangleEdge[3]
                {
                    new TriangleEdge(Vert0.Position, Vert1.Position, this),
                    new TriangleEdge(Vert0.Position, Vert2.Position, this),
                    new TriangleEdge(Vert1.Position, Vert2.Position, this)
                };
            }

            return edges_;
        }

        private TriangleEdge[] edges_;
        private Vector3f normal_;
    }
}
