﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.BoundsProcessor.BVHBuilder
{
    internal class ColourQuantiserOctreeNode
    {
        internal ColourQuantiserOctreeNode()
        {
            Children = new ColourQuantiserOctreeNode[8];
            PaletteIndex = -1;
        }

        internal ColourRGB Colour { get; set; }
        internal bool IsLeaf { get; set; }
        internal int Level { get; set; }
        internal ulong PixelCount { get; set; }
        internal int PaletteIndex { get; set; }
        internal ColourQuantiserOctreeNode[] Children { get; set; }
        internal ColourQuantiserOctreeNode NextNode { get; set; }
    }
}
