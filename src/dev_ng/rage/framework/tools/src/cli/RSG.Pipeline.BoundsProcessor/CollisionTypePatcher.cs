﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Bounds;

namespace RSG.Pipeline.BoundsProcessor
{
    internal static class CollisionTypePatcher
    {
        // B* 972693 - Map invalid bounds type flags to valid bounds type flags
        internal static BNDFile.CollisionType PatchCollisionType(BNDFile.CollisionType collisionType)
        {
            if (collisionType.HasFlag(BNDFile.CollisionType.Mover))
            {
                collisionType |= (BNDFile.CollisionType.Horse | BNDFile.CollisionType.Cover);
                collisionType &= ~(BNDFile.CollisionType.Foliage | BNDFile.CollisionType.River | 
                                   BNDFile.CollisionType.StairSlope | BNDFile.CollisionType.DeepMaterialSurface);
            }
            else if (collisionType.HasFlag(BNDFile.CollisionType.Weapons) && !(collisionType.HasFlag(BNDFile.CollisionType.Mover)))
            {
                collisionType &= ~(BNDFile.CollisionType.Horse | BNDFile.CollisionType.Vehicle | BNDFile.CollisionType.Cover | 
                                   BNDFile.CollisionType.Foliage | BNDFile.CollisionType.River | BNDFile.CollisionType.StairSlope);
            }
            else if (collisionType.HasFlag(BNDFile.CollisionType.Foliage))
            {
                collisionType = BNDFile.CollisionType.Foliage;
            }
            else if (collisionType.HasFlag(BNDFile.CollisionType.River))
            {
                collisionType = BNDFile.CollisionType.River;
            }

            return collisionType;
        }
    }
}
