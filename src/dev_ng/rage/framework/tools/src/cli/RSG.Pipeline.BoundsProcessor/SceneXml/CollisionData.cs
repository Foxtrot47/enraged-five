﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.BoundsProcessor.SceneXml
{
    /// <summary>
    /// Represents all the data required from a SceneXml.Scene during bounds processing
    /// Data is divorced from SceneXml.Scene in this way to improve code clarity
    /// </summary>
    internal class CollisionData
    {
        internal CollisionData(IEnumerable<CollisionObject> collisionObjects, 
                               IEnumerable<RefObject> rsRefObjectsToBake,
                               IEnumerable<MiloObject> miloObjects)
        {
            collisionObjects_ = collisionObjects.ToArray();
            rsRefObjectsToBake_ = rsRefObjectsToBake.ToArray();
            miloObjects_ = miloObjects.ToArray();
        }

        internal IEnumerable<CollisionObject> CollisionObjects { get { return collisionObjects_; } }
        internal IEnumerable<RefObject> RSRefObjectsToBake { get { return rsRefObjectsToBake_; } }
        internal IEnumerable<MiloObject> MiloObjects { get { return miloObjects_; } }

        private CollisionObject[] collisionObjects_;
        private RefObject[] rsRefObjectsToBake_;
        private MiloObject[] miloObjects_;
    }
}
