﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Logging;
using RSG.Base.Profiling;
using RSG.Base.Math;
using RSG.Bounds;

namespace RSG.Pipeline.BoundsProcessor.OrphanedData
{
    internal static class BestSplitFinder
    {
        internal static void FindBestSplit(IEnumerable<Primitive> primitives, ref float splitPosition, Primitive.CardinalAxis splitAxis, float sectionSize)
        {
            List<CandidateSplit> candidateSplits = new List<CandidateSplit>();

            // this should become config data
            float fractionalDivergenceToSearch = Config.TheConfig.Options.SplitDivergenceToConsider * 1.66f;// We do this to make sure that we have enough data for the outer candidates
            float divergenceToConsider = sectionSize * Config.TheConfig.Options.SplitDivergenceToConsider;

            float minSearchPosition = splitPosition - (sectionSize * fractionalDivergenceToSearch * 0.5f);
            float maxSearchPosition = splitPosition + (sectionSize * fractionalDivergenceToSearch * 0.5f);
            float minSplitPosition = splitPosition - (sectionSize * Config.TheConfig.Options.SplitDivergenceToConsider * 0.5f);
            float maxSplitPosition = splitPosition + (sectionSize * Config.TheConfig.Options.SplitDivergenceToConsider * 0.5f);
            float splitPositionIncrement = sectionSize * Config.TheConfig.Options.SplitDivergenceToConsider / (float)Config.TheConfig.Options.SplitAlternativesToConsider;
            for (int candidateIndex = 0; candidateIndex < Config.TheConfig.Options.SplitAlternativesToConsider + 1; ++candidateIndex)
            {
                float position = minSplitPosition + (splitPositionIncrement * candidateIndex);
                candidateSplits.Add(new CandidateSplit(position, Math.Abs(splitPosition - position)));
            }

            // find a subset of the mover triangles in the area being considered for the split
            Triangle[] moverTrianglesInArea =
                primitives.AsParallel().OfType<Triangle>().Where(tri => tri.CollisionType.HasFlag(BNDFile.CollisionType.Mover) &&
                                                                  tri.IsWithinAxiallyAlignedArea(splitAxis, minSearchPosition, maxSearchPosition)).ToArray();

            // Build a collection of edges for the mover triangles here
            TriangleEdge[] sharedEdges = TriangleEdge.GetSharedEdgesFromTriangles(moverTrianglesInArea).ToArray();

            foreach (CandidateSplit candidateSplit in candidateSplits)
            {
                // for each edge, check if this candidate line will split it (one triangle is on each half of the line)
                foreach (TriangleEdge sharedEdge in sharedEdges.AsParallel().Where(edge => edge.IsDividedByRay(splitAxis, candidateSplit.Position)))
                {
                    candidateSplit.AddEdge(sharedEdge.Triangle0.Normal, sharedEdge.Triangle1.Normal);
                }
            }

            CandidateSplit bestSplit = candidateSplits.OrderByDescending(candidate => candidate.Score).First();
            splitPosition = bestSplit.Position;
        }

        #region Private types
        private class CandidateSplit
        {
            internal CandidateSplit(float position, float distanceFromIdealLine)
            {
                Position = position;

                distanceFromIdealLine_ = distanceFromIdealLine;
                combinedPrimitiveBadness_ = 0;
            }

            public override string ToString()
            {
                return string.Format("Pos:{0} ; Score:{1}", Position.ToString("0.00"), Score.ToString("0.00"));
            }

            internal void AddEdge(Vector3f primitiveNormalA, Vector3f primitiveNormalB)
            {
                float edgePrimitivesDotProduct = Vector3f.Dot(primitiveNormalA, primitiveNormalB);
                edgePrimitivesDotProduct = Math.Max(0, edgePrimitivesDotProduct);// we limit the DP to 0
                float primitiveBadness = 1 - edgePrimitivesDotProduct;// As agreed with Eugene
                combinedPrimitiveBadness_ += primitiveBadness;
            }

            internal float Position { get; private set; }
            internal float DistanceFromIdealLine { get; private set; }

            internal double Score
            {
                get
                {
                    return 0 - DistanceFromIdealLine - combinedPrimitiveBadness_;
                }
            }

            private double distanceFromIdealLine_;
            private double combinedPrimitiveBadness_;
        };
        #endregion
    }
}
