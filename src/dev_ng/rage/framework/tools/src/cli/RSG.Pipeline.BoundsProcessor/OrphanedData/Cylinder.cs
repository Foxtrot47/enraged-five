﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;
using RSG.Bounds;

namespace RSG.Pipeline.BoundsProcessor.OrphanedData
{
    /// <summary>
    /// A self contained cylinder primitive
    /// </summary>
    internal class Cylinder : Primitive
    {
        internal Cylinder(BNDFile.CollisionType collisionType,
                         string material,
                         Vector4i materialColour,
                         float[] secondSurfaceDisplacements,
                         Vector3f[] shrunkVerts,
                         VertexData vert0,
                         VertexData vert1,
                         float radius,
                         uint numPerVertAttribs)
            : base(collisionType, material, materialColour, secondSurfaceDisplacements, shrunkVerts, numPerVertAttribs)
        {
            Vert0 = vert0;
            Vert1 = vert1;
            Radius = radius;

            float centreX = (vert0.Position.X + vert1.Position.X) * 0.5f;
            float centreY = (vert0.Position.Y + vert1.Position.Y) * 0.5f;
            CentreXY = new Vector2f(centreX, centreY);
            MinX = Math.Min(vert0.Position.X, vert1.Position.X);
            MaxX = Math.Max(vert0.Position.X, vert1.Position.X);
            MinY = Math.Min(vert0.Position.Y, vert1.Position.Y);
            MaxY = Math.Max(vert0.Position.Y, vert1.Position.Y);
        }

        internal VertexData Vert0 { get; private set; }
        internal VertexData Vert1 { get; private set; }
        internal float Radius { get; private set; }

        internal override int EstimatedSize
        {
            get { return 28; }// one primitive plus two verts
        }
    }
}
