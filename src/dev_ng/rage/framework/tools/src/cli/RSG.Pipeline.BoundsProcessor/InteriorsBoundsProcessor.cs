﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.Bounds;
using RSG.Pipeline.BoundsProcessor.SceneXml;
using RSG.SceneXml.MapExport;

namespace RSG.Pipeline.BoundsProcessor
{
    /// <summary>
    /// Represents the indexed bounds data for an interior scene (which may contain more than one interior)
    /// </summary>
    internal class InteriorsBoundsProcessor
    {
        internal enum Context
        {
            Normal,
            InSitu
        };

        internal InteriorsBoundsProcessor(IProject project, Context context)
        {
            context_ = context;
            project_ = project;
            interiorStaticBoundsMap_ = new Dictionary<String, IndexedBounds>();
            interiorStaticWeaponBoundsMap_ = new Dictionary<String, IndexedBounds>();
            interiorManifestBoundsMap_ = new Dictionary<String, List<String>>();
            interiorBoundsDictionaryMap_ = new Dictionary<String, IndexedBoundsDictionary>();
            worldToParentTransformMap_ = new Dictionary<String, Matrix34f>();
        }

        internal void LoadSceneBounds(IEnumerable<SceneXml.CollisionObject> collisionObjects, string[] boundsZipPathnames)
        {
            for (int boundsZipIndex = 0; boundsZipIndex < boundsZipPathnames.Length; ++boundsZipIndex)
            {
                string boundsZipPathname = boundsZipPathnames[boundsZipIndex];

                if (String.IsNullOrEmpty(boundsZipPathname))
                {
                    Log.Log__Message("There were no bounds files to load");
                    continue;
                }

                Dictionary<string, BoundObject> boundsMap = BoundsLoader.LoadAndIndexRawBoundsData(boundsZipPathname);

                // build the paired collection
                foreach (var boundsPair in boundsMap)
                {
                    SceneXml.CollisionObject collisionObject = collisionObjects.FirstOrDefault(co => 
                        String.Equals(co.Name, boundsPair.Key, StringComparison.CurrentCultureIgnoreCase));

                    if (collisionObject == null)
                    {
                        Log.Log__Warning("Unable to look up scene xml entry for node '{0}'.  " +
                                         "This means the exported data and scene xml are out of sync.  Collision data exported from max will not appear in game.",
                                         boundsPair.Key);
                        continue;
                    }

                    if (String.IsNullOrEmpty(collisionObject.MILOParentName))
                    {
                        // This warning doesn't need to be output for in-situ interiors
                        if (context_ == Context.Normal)
                        {
                            Log.Log__Warning("Unable to look up MILO for node '{0}'.  " +
                                         "This means this collision object doesn't have a MILO above it in the object hierarchy.",
                                         boundsPair.Key);
                        }
                        continue;
                    }

                    // DLC/Core detection for Static collision serialisation
                    // We want to serialise out in 3 cases:
                    // Core project (not DLC) and object is NOT new DLC
                    // DLC project and object is New DLC
                    // AllNewContentFlag (Liberty))
                    bool serialiseOut = (collisionObject.IsNewDLC == project_.IsDLC) || project_.ForceFlags.AllNewContent
                        // HACK: PatchDay3 specific. We bypass this to force the collision of the interior to go through
                        || (project_.Name.Equals("patchday3NG", StringComparison.OrdinalIgnoreCase) && collisionObject.SceneFilename.EndsWith("v_int_44.xml"));
                    if (!serialiseOut)
                        continue;

                    string dlcPrefix = collisionObject.IsNewDLC ? project_.AssetPrefix.MapPrefix : "";
                    // if we're not in a DLC, the prefix is empty, so the prefixedMILOParentName is the actual MILOParentName
                    string prefixedMILOParentName = dlcPrefix + collisionObject.MILOParentName;

                    // Determine the entry name
                    string entryName = prefixedMILOParentName;
                    IndexedBounds.SerialisationOptions serialisationOptions = IndexedBounds.SerialisationOptions.AsBVH;
                    if (collisionObject.ParentIsDynamic || collisionObject.ParentIsInAnEntitySet)
                    {
                        string prefixedParentName = dlcPrefix + collisionObject.ParentName;

                        entryName = prefixedParentName;
                        if (!worldToParentTransformMap_.ContainsKey(prefixedParentName))
                            worldToParentTransformMap_.Add(prefixedParentName, collisionObject.WorldToParentTransformMatrix);

                        if (collisionObject.ParentIsDynamic)
                            serialisationOptions = IndexedBounds.SerialisationOptions.AsDiscretePrimitives;
                    }

                    String boundsName = prefixedMILOParentName;

                    // Setup the target static bounds entry (if necessary)
                    bool addToStaticBounds = (entryName == boundsName);
                    BNDFile.CollisionType collisionType = CollisionTypePatcher.PatchCollisionType(collisionObject.CollisionType);
                    Dictionary<String, IndexedBounds> targetBoundsMap = interiorStaticBoundsMap_;


                    if (addToStaticBounds)
                    {
                        if (!interiorManifestBoundsMap_.ContainsKey(prefixedMILOParentName))
                            interiorManifestBoundsMap_.Add(prefixedMILOParentName, new List<string>());

                        if (collisionType == BNDFile.CollisionType.Weapons)
                        {
                            boundsName = "hi@" + boundsName;
                            targetBoundsMap = interiorStaticWeaponBoundsMap_;
                        }

                        if (collisionType == BNDFile.CollisionType.MaterialOnly)
                        {
                            Log.StaticLog.ErrorCtx("InteriorsBoundsProcessor", "Bound File with MaterialOnly collision flag found in {0} interior. This is not supported.", boundsName);
                        }

                        if (!targetBoundsMap.ContainsKey(boundsName))
                        {
                            targetBoundsMap.Add(boundsName, new IndexedBounds(IndexedBounds.SerialisationOptions.AsBVH));
                            interiorManifestBoundsMap_[prefixedMILOParentName].Add(boundsName);
                        }
                    }

                    if (!interiorBoundsDictionaryMap_.ContainsKey(prefixedMILOParentName))
                        interiorBoundsDictionaryMap_.Add(prefixedMILOParentName, new IndexedBoundsDictionary());

                    IEnumerable<Tuple<BNDFile.CollisionType, BVH>> bvhsToAdd =
                        BoundsLoader.ConvertBoundObjectToBVHPairCollection(boundsPair.Key, boundsPair.Value, collisionType);

                    foreach (Tuple<BNDFile.CollisionType, BVH> typeBVHPair in bvhsToAdd)
                    {
                        interiorBoundsDictionaryMap_[prefixedMILOParentName].AddBVH(
                            entryName,
                            typeBVHPair.Item1,
                            typeBVHPair.Item2,
                            serialisationOptions);

                        if (addToStaticBounds)
                            targetBoundsMap[boundsName].AddBVH(typeBVHPair.Item1, typeBVHPair.Item2);
                    }
                }

                Log.Log__Message("Loaded {0} bounds files from '{1}'.", boundsMap.Count, boundsZipPathname);
            }
        }

        internal void LoadRefBounds(IEnumerable<SceneXml.RefObject> rsRefObjectsToBake)
        {
            Dictionary<SceneXml.RefObject, Dictionary<BNDFile.CollisionType, List<BVH>>> indexedBoundsMap = BoundsLoader.LoadAndIndexRefBoundsData(rsRefObjectsToBake);

            // Now we process the indexed data (prop data that couldn't be looked up will be thrown away
            foreach (KeyValuePair<SceneXml.RefObject, Dictionary<BNDFile.CollisionType, List<BVH>>> indexedPair in indexedBoundsMap)
            {
                SceneXml.RefObject refObject = indexedPair.Key;
                if (refObject.MiloParentName == null)
                    continue;

                // DLC/Core detection for Static collision serialisation
                // We want to serialise out in 3 cases:
                // Core project (not DLC) and object is NOT new DLC
                // DLC project and object is New DLC
                // AllNewContentFlag (Liberty))
                bool serialiseOut = (refObject.IsNewDLC == project_.IsDLC) || project_.ForceFlags.AllNewContent
                    // HACK: PatchDay3 specific. We bypass this to force the collision of the interior to go through
                    || (project_.Name.Equals("patchday3NG", StringComparison.OrdinalIgnoreCase) && refObject.CollisionGroupName.StartsWith("v_res_michael", StringComparison.OrdinalIgnoreCase));
                
                if (!serialiseOut)
                    continue;

                string dlcPrefix = refObject.IsNewDLC ? project_.AssetPrefix.MapPrefix : "";
                string prefixedRefMILOParent = dlcPrefix + refObject.MiloParentName;

                Dictionary<BNDFile.CollisionType, List<BVH>> collisionTypeBoundsMap = indexedPair.Value;
                foreach (KeyValuePair<BNDFile.CollisionType, List<BVH>> collisionTypeBoundsPair in collisionTypeBoundsMap)
                {
                    String boundsName = prefixedRefMILOParent;
                    BNDFile.CollisionType collisionType = CollisionTypePatcher.PatchCollisionType(collisionTypeBoundsPair.Key);
                    Dictionary<String, IndexedBounds> targetBoundsMap = interiorStaticBoundsMap_;

                    if (collisionType == BNDFile.CollisionType.Weapons)
                    {

                        boundsName = "hi@" + boundsName;
                        targetBoundsMap = interiorStaticWeaponBoundsMap_;
                    }
                    
                    if (collisionType == BNDFile.CollisionType.MaterialOnly)
                    {
                        Log.StaticLog.ErrorCtx("InteriorsBoundsProcessor", "Bound File with MaterialOnly collision flag found in {0} interior. This is not supported.", boundsName);
                    }

                    if (!targetBoundsMap.ContainsKey(boundsName))
                    {
                        targetBoundsMap.Add(boundsName, new IndexedBounds(IndexedBounds.SerialisationOptions.AsBVH));
                        interiorManifestBoundsMap_[prefixedRefMILOParent].Add(boundsName);
                    }

                    foreach (BVH bvh in collisionTypeBoundsPair.Value)
                    {
                        bvh.ScaleContents(refObject.Scale);// Scale the verts while they're still in local space
                        bvh.TransformVerts(refObject.LocalToWorldTransform);// We need to transform this BVH into world space

                        interiorBoundsDictionaryMap_[prefixedRefMILOParent].AddBVH(
                            prefixedRefMILOParent, 
                            collisionType, 
                            bvh, 
                            IndexedBounds.SerialisationOptions.AsBVH);

                        targetBoundsMap[boundsName].AddBVH(collisionType, bvh);
                    }
                }
            }

            Log.Log__Message("Baking {0} prop instances into the bounds.", indexedBoundsMap.Count);
        }
        
        internal void ProcessAndSerialise(string outputDirectory, string inputDirectory, IEnumerable<MiloObject> miloObjects)
        {
            // Transform from world space to milo (or parent) space
            foreach (KeyValuePair<string, IndexedBoundsDictionary> interiorBoundsPair in interiorBoundsDictionaryMap_)
            {
                string miloName = interiorBoundsPair.Key;
                MiloObject miloObject = miloObjects.FirstOrDefault(miloObj =>
                {
                    string prefix = miloObj.IsNewDLC ? project_.AssetPrefix.MapPrefix : "";
                    return prefix + miloObj.Name == miloName;
                });
                Matrix34f worldToMiloTransform = miloObject.WorldToMiloTransformMatrix;

                foreach (string entryName in interiorBoundsPair.Value.Keys)
                {
                    if (worldToParentTransformMap_.ContainsKey(entryName))
                    {
                        Matrix34f worldToParentTransform = worldToParentTransformMap_[entryName];
                        interiorBoundsPair.Value[entryName].Transform(worldToParentTransform);
                    }
                    else
                    {
                        interiorBoundsPair.Value[entryName].Transform(worldToMiloTransform);
                    }
                }
            }

            string secondaryBoundsDirectory = null;
            if (!string.IsNullOrEmpty(inputDirectory) && inputDirectory != "null")
            {
                secondaryBoundsDirectory = Path.Combine(inputDirectory, "transformed_drawables");

                // Prepare the secondary directory
                if (!Directory.Exists(secondaryBoundsDirectory))
                {
                    Log.Log__Error("Missing secondary bounds directory {0}", secondaryBoundsDirectory);
                }
            }

            Log.Log__Message("Processing bounds for {0} Interiors", interiorBoundsDictionaryMap_.Count);
            foreach (KeyValuePair<string, IndexedBoundsDictionary> interiorBoundsPair in interiorBoundsDictionaryMap_)
            {
                Log.Log__Message("Processing bounds for interior '{0}'", interiorBoundsPair.Key);

                string boundsDictionaryDirectory = Path.Combine(outputDirectory, interiorBoundsPair.Key);
                Directory.CreateDirectory(boundsDictionaryDirectory);

                interiorBoundsPair.Value.ProcessAndSerialise(project_, boundsDictionaryDirectory, secondaryBoundsDirectory);
            }

            foreach (KeyValuePair<string, IndexedBounds> interiorBoundsPair in interiorStaticBoundsMap_)
            {
                BoundObject processedBoundObject = interiorBoundsPair.Value.Process();
                String outputPathname = Path.Combine(outputDirectory, interiorBoundsPair.Key + ".bnd");
                processedBoundObject.Save(outputPathname);
            }

            foreach (KeyValuePair<string, IndexedBounds> interiorBoundsPair in interiorStaticWeaponBoundsMap_)
            {
                BoundObject processedBoundObject = interiorBoundsPair.Value.Process();
                String outputPathname = Path.Combine(outputDirectory, interiorBoundsPair.Key + ".bnd");
                processedBoundObject.Save(outputPathname);
            }
        }

        // outputDirectory is not used in any path in the method
        internal void PackDrawableBounds(string outputDirectory, string inputDirectory, string drawableListString)
        {
            // Any container that doesn't have transformed drawables will come through with "null" from the XML file.
            if (inputDirectory == "null" || drawableListString == "null")
                return;

            IList<string> drawableList = drawableListString.Split(new char[]{';'}, StringSplitOptions.RemoveEmptyEntries);
            string boundsDirectory = Path.Combine(inputDirectory, "transformed_drawables");

            if (!Directory.Exists(boundsDirectory))
                Directory.CreateDirectory(boundsDirectory);

            // Only process drawables we have in our list.
            if (drawableList.Count > 0)
            {
                foreach (string drawableFilename in drawableList)
                {
                    string drawableName = null;
                    if (drawableFilename.EndsWith(".idr.zip"))
                        drawableName = drawableFilename.Replace(".idr.zip", "");

                    string drawablePath = Path.Combine(inputDirectory, drawableFilename);
                    string transformedDrawablePath = Path.Combine(boundsDirectory, drawableFilename);
                    drawableName = MapAsset.AppendMapPrefixIfRequired(project_, drawableName);
                    string boundPath = Path.Combine(boundsDirectory, string.Format("{0}.bnd", drawableName));

                    if (File.Exists(drawablePath))
                    {
                        if (!File.Exists(boundPath))
                        {
                            Log.Log__Message("Drawable doesn't have a bound to pack '{0}'", drawablePath);

                            // Even If we don't have a bound to pack, still copy over the unmodified drawable to satisfy dependencies.
                            File.Copy(drawablePath, transformedDrawablePath, true);

                            continue;
                        }

                        // Do the actual bound packing
                        PackDrawableBound(drawablePath, transformedDrawablePath, boundPath);
                    }
                    else
                    {
                        Log.Log__Warning("Couldn't find drawable to pack bounds '{0}' in path '{1}'", drawableFilename, drawablePath);
                    }
                }

                DirectoryInfo boundsDirectoryInfo = new DirectoryInfo(boundsDirectory);
                foreach (FileInfo fileinfo in boundsDirectoryInfo.GetFiles("*.bnd"))
                {
                    if(fileinfo.Exists)
                        fileinfo.Delete();
                }
            }
        }

        internal void PackDrawableBound(string drawablePath, string transformedDrawablePath, string boundPath)
        {
            Log.Log__Message("Packing bounds for '{0}' with bound file {1}", transformedDrawablePath, boundPath);

            using (Ionic.Zip.ZipFile exportedDataZipFile = Ionic.Zip.ZipFile.Read(drawablePath))
            {
                try
                {
                    exportedDataZipFile.AddFile(boundPath, "");
                    exportedDataZipFile.Save(transformedDrawablePath);
                }
                catch (Exception e)
                {
                    Log.Log__Message("Error packing bounds for '{0}' with {1}\nException: {2}", drawablePath, boundPath, e.Message);
                }
            }
        }

        internal void GetManifestData(Manifest.ManifestDataBuilder manifestDataBuilder)
        {
            foreach (KeyValuePair<String, List<String>> interiorManifestBoundsPair in interiorManifestBoundsMap_)
                manifestDataBuilder.Add(new Manifest.InteriorBoundsMapping(interiorManifestBoundsPair.Key, interiorManifestBoundsPair.Value));
        }

        private Context context_;
        private IProject project_;
        private Dictionary<String, IndexedBounds> interiorStaticBoundsMap_;// map of milo name -> standard bounds
        private Dictionary<String, IndexedBounds> interiorStaticWeaponBoundsMap_;// map of milo name -> weapon bounds
        private Dictionary<String, List<String>> interiorManifestBoundsMap_;// map of milo name -> bounds array
        private Dictionary<String, IndexedBoundsDictionary> interiorBoundsDictionaryMap_;
        private Dictionary<String, Matrix34f> worldToParentTransformMap_;
    }
}
