﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;

namespace RSG.Pipeline.BoundsProcessor.OrphanedData
{
    internal class TriangleEdge
    {
        private class TriangleEdgeEqualityComparer : IEqualityComparer<TriangleEdge>
        {
            #region IEqualityComparer<TriangleEdge> Members

            public bool Equals(TriangleEdge lhs, TriangleEdge rhs)
            {
                return (lhs.V0.X == rhs.V0.X &&
                        lhs.V0.Y == rhs.V0.Y &&
                        lhs.V0.Z == rhs.V0.Z &&
                        lhs.V1.X == rhs.V1.X &&
                        lhs.V1.Y == rhs.V1.Y &&
                        lhs.V1.Z == rhs.V1.Z);
            }

            public int GetHashCode(TriangleEdge obj)
            {
                return obj.V0.X.GetHashCode() ^ obj.V0.Y.GetHashCode() ^ obj.V0.Z.GetHashCode() ^ obj.V1.X.GetHashCode() ^ obj.V1.Y.GetHashCode() ^ obj.V1.Z.GetHashCode();
            }

            #endregion
        }

        static TriangleEdge()
        {
            EqualityComparer = new TriangleEdgeEqualityComparer();
        }

        internal TriangleEdge(Vector3f v0, Vector3f v1, Triangle triangle0)
        {
            // We need to make sure that verts are stored in some order, distance from origin is as good as anything - this simplifies comparison
            if (v0.Dist2(Vector3f.Zero) < v1.Dist2(Vector3f.Zero))
            {
                V0 = v0;
                V1 = v1;
            }
            else
            {
                V0 = v1;
                V1 = v0;
            }

            Triangle0 = triangle0;
        }

        internal bool IsDividedByRay(Primitive.CardinalAxis axis, float position)
        {
            if (Triangle0 != null && Triangle1 != null)
            {
                bool triangle0IsAbove = Triangle0.IsAboveAxiallyAlignedRay(axis, position);
                bool triangle1IsAbove = Triangle1.IsAboveAxiallyAlignedRay(axis, position);
                return (( triangle0IsAbove && !triangle1IsAbove) ||
                        (!triangle0IsAbove &&  triangle1IsAbove));
            }

            return false;
        }

        internal static IEnumerable<TriangleEdge> GetSharedEdgesFromTriangles(IEnumerable<Triangle> triangles)
        {
            List<TriangleEdge> edgeCache = new List<TriangleEdge>();

            foreach (Triangle triangle in triangles)
            {
                foreach (TriangleEdge edge in triangle.GetEdges())
                {
                    TriangleEdge edgeFound = edgeCache.AsParallel().FirstOrDefault(cachedEdge => TriangleEdge.EqualityComparer.Equals(cachedEdge, edge));
                    if (edgeFound != null)
                    {
                        edgeFound.Triangle1 = triangle;
                    }
                    else
                    {
                        edgeCache.Add(edge);
                    }
                }
            }

            return edgeCache.Where(edge => edge.Triangle0 != null && edge.Triangle1 != null).ToArray();
        }

        internal Vector3f V0 { get; private set; }
        internal Vector3f V1 { get; private set; }
        internal Triangle Triangle0 { get; private set; }
        internal Triangle Triangle1 { get; private set; }

        internal static IEqualityComparer<TriangleEdge> EqualityComparer { get; private set; }
    }
}
