﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;
using RSG.Bounds;

namespace RSG.Pipeline.BoundsProcessor.SceneXml
{
    /// <summary>
    /// Represents all the data required during processing for a ref object
    /// Data is divorced from SceneXml.ObjectDef in this way to improve code clarity
    /// </summary>
    class RefObject
    {
        internal RefObject(
            string name, 
            string refName, 
            bool isDynamic,
            string exportZipPathname,
            string processedZipPathname,
            Vector3f scale,
            Matrix34f localToWorldTransform,
            bool useFullMatrix,
            string collisionGroupName,
            string iplGroupName,
            string miloParentName,
            bool isNewDlc )
        {
            Name = name;
            RefName = refName;
            ExportZipPathname = exportZipPathname;
            ProcessedZipPathname = processedZipPathname;
            Scale = scale;
            LocalToWorldTransform = localToWorldTransform.Transpose();// Hack to fix scene XML bug
            CollisionGroupName = collisionGroupName;
            IPLGroupName = iplGroupName;
            MiloParentName = miloParentName;
            IsNewDLC = isNewDlc;

            // This has been reproduced from fwEntity::InitTransformFromDefinition().  Don't change this without liasing with whoever owns that code - JWR
            // The wrong-handed localToWorldTransform is used deliberately here so that the code below matches the runtime code - JWR
            Quaternionf orientation = new Quaternionf(localToWorldTransform);
            if (isDynamic ||
                Math.Abs(orientation.X) > 0.05f ||
                Math.Abs(orientation.Y) > 0.05f ||
                (useFullMatrix && (orientation.X != 0.0f || orientation.Y != 0.0f)))
            {
                RuntimeLocalToWorldTransform = LocalToWorldTransform;
            }
            else
            {
                float acos = (float)Math.Acos(orientation.W);
                float heading = orientation.Z < 0.0f ? 2.0f * acos : -2.0f * acos;
                float sinHeading = (float)Math.Sin(heading);
                float cosHeading = (float)Math.Cos(heading);
                RuntimeLocalToWorldTransform = new Matrix34f(
                    new Vector3f(cosHeading, sinHeading, 0.0f),
                    new Vector3f(-sinHeading, cosHeading, 0.0f),
                    new Vector3f(0.0f, 0.0f, 1.0f),
                    LocalToWorldTransform.D);
            }
        }

        internal string Name { get; private set; }
        internal string RefName { get; private set; }
        internal string ExportZipPathname { get; private set; }
        internal string ProcessedZipPathname { get; private set; }
        internal Vector3f Scale { get; private set; }
        internal Matrix34f LocalToWorldTransform { get; private set; }// Accurate transform to be used in intermediate transformations
        internal Matrix34f RuntimeLocalToWorldTransform { get; private set; }// Optimised runtime transform to be used when mirroring runtime behaviour
        internal string CollisionGroupName { get; private set; }
        internal string IPLGroupName { get; private set; }
        internal string MiloParentName { get; private set; }
        internal bool IsNewDLC { get; private set; }
    }
}
