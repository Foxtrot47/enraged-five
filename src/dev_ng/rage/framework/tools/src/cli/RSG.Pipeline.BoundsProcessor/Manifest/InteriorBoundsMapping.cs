﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.BoundsProcessor.Manifest
{
    internal class InteriorBoundsMapping : IManifestItem
    {
        internal InteriorBoundsMapping(String name, IEnumerable<String> boundsNames)
        {
            name_ = name;
            boundsNames_ = boundsNames.ToArray();
        }

        #region IManifestItem Members

        public XElement ToXElement()
        {
            return new XElement("Interior", new XAttribute("name", name_),
                boundsNames_.Select(boundsName => new XElement("Bounds", new XAttribute("name", boundsName))));
        }

        #endregion

        private readonly String name_;
        private readonly String[] boundsNames_;
    }
}
