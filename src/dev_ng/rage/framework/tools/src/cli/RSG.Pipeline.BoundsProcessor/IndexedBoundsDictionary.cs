﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using RSG.Base.Configuration;
using RSG.Base.Math;
using RSG.Bounds;
using RSG.SceneXml.MapExport;

namespace RSG.Pipeline.BoundsProcessor
{
    internal class IndexedBoundsDictionary
    {
        internal IndexedBoundsDictionary()
        {
            boundsMap_ = new Dictionary<string, IndexedBounds>();
        }

        internal IEnumerable<string> Keys
        {
            get
            {
                return boundsMap_.Keys;
            }
        }

        internal IEnumerable<IndexedBounds> Values
        {
            get
            {
                return boundsMap_.Values;
            }
        }

        internal IndexedBounds this[string entryName]
        {
            get
            {
                return boundsMap_[entryName];
            }
        }

        internal void AddBVH(string entryName, BNDFile.CollisionType collisionType, BVH bvh, IndexedBounds.SerialisationOptions serialisationOptions)
        {
            if (!boundsMap_.ContainsKey(entryName))
            {
                boundsMap_.Add(entryName, new IndexedBounds(serialisationOptions));
            }

            boundsMap_[entryName].AddBVH(collisionType, bvh);
        }

        internal void ProcessAndSerialise(IProject project, string outputDirectory, string secondaryPath = null)
        {
            foreach (KeyValuePair<string, IndexedBounds> boundsPair in boundsMap_)
            {
                BoundObject boundObject = boundsPair.Value.Process();

                string pathname = Path.Combine(outputDirectory, string.Format("{0}.bnd", boundsPair.Key));
                boundObject.Save(pathname);

                // Save out our secondary path after the object has been processed.
                if (!string.IsNullOrEmpty(secondaryPath))
                {
                    String boundFilename = MapAsset.AppendMapPrefixIfRequired(project, boundsPair.Key);
                    pathname = Path.Combine(secondaryPath, string.Format("{0}.bnd", boundFilename));
                    boundObject.Save(pathname);
                }
            }
        }

        internal void Transform(Matrix34f transformationMatrix)
        {
            foreach (KeyValuePair<string, IndexedBounds> boundsPair in boundsMap_)
            {
                boundsPair.Value.Transform(transformationMatrix);
            }
        }

        private Dictionary<string, IndexedBounds> boundsMap_;
    }
}
