﻿

namespace RSG.Pipeline.BoundsProcessor
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using RSG.Base.Configuration;
    using RSG.Base.Logging;
    using RSG.Bounds;
    using RSG.SceneXml;
    using RSG.SceneXml.MapExport;

    internal static class BoundsLoader
    {
        internal static Dictionary<string, BoundObject> LoadAndIndexRawBoundsData(string boundsZipPathname)
        {
            Dictionary<string, BoundObject> bvhMap = new Dictionary<string, BoundObject>();
            using (Ionic.Zip.ZipFile zipFile = Ionic.Zip.ZipFile.Read(boundsZipPathname))
            {
                foreach (var entry in zipFile.Entries.Where(zfe => zfe.FileName.EndsWith(".bnd")))
                {
                    using (var extractedDataStream = new MemoryStream((int)entry.UncompressedSize))
                    {
                        try
                        {
                            entry.Extract(extractedDataStream);
                            extractedDataStream.Seek(0, SeekOrigin.Begin);
                            BNDFile bndFile = BNDFile.Load(extractedDataStream);
                            string objectName = Path.GetFileNameWithoutExtension(entry.FileName).ToLower();
                            bvhMap.Add(objectName, bndFile.BoundObject);
                        }
                        catch (InvalidDataVersionException ex)
                        {
                            Log.Log__Error("The static bounds data for '{0}' was in an unexpected version ({1}).  " +
                                             "Collision data exported from max will not appear in game.", entry.FileName, ex.Message);
                        }
                        catch (InvalidDataFormatException ex)
                        {
                            Log.Log__Error("The static bounds data for '{0}' was in an unexpected format ({1}).  " +
                                             "Collision data exported from max will not appear in game.", entry.FileName, ex.Message);
                        }
                        catch (InvalidCastException ex)
                        {
                            Log.Log__Error("The static bounds data for '{0}' was not in BVH format ({1}).  " +
                                             "Collision data exported from max will not appear in game.", entry.FileName, ex.Message);
                        }
                    }
                }
            }

            return bvhMap;
        }

        internal static Dictionary<SceneXml.RefObject, Dictionary<BNDFile.CollisionType, List<BVH>>> LoadAndIndexRefBoundsData(IEnumerable<SceneXml.RefObject> refObjects)
        {
            var indexedData = new Dictionary<SceneXml.RefObject, Dictionary<BNDFile.CollisionType, List<BVH>>>();

            foreach (SceneXml.RefObject refObject in refObjects)
            {
                string boundsZipPathname;
                if (String.IsNullOrEmpty(refObject.ProcessedZipPathname))
                    boundsZipPathname = refObject.ExportZipPathname;
                else
                    boundsZipPathname = refObject.ProcessedZipPathname;

                if (!File.Exists(boundsZipPathname))
                {
                    Log.Log__Warning("Unable to bake '{0}' because the zip containing its bounds '{1}' doesn't exist locally.  " +
                                     "This means that dependencies have not been grabbed correctly.  This prop may be rendered but will have no collision.",
                                     refObject.Name, boundsZipPathname);
                    continue;
                }

                using (Ionic.Zip.ZipFile exportedDataZipFile = Ionic.Zip.ZipFile.Read(boundsZipPathname))
                {
                    string containerBoundsZipFilename = Path.GetFileNameWithoutExtension(boundsZipPathname) + ".ibd.zip";
                    if (refObject.CollisionGroupName != null)
                    {
                        string collisionGroupBoundsZipFilename = refObject.CollisionGroupName + ".ibd.zip";
                        if (exportedDataZipFile.ContainsEntry(collisionGroupBoundsZipFilename))
                        {
                            containerBoundsZipFilename = collisionGroupBoundsZipFilename; // Use the collision group bounds (if it exists)
                        }
                    }

                    // Flo : this is ugly as fuck but i dont care.
                    // We have to lookup the correct bound things; they are taken from $(processed).
                    // we need to know if we're appending, and to do that, we check if the path to the source is under $(coreasset) (aka CoreProject, Default, Assets)
                    if (exportedDataZipFile.Name.StartsWith(Branch.Project.Config.CoreProject.DefaultBranch.Assets, StringComparison.OrdinalIgnoreCase))
                    {
                        // do jack shit
                    }
                    else
                    {
                        containerBoundsZipFilename = MapAsset.AppendMapPrefixIfRequired(Branch.Project, containerBoundsZipFilename);
                    }
                    
                    // Load the zip containing the bounds we require
                    Ionic.Zip.ZipEntry containerBoundsEntry = exportedDataZipFile[containerBoundsZipFilename];
                    if (containerBoundsEntry == null)// If the collision set in question has no collision for any of its objects, then no zip will have been generated.
                        continue;

                    using (var extractedContainerBoundsStream = new MemoryStream((int)containerBoundsEntry.UncompressedSize))
                    {
                        containerBoundsEntry.Extract(extractedContainerBoundsStream);
                        extractedContainerBoundsStream.Seek(0, SeekOrigin.Begin);
                        using (Ionic.Zip.ZipFile boundsZip = Ionic.Zip.ZipFile.Read(extractedContainerBoundsStream))
                        {
                            string refObjectBoundsFilename = refObject.RefName + ".bnd";
                            Ionic.Zip.ZipEntry refObjectBoundsEntry = boundsZip[refObjectBoundsFilename];
                            if (refObjectBoundsEntry == null)// If the object in question has no collision, then no bnd will have been generated.
                                continue;

                            using (var extractedBoundsStream = new MemoryStream((int)refObjectBoundsEntry.UncompressedSize))
                            {
                                refObjectBoundsEntry.Extract(extractedBoundsStream);
                                extractedBoundsStream.Seek(0, SeekOrigin.Begin);

                                try
                                {
                                    BNDFile bndFile = BNDFile.Load(extractedBoundsStream);

                                    // Composites are special, as they contain the collision type for each child, so processing is different
                                    if (bndFile.BoundObject is Composite)
                                    {
                                        Dictionary<BNDFile.CollisionType, List<BVH>> bvhMap = BoundObjectConverter.CompositeToBVHMap((Composite)bndFile.BoundObject);
                                        indexedData.Add(refObject, bvhMap);
                                    }
                                    else// for non-composites we derive the collision type from the instance data in the scene xml
                                    {
                                        BVH boundsAsBVH = BoundObjectConverter.ToBVH(bndFile.BoundObject);

                                        Dictionary<BNDFile.CollisionType, List<BVH>> bvhMap = new Dictionary<BNDFile.CollisionType, List<BVH>>();
                                        BNDFile.CollisionType collisionType = GetCollisionTypeForPropFromSceneXml(refObject);
                                        bvhMap.Add(collisionType, new List<BVH>());
                                        bvhMap[collisionType].Add(boundsAsBVH);
                                        indexedData.Add(refObject, bvhMap);
                                    }
                                }
                                catch (InvalidDataFormatException ex)
                                {
                                    Log.Log__Warning("Unable to load BND data at '{0}'/'{1}'/'{2}' because of data format error '{3}'.",
                                        boundsZipPathname, containerBoundsZipFilename, refObjectBoundsFilename, ex.Message);
                                }
                                catch (InvalidDataVersionException ex)
                                {
                                    Log.Log__Warning("Unable to load BND data at '{0}'/'{1}'/'{2}' because of data version error '{3}'.",
                                        boundsZipPathname, containerBoundsZipFilename, refObjectBoundsFilename, ex.Message);
                                }
                            }
                        }
                    }
                }
            }

            return indexedData;
        }

        internal static IEnumerable<Tuple<BNDFile.CollisionType, BVH>> ConvertBoundObjectToBVHPairCollection(string objectName, BoundObject boundObject, BNDFile.CollisionType objectDefCollisionType)
        {
            List<Tuple<BNDFile.CollisionType, BVH>> bvhPairList = new List<Tuple<BNDFile.CollisionType, BVH>>();

            if (boundObject is BVH)
            {
                bvhPairList.Add(new Tuple<BNDFile.CollisionType, BVH>(objectDefCollisionType, (BVH)boundObject));
            }
            else if (boundObject is Composite)
            {
                Composite compositeBounds = (Composite)boundObject;
                foreach (CompositeChild compositeChild in compositeBounds.Children)
                {
                    if (compositeChild.BoundObject is BVH)
                    {
                        BVH bvhBounds = (BVH)compositeChild.BoundObject;
                        bvhBounds.TransformVerts(compositeChild.BoundToParentTransform);
                        bvhPairList.Add(new Tuple<BNDFile.CollisionType, BVH>(compositeChild.CollisionType, bvhBounds));
                    }
                    else
                    {
                        Log.Log__Warning("Composite bounds for object {0} contains a child that isn't a BVH.  This child will be ignored.", objectName);
                    }
                }
            }
            else
            {
                Log.Log__Warning("Bounds for object {0} is neither BVH nor composite.  These bounds will be ignored.", objectName);
            }

            return bvhPairList.ToArray();
        }

        internal static SceneCollection SceneCollection { get; set; }
        internal static IBranch Branch { get; set; }

        /// <summary>
        /// Here we find the collision object def in the scene xml for a refObject.
        /// This is required for objects with non-composite collision because the collision type doesn't appear in the data
        /// </summary>
        /// <param name="refObject"></param>
        /// <returns></returns>
        private static BNDFile.CollisionType GetCollisionTypeForPropFromSceneXml(SceneXml.RefObject refObject)
        {
            if (refNameCollisionTypeMap_.ContainsKey(refObject.RefName))
                return refNameCollisionTypeMap_[refObject.RefName];

            try
            {
                // Get the scene that contains this prop
                string contentName = Path.GetFileNameWithoutExtension(refObject.ExportZipPathname);
                Scene propScene = SceneCollection.GetScene(contentName);
                TargetObjectDef propObjectDef = propScene.FindObject(refObject.RefName);
                TargetObjectDef collisionObjectDef = propObjectDef.Children.First(childObjectDef => childObjectDef.AttributeClass == "gta collision");
                BNDFile.CollisionType collisionType = SceneXml.SceneXmlUtilities.GetObjectRefCollisionType(collisionObjectDef);
                refNameCollisionTypeMap_.Add(refObject.RefName, collisionType);
                return collisionType;
            }
            catch (Exception)
            {
                Log.Log__Warning("Unable to look up collision type for {0}.  Assuming default collision type.", refObject.Name);
                return BNDFile.CollisionType.Default;
            }
        }

        private static Dictionary<string, BNDFile.CollisionType> refNameCollisionTypeMap_ = new Dictionary<string, BNDFile.CollisionType>();
    }
}
