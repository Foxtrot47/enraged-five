﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;

namespace RSG.Pipeline.BoundsProcessor.OrphanedData
{
    /// <summary>
    /// Class responsible for building spatial data associated with a collection of primitives
    /// </summary>
    internal class PrimitiveBlock
    {
        internal PrimitiveBlock(IEnumerable<Primitive> primitives)
        {
            NumPrimitives = primitives.Count();
            EstimatedSize = primitives.Sum(prim => prim.EstimatedSize);

            float minX = float.MaxValue;
            float minY = float.MaxValue;
            float maxX = float.MinValue;
            float maxY = float.MinValue;

            foreach (Primitive primitive in primitives)
            {
                if (primitive.MinX < minX)
                    minX = primitive.MinX;
                if (primitive.MinY < minY)
                    minY = primitive.MinY;
                if (primitive.MaxX > maxX)
                    maxX = primitive.MaxX;
                if (primitive.MaxY > maxY)
                    maxY = primitive.MaxY;
            }

            BoundsXY = new BoundingBox2f(new Vector2f(minX, minY), new Vector2f(maxX, maxY));
        }

        internal int NumPrimitives { get; private set; }
        internal int EstimatedSize { get; private set; }
        internal BoundingBox2f BoundsXY { get; private set; }
    }
}
