﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;
using RSG.Bounds;

namespace RSG.Pipeline.BoundsProcessor.SceneXml
{
    /// <summary>
    /// Represents all the data required during processing for a collision object
    /// Data is divorced from SceneXml.ObjectDef in this way to improve code clarity
    /// </summary>
    internal class CollisionObject
    {
        internal CollisionObject(string name,
                                 string sceneFilename,
                                 BNDFile.CollisionType collisionType,
                                 int compositeOrder,
                                 string parentName,
                                 bool parentIsObject,
                                 bool parentIsDynamic,
                                 bool parentIsFragment,
                                 bool parentIsInAnEntitySet,
                                 bool parentIsPartOfLOD,
                                 bool isNewDLC,
                                 Matrix34f worldToParentTransformMatrix,
                                 string collisionGroupName,
                                 string iplGroupName,
                                 string miloParentName)
        {
            Name = name;
            SceneFilename = sceneFilename;
            CollisionType = collisionType;
            CompositeOrder = compositeOrder;
            ParentName = parentName;
            ParentIsObject = parentIsObject;
            ParentIsDynamic = parentIsDynamic;
            ParentIsFragment = parentIsFragment;
            ParentIsInAnEntitySet = parentIsInAnEntitySet;
            ParentIsPartOfLOD = parentIsPartOfLOD;
            IsNewDLC = isNewDLC;
            WorldToParentTransformMatrix = worldToParentTransformMatrix;
            CollisionGroupName = collisionGroupName;
            IPLGroupName = iplGroupName;
            MILOParentName = miloParentName;
        }


        internal string Name { get; private set; }
        internal string SceneFilename { get; private set; }
        internal BNDFile.CollisionType CollisionType { get; private set; }
        internal int CompositeOrder { get; private set; }
        internal string ParentName { get; private set; }
        internal bool ParentIsObject { get; private set; }
        internal bool ParentIsDynamic { get; private set; }
        internal bool ParentIsFragment { get; private set; }
        internal bool ParentIsInAnEntitySet { get; private set; }
        internal bool ParentIsPartOfLOD { get; private set; }
        internal bool IsNewDLC { get; private set; }
        internal Matrix34f WorldToParentTransformMatrix { get; private set; }
        internal string CollisionGroupName { get; private set; }
        internal string IPLGroupName { get; private set; }
        internal string MILOParentName { get; private set; }
    }
}
