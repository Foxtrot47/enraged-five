﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.BoundsProcessor.Manifest
{
    internal interface IManifestItem
    {
        XElement ToXElement();
    }
}
