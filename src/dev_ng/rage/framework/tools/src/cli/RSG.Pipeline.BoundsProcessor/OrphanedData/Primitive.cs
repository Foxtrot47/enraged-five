﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;
using RSG.Bounds;

namespace RSG.Pipeline.BoundsProcessor.OrphanedData
{
    /// <summary>
    /// The abstract base type for all self contained primitives
    /// </summary>
    internal abstract class Primitive
    {
        internal Primitive(BNDFile.CollisionType collisionType,
                           string material,
                           Vector4i materialColour,
                           float[] secondSurfaceDisplacements,
                           Vector3f[] shrunkVerts,
                           uint numPerVertAttribs)
        {
            CollisionType = collisionType;
            Material = material;
            MaterialColour = materialColour;
            SecondSurfaceDisplacements = secondSurfaceDisplacements;
            ShrunkVerts = shrunkVerts;
            NumPerVertAttribs = numPerVertAttribs;
        }

        internal enum CardinalAxis
        {
            X,
            Y,
            Z
        }

        internal BNDFile.CollisionType CollisionType { get; private set; }
        internal string Material { get; private set; }
        internal Vector4i MaterialColour { get; private set; }
        internal float[] SecondSurfaceDisplacements { get; private set; }
        internal Vector3f[] ShrunkVerts { get; private set; }
        internal uint NumPerVertAttribs { get; private set; }

        internal void RemapCollisionType(Dictionary<string, string> collisionTypeMap)
        {
            string[] materialTokens = Material.Split('|');
            if (materialTokens.Length > 0)
            {
                string collisionType = materialTokens[0];
                if (collisionTypeMap.ContainsKey(collisionType))
                {
                    Material = collisionTypeMap[collisionType] + Material.Substring(collisionType.Length);
                }
            }
        }

        // This data is derived and could be looked up, but caching should be worth the hit
        public Vector2f CentreXY { get; protected set; }
        public float MinX { get; protected set; }
        public float MaxX { get; protected set; }
        public float MinY { get; protected set; }
        public float MaxY { get; protected set; }

        internal abstract int EstimatedSize { get; }
    }
}
