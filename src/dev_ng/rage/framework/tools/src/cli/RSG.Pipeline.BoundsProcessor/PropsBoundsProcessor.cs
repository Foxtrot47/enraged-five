﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.Bounds;
using RSG.SceneXml.MapExport;

using Ionic.Zip;

namespace RSG.Pipeline.BoundsProcessor
{
    internal class PropsBoundsProcessor
    {
        internal PropsBoundsProcessor(IProject project, string sceneName)
        {
            sceneName_ = sceneName;
            propsBoundsMap_ = new Dictionary<string, IndexedBoundsDictionary>();
            staticPropObjectNames_ = new SortedSet<string>();
            project_ = project;
        }

        internal void LoadSceneBounds(IEnumerable<SceneXml.CollisionObject> collisionObjects, string boundsZipPathname)
        {
            if (String.IsNullOrEmpty(boundsZipPathname))
            {
                Log.Log__Message("There were no bounds files to load");
                return;
            }

            Dictionary<string, BoundObject> boundsMap = BoundsLoader.LoadAndIndexRawBoundsData(boundsZipPathname);

            // build the paired collection, respecting the order of the collision objects as there is determinism in the order of composite children
            foreach (SceneXml.CollisionObject collisionObject in collisionObjects)
            {
                KeyValuePair<String, BoundObject> boundsPair = boundsMap.FirstOrDefault(pair => 
                    String.Equals(collisionObject.Name, pair.Key, StringComparison.CurrentCultureIgnoreCase));

                if (String.IsNullOrEmpty(boundsPair.Key))// remember that a default struct is not null!
                {
                    Log.Log__Warning("Unable to look up bounds for node '{0}'.  " +
                                     "This means the exported data and scene XML are out of sync.  Collision data exported from max will not appear in game.",
                                     collisionObject.Name);
                    continue;
                }

                if (!collisionObject.ParentIsObject)
                {
                    Log.Log__Message("Skipping collision for node '{0}'.  This node's parent is not an object.", collisionObject.Name);
                    continue;
                }

                if (string.IsNullOrEmpty(collisionObject.ParentName))
                {
                    Log.Log__Message("Skipping collision for node '{0}'.  This node's parent is invalid.", collisionObject.Name);
                    continue;
                }

                if (collisionObject.ParentIsFragment)
                {
                    Log.Log__Message("Skipping collision for node '{0}'.  This node's parent is a fragment.", collisionObject.Name);
                    continue;
                }

                if (collisionObject.ParentIsPartOfLOD)
                {
                    Log.Log__Message("Skipping collision for node '{0}'. This node's parent is part of an LOD hierarchy.", collisionObject.Name);
                    continue;
                }

                string entryName = sceneName_;
                if(!String.IsNullOrEmpty(collisionObject.CollisionGroupName))
                {
                    entryName = collisionObject.CollisionGroupName;
                }

                if (!propsBoundsMap_.ContainsKey(entryName))
                {
                    propsBoundsMap_.Add(entryName, new IndexedBoundsDictionary());
                }

                IEnumerable<Tuple<BNDFile.CollisionType, BVH>> bvhsToAdd =
                    BoundsLoader.ConvertBoundObjectToBVHPairCollection(boundsPair.Key, boundsPair.Value, collisionObject.CollisionType);

                foreach (Tuple<BNDFile.CollisionType, BVH> typeBVHPair in bvhsToAdd)
                {
                    typeBVHPair.Item2.TransformVerts(collisionObject.WorldToParentTransformMatrix);
                    propsBoundsMap_[entryName].AddBVH(
                        collisionObject.ParentName,
                        typeBVHPair.Item1,
                        typeBVHPair.Item2,
                        IndexedBounds.SerialisationOptions.AsDiscretePrimitives);
                }

                if (!collisionObject.ParentIsDynamic)
                {
                    staticPropObjectNames_.Add(collisionObject.ParentName);
                }
            }

            Log.Log__Message("Loaded {0} bounds files from '{1}'.", boundsMap.Count, boundsZipPathname);
        }

        internal void ProcessAndSerialise(string outputDirectory, string inputDirectory)
        {
            Log.Log__Message("Processing bounds for {0} physics dictionaries", propsBoundsMap_.Count);

            // Make sure suitable static props get serialised as BVH
            foreach (KeyValuePair<string, IndexedBoundsDictionary> propBoundsPair in propsBoundsMap_)
            {
                IndexedBoundsDictionary boundsDictionary = propBoundsPair.Value;
                foreach (string boundsName in boundsDictionary.Keys)
                {
                    if (staticPropObjectNames_.Contains(boundsName))
                    {
                        IndexedBounds bounds = boundsDictionary[boundsName];
                        if (bounds.Volume > Config.TheConfig.Options.MaxNonBVHPropVolume &&
                            bounds.NumPrimitives > Config.TheConfig.Options.MaxNonBVHPropPrimitiveCount)
                        {
                            bounds.SerialisationOption = IndexedBounds.SerialisationOptions.AsBVH;
                        }
                    }
                }
            }

            string secondaryBoundsDirectory = null;
            if (!string.IsNullOrEmpty(inputDirectory) && String.Compare(inputDirectory, "null") != 0)
            {
                secondaryBoundsDirectory = Path.Combine(inputDirectory, "transformed_drawables");

                // Prepare the secondary directory
                if (!Directory.Exists(secondaryBoundsDirectory))
                {
                    Log.Log__Error("Missing secondary bounds directory {0}", secondaryBoundsDirectory);
                }
            }

            foreach (KeyValuePair<string, IndexedBoundsDictionary> propBoundsPair in propsBoundsMap_)
            {
                Log.Log__Message("Processing bounds for physics dictionary '{0}'", propBoundsPair.Key);

                string boundsDictionaryDirectory = Path.Combine(outputDirectory, propBoundsPair.Key);
                Directory.CreateDirectory(boundsDictionaryDirectory);

                propBoundsPair.Value.ProcessAndSerialise(project_, boundsDictionaryDirectory, secondaryBoundsDirectory);
            }
        }

        internal void PackDrawableBounds(string outputDirectory, string inputDirectory, string drawableListString)
        {
            // Any container that doesn't have transformed drawables will come through with "null" from the XML file.
            if (String.Equals(inputDirectory, "null") || String.Equals(drawableListString, "null"))
                return;

            IList<string> drawableList = drawableListString.Split(new char[]{';'}, StringSplitOptions.RemoveEmptyEntries);
            string boundsDirectory = Path.Combine(inputDirectory, "transformed_drawables");

            if (!Directory.Exists(boundsDirectory))
                Directory.CreateDirectory(boundsDirectory);

            // Only process drawables we have in our list.
            if (drawableList.Count > 0)
            {
                foreach (string drawableFilename in drawableList)
                {
                    string drawableName = null;
                    if (drawableFilename.EndsWith(".idr.zip"))
                        drawableName = drawableFilename.Replace(".idr.zip", "");
                    
                    string drawablePath = Path.Combine(inputDirectory, drawableFilename);
                    string transformedDrawablePath = Path.Combine(boundsDirectory, drawableFilename);
                    drawableName = MapAsset.AppendMapPrefixIfRequired(project_, drawableName);
                    string boundPath = Path.Combine(boundsDirectory, string.Format("{0}.bnd", drawableName));

                    if (File.Exists(drawablePath))
                    {
                        if (!File.Exists(boundPath))
                        {
                            Log.Log__Message("Drawable doesn't have a bound to pack '{0}'", drawablePath);

                            // Even If we don't have a bound to pack, still copy over the unmodified drawable to satisfy dependencies.
                            File.Copy(drawablePath, transformedDrawablePath, true);

                            continue;
                        }

                        // Do the actual bound packing
                        PackDrawableBound(drawablePath, transformedDrawablePath, boundPath);
                    }
                    else
                    {
                        Log.Log__Warning("Couldn't find drawable to pack bounds '{0}'", drawableFilename);
                    }
                }

                DirectoryInfo boundsDirectoryInfo = new DirectoryInfo(boundsDirectory);
                boundsDirectoryInfo.GetFiles("*.bnd").ToList().ForEach(file => file.Delete());
            }
        }

        internal void PackDrawableBound(string drawablePath, string transformedDrawablePath, string boundPath)
        {
            Log.Log__Message("Packing bounds for '{0}' with bound file {1}", transformedDrawablePath, boundPath);

            using (Ionic.Zip.ZipFile exportedDataZipFile = Ionic.Zip.ZipFile.Read(drawablePath))
            {
                try
                {
                    exportedDataZipFile.AddFile(boundPath, "");
                    exportedDataZipFile.Save(transformedDrawablePath);
                }
                catch (Exception e)
                {
                    Log.Log__Message("Error packing bounds for '{0}' with {1}\nException: {2}", drawablePath, boundPath, e.Message);
                }
            }
        }

        private string sceneName_;
        private IProject project_;
        private Dictionary<string, IndexedBoundsDictionary> propsBoundsMap_;
        private SortedSet<string> staticPropObjectNames_;
    }
}
