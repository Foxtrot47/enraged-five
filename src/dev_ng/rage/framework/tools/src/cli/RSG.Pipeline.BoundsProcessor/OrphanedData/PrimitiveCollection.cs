﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using RSG.Base.Logging;
using RSG.Base.Profiling;
using RSG.Base.Math;
using RSG.Bounds;

namespace RSG.Pipeline.BoundsProcessor.OrphanedData
{
    /// <summary>
    /// This class represents a (potentially hierarchical) collection of bounds primitives that are self contained.  This differentiates from a Composite
    /// because primitives in composite bounds, in the form of BVHs, index into lists of verts, second surface heights, materials, etc.
    /// Self contained primitive data is much easier to work with in terms of splitting and putting together.
    /// </summary>
    internal class PrimitiveCollection
    {
        internal PrimitiveCollection()
        {
            primitives_ = new List<Primitive>();
            childPrimitiveCollectionMap_ = null;
        }

        /// <summary>
        /// Provided for debugging purposes
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            if (!IsLeaf)
            {
                foreach (var dividedPrimitivesPair in childPrimitiveCollectionMap_)
                {
                    stringBuilder.AppendFormat("[{0},{1}]: ", dividedPrimitivesPair.Key.X, dividedPrimitivesPair.Key.Y);
                    stringBuilder.Append(dividedPrimitivesPair.Value.ToString());
                    stringBuilder.AppendLine();
                }
            }
            else
            {
                var bounds = MeasureBoundsXY();
                stringBuilder.AppendFormat("{0} primitives spread across [{1},{2}]", primitives_.Count, bounds.Width, bounds.Height);
            }

            return stringBuilder.ToString();
        }

        internal bool IsHighDetail
        {
            get
            {
                // We assume that if the first primitive is weapon only, the rest is.  This should always be valid (and is much quicker than checking the lot!)
                return (IsLeaf && 
                        primitives_.Count > 0 && 
                        primitives_[0].CollisionType == BNDFile.CollisionType.Weapons);
            }
        }

        /// <summary>
        /// Useful for diagnostics, should not be called on a normal run of the BP
        /// Builds a 2D grid of primitive density
        /// </summary>
        /// <param name="pathname"></param>
        internal void DumpSpatialDistributionData(string pathname)
        {
            using (System.IO.StreamWriter writer = System.IO.File.CreateText(pathname))
            {
                int maxX = childPrimitiveCollectionMap_.Keys.Max(key => key.X);
                int maxY = childPrimitiveCollectionMap_.Keys.Max(key => key.Y);

                for (int y = 0; y <= maxY; ++y)
                {
                    for (int x = 0; x <= maxX; ++x)
                    {
                        var key = new Vector2i(x, y);
                        if (childPrimitiveCollectionMap_.ContainsKey(key))
                        {
                            writer.Write(childPrimitiveCollectionMap_[key].CountPrimitives());
                            if (childPrimitiveCollectionMap_[key].IsLeaf)
                            {
                                writer.Write(",");
                            }
                            else
                            {
                                writer.Write("(B),");
                            }
                        }
                        else
                        {
                            writer.Write(",");
                        }
                    }
                    writer.WriteLine();
                }
            }
        }

        internal PrimitiveCollection[] GetAllLeafNodes()
        {
            List<PrimitiveCollection> leafNodes = new List<PrimitiveCollection>();

            GetAllLeafNodesRecursive(leafNodes);

            return leafNodes.ToArray();
        }

        internal void AddPrimitive(Primitive primitive)
        {
            if (!IsLeaf)
            {
                throw new InvalidOperationException();
            }

            primitives_.Add(primitive);
        }

        internal void SplitOnPhysicalXYLimit(float maxSizeXYPerSection)
        {
            if (!IsLeaf)
            {
                throw new InvalidOperationException();
            }

            // Split the primitives across the various sections
            BoundingBox2f boundsXY = MeasureBoundsXY();
            Vector2i divisionsXY = CalcualteDivisionsRequired(boundsXY, maxSizeXYPerSection);
            float sectionSizeX = (boundsXY.Max.X - boundsXY.Min.X) / (float)divisionsXY.X;
            float sectionSizeY = (boundsXY.Max.Y - boundsXY.Min.Y) / (float)divisionsXY.Y;

            Log.Log__Message("\tSplitting {0} primitives spanning {1}x{2} into {3}x{4} sub-sections of {5}x{6}",
                primitives_.Count, boundsXY.Width, boundsXY.Height, divisionsXY.X, divisionsXY.Y, sectionSizeX, sectionSizeY);

            Split(divisionsXY.X, divisionsXY.Y, boundsXY.Min.X, boundsXY.Min.Y, sectionSizeX, sectionSizeY);
        }

        internal void SplitOnPrimitiveCountLimitRecursive(int maxCompositeSize)
        {
            // If it's a leaf they try to split it
            if (IsLeaf && (GetEstimatedSizeAsComposite() > maxCompositeSize))
            {
                BoundingBox2f boundsXY = MeasureCentreDistributionBoundsXY();

                // inflate this slightly, so that the edge cases don't cause more splitting that we want
                float minX = boundsXY.Min.X - (boundsXY.Width * 1.001f);
                float maxX = boundsXY.Max.X + (boundsXY.Width * 1.001f);
                float minY = boundsXY.Min.Y - (boundsXY.Height * 1.001f);
                float maxY = boundsXY.Max.Y + (boundsXY.Height * 1.001f);

                float sectionSizeX = (maxX - minX) * 0.5f;
                float sectionSizeY = (maxY - minY) * 0.5f;

                Split(2, 2, minX, minY, sectionSizeX, sectionSizeY);
            }

            // If it's not a leaf (bear in mind it may have just been split above) then try to split the children
            if (!IsLeaf)
            {
                foreach (KeyValuePair<Vector2i, PrimitiveCollection> dividedPrimitivesPair in childPrimitiveCollectionMap_)
                {
                    dividedPrimitivesPair.Value.SplitOnPrimitiveCountLimitRecursive(maxCompositeSize);
                }
            }
        }

        internal void MergeSmallSectionsWithLargerNeighbours()
        {
            if (IsLeaf)
                return;

            List<Vector2i> sectionsWithNoSuitableMergeTarget = new List<Vector2i>();

            var smallestSectionQuery = from pair in childPrimitiveCollectionMap_
                                       where pair.Value.IsLeaf && 
                                             pair.Value.CountPrimitives() < Config.TheConfig.Options.MinimumPrimitiveCount &&
                                             !sectionsWithNoSuitableMergeTarget.Contains(pair.Key)
                                       orderby pair.Value.CountPrimitives()
                                       select pair;

            // While we still have sections that need to be merged
            while(smallestSectionQuery.Count() > 0 && childPrimitiveCollectionMap_.Where(pair => pair.Value.IsLeaf).Count() >= 2)
            {
                KeyValuePair<Vector2i, PrimitiveCollection> smallestSectionPair = smallestSectionQuery.First();
                BoundingBox2f smallestSectionBounds = smallestSectionPair.Value.MeasureBoundsXY();

                // Find the nearest neighbour
                Vector2i closestNeighbourKey = null;
                double distanceToClosestNeighbour = double.MaxValue;
                foreach(KeyValuePair<Vector2i, PrimitiveCollection> potentialNeighbourPair in childPrimitiveCollectionMap_)
                {
                    if( !potentialNeighbourPair.Value.IsLeaf || smallestSectionPair.Key == potentialNeighbourPair.Key )
                        continue;// ignore ourself and non-leaf nodes

                    // Check to see if the merge will break our BVH bounds limit
                    BoundingBox2f potentialNeighbourBounds = potentialNeighbourPair.Value.MeasureBoundsXY();
                    BoundingBox2f potentialMergedSectionBounds = new BoundingBox2f();
                    potentialMergedSectionBounds.Expand(smallestSectionBounds);
                    potentialMergedSectionBounds.Expand(potentialNeighbourBounds);
                    if (potentialMergedSectionBounds.Width > Config.TheConfig.Options.MaxBVHBoundsXY ||
                        potentialMergedSectionBounds.Height > Config.TheConfig.Options.MaxBVHBoundsXY)
                    {
                        continue;
                    }

                    double distanceFromSmallSection = (smallestSectionPair.Value.Centre - potentialNeighbourPair.Value.Centre).Magnitude();
                    if (distanceFromSmallSection < distanceToClosestNeighbour)
                    {
                        closestNeighbourKey = potentialNeighbourPair.Key;
                        distanceToClosestNeighbour = distanceFromSmallSection;
                    }
                }

                if (closestNeighbourKey != null)
                {
                    // Merge the two
                    childPrimitiveCollectionMap_[closestNeighbourKey].MergeWith(smallestSectionPair.Value);
                    childPrimitiveCollectionMap_.Remove(smallestSectionPair.Key);
                }
                else
                {
                    sectionsWithNoSuitableMergeTarget.Add(smallestSectionPair.Key);
                }
            }

            // Recurse
            foreach (KeyValuePair<Vector2i, PrimitiveCollection> pair in childPrimitiveCollectionMap_)
            {
                if (!pair.Value.IsLeaf)
                {
                    pair.Value.MergeSmallSectionsWithLargerNeighbours();
                }
            }
        }

        private static Vector2i CalcualteDivisionsRequired(BoundingBox2f boundsXY, float maxSizeXYPerSection)
        {
            float width = boundsXY.Max.X - boundsXY.Min.X;
            float height = boundsXY.Max.Y - boundsXY.Min.Y;
            float minorDimension = Math.Min(width, height);
            int numMinorDimensionDivisions = 1;
            if (minorDimension > maxSizeXYPerSection)
            {
                numMinorDimensionDivisions = (int)((minorDimension / maxSizeXYPerSection) + 0.5f);
            }
            float minorDivisionSize = minorDimension / (float)numMinorDimensionDivisions;
            float majorDimension = Math.Max(width, height);
            int numMajorDimensionDivisions = 1;
            if (majorDimension > maxSizeXYPerSection)
            {
                numMajorDimensionDivisions = (int)((majorDimension / minorDivisionSize) + 0.5f);
            }
            float majorDivisionSize = majorDimension / (float)numMajorDimensionDivisions;

            if (width >= height)
            {
                return new Vector2i(numMajorDimensionDivisions, numMinorDimensionDivisions);
            }
            else
            {
                return new Vector2i(numMinorDimensionDivisions, numMajorDimensionDivisions);
            }
        }

        private bool IsLeaf { get { return (primitives_ != null); } }
        private Vector2f Centre { get { return MeasureBoundsXY().Centre(); } }

        private void Split(int divisionsX, int divisionsY, float startX, float startY, float sectionSizeX, float sectionSizeY)
        {
            if (Config.TheConfig.Options.ComplexMapCollisionSplitting)
            {
                SplitComplex(divisionsX, divisionsY, startX, startY, sectionSizeX, sectionSizeY);
            }
            else
            {
                SplitSimple(divisionsX, divisionsY, startX, startY, sectionSizeX, sectionSizeY);
            }
        }

        private void SplitComplex(int divisionsX, int divisionsY, float startX, float startY, float sectionSizeX, float sectionSizeY)
        {
            List<float> xSplits = new List<float>();// the Y axis positions to split in X on
            for (int xSplitIndex = 1; xSplitIndex < divisionsX; ++xSplitIndex)
            {
                float splitPosition = startX + (sectionSizeX * (float)xSplitIndex);
                BestSplitFinder.FindBestSplit(primitives_, ref splitPosition, Primitive.CardinalAxis.Y, sectionSizeX);
                xSplits.Add(splitPosition);
            }

            List<float> ySplits = new List<float>();// the X axis positions to split in Y on
            for (int ySplitIndex = 1; ySplitIndex < divisionsY; ++ySplitIndex)
            {
                float splitPosition = startY + (sectionSizeY * (float)ySplitIndex);
                BestSplitFinder.FindBestSplit(primitives_, ref splitPosition, Primitive.CardinalAxis.X, sectionSizeY);
                ySplits.Add(splitPosition);
            }

            childPrimitiveCollectionMap_ = new Dictionary<Vector2i, OrphanedData.PrimitiveCollection>();
            foreach (var orphanedPrimitive in primitives_)
            {
                int xPos = divisionsX - 1;
                for (int splitIndex = 0; splitIndex < xSplits.Count && xPos != divisionsX; ++splitIndex)
                {
                    if (orphanedPrimitive.CentreXY.X <= xSplits[splitIndex])
                    {
                        xPos = splitIndex;
                        break;
                    }
                }

                int yPos = divisionsY - 1;
                for (int splitIndex = 0; splitIndex < ySplits.Count && yPos != divisionsY; ++splitIndex)
                {
                    if (orphanedPrimitive.CentreXY.Y <= ySplits[splitIndex])
                    {
                        yPos = splitIndex;
                        break;
                    }
                }

                Vector2i sectionIndex = new Vector2i(xPos, yPos);

                if (!childPrimitiveCollectionMap_.ContainsKey(sectionIndex))
                {
                    childPrimitiveCollectionMap_.Add(sectionIndex, new OrphanedData.PrimitiveCollection());
                }
                childPrimitiveCollectionMap_[sectionIndex].AddPrimitive(orphanedPrimitive);
            }

            primitives_ = null;// This marks that this collection is no longer a leaf (and frees some memory on the next collection!)
        }

        private void SplitSimple(int divisionsX, int divisionsY, float startX, float startY, float sectionSizeX, float sectionSizeY)
        {
            childPrimitiveCollectionMap_ = new Dictionary<Vector2i, OrphanedData.PrimitiveCollection>();
            foreach (var orphanedPrimitive in primitives_)
            {
                float fractionalXPos = ((orphanedPrimitive.CentreXY.X - startX) / sectionSizeX);
                float fractionalYPos = ((orphanedPrimitive.CentreXY.Y - startY) / sectionSizeY);
                Vector2i sectionIndex = new Vector2i((int)fractionalXPos, (int)fractionalYPos);

                if (!childPrimitiveCollectionMap_.ContainsKey(sectionIndex))
                {
                    childPrimitiveCollectionMap_.Add(sectionIndex, new OrphanedData.PrimitiveCollection());
                }
                childPrimitiveCollectionMap_[sectionIndex].AddPrimitive(orphanedPrimitive);
            }

            primitives_ = null;// This marks that this collection is no longer a leaf (and frees some memory on the next collection!)
        }

        private void MergeWith(PrimitiveCollection otherSection)
        {
            if (!IsLeaf || !otherSection.IsLeaf)
                throw new InvalidOperationException("Cannot merge non-leaf sections");

            primitives_.AddRange(otherSection.primitives_);
        }

        private void GetAllLeafNodesRecursive(ICollection<PrimitiveCollection> collection)
        {
            if (IsLeaf)
            {
                collection.Add(this);
            }
            else
            {
                foreach (var dividedPrimitivesPair in childPrimitiveCollectionMap_)
                {
                    dividedPrimitivesPair.Value.GetAllLeafNodesRecursive(collection);
                }
            }
        }

        /// <summary>
        /// Converts a leaf PrimitiveCollection to a Composite bounds object
        /// </summary>
        /// <returns></returns>
        internal Composite ToComposite(IndexedBounds.SerialisationOptions serialisationOptions, int maxBVHDataSize)
        {
            if (serialisationOptions == IndexedBounds.SerialisationOptions.AsBVH)
            {
                if (Config.TheConfig.Options.ComplexMapCollisionSplitting)
                {
                    return ToCompositeOfBVHsComplex(maxBVHDataSize);
                }
                else
                {
                    return ToCompositeOfBVHsSimple(maxBVHDataSize);
                }
                
            }
            else if (serialisationOptions == IndexedBounds.SerialisationOptions.AsDiscretePrimitives)
            {
                return ToCompositeOfDiscretePrimitives();
            }
            else
            {
                throw new ArgumentException(String.Format("Unrecognised serialisationOptions of {0}", serialisationOptions.ToString()), "serialisationOptions");
            }
        }

        private Composite ToCompositeOfBVHsComplex(int maxBVHDataSize)
        {
            CompositeBuilder compositeBuilder = new CompositeBuilder(maxBVHDataSize, primitives_);
            return compositeBuilder.ToComposite();
        }

        private Composite ToCompositeOfBVHsSimple(int maxBVHDataSize)
        {
            List<CompositeChild> compositeBounds = new List<CompositeChild>();

            // Here we make a set of buckets for the various primitives to go into.
            var bvhBucketMap = new Dictionary<BVHGeometryType, BVHBuilder.BVHBuilder>();

            foreach (Primitive primitive in primitives_)
            {
                var geometryType = new BVHGeometryType(
                    primitive.CollisionType,
                    primitive.MaterialColour != null,
                    primitive.SecondSurfaceDisplacements != null,
                    primitive.ShrunkVerts != null,
                    primitive.NumPerVertAttribs);

                if (!bvhBucketMap.ContainsKey(geometryType))
                {
                    bvhBucketMap.Add(geometryType, new BVHBuilder.BVHBuilder(primitive.NumPerVertAttribs));
                }

                BVHBuilder.BVHBuilder bvhBuilder = bvhBucketMap[geometryType];
                bvhBuilder.Append(primitive);

                // check to see if this bucket is full
                if (bvhBuilder.EstimatedSize >= maxBVHDataSize)
                {
                    BVH optimisedBVH = bvhBuilder.ToBVH();
                    CompositeChild newBound = new CompositeChild(primitive.CollisionType, optimisedBVH, Matrix34f.Identity);
                    compositeBounds.Add(newBound);
                    bvhBucketMap.Remove(geometryType);
                }
            }

            // TODO empty and partially filled buckets
            foreach (var bvhBucketPair in bvhBucketMap)
            {
                BVH optimisedBVH = bvhBucketPair.Value.ToBVH();
                CompositeChild newBound = new CompositeChild(bvhBucketPair.Key.CollisionType, optimisedBVH, Matrix34f.Identity);
                compositeBounds.Add(newBound);
            }
            bvhBucketMap.Clear();

            return new Composite(compositeBounds.ToArray());
        }

        private Composite ToCompositeOfDiscretePrimitives()
        {
            List<CompositeChild> compositeBounds = new List<CompositeChild>();

            foreach (OrphanedData.Box orphanedBox in primitives_.OfType<OrphanedData.Box>())
            {
                compositeBounds.Add(PrimitiveConverter.ToCompositeChildBox(orphanedBox));
            }

            foreach (OrphanedData.Capsule orphanedCapsule in primitives_.OfType<OrphanedData.Capsule>())
            {
                compositeBounds.Add(PrimitiveConverter.ToCompositeChildCapsule(orphanedCapsule));
            }

            foreach (OrphanedData.Cylinder orphanedCylinder in primitives_.OfType<OrphanedData.Cylinder>())
            {
                compositeBounds.Add(PrimitiveConverter.ToCompositeChildCylinder(orphanedCylinder));
            }

            foreach (OrphanedData.Sphere orphanedSphere in primitives_.OfType<OrphanedData.Sphere>())
            {
                compositeBounds.Add(PrimitiveConverter.ToCompositeChildSphere(orphanedSphere));
            }

            // B* 1026951 - an enumeration of triangles can => a number of composite children
            IEnumerable<OrphanedData.Triangle> orphanedTriangles = primitives_.OfType<OrphanedData.Triangle>();
            if (orphanedTriangles.Count() > 0)
            {
                IEnumerable<CompositeChild> compositeChildren = PrimitiveConverter.ToCompositeChildGeometries(orphanedTriangles);
                foreach (CompositeChild compositeChild in compositeChildren)
                    compositeBounds.Add(compositeChild);
            }

            return new Composite(compositeBounds.ToArray());
        }

        private BoundingBox2f MeasureBoundsXY()
        {
            float minX = float.MaxValue;
            float minY = float.MaxValue;
            float maxX = float.MinValue;
            float maxY = float.MinValue;

            if (IsLeaf)
            {
                foreach (Primitive primitive in primitives_)
                {
                    if (primitive.MinX < minX)
                        minX = primitive.MinX;
                    if (primitive.MinY < minY)
                        minY = primitive.MinY;
                    if (primitive.MaxX > maxX)
                        maxX = primitive.MaxX;
                    if (primitive.MaxY > maxY)
                        maxY = primitive.MaxY;
                }
            }
            else
            {
                foreach (var dividedPrimitivesPair in childPrimitiveCollectionMap_)
                {
                    BoundingBox2f childBounds = dividedPrimitivesPair.Value.MeasureBoundsXY();

                    if (childBounds.Min.X < minX)
                        minX = childBounds.Min.X;
                    if (childBounds.Min.Y < minY)
                        minY = childBounds.Min.Y;
                    if (childBounds.Max.X > maxX)
                        maxX = childBounds.Max.X;
                    if (childBounds.Max.Y > maxY)
                        maxY = childBounds.Max.Y;
                }
            }

            return new BoundingBox2f(new Vector2f(minX, minY), new Vector2f(maxX, maxY));
        }

        private BoundingBox2f MeasureCentreDistributionBoundsXY()
        {
            float minX = float.MaxValue;
            float minY = float.MaxValue;
            float maxX = float.MinValue;
            float maxY = float.MinValue;

            if (IsLeaf)
            {
                foreach (Primitive primitive in primitives_)
                {
                    if (primitive.CentreXY.X < minX)
                        minX = primitive.CentreXY.X;
                    if (primitive.CentreXY.Y < minY)
                        minY = primitive.CentreXY.Y;
                    if (primitive.CentreXY.X > maxX)
                        maxX = primitive.CentreXY.X;
                    if (primitive.CentreXY.Y > maxY)
                        maxY = primitive.CentreXY.Y;
                }
            }
            else
            {
                foreach (var dividedPrimitivesPair in childPrimitiveCollectionMap_)
                {
                    BoundingBox2f childBounds = dividedPrimitivesPair.Value.MeasureCentreDistributionBoundsXY();

                    if (childBounds.Min.X < minX)
                        minX = childBounds.Min.X;
                    if (childBounds.Min.Y < minY)
                        minY = childBounds.Min.Y;
                    if (childBounds.Max.X > maxX)
                        maxX = childBounds.Max.X;
                    if (childBounds.Max.Y > maxY)
                        maxY = childBounds.Max.Y;
                }
            }

            return new BoundingBox2f(new Vector2f(minX, minY), new Vector2f(maxX, maxY));
        }

        internal int CountPrimitives()
        {
            if (IsLeaf)
            {
                return primitives_.Count;
            }
            else
            {
                int numPrimitives = 0;
                foreach (var dividedPrimitivesPair in childPrimitiveCollectionMap_)
                {
                    numPrimitives += dividedPrimitivesPair.Value.CountPrimitives();
                }

                return numPrimitives;
            }
        }

        private int GetEstimatedSizeAsComposite()
        {
            if (IsLeaf)
            {
                int estimatedSize = primitives_.Sum(prim => prim.EstimatedSize);
                return estimatedSize;
            }
            else
            {
                int estimatedSize = 0;
                foreach (var dividedPrimitivesPair in childPrimitiveCollectionMap_)
                {
                    estimatedSize += dividedPrimitivesPair.Value.GetEstimatedSizeAsComposite();
                }

                return estimatedSize;
            }
        }

        /// <summary>
        /// Checks box primitives to make sure they aren't too small in any one dimension
        /// </summary>
        internal void CheckForSlimBoxes()
        {
            List<Box> slimBoxes = new List<Box>();

            foreach (Box box in primitives_.OfType<Box>())
            {
                if (box.SmallestDimension < Config.TheConfig.Options.SmallestPermittedBoxDimension)
                {
                    slimBoxes.Add(box);
                }
            }

            foreach (Box box in slimBoxes)
            {
                Log.Log__Warning(
                    "Found a slim box with a smallest dimension of {0}m at [{1}, {2}, {3}]",
                    box.SmallestDimension,
                    box.Centre.X,
                    box.Centre.Y,
                    box.Centre.Z);
            }
        }

        /// <summary>
        /// Corrects box primitives that aren't quite box primitives (this can be caused by non-uniform scaling)
        /// </summary>
        internal void CorrectSkewedBoxes()
        {
            foreach (Box box in primitives_.OfType<Box>())
            {
                box.CorrectSkew();
            }
        }

        /// <summary>
        /// Remaps collision types as defined by <tools>\etc\config\generic\collision_type_mappings.xml
        /// </summary>
        internal void RemapCollisionTypes()
        {
            if (collisionTypeMap_ == null)
                collisionTypeMap_ = LoadCollisionTypeMappings();

            if (collisionTypeMap_.Count == 0)
                return;

            foreach (Primitive primitive in primitives_)
            {
                primitive.RemapCollisionType(collisionTypeMap_);
            }
        }

        private Dictionary<string, string> LoadCollisionTypeMappings()
        {
            Dictionary<string, string> collisionTypeMap = new Dictionary<string, string>();

            string collisionTypeMappingsPathname = Environment.ExpandEnvironmentVariables("%RS_TOOLSROOT%\\etc\\config\\generic\\collision_type_mappings.xml");

            try
            {
                XDocument xmlDocument = XDocument.Load(collisionTypeMappingsPathname);
                foreach (XElement mappingElement in xmlDocument.Root.Elements("collision_type_mapping"))
                {
                    collisionTypeMap.Add(
                        mappingElement.Attribute("source").Value.ToLower(), 
                        mappingElement.Attribute("target").Value.ToLower());
                }
            }
            catch (Exception)
            {
                Log.Log__Warning("The bounds processor was unable to load collision type mappings file from {0}", collisionTypeMappingsPathname);
            }
            

            return collisionTypeMap;
        }

        private static Dictionary<string, string> collisionTypeMap_;

        private List<Primitive> primitives_;
        private Dictionary<Vector2i, OrphanedData.PrimitiveCollection> childPrimitiveCollectionMap_;
    }
}
