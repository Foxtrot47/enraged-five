﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.Bounds;

namespace RSG.Pipeline.BoundsProcessor.OrphanedData
{
    /// <summary>
    /// A self contained box primitive
    /// </summary>
    internal class Box : Primitive
    {
        internal Box(BNDFile.CollisionType collisionType,
                     string material,
                     Vector4i materialColour,
                     float[] secondSurfaceDisplacements,
                     Vector3f[] shrunkVerts,
                     VertexData vert0,
                     VertexData vert1,
                     VertexData vert2,
                     VertexData vert3,
                     uint numPerVertAttribs)
            : base(collisionType, material, materialColour, secondSurfaceDisplacements, shrunkVerts, numPerVertAttribs)
        {
            Vert0 = vert0;
            Vert1 = vert1;
            Vert2 = vert2;
            Vert3 = vert3;

            float centreX = (vert0.Position.X + vert1.Position.X + vert2.Position.X + vert3.Position.X) * 0.25f;
            float centreY = (vert0.Position.Y + vert1.Position.Y + vert2.Position.Y + vert3.Position.Y) * 0.25f;
            float centreZ = (vert0.Position.Z + vert1.Position.Z + vert2.Position.Z + vert3.Position.Z) * 0.25f;
            CentreXY = new Vector2f(centreX, centreY);
            Centre = new Vector3f(centreX, centreY, centreZ);
            MinX = Math.Min(vert0.Position.X, Math.Min(vert1.Position.X, Math.Min(vert2.Position.X, vert3.Position.X)));
            MaxX = Math.Max(vert0.Position.X, Math.Max(vert1.Position.X, Math.Max(vert2.Position.X, vert3.Position.X)));
            MinY = Math.Min(vert0.Position.Y, Math.Min(vert1.Position.Y, Math.Min(vert2.Position.Y, vert3.Position.Y)));
            MaxY = Math.Max(vert0.Position.Y, Math.Max(vert1.Position.Y, Math.Max(vert2.Position.Y, vert3.Position.Y)));
        }

        internal void CorrectSkew()
        {
            if (IsSkewed)
            {
                // Determine the box size (skew will affect this, but it should be close enough)
                Vector3f twiceBoxX = Vert0.Position + Vert2.Position - Vert1.Position - Vert3.Position;
                Vector3f twiceBoxY = Vert0.Position + Vert1.Position - Vert2.Position - Vert3.Position;
                Vector3f twiceBoxZ = Vert1.Position + Vert2.Position - Vert0.Position - Vert3.Position;
                Vector3f halfBoxSize = new Vector3f((float)twiceBoxX.Magnitude() * 0.25f, (float)twiceBoxY.Magnitude() * 0.25f, (float)twiceBoxZ.Magnitude() * 0.25f);

                // rebuild the box in local space
                VertexData newVert0 = new VertexData(new Vector3f(halfBoxSize.X, halfBoxSize.Y, -halfBoxSize.Z), Vert0.AttributeData);
                VertexData newVert1 = new VertexData(new Vector3f(-halfBoxSize.X, halfBoxSize.Y, halfBoxSize.Z), Vert1.AttributeData);
                VertexData newVert2 = new VertexData(new Vector3f(halfBoxSize.X, -halfBoxSize.Y, halfBoxSize.Z), Vert2.AttributeData);
                VertexData newVert3 = new VertexData(new Vector3f(-halfBoxSize.X, -halfBoxSize.Y, -halfBoxSize.Z), Vert3.AttributeData);



                // convert back into world space
                Vector3f boxXAxis = Vector3f.Normalise(twiceBoxX);
                Vector3f boxYAxis = Vector3f.Normalise(twiceBoxY);
                Vector3f boxZAxis = Vector3f.Normalise(twiceBoxZ);
                float dotXZ = Vector3f.Dot(boxXAxis, boxZAxis);
                if (dotXZ < -0.7071f || dotXZ > 0.7071)
                {
                    boxZAxis = Vector3f.Cross(boxXAxis, boxYAxis);
                    boxYAxis = Vector3f.Cross(boxZAxis, boxXAxis);
                }
                else
                {
                    boxYAxis = Vector3f.Cross(boxZAxis, boxXAxis);
                    boxZAxis = Vector3f.Cross(boxXAxis, boxYAxis);
                }

                boxYAxis.Normalise();
                boxZAxis.Normalise();
                Vector3f boxCentre = (Vert0.Position + Vert1.Position + Vert2.Position + Vert3.Position) * 0.25f;
                Matrix34f localToWorldTransformMatrix = new Matrix34f(boxXAxis, boxYAxis, boxZAxis, boxCentre);
                
                localToWorldTransformMatrix.Transform(newVert0.Position);
                localToWorldTransformMatrix.Transform(newVert1.Position);
                localToWorldTransformMatrix.Transform(newVert2.Position);
                localToWorldTransformMatrix.Transform(newVert3.Position);

                // Set the data in this
                Vert0 = newVert0;
                Vert1 = newVert1;
                Vert2 = newVert2;
                Vert3 = newVert3;
            }
        }

        private const float maxRelativeSkewFactor_ = 0.0012f;// 5 times more strict than the RAGE Physics code at the time of writing

        internal VertexData Vert0 { get; private set; }
        internal VertexData Vert1 { get; private set; }
        internal VertexData Vert2 { get; private set; }
        internal VertexData Vert3 { get; private set; }

        internal Vector3f Centre { get; private set; }

        internal float LargestDimension
        {
            get
            {
                Vector3f twiceBoxX = Vert0.Position + Vert2.Position - Vert1.Position - Vert3.Position;
                Vector3f twiceBoxY = Vert0.Position + Vert1.Position - Vert2.Position - Vert3.Position;
                Vector3f twiceBoxZ = Vert1.Position + Vert2.Position - Vert0.Position - Vert3.Position;
                Vector3f boxSize = new Vector3f((float)twiceBoxX.Magnitude() * 0.5f, (float)twiceBoxY.Magnitude() * 0.5f, (float)twiceBoxZ.Magnitude() * 0.5f);

                return Math.Max(boxSize.X, Math.Max(boxSize.Y, boxSize.Z));
            }
        }

        internal float SmallestDimension
        {
            get
            {
                Vector3f twiceBoxX = Vert0.Position + Vert2.Position - Vert1.Position - Vert3.Position;
                Vector3f twiceBoxY = Vert0.Position + Vert1.Position - Vert2.Position - Vert3.Position;
                Vector3f twiceBoxZ = Vert1.Position + Vert2.Position - Vert0.Position - Vert3.Position;
                Vector3f boxSize = new Vector3f((float)twiceBoxX.Magnitude() * 0.5f, (float)twiceBoxY.Magnitude() * 0.5f, (float)twiceBoxZ.Magnitude() * 0.5f);

                return Math.Min(boxSize.X, Math.Min(boxSize.Y, boxSize.Z));
            }
        }

        private bool IsSkewed
        {
            get
            {
                Vector3f diagonalLengths0 = new Vector3f((float)Vert0.Position.Dist(Vert1.Position), (float)Vert0.Position.Dist(Vert2.Position), (float)Vert0.Position.Dist(Vert3.Position));
                Vector3f diagonalLengths1 = new Vector3f((float)Vert2.Position.Dist(Vert3.Position), (float)Vert1.Position.Dist(Vert3.Position), (float)Vert1.Position.Dist(Vert2.Position));
                Vector3f averageDiagonalLength = (diagonalLengths0 + diagonalLengths1) * 0.5f;
                Vector3f diagonalLengthDifferences = diagonalLengths0 - diagonalLengths1;
                Vector3f maxAllowedDifference = averageDiagonalLength * maxRelativeSkewFactor_;

                return ((Math.Abs(diagonalLengthDifferences.X) > maxAllowedDifference.X) ||
                        (Math.Abs(diagonalLengthDifferences.Y) > maxAllowedDifference.Y) ||
                        (Math.Abs(diagonalLengthDifferences.Z) > maxAllowedDifference.Z));
            }
        }

        internal override int EstimatedSize 
        {
            get { return 38; }// one primitive plus 11/3 verts
        }
    }
}
