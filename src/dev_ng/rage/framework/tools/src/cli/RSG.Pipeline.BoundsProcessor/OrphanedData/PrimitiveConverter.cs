﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;
using RSG.Bounds;

namespace RSG.Pipeline.BoundsProcessor.OrphanedData
{
    /// <summary>
    /// Converts a BVHPrimitive (given the BVH it indexes) into a self contained primitive that is aware if its collision type
    /// </summary>
    internal static class PrimitiveConverter
    {
        internal static Primitive FromBVHPrimitive(BVHPrimitive bvhPrimitive, BVH bvh, BNDFile.CollisionType collisionType)
        {
            if (bvhPrimitive is BVHTriangle)
            {
                BVHTriangle bvhTriangle = (BVHTriangle)bvhPrimitive;
                string materialDefinition = bvh.Materials[bvhTriangle.MaterialIndex];
                BVHMaterial material = new BVHMaterial(materialDefinition);
                Vector4i materialColour = null;
                if (material.PaletteIndex != 0)
                {
                    materialColour = bvh.MaterialColours[material.PaletteIndex - 1];
                }
                float[] secondSurfaceDisplacements = null;
                if (bvh.SecondSurfaceDisplacements.Length > 0)
                {
                    secondSurfaceDisplacements = new float[3];
                    secondSurfaceDisplacements[0] = bvh.SecondSurfaceDisplacements[bvhTriangle.Index0];
                    secondSurfaceDisplacements[1] = bvh.SecondSurfaceDisplacements[bvhTriangle.Index1];
                    secondSurfaceDisplacements[2] = bvh.SecondSurfaceDisplacements[bvhTriangle.Index2];
                }
                Vector3f[] shrunkVerts = null;
                if (bvh.ShrunkVerts.Length > 0)
                {
                    shrunkVerts = new Vector3f[3];
                    shrunkVerts[0] = bvh.ShrunkVerts[bvhTriangle.Index0];
                    shrunkVerts[1] = bvh.ShrunkVerts[bvhTriangle.Index1];
                    shrunkVerts[2] = bvh.ShrunkVerts[bvhTriangle.Index2];
                }
                VertexData vert0 = new VertexData(bvh.Verts[bvhTriangle.Index0].Position);
                VertexData vert1 = new VertexData(bvh.Verts[bvhTriangle.Index1].Position);
                VertexData vert2 = new VertexData(bvh.Verts[bvhTriangle.Index2].Position);

                if (bvh.NumPerVertAttributes > 0)
                {
                    vert0.AttributeData = bvh.Verts[bvhTriangle.Index0].AttributeData;
                    vert1.AttributeData = bvh.Verts[bvhTriangle.Index1].AttributeData;
                    vert2.AttributeData = bvh.Verts[bvhTriangle.Index2].AttributeData;
                }

                return new Triangle(
                    collisionType,
                    materialDefinition,
                    materialColour,
                    secondSurfaceDisplacements,
                    shrunkVerts,
                    vert0,
                    vert1,
                    vert2,
                    bvh.NumPerVertAttributes);
            }
            else if (bvhPrimitive is BVHBox)
            {
                BVHBox bvhBox = (BVHBox)bvhPrimitive;
                string materialDefinition = bvh.Materials[bvhBox.MaterialIndex];
                BVHMaterial material = new BVHMaterial(materialDefinition);
                Vector4i materialColour = null;
                if (material.PaletteIndex != 0)
                {
                    materialColour = bvh.MaterialColours[material.PaletteIndex - 1];
                }
                float[] secondSurfaceDisplacements = null;
                if (bvh.SecondSurfaceDisplacements.Length > 0)
                {
                    secondSurfaceDisplacements = new float[4];
                    secondSurfaceDisplacements[0] = bvh.SecondSurfaceDisplacements[bvhBox.Index0];
                    secondSurfaceDisplacements[1] = bvh.SecondSurfaceDisplacements[bvhBox.Index1];
                    secondSurfaceDisplacements[2] = bvh.SecondSurfaceDisplacements[bvhBox.Index2];
                    secondSurfaceDisplacements[3] = bvh.SecondSurfaceDisplacements[bvhBox.Index3];
                }
                Vector3f[] shrunkVerts = null;
                if (bvh.ShrunkVerts.Length > 0)
                {
                    shrunkVerts = new Vector3f[4];
                    shrunkVerts[0] = bvh.ShrunkVerts[bvhBox.Index0];
                    shrunkVerts[1] = bvh.ShrunkVerts[bvhBox.Index1];
                    shrunkVerts[2] = bvh.ShrunkVerts[bvhBox.Index2];
                    shrunkVerts[3] = bvh.ShrunkVerts[bvhBox.Index3];
                }
                VertexData vert0 = new VertexData(bvh.Verts[bvhBox.Index0].Position);
                VertexData vert1 = new VertexData(bvh.Verts[bvhBox.Index1].Position);
                VertexData vert2 = new VertexData(bvh.Verts[bvhBox.Index2].Position);
                VertexData vert3 = new VertexData(bvh.Verts[bvhBox.Index3].Position);

                if (bvh.NumPerVertAttributes > 0)
                {
                    vert0.AttributeData = bvh.Verts[bvhBox.Index0].AttributeData;
                    vert1.AttributeData = bvh.Verts[bvhBox.Index1].AttributeData;
                    vert2.AttributeData = bvh.Verts[bvhBox.Index2].AttributeData;
                    vert3.AttributeData = bvh.Verts[bvhBox.Index3].AttributeData;
                }

                return new Box(
                    collisionType,
                    materialDefinition,
                    materialColour,
                    secondSurfaceDisplacements,
                    shrunkVerts,
                    vert0,
                    vert1,
                    vert2,
                    vert3,
                    bvh.NumPerVertAttributes);
            }
            else if (bvhPrimitive is BVHCapsule)
            {
                BVHCapsule bvhCapsule = (BVHCapsule)bvhPrimitive;
                string materialDefinition = bvh.Materials[bvhCapsule.MaterialIndex];
                BVHMaterial material = new BVHMaterial(materialDefinition);
                Vector4i materialColour = null;
                if (material.PaletteIndex != 0)
                {
                    materialColour = bvh.MaterialColours[material.PaletteIndex - 1];
                }
                float[] secondSurfaceDisplacements = null;
                if (bvh.SecondSurfaceDisplacements.Length > 0)
                {
                    secondSurfaceDisplacements = new float[2];
                    secondSurfaceDisplacements[0] = bvh.SecondSurfaceDisplacements[bvhCapsule.EndIndex0];
                    secondSurfaceDisplacements[1] = bvh.SecondSurfaceDisplacements[bvhCapsule.EndIndex1];
                }
                Vector3f[] shrunkVerts = null;
                if (bvh.ShrunkVerts.Length > 0)
                {
                    shrunkVerts = new Vector3f[2];
                    shrunkVerts[0] = bvh.ShrunkVerts[bvhCapsule.EndIndex0];
                    shrunkVerts[1] = bvh.ShrunkVerts[bvhCapsule.EndIndex1];
                }
                VertexData vert0 = new VertexData(bvh.Verts[bvhCapsule.EndIndex0].Position);
                VertexData vert1 = new VertexData(bvh.Verts[bvhCapsule.EndIndex1].Position);

                if (bvh.NumPerVertAttributes > 0)
                {
                    vert0.AttributeData = bvh.Verts[bvhCapsule.EndIndex0].AttributeData;
                    vert1.AttributeData = bvh.Verts[bvhCapsule.EndIndex1].AttributeData;
                }

                return new Capsule(
                    collisionType,
                    materialDefinition,
                    materialColour,
                    secondSurfaceDisplacements,
                    shrunkVerts,
                    vert0,
                    vert1,
                    bvhCapsule.Radius,
                    bvh.NumPerVertAttributes);
            }
            else if (bvhPrimitive is BVHCylinder)
            {
                BVHCylinder bvhCylinder = (BVHCylinder)bvhPrimitive;
                string materialDefinition = bvh.Materials[bvhCylinder.MaterialIndex];
                BVHMaterial material = new BVHMaterial(materialDefinition);
                Vector4i materialColour = null;
                if (material.PaletteIndex != 0)
                {
                    materialColour = bvh.MaterialColours[material.PaletteIndex - 1];
                }
                float[] secondSurfaceDisplacements = null;
                if (bvh.SecondSurfaceDisplacements.Length > 0)
                {
                    secondSurfaceDisplacements = new float[2];
                    secondSurfaceDisplacements[0] = bvh.SecondSurfaceDisplacements[bvhCylinder.EndIndex0];
                    secondSurfaceDisplacements[1] = bvh.SecondSurfaceDisplacements[bvhCylinder.EndIndex1];
                }
                Vector3f[] shrunkVerts = null;
                if (bvh.ShrunkVerts.Length > 0)
                {
                    shrunkVerts = new Vector3f[2];
                    shrunkVerts[0] = bvh.ShrunkVerts[bvhCylinder.EndIndex0];
                    shrunkVerts[1] = bvh.ShrunkVerts[bvhCylinder.EndIndex1];
                }
                VertexData vert0 = new VertexData(bvh.Verts[bvhCylinder.EndIndex0].Position);
                VertexData vert1 = new VertexData(bvh.Verts[bvhCylinder.EndIndex1].Position);

                if (bvh.NumPerVertAttributes > 0)
                {
                    vert0.AttributeData = bvh.Verts[bvhCylinder.EndIndex0].AttributeData;
                    vert1.AttributeData = bvh.Verts[bvhCylinder.EndIndex1].AttributeData;
                }

                return new Cylinder(
                    collisionType,
                    materialDefinition,
                    materialColour,
                    secondSurfaceDisplacements,
                    shrunkVerts,
                    vert0,
                    vert1,
                    bvhCylinder.Radius,
                    bvh.NumPerVertAttributes);
            }
            else if (bvhPrimitive is BVHQuad)
            {
                BVHQuad bvhQuad = (BVHQuad)bvhPrimitive;
                string materialDefinition = bvh.Materials[bvhQuad.MaterialIndex];
                BVHMaterial material = new BVHMaterial(materialDefinition);
                Vector4i materialColour = null;
                if (material.PaletteIndex != 0)
                {
                    materialColour = bvh.MaterialColours[material.PaletteIndex - 1];
                }
                float[] secondSurfaceDisplacements = null;
                if (bvh.SecondSurfaceDisplacements.Length > 0)
                {
                    secondSurfaceDisplacements = new float[4];
                    secondSurfaceDisplacements[0] = bvh.SecondSurfaceDisplacements[bvhQuad.Index0];
                    secondSurfaceDisplacements[1] = bvh.SecondSurfaceDisplacements[bvhQuad.Index1];
                    secondSurfaceDisplacements[2] = bvh.SecondSurfaceDisplacements[bvhQuad.Index2];
                    secondSurfaceDisplacements[3] = bvh.SecondSurfaceDisplacements[bvhQuad.Index3];
                }
                Vector3f[] shrunkVerts = null;
                if (bvh.ShrunkVerts.Length > 0)
                {
                    shrunkVerts = new Vector3f[4];
                    shrunkVerts[0] = bvh.ShrunkVerts[bvhQuad.Index0];
                    shrunkVerts[1] = bvh.ShrunkVerts[bvhQuad.Index1];
                    shrunkVerts[2] = bvh.ShrunkVerts[bvhQuad.Index2];
                    shrunkVerts[3] = bvh.ShrunkVerts[bvhQuad.Index3];
                }
                VertexData vert0 = new VertexData(bvh.Verts[bvhQuad.Index0].Position);
                VertexData vert1 = new VertexData(bvh.Verts[bvhQuad.Index1].Position);
                VertexData vert2 = new VertexData(bvh.Verts[bvhQuad.Index2].Position);
                VertexData vert3 = new VertexData(bvh.Verts[bvhQuad.Index3].Position);

                if (bvh.NumPerVertAttributes > 0)
                {
                    vert0.AttributeData = bvh.Verts[bvhQuad.Index0].AttributeData;
                    vert1.AttributeData = bvh.Verts[bvhQuad.Index1].AttributeData;
                    vert2.AttributeData = bvh.Verts[bvhQuad.Index2].AttributeData;
                    vert3.AttributeData = bvh.Verts[bvhQuad.Index3].AttributeData;
                }

                return new Quad(
                    collisionType,
                    materialDefinition,
                    materialColour,
                    secondSurfaceDisplacements,
                    shrunkVerts,
                    vert0,
                    vert1,
                    vert2,
                    vert3,
                    bvh.NumPerVertAttributes);
            }
            else if (bvhPrimitive is BVHSphere)
            {
                BVHSphere bvhSphere = (BVHSphere)bvhPrimitive;
                string materialDefinition = bvh.Materials[bvhSphere.MaterialIndex];
                BVHMaterial material = new BVHMaterial(materialDefinition);
                Vector4i materialColour = null;
                if (material.PaletteIndex != 0)
                {
                    materialColour = bvh.MaterialColours[material.PaletteIndex - 1];
                }
                float[] secondSurfaceDisplacements = null;
                if (bvh.SecondSurfaceDisplacements.Length > 0)
                {
                    secondSurfaceDisplacements = new float[1];
                    secondSurfaceDisplacements[0] = bvh.SecondSurfaceDisplacements[bvhSphere.CentreIndex];
                }
                Vector3f[] shrunkVerts = null;
                if (bvh.ShrunkVerts.Length > 0)
                {
                    shrunkVerts = new Vector3f[1];
                    shrunkVerts[0] = bvh.ShrunkVerts[bvhSphere.CentreIndex];
                }
                VertexData vert0 = new VertexData(bvh.Verts[bvhSphere.CentreIndex].Position);

                if (bvh.NumPerVertAttributes > 0)
                {
                    vert0.AttributeData = bvh.Verts[bvhSphere.CentreIndex].AttributeData;
                }

                return new Sphere(
                    collisionType,
                    materialDefinition,
                    materialColour,
                    secondSurfaceDisplacements,
                    shrunkVerts,
                    vert0,
                    bvhSphere.Radius,
                    bvh.NumPerVertAttributes);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        internal static CompositeChild ToCompositeChildBox(OrphanedData.Box orphanedBox)
        {
            Vector3f twiceBoxX = orphanedBox.Vert0.Position + orphanedBox.Vert2.Position - orphanedBox.Vert1.Position - orphanedBox.Vert3.Position;
            Vector3f twiceBoxY = orphanedBox.Vert0.Position + orphanedBox.Vert1.Position - orphanedBox.Vert2.Position - orphanedBox.Vert3.Position;
            Vector3f twiceBoxZ = orphanedBox.Vert1.Position + orphanedBox.Vert2.Position - orphanedBox.Vert0.Position - orphanedBox.Vert3.Position;

            Vector3f boxXAxis = Vector3f.Normalise(twiceBoxX);
            Vector3f boxYAxis = Vector3f.Normalise(twiceBoxY);
            Vector3f boxZAxis = Vector3f.Cross(boxXAxis, boxYAxis);

            Vector3f boxCentre = (orphanedBox.Vert0.Position + orphanedBox.Vert1.Position + orphanedBox.Vert2.Position + orphanedBox.Vert3.Position) * 0.25f;

            Matrix34f boxToWorldTransformMatrix = new Matrix34f(boxXAxis, boxYAxis, boxZAxis, boxCentre);
            Vector3f boxSize = new Vector3f((float)twiceBoxX.Magnitude() * 0.5f, (float)twiceBoxY.Magnitude() * 0.5f, (float)twiceBoxZ.Magnitude() * 0.5f);
            Vector3f centroid = new Vector3f(0.0f, 0.0f, 0.0f);
            string[] materials = new string[1] { orphanedBox.Material };

            RSG.Bounds.Box box = new RSG.Bounds.Box(boxSize, centroid, materials);

            return new CompositeChild(orphanedBox.CollisionType, box, boxToWorldTransformMatrix);
        }

        internal static CompositeChild ToCompositeChildCapsule(OrphanedData.Capsule orphanedCapsule)
        {
            Vector3f capsuleDirection = orphanedCapsule.Vert1.Position - orphanedCapsule.Vert0.Position;
            float length = (float)capsuleDirection.Magnitude();
            Vector3f capsuleCentre = orphanedCapsule.Vert0.Position + (capsuleDirection * 0.5f);
            Vector3f capsuleUnitDirection = Vector3f.Normalise(capsuleDirection);

            Vector3f centroid = new Vector3f(0.0f, 0.0f, 0.0f);
            string[] materials = new string[1] { orphanedCapsule.Material };
            RSG.Bounds.Capsule capsule = new RSG.Bounds.Capsule(
                length,
                orphanedCapsule.Radius,
                centroid,
                materials);

            Vector3f cardinalXAxis = new Vector3f(1.0f, 0.0f, 0.0f);
            Vector3f cardinalZAxis = new Vector3f(0.0f, 0.0f, 1.0f);

            Vector3f capsuleXAxis;
            Vector3f capsuleZAxis;
            float dotYZ = Vector3f.Dot(capsuleUnitDirection, cardinalZAxis);
            if (dotYZ < -0.7071f || dotYZ > 0.7071)
            {
                capsuleZAxis = Vector3f.Cross(cardinalXAxis, capsuleUnitDirection);
                capsuleXAxis = Vector3f.Cross(capsuleUnitDirection, capsuleZAxis);
            }
            else
            {
                capsuleXAxis = Vector3f.Cross(capsuleUnitDirection, cardinalZAxis);
                capsuleZAxis = Vector3f.Cross(capsuleXAxis, capsuleUnitDirection);
            }

            capsuleXAxis.Normalise();
            capsuleZAxis.Normalise();

            Matrix34f capsuleToWorldTransformMatrix = new Matrix34f(
                capsuleXAxis,
                capsuleUnitDirection,
                capsuleZAxis,
                capsuleCentre);
            return new CompositeChild(orphanedCapsule.CollisionType, capsule, capsuleToWorldTransformMatrix);
        }

        internal static CompositeChild ToCompositeChildCylinder(OrphanedData.Cylinder orphanedCylinder)
        {
            Vector3f cylinderDirection = orphanedCylinder.Vert1.Position - orphanedCylinder.Vert0.Position;
            float length = (float)cylinderDirection.Magnitude();
            Vector3f cylinderCentre = orphanedCylinder.Vert0.Position + (cylinderDirection * 0.5f);
            Vector3f cylinderUnitDirection = Vector3f.Normalise(cylinderDirection);

            Vector3f centroid = new Vector3f(0.0f, 0.0f, 0.0f);
            string[] materials = new string[1] { orphanedCylinder.Material };
            RSG.Bounds.Cylinder cylinder = new RSG.Bounds.Cylinder(
                RSG.ManagedRage.PhysicsConstants.ConvexDistanceMargin,
                length,
                orphanedCylinder.Radius,
                centroid, 
                materials);

            Vector3f cardinalXAxis = new Vector3f(1.0f, 0.0f, 0.0f);
            Vector3f cardinalZAxis = new Vector3f(0.0f, 0.0f, 1.0f);

            Vector3f cylinderXAxis;
            Vector3f cylinderZAxis;
            float dotYZ = Vector3f.Dot(cylinderUnitDirection, cardinalZAxis);
            if (dotYZ < -0.7071f || dotYZ > 0.7071)
            {
                cylinderZAxis = Vector3f.Cross(cardinalXAxis, cylinderUnitDirection);
                cylinderXAxis = Vector3f.Cross(cylinderUnitDirection, cylinderZAxis);
            }
            else
            {
                cylinderXAxis = Vector3f.Cross(cylinderUnitDirection, cardinalZAxis);
                cylinderZAxis = Vector3f.Cross(cylinderXAxis, cylinderUnitDirection);
            }

            cylinderXAxis.Normalise();
            cylinderZAxis.Normalise();

            Matrix34f cylinderToWorldTransformMatrix = new Matrix34f(
                cylinderXAxis,
                cylinderUnitDirection,
                cylinderZAxis,
                cylinderCentre);
            return new CompositeChild(orphanedCylinder.CollisionType, cylinder, cylinderToWorldTransformMatrix);
        }

        internal static CompositeChild ToCompositeChildSphere(OrphanedData.Sphere orphanedSphere)
        {
            Vector3f centroid = new Vector3f(0.0f, 0.0f, 0.0f);
            string[] materials = new string[1] { orphanedSphere.Material };
            RSG.Bounds.Sphere sphere = new RSG.Bounds.Sphere(orphanedSphere.Radius, centroid, materials);

            Matrix34f sphereToWorldTransformMatrix = new Matrix34f(
                new Vector3f(1.0f, 0.0f, 0.0f),
                new Vector3f(0.0f, 1.0f, 0.0f),
                new Vector3f(0.0f, 0.0f, 1.0f),
                orphanedSphere.Vert0.Position);
            return new CompositeChild(orphanedSphere.CollisionType, sphere, sphereToWorldTransformMatrix);
        }

        // B* 1026951 - an enumeration of triangles can => a number of composite children
        internal static IEnumerable<CompositeChild> ToCompositeChildGeometries(IEnumerable<OrphanedData.Triangle> orphanedTriangles)
        {
            // We build a Geometry for each collision type we encounter
            Dictionary<BNDFile.CollisionType, BVHBuilder.BVHBuilder> collisionTypeBuilderMap = new Dictionary<BNDFile.CollisionType, BVHBuilder.BVHBuilder>();
            
            // Push each triangle into our map
            foreach(OrphanedData.Triangle orphanedTriangle in orphanedTriangles)
            {
                if (!collisionTypeBuilderMap.ContainsKey(orphanedTriangle.CollisionType))
                    collisionTypeBuilderMap.Add(orphanedTriangle.CollisionType, new BVHBuilder.BVHBuilder(orphanedTriangle.NumPerVertAttribs));

                collisionTypeBuilderMap[orphanedTriangle.CollisionType].Append(orphanedTriangle);
            }

            // Translate our BVHBuilder objects into CompositeChild objects, as required
            List<CompositeChild> compositeChildGeometries = new List<CompositeChild>();
            foreach (KeyValuePair<BNDFile.CollisionType, BVHBuilder.BVHBuilder> collisionTypeBuilderPair in collisionTypeBuilderMap)
                compositeChildGeometries.Add(new CompositeChild(collisionTypeBuilderPair.Key, collisionTypeBuilderPair.Value.ToGeometry(), new Matrix34f()));

            return compositeChildGeometries;
        }
    }
}
