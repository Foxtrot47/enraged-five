﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Bounds;

namespace RSG.Pipeline.BoundsProcessor
{
    internal static class ParallelCompositeGenerator
    {
        internal static OrphanedData.PrimitiveCollection[] DataIn = null;
        internal static Composite[] DataOut = null;
        internal static IndexedBounds.SerialisationOptions SerialisationOptions = IndexedBounds.SerialisationOptions.AsBVH;

        internal static void Generate(int index)
        {
            if (DataIn[index].IsHighDetail)
                DataOut[index] = DataIn[index].ToComposite(IndexedBounds.SerialisationOptions.AsBVH, Config.TheConfig.Options.MaxHighDetailMapBVHDataSize);
            else
                DataOut[index] = DataIn[index].ToComposite(IndexedBounds.SerialisationOptions.AsBVH, Config.TheConfig.Options.MaxStandardMapBVHDataSize);
        }
    }
}
