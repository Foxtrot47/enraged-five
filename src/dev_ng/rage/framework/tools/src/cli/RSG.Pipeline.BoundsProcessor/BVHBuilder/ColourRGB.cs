﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.BoundsProcessor.BVHBuilder
{
    internal class ColourRGB
    {
        internal ColourRGB()
        {
            Red = 0;
            Green = 0;
            Blue = 0;
        }

        internal ColourRGB(int red, int green, int blue)
        {
            Red = (ulong)red;
            Green = (ulong)green;
            Blue = (ulong)blue;
        }

        internal ulong Red { get; set; }
        internal ulong Green { get; set; }
        internal ulong Blue { get; set; }
    }
}
