﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.BoundsProcessor.Manifest
{
    internal class ManifestDataBuilder
    {
        internal ManifestDataBuilder()
        {
            manifestItems_ = new List<IManifestItem>();
        }

        internal void Add(IManifestItem item)
        {
            manifestItems_.Add(item);
        }

        internal void Save(String pathname)
        {
            List<XElement> iplGroupElements = new List<XElement>();

            XDocument metadataDocument = new XDocument(
                new XElement("ManifestData", 
                    manifestItems_.Select(interiorBoundsMapping => interiorBoundsMapping.ToXElement())));

            String directory = Path.GetDirectoryName(pathname);
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            metadataDocument.Save(pathname);
        }

        private List<IManifestItem> manifestItems_;
    }
}
