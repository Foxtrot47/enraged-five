﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Profiling;

namespace RSG.Pipeline.BoundsProcessor
{
    internal static class Profiling
    {
        internal static IProfiler TheProfiler { get; set; }
    }
}
