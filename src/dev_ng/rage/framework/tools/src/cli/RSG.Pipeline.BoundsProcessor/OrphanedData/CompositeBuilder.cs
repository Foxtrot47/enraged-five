﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;
using RSG.Bounds;

namespace RSG.Pipeline.BoundsProcessor.OrphanedData
{
    // TODO - At some point this needs to go into RSG.Bounds along with everything in BVHBuilder and OrphanedData

    /// <summary>
    /// Class responsible for building a single composite from a collection of Primitive objects
    /// </summary>
    internal class CompositeBuilder
    {
        internal CompositeBuilder(int maxBVHDataSize)
        {
            maxBVHDataSize_ = maxBVHDataSize;
            primitiveBucketMap_ = new Dictionary<BVHGeometryType, List<Primitive>>();
        }

        internal CompositeBuilder(int maxBVHDataSize, IEnumerable<Primitive> primitives)
            : this(maxBVHDataSize)
        {
            AddRange(primitives);
        }

        internal void Add(Primitive primitive)
        {
            BVHGeometryType geometryType = new BVHGeometryType(primitive.CollisionType,
                                                               primitive.MaterialColour != null,
                                                               primitive.SecondSurfaceDisplacements != null,
                                                               primitive.ShrunkVerts != null,
                                                               primitive.NumPerVertAttribs);

            if (!primitiveBucketMap_.ContainsKey(geometryType))
            {
                primitiveBucketMap_.Add(geometryType, new List<Primitive>());
            }

            primitiveBucketMap_[geometryType].Add(primitive);
        }

        internal void AddRange(IEnumerable<Primitive> primitives)
        {
            foreach (Primitive primitive in primitives)
                Add(primitive);
        }

        internal Composite ToComposite()
        {
            List<CompositeChild> compositeBounds = new List<CompositeChild>();

            // First build a collection of BVH builders containing data to be processed
            List<Tuple<BNDFile.CollisionType, BVHBuilder.BVHBuilder>> bvhBuilderPairs = new List<Tuple<BNDFile.CollisionType, BVHBuilder.BVHBuilder>>();
            foreach (KeyValuePair<BVHGeometryType, List<Primitive>> primitiveBucketPair in primitiveBucketMap_)
            {
                int estimatedSize = primitiveBucketPair.Value.Sum(prim => prim.EstimatedSize);

                if (estimatedSize <= maxBVHDataSize_)
                {
                    BVHBuilder.BVHBuilder bvhBuilder = new BVHBuilder.BVHBuilder(primitiveBucketPair.Key.NumPerVertAttributes);
                    bvhBuilder.AppendRange(primitiveBucketPair.Value);
                    bvhBuilderPairs.Add(new Tuple<BNDFile.CollisionType, BVHBuilder.BVHBuilder>(primitiveBucketPair.Key.CollisionType, bvhBuilder));
                }
                else
                {
                    // This blocks breaks the limit so we need to split it up further
                    BVHSplitter bvhSplitter = new BVHSplitter(maxBVHDataSize_, primitiveBucketPair.Value);
                    bvhSplitter.Split(primitiveBucketPair.Key.CollisionType);
                    foreach(IEnumerable<Primitive> primitiveArray in bvhSplitter.SplitPrimitives)
                    {
                        BVHBuilder.BVHBuilder bvhBuilder = new BVHBuilder.BVHBuilder(primitiveBucketPair.Key.NumPerVertAttributes);
                        bvhBuilder.AppendRange(primitiveArray);
                        bvhBuilderPairs.Add(new Tuple<BNDFile.CollisionType, BVHBuilder.BVHBuilder>(primitiveBucketPair.Key.CollisionType, bvhBuilder));
                    }
                }
            }

            // Second, process 'em
            foreach (Tuple<BNDFile.CollisionType, BVHBuilder.BVHBuilder> bvhBuilderPair in bvhBuilderPairs)
            {
                BVH bvh = bvhBuilderPair.Item2.ToBVH();
                CompositeChild newBound = new CompositeChild(bvhBuilderPair.Item1, bvh, Matrix34f.Identity);
                compositeBounds.Add(newBound);
            }

            return new Composite(compositeBounds.ToArray());
        }

        private int maxBVHDataSize_;
        private Dictionary<BVHGeometryType, List<Primitive>> primitiveBucketMap_;
    }
}
