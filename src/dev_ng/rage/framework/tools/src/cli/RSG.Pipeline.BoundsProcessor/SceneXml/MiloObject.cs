﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;

namespace RSG.Pipeline.BoundsProcessor.SceneXml
{
    internal class MiloObject
    {
        internal MiloObject(string name, Matrix34f worldToMiloTransformMatrix, bool isNewDlc)
        {
            Name = name;
            WorldToMiloTransformMatrix = worldToMiloTransformMatrix;
            IsNewDLC = isNewDlc; 
        }

        internal string Name { get; private set; }
        internal Matrix34f WorldToMiloTransformMatrix { get; private set; }
        internal bool IsNewDLC { get; private set; }
    }
}
