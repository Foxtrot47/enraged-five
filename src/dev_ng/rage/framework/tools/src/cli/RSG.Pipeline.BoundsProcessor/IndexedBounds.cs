﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Logging;
using RSG.Base.Profiling;
using RSG.Base.Math;
using RSG.Bounds;

namespace RSG.Pipeline.BoundsProcessor
{
    /// <summary>
    /// Represents one chunk of indexed bounds (standard, high detail, an IPL group, etc)
    /// </summary>
    internal class IndexedBounds
    {
        internal enum SerialisationOptions
        {
            AsBVH,
            AsDiscretePrimitives
        }

        internal IndexedBounds(IndexedBounds.SerialisationOptions serialisationOptions)
        {
            bvhMap_ = new Dictionary<BNDFile.CollisionType, List<BVH>>();
            SerialisationOption = serialisationOptions;
        }

        internal void AddBVH(BNDFile.CollisionType collisionType, BVH bvh)
        {
            collisionType = CollisionTypePatcher.PatchCollisionType(collisionType);

            if (!bvhMap_.ContainsKey(collisionType))
            {
                bvhMap_.Add(collisionType, new List<BVH>());
            }

            bvhMap_[collisionType].Add(bvh);
        }

        internal void Transform(Matrix34f transformationMatrix)
        {
            foreach (KeyValuePair<BNDFile.CollisionType, List<BVH>> bvhPair in bvhMap_)
            {
                foreach (BVH bvh in bvhPair.Value)
                {
                    bvh.TransformVerts(transformationMatrix);
                }
            }
        }

        /// <summary>
        /// Process into a single BoundObject
        /// </summary>
        /// <returns></returns>
        internal BoundObject Process()
        {
            if (bvhMap_.Count == 0)
                throw new InvalidOperationException("Cannot convert indexed bounds into a composite if it is empty");

            // Break the geometry down into a primitive soup
            OrphanedData.PrimitiveCollection orphanedPrimitives = new OrphanedData.PrimitiveCollection();
            foreach (var bvhPair in bvhMap_)
            {
                foreach (var bvh in bvhPair.Value)
                {
                    foreach (var primitive in bvh.Primitives)
                    {
                        OrphanedData.Primitive orhpanedPrimitive = OrphanedData.PrimitiveConverter.FromBVHPrimitive(primitive, bvh, bvhPair.Key);
                        orphanedPrimitives.AddPrimitive(orhpanedPrimitive);
                    }
                }
            }

            orphanedPrimitives.CorrectSkewedBoxes();
            orphanedPrimitives.CheckForSlimBoxes();
            orphanedPrimitives.RemapCollisionTypes();

            Composite composite = orphanedPrimitives.ToComposite(SerialisationOption, Config.TheConfig.Options.MaxBVHDataSize);

            // Here we prevent the unnecessary serialisation of single bound objects with no rotation
            if (SerialisationOption == SerialisationOptions.AsDiscretePrimitives &&
                composite.Children.Length == 1)
            {
                bool hasUniformRotation = (composite.Children[0].BoundToParentTransform.A.X > 0.999f &&
                                           composite.Children[0].BoundToParentTransform.B.Y > 0.999f &&
                                           composite.Children[0].BoundToParentTransform.C.Z > 0.999f);

                if (hasUniformRotation && composite.Children[0].BoundObject is PrimitiveBoundObject)
                {
                    PrimitiveBoundObject primitiveBoundObject = (PrimitiveBoundObject)composite.Children[0].BoundObject;
                    primitiveBoundObject.Centroid = composite.Children[0].BoundToParentTransform.D;

                    return primitiveBoundObject;
                }
            }

            return composite;
        }

        internal OrphanedData.PrimitiveCollection[] ToPrimitiveCollection(float maxSizeXYPerSection, int maxCompositeSize)
        {
            if (bvhMap_.Count == 0)
                return new OrphanedData.PrimitiveCollection[0];

            // Break the geometry down into a primitive soup
            OrphanedData.PrimitiveCollection orphanedPrimitives = new OrphanedData.PrimitiveCollection();
            foreach (var bvhPair in bvhMap_)
            {
                foreach (var bvh in bvhPair.Value)
                {
                    foreach (var primitive in bvh.Primitives)
                    {
                        OrphanedData.Primitive orhpanedPrimitive = OrphanedData.PrimitiveConverter.FromBVHPrimitive(primitive, bvh, bvhPair.Key);
                        orphanedPrimitives.AddPrimitive(orhpanedPrimitive);
                    }
                }
            }

            orphanedPrimitives.CorrectSkewedBoxes();
            orphanedPrimitives.CheckForSlimBoxes();
            orphanedPrimitives.RemapCollisionTypes();

            // Split the primitive soup based on physical limits
            orphanedPrimitives.SplitOnPhysicalXYLimit(maxSizeXYPerSection);

            // Split the primitive soup based on primitive limits
            orphanedPrimitives.SplitOnPrimitiveCountLimitRecursive(maxCompositeSize);

            // Merge small 'uns with their nearest big 'un
            orphanedPrimitives.MergeSmallSectionsWithLargerNeighbours();

            // Now we have split the geometry how we want it, we have to convert it back into BVH data.  Each leaf becomes a composite made up of one or more BVH
            OrphanedData.PrimitiveCollection[] leafNodes = orphanedPrimitives.GetAllLeafNodes();

            return leafNodes;
        }

        internal float Volume
        {
            get
            {
                BoundingBox3f boundingBox = new BoundingBox3f();

                foreach (List<BVH> bvhList in bvhMap_.Values)
                {
                    foreach (BVH bvh in bvhList)
                    {
                        foreach(BVHPrimitive primitive in bvh.Primitives)
                        {
                            boundingBox.Expand(primitive.GetBoundingBox(bvh.Verts.Select(v => v.Position).ToArray()));
                        }
                    }
                }

                return boundingBox.Volume();
            }
        }

        internal int NumPrimitives
        {
            get
            {
                int numPrimitives = 0;
                foreach (List<BVH> bvhList in bvhMap_.Values)
                {
                    foreach (BVH bvh in bvhList)
                    {
                        numPrimitives += bvh.Primitives.Length;
                    }
                }

                return numPrimitives;
            }
        }

        internal SerialisationOptions SerialisationOption { get; set; }

        private Dictionary<BNDFile.CollisionType, List<BVH>> bvhMap_;
    }
}
