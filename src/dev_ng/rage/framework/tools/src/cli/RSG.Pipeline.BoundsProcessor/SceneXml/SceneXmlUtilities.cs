﻿using System;
using System.Collections.Generic;
using System.Linq;
using RSG.Base.Math;
using RSG.Bounds;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.SceneXml;
using RSG.SceneXml.MapExport;
using RSG.SceneXml.MapExport.Project.GTA5_NG;

namespace RSG.Pipeline.BoundsProcessor.SceneXml
{
    internal static class SceneXmlUtilities
    {
        internal static CollisionData GetCollisionData(IEnumerable<Scene> scenes, IEnumerable<IMAPFile> imapFiles, ContentTreeHelper contentTreeHelper)
        {
            List<CollisionObject> collisionObjects = new List<CollisionObject>();
            List<RefObject> rsRefObjectsToBake = new List<RefObject>();
            List<MiloObject> miloObjects = new List<MiloObject>();

            foreach (Scene scene in scenes)
            {
                string sceneFilename = scene.Filename;

                foreach (TargetObjectDef objectDef in scene.Objects)
                {
                    ProcessObjectDefRecursive(sceneFilename, objectDef, collisionObjects, rsRefObjectsToBake, miloObjects, contentTreeHelper);
                }
            }

            foreach(IMAPFile imapFile in imapFiles)
            {
                foreach (RSG.SceneXml.MapExport.Entity entity in imapFile.Entities)
                {
                    ProcessMapEntity(entity, rsRefObjectsToBake, contentTreeHelper);
                }
            }

            // Now we order the CollisionObjects based on their CompositeOrder, which ensures they get processed in the right order
            IEnumerable<CollisionObject> orderedCollisionObjects = collisionObjects.OrderBy(collisionObject => collisionObject.CompositeOrder);

            return new CollisionData(orderedCollisionObjects, rsRefObjectsToBake, miloObjects);
        }
        
        /// <summary>
        /// Processes a map entity generated from an external utility (e.g. Instance Placement Processor).
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="rsRefObjectsToBake"></param>
        /// <param name="configGameView"></param>
        private static void ProcessMapEntity(RSG.SceneXml.MapExport.Entity entity, 
            ICollection<RefObject> rsRefObjectsToBake,
            ContentTreeHelper contentTreeHelper)
        {
            TargetObjectDef objectDef = entity.Object;
            string objectNameLower = objectDef.Name.ToLower();

            // Lookup the scene xml and export zip pathnames
            string collisionGroupName = null;
            string refObjectDefCollisionGroup = objectDef.GetAttribute(AttrNames.OBJ_COLLISION_GROUP, AttrDefaults.OBJ_COLLISION_GROUP);
            if (!string.IsNullOrWhiteSpace(refObjectDefCollisionGroup) && refObjectDefCollisionGroup != AttrDefaults.OBJ_COLLISION_GROUP)
            {
                collisionGroupName = refObjectDefCollisionGroup;
            }

            IContentNode sceneXmlNode = contentTreeHelper.CreateFile(objectDef.MyScene.Filename);
            IContentNode exportZipNode = contentTreeHelper.GetExportZipNodeFromSceneXmlNode(sceneXmlNode);
            IContentNode processedZipNode = contentTreeHelper.GetProcessedZipNodeFromExportZipNode(exportZipNode);
            String exportZipPathname = ((RSG.Pipeline.Content.File)exportZipNode).AbsolutePath;
            String processedZipPathname = ((RSG.Pipeline.Content.File)processedZipNode).AbsolutePath;

            string iplGroup = objectDef.GetAttribute(AttrNames.OBJ_IPL_GROUP, "").ToLower(); 
            if (!String.IsNullOrEmpty(iplGroup))
            {
                // Ensure group name is prefixed if required.
                iplGroup = MapAsset.AppendMapPrefixIfRequired(contentTreeHelper.ContentTree.Branch.Project, iplGroup);
            }
            string miloParentName = GetMiloParentName(objectDef);

            bool isDynamic = objectDef.IsDynObject();
            bool useFullMatrix = objectDef.GetAttribute(AttrNames.OBJ_USE_FULL_MATRIX, AttrDefaults.OBJ_USE_FULL_MATRIX);
            bool isNewDlc = objectDef.IsNewDLCObject();

            rsRefObjectsToBake.Add(
                new RefObject(
                    objectNameLower,
                    objectDef.Name,
                    isDynamic,
                    exportZipPathname,
                    processedZipPathname,
                    entity.ScaleOverride,
                    entity.WorldTransformOverride,
                    useFullMatrix,
                    collisionGroupName,
                    iplGroup,
                    miloParentName,
                    isNewDlc));
        }

        private static void ProcessObjectDefRecursive(string sceneFilename, 
                                                      TargetObjectDef objectDef,
                                                      ICollection<CollisionObject> collisionObjects, 
                                                      ICollection<RefObject> rsRefObjectsToBake,
                                                      ICollection<MiloObject> miloObjects,
                                                      ContentTreeHelper contentTreeHelper)
        {
            if (objectDef.IsEntity() && 
                !objectDef.DontExport() && 
                objectDef.WillHaveCollisionBaked() && 
                (objectDef.IsRefObject() || objectDef.IsXRef()))
            {
                bool okToContinue = true;

                if (objectDef.RefObject == null)
                {
                    RSG.Base.Logging.Log.Log__WarningCtx(objectDef.Name, "External reference '{0}' for object '{1}' is unresolved for baking collision.", objectDef.RefName, objectDef.Name);
                    okToContinue = false;
                }

                if (okToContinue)
                {
                    string objectNameLower = objectDef.Name.ToLower();

                    // Lookup the scene xml and export zip pathnames
                    string collisionGroupName = null;
                    string refObjectDefCollisionGroup = objectDef.RefObject.GetAttribute(AttrNames.OBJ_COLLISION_GROUP, AttrDefaults.OBJ_COLLISION_GROUP);
                    if (!string.IsNullOrWhiteSpace(refObjectDefCollisionGroup) && refObjectDefCollisionGroup != AttrDefaults.OBJ_COLLISION_GROUP)
                    {
                        collisionGroupName = refObjectDefCollisionGroup;
                    }

                    IContentNode sceneXmlNode = contentTreeHelper.CreateFile(objectDef.RefObject.MyScene.Filename);
                    IContentNode exportZipNode = contentTreeHelper.GetExportZipNodeFromSceneXmlNode(sceneXmlNode);
                    IContentNode processedZipNode = contentTreeHelper.GetProcessedZipNodeFromExportZipNode(exportZipNode);
                    String exportZipPathname = ((RSG.Pipeline.Content.File)exportZipNode).AbsolutePath;
                    String processedZipPathname = ((RSG.Pipeline.Content.File)processedZipNode).AbsolutePath;

                    string iplGroup = objectDef.GetAttribute(AttrNames.OBJ_IPL_GROUP, "").ToLower(); 
                    if (!String.IsNullOrEmpty(iplGroup))
                    {
                        // Ensure group name is prefixed if required.
                        iplGroup = MapAsset.AppendMapPrefixIfRequired(contentTreeHelper.ContentTree.Branch.Project, iplGroup);
                    }
                    string miloParentName = GetMiloParentName(objectDef);

                    bool isDynamic = objectDef.RefObject.IsDynObject();
                    bool useFullMatrix = objectDef.GetAttribute(AttrNames.OBJ_USE_FULL_MATRIX, AttrDefaults.OBJ_USE_FULL_MATRIX);
                    bool isNewDlc = objectDef.IsNewDLCObject();

                    rsRefObjectsToBake.Add(
                        new RefObject(
                            objectNameLower,
                            objectDef.RefName,
                            isDynamic,
                            exportZipPathname,
                            processedZipPathname,
                            objectDef.NodeScale,
                            objectDef.NodeTransform,
                            useFullMatrix,
                            collisionGroupName,
                            iplGroup,
                            miloParentName,
                            isNewDlc));
                }
            }
            else if (objectDef.IsCollision())
            {
                string objectNameLower = objectDef.Name.ToLower();
                BNDFile.CollisionType collisionType = GetObjectRefCollisionType(objectDef);
                int compositeOrder = objectDef.GetAttribute(AttrNames.COLL_COMPOSTIE_ORDER, Int32.MaxValue);// Anything with an unspecified order should come last
                
                string parentName = "";
                bool parentIsObject = false;
                bool parentIsDynamic = false;
                bool parentIsFragment = false;
                bool parentIsPartOfAnEntitySet = false;
                bool parentIsPartOfLOD = false;
                bool isNewDLC = objectDef.IsNewDLCObject();
                string collisionGroup = "";
                string iplGroup = "";
                Matrix34f worldToParentTransformMatrix = new Matrix34f();
                
                if (objectDef.Parent != null)
                {
                    parentName = objectDef.Parent.Name.ToLower();
                    if (objectDef.Parent.IsObject())
                    {
                        parentIsObject = true;
                        parentIsDynamic = objectDef.Parent.GetAttribute(AttrNames.OBJ_IS_DYNAMIC, AttrDefaults.OBJ_IS_DYNAMIC);
                        parentIsFragment = IsPartOfFragmentHierarchy(objectDef);
                        parentIsPartOfAnEntitySet = objectDef.Parent.IsPartOfAnEntitySet();
                        parentIsPartOfLOD = objectDef.Parent.HasDrawableLODChildren();
                        collisionGroup = objectDef.Parent.GetAttribute(AttrNames.OBJ_COLLISION_GROUP, "").ToLower();
                        iplGroup = objectDef.Parent.GetAttribute(AttrNames.OBJ_IPL_GROUP, "").ToLower();
                        if (!String.IsNullOrEmpty(iplGroup))
                        {
                            // Ensure group name is prefixed if required.
                            iplGroup = MapAsset.AppendMapPrefixIfRequired(contentTreeHelper.ContentTree.Branch.Project, iplGroup);
                        }
                        isNewDLC |= objectDef.Parent.IsNewDLCObject();

                        Matrix34f objectToWorldTransformMatrix = objectDef.NodeTransform.Transpose();// Hack to fix scene XML bug
                        Matrix34f parentToWorldTransformMatrix = objectDef.Parent.NodeTransform.Transpose();// Hack to fix scene XML bug
                        worldToParentTransformMatrix = parentToWorldTransformMatrix.Inverse();
                    }
                }
                string miloParentName = GetMiloParentName(objectDef);

                CollisionObject newCollisionObject = new CollisionObject(
                        objectNameLower,
                        sceneFilename,
                        collisionType,
                        compositeOrder,
                        parentName,
                        parentIsObject,
                        parentIsDynamic,
                        parentIsFragment,
                        parentIsPartOfAnEntitySet,
                        parentIsPartOfLOD,
                        isNewDLC,
                        worldToParentTransformMatrix,
                        collisionGroup,
                        iplGroup,
                        miloParentName);
                collisionObjects.Add(newCollisionObject);
            }
            else if (objectDef.IsMilo())
            {
                RSG.Base.Configuration.IBranch branch = contentTreeHelper.ContentTree.Branch;
                string miloNameLower = objectDef.Name.ToLower();
                Matrix34f miloToWorldTransformMatrix = objectDef.NodeTransform.Transpose();// Hack to fix scene XML bug
                Matrix34f worldToMiloTransformMatrix = miloToWorldTransformMatrix.Inverse();
                bool isNewDlc = branch.Project.ForceFlags.AllNewContent || objectDef.IsNewDLCObject();
                miloObjects.Add(new MiloObject(miloNameLower, worldToMiloTransformMatrix, isNewDlc));
            }

            foreach (RSG.SceneXml.TargetObjectDef childObjectDef in objectDef.Children)
            {
                ProcessObjectDefRecursive(sceneFilename, childObjectDef, collisionObjects, rsRefObjectsToBake, miloObjects, contentTreeHelper);
            }
        }

        private static bool IsPartOfFragmentHierarchy(TargetObjectDef objectDef)
        {
            if (objectDef == null)
                return false;

            while (objectDef.Parent != null && objectDef.Parent.IsObject())
            {
                if (objectDef.Parent.IsFragment())
                    return true;
                else
                    objectDef = objectDef.Parent;
            }

            return false;
        }

        public static BNDFile.CollisionType GetObjectRefCollisionType(RSG.SceneXml.TargetObjectDef objectDef)
        {
            BNDFile.CollisionType result = 0;

            if (!objectDef.UserProperties.ContainsKey("weapons") || (string)objectDef.UserProperties["weapons"] == "true")
            {
                result |= BNDFile.CollisionType.Weapons;
            }
            if (!objectDef.UserProperties.ContainsKey("mover") || (string)objectDef.UserProperties["mover"] == "true")
            {
                result |= BNDFile.CollisionType.Mover;
            }
            if (objectDef.UserProperties.ContainsKey("river") && (string)objectDef.UserProperties["river"] == "true")
            {
                result |= BNDFile.CollisionType.River;
            }
            if (objectDef.UserProperties.ContainsKey("deep_material_surface") && (string)objectDef.UserProperties["deep_material_surface"] == "true")
            {
                result |= BNDFile.CollisionType.DeepMaterialSurface;
            }
            if (objectDef.UserProperties.ContainsKey("foliage") && (string)objectDef.UserProperties["foliage"] == "true")
            {
                result |= BNDFile.CollisionType.Foliage;
            }
            if (objectDef.UserProperties.ContainsKey("stair_slope") && (string)objectDef.UserProperties["stair_slope"] == "true")
            {
                result |= BNDFile.CollisionType.StairSlope;
            }
            if (objectDef.UserProperties.ContainsKey("material") && (string)objectDef.UserProperties["material"] == "true")
            {
                result |= BNDFile.CollisionType.MaterialOnly;
            }

            // OK, so the horse, cover and vehicle flags are a bit special.
            // They were added after others so to prevent migration issues they are only implied if 'mover' is on
            if (result.HasFlag(BNDFile.CollisionType.Mover))
            {
                if (!objectDef.UserProperties.ContainsKey("horse") || (string)objectDef.UserProperties["horse"] == "true")
                {
                    result |= BNDFile.CollisionType.Horse;
                }
                if (!objectDef.UserProperties.ContainsKey("cover") || (string)objectDef.UserProperties["cover"] == "true")
                {
                    result |= BNDFile.CollisionType.Cover;
                }
                if (!objectDef.UserProperties.ContainsKey("vehicle") || (string)objectDef.UserProperties["vehicle"] == "true")
                {
                    result |= BNDFile.CollisionType.Vehicle;
                }
            }
            else
            {
                if (objectDef.UserProperties.ContainsKey("horse") && (string)objectDef.UserProperties["horse"] == "true")
                {
                    result |= BNDFile.CollisionType.Horse;
                }
                if (objectDef.UserProperties.ContainsKey("cover") && (string)objectDef.UserProperties["cover"] == "true")
                {
                    result |= BNDFile.CollisionType.Cover;
                }
                if (objectDef.UserProperties.ContainsKey("vehicle") && (string)objectDef.UserProperties["vehicle"] == "true")
                {
                    result |= BNDFile.CollisionType.Vehicle;
                }
            }

            return result;
        }

        /// <summary>
        /// Traverse the scene to find the parent MILO if one exists, or return null
        /// </summary>
        /// <param name="objectDef"></param>
        /// <returns></returns>
        private static string GetMiloParentName(TargetObjectDef objectDef)
        {
            while (objectDef.HasParent())
            {
                objectDef = objectDef.Parent;
                if (objectDef.IsMilo())
                {
                    return objectDef.Name.ToLower();
                }
            }

            return null;
        }
    }
}
