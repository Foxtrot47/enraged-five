﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Xml.Linq;

using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Profiling;
using RSG.Bounds;
using RSG.SceneXml;
using RSG.SceneXml.MapExport;

namespace RSG.Pipeline.BoundsProcessor
{
    /// <summary>
    /// Represents the indexed bounds data for a map section
    /// </summary>
    internal class MapSectionBoundsProcessor
    {
        internal MapSectionBoundsProcessor(IProject project, string mapSectionName)
        {
            mapSectionName_ = mapSectionName;
            project_ = project;
            standardBounds_ = new IndexedBounds(IndexedBounds.SerialisationOptions.AsBVH);
            highDetailBounds_ = new IndexedBounds(IndexedBounds.SerialisationOptions.AsBVH);
            materialBounds_ = new IndexedBounds(IndexedBounds.SerialisationOptions.AsBVH);
            imapGroups_ = new SortedSet<string>();
            imapStandardBoundsMap_ = new Dictionary<string, IndexedBounds>();
            imapHighDetailBoundsMap_ = new Dictionary<string, IndexedBounds>();
            imapBoundsFilenameMap_ = new Dictionary<string, string[]>();
            dynamicBoundsMap_ = new Dictionary<string, IndexedBoundsDictionary>();
        }

        internal void LoadSceneBounds(IEnumerable<SceneXml.CollisionObject> collisionObjects, string[] boundsZipPathnames, string[] sceneFilenames)
        {
            for (int sceneIndex = 0; sceneIndex < boundsZipPathnames.Length; ++sceneIndex)
            {
                string boundsZipPathname = boundsZipPathnames[sceneIndex];
                string sceneFilename = sceneFilenames[sceneIndex];
                    
                if (String.IsNullOrEmpty(boundsZipPathname))
                {
                    Log.Log__Message("There were no bounds files to load");
                    continue;
                }

                // Extract and parse the BVH files in the bounds zip, building a name:bvh map
                Dictionary<string, BoundObject> boundsMap = BoundsLoader.LoadAndIndexRawBoundsData(boundsZipPathname);

                // build the paired collection
                foreach (var boundsPair in boundsMap)
                {
                    SceneXml.CollisionObject collisionObject = collisionObjects.FirstOrDefault(
                        co => String.Equals(co.Name, boundsPair.Key, StringComparison.CurrentCultureIgnoreCase) &&
                              String.Equals(co.SceneFilename, sceneFilename, StringComparison.CurrentCultureIgnoreCase));

                    if (collisionObject == null)
                    {
                        Log.Log__Warning("Unable to look up scene xml entry for node '{0}'.  " +
                                         "This means the exported data and scene xml are out of sync.  Collision data exported from max will not appear in game.",
                                         boundsPair.Key);
                        continue;
                    }

                    if (collisionObject.MILOParentName != null)
                    {
                        // We just need to leave these objects for the interior bounds processor
                        continue;
                    }

                    IEnumerable<Tuple<BNDFile.CollisionType, BVH>> bvhsToAdd = 
                        BoundsLoader.ConvertBoundObjectToBVHPairCollection(boundsPair.Key, boundsPair.Value, collisionObject.CollisionType);

                    if (!collisionObject.ParentIsDynamic)
                    {
                        foreach (Tuple<BNDFile.CollisionType, BVH> typeBVHPair in bvhsToAdd)
                        {
                            if (String.IsNullOrEmpty(collisionObject.IPLGroupName))
                            {
                                AddBVH(typeBVHPair.Item1, typeBVHPair.Item2);
                            }
                            else
                            {
                                bool serialiseOut = (collisionObject.IsNewDLC == project_.IsDLC) || project_.ForceFlags.AllNewContent;
                                if (serialiseOut)
                                    AddIPLGroupBVH(collisionObject.IPLGroupName, typeBVHPair.Item1, typeBVHPair.Item2);
                            }
                        }
                    }
                    else
                    {
                        if (collisionObject.ParentIsFragment)
                        {
                            Log.Log__Message("Skipping collision for node '{0}'.  This node's parent is a fragment.", boundsPair.Key);
                            continue;
                        }

                        string dictionaryName = mapSectionName_;
                        if (!String.IsNullOrEmpty(collisionObject.CollisionGroupName))
                        {
                            dictionaryName = collisionObject.CollisionGroupName;
                        }

                        if (!dynamicBoundsMap_.ContainsKey(dictionaryName))
                        {
                            dynamicBoundsMap_.Add(dictionaryName, new IndexedBoundsDictionary());
                        }

                        foreach (Tuple<BNDFile.CollisionType, BVH> typeBVHPair in bvhsToAdd)
                        {
                            typeBVHPair.Item2.TransformVerts(collisionObject.WorldToParentTransformMatrix);
                            dynamicBoundsMap_[dictionaryName].AddBVH(
                                collisionObject.ParentName,
                                typeBVHPair.Item1,
                                typeBVHPair.Item2,
                                IndexedBounds.SerialisationOptions.AsDiscretePrimitives);
                        }
                    }
                }

                Log.Log__Message("Loaded {0} bounds files from '{1}'.", boundsMap.Count, boundsZipPathname);
            }
        }

        internal void LoadRefBounds(IEnumerable<SceneXml.RefObject> rsRefObjectsToBake)
        {
            Dictionary<SceneXml.RefObject, Dictionary<BNDFile.CollisionType, List<BVH>>> indexedBoundsMap = BoundsLoader.LoadAndIndexRefBoundsData(rsRefObjectsToBake);

            // Now we process the indexed data (prop data that couldn't be looked up will be thrown away
            foreach (KeyValuePair<SceneXml.RefObject, Dictionary<BNDFile.CollisionType, List<BVH>>> indexedPair in indexedBoundsMap)
            {
                SceneXml.RefObject refObject = indexedPair.Key;
                Dictionary<BNDFile.CollisionType, List<BVH>> collisionTypeBoundsMap = indexedPair.Value;
                foreach (KeyValuePair<BNDFile.CollisionType, List<BVH>> collisionTypeBoundsPair in collisionTypeBoundsMap)
                {
                    BNDFile.CollisionType collisionType = collisionTypeBoundsPair.Key;
                    foreach (BVH bvh in collisionTypeBoundsPair.Value)
                    {
                        bvh.ScaleContents(refObject.Scale);// Scale the verts while they're still in local space
                        bvh.TransformVerts(refObject.RuntimeLocalToWorldTransform);// We need to transform this BVH into world space

                        if (String.IsNullOrEmpty(refObject.IPLGroupName))
                        {
                            AddBVH(collisionType, bvh);
                        }
                        else
                        {
                            bool serialiseOut = (refObject.IsNewDLC == project_.IsDLC) || project_.ForceFlags.AllNewContent;
                            if (serialiseOut)
                                AddIPLGroupBVH(refObject.IPLGroupName, collisionType, bvh);
                        }
                    }
                }
            }

            Log.Log__Message("Baking {0} prop instances into the bounds.", indexedBoundsMap.Count);
        }

        private void AddBVH(BNDFile.CollisionType collisionType, BVH bvh)
        {
            collisionType = CollisionTypePatcher.PatchCollisionType(collisionType);

            if (collisionType == BNDFile.CollisionType.Weapons)
            {
                highDetailBounds_.AddBVH(collisionType, bvh);
            }
            else if (collisionType == BNDFile.CollisionType.MaterialOnly)
            {
                materialBounds_.AddBVH(collisionType, bvh);
            }
            else
            {
                standardBounds_.AddBVH(collisionType, bvh);
            }
        }

        private void AddIPLGroupBVH(string imapGroupName, BNDFile.CollisionType collisionType, BVH bvh)
        {
            collisionType = CollisionTypePatcher.PatchCollisionType(collisionType);

            imapGroups_.Add(imapGroupName);

            Dictionary<string, IndexedBounds> imapBoundsMap;// Determine which container to add to
            if (collisionType != BNDFile.CollisionType.Weapons)
            {
                imapBoundsMap = imapStandardBoundsMap_;
            }
            else
            {
                imapBoundsMap = imapHighDetailBoundsMap_;
            }


            if (!imapBoundsMap.ContainsKey(imapGroupName))
            {
                imapBoundsMap.Add(imapGroupName, new IndexedBounds(IndexedBounds.SerialisationOptions.AsBVH));
            }

            imapBoundsMap[imapGroupName].AddBVH(collisionType, bvh);
        }

        private void PopulatePrimitiveBlockPathnameMap(String boundPrefix, OrphanedData.PrimitiveCollection[] primitiveBlocks, Dictionary<OrphanedData.PrimitiveCollection, string> primitiveBlockPathnameMap, String outputDirectory, string dlcPrefix = "")
        {
            for (int primitiveBlockIndex = 0; primitiveBlockIndex < primitiveBlocks.Length; ++primitiveBlockIndex)
            {
                string pathname = Path.Combine(outputDirectory, String.Format(boundPrefix + dlcPrefix + "{0}_{1}.bnd", mapSectionName_, primitiveBlockIndex));
                primitiveBlockPathnameMap.Add(primitiveBlocks[primitiveBlockIndex], pathname);
            }
        }

        internal void ProcessAndSerialise(string outputDirectory, string inputDirectory)
        {
            // handle dynamic props seperately
            Log.Log__Message("Processing bounds for {0} physics dictionaries", dynamicBoundsMap_.Count);
            ITask processDynamicBoundsTask = Profiling.TheProfiler.StartTask("Processing dynamic bounds");

            string secondaryBoundsDirectory = null;
            if (!string.IsNullOrEmpty(inputDirectory) && String.Compare(inputDirectory, "null") != 0)
            {
                secondaryBoundsDirectory = Path.Combine(inputDirectory, "transformed_drawables");

                // Prepare the secondary directory
                if (!Directory.Exists(secondaryBoundsDirectory))
                {
                    Log.Log__Error("Missing secondary bounds directory {0}", secondaryBoundsDirectory);
                }
            }

            foreach (KeyValuePair<string, IndexedBoundsDictionary> dynamicBoundsPair in dynamicBoundsMap_)
            {
                Log.Log__Message("Processing bounds for physics dictionary '{0}'", dynamicBoundsPair.Key);

                string boundsDictionaryDirectory = Path.Combine(outputDirectory, dynamicBoundsPair.Key);
                Directory.CreateDirectory(boundsDictionaryDirectory);

                dynamicBoundsPair.Value.ProcessAndSerialise(project_, boundsDictionaryDirectory, secondaryBoundsDirectory);
            }
            Profiling.TheProfiler.StopTask(processDynamicBoundsTask);

            // Build the the primitive soup
            Log.Log__Message("Building primitive soup");
            ITask buildPrimitiveSoupTask = Profiling.TheProfiler.StartTask("Building primitive soup");


            Dictionary<OrphanedData.PrimitiveCollection, string> primitiveBlockPathnameMap = new Dictionary<OrphanedData.PrimitiveCollection, string>();

            string dlcPrefix = "";
            if (project_.IsDLC)
                dlcPrefix = project_.AssetPrefix.MapPrefix;

            // Standard primitives without per vert attrib data
            OrphanedData.PrimitiveCollection[] primitiveBlocks = standardBounds_.ToPrimitiveCollection(Config.TheConfig.Options.MaxStandardBoundsXY, Config.TheConfig.Options.MaxStandardMapCompositeDataSize);
            this.PopulatePrimitiveBlockPathnameMap("", primitiveBlocks, primitiveBlockPathnameMap, outputDirectory, dlcPrefix);

            // High detail primitives without per vert attrib data
            primitiveBlocks = highDetailBounds_.ToPrimitiveCollection(Config.TheConfig.Options.MaxHighDetailBoundsXY, Config.TheConfig.Options.MaxHighDetailMapCompositeDataSize);
            this.PopulatePrimitiveBlockPathnameMap("hi@", primitiveBlocks, primitiveBlockPathnameMap, outputDirectory, dlcPrefix);

            // Material primitives without per vert attrib data
            primitiveBlocks = materialBounds_.ToPrimitiveCollection(Config.TheConfig.Options.MaxHighDetailBoundsXY, Config.TheConfig.Options.MaxHighDetailMapCompositeDataSize);
            this.PopulatePrimitiveBlockPathnameMap("ma@", primitiveBlocks, primitiveBlockPathnameMap, outputDirectory, dlcPrefix);
            
            // IMAP primitives
            Log.Log__Message("Processing bounds for {0} IMAP groups", imapGroups_.Count);
            foreach (string imapGroupName in imapGroups_)
            {
                Log.Log__Message("Processing bounds for IMAP group '{0}'", imapGroupName);
                List<OrphanedData.PrimitiveCollection> imapBlocks = new List<OrphanedData.PrimitiveCollection>();
                if (imapStandardBoundsMap_.ContainsKey(imapGroupName))
                {
                    primitiveBlocks = imapStandardBoundsMap_[imapGroupName].ToPrimitiveCollection(
                        Config.TheConfig.Options.MaxStandardBoundsXY, Config.TheConfig.Options.MaxStandardMapCompositeDataSize);
                    imapBlocks.AddRange(primitiveBlocks);
                }
                if (imapHighDetailBoundsMap_.ContainsKey(imapGroupName))
                {
                    primitiveBlocks = imapHighDetailBoundsMap_[imapGroupName].ToPrimitiveCollection(
                        Config.TheConfig.Options.MaxHighDetailBoundsXY, Config.TheConfig.Options.MaxHighDetailMapCompositeDataSize);
                    imapBlocks.AddRange(primitiveBlocks);
                }

                imapBoundsFilenameMap_.Add(imapGroupName, new string[imapBlocks.Count]);
                for (int blockIndex = 0; blockIndex < imapBlocks.Count; ++blockIndex)
                {
                    // JWR - The index here is 1 based for legacy reasons, do not change it, the runtime depends on it (for now) :-(
                    string imapBoundsFilename = string.Format("{0}_{1}", imapGroupName, blockIndex + 1);
                    imapBoundsFilenameMap_[imapGroupName][blockIndex] = imapBoundsFilename;

                    string pathname = Path.Combine(outputDirectory, string.Format("{0}.bnd", imapBoundsFilename));
                    primitiveBlockPathnameMap.Add(imapBlocks[blockIndex], pathname);
                }
            }

            Profiling.TheProfiler.StopTask(buildPrimitiveSoupTask);

            // At this point we have a map of primitive collections and the pathnames they should be serialised to.
            // We first need to convert them to composites.  This is where the time goes.
            Dictionary<Composite, string> compositePathnameMap = new Dictionary<Composite, string>();
            Log.Log__Message("Converting primitive blocks to composites");
            ITask convertToCompositeTask = Profiling.TheProfiler.StartTask("Converting to composite");

            // We order the jobs by highest primitive count so that the most complex jobs are kicked off first
            OrphanedData.PrimitiveCollection[] leafNodes = primitiveBlockPathnameMap.Keys.OrderByDescending(pc => pc.CountPrimitives()).ToArray();
            Composite[] composites = null;

            // B* 1170455 - avoid setting up the parallel for if we've got nothing to do.
            if (leafNodes.Length > 0)
            {
                if (Config.TheConfig.Options.Parallelise)
                {
                    ParallelCompositeGenerator.DataIn = leafNodes;
                    ParallelCompositeGenerator.SerialisationOptions = IndexedBounds.SerialisationOptions.AsBVH;
                    ParallelCompositeGenerator.DataOut = new Composite[leafNodes.Length];
                    System.Threading.Tasks.ParallelOptions options = new System.Threading.Tasks.ParallelOptions();
                    // Setting MaxDegreeOfParallelism is very, very important for performance as it ensures that no thread pools are created.
                    // This means that our jobs are run in order of most complex to last and we aren't sat waiting a minute for the last job.
                    options.MaxDegreeOfParallelism = leafNodes.Length;
                    System.Threading.Tasks.Parallel.For(0, leafNodes.Length, options, ParallelCompositeGenerator.Generate);
                    composites = ParallelCompositeGenerator.DataOut;
                }
                else
                {
                    composites = new Composite[leafNodes.Length];
                    for (int leafIndex = 0; leafIndex < leafNodes.Length; ++leafIndex)
                    {
                        if (leafNodes[leafIndex].IsHighDetail)
                            composites[leafIndex] = leafNodes[leafIndex].ToComposite(IndexedBounds.SerialisationOptions.AsBVH, Config.TheConfig.Options.MaxHighDetailMapBVHDataSize);
                        else
                            composites[leafIndex] = leafNodes[leafIndex].ToComposite(IndexedBounds.SerialisationOptions.AsBVH, Config.TheConfig.Options.MaxStandardMapBVHDataSize);
                    }
                }

                // Build the composite to pathname map from the output data
                for (int leafIndex = 0; leafIndex < leafNodes.Length; ++leafIndex)
                {
                    compositePathnameMap.Add(composites[leafIndex], primitiveBlockPathnameMap[leafNodes[leafIndex]]);
                }
            }
            else
            {
                Log.Log__Warning("No static map bounds to convert");
            }

            Profiling.TheProfiler.StopTask(convertToCompositeTask);

            // Report on the sizes of what we built (for diagnostic purposes)
            foreach (KeyValuePair<Composite, string> compositePathnamePair in compositePathnameMap)
            {
                foreach (CompositeChild compositeChild in compositePathnamePair.Key.Children)
                {
                    if (compositeChild.BoundObject is BVH)
                    {
                        BVH bvh = (BVH)compositeChild.BoundObject;
                        int expectedFinalDataSizeBytes = (expectedFinalPrimitiveSize_ * bvh.Primitives.Length) + (expectedFinalVertexSize_ * bvh.Verts.Length);
                        double expectedFinalDataSizeKB = (double)expectedFinalDataSizeBytes / 1024d;
                        Log.Log__Message("Produced BVH in '{0}' that is estimated to be {1} KB", compositePathnamePair.Value, expectedFinalDataSizeKB.ToString("0.00"));
                    }
                }
            }

            // At this point we have a map of composites and the pathnames they should be serialised to, nice and easy
            Log.Log__Message("Saving composites");
            ITask saveCompositesTask = Profiling.TheProfiler.StartTask("Saving composites");
            foreach (KeyValuePair<Composite, string> compositePathnamePair in compositePathnameMap)
            {
                Log.Log__Message("Composite: {0}", compositePathnamePair.Value);
                compositePathnamePair.Key.Save(compositePathnamePair.Value);
            }
            Profiling.TheProfiler.StopTask(saveCompositesTask);
        }

        internal void PackDrawableBounds(string outputDirectory, string inputDirectory, string drawableListString)
        {
            // Any container that doesn't have transformed drawables will come through with "null" from the XML file.
            if (inputDirectory == "null" || drawableListString == "null")
                return;

            IList<string> drawableList = drawableListString.Split(new char[]{';'}, StringSplitOptions.RemoveEmptyEntries);
            string boundsDirectory = Path.Combine(inputDirectory, "transformed_drawables");

            if (!Directory.Exists(boundsDirectory))
                Directory.CreateDirectory(boundsDirectory);

            // Only process drawables we have in our list.
            if (drawableList.Count > 0)
            {
                foreach (string drawableFilename in drawableList)
                {
                    string drawableName = null;
                    if (drawableFilename.EndsWith(".idr.zip"))
                        drawableName = drawableFilename.Replace(".idr.zip", "");

                    string drawablePath = Path.Combine(inputDirectory, drawableFilename);
                    string transformedDrawablePath = Path.Combine(boundsDirectory, drawableFilename);
                    drawableName = MapAsset.AppendMapPrefixIfRequired(project_, drawableName);
                    string boundPath = Path.Combine(boundsDirectory, string.Format("{0}.bnd", drawableName));

                    if (File.Exists(drawablePath))
                    {
                        if (!File.Exists(boundPath))
                        {
                            Log.Log__Message("Drawable doesn't have a bound to pack '{0}'", drawablePath);

                            // Even If we don't have a bound to pack, still copy over the unmodified drawable to satisfy dependencies.
                            File.Copy(drawablePath, transformedDrawablePath, true);

                            continue;
                        }

                        // Do the actual bound packing
                        PackDrawableBound(drawablePath, transformedDrawablePath, boundPath);
                    }
                    else
                    {
                        Log.Log__Warning("Couldn't find drawable to pack bounds '{0}'", drawableFilename);
                    }
                }

                DirectoryInfo boundsDirectoryInfo = new DirectoryInfo(boundsDirectory);
                boundsDirectoryInfo.GetFiles("*.bnd").ToList().ForEach(file => file.Delete());
            }
        }

        internal void PackDrawableBound(string drawablePath, string transformedDrawablePath, string boundPath)
        {
            Log.Log__Message("Packing bounds for '{0}' with bound file {1}", transformedDrawablePath, boundPath);

            using (Ionic.Zip.ZipFile exportedDataZipFile = Ionic.Zip.ZipFile.Read(drawablePath))
            {
                try
                {
                    exportedDataZipFile.AddFile(boundPath, "");
                    exportedDataZipFile.Save(transformedDrawablePath);
                }
                catch (Exception e)
                {
                    Log.Log__Message("Error packing bounds for '{0}' with {1}\nException: {2}", drawablePath, boundPath, e.Message);
                }
            }
        }

        internal void GetManifestData(Manifest.ManifestDataBuilder manifestDataBuilder)
        {
            foreach (KeyValuePair<string, string[]> iplBoundsPair in imapBoundsFilenameMap_)
            {
                manifestDataBuilder.Add(new Manifest.MapDataGroup(iplBoundsPair.Key, iplBoundsPair.Value));
            }
        }

        private static readonly int expectedFinalPrimitiveSize_ = 16;
        private static readonly int expectedFinalVertexSize_ = 6;

        private string mapSectionName_;
        private IProject project_;
        private IndexedBounds standardBounds_;// standard bounds for this map section
        private IndexedBounds highDetailBounds_;// high detail bounds for this map section
        private IndexedBounds materialBounds_;// high detail bounds for this map section
        private SortedSet<string> imapGroups_;// set of imap group names
        private Dictionary<string, IndexedBounds> imapStandardBoundsMap_;// standard bounds for each imap group (if available)
        private Dictionary<string, IndexedBounds> imapHighDetailBoundsMap_;// High detail bounds for each imap group (if available)
        private Dictionary<string, string[]> imapBoundsFilenameMap_;// Filenames produced for each imap group
        private Dictionary<string, IndexedBoundsDictionary> dynamicBoundsMap_;// dynamic bounds for this map section
    }
}
