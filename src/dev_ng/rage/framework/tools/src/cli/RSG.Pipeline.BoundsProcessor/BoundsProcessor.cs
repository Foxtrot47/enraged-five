﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Ionic.Zip;
using RSG.Base.Configuration;
using RSG.Base.Profiling;
using RSG.Pipeline.Services.BoundsProcessor;
using RSG.SceneXml;
using RSG.SceneXml.MapExport;
using RSG.SceneXml.MapExport.Project.GTA5_NG;

namespace RSG.Pipeline.BoundsProcessor
{
    /// <summary>
    /// The bounds processor itself.  Provides one method, ProcessMap() that does the whole job of bounds processing
    /// </summary>
    internal static class BoundsProcessor
    {
        private enum ContentType
        {
            MapSection,
            Interior,
            Prop,
            Unsupported
        }

        internal static void ProcessMap(
            CommandOptions options,
            BoundsProcessorTask task, 
            SceneCollection sceneCollection, 
            string[] sceneXmlPathnames, 
            string[] boundsZipPathnames,
            string[] inputDirectories,
            string[] drawableLists,
            string[] additionalImaps,
            string outputDirectory,
            Content.ContentTreeHelper contentTreeHelper)
        {
            ITask processMapTask = Profiling.TheProfiler.StartTask("ProcessMap()");

            ITask sceneDataTask = Profiling.TheProfiler.StartTask("Load scene data");
            var scenesQuery = from sceneXmlPathname in sceneXmlPathnames
                              select sceneCollection.LoadScene(sceneXmlPathname);
            Scene[] scenes = scenesQuery.ToArray();
            ContentType contentType = DetermineContentType(task);
            Profiling.TheProfiler.StopTask(sceneDataTask);

            BoundsLoader.SceneCollection = sceneCollection;
            BoundsLoader.Branch = options.Branch;

            List<IMAPFile> imapFiles = new List<IMAPFile>();
            foreach (string imapFile in additionalImaps)
            {
                IMAPFile imap = IMAPFile.Load(imapFile, sceneCollection);
                if (imap == null)
                {
                    throw new RSG.Bounds.InvalidDataFormatException(String.Format("Unable to find imap file {0}.", imapFile));
                }
                else
                {
                    imapFiles.Add(imap);
                }
            }

            // Process the scene data for this processed map
            ITask getCollisionDataTask = Profiling.TheProfiler.StartTask("GetCollisionData()");
            SceneXml.CollisionData collisionData = SceneXml.SceneXmlUtilities.GetCollisionData(scenes, imapFiles, contentTreeHelper);
            Profiling.TheProfiler.StopTask(getCollisionDataTask);

            Manifest.ManifestDataBuilder manifestDataBuilder = new Manifest.ManifestDataBuilder();

            if (contentType == ContentType.MapSection)
            {
                ProcessMapSectionScene(task, collisionData, boundsZipPathnames, outputDirectory, inputDirectories[0], drawableLists[0], scenes, manifestDataBuilder, options.Project);
            }
            else if (contentType == ContentType.Interior)
            {
                ProcessInteriorScene(task, collisionData, boundsZipPathnames, outputDirectory, inputDirectories[0], drawableLists[0], manifestDataBuilder, options.Project);
            }
            else if (contentType == ContentType.Prop)
            {
                ProcessPropScene(task, collisionData, boundsZipPathnames[0], outputDirectory, inputDirectories[0], drawableLists[0], manifestDataBuilder, options.Project);
            }
            // AJM: Removed the default behaviour of outputting a warning and exiting, as occlusion containers were requiring the xml file to be 
            // output for the RPF manifest.

            String manifestDataPathname = task.Output.ManifestAdditionsPathname;
            manifestDataBuilder.Save(manifestDataPathname);

            // B* 723697 - Scene objects take up lots of memory.  To prevent out of memory issues we unload them then request a collect
            foreach (string sceneXmlPathname in sceneXmlPathnames)
            {
                sceneCollection.UnloadScene(sceneXmlPathname);
            }
            GC.Collect();

            // Support for self-archive creation
            if (Config.TheConfig.Options.ArchiveOutput)
            {
                String prefix = MapAsset.GetMapProjectPrefix(options.Project);
                foreach (string boundsPathname in Directory.GetFiles(outputDirectory))
                {
                    // check if we're not trying to process an already created pack
                    if (boundsPathname.EndsWith(".ibn.zip") || boundsPathname.EndsWith(".ibd.zip"))
                        continue;

                    string boundsZipPathname = Path.ChangeExtension(boundsPathname, ".ibn.zip");
                    using (ZipFile boundsZipFile = new ZipFile())
                    {
                        boundsZipFile.AddFile(boundsPathname, "");
                        boundsZipFile.Save(boundsZipPathname);
                    }

                    File.Delete(boundsPathname);
                }

                foreach (string boundsDirectory in Directory.GetDirectories(outputDirectory))
                {
                    String boundsBasename = Path.GetFileNameWithoutExtension(boundsDirectory);
                    String boundsDirectoryName = Path.GetDirectoryName(boundsDirectory);
                    String boundsZipBasename = String.Format("{0}{1}", prefix, boundsBasename);
                    string boundsDictionaryZipPathname = Path.Combine(boundsDirectoryName, Path.ChangeExtension(boundsZipBasename, ".ibd.zip"));
                    using (ZipFile boundsZipFile = new ZipFile())
                    {
                        foreach (string boundsPathname in Directory.GetFiles(boundsDirectory))
                            boundsZipFile.AddFile(boundsPathname, "");
                        boundsZipFile.Save(boundsDictionaryZipPathname);
                    }

                    foreach (string boundsPathname in Directory.GetFiles(boundsDirectory))
                        File.Delete(boundsPathname);
                    Directory.Delete(boundsDirectory);
                }
            }

            Profiling.TheProfiler.StopTask(processMapTask);
        }

        private static ContentType DetermineContentType(BoundsProcessorTask task)
        {
            String[] uniqueSceneTypes = task.Inputs.Select(input => input.SceneType).Distinct().ToArray();

            if (uniqueSceneTypes.Length == 1)
            {
                if (uniqueSceneTypes[0] == "props")
                    return ContentType.Prop;
                else if (uniqueSceneTypes[0] == "interior")
                    return ContentType.Interior;
                else if ( (uniqueSceneTypes[0] == "lod_container") || (uniqueSceneTypes[0] == "map_container") )
                    return ContentType.MapSection;
            }

            return ContentType.Unsupported;
        }

        private static void ProcessMapSectionScene(
            BoundsProcessorTask task, 
            SceneXml.CollisionData collisionData, 
            string[] boundsZipPathnames, 
            string outputDirectory,
            string inputDirectory,
            string drawableList,
            Scene[] scenes,
            Manifest.ManifestDataBuilder manifestDataBuilder,
            IProject project)
        {
            MapSectionBoundsProcessor mapSectionBounds = new MapSectionBoundsProcessor(project, task.Output.Name);
            string[] sceneFilenames = scenes.Select(scene => scene.Filename).ToArray();

            ITask loadSceneBoundsTask = Profiling.TheProfiler.StartTask("LoadSceneBounds()");
            mapSectionBounds.LoadSceneBounds(collisionData.CollisionObjects, boundsZipPathnames, sceneFilenames);
            Profiling.TheProfiler.StopTask(loadSceneBoundsTask);

            ITask loadRefBoundsTask = Profiling.TheProfiler.StartTask("LoadRefBounds()");
            mapSectionBounds.LoadRefBounds(collisionData.RSRefObjectsToBake);
            Profiling.TheProfiler.StopTask(loadRefBoundsTask);

            ITask processAndSerialiseTask = Profiling.TheProfiler.StartTask("ProcessAndSerialise()");
            mapSectionBounds.ProcessAndSerialise(outputDirectory, inputDirectory);
            Profiling.TheProfiler.StopTask(processAndSerialiseTask);

            ITask packDrawableTask = Profiling.TheProfiler.StartTask("PackTransformedDrawables()");
            mapSectionBounds.PackDrawableBounds(outputDirectory, inputDirectory, drawableList);
            Profiling.TheProfiler.StopTask(packDrawableTask);

            mapSectionBounds.GetManifestData(manifestDataBuilder);

            if (collisionData.MiloObjects.Any())
            {
                InteriorsBoundsProcessor interiorsBoundsProcessor = new InteriorsBoundsProcessor(project, InteriorsBoundsProcessor.Context.InSitu);

                interiorsBoundsProcessor.LoadSceneBounds(collisionData.CollisionObjects, boundsZipPathnames);
                interiorsBoundsProcessor.LoadRefBounds(collisionData.RSRefObjectsToBake);
                interiorsBoundsProcessor.ProcessAndSerialise(outputDirectory, inputDirectory, collisionData.MiloObjects);
                interiorsBoundsProcessor.PackDrawableBounds(outputDirectory, inputDirectory, drawableList);
                interiorsBoundsProcessor.GetManifestData(manifestDataBuilder);
            }
        }

        private static void ProcessInteriorScene(
            BoundsProcessorTask task, 
            SceneXml.CollisionData collisionData, 
            string[] boundsZipPathnames, 
            string outputDirectory,
            string inputDirectory,
            string drawableList,
            Manifest.ManifestDataBuilder manifestDataBuilder,
            IProject project)
        {
            InteriorsBoundsProcessor interiorsBoundsProcessor = new InteriorsBoundsProcessor(project, InteriorsBoundsProcessor.Context.Normal);

            interiorsBoundsProcessor.LoadSceneBounds(collisionData.CollisionObjects, boundsZipPathnames);
            interiorsBoundsProcessor.LoadRefBounds(collisionData.RSRefObjectsToBake);
            interiorsBoundsProcessor.ProcessAndSerialise(outputDirectory, inputDirectory, collisionData.MiloObjects);
            interiorsBoundsProcessor.PackDrawableBounds(outputDirectory, inputDirectory, drawableList);
            interiorsBoundsProcessor.GetManifestData(manifestDataBuilder);
        }

        private static void ProcessPropScene(
            BoundsProcessorTask task, 
            SceneXml.CollisionData collisionData, 
            string boundsZipPathname, 
            string outputDirectory,
            string inputDirectory,
            string drawableList,
            Manifest.ManifestDataBuilder manifestDataBuilder,
            IProject project)
        {
            PropsBoundsProcessor propsBoundsProcessor = new PropsBoundsProcessor(project, task.Output.Name);

            propsBoundsProcessor.LoadSceneBounds(collisionData.CollisionObjects, boundsZipPathname);
            propsBoundsProcessor.ProcessAndSerialise(outputDirectory, inputDirectory);
            propsBoundsProcessor.PackDrawableBounds(outputDirectory, inputDirectory, drawableList);
        }

        // This should be factored into MapSectionBoundsProcessor
        private static void SerialiseIMAPGroupMetadata(
            string mapSectionName, 
            MapSectionBoundsProcessor mapSectionBoundsProcessor, 
            Manifest.ManifestDataBuilder manifestDataBuilder)
        {
            mapSectionBoundsProcessor.GetManifestData(manifestDataBuilder);
        }

        internal static void PrepareOutputDirectory(string outputDirectory)
        {
            // Prepare the output directory
            if (Directory.Exists(outputDirectory))
            {
                Directory.Delete(outputDirectory, true);
            }
            Directory.CreateDirectory(outputDirectory);
        }
    }
}
