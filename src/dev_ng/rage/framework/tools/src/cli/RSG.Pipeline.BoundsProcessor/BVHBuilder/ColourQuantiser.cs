﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RSG.Pipeline.BoundsProcessor.BVHBuilder
{
    internal class ColourQuantiser
    {
        internal ColourQuantiser()
        {
            reduceList_ = new ColourQuantiserOctreeNode[treeDepth_];
            leafLevel_ = treeDepth_;
        }

        /// <summary>
        /// Constructs an octree from an array of colours
        /// </summary>
        /// <param name="colours"></param>
        internal void BuildOctree(IEnumerable<ColourRGB> colours)
        {
            rootNode_ = new ColourQuantiserOctreeNode();
            rootNode_.Level = 0;
            foreach (ColourRGB colour in colours)
            {
                InsertOctreeNode(ref rootNode_, colour, 0);
            }
        }

        /// <summary>
        /// Reduces the tree until it has no more than maxPaletteSize leaves
        /// </summary>
        /// <param name="maxPaletteSize"></param>
        internal void ReduceTreeToSpecifiedLeafCount(int maxPaletteSize)
        {
            while (totalLeaves_ > maxPaletteSize)
            {
                ReduceTree();
            }
        }

        internal ColourRGB[] CollapseToPaletteTable()
        {
            List<ColourRGB> palette = new List<ColourRGB>();
            CollapseToPaletteTableRecursive(rootNode_, palette);
            return palette.ToArray();
        }

        private void CollapseToPaletteTableRecursive(ColourQuantiserOctreeNode node, ICollection<ColourRGB> palette)
        {
            if (node.IsLeaf)
            {
                ColourRGB collapsedColour = new ColourRGB();
                collapsedColour.Red = node.Colour.Red / node.PixelCount;
                collapsedColour.Green = node.Colour.Green / node.PixelCount;
                collapsedColour.Blue = node.Colour.Blue / node.PixelCount;
                node.PaletteIndex = palette.Count;
                palette.Add(collapsedColour);
            }
            else
            {
                foreach (ColourQuantiserOctreeNode childNode in node.Children.Where(childNode => (childNode != null)))
                {
                    CollapseToPaletteTableRecursive(childNode, palette);
                }
            }
        }

        /// <summary>
        /// Looks up the closest colour match in the palette
        /// </summary>
        /// <param name="sourceColour">The colour to look up</param>
        /// <returns></returns>
        internal int GetClosestPaletteIndex(ColourQuantiserOctreeNode node, ColourRGB sourceColour)
        {
            if (node.IsLeaf)
            {
                return node.PaletteIndex;
            }
            else
            {
                int quadrant = GetOctant(sourceColour, treeDepth_ - node.Level - 1);
                return GetClosestPaletteIndex(node.Children[quadrant], sourceColour);
            }
        }

        internal ColourQuantiserOctreeNode RootNode { get { return rootNode_; } }

        private static int GetOctant(ColourRGB colour, int levelsFromBottom)
        {
            int octant = 0;
            int maskBit = (1 << levelsFromBottom);
            if (((int)colour.Red & maskBit) == maskBit)
            {
                octant += 4;
            }
            if (((int)colour.Green & maskBit) == maskBit)
            {
                octant += 2;
            }
            if (((int)colour.Blue & maskBit) == maskBit)
            {
                octant += 1;
            }

            return octant;
        }

        private ColourQuantiserOctreeNode CreateOctreeNode(int level)
        {
            ColourQuantiserOctreeNode result = new ColourQuantiserOctreeNode();
            result.Level = level;
            result.IsLeaf = (level == treeDepth_);

            if (result.IsLeaf)
            {
                result.Colour = new ColourRGB();
                totalLeaves_++;
            }
            else
            {
                MakeReducible(level, result);
            }

            return result;
        }

        private void MakeReducible(int level, ColourQuantiserOctreeNode node)
        {
            node.NextNode = reduceList_[level];
            reduceList_[level] = node;
        }

        private void InsertOctreeNode(ref ColourQuantiserOctreeNode node, ColourRGB colour, int level)
        {
            if (node == null)
            {
                node = CreateOctreeNode(level);
            }

            if (node.IsLeaf)
            {
                ++node.PixelCount;
                node.Colour.Red += colour.Red;
                node.Colour.Green += colour.Green;
                node.Colour.Blue += colour.Blue;
            }
            else
            {
                int levelsFromBottom = treeDepth_ - (level + 1); 
                int octant = GetOctant(colour, levelsFromBottom);
                InsertOctreeNode(ref node.Children[octant], colour, level + 1);
            }
        }

        private void ReduceTree()
        {
            int childCount = 0;
            ColourQuantiserOctreeNode node = GetReducible();
            node.Colour = new ColourRGB();
            for (int childIndex = 0; childIndex < node.Children.Length; ++childIndex)
            {
                if (node.Children[childIndex] != null)
                {
                    ++childCount;
                    node.Colour.Red += node.Children[childIndex].Colour.Red;
                    node.Colour.Green += node.Children[childIndex].Colour.Green;
                    node.Colour.Blue += node.Children[childIndex].Colour.Blue;
                    node.PixelCount += node.Children[childIndex].PixelCount;

                    node.Children[childIndex] = null;
                }
            }
            node.IsLeaf = true;
            totalLeaves_ -= (childCount - 1);
        }

        private ColourQuantiserOctreeNode GetReducible()
        {
            while (reduceList_[leafLevel_ - 1] == null)
            {
                --leafLevel_;
            }

            ColourQuantiserOctreeNode node = reduceList_[leafLevel_ - 1];
            reduceList_[leafLevel_ - 1] = reduceList_[leafLevel_ - 1].NextNode;
            return node;
        }

        private readonly int treeDepth_ = 8;

        private int totalLeaves_;
        private ColourQuantiserOctreeNode rootNode_;
        private ColourQuantiserOctreeNode[] reduceList_;
        private int leafLevel_;
    }
}
