﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;
using RSG.Bounds;

namespace RSG.Pipeline.BoundsProcessor.OrphanedData
{
    /// <summary>
    /// A self contained quad primitive
    /// </summary>
    internal class Quad : Primitive
    {
        internal Quad(BNDFile.CollisionType collisionType,
                     string material,
                     Vector4i materialColour,
                     float[] secondSurfaceDisplacements,
                     Vector3f[] shrunkVerts,
                     VertexData vert0,
                     VertexData vert1,
                     VertexData vert2,
                     VertexData vert3,
                     uint numPerVertAttribs)
            : base(collisionType, material, materialColour, secondSurfaceDisplacements, shrunkVerts, numPerVertAttribs)
        {
            Vert0 = vert0;
            Vert1 = vert1;
            Vert2 = vert2;
            Vert3 = vert3;

            float centreX = (vert0.Position.X + vert1.Position.X + vert2.Position.X + vert3.Position.X) * 0.25f;
            float centreY = (vert0.Position.Y + vert1.Position.Y + vert2.Position.Y + vert3.Position.Y) * 0.25f;
            CentreXY = new Vector2f(centreX, centreY);
            MinX = Math.Min(vert0.Position.X, Math.Min(vert1.Position.X, Math.Min(vert2.Position.X, vert3.Position.X)));
            MaxX = Math.Max(vert0.Position.X, Math.Max(vert1.Position.X, Math.Max(vert2.Position.X, vert3.Position.X)));
            MinY = Math.Min(vert0.Position.Y, Math.Min(vert1.Position.Y, Math.Min(vert2.Position.Y, vert3.Position.Y)));
            MaxY = Math.Max(vert0.Position.Y, Math.Max(vert1.Position.Y, Math.Max(vert2.Position.Y, vert3.Position.Y)));
        }

        internal VertexData Vert0 { get; private set; }
        internal VertexData Vert1 { get; private set; }
        internal VertexData Vert2 { get; private set; }
        internal VertexData Vert3 { get; private set; }

        internal override int EstimatedSize
        {
            get { return 28; }// one primitive plus two verts
        }
    }
}
