﻿using System;
using System.IO;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.OS;
using RSG.Base.Profiling;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services.BoundsProcessor;
using RSG.SceneXml;

namespace RSG.Pipeline.BoundsProcessor
{
    class Program
    {
        #region Constants
        private static readonly int successExitCode_ = 0;
        private static readonly int errorExitCode_ = 1;

        private static readonly string configPathnameOptionName_ = "config";
        private static readonly string logDirectoryOptionName_ = "logdir";
        #endregion // Constants

        /// <summary>
        /// The main entry point for the application.
        /// Attribute [STAThread] for invoking the universal log from the right thread
        /// </summary>
        [STAThread]
        static int Main(string[] args)
        {
            LongOption[] opts = new LongOption[]
            {
                new LongOption(configPathnameOptionName_, LongOption.ArgType.Required,
                    "Configuration filename."),
                new LongOption(logDirectoryOptionName_, LongOption.ArgType.Optional,
                    "Log directory (optional, not recommended)."),
            };
            CommandOptions options = new CommandOptions(args, opts);

            if (!options.ContainsOption(configPathnameOptionName_))
            {
                Console.Error.WriteLine("Invalid arguments specified to Bounds Processor");
                return errorExitCode_;
            }

            string configPathanme = (string)options[configPathnameOptionName_];
            Config.TheConfig = BoundsProcessorConfig.Load(configPathanme);

            // Setup the profiler
            if (Config.TheConfig.Options.Profile)
                Profiling.TheProfiler = ProfilerFactory.CreateProfiler();
            else
                Profiling.TheProfiler = ProfilerFactory.CreateStubProfiler();

            ITask mainTask = Profiling.TheProfiler.StartTask("Main()");

            int exitCode = successExitCode_;

            string logFileDirectory = null;
            if (options.ContainsOption(logDirectoryOptionName_))
            {
                logFileDirectory = (string)options[logDirectoryOptionName_];
            }

            LogFactory.Initialize(logFileDirectory, false);
            LogFactory.CreateUniversalLogFile(Log.StaticLog);
            LogFactory.CreateApplicationConsoleLogTarget();

            ITask sceneCollectionTask = Profiling.TheProfiler.StartTask("Load scene collection");
            IContentTree contentTree = Factory.CreateTree(options.Branch);
            ContentTreeHelper contentTreeHelper = new ContentTreeHelper(contentTree);
            SceneCollection sceneCollection = new SceneCollection(options.Branch, contentTree, SceneCollectionLoadOption.Props);
            Profiling.TheProfiler.StopTask(sceneCollectionTask);

            // Flo: 07.03.14: perform spring cleaning before any task, related to B*1699820
            Config.TheConfig.Tasks.ForEach(task => BoundsProcessor.PrepareOutputDirectory(task.Output.Directory));

            foreach (BoundsProcessorTask task in Config.TheConfig.Tasks)
            {
                ITask processingTask = Profiling.TheProfiler.StartTask(String.Format("Task: {0}", task.Output.Name));

                string[] scenexmlPathnamesArray = task.Inputs.Select(input => input.SceneXmlPathname).ToArray();
                string[] boundsZipPathnamesArray = task.Inputs.Select(input => input.BoundsPathname).ToArray();
                string[] inputDirArray = task.Inputs.Select(input => input.InputDir).ToArray();
                string[] drawableListArray = task.Inputs.Select(input => input.DrawableList).ToArray();
                string[] additionalImaps = task.Inputs.SelectMany(input => input.AdditionalImaps).ToArray();
                string outputDirectory = task.Output.Directory;

                try
                {
                    BoundsProcessor.ProcessMap(
                        options,
                        task, 
                        sceneCollection, 
                        scenexmlPathnamesArray,
                        boundsZipPathnamesArray, 
                        inputDirArray, 
                        drawableListArray, 
                        additionalImaps, 
                        outputDirectory,
                        contentTreeHelper);
                }
                catch (RSG.Bounds.InvalidDataFormatException ex)
                {
                    Log.StaticLog.ToolException(ex, "Invalid data");
                    return errorExitCode_;
                }
                catch (RSG.Bounds.InvalidDataVersionException ex)
                {
                    Log.StaticLog.ToolException(ex, "Invalid data version");
                    return errorExitCode_;
                }
                catch (Exception ex)
                {
                    Log.StaticLog.ToolException(ex, "Unexpected error");
                    return errorExitCode_;
                }

                Profiling.TheProfiler.StopTask(processingTask);
            }

            if (LogFactory.HasError() || Log.StaticLog.HasErrors)
                exitCode = Constants.Exit_Failure;

            Profiling.TheProfiler.StopTask(mainTask);

            // TODO - We could use LogFactory.ProcessLogDirectory here according to DHM
            String profilerPathName = Path.Combine(LogFactory.ProcessLogDirectory, "bounds_processor", "profile.csv");
            Profiling.TheProfiler.WriteLog(profilerPathName);

            LogFactory.ApplicationShutdown();

            return exitCode;
        }
    }
}
