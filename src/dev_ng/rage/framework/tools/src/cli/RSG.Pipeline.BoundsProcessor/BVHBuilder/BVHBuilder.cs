﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.Bounds;

namespace RSG.Pipeline.BoundsProcessor.BVHBuilder
{
    /// <summary>
    /// To RSG.Bounds.BVH what StringBuilder is to String
    /// This class is used to collate OprhanedDatae.Primitive together, then convert to an optimised BVH via ToBVH()
    /// </summary>
    internal class BVHBuilder
    {
        internal BVHBuilder(uint numPerVertAttribs)
        {
            verts_ = new List<VertexData>();
            margin_ = defaultMargin_;
            materials_ = new List<BVHMaterial>();
            materialColours_ = new List<Vector4i>();
            primitives_ = new List<BVHPrimitive>();
            readEdgeNormals_ = defaultReadEdgeNormals_;
            secondSurfaceDisplacements_ = new List<float>();
            secondSurfaceMaxHeight_ = defaultSecondSurfaceMaxHeight_;
            shrunkVerts_ = new List<Vector3f>();
            estimatedSize_ = 0;
            numPerVertAttribs_ = numPerVertAttribs;
        }

        internal int NumPrimitives { get { return primitives_.Count; } }

        internal int EstimatedSize { get { return estimatedSize_; } }

        internal void Append(OrphanedData.Primitive orphanedPrimitive)
        {
            estimatedSize_ += orphanedPrimitive.EstimatedSize;

            // Add common stuff
            int materialIndex = materials_.Count;
            BVHMaterial newMaterial = new BVHMaterial(orphanedPrimitive.Material);
            materials_.Add(newMaterial);
            if (orphanedPrimitive.MaterialColour != null)
            {
                newMaterial.PaletteIndex = materialColours_.Count + 1;
                materialColours_.Add(orphanedPrimitive.MaterialColour);
            }
            if (orphanedPrimitive.SecondSurfaceDisplacements != null)
            {
                secondSurfaceDisplacements_.Add(orphanedPrimitive.SecondSurfaceDisplacements[0]);
                secondSurfaceDisplacements_.Add(orphanedPrimitive.SecondSurfaceDisplacements[1]);
                secondSurfaceDisplacements_.Add(orphanedPrimitive.SecondSurfaceDisplacements[2]);
            }
            if (orphanedPrimitive.ShrunkVerts != null)
            {
                shrunkVerts_.Add(orphanedPrimitive.ShrunkVerts[0]);
                shrunkVerts_.Add(orphanedPrimitive.ShrunkVerts[1]);
                shrunkVerts_.Add(orphanedPrimitive.ShrunkVerts[2]);
            }

            // Add type specific stuff
            if (orphanedPrimitive is OrphanedData.Triangle)
            {
                OrphanedData.Triangle orhpanedTriangle = (OrphanedData.Triangle)orphanedPrimitive;

                int index0 = verts_.Count;
                verts_.Add(orhpanedTriangle.Vert0);
                verts_.Add(orhpanedTriangle.Vert1);
                verts_.Add(orhpanedTriangle.Vert2);
                
                primitives_.Add(new BVHTriangle(index0, index0 + 1, index0 + 2, materialIndex));
                
            }
            else if (orphanedPrimitive is OrphanedData.Box)
            {
                OrphanedData.Box orhpanedBox = (OrphanedData.Box)orphanedPrimitive;

                int index0 = verts_.Count;
                verts_.Add(orhpanedBox.Vert0);
                verts_.Add(orhpanedBox.Vert1);
                verts_.Add(orhpanedBox.Vert2);
                verts_.Add(orhpanedBox.Vert3);
                
                primitives_.Add(new BVHBox(index0, index0 + 1, index0 + 2, index0 + 3, materialIndex));
            }
            else if (orphanedPrimitive is OrphanedData.Capsule)
            {
                OrphanedData.Capsule orhpanedCapsule = (OrphanedData.Capsule)orphanedPrimitive;

                int index0 = verts_.Count;
                verts_.Add(orhpanedCapsule.Vert0);
                verts_.Add(orhpanedCapsule.Vert1);

                primitives_.Add(new BVHCapsule(index0, index0 + 1, orhpanedCapsule.Radius, materialIndex));
            }
            else if (orphanedPrimitive is OrphanedData.Cylinder)
            {
                OrphanedData.Cylinder orhpanedCylinder = (OrphanedData.Cylinder)orphanedPrimitive;

                int index0 = verts_.Count;
                verts_.Add(orhpanedCylinder.Vert0);
                verts_.Add(orhpanedCylinder.Vert1);

                primitives_.Add(new BVHCylinder(index0, index0 + 1, orhpanedCylinder.Radius, materialIndex));
            }
            else if (orphanedPrimitive is OrphanedData.Quad)
            {
                OrphanedData.Quad orhpanedQuad = (OrphanedData.Quad)orphanedPrimitive;

                int index0 = verts_.Count;
                verts_.Add(orhpanedQuad.Vert0);
                verts_.Add(orhpanedQuad.Vert1);
                verts_.Add(orhpanedQuad.Vert2);
                verts_.Add(orhpanedQuad.Vert3);

                primitives_.Add(new BVHQuad(index0, index0 + 1, index0 + 2, index0 + 3, materialIndex));
            }
            else if (orphanedPrimitive is OrphanedData.Sphere)
            {
                OrphanedData.Sphere orhpanedSphere = (OrphanedData.Sphere)orphanedPrimitive;

                int index0 = verts_.Count;
                verts_.Add(orhpanedSphere.Vert0);

                primitives_.Add(new BVHSphere(index0, orhpanedSphere.Radius, materialIndex));
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        internal void AppendRange(IEnumerable<OrphanedData.Primitive> orphanedPrimitives)
        {
            foreach (OrphanedData.Primitive orphanedPrimitive in orphanedPrimitives)
                Append(orphanedPrimitive);
        }

        internal BVH ToBVH()
        {
            // The client is finished adding stuff, so we need to optimise the data before we return the BVH
            WeldVertices(Config.TheConfig.Options.VertexWeldTolerance);

            RemoveDegeneratePrimitives();
            RemoveUnreferencedVertices();

            MergeMaterialColours();
            MergeMaterials();

            Vector3f centreOfGravity = CalculateCOG();

            // Build a valid BVH from our processed data
            BVH result = new BVH(
                verts_.ToArray(),
                numPerVertAttribs_,
                centreOfGravity,
                margin_,
                materials_.Select(mat => mat.ToString()).ToArray(),// serialisation to string happens here!
                materialColours_.ToArray(),
                primitives_.ToArray(),
                readEdgeNormals_,
                secondSurfaceDisplacements_.ToArray(),
                secondSurfaceMaxHeight_,
                shrunkVerts_.ToArray());

            return result;
        }

        internal Geometry ToGeometry()
        {
            if (primitives_.OfType<BVHTriangle>().Count() != primitives_.Count)
            {
                throw new InvalidOperationException("Cannot convert to Geometry if the builder contains non-triangle primitives");
            }

            // The client is finished adding stuff, so we need to optimise the data before we return the BVH
            WeldVertices(Config.TheConfig.Options.VertexWeldTolerance);

            RemoveDegeneratePrimitives();
            RemoveUnreferencedVertices();

            MergeMaterialColours();
            MergeMaterials();

            Vector3f centreOfGravity = CalculateCOG();

            // Build a valid BVH from our processed data
            Geometry result = new Geometry(
                verts_.ToArray(),
                numPerVertAttribs_,
                centreOfGravity,
                margin_,
                materials_.Select(mat => mat.ToString()).ToArray(),// serialisation to string happens here!
                materialColours_.ToArray(),
                primitives_.ToArray(),
                readEdgeNormals_,
                secondSurfaceDisplacements_.ToArray(),
                secondSurfaceMaxHeight_,
                shrunkVerts_.ToArray());

            return result;
        }

        /// <summary>
        /// This method is accounts for around 85% of all processing.  It has been heavily optimised so take care before making changes here.
        /// </summary>
        /// <param name="tolerance"></param>
        private void WeldVertices(float tolerance)
        {
            if (secondSurfaceDisplacements_.Count == 0)
            {
                WeldVerticesIgnoreSecondSurface(tolerance);
            }
            else
            {
                WeldVerticesWithSecondSurface(tolerance);
            }
        }

        private void WeldVerticesIgnoreSecondSurface(float tolerance)
        {
            for (int vertIndexA = 0; vertIndexA < verts_.Count - 1; ++vertIndexA)
            {
                for (int vertIndexB = vertIndexA + 1; vertIndexB < verts_.Count; )
                {
                    if ((Math.Abs(verts_[vertIndexA].Position.X - verts_[vertIndexB].Position.X) < tolerance) &&
                        (Math.Abs(verts_[vertIndexA].Position.Y - verts_[vertIndexB].Position.Y) < tolerance) &&
                        (Math.Abs(verts_[vertIndexA].Position.Z - verts_[vertIndexB].Position.Z) < tolerance))
                    {
                        verts_.RemoveAt(vertIndexB);

                        // update the primitives
                        for (int primitiveIndex = 0; primitiveIndex < primitives_.Count; ++primitiveIndex)
                        {
                            primitives_[primitiveIndex].VertHasBeenCombined(vertIndexB, vertIndexA);
                        }
                    }
                    else
                    {
                        ++vertIndexB;
                    }
                }
            }
        }

        private void WeldVerticesWithSecondSurface(float tolerance)
        {
            for (int vertIndexA = 0; vertIndexA < verts_.Count - 1; ++vertIndexA)
            {
                for (int vertIndexB = vertIndexA + 1; vertIndexB < verts_.Count; )
                {
                    if ((Math.Abs(verts_[vertIndexA].Position.X - verts_[vertIndexB].Position.X) < tolerance) &&
                        (Math.Abs(verts_[vertIndexA].Position.Y - verts_[vertIndexB].Position.Y) < tolerance) &&
                        (Math.Abs(verts_[vertIndexA].Position.Z - verts_[vertIndexB].Position.Z) < tolerance) &&
                        (Math.Abs(secondSurfaceDisplacements_[vertIndexA] - secondSurfaceDisplacements_[vertIndexB]) < tolerance))
                    {
                        verts_.RemoveAt(vertIndexB);
                        secondSurfaceDisplacements_.RemoveAt(vertIndexB);

                        // update the primitives
                        for (int primitiveIndex = 0; primitiveIndex < primitives_.Count; ++primitiveIndex)
                        {
                            primitives_[primitiveIndex].VertHasBeenCombined(vertIndexB, vertIndexA);
                        }
                    }
                    else
                    {
                        ++vertIndexB;
                    }
                }
            }
        }

        private void MergeMaterialColours()
        {
            if (materialColours_.Count == 0)
                return;

            ColourRGB[] colours = new ColourRGB[materialColours_.Count];
            for (int materialColourIndex = 0; materialColourIndex < materialColours_.Count; ++materialColourIndex )
            {
                Vector4i materialColour = materialColours_[materialColourIndex];
                colours[materialColourIndex] = new ColourRGB(materialColour.X, materialColour.Y, materialColour.Z);
            }

            ColourQuantiser colourQuantiser = new ColourQuantiser();
            colourQuantiser.BuildOctree(colours);
            colourQuantiser.ReduceTreeToSpecifiedLeafCount(Config.TheConfig.Options.MaxMaterialColourPaletteSize);
            ColourRGB[] palette = colourQuantiser.CollapseToPaletteTable();

            // Quantise the colours
            for (int materialColourIndex = 0; materialColourIndex < materialColours_.Count; ++materialColourIndex)
            {
                int paletteIndex = colourQuantiser.GetClosestPaletteIndex(colourQuantiser.RootNode, colours[materialColourIndex]);
                ColourRGB quantisedColour = palette[paletteIndex];

                materialColours_[materialColourIndex].X = (int)quantisedColour.Red;
                materialColours_[materialColourIndex].Y = (int)quantisedColour.Green;
                materialColours_[materialColourIndex].Z = (int)quantisedColour.Blue;
            }

            // Merge identical colours together
            for (int materialColourIndexA = 0; materialColourIndexA < materialColours_.Count - 1; ++materialColourIndexA)
            {
                for (int materialColourIndexB = materialColourIndexA + 1; materialColourIndexB < materialColours_.Count; )
                {
                    if (ColoursAreEqual(materialColours_[materialColourIndexA], materialColours_[materialColourIndexB]))
                    {
                        CombineMaterialColours(materialColourIndexA, materialColourIndexB);
                    }
                    else
                    {
                        ++materialColourIndexB;
                    }
                }
            }
        }

        private bool ColoursAreEqual(Vector4i colourA, Vector4i colourB)
        {
            return (colourA.X == colourB.X &&
                    colourA.Y == colourB.Y &&
                    colourA.Z == colourB.Z && 
                    colourA.W == colourB.W);
        }

        private void CombineMaterialColours(int materialColourIndexA, int materialColourIndexB)
        {
            materialColours_.RemoveAt(materialColourIndexB);

            // update the materials
            for (int materialIndex = 0; materialIndex < materials_.Count; ++materialIndex)
            {
                if (materials_[materialIndex].PaletteIndex > 0)
                {
                    if (materials_[materialIndex].PaletteIndex == materialColourIndexB + 1)// palette index is '1 based'
                    {
                        materials_[materialIndex].PaletteIndex = materialColourIndexA + 1;// palette index is '1 based'
                    }
                    else if (materials_[materialIndex].PaletteIndex > materialColourIndexB + 1)// palette index is '1 based'
                    {
                        --materials_[materialIndex].PaletteIndex;
                    }
                }
            }
        }

        private void MergeMaterials()
        {
            for (int materialIndexA = 0; materialIndexA < materials_.Count - 1; ++materialIndexA)
            {
                for (int materialIndexB = materialIndexA + 1; materialIndexB < materials_.Count; )
                {
                    if (MaterialsAreIdenticalIgnoringID(materialIndexA, materialIndexB))
                    {
                        CombineMaterials(materialIndexA, materialIndexB);
                    }
                    else
                    {
                        ++materialIndexB;
                    }
                }
            }

            if (materials_.Count > maxNumMaterials_)
            {
                Log.Log__Error(
                    "There are {0} bound materials in a BVH after material merge.  The limit is {1}.  Reduce bound material complexity.",
                    materials_.Count, maxNumMaterials_);
            }
        }

        private bool MaterialsAreIdenticalIgnoringID(int materialIndexA, int materialIndexB)
        {
            return (materials_[materialIndexA].Name == materials_[materialIndexB].Name &&
                    materials_[materialIndexA].ProceduralModifier == materials_[materialIndexB].ProceduralModifier &&
                    materials_[materialIndexA].RoomID == materials_[materialIndexB].RoomID &&
                    materials_[materialIndexA].PedPopulation == materials_[materialIndexB].PedPopulation &&
                    materials_[materialIndexA].Flags == materials_[materialIndexB].Flags &&
                    materials_[materialIndexA].PaletteIndex == materials_[materialIndexB].PaletteIndex);
        }

        private void CombineMaterials(int materialIndexA, int materialIndexB)
        {
            materials_.RemoveAt(materialIndexB);

            // update the primitives
            for (int primitiveIndex = 0; primitiveIndex < primitives_.Count; ++primitiveIndex)
            {
                primitives_[primitiveIndex].MaterialHasBeenCombined(materialIndexB, materialIndexA);
            }
        }

        private void RemoveDegeneratePrimitives()
        {
            for (int primitiveIndex = 0; primitiveIndex < primitives_.Count; )
            {
                if (primitives_[primitiveIndex] is BVHTriangle && IsTriangleDegenerate((BVHTriangle)primitives_[primitiveIndex]))
                {
                    primitives_.RemoveAt(primitiveIndex);
                }
                else
                {
                    ++primitiveIndex;
                }
            }
        }

        private bool IsTriangleDegenerate(BVHTriangle triangle)
        {
            if (triangle.Index0 == triangle.Index1 || triangle.Index0 == triangle.Index2 || triangle.Index1 == triangle.Index2)
                return true;

            if (IsTriangleColinearByAngle(triangle, Config.TheConfig.Options.ColinearTriangleTolerance))
                return true;

            return false;
        }

        private bool IsTriangleColinearByAngle(BVHTriangle triangle, float tolerance)
        {
            Vector3f a = verts_[triangle.Index0].Position;
            Vector3f b = verts_[triangle.Index1].Position;
            Vector3f c = verts_[triangle.Index2].Position;

            Vector3f ab = b - a;
            ab.Normalise();
            Vector3f bc = c - b;
            bc.Normalise();

            float dot = Vector3f.Dot(ab, bc);
            float clampedDot = Math.Max(-1.0f, dot);
            clampedDot = Math.Min(1.0f, clampedDot);
            float theta = (float)Math.Acos((double)clampedDot);

            return (theta < tolerance);
        }

        private void RemoveUnreferencedVertices()
        {
            // Determine which verts have been referenced.
            bool[] referencedVerts = new bool[verts_.Count];

            foreach (BVHPrimitive primitive in primitives_)
            {
                foreach (int vertexIndex in primitive.GetVertexIndices())
                {
                    referencedVerts[vertexIndex] = true;
                }
            }

            // Remove any unloved verts.
            List<int> vertIndexesToRemove = new List<int>();
            for (int vertexIndex = 0; vertexIndex < verts_.Count; ++vertexIndex)
            {
                if (referencedVerts[vertexIndex] == false)
                {
                    // JWR - B* 290210 - get the index with the previous verts removed (if any)
                    // So, removing 1, 6, 9 => 1, 5, 7 (taking previous removals into account)
                    vertIndexesToRemove.Add(vertexIndex - vertIndexesToRemove.Count);
                }
            }

            foreach (int vertIndexToRemove in vertIndexesToRemove)
            {
                verts_.RemoveAt(vertIndexToRemove);
                if (secondSurfaceDisplacements_.Count > 0)
                {
                    secondSurfaceDisplacements_.RemoveAt(vertIndexToRemove);
                }

                foreach(BVHPrimitive primitive in primitives_)
                {
                    primitive.VertHasBeenCombined(vertIndexToRemove, vertIndexToRemove);// It can't be combined, as it isn't referenced anywhere
                }
            }
        }

        private Vector3f CalculateCOG()
        {
            float minX = float.MaxValue;
            float minY = float.MaxValue;
            float minZ = float.MaxValue;
            float maxX = float.MinValue;
            float maxY = float.MinValue;
            float maxZ = float.MinValue;

            foreach (var vert in verts_)
            {
                if (vert.Position.X < minX)
                    minX = vert.Position.X;
                if (vert.Position.Y < minY)
                    minY = vert.Position.Y;
                if (vert.Position.Z < minZ)
                    minZ = vert.Position.Z;
                if (vert.Position.X > maxX)
                    maxX = vert.Position.X;
                if (vert.Position.Y > maxY)
                    maxY = vert.Position.Y;
                if (vert.Position.Z > maxZ)
                    maxZ = vert.Position.Z;
            }

            // Centre of bounds is fine for static collision (actually, [0,0,0] is fine for static bounds, but this is more sensible)
            BoundingBox3f boundingBox = new BoundingBox3f(new Vector3f(minX, minY, minZ), new Vector3f(maxX, maxY, maxZ));
            return boundingBox.Centre();
        }

        private readonly static float defaultMargin_ = RSG.ManagedRage.PhysicsConstants.ConcaveDistanceMargin;
        private readonly static uint defaultReadEdgeNormals_ = 1;
        private readonly static float defaultSecondSurfaceMaxHeight_ = 1;
        private readonly static int maxNumMaterials_ = 255;

        // BVH data
        private List<VertexData> verts_;
        private uint numPerVertAttribs_;
        private float margin_;
        private List<BVHMaterial> materials_;
        private List<Vector4i> materialColours_;
        private List<BVHPrimitive> primitives_;
        private uint readEdgeNormals_;
        private List<float> secondSurfaceDisplacements_;
        private float secondSurfaceMaxHeight_;
        private List<Vector3f> shrunkVerts_;

        // Metadata
        private int estimatedSize_;
    }
}
