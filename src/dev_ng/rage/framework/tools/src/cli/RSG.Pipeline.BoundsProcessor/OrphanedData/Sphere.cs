﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Base.Math;
using RSG.Bounds;

namespace RSG.Pipeline.BoundsProcessor.OrphanedData
{
    /// <summary>
    /// A self contained sphere primitive
    /// </summary>
    internal class Sphere : Primitive
    {
        internal Sphere(BNDFile.CollisionType collisionType,
                        string material,
                        Vector4i materialColour,
                        float[] secondSurfaceDisplacements,
                        Vector3f[] shrunkVerts,
                        VertexData vert0,
                        float radius,
                        uint numPerVertAttribs)
            : base(collisionType, material, materialColour, secondSurfaceDisplacements, shrunkVerts, numPerVertAttribs)
        {
            Vert0 = vert0;
            Radius = radius;

            CentreXY = new Vector2f(vert0.Position.X, vert0.Position.Y);
            MinX = vert0.Position.X - radius;
            MaxX = vert0.Position.X + radius;
            MinY = vert0.Position.Y - radius;
            MaxY = vert0.Position.Y + radius;
        }

        internal VertexData Vert0 { get; private set; }
        internal float Radius { get; private set; }

        internal override int EstimatedSize
        {
            get { return 22; }// one primitive plus one vert
        }
    }
}
