//
// File: IValidator.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of IValidator.cs class
//

using System;
using System.Collections.Generic;
using RSG.SceneXml;

namespace MapExportValidation.Validation
{

    /// <summary>
    /// SceneXml validator options dictionary.
    /// </summary>
    public class ValidatorOptions : Dictionary<String, Object>
    {
    }

    /// <summary>
    /// Base SceneXml Validator interface.
    /// </summary>
    /// Errors are of the fatal variety.  Export should fail when we have 
    /// errors.
    /// 
    /// Warnings are not fatal.  Export should continue irrespective of the
    /// number of warnings.
    /// 
    public interface IValidator
    {
        #region Properties
        /// <summary>
        /// Validator enabled flag (often constant).
        /// </summary>
        bool Enabled
        {
            get;
        }

        /// <summary>
        /// Validator errors per ObjectDef.
        /// </summary>
        Dictionary<TargetObjectDef, List<String>> Errors
        {
            get;
        }

        /// <summary>
        /// Validator warnings per ObjectDef.
        /// </summary>
        Dictionary<TargetObjectDef, List<String>> Warnings
        {
            get;
        }
        
        /// <summary>
        /// Validator infos per ObjectDef.
        /// </summary>
        Dictionary<TargetObjectDef, List<String>> Infos
        {
            get;
        }
        #endregion // Properties
    }

} // MapExportValidation.Validation namespace

// IValidator.cs
