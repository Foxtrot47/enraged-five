//
// File:: DuplicateAssets.cs
// Author:: David Muir <david.muir@rockstarnorth.com>
//
// Description::
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using RSG.Base.Configuration;
using RSG.Pipeline.Content;
using RSG.SceneXml;
using RSG.SceneXml.MapExport.Util;
using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{

    /// <summary>
    /// 
    /// </summary>
    internal class DuplicateAssets : ISceneValidator
    {
        #region IObjectDefValidator Properties
        /// <summary>
        /// 
        /// </summary>
        public override bool Enabled
        {
            get { return false; }
        }
        #endregion // IObjectDefValidator Properties

        #region IObjectDefValidator Methods
        /// <summary>
        /// Validate scene for duplicate assets (object and txd names).
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="scene"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public override bool Validate(IBranch branch, ContentTreeHelper treeHelper, Scene scene, ValidatorOptions options)
        {
            bool result = true;

            Debug.Assert(options.ContainsKey("build"), "No build directory defined in validator options.  Contact tools.");
            if (!options.ContainsKey("build"))
                return (false);
            Debug.Assert((options["build"] as String).Length > 0, "Invalid build directory defined in validator options.  Contact tools.");
            if (0 == (options["build"] as String).Length)
                return (false);
            IDEReaderBasic ideData = new IDEReaderBasic(options["build"] as String);
            
            // Walk scene validating against Object and TXD names.
            foreach (TargetObjectDef obj in scene.Walk(Scene.WalkMode.DepthFirst))
            {
                if (obj.IsXRef())
                    continue;
                if (!obj.IsObject())
                    continue;
                String txdname = obj.GetAttribute(AttrNames.OBJ_TXD, AttrDefaults.OBJ_TXD);
                String objname = obj.GetObjectName();
                objname = objname.ToLower();
                txdname = txdname.ToLower();

                // Loop through all IDE files catching duplicate Object names.
                foreach (KeyValuePair<String, List<String>> ideFile in ideData.ObjectNames)
                {
                    if (Path.GetFileNameWithoutExtension(scene.Filename) == ideFile.Key)
                        continue; // Skip own IDE file as validating against SceneXml.

                    if (ideFile.Value.IndexOf(objname) > -1)
                    {
                        this.AddError(obj, String.Format("Object name {0} is a duplicate, also defined in map section {1}.",
                            objname, ideFile.Key));
                        result = false;
                    }
                }

                // Loop through all IDE files catching duplicate TXD names.
                foreach (KeyValuePair<String, List<String>> ideFile in ideData.TXDNames)
                {
                    if (Path.GetFileNameWithoutExtension(scene.Filename) == ideFile.Key)
                        continue; // Skip own IDE file as validating against SceneXml.

                    // DHM FIX ME: this check seems to fail.
                    if (ideFile.Value.IndexOf(txdname) > -1)
                    {
                        this.AddError(obj, String.Format("TXD name {0} is a duplicate, also defined in map section {1}.",
                            txdname, ideFile.Key));
                        result = false;
                    }
                }
            }

            return (result);
        }
        #endregion // IObjectDefValidator Methods
    }

} // MapExportValidation.Validator namespace

