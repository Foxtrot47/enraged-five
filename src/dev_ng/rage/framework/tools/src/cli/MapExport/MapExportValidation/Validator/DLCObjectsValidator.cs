﻿using MapExportValidation.Validation;
using RSG.Base.Configuration;
using RSG.SceneXml;

namespace MapExportValidation.Validator
{
    /// <summary>
    /// If we are in a Core Project, we don't want to have DLC object polluting the scene xml
    /// see url:bugstar:2023920 for reference
    /// </summary>
    internal class DLCObjectsValidator : IObjectDefValidator
    {
        public override bool Validate(IBranch branch, TargetObjectDef obj, Scene scene, ValidatorOptions options)
        {
            bool isCoreProject = !branch.Project.IsDLC;

            if (isCoreProject && obj.IsNewDLCObject())
            {
                this.AddError(obj, string.Format("This object is tagged as New Dlc in a non DLC project. This will lead to errors at Runtime."));
            }

            return true;
        }
    }
}
