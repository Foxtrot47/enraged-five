﻿//
// File: Program.cs
// Author: Derek Ward <derek.ward@rockstarnorth.com>
// Description: Implementation of Program class
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.OS;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services;
using RSG.SceneXml;
using System.IO;

namespace SceneXmlEnumMaker
{

    /// <summary>
    /// Main Program Static Class defining entry-point.
    /// </summary>
    static class Program
    {
        #region Constants
        private static readonly int EXIT_SUCCESS = 0;
        private static readonly int EXIT_WARNING = 1;
        private static readonly int EXIT_ERROR = 2;
        private static readonly int EXIT_CRITIC = 3;

        // Generic Options
        private static readonly String OPTION_LOG = "log";
        private static readonly String OPTION_UNILOG = "unilog";
        private static readonly String OPTION_PROFILE = "profile";
        private static readonly String OPTION_NOPOPUP = "nopopups";
        private static readonly String OPTION_BUILD = "build";
        private static readonly String OPTION_DEBUG = "debug";
        private static readonly String OPTION_DYNAMIC_ONLY = "dynamiconly";
        #endregion // Constants

        #region Static Member Data
        private static readonly String AUTHOR = "Derek Ward";
        private static readonly String EMAIL = "derek.ward@rockstarnorth.com";

        private static bool DynamicOnly = false;
        #endregion // Static Member Data

        #region Entry-Point
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static int Main(String[] args)
        {
            int exit_code = Constants.Exit_Success;

            #region Option Parsing and Setup
            // Define our executable options.  Stick with the long options
            // (e.g. --opt) rather than the single character versions.
            CommandOptions options = new CommandOptions(args, new LongOption[]
            {
                new LongOption(OPTION_LOG, LongOption.ArgType.Required, string.Empty),
                new LongOption(OPTION_UNILOG, LongOption.ArgType.Required, string.Empty),
                new LongOption(OPTION_PROFILE, LongOption.ArgType.None, string.Empty),
                new LongOption(OPTION_NOPOPUP, LongOption.ArgType.None, string.Empty),
                new LongOption(OPTION_BUILD, LongOption.ArgType.Required, string.Empty),
                new LongOption(OPTION_DEBUG, LongOption.ArgType.None, string.Empty),
                new LongOption(OPTION_DYNAMIC_ONLY, LongOption.ArgType.None, string.Empty)
            });

            // Parse our options.
            LogFactory.Initialize();
            LogFactory.CreateUniversalLogFile(Log.StaticLog);
            LogFactory.CreateApplicationConsoleLogTarget();

            bool nopopups = options.ContainsOption(OPTION_NOPOPUP) ? true : false;

            // Validate our options.
            Debug.Assert(options.TrailingArguments.Any(),
                "Must specify at least one trailing argument SceneXml XML file.");
            if (!options.TrailingArguments.Any())
                return (EXIT_CRITIC);
//            Debug.Assert(!episodic, "Episodic export is currently not enabled.");

            // DHM; not neccessarily clever...
            if (nopopups)
                Debug.Listeners.Clear();
            #endregion // Option Parsing and Setup

            try
            {
                IContentTree tree = Factory.CreateTree(options.Branch);
                ContentTreeHelper contentHelper = new ContentTreeHelper(tree);

                String builddir = (options[OPTION_BUILD] as String);
                DynamicOnly = (options.ContainsOption(OPTION_DYNAMIC_ONLY));

                IDictionary<String, IProject> allProjects = options.Config.AllProjects();

                foreach (String filename in options.TrailingArguments)
                {
                    String fullPathFilename = Path.GetFullPath(filename);

                    // Fine the project for this file.
                    IProject project = null;

                    foreach (KeyValuePair<String, IProject> kvp in allProjects)
                    {
                        IProject projectForFile = kvp.Value;
                        foreach (KeyValuePair<string,IBranch> branchKvp in projectForFile.Branches)
                        {
                            IBranch branch = branchKvp.Value;

                            String fullPathRsExport = Path.GetFullPath(branch.Export);

                            if (fullPathFilename.StartsWith(fullPathRsExport,StringComparison.CurrentCultureIgnoreCase))
                            {
                                project = branch.Project;
                                break;
                            }
                        }
                    }

                    if (project != null)
                    {
                        Scene scene = new Scene(tree, contentHelper, filename, LoadOptions.All);

                        Log.Log__Message("In project : {0}", project.IsDLC ? "DLC" : "non DLC");
                        foreach (TargetObjectDef obj in scene.Objects)
                        {
                            ProcessObjectDef(project, obj);   // Recursive                
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                // Log the exception.
                Log.Log__Exception(ex, "Unhandled exception during map metadata serialisation.");

                // Display exception dialog only if we are allowed.
                if (!nopopups)
                {
                    RSG.Base.Windows.ExceptionStackTraceDlg stackDlg =
                        new RSG.Base.Windows.ExceptionStackTraceDlg(ex, AUTHOR, EMAIL);
                    stackDlg.Show();
                }
            }

            if (Log.StaticLog.HasWarnings)
                exit_code = Constants.Exit_Success;
            if (Log.StaticLog.HasErrors)
                exit_code = Constants.Exit_Failure;
            
            Environment.ExitCode = exit_code;
            LogFactory.ApplicationShutdown();

            return (exit_code);
        }
        #endregion // Entry-Point

        #region Private Methods
        /// <summary>
        /// Resurce through object we are interested in and spit out their name.
        /// </summary>
        static private void ProcessObjectDef(IProject project, TargetObjectDef obj)
        {
            if (obj.DontExport() || obj.DontExportIDE())
                return;

            if (DynamicOnly && !obj.IsDynObject())
                return;

            if (obj.IsObject() || obj.IsMilo())
            {
                String name = RSG.SceneXml.MapExport.MapAsset.AppendMapPrefixIfRequired(project, obj.Name);
                Console.WriteLine("{0}", name);
            }

            foreach (TargetObjectDef obj2 in obj.Children)
            {
                ProcessObjectDef(project, obj2);
            }
        }
        #endregion // Private Methods
    }

} // MapExportIDEIPL namespace

// Program.cs
