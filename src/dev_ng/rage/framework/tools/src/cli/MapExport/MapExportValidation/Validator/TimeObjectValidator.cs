﻿using System;
using System.Collections.Generic;
using RSG.Base.Configuration;
using RSG.SceneXml;
using RSG.SceneXml.MapExport;
using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{

    /// <summary>
    /// 
    /// </summary>
    internal class TimeObjectValidator : IObjectDefValidator
    {
        #region Controller Methods
        /// <summary>
        /// Validate a single Object for IPL Group.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        ///
        public override bool Validate(IBranch branch, TargetObjectDef obj, RSG.SceneXml.Scene scene, ValidatorOptions options)
        {
            if (!obj.IsObject())
                return (true);

            if (obj.DontExport())
                return (true);

            bool result = true;

            // Check only the HD objects hierarchy
            if (obj.HasLODParent() && !obj.HasLODChildren() && obj.IsTimeObject())
            {
                uint timeFlags = Flags.GetTimeFlags(obj);
                TargetObjectDef parentObj = obj;

                // Walk up the LOD hierarchy
                while (parentObj.HasLODParent())
                {
                    parentObj = parentObj.LOD.Parent;

                    if (parentObj.IsContainerLODRefObject())
                        break;

                    uint parentTimeFlags = Flags.GetTimeFlags(parentObj);

                    // First make sure that the parent is also a time specific object.
                    if (!parentObj.IsTimeObject())
                    {
                        string error = String.Format("HD object ({0}) is a timed object but the LOD parent ({1}) is not setup as one.", obj.Name, parentObj.Name);
                        this.AddError(parentObj, error);

                        result = false;
                    }

                    // Then make sure the parent has the same time flags as the HD object.
                    if (parentTimeFlags != timeFlags)
                    {
                        string error = String.Format("HD object ({0}) and its LOD parent ({1}) time flags do not match. Make sure all objects in the LOD hierarchy have the same hours set.", obj.Name, parentObj.Name);
                        this.AddError(parentObj, error);

                        result = false;
                    }
                }
            }

            return result;
        }
        #endregion
    }
}
