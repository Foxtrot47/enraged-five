//
// File: ISceneValidator.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of ISceneValidator abstract class.
//

using System;
using System.Collections.Generic;
using RSG.Base.Configuration;
using RSG.Pipeline.Content;
using RSG.SceneXml;

namespace MapExportValidation.Validation
{

    /// <summary>
    /// SceneXml.Scene Validator Interface
    /// </summary>
    /// This interface should be implemented by validation algorithms that run
    /// over the entire SceneXml.Scene object.
    /// 
    /// For validation algorithms that do per-ObjectDef validation then it may
    /// be more appropriate (and quicker) to implement IObjectDefValidator
    /// instead.
    /// 
    /// <seealso cref="IValidator"/>
    /// <seealso cref="IObjectDefValidator"/>
    /// 
    public abstract class ISceneValidator : IValidator
    {
        #region Properties and Associated Member Data
        /// <summary>
        /// Validator enabled flag (individual validators can override).
        /// </summary>
        public virtual bool Enabled
        {
            get { return true; }
        }

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<TargetObjectDef, List<String>> Errors
        {
            get { return m_dErrors; }
            private set { m_dErrors = value; }
        }
        private Dictionary<TargetObjectDef, List<String>> m_dErrors;

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<TargetObjectDef, List<String>> Warnings
        {
            get { return m_dWarnings; }
            private set { m_dWarnings = value; }
        }
        private Dictionary<TargetObjectDef, List<String>> m_dWarnings;

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<TargetObjectDef, List<String>> Infos
        {
            get { return m_dInfos; }
            private set { m_dInfos = value; }
        }
        private Dictionary<TargetObjectDef, List<String>> m_dInfos;

        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ISceneValidator()
        {
            this.Errors = new Dictionary<TargetObjectDef, List<String>>();
            this.Warnings = new Dictionary<TargetObjectDef, List<String>>();
            this.Infos = new Dictionary<TargetObjectDef, List<String>>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="scene"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public abstract bool Validate(IBranch branch, ContentTreeHelper treeHelper, Scene scene, ValidatorOptions options);
        #endregion // Controller Methods

        #region Protected Methods
        /// <summary>
        /// Add an error to our error list.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="error"></param>
        protected virtual void AddError(TargetObjectDef obj, String error)
        {
            if (!this.Errors.ContainsKey(obj))
                this.Errors.Add(obj, new List<String>());
            this.Errors[obj].Add(error);
        }

        /// <summary>
        /// Add a warning to our warning list.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="warning"></param>
        protected virtual void AddWarning(TargetObjectDef obj, String warning)
        {
            if (!this.Warnings.ContainsKey(obj))
                this.Warnings.Add(obj, new List<String>());
            this.Warnings[obj].Add(warning);
        }

        /// <summary>
        /// Add a warning to our warning list.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="warning"></param>
        protected virtual void AddInfo(TargetObjectDef obj, String info)
        {
            if (!this.Infos.ContainsKey(obj))
                this.Infos.Add(obj, new List<String>());
            this.Infos[obj].Add(info);
        }

        #endregion // Protected Methods
    }

} // MapExportValidationValidation

// ISceneValidator.cs
