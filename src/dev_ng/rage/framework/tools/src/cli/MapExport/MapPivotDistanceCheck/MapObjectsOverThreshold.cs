﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using RSG.SceneXml;

namespace MapPivotDistanceCheck
{
    public partial class MapObjectsOverThreshold : Form
    {
        public MapObjectsOverThreshold(List<ObjectOutput> objects)
        {            
            InitializeComponent();
            this.dgvObjectsOverThreshold.DataSource = objects;
            this.dgvObjectsOverThreshold.AutoResizeColumns();
            this.dgvObjectsOverThreshold.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
        }
    }
}
