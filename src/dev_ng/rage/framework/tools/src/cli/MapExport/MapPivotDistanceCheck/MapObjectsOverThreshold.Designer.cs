﻿namespace MapPivotDistanceCheck
{
    partial class MapObjectsOverThreshold
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MapObjectsOverThreshold));
            this.dgvObjectsOverThreshold = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvObjectsOverThreshold)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvObjectsOverThreshold
            // 
            this.dgvObjectsOverThreshold.AllowUserToAddRows = false;
            this.dgvObjectsOverThreshold.AllowUserToDeleteRows = false;
            this.dgvObjectsOverThreshold.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvObjectsOverThreshold.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvObjectsOverThreshold.Location = new System.Drawing.Point(0, 0);
            this.dgvObjectsOverThreshold.Name = "dgvObjectsOverThreshold";
            this.dgvObjectsOverThreshold.ReadOnly = true;
            this.dgvObjectsOverThreshold.Size = new System.Drawing.Size(834, 362);
            this.dgvObjectsOverThreshold.TabIndex = 0;
            // 
            // MapObjectsOverThreshold
            // 
            this.ClientSize = new System.Drawing.Size(834, 362);
            this.Controls.Add(this.dgvObjectsOverThreshold);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MapObjectsOverThreshold";
            this.Text = "Map Object Pivot Position Checker";
            this.TopMost = true;
            ((System.ComponentModel.ISupportInitialize)(this.dgvObjectsOverThreshold)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvObjectsOverThreshold;
    }
}

