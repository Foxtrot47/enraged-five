﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using RSG.Bounds;
using RSG.SceneXml;

namespace MapExportValidation.Validator
{
    internal class CollisionObject
    {
        internal enum CollisionObjectType
        {
            Box,
            Cylinder,
            Capsule,
            Mesh,
            Sphere,
            Unknown
        }

        internal CollisionObject(TargetObjectDef objectdef)
        {
            if(objectdef == null)
                throw new ArgumentNullException("objectdef");

            if(!objectdef.IsCollision())
                throw new ArgumentException(
                    String.Format("CollisionObject instance cannot be created from non collision object '{0}'", objectdef.Name), 
                    "objectdef");

            CollisionType = GetCollisionTypeFromObjectDef(objectdef);
            ObjectType = GetObjectTypeFromObjectDef(objectdef);
        }

        internal BNDFile.CollisionType CollisionType { get; private set; }
        internal CollisionObjectType ObjectType { get; private set; }

        private BNDFile.CollisionType GetCollisionTypeFromObjectDef(TargetObjectDef objectDef)
        {
            BNDFile.CollisionType result = 0;

            if (!objectDef.UserProperties.ContainsKey("weapons") || (string)objectDef.UserProperties["weapons"] == "true")
            {
                result |= BNDFile.CollisionType.Weapons;
            }
            if (!objectDef.UserProperties.ContainsKey("mover") || (string)objectDef.UserProperties["mover"] == "true")
            {
                result |= BNDFile.CollisionType.Mover;
            }
            if (objectDef.UserProperties.ContainsKey("river") && (string)objectDef.UserProperties["river"] == "true")
            {
                result |= BNDFile.CollisionType.River;
            }
            if (objectDef.UserProperties.ContainsKey("deep_material_surface") && (string)objectDef.UserProperties["deep_material_surface"] == "true")
            {
                result |= BNDFile.CollisionType.DeepMaterialSurface;
            }
            if (objectDef.UserProperties.ContainsKey("foliage") && (string)objectDef.UserProperties["foliage"] == "true")
            {
                result |= BNDFile.CollisionType.Foliage;
            }
            if (objectDef.UserProperties.ContainsKey("stair_slope") && (string)objectDef.UserProperties["stair_slope"] == "true")
            {
                result |= BNDFile.CollisionType.StairSlope;
            }
            if (objectDef.UserProperties.ContainsKey("material") && (string)objectDef.UserProperties["material"] == "true")
            {
                result |= BNDFile.CollisionType.MaterialOnly;
            }

            // OK, so the horse, cover and vehicle flags are a bit special.
            // They were added after others so to prevent migration issues they are only implied if 'mover' is on
            if (result.HasFlag(BNDFile.CollisionType.Mover))
            {
                if (!objectDef.UserProperties.ContainsKey("horse") || (string)objectDef.UserProperties["horse"] == "true")
                {
                    result |= BNDFile.CollisionType.Horse;
                }
                if (!objectDef.UserProperties.ContainsKey("cover") || (string)objectDef.UserProperties["cover"] == "true")
                {
                    result |= BNDFile.CollisionType.Cover;
                }
                if (!objectDef.UserProperties.ContainsKey("vehicle") || (string)objectDef.UserProperties["vehicle"] == "true")
                {
                    result |= BNDFile.CollisionType.Vehicle;
                }
            }
            else
            {
                if (objectDef.UserProperties.ContainsKey("horse") && (string)objectDef.UserProperties["horse"] == "true")
                {
                    result |= BNDFile.CollisionType.Horse;
                }
                if (objectDef.UserProperties.ContainsKey("cover") && (string)objectDef.UserProperties["cover"] == "true")
                {
                    result |= BNDFile.CollisionType.Cover;
                }
                if (objectDef.UserProperties.ContainsKey("vehicle") && (string)objectDef.UserProperties["vehicle"] == "true")
                {
                    result |= BNDFile.CollisionType.Vehicle;
                }
            }

            return result;
        }

        private CollisionObjectType GetObjectTypeFromObjectDef(TargetObjectDef objectdef)
        {
            if (String.Compare(objectdef.Class, "col_box", true) == 0)
                return CollisionObjectType.Box;
            else if (String.Compare(objectdef.Class, "col_capsule", true) == 0)
                return CollisionObjectType.Capsule;
            else if (String.Compare(objectdef.Class, "col_cylinder", true) == 0)
                return CollisionObjectType.Cylinder;
            else if (String.Compare(objectdef.Class, "col_mesh", true) == 0)
                return CollisionObjectType.Mesh;
            else if (String.Compare(objectdef.Class, "col_sphere", true) == 0)
                return CollisionObjectType.Sphere;

            return CollisionObjectType.Unknown;
        }
    }
}
