﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.SceneXml;
using RSG.SceneXml.Material;
using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{
    /// <summary>
    /// Time cycle validator.
    /// </summary>
    /// This validator ensures that any time cycle modifier is suitable for export
    ///
    class TimeCycleModifier : IObjectDefValidator
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public TimeCycleModifier()
        {
            validTimeCycleNames_ = null;
        }
        #endregion // Constructor(s)

        #region IObjectDefValidator Methods

        /// <summary>
        /// Validate a single Object for its suitability as a time cycle modifier.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Validate(IBranch branch, TargetObjectDef objectDef, RSG.SceneXml.Scene scene, ValidatorOptions options)
        {
            bool result = true;

            string modifierId = null;
            if (objectDef.IsTimeCycleBox())
            {
                modifierId = (string)objectDef.GetAttribute(AttrNames.TWODFX_TCYC_BOX_IDSTRING, AttrDefaults.TWODFX_TCYC_BOX_IDSTRING);
            }
            else if (objectDef.IsTimeCycleSphere())
            {
                modifierId = (string)objectDef.GetAttribute(AttrNames.TWODFX_TCYC_SPHERE_NAME, AttrDefaults.TWODFX_TCYC_SPHERE_NAME);
            }
            if (null == modifierId)
                return true;

            if (branch.Project.IsDLC)
            {
                string timeCycleDataDirectory = System.IO.Path.Combine(branch.Common, "data/timecycle");

                if (Directory.Exists(timeCycleDataDirectory) && Directory.GetFiles(timeCycleDataDirectory, "timecycle_mods*.xml").Any())
                {
                    result = VerifyModifierID(branch, branch.Project, modifierId, objectDef);
                    return result;
                }
            }

            result = result && VerifyModifierID(branch.Project.Config.CoreProject.DefaultBranch, branch.Project, modifierId, objectDef);

            return result;
        }
        #endregion // IObjectDefValidator Methods

        private bool VerifyModifierID(IBranch branch, IProject project, String modifierID, TargetObjectDef objectDef)
        {
            bool result = true;

            if (validTimeCycleNames_ == null)// Load the data the first time we need it (most map sections don't need this check)
            {
                LoadTimeCycleModsData(branch);

                if (!hasValidTimeCycleData_)// Warn once if there was a problem loading the data
                {
                    string warningText = "Unable to load time cycle modifier data.  Time cycle objects may be invalid.";
                    AddWarning(objectDef, warningText);
                }
            }

            if (!hasValidTimeCycleData_)
            {
                return true;// We failed to load data, so just assume everything is ok
            }

            string modifierIdLower = modifierID.ToLower();

            if (!validTimeCycleNames_.Contains(modifierIdLower) && !project.IsDLC)
            {
                string errorText = String.Format("Time Cycle '{0}' refers to an invalid modifier '{1}'.", objectDef.Name, modifierID);
                AddError(objectDef, errorText);
                result = false;
            }
            else if (!validTimeCycleNames_.Contains(modifierIdLower) && project.IsDLC)
            {
                // For DLC we often don't have a way to get back TU common paths (at the moment)
                // so we just warn for now.
                string warningText = String.Format("Time Cycle '{0}' refers to an invalid modifier '{1}'.", objectDef.Name, modifierID);
                AddWarning(objectDef, warningText);
            }

            return result;
        }

        /// <summary>
        /// Loads the timecycle_mods data from data/timecycle (if there is any) and populates validTimeCycleNames_ and hasValidTimeCycleData_
        /// </summary>
        /// <param name="branch"></param>
        private void LoadTimeCycleModsData(IBranch branch)
        {
            validTimeCycleNames_ = new SortedSet<string>();
            hasValidTimeCycleData_ = true;

            try
            {
                string timeCycleDataDirectory = System.IO.Path.Combine(branch.Common, "data/timecycle");
                foreach (string timeCycleConfigPathname in System.IO.Directory.EnumerateFiles(timeCycleDataDirectory, "timecycle_mods*.xml"))
                {
                    XDocument doc = XDocument.Load(timeCycleConfigPathname);
                    foreach (XElement modifierElement in doc.Root.Elements("modifier"))
                    {
                        XAttribute modifierNameAttribute = modifierElement.Attribute("name");
                        if (modifierNameAttribute != null)
                        {
                            validTimeCycleNames_.Add(modifierNameAttribute.Value.ToLower());
                        }
                    }
                }
            }
            catch (System.Xml.XmlException)// if the local timecycle_mods data is knackered, we just refrain from checking objects and warn
            {
                validTimeCycleNames_.Clear();
                hasValidTimeCycleData_ = false;
            } 
        }

        private SortedSet<string> validTimeCycleNames_;
        private bool hasValidTimeCycleData_;
    }
}
