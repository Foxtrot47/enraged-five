﻿//
// File: LODHierarchy.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of LODHierarchy.cs class
//

using System;
using System.Collections.Generic;
using System.Linq;

using RSG.Base.Configuration;
using RSG.Base.Math;
using RSG.SceneXml;
using RSG.SceneXml.MapExport.Util;

using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{
    internal class RefPropValidator : IObjectDefValidator
    {
        #region IObjectDefValidator Methods
        public override bool Validate(IBranch branch, TargetObjectDef obj, RSG.SceneXml.Scene scene, ValidatorOptions options)
        {
            bool result = true;

            if (obj.IsRefObject() && obj.RefObject != null)
            {
                bool forceZeroPriority = obj.RefObject.GetAttribute(AttrNames.OBJ_FORCE_ZERO_PRIORITY, AttrDefaults.OBJ_FORCE_ZERO_PRIORITY);
                int priority = obj.GetAttribute(AttrNames.OBJ_PRIORITY, AttrDefaults.OBJ_PRIORITY);

                if (forceZeroPriority && priority != 0)
                {
                    this.AddError(obj, 
                        String.Format("Ref object '{0}' has its priority set to {1}, but the source prop '{2}' is flagged to prevent non-zero priorities.  " + 
                                      "To fix this you can either set the priority to zero or uncheck the 'force 0 priority' flag on the source prop.",
                            obj.Name, priority, obj.RefObject.Name));
                    result = false;
                }
            }

            return result;
        }
        #endregion
    }
}
