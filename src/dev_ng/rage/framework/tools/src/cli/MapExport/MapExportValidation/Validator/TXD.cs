//
// File: TXD.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Texture dictionary validation classes.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Collections;
using RSG.Base.Configuration;
using RSG.Pipeline.Content;
using RSG.SceneXml;
using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{

    /// <summary>
    /// TXD validator for "Gta Object" attribute class objects.
    /// </summary>
    /// Objects should always have a TXD set from the default "CHANGEME".
    /// This is a fatal error.
    /// 
    internal class TXD : IObjectDefValidator
    {
        #region Controller Methods
        /// <summary>
        /// Validate a single Object for TXD name.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Validate(IBranch branch, TargetObjectDef obj, RSG.SceneXml.Scene scene, ValidatorOptions options)
        {
            if (obj.HasParent() && obj.Parent.IsObject())
                return (true); // Ignore as its not an exportable drawable.
            if (!obj.IsObject())
                return (true);
            // InternalRef and XRef are not checked here.
            if (obj.IsInternalRef())
                return (true);
            if (obj.IsXRef())
                return (true);
            if (obj.IsRefObject())
                return (true);
            if (obj.IsRefInternalObject())
                return (true);

            String txd = obj.GetAttribute(AttrNames.OBJ_TXD, AttrDefaults.OBJ_TXD);
            if (0 == String.Compare(txd, AttrDefaults.OBJ_TXD, true))
            {
                this.AddError(obj, String.Format("Object {0} has default TXD set ({1}).", 
                    obj.Name, AttrDefaults.OBJ_TXD));
                return (false);
            }
            return (true);
        }
        #endregion // Controller Methods
    }

    /// <summary>
    /// TXD validator between LOD levels.
    /// </summary>
    /// Ref: see GTA5 Bug #144395 for checking that a TXD doesn't cross a
    /// LOD Level bound.
    /// 
    internal class TXDLodLevel : ISceneValidator
    {
        #region Constants
        private const String HREF_TXD_SHARED_ACROSS_LEVELS =
            "https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#Invalid_LOD_Level";
        #endregion // Constants

        /// <summary>
        /// Walk the scene checking TXD and Lod Level; ensure that a TXD is 
        /// only used in a single LOD level.
        /// </summary>
        /// <param name="scene"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public override bool Validate(IBranch branch, ContentTreeHelper treeHelper, Scene scene, ValidatorOptions options)
        {
            bool result = true;
            Dictionary<String, List<TargetObjectDef>> txdObjects =
                new Dictionary<String, List<TargetObjectDef>>();
            Dictionary<String, List<ObjectDef.LodLevel>> txdLevels =
                new Dictionary<String, List<ObjectDef.LodLevel>>(); 

            // Iterate through scene caching the TXD to ObjectDef list map.
            foreach (TargetObjectDef o in scene.Walk(Scene.WalkMode.BreadthFirst))
            {
                // B*1556621 - we don't consider proxy meshes
                if (o.HasShadowMeshLink())
                    continue;

                String txd = o.GetAttribute(AttrNames.OBJ_REAL_TXD, AttrDefaults.OBJ_REAL_TXD).ToLower();
                if (0 == String.Compare(txd, AttrDefaults.OBJ_REAL_TXD, true))
                    continue; // Skip default.

                // Otherwise we check whether we have already seen this TXD
                // and for which LOD level.
                if (txdLevels.ContainsKey(txd))
                {
                    txdObjects[txd].Add(o);
                    txdLevels[txd].Add(o.LODLevel);
                }
                else
                {
                    txdObjects.Add(txd, new List<TargetObjectDef>());
                    txdObjects[txd].Add(o);
                    txdLevels.Add(txd, new List<ObjectDef.LodLevel>());
                    txdLevels[txd].Add(o.LODLevel);
                }
            }

            // Iterate through our cache checking the LOD Level of the objects.
            foreach (String txd in txdObjects.Keys)
            {
                // Loop through LOD Levels for this TXD, ensuring they are
                // the same.
                ObjectDef.LodLevel level = ObjectDef.LodLevel.HD;
                foreach (TargetObjectDef o in txdObjects[txd])
                {
                    if (ObjectDef.LodLevel.ORPHANHD == o.LODLevel)
                        continue;
                    level = o.LODLevel;
                }
                IEnumerable<ObjectDef.LodLevel> mismatchingLevel =
                    txdLevels[txd].Where(item => (item != level && item != ObjectDef.LodLevel.ORPHANHD));
                if (mismatchingLevel.Any())
                {
                    // Mismatch of LOD Levels that share the same TXD.
                    // This is an export error.
                    StringBuilder objectList = new StringBuilder();
                    foreach (TargetObjectDef obj in txdObjects[txd])
                        objectList.AppendFormat("level: {0} of object {1},\n ", obj.LODLevel, obj.Name);
                    String message = String.Format("TXD {0} is shared between objects of different LOD levels (\n{1}).  See {2} for more information.",
                        txd, objectList.ToString(), HREF_TXD_SHARED_ACROSS_LEVELS);
                    AddError(txdObjects[txd][0], message);
                }
            }

            return (result);
        }
    }

} // MapExportValidation.Validator

// TXD.cs
