//
// File: IPLGroup.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of IPLGroup validator class
//

using System;
using System.Collections.Generic;
using RSG.Base.Configuration;
using RSG.SceneXml;
using MapExportValidation.Validation;
using RSG.SceneXml.MapExport.Framework;

namespace MapExportValidation.Validator
{
    
    /// <summary>
    /// IPL Group validator for "Gta Object" attribute class objects.
    /// </summary>
    /// If objects have an IPL Group set then they must not be within a LOD
    /// hierarchy (i.e. no LOD parent).  This is a fatal error.
    /// 
    internal class IPLGroup : IObjectDefValidator
    {
        #region Controller Methods
        /// <summary>
        /// Validate a single Object for IPL Group.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="obj"></param>
        /// <param name="scene"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public override bool Validate(IBranch branch, TargetObjectDef obj, Scene scene, ValidatorOptions options)
        {
            if (!obj.IsObject())
                return (true);

            String iplGroup = obj.GetAttribute(AttrNames.OBJ_IPL_GROUP, AttrDefaults.OBJ_IPL_GROUP).ToLower();
            bool iplGroupSet = !String.Equals(iplGroup, AttrDefaults.OBJ_IPL_GROUP, StringComparison.OrdinalIgnoreCase);
            
            if (iplGroupSet && obj.HasLODParent())
            {
                TargetObjectDef parent = obj.LOD.Parent;
                if (parent.IsContainerLODRefObject())
                {
                    // Previously did not allow IMAP Groups to span into a LOD Container; here we just
                    // ensure that they are defined in the same IMAP Group.
                    TargetObjectDef slod2ObjectDef = InterContainerLODManager.Instance.GetSourceSLOD2ObjectDef(parent);
                    if (null != slod2ObjectDef)
                    {
                        String parentIplGroup = slod2ObjectDef.GetAttribute(AttrNames.OBJ_IPL_GROUP, AttrDefaults.OBJ_IPL_GROUP).ToLower(); 
                        
                        if (!String.Equals(iplGroup, parentIplGroup, StringComparison.OrdinalIgnoreCase))
                        {
                            this.AddError(obj, String.Format("Object {0} is part of an IPL Group ({1}) but its LOD parent is part of a different one ({2}).",
                                obj.Name, iplGroup, parentIplGroup));
                            return (false);
                        }
                    }
                    else
                    {
                        this.AddError(obj, String.Format("Failed to resolved Container Ref object: {0} for IPL Group '{1}'.", 
                            parent.Name, iplGroup));
                    }
                }
                else
                {
                    String parentIplGroup = parent.GetAttribute(AttrNames.OBJ_IPL_GROUP, AttrDefaults.OBJ_IPL_GROUP).ToLower();

                    if (!String.Equals(iplGroup, parentIplGroup, StringComparison.OrdinalIgnoreCase))
                    {
                        this.AddError(obj, String.Format("Object {0} is part of an IPL Group ({1}) but its LOD parent is part of a different one ({2}).",
                            obj.Name, iplGroup, parentIplGroup));
                        return (false);
                    }
                }
            }
            return (true);
        }
        #endregion // Controller Methods
    }

} // MapExportValidation.Validator namespace

// IPLGroup.cs
