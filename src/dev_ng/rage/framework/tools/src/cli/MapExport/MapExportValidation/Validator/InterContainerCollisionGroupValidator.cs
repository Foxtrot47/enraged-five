﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.Pipeline.Content;
using RSG.SceneXml;
using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{

    /// <summary>
    /// 
    /// </summary>
    internal class InterContainerCollisionGroupValidator : IInterSceneValidator
    {
        public override bool Validate(IBranch branch, ContentTreeHelper treeHelper, 
            Scene scene, SceneCollection sceneCollection, ValidatorOptions options)
        {
            if (scene.SceneType != RSG.SceneXml.SceneType.EnvironmentProps)
                return true;

            bool result = true;

            IEnumerable<RSG.SceneXml.Scene> propScenes = sceneCollection.GetScenesOfMapType(RSG.SceneXml.SceneType.EnvironmentProps);

            // We need to gather all of the collision groups in use in other containers.
            Dictionary<string, string> existingCollisionGroupMap = new Dictionary<string, string>();
            foreach (RSG.SceneXml.Scene propScene in propScenes.Where(propScene => propScene != scene))
            {
                foreach (RSG.SceneXml.TargetObjectDef objectDef in propScene.Objects)
                {
                    if (objectDef.IsObject() && objectDef.HasCollision() && !objectDef.DontExport())
                    {
                        string collisionGroupName = objectDef.GetAttribute(RSG.SceneXml.AttrNames.OBJ_COLLISION_GROUP, RSG.SceneXml.AttrDefaults.OBJ_COLLISION_GROUP);
                        string collisionGroupNameLower = collisionGroupName.ToLower();
                        if (!String.IsNullOrEmpty(collisionGroupNameLower) && collisionGroupNameLower != RSG.SceneXml.AttrDefaults.OBJ_COLLISION_GROUP)
                        {
                            if(!existingCollisionGroupMap.ContainsKey(collisionGroupNameLower))
                                existingCollisionGroupMap.Add(collisionGroupNameLower, System.IO.Path.GetFileNameWithoutExtension(propScene.Filename));
                        }
                    }
                }
            }

            // Now we need to check that there are no clashes
            foreach (RSG.SceneXml.TargetObjectDef objectDef in scene.Objects)
            {
                if (objectDef.IsObject() && objectDef.HasCollision() && !objectDef.DontExport())
                {
                    string collisionGroupName = objectDef.GetAttribute(RSG.SceneXml.AttrNames.OBJ_COLLISION_GROUP, RSG.SceneXml.AttrDefaults.OBJ_COLLISION_GROUP);
                    string collisionGroupNameLower = collisionGroupName.ToLower();
                    if (existingCollisionGroupMap.ContainsKey(collisionGroupNameLower))
                    {
                        string errorMessage = String.Format(
                            "The collision group '{0}' is already used in '{1}'", 
                            collisionGroupName, 
                            existingCollisionGroupMap[collisionGroupNameLower]);
                        this.AddError(objectDef, errorMessage);
                        result = false;
                    }
                }
            }

            return result;
        }
    }
}
