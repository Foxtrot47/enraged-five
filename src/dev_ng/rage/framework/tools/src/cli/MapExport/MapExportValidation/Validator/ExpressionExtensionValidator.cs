﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.SceneXml;
using RSG.SceneXml.Material;
using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{
    /// <summary>
    /// Expression extension validator.
    /// </summary>
    /// This validator ensures that any expression extension is suitable for export
    ///
    class ExpressionExtensionValidator : IObjectDefValidator
    {
        public ExpressionExtensionValidator()
        {
            validExpressionNames_ = null;
        }

        #region IObjectDefValidator Methods

        /// <summary>
        /// Validate a single Object for its suitability as an expression extension.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="objectDef"></param>
        /// <param name="scene"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public override bool Validate(IBranch branch, TargetObjectDef objectDef, Scene scene, ValidatorOptions options)
        {
            bool result = true;

            if (objectDef.Is2dfxExpression())
            {
                if (validExpressionNames_ == null)// Load the data the first time we need it (most map sections don't need this check)
                {
                    LoadExpressionData(branch);

                    if (!hasValidExpressionNames_)// Warn once if there was a problem loading the data
                    {
                        string warningText = "Unable to load time cycle modifier data.  Time cycle objects may be invalid.";
                        AddWarning(objectDef, warningText);
                    }
                }

                if (!hasValidExpressionNames_)
                {
                    return true;// We failed to load data, so just assume everything is ok
                }

                string expressionDictionaryName = (string)objectDef.GetAttribute(AttrNames.TWODFX_EXPRESSION_DICTIONARY_NAME, AttrDefaults.TWODFX_EXPRESSION_DICTIONARY_NAME).ToLower();
                string expressionName = (string)objectDef.GetAttribute(AttrNames.TWODFX_EXPRESSION_NAME, AttrDefaults.TWODFX_EXPRESSION_NAME).ToLower();

                if (!validExpressionNames_.ContainsKey(expressionDictionaryName))
                {
                    string errorText = String.Format(
                        "Expression extension '{0}' refers to an invalid expression dictionary '{1}'.", 
                        objectDef.Name,
                        expressionDictionaryName);
                    AddError(objectDef, errorText);
                    result = false;
                }
                else if (!validExpressionNames_[expressionDictionaryName].Contains(expressionName))
                {
                    string errorText = String.Format(
                        "Expression extension '{0}' refers to an invalid expression '{1}' in dictionary {2}.", 
                        objectDef.Name,
                        expressionName,
                        expressionDictionaryName);
                    AddError(objectDef, errorText);
                    result = false;
                }
            }

            return result;
        }
        #endregion // IObjectDefValidator Methods

        /// <summary>
        /// Loads the valid expression data
        /// </summary>
        /// <param name="branch"></param>
        private void LoadExpressionData(IBranch branch)
        {
            validExpressionNames_ = new Dictionary<string, List<string>>();
            hasValidExpressionNames_ = true;

            try
            {
                string expressionDictionariesPathname = System.IO.Path.Combine(
                    branch.Project.Config.ToolsConfig, "config/characters/expression_dictionaries.xml");

                XDocument expressionDictionariesDocument = XDocument.Load(expressionDictionariesPathname);
                foreach (XElement dictionaryElement in expressionDictionariesDocument.Root.Elements("dict"))
                {
                    string dictionaryName = dictionaryElement.Attribute("name").Value.ToLower();

                    List<string> expressionNames = new List<string>();
                    foreach (XElement expressionElement in dictionaryElement.Elements("expr"))
                    {
                        expressionNames.Add(expressionElement.Attribute("name").Value.ToLower());
                    }

                    validExpressionNames_.Add(dictionaryName, expressionNames);
                }
            }
            catch (System.Xml.XmlException)
            {
                validExpressionNames_.Clear();
                hasValidExpressionNames_ = false;
            } 
        }

        private Dictionary<string, List<string>> validExpressionNames_;// dictionary-names pair
        private bool hasValidExpressionNames_;
    }
}
