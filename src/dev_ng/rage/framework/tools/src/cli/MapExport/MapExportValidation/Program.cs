//
// File: Program.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Program class
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using MapExportValidation.Validation;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.OS;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services;
using RSG.SceneXml;
using RSG.SceneXml.MapExport.Framework;
using RSG.SceneXml.MapExport.Util;

namespace MapExportValidation
{
 
    /// <summary>
    /// Main Program Static Class defining entry-point.
    /// </summary>
    static class Program
    {
        #region Constants
        private static readonly String OPTION_BUILD = "build";
        #endregion // Constants

        #region Static Member Data
        private static readonly String AUTHOR = "RSGEDI Tools";
        private static readonly String EMAIL = "*tools@rockstarnorth.com";
        #endregion // Static Member Data

        /// <summary>
        /// The main entry point for the application.
        /// Attribute [STAThread] for invoking the universal log from the right thread
        /// </summary>
        [STAThread] 
        static int Main(String[] args)
        {
            #region Option Parsing and Setup
            // Define our executable options.  Stick with the long options
            // (e.g. --opt) rather than the single character versions.
            LongOption[] lopts = {};
            CommandOptions options = new CommandOptions(args, lopts);

            // Parse our options.
            LogFactory.Initialize();
            LogFactory.CreateUniversalLogFile(Log.StaticLog);
            LogFactory.CreateApplicationConsoleLogTarget();

            int exit_code = Constants.Exit_Success;
            String builddir = (options[OPTION_BUILD] as String);

            Debug.Assert(options.TrailingArguments.Any(), "Must specify at least one trailing argument SceneXml XML file.");

            if (!options.TrailingArguments.Any())
                return (Constants.Exit_Failure);
            #endregion // Option Parsing and Setup

            try
            {
                // Load Content-Tree.  This is required for the SceneCollection.
                IBranch branch = options.Branch;
                if (options.Project.IsDLC)
                {
                    Log.StaticLog.Message("Overriding core branch {0} with DLC branch {1} - {0}", options.Project.Name, branch.Name);
                    branch = options.Project.Branches[options.Branch.Name];
                }
                IContentTree tree = Factory.CreateTree(branch);
                ContentTreeHelper treeHelper = new ContentTreeHelper(tree);

                // Initialise global-like subsystems
                String sceneOverrideDirectory = System.IO.Path.Combine(branch.Assets, "maps", "scene_overrides");
                SceneOverrideIntegrator.Instance.Initialise(branch, treeHelper, sceneOverrideDirectory);

                SceneCollection scenes = new SceneCollection(branch, tree);

                InterContainerLODManager.Instance.Initialise(scenes, treeHelper);

                // Load this scene
                IEnumerable<string> filenames = options.TrailingArguments;
                foreach (string filename in filenames)
                {
                    Scene scene = scenes.LoadScene(filename);

                    ValidatorOptions validatorOptions = new ValidatorOptions();
                    validatorOptions["build"] = builddir;

                    Log.StaticLog.Message("Starting validation:");
                    Log.StaticLog.Message("  Scene: {0}", filename);

                    Log.StaticLog.Profile("Inter-scene validators.");
                    bool validInterScene = ValidateInterScene(branch, treeHelper, scene, scenes, validatorOptions);
                    Log.StaticLog.ProfileEnd();

                    Log.StaticLog.Profile("Scene validators.");
                    bool validScene = ValidateScene(branch, treeHelper, scene, validatorOptions);
                    Log.StaticLog.ProfileEnd();

                    Log.StaticLog.Profile("Object validators.");
                    bool validObjects = ValidateObjects(branch, scene, validatorOptions);
                    Log.StaticLog.ProfileEnd();

                    if ( !( validInterScene && validScene && validObjects ) )
                    {
                        exit_code = Constants.Exit_Failure;
                    }
                }
            }
            catch (Exception ex)
            {
                exit_code = Constants.Exit_Failure;
                #region Exception Handling
                // Log the exception.
                Log.StaticLog.ToolExceptionCtx(ex.Source, ex, "Unhandled exception during Map Export validation: {0}", ex.Source);

                // Display exception dialog only if we are allowed.
                if (!options.NoPopups)
                {
                    RSG.Base.Windows.ExceptionStackTraceDlg stackDlg = new RSG.Base.Windows.ExceptionStackTraceDlg(ex, AUTHOR, EMAIL);
                    stackDlg.Show();
                }
                #endregion // Exception Handling
            }

            if (Log.StaticLog.HasWarnings)
                exit_code = Constants.Exit_Success;
            if (Log.StaticLog.HasErrors)
                exit_code = Constants.Exit_Failure;

            Environment.ExitCode = exit_code;
            LogFactory.ApplicationShutdown();

            return ( exit_code );
        }

        /// <summary>
        /// Load and create instances of all specified validator classes.
        /// </summary>
        /// <typeparam name="T">Validator interface type</typeparam>
        /// <returns>List of validator objects adhering to validator interface</returns>
        /// This method uses reflection to load all of the validator classes
        /// adhering to the specified type interface from the current entry
        /// 
        static List<T> LoadValidators<T>() where T : class
        {
            Assembly entryAssembly = Assembly.GetEntryAssembly();
            Type[] assemblyTypes = entryAssembly.GetTypes();
            List<T> validators = new List<T>();

            // Load and create validator objects.
            foreach (Type assType in assemblyTypes)
            {
                if (assType.IsAbstract)
                    continue;
                if (!typeof(T).IsAssignableFrom(assType))
                    continue;

                validators.Add(Activator.CreateInstance(assType) as T);
            }
            return (validators);
        }

        /// <summary>
        /// Lod our validator errors and warnings.
        /// </summary>
        /// <param name="validator"></param>
        static void LogValidatorexit_codes(IValidator validator)
        {
            // Loop through validators collecting errors and warnings.
            foreach (KeyValuePair<TargetObjectDef, List<String>> errorPair in validator.Errors)
            {
                foreach ( String error in errorPair.Value )
                {
                    Log.StaticLog.ErrorCtx(errorPair.Key.Name, String.Format("{0}: {1}", errorPair.Key.Name, error));
                }
            }

            foreach (KeyValuePair<TargetObjectDef, List<String>> warnPair in validator.Warnings)
            {
                foreach ( String warning in warnPair.Value )
                {
                    Log.StaticLog.WarningCtx(warnPair.Key.Name, String.Format("{0}: {1}", warnPair.Key.Name, warning));
                }
            }

            foreach (KeyValuePair<TargetObjectDef, List<String>> infoPair in validator.Infos)
            {
                foreach ( String info in infoPair.Value )
                {
                    Log.StaticLog.MessageCtx(infoPair.Key.Name, String.Format("{0}: {1}", infoPair.Key.Name, info));
                }
            }
        }

        /// <summary>
        /// Run all of our ISceneValidator algorithms on the scene.
        /// </summary>
        static bool ValidateInterScene(IBranch branch, ContentTreeHelper treeHelper, 
            Scene scene, SceneCollection sceneCollection, ValidatorOptions options)
        {
            List<IInterSceneValidator> validators = LoadValidators<IInterSceneValidator>();

            bool exit_code = true;
            foreach (IInterSceneValidator validator in validators)
            {
                if (!validator.Enabled)
                    continue;

                if (!validator.Validate(branch, treeHelper, scene, sceneCollection, options))
                    exit_code = false;
            }

            // Loop through validators collecting errors and warnings.
            foreach (IInterSceneValidator validator in validators)
                LogValidatorexit_codes(validator);

            return (exit_code);
        }

        /// <summary>
        /// Run all of our ISceneValidator algorithms on the scene.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="treeHelper"></param>
        /// <param name="scene"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        static bool ValidateScene(IBranch branch, ContentTreeHelper treeHelper,
            Scene scene, ValidatorOptions options)
        {
            List<ISceneValidator> validators = LoadValidators<ISceneValidator>();
            
            bool exit_code = true;
            foreach (ISceneValidator validator in validators) 
            {
                if (!validator.Enabled)
                    continue;

                if (!validator.Validate(branch, treeHelper, scene, options))
                    exit_code = false;
            }

            // Loop through validators collecting errors and warnings.
            foreach (ISceneValidator validator in validators)
                LogValidatorexit_codes(validator);

            return (exit_code);
        }

        static bool DeadEndFunc(TargetObjectDef obj)
        {
            return obj.IsMAXGroup();
        }

        /// <summary>
        /// Run all of our IObjectDefValidator algorithms on the scene.
        /// </summary>
        static bool ValidateObjects(IBranch branch, Scene scene, ValidatorOptions options)
        {
            List<IObjectDefValidator> validators = LoadValidators<IObjectDefValidator>();            

            // Loop through each ObjectDef invoking each validator.
            bool exit_code = true;
            foreach (TargetObjectDef obj in scene.Walk(Scene.WalkMode.DepthFirst, DeadEndFunc))
            {
                if (obj.DontExport())
                    continue;

                foreach (IObjectDefValidator validator in validators)
                {
                    if (!validator.Enabled)
                        continue;

                    if (!validator.Validate(branch, obj, scene, options))
                        exit_code = false;
                }
            }

            // Loop through validators collecting errors and warnings.
            foreach (IObjectDefValidator validator in validators)
                LogValidatorexit_codes(validator);

            return (exit_code);
        }
    }

} // MapExportValidation namespace

// Program.cs
