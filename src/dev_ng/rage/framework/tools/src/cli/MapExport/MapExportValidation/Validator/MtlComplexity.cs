﻿//
// File: MtlComplexity.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Drawable material complexity validation.
//

using System;
using System.Collections.Generic;
using RSG.Base.Configuration;
using RSG.SceneXml;
using RSG.SceneXml.Material;
using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{

    /// <summary>
    /// Drawable material complexity 
    /// </summary>
    /// If objects have an IPL Group set then they must not be within a LOD
    /// hierarchy (i.e. no LOD parent).  This is a fatal error.
    /// 
    internal class MtlComplexity : IObjectDefValidator
    {
        #region Constants
        /// <summary>
        /// Maximum number of shaders for any given drawable (runtime limit).
        /// </summary>
        private const int DRAWABLE_MAX_SHADERS = 48;

        /// <summary>
        /// Devstar Help Link.
        /// </summary>
        private readonly String URL_HELP = "https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#Material_Complexity";
        #endregion // Constants

        #region Controller Methods
        /// <summary>
        /// Validate a single Object for IPL Group.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Validate(IBranch branch, TargetObjectDef obj, RSG.SceneXml.Scene scene, ValidatorOptions options)
        {
            if (!obj.IsObject())
                return (true);

            if (obj.Material == Guid.Empty)
                return true;
            try
            {
                int numShaders = 0;
                MaterialDef rootMaterial = scene.MaterialLookup[obj.Material];
                if (rootMaterial.MaterialType == MaterialTypes.MultiMaterial)
                    numShaders = rootMaterial.SubMaterialsCount;
                else
                    numShaders = 1;

                if (numShaders > DRAWABLE_MAX_SHADERS)
                {
                    this.AddError(obj, String.Format("{0} has {1} materials applied, we have a limit of {2}.  See {3} for more information.",
                                              obj.Name, numShaders, DRAWABLE_MAX_SHADERS, URL_HELP));
                    return false;
                }
                else
                    return true;
            }
            catch (System.Collections.Generic.KeyNotFoundException)
            {
                // invalid or empty material
                return true;
            }
        }
        #endregion // Controller Methods
    }

} // MapExportValidation.Validator namespace

// MtlComplexity.cs
