﻿using System;
using System.Collections.Generic;
using RSG.Base.Configuration;
using RSG.Base.Math;
using RSG.SceneXml;
using RSG.SceneXml.MapExport;
using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{

    /// <summary>
    /// 
    /// </summary>
    internal class InteriorLODFadeDistance : IObjectDefValidator
    {
        #region Constants 
        private const String URL_HELP = "https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#IMAP_Streaming_Extents:_Interiors";
        #endregion // Constants

        #region Controller Methods
        /// <summary>
        /// Validate an interior LOD distance; ensuring its large enough for
        /// the interior to not cause streaming extents issues for any references.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="obj"></param>
        /// <param name="scene"></param>
        /// <param name="options"></param>
        /// <param name="mapSections"></param>
        /// <returns></returns>
        public override bool Validate(IBranch branch, TargetObjectDef obj, Scene scene, ValidatorOptions options)
        {
            if (!obj.IsMilo())
                return (true);

            float lod = obj.GetAttribute(AttrNames.MILO_DETAIL, AttrDefaults.MILO_DETAIL);
            BoundingBox3f objExtents = GetBoundingBox(obj);
            BoundingBox3f streamingExtents = GetStreamingExtents(obj);
            if (!streamingExtents.Contains(objExtents.Min) ||
                !streamingExtents.Contains(objExtents.Max))
            {
                double distRequired = (objExtents.Max - objExtents.Min).Magnitude() / 2.0f;
                AddWarning(obj, String.Format("[art] Interior {0} LOD distance is too small at {1:F2}m; any instances will not have a large enough streaming extents.  LOD distance should be at least {2:F2}m.",
                    obj.Name, lod, distRequired));
                //return (false);
            }
            return (true);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        private BoundingBox3f GetBoundingBox(TargetObjectDef o)
        {
            BoundingBox3f bbox = new BoundingBox3f();
            if (null != o.LocalBoundingBox)
                bbox.Expand(o.LocalBoundingBox);

            foreach (TargetObjectDef child in o.Children)
            {
                BoundingBox3f box = GetBoundingBox(child);
                if (null != box && !box.IsEmpty)
                    bbox.Expand(box);
            }

            return (bbox);
        }


        /// <summary>
        /// Return the streaming extents of a world-origin positioned entity.
        /// </summary>
        /// The streaming extents is defined by the LOD distance of this 
        /// instance; and depends on whether its in a LOD hierarchy or not.
        /// If we have a LOD parent then its relative to the parent's pivot.
        /// <returns></returns>
        public BoundingBox3f GetStreamingExtents(TargetObjectDef o)
        {
            BoundingBox3f streamingExtents = new BoundingBox3f();
            float lod = o.GetAttribute(AttrNames.MILO_DETAIL, AttrDefaults.MILO_DETAIL);
            Vector3f vlod = new Vector3f(lod, lod, lod);
            Vector3f pos = new Vector3f(0.0f, 0.0f, 0.0f);
            streamingExtents.Expand(pos + vlod);
            streamingExtents.Expand(pos - vlod);
            
            return (streamingExtents);
        }
        #endregion // Private Methods
    }

} // MapExportValidation.Validator namespace
