﻿using System;
using System.Collections.Generic;
using RSG.Base.Configuration;
using RSG.SceneXml;
using RSG.Pipeline.Content;

namespace MapExportValidation.Validation
{
    public abstract class IInterSceneValidator : IValidator
    {
        #region Properties and Associated Member Data

        public virtual bool Enabled
        {
            get { return true; }
        }

        public Dictionary<TargetObjectDef, List<String>> Errors
        {
            get;
            private set;
        }

        public Dictionary<TargetObjectDef, List<String>> Warnings
        {
            get;
            private set;
        }

        public Dictionary<TargetObjectDef, List<String>> Infos
        {
            get;
            private set;
        }

        #endregion // Properties and Associated Member Data

        #region Constructor(s)
        public IInterSceneValidator()
        {
            this.Errors = new Dictionary<TargetObjectDef, List<String>>();
            this.Warnings = new Dictionary<TargetObjectDef, List<String>>();
            this.Infos = new Dictionary<TargetObjectDef, List<String>>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Main Validator method to be overridden by subclasses
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="treeHelper"></param>
        /// <param name="scene">The scene to be validated</param>
        /// <param name="sceneCollection">The sceneCollection instance used retrieve information about other scenes that have been loaded</param>
        /// <param name="options">Generic options passed to the program itself</param>
        /// <returns></returns>
        public abstract bool Validate(IBranch branch, ContentTreeHelper treeHelper, Scene scene, SceneCollection sceneCollection, ValidatorOptions options);
        #endregion // Controller Methods

        #region Protected Methods
        /// <summary>
        /// Add an error to our error list.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="error"></param>
        protected virtual void AddError(TargetObjectDef obj, String error)
        {
            if (!this.Errors.ContainsKey(obj))
                this.Errors.Add(obj, new List<String>());
            this.Errors[obj].Add(error);
        }

        /// <summary>
        /// Add a warning to our warning list.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="warning"></param>
        protected virtual void AddWarning(TargetObjectDef obj, String warning)
        {
            if (!this.Warnings.ContainsKey(obj))
                this.Warnings.Add(obj, new List<String>());
            this.Warnings[obj].Add(warning);
        }

        /// <summary>
        /// Add a warning to our warning list.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="warning"></param>
        protected virtual void AddInfo(TargetObjectDef obj, String info)
        {
            if (!this.Infos.ContainsKey(obj))
                this.Infos.Add(obj, new List<String>());
            this.Infos[obj].Add(info);
        }
        #endregion // Protected Methods
    }
}
