﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RSG.Base.Configuration;
using RSG.SceneXml;
using RSG.SceneXml.Material;
using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{
    /// <summary>
    /// This object validator ensures that props in interiors are suitable for export
    /// </summary>
    class InteriorPropValidator : IObjectDefValidator
    {
        #region IObjectDefValidator Methods

        public override bool Validate(IBranch branch, TargetObjectDef obj, Scene scene, ValidatorOptions options)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");

            if (SceneType.EnvironmentInterior != scene.SceneType)
                return true;

            if (obj.IsObject())
            {
                // Validate that none of the collision nodes under a dynamic prop are
                bool isDynamic = obj.GetAttribute(AttrNames.OBJ_IS_DYNAMIC, AttrDefaults.OBJ_IS_DYNAMIC);

                if(!isDynamic)
                    return true;

                foreach (TargetObjectDef childObjectDef in obj.Children)
                {
                    if (childObjectDef.IsCollision())
                    {
                        int roomID = childObjectDef.GetAttribute(AttrNames.COLL_ROOM_ID, AttrDefaults.COLL_ROOM_ID);
                        if (roomID > 0)
                        {
                            string error = String.Format("Collision object '{0}' has a room id set but belongs to a dynamic object.  " + 
                                                         "This is not allowed.  " + 
                                                         "To resolve this either set the room id to 0 or make the parent object '{1}' static.", 
                                                         childObjectDef.Name, obj.Name);
                            AddError(childObjectDef, error);
                            return false;
                        }
                    }
                }
            }

            return true;
        }
        #endregion // IObjectDefValidator Methods
    }

} // MapExportValidation.Validator namespace
