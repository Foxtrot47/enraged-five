﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RSG.Base.Configuration;
using RSG.SceneXml;
using RSG.SceneXml.Material;
using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{
    /// <summary>
    /// Prop bake validator.
    /// </summary>
    /// This validator ensures that any props flagged for baking into the static bounds are suitable
    /// 
    class PropBakeValidator : IObjectDefValidator
    {
        #region Constants
        private const String nonUniformUrl_ = "https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#Non-uniform_Scaled_Prop_Instances";
        #endregion // Constants

        public PropBakeValidator()
        {
        }

        #region IObjectDefValidator Methods

        /// <summary>
        /// Validate a single Object for its baking suitability.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Validate(IBranch branch, TargetObjectDef obj, RSG.SceneXml.Scene scene, ValidatorOptions options)
        {
            if (obj == null)
                throw new ArgumentNullException("obj");

            if ((obj.IsRefObject() || obj.IsXRef()) && obj.WillHaveCollisionBaked())
            {
                if (obj.RefObject == null)
                {
                    return true;
                }

                if (obj.RefObject.IsDynObject())
                {
                    string error = String.Format("Referenced object '{0}' is dynamic, so this object cannot have its collision baked.", obj.RefName);
                    AddError(obj, error);
                    return false;
                }

                // Check for collision to be baked that has a priority set.
                if (obj.RefObject.HasCollision())
                {
                    int priority = obj.GetAttribute(AttrNames.OBJ_PRIORITY, AttrDefaults.OBJ_PRIORITY);
                    if (priority > 0)
                    {
                        string error = String.Format("The collision for object '{0}' cannot be baked as the priority is not set to 0 (it's set to {1}).  " +
                                                     "Prop instances are baked if they are scaled or have the Force Bake Collision flag set.", 
                                                     obj.Name, 
                                                     priority);
                        AddError(obj, error);
                        return false;
                    }
                }

                bool isNonUniformScalingAllowed = obj.RefObject.GetAttribute(AttrNames.OBJ_ALLOW_NON_UNIFORM_SCALED_INSTANCES, AttrDefaults.OBJ_ALLOW_NON_UNIFORM_SCALED_INSTANCES);
                if (!isNonUniformScalingAllowed)
                {
                    float minScaleDimension = Math.Min(obj.NodeScale.X, Math.Min(obj.NodeScale.Y, obj.NodeScale.Z));
                    float maxScaleDimension = Math.Max(obj.NodeScale.X, Math.Max(obj.NodeScale.Y, obj.NodeScale.Z));

                    float scaleDimensionDifference = maxScaleDimension - minScaleDimension;

                    if (scaleDimensionDifference > nonUniformTolerance_)
                    {
                        string message = String.Format(
                            "Object '{0}' has non-uniform scale [{1}, {2}, {3}] but the source ref object '{4}' doesn't support non-uniform scaling.  For more information, see {5}",
                            obj.Name, 
                            obj.NodeScale.X, 
                            obj.NodeScale.Y, 
                            obj.NodeScale.Z, 
                            obj.RefName,
                            nonUniformUrl_);
                        AddWarning(obj, message);
                        return false;
                    }
                }
            }

            return true;
        }
        #endregion // IObjectDefValidator Methods

        static readonly float nonUniformTolerance_ = 0.05f;
    }
}
