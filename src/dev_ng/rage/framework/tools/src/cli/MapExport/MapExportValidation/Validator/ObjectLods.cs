﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.Base.Math;
using RSG.Base.Logging;
using RSG.Pipeline.Content;
using RSG.SceneXml;
using RSG.SceneXml.MapExport;
using RSG.SceneXml.MapExport.Framework;
using RSG.SceneXml.MapExport.Util;
using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{
    /// <summary>
    /// Container LOD Validator.
    /// </summary>
    internal class ObjectLods : ISceneValidator
    {
        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="treeHelper"></param>
        /// <param name="scene"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public override bool Validate(IBranch branch, ContentTreeHelper treeHelper, Scene scene, ValidatorOptions options)
        {
            switch (scene.SceneType)
            {
                case SceneType.EnvironmentProps:
                case SceneType.EnvironmentInterior:
                    Log.Log__MessageCtx(Path.GetFileNameWithoutExtension(scene.Filename),
                        "Skipping Scene LOD validation for props/interior scene.");
                    return (true);
            }

            bool result = true;
            var definitionContainers = new Dictionary<string, Dictionary<string, TargetObjectDef>>();
            foreach (TargetObjectDef o in scene.Walk(Scene.WalkMode.DepthFirst))
            {
                // Check that the objects world bound box is contained within
                // its streaming extents
                if (!o.IsObject())
                    continue;

                Entity parent = null;
                if (o.HasLODParent())
                    parent = new Entity(o.LOD.Parent, parent);
                Entity entity = new Entity(o, parent);
                float lodDistance = entity.LODDistance;
                // Handle InternalRefs without affecting their exported LOD Distance.
                if (entity.Object.IsInternalRef() || entity.Object.IsRefInternalObject())
                {
                    TargetObjectDef refObject = entity.Object.MyScene.FindObject(entity.Object.RefName);
                    if (null != refObject)
                    {
                        lodDistance = entity.Object.GetAttribute(AttrNames.OBJ_LOD_DISTANCE, AttrDefaults.OBJ_LOD_DISTANCE);
                    }
                    else
                    {
                        lodDistance = -1.0f;
                    }
                }

                BoundingBox3f streamingBox = entity.GetStreamingExtents();
                bool contained = (streamingBox.Contains(entity.WorldBoundingBox.Min) &&
                          streamingBox.Contains(entity.WorldBoundingBox.Max));
                if (entity.Object.HasLODParent() && !contained)
                {
                    String message = String.Format(
                        "[art] The LOD distance ({0:F2}m) for '{1}' [{2}] is not large enough; some or all of this entity's geometry exists outside its streaming extents.  " +
                        "This entity has a LOD parent ('{3}') so the game will use the parent's pivot as the centre of this entity's streaming extents.",
                        lodDistance, entity.Object.Name, entity.Object.LODLevel, entity.Parent.Object.Name);
                    AddWarning(entity.Object, message);
                    result = false;
                }
                else if (!contained)
                {
                    double maxSize = ((entity.WorldBoundingBox.Max - entity.WorldBoundingBox.Min) / 2.0f).Magnitude();
                    String message = String.Format("[art] Entity {0} [{1}] streaming bound does not enclose its extents because its LOD distance is not large enough.  Entity LOD distance: {2:F2}m and entity size: {3:F2}m.",
                       entity.Object.Name, entity.Object.LODLevel, lodDistance, maxSize);
                    AddWarning(entity.Object, message);
                    result = false;
                }

                if (!o.IsRefObject() && !o.IsRefInternalObject() && !o.IsXRef() && !o.IsInternalRef())
                    continue;
                if (!o.Attributes.ContainsKey(AttrNames.OBJ_INSTANCE_LOD_DISTANCE))
                    continue;
                if (string.IsNullOrEmpty(o.RefFile) || string.IsNullOrEmpty(o.RefName))
                    continue;

                // We have overridden the lod distance on a reference. We need to warn if the overridden
                // lod distance if greater than the source lod distance, so need to store the ref file
                // name to load up later.
                string refFilename = System.IO.Path.GetFileNameWithoutExtension(o.RefFile);

                if (!definitionContainers.ContainsKey(refFilename))
                    definitionContainers.Add(refFilename, new Dictionary<string, TargetObjectDef>());

                if (!definitionContainers[refFilename].ContainsKey(o.RefName))
                    definitionContainers[refFilename].Add(o.RefName, o);
            }

#warning DHM : FIX ME
#if false
            if (definitionContainers.Count == 0)
                return result;
            
            foreach (var definitionContainer in definitionContainers)
            {
                MapSection section = mapSections.Find(s => s.Name == definitionContainer.Key);
                if (section == null)
                    continue;

                if (!section.CreateDataFromSceneXml(new DefinitionList()))
                    continue;

                foreach (var o in definitionContainer.Value)
                {
                    IMapDefinition definition = section.Definitions.Find(d => d.Name == o.Key);
                    if (definition == null)
                        continue;

                    float sourceDistance = GetLodDistance(definition);
                    float refLodDistance = definitionContainer.Value[definition.Name].GetLODDistance();
                    if (refLodDistance > sourceDistance)
                    {
                        String message = String.Format("[art] Referenced object {0} lod distance {1} has been overridden to a greater value than its source lod distance {2}.",
                                                        definitionContainer.Value[definition.Name].Name, refLodDistance, refLodDistance);
                        AddWarning(definitionContainer.Value[definition.Name], message);
                        result = false;
                    }
                }
            }            
#endif
            // Validate LOD distances (taking overrides and the container LOD into account)
            foreach (TargetObjectDef objectDef in scene.Walk(Scene.WalkMode.DepthFirst))
            {
                if (!objectDef.IsObject())
                    continue;

                if (objectDef.IsInternalRef() || objectDef.IsRefInternalObject())
                    continue;

                // Get the effectiveLODDistance taking into account that there may be an override on another object in these scene, or a conatiner_lod sibling

                float effectiveLODDistance = SceneOverrideIntegrator.Instance.GetObjectLODDistance(objectDef);
                if (objectDef.IsRefObject() || objectDef.IsXRef())
                {
                    bool hasInstanceLODDistance = objectDef.GetAttribute(AttrNames.OBJ_INSTANCE_LOD_DISTANCE, AttrDefaults.OBJ_INSTANCE_LOD_DISTANCE);
                    if(!hasInstanceLODDistance && objectDef.RefObject != null)
                        effectiveLODDistance = objectDef.RefObject.GetAttribute(AttrNames.OBJ_LOD_DISTANCE, AttrDefaults.OBJ_LOD_DISTANCE);
                }

                float parentLODDistance = 0.0f;

                TargetObjectDef parentObjectDef = null;
                if (objectDef.LOD != null && objectDef.LOD.Parent != null)
                {
                    parentObjectDef = objectDef.LOD.Parent;
                    if (parentObjectDef.IsContainerLODRefObject())
                    {
                        TargetObjectDef slod2ParentObjectDef = InterContainerLODManager.Instance.GetSourceSLOD2ObjectDef(parentObjectDef);

                        // B* 686042 - check for invalid SLOD2 links
                        if (slod2ParentObjectDef == null)
                        {
                            string errorMessage = String.Format(
                                "This object is linked to an invalid SLOD2 object '{0}'.  Has the SLOD2 object been deleted or renamed?",
                                parentObjectDef.RefName);
                            AddError(objectDef, errorMessage);
                            result = false;
                            continue;
                        }

                        parentObjectDef = slod2ParentObjectDef;
                    }

                    if (parentObjectDef != null)
                    {
                        parentLODDistance = SceneOverrideIntegrator.Instance.GetObjectLODDistance(parentObjectDef);
                        float parentsOverrideLODDistance = SceneOverrideIntegrator.Instance.GetObjectChildLODDistance(parentObjectDef);
                        if (parentsOverrideLODDistance > 0.0f)
                            effectiveLODDistance = parentsOverrideLODDistance;
                    }
                }

                // Now we validate using effectiveLODDistance
                if (parentLODDistance > 0 && effectiveLODDistance > parentLODDistance)
                {
                    string message = String.Format("Object cannot have a greater LOD distance ({0}) than its parent '{1}'({2})",
                        effectiveLODDistance, parentObjectDef.Name, parentLODDistance);
                    AddError(objectDef, message);
                    result = false;
                }

                // LOD level objects have some specific checks
                if (objectDef.LODLevel == ObjectDef.LodLevel.LOD)
                {
                    if (parentObjectDef == null)
                    {
                        AddWarning(objectDef, "LOD object doesn't have an SLOD parent");
                        result = false;
                    }

                    if (effectiveLODDistance > maxLODLevelLODDistance_)
                    {
                        AddWarning(objectDef, string.Format("LOD object has a LOD distance greater than {0}", maxLODLevelLODDistance_));
                        result = false;
                    }
                }

                // Check that LOD distances are common between children
                if (objectDef.LODLevel == ObjectDef.LodLevel.SLOD1 && objectDef.LOD != null && objectDef.LOD.Children.Length > 0)
                {
                    float childLODDistance = objectDef.GetAttribute(AttrNames.OBJ_CHILD_LOD_DISTANCE, AttrDefaults.OBJ_CHILD_LOD_DISTANCE);
                    if (childLODDistance > 0.0f)
                        continue;// If the child distance is overridden then there can't be any differences

                    SortedSet<float> childLODDistances = new SortedSet<float>();
                    foreach (TargetObjectDef lodChildObjectDef in objectDef.LOD.Children)
                    {
                        float lodDistance = SceneOverrideIntegrator.Instance.GetObjectLODDistance(lodChildObjectDef);
                        childLODDistances.Add(lodDistance);
                    }

                    if (childLODDistances.Count > 1)
                    {
                        AddError(objectDef, string.Format("SLOD object has a variety of LOD distances set in its LOD children ({0})", String.Join(", ", childLODDistances)));
                        result = false;
                    }
                }
            }

            return (result);
        }

#warning DHM : FIX ME
#if false
        /// <summary>
        /// 
        /// </summary>
        /// <param name="definition"></param>
        /// <returns></returns>
        private float GetLodDistance(IMapDefinition definition)
        {
            if (!definition.Attributes.ContainsKey(AttrNames.OBJ_LOD_DISTANCE))
                return AttrDefaults.OBJ_LOD_DISTANCE;

            return (float)definition.Attributes[AttrNames.OBJ_LOD_DISTANCE];
        }
#endif 
        #endregion // Controller Methods

        private static readonly float maxLODLevelLODDistance_ = 700.0f;
    } // ObjectLods
} // MapExportValidation.Validator
