﻿using System;
using System.Collections.Generic;
using RSG.Base.Configuration;
using RSG.SceneXml;
using RSG.SceneXml.Material;
using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{

    /// <summary>
    /// IPL Group validator for "Gta Object" attribute class objects.
    /// </summary>
    /// If objects have an IPL Group set then they must not be within a LOD
    /// hierarchy (i.e. no LOD parent).  This is a fatal error.
    /// 
    internal class Effect2D : IObjectDefValidator
    {
        #region Controller Methods

        /// <summary>
        /// Validate a single Object for IPL Group.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Validate(IBranch branch, TargetObjectDef obj, RSG.SceneXml.Scene scene, ValidatorOptions options)
        {
            if (obj.Is2dfx())
            {
                if (null==obj.Parent || !obj.Parent.IsObject())
                {
                    this.AddInfo(obj, "The effect object has an invalid/no parent in the scene - it won't get added to the ipl");
                }
            }

            if (obj.Is2dfxRSLadder())
            {
                // Default value is 1 for material type (3ds max array index based)
                int ladderMaterialTypeInt = obj.GetParameter(ParamNames.TWODFX_RSLADDER_MATERIAL_TYPE, ParamDefaults.TWODFX_RSLADDER_MATERIAL_TYPE);
                if (ladderMaterialTypeInt < 1 || ladderMaterialTypeInt >= (int)LadderMaterialTypes.LAST)
                {
                    String error = String.Format("The RSLadder has a material type {0} which is outside of the range 1 to {1}!", ladderMaterialTypeInt, (int)(LadderMaterialTypes.LAST - 1));
                    this.AddError(obj, error);
                    return false;
                }
            }

            return true;
        }
        #endregion // Controller Methods
    }

} // MapExportValidation.Validator namespace

// IPLGroup.cs
