﻿using System;
using System.Collections.Generic;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Math;
using RSG.SceneXml;
using RSG.SceneXml.MapExport.Util;

using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{
    class InternalRefValidator : IObjectDefValidator
    {
        public override bool Validate(IBranch branch, TargetObjectDef obj, RSG.SceneXml.Scene scene, ValidatorOptions options)
        {
            bool result = true;

            if (obj.IsRefInternalObject())
            {
                TargetObjectDef refObject = FindInternalRefObject(obj.RefName, scene);
                if (refObject == null)
                {
                    this.AddError(obj,
                        String.Format("Internal ref object '{0}' references an object '{1}' that could not be found.",
                            obj.Name, obj.RefName));
                    result = false;
                }
            }

            return result;
        }

        private TargetObjectDef FindInternalRefObject(String refName, Scene scene)
        {
            foreach (TargetObjectDef objectDef in scene.Walk(Scene.WalkMode.DepthFirst))
            {
                if (objectDef.IsObject() && String.Compare(refName, objectDef.Name, true) == 0)
                {
                    return objectDef;
                }
            }

            return null;
        }
    }
}
