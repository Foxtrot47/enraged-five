﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using RSG.Base.Configuration;
using RSG.Pipeline.Content;
using RSG.SceneXml;
using RSG.SceneXml.MapExport.Util;
using MapExportValidation.Validation;
using RSG.ObjectLinks;

namespace MapExportValidation.Validator
{
    internal class ComboLodderMeshValidator : ISceneValidator
    {
        public override bool Validate(IBranch branch, ContentTreeHelper treeHelper, Scene scene, ValidatorOptions options)
        {
            if (SceneType.EnvironmentProps != scene.SceneType)
                return true;

            bool result = true;

            // Validate the upper combo lod objects
            foreach (TargetObjectDef objectDef in scene.Walk(Scene.WalkMode.DepthFirst))
            {
                bool hasComboLodderChildren = HasComboLodderChildren(objectDef);
                bool archetypeWillBeCreated = !objectDef.GetAttribute(AttrNames.OBJ_DONT_EXPORT, AttrDefaults.OBJ_DONT_EXPORT);
                bool hasDrawableLODChildren = objectDef.HasDrawableLODChildren();
                if (hasComboLodderChildren && archetypeWillBeCreated && !hasDrawableLODChildren)
                {
                    String errorText = String.Format(
                        "Object '{0}' is a low detail object in a combo lodder hierarchy, but is otherwise set up to export as a regular prop.  " + 
                        "Please set the \"Don't Export\" flag.",
                        objectDef.Name);
                    this.AddError(objectDef, errorText);
                    result = false;
                }
            }

            return result;
        }

        private static bool HasComboLodderChildren(TargetObjectDef objectDef)
        {
            if (objectDef.IsObject() && objectDef.SceneLinks.ContainsKey(LinkChannel.LinkChannelCombinerMesh))
            {
                return (objectDef.SceneLinks[LinkChannel.LinkChannelCombinerMesh].Children.Length > 0);
            }

            return false;
        }

    }
}
