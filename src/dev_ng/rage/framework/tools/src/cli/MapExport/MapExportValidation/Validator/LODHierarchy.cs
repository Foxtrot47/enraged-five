//
// File: LODHierarchy.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of LODHierarchy.cs class
//

using System;
using System.Collections.Generic;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Math;
using RSG.Pipeline.Content;
using RSG.SceneXml;
using RSG.SceneXml.MapExport.Util;

using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{
    
    /// <summary>
    /// Object LOD hierarchy validation for "Gta Object" attribute class objects [phase 1].
    /// </summary>
    /// If objects are part of a LOD hierarchy they must all be set to don't export
    /// or none of them.  Otherwise the IPL5 serialiser has issues.
    /// 
    /// After discussion with the artists there is no valid reason for this 
    /// situation; it just appeared on some test data.
    /// 
    internal class LODHierarchy : IObjectDefValidator
    {
        #region IObjectDefValidator Methods
        /// <summary>
        /// Validate a single Object for LOD hierarchy.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Validate(IBranch branch, TargetObjectDef obj, Scene scene, ValidatorOptions options)
        {
            bool result = true;

            // B* 891954 - Make sure that MILOs don't have LOD siblings
            if ((SceneType.EnvironmentContainer == scene.SceneType) &&
                (obj.IsMilo() || obj.IsMiloTri()) &&
                (obj.HasLODParent()))
            {
                TargetObjectDef lodParent = obj.LOD.Parent;
                if (lodParent.LOD.Children.Length > 1)
                {
                    string error = String.Format(
                        "The MILO '{0}' must be the only child of its LOD parent '{1}'.  " +
                        "There is no runtime support for MILO LOD siblings.", obj.Name, lodParent.Name);
                    AddError(obj, error);
                    return false;
                }
            }

            // B* 950791 - If the object is part of a LOD hierarchy we need to make sure that:
            //   1) It is at the root of the container/scene (the IMAP serialiser makes this assumption)
            //   2) It is a valid entity
            if (obj.HasLODParent() || obj.HasLODChildren())
            {
                if (obj.Parent != null && !obj.Parent.IsContainer())
                {
                    this.AddError(obj, String.Format("Object {0} is part of a LOD hierarchy but isn't at the root of the container/scene.  To be exported correctly this object needs to be unlinked from its parent and placed at the root of the container/scene.", obj.Name));
                    result = false;
                }

                if (!obj.IsEntity() && !obj.IsMilo() && !obj.IsContainerLODRefObject())
                {
                    this.AddError(obj, String.Format("Object {0} is part of a LOD hierarchy but is neither a valid entity, a milo nor a container LOD ref object.  This object needs to be removed from the LOD hierarchy.", obj.Name));
                    result = false;
                }
            }

            if (!obj.IsObject() && !obj.IsContainerLODRefObject())
                return true;

            // Check the don't export settings
            if (obj.HasLODChildren())
            {
                if (!obj.IsContainerLODRefObject())
                {
                    foreach (TargetObjectDef childObject in obj.LOD.Children)
                    {
                        if (childObject.DontExport())
                        {
                            this.AddError(childObject, String.Format("Object {0} has LOD parent ({1}) but is set to not export.  This is invalid.", childObject.Name, obj.Name));
                            result = false;
                        }
                    }
                }
                else
                {
                    string[] childObjectNamesSetToDontExport = obj.LOD.Children.Where(childObject => childObject.DontExport()).Select(childObject => childObject.Name).ToArray();
                    if (childObjectNamesSetToDontExport.Length > 0)
                    {
                        string errorMessage = String.Format(
                            "LOD Container Ref object {0} has LOD children ({1}) that are set to not export.  This is invalid.", 
                            obj.Name,
                            String.Join(",", childObjectNamesSetToDontExport));
                        this.AddError(obj, errorMessage);
                        result = false;
                    }
                }
            }

            if (obj.HasLODParent())
            {
                if (obj.LOD.Parent.DontExport())
                {
                    this.AddError(obj, String.Format("Object {0} has LOD parent ({1}) that is set to not export.  This is invalid.", obj.Name, obj.LOD.Parent.Name));
                    result = false;
                }
            }

            // Make sure no-one has stuck a non-object in the LOD hierarchy
            if (obj.HasLODChildren())
            {
                foreach (TargetObjectDef childObject in obj.LOD.Children)
                {
                    if (!childObject.IsEntity() && !childObject.IsMilo())
                    {
                        this.AddError(childObject, String.Format("Object {0} has a LOD parent ({1}) but is not a Gta Object.  This is invalid.", childObject.Name, obj.Name));
                        result = false;
                    }
                }
            }

            // B* 941720 - Check that LOD children are of the same LOD level
            if (obj.HasLODChildren())
            {
                SortedSet<ObjectDef.LodLevel> childLODLevels = new SortedSet<ObjectDef.LodLevel>();
                foreach (TargetObjectDef childObject in obj.LOD.Children.Where(lodChild => lodChild.IsEntity()))
                    childLODLevels.Add(childObject.LODLevel);

                if (childLODLevels.Count > 1)
                {
                    this.AddError(obj, 
                        String.Format("Object {0} has a LOD level of '{1}' and LOD children with different LOD levels ('{2}').", 
                        obj.Name,
                        obj.LODLevel.ToString(),
                        String.Join(",", childLODLevels)));
                    result = false;
                }
            }

            if (obj.HasLODChildren() || obj.HasLODParent())
            {
                int priority = obj.GetAttribute(AttrNames.OBJ_PRIORITY, AttrDefaults.OBJ_PRIORITY);
                if (priority > 0)
                {
                    string error = String.Format(
                        "The object '{0}' is part of a LOD hierarchy but also has a priority greater than 0 (it's set to {1}).  " + 
                        "The priority must be 0 for objects in the LOD hierarchy.", obj.Name, priority);
                    AddError(obj, error);
                    result = false;
                }
            }

            if (obj.HasLODChildren())
            {
                List<TargetObjectDef> lowPriorityChildren = new List<TargetObjectDef>();
                foreach (TargetObjectDef childObj in obj.LOD.Children)
                {
                    bool lowPriority = childObj.GetAttribute(AttrNames.OBJ_LOW_PRIORITY, AttrDefaults.OBJ_LOW_PRIORITY);
                    if (lowPriority)
                        lowPriorityChildren.Add(childObj);
                }

                if (lowPriorityChildren.Count > 0 && lowPriorityChildren.Count != obj.LOD.Children.Length)
                {
                    this.AddError(obj,
                        String.Format("Object {0} has LOD children with a mixture of 'Low Priorty' flag settings.  " + 
                                      "All LOD children should have the same 'Low Priority' flag setting.  " + 
                                      "The following children have the 'Low Priority' flag set '{1}'",
                        obj.Name,
                        String.Join(", ", lowPriorityChildren.Select(lowPriChild => lowPriChild.Name).ToArray())));
                    result = false;
                }
            }

            return result;
        }
        #endregion // IObjectDefValidator Methods
    }

    /// <summary>
    /// Object LOD hierarchy validation for "Gta Object" attribute class objects [phase 2].
    /// </summary>
    /// We iterate through the entire scene for all LOD hierarchies.
    /// 
    internal class LODHierarchyConsistancy : ISceneValidator
    {
        #region ISceneValidator Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="scene"></param>
        /// <returns></returns>
        public override bool Validate(IBranch branch, ContentTreeHelper treeHelper, Scene scene, ValidatorOptions options)
        {
            foreach (TargetObjectDef obj in scene.Objects)
            {
                if (ObjectDef.InstanceType.HD == obj.GetInstanceType())
                    continue; // Skip HD, only validate LOD/SLOD objects

                // Validate LOD children's fade distance against radius of 
                // bounding sphere of self.
                BoundingSpheref bsphere = new BoundingSpheref(obj.LocalBoundingBox);
                foreach (TargetObjectDef child in obj.LOD.Children)
                {
                    float childDist = SceneOverrideIntegrator.Instance.GetObjectLODDistance(child);
                    if (childDist < bsphere.Radius)
                    {
                        this.AddWarning(child, String.Format("LOD child of {0} has fade distance ({1}) less than bounding sphere radius ({2}).  You may get strange fading between child and LOD objects.", 
                            obj.Name, childDist, bsphere.Radius));
                    }
                }
            }
            return (true);
        }
        #endregion // ISceneValidator Methods
    }

} // MapExportValidation.Validator namespace

// LODHierarchy.cs
