﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Configuration;
using RSG.Base.Math;
using RSG.Base.Logging;
using RSG.Pipeline.Content;
using RSG.SceneXml;
using RSG.SceneXml.MapExport;
using RSG.SceneXml.MapExport.Framework;
using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{
    /// <summary>
    /// Validates the contents of an IMAP group that will be manually streamed
    /// </summary>
    internal class ManualStreamIMAPValidator : ISceneValidator
    {
        #region Constants
        private static String URL_MILO_AND_HD = "https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#Map_IPL_Group_has_HD_Drawables_and_MILO_Interior";
        #endregion // Constants

        #region Controller Methods

        public override bool Validate(IBranch branch, ContentTreeHelper treeHelper, Scene scene, ValidatorOptions options)
        {
            bool result = true;

            switch (scene.SceneType)
            {
                case SceneType.EnvironmentProps:
                case SceneType.EnvironmentInterior:
                    Log.Log__MessageCtx(Path.GetFileNameWithoutExtension(scene.Filename),
                        "Skipping ManualStreamIMAPValidator for props/interior scene.");
                    return (true);
            }

            Dictionary<string, List<TargetObjectDef>> imapGroupObjectsMap = new Dictionary<string, List<TargetObjectDef>>();

            GetIMAPGroupObjectsRecursive(scene.Objects, imapGroupObjectsMap);

            foreach (KeyValuePair<string, List<TargetObjectDef>> imapGroupObjectsPair in imapGroupObjectsMap)
            {
                IEnumerable<TargetObjectDef> milos = imapGroupObjectsPair.Value.Where(objectDef => objectDef.IsMiloTri());
                IEnumerable<TargetObjectDef> hd_drawables = imapGroupObjectsPair.Value.Where(objectDef => objectDef.IsObject() && objectDef.LODLevel == ObjectDef.LodLevel.HD);

                if (milos.Count() > 1)
                {
                    string message = String.Format(
                        "IPL Group '{0}' has more than one MILO.  Only one is allowed.  The MILOs in the group are: {1}",
                        imapGroupObjectsPair.Key,
                        String.Join(", ", milos.Select(milo => milo.Name)));
                    AddError(milos.First(), message);
                    result = false;
                }
                else if (milos.Count() == 1 && hd_drawables.Count() > 0)
                {
                    string iplGroupName = imapGroupObjectsPair.Key;
                    string hdCount = hd_drawables.Count().ToString();
                    string hdNames = String.Join(", ", hd_drawables.Select(drawable => drawable.Name));

                    string message = String.Format(
                        "IPL Group '{0}' has {1} HD drawable(s) as well as the MILO '{2}'.  MILOs in IPL groups must be alone.  The HD drawables are: {3}.  For more information see {4}.",
                        iplGroupName,
                        hdCount,
                        milos.First().Name,
                        hdNames, 
                        URL_MILO_AND_HD);
                    AddError(milos.First(), message);
                    result = false;
                }
            }

            return result;
        }

        private static void GetIMAPGroupObjectsRecursive(IEnumerable<TargetObjectDef> objectDefs, Dictionary<string, List<TargetObjectDef>> imapGroupObjectsMap)
        {
            foreach (TargetObjectDef objectDef in objectDefs)
                GetIMAPGroupObjectsRecursive(objectDef, imapGroupObjectsMap);
        }

        private static void GetIMAPGroupObjectsRecursive(TargetObjectDef objectDef, Dictionary<string, List<TargetObjectDef>> imapGroupObjectsMap)
        {
            if ((objectDef.IsObject() || objectDef.IsMiloTri()) && 
                !objectDef.DontExport() && 
                !objectDef.DontExportIPL())
            {
                String imapGroupName = objectDef.GetAttribute(AttrNames.OBJ_IPL_GROUP, AttrDefaults.OBJ_IPL_GROUP).ToLower();
                bool imapGroupSet = !String.IsNullOrEmpty(imapGroupName) && (0 != String.Compare(imapGroupName, AttrDefaults.OBJ_IPL_GROUP, true));
            
                if(imapGroupSet)
                {
                    if(!imapGroupObjectsMap.ContainsKey(imapGroupName))
                        imapGroupObjectsMap.Add(imapGroupName, new List<TargetObjectDef>());
                    imapGroupObjectsMap[imapGroupName].Add(objectDef);
                }
            }

            if (objectDef.Children != null)
            {
                GetIMAPGroupObjectsRecursive(objectDef.Children, imapGroupObjectsMap);
            }
        }

        #endregion
    }
}
