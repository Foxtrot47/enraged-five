﻿//
// File: IPLGroup.cs
// Author: Adam Munson <Adam.Munson@rockstarnorth.com>
// Description: Implementation of Pivot Distance validator class
//

using System;
using System.Collections.Generic;
using RSG.Base.Configuration;
using RSG.Base.Math;
using RSG.SceneXml;
using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{
    
    /// <summary>
    /// Pivot Distance validator for "Gta Object" attribute class objects.
    /// </summary>
    /// 
    internal class PivotDistance : IObjectDefValidator
    {
        #region Constants
        private static readonly double THRESHOLD = 0.2; // 20%
        #endregion // Constants

        #region Controller Methods
        /// <summary>
        /// Check the pivot distance from its AABB centre
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Validate(IBranch branch, TargetObjectDef obj, RSG.SceneXml.Scene scene, ValidatorOptions options)
        {
            if (!obj.IsObject())
                return (true);

            switch (obj.LODLevel)
            {
                case ObjectDef.LodLevel.LOD:
                case ObjectDef.LodLevel.SLOD1:
                case ObjectDef.LodLevel.SLOD2:
                case ObjectDef.LodLevel.SLOD3:
                case ObjectDef.LodLevel.SLOD4:
                    break;

                default:
                    return(true);
            }

            Vector3f pivot = obj.ObjectTransform.Translation;
            BoundingBox3f boundingBox = new BoundingBox3f(obj.WorldBoundingBox.Min, obj.WorldBoundingBox.Max);

            float largestDimension = boundingBox.Max.X - boundingBox.Min.X;
            float dimY = boundingBox.Max.Y - boundingBox.Min.Y;
            float dimZ = boundingBox.Max.Z - boundingBox.Min.Z;
            
            // Find which dimension is largest of the 3
            if ( dimY > largestDimension)
            {
                largestDimension = dimY;
            }
            if ( dimZ > largestDimension )
            {
                largestDimension = dimZ;
            }

            // Find out the maximum distance offset from pivot to bb centre before we flag the object
            // as being above threshold
            double radius = largestDimension / 2.0;
            double maxOffset = radius * THRESHOLD;
            double distance = (boundingBox.Centre() - pivot).Magnitude();

            // If object pivot is further away than threshold amount, create struct for the info we 
            // want to display
            if (distance > maxOffset )
            {
                this.AddInfo(obj, String.Format("Pivot distance is over {0}% away({1}) from its AABB centre.", (THRESHOLD * 100),distance.ToString("#.##")));
                return (false);
            }

            return (true);
        }
        #endregion // Controller Methods
    }

} // MapExportValidation.Validator namespace

// PivotDistance.cs
