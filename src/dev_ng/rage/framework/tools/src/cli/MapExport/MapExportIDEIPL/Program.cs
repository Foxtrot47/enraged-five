//
// File: Program.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of Program class
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.OS;
using RSG.Base.Profiling;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.SceneXml;
using RSG.SceneXml.MapExport.AssetCombine;
using RSG.SceneXml.MapExport.Project.GTA5_NG;
using RSG.SceneXml.MapExport.Project.GTA5_NG.Collections;
using RSG.SceneXml.MapExport.Util;
using RSG.SceneXml.MapExport;

namespace MapExportIDEIPL
{

    /// <summary>
    /// Main Program Static Class defining entry-point.
    /// </summary>
    static class Program
    {
        #region Constants
        private static readonly int EXIT_SUCCESS = 0;
        private static readonly int EXIT_ERROR = 1;
        
        // Map Metadata Options
        private static readonly String OPTION_ITYP = "ityp";
        private static readonly String OPTION_ITYP_OBJECT = "itypobject";
        private static readonly String OPTION_IMAP = "imap";
        private static readonly String OPTION_ASSETCOMBINE = "asset_combine";

        // Map Metadata Options (OLD)
        private static readonly String OPTION_IDE = "ide";
        private static readonly String OPTION_META = "meta";
        private static readonly String OPTION_IDE_OBJECT = "ideobject";
        private static readonly String OPTION_IPL6 = "ipl6";
        private static readonly String OPTION_SINGLE = "single";
        private static readonly String OPTION_IPLSTREAM = "iplstream";

        // Generic Options
        private static readonly String OPTION_LOG = "log";
        private static readonly String OPTION_UNILOG = "unilog";
        private static readonly String OPTION_PROFILE = "profile";
        private static readonly String OPTION_BUILD = "build";
        private static readonly String OPTION_PREVIEW = "preview";
        private static readonly String OPTION_EPISODIC = "episodic";
        private static readonly String OPTION_GENERATESTATS = "genstats";
        private static readonly String OPTION_STREAMSTATSLOCATION = "mapStream";
        private static readonly String OPTION_NEW_UNILOG = "new_unilog";

        #endregion // Constants

        #region Static Member Data
        private static readonly String AUTHOR = "David Muir";
        private static readonly String EMAIL = "david.muir@rockstarnorth.com";
        #endregion // Static Member Data

        #region Entry-Point
        /// <summary>
        /// The main entry point for the application.
        /// Attribute [STAThread] for invoking the universal log from the right thread
        /// </summary>
        [STAThread]
        static int Main(String[] args)
        {
            int exit_code = Constants.Exit_Success;

            #region Option Parsing and Setup
            // Define our executable options.  Stick with the long options
            // (e.g. --opt) rather than the single character versions.
            LongOption[] lopts = new LongOption[] {
                new LongOption(OPTION_ITYP, LongOption.ArgType.Required, ""),
                new LongOption(OPTION_IMAP, LongOption.ArgType.Required, ""),

                new LongOption(OPTION_IDE, LongOption.ArgType.Required, ""),
                new LongOption(OPTION_META, LongOption.ArgType.Required, ""),
                new LongOption(OPTION_IDE_OBJECT, LongOption.ArgType.Required, ""),
                new LongOption(OPTION_IPL6, LongOption.ArgType.Required, ""),
                new LongOption(OPTION_SINGLE, LongOption.ArgType.None, ""),
                new LongOption(OPTION_IPLSTREAM, LongOption.ArgType.Required, ""),

                new LongOption(OPTION_ASSETCOMBINE, LongOption.ArgType.Required, ""),
                new LongOption(OPTION_LOG, LongOption.ArgType.Required, ""),
                new LongOption(OPTION_UNILOG, LongOption.ArgType.Required, ""),
                new LongOption(OPTION_PROFILE, LongOption.ArgType.None, ""),
                new LongOption(OPTION_BUILD, LongOption.ArgType.Required, ""),
                new LongOption(OPTION_PREVIEW, LongOption.ArgType.None, ""),
                new LongOption(OPTION_GENERATESTATS, LongOption.ArgType.None, ""),
                new LongOption(OPTION_STREAMSTATSLOCATION, LongOption.ArgType.Required, ""),
                new LongOption(OPTION_NEW_UNILOG, LongOption.ArgType.None, "")
            };
            CommandOptions options = new CommandOptions(args, lopts);

            // Setup the profiler
            bool profile = options.ContainsOption(OPTION_PROFILE);
            IProfiler profiler;
            if (profile)
                profiler = ProfilerFactory.CreateProfiler();
            else
                profiler = ProfilerFactory.CreateStubProfiler();

            ITask mainTask = profiler.StartTask("Main()");

            // Parse our options.
            String logFilename = options.ContainsOption(OPTION_LOG) ? (options[OPTION_LOG] as String) : String.Empty;
            LogFactory.Initialize();
            LogFactory.CreateUniversalLogFile(Log.StaticLog);
            LogFactory.CreateApplicationConsoleLogTarget();

            bool createItyp = options.ContainsOption(OPTION_ITYP);
            bool createItypForObject = options.ContainsOption(OPTION_ITYP_OBJECT);
            bool createImap = options.ContainsOption(OPTION_IMAP);
            bool createIde = options.ContainsOption(OPTION_IDE);
            bool createMeta = options.ContainsOption(OPTION_META);
            bool createIdeForObject = options.ContainsOption(OPTION_IDE_OBJECT);
            bool createIpl6 = options.ContainsOption(OPTION_IPL6);
            bool createSingleIpl = options.ContainsOption(OPTION_SINGLE);
            bool episodic = options.ContainsOption(OPTION_EPISODIC);
            bool genstats = options.ContainsOption(OPTION_GENERATESTATS);
            bool preview = options.ContainsOption(OPTION_PREVIEW);
            String itypfile = createItyp ? (options[OPTION_ITYP] as String) : String.Empty;
            String imapfile = createImap ? (options[OPTION_IMAP] as String) : String.Empty;
            String idefile = createIde ? (options[OPTION_IDE] as String) : String.Empty;
            String metafile = createMeta ? (options[OPTION_META] as String) : String.Empty;
            String ideObjectRootDir = createIdeForObject ? (options[OPTION_IDE_OBJECT] as String) : String.Empty;
            String iplfile6 = createIpl6 ? (options[OPTION_IPL6] as String) : String.Empty;
            String iplstream = options[OPTION_IPLSTREAM] as String;
            String assetCombineFile = options.ContainsOption(OPTION_ASSETCOMBINE) ?
                (options[OPTION_ASSETCOMBINE] as String) : String.Empty;
                
            Debug.Assert(createItyp || createImap || createItypForObject ||
                         createIde || createIdeForObject || createMeta || createIpl6 || genstats,
                "Must specify genstats, ityp, imap, itypobjct, ide, ideobject or ipl6 on command line otherwise we have no work to do.");
            if (!(createItyp || createImap || createItypForObject || createIde || createIdeForObject || createMeta || createIpl6 || genstats))
                return (EXIT_ERROR);
            Debug.Assert(options.TrailingArguments.Any(),
                "Must specify at least one trailing argument SceneXml XML file.");
            if (!options.TrailingArguments.Any())
                return (EXIT_ERROR);
            Debug.Assert(!episodic, "Episodic export is currently not enabled.");
            #endregion // Option Parsing and Setup
                        
            try
            {
                ITask loadConfigGameViewTask = profiler.StartTask("Loading config");
                // Load Content-Tree.  This is required for the SceneCollection.
                IContentTree tree = Factory.CreateTree(options.Branch);
                ContentTreeHelper contentHelper = new ContentTreeHelper(tree);

                profiler.StopTask(loadConfigGameViewTask);

                String sceneOverrideDirectory = System.IO.Path.Combine(options.Branch.Assets, "maps", "scene_overrides");
                SceneOverrideIntegrator.Instance.Initialise(options.Branch, contentHelper, sceneOverrideDirectory);
                String builddir = (options[OPTION_BUILD] as String);

                SceneCollection sceneCollection = null;
                if (!createIdeForObject && !genstats)
                {
                    ITask genericSceneLoadTask = profiler.StartTask("Loading generic scene data");
                    sceneCollection = new SceneCollection(options.Branch, tree);
                    profiler.StopTask(genericSceneLoadTask);
                }

                ITask contextSpecificSceneLoadTask = profiler.StartTask("Loading context-specific scene data");
                List<Scene> scenes = new List<Scene>();
                foreach (String filename in options.TrailingArguments)
                {
                    Scene scene = null;
                    if (null != sceneCollection)
                        scene = sceneCollection.LoadScene(filename);
                    else
                    {
                        scene = new Scene(tree, contentHelper, filename);
                    }
                    scenes.Add(scene);
                }
                AssetFile assetCollection = null;
                if (!String.IsNullOrEmpty(assetCombineFile) && System.IO.File.Exists(assetCombineFile))
                {
                    Log.Log__Message("Loading Asset Combine data from '{0}'.", assetCombineFile);
                    assetCollection = new AssetFile(options.Branch, assetCombineFile);
                }
                else if (!String.IsNullOrWhiteSpace(assetCombineFile))
                {
                    Log.Log__Warning("Asset Combine input file does not exist: '{0}'.", assetCombineFile);
                }
                profiler.StopTask(contextSpecificSceneLoadTask);
                
                // Map Metadata
                InteriorContainerNameMap interiorNameMap = new InteriorContainerNameMap();
                XDocument xmlManifestAdditions = CreateManifestFile(scenes);
                SceneSerialiserITYP itypSerialiser = null;
                if (createItyp && !createItypForObject)
                {
                    IDictionary<String, IEnumerable<ArchetypeLite>> coreArchetypes = 
                        new Dictionary<String, IEnumerable<ArchetypeLite>>();
                    ITask itypGenerationTask = profiler.StartTask("Generate ITYP data");
                    CheckFilePermission(itypfile);
                    CheckOutputDirectory(itypfile);
                    bool splitItyp = createImap;
                    itypSerialiser = new SceneSerialiserITYP(options.Branch, scenes.ToArray(), splitItyp, assetCollection, interiorNameMap,
                        String.Empty, coreArchetypes);
                    itypSerialiser.Process(itypfile);
                    itypSerialiser.Serialise(itypfile);
                    itypSerialiser.WriteManifestData(xmlManifestAdditions);
                    profiler.StopTask(itypGenerationTask);
                }
                if (createImap)
                {
                    ITask imapGenerationTask = profiler.StartTask("Generate IMAP data");
                    if (preview)
                    {
                        foreach (string existingImapFile in System.IO.Directory.EnumerateFiles(iplstream, "*.imap", SearchOption.TopDirectoryOnly))
                        {
                            System.IO.File.Delete(existingImapFile);
                        }
                    }

                    CheckFilePermission(imapfile);
                    CheckOutputDirectory(imapfile);
                    CheckOutputDirectory(iplstream);
                    ITYPContainer itypContainer = null;
                    ITYPContainer itypStreamedContainer = null;
                    if (createItyp)
                    {
                        itypContainer = itypSerialiser.Container;
                        if (itypSerialiser.StreamedContainer.ArchetypeCount > 0)
                            itypStreamedContainer = itypSerialiser.StreamedContainer;// We need to avoid dependencies on data that won't be generated - B* 628077
                    }
                    SceneSerialiserIMAP imapSerialiser = new SceneSerialiserIMAP(options.Branch,
                        scenes.ToArray(), itypContainer, itypStreamedContainer, iplstream, builddir, 
                        interiorNameMap, new String[]{});
                    imapSerialiser.Process(imapfile);
                    imapSerialiser.Serialise(imapfile);
                    imapSerialiser.WriteManifestData(xmlManifestAdditions, null);

                    if (preview)
                    {
                        IMAPPreview.GenerateIMAPsForPreview_GTA5_NG(options.Branch, iplstream, tree, scenes[0], imapSerialiser);
                    }

                    profiler.StopTask(imapGenerationTask);
                }
                SaveManifestFile(options.Branch, xmlManifestAdditions, Path.GetFileNameWithoutExtension(scenes[0].Filename));

                // Stats Generation
                if (genstats)
                {
                    ITask statsGenerationTask = profiler.StartTask("Generate stats");

                    String mapStream = options[OPTION_STREAMSTATSLOCATION] as String;
                    foreach (Scene scene in scenes)
                    {
                        CheckFilePermission(scene.Filename);
                        scene.GenerateStats(mapStream);
                    }

                    profiler.StopTask(statsGenerationTask);
                }

                // Map Metadata (OLD)
                if (createIde && !createIdeForObject)
                {
                    CheckFilePermission(idefile);
                    CheckOutputDirectory(idefile);
                    SceneSerialiserIDE ide = new SceneSerialiserIDE(options.Branch, scenes.ToArray());
                    ide.Write(idefile);
                }
                if (createIdeForObject)
                {
                    SceneSerialiserIDEObject serialiser = new SceneSerialiserIDEObject(scenes[0]);
                    serialiser.Write(ideObjectRootDir);
                }
                if (createMeta)
                {
                    CheckFilePermission(metafile);
                    CheckOutputDirectory(metafile);
                    SceneSerialiserMETA meta = new SceneSerialiserMETA(options.Branch, scenes.ToArray());
                    meta.Write(metafile);
                }
                if (createIpl6)
                {
                    CheckFilePermission(iplfile6);
                    CheckOutputDirectory(iplfile6);
                    if (!String.IsNullOrEmpty(iplstream))
                        CheckOutputDirectory(Path.Combine(iplstream, "test.ipl"));
                    SceneSerialiserIPL6 ipl = new SceneSerialiserIPL6(options.Branch, scenes.ToArray(), iplstream, builddir, createSingleIpl);
                    ipl.Write(iplfile6);
                }
            }
            catch (IDESerialiserFatalError ex)
            {
                Log.Log__Exception(ex, "Fatal IDE serialiser error");
                exit_code = Constants.Exit_Failure;
            }
            catch (IPLSerialiserFatalError ex)
            {
                Log.Log__Exception(ex, "Fatal IPL serialiser error");
                exit_code = Constants.Exit_Failure;
            }
            catch (IMDSerialiserFatalError ex)
            {
                Log.Log__Exception(ex, "Fatal IMD serialiser error");
                exit_code = Constants.Exit_Failure;
            }
            catch (SerialiserNotReadableError ex)
            {
                Log.Log__Exception(ex, "Fatal serialiser error");
                exit_code = Constants.Exit_Failure;
            }
            catch (Exception ex)
            {
                // Log the exception.
                Log.Log__Exception(ex, "Unhandled exception during map metadata serialisation.");

                // Display exception dialog only if we are allowed.
                if (!options.NoPopups)
                {
                    RSG.Base.Windows.ExceptionStackTraceDlg stackDlg =
                        new RSG.Base.Windows.ExceptionStackTraceDlg(ex, AUTHOR, EMAIL);
                    stackDlg.Show();
                }
                exit_code = Constants.Exit_Failure;
            }
            
            if (Log.StaticLog.HasErrors)
                exit_code = Constants.Exit_Failure;
            
            LogFactory.ApplicationShutdown();
            Environment.ExitCode = exit_code;
            profiler.StopTask(mainTask);

            string profilerPathName = Path.Combine(options.Branch.Project.Config.ToolsLogs, 
                "map_metadata_profile.csv");
            profiler.WriteLog(profilerPathName);

            return (exit_code);
        }
        #endregion // Entry-Point

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="scenes"></param>
        /// <returns></returns>
        private static XDocument CreateManifestFile(IEnumerable<Scene> scenes)
        {
            XDocument document = new XDocument(new XDeclaration("1.0", "utf-8", null));
            StringBuilder description = new StringBuilder();
            description.Append("IMAP file generated from SceneXml files:\n");
            foreach (Scene scene in scenes)
                description.AppendFormat("\t\t{0}{1}", scene.Filename, Environment.NewLine);
            XComment xmlDescription = new XComment(description.ToString());
            document.Add(xmlDescription);

            XElement rootElem = new XElement("ManifestData");
            document.Add(rootElem);

            return (document);
        }

        /// <summary>
        /// Save manifest data.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="xmlDoc"></param>
        /// <param name="filename"></param>
        private static void SaveManifestFile(IBranch branch, XDocument xmlDoc, String filename)
        {
            String manifestAdditionsFilename = Path.Combine(
                branch.Project.Config.ToolsTemp, "manifest", filename, "MapExportAdditions.xml");
            if (System.IO.File.Exists(manifestAdditionsFilename))
                System.IO.File.Delete(manifestAdditionsFilename);

            String manifestAdditionsDirectory = Path.GetDirectoryName(manifestAdditionsFilename);
            if (!System.IO.Directory.Exists(manifestAdditionsDirectory))
            {
                System.IO.Directory.CreateDirectory(manifestAdditionsDirectory);
            }
            xmlDoc.Save(manifestAdditionsFilename);
        }

        /// <summary>
        /// Ensure filepath is writable.
        /// </summary>
        /// <param name="filepath"></param>
        static private void CheckFilePermission(String filepath)
        {
            if (System.IO.File.Exists(filepath) &&
                FileAttributes.ReadOnly == (System.IO.File.GetAttributes(filepath) & FileAttributes.ReadOnly))
            {
                throw new SerialiserNotReadableError(String.Format("Scene XML file: {0} is read-only.  Aborting.", filepath));
            }
        }

        /// <summary>
        /// Ensure filepath directory exists.
        /// </summary>
        /// <param name="filepath"></param>
        static private void CheckOutputDirectory(String filepath)
        {
            if (String.IsNullOrEmpty(filepath))
                return;

            String directory = Path.GetDirectoryName(filepath);
            if (!System.IO.Directory.Exists(directory))
                System.IO.Directory.CreateDirectory(directory);
        }
        #endregion // Private Methods
     }

} // MapExportIDEIPL namespace

// Program.cs
