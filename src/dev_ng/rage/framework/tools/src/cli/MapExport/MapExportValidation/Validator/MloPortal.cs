//
// File: MloPortal.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of MloPortal class
//
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Pipeline.Content;
using RSG.SceneXml;
using RSG.SceneXml.MapExport;

using MapExportValidation.Validation;


namespace MapExportValidation.Validator
{
    /// <summary>
    /// 
    /// </summary>
    internal class MloPortal : ISceneValidator
    {
        #region IObjectDefValidator Methods
        /// <summary>
        /// Validate a single MloPortal object.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Validate(IBranch branch, ContentTreeHelper treeHelper, Scene scene, ValidatorOptions options)
        {
            bool result = true;

            // Portal pass to check for objects attached to >1 portal.
            // Dictionary mapping attached object GUIDs to container portal.
            Dictionary<Guid, TargetObjectDef> attachedObjects = new Dictionary<Guid, TargetObjectDef>();
            foreach (TargetObjectDef o in scene.Walk(Scene.WalkMode.DepthFirst))
            {
                if (!o.IsMloPortal())
                    continue; // Skip non-MloPortal objects.

                // Validation for flag combinations depending on if a mirror type is set or not.
                int mirrorTypeValue = o.GetAttribute(AttrNames.MLOPORTAL_MIRROR_TYPE, AttrDefaults.MLOPORTAL_MIRROR_TYPE);
                Flags.MLOPortalMirrorType mirrorType = (Flags.MLOPortalMirrorType)mirrorTypeValue;
                Debug.Assert(mirrorType >= Flags.MLOPortalMirrorType.NONE && mirrorType < Flags.MLOPortalMirrorType.COUNT);

                // These attributes are retrieved here for a special check to make sure that if Mirror Can See Exterior View is
                // set, so is Mirror Using Portal Traversal
                bool canSeeExteriorView = o.GetAttribute(AttrNames.MLOPORTAL_MIRROR_CAN_SEE_EXTERIOR_VIEW, 
                    AttrDefaults.MLOPORTAL_MIRROR_CAN_SEE_EXTERIOR_VIEW);
                bool portalTraversal = o.GetAttribute(AttrNames.MLOPORTAL_MIRROR_PORTAL_TRAVERSAL, 
                    AttrDefaults.MLOPORTAL_MIRROR_PORTAL_TRAVERSAL);

                if (canSeeExteriorView && !portalTraversal)
                    this.AddError(o, String.Format("Object {0} has '{1}' set, but has not got '{2}' set, which has to be.", o.Name,
                        AttrNames.MLOPORTAL_MIRROR_CAN_SEE_EXTERIOR_VIEW, AttrNames.MLOPORTAL_MIRROR_PORTAL_TRAVERSAL));

                // To allow for the situation where this validation is being done on a scene which hasn't been exported since the new
                // portal mirror changes, check if the older "Mirror" or "Mirror Floor" attributes are set.  If so, skip this validation
                // entirely as these attributes will be set to false on export when moving to the newer "Mirror Type" attribute, so we can't
                // validate on the newer Mirror Type just yet.
#warning AJM: 17/09/12: Remove backwards compatibility check for older interior portal flags, in a few months
                if (!o.GetAttribute(AttrNames.MLOPORTAL_MIRROR, AttrDefaults.MLOPORTAL_MIRROR) &&
                    !o.GetAttribute(AttrNames.MLOPORTAL_MIRROR_FLOOR, AttrDefaults.MLOPORTAL_MIRROR_FLOOR))
                {
                    List<String> conflictingFlags = new List<String>();
                    // Check for conflicting flags if no mirror type is set
                    if (mirrorType == Flags.MLOPortalMirrorType.NONE)
                    {
                        if (canSeeExteriorView)
                            conflictingFlags.Add(AttrNames.MLOPORTAL_MIRROR_CAN_SEE_EXTERIOR_VIEW);
                        if (portalTraversal)
                            conflictingFlags.Add(AttrNames.MLOPORTAL_MIRROR_PORTAL_TRAVERSAL);
                        
                        bool mirrorUsesDirectionalLight = o.GetAttribute(AttrNames.MLOPORTAL_MIRROR_USES_DIRECTIONAL_LIGHT, 
                            AttrDefaults.MLOPORTAL_MIRROR_USES_DIRECTIONAL_LIGHT);
                        if (mirrorUsesDirectionalLight)
                            conflictingFlags.Add(AttrNames.MLOPORTAL_MIRROR_USES_DIRECTIONAL_LIGHT);

                        bool expensiveShaders = o.GetAttribute(AttrNames.MLOPORTAL_EXPENSIVE_SHADERS, 
                            AttrDefaults.MLOPORTAL_EXPENSIVE_SHADERS);
                        if (expensiveShaders)
                            conflictingFlags.Add(AttrNames.MLOPORTAL_EXPENSIVE_SHADERS);

                        // If flag is true, it shouldn't be set in this case as no mirror type is set, so error
                        foreach (String conflictingFlag in conflictingFlags)
                        {
                            this.AddError(o, String.Format(
                                "Object {0} has no mirror type set, but has '{1}' set which isn't allowed if no mirror type is set.", 
                                o.Name, conflictingFlag));
                        }
                    }
                    // One of the mirror types is set so check for flags which aren't meant to be ticked when it's a mirror.
                    else
                    {
                        bool allowClosing = o.GetAttribute(AttrNames.MLOPORTAL_ALLOW_CLOSING, 
                            AttrDefaults.MLOPORTAL_ALLOW_CLOSING);
                        if (allowClosing)
                            conflictingFlags.Add(AttrNames.MLOPORTAL_ALLOW_CLOSING);

                        bool drawLowLODOnly = o.GetAttribute(AttrNames.MLOPORTAL_DRAW_LOW_LOD_ONLY, 
                            AttrDefaults.MLOPORTAL_DRAW_LOW_LOD_ONLY);
                        if(drawLowLODOnly)
                            conflictingFlags.Add(AttrNames.MLOPORTAL_DRAW_LOW_LOD_ONLY);

                        bool oneWay = o.GetAttribute(AttrNames.MLOPORTAL_ONE_WAY, AttrDefaults.MLOPORTAL_ONE_WAY);
                        if(oneWay)
                            conflictingFlags.Add(AttrNames.MLOPORTAL_ONE_WAY);

                        bool link = o.GetAttribute(AttrNames.MLOPORTAL_MLO_LINK, AttrDefaults.MLOPORTAL_MLO_LINK);
                        if(link)
                            conflictingFlags.Add(AttrNames.MLOPORTAL_MLO_LINK);

                        bool ignoreByModifier = o.GetAttribute(AttrNames.MLOPORTAL_IGNORE_BY_MODIFIER, 
                            AttrDefaults.MLOPORTAL_IGNORE_BY_MODIFIER);
                        if(ignoreByModifier)
                            conflictingFlags.Add(AttrNames.MLOPORTAL_IGNORE_BY_MODIFIER);

                        // If flag is true, it shouldn't be set in this case as there is a mirror type set, so error
                        foreach (String conflictingFlag in conflictingFlags)
                        {
                            this.AddError(o, String.Format(
                                "Object {0} has a mirror type set, but has '{1}' set which isn't allowed if a mirror type is set.", 
                                o.Name, conflictingFlag));
                        }
                    }
                } // End of Mirror conflicting attributes checks
            }

            // B* 896042 - check for dynamic objects that aren't attached to a portal
            foreach (TargetObjectDef miloObjectDef in scene.Walk(Scene.WalkMode.DepthFirst).Where(objectDef => objectDef.IsMilo()))
            {
                // 1) Find all guids of objects attached to portals in this milo
                SortedSet<Guid> portalAtatchedObjectGuids = new SortedSet<Guid>();
                TargetObjectDef[] portalObjectDefs = miloObjectDef.Children.Where(childObjDef => childObjDef.IsMloPortal()).ToArray();
                foreach (TargetObjectDef portalObjectDef in portalObjectDefs)
                {
                    Object[] attachedObjectGuidsAsObject = portalObjectDef.GetParameter(
                        ParamNames.MLOPORTAL_ATTACHED_OBJECTS, ParamDefaults.MLOPORTAL_ATTACHED_OBJECTS);
                    foreach (Guid attachedObjectGuid in attachedObjectGuidsAsObject.OfType<Guid>().Where(guid => !guid.Equals(Guid.Empty)))
                        portalAtatchedObjectGuids.Add(attachedObjectGuid);
                }

                // 2) Report errors for all objects that aren't attached to a portal
                TargetObjectDef[] dynamicObjectDefs = miloObjectDef.Children.Where(
                    childObjDef => childObjDef.IsEntity() && 
                        childObjDef.IsDynObject() && 
                        !childObjDef.DontExport()).ToArray();
                foreach (TargetObjectDef dynamicObjectDef in dynamicObjectDefs)
                {
                    if (!portalAtatchedObjectGuids.Contains(dynamicObjectDef.Guid))
                    {
                        String errorMessage = String.Format(
                            "Object '{0}' is in milo '{1}' but isn't linked to a portal.  " +
                            "You need to either attach it to a portal, remove it from the milo, or mark it as 'Do not export'",
                            dynamicObjectDef.Name,
                            miloObjectDef.Name);
                        AddError(dynamicObjectDef, errorMessage);
                    }
                }
            }

            return (result);
        }
        #endregion // IObjectDefValidator Methods
    }

} // MapExportValidation.Validator

// MloPortal.cs
