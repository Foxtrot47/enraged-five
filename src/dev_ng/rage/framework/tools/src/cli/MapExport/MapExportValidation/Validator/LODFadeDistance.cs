//
// File: LODFadeDistance.cs
// Author: David Muir <david.muir@rockstarnorth.com>
// Description: Implementation of LODFadeDistance.cs class
//

using System;
using System.Collections.Generic;
using RSG.Base.Configuration;
using RSG.SceneXml;
using RSG.SceneXml.MapExport.Util;

using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{

    /// <summary>
    /// LOD Fade Distance validator for "Gta Object" attribute class objects.
    /// </summary>
    /// If objects have LOD children then their LOD distance (fade distance)
    /// should really be equal - as the IDE/IPL export will ensure they are
    /// equal so the runtime fades them out at the same time.
    /// 
    /// This is a non-fatal error.
    /// 
    internal class LODFadeDistance : IObjectDefValidator
    {
        #region Controller Methods
        /// <summary>
        /// Validate a single Object for LOD Fade Distance
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Validate(IBranch branch, TargetObjectDef obj, Scene scene, ValidatorOptions options)
        {
            if (!obj.IsObject())
                return (true);
            if (!obj.HasLODChildren())
                return (true);

            // Loop through all LOD children.
            List<float> fade_dists = new List<float>();
            foreach (TargetObjectDef child in obj.LOD.Children)
            {
                if (null == child)
                {
                    this.AddError(obj, "Object is supposed to have LOD child which is not defined in SceneXML. Check whether the objects share the container.");
                    continue;
                }
                float fade = SceneOverrideIntegrator.Instance.GetObjectLODDistance(child);
                fade_dists.Add(fade);
            }
            if (fade_dists.Count > 0)
            {
                float min = RSG.Base.Math.Utils.Minimum(fade_dists.ToArray());
                float max = RSG.Base.Math.Utils.Maximum(fade_dists.ToArray());
                if (!min.Equals(max))
                {
                    this.AddWarning(obj, String.Format("Object {0}'s LOD children do not all have the same LOD distance set.  This will be clamped in during export.", obj));
                    return (false);
                }
            }
            return (true);
        }
        #endregion // Controller Methods
    }

} // MapExportValidation.Validator

// LODFadeDistance.cs
