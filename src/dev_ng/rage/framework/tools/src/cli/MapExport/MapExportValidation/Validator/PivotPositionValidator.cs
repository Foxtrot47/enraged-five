﻿using System;
using System.Collections.Generic;
using RSG.Base.Configuration;
using RSG.Base.Math;
using RSG.SceneXml;
using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{
    class PivotPositionValidator : IObjectDefValidator
    {
        #region Controller Methods
        /// <summary>
        /// Check the pivot distance from its AABB centre
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Validate(IBranch branch, TargetObjectDef obj, RSG.SceneXml.Scene scene, ValidatorOptions options)
        {
            if (!obj.IsObject() || obj.IsRefObject() || obj.IsRefInternalObject() || obj.HasLODParent() || IsFragmentPart(obj))
                return true;

            if (SceneType.EnvironmentContainer == scene.SceneType)
                return true;

            if (obj.ObjectTransform == null || obj.WorldBoundingBox == null)
                return true;

            float worldAABBSphereRadius = (float)(obj.WorldBoundingBox.Max - obj.WorldBoundingBox.Centre()).Magnitude();
            float pivotToWorldAABBDistance = (float)(obj.WorldBoundingBox.Centre() - obj.NodeTransform.Translation).Magnitude();
            if (pivotToWorldAABBDistance > worldAABBSphereRadius)
            {
                this.AddWarning(obj, "The pivot is outside the extents of the bounding sphere.  " + 
                                     "As LOD distances are based on the pivot this could lead to issues at runtime.");
                return false;
            }

            return (true);
        }
        #endregion // Controller Methods

        private static bool IsFragmentPart(TargetObjectDef objectDef)
        {
            if (objectDef.IsFragment())
                return true;

            if (objectDef.HasParent())
                return IsFragmentPart(objectDef.Parent);

            return false;
        }
    }
}
