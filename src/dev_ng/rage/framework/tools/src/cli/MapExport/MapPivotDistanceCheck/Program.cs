﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using RSG.Base.Logging;
using RSG.Base.Math;
using RSG.Base.OS;
using RSG.SceneXml;

namespace MapPivotDistanceCheck
{
    #region structs
    //Struct for the output of information in the form, rather than using the list
    // of ObjectDef's which have heaps of fields
    public struct ObjectOutput
    {
        private String _containerName;
        private String _modelName;
        private Vector3f _pivot;
        private Vector3f _aabbCentre;
        private double _distance;

        public ObjectOutput(String containerName, String modelName, Vector3f pivot, Vector3f aabbCentre, double distance)
        {
            _containerName = containerName;
            _modelName = modelName;
            _pivot = pivot;
            _pivot.X = (float)Math.Round(pivot.X, 3);
            _pivot.Y = (float)Math.Round(pivot.Y, 3);
            _pivot.Z = (float)Math.Round(pivot.Z, 3);
            _aabbCentre = aabbCentre;
            _aabbCentre.X = (float)Math.Round(aabbCentre.X, 3);
            _aabbCentre.Y = (float)Math.Round(aabbCentre.Y, 3);
            _aabbCentre.Z = (float)Math.Round(aabbCentre.Z, 3);
            _distance = Math.Round(distance, 3) ;
        }

        public String ContainerName
        { get { return this._containerName; } }

        public String ModelName
        { get { return this._modelName; } }

        public Vector3f Pivot
        { get { return this._pivot; } }

        public Vector3f AABBCentre
        { get { return this._aabbCentre; } }

        public double Distance
        { get { return this._distance; } }
    }
    #endregion structs

    static class Program
    {
        #region constants
        private static readonly int EXIT_SUCCESS = 0;
        private static readonly int EXIT_WARNING = 1;
        private static readonly int EXIT_ERROR = 2;
        private static readonly int EXIT_CRITIC = 3;

        private static readonly String OPTION_THRESHOLD = "threshold";
        #endregion

        #region Static Member Data
        private static readonly String AUTHOR = "Adam Munson";
        private static readonly String EMAIL = "adam.munson@rockstarnorth.com";
        #endregion // Static Member Data

        private static List<ObjectOutput> objectsOverThreshold;
        private static float threshold = 0.2f;

        #region Entry-Point
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static int Main(String[] args)
        {
            int exit_code = EXIT_SUCCESS;

            Getopt options = new Getopt(args, new LongOption[] {
                new LongOption(OPTION_THRESHOLD, LongOption.ArgType.Optional, 't'),
            });

            if (options.HasOption(OPTION_THRESHOLD))
            {
                threshold = float.Parse(options[OPTION_THRESHOLD].ToString());
            }
            Console.WriteLine("Threshold: {0}", threshold);

            Debug.Assert(options.TrailingOptions.Length > 0, "Must specify at least one trailing argument SceneXml XML file.");
            if (0 == options.TrailingOptions.Length)
            {
                Log.Log__Error("No scene xml files specified");
                return (EXIT_CRITIC);
            }

            List<Scene> scenes = new List<Scene>();
            objectsOverThreshold = new List<ObjectOutput>();

            Console.WriteLine("Running...");

            // Get all of the scene xml files passed in to work on
            foreach (String filename in options.TrailingOptions)
                scenes.Add(new Scene(filename));
            
            try
            {
                string outputFile = System.Environment.GetEnvironmentVariable("RS_TOOLSROOT");
                outputFile += "/script/map_pivot_distance_check.csv";
                LogFile logOutput = new LogFile(outputFile);
                Log.Log__Message(",Container,Object,Pivot,Bounding Box Centre,Distance");

                // Loop through each scene xml file and only operate on LOD objects
                foreach (Scene sc in scenes)
                {
                    Console.WriteLine("Scene XML: {0}", sc.Filename);
                    string containerName = "unknown";
                    foreach (ObjectDef obj in sc.Walk(Scene.WalkMode.DepthFirst))
                    {
                        if (obj.IsObject())
                        {
                            switch (obj.LODLevel)
                            {
                                case ObjectDef.LodLevel.LOD:
                                case ObjectDef.LodLevel.SLOD1:
                                case ObjectDef.LodLevel.SLOD2:
                                case ObjectDef.LodLevel.SLOD3:
                                    CheckObjectPivotDistanceFromCentre(obj, containerName);
                                    break;

                                default:
                                    continue;
                            }
                        }
                        // Get container name so it can be displayed later if object distance is
                        // over threshold
                        else if (obj.IsContainer())
                        {
                            containerName = obj.Name;
                        }
                    }
                }
                logOutput.Flush();
                logOutput.Close();

                Console.WriteLine("{0} Objects over threshold", objectsOverThreshold.Count);

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MapObjectsOverThreshold(objectsOverThreshold));
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unhandled exception: {0}", ex);
                RSG.Base.Forms.ExceptionStackTraceDlg stackDlg = new RSG.Base.Forms.ExceptionStackTraceDlg(ex, AUTHOR, EMAIL);
                stackDlg.ShowDialog();
                exit_code = EXIT_CRITIC;
            }
            return (exit_code);
        }
        #endregion Entry-Point

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// 
        static private void CheckObjectPivotDistanceFromCentre(ObjectDef obj, String contName)
        {
            Vector3f pivot = obj.ObjectTransform.Translation;
            BoundingBox3f boundingBox = new BoundingBox3f(obj.WorldBoundingBox.Min, obj.WorldBoundingBox.Max);

            float largestDimension = boundingBox.Max.X - boundingBox.Min.X;
            float dimY = boundingBox.Max.Y - boundingBox.Min.Y;
            float dimZ = boundingBox.Max.Z - boundingBox.Min.Z;
            
            // Find which dimension is largest of the 3
            if ( dimY > largestDimension)
            {
                largestDimension = dimY;
            }
            if ( dimZ > largestDimension )
            {
                largestDimension = dimZ;
            }

            // Find out the maximum distance offset from pivot to bb centre before we flag the object
            // as being above threshold
            double radius = largestDimension / 2.0;
            double maxOffset = radius * threshold;
            double distance = (boundingBox.Centre() - pivot).Magnitude();

            // If object pivot is further away than threshold amount, create struct for the info we 
            // want to display
            if (distance > maxOffset )
            {
                ObjectOutput newObjOutput = new ObjectOutput(contName, obj.Name, pivot, boundingBox.Centre(), distance);
                Log.Log__Message(",{0},{1},{2},{3},{4}", contName, obj.Name, (pivot.X + " " + pivot.Y + " " + pivot.Z), 
                    (boundingBox.Centre().X + " " + boundingBox.Centre().Y + " " + boundingBox.Centre().Z), distance);
                objectsOverThreshold.Add(newObjOutput);
            } 
        }
        #endregion Private Methods
    } //Program
} //Namespace MapPivotDistanceCheck
