﻿using System;
using System.Collections.Generic;
using RSG.Base.Configuration;
using RSG.Bounds;
using RSG.SceneXml;
using RSG.SceneXml.Material;

using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{
    internal class BoundTypeValidator : IObjectDefValidator
    {
        #region Controller Methods
        /// <summary>
        /// Validate that any collision object has a valid bounds type
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Validate(IBranch branch, TargetObjectDef obj, RSG.SceneXml.Scene scene, ValidatorOptions options)
        {
            bool result = true;

            if (obj.IsCollision())
            {
                bool parentIsDynamic = obj.Parent != null && obj.Parent.IsDynObject();

                CollisionObject collisonObject = new CollisionObject(obj);

                if ((int)collisonObject.CollisionType == 0 && !parentIsDynamic)
                {
                    this.AddError(
                        obj, 
                        "Collision object has no valid collision type flags " +
                        "(weapon|mover|river|deep_material_surface|foliage|horse|cover|vehicle|stair_slope) set.");
                }

                if (collisonObject.CollisionType.HasFlag(BNDFile.CollisionType.Foliage))
                {
                    if(collisonObject.ObjectType != CollisionObject.CollisionObjectType.Capsule &&
                       collisonObject.ObjectType != CollisionObject.CollisionObjectType.Sphere)
                    {
                        this.AddError(obj, "Collision object has foliage type flag but is neither a capsule nor a sphere");
                    }
                }
            }

            return result;
        }
        #endregion // Controller Methods
    }

} // MapExportValidation.Validator namespace

// IPLGroup.cs
