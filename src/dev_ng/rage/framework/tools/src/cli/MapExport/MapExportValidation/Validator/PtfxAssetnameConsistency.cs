﻿using System;
using System.Collections.Generic;
using SIO = System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.SceneXml;
using RSG.SceneXml.Material;
using MapExportValidation.Validation;
using RSG.SceneXml.MapExport;


namespace MapExportValidation.Validator
{

    /// <summary>
    /// Particle FX asset name consistency checker; ensures that all VFX on objects
    /// use the same hierarchy of PTFX assets.
    /// </summary>
    /// Note: this was previously limited to referencing a single PTFX asset.
    /// 
    internal class PtfxAssetnameConsistency : IObjectDefValidator
    {
        #region Constants
        private const String HREF_EFFECT_MULTIPLE_NAMES =
            "https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#Particle_Effects:_Multiple_PTFX_Asset_Sources";
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// PTFX asset relationships; child keys, parent values so we
        /// can traverse the relationships easily.
        /// </summary>
        private List<PtfxAsset> m_PTFXAssets;
        private bool m_PTFXAssetsLoaded;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public PtfxAssetnameConsistency()
        {
            this.m_PTFXAssets = new List<PtfxAsset>();
            this.m_PTFXAssetsLoaded = false;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Validate(IBranch branch, TargetObjectDef obj, RSG.SceneXml.Scene scene, ValidatorOptions options)
        {
            if (!obj.IsObject())
                return (true);
            if (obj.IsRefObject())
                return (true);

            // Load our relationship data.
            if (!m_PTFXAssetsLoaded)
                LoadPtfxHierarchy(branch);

            TargetObjectDef toCheck = obj;
            if (null != obj.SkeletonRoot)
                toCheck = obj.SkeletonRoot;

            List<String> effectTags = new List<String>();
            effectTags.AddRange(toCheck.GetPtfxEffectTags());

            List<String> assetNames = new List<String>();
            EffectCollection effectCollection = new EffectCollection(branch);
            foreach (String effectTag in effectTags)
            {
                String assetName = effectCollection.GetAssetNameForEffect(effectTag);
                if (!assetNames.Contains(assetName))
                    assetNames.Add(assetName);
            }

            // If we have more than one PTFX asset being pulled in by this object 
            // then we need to validate that they come from the same PTFX asset
            // child/parent hierarchy chain.  Otherwise we error.
            if ((assetNames.Count > 1) && (!ArePtfxAssetsRelated(assetNames)))
            {
                String msg = String.Format("Entity '{0}' has particle effects from multiple assets (Assets: {1}, Particles: {2}).  All effects must come from the same asset hierarchy.  See {3} for more information.",
                    obj.Name, assetNames.Count, effectTags.Count, HREF_EFFECT_MULTIPLE_NAMES);
                this.AddError(obj, msg);
                foreach (String assetName in assetNames)
                {
                    msg = String.Format("Archetype '{0}' uses PTFX Asset: {1}.",
                        obj.Name, assetName);
                    this.AddWarning(obj, msg);
                }
            }
            return true;
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Load our PTFX asset hierarchy information.
        /// </summary>
        /// <param name="branch"></param>
        private void LoadPtfxHierarchy(IBranch branch)
        {
            String filename = String.Empty;
            if (!branch.Project.IsDLC)
            {
                filename = SIO.Path.Combine(branch.Common, "data", "effects", "ptfxassetinfo.meta");
            }
            else
            {
                IBranch coreBranch = branch.Project.Config.CoreProject.Branches[branch.Name];
                filename = SIO.Path.Combine(coreBranch.Common, "data", "effects", "ptfxassetinfo.meta");
            }
            
            if (!SIO.File.Exists(filename))
            {
                Log.StaticLog.Warning("Failed to load PTFX Asset Dependency Info: {0} does not exist.", filename);
                this.m_PTFXAssetsLoaded = true;
                return;
            }
            
            try
            {
                XDocument xmlDoc = XDocument.Load(filename);
                foreach (XElement xmlPtfxDependencyInfo in xmlDoc.XPathSelectElements("/CPtFxAssetInfoMgr/ptfxAssetDependencyInfos/Item"))
                {
                    XElement xmlParentElem = xmlPtfxDependencyInfo.Element("parentName");
                    XElement xmlChildElem = xmlPtfxDependencyInfo.Element("childName");

                    if (null == xmlParentElem || null == xmlChildElem)
                    {
                        Log.StaticLog.Warning(xmlPtfxDependencyInfo,
                            "Incomplete parent/child set; parent: '{0}', child: '{1}'.",
                            xmlParentElem.Value, xmlChildElem.Value);
                        continue;
                    }

                    String assetName = xmlParentElem.Value;
                    String childName = xmlChildElem.Value;
                    PtfxAsset asset = this.m_PTFXAssets.Where(p => 0 == String.Compare(p.Asset, assetName, true)).FirstOrDefault();
                    if (null == asset)
                    {
                        asset = new PtfxAsset(assetName);
                        this.m_PTFXAssets.Add(asset);
                    }

                    if (!asset.Children.Contains(childName))
                        asset.Children.Add(childName);
                    else
                        Log.StaticLog.Warning("Duplicate PTFX asset relationship for {0}; child: {1}.",
                            assetName, childName);
                }
                this.m_PTFXAssetsLoaded = true;
            }
            catch (Exception ex)
            {
                Log.StaticLog.Exception(ex, "Failed to load PTFX Asset Dependency Info: {0} failed to parse.",
                    filename);
                this.m_PTFXAssetsLoaded = true;
            }
        }

        /// <summary>
        /// Return whether all PTFX Assets are related (or not).
        /// </summary>
        /// <param name="assetNames"></param>
        /// <returns></returns>
        private bool ArePtfxAssetsRelated(IEnumerable<String> assetNames)
        {
            String root = assetNames.First();
            for (int n = 1; n < assetNames.Count(); ++n)
            {
                if (!ArePtfxAssetsRelated(root, assetNames.ElementAt(n)))
                    return (false);
            }
            return (true);
        }

        /// <summary>
        /// Return whether two PTFX Assets are related (or not).
        /// </summary>
        /// <param name="asset1"></param>
        /// <param name="asset2"></param>
        /// <returns></returns>
        private bool ArePtfxAssetsRelated(String asset1, String asset2)
        {
            PtfxAsset a1 = this.m_PTFXAssets.Where(a => 0 == String.Compare(asset1, a.Asset, true)).FirstOrDefault();
            PtfxAsset a2 = this.m_PTFXAssets.Where(a => 0 == String.Compare(asset2, a.Asset, true)).FirstOrDefault();

            if ((null != a1) && (a1.Children.Contains(asset2)))
                return (true);
            if ((null != a2) && (a2.Children.Contains(asset1)))
                return (true);
            return (false);
        }
        #endregion // Private Methods
    }

    /// <summary>
    /// 
    /// </summary>
    class PtfxAsset
    {
        public String Asset { get; private set; }
        public ICollection<String> Children { get; private set; }
    
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="asset"></param>
        public PtfxAsset(String asset)
        {
            this.Asset = asset;
            this.Children = new List<String>();
        }
    }

} // MapExportValidation.Validator namespace
