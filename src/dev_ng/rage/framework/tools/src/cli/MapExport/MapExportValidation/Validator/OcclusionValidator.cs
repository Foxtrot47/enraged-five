﻿using System;
using System.Collections.Generic;
using System.Linq;

using RSG.Base.Configuration;
using RSG.Base.Math;
using RSG.Pipeline.Content;
using RSG.SceneXml;

using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{
    class OcclusionValidator : ISceneValidator
    {
        #region Controller Methods
        /// <summary>
        /// Check that occlusion data is set up correctly (or not at all)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Validate(IBranch branch, ContentTreeHelper treeHelper, Scene scene, ValidatorOptions options)
        {
            bool result = true;

            List<TargetObjectDef> occlusionObjectDefs = new List<TargetObjectDef>();
            List<TargetObjectDef> streamingExtentsObjectDefs = new List<TargetObjectDef>();
            foreach (TargetObjectDef objectDef in scene.Walk(Scene.WalkMode.DepthFirst))
            {
                if (objectDef.IsOcclusion())
                {
                    occlusionObjectDefs.Add(objectDef);
                }
                else if (objectDef.IsStreamingExtentsOverrideBox())
                {
                    streamingExtentsObjectDefs.Add(objectDef);
                }
            }

            if (occlusionObjectDefs.Count > 0)
            {
                if (streamingExtentsObjectDefs.Count == 0)
                {
                    String warningMessage = String.Format(
                        "Occlusion data was found for '{0}' but there is no RSStreamingExtentsOverrideBox.  " +
                        "Runtime need this to know at which range the occlusion data should be streamed from.  " +
                        "Please add one.",
                        System.IO.Path.GetFileNameWithoutExtension(scene.Filename));
                    this.AddWarning(occlusionObjectDefs[0], warningMessage);
                }
                else if (streamingExtentsObjectDefs.Count > 1)
                {
                    String errorMessage = String.Format(
                        "Occlusion data was found for '{0}' but there is more than one RSStreamingExtentsOverrideBox.  " +
                        "Runtime need exactly one of these to know at which range the occlusion data should be streamed from.  " +
                        "Please make sure there is exactly one RSStreamingExtentsOverrideBox.",
                        System.IO.Path.GetFileNameWithoutExtension(scene.Filename));
                    this.AddError(streamingExtentsObjectDefs[1], errorMessage);
                }
            }

            return result;
        }
        #endregion // Controller Methods
    }
}
