﻿using System;
using System.Collections.Generic;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Math;
using RSG.SceneXml;
using RSG.SceneXml.MapExport.Util;

using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{
    class InteriorValidator : IObjectDefValidator
    {
        #region IObjectDefValidator Implementation
        public override bool Validate(IBranch branch, TargetObjectDef objectDef, Scene scene, ValidatorOptions options)
        {
            bool result = true;

            if (objectDef.IsMilo())
            {
                if (!ValidateInteriorGroups(objectDef))
                    result = false;
            }

            if ((SceneType.EnvironmentContainer == scene.SceneType) &&
                (objectDef.IsMilo() || objectDef.IsMiloTri()))
            {
                if (!ValidateInteriorStreamingExtents(objectDef))
                    result = false;
            }

            return result;
        }
        #endregion IObjectDefValidator Implementation

        #region Private Methods
        private bool ValidateInteriorGroups(TargetObjectDef objectDef)
        {
            TargetObjectDef[] portalObjects = objectDef.Children.Where(obj => obj.IsMloPortal()).ToArray();
            TargetObjectDef[] objectsAttachedToAPortal = portalObjects.SelectMany(obj => GetPortalObjects(obj)).ToArray();

            TargetObjectDef[] roomObjects = objectDef.Children.Where(obj => obj.IsMloRoom()).ToArray();
            TargetObjectDef[] objectsAttachedToARoom = roomObjects.SelectMany(obj => obj.Children).ToArray();

            TargetObjectDef[] interiorGroupObjects = objectDef.Children.Where(obj => obj.IsInteriorGroup()).ToArray();

            foreach (TargetObjectDef interiorGroupObject in interiorGroupObjects)
            {
                TargetObjectDef[] objectsInThisInteriorGroup = GetInteriorGroupObjects(interiorGroupObject);
                foreach (TargetObjectDef objectInThisInteriorGroup in objectsInThisInteriorGroup)
                {
                    if (!objectsAttachedToAPortal.Contains(objectInThisInteriorGroup) && !objectsAttachedToARoom.Contains(objectInThisInteriorGroup))
                    {
                        String errorMessage = String.Format(
                            "Object '{0}' is in interior group '{1}' but is neither in a room nor attached to a portal",
                            objectInThisInteriorGroup.Name,
                            interiorGroupObject.Name);
                        this.AddError(objectInThisInteriorGroup, errorMessage);
                        return false;
                    }
                }
            }

            return true;
        }

        private bool ValidateInteriorStreamingExtents(TargetObjectDef objectDef)
        {
            BoundingBox3f entityExtents = GetInteriorEntityExtents(objectDef);
            if (entityExtents.IsEmpty)
            {
                String warningMessage = String.Format("Unable to determine entity extents of '{0}'", objectDef.Name);
                this.AddWarning(objectDef, warningMessage);
                return true;
            }

            BoundingBox3f streamingExtents = GetInteriorStreamingExtents(objectDef);
            if (streamingExtents.IsEmpty)
            {
                String warningMessage = String.Format("Unable to determine streaming extents of '{0}'", objectDef.Name);
                this.AddWarning(objectDef, warningMessage);
                return true;
            }

            if (!streamingExtents.Contains(entityExtents.Min) || !streamingExtents.Contains(entityExtents.Max))
            {
                String errorMessage = String.Format(
                            "The streaming extents for '{0}' don't encompass the physical extents.  " +
                            "You need to increase the LOD distance or use an RS Streaming Extents Override.  " +
                            "Entity exents [{1}, {2} ,{3} ], [{4}, {5}, {6}].  " +
                            "Streaming exents [{7}, {8} ,{9} ], [{10}, {11}, {12}].  " +
                            "See {13} for more information.",
                            objectDef.Name,
                            entityExtents.Min.X.ToString("0.000"), entityExtents.Min.Y.ToString("0.000"), entityExtents.Min.Z.ToString("0.000"),
                            entityExtents.Max.X.ToString("0.000"), entityExtents.Max.Y.ToString("0.000"), entityExtents.Max.Z.ToString("0.000"),
                            streamingExtents.Min.X.ToString("0.000"), streamingExtents.Min.Y.ToString("0.000"), streamingExtents.Min.Z.ToString("0.000"),
                            streamingExtents.Max.X.ToString("0.000"), streamingExtents.Max.Y.ToString("0.000"), streamingExtents.Max.Z.ToString("0.000"),
                            interiorStreamingExtentsUrl_);
                this.AddError(objectDef, errorMessage);
                return false;
            }
            else
            {
                String message = String.Format("The streaming extents for interior '{0}' are valid.", objectDef.Name);
                this.AddInfo(objectDef, message);
            }

            return true;
        }
        #endregion Private Methods

        #region Private Static Methods
        private static BoundingBox3f GetInteriorEntityExtents(TargetObjectDef objectDef)
        {
            BoundingBox3f entityExtents = new BoundingBox3f();

            if (objectDef.IsMiloTri())
            {
                if (objectDef.RefObject != null && objectDef.RefObject.LocalBoundingBox != null)
                {
                    Matrix34f localToWorldTransform = objectDef.NodeTransform.Transpose();// HACK - Transpose to work around data issue
                    entityExtents = localToWorldTransform * objectDef.RefObject.LocalBoundingBox;
                }
            }
            else if (objectDef.IsMilo())
            {
                GetEntityExtentsRecursive(objectDef, entityExtents);
            }

            return entityExtents;
        }

        private static void GetEntityExtentsRecursive(TargetObjectDef objectDef, BoundingBox3f boundingBox)
        {
            if (objectDef.WorldBoundingBox != null)
                boundingBox.Expand(objectDef.WorldBoundingBox);

            foreach (TargetObjectDef childObjectDef in objectDef.Children)
                GetEntityExtentsRecursive(childObjectDef, boundingBox);
        }

        private static BoundingBox3f GetInteriorStreamingExtents(TargetObjectDef objectDef)
        {
            BoundingBox3f streamingExtents = new BoundingBox3f();
            if (!GetInteriorStreamingExtentsOverride(objectDef, streamingExtents))
            {
                float lodDistance = GetInteriorLODDistance(objectDef);
                Vector3f streamingExtentsMin = new Vector3f(
                    objectDef.NodeTransform.D.X - lodDistance,
                    objectDef.NodeTransform.D.Y - lodDistance,
                    objectDef.NodeTransform.D.Z - lodDistance);
                Vector3f streamingExtentsMax = new Vector3f(
                    objectDef.NodeTransform.D.X + lodDistance,
                    objectDef.NodeTransform.D.Y + lodDistance,
                    objectDef.NodeTransform.D.Z + lodDistance);
                streamingExtents = new BoundingBox3f(streamingExtentsMin, streamingExtentsMax);
            }

            return streamingExtents;
        }

        private static bool GetInteriorStreamingExtentsOverride(TargetObjectDef objectDef, BoundingBox3f boundingBox)
        {
            bool result = false;

            TargetObjectDef streamingOverrideBoxObjectDef = objectDef.Children.FirstOrDefault(child => child.IsStreamingExtentsOverrideBox());
            if (streamingOverrideBoxObjectDef != null)
            {
                boundingBox.Expand(streamingOverrideBoxObjectDef.WorldBoundingBox);
                result = true;
            }

            return result;
        }

        private static float GetInteriorLODDistance(TargetObjectDef objectDef)
        {
            float lodDistance = 0;

            if (objectDef.IsMilo())
            {
                lodDistance = objectDef.GetAttribute(AttrNames.MILO_DETAIL, AttrDefaults.MILO_DETAIL);
            }
            else if (objectDef.IsMiloTri())
            {
                bool instanceLODDistance = objectDef.GetAttribute(AttrNames.OBJ_INSTANCE_LOD_DISTANCE, AttrDefaults.OBJ_INSTANCE_LOD_DISTANCE);
                if (instanceLODDistance)
                {
                    lodDistance = objectDef.GetAttribute(AttrNames.OBJ_LOD_DISTANCE, AttrDefaults.OBJ_LOD_DISTANCE);
                }
                else
                {
                    lodDistance = AttrDefaults.MILO_DETAIL;

                    if (objectDef.RefObject != null)
                    {
                        String miloObjectName = objectDef.RefObject.GetObjectName();
                        TargetObjectDef miloObjectDef = objectDef.RefObject.MyScene.FindObject(miloObjectName);
                        if (null != miloObjectDef)
                        {
                            lodDistance = miloObjectDef.GetAttribute(AttrNames.MILO_DETAIL, AttrDefaults.MILO_DETAIL);
                        }
                    }
                }
            }

            return lodDistance;
        }

        private static TargetObjectDef[] GetInteriorGroupObjects(TargetObjectDef interiorGroupObject)
        {
            Object[] groupObjectGuidObjects = interiorGroupObject.GetParameter(ParamNames.INTERIOR_GROUP_OBJECTS, ParamDefaults.INTERIOR_GROUP_OBJECTS);

            List<TargetObjectDef> interiorGroupChildObjects = new List<TargetObjectDef>();
            foreach (Guid groupObjectGuid in groupObjectGuidObjects.OfType<Guid>())
            {
                TargetObjectDef interiorGroupChildObject = interiorGroupObject.MyScene.FindObject(groupObjectGuid);
                if (interiorGroupChildObject != null)
                    interiorGroupChildObjects.Add(interiorGroupChildObject);
            }

            return interiorGroupChildObjects.ToArray();
        }

        private static TargetObjectDef[] GetPortalObjects(TargetObjectDef portalObject)
        {
            Object[] attachedGuidObjects = portalObject.GetParameter(ParamNames.MLOPORTAL_ATTACHED_OBJECTS, ParamDefaults.MLOPORTAL_ATTACHED_OBJECTS);

            List<TargetObjectDef> portalAttachedObjects = new List<TargetObjectDef>();
            foreach (Guid attachedGuid in attachedGuidObjects.OfType<Guid>())
            {
                TargetObjectDef attachedObject = portalObject.MyScene.FindObject(attachedGuid);
                if (attachedObject != null)
                    portalAttachedObjects.Add(attachedObject);
            }

            return portalAttachedObjects.ToArray();
        }
        #endregion Private Static Methods

        #region Private Static Data
        private static readonly String interiorStreamingExtentsUrl_ = 
            "https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#IMAP_Streaming_Extents:_Interiors";
        #endregion Private Static Data
    }
}
