﻿using System;
using System.Collections.Generic;
using RSG.Base.Configuration;
using RSG.SceneXml;
using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{

    /// <summary>
    /// 
    /// </summary>
    internal class AOMultipliers : IObjectDefValidator
    {
        #region Controller Methods
        /// <summary>
        /// Validate a single Object for IPL Group.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Validate(IBranch branch, TargetObjectDef obj, RSG.SceneXml.Scene scene, ValidatorOptions options)
        {
            if (!obj.IsObject())
                return (true);

            if (obj.DontExportIPL())
                return (true);

            int aoMult = obj.GetAttribute(AttrNames.OBJ_AMBIENT_OCCLUSION_MULTIPLIER, 
                AttrDefaults.OBJ_AMBIENT_OCCLUSION_MULTIPLIER);
            int aaoMult = obj.GetAttribute(AttrNames.OBJ_ARTIFICIAL_AMBIENT_OCCLUSION_MULTIPLIER,
                AttrDefaults.OBJ_ARTIFICIAL_AMBIENT_OCCLUSION_MULTIPLIER);

            bool result = true;
            if (!(aoMult >= 0 && aoMult <= 255))
            {
                this.AddError(obj, String.Format("Object {0} has an out of range Ambient Occlusion Multiplier; {1}.  Needs to be within 0..255.",
                    obj.Name, aoMult));
                result = false;
            }
            if (!(aaoMult >= 0 && aaoMult <= 255))
            {
                this.AddError(obj, String.Format("Object {0} has an out of range Artificial Ambient Occlusion Multiplier; {1}.  Needs to be within 0..255.",
                    obj.Name, aaoMult));
                result = false;
            }

            return (result);
        }
        #endregion // Controller Methods
    }

} // MapExportValidation.Validator namespace
