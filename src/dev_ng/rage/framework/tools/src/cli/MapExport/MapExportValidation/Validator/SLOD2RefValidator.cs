﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Math;
using RSG.Base.Logging;
using RSG.Base.Configuration;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;
using RSG.SceneXml;
using RSG.SceneXml.MapExport;
using RSG.SceneXml.MapExport.Framework;
using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{
    /// <summary>
    /// Validates the presence (of required) of SLOD2 proxy objects
    /// </summary>
    internal class SLOD2RefValidator : ISceneValidator
    {
        #region Controller Methods

        public override bool Validate(IBranch branch, ContentTreeHelper treeHelper, Scene scene, ValidatorOptions options)
        {
            // Find the map node for this scene
            string contentName = Path.GetFileNameWithoutExtension(scene.Filename);
            IContentNode sceneNode = GetMapMaxNode(treeHelper, contentName);

            if (null == sceneNode)
                return true;
            
            TargetObjectDef[] slod2RefObjects = GetConnectedContainerLODRefObjects(scene);
            SortedSet<string> lodContainersReferenced = new SortedSet<string>();
            foreach (TargetObjectDef slod2RefObject in slod2RefObjects.Where(ro => !String.IsNullOrEmpty(ro.RefFile)))
            {
                string refFilename = Path.GetFileNameWithoutExtension(slod2RefObject.RefFile).ToLower();
                lodContainersReferenced.Add(refFilename);
            }

            if (lodContainersReferenced.Count >= 2)
            {
                StringBuilder errorMessageBuilder = new StringBuilder();
                errorMessageBuilder.AppendFormat("The container '{0}' references SLOD2 nodes in more then one other container.  ", contentName);
                errorMessageBuilder.Append("This is not supported.  The following containers were referenced: ");
                errorMessageBuilder.Append(String.Join(", ", lodContainersReferenced));
                AddError(scene.Objects[0], errorMessageBuilder.ToString());
                return false;
            }

            // We only validate content marked as requiring an SLOD2 link
            bool hasSLOD2Link = false;
            if (sceneNode.Parameters.ContainsKey(RSG.Pipeline.Core.Constants.ParamMap_SLOD2Link))
                hasSLOD2Link = (bool)sceneNode.Parameters[RSG.Pipeline.Core.Constants.ParamMap_SLOD2Link];

            if (!hasSLOD2Link)
                return true;

            if (slod2RefObjects.Length == 0 && scene.Objects.Length > 0)
            {
                string message = string.Format(
                    "At least one SLOD1 object in this container must have a LOD parent that is a ref to an SLOD2 object. For more information see {0}", 
                    missingContainerLODRefURL_);
                AddError(scene.Objects[0], message);
            }

            return (slod2RefObjects.Length > 0);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Return 3dsmax map Content-Node for a given name.
        /// </summary>
        /// <param name="treeHelper"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        private static IContentNode GetMapMaxNode(ContentTreeHelper treeHelper, String name)
        {
            IEnumerable<IProcess> exportProcesses = treeHelper.GetAllMapExportProcesses();
            IEnumerable<IContentNode> exportProcessInputs = exportProcesses.SelectMany(p => p.Inputs);
            IContentNode mapExportNode = exportProcessInputs.Where(n =>
                0 == String.Compare(name, n.Name, true)).FirstOrDefault();

            return (mapExportNode);
        }

        /// <summary>
        /// Return processed map zip node.
        /// </summary>
        /// <param name="treeHelper"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        private static IContentNode GetMapProcessedNode(ContentTreeHelper treeHelper, IContentNode mapExportNode)
        {
            IContentNode exportZipNode = treeHelper.GetExportZipNodeFromMaxFileNode(mapExportNode);
            IContentNode processedZipNode = treeHelper.GetProcessedZipNodeFromExportZipNode(exportZipNode);

            return (processedZipNode);
        }

        private static TargetObjectDef[] GetConnectedContainerLODRefObjects(Scene scene)
        {
            List<TargetObjectDef> slod2RefObjects = new List<TargetObjectDef>();

            foreach (TargetObjectDef objectDef in scene.Objects)
            {
                GetConnectedContainerLODRefObjectsRecursive(objectDef, slod2RefObjects);
            }

            return slod2RefObjects.ToArray();
        }

        private static void GetConnectedContainerLODRefObjectsRecursive(TargetObjectDef objectDef, ICollection<TargetObjectDef> slod2RefObjects)
        {
            if (objectDef.IsContainerLODRefObject() &&
                objectDef.LOD != null &&
                objectDef.LOD.Children.Length > 0)
            {
                slod2RefObjects.Add(objectDef);
            }

            foreach (TargetObjectDef childObjectDef in objectDef.Children)
            {
                GetConnectedContainerLODRefObjectsRecursive(childObjectDef, slod2RefObjects);
            }
        }
        #endregion // Private Methods

        private static readonly String missingContainerLODRefURL_ = 
            "https://devstar.rockstargames.com/wiki/index.php/Map_Export_Errors#Missing_SLOD2_Ref";
    }

} // MapExportValidation.Validator namespace
