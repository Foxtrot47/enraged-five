﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RSG.Base.Configuration;
using RSG.SceneXml;
using RSG.SceneXml.Material;
using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{
    /// <summary>
    /// Streaming extents override validator.
    /// </summary>
    /// This validator ensures that any streaming extents override objects are valid
    ///
    class StreamingExtentsOverrideBoxValidator : IObjectDefValidator
    {
        #region IObjectDefValidator Methods

        /// <summary>
        /// Validate a single Object for its suitability as a streaming extents override.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Validate(IBranch branch, TargetObjectDef objectDef, Scene scene, ValidatorOptions options)
        {
            if (objectDef.IsMiloTri())
            {
                int numStreamingExtentsOverrideBoxes = objectDef.Children.Where(child => child.IsStreamingExtentsOverrideBox()).Count();

                if (numStreamingExtentsOverrideBoxes > 1)
                {
                    string error = String.Format("Interior '{0}' has {1} Streaming Extents Override Boxes.  There should be none or exactly one.",
                        objectDef.Name, numStreamingExtentsOverrideBoxes);
                    AddError(objectDef, error);
                }
            }

            return true;
        }
        #endregion // IObjectDefValidator Methods
    }

} // MapExportValidation.Validator namespace
