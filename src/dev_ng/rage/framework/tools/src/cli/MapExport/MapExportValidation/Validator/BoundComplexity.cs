﻿using System;
using System.Collections.Generic;
using RSG.Base.Configuration;
using RSG.SceneXml;
using RSG.SceneXml.Material;
using MapExportValidation.Validation;

namespace MapExportValidation.Validator
{

    /// <summary>
    /// IPL Group validator for "Gta Object" attribute class objects.
    /// </summary>
    /// If objects have an IPL Group set then they must not be within a LOD
    /// hierarchy (i.e. no LOD parent).  This is a fatal error.
    /// 
    internal class BoundComplexity : IObjectDefValidator
    {
        #region Controller Methods

        private void CheckChildForBoundComplexity(TargetObjectDef root, TargetObjectDef obj)
        {
            if (obj.IsCollision())
            {
                if (obj.PolyCount > 64)
                    this.AddWarning(obj, String.Format("Collision Object {0} of dynamic object {1} has more then 64 triangles: {2}. Please check that this is really required.",
                           obj.Name, root.Name, obj.PolyCount));
            }
            foreach (TargetObjectDef child in obj.Children)
            {
                CheckChildForBoundComplexity(root, child);
            }
        }

        /// <summary>
        /// Validate a single Object for IPL Group.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Validate(IBranch branch, TargetObjectDef obj, RSG.SceneXml.Scene scene, ValidatorOptions options)
        {
            if (obj.IsDynObject())
            {
                CheckChildForBoundComplexity(obj, obj);
            }
                
            return true;
        }
        #endregion // Controller Methods
    }

} // MapExportValidation.Validator namespace

// IPLGroup.cs
