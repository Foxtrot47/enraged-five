﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.ZipExtract
{

    /// <summary>
    /// Zip extract executable.
    /// </summary>
    sealed class Program
    {
        #region Constants
        private static readonly String OPTION_OUTPUTDIR = "outputdir";
        private static readonly String OPTION_TASK_NAME = "taskname";
        private static readonly String OPTION_OVERWRITE = "forceoverwrite";
        private static readonly String OPTION_EXTRACTALL = "extractall";
        private static readonly String OPTION_USE_PARENT_TIMESTAMP = "useparenttimestamp";
        #endregion // Constants

        /// <summary>
        /// Entry-point.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        [STAThread]
        static int Main(String[] args)
        {
            // Define our executable options.  Stick with the long options
            // (e.g. --opt) rather than the single character versions.
            LongOption[] opts = new LongOption[] {
                new LongOption(OPTION_OUTPUTDIR, LongOption.ArgType.Required,
                    "Output directory."),
                new LongOption(OPTION_TASK_NAME, LongOption.ArgType.Required,
                    "Task name (for XGE error parsing)."),
                new LongOption(OPTION_OVERWRITE, LongOption.ArgType.None,
                    "Overwrite destination files."),
                new LongOption(OPTION_EXTRACTALL, LongOption.ArgType.None,
                    "Extract all files."),
                new LongOption(OPTION_USE_PARENT_TIMESTAMP, LongOption.ArgType.None,
                    "Use parent timestamp."),
            };
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.CreateUniversalLog("ZipExtract");
            
            CommandOptions options = new CommandOptions(args, opts);
            if (options.ShowHelp)
            {
                PrintUsage(log, options);
                return (Constants.Exit_Failure);
            }

            // Verify options.
            if (!options.TrailingArguments.Any())
            {
                log.Error("No input files specified.");
                PrintUsage(log, options);
                return (Constants.Exit_Failure);
            }
            if (!options.ContainsOption(OPTION_OUTPUTDIR))
            {
                log.Error("No output directory specified.");
                PrintUsage(log, options);
                return (Constants.Exit_Failure);
            }
            
            // Process trailing arguments into absolute paths.
            ICollection<String> filenames = new List<String>();
            foreach (String arg in options.TrailingArguments)
            {
                filenames.Add(Path.GetFullPath(arg));
            }

            // Turn args into vars
            String outputDirectory = (String)options[OPTION_OUTPUTDIR];
            bool overwrite = options.ContainsOption(OPTION_OVERWRITE);
            bool extractAll = options.ContainsOption(OPTION_EXTRACTALL);
            bool useParentTimestamp = options.ContainsOption(OPTION_USE_PARENT_TIMESTAMP);

            String taskName = options.ContainsOption(OPTION_TASK_NAME) ? (String)options[OPTION_TASK_NAME] : String.Empty;
            if (!String.IsNullOrEmpty(taskName))
            {
                Console.WriteLine("TASK: {0}", taskName);
            }
            
            if (!Directory.Exists(outputDirectory))
                Directory.CreateDirectory(outputDirectory);

            int exit_code = Constants.Exit_Success;
            foreach (String filename in filenames)
            {
                if (!File.Exists(filename))
                {
                    Console.WriteLine("Error: Input zip file {0} does not exist.", filename);
                    exit_code = Constants.Exit_Failure;
                    continue;
                }

                if (extractAll)
                {
                    RSG.Pipeline.Services.Zip.ExtractAll(filename, outputDirectory, overwrite, useParentTimestamp);
                    MakeDirectoryAndContentsWritable(outputDirectory);
                }
                else
                {
                    RSG.Pipeline.Services.Zip.ExtractNewer(filename, outputDirectory, useParentTimestamp);
                }
            }
            return (exit_code);
        }

        #region Private Methods
        /// <summary>
        /// Print usage information to log.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="options"></param>
        private static void PrintUsage(IUniversalLog log, CommandOptions options)
        {
            log.Message(options.GetUsage("files", "Zip files to extract."));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="directory"></param>
        private static void MakeDirectoryAndContentsWritable(string directory)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(directory);
            foreach (FileSystemInfo fileSystemInfo in dirInfo.GetFileSystemInfos())
            {
                if (fileSystemInfo.Attributes.HasFlag(FileAttributes.ReadOnly))
                    fileSystemInfo.Attributes = fileSystemInfo.Attributes & (~FileAttributes.ReadOnly);
            }

            foreach (DirectoryInfo subDirectoryInfo in dirInfo.GetDirectories())
                MakeDirectoryAndContentsWritable(subDirectoryInfo.FullName);
        }
        #endregion // Private Methods
    }
} // RSG.Pipeline.ZipExtract namespace
