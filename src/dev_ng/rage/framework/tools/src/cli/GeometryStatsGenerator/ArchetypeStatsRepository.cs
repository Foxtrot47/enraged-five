﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using RSG.Base.Logging;

namespace GeometryStatsGenerator
{
    internal class ArchetypeStatsRepository
    {
        internal ArchetypeStatsRepository()
        {
            sectionMap_ = new SortedDictionary<string, ArchetypeStatsSection>();
        }

        internal void Add(string sectionName, string archetypeName, XDocument statsDocument)
        {
            if (!sectionMap_.ContainsKey(sectionName))
            {
                sectionMap_.Add(sectionName, new ArchetypeStatsSection(sectionName));
            }

            sectionMap_[sectionName].Add(archetypeName, statsDocument);
        }

        /// <summary>
        /// Saves a pargen compliant document of archetype stats
        /// </summary>
        internal void Save(string outputPathname)
        {
            XDocument document = new XDocument(this.ToXElement());
            document.Save(outputPathname);
            Log.Log__Message("Saved archetype stats to '{0}'", outputPathname);
        }

        internal XElement ToXElement()
        {
            return
                new XElement("CGeometryStats",
                    new XElement("Sections",
                        sectionMap_.Select(kvp => kvp.Value.ToXElement())));
        }

        private SortedDictionary<string, ArchetypeStatsSection> sectionMap_;
    }
}
