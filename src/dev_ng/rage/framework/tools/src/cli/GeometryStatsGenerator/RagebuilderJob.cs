﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

using RSG.Base.ConfigParser;

namespace GeometryStatsGenerator
{
    /// <summary>
    /// Represents a collection of GeometryZip files to be processed via Ragebuilder as a single job
    /// </summary>
    internal class RagebuilderJob
    {
        internal RagebuilderJob(IEnumerable<GeometryZip> geometryZips)
        {
            geometryZips_ = geometryZips.ToArray();
        }

        internal void WriteScript(ConfigGameView configGameView, string pathname)
        {
            string buildDirectory = configGameView.BuildDir;
            string shaderDirectory = buildDirectory + "/common/shaders";
            string shaderDbDirectory = shaderDirectory + "/db";
            string platform = "win32";

            using (StreamWriter writer = new StreamWriter(File.Create(pathname)))
            {
                writer.WriteLine("require(\"{0}/util/ragebuilder/generate_stats.rbs\")", configGameView.ToolsLibDir.Replace("\\", "/"));
                foreach (GeometryZip geometryZip in geometryZips_)
                {
                    if (geometryZip.Type == GeometryZip.GeometryType.Drawable)
                    {
                        writer.WriteLine("generate_drawable_stats(\"{0}\", \"{1}\", \"{2}\", \"{3}\", \"{4}\", \"{5}\")",
                            geometryZip.Pathname.Replace("\\", "/"),
                            geometryZip.StatsPathname.Replace("\\", "/"),
                            shaderDirectory.Replace("\\", "/"),
                            shaderDbDirectory.Replace("\\", "/"),
                            platform.Replace("\\", "/"),
                            buildDirectory.Replace("\\", "/"));
                    }
                    else if (geometryZip.Type == GeometryZip.GeometryType.Fragment)
                    {
                        writer.WriteLine("generate_fragment_stats(\"{0}\", \"{1}\", \"{2}\", \"{3}\", \"{4}\", \"{5}\")",
                            geometryZip.Pathname.Replace("\\", "/"),
                            geometryZip.StatsPathname.Replace("\\", "/"),
                            shaderDirectory.Replace("\\", "/"),
                            shaderDbDirectory.Replace("\\", "/"),
                            platform.Replace("\\", "/"),
                            buildDirectory.Replace("\\", "/"));
                    }
                }
            }
        }

        private GeometryZip[] geometryZips_;
    }
}
