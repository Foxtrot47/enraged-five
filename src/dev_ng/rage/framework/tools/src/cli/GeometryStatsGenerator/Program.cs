﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using RSG.Base.ConfigParser;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using XGE = RSG.Interop.Incredibuild.XGE;

namespace GeometryStatsGenerator
{
    class Program
    {
        static int Main(string[] args)
        {
            LongOption[] options = new LongOption[]
            {
                new LongOption(outputPathnameOptionName_, LongOption.ArgType.Required, 'o'),
                new LongOption(levelNameOptionName_, LongOption.ArgType.Required, 'l')
            };
            Getopt getopt = new Getopt(args, options);

            string outputPathname = getopt.HasOption(outputPathnameOptionName_) ? ((string)getopt.Options[outputPathnameOptionName_]) : String.Empty;
            string levelName = getopt.HasOption(levelNameOptionName_) ? ((string)getopt.Options[levelNameOptionName_]) : String.Empty;

            UniversalLogFile universalLogFile = InitialiseLog();

            try
            {
                if (String.IsNullOrEmpty(outputPathname))
                {
                    Log.Log__Error("required argument '{0}' specifying the output pathname wasn't found.", outputPathnameOptionName_);
                    return exitCodeError_;
                }

                if (String.IsNullOrEmpty(levelName))
                {
                    Log.Log__Error("required argument '{0}' specifying the name of the level to generate stats for wasn't found.", levelNameOptionName_);
                    return exitCodeError_;
                }

                Log.Log__Message("Loading content tree...");
                using (ConfigGameView configGameView = new ConfigGameView())
                {

                    List<GeometryContainerZip> geometryContainerZips = LoadGeometryContainerZips(configGameView, levelName);
                    List<GeometryZip> geometryZips = ExtractGeometryZips(configGameView, geometryContainerZips);
                    List<RagebuilderJob> ragebuilderJobs = BuildRagebuilderJobs(configGameView, geometryZips);
                    ProcessRagebuilderJobs(configGameView, ragebuilderJobs);
                    ArchetypeStatsRepository archetypeStatsRepository = ProcessStats(geometryContainerZips);
                    archetypeStatsRepository.Save(outputPathname);
                }
            }
            catch (System.Exception ex)
            {
                Log.Log__Error("An unexpected error occurred.  Exception details follow.");
                Log.Log__Error(ex.ToString());

                return exitCodeError_;
            }

            universalLogFile.Flush();// This shouldn't be necessary - JWR

            return exitCodeSucess_;
        }

        /// <summary>
        /// Initialises logging for this application
        /// </summary
        private static UniversalLogFile InitialiseLog()
        {
            UniversalLogFile universalLogFile = (UniversalLogFile)LogFactory.CreateUniversalLogFile("GeometryStatsGenerator", Log.StaticLog);
            ConsoleLog consoleLog = new ConsoleLog();

            return universalLogFile;
        }

        /// <summary>
        /// Parses the content tree and creates a GeometryContainerZip instance for each valid mapzip node
        /// </summary>
        private static List<GeometryContainerZip> LoadGeometryContainerZips(ConfigGameView configGameView, string levelName)
        {
            List<GeometryContainerZip> geometryContainerZips = new List<GeometryContainerZip>();

            string levelPathFilter1 = (@"levels\" + levelName).ToLower();
            string levelPathFilter2 = (@"levels/" + levelName).ToLower();
            ContentNodeMapZip[] mapZipNodes = configGameView.Content.Root.FindAll("mapzip").OfType<ContentNodeMapZip>().Where(
                node => node.Path.ToLower().Contains(levelPathFilter1) || node.Path.ToLower().Contains(levelPathFilter2)).ToArray();
            Log.Log__Message("Loaded {0} mapzip nodes", mapZipNodes.Length);

            foreach (ContentNodeMapZip mapZipNode in mapZipNodes)
            {
                try
                {
                    GeometryContainerZip geometryContainerZip = new GeometryContainerZip(mapZipNode);
                    geometryContainerZips.Add(geometryContainerZip);
                }
                catch (Exception ex)
                {
                    Log.Log__Warning("Unable to construct GeometryContainerZip from MapZipNode '{0}'.  Exception follows.", mapZipNode.Name);
                    Log.Log__Warning(ex.ToString());
                }
            }

            return geometryContainerZips;
        }

        /// <summary>
        /// Extracts the geometry data contained in each GeometryContainerZip
        /// </summary>
        private static List<GeometryZip> ExtractGeometryZips(ConfigGameView configGameView, IEnumerable<GeometryContainerZip> geometryContainerZips)
        {
            Log.Log__Message("Extracting geometry zips...");

            List<GeometryZip> geometryZips = new List<GeometryZip>();

            string baseTempDirectory = configGameView.ToolsTempDir + @"\geometry_stats";
            Directory.CreateDirectory(baseTempDirectory);
            int geometryContainerZipIndex = 0;
            foreach (GeometryContainerZip geometryContainerZip in geometryContainerZips)
            {
                Log.Log__Message("Extracting geometry container zip [{0} of {1}] from '{2}'",
                    ++geometryContainerZipIndex, geometryContainerZips.Count(), geometryContainerZip.MapZipPathname);
                geometryContainerZip.Extract(baseTempDirectory);
                geometryZips.AddRange(geometryContainerZip.GeometryZips);
            }

            return geometryZips;
        }

        /// <summary>
        /// Builds a RagebuilderJob instance for a set of GeometryZip objects based on their size on disk
        /// </summary>
        private static List<RagebuilderJob> BuildRagebuilderJobs(ConfigGameView configGameView, IEnumerable<GeometryZip> geometryZips)
        {
            Log.Log__Message("Building ragebuilder jobs...");

            List<RagebuilderJob> ragebuilderJobs = new List<RagebuilderJob>();

            List<GeometryZip> geometryZipsForNextScript = new List<GeometryZip>();
            foreach (GeometryZip geometryZip in geometryZips)
            {
                geometryZipsForNextScript.Add(geometryZip);
                if (geometryZipsForNextScript.Sum(zip => zip.FileSize) > MaxGeometryDataSizePerRagebuilderScript_)
                {
                    ragebuilderJobs.Add(new RagebuilderJob(geometryZipsForNextScript));
                    geometryZipsForNextScript.Clear();
                }
            }

            if (geometryZipsForNextScript.Count > 0)
                ragebuilderJobs.Add(new RagebuilderJob(geometryZipsForNextScript));

            return ragebuilderJobs;
        }

        /// <summary>
        /// Pushes a collection of RagebuilderJob instances through XGE, generating unprocessed stats data 
        /// </summary>
        private static bool ProcessRagebuilderJobs(ConfigGameView configGameView, IEnumerable<RagebuilderJob> ragebuilderJobs)
        {
            Log.Log__Message("Processing ragebuilder jobs...");

            string xgeBaseDirectory = configGameView.ToolsTempDir + @"\xge\gta5\dev\geometry_stats";
            Directory.CreateDirectory(xgeBaseDirectory);
            int scriptIndex = 0;
            List<string> ragebuilderScriptPathnames = new List<string>();
            foreach (RagebuilderJob ragebuilderJob in ragebuilderJobs)
            {
                string scriptPathname = Path.Combine(xgeBaseDirectory, String.Format("packet{0}.rbs", scriptIndex++));
                ragebuilderJob.WriteScript(configGameView, scriptPathname);
                ragebuilderScriptPathnames.Add(scriptPathname);
            }

            string ragebuilderProcessPathname = Path.Combine(configGameView.ToolsBinDir, "ragebuilder_0378.exe");
            XDocument xgeConfigDocument = new XDocument(
                new XElement("BuildSet", new XAttribute("FormatVersion", 1),
                    new XElement("Environments",
                        new XElement("Environment", new XAttribute("Name", "Environment"),
                            new XElement("Tools",
                                new XElement("Tool", new XAttribute("OutputPrefix", "Generating geometry stats..."), 
                                                     new XAttribute("Name", "Ragebuilder"), 
                                                     new XAttribute("OutputFileMasks", "*.*"),
                                                     new XAttribute("AllowRemote", "false"),
                                                     new XAttribute("TimeLimit", "3600"),
                                                     new XAttribute("Path", ragebuilderProcessPathname),
                                                     new XAttribute("AllowRestartOnLocal", "false"),
                                                     new XAttribute("Params", ""),
                                                     new XAttribute("GroupPrefix", "Serialising remotely..."))),
                            new XElement("Variables",
                                new XElement("Variable", new XAttribute("Name", "BinVar"), 
                                                         new XAttribute("Value", configGameView.ToolsBinDir))))),
                    new XElement("Project", new XAttribute("Name", "Geometry Stats Generation"), 
                                            new XAttribute("Env", "Environment"),
                        new XElement("TaskGroup", new XAttribute("Name", "Generating geometry stats..."),
                                                  new XAttribute("Env", "Environment"),
                                                  new XAttribute("Tool", "Ragebuilder"),
                                                  new XAttribute("WorkingDir", "$(BinVar)"),
                            ragebuilderScriptPathnames.Select(scriptPathname => new XElement("Task", new XAttribute("SourceFile", ""),
                                                                                                     new XAttribute("Params", String.Format("{0} {1}", scriptPathname, "-nopopups")),
                                                                                                     new XAttribute("Caption", Path.GetFileNameWithoutExtension(scriptPathname))))))));

            string xgeConfigDocumentPathname = Path.Combine(xgeBaseDirectory, "geometry_stats.xml");
            xgeConfigDocument.Save(xgeConfigDocumentPathname);
            string xgeLogPathname = Path.Combine(xgeBaseDirectory, "geometry_stats.log");
            bool result = XGE.XGE.Start("Geometry Stats Generation", xgeConfigDocumentPathname, xgeLogPathname);

            return result;
        }

        /// <summary>
        /// Processes the stats data associated with a collection of GeometryContainerZip instance, yielding an ArchetypeStatsRepository instance
        /// </summary>
        private static ArchetypeStatsRepository ProcessStats(IEnumerable<GeometryContainerZip> geometryContainerZips)
        {
            Log.Log__Message("Processing stats...");

            ArchetypeStatsRepository archetypeStatsRepository = new ArchetypeStatsRepository();
            foreach (GeometryContainerZip geometryContainerZip in geometryContainerZips)
            {
                foreach (GeometryZip geometryZip in geometryContainerZip.GeometryZips)
                {
                    string archetypeName = geometryZip.ArchetypeName;
                    try
                    {
                        XDocument statsDocument = XDocument.Load(geometryZip.StatsPathname);
                        archetypeStatsRepository.Add(geometryContainerZip.Name, archetypeName, statsDocument);
                    }
                    catch (Exception ex)
                    {
                        Log.Log__Warning("Failed to parse geometry stats at '{0}'.  Exception follows", geometryZip.StatsPathname);
                        Log.Log__Warning(ex.ToString());
                    }
                }
            }

            return archetypeStatsRepository;
        }

        private static readonly string outputPathnameOptionName_ = "output";
        private static readonly string levelNameOptionName_ = "level";
        private static readonly int exitCodeSucess_ = 0;
        private static readonly int exitCodeError_ = 1;
        private static readonly long MaxGeometryDataSizePerRagebuilderScript_ = 256 * 1024 * 1024;
    }
}
