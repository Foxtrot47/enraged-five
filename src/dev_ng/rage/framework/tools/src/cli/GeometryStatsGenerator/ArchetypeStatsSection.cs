﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using RSG.Base.Logging;

namespace GeometryStatsGenerator
{
    internal class ArchetypeStatsSection
    {
        internal ArchetypeStatsSection(string name)
        {
            Name = name;
            archetypeMap_ = new SortedDictionary<string, ArchetypeStats>();
        }

        internal string Name { get; private set; }

        internal void Add(string archetypeName, XDocument statsDocument)
        {
            try
            {
                archetypeMap_.Add(archetypeName, new ArchetypeStats(archetypeName, statsDocument));
            }
            catch (Exception ex)
            {
                Log.Log__Warning("Error adding archetype stats for '{0}' to section '{1}'", archetypeName, Name);
                Log.Log__Warning(ex.ToString());
            }
        }

        internal XElement ToXElement()
        {
            return
                new XElement("Item",
                    new XElement("Name", Name),
                    new XElement("Archetypes",
                        archetypeMap_.Select(kvp => kvp.Value.ToXElement())));
        }

        private SortedDictionary<string, ArchetypeStats> archetypeMap_;
    }
}
