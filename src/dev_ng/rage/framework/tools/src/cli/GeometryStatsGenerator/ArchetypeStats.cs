﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

using RSG.Base.Logging;
using RSG.Base.Math;

namespace GeometryStatsGenerator
{
    /// <summary>
    /// Responsible for processing stats generated via Ragebuilder into a heuristic
    /// </summary>
    internal class ArchetypeStats
    {
        internal ArchetypeStats(string archetypeName, XDocument statsDocument)
        {
            Name = archetypeName;
            if (statsDocument.Root.Name == "Drawable")
            {
                Heuristic = CalculateHeuristicFromDrawableElement(statsDocument.Root);
            }
            else
            {
                Log.Log__Warning("Status document for archetype '{0}' has unrecognised root element name '{1}'.  Heuristic will be 0.", Name, statsDocument.Root.Name);
            }
        }

        internal string Name { get; private set; }
        internal double Heuristic { get; private set; }

        internal XElement ToXElement()
        {
            return
                new XElement("Item",
                    new XElement("Name", Name),
                    new XElement("Heuristic", new XAttribute("value", Heuristic.ToString("0.000"))));
                    
        }

        private static BoundingBox3f GetAABBFromGeometryElement(XElement geometryElement)
        {
            XElement aabbMinElement = geometryElement.Element("AABBMin");
            XElement aabbMaxElement = geometryElement.Element("AABBMax");
            float aabbMinX = Single.Parse(aabbMinElement.Attribute("x").Value);
            float aabbMinY = Single.Parse(aabbMinElement.Attribute("y").Value);
            float aabbMinZ = Single.Parse(aabbMinElement.Attribute("z").Value);
            float aabbMaxX = Single.Parse(aabbMaxElement.Attribute("x").Value);
            float aabbMaxY = Single.Parse(aabbMaxElement.Attribute("y").Value);
            float aabbMaxZ = Single.Parse(aabbMaxElement.Attribute("z").Value);
            BoundingBox3f geometryAABB = new BoundingBox3f(
                new Vector3f(aabbMinX, aabbMinY, aabbMinZ),
                new Vector3f(aabbMaxX, aabbMaxY, aabbMaxZ));

            return geometryAABB;
        }

        private static double GetVolumeOfAABBWithMinDimension(BoundingBox3f aabb)
        {
            double dx = aabb.Max.X - aabb.Min.X;
            double dy = aabb.Max.Y - aabb.Min.Y;
            double dz = aabb.Max.Z - aabb.Min.Z;
            double maxDimension = Math.Max(dx, Math.Max(dy, dz));
            double minDimension = maxDimension * minDimensionRatio_;
            if (dx < minDimension)
                dx = minDimension;
            if (dy < minDimension)
                dy = minDimension;
            if (dz < minDimension)
                dz = minDimension;
            double volume = dx * dy * dz;

            return volume;
        }

        private double CalculateHeuristicFromDrawableElement(XElement drawableElement)
        {
            try
            {
                double totalGeometryCost = 0;
                BoundingBox3f drawableAABB = new BoundingBox3f();
                double sumOfGeometryVolumes = 0;

                foreach (XElement lodElement in drawableElement.Elements("Lod"))
                {
                    int lodIndex = Int32.Parse(lodElement.Element("Index").Value);
                    if (lodIndex != 0)
                        continue;

                    foreach (XElement modelElement in lodElement.Elements("Model"))
                    {
                        foreach (XElement geometryElement in modelElement.Elements("Geometry"))
                        {
                            double numVerts = Double.Parse(geometryElement.Element("NumVerts").Value);
                            double vertStride = Double.Parse(geometryElement.Element("VertStride").Value);
                            double costOfVerts = numVerts * vertStride;
                            string shaderName = geometryElement.Element("ShaderName").Value;
                            double shaderCostFactor = ShaderCostCalculator.Instance.GetShaderCostFactor(shaderName);
                            BoundingBox3f geometryAABB = GetAABBFromGeometryElement(geometryElement);
                            drawableAABB.Expand(geometryAABB);
                            sumOfGeometryVolumes += GetVolumeOfAABBWithMinDimension(geometryAABB);
                            double geometryCost = shaderCostFactor * costOfVerts;

                            totalGeometryCost += geometryCost;
                        }
                    }
                }

                // JWR
                // Our drawable volume we use here is quite crude.  I think what we need to do here is divide the geometry cost by the 
                // *unique* volume taken up by all of the geometries.  The key is to make sure we don't count overlapping geometries twice.
                // One way to do this would be to:
                //    1) create a volume the size of the entire drawable bounds that is split ~50 times in each dimension
                //    2) enumerate over the geometries marking each sub-volume that is encompassed by he geometry's bounds
                //    3) calculate the drawable volume as volume of the marked sub-volumes

                double realDrawableVolume = GetVolumeOfAABBWithMinDimension(drawableAABB);

                // Taking this min value gets us a good working volume in two extreme cases of:
                //    1) A drawable with geometries at the extents of its bounds (using realDrawableVolume would be bad)
                //    2) A drawable with many overlapping geometries (using sumOfGeometryVolumes would be bad)
                double drawableVolumeToUse = Math.Min(realDrawableVolume, sumOfGeometryVolumes);
                drawableVolumeToUse = Math.Max(drawableVolumeToUse, minDrawableVolume_);
                double heuristic = totalGeometryCost / drawableVolumeToUse;

                return heuristic;
            }
            catch (Exception ex)
            {
                Log.Log__Warning("Unexpected issue encountered when parsing heuristic XML for '{0}'.  Heuristic will be 0.", Name);
                Log.Log__Warning(ex.ToString());

                return 0;// prevent partial calculation giving mad results
            }
        }

        private static readonly double minDimensionRatio_ = 0.01d;// All geometries have a minimum dimension to prevent tiny volumes (as agreed with Piotr) - JWR
        private static readonly double minDrawableVolume_ = 0.125d;// Further to that, all geometries have a minimum drawable volume (1/8 m^3) - JWR
    }
}
