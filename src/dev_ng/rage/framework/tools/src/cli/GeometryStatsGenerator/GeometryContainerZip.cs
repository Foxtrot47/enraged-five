﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

using Ionic.Zip;
using RSG.Base.ConfigParser;

namespace GeometryStatsGenerator
{
    /// <summary>
    /// Represents a zip file containing geometry data
    /// </summary>
    internal class GeometryContainerZip
    {
        internal GeometryContainerZip(ContentNodeMapZip mapZipNode)
        {
            ContentNodeFile zipFileNode = mapZipNode.Children.OfType<ContentNodeFile>().First(fileNode => fileNode.Extension == "zip");
            ContentNodeFile sceneXmlFileNode = mapZipNode.Children.OfType<ContentNodeFile>().First(fileNode => fileNode.Extension == "xml");

            Name = mapZipNode.Name;
            MapZipPathname = Path.Combine(mapZipNode.Path, zipFileNode.Filename);
            SceneXmlPathname = Path.Combine(mapZipNode.Path, sceneXmlFileNode.Filename);

            if(!File.Exists(MapZipPathname))
                throw new System.IO.FileNotFoundException(String.Format("MapZip file at '{0}' doesn't exist locally", MapZipPathname));
        }

        internal string Name { get; private set; }
        internal string MapZipPathname { get; private set; }
        internal string SceneXmlPathname { get; private set; }

        internal GeometryZip[] GeometryZips { get; private set; }

        internal void Extract(string baseDirectory)
        {
            List<GeometryZip> geometryZips = new List<GeometryZip>();
            using (ZipFile mapZipFile = ZipFile.Read(MapZipPathname))
            {
                string mapZipTempDirectory = baseDirectory + "\\" + Name;
                foreach (ZipEntry geometryZipEntry in mapZipFile.Entries)
                {
                    GeometryZip.GeometryType geometryType;
                    if (geometryZipEntry.FileName.EndsWith("idr.zip"))
                        geometryType = GeometryZip.GeometryType.Drawable;
                    else if (geometryZipEntry.FileName.EndsWith("ift.zip"))
                        geometryType = GeometryZip.GeometryType.Fragment;
                    else
                        continue;

                    string targetPathname = Path.Combine(mapZipTempDirectory, geometryZipEntry.FileName);
                    bool extractRequired = true;
                    if (File.Exists(targetPathname))
                    {
                        FileInfo targetFileInfo = new FileInfo(targetPathname);
                        if (targetFileInfo.LastWriteTime == geometryZipEntry.LastModified)
                        {
                            extractRequired = false;
                        }
                        else
                        {
                            File.Delete(targetPathname);// Zip is newer
                        }
                    }

                    if (extractRequired)
                        geometryZipEntry.Extract(mapZipTempDirectory);

                    geometryZips.Add(new GeometryZip(geometryType, targetPathname));
                }
            }

            GeometryZips = geometryZips.ToArray();
        }
    }
}
