﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeometryStatsGenerator
{
    /// <summary>
    /// Responsible for generating (and caching) shader heuristics based on their name
    /// </summary>
    internal class ShaderCostCalculator
    {
        private ShaderCostCalculator()
        {
            cachedCosts_ = new Dictionary<string, double>();
        }

        internal static ShaderCostCalculator Instance
        {
            get
            {
                if (instance_ == null)
                    instance_ = new ShaderCostCalculator();

                return instance_;
            }
        }

        private static ShaderCostCalculator instance_;

        /// <summary>
        /// Shader costs are based on the shader name :-( as decided by Piotr
        /// The graphics dudes would have to improve the RageGraphics API for a better mechanism
        /// </summary>
        internal double GetShaderCostFactor(string shaderName)
        {
            if (cachedCosts_.ContainsKey(shaderName))
                return cachedCosts_[shaderName];

            string shaderNameLower = shaderName.ToLower();
            double shaderCostFactor = 1.0;
            if (shaderNameLower.Contains("normal"))
                shaderCostFactor += 0.1;
            if (shaderNameLower.Contains("reflect"))
                shaderCostFactor += 0.1;
            if (shaderNameLower.Contains("spec"))
                shaderCostFactor += 0.1;
            if (shaderNameLower.Contains("tnt"))
                shaderCostFactor += 0.1;

            cachedCosts_.Add(shaderName, shaderCostFactor);

            return shaderCostFactor;
        }

        private Dictionary<string, double> cachedCosts_;
    }
}
