﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;

namespace GeometryStatsGenerator
{
    /// <summary>
    /// Represents in intermediate geometry zip file
    /// </summary>
    internal class GeometryZip
    {
        internal enum GeometryType
        {
            Drawable,
            Fragment
        }

        internal GeometryZip(GeometryType geometryType, string pathname)
        {
            Type = geometryType;
            Pathname = pathname;
            FileInfo fileInfo = new FileInfo(pathname);
            FileSize = fileInfo.Length;
            StatsPathname = Path.ChangeExtension(Pathname, ".xml");

            if (Type == GeometryType.Drawable)
                ArchetypeName = Path.GetFileName(Pathname).Replace(".idr.zip", "").ToLower();
            else if (Type == GeometryType.Fragment)
                ArchetypeName = Path.GetFileName(Pathname).Replace(".ift.zip", "").ToLower();
        }

        internal GeometryType Type { get; private set; }
        internal string Pathname { get; private set; }
        internal long FileSize { get; private set; }
        internal string StatsPathname { get; private set; }
        internal string ArchetypeName { get; private set; }
    }
}
