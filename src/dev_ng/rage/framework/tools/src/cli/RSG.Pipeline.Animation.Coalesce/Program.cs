﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Diagnostics;
using RSG.Base.OS;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace RSG.Pipeline.Animation.Coalesce
{
    sealed class Program
    {

        #region Constants
        private static readonly int EXIT_SUCCESS = 0;
        private static readonly int EXIT_ERROR = 1;

        private static readonly String OPTION_COALESCE_EXE = "coalesce";
        private static readonly String OPTION_ANIMCOMBINE_EXE = "combine";
        private static readonly String OPTION_ANIMEDIT_EXE = "edit";
        private static readonly String OPTION_ANIMQUERY_EXE = "query";
        private static readonly String OPTION_INPUTDIR = "inputdir";
        private static readonly String OPTION_OUTPUTDIR = "outputdir";
        private static readonly String OPTION_FRAMELIMIT = "framelimit";
        private static readonly String OPTION_DOF_INJECTION_XML = "dofinjection";
        private static readonly String OPTION_MEMORY = "memory";
        private static readonly String OPTION_SCORE = "score";
        private static readonly String OPTION_TASK_NAME = "taskname";
        private static readonly String LOG_CTX = "Coalesce";
        
        #endregion // Constants

        static int RunProcess(string strExe, string strArgs, IUniversalLog log)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.Arguments = strArgs;
            startInfo.FileName = strExe;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.CreateNoWindow = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false;

            log.MessageCtx(LOG_CTX, strExe + " " + strArgs);

            Process proc = Process.Start(startInfo);
            String output = proc.StandardOutput.ReadToEnd();
            proc.WaitForExit();
            log.MessageCtx(LOG_CTX, output);

            return proc.ExitCode;
        }

        [STAThread]
        static int Main(string[] args)
        {
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.CreateUniversalLog("Coalesce");

            int exit_code = EXIT_SUCCESS;

            try
            {
                // Define our executable options.  Stick with the long options
                // (e.g. --opt) rather than the single character versions.
                Getopt options = new Getopt(args, new LongOption[] {
                    new LongOption(OPTION_COALESCE_EXE, LongOption.ArgType.Required, 
                        "coalesce filename."),
                    new LongOption(OPTION_ANIMCOMBINE_EXE, LongOption.ArgType.Required,
                        "animcombine filename."),
                    new LongOption(OPTION_ANIMEDIT_EXE, LongOption.ArgType.Required,
                        "animedit filename."),
                    new LongOption(OPTION_ANIMQUERY_EXE, LongOption.ArgType.Required,
                        "animquery filename."),
                    new LongOption(OPTION_INPUTDIR, LongOption.ArgType.Required,
                        "Input directory."),
                    new LongOption(OPTION_OUTPUTDIR, LongOption.ArgType.Required,
                        "Output directory."),
                    new LongOption(OPTION_FRAMELIMIT, LongOption.ArgType.Required,
                        ""),
                    new LongOption(OPTION_DOF_INJECTION_XML, LongOption.ArgType.Required,
                        ""),
                    new LongOption(OPTION_MEMORY, LongOption.ArgType.Optional,
                        ""),
                    new LongOption(OPTION_SCORE, LongOption.ArgType.Optional,
                        ""),
                    new LongOption(OPTION_TASK_NAME, LongOption.ArgType.Required,
                        "Asset Pipeline 3 Task Name."),
                });
                // Turn args into vars
                String coalesceExe = options[OPTION_COALESCE_EXE] as String;
                String animEditExe = options[OPTION_ANIMEDIT_EXE] as String;
                String animQueryExe = options[OPTION_ANIMQUERY_EXE] as String;
                String animCombineExe = options[OPTION_ANIMCOMBINE_EXE] as String;
                String inputDirectory = options[OPTION_INPUTDIR] as String;
                String outputDirectory = options[OPTION_OUTPUTDIR] as String;
                Int32 framelimit = Convert.ToInt32(options[OPTION_FRAMELIMIT] as String);
                String dofInjectionXml = options[OPTION_DOF_INJECTION_XML] as String;
                bool memory = options.HasOption(OPTION_MEMORY);
                bool score = options.HasOption(OPTION_SCORE);

                String taskName = options[OPTION_TASK_NAME] == null ? String.Empty : options[OPTION_TASK_NAME] as String;

                if (String.Empty != taskName)
                {
                    Console.WriteLine("TASK: {0}", taskName);
                    log.MessageCtx(LOG_CTX, "TASK: {0}", taskName);
                }

                log.MessageCtx(LOG_CTX, "Coalescing {0} to {1}", inputDirectory, outputDirectory);
                if (!Directory.Exists(inputDirectory))
                {
                    log.ErrorCtx(LOG_CTX, "Prepared/Grouped data missing from {0}.", inputDirectory);
                    return (EXIT_ERROR);
                }

                String _clipListFile = Path.Combine(inputDirectory, "cliplists.xml");
                if (!File.Exists(_clipListFile))
                {
                    log.ErrorCtx(LOG_CTX, "Nothing to coalesce from {0}.", inputDirectory);
                    exit_code = EXIT_ERROR;
                    return (exit_code);
                }

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(_clipListFile);

                List<string> alCompressionArray = new List<string>();
                List<string> alAdditiveArray = new List<string>();
                List<string> alSkeletonArray = new List<string>();
                List<List<string>> alClipsArray = new List<List<string>>();

                XmlNodeList compressionNodes = xmlDoc.SelectNodes("/Lists/List/Compression");
                XmlNodeList additiveNodes = xmlDoc.SelectNodes("/Lists/List/Additive");
                XmlNodeList skeletonNodes = xmlDoc.SelectNodes("/Lists/List/Skeleton");
                XmlNodeList clipsNodes = xmlDoc.SelectNodes("/Lists/List/Clips");
                foreach (XmlNode n in compressionNodes) alCompressionArray.Add(n.InnerText);
                foreach (XmlNode n in additiveNodes) alAdditiveArray.Add(n.InnerText);
                foreach (XmlNode n in skeletonNodes) alSkeletonArray.Add(n.InnerText);
                for (int i = 0; i < clipsNodes.Count; ++i)
                {
                    alClipsArray.Add(new List<string>());
                    XmlNode n = clipsNodes[i];
                    foreach (XmlNode c in n.ChildNodes)
                    {
                        alClipsArray[i].Add(c.InnerText);
                    }
                }

                if (!File.Exists(dofInjectionXml))
                {
                    log.ErrorCtx(LOG_CTX, "No file for eligible DOF injection skeletons: {0}.", dofInjectionXml);
                    return (EXIT_ERROR);
                }

                XDocument xdoc = XDocument.Load(dofInjectionXml);
                IEnumerable<String> eligibleSkeletons =
                    from el in xdoc.Descendants("Item")
                    select el.Value;

                for (int i = 0; i < alClipsArray.Count; ++i)
                {
                    String clipList = String.Format("{0}@{1}@{2}", alCompressionArray[i], Path.GetFileNameWithoutExtension(alSkeletonArray[i]), Convert.ToBoolean(alAdditiveArray[i]) == true ? "1" : "0");
                    log.MessageCtx(LOG_CTX, clipList);

                    try
                    {
                        log.MessageCtx(LOG_CTX, "Applying animation modifications");
                        String skeleton = alSkeletonArray[i];

                        bool additive = Convert.ToBoolean(alAdditiveArray[i]);

                        bool hasEligibleSkeleton = false;
                        foreach (String eligibleSkeleton in eligibleSkeletons)
                        {
                            if (skeleton == eligibleSkeleton)
                            {
                                hasEligibleSkeleton = true;
                                break;
                            }
                        }

                        // Loop through applying additive/DOF injection where appropriate
                        foreach (String clipFile in alClipsArray[i])
                        {
                            String animFile = clipFile.Substring(0, clipFile.IndexOf('.')) + ".anim";
                            if (!File.Exists(animFile))
                            {
                                log.ErrorCtx(LOG_CTX, "Animation file {0} doesn't exist to apply additive/DOF injection", animFile);
                                return (EXIT_ERROR);
                            }

                            if (additive == true)
                            {
                                String additive_args = String.Format("-anims {0},{0} -subtract -time 0 -out {0} -nocompression -nopopups", animFile);
                                ProcessStartInfo additiveStartInfo = new ProcessStartInfo();

                                if (RunProcess(animCombineExe, additive_args, log) != 0)
                                    return EXIT_ERROR;
                            }

                            if (hasEligibleSkeleton)
                            {
                                log.MessageCtx(LOG_CTX, "Injecting animation track modifications");
                                // We query the animation to see if it has a specific track, if it does then we inject the weight dof (kTrackFirstPersonCameraWeight) 
                                // with a value of 1.0, else 0.0
                                // Specific dof is track 53 and id 45733 which is kTrackCameraLimit, (athash16u "FirstPersonCamPivotAttrs_DOF_MinX")
                                String firstPersonCameraWeightTrack = ";add[138,0]=";
                                String animquery_args = String.Format("-anim {0} -track {1} -id {2}", animFile, "53", "45733");
                                int ret = RunProcess(animQueryExe, animquery_args, log);
                                if (ret == 0)
                                    firstPersonCameraWeightTrack += "float(0.0)";
                                else if (ret == 1)
                                    firstPersonCameraWeightTrack += "float(1.0)";
                                else
                                    return EXIT_ERROR;

                                // If the anim contains track 1, id 33869, type 1 (kTrackBoneRotation, BONETAG_CH_L_HAND kFormatTypeQuaternion) the DOF, 
                                // track 139, id 0, type 2 (kTrackConstraintHelperLeftHandWeight, 0, kFormatTypeFloat) should be set to 1, otherwise 0.
                                String leftHandWeightTrack = ";add[139,0]=";
                                animquery_args = String.Format("-anim {0} -track {1} -id {2}", animFile, "1", "33869");
                                ret = RunProcess(animQueryExe, animquery_args, log);
                                if (ret == 0)
                                    leftHandWeightTrack += "float(0.0)";
                                else if (ret == 1)
                                    leftHandWeightTrack += "float(1.0)";
                                else
                                    return EXIT_ERROR;

                                // If the anim contains track 1, id 4126, type 1 (kTrackBoneRotation, BONETAG_CH_R_HAND kFormatTypeQuaternion) the DOF, 
                                // track 140, id 0, type 2 (kTrackConstraintHelperRightHandWeight, 0, kFormatTypeFloat) should be set to 1, otherwise 0.
                                String rightHandWeightTrack = ";add[140,0]=";
                                animquery_args = String.Format("-anim {0} -track {1} -id {2}", animFile, "1", "4126");
                                ret = RunProcess(animQueryExe, animquery_args, log);
                                if (ret == 0)
                                    rightHandWeightTrack += "float(0.0)";
                                else if (ret == 1)
                                    rightHandWeightTrack += "float(1.0)";
                                else
                                    return EXIT_ERROR;

                                // 134 (kTrackUpperBodyFixupWeight) and 137 (kTrackIndependentMoverWeight) are track numbers specified by runtime.
                                String animedit_args = String.Format("-anim={0} -out={0} -op add[134,0]=float(0.0);add[137,0]=float(0.0){1}{2}{3} -nopopups -all_tty=error", animFile, firstPersonCameraWeightTrack, leftHandWeightTrack, rightHandWeightTrack);
                                animedit_args = animedit_args.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);

                                if (RunProcess(animEditExe, animedit_args, log) != 0)
                                    return EXIT_ERROR;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        log.ToolExceptionCtx(LOG_CTX, ex, "Exception processing clip list file {0}", clipList);
                        return (EXIT_ERROR);
                    }

                    String filenameOnly = Path.GetFileName(clipList);
                    String clipListFilename = Path.Combine(outputDirectory, "clips.cliplist");
                    String groupedOutputDir = Path.Combine(outputDirectory, clipList);

                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(clipListFilename))
                    {
                        foreach (string clip in alClipsArray[i].ToArray())
                        {
                            file.WriteLine(clip);
                        }
                    }

                    String coalesce_args = String.Format("-cliplist={0} -outdir={1} -outprefix={2}_ -framelimit={3} -nopopups", clipListFilename, groupedOutputDir, clipList, framelimit);
                    if (score == true)
                        coalesce_args += " -score";

                    if (memory == true)
                        coalesce_args += " -memory";

                    if (!Directory.Exists(groupedOutputDir))
                        Directory.CreateDirectory(groupedOutputDir);

                    if (RunProcess(coalesceExe, coalesce_args, log) != 0)
                        return EXIT_ERROR;
                }
            }
            catch (Exception ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Unhandled Exception during animation Coalesce Process");
                exit_code = EXIT_ERROR;
            }
            finally
            {
                LogFactory.ApplicationShutdown();
            }

            return (exit_code);
        }
    }
}
