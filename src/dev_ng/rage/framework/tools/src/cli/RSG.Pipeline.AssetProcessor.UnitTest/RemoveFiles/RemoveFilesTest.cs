﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RSG.Pipeline.AssetProcessor.Operations;
using RSG.Pipeline.AssetProcessor.UnitTest.Utils;

namespace RSG.Pipeline.AssetProcessor.UnitTest.RemoveFiles
{
    [TestClass]
    public sealed class RemoveFilesTest : AssetProcessorTestBase
    {
        private const string OutputPath = @"./unit_test\output\removefiles";

        [ClassInitialize]
        public static void RemoveFilesTestInitialize(TestContext testContext)
        {
            // delete previous tests zip outputs
            // x:\gta5\cache\maps\dev_ng\asset_processor
            if (Directory.Exists(OutputPath))
            {
                Directory.Delete(OutputPath, true);
            }
        }

        [TestInitialize]
        public void TestInitialize()
        {
            string currentConfigPath;
            string currentLogName;

            switch (TestContext.TestName)
            {
                case "RemoveFourFiles":
                    currentConfigPath = "./RemoveFiles/test_remove_four_files.xml";
                    currentLogName = "RemoveFourFiles";
                    break;
                default:
                    throw new InternalTestFailureException("Invalid TestContext.TestName. The method name must be added to the switch/case in the Test.");
            }
            // TODO Flo: move the arguments into AssetProcessorTestBase as constants.
            string[] args = { "-config", currentConfigPath, "-branch", "dev_ng", "-logdir", "x:/gta5/tools_ng/logs/RSG.Pipeline.AssetProcessor.UnitTest", "-taskname", "Remove_files_Test" };

            Configuration(currentLogName, args);

            // TODO add further setup to perform a full copy first so we can remove thing afterward
        }

        [TestCleanup]
        public void TestCleanup()
        {
            
        }

        [TestMethod]
        public void RemoveFourFiles()
        {
            RemoveFilesOperation operation = new RemoveFilesOperation();

            bool result = operation.Run(m_task, m_commandOptions, m_log, m_inputs, m_outputAsset);
            Assert.IsTrue(result);

            WriteArchive();

            ValidateOutput(m_inputs);
        }

        private void ValidateOutput(IEnumerable<Tuple<string, ZipArchive>> inputs)
        {
            // retrieve source's CRC for each entry
            var inputValues = new Dictionary<string, Tuple<long, string>>();

            foreach (Tuple<string, ZipArchive> input in inputs)
            {
                using (ZipArchive archive = new ZipArchive(File.OpenRead(input.Item1)))
                {
                    foreach (ZipArchiveEntry entry in archive.Entries)
                    {
                        using (Stream entryStream = entry.Open())
                        {
                            inputValues.Add(entry.FullName, new Tuple<long, string>(entry.Length, CRC.ComputeCRC(entryStream)));
                        }
                    }
                }
            }

            // retrieve supposedly removed entries from inputs
            var removedElements = m_task.Elements.Select(elem => elem.ToString()).ToList();

            // Compare that to what's actually in the output
            using (ZipArchive archive = new ZipArchive(File.OpenRead(m_config.Output.AbsolutePath)))
            {
                foreach (ZipArchiveEntry entry in archive.Entries)
                {
                    // make sure that every entry in the output is in the inputs
                    Tuple<long, string> sourceEntry;
                    Assert.IsTrue(inputValues.TryGetValue(entry.FullName, out sourceEntry));

                    // ensure they are the same
                    Assert.AreEqual(entry.Length, sourceEntry.Item1);

                    // validate with CRC. Yay !
                    using (Stream entrysStream = entry.Open())
                    {
                        var crc = CRC.ComputeCRC(entrysStream);
                        Assert.AreEqual(sourceEntry.Item2, crc);
                    }

                    // ensure that the entry which were removed are not there
                    Assert.IsFalse(removedElements.Contains(entry.FullName));
                }
            }
        }
    }
}
