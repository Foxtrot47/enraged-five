﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RSG.Pipeline.AssetProcessor.Operations;

namespace RSG.Pipeline.AssetProcessor.UnitTest.MaterialPreset
{
    [TestClass]
    public class MaterialPresetTest : AssetProcessorTestBase
    {
        private const string OutputPath = "./UnitTestFiles/output/materialpreset";

        [ClassInitialize]
        public static void MaterialPresetTestInitialize(TestContext testContext)
        {
            // delete previous tests zip outputs
            if (Directory.Exists(OutputPath))
            {
                Directory.Delete(OutputPath, true);
            }
        }

        [TestInitialize]
        public void TestInitialize()
        {
            string currentConfigPath;
            string currentLogName;

            switch (TestContext.TestName)
            {
                case "GenerateMaterialPreset":
                    // DO STUFF
                    currentConfigPath = "./MaterialPreset/test_material_preset.xml";
                    currentLogName = "MaterialPreset";
                    break;
                default:
                    throw new InternalTestFailureException("Invalid TestContext.TestName. The method name must be added to the switch/case in the Test.");
            }
            // TODO Flo: move the arguments into AssetProcessorTestBase as constants.
            string[] args = { "-config", currentConfigPath, "-branch", "dev_ng", "-logdir", "x:/gta5/tools_ng/logs/RSG.Pipeline.AssetProcessor.UnitTest", "-taskname", "Material_Preset_Test" };

            Configuration(currentLogName, args);
        }

        [TestMethod]
        public void GenerateMaterialPreset()
        {
            MaterialPresetOperation operation = new MaterialPresetOperation();

            bool result = operation.Run(m_task, m_commandOptions, m_log, m_inputs, m_outputAsset);
            Assert.IsTrue(result);
        }
    }
}
