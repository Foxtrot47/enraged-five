﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Services.AssetProcessor;

namespace RSG.Pipeline.AssetProcessor.UnitTest
{
    public class AssetProcessorTestBase : IDisposable
    {
        protected const string LOG_CTX = "Asset Processor Unit Tests";
        protected const string OPT_CONFIG = "config";
        protected const string OPT_TASKNAME = "taskname";
        protected const string OPT_LOGDIRECTORY = "logdir";

        // TODO Flo: set log as static accross all tests ?
        protected IUniversalLog m_log;
        protected CommandOptions m_commandOptions;
        protected AssetProcessorConfig m_config;
        protected AssetProcessorTask m_task;

        protected IEnumerable<Tuple<string, ZipArchive>> m_inputs;
        protected ZipArchive m_outputAsset;
        protected Stream m_outputStream;

        protected AssetProcessorTestBase()
        {
        }

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        protected void Configuration(string currentLogName, string[] args)
        {
            LongOption[] mOptions =
            {
                new LongOption(OPT_CONFIG, LongOption.ArgType.Required, "Asset Processor configuration filename."),
                new LongOption(OPT_TASKNAME, LongOption.ArgType.Required, "XGE taskname; echoed to TTY for XGE error log parsing in AP3."),
                new LongOption(OPT_LOGDIRECTORY, LongOption.ArgType.Required, "log directory"),
            };

            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            m_log = LogFactory.CreateUniversalLog(LOG_CTX + currentLogName);
            m_commandOptions = new CommandOptions(args, mOptions);


            string configFilename = (string)m_commandOptions[OPT_CONFIG];
            m_config = AssetProcessorConfig.Load(configFilename, m_log).FirstOrDefault();

            // Load input(s).
            m_inputs = m_config.Inputs.Select(filename => new Tuple<string, ZipArchive>(filename.AbsolutePath, ZipFile.Open(filename.AbsolutePath, ZipArchiveMode.Read))).ToList();

            // we assert for the test's sake that we only have one task.
            Assert.IsTrue(m_config.Operations.Count() == 1, "This Test has more task than allowed. Remove tasks from the config files, or rewrite the tests because whow knows, maybe we want to test multiple tasks.");

            m_task = m_config.Operations.First();

            string outputPath = Path.GetDirectoryName(m_config.Output.AbsolutePath);
            // check for output path existence and create the directory if not
            if (!Directory.Exists(outputPath))
            {
                Directory.CreateDirectory(outputPath);
            }

            // Create output.
            m_outputStream = new MemoryStream();
            m_outputAsset = new ZipArchive(m_outputStream, ZipArchiveMode.Update, true);
        }

        /// <summary>
        /// Ensure the inputs are disposed, then Dispose() the current zip archive and writes out the result
        /// </summary>
        protected void WriteArchive()
        {
            bool disposeInputs = DisposeInputs();
            Assert.IsTrue(disposeInputs, "Error during inputs dispose.");

            bool createArchive = CreateOutput();
            Assert.IsTrue(createArchive, "Error during ouput archive creation.");
        }

        /// <summary>
        /// Dispose every ZipArchive opened for the inputs
        /// </summary>
        /// <returns></returns>
        protected bool DisposeInputs()
        {
            try
            {
                // Cleanup inputs.
                foreach (Tuple<string, ZipArchive> inputAsset in m_inputs)
                {
                    inputAsset.Item2.Dispose();
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Dispose the output's ZipArchive then writes out the file to disk
        /// </summary>
        /// <returns>True if everything succeeded, false otherwise.</returns>
        protected bool CreateOutput()
        {
            try
            {
                // Dispose in Memory output archive to allow its stream to be written out later on
                // m_outputAsset MUST HAVE BEEN created with the "leaveOpen" flag set to true
                m_outputAsset.Dispose();

                // check for output path existence and create the directory if not
                var outputPath = Path.GetDirectoryName(m_config.Output.AbsolutePath);
                if (!Directory.Exists(outputPath))
                    Directory.CreateDirectory(outputPath);

                // Since output ZipArchive has been disposed, we can write the memorystream to a file
                using (FileStream fileStream = new FileStream(m_config.Output.AbsolutePath, FileMode.Create))
                {
                    m_outputStream.Seek(0, SeekOrigin.Begin);
                    m_outputStream.CopyTo(fileStream);
                }

                m_outputStream.Close();

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (m_outputAsset != null)
                {
                    m_outputAsset.Dispose();
                    m_outputAsset = null;
                }

                if (m_outputStream != null)
                {
                    m_outputStream.Close();
                }
            }
        }
    }
}