﻿using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RSG.Pipeline.AssetProcessor.Operations;

namespace RSG.Pipeline.AssetProcessor.UnitTest.DrawableMerge
{
    [TestClass]
    public sealed class DrawableMergeTest : AssetProcessorTestBase
    {
        private const string OutputPath = @"./unit_test\output\drawablemerge";

        [ClassInitialize]
        public static void DrawableMergeTestInitialize(TestContext testContext)
        {
            // delete previous tests zip outputs
            // x:\gta5\cache\maps\dev_ng\asset_processor
            if (Directory.Exists(OutputPath))
            {
                var entries = Directory.GetFileSystemEntries(OutputPath);
                foreach (var entry in entries)
                {
                    File.Delete(entry);
                }
            }
        }

        #region Tests initialization
        [TestInitialize]
        public void TestInitialize()
        {
            string currentConfigPath;
            string currentLogName;

            // Depending on which test got executed, set the according config file
            switch (TestContext.TestName)
            {
                case "DrawableMergeBasicTest":
                    currentConfigPath = "./DrawableMerge/test_drawable_merge_basic.xml";
                    currentLogName = "DrawableMerge";
                    break;
                default:
                    throw new InternalTestFailureException("Invalid TestContext.TestName. The method name must be added to the switch/case in the Test.");
            }
            // TODO Flo: move the arguments into AssetProcessorTestBase as constants.
            string[] args = { "-config", currentConfigPath, "-branch", "dev_ng", "-logdir", "X:/gta5/tools_ng/logs/RSG.Pipeline.AssetProcessor.UnitTest", "-taskname", "Drawable_Merge_Unit_Test" };

            Configuration(currentLogName, args);
        }

        [TestCleanup]
        public void TestCleanup()
        {
        }
        #endregion

        #region Drawable Merge Tests
        [TestMethod]
        public void DrawableMergeBasicTest()
        {
            DrawableMergeOperation operation = new DrawableMergeOperation();
            bool result = operation.Run(m_task, m_commandOptions, m_log, m_inputs, m_outputAsset);

            Assert.IsTrue(result);

            WriteArchive();
        }
        #endregion
    }
}

#region Idea's graveyard
//// Instantiate operations based on Reflection
//Activator.CreateInstance(Task.Assembly, Task.Type)
//<Task operation="Copy" name="01_one_to_one_file_copy" assembly="RSG.Pipeline.AssetProcessor.CopyAsset" type="CopyAssetOperation"/>
#endregion