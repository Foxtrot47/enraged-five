﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Services.AssetProcessor;

namespace RSG.Pipeline.AssetProcessor.UnitTest.AssetProcessorConfigTest
{
    [TestClass]
    public class AssetProcessorConfigTest
    {
        protected const string LOG_CTX = "Asset Processor Unit Tests";
        protected const string OPT_CONFIG = "config";
        protected const string OPT_TASKNAME = "taskname";
        protected const string OPT_LOGDIRECTORY = "logdir";

        protected IUniversalLog m_log;
        protected CommandOptions m_commandOptions;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        [TestInitialize]
        public void TestInitialize()
        {
            string currentConfigPath;
            string currentLogName;

            switch (TestContext.TestName)
            {
                case "AssetProcessorConfigLoadTest":
                case "AssetProcessorConfigSaveTest":
                case "AssetProcessorConfigCodeDrivenTest":
                    currentConfigPath = "./AssetProcessorConfigTest/test_config_load.xml";
                    currentLogName = "AssetProcessorConfigLoad";
                    break;
                default:
                    throw new InternalTestFailureException("Invalid TestContext.TestName. The method name must be added to the switch/case in the Test.");
            }
            // TODO Flo: move the arguments into AssetProcessorTestBase as constants.
            string[] args = { "-config", currentConfigPath, "-branch", "dev_ng", "-logdir", "x:/gta5/tools_ng/logs/RSG.Pipeline.AssetProcessor.UnitTest", "-taskname", "Remove_files_Test" };

            LongOption[] mOptions =
            {
                new LongOption(OPT_CONFIG, LongOption.ArgType.Required, "Asset Processor configuration filename."),
                new LongOption(OPT_TASKNAME, LongOption.ArgType.Required, "XGE taskname; echoed to TTY for XGE error log parsing in AP3."),
                new LongOption(OPT_LOGDIRECTORY, LongOption.ArgType.Required, "log directory"),
            };

            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            m_log = LogFactory.CreateUniversalLog(LOG_CTX + currentLogName);
            m_commandOptions = new CommandOptions(args, mOptions);
        }


        [TestMethod]
        public void AssetProcessorConfigLoadTest()
        {
            string configFilename = (string)m_commandOptions[OPT_CONFIG];
            var config = AssetProcessorConfig.Load(configFilename, m_log);

            Assert.IsNotNull(config);
        }

        [TestMethod]
        public void AssetProcessorConfigSaveTest()
        {
            string configFilename = (string)m_commandOptions[OPT_CONFIG];
            var config = AssetProcessorConfig.Load(configFilename, m_log);

            Assert.IsNotNull(config);

            AssetProcessorConfig.Save(@"X:\gta5\src\dev_ng\rage\framework\tools\src\cli\RSG.Pipeline.AssetProcessor.UnitTest\bin\x64\Release\AssetProcessorConfigTest/test_config_save.xml", config);
        }

        [TestMethod]
        public void AssetProcessorConfigCodeDrivenTest()
        {
            // DrawableMerge has no elements as you merge inputs to output, so no elements.
            AssetProcessorTask drawableMerge = new AssetProcessorTask(Operation.DrawableMerge);

            Uri[] removeElements =
            {
                new Uri(@"file04.txt", UriKind.RelativeOrAbsolute), 
                new Uri(@"input03/VB_38_Build01_LOD/VB_38_Build01_LOD.mesh", UriKind.RelativeOrAbsolute), 
                new Uri(@"input03/VB_38_Build01_LOD/VB_38_Build01_Proxy.mesh", UriKind.RelativeOrAbsolute), 
                new Uri(@"input02/Che_01_Details.mesh", UriKind.RelativeOrAbsolute)
            };
            AssetProcessorTask removeFiles = new AssetProcessorTask(Operation.RemoveFiles, removeElements);

            Uri[] addElements =
            {
                new Uri(@"X:\gta5\src\dev_ng\rage\framework\tools\src\cli\RSG.Pipeline.AssetProcessor.UnitTest\bin\x64\Release\UnitTestFiles/source/addfiles/file01.txt", UriKind.RelativeOrAbsolute),
                new Uri(@"X:\gta5\src\dev_ng\rage\framework\tools\src\cli\RSG.Pipeline.AssetProcessor.UnitTest\bin\x64\Release\UnitTestFiles/source/addfiles/file02.txt", UriKind.RelativeOrAbsolute),
                new Uri(@"X:\gta5\src\dev_ng\rage\framework\tools\src\cli\RSG.Pipeline.AssetProcessor.UnitTest\bin\x64\Release\UnitTestFiles/source/addfiles/file03.txt", UriKind.RelativeOrAbsolute),
                new Uri(@"X:\gta5\src\dev_ng\rage\framework\tools\src\cli\RSG.Pipeline.AssetProcessor.UnitTest\bin\x64\Release\UnitTestFiles/source/addfiles/file04.txt", UriKind.RelativeOrAbsolute),
            };
            AssetProcessorTask addFiles = new AssetProcessorTask(Operation.AddFiles, addElements);

            List<AssetProcessorTask> operations = new List<AssetProcessorTask> { addFiles, drawableMerge, removeFiles, };

            var inputs = new List<Uri>
            {
                new Uri(@"X:\gta5\src\dev_ng\rage\framework\tools\src\cli\RSG.Pipeline.AssetProcessor.UnitTest\bin\x64\Release\UnitTestFiles/source/drawablemerge/input01.zip", UriKind.RelativeOrAbsolute),
                new Uri(@"X:\gta5\src\dev_ng\rage\framework\tools\src\cli\RSG.Pipeline.AssetProcessor.UnitTest\bin\x64\Release\UnitTestFiles/source/drawablemerge/input02.zip", UriKind.RelativeOrAbsolute),
                new Uri(@"X:\gta5\src\dev_ng\rage\framework\tools\src\cli\RSG.Pipeline.AssetProcessor.UnitTest\bin\x64\Release\UnitTestFiles/source/drawablemerge/input03.zip", UriKind.RelativeOrAbsolute)
            };

            Uri output = new Uri(@"X:\gta5\src\dev_ng\rage\framework\tools\src\cli\RSG.Pipeline.AssetProcessor.UnitTest\bin\x64\Release\UnitTestFiles/output/AssetProcessor/output.idd.zip", UriKind.RelativeOrAbsolute);

            // try to create a config by code, with supposedly valid operations, inputs and output
            AssetProcessorConfig config = new AssetProcessorConfig(operations, inputs, output, m_log);

            Assert.IsNotNull(config);

            // build an enumeration init by that only config
            var configs = new[] {config};

            // save it, as a test for config validation
            AssetProcessorConfig.Save(@"X:\gta5\src\dev_ng\rage\framework\tools\src\cli\RSG.Pipeline.AssetProcessor.UnitTest\bin\x64\Release\AssetProcessorConfigTest/test_config_save_CodeDriven.xml", configs);

            // let's hope everything's fine
            configs = AssetProcessorConfig.Load(@"X:\gta5\src\dev_ng\rage\framework\tools\src\cli\RSG.Pipeline.AssetProcessor.UnitTest\bin\x64\Release\AssetProcessorConfigTest/test_config_save_CodeDriven.xml", m_log).ToArray();

            foreach (var processorConfig in configs)
            {
                Assert.IsNotNull(processorConfig);
            }
        }
    }
}
