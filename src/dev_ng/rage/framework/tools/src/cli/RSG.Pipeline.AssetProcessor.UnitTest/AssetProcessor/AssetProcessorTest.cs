﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.OS;
using RSG.Pipeline.AssetProcessor.Operations;
using RSG.Pipeline.AssetProcessor.UnitTest.Utils;
using RSG.Pipeline.Services.AssetProcessor;

namespace RSG.Pipeline.AssetProcessor.UnitTest.AssetProcessor
{
    /// <summary>
    /// This class acts as a "multiple unit tests chained in a simili-real use case"
    /// It's not really a unit test, but more like a proof of concept with multiple operations chained
    /// </summary>
    [TestClass]
    public sealed class AssetProcessorTest : AssetProcessorTestBase
    {
        private const string OutputPath = @"./unit_test\output\removefiles";

        [ClassInitialize]
        public static void AssetProcessorTestInitialize(TestContext testContext)
        {
            // delete previous tests zip outputs
            // x:\gta5\cache\maps\dev_ng\asset_processor
            if (Directory.Exists(OutputPath))
            {
                Directory.Delete(OutputPath, true);
            }
        }

        [TestInitialize]
        public void TestInitialize()
        {
            string currentConfigPath;
            string currentLogName;

            switch (TestContext.TestName)
            {
                case "AssetProcessorFullRun":
                    currentConfigPath = "./AssetProcessor/test_asset_processor_full_config.xml";
                    currentLogName = "AssetProcessorFullRun";
                    break;
                default:
                    throw new InternalTestFailureException("Invalid TestContext.TestName. The method name must be added to the switch/case in the Test.");
            }
            // TODO Flo: move the arguments into AssetProcessorTestBase as constants.
            string[] args = { "-config", currentConfigPath, "-branch", "dev_ng", "-logdir", "x:/gta5/tools_ng/logs/RSG.Pipeline.AssetProcessor.UnitTest", "-taskname", "Remove_files_Test" };

            // /!\ AssetProcessorTest do not use the common Configuration part, as it has multiple task defined
            // it's *special*

            LongOption[] mOptions =
            {
                new LongOption(OPT_CONFIG, LongOption.ArgType.Required, "Asset Processor configuration filename."),
                new LongOption(OPT_TASKNAME, LongOption.ArgType.Required, "XGE taskname; echoed to TTY for XGE error log parsing in AP3."),
                new LongOption(OPT_LOGDIRECTORY, LongOption.ArgType.Required, "log directory"),
            };

            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            m_log = LogFactory.CreateUniversalLog(LOG_CTX + currentLogName);
            m_commandOptions = new CommandOptions(args, mOptions);


            string configFilename = (string)m_commandOptions[OPT_CONFIG];
            m_config = AssetProcessorConfig.Load(configFilename, m_log).FirstOrDefault();

            // Load input(s).
            m_inputs = m_config.Inputs.Select(filename => new Tuple<string, ZipArchive>(filename.AbsolutePath, ZipFile.Open(filename.AbsolutePath, ZipArchiveMode.Read))).ToList();

            string outputPath = Path.GetDirectoryName(m_config.Output.AbsolutePath);
            // check for output path existence and create the directory if not
            if (!Directory.Exists(outputPath))
                Directory.CreateDirectory(outputPath);

            // Create output.
            m_outputStream = new MemoryStream();
            m_outputAsset = new ZipArchive(m_outputStream, ZipArchiveMode.Update, true);
        }

        [TestCleanup]
        public void TestCleanup()
        {

        }

        [TestMethod]
        public void AssetProcessorFullRun()
        {
            foreach (AssetProcessorTask task in m_config.Operations)
            {
                IAssetProcessorOperation operation = CreateOperation(task.Operation);

                bool result = operation.Run(task, m_commandOptions, m_log, m_inputs, m_outputAsset);
                Assert.IsTrue(result);
            }

            WriteArchive();

            // ValidateOutput(m_inputs);
        }

        private IAssetProcessorOperation CreateOperation(Operation operationType)
        {
            switch (operationType)
            {
                case Operation.Copy:
                    return new CopyAssetOperation();

                case Operation.DrawableMerge:
                    return new DrawableMergeOperation();

                case Operation.AddFiles:
                    return new AddFilesOperation();

                case Operation.RemoveFiles:
                    return new RemoveFilesOperation();

                case Operation.MaterialPreset:
                    return new MaterialPresetOperation();

                default:
                    Assert.Fail("Operation is not defined at this point.");
                    break;
            }
            return null;
        }

        private void ValidateOutput(IEnumerable<Tuple<string, ZipArchive>> inputs)
        {
            // retrieve source's CRC for each entry
            var inputValues = new Dictionary<string, Tuple<long, string>>();

            foreach (Tuple<string, ZipArchive> input in inputs)
            {
                using (ZipArchive archive = new ZipArchive(File.OpenRead(input.Item1)))
                {
                    var sourceZipName = Path.GetFileNameWithoutExtension(input.Item1);

                    foreach (ZipArchiveEntry entry in archive.Entries)
                    {
                        using (Stream entryStream = entry.Open())
                        {
                            inputValues.Add(string.Format(@"{0}\{1}",sourceZipName, entry.FullName), new Tuple<long, string>(entry.Length, CRC.ComputeCRC(entryStream)));
                        }
                    }
                }
            }

            // retrieve supposedly removed entries from inputs
            var removedElements = m_task.Elements.Select(elem => elem.ToString()).ToList();

            // Compare that to what's actually in the output
            using (ZipArchive archive = new ZipArchive(File.OpenRead(m_config.Output.AbsolutePath)))
            {
                foreach (ZipArchiveEntry entry in archive.Entries)
                {
                    // make sure that every entry in the output is in the inputs
                    Tuple<long, string> sourceEntry;
                    Assert.IsTrue(inputValues.TryGetValue(entry.FullName, out sourceEntry));

                    // ensure they are the same
                    Assert.AreEqual(entry.Length, sourceEntry.Item1);

                    // validate with CRC. Yay !
                    using (Stream entrysStream = entry.Open())
                    {
                        var crc = CRC.ComputeCRC(entrysStream);
                        Assert.AreEqual(sourceEntry.Item2, crc);
                    }

                    // ensure that the entry which were removed are not there
                    Assert.IsFalse(removedElements.Contains(entry.FullName));
                }
            }
        }
    }
}
