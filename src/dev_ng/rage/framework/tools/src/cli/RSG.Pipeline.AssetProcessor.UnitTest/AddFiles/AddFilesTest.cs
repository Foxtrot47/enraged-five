﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RSG.Pipeline.AssetProcessor.Operations;
using RSG.Pipeline.AssetProcessor.UnitTest.Utils;

namespace RSG.Pipeline.AssetProcessor.UnitTest.AddFiles
{
    [TestClass]
    public class AddFilesTest : AssetProcessorTestBase
    {
        private const string OutputPath = "./UnitTestFiles/output/addfiles";

        [ClassInitialize]
        public static void AddFilesTestInitialize(TestContext testContext)
        {
            // delete previous tests zip outputs
            if (Directory.Exists(OutputPath))
            {
                Directory.Delete(OutputPath, true);
            }
        }

        [TestInitialize]
        public void TestInitialize()
        {
            string currentConfigPath;
            string currentLogName;

            // Depending on which test got executed, set the according config file
            switch (TestContext.TestName)
            {
                // COPY Configs
                case "AddFiles":
                    currentConfigPath = "./AddFiles/test_add_files.xml";
                    currentLogName = "AddFiles";
                    break;
                default:
                    throw new InternalTestFailureException("Invalid TestContext.TestName. The method name must be added to the switch/case in the Test.");
            }
            // TODO Flo: move the arguments into AssetProcessorTestBase as constants.
            string[] args = { "-config", currentConfigPath, "-branch", "dev_ng", "-logdir", "x:/gta5/tools_ng/logs/RSG.Pipeline.AssetProcessor.UnitTest", "-taskname", "Add_Files_Test" };

            Configuration(currentLogName, args);
        }

        [TestCleanup]
        public void TestCleanup()
        {
            
        }

        [TestMethod]
        public void AddFiles()
        {
            AddFilesOperation operation = new AddFilesOperation();

            bool result = operation.Run(m_task, m_commandOptions, m_log, m_inputs, m_outputAsset);
            Assert.IsTrue(result);

            WriteArchive();

            ValidateOutput(m_inputs);
        }

        private void ValidateOutput(IEnumerable<Tuple<string, ZipArchive>> inputs)
        {
            // retrieve source's CRC for each entry
            var inputValues = new Dictionary<string, Tuple<long, string>>();

            foreach (Tuple<string, ZipArchive> input in inputs)
            {
                using (ZipArchive archive = new ZipArchive(File.OpenRead(input.Item1)))
                {
                    foreach (ZipArchiveEntry entry in archive.Entries)
                    {
                        using (Stream entryStream = entry.Open())
                        {
                            inputValues.Add(entry.FullName, new Tuple<long, string>(entry.Length, CRC.ComputeCRC(entryStream)));
                        }
                    }
                }
            }

            // retrieve supposedly added entries from inputs
            var addedElements = m_task.Elements.ToList();

            // Compare that to what's actually in the output
            using (ZipArchive archive = new ZipArchive(File.OpenRead(m_config.Output.AbsolutePath)))
            {
                // List of elements file names that were meant to be added for testing purpose
                var entriesNames = archive.Entries.Select(entry => entry.FullName).ToList();
                
                foreach (ZipArchiveEntry entry in archive.Entries)
                {
                    // we don't test the added files yet, they will have their own test later
                    if (entriesNames.Contains(entry.FullName))
                        continue;

                    // make sure that every entry in the output is in the inputs
                    Tuple<long, string> sourceEntry;
                    Assert.IsTrue(inputValues.TryGetValue(entry.FullName, out sourceEntry));

                    // ensure they are the same
                    Assert.AreEqual(entry.Length, sourceEntry.Item1);

                    // validate with CRC. Yay !
                    using (Stream entrysStream = entry.Open())
                    {
                        var crc = CRC.ComputeCRC(entrysStream);
                        Assert.AreEqual(sourceEntry.Item2, crc);
                    }
                }

                foreach (string element in addedElements.Select(elem => elem.LocalPath))
                {
                    var elementName = Path.GetFileName(element);

                    // ensure that the entry which were added are there
                    Assert.IsTrue(entriesNames.Contains(elementName));
                }
            }
        }
    }
}
