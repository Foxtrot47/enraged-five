﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RSG.Pipeline.AssetProcessor.Operations;
using RSG.Pipeline.AssetProcessor.UnitTest.Utils;

namespace RSG.Pipeline.AssetProcessor.UnitTest.Copy
{
    [TestClass]
    public sealed class CopyTest : AssetProcessorTestBase
    {
        private const string OutputPath = @"./unit_test\output\copy";

        // duplication from RSG.Pipeline.AssetProcessor.Program.cs
        #region Tests initialization

        [ClassInitialize]
        public static void CopyTestInitialize(TestContext testContext)
        {
            // delete previous tests zip outputs
            // x:\gta5\cache\maps\dev_ng\asset_processor
            if (Directory.Exists(OutputPath))
            {
                Directory.Delete(OutputPath, true);
            }
        }

        [TestInitialize]
        public void TestInitialize()
        {
            string currentConfigPath;
            string currentLogName;
            // Depending on which test got executed, set the according config file
            switch (TestContext.TestName)
            {
                    // COPY Configs
                case "CopyAssetOneFile":
                    currentConfigPath = "./Copy/test_copy_single_inputs.xml";
                    currentLogName = "OneFile";
                    break;

                default:
                    throw new InternalTestFailureException("Invalid TestContext.TestName. The method name must be added to the switch/case in the Test.");
            }
            // TODO Flo: move the arguments into AssetProcessorTestBase as constants.
            string[] args = {"-config", currentConfigPath, "-branch", "dev_ng", "-logdir", "x:/gta5/tools_ng/logs/RSG.Pipeline.AssetProcessor.UnitTest", "-taskname", "Copy_Unit_Test"};

            Configuration(currentLogName, args);
        }

        [TestCleanup]
        public void TestCleanup()
        {

        }
        #endregion

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        #region Copy Tests
        [TestMethod]
        public void CopyAssetOneFile()
        {
            CopyAssetOperation operation = new CopyAssetOperation();

            bool result = operation.Run(m_task, m_commandOptions, m_log, m_inputs, m_outputAsset);
            Assert.IsTrue(result);

            WriteArchive();

            ValidateOutput(m_inputs);
        }
        #endregion

        #region Output Validation methods
        private void ValidateOutput(IEnumerable<Tuple<string, ZipArchive>> inputs)
        {
            // Open every input, generate a CRC for the files in it
            // Open the output, compare output's files CRC to the one from the input
            // ???
            // profit !

            var inputValues = new Dictionary<string, Tuple<long, string>>();

            foreach (Tuple<string, ZipArchive> input in inputs)
            {
                using (ZipArchive archive = new ZipArchive(File.OpenRead(input.Item1)))
                {
                    foreach (ZipArchiveEntry entry in archive.Entries)
                    {
                        using (Stream entryStream = entry.Open())
                        {
                            inputValues.Add(entry.FullName, new Tuple<long, string>(entry.Length, CRC.ComputeCRC(entryStream)));
                        }
                    }
                }
            }

            using (ZipArchive archive = new ZipArchive(File.OpenRead(m_config.Output.AbsolutePath)))
            {
                foreach (ZipArchiveEntry entry in archive.Entries)
                {
                    Tuple<long, string> sourceEntry;
                    Assert.IsTrue(inputValues.TryGetValue(entry.FullName, out sourceEntry));

                    Assert.AreEqual(entry.Length, sourceEntry.Item1);

                    using (Stream entrysStream = entry.Open())
                    {
                        var crc = CRC.ComputeCRC(entrysStream);
                        Assert.AreEqual(sourceEntry.Item2, crc);
                    }
                }
            }
        }
        #endregion

        #region IDisposable Implementation
        #endregion
    }
}

#region Idea's graveyard
//// Instantiate operations based on Reflection
//Activator.CreateInstance(Task.Assembly, Task.Type)
//<Task operation="Copy" name="01_one_to_one_file_copy" assembly="RSG.Pipeline.AssetProcessor.CopyAsset" type="CopyAssetOperation"/>
#endregion