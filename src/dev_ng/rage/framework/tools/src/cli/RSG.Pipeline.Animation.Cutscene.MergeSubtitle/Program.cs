﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using RSG.Base.Configuration;
using RSG.Base.OS;
using RSG.Pipeline.Core;
using RSG.Base.Logging.Universal;
using RSG.Base.Logging;

namespace RSG.Pipeline.Animation.Cutscene.FinaliseCutfile
{
    class Program
    {
        #region Constants
        private static readonly String OPTION_INPUTFILE = "inputfile";
        private static readonly String OPTION_OUTPUTFILE = "outputfile";
        private static readonly String OPTION_SCENENAME = "scenename";
        private static readonly String OPTION_SUB_MERGE = "cutfsubfix";
        private static readonly String OPTION_ASSETS_DIR = "assets";
        private static readonly String OPTION_TASK_NAME = "taskname";
        private static readonly String LOG_CTX = "Merge Subtitle";
        #endregion // Constants

        static int RunProcess(string strExe, string strArgs, IUniversalLog log)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.Arguments = strArgs;
            startInfo.FileName = strExe;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.CreateNoWindow = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false;

            log.MessageCtx(LOG_CTX, strExe + " " + strArgs);

            Process proc = Process.Start(startInfo);
            String output = proc.StandardOutput.ReadToEnd();
            proc.WaitForExit();

            if (output != String.Empty)
                log.MessageCtx(LOG_CTX, output);

            return proc.ExitCode;
        }

        [STAThread]
        static int Main(string[] args)
        {
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.CreateUniversalLog("MergeSubtitle");
            try
            {
                // Define our executable options.  Stick with the long options
                // (e.g. --opt) rather than the single character versions.
                LongOption[] opts = new LongOption[] {
                new LongOption(OPTION_INPUTFILE, LongOption.ArgType.Required,
                    "Input filename."),
                new LongOption(OPTION_OUTPUTFILE, LongOption.ArgType.Required,
                    "Output filename."),
                new LongOption(OPTION_SCENENAME, LongOption.ArgType.Required,
                    ""),
                new LongOption(OPTION_SUB_MERGE, LongOption.ArgType.Required,
                    ""),
                new LongOption(OPTION_ASSETS_DIR, LongOption.ArgType.Required,
                    ""),
                new LongOption(OPTION_TASK_NAME, LongOption.ArgType.Required,
                    "Asset Pipeline 3 Task Name."),
                };

                CommandOptions options = new CommandOptions(args, opts);

                // Turn args into vars
                String inputFile = options[OPTION_INPUTFILE] as String;
                String outputFile = options[OPTION_OUTPUTFILE] as String;
                String sceneName = options[OPTION_SCENENAME] as String;
                String cutfSubFixExe = options[OPTION_SUB_MERGE] as String;
                String assetsDir = options[OPTION_ASSETS_DIR] as String;
                String taskName = options[OPTION_TASK_NAME] == null ? String.Empty : options[OPTION_TASK_NAME] as String;

                if (String.Empty != taskName)
                {
                    Console.WriteLine("TASK: {0}", taskName);
                    log.MessageCtx(LOG_CTX, "TASK: {0}", taskName);
                }

                String outputDirectory = Path.GetDirectoryName(outputFile);
                //log.MessageCtx(LOG_CTX, "Creating directory '{0}'", outputDirectory);
                //if (!RSG.Pipeline.Services.Animation.DirectoryServices.CreateDirectory(outputDirectory))
                //{
                //    log.ErrorCtx(LOG_CTX, "Failed to create directory: '{0}'.  Aborting.", outputDirectory);
                //    return (Constants.Exit_Failure);
                //}

                String cutSubFile = String.Format("{0}/cuts/!!cutsubs//{1}.cutsub", assetsDir, sceneName);

                if (!File.Exists(cutSubFile))
                {
                    if (File.Exists(inputFile))
                    {
                        log.MessageCtx(LOG_CTX, "Subtitle file '{0}' not found, copying file '{1}' to '{2}'", cutSubFile, inputFile, outputFile);
                        // Not all scenes have sub data, if not just copy the file
                        File.Copy(inputFile, outputFile, true);
                    }
                    else
                    {
                        log.ErrorCtx(LOG_CTX, "File '{0}' does not exist", inputFile);
                        if (Directory.Exists(Path.GetDirectoryName(outputFile)))
                            Directory.Delete(Path.GetDirectoryName(outputFile), true);
                        return (Constants.Exit_Failure);
                    }
                }
                else
                {
                    log.MessageCtx(LOG_CTX, "Merging subtitle file '{0}'", cutSubFile);

                    string cutfSubFixArgs = String.Format("-cutSub \"{0}\" -cutFile \"{1}\" -outputCutfile \"{2}\" -hideAlloc -nopopups",
                        cutSubFile, inputFile, outputFile);
                    if (RunProcess(cutfSubFixExe, cutfSubFixArgs, log) != 0)
                    {
                        if (Directory.Exists(Path.GetDirectoryName(outputFile)))
                            Directory.Delete(Path.GetDirectoryName(outputFile), true);
                        return (Constants.Exit_Failure);
                    }
                }

                return (Constants.Exit_Success);
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Unhandled Exception during Merge Subtitle Process");
                return (Constants.Exit_Failure);
            }
            finally
            {
                LogFactory.ApplicationShutdown();
            }
        }
    }
}
