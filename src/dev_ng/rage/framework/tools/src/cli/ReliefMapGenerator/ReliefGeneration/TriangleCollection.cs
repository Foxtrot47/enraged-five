﻿using System;
using System.Collections.Generic;
using System.Linq;
using RSG.Base.Math;

namespace ReliefGeneration
{
    /// <summary>
    /// Collection of triangles.
    /// </summary>
    internal class TriangleCollection : ICollection<Triangle>
    {
        #region Private member fields

        private List<Triangle> m_triangles;
        private Dictionary<int, List<Triangle>> m_indexMapping;

        #endregion

        #region Public properties

        /// <summary>
        /// The number of items in the collection.
        /// </summary>
        public int Count { get { return m_triangles.Count; } }

        /// <summary>
        /// True if read-only. Always returns false.
        /// </summary>
        public bool IsReadOnly { get { return false; } }

        /// <summary>
        /// Minimum point of the bounding box.
        /// </summary>
        public Vector3f Minimum { get; private set; }

        /// <summary>
        /// Maximum point of the bounding box.
        /// </summary>
        public Vector3f Maximum { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public TriangleCollection()
            : this(null)
        {
            Minimum = new Vector3f(float.MaxValue, float.MaxValue, float.MaxValue);
            Maximum = new Vector3f(float.MinValue, float.MinValue, float.MinValue);
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="collection">Initial triangle collection.</param>
        public TriangleCollection(ICollection<Triangle> collection)
        {
            m_triangles = new List<Triangle>();
            m_indexMapping = new Dictionary<int, List<Triangle>>();

            if (collection != null)
            {
                foreach (var t in collection)
                {
                    Add(t);
                }
            }
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Add a collection of triangles.
        /// </summary>
        /// <param name="triangles">Triangle enumeration.</param>
        public void AddRange(IEnumerable<Triangle> triangles)
        {
            foreach (var t in triangles)
            {
                Add(t);
            }
        }

        /// <summary>
        /// Returns an enumeration of all the neighbouring triangles.
        /// </summary>
        /// <param name="triangle">Triangle.</param>
        /// <returns>An enumeration of all the neighbouring triangles.</returns>
        public IEnumerable<Triangle> GetNeighbouringTriangles(Triangle triangle)
        {
            List<Triangle> triangles = new List<Triangle>();
            HashSet<Triangle> uniqueSet = new HashSet<Triangle>();

            foreach (int idx in triangle.Indices)
            {
                foreach (var t in m_indexMapping[idx])
                {
                    uniqueSet.Add(t);
                }
            }

            triangles.AddRange(uniqueSet.AsEnumerable());
            return triangles;
        }

        /// <summary>
        /// Add a triangle to the collection.
        /// </summary>
        /// <param name="t">Triangle.</param>
        public void Add(Triangle t)
        {
            m_triangles.Add(t);

            CheckMaxMin(t);

            foreach (int idx in t.Indices)
            {
                if (!m_indexMapping.ContainsKey(idx))
                {
                    m_indexMapping.Add(idx, new List<Triangle>());
                }

                m_indexMapping[idx].Add(t);
            }
        }

        /// <summary>
        /// Clear the collection.
        /// </summary>
        public void Clear()
        {
            foreach (int idx in m_indexMapping.Keys)
            {
                m_indexMapping[idx].Clear();
            }

            m_indexMapping.Clear();
            m_triangles.Clear();
        }

        /// <summary>
        /// Returns true if the collection contains the item.
        /// </summary>
        /// <param name="item">Triangle.</param>
        /// <returns>Returns true if the collection contains the item.</returns>
        public bool Contains(Triangle item)
        {
            return m_triangles.Contains(item);
        }

        /// <summary>
        /// Copy to an array.
        /// </summary>
        /// <param name="array">Target array.</param>
        /// <param name="arrayIndex">Start index for copy.</param>
        public void CopyTo(Triangle[] array, int arrayIndex)
        {
            for (int i = 0; i < m_triangles.Count; i++)
            {
                array.SetValue(m_triangles[i], arrayIndex + i);
            }
        }

        /// <summary>
        /// Removes an item from the collection.
        /// </summary>
        /// <param name="item">Item to remove.</param>
        /// <returns>True if the item was successfully removed.</returns>
        public bool Remove(Triangle item)
        {
            bool result = m_triangles.Remove(item);
            if (result)
            {
                foreach (int key in m_indexMapping.Keys)
                {
                    if (m_indexMapping[key].Contains(item))
                    {
                        m_indexMapping[key].Remove(item);
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Get an enumerable list of triangles.
        /// </summary>
        /// <returns>An enumerated list of triangles.</returns>
        public IEnumerator<Triangle> GetEnumerator()
        {
            foreach (Triangle t in m_triangles)
            {
                yield return t;
            }
        }

        /// <summary>
        /// Get an enumerable list of triangles.
        /// </summary>
        /// <returns>An enumerated list of triangles.</returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }


        #endregion

        #region Private helper methods

        /// <summary>
        /// Checks and sets the maximum and minimum values for the bounding box of this collection.
        /// </summary>
        /// <param name="triangle">Triangle.</param>
        private void CheckMaxMin(Triangle triangle)
        {
            for (int i = 0; i < triangle.Points.Length; i++)
            {
                Minimum.X = Math.Min(triangle.Points[i].X, Minimum.X);
                Minimum.Y = Math.Min(triangle.Points[i].Y, Minimum.Y);
                Minimum.Z = Math.Min(triangle.Points[i].Z, Minimum.Z);
            }

            for (int i = 0; i < triangle.Points.Length; i++)
            {
                Maximum.X = Math.Max(triangle.Points[i].X, Maximum.X);
                Maximum.Y = Math.Max(triangle.Points[i].Y, Maximum.Y);
                Maximum.Z = Math.Max(triangle.Points[i].Z, Maximum.Z);
            }
        }

        #endregion
    }
}
