﻿using System;
using System.Linq;
using System.IO;
using RSG.Base.ConfigParser;
using RSG.Base.Math;

namespace ReliefGeneration
{
    /// <summary>
    /// Relief map data generator.
    /// </summary>
    public class ReliefDataGenerator
    {
        #region Private member fields

        private string m_sectionZip;
        private string m_outputDir;
        private string m_outFile;
        private float m_stepValue;

        private string m_mapSection; // current map section
        private TextWriter m_writer;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="sectionZipFile">Section zip file.</param>
        /// <param name="outputDir">Output directory.</param>
        /// <param name="stepValue">The step value in metres for the probe.</param>
        public ReliefDataGenerator(string sectionZipFile, string outputDir, float stepValue)
        {
            m_sectionZip = sectionZipFile;
            m_outputDir = outputDir;
            m_stepValue = stepValue; // step value in metres.

            string filename = Path.GetFileNameWithoutExtension(sectionZipFile);
            m_outFile = Path.Combine(m_outputDir, filename + ".csv");
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Generate the relief data.
        /// </summary>
        /// <param name="writeTris">Write out the triangle data.</param>
        /// <returns>True if successful.</returns>
        public bool GenerateData(bool writeTris)
        {
            bool result = false;

            using (ConfigGameView gv = new ConfigGameView())
            {
                ContentNodeMapProcessedZip zipToProcess = FindZipFile(gv);
                if (zipToProcess == null)
                {
                    return false;
                }

                result = GenerateData(zipToProcess, writeTris);
            }

            return result;
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Generate the relief data.
        /// </summary>
        /// <param name="zipNode">The zip file entry.</param>
        /// <param name="writeTris">Write out the triangle data too.</param>
        /// <returns>True if successful.</returns>
        private bool GenerateData(ContentNodeMapProcessedZip zipNode, bool writeTris)
        {
            if (File.Exists(m_outFile))
            {
                File.Delete(m_outFile);
            }

            using (Stream s = File.Open(m_outFile, FileMode.Append, FileAccess.Write, FileShare.Read))
            {
                using (m_writer = new StreamWriter(s))
                {
                    m_mapSection = Path.GetFileNameWithoutExtension(zipNode.Filename);

                    TriangleCollection triangles = new TriangleCollection();
                    ProcessZip(zipNode, triangles);
                    WriteHeader(triangles);

                    RaycastProcessor processor = new RaycastProcessor();
                    processor.PointHit += new RayProcessorEventHandler(Processor_WritePoint);
                    processor.Process(triangles, m_stepValue);
                    processor.PointHit -= Processor_WritePoint;


                    if (writeTris)
                    {
                        string folderName = Path.GetDirectoryName(m_outFile);
                        string fileName = Path.GetFileNameWithoutExtension(m_outFile);
                        string triFile = Path.Combine(folderName, fileName + ".tri");

                        WriteTriangles(triFile, triangles);
                    }

                }
            }

            return true;
        }

        /// <summary>
        /// Write out the triangle soup data.
        /// </summary>
        /// <param name="triangleFile">Triangle file.</param>
        /// <param name="triangles">Triangle soup.</param>
        private void WriteTriangles(string triangleFile, TriangleCollection triangles)
        {
            File.Delete(triangleFile);

            using (Stream s = File.Open(triangleFile, FileMode.Append, FileAccess.Write, FileShare.Read))
            {
                using (m_writer = new StreamWriter(s))
                {
                    foreach (Triangle t in triangles)
                    {
                        m_writer.WriteLine(String.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8}", t.Points[0].X, t.Points[0].Y, t.Points[0].Z, t.Points[1].X, t.Points[1].Y, t.Points[1].Z, t.Points[2].X, t.Points[2].Y, t.Points[2].Z));
                    }
                }
            }
        }

        /// <summary>
        /// Find the correct zip file.
        /// </summary>
        /// <param name="gv">Game configuration.</param>
        /// <returns>The processed zip file.</returns>
        private ContentNodeMapProcessedZip FindZipFile(ConfigGameView gv)
        {
            string fileName = Path.GetFileName(m_sectionZip);

            ContentNodeMapProcessedZip zipToProcess = null;

            var zipNodes = gv.Content.Root.FindAll("processedmapzip");

            foreach (var zipNode in zipNodes.OfType<ContentNodeMapProcessedZip>())
            {
                if (zipNode.Filename.EndsWith(fileName, StringComparison.OrdinalIgnoreCase))
                {
                    zipToProcess = zipNode;
                    break;
                }
            }

            return zipToProcess;
        }

        /// <summary>
        /// Process the zip file.
        /// </summary>
        /// <param name="zipNode">Zip node.</param>
        /// <param name="collection">Triangle collection.</param>
        private void ProcessZip(ContentNodeMapProcessedZip zipNode, TriangleCollection collection)
        {
            ProcessedZipFile zipFile = new ProcessedZipFile(zipNode.Filename);
            if (zipFile.GetTriangles())
            {
                foreach (var bucket in zipFile.TriangleSoup.GetEnumerator())
                {
                    foreach (var tri in bucket)
                    {
                        collection.Add(tri);
                    }
                }
            }
        }

        #endregion

        #region CSV write methods

        /// <summary>
        /// Write the header line.
        /// </summary>
        /// <param name="triangles">Triangle collection.</param>
        private void WriteHeader(TriangleCollection triangles)
        {
            m_writer.WriteLine(".section {0},{1},{2},{3},{4},{5},{6}", m_mapSection, triangles.Minimum.X, triangles.Minimum.Y, triangles.Minimum.Z, triangles.Maximum.X, triangles.Maximum.Y, triangles.Maximum.Z);
        }

        /// <summary>
        /// Write the point to CSV.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Ray processor arguments.</param>
        private void Processor_WritePoint(object sender, RayProcessorArgs e)
        {
            WriteToCSV(e.Point, e.IsHit);
        }

        /// <summary>
        /// Write the point to the CSV.
        /// </summary>
        /// <param name="point">Point.</param>
        /// <param name="isHit">True if this point represents a 'hit'.</param>
        private void WriteToCSV(Vector3f point, bool isHit)
        {
            int value = isHit ? 1 : 0;
            m_writer.WriteLine("{0},{1},{2},{3}", isHit, point.X, point.Y, point.Z);
        }

        #endregion
    }
}
