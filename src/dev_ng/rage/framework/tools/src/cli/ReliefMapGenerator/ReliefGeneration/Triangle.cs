﻿using RSG.Base.Math;

namespace ReliefGeneration
{
    /// <summary>
    /// Triangle class contains at most three points in space.
    /// </summary>
    internal class Triangle
    {
        #region Public properties

        /// <summary>
        /// Gets the three points that construct the triangle.
        /// </summary>
        public Vector3f[] Points
        {
            get;
            private set;
        }

        /// <summary>
        /// Indices.
        /// </summary>
        public int[] Indices
        {
            get;
            private set;
        }

        /// <summary>
        /// Material index.
        /// </summary>
        public int MaterialIndex
        {
            get;
            private set;
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="materialIndex">Material index.</param>
        /// <param name="p1">Point 1.</param>
        /// <param name="p2">Point 2.</param>
        /// <param name="p3">Point 2.</param>
        /// <param name="idx0"></param>
        /// <param name="idx1"></param>
        /// <param name="idx2"></param>
        internal Triangle(int materialIndex, Vector3f p1, Vector3f p2, Vector3f p3, int idx0, int idx1, int idx2)
        {
            MaterialIndex = materialIndex;
            Points = new Vector3f[] { p1, p2, p3 };
            Indices = new int[] { idx0, idx1, idx2 };
        }

        #endregion
    }
}
