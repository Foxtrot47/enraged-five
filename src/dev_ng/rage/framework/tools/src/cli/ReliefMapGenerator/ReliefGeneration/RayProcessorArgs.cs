﻿using System;
using RSG.Base.Math;

namespace ReliefGeneration
{
    /// <summary>
    /// Ray processor event delegate.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="e">Event arguments.</param>
    internal delegate void RayProcessorEventHandler(object sender, RayProcessorArgs e);

    internal class RayProcessorArgs : EventArgs
    {
        #region Public properties

        /// <summary>
        /// The point in space.
        /// </summary>
        public Vector3f Point { get; private set; }

        /// <summary>
        /// Set to true if the point touches a triangle.
        /// </summary>
        public bool IsHit { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="point">The point in space. This can be the origin of the bean when isHit is false, or the point in space where the ray touches a triangle with isHit is true.</param>
        /// <param name="isHit">Set to true if the point touches a triangle.</param>
        public RayProcessorArgs(Vector3f point, bool isHit)
        {
            Point = point;
            IsHit = isHit;
        }

        #endregion
    }
}
