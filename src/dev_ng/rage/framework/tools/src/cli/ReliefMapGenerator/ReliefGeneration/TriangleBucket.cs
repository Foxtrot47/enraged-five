﻿using System.Collections.Generic;

namespace ReliefGeneration
{
    /// <summary>
    /// Simple collection of triangles.
    /// </summary>
    internal class TriangleBucket
    {
        #region Private member fields

        private List<Bucket<Triangle>> m_buckets;

        #endregion

        #region Public properties

        /// <summary>
        /// Number of triangles.
        /// </summary>
        public int Count { get { return m_buckets.Count; } }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public TriangleBucket()
        {
            m_buckets = new List<Bucket<Triangle>>();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Add a list of triangles.
        /// </summary>
        /// <param name="triangles">Triangles.</param>
        public void Add(List<Triangle> triangles)
        {
            var bucket = new Bucket<Triangle>();
            foreach (var t in triangles)
            {
                bucket.Add(t);
            }

            m_buckets.Add(bucket);
        }

        /// <summary>
        /// Get the an enumeration of the items in the collection.
        /// </summary>
        /// <returns>Enumerable collection of triangles.</returns>
        public IEnumerable<Bucket<Triangle>> GetEnumerator()
        {
            foreach (var bucket in m_buckets)
            {
                yield return bucket;
            }
        }

        #endregion
    }
}
