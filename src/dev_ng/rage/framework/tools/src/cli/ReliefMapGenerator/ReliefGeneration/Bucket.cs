﻿using System.Collections.Generic;
using System.Linq;

namespace ReliefGeneration
{
    /// <summary>
    /// Unique set of items.
    /// </summary>
    /// <typeparam name="T">Type.</typeparam>
    [System.Diagnostics.DebuggerDisplay("Count = {Count}")]
    internal class Bucket<T> : ICollection<T>
    {
        #region Private member fields

        private HashSet<T> m_items;

        #endregion

        #region Public properties

        /// <summary>
        /// The number of items in the collection.
        /// </summary>
        public int Count { get { return m_items.Count; } }

        /// <summary>
        /// True if the collection is read-only. Bucket is always writable, so this returns false.
        /// </summary>
        public bool IsReadOnly { get { return false; } }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        public Bucket()
        {
            m_items = new HashSet<T>();
        }

        #endregion

        #region Public methods

        public void Add(T item)
        {
            m_items.Add(item);
        }

        public void Clear()
        {
            m_items.Clear();
        }

        public bool Contains(T item)
        {
            return m_items.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            int i = 0;

            foreach (var item in m_items.AsEnumerable<T>())
            {
                array.SetValue(item, arrayIndex + i);
                i++;
            }
        }

        public bool Remove(T item)
        {
            bool result = m_items.Remove(item);
            return result;
        }

        public IEnumerable<T> GetItems()
        {
            foreach (var item in m_items)
            {
                yield return item;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            foreach (var triEdge in m_items)
            {
                yield return triEdge;
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}
