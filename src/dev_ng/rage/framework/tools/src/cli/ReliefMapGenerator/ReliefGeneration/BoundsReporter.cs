﻿using System;
using System.Linq;
using RSG.Base.ConfigParser;

namespace ReliefGeneration
{
    #region Event handler and arguments for bounds metric report

    /// <summary>
    /// Bounds event handler.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="e">Bounds event arguments.</param>
    public delegate void BoundsEventHandler(object sender, BoundsEventArgs e);

    public class BoundsEventArgs : EventArgs
    {
        #region Public properties

        /// <summary>
        /// The zip file.
        /// </summary>
        public string ZipFile{get;private set;}

        /// <summary>
        /// The number of box primitives.
        /// </summary>
        public int BoxCount {get;private set;}

        /// <summary>
        /// The number of triangles.
        /// </summary>
        public int TriangleCount {get;private set;}

        /// <summary>
        /// The number of sphere primitives.
        /// </summary>
        public int SphereCount { get; private set; }

        /// <summary>
        /// The number of cylinder primitives.
        /// </summary>
        public int CylinderCount { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="zipFile">The zip file.</param>
        /// <param name="boxCount">The number of box primitives.</param>
        /// <param name="triCount">The number of triangles.</param>
        /// <param name="sphereCount">The number of sphere primitives.</param>
        /// <param name="cylinderCount">The number of cylinder primitives.</param>
        public BoundsEventArgs(string zipFile, int boxCount, int triCount, int sphereCount, int cylinderCount)
        {
            ZipFile = zipFile;
            BoxCount = boxCount;
            TriangleCount = triCount;
            SphereCount = sphereCount;
            CylinderCount = cylinderCount;
        }

        #endregion

        #region Public overrides

        /// <summary>
        /// Display the contents of the class as a string.
        /// </summary>
        /// <returns>String representation of the class.</returns>
        public override string ToString()
        {
            return String.Format("{0}\r\n\tBoxes : {1}\r\n\tTriangles : {2}\r\n\tSpheres : {3}\r\n\tCylinders : {4}", ZipFile, BoxCount, TriangleCount, SphereCount, CylinderCount);
        }

        #endregion
    }

    #endregion

    /// <summary>
    /// Bounds reporter. Details the types of bounding objects are in each zip file.
    /// </summary>
    public class BoundsReporter
    {
        #region Private member fields

        private string m_level;

        #endregion

        #region Public events

        /// <summary>
        /// Zip file has been parsed event.
        /// </summary>
        public event BoundsEventHandler ZipFile;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="level">Game level.</param>
        public BoundsReporter(string level)
        {
            m_level = level;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Parse every zip file in the given level.
        /// </summary>
        public void Parse()
        {
            using (ConfigGameView gv = new ConfigGameView())
            {
                var zipNodes = gv.Content.Root.FindAll("processedmapzip");
                foreach (var zipNode in zipNodes.OfType<ContentNodeMapProcessedZip>())
                {
                    if (zipNode.Filename.Contains(m_level))
                    {
                        Process(zipNode);
                    }
                }
            }
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Process a single zip file.
        /// </summary>
        /// <param name="zipNode">Zip file entry.</param>
        private void Process(ContentNodeMapProcessedZip zipNode)
        {
            int boxCount=0;
            int triCount=0;
            int sphereCount = 0;
            int cylinderCount = 0;

            ProcessedZipFile zipFile = new ProcessedZipFile(zipNode.Filename);
            if (zipFile.TryGetMetrics(ref boxCount, ref triCount, ref sphereCount, ref cylinderCount))
            {
                if(ZipFile!=null)
                {
                    ZipFile(this, new BoundsEventArgs(zipNode.Filename, boxCount, triCount, sphereCount, cylinderCount));
                }
            }
        }

        #endregion
    }
}
