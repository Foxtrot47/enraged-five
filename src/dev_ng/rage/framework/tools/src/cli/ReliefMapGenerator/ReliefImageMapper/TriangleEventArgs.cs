﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Base.Math;
using System.Drawing;

namespace ReliefImageMapper
{
    /// <summary>
    /// Triangle event delegate.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="e">Triangle event arguments.</param>
    internal delegate void TriangleEventHandler(object sender, TriangleEventArgs e);

    /// <summary>
    /// Triangle event arguments.
    /// </summary>
    internal class TriangleEventArgs : EventArgs
    {
        #region Public properties

        /// <summary>
        /// First point.
        /// </summary>
        public Vector3f P0 { get; private set; }

        /// <summary>
        /// Second point.
        /// </summary>
        public Vector3f P1 { get; private set; }

        /// <summary>
        /// Third point.
        /// </summary>
        public Vector3f P2 { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="line">Line from file.</param>
        public TriangleEventArgs(string line)
        {
            string[] split = line.Split(",".ToCharArray());
            P0 = ToVector3f(split, 0);
            P1 = ToVector3f(split, 3);
            P2 = ToVector3f(split, 6);
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Converts a 3D point to 2D.
        /// </summary>
        /// <param name="point">Point in 3D space.</param>
        /// <returns>Point.</returns>
        public Point ToPoint(Vector3f point)
        {
            return new Point((int)point.X, -(int)point.Y);
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Convert an array of strings to a Vector3f using an offset.
        /// </summary>
        /// <param name="values">String values.</param>
        /// <param name="offset">Offset into the array.</param>
        /// <returns>The vector stored at offset in the string array.</returns>
        private Vector3f ToVector3f(string[] values, int offset)
        {
            float x = float.Parse(values[offset]);
            float y = float.Parse(values[offset + 1]);
            float z = float.Parse(values[offset + 2]);
            return new Vector3f(x, y, z);
        }

        #endregion
    }
}
