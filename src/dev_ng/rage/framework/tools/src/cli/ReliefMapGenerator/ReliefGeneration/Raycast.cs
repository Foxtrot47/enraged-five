﻿using System;
using System.Collections.Generic;
using RSG.Base.Math;

namespace ReliefGeneration
{
    /// <summary>
    /// Ray cast implementation.
    /// </summary>
    internal static class Raycast
    {
        #region Constant value

        private static readonly float SMALL_NUM = 0.00001f;

        #endregion

        #region Public methods

        /// <summary>
        /// Ray cast check against a series of vertices.
        /// </summary>
        /// <param name="ray">Ray.</param>
        /// <param name="verts">Vertices</param>
        /// <param name="vecIntersect">Vector that intersected the vertices.</param>
        /// <returns>True if the ray intersected the triangle.</returns>
        public static bool RayIntersectsTriangle(Ray ray, IEnumerable<Vector3f> verts, ref Vector3f vecIntersect)
        {
            Vector3f u = new Vector3f();
            Vector3f v = new Vector3f();
            Vector3f n = new Vector3f();
            Vector3f dir = new Vector3f();
            Vector3f w0 = new Vector3f();
            Vector3f w = new Vector3f();
            float r, a, b;

            List<Vector3f> vertices = new List<Vector3f>() { null, null, null };
            int index = 2;
            foreach (var vec in verts)
            {
                vertices[index] = vec;
                index--;
            }

            u = vertices[1] - vertices[0];
            v = vertices[2] - vertices[0];
            n = Vector3f.Cross(u, v);
            if (Mag2(n) < SMALL_NUM)
            {
                return false;
            }

            dir = ray.End - ray.Start;
            w0 = ray.Start - vertices[0];
            a = -Vector3f.Dot(n, w0);
            b = Vector3f.Dot(n, dir);
            if (Math.Abs(b) < SMALL_NUM)
            {
                // NB : Could be parallel
                return false;
            }

            r = a / b;
            if (r < 0.0f || r > 1.0f)
            {
                return false;
            }

            vecIntersect = ray.Start + (dir * r);

            float uu, uv, vv, wu, wv, D;
            uu = Vector3f.Dot(u, u);
            uv = Vector3f.Dot(u, v);
            vv = Vector3f.Dot(v, v);
            w = vecIntersect - vertices[0];
            wu = Vector3f.Dot(w, u);
            wv = Vector3f.Dot(w, v);
            D = uv * uv - uu * vv;

            float s, t;
            s = (uv * wv - vv * wu) / D;
            if (s < 0.0f || s > 1.0f)
                return false;

            t = (uv * wu - uu * wv) / D;
            if (t < 0.0f || (s + t) > 1.0f)
                return false;

            return true;
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Magnitude.
        /// </summary>
        /// <param name="vec"></param>
        /// <returns></returns>
        static float Mag2(Vector3f vec)
        {
            return Dot3(vec.X, vec.X, vec.Y, vec.Y, vec.Z, vec.Z);
        }

        /// <summary>
        /// Dot product.
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="c"></param>
        /// <param name="d"></param>
        /// <param name="e"></param>
        /// <param name="f"></param>
        /// <returns></returns>
        static float Dot3(float a, float b, float c, float d, float e, float f)
        {
            return a * b + c * d + e * f;
        }

        #endregion
    }
}
