﻿using System;
using RSG.Base.Math;

namespace ReliefGeneration
{
    /// <summary>
    /// Raycast processor.
    /// </summary>
    internal class RaycastProcessor
    {
        #region Public events

        /// <summary>
        /// Point hit event.
        /// </summary>
        public event RayProcessorEventHandler PointHit;

        #endregion

        #region Public methods

        /// <summary>
        /// Process the triangle data.
        /// </summary>
        /// <param name="triangles">Triangle collection.</param>
        /// <param name="stepValue">Step value in metres for the probe.</param>
        public void Process(TriangleCollection triangles, float stepValue)
        {
            float top = triangles.Maximum.Z + 100.0f;
            float diffX = stepValue;
            float diffY = stepValue;

            float dx = (triangles.Maximum.X - triangles.Minimum.X) / diffX;
            float dy = (triangles.Maximum.Y - triangles.Minimum.Y) / diffY;
            float iterations = dx * dy;
            int topperMost = (int)Math.Ceiling(iterations);
            bool oddRow = false;

            for (float y = triangles.Minimum.Y; y < triangles.Maximum.Y; y += diffY)
            {
                float xoff = 0.0f;

                if (oddRow)
                {
                    xoff = diffX / 2.0f;
                }

                for (float x = triangles.Minimum.X; x < triangles.Maximum.X; x += diffX)
                {
                    Vector3f start = new Vector3f(x + xoff, y, top);
                    Vector3f end = new Vector3f(x + xoff, y, -top);
                    Ray ray = new Ray(start, end);

                    Vector3f intersect = new Vector3f();
                    Triangle found = null;

                    foreach (Triangle t in triangles)
                    {
                        if (Raycast.RayIntersectsTriangle(ray, t.Points, ref intersect))
                        {
                            found = t;
                            break;
                        }
                    }

                    if (PointHit != null)
                    {
                        Vector3f point = found == null ? ray.Start : intersect;
                        RayProcessorArgs args = new RayProcessorArgs(point, (found != null));
                        PointHit(this, args);
                    }
                }

                oddRow = !oddRow;
            }
        }

        #endregion
    }
}
