﻿using System.IO;

namespace ReliefImageMapper
{
    /// <summary>
    /// Collates all the single files into a large single file.
    /// </summary>
    public class DataCollator
    {
        #region Private member fields

        private string m_outFile;
        private string m_dataDir;
        private TextWriter m_writer;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="outFile">The file to create.</param>
        /// <param name="dataDirectory">The folder containing the height map data in CSV format.</param>
        public DataCollator(string outFile, string dataDirectory)
        {
            m_outFile = outFile;
            m_dataDir = dataDirectory;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Collate the separate files into one giant file containing points and height colour data.
        /// </summary>
        /// <returns>True if successful.</returns>
        public bool Collate()
        {
            if (File.Exists(m_outFile))
            {
                try
                {
                    File.Delete(m_outFile);
                }
                catch(IOException)
                {
                    return false;
                }
                
            }
            
            ReliefMapper mapper = ReliefMapperFactory.CreateNoImage(m_dataDir);
            mapper.PointFound += new MapperEventHandler(Mapper_PointFound);

            using (Stream s = File.Open(m_outFile, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read))
            {
                using (m_writer = new StreamWriter(s))
                {
                    mapper.FindPoints();
                }
            }

            return true;
        }

        #endregion

        #region Event handlers

        /// <summary>
        /// Point found event handler.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void Mapper_PointFound(object sender, MapperEventArgs e)
        {
            m_writer.WriteLine(e.ToString());
        }

        #endregion
    }
}
