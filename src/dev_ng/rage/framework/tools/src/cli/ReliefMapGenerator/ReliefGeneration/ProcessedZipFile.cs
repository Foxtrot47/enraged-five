﻿using System.Linq;
using Ionic.Zip;
using System.IO;

namespace ReliefGeneration
{
    /// <summary>
    /// Processed map zip file.
    /// </summary>
    internal class ProcessedZipFile
    {
        private string m_filename;

        public TriangleBucket TriangleSoup
        {
            get;
            private set;
        }

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="fileName">File name.</param>
        internal ProcessedZipFile(string fileName)
        {
            m_filename = fileName;
            TriangleSoup = new TriangleBucket();
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Get the triangle soup from the zip files.
        /// </summary>
        /// <returns>True if successful.</returns>
        public bool GetTriangles()
        {
            using (ZipFile zipFile = ZipFile.Read(m_filename))
            {
                foreach (ZipEntry entry in zipFile.Entries.Where(ntry => ntry.FileName.EndsWith(".ibn.zip")))
                {
                    using (MemoryStream ms = new MemoryStream((int)entry.UncompressedSize))
                    {
                        entry.Extract(ms);
                        ms.Seek(0, SeekOrigin.Begin);
                        BoundsFile boundsFile = new BoundsFile(m_filename, ms);
                        if (boundsFile.TriangleSoup != null && boundsFile.TriangleSoup.Count > 0)
                        {
                            TriangleSoup.Add(boundsFile.TriangleSoup);
                        }
                    }
                }
            }

            return TriangleSoup.Count > 0;
        }

        /// <summary>
        /// Get the metrics for zip file.
        /// </summary>
        /// <param name="boxCount">Number of box collision meshes.</param>
        /// <param name="triCount">Number of triangles in the collision mesh.</param>
        /// <param name="sphereCount">Number of spheres in the collision meshes.</param>
        /// <param name="cylinderCount">Number of cylinders in the collision meshes.</param>
        /// <returns>True if successful.</returns>
        public bool TryGetMetrics(ref int boxCount, ref int triCount, ref int sphereCount, ref int cylinderCount)
        {
            boxCount = 0;
            triCount = 0;
            sphereCount = 0;
            cylinderCount = 0;

            using (ZipFile zipFile = ZipFile.Read(m_filename))
            {
                if (zipFile == null)
                {
                    return false;
                }

                foreach (ZipEntry entry in zipFile.Entries.Where(ntry => ntry.FileName.EndsWith(".ibn.zip")))
                {

                    if (entry != null)
                    {
                        using (MemoryStream ms = new MemoryStream((int)entry.UncompressedSize))
                        {
                            entry.Extract(ms);
                            ms.Seek(0, SeekOrigin.Begin);
                            BoundsFile boundsFile = new BoundsFile(m_filename, ms);
                            boxCount += boundsFile.BoxCount;
                            if (boundsFile.TriangleSoup != null)
                            {
                                triCount += boundsFile.TriangleSoup.Count;
                            }

                            sphereCount += boundsFile.SphereCount;
                            cylinderCount += boundsFile.CylinderCount;
                            
                        }
                    }
                }
            }

            return true;
        }

        #endregion
    }
}
