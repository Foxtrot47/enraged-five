﻿using RSG.Base.Math;

namespace ReliefGeneration
{
    /// <summary>
    /// Ray.
    /// </summary>
    internal class Ray
    {
        /// <summary>
        /// Start point of the ray.
        /// </summary>
        public Vector3f Start { get; set; }

        /// <summary>
        /// End point of the ray.
        /// </summary>
        public Vector3f End { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="start">Start point of the ray.</param>
        /// <param name="end">End point of the ray.</param>
        public Ray(Vector3f start, Vector3f end)
        {
            Start = start;
            End = end;
        }
    }
}
