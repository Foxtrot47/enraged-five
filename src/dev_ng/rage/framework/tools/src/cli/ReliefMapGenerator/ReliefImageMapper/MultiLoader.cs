﻿using System;
using System.IO;

namespace ReliefImageMapper
{
    /// <summary>
    /// Multiple file loader. 
    /// </summary>
    internal class MultiLoader : ILoader
    {
        #region Private member fields

        private string m_reliefDataFolder;

        #endregion

        #region Public events

        /// <summary>
        /// New section event.
        /// </summary>
        public event NewSectionEventHandler NewSectionFound;

        /// <summary>
        /// Relief point event.
        /// </summary>
        public event ReliefPointEventHandler ReliefPointFound;

        /// <summary>
        /// Triangle found event.
        /// </summary>
        public event TriangleEventHandler TriangleFound;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="reliefDataFolder">Data folder that contains the height map data for each section.</param>
        public MultiLoader(string reliefDataFolder)
        {
            m_reliefDataFolder = reliefDataFolder;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Parse each .csv file in the given folder and extract the data.
        /// </summary>
        public virtual void Parse()
        {
            //ParseTriangleFiles();
            ParseCSVFiles();
        }

        /// <summary>
        /// Parse the triangle data.
        /// </summary>
        private void ParseTriangleFiles()
        {
            string[] files = Directory.GetFiles(m_reliefDataFolder, "*.tri");
            foreach (string file in files)
            {
                using (Stream s = File.Open(file, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    using (TextReader reader = new StreamReader(s))
                    {
                        string line = String.Empty;
                        while ((line = reader.ReadLine()) != null)
                        {
                            if (TriangleFound != null)
                            {
                                TriangleEventArgs args = new TriangleEventArgs(line);
                                TriangleFound(this, args);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Parse the CSV data.
        /// </summary>
        private void ParseCSVFiles()
        {
            string[] files = Directory.GetFiles(m_reliefDataFolder, "*.csv");
            foreach (string file in files)
            {
                Loader loader = new Loader(file);
                loader.NewSectionFound += new NewSectionEventHandler(Loader_NewSectionFound);
                loader.ReliefPointFound += new ReliefPointEventHandler(Loader_ReliefPointFound);

                loader.Parse();

                loader.NewSectionFound -= Loader_NewSectionFound;
                loader.ReliefPointFound -= Loader_ReliefPointFound;
            }
        }

        #endregion

        #region Event handlers

        /// <summary>
        /// Point found event handler.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void Loader_ReliefPointFound(object sender, ReliefPointEventArgs e)
        {
            if (ReliefPointFound != null)
            {
                ReliefPointFound(this, e);
            }
        }

        /// <summary>
        /// New section found event handler.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void Loader_NewSectionFound(object sender, NewSectionEventArgs e)
        {
            if (NewSectionFound != null)
            {
                NewSectionFound(this, e);
            }
        }

        #endregion
    }
}
