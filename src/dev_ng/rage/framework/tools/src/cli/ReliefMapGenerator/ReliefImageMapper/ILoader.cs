﻿namespace ReliefImageMapper
{
    /// <summary>
    /// ILoader interface.
    /// </summary>
    internal interface ILoader
    {
        /// <summary>
        /// New section found event.
        /// </summary>
        event NewSectionEventHandler NewSectionFound;

        /// <summary>
        /// Relief point found event.
        /// </summary>
        event ReliefPointEventHandler ReliefPointFound;

        /// <summary>
        /// Triangle found event.
        /// </summary>
        event TriangleEventHandler TriangleFound;

        /// <summary>
        /// Parses the relief data.
        /// </summary>
        void Parse();
    }
}
