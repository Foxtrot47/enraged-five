﻿namespace ReliefImageMapper
{
    /// <summary>
    /// Relief mapper factory.
    /// </summary>
    public static class ReliefMapperFactory
    {
        /// <summary>
        /// Creates a multi-loader version of the relief mapper.
        /// </summary>
        /// <param name="folder">Folder that contains all the height map data.</param>
        /// <param name="outImageFile">The image file to render.</param>
        /// <returns>A relief mapper instance.</returns>
        public static ReliefMapper Create(string folder, string outImageFile)
        {
            var bounds = new BoundsLoader(folder);
            bounds.Parse();

            ILoader loader = new MultiLoader(folder);
            var reliefMapper = new ReliefMapper(loader, outImageFile);
            reliefMapper.SetMinMax(bounds.Minimum, bounds.Maximum);
            return reliefMapper;
        }

        /// <summary>
        /// Creates a no-image relief mapper. The caller should subscribe to the PointFound event.
        /// </summary>
        /// <param name="folder">Folder that contains all the height map data.</param>
        /// <returns>A relief mapper instance.</returns>
        public static ReliefMapper CreateNoImage(string folder)
        {
            ILoader loader = new MultiLoader(folder);
            var reliefMapper = new ReliefMapper(loader);
            return reliefMapper;
        }

        /// <summary>
        /// Creates a single file version of the relief mapper. 
        /// </summary>
        /// <param name="file">Single .csv file that contains height map data.</param>
        /// <param name="outImageFile">The image file to render.</param>
        /// <returns>A relief mapper instance.</returns>
        public static ReliefMapper CreateSingle(string file, string outImageFile)
        {
            ILoader loader = new Loader(file);
            return new ReliefMapper(loader, outImageFile);
        }
    }
}
