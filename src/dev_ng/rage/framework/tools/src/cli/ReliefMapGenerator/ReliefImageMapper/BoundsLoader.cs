﻿using System.IO;
using RSG.Base.Math;

namespace ReliefImageMapper
{
    /// <summary>
    /// The BoundsLoader reads in the individual files and determines the bounding box for all sections.
    /// </summary>
    internal class BoundsLoader : ILoader
    {
        #region Private member fields

        private string m_reliefDataFolder;

        private Vector3f m_max;
        private Vector3f m_min;

        #endregion

        #region Public properties

        /// <summary>
        /// Maximum point of the bounding box.
        /// </summary>
        public Vector3f Maximum { get { return m_max; } }

        /// <summary>
        /// Minimum point of the bounding box.
        /// </summary>
        public Vector3f Minimum { get { return m_min; } }

        #endregion

        #region Public events

        /// <summary>
        /// New section event.
        /// </summary>
        public event NewSectionEventHandler NewSectionFound;

        /// <summary>
        /// Relief point event.
        /// </summary>
        public event ReliefPointEventHandler ReliefPointFound;

        /// <summary>
        /// Triangle found event.
        /// </summary>
        public event TriangleEventHandler TriangleFound;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="reliefDataFolder">Height map data folder.</param>
        public BoundsLoader(string reliefDataFolder)
        {
            m_reliefDataFolder = reliefDataFolder;
            m_max = new Vector3f(float.MinValue, float.MinValue, float.MinValue);
            m_min = new Vector3f(float.MaxValue, float.MaxValue, float.MaxValue);
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Parse the files in the folder and determine the maximum and minimum bounds.
        /// </summary>
        public void Parse()
        {
            string[] files = Directory.GetFiles(m_reliefDataFolder, "*.csv");
            foreach (string file in files)
            {
                Loader loader = new Loader(file);
                loader.NewSectionFound += new NewSectionEventHandler(Loader_NewSectionFound);

                loader.Parse();

                loader.NewSectionFound -= Loader_NewSectionFound;
            }
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// New section found event handler.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void Loader_NewSectionFound(object sender, NewSectionEventArgs e)
        {
            Max(e.Maximum);
            Min(e.Minimum);

            if (NewSectionFound != null)
            {
                NewSectionFound(this, e);
            }
        }

        /// <summary>
        /// Determine minimum values.
        /// </summary>
        /// <param name="min">Potential minimum values.</param>
        private void Min(Vector3f min)
        {
            if (min.X < m_min.X)
            {
                m_min.X = min.X;
            }

            if (min.Y < m_min.Y)
            {
                m_min.Y = min.Y;
            }

            if (min.Z < m_min.Z)
            {
                m_min.Z = min.Z;
            }
        }

        /// <summary>
        /// Determine maximum values.
        /// </summary>
        /// <param name="max">Potential maximum values.</param>
        private void Max(Vector3f max)
        {
            if (max.X > m_max.X)
            {
                m_max.X = max.X;
            }

            if (max.Y > m_max.Y)
            {
                m_max.Y = max.Y;
            }

            if (max.Z > m_max.Z)
            {
                m_max.Z = max.Z;
            }
        }

        #endregion
    }
}
