﻿using System;
using RSG.Base.Math;
using System.Drawing;

namespace ReliefImageMapper
{
    /// <summary>
    /// Mapper event delegate.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="e">Mapper event arguments.</param>
    public delegate void MapperEventHandler(object sender, MapperEventArgs e);

    /// <summary>
    /// Mapper event arguments.
    /// </summary>
    public class MapperEventArgs : EventArgs
    {
        #region Public properties

        /// <summary>
        /// Point in space.
        /// </summary>
        public Vector3f Point { get; private set; }

        /// <summary>
        /// Height colour.
        /// </summary>
        public Color Colour { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="point">Point in space.</param>
        /// <param name="colour">Height colour.</param>
        public MapperEventArgs(Vector3f point, Color colour)
        {
            Point = point;
            Colour = colour;
        }

        #endregion

        #region Overrides

        /// <summary>
        /// Return the arguments as a string.
        /// </summary>
        /// <returns>The arguments as a string.</returns>
        public override string ToString()
        {
            return String.Format("{0},{1},{2},{3},{4},{5}", Point.X, Point.Y, Point.Z, Colour.R, Colour.G, Colour.B);
        }

        #endregion
    }
}
