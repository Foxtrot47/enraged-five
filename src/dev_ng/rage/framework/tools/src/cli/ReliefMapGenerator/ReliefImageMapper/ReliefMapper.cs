﻿using System;
using RSG.Base.Math;
using System.Drawing;
using System.Drawing.Imaging;

namespace ReliefImageMapper
{
    public class ReliefMapper
    {
        #region Private settings class

        /// <summary>
        /// Relief mapper settings.
        /// </summary>
        private class CurrentSettings
        {
            #region Public properties

            /// <summary>
            /// Map section name.
            /// </summary>
            public string MapSection { get; set; }

            /// <summary>
            /// Minimum bounding box point.
            /// </summary>
            public Vector3f Maximum { get; set; }

            /// <summary>
            /// Maximum bounding box point.
            /// </summary>
            public Vector3f Minimum { get; set; }

            /// <summary>
            /// Width of the image.
            /// </summary>
            public int ImageWidth { get; set; }

            /// <summary>
            /// Height of the image.
            /// </summary>
            public int ImageHeight { get; set; }

            #endregion

            #region Private properties

            /// <summary>
            /// The height difference.
            /// </summary>
            private float HeightDiff { get { return Maximum.Z - Minimum.Z; } }

            #endregion
            
            #region Public methods

            /// <summary>
            /// Calculates the ratio between the point and the height differential of the current bounding box.
            /// </summary>
            /// <param name="point">Point.</param>
            /// <returns>The ratio between the point and the height differential.</returns>
            public float CalcReliefRatio(Vector3f point)
            {
                float offsetHeight = point.Z < 0 ? 0 : point.Z;
                float ratio = (1 - (offsetHeight / Maximum.Z)) * 180.0f;
                return ratio;
            }

            #endregion
        }

        #endregion

        #region Private member fields

        private int m_numPoints;
        private int m_numHits;
        private int m_pointSize;

        private bool m_externalBoundsSet;

        private string m_outImageFile;

        private ILoader m_loader;
        private CurrentSettings m_settings;
        private Graphics m_graphics;

        #endregion

        #region Public enumeration

        /// <summary>
        /// Generate enumeration. Determines how the results are to be returned to the caller.
        /// </summary>
        public enum Generate
        {
            /// <summary>
            /// Generate an image file.
            /// </summary>
            Image,

            /// <summary>
            /// Return individual points and their colour data to the caller using the PointFound event.
            /// </summary>
            Event
        }

        #endregion

        #region Public events

        /// <summary>
        /// Point found event.
        /// </summary>
        public event MapperEventHandler PointFound;

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="loader"></param>
        /// <param name="outImageFile"></param>
        internal ReliefMapper(ILoader loader, string outImageFile)
        {
            m_outImageFile = outImageFile;

            m_settings = new CurrentSettings();
            m_externalBoundsSet = false;

            m_loader = loader;
            m_loader.NewSectionFound += new NewSectionEventHandler(Loader_NewSectionFound);
            m_loader.ReliefPointFound += new ReliefPointEventHandler(Loader_ReliefPointFound);
            m_loader.TriangleFound += new TriangleEventHandler(Loader_TriangleFound);
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="loader">Loader.</param>
        internal ReliefMapper(ILoader loader)
        {
            m_settings = new CurrentSettings();
            m_loader = loader;
            m_externalBoundsSet = false;
            m_loader.NewSectionFound += new NewSectionEventHandler(Loader_NewSectionFound);
            m_loader.ReliefPointFound += new ReliefPointEventHandler(Loader_FireReliefPointFound);
            m_loader.TriangleFound += new TriangleEventHandler(Loader_TriangleFound);
        }

        #endregion

        /// <summary>
        /// Set the maximum and minimum values.
        /// </summary>
        /// <param name="min">Minimum point of the bounding box.</param>
        /// <param name="max">Maximum point of the bounding box.</param>
        public void SetMinMax(Vector3f min, Vector3f max)
        {
            m_externalBoundsSet = true;
            m_settings.Minimum = min;
            m_settings.Maximum = max;
        }

        /// <summary>
        /// Create a relief map image.
        /// </summary>
        public void FindPoints()
        {
            m_loader.Parse();
            m_loader.ReliefPointFound -= Loader_FireReliefPointFound;
            m_loader.NewSectionFound -= Loader_NewSectionFound;
            m_loader.TriangleFound -= Loader_TriangleFound;
        }

        /// <summary>
        /// Create the collision coverage map.
        /// </summary>
        /// <param name="width">Width of the image in pixel.</param>
        /// <param name="dotSize">Size of each dot.</param>
        public void MakeImage(int width, float dotSize)
        {
            int height = CalcHeight(width);

            m_settings.ImageHeight = height;
            m_settings.ImageWidth = width;
            
            m_pointSize = dotSize < 1.0f ? 2 : (int)dotSize;

            Image img = new Bitmap(width, height);

            using (Graphics g = Graphics.FromImage(img))
            {
                g.FillRectangle(Brushes.Black, new Rectangle(0, 0, width, height));

                m_graphics = g;
                m_loader.Parse();
            }

            img.Save(m_outImageFile, ImageFormat.Png);

            m_loader.NewSectionFound -= Loader_NewSectionFound;
            m_loader.ReliefPointFound -= Loader_ReliefPointFound;
            m_loader.TriangleFound -= Loader_TriangleFound;
        }

        /// <summary>
        /// Calculate the height of the image based upon the given width.
        /// </summary>
        /// <param name="width">Width.</param>
        /// <returns>The height of the image.</returns>
        private int CalcHeight(int width)
        {
            float worldWidth = (m_settings.Maximum.X - m_settings.Minimum.X);
            float worldHeight = (m_settings.Maximum.Y - m_settings.Minimum.Y);
            float ratio = worldHeight / worldWidth;

            return (int)(width * ratio);
        }

        #region Loader event handlers

        /// <summary>
        /// Handle relief points.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Relief point data.</param>
        void Loader_ReliefPointFound(object sender, ReliefPointEventArgs e)
        {
            if (!e.IsHit)
            {
                return;
            }

            float actualX = e.Point.X;
            float actualY = e.Point.Y;

            float px = (actualX - m_settings.Minimum.X) / (m_settings.Maximum.X - m_settings.Minimum.X);
            float py = (actualY - m_settings.Minimum.Y) / (m_settings.Maximum.Y - m_settings.Minimum.Y);

            int x = (int)(px * m_settings.ImageWidth);
            int y = m_settings.ImageHeight - (int)(py * m_settings.ImageHeight);

            using (SolidBrush p = new SolidBrush(CalcReliefColour(e.Point)))
            {
                m_graphics.FillEllipse(p, new Rectangle(x, y, m_pointSize, m_pointSize));
            }
        }

        /// <summary>
        /// Handle the relief point event and pass it up to the caller using the PointFound event.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void Loader_FireReliefPointFound(object sender, ReliefPointEventArgs e)
        {
            m_numPoints++;
            m_numHits = e.IsHit ? m_numHits + 1 : m_numHits;

            if (!e.IsHit)
            {
                return;
            }

            Color colour = CalcReliefColour(e.Point);

            if (PointFound != null)
            {
                PointFound(this, new MapperEventArgs(e.Point, colour));
            }
        }

        /// <summary>
        /// Triangle found event handler.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event arguments.</param>
        private void Loader_TriangleFound(object sender, TriangleEventArgs e)
        {
            using(Pen p = new Pen(Brushes.White))
            {
                p.Width = 1;
                m_graphics.DrawLine(p, e.ToPoint(e.P0), e.ToPoint(e.P1));
                m_graphics.DrawLine(p, e.ToPoint(e.P1), e.ToPoint(e.P2));
                m_graphics.DrawLine(p, e.ToPoint(e.P2), e.ToPoint(e.P0));
            }
        }

        /// <summary>
        /// Handle new section.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">New section event arguments.</param>
        private void Loader_NewSectionFound(object sender, NewSectionEventArgs e)
        {
            if (m_externalBoundsSet)
            {
                return;
            }

            m_settings.MapSection = e.MapSection;
            m_settings.Minimum = e.Minimum;
            m_settings.Maximum = e.Maximum;
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Determine the relief map colour from the point in space.
        /// </summary>
        /// <param name="point">Point.</param>
        /// <returns>The height colour of the point in space.</returns>
        private Color CalcReliefColour(Vector3f point)
        {
            float ratio = m_settings.CalcReliefRatio(point);
            return ToColour(ratio, 1, 1);
        }

        /// <summary>
        /// Convert an HSV to colour.
        /// </summary>
        /// <param name="h">Hue.</param>
        /// <param name="s">Saturation.</param>
        /// <param name="v">Value.</param>
        /// <returns>Colour.</returns>
        private Color ToColour(float h, float s, float v)
        {
            int i;
            float f, p, q, t;
            if (s == 0)
            {
                return Color.FromArgb((int)(v * 255), (int)(v * 255), (int)(v * 255));// achromatic (grey)
            }

            h /= 60;			// sector 0 to 5
            i = (int)Math.Floor(h);
            f = h - i;			// factorial part of h
            p = v * (1 - s);
            q = v * (1 - s * f);
            t = v * (1 - s * (1 - f));
            switch (i)
            {
                case 0:
                    return ColorFromFloat(v, t, p);
                case 1:
                    return ColorFromFloat(q, v, p);
                case 2:
                    return ColorFromFloat(p, v, t);
                case 3:
                    return ColorFromFloat(p, q, v);
                case 4:
                    return ColorFromFloat(t, p, v);
                default:		// case 5:
                    return ColorFromFloat(v, p, q);
            }
        }

        /// <summary>
        /// Converts an RGB float tuple to a colour.
        /// </summary>
        /// <param name="r">Red.</param>
        /// <param name="g">Green.</param>
        /// <param name="b">Blue.</param>
        /// <returns>Colour.</returns>
        private Color ColorFromFloat(float r, float g, float b)
        {
            return Color.FromArgb(255, (int)(r * 255), (int)(g * 255), (int)(b * 255));
        }

        #endregion
    }
}
