﻿using System;
using RSG.Base.Math;

namespace ReliefImageMapper
{
    /// <summary>
    /// New section event delegate.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="e">New section event arguments.</param>
    internal delegate void NewSectionEventHandler(object sender, NewSectionEventArgs e);

    /// <summary>
    /// New section event arguments.
    /// </summary>
    internal class NewSectionEventArgs : EventArgs
    {
        #region Public properties

        /// <summary>
        /// Map section.
        /// </summary>
        public string MapSection { get; set; }

        /// <summary>
        /// Minimum point of the section's bounding box.
        /// </summary>
        public Vector3f Minimum { get; set; }

        /// <summary>
        /// Maximum point of the section's bounding box.
        /// </summary>
        public Vector3f Maximum { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="mapSection">Map section.</param>
        /// <param name="min">Minimum point of the section's bounding box.</param>
        /// <param name="max">Maximum point of the section's bounding box.</param>
        public NewSectionEventArgs(string mapSection, Vector3f min, Vector3f max)
        {
            MapSection = mapSection;
            Minimum = min;
            Maximum = max;
        }

        #endregion
    }
}
