﻿using System;
using System.Collections.Generic;
using System.IO;
using RSG.Base.Math;

namespace ReliefImageMapper
{
    /// <summary>
    /// Simple loader reads in the specially formatted .csv files that contain section and point data.
    /// </summary>
    internal class Loader : ILoader
    {
        #region Private member fields

        private string m_filename;
        private ParseType m_parseType;

        #endregion

        #region Public events

        /// <summary>
        /// New section found event.
        /// </summary>
        public event NewSectionEventHandler NewSectionFound;

        /// <summary>
        /// Relief point found event.
        /// </summary>
        public event ReliefPointEventHandler ReliefPointFound;

        /// <summary>
        /// Triangle found event.
        /// </summary>
        public event TriangleEventHandler TriangleFound;

        #endregion

        #region Public enumeration

        /// <summary>
        /// Parse type.
        /// </summary>
        public enum ParseType
        {
            /// <summary>
            /// Everything.
            /// </summary>
            All,

            /// <summary>
            /// Just the points will be parsed.
            /// </summary>
            Points,

            /// <summary>
            /// Just the sections will be parsed.
            /// </summary>
            Sections
        }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="parseType">Parse type.</param>
        public Loader(string filename, ParseType parseType)
        {
            this.m_filename = filename;
            m_parseType = parseType;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="filename">Full path to the file containing the relief data.</param>
        public Loader(string filename)
            : this(filename, ParseType.All)
        {

        }

        #endregion

        #region Public methods

        /// <summary>
        /// Parses the relief data file.
        /// </summary>
        public void Parse()
        {
            int totalLines = 0;

            using (Stream s = File.Open(m_filename, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                string currentMapSection = String.Empty;

                using (TextReader reader = new StreamReader(s))
                {
                    string line = String.Empty;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (line.StartsWith(".section "))
                        {
                            FireNewSectionEvent(line);
                        }
                        else
                        {
                            FireReliefPointEvent(line);
                        }

                        totalLines++;
                    }
                }
            }
        }

        #endregion

        #region Private helper methods

        /// <summary>
        /// Fire the ReliefPointFound event.
        /// </summary>
        /// <param name="line">Relief point details from the file.</param>
        private void FireReliefPointEvent(string line)
        {
            if (m_parseType == ParseType.Sections)
            {
                return;
            }

            bool isHit = false;

            try
            {
                List<float> numbers = GetNumbers(line, ref isHit);
                Vector3f point = new Vector3f(numbers[0], numbers[1], numbers[2]);
                ReliefPointEventArgs args = new ReliefPointEventArgs(point, isHit);
                if (ReliefPointFound != null)
                {
                    ReliefPointFound(this, args);
                }
            }
            catch
            {

            }
        }

        /// <summary>
        /// Fire the NewSectionFound event.
        /// </summary>
        /// <param name="line">Section details from the file.</param>
        private void FireNewSectionEvent(string line)
        {
            if (m_parseType == ParseType.Points)
            {
                return;
            }

            string sectionLine = line.Substring(".section ".Length);
            string[] split = sectionLine.Split(",".ToCharArray(), 2, StringSplitOptions.RemoveEmptyEntries);

            Vector3f min = new Vector3f(0, 0, 0);
            Vector3f max = new Vector3f(0, 0, 0);
            GetMaximaMinima(split[1], ref min, ref max);

            if (NewSectionFound != null)
            {
                NewSectionFound(this, new NewSectionEventArgs(split[0], min, max));
            }
        }

        /// <summary>
        /// Get the point data from the line.
        /// </summary>
        /// <param name="line">Line in the relief file.</param>
        /// <param name="isHit">Will be set to true if the ray that was cast hit a triangle.</param>
        /// <returns></returns>
        private List<float> GetNumbers(string line, ref bool isHit)
        {
            string[] split = line.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            isHit = bool.Parse(split[0]);

            List<float> values = new List<float>();
            for (int i = 1; i < split.Length; i++)
            {
                values.Add(float.Parse(split[i]));
            }

            return values;
        }

        /// <summary>
        /// Get the maximum and minimum point values from the header line.
        /// </summary>
        /// <param name="line">Header line.</param>
        /// <param name="min">Maximum point in the region.</param>
        /// <param name="max">Minimum point in the region.</param>
        private void GetMaximaMinima(string line, ref Vector3f min, ref Vector3f max)
        {
            string[] split = line.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            List<float> values = new List<float>();
            for (int i = 0; i < split.Length; i++)
            {
                values.Add(float.Parse(split[i]));
            }

            min.X = values[0];
            min.Y = values[1];
            min.Z = values[2];

            max.X = values[3];
            max.Y = values[4];
            max.Z = values[5];
        }

        #endregion
    }
}
