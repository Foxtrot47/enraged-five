﻿using System;
using RSG.Base.Math;

namespace ReliefImageMapper
{
    /// <summary>
    /// Relief point event delegate.
    /// </summary>
    /// <param name="sender">Sender.</param>
    /// <param name="e">Relief point event arguments.</param>
    internal delegate void ReliefPointEventHandler(object sender, ReliefPointEventArgs e);

    /// <summary>
    /// Relief point event arguments.
    /// </summary>
    internal class ReliefPointEventArgs : EventArgs
    {
        #region Public properties

        /// <summary>
        /// Point in space.
        /// </summary>
        public Vector3f Point { get; set; }

        /// <summary>
        /// True if the point contains a point on a triangle. i.e. we hit a triangle with a ray, so this point contains height data.
        /// </summary>
        public bool IsHit { get; set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="point">Point in space.</param>
        /// <param name="isHit">True if the point contains a point on a triangle. i.e. we hit a triangle with a ray, so this point contains height data.</param>
        public ReliefPointEventArgs(Vector3f point, bool isHit)
        {
            Point = point;
            IsHit = isHit;
        }

        #endregion
    }
}
