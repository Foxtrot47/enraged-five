﻿using System;
using System.Collections.Generic;
using System.Linq;
using RSG.Bounds;
using System.IO;
using Ionic.Zip;
using RSG.Base.Math;

namespace ReliefGeneration
{
    /// <summary>
    /// Bounds file.
    /// </summary>
    internal class BoundsFile
    {
        #region Private member fields

        private List<BVH> m_bvhs;

        #endregion

        #region Public properties

        /// <summary>
        /// File name.
        /// </summary>
        public string Filename { get; private set; }

        /// <summary>
        /// Triangle soup representing the collision mesh data in the bounds file.
        /// </summary>
        public List<Triangle> TriangleSoup { get; private set; }

        /// <summary>
        /// The number of collision boxes in the bounds file.
        /// </summary>
        public int BoxCount { get; private set; }

        /// <summary>
        /// The number of spheres in the bounds file.
        /// </summary>
        public int SphereCount { get; private set; }

        /// <summary>
        /// The number of cylinders in the bounds file.
        /// </summary>
        public int CylinderCount { get; private set; }

        #endregion

        #region Constructor

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="fileName">File name.</param>
        /// <param name="stream">Stream containing the zip file data.</param>
        internal BoundsFile(string fileName, Stream stream)
        {
            Filename = fileName;
            m_bvhs = new List<BVH>();
            BoxCount = 0;
            SphereCount = 0;
            CylinderCount = 0;
            TriangleSoup = new List<Triangle>();

            using (ZipFile zipFile = ZipFile.Read(stream))
            {
                System.Diagnostics.Debug.Assert(zipFile.Entries.Count == 1, "Invalid number of entries. There can only be one .bnd file contained in the zip file.");
                ZipEntry entry = zipFile.Entries.First();

                using (MemoryStream ms = new MemoryStream((int)entry.UncompressedSize))
                {
                    entry.Extract(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    BNDFile bndFile = BNDFile.Load(ms);

                    Composite composite = (Composite)bndFile.BoundObject;

                    foreach (var child in composite.Children)
                    {
                        if ((child.CollisionType & RSG.Bounds.BNDFile.CollisionType.Mover) != RSG.Bounds.BNDFile.CollisionType.Mover)
                        {
                            continue;
                        }

                        AddToSoup((RSG.Bounds.BVH)child.BoundObject);
                    }
                }
            }
        }

        /// <summary>
        /// Add the triangles in the bounding volume hierarchy object to the collection of triangles.
        /// </summary>
        /// <param name="bvh">Bounding volume hierarchy.</param>
        private void AddToSoup(BVH bvh)
        {
            foreach (var prim in bvh.Primitives)
            {
                if (prim is BVHTriangle)
                {
                    var tri = prim as BVHTriangle;

                    Vector3f vert0 = bvh.Verts[tri.Index0];
                    Vector3f vert1 = bvh.Verts[tri.Index1];
                    Vector3f vert2 = bvh.Verts[tri.Index2];

                    TriangleSoup.Add(new Triangle(tri.MaterialIndex, vert0, vert1, vert2, tri.Index0, tri.Index1, tri.Index2));
                }
                else if (prim is BVHBox)
                {
                    BoxCount = BoxCount + 1;
                    var box = prim as BVHBox;
                    List<Triangle> triangles = CreateTriangles(bvh, box);
                    TriangleSoup.AddRange(triangles);
                }
                else if(prim is BVHCylinder)
                {
                    var cylinder = prim as BVHCylinder;
                    CylinderCount = CylinderCount + 1;
                    TriangleSoup.AddRange(CreateTriangles(bvh, cylinder));
                }
                else if(prim is BVHSphere)
                {
                    SphereCount = SphereCount + 1;
                }
                else if (prim is BVHQuad)
                {
                    int fred = 5;
                    Console.WriteLine(fred);
                }
            }
        }

        private List<Triangle> CreateTriangles(BVH bvh, BVHCylinder cylinder)
        {
            List<Triangle> triangles = new List<Triangle>();

            Vector3f vert0 = bvh.Verts[cylinder.EndIndex0];
            Vector3f vert1 = bvh.Verts[cylinder.EndIndex1];
            float radius = cylinder.Radius;

            float centreX = (vert0.X + vert1.X) * 0.5f;
            float centreY = (vert0.Y + vert1.Y) * 0.5f;
            Vector3f topMiddle = new Vector3f(centreX, centreY, Math.Max(vert0.Z, vert1.Z));

            Vector3f radial = new Vector3f(0, radius, 0);
            Vector3f lastPoint = radial;

            for (float angle = 0; angle <= Math.PI * 2; angle += (float)Math.PI / 8)
            {
                float x = radial.Y * (float)Math.Sin(angle);
                float y = radial.Y * (float)Math.Cos(angle);

                Vector3f p0 = topMiddle;
                Vector3f p1 = lastPoint + topMiddle;
                Vector3f p2 = (new Vector3f(x, y, 0)) + topMiddle;
                triangles.Add(new Triangle(0, p0, p1, p2, -1, -1, -1));
            }

            return triangles;
        }



        private List<Triangle> CreateTriangles(BVH bvh, BVHBox box)
        {
            List<Triangle> triangles = new List<Triangle>();

            Vector3f vert0 = bvh.Verts[box.Index0];
            Vector3f vert1 = bvh.Verts[box.Index1];
            Vector3f vert2 = bvh.Verts[box.Index2];
            Vector3f vert3 = bvh.Verts[box.Index3];

            float maxX = Math.Max(vert0.X, Math.Max(vert1.X, (Math.Max(vert2.X, vert3.X))));
            float maxY = Math.Max(vert0.Y, Math.Max(vert1.Y, (Math.Max(vert2.Y, vert3.Y))));
            float maxZ = Math.Max(vert0.Z, Math.Max(vert1.Z, (Math.Max(vert2.Z, vert3.Z))));

            float minX = Math.Min(vert0.X, Math.Min(vert1.X, (Math.Min(vert2.X, vert3.X))));
            float minY = Math.Min(vert0.Y, Math.Min(vert1.Y, (Math.Min(vert2.Y, vert3.Y))));
            float minZ = Math.Min(vert0.Z, Math.Min(vert1.Z, (Math.Min(vert2.Z, vert3.Z))));

            // Top
            Triangle t0 = new Triangle(0, new Vector3f(minX, maxY, maxZ), new Vector3f(maxX, maxY, maxZ), new Vector3f(maxX, minY, maxZ), -1, -1, -1);
            Triangle t1 = new Triangle(0, new Vector3f(minX, maxY, maxZ), new Vector3f(maxX, minY, maxZ), new Vector3f(minX, minY, maxZ), -1, -1, -1);

            // Bottom
            Triangle t2 = new Triangle(0, new Vector3f(minX, maxY, minZ), new Vector3f(maxX, maxY, minZ), new Vector3f(maxX, minY, minZ), -1, -1, -1);
            Triangle t3 = new Triangle(0, new Vector3f(minX, maxY, minZ), new Vector3f(maxX, minY, minZ), new Vector3f(minX, minY, minZ), -1, -1, -1);

        /*     0         1
               +---------+
              /         /|
             /         / |
          3 +---------+2 |
            |         |  |
         *  |  + 4    |  + 5
         *  |         | /  
          7 +---------+ 6
         
         
         */

            // Front
            Triangle t4 = new Triangle(0, new Vector3f(minX, minY, maxZ), new Vector3f(maxX, minY, minZ), new Vector3f(minX, minY, minZ), -1, -1, -1);
            Triangle t5 = new Triangle(0, new Vector3f(minX, minY, maxZ), new Vector3f(maxX, minY, maxZ), new Vector3f(maxX, minY, minZ), -1, -1, -1);

            // Back
            Triangle t6 = new Triangle(0, new Vector3f(minX, maxY, maxZ), new Vector3f(maxX, maxY, minZ), new Vector3f(minX, maxY, minZ), -1, -1, -1);
            Triangle t7 = new Triangle(0, new Vector3f(minX, maxY, maxZ), new Vector3f(maxX, maxY, maxZ), new Vector3f(maxX, maxY, minZ), -1, -1, -1);

            // LHS
            Triangle t8 = new Triangle(0, new Vector3f(minX, maxY, minZ), new Vector3f(minX, minY, maxZ), new Vector3f(minX, minY, minZ), -1, -1, -1);
            Triangle t9 = new Triangle(0, new Vector3f(minX, maxY, minZ), new Vector3f(minX, minY, minZ), new Vector3f(minX, maxY, maxZ), -1, -1, -1);

            // RHS
            Triangle t10 = new Triangle(0, new Vector3f(maxX, maxY, minZ), new Vector3f(maxX, minY, maxZ), new Vector3f(maxX, minY, minZ), -1, -1, -1);
            Triangle t11 = new Triangle(0, new Vector3f(maxX, maxY, minZ), new Vector3f(maxX, minY, minZ), new Vector3f(maxX, maxY, maxZ), -1, -1, -1);

            triangles.AddRange(new Triangle[] { t0, t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11 });
            return triangles;
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Get the bounding volume hierarchies.
        /// </summary>
        /// <returns>Enumeration of bounding volume hierarchies.</returns>
        public IEnumerable<BVH> GetBoundingVolumeHierarchies()
        {
            foreach (var bvh in m_bvhs)
            {
                yield return bvh;
            }
        }

        #endregion
    }
}
