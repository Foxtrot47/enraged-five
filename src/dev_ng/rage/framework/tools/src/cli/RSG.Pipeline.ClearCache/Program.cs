﻿using System;
using System.Collections.Generic;
using System.IO;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services;

namespace RSG.Pipeline.ClearCache
{

    /// <summary>
    /// Program to clear the project cache directory.
    /// </summary>
    class Program
    {
        #region Constants
        private const String OPTION_WILDCARD = "wildcard";
        private const String OPTION_NORECURSIVE = "no-recursive";
        #endregion // Constants

        /// <summary>
        /// Entry-point.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        static int Main(String[] args)
        {
            LogFactory.Initialize();
            IUniversalLog log = LogFactory.ApplicationLog;
            LogFactory.CreateApplicationConsoleLogTarget();
            
            LongOption[] lopts = new LongOption[] {
                new LongOption(OPTION_WILDCARD, LongOption.ArgType.Required, 
                    "Wildcard of files to delete (default: *.*)."),
                new LongOption(OPTION_NORECURSIVE, LongOption.ArgType.None,
                    "Disable cache directory recursion.")
            };
            CommandOptions options = new CommandOptions(args, lopts);
            if (!Directory.Exists(options.Project.Cache))
            {
                log.Warning("Project cache directory: {0} does not exist.  Exiting.", 
                    options.Project.Cache);
                return (Constants.Exit_Success);
            }

            log.Message("Clearing out project cache directory: {0}.", 
                options.Project.Cache);
            bool result = true;
            String wildcard = options.ContainsOption(OPTION_WILDCARD) ? 
                (String)options[OPTION_WILDCARD] : "*.*";
            SearchOption recurseOption = options.ContainsOption(OPTION_NORECURSIVE) ?
                SearchOption.TopDirectoryOnly : SearchOption.AllDirectories;
            try
            {
                Console.Write("[{0}] ", options.Project.Cache);
                result = Cache.ClearDirectory(log, options.Project.Cache);
            }
            catch (Exception ex)
            {
                log.ToolException(ex, "Unhandled exception.");
                result = false;
            }
            return (result ? Constants.Exit_Success : Constants.Exit_Failure);
        }

        #region Private Methods
        #endregion // Private Methods
    }
    
} // RSG.Pipeline.ClearCache namespace
