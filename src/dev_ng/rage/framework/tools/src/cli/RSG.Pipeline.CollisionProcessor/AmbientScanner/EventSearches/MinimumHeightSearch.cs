﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Metadata;
using RSG.Metadata.Model;
using RSG.Metadata.Data;
using RSG.Pipeline.CollisionProcessor.AmbientScanner;

namespace RSG.Pipeline.CollisionProcessor.AmbientScanner.EventSearches
{
    public sealed class MinimumHeightSearch :
        IEventSearch
    {
        #region Constants
        private static readonly String MINIMUM_VALUE = "MinimumValue";
        private static readonly String RADIUS = "Radius";
        private static readonly String SEARCH_PRECISION = "SearchPrecision";
        #endregion

        #region Methods
        /// <summary>
        /// Run a search to see if our cell meets the minimum height condition.
        /// </summary>
        /// <param name="eventSearch"></param>
        /// <param name="inputCell"></param>
        /// <param name="grid"></param>
        /// <returns></returns>
        public bool RunSearch(StructureTunable eventSearch, InputCell inputCell, ref Grid grid)
        {
            float minValue = ((FloatTunable)eventSearch[MINIMUM_VALUE]).Value;
            float radius = ((FloatTunable)eventSearch[RADIUS]).Value;
            SearchPrecision precision = (SearchPrecision)((EnumTunable)eventSearch[SEARCH_PRECISION]).Value;

            bool result = true;

            // Get all the cells in the radius of this cell.
            // If the radius is 0.0f we only return the inputCell.
            IEnumerable<InputCell> cellsInRadius;
            grid.GetCellsInRadius(radius, inputCell, out cellsInRadius);

            switch (precision)
            {
                case SearchPrecision.PRECISION_ACCURATE:
                    {
                        foreach (InputCell neighborCell in cellsInRadius)
                        {
                            if (!neighborCell.HasValidBounds())
                                continue;

                            if (neighborCell.MinimumHeight <= minValue)
                            {
                                result = false;
                                break;
                            }
                        }
                    }
                    break;
                case SearchPrecision.PRECISION_AVERAGE:
                    {
                        foreach (InputCell neighborCell in cellsInRadius)
                        {
                            if (!neighborCell.HasValidBounds())
                                continue;

                            if (neighborCell.AverageHeight <= minValue)
                                result = false;
                        }
                    }
                    break;
                default:
                    break;
            }

            return result;
        }
        #endregion
    }
}
