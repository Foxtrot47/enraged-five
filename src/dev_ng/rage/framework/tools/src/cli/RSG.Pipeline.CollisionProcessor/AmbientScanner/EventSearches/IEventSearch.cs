﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Metadata;
using RSG.Metadata.Model;
using RSG.Metadata.Data;

namespace RSG.Pipeline.CollisionProcessor.AmbientScanner.EventSearches
{
    public interface IEventSearch
    {
        #region Methods
        bool RunSearch(StructureTunable eventSearch, InputCell inputCell, ref Grid grid);
        #endregion
    }
}
