﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RSG.Pipeline.CollisionProcessor.AmbientScanner
{

    /// <summary>
    /// 
    /// </summary>
    public class OutputCell
    {
        #region Constants
        private static readonly String MINIMUM_HEIGHT = "MinHeight";
        private static readonly String MAXIMUM_HEIGHT = "MaxHeight";
        private static readonly String CELL_X = "CellX";
        private static readonly String CELL_Y = "CellY";
        private static readonly String SEARCH_RESULTS = "SearchResults";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Associated input cell.
        /// </summary>
        public InputCell Cell
        {
            get;
            private set;
        }

        /// <summary>
        /// Quantized minimum height within entire InputCell set.
        /// </summary>
        public Byte QuantizedMinimumHeight
        {
            get;
            set;
        }

        /// <summary>
        /// Quantized maximum height within entire InputCell set.
        /// </summary>
        public Byte QuantizedMaximumHeight
        {
            get;
            set;
        }

        /// <summary>
        /// Search bit flags that are valid for this cell.
        /// </summary>
        public List<String> SearchResults
        {
            get;
            set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="cell"></param>
        public OutputCell(InputCell cell)
        {
            this.Cell = cell;
            this.SearchResults = new List<String>();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Serialise the task object to an XElement (parCodeGen-compatible).
        /// </summary>
        /// <param name="name">XML element name.</param>
        /// <returns></returns>
#warning DHM FIX ME: update when we have C# parCodeGen-generator to read config rather than generic interface.
        public XElement ToXElement(String name)
        {
            XElement xmlOutputCell = new XElement(name,
                new XElement(MINIMUM_HEIGHT, new XAttribute("value", this.QuantizedMinimumHeight)),
                new XElement(MAXIMUM_HEIGHT, new XAttribute("value", this.QuantizedMaximumHeight)),
                new XElement(CELL_X, new XAttribute("value", this.Cell.X)),
                new XElement(CELL_Y, new XAttribute("value", this.Cell.Y)),
                new XElement(SEARCH_RESULTS, String.Join(" ", this.SearchResults)));

            return (xmlOutputCell);
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.CollisionProcessor.AmbientScanner namespace
