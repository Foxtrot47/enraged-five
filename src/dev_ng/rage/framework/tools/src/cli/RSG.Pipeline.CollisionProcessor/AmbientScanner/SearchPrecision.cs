﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.CollisionProcessor.AmbientScanner
{
    /// <summary>
    /// The accuracy we want when doing our searches.
    /// This should match the AmbientScanner.psc file.
    /// </summary>
    public enum SearchPrecision
    {
        PRECISION_ACCURATE,
        PRECISION_AVERAGE
    }
}
