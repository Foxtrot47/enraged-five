﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Metadata;
using RSG.Metadata.Model;
using RSG.Metadata.Data;

namespace RSG.Pipeline.CollisionProcessor.AmbientScanner.EventSearches
{
    public class MaximumSlopeSearch
        : IEventSearch
    {
        #region Constants
        private static readonly String MAXIMUM_VALUE = "MaximumValue";
        private static readonly String RADIUS = "Radius";
        private static readonly String SEARCH_PRECISION = "SearchPrecision";
        #endregion

        #region Methods
        /// <summary>
        /// Run a search to see if our cell meets the maximum slope condition.
        /// </summary>
        /// <param name="eventSearch"></param>
        /// <param name="inputCell"></param>
        /// <param name="grid"></param>
        /// <returns></returns>
        public bool RunSearch(StructureTunable eventSearch, InputCell inputCell, ref Grid grid)
        {
            float maxValue = ((FloatTunable)eventSearch[MAXIMUM_VALUE]).Value;
            float radius = ((FloatTunable)eventSearch[RADIUS]).Value;
            SearchPrecision precision = (SearchPrecision)((EnumTunable)eventSearch[SEARCH_PRECISION]).Value;

            bool result = true;

            // Get all the cells in the radius of this cell.
            // If the radius is 0.0f we only return the inputCell.
            IEnumerable<InputCell> cellsInRadius;
            grid.GetCellsInRadius(radius, inputCell, out cellsInRadius);

            switch (precision)
            {
                case SearchPrecision.PRECISION_ACCURATE:
                    {
                        foreach (InputCell neighborCell in cellsInRadius)
                        {
                            if (!neighborCell.HasValidBounds())
                                continue;

                            if (neighborCell.MaximumSlope >= maxValue)
                            {
                                result = false;
                                break;
                            }
                        }
                    }
                    break;
                case SearchPrecision.PRECISION_AVERAGE:
                    {
                        foreach (InputCell neighborCell in cellsInRadius)
                        {
                            if (!neighborCell.HasValidBounds())
                                continue;

                            if (neighborCell.AverageSlope >= maxValue)
                                result = false;
                        }
                    }
                    break;
                default:
                    break;
            }

            return result;
        }
        #endregion
    }
}
