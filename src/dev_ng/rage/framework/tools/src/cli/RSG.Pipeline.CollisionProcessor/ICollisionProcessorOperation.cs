﻿using System;
using System.Collections.Generic;
using RSG.Base.Logging.Universal;
using RSG.Bounds;
using RSG.Base.Configuration;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.CollisionProcessor;

namespace RSG.Pipeline.CollisionProcessor
{

    /// <summary>
    /// Collision Processor Operation interface.
    /// </summary>
    internal interface ICollisionProcessorOperation
    {
        /// <summary>
        /// Operation handled by this class.
        /// </summary>
        Operation Operation { get; }

        /// <summary>
        /// Run the operation over the input asset collection.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="log">Log</param>
        /// <param name="task">Task to execute.</param>
        /// <param name="inputFiles">Input disk files (use inputAssets rather than reload these)</param>
        /// <param name="inputAssets">Input data set.</param>
        /// <param name="rebuild">Rebuild data.</param>
        /// <returns>true iff successful; false otherwise.</returns>
        bool Run(CommandOptions options, IUniversalLog log, CollisionProcessorTask task, 
            IEnumerable<String> inputFiles, IEnumerable<BNDFile> inputAssets, bool rebuild);
    }

} // RSG.Pipeline.CollisionProcessor namespace
