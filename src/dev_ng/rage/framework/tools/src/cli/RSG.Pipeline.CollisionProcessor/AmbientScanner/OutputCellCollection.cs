﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.CollisionProcessor.AmbientScanner
{
    /// <summary>
    /// Class to hold a collection of OutputCells.
    /// </summary>
    public class OutputCellCollection : IEnumerable<OutputCell>
    {
        #region Fields
        /// <summary>
        /// The collection of output cells
        /// </summary>
        private List<OutputCell> _Items = new List<OutputCell>();
        #endregion // Fields

        #region Constructors
        /// <summary>
        /// Create a new instance initialized from a list of input cells.
        /// </summary>
        /// <param name="inputCells"></param>
        public OutputCellCollection(IEnumerable<InputCell> inputCells)
        {
            CreateFromInputCells(inputCells);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Create the list of output cells from a given list of input cells.
        /// </summary>
        /// <param name="inputCells"></param>
        public void CreateFromInputCells(IEnumerable<InputCell> inputCells)
        {
            foreach (InputCell inputCell in inputCells)
            {
                OutputCell outputCell = new OutputCell(inputCell);
                Add(outputCell);
            }
        }

        /// <summary>
        /// Add an output cell to the list.
        /// </summary>
        /// <param name="outputCell"></param>
        public void Add(OutputCell outputCell)
        {
            _Items.Add(outputCell);
        }

        /// <summary>
        /// Find an associated output cell that matches the GUID from the input cell.
        /// </summary>
        /// <param name="inputCell"></param>
        /// <returns></returns>
        public void Find(InputCell inputCell, ref OutputCell outputCell)
        {
            outputCell = _Items.FirstOrDefault(cell => cell.Cell.Id.Equals(inputCell.Id));
        }

        /// <summary>
        /// Quantize the given value in between our min/max range.
        /// </summary>
        /// <param name="value"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public Byte Quantize(float value, float min, float max)
        {
#warning TL FIX ME: Hook up the quantization. Not sure if it's going to be required data?
            return 0;
        }

        /// <summary>
        /// Quantize all the output cells from our calculated minimum/maximum Z that comes from
        /// the input cells.
        /// </summary>
        /// <param name="minZ"></param>
        /// <param name="maxZ"></param>
        public void QuantizeHeights(IEnumerable<InputCell> cells, float minZ, float maxZ)
        {
            foreach (InputCell inputCell in cells)
            {
                // Ignore cells that don't have collision otherwise
                // the heights won't be valid as they are just calculated from default values.
                if (!inputCell.HasValidBounds())
                    continue;

                OutputCell outputCell = null;
                Find(inputCell, ref outputCell);

                if (outputCell != null)
                {
                    outputCell.QuantizedMinimumHeight = Quantize(inputCell.MinimumHeight, minZ, maxZ);
                    outputCell.QuantizedMaximumHeight = Quantize(inputCell.MaximumHeight, minZ, maxZ);
                }
            }
        }
        #endregion

        #region IEnumerable<OutputCell> Members
        public IEnumerator<OutputCell> GetEnumerator()
        {
            return _Items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion
    }
}
