﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.Math;
using RSG.Bounds;
using RSG.Metadata;
using RSG.Metadata.Model;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.CollisionProcessor;
using RSG.Metadata.Parser;
using RSG.Metadata.Data;
using RSG.Pipeline.CollisionProcessor.AmbientScanner.EventSearches;

namespace RSG.Pipeline.CollisionProcessor.AmbientScanner
{
    
    /// <summary>
    /// Ambient Scanner operation class.
    /// </summary>
    internal class AmbientScannerOperation :
        CollisionProcessorOperationBase,
        ICollisionProcessorOperation
    {
        #region Constants
        private static readonly String CONFIG_STRUCTURE = "AmbientScannerConfiguration";
        private static readonly String CONFIG_GRIDSIZE = "GridSize";
        private static readonly String CONFIG_ARRAYSEARCHES = "EventSearches";

        private static readonly String EVENT_EVENTNAME = "Event";
        private static readonly String EVENT_SEARCHCRITERIA = "SearchCriteria";
        private static readonly String EVENT_SEARCHTYPE = "SearchType";
        #endregion // Constants
        
        #region Member Data
        /// <summary>
        /// PSC structure dictionary; configuration data loading.
        /// </summary>
        StructureDictionary structureDictionary;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public AmbientScannerOperation()
            : base(Operation.AmbientScanner)
        {
        }
        #endregion // Constructor(s)

        #region ICollisionProcessorOperation Interface Methods
        /// <summary>
        /// Execute the statistics operation.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="log"></param>
        /// <param name="task"></param>
        /// <param name="inputFiles"></param>
        /// <param name="inputAssets"></param>
        /// <param name="rebuild"></param>
        /// <returns></returns>
        public override bool Run(CommandOptions options, IUniversalLog log, 
            CollisionProcessorTask task, IEnumerable<String> inputFiles, 
            IEnumerable<BNDFile> inputAssets, bool rebuild)
        {
            Debug.Assert(task.Operation.Equals(this.Operation),
                "Internal error.  Running operation for invalid setup.");
            if (!task.Operation.Equals(this.Operation))
                return (false);
            
            // Initial run; we load our structure dictionary.
            bool result = Initialise(options, log);
            if (!File.Exists(task.ConfigFilename))
            {
                log.Error("Task configuration file: {0} does not exist.  Aborting.",
                    task.ConfigFilename);
                return (false);
            }
#warning DHM FIX ME: update when we have C# parCodeGen-generator to read config rather than generic interface.
            log.Message("Loading: {0}.", task.ConfigFilename);
            Structure configStruct = this.structureDictionary[CONFIG_STRUCTURE];
            MetaFile configData = new MetaFile(task.ConfigFilename, configStruct);

            // 1. Segment loaded collision into grid; the grids are cached into
            //    a file based on the hash of the input filenames.
            //    (unless 'rebuild' is set).
            Grid grid;

            result &= LoadGridCells(log, options.Branch, task, configData, inputFiles,
                inputAssets, rebuild, out grid);

            // 2. Run searches per grid.
            result &= RunEventSearches(log, configData, ref grid);
            if (!result)
                log.Error("Failed to run event searches.  See errors above.");

            // 3. Serialize output grid cells
            log.Message("Serializing grid output cells");
            result &= SerializeOutputCells(log, options.Branch, task, ref grid);
            if (!result)
                log.Error("Failed to serialize output cells.");

            return (result);
        }
        #endregion // ICollisionProcessorOperation Interface Methods

        #region Private Methods
        /// <summary>
        /// One-off initialisation.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="log"></param>
        /// <returns></returns>
        private bool Initialise(CommandOptions options, IUniversalLog log)
        {
            if (null == structureDictionary)
            {
                structureDictionary = new Metadata.Model.StructureDictionary();
                structureDictionary.Load(options.Branch, options.Branch.Definitions);

                Debug.Assert(structureDictionary.ContainsKey(CONFIG_STRUCTURE),
                    String.Format("No PSC for '{0}'.  Internal error.", CONFIG_STRUCTURE));
                if (!structureDictionary.ContainsKey(CONFIG_STRUCTURE))
                {
                    log.Error("No PSC for '{0}'.  Internal error.", CONFIG_STRUCTURE);
                    return (false);
                }
            }
            return (true);
        }

        /// <summary>
        /// Load grid cells (or generate if cache is not available).
        /// </summary>
        /// <param name="log"></param>
        /// <param name="branch"></param>
        /// <param name="task"></param>
        /// <param name="configData"></param>
        /// <param name="inputFiles"></param>
        /// <param name="inputAssets"></param>
        /// <param name="rebuild"></param>
        /// <param name="grids"></param>
        /// <returns></returns>
        private bool LoadGridCells(IUniversalLog log, IBranch branch, CollisionProcessorTask task,
            MetaFile configData, IEnumerable<String> inputFiles, IEnumerable<BNDFile> inputAssets, 
            bool rebuild, out Grid grid)
        {
            bool result = true;
            String[] inputFilenames = inputFiles.ToArray();
            uint cacheBasename = RSG.Base.Security.Cryptography.OneAtATime.ComputeHash(String.Join(",", inputFilenames));
            String cacheFilename = Path.Combine("$(cache)", "maps", "$(branch)", "ambient_scanner", cacheBasename + ".xml");
            cacheFilename = Path.GetFullPath(branch.Environment.Subst(cacheFilename));
            
            DateTime latestModTime = GetFilesLatestModTime(inputFilenames);
            DateTime cacheModTime = GetFilesLatestModTime(cacheFilename);

            // Generate our grid cells if our cache is stale.
            if (rebuild || latestModTime.IsLaterThan(cacheModTime))
            {
                log.Message("Generating grid cells.");
                result &= GenerateGridCells(log, task, configData, inputAssets, out grid);
                if (!result)
                {
                    log.Error("Failed to generate grid cells.  Aborting.");
                    return (result);
                }
                log.Message("Saving grid cells to cache file: {0}.", cacheFilename);

                String cacheDirectory = Path.GetDirectoryName(cacheFilename);
                if (!Directory.Exists(cacheDirectory))
                    Directory.CreateDirectory(cacheDirectory);

                Grid.Save(grid, cacheFilename);
            }
            else
            {
                log.Message("Loading grid from cache file: {0}.", cacheFilename);
                grid = Grid.Load(cacheFilename);
            }

            // Initialize our output cell collection now that we have our list of input cells.
            grid.InitializeOutputCells();

            return (result);
        }

        /// <summary>
        /// Generate grid cells (invoked by LoadGridCells).
        /// </summary>
        /// <param name="log"></param>
        /// <param name="task"></param>
        /// <param name="configData"></param>
        /// <param name="inputAssets"></param>
        /// <param name="grids"></param>
        /// <returns></returns>
        private bool GenerateGridCells(IUniversalLog log, CollisionProcessorTask task,
            MetaFile configData, IEnumerable<BNDFile> inputAssets, 
            out Grid grid)
        {
            // Config Data.
            StructureTunable root = (StructureTunable)configData.Members[CONFIG_STRUCTURE];
            float gridSize = ((FloatTunable)root[CONFIG_GRIDSIZE]).Value;

            BoundingBox3f worldBound = new BoundingBox3f();
            foreach (BNDFile bndFile in inputAssets)
            {
                ExpandFor(log, bndFile.BoundObject, worldBound);
            }
            log.Message("Loaded bounds covers: {0} to {1}.", worldBound.Min, worldBound.Max);
            
            // Calculate the bounds for a snapped grid; exact number of cells.
            Vector2f minRounded = new Vector2f(
                (worldBound.Min.X % gridSize) != 0.0f ? worldBound.Min.X + (gridSize - (worldBound.Min.X % gridSize)) : worldBound.Min.X,
                (worldBound.Min.Y % gridSize) != 0.0f ? worldBound.Min.Y + (gridSize - (worldBound.Min.Y % gridSize)) : worldBound.Min.Y);
            Vector2f maxRounded = new Vector2f(
                (worldBound.Max.X % gridSize) != 0.0f ? worldBound.Max.X + (gridSize - (worldBound.Max.X % gridSize)) : worldBound.Max.X,
                (worldBound.Max.Y % gridSize) != 0.0f ? worldBound.Max.Y + (gridSize - (worldBound.Max.Y % gridSize)) : worldBound.Max.Y);
            BoundingBox2f worldBoundRounded = new BoundingBox2f(minRounded, maxRounded);
            int numCellsWidth = (int)((maxRounded.X - minRounded.X) / gridSize);
            int numCellsHeight = (int)((maxRounded.Y - minRounded.Y) / gridSize);
            log.Message("Rounded 2D bounds for cell size of {0} m: {1} to {2} ({3} x {4} cells).",
                gridSize, minRounded, maxRounded, numCellsWidth, numCellsHeight);

            grid = new Grid(minRounded, maxRounded, gridSize, numCellsWidth, numCellsHeight);

            // Create cells; this could be multi-threaded if required provided
            // write to cell and gridCells list is locked.  The cache helps
            // in that this is only regenerated when required.
            List<InputCell> constructedGridCells = new List<InputCell>();
            using (ProfileContext ctx = new ProfileContext(log, "Creating cells and bucketing Bound Objects."))
            {
                for (int cellX = 0; cellX < numCellsWidth; ++cellX)
                {
                    for (int cellY = 0; cellY < numCellsHeight; ++cellY)
                    {
                        Vector3f min = new Vector3f(minRounded.X + (cellX * gridSize),
                            minRounded.Y + (cellY * gridSize),
                            float.MinValue);
                        Vector3f max = new Vector3f(minRounded.X + ((cellX + 1) * gridSize),
                            minRounded.Y + ((cellY + 1) * gridSize),
                            float.MaxValue);
                        BoundingBox3f box = new BoundingBox3f(min, max);
                        InputCell cell = new InputCell(cellX, cellY, box);

                        // Buckets BNDFiles into cell (can appear in multiple if they intersect the cell).
                        // Do we need to improve the intersection tests?
                        foreach (BNDFile bndFile in inputAssets)
                        {
                            BoundingBox3f bndFileBox = new BoundingBox3f();
                            ExpandFor(log, bndFile.BoundObject, bndFileBox);

                            if (bndFileBox.Intersects(box) || box.Intersects(bndFileBox) ||
                                box.Contains(bndFileBox.Min) || box.Contains(bndFileBox.Max))
                                cell.AddBoundObject(bndFile.BoundObject);
                        }

                        cell.Freeze();
                        grid.AddGridCell(cell, cellX, cellY);
                    }
                }
            }

            // Build the flat cell list (used for serialization) after the cells have been added.
            grid.BuildCellList();

            return (true);
        }

        /// <summary>
        /// Run through the event searches.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="configData"></param>
        /// <param name="inputCells"></param>
        /// <param name="outputCellCollection"></param>
        /// <returns></returns>
        private bool RunEventSearches(IUniversalLog log, MetaFile configData, ref Grid grid)
        {
            bool result = true;
            StructureTunable root = (StructureTunable)configData.Members[CONFIG_STRUCTURE];
            float gridSize = ((FloatTunable)root[CONFIG_GRIDSIZE]).Value;
            ArrayTunable eventSearches = ((ArrayTunable)root[CONFIG_ARRAYSEARCHES]);

            foreach (StructureTunable eventSearch in eventSearches.Items)
                result &= RunEventSearch(log, eventSearch, ref grid);

            return result;
        }

        /// <summary>
        /// Run a single event search on our input cells.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="eventSearch"></param>
        /// <param name="inputCells"></param>
        /// <param name="outputCellCollection"></param>
        /// <returns></returns>
        private bool RunEventSearch(IUniversalLog log, StructureTunable eventSearch, 
            ref Grid grid)
        {
            EnumTunable eventTunable = (EnumTunable)eventSearch[EVENT_EVENTNAME];

            long eventEnum = eventTunable.Value;
            String eventName = eventTunable.ValueAsString;

            log.Message("Running Event Search : {0}", eventName);

            ArrayTunable searchCriteria = ((ArrayTunable)eventSearch[EVENT_SEARCHCRITERIA]);

            IEnumerable<InputCell> inputCells = grid.GridCellList;

            foreach (InputCell inputCell in inputCells)
            {
                bool searchResult = true;

                if (inputCell.HasValidBounds())
                {
                    foreach (PointerTunable searchEntry in searchCriteria.Items)
                    {
                        StructureTunable entryTunable = ((StructureTunable)searchEntry.Value);
                        String searchType = ((EnumTunable)entryTunable[EVENT_SEARCHTYPE]).ValueAsString;

                        switch (searchType)
                        {
                            case "SearchDef_UseHeightMinimum":
                                MinimumHeightSearch minHeightSearch = new MinimumHeightSearch();
                                searchResult &= minHeightSearch.RunSearch(entryTunable, inputCell, ref grid);
                                break;
                            case "SearchDef_UseHeightMaximum":
                                MaximumHeightSearch maxHeightSearch = new MaximumHeightSearch();
                                searchResult &= maxHeightSearch.RunSearch(entryTunable, inputCell, ref grid);
                                break;
                            case "SearchDef_UseSlopeMinimum":
                                MinimumSlopeSearch minimumSlopeSearch = new MinimumSlopeSearch();
                                searchResult &= minimumSlopeSearch.RunSearch(entryTunable, inputCell, ref grid);
                                break;
                            case "SearchDef_UseSlopeMaximum":
                                MaximumSlopeSearch maximumSlopeSearch = new MaximumSlopeSearch();
                                searchResult &= maximumSlopeSearch.RunSearch(entryTunable, inputCell, ref grid);
                                break;
                            case "SearchDef_UseBoundMaterialIncludes":
                                BoundMaterialIncludesSearch boundsMaterialIncludesSearch = new BoundMaterialIncludesSearch();
                                searchResult &= boundsMaterialIncludesSearch.RunSearch(entryTunable, inputCell, ref grid);
                                break;
                            case "SearchDef_UseBoundMaterialExcludes":
                                BoundMaterialExcludesSearch boundsMaterialExcludesSearch = new BoundMaterialExcludesSearch();
                                searchResult &= boundsMaterialExcludesSearch.RunSearch(entryTunable, inputCell, ref grid);
                                break;
                            default:
                                log.Warning("Invalid search type: {0}", searchType);
                                break;
                        }
                    }
                }
                else
                {
                    searchResult = false;
                }

                // If all the critera for the search has been met, flag the cell as being valid for this event.
                if (searchResult)
                    grid.FlagSearchEvent(inputCell, eventName);
            }

            return (true);
        }

        /// <summary>
        /// Serializes the entire set of output cells to our PSC format.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="outputCellCollection"></param>
        /// <returns></returns>
        private bool SerializeOutputCells(IUniversalLog log, IBranch branch, CollisionProcessorTask task, ref Grid grid)
        {
            string filenameNoExt = Path.GetFileNameWithoutExtension(task.ConfigFilename);
            string outputFilename = Path.Combine(task.OutputAsset, filenameNoExt + ".pso.meta");
            outputFilename = branch.Environment.Subst(outputFilename);

            String outputDirectory = Path.GetDirectoryName(outputFilename);
            if (!Directory.Exists(outputDirectory))
                Directory.CreateDirectory(outputDirectory);

            XDocument document = new XDocument(
                new XElement("BoundsCellData",
                    new XElement("MinZ", grid.MinZ),
                    new XElement("MaxZ", grid.MaxZ),
                    grid.ToGridXElement(),
                    grid.ToOutputCellXElement()));

            document.Save(outputFilename);

            // Generate a pretty picture to see what cells have collision/valid searches, etc.
            // Not used for anything in particular, just some visual representation when testing searches.
            string outputImageFilename = Path.Combine(outputDirectory, filenameNoExt + ".bmp");
            grid.GenerateDebugImage(outputImageFilename);

            return true;
        }

        /// <summary>
        /// Expand a BoundingBox3f to enclose a BoundObject.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="boundObj"></param>
        /// <param name="box"></param>
        private void ExpandFor(IUniversalLog log, BoundObject boundObj, BoundingBox3f box)
        {
            if (boundObj is Composite)
            {
                Composite bound = (Composite)boundObj;
                foreach (CompositeChild child in bound.Children)
                    ExpandFor(log, child.BoundObject, box);
            }
            else if (boundObj is BVH)
            {
                BVH bvh = (BVH)boundObj;
                Debug.Assert(!bvh.Bounds.IsEmpty);
                box.Expand(bvh.Bounds);
            }
            else if (boundObj is Geometry)
            {
                Geometry bound = (Geometry)boundObj;
                box.Expand(bound.Bounds);
            }
            // Primitive Bound Types.
            else if (boundObj is Box)
            {
                Box b = (Box)boundObj;
                Vector3f halfSize = b.Size / 2.0f;
                box.Expand(b.Centroid - halfSize);
                box.Expand(b.Centroid + halfSize);
            }
#if false
            else if (boundObj is Capsule)
            {

            }
            else if (boundObj is Cylinder)
            {

            }
            else if (boundObj is Sphere)
            {

            }
#endif
            else
            {
                log.Error("Unsupported bound object type: {0}.  Internal error.",
                    boundObj.GetType().ToString());
            }
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.CollisionProcessor.AmbientScanner namespace
