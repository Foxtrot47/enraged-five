﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSG.Pipeline.CollisionProcessor.AmbientScanner
{
    /// <summary>
    /// Handles a collection of grids for use with adjacent grid (container) searches.
    /// </summary>
    public class GridCollection
        : IEnumerable<Grid>
    {
        #region Fields
        /// <summary>
        /// The collection of grids
        /// </summary>
        private List<Grid> _Items = new List<Grid>();
        #endregion // Fields

        #region Constructors
        public GridCollection()
        {
        }
        #endregion

        #region Methods
        #endregion

        #region IEnumerable<Grid> Members
        public IEnumerator<Grid> GetEnumerator()
        {
            return _Items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
        #endregion
    }
}
