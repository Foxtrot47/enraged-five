﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using RSG.Base.Math;

namespace RSG.Pipeline.CollisionProcessor.AmbientScanner
{
    /// <summary>
    /// Defines a grid that the AmbientScanner uses.
    /// 
    /// This is the main class that all the data for this tool will be stored in.
    /// We have our list of input cells which define the specific cells in our grid.
    /// We also have a list of output cells which are filled with the results of our searches
    /// and all the other data that makes up our grid (number of cells, grid width/height, etc).
    /// </summary>
    [DataContract]
    public class Grid
    {
        #region Constants
        private static readonly String MIN_X = "MinX";
        private static readonly String MAX_X = "MaxX";
        private static readonly String MIN_Y = "MinY";
        private static readonly String MAX_Y = "MaxY";

        private static readonly String CELL_DIM_X = "CellDimX";
        private static readonly String CELL_DIM_Y = "CellDimY";

        private static readonly String NUM_CELLS = "NumCells";
        #endregion

        #region Properties
        /// <summary>
        /// Max position of grid.
        /// </summary>
        [DataMember]
        public Vector2f Max
        {
            get;
            private set;
        }

        /// <summary>
        /// Min position of grid.
        /// </summary>
        [DataMember]
        public Vector2f Min
        {
            get;
            private set;
        }

        /// <summary>
        /// Minimum height caclulated by finding the minimum 
        /// height from every cell in the grid.
        /// </summary>
        [DataMember]
        public float MaxZ
        {
            get;
            private set;
        }

        /// <summary>
        /// Minimum height caclulated by finding the minimum 
        /// height from every cell in the grid.
        /// </summary>
        [DataMember]
        public float MinZ
        {
            get;
            private set;
        }

        /// <summary>
        /// Width (in # of cells) of the grid.
        /// </summary>
        [DataMember]
        public int Width
        {
            get;
            private set;
        }

        /// <summary>
        /// Height (in # of cells) of the grid.
        /// </summary>
        [DataMember]
        public int Height
        {
            get;
            private set;
        }

        /// <summary>
        /// Grid cell dimension.
        /// </summary>
        [DataMember]
        public float CellDimension
        {
            get;
            private set;
        }

        /// <summary>
        /// Number of cells in the grid.
        /// </summary>
        [DataMember]
        public int NumCells
        {
            get;
            private set;
        }

        /// <summary>
        /// Flat list of grid cells.
        /// </summary>
        [DataMember]
        public List<InputCell> GridCellList
        {
            get;
            private set;
        }

        /// <summary>
        /// Input cells defining the entire grid.
        /// This cannot be serialized because it is a multi-dimensional array
        /// so it needs to be rebuilt from the GridCellList on deserialization.
        /// </summary>
        public InputCell[,] GridCells
        {
            get;
            private set;
        }

        /// <summary>
        /// Holds a list of all the output cells we have for this grid.
        /// 
        /// Note: This doesn't get serialized, it gets re-created every time from our base
        /// input cells and then has the search results written to them.
        /// </summary>
        public OutputCellCollection OutputCells
        {
            get;
            private set;
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Create a new grid with all the values default.
        /// </summary>
        public Grid()
        {
            this.Max = new Vector2f(0.0f, 0.0f);
            this.Min = new Vector2f(0.0f, 0.0f);
            this.MaxZ = 0;
            this.MinZ = 0;
            this.CellDimension = 1.0f;
            this.NumCells = 0;
            this.Width = 0;
            this.Height = 0;
            this.GridCellList = new List<InputCell>();
        }

        /// <summary>
        /// Create a new grid with all the values set.
        /// </summary>
        public Grid(Vector2f gridMin, Vector2f gridMax, float cellDimension, int width, int height)
        {
            this.Max = gridMax;
            this.Min = gridMin;
            this.MaxZ = float.MinValue;
            this.MinZ = float.MaxValue;
            this.Width = width;
            this.Height = height;
            this.CellDimension = cellDimension;
            this.NumCells = width * height;

            this.GridCells = new InputCell[width, height];
            this.GridCellList = new List<InputCell>();
        }
        #endregion

        #region Static Methods
        /// <summary>
        /// Deserialize the Grid object from the given filename.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static Grid Load(String filename)
        {
            using (FileStream fs = File.OpenRead(filename))
                return (Load(fs));
        }

        /// <summary>
        /// Deserialize the Grid object from a given file stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static Grid Load(Stream stream)
        {
            using (XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(
                stream, new XmlDictionaryReaderQuotas()))
            {
                DataContractSerializer serialiser = new DataContractSerializer(
                    typeof(Grid));
                Grid grid = (Grid)serialiser.ReadObject(reader, true);
                
                // Reload the multidimensional array of cells after the flat list has been deserialized.
                grid.ReloadCellGrid();

                return (grid);
            }
        }

        /// <summary>
        /// Serialize our Grid object to the given filename.
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="filename"></param>
        public static void Save(Grid grid, String filename)
        {
            using (FileStream fs = File.Create(filename))
                Save(grid, fs);
        }

        /// <summary>
        /// Serialize our Grid object to the given file stream.
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="stream"></param>
        public static void Save(Grid grid, Stream stream)
        {
            DataContractSerializer serialiser = new DataContractSerializer(
                typeof(Grid));
            XmlWriterSettings serialiserSettings = new XmlWriterSettings { Indent = true };
            using (XmlWriter writer = XmlWriter.Create(stream, serialiserSettings))
                serialiser.WriteObject(writer, grid);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Load the grid cells back into our multidimensional array.
        /// We need this for serializing because we can't serialize
        /// a multidimensional array so we save a flat list of cells
        /// which we then hook back into our array for easier access.
        /// </summary>
        public void ReloadCellGrid()
        {
            if (this.GridCells == null)
                this.GridCells = new InputCell[this.Width, this.Height];

            if (this.GridCellList.Count > 0)
            {
                foreach (InputCell cell in this.GridCellList)
                {
                    this.GridCells[cell.X, cell.Y] = cell;
                }
            }
        }

        /// <summary>
        /// Initialize our output cell structure from our current cell list.
        /// </summary>
        public void InitializeOutputCells()
        {
            this.OutputCells = new OutputCellCollection(this.GridCellList);
        }

        /// <summary>
        /// Use the input cell to find our associated output cell and flag it with the proper
        /// bit flag for a given search.
        /// </summary>
        /// <param name="inputCell"></param>
        /// <param name="searchEventEnumString"></param>
        public void FlagSearchEvent(InputCell inputCell, String searchEventEnumString)
        {
            OutputCell outputCell = null;
            this.OutputCells.Find(inputCell, ref outputCell);

            if (outputCell != null)
                outputCell.SearchResults.Add(searchEventEnumString);
        }

        /// <summary>
        /// Add a cell to the grid at the given x-y location
        /// </summary>
        /// <param name="cell"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public void AddGridCell(InputCell cell, int x, int y)
        {
            Debug.Assert(x < this.GridCells.GetLength(0), "X range invalid for grid cell");
            Debug.Assert(y < this.GridCells.GetLength(1), "Y range invalid for grid cell");

            this.GridCells[x, y] = cell;

            // Get the min/max height from the cell and update until we get our
            // overall grid min/max to be used to quantize our cell level min/max height.
            if (cell.MaximumHeight > this.MaxZ)
                this.MaxZ = cell.MaximumHeight;
            if (cell.MinimumHeight < this.MinZ)
                this.MinZ = cell.MinimumHeight;
        }

        /// <summary>
        /// Get a grid cell at the given x-y location
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public InputCell GetGridCell(int x, int y)
        {
            if (x < 0 || x >= this.Width)
                return null;

            if (y < 0 || y >= this.Height)
                return null;

            return this.GridCells[x, y];
        }

        /// <summary>
        /// Build a flat list of all the cells we have in our grid.
        /// </summary>
        public void BuildCellList()
        {
            for (int x = 0; x < this.GridCells.GetLength(0); x++)
            {
                for (int y = 0; y < this.GridCells.GetLength(1); y++)
                {
                    this.GridCellList.Add(this.GridCells[x, y]);
                }
            }
        }

        /// <summary>
        /// Return a list of all the cells that exist in the radius of the given cell.
        /// </summary>
        /// <param name="radius"></param>
        /// <param name="searchCell"></param>
        /// <param name="cells"></param>
        public void GetCellsInRadius(float radius, InputCell searchCell, out IEnumerable<InputCell> cells)
        {
            List<InputCell> cellsInRadius = new List<InputCell>();

            // If we don't have a radius, return just the 1 cell we're asking to search from.
            if (radius == 0.0f)
            {
                cellsInRadius.Add(searchCell);
            }
            // Otherwise, grab all the cells in our radius.
            else
            {
                int searchX = searchCell.X;
                int searchY = searchCell.Y;

                // Find out how many cells across we need to search
                int cellDistance = (int)Math.Ceiling((double)((radius / 2.0f) / this.CellDimension));

                // Create a smaller "radius" grid and search through it.
                for (int x = searchX - cellDistance; x <= (searchX + cellDistance); x++)
                {
                    for (int y = searchY - cellDistance; y <= (searchY + cellDistance); y++)
                    {
                        InputCell neighborCell = GetGridCell(x, y);
                        if (neighborCell != null)
                            cellsInRadius.Add(searchCell);
                    }
                }
            }

            cells = cellsInRadius;
        }

        /// <summary>
        /// Create a PSC like structure for the rage::spdGrid structure
        /// base on the values defining the grid.
        /// </summary>
        /// <returns></returns>
        public XElement ToGridXElement()
        {
            XElement xmlOutputGrid = new XElement("Grid",
                new XElement(MIN_X, new XAttribute("value", this.Min.X)),
                new XElement(MAX_X, new XAttribute("value", this.Max.X)),
                new XElement(MIN_Y, new XAttribute("value", this.Min.Y)),
                new XElement(MAX_Y, new XAttribute("value", this.Max.Y)),
                new XElement(CELL_DIM_X, new XAttribute("value", this.CellDimension)),
                new XElement(CELL_DIM_Y, new XAttribute("value", this.CellDimension)),
                new XElement(NUM_CELLS, new XAttribute("value", this.NumCells)));

            return (xmlOutputGrid);
        }

        /// <summary>
        /// Create a PSC like structure for the OutputCell structure.
        /// </summary>
        /// <returns></returns>
        public XElement ToOutputCellXElement()
        {
            // Quantize the heights before we write out our cell data.
            this.OutputCells.QuantizeHeights(this.GridCellList, this.MinZ, this.MaxZ);

            XElement xmlOutputCells = new XElement("TaskList",
                        this.OutputCells.Select(cell => cell.ToXElement("CellData")));

            return xmlOutputCells;
        }

        /// <summary>
        /// DEBUGGERY...
        /// 
        /// Generates a bitmap that represents the entire grid of cells.
        /// For any cell that contains a search result will be painted in 
        /// as some visual representation of what is getting results.
        /// </summary>
        public void GenerateDebugImage(string filename)
        {
            Bitmap bitmap = new Bitmap(this.Width * (int)this.CellDimension, this.Height * (int)this.CellDimension);
            Graphics g = Graphics.FromImage(bitmap);

            // Generate the grid based on the size of the cell dimensions
            Brush outlineBrush = new SolidBrush(Color.White);
            Pen pen = new Pen(outlineBrush, 1);

            for (int w = 0; w < this.Width; w++)
            {
                g.DrawLine(pen, new Point(w * (int)this.CellDimension, 0), new Point(w * (int)this.CellDimension, this.Height * (int)this.CellDimension));
            }
            for (int h = 0; h < this.Height; h++)
            {
                g.DrawLine(pen, new Point(0, h * (int)this.CellDimension), new Point(this.Width * (int)this.CellDimension, h * (int)this.CellDimension));
            }

            // Paint in any cell that has a valid search result (found at least 1 event)
            foreach (InputCell cell in this.GridCellList)
            {
                int xPos = (cell.X * (int)this.CellDimension) + 1;
                int yPos = (((this.Height - 1) - cell.Y) * (int)this.CellDimension) + 1;

                Rectangle cellRect = new Rectangle(xPos, yPos, (int)this.CellDimension - 1, (int)this.CellDimension - 1);

                // Mark the cells that don't have valid collision in them.
                if (!cell.HasValidBounds())
                {
                    Brush invalidBrush = new SolidBrush(Color.Yellow);
                    g.FillRectangle(invalidBrush, cellRect);
                    continue;
                }

                OutputCell outputCell = null;
                this.OutputCells.Find(cell, ref outputCell);

                // Find the output cell so we can check the search results.
                if (outputCell != null)
                {
                    if (outputCell.SearchResults.Count <= 0)
                        continue;
                }

                // Mark the cell if we have valid searches that were met.
                Brush fillBrush = new SolidBrush(Color.Red);
                g.FillRectangle(fillBrush, cellRect);
            }

            bitmap.Save(filename, ImageFormat.Bmp);
        }
        #endregion
    }
}
