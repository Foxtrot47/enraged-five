﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Metadata;
using RSG.Metadata.Model;
using RSG.Metadata.Data;

namespace RSG.Pipeline.CollisionProcessor.AmbientScanner.EventSearches
{
    public class BoundMaterialIncludesSearch
        : IEventSearch
    {
        #region Constants
        private static readonly String MATERIAL_LIST = "MaterialList";
        private static readonly String RADIUS = "Radius";
        #endregion

        /// <summary>
        /// Runs a material inclusion search on the input cells.
        /// </summary>
        /// <param name="eventSearch"></param>
        /// <param name="inputCell"></param>
        /// <param name="grid"></param>
        /// <returns></returns>
        public bool RunSearch(StructureTunable eventSearch, InputCell inputCell, ref Grid grid)
        {
            ArrayTunable materialList = ((ArrayTunable)eventSearch[MATERIAL_LIST]);
            float radius = ((FloatTunable)eventSearch[RADIUS]).Value;

            // Build up a list of the material names in the search criteria
            List<String> materialNames = new List<String>();
            foreach (StringTunable materialEntry in materialList.Items)
                materialNames.Add(materialEntry.Value);

            // Get all the cells in the radius of this cell.
            IEnumerable<InputCell> cellsInRadius;
            grid.GetCellsInRadius(radius, inputCell, out cellsInRadius);

            // Check to see if we found a material
            bool foundInclusionMaterial = false;
            foreach (InputCell neighborCell in cellsInRadius)
            {
                if (!neighborCell.HasValidBounds())
                    continue;

                // Make sure our cell matches all the included materials we want to check for.
                int matchingCount = materialNames.Count(mat => neighborCell.MaterialList.Contains(mat.ToLower()));
                if (matchingCount == materialNames.Count)
                    foundInclusionMaterial = true;
            }

            return foundInclusionMaterial;
        }
    }
}
