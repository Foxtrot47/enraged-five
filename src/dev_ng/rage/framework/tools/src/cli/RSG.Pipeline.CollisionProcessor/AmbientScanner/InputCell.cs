﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml;
using RSG.Base.Math;
using RSG.Bounds;

namespace RSG.Pipeline.CollisionProcessor.AmbientScanner
{

    /// <summary>
    /// Ambient Scanner input grid cell; loaded collision is processed into
    /// a 2D grid of these objects.
    /// </summary>
    /// Note: when adding new properties that need to be frozen and saved
    /// ensure you update the Freeze, Serialise and Load methods.
    /// 
    /// Note: we use DataContract here because these objects are only
    /// serialised out for **temporary data**.  If that ever changes we
    /// need to implement the seialisation to control it.
    /// 
    [DataContract]
    public class InputCell
    {
        #region Properties
        /// <summary>
        /// Cell unique identifier.
        /// </summary>
        [DataMember]
        public Guid Id
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int X
        {
            get;
            private set;
        }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int Y
        {
            get;
            private set;
        }

        /// <summary>
        /// 3D bounding box of cell.
        /// </summary>
        [DataMember]
        public BoundingBox3f BoundingBox
        {
            get;
            private set;
        }

        /// <summary>
        /// Enumerable of Bound Objects that intersect this cell.
        /// </summary>
        /// Note: specifically not serialised out.
        /// 
        public IEnumerable<BoundObject> BoundObjects
        {
            get { return _boundObjects; }
        }

        /// <summary>
        /// Whether the object has been frozen.
        /// </summary>
        [DataMember]
        public bool Frozen
        {
            get;
            private set;
        }

        /// <summary>
        /// Whether we found valid collision data in this cell.
        /// </summary>
        [DataMember]
        public bool FoundCollision
        {
            get;
            private set;
        }

        /// <summary>
        /// Minimum height in cell (in meters).
        /// </summary>
        [DataMember]
        public float MinimumHeight
        {
            get;
            private set;
        }

        /// <summary>
        /// Maximum height in cell (in meters).
        /// </summary>
        [DataMember]
        public float MaximumHeight
        {
            get;
            private set;
        }

        /// <summary>
        /// Average height in cell (in meters).
        /// </summary>
        [DataMember]
        public float AverageHeight
        {
            get;
            private set;
        }

        /// <summary>
        /// Maximum slope in cell (in degrees)
        /// </summary>
        [DataMember]
        public float MaximumSlope
        {
            get;
            private set;
        }

        /// <summary>
        /// Minimum slope in cell (in degrees)
        /// </summary>
        [DataMember]
        public float MinimumSlope
        {
            get;
            private set;
        }

        /// <summary>
        /// Average slope in cell (in degrees)
        /// </summary>
        [DataMember]
        public float AverageSlope
        {
            get;
            private set;
        }

        /// <summary>
        /// Material list of all the bounds in this cell
        /// </summary>
        [DataMember]
        public List<String> MaterialList
        {
            get;
            private set;
        }

#warning DHM FIX ME: add obstruction information
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Bound object collection (so it can be frozen).
        /// </summary>
        private ICollection<BoundObject> _boundObjects;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public InputCell(int x, int y, BoundingBox3f box)
        {
            Debug.Assert(!box.IsEmpty, "BoundingBox is empty!  Internal error.");

            this.FoundCollision = false;
            this.Frozen = false;
            this.Id = Guid.NewGuid();
            this.X = x;
            this.Y = y;
            this.BoundingBox = box;
            this.MaterialList = new List<String>();
            Reset();
        }
        #endregion // Constructor(s)

        #region Static Controller Methods (Deprecated - Use Grid Serializing)
        /// <summary>
        /// Load InputCell objects from file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        public static IEnumerable<InputCell> Load(String filename)
        {
            using (FileStream fs = File.OpenRead(filename))
                return (Load(fs));
        }

        /// <summary>
        /// Load InputCell objects from stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <returns></returns>
        public static IEnumerable<InputCell> Load(Stream stream)
        {
            using (XmlDictionaryReader reader = XmlDictionaryReader.CreateTextReader(
                stream, new XmlDictionaryReaderQuotas()))
            {
                DataContractSerializer serialiser = new DataContractSerializer(
                    typeof(IEnumerable<InputCell>));
                IEnumerable<InputCell> cells = (IEnumerable<InputCell>)serialiser.ReadObject(reader, true);
                return (cells);
            }
        }

        /// <summary>
        /// Save InputCell objects to file.
        /// </summary>
        /// <param name="inputCells"></param>
        /// <param name="filename"></param>
        public static void Save(IEnumerable<InputCell> inputCells, String filename)
        {
            using (FileStream fs = File.Create(filename))
                Save(inputCells, fs);
        }

        /// <summary>
        /// Save InputCell objects to stream.
        /// </summary>
        /// <param name="inputCells"></param>
        /// <param name="stream"></param>
        public static void Save(IEnumerable<InputCell> inputCells, Stream stream)
        {
            DataContractSerializer serialiser = new DataContractSerializer(
                typeof(IEnumerable<InputCell>));
            XmlWriterSettings serialiserSettings = new XmlWriterSettings { Indent = true };
            using (XmlWriter writer = XmlWriter.Create(stream, serialiserSettings))
                serialiser.WriteObject(writer, inputCells);
        }
        #endregion // Static Controller Methods
 
        #region Controller Methods
        /// <summary>
        /// Reset all calculated cell data.
        /// </summary>
        public void Reset()
        {
            this._boundObjects = new List<BoundObject>();

            this.MinimumHeight = float.MaxValue;
            this.MaximumHeight = float.MinValue;
            this.AverageHeight = 0.0f;
            
            this.MaximumSlope = 0.0f;
            this.MinimumSlope = 0.0f;
            this.AverageSlope = 0.0f;

            this.FoundCollision = false;

            this.MaterialList.Clear();
        }

        /// <summary>
        /// Add bound object to this cell.
        /// </summary>
        /// <param name="boundObj"></param>
        public void AddBoundObject(BoundObject boundObj)
        {
            if (this.Frozen)
                throw (new NotSupportedException("Cell is frozen; cannot be modified."));

            this._boundObjects.Add(boundObj);
        }

        /// <summary>
        /// Freeze the cell; calculating our properties.
        /// </summary>
        public void Freeze()
        {
            this.averageSlopeCount = 0;
            this.AverageSlope = 0.0f;

            this.averageHeightCount = 0;
            this.AverageHeight = 0.0f;

            foreach (BoundObject boundObj in this._boundObjects)
            {
                FreezeBoundObject(boundObj);
                CollectBoundMaterials(boundObj);
            }

            // Get the average slope and height after all collision has been scanned.
            this.AverageSlope /= this.averageSlopeCount;
            this.AverageHeight /= this.averageHeightCount;

            this.Frozen = true;
        }

        /// <summary>
        /// Returns whether this cell actually has valid bounds.
        /// If not then we shouldn't run any searches on it.
        /// </summary>
        /// <returns></returns>
        public bool HasValidBounds()
        {
            // Was also checking if boundObjects.Count > 0 but
            // they were not being serialized so need to check this flag instead.

            return (this.FoundCollision);
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Calculate our max/min height and slopes.
        /// </summary>
        /// <param name="boundObj"></param>
        private void FreezeBoundObject(BoundObject boundObj)
        {
            if (boundObj is Composite)
            {
                Composite bound = (Composite)boundObj;
                foreach (CompositeChild child in bound.Children)
                    FreezeBoundObject(child.BoundObject);
            }
            else if (boundObj is BVH)
            {
                BVH bvh = (BVH)boundObj;

                for (int index = 0; index < bvh.Primitives.Count(); ++index)
                {
                    Debug.Assert(bvh.Primitives[index] is BVHTriangle);
                    BVHPrimitive prim = bvh.Primitives[index];

                    BoundingBox3f boundingBox = prim.GetBoundingBox(bvh.Verts.Select(v => v.Position).ToArray());

                    if (!this.BoundingBox.Intersects(boundingBox))
                        continue;

                    this.FoundCollision = true;

                    IEnumerable<int> vertIndices = prim.GetVertexIndices();

                    foreach (int i in vertIndices)
                    {
                        Vector3f v = bvh.Verts[i].Position;

                        if (v.Z > this.MaximumHeight)
                            this.MaximumHeight = v.Z;
                        if (v.Z < this.MinimumHeight)
                            this.MinimumHeight = v.Z;

                        this.averageHeightCount++;
                        this.AverageHeight += v.Z;
                    }
                }

                for (int p = 0; p < bvh.Primitives.Length; p++)
                {
                    BVHPrimitive prim = bvh.Primitives[p];
                    FreezeSlope(prim, bvh.Verts.Select(v => v.Position).ToArray());
                }
            }
            else if (boundObj is Geometry)
            {
                Geometry geo = (Geometry)boundObj;

                for (int index = 0; index < geo.Primitives.Count(); ++index)
                {
                    BVHPrimitive prim = geo.Primitives[index];
                    BoundingBox3f boundingBox = prim.GetBoundingBox(geo.Verts.Select(v => v.Position).ToArray());

                    if (!this.BoundingBox.Intersects(boundingBox))
                        continue;

                    this.FoundCollision = true;

                    IEnumerable<int> vertIndices = prim.GetVertexIndices();

                    foreach (int i in vertIndices)
                    {
                        Vector3f v = geo.Verts[i].Position;

                        if (v.Z > this.MaximumHeight)
                            this.MaximumHeight = v.Z;
                        if (v.Z < this.MinimumHeight)
                            this.MinimumHeight = v.Z;

                        this.averageHeightCount++;
                        this.AverageHeight += v.Z;
                    }
                }

                for (int p = 0; p < geo.Primitives.Length; p++)
                {
                    BVHPrimitive prim = geo.Primitives[p];
                    FreezeSlope(prim, geo.Verts.Select(v => v.Position).ToArray());
                }
            }
            // Primitive Bound Types.
            else if (boundObj is Box)
            {
                this.FoundCollision = true;

                Box box = (Box)boundObj;
                Vector3f worldMax = box.Centroid + (box.Size / 2.0f);
                Vector3f worldMin = box.Centroid - (box.Size / 2.0f);
                if (worldMax.Z > this.MaximumHeight)
                    this.MaximumHeight = worldMax.Z;
                if (worldMin.Z < this.MinimumHeight)
                    this.MinimumHeight = worldMin.Z;

                this.averageHeightCount++;
                this.AverageHeight += worldMax.Z;
            }
#if false
            else if (boundObj is Capsule)
            {

            }
            else if (boundObj is Cylinder)
            {

            }
            else if (boundObj is Sphere)
            {

            }
#endif
            else
            {
                throw (new NotSupportedException(String.Format("BoundObject type not supported: '{0}'.",
                    boundObj.GetType().ToString())));
            }
        }

        /// <summary>
        /// Gather the slope data from any BVH primitive.
        /// </summary>
        /// <param name="prim"></param>
        /// <param name="verts"></param>
        private void FreezeSlope(BVHPrimitive prim, Vector3f[] verts)
        {
            if (!this.BoundingBox.Intersects(prim.GetBoundingBox(verts)))
                return;

            if (prim is BVHTriangle)
            {
                BVHTriangle tri = (BVHTriangle)prim;

                Plane plane = new Plane(verts[tri.Index0], verts[tri.Index1], verts[tri.Index2]);
                float angle = (float)Math.Acos((double)plane.Z);

                if (angle > this.MaximumSlope)
                    this.MaximumSlope = angle;
                if (angle < this.MinimumSlope)
                    this.MinimumSlope = angle;

                this.AverageSlope += angle;
                this.averageSlopeCount++;
            }
            else if (prim is BVHQuad)
            {
                BVHQuad quad = (BVHQuad)prim;

                Plane plane1 = new Plane(verts[quad.Index0], verts[quad.Index1], verts[quad.Index2]);
                float angle1 = (float)Math.Acos((double)plane1.Z);

                if (angle1 > this.MaximumSlope)
                    this.MaximumSlope = angle1;
                if (angle1 < this.MinimumSlope)
                    this.MinimumSlope = angle1;

                this.AverageSlope += angle1;

                Plane plane2 = new Plane(verts[quad.Index0], verts[quad.Index2], verts[quad.Index3]);
                float angle2 = (float)Math.Acos((double)plane2.Z);

                if (angle2 > this.MaximumSlope)
                    this.MaximumSlope = angle2;
                if (angle2 < this.MinimumSlope)
                    this.MinimumSlope = angle2;

                this.AverageSlope += angle2;
                this.averageSlopeCount += 2;
            }
        }

        /// <summary>
        /// Gathers our list of material names from all the bound objects that reside in our cell.
        /// </summary>
        /// <param name="boundObj"></param>
        private void CollectBoundMaterials(BoundObject boundObj)
        {
            if (boundObj is Composite)
            {
                Composite bound = (Composite)boundObj;
                foreach (CompositeChild child in bound.Children)
                    CollectBoundMaterials(child.BoundObject);
            }
            else if (boundObj is BVH)
            {
                CollectBVHMaterials((BVH)boundObj);
            }
            else if (boundObj is Geometry)
            {
                Geometry geometry = (Geometry)boundObj;
                Debug.Assert(!geometry.Bounds.IsEmpty);
                CollectBoundMaterial(geometry.Materials);
            }
            else if (boundObj is Box)
            {
                Box box = (Box)boundObj;
                CollectBoundMaterial(box.Materials);
            }
            else if (boundObj is Capsule)
            {
                Capsule capsule = (Capsule)boundObj;
                CollectBoundMaterial(capsule.Materials);
            }
            else if (boundObj is Cylinder)
            {
                Cylinder cylinder = (Cylinder)boundObj;
                CollectBoundMaterial(cylinder.Materials);
            }
            else if (boundObj is Sphere)
            {
                Sphere sphere = (Sphere)boundObj;
                CollectBoundMaterial(sphere.Materials);
            }
            else
            {
                throw (new NotSupportedException(String.Format("BoundObject type not supported to gather materials from: '{0}'.",
                    boundObj.GetType().ToString())));
            }
        }

        /// <summary>
        /// Collect all the materials in the material list if they don't already exist in our
        /// current list of materials.
        /// </summary>
        /// <param name="materialList"></param>
        private void CollectBoundMaterial(string[] materialList)
        {
            foreach (string material in materialList)
            {
                string materialName = "";
                string[] materialNameSplit = material.ToLower().Split('|');

                if (materialNameSplit.Length > 1)
                    materialName = materialNameSplit[0];

                if (!String.IsNullOrEmpty(materialName) && !this.MaterialList.Contains(materialName))
                    this.MaterialList.Add(materialName);
            }
        }

        /// <summary>
        /// BVHPrimitives need to be scanned differently because we need to check each type
        /// and make sure the primitive actually falls in our cell boundry so we only pull
        /// the materials from those that are in the boundry.
        /// </summary>
        /// <param name="bvh"></param>
        private void CollectBVHMaterials(BVH bvh)
        {
            foreach (BVHPrimitive primitive in bvh.Primitives)
            {
                // First make sure we are actually in our cell boundry before grabbing the material.
                if (!IsBVHPrmitiveInCellBoundry(primitive, bvh))
                    continue;

                if (primitive is BVHBox)
                {
                    BVHBox bvhBox = (BVHBox)primitive;
                    CollectBVHMaterial(bvh, bvhBox.MaterialIndex);
                }
                else if (primitive is BVHQuad)
                {
                    BVHQuad bvhQuad = (BVHQuad)primitive;
                    CollectBVHMaterial(bvh, bvhQuad.MaterialIndex);
                }
                else if (primitive is BVHTriangle)
                {
                    BVHTriangle bvhTri = (BVHTriangle)primitive;
                    CollectBVHMaterial(bvh, bvhTri.MaterialIndex);
                }
                else if (primitive is BVHCapsule)
                {
                    BVHCapsule bvhCap = (BVHCapsule)primitive;
                    CollectBVHMaterial(bvh, bvhCap.MaterialIndex);
                }
                else if (primitive is BVHCylinder)
                {
                    BVHCylinder bvhCyl = (BVHCylinder)primitive;
                    CollectBVHMaterial(bvh, bvhCyl.MaterialIndex);
                }
                else if (primitive is BVHSphere)
                {
                    BVHSphere bvhSphere = (BVHSphere)primitive;
                    CollectBVHMaterial(bvh, bvhSphere.MaterialIndex);
                }
            }
        }

        /// <summary>
        /// Collect the materials from the BVH object from the supplied material index.
        /// </summary>
        /// <param name="bvh"></param>
        /// <param name="materialIndex"></param>
        private void CollectBVHMaterial(BVH bvh, int materialIndex)
        {
            Debug.Assert(materialIndex > bvh.Materials.Length, "BVH material index is out of range.");

            string materialName = "";
            string[] materialNameSplit = bvh.Materials[materialIndex].ToLower().Split('|');
            
            if (materialNameSplit.Length > 1)
                materialName = materialNameSplit[0];

            if (!String.IsNullOrEmpty(materialName) && !this.MaterialList.Contains(materialName))
                this.MaterialList.Add(materialName);
        }

        /// <summary>
        /// Check to see if any of the verts from the bvh primitive fall in the cell boundry.
        /// </summary>
        /// <param name="bvh"></param>
        /// <param name="vertexIndices"></param>
        /// <returns></returns>
        private bool IsBVHPrmitiveInCellBoundry(BVHPrimitive prim, BVH bvh)
        {
            BoundingBox3f primBB = prim.GetBoundingBox(bvh.Verts.Select(v => v.Position).ToArray());
            return this.BoundingBox.Intersects(primBB);
        }
        #endregion // Private Methods

        #region Variables
        private int averageSlopeCount;
        private int averageHeightCount;
        #endregion
    }

} // RSG.Pipeline.CollisionProcessor.AmbientScanner namespace
