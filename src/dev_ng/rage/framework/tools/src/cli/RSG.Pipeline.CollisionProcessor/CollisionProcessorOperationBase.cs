﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging.Universal;
using RSG.Bounds;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.CollisionProcessor;

namespace RSG.Pipeline.CollisionProcessor
{

    /// <summary>
    /// Abstract base class for Collision Processor Operations.
    /// </summary>
    internal abstract class CollisionProcessorOperationBase : ICollisionProcessorOperation
    {
        #region Properties
        /// <summary>
        /// Operation handled by this class.
        /// </summary>
        public Operation Operation
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="op"></param>
        public CollisionProcessorOperationBase(Operation op)
        {
            this.Operation = op;
        }
        #endregion // Constructor(s)

        #region ICollisionProcessorOperation Methods
        /// <summary>
        /// Run the operation over the input asset collection.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="log">Log</param>
        /// <param name="task">Task to execute.</param>
        /// <param name="inputFiles">Input disk files (use inputAssets rather than reload these)</param>
        /// <param name="inputAssets">Input data set.</param>
        /// <param name="rebuild">Rebuild data.</param>
        /// <returns>true iff successful; false otherwise.</returns>
        public abstract bool Run(CommandOptions options, IUniversalLog log, CollisionProcessorTask task,
            IEnumerable<String> inputFiles, IEnumerable<BNDFile> inputAssets, bool rebuild);
        #endregion // ICollisionProcessorOperation Methods

        #region Protected Helper Methods
        /// <summary>
        /// Return most recent modified time for set of files.
        /// </summary>
        /// <param name="inputFiles"></param>
        /// <returns></returns>
        protected DateTime GetFilesLatestModTime(params String[] inputFiles)
        {
            DateTime latestModTime = DateTime.MinValue;
            foreach (String inputFilename in inputFiles)
            {
                if (!File.Exists(inputFilename))
                    continue;
                DateTime inputModTime = (new FileInfo(inputFilename)).LastWriteTimeUtc;
                if (inputModTime.IsLaterThan(latestModTime))
                    latestModTime = inputModTime;
            }
            return (latestModTime);
        }
        #endregion // Protected Helper Methods
    }

} // RSG.Pipeline.CollisionProcessor namespace
