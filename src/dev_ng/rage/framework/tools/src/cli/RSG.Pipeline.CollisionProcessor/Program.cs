﻿using System;
using System.Collections.Generic;
using SIO = System.IO;
using SIOC = System.IO.Compression;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Bounds;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.CollisionProcessor;

namespace RSG.Pipeline.CollisionProcessor
{

    /// <summary>
    /// Pipeline Collision Processor executable.
    /// </summary>
    class Program
    {
        #region Constants
        private static readonly String AUTHOR = "RSGEDI Tools";
        private static readonly String EMAIL = "*tools@rockstarnorth.com";

        private static readonly String LOG_CTX = "Collision Processor";
        private static readonly String OPTION_CONFIG = "config";
        private static readonly String OPTION_TASKNAME = "taskname";
        private static readonly String OPTION_REBUILD = "rebuild";

        private static readonly String EXT_STATIC_BOUNDS = ".ibn.zip";
        private static readonly String EXT_ZIP = ".zip";
        private static readonly String EXT_BND = ".bnd";
        #endregion // Constants

        /// <summary>
        /// Processor entry-point.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        [STAThread]
        static int Main(String[] args)
        {
            // Merge our options with the default pipeline command options.
            LongOption[] opts = new LongOption[] {
                new LongOption(OPTION_CONFIG, LongOption.ArgType.Required,
                    "Processor configuration file."),
                new LongOption(OPTION_TASKNAME, LongOption.ArgType.Required,
                    "XGE taskname; echoed to TTY for XGE error log parsing in AP3."),
                new LongOption(OPTION_REBUILD, LongOption.ArgType.None,
                    "Rebuild all; ignoring any cached intermediate files.")
            };
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.CreateUniversalLog(LOG_CTX);
            CommandOptions options = new CommandOptions(args, opts);
            int exit_code = Constants.Exit_Success;

            // Output task name for AP3 XGE logging compatibility.
            if (options.ContainsOption(OPTION_TASKNAME))
            {
                Console.WriteLine("TASK: {0}", (String)options[OPTION_TASKNAME]);
                Console.Out.Flush();
            }
            // Verify options.
            if (options.ShowHelp)
            {
                String processName = System.Diagnostics.Process.GetCurrentProcess().ProcessName;
                ShowUsage(processName, options);
                return (Constants.Exit_Success);
            }
            if (!options.ContainsOption(OPTION_CONFIG))
            {
                log.Error("'{0}' is a required option.", OPTION_CONFIG);
                Console.Error.WriteLine("'{0}' is a required option.", OPTION_CONFIG);
                Console.WriteLine(options.GetUsage());
                return (Constants.Exit_Failure);
            }

            bool rebuild = options.ContainsOption(OPTION_REBUILD) ? true : false;
            String configFilename = (String)options[OPTION_CONFIG];
            CollisionProcessorConfig config = CollisionProcessorConfig.Load(options.Branch, configFilename);
            try
            {
                // Load inputs.
                IEnumerable<BNDFile> inputCollisionData;
                using (ProfileContext ctx = new ProfileContext(log, "Loading {0} inputs.",
                    config.InputAssets.Count()))
                {
                    inputCollisionData = LoadInputs(log, config.InputAssets);
                    log.Message("Loaded {0} BND file inputs.", inputCollisionData.Count());
                }

                if (!inputCollisionData.Any())
                {
                    log.Error("No inputs; no tasks will be run.  Aborting.");
                }
                else
                {
                    foreach (CollisionProcessorTask task in config.Tasks)
                    {
                        if (!RunOperation(options, log, task, config.InputAssets, inputCollisionData, rebuild))
                        {
                            log.Error("Failed to run task for operation {0}.  Aborting.", 
                                task.Operation);
                            exit_code = Constants.Exit_Failure;
                            break;
                        }
                    }
                }

                if (LogFactory.HasError())
                    exit_code = Constants.Exit_Failure;
            }
            catch (Exception ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Unhandled exception in Collision Processor");
                exit_code = Constants.Exit_Failure;
            }
            finally 
            {
                LogFactory.ApplicationShutdown();
            }
            return (exit_code);
        }

        #region Private Methods
        /// <summary>
        /// Print usage information to console output.
        /// </summary>
        /// <param name="program"></param>
        /// <param name="options"></param>
        private static void ShowUsage(String program, CommandOptions options)
        {
            Console.WriteLine(program);
            Console.WriteLine("Usage:");
            Console.WriteLine(options.GetUsage());
        }

        /// <summary>
        /// Load all of the BND files specified as inputs (directly or from zips).
        /// </summary>
        /// <param name="log"></param>
        /// <param name="inputAssets"></param>
        /// <returns></returns>
        private static IEnumerable<BNDFile> LoadInputs(IUniversalLog log, 
            IEnumerable<String> inputAssets)
        {
            List<BNDFile> inputs = new List<BNDFile>();
            foreach (String inputAsset in inputAssets)
            {
                log.Message("Loading: {0}.", inputAsset);  
                inputs.AddRange(LoadInputsFromFile(inputAsset));
            }
            return (inputs);
        }

        /// <summary>
        /// Load input BND files from a file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private static IEnumerable<BNDFile> LoadInputsFromFile(String filename)
        {
            List<BNDFile> inputs = new List<BNDFile>();
            String extension = SIO.Path.GetExtension(filename);
            if (extension.Equals(EXT_ZIP))
            {
                using (SIOC.ZipArchive zipArchive = SIOC.ZipFile.OpenRead(filename))
                {
                    foreach (SIOC.ZipArchiveEntry zipEntry in zipArchive.Entries)
                    {
                        using (SIO.Stream stream2 = zipEntry.Open())
                            inputs.AddRange(LoadInputsFromStream(zipEntry.FullName, stream2));
                    }
                }
            }
            else if (extension.Equals(EXT_BND))
            {
                using (SIO.FileStream fs = SIO.File.OpenRead(filename))
                    inputs.AddRange(LoadInputsFromStream(filename, fs));
            }
            return (inputs);
        }

        /// <summary>
        /// Load input BND files from a stream.
        /// </summary>
        /// <param name="filename"></param>
        /// <param name="stream"></param>
        /// <returns></returns>
        private static IEnumerable<BNDFile> LoadInputsFromStream(String filename, SIO.Stream stream)
        {
            List<BNDFile> inputs = new List<BNDFile>();
            String extension = SIO.Path.GetExtension(filename);
            if (extension.Equals(EXT_ZIP))
            {
                using (SIOC.ZipArchive zipArchive = new SIOC.ZipArchive(stream, SIOC.ZipArchiveMode.Read))
                {
                    foreach (SIOC.ZipArchiveEntry zipEntry in zipArchive.Entries)
                    {
                        using (SIO.Stream stream2 = zipEntry.Open())
                            inputs.AddRange(LoadInputsFromStream(zipEntry.FullName, stream2));
                    }
                }
            }
            else if (extension.Equals(EXT_BND))
            {
                inputs.Add(BNDFile.Load(stream));
            }
            return (inputs);
        }

        /// <summary>
        /// Run the task operation on the previously loaded input data collection.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="log"></param>
        /// <param name="task"></param>
        /// <param name="inputFiles"></param>
        /// <param name="inputAssets"></param>
        /// <param name="rebuild"></param>
        /// <returns></returns>
        private static bool RunOperation(CommandOptions options, IUniversalLog log, 
            CollisionProcessorTask task, IEnumerable<String> inputFiles, 
            IEnumerable<BNDFile> inputAssets, bool rebuild)
        {
            switch (task.Operation)
            {
                case Operation.AmbientScanner:
                    {
                        AmbientScanner.AmbientScannerOperation scannerOp =
                            new AmbientScanner.AmbientScannerOperation();
                        return (scannerOp.Run(options, log, task, inputFiles, inputAssets, rebuild));
                    }
                default:
                    log.Error("Invalid operation or operation not implemented: {0}.  Ignoring.", task.Operation);
                    return (false);
            }
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.CollisionProcessor namespace
