﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Configuration.ROS;
using RSG.Base.Logging;
using RSG.ROS;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Dto.ProfileStats;

namespace RSG.Statistics.Console.Util
{
    /// <summary>
    /// Queries the social club services for the profile stat definitions and uploads them to the db.
    /// </summary>
    public class ProfileStatDefinitionProcessor
    {
        #region Member Data
        /// <summary>
        /// Log object for the sync service.
        /// </summary>
        protected ILog m_log;

        /// <summary>
        /// Local ROS config object.
        /// </summary>
        protected IROSConfig m_rosConfig;

        /// <summary>
        /// Statistics server to communicate with.
        /// </summary>
        protected IStatsServer m_server;
        #endregion // Member Data
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public ProfileStatDefinitionProcessor(IStatsServer server)
        {
            m_log = LogFactory.CreateUniversalLog(GetType().Name);
            m_rosConfig = ConfigFactory.CreateROSConfig();
            m_server = server;
        }
        #endregion // Constructor(s)

        #region Public Methods
        /// <summary>
        /// Synchronises profile stat definitions.
        /// </summary>
        public void SyncProfileStatDefinitions()
        {
            AdminAuthService authService = new AdminAuthService(m_rosConfig);
            ProfileStatService statService = new ProfileStatService(m_rosConfig);

            // Loop over all the platforms getting the list of all definitions.
            IDictionary<int, ProfileStatDefinitionDto> allDefinitions = new Dictionary<int, ProfileStatDefinitionDto>();

            IList<ROSPlatform> platforms = new List<ROSPlatform>();
            platforms.Add(ROSPlatform.PS3);
            platforms.Add(ROSPlatform.Xbox360);

            foreach (ROSPlatform platform in platforms)
            {
                // Create an authentication ticket for this platform.
                AuthTicket ticket = CreateAuthenticationTicket(authService, platform);
                if (ticket == null || !ticket.IsValid)
                {
                    m_log.Error("Failed to get valid ROS authentication ticket for {0}.", platform);
                    continue;
                }
                m_log.Debug("Ticket: {0}", ticket.EncryptedAuth);

                m_log.Profile("Retrieving definitions for {0}", platform);
                IList<ProfileStatDefinitionDto> definitions = GetProfileStatDefinitions(statService, ticket);
                m_log.ProfileEnd();

                // Merging with the existing ones.
                foreach (ProfileStatDefinitionDto definition in definitions)
                {
                    if (!allDefinitions.ContainsKey(definition.RosId))
                    {
                        allDefinitions[definition.RosId] = definition;
                    }
                }
            }

            // Send all the definitions to the stats server.
            if (allDefinitions.Any())
            {
                m_log.Message("{0} definitions to sync.", allDefinitions.Count);
                m_log.Profile("Updating definitions on server.");
                using (ProfileStatClient client = new ProfileStatClient(m_server))
                {
                    client.UpdateDefinitions(allDefinitions.Values.ToList());
                }
                m_log.ProfileEnd();
            }
            else
            {
                m_log.Error("No definitions to sync.");
            }
        }
        #endregion // Public Methods

        #region Private Methods
        /// <summary>
        /// Creates a ROS authentication ticket for a particular platform
        /// </summary>
        private AuthTicket CreateAuthenticationTicket(AdminAuthService authService, ROSPlatform platform)
        {
            m_log.Message("Creating authentication ticket for {0}, {1}.", m_rosConfig.GameTitle, platform);
            AuthTicket ticket = authService.CreateTicket(m_rosConfig.Username, m_rosConfig.Password, m_rosConfig.GameTitle, platform);

            // Check that we got a valid ticket
            if (ticket == null || !ticket.IsValid)
            {
                // Clear out the login details as they are incorrect
                m_rosConfig.ClearLoginDetails();
                m_rosConfig.Save(true);
                ticket = null;
            }
            else
            {
                // Save the local config to persist the ROS username/password
                m_rosConfig.Save(true);
            }

            return ticket;
        }

        /// <summary>
        /// Get the profile stat definitions from social club services.
        /// </summary>
        private IList<ProfileStatDefinitionDto> GetProfileStatDefinitions(ProfileStatService service, AuthTicket ticket)
        {
            IList<ProfileStatDefinitionDto> definitions = new List<ProfileStatDefinitionDto>();

            using (Stream dataStream = service.ReadMetadata(ticket))
            {
                if (dataStream != null)
                {
                    XDocument xDoc = XDocument.Load(dataStream);
                    XmlNamespaceManager nsManager = new XmlNamespaceManager(new NameTable());
                    nsManager.AddNamespace("r", "ReadMetadataResponse");

                    foreach (XElement statElem in xDoc.XPathSelectElements("/r:Response/r:Metadata/r:Stat", nsManager))
                    {
                        XAttribute idAtt = statElem.Attribute("Id");
                        XAttribute nameAtt = statElem.Attribute("Name");
                        XAttribute typeAtt = statElem.Attribute("Type");
                        XAttribute defaultAtt = statElem.Attribute("Default");
                        XAttribute commentAtt = statElem.Attribute("Comment");
                        if (idAtt != null && nameAtt != null && typeAtt != null && defaultAtt != null && commentAtt != null)
                        {
                            definitions.Add(new ProfileStatDefinitionDto
                                {
                                    RosId = Int32.Parse(idAtt.Value),
                                    Name = nameAtt.Value,
                                    Type = typeAtt.Value,
                                    DefaultValue = defaultAtt.Value,
                                    Comment = commentAtt.Value,
                                });
                        }
                    }
                }
                else
                {
                    m_log.Error("No profile stat definition data received.");
                }
            }

            return definitions;
        }
        #endregion // Private Methods
    } // ProfileStatDefinitionProcessor
}
