﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Extensions;
using RSG.Base.Logging.Universal;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Dto.Reports;

namespace RSG.Statistics.Console.Util
{
    /// <summary>
    /// Helper class for generating presets.
    /// </summary>
    internal class PresetHelper
    {
        #region Constants
        /// <summary>
        /// Location of the file that contains the report/preset information.
        /// </summary>
        private const String c_configFile = @"Config\PresetConfig.xml";
        #endregion // Constants

        #region Private Classes
        /// <summary>
        /// 
        /// </summary>
        private class ReportItem
        {
            public String Name { get; set; }
        }
        #endregion // Private Classes

        #region Member Data
        private IUniversalLog m_log;
        private IStatsServer m_server;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public PresetHelper(IStatsServer server, IUniversalLog log)
        {
            m_log = log;
            m_server = server;
        }
        #endregion // Constructor(s)

        #region Caching Functions
        /// <summary>
        /// Caches reports between the two date time's specified.
        /// </summary>
        public void CacheReports(DateTime start, DateTime end, String presetPrefix)
        {
            try
            {
                XDocument doc = XDocument.Load(Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), c_configFile));

                foreach (XElement presetElem in doc.XPathSelectElements("/config/presets/preset"))
                {
                    // First parse all parameters
                    IDictionary<String, String> presetParameters = new Dictionary<String, String>();
                    presetParameters.AddRange(ExtractParams(presetElem));
                    
                    // Next go over the reports adding any additional parameters.
                    foreach (XElement reportElem in doc.XPathSelectElements("/config/reports/report"))
                    {
                        XAttribute nameAtt = reportElem.Attribute("name");
                        if (nameAtt != null)
                        {
                            String reportName = nameAtt.Value;
                            IDictionary<String, String> reportParameters = new Dictionary<String, String>(presetParameters);
                            reportParameters.AddRange(ExtractParams(reportElem));

                            if (RunReport(reportName, reportParameters))
                            {
                                m_log.Message("Successfully ran the {0} report.", reportName);
                            }
                        }
                    }

                    // Check whether we should create a preset entry for this set of parameters?.
                    String presetName;
                    if (ShouldCreatePrefixEntry(presetElem, presetPrefix, out presetName))
                    {
                        ReportPreset preset = new ReportPreset(presetName);
                        preset.Parameters.Add(new ReportPresetParameter("platforms", presetParameters["PlatformNames"]));
                        preset.Parameters.Add(new ReportPresetParameter("radio-checked", "dates-field"));
                        preset.Parameters.Add(new ReportPresetParameter("date-from", start.ToString("dd\\/MM\\/yyyy")));
                        preset.Parameters.Add(new ReportPresetParameter("date-to", end.ToString("dd\\/MM\\/yyyy")));
                        if (CreatePreset(preset))
                        {
                            m_log.Message("Successfully created the {0} preset.", presetName);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                m_log.ToolException(ex, "Failed to parse the preset config file.");
            }
        }

        /// <summary>
        /// Extracts all parameters that are children of the passed in element.
        /// </summary>
        private IEnumerable<KeyValuePair<String, String>> ExtractParams(XElement elem)
        {
            foreach (XElement paramElem in elem.Elements("param"))
            {
                String name, value;
                if (ExtractParam(paramElem, out name, out value))
                {
                    yield return new KeyValuePair<String, String>(name, value);
                }
            }
        }

        /// <summary>
        /// Extracts the name/value attributes from an element.
        /// </summary>
        private bool ExtractParam(XElement elem, out String name, out String value)
        {
            XAttribute nameAtt = elem.Attribute("name");
            XAttribute valueAtt = elem.Attribute("value");

            if (nameAtt != null && valueAtt != null)
            {
                name = nameAtt.Value;
                value = valueAtt.Value;
                return true;
            }
            else
            {
                name = null;
                value = null;
                return false;
            }
        }

        /// <summary>
        /// Runs the specified report with the given parameters.
        /// </summary>
        private bool RunReport(String name, IDictionary<String, String> parameterValues)
        {
            try
            {
                using (ReportClient client = new ReportClient(m_server))
                {
                    // Set the list of parameters required for this report.
                    List<ReportParameter> parameters = client.GetReportParameters(name);
                    SetParameterValues(parameters, parameterValues);

                    // Next run the report.
                    client.RunReport(name, true, parameters);
                }
                return true;
            }
            catch (System.Exception ex)
            {
                m_log.ToolException(ex, "Failed to run the {0} report.", name);
                return false;
            }
        }

        /// <summary>
        /// Sets the report parameter dto values based on those passed in.
        /// </summary>
        private void SetParameterValues(List<ReportParameter> parameters, IDictionary<String, String> values)
        {
            foreach (ReportParameter parameter in parameters)
            {
                if (values.ContainsKey(parameter.Name))
                {
                    parameter.Value = values[parameter.Name];
                }
            }
        }

        /// <summary>
        /// Determines whether we should create an entry in the prefix table for this preset.
        /// </summary>
        private bool ShouldCreatePrefixEntry(XElement elem, String prefix, out String name)
        {
            XAttribute genAtt = elem.Attribute("generate_preset");
            XAttribute nameAtt = elem.Attribute("name");

            if (genAtt != null && nameAtt != null && Boolean.Parse(genAtt.Value) == true)
            {
                name = String.Format(nameAtt.Value, prefix);
                return true;
            }
            else
            {
                name = null;
                return false;
            }
        }

        private bool CreatePreset(ReportPreset preset)
        {
            try
            {
                using (ReportClient client = new ReportClient(m_server))
                {
                    // Next run the report.
                    client.CreatePreset(preset);
                }
                return true;
            }
            catch (System.Exception ex)
            {
                m_log.ToolException(ex, "Failed to create the {0} preset.", preset.Name);
                return false;
            }
        }
        #endregion // Caching Functions
    } // PresetHelper
}
