﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.OS;
using RSG.Editor.Controls;
using RSG.Pipeline.Core;
using RSG.Statistics.Client.Common;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common.Config;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Console.Util;

namespace RSG.Statistics.Console
{
    /// <summary>
    /// Statistics Console Application
    /// </summary>
    public class StatisticsConsoleApp : RsInteractiveConsoleApplication
    {
        #region Program Entry Point
        /// <summary>
        /// Program entry point.
        /// </summary>
        /// <param name="args"></param>
        [STAThread]
        static void Main(string[] args)
        {
            StatisticsConsoleApp app = new StatisticsConsoleApp();
            app.Run();
        }
        #endregion

        #region Constants
        /// <summary>
        /// Log context.
        /// </summary>
        private const String LogCtx = "Console";

        /// <summary>
        /// Location of the build version file relative to the build dir
        /// </summary>
        private const String BuildVersionFile = @"$(common)\data\version.txt";

        /// <summary>
        /// The tag that is found on the line preceding the version number
        /// </summary>
        private const String VersionNumberTag = @"[VERSION_NUMBER]";
        #endregion

        #region Fields
        /// <summary>
        /// Server we are connected to.
        /// </summary>
        private IStatsServer _server;

        /// <summary>
        /// Branch we are using to get the data from.
        /// </summary>
        private IBranch _branch;

        /// <summary>
        /// Private field for the <see cref="AssetSyncService"/> property.
        /// </summary>
        private AssetSyncService _assetSyncService;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="StatisticsConsoleApp"/> class.
        /// </summary>
        public StatisticsConsoleApp()
            : base()
        {
            // Register the command line arguments.
            RegisterCommandlineArg("server", LongOption.ArgType.Required, "Statistics server to connect to.");

            // Register the commands we wish to support.
            RegisterConsoleCommand("sync-assets", SyncAssetsCommandHandler, "<ASSET> Syncs a particular asset type to the server.");
            RegisterConsoleCommand("sync-stats", SyncAssetStatsCommandHandler, "<ASSET> <BUILD> Syncs per build stats for a particular asset type.");
            RegisterConsoleCommand("sync-scenexml", SyncSceneXmlCommandHandler, "<FILE(S)> Syncs a particular scene-xml file to the server.");
            RegisterConsoleCommand("sync-profilestatdefs", SyncProfileStatDefsCommandHandler, "Syncs profile stat definitions from social club services.");
            RegisterConsoleCommand("build-flag", SetBuildFlagCommandHandler, "<BUILD> <FLAG> <VALUE> Sets a particular flag for a build.");
            RegisterConsoleCommand("preset", CreatePresetCommandHandler, "<DAILY|WEEKLY> [DATE] Creates a particular type of preset with an optional date.");
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets a semi-colon separate list of the application authors email addresses. These
        /// are the addresses which are used by the unhandled exception dialog to determine
        /// where the mail should be sent by default.
        /// e.g.
        /// *tools@rockstarnorth.com
        /// michael.taschler@rockstarnorth.com;dave.evans@rockstarnorth.com
        /// </summary>
        public override string AuthorEmailAddress
        {
            get { return "RDR3Tools@rockstarsandiego.com"; }
        }

        /// <summary>
        /// Text to display in the console's caret.
        /// </summary>
        protected override string CaretText
        {
            get { return String.Format("{0}@{1}", Environment.UserName, _server.Address); }
        }

        /// <summary>
        /// Property for lazily constructing the asset sync service.
        /// </summary>
        private AssetSyncService AssetSyncService
        {
            get
            {
                if (_assetSyncService == null)
                {
                    _assetSyncService = new AssetSyncService(_branch, _server);
                }
                return _assetSyncService;
            }
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Sets up the console and stars a task that will run it.
        /// </summary>
        protected override bool OnConsoleStartup()
        {
            IConfig config = ConfigFactory.CreateConfig();
            _branch = config.Project.DefaultBranch;

            IStatisticsConfig statsConfig = new StatisticsConfig(_branch);

            if (CommandLineOptions.HasOption("server"))
            {
                String serverName = (String)CommandLineOptions["server"];
                Debug.Assert(statsConfig.Servers.ContainsKey(serverName), "Requested an unknown server");
                _server = statsConfig.Servers[serverName];
            }
            else
            {
                _server = statsConfig.DefaultServer;
            }

            return true;
        }
        #endregion

        #region Command Handlers
        /// <summary>
        /// Command for syncing asset names.
        /// </summary>
        /// <param name="command">
        /// The command that is being handled by this method.
        /// </param>
        /// <param name="arguments">
        /// The arguments that have be sent with the command.
        /// </param>
        protected void SyncAssetsCommandHandler(string command, string[] arguments)
        {
            if (1 != arguments.Length)
            {
                Log.ErrorCtx(LogCtx, "Invalid number of arguments ({0}); expecting 1: <ASSET>.",
                    arguments.Length);
                HelpCommandHandler(command, arguments);
                return;
            }

            String assetType = (String)arguments[0];
            switch (assetType.ToUpper())
            {
                case "ARCHETYPES":
                    AssetSyncService.SyncArchetypes();
                    break;
                case "CAPTURESTRINGS":
                    AssetSyncService.SyncCaptureRelatedStrings();
                    break;
                case "CASHPACKS":
                    AssetSyncService.SyncCashPacks();
                    break;
                case "CLIPDICTIONARIES":
                    AssetSyncService.SyncClipDictionaries();
                    break;
                case "CHARACTERS":
                    AssetSyncService.SyncCharacters();
                    break;
                case "CUTSCENES":
                    AssetSyncService.SyncCutscenes();
                    break;
                case "DRAWLISTS":
                    AssetSyncService.SyncDrawLists();
                    break;
                case "ENTITIES":
                    AssetSyncService.SyncEntities();
                    break;
                case "LEVELS":
                    AssetSyncService.SyncLevels();
                    break;
                case "MAPAREAS":
                    AssetSyncService.SyncMapAreas();
                    break;
                case "MAPSECTIONS":
                    AssetSyncService.SyncMapSections();
                    break;
                case "MEMORYNAMES":
                    AssetSyncService.SyncMemoryNames();
                    break;
                case "MINIGAMEVARIANTS":
                    AssetSyncService.SyncMiniGameVariants();
                    break;
                case "MISSIONS":
                    AssetSyncService.SyncMissions();
                    break;
                case "MOVIES":
                    AssetSyncService.SyncMovies();
                    break;
                case "PROPERTIES":
                    AssetSyncService.SyncProperties();
                    break;
                case "RADIOSTATIONS":
                    AssetSyncService.SyncRadioStations();
                    break;
                case "RANKS":
                    AssetSyncService.SyncRanks();
                    break;
                case "ROOMS":
                    AssetSyncService.SyncRooms();
                    break;
                case "SHOPS":
                    AssetSyncService.SyncShops();
                    break;
                case "SHOPITEMS":
                    AssetSyncService.SyncShopItems();
                    break;
                case "TVSHOWS":
                    AssetSyncService.SyncTVShows();
                    break;
                case "UNLOCKS":
                    AssetSyncService.SyncUnlocks();
                    break;
                case "VEHICLES":
                    AssetSyncService.SyncVehicles();
                    break;
                case "WEAPONS":
                    AssetSyncService.SyncWeapons();
                    break;
                case "WEBSITES":
                    AssetSyncService.SyncWebSites();
                    break;
                case "XPCATEGORIES":
                    AssetSyncService.SyncXPCategories();
                    break;
                case "XPTYPES":
                    AssetSyncService.SyncXPTypes();
                    break;
                default:
                    Log.ErrorCtx(LogCtx, "Unknown asset type supplied: {0}.", assetType);
                    break;
            }
        }

        /// <summary>
        /// Command for syncing asset stats for a particular build.
        /// </summary>
        /// <param name="command">
        /// The command that is being handled by this method.
        /// </param>
        /// <param name="arguments">
        /// The arguments that have be sent with the command.
        /// </param>
        protected void SyncAssetStatsCommandHandler(string command, string[] arguments)
        {
            if (arguments.Length < 1 || arguments.Length > 2)
            {
                Log.ErrorCtx(LogCtx, "Invalid number of arguments ({0}); expecting 2: <ASSET> [BUILD].",
                    arguments.Length);
                HelpCommandHandler(command, arguments);
                return;
            }

            String assetType = (String)arguments[0];
            String buildIdentifier = null;

            if (arguments.Length > 1)
            {
                buildIdentifier = (String)arguments[1];
            }
            else
            {
                buildIdentifier = GetBuildVersion(_branch);
                if (buildIdentifier == null)
                {
                    Log.ErrorCtx(LogCtx, "Unable to determine the local build version from the {0} file.", BuildVersionFile);
                    return;
                }
            }

            // Get the build dto object for this identifier.
            BuildDto build = GetBuild(buildIdentifier);

            // Which asset type should we be syncing stats for?
            switch (assetType.ToUpper())
            {
                case "ARCHETYPES":
                    AssetSyncService.SyncArchetypeStats(build);
                    break;
                case "CARGENS":
                    AssetSyncService.SyncCarGens(build);
                    break;
                case "CLIPDICTIONARIES":
                    AssetSyncService.SyncClipDictionaryStats(build);
                    break;
                case "CHARACTERS":
                    AssetSyncService.SyncCharacterStats(build);
                    break;
                case "CUTSCENES":
                    AssetSyncService.SyncCutsceneStats(build);
                    break;
                case "ENTITIES":
                    AssetSyncService.SyncEntityStats(build);
                    break;
                case "LEVELS":
                    AssetSyncService.SyncLevelStats(build);
                    break;
                case "MAPAREAS":
                    AssetSyncService.UpdateMapAreaStats(build);
                    break;
                case "MAPHIERARCHIES":
                    AssetSyncService.SyncMapHierarchy(build);
                    break;
                case "MAPSECTIONS":
                    AssetSyncService.UpdateMapSectionStats(build);
                    break;
                case "SPAWNPOINTS":
                    AssetSyncService.SyncSpawnPoints(build);
                    break;
                case "VEHICLES":
                    AssetSyncService.SyncVehicleStats(build);
                    break;
                case "WEAPONS":
                    AssetSyncService.SyncWeaponStats(build);
                    break;
                default:
                    Log.ErrorCtx(LogCtx, "Unknown asset type supplied: {0}.", assetType);
                    break;
            }
        }

        /// <summary>
        /// Parses the game's build version file to determine the build.
        /// </summary>
        /// <param name="branch"></param>
        /// <returns></returns>
        private String GetBuildVersion(IBranch branch)
        {
            String versionFile = branch.Environment.Subst(BuildVersionFile);
            Debug.Assert(File.Exists(versionFile), "Build version file doesn't exist on your machine.");
            if (!File.Exists(versionFile))
            {
                return null;
            }

            // Read the file line by line looking for the line containing the version number
            String buildIdentifer = null;

            using (StreamReader fileStream = new StreamReader(versionFile))
            {
                String line;
                bool extractFromLine = false;

                while ((line = fileStream.ReadLine()) != null)
                {
                    if (extractFromLine)
                    {
                        // Check whether the line matches the build format.
                        if (Regex.IsMatch(line, @"^(?'major'\d+)\.?(?'minor'\d+)?$"))
                        {
                            buildIdentifer = line;
                        }
                        break;
                    }

                    // Next line will contain the build version number.
                    if (line.Contains(VersionNumberTag))
                    {
                        extractFromLine = true;
                    }
                }
            }

            return buildIdentifer;
        }

        /// <summary>
        /// Gets a particular build based on the majoe/minor versions.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        private BuildDto GetBuild(String buildIdentifier)
        {
            // Try and get the build dto object.
            BuildDto build = null;

            using (BuildClient buildClient = new BuildClient(_server))
            {
                // Check whether this build already exists on the stats server
                try
                {
                    build = buildClient.GetByIdentifier(buildIdentifier);
                }
                catch (System.Exception)
                {
                    // Intentionally empty.
                }

                if (build == null)
                {
                    // Build didn't exist, so create a new one
                    Log.MessageCtx(LogCtx, "Creating new build with identifier '{0}'", buildIdentifier);
                    build = new BuildDto();
                    build.Identifier = buildIdentifier;
                    build.GameVersion = GetGameVersionForBuild(buildIdentifier);
                    build = buildClient.PutAsset(buildIdentifier, build);
                }
                else
                {
                    Log.MessageCtx(LogCtx, "Retrieved existing build with identifier '{0}'", buildIdentifier);
                }
            }

            return build;
        }

        /// <summary>
        /// Attempts to convert the build identifier into a version that the game would use.
        /// </summary>
        /// <param name="buildIdentifier"></param>
        private uint? GetGameVersionForBuild(String buildIdentifier)
        {
            Regex validBuildRegex = new Regex(@"^(?'major'\d+)\.?(?'minor'\d+)?$");
            uint? gameVersion = null;

            Match match = validBuildRegex.Match(buildIdentifier);
            if (match.Success)
            {
                Group majorGroup = match.Groups["major"];
                Group minorGroup = match.Groups["minor"];

                uint major = UInt32.Parse(majorGroup.Value);
                uint minor = 0;

                if (minorGroup.Success)
                {
                    minor = UInt32.Parse(minorGroup.Value);
                }

                gameVersion = (major << 4) | minor;
            }

            return gameVersion;
        }

        /// <summary>
        /// Syncs the contents of a scene xml file to the server.
        /// </summary>
        /// <param name="command">
        /// The command that is being handled by this method.
        /// </param>
        /// <param name="arguments">
        /// The arguments that have be sent with the command.
        /// </param>
        protected void SyncSceneXmlCommandHandler(string command, string[] arguments)
        {
            if (arguments.Length < 1)
            {
                Log.ErrorCtx(LogCtx, "Invalid number of arguments ({0}); expecting 1: <FILE(S)>.",
                    arguments.Length);
                HelpCommandHandler(command, arguments);
                return;
            }

            // Determine the list of files to process.
            String[] filesToProcess = arguments;
            if (arguments[0].ToUpper() == "ALL")
            {
                IProcessorCollection processors = new ProcessorCollection(_branch.Project.Config);
                IContentTree contentTree = RSG.Pipeline.Content.Factory.CreateTree(_branch);
                RSG.Pipeline.Content.ContentTreeHelper treeHelper = new RSG.Pipeline.Content.ContentTreeHelper(contentTree);
                IContentNode[] nodes = treeHelper.GetAllMapSceneXmlNodes();

                IList<String> filesList = new List<String>();
                foreach (RSG.Pipeline.Content.IFilesystemNode node in nodes.OfType<RSG.Pipeline.Content.IFilesystemNode>())
                {
                    filesList.Add(node.AbsolutePath);
                }
                filesToProcess = filesList.ToArray();
            }

            // Do the sync
            SceneXmlProcessor processor = new SceneXmlProcessor(_server);
            foreach (String filename in filesToProcess.Distinct())
            {
                if (!File.Exists(filename))
                {
                    Log.ErrorCtx(LogCtx, "'{0}' does not exist on disk.  Please verify the supplied file path.", filename);
                    continue;
                }
                processor.SyncSceneXmlData(filename);
            }
        }

        /// <summary>
        /// Syncs the profile stat definitions from social club services.
        /// </summary>
        /// <param name="command">
        /// The command that is being handled by this method.
        /// </param>
        /// <param name="arguments">
        /// The arguments that have be sent with the command.
        /// </param>
        protected void SyncProfileStatDefsCommandHandler(string command, string[] arguments)
        {
            if (arguments.Length != 0)
            {
                Log.ErrorCtx(LogCtx, "Invalid number of arguments ({0}); expecting 0.",
                    arguments.Length);
                HelpCommandHandler(command, arguments);
                return;
            }

            // Do the sync
            ProfileStatDefinitionProcessor defProcessor = new ProfileStatDefinitionProcessor(_server);
            defProcessor.SyncProfileStatDefinitions();
        }

        /// <summary>
        /// Sets a build flag on the server.
        /// </summary>
        /// <param name="command">
        /// The command that is being handled by this method.
        /// </param>
        /// <param name="arguments">
        /// The arguments that have be sent with the command.
        /// </param>
        protected void SetBuildFlagCommandHandler(String command, String[] arguments)
        {
            if (3 != arguments.Length)
            {
                Log.ErrorCtx(LogCtx, "Invalid number of arguments ({0}); expecting 3: <BUILD> <FLAG> <VALUE>.",
                    arguments.Length);
                HelpCommandHandler(command, arguments);
                return;
            }

            // Parse the parameters
            String buildIdentifier = (String)arguments[0];
            String flag = (String)arguments[1];
            bool value;
            if (!Boolean.TryParse((String)arguments[2], out value))
            {
                Log.ErrorCtx(LogCtx, "Invalid value argument ({0}); expecting a boolean (True/False).",
                       (String)arguments[2]);
            }

            // Get the build dto object for this identifier.
            if (buildIdentifier.ToLower() == "local")
            {
                buildIdentifier = GetBuildVersion(_branch);
                if (buildIdentifier == null)
                {
                    Log.ErrorCtx(LogCtx, "Unable to determine the local build version from the {0} file.", BuildVersionFile);
                    return;
                }
            }

            BuildDto build = GetBuild(buildIdentifier);
            bool flagSet = false;

            switch (flag.ToUpper())
            {
                case "ASSETSTATS":
                    build.HasAssetStats = value;
                    flagSet = true;
                    break;
                default:
                    Log.ErrorCtx(LogCtx, "Unknown build flag supplied: {0}.", flag);
                    break;
            }

            // Only update the build on the server if we changed a value.
            if (flagSet)
            {
                using (BuildClient client = new BuildClient(_server))
                {
                    client.PutAsset(build.Identifier, build);
                }
            }
        }

        /// <summary>
        /// Command for generating a particular type of preset.
        /// </summary>
        /// <param name="command">
        /// The command that is being handled by this method.
        /// </param>
        /// <param name="arguments">
        /// The arguments that have be sent with the command.
        /// </param>
        protected void CreatePresetCommandHandler(string command, string[] arguments)
        {
            if (arguments.Length < 1 || arguments.Length > 2)
            {
                Log.ErrorCtx(LogCtx, "Invalid number of arguments ({0}); expecting 1 or 2: <DAILY|WEEKLY> [DATE].",
                    arguments.Length);
                HelpCommandHandler(command, arguments);
                return;
            }

            // Parse the arguments.
            String type = arguments[0].ToUpper();
            DateTime initDate = DateTime.UtcNow;
            bool dateSupplied = false;
            if (arguments.Length > 1)
            {
                dateSupplied = true;
                if (!DateTime.TryParse(arguments[1], out initDate))
                {
                    Log.ErrorCtx(LogCtx, "Unparseable date supplied as second argument. Needs to be of format 2013-01-01.");
                    HelpCommandHandler(command, arguments);
                    return;
                }
            }

            // Determine the start/end dates based on the supplied parameters.
            DateTime startDate;
            DateTime endDate;
            String presetPrefix;

            if (type == "DAILY")
            {
                // If a date was supplied create cached reports for that day, otherwise do it for yesterday.
                if (dateSupplied)
                {
                    startDate = initDate.Date;
                    endDate = initDate.Date.AddDays(1);
                }
                else
                {
                    startDate = initDate.Date.AddDays(-1);
                    endDate = initDate.Date;
                }

                presetPrefix = startDate.ToString("d");
            }
            else if (type == "WEEKLY")
            {
                // If a date was supplied create cached reports for that day, otherwise do it for last week.
                if (dateSupplied)
                {
                    startDate = initDate.StartOfWeek(DayOfWeek.Sunday);
                    endDate = initDate.StartOfWeek(DayOfWeek.Sunday).AddDays(7);
                }
                else
                {
                    startDate = initDate.StartOfWeek(DayOfWeek.Sunday).AddDays(-7);
                    endDate = initDate.StartOfWeek(DayOfWeek.Sunday);
                }

                // Determine which week of the year this is for.
                GregorianCalendar cal = new GregorianCalendar(GregorianCalendarTypes.Localized);
                int week = cal.GetWeekOfYear(startDate, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Sunday);

                presetPrefix = String.Format("{0} - Week {1} ({2:d} - {3:d})", startDate.Year, week, startDate, endDate);
            }
            else
            {
                Log.ErrorCtx(LogCtx, "Invalid argument; expecting 1: <DAILY|WEEKLY>.");
                HelpCommandHandler(command, arguments);
                return;
            }

            // Use the preset helper to generate all the required reports.
            PresetHelper helper = new PresetHelper(_server, Log);
            helper.CacheReports(startDate, endDate, presetPrefix);
        }
        #endregion
    }
}
