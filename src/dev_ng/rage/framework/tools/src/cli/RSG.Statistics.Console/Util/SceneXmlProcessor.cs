﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Common.Config;
using SceneXmlModel = RSG.SceneXml;
using SceneXmlDto = RSG.Statistics.Common.Dto.SceneXml;
using RSG.Base.Math;
using RSG.SceneXml.Material;

namespace RSG.Statistics.Console.Util
{
    /// <summary>
    /// 
    /// </summary>
    public class SceneXmlProcessor
    {
        #region Member Data
        /// <summary>
        /// Log object for the sync service.
        /// </summary>
        protected ILog _log;

        /// <summary>
        /// Statistics server to communicate with.
        /// </summary>
        protected IStatsServer _server;
        #endregion // Member Data
        
        #region Constructor(s)
        /// <summary>
        /// 
        /// </summary>
        public SceneXmlProcessor(IStatsServer server)
        {
            _log = LogFactory.CreateUniversalLog(GetType().Name);
            _server = server;
        }
        #endregion // Constructor(s)
        
        #region Syncing Scene Xml Data
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filename"></param>
        public void SyncSceneXmlData(String filename)
        {
            SceneXmlModel.Scene sceneModel = new SceneXmlModel.Scene(filename, SceneXml.LoadOptions.All, true);
            SceneXmlDto.Scene sceneDto = ConvertSceneModelToDto(sceneModel);

            using (Stream stream = File.OpenRead(filename))
            {
                SceneXmlManipulationClient client = new SceneXmlManipulationClient(_server);
                try
                {
                    _log.Message("Syncing scene xml data for '{0}'.", filename);
                    client.PopulateSceneXmlFile(filename, sceneDto);
                }
                catch (System.Exception ex)
                {
                    _log.ToolException(ex, "Unhandled exception while syncing scene xml data.");
                }
                finally
                {
                    client.Close();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneModel"></param>
        /// <returns></returns>
        private SceneXmlDto.Scene ConvertSceneModelToDto(SceneXmlModel.Scene sceneModel)
        {
            SceneXmlDto.Scene sceneDto = new SceneXmlDto.Scene();
            sceneDto.Filename = Path.GetFullPath(sceneModel.Filename).ToLower();
            sceneDto.Version = sceneModel.Version;
            sceneDto.Timestamp = sceneModel.ExportTimestamp;
            sceneDto.Username = sceneModel.ExportUser;

            foreach (SceneXmlModel.TargetObjectDef objectDef in sceneModel.Objects)
            {
                SceneXmlDto.Object objectDto = ConvertObjectToDto(objectDef);
                sceneDto.Objects.Add(objectDto);
            }

            foreach (SceneXmlModel.Material.MaterialDef materialDef in sceneModel.Materials)
            {
                SceneXmlDto.Material materialDto = ConvertMaterialToDto(materialDef);
                sceneDto.Materials.Add(materialDto);
            }

            return sceneDto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectDef"></param>
        /// <returns></returns>
        private SceneXmlDto.Object ConvertObjectToDto(SceneXmlModel.TargetObjectDef objectDef)
        {
            SceneXmlDto.Object objectDto = new SceneXmlDto.Object();
            objectDto.Guid = objectDef.Guid;
            objectDto.Name = objectDef.Name;
            objectDto.Class = objectDef.Class;
            objectDto.SuperClass = objectDef.SuperClass;
            objectDto.AttributeClass = objectDef.AttributeClass;
            objectDto.MaterialGuid = objectDef.Material;
            objectDto.PolyCount = (uint)objectDef.PolyCount;
            objectDto.ObjectTranslation = (objectDef.ObjectTransform != null ? objectDef.ObjectTransform.Translation : new Vector3f());
            objectDto.ObjectRotation = objectDef.ObjectRotation ?? new Quaternionf();
            objectDto.ObjectScale = objectDef.ObjectScale ?? new Vector3f();
            objectDto.NodeTranslation = (objectDef.NodeTransform != null ? objectDef.NodeTransform.Translation : new Vector3f());
            objectDto.NodeRotation = objectDef.NodeRotation ?? new Quaternionf();
            objectDto.NodeScale = objectDef.NodeScale ?? new Vector3f();
            objectDto.LocalBoundingBox = objectDef.LocalBoundingBox ?? new BoundingBox3f(new Vector3f(), new Vector3f());
            objectDto.WorldBoundingBox = objectDef.WorldBoundingBox ?? new BoundingBox3f(new Vector3f(), new Vector3f());

            foreach (SceneXmlModel.TargetObjectDef childDef in objectDef.Children)
            {
                SceneXmlDto.Object childDto = ConvertObjectToDto(childDef);
                objectDto.Children.Add(childDto);
            }

            foreach (KeyValuePair<String, Object> attributeDef in objectDef.Attributes)
            {
                SceneXmlDto.Attribute attributeDto = ConvertAttributeToDto(attributeDef.Key, attributeDef.Value);
                if (attributeDto != null)
                {
                    objectDto.Attributes.Add(attributeDto);
                }
            }

            if (objectDef.ParamBlocks != null && objectDef.ParamBlocks.Any())
            {
                foreach (KeyValuePair<String, Object> parameterDef in objectDef.ParamBlocks[0])
                {
                    SceneXmlDto.Parameter parameterDto = ConvertParameterToDto(parameterDef.Key, parameterDef.Value);
                    if (parameterDto != null)
                    {
                        objectDto.Parameters.Add(parameterDto);
                    }
                }
            }

            return objectDto;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns></returns>
        private SceneXmlDto.Attribute ConvertAttributeToDto(String attributeName, Object attributeValue)
        {
            SceneXmlDto.Attribute attributeDto = new SceneXmlDto.Attribute();
            attributeDto.Name = attributeName;

            if (attributeValue is String)
            {
                attributeDto.StringValue = (String)attributeValue;
            }
            else if (attributeValue is Int32)
            {
                attributeDto.IntValue = (int)attributeValue;
            }
            else if (attributeValue is Single)
            {
                attributeDto.FloatValue = (float)attributeValue;
            }
            else if (attributeValue is Boolean)
            {
                attributeDto.BoolValue = (bool)attributeValue;
            }
            else
            {
                _log.Warning("Unsupported attribute type '{0}' encountered.  Ignoring the attribute.", attributeValue.GetType());
                return null;
            }

            return attributeDto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns></returns>
        private SceneXmlDto.Parameter ConvertParameterToDto(String parameterName, Object parameterValue)
        {
            SceneXmlDto.Parameter parameterDto = new SceneXmlDto.Parameter();
            parameterDto.Name = parameterName;

            if (parameterValue is String)
            {
                parameterDto.StringValue = (String)parameterValue;
            }
            else if (parameterValue is Int32)
            {
                parameterDto.IntValue = (int)parameterValue;
            }
            else if (parameterValue is Single)
            {
                parameterDto.FloatValue = (float)parameterValue;
            }
            else if (parameterValue is Boolean)
            {
                parameterDto.BoolValue = (bool)parameterValue;
            }
            else
            {
                _log.Warning("Unsupported parameter type '{0}' encountered.  Ignoring the parameter.", parameterValue.GetType());
                return null;
            }

            return parameterDto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="materialDef"></param>
        /// <returns></returns>
        private SceneXmlDto.Material ConvertMaterialToDto(SceneXmlModel.Material.MaterialDef materialDef)
        {
            SceneXmlDto.Material materialDto = new SceneXmlDto.Material();
            materialDto.Guid = materialDef.Guid;
            materialDto.Type = materialDef.MaterialType;
            materialDto.Preset = materialDef.Preset;

            if (materialDef.HasSubMaterials)
            {
                foreach (SceneXmlModel.Material.MaterialDef subMaterialDef in materialDef.SubMaterials)
                {
                    SceneXmlDto.Material subMaterialDto = ConvertMaterialToDto(subMaterialDef);
                    materialDto.SubMaterials.Add(subMaterialDto);
                }
            }

            if (materialDef.HasTextures)
            {
                foreach (SceneXmlModel.Material.TextureDef textureDef in materialDef.Textures)
                {
                    SceneXmlDto.Texture textureDto = ConvertTextureToDto(textureDef);
                    materialDto.Textures.Add(textureDto);
                }

                SetAlphaTexturePaths(materialDto.Textures);
            }

            return materialDto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="textureDef"></param>
        /// <returns></returns>
        private SceneXmlDto.Texture ConvertTextureToDto(SceneXmlModel.Material.TextureDef textureDef)
        {
            SceneXmlDto.Texture textureDto = new SceneXmlDto.Texture();
            if (!String.IsNullOrEmpty(textureDef.FilePath))
            {
                textureDto.Filename = Path.GetFullPath(textureDef.FilePath).ToLower();
            }
            else
            {
                textureDto.Filename = "";
            }
            textureDto.Type = textureDef.Type;
            return textureDto;
        }

        /// <summary>
        /// Sets the alpha filenames for the passed in textures based on the next/previous texture types.
        /// </summary>
        /// <param name="textureDtos"></param>
        private void SetAlphaTexturePaths(IList<SceneXmlDto.Texture> textureDtos)
        {
            for (int i = 0; i < textureDtos.Count; i++)
            {
                SceneXmlDto.Texture currentTexture = textureDtos[i];
                SceneXmlDto.Texture nextTexture = ((i + 1) < textureDtos.Count ? textureDtos[i + 1] : null);

                if (nextTexture != null)
                {
                    TextureTypes currentType = currentTexture.Type;
                    TextureTypes nextType = nextTexture.Type;

                    if (currentType == TextureTypes.DiffuseMap && nextType == TextureTypes.DiffuseAlpha ||
                        currentType == TextureTypes.SpecularMap && nextType == TextureTypes.SpecularAlpha ||
                        currentType == TextureTypes.BumpMap && nextType == TextureTypes.BumpAlpha)
                    {
                        currentTexture.AlphaFilename = nextTexture.Filename;
                        ++i;
                    }
                    else if (currentType == TextureTypes.DiffuseAlpha && nextType == TextureTypes.DiffuseMap ||
                             currentType == TextureTypes.SpecularAlpha && nextType == TextureTypes.SpecularMap ||
                             currentType == TextureTypes.BumpAlpha && nextType == TextureTypes.BumpMap)
                    {
                        nextTexture.AlphaFilename = nextTexture.Filename;
                        ++i;
                    }
                }
            }
        }
        #endregion
    }
}
