﻿using System;
using System.Diagnostics;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Services;
using RSG.ROS;
using RSG.Statistics.Client.Common;
using RSG.Statistics.Common.Config;

namespace RSG.Statistics.PopulateResourcingStats
{
    class Program
    {
        #region Constants
        // Return codes
        private static readonly int EXIT_SUCCESS = 0;
        private static readonly int EXIT_WARNING = 1;
        private static readonly int EXIT_ERROR = 2;
        private static readonly int EXIT_CRITIC = 3;

        // Main options
        private static readonly String OPT_SERVER = "server";
        private static readonly String OPTION_NO_SYNC = "nosync";       // DEPRECATED Flag to skip the syncing of assets
        private static readonly String OPTION_WILDCARD = "wildcard";    // Process data from a file

        // Dev options.
        private static readonly String OPTION_DEV = "dev";              // Flag indicating that this is running in dev mode
        #endregion // Constants

        #region Member Data
        private static IUniversalLog m_log;
        private static IStatsServer s_server;
        #endregion // Member Data

        #region Program Entry Point
        /// <summary>
        /// The main entry point for the application.
        /// Attribute [STAThread] for invoking the universal log from the right thread
        /// </summary>
        [STAThread]
        static int Main(string[] args)
        {
            int exit_code = EXIT_SUCCESS;

            // Create the log
            LogFactory.Initialize();
            m_log = LogFactory.CreateUniversalLog("Statistics_PopulateResourceStats");
            LogFactory.CreateApplicationConsoleLogTarget();
            UniversalLogFile logfile = LogFactory.CreateUniversalLogFile(m_log) as UniversalLogFile;

            // Define our executable options.  Stick with the long options
            // (e.g. --opt) rather than the single character versions.
            LongOption[] lopts = new LongOption[] {
                new LongOption(OPT_SERVER, LongOption.ArgType.Required, "Server to connect to."),
                new LongOption(OPTION_NO_SYNC, LongOption.ArgType.None, "Skip the asset sync step."),
                new LongOption(OPTION_DEV, LongOption.ArgType.None, "Dev mode."),
                new LongOption(OPTION_WILDCARD, LongOption.ArgType.Required, "Wildcard for searching for files."),
            };
            CommandOptions options = new CommandOptions(args, lopts);

            // Parse and validate the input parameters.
            bool dev = options.ContainsOption(OPTION_DEV);
            String wildcard = (options.ContainsOption(OPTION_WILDCARD) ? options[OPTION_WILDCARD] as String : null);

            // Initialise the server to communicate with.
            IStatisticsConfig config = new StatisticsConfig(options.Branch);
            if (!options.ContainsOption(OPT_SERVER))
            {
                s_server = config.DefaultServer;
            }
            else
            {
                String serverName = (String)options[OPT_SERVER];
                Debug.Assert(config.Servers.ContainsKey(serverName), "Requested an unknown server");
                s_server = config.Servers[serverName];
            }

            // Preamble...
            if (dev)
            {
                Console.WriteLine("Press <enter> to start client.");
                Console.ReadLine();
                Console.WriteLine();
                Console.WriteLine("Client is now running...");
            }

            try
            {
                // Now process the wildcard of resource files               
                if (wildcard != null)
                {
                    ResourceStatSyncService resourceSyncService = new ResourceStatSyncService();
                    resourceSyncService.SyncStatsWildcard(wildcard, s_server);
                }
            }
            catch (System.Exception ex)
            {
#if false
                Log.Log__Exception(ex, "Unexpected exception occurred while populating resourcing stats.");
                exit_code = EXIT_CRITIC;
#else
                Log.Log__Warning("Unexpected exception occurred while populating resourcing stats. This is a warning in order that such 'errors' don't spam artists. : {0}", ex);
#endif                
            }

            if (dev)
            {
                Console.WriteLine("Press <enter> to stop client.");
                Console.ReadLine();
            }

            LogFactory.ApplicationShutdown();

            Environment.ExitCode = exit_code;
            return (exit_code);
        }
        #endregion // Program Entry Point
    }
}
