﻿Command Line Argument
=====================

Running in dev mode (pauses the client until the server is started)
--dev

Parsing a local wildcard
--wildcard X:\gta5\tools\logs\resource_statistics*.xml 

Skip the data sync step
--nosync