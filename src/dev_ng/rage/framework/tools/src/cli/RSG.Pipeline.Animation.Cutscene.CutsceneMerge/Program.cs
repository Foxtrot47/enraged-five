﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.Animation.Cutscene.CutsceneMerge
{
    class Program
    {
        #region Constants
        private static readonly String OPTION_INPUTDIR = "inputdir";
        private static readonly String OPTION_OUTPUTDIR = "outputdir";
        private static readonly String OPTION_CUTFMERGE = "cutfmerge";
        private static readonly String OPTION_TASK_NAME = "taskname";
        private static readonly String LOG_CTX = "Cutscene Merge";
        #endregion // Constants

        [STAThread]
        static int Main(string[] args)
        {
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.CreateUniversalLog("CutsceneMerge");
            try
            {
                // Define our executable options.  Stick with the long options
                // (e.g. --opt) rather than the single character versions.
                Getopt options = new Getopt(args, new LongOption[] {
                    new LongOption(OPTION_INPUTDIR, LongOption.ArgType.Required,
                        "Input directory."),
                    new LongOption(OPTION_OUTPUTDIR, LongOption.ArgType.Required,
                        "Output directory."),
                    new LongOption(OPTION_CUTFMERGE, LongOption.ArgType.Required,
                        "Cutscene merge tool."),
                    new LongOption(OPTION_TASK_NAME, LongOption.ArgType.Required,
                        "")
                });
                // Turn args into vars
                String inputDir = options[OPTION_INPUTDIR] as String;
                String outputDir = options[OPTION_OUTPUTDIR] as String;
                String cutfMergeExe = options[OPTION_CUTFMERGE] as String;
                String taskName = options[OPTION_TASK_NAME] == null ? String.Empty : options[OPTION_TASK_NAME] as String;

                if (String.Empty != taskName)
                {
					Console.WriteLine("TASK: {0}", taskName);
                    log.MessageCtx(LOG_CTX, "TASK: {0}", taskName);
                }

                //if (!Directory.Exists(inputDir))
                //{
                //    log.ErrorCtx(LOG_CTX, "Directory '{0}' does not exist", inputDir);
                //    return RSG.Pipeline.Core.Constants.Exit_Failure;
                //}

                log.MessageCtx(LOG_CTX, "Creating directory '{0}'", outputDir);
                //if (!RSG.Pipeline.Services.Animation.DirectoryServices.CreateDirectory(outputDir))
                //{
                //    log.ErrorCtx(LOG_CTX, "Failed to create directory: '{0}'.  Aborting.", outputDir);
                //    return (Constants.Exit_Failure);
                //}

                CutsceneMerge c = new CutsceneMerge(cutfMergeExe,log);

                string[] folderList = inputDir.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                if (c.Process(folderList, outputDir) != 0)
                {
                    if (Directory.Exists(outputDir))
                        Directory.Delete(outputDir, true);
                    return RSG.Pipeline.Core.Constants.Exit_Failure;
                }

                return RSG.Pipeline.Core.Constants.Exit_Success;
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Unhandled exception during cutscene merge process");
                return RSG.Pipeline.Core.Constants.Exit_Failure;
            }
            finally
            {
                LogFactory.ApplicationShutdown();
            }
        }
    }
}
