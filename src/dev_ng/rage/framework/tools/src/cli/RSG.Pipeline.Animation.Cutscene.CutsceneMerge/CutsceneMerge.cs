﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using System.Threading;
using System.Xml;
using RSG.Base.Logging.Universal;

namespace RSG.Pipeline.Animation.Cutscene.CutsceneMerge
{
    #region Additional Classes

    class AnimConcatEntry
    {
        public string CutFile
        {
            get { return strCutFile; }
            set { strCutFile = value; }
        }

        public ArrayList AnimFiles
        {
            get { return lstAnimFiles; }
            set { lstAnimFiles = value; }
        }

        public void AddAnim(string strAnimPath)
        {
            lstAnimFiles.Add(strAnimPath);
        }

        private string strCutFile = String.Empty;
        private ArrayList lstAnimFiles = new ArrayList();
    }

    class ClipEntry
    {
        public string ClipName
        {
            get { return strClipName; }
            set { strClipName = value; }
        }

        public ArrayList ClipFiles
        {
            get { return lstClipFiles; }
            set { lstClipFiles = value; }
        }

        public void AddClip(string strClipPath)
        {
            lstClipFiles.Add(strClipPath);
        }

        private string strClipName = String.Empty;
        private ArrayList lstClipFiles = new ArrayList();
    }

    class AnimRun
    {
        public ArrayList Operations
        {
            get { return lstOperations; }
            set { lstOperations = value; }
        }

        public string AnimName
        {
            get { return strAnimName; }
            set { strAnimName = value; }
        }

        private string strAnimName = String.Empty;
        private ArrayList lstOperations = new ArrayList();
    }

    #endregion

    class CutsceneMerge
    {
        private static readonly String LOG_CTX = "Cutscene Merge";

        private string strCutXMLMergePath = String.Empty;
        private string OutputFolder = null;
        private IUniversalLog log = null;

        public CutsceneMerge(String mergeExe, IUniversalLog l)
        {
            strCutXMLMergePath = mergeExe;
            log = l;
        }

        // So it begins!
        public int Process(string[] folderList, string strOutputFolder)
        {
            //if (!ValidateAmountOfDirs(folderList)) return 1;

            // Copy animations from each dir to the main - we shouldn't have any clashes
            foreach (string folder in folderList)
            {
                CopyFiles(folder, strOutputFolder, "", "*.anim");
                CopyFiles(folder, strOutputFolder, "", "*.clip");
                CopyFiles(folder, strOutputFolder, "", "*.lightxml");
            }

            // Validate Shots
            if (!ValidateShots(folderList)) return 1;

            string[] shots = ParseCutPartFile(Path.Combine(folderList[0], "data.cutpart"));

            // Merge the cut files
            if (!MergeCutFiles(folderList, strOutputFolder, "", "data.cutxml")) return -1;
            foreach (string shot in shots)
            {
                if (!MergeCutFiles(folderList, strOutputFolder, "", String.Format("{0}.cutxml", shot))) return -1;
                if (!MergeCutFiles(folderList, strOutputFolder, shot, "data_stream.cutxml")) return -1;
            }

            return 0;
        }

        public string[] ParseCutPartFile(string filename)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(filename);

            XmlNodeList itemNodes = xmlDoc.SelectNodes("/RexRageCutscenePartFile/parts/Item/name");

            List<string> alItems = new List<string>();
            foreach (XmlNode n in itemNodes) alItems.Add(Path.GetFileNameWithoutExtension(n.InnerText).ToLower());

            return alItems.ToArray();
        }

        private bool MergeCutFiles(string[] folderList, string outputDir,  string sub, string file)
        {
            string strTempCutFilename = Path.GetTempFileName();
            TextWriter twCutFileList = new StreamWriter(strTempCutFilename);

            foreach (string folder in folderList)
            {
                twCutFileList.WriteLine(Path.Combine(folder, sub, file));
            }

            twCutFileList.Close();

            String strOutput = String.Empty;
            return RunProcess(strCutXMLMergePath, String.Format("-cutlistFile {0} -outputFile {1} -nopopups", strTempCutFilename, Path.Combine(outputDir, sub, file)), out strOutput, true);
        }

        private bool ValidateShots(string[] folderList)
        {
            string[] shots = ParseCutPartFile(Path.Combine(folderList[0], "data.cutpart"));
            Array.Sort(shots);

            for (int i = 1; i < folderList.Length; ++i)
            {
                string[] _shots = ParseCutPartFile(Path.Combine(folderList[i], "data.cutpart"));
                Array.Sort(_shots);

                if (!Enumerable.SequenceEqual(shots, _shots))
                {
                    log.ErrorCtx(LOG_CTX, "Merge scenes do not have identical amount of shots.");
                    return false;
                }
            }

            return true;
        }

        private bool ValidateAmountOfDirs(string[] folderList)
        {
            int subDirCount = -1;
            foreach (string folder in folderList)
            {
                int num = Directory.GetDirectories(folder, "*.*", SearchOption.AllDirectories).Length;

                if (subDirCount != -1 && subDirCount != num)
                {
                    log.ErrorCtx(LOG_CTX, "Merged scenes differ in folder structure.");
                    return false;
                }
                subDirCount = num;
            }

            return true;
        }

        public void CopyFiles(string parent, string output, string sub, string pattern)
        {
            string outDir = output;
            if (sub != "")
            {
                outDir = Path.Combine(output, sub);
                if (!Directory.Exists(outDir))
                    Directory.CreateDirectory(outDir);
            }

            string[] files = Directory.GetFiles(parent, pattern);
            foreach (string file in files)
            {
                File.Copy(file, Path.Combine(outDir, Path.GetFileName(file)), true);
            }

            string[] subDirs = Directory.GetDirectories(parent);
            foreach (string dir in subDirs)
            {
                CopyFiles(dir, outDir, Path.GetFileName(dir), pattern);
            }
        }

        #region Helper Functions

        // Run the animconcat process to join our anims or padding
        private bool RunProcess(string strExe, string strArguments, out string strOutput, bool bWaitForExit)
        {
            try
            {
                System.Diagnostics.Process pExe = new System.Diagnostics.Process();
                pExe.StartInfo.FileName = strExe;
                pExe.StartInfo.Arguments = strArguments;
                pExe.StartInfo.CreateNoWindow = true;
                pExe.StartInfo.UseShellExecute = false;
                pExe.StartInfo.RedirectStandardOutput = true;
                pExe.Start();

                log.MessageCtx(LOG_CTX, "{0} {1}", strExe, strArguments);

                strOutput = String.Empty;
                if (bWaitForExit)
                {
                    strOutput = pExe.StandardOutput.ReadToEnd();
                    pExe.WaitForExit();

                    if (pExe.ExitCode != 0)
                    {
                        log.ErrorCtx(LOG_CTX, "{0} failed: {1}", strExe, strOutput);
                        return false;
                    }
                }

                return true;
            }
            catch (System.ComponentModel.Win32Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Unhandled exception during animconcat process");
            }

            strOutput = String.Empty;
            return false;
        }

        #endregion
    }
}
