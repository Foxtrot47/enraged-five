﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using RSG.Base.OS;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace RSG.Pipeline.Animation.Postprocess
{
    sealed class Program
    {
         #region Constants
        private static readonly int EXIT_SUCCESS = 0;
        private static readonly int EXIT_ERROR = 1;

        private static readonly String OPTION_INPUTDIR = "inputdir";
        private static readonly String OPTION_OUTPUTFILE = "outputfile";
        private static readonly String OPTION_TASK_NAME = "taskname";
        private static readonly String LOG_CTX = "Post Process";

        #endregion // Constants

        [STAThread]
        static int Main(string[] args)
        {
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.CreateUniversalLog("Postprocess");
            int exit_code = EXIT_SUCCESS;
            try
            {
                // Define our executable options.  Stick with the long options
                // (e.g. --opt) rather than the single character versions.
                Getopt options = new Getopt(args, new LongOption[] {
                    new LongOption(OPTION_INPUTDIR, LongOption.ArgType.Required,
                        "Input directory."),
                    new LongOption(OPTION_OUTPUTFILE, LongOption.ArgType.Required,
                        "Output filename."),
                    new LongOption(OPTION_TASK_NAME, LongOption.ArgType.Required,
                        "Asset Pipeline 3 Task Name."),
                });
                // Turn args into vars
                String inputDirectory = options[OPTION_INPUTDIR] as String;
                String outputFile = options[OPTION_OUTPUTFILE] as String;

                if (File.Exists(outputFile))
                    File.Delete(outputFile);

                String taskName = options[OPTION_TASK_NAME] == null ? String.Empty : options[OPTION_TASK_NAME] as String;

                if (String.Empty != taskName)
                {
                    Console.WriteLine("TASK: {0}", taskName);
                    log.MessageCtx(LOG_CTX, "TASK: {0}", taskName);
                }

                log.MessageCtx(LOG_CTX, "Packing {0} to {1}", inputDirectory, outputFile);
                String[] clipFiles = Directory.GetFiles(inputDirectory, "*.clip");
                if (clipFiles.Count() == 0)
                {
                    log.ErrorCtx(LOG_CTX, "No clip files were found attempting to package {0}.", outputFile);
                    return (EXIT_ERROR);
                }
                String[] animFiles = Directory.GetFiles(inputDirectory, "*.anim");
                if (clipFiles.Count() == 0)
                {
                    log.ErrorCtx(LOG_CTX, "No anim files were found attempting to package {0}.", outputFile);
                    return (EXIT_ERROR);
                }
                String[] filesForZip = new String[clipFiles.Length + animFiles.Length];
                Array.Copy(clipFiles, 0, filesForZip, 0, clipFiles.Length);
                Array.Copy(animFiles, 0, filesForZip, clipFiles.Length, animFiles.Length);
                try
                {
                    using (var clipsArchive = new Ionic.Zip.ZipFile(outputFile))
                    {
                        clipsArchive.AddFiles(filesForZip, "");
                        clipsArchive.Save();
                    }
                }
                catch (Exception ex)
                {
                    String filelist = "";
                    foreach (String file in filesForZip)
                    {
                        filelist = String.Format("{0}{1},\n", filelist, file);
                    }
                    log.ToolExceptionCtx(LOG_CTX, ex, "Exception in Animation PostProcess");
                    log.ToolErrorCtx(LOG_CTX, "Could not zip the following files:\n {0}", filelist);
                    exit_code = EXIT_ERROR;
                }
            }
            finally
            {
                LogFactory.ApplicationShutdown();
            }

            return (exit_code);
        }
    }
}
