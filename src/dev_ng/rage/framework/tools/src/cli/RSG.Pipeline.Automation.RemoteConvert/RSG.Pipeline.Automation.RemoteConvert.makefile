#
# RSG.Pipeline.Automation.RemoteConvert.makefile
#
 
Project RSG.Pipeline.Automation.RemoteConvert

BuildTemplate $(toolsroot)/etc/projgen/RSG.Pipeline.Executable.build
 
ConfigurationType exe
 
FrameworkVersion 4.5
 
OutputPath $(toolsroot)\ironlib\lib\
 
Files {
	RemoteConsoleApp.cs
	Directory Properties {
			AssemblyInfo.cs
	}
	Directory Resources {
		Directory Images
		{
			rockstar.png
		}
		StringTable.resx(LastGenOutput=StringTable.Designer.cs)(Generator=PublicResXFileCodeGenerator)
		StringTable.Designer.cs(DependentUpon=StringTable.resx)(DesignTime=True)(AutoGen=True)
	}
	app.config
	CommentDialogBox.xaml(Generator=MSBuild:Compile)(SubType=Designer)
	CommentDialogBox.xaml.cs(DependentUpon=CommentDialogBox.Xaml)
	rockstar.ico	
}
 
ProjectReferences {
	..\..\..\..\..\..\..\..\..\3rdParty\dev\cli\P4.Net\src\P4API\P4API.csproj
	..\..\..\..\..\base\tools\libs\RSG.Base.Configuration\RSG.Base.Configuration.csproj
	..\..\..\..\..\base\tools\libs\RSG.Base\RSG.Base.csproj
	..\..\..\..\..\base\tools\libs\RSG.Configuration\RSG.Configuration.csproj
	..\..\..\..\..\base\tools\libs\RSG.Interop.Microsoft\RSG.Interop.Microsoft.csproj
	..\..\ui\Libs\RSG.Editor\RSG.Editor.csproj
	..\..\ui\Libs\RSG.Editor.Controls\RSG.Editor.Controls.csproj
	..\..\Libs\RSG.Interop.Rockstar\RSG.Interop.Rockstar.csproj
	..\..\Libs\RSG.Pipeline.Automation.Common\RSG.Pipeline.Automation.Common.csproj
	..\..\Libs\RSG.Pipeline.Automation.Consumers\RSG.Pipeline.Automation.Consumers.csproj
	..\..\Libs\RSG.Pipeline.Core\RSG.Pipeline.Core.csproj
	..\..\Libs\RSG.Pipeline.Services\RSG.Pipeline.Services.csproj
}
References {
	PresentationCore
	PresentationFramework
	System
	System.ComponentModel.Composition
	System.Core
	System.Runtime.Serialization
	System.ServiceModel
	System.ServiceModel.Activation
	System.ServiceModel.Web
	System.Xaml
	System.Xml.Linq
	System.Data.DataSetExtensions
	Microsoft.CSharp
	System.Data
	System.Xml
	WindowsBase
}
