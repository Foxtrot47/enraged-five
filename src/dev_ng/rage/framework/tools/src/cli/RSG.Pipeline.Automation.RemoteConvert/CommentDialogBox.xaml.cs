﻿//---------------------------------------------------------------------------------------------
// <copyright file="CommentDialogBox.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Automation.RemoteConvert
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using RSG.Editor.Controls;

    /// <summary>
    /// Interaction logic for CommentDialogBox.xaml
    /// </summary>
    internal partial class CommentDialogBox : RsWindow
    {
        #region Properties
        /// <summary>
        /// Gets the command that is fired when the user selects the Submit button.
        /// </summary>
        public static RoutedCommand SubmitCommand
        {
            get { return _submitCommand; }
        }

        /// <summary>
        /// Gets the user specified comment.
        /// </summary>
        public String UserComment
        {
            get;
            private set;
        }

        /// <summary>
        /// Collection of files.
        /// </summary>
        public ObservableCollection<Tuple<String, String>> Files
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// The private field used for the <see cref="SubmitCommand"/> property.
        /// </summary>
        private static RoutedCommand _submitCommand;  
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CommentDialogBox(IEnumerable<Tuple<String, String>> files)
        {
            this.InitializeComponent();
            this.DataContext = this;
            this.Files = new ObservableCollection<Tuple<String, String>>(files);
            this.CommandBindings.Add(
                new System.Windows.Input.CommandBinding(SubmitCommand, this.Submit, this.CanSubmit));
        }

        static CommentDialogBox()
        {
            _submitCommand = new RoutedCommand("SubmitCommand", typeof(CommentDialogBox));
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// Called when the user executes the Submit command.
        /// </summary>
        /// <param name="s">
        /// The object that fired the AttemptLogin command.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.ExecutedRoutedEventArgs containing the event data.
        /// </param>
        private void Submit(Object s, ExecutedRoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        /// <summary>
        /// Determines whether the Submit command can be executed by the user.
        /// This simply validates the user's comment.
        /// </summary>
        /// <param name="s">
        /// The object that fired the Submit command.
        /// </param>
        /// <param name="e">
        /// The System.Windows.Input.CanExecuteRoutedEventArgs containing the event data.
        /// </param>
        private void CanSubmit(Object s, CanExecuteRoutedEventArgs e)
        {
            if (String.IsNullOrWhiteSpace(this.CommentBox.Text))
            {
                e.CanExecute = false;
                this.ErrorMessage.Text = "Empty comment field.  Add a comment to continue.";
                return;
            }
            else
            {
                // Check that we have a couple of words at least.
                String[] words = this.CommentBox.Text.Split(new char[] { ' ', '\t' }, 
                    StringSplitOptions.RemoveEmptyEntries);
                if (words.Length <= 1 || this.CommentBox.Text.Length < 10)
                {
                    this.ErrorMessage.Text = "Comment must be meaningful.";
                    return;
                }
            }

            this.ErrorMessage.Text = String.Empty;
            e.CanExecute = true;
        }

        /// <summary>
        /// Called whenever the user presses the cancel button. This just closes the dialog
        /// making sure false is returned.
        /// </summary>
        /// <param name="sender">
        /// The object this handler is attached to.
        /// </param>
        /// <param name="e">
        /// The System.Windows.RoutedEventArgs containing the event data.
        /// </param>
        private void CancelButtonClicked(Object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.RemoteConvert namespace
