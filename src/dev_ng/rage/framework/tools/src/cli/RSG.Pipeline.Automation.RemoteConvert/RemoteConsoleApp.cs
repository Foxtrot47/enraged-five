﻿//---------------------------------------------------------------------------------------------
// <copyright file="RemoteConvertApp.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.Pipeline.Automation.RemoteConvert
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using RSG.Base.Configuration;
    using RSG.Base.Configuration.Automation;
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;
    using RSG.Base.OS;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Interop.Rockstar;
    using RSG.Pipeline.Core;
    using RSG.Pipeline.Automation.Common.Jobs;
    using RSG.Pipeline.Automation.Common.Triggers;

    /// <summary>
    /// Remote Convert Application; enqueues Asset Builds on the remote Asset Builder
    /// farm based on project and branch information.
    /// </summary>
    class RemoteConsoleApp : RsConsoleApplication
    {
        #region Constants
        // Options.
        private const String OPTION_RECURSIVE = "recursive";
        private const String OPTION_RECURSIVE_WILDCARD = "wildcard"; 
        private const String RECURSIVE_WILDCARD_DEFAULT = "*.*";
        private const String OPTION_REBUILD = "rebuild";
        private const String OPTION_COMMENT = "comment";
        private static LongOption[] OPTIONS =
        {
            new LongOption(OPTION_RECURSIVE, LongOption.ArgType.None,
                "Recurse directory search (for directories only)."),
            new LongOption(OPTION_RECURSIVE_WILDCARD, LongOption.ArgType.Required,
                "Recursive directory search file wildcard (required: recursive option)."),
            new LongOption(OPTION_REBUILD, LongOption.ArgType.None, 
                "Rebuild data; ignoring timestamp information."),
            new LongOption(OPTION_COMMENT, LongOption.ArgType.Required,
                "User comment for queuing the data for build.")
        };
        private const String OPTION_TRAILING = "files";
        private const String OPTION_TRAILING_DESC = "Files to remotely queue for conversion.";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Gets a semi-colon separate list of the application authors email addresses. These
        /// are the addresses which are used by the unhandled exception dialog to determine
        /// where the mail should be sent by default.
        /// e.g.
        /// *tools@rockstarnorth.com
        /// michael.taschler@rockstarnorth.com;dave.evans@rockstarnorth.com
        /// </summary>
        public override string AuthorEmailAddress
        {
            get { return "*tools@rockstarnorth.com"; }
        }
        #endregion // Properties
        
        #region Entry-Point
        [STAThread]
        static int Main(String[] args)
        {
            RemoteConsoleApp app = new RemoteConsoleApp();
            return (app.Run());
        }
        #endregion // Entry-Point

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        public RemoteConsoleApp()
            : base()
        {
            foreach (LongOption o in RSG.Configuration.CommandOptions.DefaultArguments)
                this.RegisterCommandlineArg(o.Longopt, o.ArgumentSpec, o.Description);
            foreach (LongOption o in OPTIONS)
                this.RegisterCommandlineArg(o);
        }
        #endregion // Constructor(s)

        #region RsConsoleApplication Overridden Methods
        /// <summary>
        /// Program Entry-Point.
        /// </summary>
        /// <returns></returns>
        protected override int ConsoleMain()
        {
            // Construct our CommandOptions object; this isn't yet integrated into the Editor
            // Framework RsConsoleApplication stuff.
            String[] args = Environment.GetCommandLineArgs().Skip(1).ToArray(); // HACK: skip app name.
            CommandOptions options = new CommandOptions(args, OPTIONS);
            
            int exit_code = RSG.Pipeline.Core.Constants.Exit_Success;
            String consoleLogFilename = String.Empty;
            try
            {                
                if (options.ShowHelp)
                {
                    ShowUsage();
                }
                if (0 == options.TrailingArguments.Count())
                {
                    RsMessageBox.Show("No files or directories specified.  Aborting.",
                        Resources.StringTable.TitleContent, MessageBoxButton.OK, MessageBoxImage.Error);
                    this.Log.Error("No files or directories specified.  Aborting.");
                    ShowUsage();
                }

                if (!this.Log.HasErrors)
                {
                    this.Log.Message("Determining Asset Builder to queue remote jobs on.");
                    IAutomationServicesConfig config = ConfigFactory.CreateAutomationServicesConfig(options.Project);
                    IEnumerable<IAutomationServiceConfig> assetBuilderServices =
                        config.Services.Where(s => s.ServiceType == "AssetBuilder");
                    this.Log.Message("{0} project has {1} Asset Builder services available.",
                        options.Project.FriendlyName, assetBuilderServices.Count());

                    IAutomationServiceConfig assetBuilderService = config.Services.FirstOrDefault(s =>
                        s.Console.BranchName.Equals(options.Branch.Name));
                    if (null == assetBuilderService)
                    {
                        this.Log.Error("Failed to find Asset Builder Service for branch '{0}'.", options.Branch.Name);
                    }
                    else
                    {
                        bool rebuild = options.ContainsOption(OPTION_REBUILD);
                        bool recursive = options.ContainsOption(OPTION_RECURSIVE);
                        String recursive_wildcard = options.ContainsOption(OPTION_RECURSIVE_WILDCARD) ?
                            (String)options[OPTION_RECURSIVE_WILDCARD] : RECURSIVE_WILDCARD_DEFAULT;
                        IEnumerable<String> filenames = EvaluateTrailingArguments(
                            options.TrailingArguments, recursive, recursive_wildcard);

                        // Prompt for comment; if its not supplied.
                        String comment = String.Empty;
                        if (!options.ContainsOption(OPTION_COMMENT) && !options.NoPopups)
                        {
                            List<Tuple<String, String>> files = new List<Tuple<String, String>>();
                            foreach (String filename in filenames)
                            {
                                if (!File.Exists(filename))
                                    continue;
                                FileInfo fi = new FileInfo(filename);
                                String sz = new RSG.Base.FileSize(fi.Length).ToString();
                                files.Add(new Tuple<String, String>(filename, sz));
                            }

                            CommentDialogBox dlgBox = new CommentDialogBox(files);
                            bool? result = dlgBox.ShowDialog();
                            if (!result.HasValue || !result.Value)
                            {
                                this.Log.Warning("User canceled when asked for comment.");
                                RsMessageBox.Show("Assets have not been queued.", Resources.StringTable.TitleContent,
                                    MessageBoxButton.OK, MessageBoxImage.Warning);
                                return (0);
                            }
                            comment = dlgBox.UserComment;
                        }
                        else if (options.ContainsOption(OPTION_COMMENT))
                        {
                            comment = (String)options[OPTION_COMMENT];
                        }
                        else
                        {
                            this.Log.Error("No comment supplied.  See usage.");
                            ShowUsage();
                        }

                        if (!EnqueueBuild(options, assetBuilderService, rebuild, filenames, comment, out consoleLogFilename))
                        {
                            this.Log.Error("Automation Console script execution failed.  See other errors.");
                            if (!options.NoPopups)
                                RsMessageBox.Show("Assets failed to enqueue.  See error log.", Resources.StringTable.TitleContent,
                                    MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                        else
                        {
                            RsMessageBox msgBox = new RsMessageBox();
                            msgBox.Caption = Resources.StringTable.TitleContent;
                            msgBox.Text = String.Format("Assets have been queued on the Asset Builder for {0} [{1}].\n\nCheck the Automation Monitor for progress.",
                                options.Project.FriendlyName, options.Branch.Name);
                            msgBox.AddButton("Start Automation Monitor", 1000, false, false);
                            msgBox.AddButton("Close", (long)MessageBoxButton.OK, true, false);
                            if (1000 == msgBox.ShowMessageBox())
                            {
                                AutomationMonitorApplication monitorApp = new AutomationMonitorApplication(options.Project);
                                StartProcess(monitorApp.InstallExecutable, String.Empty);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolException(ex, "Unhandled exception.");
            }
            finally
            {
                // Show log if showing popups.
                if ((this.Log.HasErrors) && (!options.NoPopups))
                {
                    UniversalLogViewerApplication ulogApp = new UniversalLogViewerApplication(options.Project);
                    String arg = String.Format("{0} {1}", LogFactory.ApplicationLogFilename,
                        consoleLogFilename);
                    StartProcess(ulogApp.InstallExecutable, arg);
                }

                exit_code = this.Log.HasErrors ? Constants.Exit_Failure : Constants.Exit_Success;
                LogFactory.ApplicationShutdown();
            }
            return (exit_code);
        }
        #endregion // RsConsoleApplication Overridden Methods

        #region Private Methods
        /// <summary>
        /// Print usage information.
        /// </summary>
        private void ShowUsage()
        {
            base.ShowUsage(OPTION_TRAILING, OPTION_TRAILING_DESC);
        }

        /// <summary>
        /// Evaluate trailing arguments depending on recursive options.
        /// </summary>
        /// <param name="trailingArgs">Trailing arguments sent to executable.</param>
        /// <param name="recursive">Expand directories recursively.</param>
        /// <param name="wildcard">Directory search wildcard (if recursive).</param>
        /// <returns></returns>
        private IEnumerable<String> EvaluateTrailingArguments(IEnumerable<String> trailingArgs,
            bool recursive, String wildcard)
        {
            // Expand local paths.
            IEnumerable<String> filenames = trailingArgs.Select(f => Path.GetFullPath(f));

            // Handle recursive call; for directories only.
            if (recursive)
            {
                List<String> expandedFilenames = new List<String>();
                foreach (String filename in filenames)
                {
                    if (Directory.Exists(filename))
                    {
                        // Directory; expand recursively and add directory search results
                        // to our set.
                        String[] files = Directory.GetFiles(filename, wildcard,
                            SearchOption.AllDirectories);
                        expandedFilenames.AddRange(files);
                    }
                    else
                    {
                        // Simply a filename; so add to our set.
                        expandedFilenames.Add(filename);
                    }
                }
                filenames = expandedFilenames;
            }

            return (filenames);
        }

        /// <summary>
        /// Enqueue a build using the Automation Console.
        /// </summary>
        /// <param name="options"></param>
        /// <param name="service"></param>
        /// <param name="rebuild"></param>
        /// <param name="filenames"></param>
        /// <param name="submitComment"></param>
        /// <param name="logFilename"></param>
        private bool EnqueueBuild(CommandOptions options, IAutomationServiceConfig service, 
            bool rebuild, IEnumerable<String> filenames, String submitComment, out String logFilename)
        {
            IEnvironment env = ConfigFactory.CreateEnvironment();
            env.Add("branch", options.Branch.Name);
            env.Add("host", service.ServerHost.Host);
            String consoleArgs = env.Subst(service.Console.Arguments);

            // Create temporary file for console commands.
            String tempFilename = Path.Combine(Path.GetTempPath(), "RemoteConvert.console");
            using (StreamWriter sw = new StreamWriter(tempFilename))
            {
                String filenameCommand = String.Join(" ", filenames.ToArray());
                if (rebuild)
                    sw.WriteLine("asset_rebuild {0} \"{1}\"", filenameCommand, submitComment);
                else
                    sw.WriteLine("asset_build {0} \"{1}\"", filenameCommand, submitComment);
                sw.WriteLine("exit");
            }

            // Run the automation console.
            AutomationConsoleApplication consoleApp = new AutomationConsoleApplication(options.Project);
            String arguments = String.Format("{0} -script {1}", consoleArgs, tempFilename);
            int pid = -1;
            int exit_code = StartProcessAndWait(consoleApp.InstallExecutable, arguments, out pid);

            String consoleExe = Path.GetFileNameWithoutExtension(consoleApp.InstallExecutable);
            logFilename = Path.Combine(LogFactory.ParentProcessLogDirectory, String.Format("{0}.{1}.ulog", consoleExe, pid));
            return (Constants.Exit_Success == exit_code);
        }

        /// <summary>
        /// Start process; do not wait.
        /// </summary>
        /// <param name="executable"></param>
        /// <param name="arguments"></param>
        private void StartProcess(String executable, String arguments)
        {
            System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo();
            si.FileName = executable;
            si.Arguments = arguments;
            si.CreateNoWindow = true;
            si.UseShellExecute = true;

            Process p = new Process();
            p.StartInfo = si;
            p.Start();
        }

        /// <summary>
        /// Start process and wait for it to exit; returning exit code.
        /// </summary>
        /// <param name="executable"></param>
        /// <param name="arguments"></param>
        /// <param name="wait"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        private int StartProcessAndWait(String executable, String arguments, out int pid)
        {
            System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo();
            si.FileName = executable;
            si.Arguments = arguments;
            si.CreateNoWindow = true;
            si.UseShellExecute = true;

            Process p = new Process();
            p.StartInfo = si;
            p.Start();
            p.WaitForExit();

            pid = p.Id;
            return (p.ExitCode);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.RemoteConvert namespace
