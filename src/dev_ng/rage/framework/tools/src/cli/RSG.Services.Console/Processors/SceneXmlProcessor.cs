﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using RSG.Base.Logging;
using SceneXmlModel = RSG.SceneXml;
using SceneXmlDto = RSG.Services.Assets.Contracts.DataContracts;
using RSG.Base.Math;
using RSG.SceneXml.Material;
using RSG.Services.Configuration;
using RSG.Services.Configuration.ServiceModel;
using RSG.Services.Assets.Consumers;
using RSG.Services.Assets.Contracts.DataContracts;
using System.Diagnostics;

namespace RSG.Services.Console.Processors
{
    /// <summary>
    /// Processor that handles scene-xml based requests to an asset server.
    /// </summary>
    public class SceneXmlProcessor
    {
        #region Fields
        /// <summary>
        /// Log object for the sync service.
        /// </summary>
        protected ILog _log;

        /// <summary>
        /// Asset server to communicate with.
        /// </summary>
        protected IServer _server;
        #endregion
        
        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="SceneXmlProccesor"/> class using
        /// the specified server configuration object and log.
        /// </summary>
        /// <param name="server"></param>
        /// <param name="log"></param>
        public SceneXmlProcessor(IServer server, ILog log)
        {
            _log = log;
            _server = server;
        }
        #endregion
        
        #region Syncing Scene Xml Data
        /// <summary>
        /// Loads up and syncs the scene xml data for the specified scene xml file.
        /// </summary>
        /// <param name="filename"></param>
        public void SyncSceneXmlData(string filename)
        {
            SceneXmlModel.Scene sceneModel = new SceneXmlModel.Scene(filename, SceneXml.LoadOptions.All, true);
            SceneXmlDto.Scene sceneDto = ConvertSceneModelToDto(sceneModel);

            using (Stream stream = File.OpenRead(filename))
            {
                AssetPopulationClient client = new AssetPopulationClient(_server, Uri.UriSchemeNetTcp);
                try
                {
                    client.PopulateSceneXmlFile(filename, sceneDto);
                }
                catch (System.Exception ex)
                {
                    _log.ToolException(ex, "Unhandled exception while syncing scene xml data.");
                }
                finally
                {
                    if (client != null)
                    {
                        client.CloseOrAbort();
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sceneModel"></param>
        /// <returns></returns>
        private SceneXmlDto.Scene ConvertSceneModelToDto(SceneXmlModel.Scene sceneModel)
        {
            SceneXmlDto.Scene sceneDto = new SceneXmlDto.Scene();
            sceneDto.Filename = Path.GetFullPath(sceneModel.Filename).ToLower();
            sceneDto.Version = sceneModel.Version;
            sceneDto.Timestamp = sceneModel.ExportTimestamp;
            sceneDto.Username = sceneModel.ExportUser;
            sceneDto.Objects = new List<SceneXmlDto.Object>();
            sceneDto.Materials = new List<SceneXmlDto.Material>();

            foreach (SceneXmlModel.TargetObjectDef objectDef in sceneModel.Objects)
            {
                SceneXmlDto.Object objectDto = ConvertObjectToDto(objectDef);
                sceneDto.Objects.Add(objectDto);
            }

            foreach (SceneXmlModel.Material.MaterialDef materialDef in sceneModel.Materials)
            {
                SceneXmlDto.Material materialDto = ConvertMaterialToDto(materialDef);
                sceneDto.Materials.Add(materialDto);
            }

            return sceneDto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectDef"></param>
        /// <returns></returns>
        private SceneXmlDto.Object ConvertObjectToDto(SceneXmlModel.TargetObjectDef objectDef)
        {
            SceneXmlDto.Object objectDto = new SceneXmlDto.Object();
            objectDto.Guid = objectDef.Guid;
            objectDto.Name = objectDef.Name;
            objectDto.Class = objectDef.Class;
            objectDto.SuperClass = objectDef.SuperClass;
            objectDto.AttributeClass = objectDef.AttributeClass;
            objectDto.MaterialGuid = objectDef.Material;
            objectDto.PolyCount = (uint)objectDef.PolyCount;
            objectDto.ObjectTranslation = (objectDef.ObjectTransform != null ? objectDef.ObjectTransform.Translation : new Vector3f());
            objectDto.ObjectRotation = objectDef.ObjectRotation ?? new Quaternionf();
            objectDto.ObjectScale = objectDef.ObjectScale ?? new Vector3f();
            objectDto.NodeTranslation = (objectDef.NodeTransform != null ? objectDef.NodeTransform.Translation : new Vector3f());
            objectDto.NodeRotation = objectDef.NodeRotation ?? new Quaternionf();
            objectDto.NodeScale = objectDef.NodeScale ?? new Vector3f();
            objectDto.LocalBoundingBox = objectDef.LocalBoundingBox ?? new BoundingBox3f(new Vector3f(), new Vector3f());
            objectDto.WorldBoundingBox = objectDef.WorldBoundingBox ?? new BoundingBox3f(new Vector3f(), new Vector3f());
            objectDto.Children = new List<SceneXmlDto.Object>();
            objectDto.Attributes = new List<SceneXmlDto.Attribute>();
            objectDto.Parameters = new List<SceneXmlDto.Parameter>();

            foreach (SceneXmlModel.TargetObjectDef childDef in objectDef.Children)
            {
                SceneXmlDto.Object childDto = ConvertObjectToDto(childDef);
                objectDto.Children.Add(childDto);
            }

            foreach (KeyValuePair<String, System.Object> attributeDef in objectDef.Attributes)
            {
                SceneXmlDto.Attribute attributeDto = ConvertAttributeToDto(attributeDef.Key, attributeDef.Value);
                if (attributeDto != null)
                {
                    objectDto.Attributes.Add(attributeDto);
                }
            }

            if (objectDef.ParamBlocks != null && objectDef.ParamBlocks.Any())
            {
                foreach (KeyValuePair<String, System.Object> parameterDef in objectDef.ParamBlocks[0])
                {
                    SceneXmlDto.Parameter parameterDto = ConvertParameterToDto(parameterDef.Key, parameterDef.Value);
                    if (parameterDto != null)
                    {
                        objectDto.Parameters.Add(parameterDto);
                    }
                }
            }

            return objectDto;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns></returns>
        private SceneXmlDto.Attribute ConvertAttributeToDto(String attributeName, System.Object attributeValue)
        {
            SceneXmlDto.Attribute attributeDto = new SceneXmlDto.Attribute();
            attributeDto.Name = attributeName;

            if (attributeValue is String)
            {
                attributeDto.StringValue = (String)attributeValue;
            }
            else if (attributeValue is Int32)
            {
                attributeDto.IntValue = (int)attributeValue;
            }
            else if (attributeValue is Single)
            {
                attributeDto.FloatValue = (float)attributeValue;
            }
            else if (attributeValue is Boolean)
            {
                attributeDto.BoolValue = (bool)attributeValue;
            }
            else
            {
                _log.Warning("Unsupported attribute type '{0}' encountered.  Ignoring the attribute.", attributeValue.GetType());
                return null;
            }

            return attributeDto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="attribute"></param>
        /// <returns></returns>
        private SceneXmlDto.Parameter ConvertParameterToDto(String parameterName, System.Object parameterValue)
        {
            SceneXmlDto.Parameter parameterDto = new SceneXmlDto.Parameter();
            parameterDto.Name = parameterName;

            if (parameterValue is String)
            {
                parameterDto.StringValue = (String)parameterValue;
            }
            else if (parameterValue is Int32)
            {
                parameterDto.IntValue = (int)parameterValue;
            }
            else if (parameterValue is Single)
            {
                parameterDto.FloatValue = (float)parameterValue;
            }
            else if (parameterValue is Boolean)
            {
                parameterDto.BoolValue = (bool)parameterValue;
            }
            else
            {
                _log.Warning("Unsupported parameter type '{0}' encountered.  Ignoring the parameter.", parameterValue.GetType());
                return null;
            }

            return parameterDto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="materialDef"></param>
        /// <returns></returns>
        private SceneXmlDto.Material ConvertMaterialToDto(SceneXmlModel.Material.MaterialDef materialDef)
        {
            SceneXmlDto.Material materialDto = new SceneXmlDto.Material();
            materialDto.Guid = materialDef.Guid;
            materialDto.Type = ConvertMaterialType(materialDef.MaterialType);
            materialDto.Preset = materialDef.Preset;
            materialDto.SubMaterials = new List<SceneXmlDto.Material>();
            materialDto.Textures = new List<SceneXmlDto.Texture>();

            if (materialDef.HasSubMaterials)
            {
                foreach (SceneXmlModel.Material.MaterialDef subMaterialDef in materialDef.SubMaterials)
                {
                    SceneXmlDto.Material subMaterialDto = ConvertMaterialToDto(subMaterialDef);
                    materialDto.SubMaterials.Add(subMaterialDto);
                }
            }

            if (materialDef.HasTextures)
            {
                foreach (SceneXmlModel.Material.TextureDef textureDef in materialDef.Textures)
                {
                    SceneXmlDto.Texture textureDto = ConvertTextureToDto(textureDef);
                    materialDto.Textures.Add(textureDto);
                }

                SetAlphaTexturePaths(materialDto.Textures);
            }

            return materialDto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="materialType"></param>
        /// <returns></returns>
        private SceneXmlDto.MaterialType ConvertMaterialType(SceneXmlModel.Material.MaterialTypes materialType)
        {
            switch (materialType)
            {
                case MaterialTypes.None:
                    return SceneXmlDto.MaterialType.Unknown;
                case MaterialTypes.MultiMaterial:
                    return SceneXmlDto.MaterialType.MultiMaterial;
                case MaterialTypes.Standard:
                    return SceneXmlDto.MaterialType.Standard;
                case MaterialTypes.Rage:
                    return SceneXmlDto.MaterialType.Rage;
                default:
                    Debug.Fail("Unknown material type.");
                    _log.Error("Unknown material type: {0}.", materialType.ToString());
                    return SceneXmlDto.MaterialType.Unknown;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="textureDef"></param>
        /// <returns></returns>
        private SceneXmlDto.Texture ConvertTextureToDto(SceneXmlModel.Material.TextureDef textureDef)
        {
            SceneXmlDto.Texture textureDto = new SceneXmlDto.Texture();
            if (!String.IsNullOrEmpty(textureDef.FilePath))
            {
                textureDto.Filename = Path.GetFullPath(textureDef.FilePath).ToLower();
            }
            else
            {
                textureDto.Filename = "";
            }
            textureDto.Type = ConvertTextureType(textureDef.Type);
            return textureDto;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="textureType"></param>
        /// <returns></returns>
        private SceneXmlDto.TextureType ConvertTextureType(SceneXmlModel.Material.TextureTypes textureType)
        {
            switch (textureType)
            {
                case TextureTypes.None:
                    return SceneXmlDto.TextureType.Unknown;
                case TextureTypes.DiffuseMap:
                    return SceneXmlDto.TextureType.DiffuseMap;
                case TextureTypes.BumpMap:
                    return SceneXmlDto.TextureType.BumpMap;
                case TextureTypes.SpecularMap:
                    return SceneXmlDto.TextureType.SpecularMap;
                case TextureTypes.GlossMap:
                    return SceneXmlDto.TextureType.GlossMap;
                case TextureTypes.SpecularLevelMap:
                    return SceneXmlDto.TextureType.SpecularLevelMap;
                case TextureTypes.AmbientMap:
                    return SceneXmlDto.TextureType.AmbientMap;
                case TextureTypes.SelfIlluminationMap:
                    return SceneXmlDto.TextureType.SelfIlluminationMap;
                case TextureTypes.OpacityMap:
                    return SceneXmlDto.TextureType.OpacityMap;
                case TextureTypes.FilterColourMap:
                    return SceneXmlDto.TextureType.FilterColourMap;
                case TextureTypes.ReflectionMap:
                    return SceneXmlDto.TextureType.ReflectionMap;
                case TextureTypes.RefractionMap:
                    return SceneXmlDto.TextureType.RefractionMap;
                case TextureTypes.DisplacementMap:
                    return SceneXmlDto.TextureType.DisplacementMap;
                case TextureTypes.DiffuseAlpha:
                    return SceneXmlDto.TextureType.DiffuseAlpha;
                case TextureTypes.BumpAlpha:
                    return SceneXmlDto.TextureType.BumpAlpha;
                case TextureTypes.SpecularAlpha:
                    return SceneXmlDto.TextureType.SpecularAlpha;
                default:
                    Debug.Fail("Unknown texture type.");
                    _log.Error("Unknown texture type: {0}.", textureType.ToString());
                    return SceneXmlDto.TextureType.Unknown;
            }
        }

        /// <summary>
        /// Sets the alpha filenames for the passed in textures based on the next/previous texture types.
        /// </summary>
        /// <param name="textureDtos"></param>
        private void SetAlphaTexturePaths(IList<SceneXmlDto.Texture> textureDtos)
        {
            for (int i = 0; i < textureDtos.Count; i++)
            {
                SceneXmlDto.Texture currentTexture = textureDtos[i];
                SceneXmlDto.Texture nextTexture = ((i + 1) < textureDtos.Count ? textureDtos[i + 1] : null);

                if (nextTexture != null)
                {
                    TextureType currentType = currentTexture.Type;
                    TextureType nextType = nextTexture.Type;

                    if (currentType == TextureType.DiffuseMap && nextType == TextureType.DiffuseAlpha ||
                        currentType == TextureType.SpecularMap && nextType == TextureType.SpecularAlpha ||
                        currentType == TextureType.BumpMap && nextType == TextureType.BumpAlpha)
                    {
                        currentTexture.AlphaFilename = nextTexture.Filename;
                        ++i;
                    }
                    else if (currentType == TextureType.DiffuseAlpha && nextType == TextureType.DiffuseMap ||
                             currentType == TextureType.SpecularAlpha && nextType == TextureType.SpecularMap ||
                             currentType == TextureType.BumpAlpha && nextType == TextureType.BumpMap)
                    {
                        nextTexture.AlphaFilename = nextTexture.Filename;
                        ++i;
                    }
                }
            }
        }
        #endregion
        
        #region Deleting Scene Xml Data
        /// <summary>
        /// Deletes the requested scenexml files.
        /// </summary>
        /// <param name="filename"></param>
        public void DeleteSceneXmlData(string filename)
        {
            AssetPopulationClient client = new AssetPopulationClient(_server, Uri.UriSchemeNetTcp);
            try
            {
                client.DeleteSceneXmlFile(filename);
            }
            catch (System.Exception ex)
            {
                _log.ToolException(ex, "Unhandled exception while deleting scene xml data.");
            }
            finally
            {
                if (client != null)
                {
                    client.CloseOrAbort();
                }
            }
        }
        #endregion
    }
}
