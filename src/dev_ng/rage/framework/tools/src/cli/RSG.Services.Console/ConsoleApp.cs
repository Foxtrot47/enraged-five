﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.OS;
using RSG.Editor.Controls;
using RSG.Pipeline.Core;
using RSG.Services.Configuration;
using RSG.Services.Configuration.Statistics;
using RSG.Services.Console.Processors;
using RSG.Services.Statistics.Consumers.Clients;
using RSG.Services.Statistics.Contracts.ServiceContracts;
using RSG.Base.Configuration;
using RSG.Services.Configuration.Assets;

namespace RSG.Services.Console
{
    /// <summary>
    /// 
    /// </summary>
    public class ConsoleApp : RsInteractiveConsoleApplication
    {
        #region Program Entry Point
        /// <summary>
        /// Program entry point.
        /// </summary>
        /// <param name="args"></param>
        [STAThread]
        static void Main(string[] args)
        {
            ConsoleApp app = new ConsoleApp();
            app.Run();
        }
        #endregion

        #region Fields
        /// <summary>
        /// Assets server configuration information.
        /// </summary>
        private IServer _assetsServer;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="StatisticsConsoleApp"/> class.
        /// </summary>
        public ConsoleApp()
            : base()
        {
            // Register additional commandline args.
            RegisterCommandlineArg("assets-server", LongOption.ArgType.Required, "Name of the assets server to connect to.");

            // Register the commands we wish to support.
            RegisterConsoleCommand("delete-scenexml", DeleteSceneXmlCommandHandler, "<FILE(S)> Deletes all data associated with a particular scene-xml file from the server.");
            RegisterConsoleCommand("sync-scenexml", SyncSceneXmlCommandHandler, "<FILE(S)> Syncs a particular scene-xml file to the server.");
            RegisterConsoleCommand("test", TestCommandHandler, "Test command.");
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Sets up the console and stars a task that will run it.
        /// </summary>
        protected override bool OnConsoleStartup()
        {
            Type blah = typeof(IApplicationSessionService);

            IServicesConfig<IDatabaseServer> config = new AssetsConfig(CommandOptions.Config);
            if (!CommandOptions.ContainsOption("assets-server"))
            {
                _assetsServer = config.DefaultServer;
            }
            else
            {
                string serverName = (string)CommandOptions["assets-server"];
                _assetsServer = config.GetServerByName(serverName);
            }

            return true;
        }
        #endregion

        #region Command Handlers
        /// <summary>
        /// Syncs the contents of a scene xml file to the server.
        /// </summary>
        /// <param name="command">
        /// The command that is being handled by this method.
        /// </param>
        /// <param name="arguments">
        /// The arguments that have be sent with the command.
        /// </param>
        protected void DeleteSceneXmlCommandHandler(string command, string[] arguments)
        {
            if (arguments.Length < 1)
            {
                Log.Error("Invalid number of arguments ({0}); expecting 1 or more: <FILE(S)>.",
                    arguments.Length);
                HelpCommandHandler(command, arguments);
                return;
            }

            IEnumerable<String> filesToDelete =
                arguments.Select(item => Path.GetFullPath(item).ToLower()).Distinct();

            SceneXmlProcessor processor = new SceneXmlProcessor(_assetsServer, Log);
            foreach (String filename in filesToDelete)
            {
                Log.Message("Deleting scene xml data for '{0}'.", filename);
                processor.DeleteSceneXmlData(filename);
            }
        }

        /// <summary>
        /// Syncs the contents of a scene xml file to the server.
        /// </summary>
        /// <param name="command">
        /// The command that is being handled by this method.
        /// </param>
        /// <param name="arguments">
        /// The arguments that have be sent with the command.
        /// </param>
        protected void SyncSceneXmlCommandHandler(string command, string[] arguments)
        {
            if (arguments.Length < 1)
            {
                Log.Error("Invalid number of arguments ({0}); expecting 1 or more: <FILE(S)>.",
                    arguments.Length);
                HelpCommandHandler(command, arguments);
                return;
            }

            // Determine the list of files to process.
            String[] fileList = arguments;
            if (String.Equals(arguments[0], "ALL", StringComparison.InvariantCultureIgnoreCase))
            {
                CommandOptions options = new CommandOptions();
                IProcessorCollection processors = new ProcessorCollection(options.Project.Config);
                IContentTree contentTree = RSG.Pipeline.Content.Factory.CreateTree(options.Branch);
                RSG.Pipeline.Content.ContentTreeHelper treeHelper = new RSG.Pipeline.Content.ContentTreeHelper(contentTree);
                IContentNode[] nodes = treeHelper.GetAllMapSceneXmlNodes();

                IList<String> filesList = new List<String>();
                foreach (RSG.Pipeline.Content.IFilesystemNode node in nodes.OfType<RSG.Pipeline.Content.IFilesystemNode>())
                {
                    filesList.Add(node.AbsolutePath);
                }
                fileList = filesList.ToArray();
            }

            IEnumerable<String> filesToProcess =
                fileList.Select(item => Path.GetFullPath(item).ToLower()).Distinct();

            // Do the sync
            SceneXmlProcessor processor = new SceneXmlProcessor(_assetsServer, Log);
            foreach (String filename in filesToProcess)
            {
                if (!File.Exists(filename))
                {
                    Log.Error("'{0}' does not exist on disk.  Please verify the supplied file path.", filename);
                    continue;
                }
                Log.Message("Syncing scene xml data for '{0}'.", filename);
                processor.SyncSceneXmlData(filename);
            }
        }

        /// <summary>
        /// Command for syncing asset names.
        /// </summary>
        /// <param name="command">
        /// The command that is being handled by this method.
        /// </param>
        /// <param name="arguments">
        /// The arguments that have be sent with the command.
        /// </param>
        protected void TestCommandHandler(string command, string[] arguments)
        {
            /*
            ProfileSessionClient client = new ProfileSessionClient(_server, Uri.UriSchemeNetTcp);
            client.Test();
            client.Close();
            */
        }
        #endregion
    }
}
