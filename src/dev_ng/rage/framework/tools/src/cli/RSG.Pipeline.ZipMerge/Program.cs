﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.ZipMerge
{
    class Program
    {
        #region Constants
        private const string LoggingContext = "RSG.Pipeline.ZipMerge";
        private const string OptionOutput   = "output";
        private const string OptionTaskname = "taskname";
        private const string OptionReverse  = "reverse";
        #endregion

        /// <summary>
        /// ZipMerge entry point
        /// </summary>
        /// <param name="args"></param>
        private static int Main(string[] args)
        {
            LongOption[] longOptions =
            {
                new LongOption(OptionReverse, LongOption.ArgType.Optional, "Reverse the overwrite priority of supplied source zips."), 
                new LongOption(OptionOutput, LongOption.ArgType.Required, "Full file path of the resulting merge."),
                new LongOption(OptionTaskname, LongOption.ArgType.Required, "XGE taskname; echoed to TTY for XGE error log parsing in AP3."),
            };

            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();

            IUniversalLog log = LogFactory.CreateUniversalLog(LoggingContext);
            CommandOptions options = new CommandOptions(args, longOptions);

            #region Validate Options
            // Output task name for AP3 XGE logging compatibility.
            if (options.ContainsOption(OptionTaskname))
            {
                Console.WriteLine("TASK: {0}", options[OptionTaskname]);
                Console.Out.Flush();
            }

            // Verify options.
            if (options.ShowHelp)
            {
                Console.WriteLine(options.GetUsage());
                return Constants.Exit_Success;
            }

            if (!options.ContainsOption(OptionOutput))
            {
                Console.Error.WriteLine("'{0}' is a required option.", OptionOutput);
                Console.WriteLine(options.GetUsage());
                return Constants.Exit_Failure;
            }

            if (options.TrailingArguments.Count() < 2)
            {
                Console.Error.WriteLine("Not enough arguments supplied. You have to supply two or more zip source.");
                return Constants.Exit_Failure;
            }

            #endregion

            // Declaring the collection of sourceFiles here as we might want to filter it platform wise beforehand
            IEnumerable<string> sourceFiles = options.ContainsOption(OptionReverse) ? options.TrailingArguments.Reverse().ToList() : options.TrailingArguments.ToList();
            foreach (string file in sourceFiles)
            {
                if (File.Exists(file))
                {
                    if (!file.EndsWith(".zip", StringComparison.OrdinalIgnoreCase))
                    {
                        log.ToolErrorCtx(LoggingContext, "File '{0}' is not a zip.", file);
                    }
                }
                else
                {
                    // At least one source file is missing. Logging errors so we can abort the whole operation later on.    
                    log.ToolErrorCtx(LoggingContext, "File at {0} is missing.", file);
                }
            }

            // validating outputs
            string outputFile = (string)options[OptionOutput];
            string outputPath = Path.GetDirectoryName(outputFile);

            if (File.Exists(outputFile))
            {
                log.ToolErrorCtx(LoggingContext, "Output file '{0}' already exists. Aborting.", outputFile);
            }

            if (log.HasErrors)
            {
                return Constants.Exit_Failure;
            }

            // Actual merge. The stuff before was only to validate options and init.
            IEnumerable<Tuple<string, ZipArchive>> inputZips = sourceFiles.Select(filename => new Tuple<string, ZipArchive>(filename, ZipFile.Open(filename, ZipArchiveMode.Read))).ToList();

            // We compress an in memory stream, so that we perform everything in mem, and avoid reading and writing from/to disk
            Stream outputStream = new MemoryStream();
            // Magic of ZipArchive we can new it in Update mode and it works anyway. Yay
            ZipArchive outputZip = new ZipArchive(outputStream, ZipArchiveMode.Update, true);

            // Loop through out drawable inputs; 
            foreach (Tuple<string, ZipArchive> input in inputZips)
            {
                ZipArchive inputArchive = input.Item2;

                // Loop through all entries packing them into output.
                foreach (ZipArchiveEntry inputEntry in inputArchive.Entries)
                {
                    string outputEntryName = inputEntry.FullName;
                    ZipArchiveEntry outputEntry = outputZip.CreateEntry(outputEntryName);
                    using (Stream outputEntryStream = outputEntry.Open())
                    {
                        using (Stream inputStream = inputEntry.Open())
                        {
                            inputStream.CopyTo(outputEntryStream);
                        }
                    }
                }
            }

            // Cleanup inputs.
            foreach (Tuple<string, ZipArchive> inputAsset in inputZips)
            {
                inputAsset.Item2.Dispose();
            }

            // Dispose in Memory output archive to allow its stream to be written out later on
            outputZip.Dispose();

            if (outputPath == null)
            {
                log.ToolErrorCtx(LoggingContext, "Output path for specified output \"{0}\" is invalid.", outputFile);
                outputStream.Close();
                return Constants.Exit_Failure;
            }

            // check for output path existence and create the directory if not
            if (!Directory.Exists(outputPath))
                Directory.CreateDirectory(outputPath);

            // Since output ZipArchive has been disposed, we can write the memorystream to a file
            using (FileStream fileStream = new FileStream(outputFile, FileMode.Create))
            {
                outputStream.Seek(0, SeekOrigin.Begin);
                outputStream.CopyTo(fileStream);
            }

            outputStream.Close();

            return Constants.Exit_Success;
        }
    }
}
