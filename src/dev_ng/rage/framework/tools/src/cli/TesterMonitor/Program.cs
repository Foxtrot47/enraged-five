﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Base.SCM;

namespace TesterMonitor
{
    /// <summary>
    /// Main entry point class
    /// </summary>
    class Program
    {
        #region Constants
        #region General
        private static readonly String AUTHOR                       = "Derek Ward";
        private static readonly String EMAIL                        = "derek.ward@rockstarnorth.com";
        private static readonly String APP_NAME                     = "TesterMonitor";
        private static readonly String WIKI                         = "https://devstar.rockstargames.com/wiki/index.php/Test_Farm";
        private static readonly int SLEEP_AFTERKILL_SYSTRAY         = (5*1000); // 5 seconds
        private static readonly int MAX_CRASH_DEFAULT               = 10;
        private static readonly int LOG_POLL_DEFAULT                = (10*1000); // 10 seconds
        private static readonly int SILENT_LOG_TIME_MAX_DEFAULT     = (5*60*1000); // 5 mins
        private static readonly int LOG_LINES_IN_CRASH_DEFAULT      = 5;
        private static readonly String CONSOLE_LOG_DEFAULT          = @"X:\maptester_everything.log";
        private static readonly String LAUNCH_SCRIPT_DEFAULT        = @"N:\RSGEDI\Builders\testfarm\everything1.bat";
        private static readonly String RESPONSE_FILE_DEFAULT        = @"N:\RSGEDI\Builders\testfarm\everything1_skip.rsp";
        private static readonly String SKIP_TELEMETRY_COMMANDLINE   = @"skip_telemetry_samples";
        private static readonly String REGEX_LAST_SAMPLE            = @"(.*)LAST TELEMETRY SAMPLE(.*)";
        private static readonly String REGEX_SAMPLE                 = @".*TELEMETRY SAMPLE\s+\#(\d+)\s+was captured ok.*";
        private static readonly String REGEX_CRASH                  = @"(.*)exception(.*)";
        private static readonly int WARNING_TIMEOUT                 = 60000;

        #endregion // General
        #region Return Codes
        private static readonly int EXIT_SUCCESS            = 0;
        private static readonly int EXIT_WARNING            = 1;
        private static readonly int EXIT_ERROR              = 2;
        private static readonly int EXIT_CRITIC             = 3;
        #endregion //Return Codes
        #region Options
        private static readonly String OPTION_VERBOSE       = "verbose";
        private static readonly String OPTION_LOG           = "log";
        private static readonly String OPTION_UNILOG        = "unilog";
        private static readonly String OPTION_NEW_UNILOG    = "new_unilog";
        private static readonly String OPTION_NOPOPUP       = "nopopups";
        private static readonly String OPTION_HELP          = "help";   
        private static readonly String OPTION_DEBUG         = "debug";     
        private static readonly String OPTION_LOG_TIMEOUT   = "log_timeout";
        private static readonly String OPTION_LOG_POLL      = "log_poll";
        private static readonly String OPTION_MAX_RETRY     = "max_retry";
        private static readonly String OPTION_CONSOLE_LOG   = "console_log";
        private static readonly String OPTION_LAUNCH_SCRIPT = "launch_script";
        private static readonly String OPTION_SKIP_FILENAME = "skip_filename";
        private static readonly String OPTION_LOG_LINES     = "log_lines";
             
        #endregion // Options
        #endregion // Constants

        #region Properties
        private static bool Verbose { get; set; }
        #endregion //Properties

        #region Private Methods

        private static void VerboseMsg(String message, params Object[] args)
        {
            if (Verbose)
            {
                Log.Log__Message(message, args);
            }
        }

        private static void VerboseWarning(String message, params Object[] args)
        {
            if (Verbose)
            {
                Log.Log__Warning(message, args);
            }
        }

        private static void VerboseProfile(String message, params Object[] args)
        {
            if (Verbose)
            {
                Log.Log__Profile(message, args);
            }
        }

        /// <summary>
        /// Create GetOpt object
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        private static Getopt GetOptions(string[] args)
        {
            // Define our executable options.  Stick with the long options
            // (e.g. --opt) rather than the single character versions.
            return new Getopt(args,
                                new LongOption[] 
                                {                                     
                                    new LongOption(OPTION_HELP,         LongOption.ArgType.Optional,    'h', "Show help/usage"),         
                                    new LongOption(OPTION_LOG,          LongOption.ArgType.Required,    'l', "Usage of log files."),
                                    new LongOption(OPTION_NEW_UNILOG,   LongOption.ArgType.None,        'y', "Use new unilog."),
                                    new LongOption(OPTION_NOPOPUP,      LongOption.ArgType.None,        'n', "Disable popups."),
                                    new LongOption(OPTION_VERBOSE,      LongOption.ArgType.Optional,    'z', "Output Verbose to the console."),
                                    new LongOption(OPTION_UNILOG,       LongOption.ArgType.Required,    'u', "Use the universal log."),
                                    new LongOption(OPTION_DEBUG,        LongOption.ArgType.None,        'd', "Allow Visual Studio debugger to be attached."),
                                    new LongOption(OPTION_LOG_TIMEOUT,  LongOption.ArgType.Optional,    't', "Timeout (ms) of log inactivity that would indicate a crash."),
                                    new LongOption(OPTION_LOG_POLL,     LongOption.ArgType.Optional,    'p', "Poll rate (ms) of log file."),
                                    new LongOption(OPTION_MAX_RETRY,    LongOption.ArgType.Optional,    'r', "Maximum number of retries."),
                                    new LongOption(OPTION_CONSOLE_LOG,  LongOption.ArgType.Required,    'c', "Console log filename."),
                                    new LongOption(OPTION_LAUNCH_SCRIPT,LongOption.ArgType.Required,    's', "Script that is called that launches the game."),
                                    new LongOption(OPTION_SKIP_FILENAME,LongOption.ArgType.Required,    'k', "Filename of response file that will log the nodes captured."),
                                    new LongOption(OPTION_LOG_LINES,    LongOption.ArgType.Optional,    'x', "Last n log lines to display upon a crash."),                                    
                                }
                );
        }

        private static void ShowCrash(int logLinesToShowUponCrash, List<string> logLines, int timeoutMs)
        {
            Log.Log__Warning("Crash or Hang Detected ( no samples written in last {1} seconds) : Showing last {0} log lines ", logLinesToShowUponCrash, timeoutMs / 1000);
            int startIdx = Math.Max(logLines.Count - 1 - logLinesToShowUponCrash, 0);
            for (int i = startIdx; i < logLines.Count; i++)
            {
                Log.Log__Warning(logLines[i]);
            }
        }

        private static void RestartGame(Process process)
        {
            Log.Log__Message("Stopping process");
            process.Close();

            Log.Log__Message("Starting process");
            process.Start();
        }

        private static void LaunchGame(Process process, string launchScript)
        {
            if (File.Exists(launchScript))
            {
                process.StartInfo.FileName = Environment.ExpandEnvironmentVariables("cmd.exe");
                process.StartInfo.Arguments = Environment.ExpandEnvironmentVariables("/C " + launchScript);
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.Start();
            }
        }

        private static void DeletePreviousLogFile(string consoleLogFilename)
        {
            if (File.Exists(consoleLogFilename))
            {
                Log.Log__Message("Delete {0}", consoleLogFilename);
                File.Delete(consoleLogFilename);
            }
        }

        private static void KillSysTrayRfs()
        {
            string sysTrayProcessName = "SysTrayRfs";
            foreach (System.Diagnostics.Process proc in System.Diagnostics.Process.GetProcesses())
            {
                if (string.Compare(proc.ProcessName, sysTrayProcessName, true) == 0)
                {
                    Log.Log__Message("Kill SysTrayRfs");
                    proc.Kill();
                }
            }

            // Wait - sometimes processes take time to shutdown
            Log.Log__Message("Sleep {0}", SLEEP_AFTERKILL_SYSTRAY/1000);
            System.Threading.Thread.Sleep(SLEEP_AFTERKILL_SYSTRAY);
        }

        #endregion // Private Methods

        #region Main
        /// <summary>
        /// Main entry point
        /// </summary>
        /// <param name="args"></param>
        /// <returns>true upon success, false upon errors</returns>
        static int Main(string[] args)
        {
            #region Startup crud
            int exit_code = EXIT_SUCCESS;
            Getopt options = GetOptions(args);

            LogFactory.CreateUniversalLogFile(Log.StaticLog);
            LogFactory.CreateApplicationConsoleLogTarget();

            bool useUniLog = false;
            #endregion // Startup crud

            #region Parse Options

            int maxCrashes = options.HasOption(OPTION_MAX_RETRY) ? Int32.Parse(options[OPTION_MAX_RETRY] as String) : MAX_CRASH_DEFAULT;
            int sleepMs = options.HasOption(OPTION_LOG_POLL) ? Int32.Parse(options[OPTION_LOG_POLL] as String) : LOG_POLL_DEFAULT;
            int silentLogTimeMax = options.HasOption(OPTION_LOG_TIMEOUT) ? Int32.Parse(options[OPTION_LOG_TIMEOUT] as String) : SILENT_LOG_TIME_MAX_DEFAULT;
            int logLinesToShowUponCrash = options.HasOption(OPTION_LOG_LINES) ? Int32.Parse(options[OPTION_LOG_LINES] as String) : LOG_LINES_IN_CRASH_DEFAULT;
            string consoleLogFilename = options.HasOption(OPTION_CONSOLE_LOG) ? options[OPTION_CONSOLE_LOG] as String : CONSOLE_LOG_DEFAULT;
            string launchScript = options.HasOption(OPTION_LAUNCH_SCRIPT) ? options[OPTION_LAUNCH_SCRIPT] as String : LAUNCH_SCRIPT_DEFAULT;
            string skipFilename = options.HasOption(OPTION_SKIP_FILENAME) ? options[OPTION_SKIP_FILENAME] as String : RESPONSE_FILE_DEFAULT;
            string logFilename = options.HasOption(OPTION_LOG) ? (options[OPTION_LOG] as String) : String.Empty;

            bool nopopups = options.HasOption(OPTION_NOPOPUP);
            bool debugger = options.HasOption(OPTION_DEBUG);
            Verbose = options.HasOption(OPTION_VERBOSE);
            #endregion // Parse Options

            #region Debug Mode Attach Point
            // If debug option was set and there is no debugger currently 
            // attached to our process then wait for it or for user input.
            if (debugger && !Debugger.IsAttached)
            {
                Console.WriteLine("Attach debugger or press any key to continue...");
                Console.ReadKey();
            }
            #endregion // Debug Mode Attach Point

            #region Startup Message
            string version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            string helpLink = WIKI;
            string appAndVersion = string.Format("{0} v{1}", APP_NAME, version);
            string startupMsg = string.Format("=== {0} ===\n\n  {3}\n\n  Command Line: {1}.exe \n\n\t{2}\n", appAndVersion, APP_NAME, string.Join(" ", args), helpLink);
            Log.Log__Message(startupMsg);

            if (options.HasOption(OPTION_HELP))
            {
                if (!Verbose)
                {
                    Console.WriteLine("\n{0}", startupMsg);
                }
                Console.WriteLine(options.Usage());
                return 0;
            }
            #endregion // Startup Message

            #region Main control logic

            Process process = new Process();
            List<string> logLines = new List<string>();
            try
            {
                // ==========================================================
                // ================   MAIN CONTROL LOGIC  ===================
                // ==========================================================                
                Log.Log__Message("=== {1} Started @ {0} ===", System.DateTime.Now, APP_NAME);
                Log.Log__Message("\n");
                Log.Log__Message("- Poll logfile {0} every {1}s", consoleLogFilename, sleepMs/1000);
                Log.Log__Message("- Considered crashed after no logfile activity for {0}s", silentLogTimeMax/1000);
                Log.Log__Message("- Restarts captures for max of {0} retries", maxCrashes);
                Log.Log__Message("- Show last {0} log lines upon crash.", logLinesToShowUponCrash);
                Log.Log__Message("- Runs {0}", launchScript); 
                Log.Log__Message("- Uses response file {0}", skipFilename);
                Log.Log__Message("- Last sample regex {0}", REGEX_LAST_SAMPLE);
                Log.Log__Message("- Sample regex {0}", REGEX_SAMPLE);
                Log.Log__Message("- Crash regex {0}", REGEX_CRASH);
                Log.Log__Message("\n");

                int sampleCaptured = 0;
                int numCrashes = 0;
                int silentLogTime = 0;
                long seekOffset = 0;
                bool finished = false;
                bool retrySameSample = true;

                // Kill SysTrayRfs
                KillSysTrayRfs();

                // Delete previous logfile
                DeletePreviousLogFile(consoleLogFilename);

                // Skip filename - if it exists delete it.
                if (File.Exists(skipFilename))
                {
                    Log.Log__Warning("{0} already exists and has been deleted", skipFilename);
                    File.Delete(skipFilename);                    
                }

                // Launch Game
                LaunchGame(process, launchScript);

                // Check progress - loop until we are finished or it has crashed too many times.
                do
                {
                    // Sleep
                    Log.Log__Message("Sleep {0}", sleepMs/1000);
                    System.Threading.Thread.Sleep(sleepMs);

                    // Read Log file
                    if (File.Exists(consoleLogFilename))
                    {
                        bool crashed = false;

                        {
                            FileStream stream = new FileStream(consoleLogFilename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                            StreamReader reader = new StreamReader(stream);

                            // Seek to last position - fortunately we don't have partial lines flushed so it seems so that is good news, nothing should go unparsed.
                            // regardless our reliance on parsing tty is only critical to read the finished message.
                            reader.BaseStream.Seek(seekOffset, SeekOrigin.Begin);
                            
                            silentLogTime += sleepMs;
                            int linesLastRead = 0;

                            string line;
                            while ((line = reader.ReadLine()) != null)
                            {
                                VerboseMsg(line);
                                logLines.Add(line);
                                linesLastRead++;                               

                                if (Regex.IsMatch(line, REGEX_CRASH))
                                {
                                    crashed = true;
                                    break;
                                }
                                else if (Regex.IsMatch(line, REGEX_SAMPLE))
                                {
                                    silentLogTime = 0;// we have lines and they are a sample, reset counter of time since we last read such lines in the log file.

                                    Match match = Regex.Match(line,REGEX_SAMPLE);
                                    int sampleNum = int.Parse(match.Groups[1].Value);

                                    sampleCaptured++;
                                    Log.Log__Message("====TELEMETRY SAMPLE {0} DETECTED IN LOG : (Total Captured {1}) ====", sampleNum, sampleCaptured);

                                    if (sampleNum < sampleCaptured - 1)
                                    {
                                        Log.Log__Warning("Sample #{0} is not expected, should be >= #{1}", sampleNum, sampleCaptured);
                                    }

                                    // Record node/samples taken in this progress response file - this file will be used if we resume the capture
                                    WriteResumeFile(skipFilename, sampleCaptured);
                                }
                                else if (Regex.IsMatch(line, REGEX_LAST_SAMPLE)) 
                                {
                                    finished = true;
                                    Log.Log__Message("================================================================");
                                    Log.Log__Message("The last telemetry sample was detected - the capture is complete");
                                    Log.Log__Message("================================================================");
                                    Log.Log__Message("Summary : {0} samples captured, {1} crashes", sampleCaptured, numCrashes);
                                    Log.Log__Message("================================================================");

                                    // Remove skip filename
                                    if (File.Exists(skipFilename))
                                    {
                                        File.Delete(skipFilename);
                                    }
                                }                                
                            }

                            if (linesLastRead > 0)
                            {
                                Log.Log__Message("Read {0} lines", linesLastRead);
                            }

                            seekOffset = reader.BaseStream.Position;
                            reader.Close();
                            stream.Close();
                        }

                        if (silentLogTimeMax-silentLogTime <= WARNING_TIMEOUT )
                        {
                            int time = (silentLogTimeMax - silentLogTime) / 1000;
                            if (time > 0)
                            {
                                Log.Log__Warning("Warning: Timeout in {0} seconds", time);
                            }
                        }

                        // Restart if crashed
                        if (crashed || silentLogTime > silentLogTimeMax)
                        {
                            ShowCrash(logLinesToShowUponCrash, logLines, silentLogTimeMax);
                            silentLogTime = 0;
                            seekOffset = 0;
                            logLines.Clear();
                            numCrashes++;

                            if (numCrashes < maxCrashes)
                            {
                                Log.Log__Message("\n\n\n==================================================");
                                Log.Log__Message("#Crashes = {0}/{1}", numCrashes, maxCrashes);
                                KillSysTrayRfs();
                                DeletePreviousLogFile(consoleLogFilename);

                                // The logic here retries any smaple that crashed, if it crashes again it is skipped.
                                if (retrySameSample)
                                {                                    
                                    WriteResumeFile(skipFilename, sampleCaptured);
                                }
                                else
                                {
                                    WriteResumeFile(skipFilename, sampleCaptured+1);
                                }

                                retrySameSample = !retrySameSample;

                                RestartGame(process);
                            }
                            else
                            {
                                Log.Log__Error("#Crashes = {0}/{1}", numCrashes, maxCrashes);
                                Log.Log__Error("TELEMETRY CAPTURE HAS GIVEN UP THE GAME IS CRASHING TOO MUCH.");
                                finished = true;
                            }
                        }
                    }
                } while (!finished);

                if (Log.StaticLog.HasWarnings)
                {
                    VerboseWarning("*** {0} : Warnings encountered - please review logs & console output. ***", APP_NAME);
                    exit_code = EXIT_WARNING;
                }
             
                if (Log.StaticLog.HasErrors)
                {
                    VerboseWarning("*** {0} : Errors encountered - please review logs & console output. ***", APP_NAME);
                    exit_code = EXIT_ERROR;
                }

                Log.Log__Message("=== {1} Finished @ {0} with exit code {2} ===", System.DateTime.Now, APP_NAME, exit_code);

                // ==============================================================
                // ==============================================================
                // ==============================================================                
            }
            catch (Exception ex)
            {
                process.Close();

                #region main exception handling
                Log.Log__Exception(ex, "*** Top Level Exception in {0} ***\n===Msg===\n{1}\n===Stack trace===\n{2}", APP_NAME, ex.Message, ex.StackTrace);

                // Return immediately so we don't display error/warning dialog.
                if (!useUniLog)
                {
                    Log.Log__Message("Exiting with {0}", EXIT_CRITIC);
                    return (EXIT_CRITIC);
                }
                #endregion // main exception handling
            }
            #endregion // Main control logic

            #region ulog handling of errors and warnings
 
            if (Log.StaticLog.HasErrors)
                exit_code = EXIT_ERROR;
            // Display error and warning dialog.
            if (!nopopups && (Log.StaticLog.HasErrors || Log.StaticLog.HasWarnings))
            {
            }
            #endregion //  ulog handling of errors and warnings

            #region Return
            Environment.ExitCode = exit_code;
            Log.Log__Message("Exiting with {0}", exit_code);

            LogFactory.ApplicationShutdown();

            return (exit_code);
            #endregion // Return
        } // Main

        private static void WriteResumeFile(string skipFilename, int sampleCaptured)
        {
            TextWriter tw = new StreamWriter(skipFilename);
            tw.WriteLine("-{1}={0}", sampleCaptured, SKIP_TELEMETRY_COMMANDLINE);
            tw.Close();
        }

        #endregion // main
    } // class Program
} // namespace TesterMonitor
