﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.IO;
using System.Threading;
using System.Xml;
using RSG.Base.Logging.Universal;

namespace RSG.Pipeline.Animation.Cutscene.CutsceneConcatenationLighting
{       
    class PartEntry
    {
        public string strParentFolder = string.Empty;
        public List<string> shotEntries = new List<string>();
    }

    class CutsceneConcatLighting
    {
        private static readonly String LOG_CTX = "Cutscene Concatenation Lighting";

        List<PartEntry> lstParts = new List<PartEntry>();
        List<string> lstOrderedParts = new List<string>();
        private string strCutXMLConcatPath = String.Empty; 
        private string strLightMergePath = String.Empty; 
        private string strAnimConcatPath = String.Empty; 
        private string strClipConcatPath = String.Empty; 
        private string strTempDir = Path.GetTempPath() + "LightManager";
        string strOutputFolder = String.Empty;
        private IUniversalLog log = null;

        public CutsceneConcatLighting(string cutConcatExe, string cutLightMergeExe, string animConcatExe, string clipConcatExe, IUniversalLog l)
        {
            strCutXMLConcatPath = cutConcatExe;
            strLightMergePath = cutLightMergeExe;
            strAnimConcatPath = animConcatExe;
            strClipConcatPath = clipConcatExe;
            log = l;
        }

        private void GetAnimatedLights(string filename, ref List<string> lstLights)
        {
            if (!File.Exists(filename)) return;

            var xmlDoc = new XmlDocument();
            xmlDoc.Load(filename);

            XmlNodeList lightNodes = xmlDoc.SelectNodes("/rage__cutfCutsceneFile2/pCutsceneObjects/Item[@type='rage__cutfAnimatedLightObject']");

            foreach (XmlNode node in lightNodes)
            {
                string objectName = node["cName"].InnerText;

                lstLights.Add(objectName);
            }
        }

        private bool GetDurationFromCutFile(string strCutFile, out double fDuration)
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(strCutFile);

            var node = xmlDoc.SelectSingleNode("/rage__cutfCutsceneFile2/fTotalDuration/@value");
            double dResult = 0;
            if (Double.TryParse(node.Value, out dResult))
            {
                fDuration = (float)dResult;
                return true;
            }

            fDuration = 0;
            return false;
        }

        private bool ValidateLightsAreUnique()
        {
            bool bResult = true;
            Dictionary<string, string> dictLightList = new Dictionary<string, string>();

            for (int i = 0; i < lstParts.Count; ++i)
            {
                string filename = Path.Combine(lstParts[i].strParentFolder, "data.lightxml");

                if (!File.Exists(filename)) continue;

                var xmlDoc = new XmlDocument();
                xmlDoc.Load(filename);

                XmlNodeList lightNodes = xmlDoc.SelectNodes("/rage__cutfCutsceneFile2/pCutsceneObjects/Item[@type='rage__cutfLightObject']");

                foreach (XmlNode node in lightNodes)
                {
                    string objectName = node["cName"].InnerText;

                    if (dictLightList.ContainsKey(objectName.ToLower()))
                    {
                        string strValue = String.Empty;
                        dictLightList.TryGetValue(objectName.ToLower(), out strValue);

                        log.ErrorCtx(LOG_CTX, "Light object '{0}' in '{1}' is already defined in '{2}'", objectName, filename, strValue);
                        bResult = false;
                    }

                    dictLightList.Add(objectName.ToLower(), filename);
                }
            }

            if (bResult)
                log.MessageCtx(LOG_CTX, "Lights validated as unique");

            return bResult;
        }

        // Process the animated lights (if any), this will create lights from the shots in the external concat directory.
        private bool ProcessAnimatedLights()
        {
            // Look through every part to see if it contains any animated lights
            for (int i = 0; i < lstOrderedParts.Count; ++i)
            {
                string strMainLightFile = Path.Combine(Path.GetDirectoryName(lstOrderedParts[i]), "data.lightxml");
                List<string> lstAnimatedLights = new List<string>();
                GetAnimatedLights(strMainLightFile, ref lstAnimatedLights);

                log.MessageCtx(LOG_CTX, "'{0}' has '{1}' animated lights", strMainLightFile, lstAnimatedLights.Count);

                List<double> lstEntries = new List<double>();

                if (lstAnimatedLights.Count > 0)
                {
                    double iStartCount = 0.0f;
                    double iEndCount = 0.0f;

                    // Now iterate over every part again and create a list to create a concatted animation
                    for (int j = 0; j < lstOrderedParts.Count; ++j)
                    {
                        string strStreamFile = Path.Combine(lstOrderedParts[j], "data_stream.cutxml");
                        if (!File.Exists(strStreamFile))
                        {
                            log.ErrorCtx(LOG_CTX, "Unable to find file '{0}'", strStreamFile);
                            return false;
                        }

                        double fDuration = 0;
                        GetDurationFromCutFile(strStreamFile, out fDuration);

                        if (j < i)
                        {
                            iStartCount += fDuration;
                        }

                        if (j == i)
                        {
                            if (iStartCount != 0.0f)
                            {
                                lstEntries.Add(iStartCount);
                            }

                            lstEntries.Add(-1);
                        }

                        if (j > i)
                        {
                            iEndCount += fDuration;
                        }

                        if (j == lstOrderedParts.Count - 1)
                        {
                            if (iEndCount != 0.0f)
                            {
                                lstEntries.Add(iEndCount);
                            }
                        }
                    }

                    for (int k = 0; k < lstAnimatedLights.Count; ++k)
                    {
                        string strLightFile = strTempDir + "\\" + lstAnimatedLights[k] + ".anim";
                        if (File.Exists(strLightFile))
                        {
                            if (lstEntries.Count == 1) // If we have 1 entry, it is a 1 part concat and just copy the file
                            {
                                string strOutLightFile = strOutputFolder + "/" + lstAnimatedLights[k] + ".anim";
                                if (File.Exists(strOutLightFile))
                                {
                                    File.Delete(strOutLightFile);
                                }
                                File.Copy(strLightFile, strOutLightFile);
                            }
                            else if (lstEntries.Count == 2) // We should only ever have a max of 3 entries, before/after and our scene
                            {
                                if (lstEntries[0] == -1 && lstEntries[1] != -1) // Anim/Padding
                                {
                                    string strOutput = String.Empty;
                                    if (!RunProcess(strAnimConcatPath, "-anims=" + strLightFile + " -leadout=" + lstEntries[1] + " -overlap=0 -out=" + Path.Combine(strOutputFolder, lstAnimatedLights[k]) + " –absolutemover -superset" + " -nopopups", out strOutput, true))
                                    {
                                        return false;
                                    }
                                }

                                if (lstEntries[0] != -1 && lstEntries[1] == -1) // Padding/Anim
                                {
                                    string strOutput = String.Empty;
                                    if (!RunProcess(strAnimConcatPath, "-anims=" + strLightFile + " -leadin=" + lstEntries[0] + " -overlap=0 -out=" + Path.Combine(strOutputFolder, lstAnimatedLights[k]) + " –absolutemover -superset" + " -nopopups", out strOutput, true))
                                    {
                                        return false;
                                    }
                                }
                            }
                            else if (lstEntries.Count == 3) // If we have 3 this should always be Padding/Anim/Padding
                            {
                                if (lstEntries[1] == -1)
                                {
                                    string strTemp = Path.GetTempFileName();
                                    File.Delete(strTemp);

                                    string strOutput = String.Empty;
                                    if (!RunProcess(strAnimConcatPath, "-anims=" + strLightFile + " -leadin=" + lstEntries[0] + " -overlap=0 -out=" + strTemp + " –absolutemover -superset" + " -nopopups", out strOutput, true))
                                    {
                                        return false;
                                    } 
                                    
                                    if (!RunProcess(strAnimConcatPath, "-anims=" + strTemp + " -leadout=" + lstEntries[2] + " -overlap=0 -out=" + Path.Combine(strOutputFolder, lstAnimatedLights[k]) + " –absolutemover -superset" + " -nopopups", out strOutput, true))
                                    {
                                        DeleteFile(strTemp);
                                        return false;
                                    }

                                    DeleteFile(strTemp);
                                }
                            }
                        }

                        string strLightClipFile = strTempDir + "\\" + lstAnimatedLights[k] + ".clip";
                        if (File.Exists(strLightClipFile))
                        {
                            string strOutput = String.Empty;
                            if (!RunProcess(strClipConcatPath, "-clips=" + strLightClipFile + " -hidealloc -nopopups -out=" + Path.Combine(strOutputFolder, lstAnimatedLights[k]), out strOutput, true))
                            {
                                return false;
                            }
                        }
                    }
                }
            }

            return true;
        }

        private void AddPart(string filename)
        {
            filename = filename.Replace("\\", "/");

            lstOrderedParts.Add(filename);

            for (int i = 0; i < lstParts.Count; ++i)
            {
                if (String.Compare(Path.GetDirectoryName(filename), lstParts[i].strParentFolder, StringComparison.OrdinalIgnoreCase) == 0)
                {
                    lstParts[i].shotEntries.Add(Path.GetFileNameWithoutExtension(filename));
                    return;
                }
            }

            PartEntry p = new PartEntry();
            p.strParentFolder = Path.GetDirectoryName(filename);
            p.shotEntries.Add(Path.GetFileNameWithoutExtension(filename));
            lstParts.Add(p);
        }

        private bool RunProcess(string strExe, string strArguments, out string strOutput, bool bWaitForExit)
        {
            try
            {
                System.Diagnostics.Process pExe = new System.Diagnostics.Process();
                pExe.StartInfo.FileName = strExe;
                pExe.StartInfo.Arguments = strArguments;
                pExe.StartInfo.CreateNoWindow = true;
                pExe.StartInfo.UseShellExecute = false;
                pExe.StartInfo.RedirectStandardOutput = true;
                pExe.Start();

                log.MessageCtx(LOG_CTX, "{0} : {1}", strExe, strArguments);

                strOutput = String.Empty;
                if (bWaitForExit)
                {
                    strOutput = pExe.StandardOutput.ReadToEnd();
                    pExe.WaitForExit();
                    log.MessageCtx(LOG_CTX, strOutput);

                    if (pExe.ExitCode != 0)
                    {
                        return false;
                    }
                }

                return true;
            }
            catch (System.ComponentModel.Win32Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Unhandled Exception during process {0} args: {1}", strExe, strArguments);
            }

            strOutput = String.Empty;
            return false;
        }

        // Split the parent internal concat light file into child part light files and save them into a temp dir.
        private bool SplitLightFiles()
        {
            foreach (PartEntry part in lstParts)
            {
                string strOutput = String.Empty;
                if (!RunProcess(strLightMergePath, "-split -cutFile=" + part.strParentFolder + @"\data.cutxml -cutLight=" + part.strParentFolder + @"\data.lightxml -hidealloc -out=" + strTempDir, out strOutput, true))
                {
                    return false;
                }
            }

            return true;
        }

        private bool JoinLightFiles()
        {
            string strTempCutFilename = Path.GetTempFileName();
            TextWriter twCutFileList = new StreamWriter(strTempCutFilename);

            // Create the cutfile list which is used by the cutfconcat tool
            for (int iFolder = 0; iFolder < lstOrderedParts.Count; ++iFolder)
            {
                string[] paths = lstOrderedParts[iFolder].Split('/');
                string s = Path.GetDirectoryName(lstOrderedParts[iFolder]);

                string lightFile = strTempDir + "\\" + paths[paths.Length - 2] + "_" + Path.GetFileNameWithoutExtension(lstOrderedParts[iFolder]) + ".lightxml";
                twCutFileList.WriteLine(lightFile);
            }

            twCutFileList.Close();

            string strOutput = String.Empty;
            if (!RunProcess(strCutXMLConcatPath, "-cutlistFile=" + strTempCutFilename + " -outputCutfile=" + strOutputFolder + "\\data.lightxml -outputDir=" + strOutputFolder + " -hidealloc -external -nopopups", out strOutput, true))
            {
                File.Delete(strTempCutFilename);
                return false;
            }

            File.Delete(strTempCutFilename);
            return true;
        }

        private void UpdateObjectsToAnimationManager()
        {
            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.Load(strOutputFolder + "\\data.lightxml");

            XmlNodeList lightNodes = xmlDoc.SelectNodes("/rage__cutfCutsceneFile2/pCutsceneEventList/Item[@type='rage__cutfObjectIdEvent']");

            // Lights have there own events now, not just SET/CLEAR events so we set a unique id that gets replaced later. These events are owned by the anim manager
            foreach (XmlNode node in lightNodes)
            {
                if (node["iEventId"].Attributes["value"].Value == "22" || node["iEventId"].Attributes["value"].Value == "23")
                    node["iObjectId"].Attributes["value"].Value = "-2";
            }

            xmlDoc.Save(strOutputFolder + "\\data.lightxml");
        }

        private bool ValidateLightFilesExist()
        {
            foreach(PartEntry p in lstParts)
            {
                if(!File.Exists(String.Format("{0}\\data.lightxml", p.strParentFolder)))
                {
                    log.WarningCtx(LOG_CTX, "Unable to find light file for '{0}'. Dummy file will be created.", p.strParentFolder);
                    //return false;
                }

                log.MessageCtx(LOG_CTX, String.Format("{0}\\data.lightxml", p.strParentFolder));
            }

            return true;
        }

        public bool Process(string[] folders, string output)
        {
            Reset();
            DeleteFolderAndContents(strTempDir);
            CreateDirectory(strTempDir);

            strOutputFolder = output;

            foreach (string folder in folders)
            {
                AddPart(folder);
            }

            ValidateLightFilesExist();

            bool bResult = true;

            bResult = bResult && ValidateLightsAreUnique();

            bResult = bResult && SplitLightFiles();

            bResult = bResult && ProcessAnimatedLights();

            bResult = bResult && JoinLightFiles();

            if(bResult)
                UpdateObjectsToAnimationManager();

            return bResult;
        }

        #region Helper Functions

        private void Reset()
        {
            lstParts.Clear();
            lstOrderedParts.Clear();
            strOutputFolder = String.Empty;
        }

        private void CreateDirectory(string folder)
        {
            try
            {
                Directory.CreateDirectory(folder);
            }
            catch (Exception /*e*/) { }
        }

        private void DeleteFolderAndContents(string folder)
        {
            try
            {
                Directory.Delete(folder, true);
            }
            catch (Exception /*e*/) { }
        }

        private void DeleteFile(string file)
        {
            try
            {
                File.Delete(file);
            }
            catch (Exception /*e*/) { }
        }

        #endregion
    }
}
