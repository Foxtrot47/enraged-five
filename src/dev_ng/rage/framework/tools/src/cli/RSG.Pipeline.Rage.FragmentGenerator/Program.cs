﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;
using RSG.Pipeline.Services.Rage;
using RSG.Base.OS;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Rage;
using RSG.Rage.Entity;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.Rage.FragmentGenerator
{
    sealed class Program
    {
        #region Constants
        private static readonly int EXIT_SUCCESS = 0;
        private static readonly int EXIT_ERROR = 1;

        private static readonly String OPTION_LOD_DESCRIPTION = "loddesc";
        private static readonly String OPTION_INPUT = "input";
        private static readonly String OPTION_OUTPUT = "output";
        #endregion // Constants
        
        [STAThread]
        static int Main(string[] args)
        {
            int exit_code = EXIT_SUCCESS;
            // Define our executable options.  Stick with the long options
            // (e.g. --opt) rather than the single character versions.
            Getopt options = new Getopt(args, new LongOption[] {
                new LongOption(OPTION_LOD_DESCRIPTION, LongOption.ArgType.Required,
                    "LOD description filename."),
                new LongOption(OPTION_INPUT, LongOption.ArgType.Required,
                    "Input filename."),
                new LongOption(OPTION_OUTPUT, LongOption.ArgType.Required,
                    "Output filename."),
            });

            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IConfig config = ConfigFactory.CreateConfig();
            IUniversalLog log = LogFactory.ApplicationLog;
            
            String lodDescriptionFile = options[OPTION_LOD_DESCRIPTION] as String;
            String inputDir = options[OPTION_INPUT] as String;
            String outputDir = options[OPTION_OUTPUT] as String;

            try
            {
                IEnumerable<ILodDescription> lds = LodDescription.LoadLodDescriptionsFromFile(lodDescriptionFile, config, log);
                foreach (LodDescription ld in lds)
                {
                    String outputDirLd = ld.GetDataLocation(outputDir);
                    LodHierarchyBuilder lhb = new LodHierarchyBuilder(ld, inputDir, outputDirLd, log);
                    lhb.Fabricate();
                }
            }
            catch (Exception ex)
            {
                log.ToolExceptionCtx("LOD Generator", ex, "Exception creating lod hiearchy from {0}", lodDescriptionFile);
                return (Constants.Exit_Failure);
            }

            if (LogFactory.HasError())
                exit_code = Constants.Exit_Failure;

            return exit_code;
        }
    }
}
