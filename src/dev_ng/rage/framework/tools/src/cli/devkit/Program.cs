using System;
using System.Collections.Generic;
using System.Text;
using System.Timers;
using System.Configuration;
using System.Diagnostics;
using System.Threading;
using System.Net;
using RSG.Base.Logging;
using XDevkit;
using XBox360;
using PS3;
using GameInterface;

// DW - this app is a quickly 'boshed together' thing that will eventually be replaced by RS Target Manager.

namespace DevKit
{
    class Program
    {
        #region command lines
        // -platform 360 -workingdir X:\jimmy\build\dev\ -exe JimmyBeta_xenon.xex -commandline "-level=1" -duration 60 -deploydir xE:\jimmy -reboot 0
        // -platform PS3 -workingdir X:\jimmy\build\dev\ -exe jimmy_psn_beta.self -commandline "-level=1" -duration 60 -deploydir xE:\jimmy -reboot 0
        #endregion //command lines

        #region TestData
        static XDevkit.XboxManagerClass xboxManager;
        static XDevkit.XboxConsole xboxConsole;
//static XDevkit.IXboxDebugTarget xboxDebugTarget;
        static String xboxTarget = "";
        #endregion // TestData

        #region Member Variables
        static double VERSION = 1.1;
        static String m_platform = "";
        static String m_workingdir = "";
        static String m_exe = "";
        static String m_commandline = "";
        static String m_deploydir = "";
        static String m_ipAddr = "";
        static int m_duration = int.MaxValue;
        static int m_numExpectedArgs = 16;
        static String m_machineName = "MyMachineName";
        static String m_name = "DereksXboxName";
        static String m_config_name = "JimmyBeta?";
        static int m_reboot = 0;
        static byte[] m_addr_360 = new byte[4] { 0, 0, 0, 0 }; // removed my hardwired devkits for safety!
        static byte[] m_addr_PS3 = new byte[4] { 0, 0, 0, 0 };
        static bool bTimerExit = false;
        #endregion // Member Variables

        #region XBoxcallbackTest
        static void xboxConsole_OnStdNotify(XboxDebugEventType eventCode, IXboxEventInfo eventInformation)
        {
            Console.WriteLine(eventInformation.Info.Message);

            switch (eventCode)
            {
                // Handler for DM_EXEC changes, mainly for detecting reboots
                case XDevkit.XboxDebugEventType.ExecStateChange:

                    Console.WriteLine("Code" + eventInformation.Info.ExecState);
                    if (eventInformation.Info.ExecState != XDevkit.XboxExecutionState.Rebooting)
                        //  xDebugTarget.StopOn(XboxStopOnFlags.OnStackTrace, true);
                        Console.WriteLine("Connection to Xbox successful\n");
                    else if (eventInformation.Info.ExecState == XDevkit.XboxExecutionState.Rebooting)
                        Console.WriteLine("Rebooting\n");
                    else if (eventInformation.Info.ExecState == XboxExecutionState.RebootingTitle)
                        Console.WriteLine("RebootingTitle\n");
                    else
                        Console.WriteLine("Unhandled ExecStateChange\n");
                    break;
                case XDevkit.XboxDebugEventType.DebugString:
                    string[] messages = eventInformation.Info.Message.Split('\n');
                    break;
                case XDevkit.XboxDebugEventType.AssertionFailed:
                    Console.WriteLine("Assertion failed: \r\n");
                    Console.WriteLine(eventInformation.Info.Message);
                    break;
                case XDevkit.XboxDebugEventType.RIP: // OS RIP
                    Console.WriteLine("RIP : " + eventInformation.Info.Message);
                    //RipManager xboxRipManager = new RipManager(xboxConsole, xboxWatson);
                    //xboxRipManager.HandleRip(eventInformation);
                    break;
                case XDevkit.XboxDebugEventType.ExecutionBreak:
                    {
                        Console.WriteLine("Execution Breakpoint: ");// + eventInformation.Info.Address + " " +
                        // ExceptionManager xboxExceptionManager = new ExceptionManager(xboxConsole, xboxWatson);
                        //xboxExceptionManager.HandleException(eventCode, eventInformation);
                        break;
                    }
                case XDevkit.XboxDebugEventType.DataBreak:
                    {
                        Console.WriteLine("Data Breakpoint : " + eventInformation.Info.Address + " " +
                                eventInformation.Info.Thread.ThreadId + " " +
                                eventInformation.Info.GetType() + " " +
                                eventInformation.Info.Address);
                        // ExceptionManager xboxExceptionManager = new ExceptionManager(xboxConsole, xboxWatson);
                        //xboxExceptionManager.HandleException(eventCode, eventInformation);
                        break;
                    }
                case XDevkit.XboxDebugEventType.Exception:
                    if (eventInformation.Info.Flags == XDevkit.XboxExceptionFlags.FirstChance)
                    {
                        Console.WriteLine("First Chance Exception : Thread Id " + eventInformation.Info.Thread.ThreadId +
                                  " Code " + eventInformation.Info.Code + " Address " +
                                  eventInformation.Info.Address + " Flags " +
                                  eventInformation.Info.Flags + "\n");
                    }
                    else
                    {
                        Console.WriteLine("Exception : " + eventInformation.Info.Thread.ThreadId +
                                " " + eventInformation.Info.Code + " " +
                                eventInformation.Info.Address + " " +
                                eventInformation.Info.Flags + "\n");
                    }
                    // XDevkit.IXboxStackFrame Context = eventInformation.Info.Thread.TopOfStack;
                    break;
                default:
                    Console.WriteLine(eventInformation.Info.Message + " event type " + eventCode);
                    break;
            }
        }
        #endregion // XBoxcallbackTest

        #region TimedEvent
        // Specify what you want to happen when the Elapsed event is raised.
        /// <summary>
        /// callback function when the time for testing expires
        /// </summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            bTimerExit = true;
        }
        #endregion // TimedEvent

        #region OnTTY Event
        /// <summary>
        /// callback function when TTY is received
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private static void OnTTY(Object sender, MachineInterface.cDebugLogTextEventArgs e)
        {    
           // commented out since writing to log file is only supported ( otherwise we duplicate errors that are for post filter script )
           // Console.WriteLine(e.Text);
        }
        #endregion // OnTTY Event

        #region GameInterfaces
        /// <summary>
        /// 
        /// </summary>
        public class GameInterfaceDevKit : iGame
        {
            #region Properties and Associated Member Data
            public String Name
            {
                get { return m_sName; }
            }
            private String m_sName;

            public String RootDirectory
            {
                get { return m_sRoot; }
            }
            private String m_sRoot;

            public float SectorSize
            {
                get { return m_fSectorSize; }
            }
            private float m_fSectorSize;

            public List<cConfiguration> Configurations
            {
                get { return m_Configurations; }
            }
            private List<cConfiguration> m_Configurations;

            public cConfiguration ActiveConfiguration
            {
                get { return m_ActiveConfiguration; }
                set { m_ActiveConfiguration = value; }
            }
            private cConfiguration m_ActiveConfiguration;
            #endregion

            #region Constructor
            public GameInterfaceDevKit(String name, String root, float fSectorSize, List<cConfiguration> configs)
            {
                m_sName = name;
                m_sRoot = root;
                m_fSectorSize = fSectorSize;
                m_Configurations = configs;
            }
            #endregion
        }
        #endregion // GameInterfaces

        #region ProcessArgs
        /// <summary>
        /// process the args passed to the application
        /// </summary>
        /// <param name="args">the args as received by the application entry point</param>
        /// <returns></returns>
        private static int ProcessArgs( string[] args)
        {
            // Parse args
            if (args.GetLength(0) != m_numExpectedArgs)
            {
                Console.WriteLine("{0} args read expected {1}\n", args.GetLength(0), m_numExpectedArgs);
                Console.WriteLine("Usage : DevKit platform directory executable commandline timeout(seconds)");
                Console.WriteLine("e.g. DevKit -platform 360 -workingdir X:\\jimmy\\build\\dev\\ -exe JimmyBeta_xenon.xex -commandline \"-level=1\" -duration 180 -deploydir xE:\\jimmy -reboot 0 -ip 10.11.25.4");
                foreach (string s in args)
                {
                    Console.WriteLine(s);
                }
                //Console.ReadKey();
                return -1;
            }
           
            for (int i = 0; i < args.Length; i++)
            {
                Console.WriteLine("{0}:{1}",args[i],args[i+1]);
                if (args[i].Equals("-platform", StringComparison.CurrentCultureIgnoreCase))
                {
                    m_platform = args[++i];
                }
                else if (args[i].Equals("-reboot", StringComparison.CurrentCultureIgnoreCase))
                {
                    m_reboot = int.Parse(args[++i]);
                }
                else if (args[i].Equals("-ip", StringComparison.CurrentCultureIgnoreCase))
                {
                    m_ipAddr = args[++i];
                    Console.WriteLine(m_ipAddr);
                }
                else if (args[i].Equals("-workingdir", StringComparison.CurrentCultureIgnoreCase))
                {
                    m_workingdir = args[++i];

                    // Standardise slashes
          //          m_workingdir = m_workingdir.Replace('/', '\\');
          //          if (!m_workingdir.EndsWith("\\")) m_workingdir += "\\";
                }
                else if (args[i].Equals("-deploydir", StringComparison.CurrentCultureIgnoreCase))
                {
                    m_deploydir = args[++i];
                }
                else if (args[i].Equals("-commandline", StringComparison.CurrentCultureIgnoreCase))
                {
                    m_commandline = args[++i];
                }
                else if (args[i].Equals("-exe", StringComparison.CurrentCultureIgnoreCase))
                {
                    m_exe = args[++i];
                }
                else if (args[i].Equals("-duration", StringComparison.CurrentCultureIgnoreCase))
                {
                    m_duration = int.Parse(args[++i]);
                }
                else
                {
                    Console.WriteLine("Argument unknown : " + args[i]);
                    //Console.ReadKey();
                    return -1;
                }
            }
            return 1;
        }
        #endregion // ProcessArgs

        #region Main
        /// <summary>
        /// main entry point
        /// </summary>
        /// <param name="args">command line args</param>
        /// <returns></returns>
        static int Main(string[] args)
        {
            Thread.CurrentThread.Name = "Main";
            try
            {
                if (ProcessArgs(args) < 0)
                {
                    return -1;
                }

                Process();

                Console.WriteLine("...End Wait... application is finished.");
            }
            catch (System.Exception e)
            {
                Console.WriteLine("Exception occurred : " + e.Message);
            }

           // Console.WriteLine("Press a key to exit DevKit");
           // Console.ReadKey();

            Console.WriteLine("...Application is quiting cleanly.");

            return 0;
        }
        #endregion // Main

        #region Timer
        /// <summary>
        /// Set up the timer for which it will call a callback when the timer expires.
        /// </summary>
        static void SetupTimer()
        {
            // Setup a timer to kill the application when required.
            System.Timers.Timer aTimer = new System.Timers.Timer();
            aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            aTimer.Interval = m_duration * 1000; // ms
            aTimer.Enabled = true;
        }
        #endregion // timer

        #region ConvertIP
        /// <summary>
        /// convert ip address from string to byte array
        /// </summary>
        static void ConvertIP(String ip, ref byte[] addr)
        {
            // quickly hacked in, sorry, probably some good .net code out there to already do this.
            if (ip != "")
            {
                String[] strArray = ip.Split('.');

                if (strArray.Length == 4)
                {
                    int i = 0;
                    foreach (String str in strArray)
                    {
                        addr[i] = byte.Parse(strArray[i]);
                        i++;
                    }
                }
            }
        }
        #endregion // timer


        #region Process
        /// <summary>
        /// Main process function. Chooses call to make for each platform.
        /// </summary>
        /// <returns></returns>
        static int Process()
        {
            Console.WriteLine("########################");
            Console.WriteLine("#### DEVKIT v{0}     ###",VERSION);
            Console.WriteLine("########################");
            Console.WriteLine("\n{0} will run {2}\\{1} from {5}\nwith command line {3} for {4} seconds on {5}\n", m_platform, m_exe, m_workingdir, m_commandline, m_duration, m_deploydir, m_ipAddr);

            SetupTimer();

            Console.WriteLine("Configuration setting");
            cConfiguration config = new cConfiguration(m_machineName, m_name, m_deploydir, m_exe, m_commandline);
            List<cConfiguration> list = new List<cConfiguration>();
            list.Add(config);
            // Create a new game interface so we can start the console with some parameters to launch it.               
            GameInterfaceDevKit gameInterface = new GameInterfaceDevKit(m_config_name, m_workingdir, 1.0f, list);

            int rc = -1;
            if (m_platform.Equals("360", StringComparison.CurrentCultureIgnoreCase) ||
                m_platform.Equals("xbox360", StringComparison.CurrentCultureIgnoreCase))
            {
                rc = Process360(gameInterface,config);
            }
            else if (m_platform.Equals("PS3", StringComparison.CurrentCultureIgnoreCase))
            {
                rc = ProcessPS3(gameInterface, config);
            }
            else
            {
                Console.Error.WriteLine("Platform {0} not recognised", m_platform);
            }

            return rc;
        }
        #endregion // Process

        #region Process360
        /// <summary>
        /// 360 specific call, handles safe rebooting and launches application.
        /// </summary>
        /// <param name="gameInterface"></param>
        /// <param name="config"></param>
        /// <returns></returns>
        static int Process360(GameInterfaceDevKit gameInterface, cConfiguration config)
        {
            // Create a machine object
            xboxManager = new XboxManagerClass();
            xboxConsole = xboxManager.OpenConsole(xboxTarget == "" ? xboxManager.DefaultConsole : xboxTarget);

            ConvertIP(m_ipAddr, ref m_addr_360);

            cXbox360 machine = new cXbox360(new IPAddress(m_addr_360)); // xboxConsole.IPAddress); // this method has proved to be unreliable!!! I don't know why!
            Console.WriteLine("XBox added : {0}", machine.IP.ToString());
            machine.DebugLog.DebugTextEvent += OnTTY;

            Console.WriteLine("Reboot initiated");           
            machine.Reboot();
            if (m_reboot==1) // if rebooting just exit now.
                return 1;

            Thread.Sleep(1000);
            while (machine.Status == MachineInterface.etMachineStatus.Rebooting ||
                  machine.Status == MachineInterface.etMachineStatus.Off)
            {
                Thread.Sleep(100);
            }
            Thread.Sleep(60000); // wait ten seconds so that the player will sign in?

            Console.WriteLine("Reboot complete");

            // Test the machine object is ready.
            long latency = machine.Ping();
            Console.WriteLine("Latency of Ping = {0}", latency);

            // Launch the machine object
            Console.WriteLine("Launching...");
            machine.Start(gameInterface, config, 1000);
            Console.WriteLine("...Launched");
            Console.WriteLine("Waiting...for {0} seconds", m_duration);
           
            while (!bTimerExit)
            {
                Thread.Sleep(10000);
            }

            Console.WriteLine("Reboot initiated");
            machine.Reboot();
            return 1;     
        }
        #endregion // Process360

        #region ProcessPS3
        /// <summary>
        /// PS3 specific call, handles safe rebooting and launches application.
        /// </summary>
        /// <param name="gameInterface"></param>
        /// <param name="config"></param>
        /// <returns></returns>
        static int ProcessPS3(GameInterfaceDevKit gameInterface, cConfiguration config)
        {
            ConvertIP(m_ipAddr, ref m_addr_PS3);

            cPS3 machine = new cPS3(new IPAddress(m_addr_PS3));
            machine.DebugLog.DebugTextEvent += OnTTY;
            Console.WriteLine("PS3 added : {0}", machine.IP.ToString());

            Console.WriteLine("Reboot initiated");
            machine.Disconnect();
            machine.RebootCold();
            if (m_reboot == 1) // if rebooting just exit now.
            {
                machine.Disconnect();
                return 0;
            }

            Thread.Sleep(1000);
            while (machine.Status == MachineInterface.etMachineStatus.Rebooting ||
                  machine.Status == MachineInterface.etMachineStatus.Off)
            {
                Thread.Sleep(100);
            }
            Thread.Sleep(1000);

            Console.WriteLine("Reboot complete");

            // Test the machine object is ready.
            long latency = machine.Ping();
            Console.WriteLine("Latency of Ping = {0}", latency);

            // Launch the machine object
            Console.WriteLine("Launching...");
            machine.Start(gameInterface, config, config.Arguments, 10);
            Console.WriteLine("...Launched");                    
            Console.WriteLine("Waiting...for {0} seconds",m_duration );

            while (!bTimerExit)
            {
                Thread.Sleep(10000);
            }


            try
            {
                Console.WriteLine("Disconnecting from machine");
                machine.Disconnect();
            }
            catch (Exception)
            {
                Console.WriteLine("an Exception has occurred but we happy to ignore it because debugging the managed c++ PS3 shite is a no-no");
            }

            Console.WriteLine("Reboot initiated");
            machine.RebootCold();

            Console.WriteLine("End processPS3");
            
            return 0;
        }
        #endregion // processPS3

        #region archive        
        /*
                        try
                        {
                            Console.WriteLine("Beginning DevKit application");


                            // console ip address passed in?
                            // need to reboot the console
                            // launch the XEX
                            // grab the TTY - flushing as we go.

                            xboxManager = new XboxManagerClass();
                            xboxConsole = xboxManager.OpenConsole(xboxTarget == "" ? xboxManager.DefaultConsole : xboxTarget);
                            xboxDebugTarget = xboxConsole.DebugTarget;

                            Console.WriteLine("Connected to Xbox IP address : {0}", xboxConsole.IPAddress);
                            Console.WriteLine("Connected to Xbox IP address Title : {0} ({1})", xboxConsole.IPAddressTitle, xboxConsole.Name);
                            Console.WriteLine("Console type is " + xboxConsole.ConsoleType.ToString());
                            Console.WriteLine("ConnectTimeout is " + xboxConsole.ConnectTimeout.ToString());
                            Console.WriteLine("ConversationTimeout is " + xboxConsole.ConversationTimeout.ToString());
                            Console.WriteLine("DebugTarget name is " + xboxConsole.DebugTarget.Name);
                            Console.WriteLine("RunningProcessInfo ProgramName is " + xboxConsole.RunningProcessInfo.ProgramName);
                            Console.WriteLine("RunningProcessInfo ProcessId is " + xboxConsole.RunningProcessInfo.ProcessId.ToString());
                            Console.WriteLine("SystemTime is " + xboxConsole.SystemTime.ToString());
                            Console.WriteLine("Drives are " + xboxConsole.Drives);
                            //ulong freeBytes, totalBytes, totalFreeBytes;
                            //xboxConsole.GetDiskFreeSpace(1, out freeBytes, out totalBytes,out totalFreeBytes);
                            //Console.WriteLine("Drive[0] disk space is {0}/{1} : {2}" + freeBytes,totalBytes,totalFreeBytes);
                            bool alreadyStopped;
                            xboxDebugTarget.Stop(out alreadyStopped);
                            bool notStopped;
                            xboxDebugTarget.Go(out notStopped);
                            xboxConsole.OnStdNotify += new XboxEvents_OnStdNotifyEventHandler(xboxConsole_OnStdNotify);
                            xboxDebugTarget.ConnectAsDebugger(null, XboxDebugConnectFlags.MonitorOnly);
                            xboxConsole.OnStdNotify -= new XboxEvents_OnStdNotifyEventHandler(xboxConsole_OnStdNotify);
                        }
                        catch (System.Exception e)
                        {
                            Console.WriteLine("Exception occurred : " + e.Message);
                        }
            */
        #endregion //archive
    }
}

 