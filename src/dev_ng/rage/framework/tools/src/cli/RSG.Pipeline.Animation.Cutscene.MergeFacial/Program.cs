﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.Animation.Cutscene.MergeFacial
{
    class Program
    {
        #region Constants
        private static readonly String OPTION_INPUTDIR = "inputdir";
        private static readonly String OPTION_OUTPUTDIR = "outputdir";
        private static readonly String OPTION_CUTFILE = "cutfile";
        private static readonly String OPTION_CUTANIMCOMBINE = "cutfanimcombine";
        private static readonly String OPTION_ANIMCOMBINE = "animcombine";
        private static readonly String OPTION_ANIMEDIT = "animedit";
        private static readonly String OPTION_CLIPEDIT = "clipedit";
        private static readonly String OPTION_TASK_NAME = "taskname";
        private static readonly String LOG_CTX = "Merge Facial";
        #endregion // Constants

        private static IUniversalLog log = null;

        static int RunProcess(string strExe, string strArgs)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.Arguments = strArgs;
            startInfo.FileName = strExe;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.CreateNoWindow = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false;

            log.MessageCtx(LOG_CTX, strExe + " " + strArgs);

            Process proc = Process.Start(startInfo);
            String output = proc.StandardOutput.ReadToEnd();
            proc.WaitForExit();

            if (output != String.Empty)
                log.MessageCtx(LOG_CTX, output);

            return proc.ExitCode;
        }

        static void SubstEnv(List<string> alArray, string inputDir, string outputDir)
        {
            for (int i = 0; i < alArray.Count; ++i)
            {
                alArray[i] = alArray[i].Replace("$(inputFaceDir)", Path.Combine(inputDir, "faces"));
                alArray[i] = alArray[i].Replace("$(inputDir)", inputDir);
                alArray[i] = alArray[i].Replace("$(intermediateDir)", outputDir);
                alArray[i] = alArray[i].Replace("$(outputDir)", outputDir);
            }
        }

        static bool RemoveTracks(string animEditExe, string inputDir, string outputDir)
        {
            try
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(Path.Combine(outputDir, "anim_combine.targets"));

                List<string> alInputAnimArray = new List<string>();
                List<string> alOutputAnimArray = new List<string>();
                List<string> alRemoveAnimTrackArray = new List<string>();

                System.Xml.XmlNamespaceManager xmlnsManager = new System.Xml.XmlNamespaceManager(xmlDoc.NameTable);
                xmlnsManager.AddNamespace("ns", xmlDoc.DocumentElement.NamespaceURI);

                XmlNodeList removeItemNodes = xmlDoc.SelectNodes("/ns:Project/ns:ItemGroup/ns:RemoveAnimTracksItem", xmlnsManager);
                XmlNodeList removeOutputFileNodes = xmlDoc.SelectNodes("/ns:Project/ns:ItemGroup/ns:RemoveAnimTracksItem/ns:outputFile", xmlnsManager);
                XmlNodeList removeTracksFileNodes = xmlDoc.SelectNodes("/ns:Project/ns:ItemGroup/ns:RemoveAnimTracksItem/ns:removeTracks", xmlnsManager);
                foreach (XmlNode n in removeItemNodes) alInputAnimArray.Add(n.Attributes["Include"].InnerText);
                foreach (XmlNode n in removeOutputFileNodes) alOutputAnimArray.Add(n.InnerText);
                foreach (XmlNode n in removeTracksFileNodes) alRemoveAnimTrackArray.Add(n.InnerText);

                Debug.Assert(alInputAnimArray.Count == alOutputAnimArray.Count && alOutputAnimArray.Count == alRemoveAnimTrackArray.Count, "Array counts are not equal");

                SubstEnv(alInputAnimArray, inputDir, outputDir);
                SubstEnv(alOutputAnimArray, inputDir, outputDir);
                SubstEnv(alRemoveAnimTrackArray, inputDir, outputDir);

                for (int i = 0; i < alInputAnimArray.Count; ++i)
                {
                    if (!File.Exists(alInputAnimArray[i]))
                    {
                        log.ErrorCtx(LOG_CTX, "File '{0}' does not exist.", alInputAnimArray[i]);
                        return false;
                    }
                }

                for (int i = 0; i < alInputAnimArray.Count; ++i)
                {
                    String animEditArgs = String.Empty;

                    if (alRemoveAnimTrackArray[i] == String.Empty)
                    {
                        animEditArgs = String.Format("-anim \"{0}\" -out \"{1}\" -nopopups", alInputAnimArray[i], alOutputAnimArray[i]);

                        if (RunProcess(animEditExe, animEditArgs) != 0) return false;
                    }
                    else
                    {
                        string[] arrayRemoveAnimTracks = alRemoveAnimTrackArray[i].Split(new char[] { ';'}, StringSplitOptions.RemoveEmptyEntries);

                        if (arrayRemoveAnimTracks.Length > 1)
                        {
                            string[] firstArray = arrayRemoveAnimTracks.ToArray().Take(arrayRemoveAnimTracks.Length / 2).ToArray();
                            string[] secondArray = arrayRemoveAnimTracks.Skip(arrayRemoveAnimTracks.Length / 2).ToArray();

                            animEditArgs = String.Format("-anim \"{0}\" -out \"{1}\" -op {2} -nopopups", alInputAnimArray[i], alOutputAnimArray[i], String.Join(";", firstArray));

                            if (RunProcess(animEditExe, animEditArgs) != 0) return false;

                            animEditArgs = String.Format("-anim \"{0}\" -out \"{1}\" -op {2} -nopopups", alOutputAnimArray[i], alOutputAnimArray[i], String.Join(";", secondArray));

                            if (RunProcess(animEditExe, animEditArgs) != 0) return false;
                        }
                        else
                        {
                            animEditArgs = String.Format("-anim \"{0}\" -out \"{1}\" -op {2} -nopopups", alInputAnimArray[i], alOutputAnimArray[i], String.Join(";", arrayRemoveAnimTracks));

                            if (RunProcess(animEditExe, animEditArgs) != 0) return false;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Unhandled Exception during Merge Facial process in RemoveTracks function");
                return false;
            }

            return true;
        }

        static bool CombineTracks(string animCombineExe, string clipEditExe, string inputDir, string outputDir)
        {
            try
            {
                var xmlDoc = new XmlDocument();
                xmlDoc.Load(Path.Combine(outputDir, "anim_combine.targets"));

                List<string> alInputAnimArray = new List<string>();
                List<string> alOutputAnimArray = new List<string>();
                List<string> alOutputClipArray = new List<string>();
                List<string> alFaceAnimArray = new List<string>();
                List<string> alClipFileArray = new List<string>();

                System.Xml.XmlNamespaceManager xmlnsManager = new System.Xml.XmlNamespaceManager(xmlDoc.NameTable);
                xmlnsManager.AddNamespace("ns", xmlDoc.DocumentElement.NamespaceURI);

                XmlNodeList combineItemNodes = xmlDoc.SelectNodes("/ns:Project/ns:ItemGroup/ns:CombineAnimsItem", xmlnsManager);
                XmlNodeList clipFileNodes = xmlDoc.SelectNodes("/ns:Project/ns:ItemGroup/ns:CombineAnimsItem/ns:clipFile", xmlnsManager);
                XmlNodeList faceAnimFileNodes = xmlDoc.SelectNodes("/ns:Project/ns:ItemGroup/ns:CombineAnimsItem/ns:faceAnimFile", xmlnsManager);
                XmlNodeList outputAnimFileNodes = xmlDoc.SelectNodes("/ns:Project/ns:ItemGroup/ns:CombineAnimsItem/ns:outputAnimFile", xmlnsManager);
                XmlNodeList outputClipFileNodes = xmlDoc.SelectNodes("/ns:Project/ns:ItemGroup/ns:CombineAnimsItem/ns:outputClipFile", xmlnsManager);
                foreach (XmlNode n in combineItemNodes) alInputAnimArray.Add(n.Attributes["Include"].InnerText);
                foreach (XmlNode n in clipFileNodes) alClipFileArray.Add(n.InnerText);
                foreach (XmlNode n in faceAnimFileNodes) alFaceAnimArray.Add(n.InnerText);
                foreach (XmlNode n in outputAnimFileNodes) alOutputAnimArray.Add(n.InnerText);
                foreach (XmlNode n in outputClipFileNodes) alOutputClipArray.Add(n.InnerText);

                SubstEnv(alInputAnimArray, inputDir, outputDir);
                SubstEnv(alOutputClipArray, inputDir, outputDir);
                SubstEnv(alFaceAnimArray, inputDir, outputDir);
                SubstEnv(alClipFileArray, inputDir, outputDir);

                string[] animFiles = Directory.GetFiles(inputDir, "*.anim");
                string[] clipFiles = Directory.GetFiles(inputDir, "*.clip");

                foreach (string file in animFiles) File.Copy(file, Path.Combine(outputDir, Path.GetFileName(file)), true);
                foreach (string file in clipFiles) File.Copy(file, Path.Combine(outputDir, Path.GetFileName(file)), true);

                for (int i = 0; i < alInputAnimArray.Count; ++i)
                {
                    if (!File.Exists(alInputAnimArray[i]))
                    {
                        log.ErrorCtx(LOG_CTX, "File '{0}' does not exist.", alInputAnimArray[i]);
                        return false;
                    }
                }

                for (int i = 0; i < alInputAnimArray.Count; ++i)
                {
                    String animCombineArgs = String.Format("-anims \"{0},{1}\" -out \"{2}\" -merge -sync -nocompression -nopopups", alInputAnimArray[i], alFaceAnimArray[i], Path.Combine(outputDir, alOutputAnimArray[i]));

                    if (RunProcess(animCombineExe, animCombineArgs) != 0) return false;
                }

                for (int i = 0; i < alInputAnimArray.Count; ++i)
                {
                    String clipEditArgs = String.Format("-clip \"{0}\" -out \"{1}\" -clipanimation \",,,{2}\" -path \"{3}\" -nopopups", alClipFileArray[i], alOutputClipArray[i], alOutputAnimArray[i], outputDir);

                    if (RunProcess(clipEditExe, clipEditArgs) != 0) return false;
                }
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Unhandled Exception during Merge Facial process in CombineTracks function");
                return false;
            }

            return true;
        }

        [STAThread]
        static int Main(string[] args)
        {
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            log = LogFactory.CreateUniversalLog("MergeFacial");
            try
            {
                // Define our executable options.  Stick with the long options
                // (e.g. --opt) rather than the single character versions.
                LongOption[] opts = new LongOption[] {
                new LongOption(OPTION_INPUTDIR, LongOption.ArgType.Required,
                    "Input directory."),
                new LongOption(OPTION_OUTPUTDIR, LongOption.ArgType.Required,
                    "Output directory."),
                new LongOption(OPTION_CUTFILE, LongOption.ArgType.Required,
                    ""),
                new LongOption(OPTION_CUTANIMCOMBINE, LongOption.ArgType.Required,
                    "cutanimcombine filename."),
                new LongOption(OPTION_ANIMCOMBINE, LongOption.ArgType.Required,
                    "animcombine filename."),
                new LongOption(OPTION_ANIMEDIT, LongOption.ArgType.Required,
                    "animedit filename."),
                new LongOption(OPTION_CLIPEDIT, LongOption.ArgType.Required,
                    "clipedit filename."),
                new LongOption(OPTION_TASK_NAME, LongOption.ArgType.Required,
                    "Asset Pipeline 3 Task Name."),
                };

                CommandOptions options = new CommandOptions(args, opts);

                // Turn args into vars
                String inputDir = options[OPTION_INPUTDIR] as String;
                String outputDir = options[OPTION_OUTPUTDIR] as String;
                String cutFile = options[OPTION_CUTFILE] as String;
                String cutfAnimCombineExe = options[OPTION_CUTANIMCOMBINE] as String;
                String animCombineExe = options[OPTION_ANIMCOMBINE] as String;
                String animEditExe = options[OPTION_ANIMEDIT] as String;
                String clipEditExe = options[OPTION_CLIPEDIT] as String;
                String taskName = options[OPTION_TASK_NAME] == null ? String.Empty : options[OPTION_TASK_NAME] as String;

                if (String.Empty != taskName)
                {
                    Console.WriteLine("TASK: {0}", taskName);
                    log.MessageCtx(LOG_CTX, "TASK: {0}", taskName);
                }

                if (!Directory.Exists(inputDir))
                {
                    log.ErrorCtx(LOG_CTX, "Directory '{0}' does not exist", inputDir);
                    return RSG.Pipeline.Core.Constants.Exit_Failure;
                }

                //log.MessageCtx(LOG_CTX, "Creating directory '{0}'", outputDir);
                //if (!RSG.Pipeline.Services.Animation.DirectoryServices.CreateDirectory(outputDir))
                //{
                //    log.ErrorCtx(LOG_CTX, "Failed to create directory: '{0}'.  Aborting.", outputDir);
                //    return (Constants.Exit_Failure);
                //}

                // Generate the anim_combine.targets file using cutfanimcombine
                String cutfAnimCombineArgs = String.Format("-inputDir={0} -intermediateDir={1} -assetsDir={2} -cutsceneFile={3} -hideAlloc -nopopups",
                                                           inputDir, outputDir, options.Branch.Assets, cutFile);
                if (RunProcess(cutfAnimCombineExe, cutfAnimCombineArgs) != 0)
                {
                    if (Directory.Exists(outputDir))
                        Directory.Delete(outputDir, true);
                    return RSG.Pipeline.Core.Constants.Exit_Failure;
                }

                // Parse the anim_combine.targets file for the data we need
                if (!RemoveTracks(animEditExe, inputDir, outputDir))
                {
                    if (Directory.Exists(outputDir))
                        Directory.Delete(outputDir, true);
                    return RSG.Pipeline.Core.Constants.Exit_Failure;
                }

                // Parse the anim_combine.targets file for the data we need
                if (!CombineTracks(animCombineExe, clipEditExe, inputDir, outputDir))
                {
                    if (Directory.Exists(outputDir))
                        Directory.Delete(outputDir, true);
                    return RSG.Pipeline.Core.Constants.Exit_Failure;
                }

                return RSG.Pipeline.Core.Constants.Exit_Success;
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Unhandled Exception during Merge Facial Process");
                return RSG.Pipeline.Core.Constants.Exit_Failure;
            }
            finally
            {
                LogFactory.ApplicationShutdown();
            }
        }
    }
}
