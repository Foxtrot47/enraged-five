﻿namespace SanScriptProjectConverter
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml;

    using RSG.Base.Configuration;
    using RSG.Base.OS;

    class Program
    {
        private const string OPTION_INPUT = "input";
        private const string OPTION_OUTPUTDIR = "outputdir";

        private static string Input
        {
            get;
            set;
        }

        private static string OutputDirectory
        {
            get;
            set;
        }

        private enum Error
        {
            INVALID_ARGUMENTS,
            MISSING_INPUT_FILE,
            INVALID_INPUT,
            UNAUTHORISED_ACCESS,
            PARSE_EXCEPTION,
        };

        static void Main(string[] args)
        {
            LongOption[] opts = new LongOption[] {
                new LongOption(OPTION_INPUT, LongOption.ArgType.Optional,
                    "Specify input filename."), 
                new LongOption(OPTION_OUTPUTDIR, LongOption.ArgType.Optional,
                    "Specify output directory."), 
            };

            CommandOptions options = new CommandOptions(args, opts);

            Console.Clear();

            Console.WriteLine("************************************************************");
            Console.WriteLine("** Convert an old '.scproj' SanScript Editor project to a **");
            Console.WriteLine("** new '.ssproj' Visual Studio SanScript project.         **");
            Console.WriteLine("************************************************************");

            try
            {
                ParseCommandLine(options);
            }
            catch (ArgumentException ex)
            {
                HandleError(Error.INVALID_ARGUMENTS , ex);
            }

            if (!Input.EndsWith(".scproj"))
            {
                HandleError(Error.INVALID_INPUT, null);
            }

            if (!File.Exists(Input))
            {
                HandleError(Error.MISSING_INPUT_FILE, null);
            }

            Directory.CreateDirectory(OutputDirectory);

            string output = Path.Combine(OutputDirectory, Path.GetFileName(Input));
            output = Path.ChangeExtension(output, ".ssproj");

            if (File.Exists(output))
            {
                bool isReadOnly = ((File.GetAttributes(output) & FileAttributes.ReadOnly) == FileAttributes.ReadOnly);
                if (isReadOnly)
                {
                    HandleError(Error.UNAUTHORISED_ACCESS, null);
                }
            }

            Console.WriteLine("***** Reading original project file.");
            Project project = null;
            string currentDirectory = Directory.GetCurrentDirectory();
            try
            {
                Directory.SetCurrentDirectory(Path.GetDirectoryName(Input));
                using (XmlReader reader = XmlReader.Create(Input))
                {
                    project = new Project(reader, output, options.Branch);
                }
            }
            catch (Exception ex)
            {
                HandleError(Error.PARSE_EXCEPTION, ex);
            }
            finally
            {
                Directory.SetCurrentDirectory(currentDirectory);
            }

            Console.WriteLine(
                "***** Read {0} files and {1} folders.\n",
                project.Items.Count,
                project.Folders.Count);

            Console.WriteLine("***** Writing out new Visual Studio project file.");
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.Encoding = Encoding.UTF8;
            using (XmlWriter writer = XmlWriter.Create(output, settings))
            {
                writer.WriteStartDocument();
                project.Serialise(writer);
                writer.WriteEndDocument();
            }

            List<ProjectItem> invalid = (from item in project.Items where !item.IsValid select item).ToList();
            if (invalid.Count > 0)
            {
                Console.WriteLine("The following '{0}' files couldn't be converted as the relative path couldn't be created.", invalid.Count);
                foreach (ProjectItem item in invalid)
                {
                    Console.WriteLine("{0}", item.FullPath);
                }

                Console.WriteLine("");
            }

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey(true);
            Environment.Exit(0);
        }

        /// <summary>
        /// Prints an error to the command window and exits after a pause
        /// </summary>
        /// <param name="error">
        /// The error to report
        /// </param>
        private static void HandleError(Error error, Exception ex)
        {
            switch (error)
            {
                case Error.MISSING_INPUT_FILE:
                    Console.WriteLine("Error: Missing input file '{0}'.", Input);

                    break;
                case Error.INVALID_ARGUMENTS:
                    Console.WriteLine(
                        "Error: Invalid commandline arguments. {0}", ex.Message);

                    break;
                case Error.INVALID_INPUT:
                    Console.WriteLine(
                        "Error: Invalid input file type, please specify a '.scproj' file.\n");

                    break;
                case Error.UNAUTHORISED_ACCESS:
                    Console.WriteLine(
                        "Error: Unauthorised access when writing '.ssproj' output file.");
                    Console.WriteLine("Do you have the file checked out in Perforce?\n");

                    break;
                case Error.PARSE_EXCEPTION:
                    Console.WriteLine(
                        "Error: Exception caught while parsing the old '.scproj' file.\n");
                    Console.WriteLine("Exception: {0}", ex.Message);

                    break;
            }

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey(true);
            Environment.Exit(-1);
        }

        /// <summary>
        /// Parse specified command line arguments and populate Options dict.
        /// </summary>	
        /// <exception cref="System.ArgumentException"/>
        private static void ParseCommandLine(CommandOptions options)
        {
            //File dragged onto .exe
            if (!options.ContainsOption(OPTION_INPUT) && !options.ContainsOption(OPTION_OUTPUTDIR))
            {
                string input = options.TrailingArguments.FirstOrDefault();
                if (String.IsNullOrWhiteSpace(input))
                    throw new ArgumentException("Unable to find a valid project to convert.");

                Input = input;
                OutputDirectory = Path.GetDirectoryName(input);
            }
            else
            {
                Input = (String) options[OPTION_INPUT];
                OutputDirectory = (String) options[OPTION_OUTPUTDIR];         
            } 
        }
    }
}
