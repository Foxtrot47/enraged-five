﻿

namespace SanScriptProjectConverter
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml;
    using RSG.Base.Configuration;

    /// <summary>
    /// 
    /// </summary>
    internal class Project
    {
        #region Fields
        /// <summary>
        /// The private field used for substituting paths on a per-branch basis.
        /// </summary>
        private IBranch _branch;
        /// <summary>
        /// The private field used for the <see cref="Configurations"/> property.
        /// </summary>
        private List<ProjectConfiguration> _configurations;

        /// <summary>
        /// The private field used for the <see cref="Items"/> property.
        /// </summary>
        private List<ProjectItem> _items;

        /// <summary>
        /// The private field used for the <see cref="Folders"/> property.
        /// </summary>
        private List<string> _folders;

        /// <summary>
        /// 
        /// </summary>
        private int _currentSettingIndex;

        private string _name;

        private string _filename;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="Project"/> class.
        /// </summary>
        /// <param name="reader">
        /// The reader that contains the data to use to initialise this instance.
        /// </param>
        public Project(XmlReader reader, string newFilename, IBranch branch)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this._branch = branch;
            this._configurations = new List<ProjectConfiguration>();
            this._items = new List<ProjectItem>();
            this._folders = new List<string>();
            this._name = Path.GetFileNameWithoutExtension(reader.BaseURI);
            this._filename = newFilename;
            this.Deserialise(reader);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the collection of project configurations that are defined for this project.
        /// </summary>
        public List<ProjectConfiguration> Configurations
        {
            get { return this._configurations; }
        }

        /// <summary>
        /// Gets the collection of items defined in this project.
        /// </summary>
        public List<ProjectItem> Items
        {
            get { return this._items; }
        }

        /// <summary>
        /// Gets the collection of folders defined in this project.
        /// </summary>
        public List<string> Folders
        {
            get { return this._folders; }
        }

        /// <summary>
        /// Gets the new filename that this project has. (The path that the new project items
        /// should be relative to).
        /// </summary>
        public string Filename
        {
            get { return this._filename; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        public void Serialise(XmlWriter writer)
        {
            writer.WriteStartElement("Project", "http://schemas.microsoft.com/developer/msbuild/2003");
            writer.WriteAttributeString("DefaultTargets", "Build");
            writer.WriteAttributeString("ToolsVersion", "4.0");

            this.SerialiseMasterConfig(writer);
            this.SerialiseSanScriptPath(writer);
            this.SerialiseConfigurations(writer);
            this.SerialiseFolders(writer);
            this.SerialiseItems(writer);

            writer.WriteStartElement("Import");
            writer.WriteAttributeString("Project", "$(SanScriptPath)\\SanScript.targets");
            writer.WriteEndElement();

            writer.WriteEndElement();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        private void SerialiseMasterConfig(XmlWriter writer)
        {
            writer.WriteStartElement("PropertyGroup");

            writer.WriteStartElement("Configuration");
            writer.WriteAttributeString("Condition", " '$(Configuration)' == '' ");
            writer.WriteString(this._configurations[this._currentSettingIndex].Name);
            writer.WriteEndElement();

            writer.WriteElementString("SchemaVersion", "2.0");
            writer.WriteElementString("ProjectGuid", Guid.NewGuid().ToString());
            writer.WriteElementString("OutputType", "Library");
            writer.WriteElementString("RootNamespace", this._name);
            writer.WriteElementString("AssemblyName", this._name);
            writer.WriteElementString("EnableUnmanagedDebugging", "false");
            writer.WriteElementString("Name", this._name);

            writer.WriteEndElement();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        private void SerialiseSanScriptPath(XmlWriter writer)
        {
            writer.WriteStartElement("PropertyGroup");

            string final = @"$(LocalAppData)\Microsoft\VisualStudio\11.0\Extensions\Rockstar Games\SanScript\1.0\SanScript.targets";
            string exp = @"$(LocalAppData)\Microsoft\VisualStudio\11.0Exp\Extensions\Rockstar Games\SanScript\1.0\SanScript.targets";
            string conditional = " '$(SanScriptPath)' == '' AND Exists('{0}')";

            writer.WriteStartElement("SanScriptPath");
            writer.WriteAttributeString("Condition", String.Format(conditional, final));
            writer.WriteString(Path.GetDirectoryName(final));
            writer.WriteEndElement();

            writer.WriteStartElement("SanScriptPath");
            writer.WriteAttributeString("Condition", String.Format(conditional, exp));
            writer.WriteString(Path.GetDirectoryName(exp));
            writer.WriteEndElement();

            writer.WriteEndElement();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        private void SerialiseConfigurations(XmlWriter writer)
        {
            string conditional = " '$(Configuration)' == '{0}' ";
            foreach (ProjectConfiguration config in this._configurations)
            {
                writer.WriteStartElement("PropertyGroup");
                writer.WriteAttributeString("Condition", String.Format(conditional, config.Name));

                string fullOutputDirectory = Path.GetFullPath(config.OutputDirectory);
                fullOutputDirectory = this._branch.Environment.Subst(fullOutputDirectory);

                Uri outputDirectory = new Uri(fullOutputDirectory);
                Uri project = new Uri(this._filename);
                Uri relative = project.MakeRelativeUri(outputDirectory);

                writer.WriteElementString("OutputPath", relative.OriginalString.Replace("/", "\\"));
                writer.WriteElementString("IncludeFile", config.IncludeFile);
                writer.WriteElementString("IncludeDirectories", string.Join(";", config.IncludePaths));
                writer.WriteElementString("CompilerExecutable", config.CompilerExecutable);

                writer.WriteElementString("DebugParser", config.DebugParser.ToString());
                writer.WriteElementString("DumpThreadState", config.DumpThreadState.ToString());
                writer.WriteElementString("DisplayDisassembly", config.DisplayDisassembly.ToString());
                writer.WriteElementString("EnableHsm", config.HsmEnabled.ToString());
                writer.WriteElementString("TreatWarningsAsErrors", config.WarningsAsErrors.ToString());
                //writer.WriteElementString("CompileToConfigurationFolder", config.);
                writer.WriteElementString("CustomArguments", config.Custom);
                writer.WriteElementString("CreateDependencyFile", config.OutputDependencyFile.ToString());
                writer.WriteElementString("CreateNativeFile", config.OutputNativeFile.ToString());

                /*if (!string.IsNullOrEmpty(config.PostCompileCommand))
                {
                    writer.WriteElementString("PostBuildCommandLine", config.PostCompileCommand.ToString());
                    writer.WriteElementString("UsePostBuildStep", "True");
                }*/

                // Force the new post build command
                writer.WriteElementString("PostBuildCommandLine"
                    , String.Format("X:\\gta5\\tools_ng\\ironlib\\lib\\RSG.Pipeline.Convert.exe --no-default-resolver {0}", fullOutputDirectory));
                writer.WriteElementString("UsePostBuildStep", "True");

                writer.WriteEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        private void SerialiseFolders(XmlWriter writer)
        {
            writer.WriteStartElement("ItemGroup");

            var folders = (from item in this._items
                           where item.IsValid
                           from folder in item.Folders
                           select folder).Distinct();

            foreach (string folder in from folder in folders orderby folder select folder)
            {
                writer.WriteStartElement("Folder");
                writer.WriteAttributeString("Include", folder);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        private void SerialiseItems(XmlWriter writer)
        {
            foreach (ProjectItem item in this._items)
            {
                if (!item.IsValid)
                {
                    continue;
                }

                string startElement = "Content";
                string subType = "Content";

                if(item.RelativePath.EndsWith(".sc") || item.RelativePath.EndsWith(".sch"))
                {
                    startElement = "Compile";
                    subType = "Code";
                }

                writer.WriteStartElement("ItemGroup");
                writer.WriteStartElement(startElement);
                writer.WriteAttributeString("Include", item.RelativePath);

                writer.WriteElementString("SubType", subType);

                if (item.IsLink)
                {
                    writer.WriteElementString("Link", item.LinkPath);
                }

                writer.WriteEndElement();
                writer.WriteEndElement();
            }
        }

        /// <summary>
        /// Reads the data in the specified reader to initialise this instance.
        /// </summary>
        /// <param name="reader">
        /// The reader that contains the data to use to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader)
        {
            while (reader.MoveToContent() != XmlNodeType.None)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (String.Equals(reader.Name, "ProjectExplorerItem"))
                {
                    string type = reader.GetAttribute("xsi:type");
                    if (String.Equals(type, "ProjectExplorerFolder"))
                    {
                        this.DeserialiseFolder(reader, "");
                    }
                    else
                    {
                        this.DeserialiseFile(reader, "");
                    }
                }
                else if (String.Equals(reader.Name, "CurrentCompilingSettingsIndex"))
                {
                    this._currentSettingIndex = reader.ReadElementContentAsInt();
                }
                else if (String.Equals(reader.Name, "CompilingSettings"))
                {
                    this._configurations.Add(new ProjectConfiguration(reader, this._branch));
                }
                else
                {
                    reader.Read();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader">
        /// 
        /// </param>
        private void DeserialiseFile(XmlReader reader, string folderName)
        {
            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (String.Equals(reader.Name, "Name"))
                {
                    string filename = reader.ReadElementContentAsString();
                    filename = this._branch.Environment.Subst(filename);
                    this._items.Add(new ProjectItem(filename, folderName, this));
                    reader.Skip();
                }
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader">
        /// 
        /// </param>
        /// <param name="currentFolder">
        /// 
        /// </param>
        private void DeserialiseFolder(XmlReader reader, string currentFolder)
        {
            string folderName = currentFolder;
            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (String.Equals(reader.Name, "Name"))
                {
                    folderName = Path.Combine(folderName, reader.ReadElementContentAsString());
                    this._folders.Add(folderName);
                    reader.Skip();
                }
                else if (String.Equals(reader.Name, "Items"))
                {
                    if (!reader.IsEmptyElement)
                    {
                        while (reader.MoveToContent() != XmlNodeType.EndElement)
                        {
                            if (String.Equals(reader.Name, "ProjectExplorerItem"))
                            {
                                string type = reader.GetAttribute("xsi:type");
                                if (String.Equals(type, "ProjectExplorerFolder"))
                                {
                                    DeserialiseFolder(reader, folderName);
                                }
                                else
                                {
                                    DeserialiseFile(reader, folderName);
                                }
                            }
                            else
                            {
                                reader.Read();
                            }
                        }
                    }

                    reader.Read();
                }
                else
                {
                    reader.Read();
                }
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }
        #endregion Methods
    } // SanScriptProjectConverter.Project {Class}
} // SanScriptProjectConverter {Namespace}
