﻿namespace SanScriptProjectConverter
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using RSG.Interop.Microsoft.Windows;

    /// <summary>
    /// Defines a single project item that has a disk name and a folder name.
    /// </summary>
    internal class ProjectItem
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="Filename"/> property.
        /// </summary>
        private string _filename;

        /// <summary>
        /// The private field used for the <see cref="Folder"/> property.
        /// </summary>
        private string _folder;

        /// <summary>
        /// The private field used for the <see cref="Folders"/> property.
        /// </summary>
        private List<string> _folders;

        /// <summary>
        /// The private field used for the <see cref="FullPath"/> property.
        /// </summary>
        private string _fullPath;

        /// <summary>
        /// The private field used for the <see cref="RelativePath"/> property.
        /// </summary>
        private string _relativePath;

        /// <summary>
        /// The private field used for the <see cref="IsLink"/> property.
        /// </summary>
        private bool _isLink;

        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectItem"/> class.
        /// </summary>
        /// <param name="filename">
        /// The filename of the item on disk.
        /// </param>
        public ProjectItem(string filename, string folderName, Project project)
        {
            if (filename.Contains("\\build\\"))
            {

            }

            this._isLink = false;

            this._folders = new List<string>();
            this._filename = System.IO.Path.GetFileName(filename);
            this._fullPath = Path.GetFullPath(filename);
            string directory = Path.GetDirectoryName(project.Filename);
            string path = this._fullPath;

            this._relativePath = Shlwapi.MakeRelativeFromDirectoryToFile(directory, this._fullPath);
            this._folder = Path.GetDirectoryName(this._relativePath);

            string[] folders = this._folder.Split('\\');
            for (int i = 0; i < folders.Length; i++)
            {
                this._folders.Add(string.Join("\\", folders.Take(i + 1)));
            }

            if(this._folders.Contains(".."))
            {
                this._isLink = true;
                this._folder = folderName;
                folders = this._folder.Split('\\');
                this._folders.Clear();
                for (int i = 0; i < folders.Length; i++)
                {
                    this._folders.Add(string.Join("\\", folders.Take(i + 1)));
                }
            }

        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the filename of the item on disk.
        /// </summary>
        public string Filename
        {
            get { return this._filename; }
        }

        /// <summary>
        /// Gets the project folder this item appears in.
        /// </summary>
        public string Folder
        {
            get { return this._folder; }
        }

        /// <summary>
        /// Gets all of the folders that make up this item.
        /// </summary>
        public List<string> Folders
        {
            get { return this._folders; }
        }

        /// <summary>
        /// Gets the full path on disk for this item.
        /// </summary>
        public string FullPath
        {
            get { return this._fullPath; }
        }

        /// <summary>
        /// Gets the full path on disk for this item.
        /// </summary>
        public string RelativePath
        {
            get { return this._relativePath; }
        }

        /// <summary>
        /// Gets a value indicating whether this item is valid to be serialised out to the
        /// project file.
        /// </summary>
        public bool IsValid
        {
            get { return !this._folders.Contains(".."); }
        }

        /// <summary>
        /// Gets a value indicating wether this items disk location is outside the project folder hierarchy.
        /// </summary>
        public bool IsLink
        {
            get { return this._isLink; }
        }

        /// <summary>
        /// Link path
        /// </summary>
        public string LinkPath
        {
            get { return System.IO.Path.Combine(Folder, Filename); }
        }

        #endregion Properties
    } // SanScriptProjectConverter.ProjectItem {Class}
} // SanScriptProjectConverter {Namespace}
