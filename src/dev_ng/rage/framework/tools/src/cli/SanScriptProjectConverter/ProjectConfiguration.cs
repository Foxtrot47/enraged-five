﻿namespace SanScriptProjectConverter
{
    using System;
    using System.Collections.Generic;
    using System.Xml;
    using RSG.Base.Configuration;

    /// <summary>
    /// 
    /// </summary>
    internal class ProjectConfiguration
    {
        #region Fields
        /// <summary>
        /// The private field used for the <see cref="CompilerExecutable"/> property.
        /// </summary>
        private string _compilerExecutable;

        /// <summary>
        /// The private field used for the <see cref="Name"/> property.
        /// </summary>
        private string _name;

        /// <summary>
        /// The private field used for the <see cref="Custom"/> property.
        /// </summary>
        private string _custom;

        /// <summary>
        /// The private field used for the <see cref="DebugParser"/> property.
        /// </summary>
        private bool _debugParser;

        /// <summary>
        /// The private field used for the <see cref="DependencyFile"/> property.
        /// </summary>
        private string _dependencyFile;

        /// <summary>
        /// The private field used for the <see cref="DisplayDisassembly"/> property.
        /// </summary>
        private bool _displayDisassembly;

        /// <summary>
        /// The private field used for the <see cref="DumpThreadState"/> property.
        /// </summary>
        private bool _dumpThreadState;

        /// <summary>
        /// The private field used for the <see cref="Globals"/> property.
        /// </summary>
        private string _globals;

        /// <summary>
        /// The private field used for the <see cref="HsmEnabled"/> property.
        /// </summary>
        private bool _hsmEnabled;

        /// <summary>
        /// The private field used for the <see cref="IncludeFile"/> property.
        /// </summary>
        private string _includeFile;

        /// <summary>
        /// The private field used for the <see cref="IncludePaths"/> property.
        /// </summary>
        private List<string> _includePaths;

        /// <summary>
        /// The private field used for the <see cref="NativeFile"/> property.
        /// </summary>
        private string _nativeFile;

        /// <summary>
        /// The private field used for the <see cref="OutputDependencyFile"/> property.
        /// </summary>
        private bool _outputDependencyFile;

        /// <summary>
        /// The private field used for the <see cref="OutputDirectory"/> property.
        /// </summary>
        private string _outputDirectory;

        /// <summary>
        /// The private field used for the <see cref="OutputNativeFile"/> property.
        /// </summary>
        private bool _outputNativeFile;

        /// <summary>
        /// The private field used for the <see cref="PostCompileCommand"/> property.
        /// </summary>
        private string _postCompileCommand;
        /// <summary>
        /// The private field used for the <see cref="WarningsAsErrors"/> property.
        /// </summary>
        private bool _warningsAsErrors;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ProjectConfiguration"/> class.
        /// </summary>
        /// <param name="reader">
        /// The reader that contains the data to use to initialise this instance.
        /// </param>
        public ProjectConfiguration(XmlReader reader, IBranch branch)
        {
            if (reader == null)
            {
                throw new ArgumentNullException("reader");
            }

            this.Deserialise(reader, branch);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Gets the compiler executable value for this project configuration group.
        /// </summary>
        public string CompilerExecutable
        {
            get { return this._compilerExecutable; }
        }

        /// <summary>
        /// Gets the configuration name value for this project configuration group.
        /// </summary>
        public string Name
        {
            get { return this._name; }
        }

        /// <summary>
        /// Gets the custom value for this project configuration group.
        /// </summary>
        public string Custom
        {
            get { return this._custom; }
        }

        /// <summary>
        /// Gets the debug parser flag for this project configuration group.
        /// </summary>
        public bool DebugParser
        {
            get { return this._debugParser; }
        }

        /// <summary>
        /// Gets the dependency file value for this project configuration group.
        /// </summary>
        public string DependencyFile
        {
            get { return this._dependencyFile; }
        }

        /// <summary>
        /// Gets the display disassembly flag for this project configuration group.
        /// </summary>
        public bool DisplayDisassembly
        {
            get { return this._displayDisassembly; }
        }

        /// <summary>
        /// Gets the bump thread state flag for this project configuration group.
        /// </summary>
        public bool DumpThreadState
        {
            get { return this._dumpThreadState; }
        }

        /// <summary>
        /// Gets the globals value for this project configuration group.
        /// </summary>
        public string Globals
        {
            get { return this._globals; }
        }
        
        /// <summary>
        /// Gets the hsm enabled flag for this project configuration group.
        /// </summary>
        public bool HsmEnabled
        {
            get { return this._hsmEnabled; }
        }

        /// <summary>
        /// Gets the include file value for this project configuration group.
        /// </summary>
        public string IncludeFile
        {
            get { return this._includeFile; }
        }

        /// <summary>
        /// Gets the include paths for this project configuration group.
        /// </summary>
        public List<string> IncludePaths
        {
            get { return this._includePaths; }
        }

        /// <summary>
        /// Gets the native file value for this project configuration group.
        /// </summary>
        public string NativeFile
        {
            get { return this._nativeFile; }
        }

        /// <summary>
        /// Gets the output dependency file flag for this project configuration group.
        /// </summary>
        public bool OutputDependencyFile
        {
            get { return this._outputDependencyFile; }
        }

        /// <summary>
        /// Gets the output directory value for this project configuration group.
        /// </summary>
        public string OutputDirectory
        {
            get { return this._outputDirectory; }
        }

        /// <summary>
        /// Gets the output native file flag for this project configuration group.
        /// </summary>
        public bool OutputNativeFile
        {
            get { return this._outputNativeFile; }
        }

        /// <summary>
        /// Gets the post compile command value for this project configuration group.
        /// </summary>
        public string PostCompileCommand
        {
            get { return this._postCompileCommand; }
        }

        /// <summary>
        /// Gets the warning as errors flag for this project configuration group.
        /// </summary>
        public bool WarningsAsErrors
        {
            get { return this._warningsAsErrors; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Reads the data in the specified reader to initialise this instance.
        /// </summary>
        /// <param name="reader">
        /// The reader that contains the data to use to initialise this instance.
        /// </param>
        private void Deserialise(XmlReader reader, IBranch branch)
        {
            if (reader.IsEmptyElement)
            {
                reader.Skip();
                return;
            }

            reader.ReadStartElement();
            while (reader.MoveToContent() != XmlNodeType.EndElement)
            {
                if (!reader.IsStartElement())
                {
                    reader.Read();
                    continue;
                }

                if (String.Equals(reader.Name, "ConfigurationName"))
                {
                    this._name = reader.ReadElementContentAsString();
                    reader.Skip();
                }
                else if (String.Equals(reader.Name, "CompilerExecutable"))
                {
                    string compilerExecutable = branch.Environment.Subst(reader.ReadElementContentAsString());
                    this._compilerExecutable = compilerExecutable;
                    reader.Skip();
                }
                else if (String.Equals(reader.Name, "OutputDirectory"))
                {
                    string outputDirectory = branch.Environment.Subst(reader.ReadElementContentAsString());
                    this._outputDirectory = outputDirectory;
                    reader.Skip();
                }
                else if (String.Equals(reader.Name, "PostCompileCommand"))
                {
                    string postCompileCommand = branch.Environment.Subst(reader.ReadElementContentAsString());
                    this._postCompileCommand = postCompileCommand;
                    reader.Skip();
                }
                else if (String.Equals(reader.Name, "IncludeFile"))
                {
                    this._includeFile = reader.ReadElementContentAsString();
                    reader.Skip();
                }
                else if (String.Equals(reader.Name, "IncludePaths"))
                {
                    this._includePaths = new List<string>();
                    if (!reader.IsEmptyElement)
                    {
                        while (reader.MoveToContent() != XmlNodeType.EndElement)
                        {
                            if (String.Equals(reader.Name, "string"))
                            {
                                string includePath = reader.ReadElementContentAsString();
                                includePath = branch.Environment.Subst(includePath);

                                this._includePaths.Add(includePath);
                                reader.Skip();
                            }
                            else
                            {
                                reader.Read();
                            }
                        }
                    }

                    reader.Read();
                }
                else if (String.Equals(reader.Name, "Custom"))
                {
                    this._custom = reader.ReadElementContentAsString();
                    reader.Skip();
                }
                else if (String.Equals(reader.Name, "DebugParser"))
                {
                    this._debugParser = reader.ReadElementContentAsBoolean();
                    reader.Skip();
                }
                else if (String.Equals(reader.Name, "DumpThreadState"))
                {
                    this._dumpThreadState = reader.ReadElementContentAsBoolean();
                    reader.Skip();
                }
                else if (String.Equals(reader.Name, "DisplayDisassembly"))
                {
                    this._displayDisassembly = reader.ReadElementContentAsBoolean();
                    reader.Skip();
                }
                else if (String.Equals(reader.Name, "HsmEnabled"))
                {
                    this._hsmEnabled = reader.ReadElementContentAsBoolean();
                    reader.Skip();
                }
                else if (String.Equals(reader.Name, "OutputDependencyFile"))
                {
                    this._outputDependencyFile = reader.ReadElementContentAsBoolean();
                    reader.Skip();
                }
                else if (String.Equals(reader.Name, "OutputNativeFile"))
                {
                    this._outputNativeFile = reader.ReadElementContentAsBoolean();
                    reader.Skip();
                }
                else if (String.Equals(reader.Name, "WarningsAsErrors"))
                {
                    this._warningsAsErrors = reader.ReadElementContentAsBoolean();
                    reader.Skip();
                }
                else if (String.Equals(reader.Name, "Globals"))
                {
                    this._globals = reader.ReadElementContentAsString();
                    reader.Skip();
                }
                else if (String.Equals(reader.Name, "DependencyFile"))
                {
                    this._dependencyFile = reader.ReadElementContentAsString();
                    reader.Skip();
                }
                else if (String.Equals(reader.Name, "NativeFile"))
                {
                    this._nativeFile = reader.ReadElementContentAsString();
                    reader.Skip();
                }
                else
                {
                    reader.Skip();
                }
            }

            if (reader.NodeType == XmlNodeType.EndElement)
            {
                reader.ReadEndElement();
            }
        }
        #endregion Methods
    } // SanScriptProjectConverter.ProjectConfiguration {Class}
} // SanScriptProjectConverter {Namespace}
