﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using RSG.Metadata.Model.Tunables;

namespace MetadataPatchGenerator
{
    /// <summary>
    /// Base class for tunable modifications.
    /// </summary>
    public abstract class TunableModificationBase : ITunableModification
    {
        #region Fields
        /// <summary>
        /// The tunable that has been modified.
        /// </summary>
        protected readonly ITunable Tunable;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="TunableModificationBase"/> class
        /// using the specified tunable object.
        /// </summary>
        /// <param name="tunable"></param>
        protected TunableModificationBase(ITunable tunable)
        {
            Tunable = tunable;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The verb to use for the modification.
        /// </summary>
        public abstract String HttpVerb { get; }

        /// <summary>
        /// The Uri of the tunable we are modifying.
        /// </summary>
        public virtual String TunableUri
        {
            get { return TunableUtils.GetTunableUri(Tunable); }
        }


        /// <summary>
        /// Description of this modications (for outputting to the log).
        /// </summary>
        public abstract String Description { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Serialise the modification.
        /// </summary>
        /// <param name="writer"></param>
        public abstract void Serialise(XmlWriter writer);
        #endregion
    }
}
