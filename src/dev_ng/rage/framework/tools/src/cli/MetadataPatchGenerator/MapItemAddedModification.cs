﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using RSG.Metadata.Model.Tunables;

namespace MetadataPatchGenerator
{
    /// <summary>
    /// Modification containing information about an item that has been added to a map tunable.
    /// </summary>
    public class MapItemAddedModification : TunableModificationBase
    {
        #region Fields
        /// <summary>
        /// Item that was added.
        /// </summary>
        private readonly ITunable _newMapItem;

        /// <summary>
        /// Key of the item that was added.
        /// </summary>
        private readonly String _key;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="MapItemAddedModification"/> class
        /// using the specified map tunable and new map item tunable.
        /// </summary>
        /// <param name="tunable"></param>
        /// <param name="newMapItem"></param>
        /// <param name="key"></param>
        public MapItemAddedModification(ITunable tunable, ITunable newMapItem, String key)
            : base(tunable)
        {
            _newMapItem = newMapItem;
            _key = key;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The verb to use for the modification.
        /// </summary>
        public override String HttpVerb
        {
            get { return "POST"; }
        }

        /// <summary>
        /// The Uri of the tunable we are modifying.
        /// </summary>
        public override String TunableUri
        {
            get { return String.Format("{0}?insert={1}", base.TunableUri, _key); }
        }

        /// <summary>
        /// Description of this modification (for outputting to the log).
        /// </summary>
        public override String Description
        {
            get
            {
                return String.Format("Added new item to the {0} map with key {1}.", base.TunableUri, _key);
            }
        }
        #endregion

        #region ITunableModification Implementation
        /// <summary>
        /// Serialise the modification.
        /// </summary>
        /// <param name="writer"></param>
        public override void Serialise(XmlWriter writer)
        {
            writer.WriteStartElement("Item");
            _newMapItem.Serialise(writer, false);
            writer.WriteEndElement();
        }
        #endregion 
    }
}
