﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using System.Xml.Linq;
using RSG.Base;
using RSG.Base.OS;
using RSG.Editor.Controls;
using RSG.Editor.Model;
using RSG.Metadata.Model.Definitions;
using RSG.Metadata.Model.Tunables;

namespace MetadataPatchGenerator
{
    /// <summary>
    /// Application for the metadata patch generator.
    /// </summary>
    public class MetadataPatchGeneratorApp : RsConsoleApplication
    {
        #region Program Entry Point
        /// <summary>
        /// Program entry point.
        /// </summary>
        /// <param name="args"></param>
        [STAThread]
        static int Main(string[] args)
        {
            return new MetadataPatchGeneratorApp().Run();
        }
        #endregion

        #region Fields
        /// <summary>
        /// File to use as the base file for the diff.
        /// </summary>
        private String _baseFile;

        /// <summary>
        /// The modified meta file for which we require the diff to be generated.
        /// </summary>
        private String _modifiedFile;

        /// <summary>
        /// The directory containing the metadata definition files.
        /// </summary>
        private String _definitionRoot;

        /// <summary>
        /// Full path to the output file.
        /// </summary>
        private String _patchFile;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="MetadataPatchGeneratorApp"/> class.
        /// </summary>
        public MetadataPatchGeneratorApp()
            : base()
        {
            RegisterCommandlineArg("base_file", LongOption.ArgType.Required, "File to use as a base file for the diff.");
            RegisterCommandlineArg("modified_file", LongOption.ArgType.Required, "The modified meta file for which we require a diff.");
            RegisterCommandlineArg("definitions", LongOption.ArgType.Required, "Directory containing the metadata definition files.");
            RegisterCommandlineArg("patch_file", LongOption.ArgType.Required, "Full path to the patch file.");
            //RegisterCommandlineArg("size_warn_percent", LongOption.ArgType.Required, "Percentage of the original file at which a warning gets generated.");
        }
        #endregion

        #region Overrides
        /// <summary>
        /// Checks that the required command line parameters were provided.
        /// </summary>
        /// <returns>
        /// Value indicating whether we should execute the <see cref="ConsoleMain"/>
        /// method.
        /// </returns>
        protected override bool OnConsoleStartup()
        {
            if (!CommandLineOptions.HasOption("base_file"))
            {
                Log.Error("Missing required command line argument: base_file.");
                ShowUsage();
                return false;
            }
            _baseFile = Path.GetFullPath((String)CommandLineOptions["base_file"]);

            if (!CommandLineOptions.HasOption("modified_file"))
            {
                Log.Error("Missing required command line argument: modified_file.");
                ShowUsage();
                return false;
            }
            _modifiedFile = Path.GetFullPath((String)CommandLineOptions["modified_file"]);

            if (!CommandLineOptions.HasOption("definitions"))
            {
                Log.Error("Missing required command line argument: definitions.");
                ShowUsage();
                return false;
            }
            _definitionRoot = Path.GetFullPath((String)CommandLineOptions["definitions"]);

            // Check whether the user supplied an output path.
            if (CommandLineOptions.HasOption("patch_file"))
            {
                _patchFile = Path.GetFullPath((String)CommandLineOptions["patch_file"]);
            }
            else
            {
                _patchFile = Path.ChangeExtension(_baseFile, ".patch");
            }

            return true;
        }

        /// <summary>
        /// Executes the application logic.
        /// </summary>
        protected override int ConsoleMain()
        {
            IDefinitionDictionary definitions;
            TunableStructure baseStructure;
            TunableStructure otherStructure;

            try
            {
                definitions = LoadDefinitions();
            }
            catch (Exception e)
            {
                Log.ToolException(e, "Unable to load metadata definition files.");
                return 1;
            }
            Log.Message("Loaded metadata definition files from '{0}'.", _definitionRoot);

            try
            {
                baseStructure = LoadMetaFile(_baseFile, definitions);
            }
            catch (Exception e)
            {
                Log.ToolException(e, "Unable to load base metadata file.");
                return 1;
            }
            Log.Message("Loaded base metadata file '{0}'.", _baseFile);

            try
            {
                otherStructure = LoadMetaFile(_modifiedFile, definitions);
            }
            catch (Exception e)
            {
                Log.ToolException(e, "Unable to load modified metadata file.");
                return 1;
            }
            Log.Message("Loaded modified metadata file '{0}'.", _modifiedFile);

            Log.Message("Generating modifications list.");
            IList<ITunable> modifiedTunables = new List<ITunable>();
            IList<Tuple<ITunableParent, ITunableParent>> modifiedTunableParents = new List<Tuple<ITunableParent, ITunableParent>>();

            CompareTunableStructures(baseStructure, otherStructure, modifiedTunables, modifiedTunableParents);

            // Check if any tunables are different.
            if (modifiedTunables.Any() || modifiedTunableParents.Any())
            {
                ITunableModification[] modifications = CreateModificationList(modifiedTunables, modifiedTunableParents).ToArray();
                Log.Message("{0} modifications detected.", modifications.Length);
                foreach (ITunableModification mod in modifications)
                {
                    Log.Message(mod.Description);
                }

                Log.Message("Creating patch file '{0}'.", _patchFile);
                CreatePatchFile(modifications);
                Log.Message("Patch file successfully created.");
                OutputStats();

                String generationStatus = "successfully";
                if (Log.HasErrors)
                {
                    generationStatus = "with errors";
                }
                else if (Log.HasWarnings)
                {
                    generationStatus = "with warnings";
                }

                Log.Message("Patch generation process completed {0}.", generationStatus);
            }
            else
            {
                Log.Message("No differences where detected between the two files.  No patch file will be generated.");
            }

            // Set the appropriate return code.
            int returnCode = 0;
            if (Log.HasErrors)
            {
                returnCode = 1;
            }
            else if (Log.HasWarnings)
            {
                returnCode = 2;
            }

            return returnCode;
        }
        #endregion

        #region Methods
        /// <summary>
        /// Loads the metadata definitions.
        /// </summary>
        /// <returns></returns>
        private IDefinitionDictionary LoadDefinitions()
        {
            IDefinitionDictionary definitions = new DefinitionDictionary();

            if (Directory.Exists(_definitionRoot))
            {
                string[] files = Directory.GetFiles(_definitionRoot, "*.psc", SearchOption.AllDirectories);
                foreach (string file in files)
                {
                    definitions.LoadFile(file);
                }
            }

            return definitions;
        }

        /// <summary>
        /// Creates a tunable structure based off of the provided filepath and definitions.
        /// </summary>
        /// <param name="filepath"></param>
        /// <returns></returns>
        private TunableStructure LoadMetaFile(String filepath, IDefinitionDictionary definitions)
        {
            TunableStructure tunableStructure = null;
            using (XmlReader reader = XmlReader.Create(filepath))
            {
                reader.MoveToContent();
                tunableStructure = new TunableStructure(reader, null, definitions, Log);
            }
            return tunableStructure;
        }

        /// <summary>
        /// Compares two tunable structures adding any differences to the modifications list.
        /// </summary>
        /// <param name="baseParent"></param>
        /// <param name="otherParent"></param>
        /// <param name="modifiedTunables"></param>
        /// <param name="modifiedTunableParents"></param>
        private void CompareTunableStructures(
            TunableStructure baseStructure,
            TunableStructure otherStructure,
            IList<ITunable> modifiedTunables,
            IList<Tuple<ITunableParent, ITunableParent>> modifiedTunableParents)
        {
            if (baseStructure.Definition != otherStructure.Definition)
            {
                Log.Error("Files are using different structure definitions. Unable to compare their contents.");
                return;
            }

            // Check whether there are any differences.
            if (baseStructure.EqualByValue(otherStructure))
            {
                return;
            }

            // Find the differences between the two.
            CompareTunableParents(baseStructure, otherStructure, modifiedTunables, modifiedTunableParents);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="baseTunableParent"></param>
        /// <param name="otherTunableParent"></param>
        /// <param name="modifiedTunables"></param>
        /// <param name="modifiedTunableParents"></param>
        private void CompareTunableParents(
            ITunableParent baseTunableParent,
            ITunableParent otherTunableParent,
            IList<ITunable> modifiedTunables,
            IList<Tuple<ITunableParent, ITunableParent>> modifiedTunableParents)
        {
            bool parentModified = false;

            if (baseTunableParent.GetType() != otherTunableParent.GetType())
            {
                if (otherTunableParent is ITunable)
                {
                    modifiedTunables.Add((ITunable)otherTunableParent);
                }
                else
                {
                    throw new NotSupportedException();
                }
            }
            else if (baseTunableParent is MapTunable)
            {
                // Check whether the keys match.
                MapTunable baseMapTunable = (MapTunable)baseTunableParent;
                MapTunable otherMapTunable = (MapTunable)otherTunableParent;
                if (baseMapTunable.Keys.Except(otherMapTunable.Keys).Any() ||
                    otherMapTunable.Keys.Except(baseMapTunable.Keys).Any())
                {
                    modifiedTunableParents.Add(Tuple.Create(baseTunableParent, otherTunableParent));
                }

                foreach (String key in otherMapTunable.Keys)
                {
                    ITunable baseTunable = baseMapTunable[key];
                    ITunable otherTunable = otherMapTunable[key];
                    if (baseTunable != null)
                    {
                        CompareTunable(baseTunable, otherTunable, modifiedTunables, modifiedTunableParents);
                    }
                }
            }
            else
            {
                if (baseTunableParent.Tunables.Count != otherTunableParent.Tunables.Count)
                {
                    modifiedTunableParents.Add(Tuple.Create(baseTunableParent, otherTunableParent));
                }

                // Loop over all the children.
                int childCount = Math.Min(baseTunableParent.Tunables.Count, otherTunableParent.Tunables.Count);
                for (int i = 0; i < childCount; ++i)
                {
                    ITunable baseTunable = baseTunableParent.Tunables[i];
                    ITunable otherTunable = otherTunableParent.Tunables[i];
                    CompareTunable(baseTunable, otherTunable, modifiedTunables, modifiedTunableParents);
                }
            }
        }

        /// <summary>
        /// Compares two tunables adding any differences to the modifications list.
        /// </summary>
        /// <param name="baseTunable"></param>
        /// <param name="otherTunable"></param>
        /// <param name="modifiedTunables"></param>
        /// <param name="modifiedTunableParents"></param>
        private void CompareTunable(
            ITunable baseTunable,
            ITunable otherTunable,
            IList<ITunable> modifiedTunables,
            IList<Tuple<ITunableParent, ITunableParent>> modifiedTunableParents)
        {
            // If the tunables are equal then there is nothing to do.
            if (baseTunable.EqualByValue(otherTunable))
            {
                return;
            }

            //
            if (baseTunable is ITunableParent)
            {
                ITunableParent baseTunableParent = (ITunableParent)baseTunable;

                if (otherTunable is ITunableParent)
                {
                    ITunableParent otherTunableParent = (ITunableParent)otherTunable;
                    CompareTunableParents(baseTunableParent, otherTunableParent, modifiedTunables, modifiedTunableParents);
                }
                else
                {
                    modifiedTunables.Add(otherTunable);
                }
            }
            else
            {
                modifiedTunables.Add(otherTunable);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="modifiedTunables"></param>
        /// <param name="modifiedTunableParents"></param>
        /// <returns></returns>
        private IEnumerable<ITunableModification> CreateModificationList(
            IEnumerable<ITunable> modifiedTunables,
            IEnumerable<Tuple<ITunableParent, ITunableParent>> modifiedTunableParents)
        {
            IList<ITunableModification> modifications = new List<ITunableModification>();

            // Go over basic tunable item changes first grouping them by their parent.
            IEnumerable<IGrouping<ITunable, ITunable>> modifiedTunablesGroupedByParent =
                modifiedTunables.GroupBy(item => item.Parent as ITunable);
            foreach (IGrouping<ITunable, ITunable> group in modifiedTunablesGroupedByParent)
            {
                modifications.Add(new ItemModification(group.Key, group.ToArray()));
            }

            // Next check to see whether any parent tunables have been modified
            foreach (Tuple<ITunableParent, ITunableParent> parentTuple in modifiedTunableParents)
            {
                ITunableParent baseParent = parentTuple.Item1;
                ITunableParent modifiedParent = parentTuple.Item2;

                if (baseParent is MapTunable)
                {
                    MapTunable baseMap = (MapTunable)baseParent;
                    MapTunable modifiedMap = (MapTunable)modifiedParent;

                    // Check for keys that were removed.
                    foreach (String key in baseMap.Keys.Except(modifiedMap.Keys))
                    {
                        Log.Warning("Map tunable '{0}' has had child tunable with key '{1}' removed.",
                            TunableUtils.GetTunableUri(modifiedMap),
                            key);
                        modifications.Add(new MapItemRemovedModification(baseMap, key));
                    }

                    // Chck for keys that were addded.
                    foreach (String key in modifiedMap.Keys.Except(baseMap.Keys))
                    {
                        ITunable tunableAdded = modifiedMap[key];
                        modifications.Add(new MapItemAddedModification(baseMap, tunableAdded, key));
                    }
                }
                else
                {
                    int originalChildCount = baseParent.Tunables.Count;
                    int modifiedChildCount = modifiedParent.Tunables.Count;

                    if (originalChildCount > modifiedChildCount)
                    {
                        Log.Error("Number of child tunables for '{0}' decreased from {1} to {2}.  This is not supported!",
                            TunableUtils.GetTunableUri(parentTuple.Item1 as ITunable),
                            originalChildCount,
                            modifiedChildCount);
                    }
                    else
                    {
                        if (parentTuple.Item1 is ArrayTunable)
                        {
                            Log.Warning("The '{0}' array has been modified.  If the patch file contains a large number of modifications this might be because an item was added to the middle of an array.",
                                TunableUtils.GetTunableUri(parentTuple.Item1 as ITunable));

                            for (int i = originalChildCount; i < modifiedChildCount; ++i)
                            {
                                modifications.Add(new ArrayItemAddedModification(baseParent as ITunable, modifiedParent.Tunables[i]));
                            }
                        }
                        else
                        {
                            Log.Error("");
                        }
                    }
                }
            }

            return modifications;
        }

        /// <summary>
        /// Serialises out the patch file.
        /// </summary>
        /// <param name="modifications"></param>
        private void CreatePatchFile(IEnumerable<ITunableModification> modifications)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "\t";

            using (XmlWriter writer = XmlWriter.Create(_patchFile, settings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Patch");

                foreach (ITunableModification mod in modifications)
                {
                    writer.WriteStartElement("Item");
                    writer.WriteAttributeString("verb", mod.HttpVerb);
                    writer.WriteAttributeString("uri", mod.TunableUri);
                    mod.Serialise(writer);
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }

        /// <summary>
        /// Outputs some summary information about the patch generation that just took place.
        /// </summary>
        private void OutputStats()
        {
            FileInfo baseFileInfo = new FileInfo(_baseFile);
            FileInfo modifiedFileInfo = new FileInfo(_modifiedFile);
            FileInfo patchFileInfo = new FileInfo(_patchFile);

            Log.Message("Patch File Stats:");
            Log.Message("Base File Size:     {0}", (FileSize)baseFileInfo.Length);
            Log.Message("Modified File Size: {0}", (FileSize)modifiedFileInfo.Length);
            Log.Message("Patch File Size:    {0}", (FileSize)patchFileInfo.Length);
            Log.Message("% of Base File:     {0:P}", (double)patchFileInfo.Length / baseFileInfo.Length);
            Log.Message("% of Modified File: {0:P}", (double)patchFileInfo.Length / modifiedFileInfo.Length);
            
            if (patchFileInfo.Length > modifiedFileInfo.Length)
            {
                Log.Error("Patch file is larger than the modified file.");
            }
            else if (patchFileInfo.Length > (modifiedFileInfo.Length * 0.75))
            {
                Log.Warning("Patch file size is within 75% of the modified file.");
            }
        }
        #endregion
    }
}
