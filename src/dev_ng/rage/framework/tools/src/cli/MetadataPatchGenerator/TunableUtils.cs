﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Metadata.Model.Tunables;

namespace MetadataPatchGenerator
{
    /// <summary>
    /// Utility methods.
    /// </summary>
    public static class TunableUtils
    {
        /// <summary>
        /// Returns the path to the object, including its name and any array indexes, in this format:
        /// <RootTunableName>/<ArrayTunableName>/<ArrayIndex>/<ThisTunableName>
        /// This is designed to be compatible with the URL format for live editing metadata.
        /// Handles arrays and pointers correctly, as far as I know.
        /// Example:
        /// A tunable named AmmoMax that has a StructureTunable as it's parent, which
        /// itself is pointed to by a PointerTunable, which itself is the first element of a
        /// ArrayTunable called Infos, which is pointed to by a PT which lies inside another AT
        /// also called Infos will get this path:
        /// Infos/0/Infos/0/AmmoMax
        /// </summary>
        /// <returns></returns>
        public static String GetTunableUri(ITunable tunable, bool includeTunableName = true)
        {
            ITunable current = tunable;
            ITunableParent parent = tunable.Parent as ITunableParent;

            IList<String> pathComponents = new List<String>();
            if (includeTunableName)
            {
                pathComponents.Add(current.Member.MetadataName);
            }

            while (parent != null)
            {
                if (parent is ArrayTunable)
                {
                    int index = parent.Tunables.IndexOf(current);
                    if (index == -1)
                    {
                        throw new MetadataException("Invalid parent/child relationship.");
                    }

                    pathComponents.Add(index.ToString());
                    pathComponents.Add((parent as ITunable).Member.MetadataName);
                }
                else if (parent is MapTunable)
                {
                    MapTunable parentMap = (MapTunable)parent;
                    String key = parentMap[current];
                    if (key == null)
                    {
                        throw new MetadataException("Invalid parent/child relationship.");
                    }

                    pathComponents.Add(key);
                    pathComponents.Add((parent as ITunable).Member.MetadataName);
                }
                else if (!(parent is PointerTunable) && !(parent is TunableStructure))
                {
                    pathComponents.Add((parent as ITunable).Member.MetadataName);
                }

                current = current.Parent as ITunable;
                parent = parent.Parent as ITunableParent;
            }

            return "/" + String.Join("/", pathComponents.Reverse());
        }
    }
}
