﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using RSG.Metadata.Model.Tunables;

namespace MetadataPatchGenerator
{
    /// <summary>
    /// Modification which contains information an item that has been added to and array tunable.
    /// </summary>
    public class ArrayItemAddedModification : TunableModificationBase
    {
        #region Fields
        /// <summary>
        /// New item that was added to the array.
        /// </summary>
        private readonly ITunable _addedTunable;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ArrayItemAddedModification"/> class
        /// using the specified tunable parent and tunable that was added.
        /// </summary>
        /// <param name="tunable"></param>
        /// <param name="addedTunable"></param>
        public ArrayItemAddedModification(ITunable tunable, ITunable addedTunable)
            : base(tunable)
        {
            _addedTunable = addedTunable;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The verb to use for the modification.
        /// </summary>
        public override String HttpVerb
        {
            get { return "POST"; }
        }

        /// <summary>
        /// The Uri of the tunable we are modifying.
        /// </summary>
        public override String TunableUri
        {
            get { return String.Format("{0}?insert", base.TunableUri); }
        }

        /// <summary>
        /// Description of this modification (for outputting to the log).
        /// </summary>
        public override String Description
        {
            get
            {
                return String.Format("Added new item to {0} at index {1}.", base.TunableUri, ((ArrayTunable)Tunable).Tunables.IndexOf(_addedTunable));
            }
        }
        #endregion

        #region ITunableModification Implementation
        /// <summary>
        /// Serialise the modification.
        /// </summary>
        /// <param name="writer"></param>
        public override void Serialise(XmlWriter writer)
        {
            writer.WriteStartElement("Item");
            _addedTunable.Serialise(writer, false);
            writer.WriteEndElement();
        }
        #endregion 
    }
}
