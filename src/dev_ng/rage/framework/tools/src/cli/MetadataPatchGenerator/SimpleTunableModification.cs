﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using RSG.Metadata.Model.Tunables;

namespace MetadataPatchGenerator
{
    /// <summary>
    /// Represents a simple modification to a basic tunable object.
    /// </summary>
    public class SimpleTunableModification : TunableModificationBase
    {
        #region Fields
        /// <summary>
        /// Name of the property that changed value.
        /// </summary>
        private readonly String _propertyName;

        /// <summary>
        /// New value for the property.
        /// </summary>
        private readonly String _modifiedValue;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="SimpleTunableModification"/> class
        /// using the specified tunable and changed property values.
        /// </summary>
        /// <param name="tunable"></param>
        /// <param name="propertyName"></param>
        /// <param name="modifiedValue"></param>
        public SimpleTunableModification(ITunable tunable, String propertyName, String modifiedValue)
            : base(tunable)
        {
            _propertyName = propertyName;
            _modifiedValue = modifiedValue;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The verb to use for the modification.
        /// </summary>
        public override string HttpVerb
        {
            get { return "PUT"; }
        }

        /// <summary>
        /// Description of this modications (for outputting to the log).
        /// </summary>
        public override String Description
        {
            get
            {
                return "";
            }
        }
        #endregion

        #region ITunableModification Implementation
        /// <summary>
        /// Serialise the modification.
        /// </summary>
        /// <param name="writer"></param>
        public override void Serialise(XmlWriter writer)
        {
            writer.WriteString(_modifiedValue);
        }
        #endregion 
    }
}
