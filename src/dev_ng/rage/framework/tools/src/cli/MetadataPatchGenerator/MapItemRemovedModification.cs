﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using RSG.Metadata.Model.Tunables;

namespace MetadataPatchGenerator
{
    /// <summary>
    /// Modification containing information about an item that has been added to a map tunable.
    /// </summary>
    public class MapItemRemovedModification : TunableModificationBase
    {
        #region Fields
        /// <summary>
        /// Key of the item that was removed.
        /// </summary>
        private readonly String _key;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="MapItemRemovedModification"/> class
        /// using the specified map tunable and key of the tunable that was removed.
        /// </summary>
        /// <param name="tunable"></param>
        /// <param name="key"></param>
        public MapItemRemovedModification(ITunable tunable, String key)
            : base(tunable)
        {
            _key = key;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The verb to use for the modification.
        /// </summary>
        public override String HttpVerb
        {
            get { return "POST"; }
        }

        /// <summary>
        /// The Uri of the tunable we are modifying.
        /// </summary>
        public override String TunableUri
        {
            get { return String.Format("{0}?delete={1}", base.TunableUri, _key); }
        }

        /// <summary>
        /// Description of this modification (for outputting to the log).
        /// </summary>
        public override String Description
        {
            get
            {
                return String.Format("Removed item from the {0} map with key {1}.", base.TunableUri, _key);
            }
        }
        #endregion

        #region ITunableModification Implementation
        /// <summary>
        /// Serialise the modification.
        /// </summary>
        /// <param name="writer"></param>
        public override void Serialise(XmlWriter writer)
        {
            // Nothing to do.
        }
        #endregion
    }
}
