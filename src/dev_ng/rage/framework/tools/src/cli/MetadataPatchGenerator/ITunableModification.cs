﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace MetadataPatchGenerator
{
    /// <summary>
    /// Keeps track of a single modification to the metadata file.
    /// </summary>
    public interface ITunableModification
    {
        /// <summary>
        /// The verb to use for the modification.
        /// </summary>
        String HttpVerb { get; }

        /// <summary>
        /// The Uri of the tunable we are modifying.
        /// </summary>
        String TunableUri { get; }

        /// <summary>
        /// Description of this modications (for outputting to the log).
        /// </summary>
        String Description { get; }

        /// <summary>
        /// Serialise the modification.
        /// </summary>
        /// <param name="writer"></param>
        void Serialise(XmlWriter writer);
    }
}
