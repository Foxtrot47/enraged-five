﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using RSG.Metadata.Model.Tunables;

namespace MetadataPatchGenerator
{
    /// <summary>
    /// Represents a modification to an item in an array tunable object.
    /// </summary>
    public class ItemModification : TunableModificationBase
    {
        #region Fields
        /// <summary>
        /// Tunables that were modified.
        /// </summary>
        private readonly ITunable[] _modifiedTunables;
        #endregion

        #region Constructor(s)
        /// <summary>
        /// Initialises a new instance of the <see cref="ItemModification"/> class
        /// using the specified tunable and changed property values.
        /// </summary>
        /// <param name="tunable"></param>
        /// <param name="propertyName"></param>
        /// <param name="modifiedValue"></param>
        public ItemModification(ITunable tunable, ITunable[] modifiedTunables)
            : base(tunable)
        {
            _modifiedTunables = modifiedTunables;
        }
        #endregion

        #region Properties
        /// <summary>
        /// The verb to use for the modification.
        /// </summary>
        public override string HttpVerb
        {
            get { return "PUT"; }
        }

        /// <summary>
        /// The Uri of the tunable we are modifying.
        /// </summary>
        public override String TunableUri
        {
            get { return TunableUtils.GetTunableUri(Tunable, false); }
        }

        /// <summary>
        /// Description of this modications (for outputting to the log).
        /// </summary>
        public override String Description
        {
            get
            {
                return String.Format("{0} has {1} modified values.", TunableUri, _modifiedTunables.Length);
            }
        }
        #endregion

        #region ITunableModification Implementation
        /// <summary>
        /// Serialise the modification.
        /// </summary>
        /// <param name="writer"></param>
        public override void Serialise(XmlWriter writer)
        {
            writer.WriteStartElement(Tunable.Member.MetadataName);
            foreach (ITunable modifiedTunable in _modifiedTunables)
            {
                writer.WriteStartElement(modifiedTunable.Member.MetadataName);
                modifiedTunable.Serialise(writer, true);
                writer.WriteEndElement();
            }
            writer.WriteEndElement();
        }
        #endregion
    }
}
