﻿//---------------------------------------------------------------------------------------------
// <copyright file="ConsoleHostApp.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ServiceModel;
using RSG.Base.Logging;
using RSG.Base.OS;
using RSG.Configuration;
using RSG.Editor.Controls;
using RSG.Services.Configuration;
using RSG.Services.Configuration.ServiceModel;
using RSG.Services.Configuration.Statistics;
using RSG.Services.Persistence;
using RSG.Services.Statistics.Implementations;

namespace RSG.Services.Statistics.ConsoleHost
{
    /// <summary>
    /// Statistics console host application.
    /// </summary>
    public class ConsoleHostApp : RsInteractiveConsoleApplication
    {
        #region Program Entry Point
        /// <summary>
        /// Program entry point.
        /// </summary>
        /// <param name="args"></param>
        [STAThread]
        static void Main(string[] args)
        {
            ConsoleHostApp app = new ConsoleHostApp();
            app.Run();
        }
        #endregion

        #region Fields
        /// <summary>
        /// Reference to the service host factory.
        /// </summary>
        private StatisticsServiceHostFactory _serviceHostFactory;

        /// <summary>
        /// Collection of service hosts that have been opened by the application.
        /// </summary>
        private readonly IList<ServiceHost> _serviceHosts = new List<ServiceHost>();
        #endregion

        #region Constructors
        /// <summary>
        /// Initialises a new instance of the <see cref="ConsoleHostApp"/> class.
        /// </summary>
        public ConsoleHostApp()
            : base()
        {
            // Command-line args.
            RegisterCommandlineArg("wipeDatabase", LongOption.ArgType.None, "Optional flag to wipe the database.");

            // Custom console commands.
            RegisterConsoleCommand("show-stats", ShowStatsCommandHandler, "Displays NHibernate session factory statistics.");
            RegisterConsoleCommand("clear-stats", ClearStatsCommandHandler, "Clears NHibernate session factory statistics.");
        }
        #endregion

        #region Methods
        /// <summary>
        /// Clears the NHibernate session factory statistics.
        /// </summary>
        /// <param name="command">
        /// The command that is being handled by this method.
        /// </param>
        /// <param name="arguments">
        /// The arguments that have be sent with the command.
        /// </param>
        protected void ClearStatsCommandHandler(string command, string[] arguments)
        {
            _serviceHostFactory.SessionFactory.Statistics.Clear();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="factory"></param>
        /// <returns></returns>
        private ServiceHost CreateServiceHost<T>() where T : TransactionalService
        {
            Type type = typeof(T);
            Log.Message("Creating service host for the '{0}' service.", type.FullName);

            ServiceHost host = _serviceHostFactory.CreateServiceHost(type);
            host.Open();
            return host;
        }

        /// <summary>
        /// Sets up the console and stars a task that will run it.
        /// </summary>
        protected override bool OnConsoleStartup()
        {
            bool wipeDatabase = CommandOptions.ContainsOption("wipeDatabase");

            // Create the service host factory.
            _serviceHostFactory = new StatisticsServiceHostFactory((IStatisticsServer)StatisticsServer, wipeDatabase);

            // Keep track of the service hosts we opened so that we can close them later on
            using (new ProfileContext(Log, "Starting WCF Services"))
            {
                _serviceHosts.Add(CreateServiceHost<ApplicationSessionService>());
                _serviceHosts.Add(CreateServiceHost<ProfileSessionService>());
            }

            return true;
        }

        /// <summary>
        /// Allows derived classes to execute code after the <see cref="ConsoleMain"/>
        /// method has been called.
        /// </summary>
        protected override void OnConsoleShutdown()
        {
            using (new ProfileContext(Log, "Shutting down WCF Services"))
            {
                foreach (ServiceHost host in _serviceHosts)
                {
                    host.CloseOrAbort();
                }
                _serviceHosts.Clear();
            }
        }

        /// <summary>
        /// Shows the NHibernate session factory statistics.
        /// </summary>
        /// <param name="command">
        /// The command that is being handled by this method.
        /// </param>
        /// <param name="arguments">
        /// The arguments that have be sent with the command.
        /// </param>
        protected void ShowStatsCommandHandler(string command, string[] arguments)
        {
            _serviceHostFactory.SessionFactory.Statistics.LogSummary();
        }
        #endregion
    }
}
