
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stddef.h>
#include <sys/memory.h>
#include <cell/pad.h>
#include <cell/gcm.h>
#include <sys/sys_fixed_addr.h>


void FoundMemoryError(unsigned int* pMemAddr, unsigned int iDesiredVal, unsigned int iActualVal, int iFailedAfter)
{
	printf("\n\nDetected memory error at %x - should have been %x, but was %x\n", pMemAddr, iDesiredVal, iActualVal);
	printf("Failed after %i tests\n", iFailedAfter);
}

bool TestMemory(void *pMemory, int iNumBytes, unsigned int iExpectedVal, int iTestIteration)
{
	unsigned int *pMem = (unsigned int*)pMemory;

	int iNumDwords = iNumBytes / sizeof(unsigned int);
	for(int i=0; i<iNumDwords; i++)
	{
		if(pMem[i] != iExpectedVal)
		{
			FoundMemoryError(&pMem[i], iExpectedVal, pMem[i], iTestIteration);
			return false;
		}
	}
	return true;
}


#define VRAM_SIZE	(249)

int main(void)
{
	cellGcmInit(0x10000, 0x100000, (void*)(((uintptr_t)malloc(0x200000)+0xfffff)&~0xfffff));

	int iFreeMem = (VRAM_SIZE * 1024 * 1024);
//	int iAllocStepSize = 1024*1024;
	int iMemToAlloc = iFreeMem;	// - iAllocStepSize;	//??

	sys_addr_t pBuffer = RSX_FB_BASE_ADDR;

	printf("\n\n\n\nBegining Memory Test\n");

	bool bQuit = false;

	while(!bQuit)
	{
		for(unsigned int i=0; i<256; i++)
		{
			unsigned int testValue = i;
			unsigned int testValue32;

			testValue32 = (testValue << 24) | (testValue<<16) | (testValue<<8) | testValue;

			memset((void*)pBuffer, testValue, iMemToAlloc);		//XMemSet(pBuffer, testValue, iMemToAlloc);
			
			if(!TestMemory((void*)pBuffer, iMemToAlloc, testValue32, i))
			{
				return 0;	
			}

			testValue = i ^ 0xff;
			testValue32 = (testValue << 24) | (testValue<<16) | (testValue<<8) | testValue;

			memset((void*)pBuffer, testValue, iMemToAlloc);		//XMemSet(pBuffer, testValue, iMemToAlloc);

			if(!TestMemory((void*)pBuffer, iMemToAlloc, testValue32, i))
			{
				return 0;	
			}

			printf(".");		//g_Console.Format(".");

			if(((i+1)&0xf)==0)
				printf("\n");
		}
		//*********************************************************************
		//	Test has completed successfully.  Your PS3 devkit is probably ok

		printf("\n\nMemory appears to be ok.\n");

		bQuit = true;
	}
	return 0;
}
