﻿using System;
using System.IO;
using System.Threading;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;

namespace BugstarDifficultyIntegration
{
    class Program
    {
        #region Constants
        private static readonly String OPTION_FILE_LOCATION = "file";
        #endregion

        #region Fields
        private static string _telemetryPath;
        private static long _previousSize;
        private static long _latestUniqueIdentifier;
        private static volatile bool _isRunning;
        private static IUniversalLog _log;
        private static IUniversalLogTarget _logFile;
        #endregion

        static int Main(string[] args)
        {
            _log = LogFactory.CreateUniversalLog("Bugstar Difficulty Console");
            _logFile = LogFactory.CreateUniversalLogFile(_log);
            LogFactory.CreateApplicationConsoleLogTarget();

            Getopt options = new Getopt(args, new LongOption[] {
                new LongOption(OPTION_FILE_LOCATION, LongOption.ArgType.Required, 'f')
            });

            if (!options.HasOption(OPTION_FILE_LOCATION))
            {
                // No file specified; bail.
                return (1);
            }

            _telemetryPath = (string)options.Options[OPTION_FILE_LOCATION];
            _isRunning = true;

            Console.CancelKeyPress += delegate(object sender, ConsoleCancelEventArgs e)
            {
                e.Cancel = true;
                _isRunning = false;
            };

            // Consume the entire contents of the file.
            FileInfo fi = new FileInfo(_telemetryPath);
            _previousSize = fi.Length;

            while (_isRunning)
            {
                CheckFile();
                Thread.Sleep(3000);
            }

            return (0);
        }

        private static void CheckFile()
        {
            try
            {
                FileInfo fi = new FileInfo(_telemetryPath);
                long currentSize = fi.Length;
                    
                if (currentSize == _previousSize)
                {
                    // Nothing to do.
                    return;
                }

                long size = currentSize - _previousSize;
                if (size < 0)
                {
                    // Game was reset. Seek to EOF.
                    _previousSize = currentSize;
                    return;
                }

                using (FileStream fs = new FileStream(_telemetryPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    try
                    {
                        fs.Seek(_previousSize, SeekOrigin.Begin);
                    }
                    catch (Exception ex)
                    {
                        _log.Error(String.Format("Seek error; sought position {0} but stream length was {1}", _previousSize, fs.Length), ex);
                    }

                    byte[] b = new byte[size];
                    fs.Read(b, 0, (int)(size));

                    string inbound = System.Text.Encoding.UTF8.GetString(b);
                    _previousSize += size;
                    ConsumeData(inbound); // Process the data that was just appended to the file.
                }
            }
            catch (Exception ex)
            {
                _log.Error("Error while reading telemetry file", ex);
            }
        }

        private static void ConsumeData(string data)
        {
            // Loop over each line in the data and attempt to parse JSON.
            string[] lines = data.Split(new string[] { "\r\n", "\n" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string line in lines)
            {
                Console.WriteLine(line);
            }
        }
    }
}
