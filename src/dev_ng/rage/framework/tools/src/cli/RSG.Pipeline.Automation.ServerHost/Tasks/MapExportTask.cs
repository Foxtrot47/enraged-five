﻿using System;
using System.Collections.Generic;
using System.Linq;
using SIO = System.IO;
using System.Text;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Export;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Services;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;
using RSG.SourceControl.Perforce;
using RSG.SourceControl.Perforce.Util;
using RSG.Pipeline.Services;

namespace RSG.Pipeline.Automation.ServerHost.Tasks
{

    /// <summary>
    /// Map exporting server-side task.
    /// </summary>
    internal class MapExportTask :
        MonitoringTaskBase,
        IMonitoringTask
    {
        #region Constants
        private static readonly String NAME = "Map Auto-Export";
        private static readonly String DESC = "Auto-exporting monitoring task.";

        /// <summary>
        /// Parameter for the file listing all the paths to monitor.
        /// </summary>
        private static readonly String PARAM_MONITOR_PATH_FILE = "Path Monitoring File";

        /// <summary>
        /// Parameter for the file listing email addresses to send notification data to.
        /// </summary>
        private static readonly String PARAM_EMAIL_NOTIFICATIONS_FILE = "Notifications File";

        /// <summary>
        /// Parameter for number of hours between job cleans.
        /// </summary>
        private static readonly String PARAM_HOURS_JOB = "Hours to Keep Completed Jobs";

        /// <summary>
        /// Parameter to determine whether to enqueue job on the local server, or onto a remote task
        /// </summary>
        private static readonly String PARAM_ENQUEUE_JOB_REMOTELY = "Enqueue Job Remotely";

        /// <summary>
        /// Parameter for host to send the job to
        /// </summary>
        private static readonly String PARAM_NETWORK_URI = "Map Export Network URI";

        #endregion // Constants

        #region Member Data

        /// <summary>
        /// AP3 processor collection.
        /// </summary>
        IProcessorCollection ProcessorCollection;

        /// <summary>
        /// Log object.
        /// </summary>
        private IUniversalLog Log;

        /// <summary>
        /// Switch to determine whether to enqueue a remote job or a local one
        /// </summary>
        private bool EnqueueJobRemotely;

        /// <summary>
        /// Automation consumer to put 
        /// </summary>
        private AutomationServiceConsumer AutomationConsumer;

        /// <summary>
        /// The host to whom we will send the map export job
        /// </summary>
        private String MapExportNetworkUri;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="server"></param>
        /// <param name="location"></param>
        public MapExportTask(String location, CommandOptions options)
            : this(new String[] { location }, options)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="server"></param>
        /// <param name="locations"></param>
        public MapExportTask(IEnumerable<String> locations, CommandOptions options)
            : base(NAME, DESC, CapabilityType.MapExport, locations, options)
        {            
            this.ProcessorCollection = new ProcessorCollection(this.Config);
            this.Log = LogFactory.CreateUniversalLog("MapExportTask");

            String filename = GetSettingsFilename();

            IDictionary<String, Object> parameters = new Dictionary<String, Object>();
            RSG.Base.Xml.Parameters.Load(filename, ref parameters);
            this.Parameters = parameters;

            if (this.Parameters.ContainsKey(PARAM_NETWORK_URI))
                this.MapExportNetworkUri = (String)this.Parameters[PARAM_NETWORK_URI];

            if (this.Parameters.ContainsKey(PARAM_ENQUEUE_JOB_REMOTELY))
                this.EnqueueJobRemotely = (bool)this.Parameters[PARAM_ENQUEUE_JOB_REMOTELY];

            if (EnqueueJobRemotely)
            {
                Uri automationService = new Uri(MapExportNetworkUri);
                AutomationConsumer = new AutomationServiceConsumer(automationService);
            }

            this.LoadMonitoringLocations();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        public override void Update()
        {
            // Get list of changelists from base class; turn these info Jobs.
            IEnumerable<Changelist> changelists = base.GetSubmittedChangelists();
            if (changelists.Count() > 0)
                this.MaximumChangelist = changelists.First().Number;

#warning DHM FIX ME: using DefaultBranch!!  All tasks need to be branch aware!
            IBranch branch = this.Config.Project.DefaultBranch;
            IContentTree tree = Factory.CreateTree(branch);

            foreach (Changelist change in changelists)
            {
                if (!EnqueueJobRemotely || BuildTokenHelper.HasBuildToken(this.Log, change.Description, BuildToken.EXPORT))
                {
                    IEnumerable<MapExportJob> jobs = MapExportJobFactory.Create(branch, tree, this.P4, change, false);
                    foreach (MapExportJob job in jobs)
                    {
                        if (job != null)
                        {
                            if (EnqueueJobRemotely)
                                AutomationConsumer.EnqueueJob(job);
                            else
                                this.EnqueueJob(job);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Task notification of job being completed.
        /// </summary>
        /// <param name="job"></param>
        public override void JobCompleted(IJob job, IEnumerable<IJobResult> jobResults)
        {
            base.JobCompleted(job, jobResults);
        }

        /// <summary>
        /// File containing information required to send notifications.
        /// </summary>
        /// <returns></returns>
        public override String GetNotificationDataFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_EMAIL_NOTIFICATIONS_FILE]));
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Method to clean out jobs and their files.
        /// </summary>
        private void CleanJobs()
        {
            int hours = (int)this.Parameters[PARAM_HOURS_JOB];
            DateTime modifiedTime = DateTime.UtcNow - TimeSpan.FromHours((double)hours);

            // We now just delete any job that has existed for the specified time rather than
            // specifically just completed jobs.
            foreach (IJob job in this.Jobs)
            {
                DateTime completionTime = (job.ProcessedAt + job.ProcessingTime);
                if (completionTime.IsEarlierThan(modifiedTime))
                {
                    CleanJob(job);
                }
            }
        }

        /// <summary>
        /// Remove job from our queue and clean necessary files.
        /// </summary>
        /// <param name="job"></param>
        private void CleanJob(IJob job)
        {
            lock (this.m_Jobs)
            {
                IEnumerable<KeyValuePair<JobPriority, IJob>> jobEntries =
                    this.m_Jobs.Where(j => j.Value.ID.Equals(job.ID));
                if (jobEntries.Count() > 0)
                    this.m_Jobs.Remove(jobEntries.First());
            }
            Services.FileTransferService.CleanJobFiles(this.Config, job, this.Log);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private String GetSettingsFilename()
        {
            return (SIO.Path.Combine(this.Config.ToolsConfig, "automation",
                "tasks", String.Format("{0}.xml", this.GetType().Name)));
        }

        /// <summary>
        /// 
        /// </summary>
        private void LoadMonitoringLocations()
        {
            (this.MonitoredPaths as List<String>).Clear();
            String monitoringFile = this.Config.Environment.Subst((String)this.Parameters[PARAM_MONITOR_PATH_FILE]);
            if (!SIO.File.Exists(monitoringFile) && this.MonitoredPaths.Count() == 0)
            {
                this.Log.ErrorCtx(LOG_CTX, "Monitoring file does not exist ({0}).  Map Export task will not be monitoring any locations.", monitoringFile);
            }

            XDocument xdoc = XDocument.Load(monitoringFile);
            List<String> locations = new List<String>();
            foreach (XElement elem in xdoc.Descendants("location"))
            {
#warning TODO LPXO: Add some mechanism for removing paths, similar to perforce workspace capabilities
                String path = elem.Attribute("path").Value;
                locations.Add(Options.Branch.Environment.Subst(path));
            }

            FileMapping[] fms = FileMapping.Create(this.P4, locations.ToArray());
            (this.MonitoredPaths as List<String>).AddRange(fms.Select(fm => fm.DepotFilename));
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.ServerHost.Tasks namespace
