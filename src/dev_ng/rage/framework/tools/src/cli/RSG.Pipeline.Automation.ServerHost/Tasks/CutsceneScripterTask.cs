﻿using System;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Common.JobManagement;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Services;
using RSG.Pipeline.Automation.Common.Batch;
using RSG.Pipeline.Automation.Common.Services.Messages;

namespace RSG.Pipeline.Automation.ServerHost.Tasks
{
    /// <summary>
    /// Asset Builder server-side monitoring task.
    /// </summary>
    internal class CutsceneScripterTask : 
        MonitoringTaskBase,
        IMonitoringTask,
        IJobManagedTask
    {
        #region Constants
        /// <summary>
        /// The task name.
        /// </summary>
        private static readonly String NAME = "Cutscene Scripter";

        /// <summary>
        /// The task description.
        /// </summary>
        private static readonly String DESC = "Cutscene Scripter monitoring task.";

        /// <summary>
        /// The task log context.
        /// </summary>
        private static readonly String LOG_CTX = "Automation:CutsceneScripterTask";

        /// <summary>
        /// Parameter for the file listing all the paths to monitor.
        /// </summary>
        private static readonly string PARAM_MONITOR_PATH_FILE = "Path Monitoring File";

        /// <summary>
        /// Parameter for the file listing email addresses to send notification data to.
        /// </summary>
        private static readonly string PARAM_EMAIL_NOTIFICATIONS_FILE = "Notifications File";

        /// <summary>
        /// Parameter for number of hours between job cleans.
        /// </summary>
        private static readonly String PARAM_HOURS_JOB = "Hours to Keep Completed Jobs";

        /// <summary>
        /// Parameter to allow ANY project to be monitored
        /// </summary>
        private static readonly String PARAM_MONITOR = "Monitor";

        #endregion // Constants

        #region Member Data        
        /// <summary>
        /// 
        /// </summary>
        public DispatchScheme DispatchScheme { get; set; }

        /// <summary>
        /// Stored to know when this file is modified
        /// </summary>
        private DateTime MonitoringFileLastModified { get; set; }
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="location"></param>
        public CutsceneScripterTask(CommandOptions options)
            : this(new String[] { String.Empty }, options)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="locations"></param>
        public CutsceneScripterTask(IEnumerable<String> locations, CommandOptions options)
            : base(NAME, DESC, CapabilityType.CutsceneScripter, locations, options)
        {
            String filename = GetSettingsFilename();

            IDictionary<String, Object> parameters = new Dictionary<String, Object>();
            RSG.Base.Xml.Parameters.Load(filename, ref parameters);
            this.Parameters = parameters;

            this.MonitoringFileLastModified = DateTime.MinValue;
            this.LoadMonitoringLocations();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        public override void Update()
        {
            // Only do job maintainance; cleaning the queue and associated files.
            this.CleanJobs();

            // Reload all parameters
            String filename = GetSettingsFilename();
            IDictionary<String, Object> parameters = new Dictionary<String, Object>();
            RSG.Base.Xml.Parameters.Load(filename, ref parameters);
            this.Parameters = parameters;

            /*// Allows us to update monitoring locations without restarting the service.
            this.LoadMonitoringLocations();

            // Get list of changelists from base class; turn these info Jobs.
            IEnumerable<Changelist> changelists = base.GetSubmittedChangelists();
            if (changelists.Count() > 0)
                this.MaximumChangelist = changelists.Select(changelist => changelist.Number).Max();

            foreach (Changelist changelist in changelists)
            {
                Log.MessageCtx(LOG_CTX, "Considering changelist #{0} submitted by '{1}' at {2}, which contains {3} files", 
                    changelist.Number, changelist.Username, changelist.Time.ToString(), changelist.Files.Count());

                if (changelist.Files.Count() == 0)
                {
                    Log.WarningCtx(LOG_CTX, "Empty changelist {0}: {1}.  Ignoring.", changelist.Number, changelist.Description);
                    continue;
                }

                // Ensure we only process files within a changelist that should be monitored.
                List<String> monitoredFiles = new List<String>();
                foreach(String monitoredPath in this.MonitoredPaths)
                    monitoredFiles.AddRange(changelist.Files.Where(f => f.ToLower().StartsWith(monitoredPath.ToLower())));

                IEnumerable<FileMapping> changeListFiles = FileMapping.Create(this.P4, monitoredFiles.ToArray());

                Dispatch(changelist, changeListFiles);
            }*/
        }

        /// <summary>
        /// File containing information required to send notifications.
        /// </summary>
        /// <returns></returns>
        public override String GetNotificationDataFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_EMAIL_NOTIFICATIONS_FILE]));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="machinename"></param>
        /// <returns></returns>
        public override IJob GetSuitableJob(string machinename)
        {
            if (!base.CanGetSuitableJob(machinename))
                return null;

            // Here we have a 'fallback' client, which processes anything left over.
            // So, we find the next job that has no dispatch group interested in it
            lock (this.m_Jobs)
            {
                foreach (IJob job in this.PendingJobs.Where(j => j.Trigger is FilesTrigger).ToList())
                {
                    return job;
                }
            }

            return null;
        }

        /// <summary>
        /// Returns location of dispatch scheme data file.
        /// </summary>
        /// <returns></returns>
        public String GetJobManagementFile()
        {
            return String.Empty;
        }

        /// <summary>
        /// Task notification of job being completed.
        /// </summary>
        /// <param name="job"></param>
        public override void JobCompleted(IJob job, IEnumerable<IJobResult> jobResults)
        {
            base.JobCompleted(job, jobResults);
        }

        #endregion // Controller Methods

        #region Private Methods

        /// <summary>
        /// Method to clean out jobs and their files.
        /// </summary>
        private void CleanJobs()
        {
            int hours = (int)this.Parameters[PARAM_HOURS_JOB];
            DateTime modifiedTime = DateTime.UtcNow - TimeSpan.FromHours((double)hours);

            // We now just delete any job that has existed for the specified time rather than
            // specifically just completed jobs.
            lock (this.m_Jobs)
            {
                foreach (IJob job in this.Jobs)
                {
                    DateTime completionTime = (job.ProcessedAt + job.ProcessingTime);
                    if (completionTime.IsEarlierThan(modifiedTime))
                    {
                        CleanJob(job);
                    }
                }

                // B*1377109 - Remove any job results for jobs that no longer exist
                SortedSet<Guid> jobIDs = new SortedSet<Guid>(this.Jobs.Select(job => job.ID));
                lock (this.m_JobResults)
                {
                    IJobResult[] resultsToRemove = m_JobResults.Where(jobResult => !jobIDs.Contains(jobResult.JobId)).ToArray();
                    foreach (IJobResult resultToRemove in resultsToRemove)
                    {
                        this.m_JobResults.Remove(resultToRemove);
                    }
                }
            }
        }

        /// <summary>
        /// Remove job from our queue and clean necessary files.
        /// </summary>
        /// <param name="job"></param>
        private void CleanJob(IJob job)
        {
            lock (this.m_Jobs)
            {
                IEnumerable<KeyValuePair<JobPriority, IJob>> jobEntries =
                    this.m_Jobs.Where(j => j.Value.ID.Equals(job.ID));
                if (jobEntries.Count() > 0)
                    this.m_Jobs.Remove(jobEntries.First());
            }
            Services.FileTransferService.CleanJobFiles(this.Config, job, this.Log);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private String GetSettingsFilename()
        {
            return (Path.Combine(this.Config.ToolsConfig, "automation",
                "tasks", String.Format("{0}.xml", this.GetType().Name)));
        }

        /// <summary>
        /// 
        /// </summary>
        private void LoadMonitoringLocations()
        {
            (this.MonitoredPaths as List<String>).Clear();
            String monitoringFile = this.Config.Environment.Subst((String)this.Parameters[PARAM_MONITOR_PATH_FILE]);
            if (!File.Exists(monitoringFile) && this.MonitoredPaths.Count() == 0)
            {
                this.Log.ErrorCtx(LOG_CTX, "Monitoring file does not exist ({0}).  Asset builder task will not be monitoring any locations.", monitoringFile);
            }

            // Cache monitoring path last modified time
            DateTime monitoringFileModified = System.IO.File.GetLastWriteTime(monitoringFile);
            bool showMonitoringPaths = this.MonitoringFileLastModified != monitoringFileModified;
            if (showMonitoringPaths && this.MonitoringFileLastModified != DateTime.MinValue)
            {
                this.Log.MessageCtx(LOG_CTX, " ");
                this.Log.MessageCtx(LOG_CTX, "*** Monitoring paths may have been modified : {0} ***", monitoringFile);
                this.Log.MessageCtx(LOG_CTX, " ");
            }

            this.MonitoringFileLastModified = monitoringFileModified;

            XDocument xdoc = XDocument.Load(monitoringFile);

            // Read all locations
            ICollection<String> locationsDeserialised = new List<String>();

            foreach (XElement elem in xdoc.Descendants("location"))
            {
#warning TODO LPXO: Add some mechanism for removing paths, similar to perforce workspace capabilities
                String path = elem.Attribute("path").Value;
                if (showMonitoringPaths)
                {
                    this.Log.MessageCtx(LOG_CTX, "Monitoring Path: {0}", path);
                }
                locationsDeserialised.Add(path);
            }

            // For each deserialised location translate to appropriate location(s)
            ICollection<String> coreLocations = new List<String>();
            foreach (String location in locationsDeserialised)
            {
                String branchName = this.Options.Branch.Name;

                Debug.Assert(this.Options.CoreProject.Branches.ContainsKey(branchName),
                    "Core project does not have branch '{0}' defined.", branchName);
                if (!this.Options.CoreProject.Branches.ContainsKey(branchName))
                    throw (new ArgumentException(String.Format("Core project does not have branch '{0}' defined.", branchName)));

                string rootProjectLocation = this.Options.CoreProject.Branches[branchName].Environment.Subst(location);
                if (showMonitoringPaths)
                {
                    this.Log.MessageCtx(LOG_CTX, "Core project evaluated monitoring path: {0}", rootProjectLocation);
                }
                coreLocations.Add(rootProjectLocation);
            }

            // Join all core & dlc locations
            ICollection<String> locations = new List<String>();
            locations.AddRange(coreLocations);

            if (locations.Any())
            {
                FileMapping[] fms = FileMapping.Create(this.P4, locations.Distinct().ToArray());
                (this.MonitoredPaths as List<String>).AddRange(fms.Select(fm => fm.DepotFilename));
            }
            else
            {
                this.Log.ErrorCtx(LOG_CTX, "There are no monitoring locations, please examine your monitoring file {0}", monitoringFile);
            }
        }

        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.ServerHost.Tasks namespace
