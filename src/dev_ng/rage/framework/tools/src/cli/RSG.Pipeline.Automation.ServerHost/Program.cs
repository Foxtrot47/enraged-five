﻿using System;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Threading;
using System.IO;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.Net;
using RSG.Base.OS;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Services;
using RSG.Pipeline.Automation.Common.Client;
using RSG.Pipeline.Core;
using RSG.Pipeline.Automation.Common.Services.Messages;
using RSG.Pipeline.Automation.ServerHost.WCFExtensions;

namespace RSG.Pipeline.Automation.ServerHost
{

    /// <summary>
    /// Main program class.
    /// </summary>
    class Program
    {
        #region Constants
        private static readonly String OPT_ROLE = "role";
        private static readonly String OPT_ROLE_DEFAULT = "none";
        private static readonly String OPT_ROLE_MAPEXPORT = "mapexport";
        private static readonly String OPT_ROLE_CUTSCENEEXPORT = "cutsceneexport";
        private static readonly String OPT_ROLE_ASSETBUILDER = "assetbuilder";
        private static readonly String OPT_ROLE_MAPEXPORT_NETWORK = "mapexport_network";
        private static readonly String OPT_ROLE_CUTSCENEEXPORT_NETWORK = "cutsceneexport_network";
        private static readonly String OPT_ROLE_CUTSCENEPACKAGE = "cutscenepackage";
        private static readonly String OPT_ROLE_MAPGTXDTASK = "mapgtxdexport";
        private static readonly String OPT_ROLE_CODEBUILDER = "codebuilder";
        private static readonly String OPT_ROLE_TEST = "test";
        private static readonly String OPT_ROLE_INTEGRATOR = "integrator";
        private static readonly String OPT_ROLE_CODEBUILDER2 = "codebuilder2";        
        private static readonly String OPT_ROLE_SMOKETESTER = "smoketester";
        private static readonly String OPT_ROLE_SCRIPTBUILDER = "scriptbuilder";        
        private static readonly String OPT_ROLE_CUTSCENESCRIPTER = "cutscenescripter";
        private static readonly String OPT_ROLE_COMMANDRUNNER = "commandrunner";
        private static readonly String OPT_ROLE_TEXTUREPROCESSOR = "textureprocessor";
        private static readonly String OPT_ROLE_FACEANIMATIONPROCESSOR = "faceanimationprocessor";
        private static readonly String OPT_ROLE_NAVMESHGENERATOR = "navmeshgenerator";
        private static readonly String OPT_ROLE_PTFXPACKAGE = "ptfxpackage";
        private static readonly String OPT_ROLE_DATABASEMANAGER = "databasemanager";
        private static readonly String OPT_ROLE_CUTSCENERPFBUILDER = "cutscenerpfbuilder";
        private static readonly String OPT_ROLE_FRAMECAPTURE = "framecapture";
        
        private static readonly String OPT_WEB_ROOT = "webroot";

        private static readonly String OPT_DISABLE_CONFIG_SYNC = "disable_config_sync";
        #endregion // Constants

        #region Member Data
        private static Services.AutomationService m_AutomationService;
        private static ServiceHost m_HostAutomation;
        private static ServiceHost m_HostFileTransfer;
        private static IUniversalLog log;
        #endregion // Member Data

        /// <summary>
        /// Entry-point.
        /// </summary>
        /// <param name="args"></param>
        [STAThread]
        static int Main(String[] args)
        {           
            try
            {
                LogFactory.Initialize();

                LongOption[] opts = new LongOption[] {
                    new LongOption(OPT_ROLE, LongOption.ArgType.Required,
                        "Automation role."),
                    new LongOption(OPT_WEB_ROOT, LongOption.ArgType.Required,
                        "Automation Monitor web-service root directory."),
                    new LongOption(OPT_DISABLE_CONFIG_SYNC, LongOption.ArgType.Optional,
                        "Optionally disable tools config syncing.")
                };
                CommandOptions options = new CommandOptions(args, opts);
                options.NoPopups = true;

                String webRootOverride = options.ContainsOption(OPT_WEB_ROOT) ?
                    (String)options[OPT_WEB_ROOT] : null;
                String roleArg = options.ContainsOption(OPT_ROLE) ?
                    (String)options[OPT_ROLE] : OPT_ROLE_DEFAULT;
                String[] roles = roleArg.Split(new char[] { ',', ';', '.' });
                CapabilityType capability = DeriveCapabilitiesFromRoles(roles);
               
                bool enableConfigSync = options.ContainsOption(OPT_DISABLE_CONFIG_SYNC) ? false : true;
                InitialiseConsole(capability);


                log.Message("Web Server Startup");
                // Start up the web service
                RSG.Base.Configuration.IConfig config = options.Config;
                if (webRootOverride != null)
                {
                    WebService.WebRootDir = webRootOverride;
                }
                else
                {
                    WebService.WebRootDir = Path.Combine(config.ToolsRoot, "web", "release");
                }
                WebService.SetConfigDir(config.ToolsConfig);

                Thread webUpdateThread = new Thread(new ThreadStart(WebService.Start) )
                { 
                    IsBackground = true
                };

                webUpdateThread.Start();

                log.Message("WCF Host Initialisation.");
                m_AutomationService = new Services.AutomationService(options, config, capability, enableConfigSync);
                m_HostAutomation = new CorsEnabledServiceHost(m_AutomationService);

                foreach (System.Uri baseAddress in m_HostAutomation.BaseAddresses)
                    log.Message("Automation Service hosted at {0}", baseAddress.AbsoluteUri);

                m_HostFileTransfer = new ServiceHost(typeof(Services.FileTransferService));
                foreach (System.Uri baseAddress in m_HostFileTransfer.BaseAddresses)
                    log.Message("File Transfer Service hosted at {0}", baseAddress.AbsoluteUri);

                log.Message("WCF Host Startup.");

                try
                {
                    m_HostAutomation.Open();
                    m_HostFileTransfer.Open();

                    m_AutomationService.Start();
                }
                catch (AddressAlreadyInUseException e)
                {
                    // This is to handle the case were there are 2 instances of the server trying to run. There is
                    // and edge case which the port doesn't get released and you have to restart the machine.
                    log.ToolException(e, "Port already in use : please close any other instances of the server process. If this persists a machine reboot will be required.");
                    log.Warning("Press Enter to terminate the server");
                    Console.ReadLine();
                    throw e;
                }
            }
            catch (Exception ex)
            {
                log.Exception(ex, "Hosting exception");
                m_AutomationService.ExceptionAlert(log, ex, string.Format("Top Level Hosting Exception: {0}", Environment.MachineName));
            }
            finally
            {
                Shutdown(ShutdownType.WorkersAndHost);
            }

            int exitCode = ComputeExitCode();

            log.Message("Shutting down with exit code {0}", exitCode);

            // ***************************************************************************
            // IMPORTANT (DO NOT REMOVE) : Main thread sleeps before exiting process. 
            Thread.Sleep(1000);
            // ***************************************************************************

            return (exitCode);
        }

        /// <summary>
        /// For a list of roles derive the capabilities.
        /// </summary>
        /// <param name="roles"></param>
        /// <returns></returns>
        private static CapabilityType DeriveCapabilitiesFromRoles(String[] roles)
        {
            CapabilityType capability = CapabilityType.None;

            foreach (String role in roles)
            {
                if (0 == String.Compare(role, OPT_ROLE_MAPEXPORT))
                {
                    capability |= CapabilityType.MapExport;
                    // capability |= CapabilityType.UserDetails;
                }
                else if (0 == String.Compare(role, OPT_ROLE_MAPEXPORT_NETWORK))
                {
                    capability |= CapabilityType.MapExportNetwork;
                    // capability |= CapabilityType.UserDetails;
                }
                else if (0 == String.Compare(role, OPT_ROLE_ASSETBUILDER))
                {
                    capability |= CapabilityType.AssetBuilder;
                    capability |= CapabilityType.AssetBuilderRebuilds;
                    //capability |= CapabilityType.UserDetails;
                }
                else if (0 == String.Compare(role, OPT_ROLE_CUTSCENEEXPORT))
                {
                    capability |= CapabilityType.CutsceneExport;
                }
                else if (0 == String.Compare(role, OPT_ROLE_CUTSCENEEXPORT_NETWORK))
                {
                    capability |= CapabilityType.CutsceneExportNetwork;
                    // capability |= CapabilityType.UserDetails;
                }
                else if (0 == String.Compare(role, OPT_ROLE_TEST))
                {
                    capability |= CapabilityType.SchedulingUnitTesting;
                }
                else if (0 == String.Compare(role, OPT_ROLE_CUTSCENEPACKAGE))
                    capability |= CapabilityType.CutscenePackage;
                else if (0 == String.Compare(role, OPT_ROLE_MAPGTXDTASK))
                    capability |= CapabilityType.MapGTXDExportTask;
                else if (0 == String.Compare(role, OPT_ROLE_CODEBUILDER))
                    capability |= CapabilityType.CodeBuilder;
                else if (0 == String.Compare(role, OPT_ROLE_INTEGRATOR))
                {
                    capability |= CapabilityType.Integrator;
                }
                else if (0 == String.Compare(role, OPT_ROLE_CODEBUILDER2))
                {
                    capability |= CapabilityType.CodeBuilder2;
                }
                else if (0 == String.Compare(role, OPT_ROLE_SCRIPTBUILDER))
                {
                    capability |= CapabilityType.ScriptBuilder;
                }
                else if (0 == String.Compare(role, OPT_ROLE_SMOKETESTER))
                {
                    capability |= CapabilityType.SmokeTester;
                }
                else if (0 == String.Compare(role, OPT_ROLE_COMMANDRUNNER))
                {
                    capability |= CapabilityType.CommandRunner;
                }
                else if (0 == String.Compare(role, OPT_ROLE_DATABASEMANAGER))
                {
                    capability |= CapabilityType.DatabaseManager;
                }
                else if (0 == String.Compare(role, OPT_ROLE_CUTSCENERPFBUILDER))
                {
                    capability |= CapabilityType.CutSceneRPFBuilder;
                }
                else if (0 == String.Compare(role, OPT_ROLE_FRAMECAPTURE))
                {
                    capability |= CapabilityType.FrameCapture;
                }
                else if (0 == String.Compare(role, OPT_ROLE_CUTSCENESCRIPTER))
                {
                    capability |= CapabilityType.CutsceneScripter;
                }
            }
            return capability;
        }

        private static int ComputeExitCode()
        {
            int exitCode = Constants.Exit_Success;
            if (m_AutomationService.ShutdownType.HasFlag(ShutdownType.Resume))
            {
                exitCode |= Constants.Exit_Resume;
            }

            if (m_AutomationService.ShutdownType.HasFlag(ShutdownType.ScmSyncHead))
            {
                exitCode |= Constants.Exit_SyncHead;
            }

            if (m_AutomationService.ShutdownType.HasFlag(ShutdownType.ScmSyncLabel))
            {
                exitCode |= Constants.Exit_SyncLabel;
            }

            if (m_AutomationService.ShutdownType.HasFlag(ShutdownType.ScmSyncConfig))
            {
                exitCode |= Constants.Exit_SyncConfig;
            }

            if (m_AutomationService.ShutdownType.HasFlag(ShutdownType.ScmSyncContent))
            {
                exitCode |= Constants.Exit_SyncContent;
            }

            if (m_AutomationService.ShutdownType.HasFlag(ShutdownType.CleanCache))
            {
                exitCode |= Constants.Exit_CleanCache;
            }

            log.Message("Shutting down with exit code {0}", exitCode);
            m_AutomationService.ShutdownAlert(log, m_AutomationService.ShutdownType, String.Format("Shutdown Complete: {0}", Environment.MachineName));

            // ***************************************************************************
            // IMPORTANT (DO NOT REMOVE) : Main thread sleeps before exiting process. 
            Thread.Sleep(1000);
            // ***************************************************************************

            return exitCode;
        }

        /// <summary>
        /// Initialise console display.
        /// </summary>
        static void InitialiseConsole(CapabilityType role)
        {
            var descriptionAttribute = Assembly.GetEntryAssembly()
                    .GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false)
                    .OfType<AssemblyDescriptionAttribute>()
                    .FirstOrDefault();

            if (descriptionAttribute != null)
                Console.Title = String.Format("{0} Role: {1}",
                    descriptionAttribute.Description, role);
            log = LogFactory.CreateUniversalLog("Automation_ServerHost");
            LogFactory.CreateApplicationConsoleLogTarget();
            UniversalLogFile logfile = LogFactory.CreateUniversalLogFile(log) as UniversalLogFile;
            Console.CancelKeyPress += ShutdownKill;
        }

        /// <summary>
        /// 
        /// </summary>
        static void Shutdown(ShutdownType shutdownType)
        {
            try
            {

                log.Message("Stopping services.");
                WebService.Stop();
                m_AutomationService.Stop(shutdownType);
                log.Message("WCF Host Shutdown.");
                m_HostFileTransfer.Close();
                m_HostAutomation.Close();
            }
            catch (Exception e)
            {
                // Sometimes the objects can be disposed by the time they get to this point.
                // This is mainly the case when the port is in use by another instance/application
                // and the server fails to bind. 
                log.ToolException(e, "Server host encountered an error while shutting down.");
            }
        }

        #region Event Handlers
        /// <summary>
        /// Ctrl+C Console Handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void ShutdownKill(Object sender, EventArgs e)
        {
            log.Warning("Host process being killed; WCF host cleanup...");
            Shutdown(ShutdownType.WorkersAndHost);
        }
        #endregion // Event Handlers
    }

} // RSG.Pipeline.Automation.ServerHost namespace
