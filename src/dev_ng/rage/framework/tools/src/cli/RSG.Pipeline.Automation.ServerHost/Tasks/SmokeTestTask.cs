﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Pipeline.Automation.Consumers.Clients;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Export;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Services;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Services;
using RSG.Platform;
using RSG.Statistics.Common;
using RSG.SourceControl.Perforce;

namespace RSG.Pipeline.Automation.ServerHost.Tasks
{

    /// <summary>
    /// Task that looks for modified files; once found, launches a job for a client to analyze the modified files
    /// </summary>
        public class SmokeTestTask :
        TaskBase
    {
        #region Constants
        private static readonly String NAME = "Smoke Test";
        private static readonly String DESC = "Monitors file locations.  When files change, launches a job for client analysis.";
        private static readonly String LOG_CTX = "Smoke Test Task";

        private static readonly String PARAM_HOURS_JOB = "Hours to Keep Completed Jobs";
        private static readonly String PARAM_EMAIL_NOTIFICATIONS_FILE = "Notifications File";
        private static readonly String PARAM_MONITOR_PATH_FILE = "Path Monitoring File";
        private static readonly String PARAM_BRANCH = "Branch";
        private static readonly String PARAM_JOB_DEFINITION_FILE = "JobDef Build Combinations";
        private static readonly String PARAM_UPDATE_SLEEP_TIME = "Update Sleep Time";
        private static readonly String PARAM_SYNC_FILES = "Sync Files";
        private static readonly String PARAM_FORCE_MONITORED_FILE_FOUND = "Force Monitored File Found";
        #endregion // Constants


        #region Member Data
        /// <summary>
        /// Task parameters.
        /// </summary>
        ///
        private String Branch;
        private IDictionary<String, Object> Parameters;
        private DateTime LastMonitoringFileWriteTime;
        private int JobLock; // Forces us to wait until all jobs dispatched at once finish before taking on more
        private int UpdateSleepTime;
        private bool SyncFiles;
        private bool ForceMonitoredFileFound;

        #endregion // Member Data

        #region Properties
        /// <summary>
        /// path and filename for each executable
        /// </summary>
        public IList<String> MonitoredFiles { get; set; }

        /// <summary>
        /// CL for the source file changes to generate each monitored file
        /// </summary>
        public IList<uint> MonitoredFilesCLs { get; set; }

        /// <summary>
        /// CL for the executables being built based on the CL of the same index in MonitoredFilesCLs
        /// </summary>
        public IList<uint> MonitoredFilesSubmittedAsCLs { get; set; }

        /// <summary>
        /// Write times for each executable.
        /// </summary>
        public IList<DateTime> LastWriteTime { get; set; }
        #endregion //Properties

        #region Constructor(s)

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SmokeTestTask(CommandOptions options)
            : base(NAME, DESC, Common.CapabilityType.SmokeTester, options)
        {
            this.Parameters = new Dictionary<String, Object>();
            LastMonitoringFileWriteTime = DateTime.MinValue;

            JobLock = 0;

            LastWriteTime = new List<DateTime>();
            MonitoredFiles = new List<string>();
            MonitoredFilesCLs = new List<uint>();
            MonitoredFilesSubmittedAsCLs = new List<uint>();

            String filename = GetSettingsFilename();
            RSG.Base.Xml.Parameters.Load(filename, ref this.Parameters);

            if (Parameters.ContainsKey(PARAM_BRANCH))
            {
                this.Branch = (String)Parameters[PARAM_BRANCH];
                if (!Config.Project.Branches.ContainsKey(this.Branch))
                {
                    this.Log.WarningCtx(LOG_CTX, "Branch {0} not found.  Using DefaultBranch {1}",
                                     this.Branch, this.Config.Project.DefaultBranchName);
                    this.Branch = "dev";
                }
            }
            else
            {
                this.Branch = "dev";
            }

            if (Parameters.ContainsKey(PARAM_SYNC_FILES))
            {
                this.SyncFiles = Convert.ToBoolean(Parameters[PARAM_SYNC_FILES]);
            }
            else
            {
                this.SyncFiles = true;
            }

            if (Parameters.ContainsKey(PARAM_FORCE_MONITORED_FILE_FOUND))
            {
                this.ForceMonitoredFileFound = Convert.ToBoolean(Parameters[PARAM_FORCE_MONITORED_FILE_FOUND]);
            }
            else
            {
                this.ForceMonitoredFileFound = false;
            }

            if (Parameters.ContainsKey(PARAM_UPDATE_SLEEP_TIME))
            {
                this.UpdateSleepTime = Convert.ToInt32(Parameters[PARAM_UPDATE_SLEEP_TIME]);
            }
            else
            {
                this.UpdateSleepTime = 0;
            }
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Task update tick (not required for this task).
        /// </summary>
        public override void Update()
        {
            // Allows us to update monitoring locations without restarting the service.
            this.LoadMonitoringLocations();

            SyncMonitoredFiles();

            // determine if a monitored exe has been updated; 
            // if so, create a task to process that exe and wait for it to process
            for (int i = 0; i < MonitoredFiles.Count; i++)
            {
                FileInfo fileInfo = new FileInfo(MonitoredFiles[i]);
                if (fileInfo.Exists)
                {
                    DateTime lastWriteTime = fileInfo.LastWriteTime;

                    //dev switch to keep pumping "new" files
                    if (ForceMonitoredFileFound)
                    {
                        lastWriteTime = this.LastWriteTime[i].AddMinutes(5);
                        //@TODO awful; new trigger requires files
                        if (MonitoredFilesCLs[i] == 0)
                        {
                            MonitoredFilesCLs[i] = MonitoredFilesSubmittedAsCLs[i] = 5370843;
                        }
                    }

                    Log.MessageCtx(LOG_CTX, "Update: MonitoredFiles found {0} @ {1}", MonitoredFiles[i], lastWriteTime);

                    if (lastWriteTime > this.LastWriteTime[i])
                    {
                        if (!this.SyncFiles || this.MonitoredFilesCLs[i] != 0)
                        {
                            Log.MessageCtx(LOG_CTX, "New exe found {0}.  Creating Smoke Test Task", MonitoredFiles[i]);

                            this.LastWriteTime[i] = lastWriteTime;

                            GenerateJobsFromMonitoring(MonitoredFiles[i], MonitoredFilesCLs[i], MonitoredFilesSubmittedAsCLs[i]);

                            //reset our monitored CL so we'll detect it again next time
                            MonitoredFilesCLs[i] = 0;
                        }
                    }
                }
                else
                {
                    this.Log.WarningCtx(LOG_CTX, "MonitoredFile {0} not found", MonitoredFiles[i]);
                }
            }

            // don't spam perforce
            this.Log.MessageCtx(LOG_CTX, "Waiting {0} ms...", this.UpdateSleepTime);
            System.Threading.Thread.Sleep(this.UpdateSleepTime);

            // Job maintainance; cleaning the queue and associated files.
            this.CleanJobs();
        }

        /// <summary>
        /// SyncMonitoredFiles
        /// </summary>
        public void SyncMonitoredFiles()
        {
            try
            {
                using (P4 p4 = Options.Project.SCMConnect())
                {
                    for (int i = 0; i < MonitoredFiles.Count; i++)
                    {
                        //@TODO force?
                        String syncFileCmd = String.Format("{0}{1}", MonitoredFiles[i], @"#head");

                        if (this.SyncFiles)
                        {
                            this.Log.MessageCtx(LOG_CTX, "SyncP4 {0}", syncFileCmd.ToString());
                            P4API.P4RecordSet syncResults = p4.Run("sync", syncFileCmd);

                            // do we already have the latest version?
                            if (syncResults.Records.Count() == 0)
                            {
                                this.Log.MessageCtx(LOG_CTX, "SyncP4 complete. File {0} up-to-date", MonitoredFiles[i]);
                            }
                            else
                            {
                                this.Log.MessageCtx(LOG_CTX, "SyncP4 complete. File {0} updated", MonitoredFiles[i]);
                            }

                            //lookup the CL's if we haven't already
                            if (MonitoredFilesCLs[i] == 0)
                            {
                                //find the CL that was used to generate this updated file
                                P4API.P4RecordSet changesResult = p4.Run("changes", "-m", "1",
                                                                              MonitoredFiles[i]);

                                if (changesResult.Records.Count() >= 1)
                                {
                                    this.MonitoredFilesSubmittedAsCLs[i] = uint.Parse(changesResult.Records[0]["change"]);
                                    this.Log.MessageCtx(LOG_CTX, "P4 changes complete.  New executable submitted as CL {0}",
                                                            this.MonitoredFilesSubmittedAsCLs[i]);

                                    //read the description of that CL to find the CL that resulted in this monitored file
                                    P4API.P4RecordSet describeResults = p4.Run("describe", "-s",
                                                                        this.MonitoredFilesSubmittedAsCLs[i].ToString());
                                    this.Log.MessageCtx(LOG_CTX, "P4 describe complete {0}", describeResults.Records[0]["desc"]);

                                    //decode the CL number from the submission description
                                    String[] description = describeResults.Records[0]["desc"].Split('.');
                                    uint basedOnCL = 0;
                                    foreach (String str in description)
                                    {
                                        if (str.StartsWith("CL"))
                                        {
                                            //could have a range of CL's"CL3342342-234234234"
                                            String withoutCL = str.Replace("CL", "");
                                            String[] cls = withoutCL.Split('-');

                                            basedOnCL = uint.Parse(cls[cls.Count() - 1]);
                                        }
                                    }
                                    this.Log.MessageCtx(LOG_CTX, "Executable based on CL {0}", basedOnCL);
                                    this.MonitoredFilesCLs[i] = basedOnCL;
                                }
                                else
                                {
                                    this.Log.WarningCtx(LOG_CTX, "Could not get the changes for file {0}. Are we logged in to P4?", MonitoredFiles[i]);
                                    this.MonitoredFilesCLs[i] = 0;
                                }
                            }                         
                        }
                        else
                        {
                            this.Log.WarningCtx(LOG_CTX, "Not synching due to Sync Files config.  Command:{0}", syncFileCmd.ToString());
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                String exception = String.Format("SyncMonitoredFiles Exception: {0}", ex.ToString());
                this.Log.WarningCtx(LOG_CTX, exception);

                String[] recipients = new String[1] { "sshoemaker@rockstarnewengland.com" };
                String subject = String.Format("SmokeTestServer {0} ex:{1}", System.Environment.MachineName, exception);
                String[] attachment = new String[0];
                String body = String.Format("{0}. Do we need to log in to p4?", subject);

                RSG.UniversalLog.Util.Email.SendEmail(Options.Config, recipients,
                                                      subject, body, attachment);

                this.Log.MessageCtx(LOG_CTX, "Mailing exception to {0}", recipients.ToString());
            }
        }

        /// <summary>
        /// File containing information required to send notifications.
        /// </summary>
        /// <returns></returns>
        public override String GetNotificationDataFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_EMAIL_NOTIFICATIONS_FILE]));
        }

        /// <summary>
        /// Job completed handler.
        /// </summary>
        /// <param name="job"></param>
        public override void JobCompleted(IJob job, IEnumerable<IJobResult> jobResults)
        {
            --JobLock;
        }

        /// <summary>
        /// Loads an XML file that dictates what paths to monitor in Perforce.
        /// </summary>
        private void LoadMonitoringLocations()
        {
            String monitoringFile = this.Config.Environment.Subst((String)this.Parameters[PARAM_MONITOR_PATH_FILE]);
            if (!File.Exists(monitoringFile) && this.MonitoredFiles.Count() == 0)
            {
                this.Log.ErrorCtx(LOG_CTX, "Monitoring file does not exist ({0}).  Smoke Test task will not be monitoring any locations.", monitoringFile);
            }

            if (LastMonitoringFileWriteTime >= File.GetLastWriteTimeUtc(monitoringFile))
            {
                //No need to reload.
                return;
            }
          
            this.Log.MessageCtx(LOG_CTX, "Checking for update in Monitoring Locations...{0}", monitoringFile);

            (this.MonitoredFiles as List<String>).Clear();
            LastMonitoringFileWriteTime = File.GetLastWriteTimeUtc(monitoringFile);

            XDocument xdoc = XDocument.Load(monitoringFile);
            List<String> locations = new List<String>();
            foreach (XElement elem in xdoc.Descendants("location"))
            {
                String path = elem.Attribute("path").Value;

                String exePath = Config.Project.Branches[this.Branch].Environment.Subst(path);

                this.Log.MessageCtx(LOG_CTX, "Adding ...{0}... to monitoring", exePath);
                this.MonitoredFiles.Add(exePath);
                this.LastWriteTime.Add(DateTime.MinValue);
                this.MonitoredFilesCLs.Add(0);
                this.MonitoredFilesSubmittedAsCLs.Add(0);
            }
        }

        /// <summary>
        /// Function parses the RestrictToPlatforms item to create jobs per platform.
        /// </summary>
        /// <returns></returns>
        private List<RSG.Platform.Platform> ParseRestrictedPlatforms(XElement xmlJobElement)
        {
            List<RSG.Platform.Platform> result = new List<Platform.Platform>();
            XElement platformsElement = xmlJobElement.Element("RestrictToPlatforms");
            if (platformsElement != null)
            {
                String str = platformsElement.Value;
                String[] platformStrings = str.Split(new char[] { ' ', ';' });

                if (platformStrings.Length == 0)
                {
                    //Non restricted, add all platforms
                    result.Add(RSG.Platform.Platform.Win64);
                    result.Add(RSG.Platform.Platform.XboxOne);
                    result.Add(RSG.Platform.Platform.PS4);
                }
                else
                {
                    for (int i = 0; i < platformStrings.Length; ++i)
                    {
                        RSG.Platform.Platform platform;
                        if (Enum.TryParse(platformStrings[i], out platform))
                        {
                            result.Add(platform);
                        }
                        else
                        {
                            Log.ErrorCtx(LOG_CTX, "Error creating SmoketestJob from XML. Could not parse '{0}' as a platform in <RestrictToPlatforms>.", platformStrings[i]);
                        }
                    }
                }
            }
            else
            {
                //Non restricted, add all platforms
                result.Add(RSG.Platform.Platform.Win64);
                result.Add(RSG.Platform.Platform.XboxOne);
                result.Add(RSG.Platform.Platform.PS4);
            }
            return result;
        }

        /// <summary>
        /// Function to create all combinations of build possibilities to be turned into jobs. 
        /// </summary>
        /// <param name="exePath"></param>
        /// <param name="changelist"></param>
        /// <param name="submittedChangelist"></param>
        /// <returns></returns>
        private bool GenerateJobsFromMonitoring(String exePath, uint changelist, uint submittedChangelist)
        {
            if (this.Parameters.ContainsKey(PARAM_JOB_DEFINITION_FILE) == true)
            {
                using (P4 p4 = Options.Project.SCMConnect())
                {
                    String jobCombinationsFile = this.Config.Environment.Subst((String)this.Parameters[PARAM_JOB_DEFINITION_FILE]);
                    XDocument xmlDoc = XDocument.Load(jobCombinationsFile);
                    IEnumerable<XElement> xmlJobs = xmlDoc.Descendants("Job");

                    Changelist initialCL = new Changelist(p4, (int)submittedChangelist);
                    RSG.Platform.Platform exePlatform = DetectPlatform(exePath);

                    xmlJobs.ForEach(xmlJob =>
                    {
                        IEnumerable<RSG.Platform.Platform> Platforms = ParseRestrictedPlatforms(xmlJob);
                        foreach (RSG.Platform.Platform platform in Platforms)
                        {
                            if (platform == exePlatform)
                            {
                                BaseSmokeTestJob job = new BaseSmokeTestJob(xmlJob, new ChangelistTrigger(initialCL), CapabilityType.SmokeTester, platform, GetBuildConfigFromExe(exePath), this.Options.Branch);
                                if (String.Compare((String)xmlJob.Attribute("type"), "SmokeTestMetricsJob", true) == 0)
                                {
                                    job = new SmokeTestMetricsJob(xmlJob, CapabilityType.SmokeTester, new ChangelistTrigger(initialCL),
                                                                  platform, GetBuildConfigFromExe(exePath), this.Options.Branch, submittedChangelist, 
                                                                  /*latest code*/0, changelist/*,shelved CL*/, 0);
                                }

                                job.ExePath = exePath;
                                job.Changelist = changelist;
                                job.CodeBuilderChangelist = submittedChangelist;
                                job.SyncFiles = this.SyncFiles;

                                job.CommandLine = job.CommandLine.Replace("[Changelist]", job.Changelist.ToString());
                                job.LogFile = job.LogFile.Replace("[Changelist]", job.Changelist.ToString());

                                ++JobLock;
                                this.EnqueueJob(job);
                            }
                        }
                    }
                    );
                }
            }
            else
            {
                Log.ErrorCtx(LOG_CTX, "Missing SmokeTest_jobdef.xml. Refresh your tools directory.");
                return false;
            }
            return true;
        }      

        /// <summary>
        /// Client requests for a suitable job
        /// </summary>
        /// <param name="machinename"></param>
        /// <returns>a codebuilder2job</returns>
        public override IJob GetSuitableJob(string machinename)
        {
             Log.MessageCtx(LOG_CTX, "Client '{0}' is requesting a suitable job.", machinename.ToLower());

            lock (this.m_Jobs)
            {
                // Get a list of candidate jobs for consuming 
                IEnumerable<SmokeTestMetricsJob> candidateJobs = this.Jobs.
                    OfType<SmokeTestMetricsJob>().
                    Where(j => j.State == JobState.Pending).
                    OrderByDescending(j => j.CreatedAt);

                //@TODO really need to bind older jobs and skip consumption

                // return the newest job that matches our client
                foreach (SmokeTestMetricsJob smoketestJob in candidateJobs)
                {
                    if (smoketestJob.ClientName != null)
                    {
                        //could we replace this with a machine IP addr lookup and then match that to targets?
                        if (String.Equals(smoketestJob.ClientName, machinename, StringComparison.CurrentCultureIgnoreCase))
                        {
                            return smoketestJob;
                        }
                    }
                }
            }
            return null;
        }

        //@TODO move to common 
        private RSG.Platform.Platform DetectPlatform(String exePath)
        {
            RSG.Platform.Platform[] platforms = (RSG.Platform.Platform[])Enum.GetValues(typeof(RSG.Platform.Platform));

            foreach (RSG.Platform.Platform plat in platforms)
            {
                //platform values need to match those ingame in AutoGPUCapture.cpp
                if (exePath.Contains(plat.PlatformToRagebuilderPlatform()))
                {
                    return plat;
                }
            }
            this.Log.ErrorCtx(LOG_CTX, "Platform not found in exePath {0}; defaulting to Independent", exePath);
            return RSG.Platform.Platform.Independent;
        }

        /// <summary>
        /// Parses the exepath for the buildConfiguration
        /// </summary>
        /// <returns></returns>
        private BuildConfig GetBuildConfigFromExe(String exe)
        {
            BuildConfig foundConfig = BuildConfig.Beta;

            if (BuildConfigUtils.TryParseFromFilepath(exe, out foundConfig))
            {
                return foundConfig;
            }
            else
            {
                throw new NotImplementedException(String.Format("Could not find build configuration that matches with the executable: {0}.", exe));
            }
        }

        /// <summary>
        /// Return status information for this task.
        /// </summary>
        /// <returns></returns>
        public override TaskStatus Status()
        {
            return (new TaskStatus(this));
        }

        /// <summary>
        /// Send pre job completion notifications
        /// </summary>
        /// <param name="job"></param>
        /// <param name="workerUri"></param>
        /// <param name="workerGuid"></param>
        public override void SendPreJobNotifications(IJob job, Uri workerUri, Guid workerGuid)
        {
        }
   
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Return settings XML filename.
        /// </summary>
        /// <returns></returns>
        /// This is virtual allowing you to override it if required; although
        /// it's not recommended.
        /// 
        private String GetSettingsFilename()
        {
            return (Path.Combine(this.Config.ToolsConfig, "automation",
                "tasks", String.Format("{0}.xml", this.GetType().Name)));
        }

        /// <summary>
        /// Method to clean out jobs and their files.
        /// </summary>
        private void CleanJobs()
        {
            int hours = (int)this.Parameters[PARAM_HOURS_JOB];
            DateTime modifiedTime = DateTime.UtcNow - TimeSpan.FromHours((double)hours);

            IEnumerable<IJob> completedJobs = this.Jobs.Where(j =>
                JobState.Completed == j.State ||
                JobState.Errors == j.State ||
                JobState.Aborted == j.State);
            foreach (IJob job in completedJobs)
            {
                DateTime completionTime = (job.ProcessedAt + job.ProcessingTime);
                if (completionTime.IsEarlierThan(modifiedTime))
                {
                    CleanJob(job);
                }
            }
        }

        /// <summary>
        /// Remove job from our queue and clean necessary files.
        /// </summary>
        /// <param name="job"></param>
        private void CleanJob(IJob job)
        {
            lock (this.m_Jobs)
            {
                IEnumerable<KeyValuePair<JobPriority, IJob>> jobEntries =
                    this.m_Jobs.Where(j => j.Value.ID.Equals(job.ID));
                if (jobEntries.Count() > 0)
                    this.m_Jobs.Remove(jobEntries.First());
            }
            Services.FileTransferService.CleanJobFiles(this.Config, job, Log);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.ServerHost.Tasks namespace
