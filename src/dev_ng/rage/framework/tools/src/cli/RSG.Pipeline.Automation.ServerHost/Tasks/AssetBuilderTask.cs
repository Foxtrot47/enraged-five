using System;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Common.JobManagement;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Services;
using RSG.Pipeline.Automation.Common.Batch;
using RSG.Pipeline.Automation.Common.Services.Messages;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core.Build;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Engine;
using RSG.Pipeline.Automation.Common.Notifications;
using RSG.Pipeline.Automation.Common.Notifications.Alerts;

namespace RSG.Pipeline.Automation.ServerHost.Tasks
{
    /// <summary>
    /// Enum of job binding states. ( skip consume )
    /// </summary>
    internal enum JobBindMode
    {
        Off,                            // No binding whatsoever
        SourceFilenames,                // Bind on source filename
        ContentTreeOutputFilenames      // Bind on Content Tree Output
    }

    /// <summary>
    /// Asset Builder server-side monitoring task.
    /// </summary>
    internal class AssetBuilderTask : 
        MonitoringTaskBase,
        IMonitoringTask,
        IJobManagedTask,
        IDispatchSchemeTask
    {
        #region Constants
        /// <summary>
        /// The task name.
        /// </summary>
        private static readonly String NAME = "Asset Builder";

        /// <summary>
        /// The task description.
        /// </summary>
        private static readonly String DESC = "Asset Builder monitoring task.";

        /// <summary>
        /// The task log context.
        /// </summary>
        private static readonly String LOG_CTX = "Automation:AssetBuilderTask";

        /// <summary>
        /// Parameter for the file listing all the paths to monitor.
        /// </summary>
        private static readonly string PARAM_MONITOR_PATH_FILE = "Path Monitoring File";

        /// <summary>
        /// Parameter for the file listing email addresses to send notification data to.
        /// </summary>
        private static readonly string PARAM_EMAIL_NOTIFICATIONS_FILE = "Notifications File";

        /// <summary>
        /// Parameter for the file listing job management data.
        /// </summary>
        private static readonly string PARAM_DISPATCH_SCHEME_FILE = "Dispatch Scheme File";

        /// <summary>
        /// Parameter for number of hours between job cleans.
        /// </summary>
        private static readonly String PARAM_HOURS_JOB = "Hours to Keep Completed Jobs";

        /// <summary>
        /// Parameter to allow ANY project to be monitored
        /// </summary>
        private static readonly String PARAM_MONITOR = "Monitor";

        /// <summary>
        /// Parameter to allow DLC projects to be monitored
        /// </summary>
        private static readonly String PARAM_MONITOR_DLC_PROJECTS = "Monitor DLC Projects";

        /// <summary>
        /// Parameter to allow NON-DLC (CORE) projects to be monitored
        /// </summary>
        private static readonly String PARAM_MONITOR_CORE_PROJECT = "Monitor Core Project";

        /// <summary>
        /// Parameter to allow job binding
        /// </summary>
        private static readonly String PARAM_JOB_BINDING = "Job Bind Mode";

        /// <summary>
        /// Parameter to only permit job of the same trigger types to bind.
        /// - this would prevent rebuilds from picking up changelists from users.
        /// </summary>
        private static readonly String PARAM_JOB_BINDING_ONLY_BIND_SIMILAR_TRIGGER_TYPES = "Job Binding Only Bind Similar Trigger Types";
        
        /// <summary>
        /// Some dev controls : sends emails for job bindings
        /// </summary>                                
        private static readonly String PARAM_EMAIL_ASSETBUILDER_JOB_BINDING = "Email job binding";

        /// <summary>
        /// Some dev controls : early outs of the job binding after sent a debug email.
        /// </summary>
        private static readonly String PARAM_DONT_BIND_AFTER_EMAIL_ASSETBUILDER_JOB_BINDING = "Dont bind after email";

        #endregion // Constants

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        public DispatchScheme DispatchScheme { get; set; }

        /// <summary>
        /// Stored to know when this file is modified
        /// </summary>
        private DateTime MonitoringFileLastModified { get; set; }

        /// <summary>
        /// Caching of job output files derived from content tree 
        /// - consider if this cache should be erased when content tree is reloaded?
        /// - Cache is not persistent with restarting the process, it does not aspire to be either.
        /// - No need to clear this cache out, config sync mechanism will restart the process.
        /// </summary>
        private IDictionary<Guid, IEnumerable<String>> JobContentTreeOutputPathsCache { get; set; }

        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="location"></param>
        public AssetBuilderTask(CommandOptions options)
            : this(new String[] { String.Empty }, options)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="locations"></param>
        public AssetBuilderTask(IEnumerable<String> locations, CommandOptions options)
            : base(NAME, DESC, CapabilityType.AssetBuilder, locations, options)
        {
            String filename = GetSettingsFilename();

            IDictionary<String, Object> parameters = new Dictionary<String, Object>();
            RSG.Base.Xml.Parameters.Load(filename, ref parameters);
            this.Parameters = parameters;

            this.MonitoringFileLastModified = DateTime.MinValue;
            this.LoadMonitoringLocations();

            this.DispatchScheme = new DispatchScheme(this.Config, options, this.Log, this.GetJobManagementFile());

            this.JobContentTreeOutputPathsCache = new Dictionary<Guid, IEnumerable<String>>();  
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        public override void Update()
        {
            // Only do job maintainance; cleaning the queue and associated files.
            this.CleanJobs();

            // Reload all parameters
            String filename = GetSettingsFilename();
            IDictionary<String, Object> parameters = new Dictionary<String, Object>();
            RSG.Base.Xml.Parameters.Load(filename, ref parameters);
            this.Parameters = parameters;

            // Allows us to update monitoring locations without restarting the service.
            this.LoadMonitoringLocations();

            // Allow us to update job dispatch data without restarting the service
            lock (this.DispatchScheme)
            {
                bool syncDispatch = this.Parameters.ContainsKey(PARAM_SYNC_DISPATCH) && (bool)this.Parameters[PARAM_SYNC_DISPATCH];
                this.DispatchScheme.LoadDispatchData(this.GetJobManagementFile(), this.Options, syncDispatch);
                AssignProcessingHostForPendingJobs();
            }

            // Get list of changelists from base class; turn these info Jobs.
            IEnumerable<Changelist> changelists = base.GetSubmittedChangelists();
            if (changelists.Count() > 0)
                this.MaximumChangelist = changelists.Select(changelist => changelist.Number).Max();

            foreach (Changelist changelist in changelists)
            {
                Log.MessageCtx(LOG_CTX, "Considering changelist #{0} submitted by '{1}' at {2}, which contains {3} files", 
                    changelist.Number, changelist.Username, changelist.Time.ToString(), changelist.Files.Count());

                if (changelist.Files.Count() == 0)
                {
                    Log.WarningCtx(LOG_CTX, "Empty changelist {0}: {1}.  Ignoring.", changelist.Number, changelist.Description);
                    continue;
                }

                // Ensure we only process files within a changelist that should be monitored.
                List<String> monitoredFiles = new List<String>();
                foreach(String monitoredPath in this.MonitoredPaths)
                    monitoredFiles.AddRange(changelist.Files.Where(f => f.ToLower().StartsWith(monitoredPath.ToLower())));

                IEnumerable<FileMapping> changeListFiles = FileMapping.Create(this.P4, monitoredFiles.ToArray());

                // --- DLC ---
                // File(s) warrant their own job for each unique project & branch they belong to ( DLC or otherwise ) - THEN the jobs are split into a dispatch.
                // - Build a dictionary of these batches keyed by project and branch.
                IDictionary<ProjectBatchKey, IEnumerable<FileMapping>> changeListFilesBatched = ProjectBatcher.Batch(changeListFiles, this.Options, Log);
                ProjectBatcher.DisplayBatched(changeListFiles, changeListFilesBatched, Log);

                // Spread this changelist (distinct by project and branch) across a number of dispatch groups (if necessary)
                lock (this.DispatchScheme)
                {
                    foreach (KeyValuePair<ProjectBatchKey, IEnumerable<FileMapping>> changelistFilesKvp in changeListFilesBatched)
                    {
						// Decipher kvp
                        ProjectBatchKey projectBatchKey = changelistFilesKvp.Key;
                        IEnumerable<FileMapping> changelistFilesInBatch = changelistFilesKvp.Value;
                        Log.MessageCtx(LOG_CTX, "Dispatching for project ({0} {1})", projectBatchKey.Project, projectBatchKey.Branch);
                        Dispatch(changelist, changelistFilesInBatch, projectBatchKey.Project, projectBatchKey.Branch);
                    }
                }
            }
        }

        /// <summary>
        /// File containing information required to send notifications.
        /// </summary>
        /// <returns></returns>
        public override String GetNotificationDataFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_EMAIL_NOTIFICATIONS_FILE]));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="machinename"></param>
        /// <returns></returns>
        public override IJob GetSuitableJob(string machinename)
        {
            Log.MessageCtx(LOG_CTX, "Client '{0}' is requesting a suitable job.", machinename.ToLower());

            if (!base.CanGetSuitableJob(machinename))
                return null;

            lock (this.DispatchScheme)
            {                
                // Find any suitable dispatch groups for this machine
                DispatchGroup[] suitableDispatchGroups = this.DispatchScheme.Groups.Where(jg => jg.MachineName == machinename.ToLower()).ToArray();

                if (suitableDispatchGroups.Length > 0)
                {
                    Log.MessageCtx(LOG_CTX, "Client '{0}' has suitable dispatch groups registered.", machinename.ToLower());

                    // Only handle file triggers since we only support path group items at this stage. 
                    // Loop through machine suitable groups looking for a match
                    lock (this.m_Jobs)
                    {
                        foreach (IJob job in this.PendingJobs)
                        {
                            IEnumerable<String> files = job.Trigger.TriggeringFiles();

                            foreach (DispatchGroup suitableDispatchGroup in suitableDispatchGroups)
                            {
                                // Check override of job dispatch
                                if (suitableDispatchGroup.ContainsJob(job))
                                {
                                    Log.MessageCtx(LOG_CTX, "Found job '{0}' for client '{1}' : it was explicity assigned to this machine and overrides any path assignment.", job.ID.ToString(), machinename.ToLower());
                                    return job;
                                }

                                if (suitableDispatchGroup.ContainsAnyPath(Log, files))
                                {
                                    Log.MessageCtx(LOG_CTX, "Found job '{0}' for client '{1}'.", job.ID.ToString(), machinename.ToLower());

                                    IJob consumingJob = ConsumeJobs(machinename, job);
                                    if (consumingJob != null)
                                        return consumingJob;
                                    else
                                        return job;
                                }
                            }
                        }
                    }
                }
                else
                {
                    Log.MessageCtx(LOG_CTX, "Client '{0}' doesn't have any dispatch groups registered.", machinename.ToLower());

                    // Here we have a 'fallback' client, which processes anything left over.
                    // So, we find the next job that has no dispatch group interested in it
                    lock (this.m_Jobs)
                    {
                        foreach (IJob job in this.PendingJobs)
                        {
                            IEnumerable<String> filesTriggering = job.Trigger.TriggeringFiles();
                            if (filesTriggering.Any())
                            {
                                Log.DebugCtx(LOG_CTX, "--- Checking if Client {0} could process job {1}  (if another dispatch group is interested in the files it can't process it.) --- ", machinename.ToLower(), job.ID);
                                bool dipatchGroupInterested = false;
                                foreach (DispatchGroup jg in this.DispatchScheme.Groups.ToList())
                                {
                                    Log.DebugCtx(LOG_CTX, "\tChecking {0}", jg.MachineName);

                                    if (jg.ContainsAnyPath(Log, filesTriggering))
                                    {
                                        Log.DebugCtx(LOG_CTX, "\tClient {0} will not process Job {1} since other client {2} will handle it", machinename.ToLower(), job.ID, jg.MachineName);
                                        dipatchGroupInterested = true;
                                    }
                                }

                                if (!dipatchGroupInterested)
                                {
                                    Log.MessageCtx(LOG_CTX, "Found job '{0}' for client '{1}'.", job.ID.ToString(), machinename.ToLower());

                                    IJob consumingJob = ConsumeJobs(machinename, job);
                                    if (consumingJob != null)
                                        return consumingJob;
                                    else
                                        return job;
                                }
                            }
                        }
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Returns location of dispatch scheme data file.
        /// </summary>
        /// <returns></returns>
        public String GetJobManagementFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_DISPATCH_SCHEME_FILE]));
        }

        /// <summary>
        /// Task notification of job being completed.
        /// </summary>
        /// <param name="job"></param>
        public override void JobCompleted(IJob job, IEnumerable<IJobResult> jobResults)
        {
            base.JobCompleted(job, jobResults);
        }

        #endregion // Controller Methods

        #region Private Methods

        /// <summary>
        /// Sets the processinghost on pending jobs for the processing host it currently would be assigned to.
        /// </summary>
        /// <param name="pendingJobs"></param>
        private void AssignProcessingHostForPendingJobs()
        {
            foreach (IJob job in this.PendingJobs.Where(j => (j is AssetBuilderJob)))
            {
                IEnumerable<String> files = job.Trigger.TriggeringFiles();

                DispatchGroup dispatchGroup = this.DispatchScheme.Groups.Where(g => g.ContainsAnyPath(Log, files)).FirstOrDefault();
                
                // use the dispatch group machine OR the fallback.
                String processingHost = (dispatchGroup != null) ? dispatchGroup.MachineName : Environment.MachineName.ToLower();

                if (!job.ProcessingHost.Equals(processingHost, StringComparison.CurrentCultureIgnoreCase))
                {
                    Log.MessageCtx(LOG_CTX, "Pending Job {0} now expects to process on processing host {1}", job.ID, processingHost);
                    job.ProcessingHost = processingHost;
                }
            }
        }

        /// <summary>
        /// Find any number of possible job binding(s) then enqueue new 'consuming' job(s) encompassing these.
        /// - the consumed jobs state changes to 'skippedconsumed'.
        /// - a consuming job cannot be 'reconsumed'.
        /// - jobs store their skipconsumed setting so that and only jobs of a similar skip consumed setting may bind together.
        /// - the skip threshold is defined as being the number of jobs that can be binded together that will trigger the binding, but more jobs can be binded than the threshold if more can be found.
        /// - Jobs are to be consumed in order of age ( oldest first )
        /// </summary>
        /// <param name="machinename"></param>
        /// <param name="jobToConsume"></param>
        /// <returns></returns>
        private IJob ConsumeJobs(String machinename, IJob jobToConsume)
        {
            try
            {
                if (!this.Parameters.ContainsKey(PARAM_JOB_BINDING))
                    return null;

                JobBindMode jobBindMode = (JobBindMode)Enum.Parse(typeof(JobBindMode), (String)this.Parameters[PARAM_JOB_BINDING], true);

                if (jobBindMode == JobBindMode.Off)
                    return null;
                
                Log.MessageCtx(LOG_CTX, " ");
                Log.MessageCtx(LOG_CTX, "--- Consuming jobs ---");
                Log.MessageCtx(LOG_CTX, " ");

                int numConsumed = 0;
                int numConsumers = 0;

                lock (this.m_Jobs)
                {
                    // Get a list of candidate jobs for consuming 
                    IEnumerable<AssetBuilderJob> originalCandidateJobs = this.Jobs.
                        OfType<AssetBuilderJob>().
                        Where(j => j.State == JobState.Pending).
                        Where(j => j.ConsumedByJobID == Guid.Empty).
                        OrderBy(j => j.CreatedAt);

                    IEnumerable<AssetBuilderJob> candidateJobs = originalCandidateJobs;

                    if (jobToConsume != null)
                    {
                        if (!(jobToConsume is AssetBuilderJob))
                        {
                            Log.ErrorCtx(LOG_CTX, "Cannot consume jobs of this type {0} : {1}", jobToConsume.ID, jobToConsume.GetType());
                            return null;
                        }

                        if (!candidateJobs.Contains(jobToConsume))
                        {
                            Log.ErrorCtx(LOG_CTX, "Job {0} was asked to be consumed but this is not a candidate job", jobToConsume.ID);
                            return null;
                        }

                        // Prune list down to this one job.
                        candidateJobs = new List<AssetBuilderJob>() { (AssetBuilderJob)jobToConsume };
                    }

                    using (ProfileContext ctx = new ProfileContext(Log, LOG_CTX, "Check binding of jobs"))
                    {
                        // Here we store a list of jobs that are already consumed
                        ISet<AssetBuilderJob> allJobsConsumed = new HashSet<AssetBuilderJob>();

                        // For each job check if it can bind with another job
                        int bindingsChecked = 0;
                        foreach (AssetBuilderJob bindingJob1 in candidateJobs)
                        {
                            if (allJobsConsumed.Contains(bindingJob1))
                                continue; // already consumed

                            IEnumerable<AssetBuilderJob> jobsToCheckBinding = originalCandidateJobs.Where(j => j != bindingJob1 && !allJobsConsumed.Contains(j));

                            ISet<AssetBuilderJob> jobsBindable = new HashSet<AssetBuilderJob>();

                            int numToCheck = jobsToCheckBinding.Count();

                            // Setup the engine should it be required
                            IEngineDelegate engineDelegates = null;
                            Engine.Engine engine = null;
                            IContentTree contentTree = null;

                            if (numToCheck > 0 && jobBindMode == JobBindMode.ContentTreeOutputFilenames)
                            {
                                // Work out the branch
                                IEnumerable<String> filesTriggering = bindingJob1.Trigger.TriggeringFiles();
                                if (!filesTriggering.Any())
                                {
                                    this.Log.ErrorCtx(LOG_CTX, "Unsupported trigger attached to job.  Must contain a FilesTrigger");
                                    continue;
                                }
                                IEnumerable<FileMapping> files = FileMapping.Create(this.P4, filesTriggering.ToArray());
                                // Use the batching to get the files and branch - there should be only ONE batch.
                                IDictionary<ProjectBatchKey, IEnumerable<FileMapping>> batches = ProjectBatcher.Batch(files, this.Options, Log);
                                ProjectBatchKey projectBatchKey = batches.Keys.First();

                                engineDelegates = new Engine.EngineDelegate();
                                engineDelegates.PreBuildCallback += this.PrebuildOutputCallback;

                                engine = new Engine.Engine(this.Options, this.Options.Config, projectBatchKey.Project, projectBatchKey.Branch, EngineFlags.Default, null, engineDelegates, null);
                                contentTree = Factory.CreateTree(projectBatchKey.Branch);
                            }

                            foreach (AssetBuilderJob bindingJob2 in jobsToCheckBinding)
                            {
                                bindingsChecked++;

                                Log.MessageCtx(LOG_CTX, " ");
                                Log.MessageCtx(LOG_CTX, "({0}/{1}) Considering Job Binding {2} & {3}", bindingsChecked, numToCheck, bindingJob1.ID, bindingJob2.ID);
                                Log.MessageCtx(LOG_CTX, "-----------------------------------------------------------------------------------------------------------");

                                if (CanBind(bindingJob1, bindingJob2, jobBindMode, engine, contentTree, engineDelegates))
                                {
                                    jobsBindable.Add(bindingJob1);
                                    jobsBindable.Add(bindingJob2);
                                    Log.MessageCtx(LOG_CTX, " ");
                                    Log.MessageCtx(LOG_CTX, "\t================================================================================================================");
                                    Log.MessageCtx(LOG_CTX, "\t*** ({0}/{1})    Job pair CAN bind ({2} {3}) ***", bindingsChecked, numToCheck, bindingJob1.ID, bindingJob2.ID);
                                }
                                else
                                {
                                    Log.MessageCtx(LOG_CTX, " ");
                                    Log.MessageCtx(LOG_CTX, "\t================================================================================================================");
                                    Log.MessageCtx(LOG_CTX, "\t({0}/{1}) Job pair CANNOT bind ({2} {3})", bindingsChecked, numToCheck, bindingJob1.ID, bindingJob2.ID);
                                }
                                Log.MessageCtx(LOG_CTX, "\t================================================================================================================");
                            }

                            if (jobsBindable.Any())
                            {
                                Log.MessageCtx(LOG_CTX, " ");
                                Log.MessageCtx(LOG_CTX, "==================================");
                                Log.MessageCtx(LOG_CTX, "=== Consuming up to {0,5} jobs ===", jobsBindable.Count());
                                Log.MessageCtx(LOG_CTX, "==================================");

                                // joins the jobs it can together ( should be all of them ) forming a newly enqueued job
                                IEnumerable<AssetBuilderJob> jobsConsumedByNewConsumingJob;
                                AssetBuilderJob consumingJob = Bind(jobsBindable, out jobsConsumedByNewConsumingJob);
                                
                                if (this.Parameters.ContainsKey(PARAM_EMAIL_ASSETBUILDER_JOB_BINDING) && jobsConsumedByNewConsumingJob.Any())
                                {
                                    SendAssetbuilderConsumeEmail(this.Log, jobsConsumedByNewConsumingJob);
                                }

                                if (this.Parameters.ContainsKey(PARAM_DONT_BIND_AFTER_EMAIL_ASSETBUILDER_JOB_BINDING))
                                {
                                    return jobToConsume;
                                }


                                if (consumingJob != null)
                                {
                                    // Check all jobs were consumed as this might indicate an issue if not.
                                    if (jobsBindable.Count() != jobsConsumedByNewConsumingJob.Count())
                                    {
                                        Log.WarningCtx(LOG_CTX, "Only {0} jobs were consumed out of {1} jobs.", jobsConsumedByNewConsumingJob.Count(), jobsBindable.Count());
                                    }

                                    // Update store of all the jobs consumed so we don't reuse already bound jobs.
                                    allJobsConsumed.UnionWith(jobsConsumedByNewConsumingJob);

                                    // The jobs that were consumed are now flagged as skipped/consumed
                                    foreach (AssetBuilderJob jobConsumed in jobsConsumedByNewConsumingJob)
                                    {
                                        Log.MessageCtx(LOG_CTX, "=== {0} is now skipped consumed", jobConsumed.ID);
                                        jobConsumed.SetConsumed(consumingJob);
                                        numConsumed++;
                                    }
                                    Log.MessageCtx(LOG_CTX, "=== {0} jobs consumed by job {1}", jobsConsumedByNewConsumingJob.Count(), consumingJob.ID);
                                    Log.MessageCtx(LOG_CTX, "==================================");
                                    Log.MessageCtx(LOG_CTX, " ");

                                    numConsumers++;
                                }
                                else
                                {
                                    Log.ErrorCtx(LOG_CTX, "No consuming jobs created.");
                                }
                            }

                            if (engineDelegates != null)
                                engineDelegates.PreBuildCallback -= this.PrebuildOutputCallback;
                        }

                        Log.MessageCtx(LOG_CTX, " ");
                        Log.MessageCtx(LOG_CTX, "==================================================================");
                        Log.MessageCtx(LOG_CTX, "=== BINDING SUMMARY : Job {0} ===", jobToConsume.ID);

                        Log.MessageCtx(LOG_CTX, "\t{0} job bindings checked during AssetBuilder ConsumeJobs", bindingsChecked);

                        if (numConsumed > 0)
                        {
                            Log.MessageCtx(LOG_CTX, "\t*** {0} Total jobs consumed into {1} new jobs ***", numConsumed, numConsumers);

                            // Find the consuming job
                            IJob consumingJob = this.PendingJobs.Single(j => j.ID == jobToConsume.ConsumedByJobID);
                            Log.MessageCtx(LOG_CTX, "\tInstead of giving out job {0} we have combined {1} jobs and will give {2} to {3}", jobToConsume.ID, numConsumed, consumingJob.ID, machinename.ToLower());

                            // Save the task when jobs are created - otherwise they could go missing if say the automation service crashed before the job could be executed.
                            Save();

                            return consumingJob;
                        }
                        else
                        {
                            Log.MessageCtx(LOG_CTX, "\tNO JOB BINDING for job {0}.", jobToConsume.ID);
                        }
                        Log.MessageCtx(LOG_CTX, "==================================================================");
                        Log.MessageCtx(LOG_CTX, " ");
                    }
                }
            }
            catch (Exception ex)
            {
               Log.ToolExceptionCtx(LOG_CTX, ex, "Exception during job consumption, no binding will take place.");
            }
            return null;
        }


        /// <summary>
        /// Sends an email to notify some users of new assetbuilder job consuming functionality
        /// </summary>
        /// <param name="log"></param>
        private void SendAssetbuilderConsumeEmail(IUniversalLog log, IEnumerable<IJob> jobsConsumedByNewConsumingJob, String subject = "Assetbuilder Consumed")
        {
            try
            {
                // Send a ( hardcoded ) notification 
                String context = String.Format("AssetBuilderConsumeAlert : {0} {1}: {2} Jobs(s)", this.Options.Config.CoreProject.Name, this.Options.Branch.Name, jobsConsumedByNewConsumingJob.Count());

                subject = String.Format("{0} : {1}", context, subject);

                IList<String> body = new List<String>();
                body.Add("The following jobs are to be consumed\n");
                foreach (IJob job in jobsConsumedByNewConsumingJob)
                {
                    body.Add(String.Format("\t{0}",job.ID));
                    body.Add(String.Format("\t-----------------------------------------------", job.ID));
                    foreach (String filename in job.Trigger.TriggeringFiles())
                    {
                        body.Add(String.Format("\t\t{0}", filename));
                    }
                    body.Add("\t----------------------------------------");
                }

                IEnumerable<IEmailRecipient> notificationRecipients = new List<IEmailRecipient>() { new NotificationRecipient("tools", "*tools@rockstarnorth.com", NotificationRule.OnAll) };
                GeneralAlertNotification notifier = new GeneralAlertNotification(log, this.Options, subject, body);
                notifier.Send(notificationRecipients, null);
            }
            catch (Exception e) // deliberately silenced.
            {
                log.WarningCtx(LOG_CTX, "AssetBuilderConsumeAlert Email Failed {0}", e);
            }
        }

        /// <summary>
        /// Checks the details of two jobs indicate they can be binded together for consuming in a bigger job.
        /// </summary>
        /// <param name="job1"></param>
        /// <param name="job2"></param>
        /// <param name="jobBindMode">the mode of binding</param>
        /// <returns>true if bindable</returns>
        private bool CanBind(AssetBuilderJob job1, AssetBuilderJob job2, JobBindMode jobBindMode,  Engine.Engine engine, IContentTree contentTree, IEngineDelegate engineDelegates)
        {
            // MultiTriggers must not bind : it will complicate jobs beyond debugability!
            // The checks for IFilesTrigger would satisfy this but I want to make that absolutely clear.

            if (!job1.ProjectName.Equals(job2.ProjectName, StringComparison.CurrentCultureIgnoreCase))
            {
                Log.MessageCtx(LOG_CTX, "\tCan't bind : differing projects");
                return false;
            }

            if (!job1.BranchName.Equals(job2.BranchName, StringComparison.CurrentCultureIgnoreCase))
            {
                Log.MessageCtx(LOG_CTX, "\tCan't bind : differing branches");
                return false;
            }

            if ((job1.Trigger is IMultiTrigger))
            {
                Log.MessageCtx(LOG_CTX, "\tCan't bind : already IMultiTrigger");
                return false;
            }
            if ((job2.Trigger is IMultiTrigger))
            {
                Log.MessageCtx(LOG_CTX, "\tCan't bind : already IMultiTrigger");
                return false;
            }

            if (!(job1.Trigger is IFilesTrigger))
            {
                Log.MessageCtx(LOG_CTX, "\tCan't bind : not IFilesTrigger");
                return false;
            }
            if (!(job2.Trigger is IFilesTrigger))
            {
                Log.MessageCtx(LOG_CTX, "\tCan't bind : not IFilesTrigger");
                return false;
            }

            if (!(job1 is AssetBuilderJob))
            {
                Log.MessageCtx(LOG_CTX, "\tCan't bind : not AssetBuilderJob");
                return false;
            }
            if (!(job2 is AssetBuilderJob))
            {
                Log.MessageCtx(LOG_CTX, "\tCan't bind : not AssetBuilderJob");
                return false;
            }

            if (job1.Role != job2.Role)
            {
                Log.MessageCtx(LOG_CTX, "\tCan't bind : Roles differ");
                return false;
            }

            // handle null prebuild commands
            if (job1.PrebuildCommands == null) 
            {
                if (job2.PrebuildCommands != null && job2.PrebuildCommands.Any())
                {
                    Log.MessageCtx(LOG_CTX, "\tCan't bind : prebuild commands differ");
                    return false;
                }
            }
            else if (job2.PrebuildCommands == null)
            {
                if (job1.PrebuildCommands != null && job1.PrebuildCommands.Any())
                {
                    Log.MessageCtx(LOG_CTX, "\tCan't bind : prebuild commands differ");
                    return false;
                }
            }
            else if (!Enumerable.SequenceEqual(job1.PrebuildCommands, job2.PrebuildCommands))
            {
                Log.MessageCtx(LOG_CTX, "\tCan't bind : prebuild commands differ");
                return false;
            }

            // handle null postbuild commands
            if (job1.PostbuildCommands == null)
            {
                if (job2.PostbuildCommands != null && job2.PostbuildCommands.Any())
                {
                    Log.MessageCtx(LOG_CTX, "\tCan't bind : postbuild commands differ");
                    return false;
                }
            }
            else if (job2.PostbuildCommands == null)
            {
                if (job1.PostbuildCommands != null && job1.PostbuildCommands.Any())
                {
                    Log.MessageCtx(LOG_CTX, "\tCan't bind : postbuild commands differ");
                    return false;
                }
            }
            else if (!Enumerable.SequenceEqual(job1.PostbuildCommands, job2.PostbuildCommands))
            {
                Log.MessageCtx(LOG_CTX, "\tCan't bind : postbuild commands differ");
                return false;
            }

            // Permit the rebuild flag only to differ.
            EngineFlags engineFlagsDiffering = job1.EngineFlags ^ job2.EngineFlags;
            if (engineFlagsDiffering != EngineFlags.None && 
                engineFlagsDiffering != EngineFlags.Rebuild)
            {
                Log.MessageCtx(LOG_CTX, "\tCan't bind : engine flags differ. Jobs=( {0} {1} )", job1.ID, job2.ID);
                return false;
            }

            // At present triggers of different types are non binding, however soon it should be possible 
            if (this.Parameters.ContainsKey(PARAM_JOB_BINDING_ONLY_BIND_SIMILAR_TRIGGER_TYPES) && (bool)this.Parameters[PARAM_JOB_BINDING_ONLY_BIND_SIMILAR_TRIGGER_TYPES])
            {
                if (job1.Trigger.GetType() != job2.Trigger.GetType())
                {
                    Log.MessageCtx(LOG_CTX, "\tCan't bind : trigger types differ. Jobs=( {0} {1} )", job1.ID, job2.ID);
                    return false;
                }
            }

            // A simple and fast way - primarily for development, but could serve as a fallback should it all go wrong.
            if (jobBindMode == JobBindMode.SourceFilenames)
            {
                IFilesTrigger filesTrigger1 = (IFilesTrigger)job1.Trigger;
                IFilesTrigger filesTrigger2 = (IFilesTrigger)job2.Trigger;

                IEnumerable<String> intersections = filesTrigger1.Files.Intersect(filesTrigger2.Files);
                if (!intersections.Any())
                {
                    Log.MessageCtx(LOG_CTX, "\tJob {0} & {1} have no TRIGGERING FILENAME intersections - they will not bind", job1.ID, job2.ID);
                    return false;
                }

                Log.MessageCtx(LOG_CTX, "\tJob {0} & {1} intersect using TRIGGERING FILENAMES", job1.ID, job2.ID);
                foreach (String intersection in intersections)
                {
                    Log.MessageCtx(LOG_CTX, "\tIntersecting output file : {0}", intersection);
                }
            }
            // The more involved way - content tree analysis.
            else if (jobBindMode == JobBindMode.ContentTreeOutputFilenames)
            {
                IEnumerable<String> outputPathsJob1 = GetOutputsFromEnginePrebuild(job1, engine, contentTree, engineDelegates);
                IEnumerable<String> outputPathsJob2 = GetOutputsFromEnginePrebuild(job2, engine, contentTree, engineDelegates);

                if (outputPathsJob1 == null || outputPathsJob2 == null)
                {
                    if (outputPathsJob1 == null)
                        Log.MessageCtx(LOG_CTX, "\tJob {0} doesn't produce any output - it cannot bind", job1.ID);
                    if (outputPathsJob2 == null)
                        Log.MessageCtx(LOG_CTX, "\tJob {0} doesn't produce any output - it cannot bind", job2.ID);
                    return false;
                }

                IEnumerable<String> intersections = outputPathsJob1.Intersect(outputPathsJob2);

                if (!intersections.Any())
                {
                    Log.MessageCtx(LOG_CTX, "\tNO CONTENT TREE intersections - jobs will not bind");
                    return false;
                }

                Log.MessageCtx(LOG_CTX, "\tIntersect using CONTENT TREE ( {2} intersections )", job1.ID, job2.ID, intersections.Count());
                foreach (String intersection in intersections)
                {
                    Log.MessageCtx(LOG_CTX, "\t\tIntersecting output file : {0}", intersection);
                }
            }
            else
            {
                throw new NotSupportedException(String.Format("Job Bind Mode {0} not supported yet.", jobBindMode));
            }

            return true;
        }

        /// <summary>
        /// For the files in the job, call engine prebuild to determine the output files that will be produced.
        /// - supports caching in memory.
        /// </summary>
        /// <param name="job"></param>
        /// <returns>The outputs</returns>
        private IEnumerable<String> GetOutputsFromEnginePrebuild(AssetBuilderJob job, Engine.Engine engine, IContentTree contentTree, IEngineDelegate engineDelegates)
        {
            // Check cache for the job output files.
            if (JobContentTreeOutputPathsCache.ContainsKey(job.ID))
            {
                return JobContentTreeOutputPathsCache[job.ID];
            }

            Log.MessageCtx(LOG_CTX, "Running Engine prebuild on job {0} to determine if jobs can bind", job.ID);
            IEnumerable<String> filesTriggering = job.Trigger.TriggeringFiles();
            IEnumerable<FileMapping> fileMappings = FileMapping.Create(this.P4, filesTriggering.ToArray());

            IEnumerable<IOutput> outputs;
            IEnumerable<String> inputs = fileMappings.Select(fm => fm.LocalFilename);

            // run engine prebuild
            try
            {
                // This has to be reset every time : not sure why really but some static horribleness 
                RSG.Pipeline.Processor.Map.PreProcess.ResetStaticData();

                IUniversalLog silentLog = LogFactory.CreateUniversalLog("silent_prebuild_log");
                IEngineParameters param = new EngineParameters(engine.Branch, engine.Flags, engineDelegates, BuildType.All, silentLog);
                IEnumerable<IProcess> resolvedProcesses = ProcessResolver.Resolve(param, contentTree, inputs);

                // Have to force the process state to be initialised each time for now see url:bugstar:1804164
                foreach (IProcess process in resolvedProcesses)
                {
                    process.State = ProcessState.Initialised;
                }
                
                IInput input = new BuildInput(this.Options.Branch, BuildType.All);
                input.RawProcesses.AddRange(resolvedProcesses);

                //engine.Prebuild(inputs, out outputs, out buildTime, param);
                TimeSpan buildTime = new TimeSpan();
                engine.Prebuild(ref input, out outputs, out buildTime);

                if (!outputs.Any())
                {
                    this.Log.ErrorCtx(LOG_CTX, "No outputs returned from prebuild");
                }
            }
            catch (Exception ex)
            {
                // The exception possibly can be ignored as they can occur due to not being synced to the CL of the job we are prebuilding.
                this.Log.WarningCtx(LOG_CTX, "Possibly can be ignored : An unhandled exception occured during assetbuilder TASK engine creation/prebuild: {0}", ex.Message);

                // Still populate the cache ( with a null ) because we don't want this hard work being executed continually.
                JobContentTreeOutputPathsCache[job.ID] = null;

                return null;
            }

            // Analyise outputs of prebuild.
            ISet<String> outputFilenames = new HashSet<String>();

            foreach (IOutput output in outputs)
            {
                foreach (IContentNode outputItem in output.Items.OfType<IFilesystemNode>())
                {
                    IFilesystemNode fsNode = (IFilesystemNode)outputItem;
                    outputFilenames.Add(fsNode.AbsolutePath);
                }
            }

            if (!outputFilenames.Any())
            {
                this.Log.WarningCtx(LOG_CTX, "No output filenames found for prebuild of job {0} yet it has {1} outputs", job.ID, outputs.Count());
            }

            // Update cache
            JobContentTreeOutputPathsCache[job.ID] = outputFilenames;

            return outputFilenames;
        }


        /// <summary>
        /// Asset builder TASK prebuild callback.  Used for checking out files that are output from any engine processors.
        /// </summary>
        /// <param name="branch"></param>
        /// <param name="contentList"></param>
        public bool PrebuildOutputCallback(IBranch branch, IEnumerable<IContentNode> contentList, Object userData)
        {
            List<String> files = new List<String>();

            try
            {
                foreach (IContentNode input in contentList)
                {
                    Debug.Assert(input is IFilesystemNode);
                    if (input is IFilesystemNode)
                    {
                        IFilesystemNode fileNode = input as IFilesystemNode;
                        if (input is Pipeline.Content.File)
                        {
                            files.Add(fileNode.AbsolutePath);
                        }
                        else if (input is Pipeline.Content.Directory)
                        {
                            if (System.IO.Directory.Exists(fileNode.AbsolutePath))
                            {
                                String[] allFiles = System.IO.Directory.GetFiles(fileNode.AbsolutePath, "*.*", System.IO.SearchOption.AllDirectories);
                                files.AddRange(allFiles);
                            }
                        }
                        else
                        {
                            Log.ErrorCtx(LOG_CTX, "Invald content node type {0}::{1}", input.GetType().ToString(), input.Name);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.ToolExceptionCtx(LOG_CTX, ex, "Exception generating content list in *AssetBuilderTask* prebuild callback: {0}", ex.Message);
                return false;
            }

            return (true);
        }

        /// <summary>
        /// Creates jobs from an enumerable of jobs binding the common details together into a larger job
        /// - this job is enqueued.
        /// </summary>
        /// <param name="jobsToConsume">the jobs to try to consume</param>
        /// <param name="jobsConsumed">the actual jobs consumed</param>
        /// <returns>the new job created if any</returns>
        private AssetBuilderJob Bind(IEnumerable<AssetBuilderJob> jobsToConsume, out IEnumerable<AssetBuilderJob> jobsConsumed)
        {
            // Collate all the details across all the jobs to build the consuming job.           

            // OR together all the flags - jobs should not have been asked to bind unless it's ok to OR their flags ( eg. EngineFlags.Rebuild )
            EngineFlags engineFlags = EngineFlags.None;

            foreach (AssetBuilderJob assetbuilderJob in jobsToConsume)
            {
                engineFlags |= assetbuilderJob.EngineFlags;
            }

            IBranch branch; 
            IProject project;
            if (!jobsToConsume.First().GetProjectAndBranch(this.Options, this.Log, out project, out branch))
            {
                this.Log.ErrorCtx(LOG_CTX, "Job binding: Couldn't derive project and branch for assetbuilder job");
                jobsConsumed = null;
                return null;
            }

            IEnumerable<ITrigger> triggers = jobsToConsume.Select(j => j.Trigger);
            AssetBuilderJob consumingJob = new AssetBuilderJob(branch, triggers, engineFlags);
            EnqueueJob(consumingJob);

            // We make the jobs consumed accessible to calling method.
            jobsConsumed = jobsToConsume;

            return consumingJob;
        }

        /// <summary>
        /// Dispatch jobs for a changelist
        /// </summary>
        /// <param name="changelist"></param>
        /// <param name="changeListFiles"></param>
        private void Dispatch(Changelist changelist, IEnumerable<FileMapping> changeListFiles, IProject project, IBranch branch)
        {
            // Record which files were routed so we can create a job for any 'stragglers'
            List<FileMapping> changeListFilesRoutedToDispatchGroup = new List<FileMapping>();

            Guid triggerId = Guid.NewGuid();
            foreach (DispatchGroup dispatchGroup in this.DispatchScheme.Groups)
            {
                Log.MessageCtx(LOG_CTX, "Considering dispatch group '{0}', which has {1} items",
                    dispatchGroup.Name, dispatchGroup.Items.Count());

                List<String> filesForThisTarget = new List<String>();

                foreach (FileMapping changeListFile in changeListFiles)
                {
                    String changelistFilePathname = Path.GetFullPath(changeListFile.ExpandedLocalFilename);
                    String changelistFileDirectoryName = Path.GetDirectoryName(changelistFilePathname);

					if(dispatchGroup.ContainsAnyPath(Log, new String[]{changelistFileDirectoryName}))
                    {
                        filesForThisTarget.Add(changelistFilePathname);
                        changeListFilesRoutedToDispatchGroup.Add(changeListFile);
                    }
                    // LA: Doing this as above only passes the directory. Don't want to effect any logic from this. 
                    else if (dispatchGroup.ContainsAnyPath(Log, new String[] { changelistFilePathname }))
                    {
                        filesForThisTarget.Add(changelistFilePathname);
                        changeListFilesRoutedToDispatchGroup.Add(changeListFile);
                    }
                }

                if (filesForThisTarget.Any())
                {
                    Log.MessageCtx(LOG_CTX, "Creating AssetBuilderJob from changelist {0} with the following {1} files:",
                        changelist.Number, filesForThisTarget.Count);
                    foreach (String fileForThisTarget in filesForThisTarget)
                        Log.MessageCtx(LOG_CTX, "\t'{0}'", fileForThisTarget);

                    AssetBuilderJob abj = new AssetBuilderJob(branch, changelist, filesForThisTarget, triggerId);
                    this.EnqueueJob(abj);
                }
            }

            FileMapping[] changeListFilesNotRouted = changeListFiles.Except(changeListFilesRoutedToDispatchGroup).ToArray();

            // If we couldn't spread this changelist across registered dispatch groups then revert to the fallback,
            // which is to create a job for the entire changelist
            if (changeListFilesNotRouted.Length > 0)
            {
                Log.MessageCtx(LOG_CTX, "A dispatch group wasn't found for all files in changelist {0}.  Creating fallback AssetBuilderJob.", changelist.Number);
                AssetBuilderJob abj = new AssetBuilderJob(branch, changelist, changeListFilesNotRouted.Select(f => f.ExpandedLocalFilename), triggerId);
                this.EnqueueJob(abj);
            }
        }

        /// <summary>
        /// Method to clean out jobs and their files.
        /// </summary>
        private void CleanJobs()
        {
            int hours = (int)this.Parameters[PARAM_HOURS_JOB];
            DateTime modifiedTime = DateTime.UtcNow - TimeSpan.FromHours((double)hours);

            // We now just delete any job that has existed for the specified time rather than
            // specifically just completed jobs.
            lock (this.m_Jobs)
            {
                // Clean old completed jobs.
                foreach (IJob job in this.Jobs)
                {
                    // Check if completed a long time ago.
                    DateTime completionTime = (job.ProcessedAt + job.ProcessingTime);
                    if (completionTime.IsEarlierThan(modifiedTime))
                    {
                        CleanJob(job);
                    }
                }

                // Clean if the consuming job is now removed.
                foreach (IJob job in this.Jobs.Where(j => j.State == JobState.SkippedConsumed))
                {
                    if (!this.Jobs.Where(j => j.ID == job.ConsumedByJobID).Any())
                    {
                        CleanJob(job);
                    }
                }

                // B*1377109 - Remove any job results for jobs that no longer exist
                SortedSet<Guid> jobIDs = new SortedSet<Guid>(this.Jobs.Select(job => job.ID));
                lock (this.m_JobResults)
                {
                    IJobResult[] resultsToRemove = m_JobResults.Where(jobResult => !jobIDs.Contains(jobResult.JobId)).ToArray();
                    foreach (IJobResult resultToRemove in resultsToRemove)
                    {
                        this.m_JobResults.Remove(resultToRemove);
                    }
                }
            }
        }

        /// <summary>
        /// Remove job from our queue and clean necessary files.
        /// </summary>
        /// <param name="job"></param>
        private void CleanJob(IJob job)
        {
            lock (this.m_Jobs)
            {
                IEnumerable<KeyValuePair<JobPriority, IJob>> jobEntries =
                    this.m_Jobs.Where(j => j.Value.ID.Equals(job.ID));
                if (jobEntries.Count() > 0)
                    this.m_Jobs.Remove(jobEntries.First());
            }
            Services.FileTransferService.CleanJobFiles(this.Config, job, this.Log);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private String GetSettingsFilename()
        {
            return (Path.Combine(this.Config.ToolsConfig, "automation",
                "tasks", String.Format("{0}.xml", this.GetType().Name)));
        }

        /// <summary>
        /// 
        /// </summary>
        private void LoadMonitoringLocations()
        {
            (this.MonitoredPaths as List<String>).Clear();
            String monitoringFile = this.Config.Environment.Subst((String)this.Parameters[PARAM_MONITOR_PATH_FILE]);
            if (!System.IO.File.Exists(monitoringFile) && this.MonitoredPaths.Count() == 0)
            {
                this.Log.ErrorCtx(LOG_CTX, "Monitoring file does not exist ({0}).  Asset builder task will not be monitoring any locations.", monitoringFile);
            }

            // Cache monitoring path last modified time
            DateTime monitoringFileModified = System.IO.File.GetLastWriteTime(monitoringFile);
            bool showMonitoringPaths = this.MonitoringFileLastModified != monitoringFileModified;
            if (showMonitoringPaths && this.MonitoringFileLastModified != DateTime.MinValue)
            {
                this.Log.MessageCtx(LOG_CTX, " ");
                this.Log.MessageCtx(LOG_CTX, "*** Monitoring paths may have been modified : {0} ***", monitoringFile);
                this.Log.MessageCtx(LOG_CTX, " ");
            }

            this.MonitoringFileLastModified = monitoringFileModified;

            XDocument xdoc = XDocument.Load(monitoringFile);

            // Read all locations
            ICollection<String> locationsDeserialised = new List<String>();

            bool any = this.Parameters.ContainsKey(PARAM_MONITOR) ? Boolean.Parse(this.Parameters[PARAM_MONITOR].ToString()) : true;
            bool core = any && (this.Parameters.ContainsKey(PARAM_MONITOR_CORE_PROJECT) ? Boolean.Parse(this.Parameters[PARAM_MONITOR_CORE_PROJECT].ToString()) : true);
            bool dlc = any && (this.Parameters.ContainsKey(PARAM_MONITOR_DLC_PROJECTS) ? Boolean.Parse(this.Parameters[PARAM_MONITOR_DLC_PROJECTS].ToString()): true);

            foreach (XElement elem in xdoc.Descendants("location"))
            {
#warning TODO LPXO: Add some mechanism for removing paths, similar to perforce workspace capabilities
                String path = elem.Attribute("path").Value;
                if (showMonitoringPaths)
                {
                    this.Log.MessageCtx(LOG_CTX, "Monitoring Path: {0}", path);
                }
                locationsDeserialised.Add(path);
            }

            // For each deserialised location translate to appropriate location(s)
            ICollection<String> coreLocations = new List<String>();
            ICollection<String> dlcLocations = new List<String>();
            foreach (String location in locationsDeserialised)
            {
                String branchName = this.Options.Branch.Name;
                if (core)
                {
                    Debug.Assert(this.Options.CoreProject.Branches.ContainsKey(branchName),
                        "Core project does not have branch '{0}' defined.", branchName);
                    if (!this.Options.CoreProject.Branches.ContainsKey(branchName))
                        throw (new ArgumentException(String.Format("Core project does not have branch '{0}' defined.", branchName)));

                    string rootProjectLocation = this.Options.CoreProject.Branches[branchName].Environment.Subst(location);
                    if (showMonitoringPaths)
                    {
                        this.Log.MessageCtx(LOG_CTX, "Core project evaluated monitoring path: {0}", rootProjectLocation);
                    }
                    coreLocations.Add(rootProjectLocation);
                }

                if (dlc)
                {
                    Debug.Assert(this.Options.Project.Branches.ContainsKey(branchName),
                        "DLC project does not have branch '{0}' defined.", branchName);
                    if (!this.Options.Project.Branches.ContainsKey(branchName))
                        throw (new ArgumentException(String.Format("DLC project does not have branch '{0}' defined.", branchName)));

                    foreach (KeyValuePair<string, IProject> kvp in this.Config.DLCProjects)
                    {
                        string dlcProjectName = kvp.Key;
                        IProject dlcProject = kvp.Value;
                        if (dlcProject.Branches.ContainsKey(branchName))
                        {
                            String dlcProjectLocation = dlcProject.Branches[branchName].Environment.Subst(location);
                            if (showMonitoringPaths)
                            {
                                this.Log.MessageCtx(LOG_CTX, " - DLC project evaluated monitoring path: {0}", dlcProjectLocation);
                            }
                            dlcLocations.Add(dlcProjectLocation);
                        }
                        else
                        {
                            this.Log.WarningCtx(LOG_CTX, "DLC project branch does not exist: {0}", branchName);                         
                        }
                    }
                }
            }

            // Join all core & dlc locations
            ICollection<String> locations = new List<String>();
            locations.AddRange(coreLocations);
            locations.AddRange(dlcLocations);

            if (locations.Any())
            {
                FileMapping[] fms = FileMapping.Create(this.P4, locations.Distinct().ToArray());
                (this.MonitoredPaths as List<String>).AddRange(fms.Select(fm => fm.DepotFilename));
            }
            else
            {
                this.Log.ErrorCtx(LOG_CTX, "There are no monitoring locations, please examine your monitoring file {0}", monitoringFile);
            }
        }

        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.ServerHost.Tasks namespace
