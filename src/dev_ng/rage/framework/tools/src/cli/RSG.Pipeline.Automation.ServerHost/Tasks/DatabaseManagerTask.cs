﻿using RSG.Base.Configuration;
using RSG.Base.Configuration.Services;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Automation.Database.Common;
using RSG.Pipeline.Automation.Database.Common.Extensions;
using RSG.Pipeline.Automation.Database.Consumers;
using RSG.Pipeline.Automation.Database.Domain.Entities;
using RSG.Pipeline.Automation.Database.Domain.Entities.JobResults;
using RSG.Pipeline.Automation.Database.Domain.Entities.Jobs;
using RSG.Pipeline.Automation.Database.Domain.Entities.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Automation.ServerHost.Tasks
{
    public class DatabaseManagerTask : TaskBase, ITask, IDisposable
    {
        #region Constants
        /// <summary>
        /// The task name.
        /// </summary>
        private static readonly String NAME = "Database Manager";

        /// <summary>
        /// The task description.
        /// </summary>
        private static readonly String DESC = "Database Manager monitoring task.";

        #endregion // Constants

        #region Properties

        /// <summary>
        /// A list of consumers mapped by host name
        /// </summary>
        private IDictionary<String, AutomationAdminConsumer> Clients { get; set; }

        private JobDatabaseConsumer Consumer { get; set; }

        #endregion // Properties

        #region Controller Methods
        /// <summary>
        /// Constructs the task including connecting to the database server
        /// </summary>
        /// <param name="options"></param>
        public DatabaseManagerTask(CommandOptions options)
            : base(NAME, DESC, CapabilityType.DatabaseManager, options)
        {
            string monitorFile = GetAutomationServicesFile();

            IJobConfig config = new JobConfig(options.Branch);
            IServer serverConfig = config.Servers["local"];

            while (true)
            {
                try
                {
                    Consumer = new JobDatabaseConsumer(serverConfig);
                    break;
                }
                catch (Exception e)
                {
                    Log.WarningCtx(LOG_CTX, e.ToString());
                }
            }

            UpdateClients();
        }

        /// <summary>
        /// Make sure the consumer is disconnected
        /// </summary>
        public void Dispose()
        {
            
        }

        public override TaskStatus Status()
        {
            return (new TaskStatus(this));
        }

        public override string GetNotificationDataFile()
        {
            return String.Empty;
        }

        /// <summary>
        /// Gets all the servers from the AutomationServices.xml and query them for their current job state
        /// </summary>
        public override void Update()
        {
            UpdateClients();

            foreach (KeyValuePair<string, AutomationAdminConsumer> client in Clients)
            {
                Uri fileTransferService = new Uri(String.Format("net.tcp://{0}:7010/FileTransfer.svc", client.Value.ServiceConnection.Host));
                FileTransferServiceConsumer fileTransferConsumer = new FileTransferServiceConsumer(this.Options.Config, fileTransferService);

                foreach(TaskStatus status in client.Value.Monitor())
                {
                    foreach(IJob job in status.Jobs)
                    {
                        if (job.ID == Guid.Empty || job.Trigger.TriggerId == Guid.Empty)
                        {
                            continue;
                        }

                        JobModel jobModel = Consumer.GetJob(job.ID);
                        if (jobModel == null)
                        {
                            jobModel = job.Convert();
                            jobModel.Server = client.Value.ServiceConnection.Host;
                            Consumer.CreateJob(jobModel);
                        }
                        else
                        {
                            JobModel updatedJob = job.Convert();
                            updatedJob.Server = jobModel.Server = client.Value.ServiceConnection.Host;

                            List<Change> changes = jobModel.DetailedCompare(updatedJob);
                            if (changes.Count() > 0)
                            {
                                if (jobModel.Changes == null)
                                {
                                    jobModel.Changes = new List<Change>();
                                }

                                changes.AddRange(jobModel.Changes);

                                updatedJob.Changes = changes;

                                Consumer.UpdateJob(updatedJob);
                                LogFactory.ApplicationLog.Message("Changes {0}", changes.Count());
                            }
                        }
                    }

                    // Now save the job result
                    foreach (IJobResult result in status.JobResults)
                    {
                        JobResultModel resultModel = Consumer.GetJobResult(result.JobId);
                        if (resultModel == null)
                        {
                            JobResultModel jobResult = result.Convert();
                            Consumer.CreateJobResult(jobResult);
                            
                            IJob job = status.Jobs.Where(j => j.ID == jobResult.JobId).FirstOrDefault();
                            if(job != null)
                            {
                                Byte[] md5 = RSG.Base.IO.FileMD5.Empty;
                                bool fileResult = fileTransferConsumer.DownloadFile(job, "job.ulog","job.ulog", out md5);
                                if (fileResult)
                                {
                                    string logFile = fileTransferConsumer.GetClientFileDirectory(job);
                                    logFile = System.IO.Path.Combine(logFile, "job.ulog");

                                    XDocument xdoc = XDocument.Load(logFile);
                                    IEnumerable<UniversalLogFileBufferedMessage> messageList = UniversalLogFile.GetMessageList(xdoc).Where(m => m.Level == LogLevel.Error);

                                    IList<MessageModel> logMessages = new List<MessageModel>();
                                    foreach (UniversalLogFileBufferedMessage message in messageList)
                                    {
                                        MessageModel messageModel = new MessageModel(job.ID, message.Timestamp.ToUniversalTime(), message.Level, message.Context, message.Message);
                                        logMessages.Add(messageModel);
                                    }

                                    if (logMessages.Any())
                                    {
                                        Consumer.CreateLogMessageFromList(logMessages);
                                    }
                                }
                            }
                        }
                        else
                        {
                            JobResultModel updatedResult = result.Convert();

                            List<Change> changes = resultModel.DetailedCompare(updatedResult);
                            updatedResult.Changes = changes;

                            if (changes.Count() > 0)
                            {
                                LogFactory.ApplicationLog.Warning("Result for Job ID {0} is being updated. Results shouldn't change is this correct?", result.JobId);
                                Consumer.UpdateJobResult(updatedResult);
                            }
                        }
                    }
                }
            }
        }
        #endregion // Controller Methods

        #region Private Methods
        private String GetAutomationServicesFile()
        {
            return Options.Branch.Environment.Subst(System.IO.Path.Combine("$(toolsroot)", "etc", "automation", "AutomationServices.xml"));
            //return @"X:\gta5\tools_ng\etc\automation\AutomationServices.xml";
        }

        private void UpdateClients()
        {
            string monitorFile = GetAutomationServicesFile();

            XDocument doc = XDocument.Load(monitorFile);
            Clients = new Dictionary<string, AutomationAdminConsumer>();

            foreach (XElement elem in doc.Descendants("Service"))
            {
                try
                {
                    String serverURL = String.Format("{0}/{1}", elem.Element("ServerHost").Value, "automation.svc");
                    Log.Message("Connecting to server {0}", serverURL);
                    Uri serverHost = new Uri(serverURL);


                    AutomationAdminConsumer consumer = new AutomationAdminConsumer(serverHost);
                    consumer.Monitor();
                    Clients.Add(serverHost.Host, consumer);
                }
                catch (Exception e)
                {
                    Log.WarningCtx(LOG_CTX, e.ToString());
                }
            }
        }
        #endregion // Private Methods
    }
}
