﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;

namespace RSG.Pipeline.Automation.ServerHost.WCFExtensions
{
    /// <summary>
    /// Custom message inspector for adding the Access-Control-Allow-Origin if the Origin header was specified on the
    /// incoming message request and the endpoint is Cors enabled.
    /// 
    /// Code based on the following article:
    /// http://code.msdn.microsoft.com/Implementing-CORS-support-c1f9cd4b
    /// </summary>
    internal class CorsEnabledMessageInspector : IDispatchMessageInspector
    {
        /// <summary>
        /// Names of the cors enabled operations.
        /// </summary>
        private List<string> _corsEnabledOperationNames;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="corsEnabledOperations"></param>
        public CorsEnabledMessageInspector(List<OperationDescription> corsEnabledOperations)
        {
            this._corsEnabledOperationNames = corsEnabledOperations.Select(o => o.Name).ToList();
        }

        /// <summary>
        /// Checks to see whether the "Origin" header was sent and whether the destination operation is CORS enabled.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="channel"></param>
        /// <param name="instanceContext"></param>
        /// <returns></returns>
        public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
        {
            HttpRequestMessageProperty httpProp = (HttpRequestMessageProperty)request.Properties[HttpRequestMessageProperty.Name];
            object operationName;
            request.Properties.TryGetValue(WebHttpDispatchOperationSelector.HttpOperationNamePropertyName, out operationName);
            if (httpProp != null && operationName != null && this._corsEnabledOperationNames.Contains((string)operationName))
            {
                string origin = httpProp.Headers[CorsConstants.Origin];
                if (origin != null)
                {
                    return origin;
                }
            }

            return null;
        }

        /// <summary>
        /// If we are replying to a CORS enabled endpoint request, send back the Access-Control-Allow-Origin header.
        /// </summary>
        /// <param name="reply"></param>
        /// <param name="correlationState"></param>
        public void BeforeSendReply(ref Message reply, object correlationState)
        {
            string origin = correlationState as string;
            if (origin != null)
            {
                HttpResponseMessageProperty httpProp = null;
                if (reply.Properties.ContainsKey(HttpResponseMessageProperty.Name))
                {
                    httpProp = (HttpResponseMessageProperty)reply.Properties[HttpResponseMessageProperty.Name];
                }
                else
                {
                    httpProp = new HttpResponseMessageProperty();
                    reply.Properties.Add(HttpResponseMessageProperty.Name, httpProp);
                }

                httpProp.Headers.Add(CorsConstants.AccessControlAllowOrigin, origin);
            }
        }
    } // CorsEnabledMessageInspector
}
