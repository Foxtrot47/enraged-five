﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Export;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Services;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Services;
using System.Diagnostics;

namespace RSG.Pipeline.Automation.ServerHost.Tasks
{

    /// <summary>
    /// Task that responds to user-submitted jobs.
    /// </summary>
    public class CutscenePackageTask :
        MonitoringTaskBase,
        IMonitoringTask,
        IDisposable
    {
        #region Constants
        private static readonly String NAME = "Cutscene Package-Export";
        private static readonly String DESC = "Auto-exporting network task.";
        private static readonly String LOG_CTX = "MotionBuilder Export Network Task";

        /// <summary>
        /// Parameter to allow DLC projects to be monitored
        /// </summary>
        private static readonly String PARAM_MONITOR_DLC_PROJECTS = "Monitor DLC Projects";

        /// <summary>
        /// Parameter to allow NON-DLC (CORE) projects to be monitored
        /// </summary>
        private static readonly String PARAM_MONITOR_CORE_PROJECT = "Monitor Core Project";

        private static readonly String PARAM_HOURS_JOB = "Hours to Keep Completed Jobs";
        private static readonly String PARAM_MINIMUM_BUILDS = "Minimum Number of Builds";
        private static readonly String PARAM_EMAIL_NOTIFICATIONS_FILE = "Notifications File";
        private static readonly String PARAM_MONITOR_PATH_FILE = "Path Monitoring File";
        private static readonly String PARAM_STOP_BUILD = "Stop Build";
        #endregion // Constants

        #region Member Data
        private DateTime m_LastMonitoringFileWriteTime;
        private bool m_JobLock;  //Allows only one job to be going at a time.
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="location"></param>
        public CutscenePackageTask(String location, CommandOptions options)
            : this(new String[] { location }, options)
        {
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CutscenePackageTask(IEnumerable<String> locations, CommandOptions options)
            : base(NAME, DESC, Common.CapabilityType.CutscenePackage, locations, options)
        {
            m_LastMonitoringFileWriteTime = DateTime.MinValue;

            m_JobLock = false;

            String filename = GetSettingsFilename();

            IDictionary<String, Object> parameters = new Dictionary<String, Object>();
            RSG.Base.Xml.Parameters.Load(filename, ref parameters);
            this.Parameters = parameters;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Task update tick (not required for this task).
        /// </summary>
        public override void Update()
        {
            // Allows us to update monitoring locations without restarting the service.
            this.LoadMonitoringLocations();

            bool stopBuild = (bool)this.Parameters[PARAM_STOP_BUILD];
            if (stopBuild == true) return;

            //Only process one job at a time.
            if (m_JobLock == false)
            {
                // Get list of changelists from base class; turn these info Jobs.
                IEnumerable<Changelist> changelists = base.GetSubmittedChangelists();
                if (changelists.Count() > 0)
                {
                    this.MaximumChangelist = changelists.Select(changelist => changelist.Number).Max();
                    Changelist maxChangelist = changelists.Where(changelist => (changelist.Number == this.MaximumChangelist)).First();

                    Log.MessageCtx(LOG_CTX, "Packaging cutscene data up to Change {0}.  Containing {1} changes.", this.MaximumChangelist, changelists.ToList().Count);

                    //Send a single job with all of these changelists.
                    Job packageTask = new Job(this.Options.Branch, CapabilityType.CutscenePackage, maxChangelist);
                    this.EnqueueJob(packageTask);
                    m_JobLock = true;
                }
            }
            
            // Job maintainance; cleaning the queue and associated files.
            this.CleanJobs();
        }

        /// <summary>
        /// Job completed handler.
        /// </summary>
        /// <param name="job"></param>
        public override void JobCompleted(IJob job, IEnumerable<IJobResult> jobResults)
        {
            m_JobLock = false;
        }

        /// <summary>
        /// Loads an XML file that dictates what paths to monitor in Perforce.
        /// </summary>
        private void LoadMonitoringLocations()
        {
            String monitoringFile = this.Config.Environment.Subst((String)this.Parameters[PARAM_MONITOR_PATH_FILE]);
            if (!File.Exists(monitoringFile) && this.MonitoredPaths.Count() == 0)
            {
                this.Log.ErrorCtx(LOG_CTX, "Monitoring file does not exist ({0}).  Asset builder task will not be monitoring any locations.", monitoringFile);
            }

            if (m_LastMonitoringFileWriteTime >= File.GetLastWriteTimeUtc(monitoringFile))
            {
                //No need to reload.
                return;
            }

            (this.MonitoredPaths as List<String>).Clear();
            m_LastMonitoringFileWriteTime = File.GetLastWriteTimeUtc(monitoringFile);

            XDocument xdoc = XDocument.Load(monitoringFile);

            // Read all locations
            ICollection<String> locationsDeserialised = new List<String>();

            bool core = (this.Parameters.ContainsKey(PARAM_MONITOR_CORE_PROJECT) ? Boolean.Parse(this.Parameters[PARAM_MONITOR_CORE_PROJECT].ToString()) : true);
            bool dlc = (this.Parameters.ContainsKey(PARAM_MONITOR_DLC_PROJECTS) ? Boolean.Parse(this.Parameters[PARAM_MONITOR_DLC_PROJECTS].ToString()) : true);

            foreach (XElement elem in xdoc.Descendants("location"))
            {
#warning TODO LPXO: Add some mechanism for removing paths, similar to perforce workspace capabilities
                String path = elem.Attribute("path").Value;
                locationsDeserialised.Add(path);
            }

            // For each deserialised location translate to appropriate location(s)
            ICollection<String> coreLocations = new List<String>();
            ICollection<String> dlcLocations = new List<String>();
            foreach (String location in locationsDeserialised)
            {
                String branchName = this.Options.Branch.Name;
                if (core)
                {
                    Debug.Assert(this.Options.CoreProject.Branches.ContainsKey(branchName),
                        "Core project does not have branch '{0}' defined.", branchName);
                    if (!this.Options.CoreProject.Branches.ContainsKey(branchName))
                        throw (new ArgumentException(String.Format("Core project does not have branch '{0}' defined.", branchName)));

                    string rootProjectLocation = this.Options.CoreProject.Branches[branchName].Environment.Subst(location);
                    coreLocations.Add(rootProjectLocation);
                }

                if (dlc)
                {
                    Debug.Assert(this.Options.Project.Branches.ContainsKey(branchName),
                        "DLC project does not have branch '{0}' defined.", branchName);
                    if (!this.Options.Project.Branches.ContainsKey(branchName))
                        throw (new ArgumentException(String.Format("DLC project does not have branch '{0}' defined.", branchName)));

                    foreach (KeyValuePair<string, IProject> kvp in this.Config.DLCProjects)
                    {
                        string dlcProjectName = kvp.Key;
                        IProject dlcProject = kvp.Value;
                        if (dlcProject.Branches.ContainsKey(branchName))
                        {
                            String dlcProjectLocation = dlcProject.Branches[branchName].Environment.Subst(location);
                            dlcLocations.Add(dlcProjectLocation);
                        }
                        else
                        {
                            this.Log.WarningCtx(LOG_CTX, "DLC project branch does not exist: {0}", branchName);
                        }
                    }
                }
            }

            // Join all core & dlc locations
            ICollection<String> locations = new List<String>();
            locations.AddRange(coreLocations);
            locations.AddRange(dlcLocations);

            if (locations.Any())
            {
                FileMapping[] fms = FileMapping.Create(this.P4, locations.Distinct().ToArray());
                (this.MonitoredPaths as List<String>).AddRange(fms.Select(fm => fm.DepotFilename));
            }
            else
            {
                this.Log.ErrorCtx(LOG_CTX, "There are no monitoring locations, please examine your monitoring file {0}", monitoringFile);
            }
        }

        /// <summary>
        /// Return status information for this task.
        /// </summary>
        /// <returns></returns>
        public override TaskStatus Status()
        {
            return (new TaskStatus(this));
        }

        /// <summary>
        /// File containing information required to send notifications.
        /// </summary>
        /// <returns></returns>
        public override String GetNotificationDataFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_EMAIL_NOTIFICATIONS_FILE]));
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Return settings XML filename.
        /// </summary>
        /// <returns></returns>
        /// This is virtual allowing you to override it if required; although
        /// it's not recommended.
        /// 
        private String GetSettingsFilename()
        {
            return (Path.Combine(this.Config.ToolsConfig, "automation",
                "tasks", String.Format("{0}.xml", this.GetType().Name)));
        }

        /// <summary>
        /// Method to clean out jobs and their files.
        /// </summary>
        private void CleanJobs()
        {
            int minimumBuilds = (int) this.Parameters[PARAM_MINIMUM_BUILDS];
            
            if (this.Jobs.Count() <= minimumBuilds)
                return;

            int hours = (int)this.Parameters[PARAM_HOURS_JOB];
            DateTime modifiedTime = DateTime.UtcNow - TimeSpan.FromHours((double)hours);

            IEnumerable<IJob> completedJobs = this.Jobs.Where(j =>
                JobState.Completed == j.State ||
                JobState.Errors == j.State ||
                JobState.Aborted == j.State);
            foreach (IJob job in completedJobs)
            {
                DateTime completionTime = (job.ProcessedAt + job.ProcessingTime);
                if (completionTime.IsEarlierThan(modifiedTime))
                {
                    CleanJob(job);
                }

                if (this.Jobs.Count() <= minimumBuilds)
                    break;
            }
        }

        /// <summary>
        /// Remove job from our queue and clean necessary files.
        /// </summary>
        /// <param name="job"></param>
        private void CleanJob(IJob job)
        {
            lock (this.m_Jobs)
            {
                IEnumerable<KeyValuePair<JobPriority, IJob>> jobEntries =
                    this.m_Jobs.Where(j => j.Value.ID.Equals(job.ID));
                if (jobEntries.Count() > 0)
                    this.m_Jobs.Remove(jobEntries.First());
            }
            Services.FileTransferService.CleanJobFiles(this.Config, job, this.Log);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.ServerHost.Tasks namespace
