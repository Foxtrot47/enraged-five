using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Text;
using System.Threading;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Client;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Services;
using RSG.Pipeline.Automation.Common.Services.Messages;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Common.JobManagement;
using RSG.Pipeline.Automation.ServerHost.Tasks;
using RSG.Pipeline.Automation.Consumers;
using RSG.Pipeline.Services;
using RSG.Base.Configuration.Bugstar;
using RSG.Interop.Bugstar;
using RSG.Interop.Bugstar.Organisation;
using RSG.Pipeline.Automation.Common.Utils;
using RSG.Pipeline.Automation.Common.Bug;
using RSG.SourceControl.Perforce;

namespace RSG.Pipeline.Automation.ServerHost.Services
{

    /// <summary>
    /// Core automation service; singleton.
    /// </summary>
    /// Rather than have a global server object that handles all of the 
    /// workers and tasks; we add those details into the Automation Service
    /// as thats where they belong.  We enforce that only a single instance
    /// of this exists.
    /// 
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single,
        ConcurrencyMode = ConcurrencyMode.Multiple)]
    internal class AutomationService :
        RSG.Pipeline.Automation.Common.Services.IAutomationService,
        RSG.Pipeline.Automation.Common.Services.IAutomationAdminService
    {
        #region Constants
        /// <summary>
        /// Log context string.
        /// </summary>
        private static readonly String LOG_CTX = "Automation Service";

        /// <summary>
        /// Option to disable config syncing
        /// </summary>
        private static readonly String OPTION_DISABLE_CONFIG_SYNC = "DisableConfigSync";

        /// <summary>
        /// Warning threshold for task ticks.
        /// </summary>
        private static readonly int WARN_TASK_TICK_MS = 5000;        

        /// <summary>
        /// Update thread tick in milliseconds (want this to be quick to service
        /// shutdown's etc fairly rapidly).
        /// </summary>
        private static readonly int UPDATE_TICK = 100;

        /// <summary>
        /// Task update poll sleep in milliseconds.
        /// </summary>
        private static readonly int TASK_UPDATE_TICK = 30000;

        /// <summary>
        /// Job update poll in milliseconds.
        /// </summary>
        private static int CLIENT_UPDATE_TICK = 10000;

        /// <summary>
        /// Millisecond delay before we check for config and dependency file changes
        /// - workers will be trying to register at startup so we enforce this delay
        /// to enable the opportunity to register.
        /// - if a worker were to be unregistered and we rebooted for this config sync it 
        /// would leave the worker unsynced.
        /// </summary>
        private static readonly int CONFIG_SYNC_CHECK_DELAY = 20000;
        
        /// <summary>
        /// Allows tasks to override how often tasks update.
        /// </summary>
        private static readonly String TASK_UPDATE_TICK_OVERRIDE = "Task Update Time";
        
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Command options object.
        /// </summary>
        private CommandOptions Options
        {
            get;
            set;
        }

        /// <summary>
        /// Automation service role.
        /// </summary>
        public CapabilityType Role
        {
            get;
            private set;
        }

        /// <summary>
        /// Task management collection.
        /// </summary>
        /// Get accessor returns a copy of the private collection for thread-
        /// safety.
        /// 
        public IEnumerable<ITask> Tasks
        {
            get { return (new List<ITask>(m_Tasks)); }
        }
        private ICollection<ITask> m_Tasks;

        private IDictionary<Uri, AutomationWorkerServiceConsumer> m_Workers;
        
        /// <summary>
        /// Configuration object.
        /// </summary>
        private RSG.Base.Configuration.IConfig Config { get; set; }

        /// <summary>
        /// Flag whether the automation service is running.
        /// </summary>
        private bool IsRunning { get; set; }

        /// <summary>
        /// Log object.
        /// </summary>
        private IUniversalLog Log { get; set; }

        /// <summary>
        /// The type of shutdown requested.
        /// </summary>
        public ShutdownType ShutdownType { get; private set; }

        /// <summary>
        /// Handy switch to disable config sync ( on by default ).
        /// </summary>
        private bool EnableConfigSync { get; set; }

        /// <summary>
        /// Indicates the config sync has been checked.
        /// - this prevents workers consuming jobs too soon, only to be rebooted as a result
        /// of the config sync.
        /// </summary>
        private bool ConfigSyncChecked { get; set; }

        /// <summary>
        /// Stopwatch counting the time the service has been running for.
        /// </summary>
        private Stopwatch EpochStopWatch { get; set; }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="role">Role for this automated service instance.</param>
        /// 
        public AutomationService(CommandOptions options, IConfig config, CapabilityType role, bool enableConfigSync)
        {
            Debug.Assert(null != config, "Undefined configuration data.");
            if (null == config)
                throw new ArgumentNullException("config");

            this.Options = options;
            this.Config = config;
            this.Log = LogFactory.CreateUniversalLog("Automation Service");
            this.Log.Message("Automation Service Startup");
            this.m_Tasks = new List<ITask>();
            this.m_Workers = new Dictionary<Uri, AutomationWorkerServiceConsumer>();
            this.ShutdownType = ShutdownType.None;
            this.Role = role;
            this.EnableConfigSync = enableConfigSync;
            this.ConfigSyncChecked = false;
            this.EpochStopWatch = new Stopwatch();
            EpochStopWatch.Start();
            
        }
        #endregion // Constructor(s)

        #region IAutomationService Interface Methods
        /// <summary>
        /// Register worker to the automation service; providing information 
        /// about what the worker's capabilities are.
        /// </summary>
        /// <param name="workerConnection"></param>
        /// <param name="capability"></param>
        /// 
        public Guid RegisterWorker(String workerConnection, Capability capability)
        {
            this.Log.DebugCtx(LOG_CTX, "AutomationService::RegisterWorker()");

            // If host and/or workers are shutting down - do not allow workers to register
            if (this.ShutdownType.HasFlag(ShutdownType.WorkersAndHost))
            {
                this.Log.MessageCtx(LOG_CTX, "*** {0} Worker refused registration : A shutdown is in progress ***", workerConnection);
                return Guid.Empty;
            }

            lock (this.m_Workers)
            {
                Uri uri = new Uri(workerConnection);
                if (this.m_Workers.ContainsKey(uri))
                {
                    Debug.Assert(false, String.Format("Worker '{0}' already attached; worker bug?",
                        workerConnection));
                    this.Log.ErrorCtx(LOG_CTX, "Worker '{0}' already attached; worker bug?",
                        workerConnection);

                    AutomationWorkerServiceConsumer worker = this.m_Workers[uri];
                    return (worker.ID);
                }
                else
                {
                    AutomationWorkerServiceConsumer worker =
                        new AutomationWorkerServiceConsumer(workerConnection, capability.Type);
                    this.m_Workers.Add(uri, worker);

                    this.Log.MessageCtx(LOG_CTX, "Worker registered: {0}; capabilities: {1}.",
                        worker.ServiceConnection, worker.Capability.Type);
                    if (CapabilityType.None == worker.Capability.Type)
                        this.Log.WarningCtx(LOG_CTX, "Stupid worker; has no capabilities!");

                    return (worker.ID);
                }
            }
        }

        /// <summary>
        /// Unregister worker from automation service; worker is being stopped,
        /// killed or shutdown.
        /// </summary>
        public void UnregisterWorker(Guid id)
        {
            this.Log.DebugCtx(LOG_CTX, "AutomationService::UnregisterWorker()");
            lock (this.m_Workers)
            {
                AutomationWorkerServiceConsumer worker = this.m_Workers.Values.
                    Where(w => w.ID.Equals(id)).FirstOrDefault();

                if (null != worker)
                {
                    this.Log.MessageCtx(LOG_CTX, "Worker unregistered: {0}.",
                        worker.ServiceConnection);
                    this.m_Workers.Remove(worker.ServiceConnection);
                }
                else
                {
                    this.Log.ErrorCtx(LOG_CTX, "Worker unregister failed; unknown worker: {0}.",
                        id);
                }
            }
        }

        /// <summary>
        /// Queries to see if a worker with a given ID is registered
        /// </summary>
        public bool IsWorkerRegistered(Guid id)
        {
            bool isRegistered = false;

            lock (this.m_Workers)
            {
                isRegistered = (this.m_Workers.Values.FirstOrDefault(worker => worker.ID.Equals(id)) != null);
            }

            this.Log.DebugCtx(LOG_CTX, "AutomationService::IsWorkerRegistered({0}) returning {1}", id.ToString(), isRegistered);

            return isRegistered;
        }

        /// <summary>
        /// Externally enqueue a job onto a task queue; based on the job role
        /// and priority.  This should be used for user-invoked rebuilds.
        /// </summary>
        /// <param name="job"></param>
        /// <returns>true iff successfully enqueued; false otherwise.</returns>
        /// 
        public bool EnqueueJob(IJob job)
        {
            bool result = true;
            this.Log.DebugCtx(LOG_CTX, "AutomationService::EnqueueJob({0})", job.ID);
            
            // Determine whether we have a valid server task bucket for the job.
            ITask task = this.m_Tasks.Where(t => t.Role.HasFlag(job.Role)).FirstOrDefault();
            if (null != task)
            {
                this.Log.MessageCtx(LOG_CTX, "Job {0} queued for task {1}.", job.ID, task.Name);
                task.EnqueueJob(job);

                // Save state.
                foreach (ITask existingtask in this.m_Tasks)
                    existingtask.Save();
            }
            else
            {
                this.Log.ErrorCtx(LOG_CTX, "Job {0} ignored; no valid task to handle role.", job.ID);
                result = false;
            }

            return (result);
        }

        /// <summary>
        /// Job notification to service that a job has completed.
        /// </summary>
        /// <param name="job"></param>
        /// <param name="successful"></param>
        public void JobCompleted(IJob job, IEnumerable<IJobResult> jobResults)
        {
            bool successful = true;

            foreach (IJobResult jobResult in jobResults)
            {
                successful &= jobResult.Succeeded;
            }

            this.Log.DebugCtx(LOG_CTX, "AutomationService::JobCompleted({0} {1})",
                job.ID, successful);

            IJob serverJob = GetServerJob(job.ID);
            if (null == serverJob)
                this.Log.ErrorCtx(LOG_CTX, "Job {0} not found to mark complete.", job.ID);
            else
            {
                AutomationWorkerServiceConsumer worker = null;
                lock (this.m_Workers)
                {
                    worker = this.m_Workers.Values.
                        Where(w => w.AssignedJob.Equals(job.ID)).FirstOrDefault();
                    Debug.Assert(null != worker, String.Format("Worker assigned job {0} not found.", job.ID));
                    if (null != worker)
                        worker.JobCompleted(job);
                }
                serverJob.CompletedAt = DateTime.UtcNow;
                serverJob.ProcessingTime = serverJob.CompletedAt - serverJob.ProcessedAt;
                
                if (!successful)
                    serverJob.State = JobState.Errors;
                else
                    serverJob.State = JobState.Completed;

                ITask task = this.m_Tasks.Where(t => t.Role.HasFlag(job.Role)).FirstOrDefault();
                task.JobCompleted(serverJob, jobResults);
                task.Save();
                try
                {
                    if (task.PostJobNotificationRequired(serverJob))
                    {
                        // Create any emails & bug notifications if required.
                        String logFilename = Services.FileTransferService.GetJobLogFilename(this.Config, serverJob);
                        task.SendPostJobNotifications(serverJob, jobResults, worker.ServiceConnection, worker.ID, logFilename);
                    }
                }
                catch (Exception ex)
                {
                    this.Log.Exception(ex, "Exception sending notifications: {0}", ex.Message);
                }
            }
        }
        #endregion // IAutomationService Interface Methods

        #region IAutomationAdminService Interface Methods
        /// <summary>
        /// Return array of TaskStatus objects for all tasks in the service.
        /// </summary>
        /// <returns></returns>
        public TaskStatus[] Monitor()
        {
            return (this.MonitorFiltered(CapabilityType.All, 0, int.MaxValue));
        }

        /// <summary>
        /// Return array of TaskStatus objects for filtered tasks in the service.
        /// </summary>
        /// <param name="role">Filter of role across tasks</param>
        /// <param name="startJob">Starting job index to begin returning jobs from ( pagination )</param>
        /// <param name="countJob">Max count of jobs to return in the task status</param>
        /// <returns></returns>
        public TaskStatus[] MonitorFiltered(CapabilityType role, int startJob, int countJob)
        {
            this.Log.DebugCtx(LOG_CTX, "AutomationAdminService::MonitorFiltered(role: {0}, start: {1} count: {2})", role, startJob, countJob);

            IEnumerable<ITask> tasksFiltered = this.Tasks.Where(task => role.HasFlag(task.Role));

            ICollection<TaskStatus> status = new List<TaskStatus>();
            foreach (ITask task in tasksFiltered)
            {
#warning DHM FIX ME: see comment.
                // Using Skip and Take below aren't safe unless we add some form of
                // protected.  The IEnumerable's returned cause serialisation screwups
                // (under very strange circumstances that aren't documented).  They
                // have been removed for now.  This is why .ToList "fixed" it.
                // This implied .Net 4.5 makes the use of Skip and Take safe;
                // server running on .Net 4.5 was ok, on .Net 4 not.
                TaskStatus taskStatus = task.Status();
                taskStatus.Jobs = taskStatus.Jobs; //.Skip(startJob).Take(countJob);

                status.Add(taskStatus);
            }
            return (status.ToArray());
        }        
        
        /// <summary>
        /// Return job count for a particular task (used for paginating MonitorFiltered).
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        public int MonitorFilteredCount(CapabilityType role)
        {
            this.Log.DebugCtx(LOG_CTX, "AutomationAdminService::MonitorFilteredCount(role: {0})", role);

            IEnumerable<ITask> tasksFiltered = this.Tasks.Where(task => role.HasFlag(task.Role));
            return (tasksFiltered.Sum(t => t.Jobs.Count()));
        }

        /// <summary>
        /// Return array of WorkerStatus objects for all workers attached to
        /// the service.
        /// </summary>
        /// <returns></returns>
        public WorkerStatus[] Clients()
        {
            this.Log.DebugCtx(LOG_CTX, "AutomationAdminService::Clients()");

            IEnumerable<AutomationWorkerServiceConsumer> workers = null;
            lock (this.m_Workers)
                workers = new List<AutomationWorkerServiceConsumer>(this.m_Workers.Values);

            ICollection<WorkerStatus> status = new List<WorkerStatus>();
            foreach (AutomationWorkerServiceConsumer worker in workers)
            {
                WorkerStatus stat = new WorkerStatus();
                stat.ID = worker.ID;
                stat.ServiceConnection = worker.ServiceConnection;
                stat.Capability = worker.Capability;
                stat.State = worker.GetWorkerState();
                stat.AssignedJob = worker.AssignedJob;
                status.Add(stat);
            }
            return (status.ToArray());
        }

        /// <summary>
        /// Skip processing of the specified job.
        /// </summary>
        /// <param name="job">Job identifier.</param>
        /// <returns></returns>
        public JobResponse SkipJob(Guid id)
        {
            this.Log.DebugCtx(LOG_CTX, "AutomationAdminService::SkipJob({0})", id);
            bool status = false;

            ITask requestedTask = this.GetTaskForServerJob(id);
            if (null != requestedTask)
            {
                status = requestedTask.Skip(id);
            }
            else
            {
                this.Log.ErrorCtx(LOG_CTX, "No Job ID {0} not found in any active task.", id);
            }

            return (new JobResponse(id, JobOp.Skip, status));
        }

        /// <summary>
        /// Prioritise processing of the specified job (next available 
        /// worker).
        /// </summary>
        /// <param name="job">Job identifier.</param>
        /// <returns></returns>
        public JobResponse PrioritiseJob(Guid id)
        {
            this.Log.DebugCtx(LOG_CTX, "AutomationAdminService::PrioritiseJob({0})", id);
            bool status = false;

            ITask requestedTask = this.GetTaskForServerJob(id);
            if (null != requestedTask)
            {
                status = requestedTask.Prioritise(id);
            }
            else
            {
                this.Log.ErrorCtx(LOG_CTX, "No Job ID {0} not found in any active task.", id);
            }
            return (new JobResponse(id, JobOp.Prioritise, status));
        }
        
        /// <summary>
        /// Reprocess processing of the specific job.
        /// </summary>
        /// <param name="job">Job identifier.</param>
        /// <returns></returns>
        public JobResponse ReprocessJob(Guid id)
        {
            this.Log.DebugCtx(LOG_CTX, "AutomationAdminService::ReprocessJob({0})", id);
            bool status = false;

            ITask requestedTask = this.GetTaskForServerJob(id);
            if (null != requestedTask)
            {
                status = requestedTask.Reprocess(id);
            }
            else
            {
                this.Log.ErrorCtx(LOG_CTX, "No Job ID {0} not found in any active task.", id);
            }
            
            return (new JobResponse(id, JobOp.Reprocess, status));
        }

        /// <summary>
        /// Return dictionary of int => String for a particular enum type.
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        /// This service is required because the Microsoft JSON serialiser 
        /// doesn't serialise the String representation; which we typically
        /// want for friendly names etc on our pretty webpages.
        public IDictionary<int, String> EnumValues(String type)
        {
            this.Log.DebugCtx(LOG_CTX, "AutomationAdminService::EnumValues({0})", type);

            // Find type corresponding to type string.
            IDictionary<int, String> result = new Dictionary<int, String>();
            try
            {
                Assembly[] loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies();
                foreach (Assembly assembly in loadedAssemblies)
                {
                    // Get public, non-abstract enumerations.
                    Type typeObject = assembly.GetTypes().Where(t =>
                        t.IsPublic && t.IsEnum && !t.IsAbstract && t.AssemblyQualifiedName.StartsWith(type)).
                        FirstOrDefault();
                    if (null != typeObject)
                    {
                        // Found type; lets get its values and populate our return dictionary.
                        String[] names = Enum.GetNames(typeObject);
                        Array values = Enum.GetValues(typeObject);

                        Debug.Assert(names.Length == values.Length,
                            "Enum definition is borked.");
                        if (names.Length == values.Length)
                        {
                            for (int n = 0; n < names.Length; ++n)
                            {
                                result[(int)values.GetValue(n)] = names[n];
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                this.Log.ExceptionCtx(LOG_CTX, ex, "Error reading enum values: {0}.", type);
            }
            return (result);
        }

        /// <summary>
        /// Return total message count on coordinator log.
        /// </summary>
        /// <returns></returns>
        public Int32 CoordinatorLogMessageCount()
        {
            XDocument xdoc = XDocument.Load(LogFactory.ApplicationLogFilename);
            IEnumerable<UniversalLogFileBufferedMessage> messageList = UniversalLogFile.GetMessageList(xdoc);

            return messageList.Count();
        }

        /// <summary>
        /// Return messages starting at startindx through to messageCount from coordinator log.
        /// </summary>
        /// <returns></returns>
        public XmlDocument CoordinatorLog(Int32 startidx = 0, Int32 messageCount = 0)
        {
            if (startidx < 0 || messageCount < 0)
            {
                this.Log.ErrorCtx(LOG_CTX, "Attempted to query invalid coordinator log message interval: Start {0} Count {1}", startidx, messageCount);
                return null;
            }

            XDocument xdocProcessed = null;
            try
            {
                XDocument xdoc = XDocument.Load(LogFactory.ApplicationLogFilename);

                // Build a list of buffered messages
                IEnumerable<UniversalLogFileBufferedMessage> messageList = UniversalLogFile.GetMessageList(xdoc);
                ICollection<UniversalLogFileBufferedMessage> filteredList = null;

                // Sort out list by timestamp
                (messageList as List<UniversalLogFileBufferedMessage>).Sort(delegate(UniversalLogFileBufferedMessage a, UniversalLogFileBufferedMessage b) { return a.Timestamp.CompareTo(b.Timestamp); });

                // Filter the meshes
                if (startidx == 0 && messageCount == 0 || startidx + messageCount >= messageList.Count())
                {
                    filteredList = messageList.ToList();
                }
                else
                    filteredList = (messageList as List<UniversalLogFileBufferedMessage>).GetRange(startidx, messageCount);

                // Build an xdocument from our filtered collection
                xdocProcessed = UniversalLogFile.TransformToXDocument(filteredList);
            }
            catch(Exception ex)
            {
                this.Log.ExceptionCtx(LOG_CTX, ex, "Exception generating server log for transfer.");
            }

            var xmlDocument = new XmlDocument();
            using(var xmlReader = xdocProcessed.CreateReader())
            {
                xmlDocument.Load(xmlReader);
            }
            return xmlDocument;
        }

        /// <summary>
        /// Host processes shutdown, shutting itself and or its workers down.
        /// </summary>
        /// <param name="shutdownType"></param>
        public void Shutdown(ShutdownType shutdownType, String reason = "No reason given")
        {
            ShutdownType = shutdownType;
 
			ShutdownAlert(this.Log, ShutdownType, String.Format("Shutdown Started: {0}", Environment.MachineName), reason);

			if (!shutdownType.HasFlag(ShutdownType.WorkersAndHost))
            {
                return;
            }

            IEnumerable<AutomationWorkerServiceConsumer> workers = null;
            lock (this.m_Workers)
                workers = new List<AutomationWorkerServiceConsumer>(this.m_Workers.Values);

            this.Log.MessageCtx(LOG_CTX, " ");
            this.Log.MessageCtx(LOG_CTX, reason);
            this.Log.MessageCtx(LOG_CTX, " ");
            this.Log.MessageCtx(LOG_CTX, "*****************************");
            this.Log.MessageCtx(LOG_CTX, "*** Shutting down Workers ***");
            this.Log.MessageCtx(LOG_CTX, "*****************************");
            this.Log.MessageCtx(LOG_CTX, " ");
                
            foreach (AutomationWorkerServiceConsumer worker in workers)
            {
                this.Log.MessageCtx(LOG_CTX, " - Shutting down Worker {0} {1} ({2})", worker.ID, worker.ServiceConnection, worker.GetWorkerState());
                worker.Shutdown(shutdownType);
            }
        }

        /// <summary>
        /// Clears each worker's cache
        /// </summary>
        public void ClearAllWorkersCache()
        {
            IEnumerable<AutomationWorkerServiceConsumer> workers = null;
            lock (this.m_Workers)
                workers = new List<AutomationWorkerServiceConsumer>(this.m_Workers.Values);

            this.Log.MessageCtx(LOG_CTX, " ");
            this.Log.MessageCtx(LOG_CTX, "***********************");
            this.Log.MessageCtx(LOG_CTX, "*** Clearing Caches ***");
            this.Log.MessageCtx(LOG_CTX, "***********************");
            this.Log.MessageCtx(LOG_CTX, " ");

            foreach (AutomationWorkerServiceConsumer worker in workers)
            {
                this.Log.MessageCtx(LOG_CTX, " - Clearing Cache {0} {1} ({2})", worker.ID, worker.ServiceConnection, worker.GetWorkerState());
                worker.FlagClearCache();
            }
        }

        /// <summary>
        /// Start tasks of the given name
        /// </summary>
        /// <param name="taskName"></param>
        public void StartTask(String taskName)
        {
            bool started = false;
            foreach (ITask task in this.Tasks.Where(t => taskName == null || 0 == String.Compare(t.Role.ToString(), taskName, true)))
            {
                this.Log.MessageCtx(LOG_CTX, "***");
                this.Log.MessageCtx(LOG_CTX, "**** Starting Task {0}", task.Role.ToString());
                this.Log.MessageCtx(LOG_CTX, "***");
                task.Start();

                started = true;

                // need to save the state file out so this state persists
                task.Save();
            }

            if (!started)
            {
                this.Log.WarningCtx(LOG_CTX, "Could not find task {0} to start", taskName == null ? "?" : taskName);
            }
        }

        /// <summary>
        /// Stop tasks of the given name
        /// </summary>
        /// <param name="taskName"></param>
        public void StopTask(String taskName)
        {
            bool stopped = false;
            foreach (ITask task in this.Tasks.Where(t => taskName == null || 0 == String.Compare(t.Role.ToString(), taskName, true)))
            {
                this.Log.MessageCtx(LOG_CTX, "***");
                this.Log.MessageCtx(LOG_CTX, "*** Stopping Task {0}", task.Role.ToString());
                this.Log.MessageCtx(LOG_CTX, "***");
                task.Stop();

                stopped = true;

                // need to save the state file out so this state persists
                task.Save();
            }

            if (!stopped)
            {
                this.Log.WarningCtx(LOG_CTX, "Could not find task {0} to stop", taskName == null ? "?" : taskName);
            }
        }

        /// <summary>
        /// Sets a task parameter to a new value
        /// - the param is set right away and is also saved to the task.local.xml param file.
        /// </summary>
        /// <param name="taskName"></param>
        /// <param name="paramName"></param>
        /// <param name="paramValue"></param>
        /// <returns></returns>
        public void SetTaskParam(String taskName, String paramName, String paramValue)
        {
            foreach (ITask task in this.Tasks)
            {
                if (0 == String.Compare(task.Role.ToString(), taskName, true))
                {
                    if (task.Parameters.ContainsKey(paramName))
                    {
                        if (0 == String.Compare(task.Parameters[paramName].ToString(), paramValue, true))
                        {
                            this.Log.WarningCtx(LOG_CTX, "No change to task parameter");
                            break;
                        }
                        else
                        {
                            this.Log.MessageCtx(LOG_CTX, "Overriding Task parameter {0} from {1} to {2}", paramName, task.Parameters[paramName], paramValue);
                        }
                    }
                    else
                    {
                        this.Log.MessageCtx(LOG_CTX, "Setting new Task parameter {0} to {1}", paramName, paramValue);
                    }

                    String filename = Path.Combine(this.Config.ToolsConfig, "automation", "tasks", String.Format("{0}Task.xml", taskName));

                    // Attempt to load a "<filename>.local.xml" file; for local overrides.
                    String extension = String.Format("local{0}", System.IO.Path.GetExtension(filename));
                    String localFilename = System.IO.Path.ChangeExtension(filename, extension);

                    IDictionary<String, Object> parameters = new Dictionary<String, Object>();

                    // Load in existing params
                    if (System.IO.File.Exists(localFilename))
                    {
                        RSG.Base.Xml.Parameters.Load(localFilename, ref parameters);
                    }

                    int integer;
                    bool boolean;

                    // add new or override 
                    // due to the way this is done you may not have a string that is true OR a string that is an integer
                    // feel free to reinvent if you have the time, this will do for now.
                    if (int.TryParse(paramValue, out integer))
                    {
                        parameters[paramName] = integer;
                    }
                    else if (Boolean.TryParse(paramValue, out boolean))
                    {
                        parameters[paramName] = boolean;
                    }
                    else
                    {
                        parameters[paramName] = paramValue;
                    }

                    // Set it now
                    task.Parameters[paramName] = paramValue;

                    // Save out, it will be loaded at task update
                    RSG.Base.Xml.Parameters.Save(localFilename, parameters);
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taskName"></param>
        /// <returns></returns>
        public IDictionary<String, Object> GetTaskParams(String taskName)
        {
            IDictionary<String, Object> dict = new Dictionary<String, Object>();

            foreach (ITask task in this.Tasks)
            {
                if (0 == String.Compare(task.Role.ToString(), taskName, true))
                {
                    foreach (KeyValuePair<String, Object> kvp in task.Parameters)
                    {
                        String paramName = kvp.Key;
                        Object value = kvp.Value;
                        this.Log.MessageCtx(LOG_CTX, "Task Param: {0} = {1}", paramName, value);
                        dict.Add(paramName, value);
                    }
                }
            }

            return dict;
        }

        /// <summary>
        /// Updates the tick rate between client updates
        /// </summary>
        /// <param name="timeMs"></param>
        public void SetClientUpdateTick(Int32 timeMs)
        {
            this.Log.MessageCtx(LOG_CTX, "Updating client update tick rate to {0}ms", timeMs);
            CLIENT_UPDATE_TICK = timeMs;
        }

        /// <summary>
        /// Sets the dispatch scheme to assign job to particular hosts
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="host"></param>
        public void SetJobDispatch(Guid jobId, String host)
            {
            this.Log.MessageCtx(LOG_CTX, "Setting {0} to process on host {1}", jobId, host);

            ITask task = this.GetTaskForServerJob(jobId);
            if (task == null)
                {
                Log.ErrorCtx(LOG_CTX, "Unknown job guid {0} cannto dispatch to {1}", jobId, host);
                return;
                }

            IDispatchSchemeTask dispatchSchemeTask = task as IDispatchSchemeTask;
            if (dispatchSchemeTask == null)
            {
                Log.ErrorCtx(LOG_CTX, "Task for job guid {0} does not support dispatching", jobId);
                return;
            }

            DispatchScheme dispatchScheme = dispatchSchemeTask.DispatchScheme;
            DispatchGroup dispatchGroup = dispatchScheme.Groups.FirstOrDefault(g => g.MachineName.Equals(host, StringComparison.CurrentCultureIgnoreCase));
            if (dispatchGroup == default(DispatchGroup))
            {
                Log.ErrorCtx(LOG_CTX, "There is no existing dispatch group for {0}, you cannot assign to arbitrary hosts.", host);
                return;
        }

            Log.MessageCtx(LOG_CTX, "***");
            Log.MessageCtx(LOG_CTX, "*** DISPATCH UPDATE : Job {0} CAN be dispatched to {1}", jobId, host);
            Log.MessageCtx(LOG_CTX, "***");
            dispatchGroup.JobGuids.Add(jobId);          
        }
        #endregion // IAutomationAdminService Interface Methods

        #region Controller Methods
        /// <summary>
        /// Start automation service internal processing.
        /// </summary>
        public void Start()
        {
            this.InitialiseTasks();

            this.Log.Message("Starting task update thread.");
            Thread taskUpdateThread = new Thread(new ThreadStart(this.TaskUpdateThread))
            {
                Name = String.Format("Automation Service: TaskUpdateThread")
            };
            taskUpdateThread.Start();

            this.Log.Message("Starting worker updates.");
            this.IsRunning = true;

            StartupAlert(this.Log, string.Format("Started: {0}", Environment.MachineName));

            while (this.IsRunning)
            {
                this.ClientUpdate();
                ShutdownHandler();
                Thread.Sleep(CLIENT_UPDATE_TICK);
            }
        }
        
        /// <summary>
        /// Stop automation service internal processing.
        /// </summary>
        public void Stop(ShutdownType shutdownType)
        {
            if (this.IsRunning)
            {
                // Disconnect workers.
                // - the workers are possibly already shutdown..
                lock (this.m_Workers)
                {
                    foreach (AutomationWorkerServiceConsumer client in this.m_Workers.Values)
                        client.Shutdown(shutdownType);
                }

                // Save state.
                foreach (ITask task in this.m_Tasks)
                    task.Save();

                this.IsRunning = false;                
            }
            Thread.Sleep(UPDATE_TICK);
        }


        /// <summary>
        /// Exception Notification
        /// - suited for top level exceptions.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="exception"></param>
        /// <param name="summary"></param>
        /// <param name="detail"></param>
        public void ExceptionAlert(ILog log, Exception exception, string summary = null, string detail = null)
        {
            try
            {
                if (Tasks != null)
                {
                    foreach (ITask task in Tasks)
                    {
                        task.ExceptionAlert(log, exception, summary, detail);
                    }
                }
            }
            catch
            {
                // Log warning so not to pollute the logs...
                log.Warning("Exception notification threw a further exception.");
            }
        }

        /// <summary>
        /// Shutdown Notification
        /// </summary>
        /// <param name="log"></param>
        /// <param name="shutdownType"></param>
        /// <param name="summary"></param>
        /// <param name="detail"></param>
        public void ShutdownAlert(ILog log, ShutdownType shutdownType, string summary = null, string detail = null)
        {
            try
            {
                if (Tasks != null)
                {
                    foreach (ITask task in Tasks)
                    {
                        task.ShutdownAlert(log, shutdownType, summary, detail);
                    }
                }
            }
            catch (Exception ex)
            {
                log.ToolException(ex, "Shutdown Alert Exception");
            }
        }

        /// <summary>
        /// Startup Notification
        /// </summary>
        /// <param name="log"></param>
        /// <param name="summary"></param>
        /// <param name="detail"></param>
        public void StartupAlert(ILog log, string summary = null, string detail = null)
        {
            try
            {
                if (Tasks != null)
                {
                    foreach (ITask task in Tasks)
                    {
                        task.StartupAlert(log, summary, detail);
                    }
                }
            }
            catch (Exception ex)
            {
                log.ToolException(ex, "Startup Alert Exception");
            }
        }

        /// <summary>
        /// General Alert Notification
        /// </summary>
        /// <param name="log"></param>
        /// <param name="summary"></param>
        /// <param name="detail"></param>
        public void GeneralAlert(ILog log, string summary = null, string detail = null)
        {
            try
            {
                if (Tasks != null)
                {
                    foreach (ITask task in Tasks)
                    {
                        task.GeneralAlert(log, summary, detail, null);
                    }
                }
            }
            catch (Exception ex)
            {
                log.ToolException(ex, "General Alert Exception");
            }
        }
        #endregion // Controller Methods

        #region Private Methods

        private void ShutdownHandler()
        {
            if (ShutdownType.HasFlag(ShutdownType.WorkersAndHost))
            {
                // Don't shutdown the host until the clients are shutdown
                if (Clients().Length > 0)
                {
                    this.Log.MessageCtx(LOG_CTX, "*** Waiting for {0} clients to shutdown ***", Clients().Length);
                    return;
                }
                else if (Clients().Length == 0)
                {
                    this.Log.MessageCtx(LOG_CTX, "*** No longer waiting for clients to shutdown ***");
                }

                // Initiate Host shutdown
                this.Log.MessageCtx(LOG_CTX, " ");
                this.Log.MessageCtx(LOG_CTX, "**************************");
                this.Log.MessageCtx(LOG_CTX, "*** Shutting Down Host ***");
                this.Log.MessageCtx(LOG_CTX, "**************************");
                this.Log.MessageCtx(LOG_CTX, " ");
                Stop(ShutdownType);
            }
        }

        /// <summary>
        /// Initialise tasks based on configured capabilities.
        /// </summary>
        private void InitialiseTasks()
        {
            // Depending on our roles we initialise tasks.
            if (this.Role.HasFlag(CapabilityType.SchedulingUnitTesting))
            {
                this.Log.Message("Scheduling Unit Test Enabled.");
                lock (this.m_Tasks)
                {
                    this.m_Tasks.Add(new SchedulingTestTask(this.Config.ToolsRoot, this.Options));
                }
            }
            if (this.Role.HasFlag(CapabilityType.MapExport))
            {
                this.Log.Message("Map Export Capability Enabled.");
                lock (this.m_Tasks)
                {
#warning DHM FIX ME: maps add to config data.
                    this.m_Tasks.Add(new MapExportTask(System.IO.Path.Combine(this.Config.Project.DefaultBranch.Art, "Models"), this.Options));
                }
            }
            if (this.Role.HasFlag(CapabilityType.MapExportNetwork))
            {
                this.Log.Message("Map Network Export Capability Enabled.");
                lock (this.m_Tasks)
                {
                    this.m_Tasks.Add(new MapExportNetworkTask(this.Options));
                }
            }
            if (this.Role.HasFlag(CapabilityType.AssetBuilder))
            {
                this.Log.Message("Asset Builder Capability Enabled.");
                lock (this.m_Tasks)
                {
                    // AssetBuilder loads its monitoring locations from XML.
                    this.m_Tasks.Add(new AssetBuilderTask(this.Options));
                }
            }
            if (this.Role.HasFlag(CapabilityType.CutsceneExport))
            {
                this.Log.Message("Cutscene Export Capability Enabled.");
                lock (this.m_Tasks)
                {
                    this.m_Tasks.Add(new CutsceneExportTask(this.Options));
                }
            }
            if (this.Role.HasFlag(CapabilityType.CutsceneExportNetwork))
            {
                this.Log.Message("Cutscene Network Export Capability Enabled.");
                lock (this.m_Tasks)
                {
                    this.m_Tasks.Add(new CutsceneExportNetworkTask(this.Options));
                }
            }
            if (this.Role.HasFlag(CapabilityType.CutsceneScripter))
            {
                this.Log.Message("Cutscene Scriptor Capability Enabled.");
                lock (this.m_Tasks)
                {
                    this.m_Tasks.Add(new CutsceneScripterTask(this.Options));
                }
            }
            if (this.Role.HasFlag(CapabilityType.UserDetails))
            {
                this.Log.Message("User Details Capability Enabled.");
                lock (this.m_Tasks)
                {
                    this.m_Tasks.Add(new UserDetailsTask(this.Options));
                }
            }
            if (this.Role.HasFlag(CapabilityType.CutscenePackage))
            {
                this.Log.Message("Cutscene Package Capability Enabled.");
                lock (this.m_Tasks)
                {
                    this.m_Tasks.Add(new CutscenePackageTask(new List<String>(),this.Options));
                }
            }
            if (this.Role.HasFlag(CapabilityType.MapGTXDExportTask))
            {
                this.Log.Message("Map Export GTXD Capability Enabled.");
                lock (this.m_Tasks)
                {
                    this.m_Tasks.Add(new MapGtxdExportTask(this.Options));
                }
            }
    		if (this.Role.HasFlag(CapabilityType.SmokeTester))
            {
                this.Log.Message("Smoke Tester Capability Enabled.");
                lock (this.m_Tasks)
                {
                    this.m_Tasks.Add(new SmokeTestTask(this.Options));
                }
            }
            if (this.Role.HasFlag(CapabilityType.CodeBuilder))
            {
                this.Log.Message("Code Builder Capability Enabled.");
                lock (this.m_Tasks)
                {
                    this.m_Tasks.Add(new CodeBuilderTask(new List<String>(), this.Options));
                }
            }
            if (this.Role.HasFlag(CapabilityType.Integrator))
            {
                this.Log.Message("Integrator Capability Enabled.");
                lock (this.m_Tasks)
                {
                    this.m_Tasks.Add(new IntegratorTask(this.Options));
                }
            }
            if (this.Role.HasFlag(CapabilityType.CodeBuilder2))
            {
                this.Log.Message("Codebuilder 2 Capability Enabled.");
                lock (this.m_Tasks)
                {
                    this.m_Tasks.Add(new CodeBuilder2Task(this.Options));
                }
            }
            if (this.Role.HasFlag(CapabilityType.ScriptBuilder))
            {
                this.Log.Message("Script Builder Capability Enabled.");
                lock (this.m_Tasks)
                {
                    this.m_Tasks.Add(new ScriptBuilderTask(this.Options));
                }
            }

            if (this.Role.HasFlag(CapabilityType.CommandRunner))
            {
                this.Log.Message("Command Runner Capability Enabled.");
                lock (this.m_Tasks)
                {
                    this.m_Tasks.Add(new CommandRunnerTask(this.Options));
                }
            }
            if (this.Role.HasFlag(CapabilityType.DatabaseManager))
            {
                this.Log.Message("Database Manager Capability Enabled.");
                lock (this.m_Tasks)
                {
                    this.m_Tasks.Add(new DatabaseManagerTask(this.Options));
                }
            }

            if (this.Role.HasFlag(CapabilityType.CutSceneRPFBuilder))
            {
                this.Log.Message("CutScene RPF Builder Capability Enabled.");
                lock (this.m_Tasks)
                {
                    this.m_Tasks.Add(new CutSceneRPFBuilderTask(this.Options));
                }
            }

            if (this.Role.HasFlag(CapabilityType.FrameCapture))
            {
                this.Log.Message("Frame Capture Capability Enabled.");
                lock (this.m_Tasks)
                {
                    this.m_Tasks.Add(new FrameCaptureTask(this.Options));
                }
            }

            this.Log.Message("Starting task thread updates.");
            foreach (ITask task in this.m_Tasks)
            {
                task.Load();

                // after loading state decide whether to start or stop or other...
                switch (task.TaskState)
                {
                    case TaskState.Unknown:
                        this.Log.Warning("Task state unknown - perhaps there was no state to load - assuming it should be started");
                task.Start();
                        break;
                    case TaskState.Active:
                        task.Start();
                        break;
                    case TaskState.Inactive:
                        task.Stop();
                        break;
                    default:
                        this.Log.Error("Unhandled task state", task.TaskState);
                        break;
            }
        }
        }

        /// <summary>
        /// Task update thread invocation.  Handles the maintainance of calling
        /// task update calls; creating Jobs which are farmed out in the worker
        /// update (below).
        /// </summary>
        private void TaskUpdateThread()
        {
            // Note we do not start the stopwatch - if a stopwatch is not running we run the task immediately as this indicates we are executing the first task tick.
            IDictionary<ITask, Stopwatch> taskStopwatches = this.Tasks.ToDictionary(t => t, t => new Stopwatch());

            while (this.IsRunning)
            {
                // Only run config sync once when there are multiple tasks to run in a tick.
                bool runConfigSyncThisTick = false;

                // For each task update it if it's time to tick.  
                foreach (ITask task in this.Tasks)
                {
                    // If not active do not update
                    if (task.TaskState != TaskState.Active)
                    {
                        this.Log.WarningCtx(LOG_CTX, " ");
                        this.Log.WarningCtx(LOG_CTX, "***");
                        this.Log.WarningCtx(LOG_CTX, "*** Task: '{0}' is not active it will not update as it is in task state '{1}'.", task.Name, task.TaskState);
                        this.Log.WarningCtx(LOG_CTX, "***");
                        this.Log.WarningCtx(LOG_CTX, " ");
                        continue;
                    }

                    int taskUpdateTick = task.Parameters.ContainsKey(TASK_UPDATE_TICK_OVERRIDE) ? (int)task.Parameters[TASK_UPDATE_TICK_OVERRIDE] : TASK_UPDATE_TICK;

                    // If stopwatch not running we run immediately as this indicates the first task tick ( thereafter it will be running ) 
                    bool stopWatchRunning = taskStopwatches[task].IsRunning;
                    if (!stopWatchRunning || taskStopwatches[task].ElapsedMilliseconds > taskUpdateTick)
                    {
                        this.Log.MessageCtx(LOG_CTX, "Task Update Tick ({0} ms).", taskUpdateTick);
                        if (!stopWatchRunning && taskUpdateTick < WARN_TASK_TICK_MS)
                        {
                            this.Log.WarningCtx(LOG_CTX, "Task Update Tick ({0} ms) is very fast - please ensure you are are not hammering perforce or any other services.", taskUpdateTick);
                        }

                        if (!runConfigSyncThisTick)
                        {
                            // Outside of task updates periodically check if a tools config/content resync is required.
                            ResyncToolsConfig();
                            runConfigSyncThisTick = true;
                        }

                        task.Update();
                        taskStopwatches[task].Restart();

                        this.Log.MessageCtx(LOG_CTX, "Task: {0} has {1} jobs. {2} are Pending.", task.Name, task.Jobs.Count(), (task.PendingJobs.Count()));
                    }
                }

                Thread.Sleep(UPDATE_TICK);
            }
        }

        /// <summary>
        /// Control logic that checks if tools config syncing is required, if so trigger a shutdown of that type.
        /// - inclusive of content tree if desired.
        /// *** We DO NOT SYNC to these files here, this will be done outside of the server process by using the bootstrapping mechanisms. ***
        /// </summary>
        private void ResyncToolsConfig()
        {
            // Option based flag to disable.
            if (!this.EnableConfigSync)
                return;

            // Already shutting down.
            if (this.ShutdownType != ShutdownType.None)
                return;

            // This heuristic delay ensures that workers get the opportunity to register 
            // in order that they can be restarted with the instruction to sync config.
            if (this.EpochStopWatch.ElapsedMilliseconds < CONFIG_SYNC_CHECK_DELAY)
            {
                Log.Message("{0}s till config sync checking begins. Workers will not receive jobs until sync checked.", (CONFIG_SYNC_CHECK_DELAY - this.EpochStopWatch.ElapsedMilliseconds) / 1000);
                return;
            }
            else if (!this.ConfigSyncChecked)
            {
                Log.Message(" ");
                Log.Message("--- 'Config Restart & Sync Mechanism' is now active and will recur every task update tick. ---");
                Log.Message("--- Workers can now consume jobs. ---");
                Log.Message(" ");
            }

            // Allow the tasks to decide on what is worth syncing to.
            bool syncToolsConfig = false;
            bool syncToolsContent = false;
            foreach (ITask task in this.Tasks)
            {
                if (task.Parameters.ContainsKey(TaskBase.PARAM_AUTO_SYNC_TOOLS_CONFIG) && (bool)task.Parameters[TaskBase.PARAM_AUTO_SYNC_TOOLS_CONFIG])
                {
                    syncToolsConfig = true;
                }

                if (task.Parameters.ContainsKey(TaskBase.PARAM_AUTO_SYNC_CONTENT_TREE) && (bool)task.Parameters[TaskBase.PARAM_AUTO_SYNC_CONTENT_TREE])
                {
                    syncToolsContent = true;
                }
            }

            if (syncToolsConfig || syncToolsContent)
            {
                ICollection<String> configFilesNotOnHead = new List<String>();
                StringBuilder sb = new StringBuilder();

                // Get the names of he config files that the client workspace is not synced to head on.        
                using (P4 p4 = Config.Project.SCMConnect())
                {
                    //bool enabled = P4.Log.Enabled; // because P4 logging is too noisy for the task update tick
                    //P4.Log.Enabled = false;
                    /*p4.CallingProgram = String.Format("AutomationService : {0} - {1}() - {2}", Assembly.GetExecutingAssembly().GetName(), System.Reflection.MethodBase.GetCurrentMethod().Name, this.Role);
                    p4.Connect();*/

                    IEnumerable<String> configDepotFiles = ConfigSync.DepotFilesTriggeringRestartResync(p4, this.Options.Branch, this.Config, syncToolsConfig, syncToolsContent);

                    if (!configDepotFiles.Any())
                    {
                        Log.Error("No tools config files found, this has been seen, but is unexplained and shouldn't be possible.");
                        return;
                    }

                    FileState[] filestates = FileState.Create(p4, configDepotFiles.ToArray());

                    //P4.Log.Enabled = enabled;

                    Log.Message(" ");
                    Log.Debug(" - Tools Auto Check (Restart & Sync) config files...", String.Join(" ", configDepotFiles));
                    Log.Message(" - Tools Auto Check (Restart & Sync) config files...");
                    
                    foreach (FileState filestate in filestates)
                    {
                        // If we are not on the head revision for this file consider it.
                        // If it was deleted this does not constitute a reason to sync to it.
                        if (filestate.HaveRevision != filestate.HeadRevision &&
                            filestate.HeadRevision > 0 &&
                            (filestate.HeadAction != FileAction.Delete && filestate.HeadAction != FileAction.MoveAndDelete))
                        {
                            // Ensure we do not have this file open - if you hacked a file locally we don't want to be eternally rebooting to sync to a file we can't sync!
                            if (filestate.OpenAction == FileAction.Add || filestate.OpenAction == FileAction.Edit)
                            {
                                String s = String.Format("\tConfig file {0} : Open action is '{1}' : *** This file will not trigger a config sync. *** It is not on head revision ({2}/{3}).", filestate.DepotFilename, filestate.OpenAction, filestate.HaveRevision, filestate.HeadRevision);
                                sb.AppendLine(s);
                                Log.Warning(s);
                            }
                            else
                            {
                                String s = String.Format("*** Config file {0} will trigger the config restart & resync mechanism. It is not on head revision ({1}/{2}). ***", filestate.DepotFilename, filestate.HaveRevision, filestate.HeadRevision);
                                sb.AppendLine(s);
                                Log.Warning(s);
                                configFilesNotOnHead.Add(filestate.ClientFilename);
                            }
                        }
                    }
                }

                // Some config files are not on head - issue a shutdown sync config.
                if (configFilesNotOnHead.Count() > 0)
                {
                    // This shutdown will send an alert.
                    ShutdownType shutdownType = (syncToolsConfig ? ShutdownType.ScmSyncConfig : 0) | (syncToolsContent ? ShutdownType.ScmSyncContent : 0) | ShutdownType.WorkersAndHost | ShutdownType.Resume;
                    Shutdown(shutdownType, String.Format("Syncing to tools configuration/content data ( requires restart )\n{0}", sb));
                }
                else
                {
                    Log.Message(" - Tools Auto Check on #head");
                }
                Log.Message(" ");
            }
            
            // Enabling this flag will now allow clients to consume jobs.
            //- However, if a shutdown was just triggered they wouldn't be able to process jobs until that had completed.
            this.ConfigSyncChecked = true;
        }

        /// <summary>
        /// Job update tick call; check worker status and pass out more jobs
        /// if worker is idle.
        /// </summary>
        /// Note: in the original MetaJob class this was handled at that level;
        /// this is now central to the Automation Service.
        /// 
        private void ClientUpdate()
        {
            // Clean out the dead wood
            this.ClearUnresponsiveWorkers();

            // Local copy of workers for enumerating safely if workers are
            // attaching and detaching.
            IEnumerable<AutomationWorkerServiceConsumer> workers = null;
            lock (this.m_Workers)
                workers = new List<AutomationWorkerServiceConsumer>(this.m_Workers.Values);

            this.Log.MessageCtx(LOG_CTX, "Job update tick ({0} clients).",
                workers.Count());

            foreach (AutomationWorkerServiceConsumer worker in workers)
            {
                if (worker.State == WorkerConsumerState.Unresponsive)
                {
                    foreach (ITask task in this.Tasks)
                    {
                        IJob failedJob = task.Jobs.Where(j => j.ID == worker.AssignedJob).FirstOrDefault();
                        failedJob.State = JobState.ClientError;
                        this.Log.MessageCtx(LOG_CTX, String.Format("Failing job {0} because worker {1} has become unresponsive.", failedJob.ID.ToString(), worker.ServiceConnection));
                    }
                    continue;
                }
                WorkerState state = worker.GetWorkerState();

                if (WorkerState.Idle != state)
                    continue;

                foreach (ITask task in this.Tasks.OrderByDescending(t => t.Priority))
                {
                    Log.MessageCtx(LOG_CTX, "Checking {0} for role {1}", worker.ServiceConnection.Host, task.Role.ToString());
                    if (!worker.Capability.Type.HasFlag(task.Role))
                        continue; // No match for task so we skip.

                    Log.MessageCtx(LOG_CTX, "{0} has {1} jobs", task.Role.ToString(), task.Jobs.Count());
                    if (0 == task.Jobs.Count())
                        continue; // No jobs to farm out.

                    Log.MessageCtx(LOG_CTX, "Config Sync Checked {0}", ConfigSyncChecked);
                    if (!this.ConfigSyncChecked)
                        continue; // Let config sync check before farming jobs out.

                    // If we only have a single worker always take the next pending job.
                    IJob job = task.GetSuitableJob(worker.ServiceConnection.Host);

                    if (null == job)
                    {
                        this.Log.Message("No pending jobs for worker {0}@'{1}'.", worker.ID, worker.ServiceConnection.Host);
                    }
                    else
                    {
                        Log.Message("Requesting worker {0}@'{1}' to process job {2}.", worker.ID, worker.ServiceConnection.Host, job.ID);
                        task.SendPreJobNotifications(job, worker.ServiceConnection, worker.ID);
                        task.SetCurrentJob(job);
                        worker.Process(job);
                        task.SendAssignedJobNotifications(job, worker.ServiceConnection, worker.ID);
                        job.ProcessingHost = worker.ServiceConnection.Host;
                        }
                    }                        
                }
            }
#warning LPXO: I'm not sure whether this will be required.  It depends how complex this system ends up being.
        /*
        /// <summary>
        /// 
        /// </summary>
        /// <param name="worker"></param>
        /// <param name="task"></param>
        /// <returns></returns>
        private IJob GetSuitableJob(ServiceClients.AutomationWorkerConsumer worker, ITask task)
        {
            // Local copy of workers for enumerating safely if workers are
            // attaching and detaching.
            IJobManagedTask jobManagedTask = task as IJobManagedTask;

            IEnumerable<ServiceClients.AutomationWorkerConsumer> workers = null;
            lock (this.m_Workers)
                workers = new List<ServiceClients.AutomationWorkerConsumer>(this.m_Workers.Values);

            ServiceClients.AutomationWorkerConsumer defaultWorker = GetDefaultWorker(workers.ToList(), task);

            return task.GetSuitableJob(worker.MachineName);
        }

        /// <summary>
        /// Find a worker that hasn't been dedicated to a group and if there isn't one, find the lowest priority groups machine.
        /// </summary>
        /// <param name="workers"></param>
        /// <returns></returns>
        private ServiceClients.AutomationWorkerConsumer GetDefaultWorker(List<ServiceClients.AutomationWorkerConsumer> workers, ITask task)
        {
            IJobManagedTask jobManagedTask = task as IJobManagedTask;
            var reservedWorkers = from jobGroup in jobManagedTask.JobGroupManager.Groups
                                                  where jobGroup.MachineName != String.Empty
                                                  select jobGroup.MachineName;
                                                  
            ServiceClients.AutomationWorkerConsumer defaultWorker = workers.Where(w => !reservedWorkers.Contains(w.MachineName)).FirstOrDefault();
            if (defaultWorker == null)
            {
                defaultWorker = workers.Where(w => w.MachineName == jobManagedTask.JobGroupManager.Groups.Reverse().FirstOrDefault().MachineName).FirstOrDefault();
            }

            return defaultWorker;
        }
        */
          
        /// <summary>
        /// Remove any Worker Consumer objects that have fallen off the radar.
        /// </summary>
        private void ClearUnresponsiveWorkers()
        {
            lock (this.m_Workers)
            {
                List<KeyValuePair<Uri, AutomationWorkerServiceConsumer>> unresponsiveWorkers = new List<KeyValuePair<Uri, AutomationWorkerServiceConsumer>>();
                unresponsiveWorkers.AddRange(this.m_Workers.Where(kvp => kvp.Value.State == WorkerConsumerState.Unresponsive).ToList());
                foreach (KeyValuePair<Uri, AutomationWorkerServiceConsumer> worker in unresponsiveWorkers)
                {
                    this.Log.MessageCtx(LOG_CTX, "Removing unresponsive worker {0}.", worker.Value.ServiceConnection);
                    GeneralAlert(this.Log, string.Format("Unresponsive worker at '{0}' was unregistered", worker.Key));
                    this.m_Workers.Remove(worker.Key);
                }
            }
        }

        /// <summary>
        /// Return server IJob from the owning task.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private IJob GetServerJob(Guid id)
        {
            IJob job = null;
            foreach (ITask task in this.m_Tasks)
            {
                job = task.Jobs.Where(j => j.ID.Equals(id)).FirstOrDefault();
                if (null != job)
                    return (job);
            }
            return (job);
        }

        /// <summary>
        /// Return ITask owner of server IJob.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private ITask GetTaskForServerJob(Guid id)
        {
            foreach (ITask task in this.m_Tasks)
            {
                IJob job = task.Jobs.Where(j => j.ID.Equals(id)).FirstOrDefault();
                if (null != job)
                    return (task);
            }
            return (null);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.ServerHost.Services namespace
