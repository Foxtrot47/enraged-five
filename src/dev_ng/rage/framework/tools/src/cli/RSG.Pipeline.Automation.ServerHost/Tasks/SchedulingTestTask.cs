﻿using System;
using System.Collections.Generic;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Services;

namespace RSG.Pipeline.Automation.ServerHost.Tasks
{

    /// <summary>
    /// Automation scheduler unit testing task.
    /// </summary>
    internal class SchedulingTestTask :
        MonitoringTaskBase,
        IMonitoringTask
    {
        #region Constants
        private static readonly String NAME = "Scheduling Unit Test";
        private static readonly String DESC = "Scheduling unit testing monitoring task.";
        #endregion // Constants
        
        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="server"></param>
        /// <param name="location"></param>
        public SchedulingTestTask(String location, CommandOptions options)
            : this(new String[] { location }, options)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="server"></param>
        /// <param name="locations"></param>
        public SchedulingTestTask(IEnumerable<String> locations, CommandOptions options)
            : base(NAME, DESC, CapabilityType.SchedulingUnitTesting, locations, options)
        {
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Scheduling update.
        /// </summary>
        public override void Update()
        {
            // Get list of changelists from base class; turn these info Jobs.
            IEnumerable<Changelist> changelists = base.GetSubmittedChangelists();
            if (changelists.Count() > 0)
                this.MaximumChangelist = changelists.First().Number;

            foreach (Changelist change in changelists)
            {
                List<String> files = new List<String>();
                FileMapping[] fms = new FileMapping[] { };
                if (change.Files.Count() > 0)
                    fms = FileMapping.Create(this.P4, change.Files.ToArray());

                files.AddRange(fms.Select(fm => fm.DepotFilename));
                if (files.Count() > 0)
                {
                    IJob addjob = new Job(this.Options.Branch, CapabilityType.SchedulingUnitTesting, change);
                    this.EnqueueJob(addjob);
                }
            }
        }
        #endregion // Controller Methods
    }

} // RSG.Pipeline.Automation.ServerHost.Tasks namespace
