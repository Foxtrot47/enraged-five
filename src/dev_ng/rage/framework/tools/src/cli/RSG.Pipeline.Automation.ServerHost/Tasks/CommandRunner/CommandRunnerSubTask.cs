﻿using RSG.Base.Configuration;
using RSG.Pipeline.Automation.Common.Jobs.JobData;
using RSG.Pipeline.Automation.Common.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Automation.ServerHost.Tasks.CommandRunner
{
        /// <summary>
        /// Used to define a sub task of Command Runner. Since Command Runner is one 
        /// type we need a way top store the data for the task XMLs.
        /// </summary>
        public class CommandRunnerSubTask
        {
            public CommandOptions Options { get; set; }
            public string Branch { get; set; }
            public string Machine { get; set; }
            public string Name { get; set; }
            public string TargetDir { get; set; }
            public string PublishDir { get; set; }
            public string TargetRegex { get; set; }
            public int SkipConsume { get; set; }

            public ICollection<String> MonitorPaths { get; set; }
            public ICollection<CommandRunnerJobInfo> PrebuildCommands { get; set; }
            public ICollection<CommandRunnerJobInfo> Commands { get; set; }
            public ICollection<CommandRunnerJobInfo> PostBuildCommands { get; set; }
            public ICollection<IEmailRecipient> Notifications { get; set; }


            private static readonly string XML_PRE_BUILD_COMMANDS = "prebuildcommands";
            private static readonly string XML_POST_BUILD_COMMANDS = "postbuildcommands";
            private static readonly string XML_MAIN_BUILD_COMMANDS = "mainbuildcommands";
            
            private static readonly string XML_PUBLISHING = "publishing";
            private static readonly string XML_TARGET_DIR = "targetdirectory";
            private static readonly string XML_PUBLISH_DIR = "publishdirectory";
            private static readonly string XML_TARGET_REGEX = "targetregex";

            private static readonly string XML_COMMAND = "command";
            private static readonly string XML_ERROR_REGEX = "errorregex";
            private static readonly string XML_WARNING_REGEX = "warningregex";
            private static readonly string XML_PROCESS = "process";
            private static readonly string XML_ARGS = "args";

            private static readonly string XML_MONITORING_PATHS = "monitoringpaths";
            private static readonly string XML_MONITORING_PATH = "monitoringpath";

            private static readonly string XML_EMAIL_RECIPIENTS = "emailrecipients";
            private static readonly string XML_EMAIL_RECIPIENT = "emailrecipient";
            private static readonly string XML_NAME = "name";
            private static readonly string XML_EMAIL = "email";
            private static readonly string XML_RULES = "rules";
            private static readonly string XML_RULE = "rule";
            private static readonly string XML_BRANCH = "branchname";
            private static readonly string XML_SKIP_CONSUME = "skipconsume";

            #region Constructors

            /// <summary>
            /// Takes an XElement a task file and populates the task
            /// </summary>
            /// <param name="options"></param>
            /// <param name="element"></param>
            public CommandRunnerSubTask(CommandOptions options, XElement element)
            {
                Options = options;
                
                LoadFromXML(element);
            }
            #endregion //Constructors

            /// <summary>
            /// Used to load any type of command pre, main or post
            /// </summary>
            /// <param name="commandCollection">Pass in the phase this command should be added to</param>
            /// <param name="command">XElement of the command</param>
            private void LoadCommand(ICollection<CommandRunnerJobInfo> commandCollection, XElement command)
            {
                try
                {
                    String errorRegex = String.Empty;
                    String warningRegex = String.Empty;

                    XElement errorRegexAttr = command.Element(XML_ERROR_REGEX);
                    if (errorRegexAttr != null)
                    {
                        errorRegex = errorRegexAttr.Value;
                    }

                    XElement warningRegexAttr = command.Element(XML_WARNING_REGEX);
                    if (warningRegexAttr != null)
                    {
                        warningRegex = warningRegexAttr.Value;
                    }

                    #warning TODO: Make this better so we can handle attributes that don't exists
                    commandCollection.Add(new CommandRunnerJobInfo(
                                                Environment.ExpandEnvironmentVariables(this.Options.Branch.Environment.Subst(command.Element(XML_PROCESS).Value)).Replace("/", "\\"),
                                                Environment.ExpandEnvironmentVariables(this.Options.Branch.Environment.Subst(command.Element(XML_ARGS).Value)).Replace("/", "\\"),
                                                errorRegex,
                                                warningRegex
                    ));
                }
                catch (Exception e){}
            }

            private void LoadFromXML(XElement task)
            {
                Name = task.Attribute("name").Value;
                Machine = task.Attribute("machine").Value;

                XElement branchElement = task.Element(XML_BRANCH);
                if (branchElement != null)
                {
                    Branch = branchElement.Value;
                }

                XElement skipConsume = task.Element(XML_SKIP_CONSUME);
                if (skipConsume != null)
                {
                    SkipConsume = int.Parse(skipConsume.Value);
                }
                else
                {
                    SkipConsume = 0;
                }

                XElement prebuildsElem = task.Element(XML_PRE_BUILD_COMMANDS);
                if(prebuildsElem != null)
                {
                    PrebuildCommands = new List<CommandRunnerJobInfo>();
                    IEnumerable<XElement> prebuild = prebuildsElem.Elements(XML_COMMAND);
                    foreach (XElement pb in prebuild)
                    {
                        LoadCommand(PrebuildCommands, pb);
                    }
                }

                XElement commandsElem = task.Element(XML_MAIN_BUILD_COMMANDS);
                if (commandsElem != null)
                {
                    Commands = new List<CommandRunnerJobInfo>();
                    IEnumerable<XElement> commands = commandsElem.Elements(XML_COMMAND);
                    foreach (XElement c in commands)
                    {
                        LoadCommand(Commands, c);
                    }
                }

                XElement postbuildsElem = task.Element(XML_POST_BUILD_COMMANDS);
                if (postbuildsElem != null)
                {
                    PostBuildCommands = new List<CommandRunnerJobInfo>();
                    IEnumerable<XElement> postbuilds = postbuildsElem.Elements(XML_COMMAND);
                    foreach (XElement pb in postbuilds)
                    {
                        LoadCommand(PostBuildCommands, pb);
                    }
                }

                XElement publishingElem = task.Element(XML_PUBLISHING);
                if (publishingElem != null)
                {
                    XElement publishDirElem = publishingElem.Element(XML_PUBLISH_DIR);
                    if (publishDirElem != null)
                    {
                        PublishDir = this.Options.Branch.Environment.Subst(publishDirElem.Value);
                    }

                    XElement targetDirElem = publishingElem.Element(XML_TARGET_DIR);
                    if (targetDirElem != null)
                    {
                        TargetDir = this.Options.Branch.Environment.Subst(targetDirElem.Value);
                    }

                    XElement targetRegexElem = publishingElem.Element(XML_TARGET_REGEX);
                    if (targetRegexElem != null)
                    {
                        TargetRegex = targetRegexElem.Value;
                    }
                }

                XElement monitoringPathsElem = task.Element(XML_MONITORING_PATHS);
                if (monitoringPathsElem != null)
                {
                    MonitorPaths = new List<string>();
                    IEnumerable<XElement> monitorPaths = monitoringPathsElem.Elements(XML_MONITORING_PATH);
                    foreach (XElement pb in monitorPaths)
                    {
                        // TODO: Make this better so we can handle attributes that don't exists
                        MonitorPaths.Add(Environment.ExpandEnvironmentVariables(this.Options.Branch.Environment.Subst(pb.Value).Replace("/", "\\")));
                    }
                }

                XElement notificationsElem = task.Element(XML_EMAIL_RECIPIENTS);
                if (notificationsElem != null)
                {
                    Notifications = new List<IEmailRecipient>();

                    foreach (XElement er in notificationsElem.Elements(XML_EMAIL_RECIPIENT))
                    {

                        String rules = String.Empty;
                        XElement rulesElem = er.Element(XML_RULES);
                        ICollection<String> rulesList = new List<String>();

                        if (rulesElem != null)
                        {
                            foreach(XElement rule in rulesElem.Elements(XML_RULE))
                            {
                                if (!String.IsNullOrEmpty(rule.Value))
                                {
                                    rulesList.Add(rule.Value);
                                }
                            }

                            rules = String.Join(",", rulesList);
                        }


                        Notifications.Add(new NotificationRecipient(er.Element(XML_NAME).Value, 
                                                                    er.Element(XML_EMAIL).Value,
                                                                    NotificationRecipient.NotifyStringToRule(rules)));
                    }
                }
            }
        }
}
