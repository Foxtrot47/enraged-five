﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;
using RSG.Base.Configuration;
using RSG.Base.ConfigParser;
using RSG.Base.Extensions;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Common.JobManagement;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Services;
using P4API;

namespace RSG.Pipeline.Automation.ServerHost.Tasks
{
    internal class RageBuilderTestTask :
        TimedTaskBase
    {
        #region Constants
        private static readonly String NAME = "Rage Builder Test Task";
        private static readonly String DESC = "Rage builder test task.";
        private static readonly String LOG_CTX = "Rage Builder Test Task";

        private static readonly String PARAM_TIMED_TASK_FILE = "Timed Task File";
        private static readonly String PARAM_BUILD_EXECUTABLE_PATH = "Executable Path";
        private static readonly String PARAM_EMAIL_NOTIFICATIONS_FILE = "Notifications File";
        private static readonly String PARAM_HOURS_JOB = "Hours to Keep Completed Jobs";
        private static readonly String PARAM_UPDATE_TOOLS = "Update Tools Script"; 

        /// <summary>
        /// Parameter for the file listing job management data.
        /// </summary>
        private static readonly string PARAM_DISPATCH_SCHEME_FILE = "Dispatch Scheme File";

        /// <summary>
        /// 
        /// </summary>
        public DispatchScheme DispatchScheme { get; set; }
        #endregion

        #region Member Data
        /// <summary>
        /// Task parameters.
        /// </summary>
        private IDictionary<String, Object> Parameters;
        private List<IJob> RageBuilderTestJobs;

        private List<IJob> QueuedJobs;
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public RageBuilderTestTask(IEnumerable<String> locations, CommandOptions options)
            : base(NAME, DESC, Common.CapabilityType.RagebuilderTest, options)
        {
            this.QueuedJobs = new List<IJob>();

            String filename = GetSettingsFilename();

            IDictionary<String, Object> parameters = new Dictionary<String, Object>();
            RSG.Base.Xml.Parameters.Load(filename, ref parameters);
            this.Parameters = parameters;
            this.DispatchScheme = new DispatchScheme(this.Config, options, this.Log, this.GetJobManagementFile());
        }
        #endregion

        #region Controller Methods

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String GetTimedTasksFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_TIMED_TASK_FILE]));
        }

        /// <summary>
        /// This is the function that is called once an appropriate timer has hit
        /// </summary>
        public override void ExecuteTimedTask(TimedTaskEvent timedTaskEvent)
        {
            Log.MessageCtx(LOG_CTX, "Executing Timed Task for {0}", this.Name);
            LoadJobData(timedTaskEvent.TimedTrigger);
            
            lock (this.DispatchScheme)
            {
                bool syncDispatch = this.Parameters.ContainsKey(PARAM_SYNC_DISPATCH) && (bool)this.Parameters[PARAM_SYNC_DISPATCH];
                this.DispatchScheme.LoadDispatchData(this.GetJobManagementFile(), this.Options, syncDispatch);
            }
        }

        /// <summary>
        /// Return status of task.
        /// </summary>
        /// <returns></returns>
        public override TaskStatus Status()
        {
            return (new TaskStatus(this));
        }


        /// <summary>
        /// Loads all data for which jobs to run.
        /// </summary>
        /// <returns></returns>
        private void LoadJobData(TimedTrigger TTrigger)
        {
            this.RageBuilderTestJobs = new List<IJob>();
            if (this.Parameters.ContainsKey(PARAM_BUILD_EXECUTABLE_PATH) == true)
            {
                string exePath = this.Config.Environment.Subst((String)this.Parameters[PARAM_BUILD_EXECUTABLE_PATH]);
                using (P4 p4 = this.Config.Project.SCMConnect())
                {
                    P4RecordSet result = p4.Run("changes", "-m 1", Options.Project.Config.ToolsBin + "\\ragebuilder_x64_0378.exe");
                    String updateToolsScript = null;
                    uint latestCL = uint.Parse(result.Records.First().Fields["change"]);
                    if (this.Parameters.ContainsKey(PARAM_UPDATE_TOOLS) == true)
                    {
                        updateToolsScript = this.Config.Environment.Subst((String)this.Parameters[PARAM_UPDATE_TOOLS]);
                    }

                    if (!String.IsNullOrWhiteSpace(updateToolsScript) && System.IO.File.Exists(updateToolsScript))
                    {
                        //Telling the Server/clients to restart and update their /tools/...
                        //StartUp .bat scripts will be in charge of queue'ing the new job at latest (shouldn't be fighting CL #'s at midnight PST)
                        Log.DebugCtx(LOG_CTX, String.Format("Found an update script. Launching: {0}", updateToolsScript));
                        System.Diagnostics.Process restartProc = new System.Diagnostics.Process();
                        restartProc.StartInfo.FileName = updateToolsScript;
                        restartProc.StartInfo.UseShellExecute = true;
                        restartProc.Start();
                    }
                    else
                    {
                        //Update job with the latest ragebuilder number
                        RageBuilderTestJob testJob = new RageBuilderTestJob(exePath, latestCL);
                        testJob.Trigger = new TimedTrigger();
                        RageBuilderTestJobs.Add(testJob);
                        this.EnqueueJob(testJob);
                    }
                }
            }
            else
            {
                Log.ErrorCtx(LOG_CTX, "No executable path was defined in RageBuilderTestTask.xml.");
            }
        }

        /// <summary>
        /// Task notification of job being completed.
        /// </summary>
        /// <param name="job"></param>
        public override void JobCompleted(IJob job, IEnumerable<IJobResult> jobResults)
        {
            base.JobCompleted(job, jobResults);
            try
            {
                    IJob finishedJob = this.Jobs.Single(j => j.ID == job.ID);
                    finishedJob.State = job.State;
            }
            catch (InvalidOperationException)
            {
                Log.ErrorCtx(LOG_CTX, "Unable to find job that got results for IN TIMEDTASK.");
            }
            finally
            {
                Log.DebugCtx(LOG_CTX, "Job list null, but got job results.");
            }
            
        }

        /// <summary>
        /// Cleans all jobs.
        /// </summary>
        public void CleanJobs()
        {
            int hours = (int)this.Parameters[PARAM_HOURS_JOB];
            DateTime modifiedTime = DateTime.UtcNow - TimeSpan.FromHours((double)hours);

            IEnumerable<IJob> completedJobs = this.Jobs.Where(j =>
                JobState.Completed == j.State ||
                JobState.Errors == j.State ||
                JobState.Aborted == j.State);
            foreach (IJob job in completedJobs)
            {
                DateTime completionTime = (job.ProcessedAt + job.ProcessingTime);
                if (completionTime.IsEarlierThan(modifiedTime))
                {
                    CleanJob(job);
                }
            }
        }

        /// <summary>
        /// Finds jobs matching dispatch groups monitoring path matches for rolling builds
        /// or Build and Platform configuration for builds generated via jobdef
        /// </summary>
        /// <param name="machinename"></param>
        /// <returns></returns>
        public override IJob GetSuitableJob(string machinename)
        {
            Log.MessageCtx(LOG_CTX, "Client '{0}' is requesting a suitable job. TIMED TASK", machinename.ToLower());
            lock (this.DispatchScheme)
            {
                // Here we have a 'fallback' client, which processes anything left over.
                // So, we find the next job that has no dispatch group interested in it
                lock (this.m_Jobs)
                {
                    foreach (RageBuilderTestJob job in this.PendingJobs.Where(j => j.Role == CapabilityType.RagebuilderTest).ToList())
                    {
                        return job;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Remove job from our queue and clean necessary files.
        /// </summary>
        /// <param name="job"></param>
        private void CleanJob(IJob job)
        {
            lock (this.m_Jobs)
            {
                IEnumerable<KeyValuePair<JobPriority, IJob>> jobEntries =
                    this.m_Jobs.Where(j => j.Value.ID.Equals(job.ID));
                if (jobEntries.Count() > 0)
                    this.m_Jobs.Remove(jobEntries.First());
            }
            Services.FileTransferService.CleanJobFiles(this.Config, job, Log);
        }

        /// <summary>
        /// Acquire the local settings file.
        /// </summary>
        private String GetSettingsFilename()
        {
            return (Path.Combine(this.Config.ToolsConfig, "automation",
                "tasks", String.Format("{0}.xml", this.GetType().Name)));
        }


        /// <summary>
        /// File containing information required to send notifications.
        /// </summary>
        /// <returns></returns>
        public override String GetNotificationDataFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_EMAIL_NOTIFICATIONS_FILE]));
        }

        /// <summary>
        /// Returns location of dispatch sceheme data file.
        /// </summary>
        /// <returns></returns>
        public String GetJobManagementFile()
        {
            if (this.Parameters.ContainsKey(PARAM_DISPATCH_SCHEME_FILE) == true)
            {
                return (this.Config.Environment.Subst((String)this.Parameters[PARAM_DISPATCH_SCHEME_FILE]));
            }
            else
                return null;
        }
        #endregion
    }
}
