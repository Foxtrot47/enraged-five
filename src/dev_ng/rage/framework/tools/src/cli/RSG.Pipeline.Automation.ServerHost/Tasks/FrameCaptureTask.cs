﻿using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.JobManagement;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.SourceControl.Perforce;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RSG.Pipeline.Automation.ServerHost.Tasks
{
    public class FrameCaptureTask :
        MonitoringTaskBase,
        IMonitoringTask,
        IJobManagedTask
    {
        #region Constants
        /// <summary>
        /// The task name.
        /// </summary>
        private static readonly String NAME = "Frame Capture";

        /// <summary>
        /// The task description.
        /// </summary>
        private static readonly String DESC = "Frame Capture monitoring task.";

        /// <summary>
        /// The task log context.
        /// </summary>
        private static readonly new String LOG_CTX = "Automation:FrameCaptureTask";

        /// <summary>
        /// Parameter for the file listing all the paths to monitor.
        /// </summary>
        private static readonly string PARAM_MONITOR_PATH_FILE = "Path Monitoring File";

        /// <summary>
        /// Parameter for the file listing email addresses to send notification data to.
        /// </summary>
        private static readonly string PARAM_EMAIL_NOTIFICATIONS_FILE = "Notifications File";

        /// <summary>
        /// Parameter for the file listing job management data.
        /// - the dispatch file also defines the builds to build.
        /// </summary>
        private static readonly string PARAM_DISPATCH_SCHEME_FILE = "Dispatch Scheme File";

        /// <summary>
        /// Parameter for number of hours between job cleans.
        /// </summary>
        private static readonly String PARAM_HOURS_JOB = "Hours to Keep Completed Jobs";

        /// <summary>
        /// Parameter for number of hours between job cleans of pending jobs
        /// - these can hang around when solutions to build are removed.
        /// </summary>
        private static readonly String PARAM_HOURS_TO_KEEP_PENDING_JOBS = "Hours to Keep Pending Jobs";

        #endregion // Constants

        #region Member Data
        /// <summary>
        /// The Dispatch scheme.
        /// </summary>
        public DispatchScheme DispatchScheme { get; set; }

        /// <summary>
        /// Stored to know when this file is modified
        /// </summary>
        private DateTime MonitoringFileLastModified { get; set; }

        #endregion // Constants


        #region Constructors
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="location"></param>
        public FrameCaptureTask(CommandOptions options)
            : this(new String[] { String.Empty }, options)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="locations"></param>
        public FrameCaptureTask(IEnumerable<String> locations, CommandOptions options)
            : base(NAME, DESC, CapabilityType.FrameCapture, locations, options)
        {
            String filename = GetSettingsFilename();

            IDictionary<String, Object> parameters = new Dictionary<String, Object>();
            RSG.Base.Xml.Parameters.Load(filename, ref parameters);
            this.Parameters = parameters;

            this.MonitoringFileLastModified = DateTime.MinValue;
            this.LoadMonitoringLocations();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        public override void Update()
        {
            // Allows us to update monitoring locations without restarting the service.
            this.LoadMonitoringLocations();

            // Get list of changelists from base class; turn these into Jobs.
            IEnumerable<Changelist> changelists = base.GetSubmittedChangelists();
            if (changelists.Count() > 0)
                this.MaximumChangelist = changelists.Select(changelist => changelist.Number).Max();

            int jobsCreated = 0;

            lock (this.m_Jobs) // must lock jobs otherwise one might be consumed before the whole list of jobs is known, when that happens job consumption is not to the head
            {
                foreach (Changelist changelist in changelists)
                {
                    Log.MessageCtx(LOG_CTX, "Considering changelist #{0} submitted by '{1}' at {2}",
                        changelist.Number, changelist.Username, changelist.Time.ToString(), changelist.Files.Count());

                    if (changelist.Files.Count() == 0)
                    {
                        Log.WarningCtx(LOG_CTX, "Empty changelist {0}: {1}.  Ignoring.", changelist.Number, changelist.Description);
                        continue;
                    }

                    // Ensure we only process files within a changelist that should be monitored.
                    List<String> monitoredFiles = new List<String>();
                    foreach (String monitoredPath in this.MonitoredPaths)
                        monitoredFiles.AddRange(changelist.Files.Where(f => f.ToLower().StartsWith(monitoredPath.ToLower())));

                    if (monitoredFiles.Any())
                    {
                        IEnumerable<FileMapping> changeListFiles = FileMapping.Create(this.P4, monitoredFiles.ToArray());

                        // Create jobs for this changelist
                        jobsCreated += CreateJobsForChangelist(changelist, changeListFiles);
                    }
                    
                }

                if (jobsCreated > 0)
                {
                    Log.MessageCtx(LOG_CTX, " ");
                    Log.MessageCtx(LOG_CTX, "**********************");
                    Log.MessageCtx(LOG_CTX, "* {0,5} Jobs Created *", jobsCreated);
                    Log.MessageCtx(LOG_CTX, "**********************");
                    Log.MessageCtx(LOG_CTX, " ");
                    // Save the task when jobs are created - otherwise they could go missing if say the automation service crashed before the job could be executed.
                    Save();
                }
            }
        }

        /// <summary>
        /// Returns location of dispatch scheme data file.
        /// </summary>
        /// <returns></returns>
        public String GetJobManagementFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_DISPATCH_SCHEME_FILE]));
        }

        /// <summary>
        /// Task notification of job being completed.
        /// </summary>
        /// <param name="job"></param>
        public override void JobCompleted(IJob job, IEnumerable<IJobResult> jobResults)
        {
            // Ensure any consumed jobs inherit the details of the completed consuming job.
            foreach (IJob consumedJob in this.Jobs.Where(j => j.ConsumedByJobID == job.ID))
            {
                consumedJob.CompletedAt = job.CompletedAt;
                consumedJob.ProcessedAt = job.ProcessedAt;
                consumedJob.ProcessingTime = job.ProcessingTime;
            }

            base.JobCompleted(job, jobResults);
        }

        /// <summary>
        /// File containing information required to send notifications.
        /// </summary>
        /// <returns></returns>
        public override String GetNotificationDataFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_EMAIL_NOTIFICATIONS_FILE]));
        }

        /// <summary>
        /// Client requests for a suitable job
        /// </summary>
        /// <param name="machinename"></param>
        /// <returns>a codebuilder2job</returns>
        public override IJob GetSuitableJob(string machinename)
        {
            lock (this.Jobs)
            {
                return this.PendingJobs.FirstOrDefault();
            }
        }
        #endregion // COntroller Methods

        /// <summary>
        /// Get the tasks settings filename.
        /// </summary>
        /// <returns></returns>
        private String GetSettingsFilename()
        {
            return (Path.Combine(this.Config.ToolsConfig, "automation",
                "tasks", String.Format("{0}.xml", this.GetType().Name)));
        }

        /// <summary>
        /// Load the monitoring locations settings file.
        /// </summary>
        private void LoadMonitoringLocations()
        {
            (this.MonitoredPaths as List<String>).Clear();
            String monitoringFile = this.Config.Environment.Subst((String)this.Parameters[PARAM_MONITOR_PATH_FILE]);
            if (!File.Exists(monitoringFile) && this.MonitoredPaths.Count() == 0)
            {
                this.Log.ErrorCtx(LOG_CTX, "Monitoring file does not exist ({0}).  Asset builder task will not be monitoring any locations.", monitoringFile);
            }

            // Cache monitoring path last modified time
            DateTime monitoringFileModified = System.IO.File.GetLastWriteTime(monitoringFile);
            bool showMonitoringPaths = this.MonitoringFileLastModified != monitoringFileModified;
            if (showMonitoringPaths && this.MonitoringFileLastModified != DateTime.MinValue)
            {
                this.Log.MessageCtx(LOG_CTX, " ");
                this.Log.MessageCtx(LOG_CTX, "*** Monitoring paths may have been modified : {0} ***", monitoringFile);
                this.Log.MessageCtx(LOG_CTX, " ");
            }

            this.MonitoringFileLastModified = monitoringFileModified;

            XDocument xdoc = XDocument.Load(monitoringFile);

            // Read all locations
            ICollection<String> locationsDeserialised = new List<String>();

            foreach (XElement elem in xdoc.Descendants("location"))
            {
                String path = elem.Attribute("path").Value;
                if (showMonitoringPaths)
                {
                    this.Log.MessageCtx(LOG_CTX, "Monitoring Path: {0}", path);
                }
                locationsDeserialised.Add(path);
            }

            // For each deserialised location translate to appropriate location(s)
            ISet<String> coreLocations = new HashSet<String>();
            ISet<String> dlcLocations = new HashSet<String>();

            foreach (String location in locationsDeserialised)
            {
                String branchName = this.Options.Branch.Name;
                Debug.Assert(this.Options.CoreProject.Branches.ContainsKey(branchName),
                    "Core project does not have branch '{0}' defined.", branchName);
                if (!this.Options.CoreProject.Branches.ContainsKey(branchName))
                    throw (new ArgumentException(String.Format("Core project does not have branch '{0}' defined.", branchName)));

                string rootProjectLocation = this.Options.CoreProject.Branches[branchName].Environment.Subst(location);
                rootProjectLocation = Environment.ExpandEnvironmentVariables(rootProjectLocation);
                if (showMonitoringPaths)
                {
                    this.Log.MessageCtx(LOG_CTX, "Core project evaluated monitoring path: {0}", rootProjectLocation);
                }

                // DLC Projects
                foreach (KeyValuePair<string, IProject> kvp in this.Config.DLCProjects)
                {
                    string dlcProjectName = kvp.Key;
                    IProject dlcProject = kvp.Value;
                    if (dlcProject.Branches.ContainsKey(branchName))
                    {
                        String dlcProjectLocation = dlcProject.Branches[branchName].Environment.Subst(location);
                        if (showMonitoringPaths)
                        {
                            this.Log.MessageCtx(LOG_CTX, " - DLC project evaluated monitoring path: {0}", dlcProjectLocation);
                        }
                        dlcLocations.Add(dlcProjectLocation);
                    }
                    else
                    {
                        this.Log.WarningCtx(LOG_CTX, "DLC project branch does not exist: {0}", branchName);
                    }
                }

                coreLocations.Add(rootProjectLocation);
            }

            // Join all core & dlc locations
            ICollection<String> locations = new List<String>();
            locations.AddRange(coreLocations);
            locations.AddRange(dlcLocations);

            if (locations.Any())
            {
                FileMapping[] fms = FileMapping.Create(this.P4, locations.Distinct().ToArray());
                (this.MonitoredPaths as List<String>).AddRange(fms.Select(fm => fm.DepotFilename));
            }
            else
            {
                this.Log.ErrorCtx(LOG_CTX, "There are no monitoring locations, please examine your monitoring file {0}", monitoringFile);
            }
        }


        /// <summary>
        /// Method to clean out jobs and their files.
        /// </summary>
        private void CleanJobs()
        {
            int hoursKeepCompleted = (int)this.Parameters[PARAM_HOURS_JOB];
            int hoursKeepPending = (int)this.Parameters[PARAM_HOURS_TO_KEEP_PENDING_JOBS];

            DateTime modifiedTimeCompleted = DateTime.UtcNow - TimeSpan.FromHours((double)hoursKeepCompleted);
            DateTime modifiedTimePending = DateTime.UtcNow - TimeSpan.FromHours((double)hoursKeepPending);

            // We now just delete any job that has existed for the specified time rather than
            // specifically just completed jobs.
            lock (this.m_Jobs)
            {
                foreach (IJob job in this.Jobs)
                {
                    DateTime completionTime = (job.ProcessedAt + job.ProcessingTime);
                    if (completionTime.IsEarlierThan(modifiedTimeCompleted))
                    {
                        CleanJob(job);
                    }

                    // Clean job if found pending for too long.
                    if (job.State == JobState.Pending && job.CreatedAt.IsEarlierThan(modifiedTimePending))
                    {
                        Log.WarningCtx(LOG_CTX, "A Job {0} was found to be in state pending for more than {1} hours. It has been cleaned.", job.ID, hoursKeepPending);
                        CleanJob(job);
                    }
                }

                // B*1377109 - Remove any job results for jobs that no longer exist
                SortedSet<Guid> jobIDs = new SortedSet<Guid>(this.Jobs.Select(job => job.ID));
                lock (this.m_JobResults)
                {
                    IJobResult[] resultsToRemove = m_JobResults.Where(jobResult => !jobIDs.Contains(jobResult.JobId)).ToArray();
                    foreach (IJobResult resultToRemove in resultsToRemove)
                    {
                        this.m_JobResults.Remove(resultToRemove);
                    }
                }
            }
        }

        /// <summary>
        /// Remove job from our queue and clean necessary files.
        /// </summary>
        /// <param name="job"></param>
        private void CleanJob(IJob job)
        {
            lock (this.m_Jobs)
            {
                IEnumerable<KeyValuePair<JobPriority, IJob>> jobEntries =
                    this.m_Jobs.Where(j => j.Value.ID.Equals(job.ID));
                if (jobEntries.Count() > 0)
                    this.m_Jobs.Remove(jobEntries.First());
            }
            Services.FileTransferService.CleanJobFiles(this.Config, job, this.Log);
        }
                /// <summary>
        /// Process the changelist creating jobs according the internal dispatch settings.
        /// </summary>
        /// <param name="changelist"></param>
        /// <param name="changeListFileMappings"></param>
        /// <returns></returns>
        private int CreateJobsForChangelist(Changelist changelist, IEnumerable<FileMapping> changeListFileMappings)
        {
            int numJobs = 0;
            if (!changeListFileMappings.Any())
            {
                Log.ErrorCtx(LOG_CTX, "No files in changelist : this is unexpected.");
                return 0;
            }

            ICollection<String> changeListFilenames = new List<String>();
            changeListFileMappings.ForEach(fm => changeListFilenames.Add(fm.DepotFilename));

            ICollection<string> animationTokens = BuildTokenHelper.GetBuildTokens(Log, changelist.Description, BuildToken.ANIMATION);

            String animationName = String.Empty;

            if (animationTokens != null && animationTokens.Any())
            {
                animationName = animationTokens.First();
            }


            ICollection<string> positionTokens = BuildTokenHelper.GetBuildTokens(Log, changelist.Description, BuildToken.POSITION);

            String position = String.Empty;

            if (positionTokens != null && positionTokens.Any())
            {
                position = positionTokens.First();
            }


            if (!String.IsNullOrEmpty(animationName))
            {
                this.EnqueueJob(new FrameCaptureJob(this.Options.Branch, changelist, animationName, changeListFilenames, position, this.MonitoredPaths));
                numJobs++;
            }

            return numJobs;
        }
    }
}
