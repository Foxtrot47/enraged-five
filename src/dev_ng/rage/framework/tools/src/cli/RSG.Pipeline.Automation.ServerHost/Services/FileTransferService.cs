﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Services;
using RSG.Pipeline.Automation.Common.Services.Messages;

namespace RSG.Pipeline.Automation.ServerHost.Services
{
    
    /// <summary>
    /// Upload Service Implementation.
    /// </summary>
    /// Events are used to signal to the Automation Service to act on the files
    /// uploaded (e.g. Universal Log... move to Job ID folder for later retrieval
    /// and parsing).
    /// 
    public class FileTransferService : 
        RSG.Pipeline.Automation.Common.Services.IFileTransferService
    {
        #region Constants
        private static readonly String LOG_CTX = "Upload Service";

        /// <summary>
        /// Parameter Key: automation server file upload directory.
        /// </summary>
        private static readonly String PARAM_AUTOMATION_FILE_DIRECTORY = "Automation File Directory";

        /// <summary>
        /// Parameter Key: client file directory.
        /// </summary>
        private static readonly String PARAM_CLIENT_FILE_DIRECTORY = "Client File Directory";

        /// <summary>
        /// Parameter Key: Buffer size for file uploads in bytes.
        /// </summary>
        private static readonly String PARAM_BUFFER_SIZE = "Buffer Size";
        #endregion // Constants

        #region Properties
        /// <summary>
        /// Automation Service Parameters.
        /// </summary>
        private IDictionary<String, Object> Parameters;

        /// <summary>
        /// 
        /// </summary>
        private IConfig Config { get; set; }

        /// <summary>
        /// Log object.
        /// </summary>
        private IUniversalLog Log { get; set; }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public FileTransferService()
        {
            this.Config = ConfigFactory.CreateConfig();
            this.Log = LogFactory.CreateUniversalLog("Upload Service");
            this.Log.Message("Upload Service Startup");
            this.LoadFileTransferParameters();
        }
        #endregion // Constructor(s)

        #region IFileTransferService Interface Methods
        /// <summary>
        /// Upload a file to the Server Host.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public TransferResponse UploadFile(RemoteFile request)
        {
            this.Log.DebugCtx(LOG_CTX, "FileTransferService::Upload()");
            try
            {
                String jobUploadDirectory = GetServerFileDirectory(request.Job);
                if (!Directory.Exists(jobUploadDirectory))
                    Directory.CreateDirectory(jobUploadDirectory);

                String filename = request.Filename;
                String uploadFilename = Path.Combine(jobUploadDirectory, filename);
                if (File.Exists(uploadFilename))
                    File.Delete(uploadFilename);

                System.Diagnostics.Stopwatch stopWatch = new System.Diagnostics.Stopwatch();
                stopWatch.Start();

                this.Log.MessageCtx(LOG_CTX, "Uploading: {0}.",
                    request.Filename);
                int bufferSize = (int)this.Parameters[PARAM_BUFFER_SIZE];
                Byte[] buffer = new Byte[bufferSize];
                Log.MessageCtx(LOG_CTX, "Buffer Size: {0}", bufferSize);
                using (FileStream outputStream = new FileStream(uploadFilename,
                    FileMode.Create, FileAccess.Write))
                {
                    int bytesRead = request.Stream.Read(buffer, 0, bufferSize);
                    while (bytesRead > 0)
                    {
                        outputStream.Write(buffer, 0, bytesRead);
                        bytesRead = request.Stream.Read(buffer, 0, bufferSize);
                    }
                    outputStream.Close();
                }

                stopWatch.Stop();
                TimeSpan ts = stopWatch.Elapsed;
                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}",
                                            ts.Hours, ts.Minutes, ts.Seconds,
                                            ts.Milliseconds / 10);
                Log.MessageCtx(LOG_CTX, "Upload file took: {0}", elapsedTime);

                // Verify MD5 hash; by recalculating it and comparing it to the
                // RemoteFile request one.
                bool hashOk = true;
                Byte[] hash = RSG.Base.IO.FileMD5.ComputeMD5(uploadFilename);
                if (!RSG.Base.IO.FileMD5.Equal(request.Hash, hash))
                {
                    Log.ErrorCtx(LOG_CTX, "MD5 hash comparison failure: request: {0}, local: {1}.  File will be deleted.",
                        RSG.Base.IO.FileMD5.Format(request.Hash), RSG.Base.IO.FileMD5.Format(hash));
                    FileInfo fi = new FileInfo(uploadFilename);
                    fi.Delete();
                    hashOk = false;
                }


                return (new TransferResponse { Successful = hashOk });
            }
            catch (Exception ex)
            {
                Log.ExceptionCtx(LOG_CTX, ex, "Error upload data: {0}.", 
                    request.Filename);
                if (File.Exists(request.Filename))
                    File.Delete(request.Filename);
                return (new TransferResponse { Successful = false });
            }
        }

        /// <summary>
        /// Download a file from the Server Host.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoteFile DownloadFile(RemoteFileRequest request)
        {
            try
            {
                String jobUploadDirectory = GetServerFileDirectory(request.Job);
                String filename = Path.Combine(jobUploadDirectory, request.Filename);

                if (!File.Exists(filename))
                    throw new FileNotFoundException("File not found.", request.Filename);

                RemoteFile rf = new RemoteFile(request.Job, filename);
                return (rf);
            }
            catch (Exception ex)
            {
                this.Log.ToolExceptionCtx(LOG_CTX, ex, "Error downloading data: {0} for Job {1}.", 
                    request.Filename, request.Job);
                return (new RemoteFile(request.Job, request.Filename, true));
            }
        }

        /// <summary>
        /// Read information about the available files.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Obsolete("Use FileAvailability2 - more file information is provided in an extensible way.")]
        public RemoteFileAvailability FileAvailability(RemoteFileAvailabilityRequest request)
        {
            try
            {
                this.Log.MessageCtx(LOG_CTX, "Attempting to get file availability {0} ", request.Job);
                String uploadDirectory = (String)this.Parameters[PARAM_AUTOMATION_FILE_DIRECTORY];
                String jobUploadDirectory = Path.Combine(uploadDirectory, request.Job.ToString());
                jobUploadDirectory = this.Config.Project.Environment.Subst(jobUploadDirectory);

                if (Directory.Exists(jobUploadDirectory))
                {
                    String[] files = Directory.GetFiles(jobUploadDirectory);
                    IEnumerable<String> filenames = files.Select(filename =>
                        Path.GetFileName(filename));
                    int fileCount = filenames.Count();

                    return (new RemoteFileAvailability { FileCount = fileCount, Filenames = filenames, Job = request.Job });
                }
                else
                {
                    return (new RemoteFileAvailability { FileCount = 0, Filenames = new List<String>(), Job = request.Job });
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolExceptionCtx(LOG_CTX, ex, "Error determining file availability for Job {0}.",
                    request.Job);
                return (new RemoteFileAvailability { FileCount = 0, Filenames = new List<String>(), Job = request.Job });
            }
        }

        /// <summary>
        /// Read information about the available files.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public RemoteFileAvailability2 FileAvailability2(RemoteFileAvailabilityRequest request)
        {
            try
            {
                String uploadDirectory = (String)this.Parameters[PARAM_AUTOMATION_FILE_DIRECTORY];
                String jobUploadDirectory = Path.Combine(uploadDirectory, request.Job.ToString());
                jobUploadDirectory = this.Config.Project.Environment.Subst(jobUploadDirectory);

                if (Directory.Exists(jobUploadDirectory))
                {
                    List<RemoteFileInfo> rfis = new List<RemoteFileInfo>();
                    String[] filenames = Directory.GetFiles(jobUploadDirectory, "*.*", SearchOption.AllDirectories);
                    foreach (String filename in filenames)
                    {
                        String fullFileName = Path.GetFullPath(filename);
                        String fullUploadDir = Path.GetFullPath(jobUploadDirectory) + "\\";
                        FileInfo fi = new FileInfo(filename);
                        RemoteFileInfo rfi = new RemoteFileInfo()
                        {
                            FullPath = filename,
                            RelativePath = fullFileName.Remove(0, fullUploadDir.Length),
                            Filename = Path.GetFileName(filename),
                            FileSize = fi.Length
                        };
                        rfis.Add(rfi);
                    }
                    int fileCount = filenames.Count();

                    return (new RemoteFileAvailability2 { FileCount = fileCount, Files = rfis, Job = request.Job });
                }
                else
                {
                    return (new RemoteFileAvailability2 { FileCount = 0, Files = new RemoteFileInfo[0], Job = request.Job });
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolExceptionCtx(LOG_CTX, ex, "Error determining file availability for Job {0}.",
                    request.Job);
                return (new RemoteFileAvailability2 { FileCount = 0, Files = new RemoteFileInfo[0], Job = request.Job });
            }
        }

        /// <summary>
        /// Return directory used for files transferred to client.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public String GetClientFileDirectory(IJob job)
        {
            String directory = (String)this.Parameters[PARAM_CLIENT_FILE_DIRECTORY];
            directory = this.Config.Project.Environment.Subst(directory);
            directory = Path.GetFullPath(Path.Combine(directory, job.ID.ToString()));

            return (directory);
        }
        #endregion // IFileTransferService Interface Methods

        #region Static Controller Methods
        /// <summary>
        /// Return merged Universal Log filename for a particular job.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public static String GetJobLogFilename(IConfig config, IJob job)
        {
            IDictionary<String, Object> parameters = new Dictionary<String, Object>();
            RSG.Base.Xml.Parameters.Load(FileTransferService.GetSettingFilenameStatic(config),
                ref parameters);

            String uploadDirectory = (String)parameters[PARAM_AUTOMATION_FILE_DIRECTORY];
            String jobUploadDirectory = Path.Combine(uploadDirectory, job.ID.ToString());
            jobUploadDirectory = Path.GetFullPath(config.Project.Environment.Subst(jobUploadDirectory));

            return (Path.Combine(jobUploadDirectory, "job.ulog"));
        }

        /// <summary>
        /// Clean all files associated with a job on the automation server.
        /// </summary>
        /// <param name="job"></param>
        public static void CleanJobFiles(IConfig config, IJob job, ILog log)
        {
            IDictionary<String, Object> parameters = new Dictionary<String, Object>();
            RSG.Base.Xml.Parameters.Load(FileTransferService.GetSettingFilenameStatic(config), 
                ref parameters);
            
            String uploadDirectory = (String)parameters[PARAM_AUTOMATION_FILE_DIRECTORY];
            String jobUploadDirectory = Path.Combine(uploadDirectory, job.ID.ToString());
            jobUploadDirectory = Path.GetFullPath(config.Project.Environment.Subst(jobUploadDirectory));

            try
            {
                if (!Directory.Exists(jobUploadDirectory))
                    return;

                // Some max/maxc files were readonly in the job folder.
                String[] filesInJobFolder = Directory.GetFiles(jobUploadDirectory);
                foreach (String fi in filesInJobFolder)
                {
                    FileAttributes attributes = File.GetAttributes(fi);
                    File.SetAttributes(fi, attributes & ~FileAttributes.ReadOnly);
                }

                log.MessageCtx(LOG_CTX, "Deleting job data: {0}.", jobUploadDirectory);
                Directory.Delete(jobUploadDirectory, true);
            }
            catch (DirectoryNotFoundException ex)
            {
                if (Directory.Exists(jobUploadDirectory))
                {
                    log.ToolException(ex, "Error deleting job data, there is a problem accessing this directory, not a serious issue but it requires attention : {0}.", jobUploadDirectory);                    
                }
                else
                {
                    log.Warning("When deleting job data the directory was found no longer to exist ( don't worry about this, but it is strange ): {0}.", jobUploadDirectory);
                }
            }
            catch (Exception ex)
            {
                log.ToolException(ex, "Error deleting job data: {0}.", jobUploadDirectory);
            }
        }
        #endregion // Static Controller Methods

        #region Private Methods
        /// <summary>
        /// Return directory used for file uploads for a job.
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        private String GetServerFileDirectory(Guid job)
        {
            String uploadDirectory = (String)this.Parameters[PARAM_AUTOMATION_FILE_DIRECTORY];
            String jobUploadDirectory = Path.Combine(uploadDirectory, job.ToString());
            jobUploadDirectory = Path.GetFullPath(this.Config.Project.Environment.Subst(jobUploadDirectory));

            return (jobUploadDirectory);
        }

        /// <summary>
        /// Return settings XML filename.
        /// </summary>
        /// <returns></returns>
        /// This is virtual allowing you to override it if required; although
        /// it's not recommended.
        /// 
        private String GetSettingsFilename()
        {
            return (GetSettingFilenameStatic(this.Config));
        }

        /// <summary>
        /// Return settings XML filename.
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        private static String GetSettingFilenameStatic(IConfig config)
        {
            return (Path.Combine(config.ToolsConfig, "automation",
                "services", String.Format("{0}.xml", typeof(FileTransferService).Name)));
        }

        private void LoadFileTransferParameters()
        {
            this.Parameters = new Dictionary<String, Object>();
            String filename = GetSettingsFilename();
            RSG.Base.Xml.Parameters.Load(filename, ref this.Parameters);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.ServerHost.Services namespace
