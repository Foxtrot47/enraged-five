﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Common.JobManagement;
using RSG.SourceControl.Perforce;
using P4API;
using RSG.Pipeline.Services;
using System.Diagnostics;
using System.Collections;
using System.Text.RegularExpressions;
using RSG.Pipeline.Automation.Common.Notifications;
using RSG.SourceControl.Perforce.Extensions;

namespace RSG.Pipeline.Automation.ServerHost.Tasks
{
    /// <summary>
    /// Integrator task.
    /// </summary>
    internal class IntegratorTask :
        TaskBase,
        IIntegratorTask,
        IDisposable
    {
        #region Constants

        /// <summary>
        /// Procedure for fixing integrations
        /// </summary>
        private static readonly String PROCEDURE_WIKI = "https://devstar.rockstargames.com/wiki/index.php/Automated_Integrator_Resolve_Procedure#Procedure_for_unblocking_integrator";

        /// <summary>
        /// The task name.
        /// </summary>
        private static readonly String NAME = "Integrator";

        /// <summary>
        /// The task description.
        /// </summary>
        private static readonly String DESC = "Integrator monitoring and integration task.";

        /// <summary>
        /// The task log context.
        /// </summary>
        private static readonly String LOG_CTX = "Integrator:IntegratorTask";

        /// <summary>
        /// Parameter for the file listing all the integration paths to monitor.
        /// </summary>
        private static readonly string PARAM_MONITOR_PATH_FILE = "Integration Monitoring File";

        /// <summary>
        /// Parameter for the file listing email addresses to send notification data to.
        /// </summary>
        private static readonly string PARAM_EMAIL_NOTIFICATIONS_FILE = "Notifications File";

        /// <summary>
        /// Parameter for number of hours between job cleans.
        /// </summary>
        private static readonly String PARAM_HOURS_JOB = "Hours to Keep Completed Jobs";

        /// <summary>
        /// Parameter for number of hours between blocked integration alerts.
        /// </summary>
        private static readonly String PARAM_BLOCKED_ALERT_HOURS = "Hours to alert on blocked integrations";

        /// <summary>
        /// Parameter for alert reminder when job completes.
        /// </summary>
        private static readonly String PARAM_BLOCKED_ALERT_IMMEDIATE = "Alert when job completes";          

        /// <summary>
        /// Parameter for turning off blocking ( use judiciously )
        /// </summary>
        private static readonly String PARAM_BLOCKING_ENABLED = "Blocking enabled";       
   
        /// <summary>
        /// Parameter for alerting users of the blocking changelist, over and above the normal notification system.
        /// </summary>
        private static readonly String PARAM_ALERT_USERS = "Alert users of blocking changelist";

        /// <summary>
        /// Parameter for alerting users of the blocking changelist, over and above the normal notification system, as derived from branchspec description.
        /// </summary>
        private static readonly String PARAM_ALERT_BRANCHSPEC_USERS = "Alert branchspec users of blocking changelist";

        /// <summary>
        /// By setting this you do not integrate CLs wht have purged files.
        /// </summary>
        private static readonly String PARAM_IGNORE_PURGED_JOBS = "Ignore purged jobs";

        /// <summary>
        /// By setting this you do not integrate CLs wht have purged files.
        /// </summary>
        private static readonly String PARAM_CACHED_INTERCHANGES = "Cached Interchanges";

        /// <summary>
        /// Regex that is used in the branchspec description to user disable branchspecs
        /// </summary>
        private static readonly String REGEX_DISABLE_BRANCHSPEC = @"disabled\s*=\s*(.+)";

        /// <summary>
        /// Regex that is used in the branchspec description to get user emails
        /// </summary>
        private static readonly String REGEX_EMAIL_BRANCHSPEC = @"([A-Z0-9._%+-]+@rockstar[A-Z0-9.-]*\.com)";        

        #endregion // Constants

        #region Properties
        /// <summary>
        /// Configuration data.
        /// </summary>
        private IConfig Config { get; set; }

        /// <summary>
        /// Date time of last blocked integration alert
        /// </summary>
        private DateTime LastBlockedIntegrationAlert { get; set; }
        
        /// <summary>
        /// Slows client update rate to task update rate
        /// </summary>
        private bool TaskUpdated { get; set; }

        /// <summary>
        /// Integrations monitored and integrated to.
        /// </summary>
        public IEnumerable<Integration> Integrations { get; private set; }

        /// <summary>
        /// The number of integrations jobs handed out to clients.
        /// - this is used for round robin handling of each branch spec to ensure branchspecs 
        /// don't get left behind when a branchspec has stalled for a long time.
        /// </summary>
        private int IntegrationRoundRobinIdx { get; set; }

        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="locations"></param>
        public IntegratorTask(CommandOptions options)
            : base(NAME, DESC, CapabilityType.Integrator, options)
        {
            this.IntegrationRoundRobinIdx = 0;
            this.Integrations = new List<Integration>();

            this.Config = options.Config;     
            this.TaskUpdated = false;
            this.LastBlockedIntegrationAlert = DateTime.MinValue;

            String filename = GetSettingsFilename();

            IDictionary<String, Object> parameters = new Dictionary<String, Object>();
            RSG.Base.Xml.Parameters.Load(filename, ref parameters);
            this.Parameters = parameters;

            this.LoadIntegrations();

            using (P4 p4 = Config.Project.SCMConnect())
            {
                Log.FatMessageCtx(LOG_CTX, "p4 SCM CONNECT : constructor");
                foreach (Integration integration in Integrations)
                {
                    // this updates the cache
                    DateTime lastModified = p4.GetBranchSpecModifiedTime(integration.BranchSpec, updateCacheModifiedTime: true);
                }
            }
        }

        ~IntegratorTask()
        {
            this.Dispose(false);
        }
        #endregion // Constructor(s)

        #region IDisposable
        public void Dispose()
        {
            this.Dispose(true);
        }

        protected virtual void Dispose(bool disposeManagedResources)
        {
            if (disposeManagedResources)
            {
            }
        }
        #endregion  // IDisposable

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        public override void Update()
        {
            using (P4 p4 = Config.Project.SCMConnect())
            {
                Log.FatMessageCtx(LOG_CTX, "p4 SCM CONNECT : TaskUpdate");

                // Only do job maintainance; cleaning the queue and associated files.
                this.CleanJobs();

                // Allows us to update without restarting the service.
                this.LoadIntegrations();

                IDictionary<String, DateTime> branchSpecModifiedDict = new Dictionary<String, DateTime>();

                // Cache the last modified times of the integration branchspecs
                // we can use this later to determine if they were modified - if so we can create jobs from the dawn of time
                // for those integrations - this is required because some jobs might be only part integrated
                // due to a branch spec update.
                foreach (Integration integration in Integrations)
                {
                    DateTime lastModified = p4.GetBranchSpecModifiedTimeFromCache(integration.BranchSpec);
                    branchSpecModifiedDict[integration.BranchSpec] = lastModified;
                }

                // Monitor SCM and create integration jobs
                this.CreateIntegrationJobs(p4, branchSpecModifiedDict);

                this.LogBlockageState();

                this.AlertOnAllBlockages(p4);

                this.TaskUpdated = true;
            }
        }

        /// <summary>
        /// File containing information required to send notifications.
        /// </summary>
        /// <returns></returns>
        public override String GetNotificationDataFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_EMAIL_NOTIFICATIONS_FILE]));
        }

        /// <summary>
        /// Task notification of job being completed.
        /// </summary>
        /// <param name="job"></param>
        /// <param name="jobResults"></param>
        public override void JobCompleted(IJob job, IEnumerable<IJobResult> jobResults)
        {
            base.JobCompleted(job, jobResults);

            if ((bool)this.Parameters[PARAM_BLOCKED_ALERT_IMMEDIATE])
            {
                // Send an alert now
                this.AlertOnAllBlockages(null);
            }
        }

        /// <summary>
        /// Client is requesting a job
        /// </summary>
        /// <param name="machinename"></param>
        /// <returns></returns>
        public override IJob GetSuitableJob(string machinename)
        {
            if (!base.CanGetSuitableJob(machinename))
                return null;  

            // DW: HACK TODO: Client update is baked ( should be configurable ) - here we slow client update task to task update tick because p4 interchanges might be slow.
            // this would only be a problem when there are jobs pending by blocked integrations.
            if (!this.TaskUpdated)
            {
                Log.MessageCtx(LOG_CTX, "Client '{0}' is requesting a suitable job. (Task doesn't want to give jobs at this time)", machinename.ToLower()); 
                return null;
            }
            else
            {
                Log.MessageCtx(LOG_CTX, "Client '{0}' is requesting a suitable job.", machinename.ToLower());
                this.TaskUpdated = false;
            }

            lock (this.m_Jobs)
            lock (this.m_JobResults)
            {
                if (!this.PendingJobs.Any())
                    return null;

                using (P4 p4 = Config.Project.SCMConnect())
                {
                    Log.FatMessageCtx(LOG_CTX, "p4 SCM CONNECT : Get suitable Job");

                    IEnumerable<Integration> integrationsPrioritised = GetPrioritisedIntegrationsToProcess();

                    // Now check each integration until we get a job we can process
                    foreach (Integration integration in integrationsPrioritised)
                    {
                        // First make sure we know where we are in terms of integration - because integrations can be done by users at any time whilst we process.
                        P4API.P4RecordSet records = ((bool)this.Parameters[PARAM_CACHED_INTERCHANGES]) ? p4.InterchangesCached(Log, integration.BranchSpec, updateModifiedTime: false) : p4.Run("interchanges", "-b", integration.BranchSpec);
                        if (records == null || !records.Records.Any())
                        {
                            Log.MessageCtx(LOG_CTX, "No interchanges results : {0}", integration.BranchSpec);
                            this.IntegrationRoundRobinIdx++;
                            continue;
                        }

                        // Sort all changelists so we process them in correct order ( ascending )
                        IEnumerable<int> changelists = records.Records.Select(r => int.Parse(r.Fields["change"])).OrderBy(c => c).ToList();
                        if (!changelists.Any())
                        {
                            Log.MessageCtx(LOG_CTX, "No changes in interchanges : {0}", integration.BranchSpec);
                            this.IntegrationRoundRobinIdx++;
                            continue;
                        }

                        int firstChangelist = changelists.First();

                        if ((bool)this.Parameters[PARAM_IGNORE_PURGED_JOBS])
                        {
                            // ensure we do not hand out 'purgey' jobs.
                            foreach (int cl in changelists)
                            {
                                // The first changelist is the one that needs to be integrated next.
                                firstChangelist = cl;

                                Changelist changelist = new Changelist(p4, cl);
                                String filesInCl = String.Join(" ", changelist.Files);

                                // We need to fstat that CL to discover if it has any purged files
                                Log.MessageCtx(LOG_CTX, "Check if no purges : {0}", integration.BranchSpec);
                                P4API.P4RecordSet recordsFstat = p4.Run("fstat", "-e", firstChangelist.ToString(), filesInCl);
                                if (recordsFstat.Records.Any())
                                {
                                    IEnumerable<FileState> filestates = FileState.Create(p4, recordsFstat);

                                    foreach (FileState filestate in filestates.Where(fs => fs.HeadAction == FileAction.Purge))
                                    {
                                        // Ensure that CL is not used.
                                        firstChangelist = -1;
                                        Log.WarningCtx(LOG_CTX, "CL {0} has a file purged : {1}#{2}, it won't be used as the next integration, we hope that by ignoring this that later we will integrate files not purged - and that will clear a blockage.", firstChangelist, filestate.DepotFilename, filestate.HeadRevision);
                                    }
                                }

                                // We have a CL no need to keep checking
                                if (firstChangelist > 0)
                                {
                                    break;
                                }
                            }
                        }

                        // all changelists were containing purges?
                        if (firstChangelist < 0)
                        {
                            Log.MessageCtx(LOG_CTX, "all changelists were containing purges : {0}", integration.BranchSpec);
                            this.IntegrationRoundRobinIdx++;
                            continue;
                        }

                        // Get who or for what reason the integration is disabled.
                        String disabledReason = GetIntegrationUserDisabled(p4, integration);

                        if (String.IsNullOrEmpty(disabledReason))
                        {
                            // loop over jobs where they process this branchspec
                            foreach (IntegratorJob integratorJob in this.PendingJobs.Where(j => ((IntegratorJob)j).Integration.BranchSpec == integration.BranchSpec))
                            {
                                Log.MessageCtx(LOG_CTX, "Found job '{0}' for client '{1}'.", integratorJob.ID.ToString(), machinename.ToLower());

                                // Accept job if it has a CL number that is equal to the first that should be integrated 
                                // OR if the job is before this CL ( that will at least clear the job if this is no longer required to be integrated )
                                if (integratorJob.SourceChangelist <= firstChangelist || (bool)this.Parameters[PARAM_BLOCKING_ENABLED] == false)
                                {
                                    // when a job is returned - we move onto the next integration next time - round robin.
                                    this.IntegrationRoundRobinIdx++;
                                    return integratorJob;
                                }
                                else
                                {
                                    Log.WarningCtx(LOG_CTX, "Found job '{0}' for client '{1}'.... but this is blocked on pending CL {2}", integratorJob.ID.ToString(), machinename.ToLower(), firstChangelist);

                                    AlertOnIncompleteIntegration(p4, integratorJob, firstChangelist);
                                }
                            }
                        }

                        // No jobs for this branchspec - you lost your place in the round robin queue.
                        this.IntegrationRoundRobinIdx++;
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Return status of task.
        /// </summary>
        /// <returns></returns>
        public override TaskStatus Status()
        {
            return (new IntegratorTaskStatus(this));
        }

        /// <summary>
        /// Set tasks current job (if it keeps track).
        /// </summary>
        /// <param name="job"></param>
        public override void SetCurrentJob(IJob job)
        {
            if (!(job.Trigger is IChangelistTrigger))
                return;
        }

        /// <summary>
        /// Load state from disk.
        /// </summary>
        public override void Load()
        {
            this.Log.MessageCtx(LOG_CTX, "Loading state from {0}.", this.Filename);
            try
            {
                if (!File.Exists(this.Filename))
                    return; // No state to load.

                XDocument xmlDoc = XDocument.Load(this.Filename);
                String typeName = ((IEnumerable)xmlDoc.Root.XPathEvaluate("/Task/@type")).
                    Cast<XAttribute>().First().Value;
                Debug.Assert(0 == String.Compare(this.GetType().Name, typeName),
                    String.Format("Loading task status information from incorrect file: {0} {1}.",
                    this.Name, this.Filename));

                // Load jobs
                LoadJobs(xmlDoc);

                // Load job results
                LoadJobResults(xmlDoc);
            }
            catch (Exception ex)
            {
                this.Log.ToolExceptionCtx(LOG_CTX, ex, "Error deserialising status from disk: {0}.",
                    this.Filename);
            }
        }

        /// <summary>
        /// Save state to disk.
        /// </summary>
        public override void Save()
        {
            this.Log.MessageCtx(LOG_CTX, "Saving state to {0}.", this.Filename);
            try
            {
                XDocument xmlDoc = new XDocument(
                    new XDeclaration("1.0", "UTF-8", "yes"),
                    new XElement("Task",
                        new XAttribute("name", this.Name),
                        new XAttribute("type", this.GetType().Name),
                        new XElement("Jobs", this.Jobs.Select(j => j.Serialise())),
                        new XElement("JobResults", this.JobResults.Select(jr => jr.Serialise())))
                );

                String directoryName = Path.GetDirectoryName(this.Filename);
                if (!Directory.Exists(directoryName))
                    Directory.CreateDirectory(directoryName);
                lock (this.Filename)
                {
                    xmlDoc.Save(this.Filename);
                }
            }
            catch (Exception ex)
            {
                this.Log.ToolExceptionCtx(LOG_CTX, ex, "Error serialising status to disk: {0}.",
                    this.Filename);
            }
        }
        #endregion // Controller Methods

        #region Private Methods

        /// <summary>
        /// Read the integration description to determine if we wish to integrate or not?
        /// - this gives users control over integrations.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="integration"></param>
        /// <returns>the reason for disablement</returns>
        private string GetIntegrationUserDisabled(P4 p4, Integration integration)
        {
            // Check if the branchspec is enabled?
            String disabledReason = null;
            String description = GetBranchSpecDescription(p4, integration);
            Match match = Regex.Match(description, REGEX_DISABLE_BRANCHSPEC, RegexOptions.IgnoreCase);
            if (match.Success && match.Groups.Count >= 2)
            {
                disabledReason = match.Groups[1].Value.Trim();
                if (!String.IsNullOrEmpty(disabledReason))
                {
                    Log.MessageCtx(LOG_CTX, "*** ");
                    Log.MessageCtx(LOG_CTX, "*** Branchspec {0} is disabled for the reason '{1}' in the branchspec description. Uses a regex : {2}", integration.BranchSpec, disabledReason, REGEX_DISABLE_BRANCHSPEC);
                    Log.MessageCtx(LOG_CTX, "*** ");
                }
            }
            return disabledReason;
        }

        /// <summary>
        /// Read the integration description to determine if there are any email addresses
        /// - these email addresses ( if of rockstar ) will also be emailed.
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="integration"></param>
        /// <returns></returns>
        private IEnumerable<String> GetIntegrationBranchSpecEmails(P4 p4, Integration integration)
        {
            // Check if the branchspec is enabled?
            ISet<String> emails = new HashSet<String>();

            if (!(bool)this.Parameters[PARAM_ALERT_BRANCHSPEC_USERS])
                return emails;

            String description = GetBranchSpecDescription(p4, integration);
            MatchCollection matches = Regex.Matches(description, REGEX_EMAIL_BRANCHSPEC, RegexOptions.IgnoreCase);
            foreach (Match match in matches)
            {
                String email = match.Value.Trim();
                if (!String.IsNullOrEmpty(email))
                {
                    Log.MessageCtx(LOG_CTX, "*** ");
                    Log.MessageCtx(LOG_CTX, "*** Read email {0} from branchspec {1}", email, integration.BranchSpec);
                    Log.MessageCtx(LOG_CTX, "*** ");
                    emails.Add(email);
                }
            }

            return emails;
        }

        /// <summary>
        /// Get the description of the integration ( from the branch spec )
        /// </summary>
        /// <param name="p4"></param>
        /// <param name="integration"></param>
        /// <returns></returns>
        private String GetBranchSpecDescription(P4 p4, Integration integration)
        {
            String description = String.Empty;
            P4API.P4RecordSet recordsBranches = p4.Run("branches", "-e", integration.BranchSpec);
            P4Record rec = recordsBranches.Records.Where(r => r.Fields.ContainsKey("Description")).FirstOrDefault();
            if (rec != default(P4Record))
            {
                description = rec.Fields["Description"];
            }

            return description;
        }

        /// <summary>
        /// Get an enumerable of integrations to process in a particular order.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<Integration> GetPrioritisedIntegrationsToProcess()
        {
            // robin robin load balancing.
            int numIntegrations = this.Integrations.Count();
            int indexOfBranchSpecToService = IntegrationRoundRobinIdx % numIntegrations;

            Log.MessageCtx(LOG_CTX, "prioritising intergration {0}", this.Integrations.ElementAt(indexOfBranchSpecToService).BranchSpec);

            // Order the integration 'branchspecs' that we will consider.
            ICollection<Integration> integrationsToCheck = new List<Integration>();
            for (int i = indexOfBranchSpecToService; i < numIntegrations; i++)
                integrationsToCheck.Add(this.Integrations.ElementAt(i));

            for (int i = 0; i < indexOfBranchSpecToService; i++)
                integrationsToCheck.Add(this.Integrations.ElementAt(i));

            return integrationsToCheck;
        }

        private IEnumerable<IntegratorJobResult> BlockingJobResults()
        {
            return this.JobResults.Cast<IntegratorJobResult>().Where(jr => jr.PendingIntegrationChangelistNumber >= 0);
        }

        private IEnumerable<IntegratorJobResult> NonBlockingJobResults()
        {
            return this.JobResults.Cast<IntegratorJobResult>().Where(jr => jr.PendingIntegrationChangelistNumber < 0);
        }

        private void LogBlockageState()
        {
            int numBlockages = this.BlockingJobResults().Count();

            if (numBlockages > 0)
            {
                StringBuilder sb = new StringBuilder();
                foreach (IntegratorJobResult integratorJobResult in this.BlockingJobResults())
                {
                    sb.Append("(");
                    foreach (IntegratorJob integratorJob in Jobs.Where(j => j.ID == integratorJobResult.JobId))
                    {
                        sb.Append(integratorJob.Integration.BranchSpec);
                        sb.Append(" - ");
                    }
                    sb.Append(integratorJobResult.PendingIntegrationChangelistNumber);
                    sb.Append(") ");
                }

                Log.MessageCtx(LOG_CTX, "***");
                Log.WarningCtx(LOG_CTX, "*** TASK HAS {0} BLOCKAGES: {1}", numBlockages, sb.ToString());
                Log.MessageCtx(LOG_CTX, "***");
            }
            else if (!this.OutstandingJobs.Any())
            {
                Log.MessageCtx(LOG_CTX, "***");
                Log.MessageCtx(LOG_CTX, "*** ALL INTEGRATIONS ARE UP TO DATE");
                Log.MessageCtx(LOG_CTX, "***");
            }
        }

        /// <summary>
        /// Search completed integrated jobs 
        /// if they completed without error
        /// If they processed the pending CL then issue an alert 
        /// - something has gone wrong we are not waiting on this integration ( part integrated - shouldn't but can happen! - a long story ) 
        /// </summary>
        /// <param name="integratorJob"></param>
        /// <param name="blockingChangelist"></param>
        private void AlertOnIncompleteIntegration(P4 p4, IntegratorJob pendingIntegratorJob, int blockingChangelist)
        {
            int hours = (int)this.Parameters[PARAM_BLOCKED_ALERT_HOURS];
            TimeSpan alertRate = TimeSpan.FromHours((double)hours);
            TimeSpan elapsed = DateTime.UtcNow - LastBlockedIntegrationAlert;

            if (elapsed > alertRate)
            {
                IntegratorJob completedIntegratorJob = this.Jobs.Cast<IntegratorJob>().Where(j => (j.State == JobState.Completed &&
                                j.Integration.BranchSpec == pendingIntegratorJob.Integration.BranchSpec &&
                                j.SourceChangelist == blockingChangelist)).FirstOrDefault();

                if (completedIntegratorJob != null)
                {
                    IntegratorJobResult integratorJobResult = (IntegratorJobResult)JobResults.Where(jr => jr.JobId == completedIntegratorJob.ID).FirstOrDefault();

                    if (integratorJobResult != null &&
                        integratorJobResult.SubmittedChangelistNumber >= 0)
                    {
                        // Get extra emails to notify about integration.
                        IEnumerable<String> userEmails = GetIntegrationBranchSpecEmails(p4, completedIntegratorJob.Integration);

                        String message = String.Format("Integration(s) blocked on CL {0} ( It may have been part integrated  - please integrate it now  )", blockingChangelist);
                        Log.ErrorCtx(LOG_CTX, message);
                        this.GeneralAlert(Log, message, String.Empty, userEmails);
                        LastBlockedIntegrationAlert = DateTime.UtcNow;
                    }
                }
            }
        }

        /// <summary>
        /// p4 interchanges for all integrations
        /// Used for 2 purposes...
        /// - identifies changelists needing integrated so to create jobs.
        /// - holds up client job allocations until failed integrations are resolved by hand.
        /// </summary>
        /// <returns></returns>
        private IDictionary<String, P4API.P4RecordSet> GetAllIntegrationsInterChanges(P4 p4)
        {
            // Cache all interchanges on branc specs for all known integrations
            IDictionary<String, P4API.P4RecordSet> recordSetsDict = new Dictionary<String, P4API.P4RecordSet>();

            foreach (Integration integration in this.Integrations)
            {
                /// **** this is the ONLY time we should update the modified time of the branchspec in the cache ****
                P4API.P4RecordSet records = ((bool)this.Parameters[PARAM_CACHED_INTERCHANGES]) ? p4.InterchangesCached(Log, integration.BranchSpec, updateModifiedTime : true) : p4.Run("interchanges", "-b", integration.BranchSpec);
                recordSetsDict.Add(integration.BranchSpec, records);
            }
            return recordSetsDict;       
        }

        /// <summary>
        /// For each integration, get the changelists requiring integration and create jobs for it.
        /// </summary>
        private void CreateIntegrationJobs(P4 p4, IDictionary<String, DateTime> branchSpecModifiedDict)
        {
            bool updated = false;
            IDictionary<String, P4API.P4RecordSet> recordSetsDict = GetAllIntegrationsInterChanges(p4);
            IDictionary<uint, Guid> CLsToTriggerId = new Dictionary<uint, Guid>();
            foreach (Integration integration in Integrations)
            {
                IEnumerable<Changelist> changelists = new List<Changelist>();

                if (!recordSetsDict.ContainsKey(integration.BranchSpec))
                {
                    Log.ErrorCtx(LOG_CTX, "Branchspec {0} is not known.", integration.BranchSpec);
                }

                P4API.P4RecordSet records = recordSetsDict[integration.BranchSpec];

                if (records == null || !records.Records.Any())
                {
                    Log.MessageCtx(LOG_CTX, "***");
                    Log.MessageCtx(LOG_CTX, "*** Branchspec {0} is integrated to the head.", integration.BranchSpec);
                    Log.MessageCtx(LOG_CTX, "***");
                }
                else
                {
                    changelists = (Changelist.CreateFromChangesRecords(p4, records).OrderBy(cl => cl.Number)).ToList();

                    if (changelists.Any())
                    {
                        DateTime lastModified = p4.GetBranchSpecModifiedTime(integration.BranchSpec, false);

                        foreach (Changelist changelist in changelists)
                        {
                            if (!CLsToTriggerId.ContainsKey(changelist.Number))
                            {
                                CLsToTriggerId.Add(changelist.Number, Guid.NewGuid());
                            }

                            // Create jobs for this changelist (that require integrating) if we can't find any job that has it in hand.. be it processed, pending, errored or otherwise.
                            // This should be safe in regards to periodic job cleaning too ( courtesy of NonBlockingJobResults() )

                            bool integrationIsInHand = false;

                            foreach (IntegratorJob integratorJob in this.Jobs.Cast<IntegratorJob>().
                                    Where(j => j.SourceChangelist == changelist.Number && j.Integration.BranchSpec == integration.BranchSpec))
                            {
                                if (integratorJob.State == JobState.Pending)
                                {
                                    integrationIsInHand = true;
                                }
                                else
                                {
                                    // You can only say an integration is in hand if the branchspec has not been updated 
                                    // - otherwise - go ahead and create jobs for it *** IF those jobs are not Pending ***
                                    // that means the job is already processed and we will process it again.
                                    // If the branch spec hasn't changed it's ok to say this is in hand - the job ran it either errored of completed and we are done with it.
                                    if (branchSpecModifiedDict[integration.BranchSpec] == lastModified)
                                    {
                                        integrationIsInHand = true;
                                    }

                                    Log.MessageCtx(LOG_CTX, "*** For outstanding integration {0} : {1} IntegratorJob is {2}", integration.BranchSpec, changelist.Number, integratorJob.State);
                                }
                            }

                            if (!integrationIsInHand)
                            {
                                Log.MessageCtx(LOG_CTX, "***");
                                Log.MessageCtx(LOG_CTX, "*** Creating new IntegratorJob from changelist {0} : {1}", changelist.Number, integration);
                                Log.MessageCtx(LOG_CTX, "***");
                                IntegratorJob ijob = new IntegratorJob(changelist, integration, Options.Branch, CLsToTriggerId[changelist.Number]);
                                this.EnqueueJob(ijob);
                            }
                        }
                    }
                }

                if (this.UpdateBlockingJobResults(integration, changelists))
                {
                    updated = true;
                }
            }

            if (updated)
            {
                // Saving because job results have updated.
                Save();
            }
        }

        private void AlertOnAllBlockages(P4 p4)
        {
            if (p4 == null)
            {
                Log.ErrorCtx(LOG_CTX, "P4 is null, we may not alert!");
                return;
            }

            foreach (IntegratorJobResult integratorJobResult in BlockingJobResults())
            {
                this.AlertBlockedIntegrations(integratorJobResult, p4);
            }
        }

        /// <summary>
        /// Spams/Reminds users to sort out their integrations.
        /// - this is the only communication channel to users.
        /// </summary>
        /// <param name="integratorJobResult"></param>
        private void AlertBlockedIntegrations(IntegratorJobResult integratorJobResult, P4 p4)
        {
            int hours = (int)this.Parameters[PARAM_BLOCKED_ALERT_HOURS];
            TimeSpan alertRate = TimeSpan.FromHours((double)hours);
            TimeSpan elapsed = DateTime.UtcNow - LastBlockedIntegrationAlert;
            Log.MessageCtx(LOG_CTX, "===> AlertBlockedIntegrations : AlertRate {0} DateTimeNow {1} Last alert {2}", alertRate, DateTime.UtcNow, LastBlockedIntegrationAlert);
            if (elapsed > alertRate)
            {
                Log.MessageCtx(LOG_CTX, "AlertBlockedIntegrations : Alert rate has elapsed");
                this.LoadNotificationData();

                StringBuilder sb = new StringBuilder();

                sb.AppendLine(String.Format("=================================================================================="));
                sb.AppendLine(String.Format("* ACTION REQUIRED - AUTOMATED INTEGRATIONS ARE BLOCKED - This alert will occur every {0} minutes *", alertRate.TotalMinutes));

                ICollection<String> extraSendList = new List<String>();

                bool sendEmail = false;

                String branchSpec = String.Empty;

                foreach (IntegratorJob integratorJob in Jobs.Where(j => j.ID == integratorJobResult.JobId))
                {
                    branchSpec = integratorJob.Integration.BranchSpec;
                    P4API.P4RecordSet records = ((bool)this.Parameters[PARAM_CACHED_INTERCHANGES]) ? p4.InterchangesCached(Log, branchSpec, updateModifiedTime: false) : p4.Run("interchanges", "-b", branchSpec);
                    IEnumerable<Changelist> changelists = records!=null ? (Changelist.CreateFromChangesRecords(p4, records).OrderBy(cl => cl.Number)).ToList() : new List<Changelist>();

                    if ((changelists.Any() && changelists.First().Number > integratorJob.SourceChangelist) || !changelists.Any())
                    {
                        Log.WarningCtx(LOG_CTX, "Job {0} would have sent an alert if it were for not the fact that it seems to be already integrated.", integratorJobResult.JobId);
                        continue;
                    }

                    Log.MessageCtx(LOG_CTX, "Send email flag set");
                    sendEmail = true;

                    // Add the user of blocked CL to a list of usernames that will be reminded of their blockage.
                    String username = GetJobUserName(integratorJob);
                    if ((bool)this.Parameters[PARAM_ALERT_USERS])
                    {
                        extraSendList.Add(username);
                        Log.MessageCtx(LOG_CTX, "!!! Directing blocked integration alert to '{0}' !!!", username);
                    }
                    else
                    {
                        Log.WarningCtx(LOG_CTX, "User '{0}' has not been alerted about blockage.", username);
                    }

                    sb.AppendLine(String.Format("\n=== BLOCKING INTEGRATION ==="));
                    sb.AppendLine(String.Format("\tBranchspec= \t{0}", branchSpec));
                    sb.AppendLine(String.Format("\tChangelist= \t{0} ({1})", integratorJobResult.PendingIntegrationChangelistNumber, username)); 
                    sb.AppendLine(String.Format("\tP4 Resolve= \t{0}", integratorJob.Integration.P4ResolveOptions));

                    // Get extra emails to notify about integration.
                    IEnumerable<String> branchSpecEmails = GetIntegrationBranchSpecEmails(p4, integratorJob.Integration);

                    if (branchSpecEmails.Any())
                    {
                        sb.AppendLine(String.Format("\n=== EMAILED {0} ADDTIONAL USERS (as derived from branchspec desc) ===", branchSpecEmails.Count()));
                        foreach (String branchSpecEmail in branchSpecEmails)
                        {
                            sb.AppendLine(String.Format("\t{0}", branchSpecEmail));
                            extraSendList.Add(branchSpecEmail);
                            Log.MessageCtx(LOG_CTX, "!!! Directing blocked integration alert to '{0}' !!!", branchSpecEmail);
                        }
                    }

                    sb.AppendLine(String.Format("\n=== RESOLUTION/WIKI ==="));
                    sb.AppendLine(String.Format("\t{0}\n", PROCEDURE_WIKI));

                    sb.AppendLine(String.Format("=================================================================================="));
                }

                if (sendEmail)
                {                  
                    Log.MessageCtx(LOG_CTX, "Sending Email");
                    this.GeneralAlert(Log, String.Format("Integration(s) blocked : {0}", branchSpec), sb.ToString(), extraSendList);
                }
                else
                {
                    Log.WarningCtx(LOG_CTX, "Not sending email");
                }
                
                this.LastBlockedIntegrationAlert = DateTime.UtcNow;
            }
        }
  
        /// <summary>
        /// Method to clean out jobs and their files.
        /// </summary>
        private void CleanJobs()
        {
            int hours = (int)this.Parameters[PARAM_HOURS_JOB];
            DateTime modifiedTime = DateTime.UtcNow - TimeSpan.FromHours((double)hours);

            // We now just delete any job that has existed for the specified time rather than
            // specifically just completed jobs.
            lock (this.m_Jobs)
            {
                SortedSet<Guid> jobResultIDs = new SortedSet<Guid>(this.NonBlockingJobResults().Select(jr => jr.JobId));

                foreach (IntegratorJob job in this.Jobs)
                {
                    DateTime completionTime = (job.ProcessedAt + job.ProcessingTime);
                    if (completionTime.IsEarlierThan(modifiedTime) &&
                        jobResultIDs.Contains(job.ID) )
                    {
                        this.CleanJob(job);
                    }
                }
            }

            SortedSet<Guid> jobIDs = new SortedSet<Guid>(this.Jobs.Select(job => job.ID));
            lock (this.m_JobResults)
            {
                IJobResult[] resultsToRemove = m_JobResults.Where(jobResult => !jobIDs.Contains(jobResult.JobId)).ToArray();
                foreach (IJobResult resultToRemove in resultsToRemove)
                {
                    this.m_JobResults.Remove(resultToRemove);
                }
            }
        }

        /// <summary>
        /// Remove job from our queue and clean necessary files.
        /// </summary>
        /// <param name="job"></param>
        private void CleanJob(IJob job)
        {
            lock (this.m_Jobs)
            lock (this.m_JobResults)
            {
                IEnumerable<KeyValuePair<JobPriority, IJob>> jobEntries =
                this.m_Jobs.Where(j => j.Value.ID.Equals(job.ID));

                if (jobEntries.Count() > 0)
                    this.m_Jobs.Remove(jobEntries.First());
            }

            Services.FileTransferService.CleanJobFiles(this.Config, job, this.Log);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private String GetSettingsFilename()
        {
            return (Path.Combine(this.Config.ToolsConfig, "automation",
                "tasks", String.Format("{0}.xml", this.GetType().Name)));
        }

        /// <summary>
        /// Loads the xml integrations file
        /// </summary>
        private void LoadIntegrations()
        {
            (this.Integrations as List<Integration>).Clear();
            String monitoringFile = this.Config.Environment.Subst((String)this.Parameters[PARAM_MONITOR_PATH_FILE]);
            if (!File.Exists(monitoringFile) && this.Integrations.Count() == 0)
            {
                this.Log.ErrorCtx(LOG_CTX, "Monitoring file does not exist ({0}).  Integrator task will not be monitoring any locations.", monitoringFile);
            }

            XDocument xdoc = XDocument.Load(monitoringFile);
            ICollection<Integration> integrations = new List<Integration>();
            foreach (XElement elem in xdoc.Descendants("Integration"))
            {
                Integration integration = new Integration(elem);
                integration.Deserialise(elem);

                integrations.Add(integration);
            }

            (this.Integrations as List<Integration>).AddRange(integrations);
        }

        /// <summary>
        /// Helper to get a job's username.
        /// </summary>
        /// <param name="integratorJob"></param>
        /// <returns></returns>
        private static string GetJobUserName(IntegratorJob integratorJob)
        {
            IEnumerable<String> usernames = integratorJob.Trigger.Usernames();
            string username = usernames.Any() ? usernames.Last() : "unknown";
            return username;
        }

        /// <summary>
        /// Updates Job Results to reflect that they have been fixed.
        /// - notifies in console too.
        /// </summary>
        /// <param name="integration"></param>
        /// <param name="changelists"></param>
        /// <returns>bool to indicate it updated.</returns>
        protected bool  UpdateBlockingJobResults(Integration integration, IEnumerable<Changelist> changelists)
        {
            bool updated = false;
            // Clear the state of job results if we are no longer requiring their integration.
            foreach (IntegratorJobResult integratorJobResult in this.JobResults)
            {
                bool containsCL = changelists.Any(c => integratorJobResult.PendingIntegrationChangelistNumber == c.Number);

                if (!containsCL &&
                    integratorJobResult.PendingIntegrationChangelistNumber >= 0)
                {
                    foreach (IntegratorJob integratorJob in Jobs.Where(j => j.ID == integratorJobResult.JobId))
                    {
                        if (integratorJob.Integration.BranchSpec == integration.BranchSpec)
                        {
                            Log.MessageCtx(LOG_CTX, "***");
                            Log.MessageCtx(LOG_CTX, "*** IntegratorJob no longer blocking! someone has integrated {0} : {1}", integratorJobResult.PendingIntegrationChangelistNumber, integration);
                            Log.MessageCtx(LOG_CTX, "***");
                            integratorJobResult.PendingIntegrationChangelistNumber = (int)JobSubmissionState.Integrated;
                            this.LastBlockedIntegrationAlert = DateTime.MinValue;
                        }
                    }
                    updated = true;
                }
            }
            return updated;
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.ServerHost.Tasks namespace
