﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Logging;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Common.JobManagement;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Services;
//using RSG.Platform;
using P4API;

namespace RSG.Pipeline.Automation.ServerHost.Tasks
{
    internal class CodeBuilderTask :
        MonitoringTaskBase,
        IMonitoringTask,
        IJobManagedTask
    {
        #region Constants
        private static readonly String NAME = "Code Builder Task";
        private static readonly String DESC = "Code building task.";
        private static readonly String LOG_CTX = "Code Builder Task";

        private static readonly String PARAM_HOURS_JOB = "Hours to Keep Completed Jobs";
        private static readonly String PARAM_EMAIL_NOTIFICATIONS_FILE = "Notifications File";
        private static readonly String PARAM_MONITOR_PATH_FILE = "Path Monitoring File";
        private static readonly String PARAM_JOB_DEFINITION_FILE = "JobDef Build Combinations";

        /// <summary>
        /// Parameter for the file listing job management data.
        /// </summary>
        private static readonly string PARAM_DISPATCH_SCHEME_FILE = "Dispatch Scheme File";

        /// <summary>
        /// 
        /// </summary>
        public DispatchScheme DispatchScheme { get; set; }
        #endregion

        /// <summary>
        /// Build Configurations read in from the XML: CodebuilderCompilerConfigurations.xml
        /// </summary>
        public static HashSet<String> BuildConfigurations { get; set; }

        #region Member Data
        /// <summary>
        /// Task parameters.
        /// </summary>
        private IDictionary<String, Object> Parameters;
        private List<IJob> CodeBuilderJobs;

        private DateTime m_LastMonitoringFileWriteTime;
        #endregion

        #region Constructors
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CodeBuilderTask(IEnumerable<String> locations, CommandOptions options)
            : base(NAME, DESC, Common.CapabilityType.CodeBuilder, locations, options)
        {
            this.CodeBuilderJobs = new List<IJob>();

            String filename = GetSettingsFilename();
            m_LastMonitoringFileWriteTime = DateTime.MinValue;

            IDictionary<String, Object> parameters = new Dictionary<String, Object>();
            RSG.Base.Xml.Parameters.Load(filename, ref parameters);
            this.Parameters = parameters;
            this.DispatchScheme = new DispatchScheme(this.Config, options, this.Log, this.GetJobManagementFile());
        }
        #endregion

        #region Controller Methods

        /// <summary>
        /// Regular update loop.
        /// </summary>
        public override void Update()
        {
            // Allows us to update monitoring locations without restarting the service.
            this.LoadMonitoringLocations();
            // Allow us to update job dispatch data without restarting the service
            lock (this.DispatchScheme)
            {
                bool syncDispatch = this.Parameters.ContainsKey(PARAM_SYNC_DISPATCH) && (bool)this.Parameters[PARAM_SYNC_DISPATCH];
                this.DispatchScheme.LoadDispatchData(this.GetJobManagementFile(), this.Options, syncDispatch);
            }

            // Get list of changelists submitted into monitoring paths from base class; turn these info Jobs.
            IEnumerable<Changelist> changelists = base.GetSubmittedChangelists();
            if (changelists.Count() > 0)
            {
                using (P4 p4 = this.Options.Config.Project.SCMConnect())
                {
                    this.MaximumChangelist = changelists.Select(changelist => changelist.Number).Max();
                    Changelist maxChangelist = changelists.Where(changelist => (changelist.Number == this.MaximumChangelist)).First();

                    Log.MessageCtx(LOG_CTX, "Packaging code submission up to Change {0}.  Containing {1} changes.", this.MaximumChangelist, changelists.ToList().Count);

                        
                    //  This will generate jobs from X:\rdr3\tools\etc\automation\tasks\jobdefinitions\CodeBuild_Combinations.xml
                    if (!GenerateJobsFromMonitoring(maxChangelist, maxChangelist.Files, this.MonitoredPaths))
                    {
                        Log.ErrorCtx(LOG_CTX, "Error creating jobs from monitoring paths.");
                        return;
                    }
                }
            }
            
            else
            {
                int pendingJobs = this.CodeBuilderJobs.Where(x => (x.State == JobState.Pending || x.State == JobState.Assigned)).Count();
                if (pendingJobs == 0)
                {
                    int numSucceeded = this.CodeBuilderJobs.Where(job => job.State == JobState.Completed).Count();
                    int numFailed = this.CodeBuilderJobs.Count() - numSucceeded;

                    Log.MessageCtx(LOG_CTX, "Code building pass completed.  " + numSucceeded + " succeeded.  " + numFailed + " failed.");

                    this.CodeBuilderJobs.Clear();
                }
                else
                {
                    Log.MessageCtx(LOG_CTX, String.Format("{0} jobs remaining to be processed.", pendingJobs));
                }
            } 
            this.CleanJobs();
        }

        /// <summary>
        /// Function to create all combinations of build possibilities to be turned into jobs. 
        /// </summary>
        /// <returns></returns>
        private bool GenerateJobsFromMonitoring(Changelist CL, IEnumerable<String> files, IEnumerable<String> monitoringPaths)
        {
            if (this.Parameters.ContainsKey(PARAM_JOB_DEFINITION_FILE) == true)
            {
                String jobCombinationsFile = this.Config.Environment.Subst((String)this.Parameters[PARAM_JOB_DEFINITION_FILE]);
                XDocument xmlDoc = XDocument.Load(jobCombinationsFile);
                IEnumerable<XElement> xmlJobs = xmlDoc.Descendants("Job");
                //This is to mimick formatting from MonitoringTaskBase.GetSubmittedChangelists()
                monitoringPaths.ForEach(f => f = f + "/...");
                xmlJobs.ForEach(xmlJob =>
                {
                    CodeBuilderJob job = new CodeBuilderJob(xmlJob, CapabilityType.CodeBuilder, Options.Branch);
                    job.Trigger = new ChangelistTrigger(CL, files, monitoringPaths);
                    CodeBuilderJobs.Add(job);
                    this.EnqueueJob(job);
                }
                );
            }
            else
            {
                Log.ErrorCtx(LOG_CTX, "Missing CodeBuildMonitoring_Combinations.xml. Refresh your tools directory."); 
                return false;
            }
            return true; 
        }

        /// <summary>
        /// Loads all mointoring paths for this jobs to run.
        /// </summary>
        /// <returns></returns>
        private void LoadMonitoringLocations()
        {
            String monitoringFile = this.Config.Environment.Subst((String)this.Parameters[PARAM_MONITOR_PATH_FILE]);
            if (!File.Exists(monitoringFile) && this.MonitoredPaths.Count() == 0)
            {
                this.Log.WarningCtx(LOG_CTX, "Monitoring file does not exist ({0}).  Code builder task will not be monitoring any locations.", monitoringFile);
            }

            if (m_LastMonitoringFileWriteTime >= File.GetLastWriteTimeUtc(monitoringFile))
            {
                //No need to reload.
                return;
            }

            (this.MonitoredPaths as List<String>).Clear();
            m_LastMonitoringFileWriteTime = File.GetLastWriteTimeUtc(monitoringFile);

            XDocument xdoc = XDocument.Load(monitoringFile);
            List<String> locations = new List<String>();
            foreach (XElement elem in xdoc.Descendants("location"))
            {
                String path = elem.Attribute("path").Value;
                locations.Add(this.Options.Branch.Environment.Subst(path));
            }
            foreach (String path in locations)
            {
                this.Log.MessageCtx(LOG_CTX, "Trying to monitor: {0}", path);
            }

            FileMapping[] fms = FileMapping.Create(this.P4, locations.ToArray());
            (this.MonitoredPaths as List<String>).AddRange(fms.Select(fm => fm.DepotFilename.Replace("/...", "")));
            foreach (String path in MonitoredPaths)
            {
                this.Log.MessageCtx(LOG_CTX, "Parsed trying to monitor: {0}", path);
            }
        }

        /// <summary>
        /// Task notification of job being completed.
        /// </summary>
        /// <param name="job"></param>
        public override void JobCompleted(IJob job, IEnumerable<IJobResult> jobResults)
        {
            base.JobCompleted(job, jobResults);
            try
            {
                IJob finishedJob = this.Jobs.Single(j => j.ID == job.ID);
                finishedJob.State = job.State;
            }
            catch (InvalidOperationException)
            {
               Log.ErrorCtx(LOG_CTX, "Unable to find job that got results for.");
            }
            finally 
            {
                Log.DebugCtx(LOG_CTX, "Job list null, but got job results.");
            }
        }

        /// <summary>
        /// Cleans all jobs.
        /// </summary>
        public void CleanJobs()
        {
            int hours = (int)this.Parameters[PARAM_HOURS_JOB];
            DateTime modifiedTime = DateTime.UtcNow - TimeSpan.FromHours((double)hours);

            IEnumerable<IJob> completedJobs = this.Jobs.Where(j =>
                JobState.Completed == j.State ||
                JobState.Errors == j.State ||
                JobState.Aborted == j.State);
            foreach (IJob job in completedJobs)
            {
                DateTime completionTime = (job.ProcessedAt + job.ProcessingTime);
                if (completionTime.IsEarlierThan(modifiedTime))
                {
                    CleanJob(job);
                }
            }
        }

        /// <summary>
        /// Finds jobs matching dispatch groups monitoring path matches for rolling builds
        /// or Build and Platform configuration for builds generated via jobdef
        /// </summary>
        /// <param name="machinename"></param>
        /// <returns></returns>
        public override IJob GetSuitableJob(string machinename)
        {/*
            Log.MessageCtx(LOG_CTX, "Client '{0}' is requesting a suitable job.", machinename.ToLower());
            lock (this.DispatchScheme)
            {
                // Here we have a 'fallback' client, which processes anything left over.
                // So, we find the next job that has no dispatch group interested in it
                lock (this.m_Jobs)
                {
                    foreach (CodeBuilderJob job in this.PendingJobs.Where(j => j.Role == CapabilityType.CodeBuilder).ToList())
                    {
                       
                        //This will need an if around it in order to change how we handle jobdef defined jobs
                        foreach (DispatchGroup dg in this.DispatchScheme.Groups.ToList())
                        {
                            if (dg.MatchBuildSettings(job.Configuration) && dg.MatchesAnyPlatform(PlatformUtils.PlatformToCompilerSetting(job.Platform)))
                            {
                                Log.MessageCtx(LOG_CTX, "Found job '{0}' for client '{1}'.", job.ID.ToString(), machinename.ToLower());
                                return job;
                            }
                        }
                    }
                }
            }*/
            return null;
        }

        /// <summary>
        /// Remove job from our queue and clean necessary files.
        /// </summary>
        /// <param name="job"></param>
        private void CleanJob(IJob job)
        {
            lock (this.m_Jobs)
            {
                IEnumerable<KeyValuePair<JobPriority, IJob>> jobEntries =
                    this.m_Jobs.Where(j => j.Value.ID.Equals(job.ID));
                if (jobEntries.Count() > 0)
                    this.m_Jobs.Remove(jobEntries.First());
            }
            Services.FileTransferService.CleanJobFiles(this.Config, job, this.Log);
        }
        
        /// <summary>
        /// Acquire the local settings file.
        /// </summary>
        private String GetSettingsFilename()
        {
            return (Path.Combine(this.Config.ToolsConfig, "automation",
                "tasks", String.Format("{0}.xml", this.GetType().Name)));
        }


        /// <summary>
        /// File containing information required to send notifications.
        /// </summary>
        /// <returns></returns>
        public override String GetNotificationDataFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_EMAIL_NOTIFICATIONS_FILE]));
        }

        /// <summary>
        /// Returns location of dispatch sceheme data file.
        /// </summary>
        /// <returns></returns>
        public String GetJobManagementFile()
        {
            if (this.Parameters.ContainsKey(PARAM_DISPATCH_SCHEME_FILE) == true)
            {
                return (this.Config.Environment.Subst((String)this.Parameters[PARAM_DISPATCH_SCHEME_FILE]));
            }
            else
                return null;
        }
        #endregion
    }
}
