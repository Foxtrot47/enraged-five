﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Pipeline.Automation.Common.Export;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Services;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Services;

namespace RSG.Pipeline.Automation.ServerHost.Tasks
{

    /// <summary>
    /// Task that responds to user-submitted jobs.
    /// </summary>
    public class CutsceneExportNetworkTask :
        TaskBase,
        ITask
    {
        #region Constants
        private static readonly String NAME = "Cutscene Network-Export";
        private static readonly String DESC = "Auto-exporting network task.";
        private static readonly String LOG_CTX = "Cutscene Export Network Task";

        private static readonly String PARAM_HOURS_JOB = "Hours to Keep Completed Jobs";
        private static readonly string PARAM_EMAIL_NOTIFICATIONS_FILE = "Notifications File";
        #endregion // Constants

        #region Member Data
        #endregion // Member Data
        
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public CutsceneExportNetworkTask(CommandOptions options)
            : base(NAME, DESC, Common.CapabilityType.CutsceneExportNetwork, options)
        {
            String filename = GetSettingsFilename();
            IDictionary<String, Object> parameters = new Dictionary<String, Object>();
            RSG.Base.Xml.Parameters.Load(filename, ref parameters);
            this.Parameters = parameters;
        }
        #endregion // Constructor(s)
        
        #region Controller Methods
        /// <summary>
        /// Task update tick (not required for this task).
        /// </summary>
        public override void Update()
        {
            // Only do job maintainance; cleaning the queue and associated files.
            this.CleanJobs();
        }
        
        /// <summary>
        /// Return status information for this task.
        /// </summary>
        /// <returns></returns>
        public override TaskStatus Status()
        {
            return (new TaskStatus(this));
        }

        /// <summary>
        /// File containing information required to send notifications.
        /// </summary>
        /// <returns></returns>
        public override String GetNotificationDataFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_EMAIL_NOTIFICATIONS_FILE]));
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Return settings XML filename.
        /// </summary>
        /// <returns></returns>
        /// This is virtual allowing you to override it if required; although
        /// it's not recommended.
        /// 
        private String GetSettingsFilename()
        {
            return (Path.Combine(this.Config.ToolsConfig, "automation",
                "tasks", String.Format("{0}.xml", this.GetType().Name)));
        }

        /// <summary>
        /// Method to clean out jobs and their files.
        /// </summary>
        private void CleanJobs()
        {
            int hours = (int)this.Parameters[PARAM_HOURS_JOB];
            DateTime modifiedTime = DateTime.UtcNow - TimeSpan.FromHours((double)hours);

            IEnumerable<IJob> completedJobs = this.Jobs.Where(j =>
                JobState.Completed == j.State ||
                JobState.Errors == j.State ||
                JobState.Aborted == j.State);
            foreach (IJob job in completedJobs)
            {
                DateTime completionTime = (job.ProcessedAt + job.ProcessingTime);
                if (completionTime.IsEarlierThan(modifiedTime))
                {
                    CleanJob(job);
                }
            }
        }

        /// <summary>
        /// Remove job from our queue and clean necessary files.
        /// </summary>
        /// <param name="job"></param>
        private void CleanJob(IJob job)
        {
            lock (this.m_Jobs)
            {
                IEnumerable<KeyValuePair<JobPriority, IJob>> jobEntries =
                    this.m_Jobs.Where(j => j.Value.ID.Equals(job.ID));
                if (jobEntries.Count() > 0)
                    this.m_Jobs.Remove(jobEntries.First());
            }
            Services.FileTransferService.CleanJobFiles(this.Config, job, this.Log);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.ServerHost.Tasks namespace
