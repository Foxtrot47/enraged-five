﻿using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Batch;
using RSG.Pipeline.Automation.Common.JobManagement;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Services.Messages;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Services;
using RSG.SourceControl.Perforce;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace RSG.Pipeline.Automation.ServerHost.Tasks
{
    /// <summary>
    /// Script Builder server-side monitoring task.
    /// </summary>
    internal class ScriptBuilderTask :
        MonitoringTaskBase,
        IMonitoringTask,
        IJobManagedTask
    {
        #region Constants
        /// <summary>
        /// The task name.
        /// </summary>
        private static readonly String NAME = "Script Builder";

        /// <summary>
        /// The task description.
        /// </summary>
        private static readonly String DESC = "Script Builder monitoring task.";

        /// <summary>
        /// The task log context.
        /// </summary>
        private static readonly String LOG_CTX = "Automation:ScriptBuilderTask";

        /// <summary>
        /// Parameter for the file listing all the paths to monitor.
        /// </summary>
        private static readonly string PARAM_MONITOR_PATH_FILE = "Path Monitoring File";

        /// <summary>
        /// Parameter for the file listing email addresses to send notification data to.
        /// </summary>
        private static readonly string PARAM_EMAIL_NOTIFICATIONS_FILE = "Notifications File";

        /// <summary>
        /// Parameter for the file listing job management data.
        /// - the dispatch file also defines the builds to build.
        /// </summary>
        private static readonly string PARAM_DISPATCH_SCHEME_FILE = "Dispatch Scheme File";

        /// <summary>
        /// Parameter for number of hours between job cleans.
        /// </summary>
        private static readonly String PARAM_HOURS_JOB = "Hours to Keep Completed Jobs";

        /// <summary>
        /// Parameter for number of hours between job cleans of pending jobs
        /// - these can hang around when script projects to build are removed.
        /// </summary>
        private static readonly String PARAM_HOURS_TO_KEEP_PENDING_JOBS = "Hours to Keep Pending Jobs";

        /// <summary>
        /// Parameter for the file listing labels to set.
        /// </summary>
        private static readonly string PARAM_LABEL_FILE = "Label File";        

        #endregion // Constants

        #region Member Data

        /// <summary>
        /// The Dispatch scheme.
        /// - it stores the scriptbuilder configurations to build and where to dispatch them.
        /// </summary>
        public DispatchScheme DispatchScheme { get; set; }

        /// <summary>
        /// Stored to know when this file is modified
        /// </summary>
        private DateTime MonitoringFileLastModified { get; set; }

        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="location"></param>
        public ScriptBuilderTask(CommandOptions options)
            : this(new String[] { String.Empty }, options)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="locations"></param>
        public ScriptBuilderTask(IEnumerable<String> locations, CommandOptions options)
            : base(NAME, DESC, CapabilityType.ScriptBuilder, locations, options)
        {
            String filename = GetSettingsFilename();

            IDictionary<String, Object> parameters = new Dictionary<String, Object>();
            RSG.Base.Xml.Parameters.Load(filename, ref parameters);
            this.Parameters = parameters;

            this.MonitoringFileLastModified = DateTime.MinValue;
            this.LoadMonitoringLocations();

            this.DispatchScheme = new DispatchScheme(this.Config, options, this.Log, this.GetJobManagementFile());
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Update.
        /// </summary>
        public override void Update()
        {
            // Only do job maintainance; cleaning the queue and associated files.
            this.CleanJobs();

            // Reload all parameters
            String filename = GetSettingsFilename();
            IDictionary<String, Object> parameters = new Dictionary<String, Object>();
            RSG.Base.Xml.Parameters.Load(filename, ref parameters);
            this.Parameters = parameters;

            // Allows us to update monitoring locations without restarting the service.
            this.LoadMonitoringLocations();

            // Allows us to update label data without restarting the service.
            this.LoadLabelData();

            // Allow us to update job dispatch data without restarting the service.
            lock (this.DispatchScheme)
            {
                bool syncDispatch = this.Parameters.ContainsKey(PARAM_SYNC_DISPATCH) && (bool)this.Parameters[PARAM_SYNC_DISPATCH];
                this.DispatchScheme.LoadDispatchData(this.GetJobManagementFile(), this.Options, syncDispatch);
                AssignProcessingHostForPendingJobs();
            }

            // Get list of changelists from base class; turn these into Jobs.
            IEnumerable<Changelist> changelists = base.GetSubmittedChangelists();
            if (changelists.Count() > 0)
                this.MaximumChangelist = changelists.Select(changelist => changelist.Number).Max();

            int jobsCreated = 0;

            lock (this.m_Jobs) // must lock jobs otherwise one might be consumed before the whole list of jobs is known, when that happens job consumption is not to the head
            {
                foreach (Changelist changelist in changelists)
                {
                    Log.MessageCtx(LOG_CTX, "Considering changelist #{0} submitted by '{1}' at {2}, which contains {3} files",
                        changelist.Number, changelist.Username, changelist.Time.ToString(), changelist.Files.Count());

                    if (changelist.Files.Count() == 0)
                    {
                        Log.WarningCtx(LOG_CTX, "Empty changelist {0}: {1}.  Ignoring.", changelist.Number, changelist.Description);
                        continue;
                    }

                    // Ensure we only process files within a changelist that should be monitored.
                    List<String> monitoredFiles = new List<String>();
                    foreach (String monitoredPath in this.MonitoredPaths)
                        monitoredFiles.AddRange(changelist.Files.Where(f => f.ToLower().StartsWith(monitoredPath.ToLower())));

                    IEnumerable<FileMapping> changeListFiles = FileMapping.Create(this.P4, monitoredFiles.ToArray());

                    // Create jobs for this changelist
                    jobsCreated += CreateJobsForChangelist(changelist, changeListFiles);
                }

                if (jobsCreated > 0)
                {
                    Log.MessageCtx(LOG_CTX, " ");
                    Log.MessageCtx(LOG_CTX, "**********************");
                    Log.MessageCtx(LOG_CTX, "* {0,5} Jobs Created *", jobsCreated);
                    Log.MessageCtx(LOG_CTX, "**********************");
                    Log.MessageCtx(LOG_CTX, " ");
                    // Save the task when jobs are created - otherwise they could go missing if say the automation service crashed before the job could be executed.
                    Save();
                }
            }
        }

        /// <summary>
        /// Override the post job notification base method
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public override bool PostJobNotificationRequired(IJob job)
        {
            ScriptBuilderJob scriptbuilderJob = (ScriptBuilderJob)job;
            return scriptbuilderJob.RequiresPostJobNotification;
        }

        /// <summary>
        /// File containing information required to send notifications.
        /// </summary>
        /// <returns></returns>
        public override String GetNotificationDataFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_EMAIL_NOTIFICATIONS_FILE]));
        }

        /// <summary>
        /// Client requests for a suitable job
        /// - check the dispatch groups and return the most appropriate job.
        /// </summary>
        /// <param name="machinename"></param>
        /// <returns>a ScriptBuilderjob</returns>
        public override IJob GetSuitableJob(string machinename)
        {
            if (!base.CanGetSuitableJob(machinename))
                return null;

            Log.MessageCtx(LOG_CTX, "Client '{0}' is requesting a suitable job.", machinename.ToLower());
            lock (this.DispatchScheme)
            {
                // Find any suitable dispatch groups for this machine
                DispatchGroup[] suitableDispatchGroups = this.DispatchScheme.Groups.Where(jg => jg.MachineName == machinename.ToLower()).ToArray();

                if (suitableDispatchGroups.Length > 0)
                {
                    Log.MessageCtx(LOG_CTX, "Client '{0}' has suitable dispatch groups registered.", machinename.ToLower());

                    lock (this.m_Jobs)
                    {
                        foreach (ScriptBuilderJob scriptBuilderJob in this.PendingJobs.Where(j => j is ScriptBuilderJob))
                        {
                            foreach (DispatchGroup suitableDispatchGroup in suitableDispatchGroups)
                            {
                                foreach (ScriptProjectItem scriptProjectItem in suitableDispatchGroup.Items.Where(i => i is ScriptProjectItem))
                                {
                                    // If the job matches the settings of the ScriptProjectItem in the dispatch group 
                                    // then we can give this client worker this job
                                    if (ScriptBuilderTask.JobMatchesScriptProjectItem(scriptBuilderJob, scriptProjectItem))
                                    {
                                        Log.MessageCtx(LOG_CTX, "Found job '{0}' for client '{1}'.", scriptBuilderJob.ID.ToString(), machinename.ToLower());

                                        // Instead of assigning this job can we combine some jobs with this? ( skip to head ) then return that job.
                                        if (scriptBuilderJob.SkipConsume >= 0)
                                        {
                                            // Consume any jobs that can bind to this job.
                                            int numConsumed = ConsumeJobs(scriptBuilderJob);
                                            if (numConsumed > 0)
                                            {
                                                // Find the consuming job
                                                IJob consumingJob = this.PendingJobs.Single(j => j.ID == scriptBuilderJob.ConsumedByJobID);
                                                Log.MessageCtx(LOG_CTX, "Instead of giving out job {0} we have combined {1} jobs and will give {2} to {3}", scriptBuilderJob.ID, numConsumed, consumingJob.ID, machinename.ToLower());
                                                return consumingJob;
                                            }
                                        }
                                        return scriptBuilderJob;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    Log.ErrorCtx(LOG_CTX, "Client '{0}' doesn't have any dispatch groups registered, scriptbuilder does not support a 'fallback' - setup the dispatch groups.", machinename.ToLower());
                }
            }
            return null;
        }

        /// <summary>
        /// Returns location of dispatch scheme data file.
        /// </summary>
        /// <returns></returns>
        public String GetJobManagementFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_DISPATCH_SCHEME_FILE]));
        }

        /// <summary>
        /// Task notification of job being completed.
        /// </summary>
        /// <param name="job"></param>
        public override void JobCompleted(IJob job, IEnumerable<IJobResult> jobResults)
        {
            // Ensure any consumed jobs inherit the details of the completed consuming job.
            foreach (IJob consumedJob in this.Jobs.Where(j => j.ConsumedByJobID == job.ID))
            {
                consumedJob.CompletedAt = job.CompletedAt;
                consumedJob.ProcessedAt = job.ProcessedAt;
                consumedJob.ProcessingTime = job.ProcessingTime;
            }

            base.JobCompleted(job, jobResults);

            // If required set labels
            base.SetLabels(job);
        }

        /// <summary>
        /// File containing information required to set labels.
        /// </summary>
        /// <returns></returns>
        public override String GetLabelDataFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_LABEL_FILE]));
        }

        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Sets the processinghost on pending jobs for the processing host it currently would be assigned to.
        /// </summary>
        /// <param name="pendingJobs"></param>
        private void AssignProcessingHostForPendingJobs()
        {
            foreach (ScriptBuilderJob scriptbuilderJob in this.PendingJobs.Where(j => j is ScriptBuilderJob))
            {
                // Clear it - since if we can't find a match then it should not be set ( there is no fallback )
                scriptbuilderJob.ProcessingHost = String.Empty;

                foreach (DispatchGroup dispatchGroup in this.DispatchScheme.Groups)
                {
                    foreach (ScriptProjectItem solutionItem in dispatchGroup.Items.Where(i => i is ScriptProjectItem))
                    {
                        if (ScriptBuilderTask.JobMatchesScriptProjectItem(scriptbuilderJob, solutionItem))
                        {
                            if (scriptbuilderJob.ProcessingHost != dispatchGroup.MachineName)
                            {
                                Log.MessageCtx(LOG_CTX, "Pending Job {0} now expects to process on processing host {1}", scriptbuilderJob.ID, dispatchGroup.MachineName);
                                scriptbuilderJob.ProcessingHost = dispatchGroup.MachineName;
                            }
                            break;
                        }
                    }
                    if (!String.IsNullOrEmpty(scriptbuilderJob.ProcessingHost))
                    {
                        break;
                    }
                }
                if (String.IsNullOrEmpty(scriptbuilderJob.ProcessingHost))
                {
                    Log.WarningCtx(LOG_CTX, "No dispatch machine found for pending job {0}", scriptbuilderJob.ID);
                }
            }
        }

        /// <summary>
        /// Find any number of possible job binding(s) then enqueue new 'consuming' job(s) encompassing these.
        /// - the consumed jobs state changes to 'skippedconsumed'.
        /// - a consuming job cannot be 'reconsumed'.
        /// - jobs store their skipconsumed setting so that and only jobs of a similar skip consumed setting may bind together.
        /// - the skip threshold is defined as being the number of jobs that can be binded together that will trigger the binding, but more jobs can be binded than the threshold if more can be found.
        /// - Jobs are to be consumed in order of age ( oldest first )
        /// </summary>
        /// <param name="jobToConsume">optional param to consume only with this candidate job</param>
        /// <returns>the number of jobs consumed</returns>
        private int ConsumeJobs(IJob jobToConsume = null)
        {
            int numConsumed = 0;
            int numConsumers = 0;

            lock (this.m_Jobs)
            {
                // Get a list of candidate jobs for consuming 
                IEnumerable<ScriptBuilderJob> originalCandidateJobs = this.Jobs.
                    OfType<ScriptBuilderJob>().
                    Where(j => j.State == JobState.Pending).
                    Where(j => j.SkipConsume >= 0).
                    Where(j => j.ConsumedByJobID == Guid.Empty).
                    OrderBy(j => j.CreatedAt);

                IEnumerable<ScriptBuilderJob> candidateJobs = originalCandidateJobs;

                if (jobToConsume != null)
                {
                    if (!(jobToConsume is ScriptBuilderJob))
                    {
                        Log.ErrorCtx(LOG_CTX, "Cannot consume jobs of this type {0} : {1}", jobToConsume.ID, jobToConsume.GetType());
                        return 0;
                    }

                    if (!candidateJobs.Contains(jobToConsume))
                    {
                        Log.ErrorCtx(LOG_CTX, "Job {0} was asked to be consumed but this is not a candidate job", jobToConsume.ID);
                        return 0;
                    }

                    // Prune list down to this one job.
                    candidateJobs = new List<ScriptBuilderJob>() { (ScriptBuilderJob)jobToConsume };
                }

                // Here we store a list of jobs that are already consumed
                ISet<ScriptBuilderJob> allJobsConsumed = new HashSet<ScriptBuilderJob>();

                // For each job check if it can bind with another job
                int bindingsChecked = 0;
                foreach (ScriptBuilderJob bindingJob1 in candidateJobs)
                {
                    if (allJobsConsumed.Contains(bindingJob1))
                        continue; // already consumed

                    IEnumerable<ScriptBuilderJob> jobsToCheckBinding = originalCandidateJobs.Where(j => j != bindingJob1 && !allJobsConsumed.Contains(j));

                    ISet<ScriptBuilderJob> jobsBindable = new HashSet<ScriptBuilderJob>();
                    foreach (ScriptBuilderJob bindingJob2 in jobsToCheckBinding)
                    {
                        bindingsChecked++;

                        if (CanBind(bindingJob1, bindingJob2))
                        {
                            jobsBindable.Add(bindingJob1);
                            jobsBindable.Add(bindingJob2);
                            Log.DebugCtx(LOG_CTX, "Job pair can bind ({0} {1}) : {2}|{3}", bindingJob1.ID, bindingJob2.ID, bindingJob1.ScriptProjectFilename, bindingJob1.BuildConfig);
                        }
                    }

                    if (jobsBindable.Any())
                    {
                        // Ensure the threshold to skip has been passed. - all jobs have the same skip setting.
                        int skipThreshold = jobsBindable.First().SkipConsume;

                        // If the skip theshold is hit then consume all these jobs.
                        if (jobsBindable.Count() >= (int)skipThreshold)
                        {
                            Log.MessageCtx(LOG_CTX, " ");
                            Log.MessageCtx(LOG_CTX, "==================================");
                            Log.MessageCtx(LOG_CTX, "=== Consuming up to {0,5} jobs ===", jobsBindable.Count());
                            Log.MessageCtx(LOG_CTX, "==================================");

                            // joins the jobs it can together ( should be all of them ) forming a newly enqueued job
                            IEnumerable<ScriptBuilderJob> jobsConsumedByNewConsumingJob;
                            ScriptBuilderJob consumingJob = Bind(jobsBindable, out jobsConsumedByNewConsumingJob);

                            if (consumingJob != null)
                            {
                                // Check all jobs were consumed as this might indicate an issue if not.
                                if (jobsBindable.Count() != jobsConsumedByNewConsumingJob.Count())
                                {
                                    Log.WarningCtx(LOG_CTX, "Only {0} jobs were consumed out of {1} jobs.", jobsConsumedByNewConsumingJob.Count(), jobsBindable.Count());
                                }

                                // Update store of all the jobs consumed so we don't reuse already bound jobs.
                                allJobsConsumed.UnionWith(jobsConsumedByNewConsumingJob);

                                // The jobs that were consumed are now flagged as skipped/consumed
                                foreach (ScriptBuilderJob jobConsumed in jobsConsumedByNewConsumingJob)
                                {
                                    Log.MessageCtx(LOG_CTX, "=== {0} is now skipped consumed", jobConsumed.ID);
                                    jobConsumed.SetConsumed(consumingJob);
                                    numConsumed++;
                                }
                                Log.MessageCtx(LOG_CTX, "=== {0} jobs consumed by job {1}", jobsConsumedByNewConsumingJob.Count(), consumingJob.ID);
                                Log.MessageCtx(LOG_CTX, "==================================");
                                Log.MessageCtx(LOG_CTX, " ");

                                numConsumers++;
                            }
                            else
                            {
                                Log.ErrorCtx(LOG_CTX, "No consuming jobs created.");
                            }
                        }
                    }
                }
                Log.MessageCtx(LOG_CTX, "{0} job bindings checked during ScriptBuilder ConsumeJobs", bindingsChecked);
            }


            if (numConsumed > 0)
            {
                Log.MessageCtx(LOG_CTX, " ");
                Log.MessageCtx(LOG_CTX, "=== {0} Total jobs consumed into {1} new jobs ===", numConsumed, numConsumers);
                Log.MessageCtx(LOG_CTX, " ");

                // Save the task when jobs are created - otherwise they could go missing if say the automation service crashed before the job could be executed.
                Save();
            }

            return numConsumed;
        }

        /// <summary>
        /// Creates jobs from an enumerable of jobs binding the common details together into a larger job
        /// </summary>
        /// <param name="jobsToConsume">the jobs to try to consume</param>
        /// <param name="jobsConsumed">the actual jobs consumed</param>
        /// <returns>the new job created if any</returns>
        private ScriptBuilderJob Bind(IEnumerable<ScriptBuilderJob> jobsToConsume, out IEnumerable<ScriptBuilderJob> jobsConsumed)
        {
            // Collate all the details across all the jobs to build the consuming job.           
            try
            {
                // Single() by virtue of the expection it throws assures us we are not losing information.
                String scriptProjectFilename = jobsToConsume.Select(j => j.ScriptProjectFilename).Distinct().Single();
                String buildConfig = jobsToConsume.Select(j => j.BuildConfig).Distinct().Single();
                String tool = jobsToConsume.Select(j => j.Tool).Distinct().Single();
                bool rebuild = jobsToConsume.Select(j => j.Rebuild).Distinct().Single();
                bool requiresPostJobNotification = jobsToConsume.Select(j => j.RequiresPostJobNotification).Distinct().Single();
                int skipConsume = jobsToConsume.Select(j => j.SkipConsume).Distinct().Single(); // only used to throw exception if not single.

                // DW: really this should be checked to be distinct and single too, but CanBind() should already assure this.
                ScriptBuilderJob firstJob = jobsToConsume.First();
                IEnumerable<String> prebuildCommands = firstJob.PrebuildCommands;
                IEnumerable<String> postbuildCommands = firstJob.PostbuildCommands;
                IEnumerable<String> labels = firstJob.Labels;

                // DW: these priorities should be extracted into a common class, I don't see that need more than one priority class?
                RSG.Pipeline.Automation.Common.JobManagement.Priority priority = firstJob.Priority == JobPriority.Normal ? RSG.Pipeline.Automation.Common.JobManagement.Priority.Standard : RSG.Pipeline.Automation.Common.JobManagement.Priority.High;

                IEnumerable<ITrigger> triggers = jobsToConsume.Select(j => j.Trigger);
                int skipConsumeState = -3;
                //Using a default false here doesn't matter as the script project item isn't directly saved into the ScriptBuilderJob
                ScriptProjectItem ScriptProjectItem = new ScriptProjectItem(scriptProjectFilename, priority, buildConfig, tool, rebuild, prebuildCommands, postbuildCommands, labels, skipConsumeState, requiresPostJobNotification, false, null);

                IBranch branch;
                IProject project;
                if (!jobsToConsume.First().GetProjectAndBranch(this.Options, this.Log, out project, out branch))
                {
                    this.Log.ErrorCtx(LOG_CTX, "Job binding: Couldn't derive project and branch for assetbuilder job");
                    jobsConsumed = null;
                    return null;
                }

                ScriptBuilderJob consumingJob = new ScriptBuilderJob(triggers, ScriptProjectItem, branch);
                EnqueueJob(consumingJob);

                // We ensure we externally list all the jobs consumed
                jobsConsumed = jobsToConsume;

                return consumingJob;
            }
            catch (InvalidOperationException ex)
            {
                Log.ToolExceptionCtx(LOG_CTX, ex, "Distinct binding of jobs has non single properties that would be bound");
            }
            jobsConsumed = null;
            return null;
        }

        /// <summary>
        /// Two jobs are designated the same 'type of work', and from the perpective of the task are said to be 'associated'
        /// - this association is used to group bindable jobs and to associate jobs that form an incremental breakers list.
        /// </summary>
        /// <param name="job1"></param>
        /// <param name="job2"></param>
        /// <returns>true is associated</returns>
        public override bool Associated(IJob job1, IJob job2)
        {
            if (!(job1 is ScriptBuilderJob))
                return false;
            if (!(job2 is ScriptBuilderJob))
                return false;

            ScriptBuilderJob scriptBuilderJob1 = (ScriptBuilderJob)job1;
            ScriptBuilderJob scriptBuilderJob2 = (ScriptBuilderJob)job2;
            if (scriptBuilderJob1.Trigger.GetType() != scriptBuilderJob2.Trigger.GetType())
                return false;

            if (scriptBuilderJob1.Role != scriptBuilderJob2.Role)
                return false;
            if (scriptBuilderJob1.ScriptProjectFilename != scriptBuilderJob2.ScriptProjectFilename)
                return false;
            if (scriptBuilderJob1.BuildConfig != scriptBuilderJob2.BuildConfig)
                return false;
            if (scriptBuilderJob1.Rebuild != scriptBuilderJob2.Rebuild)
                return false;

            if (scriptBuilderJob1.RequiresPostJobNotification != scriptBuilderJob2.RequiresPostJobNotification)
                return false;

            // handle null prebuild commands
            if (job1.PrebuildCommands == null)
            {
                if (!(job2.PrebuildCommands == null || job2.PrebuildCommands.Any()))
                    return false;
            }
            else if (job2.PrebuildCommands == null)
            {
                if (!job1.PrebuildCommands.Any())
                    return false;
            }
            else if (!Enumerable.SequenceEqual(job1.PrebuildCommands, job2.PrebuildCommands))
            {
                return false;
            }

            // handle null postbuild commands
            if (job1.PostbuildCommands == null)
            {
                if (!(job2.PostbuildCommands == null || job2.PostbuildCommands.Any()))
                    return false;
            }
            else if (job2.PostbuildCommands == null)
            {
                if (!job1.PostbuildCommands.Any())
                    return false;
            }
            else if (!Enumerable.SequenceEqual(job1.PostbuildCommands, job2.PostbuildCommands))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Checks the details of two jobs indicate they can be binded together for consuming in a bigger job.
        /// </summary>
        /// <param name="job1"></param>
        /// <param name="job2"></param>
        /// <returns>true if bindable</returns>
        private bool CanBind(ScriptBuilderJob job1, ScriptBuilderJob job2)
        {
            if (!job1.ProjectName.Equals(job2.ProjectName, StringComparison.CurrentCultureIgnoreCase))
                return false;

            if (!job1.BranchName.Equals(job2.BranchName, StringComparison.CurrentCultureIgnoreCase))
                return false;

            if (job1.SkipConsume < 0)
                return false;
            if (job2.SkipConsume < 0)
                return false;

            // Jobs must share the same skip consume setting to bind together.
            if (job1.SkipConsume != job2.SkipConsume)
                return false;

            // We can only join CL triggers
            if (!(job1.Trigger is IChangelistTrigger))
                return false;
            if (!(job2.Trigger is IChangelistTrigger))
                return false;

            if (!Associated(job1, job2))
                return false;

            return true;
        }

        /// <summary>
        /// Process the changelist creating jobs according the internal dispatch settings.
        /// </summary>
        /// <param name="changelist"></param>
        /// <param name="changeListFileMappings"></param>
        /// <returns></returns>
        private int CreateJobsForChangelist(Changelist changelist, IEnumerable<FileMapping> changeListFileMappings)
        {
            if (!changeListFileMappings.Any())
            {
                Log.ErrorCtx(LOG_CTX, "No files in changelist : this is unexpected.");
                return 0;
            }

            Guid triggerId = Guid.NewGuid();
            int totalJobCreated = 0;

            foreach (DispatchGroup dispatchGroup in this.DispatchScheme.Groups)
            {
                Log.MessageCtx(LOG_CTX, "Considering dispatch group '{0}', which has {1} items",
                dispatchGroup.Name, dispatchGroup.Items.Count());

                Log.MessageCtx(LOG_CTX, "Creating ScriptBuilderJobs from changelist {0} with the following {1} files:",
                    changelist.Number, changeListFileMappings.Count());

                foreach (FileMapping fm in changeListFileMappings)
                    Log.MessageCtx(LOG_CTX, "\t'{0}'", fm.DepotFilename);

                ICollection<String> changeListFilenames = new List<String>();
                changeListFileMappings.ForEach(fm => changeListFilenames.Add(fm.DepotFilename));

                //
                // FOR EVERY SCRIPT PROJECT ITEM IN THE DISPATCH GROUPS CREATE A JOB
                // - client workers don't work filter on paths contained within the files.
                // - we just create a mass of jobs to be processed by the worker farm.
                //
                int numJobsCreated = 0;
                IEnumerable<IItem> items = dispatchGroup.Items.Where(dg => dg is ScriptProjectItem);
                int count = items.Count();
                Log.MessageCtx(LOG_CTX, " ");
                foreach (IItem item in items)
                {
                    ScriptProjectItem scriptProjectItem = (ScriptProjectItem)item;
                    if (scriptProjectItem.MonitoringPaths)
                    {
                        Log.MessageCtx(LOG_CTX, "Creating ScriptBuilderJob ({0}/{1}) for {2} from changelist {3} : ({4,12}) : \t{5}", numJobsCreated + 1, count, dispatchGroup.MachineName, changelist.Number, scriptProjectItem.BuildConfig, scriptProjectItem.ScriptProjectFilename);
                        ScriptBuilderJob cbj = new ScriptBuilderJob(changelist, changeListFilenames, scriptProjectItem, Options.Branch, this.MonitoredPaths, triggerId);
                        this.EnqueueJob(cbj);
                        numJobsCreated++;
                    }
                }
                totalJobCreated += numJobsCreated;
            }
            Log.MessageCtx(LOG_CTX, "Created {0} total jobs for CL {1}", NAME, changelist.Number);

            return totalJobCreated;
        }

        /// <summary>
        /// Method to clean out jobs and their files.
        /// </summary>
        private void CleanJobs()
        {
            int hoursKeepCompleted = (int)this.Parameters[PARAM_HOURS_JOB];
            int hoursKeepPending = (int)this.Parameters[PARAM_HOURS_TO_KEEP_PENDING_JOBS];

            DateTime modifiedTimeCompleted = DateTime.UtcNow - TimeSpan.FromHours((double)hoursKeepCompleted);
            DateTime modifiedTimePending = DateTime.UtcNow - TimeSpan.FromHours((double)hoursKeepPending);

            // We now just delete any job that has existed for the specified time rather than
            // specifically just completed jobs.
            lock (this.m_Jobs)
            {
                // Clean old completed jobs.
                foreach (IJob job in this.Jobs)
                {
                    DateTime completionTime = (job.ProcessedAt + job.ProcessingTime);
                    if (completionTime.IsEarlierThan(modifiedTimeCompleted))
                    {
                        CleanJob(job);
                    }
                }

                // Clean old pending jobs. ( could be safer?! ) 
                foreach (IJob job in this.Jobs)
                {                    
                    // Clean job if found pending for too long.
                    if (job.State == JobState.Pending && job.CreatedAt.IsEarlierThan(modifiedTimePending))
                    {
                        Log.WarningCtx(LOG_CTX, "A Job {0} was found to be in state pending for more than {1} hours. It has been cleaned.", job.ID, hoursKeepPending);
                        CleanJob(job);
                    }
                }

                // Clean if the consuming job is now removed.
                foreach (IJob job in this.Jobs.Where(j => j.State == JobState.SkippedConsumed))
                {
                    if (!this.Jobs.Where(j => j.ID == job.ConsumedByJobID).Any())
                    {
                        CleanJob(job);
                    }
                }

                // B*1377109 - Remove any job results for jobs that no longer exist
                SortedSet<Guid> jobIDs = new SortedSet<Guid>(this.Jobs.Select(job => job.ID));
                lock (this.m_JobResults)
                {
                    IJobResult[] resultsToRemove = m_JobResults.Where(jobResult => !jobIDs.Contains(jobResult.JobId)).ToArray();
                    foreach (IJobResult resultToRemove in resultsToRemove)
                    {
                        this.m_JobResults.Remove(resultToRemove);
                    }
                }
            }
        }

        /// <summary>
        /// Remove job from our queue and clean necessary files.
        /// </summary>
        /// <param name="job"></param>
        private void CleanJob(IJob job)
        {
            lock (this.m_Jobs)
            {
                IEnumerable<KeyValuePair<JobPriority, IJob>> jobEntries =
                    this.m_Jobs.Where(j => j.Value.ID.Equals(job.ID));
                if (jobEntries.Count() > 0)
                    this.m_Jobs.Remove(jobEntries.First());
            }
            Services.FileTransferService.CleanJobFiles(this.Config, job, this.Log);
        }

        /// <summary>
        /// Get the tasks settings filename.
        /// </summary>
        /// <returns></returns>
        private String GetSettingsFilename()
        {
            return (Path.Combine(this.Config.ToolsConfig, "automation",
                "tasks", String.Format("{0}.xml", this.GetType().Name)));
        }

        /// <summary>
        /// Load the monitoring locations settings file.
        /// </summary>
        private void LoadMonitoringLocations()
        {
            (this.MonitoredPaths as List<String>).Clear();
            String monitoringFile = this.Config.Environment.Subst((String)this.Parameters[PARAM_MONITOR_PATH_FILE]);
            if (!File.Exists(monitoringFile) && this.MonitoredPaths.Count() == 0)
            {
                this.Log.ErrorCtx(LOG_CTX, "Monitoring file does not exist ({0}).  Script Builder task will not be monitoring any locations.", monitoringFile);
            }

            // Cache monitoring path last modified time
            DateTime monitoringFileModified = System.IO.File.GetLastWriteTime(monitoringFile);
            bool showMonitoringPaths = this.MonitoringFileLastModified != monitoringFileModified;
            if (showMonitoringPaths && this.MonitoringFileLastModified != DateTime.MinValue)
            {
                this.Log.MessageCtx(LOG_CTX, " ");
                this.Log.MessageCtx(LOG_CTX, "*** Monitoring paths may have been modified : {0} ***", monitoringFile);
                this.Log.MessageCtx(LOG_CTX, " ");
            }

            this.MonitoringFileLastModified = monitoringFileModified;

            XDocument xdoc = XDocument.Load(monitoringFile);

            // Read all locations
            ICollection<String> locationsDeserialised = new List<String>();

            foreach (XElement elem in xdoc.Descendants("location"))
            {
#warning TODO LPXO: Add some mechanism for removing paths, similar to perforce workspace capabilities
                String path = elem.Attribute("path").Value;
                if (showMonitoringPaths)
                {
                    this.Log.MessageCtx(LOG_CTX, "Monitoring Path: {0}", path);
                }
                locationsDeserialised.Add(path);
            }

            // For each deserialised location translate to appropriate location(s)
            ISet<String> coreLocations = new HashSet<String>();
            foreach (String location in locationsDeserialised)
            {
                String branchName = this.Options.Branch.Name;
                Debug.Assert(this.Options.CoreProject.Branches.ContainsKey(branchName),
                    "Core project does not have branch '{0}' defined.", branchName);
                if (!this.Options.CoreProject.Branches.ContainsKey(branchName))
                    throw (new ArgumentException(String.Format("Core project does not have branch '{0}' defined.", branchName)));

                string rootProjectLocation = this.Options.CoreProject.Branches[branchName].Environment.Subst(location);
                rootProjectLocation = Environment.ExpandEnvironmentVariables(rootProjectLocation);
                if (showMonitoringPaths)
                {
                    this.Log.MessageCtx(LOG_CTX, "Core project evaluated monitoring path: {0}", rootProjectLocation);
                }
                coreLocations.Add(rootProjectLocation);
            }

            if (coreLocations.Any())
            {
                FileMapping[] fms = FileMapping.Create(this.P4, coreLocations.ToArray());
                (this.MonitoredPaths as List<String>).AddRange(fms.Where(fm => fm.Mapped).Select(fm => fm.DepotFilename.Replace("/...", String.Empty)));

                if (showMonitoringPaths)
                {
                    this.Log.MessageCtx(LOG_CTX, " ");
                    this.Log.MessageCtx(LOG_CTX, "{0} MONITORING PATHS", this.MonitoredPaths.Count());
                    foreach (String depotPath in this.MonitoredPaths)
                    {
                        this.Log.MessageCtx(LOG_CTX, "\tMonitoring depot path: {0}", depotPath);
                    }
                    this.Log.MessageCtx(LOG_CTX, " ");
                }
            }
            else
            {
                this.Log.ErrorCtx(LOG_CTX, "There are no monitoring locations, please examine your monitoring file {0}", monitoringFile);
            }
        }

        #endregion // Private Methods
        #region Static Public Methods
        /// <summary>
        /// Does a ScriptBuilder matches a ScriptProjectItem
        /// DW - Review this method, is was something Travis extrapolated from the original codebuilder, I'm not sure if I'm keen on it.
        /// </summary>
        /// <param name="cbj"></param>
        /// <param name="si"></param>
        static public bool JobMatchesScriptProjectItem(ScriptBuilderJob scriptBuilderJob, ScriptProjectItem solutionProjectItem)
        {
            if (!(0 == String.Compare(solutionProjectItem.BuildConfig, scriptBuilderJob.BuildConfig, true) &&
                0 == String.Compare(solutionProjectItem.ScriptProjectFilename, scriptBuilderJob.ScriptProjectFilename, true) &&
                solutionProjectItem.Rebuild == scriptBuilderJob.Rebuild
                ))
            {
                return false;
            }
            return true;
        }
        #endregion
    }

} // RSG.Pipeline.Automation.ServerHost.Tasks namespace
