using System;
using System.Diagnostics;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.Common.JobManagement;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Services;
using RSG.Pipeline.Automation.Common.Batch;
using RSG.Pipeline.Automation.Common.Services.Messages;
using RSG.Pipeline.Automation.Common.JobFactories;

namespace RSG.Pipeline.Automation.ServerHost.Tasks
{
    /// <summary>
    /// Code Builder server-side monitoring task.
    /// </summary>
    internal class CodeBuilder2Task :
        MonitoringTaskBase,
        IMonitoringTask,
        IJobManagedTask
    {
        #region Constants
        /// <summary>
        /// The task name.
        /// </summary>
        private static readonly String NAME = "Code Builder 2";

        /// <summary>
        /// The task description.
        /// </summary>
        private static readonly String DESC = "Code Builder 2 monitoring task.";

        /// <summary>
        /// The task log context.
        /// </summary>
        private static readonly new String LOG_CTX = "Automation:CodeBuilder2Task";

        /// <summary>
        /// Parameter for the file listing all the paths to monitor.
        /// </summary>
        private static readonly string PARAM_MONITOR_PATH_FILE = "Path Monitoring File";

        /// <summary>
        /// Parameter for the file listing email addresses to send notification data to.
        /// </summary>
        private static readonly string PARAM_EMAIL_NOTIFICATIONS_FILE = "Notifications File";

        /// <summary>
        /// Parameter for the file listing labels to set.
        /// </summary>
        private static readonly string PARAM_LABEL_FILE = "Label File";        

        /// <summary>
        /// Parameter for the file listing job management data.
        /// - the dispatch file also defines the builds to build.
        /// </summary>
        private static readonly string PARAM_DISPATCH_SCHEME_FILE = "Dispatch Scheme File";

        /// <summary>
        /// Parameter for number of hours between job cleans.
        /// </summary>
        private static readonly String PARAM_HOURS_JOB = "Hours to Keep Completed Jobs";

        /// <summary>
        /// Parameter for number of hours between job cleans of pending jobs
        /// - these can hang around when solutions to build are removed.
        /// </summary>
        private static readonly String PARAM_HOURS_TO_KEEP_PENDING_JOBS = "Hours to Keep Pending Jobs";

        #endregion // Constants

        #region Member Data
        
        /// <summary>
        /// The Dispatch scheme.
        /// - it stores the codebuilder configurations to build and where to dispatch them.
        /// </summary>
        public DispatchScheme DispatchScheme { get; set; }

        /// <summary>
        /// Stored to know when this file is modified
        /// </summary>
        private DateTime MonitoringFileLastModified { get; set; }

        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="location"></param>
        public CodeBuilder2Task(CommandOptions options)
            : this(new String[] { String.Empty }, options)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="locations"></param>
        public CodeBuilder2Task(IEnumerable<String> locations, CommandOptions options)
            : base(NAME, DESC, CapabilityType.CodeBuilder2, locations, options)
        {
            String filename = GetSettingsFilename();

            IDictionary<String, Object> parameters = new Dictionary<String, Object>();
            RSG.Base.Xml.Parameters.Load(filename, ref parameters);
            this.Parameters = parameters;

            this.MonitoringFileLastModified = DateTime.MinValue;
            this.LoadMonitoringLocations();

            this.DispatchScheme = new DispatchScheme(this.Config, options, this.Log, this.GetJobManagementFile());
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// Update.
        /// </summary>
        public override void Update()
        {
            // Only do job maintainance; cleaning the queue and associated files.
            this.CleanJobs();

            // Reload all parameters
            String filename = GetSettingsFilename();
            IDictionary<String, Object> parameters = new Dictionary<String, Object>();
            RSG.Base.Xml.Parameters.Load(filename, ref parameters);
            this.Parameters = parameters;

            // Allows us to update monitoring locations without restarting the service.
            this.LoadMonitoringLocations();

            // Allows us to update label data without restarting the service.
            this.LoadLabelData();

            // Allow us to update job dispatch data without restarting the service.
            lock (this.DispatchScheme)
            {
                bool syncDispatch = this.Parameters.ContainsKey(PARAM_SYNC_DISPATCH) && (bool)this.Parameters[PARAM_SYNC_DISPATCH];
                this.DispatchScheme.LoadDispatchData(this.GetJobManagementFile(), this.Options, syncDispatch);
                AssignProcessingHostForPendingJobs();
            }

            // Get list of changelists from base class; turn these into Jobs.
            IEnumerable<Changelist> changelists = base.GetSubmittedChangelists();
            if (changelists.Count() > 0)
                this.MaximumChangelist = changelists.Select(changelist => changelist.Number).Max();

            int jobsCreated = 0;

            lock (this.m_Jobs) // must lock jobs otherwise one might be consumed before the whole list of jobs is known, when that happens job consumption is not to the head
            {
                foreach (Changelist changelist in changelists)
                {
                    Log.MessageCtx(LOG_CTX, "Considering changelist #{0} submitted by '{1}' at {2}, which contains {3} files",
                        changelist.Number, changelist.Username, changelist.Time.ToString(), changelist.Files.Count());

                    if (changelist.Files.Count() == 0)
                    {
                        Log.WarningCtx(LOG_CTX, "Empty changelist {0}: {1}.  Ignoring.", changelist.Number, changelist.Description);
                        continue;
                    }

                    // Ensure we only process files within a changelist that should be monitored.
                    List<String> monitoredFiles = new List<String>();
                    foreach (String monitoredPath in this.MonitoredPaths)
                        monitoredFiles.AddRange(changelist.Files.Where(f => f.ToLower().StartsWith(monitoredPath.ToLower())));

                    IEnumerable<FileMapping> changeListFiles = FileMapping.Create(this.P4, monitoredFiles.ToArray());           
           
                    // Create jobs for this changelist                    
                    IEnumerable<IJob> jobs = CodeBuilder2JobFactory.CreateJobsForChangelist(changelist, changeListFiles, DispatchScheme, MonitoredPaths, this.Options.Branch);
                    foreach (IJob job in jobs)
                    {
                        this.EnqueueJob(job);
                        jobsCreated++;
                    }                    
                }

                if (jobsCreated > 0)
                {
                    Log.MessageCtx(LOG_CTX, " ");
                    Log.MessageCtx(LOG_CTX, "**********************");
                    Log.MessageCtx(LOG_CTX, "* {0,5} Jobs Created *", jobsCreated);
                    Log.MessageCtx(LOG_CTX, "**********************");
                    Log.MessageCtx(LOG_CTX, " ");
                    // Save the task when jobs are created - otherwise they could go missing if say the automation service crashed before the job could be executed.
                    Save();
                }
            }
        }

        /// <summary>
        /// Override the post job notification base method
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public override bool PostJobNotificationRequired(IJob job)
        {
            CodeBuilder2Job codebuilderJob = (CodeBuilder2Job)job;
            return codebuilderJob.RequiresPostJobNotification;
        }

        /// <summary>
        /// File containing information required to send notifications.
        /// </summary>
        /// <returns></returns>
        public override String GetNotificationDataFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_EMAIL_NOTIFICATIONS_FILE]));
        }

        /// <summary>
        /// File containing information required to set labels.
        /// </summary>
        /// <returns></returns>
        public override String GetLabelDataFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_LABEL_FILE]));
        }

        /// <summary>
        /// Client requests for a suitable job
        /// - check the dispatch groups and return the most appropriate job.
        /// </summary>
        /// <param name="machinename"></param>
        /// <returns>a codebuilder2job</returns>
        public override IJob GetSuitableJob(string machinename)
        {
            Log.MessageCtx(LOG_CTX, "Client '{0}' is requesting a suitable job.", machinename.ToLower());

            if (!base.CanGetSuitableJob(machinename))
                return null;

            lock (this.DispatchScheme)
            {
                // Find any suitable dispatch groups for this machine
                DispatchGroup[] suitableDispatchGroups = this.DispatchScheme.Groups.Where(jg => jg.MachineName == machinename.ToLower()).ToArray();

                if (suitableDispatchGroups.Length > 0)
                {
                    Log.MessageCtx(LOG_CTX, "Client '{0}' has suitable dispatch groups registered.", machinename.ToLower());

                    lock (this.m_Jobs)
                    {
                        foreach (CodeBuilder2Job codebuilder2Job in this.PendingJobs.Where(j => j is CodeBuilder2Job))
                        {
                            foreach (DispatchGroup suitableDispatchGroup in suitableDispatchGroups)
                            {
                                foreach (SolutionItem solutionItem in suitableDispatchGroup.Items.Where(i => i is SolutionItem))
                                {
                                    // If the job matches the settings of the solutionitem in the dispatch group 
                                    // then we can give this client worker this job
                                    if (CodeBuilder2Task.JobMatchesSolutionItem(codebuilder2Job, solutionItem))
                                    {
                                        Log.MessageCtx(LOG_CTX, "Found job '{0}' for client '{1}'.", codebuilder2Job.ID.ToString(), machinename.ToLower());

                                        // Instead of assigning this job can we combine some jobs with this? ( skip to head ) then return that job.
                                        if (codebuilder2Job.SkipConsume >= 0)
                                        {
                                            // Consume any jobs that can bind to this job.
                                            int numConsumed = ConsumeJobs(codebuilder2Job);
                                            if (numConsumed > 0)
                                            {
                                                // Find the consuming job
                                                IJob consumingJob = this.PendingJobs.Single(j => j.ID == codebuilder2Job.ConsumedByJobID);
                                                Log.MessageCtx(LOG_CTX, "Instead of giving out job {0} we have combined {1} jobs and will give {2} to {3}", codebuilder2Job.ID, numConsumed, consumingJob.ID, machinename.ToLower());
                                                return consumingJob;
                                            }
                                        }
                                        return codebuilder2Job;
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    Log.ErrorCtx(LOG_CTX, "Client '{0}' doesn't have any dispatch groups registered, codebuilder does not support a 'fallback' - setup the dispatch groups.", machinename.ToLower());
                }
            }
            return null;
        }

        /// <summary>
        /// Returns location of dispatch scheme data file.
        /// </summary>
        /// <returns></returns>
        public String GetJobManagementFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_DISPATCH_SCHEME_FILE]));
        }

        /// <summary>
        /// Task notification of job being completed.
        /// </summary>
        /// <param name="job"></param>
        public override void JobCompleted(IJob job, IEnumerable<IJobResult> jobResults)
        {
            // Ensure any consumed jobs inherit the details of the completed consuming job.
            foreach (IJob consumedJob in this.Jobs.Where(j => j.ConsumedByJobID == job.ID))
            {
                consumedJob.CompletedAt = job.CompletedAt;
                consumedJob.ProcessedAt = job.ProcessedAt;
                consumedJob.ProcessingTime = job.ProcessingTime;
            }

            base.JobCompleted(job, jobResults);

            // If required set labels
            base.SetLabels(job);
        }

        #endregion // Controller Methods

        #region Private Methods

        /// <summary>
        /// Sets the processinghost on pending jobs for the processing host it currently would be assigned to.
        /// </summary>
        /// <param name="pendingJobs"></param>
        private void AssignProcessingHostForPendingJobs()
        {
            foreach (CodeBuilder2Job codebuilder2Job in this.PendingJobs.Where(j => j is CodeBuilder2Job))
            {
                // Clear it - since if we can't find a match then it should not be set ( there is no fallback )
                codebuilder2Job.ProcessingHost = String.Empty;

                foreach (DispatchGroup dispatchGroup in this.DispatchScheme.Groups)
                {
                    foreach (SolutionItem solutionItem in dispatchGroup.Items.Where(i => i is SolutionItem))
                    {
                        // If the job matches the settings of the solutionitem in the dispatch group 
                        // then we can give this client worker this job
                        if (CodeBuilder2Task.JobMatchesSolutionItem(codebuilder2Job, solutionItem))
                        {
                            if (codebuilder2Job.ProcessingHost != dispatchGroup.MachineName)
                            {
                                Log.MessageCtx(LOG_CTX, "Pending Job {0} now expects to process on processing host {1}", codebuilder2Job.ID, dispatchGroup.MachineName);
                                codebuilder2Job.ProcessingHost = dispatchGroup.MachineName;
                            }
                            break;
                        }
                    }
                    if (!String.IsNullOrEmpty(codebuilder2Job.ProcessingHost))
                    {                        
                        break;
                    }
                }
                if (String.IsNullOrEmpty(codebuilder2Job.ProcessingHost))
                {
                    Log.WarningCtx(LOG_CTX, "No dispatch machine found for pending job {0}", codebuilder2Job.ID);
                }
            }
        }

        /// <summary>
        /// Find any number of possible job binding(s) then enqueue new 'consuming' job(s) encompassing these.
        /// - the consumed jobs state changes to 'skippedconsumed'.
        /// - a consuming job cannot be 'reconsumed'.
        /// - jobs store their skipconsumed setting so that and only jobs of a similar skip consumed setting may bind together.
        /// - the skip threshold is defined as being the number of jobs that can be binded together that will trigger the binding, but more jobs can be binded than the threshold if more can be found.
        /// - Jobs are to be consumed in order of age ( oldest first )
        /// </summary>
        /// <param name="jobToConsume">optional param to consume only with this candidate job</param>
        /// <returns>the number of jobs consumed</returns>
        private int ConsumeJobs(IJob jobToConsume = null)
        {
            int numConsumed = 0;
            int numConsumers = 0;

            lock (this.m_Jobs)
            {
                // Get a list of candidate jobs for consuming 
                IEnumerable<CodeBuilder2Job> originalCandidateJobs = this.Jobs.
                    OfType<CodeBuilder2Job>().
                    Where(j => j.State == JobState.Pending).
                    Where(j => j.SkipConsume >= 0).
                    Where(j => j.ConsumedByJobID == Guid.Empty).
                    OrderBy(j => j.CreatedAt);

                IEnumerable<CodeBuilder2Job> candidateJobs = originalCandidateJobs;

                if (jobToConsume != null)
                {
                    if (!(jobToConsume is CodeBuilder2Job))
                    {
                        Log.ErrorCtx(LOG_CTX, "Cannot consume jobs of this type {0} : {1}", jobToConsume.ID, jobToConsume.GetType());
                        return 0;
                    }

                    if (!candidateJobs.Contains(jobToConsume))
                    {
                        Log.ErrorCtx(LOG_CTX, "Job {0} was asked to be consumed but this is not a candidate job", jobToConsume.ID);
                        return 0;
                    }

                    // Prune list down to this one job.
                    candidateJobs = new List<CodeBuilder2Job>() { (CodeBuilder2Job)jobToConsume };
                }

                // Here we store a list of jobs that are already consumed
                ISet<CodeBuilder2Job> allJobsConsumed = new HashSet<CodeBuilder2Job>();

                // For each job check if it can bind with another job
                int bindingsChecked = 0;
                foreach (CodeBuilder2Job bindingJob1 in candidateJobs)
                {
                    if (allJobsConsumed.Contains(bindingJob1))
                        continue; // already consumed

                    IEnumerable<CodeBuilder2Job> jobsToCheckBinding = originalCandidateJobs.Where(j => j != bindingJob1 && !allJobsConsumed.Contains(j));

                    ISet<CodeBuilder2Job> jobsBindable = new HashSet<CodeBuilder2Job>();
                    foreach (CodeBuilder2Job bindingJob2 in jobsToCheckBinding)
                    {
                        bindingsChecked++;

                        if (CanBind(bindingJob1, bindingJob2))
                        {
                            jobsBindable.Add(bindingJob1);
                            jobsBindable.Add(bindingJob2);
                            Log.DebugCtx(LOG_CTX, "Job pair can bind ({0} {1}) : {2}|{3}{4}", bindingJob1.ID, bindingJob2.ID, bindingJob1.SolutionFilename, bindingJob1.Platform, bindingJob1.BuildConfig);
                        }
                    }

                    if (jobsBindable.Any())
                    {
                        // Ensure the threshold to skip has been passed. - all jobs have the same skip setting.
                        int skipThreshold = jobsBindable.First().SkipConsume;

                        // If the skip theshold is hit then consume all these jobs.
                        if (jobsBindable.Count() >= (int)skipThreshold)
                        {
                            Log.MessageCtx(LOG_CTX, " ");
                            Log.MessageCtx(LOG_CTX, "==================================");
                            Log.MessageCtx(LOG_CTX, "=== Consuming up to {0,5} jobs ===", jobsBindable.Count());
                            Log.MessageCtx(LOG_CTX, "==================================");

                            // joins the jobs it can together ( should be all of them ) forming a newly enqueued job
                            IEnumerable<CodeBuilder2Job> jobsConsumedByNewConsumingJob;
                            CodeBuilder2Job consumingJob = Bind(jobsBindable, out jobsConsumedByNewConsumingJob);

                            if (consumingJob != null)
                            {
                                // Check all jobs were consumed as this might indicate an issue if not.
                                if (jobsBindable.Count() != jobsConsumedByNewConsumingJob.Count())
                                {
                                    Log.WarningCtx(LOG_CTX, "Only {0} jobs were consumed out of {1} jobs.", jobsConsumedByNewConsumingJob.Count(), jobsBindable.Count());
                                }

                                // Update store of all the jobs consumed so we don't reuse already bound jobs.
                                allJobsConsumed.UnionWith(jobsConsumedByNewConsumingJob);

                                // The jobs that were consumed are now flagged as skipped/consumed
                                foreach (CodeBuilder2Job jobConsumed in jobsConsumedByNewConsumingJob)
                                {
                                    Log.MessageCtx(LOG_CTX, "=== {0} is now skipped consumed", jobConsumed.ID);
                                    jobConsumed.SetConsumed(consumingJob);
                                    numConsumed++;
                                }
                                Log.MessageCtx(LOG_CTX, "=== {0} jobs consumed by job {1}", jobsConsumedByNewConsumingJob.Count(), consumingJob.ID);
                                Log.MessageCtx(LOG_CTX, "==================================");
                                Log.MessageCtx(LOG_CTX, " ");

                                numConsumers++;
                            }
                            else
                            {
                                Log.ErrorCtx(LOG_CTX, "No consuming jobs created.");
                            }
                        }
                    }
                }
                Log.MessageCtx(LOG_CTX, "{0} job bindings checked during Codebuilder2 ConsumeJobs", bindingsChecked);
            }


            if (numConsumed > 0)
            {
                Log.MessageCtx(LOG_CTX, " ");
                Log.MessageCtx(LOG_CTX, "=== {0} Total jobs consumed into {1} new jobs ===", numConsumed, numConsumers);
                Log.MessageCtx(LOG_CTX, " ");

                // Save the task when jobs are created - otherwise they could go missing if say the automation service crashed before the job could be executed.
                Save();
            }

            return numConsumed;
        }

        /// <summary>
        /// Creates jobs from an enumerable of jobs binding the common details together into a larger job
        /// </summary>
        /// <param name="jobsToConsume">the jobs to try to consume</param>
        /// <param name="jobsConsumed">the actual jobs consumed</param>
        /// <returns>the new job created if any</returns>
        private CodeBuilder2Job Bind(IEnumerable<CodeBuilder2Job> jobsToConsume, out IEnumerable<CodeBuilder2Job> jobsConsumed)
        {
            CodeBuilder2Job consumingJob = CodeBuilder2JobFactory.CreateConsumingJob(this.Options, jobsToConsume, out jobsConsumed);

            if (consumingJob != null)
            {
                EnqueueJob(consumingJob);                
            }

            return consumingJob;
        }

        /// <summary>
        /// Two jobs are designated the same 'type of work', and from the perpective of the task are said to be 'associated'
        /// - this association is used to group bindable jobs and to associate jobs that form an incremental breakers list.
        /// </summary>
        /// <param name="job1"></param>
        /// <param name="job2"></param>
        /// <returns>true is associated</returns>
        public override bool Associated(IJob job1, IJob job2)
        {
            if (!(job1 is CodeBuilder2Job))
                return false;
            if (!(job2 is CodeBuilder2Job))
                return false;

            CodeBuilder2Job codebuilderJob1 = (CodeBuilder2Job)job1;
            CodeBuilder2Job codebuilderJob2 = (CodeBuilder2Job)job2;

            if (codebuilderJob1.Role != codebuilderJob2.Role)
                return false;
            if (codebuilderJob1.SolutionFilename != codebuilderJob2.SolutionFilename)
                return false;
            if (codebuilderJob1.BuildConfig != codebuilderJob2.BuildConfig)
                return false;
            if (codebuilderJob1.Platform != codebuilderJob2.Platform)
                return false;

            if (codebuilderJob1.Tool != codebuilderJob2.Tool)
                return false;
            if (codebuilderJob1.Rebuild != codebuilderJob2.Rebuild)
                return false;

            if (codebuilderJob1.TargetDir != codebuilderJob2.TargetDir)
                return false;
            if (codebuilderJob1.PublishDir != codebuilderJob2.PublishDir)
                return false;

            if (codebuilderJob1.RequiresPostJobNotification != codebuilderJob2.RequiresPostJobNotification)
                return false;

            // handle null prebuild commands
            if (job1.PrebuildCommands == null)
            {
                if (!(job2.PrebuildCommands == null || job2.PrebuildCommands.Any()))
                    return false;
            }
            else if (job2.PrebuildCommands == null)
            {
                if (!job1.PrebuildCommands.Any())
                    return false;
            }
            else if (!Enumerable.SequenceEqual(job1.PrebuildCommands, job2.PrebuildCommands))
            {
                return false;
            }

            // handle null postbuild commands
            if (job1.PostbuildCommands == null)
            {
                if (!(job2.PostbuildCommands == null || job2.PostbuildCommands.Any()))
                    return false;
            }
            else if (job2.PostbuildCommands == null)
            {
                if (!job1.PostbuildCommands.Any())
                    return false;
            }
            else if (!Enumerable.SequenceEqual(job1.PostbuildCommands, job2.PostbuildCommands))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Checks the details of two jobs indicate they can be binded together for consuming in a bigger job.
        /// </summary>
        /// <param name="job1"></param>
        /// <param name="job2"></param>
        /// <returns>true if bindable</returns>
        private bool CanBind(CodeBuilder2Job job1, CodeBuilder2Job job2)
        {
            if (!job1.ProjectName.Equals(job2.ProjectName, StringComparison.CurrentCultureIgnoreCase))
                return false;

            if (!job1.BranchName.Equals(job2.BranchName, StringComparison.CurrentCultureIgnoreCase))
                return false;

            if (job1.SkipConsume < 0)
                return false;
            if (job2.SkipConsume < 0)
                return false;

            // Jobs must share the same skip consume setting to bind together.
            if (job1.SkipConsume != job2.SkipConsume)
                return false;

            // We can only join CL triggers
            if (!(job1.Trigger is IChangelistTrigger))
                return false;
            if (!(job2.Trigger is IChangelistTrigger))
                return false;

            if (!Associated(job1, job2)) 
                return false;

            return true;
        }

        /// <summary>
        /// Method to clean out jobs and their files.
        /// </summary>
        private void CleanJobs()
        {
            int hoursKeepCompleted = (int)this.Parameters[PARAM_HOURS_JOB];
            int hoursKeepPending = (int)this.Parameters[PARAM_HOURS_TO_KEEP_PENDING_JOBS];

            DateTime modifiedTimeCompleted = DateTime.UtcNow - TimeSpan.FromHours((double)hoursKeepCompleted);
            DateTime modifiedTimePending = DateTime.UtcNow - TimeSpan.FromHours((double)hoursKeepPending);

            // We now just delete any job that has existed for the specified time rather than
            // specifically just completed jobs.
            lock (this.m_Jobs)
            {
                // Clean old completed jobs.
                foreach (IJob job in this.Jobs)
                {
                    DateTime completionTime = (job.ProcessedAt + job.ProcessingTime);
                    if (completionTime.IsEarlierThan(modifiedTimeCompleted))
                    {
                        CleanJob(job);
                    }
                }

                // Clean old pending jobs. ( could be safer?! ) 
                foreach (IJob job in this.Jobs)
                {
                    // Clean job if found pending for too long.
                    if (job.State == JobState.Pending && job.CreatedAt.IsEarlierThan(modifiedTimePending))
                    {
                        Log.WarningCtx(LOG_CTX, "A Job {0} was found to be in state pending for more than {1} hours. It has been cleaned.", job.ID, hoursKeepPending);
                        CleanJob(job);
                    }
                }

                // Clean if the consuming job is now removed.
                foreach (IJob job in this.Jobs.Where(j => j.State == JobState.SkippedConsumed))
                {
                    if (!this.Jobs.Where(j => j.ID == job.ConsumedByJobID).Any())
                    {
                        CleanJob(job);
                    }
                }

                // B*1377109 - Remove any job results for jobs that no longer exist
                SortedSet<Guid> jobIDs = new SortedSet<Guid>(this.Jobs.Select(job => job.ID));
                lock (this.m_JobResults)
                {
                    IJobResult[] resultsToRemove = m_JobResults.Where(jobResult => !jobIDs.Contains(jobResult.JobId)).ToArray();
                    foreach (IJobResult resultToRemove in resultsToRemove)
                    {
                        this.m_JobResults.Remove(resultToRemove);
                    }
                }
            }
        }

        /// <summary>
        /// Remove job from our queue and clean necessary files.
        /// </summary>
        /// <param name="job"></param>
        private void CleanJob(IJob job)
        {
            lock (this.m_Jobs)
            {
                IEnumerable<KeyValuePair<JobPriority, IJob>> jobEntries =
                    this.m_Jobs.Where(j => j.Value.ID.Equals(job.ID));
                if (jobEntries.Count() > 0)
                    this.m_Jobs.Remove(jobEntries.First());
            }
            Services.FileTransferService.CleanJobFiles(this.Config, job, this.Log);
        }

        /// <summary>
        /// Get the tasks settings filename.
        /// </summary>
        /// <returns></returns>
        private String GetSettingsFilename()
        {
            return (Path.Combine(this.Config.ToolsConfig, "automation",
                "tasks", String.Format("{0}.xml", this.GetType().Name)));
        }

        /// <summary>
        /// Load the monitoring locations settings file.
        /// </summary>
        private void LoadMonitoringLocations()
        {
            (this.MonitoredPaths as List<String>).Clear();
            String monitoringFile = this.Config.Environment.Subst((String)this.Parameters[PARAM_MONITOR_PATH_FILE]);
            if (!File.Exists(monitoringFile) && this.MonitoredPaths.Count() == 0)
            {
                this.Log.ErrorCtx(LOG_CTX, "Monitoring file does not exist ({0}).  Asset builder task will not be monitoring any locations.", monitoringFile);
            }

            // Cache monitoring path last modified time
            DateTime monitoringFileModified = System.IO.File.GetLastWriteTime(monitoringFile);
            bool showMonitoringPaths = this.MonitoringFileLastModified != monitoringFileModified;
            if (showMonitoringPaths && this.MonitoringFileLastModified != DateTime.MinValue)
            {
                this.Log.MessageCtx(LOG_CTX, " ");
                this.Log.MessageCtx(LOG_CTX, "*** Monitoring paths may have been modified : {0} ***", monitoringFile);
                this.Log.MessageCtx(LOG_CTX, " ");
            }

            this.MonitoringFileLastModified = monitoringFileModified;

            XDocument xdoc = XDocument.Load(monitoringFile);

            // Read all locations
            ICollection<String> locationsDeserialised = new List<String>();

            foreach (XElement elem in xdoc.Descendants("location"))
            {
                String path = elem.Attribute("path").Value;
                if (showMonitoringPaths)
                {
                    this.Log.MessageCtx(LOG_CTX, "Monitoring Path: {0}", path);
                }
                locationsDeserialised.Add(path);
            }

            // For each deserialised location translate to appropriate location(s)
            ISet<String> coreLocations = new HashSet<String>();
            foreach (String location in locationsDeserialised)
            {
                String branchName = this.Options.Branch.Name;
                Debug.Assert(this.Options.CoreProject.Branches.ContainsKey(branchName),
                    "Core project does not have branch '{0}' defined.", branchName);
                if (!this.Options.CoreProject.Branches.ContainsKey(branchName))
                    throw (new ArgumentException(String.Format("Core project does not have branch '{0}' defined.", branchName)));

                string rootProjectLocation = this.Options.CoreProject.Branches[branchName].Environment.Subst(location);
                rootProjectLocation = Environment.ExpandEnvironmentVariables(rootProjectLocation);
                if (showMonitoringPaths)
                {
                    this.Log.MessageCtx(LOG_CTX, "Core project evaluated monitoring path: {0}", rootProjectLocation);
                }
                coreLocations.Add(rootProjectLocation);
            }

            if (coreLocations.Any())
            {
                FileMapping[] fms = FileMapping.Create(this.P4, coreLocations.ToArray());
                (this.MonitoredPaths as List<String>).AddRange(fms.Where(fm => fm.Mapped).Select(fm => fm.DepotFilename.Replace("/...",String.Empty)));
                
                if (showMonitoringPaths)
                {
                    this.Log.MessageCtx(LOG_CTX, " ");
                    this.Log.MessageCtx(LOG_CTX, "{0} MONITORING PATHS", this.MonitoredPaths.Count());
                    foreach (String depotPath in this.MonitoredPaths)
                    {
                        this.Log.MessageCtx(LOG_CTX, "\tMonitoring depot path: {0}", depotPath);
                    }
                    this.Log.MessageCtx(LOG_CTX, " ");
                }
            }
            else
            {
                this.Log.ErrorCtx(LOG_CTX, "There are no monitoring locations, please examine your monitoring file {0}", monitoringFile);
            }
        }

        #endregion // Private Methods
        #region Static Public Methods
        /// <summary>
        /// Telling if a CodeBuilder matches a SolutionItem
        /// Note: Cannot compare Prebuild/Postbuild commands as some of the commands are spliced from
        /// the SolutionItem version to the CodeBuilder command due to situations similar to the 
        /// Sync_Code_Label.bat arguments. 
        /// </summary>
        /// <param name="cbj"></param>
        /// <param name="si"></param>
        static public bool JobMatchesSolutionItem(CodeBuilder2Job cbj, SolutionItem si)
        {
            if (!(0 == String.Compare(si.BuildConfig, cbj.BuildConfig, true) &&
                0 == String.Compare(si.Platform, cbj.Platform, true) &&
                0 == String.Compare(si.SolutionFilename, cbj.SolutionFilename, true) &&
                0 == String.Compare(si.Tool, cbj.Tool, true) &&
                si.Rebuild == cbj.Rebuild &&
                0 == String.Compare(si.TargetDir, cbj.TargetDir) &&
                0 == String.Compare(si.PublishDir, cbj.PublishDir)
                ))
            {
                return false;
            }
            return true;
        }
        #endregion
    }

} // RSG.Pipeline.Automation.ServerHost.Tasks namespace
