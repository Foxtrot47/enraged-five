﻿using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.JobManagement;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Jobs.JobData;
using RSG.Pipeline.Automation.Common.Notifications;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Automation.ServerHost.Tasks.CommandRunner;
using RSG.SourceControl.Perforce;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace RSG.Pipeline.Automation.ServerHost.Tasks
{
    public class CommandRunnerTask :
        MonitoringTaskBase,
        IMonitoringTask,
        IJobManagedTask
    {
        #region Constants
        /// <summary>
        /// The task name.
        /// </summary>
        private static readonly String NAME = "Command Runner";

        /// <summary>
        /// The task description.
        /// </summary>
        private static readonly String DESC = "Command Runner monitoring task.";

        /// <summary>
        /// The task log context.
        /// </summary>
        private static readonly String LOG_CTX = "Automation:CommandRunnerTask";

        /// <summary>
        /// Used to specify the folder which the command runner xmls reside in.
        /// </summary>
        private static readonly String PARAM_COMMANDS_FOLDER = "Command XML Location";
        /// <summary>
        /// Parameter for number of hours between job cleans.
        /// </summary>
        private static readonly String PARAM_HOURS_JOB = "Hours to Keep Completed Jobs";

        /// <summary>
        /// Parameter for number of hours between job cleans of pending jobs
        /// </summary>
        private static readonly String PARAM_HOURS_TO_KEEP_PENDING_JOBS = "Hours to Keep Pending Jobs";

        /// <summary>
        /// Parameter for the file listing job management data.
        /// </summary>
        private static readonly string PARAM_DISPATCH_SCHEME_FILE = "Dispatch Scheme File";

        /// <summary>
        /// Parameter for toggling sync to command definitions.
        /// </summary>
        private static readonly string PARAM_SYNC_SUBTASKS = "Sync Commands";

        /// <summary>
        /// Used for suffix
        /// </summary>
        private static readonly string P4_PATH_END = "\\...";
        #endregion // Constants


        #region Member Data

        /// <summary>
        /// The Dispatch scheme.
        /// </summary>
        public DispatchScheme DispatchScheme { get; private set; }

        /// <summary>
        /// The tasks which are to be run by the server.
        /// </summary>
        private IList<CommandRunnerSubTask> Tasks;
        #endregion // Member Data

        #region Constructors
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="location"></param>
        public CommandRunnerTask(CommandOptions options)
            : this(new String[] { String.Empty }, options)
        {
        }

        public CommandRunnerTask(IEnumerable<String> locations, CommandOptions options)
            : base(NAME, DESC, CapabilityType.CommandRunner, locations, options)
        {
            String filename = GetSettingsFilename();

            IDictionary<String, Object> parameters = new Dictionary<String, Object>();
            RSG.Base.Xml.Parameters.Load(filename, ref parameters);
            this.Parameters = parameters;

           // this.DispatchScheme = new DispatchScheme(this.Config, options, this.Log, this.GetJobManagementFile());
        }
        #endregion //Constructors

        #region Controller Methods

        public override void Update()
        {
            // Only do job maintainance; cleaning the queue and associated files.
            this.CleanJobs();

            // Allow us to update job dispatch data without restarting the service.
            bool syncSubTasks = this.Parameters.ContainsKey(PARAM_SYNC_SUBTASKS) && (bool)this.Parameters[PARAM_SYNC_SUBTASKS];
            this.LoadCommandSubTasks(syncSubTasks);

            // Get list of changelists from base class; turn these into Jobs.
            IEnumerable<Changelist> changelists = base.GetSubmittedChangelists();
            if (changelists.Count() > 0)
                this.MaximumChangelist = changelists.Select(changelist => changelist.Number).Max();

            int jobsCreated = 0;

            lock (this.m_Jobs) // must lock jobs otherwise one might be consumed before the whole list of jobs is known, when that happens job consumption is not to the head
            {
                foreach (Changelist changelist in changelists)
                {
                    Log.MessageCtx(LOG_CTX, "Considering changelist #{0} submitted by '{1}' at {2}, which contains {3} files",
                        changelist.Number, changelist.Username, changelist.Time.ToString(), changelist.Files.Count());

                    if (changelist.Files.Count() == 0)
                    {
                        Log.WarningCtx(LOG_CTX, "Empty changelist {0}: {1}.  Ignoring.", changelist.Number, changelist.Description);
                        continue;
                    }

                    // Create jobs for this changelist
                    jobsCreated += CreateJobsForChangelist(changelist);
                }
            }
        }

        /// <summary>
        /// Client requests for a suitable job
        /// - check the dispatch groups and return the most appropriate job.
        /// </summary>
        /// <param name="machinename"></param>
        /// <returns>a codebuilder2job</returns>
        public override IJob GetSuitableJob(string machinename)
        {
            if (!base.CanGetSuitableJob(machinename))
                return null;

            lock (this.Tasks)
            {

                // Get the list tasks the machine can run.
                IList<CommandRunnerSubTask> tasks = this.Tasks.Where(t => t.Machine == machinename).ToList();

                // Get all the command runner jobs and iterate over them to find one we can use.
                foreach (CommandRunnerJob job in this.PendingJobs.Where(j => j is CommandRunnerJob))
                {
                    if (tasks.Any(t => t.Name == job.CommandName))
                    {
                        // Instead of assigning this job can we combine some jobs with this? ( skip to head ) then return that job.
                        if (job.SkipConsume >= 0)
                        {
                            // Consume any jobs that can bind to this job.
                            int numConsumed = ConsumeJobs(job);
                            if (numConsumed > 0)
                            {
                                // Find the consuming job
                                IJob consumingJob = this.PendingJobs.Single(j => j.ID == job.ConsumedByJobID);
                                Log.MessageCtx(LOG_CTX, "Instead of giving out job {0} we have combined {1} jobs and will give {2} to {3}", job.ID, numConsumed, consumingJob.ID, machinename.ToLower());
                                return consumingJob;
                            }
                        }

                        return job;
                    }
                }

                // Fallback process any tasks without a machine name
                if (machinename.Equals(Environment.MachineName, StringComparison.CurrentCultureIgnoreCase))
                {
                    tasks = this.Tasks.Where(t => String.IsNullOrEmpty(t.Machine)).ToList();
                    // Get all the command runner jobs and iterate over them to find one we can use.
                    foreach (CommandRunnerJob job in this.PendingJobs.Where(j => j is CommandRunnerJob))
                    {
                        if (tasks.Any(t => t.Name == job.CommandName))
                        {

                            // Instead of assigning this job can we combine some jobs with this? ( skip to head ) then return that job.
                            if (job.SkipConsume >= 0)
                            {
                                // Consume any jobs that can bind to this job.
                                int numConsumed = ConsumeJobs(job);
                                if (numConsumed > 0)
                                {
                                    // Find the consuming job
                                    IJob consumingJob = this.PendingJobs.Single(j => j.ID == job.ConsumedByJobID);
                                    Log.MessageCtx(LOG_CTX, "Instead of giving out job {0} we have combined {1} jobs and will give {2} to {3}", job.ID, numConsumed, consumingJob.ID, machinename.ToLower());
                                    return consumingJob;
                                }
                            }

                            return job;
                        }
                    }
                }
            }
            
            return null;
        }

        /// <summary>
        /// Returns location of dispatch scheme data file.
        /// </summary>
        /// <returns></returns>
        public String GetJobManagementFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_DISPATCH_SCHEME_FILE]));
        }

        /// <summary>
        /// Task notification of job being completed.
        /// </summary>
        /// <param name="job"></param>
        public override void JobCompleted(IJob job, IEnumerable<IJobResult> jobResults)
        {
            // Ensure any consumed jobs inherit the details of the completed consuming job.
            foreach (IJob consumedJob in this.Jobs.Where(j => j.ConsumedByJobID == job.ID))
            {
                consumedJob.CompletedAt = job.CompletedAt;
                consumedJob.ProcessedAt = job.ProcessedAt;
                consumedJob.ProcessingTime = job.ProcessingTime;
            }
            
            base.JobCompleted(job, jobResults);
        }

        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Get the tasks settings filename.
        /// </summary>
        /// <returns></returns>
        private String GetSettingsFilename()
        {
            return (Path.Combine(this.Config.ToolsConfig, "automation",
                "tasks", String.Format("{0}.xml", this.GetType().Name)));
        }

        /// <summary>
        /// Get the tasks settings filename.
        /// </summary>
        /// <returns></returns>
        private String GetCommandFolder()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_COMMANDS_FOLDER]));
        }

        /// <summary>
        /// Method to clean out jobs and their files.
        /// </summary>
        private void CleanJobs()
        {
            int hoursKeepCompleted = (int)this.Parameters[PARAM_HOURS_JOB];
            int hoursKeepPending = (int)this.Parameters[PARAM_HOURS_TO_KEEP_PENDING_JOBS];

            DateTime modifiedTimeCompleted = DateTime.UtcNow - TimeSpan.FromHours((double)hoursKeepCompleted);
            DateTime modifiedTimePending = DateTime.UtcNow - TimeSpan.FromHours((double)hoursKeepPending);

            // We now just delete any job that has existed for the specified time rather than
            // specifically just completed jobs.
            lock (this.m_Jobs)
            {
                foreach (IJob job in this.Jobs)
                {
                    DateTime completionTime = (job.ProcessedAt + job.ProcessingTime);
                    if (completionTime.IsEarlierThan(modifiedTimeCompleted))
                    {
                        CleanJob(job);
                    }

                    // Clean job if found pending for too long.
                    if (job.State == JobState.Pending && job.CreatedAt.IsEarlierThan(modifiedTimePending))
                    {
                        Log.WarningCtx(LOG_CTX, "A Job {0} was found to be in state pending for more than {1} hours. It has been cleaned.", job.ID, hoursKeepPending);
                        CleanJob(job);
                    }
                }

                // B*1377109 - Remove any job results for jobs that no longer exist
                SortedSet<Guid> jobIDs = new SortedSet<Guid>(this.Jobs.Select(job => job.ID));
                lock (this.m_JobResults)
                {
                    IJobResult[] resultsToRemove = m_JobResults.Where(jobResult => !jobIDs.Contains(jobResult.JobId)).ToArray();
                    foreach (IJobResult resultToRemove in resultsToRemove)
                    {
                        this.m_JobResults.Remove(resultToRemove);
                    }
                }
            }
        }

        /// <summary>
        /// Remove job from our queue and clean necessary files.
        /// </summary>
        /// <param name="job"></param>
        private void CleanJob(IJob job)
        {
            lock (this.m_Jobs)
            {
                IEnumerable<KeyValuePair<JobPriority, IJob>> jobEntries =
                    this.m_Jobs.Where(j => j.Value.ID.Equals(job.ID));
                if (jobEntries.Count() > 0)
                    this.m_Jobs.Remove(jobEntries.First());
            }
            Services.FileTransferService.CleanJobFiles(this.Config, job, this.Log);
        }

        /// <summary>
        /// Sync to folder so that new subtasks can be dropped into p4
        /// </summary>
        /// <param name="folder"></param>
        /// <returns>true if a sync took place</returns>
        private bool SyncCommandFolder(String folder)
        {
            this.Log.MessageCtx(LOG_CTX, " ");
            this.Log.MessageCtx(LOG_CTX, "**********************************");
            this.Log.MessageCtx(LOG_CTX, "Syncing command folder if required");
            using (P4 p4 = this.Options.Project.SCMConnect())
            {
                FileState[] filestates = FileState.Create(p4, new String[] { Path.GetFullPath(folder) + "..." });

                List<String> depotFilesNotOnHead = new List<String>();
                foreach (FileState fs in filestates)
                {
                    if (fs.OpenAction == FileAction.Edit)
                    {
                        this.Log.WarningCtx(LOG_CTX, "\tCommand file is opened for edit - cannot sync : {0}", fs.DepotFilename);
                    }
                    else if (fs.HaveRevision != fs.HeadRevision)
                    {
                        if (fs.HaveRevision == 0 && (fs.HeadAction == FileAction.Delete || fs.HeadAction == FileAction.MoveAndDelete || fs.HeadAction == FileAction.Purge))
                        {
                            // nothing to do - file is deleted and we are effectively on that revision. 
                            continue;
                        }
                        else
                        {
                            this.Log.MessageCtx(LOG_CTX, "\tNot on head revision : {0} :#{1}/{2}", fs.ClientFilename, fs.HaveRevision, fs.HeadRevision);
                            depotFilesNotOnHead.Add(fs.DepotFilename);
                        }
                    }
                }

                if (depotFilesNotOnHead.Any())
                {
                    this.Log.MessageCtx(LOG_CTX, "\tSyncing {0} Command Files as we are not on the head revision", depotFilesNotOnHead.Count);
                    P4API.P4RecordSet recordSet = p4.Run("sync", depotFilesNotOnHead.ToArray());
                    p4.ParseAndLogRecordSetForDepotFilename("sync", recordSet);
                    p4.LogP4RecordSetMessages(recordSet);
                    this.Log.MessageCtx(LOG_CTX, "**********************************");
                    this.Log.MessageCtx(LOG_CTX, " ");
                    return true;
                }
            }

            this.Log.MessageCtx(LOG_CTX, "No command folder sync was required");
            this.Log.MessageCtx(LOG_CTX, "**********************************");
            this.Log.MessageCtx(LOG_CTX, " ");
            return false;
        }

        /// <summary>
        /// Loads the command 
        /// </summary>
        /// <param name="syncFolder">Sync to folder so that new subtasks can be dropped into p4</param>
        private void LoadCommandSubTasks(bool syncCommandFolder)
        {
            Tasks = new List<CommandRunnerSubTask>();

            String folder = GetCommandFolder();
            if (!Directory.Exists(folder))
            {
                Log.ErrorCtx(LOG_CTX, "Command directory does not exist {0}", folder);
                return;
            }

            // Optionally sync to the command folderbefore opening it.
            if (syncCommandFolder)
            {
                SyncCommandFolder(folder);
            }

            String filePattern = "*.xml";
            Log.MessageCtx(LOG_CTX, "Loading {0} in command folder {1}", filePattern, folder);
            DirectoryInfo commandsDirectory = new DirectoryInfo(folder);

            IEnumerable<FileInfo> commandFileFileInfo = commandsDirectory.GetFiles(filePattern);

            Log.MessageCtx(LOG_CTX, "{0} files matching pattern {1} found in {2}", commandFileFileInfo.Count(), filePattern, folder);

            foreach (FileInfo commandFile in commandFileFileInfo)
            {
                String filename = commandFile.FullName;
                try
                {
                    XDocument xmlDoc = XDocument.Load(commandFile.FullName);
                    IEnumerable<XElement> xmlTaskElems = xmlDoc.XPathSelectElements("/tasks/task");
                    Log.MessageCtx(LOG_CTX, "\tLoaded {0} subtasks in {1}", xmlTaskElems.Count(), commandFile.FullName);
                    
                    for (int i = 0; i < xmlTaskElems.Count(); i++)
                    {
                        XElement xmlTaskElem = xmlTaskElems.ElementAt(i);
                        CommandRunnerSubTask commandRunnerSubTask = new CommandRunnerSubTask(this.Options, xmlTaskElem);
                        Tasks.Add(commandRunnerSubTask);
                        Log.MessageCtx(LOG_CTX, "\t\tAdded SubTask {0}/{1} : {2}", i + 1, xmlTaskElems.Count(), commandRunnerSubTask.Name);
                    }
                }
                catch (XmlException xmlException)
                {
                    // DW: For the sake of keeping the builds running : just handle the invalid xml exception for now.
                    Log.ExceptionCtx(LOG_CTX, xmlException, "Invalid xml file {0} will be ignored.", filename);
                }
            }

            (this.MonitoredPaths as List<String>).Clear();
            ISet<String> monPaths = new HashSet<string>();
            foreach (CommandRunnerSubTask task in Tasks.Where(t => t.MonitorPaths != null))
            {
                monPaths.AddRange(task.MonitorPaths);
            }

            FileMapping[] fms = FileMapping.Create(this.P4, monPaths.ToArray());
            (this.MonitoredPaths as List<String>).AddRange(fms.Where(fm => fm.Mapped).Select(fm => fm.DepotFilename));
            Log.MessageCtx(LOG_CTX, "Added {0} monitoring paths", this.MonitoredPaths.Count());

            //MonitoredPaths = monPaths;
            Log.MessageCtx(LOG_CTX, "Loaded {0} Command Runner Tasks", Tasks.Count);
        }

         /// <summary>
        /// Process the changelist creating jobs according the internal dispatch settings.
        /// </summary>
        /// <param name="changelist"></param>
        /// <param name="changeListFileMappings"></param>
        /// <returns></returns>
        private int CreateJobsForChangelist(Changelist changelist)
        {
            int totalJobCreated = 0;
            foreach (CommandRunnerSubTask task in Tasks)
            {
                // Ensure we only process files within a changelist that should be monitored.
                ICollection<String> monitoredFiles = new List<String>();
                foreach (String monitoredPath in task.MonitorPaths)
                {
                    
                    string mp = monitoredPath.ToLower();
                    int pathEnd = mp.IndexOf(P4_PATH_END);
                    if (pathEnd > 0)
                    {
                        mp = mp.Substring(0, pathEnd);
                    }

                    FileMapping fp = FileMapping.Create(this.P4, mp).FirstOrDefault();
                    if (fp == null)
                    {
                       // DW I'm not sure this merits a warning? Log.WarningCtx(LOG_CTX, "Monitoring {0} failed to produce file mappings", monitoredPath);
                        continue;
                    }

                    if (pathEnd > 0 && monitoredPath.Length > (pathEnd + P4_PATH_END.Length))
                    {
                        string ext = monitoredPath.Substring(pathEnd + P4_PATH_END.Length);
                        IEnumerable<String> monitorPaths = changelist.Files.Where(f => f.StartsWith(fp.DepotFilename, StringComparison.CurrentCultureIgnoreCase) 
                                                                                        && f.EndsWith(ext, StringComparison.CurrentCultureIgnoreCase));
                        monitoredFiles.AddRange(monitorPaths);
                    }
                    else
                    {
                        IEnumerable<String> monitorPaths = changelist.Files.Where(f => f.StartsWith(fp.DepotFilename, StringComparison.CurrentCultureIgnoreCase));
                        monitoredFiles.AddRange(monitorPaths);
                    }    
                }


                if (!monitoredFiles.Any())
                {
                    continue;
                }

                IEnumerable<FileMapping> changeListFiles = FileMapping.Create(this.P4, monitoredFiles.ToArray());
                
                ICollection<String> changeListFilenames = new List<String>();
                changeListFiles.ForEach(fm => changeListFilenames.Add(fm.DepotFilename));

                if (task.MonitorPaths.Count > 0)
                {
                    ICollection<String> preBuildCommands = new List<string>();
                    foreach (CommandRunnerJobInfo info in task.PrebuildCommands)
                    {
                        // CommandRunnerJobInfo to string builds the command
                        preBuildCommands.Add(info.ToString());
                    }

                    ICollection<String> postBuildCommands = new List<string>();    
                    foreach (CommandRunnerJobInfo info in task.PostBuildCommands)
                    {
                        // CommandRunnerJobInfo to string builds the command
                        postBuildCommands.Add(info.ToString());
                    }

                    CommandRunnerJob job = new CommandRunnerJob(task.Name, task.Options.Branch, changelist, task.SkipConsume, task.Commands, changeListFilenames, task.MonitorPaths, preBuildCommands, postBuildCommands, true, task.TargetDir, task.PublishDir, task.TargetRegex);
                    Log.MessageCtx(LOG_CTX, "\tCreating CommandRunner job ({0,-40} - {1,-15} - {2})", job.CommandName, job.BranchName, job.ID);
                    this.EnqueueJob(job);
                }

                totalJobCreated++;
            }
            
            return totalJobCreated;
        }

        /// <summary>
        /// Override the post job notification base method
        /// </summary>
        /// <param name="job"></param>
        /// <returns></returns>
        public override bool PostJobNotificationRequired(IJob job)
        {
            if (!(job is CommandRunnerJob))
                return false;

            CommandRunnerJob commandJob = job as CommandRunnerJob;
            return commandJob.RequiresPostJobNotification;
            
        }

        /// <summary>
        /// Send post job completion notifications
        /// </summary>
        /// <param name="job"></param>
        /// <param name="workerUri"></param>
        /// <param name="workerGuid"></param>
        public override void SendPostJobNotifications(IJob job, IEnumerable<IJobResult> jobResults, Uri workerUri, Guid workerGuid, String logFilename)
        {
            CommandRunnerJob commandJob = job as CommandRunnerJob;
            FileMapping[] fms = new FileMapping[0];
            IEnumerable<String> files = job.Trigger.TriggeringFiles();

            using (P4 p4 = this.Config.Project.SCMConnect())
            {
                if (p4 != null)
                {
                    fms = FileMapping.Create(p4, files.ToArray());
                }
            }

            CommandRunnerSubTask task = this.Tasks.Where(t => t.Name == commandJob.CommandName).FirstOrDefault();

            List<IEmailRecipient> sendList = new List<IEmailRecipient>();
            foreach (IEmailRecipient notificationRecipient in task.Notifications)
            {
                if (this.NotificationRequired(job, notificationRecipient, fms))
                {
                    sendList.Add(notificationRecipient);
                }
            }

            try
            {
                // Create bug(s) if required, return the bugids created
                IEnumerable<uint> bugIds = this.PostJobBugNotification(job, jobResults, workerUri, workerGuid, logFilename);

                // Create emails if required - pass the bugids so the email can show which bugs were raised.
                this.PostJobEmailNotification(sendList, bugIds, job, jobResults, workerUri, workerGuid, logFilename);
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                foreach (IEmailRecipient recipient in sendList)
                {
                    sb.Append(String.Format("Name :{0}", recipient.Name));
                    sb.Append(String.Format("Email :{0}", recipient.Email));
                }

                this.Log.ToolExceptionCtx(LOG_CTX, ex, "Error sending post job notification to ", sendList);
            }
        }

        protected override void PostJobEmailNotification(IEnumerable<IEmailRecipient> sendList, IEnumerable<uint> bugIds, IJob job, IEnumerable<IJobResult> jobResults, Uri workerUri, Guid workerGuid, String logFilename)
        {
            // A list of contiguous jobs of the same state that are associated and have been previously processed.
            IEnumerable<IJob> associatedJobs = new List<IJob>();

            if (job.State == JobState.Errors)
            {
                associatedJobs = this.Jobs.OrderByDescending(j => j.ProcessedAt)
                         .Where(j => Associated(job, j))
                         .TakeWhile(j => j.State == job.State);
            }

            CommandRunnerJob crJob = job as CommandRunnerJob;

            // LA: Keep this here. It is a way to get around not having ICloneable as a parent of the CommandOptions. 
            // This will only be here for a limited time as we will be rewriting the notifications system soon.

            IEmailNotification emailNotification = new JobPostNotification(job, associatedJobs, jobResults, Log, workerUri, workerGuid, this.Options, bugIds, true, 20, crJob.BranchName, crJob.CommandName);
            emailNotification.Send(sendList, EmailRedirects);
        }

        private int ConsumeJobs(IJob jobToConsume = null)
        {
            int numConsumed = 0;
            int numConsumers = 0;

            lock (this.m_Jobs)
            {
                // Get a list of candidate jobs for consuming 
                IEnumerable<CommandRunnerJob> originalCandidateJobs = this.Jobs.
                    OfType<CommandRunnerJob>().
                    Where(j => j.State == JobState.Pending).
                    Where(j => j.SkipConsume >= 0).
                    Where(j => j.ConsumedByJobID == Guid.Empty).
                    OrderBy(j => j.CreatedAt);

                IEnumerable<CommandRunnerJob> candidateJobs = originalCandidateJobs;

                if (jobToConsume != null)
                {
                    if (!(jobToConsume is CommandRunnerJob))
                    {
                        Log.ErrorCtx(LOG_CTX, "Cannot consume jobs of this type {0} : {1}", jobToConsume.ID, jobToConsume.GetType());
                        return 0;
                    }

                    if (!candidateJobs.Contains(jobToConsume))
                    {
                        Log.ErrorCtx(LOG_CTX, "Job {0} was asked to be consumed but this is not a candidate job", jobToConsume.ID);
                        return 0;
                    }

                    // Prune list down to this one job.
                    candidateJobs = new List<CommandRunnerJob>() { (CommandRunnerJob)jobToConsume };
                }

                // Here we store a list of jobs that are already consumed
                ISet<CommandRunnerJob> allJobsConsumed = new HashSet<CommandRunnerJob>();

                // For each job check if it can bind with another job
                int bindingsChecked = 0;
                foreach (CommandRunnerJob bindingJob1 in candidateJobs)
                {
                    if (allJobsConsumed.Contains(bindingJob1))
                        continue; // already consumed

                    IEnumerable<CommandRunnerJob> jobsToCheckBinding = originalCandidateJobs.Where(j => j != bindingJob1 && !allJobsConsumed.Contains(j));

                    ISet<CommandRunnerJob> jobsBindable = new HashSet<CommandRunnerJob>();
                    foreach (CommandRunnerJob bindingJob2 in jobsToCheckBinding)
                    {
                        bindingsChecked++;

                        if (CanBind(bindingJob1, bindingJob2))
                        {
                            jobsBindable.Add(bindingJob1);
                            jobsBindable.Add(bindingJob2);
                            Log.DebugCtx(LOG_CTX, "Job pair can bind ({0} {1})", bindingJob1.ID, bindingJob2.ID);
                        }
                    }

                    if (jobsBindable.Any())
                    {
                        // Ensure the threshold to skip has been passed. - all jobs have the same skip setting.
                        int skipThreshold = jobsBindable.First().SkipConsume;

                        // If the skip theshold is hit then consume all these jobs.
                        if (jobsBindable.Count() >= (int)skipThreshold)
                        {
                            Log.MessageCtx(LOG_CTX, " ");
                            Log.MessageCtx(LOG_CTX, "==================================");
                            Log.MessageCtx(LOG_CTX, "=== Consuming up to {0,5} jobs ===", jobsBindable.Count());
                            Log.MessageCtx(LOG_CTX, "==================================");

                            // joins the jobs it can together ( should be all of them ) forming a newly enqueued job
                            IEnumerable<CommandRunnerJob> jobsConsumedByNewConsumingJob;
                            CommandRunnerJob consumingJob = Bind(jobsBindable, out jobsConsumedByNewConsumingJob);

                            if (consumingJob != null)
                            {
                                // Check all jobs were consumed as this might indicate an issue if not.
                                if (jobsBindable.Count() != jobsConsumedByNewConsumingJob.Count())
                                {
                                    Log.WarningCtx(LOG_CTX, "Only {0} jobs were consumed out of {1} jobs.", jobsConsumedByNewConsumingJob.Count(), jobsBindable.Count());
                                }

                                // Update store of all the jobs consumed so we don't reuse already bound jobs.
                                allJobsConsumed.UnionWith(jobsConsumedByNewConsumingJob);

                                // The jobs that were consumed are now flagged as skipped/consumed
                                foreach (CommandRunnerJob jobConsumed in jobsConsumedByNewConsumingJob)
                                {
                                    Log.MessageCtx(LOG_CTX, "=== {0} is now skipped consumed", jobConsumed.ID);
                                    jobConsumed.SetConsumed(consumingJob);
                                    numConsumed++;
                                }
                                Log.MessageCtx(LOG_CTX, "=== {0} jobs consumed by job {1}", jobsConsumedByNewConsumingJob.Count(), consumingJob.ID);
                                Log.MessageCtx(LOG_CTX, "==================================");
                                Log.MessageCtx(LOG_CTX, " ");

                                numConsumers++;
                            }
                            else
                            {
                                Log.ErrorCtx(LOG_CTX, "No consuming jobs created.");
                            }
                        }
                    }
                }
                Log.MessageCtx(LOG_CTX, "{0} job bindings checked during Codebuilder2 ConsumeJobs", bindingsChecked);
            }


            if (numConsumed > 0)
            {
                Log.MessageCtx(LOG_CTX, " ");
                Log.MessageCtx(LOG_CTX, "=== {0} Total jobs consumed into {1} new jobs ===", numConsumed, numConsumers);
                Log.MessageCtx(LOG_CTX, " ");

                // Save the task when jobs are created - otherwise they could go missing if say the automation service crashed before the job could be executed.
                Save();
            }

            return numConsumed;
        }

        /// <summary>
        /// Checks the details of two jobs indicate they can be binded together for consuming in a bigger job.
        /// </summary>
        /// <param name="job1"></param>
        /// <param name="job2"></param>
        /// <returns>true if bindable</returns>
        private bool CanBind(CommandRunnerJob job1, CommandRunnerJob job2)
        {
            if (!job1.ProjectName.Equals(job2.ProjectName, StringComparison.CurrentCultureIgnoreCase))
                return false;

            if (!job1.BranchName.Equals(job2.BranchName, StringComparison.CurrentCultureIgnoreCase))
                return false;

            if (job1.SkipConsume < 0)
                return false;
            if (job2.SkipConsume < 0)
                return false;

            // Jobs must share the same skip consume setting to bind together.
            if (job1.SkipConsume != job2.SkipConsume)
                return false;

            // We can only join CL triggers
            if (!(job1.Trigger is IChangelistTrigger))
                return false;
            if (!(job2.Trigger is IChangelistTrigger))
                return false;

            if (!Associated(job1, job2))
                return false;

            return true;
        }

        // <summary>
        /// Two jobs are designated the same 'type of work', and from the perpective of the task are said to be 'associated'
        /// - this association is used to group bindable jobs and to associate jobs that form an incremental breakers list.
        /// </summary>
        /// <param name="job1"></param>
        /// <param name="job2"></param>
        /// <returns>true is associated</returns>
        public override bool Associated(IJob job1, IJob job2)
        {
            if (!(job1 is CommandRunnerJob))
                return false;
            if (!(job2 is CommandRunnerJob))
                return false;

            CommandRunnerJob commandRunnerJob1 = (CommandRunnerJob)job1;
            CommandRunnerJob commandRunnerJob2 = (CommandRunnerJob)job2;

            if (commandRunnerJob1.Role != commandRunnerJob2.Role)
                return false;

            if (commandRunnerJob1.BranchName != commandRunnerJob2.BranchName)
                return false;

            if (commandRunnerJob1.CommandName != commandRunnerJob2.CommandName)
                return false;

            if (commandRunnerJob1.TargetDir != commandRunnerJob2.TargetDir)
                return false;
            if (commandRunnerJob1.PublishDir != commandRunnerJob2.PublishDir)
                return false;

            if (commandRunnerJob1.RequiresPostJobNotification != commandRunnerJob2.RequiresPostJobNotification)
                return false;

            // handle null prebuild commands
            if (job1.PrebuildCommands == null)
            {
                if (!(job2.PrebuildCommands == null || job2.PrebuildCommands.Any()))
                    return false;
            }
            else if (job2.PrebuildCommands == null)
            {
                if (!job1.PrebuildCommands.Any())
                    return false;
            }
            else if (!Enumerable.SequenceEqual(job1.PrebuildCommands, job2.PrebuildCommands))
            {
                return false;
            }

            // handle null postbuild commands
            if (job1.PostbuildCommands == null)
            {
                if (!(job2.PostbuildCommands == null || job2.PostbuildCommands.Any()))
                    return false;
            }
            else if (job2.PostbuildCommands == null)
            {
                if (!job1.PostbuildCommands.Any())
                    return false;
            }
            else if (!Enumerable.SequenceEqual(job1.PostbuildCommands, job2.PostbuildCommands))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Creates jobs from an enumerable of jobs binding the common details together into a larger job
        /// </summary>
        /// <param name="jobsToConsume">the jobs to try to consume</param>
        /// <param name="jobsConsumed">the actual jobs consumed</param>
        /// <returns>the new job created if any</returns>
        private CommandRunnerJob Bind(IEnumerable<CommandRunnerJob> jobsToConsume, out IEnumerable<CommandRunnerJob> jobsConsumed)
        {
            String commandName = jobsToConsume.Select(j => j.CommandName).Distinct().Single();
            String branchName = jobsToConsume.Select(j => j.BranchName).Distinct().Single();
            String targetDir = jobsToConsume.Select(j => j.TargetDir).Distinct().Single();
            String publishDir = jobsToConsume.Select(j => j.PublishDir).Distinct().Single();
            String targetRegex = jobsToConsume.Select(j => j.TargetRegex).Distinct().Single();

            IEnumerable<ITrigger> triggers = jobsToConsume.Select(j => j.Trigger);

            // DW: really this should be checked to be distinct and single too, but CanBind() should already assure this.
            CommandRunnerJob firstJob = jobsToConsume.First();
            IEnumerable<String> prebuildCommands = firstJob.PrebuildCommands;
            IEnumerable<String> postbuildCommands = firstJob.PostbuildCommands;
            IEnumerable<String> labels = firstJob.Labels;
            IEnumerable<CommandRunnerJobInfo> commands = firstJob.Commands;

            IProject project;
            IBranch branch;

            if (!firstJob.GetProjectAndBranch(this.Options, Log, out project, out branch))
            {
                Log.ErrorCtx(LOG_CTX, "Job binding: Couldn't derive project and branch for codebuilder job");
                jobsConsumed = null;
                return null;
            }

            CommandRunnerJob consumingJob = new CommandRunnerJob(commandName, branch, triggers, commands, prebuildCommands, postbuildCommands, true, targetDir, publishDir, targetRegex, Guid.NewGuid(), DateTime.UtcNow); 

            if (consumingJob != null)
            {
                EnqueueJob(consumingJob);
            }

            // We ensure we externally list all the jobs consumed
            jobsConsumed = jobsToConsume;

            return consumingJob;
        }
        #endregion // Private Methods
    }
}
