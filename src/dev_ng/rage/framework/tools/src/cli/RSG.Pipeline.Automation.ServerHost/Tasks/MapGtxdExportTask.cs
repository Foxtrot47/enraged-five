﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Imaging;
using System.Xml.Linq;
using RSG.Base.Logging;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Core;
using RSG.SourceControl.Perforce;
using RSG.Interop.Bugstar;
using RSG.Interop.Bugstar.Organisation;
using RSG.Pipeline.Services;


namespace RSG.Pipeline.Automation.ServerHost.Tasks
{

    /// <summary>
    /// Map Export GTXD server-side task.
    /// This is a temporary measure to ensure that the GTXD is being built in tandem 
    /// with other texture changes, until url:bugstar:1179456 can be resolved.
    /// </summary>
    internal class MapGtxdExportTask :
        TimedTaskBase
    {
        #region Constants
        /// <summary>
        /// The task name.
        /// </summary>
        private static readonly String NAME = "Map GTXD Export Task";

        /// <summary>
        /// The task description.
        /// </summary>
        private static readonly String DESC = "Map GTXD Export Task.";

        /// <summary>
        /// Parameter for the file listing email addresses to send notification data to.
        /// </summary>
        private static readonly string PARAM_TIMED_TASK_FILE = "Timed Task File";

        /// <summary>
        /// Parameter for the file listing email addresses to send notification data to.
        /// </summary>
        private static readonly string PARAM_EMAIL_NOTIFICATIONS_FILE = "Notifications File";

        /// <summary>
        /// Parameter specifying the GTXD file.
        /// </summary>
        private static readonly string PARAM_GTXD_FILE = "GTXD File";

        /// <summary>
        /// Task parameters.
        /// </summary>
        private IDictionary<String, Object> Parameters;
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Constructor
        /// </summary>
        public MapGtxdExportTask(CommandOptions options)
            : base(NAME, DESC, CapabilityType.AssetBuilder | CapabilityType.AssetBuilderRebuilds, options)
        {
            String filename = GetSettingsFilename();
            RSG.Base.Xml.Parameters.Load(filename, ref this.Parameters);
        }
        #endregion // Constructor(s)

        #region Controller Methods        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override String GetTimedTasksFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_TIMED_TASK_FILE]));
        }

        /// <summary>
        /// Populate cache with user images and user details.
        /// </summary>
        public override void ExecuteTimedTask(TimedTaskEvent timedTaskEvent)
        {
            string gtxdFilename = (string)this.Parameters[PARAM_GTXD_FILE];
            if (String.IsNullOrWhiteSpace(gtxdFilename)) return;

            //Only send out a job if one isn't already pending.
            if (this.OutstandingJobs.Count() == 0)
            {
                //Clear out all of the jobs.
                this.m_Jobs.Clear();

                List<String> filenames = new List<String>();
                filenames.Add(gtxdFilename);
                AssetBuilderJob job = new AssetBuilderJob(this.Options.Branch);
                job.EngineFlags = EngineFlags.Default;
                job.Trigger = new UserRequestTrigger(Environment.UserName, filenames, Guid.NewGuid(), DateTime.UtcNow);
                this.EnqueueJob(job);
            }
        }

        /// <summary>
        /// Return status of task.
        /// </summary>
        /// <returns></returns>
        public override TaskStatus Status()
        {
            return (new TaskStatus(this));
        }
        #endregion // Controller Methods

        #region Private Methods

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        String GetSettingsFilename()
        {
            return (Path.Combine(this.Config.ToolsConfig, "automation",
                "tasks", String.Format("{0}.xml", this.GetType().Name)));
        }

        /// <summary>
        /// File containing information required to send notifications.
        /// </summary>
        /// <returns></returns>
        public override String GetNotificationDataFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_EMAIL_NOTIFICATIONS_FILE]));
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.ServerHost.Tasks namespace
