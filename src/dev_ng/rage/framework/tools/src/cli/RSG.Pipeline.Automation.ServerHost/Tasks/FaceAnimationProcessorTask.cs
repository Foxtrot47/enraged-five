﻿using System;
using System.Collections.Generic;
using System.Linq;
using SIO = System.IO;
using System.Text;
using System.Xml.Linq;
using RSG.Base.ConfigParser;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Automation.Common;
using RSG.Pipeline.Automation.Common.Export;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.Services;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.Pipeline.Automation.Common.Triggers;
using RSG.Pipeline.Core;
using RSG.Pipeline.Content;
using RSG.SourceControl.Perforce;
using RSG.SourceControl.Perforce.Util;
using RSG.Pipeline.Services;

namespace RSG.Pipeline.Automation.ServerHost.Tasks
{
    class FaceAnimationProcessorTask : MonitoringTaskBase, IMonitoringTask
    {
        #region Constants
        private static readonly String NAME = "Face Animation Processor";
        private static readonly String DESC = "Auto-implement face animation into FBX files.";

        /// <summary>
        /// Parameter for the file listing all the paths to monitor.
        /// </summary>
        private static readonly string PARAM_MONITOR_PATH_FILE = "Path Monitoring File";

        /// <summary>
        /// Parameter for the file listing email addresses to send notification data to.
        /// </summary>
        private static readonly string PARAM_EMAIL_NOTIFICATIONS_FILE = "Notifications File";

        /// <summary>
        /// Parameter for number of hours between job cleans.
        /// </summary>
        private static readonly String PARAM_HOURS_JOB = "Hours to Keep Completed Jobs";

        #endregion // Constants

        #region Member Data

        /// <summary>
        /// AP3 processor collection.
        /// </summary>
        IProcessorCollection ProcessorCollection;

        /// <summary>
        /// Log object.
        /// </summary>
        //private IUniversalLog Log;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="server"></param>
        /// <param name="location"></param>
        public FaceAnimationProcessorTask(CommandOptions options)
            : this(new String[] { String.Empty }, options)
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="server"></param>
        /// <param name="locations"></param>
        public FaceAnimationProcessorTask(IEnumerable<String> locations, CommandOptions options)
            : base(NAME, DESC, CapabilityType.FaceAnimationProcessor, locations, options)
        {
            this.ProcessorCollection = new ProcessorCollection(this.Config);
            //this.Log = LogFactory.CreateUniversalLog("FaceAnimationProcessorTask");

            String filename = GetSettingsFilename();

            IDictionary<String, Object> parameters = new Dictionary<String, Object>();
            RSG.Base.Xml.Parameters.Load(filename, ref parameters);
            this.Parameters = parameters;

            this.LoadMonitoringLocations();
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        public override void Update()
        {
            // Only do job maintainance; cleaning the queue and associated files.
            this.CleanJobs();

            // Reload all parameters
            String filename = GetSettingsFilename();
            IDictionary<String, Object> parameters = new Dictionary<String, Object>();
            RSG.Base.Xml.Parameters.Load(filename, ref parameters);
            this.Parameters = parameters;

            // Allows us to update monitoring locations without restarting the service.
            this.LoadMonitoringLocations();

            // Get list of changelists from base class; turn these into Jobs.
            IEnumerable<Changelist> changelists = base.GetSubmittedChangelists();
            if (changelists.Count() > 0)
                this.MaximumChangelist = changelists.Select(changelist => changelist.Number).Max();

            foreach (Changelist change in changelists)
            {
                // Only process changelist if it has the trigger in the description.
                if (BuildTokenHelper.HasBuildToken(this.Log, change.Description, BuildToken.FACE))
                {
                    List<String> filesToProcess = new List<String>();

                    foreach (string file in change.Files)
                    {
                        if (file.ToLower().EndsWith(".fbx"))
                        {
                            filesToProcess.Add(file);
                        }
                    }

                    FileMapping[] mappedFiles = FileMapping.Create(this.P4, filesToProcess.ToArray());
                    List<String> jobFiles = new List<String>();

                    // Create a job for each FBX file in the changelist.
                    foreach (FileMapping mappedFile in mappedFiles)
                    {
                        jobFiles.Clear();
                        jobFiles.Add(mappedFile.DepotFilename);

                        Job job = new Job(this.Options.Branch, this.Role, jobFiles);
                        job.Trigger = new FilesTrigger(jobFiles);

                        this.EnqueueJob(job);
                    }

                    Save();
                }
            }
        }

        /// <summary>
        /// Task notification of job being completed.
        /// </summary>
        /// <param name="job"></param>
        public override void JobCompleted(IJob job, IEnumerable<IJobResult> jobResults)
        {
            base.JobCompleted(job, jobResults);
        }

        /// <summary>
        /// File containing information required to send notifications.
        /// </summary>
        /// <returns></returns>
        public override String GetNotificationDataFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_EMAIL_NOTIFICATIONS_FILE]));
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Method to clean out jobs and their files.
        /// </summary>
        private void CleanJobs()
        {
            int hours = (int)this.Parameters[PARAM_HOURS_JOB];
            DateTime modifiedTime = DateTime.UtcNow - TimeSpan.FromHours((double)hours);

            // We now just delete any job that has existed for the specified time rather than
            // specifically just completed jobs.
            foreach (IJob job in this.Jobs)
            {
                DateTime completionTime = (job.ProcessedAt + job.ProcessingTime);
                if (completionTime.IsEarlierThan(modifiedTime))
                {
                    CleanJob(job);
                }
            }
        }

        /// <summary>
        /// Remove job from our queue and clean necessary files.
        /// </summary>
        /// <param name="job"></param>
        private void CleanJob(IJob job)
        {
            lock (this.m_Jobs)
            {
                IEnumerable<KeyValuePair<JobPriority, IJob>> jobEntries =
                    this.m_Jobs.Where(j => j.Value.ID.Equals(job.ID));
                if (jobEntries.Count() > 0)
                    this.m_Jobs.Remove(jobEntries.First());
            }
            Services.FileTransferService.CleanJobFiles(this.Config, job, this.Log);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private String GetSettingsFilename()
        {
            return (SIO.Path.Combine(this.Config.ToolsConfig, "automation",
                "tasks", String.Format("{0}.xml", this.GetType().Name)));
        }

        /// <summary>
        /// 
        /// </summary>
        private void LoadMonitoringLocations()
        {
            (this.MonitoredPaths as List<String>).Clear();
            String monitoringFile = this.Config.Environment.Subst((String)this.Parameters[PARAM_MONITOR_PATH_FILE]);
            if (!SIO.File.Exists(monitoringFile) && this.MonitoredPaths.Count() == 0)
            {
                this.Log.ErrorCtx(LOG_CTX, "Monitoring file does not exist ({0}).  Texture Processor task will not be monitoring any locations.", monitoringFile);
            }

            XDocument xdoc = XDocument.Load(monitoringFile);
            List<String> locations = new List<String>();
            foreach (XElement elem in xdoc.Descendants("location"))
            {
                String path = elem.Attribute("path").Value;
                locations.Add(Options.Branch.Environment.Subst(path));
            }

            FileMapping[] fms = FileMapping.Create(this.P4, locations.ToArray());
            (this.MonitoredPaths as List<String>).AddRange(fms.Select(fm => fm.DepotFilename));
        }
        #endregion // Private Methods
    }
}
