﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Threading;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Pipeline.Automation.Common.Export;
using RSG.Pipeline.Automation.Common.Jobs;
using RSG.Pipeline.Automation.Common.JobManagement;
using RSG.Pipeline.Automation.Common.Notifications;
using RSG.Pipeline.Automation.Common.Services;
using RSG.Pipeline.Automation.Common.Tasks;
using RSG.SourceControl.Perforce;
using RSG.Pipeline.Services;

namespace RSG.Pipeline.Automation.ServerHost.Tasks
{

    /// <summary>
    /// Task that responds to user-submitted jobs.
    /// </summary>
    public class MapExportNetworkTask :
        TaskBase,
        ITask,
        IJobManagedTask
    {
        #region Constants
        private static readonly String NAME = "Map Network-Export";
        private static readonly String DESC = "Auto-exporting network task.";
        private static readonly String LOG_CTX = "Map Export Network Task";
        private static readonly String PARAM_HOURS_JOB = "Hours to Keep Completed Jobs";

        /// <summary>
        /// Parameter for the file listing email addresses to send notification data to.
        /// </summary>
        private static readonly string PARAM_EMAIL_NOTIFICATIONS_FILE = "Notifications File";

        /// <summary>
        /// Parameter for the file listing job management data.
        /// </summary>
        private static readonly String PARAM_DISPATCH_SCHEME_FILE = "Dispatch Scheme File";
        private static readonly String DISPATCH_SCHEME_STUDIO_NAME_TO_REPLACE = "[Studio]";

        public DispatchScheme DispatchScheme { get; set; }
        #endregion // Constants
                
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public MapExportNetworkTask(CommandOptions options)
            : base(NAME, DESC, Common.CapabilityType.MapExportNetwork, options)
        {
            String filename = GetSettingsFilename();

            IDictionary<String, Object> parameters = new Dictionary<String, Object>();
            RSG.Base.Xml.Parameters.Load(filename, ref parameters);
            this.Parameters = parameters;

            this.DispatchScheme = new DispatchScheme(this.Config, options, this.Log, this.GetJobManagementFile());
        }
        #endregion // Constructor(s)
        
        #region Controller Methods
        /// <summary>
        /// Task update tick (not required for this task).
        /// </summary>
        public override void Update()
        {
            // Only do job maintainance; cleaning the queue and associated files.
            this.CleanJobs();

            // Allow us to update job dispatch data without restarting the service
            lock (this.DispatchScheme)
            {
                bool syncDispatch = this.Parameters.ContainsKey(PARAM_SYNC_DISPATCH) && (bool)this.Parameters[PARAM_SYNC_DISPATCH];
                this.DispatchScheme.LoadDispatchData(this.GetJobManagementFile(), this.Options, syncDispatch);
            }
        }
        
        /// <summary>
        /// Return status information for this task.
        /// </summary>
        /// <returns></returns>
        public override TaskStatus Status()
        {
            return (new TaskStatus(this));
        }

        /// <summary>
        /// File containing information required to send notifications.
        /// </summary>
        /// <returns></returns>
        public override String GetNotificationDataFile()
        {
            return (this.Config.Environment.Subst((String)this.Parameters[PARAM_EMAIL_NOTIFICATIONS_FILE]));
        }

        /// <summary>
        /// Returns location of dispatch scheme data file, after substituting out the studio name
        /// </summary>
        /// <returns></returns>
        public String GetJobManagementFile()
        {
            String jobManagementFile = this.Config.Environment.Subst((String)this.Parameters[PARAM_DISPATCH_SCHEME_FILE]);
            try
            {
                jobManagementFile = jobManagementFile.Replace(DISPATCH_SCHEME_STUDIO_NAME_TO_REPLACE, this.Config.Studios.ThisStudio.Name);
            }
            catch (Exception ex )
            {
                Log.ErrorCtx(LOG_CTX, "Exception whilst trying to figure out jobManagementFile: {0}", ex.ToString());
            }
            
            return jobManagementFile;
        }

        /// <summary>
        /// Network exporter GetSuitableJob override.
        /// </summary>
        /// <param name="machinename"></param>
        /// <returns></returns>
        public override IJob GetSuitableJob(string machinename)
        {
            if (!base.CanGetSuitableJob(machinename))
                return null;

            if (this.TaskState != TaskState.Active)
            {
                Log.MessageCtx(LOG_CTX, "No Job given to host {0} as the task {1} is {2}", machinename, this.Name, this.TaskState);
                return null;
            }

            Log.MessageCtx(LOG_CTX, "Client '{0}' is requesting a suitable job.", machinename.ToLower());
            lock (this.DispatchScheme)
            {
                // Find any suitable dispatch groups for this machine
                DispatchGroup[] suitableDispatchGroups = this.DispatchScheme.Groups.Where(jg => jg.MachineName.ToLower() == machinename.ToLower()).ToArray();

                if (suitableDispatchGroups.Length > 0)
                {
                    Log.MessageCtx(LOG_CTX, "Client '{0}' has suitable dispatch groups registered.", machinename.ToLower());
 
                    // Loop through machine suitable groups looking for a match
                    lock (this.m_Jobs)
                    {
                        foreach (IJob job in this.PendingJobs.Where(j => j is MapExportJob))
                        {
                            foreach (DispatchGroup suitableDispatchGroup in suitableDispatchGroups)
                            {
                                // AJM: This will need updating if/when we start allowing multiple export processes in one job again.
                                MapExportJob mapExpJob = (MapExportJob)job;
                                List<String> exportFiles = mapExpJob.ExportProcesses.SelectMany(expProc => expProc.ExportFiles).ToList();

                                if (suitableDispatchGroup.ContainsAnyPath(Log, exportFiles))
                                {
                                    Log.MessageCtx(LOG_CTX, "Found job '{0}' for client '{1}'.", job.ID.ToString(), machinename.ToLower());
                                    return job;
                                }
                            }
                        }
                    }
                }
                else
                {
                    // Just take the first job from list if no suitable dispatch group for this exporter was found.  
                    // If a machine isn't in the table then it might start running out of disk space - we'll see how we get on.
                    return this.PendingJobs.FirstOrDefault();
                }
            }
            return null;
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// Return settings XML filename.
        /// </summary>
        /// <returns></returns>
        /// This is virtual allowing you to override it if required; although
        /// it's not recommended.
        /// 
        private String GetSettingsFilename()
        {
            return (Path.Combine(this.Config.ToolsConfig, "automation",
                "tasks", String.Format("{0}.xml", this.GetType().Name)));
        }

        /// <summary>
        /// Method to clean out jobs and their files.
        /// </summary>
        private void CleanJobs()
        {
            int hours = (int)this.Parameters[PARAM_HOURS_JOB];
            DateTime modifiedTime = DateTime.UtcNow - TimeSpan.FromHours((double)hours);

            // We now just delete any job that has existed for the specified time rather than
            // specifically just completed jobs, or if a job was skipped/aborted/client errored, remove 
            // that if the createdAt time is older than the modified time.
            foreach (IJob job in this.Jobs)
            {
                switch (job.State)
                {
                    case JobState.Completed:
                    case JobState.Errors:
                        if (job.CompletedAt.IsEarlierThan(modifiedTime))
                        {
                            CleanJob(job);
                        }
                        break;
                    case JobState.Aborted:
                    case JobState.Assigned:
                    case JobState.ClientError:
                    case JobState.Skipped:
                    case JobState.SkippedConsumed:
                        if (job.CreatedAt.IsEarlierThan(modifiedTime))
                        {
                            CleanJob(job);
                        }
                        break;
                    default:
                        break;               
                }               
            }
        }

        /// <summary>
        /// Remove job from our queue and clean necessary files.
        /// </summary>
        /// <param name="job"></param>
        private void CleanJob(IJob job)
        {
            lock (this.m_Jobs)
            {
                IEnumerable<KeyValuePair<JobPriority, IJob>> jobEntries =
                    this.m_Jobs.Where(j => j.Value.ID.Equals(job.ID));
                if (jobEntries.Count() > 0)
                    this.m_Jobs.Remove(jobEntries.First());
            }
            Services.FileTransferService.CleanJobFiles(this.Config, job, this.Log);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.Automation.ServerHost.Tasks namespace
