﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Statistics.Common.Dto.GameAssets;
using RSG.Statistics.Client.ServiceClients;
using RSG.Statistics.Client.Common;
using RSG.Base.Logging;
using RSG.Base.OS;
using System.IO;
using System.Text.RegularExpressions;
using RSG.Base.Logging.Universal;
using RSG.Statistics.Common.Config;
using RSG.Pipeline.Services;
using System.Diagnostics;
using RSG.Base.Configuration;

namespace RSG.Statistics.GenerateAssetStats
{
    class Program
    {
        #region Constants
        // Return codes
        private static readonly int EXIT_SUCCESS = 0;
        private static readonly int EXIT_WARNING = 1;
        private static readonly int EXIT_ERROR = 2;
        private static readonly int EXIT_CRITIC = 3;

        /// <summary>
        /// Location of the build version file relative to the build dir
        /// </summary>
        const string BUILD_VERSION_FILE = @"common\data\version.txt";

        /// <summary>
        /// The tag that is found on the line preceding the version number
        /// </summary>
        const string VERSION_NUMBER_TAG = @"[VERSION_NUMBER]";

        // Parameters
        private static readonly String OPTION_FORCE_SYNC = "force";     // Force the syncing of the data
        private static readonly String OPTION_BUILD = "build";          // Override the build that should be used
        private static readonly String OPTION_DEV = "dev";              // Flag indicating that this is running in dev mode
        private static readonly String OPT_SERVER = "server";
        #endregion // Constants

        #region Member Data
        private static IUniversalLog m_log;
        private static IStatsServer s_server;
        #endregion // Member Data

        #region Program Entry Point
        /// <summary>
        /// The main entry point for the application.
        /// Attribute [STAThread] for invoking the universal log from the right thread
        /// </summary>
        [STAThread]
        static int Main(string[] args)
        {
            int exit_code = EXIT_SUCCESS;

            // Create the log
            LogFactory.Initialize();
            m_log = LogFactory.CreateUniversalLog("Statistics_GenerateAssetStats");
            LogFactory.CreateApplicationConsoleLogTarget();
            UniversalLogFile logfile = LogFactory.CreateUniversalLogFile(m_log) as UniversalLogFile;
            
            // Define our executable options.  Stick with the long options
            // (e.g. --opt) rather than the single character versions.
            LongOption[] lopts = new LongOption[] {
                new LongOption(OPT_SERVER, LongOption.ArgType.Required, "Server to connect to."),
                new LongOption(OPTION_FORCE_SYNC, LongOption.ArgType.None, "Force synchronise the assets."),
                new LongOption(OPTION_BUILD, LongOption.ArgType.Required, "Build we are syncing assets for."),
                new LongOption(OPTION_DEV, LongOption.ArgType.None, "Dev mode.")
            };
            CommandOptions options = new CommandOptions(args, lopts);

            bool forceSync = options.ContainsOption(OPTION_FORCE_SYNC);
            String buildOverride = (options.ContainsOption(OPTION_BUILD) ? options[OPTION_BUILD] as String : null);
            bool dev = options.ContainsOption(OPTION_DEV);

            // Initialise the server to communicate with.
            IStatisticsConfig config = new StatisticsConfig(options.Branch);
            if (!options.ContainsOption(OPT_SERVER))
            {
                s_server = config.DefaultServer;
            }
            else
            {
                String serverName = (String)options[OPT_SERVER];
                Debug.Assert(config.Servers.ContainsKey(serverName), "Requested an unknown server");
                s_server = config.Servers[serverName];
            }

            // Preamble...
            if (dev)
            {
                Console.WriteLine("Press <enter> to start client.");
                Console.ReadLine();
                Console.WriteLine();
                Console.WriteLine("Client is now running...");
            }

            try
            {
                // Get the build version
                m_log.Message("Extracting Build Version");

                uint buildMajor, buildMinor;
                if (!GetBuildVersion(out buildMajor, out buildMinor, buildOverride, options.Branch))
                {
                    throw new Exception("Unable to retrieve the current build version.");
                }
                m_log.Message("Major Version: {0}", buildMajor);
                m_log.Message("Minor Version: {0}", buildMinor);
                
                // Add the build to the stats server
                m_log.Message("Adding Build to the Database");
                BuildDto build = AddBuildToDb(buildMajor, buildMinor);

                // Sync the
                
                if (forceSync || !build.HasAssetStats)
                {
                    AssetSyncService syncService = new AssetSyncService(options.Branch, s_server);
                    syncService.SyncClipDictionaryStats(build);
                    syncService.SyncCutsceneStats(build);
                    syncService.SyncLevels();
                    syncService.SyncLevelStats(build);
                    syncService.SyncCharacters();
                    syncService.SyncCharacterStats(build);
                    syncService.SyncVehicles();
                    syncService.SyncVehicleStats(build);
                    syncService.SyncWeapons();
                    syncService.SyncWeaponStats(build);
                    syncService.SyncRadioStations();
                    syncService.SyncMapAreas();
                    syncService.SyncMapSections();
                    syncService.SyncMapHierarchy(build);
                    syncService.UpdateMapAreaStats(build);
                    syncService.UpdateMapSectionStats(build);
                    syncService.SyncArchetypes();
                    syncService.SyncRooms();
                    syncService.SyncArchetypeStats(build);
                    syncService.SyncEntities();
                    syncService.SyncEntityStats(build);
                    syncService.SyncCarGens(build);
                    syncService.SyncSpawnPoints(build);

                    m_log.Message("Updating Build Info");
                    if (!UpdateBuild(build))
                    {
                        throw new Exception("An error occurred while updating the build to toggle the has asset stats flag");
                    }
                    Console.WriteLine();
                }
                else if (!forceSync)
                {
                    m_log.Message("Nothing to do as the build already has asset stats associated with it.");
                }
            }
            catch (System.Exception ex)
            {
                m_log.ToolException(ex, "Unexpected exception occurred while generating the game asset stats.");
                exit_code = EXIT_CRITIC;
            }

            if (dev)
            {
                Console.WriteLine("Press <enter> to stop client.");
                Console.ReadLine();
            }

            LogFactory.ApplicationShutdown();

            Environment.ExitCode = exit_code;
            return exit_code;
        }
        #endregion // Program Entry Point

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="major"></param>
        /// <param name="minor"></param>
        /// <returns></returns>
        private static bool GetBuildVersion(out uint major, out uint minor, string buildOverride, IBranch branch)
        {
            bool successfullyRetrieved = false;

            // Reset the values
            major = 0;
            minor = 0;

            if (buildOverride != null)
            {
                successfullyRetrieved = ParseBuildString(buildOverride, out major, out minor);
            }
            else
            {
                String filePath = Path.Combine(branch.Build, BUILD_VERSION_FILE);

                if (File.Exists(filePath))
                {
                    // Read the file line by line looking for the line containing the version number
                    using (StreamReader fileStream = new StreamReader(filePath))
                    {
                        string line;
                        bool extractFromLine = false;

                        while ((line = fileStream.ReadLine()) != null)
                        {
                            if (extractFromLine)
                            {
                                successfullyRetrieved = ParseBuildString(line, out major, out minor);
                                break;
                            }

                            if (line.Contains(VERSION_NUMBER_TAG))
                            {
                                extractFromLine = true;
                            }
                        }
                    }
                }
            }

            return successfullyRetrieved;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buildString"></param>
        /// <param name="major"></param>
        /// <param name="minor"></param>
        private static bool ParseBuildString(string buildString, out uint major, out uint minor)
        {
            // Reset the values
            major = 0;
            minor = 0;

            // Regex to match the build string
            Regex reg = new Regex(@"^(?<major>\d+).?(?<minor>\d*)");

            // Try to match the regex
            Match match = reg.Match(buildString);
            if (match.Success)
            {
                Group majorGroup = match.Groups["major"];
                major = UInt32.Parse(majorGroup.Value);

                Group minorGroup = match.Groups["minor"];
                if (minorGroup.Success && !String.IsNullOrEmpty(minorGroup.Value))
                {
                    minor = UInt32.Parse(minorGroup.Value);
                }
            }

            return match.Success;
        }

        /// <summary>
        /// Adds a particular build to the database
        /// </summary>
        /// <param name="major"></param>
        /// <param name="minor"></param>
        /// <returns></returns>
        private static BuildDto AddBuildToDb(uint major, uint minor)
        {
            BuildClient buildClient = new BuildClient(s_server);

            string buildId;
            if (minor == 0)
            {
                buildId = String.Format("{0}", major);
            }
            else
            {
                buildId = String.Format("{0}.{1}", major, minor);
            }

            // Check whether this build already exists on the stats server
            BuildDto build = null;
            
            try
            {
                build = buildClient.GetByIdentifier(buildId);
            }
            catch (System.Exception)
            {	
            }

            if (build == null)
            {
                // Build didn't exist, so create a new one
                Log.Log__Message("Creating new build with identifier '{0}'", buildId);
                build = new BuildDto();
                build.Identifier = buildId;
                build.GameVersion = (major << 4) | minor;
                build = buildClient.PutAsset(buildId, build);
            }
            else
            {
                Log.Log__Message("Retrieved existing build with identifier '{0}'", buildId);
            }

            return build;
        }

        /// <summary>
        /// Sets the has asset stats flag true for the passed in build
        /// </summary>
        /// <param name="build"></param>
        /// <returns></returns>
        private static bool UpdateBuild(BuildDto build)
        {
            build.HasAssetStats = true;

            BuildClient buildClient = new BuildClient(s_server);
            buildClient.PutAsset(build.Identifier, build);
            return true;
        }
        #endregion // Private Methods
    }
}
