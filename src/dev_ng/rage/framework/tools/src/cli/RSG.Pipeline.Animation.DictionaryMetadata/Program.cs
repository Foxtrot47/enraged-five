﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using RSG.Base.OS;
using System.Xml;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace RSG.Pipeline.Animation.Postprocess
{
    sealed class Program
    {
         #region Constants
        private static readonly int EXIT_SUCCESS = 0;
        private static readonly int EXIT_ERROR = 1;

        private static readonly String OPTION_SOURCE_DIR = "sourcedir";
        private static readonly String OPTION_PROCESSED_DIR = "processeddir";
        private static readonly String OPTION_OUTPUTFILE = "outputfile";
        private static readonly String OPTION_TASK_NAME = "taskname";
        private static readonly String LOG_CTX = "Dictionary Metadata";
        
        #endregion // Constants

        [STAThread]
        static int Main(string[] args)
        {
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.CreateUniversalLog("MergeExternal");

            int exit_code = EXIT_SUCCESS;
            try
            {
                // Define our executable options.  Stick with the long options
                // (e.g. --opt) rather than the single character versions.
                Getopt options = new Getopt(args, new LongOption[] {
                    new LongOption(OPTION_SOURCE_DIR, LongOption.ArgType.Required,
                        "Input directory."),
                    new LongOption(OPTION_OUTPUTFILE, LongOption.ArgType.Required,
                        "Output filename."),
                    new LongOption(OPTION_PROCESSED_DIR, LongOption.ArgType.Required,
                        "Processed directory."),
                    new LongOption(OPTION_TASK_NAME, LongOption.ArgType.Required,
                        "Asset Pipeline 3 Task Name."),
                });
                // Turn args into vars
                String sourceDirectory = options[OPTION_SOURCE_DIR] as String;
                String outputFile = options[OPTION_OUTPUTFILE] as String;
                String processedDirectory = options[OPTION_PROCESSED_DIR] as String;

                if (File.Exists(outputFile))
                    File.Delete(outputFile);

                String taskName = options[OPTION_TASK_NAME] == null ? String.Empty : options[OPTION_TASK_NAME] as String;

                if (String.Empty != taskName)
                {
                    Console.WriteLine("TASK: {0}", taskName);
                    log.MessageCtx(LOG_CTX, "TASK: {0}", taskName);
                }

                Directory.GetFiles(sourceDirectory);

                Dictionary<string, string> dictSource = new Dictionary<string, string>();
                Dictionary<string, string> dictProcessed = new Dictionary<string, string>();

                foreach (string file in Directory.GetFiles(sourceDirectory))
                    dictSource.Add(Path.GetFileNameWithoutExtension(file), file);

                foreach (string file in Directory.GetFiles(processedDirectory))
                    dictProcessed.Add(Path.GetFileNameWithoutExtension(file), file);

                XmlWriterSettings settings = new XmlWriterSettings();
                settings.Indent = true;

                using (XmlWriter writer = XmlWriter.Create(outputFile, settings))
                {
                    writer.WriteStartDocument();
                    writer.WriteStartElement("fwClipRpfBuildMetadata");

                    writer.WriteStartElement("name");
                    writer.WriteString(Path.GetFileName(Path.GetDirectoryName(processedDirectory)));
                    writer.WriteEndElement();

                    writer.WriteStartElement("dictionaries");

                    foreach (KeyValuePair<String, String> entry in dictSource)
                    {
                        writer.WriteStartElement("Item");
                        writer.WriteAttributeString("key", Path.GetFileName(entry.Value));

                        long sourceFileSizeBytes = 0;
                        FileInfo fi = new FileInfo(entry.Value);
                        if (fi != null)
                            sourceFileSizeBytes = fi.Length;

                        writer.WriteStartElement("sizeBefore");
                        writer.WriteAttributeString("value", sourceFileSizeBytes.ToString());
                        writer.WriteEndElement();

                        long processedFileSizeBytes = 0;
                        if (dictProcessed.ContainsKey(entry.Key))
                            fi = new FileInfo(dictProcessed[entry.Key]);
                        if (fi != null)
                            processedFileSizeBytes = fi.Length;

                        writer.WriteStartElement("sizeAfter");
                        writer.WriteAttributeString("value", processedFileSizeBytes.ToString());
                        writer.WriteEndElement();

                        writer.WriteEndElement();
                    }

                    writer.WriteEndElement();

                    writer.WriteEndElement();

                    writer.WriteEndDocument();
                }
            }
            catch (Exception ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Unhandled Exception building animation Dictionary Metadata");
                exit_code = EXIT_ERROR;
            }
            finally
            {
                LogFactory.ApplicationShutdown();
            }
            return (exit_code);
        }
    }
}
