﻿using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Metadata.Data;
using RSG.Metadata.Model;
using System.Linq;
using System.Xml;
using RSG.Metadata.Parser;
using System.Text;

namespace MetadataDiffBuilder
{
    class Program
    {
        /// <summary>
        /// Local log.
        /// </summary>
        private static IUniversalLog s_log;

        static void Main(string[] args)
        {
            s_log = LogFactory.CreateUniversalLog("ReportGenerator");
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLogTarget logfile = LogFactory.CreateUniversalLogFile(s_log);

            Getopt options = new Getopt(args, new LongOption[] {
                new LongOption("current", LongOption.ArgType.Required, 'c'),
                new LongOption("new", LongOption.ArgType.Required, 'd'),
                new LongOption("output", LongOption.ArgType.Required, 'o')
            });

            string currentFilename = options.Options["current"] as string;
            string newFilename = options.Options["new"] as string;
            string patchFilename = options.Options["output"] as string;

            IConfig config = ConfigFactory.CreateConfig();
            IBranch branch = config.Project.DefaultBranch;

            StructureDictionary structures = new StructureDictionary();
            structures.Load(branch, Path.Combine(branch.Metadata, "definitions"));

            MetaFile currentMetaFile = new MetaFile(currentFilename, structures);
            MetaFile newMetaFile = new MetaFile(newFilename, structures);
            if (currentMetaFile.Members.Definition != newMetaFile.Members.Definition)
            {
                s_log.Error("Provided files contain different definitions. Aborting.");
                System.Environment.Exit(2);
            }

            List<ITunable> currentTunables = GetAllTunablesRecursive(currentMetaFile);
            List<ITunable> newTunables = GetAllTunablesRecursive(newMetaFile);
            if (currentTunables.Count != newTunables.Count)
            {
                s_log.Error("Provided files contain different numbers of items. Aborting.");
                System.Environment.Exit(2);
            }

            List<ITunable> changedTunables = new List<ITunable>();
            for (int i = 0; i < currentTunables.Count; i++)
            {
                ITunable currentTunable = currentTunables[i];
                ITunable newTunable = newTunables[i];

                bool equal = currentTunable.HasSameValue(newTunable);
                if (!equal)
                {
                    ITunable parentModel = newTunable.ParentModel as ITunable;
                    while (parentModel != null)
                    {
                        if (parentModel is ArrayTunable)
                        {
                            foreach (ITunable arrayItem in (parentModel as ArrayTunable).Items)
                            {
                                if (!changedTunables.Contains(arrayItem))
                                {
                                    changedTunables.Add(arrayItem);
                                }
                            }
                        }

                        parentModel = parentModel.ParentModel as ITunable;
                    }

                    if (!changedTunables.Contains(newTunable))
                    {
                        changedTunables.Add(newTunable);
                    }
                }
            }

            SerialisationHierarchy serialisationHierarchy = new SerialisationHierarchy();
            foreach (ITunable changedTunable in changedTunables)
            {
                ITunable parentModel = changedTunable.ParentModel as ITunable;
                List<ITunable> parentHierarchy = new List<ITunable>() { changedTunable };
                while (parentModel != null)
                {
                    parentHierarchy.Add(parentModel);
                    parentModel = parentModel.ParentModel as ITunable;
                }

                while (serialisationHierarchy.FindItem(changedTunable) == null)
                {
                    SerialisationHierarchy found = null;
                    int index = 0;
                    int maxIndex = parentHierarchy.Count - 1;
                    while (found == null && index <= parentHierarchy.Count - 1)
                    {
                        ITunable parentTunable = parentHierarchy[index];
                        found = serialisationHierarchy.FindItem(parentTunable);
                        if (found == null)
                        {
                            index++;
                        }
                    }

                    if (found == null)
                    {
                        serialisationHierarchy.Tunable = parentHierarchy[maxIndex];
                    }
                    else
                    {
                        found.Children.Add(new SerialisationHierarchy(parentHierarchy[index - 1]));
                    }
                }
            }

            using (FileStream stream = new FileStream(patchFilename, FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                using (XmlTextWriter writer = new XmlTextWriter(stream, Encoding.UTF8))
                {
                    writer.Formatting = Formatting.Indented;
                    writer.WriteStartDocument();
                    Serialise(writer, serialisationHierarchy, false, false, stream);
                    writer.WriteEndDocument();
                }
            }
        }

        public static List<ITunable> GetAllTunablesRecursive(MetaFile root)
        {
            List<ITunable> tunables = new List<ITunable>();
            foreach (ITunable child in root.Members.Values)
            {
                GetAllTunables(child, ref tunables);
            }

            return tunables;
        }

        private static void GetAllTunables(ITunable root, ref List<ITunable> tunables)
        {
            if (root is StructureTunable)
            {
                foreach (ITunable child in (root as StructureTunable).Values)
                {
                    GetAllTunables(child, ref tunables);
                }
            }
            else if (root is PointerTunable)
            {
                if ((root as PointerTunable).Value is ITunable)
                {
                    ITunable child = (root as PointerTunable).Value as ITunable;
                    GetAllTunables(child, ref tunables);
                }
            }
            else if (root is ArrayTunable)
            {
                foreach (ITunable child in (root as ArrayTunable).Items)
                {
                    GetAllTunables(child, ref tunables);
                }
            }
            else if (root is MapTunable)
            {
                foreach (ITunable child in (root as MapTunable).Items)
                {
                    GetAllTunables(child, ref tunables);
                }
            }
            else if (root is PointerTunable)
            {
                if ((root as PointerTunable).Value is ITunable)
                {
                    ITunable child = (root as PointerTunable).Value as ITunable;
                    GetAllTunables(child, ref tunables);
                }
            }
            else if (root is MapItemTunable)
            {
                if ((root as MapItemTunable).Value is ITunable)
                {
                    ITunable child = (root as MapItemTunable).Value as ITunable;
                    GetAllTunables(child, ref tunables);
                }
            }
            else
            {
                if (!tunables.Contains(root))
                {
                    tunables.Add(root);
                }
            }
        }

        private static void Serialise(XmlWriter writer, SerialisationHierarchy hierarchy, bool isInArray, bool isInPointer, FileStream stream)
        {
            if (!isInPointer)
            {
                if (hierarchy.Tunable != null)
                {
                    if (isInArray)
                    {
                        writer.WriteStartElement("Item");
                        ArrayTunable arrayTunable = hierarchy.Tunable.ParentModel as ArrayTunable;
                        int index = arrayTunable.Items.IndexOf(hierarchy.Tunable);
                        if (hierarchy.Children.Count != 0)
                        {
                            StructureTunable structure = arrayTunable[index] as StructureTunable;
                            if (structure == null)
                            {
                                PointerTunable pointer = arrayTunable[index] as PointerTunable;
                                if (pointer != null)
                                {
                                    hierarchy.Tunable.SerialiseWithoutChildren(writer);
                                }

                                structure = pointer.Value as StructureTunable;
                            }

                            if (structure != null)
                            {
                                if (structure.TunableKey is StringTunable)
                                {
                                    StringTunable key = structure.TunableKey as StringTunable;
                                    (writer as XmlTextWriter).Formatting = Formatting.None;
                                    writer.WriteComment(" " + key.Value + " ");
                                    (writer as XmlTextWriter).Formatting = Formatting.Indented;
                                }
                            }                            
                        }
                    }
                    else
                    {
                        writer.WriteStartElement(hierarchy.Tunable.Name.Replace(':', '_'));
                        hierarchy.Tunable.SerialiseWithoutChildren(writer);
                    }
                }
            }

            foreach (SerialisationHierarchy child in hierarchy.Children)
            {
                Serialise(writer, child, hierarchy.Tunable is ArrayTunable, hierarchy.Tunable is PointerTunable, stream);
            }

            if (!isInPointer)
            {
                if (hierarchy.Tunable != null)
                {
                    writer.WriteEndElement();
                }

                if (isInArray)
                {
                    if (hierarchy.Children.Count == 0)
                    {
                        ArrayTunable arrayTunable = hierarchy.Tunable.ParentModel as ArrayTunable;
                        int index = arrayTunable.Items.IndexOf(hierarchy.Tunable);
                        StructureTunable structure = arrayTunable[index] as StructureTunable;
                        if (structure == null)
                        {
                            PointerTunable pointer = arrayTunable[index] as PointerTunable;
                            structure = pointer.Value as StructureTunable;
                        }

                        if (structure != null)
                        {
                            if (structure.TunableKey is StringTunable)
                            {
                                StringTunable key = structure.TunableKey as StringTunable;
                                (writer as XmlTextWriter).Formatting = Formatting.None;
                                writer.WriteComment(" " + key.Value + " ");
                                (writer as XmlTextWriter).Formatting = Formatting.Indented;
                            }
                        }
                    }
                }
            }
        }

        private class SerialisationHierarchy
        {
            public ITunable Tunable { get; set; }

            public List<SerialisationHierarchy> Children { get; private set; }

            public SerialisationHierarchy()
            {
                this.Children = new List<SerialisationHierarchy>();
            }

            public SerialisationHierarchy(ITunable tunable)
            {
                this.Children = new List<SerialisationHierarchy>();
                this.Tunable = tunable;
            }

            public SerialisationHierarchy FindItem(ITunable tunable)
            {
                if (this.Tunable == tunable)
                {
                    return this;
                }

                foreach (SerialisationHierarchy hierarchy in this.Children)
                {
                    SerialisationHierarchy found = hierarchy.FindItem(tunable);
                    if (found != null)
                    {
                        return found;
                    }
                }

                return null;
            }
        }
    }
}
