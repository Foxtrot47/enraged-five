﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Xml.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Content;
using RSG.Pipeline.Core;
using RSG.SourceControl.Perforce;
using File = RSG.Pipeline.Content.File;
using SIO = System.IO;

namespace RSG.Pipeline.InteriorProxyGenerator
{
    class Program
    {
        private const string ArgOutput = "output";
        private const string ArgForceSync = "forcesync";
        private const string ArgContentFilter = "filter";

        private const string LoggingContext = "Interior Proxies Generation";

        private static CommandOptions Options;

        [STAThread]
        static int Main(string[] args)
        {
            LongOption[] options =
            {
                new LongOption(ArgOutput, LongOption.ArgType.Required, "Output file list of metadata gathered interiors"),
                new LongOption(ArgForceSync, LongOption.ArgType.Required, "if you want to force sync the metadata for that one"),
                new LongOption(ArgContentFilter, LongOption.ArgType.Required, "filter through content.xml file"),
            };

            Options = new CommandOptions(args, options);

            LogFactory.Initialize();
            IUniversalLog log = LogFactory.CreateUniversalLog("InteriorProxiesUpdater");

            // we are in DLC ? if not, bail out
            if (!Options.Branch.Project.IsDLC)
            {
                log.MessageCtx(LoggingContext, "Not in a DLC context, exiting.");
                return Constants.Exit_Success;
            }

            log.MessageCtx(LoggingContext, "Retrieving metadata");
            // get metadata for each districts
            // get the metadata combine through contentTree
            IContentTree contentTree = Factory.CreateTree(Options.Branch);
            ContentTreeHelper contentHelper = new ContentTreeHelper(contentTree);

            // get the content.xml for the project and branch
            string contentPath = Path.Combine(Options.Branch.Build, "content.xml");
            // sync it
            using (P4 p4 = Options.Branch.Project.SCMConnect())
            {
                p4.Run("sync", false, new[] {contentPath});
            }
            // parse it
            IEnumerable<string> metadatafiles = null;
            if (Options.ContainsOption(ArgContentFilter) && bool.Parse((string)Options[ArgContentFilter]))
            {
                using (FileStream fs = SIO.File.OpenRead(contentPath))
                {
                    XDocument contentXml = XDocument.Load(fs);

                    var items = contentXml
                        .Element("CDataFileMgr__ContentsOfDataFileXml")
                        .Elements("contentChangeSets")
                        .Elements("Item")
                        .Elements("mapChangeSetData")
                        .Elements("Item")
                        .Elements("filesToEnable")
                        .Elements("Item");
                    if (items.Any())
                    {
                        metadatafiles = items
                            .Where(i => i.Value.EndsWith("metadata.rpf", StringComparison.OrdinalIgnoreCase))
                            .Select(i => Path.GetFileNameWithoutExtension(i.Value))
                            .ToList();
                    }
                }
            }
            else
            {
                metadatafiles = new string[0];
            }

            // METADATA SYNCING
            var metadataNodes = contentHelper.GetMetadataNodes();
            
            // filter the metadata with the list retrieved from content
            log.MessageCtx(LoggingContext, "Filtering metadata.");
            List<File> nodesToSync = metadataNodes
                .OfType<File>()
                .Where(node => node.AbsolutePath.StartsWith(Options.Branch.Processed, StringComparison.OrdinalIgnoreCase)).ToList();

            for (int i = 0; i < nodesToSync.Count; i++)
            {
                var node = nodesToSync[i].Basename;

                if (!metadatafiles.Contains(node))
                {
                    nodesToSync.RemoveAt(i);
                }
            }

            List<string> filesToOpen;

            using (P4 p4 = Options.Branch.Project.SCMConnect())
            {
                log.Message("Building sync list of required Metadata.");
                var clientFilenames = nodesToSync.Select(n => n.AbsolutePath).ToList();

                filesToOpen = P4AltSync(Options.Branch, p4, clientFilenames);
                filesToOpen.Sort((a, b) => String.Compare(a, b, StringComparison.OrdinalIgnoreCase));

                bool forceSync = Options.ContainsOption(ArgForceSync) && bool.Parse((string)Options[ArgForceSync]);
                string[] p4args = forceSync ? new[] { "-f" }.Concat(filesToOpen).ToArray() : filesToOpen.ToArray();

                log.Message("Syncing Metadata.");
                p4.Run("sync", false, p4args);
            }

            log.Message("Parsing files...");
            List<string> interiorList = new List<string>();

            foreach (string s in filesToOpen)
            {
                using (Stream sourceStream = System.IO.File.OpenRead(s))
                {
                    using (ZipArchive archive = new ZipArchive(sourceStream, ZipArchiveMode.Read))
                    {
                        foreach (ZipArchiveEntry zipArchiveEntry in archive.Entries.Where(e => e.Name == "mapexportadditions.xml"))
                        {
                            using (var entry = zipArchiveEntry.Open())
                            {
                                XDocument mapExportAdditions = XDocument.Load(entry);

                                var dependencies = mapExportAdditions.Element("ManifestData").Elements("IMAPDependency2");

                                var imapNames = dependencies.Where(d => d.Attribute("isInterior").Value == "True").Select(d => d.Attribute("imapName").Value);
                                interiorList.AddRange(imapNames);
                            }
                        }
                    }
                }
            }

            using (FileStream fs = new FileStream(Options[ArgOutput] as string, FileMode.Create, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(fs))
                {
                    var interiorsDistinct = interiorList.Distinct();
                    foreach (var interior in interiorsDistinct)
                    {
                        sw.WriteLine("\t<Item>" + interior + "</Item>");
                    }
                }
            }

            return Constants.Exit_Success;
        }

        private static List<string> P4AltSync(IBranch branch, P4 p4, IEnumerable<String> clientFilenames)
        {
            List<string> localFiles = new List<string>();

            FileState[] filestates = FileState.Create(p4, new[] { branch.Processed + "..." + "#head" });

            foreach (var filename in clientFilenames)
            {
                foreach (FileState fs in filestates)
                {
                    if (fs.ClientFilename.Equals(filename, StringComparison.OrdinalIgnoreCase))
                    {
                        string file = Path.GetFileNameWithoutExtension(filename);
                        if (file != null && file.EndsWith("_metadata"))
                        {
                            localFiles.Add(filename);
                        }
                    }
                }
            }

            return localFiles;
        }
    }
}
