﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using RSG.Pipeline.Services.Animation;
using RSG.Base.OS;
using RSG.Base.Logging.Universal;
using RSG.Base.Logging;

namespace RSG.Pipeline.Animation.ClipGroup
{
    sealed class Program
    {

        #region Constants
        private static readonly int EXIT_SUCCESS = 0;
        private static readonly int EXIT_ERROR = 1;

        private static readonly String OPTION_ZIP = "zip";
        private static readonly String OPTION_OUTDIR = "outdir";
        private static readonly String OPTION_MODDIR = "moddir";
        private static readonly String OPTION_TEMPLATEDIR = "templatedir";
        private static readonly String OPTION_SKELDIR = "skeldir";
        private static readonly String OPTION_CLIP_FIXUP = "cliprename";
        private static readonly String OPTION_DEFAULTSKEL = "defaultskeleton";
        private static readonly String OPTION_TASK_NAME = "taskname";
        private static readonly String LOG_CTX = "Clip Group";
        #endregion // Constants

        [STAThread]
        static int Main(string[] args)
        {
            int exit_code = EXIT_SUCCESS;

            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.CreateUniversalLog("ClipGroup");

            try
            {
                // Define our executable options.  Stick with the long options
                // (e.g. --opt) rather than the single character versions.
                Getopt options = new Getopt(args, new LongOption[] {
                    new LongOption(OPTION_ZIP, LongOption.ArgType.Required,
                        ""),
                    new LongOption(OPTION_OUTDIR, LongOption.ArgType.Required,
                        "Output directory."),
                    new LongOption(OPTION_MODDIR, LongOption.ArgType.Required,
                        ""),
                    new LongOption(OPTION_TEMPLATEDIR, LongOption.ArgType.Required,
                        ""),
                    new LongOption(OPTION_SKELDIR, LongOption.ArgType.Required,
                        "Skeleton directory."),
                    new LongOption(OPTION_CLIP_FIXUP, LongOption.ArgType.Required,
                        ""),
                    new LongOption(OPTION_DEFAULTSKEL, LongOption.ArgType.Required,
                        "Default skeleton file."),
                    new LongOption(OPTION_TASK_NAME, LongOption.ArgType.Required,
                        "Asset Pipeline 3 Task Name."),
                });
                // Turn args into vars
                String zipFile = options[OPTION_ZIP] as String;
                String outputDirectory = options[OPTION_OUTDIR] as String;
                String modDirectory = options[OPTION_MODDIR] as String;
                String templateDirectory = options[OPTION_TEMPLATEDIR] as String;
                String skelDirectory = options[OPTION_SKELDIR] as String;
                String clipFixupExe = options[OPTION_CLIP_FIXUP] as String;
                String defaultSkeleton = options[OPTION_DEFAULTSKEL] == null ? String.Empty : options[OPTION_DEFAULTSKEL] as String;
                String taskName = options[OPTION_TASK_NAME] == null ? String.Empty : options[OPTION_TASK_NAME] as String;

                if (String.Empty != taskName)
                {
                    Console.WriteLine("TASK: {0}", taskName);
                    log.MessageCtx(LOG_CTX, "TASK: {0}", taskName);
                }

                exit_code = GroupFactory.ExtractAndCreateClipGroups(log, zipFile, outputDirectory, modDirectory, templateDirectory, skelDirectory, clipFixupExe, defaultSkeleton) ? EXIT_SUCCESS : EXIT_ERROR;
            }
            catch (Exception ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Unhandled Exception during animation ClipGroup Process");
                exit_code = EXIT_ERROR;
            }
            finally
            {
                LogFactory.ApplicationShutdown();
            }
            return ( exit_code );
        }
    }
}
