﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.Animation.Cutscene.MergeExternal
{
    class Program
    {
        #region Constants
        private static readonly String OPTION_INPUTFILE = "inputfile";
        private static readonly String OPTION_OUTPUTFILE = "outputfile";
        private static readonly String OPTION_SCENENAME = "scenename";
        private static readonly String OPTION_ERRORCHECK = "errorcheck";
        private static readonly String OPTION_EXTERNAL_MERGE = "cutfmergeexternal";
        private static readonly String OPTION_ASSETS_DIR = "assets";
        private static readonly String OPTION_TASK_NAME = "taskname";
        private static readonly String LOG_CTX = "Merge External";
        #endregion // Constants

        static int RunProcess(string strExe, string strArgs, IUniversalLog log)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.Arguments = strArgs;
            startInfo.FileName = strExe;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.CreateNoWindow = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false;

            log.MessageCtx(LOG_CTX, strExe + " " + strArgs);

            Process proc = Process.Start(startInfo);
            String output = proc.StandardOutput.ReadToEnd();
            proc.WaitForExit();

            if (output != String.Empty)
                log.MessageCtx(LOG_CTX, output);

            return proc.ExitCode;
        }

        static bool GetConcatNameForPart(string name, string dir, IUniversalLog log, ref string concatName)
        {
            if (Directory.Exists(dir))
            {
                string[] files = Directory.GetFiles(dir, "*.concatlist");
                foreach (string file in files)
                {
                    try
                    {
                        var xmlDoc = new XmlDocument();
                        xmlDoc.Load(file);

                        XmlNodeList nodes = xmlDoc.SelectNodes("/RexRageCutscenePartFile/parts/Item/filename");
                        foreach (XmlNode partnode in nodes)
                        {
                            if (partnode.InnerText.ToLower().Contains(name.ToLower()))
                            {
                                XmlNode node = xmlDoc.SelectSingleNode("/RexRageCutscenePartFile/path");
                                if (node != null)
                                {
                                    concatName = Path.GetFileName(node.InnerText.ToLower());
                                    return true;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        log.ToolExceptionCtx(LOG_CTX, ex, "Error while parsing file: {0}", file);
                        return false;
                    }
                }
            }
            concatName = String.Empty;
            return false;
        }

        static bool IsConcatScene(string name, string dir, IUniversalLog log)
        {
            if (Directory.Exists(dir))
            {
                string[] files = Directory.GetFiles(dir, "*.concatlist");
                foreach (string file in files)
                {
                    try
                    {
                        var xmlDoc = new XmlDocument();
                        xmlDoc.Load(file);

                        XmlNode node = xmlDoc.SelectSingleNode("/RexRageCutscenePartFile/path");
                        if (node != null)
                        {
                            if (Path.GetFileName(node.InnerText.ToLower()).ToLower() == name.ToLower())
                                return true;
                        }
                    }
                    catch (Exception ex)
                    {
                        log.ToolExceptionCtx(LOG_CTX, ex, "Error while parsing file: {0}", file);
                        return false;
                    }
                }
            }

            return false;
        }

        [STAThread]
        static int Main(string[] args)
        {
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.CreateUniversalLog("MergeExternal");
            try
            {
                // Define our executable options.  Stick with the long options
                // (e.g. --opt) rather than the single character versions.
                LongOption[] opts = new LongOption[] {
                new LongOption(OPTION_INPUTFILE, LongOption.ArgType.Required,
                    "Input filename."),
                new LongOption(OPTION_OUTPUTFILE, LongOption.ArgType.Required,
                    "Output filename."),
                new LongOption(OPTION_SCENENAME, LongOption.ArgType.Required,
                    ""),
                new LongOption(OPTION_ERRORCHECK, LongOption.ArgType.Required,
                    ""),
                new LongOption(OPTION_EXTERNAL_MERGE, LongOption.ArgType.Required,
                    ""),
                new LongOption(OPTION_ASSETS_DIR, LongOption.ArgType.Required,
                    ""),
                new LongOption(OPTION_TASK_NAME, LongOption.ArgType.Required,
                    "Asset Pipeline 3 Task Name."),
                };

                CommandOptions options = new CommandOptions(args, opts);
                
                // Turn args into vars
                String inputFile = options[OPTION_INPUTFILE] as String;
                String outputFile = options[OPTION_OUTPUTFILE] as String;
                String sceneName = options[OPTION_SCENENAME] as String;
                bool errorCheck = options.ContainsOption(OPTION_ERRORCHECK);
                String cutfExternalMergeExe = options[OPTION_EXTERNAL_MERGE] as String;
                String assetsDir = options[OPTION_ASSETS_DIR] as String;
                String taskName = options[OPTION_TASK_NAME] == null ? String.Empty : options[OPTION_TASK_NAME] as String;
                bool noShadows = false;

                if (String.Empty != taskName)
                {
					Console.WriteLine("TASK: {0}", taskName);
                    log.MessageCtx(LOG_CTX, "TASK: {0}", taskName);
                }

                String outputDirectory = Path.GetDirectoryName(outputFile);
                //log.MessageCtx(LOG_CTX, "Creating directory '{0}'", outputDirectory);
                //if (!RSG.Pipeline.Services.Animation.DirectoryServices.CreateDirectory(outputDirectory))
                //{
                //    log.ErrorCtx(LOG_CTX, "Failed to create directory: '{0}'.  Aborting.", outputDirectory);
                //    return (Constants.Exit_Failure);
                //}

                String cutGameFile = String.Empty;
                String cutReplayFile = String.Empty;

                if (File.Exists(inputFile))
                {
                    String concatSceneName = String.Empty;

                    if (IsConcatScene(sceneName, Path.Combine(assetsDir, "cuts", "!!Cutlists"), log))
                    {
                        cutGameFile = String.Format("{0}/cuts/!!game_data//{1}.cutxml", assetsDir, sceneName);
                        cutReplayFile = String.Format("{0}/metadata/animation/replayedits//{1}.meta", assetsDir, sceneName);
                    }
                    else if (GetConcatNameForPart(sceneName, Path.Combine(assetsDir, "cuts", "!!Cutlists"), log, ref concatSceneName))
                    {
                        cutGameFile = String.Format("{0}/cuts/!!game_data//{1}.cutxml", assetsDir, concatSceneName);
                        cutReplayFile = String.Format("{0}/metadata/animation/replayedits//{1}.meta", assetsDir, concatSceneName);
                        if (!File.Exists(cutGameFile))
                        {
                            cutGameFile = String.Format("{0}/cuts/!!game_data//{1}.cutxml", assetsDir, sceneName);
                            noShadows = true;
                        }
                        if (!File.Exists(cutReplayFile))
                        {
                            cutReplayFile = String.Format("{0}/metadata/animation/replayedits//{1}.meta", assetsDir, sceneName);
                        }
                    }
                    else
                    {
                        cutGameFile = String.Format("{0}/cuts/!!game_data//{1}.cutxml", assetsDir, sceneName);
                        cutReplayFile = String.Format("{0}/metadata/animation/replayedits//{1}.meta", assetsDir, sceneName);
                    }

                    if(!File.Exists(cutGameFile) && !File.Exists(cutReplayFile))
                    {
                        log.MessageCtx(LOG_CTX, "Game/Replay files '{0}' not found, no concat dropback found, copying file '{1}' to '{2}'", String.Format("{0}/cuts/!!game_data//{1}.cutxml", assetsDir, sceneName), inputFile, outputFile);
                        // Not all scenes have game data, if not just copy the file
                        File.Copy(inputFile, outputFile, true);
                        return RSG.Pipeline.Core.Constants.Exit_Success;
                    }
                }
                else
                {
                    log.ErrorCtx(LOG_CTX, "File '{0}' does not exist", inputFile);
                    if (Directory.Exists(Path.GetDirectoryName(outputFile)))
                        Directory.Delete(Path.GetDirectoryName(outputFile), true);
                    return RSG.Pipeline.Core.Constants.Exit_Failure;
                }

                if(log.HasErrors)
                    return RSG.Pipeline.Core.Constants.Exit_Failure;

                log.MessageCtx(LOG_CTX, "Merging game file '{0}' / Replay file '{1}'", cutGameFile, cutReplayFile);

                string cutfMergeExternalArgs = String.Format("-cutFile \"{0}\" -outputCutfile \"{1}\" -hideAlloc -nopopups",
                    inputFile, outputFile, cutReplayFile);
                if(File.Exists(cutGameFile))
                    cutfMergeExternalArgs += String.Format(" -cutGame \"{0}\"", cutGameFile);
                if (File.Exists(cutReplayFile))
                    cutfMergeExternalArgs += String.Format(" -cutReplay \"{0}\"", cutReplayFile);
                if (errorCheck)
                    cutfMergeExternalArgs += " -errorCheck";
                if (noShadows)
                    cutfMergeExternalArgs += " -noShadows";
                if (RunProcess(cutfExternalMergeExe, cutfMergeExternalArgs, log) != 0)
                {
                    if (Directory.Exists(Path.GetDirectoryName(outputFile)))
                        Directory.Delete(Path.GetDirectoryName(outputFile), true);
                    return RSG.Pipeline.Core.Constants.Exit_Failure;
                }
                
                return RSG.Pipeline.Core.Constants.Exit_Success;
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Unhandled Exception during Merge External Process");
                return RSG.Pipeline.Core.Constants.Exit_Failure;
            }
            finally
            {
                LogFactory.ApplicationShutdown();
            }
        }
    }
}
