﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.OS;
using RSG.Configuration;

namespace BootStrapper
{
    /// <summary>
    /// Application which will be used to bootstrap another app shadow copying the
    /// assemblies the app uses to a directory in the user profile.
    /// </summary>
    public static class BootstrapperApp
    {
        #region Constants
        /// <summary>
        /// Command-line option to import core tools configuration variables.
        /// </summary>
        /// This is used for compatibility with existing scripts/tools to
        /// migrate to the installer-less world.
        private const String OPT_TOOLSCONFIG = "toolsconfig";
        #endregion // Constants

        /// <summary>
        /// Main entry point into the application. The first thing to get called.
        /// </summary>
        /// <param name="args"></param>
        public static int Main(string[] args)
        {
            int returnCode = ExitCode.Success;
            try
            {
                // Parse command-line options.
                LongOption[] opts = new LongOption[]
                {
                    new LongOption(OPT_TOOLSCONFIG, LongOption.ArgType.None, 
                        "Import core tools configuration variables.")
                };
                Getopt options = new Getopt(args, opts);

                // Make sure enough args were provided.
                if (options.TrailingOptions.Length < 1)
                {
                    throw new ArgumentException("Not enough arguments provided.");
                }

                // Determine the name of the application/config file.
                string applicationExe = options.TrailingOptions[0];
                if (!File.Exists(applicationExe))
                {
                    throw new FileNotFoundException("Specified application does not exist: " + applicationExe);
                }

                string applicationDir = Path.GetDirectoryName(applicationExe);
                string applicationName = Path.GetFileNameWithoutExtension(applicationExe);

                // Determine the location where we wish to shadow copy the assemblies to.
                string appDataFolder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                string commonCachePath = Path.Combine(appDataFolder, "Rockstar Games", "Boot Strapper", applicationName);

                // Split up the command line args to provide the application.
                string[] applicationArguments = args.Skip(1).ToArray();

                do
                {
                    Guid applicationInstanceGuid = Guid.NewGuid();
                    string applicationCachePath = Path.Combine(commonCachePath, applicationInstanceGuid.ToString());

                    // Copy the files to another directory.
                    DirectoryCopy(applicationDir, applicationCachePath);

                    // Remove the readonly flag from all files in the destination directory.
                    ClearReadOnlyFlags(applicationCachePath);

                    // Set our working directory to handle the tools config.
                    Directory.SetCurrentDirectory(applicationDir);
                    if (options.HasOption(OPT_TOOLSCONFIG))
                    {
                        ImportToolsConfig();
                    }

                    // Run the application from the cache directory.
                    Process proc = new Process();
                    proc.StartInfo.FileName = Path.Combine(applicationCachePath, Path.GetFileName(applicationExe));
                    proc.StartInfo.Arguments = String.Join(" ", applicationArguments);
                    proc.StartInfo.WorkingDirectory = applicationDir;
                    proc.Start();
                    proc.WaitForExit();
                    returnCode = proc.ExitCode;

                    // Make sure we clean up after the application has exited.
                    try
                    {
                        Directory.Delete(applicationCachePath, true);
                    }
                    catch
                    {
                        // Ignore any problems that occur when trying to delete the cached copy of the application.
                    }
                } while (returnCode == ExitCode.Restart);
            }
            catch (Exception ex)
            {
                returnCode = ExitCode.Failure;

                RSG.Base.Logging.ExceptionFormatter formatter = new RSG.Base.Logging.ExceptionFormatter(ex);
                String caption = formatter.FormatHeading();
                String message = String.Format("{0}\n\n{1}", ex.Message, String.Join(Environment.NewLine, formatter.Format()));
                System.Windows.Forms.MessageBox.Show(message, caption);
            }

            // Return the bootstrapped apps return code.
            return returnCode;
        }

        /// <summary>
        /// Copies all the contents including sub directories from one folder to another.
        /// </summary>
        /// <param name="sourceDirName"></param>
        /// <param name="destDirName"></param>
        private static void DirectoryCopy(string sourceDirName, string destDirName)
        {
            // Make sure the directory we are going to copy actually exists.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            // If the destination directory doesn't exist, create it. 
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, false);
            }

            // Copy all sub directories and their contents to new location.
            DirectoryInfo[] dirs = dir.GetDirectories();
            foreach (DirectoryInfo subdir in dirs)
            {
                string temppath = Path.Combine(destDirName, subdir.Name);
                DirectoryCopy(subdir.FullName, temppath);
            }
        }

        /// <summary>
        /// Removes the read-only flag from a directory and all of its contents.
        /// </summary>
        /// <param name="directory"></param>
        private static void ClearReadOnlyFlags(string directory)
        {
            DirectoryInfo dirInfo = new DirectoryInfo(directory);
            dirInfo.Attributes &= ~FileAttributes.ReadOnly;

            // Iterate over all files in the directory and sub-directory.
            foreach (FileInfo file in dirInfo.GetFiles("*", SearchOption.AllDirectories))
            {
                file.Attributes &= ~FileAttributes.ReadOnly;
            }
        }

        /// <summary>
        /// Imports the tools config variables into the process-space.
        /// </summary>
        private static void ImportToolsConfig()
        {
            ToolsConfig dynamicConfig = new ToolsConfig();
            Environment.SetEnvironmentVariable("RS_TOOLSROOT", dynamicConfig.ToolsRootDirectory,
                EnvironmentVariableTarget.Process);
        }
    }

} // BootStrapper namespace
