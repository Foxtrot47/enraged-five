﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using RSG.Base.OS;
using System.Xml;
using RSG.Base.Logging.Universal;
using RSG.Base.Logging;
using System.Threading;

namespace RSG.Pipeline.Animation.Coalesce
{
    sealed class Program
    {

        #region Constants
        private static readonly int EXIT_SUCCESS = 0;
        private static readonly int EXIT_ERROR = 1;

        private static readonly String OPTION_COMPRESS_EXE = "compress";
        private static readonly String OPTION_TEMPLATE_PATH = "templatepath";
        private static readonly String OPTION_SKELETON_PATH = "skeletonpath";
        private static readonly String OPTION_INPUTDIR = "inputdir";
        private static readonly String OPTION_OUTPUTDIR = "outputdir";
        private static readonly String OPTION_DEFAULTSKEL = "defaultskeleton";
        private static readonly String OPTION_TASK_NAME = "taskname";
        private static readonly string OPTION_MAX_BLOCK_SIZE = "maxblocksize";
        private static readonly string OPTION_MULTITHREADED = "multithreaded";
        private static readonly String LOG_CTX = "Compress";

        private static readonly Int32 COMPRESS_SETTING_MAXBLOCK = 65536;
        private static readonly Single COMPRESS_SETTING_TRANSLATION = 0.0005f;
        private static readonly Single COMPRESS_SETTING_ROTATION = 0.0005f;
        private static readonly Single COMPRESS_SETTING_SCALE = 0.0005f;
        private static readonly Single COMPRESS_SETTING_DEFAULT = 0.005f;
        private static readonly Int32 COMPRESS_SETTING_COMPRESS_COST = 60;
        private static readonly Int32 COMPRESS_SETTING_DECOMPRESS_COST = 80;


        #endregion // Constants

        static int RunCompressionProcess(string compressExe, string inFile, string skelFilePath, string outFile, string compressionRules, int maxBlockSize, IUniversalLog log)
        {
            String compressArgs = String.Format("-anim={0} -skel={1} -out={2} -maxblocksize={3} -compressionrotation={4}" +
                                                                        " -compressiontranslation={5} -compressionscale={6} -compressiondefault={7}" +
                                                                       " -compressioncost={8} -decompressioncost={9} -compressionrules={10} -nopopups",
                                                                       inFile, skelFilePath, outFile, maxBlockSize, COMPRESS_SETTING_ROTATION,
                                                                       COMPRESS_SETTING_TRANSLATION, COMPRESS_SETTING_SCALE, COMPRESS_SETTING_DEFAULT,
                                                                       COMPRESS_SETTING_COMPRESS_COST, COMPRESS_SETTING_DECOMPRESS_COST, compressionRules);

            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.Arguments = compressArgs;
            startInfo.FileName = compressExe;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.CreateNoWindow = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false;

            Process proc = Process.Start(startInfo);
            String output = proc.StandardOutput.ReadToEnd();
            proc.WaitForExit();
            if(output != String.Empty)
                log.MessageCtx(LOG_CTX, output);

            log.MessageCtx(LOG_CTX, "{0} {1}", compressExe, compressArgs);

            return proc.ExitCode;
        }

        public class CompressionEntry
        {
            public CompressionEntry(string compressExe, string inFile, string skelFilePath, string outFile, string compressionRules, int maxBlockSizeValue, IUniversalLog log)
            {
                _compressExe = compressExe;
                _inFile = inFile;
                _skelFilePath = skelFilePath;
                _outFile = outFile;
                _compressionRules = compressionRules;
                _maxBlockSizeValue = maxBlockSizeValue;
                _log = log;
            }

            public string _compressExe;
            public string _inFile;
            public string _skelFilePath;
            public string _outFile;
            public string _compressionRules;
            public int _maxBlockSizeValue;
            public IUniversalLog _log;
        }

        private static List<Thread> ActiveThreads;
        private static int MaximumThreads;
        private static bool ProcessRunsSuccess;
        private static bool MultiThreaded;

        //Threading
        public static void ProcessCompressionThread(object run)
        {
            CompressionEntry c = (CompressionEntry)run;
            int processResult = RunCompressionProcess(c._compressExe, c._inFile, c._skelFilePath, c._outFile, c._compressionRules, c._maxBlockSizeValue, c._log);
            if (processResult != 0)
            {
                ProcessRunsSuccess = false;
            }
        }

        public static int CompressAnimations(List<CompressionEntry> compressionEntries)
        {
            if (MultiThreaded)
            {
                int currentThreadIndex = 0;
                ProcessRunsSuccess = true;
                ActiveThreads.Clear();

                while (currentThreadIndex < compressionEntries.Count)
                {
                    for (int threadIndex = 0; threadIndex < ActiveThreads.Count; ++threadIndex)
                    {
                        Thread currentThread = ActiveThreads[threadIndex];
                        if (currentThread.IsAlive == false)
                        {
                            //Remove this from the list.
                            ActiveThreads.RemoveAt(threadIndex);
                            threadIndex--;
                        }
                    }

                    while (ActiveThreads.Count < MaximumThreads && currentThreadIndex < compressionEntries.Count)
                    {
                        Thread newRunThread = new Thread(ProcessCompressionThread);
                        //newRunThread.SetApartmentState(ApartmentState.STA);
                        newRunThread.Name = "Process Run Thread " + currentThreadIndex;
                        ActiveThreads.Add(newRunThread);

                        CompressionEntry comp = (CompressionEntry)compressionEntries[currentThreadIndex];
                        Console.WriteLine("Compressing: {0} - {1} / {2}", Path.GetFileName(comp._inFile), (currentThreadIndex + 1), compressionEntries.Count);

                        newRunThread.Start(comp);
                        currentThreadIndex++;
                    }
                }

                foreach (Thread runThread in ActiveThreads)
                {
                    runThread.Join();
                }

                if (!ProcessRunsSuccess)
                {
                    return -1;
                }
            }
            else
            {
                foreach (CompressionEntry entry in compressionEntries)
                {
                    if (RunCompressionProcess(entry._compressExe, entry._inFile, entry._skelFilePath, entry._outFile, entry._compressionRules, entry._maxBlockSizeValue, entry._log) != 0) return -1;
                }
            }

            return 0;
        }

        [STAThread]
        static int Main(string[] args)
        {
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.CreateUniversalLog("Compress");

            int exit_code = EXIT_SUCCESS;

            try
            {
                // Define our executable options.  Stick with the long options
                // (e.g. --opt) rather than the single character versions.
                Getopt options = new Getopt(args, new LongOption[] {
                    new LongOption(OPTION_COMPRESS_EXE, LongOption.ArgType.Required,
                        "compress filename."),
                    new LongOption(OPTION_TEMPLATE_PATH, LongOption.ArgType.Required,
                        ""),
                    new LongOption(OPTION_SKELETON_PATH, LongOption.ArgType.Required,
                        ""),
                    new LongOption(OPTION_INPUTDIR, LongOption.ArgType.Required,
                        "Input directory."),
                    new LongOption(OPTION_OUTPUTDIR, LongOption.ArgType.Required,
                        "Output directory."),
                    new LongOption(OPTION_DEFAULTSKEL, LongOption.ArgType.Required,
                        "Default skeleton filename."),
                    new LongOption(OPTION_TASK_NAME, LongOption.ArgType.Required,
                        "Asset Pipeline 3 Task Name."),
                    new LongOption(OPTION_MAX_BLOCK_SIZE, LongOption.ArgType.Required,
                        ""),
                    new LongOption(OPTION_MULTITHREADED, LongOption.ArgType.Required,
                        ""),
                });
                // Turn args into vars
                String compressExe = options[OPTION_COMPRESS_EXE] as String;
                String inputDirectory = options[OPTION_INPUTDIR] as String;
                String outputDirectory = options[OPTION_OUTPUTDIR] as String;
                String templatePath = options[OPTION_TEMPLATE_PATH] as String;
                String skeletonPath = options[OPTION_SKELETON_PATH] as String;
                String defaultSkeleton = options[OPTION_DEFAULTSKEL] as String;
                String maxBlockSize = options[OPTION_MAX_BLOCK_SIZE] as String;
                String taskName = options[OPTION_TASK_NAME] == null ? String.Empty : options[OPTION_TASK_NAME] as String;
                MultiThreaded = (String.Compare(options[OPTION_MULTITHREADED] as String, "true", true) == 0);

                if (String.Empty != taskName)
                {
                    Console.WriteLine("TASK: {0}", taskName);
                    log.MessageCtx(LOG_CTX, "TASK: {0}", taskName);
                }

                ActiveThreads = new List<Thread>();
                MaximumThreads = Environment.ProcessorCount + 2;

                log.MessageCtx(LOG_CTX, "MultiThreading : {0} ({1} max threads)", options[OPTION_MULTITHREADED] as String, MaximumThreads.ToString());

                log.MessageCtx(LOG_CTX, "Compressing '{0}' to '{1}'", inputDirectory, outputDirectory);
                if (!Directory.Exists(inputDirectory))
                {
                    log.ErrorCtx(LOG_CTX, "Coalesce pool missing from cache '{0}'.", inputDirectory);
                    exit_code = EXIT_ERROR;
                    return (exit_code);
                }

                int maxBlockSizeValue = COMPRESS_SETTING_MAXBLOCK;
                if (maxBlockSize != null)
                {
                    if (int.TryParse(maxBlockSize, out maxBlockSizeValue) == false)
                    {
                        log.ErrorCtx(LOG_CTX, "Max block size argumment is not a valid integer: '{0}'.", options[OPTION_MAX_BLOCK_SIZE]);
                        exit_code = EXIT_ERROR;
                        return (exit_code);
                    }
                }

                String _clipListFile = Path.Combine(inputDirectory, "cliplists.xml");

                List<string> alCompressionArray = new List<string>();
                List<string> alAdditiveArray = new List<string>();
                List<string> alSkeletonArray = new List<string>();
                List<List<string>> alClipsArray = new List<List<string>>();

                if (File.Exists(_clipListFile))
                {
                    var xmlDoc = new XmlDocument();
                    xmlDoc.Load(_clipListFile);

                    XmlNodeList compressionNodes = xmlDoc.SelectNodes("/Lists/List/Compression");
                    XmlNodeList additiveNodes = xmlDoc.SelectNodes("/Lists/List/Additive");
                    XmlNodeList skeletonNodes = xmlDoc.SelectNodes("/Lists/List/Skeleton");
                    XmlNodeList clipsNodes = xmlDoc.SelectNodes("/Lists/List/Clips");
                    foreach (XmlNode n in compressionNodes) alCompressionArray.Add(n.InnerText);
                    foreach (XmlNode n in additiveNodes) alAdditiveArray.Add(n.InnerText);
                    foreach (XmlNode n in skeletonNodes) alSkeletonArray.Add(n.InnerText);
                    for (int i = 0; i < clipsNodes.Count; ++i)
                    {
                        alClipsArray.Add(new List<string>());
                        XmlNode n = clipsNodes[i];
                        foreach (XmlNode c in n.ChildNodes)
                        {
                            alClipsArray[i].Add(c.InnerText);
                        }
                    }
                }

                //if (!Directory.Exists(outputDirectory))
                //    Directory.CreateDirectory(outputDirectory);

                if (alClipsArray.Count > 0)
                {
                    for (int i = 0; i < alClipsArray.Count; ++i)
                    {
                        String compressionFilePath = Path.Combine(templatePath, alCompressionArray[i] + ".txt");
                        String skelFilePath = (alSkeletonArray[i] == String.Empty) ? defaultSkeleton : Path.Combine(skeletonPath, alSkeletonArray[i] + ".skel");
                        bool additive = Convert.ToBoolean(alAdditiveArray[i]);

                        if (!File.Exists(compressionFilePath))
                        {
                            log.ErrorCtx(LOG_CTX, "Couldn't find compression file '{0}'.", compressionFilePath);
                            exit_code = EXIT_ERROR;
                            return (exit_code);
                        }

                        // Extract the compression settings from the clip list name
                        String compressionRules = "";
                        using (FileStream templateFileStream = new FileStream(compressionFilePath, FileMode.Open, FileAccess.Read))
                        {
                            using (StreamReader sr = new StreamReader(templateFileStream))
                            {
                                while (!sr.EndOfStream)
                                {
                                    compressionRules += sr.ReadLine().Trim();
                                }
                            }
                        }

                        List<String> animFileList = new List<String>();
                        List<String> clipFileList = new List<String>();

                        foreach (string clip in alClipsArray[i])
                        {
                            animFileList.Add(Path.ChangeExtension(clip, ".anim"));
                            clipFileList.Add(clip);
                        }

                        if (animFileList.Count == 0)
                        {
                            log.ErrorCtx(LOG_CTX, "Nothing to compress from '{0}'.", inputDirectory);
                            exit_code = EXIT_ERROR;
                            return (exit_code);
                        }

                        List<CompressionEntry> compressionEntries = new List<CompressionEntry>();
                        foreach (String inFile in animFileList)
                        {
                            String outFile = Path.GetFileName(inFile);
                            outFile = Path.Combine(outputDirectory, outFile);

                            CompressionEntry entry = new CompressionEntry(compressExe, inFile, skelFilePath, outFile, compressionRules, maxBlockSizeValue, log);
                            compressionEntries.Add(entry);
                        }

                        if (CompressAnimations(compressionEntries) != 0) return EXIT_ERROR;

                        // Copy all clip files to the location of the newly compressed animations
                        foreach (String clipFile in clipFileList)
                        {
                            String destClipFile = Path.GetFileName(clipFile);
                            destClipFile = Path.Combine(outputDirectory, destClipFile);
                            File.Delete(destClipFile);
                            File.Copy(clipFile, destClipFile);
                        }
                    }
                }
                else
                {
                    String[] compressDirs = Directory.GetDirectories(inputDirectory);
                    // Error if we aren't actually processing anything
                    if (compressDirs.Length == 0)
                    {
                        log.ErrorCtx(LOG_CTX, "No directories under '{0}' to compress from.", inputDirectory);
                        exit_code = EXIT_ERROR;
                        return (exit_code);
                    }
                    foreach (String compressDir in compressDirs)
                    {
                        String compressSettingsString = Path.GetFileName(compressDir);
                        String[] compressSettingsTokens = compressSettingsString.Split('@');

                        String compressionFilePath = Path.Combine(templatePath, compressSettingsTokens[0] + ".txt");
                        String skelFilePath = (compressSettingsTokens[1] == String.Empty) ? defaultSkeleton : Path.Combine(skeletonPath, compressSettingsTokens[1] + ".skel");
                        Int32 additive = Convert.ToInt32(compressSettingsTokens[2]);

                        // Extract the compression settings from the clip list name
                        String compressionRules = "";
                        using (FileStream templateFileStream = new FileStream(compressionFilePath, FileMode.Open, FileAccess.Read))
                        {
                            using (StreamReader sr = new StreamReader(templateFileStream))
                            {
                                while (!sr.EndOfStream)
                                {
                                    compressionRules += sr.ReadLine().Trim();
                                }
                            }
                        }

                        if (!Directory.Exists(outputDirectory))
                            Directory.CreateDirectory(outputDirectory);

                        // Loop through anims compressing with our assembled compression setting string
                        String[] animList = Directory.GetFiles(compressDir, "*.anim");
                        // Error if we aren't actually processing anything
                        if (animList.Length == 0)
                        {
                            log.ErrorCtx(LOG_CTX, "Nothing to compress from '{0}'.", compressDir);
                            exit_code = EXIT_ERROR;
                            return (exit_code);
                        }

                        List<CompressionEntry> compressionEntries = new List<CompressionEntry>();
                        foreach (String inFile in animList)
                        {
                            String outFile = Path.GetFileName(inFile);
                            outFile = Path.Combine(outputDirectory, outFile);

                            CompressionEntry entry = new CompressionEntry(compressExe, inFile, skelFilePath, outFile, compressionRules, maxBlockSizeValue, log);
                            compressionEntries.Add(entry);
                        }

                        if (CompressAnimations(compressionEntries) != 0) return EXIT_ERROR;

                        // Copy all clip files to the location of the newly compressed animations
                        String[] clipFiles = Directory.GetFiles(compressDir, "*.clip");
                        foreach (String clipFile in clipFiles)
                        {
                            String destClipFile = Path.GetFileName(clipFile);
                            destClipFile = Path.Combine(outputDirectory, destClipFile);
                            File.Delete(destClipFile);
                            File.Copy(clipFile, destClipFile);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Unhandled Exception during animation compression");
                exit_code = EXIT_ERROR;
            }
            finally
            {
                LogFactory.ApplicationShutdown();
            }

            return (exit_code);
        }
    }
}
