﻿// 
// A tool to set the console settings for Bugstar Streaming Video.
//

// Using //////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

// Namespace //////////////////////////////////////////////////////////////////////////////////////

namespace BugstarStreamingVideo 
{

// Class //////////////////////////////////////////////////////////////////////////////////////////

class Program
{

// Private Static Methods /////////////////////////////////////////////////////////////////////////

	private static int Main(string[] args)
	{
		int errorCode;

		try
		{
			errorCode = Run(args);
		}
		catch (Exception ex)
		{
			Console.Error.WriteLine("Error: Caught exception:\n{0}", ex);
			return -1;
		}

		return errorCode;
	}

	private static int Run(string[] args)
	{
		string[] commands;

		if (ParseArguments(args, out _optionsDict, out commands) == false)
		{
			ShowHelp();
			Console.Error.WriteLine("Error: Invalid arguments.");
			return -1;
		}

		if (commands.Any() == false)
		{
			ShowHelp();
			Console.Error.WriteLine("Error: No commands specified.");
			return -1;
		}

		foreach (string helpOption in _helpOptions)
		{
			if (_optionsDict.ContainsKey(helpOption) == true)
			{
				ShowHelp();
				return 0;
			}
		}

		bool? isEnabled = null;

		foreach (string command in commands)
		{

			switch (command)
			{

			case "enable":
				{
					isEnabled = true;
				}
				break;

			case "disable":
				{
					isEnabled = false;
				}
				break;
			}
		}

		if (isEnabled != null)
		{
			GameSettings gameSettings = new GameSettings();

			if (System.IO.Directory.Exists(_directoryRoot) == false)
			{
				System.IO.Directory.CreateDirectory(_directoryRoot);
			}

			if (isEnabled.Value == true)
			{
				Console.WriteLine("Setting Orbis video streaming settings...");
			}
			else
			{
				Console.WriteLine("Clearing Orbis video streaming settings...");
			}

			string orbisToolFilePath = System.IO.Path.Combine(_directoryRoot, "ToolOrbisSettings.xml");

			if (SetStreamingVideoSettingsOrbis(isEnabled.Value, gameSettings, orbisToolFilePath) == false)
			{
				return -1;
			}

			string settingsFilePath = System.IO.Path.Combine(_directoryRoot, "Settings.xml");

			if (isEnabled.Value == true)
			{
				Console.WriteLine("Saving game settings...");

				if (gameSettings.Save(settingsFilePath) == false)
				{ 
					return -2;
				}
			}
			else 
			{
				Console.WriteLine("Deleting game settings...");

				System.IO.File.Delete(settingsFilePath);
			}
		}

		Console.WriteLine("Finished.");

		return 0;
	}

	private static void ShowHelp()
	{
		Console.WriteLine(string.Format("Syntax: BugstarStreamingVideo [{0}] [-uri <base uri>] [-noreboot] [enable|disable]", string.Join("|", _helpOptions)));
	}

	private static bool ParseArguments(string[] arguments, out Dictionary<string, string> optionsDict, out string[] commands)
	{
		optionsDict = new Dictionary<string, string>();

		List<string> commandList = new List<string>();

		for (int argIndex = 0; argIndex < arguments.Length; ++argIndex)
		{
			string argument = arguments[argIndex];

			if (argument.StartsWith("-") == false)
			{
				commandList.Add(argument);
				continue;
			}

			argument = argument.TrimStart(new char[]{'-'});

			string[] split = argument.Split(new char[]{'='});

			switch (split.Length)
			{
				case 1:
					optionsDict.Add(split[0], null);
					break;

				case 2:
					optionsDict.Add(split[0], split[1]);
					break;

				default:
					commands = new string[] { };

					return false;
			}
		}

		commands = commandList.ToArray();

		return true;
	}

	private static int RunCommand(string command)
	{
		ProcessStartInfo startInfo = new ProcessStartInfo();

		string[] commandParts = command.Split(new char[] {' ', '\t'});

		string filename = commandParts[0];

		string arguments = command.Remove(0, filename.Length + 1);

		startInfo.CreateNoWindow			= true;
		startInfo.UseShellExecute			= false;
		startInfo.FileName					= filename;
		startInfo.RedirectStandardOutput	= true;
		startInfo.Arguments					= arguments;

		Process process = new Process();

		process.StartInfo = startInfo;
		process.EnableRaisingEvents = true;

		process.OutputDataReceived += new DataReceivedEventHandler
		(
			delegate(object sender, DataReceivedEventArgs e)
			{
				if (e.Data != null)
				{ 
					Console.WriteLine("{0}", e.Data.Replace("\n", "\r\n"));
				}
			}
		);

		int exitCode = 0;

		try
		{
			process.Start();
			process.BeginOutputReadLine();
			process.WaitForExit();

			exitCode = process.ExitCode;
		}
		catch (System.Exception ex)
		{
			Console.Error.WriteLine("Error: Failed to run '{0}'. Caught exception {1}.", command, ex);

			return -1;
		}

		return exitCode;
	}

	private static void AddSettingOrbis(
		XmlDocument xmlDoc,
		XmlElement	parentNode,
		string		key, 
		string		dest, 
		string		size, 
		string		type, 
		string		value, 
		string		eng)
	{
		XmlElement element = xmlDoc.CreateElement("setting");

		element.SetAttribute("key",		key);
		element.SetAttribute("dest",	dest);
		element.SetAttribute("size",	size);
		element.SetAttribute("type",	type);
		element.SetAttribute("value",	value);
		element.SetAttribute("eng",		eng);

		parentNode.AppendChild(element);
	}

	private static bool SetStreamingVideoSettingsOrbis(bool enabled, GameSettings gameSettings, string filePath)
	{
		XmlDocument xmlDoc = new XmlDocument();

		XmlNode declarationNode = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);

		xmlDoc.AppendChild(declarationNode);

		XmlElement settingsNode = xmlDoc.CreateElement("settings");

		settingsNode.SetAttribute("version", "1");
		settingsNode.SetAttribute("target", "orbis");

		xmlDoc.AppendChild(settingsNode);

		string liveStreamingMode;
		string socialFeedbackMode;
		string liveQualityDebug;

		if (enabled == true)
		{
			liveStreamingMode	= "0x0";
			socialFeedbackMode	= "0x0";
			liveQualityDebug	= "0x5";
		}
		else
		{
			liveStreamingMode	= "0x2";
			socialFeedbackMode	= "0x1";
			liveQualityDebug	= "0x1";
		}

		string baseUri;

		if (_optionsDict.TryGetValue("uri", out baseUri) == true)
		{
			_baseUri = baseUri;
		}

		gameSettings.BaseUri = _baseUri;

		AddSettingOrbis(xmlDoc, settingsNode, "0x32010000", "0x00",   "4", "0x0", liveStreamingMode,	"Live Streaming Mode");
		AddSettingOrbis(xmlDoc, settingsNode, "0x32020000", "0x00",   "4", "0x0", socialFeedbackMode,	"Social Feedback Mode");
		AddSettingOrbis(xmlDoc, settingsNode, "0x32890000", "0x00",   "4", "0x0", liveQualityDebug,		"Live Quality: Debug");

		xmlDoc.Save(filePath);

		string orbisExe = "orbis-ctrl.exe";

		string commandline = string.Format("{0} settings-import \"{1}\"", orbisExe, filePath);

		int commandResult = RunCommand(commandline);

		if (commandResult != 0) 
		{
			Console.Error.WriteLine("Error: Received error code {0} when running command:'{1}'", commandResult, commandline);
			return false;
		}

		if (enabled == true)
		{
			Console.WriteLine("Set Orbis streaming video settings.\n");
		}
		else
		{
			Console.WriteLine("Cleared streaming video settings.");
		}

		if (_optionsDict.ContainsKey("noreboot") == false)
		{
			Console.WriteLine("Rebooting Orbis...");

			commandResult = RunCommand(string.Format("{0} reboot", orbisExe));

			if (commandResult != 0) 
			{
				Console.Error.WriteLine("Error: Received error code {0} when running command:'{1}'", commandResult, commandline);
				return false;
			}
		}

		return true;
	}

// Private Static Members /////////////////////////////////////////////////////////////////////////

	private static readonly string	_directoryRoot = "x:\\BugstarStreamingVideo";

	private static readonly string[] _helpOptions = 
		{
			"--help",
			"-help",
			"/?",
		};

	private static string _baseUri = "rtmp://rsgfms.rockstar.t2.corp/bugliverecordbufferoverlapped";

	private static Dictionary<string, string>	_optionsDict;

///////////////////////////////////////////////////////////////////////////////////////////////////

} // Class

///////////////////////////////////////////////////////////////////////////////////////////////////

} // Namespace



