﻿
// Using //////////////////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

// Namespace //////////////////////////////////////////////////////////////////////////////////////

namespace BugstarStreamingVideo 
{

// Class //////////////////////////////////////////////////////////////////////////////////////////

class GameSettings
{

// Public Methods /////////////////////////////////////////////////////////////////////////////////

	public bool Save(string filePath)
	{
		XmlDocument xmlDoc = new XmlDocument();

		XmlNode declarationNode = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);

		xmlDoc.AppendChild(declarationNode);

		XmlElement rootElement = xmlDoc.CreateElement("BugstarStreamingVideo");

		xmlDoc.AppendChild(rootElement);

		Utils.AddXmlElement(xmlDoc, rootElement, "BaseUri", BaseUri);

		try
		{
			xmlDoc.Save(filePath);
		}
		catch (System.Exception ex)
		{
			Console.Error.WriteLine("Error: Failed to save {0}. Caught exception {1}.", filePath, ex);

			return false;
		}

		return true;
	}

// Public Properties //////////////////////////////////////////////////////////////////////////////

	public string BaseUri { get; set; }


///////////////////////////////////////////////////////////////////////////////////////////////////

} // Class

	///////////////////////////////////////////////////////////////////////////////////////////////////

} // Namespace



