﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using System.Web.UI;

namespace DialogueDiffCreator
{
    class Program
    {
        private class DialogueSection
        {
            public List<KeyValuePair<string, string>> Pairs = new List<KeyValuePair<string, string>>();
        }

        private class DialogueFile
        {
            public List<string> StartBlock = new List<string>();
            public Dictionary<string, DialogueSection> Sections = new Dictionary<string, DialogueSection>();

            public void AddPair(KeyValuePair<string, string> pair)
            {
                string sectionKey = pair.Key;
                string rawSectionKey = pair.Key;
                if (pair.Key.Contains(':'))
                {
                    rawSectionKey = pair.Key.Substring(0, pair.Key.IndexOf(':'));
                }

                sectionKey = rawSectionKey;
                if (sectionKey.EndsWith("IF") || sectionKey.EndsWith("SL") || sectionKey.EndsWith("LF"))
                {
                    sectionKey = sectionKey.Substring(0, sectionKey.Length - 2);
                    DialogueSection section = null;
                    if (Sections.TryGetValue(sectionKey, out section))
                    {
                        section.Pairs.Add(pair);
                        return;
                    }
                }

                sectionKey = rawSectionKey;
                if (sectionKey.Where(c => c == '_').Count() >= 2)
                {
                    sectionKey = sectionKey.Substring(0, sectionKey.LastIndexOf('_'));
                }

                {
                    DialogueSection section = null;
                    if (!Sections.TryGetValue(sectionKey, out section))
                    {
                        section = new DialogueSection();
                        this.Sections.Add(sectionKey, section);
                    }

                    section.Pairs.Add(pair);
                }
            }

            public bool FindPair(string sectionKey, string pairKey, out KeyValuePair<string, string> pair)
            {
                pair = new KeyValuePair<string, string>();

                DialogueSection section;
                if (!this.Sections.TryGetValue(sectionKey, out section))
                {
                    return false;
                }

                foreach (KeyValuePair<string, string> kvp in section.Pairs)
                {
                    if (String.Equals(kvp.Key, pairKey))
                    {
                        pair = kvp;
                        return true;
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// Local log.
        /// </summary>
        private static IUniversalLog s_log;

        static void Main(string[] args)
        {
            s_log = LogFactory.CreateUniversalLog("ReportGenerator");
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLogTarget logfile = LogFactory.CreateUniversalLogFile(s_log);

            Getopt options = new Getopt(args, new LongOption[] {
                new LongOption("original", LongOption.ArgType.Required, 'o'),
                new LongOption("updated", LongOption.ArgType.Required, 'u'),
                new LongOption("output", LongOption.ArgType.Required, 'r'),
            });

            string originalPath = options["original"] as string;
            string updatedPath = options["updated"] as string;
            string outputPath = options["output"] as string;
            string[] originalFiles = new string[0];
            string[] updatedFiles = new string[0];

            if (!Directory.Exists(originalPath))
            {
                s_log.Error("Provided original directory path doesn't exist. Aborting.");
                System.Environment.Exit(2);
            }
            else
            {
                originalFiles = Directory.GetFiles(originalPath, "*.txt");
            }

            if (!Directory.Exists(updatedPath))
            {
                s_log.Error("Provided updated directory path doesn't exist. Aborting.");
                System.Environment.Exit(2);
            }
            else
            {
                updatedFiles = Directory.GetFiles(updatedPath, "*.txt");
            }

            if (String.IsNullOrWhiteSpace(outputPath))
            {
                s_log.Error("Provided output path isn't valid. Aborting.");
                System.Environment.Exit(2);
            }
            else
            {
                if (!Directory.Exists(outputPath))
                {
                    Directory.CreateDirectory(outputPath);
                }
            }

            var updatedDialogueFiles = new Dictionary<string, DialogueFile>();
            var originalDialogueFiles = new Dictionary<string, DialogueFile>();
            foreach (string updatedFile in updatedFiles)
            {
                string name = Path.GetFileNameWithoutExtension(updatedFile);
                updatedDialogueFiles.Add(name, GetSections(updatedFile));
                DialogueFile orginalDialogueFile = new DialogueFile();
                foreach (string originalFile in originalFiles)
                {
                    string originalFileName = Path.GetFileNameWithoutExtension(originalFile);
                    if (!String.Equals(originalFileName, name, StringComparison.OrdinalIgnoreCase))
                    {
                        continue;
                    }

                    orginalDialogueFile = GetSections(originalFile);
                    break;
                }

                originalDialogueFiles.Add(name, orginalDialogueFile);
            }


            foreach (string filename in updatedFiles)
            {
                MemoryStream stream = new MemoryStream();
                StreamWriter reportFile = new StreamWriter(stream);

                HtmlTextWriter writer = new HtmlTextWriter(reportFile);
                writer.RenderBeginTag(HtmlTextWriterTag.Html);
                writer.RenderBeginTag(HtmlTextWriterTag.Head);
                writer.RenderEndTag();
                writer.RenderBeginTag(HtmlTextWriterTag.Body);

                writer.AddAttribute("style", "font-size: 36px;Display: block;font-weight: normal;font-family: 'Segoe UI';font-variant: normal;line-height: 42px;");
                writer.RenderBeginTag(HtmlTextWriterTag.H1);
                writer.Write("Dialogue Text File Differences");
                writer.RenderEndTag();

                writer.AddAttribute("style", "font-size: 23;font-family: Segoe UI Semibold;display: block;padding-bottom: 19px;padding-top: 19px");
                writer.RenderBeginTag(HtmlTextWriterTag.Span);
                writer.Write(Path.GetFileName(filename));
                writer.RenderEndTag();

                writer.AddAttribute(HtmlTextWriterAttribute.Border, "1px solid");
                writer.AddAttribute(HtmlTextWriterAttribute.Bordercolor, "#bbb");
                writer.AddAttribute(HtmlTextWriterAttribute.Width, "90%");
                writer.AddAttribute("style", "border-collapse: collapse;");
                writer.RenderBeginTag(HtmlTextWriterTag.Table);

                writer.RenderBeginTag(HtmlTextWriterTag.Tr);
                writer.RenderBeginTag(HtmlTextWriterTag.Tbody);

                writer.AddAttribute(HtmlTextWriterAttribute.Bgcolor, "#ededed");
                writer.AddAttribute(HtmlTextWriterAttribute.Align, "left");
                writer.AddAttribute("style", "color: #707070;font-size: 13;font-family: Segoe UI;padding-left: 8px;padding-top: 10px;padding-bottom: 10px;padding-right: 8px;");
                writer.RenderBeginTag(HtmlTextWriterTag.Th);
                writer.Write("Submission Data");
                writer.RenderEndTag();

                writer.AddAttribute(HtmlTextWriterAttribute.Bgcolor, "#ededed");
                writer.AddAttribute(HtmlTextWriterAttribute.Width, "33%");
                writer.AddAttribute(HtmlTextWriterAttribute.Align, "left");
                writer.AddAttribute("style", "color: #707070;font-size: 13;font-family: Segoe UI;padding-left: 8px;padding-top: 10px;padding-bottom: 10px;padding-right: 8px;");
                writer.RenderBeginTag(HtmlTextWriterTag.Th);
                writer.Write("Current Title Update Data");
                writer.RenderEndTag();

                writer.AddAttribute(HtmlTextWriterAttribute.Bgcolor, "#ededed");
                writer.AddAttribute(HtmlTextWriterAttribute.Align, "left");
                writer.AddAttribute("style", "color: #707070;font-size: 13;font-family: Segoe UI;padding-left: 8px;padding-top: 10px;padding-bottom: 10px;padding-right: 8px;");
                writer.RenderBeginTag(HtmlTextWriterTag.Th);
                writer.Write("Desired Data");
                writer.RenderEndTag();
                writer.RenderEndTag();

                DialogueFile original = null;
                DialogueFile updated = null;
                string key = Path.GetFileNameWithoutExtension(filename);
                if (!originalDialogueFiles.TryGetValue(key, out original))
                {
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                    continue;
                }

                if (!updatedDialogueFiles.TryGetValue(key, out updated))
                {
                    writer.RenderEndTag();
                    writer.RenderEndTag();
                    continue;
                }

                foreach (KeyValuePair<string, DialogueSection> section in original.Sections)
                {
                    foreach (KeyValuePair<string, string> pair in section.Value.Pairs)
                    {
                        writer.AddAttribute("style", "font-size: 13;font-family: Segoe UI;");
                        writer.RenderBeginTag(HtmlTextWriterTag.Tr);

                        writer.AddAttribute("style", "font-size: 13;font-family: Segoe UI;padding-left: 8px;padding-top: 10px;padding-bottom: 10px;padding-right: 8px;");
                        writer.RenderBeginTag(HtmlTextWriterTag.Td);
                        writer.Write(String.Format("[{0}] {1}", pair.Key, pair.Value));
                        writer.RenderEndTag();

                        KeyValuePair<string, string> updatedPair = new KeyValuePair<string,string>();
                        if (updated.FindPair(section.Key, pair.Key, out updatedPair))
                        {
                            writer.AddAttribute("style", "font-size: 13;font-family: Segoe UI;padding-left: 8px;padding-top: 10px;padding-bottom: 10px;padding-right: 8px;");
                            writer.RenderBeginTag(HtmlTextWriterTag.Td);
                            writer.Write(String.Format("[{0}] {1}", updatedPair.Key, updatedPair.Value));
                            writer.RenderEndTag();

                            if (String.Equals(pair.Value, updatedPair.Value) && !pair.Key.StartsWith("DUMMY"))
                            {
                                writer.AddAttribute("style", "font-size: 13;font-family: Segoe UI;padding-left: 8px;padding-top: 10px;padding-bottom: 10px;padding-right: 8px;");
                                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                                writer.Write("-");
                                writer.RenderEndTag();
                            }
                            else
                            {
                                writer.AddAttribute("style", "font-size: 13;font-family: Segoe UI;padding-left: 8px;padding-top: 10px;padding-bottom: 10px;padding-right: 8px;");
                                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                                writer.Write(String.Format("[{0}] {1}", updatedPair.Key, updatedPair.Value));
                                writer.RenderEndTag();
                            }
                        }
                        else
                        {
                            writer.AddAttribute("style", "font-size: 13;font-family: Segoe UI;padding-left: 8px;padding-top: 10px;padding-bottom: 10px;padding-right: 8px;");
                            writer.RenderBeginTag(HtmlTextWriterTag.Td);
                            writer.Write("-");
                            writer.RenderEndTag();

                            if (pair.Key.StartsWith("DUMMY"))
                            {
                                writer.AddAttribute("style", "font-size: 13;font-family: Segoe UI;padding-left: 8px;padding-top: 10px;padding-bottom: 10px;padding-right: 8px;");
                                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                                writer.Write(String.Format("[{0}] {1}", pair.Key, pair.Value));
                                writer.RenderEndTag();
                            }
                            else
                            {
                                writer.AddAttribute("style", "font-size: 13;font-family: Segoe UI;padding-left: 8px;padding-top: 10px;padding-bottom: 10px;padding-right: 8px;");
                                writer.RenderBeginTag(HtmlTextWriterTag.Td);
                                writer.Write("-");
                                writer.RenderEndTag();
                            }
                        }

                        writer.RenderEndTag();
                    }
                }

                writer.RenderEndTag();
                writer.RenderEndTag();
                writer.RenderEndTag();
                writer.RenderEndTag();
                writer.Flush();

                stream.Position = 0;
                string realPath = Path.Combine(outputPath, Path.GetFileNameWithoutExtension(filename) + "_table.html");
                using (FileStream file = new FileStream(realPath, FileMode.Create, FileAccess.Write))
                {
                    byte[] bytes = new byte[stream.Length];
                    stream.Read(bytes, 0, (int)stream.Length);
                    file.Write(bytes, 0, bytes.Length);
                    stream.Close();
                }
            }

            foreach (string filename in updatedFiles)
            {
                MemoryStream stream = new MemoryStream();
                TextWriter writer = new StreamWriter(stream);

                DialogueFile original = null;
                DialogueFile updated = null;
                string key = Path.GetFileNameWithoutExtension(filename);
                if (!originalDialogueFiles.TryGetValue(key, out original))
                {
                    continue;
                }

                if (!updatedDialogueFiles.TryGetValue(key, out updated))
                {
                    continue;
                }

                bool hasDummy = false;
                foreach (KeyValuePair<string, DialogueSection> section in updated.Sections)
                {
                    int count = 0;
                    foreach (KeyValuePair<string, string> updatedPair in section.Value.Pairs)
                    {
                        bool output = true;
                        KeyValuePair<string, string> originalPair = new KeyValuePair<string, string>();

                        if (original.FindPair(section.Key, updatedPair.Key, out originalPair))
                        {
                            if (String.Equals(updatedPair.Value, originalPair.Value) && !originalPair.Key.StartsWith("DUMMY"))
                            {
                                output = false;
                            }
                        }
                        
                        if (output)
                        {
                            if (originalPair.Key != null && originalPair.Key.StartsWith("DUMMY"))
                            {
                                hasDummy = true;
                            }

                            count++;
                            writer.WriteLine("[{0}]", updatedPair.Key);
                            writer.WriteLine(updatedPair.Value);
                        }
                    }

                    if (count > 0)
                    {
                        writer.WriteLine();
                    }
                }


                if (!hasDummy)
                {
                    foreach (KeyValuePair<string, DialogueSection> section in original.Sections)
                    {
                        foreach (KeyValuePair<string, string> originalPair in section.Value.Pairs)
                        {
                            if (originalPair.Key.StartsWith("DUMMY"))
                            {
                                writer.WriteLine("[{0}]", originalPair.Key);
                                writer.WriteLine(originalPair.Value);
                            }
                        }
                    }
                }

                writer.Flush();
                stream.Position = 0;
                string realPath = Path.Combine(outputPath, Path.GetFileName(filename));
                using (FileStream file = new FileStream(realPath, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
                {
                    byte[] bytes = new byte[stream.Length];
                    stream.Read(bytes, 0, (int)stream.Length);
                    file.Write(bytes, 0, bytes.Length);
                    stream.Close();
                }
            }
        }

        private static DialogueFile GetSections(string filename)
        {
            if (!File.Exists(filename))
            {
                return new DialogueFile();
            }

            bool inStartBlock = false;
            string currentKey = null;
            List<string> startBlock = new List<string>();
            char[] trim = new char[] { '[', ']' };
            DialogueFile file = new DialogueFile();
            using (Stream s = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                //KeyValueList kvpList = null;
                using (TextReader reader = new StreamReader(s))
                {
                    string line = String.Empty;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (String.IsNullOrWhiteSpace(line))
                        {
                            continue;
                        }
                        else if (line.StartsWith("{"))
                        {
                            // comment
                            continue;
                        }
                        else if (line.StartsWith("["))
                        {
                            if (inStartBlock)
                            {
                                file.StartBlock.Add(line);
                                continue;
                            }
                            
                             currentKey = line.Trim(trim);
                        }
                        else if (String.Equals(line, "start"))
                        {
                            inStartBlock = true;
                        }
                        else if (String.Equals(line, "end"))
                        {
                            inStartBlock = false;
                        }
                        else
                        {
                            if (inStartBlock)
                            {
                                file.StartBlock.Add(line);
                                continue;
                            }

                            if (currentKey == null)
                            {
                                continue;
                            }

                            file.AddPair(new KeyValuePair<string, string>(currentKey, line));
                            currentKey = null;
                        }
                    }
                }
            }

            return file;
        }
    }
}
