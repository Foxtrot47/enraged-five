﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Services;
using RSG.Statistics.Common.Config;
using RSG.Statistics.DataMigration.Script;
using RSG.Statistics.Server;
using RSG.Statistics.Server.Context;
using RSG.Statistics.Server.TransManager;

namespace RSG.Statistics.DataMigration
{
    class Program
    {
        #region Constants
        // Return codes
        private static readonly int EXIT_SUCCESS = 0;
        private static readonly int EXIT_WARNING = 1;
        private static readonly int EXIT_ERROR = 2;
        private static readonly int EXIT_CRITIC = 3;

        private static readonly String OPT_SERVER = "server";
        private static readonly String OPT_SCRIPTS = "scripts";      // Comma seperated list of scripts to run (order is important).
        #endregion // Constants

        #region Member Data
        private static IUniversalLog m_log;
        private static IStatsServer s_server;
        #endregion // Member Data

        #region Program Entry Point
        /// <summary>
        /// The main entry point for the application.
        /// Attribute [STAThread] for invoking the universal log from the right thread
        /// </summary>
        [STAThread]
        static int Main(string[] args)
        {
            int exit_code = EXIT_SUCCESS;

            // Create the log
            m_log = LogFactory.CreateUniversalLog("Statistics_DataMigration");
            LogFactory.CreateApplicationConsoleLogTarget();
            UniversalLogFile logfile = LogFactory.CreateUniversalLogFile(m_log) as UniversalLogFile;

            // Define our executable options.  Stick with the long options
            // (e.g. --opt) rather than the single character versions.
            LongOption[] lopts = new LongOption[] {
                    new LongOption(OPT_SERVER, LongOption.ArgType.Required,
                        "Server to connect to."),
                    new LongOption(OPT_SCRIPTS, LongOption.ArgType.Required,
                        "Command script filename to execute (e.g. for batching jobs).")
                };
            CommandOptions options = new CommandOptions(args, lopts);

            // Initialise the server to communicate with.
            IStatisticsConfig config = new StatisticsConfig(options.Branch);
            if (!options.ContainsOption(OPT_SERVER))
            {
                s_server = config.DefaultServer;
            }
            else
            {
                String serverName = (String)options[OPT_SERVER];
                Debug.Assert(config.Servers.ContainsKey(serverName), "Requested an unknown server");
                s_server = config.Servers[serverName];
            }

            // Do the work.
            IList<String> scriptNames = ParseScriptNames(options);
            if (scriptNames.Any())
            {
                IList<IMigrationScript> scripts = CreateMigrationScriptObjects(scriptNames);
                if (scripts.Count == scriptNames.Count)
                {
                    // Initialise the server
                    ServerBootStrapper.Initialise(s_server);

                    // Run each of the scripts.
                    foreach (IMigrationScript script in scripts)
                    {
                        m_log.Message("Running the '{0}' script. ({1})", script.GetType().Name, script.Description);

                        m_log.Profile("{0} execution", script.GetType().Name);
                        bool success = script.Run(m_log);
                        m_log.ProfileEnd();

                        if (!success)
                        {
                            m_log.Error("Script didn't complete successfully.  Aborting the data migration until the issue can be investigated.");
                            exit_code = EXIT_CRITIC;
                            break;
                        }
                        else
                        {
                            m_log.Message("Script completed successfully.");
                        }
                    }
                }
                else
                {
                    m_log.Error("Couldn't create the correct number of scripts.");
                    exit_code = EXIT_ERROR;
                }
            }
            else
            {
                m_log.Error("No script names were provided.");
                exit_code = EXIT_ERROR;
            }

            // Shutdown the log and set the return code.
            LogFactory.ApplicationShutdown();
            Environment.ExitCode = exit_code;
            return (exit_code);
        }
        #endregion // Program Entry Point

        #region Private Methods
        /// <summary>
        /// Parses the script names.
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        private static IList<String> ParseScriptNames(CommandOptions options)
        {
            IList<String> names = new List<String>();
            if (options.ContainsOption(OPT_SCRIPTS))
            {
                String rawString = options[OPT_SCRIPTS] as String;
                names.AddRange(rawString.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
            }
            return names;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scriptNames"></param>
        /// <returns></returns>
        private static IList<IMigrationScript> CreateMigrationScriptObjects(IList<String> scriptNames)
        {
            // First get a list of the known scripts.
            Assembly ass = Assembly.GetAssembly(typeof(IMigrationScript));

            IDictionary<String, Type> knownScripts = new Dictionary<String, Type>();
            foreach (Type type in ass.GetTypes())
            {
                if (!type.IsAbstract && typeof(IMigrationScript).IsAssignableFrom(type))
                {
                    knownScripts.Add(type.Name.ToLower(), type);
                }
            }

            // Next convert the script names to actual scripts.
            IList<IMigrationScript> migrationScripts = new List<IMigrationScript>();
            foreach (String scriptName in scriptNames)
            {
                Type scriptType = knownScripts[scriptName.ToLower()];
                ConstructorInfo cInfo = scriptType.GetConstructor(new Type[] { });

                if (cInfo == null)
                {
                    String message = String.Format("Unable to find the default constructor for the '{0}' script.  No script will be generated for it.", scriptName);
                    Debug.Assert(false, message);
                    m_log.Error(message);
                    continue;
                }

                IMigrationScript scriptInstance = (IMigrationScript)cInfo.Invoke(new object[] { });
                if (scriptInstance == null)
                {
                    String message = String.Format("Unable to create an instance of the '{0}' script.", scriptName);
                    Debug.Assert(false, message);
                    m_log.Error(message);
                    continue;
                }

                migrationScripts.Add(scriptInstance);
            }
            return migrationScripts;
        }
        #endregion // Private Methods
    } // Program
}
