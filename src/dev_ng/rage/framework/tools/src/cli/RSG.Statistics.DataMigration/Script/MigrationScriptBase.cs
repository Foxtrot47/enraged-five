﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Logging;
using RSG.Statistics.Server.Context;
using RSG.Statistics.Server.Repository;
using RSG.Statistics.Server.TransManager;

namespace RSG.Statistics.DataMigration.Script
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class MigrationScriptBase : IMigrationScript
    {
        /// <summary>
        /// Short description about what the script is doing.
        /// </summary>
        public abstract String Description { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="log"></param>
        /// <returns></returns>
        public virtual bool Run(ILog log)
        {
            return ExecuteSubCommand(locator => RunCommand(locator, log));
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual bool RunCommand(IRepositoryLocator locator, ILog log)
        {
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="command"></param>
        protected T ExecuteSubCommand<T>(Func<IRepositoryLocator, T> command)
        {
            using (ITransManager manager = GlobalContext.Instance.TransManagerFactory.CreateManager(false))
            {
                return manager.ExecuteCommand(command);
            }
        }
    } // MigrationScriptBase
}
