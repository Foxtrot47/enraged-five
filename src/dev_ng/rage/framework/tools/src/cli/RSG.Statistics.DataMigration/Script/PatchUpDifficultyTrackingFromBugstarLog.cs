﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.XPath;
using NHibernate;
using NHibernate.Criterion;
using RSG.Base.Logging;
using RSG.Statistics.Common.Report;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Domain.Entities.Playthrough;
using RSG.Statistics.Server.Repository;
using PlayModel = RSG.Statistics.Common.Model.Playthrough;

namespace RSG.Statistics.DataMigration.Script
{
    /// <summary>
    /// 
    /// </summary>
    public class PatchUpDifficultyTrackingFromBugstarLog : MigrationScriptBase
    {
        /// <summary>
        /// Short description about what the script is doing.
        /// </summary>
        public override String Description
        {
            get { return "Patches up the difficulty tracking checkpoint overwrite."; }
        }

        public override bool Run(ILog log)
        {
            bool retVal = true;
            retVal &= ExecuteSubCommand(locator => UpdateUserCommand(locator, log, @"D:\temp\working_cached_files\bugstar_logs\Boat\application0.log"));
            retVal &= ExecuteSubCommand(locator => UpdateUserCommand(locator, log, @"D:\temp\working_cached_files\bugstar_logs\Car\application0.log"));
            retVal &= ExecuteSubCommand(locator => UpdateUserCommand(locator, log, @"D:\temp\working_cached_files\bugstar_logs\Doggy\application0.log"));
            retVal &= ExecuteSubCommand(locator => UpdateUserCommand(locator, log, @"D:\temp\working_cached_files\bugstar_logs\Hat\application0.log"));
            retVal &= ExecuteSubCommand(locator => UpdateUserCommand(locator, log, @"D:\temp\working_cached_files\bugstar_logs\Iron\application0.log"));
            retVal &= ExecuteSubCommand(locator => UpdateUserCommand(locator, log, @"D:\temp\working_cached_files\bugstar_logs\Shoe\application0.log"));
            retVal &= ExecuteSubCommand(locator => UpdateUserCommand(locator, log, @"D:\temp\working_cached_files\bugstar_logs\Thimble\application0.log"));
            retVal &= ExecuteSubCommand(locator => UpdateUserCommand(locator, log, @"D:\temp\working_cached_files\bugstar_logs\Wheelbarrow\application0.log"));
            return retVal;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool UpdateUserCommand(IRepositoryLocator locator, ILog log, String filename)
        {
            // Parse the file for the correct information.
            Regex checkpointPostRegex = new Regex(@"com\.rsg\.utils\.framework\.task\.AbstractRsgRestTask REST Request Success.*\{(?<json>.*)\}.*MissionAttempts/(?<missionattempt>\d+)/CheckpointAttempts");

            using (StreamReader reader = File.OpenText(filename))
            {
                String line = null;
                while ((line = reader.ReadLine()) != null)
                {
                    Match match = checkpointPostRegex.Match(line);
                    if (match.Success)
                    {
                        Group jsonGroup = match.Groups["json"];
                        Group attemptIdGroup = match.Groups["missionattempt"];

                        if (jsonGroup.Success && attemptIdGroup.Success)
                        {
                            ProcessCheckpointLine(locator, log, UInt32.Parse(attemptIdGroup.Value), jsonGroup.Value);
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        private void ProcessCheckpointLine(IRepositoryLocator locator, ILog log, uint missionAttemptId, String json)
        {
            try
            {
                // Split the line up into it's component parts.
                String[] split = json.Replace("\"", "").Split(new char[] { ',' });
                IDictionary<String, String> checkpointParts = new Dictionary<String, String>();
                foreach (String item in split)
                {
                    String[] components = item.Split(new char[] { ':' });
                    if (components.Length == 2)
                    {
                        checkpointParts[components[0]] = components[1];
                    }
                }

                DateTime start = ConvertJsonToDateTime(checkpointParts["Start"]);
                DateTime end = ConvertJsonToDateTime(checkpointParts["End"]);
                uint timeToComplete = UInt32.Parse(checkpointParts["TimeToComplete"]);
                uint previousChk = UInt32.Parse(checkpointParts["PreviousCheckpointIdx"]);

                // Get the mission attempt we are editing.
                PlaythroughMissionAttempt missionAttempt = locator.GetById<PlaythroughMissionAttempt>(missionAttemptId);
                if (missionAttempt == null)
                {
                    throw new ArgumentOutOfRangeException("missionAttemptId");
                }

                // Get the checkpoint attempt that we need to patch up.
                ICriteria criteria =
                    locator.CreateCriteria<PlaythroughCheckpointAttempt>()
                        .Add(Restrictions.Eq("Start", start))
                        .Add(Restrictions.Eq("End", end))
                        .Add(Restrictions.Eq("TimeToComplete", timeToComplete))
                        .Add(Restrictions.Eq("PlaythroughMissionAttempt", missionAttempt));
                PlaythroughCheckpointAttempt checkpointAttempt = criteria.List<PlaythroughCheckpointAttempt>().FirstOrDefault();

                MissionCheckpoint checkpoint = GetOrCreateCheckpoint(locator, missionAttempt.Mission, previousChk);

                if (checkpointAttempt.Checkpoint != checkpoint)
                {
                    log.Message("Updating mission attempt {0} checkpoint attempt {1}.  Setting mission checkpoint from {2} to {3}.",
                        missionAttempt.Id, checkpointAttempt.Id, checkpointAttempt.Checkpoint.Id, checkpoint.Id);
                    checkpointAttempt.Checkpoint = checkpoint;
                    locator.Save(checkpointAttempt);
                }
            }
            catch (Exception e)
            {
                log.ToolException(e, ":[");
            }
        }

        private DateTime ConvertJsonToDateTime(String json)
        {
            Match match = Regex.Match(json, @"/Date\((?<millisecs>-?\d*)\+(?<zone>\d*)\)/");
            long millisecs = Convert.ToInt64(match.Groups["millisecs"].Value);
            long zone = Convert.ToInt64(match.Groups["zone"].Value) / 100;
            return new DateTime(1970, 1, 1).AddMilliseconds(millisecs).AddHours(zone);
        }

        /// <summary>
        /// 
        /// </summary>
        private MissionCheckpoint GetOrCreateCheckpoint(IRepositoryLocator locator, Mission mission, uint checkpointIdx)
        {
            // Retrieve the checkpoint.
            DetachedCriteria criteria = DetachedCriteria.For<MissionCheckpoint>()
                .Add(Expression.Eq("Mission", mission))
                .Add(Expression.Eq("Index", checkpointIdx));
            MissionCheckpoint checkpoint = locator.FindFirst<MissionCheckpoint>(criteria);

            if (checkpoint == null)
            {
                RSG.Base.Logging.Log.Log__Message("Checkpoint {0} doesn't exist for mission {1}.  Creating a new checkpoint.", checkpointIdx, mission.Name);
                checkpoint = new MissionCheckpoint();
                checkpoint.Name = "Unknown Checkpoint";
                checkpoint.Identifier = "";
                checkpoint.Mission = mission;
                checkpoint.Index = checkpointIdx;
                checkpoint.InBugstar = false;
                locator.Save(checkpoint);
            }

            return checkpoint;
        }
    }
}
