﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Criterion;
using RSG.Base.Logging;
using RSG.Statistics.Domain.Entities.Playthrough;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.DataMigration.Script
{
    /// <summary>
    /// 
    /// </summary>
    public class UpdatePlaythroughComments : MigrationScriptBase
    {
        /// <summary>
        /// Short description about what the script is doing.
        /// </summary>
        public override String Description
        {
            get { return "Moves comments associated with playthrough mission attempts to the checkpoint attempts."; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override bool RunCommand(IRepositoryLocator locator, ILog log)
        {
            // Get all the mission attempts associated with a playthrough from the mission attempt stat table.
            DetachedCriteria criteria =
                DetachedCriteria.For<PlaythroughMissionAttempt>()
                                .Add(Restrictions.IsNotNull("PlaythroughSession"));
            IList<PlaythroughMissionAttempt> missionAttempts = locator.FindAll<PlaythroughMissionAttempt>(criteria);

            foreach (PlaythroughMissionAttempt missionAttempt in missionAttempts)
            {
                PlaythroughCheckpointAttempt lastCheckpointAttempt = missionAttempt.CheckpointAttempts.OrderBy(item => item.Start).LastOrDefault();
                if (lastCheckpointAttempt != null)
                {
                    lastCheckpointAttempt.Comment = missionAttempt.Comment;
                    missionAttempt.Comment = null;
                    locator.Save(lastCheckpointAttempt);
                    locator.Save(missionAttempt);
                }
            }

            return true;
        }
    } // UpdatePlaythroughComments
}
