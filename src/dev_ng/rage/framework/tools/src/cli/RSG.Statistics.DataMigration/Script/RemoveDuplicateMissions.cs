﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate.Criterion;
using RSG.Base.Logging;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Domain.Entities.GameAssetStats;
using RSG.Statistics.Domain.Entities.Telemetry.Raw;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.DataMigration.Script
{
    /// <summary>
    /// 
    /// </summary>
    public class RemoveDuplicateMissions : MigrationScriptBase
    {
        /// <summary>
        /// Short description about what the script is doing.
        /// </summary>
        public override String Description
        {
            get { return "Removes any duplicate missions that appear due to a change in what the game was sending."; }
        }

        /// <summary>
        /// 
        /// </summary>
        public override bool Run(ILog log)
        {
            bool result = true;

            // Get the list of gamers that need updating.
            IDictionary<long, long> missionsToChange = ExecuteSubCommand(locator => GetMissionReplacements(locator, log));

            // Work our way over each of them.
            log.Message("{0} missions to update.", missionsToChange.Count);
            
            foreach (KeyValuePair<long, long> missionPair in missionsToChange)
            {
                IDictionary<long, long> missionCheckpointsToChange = ExecuteSubCommand(locator => GetCheckpointReplacements(locator, log, missionPair.Key, missionPair.Value));
                log.Message("{0} mission checkpoints to update.", missionCheckpointsToChange.Count);

                foreach (KeyValuePair<long, long> checkpointPair in missionCheckpointsToChange)
                {
                    result &= ExecuteSubCommand(locator => PatchUpMissionCheckpoint(locator, log, checkpointPair.Key, checkpointPair.Value));
                }

                result &= ExecuteSubCommand(locator => PatchUpMission(locator, log, missionPair.Key, missionPair.Value));
            }
            
            return result;
        }

        /// <summary>
        /// Simply runs the script.
        /// </summary>
        /// <returns>Whether the script ran successfully.</returns>
        private IDictionary<long, long> GetMissionReplacements(IRepositoryLocator locator, ILog log)
        {
            // Get all the missions.
            DetachedCriteria criteria =
                DetachedCriteria.For<Mission>()
                                .Add(Restrictions.IsNotNull("MissionId"));
            IList<Mission> validMissions = locator.FindAll<Mission>(criteria);
            log.Message("Mission table contains {0} correct missions.", validMissions.Count);

            criteria =
                DetachedCriteria.For<Mission>()
                                .Add(Restrictions.IsNull("MissionId"));
            IList<Mission> badMissions = locator.FindAll<Mission>(criteria);
            log.Message("Mission table contains {0} bad missions.", badMissions.Count);

            // Create the lookup
            IDictionary<String, Mission> validMissionLookup = new Dictionary<String, Mission>();
            foreach (Mission mission in validMissions)
            {
                String missionId = mission.MissionId.ToLower();

                if (validMissionLookup.ContainsKey(missionId))
                {
                    log.Debug("Mission with id {0} already exists in the lookup.", missionId);
                }
                else
                {
                    validMissionLookup[missionId] = mission;
                }
            }


            // Create a map of wrong gamers to right ones.
            IDictionary<long, long> replacementMap = new Dictionary<long, long>();

            foreach (Mission badMission in badMissions)
            {
                String validMissionId = badMission.ScriptName.ToLower();

                // Check whether we already have a valid gamer for this one.
                if (validMissionLookup.ContainsKey(validMissionId))
                {
                    replacementMap[badMission.Id] = validMissionLookup[validMissionId].Id;
                }
                else
                {
                    log.Message("{0} doesn't exist as a valid mission.  Ignoring for now.", validMissionId);
                }
            }

            return replacementMap;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private IDictionary<long, long> GetCheckpointReplacements(IRepositoryLocator locator, ILog log, long invalidMissionId, long validMissionId)
        {
            Mission validMission = locator.GetById<Mission>(validMissionId);
            Mission invalidMission = locator.GetById<Mission>(invalidMissionId);

            // Create a look up for the valid mission's checkpoints.
            IDictionary<uint, MissionCheckpoint> checkpointLookup = new Dictionary<uint, MissionCheckpoint>();

            foreach (MissionCheckpoint checkpoint in validMission.Checkpoints)
            {
                if (checkpoint.Index != null)
                {
                    checkpointLookup.Add(checkpoint.Index.Value, checkpoint);
                }
            }

            IDictionary<long, long> replacementMap = new Dictionary<long, long>();

            foreach (MissionCheckpoint invCheckpoint in invalidMission.Checkpoints)
            {
                if (invCheckpoint.Index != null)
                {
                    if (checkpointLookup.ContainsKey(invCheckpoint.Index.Value))
                    {
                        MissionCheckpoint valCheckpoint = checkpointLookup[invCheckpoint.Index.Value];
                        replacementMap[invCheckpoint.Id] = valCheckpoint.Id;
                    }
                    else
                    {
                        log.Message("Creating new checkpoint with index {0} for mission {1}.", invCheckpoint.Index.Value, validMission.Name);
                        MissionCheckpoint newCheckpoint = new MissionCheckpoint();
                        newCheckpoint.Name = "Unknown Checkpoint";
                        newCheckpoint.Identifier = "";
                        newCheckpoint.Mission = validMission;
                        newCheckpoint.Index = invCheckpoint.Index;
                        newCheckpoint.InBugstar = false;
                        locator.Save(newCheckpoint);

                        replacementMap[invCheckpoint.Id] = newCheckpoint.Id;
                    }
                }
            }

            return replacementMap;
        }

        /// <summary>
        /// 
        /// </summary>
        private bool PatchUpMissionCheckpoint(IRepositoryLocator locator, ILog log, long invalidCheckpointId, long validCheckpointId)
        {
            MissionCheckpoint invalidCheckpoint = locator.GetById<MissionCheckpoint>(invalidCheckpointId);
            MissionCheckpoint validCheckpoint = locator.GetById<MissionCheckpoint>(validCheckpointId);
            log.Message("Replacing {0} with {1}.", invalidCheckpoint.Id, validCheckpoint.Id);

            DetachedCriteria attemptCriteria =
                DetachedCriteria.For<MissionCheckpointAttempt>()
                                .Add(Restrictions.Eq("Checkpoint", invalidCheckpoint));
            foreach (MissionCheckpointAttempt attempt in locator.FindAll<MissionCheckpointAttempt>(attemptCriteria))
            {
                attempt.Checkpoint = validCheckpoint;
                locator.Save(attempt);
            }

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        private bool PatchUpMission(IRepositoryLocator locator, ILog log, long invalidMissionId, long validMissionId)
        {
            Mission invalidMission = locator.GetById<Mission>(invalidMissionId);
            Mission validMission = locator.GetById<Mission>(validMissionId);
            log.Message("Replacing {0} with {1}.", invalidMissionId, validMissionId);

            DetachedCriteria attemptCriteria =
                DetachedCriteria.For<MissionAttemptStat>()
                                .Add(Restrictions.Eq("Mission", invalidMission));
            foreach (MissionAttemptStat attempt in locator.FindAll<MissionAttemptStat>(attemptCriteria))
            {
                attempt.Mission = validMission;
                locator.Save(attempt);
            }

            DetachedCriteria skeletonCriteria =
                DetachedCriteria.For<TelemetryMemorySkeleton>()
                                .Add(Restrictions.Eq("Mission", invalidMission));
            foreach (TelemetryMemorySkeleton stat in locator.FindAll<TelemetryMemorySkeleton>(skeletonCriteria))
            {
                stat.Mission = validMission;
                locator.Save(stat);
            }

            DetachedCriteria poolCriteria =
                DetachedCriteria.For<TelemetryMemoryPools>()
                                .Add(Restrictions.Eq("Mission", invalidMission));
            foreach (TelemetryMemoryPools stat in locator.FindAll<TelemetryMemoryPools>(poolCriteria))
            {
                stat.Mission = validMission;
                locator.Save(stat);
            }

            locator.Delete(invalidMission);

            return true;
        }
    } // RemoveDuplicateMissions
}
