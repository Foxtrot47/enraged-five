@ECHO OFF
:: ***************************************************************************************************************
:: *** copy_release_to_tools_bin.bat
:: ***
:: *** Michael T�schler <michael.taschler@rockstarnorth.com> Aug 2012
:: ***
:: *** - Script for copying the release build of the workbench to the tools bin directory. Including checking
:: ***   the relevant files from perforce and adding/deleting files as required.
:: *** 
:: *** Usage:
:: ***   copy_release_to_tools_bin.bat
:: ***************************************************************************************************************

setlocal EnableDelayedExpansion

:: Load in project environment variables
call setenv.bat

set SOURCE_DIR=%~dp0\bin\Release
set TARGET_DIR=%RS_TOOLSBIN%\Statistics
SET APP_NAME=RSG.Statistics.DataMigration.exe
set APP_CONFIG_NAME=%APP_NAME%.config

:: Copying core exe/exe config
echo Copying core exe/exe config files.

pushd %SOURCE_DIR%
p4 edit %TARGET_DIR%\%APP_NAME%
p4 edit %TARGET_DIR%\%APP_CONFIG_NAME%
copy /v /y /d %SOURCE_DIR%\%APP_NAME% %TARGET_DIR%\%APP_NAME%
copy /v /y /d %SOURCE_DIR%\%APP_CONFIG_NAME% %TARGET_DIR%\%APP_CONFIG_NAME%
popd

:: Copying core dll's
SET SUB_DIR=
SET FILTER="*.dll"
call:DoCopy

:: Copying config xml's
set SUB_DIR=Config
SET FILTER="*.xml"
call:DoCopy

:: Complete
echo Finished.
if not "%1"=="skippause" (
	pause
)
goto:eof


:DoCopy

set EXT_SOURCE_DIR=%SOURCE_DIR%
set EXT_TARGET_DIR=%TARGET_DIR%
if not "%SUB_DIR%"=="" (
	set EXT_SOURCE_DIR=%EXT_SOURCE_DIR%\%SUB_DIR%
	set EXT_TARGET_DIR=%EXT_TARGET_DIR%\%SUB_DIR%
)

echo Copying from %EXT_SOURCE_DIR% to %EXT_TARGET_DIR% filtering by %FILTER%

:: Copy source files to destination
pushd %EXT_SOURCE_DIR%
for %%x in (%FILTER%) do (
	if not exist %EXT_TARGET_DIR%\%%x (
		copy /v /y %EXT_SOURCE_DIR%\%%x %EXT_TARGET_DIR%\%%x
		p4 add %EXT_TARGET_DIR%\%%x
	) else (
		p4 edit %EXT_TARGET_DIR%\%%x
		copy /v /y %EXT_SOURCE_DIR%\%%x %EXT_TARGET_DIR%\%%x
	)
)
popd

:: Mark files for delete that are in destination but not in source
::pushd %EXT_TARGET_DIR%
::for %%x in (%FILTER%) do (
::	if not exist %EXT_SOURCE_DIR%\%%x (
::		p4 delete %EXT_TARGET_DIR%\%%x
::	)
::)
::popd
goto:eof

