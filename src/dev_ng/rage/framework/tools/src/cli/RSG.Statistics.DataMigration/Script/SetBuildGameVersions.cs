﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using NHibernate;
using RSG.Base.Logging;
using RSG.Statistics.Domain.Entities.GameAssets;
using RSG.Statistics.Server.Repository;

namespace RSG.Statistics.DataMigration.Script
{
    /// <summary>
    /// 
    /// </summary>
    public class SetBuildGameVersions : MigrationScriptBase
    {
        /// <summary>
        /// Short description about what the script is doing.
        /// </summary>
        public override String Description
        {
            get { return "Adds game versions for all valid builds (i.e. Ones with valid build identifiers)."; }
        }
        
        /// <summary>
        /// Simply runs the script.
        /// </summary>
        /// <returns>Whether the script ran successfully.</returns>
        public override bool RunCommand(IRepositoryLocator locator, ILog log)
        {
            Regex validBuildRegex = new Regex(@"^(?'major'\d+)\.?(?'minor'\d+)?$");

            ICriteria criteria = locator.CreateCriteria<Build>();
            foreach (Build build in criteria.List<Build>())
            {
                Match match = validBuildRegex.Match(build.Identifier);
                if (match.Success)
                {
                    Group majorGroup = match.Groups["major"];
                    Group minorGroup = match.Groups["minor"];

                    uint major = UInt32.Parse(majorGroup.Value);
                    uint minor = 0;

                    if (minorGroup.Success)
                    {
                        minor = UInt32.Parse(minorGroup.Value);
                    }

                    build.GameVersion = (major << 4) | minor;
                    locator.Save(build);
                }
            }

            return true;
        }
    } // SetBuildGameVersions
}
