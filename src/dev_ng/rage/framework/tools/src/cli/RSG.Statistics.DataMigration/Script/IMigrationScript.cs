﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Logging;
using RSG.Statistics.Server.Repository;
using RSG.Statistics.Server.TransManager;

namespace RSG.Statistics.DataMigration.Script
{
    /// <summary>
    /// Interface which all migration scripts need to adhere to.
    /// </summary>
    public interface IMigrationScript
    {
        /// <summary>
        /// Short description about what the script is doing.
        /// </summary>
        String Description { get; }

        /// <summary>
        /// Simply runs the script.
        /// </summary>
        /// <returns>Whether the script ran successfully.</returns>
        bool Run(ILog log);
    } // IMigrationScript
}
