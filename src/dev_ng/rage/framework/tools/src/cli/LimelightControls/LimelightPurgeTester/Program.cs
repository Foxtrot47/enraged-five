﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Net;
using RSG.Interop.Limelight;
using LimelightControls;
using LitJson;

namespace LimelightPurgeTester
{
    class Program
    {
        static void Main(string[] args)
        {
            Credentials c = null;
            if (args.Length >= 2)
            {
                c = new Credentials();
                c.UserName = args[0];
                c.SharedKey = args[1];

                Console.WriteLine("Using command line credentials for: " + c.UserName);
                if (args.Length == 3 && args[2] == "-save")
                {
                    Console.WriteLine("Storing Limelight credentials to limelight.xml for: " + c.UserName);
                    Credentials.Save(c, "limelight.xml");
                }
            }
            else
            {
                c = Credentials.Load("limelight.xml");
                if (c != null)
                {
                    Console.WriteLine("Using stored credentials (limelight.xml) for user: " + c.UserName);
                }
            }

            if (c == null)
            {
                Console.WriteLine("No stored credentials found, and no credentials specified in launch arguments");
                Console.WriteLine("     Usage: LimelightPurgeTester.exe [username] [sharedKey]");
                Console.Read();
                return;
            }

            PurgeInterface purgeInterface = new PurgeInterface();
            purgeInterface.Init(c.UserName, c.SharedKey);
            
            string subject = "LimelightControl_API_Purge_test";
            string emailTo = "john.moore@rockstartoronto.com";
            string emailCC = "";
            string emailBCC = "";

            List<PurgeEntry> entries = new List<PurgeEntry>();
            PurgeEntry e = new PurgeEntry("rsgames", "http://patches.rockstargames.com/dev/test/purgetest.txt", false, false);
            entries.Add(e);

            string response = purgeInterface.PostPurgeRequest(EmailType.summary, subject, emailTo, emailCC, emailBCC, entries);
            JsonReader reader = new JsonReader(response);
            JsonData data = JsonMapper.ToObject(reader);

            if (data.ContainsKey("id"))
            {
                string id = data["id"].ToString();
                System.Threading.Thread.Sleep(5000);
                string result = purgeInterface.GetRequestStatus(id);
            }
        }
    }
}
