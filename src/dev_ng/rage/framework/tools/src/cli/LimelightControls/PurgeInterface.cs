﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSG.Base.Net;
using RSG.Interop.Limelight;
using LitJson;

namespace LimelightControls
{
    public class PurgeInterface
    {
        #region Fields

        /// <summary>
        /// The RestConnection interface
        /// </summary>
        private RestConnection Connection;

        /// <summary>
        /// Limelight host name
        /// </summary>
        private string _hostName = "http://control.llnw.com";

        /// <summary>
        /// Limelight REST version
        /// </summary>
        private string _version = "v1";

        /// <summary>
        /// Limelight namespace (purge)
        /// </summary>
        private string _api = "purge-api";

        /// <summary>
        /// Shared key, must be set
        /// </summary>
        private string _apiSharedKey = "";

        /// <summary>
        /// User name, must be set
        /// </summary>
        private string _apiUserName = "";

        /// <summary>
        /// Initialized status, set true when initialized
        /// </summary>
        private bool _initialized = false;

        #endregion

        #region Private Methods

        private string RunQuery(IRestQuery query)
        {
            using (Stream responseStream = Connection.RunQuery(query))
            {
                StreamReader streamReader = new StreamReader(responseStream);
                return streamReader.ReadToEnd();
            }
        }

        #endregion

        #region Public Methods

        public string GetPurgeEntitlements()
        {
            if (!_initialized)
            {
                throw new Exception("Not initialized");
            }

            string method = "/purgeEntitlements";
            PurgeQueryBuilder pb = new PurgeQueryBuilder(RequestMethod.GET, _hostName, _api, _version, method);
            return RunQuery(pb.ToQuery(_apiUserName, _apiSharedKey));
        }

        public string GetRequestStatus(string requestId)
        {
            if (!_initialized)
            {
                throw new Exception("Not initialized");
            }

            string method = String.Format("/requestStatus/{0}/", requestId);
            PurgeQueryBuilder pb = new PurgeQueryBuilder(RequestMethod.GET, _hostName, _api, _version, method);
            return RunQuery(pb.ToQuery(_apiUserName, _apiSharedKey));
        }

        public string PostPurgeRequest(EmailType emailType, string emailSubject, string emailTo, string emailCC, string emailBCC, List<PurgeEntry> entries)
        {
            if (!_initialized)
            {
                throw new Exception("Not initialized");
            }

            string method = "/request";
            PurgeQueryBuilder pb = new PurgeQueryBuilder(RequestMethod.POST, _hostName, _api, _version, method);
            pb.CreatePurgeRequest(emailType, emailSubject, emailTo, emailCC, emailBCC, entries);
            return RunQuery(pb.ToQuery(_apiUserName, _apiSharedKey));
        }

        public bool Init(string userName, string sharedKey)
        {
            Connection = new LimelightConnection(new Uri(_hostName), null);
            _apiUserName = userName;
            _apiSharedKey = sharedKey;
            _initialized = true;
            return true;
        }

        #endregion
    }
}
