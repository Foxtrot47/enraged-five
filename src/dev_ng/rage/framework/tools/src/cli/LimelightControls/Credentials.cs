﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using RSG.Base.Xml;

namespace LimelightControls
{
    public class Credentials
    {
        public string UserName;
        public string SharedKey;

        public static Credentials Load(string path)
        {
            try
            {
                Credentials c = new Credentials();
                var serializer = new XmlSerializer(typeof(Credentials));
                using (var reader = XmlReader.Create(path))
                {
                    c = (Credentials)serializer.Deserialize(reader);
                    return c;
                }

            }
            catch (System.Exception )
            {

            }

            return null;
        }

        public static bool Save(Credentials c, string path)
        {
            try
            {
                var serializer = new XmlSerializer(typeof(Credentials));
                using (var writer = XmlWriter.Create(path))
                {
                    serializer.Serialize(writer, c);
                }

                return true;
            }
            catch (System.Exception )
            {
                
            }

            return false;
        }
    }
}
