﻿//---------------------------------------------------------------------------------------------
// <copyright file="Conversation.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueProjectCreator
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Xml;
    using System.Xml.XPath;

    /// <summary>
    /// 
    /// </summary>
    public class Conversation
    {
        #region Fields
        private static readonly String PlaceHolderAttribute = "placeholder";
        private static readonly String RootAttribute = "root";
        private static readonly String DescriptionAttribute = "description";
        private static readonly String RandomAttribute = "random";
        private static readonly String CategoryAttribute = "category";
        private static readonly String LockedAttribute = "locked";
        private static readonly String RoleAttribute = "role";
        private static readonly String ModelNameAttribute = "model";
        private static readonly String VoiceAttribute = "voice";
        private static readonly String UseVoiceAttribute = "usevoice";
        private static readonly String InterruptibleAttribute = "interruptible";
        private static readonly String IsAnimTriggeredAttribute = "anim";
        private static readonly String CutsceneSubtitlesAttribute = "cutscene";

        private static readonly XPathExpression XmlLinePath = XPathExpression.Compile("Lines/Line");
        private static readonly XPathExpression XmlAttributes = XPathExpression.Compile("@*");

        public bool m_placeholder;
        public bool m_interruptible;
        public bool m_isAnimTriggered;
        public bool m_isCutsceneSubtitles;
        public string m_root;
        public string m_description;
        public bool m_random;
        public bool m_mocapConversation;
        public string m_category;
        public bool m_locked;
        public string m_role;
        public string m_modelName;
        public string m_voice;
        public bool m_useVoice;
        public Dialogue m_mission;
        public bool m_filenameErrors;
        public bool m_selected;
        public List<Line> m_lines;
        public int _audioFilepathIndex;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Loads a conversation from a xml document
        /// </summary>
        /// <param name="navigator">The navigator for this object in the xml file</param>
        /// <param name="mission">The mission this conversation belongs to</param>
        public Conversation(XPathNavigator navigator, Dialogue mission)
        {
            this.m_mission = mission;

            this.m_placeholder = false;
            this.m_root = String.Empty;
            this.m_description = String.Empty;
            this.m_random = false;
            this.m_mocapConversation = false;
            this.m_lines = new List<Line>();
            this.m_locked = false;
            this.m_role = String.Empty;
            this.m_modelName = String.Empty;
            this.m_voice = String.Empty;
            this.m_useVoice = false;
            this.m_interruptible = true;
            this.m_isAnimTriggered = false;
            this.m_isCutsceneSubtitles = false;

            this.Deserialise(navigator);
        }
        #endregion Constructors

        #region Properties
        public Dialogue Mission
        {
            get { return this.m_mission; }
        }

        public bool Locked
        {
            get { return this.m_locked; }
            set { this.m_locked = value; }
        }

        public int AudioFilepathIndex
        {
            get { return this._audioFilepathIndex; }
            set { this._audioFilepathIndex = value; }
        }
        #endregion Properties

        #region Methods
        /// <summary>
        /// Deserialise the conversation from the given navigator
        /// property
        /// </summary>
        private void Deserialise(XPathNavigator navigator)
        {
            XPathNodeIterator converationAttrIt = navigator.Select(XmlAttributes);
            while (converationAttrIt.MoveNext())
            {
                if (typeof(String) != converationAttrIt.Current.ValueType)
                    continue;
                String value = (converationAttrIt.Current.TypedValue as String);

                if (converationAttrIt.Current.Name == PlaceHolderAttribute)
                {
                    this.m_placeholder = (value == "True" ? true : false);
                }
                else if (converationAttrIt.Current.Name == RootAttribute)
                {
                    this.m_root = value;
                }
                else if (converationAttrIt.Current.Name == DescriptionAttribute)
                {
                    this.m_description = value;
                }
                else if (converationAttrIt.Current.Name == RandomAttribute)
                {
                    this.m_random = (value == "True" ? true : false);
                }
                else if (converationAttrIt.Current.Name == CategoryAttribute)
                {
                    this.m_category = value;
                }
                else if (converationAttrIt.Current.Name == LockedAttribute)
                {
                    this.m_locked = (value == "True" ? true : false);
                }
                else if (converationAttrIt.Current.Name == RoleAttribute)
                {
                    this.m_role = value;
                }
                else if (converationAttrIt.Current.Name == ModelNameAttribute)
                {
                    this.m_modelName = value;
                }
                else if (converationAttrIt.Current.Name == VoiceAttribute)
                {
                    this.m_voice = value;
                }
                else if (converationAttrIt.Current.Name == UseVoiceAttribute)
                {
                    this.m_useVoice = (value == "True" ? true : false);
                }
                else if (converationAttrIt.Current.Name == InterruptibleAttribute)
                {
                    this.m_interruptible = (value == "True" ? true : false);
                }
                else if (converationAttrIt.Current.Name == IsAnimTriggeredAttribute)
                {
                    this.m_isAnimTriggered = (value == "True" ? true : false);
                }
                else if (converationAttrIt.Current.Name == CutsceneSubtitlesAttribute)
                {
                    this.m_isCutsceneSubtitles = (value == "True" ? true : false);
                }
            }

            // Load all of the lines in this converation
            XPathNodeIterator lineIterator = navigator.Select(XmlLinePath);
            while (lineIterator.MoveNext())
            {
                XPathNavigator lineNavigator = lineIterator.Current;
                Line newLine = new Line(lineNavigator, this);
                this.m_lines.Add(newLine);
            }

            if (this.m_random)
            {
                foreach (Line line in this.m_lines)
                {
                    if (line.Filename[line.Filename.Length - 3] == '_')
                    {
                        line.Filename = line.Filename.Substring(0, line.Filename.Length - 3);
                    }
                }
            }
        }

        public int DetermineConversationIndex(Regex normalRegex, Regex randomRegex)
        {
            if (this.m_random)
            {
                Line line = this.m_lines.FirstOrDefault();
                if (line == null)
                {
                    return -1;
                }

                if (randomRegex.IsMatch(line.Filename))
                {
                    string substring = line.Filename.Substring(line.Filename.IndexOf('_') + 1, 2);
                    int index = this.GetIndexFromFilenameString(substring);
                    return index;
                }
                else
                {
                    return -1;
                }
            }
            else
            {
                int index = -1;
                foreach (Line line in this.m_lines)
                {
                    if (line == null)
                    {
                        continue;
                    }

                    if (normalRegex.IsMatch(line.Filename))
                    {
                        string substring = line.Filename.Substring(line.Filename.IndexOf('_') + 1, 2);
                        if (index == -1)
                        {
                            index = this.GetIndexFromFilenameString(substring);
                        }
                        else
                        {
                            int newIndex = this.GetIndexFromFilenameString(substring);
                            if (newIndex != index)
                            {
                                return -1;
                            }
                        }                       
                    }
                }

                return index;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private int GetIndexFromFilenameString(string value)
        {
            int mod = (int)(value[0]) - 65;
            int remainder = (int)(value[1]) - 65;

            return (26 * mod) + remainder;
        }

        public void Serialise(XmlWriter writer, Configurations config)
        {
            if (this._audioFilepathIndex == -1)
            {
                Trace.Assert(false);
            }

            writer.WriteStartElement("AudioFilepathIndex");
            writer.WriteAttributeString("value", this._audioFilepathIndex.ToString());
            writer.WriteEndElement();

            writer.WriteElementString("Category", this.m_category.ToString());

            writer.WriteStartElement("CutsceneSubtitles");
            writer.WriteAttributeString("value", this.m_isCutsceneSubtitles.ToString());
            writer.WriteEndElement();

            writer.WriteElementString("Description", this.m_description);

            writer.WriteStartElement("Interruptible");
            writer.WriteAttributeString("value", this.m_interruptible.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("IsPlaceholder");
            writer.WriteAttributeString("value", this.m_placeholder.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("IsRandom");
            writer.WriteAttributeString("value", this.m_random.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("IsTriggeredByAnimation");
            writer.WriteAttributeString("value", this.m_isAnimTriggered.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("LockedFilenames");
            writer.WriteAttributeString("value", this.m_locked.ToString());
            writer.WriteEndElement();

            writer.WriteElementString("Root", this.m_root);

            writer.WriteStartElement("Lines");
            foreach (Line line in this.m_lines)
            {
                writer.WriteStartElement("Item");
                line.Serialise(writer, config);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();
        }
        #endregion Methods
    } // DialogueProjectCreator.Conversation {Class}
} // DialogueProjectCreator {Namespace}
