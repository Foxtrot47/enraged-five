﻿//---------------------------------------------------------------------------------------------
// <copyright file="Dialogue.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueProjectCreator
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Xml;
    using System.Xml.XPath;

    /// <summary>
    /// 
    /// </summary>
    public class Dialogue
    {
        #region Fields
        private static readonly string ExpectedVersion = "2.0";
        private static readonly string VersionAttribute = "version";
        private static readonly string MissionIdAttribute = "id";
        private static readonly string MissionNameAttribute = "name";
        private static readonly string MissionSubtitleIdAttribute = "subtitle";
        private static readonly string TypeAttribute = "type";
        private static readonly string MultiplayerAttribute = "multiplayer";
        private static readonly string AmbientAttribute = "ambient";

        private static readonly XPathExpression XmlMissionPath = XPathExpression.Compile("/MissionDialogue");
        private static readonly XPathExpression XmlConversationPath = XPathExpression.Compile("Conversations/Conversation");
        private static readonly XPathExpression XmlAttributes = XPathExpression.Compile("@*");

        private static readonly string MocapMarkup = "Mocap";
        private static readonly string RandomMarkup = "Random";
        private static readonly string PhoneMarkup = "Phone";
        public string m_filename;
        public string m_name;
        public string m_missionId;
        public string m_missionName;
        public string m_missionSubtitleId;
        public bool m_randomEventMission;
        public string m_missionType;
        public List<Conversation> m_conversations;
        public Configurations m_configurations;
        public bool m_hasLockedConversations;
        public bool m_isMultiplayer;
        public bool m_isAmbient;
        public List<string> _deletedFilenames;
        #endregion Fields

        #region Constructors
        /// <summary>
        /// Creates the mission dialogue based on the filename given.
        /// </summary>
        /// <param name="filename">The filename to create this object from</param>
        /// <param name="configurations">The configurations that will be assigned to this dialogue</param>
        public Dialogue(string filename, Configurations configurations)
        {
            m_configurations = configurations;

            this._deletedFilenames = new List<string>();
            this.m_filename = filename;
            this.m_name = Path.GetFileNameWithoutExtension(filename);
            this.m_missionId = String.Empty;
            this.m_missionName = String.Empty;
            this.m_missionSubtitleId = String.Empty;
            this.m_missionType = Configurations.MissionTypes.First();
            this.m_conversations = new List<Conversation>();
            this.m_hasLockedConversations = false;
            this.m_isMultiplayer = false;
            this.m_isAmbient = false;

            this.Deserialise();
            this.DetermineLockedConversations();
            this.DetermineConversationIndex();
        }

        /// <summary>
        /// Creates the mission dialogue based on the filename given.
        /// </summary>
        /// <param name="filename">The filename to create this object from</param>
        /// <param name="configurations">The configurations that will be assigned to this dialogue</param>
        public Dialogue(Stream stream, Configurations configurations)
        {
            m_configurations = configurations;

            this._deletedFilenames = new List<string>();
            this.m_conversations = new List<Conversation>();

            this.Deserialise(stream);
            this.DetermineLockedConversations();
            this.DetermineConversationIndex();
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// The configurations that this mission dialogue should use
        /// </summary>
        public Configurations Configurations
        {
            get { return m_configurations; }
            set { m_configurations = value; }
        }
        #endregion Properties

        #region Methods
        public void DetermineLockedConversations()
        {
            Boolean hasLockedConversations = false;
            foreach (Conversation conversation in this.m_conversations)
            {
                if (conversation.Locked == true)
                {
                    hasLockedConversations = true;
                    break;
                }
            }
            this.m_hasLockedConversations = hasLockedConversations;
        }

        public void DetermineConversationIndex()
        {
            Regex normalRegex = new Regex("\\A" + this.m_missionId + "_[a-zA-Z]{4}\\Z");
            Regex randomRegex = new Regex("\\A" + this.m_missionId + "_[a-zA-Z]{4}\\Z");

            List<Conversation> invalid = new List<Conversation>();
            List<int> indices = new List<int>();
            foreach (Conversation conversation in this.m_conversations)
            {
                if (conversation == null)
                {
                    continue;
                }

                int index = conversation.DetermineConversationIndex(normalRegex, randomRegex);
                if (index == -1)
                {
                    invalid.Add(conversation);
                }
                else
                {
                    if (!indices.Contains(index))
                    {
                        conversation._audioFilepathIndex = index;
                        indices.Add(index);
                    }
                    else
                    {
                        invalid.Add(conversation);
                    }
                }
            }

            foreach (string filename in this._deletedFilenames)
            {
                string substring = null;
                if (normalRegex.IsMatch(filename))
                {
                    substring = filename.Substring(filename.IndexOf('_') + 1, 2);
                }
                else
                {
                    continue;
                }

                int mod = (int)(substring[0]) - 65;
                int remainder = (int)(substring[1]) - 65;
                int index = (26 * mod) + remainder;
                if (!indices.Contains(index))
                {
                    indices.Add(index);
                }
            }

            indices.Sort();
            System.Diagnostics.Debug.WriteLine(String.Format("Invalid count: {0}", invalid.Count.ToString()));
            foreach (Conversation conversation in invalid)
            {
                int index = 0;
                while (indices.Contains(index))
                {
                    index++;
                }

                conversation._audioFilepathIndex = index;
                indices.Add(index);
            }
        }

        /// <summary>
        /// Deserialise the mission dialogue from the filename
        /// property
        /// </summary>
        /// <returns>This returns true if it has either failed or succeed in loading a new version dstar file
        /// it returns false if it has tried to load a old version dstar file.</returns>
        private Boolean Deserialise()
        {
            if (!File.Exists(this.m_filename))
            {
                return true;
            }

            XmlDocument document = new XmlDocument();
            try
            {
                document.Load(this.m_filename);
                return Deserialise(document);
            }
            catch (XmlException)
            {
                return true;
            }
        }

        /// <summary>
        /// Deserialise the mission dialogue from the filename
        /// property
        /// </summary>
        /// <returns>This returns true if it has either failed or succeed in loading a new version dstar file
        /// it returns false if it has tried to load a old version dstar file.</returns>
        private Boolean Deserialise(Stream stream)
        {
            XmlDocument document = new XmlDocument();
            try
            {
                document.Load(stream);
                return Deserialise(document);
            }
            catch (XmlException)
            {
                return true;
            }
        }

        private bool Deserialise(XmlDocument document)
        {
            XPathNavigator navigator = document.CreateNavigator();
            XPathNodeIterator missionIterator = navigator.Select(XmlMissionPath);

            if (missionIterator.Count == 0)
            {
                // This probably means that we are trying to load a old format dstar file.
                return false;
            }

            XPathNavigator missionNavigator = missionIterator.Current;
            XPathNodeIterator missionAttrIterator = missionNavigator.Select(XPathExpression.Compile("/MissionDialogue/@*"));

            // Collect the attributes for the mission
            while (missionAttrIterator.MoveNext())
            {
                if (typeof(String) != missionAttrIterator.Current.ValueType)
                    continue;
                String value = (missionAttrIterator.Current.TypedValue as String);

                if (missionAttrIterator.Current.Name == MissionIdAttribute)
                {
                    this.m_missionId = value;
                }
                else if (missionAttrIterator.Current.Name == MissionNameAttribute)
                {
                    this.m_missionName = value;
                }
                else if (missionAttrIterator.Current.Name == MissionSubtitleIdAttribute)
                {
                    this.m_missionSubtitleId = value;
                }
                else if (missionAttrIterator.Current.Name == TypeAttribute)
                {
                    this.m_missionType = value;
                    this.m_randomEventMission = (this.m_missionType == "Random Event");
                }
                else if (missionAttrIterator.Current.Name == MultiplayerAttribute)
                {
                    this.m_isMultiplayer = bool.Parse(value);
                }
                else if (missionAttrIterator.Current.Name == AmbientAttribute)
                {
                    this.m_isAmbient = bool.Parse(value);
                }
            }

            // Load all of the conversations
            XPathNodeIterator conversationIterator = missionNavigator.Select(XPathExpression.Compile("MissionDialogue/Conversations/Conversation"));
            while (conversationIterator.MoveNext())
            {
                XPathNavigator conversationNavigator = conversationIterator.Current;
                Conversation newConversation = new Conversation(conversationNavigator, this);
                this.m_conversations.Add(newConversation);
            }

            // Load all of the deleted filenames
            XPathNodeIterator deleteFilenameIterator = missionNavigator.Select(XPathExpression.Compile("MissionDialogue/DeletedFilenames/Filename"));
            while (deleteFilenameIterator.MoveNext())
            {
                XPathNavigator currentNavigator = deleteFilenameIterator.Current;
                this._deletedFilenames.Add(currentNavigator.Value);
            }

            return true;
        }

        public void Serialise(XmlWriter writer, Configurations config)
        {
            writer.WriteStartElement("AmbientFile");
            writer.WriteAttributeString("value", this.m_isAmbient.ToString());
            writer.WriteEndElement();

            writer.WriteElementString("Id", this.m_missionId);

            writer.WriteStartElement("Multiplayer");
            writer.WriteAttributeString("value", this.m_isMultiplayer.ToString());
            writer.WriteEndElement();

            writer.WriteElementString("Name", this.m_missionName);
            writer.WriteElementString("SubtitleId", this.m_missionSubtitleId);
            writer.WriteElementString("Type", this.m_missionType);

            writer.WriteStartElement("Conversations");
            foreach (Conversation conversation in this.m_conversations)
            {
                writer.WriteStartElement("Item");
                conversation.Serialise(writer, config);
                writer.WriteEndElement();
            }

            writer.WriteEndElement();

            writer.WriteStartElement("DeletedFilenames");
            foreach (string deletedFilename in this._deletedFilenames)
            {
                writer.WriteElementString("Item", deletedFilename);
            }

            writer.WriteEndElement();
        }
        #endregion Methods
    } // DialogueProjectCreator.Dialogue {Class}
} // DialogueProjectCreator {Namespace}
