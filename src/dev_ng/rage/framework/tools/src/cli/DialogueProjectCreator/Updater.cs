﻿
namespace DialogueProjectCreator
{
    using System.IO;
    using System.Xml;
    using RSG.Text.Model;

    public class Updater
    {
        /// <summary>
        /// Called to update an old file format to a new format.
        /// </summary>
        /// <param name="inStream">
        /// The stream that the old data can be read from.
        /// </param>
        /// <param name="outStream">
        /// The new stream that the new data can be written to.
        /// </param>
        /// <param name="project">
        /// The project the updated file is being added to.
        /// </param>
        public void Update(Stream inStream, Stream outStream, DialogueConfigurations config)
        {
            Configurations oldConfig = new Configurations(config);
            Dialogue dialogue = new Dialogue(inStream, oldConfig);

            XmlWriterSettings settings = new XmlWriterSettings() { Indent = true };
            using (XmlWriter writer = XmlWriter.Create(outStream, settings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Dialogue");

                dialogue.Serialise(writer, oldConfig);

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }
        }
    }
}
