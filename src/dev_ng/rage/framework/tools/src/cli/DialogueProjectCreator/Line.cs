﻿//---------------------------------------------------------------------------------------------
// <copyright file="Line.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueProjectCreator
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Xml;
    using System.Xml.XPath;

    /// <summary>
    /// 
    /// </summary>
    public class Line
    {
        #region Fields
        private static readonly String CharacterGuidAttribute = "guid";
        private static readonly String DialogueAttribute = "dialogue";
        private static readonly String VolumeAttribute = "volume";
        private static readonly String SpeakerAttribute = "speaker";
        private static readonly String ListenerAttribute = "listener";
        private static readonly String AudioAttribute = "audio";
        private static readonly String FilenameAttribute = "filename";
        private static readonly String ManualFilenameAttribute = "manual";
        private static readonly String SpecialAttribute = "special";
        private static readonly String RecordedAttribute = "recorded";
        private static readonly String SentToRecordedAttribute = "sent";
        private static readonly String TimestampAttribute = "timestamp";
        private static readonly String SubtitledAttribute = "subtitled";
        private static readonly String InterruptibleAttribute = "interruptible";
        private static readonly String DontInterruptForSpecialAbilityAttribute = "dontInterruptForSpecialAbility";
        private static readonly String DucksScoreAttribute = "ducks_score";
        private static readonly String DucksRadioAttribute = "ducks_radio";
        private static readonly String AudibilityAttribute = "audibility";
        private static readonly String HeadsetSubmixAttribute = "headset_submix";
        private static readonly String ControllerPadSpeakerAttribute = "controller_pad_speaker";
        private static readonly XPathExpression XmlAttributes = XPathExpression.Compile("@*");

        public static Dictionary<int, String> StringFormatReplacements;

        public Guid m_characterGuid;
        public string m_lineDialogue;
        public string m_volume;
        public string m_speaker;
        public string m_listener;
        public string m_audioType;
        public string m_filename;
        public string m_filenameWithLineIndex;
        public bool m_manualFilename;
        public string m_special;
        public bool m_sentToBeRecorded;
        public bool m_recorded;
        public bool m_alwaysSubtitled;
        public bool m_interruptible;
        public bool m_dontInterruptForSpecialAbility;
        public bool m_ducksScore;
        public bool m_ducksRadio;
        public Conversation m_conversation;
        public DateTime m_timestamp;
        public bool m_hasError;
        public int m_audibility;
        public bool m_headsetSubmix;
        public bool m_selected;
        public bool m_controllerPadSpeaker;
        #endregion Fields

        #region Constructors
        static Line()
        {
            StringFormatReplacements = new Dictionary<int, String>();

            // Microsoft apostrophe
            StringFormatReplacements.Add(8211, "-");
            StringFormatReplacements.Add(8212, "-");
            StringFormatReplacements.Add(8216, "\'");
            StringFormatReplacements.Add(8217, "\'");
            StringFormatReplacements.Add(8220, "\"");
            StringFormatReplacements.Add(8221, "\"");
            StringFormatReplacements.Add(8230, "...");
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="Line"/> class using the specified
        /// xml navigator.
        /// </summary>
        /// <param name="navigator">
        /// The navigator for this object in the xml file.
        /// </param>
        /// <param name="mission">
        /// The conversation this line belongs to.
        /// </param>
        public Line(XPathNavigator navigator, Conversation conversation)
        {
            this.m_conversation = conversation;

            this.m_lineDialogue = String.Empty;
            this.m_speaker = "0";
            this.m_listener = "0";
            this.m_filename = String.Empty;
            this.m_manualFilename = false;
            this.m_recorded = false;
            this.m_sentToBeRecorded = false;
            this.m_alwaysSubtitled = false;
            this.m_timestamp = DateTime.Now;
            this.m_interruptible = true;
            this.m_dontInterruptForSpecialAbility = false;
            this.m_ducksScore = false;
            this.m_ducksRadio = true;
            this.m_audibility = 0;
            this.m_headsetSubmix = false;
            this.m_controllerPadSpeaker = false;

            this.Deserialise(navigator);
        }
        #endregion Constructors

        #region Properties
        /// <summary>
        /// Represents the filename for this line. This is the filename that
        /// the recordind is actually in, this has a lot of rules surrounding it
        /// and can be either manually entered or automatically generated.
        /// </summary>
        public String Filename
        {
            get { return m_filename; }
            set
            {
                String newStringValue = GetStringInRightFormat(value as String);
                m_filename = newStringValue;
                this.m_timestamp = DateTime.Now;
            }
        }
        #endregion Properties

        #region Methods
        public static String GetStringInRightFormat(String oldFormat)
        {
            String correctStringFormat = String.Empty;
            int index = 0;
            foreach (char character in oldFormat.ToCharArray())
            {
                int characterValue = (int)character;
                if (StringFormatReplacements.ContainsKey(characterValue))
                {
                    correctStringFormat += StringFormatReplacements[character];
                }
                else
                {
                    correctStringFormat += character;
                }

                index++;
            }

            return correctStringFormat;
        }

        /// <summary>
        /// Deserialise the line from the given navigator property.
        /// </summary>
        /// <param name="navigator">
        /// The xml navigator containing the data used to initialise this instance.
        /// </param>
        private void Deserialise(XPathNavigator navigator)
        {
            XPathNodeIterator lineAttrIt = navigator.Select(XmlAttributes);
            while (lineAttrIt.MoveNext())
            {
                if (typeof(string) != lineAttrIt.Current.ValueType)
                {
                    continue;
                }

                string value = (lineAttrIt.Current.TypedValue as string);

                if (lineAttrIt.Current.Name == CharacterGuidAttribute)
                {
                    Guid guid = Guid.Empty;
                    Guid.TryParse(value, out guid);

                    this.m_characterGuid = guid;
                }
                else if (lineAttrIt.Current.Name == DialogueAttribute)
                {
                    this.m_lineDialogue = GetStringInRightFormat(value);
                }
                else if (lineAttrIt.Current.Name == VolumeAttribute)
                {
                    this.m_volume = value;
                }
                else if (lineAttrIt.Current.Name == SpeakerAttribute)
                {
                    this.m_speaker = value;
                    int result;
                    if (!int.TryParse(this.m_speaker, out result))
                    {
                        if (value.Length > 0)
                        {
                            int characterValue = ((int)Char.ToUpper(value[0])) - 65;
                            if (characterValue >= 0 && characterValue <= 25)
                            {
                                this.m_speaker = (characterValue + 10).ToString();
                            }
                            else
                            {
                                this.m_speaker = "0";
                            }
                        }
                        else
                        {
                            this.m_speaker = "0";
                        }
                    }
                }
                else if (lineAttrIt.Current.Name == ListenerAttribute)
                {
                    this.m_listener = value;
                    int result;
                    if (!int.TryParse(this.m_listener, out result))
                    {
                        if (value.Length > 0)
                        {
                            int characterValue = ((int)Char.ToUpper(value[0])) - 65;
                            if (characterValue >= 0 && characterValue <= 25)
                            {
                                this.m_listener = (characterValue + 10).ToString();
                            }
                            else
                            {
                                this.m_listener = "0";
                            }
                        }
                        else
                        {
                            this.m_listener = "0";
                        }
                    }
                }
                else if (lineAttrIt.Current.Name == AudioAttribute)
                {
                    this.m_audioType = value;
                }
                else if (lineAttrIt.Current.Name == FilenameAttribute)
                {
                    this.m_filename = value;
                }
                else if (lineAttrIt.Current.Name == ManualFilenameAttribute)
                {
                    this.m_manualFilename = (value == "True" ? true : false);
                }
                else if (lineAttrIt.Current.Name == SpecialAttribute)
                {
                    this.m_special = value;
                }
                else if (lineAttrIt.Current.Name == RecordedAttribute)
                {
                    this.m_recorded = (value == "True" ? true : false);
                }
                else if (lineAttrIt.Current.Name == SentToRecordedAttribute)
                {
                    this.m_sentToBeRecorded = (value == "True" ? true : false);
                }
                else if (lineAttrIt.Current.Name == SubtitledAttribute)
                {
                    this.m_alwaysSubtitled = (value == "True" ? true : false);
                }
                else if (lineAttrIt.Current.Name == InterruptibleAttribute)
                {
                    this.m_interruptible = (value == "True" ? true : false);
                }
                else if (lineAttrIt.Current.Name == DontInterruptForSpecialAbilityAttribute)
                {
                    this.m_dontInterruptForSpecialAbility = (value == "True" ? true : false);
                }
                else if (lineAttrIt.Current.Name == DucksScoreAttribute)
                {
                    this.m_ducksScore = (value == "True" ? true : false);
                }
                else if (lineAttrIt.Current.Name == DucksRadioAttribute)
                {
                    this.m_ducksRadio = (value == "True" ? true : false);
                }
                else if (lineAttrIt.Current.Name == AudibilityAttribute)
                {
                    this.m_audibility = int.Parse(value);
                }
                else if (lineAttrIt.Current.Name == HeadsetSubmixAttribute)
                {
                    this.m_headsetSubmix = bool.Parse(value);
                }
                else if (lineAttrIt.Current.Name == ControllerPadSpeakerAttribute)
                {
                    this.m_controllerPadSpeaker = bool.Parse(value);
                }
                else if (lineAttrIt.Current.Name == TimestampAttribute)
                {
                    this.m_timestamp = DateTime.Now;
                    DateTime time = DateTime.Now;
                    CultureInfo info = CultureInfo.CreateSpecificCulture("en-GB");
                    if (DateTime.TryParse(value, info, DateTimeStyles.None, out time))
                    {
                        this.m_timestamp = time;
                    }
                    else
                    {
                        info = CultureInfo.CreateSpecificCulture("en-US");
                        if (DateTime.TryParse(value, info, DateTimeStyles.None, out time))
                        {
                            this.m_timestamp = time;
                        }
                    }
                }

                if (this.m_characterGuid != this.m_conversation.Mission.Configurations.SfxIdentifier)
                {
                    this.m_manualFilename = false;
                }
                else
                {
                    this.m_manualFilename = true;
                }
            }
        }

        public void Serialise(XmlWriter writer, Configurations config)
        {
            writer.WriteStartElement("AlwaysSubtitle");
            writer.WriteAttributeString("value", this.m_alwaysSubtitled.ToString());
            writer.WriteEndElement();

            writer.WriteElementString("Audibility", config.audibilityMap[this.m_audibility].ToString("D"));
            writer.WriteElementString("AudioFilepath", this.Filename);
            writer.WriteElementString("AudioType", config.audioTypeMap[this.m_audioType].ToString("D"));
            writer.WriteElementString("CharacterId", this.m_characterGuid.ToString("D"));
            writer.WriteElementString("Dialogue", this.m_lineDialogue);

            writer.WriteStartElement("DontInterruptForSpecialAbility");
            writer.WriteAttributeString(
                "value", this.m_dontInterruptForSpecialAbility.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("DucksRadio");
            writer.WriteAttributeString("value", this.m_ducksRadio.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("DucksScore");
            writer.WriteAttributeString("value", this.m_ducksScore.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("HeadsetSubmix");
            writer.WriteAttributeString("value", this.m_headsetSubmix.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("Interruptible");
            writer.WriteAttributeString("value", this.m_interruptible.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("LastModifiedTime");
            writer.WriteAttributeString("value", this.m_timestamp.Ticks.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("Listener");
            writer.WriteAttributeString("value", this.m_listener.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("ManualFilename");
            writer.WriteAttributeString("value", this.m_manualFilename.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("PadSpeaker");
            writer.WriteAttributeString("value", this.m_controllerPadSpeaker.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("Recorded");
            writer.WriteAttributeString("value", this.m_recorded.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("SentForRecording");
            writer.WriteAttributeString("value", this.m_sentToBeRecorded.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("Speaker");
            writer.WriteAttributeString("value", this.m_speaker.ToString());
            writer.WriteEndElement();

            writer.WriteElementString("Special", config.specialMap[this.m_special].ToString("D"));
        }
        #endregion Methods
    } // DialogueProjectCreator.Line {Class}
} // DialogueProjectCreator {Namespace}
