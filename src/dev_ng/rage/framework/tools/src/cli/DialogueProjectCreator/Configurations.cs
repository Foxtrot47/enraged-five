﻿
namespace DialogueProjectCreator
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Xml;
    using System.Xml.XPath;
    using RSG.Base.Collections;
    using RSG.Text.Model;

    /// <summary>
    /// Contains all of the configurations that the model current has on it
    /// </summary>
    public class Configurations
    {
        #region Constants
        private static readonly String VersionAttribute = "version";
        private static readonly String GuidAttribute = "guid";
        private static readonly String NameAttribute = "name";
        private static readonly String VoiceAttribute = "voice";


        private static readonly XPathExpression XmlConfigurationPath = XPathExpression.Compile("/Configurations");
        private static readonly XPathExpression XmlCharacterPath = XPathExpression.Compile("Characters/Character");
        private static readonly XPathExpression XmlVoicesPath = XPathExpression.Compile("Voices/Voice");
        private static readonly XPathExpression XmlSpecialsPath = XPathExpression.Compile("Specials/Special");
        private static readonly XPathExpression XmlVoiceRootPath = XPathExpression.Compile("/Configurations/Voices");
        private static readonly XPathExpression XmlSpecialRootPath = XPathExpression.Compile("/Configurations/Specials");
        private static readonly XPathExpression XmlAmericanExportRootPath = XPathExpression.Compile("/Configurations/Export/American");
        private static readonly XPathExpression XmlAttributes = XPathExpression.Compile("@*");
        #endregion Constants// Constants

        #region Properties
        /// <summary>
        /// This list contains the options for the mission dialogue type.
        /// </summary>
        public System.Collections.ObjectModel.ObservableCollection<String> MissionTypes
        {
            get { return m_missionTypes; }
            set { m_missionTypes = value; }
        }
        private System.Collections.ObjectModel.ObservableCollection<String> m_missionTypes;

        /// <summary>
        /// This list contains the options for the individual conversations
        /// determining the type the conversation
        /// </summary>
        public System.Collections.ObjectModel.ObservableCollection<String> ConversationCategories
        {
            get { return m_conversationCategories; }
            set { m_conversationCategories = value; }
        }
        private System.Collections.ObjectModel.ObservableCollection<String> m_conversationCategories;

        /// <summary>
        /// The characters that the line can be said for.
        /// I'm using a observable dictionary here so that the character strings can be linked to guids
        /// that will mean that the characters can be easily renamed, merged, deleted or added
        /// </summary>
        public ObservableDictionary<Guid, String> Characters
        {
            get { return m_characters; }
            set { m_characters = value; }
        }
        private ObservableDictionary<Guid, String> m_characters;

        /// <summary>
        /// This list contains the options for the individual lines of the 
        /// conversations that determines the volume setting for the line.
        /// </summary>
        public System.Collections.ObjectModel.ObservableCollection<String> VolumeSettings
        {
            get { return m_volumeSettings; }
            set { m_volumeSettings = value; }
        }
        private System.Collections.ObjectModel.ObservableCollection<String> m_volumeSettings;

        /// <summary>
        /// This list contains the options that determines the audio type for
        /// the individual lines
        /// </summary>
        public System.Collections.ObjectModel.ObservableCollection<String> AudioTypes
        {
            get { return m_audioTypes; }
            set { m_audioTypes = value; }
        }
        private System.Collections.ObjectModel.ObservableCollection<String> m_audioTypes;

        /// <summary>
        /// This list contains the options that determines the special variable
        /// for the indiviual lines.
        /// </summary>
        public System.Collections.ObjectModel.ObservableCollection<String> Specials
        {
            get { return m_special; }
            set { m_special = value; }
        }
        private System.Collections.ObjectModel.ObservableCollection<String> m_special;

        /// <summary>
        /// 
        /// </summary>
        public VoiceRepository InstalledVoices
        {
            get;
            set;
        }

        /// <summary>
        /// This maps the character IDs to an installed voice.
        /// </summary>
        public ObservableDictionary<Guid, String> CharacterVoices
        {
            get { return m_characterVoices; }
            set { m_characterVoices = value; }
        }
        private ObservableDictionary<Guid, String> m_characterVoices;

        /// <summary>
        /// The voice root node to serialise back out.
        /// </summary>
        private String VoiceRootInnerMarkup
        {
            get;
            set;
        }

        /// <summary>
        /// The special root node to serialise back out.
        /// </summary>
        private String SpecialRootInnerMarkup
        {
            get;
            set;
        }

        public Guid SfxIdentifier
        {
            get;
            private set;
        }
        #endregion // Properties

        #region Constructor(s)
        /// <summary>
        /// Loads the configuration file given and sets all the other options to there default values
        /// </summary>
        /// <param name="configPath">The path to the config file to load</param>
        public Configurations(String configPath)
        {
            this.InstalledVoices = new VoiceRepository();
            this.m_characters = new ObservableDictionary<Guid, String>();
            this.m_characterVoices = new ObservableDictionary<Guid, String>();
            this.m_missionTypes = new System.Collections.ObjectModel.ObservableCollection<String>();
            this.m_conversationCategories = new System.Collections.ObjectModel.ObservableCollection<String>();
            this.m_volumeSettings = new System.Collections.ObjectModel.ObservableCollection<String>();
            this.m_audioTypes = new System.Collections.ObjectModel.ObservableCollection<String>();
            this.m_special = new System.Collections.ObjectModel.ObservableCollection<String>();

            this.Deserialise(configPath);

            this.m_missionTypes.Add("Mission");
            this.m_missionTypes.Add("Random Event");

            this.m_conversationCategories.Add("Default");
            this.m_conversationCategories.Add("Cellphone");
            this.m_conversationCategories.Add("MoCap");

            this.m_volumeSettings.Add("Normal");
            this.m_volumeSettings.Add("Shout");

            this.m_audioTypes.Add("Normal");
            this.m_audioTypes.Add("Shouted");
            this.m_audioTypes.Add("FrontEnd");


            foreach (string voice in this.InstalledVoices.Voices)
            {
                voiceMap.Add(voice, Guid.NewGuid());
            }

            foreach (string special in this.Specials)
            {
                specialMap.Add(special, Guid.NewGuid());
            }

            foreach (string audioType in this.AudioTypes)
            {
                audioTypeMap.Add(audioType, Guid.NewGuid());
            }

            foreach (string audibility in audibilities)
            {
                audibilityMap.Add(audibilities.IndexOf(audibility), Guid.NewGuid());
            }
        }

        public Configurations(DialogueConfigurations configurations)
        {
            foreach (DialogueVoice voice in configurations.Voices)
            {
                voiceMap.Add(voice.Name, voice.Id);
            }

            foreach (DialogueSpecial special in configurations.Specials)
            {
                specialMap.Add(special.Name, special.Id);
            }

            foreach (DialogueAudioType audioType in configurations.AudioTypes)
            {
                audioTypeMap.Add(audioType.Name, audioType.Id);
            }

            foreach (DialogueAudibility audibility in configurations.Audibilities)
            {
                audibilityMap.Add(audibility.ExportIndex, audibility.Id);
            }
        }
        #endregion // Constructor(s)

        #region Public Function(s)
        public bool DoesCharacterExist(string characterName)
        {
            foreach (KeyValuePair<Guid, string> character in this.Characters)
            {
                if (character.Value == characterName)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Saves the current configurations into the given file.
        /// </summary>
        /// <param name="configPath">The file to save the configurations into</param>
        public void SaveConfigurations(String configPath)
        {
            this.Serialise(configPath);
        }

        /// <summary>
        /// Creates the xml document and loads the given config file then loops through all the configurations deserialise them.
        /// </summary>
        /// <param name="configPath">The config file to load</param>
        private void Deserialise(String configPath)
        {
            XmlDocument document = new XmlDocument();
            try
            {
                document.Load(configPath);
            }
            catch (XmlException ex)
            {
                RSG.Base.Logging.Log.Log__Exception(ex, "Unable to open the config file located at " + configPath);
                return;
            }

            XPathNavigator navigator = document.CreateNavigator();
            XPathNodeIterator configurationsIterator = navigator.Select(XmlConfigurationPath);

            while (configurationsIterator.MoveNext())
            {
                XPathNavigator configurationNavigator = configurationsIterator.Current;
                XPathNodeIterator configurationAttrIterator = configurationNavigator.Select(XmlAttributes);

                // Load the characters
                XPathNodeIterator characterIterator = configurationNavigator.Select(XmlCharacterPath);
                while (characterIterator.MoveNext())
                {
                    XPathNavigator characterNavigator = characterIterator.Current;
                    DeserialiseCharacter(characterNavigator);
                }

                // Load the voices
                XPathNodeIterator voiceIterator = configurationNavigator.Select(XmlVoicesPath);
                while (voiceIterator.MoveNext())
                {
                    XPathNavigator voiceNavigator = voiceIterator.Current;
                    DeserialiseVoice(voiceNavigator);
                }

                XPathNodeIterator voiceRootIterator = document.CreateNavigator().Select(XmlVoiceRootPath);
                while (voiceRootIterator.MoveNext())
                {
                    VoiceRootInnerMarkup = voiceRootIterator.Current.InnerXml;
                    VoiceRootInnerMarkup = VoiceRootInnerMarkup.Replace("\r\n", "\r\n    ");
                    VoiceRootInnerMarkup += "\r\n  ";
                }

                // Load the specials
                XPathNodeIterator specialIterator = configurationNavigator.Select(XmlSpecialsPath);
                while (specialIterator.MoveNext())
                {
                    XPathNavigator specialNavigator = specialIterator.Current;
                    DeserialiseSpecial(specialNavigator);
                }

                XPathNodeIterator specialRootIterator = document.CreateNavigator().Select(XmlSpecialRootPath);
                while (specialRootIterator.MoveNext())
                {
                    SpecialRootInnerMarkup = specialRootIterator.Current.InnerXml;
                    SpecialRootInnerMarkup = SpecialRootInnerMarkup.Replace("\r\n", "\r\n    ");
                    SpecialRootInnerMarkup += "\r\n  ";
                }
            }
        }

        public Dictionary<string, Guid> voiceMap = new Dictionary<string, Guid>();
        public Dictionary<string, Guid> specialMap = new Dictionary<string, Guid>();
        public Dictionary<string, Guid> audioTypeMap = new Dictionary<string, Guid>();
        public Dictionary<int, Guid> audibilityMap = new Dictionary<int, Guid>();
        private List<string> audibilities = new List<string>()
            {
                "Normal",
                "Clear",
                "Critical",
                "LeadIn",
                "Ambient",
            };

        /// <summary>
        /// Saves the current configurations into the given file.
        /// </summary>
        public void Serialise(XmlWriter writer)
        {
            writer.WriteStartElement("Characters");
            foreach (KeyValuePair<Guid, string> character in this.Characters)
            {
                writer.WriteStartElement("Item");
                writer.WriteElementString("Id", character.Key.ToString("D"));
                writer.WriteElementString("Name", character.Value);

                string voice = this.CharacterVoices[character.Key];
                if (voice != null)
                {
                    if (voiceMap.ContainsKey(voice))
                    {
                        writer.WriteElementString("Voice", voiceMap[voice].ToString("D"));
                    }
                }

                if (character.Value == "SFX")
                {
                    writer.WriteStartElement("DontExport");
                    writer.WriteAttributeString("value", "True");
                    writer.WriteEndElement();

                    writer.WriteStartElement("UsesManualFilenames");
                    writer.WriteAttributeString("value", "True");
                    writer.WriteEndElement();
                }

                writer.WriteEndElement();
            }

            writer.WriteEndElement();

            writer.WriteStartElement("Voices");
            foreach (string voice in this.InstalledVoices.Voices)
            {
                writer.WriteStartElement("Item");
                writer.WriteElementString("Name", voice);
                writer.WriteElementString("Id", voiceMap[voice].ToString("D"));
                writer.WriteEndElement();
            }

            writer.WriteEndElement();



            writer.WriteStartElement("Specials");
            foreach (string special in this.Specials)
            {
                writer.WriteStartElement("Item");
                writer.WriteElementString("Name", special);
                writer.WriteElementString("Id", specialMap[special].ToString("D"));

                if (special == "Headset")
                {
                    writer.WriteElementString("Flags", "Headset");
                }
                
                if (special == "Pad_Speaker")
                {
                    writer.WriteElementString("Flags", "PadSpeaker");
                }

                writer.WriteEndElement();
            }

            writer.WriteEndElement();



            writer.WriteStartElement("AudioTypes");
            int exportIndex = 0;
            foreach (string audioType in this.AudioTypes)
            {
                writer.WriteStartElement("Item");
                writer.WriteElementString("Name", audioType);
                writer.WriteElementString("Id", audioTypeMap[audioType].ToString("D"));


                writer.WriteStartElement("ExportIndex");
                writer.WriteAttributeString("value", exportIndex.ToString());
                writer.WriteEndElement();

                writer.WriteEndElement();
                exportIndex++;
            }

            writer.WriteEndElement();


            writer.WriteStartElement("Audibilities");
            exportIndex = 0;
            foreach (string audibility in audibilities)
            {
                writer.WriteStartElement("Item");
                writer.WriteElementString("Name", audibility);
                writer.WriteElementString("Id", audibilityMap[audibilities.IndexOf(audibility)].ToString("D"));

                writer.WriteStartElement("ExportIndex");
                writer.WriteAttributeString("value", exportIndex.ToString());
                writer.WriteEndElement();

                writer.WriteEndElement();
                exportIndex++;
            }

            writer.WriteEndElement();
        }

        /// <summary>
        /// Saves the current configurations into the given file.
        /// </summary>
        /// <param name="configPath">The file to save the configurations into</param>
        private void Serialise(String configPath)
        {
            // Create the xml file and add a header to it.
            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration xmlDecl = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", String.Empty);
            xmlDoc.AppendChild(xmlDecl);

            // Add the root node with the current version in it.
            XmlElement rootNode = xmlDoc.CreateElement("Configurations");
            xmlDoc.InsertBefore(xmlDecl, xmlDoc.DocumentElement);

            // Serialise all of the characters
            XmlElement characterRootNode = xmlDoc.CreateElement("Characters");
            SerialiseCharacters(xmlDoc, characterRootNode);
            rootNode.AppendChild(characterRootNode);

            // Serialise all of the voices
            XmlElement voiceRootNode = xmlDoc.CreateElement("Voices");
            voiceRootNode.InnerXml = this.VoiceRootInnerMarkup;
            rootNode.AppendChild(voiceRootNode);

            // Serialise all of the specials
            XmlElement specialRootNode = xmlDoc.CreateElement("Specials");
            specialRootNode.InnerXml = this.SpecialRootInnerMarkup;
            rootNode.AppendChild(specialRootNode);

            xmlDoc.AppendChild(rootNode);

            // Just before saving just make sure the file sn't read only (if it exists) it should have been checked out, but you never know
            FileInfo fileinfo = new FileInfo(configPath);
            if (fileinfo != null && fileinfo.IsReadOnly && File.Exists(configPath))
            {
                File.SetAttributes(configPath, FileAttributes.Normal);
            }
            xmlDoc.Save(configPath);
        }

        /// <summary>
        /// Deserialises an indiviual character xml path and adds it to the character dictionary
        /// </summary>
        /// <param name="characterNavigator"></param>
        private void DeserialiseCharacter(XPathNavigator characterNavigator)
        {
            XPathNodeIterator characterAttrIterator = characterNavigator.Select(XmlAttributes);

            Guid characterGuid = Guid.NewGuid();
            String characterName = String.Empty;
            String voiceName = String.Empty;

            while (characterAttrIterator.MoveNext())
            {
                if (typeof(String) != characterAttrIterator.Current.ValueType)
                    continue;
                String value = (characterAttrIterator.Current.TypedValue as String);

                if (characterAttrIterator.Current.Name == GuidAttribute)
                {
                    Guid.TryParse(value, out characterGuid);
                }
                else if (characterAttrIterator.Current.Name == NameAttribute)
                {
                    characterName = value;
                }
                else if (characterAttrIterator.Current.Name == VoiceAttribute)
                {
                    voiceName = value;
                }
            }

            this.m_characters.Add(characterGuid, characterName);
            if (characterName == "SFX")
            {
                SfxIdentifier = characterGuid;
            }

            if (String.IsNullOrEmpty(voiceName))
                voiceName = this.InstalledVoices.DefaultVoiceName;

            this.m_characterVoices.Add(characterGuid, voiceName);
        }

        /// <summary>
        /// Deserialises an indiviual voice xml path and adds it to the installed voices
        /// </summary>
        private void DeserialiseVoice(XPathNavigator voiceNavigator)
        {
            XPathNodeIterator voiceAttrIterator = voiceNavigator.Select("text()");
            while (voiceAttrIterator.MoveNext())
            {
                this.InstalledVoices.Voices.Add(voiceAttrIterator.Current.Value);
                break;
            }
        }

        /// <summary>
        /// Deserialises an indiviual voice xml path and adds it to the installed voices
        /// </summary>
        private void DeserialiseSpecial(XPathNavigator voiceNavigator)
        {
            XPathNodeIterator specialAttrIterator = voiceNavigator.Select("text()");
            while (specialAttrIterator.MoveNext())
            {
                this.m_special.Add(specialAttrIterator.Current.Value);
                break;
            }
        }

        /// <summary>
        /// Serialises all of the characters out to the given root node.
        /// </summary>
        /// <param name="xmlDoc">The document that will be saved out</param>
        /// <param name="parentNode">The parent node to append all of the characters in the xml file</param>
        private void SerialiseCharacters(XmlDocument xmlDoc, XmlElement parentNode)
        {
            foreach (KeyValuePair<Guid, String> character in this.Characters)
            {
                XmlElement characterNode = xmlDoc.CreateElement("Character");
                characterNode.SetAttribute(GuidAttribute, character.Key.ToString().ToUpper());
                characterNode.SetAttribute(NameAttribute, character.Value);
                if (this.m_characterVoices.ContainsKey(character.Key))
                    characterNode.SetAttribute(VoiceAttribute, this.m_characterVoices[character.Key]);
                parentNode.AppendChild(characterNode);
            }
        }

        /// <summary>
        /// Serialises all of the voices out to the given root node.
        /// </summary>
        /// <param name="xmlDoc">The document that will be saved out</param>
        /// <param name="parentNode">The parent node to append all of the voices in the xml file</param>
        private void SerialiseVoice(XmlDocument xmlDoc, XmlElement parentNode)
        {
            foreach (String voice in this.InstalledVoices.Voices)
            {
                XmlElement voiceNode = xmlDoc.CreateElement("Voice");
                XmlText xmlText = xmlDoc.CreateTextNode(voice);
                voiceNode.AppendChild(xmlText);
                parentNode.AppendChild(voiceNode);
            }
        }

        #endregion // Private Function(s)

        #region Classes
        public class VoiceRepository
        {
            #region Fields
            String m_defaultVoiceName;
            List<String> m_voices;
            #endregion Fields

            #region Properties
            /// <summary>
            /// The name of the default voice, the voice 
            /// that gets serialised out if no voice is
            /// selected for a character.
            /// </summary>
            public String DefaultVoiceName
            {
                get { return m_defaultVoiceName; }
                private set
                {
                    if (m_defaultVoiceName == value)
                        return;

                    m_defaultVoiceName = value;
                }
            }

            /// <summary>
            /// 
            /// </summary>
            public List<String> Voices
            {
                get { return m_voices; }
                private set
                {
                    if (m_voices == value)
                        return;

                    m_voices = value;
                }
            }
            #endregion Properties// Properties

            #region Constructors
            /// <summary>
            /// Default Constructor
            /// </summary>
            public VoiceRepository()
            {
                this.m_voices = new List<String>();
            }
            #endregion // Constructors
        } // VoiceRepository
        #endregion Classes
    } // DialogueProjectCreator.Configurations
} // DialogueProjectCreator
