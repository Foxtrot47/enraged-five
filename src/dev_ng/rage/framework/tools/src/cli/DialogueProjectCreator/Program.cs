﻿//---------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Rockstar">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace DialogueProjectCreator
{
    using System.IO;
    using System.Xml;
    using RSG.Base.OS;

    class Program
    {
        static void Main(string[] args)
        {
            LongOption[] options = new LongOption[]
            {
                new LongOption("files", LongOption.ArgType.Required, "directory containing old dstar files to group into a single project"),
                new LongOption("config", LongOption.ArgType.Required, "The full path to the configuration file."),
                new LongOption("newconfig", LongOption.ArgType.Required, "The full path where the new configuration file should be saved out."),
                new LongOption("output", LongOption.ArgType.Required, "The full path where the new project file should be saved out.")
            };

            Getopt opt = new Getopt(args, options);
            Configurations config = new Configurations(opt["config"] as string);

            XmlWriterSettings settings = new XmlWriterSettings() { Indent = true };
            using (XmlWriter writer = XmlWriter.Create(opt["newconfig"] as string, settings))
            {
                writer.WriteStartDocument();
                writer.WriteStartElement("Configurations");

                config.Serialise(writer);

                writer.WriteEndElement();
                writer.WriteEndDocument();
            }

            string[] filenames = Directory.GetFiles(opt["files"] as string, "*.dstar");
            foreach (string filename in filenames)
            {
                System.Diagnostics.Debug.Write(filename);
                Dialogue dialogue = new Dialogue(filename, config);

                string directory = Path.GetDirectoryName(filename);
                string name = Path.GetFileNameWithoutExtension(filename);
                string newFilename = directory + @"\NewVersion3\" + name + ".dstar2";

                if (!Directory.Exists(Path.GetDirectoryName(newFilename)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(newFilename));
                }

                using (XmlWriter writer = XmlWriter.Create(newFilename, settings))
                {
                    writer.WriteStartDocument();
                    writer.WriteStartElement("Dialogue");

                    dialogue.Serialise(writer, config);

                    writer.WriteEndElement();
                    writer.WriteEndDocument();
                }
            }
        }
    }
}
