﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using SIO = System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using RSG.Base.Configuration;
using RSG.Base.Extensions;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services;
using RSG.Pipeline.Services.InstancePlacement;
using RSG.Pipeline.Services.Platform.Manifest;

namespace RSG.Pipeline.RpfManifest
{

    /// <summary>
    /// RPF Manifest File Construction Executable.
    /// </summary>
    class Program
    {
        #region Constants
        private static readonly String LOG_CTX = "RPF Manifest";
        private static readonly String OPTION_TASKNAME = "taskname";
        private static readonly String OPTION_OUTPUT = "output";
        private static readonly String OPTION_MAP = "map";
        private static readonly String OPTION_BOUNDS = "bounds";
        private static readonly String OPTION_ASSETBINDING = "assetbindings";
        private static readonly String OPTION_INSTANCE_PLACEMENT = "instancePlacement";
        #endregion // Constants

        /// <summary>
        /// RPF Manifest constructor entry-point.
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        [STAThread]
        static int Main(String[] args)
        {
            // Merge our options with the default pipeline command options.
            // Define our executable options.  Stick with the long options
            // (e.g. --opt) rather than the single character versions.
            LongOption[] opts = new LongOption[] {
                new LongOption(OPTION_TASKNAME, LongOption.ArgType.Required,
                    "XGE taskname; echoed to TTY for XGE error log parsing in AP3."),
                new LongOption(OPTION_OUTPUT, LongOption.ArgType.Required, 
                    "Output manifest file (.imf)."),
                new LongOption(OPTION_MAP, LongOption.ArgType.Required,
                    "Input SceneXml map additions file for map dependencies."),
                new LongOption(OPTION_BOUNDS, LongOption.ArgType.Required,
                    "Input additions file for bounds processor dependencies."),
                new LongOption(OPTION_INSTANCE_PLACEMENT, LongOption.ArgType.Required,
                    "Input additions XML listing file for instance placement processor dependencies."),
                new LongOption(OPTION_ASSETBINDING, LongOption.ArgType.Required,
                    "Input Asset Binding file dependencies.")
            };
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.CreateUniversalLog("RpfManifest");
            CommandOptions options = new CommandOptions(args, opts);

            // Output task name for AP3 XGE logging compatibility.
            if (options.ContainsOption(OPTION_TASKNAME))
            {
                Console.WriteLine("TASK: {0}", (String)options[OPTION_TASKNAME]);
                Console.Out.Flush();
            }

            if (options.ShowHelp)
            {
                Console.WriteLine(options.GetUsage());
                return (Constants.Exit_Failure);
            }

            if (!options.ContainsOption(OPTION_OUTPUT) &&
                !String.IsNullOrWhiteSpace((String)options[OPTION_OUTPUT]))
            {
                log.ErrorCtx(LOG_CTX, "Manifest output file not specified or invalid format.");
                return (Constants.Exit_Failure);
            }

            int exit_code = Constants.Exit_Success;
            try
            {
                // Parse our options.
                String manifestFilename = (String)options[OPTION_OUTPUT];
                String manifestDirectory = SIO.Path.GetDirectoryName(manifestFilename);
                ManifestFile manifest = new ManifestFile(log, manifestFilename);

                // Map Manifest Data.
                if (options.ContainsOption(OPTION_MAP))
                {
                    String mapFilename = (String)options[OPTION_MAP];
                    if (SIO.File.Exists(mapFilename))
                    {
                        log.MessageCtx(LOG_CTX, "Adding map dependencies from {0}.",
                            mapFilename);
                        manifest.AddMapDependencies(mapFilename);
                    }
                    else
                    {
                        log.ErrorCtx(LOG_CTX, "Failed to add map dependencies from {0}.  File does not exist.",
                            mapFilename);
                        exit_code = Constants.Exit_Failure;
                    }
                }

                // Bounds Manifest Data.
                if (options.ContainsOption(OPTION_BOUNDS))
                {
                    String boundsAdditionsFilename = (String)options[OPTION_BOUNDS];
                    if (SIO.File.Exists(boundsAdditionsFilename))
                    {
                        log.MessageCtx(LOG_CTX, "Adding bounds dependencies from {0}.", boundsAdditionsFilename);
                        manifest.AddBoundsDependencies(boundsAdditionsFilename);
                    }
                    else
                    {
                        log.ErrorCtx(LOG_CTX, "Failed to add bounds dependencies from {0}.  File does not exist.",
                            boundsAdditionsFilename);
                        exit_code = Constants.Exit_Failure;
                    }
                }

                // Instance Placement Manifest Data
                if (options.ContainsOption(OPTION_INSTANCE_PLACEMENT))
                {
                    String instancePlacementAdditionsListingFile = (String)options[OPTION_INSTANCE_PLACEMENT];

                    if (SIO.File.Exists(instancePlacementAdditionsListingFile))
                    {
                        IEnumerable<String> instancePlacementAdditions = InstancePlacementManifest.Deserialize(instancePlacementAdditionsListingFile);

                        foreach (String instancePlacementAdditionFilename in instancePlacementAdditions)
                        {
                            if (SIO.File.Exists(instancePlacementAdditionFilename))
                            {
                                log.MessageCtx(LOG_CTX, "Adding instance placement dependencies from {0}.", instancePlacementAdditionFilename);
                                manifest.AddMapDependencies(instancePlacementAdditionFilename);
                            }
                            else
                            {
                                log.ErrorCtx(LOG_CTX, "Failed to add instance placement dependencies from {0}.  File does not exist.",
                                    instancePlacementAdditionFilename);
                                exit_code = Constants.Exit_Failure;
                            }
                        }
                    }
                    else
                    {
                        log.ErrorCtx(LOG_CTX, "Failed to add instance placement dependencies from listing file {0}.  File does not exist.",
                            instancePlacementAdditionsListingFile);
                        exit_code = Constants.Exit_Failure;
                    }
                }

                // Asset Binding Manifest Data.
                if (options.ContainsOption(OPTION_ASSETBINDING))
                {
                    String assetBindingFilename = (String)options[OPTION_ASSETBINDING];
                    if (SIO.File.Exists(assetBindingFilename))
                    {
                        log.MessageCtx(LOG_CTX, "Adding asset bindings from {0}.",
                            assetBindingFilename);
                        IEnumerable<HDTXDAssetBinding> assetBindings = ReadAssetBindings(assetBindingFilename);
                        log.MessageCtx(LOG_CTX, "Loaded {0} HD TXD asset bindings.", assetBindings.Count());
                        manifest.HDTXDAssetBindings.AddRange(assetBindings);
                    }
                    else
                    {
                        log.ErrorCtx(LOG_CTX, "Failed to add asset bindings from {0}.  File does not exist.",
                            assetBindingFilename);
                        exit_code = Constants.Exit_Failure;
                    }
                }

                // Save manifest file.
                if (!SIO.Directory.Exists(manifestDirectory))
                    SIO.Directory.CreateDirectory(manifestDirectory);

                log.MessageCtx(LOG_CTX, "Saving manifest file {0}.", manifestFilename);
                if (!manifest.Save(manifestFilename))
                {
                    log.ErrorCtx(LOG_CTX, "Failed to save manifest file: {0}.",
                        manifestFilename);
                    exit_code = Constants.Exit_Failure;
                }
            }
            catch (Exception ex)
            {
                log.ToolExceptionCtx(LOG_CTX, ex, "Unhandled exception during RPF Manifest construction.");
                exit_code = Constants.Exit_Failure;
            }

            return (exit_code);
        }

        /// <summary>
        /// Read HD TXD Asset Bindings from XML file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns></returns>
        private static IEnumerable<HDTXDAssetBinding> ReadAssetBindings(String filename)
        {            
            XDocument xmlDoc = XDocument.Load(filename);
            IEnumerable<XElement> bindingElements = xmlDoc.Root.XPathSelectElements("/HDTxdAssetBindings/Item");
            IEnumerable<HDTXDAssetBinding> bindings = bindingElements.Select(xmlItem =>
                new HDTXDAssetBinding(xmlItem));
            return (bindings);
        }
    }

} // RSG.Pipeline.RpfManifest
