#
# RSG.Pipeline.Animation.Cutscene.InjectDof.makefile
# - Initally generated by x:/gta5/tools_ng/ironlib/util/projGen/generate_makefiles.rb
# - Edit as required.
#
 
Project RSG.Pipeline.Animation.Cutscene.InjectDof
 
ConfigurationType exe
 
FrameworkVersion 4.0
 
OutputPath $(toolsroot)\ironlib\bin\anim\
 
Files {
	Program.cs
	Directory Properties {
			AssemblyInfo.cs
	}
	rockstar.ico
}
 
ProjectReferences {
	..\..\..\..\..\base\tools\libs\RSG.Base\RSG.Base.csproj
	..\..\..\..\..\base\tools\libs\RSG.ManagedRage\RSG.ManagedRage_2010.vcxproj
	..\..\Libs\RSG.Pipeline.Core\RSG.Pipeline.Core.csproj
	..\..\Libs\RSG.Pipeline.Services\RSG.Pipeline.Services.csproj
}
References {
	System
	System.Core
	System.Xml.Linq
	System.Data.DataSetExtensions
	Microsoft.CSharp
	System.Data
	System.Xml
}
