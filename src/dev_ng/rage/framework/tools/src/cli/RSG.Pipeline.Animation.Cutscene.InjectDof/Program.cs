﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.ManagedRage.ClipAnimation;
using RSG.Pipeline.Core;

namespace RSG.Pipeline.Animation.Cutscene.InjectDof
{
    class Program
    {
        #region Constants
        private static readonly String OPTION_INPUTDIR = "inputdir";
        private static readonly String OPTION_OUTPUTDIR = "outputdir";
        private static readonly String OPTION_ANIMEDIT = "animedit";
        private static readonly String OPTION_TASK_NAME = "taskname";
        private static readonly String LOG_CTX = "Inject Dof";
        #endregion // Constants

        static int RunProcess(string strExe, string strArgs, IUniversalLog log)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.Arguments = strArgs;
            startInfo.FileName = strExe;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.CreateNoWindow = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false;

            log.MessageCtx(LOG_CTX, strExe + " " + strArgs);

            Process proc = Process.Start(startInfo);
            String output = proc.StandardOutput.ReadToEnd();
            proc.WaitForExit();

            if (output != String.Empty)
                log.MessageCtx(LOG_CTX, output);

            return proc.ExitCode;
        }

        [STAThread]
        static int Main(string[] args)
        {
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.CreateUniversalLog("InjectDof");
            try
            {
                // Define our executable options.  Stick with the long options
                // (e.g. --opt) rather than the single character versions.
                Getopt options = new Getopt(args, new LongOption[] {
                new LongOption(OPTION_INPUTDIR, LongOption.ArgType.Required,
                    "Input directory."),
                new LongOption(OPTION_OUTPUTDIR, LongOption.ArgType.Required,
                    "Output directory."),
                new LongOption(OPTION_ANIMEDIT, LongOption.ArgType.Required,
                    "animedit filename."),
                new LongOption(OPTION_TASK_NAME, LongOption.ArgType.Required,
                    "Asset Pipeline 3 Task Name."),
                });

                // Turn args into vars
                String inputDir = options[OPTION_INPUTDIR] as String;
                String outputDir = options[OPTION_OUTPUTDIR] as String;
                String animEditExe = options[OPTION_ANIMEDIT] as String;
                String taskName = options[OPTION_TASK_NAME] == null ? String.Empty : options[OPTION_TASK_NAME] as String;

                if (String.Empty != taskName)
                {
                    Console.WriteLine("TASK: {0}", taskName);
                    log.MessageCtx(LOG_CTX, "TASK: {0}", taskName);
                }

                if (!Directory.Exists(inputDir))
                {
                    log.ErrorCtx(LOG_CTX, "Directory '{0}' does not exist", inputDir);
                    return RSG.Pipeline.Core.Constants.Exit_Failure;
                }

                //log.MessageCtx(LOG_CTX, "Creating directory '{0}'", outputDir);
                //if (!RSG.Pipeline.Services.Animation.DirectoryServices.CreateDirectory(outputDir))
                //{
                //    log.ErrorCtx(LOG_CTX, "Failed to create directory: '{0}'.  Aborting.", outputDir);
                //    return (Constants.Exit_Failure);
                //}

                string[] clipFiles = Directory.GetFiles(inputDir, "*.clip");
                string[] animFiles = Directory.GetFiles(inputDir, "*.anim");

                // Copy all current anim and clips, so we have them all as only selected ones will have the blend tags
                log.MessageCtx(LOG_CTX, "Copying clip files from '{0}' to '{1}'", inputDir, outputDir);
                foreach (string clipFile in clipFiles)
                {
                    File.Copy(clipFile, Path.Combine(outputDir, Path.GetFileName(clipFile)), true);
                }

                log.MessageCtx(LOG_CTX, "Copying anim files from '{0}' to '{1}'", inputDir, outputDir);
                foreach (string animFile in animFiles)
                {
                    File.Copy(animFile, Path.Combine(outputDir, Path.GetFileName(animFile)), true);
                }

                MClip.Init();

                // Inject dof into all anims that have a TagSyncBlendOut tag
                foreach (string clipFile in clipFiles)
                {
                    MClip clip = new MClip(MClip.ClipType.Normal);
                    clip.Load(clipFile);

                    List<MTag> tags = clip.GetTags();
                    foreach (MTag tag in tags)
                    {
                        if (tag.GetName().ToLower() == "tagsyncblendout")
                        {
                            string animFile = Path.ChangeExtension(clipFile, ".anim");

                            if (!File.Exists(animFile))
                            {
                                log.ErrorCtx(LOG_CTX, "File '{0}' does not exist.", animFile);
                                if (Directory.Exists(outputDir))
                                    Directory.Delete(outputDir, true);
                                clip.Dispose();
                                return RSG.Pipeline.Core.Constants.Exit_Failure;
                            }

                            // 134/137 are track numbers specified by runtime.
                            String animEditArgs = String.Format("-anim={0} -out={1} -op add[134,0]=float(0.0);add[137,0]=float(0.0) -nopopups",
                                animFile,
                                Path.Combine(outputDir, Path.GetFileName(animFile)));
                            if (RunProcess(animEditExe, animEditArgs, log) != 0)
                            {
                                if (Directory.Exists(outputDir))
                                    Directory.Delete(outputDir, true);
                                clip.Dispose();
                                return (Constants.Exit_Failure);
                            }
                        }
                    }

                    clip.Dispose();
                }

                return (Constants.Exit_Success);
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Unhandled Exception during InjectDof Process");
                return (Constants.Exit_Failure);
            }
            finally
            {
                LogFactory.ApplicationShutdown();
            }
        }
    }
}
