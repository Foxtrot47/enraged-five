﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using RSG.Base.OS;
using RSG.Pipeline.Core;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;

namespace RSG.Pipeline.Animation.Cutscene.MergeLights
{
    class Program
    {
        #region Constants
        private static readonly String OPTION_INPUTFILE = "inputfile";
        private static readonly String OPTION_OUTPUTFILE = "outputfile";
        private static readonly String OPTION_LIGHTFILE = "lightfile";
        private static readonly String OPTION_CUT_MERGE = "cutmerge";
        private static readonly String OPTION_REMOVEDUPES = "removedupes";
        private static readonly String OPTION_TASK_NAME = "taskname";
        private static readonly String LOG_CTX = "Merge Lights";
        #endregion // Constants

        static int RunProcess(string strExe, string strArgs, IUniversalLog log)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.Arguments = strArgs;
            startInfo.FileName = strExe;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.CreateNoWindow = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false;

            log.MessageCtx(LOG_CTX, strExe + " " + strArgs);

            Process proc = Process.Start(startInfo);
            String output = proc.StandardOutput.ReadToEnd();
            proc.WaitForExit();

            if (output != String.Empty)
                log.MessageCtx(LOG_CTX, output);

            return proc.ExitCode;
        }

        [STAThread]
        static int Main(string[] args)
        {
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.CreateUniversalLog("MergeLights");
            try
            {
                // Define our executable options.  Stick with the long options
                // (e.g. --opt) rather than the single character versions.
                Getopt options = new Getopt(args, new LongOption[] {
                new LongOption(OPTION_INPUTFILE, LongOption.ArgType.Required,
                    "Input filename."),
                new LongOption(OPTION_OUTPUTFILE, LongOption.ArgType.Required,
                    "Output filename."),
                new LongOption(OPTION_LIGHTFILE, LongOption.ArgType.Required,
                    ""),
                new LongOption(OPTION_CUT_MERGE, LongOption.ArgType.Required,
                    ""),
                new LongOption(OPTION_TASK_NAME, LongOption.ArgType.Required, 
                    "Asset Pipeline 3 Task Name."),
                new LongOption(OPTION_REMOVEDUPES, LongOption.ArgType.Optional,
                    ""),
            });
                // Turn args into vars
                String inputFile = options[OPTION_INPUTFILE] as String;
                String outputFile = options[OPTION_OUTPUTFILE] as String;
                String lightFile = options[OPTION_LIGHTFILE] as String;
                String mergeExe = options[OPTION_CUT_MERGE] as String;
                bool removeDupes = options.HasOption(OPTION_REMOVEDUPES);
                String taskName = options[OPTION_TASK_NAME] == null ? String.Empty : options[OPTION_TASK_NAME] as String;

                if (String.Empty != taskName)
                {
                    Console.WriteLine("TASK: {0}", taskName);
                    log.MessageCtx(LOG_CTX, "TASK: {0}", taskName);
                }

                String outputDirectory = Path.GetDirectoryName(outputFile);
                //log.MessageCtx(LOG_CTX, "Creating directory '{0}'", outputDirectory);
                //if (!RSG.Pipeline.Services.Animation.DirectoryServices.CreateDirectory(outputDirectory))
                //{
                //    log.ErrorCtx(LOG_CTX, "Failed to create directory: '{0}'.  Aborting.", outputDirectory);
                //    return (Constants.Exit_Failure);
                //}

                if (!File.Exists(lightFile))
                {
                    if (File.Exists(inputFile))
                    {
                        log.MessageCtx(LOG_CTX, "No light file found, copying file '{0}' to '{1}'", inputFile, outputFile);
                        // Not all scenes have light data, if not just copy the file
                        File.Copy(inputFile, outputFile, true);
                    }
                    else
                    {
                        log.ErrorCtx(LOG_CTX, "File '{0}' does not exist", inputFile);
                        if (Directory.Exists(Path.GetDirectoryName(outputFile)))
                            Directory.Delete(Path.GetDirectoryName(outputFile), true);
                        return (Constants.Exit_Failure);
                    }
                }
                else
                {
                    log.MessageCtx(LOG_CTX, "Merging light file '{0}'", lightFile);

                    String mergeLightArgs = String.Format("-merge -cutFile={0} -outputCutFile={1} -cutLight={2} -hideAlloc -nopopups",
                                                               inputFile, outputFile, lightFile);
                    if (removeDupes)
                        mergeLightArgs += " -removeDupes";

                    if (RunProcess(mergeExe, mergeLightArgs, log) != 0)
                    {
                        if (Directory.Exists(Path.GetDirectoryName(outputFile)))
                            Directory.Delete(Path.GetDirectoryName(outputFile), true);
                        return (Constants.Exit_Failure);
                    }
                }

                return (Constants.Exit_Success);
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Unhandled Exception during Merge Lights Process");
                return (Constants.Exit_Failure);
            }
            finally
            {
                LogFactory.ApplicationShutdown();
            }
        }
    }
}
