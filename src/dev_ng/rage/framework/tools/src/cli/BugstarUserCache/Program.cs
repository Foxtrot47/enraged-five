﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.Math;
using RSG.Base.OS;
using RSG.Model.Bugstar;
using RSG.Base.ConfigParser;
using RSG.Model.Bugstar.Organisation;
using System.Windows.Media.Imaging;
using System.Net;

namespace BugstarUserCache
{

    /// <summary>
    /// Main Program Static Class defining entry-point.
    /// </summary>
    static class Program
    {
        #region Constants
        private static readonly int EXIT_SUCCESS = 0;
        //private static readonly int EXIT_WARNING = 1;
        //private static readonly int EXIT_ERROR = 2;
        private static readonly int EXIT_CRITIC = 3;

        // Generic Options
        //private static readonly String OPTION_FREQ = "freq";
        #endregion // Constants

        #region Static Member Data
        private static readonly String AUTHOR = "Derek Ward";
        private static readonly String EMAIL = "derek.ward@rockstarnorth.com";
        private static readonly String OUT_DIR = "%RS_TOOLSROOT%/bin/CruiseControl/WebDashboard/custom_reports/users/";
        private static readonly bool MUPPETS = false; // dummy images ... literally, for testing.
        private static readonly String DUMMY_URI = "http://2.bp.blogspot.com/-RELQTdg5Eio/TwTO2boq81I/AAAAAAAAJQE/VeOOs2TMmBs/s1600/Rowlf.jpg";
        #endregion // Static Member Data

        #region Entry-Point
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static int Main(String[] args)
        {
            int exit_code = EXIT_SUCCESS;

            #region Option Parsing and Setup
            // Define our executable options.  Stick with the long options
            // (e.g. --opt) rather than the single character versions.
            // Getopt options = new Getopt(args, new LongOption[] {
            //    new LongOption(OPTION_FREQ, LongOption.ArgType.Required, 'b'),
            //});

            ConsoleLog consoleLog = new ConsoleLog();
           // bool nopopups = options.HasOption(OPTION_NOPOPUP) ? true : false;
            #endregion // Option Parsing and Setup

            ConfigGameView gv = new ConfigGameView();
            string username = gv.BugstarReadOnlyUser;
            string password = gv.BugstarReadOnlyPassword; 
            uint bugstarId = gv.BugstarId;

            Log.Log__Message("Bugstar User Cache : {0} {1}", AUTHOR, EMAIL);

            try
            {
                Log.Log__Message("BugstarUserCache Started @ {0} : user {1} {2} {3}", System.DateTime.Now, username, password, bugstarId);

                do
                {
                    Log.Log__Message("Caching users Started @ {0}", System.DateTime.Now);

                    BugstarConnection connection = new BugstarConnection();

                    int piccys_saved = 0;

                    try
                    {
                        // Use the rockstar domain if the user is in the active directory.  If the user is an "external user" i.e. one set up for dev purposes set to false
                        bool useRockstarDomain = false;
                        connection.Login(username, password, useRockstarDomain);

                        RSG.Model.Bugstar.Organisation.Project gta5Project = Project.GetProjectById(connection, bugstarId);

                        int numUsers = gta5Project.Users.Length;
                        int i = 0;

                        foreach (User user in gta5Project.Users)
                        {
                            Log.Log__Message("#{0}/{1} : {2} : {3}", i + 1, numUsers, user.Name, user.UserName);

                            string filePathToSaveImageTo = Environment.ExpandEnvironmentVariables(OUT_DIR + user.UserName + ".jpg");

                            if (MUPPETS)
                            {
                                Uri uri = new Uri(DUMMY_URI);
                                WebClient webClient = new WebClient();
                                webClient.DownloadFile(uri, filePathToSaveImageTo);
                                piccys_saved += 1;
                            }
                            else
                            {
                                BitmapImage userImage = user.Image;
                                if (userImage != null)
                                {
                                    try
                                    {
                                        FileStream stream = new FileStream(filePathToSaveImageTo, FileMode.Create);
                                        JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                                        encoder.QualityLevel = 100;
                                        encoder.Frames.Add(BitmapFrame.Create(userImage));
                                        encoder.Save(stream);
                                        stream.Close();
                                        piccys_saved += 1;
                                    }
                                    catch (Exception e)
                                    {
                                        Log.Log__Error("Exception making image : {0}", e);
                                    }
                                }
                            }

                            i += 1;
                        }
                    }
                    catch (InvalidLoginException e)
                    {
                        Log.Log__Error("Login credentials invalid : {0}", e);
                    }
                    catch (RunException e)
                    {
                        Log.Log__Error("generic bugstar exception : {0}", e);
                    }
                    catch (Exception e)
                    {
                        Log.Log__Error("other exception : {0}", e);
                    }

                    Log.Log__Message("Caching users Finished @ {0} Num saved = {1}", System.DateTime.Now, piccys_saved);
                } while (false);

                Log.Log__Message("BugstarUserCache Ended @ {0}", System.DateTime.Now);  
            }
            catch (Exception ex)
            {
                // Log the exception.
                Log.Log__Exception(ex, "Unhandled exception during BugstarUserCache");
                return (EXIT_CRITIC);
            }

            Environment.ExitCode = exit_code;
            return (exit_code);
        }
        #endregion // Entry-Point

        #region Private Methods
        #endregion // Private Methods
    }

} // BugstarUserCache namespace
