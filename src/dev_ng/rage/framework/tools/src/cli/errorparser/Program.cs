﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;

// derek.ward@rockstarnorth.com - Derek Ward - Rockstar North - 21st June 2010
// parse a file for errors as per regexps contained in code ( at least for now )
// direct error matches to stderr, other matches to stdout.
// But don't output everything else since the point of this minor app is to be fast and to 
// to deal with the likes of 400 MB files!

namespace errorparser
{
    class Program
    {
        // Params : filename, 
        //          additional stderr regex 
        //          additional stdio regex
        static int Main(string[] args)
        {
            int linesRead = 0;
            int errorCount = 0;
            int warningCount = 0;

            if (args.Length < 3)
            {
                Console.Error.WriteLine("Error : Usage:  errorparser.exe filename_to_parse error_regexp other_regexp");
                return -1;
            }

            System.IO.StreamReader file = new System.IO.StreamReader(args[0]);
            if (file != null)
            {
                Regex errorPattern = new Regex(args[1], RegexOptions.IgnoreCase);
                Regex warningPattern = new Regex(args[2], RegexOptions.IgnoreCase);

                string line;
                while ((line = file.ReadLine()) != null)
                {
                    if (errorPattern.IsMatch(line))
                    {
                        Console.Error.WriteLine("{0}: {1}", linesRead, line);
                        errorCount++;
                    }
                    else if (warningPattern.IsMatch(line))
                    {
                        Console.WriteLine("{0}: {1}", linesRead, line);
                        warningCount++;
                    }

                    linesRead++;
                }
                file.Close();
            }         

            return errorCount;
        }
    }
}