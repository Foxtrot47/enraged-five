﻿//---------------------------------------------------------------------------------------------
// <copyright file="ServicesWindow.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.ToolsService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using RSG.Editor.Controls;

    /// <summary>
    /// Services Window; show information about currently available and running
    /// services.
    /// </summary>
    public partial class ServicesWindow : RsMainWindow
    {
        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public ServicesWindow()
        {
            InitializeComponent();
        }
        #endregion // Constructor(s)
    }

} // RSG.ToolsService namespace
