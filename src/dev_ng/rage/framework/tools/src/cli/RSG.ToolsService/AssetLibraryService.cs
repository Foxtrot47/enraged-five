﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using RSG.Base.Logging.Universal;
using RSG.ToolsService.Services;

namespace RSG.ToolsService
{
    partial class AssetLibraryService : ServiceBase
    {
        #region Constants
        private const String SERVICE_NAME = "Asset Library Service";
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private ServiceHost _host;
        private LibraryService _libraryService;
        private IUniversalLog _log;
        #endregion // Member Data

        public AssetLibraryService(IUniversalLog log)
        {
            InitializeComponent();

            this.ServiceName = SERVICE_NAME;
            this._log = log;
        }

        protected override void OnStart(string[] args)
        {
            _libraryService = new LibraryService(this._log, args);
            _host = new ServiceHost(_libraryService);
            try
            {
                _host.Open();
            }
            catch (AddressAlreadyInUseException ex)
            {
                this._log.ToolException(ex, "Exception starting service.");
            }
        }

        protected override void OnStop()
        {
            _host.Close();
        }
    }
}
