﻿//---------------------------------------------------------------------------------------------
// <copyright file="AssetPipelineService.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.ToolsService
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Diagnostics;
    using System.Linq;
    using System.ServiceModel;
    using System.ServiceProcess;
    using System.Threading.Tasks;
    using RSG.Base.Logging.Universal;
    using RSG.ToolsService.Services;

    /// <summary>
    /// Asset Pipeline Windows Service.
    /// </summary>
    public partial class AssetPipelineService : ServiceBase
    {
        #region Constants
        private const String SERVICE_NAME = "Asset Pipeline Services";
        #endregion // Constants

        #region Member Data
        /// <summary>
        /// 
        /// </summary>
        private ServiceHost _host;
        private EngineProgressService _engineProgressService;
        private IUniversalLog _log;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="log"></param>
        public AssetPipelineService(IUniversalLog log)
        {
            InitializeComponent();
            this.ServiceName = SERVICE_NAME;
            this._log = log;
        }
        #endregion // Constructor(s)

        #region Controller Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(String[] args)
        {
            _engineProgressService = new EngineProgressService(this._log);
            _host = new ServiceHost(_engineProgressService);
            try
            {
                _host.Faulted += EngineProgressServiceFaulted;
                foreach (Uri baseAddress in _host.BaseAddresses)
                    this._log.Message("Engine Progress Service hosted at {0}.", baseAddress.AbsoluteUri);
                
                this._log.Message("WCF Host Startup.");
                _host.Open();
            }
            catch (AddressAlreadyInUseException ex)
            {
                this._log.ToolException(ex, "Exception starting service.");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        protected override void OnStop()
        {
            _host.Close();
        }
        #endregion // Controller Methods

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void EngineProgressServiceFaulted(Object sender, EventArgs e)
        {
            this._log.Error("Engine Progress Service has faulted.");
        }
        #endregion // Private Methods
    }

} // RSG.ToolsService namespace
