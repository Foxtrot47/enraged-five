﻿//---------------------------------------------------------------------------------------------
// <copyright file="ToolsServiceApp.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.ToolsService
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel.Composition;
    using System.ComponentModel.Composition.Hosting;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using RSG.Base.Logging;
    using RSG.Base.Logging.Universal;
    using RSG.Configuration;
    using RSG.Editor;
    using RSG.Editor.Controls;
    using RSG.Editor.Controls.Bugstar;
    using RSG.Editor.Controls.Perforce;
    using RSG.Editor.SharedCommands;
    using RSG.ToolsService.Contracts;
    using Microsoft.Win32;
    using System.ComponentModel;

    /// <summary>
    /// 
    /// </summary>
    internal class ToolsServiceApp : 
        RsApplication, 
        IPartImportsSatisfiedNotification
    {
        #region Properties
        /// <summary>
        /// Gets the unique name for this application that is used for mutex creation, and
        /// folder organisation inside the local app data folder and the registry.
        /// </summary>
        public override String UniqueName
        {
            get { return "ToolsService"; }
        }

        /// <summary>
        /// Services view-model.
        /// </summary>
        public ServicesViewModel ServicesViewModel
        {
            get { return _servicesViewModel; }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Universal log.
        /// </summary>
        private IUniversalLog _log;

        /// <summary>
        /// Command options object.
        /// </summary>
        private CommandOptions _options;

        /// <summary>
        /// MEF composition container.
        /// </summary>
        private CompositionContainer _container;

        /// <summary>
        /// MEF composed IToolsService objects.
        /// </summary>
        [ImportMany(typeof(IToolsService))]
        private IEnumerable<IToolsService> _services;

        /// <summary>
        /// Services view-model; states etc.
        /// </summary>
        private ServicesViewModel _servicesViewModel;

        /// <summary>
        /// Perforce service object (null until requested in GetService).
        /// </summary>
        private IPerforceService _perforceService;

        /// <summary>
        /// Bugstar service object (null until requested in GetService).
        /// </summary>
        private IBugstarService _bugstarService;

        /// <summary>
        /// The task bar icon that this application is hosted in.
        /// </summary>
        private RsTaskbarIcon _taskbarIcon;
        #endregion // Member Data

        #region Entry-Point
        /// <summary>
        /// Main entry point into the application. The first thing to get called.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            ToolsServiceApp app = new ToolsServiceApp();
            app.ShutdownMode = ShutdownMode.OnExplicitShutdown;
            RsApplication.OnEntry(app, ApplicationMode.Single);
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ToolsServiceApp()
        {
            this._options = new CommandOptions(new String[0], new RSG.Base.OS.LongOption[0]);
            this._log = LogFactory.ApplicationLog;
            this._services = new List<IToolsService>();
            this._servicesViewModel = null; // Initialised in OnImportsSatisfied.
            this._perforceService = null; // Initialised when first requested.
            this._bugstarService = null; // Initialised when first requested.

            // Merge in the resource dictionary that contains our taskbar icon.
            MergeTaskbarIconResources();
        }
        #endregion // Entry-Point

        #region IPartImportsSatisfiedNotification Interface Methods
        /// <summary>
        /// 
        /// </summary>
        public void OnImportsSatisfied()
        {
            this._servicesViewModel = new ServicesViewModel(this._services);
        }
        #endregion // IPartImportsSatisfiedNotification Interface Methods

        #region RsApplication Overrides
        /// <summary>
        /// Gets the service object of the specified type.
        /// </summary>
        /// <param name="serviceType">
        /// An object that specifies the type of service object to get.
        /// </param>
        /// <returns>
        /// The service of the specified type if found; otherwise, null.
        /// </returns>
        public override Object GetService(Type serviceType)
        {
            if (typeof(IPerforceService) == serviceType)
            {
                if (null == this._perforceService)
                    this._perforceService = new PerforceService();
                return (this._perforceService);
            }
            else if (typeof(IBugstarService) == serviceType)
            {
                if (null == this._bugstarService)
                    this._bugstarService = new BugstarService(LogFactory.ApplicationLog, this._options.Project);
                return (this._bugstarService);
            }
            return (base.GetService(serviceType));
        }

        /// <summary>
        /// Override to create the main window for the application.
        /// </summary>
        /// <returns>
        /// The System.Windows.Window that will be the main window for this application.
        /// </returns>
        protected override Window CreateMainWindow()
        {
            // Application doesn't have a main window.
            return (null);
        }

        /// <summary>
        /// Starts the shortcut menu service.
        /// </summary>
        /// <param name="e">
        /// The event data.
        /// </param>
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            Initialise(_log);
            Application.Current.Exit += HandleOnExiting;

            _log.Message("Starting services.");
            foreach (IToolsService service in _services)
            {
                if (ServiceState.Disabled == service.Status)
                    continue;

                service.Start(_log, this.CommandOptions.Config);
            }

            // Create the taskbar icon by finding the resource.
            _log.Message("Initialising the taskbar icon.");
            _taskbarIcon = (RsTaskbarIcon)FindResource("TaskbarIcon");
            _taskbarIcon.ToolTipText = RSG.ToolsService.Resources.StringTable.ApplicationToolTip;

            // Subscribe to system-events.
            SystemEvents.SessionEnding += SystemEvents_SessionEnding;
        }

        /// <summary>
        /// Make sure we notify the long running tasks that they should shutdown.
        /// </summary>
        /// <param name="persistentDirectory"></param>
        protected void HandleOnExiting(object sender, ExitEventArgs e)
        {
            // Unsubscribe from system-events.
            SystemEvents.SessionEnding -= SystemEvents_SessionEnding;
            
            _log.Message("Stopping services.");
            foreach (IToolsService service in _services)
            {
                if (ServiceState.Running != service.Status)
                    continue;

                service.Stop(_log);
            }
        }
        #endregion // RsApplication Overrides

        #region Private Methods
        /// <summary>
        /// Makes sure that the task bar icon resource dictionary is merged in with the
        /// applications resources.
        /// </summary>
        private void MergeTaskbarIconResources()
        {
            Collection<ResourceDictionary> resources = this.Resources.MergedDictionaries;
            if (resources == null)
            {
                return;
            }

            string dictionaryPath = "pack://application:,,,/RSG.ToolsService;component/TaskbarIconResources.xaml";
            Uri source = new Uri(dictionaryPath, System.UriKind.RelativeOrAbsolute);

            ResourceDictionary newDictionary = new ResourceDictionary();
            try
            {
                newDictionary.Source = source;
            }
            catch (Exception)
            {
                Debug.Assert(newDictionary != null, "Unable to create the standard themes.");
                return;
            }

            resources.Add(newDictionary);
        }

        /// <summary>
        /// One-time initialisation method.
        /// </summary>
        /// <param name="log"></param>
        private void Initialise(IUniversalLog log)
        {
            log.Debug("ToolsServiceApp::Initialise()");

            Assembly currentAssembly = Assembly.GetCallingAssembly();
            String assemblyDirectory = System.IO.Path.GetDirectoryName(currentAssembly.Location);

            AggregateCatalog catalog = new AggregateCatalog();
            catalog.Catalogs.Add(new AssemblyCatalog(currentAssembly));
            catalog.Catalogs.Add(new DirectoryCatalog(assemblyDirectory, "RSG.ToolsService.*.dll"));

            _container = new CompositionContainer(catalog);
            try
            {
                _container.ComposeParts(this);
            }
            catch (CompositionException ex)
            {
                log.ToolException(ex, "ToolsService MEF Composition Exception");
                foreach (CompositionError error in ex.Errors)
                    log.ToolException(error.Exception, error.Description);
            }
        }
        
        /// <summary>
        /// System Event Handler: Session Ending.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SystemEvents_SessionEnding(Object sender, SessionEndingEventArgs e)
        {
            _log.Debug("ToolsServiceApp::SystemEvents_SessionEnding()");
            _log.Message("User-session ending; notifying services.");
            foreach (IToolsService service in _services)
                service.Stop(_log);
        }
        #endregion // Private Methods
    }

} // RSG.ToolsService namespace
