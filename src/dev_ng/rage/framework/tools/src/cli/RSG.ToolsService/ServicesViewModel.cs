﻿//---------------------------------------------------------------------------------------------
// <copyright file="ServicesViewModel.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2015. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.ToolsService
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using RSG.Editor;
    using RSG.ToolsService.Contracts;
    
    /// <summary>
    /// 
    /// </summary>
    internal class ServicesViewModel : NotifyPropertyChangedBase
    {
        #region Properties
        /// <summary>
        /// Service window title.
        /// </summary>
        public String ServiceWindowTitle
        {
            get { return _serviceWindowTitle; }
            private set { this.SetProperty(ref _serviceWindowTitle, value); }
        }

        /// <summary>
        /// Available services.
        /// </summary>
        public ObservableCollection<IToolsService> Services
        {
            get { return _services; }
            private set { this.SetProperty(ref _services, value); }
        }
        #endregion // Properties

        #region Member Data
        /// <summary>
        /// Services window title.
        /// </summary>
        private String _serviceWindowTitle;

        /// <summary>
        /// Services data.
        /// </summary>
        private ObservableCollection<IToolsService> _services;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="services"></param>
        public ServicesViewModel(IEnumerable<IToolsService> services)
        {
            this.ServiceWindowTitle = Resources.StringTable.ServiceWindowTitle;
            this.Services = new ObservableCollection<IToolsService>(services);            
        }
        #endregion // Constructor(s)
    }

} // RSG.ToolsService namespace
