﻿//---------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.ToolsService
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// Tools services main class.
    /// </summary>
    static class Program
    {

        #region Entry-Point
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            try
            {



                // Run the services in the right mode.
                if (!Environment.UserInteractive)
                {
                    log.Message("Starting services.");
                    ServiceBase.Run(ServicesToRun);
                }
                else
                {
                    // Allocate console so we can see what is happening.
                    RSG.Interop.Microsoft.Windows.Kernel32.AllocConsole();
                    LogFactory.CreateApplicationConsoleLogTarget();
                    RunInteractively(log, ServicesToRun);
                }
            }
            catch (ConfigurationException ex)
            {
                using (StreamWriter writer = new StreamWriter(@"x:\RSG.ToolsService.log"))
                {
                    writer.WriteLine("ConfigurationException: {0}", ex.Message);
                    writer.Write(ex.StackTrace);
                    writer.WriteLine();
                }
            }
            catch (Exception ex)
            {
                using (StreamWriter writer = new StreamWriter(@"x:\RSG.ToolsService.log"))
                {
                    writer.WriteLine("Exception: {0}", ex.Message);
                    writer.Write(ex.StackTrace);
                    writer.WriteLine();
                }
            }
        }
        #endregion // Entry-Point

        #region Private Methods

        /// <summary>
        /// Run services interactively (for debugging purposes).
        /// </summary>
        /// <param name="log"></param>
        /// <param name="services"></param>
        private static void RunInteractively(ILog log, ServiceBase[] services)
        {
            log.Message("Services running in interactive mode.");
            
            MethodInfo onStartMethod = typeof(ServiceBase).GetMethod("OnStart", 
                BindingFlags.Instance | BindingFlags.NonPublic);
            foreach (ServiceBase service in services)
            {
                log.Message("Starting {0}...", service.ServiceName);
                onStartMethod.Invoke(service, new object[] { new string[] { } });
                log.Message("Started");
            }

            log.Message("Press any key to stop the services and end the process...");
            Console.ReadKey();
            
            MethodInfo onStopMethod = typeof(ServiceBase).GetMethod("OnStop", 
                BindingFlags.Instance | BindingFlags.NonPublic);
            foreach (ServiceBase service in services)
            {
                log.Message("Stopping {0}...", service.ServiceName);
                onStopMethod.Invoke(service, null);
                log.Message("Stopped");
            }

            log.Message("All services stopped.  Shutting down.");
            // Keep the console alive for a second to allow the user to see the message.
            System.Threading.Thread.Sleep(1000);
        }
        #endregion // Private Methods
    }

} // RSG.ToolsService namespace
