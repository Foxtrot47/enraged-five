﻿#
# RSG.ToolsService.makefile
#

Project RSG.ToolsService

BuildTemplate $(toolsroot)/etc/projgen/RSG.Pipeline.Executable.build

ConfigurationType WinExe

FrameworkVersion 4.5

OutputPath $(toolsroot)\bin\ToolsService\

Files {
Directory Resources {
	Restart_6322.png
	Services_5724.ico
	StringTable.resx
	StringTable.Designer.cs
}
Directory Properties {
	AssemblyInfo.cs
}
Directory Services {
	LogDirectoryMaintenanceService.cs
}
app.config
Services_5724.ico
ServicesViewModel.cs
ServicesWindow.xaml
ServicesWindow.xaml.cs
TaskbarIconResources.xaml
TaskbarIconResources.xaml.cs
ToolsServiceApp.cs
}

ProjectReferences {
..\..\..\..\..\base\tools\libs\RSG.Base\RSG.Base.csproj
..\..\..\..\..\base\tools\libs\RSG.Configuration\RSG.Configuration.csproj
..\..\..\..\..\base\tools\libs\RSG.Interop.Microsoft\RSG.Interop.Microsoft.csproj
..\..\..\..\..\framework\tools\src\libs\RSG.ToolsService.Contracts\RSG.ToolsService.Contracts.csproj
..\..\..\..\..\framework\tools\src\libs\RSG.ToolsService.GameConnection\RSG.ToolsService.GameConnection.csproj
..\..\..\..\..\framework\tools\src\ui\libs\RSG.Editor\RSG.Editor.csproj
..\..\..\..\..\framework\tools\src\ui\libs\RSG.Editor.Controls\RSG.Editor.Controls.csproj
..\..\..\..\..\framework\tools\src\ui\libs\RSG.Editor.Controls.Bugstar\RSG.Editor.Controls.Bugstar.csproj
..\..\..\..\..\framework\tools\src\ui\libs\RSG.Editor.Controls.Perforce\RSG.Editor.Controls.Perforce.csproj
..\..\..\..\..\framework\tools\src\libs\RSG.Pipeline.Services.Engine\RSG.Pipeline.Services.Engine.csproj
}
References {
Microsoft.CSharp
PresentationCore
PresentationFramework
System
System.ComponentModel.Composition
System.Core
System.Data
System.Data.DataSetExtensions
System.ServiceModel
System.Xaml
System.Xml.Linq
System.Xml
WindowsBase
}
