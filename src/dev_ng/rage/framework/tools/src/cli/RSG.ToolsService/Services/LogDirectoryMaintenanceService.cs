﻿//---------------------------------------------------------------------------------------------
// <copyright file="LogDirectoryMaintenanceService.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.ToolsService.Services
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.Composition;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Threading;
    using RSG.Base.Extensions;
    using RSG.Base.Logging.Universal;
    using RSG.Base.Xml;
    using RSG.Configuration;
    using RSG.ToolsService.Contracts;

    /// <summary>
    /// 
    /// </summary>
    [Export(typeof(IToolsService))]
    class LogDirectoryMaintenanceService : 
        ToolsServiceBase,
        IToolsService
    {
        #region Constants
        private const String ParamFilename = @"services\LogDirectoryMaintenanceService.meta";

        private const String ParamLogAge = "Log Age";
        private const int ParamLogAgeDefault = 21 * 24 * 60 * 60; // 21 days.

        private const String ParamCheckFrequency = "Check Frequency";
        private const int ParamCheckFrequencyDefault = 60 * 60; // 1 hour tick rate.

        private const String ParamMaximumFilesPerIteration = "Maximum Files Per Iteration";
        private const int ParamMaximumFilesPerIterationDefault = 1000;
        #endregion // Constants
        
        #region Member Data
        /// <summary>
        /// Service parameters.
        /// </summary>
        private ParameterDictionary _parameters;

        /// <summary>
        /// Dispatch timer.
        /// </summary>
        private DispatcherTimer _dispatcherTimer;

        /// <summary>
        /// Process config data.
        /// </summary>
        private IConfig _config;

        /// <summary>
        /// Process log.
        /// </summary>
        private IUniversalLog _log;
        #endregion // Member Data

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public LogDirectoryMaintenanceService()
        {
            this.ServiceName = Resources.StringTable.LogDirectoryMaintenanceServiceName;
            this.Description = Resources.StringTable.LogDirectoryMaintenanceServiceDescription;
        }
        #endregion // Constructor(s)

        #region ToolsServiceBase Methods
        /// <summary>
        /// Service call invoked when services are starting.
        /// </summary>
        /// <param name="log"></param>
        /// <param name="config"></param>
        protected override void OnStart(IUniversalLog log, IConfig config)
        {
            log.DebugCtx(this.ServiceName, "LogDirectoryMaintenanceService::OnStart()");

            try
            {
                ReloadParameters(config);
                double timeTick = (double)this._parameters.GetParameter(ParamCheckFrequency,
                    ParamCheckFrequencyDefault);
                _config = config;
                _log = log;

                // Setup dispatch timer to invoke task.
                _dispatcherTimer = new DispatcherTimer();
                _dispatcherTimer.Interval = TimeSpan.FromSeconds(timeTick);
                _dispatcherTimer.Tick += ClearOldLogFilesTask;
                _dispatcherTimer.Start();
            }
            catch (Exception ex)
            {
                log.ToolExceptionCtx(this.ServiceName, ex, "Exception starting service.");
            }
        }

        /// <summary>
        /// Service call invoked when services are shutting down.
        /// </summary>
        /// <param name="log"></param>
        protected override void OnStop(IUniversalLog log)
        {
            log.DebugCtx(this.ServiceName, "LogDirectoryMaintenanceService::OnStop()");

            _dispatcherTimer.Stop();
        }
        #endregion // ToolsServiceBase Interface Methods

        #region Private Methods
        /// <summary>
        /// Reload our parameter dictionary.
        /// </summary>
        /// <param name="config"></param>
        private void ReloadParameters(IConfig config)
        {
            String configFilename = System.IO.Path.Combine(config.ToolsConfigDirectory, ParamFilename);
            if (System.IO.File.Exists(configFilename))
            {
                this._parameters = ParameterDictionary.Load(configFilename);
            }
            else
            {
                this._parameters = new ParameterDictionary();
            }
        }

        /// <summary>
        /// Task thread method.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ClearOldLogFilesTask(Object sender, EventArgs e)
        {
            // This ensures the process doesn't just suddenly clean the
            // log directory on startup.
            ReloadParameters(_config);
            double timeTick = (double)this._parameters.GetParameter(ParamCheckFrequency,
                ParamCheckFrequencyDefault);

            // Update timer interval if the parameter has been changed in config file.
            _dispatcherTimer.Interval = TimeSpan.FromSeconds(timeTick);

            _log.MessageCtx(this.ServiceName, "Checking Log Directory for out of date log files.");
            IEnumerable<String> oldLogDirs = GetOldLogDirectories(_config);
            foreach (String logDirectory in oldLogDirs)
            {
                try
                {
                    Directory.Delete(logDirectory, true);
                }
                catch (IOException)
                {
                }
            }
        }

        /// <summary>
        /// Return all old log directories.
        /// </summary>
        /// <param name="config"></param>
        /// <returns></returns>
        private IEnumerable<String> GetOldLogDirectories(IConfig config)
        {
            double ageInDays = (double)this._parameters.GetParameter(ParamLogAge, ParamLogAgeDefault);
            DateTime currentAgeLimit = DateTime.Now - TimeSpan.FromSeconds(ageInDays);

            // We only delete a maximum files of files to ensure we don't lock
            // the users machine for too long deleting files.
            int maximumPerIteration = (int)this._parameters.GetParameter(ParamMaximumFilesPerIteration,
                ParamMaximumFilesPerIterationDefault);

            IList<String> oldLogDirectories = new List<String>();
            IEnumerable<String> allLogDirectories = Directory.GetDirectories(config.ToolsLogsDirectory, "*.*", 
                SearchOption.TopDirectoryOnly);
            foreach (String logDirectory in allLogDirectories)
            {
                try
                {
                    DateTime lastWriteTime = Directory.GetLastWriteTime(logDirectory);
                    if (lastWriteTime.IsEarlierThan(currentAgeLimit))
                        oldLogDirectories.Add(logDirectory);
                }
                catch (IOException)
                {
                    // Ignore.
                }
            }
            return (oldLogDirectories);
        }
        #endregion // Private Methods
    }

} // RSG.ToolsService.Services namespace
