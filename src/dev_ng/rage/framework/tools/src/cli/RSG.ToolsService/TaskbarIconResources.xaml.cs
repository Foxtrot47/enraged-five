﻿//---------------------------------------------------------------------------------------------
// <copyright file="TaskbarIconResources.cs" company="Rockstar Games">
//     Copyright © Rockstar Games 2014. All rights reserved
// </copyright>
//---------------------------------------------------------------------------------------------

namespace RSG.ToolsService
{
    using System;
    using System.Diagnostics;
    using SIO = System.IO;
    using System.Windows;
    using RSG.Base.Extensions;
    using RSG.Base.OS;
    using RSG.Editor;
    using RSG.Editor.Controls;

    /// <summary>
    /// 
    /// </summary>
    internal partial class TaskbarIconResources : ResourceDictionary
    {
        #region Constants
        #endregion // Constants

        #region Constructor(s)
        /// <summary>
        /// Default constructor.
        /// </summary>
        public TaskbarIconResources()
        {
            InitializeComponent();
        }
        #endregion // Constructor(s)

        #region Private Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ViewLocalServicesClick(Object sender, RoutedEventArgs e)
        {
            ServicesWindow servicesWindow = new ServicesWindow();
            servicesWindow.DataContext = ((ToolsServiceApp)RsApplication.Current).ServicesViewModel;
            servicesWindow.ShowDialog();
        }

        /// <summary>
        /// Exit application; signalling bootstrapper to restart process.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RestartClick(Object sender, RoutedEventArgs e)
        {
            Process process = Process.GetCurrentProcess();
            if (!process.IsBootstrapped())
            {
                String startupScript = Environment.ExpandEnvironmentVariables(@"%RS_TOOLSROOT%\bin\ToolsService.bat");
                RsMessageBox.Show(String.Format(Resources.StringTable.ApplicationRestartNoBootstrapper, SIO.Path.GetFullPath(startupScript)),
                    Resources.StringTable.ApplicationCaption);
            }

            RsApplication app = (RsApplication)Application.Current;
            app.Shutdown(RSG.Base.OS.ExitCode.Restart);
        }

        /// <summary>
        /// Exit application.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitClick(Object sender, RoutedEventArgs e)
        {
            RsApplication app = (RsApplication)Application.Current;
            app.Shutdown(RSG.Base.OS.ExitCode.Success);
        }
        #endregion // Private Methods
    }

} // RSG.ToolsService namespace
