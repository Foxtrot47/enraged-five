﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using RSG.Base.ConfigParser;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Model.Character;
using RSG.Model.Common;
using RSG.Model.Report;
using RSG.Model.Vehicle;
using System.Reflection;
using RSG.Model.Report.Reports;
using RSG.Model.Asset.Level;
using RSG.Model.Asset.Platform;
using RSG.Model.Weapon;
using RSG.Base.Configuration.Reports;
using RSG.Base.Configuration.Bugstar;
using RSG.Base.Configuration;
using RSG.Model.Common.Map;
using RSG.Model.Map3;
using RSG.Interop.Bugstar;
using System.Threading;

namespace ReportGenerator
{
    class Program
    {
        #region Constants
        private static readonly int EXIT_SUCCESS = 0;
        private static readonly int EXIT_WARNING = 1;
        private static readonly int EXIT_ERROR = 2;
        private static readonly int EXIT_CRITIC = 3;
        
        // Report Options
        private static readonly String OPTION_LEVEL = "level";
        private static readonly String OPTION_BUILD = "build";
        private static readonly String OPTION_OUTPUT_DIRECTORY = "outputdir";
        private static readonly String OPTION_REPORTS = "reports";
        private static readonly String OPTION_GEOMETRY_STATS_PATHNAME = "geomstats";
        private static readonly String OPTION_P4EDIT = "p4edit";
        private static readonly String OPTION_P4SUBMIT = "p4submit";
        private static readonly String OPTION_CL_DESCRIPTION = "cldesc";
        private static readonly String OPTION_SKIP_LEVEL_LOAD = "skiplevelload";

        // Generic Options
        private static readonly String OPTION_NOPOPUP = "nopopups";
        #endregion // Constants

        #region Static Member Data
        private static readonly String AUTHOR = "David Muir";
        private static readonly String EMAIL = "david.muir@rockstarnorth.com";

        private static readonly String P4_CHANGELIST_DESCRIPTION = "Automatic report generation for {0} build version {1}";
        private static readonly String P4_CHANGELIST_DESCRIPTION_NO_BUILD = "Automatic report generation for {0}";
        
        /// <summary>
        /// Local log.
        /// </summary>
        private static IUniversalLog s_log;
        #endregion // Member Data

        #region Entry-Point
        /// <summary>
        /// The main entry point for the application.
        /// Attribute [STAThread] for invoking the universal log from the right thread
        /// </summary>
        [STAThread]
        static int Main(String[] args)
        {
            LongOption[] opts = new LongOption[]
            { 
                new LongOption(OPTION_LEVEL, LongOption.ArgType.Required,
                    "Level name to load."),
                new LongOption(OPTION_BUILD, LongOption.ArgType.Required,
                    "Build directory."),
                new LongOption(OPTION_OUTPUT_DIRECTORY, LongOption.ArgType.Required,
                    "Report output directory."),
                new LongOption(OPTION_REPORTS, LongOption.ArgType.Required,
                    "Comma-separated list of reports to run."),
                new LongOption(OPTION_GEOMETRY_STATS_PATHNAME, LongOption.ArgType.Required,
                    ""),
                new LongOption(OPTION_P4EDIT, LongOption.ArgType.None,
                    "Whether report files are checked-out."),
                new LongOption(OPTION_P4SUBMIT, LongOption.ArgType.None,
                    "Whether report files are checked-in."),
                new LongOption(OPTION_CL_DESCRIPTION, LongOption.ArgType.Required,
                    "Report checkin changelist description."),
                new LongOption(OPTION_SKIP_LEVEL_LOAD, LongOption.ArgType.None,
                    "Whether the level load is skipped (not required for all reports)."),
            };
            CommandOptions options = new CommandOptions(args, opts);

            // Create the log
            s_log = LogFactory.CreateUniversalLog("ReportGenerator");
            LogFactory.CreateApplicationConsoleLogTarget();
            UniversalLogFile logfile = LogFactory.CreateUniversalLogFile(s_log) as UniversalLogFile;

            int exit_code = EXIT_SUCCESS;

            #region Option Parsing and Setup
            ConfigGameView gv = new ConfigGameView();

            String outputDir = options.ContainsOption(OPTION_OUTPUT_DIRECTORY) ? (options[OPTION_OUTPUT_DIRECTORY] as String) : String.Empty;
            String levelName = options.ContainsOption(OPTION_LEVEL) ? (options[OPTION_LEVEL] as String) : String.Empty;
            String buildId = options.ContainsOption(OPTION_BUILD) ? (options[OPTION_BUILD] as String) : null;
            List<string> reports = options.ContainsOption(OPTION_REPORTS) ? (options[OPTION_REPORTS] as String).Split(new char[] { ',' }).ToList() : new List<string>();
            string geometryStatsPathname = options.ContainsOption(OPTION_GEOMETRY_STATS_PATHNAME) ? (options[OPTION_GEOMETRY_STATS_PATHNAME] as String) : String.Empty;
            bool p4submit = options.ContainsOption(OPTION_P4SUBMIT);
            bool p4edit = options.ContainsOption(OPTION_P4EDIT) ? true : p4submit;
            string clDesc = options.ContainsOption(OPTION_CL_DESCRIPTION) ? (options[OPTION_CL_DESCRIPTION] as String) : null;
            bool skipLevelLoad = options.ContainsOption(OPTION_SKIP_LEVEL_LOAD);
            bool nopopups = options.ContainsOption(OPTION_NOPOPUP) ? true : false;
            #endregion // Option Parsing and Setup

            // Changelist that is created for the files
            int p4Changelist = 0;

            try
            {
                // Ensure that the output dir is a full path and that the directory exists
                outputDir = Path.GetFullPath(outputDir);
                if (!Directory.Exists(outputDir))
                {
                    Directory.CreateDirectory(outputDir);
                }

                // Set default level.
                if (String.IsNullOrWhiteSpace(levelName) && !skipLevelLoad)
                {
                    ContentNodeLevel level = (gv.Content.Root.FindFirst("", "level") as ContentNodeLevel);
                    if (null != level)
                    {
                        levelName = level.Name;
                    }
                    else
                    {
                        s_log.Error("No levels defined in content tree.  Aborting.");
                        exit_code = EXIT_CRITIC;
                    }
                }

                // Generate the list of reports that we wish to generate
                s_log.Profile("Gathering reports");
                IDictionary<IReport, string> reportsToGenerate = GatherReportsToGenerate(options.Config, reports, outputDir);
                s_log.ProfileEnd();

                // Do we need to check files out?
                if (p4edit)
                {
                    p4Changelist = CheckoutFiles(reportsToGenerate, levelName, buildId, clDesc);
                }

                s_log.Message("Generating requested reports.");
                if (!RunReports(levelName, outputDir, reportsToGenerate, geometryStatsPathname, gv, options.Config, skipLevelLoad))
                {
                    exit_code = EXIT_ERROR;
                }

                if (p4edit)
                {
                    AddFilesToSourceControl(reportsToGenerate, p4Changelist);
                }

                if (p4submit)
                {
                    SubmitChangelist(p4Changelist);
                }
            }
            catch (Exception ex)
            {
                // Log the exception.
                s_log.Exception(ex, "Unhandled exception during report generation.");
                exit_code = EXIT_CRITIC;

                // Display exception dialog only if we are allowed.
                if (!nopopups)
                {
                    RSG.Base.Windows.ExceptionStackTraceDlg stackDlg =
                        new RSG.Base.Windows.ExceptionStackTraceDlg(ex, AUTHOR, EMAIL);
                    stackDlg.Show();
                }
            }
            finally
            {
                gv.Dispose();
            }

            LogFactory.ApplicationShutdown();
            Environment.ExitCode = exit_code;
            return (exit_code);
        }
        #endregion // Entry-Point

        #region Private Methods
        /// <summary>
        /// Attempts to check the files out of perforce
        /// </summary>
        /// <param name="outputDir"></param>
        /// <param name="levelName"></param>
        /// <returns>Changelist number</returns>
        private static int CheckoutFiles(IDictionary<IReport, string> reportsToGenerate, string levelName, string buildId, string clDesc)
        {
            String currDir = Directory.GetCurrentDirectory();
            RSG.SourceControl.Perforce.P4 p4 = new RSG.SourceControl.Perforce.P4();
            int changelistNumber = 0;

            string changelistDescription = clDesc;
            if (changelistDescription == null)
            {
                if (!String.IsNullOrEmpty(buildId))
                {
                    changelistDescription = String.Format(P4_CHANGELIST_DESCRIPTION, levelName, buildId);
                }
                else
                {
                    changelistDescription = String.Format(P4_CHANGELIST_DESCRIPTION_NO_BUILD, levelName);
                }
            }

            try
            {
                p4.Connect();

                // Create a changelist for the files
                try
                {
                    P4API.P4PendingChangelist changelist = null;
                    if (!String.IsNullOrEmpty(buildId))
                    {
                        changelist = p4.CreatePendingChangelist(changelistDescription);
                    }
                    else
                    {
                        changelist = p4.CreatePendingChangelist(changelistDescription);
                    }
                    changelistNumber = changelist.Number;
                }
                catch (P4API.Exceptions.P4APIExceptions ex)
                {
                    s_log.Exception(ex, "Unhandled Perforce exception while trying to create new pending changelist.");
                }

                // Checkout all the files in the output directory
                List<string> directories = new List<string>();
                foreach (string outputPath in reportsToGenerate.Values)
                {
                    string outputDir = Path.GetDirectoryName(outputPath);
                    if (!directories.Contains(outputDir))
                    {
                        directories.Add(outputDir);
                    }
                }

                foreach (string outputDir in directories)
                {
                    try
                    {
                        p4.Run("revert", Path.Combine(outputDir, "*"));
                        p4.Run("sync", Path.Combine(outputDir, "*"));
                        p4.Run("edit", "-c", changelistNumber.ToString(), Path.Combine(outputDir, "*"));
                    }
                    catch (P4API.Exceptions.P4APIExceptions ex)
                    {
                        s_log.Exception(ex, "Unhandled Perforce exception while trying to edit the files in the {0} folder.", outputDir);
                    }
                }
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                s_log.Exception(ex, "Unhandled Perforce exception while trying to connect to the p4 server.");
            }
            finally
            {
                p4.Disconnect();
                Directory.SetCurrentDirectory(currDir);
            }

            return changelistNumber;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="outputDir"></param>
        /// <param name="changelistNumber"></param>
        private static void AddFilesToSourceControl(IDictionary<IReport, string> reportsToGenerate, int changelistNumber)
        {
            String currDir = Directory.GetCurrentDirectory();
            RSG.SourceControl.Perforce.P4 p4 = new RSG.SourceControl.Perforce.P4();
            try
            {
                p4.Connect();

                List<string> directories = new List<string>();
                foreach (string outputPath in reportsToGenerate.Values)
                {
                    string outputDir = Path.GetDirectoryName(outputPath);
                    if (!directories.Contains(outputDir))
                    {
                        directories.Add(outputDir);
                    }
                }

                foreach (string outputDir in directories)
                {
                    DirectoryInfo dirInfo = new DirectoryInfo(outputDir);
                    foreach (FileInfo file in dirInfo.EnumerateFiles())
                    {
                        try
                        {
                            p4.Run("add", "-c", changelistNumber.ToString(), file.FullName);
                        }
                        catch (P4API.Exceptions.P4APIExceptions ex)
                        {
                            s_log.Exception(ex, "Unhandled Perforce exception while trying to edit the files in the {0} folder.", outputDir);
                        }
                    }
                }
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                s_log.Exception(ex, "Unhandled Perforce exception while trying to connect to the p4 server.");
            }
            finally
            {
                p4.Disconnect();
                Directory.SetCurrentDirectory(currDir);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="changelistNumber"></param>
        private static void SubmitChangelist(int changelistNumber)
        {
            String currDir = Directory.GetCurrentDirectory();
            RSG.SourceControl.Perforce.P4 p4 = new RSG.SourceControl.Perforce.P4();
            try
            {
                p4.Connect();
                
                try
                {
                    // Submit the changelist reverting any unchanged files in the process
                    p4.Run("submit", "-c", changelistNumber.ToString(), "-f", "revertunchanged");
                }
                catch (P4API.Exceptions.P4APIExceptions ex)
                {
                    s_log.Exception(ex, "Unhandled Perforce exception while trying to submit the changelist {0}.", changelistNumber);
                    p4.Run("change", "-d", changelistNumber.ToString());
                }
            }
            catch (P4API.Exceptions.P4APIExceptions ex)
            {
                s_log.Exception(ex, "Unhandled Perforce exception while trying to connect to the p4 server.");
            }
            finally
            {
                p4.Disconnect();
                Directory.SetCurrentDirectory(currDir);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="levelName"></param>
        /// <param name="reports"></param>
        /// <param name="outputDirectory"></param>
        /// <param name="gv"></param>
        private static bool RunReports(String levelName, String outputDir, IDictionary<IReport, string> reportsToGenerate, string geometryStatsPathname, ConfigGameView gv, IConfig config, bool skipLevelLoad = false)
        {
            ILevel level = null;

            IPlatformFactory platformFactory = new PlatformFactory();
            IPlatformCollection platforms = platformFactory.CreateCollection(DataSource.SceneXml, config);
            IBugstarConfig bugstarConfig = ConfigFactory.CreateBugstarConfig(config);

            if (!skipLevelLoad)
            {
                level = GetLevel(levelName, gv, config, bugstarConfig, platforms, geometryStatsPathname);
                if (level == null)
                {
                    s_log.Error("Unable to generate reports for the {0} level as it was not found.", levelName);
                    return false;
                }
            }
            else
            {
                ILevelFactory levelFactory = new LevelFactory(config, bugstarConfig);
                ILevelCollection levelCollection = levelFactory.CreateCollection(DataSource.SceneXml);
                level = levelCollection.FirstOrDefault(item => item.Name == levelName);
            }

            // Now generate the actual reports
            s_log.Profile("Gathering reports");
            bool success = GenerateReports(reportsToGenerate, gv, config, level, platforms);
            s_log.ProfileEnd();

            return success;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="levelName"></param>
        /// <returns></returns>
        private static ILevel GetLevel(string levelName, ConfigGameView gv, IConfig config, IBugstarConfig bugstarConfig, IPlatformCollection platforms, string geometryStatsPathname)
        {
            // Get the level we are after
            ILevelFactory levelFactory = new LevelFactory(config, bugstarConfig);
            ILevelCollection levelCollection = levelFactory.CreateCollection(DataSource.SceneXml);
            levelCollection.LoadStats(new StreamableStat[] { StreamableLevelStat.ExportDataPath, StreamableLevelStat.ProcessedDataPath, StreamableLevelStat.ImageBounds, StreamableLevelStat.BackgroundImage });

            ILevel level = levelCollection.FirstOrDefault(item => item.Name == levelName);

            // Check that we found the requested level
            if (level != null)
            {
                // Characters
                ICharacterFactory characterFactory = new CharacterFactory();
                level.Characters = characterFactory.CreateCollection(DataSource.SceneXml);

                // Vehicles
                IVehicleFactory vehicleFactory = new VehicleFactory();
                level.Vehicles = vehicleFactory.CreateCollection(DataSource.SceneXml);

                // Map hierarchy
                BugstarConnection bugstarConnection = new BugstarConnection(bugstarConfig.RESTService, bugstarConfig.AttachmentService);
                IMapFactory mapFactory = new MapFactory(config, bugstarConfig, bugstarConnection);
                level.MapHierarchy = mapFactory.CreateHierarchy(DataSource.SceneXml, level);

                // Load the archetypes for all the prop/interior sections (do this in two steps)
                s_log.Profile("Pre-loading props.");
                IEnumerable<IMapSection> propSections = level.MapHierarchy.AllSections.Where(item => item.Type == RSG.Model.Common.Map.SectionType.Prop);
                propSections.AsParallel().ForAll(section =>
                    {
                        section.LoadStats(new StreamableStat[] { StreamableMapSectionStat.Archetypes });
                    });
                s_log.ProfileEnd();

                s_log.Profile("Pre-loading interiors.");
                IEnumerable<IMapSection> interiorSections = level.MapHierarchy.AllSections.Where(item => item.Type == RSG.Model.Common.Map.SectionType.Interior);
                interiorSections.AsParallel().ForAll(section =>
                    {
                        section.LoadStats(new StreamableStat[] { StreamableMapSectionStat.Archetypes });
                    });
                s_log.ProfileEnd();

                level.Initialised = true;

                // Weapons
                IWeaponFactory weaponFactory = new WeaponFactory();
                level.Weapons = weaponFactory.CreateCollection(DataSource.SceneXml);

                s_log.Profile("Geometry stats creation");
                IGeometryStatsFactory geometryStatsFactory = new GeometryStatsFactory();
                level.GeometryStats = geometryStatsFactory.CreateFromFile(geometryStatsPathname);
                s_log.ProfileEnd();
            }

            return level;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        /// <param name="reportNames"></param>
        /// <param name="outputDir"></param>
        /// <returns></returns>
        private static IDictionary<IReport, String> GatherReportsToGenerate(IConfig config, IEnumerable<String> reportNames, String outputDir)
        {
            // Create the config to use when determining which reports to create
            IReportsConfig reportsConfig = ConfigFactory.CreateReportConfig(config.Project);

            // Get the list of reports that we now how to generate.
            IDictionary<String, Type> reportTypeLookup = GetKnownReports().ToDictionary(item => item.Name);

            // Generate the list of reports that we wish to generate
            Dictionary<IReport, String> reportsToGenerate = new Dictionary<IReport, String>();

            foreach (String reportName in reportNames)
            {
                // Check that the report's friendly name exists in the config.
                if (!reportsConfig.ReportGeneratorConfig.Reports.ContainsKey(reportName))
                {
                    String message = String.Format("Report with the friendly name '{0}' was not found in the reports config.", reportName);
                    Debug.Assert(false, message);
                    s_log.Error(message);
                    continue;
                }

                IReportConfig reportConfig = reportsConfig.ReportGeneratorConfig.Reports[reportName];

                // check that the corresponding class of report is known about.
                if (!reportTypeLookup.ContainsKey(reportConfig.ClassName))
                {
                    String message = String.Format("Unable to create instance of the '{0}' report as the report wasn't found in the Report Assembly.", reportConfig.ClassName);
                    Debug.Assert(false, message);
                    s_log.Error(message);
                    continue;
                }

                // Use reflection to create an instance of the report
                Type reportType = reportTypeLookup[reportConfig.ClassName];
                ConstructorInfo cInfo = reportType.GetConstructor(new Type[] { });
                if (cInfo == null)
                {
                    String message = String.Format("Unable to find the default constructor for the '{0}' report.  No report will be generated for it.", reportConfig.ClassName);
                    Debug.Assert(false, message);
                    s_log.Error(message);
                    continue;
                }
                object reportInstance = cInfo.Invoke(new object[] { });

                // Check that we can generate a valid output file for the created report
                if (reportInstance is IReportFileProvider && !String.IsNullOrEmpty(reportConfig.OutputFilename))
                {
                    // Verify that the filename specified has the correct extension
                    string providedExtension = Path.GetExtension(reportConfig.OutputFilename);
                    string expectedExtension = (reportInstance as IReportFileProvider).SupportedExtension;

                    if (providedExtension != expectedExtension)
                    {
                        String message = String.Format("The output file for the '{0}' report doesn't have a valid extension.  Expected: {1}, Received: {2}", reportName, expectedExtension, providedExtension);
                        Debug.Assert(false, message);
                        s_log.Error(message);
                        continue;
                    }
                }
                else if (!(reportInstance is IReportStreamProvider || reportInstance is IReportMailProvider))
                {
                    s_log.Warning("The '{0}' report doesn't generate an output file so will be ignored.", reportName);
                    continue;
                }

                // Override any report properties based on those that are specified in the config
                foreach (KeyValuePair<string, object> param in reportConfig.Parameters)
                {
                    PropertyInfo pInfo = reportType.GetProperty(param.Key, param.Value.GetType());
                    if (pInfo != null)
                    {
                        pInfo.SetValue(reportInstance, param.Value, null);
                    }
                    else
                    {
                        s_log.Warning("Property with name '{0}' doesn't exist (or is of the wrong type) for report '{1}'.  It's value won't be set.", param.Key, reportConfig.ClassName);
                    }
                }

                // Everything is good, so add the report to the list of those to process
                reportsToGenerate[reportInstance as IReport] = (String.IsNullOrEmpty(reportConfig.OutputFilename) ? null : Path.Combine(outputDir, reportConfig.OutputFilename));
            }

            return reportsToGenerate;
        }

        /// <summary>
        /// Retrieve all the reports that we know about.
        /// </summary>
        /// <returns></returns>
        private static IEnumerable<Type> GetKnownReports()
        {
            Assembly ass = Assembly.GetAssembly(typeof(IReport));

            foreach (Type type in ass.GetTypes())
            {
                if (!type.IsAbstract && typeof(IReport).IsAssignableFrom(type))
                {
                    yield return type;
                }
            }
        }

        /// <summary>
        /// Generate the list of reports for the given level/platforms.
        /// </summary>
        /// <param name="reports"></param>
        /// <returns></returns>
        private static bool GenerateReports(IDictionary<IReport, String> reports, ConfigGameView gv, IConfig config, ILevel level, IPlatformCollection platforms)
        {
            bool reportFailed = false;

            foreach (KeyValuePair<IReport, String> pair in reports)
            {
                IReport report = pair.Key;
                string outputFile = pair.Value;

                s_log.Profile("{0} report generation.", report.Name);
                try
                {
                    GenerateReport(report, outputFile, gv, config, level, platforms);
                }
                catch (System.Exception ex)
                {
                    s_log.Exception(ex, "An unexpected exception occurred while generating the " + report.Name + " report.");
                    reportFailed = true;
                }
                s_log.ProfileEnd();
            }

            return !reportFailed;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="report"></param>
        /// <param name="outputFile"></param>
        private static void GenerateReport(IReport report, String outputFile, ConfigGameView gv, IConfig config, ILevel level, IPlatformCollection platforms)
        {
            String reportName = report.GetType().FullName;

            if (report is IReportFileProvider)
            {
                // Set the output file name
                (report as IReportFileProvider).Filename = outputFile;
                try
                {
                    if (!String.IsNullOrEmpty(outputFile))
                    {
                        if (!Directory.Exists(Path.GetDirectoryName(outputFile)))
                        {
                            Directory.CreateDirectory(Path.GetDirectoryName(outputFile));
                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Log__Exception(ex, "Exception while trying to create directory for - {0}", outputFile);
                }
            }

            // Check if we need to generate any dynamic content
            if (report is IDynamicLevelReport)
            {
                IDynamicLevelReport dynamicMapReport = report as IDynamicLevelReport;

                if (dynamicMapReport.GenerationTask != null)
                {
                    DynamicLevelReportContext context = new DynamicLevelReportContext(gv, config, level, platforms);
                    dynamicMapReport.GenerationTask.Reset();
                    dynamicMapReport.GenerationTask.Execute(context);
                }
            }
            else if (report is IDynamicReport)
            {
                IDynamicReport dynamicReport = report as IDynamicReport;

                if (dynamicReport.GenerationTask != null)
                {
                    DynamicReportContext context = new DynamicReportContext(gv, config);
                    dynamicReport.GenerationTask.Reset();
                    dynamicReport.GenerationTask.Execute(context);
                }
            }

            // Check if we need to convert a stream into a file
            if (report is IReportStreamProvider && !(report is IReportFileProvider))
            {
                if (!String.IsNullOrEmpty(outputFile))
                {
                    if (!Directory.Exists(Path.GetDirectoryName(outputFile)))
                    {
                        Directory.CreateDirectory(Path.GetDirectoryName(outputFile));
                    }

                    Stream streamToSave = (report as IReportStreamProvider).Stream;
                    streamToSave.Position = 0;

                    // Copy the stream to file
                    using (Stream fileStream = new FileStream(outputFile, FileMode.Create, FileAccess.Write))
                    {
                        streamToSave.CopyTo(fileStream);
                    }
                }
            }
            if (report is IReportMailProvider)
            {
                //Send message
                System.Net.Mail.MailMessage message = (report as IReportMailProvider).MailMessage;
                if (message != null)
                {
#if DEBUG
                    // Override the to list.
                    message.To.Clear();
                    message.To.Add("michael.taschler@rockstarnorth.com");
#endif
                    using (System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient(gv.LogMailServerHost, gv.LogMailServerPort))
                    {
                        smtpClient.Send(message);
                    }
                }
            }
        }
        #endregion // Private Methods
    }
} // ReportGenerator namespace
