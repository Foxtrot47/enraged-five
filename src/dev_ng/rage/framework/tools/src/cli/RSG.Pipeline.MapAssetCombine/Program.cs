﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using RSG.Base.Configuration;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Base.OS;
using RSG.Pipeline.Core;
using RSG.Pipeline.Services.Map.AssetCombine;
using RSG.SceneXml;
using RSG.SceneXml.MapExport.AssetCombine;

namespace RSG.Pipeline.MapAssetCombine
{

    /// <summary>
    /// Map Asset Combine application class.
    /// </summary>
    class Program
    {
        #region Constants
        // Processing Options
        private const String OPT_CONFIG = "config";
        private const String LOG_CTX = "Asset Combine";
        #endregion // Constants
        
        #region Entry-Point
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args"></param>
        [STAThread]
        static int Main(String[] args)
        {
            int exit_code = Constants.Exit_Success;

            // Define our executable options.  Stick with the long options
            // (e.g. --opt) rather than the single character versions.
            LongOption[] lopts = new LongOption[] {
                new LongOption(OPT_CONFIG, LongOption.ArgType.Required, "Configuration filename."),
            };
            CommandOptions options = new CommandOptions(args, lopts);
            LogFactory.Initialize();
            IUniversalLog log = LogFactory.CreateUniversalLog("Map Asset Combine");
            LogFactory.CreateUniversalLogFile(log);
            LogFactory.CreateApplicationConsoleLogTarget();

            if (options.ShowHelp || !options.ContainsOption(OPT_CONFIG))
            {
                log.MessageCtx(LOG_CTX, options.GetUsage());
                return (Constants.Exit_Failure);
            }

            // Top-level profile call.
            using (ProfileContext ctx = new ProfileContext(log, "RSG.Pipeline.MapAssetCombine Main"))
            {
                String configPathname = (String)options[OPT_CONFIG];
                AssetCombineConfig config = AssetCombineConfig.Load(configPathname);

                try
                {
                    foreach (AssetCombineConfigTask task in config.Tasks)
                    {
                        List<AssetCombineInput> processorInputs = new List<AssetCombineInput>();
                        IEnumerable<AssetCombineConfigInput> dlcInputs = task.Inputs.Where(i => i.IsDLC);
                        IEnumerable<AssetCombineConfigInput> coreInputs = task.Inputs.Where(i => !i.IsDLC).ToList();
                        List<AssetCombineConfigInput> coreInputsToIgnore = new List<AssetCombineConfigInput>();

                        // Loop through each DLC input first.
                        foreach (AssetCombineConfigInput configInput in dlcInputs)
                        {
                            // Determine if we have an equivalent core input.
                            AssetCombineConfigInput coreInput = coreInputs.FirstOrDefault(i => String.Equals(Path.GetFileName(i.ZipPathname), Path.GetFileName(configInput.ZipPathname), StringComparison.OrdinalIgnoreCase)); // Todo Flo : find what is "0" in StringComparison
                            if (null != coreInput)
                            {
                                String dlcSceneFilename = configInput.SceneXmlPathname;
                                SceneType dlcSceneType = SceneTypeUtils.FromContentTreeSceneType(configInput.SceneType);
                                Scene dlcScene = new Scene(dlcSceneType, dlcSceneFilename, LoadOptions.Objects | LoadOptions.Materials, true);

                                String coreSceneFilename = coreInput.SceneXmlPathname;
                                SceneType coreSceneType = SceneTypeUtils.FromContentTreeSceneType(coreInput.SceneType);
                                Scene coreScene = new Scene(coreSceneType, coreSceneFilename, LoadOptions.Objects | LoadOptions.Materials, true);

                                AssetCombineInput input = AssetCombineInput.Create(coreInput, coreScene, coreSceneType, configInput, dlcScene, dlcSceneType);
                                processorInputs.Add(input);
                                coreInputsToIgnore.Add(coreInput);
                            }
                            else
                            {
                                String sceneXmlFilename = configInput.SceneXmlPathname;
                                SceneType sceneType = SceneTypeUtils.FromContentTreeSceneType(configInput.SceneType);
                                Scene scene = new Scene(sceneType, sceneXmlFilename, LoadOptions.Objects | LoadOptions.Materials, true);
                                processorInputs.Add(AssetCombineInput.Create(configInput, scene));
                            }
                        }

                        // Loop through each Core input first.
                        foreach (AssetCombineConfigInput configInput in coreInputs.Except(coreInputsToIgnore))
                        {
                            String sceneXmlFilename = configInput.SceneXmlPathname;
                            SceneType sceneType = SceneTypeUtils.FromContentTreeSceneType(configInput.SceneType);
                            Scene scene = new Scene(sceneType, sceneXmlFilename, LoadOptions.Objects | LoadOptions.Materials, true);
                            processorInputs.Add(AssetCombineInput.Create(configInput, scene));
                        }
                        CheckOutputDirectory(task.Output.Filename);

                        MapAssetCollection[] outputs;
                        AssetCombineProcessor.Process(options.Branch, log, processorInputs.ToArray(), task.Output.CacheDirectory, task.Name, out outputs);
                        AssetFile af = new AssetFile(options.Branch, outputs);

                        af.Save(task.Output.Filename, options.Branch.Project.IsDLC);
                    }

                    if (LogFactory.HasError())
                        exit_code = Constants.Exit_Failure;
                }
                catch (Exception ex)
                {
                    log.ToolExceptionCtx(LOG_CTX, ex, "Unhandled exception during Asset Combine Processing.");
                    exit_code = Constants.Exit_Failure;
                }
            }

            Environment.ExitCode = exit_code;
            return (exit_code);
        }
        #endregion // Entry-Point

        #region Private Methods
        /// <summary>
        /// Ensure filepath directory exists.
        /// </summary>
        /// <param name="filepath"></param>
        static private void CheckOutputDirectory(String filepath)
        {
            if (String.IsNullOrEmpty(filepath))
                return;

            String directory = Path.GetDirectoryName(filepath);
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);
        }
        #endregion // Private Methods
    }

} // RSG.Pipeline.MapAssetCombine namespace
