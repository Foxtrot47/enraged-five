﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using RSG.Base.OS;
using RSG.Base.Logging;
using RSG.Base.Logging.Universal;
using RSG.Pipeline.Core;
using RSG.Base.Configuration;
using RSG.SceneXml;
using System.Text.RegularExpressions;

namespace RSG.Pipeline.Animation.Cutscene.InjectPrefix
{
    class Program
    {
        #region Constants
        private static readonly String OPTION_INPUTANIMDIR = "inputanimdir";
        private static readonly String OPTION_INPUTMETADIR = "inputmetadir";
        private static readonly String OPTION_OUTPUTDIR = "outputdir";
        private static readonly String OPTION_TASK_NAME = "taskname";
        private static readonly String LOG_CTX = "Inject Prefix";
        #endregion // Constants

        static int RunProcess(string strExe, string strArgs, IUniversalLog log)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.Arguments = strArgs;
            startInfo.FileName = strExe;
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.CreateNoWindow = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.UseShellExecute = false;

            log.MessageCtx(LOG_CTX, strExe + " " + strArgs);

            Process proc = Process.Start(startInfo);
            String output = proc.StandardOutput.ReadToEnd();
            proc.WaitForExit();

            log.MessageCtx(LOG_CTX, output);

            return proc.ExitCode;
        }

        static bool PrefixProp(SceneCollection sceneCollection, String itypeFile, IUniversalLog log)
        {
            IEnumerable<Scene> scenes = sceneCollection.GetScenesOfMapType(SceneType.EnvironmentProps);
            foreach (Scene s in scenes)
            {
                TargetObjectDef target = s.FindObjectSlow(itypeFile);
                if (target != null)
                {
                    if(target.ObjectDef.GetExportRoot() != null)
                    {
                        if (!target.ObjectDef.GetExportRoot().GetAttribute("New DLC", false))
                            log.WarningCtx(LOG_CTX, "Prop '{0}' resolved to root '{1}' but is not tagged as 'New DLC'", itypeFile, target.ObjectDef.GetExportRoot().Name);

                        return target.ObjectDef.GetExportRoot().GetAttribute(AttrNames.OBJ_NEW_DLC, AttrDefaults.OBJ_NEW_DLC);
                    }
                    else
                    {
                        if (!target.IsNewDLCObject())
                            log.WarningCtx(LOG_CTX, "Prop '{0}' is not tagged as 'New DLC'", itypeFile, target.Name);

                        return target.IsNewDLCObject();
                    }
                }
            }

            return false;
        }

        static string RemoveTags(String value, String tagName)
        {
            return value.Replace(String.Format("<{0}>", tagName), "").Replace(String.Format("</{0}>", tagName), "");
        }

        static string GetPropName(String value, String prefix)
        {
            string[] split = value.Split(':');
            if (split.Length > 1)
            {
                if (split[split.Length - 1].StartsWith(prefix))
                    return split[split.Length - 1].Remove(0, prefix.Length);

                return split[split.Length - 1];
            }

            return value;
        }

        [STAThread]
        static int Main(string[] args)
        {
            LogFactory.Initialize();
            LogFactory.CreateApplicationConsoleLogTarget();
            IUniversalLog log = LogFactory.CreateUniversalLog("InjectPrefix");
            try
            {
                // Define our executable options.  Stick with the long options
                // (e.g. --opt) rather than the single character versions.
                LongOption[] lopts = new LongOption[] {
                new LongOption(OPTION_INPUTANIMDIR, LongOption.ArgType.Required,
                    "Input directory."),
                new LongOption(OPTION_INPUTMETADIR, LongOption.ArgType.Required,
                    "Input directory."),
                new LongOption(OPTION_OUTPUTDIR, LongOption.ArgType.Required,
                    "Output Directory."),
                new LongOption(OPTION_TASK_NAME, LongOption.ArgType.Required,
                    "Asset Pipeline 3 Task Name.")
                };
                CommandOptions options = new CommandOptions(args, lopts);

                // Turn args into vars
                String inputAnimDir = options[OPTION_INPUTANIMDIR] as String;
                String inputMetaDir = options[OPTION_INPUTMETADIR] as String;
                String outputDir = options[OPTION_OUTPUTDIR] as String;
                String taskName = options[OPTION_TASK_NAME] == null ? String.Empty : options[OPTION_TASK_NAME] as String;

                if (String.Empty != taskName)
                {
                    Console.WriteLine("TASK: {0}", taskName);
                    log.MessageCtx(LOG_CTX, "TASK: {0}", taskName);
                }

                if (!Directory.Exists(inputAnimDir))
                {
                    log.ErrorCtx(LOG_CTX, "Directory '{0}' does not exist", inputAnimDir);
                    return RSG.Pipeline.Core.Constants.Exit_Failure;
                }

                if (!Directory.Exists(inputMetaDir))
                {
                    log.ErrorCtx(LOG_CTX, "Directory '{0}' does not exist", inputMetaDir);
                    return RSG.Pipeline.Core.Constants.Exit_Failure;
                }

                String dlcprefix = options.Branch.Project.AssetPrefix.MapPrefix;

                // only process if this is a dlc scene and we have a prefix
                if (options.Branch.Project.IsDLC && !String.IsNullOrEmpty(dlcprefix))
                {
                    List<String> propEntries = new List<String>();

                    String cutFileContents = File.ReadAllText(Path.Combine(inputMetaDir, "data_stream.cutxml"));
                    Match mCutObjects = Regex.Match(cutFileContents, "<pCutsceneObjects>[\\s\\S]*?<\\/pCutsceneObjects>");
                    if (mCutObjects.Success)
                    {
                        // store all the prop object entries so we can pull data from them
                        foreach (Match mPropObjects in Regex.Matches(mCutObjects.Value, "<Item type=\"rage__cutfPropModelObject\">[\\s\\S]*?<\\/Item>"))
                        {
                            propEntries.Add(mPropObjects.Value);
                        }

                        if (propEntries.Count > 0)
                        {
                            IContentTree tree = RSG.Pipeline.Content.Factory.CreateTree(options.Branch);
                            SceneCollection sceneCollection = new SceneCollection(options.Branch,
                                tree, SceneCollectionLoadOption.Props);
                            sceneCollection.LoadMapSceneFilesForBranch(SceneType.EnvironmentProps, options.Branch);

                            Dictionary<String, String> dictPropObjects = new Dictionary<String, String>();

                            // Iterate over the props and get the itype entry and then lookup its prop
                            foreach (String prop in propEntries)
                            {
                                Match mPropName = Regex.Match(prop, "<cName>([\\s\\S]*?)<\\/cName>");
                                if (mPropName.Success)
                                {
                                    // The prop name might have the dlc prefix already applied, remove this for our lookup into the scenexml
                                    string propName = GetPropName(RemoveTags(mPropName.Value, "cName"), dlcprefix);
                                    Match mPropTypeFile = Regex.Match(prop, "<typeFile>([\\s\\S]*?)<\\/typeFile>");
                                    if (mPropTypeFile.Success)
                                    {
                                        String typeFile = RemoveTags(mPropTypeFile.Value, "typeFile");
                                        // look into the scenexml data and check if the prop is of the new dlc type
                                        if (PrefixProp(sceneCollection, propName, log))
                                        {
                                            String propModified = prop.Replace(mPropTypeFile.Value, String.Format("<typeFile>{0}{1}</typeFile>", dlcprefix, typeFile));
                                            dictPropObjects.Add(prop, propModified);

                                            log.MessageCtx(LOG_CTX, "Renamed itype file '{0}' to '{1}' for prop '{2}'.", typeFile, String.Format("{0}{1}", dlcprefix, typeFile), propName);
                                        }
                                        else
                                        {
                                            log.MessageCtx(LOG_CTX, "Assuming disc prop itype file '{0}' for prop '{1}'.", typeFile, propName);
                                        }
                                    }
                                }
                            }

                            foreach (KeyValuePair<String, String> pair in dictPropObjects)
                            {
                                cutFileContents = cutFileContents.Replace(pair.Key, pair.Value);
                            }
                        }
                        else
                        {
                            log.MessageCtx(LOG_CTX, "No prop objects found.");
                        }
                    }

                    File.WriteAllText(Path.Combine(outputDir, "data_stream.cutxml"), cutFileContents);
                }
                else
                {
                    log.MessageCtx(LOG_CTX, "Copying '{0}' to '{1}'", Path.Combine(inputMetaDir, "data_stream.cutxml"), Path.Combine(outputDir, "data_stream.cutxml"));
                    File.Copy(Path.Combine(inputMetaDir, "data_stream.cutxml"), Path.Combine(outputDir, "data_stream.cutxml"));
                }

                // Until we implement prop renaming just copy the anim data straight to output
                foreach (var file in Directory.GetFiles(inputAnimDir))
                {
                    File.Copy(file, Path.Combine(outputDir, Path.GetFileName(file)));
                }

                return RSG.Pipeline.Core.Constants.Exit_Success;
            }
            catch (Exception e)
            {
                log.ToolExceptionCtx(LOG_CTX, e, "Unhandled Exception during Inject Prefix Process");
                return RSG.Pipeline.Core.Constants.Exit_Failure;
            }
            finally
            {
                LogFactory.ApplicationShutdown();
            }
        }
    }
}
