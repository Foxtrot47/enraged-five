﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Web;
using RSG.Base.Logging;
using RSG.Statistics.Common.Config;
using RSG.Statistics.VerticaServices.Service;

namespace RSG.Statistics.VerticaIISHost
{
    /// <summary>
    /// Custom service host factory that injects the vertica server config object into the services that are created by IIS.
    /// </summary>
    public class VerticaServiceHostFactory : ServiceHostFactory
    {
        /// <summary>
        /// Server configuration object.
        /// </summary>
        private static readonly IVerticaServer s_server;

        /// <summary>
        /// Static constructor.
        /// </summary>
        static VerticaServiceHostFactory()
        {
            // Initialise the server we are using.
            LogFactory.Initialize();
            IStatisticsConfig config = new StatisticsConfig();
            String serverName = ConfigurationManager.AppSettings["server"];
            if (serverName != null)
            {
                s_server = config.VerticaServers[serverName];
            }
            else
            {
                throw new ArgumentNullException("Configuration is missing the server element.");
            }

            Debug.Assert(s_server.UsingIIS, "Trying to host a server in IIS that isn't flagged as using IIS.");
            if (!s_server.UsingIIS)
            {
                throw new ArgumentException("Trying to host a server in IIS that isn't flagged as using IIS.");
            }
        }

        /// <summary>
        /// Override the creation of a service host to create our local server host that injects the server config.
        /// </summary>
        protected override ServiceHost CreateServiceHost(Type t, Uri[] baseAddresses)
        {
            ServiceBehaviorAttribute att =
                t.GetCustomAttributes(typeof(ServiceBehaviorAttribute), true).FirstOrDefault() as ServiceBehaviorAttribute;
            Debug.Assert(att != null, "Trying to instantiate a service instance that doesn't have a ServiceBehaviorAttribute associated with it.");
            if (att == null)
            {
                throw new ArgumentNullException("Trying to instantiate a service instance that doesn't have a ServiceBehaviorAttribute associated with it.");
            }

            // Construct the config service host based on the instance context mode of the service.
            if (att.InstanceContextMode == InstanceContextMode.Single)
            {
                object serviceInstance = Activator.CreateInstance(t, s_server);
                return new VerticaServiceHost(serviceInstance, baseAddresses);
            }
            else
            {
                return new VerticaServiceHost(s_server, t, baseAddresses);
            }
        }
    } // VerticaServiceHostFactory
}