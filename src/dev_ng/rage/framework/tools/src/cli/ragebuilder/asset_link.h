#ifndef __RAGEBUILDER_ASSET_LINK_H__
#define __RAGEBUILDER_ASSET_LINK_H__

// RAGE headers
#include "atl/string.h"
#include "file/limits.h"
#include "parser/macros.h"

// Local headers
#include "asset_link.h"

namespace rage
{

class CAssetLink
{
public:
	CAssetLink( ) { }
	virtual ~CAssetLink( ) { }

	void ResolvePath( );

	static const char* GetExtension() { return "link"; }

	::rage::atString m_filename;

	PAR_SIMPLE_PARSABLE;
};

} // rage namespace

#endif // __RAGEBUILDER_ASSET_LINK_H__
