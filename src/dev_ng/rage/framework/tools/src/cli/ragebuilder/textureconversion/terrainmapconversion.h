// ========================================================
// terrainmapconversion.h
// Copyright (c) 2011 Rockstar Games.  All Rights Reserved. 
// ========================================================

#ifndef TEXTURECONVERSION_TERRAINMAPCONVERSION_H
#define TEXTURECONVERSION_TERRAINMAPCONVERSION_H

#include <map>

#include "configParser/configParser.h"
#include "configParser/configGameView.h"
#include "vector/color32.h"

class TextureConverter 
{

public:
	virtual bool Start() { return false; }
	virtual void Convert( const Vector4& src, Vector4& des ) = 0;
	virtual void End() {}

	// settings for converter
	//virtual int MipFilter() { return D3DX_FILTER_BOX; }
};

class TerrainMapTexture : public TextureConverter 
{
protected:
	std::map<int, int>							m_ColorRemapTable;
	typedef std::map<int, int>::iterator		LutIterator;

public:
	virtual ~TerrainMapTexture() {};
	virtual bool Start();
	virtual void Convert(const Vector4& src, Vector4& des );
	virtual void End() {};

	//bool RequiresMipFilterOverride() { return true; } 
	//TCConvertOptions::TCMipFilter GetMipFilterOverride() { return TCConvertOptions::MIPFILTER_POINT; }
};


class TerrainMapTexture2 : public TerrainMapTexture
{
public:
	enum CHANNEL_MODE
	{
		kMode012,
		kMode345
	};

	TerrainMapTexture2(CHANNEL_MODE mode)
		: m_mode( mode )
	{}

	TerrainMapTexture2() { }
	void SetChannelMode(CHANNEL_MODE mode) { m_mode = mode; }
	virtual bool Start( );
	virtual void Convert(const Vector4& src, Vector4& des );

private:
	CHANNEL_MODE	m_mode;
	Color32			m_weights[8];
};

#endif //TEXTURECONVERSION_TERRAINMAPCONVERSION_H