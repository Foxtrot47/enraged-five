// ========================================================
// terrainmapconversion.cpp
// Copyright (c) 2011 Rockstar Games.  All Rights Reserved. 
// ========================================================

#include <fstream>

#include "terrainmapconversion.h"

enum 
{
	TEXTURE_CHANNEL_RED = 0,
	TEXTURE_CHANNEL_GREEN,
	TEXTURE_CHANNEL_BLUE,
	TEXTURE_CHANNEL_ALPHA
};

bool TerrainMapTexture::Start() 
{
	static configParser::ConfigGameView gv;

	TCHAR lookupPath[_MAX_PATH] = {0};
	gv.GetMetadataDir( lookupPath, _MAX_PATH );
	strcat_s(lookupPath, _MAX_PATH, "\\terrain\\channellookup.txt");

	std::ifstream in( lookupPath );

	if(!in.is_open())
	{
		Errorf("Error: Failed to open channel lookup file '%s'.", lookupPath);
		return false;
	}

	while( in )
	{
		int vR;
		int vG;
		int vB;
		int index;

		in >> vR;
		in >> vG;
		in >> vB;
		in >> index;

		int val = vR << 16 | vG << 8 | vB;
		m_ColorRemapTable[ val ] = index;
	}

	in.close();

	return true;
}

void TerrainMapTexture::Convert(const Vector4& src, Vector4& des ) 
{
	static int sMissingClrCount = 0;

	int vR = (int)( src[TEXTURE_CHANNEL_RED] * 255.0f);
	int vG = (int)( src[TEXTURE_CHANNEL_GREEN] * 255.0f );
	int vB = (int)( src[TEXTURE_CHANNEL_BLUE ] * 255.0f);

	int val = vR << 16 | vG << 8 | vB;

	LutIterator it = m_ColorRemapTable.find( val );

	des[TEXTURE_CHANNEL_RED] = 0.0f;
	des[TEXTURE_CHANNEL_BLUE] = 0.0f;
	des[TEXTURE_CHANNEL_GREEN] = 0.0f;

	if ( it != m_ColorRemapTable.end() )
	{
		int type = it->second ;

		if ( type < 0 )
		{
			Warningf(" Color  R %i  G %i  B %i  has been deprecated and is not in the texture atlas any more\n", vR, vG, vB );
			return;
		}
		float v = (float)type / 255.0f;

		des[ TEXTURE_CHANNEL_RED ] = v;
		des[ TEXTURE_CHANNEL_GREEN ] = v;
		des[ TEXTURE_CHANNEL_BLUE ] = v;
		des[ TEXTURE_CHANNEL_ALPHA ] = v;
	}
	else
	{
		if( sMissingClrCount < 64)
		{
			Warningf(" Color  R %i  G %i  B %i  not found in lookup table\n", vR, vG, vB );
			sMissingClrCount++;
		}
		else if( sMissingClrCount == 64 )
		{
			Warningf(" More than 64 pixel values could not be found in the color table, so I won't annoy you with more warnings...");
			sMissingClrCount++;
		}
	}
}

bool TerrainMapTexture2::Start()
{
	if(!TerrainMapTexture::Start())
		return false;

	static configParser::ConfigGameView gv;

	TCHAR weightsPath[_MAX_PATH] = {0};
	gv.GetMetadataDir( weightsPath, _MAX_PATH );
	strcat_s(weightsPath, "\\terrain");

	if(m_mode == kMode012)
	{
		strcat_s(weightsPath, _MAX_PATH, "\\channelweights012.txt");
	}
	else
	{
		strcat_s(weightsPath, _MAX_PATH, "\\channelweights345.txt");
	}

	std::ifstream in( weightsPath );

	if(!in.is_open())
	{
		Errorf("Error: Failed to open channel weight file '%s'.", weightsPath);
		return false;
	}

	for(int i=0; i<8; i++)
	{
		int index;
		float r,g,b;

		in >> index;
		in >> r;
		in >> g;
		in >> b;

		m_weights[i] = Color32(r,g,b);
	}

	in.close();

	return true;
}

void TerrainMapTexture2::Convert(const Vector4& src, Vector4& des) 
{
	static int sMissingClrCount = 0;

	int vR = (int)( src[TEXTURE_CHANNEL_RED] * 255.0f);
	int vG = (int)( src[TEXTURE_CHANNEL_GREEN] * 255.0f );
	int vB = (int)( src[TEXTURE_CHANNEL_BLUE ] * 255.0f);

	int val = vR << 16 | vG << 8 | vB;

	LutIterator it = m_ColorRemapTable.find( val );
	if ( it != m_ColorRemapTable.end() )
	{
		int oldindex = it->second ;
		int newindex = (oldindex < 8) ? oldindex : oldindex / 8;

		Color32 colorWeight = m_weights[newindex];

		des[ TEXTURE_CHANNEL_RED ] = colorWeight.GetRedf();
		des[ TEXTURE_CHANNEL_GREEN ] = colorWeight.GetGreenf();
		des[ TEXTURE_CHANNEL_BLUE ] = colorWeight.GetBluef();
		des[ TEXTURE_CHANNEL_ALPHA ] = src[TEXTURE_CHANNEL_ALPHA];
	}
	else
	{
		if( sMissingClrCount < 64)
		{
			Warningf("Color  R %i  G %i  B %i  not found in lookup table.", vR, vG, vB );
			sMissingClrCount++;
		}
		else if( sMissingClrCount == 64 )
		{
			Warningf("More than 64 pixel values could not be found in the color table.  Suppressing warnings...");
			sMissingClrCount++;
		}
	}
}