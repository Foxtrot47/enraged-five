//
// luarage.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "luarage.h"
#include "scriptrage.h"

//=============================================================================
// Commands
//=============================================================================

//
// name:		InitRage
// lua command:	init_rage
// description:	Initialises Rage
int InitRage(lua_State *L)
{
	GetArguments(L, 0, "init_rage");

	if( !RBInitRage() )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		ShutdownRage
// lua command:	shutdown_rage
// description:	Shutdown Rage
int ShutdownRage(lua_State *L)
{
	GetArguments(L, 0, "shutdown_rage");
	RBShutdownRage();
	LUA_RETURN_SUCCESS();
}

//
// name:		SetRootDirectory
// lua command:	set_root_dir
// description:	Set the root directory that all file access works from
int SetRootDirectory(lua_State *L)
{
	GetArguments(L, 1, "set_root_dir");
	CheckArgument(L, 1, lua_isstring, "set_root_dir");

	if(!RBSetRootDirectory(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int SetAutoTexDict(lua_State* L)
{
	GetArguments(L, 1, "set_auto_texdict");
	RBSetAutoTexDict(lua_toboolean(L, 1) != 0);
	LUA_RETURN_SUCCESS();
}

int RegisterTextureName(lua_State* L)
{
	GetArguments(L, 1, "register_texturename");
	RBRegisterTextureName(lua_tostring(L, 1));
	LUA_RETURN_SUCCESS();
}

int LoadProceduralDefs(lua_State* L)
{
	GetArguments(L, 1, "load_proceduraldefs");
	CheckArgument(L, 1, lua_isstring, "load_materialdefs");

	if(!RBLoadProceduralDefs(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int LoadMaterialDefs(lua_State* L)
{
	GetArguments(L, 1, "load_materialdefs");
	CheckArgument(L, 1, lua_isstring, "load_materialdefs");

	if(!RBLoadMaterialDefs(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

// Set the effect path.
int SetEffectPath(lua_State* L)
{
	GetArguments(L, 1, "set_effectpath");
	CheckArgument(L, 1, lua_isstring, "set_effectpath");
	RBSetEffectPath(lua_tostring(L,1));
	LUA_RETURN_SUCCESS();
}

// Set the shader path.
int SetShaderPath(lua_State* L)
{
	GetArguments(L, 1, "set_shaderpath");
	CheckArgument(L, 1, lua_isstring, "set_shaderpath");
	RBSetShaderPath(lua_tostring(L,1));
	LUA_RETURN_SUCCESS();
}

// Set the shader path.
int SetShaderDbPath(lua_State* L)
{
	GetArguments(L, 1, "set_shaderdbpath");
	CheckArgument(L, 1, lua_isstring, "set_shaderdbpath");
	RBSetShaderDbPath(lua_tostring(L,1));
	LUA_RETURN_SUCCESS();
}

// Set the build path.
int SetBuildPath(lua_State* L)
{
	GetArguments(L, 1, "set_buildpath");
	CheckArgument(L, 1, lua_isstring, "set_buildpath");
	RBSetBuildPath(lua_tostring(L,1));
	LUA_RETURN_SUCCESS();
}

// Set the assets path.
int SetAssetsPath(lua_State* L)
{
	GetArguments(L, 1, "set_assetspath");
	CheckArgument(L, 1, lua_isstring, "set_assetspath");
	RBSetAssetsPath(lua_tostring(L,1));
	LUA_RETURN_SUCCESS();
}

// Set the core assets path.
int SetCoreAssetsPath(lua_State* L)
{
	GetArguments(L, 1, "set_coreassetspath");
	CheckArgument(L, 1, lua_isstring, "set_coreassetspath");
	RBSetCoreAssetsPath(lua_tostring(L,1));
	LUA_RETURN_SUCCESS();
}

// Set the metadata definitions path.
int SetMetadataDefinitionsPath(lua_State* L)
{
	GetArguments(L, 1, "set_metadatadefinitionspath");
	CheckArgument(L, 1, lua_isstring, "set_metadatadefinitionspath");
	RBSetMetadataDefinitionsPath(lua_tostring(L,1));
	LUA_RETURN_SUCCESS();
}

int GetPlatform(lua_State* L)
{
	lua_pushstring(L, RBGetPlatform());
	return 1;
}

int SetPlatform(lua_State* L)
{
	GetArguments(L, 1, "set_platform");
	CheckArgument(L, 1, lua_isstring, "set_platform");

	if(!RBSetPlatform(lua_tostring(L, 1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

// return an extension for a file that is platform specific
// unknown extensions are allowed through.
int GetPlatformExtension(lua_State* L)
{
	GetArguments(L, 1, "get_platform_extension");
	CheckArgument(L, 1, lua_isstring, "get_platform_extension");
	lua_pushstring(L, RBGetPlatformExtension(lua_tostring(L, 1)));
	return 1;
}

// convert a filename to an extension given a platform
int ConvertFilenameToPlatform(lua_State* L)
{
	GetArguments(L, 2, "convert_filename_to_platform");
	CheckArgument(L, 1, lua_isstring, "convert_filename_to_platform");
	CheckArgument(L, 2, lua_isstring, "convert_filename_to_platform");

	const char* filename = lua_tostring(L, 1);
	const char* platform = lua_tostring(L, 2);

	const char* ret = RBConvertFilenameToPlatform(filename,platform);

	if(ret == NULL)
		LUA_RETURN_FAILURE();

	lua_pushstring(L, ret);
	return 1;
}



//
// name:		StartBuild
// lua command:	start_build
// description:	Start resource heap build
int StartBuild(lua_State* L)
{
	GetArguments(L, 0, "start_build");

	if( !RBStartBuild() )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		SetTuningFolder
// lua command:	set_tuning_folder
// description:	Start resource heap build
// GunnarD: Retiring in favour of Configmanager setting
// int SetTuningFolder(lua_State* L)
// {
// 	GetArguments(L, 1, "set_tuning_folder");
// 	RBSetTuningFolder(lua_tostring(L,1));
// 	LUA_RETURN_SUCCESS();
// }


//
// name:		LoadTextureTemplates
// lua command:	load_textures
// description:	Load DDS textures from a filter
int LoadTextureTemplates(lua_State* L)
{
	GetArguments(L, 1, "load_textures_templates");
	CheckArgument(L, 1, lua_isstring, "load_textures_templates");

	if(!RBLoadTextureTemplates(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		LoadTexture
// lua command:	load_texture
// description:	Load a DDS texture
int LoadTexture(lua_State* L)
{
	GetArguments(L, 1, "load_texture");
	CheckArgument(L, 1, lua_isstring, "load_texture");

	if(!RBLoadTexture(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		LoadTextures
// lua command:	load_textures
// description:	Load DDS textures from a filter
int LoadTextures(lua_State* L)
{
	GetArguments(L, 1, "load_textures");
	CheckArgument(L, 1, lua_isstring, "load_textures");

	int count = 0;
	if ( !RBLoadTextures( lua_tostring(L,1), count ) )
		LUA_RETURN( -1 );
	LUA_RETURN( count );
}

//
// name:		LoadAnim
// lua command:	load_anim
// description:	Load an animation
int LoadAnim(lua_State* L)
{
	GetArguments(L, 1, "load_anim");
	CheckArgument(L, 1, lua_isstring, "load_anim" );

	if(!RBLoadAnim(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		LoadAnims
// lua command:	load_anims
// description:	Load animations from a filter
int LoadAnims(lua_State* L)
{
	GetArguments(L, 1, "load_anims");
	CheckArgument(L, 1, lua_isstring, "load_anims" );

	if(!RBLoadAnims(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		LoadAnim
// lua command:	load_anim
// description:	Load an animation
int LoadClip(lua_State* L)
{
	GetArguments(L, 1, "load_clip");
	CheckArgument(L, 1, lua_isstring, "load_clip" );

	if(!RBLoadClip(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		LoadAnims
// lua command:	load_anims
// description:	Load animations from a filter
int LoadClips(lua_State* L)
{
	GetArguments(L, 1, "load_clips");
	CheckArgument(L, 1, lua_isstring, "load_clips" );

	if(!RBLoadClips(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		LoadParamMotion
// lua command:	load_param_motion
// description:	Load param motion from a filter
int LoadParamMotion(lua_State* L)
{
	GetArguments(L, 1, "load_param_motion");
	CheckArgument(L, 1, lua_isstring, "load_param_motion" );

	if(!RBLoadParamMotion(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		LoadParamMotions
// lua command:	load_param_motions
// description:	Load param motions from a filter
int LoadParamMotions(lua_State* L)
{
	GetArguments(L, 1, "load_param_motions");
	CheckArgument(L, 1, lua_isstring, "load_param_motions" );

	if(!RBLoadParamMotions(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		LoadExpressions
// lua command:	load_expressions
// description:	Load expressions from a filter
int LoadExpressions(lua_State* L)
{
	GetArguments(L, 1, "load_expressions");
	CheckArgument(L, 1, lua_isstring, "load_expressions" );

	if(!RBLoadExpressions(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		LoadExpressionsList
// lua command:	load_expressions_list
// description:	Load expressions list from a filter
int LoadExpressionsList(lua_State* L)
{
	GetArguments(L, 1, "load_expressions_list");
	CheckArgument(L, 1, lua_isstring, "load_expressions_list" );

	if(!RBLoadExpressionsList(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}
//
// name:		LoadModel
// lua command:	load_model
// description:	Load a drawable
int LoadModel(lua_State* L)
{
	GetArguments(L, 1, "load_model");
	CheckArgument(L, 1, lua_isstring, "load_model" );

	if(!RBLoadModel(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		LoadBlendShape
// lua command:	load_blendshape
// description:	Load a blend shape
int LoadBlendShape(lua_State* L)
{
	GetArguments(L, 1, "load_blendshape");
	CheckArgument(L, 1, lua_isstring, "load_blendshape" );

	if(!RBLoadBlendShape(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		LoadFragment
// lua command:	load_fragment
// description:	Load a fragment
int LoadFragment(lua_State* L)
{
	GetArguments(L, 1, "load_fragment");
	CheckArgument(L, 1, lua_isstring, "load_fragment" );

	if(!RBLoadFragment(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		LoadNMData
// lua command:	load_nm_data
// description:	Load nm data onto current fragment
int LoadNMData(lua_State* L)
{
	GetArguments(L, 1, "load_nm_data");
	CheckArgument(L, 1, lua_isstring, "load_nm_data" );

	if(!RBLoadNMData(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		LoadModels
// lua command:	load_models
// description:	Load drawable from a filter
int LoadModels(lua_State* L)
{
	GetArguments(L, 1, "load_models");
	CheckArgument(L, 1, lua_isstring, "load_models" );

	if(!RBLoadModels(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		LoadBound
// lua command:	load_bound
// description:	Load a drawable
int LoadBound(lua_State* L)
{
	GetArguments(L, 1, "load_bound");
	CheckArgument(L, 1, lua_isstring, "load_bound" );

	if(!RBLoadBound(lua_tostring(L, 1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		LoadFrameFilter
// lua command:	load_framefilter
// description:	Load frame filter from a frame filter dictionary
int LoadFrameFilter(lua_State* L)
{
	GetArguments(L, 1, "load_framefilter");
	CheckArgument(L, 1, lua_isstring, "load_framefilter" );

	if(!RBLoadFrameFilter(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		LoadBounds
// lua command:	load_bounds
// description:	Load drawable from a filter
int LoadBounds(lua_State* L)
{
	GetArguments(L, 1, "load_bounds");
	CheckArgument(L, 1, lua_isstring, "load_bounds" );

	if(!RBLoadBounds(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int LoadPathRegion(lua_State* L)
{
	GetArguments(L, 1, "load_pathregion");
	CheckArgument(L, 1, lua_isstring, "load_pathregion" );

	if(!RBLoadPathRegion(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int LoadNavMesh(lua_State* L)
{
	GetArguments(L, 1, "load_navmesh");
	CheckArgument(L, 1, lua_isstring, "load_navmesh" );

	if(!RBLoadNavMesh(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int LoadHeightMesh(lua_State* L)
{
	GetArguments(L, 1, "load_heightmesh");
	CheckArgument(L, 1, lua_isstring, "load_heightmesh" );

	if(!RBLoadHeightMesh(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int LoadAudMesh(lua_State* L)
{
	GetArguments(L, 1, "load_audmesh");
	CheckArgument(L, 1, lua_isstring, "load_audmesh");

	if (!RBLoadAudMesh(lua_tostring(L, 1)))
	{
		LUA_RETURN_FAILURE();
	}

	LUA_RETURN_SUCCESS();
}

int LoadMeta(lua_State* L)
{
	GetArguments(L, 1, "load_meta");
	CheckArgument(L, 1, lua_isstring, "load_meta");

	if (!RBLoadMeta(lua_tostring(L, 1)))
	{
		LUA_RETURN_FAILURE();
	}

	LUA_RETURN_SUCCESS();
}

int LoadScript(lua_State* L)
{
	GetArguments(L, 1, "load_script");
	CheckArgument(L, 1, lua_isstring, "load_script");

	if (!RBLoadScript(lua_tostring(L, 1)))
	{
		LUA_RETURN_FAILURE();
	}

	LUA_RETURN_SUCCESS();
}

int LoadOccMeshes(lua_State* L)
{
	GetArguments(L, 1, "load_occ_meshes");
	CheckArgument(L, 1, lua_isstring, "load_occ_meshes");

	if (!RBLoadOccMeshes(lua_tostring(L, 1)))
	{
		LUA_RETURN_FAILURE();
	}

	LUA_RETURN_SUCCESS();
}

int LoadFrameFilterList(lua_State* L)
{
	GetArguments(L, 1, "load_framefilter_list");
	CheckArgument(L, 1, lua_isstring, "load_framefilter_list" );

	if(!RBLoadFrameFilterList(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int LoadHierarchicalNav(lua_State* L)
{
	GetArguments(L, 1, "load_hierarchicalnav");
	CheckArgument(L, 1, lua_isstring, "load_hierarchicalnav" );

	if(!RBLoadHierarchicalNav(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int LoadWaypointRecording(lua_State* L)
{
	GetArguments(L, 1, "load_waypointrecording");
	CheckArgument(L, 1, lua_isstring, "load_waypointrecording" );

	if(!RBLoadWaypointRecording(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int LoadEffect(lua_State* L)
{
	GetArguments(L, 1, "load_effect");
	CheckArgument(L, 1, lua_isstring, "load_effect" );

	if(!RBLoadEffect(lua_tostring(L,1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int LoadVehicleRecording(lua_State* L)
{
	GetArguments(L, 1, "load_vehiclerecording");
	CheckArgument(L, 1, lua_isstring, "load_vehiclerecording");

	if (!RBLoadVehicleRecording(lua_tostring(L,1)))
	{
		LUA_RETURN_FAILURE();
	}
	LUA_RETURN_SUCCESS();
}

//
// name:		SaveTextureDictionary
// lua command:	save_texture_dictionary
// description:	save texture dictionary
int SaveTextureDictionary(lua_State* L)
{
	GetArguments(L, 2, "save_texture_dictionary");
	CheckArgument(L, 1, lua_isstring, "save_texture_dictionary");
	CheckArgument(L, 2, lua_isstring, "save_texture_dictionary");

	const char* filename = lua_tostring( L, 1 );
	const char* suffix = lua_tostring( L, 2 );
	if( !RBSaveTextureDictionary( filename, suffix ) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		SaveTextureDictionaryHiMip
// lua command:	save_texture_dictionary_himip
// description:	save texture dictionary hi-MIP
int SaveTextureDictionaryHiMip(lua_State* L)
{
	GetArguments(L, 2, "save_texture_dictionary_himip");
	CheckArgument(L, 1, lua_isstring, "save_texture_dictionary_himip");
	CheckArgument(L, 2, lua_isstring, "save_texture_dictionary_himip");

	const char* filename = lua_tostring( L, 1 );
	const char* suffix = lua_tostring( L, 2 );
	if( !RBSaveTextureDictionaryHiMip( filename, suffix ) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		SaveClipDictionary
// description:	save clip dictionary
int SaveClipDictionary(lua_State* L)
{
	GetArguments(L, 2, "save_clip_dictionary");
	CheckArgument(L, 1, lua_isstring, "save_clip_dictionary");
	CheckArgument(L, 2, lua_isnumber, "save_clip_dictionary");

	bool useLoader = lua_tonumber(L, 2) != 0 ? true : false;
	if( !RBSaveClipDictionary( lua_tostring(L,1), useLoader ) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		SaveParamMotionDictionary
// description:	save param motion dictionary
int SaveParamMotionDictionary(lua_State* L)
{
	GetArguments(L, 1, "save_param_motion_dictionary");
	CheckArgument(L, 1, lua_isstring, "save_param_motion_dictionary");

	if( !RBSaveParamMotionDictionary(lua_tostring(L,1)) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		SaveExpressionsDictionary
// description:	save expressions dictionary
int SaveExpressionsDictionary(lua_State* L)
{
	GetArguments(L, 1, "save_expressions_dictionary");
	CheckArgument(L, 1, lua_isstring, "save_expressions_dictionary");

	if( !RBSaveExpressionsDictionary(lua_tostring(L,1)) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}
//
// name:		SaveModelDictionary
// description:	save model dictionary
int SaveModelDictionary(lua_State* L)
{
	GetArguments(L, 1, "save_model_dictionary");
	CheckArgument(L, 1, lua_isstring, "save_model_dictionary");

	if( !RBSaveModelDictionary(lua_tostring(L,1)) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		SaveModelDictionary
// description:	save model dictionary
int SaveClothDictionary(lua_State* L)
{
	GetArguments(L, 1, "save_cloth_dictionary");
	CheckArgument(L, 1, lua_isstring, "save_cloth_dictionary");

	if( !RBSaveClothDictionary(lua_tostring(L,1)) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}


//
// name:		SaveBlendShapeDictionary
// description:	save blendshape dictionary
int SaveBlendShapeDictionary(lua_State* L)
{
	GetArguments(L, 1, "save_blendshape_dictionary");
	CheckArgument(L, 1, lua_isstring, "save_blendshape_dictionary");

	if( !RBSaveBlendShapeDictionary(lua_tostring(L,1)) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		SaveBoundDictionary
// description:	save bound dictionary
int SaveBoundDictionary(lua_State* L)
{
	GetArguments(L, 1, "save_bound_dictionary");
	CheckArgument(L, 1, lua_isstring, "save_bound_dictionary");

	if( !RBSaveBoundDictionary(lua_tostring(L,1)) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}


//
// name:		SavePhysicsDictionary
// description:	save physics dictionary
int SavePhysicsDictionary(lua_State* L)
{
	GetArguments(L, 1, "save_physics_dictionary");
	CheckArgument(L, 1, lua_isstring, "save_physics_dictionary");

	if( !RBSavePhysicsDictionary(lua_tostring(L,1)) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		SaveFrameFilterDictionary
// description:	save frame filter dictionary
int SaveFrameFilterDictionary(lua_State* L)
{
	GetArguments(L, 1, "save_framefilter_dictionary");
	CheckArgument(L, 1, lua_isstring, "save_framefilter_dictionary");

	if( !RBSaveFrameFilterDictionary(lua_tostring(L,1)) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		SaveTexture
// description:	save a single texture. save the last on the list
int SaveTexture(lua_State* L)
{
	GetArguments(L, 1, "save_texture");
	CheckArgument(L, 1, lua_isstring, "save_texture");

	if( !RBSaveTexture(lua_tostring(L,1)) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		SaveAnim
// description:	save a single anim. save the last on the list
int SaveAnim(lua_State* L)
{
	GetArguments(L, 1, "save_anim");
	CheckArgument(L, 1, lua_isstring, "save_anim");

	if( !RBSaveAnim(lua_tostring(L,1)) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}


//
// name:		SaveModel
// description:	save a single model. save the last on the list
int SaveModel(lua_State* L)
{
	GetArguments(L, 1, "save_model");
	CheckArgument(L, 1, lua_isstring, "save_model");

	if( !RBSaveModel(lua_tostring(L,1)) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		SaveBlendShape
// description:	save a single blendshape. save the last on the list
int SaveBlendShape(lua_State* L)
{
	GetArguments(L, 2, "save_blendshape");
	CheckArgument(L, 1, lua_isstring, "input_basename");
	CheckArgument(L, 2, lua_isstring, "output_basename");

	if( !RBSaveBlendShape(lua_tostring(L,1), lua_tostring(L,2)) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}


//
// name:		SaveFragment
// description:	save a single fragment. save the last on the list
int SaveFragment(lua_State* L)
{
	GetArguments(L, 1, "save_fragment");
	CheckArgument(L, 1, lua_isstring, "save_fragment");\

	if( !RBSaveFragment(lua_tostring(L,1)) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		SaveBound
// description:	save a single bound. save the last on the list
int SaveBound(lua_State* L)
{
	GetArguments(L, 1, "save_bound");
	CheckArgument(L, 1, lua_isstring, "save_bound");

	if( !RBSaveBound(lua_tostring(L,1)) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		SavePathRegion
// description:	save a pathRegion file. save the last on the list
int SavePathRegion(lua_State* L)
{
	GetArguments(L, 1, "save_pathregion");
	CheckArgument(L, 1, lua_isstring, "save_pathregion");

	if( !RBSavePathRegion(lua_tostring(L,1)) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		SaveNavMesh
// description:	save a navmesh file. save the last on the list
int SaveNavMesh(lua_State* L)
{
	GetArguments(L, 1, "save_navmesh");
	CheckArgument(L, 1, lua_isstring, "save_navmesh");

	if( !RBSaveNavMesh(lua_tostring(L,1)) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int SaveHeightMesh(lua_State* L)
{
	GetArguments(L, 1, "save_heightmesh");
	CheckArgument(L, 1, lua_isstring, "save_heightmesh");

	if( !RBSaveHeightMesh(lua_tostring(L,1)) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int SaveAudMesh(lua_State* L)
{
	GetArguments(L, 1, "save_audmesh");
	CheckArgument(L, 1, lua_isstring, "save_audmesh");

	if( !RBSaveAudMesh(lua_tostring(L, 1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int SaveRbf(lua_State* L)
{
	GetArguments(L, 1, "save_rbf");
	CheckArgument(L, 1, lua_isstring, "save_rbf");

	if( !RBSaveRbf(lua_tostring(L, 1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int SavePso(lua_State* L)
{
	GetArguments(L, 1, "save_pso");
	CheckArgument(L, 1, lua_isstring, "save_pso");

	if( !RBSavePso(lua_tostring(L, 1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int SavePsoAs(lua_State* L)
{
	GetArguments(L, 2, "save_pso");
	CheckArgument(L, 1, lua_isstring, "save_pso");
	CheckArgument(L, 2, lua_isstring, "save_pso");

	const char* destfile = lua_tostring(L, 1);
	const char* fmt = lua_tostring(L, 2);

	if( !RBSavePsoAs(destfile, fmt))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}


// Need to wrap the macro isboolean in a function
int IsBooleanWrapper(lua_State* L, int index)
{
	return lua_isboolean(L, index);
}

int IsPsoInPlaceLoadable(lua_State* L)
{
	GetArguments(L, 2, "is_pso_inplace_loadable");
	CheckArgument(L, 1, lua_isstring, "is_pso_inplace_loadable");
	CheckArgument(L, 2, IsBooleanWrapper, "is_pso_inplace_loadable");

	int status = RBIsPsoInPlaceLoadable(lua_tostring(L, 1), !!lua_toboolean(L, 2));

	lua_pushnumber(L, status);

	return 1;
}

int PsoDebugStringSize(lua_State* L)
{
	GetArguments(L, 1, "pso_debugstring_size");
	CheckArgument(L, 1, lua_isstring, "pso_debugstring_size");
	int size = RBPsoDebugStringSize(lua_tostring(L, 1));
	lua_pushnumber(L, size);
	return 1;
}

int SaveMapData( lua_State* L )
{
	GetArguments( L, 1, "save_mapdata" );
	CheckArgument( L, 1, lua_isstring, "save_mapdata" );

	if ( !RBSaveMapData( lua_tostring(L, 1) ) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int SaveMapTypes( lua_State* L )
{
	GetArguments( L, 1, "save_maptypes" );
	CheckArgument( L, 1, lua_isstring, "save_maptypes" );

	if ( !RBSaveMapTypes( lua_tostring(L, 1) ) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int SaveImf( lua_State* L )
{
	GetArguments( L, 1, "save_imf" );
	CheckArgument( L, 1, lua_isstring, "save_imf" );

	if ( !RBSaveImf( lua_tostring( L, 1 ) ) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int SaveScript( lua_State* L )
{
	GetArguments(L, 1, "save_script");
	CheckArgument(L, 1, lua_isstring, "save_script");

	if( !RBSaveScript(lua_tostring(L, 1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int SaveOccImap( lua_State* L )
{
	GetArguments(L, 1, "save_occ_imap");
	CheckArgument(L, 1, lua_isstring, "save_occ_imap");

	if( !RBSaveOccImap(lua_tostring(L, 1)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int SaveOccXml( lua_State* L )
{
	GetArguments(L, 1, "save_occ_imap");
	CheckArgument(L, 1, lua_isstring, "save_occ_imap");

	if( !RBSaveOccXml( lua_tostring(L, 1) ) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int SaveTextDatabase( lua_State* L )
{
	GetArguments(L, 1, "save_textdatabase");
	CheckArgument(L, 1, lua_isstring, "save_textdatabase");

	if( !RBSaveTextDatabase( lua_tostring(L, 1) ) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int SaveCutfile( lua_State* L )
{
	GetArguments( L, 1, "save_cutfile" );
	CheckArgument( L, 1, lua_isstring, "save_cutfile" );

	if ( !RBSaveCutfile( lua_tostring( L, 1 ) ) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int SaveHierarchicalNav(lua_State* L)
{
	GetArguments(L, 1, "save_hierarchicalnav");
	CheckArgument(L, 1, lua_isstring, "save_hierarchicalnav");

	if( !RBSaveHierarchicalNav(lua_tostring(L,1)) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int SaveWaypointRecording(lua_State* L)
{
	GetArguments(L, 1, "save_waypointrecording");
	CheckArgument(L, 1, lua_isstring, "save_waypointrecording");

	if( !RBSaveWaypointRecording(lua_tostring(L,1)) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int SaveEffect(lua_State* L)
{
	GetArguments(L, 1, "save_effect");
	CheckArgument(L, 1, lua_isstring, "save_effect");

	if( !RBSaveEffect(lua_tostring(L,1)) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int SaveVehicleRecording(lua_State* L)
{
	GetArguments(L, 1, "save_vehiclerecording");
	CheckArgument(L, 1, lua_isstring, "save_vehiclerecording");

	if( !RBSaveVehicleRecording(lua_tostring(L,1)) )
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

//
// name:		SetMaxMipMapCount
// description:
int SetMaxMipMapCount(lua_State* L)
{
	GetArguments(L, 1, "set_texture_maxmipmap");
	CheckArgument(L, 1, lua_isnumber, "set_texture_maxmipmap");

	RBSetMaxMipMapCount((int)lua_tonumber(L,1));

	LUA_RETURN_SUCCESS();
}

int SetMipMapInterleaving(lua_State* L)
{
	GetArguments(L, 1, "set_mipmap_interleaving");
	CheckArgument(L, 1, lua_isnumber, "set_mipmap_interleaving");

	RBSetMipMapInterleaving((lua_tonumber(L,1) == 0) ? true : false);

	LUA_RETURN_SUCCESS();
}

//
// name:		SetTextureMin
// description:
int SetTextureMin(lua_State* L)
{
	GetArguments(L, 1, "set_texture_min");
	CheckArgument(L, 1, lua_isnumber, "set_texture_min");

	RBSetTextureMin((int)lua_tonumber(L,1));

	LUA_RETURN_SUCCESS();
}

//
// name:		SetTextureMax
// description:
int SetTextureMax(lua_State* L)
{
	GetArguments(L, 1, "set_texture_max");
	CheckArgument(L, 1, lua_isnumber, "set_texture_max");

	RBSetTextureMax((int)lua_tonumber(L,1));

	LUA_RETURN_SUCCESS();
}

//
// name:		SetTextureScale
// description:
int SetTextureScale(lua_State* L)
{
	GetArguments(L, 1, "set_texture_scale");
	CheckArgument(L, 1, lua_isnumber, "set_texture_scale");

	RBSetTextureScale((float)lua_tonumber(L,1));

	LUA_RETURN_SUCCESS();
}

//
// name:		SetOverloadTextureSrc
// description:
int SetOverloadTextureSrc(lua_State* L)
{
	GetArguments(L, 1, "set_overload_texture_src");
	CheckArgument(L, 1, lua_isstring, "set_overload_texture_src");

	RBSetOverloadTextureSrc(lua_tostring(L,1));

	LUA_RETURN_SUCCESS();
}

//
// name:		SetLimitExemptTextures
// description:
int SetLimitExemptTextures(lua_State* L)
{
	GetArguments(L, 1, "set_limitexempt_textures");
	CheckArgument(L, 1, lua_isstring, "set_limitexempt_textures");

	RBSetLimitExemptTextures(lua_tostring(L,1));

	LUA_RETURN_SUCCESS();
}

// SetCompressResources
// Enable/disable compression of rage resources (this is not the same as compressing things inside a packfile)
int SetCompressResources(lua_State* L)
{
	GetArguments(L, 1, "set_compress_resources");
	CheckArgument(L, 1, lua_isnumber, "set_compress_resources");

	RBSetCompressResources(lua_tonumber(L,1) == 1);

	LUA_RETURN_SUCCESS();
}

// SetTriStripping
// Enable/disable compression of rage resources (this is not the same as compressing things inside a packfile)
int SetTriStripping(lua_State* L)
{
	GetArguments(L, 1, "set_tristripping");
	CheckArgument(L, 1, lua_isnumber, "set_tristripping");

	RBSetTriStripping((lua_tonumber(L,1) == 1));

	LUA_RETURN_SUCCESS();
}

int PushBuildVars(lua_State* L)
{
	RBPushBuildVars();
	LUA_RETURN_SUCCESS();
}

int PopBuildVars(lua_State* L)
{
	RBPopBuildVars();
	LUA_RETURN_SUCCESS();
}

int LoadTextureList(lua_State* L)
{
	GetArguments(L, 1, "load_texlist");
	CheckArgument(L, 1, lua_isstring, "load_texlist");

	RBLoadTextureList(lua_tostring(L,1));

	LUA_RETURN_SUCCESS();
}

int DoesTexHaveMips(lua_State* L)
{
	GetArguments(L, 1, "does_tex_havemips");
	CheckArgument(L, 1, lua_isstring, "does_tex_havemips");

	if(RBDoesTexHaveMips(lua_tostring(L,1)))
		LUA_RETURN_SUCCESS();

	LUA_RETURN_FAILURE();
}

int PostProcessVehicle(lua_State* L)
{
	GetArguments(L, 1, "postprocess_vehicle");
	CheckArgument(L, 1, lua_isnumber, "postprocess_vehicle");

	if(RBPostProcessVehicle(lua_tonumber(L,1) == 1))
		LUA_RETURN_SUCCESS();

	LUA_RETURN_FAILURE();
}

int HeadBlendGeometry(lua_State* L)
{
	GetArguments(L, 1, "head_blend_geometry");
	CheckArgument(L, 1, lua_isnumber, "head_blend_geometry");

	if(RBHeadBlendGeometry(lua_tonumber(L,1) == 1))
		LUA_RETURN_SUCCESS();

	LUA_RETURN_FAILURE();
}

int HeadBlendWriteBuffer(lua_State* L)
{
	GetArguments(L, 1, "head_blend_write_buffer");
	CheckArgument(L, 1, lua_isnumber, "head_blend_write_buffer");

	if(RBHeadBlendWriteBuffer(lua_tonumber(L,1) == 1))
		LUA_RETURN_SUCCESS();

	LUA_RETURN_FAILURE();
}

int MicroMorphMesh(lua_State* L)
{
	GetArguments(L, 1, "micro_morph_mesh");
	CheckArgument(L, 1, lua_isnumber, "micro_morph_mesh");

	if(RBMicroMorphMesh(lua_tonumber(L,1) == 1))
		LUA_RETURN_SUCCESS();

	LUA_RETURN_FAILURE();
}

int EnableFragMatrixSet(lua_State* L)
{
	GetArguments(L, 1, "enable_frag_matrixset");
	CheckArgument(L, 1, lua_isnumber, "enable_frag_matrixset");

	if(RBEnableFragMatrixSet(lua_tonumber(L,1) == 1))
		LUA_RETURN_SUCCESS();

	LUA_RETURN_FAILURE();
}

int PPUOnlyHint(lua_State* L)
{
	GetArguments(L, 1, "PPUOnlyHint");
	CheckArgument(L, 1, lua_isnumber, "PPUOnlyHint");

	if(RBPPUOnlyHint(lua_tonumber(L,1) == 1))
		LUA_RETURN_SUCCESS();

	LUA_RETURN_FAILURE();
}

int LodSkeleton(lua_State* L)
{
	GetArguments(L, 1, "lod_skeleton");
	CheckArgument(L, 1, lua_isnumber, "lod_skeleton");

	if(RBLodSkeleton(lua_tonumber(L,1) == 1))
		LUA_RETURN_SUCCESS();

	LUA_RETURN_FAILURE();
}

int GetSettings(lua_State* L)
{
	const char* buffer = RBGetSettings();
	lua_pushstring(L, buffer);
	return 1;
}

int SetSettings(lua_State* L)
{
	GetArguments(L, 1, "set_settings");
	CheckArgument(L, 1, lua_isstring, "set_settings");

	if(RBSetSettings(lua_tostring(L,1)))
		LUA_RETURN_SUCCESS();

	LUA_RETURN_FAILURE();
}

// CompressAnim
// For compressing uncompressed exported animations
int CompressAnim(lua_State* L)
{
	GetArguments(L, 11, "compress_anim");
	CheckArgument(L, 1, lua_isstring, "src_anim");
	CheckArgument(L, 2, lua_isstring, "skel");
	CheckArgument(L, 3, lua_isstring, "out_anim");
	CheckArgument(L, 4, lua_isstring, "max_block_size");
	CheckArgument(L, 6, lua_isstring, "compression_rotation");
	CheckArgument(L, 5, lua_isstring, "compression_translation");
	CheckArgument(L, 7, lua_isstring, "compression_default");
	CheckArgument(L, 8, lua_isstring, "compression_cost");
	CheckArgument(L, 9, lua_isstring, "decompression_cost");
	CheckArgument(L, 10, lua_isstring, "compression_rules");
	CheckArgument(L, 11, lua_isnumber, "raw_only");

	const char* srcfile = lua_tostring(L, 1);
	const char* skelfile = lua_tostring(L, 2);
	const char* outfile = lua_tostring(L, 3);
	int maxblocksize = (int)lua_tonumber(L, 4);
	float compressionrotation = (float)lua_tonumber(L, 5);
	float compressiontranslation = (float)lua_tonumber(L, 6);
	float compressiondefault = (float)lua_tonumber(L, 7);
	int compressioncost = (int)lua_tonumber(L, 8);
	int decompressioncost = (int)lua_tonumber(L, 9);
	const char* compressionrules = lua_tostring(L, 10);
	int rawonly = (int)lua_tonumber(L, 11);

	if( RBCompressAnim(srcfile, skelfile, outfile, maxblocksize, compressionrotation, compressiontranslation, compressiondefault, compressiondefault, compressioncost, decompressioncost, compressionrules, (rawonly == 1) ? true : false ))
		LUA_RETURN_SUCCESS();

	LUA_RETURN_FAILURE();
}

int GetClipPropertyString(lua_State* L)
{
	GetArguments(L, 3, "get_clip_property");
	CheckArgument(L, 1, lua_isstring, "property_name");
	CheckArgument(L, 2, lua_isstring, "clip_file");
	//CheckArgument(L, 3, lua_isnumber, "use_loader");

	bool useLoader = lua_toboolean(L, 3) != 0 ? true : false;
	lua_pushstring(L, RBGetClipPropertyString(lua_tostring(L, 1), lua_tostring(L, 2), useLoader ));
	return 1;

}

int GetClipPropertyInt(lua_State* L)
{
	GetArguments(L, 3, "get_clip_property");
	CheckArgument(L, 1, lua_isstring, "property_name");
	CheckArgument(L, 2, lua_isstring, "clip_file");
	//CheckArgument(L, 3, lua_isnumber, "use_loader");
	
	bool useLoader = lua_toboolean(L, 3) != 0 ? true : false;
	lua_pushnumber(L, RBGetClipPropertyInt(lua_tostring(L, 1), lua_tostring(L, 2), useLoader ));
	return 1;

}

int GetClipPropertyFloat(lua_State* L)
{
	GetArguments(L, 3, "get_clip_property");
	CheckArgument(L, 1, lua_isstring, "property_name");
	CheckArgument(L, 2, lua_isstring, "clip_file");
	//CheckArgument(L, 3, lua_isnumber, "use_loader");
	
	bool useLoader = lua_toboolean(L, 3) != 0 ? true : false;
	lua_pushnumber(L, RBGetClipPropertyFloat(lua_tostring(L, 1), lua_tostring(L, 2), useLoader));
	return 1;

}

int SetClipPropertyString(lua_State* L)
{
	GetArguments(L, 4, "set_clip_property");
	CheckArgument(L, 1, lua_isstring, "property_name");
	CheckArgument(L, 2, lua_isstring, "property_value");
	CheckArgument(L, 3, lua_isstring, "clip_file");
	//CheckArgument(L, 4, lua_isnumber, "use_loader");

	bool useLoader = lua_toboolean(L, 4) != 0 ? true : false;
	if(RBSetClipPropertyString(lua_tostring(L, 1), lua_tostring(L, 2), lua_tostring(L, 3), useLoader))
		LUA_RETURN_SUCCESS();
	
	LUA_RETURN_FAILURE();
}

int ConvertAndSaveDDS(lua_State* L)
{
	GetArguments(L, 4, "convert_and_save_dds");
	CheckArgument(L, 1, lua_isstring, "src_dds_file");
	CheckArgument(L, 2, lua_isstring, "dest_dds_file");
	CheckArgument(L, 3, lua_isstring, "template_name");
	CheckArgument(L, 4, lua_isstring, "platform");
	
	if( RBConvertAndSaveDDS( lua_tostring(L, 1), lua_tostring(L, 2), lua_tostring(L, 3), lua_tostring(L, 4) ) )
		LUA_RETURN_SUCCESS();

	LUA_RETURN_FAILURE();
}


int ValidateClip(lua_State* L)
{
	GetArguments(L, 1, "validate_clip");
	CheckArgument(L, 1, lua_isstring, "clip_file");
	if(RBValidateClip(lua_tostring(L, 1)))
		LUA_RETURN_SUCCESS();

	LUA_RETURN_FAILURE();
}

int SetDwdType(lua_State* L)
{
	GetArguments(L, 1, "set_dwd_type");
	CheckArgument(L, 1, lua_isstring, "dwd_type");
	if(RBSetDwdType(lua_tostring(L, 1)))
		LUA_RETURN_SUCCESS();

	LUA_RETURN_FAILURE();
}

int GenerateDrawableStats(lua_State* L)
{
	GetArguments(L, 2, "generate_drawable_stats_rb");
	CheckArgument(L, 1, lua_isstring, "drawable_pathname" );
	CheckArgument(L, 2, lua_isstring, "output_pathname" );

	if(!RBGenerateDrawableStats(lua_tostring(L,1), lua_tostring(L,2)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int GenerateFragmentStats(lua_State* L)
{
	GetArguments(L, 2, "generate_fragment_stats_rb");
	CheckArgument(L, 1, lua_isstring, "fragment_pathname" );
	CheckArgument(L, 2, lua_isstring, "output_pathname" );

	if(!RBGenerateFragmentStats(lua_tostring(L,1), lua_tostring(L,2)))
		LUA_RETURN_FAILURE();

	LUA_RETURN_SUCCESS();
}

int SetEdgify(lua_State* L)
{
	GetArguments(L, 1, "SetEdgify");
	CheckArgument(L, 1, lua_isnumber, "SetEdgify");

	if(RBSetEdgify(lua_tonumber(L,1) == 1))
		LUA_RETURN_SUCCESS();

	LUA_RETURN_FAILURE();
}

