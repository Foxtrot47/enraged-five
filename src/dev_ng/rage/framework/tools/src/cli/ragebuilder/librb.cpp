//
// Ragebuilder library header.
//
// Copyright (C) 1999-2014 Rockstar North.  All Rights Reserved.
//

#include "librb.h"

// C headers
#if !__XENON
#include "direct.h"
#endif

// RAGE headers
#include "system/param.h"
#include "file/asset.h"
#include "diag/output.h"
#include "file/device.h"
#include "system/nelem.h"
#include "system/stack.h"

using namespace rage;

// project includes
#include "ragefstream.h"
#include "ragebuilder.h"
#include "log.h"
#include <lua.h>

// Filestream factory - sort of...
WinLib::FileStream* RageCreateStream()
{
	return new RageFileStream;
}

bool RBInit()
{
#ifdef RAGEBUILDER_LIB

	static int __argc = 0;
	static char** __argv = NULL;

	if(__argc == 0)
	{
		__argc = 1;
		__argv = new char*[1];
		__argv[0] = new char[64];
		strcpy(__argv[0],"ragebuilder_lib");
	}

	sysParam::Init(__argc,__argv);

#else

	extern int __argc;
	extern char **__argv;

	sysParam::Init(__argc,__argv);

#endif

	rbCoreInit();

	return true;
}

bool RBShutdown()
{
	rbCoreShutdown();

	return true;
}

bool rbCoreInit()
{
	RageBuilder::Init();

	char* tempdir = getenv( "TEMP" );

	char buildlog[MAX_PATH];
	char errorlog[MAX_PATH];

	sprintf(buildlog, "%s\\buildlog.txt", tempdir);
	sprintf(errorlog, "%s\\errorlog.txt", tempdir);

	LogListener::Init(buildlog, errorlog);

	// get the current working dir on systems that support it.
#if __XENON
	ASSET.SetPath("X:\\");
#else
	char rootDirName[260];
	_getcwd(rootDirName, 260);
	strcat(rootDirName, "\\");
	ASSET.SetPath(rootDirName);
#endif

	WinLib::FileStream::SetCreateStream(RageCreateStream);

	return true;
}

bool rbCoreShutdown()
{
	RageBuilder::Close();
	LogListener::Close();

	return true;
}

#ifndef RAGEBUILDER_LIB

// Display a header on startup.
int PrintVersion(lua_State* L)
{
	rbDisplayf("%s using " LUA_VERSION "  " LUA_COPYRIGHT "\n", sysParam::GetArg(0));

	LUA_RETURN(true);
}

int GetHashCode(lua_State* L)
{
	char baseName[64];
	ASSET.BaseName(baseName, 64, lua_tostring(L, 1));
	u32 code = atStringHash(baseName);

	lua_pushnumber(L, code );

	return 1;
}

int GetExeName(lua_State* L)
{
	lua_pushstring(L, sysParam::GetArg(0) );
	return 1;
}

// lua includes
#define lua_c
extern "C"
{
# include "lua.h"
# include "lauxlib.h"
# include "lualib.h"
}

// lua command includes
#include "luacore.h"
#include "luageneral.h"
#include "luapack.h"
#include "luazip.h"
#include "luarage.h"
#include "luagta.h"
#include "luaanim.h"

static CLuaCore* g_pLuaCore;

// Commands in data format, so we can add some help
typedef struct _LuaCommand
{
	const char*		name;
	LuaCommandFn*	command;
	const char*		description;
} LuaCommand;

LuaCommand gs_commands[] =
{
	{ "init_rage",						InitRage,				"Initialises the rage engine." },
	{ "shutdown_rage",					ShutdownRage,			"Shuts down the rage engine." },
	{ "set_platform",					SetPlatform,			"Set the target platform." },
	{ "get_platform",					GetPlatform,			"Get the target platform." },

	{ "set_root_dir",					SetRootDirectory,		"Set the root director for file loads and saves."},
	{ "get_env",						GetEnv,					"Get an environment variable." },
	{ "set_env",						SetEnv,					"Set an environment variable." },
	{ "get_param",						GetParam,				"Get one of the command line arguments." },
	{ "get_paramcount",					GetParamCount,			"Get number of command line arguments." },
	{ "get_parambyindex",				GetParamByIndex,		"Get command line argument by index." },
	{ "wait",							Wait,					"Wait for a key press." },
	{ "sleep",							Sleep,					"Sleep for specified number of milliseconds." },

	//	for the moment the messages 	that cause the most useless messages have been commented out....
	//	to be replaced with this

	{ "set_loglevel",					SetLogLevel,			"Set the level of output." },
	{ "set_stoponerror",				SetStopOnError,			"Enable or disable stop on error." },
	{ "display",						ReportDisplay,			"Report an message" },
	{ "error",							ReportError,			"Report an error" },
	{ "warning",						ReportWarning,			"Report a warning" },
	{ "quit",							ReportQuit,				"Report a quit" },
	{ "trace1",							ReportTrace1,			"Report an message with level 1" },
	{ "trace2",							ReportTrace2,			"Report an message with level 2" },
	{ "trace3",							ReportTrace3,			"Report an message with level 3" },

	{ "get_buildlog_filename",			GetBuildLogFilename,	"Return the filename of the build log" },
	{ "get_errorlog_filename",			GetErrorLogFilename,	"Return the filename of the error log" },

	{ "file_exists",					FileExists,				"Does the file exist." },
	{ "dir_exists",						DirectoryExists,		"Does the directory exist." },
	{ "del_file",						DelFile,				"Delete a specified file." },
	{ "del_dir",						DelDir,					"Delete a specified directory ." },
	{ "find_files",						FindFiles,				"Returns a list of files according to a file filter." },
	{ "find_platform_files",			FindPlatformFiles,		"Returns a list of files according to a file filter with a platform specific extension." },
	{ "get_platform_extension",			GetPlatformExtension,	"Returns a platform specific extension for a file type."},
	{ "convert_filename_to_platform",	ConvertFilenameToPlatform,	"Converts a filename to a platform specific version."},

	{ "find_dirs",						FindDirs,				"Returns a list of the subdirectories of a passed in directory." },
	{ "remove_extension_from_path",		RemoveExtensionFromPath,"Removes the extension part of a file path."},
	{ "remove_all_extensions_from_path", RemoveAllExtensionsFromPath, "Removes all of the extension strings from a file path." },
	{ "get_extension_from_path",		GetExtensionFromPath,	"Gets the extension part of a file path."},
	{ "get_filename_from_path",			GetFilenameFromPath,	"Gets the filename part of a file path."},
	{ "remove_name_from_path",			RemoveNameFromPath,		"Gets the root path of a file path."},
	{ "create_leadingpath",				CreateLeadingPath,		"Create the path that leads to the given filename"},
	{ "set_file_attrib",				SetFileAttrib,			"Set a file attribute"},
	{ "get_file_attrib",				GetFileAttrib,			"Get a file attribute"},
	{ "clone_timestamp",				CloneTimestamp,			"Copy the timestamp from one file to another"},

	{ "copy_file",						Copy_File,				"Copy a file from a source to a destination" },
	{ "move_file",						Move_File,				"Move a file from a source to a destination" },
	{ "filetime_compare",				FileTimeCompare,		"Compares two files for time (> -1, < 1,= 0)" },
	{ "filesize",						FileSize,				"Return the size of a file" },
	{ "filetime",						Get_FileTime,			"Return file modified timestamp" },

	{ "resource_size",					ResourceSize,			"Return the total size of a resource (0 if not a resource)." },
	{ "resource_physical_size",			ResourcePhysicalSize,	"Return the physical size of a resource (0 if not a resource)." },
	{ "resource_virtual_size",			ResourceVirtualSize,	"Return the virtual size of a resource (0 if not a resource)." },

	{ "find_xml_match",					FindXMLMatch,			"Load an xml and translates one string to another" },

	{ "start_build",					StartBuild,				"Start a new dictionary." },
//	{ "set_tuning_folder",				SetTuningFolder,		"Sets the source folder for finding tune files for fragments" },

	{ "load_textures_templates",		LoadTextureTemplates,	"Load all texture templates according to a file filter."},
	{ "load_textures",					LoadTextures,			"Load one or more textures according to a file filter."},
	{ "load_models",					LoadModels,				"Load one or more models according to a file filter."},
	{ "load_texture",					LoadTexture,			"Load a texture"},
	{ "load_anims",						LoadAnims,				"Load one or more animations according to a file filter."},
	{ "load_clips",						LoadClips,				"Load one or more clips according to a file filter."},
	{ "load_param_motions",				LoadParamMotions,		"Load one or more param motions according to a file filter."},
	{ "load_expressions_list",			LoadExpressionsList,	"Load one or more expressions according to a file filter."},
	{ "load_framefilter_list",			LoadFrameFilterList,	"Load one or more framefilter according to a file filter."},

	{ "load_model",							LoadModel,							"Load a model."},
	{ "load_blendshape",					LoadBlendShape,						"Load a blend shape."},
	{ "load_fragment",						LoadFragment,						"Load a fragment."},
	{ "load_nm_data",						LoadNMData,							"Load NM data onto the current fragment"},
	{ "load_bounds",						LoadBounds,							"Load one or more bounds according to a file filter."},
	{ "load_pathregion",					LoadPathRegion,						"Load a pathregion file"},
	{ "load_navmesh",						LoadNavMesh,						"Load a navmesh"},
	{ "load_heightmesh",					LoadHeightMesh,						"Load a heightmesh (navmesh for aerial navigation)."},
	{ "load_hierarchicalnav",				LoadHierarchicalNav,				"Load a hierarchical nav file"},
	{ "load_waypointrecording",				LoadWaypointRecording,				"Load a waypoint recording file"},
	{ "load_anim",							LoadAnim,							"Load an animation."},
	{ "load_clip",							LoadClip,							"Load a clip."},
	{ "load_param_motion",					LoadParamMotion,					"Load a parameterised motion."},
	{ "load_expressions",					LoadExpressions,					"Load an expressions."},
	{ "load_framefilter",					LoadFrameFilter,					"Load a frame filter."},

	{ "load_bound",						LoadBound,				"Load a bound."},
	{ "load_effect",					LoadEffect,				"Load an effect."},
	{ "load_vehiclerecording",			LoadVehicleRecording,	"Load a vehicle recording file"},
	{ "load_audmesh",					LoadAudMesh,			"Load an audmesh file" },
	{ "load_meta",						LoadMeta,				"Load an meta file" },
	{ "load_script",					LoadScript,				"Load a script file" },
	{ "load_occ_meshes",				LoadOccMeshes,			"Load an occlusion mesh" },

	{ "save_texture_dictionary",		SaveTextureDictionary,	"Save a built texture dictionary."},
	{ "save_texture_dictionary_himip",	SaveTextureDictionaryHiMip, "Save a built hi-MIP texture dictionary." },
	{ "save_clip_dictionary",			SaveClipDictionary,		"Save a built clip dictionary."},
	{ "save_param_motion_dictionary",	SaveParamMotionDictionary,		"Save a built PM dictionary."},
	{ "save_expressions_dictionary",	SaveExpressionsDictionary,		"Save a built PM dictionary."},
	{ "save_model_dictionary",			SaveModelDictionary,	"Save a model dictionary."},
	{ "save_bound_dictionary",			SaveBoundDictionary,	"Save a bound dictionary."},
	{ "save_physics_dictionary",		SavePhysicsDictionary,	"Save a physics dictionary."},
	{ "save_blendshape_dictionary",		SaveBlendShapeDictionary,"Save a blend shape dictionary."},
	{ "save_cloth_dictionary",			SaveClothDictionary,	"Save a cloth dictionary."},
	{ "save_framefilter_dictionary",	SaveFrameFilterDictionary,		"Save a built FrameFilter dictionary."},

	{ "save_model",						SaveModel,				"Save a model."},
	{ "save_blendshape",				SaveBlendShape,			"Save a blend shape."},
	{ "save_fragment",					SaveFragment,			"Save a fragment."},
	{ "save_bound",						SaveBound,				"Save a bound."},
	{ "save_pathregion",				SavePathRegion,			"Save a pathregion file"},
	{ "save_navmesh",					SaveNavMesh,			"Save a navmesh"},
	{ "save_heightmesh",				SaveHeightMesh,			"Save a heightmesh (navmesh for aerial navigation)."},
	{ "save_hierarchicalnav",			SaveHierarchicalNav,	"Save a hierarchical nav file"},
	{ "save_waypointrecording",			SaveWaypointRecording,	"Save a waypoint recording file"},
	{ "save_texture",					SaveTexture,			"Save a texture"},
	{ "save_anim",						SaveAnim,				"Save an anim"},
	{ "save_effect",					SaveEffect,				"Save an anim"},
	{ "save_vehiclerecording",			SaveVehicleRecording,	"Save a vehicle recording file"},
	{ "save_audmesh",					SaveAudMesh,			"Save an audmesh file"},
	{ "save_rbf",						SaveRbf,				"Save a Rockstar Binary Format file"},
	{ "save_pso",						SavePso,				"Save a Parser Serialized Object Format file"},
	{ "save_pso_as",					SavePsoAs,				"Save a Parser Serialized Object Format file in either \"iff\" or \"rsc\" style (\"iff\" is the default)"},
	{ "is_pso_inplace_loadable",		IsPsoInPlaceLoadable,	"Returns 1 if the PSO is in-place loadable, based on the current .#esd files"},
	{ "pso_debugstring_size",			PsoDebugStringSize,		"Returns the size of the debug strings in a pso file"},
	{ "save_mapdata",					SaveMapData,			"Save Map Data Binary Format file"},
	{ "save_maptypes",					SaveMapTypes,			"Save Map Types Binary Format file"},
	{ "save_imf",						SaveImf,				"Save manifest binary format file"},
	{ "save_script",					SaveScript,				"Save a script file"},
	{ "save_occ_imap",					SaveOccImap,			"Save an imap file with occlusion mesh data" },
	{ "save_occ_xml",					SaveOccXml,				"Save XML file with occlusion mesh data." },
	{ "save_textdatabase",				SaveTextDatabase,		"Save a text database."},
	{ "save_cutfile",					SaveCutfile,			"Save cutxml metadata to PSO." },

	{ "start_pack",						StartPack,				"Start a new pack file." },
	{ "start_pack_xcompress",			StartPackXCompress,		"Start a new xcompress pack file." },
	{ "start_pack_zlib",				StartPackZlib,			"Start a new zlib pack file." },
	{ "start_uncompressed_pack",		StartUncompressedPack,	"Start an uncompressed new pack file." },
	{ "save_pack",						SavePack,				"Save the current pack file." },
	{ "add_to_pack",					AddToPack,				"Add one or more files to the pack according to a file filter." },
	{ "close_pack",						ClosePack,				"Close the current pack file." },

	{ "mount_pack",						MountPack,				"Mount a pack file as a device. Optional second argument is mount path." },
	{ "unmount_pack",					UnmountPack,			"Unmount a pack file as a device." },

	{ "is_compressed",					IsCompressed,			"Check to see if a particular file within a pack file is compressed"},

	{ "extract_file_to_memory",			ExtractFileToMemory,	"Loads a file (maybe part of a packfile) in memory, returns a memory file name"},
	{ "release_file_from_memory",		ReleaseFileFromMemory,  "Frees up memory used by a memory mapped file"},

	// Zip File Support
	{ "start_zip",						StartZip,				"Start a new zip." },
	{ "start_uncompressed_zip",			StartUncompressedZip,	"Start a new uncompressed zip." },
	{ "save_zip",						SaveZip,				"Save the current zip." },
	{ "add_to_zip",						AddToZip,				"Add one or more files to the current zip according to a file filter." },
	{ "close_zip",						CloseZip,				"Close the current zip." },
	{ "mount_zip",						MountZip,				"Mount a zip file as a device.  Optional second argument is mount path." },
	{ "unmount_zip",					UnmountZip,				"Unmount a zip file as a device." },

	{ "load_materialdefs",				LoadMaterialDefs,		"Load material names from a dat file" },
	{ "load_proceduraldefs",			LoadProceduralDefs,		"Load procedural names from a dat file" },
	{ "load_texturelist",				LoadTextureList,		"Load bump map texture list" },

	{ "set_effectpath",					SetEffectPath,			"Set effect path" },
	{ "set_shaderpath",					SetShaderPath,			"Set shader path" },
	{ "set_shaderdbpath",				SetShaderDbPath,		"Set shader database path" },

	{ "set_buildpath",					SetBuildPath,			"Set build path" },
	{ "set_assetspath",					SetAssetsPath,			"Set assets path" },
	{ "set_coreassetspath",				SetCoreAssetsPath,		"Set core assets path" },
	{ "set_metadatadefinitionspath",	SetMetadataDefinitionsPath,		"Set metadata definitions path" },

	{ "push_timer",						PushTimer,				"Start a new timer and push it onto the timer stack." },
	{ "pop_timer",						PopTimer,				"Pop the last timer off the timer stack and print the time since it started." },

	{ "get_version",					GetVersion,				"returns the ragebuilder version number as a string." },

	{ "require",						Require,				"Require another lua file" },	// To make relative paths work.

	{ "set_mipmap_interleaving",		SetMipMapInterleaving,	"Set mipmap interleaving on or off" },	// Set the texture maximum dimension
	{ "set_mipmapcount_max",			SetMaxMipMapCount,		"Set the maximum number of mipmaps" },	// Set the texture maximum dimension
	{ "set_texture_minmipmap",			SetTextureMin,			"Set the texture minimum mipmap dimension" },	// Set the texture maximum dimension
	{ "set_texture_max",				SetTextureMax,			"Set the texture maximum dimension" },	// Set the texture maximum dimension
	{ "set_texture_scale",				SetTextureScale,		"Set the texture scale" },	// Set the texture scale
	{ "set_compress_resources",			SetCompressResources,	"Enable/disable the compression of resources" },
	{ "set_tristripping",				SetTriStripping,		"Enable/disable tristripping" },
	{ "set_limitexempt_textures",		SetLimitExemptTextures, "Set a list of textures that are exempt from limitations uch as min mipmap size"},
	{ "set_overload_texture_src",		SetOverloadTextureSrc,	"A source folder that textures will be loaded from, overriding load_texture location"},

	{ "push_buildvars",					PushBuildVars,			"Store the current builds vars on a stack" },
	{ "pop_buildvars",					PopBuildVars,			"Restore the top builds vars from a stack" },

	{ "print_version",					PrintVersion,			"print out the version of ragebuilder we are using" },

	{ "get_exe_name",					GetExeName,				"get the name of the exe used to run this script" },

	{ "set_auto_texdict",				SetAutoTexDict,			"set rage to create owned texture dictionary" },
	{ "register_texturename",			RegisterTextureName,	"register a texture name such that a texture in drawable local texture dictionary doesn't override it" },

	{ "get_hash_code",					GetHashCode,			"get the hash code from a string" },

	{ "does_tex_havemips",				DoesTexHaveMips,		"Does the supplied tex have mipmaps" },
	{ "postprocess_vehicle",			PostProcessVehicle,		"set the post process vehicle on/off" },
	{ "head_blend_geometry",			HeadBlendGeometry,		"Set the head blend geometry processing on/off" },
	{ "head_blend_write_buffer",		HeadBlendWriteBuffer,	"Set the head blend write buffer flag on/off" },
	{ "micro_morph_mesh",				MicroMorphMesh,			"Set the micro morph mesh flag on/off" },

	{ "enable_frag_matrixset",			EnableFragMatrixSet,	"Enables/disables creation of matrix sets for frags" },

	{ "ppu_only_hint",					PPUOnlyHint,			"set PPU as default processor for entity to come due to its complexity." },

	{ "lod_skeleton",					LodSkeleton,			"Lods the skeleton by tagging skinned bones and re-arranging the matrix indices on the verts" },

	{ "get_settings",					GetSettings,			"get a string containing all the settings for spawning a new ragebuilder" },
	{ "set_settings",					SetSettings,			"set a string containing all the settings for spawning a new ragebuilder" },

	{ "compress_anim",					CompressAnim,			"compress an uncompressed anim" },

	{ "get_clip_property_string",		GetClipPropertyString,	"get a clip property" },
	{ "convert_and_save_dds",			ConvertAndSaveDDS,		"convert and save a dds using template" },

	{ "get_clip_property_int",			GetClipPropertyInt,		"get a clip property" },

	{ "get_clip_property_float",		GetClipPropertyFloat,	"get a clip property" },

	{ "set_clip_property_string",		SetClipPropertyString,	"set a clip property" },

	{ "validate_clip",					ValidateClip,			"validate a clip" },

	{ "set_dwd_type",					SetDwdType,				"set a dwd type" },

	{ "generate_drawable_stats_rb",		GenerateDrawableStats,	"generates stats for a given drawable"},

	{ "generate_fragment_stats_rb",		GenerateFragmentStats,	"generates stats for a given fragment"},

	{ "set_edgify",			SetEdgify,		"force edgify"},
};

int gs_numCommands = sizeof(gs_commands) / sizeof(gs_commands[0]);

// register all the tool commands
void RegisterCommands(CLuaCore& lua)
{
	for(int i = 0; i < gs_numCommands; ++i)
	{
		lua.RegisterCommand(gs_commands[i].name, gs_commands[i].command);
	}
}

// Initialise the rage builder system.
bool rbLuaInit()
{
	rbCoreInit();

#if __BANK
	bkRemotePacket::PrintCommandLine();
#endif

	g_pLuaCore = new CLuaCore;
	if (!g_pLuaCore)
	{

		return false;
	}

#if !__XENON
	//	getargs(rage::sysParam::GetArgCount());  // collect arguments
	g_pLuaCore->OpenStdLibs();
#endif

	RegisterCommands(*g_pLuaCore);
	RegisterCommandsGta(*g_pLuaCore);
	RegisterCommandsAnim(*g_pLuaCore);

	//PrintVersion();

	return true;
}

// Exception handler to just display the stack if there is a crash in a script file.
static long __stdcall ExceptionFilter(EXCEPTION_POINTERS *ep)
{
	sysStack::OpenSymbolFile();

	Errorf("*** EXCEPTION %x CAUGHT at address %p", ep->ExceptionRecord->ExceptionCode, ep->ExceptionRecord->ExceptionAddress);

	PVOID trace[32] = {0};
	const unsigned count = CaptureStackBackTrace(0, NELEM(trace), trace, NULL);
	for (unsigned i=0; i<count; ++i)
	{
		char symName[256] = "unknown";
		const u32 disp = sysStack::ParseMapFileSymbol(symName, sizeof(symName), (size_t)trace[i]);
		Errorf("0x%016" SIZETFMT "x - %s+0x%" SIZETFMT "x", (size_t)trace[i], symName, disp);
	}

	sysStack::CloseSymbolFile();

	return EXCEPTION_EXECUTE_HANDLER;
}

// Run a script file.
// Returns:	0 when successfull.
//			1 when errors occured and a log was produced
//			2 when errors occured but no error log was produced
//			3 when a critical error occured (command execution failure, or error in script)
//			4 when a critical error occured (uncaught exception)
int rbLuaRunScriptFile(const char* filename)
{
	__try
	{
		int iResult = g_pLuaCore->ExecuteFile( filename );

		if (iResult)
		{
			Errorf("A fatal error occurred during the execution of %s", filename);
			return 3;
		}

		if (LogListener::GetErrorCount() > 0)
		{
			if (LogListener::GetErrorLogProduced())
			{
				return 1;
			}
			else
			{
				return 2;
			}
		}
	}
	__except (ExceptionFilter(GetExceptionInformation()))
	{
		Errorf("An uncaught exception occured during execution of %s", filename);
		return 4;
	}

	return 0;
}

void
rbLuaInteractiveConsolePrompt( )
{
	static int line = 0;
	static char exeName[RAGE_MAX_PATH] = {0};
	ASSET.BaseName( exeName, RAGE_MAX_PATH, ASSET.FileName( ::rage::sysParam::GetArg( 0 ) ) );

	printf( "%s:%.3d> ", exeName, line++ );
}

void
rbLuaInteractiveConsoleResult( int result )
{
	printf( "=> %d\n", result );
	rbLuaInteractiveConsolePrompt( );
}

int
rbLuaRunInteractiveConsole( )
{
	lua_State* pState = lua_open( );
	CLuaCore luaCore( pState );
	PrintVersion( pState );

	setvbuf( stdout, NULL, _IONBF, 0 );
	rbDisplayf( "Starting interactive console..." );

	char line[4096] = {0};
	rbLuaInteractiveConsolePrompt( );
	while ( 0 != scanf( "%s", line ) )
	{
		if ( 0 == stricmp( "quit", line ) )
			break;
		if ( 0 == stricmp( "exit", line ) )
			break;

		int result = luaCore.ExecuteText( line );
		rbLuaInteractiveConsoleResult( result );
		memset( line, 0, sizeof( char ) * 4096 );
	}

	return ( 0 );
}

// Close the rage builder system.
void rbLuaClose()
{
	if (g_pLuaCore)
	{
		delete g_pLuaCore;
		g_pLuaCore = NULL;
	}

	rbCoreShutdown();
}

#endif //RAGEBUILDER_LIB
