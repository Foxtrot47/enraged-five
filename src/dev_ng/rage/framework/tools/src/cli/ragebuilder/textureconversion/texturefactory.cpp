//
// File:: ragebuilder/textureconversion/texturefactory.cpp
// Description:: Ragebuilder's texture factory, to save adding lots of 
//               RESOURCECOMPILER nonsense to grcore's texture factory.
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 4 May 2011
//

#include "texturefactory.h"
#include "textureconversion.h"

// RAGE headers
#include "file/asset.h"
#include "file/limits.h"
#include "grcore/texture.h"
#include "grcore/image.h"
using namespace rage;

bool 
TextureFactory::Create( const char* filename, 
					    int texture_dict_id, 
						TextureList& outputs,
						grcTextureFactory::TextureCreateParams* params )
{
	sysMemStartTemp();
	grcImage::ImageList &images = *rage_new grcImage::ImageList;
	sysMemEndTemp();
	CTextureConversionSpecification* pSpec = NULL;
	bool load = grcImage::RunCustomLoadFunc( filename, images, (void**)&pSpec );
		
	for ( grcImage::ImageList::iterator it = images.begin();
		  it != images.end();
		  ++it )
	{
		int key = it->first;
		grcImage* pImage = it->second;
		if ( pImage == NULL )
			continue; 
		// Don't create grcTexture objects for stuff we don't care about.

		//HACK:  Dirty hack to slot which textures go into texture dictionaries.
		bool include = false;
		switch(texture_dict_id)
		{
		case TEXTURE_DICT_ID_DEFAULT:
			if ( key == TEXTURE_ID_MIP_CHAIN_DEFAULT || key == TEXTURE_ID_CHANNEL_012 || key == TEXTURE_ID_CHANNEL_345 )
				include = true;
			break;

		case TEXTURE_DICT_ID_HIGH:
			if ( key == TEXTURE_ID_MIP_CHAIN_HIGH || key == TEXTURE_ID_CHANNEL_012 || key == TEXTURE_ID_CHANNEL_345 )
				include = true;
			break;
		}

		if ( include == false )
		{
			sysMemStartTemp( );
			pImage->Release();
			sysMemEndTemp( );
			continue;
		}

		// Doublecheck for the sysmem flag on pc textures - sometimes it's ignored
		// (there are several other ways to check the platform, but this one seems to work)
		if(RageBuilder::GetPlatform()==RageBuilder::pltWin64 && pSpec->m_sysmem)
			pImage->SetIsSysMem();

		char basename[RAGE_MAX_PATH] = {0};		
		ASSET.BaseName( basename, RAGE_MAX_PATH, ASSET.FileName( filename ) );
		grcTexture::SetCustomLoadName( basename );
		grcTexture* pTexture = grcTextureFactory::GetInstance().Create( pImage, params );
		pTexture->RunCustomProcessFunc( pSpec );
		grcTexture::SetCustomLoadName( NULL );

		sysMemStartTemp( );
		outputs[key] = pTexture;
		sysMemEndTemp( );
	}

	sysMemStartTemp( );
	for ( grcImage::ImageList::iterator it = images.begin();
		  it != images.end();
		  ++it )
	{
		(*it).second = NULL;
	}
	delete &images;
	sysMemEndTemp( );

	return ( load );
}