#ifndef __RAGEBUILDER_PROGRESS_H__
#define __RAGEBUILDER_PROGRESS_H__

//
//
//
class Progress
{
public:
	static bool ProgressSizeCallback( ::rage::u32 size );
	static bool ProgressPosCallback( ::rage::u32 pos );

private:
	static ::rage::u32 ms_uNumFiles;
};

#endif // __RAGEBUILDER_PROGRESS_H__
