#include <conio.h>

// rage headers
#include "atl/map.h"
#include "system/xtl.h"
#include "system/param.h"
#include "file/asset.h"
#include "file/device.h"
#include "system/performancetimer.h"

#include <algorithm>
// Boost Headers Files
#pragma warning( push )
#pragma warning( disable : 4512 )
#define BOOST_MSVC_FULL_VER_WORKAROUND_GUARD 0
#include "boost/algorithm/string/replace.hpp"
#pragma warning( pop )

// project headers
#include "luacore.h"
#include "ragebuilder.h"
#include "utils.h"
#include "log.h"
#include "librb.h"
#include "scriptgeneral.h"

#include "libxml/parser.h"
#include "libxml/xpath.h"

// DHM - this static string buffer is used all over the place.  Could do with cleaning up.
static char pathret[MAX_PATH];

// NaturalMotion XML ragdoll dictionary (saves loading every fragment like we used to do).
// Which took ~500-1000ms per fragment.
static rage::atMap<rage::atString, rage::atString> g_RagdollDictionary;

const char* RBGetParam(const char* param)
{
	const char* result = rage::sysParam::GetByName( param, NULL );

	if( result )
		return result;
	else
	{
		// an extra parameter - here we operate in a restricted way
		// we search through all the arguments until we hit one of the form
		// -<name> <argument>, otherwise it ain't there
		char dashParam[256] = "-";
		strcat( dashParam, param );
		for(int i = 0; i < sysParam::GetArgCount(); ++i )
		{
			if(stricmp(dashParam, sysParam::GetArg(i)) == 0)
			{
				if( ( i + 1 ) < sysParam::GetArgCount() )
					return sysParam::GetArg(i+1);
			}
		}
	}

	return NULL;
}

bool RBWait()
{
	_getch();
	return true;
}

void RBSleep(int ms)
{
	sysIpcSleep( ms );
}

bool RBSetLogLevel(int loglevel)
{
	LogListener::SetLogLevel(loglevel);
	return true;
}

bool RBSetStopOnError(bool stoponerror)
{
	LogListener::SetStopOnError(stoponerror);
	return true;
}

bool RBReportError(const char* msg)
{
	Errorf(msg);
	return true;
}

bool RBReportQuit(const char* msg)
{
	Quitf(msg);
	return true;
}

bool RBReportWarning(const char* msg)
{
	Warningf(msg);
	return true;
}

bool RBReportDisplay(const char* msg)
{
	rbDisplayf(msg);
	return true;
}

bool RBReportTrace1(const char* msg)
{
	rbTracef1(msg);
	return true;
}

bool RBReportTrace2(const char* msg)
{
	rbTracef2(msg);
	return true;
}

bool RBReportTrace3(const char* msg)
{
	rbTracef3(msg);
	return true;
}

const char* RBGetBuildLogFilename()
{
	return LogListener::GetBuildLogFilename();
}

const char* RBGetErrorLogFilename()
{
	return LogListener::GetErrorLogFilename();
}

bool RBDelFile(const char* filename)
{
	const fiDevice& device = fiDeviceLocal::GetInstance();

	rbTracef2("deleting %s", filename);
	if(device.Delete(filename))
		return true;

	return false;
}

bool RBDelDir(const char* directory, bool contents)
{
	if (contents)
		rbTracef2("deleting %s with contents", directory);
	else
		rbTracef2("deleting %s", directory);

	if (fiDeviceLocal::DeleteDirectory(directory, contents))
		return true;

	return false;
}

static char* gp_temp = NULL;

const char* StringListToString(string_list& filelist)
{
	s32 size = 0;

	for (string_list::iterator it = filelist.begin(); it != filelist.end(); ++it)
	{
		size += strlen((*it).c_str()) + 1;
	}

	size += 1;
	gp_temp = new char[size];
	gp_temp[0] = '\0';

	size = 0;

	for (string_list::iterator it = filelist.begin(); it != filelist.end(); ++it)
	{
		strcpy(&(gp_temp[size]),(*it).c_str());
		strcat(gp_temp,",");
		size += strlen((*it).c_str()) + 1;
	}

	return gp_temp;
}

const char* RBFindFiles(const char* path, bool relative, bool sorted)
{
	if(gp_temp)
		delete gp_temp;

	string_list filelist;

	AddFilesToList( path, filelist, relative, sorted );

	return StringListToString(filelist);
}

const char* RBFindPlatformFiles(const char* path, bool relative, bool sorted)
{
	if(gp_temp)
		delete gp_temp;

	string_list filelist;

	AddPlatformFilesToList( path, filelist, relative, sorted );

	return StringListToString(filelist);
}

const char* RBFindDirs(const char* path)
{
	if(gp_temp)
		delete gp_temp;

	string_list dirlist;

	AddSubDirToList( path, dirlist );

	return StringListToString(dirlist);
}

const char* RBRemoveExtensionFromPath(const char* pathin)
{
	ASSET.RemoveExtensionFromPath( pathret, MAX_PATH, pathin);
	return pathret;
}

const char* RBGetExtensionFromPath(const char* path)
{
	return ASSET.FindExtensionInPath( path );
}

const char* RBGetFilenameFromPath(const char* path)
{
	return ASSET.FileName( path );
}

const char* RBRemoveNameFromPath(const char* pathin)
{
	ASSET.RemoveNameFromPath( pathret, MAX_PATH, pathin );
	return pathret;
}

bool RBCreateLeadingPath(const char* path)
{
	if( ASSET.CreateLeadingPath( path ) == false )
		return false;

	return true;
}

bool RBCopy_File(const char* src,const char* dest)
{
	const fiDevice* pDevice = fiDevice::GetDevice(src);
	if (pDevice == &fiDeviceLocal::GetInstance())
	{
		if (CopyFile(src, dest, false) == false)
		{
			Errorf("copy_file: failed to copy file %s to %s (%d)", src, dest, GetLastError());
			return false;
		}
	}
	else
	{
		if (CopyFileFunnel(src, dest) == false)
		{
			Errorf("copy_file: failed to funnel copy file %s to %s (%d)", src, dest, GetLastError());
			return false;
		}
	}

	rbTracef2( "Copied %s to %s", src, dest );
	return true;
}

bool RBMove_File(const char* src,const char* dest)
{
	if (MoveFileEx(src, dest, MOVEFILE_REPLACE_EXISTING) == false)
	{
		Errorf("move_file: failed to open move file %s to %s (%d)", src, dest, GetLastError());
		return false;
	}

	rbTracef1( "Moved %s to %s\n", src, dest );
	return true;
}

bool RBFileExists(const char* filename)
{
	return ASSET.Exists( filename, "" );
}

bool RBDirectoryExists(const char* dir)
{
	return DirExists( dir );
}

int RBFileTimeCompare(const char* file1,const char* file2)
{
	u64 u64time1 = Get_FileTime( file1 );
	u64 u64time2 = Get_FileTime( file2 );

	if( u64time1 > u64time2 )
		return -1;
	else if ( u64time1 < u64time2 )
		return 1;

	return 0;
}

unsigned int RBFileSize(const char* filename)
{
	return (unsigned int)Get_FileSize( filename );
}

unsigned int RBResourceSize(const char* filename)
{
	const fiDevice* pDevice = fiDevice::GetDevice( filename );
	if ( !pDevice )
		return (0);

	datResourceFileHeader hdr;
	fiHandle hFile = pDevice->Open( filename, true );
	if ( fiHandleInvalid == hFile )
		return (0);

	int bytesRead = pDevice->Read( hFile, &hdr, sizeof( datResourceFileHeader ) );
	Assertf( bytesRead == sizeof( datResourceFileHeader ), 
		"Error reading datResourceFileHeader from %s", filename);
	pDevice->Close( hFile );
	if ( bytesRead != sizeof( datResourceFileHeader ) )
		return ( 0 );
	return hdr.IsValidResource()? hdr.Info.GetVirtualSize() + hdr.Info.GetPhysicalSize() : 0;
}

unsigned int RBResourcePhysicalSize(const char* filename)
{
	const fiDevice* pDevice = fiDevice::GetDevice( filename );
	if ( !pDevice )
		return (0);

	datResourceFileHeader hdr;
	fiHandle hFile = pDevice->Open( filename, true );
	if ( fiHandleInvalid == hFile )
		return (0);

	int bytesRead = pDevice->Read( hFile, &hdr, sizeof( datResourceFileHeader ) );
	Assertf( bytesRead == sizeof( datResourceFileHeader ), 
		"Error reading datResourceFileHeader from %s", filename);
	pDevice->Close( hFile );
	if ( bytesRead != sizeof( datResourceFileHeader ) )
		return ( 0 );
	
	return hdr.IsValidResource()? hdr.Info.GetPhysicalSize() : 0;
}

unsigned int RBResourceVirtualSize(const char* filename)
{
	const fiDevice* pDevice = fiDevice::GetDevice( filename );
	if ( !pDevice )
		return (0);

	datResourceFileHeader hdr;
	fiHandle hFile = pDevice->Open( filename, true );
	if ( fiHandleInvalid == hFile )
		return (0);

	int bytesRead = pDevice->Read( hFile, &hdr, sizeof( datResourceFileHeader ) );
	Assertf( bytesRead == sizeof( datResourceFileHeader ), 
		"Error reading datResourceFileHeader from %s", filename);
	pDevice->Close( hFile );
	if ( bytesRead != sizeof( datResourceFileHeader ) )
		return ( 0 );

	return hdr.IsValidResource()? hdr.Info.GetVirtualSize() : 0;
}

bool RBSetFileAttrib(const char* filename,const char* attribute,bool val)
{
	DWORD attributes = GetFileAttributes(filename);

	if (attributes == INVALID_FILE_ATTRIBUTES)
	{
		// Only error if we're trying to *set* the readonly bit.  If we're trying to clear
		// the bit then it's probably because we were about to copy over it anyway.
		if (val)
			Errorf("Unable to get the file attributes for %s (%d)", filename, GetLastError());
		return false;
	}

	DWORD newAttributes = attributes;

	if (strcmp(attribute, "readonly") == 0)
	{
		if (val)
			newAttributes |= FILE_ATTRIBUTE_READONLY;
		else
			newAttributes &= ~FILE_ATTRIBUTE_READONLY;
	}
	else
	{
		Errorf("Unknown attribute %s in set_file_attrib", attribute );
		return false;
	}

	// .. add more attributes when needed

	if (attributes != newAttributes && (SetFileAttributes(filename, newAttributes) == false))
	{
		Errorf("Failed to set file '%s' attribute '%s' to '%s' (%d)", filename, attribute, val?"true":"false", GetLastError());
		return false;
	}

	return true;
}

bool RBGetFileAttrib(const char* filename,const char* attribute)
{
	DWORD attributes = GetFileAttributes(filename);

	if (stricmp(attribute, "readonly") == 0)
	{
		return ((attributes & FILE_ATTRIBUTE_READONLY) == FILE_ATTRIBUTE_READONLY);
	}

	Errorf("Unknown attribute %s in get_file_attrib", attribute );
	return false;
}

bool RBCloneTimestamp(const char* source,const char* destination)
{
	FILETIME creationTime;
	FILETIME lastAccessTime;
	FILETIME lastWriteTime;

	{
		HANDLE hFile = CreateFile(source, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_ARCHIVE, 0);
		if (hFile == INVALID_HANDLE_VALUE)
		{
			Errorf("clone_timestamp: failed to open source file %s (%d)", source, GetLastError());
			return false;
		}
		if (GetFileTime(hFile, &creationTime, &lastAccessTime, &lastWriteTime) == 0)
		{
			Errorf("clone_timestamp: failed to get file time for source file %s (%d)", source, GetLastError());
			return false;
		}

		CloseHandle(hFile);
	}

	{
		HANDLE hFile = CreateFile(destination, GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_ARCHIVE, 0);
		if (hFile == INVALID_HANDLE_VALUE)
		{
			Errorf("clone_timestamp: failed to open destination file %s (%d)", destination, GetLastError());
			return false;
		}
		if (SetFileTime(hFile, &creationTime, &lastAccessTime, &lastWriteTime) == 0)
		{
			Errorf("clone_timestamp: failed to set file time for destination file %s (%d)", destination, GetLastError());
			return false;
		}
		CloseHandle(hFile);
	}

	rbTracef2("Cloned timestamp from %s to %s", source, destination);
	return true;
}

atArray<sysPerformanceTimer*> g_listTimers;

bool RBPushTimer()
{
	sysPerformanceTimer* pTimer = new sysPerformanceTimer("");

	g_listTimers.PushAndGrow(pTimer);
	pTimer->Start();

	return true;
}

bool RBPopTimer(const char* msg)
{
	sysPerformanceTimer* pTimer = g_listTimers.Pop();
	pTimer->Stop();
	double diff = pTimer->GetTimeMS() / 1000.0f;
	delete pTimer;

	Displayf("%s %.2f sec",msg,diff);

	if(g_listTimers.GetCount() == 0)
		g_listTimers.Reset();

	return true;
}

// DHM - we should use the Environment class for these.
void EnvReplace( char* subst, size_t size, const char* replace, const char* replaceWith )
{
	std::string io( subst );
	boost::replace_all( io, replace, replaceWith );

	strcpy_s( subst, size, io.c_str() );
}

void LoadNMXMLData( const char* rawxmlfilename )
{
	char projRoot[MAX_PATH] = {0};
	char toolsRoot[MAX_PATH] = {0};
	const char* assetsDir = RageBuilder::GetPlatformImpl()->GetAssetsPath( );
	const char* coreAssetsDir = RageBuilder::GetPlatformImpl()->GetCoreAssetsPath( );
	const char* buildDir = RageBuilder::GetPlatformImpl()->GetBuildPath( );
	char toolsRootReplace[] = "$(toolsroot)";
	char projRootReplace[] = "$(root)";
	char coreAssetsReplace[] = "$(core_assets)";
	char assetsReplace[] = "$(assets)"; 
	char buildReplace[] = "$(build)";
	char xmlfilename[MAX_PATH];

	RageBuilder::GetConfig().GetToolsRootDir( toolsRoot, _MAX_PATH );
	RageBuilder::GetConfig().GetProjectRootDir( projRoot, _MAX_PATH );
	strcpy_s( xmlfilename, MAX_PATH, rawxmlfilename );
	EnvReplace( xmlfilename, MAX_PATH, toolsRootReplace, toolsRoot );
	EnvReplace( xmlfilename, MAX_PATH, assetsReplace, assetsDir );
	EnvReplace( xmlfilename, MAX_PATH, coreAssetsReplace, coreAssetsDir );
	EnvReplace( xmlfilename, MAX_PATH, buildReplace, buildDir );

	xmlInitParser( );
	xmlDocPtr pXmlDoc = xmlParseFile( xmlfilename );
	if ( !pXmlDoc )
	{
		Errorf( "Failed to load NM XML file: %s.", xmlfilename );
		xmlCleanupParser( );
		return;
	}

	xmlXPathContextPtr m_pXPathCtx = xmlXPathNewContext( pXmlDoc );	
	xmlXPathObjectPtr pXPathObj = xmlXPathEvalExpression( BAD_CAST "//xmlmatchs/xmlmatch", m_pXPathCtx );

	if( pXPathObj->type == XPATH_NODESET )
	{
		size_t count = pXPathObj->nodesetval->nodeNr;
		for ( size_t n = 0; n < count; ++n )
		{
			xmlNodePtr pNode = pXPathObj->nodesetval->nodeTab[n];

			char* source = (char*)xmlGetProp(pNode, BAD_CAST "source");
			char* destination = (char*)xmlGetProp(pNode, BAD_CAST "destination");
			
			// Lowercase source.
			rage::atString src( source );
			src.Lowercase( );

			// Lowercase destination.
			char dest[MAX_PATH] = {0};
			strcpy_s( dest, MAX_PATH, destination );
			EnvReplace( dest, MAX_PATH, projRootReplace, projRoot);
			EnvReplace( dest, MAX_PATH, assetsReplace, assetsDir );
			EnvReplace( dest, MAX_PATH, coreAssetsReplace, coreAssetsDir );
			EnvReplace( dest, MAX_PATH, buildReplace, buildDir );
			rage::atString dst( dest );
			dst.Lowercase( );

			g_RagdollDictionary.Insert( src, dst );
		}
	}

	xmlFreeDoc( pXmlDoc );
	xmlCleanupParser( );
}

// DHM - 
//  1) this should be updated to use the Environment class.
void RBFindXMLMatch( const char* src, const char* rawxmlfilename, char* dst, size_t size )
{
	// We load the ragdoll XML data once.  This previously had considerable cost per-fragment.
	if ( 0 == g_RagdollDictionary.GetNumUsed() )
		LoadNMXMLData( rawxmlfilename );

	char projRoot[MAX_PATH] = {0};
	char toolsRoot[MAX_PATH] = {0};
	const char* assetsDir = RageBuilder::GetPlatformImpl()->GetAssetsPath( );
	const char* coreAssetsDir = RageBuilder::GetPlatformImpl()->GetCoreAssetsPath( );
	const char* buildDir = RageBuilder::GetPlatformImpl()->GetBuildPath( );
	char toolsRootReplace[] = "$(toolsroot)";
	char projRootReplace[] = "$(root)";
	char coreAssetsReplace[] = "$(core_assets)";
	char assetsReplace[] = "$(assets)"; 
	char buildReplace[] = "$(build)";

	rage::atString source( src );
	source.Lowercase( );

	// DHM; would like to change this to simply index key into the atMap
	// but for some reason the source strings have a '/' prefix and '.' postfix
	// rather than matching the asset basename (saving dupe entries??).
	for (rage::atMap<rage::atString, rage::atString>::Iterator it = g_RagdollDictionary.CreateIterator();
		 !it.AtEnd(); 
		 it.Next())
	{
		rage::atString& key = it.GetKey( );
		rage::atString& data = it.GetData( );
		
		if ( source.IndexOf( key ) > -1 )
		{
			strcpy_s( dst, size, data.c_str() );
			EnvReplace( dst, size, projRootReplace, projRoot);
			EnvReplace( dst, size, assetsReplace, assetsDir );
			EnvReplace( dst, size, coreAssetsReplace, coreAssetsDir );
			EnvReplace( dst, size, buildReplace, buildDir );
			break;
		}
	}
}

const char* RBGetVersion()
{
	// DHM - needs fixed.
	sprintf(pathret,"%d.%d",RAGE_RELEASE,BUILD_VERSION);
	return pathret;
}
