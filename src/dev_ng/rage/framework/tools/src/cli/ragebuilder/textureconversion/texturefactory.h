//
// File:: ragebuilder/textureconversion/texturefactory.h
// Description:: Ragebuilder's texture factory, to save adding lots of 
//               RESOURCECOMPILER nonsense to grcore's texture factory.
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 4 May 2011
//

#ifndef RAGEBUILDER_TEXTUREFACTORY_H
#define RAGEBUILDER_TEXTUREFACTORY_H

// RAGE headers
#include "grcore/texture.h"

// STL headers
#include <map>

/*
PURPOSE
*/
typedef ::std::map<int, ::rage::grcTexture*> TextureList;

// rage forward-declarations
namespace rage
{
	class CTextureConversionSpecification;
}

/*
PURPOSE
	TextureFactory is the class factory for textures in Ragebuilder.
<FLAG Component>
*/
class TextureFactory
{
public:
	/*  PURPOSE
			Enumeration to define which textures belong to which dictionary.
	*/
	enum eTextureDictID
	{
		TEXTURE_DICT_ID_NONE			= 0x0000000, //None
		TEXTURE_DICT_ID_DEFAULT			= 0x0000001, //Default 
		TEXTURE_DICT_ID_HIGH			= 0x0000002, //High
	};

	/*	PURPOSE
			Enumeration to define the different texture types that can be 
			returned in the multiple output map */
	enum eTextureID
	{
		TEXTURE_ID_NONE					= 0x00000000, // None
		TEXTURE_ID_MIP_CHAIN_DEFAULT	= 0x00000001, // Default mip chain (default as before, but may exclude top mip)
		TEXTURE_ID_MIP_CHAIN_HIGH		= 0x00000002, // Texture including the top-most mip (for m_imageSplitHD support).
		TEXTURE_ID_CHANNEL_012			= 0x00000003, // Channel map for terrain processing -- blending the 0, 1 and 2 channels.
		TEXTURE_ID_CHANNEL_345			= 0x00000004, // Channel map for terrain processing -- blending the 3, 4 and 5 channels.
		TEXTURE_ID_ALL					= 0xFFFFFFFF  // All
	};	

	/*	PURPOSE
			Create texture(s) based on a named file (for resource compilers only)
		PARAMS
			filename - name of the file to load
			outputs - map of output grcTexture objects
		RETURNS
			true iff successful; false otherwise */
	static bool Create( const char* filename, 
						int texture_id, 
						TextureList& outputs, 
						grcTextureFactory::TextureCreateParams* params = NULL );
};

#endif // RAGEBUILDER_TEXTUREFACTORY_H
