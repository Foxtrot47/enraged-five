//
// luarage.h
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#ifndef INC_LUA_RAGE_H_
#define INC_LUA_RAGE_H_

#include "luacore.h"

int InitRage(lua_State* L);
int	ShutdownRage(lua_State* L);

int SetPlatform(lua_State* L);
int GetPlatform(lua_State* L);
int GetPlatformExtension(lua_State* L);
int ConvertFilenameToPlatform(lua_State* L);

int StartBuild(lua_State* L);
int SetRootDirectory(lua_State* L);

int SetAutoTexDict(lua_State* L);
int RegisterTextureName(lua_State* L);
int LoadMaterialDefs(lua_State* L);
int LoadProceduralDefs(lua_State* L);
int SetEffectPath(lua_State* L);
int SetShaderPath(lua_State* L);
int SetShaderDbPath(lua_State* L);
int SetBuildPath(lua_State* L);
int SetAssetsPath(lua_State* L);
int SetCoreAssetsPath(lua_State* L);
int SetMetadataDefinitionsPath(lua_State* L);
int LoadTextureTemplates(lua_State* L);

// dictionaries
int LoadTextures(lua_State* L);
int LoadAnims(lua_State* L);
int LoadClips(lua_State* L);
int LoadParamMotions(lua_State* L);
int LoadExpressionsList(lua_State* L);
int LoadModels(lua_State* L);
int LoadBounds(lua_State* L);
int LoadPathRegion(lua_State* L);
int LoadNavMesh(lua_State* L);
int LoadHeightMesh(lua_State* L);
int LoadHierarchicalNav(lua_State* L);
int LoadWaypointRecording(lua_State* L);
int LoadEffect(lua_State* L);
int LoadVehicleRecording(lua_State* L);
int LoadAudMesh(lua_State* L);
int LoadMeta(lua_State* L);
int LoadScript( lua_State* L );
int LoadOccMeshes(lua_State* L);
int LoadFrameFilterList(lua_State* L);

int SaveTextureDictionary(lua_State* L);
int SaveTextureDictionaryHiMip(lua_State* L);
int SaveClipDictionary(lua_State* L);
int SaveParamMotionDictionary(lua_State* L);
int SaveExpressionsDictionary(lua_State* L);
int SaveModelDictionary(lua_State* L);
int SaveClothDictionary(lua_State* L);
int SaveBlendShapeDictionary(lua_State* L);
int SaveBoundDictionary(lua_State* L);
int SavePhysicsDictionary(lua_State* L);
int SaveFrameFilterDictionary(lua_State* L);

// objects
int LoadTexture(lua_State* L);
int LoadAnim(lua_State* L);
int LoadClip(lua_State* L);
int LoadParamMotion(lua_State* L);
int LoadExpressions(lua_State* L);
int LoadModel(lua_State* L);
int LoadBlendShape(lua_State* L);
int LoadFragment(lua_State* L);
int LoadNMData(lua_State* L);
int LoadBound(lua_State* L);
int LoadFrameFilter(lua_State* L);

int SaveTexture(lua_State* L);
int SaveModel(lua_State* L);
int SaveBlendShape(lua_State* L);
int SaveFragment(lua_State* L);
int SaveBound(lua_State* L);
int SavePathRegion(lua_State* L);
int SaveNavMesh(lua_State* L);
int SaveHeightMesh(lua_State* L);
int SaveHierarchicalNav(lua_State* L);
int SaveWaypointRecording(lua_State* L);
int SaveAnim(lua_State* L);
int SaveEffect(lua_State* L);
int SaveVehicleRecording(lua_State* L);
int SaveAudMesh(lua_State* L);
int SaveRbf(lua_State* L);
int SavePso(lua_State* L);
int SavePsoAs(lua_State* L);
int SaveMapData(lua_State* L);
int SaveMapTypes(lua_State* L);
int SaveCutfile(lua_State* L);
int SaveImf(lua_State* L);
int SaveScript( lua_State* L );
int SaveOccImap( lua_State* L );
int SaveOccXml( lua_State* L );
int SaveTextDatabase( lua_State* L );

int IsPsoInPlaceLoadable( lua_State* L);
int PsoDebugStringSize( lua_State* L );

int SetMipMapInterleaving(lua_State* L);
int SetMaxMipMapCount(lua_State* L);
int SetTextureMin(lua_State* L);
int SetTextureMax(lua_State* L);
int SetTextureScale(lua_State* L);
int SetCompressResources(lua_State* L);
int SetTriStripping(lua_State* L);
int SetLimitExemptTextures(lua_State* L);
int SetOverloadTextureSrc(lua_State* L);

int PushBuildVars(lua_State* L);
int PopBuildVars(lua_State* L);

int SetTuningFolder(lua_State* L);

int LoadTextureList(lua_State* L);

int DoesTexHaveMips(lua_State* L);
int PostProcessVehicle(lua_State* L);
int HeadBlendGeometry(lua_State* L);
int HeadBlendWriteBuffer(lua_State* L);
int MicroMorphMesh(lua_State* L);
int EnableFragMatrixSet(lua_State* L);
int PPUOnlyHint(lua_State* L);
int LodSkeleton(lua_State* L);

int GetSettings(lua_State* L);
int SetSettings(lua_State* L);
int CompressAnim(lua_State* L);
int GetClipPropertyString(lua_State* L);
int GetClipPropertyInt(lua_State* L);
int GetClipPropertyFloat(lua_State* L);
int SetClipPropertyString(lua_State* L);
int ConvertAndSaveDDS(lua_State* L);
int ValidateClip(lua_State* L);
int SetDwdType(lua_State* L);
int GenerateDrawableStats(lua_State* L);
int GenerateFragmentStats(lua_State* L);

int SetTextureDictionarySaveHi( lua_State* L );
int GetTextureDictionarySaveHi( lua_State* L );

int SetEdgify(lua_State* L);

#endif // INC_LUA_RAGE_H_
