#include "gtamaterialmgr.h"
#include "atl/map.h"
#include "parser/tree.h"
#include "parser/manager.h"
#include "parser/macros.h"

using namespace rage;

#define STRING_LENGTH 256
static phMaterial g_RetMat;


void GtaMaterial::LoadGtaMtlData(char* pLine, float version)
{	
	// vars to read data into
	char materialName[64];
	char filterName[64];
	char vfxGroupName[64];
	char vfxDisturbanceTypeName[64];
	char reactWeaponTypeName[64];
	char rumbleProfileName[64] = {0};
	float rageFriction, rageElasticity;
	float density, typeGrip, wetMult, tyreDrag, topSpeedMult;
	float softness, noisiness;
	float flammability, burnTime, burnStrength;
	float penetrationResistance;

	// vars to read flag data into
	s32 isSeeThrough, isShootThrough, isShootThroughFx, isNoDecal, isPorous, heatsTyre;

	if (version>=12.0f)
	{
		// NOTE: This must line up with /framework/tools/src/cli/ragebuilder/gtamaterialmgr.cpp GtaMaterial::LoadGtaMtlData
		sscanf(pLine, "%s %s   %s %s %s %s   %f %f    %f %f %f %f %f   %f %f    %f    %d %d %d %d %d %d",
			materialName,		filterName,
			vfxGroupName,		vfxDisturbanceTypeName,	rumbleProfileName,		reactWeaponTypeName,
			&rageFriction,		&rageElasticity,
			&density,			&typeGrip,				&wetMult,				&tyreDrag,				&topSpeedMult,
			&softness,			&noisiness,
			&penetrationResistance,
			&isSeeThrough,		&isShootThrough,		&isShootThroughFx,		&isNoDecal,				&isPorous,		&heatsTyre);
	}
	else
	{
		// NOTE: This must line up with /framework/tools/src/cli/ragebuilder/gtamaterialmgr.cpp GtaMaterial::LoadGtaMtlData
		sscanf(pLine, "%s %s   %s %s %s %s   %f %f    %f %f %f %f %f   %f %f    %f %f %f    %f    %d %d %d %d %d %d",
			materialName,		filterName,
			vfxGroupName,		vfxDisturbanceTypeName,	rumbleProfileName,		reactWeaponTypeName,
			&rageFriction,		&rageElasticity,
			&density,			&typeGrip,				&wetMult,				&tyreDrag,				&topSpeedMult,
			&softness,			&noisiness,
			&flammability,		&burnTime,				&burnStrength,
			&penetrationResistance,
			&isSeeThrough,		&isShootThrough,		&isShootThroughFx,		&isNoDecal,				&isPorous,		&heatsTyre);
	}

	m_IsSeeThrough = (isSeeThrough != 0);
	m_IsShootThrough = (isShootThrough != 0);
	m_IsShootThroughFx = (isShootThroughFx != 0);
}

GtaMaterialMgr::~GtaMaterialMgr()
{

}

void GtaMaterialMgr::Create()
{
	GtaMaterialMgr* instance = new GtaMaterialMgr;

	instance->AllocateMaterial("DEFAULT");
	instance->AllocateProcedural("DEFAULT", 0);

	SetInstance(instance);
}

void GtaMaterialMgr::Destroy()
{
	delete this;
}

int GtaMaterialMgr::GetNumMaterials() const
{
	return m_MaterialList.GetCount();
}

const phMaterial& GtaMaterialMgr::GetMaterialImpl(Id id) const
{
	return g_RetMat;
}

const phMaterial& GtaMaterialMgr::FindMaterial(const char* name) const
{
	return GetMaterialImpl(FindMaterialIdImpl(name));
}

phMaterialMgr::Id GtaMaterialMgr::FindMaterialIdImpl(const char* name) const
{
//	ASSERT(strlen(name) < STRING_LENGTH);
	char uprname[STRING_LENGTH];
	strcpy(uprname,name);
	strupr(uprname);

	name = uprname;

	enum
	{
		IDSTATE_MTL,
		IDSTATE_PROC,
		IDSTATE_ROOMID,
		IDSTATE_POPMULT,
		IDSTATE_FLAGS,
#if PH_MATERIAL_ID_64BIT
		IDSTATE_COLOUR_ID,
#endif // PH_MATERIAL_ID_64BIT
		IDSTATE_END,
	};

	//the data we want to pull out

	char mtlName[STRING_LENGTH];
	char procName[STRING_LENGTH];
	int32 roomID = 0;
	int32 popMult = 0;
	int32 flags = 0;
#if PH_MATERIAL_ID_64BIT
	int32 colId = 0;
#endif // PH_MATERIAL_ID_64BIT

	mtlName[0] = '\0';
	procName[0] = '\0';

	//

	char buffer[STRING_LENGTH];
	int32 state = IDSTATE_MTL;
	char* p_write = buffer;

	buffer[0] = '\0';

	while(true)
	{
		if(*name == '\0' || *name == '|')
		{
			*p_write = '\0';

			switch(state)
			{
			case IDSTATE_MTL:
				strcpy(mtlName,buffer);
				state = IDSTATE_PROC;
				break;
			case IDSTATE_PROC:
				strcpy(procName,buffer);
				state = IDSTATE_ROOMID;
				break;
			case IDSTATE_ROOMID:
				roomID = atoi(buffer);
				state = IDSTATE_POPMULT;
				break;
			case IDSTATE_POPMULT:
				popMult = atoi(buffer);

				if(popMult < 0)
					popMult = 0;

				state = IDSTATE_FLAGS;
				break;
			case IDSTATE_FLAGS:
				flags = atoi(buffer);
#if PH_MATERIAL_ID_64BIT
				state = IDSTATE_COLOUR_ID;
#else
				state = IDSTATE_END;
#endif // PH_MATERIAL_ID_64BIT
				break;
#if PH_MATERIAL_ID_64BIT
			case IDSTATE_COLOUR_ID:
				colId = atoi(buffer);
				state = IDSTATE_END;
				break;
#endif // PH_MATERIAL_ID_64BIT
			}

			buffer[0] = '\0';
			p_write = buffer;

			if((*name == '\0') || (state == IDSTATE_END))
				break;
		}
		else
		{
			*p_write++ = *name;
		}

		name++;
	}
	
	//construct the id
#if PH_MATERIAL_ID_64BIT
	int64 retID = 0;

	const int32* p_Data = m_Material2ID.Access(mtlName);
	if(p_Data)
		retID |= int64(*p_Data) & 255;

	p_Data = m_Procedural2ID.Access(procName);
	if(p_Data)
		retID |= int64((*p_Data)& 255) << 8;
	retID |= int64(roomID & 31) << 16;
	retID |= int64(popMult& 7)	<< 21;
	retID |= int64(flags)		<< 24;
	retID |= int64(colId)		<< 40;
#else
	int32 retID = 0;

	const int32* p_Data = m_Material2ID.Access(mtlName);
	if(p_Data)
		retID |= (*p_Data) & 255;

	p_Data = m_Procedural2ID.Access(procName);
	if(p_Data)
		retID |= ((*p_Data) & 255) << 8;

	retID |= (roomID & 31) << 16;
	retID |= (popMult & 7) << 21;
	retID |= flags << 24;
#endif // PH_MATERIAL_ID_64BIT

	if(const int32* p_Data = m_Material2ID.Access(mtlName))
	{
		// Pack in polyflags that are always on certain materials
		const GtaMaterial& material = m_MaterialList[*p_Data];
		if(material.m_IsSeeThrough)
		{
			retID |= GetPackedPolyFlagValue(POLYFLAG_SEE_THROUGH);
		}

		if(material.m_IsShootThroughFx)
		{
			// The ShootThroughFx polyflag is mutually exclusive with the shoot through flag
			retID &= ~GetPackedPolyFlagValue(POLYFLAG_SHOOT_THROUGH);
			retID |= GetPackedPolyFlagValue(POLYFLAG_SHOOT_THROUGH_FX);
		}
		else if(material.m_IsShootThrough)
		{
			retID |= GetPackedPolyFlagValue(POLYFLAG_SHOOT_THROUGH);
		}
	}

	return retID;
}

phMaterialMgr::Id GtaMaterialMgr::GetMaterialId(const phMaterial& material) const
{
	return FindMaterialId(material.GetName());
}

const phMaterial& GtaMaterialMgr::GetDefaultMaterial() const
{
	return g_RetMat;
}

const char* GtaMaterialMgr::GetMaterialNameFromIndex(int index) const
{
	if (AssertVerify(index >= 0 && index < m_MaterialList.GetCount()))
	{
		return m_MaterialList[index].m_Name.c_str();
	}
	else
	{
		return "";
	}
}

phMaterial& GtaMaterialMgr::AllocateMaterial(const char* name)
{
//	ASSERT(strlen(name) < STRING_LENGTH);
	char uprname[STRING_LENGTH];
	strcpy(uprname,name);
	strupr(uprname);

	int32 CurrentID = m_MaterialList.GetCount();
	atString setname(uprname);

	m_MaterialList.PushAndGrow(GtaMaterial(setname));
	m_Material2ID.Insert(setname,CurrentID);

	return m_MaterialList.back();
}

void GtaMaterialMgr::AllocateProcedural(const char* name, int index)
{
//	ASSERT(strlen(name) < STRING_LENGTH);
	char uprname[STRING_LENGTH];
	strcpy(uprname,name);
	strupr(uprname);

	int* p_Data = m_Procedural2ID.Access(uprname);
	if(p_Data)
		return;

	atString setname(uprname);

	m_ProceduralList.PushAndGrow(setname);
	m_Procedural2ID.Insert(setname,index);
}

void GtaMaterialMgr::LoadMaterials(const char* p_filename)
{
	fiStream* p_File = fiStream::Open(p_filename);

	if( !p_File )
	{
		Errorf( "Could not open %s as a materials file", p_filename );
		return;
	}

	char LineData[4092];
	static bool First = true;

	fgetline(LineData,4092,p_File);

	float version = 1.0f;
	sscanf(LineData, "%f", &version);

	while(p_File->Tell() != p_File->Size())
	{
		fgetline(LineData,4092,p_File);

		if((LineData[0] == '\0') || (LineData[0] == '#'))
		{
			continue;
		}

		if(First)
		{
			First = false;
		}
		else
		{
			char NameData[128];
			sscanf(LineData, "%s", NameData);
			strlwr(NameData);
			GtaMaterial& material = static_cast<GtaMaterial&>(AllocateMaterial(NameData));
			material.LoadGtaMtlData(LineData, version);
		}
	}

	p_File->Close();
}

void GtaMaterialMgr::LoadProcedurals( const char* pFilename )
{
	const int plantInfoOffset = 128;

	fiStream* pFile = fiStream::Open( pFilename );
	if( !pFile )
	{
		Errorf( "Could not open procedurals file: %s.", pFilename );
		return;
	}

	// parCodeGen Tree Structure; we don't have the structure defined as that
	// is currently in game code and the members we require are well defined.
	parTree* pTree = PARSER.LoadTree( pFile );
	if ( !pTree )
	{
		Errorf( "Could not parse procedural file: %s.", pFilename );
		return;
	}
	parTreeNode* pNode = pTree->GetRoot();
	parTreeNode* pProcTagTable = pNode->FindChildWithName( "procTagTable" );
	if ( !pProcTagTable )
	{
		Errorf( "Failed to find procTagTable in procedural file: %s.", pFilename );
		return;
	}

	int index = 0;
	for ( parTreeNode::ChildNodeIterator itProcTag = pProcTagTable->BeginChildren();
		  itProcTag != pProcTagTable->EndChildren();
		  ++itProcTag )
	{
		parTreeNode* pProcTag = *itProcTag;
		parTreeNode* pProcName = pProcTag->FindChildWithName( "name" );
		if ( pProcName && pProcName->GetData())
		{
			char* pName = pProcName->GetData( );
			strupr( pName );
			//Displayf( "[procedural] Allocate Procedural [%d]: '%s'.", index, pName );
			AllocateProcedural( pName, index );
		}
		index++;
	}

	pFile->Close();
}
