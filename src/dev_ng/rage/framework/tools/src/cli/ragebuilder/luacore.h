//
// Cpp wrapper class for Lua, with some support functions for writing
// Lua commands.
//
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
//

#if !defined(LUA_CORE_H_)
#define LUA_CORE_H_

// lua includes
extern "C" 
{
# include "lua.h"
}

// lua command prototype
typedef int (LuaCommandFn) (lua_State *L);

// lua support class
class CLuaCore
{
public:
				CLuaCore();
				CLuaCore(lua_State* state, bool bOwns = false);
				~CLuaCore();

	void		OpenStdLibs();
	void		RegisterCommand(const char* name, LuaCommandFn* function);
	int			ExecuteFile(const char* name);
	int			ExecuteText(const char* text, const char* name = "");
private:
	int			DoCall(int status);
	int			LCall(int narg, int clear);
	void		LAction(int i);
	void		LStop(lua_State *l, lua_Debug *ar);
	int			Report(int status);
	void		LMessage(const char* pname, const char* msg);

	lua_State*	m_L;
	bool		m_bOwns;
};

// utility functions
void PushError(lua_State *L, const char* pCommand, const char* pError);
void GetArguments(lua_State *L, int number, const char* pCommand);
int  GetArgumentsRange(lua_State *L, int minimum, int maximum, const char* pCommand);
void CheckArgument(lua_State *L, unsigned int index, int (*lua_istype) (lua_State *L, int idx), const char* pCommand);

#define LUA_RETURN(rt)			{ lua_pushnumber(L, rt); return 1;}
#define LUA_RETURN_SUCCESS()	{ lua_pushnumber(L, 1); return 1;}
#define LUA_RETURN_FAILURE()	{ lua_pushnumber(L, 0); return 1;}

#endif // LUACORE_H_