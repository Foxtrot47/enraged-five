
// Local headers
#include "texentry.h"
#include "textureconversion.h"

// RAGE headers
#include "file/asset.h"
#include "file/device.h"

namespace rage
{

TexEntry::TexEntry( )
	: name(NULL), 
	fileSize( 0 ), 
	index( 0 ),
	splitHD( false ),
	splitHD2( false ),
	linked( false )
{
	sysMemSet( specificationFilename, 0, MAX_PATH );
	sysMemSet( filename, 0, MAX_PATH );
	sysMemSet( basename, 0, MAX_PATH );
}

TexEntry::TexEntry( const TexEntry& other )
{
	name = other.name;
	strcpy_s( specificationFilename, MAX_PATH, other.specificationFilename );
	strcpy_s( filename, MAX_PATH, other.filename );
	strcpy_s( basename, MAX_PATH, other.basename );
	fileSize = other.fileSize;
	index = other.index;
	splitHD = other.splitHD;
	splitHD2 = other.splitHD2;
	linked = other.linked;
	pSpec = other.pSpec;
}

bool	
TexEntry::LoadSpecification( const char* specFilename )
{
	sysMemStartTemp( );
	strcpy_s( specificationFilename, MAX_PATH, specFilename );
	bool foundLinkedTexture = false;

	// DHM; these specifications need to be cached; we're loading them over and over again.
	// Even better lets turn this TexEntry struct into a higher-level abstraction.
	if ( !pSpec )
		pSpec = boost::shared_ptr<CTextureConversionSpecification>(CTextureConversionSpecification::Load( specFilename, false ) );

	for ( atArray<CTextureConversionResourceTexture>::iterator it = pSpec->m_resourceTextures.begin();
		it != pSpec->m_resourceTextures.end();
		++it )
	{
		if ( foundLinkedTexture )
			break;

		CTextureConversionResourceTexture& resourceTexture = (*it);
		const char* pExtension = ASSET.FindExtensionInPath( resourceTexture.m_pathname );
		ASSET.BaseName( basename, MAX_PATH, ASSET.FileName( name ) );
		atString textureName( basename );
		textureName += pExtension;
		textureName.Lowercase();

		atString pathname( resourceTexture.m_pathname );
		pathname.Lowercase();
		if ( pathname.IndexOf( textureName ) > -1 )
		{
			foundLinkedTexture = true;

			// Need to copy this as the specification gets deleted; this temp memory
			// gets deallocated at the end of the method.  Why does RAGE need the
			// filename without the extension?!  This likely won't work for TIFFs.
			strcpy_s( filename, MAX_PATH, resourceTexture.m_pathname );
			fileSize = fiDevice::GetDevice( resourceTexture.m_pathname )->GetFileSize(
				resourceTexture.m_pathname );
		}
	}
	linked = foundLinkedTexture;
	splitHD = pSpec->m_imageSplitHD;
	splitHD2 = pSpec->m_imageSplitHD2;
	sysMemEndTemp( );

	pSpec->FinaliseConversionParams( );
	return ( foundLinkedTexture );
}

} // rage namespace
