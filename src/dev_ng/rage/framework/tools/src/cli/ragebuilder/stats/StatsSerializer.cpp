
// ========================================================
// StatsSerializer.cpp
// Copyright (c) 2012 Rockstar North.  All Rights Reserved. 
// ========================================================

// project includes
#include "StatsSerializer.h"

using namespace rage;

StatsSerializer::StatsSerializer(const char* outputFilename)
{
	outputStream_.open(outputFilename);
}

StatsSerializer::~StatsSerializer()
{
	outputStream_.close();
}

void StatsSerializer::Serialize(gtaDrawable& drawable)
{
	outputStream_ << "<Drawable>" << std::endl;

	const rage::grmShaderGroup& shaderGroup = drawable.GetShaderGroup();

	const rage::rmcLodGroup& lodGroup = drawable.GetLodGroup();
	for(int lodIndex = 0 ; lodIndex < 4 ; ++lodIndex)
	{
		if(lodGroup.ContainsLod(lodIndex))
		{
			const rmcLod& lod = lodGroup.GetLod(lodIndex);
			outputStream_ << "  <Lod>" << std::endl;
			outputStream_ << "    <Index>" << lodIndex << "</Index>" << std::endl;
			for(int modelIndex = 0 ; modelIndex < lod.GetCount() ; ++modelIndex)
			{
				outputStream_ << "    <Model>" << std::endl;
				grmModel* model = lod.GetModel(modelIndex);

				for(int geometryIndex = 0 ; geometryIndex < model->GetGeometryCount() ; ++geometryIndex)
				{
					outputStream_ << "      <Geometry>" << std::endl;

					rage::grmGeometry& geometry = model->GetGeometry(geometryIndex);
					u32 numVerts = geometry.GetVertexCount();
					u32 vertexStride = geometry.GetVertexBuffer()->GetVertexStride();
					outputStream_ << "        <NumVerts>" << numVerts << "</NumVerts>" << std::endl;
					outputStream_ << "        <VertStride>" << vertexStride << "</VertStride>" << std::endl;

					int shaderIndex = model->GetShaderIndex(geometryIndex);
					rage::grmShader& shader = shaderGroup.GetShader(shaderIndex);
					outputStream_ << "        <ShaderName>" << shader.GetName() << "</ShaderName>" << std::endl;

					const rage::spdAABB& geomAABB = model->GetGeometryAABB(geometryIndex);
					const rage::Vector3& geomAABBMin = geomAABB.GetMinVector3();
					const rage::Vector3& geomAABBMax = geomAABB.GetMaxVector3();
					outputStream_ << "        <AABBMin x='" << geomAABBMin.GetX() << "' y='" << geomAABBMin.GetY() << "' z='" << geomAABBMin.GetZ() << "' />" << std::endl;
					outputStream_ << "        <AABBMax x='" << geomAABBMax.GetX() << "' y='" << geomAABBMax.GetY() << "' z='" << geomAABBMax.GetZ() << "' />" << std::endl;

					outputStream_ << "      </Geometry>" << std::endl;
				}

				outputStream_ << "    </Model>" << std::endl;
			}

			outputStream_ << "  </Lod>" << std::endl;
		}
	}

	outputStream_ << "</Drawable>" << std::endl;
	outputStream_.close();
}