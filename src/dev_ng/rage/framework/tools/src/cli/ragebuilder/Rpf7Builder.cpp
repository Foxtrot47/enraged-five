//
// Rpf7Builder.cpp
//
// Copyright (C) 1999-2005 Rockstar North.  All Rights Reserved. 
//

// Local headers
#include "Rpf7Builder.h"

// RAGE headers

// Platform SDK headers
#include <stdlib.h>


Rpf7Builder::Rpf7Builder( bool compress, bool keepNameHeap, int shift ) : IRpfBuilder(compress,keepNameHeap), m_Builder(shift)
{
}

Rpf7Builder::~Rpf7Builder( )
{
}

bool 
Rpf7Builder::Add( const char* source, const char* destination )
{
	return m_Builder.AddFile(source, destination);
}

bool 
Rpf7Builder::Save( fiStream* pack )
{
	m_Builder.Save(pack);
	return true;
}
