//
// Lua functions for creating PKWARE-compatible ZIP files.
//
// Copyright (C) 2010 Rockstar North.  All Rights Reserved. 
//

#if !defined(LUA_ZIP_H_)
#define LUA_ZIP_H_

#include "luacore.h"

int StartZip( lua_State* L );
int StartUncompressedZip( lua_State* L );
int AddToZip( lua_State* L );
int SaveZip( lua_State* L );
int CloseZip( lua_State* L );

int MountZip( lua_State* L );
int UnmountZip( lua_State* L );

#endif LUA_ZIP_H_
