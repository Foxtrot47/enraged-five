//
// rage_independent.cpp
//
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#include "rage_independent.h"
#include "pack.h"

#include "file/asset.h"
#include "paging/rscbuilder.h"
#include "system/xtl.h"
#include "grcore/image.h"
#include "grcore/image.h"
#include "utils.h"

using namespace rage;

void RagePlatformIndependent::Init()
{
	// do nothing
}

void RagePlatformIndependent::Shutdown()
{
	// do nothing
}

// Add model files to a pack from a path
bool RagePlatformIndependent::AddModelFiles(const char* filefilter, const char* destination)
{
	char path[MAX_PATH];
	char filter[MAX_PATH];

	SplitFilename(filefilter, path, NULL, NULL );

	AssertVerify(CRagePackFileBuilder::AddFilesByFilter(filefilter, destination));
	sprintf(filter, "%s*.sva", path);
	AssertVerify(CRagePackFileBuilder::AddFilesByFilter(filter, destination));
	sprintf(filter, "%s*.bnd", path);
	AssertVerify(CRagePackFileBuilder::AddFilesByFilter(filter, destination));
	sprintf(filter, "%s*.mesh", path);
	AssertVerify(CRagePackFileBuilder::AddFilesByFilter(filter, destination));
	sprintf(filter, "%s*.skel", path);
	AssertVerify(CRagePackFileBuilder::AddFilesByFilter(filter, destination));
	sprintf(filter, "%s*.textures", path);
	AssertVerify(CRagePackFileBuilder::AddFilesByFilter(filter, destination));
	sprintf(filter, "%s*.tune", path);
	AssertVerify(CRagePackFileBuilder::AddFilesByFilter(filter, destination));

	return true;
}


// Load models.
bool RagePlatformIndependent::LoadModels(const char* filefilter)
{
	Errorf( "NYI" );
	return false;
}


// Save a texture dictionary.
bool RagePlatformIndependent::SaveTextureDictionary(const char* filename, const char* hi_suffix)
{
	// always save - since system depends on having a texdict, even if it is empty

	CRagePackFileBuilder::Open();

	char fullname[256];
	ASSET.FullPath(fullname, sizeof(fullname), filename, PI_TEXTURE_DICTIONARY_EXT);

	for(string_list::iterator it = ms_TextureList.begin(); it != ms_TextureList.end(); ++it)
	{
		const char* name = ASSET.FileName( (*it).c_str() );
		CRagePackFileBuilder::AddFile( (*it).c_str(), name );
	}

	bool rt = CRagePackFileBuilder::Save(fullname, true);
	CRagePackFileBuilder::Close();

	return rt;
}

// Save an clip dictionary.
bool RagePlatformIndependent::SaveClipDictionary(const char* filename, bool usePackLoader)
{
	if (ms_ClipList.size() == 0)
	{
		Warningf("ClipDictionary: There is nothing to save - %s\n", filename);
		return false;
	}

	CRagePackFileBuilder::Open();


	char fullname[256];
	ASSET.FullPath(fullname, sizeof(fullname), filename, PI_CLIP_DICTIONARY_EXT);

	for(string_list::iterator it = ms_ClipList.begin(); it != ms_ClipList.end(); ++it)
	{
		const char* name = ASSET.FileName( (*it).c_str() );
		CRagePackFileBuilder::AddFile( (*it).c_str(), name );
	}

	bool rt = CRagePackFileBuilder::Save( fullname );
	CRagePackFileBuilder::Close();

	return rt;
}

bool RagePlatformIndependent::SaveParamMotionDictionary(const char* filename)
{
	if (ms_ParamMotionList.size() == 0)
	{
		Warningf("PMDictionary: There is nothing to save - %s\n", filename);
		return false;
	}

	CRagePackFileBuilder::Open();


	char fullname[256];
	ASSET.FullPath(fullname, sizeof(fullname), filename, PI_PARAM_MOTION_DICTIONARY_EXT);

	for(string_list::iterator it = ms_ParamMotionList.begin(); it != ms_ParamMotionList.end(); ++it)
	{
		const char* name = ASSET.FileName( (*it).c_str() );
		CRagePackFileBuilder::AddFile( (*it).c_str(), name );
	}

	bool rt = CRagePackFileBuilder::Save( fullname );
	CRagePackFileBuilder::Close();

	return rt;
}

bool RagePlatformIndependent::SaveExpressionsDictionary(const char* filename)
{
	if (ms_ExpressionsList.size() == 0)
	{
		Warningf("ExpressionsDictionary: There is nothing to save - %s\n", filename);
		return false;
	}

	CRagePackFileBuilder::Open();


	char fullname[256];
	ASSET.FullPath(fullname, sizeof(fullname), filename, PI_EXPRESSIONS_DICTIONARY_EXT);

	for(string_list::iterator it = ms_ExpressionsList.begin(); it != ms_ExpressionsList.end(); ++it)
	{
		const char* name = ASSET.FileName( (*it).c_str() );
		CRagePackFileBuilder::AddFile( (*it).c_str(), name );
	}

	bool rt = CRagePackFileBuilder::Save( fullname );
	CRagePackFileBuilder::Close();

	return rt;
}

// Save a blend shape dictionary.
bool RagePlatformIndependent::SaveBlendShapeDictionary(const char* filename)
{
	if (ms_BlendShapeList.size() == 0)
	{
		Warningf("SaveDictionary: There is nothing to save - %s\n", filename);
		return false;
	}

	CRagePackFileBuilder::Open();


	char fullname[256];
	ASSET.FullPath(fullname, sizeof(fullname), filename, PI_BLENDSHAPE_DICTIONARY_EXT);

	for(string_list::iterator it = ms_BlendShapeList.begin(); it != ms_BlendShapeList.end(); ++it)
	{
		char path[MAX_PATH];
		char modelname[MAX_PATH];

		const char* name = (*it).c_str();
		SplitFilename(name, path, NULL, NULL);

		// now get the last folder name
		path[strlen(path)-1] = '\0';
		SplitFilename(path, NULL, modelname, NULL);

		AddModelFiles((*it).c_str(), modelname);
	}

	bool rt = CRagePackFileBuilder::Save( fullname );
	CRagePackFileBuilder::Close();

	return rt;
}

// Save a cloth dictionary.
bool RagePlatformIndependent::SaveClothDictionary(const char* filename)
{
	if (ms_ModelList.size() == 0)
	{
		Warningf("SaveDictionary: There is nothing to save - %s\n", filename);
		return false;
	}

	CRagePackFileBuilder::Open();

#if RAGE_RELEASE < 175
	pgChunkExt = PI_DRAWABLE_DICTIONARY_EXT;
#endif

	char fullname[256];
	ASSET.FullPath(fullname, sizeof(fullname), filename, PI_DRAWABLE_DICTIONARY_EXT);

	for(string_list::iterator it = ms_ModelList.begin(); it != ms_ModelList.end(); ++it)
	{
		char path[MAX_PATH];
		char modelname[MAX_PATH];

		const char* name = (*it).c_str();
		SplitFilename(name, path, NULL, NULL);

		// now get the last folder name
		path[strlen(path)-1] = '\0';
		SplitFilename(path, NULL, modelname, NULL);

		AddModelFiles((*it).c_str(), modelname);
	}

	bool rt = CRagePackFileBuilder::Save( fullname );
	CRagePackFileBuilder::Close();

	return rt;
}


// Save a model dictionary.
bool RagePlatformIndependent::SaveModelDictionary(const char* filename)
{
	if (ms_ModelList.size() == 0)
	{
		Warningf("SaveDictionary: There is nothing to save - %s\n", filename);
		return false;
	}

	CRagePackFileBuilder::Open();

#if RAGE_RELEASE < 175
	pgChunkExt = PI_DRAWABLE_DICTIONARY_EXT;
#endif

	char fullname[256];
	ASSET.FullPath(fullname, sizeof(fullname), filename, PI_DRAWABLE_DICTIONARY_EXT);

	for(string_list::iterator it = ms_ModelList.begin(); it != ms_ModelList.end(); ++it)
	{
		char path[MAX_PATH];
		char modelname[MAX_PATH];

		const char* name = (*it).c_str();
		SplitFilename(name, path, NULL, NULL);

		// now get the last folder name
		path[strlen(path)-1] = '\0';
		SplitFilename(path, NULL, modelname, NULL);

		AddModelFiles((*it).c_str(), modelname);
	}

	bool rt = CRagePackFileBuilder::Save( fullname );
	CRagePackFileBuilder::Close();

	return rt;
}


// Save a bound dictionary.
bool RagePlatformIndependent::SaveBoundDictionary(const char* filename)
{
	if (ms_BoundList.size() == 0)
	{
		Warningf("SaveDictionary: There is nothing to save - %s\n", filename);
		return false;
	}

	CRagePackFileBuilder::Open();

#if RAGE_RELEASE < 175
	pgChunkExt = PI_BOUNDS_DICTIONARY_EXT;
#endif

	char fullname[256];
	ASSET.FullPath(fullname, sizeof(fullname), filename, PI_BOUNDS_DICTIONARY_EXT);

	for(string_list::iterator it = ms_BoundList.begin(); it != ms_BoundList.end(); ++it)
	{
		const char* name = ASSET.FileName( (*it).c_str() );
		CRagePackFileBuilder::AddFile( (*it).c_str(), name );
	}

	bool rt = CRagePackFileBuilder::Save( fullname );
	CRagePackFileBuilder::Close();

	return rt;
}


// Save a physics dictionary.
bool RagePlatformIndependent::SavePhysicsDictionary(const char* filename)
{
	if (ms_BoundList.size() == 0)
	{
		Warningf("SaveDictionary: There is nothing to save - %s\n", filename);
		return false;
	}

	CRagePackFileBuilder::Open();

#if RAGE_RELEASE < 175
	pgChunkExt = PI_PHYSICS_DICTIONARY_EXT;
#endif

	char fullname[256];
	ASSET.FullPath(fullname, sizeof(fullname), filename, PI_PHYSICS_DICTIONARY_EXT);

	for(string_list::iterator it = ms_BoundList.begin(); it != ms_BoundList.end(); ++it)
	{
		const char* name = ASSET.FileName( (*it).c_str() );
		CRagePackFileBuilder::AddFile( (*it).c_str(), name );
	}

	bool rt = CRagePackFileBuilder::Save( fullname );
	CRagePackFileBuilder::Close();

	return rt;
}

// Save a blend shape
bool RagePlatformIndependent::SaveBlendShape(const char* filenameIn, const char* filenameOut)
{
	CRagePackFileBuilder::Open();

#if RAGE_RELEASE < 175
	pgChunkExt = PI_BLENDSHAPE_EXT;
#endif

	char fullname[256];
	ASSET.FullPath(fullname, sizeof(fullname), filenameOut, PI_BLENDSHAPE_EXT);

	for(string_list::iterator it = ms_BlendShapeList.begin(); it != ms_BlendShapeList.end(); ++it)
	{
		AddModelFiles((*it).c_str(), NULL);
	}

	bool rt = CRagePackFileBuilder::Save( fullname );
	CRagePackFileBuilder::Close();

	return rt;
}

// Save a model dictionary.
bool RagePlatformIndependent::SaveModel(const char* filename)
{
	CRagePackFileBuilder::Open();

#if RAGE_RELEASE < 175
	pgChunkExt = PI_DRAWABLE_EXT;
#endif

	char fullname[256];
	ASSET.FullPath(fullname, sizeof(fullname), filename, PI_DRAWABLE_EXT);

	for(string_list::iterator it = ms_ModelList.begin(); it != ms_ModelList.end(); ++it)
	{
		AddModelFiles((*it).c_str(), NULL);
	}

	//for in built textures...

	for(string_list::iterator it = ms_TextureList.begin(); it != ms_TextureList.end(); ++it)
	{
		const char* name = ASSET.FileName( (*it).c_str() );
		CRagePackFileBuilder::AddFile( (*it).c_str(), name );
	}

	bool rt = CRagePackFileBuilder::Save( fullname );
	CRagePackFileBuilder::Close();

	return rt;
}


// Save a fragment.
bool RagePlatformIndependent::SaveFragment(const char* filename)
{
	CRagePackFileBuilder::Open();

#if RAGE_RELEASE < 175
	pgChunkExt = PI_FRAGMENT_EXT;
#endif

	char fullname[256];
	ASSET.FullPath(fullname, sizeof(fullname), filename, PI_FRAGMENT_EXT);

	for(string_list::iterator it = ms_FragmentList.begin(); it != ms_FragmentList.end(); ++it)
	{
		AddModelFiles((*it).c_str(), NULL);
	}

	bool rt = CRagePackFileBuilder::Save( fullname );
	CRagePackFileBuilder::Close();

	return rt;
}

bool RagePlatformIndependent::SaveTexture(const char* filename)
{
	if(ms_TextureList.size() < 1)
		return false;

	//the reason for this is so that the texture limits such as max mips
	//and the texture size caps

	grcImage* image = grcImage::Load((*ms_TextureList.begin()).c_str());

	if(!image)
		return false;

	image->SaveDDS(filename);
	image->Release();

	return true;
}

// Save a bound.
bool RagePlatformIndependent::SaveBound(const char* filename)
{
	CRagePackFileBuilder::Open();

#if RAGE_RELEASE < 175
	pgChunkExt = PI_BOUNDS_EXT;
#endif

	char fullname[256];
	ASSET.FullPath(fullname, sizeof(fullname), filename, PI_BOUNDS_EXT);

	for(string_list::iterator it = ms_BoundList.begin(); it != ms_BoundList.end(); ++it)
	{
		const char* name = ASSET.FileName( (*it).c_str() );
		CRagePackFileBuilder::AddFile( (*it).c_str(), name );
	}

	bool rt = CRagePackFileBuilder::Save( fullname );
	CRagePackFileBuilder::Close();

	return rt;
}

// Save a pathRegion file.
bool RagePlatformIndependent::SavePathRegion(const char* filename)
{
	CRagePackFileBuilder::Open();

#if RAGE_RELEASE < 175
	pgChunkExt = PI_PATHREGION_EXT;
#endif

	char fullname[256];
	ASSET.FullPath(fullname, sizeof(fullname), filename, PI_PATHREGION_EXT);

	for(string_list::iterator it = ms_PathRegionList.begin(); it != ms_PathRegionList.end(); ++it)
	{
		const char* name = ASSET.FileName( (*it).c_str() );
		CRagePackFileBuilder::AddFile( (*it).c_str(), name );
	}

	bool rt = CRagePackFileBuilder::Save( fullname );
	CRagePackFileBuilder::Close();

	return rt;
}


// Save a navmesh.
bool RagePlatformIndependent::SaveNavMesh(const char* filename)
{
	CRagePackFileBuilder::Open();

#if RAGE_RELEASE < 175
	pgChunkExt = PI_NAVMESH_EXT;
#endif

	char fullname[256];
	ASSET.FullPath(fullname, sizeof(fullname), filename, PI_NAVMESH_EXT);

	for(string_list::iterator it = ms_NavMeshList.begin(); it != ms_NavMeshList.end(); ++it)
	{
		const char* name = ASSET.FileName( (*it).c_str() );
		CRagePackFileBuilder::AddFile( (*it).c_str(), name );
	}

	bool rt = CRagePackFileBuilder::Save( fullname );
	CRagePackFileBuilder::Close();

	return rt;
}

// Save a heightmesh.
bool RagePlatformIndependent::SaveHeightMesh(const char* filename)
{
	CRagePackFileBuilder::Open();

	char fullname[256];
	ASSET.FullPath(fullname, sizeof(fullname), filename, PI_HEIGHTMESH_EXT);

	for(string_list::iterator it = ms_HeightMeshList.begin(); it != ms_HeightMeshList.end(); ++it)
	{
		const char* name = ASSET.FileName( (*it).c_str() );
		CRagePackFileBuilder::AddFile( (*it).c_str(), name );
	}

	bool rt = CRagePackFileBuilder::Save( fullname );
	CRagePackFileBuilder::Close();

	return rt;
}

bool RagePlatformIndependent::SaveAudMesh(const char* filename)
{
	CRagePackFileBuilder::Open();

#if RAGE_RELEASE < 175
	pgChunkExt = PI_NAVMESH_EXT;
#endif

	char fullname[256];
	ASSET.FullPath(fullname, sizeof(fullname), filename, PI_AUDMESH_EXT);

	for (string_list::iterator it = ms_AudMeshList.begin(); it != ms_AudMeshList.end(); ++it)
	{
		const char* name = ASSET.FileName((*it).c_str());
		CRagePackFileBuilder::AddFile((*it).c_str(), name);
	}

	bool rt = CRagePackFileBuilder::Save( fullname);
	CRagePackFileBuilder::Close();

	return rt;
}

// Save a hierarchical nav file.
bool RagePlatformIndependent::SaveHierarchicalNav(const char* filename)
{
	CRagePackFileBuilder::Open();

#if RAGE_RELEASE < 175
	pgChunkExt = PI_HIERARCHICAL_NAV_EXT;
#endif

	char fullname[256];
	ASSET.FullPath(fullname, sizeof(fullname), filename, PI_HIERARCHICAL_NAV_EXT);

	for(string_list::iterator it = ms_HierarchicalNavList.begin(); it != ms_HierarchicalNavList.end(); ++it)
	{
		const char* name = ASSET.FileName( (*it).c_str() );
		CRagePackFileBuilder::AddFile( (*it).c_str(), name );
	}

	bool rt = CRagePackFileBuilder::Save( fullname );
	CRagePackFileBuilder::Close();

	return rt;
}

// Save a waypoint route file.
bool RagePlatformIndependent::SaveWaypointRecording(const char* filename)
{
	CRagePackFileBuilder::Open();

#if RAGE_RELEASE < 175
	pgChunkExt = PI_WAYPOINT_RECORDING_EXT;
#endif

	char fullname[256];
	ASSET.FullPath(fullname, sizeof(fullname), filename, PI_WAYPOINT_RECORDING_EXT);

	for(string_list::iterator it = ms_WaypointRecordingList.begin(); it != ms_WaypointRecordingList.end(); ++it)
	{
		const char* name = ASSET.FileName( (*it).c_str() );
		CRagePackFileBuilder::AddFile( (*it).c_str(), name );
	}

	bool rt = CRagePackFileBuilder::Save( fullname );
	CRagePackFileBuilder::Close();

	return rt;
}

void Effects_AddToPackFromList(const char* listfile,const char* p_effectpath,const char* p_subfolder,const char* p_ext)
{
	fiStream *S = ASSET.Open(listfile,"");
	int i,lineCount = 0;
	char buf[RAGE_MAX_PATH];
	while (fgetline(buf,sizeof(buf),S))
		++lineCount;
	const char **inputs = Alloca(const char*,lineCount);
	S->Seek(0);
	lineCount = 0;
	while (fgetline(buf,sizeof(buf),S)) {
		inputs[lineCount] = Alloca(char,strlen(buf)+1);
		strcpy((char*)inputs[lineCount], buf);
		++lineCount;
	}
	S->Close();

	char addfile[MAX_PATH];

	for(i=0;i<lineCount;i++)
	{
		if(p_ext[0] == '\0')
		{
			sprintf(addfile,"%s%s/%s/*.*",p_effectpath,p_subfolder,inputs[i]);

			const char* p_file;
			string_list filelist;
			AddFilesToList( addfile, filelist, true, true );

			for (string_list::iterator it = filelist.begin(); it != filelist.end(); ++it)
			{
				p_file = (*it).c_str();
				sprintf(addfile,"%s%s/%s/%s",p_effectpath,p_subfolder,inputs[i],p_file);

				//special case hack...

				if(strstr(p_file,".dds")||strstr(p_file,".DDS"))
				{
					char tofile[1024];
					sprintf(tofile,"modeltex/%s",p_file);

					CRagePackFileBuilder::AddFile(addfile, tofile);
				}
				else
				{
					CRagePackFileBuilder::AddFile(addfile, addfile + strlen(p_effectpath));
				}
			}
		}
		else
		{
			sprintf(addfile,"%s%s/%s.%s",p_effectpath,p_subfolder,inputs[i],p_ext);
			CRagePackFileBuilder::AddFile( addfile, addfile + strlen(p_effectpath));
		}
	}
}

bool RagePlatformIndependent::SaveEffect(const char* filename)
{
	const char* p_effectpath = RagePlatform::GetEffectPath();
	const char* p_effect = (ms_EffectList.begin())->c_str();
	const char* effectname;

	char outfilename[MAX_PATH];
	char srcfolder[MAX_PATH];
	char effectnoext[MAX_PATH];
	char addfile[MAX_PATH];

	ASSET.RemoveExtensionFromPath( effectnoext, MAX_PATH, p_effect);
	ASSET.RemoveExtensionFromPath(srcfolder, MAX_PATH, filename);
	ASSET.FullPath(outfilename, sizeof(outfilename), filename, PI_EFFECT_EXT);

	effectname = ASSET.FileName(effectnoext);

	CRagePackFileBuilder::Open();

	//const char* p_savename = p_effect + strlen(p_effectpath);
	//CRagePackFileBuilder::AddFile( p_effect, p_savename );

	//textures...

	sprintf(addfile,"%sfxlists/%s/%s.texlist",p_effectpath,effectname,effectname);
	CRagePackFileBuilder::AddFile( addfile, addfile + strlen(p_effectpath));
	Effects_AddToPackFromList(addfile,p_effectpath,"textures","dds");

	sprintf(addfile,"%sfxlists/%s/%s.shaderlist",p_effectpath,effectname,effectname);
	CRagePackFileBuilder::AddFile( addfile, addfile + strlen(p_effectpath));

	sprintf(addfile,"%sfxlists/%s/%s.ptxlist",p_effectpath,effectname,effectname);
	CRagePackFileBuilder::AddFile( addfile, addfile + strlen(p_effectpath));
	Effects_AddToPackFromList(addfile,p_effectpath,"ptxrules","ptxrule");

	sprintf(addfile,"%sfxlists/%s/%s.modellist",p_effectpath,effectname,effectname);
	CRagePackFileBuilder::AddFile( addfile, addfile + strlen(p_effectpath));
	Effects_AddToPackFromList(addfile,p_effectpath,"models","");

	sprintf(addfile,"%sfxlists/%s/%s.fxlist",p_effectpath,effectname,effectname);
	CRagePackFileBuilder::AddFile( addfile, addfile + strlen(p_effectpath));

	sprintf(addfile,"%sfxlists/%s/%s.fullfxlist",p_effectpath,effectname,effectname);
	CRagePackFileBuilder::AddFile( addfile, addfile + strlen(p_effectpath));
	Effects_AddToPackFromList(addfile,p_effectpath,"effectrules","effectrule");

	sprintf(addfile,"%sfxlists/%s/%s.emitlist",p_effectpath,effectname,effectname);
	CRagePackFileBuilder::AddFile( addfile, addfile + strlen(p_effectpath));
	Effects_AddToPackFromList(addfile,p_effectpath,"emitrules","emitrule");

	bool rt = CRagePackFileBuilder::Save( outfilename );
	CRagePackFileBuilder::Close();

	return rt;
}

bool RagePlatformIndependent::SaveFrameFilterDictionary(const char* filename)
{
	if (ms_FrameFilterList.size() == 0)
	{
		Warningf("FrameFilterDictionary: There is nothing to save - %s\n", filename);
		return false;
	}

	CRagePackFileBuilder::Open();


	char fullname[256];
	ASSET.FullPath(fullname, sizeof(fullname), filename, PI_FRAMEFILTER_DICTIONARY_EXT);

	for(string_list::iterator it = ms_FrameFilterList.begin(); it != ms_FrameFilterList.end(); ++it)
	{
		const char* name = ASSET.FileName( (*it).c_str() );
		CRagePackFileBuilder::AddFile( (*it).c_str(), name );
	}

	bool rt = CRagePackFileBuilder::Save( fullname );
	CRagePackFileBuilder::Close();

	return rt;
}
