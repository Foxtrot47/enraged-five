//
// ragebuilder.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "ragebuilder.h"
#include "gta.h"
#include "log.h"
#include "gtamaterialmgr.h"
#include "gtashaderfactory.h"
#include "scriptgta.h"
#include "scriptgeneral.h"
#include "clipmgr.h"
#include "asset_link.h"
#include "textureconversion/textureconversion.h"
#include "textureconversion/texentry.h"
#include "textureconversion/texturefactory.h"
#include "textureconversion/imageutil.h"
#include "stats/StatsSerializer.h"

#include "rage_win32.h"
#include "rage_independent.h"
#include "rage_xenon.h"
#include "rage_ps3.h"
#include "rage_durango.h"
#include "rage_orbis.h"

#include "breakableglass/glassmanager.h"
#include "cranimation/animation.h"
#include "crclip/clipanimation.h"
#include "crclip/clipdictionary.h"
#include "crtools/animcompresscore.h"
#include "cloth/charactercloth.h"
#include "crmetadata/propertyattributes.h"
#include "crmetadata/properties.h"
#include "crskeleton/skeletondata.h"
#include "crskeleton/skeleton.h"
#include "crparameterizedmotion/parameterizedmotiondictionary.h"
#include "crextra/expressions.h"
#include "mesh/mesh.h"
#include "data/resource.h"
#include "devil/il.h"
#include "devil/ilu.h"
#include "file/asset.h"
#include "file/packfile.h"
#include "fragment/drawable.h"
#include "fragment/resourceversions.h"
#include "fragment/tune.h"
#include "fragment/type.h"
#include "fragment/typechild.h"
#include "fragment/typephysicslod.h"
#include "grblendshapes/blendshapes_config.h"
#include "grblendshapes/manager.h"
#include "grcore/image.h"
#include "grcore/texturedefault.h"
#include "grcore/vertexbuffer.h"
#include "grcore/texturegcm.h"
#include "grcore/texturexenon.h"
#include "grmodel/modelfactory.h"
#include "grmodel/setup.h"
#include "grmodel/shader.h"
#include "grmodel/shaderfx.h"
#include "grmodel/shadergroup.h"
#include "grmodel/geometry.h"
#include "paging/dictionary.h"
#include "paging/rscbuilder.h"
#include "parser/manager.h"
#include "parser/psofile.h"
#include "parser/psoparserbuilder.h"
#include "parser/psorscparserbuilder.h"
#include "parser/visitorutils.h"
#include "phbound/bound.h"
#include "phbound/boundcomposite.h"
#include "phcore/config.h"
#include "phcore/materialmgr.h"
#include "phcore/materialmgrimpl.h"
#include "physics/archetype.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "rmcore/drawable.h"
#include "rmptfx/ptxfxlist.h"
#include "rmptfx/ptxmanager.h"
#include "script/program.h"
#include "spatialdata/aabb.h"
#include "system/memory.h"
#include "system/param.h"
#include "system/xtl.h"
#include "system/platform.h"
#include "diag/art_channel.h"
#include "string/stringutil.h"

//framework headers

#include "fwaudio/audmesh.h"
#include "ai/navmesh/navmesh.h"
#include "vfx/ptfx/behaviours/ptxu_decal.h"
#include "vfx/ptfx/behaviours/ptxu_decalpool.h"
#include "vfx/ptfx/behaviours/ptxu_decaltrail.h"
#include "vfx/ptfx/behaviours/ptxu_fire.h"
#include "vfx/ptfx/behaviours/ptxu_fogvolume.h"
#include "vfx/ptfx/behaviours/ptxu_light.h"
#include "vfx/ptfx/behaviours/ptxu_liquid.h"
#include "vfx/ptfx/behaviours/ptxu_river.h"
#include "vfx/ptfx/behaviours/ptxu_zcull.h"
#include "vfx/vehicleglass/vehicleglassconstructor.h"
#include "vfx/vehicleglass/vehicleglasswindow.h"
#include "fwvehicleai/pathfindtypes.h"
#include "fwcontrol/waypointrecordingroute.h"
#include "fwcontrol/record.h"
#include "fwlocalisation/textDatabaseSource.h"
#include "fwlocalisation/textFileConversion.h"
#include "fwscene/mapdata/mapdata.h"
#include "fwscene/stores/framefilterdictionarystore.h"
#include "pedsafezone/pedsafezone.h"

// Boost headers
#include "boost/shared_ptr.hpp"

// Xenon specific:
#ifndef _DEBUG
	#define	_DEBUG
	#include "d3d9.h"
	#undef	_DEBUG
#else
	#include "d3d9.h"
#endif
#include "d3d9types.h"
#include "xgraphics.h"

using namespace rage;

#if !__PAGING
#error "Need paging build"
#endif

RageBuilder::PLATFORM			RageBuilder::ms_ePlatform;
RagePlatform*					RageBuilder::ms_pPlatform;
RageBuilder::COMPRESSION_TYPE	RageBuilder::ms_compressionType;
sysMemExternalAllocator*		RageBuilder::ms_virtualFixedAllocator = NULL;
sysMemExternalAllocator*		RageBuilder::ms_physicalFixedAllocator = NULL;
phLevelNew*						RageBuilder::ms_pLevelNew(NULL);
phSimulator*					RageBuilder::ms_pPhysSim(NULL);
fragManager*					RageBuilder::ms_pFragManager(NULL);

const configParser::ConfigGameView*	RageBuilder::gv;

#if __64BIT
#define	DEFAULT_BUILD_SIZE	( 128 * 1024 * 1024 )
#define SCRIPT_BUILD_SIZE   ( 8 * 1024 * 1024 )
#else
#define	DEFAULT_BUILD_SIZE	( 64 * 1024 * 1024 )
#define SCRIPT_BUILD_SIZE   ( 4 * 1024 * 1024 )
#endif	// _64BIT

//XPARAM(vertexopt,"Enable vertex cache optimization at load time.");
XPARAM(vertexopt);
XPARAM(rmptfxpath);

namespace rage {
	XPARAM(nointerleavemips);
}

PARAM(key,"Not sure, but avoids a warning.");
PARAM(value,"Not sure, but avoids a warning.");
PARAM(trail,"Not sure, but avoids a warning.");

PARAM(validatepsolayout, "If true, makes sure the PSO layout derived from ParserSchema (PSC) files matches the latest info from the runtime (stored in .#esd files)");

//#define INIT_GFX

bool			RagePlatform::ms_InitedFactories = false;
bool			RagePlatform::ms_CompressResources = false;
rage::fragManager* m_FragManager(NULL);
char			RagePlatform::ms_shaderpath[RAGE_MAX_PATH];
char			RagePlatform::ms_shaderdbpath[RAGE_MAX_PATH];
char			RagePlatform::ms_TuningFolder[RAGE_MAX_PATH];
char			RagePlatform::ms_effectpath[RAGE_MAX_PATH];
char			RagePlatform::ms_BuildPath[RAGE_MAX_PATH];
char			RagePlatform::ms_AssetsPath[RAGE_MAX_PATH];
char			RagePlatform::ms_CoreAssetsPath[RAGE_MAX_PATH];
char			RagePlatform::ms_MetadataDefinitionsPath[RAGE_MAX_PATH];
s32				RagePlatform::m_LimitStackPos(0);
std::string		RagePlatform::ms_OverloadTexSrc;
RagePlatform::LimitStackSave RagePlatform::m_LimitStack[MAX_STACK];
RagePlatform::DWDTYPE		 RagePlatform::ms_eDwdType = RagePlatform::dwdHD;

bool	RagePlatform::ms_PostProcessVehicles = false;
bool	RagePlatform::ms_HeadBlendGeometry = false;
bool	RagePlatform::ms_HeadBlendWriteBuffer = false;
bool	RagePlatform::ms_MicroMorphMesh = false;
bool	RagePlatform::ms_EnableFragMatrixSet = true;
bool	RagePlatform::ms_PPUOnlyHint = false;
bool	RagePlatform::ms_LodSkeleton = false;

parManager*		RagePlatform::ms_pExternalParser = NULL;
parManager*		RagePlatform::ms_pReferenceParser = NULL;

static atArray<u8>* PrepackageTint(rmcDrawable *pDrawable, const char *pFilename);

namespace rage
{
	XPARAM(uncompressed);
	XPARAM(drawablecontainersize);
}

const configParser::ConfigGameView& RageBuilder::GetConfig()
{
	Assert(gv);
	return *gv;
}

static void SetBoundCenters(mshMaterial& mtl, const int iTangentIndex, const int iRadius)
{
	const u32 numVerts = mtl.GetVertexCount();
	const u32 numTriangles = mtl.GetTriangleCount();

	const u32 FaceUnused = 0xFFFFFFFF;
	mshArray<u32> Faces;
	Faces.Init(FaceUnused, numTriangles);

	const u32 IndexUnused = 0xFFFFFFFF;
	mshArray<u32> IndexGroups;
	IndexGroups.Init(IndexUnused, numTriangles * 3);

	u32 uGroupCount = 0;

	do 
	{
		// Loop until all faces are marked used
		// Find free unused face
		mshMaterial::TriangleIterator FreeTriIt;
		u32 uUnusedFace = FaceUnused;
		s32 iFace = 0;

		for (FreeTriIt = mtl.BeginTriangles(); FreeTriIt!=mtl.EndTriangles(); ++FreeTriIt)
		{
			if (Faces[iFace] == FaceUnused)
			{
				// New face group found
				uUnusedFace = iFace;
				Faces[iFace] = uGroupCount;
				break;
			}
			++iFace;
		}

		if (uUnusedFace == FaceUnused)
		{
			// All groups of triangles have been found - Stop and center them
			break;
		}

		// Mark all indices as used
		int v[3];
		FreeTriIt.GetVertIndices(v[0],v[1],v[2]);
		IndexGroups[v[0]] = uGroupCount;
		IndexGroups[v[1]] = uGroupCount;
		IndexGroups[v[2]] = uGroupCount;

		// Loop over and over again until no new indices are marked as unused
		bool bMarkedIndex;
		do
		{
			bMarkedIndex = false;
			s32 iFace = 0;

			for (mshMaterial::TriangleIterator triIt = mtl.BeginTriangles(); triIt!=mtl.EndTriangles(); ++triIt )
			{
				int v[3];
				triIt.GetVertIndices(v[0],v[1],v[2]);
				if ((IndexGroups[v[0]] == uGroupCount) ||
					(IndexGroups[v[1]] == uGroupCount) ||
					(IndexGroups[v[2]] == uGroupCount))
				{
					// Triangle may or may not be part of group already check if any indices are not already marked
					if ((IndexGroups[v[0]] != uGroupCount) ||
						(IndexGroups[v[1]] != uGroupCount) ||
						(IndexGroups[v[2]] != uGroupCount))
					{
						Faces[iFace] = uGroupCount;
						IndexGroups[v[0]] = IndexGroups[v[1]] = IndexGroups[v[2]] = uGroupCount;
						bMarkedIndex = true;
					}
				}
				iFace++;
			}
		} while(bMarkedIndex);

		// Full group found - Center group for camera facing
		Vector3 centre(0.0f, 0.0f, 0.0f);
		Vector3 vBodyMin(FLT_MAX, FLT_MAX, FLT_MAX);
		Vector3 vBodyMax(-FLT_MAX, -FLT_MAX, -FLT_MAX);
		u32 uFaceCount = 0;

		// Average all point in group
		mshMaterial::TriangleIterator triIt;
		for (triIt = mtl.BeginTriangles();triIt!=mtl.EndTriangles();++triIt)
		{
			int v[3];
			triIt.GetVertIndices(v[0],v[1],v[2]);
			if (IndexGroups[v[0]] == uGroupCount)
			{
				for(int k=0;k<3;k++)
				{
					centre += mtl.GetVertex(v[k]).Pos;
					vBodyMin.Min(vBodyMin,mtl.GetPos(v[k]));
					vBodyMax.Max(vBodyMax,mtl.GetPos(v[k]));
				}

				uFaceCount++;
			}
		}

		// Centre of Group
		centre /= ((float)uFaceCount * 3.0f);

		Vector3 vRadius;
		vRadius = (vBodyMax - vBodyMin); 
		float fRadius = max(vRadius.GetX(), max(vRadius.GetY(), vRadius.GetZ()));

		// Assign center to tangent index
		for (triIt = mtl.BeginTriangles();triIt!=mtl.EndTriangles();++triIt)
		{
			int v[3];
			triIt.GetVertIndices(v[0],v[1],v[2]);
			if (IndexGroups[v[0]] == uGroupCount)
			{
				for(int k=0;k<3;k++)
				{
					// Hard coded to tangent 1 which means it needs a tangent in order to work which will only work with normal maps
					// I would like to put the radius into component 4 but hard coded limit
					mtl.SetTangent(v[k], iTangentIndex, Vector3(centre.x, centre.y, centre.z));
				}
			}
		}
		// Start next group
		uGroupCount++;
	} while(true);
}

static void ConvertResourceBoundFlagsToRuntimeTypeFlags(u32 nBoundTypeFlag, u32& nTypeFlags, u32& nIncludeFlags, bool& bContainsMover)
{
	// This function generates correct per-component type / include flags for phBoundComposites from the
	// bound flag tokens in .bnd files.

	if(nBoundTypeFlag >= BoundTypes::LAST_BOUND_TYPE)
	{
		Warningf("Collision found with unexpected bound type flags. Raw bound flags=0x%08x.", nBoundTypeFlag);
		nBoundTypeFlag = 0;
	}

	// Enforce good behaviour on spotting some invalid flag combinations.
	if(nBoundTypeFlag&BoundTypes::WEAPON_COLLISION_BOUND &&
		!(nBoundTypeFlag&BoundTypes::MOVER_COLLISION_BOUND || nBoundTypeFlag&BoundTypes::DEEP_SURFACE_BOUND))
	{
		// If a bound is flagged as WEAPON and does not have MOVER or DEEP_SURFACE set we assume none of the other map collision types should be set.
		nBoundTypeFlag = BoundTypes::WEAPON_COLLISION_BOUND;
	}
	if(nBoundTypeFlag&BoundTypes::MOVER_COLLISION_BOUND)
	{
		// If MOVER flag is set on a bound we assume all other low LOD map collision types should be set.
		nBoundTypeFlag |= BoundTypes::HORSE_COLLISION_BOUND | BoundTypes::COVER_COLLISION_BOUND;
	}


	// Convert the resource bound type flags into runtime type/include flags.
	if(nBoundTypeFlag&BoundTypes::MOVER_COLLISION_BOUND)
	{
		nTypeFlags |= ArchetypeFlags::GTA_MAP_TYPE_MOVER;
		// Set flags in archetype to say what type of physics object we wish to collide with.
		nIncludeFlags |= ArchetypeFlags::GTA_BUILDING_INCLUDE_TYPES;
	}
	if(nBoundTypeFlag&BoundTypes::WEAPON_COLLISION_BOUND)
	{
		nTypeFlags |= ArchetypeFlags::GTA_MAP_TYPE_WEAPON;
		// Set flags in archetype to say what type of physics object we wish to collide with.
		nIncludeFlags |= ArchetypeFlags::GTA_HIGH_DETAIL_MAP_INCLUDE_TYPES;
	}
	if(nBoundTypeFlag&BoundTypes::DEEP_SURFACE_BOUND)
	{
		// Special collision bounds which represent the top surface in areas where objects can sink into the ground (like sand / snow).
		nTypeFlags |= ArchetypeFlags::GTA_DEEP_SURFACE_TYPE;
		// Set flags in archetype to say what type of physics object we wish to collide with.
		nIncludeFlags |= ArchetypeFlags::GTA_CAMERA_TEST;
	}
	if(nBoundTypeFlag&BoundTypes::HORSE_COLLISION_BOUND)
	{
		// Special collision bounds meant to affect horse mover / ragdoll only.
		nTypeFlags |= ArchetypeFlags::GTA_MAP_TYPE_HORSE;
		// Set flags in archetype to say what type of physics object we wish to collide with.
		nIncludeFlags |= ArchetypeFlags::GTA_HORSE_TYPE | ArchetypeFlags::GTA_HORSE_RAGDOLL_TYPE;
	}
	if(nBoundTypeFlag&BoundTypes::COVER_COLLISION_BOUND)
	{
		// Special collision bounds used for cover tests only.
		nTypeFlags |= ArchetypeFlags::GTA_MAP_TYPE_COVER;
		// Set flags in archetype to say what type of physics object we wish to collide with.
		nIncludeFlags |= ArchetypeFlags::GTA_ALL_SHAPETEST_TYPES;
	}
	if(nBoundTypeFlag&BoundTypes::VEHICLE_COLLISION_BOUND)
	{
		// Special collision bounds meant to affect vehicles only.
		nTypeFlags |= ArchetypeFlags::GTA_MAP_TYPE_VEHICLE;
		// Set flags in archetype to say what type of physics object we wish to collide with.
		nIncludeFlags |= ArchetypeFlags::GTA_VEHICLE_TYPE;
	}


	// NB - Do these last. We don't want any other collision types screwing with river bounds, foliage or stair slope bounds, hence the
	// direct assignment here instead of a binary composition.
	if(nBoundTypeFlag&BoundTypes::FOLIAGE_COLLISION_BOUND)
	{
		if(nBoundTypeFlag!=BoundTypes::FOLIAGE_COLLISION_BOUND)
		{
			Warningf("Foliage bound found with extra collision flags. Raw bound flags=0x%08x.", nBoundTypeFlag);
		}

		nTypeFlags = ArchetypeFlags::GTA_FOLIAGE_TYPE;
		// Set flags in archetype to say what type of physics object we wish to collide with.
		nIncludeFlags = ArchetypeFlags::GTA_OBJECT_INCLUDE_TYPES;
		nIncludeFlags &= ~ArchetypeFlags::GTA_WHEEL_TEST;
	}
	if(nBoundTypeFlag&BoundTypes::STAIR_SLOPE_BOUND)
	{
		if(nBoundTypeFlag!=BoundTypes::STAIR_SLOPE_BOUND)
		{
			Warningf("Stair ramp bound found with extra collision flags. Raw bound flags=0x%08x.", nBoundTypeFlag);
		}

		// Special collision bounds which go over the top of a set of stairs like a ramp to make smooth out ped
		// capsule motion.
		nTypeFlags = ArchetypeFlags::GTA_STAIR_SLOPE_TYPE;
		// Set flags in archetype to say what type of physics object we wish to collide with.
		nIncludeFlags = ArchetypeFlags::GTA_PED_TYPE;
		// Allow any shape tests to find the stair slope bound if they enable it in their include flags.
		nIncludeFlags |= ArchetypeFlags::GTA_ALL_SHAPETEST_TYPES;
	}
	if(nBoundTypeFlag&BoundTypes::RIVER_BOUND)
	{
		if(nBoundTypeFlag!=BoundTypes::RIVER_BOUND)
		{
			Warningf("River bound found with extra collision flags. Raw bound flags=0x%08x.", nBoundTypeFlag);
		}

		nTypeFlags = ArchetypeFlags::GTA_RIVER_TYPE;
		// Set flags in archetype to say what type of physics object we wish to collide with.
		nIncludeFlags = ArchetypeFlags::GTA_ALL_SHAPETEST_TYPES;
	}
}

void RageBuilder::Init()
{
	// set the current directory
#if __XENON
	ASSET.SetPath("x:\\");
#else // __XENON
	char currentDirectory[256];
	if(GetCurrentDirectory(256, &currentDirectory[0]))
	{
		ASSET.SetPath(&currentDirectory[0]);
	}
#endif // __XENON

	//ms_ePlatform = RageBuilder::pltWin32;
	//ms_pPlatform = new RagePlatformWin32();
	ms_ePlatform = RageBuilder::pltNeutral;
	ms_pPlatform = NULL;

	ms_compressionType = RageBuilder::ctDefault;

	PARAM_drawablecontainersize.Set("256");

	//we shouldnt set the platform at startup anymore.
	//this should be done explicitly in script to reduce
	//script dependency on shaders
	//	SetPlatform(RageBuilder::pltWin32);

	// We have to enable ref counting before creating the frag manager, because we are going to turn it on later,
	// then we get a crash during shutdown.
	phConfig::EnableRefCounting();

	// create a physics level

	ms_pLevelNew = new phLevelNew;
	ms_pLevelNew->SetActiveInstance();
	ms_pLevelNew->SetExtents(Vec3V(-10000.0f, -10000.0f, -3000.0f), Vec3V(10000.0f, 10000.0f, 3000.0f));
	ms_pLevelNew->SetMaxObjects(64);
	ms_pLevelNew->SetMaxActive(64);
	ms_pLevelNew->SetNumOctreeNodes(128);
	ms_pLevelNew->Init();

	phSimulator::InitClass();

	phSimulator::InitParams simParams;
	simParams.maxManagedColliders = 64;
	simParams.scratchpadSize = 1024*1024;
	simParams.maxManifolds = 64;
	simParams.maxExternVelManifolds = 256;
	simParams.maxInstBehaviors = 1;
	simParams.maxConstraints = 32;

	ms_pPhysSim = new phSimulator;
	ms_pPhysSim->SetActiveInstance();
	ms_pPhysSim->Init(ms_pLevelNew, simParams);

	grcDevice::InitSingleton();

	// initialise material manager

	GtaMaterialMgr::Create();

	//phMaterialMgrImpl<phMaterial>::Create();

	gv = new configParser::ConfigGameView();
		
	TCHAR commonPath[_MAX_PATH] = {0};
	gv->GetCommonDir( commonPath, _MAX_PATH );
	TCHAR assetPath[_MAX_PATH] = {0};
	gv->GetAssetsDir( assetPath, _MAX_PATH );

	if(!m_FragManager)
	{
		m_FragManager = new fragManager();

		fragTune::Instantiate();
		static TCHAR assPath[_MAX_PATH] = "";
		strcpy_s( assPath, _MAX_PATH, assetPath );
		strcat_s( assPath, _MAX_PATH, "/fragments" );
		FRAGTUNE->SetForcedTuningPath(assPath);

		static TCHAR fragPath[_MAX_PATH] = "";
		strcpy_s( fragPath, _MAX_PATH, commonPath );
		strcat_s( fragPath, _MAX_PATH, "/data/fragment.xml" );
		FRAGTUNE->Load(fragPath);
	}

	crAnimation::InitClass();
	crClip::InitClass();
	crpmParameterizedMotion::InitClass();
	crExpressions::InitClass();
	crFrameFilterDictionary::InitClass();

	grmModelFactory::CreateStandardModelFactory();
	grmShaderFactory::CreateStandardShaderFactory();

	pgDictionaryBase::SetListOwnerThread(g_CurrentThreadId);
	// Init breakable glass
	{
		TCHAR glassPath[_MAX_PATH] = "";
		strcpy_s( glassPath, _MAX_PATH, commonPath );
		strcat_s( glassPath, _MAX_PATH, "/data/glass" );
		ASSET.PushFolder( glassPath );
		static int s_GlassBufferMem = 1200;//__PS3?600:1200; // size in kilobytes
		static int s_GlassHeapMem = 127;
		const int kGlassCountMax = 12;
		bgGlassManager::InitClass();
		ASSET.PopFolder();
	}

	// phBound::SetBoundFlag(phBound::NOT_OCTREE_AS_BVH, false); no longer rqrd

	phBoundComposite::CompositeBoundTypeAndIncludeFlagsHook functor;
	functor.Reset<&ConvertResourceBoundFlagsToRuntimeTypeFlags>();
	phBoundComposite::SetTypeAndIncludeFlagsHook(functor);
}

void RageBuilder::Close()
{
	//string_list().swap();

	if(ms_pPlatform)
	{
		ms_pPlatform->EndBuild(); // resets...
		ms_pPlatform->Shutdown();

		delete ms_pPlatform;
		ms_pPlatform = NULL;
	}

	if(m_FragManager)
	{
		fragTune::Destroy();

		delete m_FragManager;
		m_FragManager = NULL;
	}

	if(ms_pPhysSim)
	{
		delete ms_pPhysSim;
		ms_pPhysSim = NULL;
	}

	if(ms_pLevelNew)
	{
		ms_pLevelNew->Shutdown();
		delete ms_pLevelNew;
		ms_pLevelNew = NULL;
	}

	delete gv;
}

void RageBuilder::LoadTextureList(const char* filename)
{
	fiStream *S = ASSET.Open(filename,"");
	int lineCount = 0;
	char buf[RAGE_MAX_PATH];
	while (fgetline(buf,sizeof(buf),S))
		++lineCount;
	const char **inputs = Alloca(const char*,lineCount);
	S->Seek(0);
	lineCount = 0;
	while (fgetline(buf,sizeof(buf),S)) {
		inputs[lineCount] = Alloca(char,strlen(buf)+1);
		strcpy((char*)inputs[lineCount], buf);
		++lineCount;
	}
	S->Close();
}

// sysMemExternalAllocator* RageBuilder::CreateFixedAllocator(uint32 address, uint32 size, uint32 chunkSize, uint32 align, uint32 nodeCount /* = 0 */)
// {
// 	if (!nodeCount)
// 		nodeCount = size / 256;
//
// 	Assert(address % align == 0);
//
// 	sysMemExternalAllocator* allocator = new sysMemExternalAllocator(
// 			size,
// 			(void*) address,
// 			nodeCount,
// 			new char[COMPUTE_WORKSPACE_SIZE(nodeCount)],
// 			chunkSize);
//
// 	return allocator;
// }
//
// void RageBuilder::ReleaseFixedAllocator(sysMemExternalAllocator*& allocator)
// {
// 	// we don't release the heap itself
// 	delete [] (char*) allocator->GetWorkspace();
// 	delete allocator;
// 	allocator = NULL;
//}

char s_Buildname[RAGE_MAX_PATH];
void RageBuilder::BeginBuild(const char* name, const char* ext, bool secondPass, int heapSize,size_t virtualChunkSize /* = 0 */,size_t physicalChunkSize /* = 0 */,int nodeCount /* = 0 */)
{
	sprintf(s_Buildname, "%s.%s", ASSET.FileName(name), ext);
	//ms_virtualFixedAllocator = CreateFixedAllocator(FIXED_VIRTUAL_ADDRESS, FIXED_VIRTUAL_SIZE, 0, FIXED_VIRTUAL_ALIGN);
	//ms_physicalFixedAllocator = CreateFixedAllocator(FIXED_PHYSICAL_ADDRESS, FIXED_PHYSICAL_SIZE, 0, FIXED_PHYSICAL_ALIGN);

	//pgRscBuilder::BeginBuild(*ms_virtualFixedAllocator, *ms_physicalFixedAllocator, false);
	pgRscBuilder::SetBuildName(s_Buildname);
	pgRscBuilder::BeginBuild(secondPass, DEFAULT_BUILD_SIZE, virtualChunkSize, physicalChunkSize);
}

bool RageBuilder::SaveBuild(const char* filename, const char* extension, int version)
{
	size_t filesize = 0;
	bool rt = pgRscBuilder::SaveBuild(filename, extension, version, &filesize);
	CheckFileSize(filename, extension, filesize);

	if (g_useVirtualMemory != 0)
	{
		Errorf("g_useVirtualMemory badness");
	}

	if (g_usePhysicalMemory != 0)
	{
		Errorf("g_usePhysicalMemory badness");
	}

	return rt;
}

void RageBuilder::EndBuild(pgBase *toplevelPointer)
{
	pgRscBuilder::EndBuild(toplevelPointer);

	//	ReleaseFixedAllocator(ms_physicalFixedAllocator);
	//	ReleaseFixedAllocator(ms_virtualFixedAllocator);
}

// Set the target platform.
bool RageBuilder::SetPlatform(PLATFORM ePlatform)
{
	if( ePlatform != ms_ePlatform )
	{
		if (ms_pPlatform)
		{
			ms_pPlatform->Shutdown();
			delete ms_pPlatform;
			ms_pPlatform = NULL;
		}

		ms_ePlatform = ePlatform;

		switch( ePlatform )
		{
		case pltWin32:
		case pltWin64:
			ms_pPlatform = new RagePlatformWin32();
			break;
		case pltDurango:
			ms_pPlatform = new RagePlatformDurango();
			break;
		case pltOrbis:
			ms_pPlatform = new RagePlatformOrbis();
			break;
#if !__64BIT
		case pltPs3:
			ms_pPlatform = new RagePlatformPs3();
			break;
#endif
		case pltIndependent:
			ms_pPlatform = new RagePlatformIndependent();
			break;
#if !__64BIT
		case pltXenon:
			ms_pPlatform = new RagePlatformXenon();
			break;
#endif
		default:
			Errorf( "Unknown platform" );
			return false;
			break;
		}

		ms_pPlatform->Init();

		// DHM FIX ME: this sucks as we should just use ::rage::g_sysPlatform.
		// We have a platform abstraction in RageCore, Ragebuilder and Texture Conversion! Argh.
		// That is for another day.
		CTextureConversionParams::SetPlatform( ms_ePlatform );
	}

	return true;
}


// Get the target platform.
RageBuilder::PLATFORM RageBuilder::GetPlatform()
{
	return ms_ePlatform;
}


// Get the current target platform.
RagePlatform* RageBuilder::GetPlatformImpl()
{
	return ms_pPlatform;
}

void RageBuilder::SetCompressionType(COMPRESSION_TYPE eType)
{
	ms_compressionType = eType;
}

RageBuilder::COMPRESSION_TYPE RageBuilder::GetCompressionType()
{
	return ms_compressionType;
}

static char aExtensions[RageBuilder::pltIndependent + 1][RageBuilder::fiUnknown][7] =
{
	// pltNeutral
	{
			"cd", // fiClipDictionary 
			"bd", // fiBoundsDictionary,
			"dd", // fiDrawableDictionary,
			"pd", // fiPhysicsDictionary,
			"td", // fiTextureDictionary,
			"sd", // fiBlendShapeDictionary,
			"ld", // fiClothDictionary,
			"dr", // fiDrawable,
			"ft", // fiFragment,
			"bn", // fiBounds,
			"nd", // fiPathRegion
			"nv", // fiNavMesh
			"hn", // fiHierarchicalNavData
			"wr", // fiWaypointRecording
			"bs", // fiBlendShape
			"ide", // fiDefinitions
			"ipl", // fiPlacements
			"ipt",// fiEffect
			"vr",  // fiVehicleRecording
			"pm",  // fiParamMotionDictionary
			"ed",  // fiExpressionsDictionary
			"am",  // fiAudMesh
			"meta",// meta
			"map", // fiMapData
			"typ", // fiMapTypes
			"mf",  // fiManifest
			"sco", // scrProgram
			"nh",  // fiHeightMesh
			"fd",  // fiFrameFilterDictionary
			"ldb", // fiTextDatabase
			"cutxml" // fiCutfile
	},
	// pltWin32
	{
			"wcd", // fiClipDictionary
			"wbd", // fiBoundsDictionary,
			"wdd", // fiDrawableDictionary,
			"wpd", // fiPhysicsDictionary,
			"wtd", // fiTextureDictionary,
			"wsd", // fiBlendShapeDictionary,
			"wld", // fiClothDictionary,
			"wdr", // fiDrawable,
			"wft", // fiFragment,
			"wbn", // fiBounds,
			"wnd", // fiPathRegion
			"wnv", // fiNavMesh
			"whn", // fiHierarchicalNavData
			"wwr", // fiWaypointRecording
			"wbs", // fiBlendShape
			"ide", // fiDefinitions,
			"wpl",  // fiPlacements,
			"wpt", //fiEffect,
			"wvr",  // fiVehicleRecording
			"wpm", // fiParamMotionDictionary
			"wed", // fiExpressionsDictionary
			"wam", // fiAudMesh
			"wmt", // meta
			"wmap", // fiMapData
			"wtyp", // fiMapTypes
			"wmf",  // fiManifest
			"wsc", // scrProgram
			"wnh", // fiHeightMesh
			"wfd", // fiFrameFilterDictionary
			"wldb", // fiTextDatabase
			"cut" // fiCutfile
		},
		// pltWin64
		{
			"ycd", // fiClipDictionary
			"ybd", // fiBoundsDictionary,
			"ydd", // fiDrawableDictionary,
			"ypd", // fiPhysicsDictionary,
			"ytd", // fiTextureDictionary,
			"ysd", // fiBlendShapeDictionary,
			"yld", // fiClothDictionary,
			"ydr", // fiDrawable,
			"yft", // fiFragment,
			"ybn", // fiBounds,
			"ynd", // fiPathRegion
			"ynv", // fiNavMesh
			"yhn", // fiHierarchicalNavData
			"ywr", // fiWaypointRecording
			"ybs", // fiBlendShape
			"ide", // fiDefinitions,
			"ypl",  // fiPlacements,
			"ypt", //fiEffect,
			"yvr",  // fiVehicleRecording
			"ypm", // fiParamMotionDictionary
			"yed", // fiExpressionsDictionary
			"yam", // fiAudMesh
			"ymt", // meta
			"ymap", // fiMapData
			"ytyp", // fiMapTypes
			"ymf",  // fiManifest
			"ysc", // scrProgram
			"ynh", // fiHeightMesh
			"yfd", // fiFrameFilterDictionary
			"yldb", // fiTextDatabase
			"cut" // fiCutfile
		},
		// pltXenon
		{
			"xcd", // fiClipDictionary
			"xbd", // fiBoundsDictionary,
			"xdd", // fiDrawableDictionary,
			"xpd", // fiPhysicsDictionary,
			"xtd", // fiTextureDictionary,
			"xsd", // fiBlendShapeDictionary,
			"xld", // fiClothDictionary,
			"xdr", // fiDrawable,
			"xft", // fiFragment,
			"xbn", // fiBounds,
			"xnd", // fiPathRegion
			"xnv", // fiNavMesh
			"xhn", // fiHierarchicalNavData
			"xwr", // fiWaypointRecording
			"xbs", // fiBlendShape
			"ide", // fiDefinitions,
			"xpl",  // fiPlacements,
			"xpt", //fiEffect,
			"xvr",  // fiVehicleRecording
			"xpm", // fiParamMotionDictionary
			"xed", // fiExpressionsDictionary
			"xam", // fiAudMesh
			"xmt", // meta
			"xmap", // fiMapData
			"xtyp",	// fiMapTypes
			"xmf",  // fiManifest
			"xsc", // scrProgram
			"xnh", // fiHeightMesh
			"xfd", // fiFrameFilterDictionary
			"xldb", // fiTextDatabase
			"cut" // fiCutfile
		},
		// pltPs3
		{
			"ccd", // fiClipDictionary
			"cbd", // fiBoundsDictionary,
			"cdd", // fiDrawableDictionary,
			"cpd", // fiPhysicsDictionary,
			"ctd", // fiTextureDictionary,
			"csd", // fiBlendShapeDictionary,
			"cld", // fiClothDictionary,
			"cdr", // fiDrawable,
			"cft", // fiFragment,
			"cbn", // fiBounds,
			"cnd", // fiPathRegion
			"cnv", // fiNavMesh
			"chn", // fiHierarchicalNavData
			"cwr", // fiWaypointRecording
			"cbs", // fiBlendShape
			"ide", // fiDefinitions,
			"cpl",  // fiPlacements,
			"cpt", //fiEffect,
			"cvr",  // fiVehicleRecording
			"cpm", // fiParamMotionDictionary
			"ced", // fiExpressionsDictionary
			"cam", // fiAudMesh
			"cmt", // meta
			"cmap", // fiMapData
			"ctyp", // fiMapTypes
			"cmf",  // fiManifest
			"csc", // scrProgram
			"cnh", // fiHeightMesh
			"cfd", // fiFrameFilterDictionary
			"cldb", // fiTextDatabase
			"cut" // fiCutfile
		},
		// durango
		{
			"dcd", // fiClipDictionary
			"dbd", // fiBoundsDictionary,
			"ddd", // fiDrawableDictionary,
			"dpd", // fiPhysicsDictionary,
			"dtd", // fiTextureDictionary,
			"dsd", // fiBlendShapeDictionary,
			"dld", // fiClothDictionary,
			"ddr", // fiDrawable,
			"dft", // fiFragment,
			"dbn", // fiBounds,
			"dnd", // fiPathRegion
			"dnv", // fiNavMesh
			"dhn", // fiHierarchicalNavData
			"dwr", // fiWaypointRecording
			"dbs", // fiBlendShape
			"dde", // fiDefinitions,
			"dpl",  // fiPlacements,
			"dpt", //fiEffect,
			"dvr",  // fiVehicleRecording
			"dpm", // fiParamMotionDictionary
			"ded", // fiExpressionsDictionary
			"dam", // fiAudMesh
			"dmt", // meta
			"dmap", // fiMapData
			"dtyp", // fiMapTypes
			"dmf",  // fiManifest
			"dsc", // scrProgram
			"dnh", // fiHeightMesh
			"dfd", // fiFrameFilterDictionary
			"dldb", // fiTextDatabase
			"cut" // fiCutfile
		},
		// orbis
		{
			"ocd", // fiClipDictionary
			"obd", // fiBoundsDictionary,
			"odd", // fiDrawableDictionary,
			"opd", // fiPhysicsDictionary,
			"otd", // fiTextureDictionary,
			"osd", // fiBlendShapeDictionary,
			"old", // fiClothDictionary,
			"odr", // fiDrawable,
			"oft", // fiFragment,
			"obn", // fiBounds,
			"ond", // fiPathRegion
			"onv", // fiNavMesh
			"ohn", // fiHierarchicalNavData
			"owr", // fiWaypointRecording
			"obs", // fiBlendShape
			"ode", // fiDefinitions,
			"opl",  // fiPlacements,
			"opt", //fiEffect,
			"ovr",  // fiVehicleRecording
			"opm", // fiParamMotionDictionary
			"oed", // fiExpressionsDictionary
			"oam", // fiAudMesh
			"omt", // meta
			"omap", // fiMapData
			"otyp", // fiMapTypes
			"omf",  // fiManifest
			"osc", // scrProgram
			"onh", // fiHeightMesh
			"ofd", // fiFrameFilterDictionary
			"oldb", // fiTextDatabase
			"cut" // fiCutfile
		},
		// pltIndependent
		{
			"icd", // fiClipDictionary
			"ibd", // fiBoundsDictionary,
			"idd", // fiDrawableDictionary,
			"ipd", // fiPhysicsDictionary,
			"itd", // fiTextureDictionary,
			"isd", // fiBlendShapeDictionary,
			"ild", // fiClothDictionary,
			"idr", // fiDrawable,
			"ift", // fiFragment,
			"ibn", // fiBounds,
			"ind", // fiPathRegion
			"inv", // fiNavMesh
			"ihn", // fiHierarchicalNavData
			"iwr", // fiWaypointRecording
			"ibs", // fiBlendShape
			"ide", // fiDefinitions,
			"ipl",  // fiPlacements,
			"ipt", //fiEffect,
			"ivr",  // fiVehicleRecording
			"ipm", // fiParamMotionDictionary
			"ied", // fiExpressionsDictionary
			"iam", // faAudMesh
			"meta", // meta
			"imap", // fiMapData
			"ityp", // fiMapTypes
			"imf",  // fiManifest
			"sco", // scrProgram
			"inh", // fiHeightMesh
			"ifd", // fiFrameFilterDictionary
			"ildb", // fiTextDatabase
			"cutxml" // fiCutfile
		}
};

// Return a platform specific extension for a file type.
const char* RageBuilder::GetPlatformExtension(const char* extension)
{
	return ConvertExtensionToPlatform( extension, ms_ePlatform );
}


const char* RageBuilder::ConvertExtensionToPlatform(const char* extension, PLATFORM ePlatform)
{
	PLATFORM eSourcePlatform = GetPlatformForExtension(extension);

	for (int filetype = fiClipDictionary; filetype < fiUnknown; ++filetype) // don't include fiUnknown
	{
		if( stricmp(extension, aExtensions[eSourcePlatform][filetype]) == 0 )
		{
			return aExtensions[ePlatform][filetype];
		}
	}

	return extension;
}


RageBuilder::PLATFORM RageBuilder::GetPlatformForExtension(const char* ext)
{
	for (int platform = pltWin32; platform <= pltIndependent; ++platform) // dont include neutral
	{
		for (int filetype = fiClipDictionary; filetype < fiUnknown; ++filetype) // don't include fiUnknown
		{
			if( stricmp(ext, aExtensions[platform][filetype]) == 0 )
			{
				return (RageBuilder::PLATFORM) platform;
			}
		}
	}

	return pltNeutral;
}


bool RageBuilder::DoesExtensionMatchPlatform(const char* extension)
{
	for (int filetype = fiClipDictionary; filetype < fiUnknown; ++filetype) // don't include fiUnknown
	{
		if( stricmp(extension, aExtensions[ms_ePlatform][filetype]) == 0 )
		{
			return true;
		}
	}

	RageBuilder::PLATFORM ePlatform = GetPlatformForExtension( extension );

	return (ePlatform == pltNeutral);
}



//
//	RagePlatform class
//

grmSetup*			gs_pSetup = NULL;


RagePlatform::RagePlatform()
{
	//this->Init();
}

RagePlatform::~RagePlatform()
{
	//Shutdown();
}

// Initialise rage.
bool RagePlatform::InitRage()
{
	return true;
}


void RagePlatform::ShutdownRage()
{
	// do nothing
}

void RagePlatform::SetAutoTexDict(bool autoTexDict)
{
	grmShaderGroup::SetAutoTexDict(autoTexDict);
}

void RagePlatform::RegisterTextureName(const char* pTexname)
{
	grcTextureFactory::GetInstance().RegisterTextureReference(pTexname, grcTexture::None);
}

const char* RagePlatform::GetTuningFolder()
{
	TCHAR *buffer = new TCHAR[RAGE_MAX_PATH];
	strcpy_s(buffer, RAGE_MAX_PATH, GetAssetsPath());
	strcat_s(buffer, RAGE_MAX_PATH, "/fragments");
	NormalisePath(buffer, RAGE_MAX_PATH, buffer);
	return buffer;
}

void RagePlatform::BaseInit()
{
#if __RESOURCECOMPILER
	pgRscBuilder::ConfigureBuild();
#endif

	grcEffect::InitClass();

	InitFactories();

	FreezeRefCounting();

	SetCompressResources(ms_CompressResources);
}

void RagePlatform::InitFactories()
{
	if(ms_InitedFactories) return;

	ms_InitedFactories = true;

	// initialise factories
	grmModelFactory::CreateStandardModelFactory();
	grmShaderFactory::CreateStandardShaderFactory();
	//	grmShaderFactoryRagebuilder::SetActiveInstance();

	//grmShaderGroup::SetDeferredLoad(true, ms_shaderpath, ms_shaderdbpath);

	//grmShaderFactory::GetInstance().PreloadShaders(grmShaderGroup::GetDeferredShaderPath());
	grmShaderFactory::GetInstance().PreloadShaders(ms_shaderpath);
	//grmShaderPreset::InitClass();
	//grmShaderPreset::Preload(ms_shaderdbpath);
	grcMaterialLibrary::SetCurrent(grcMaterialLibrary::Preload(ms_shaderdbpath));

}

void RagePlatform::Shutdown()
{
	GtaData::reset();

	if (grcMaterialLibrary::GetCurrent()) {
		grcMaterialLibrary::SetCurrent(NULL)->Release();
		grcEffect::ShutdownClass();
	}

	if(ms_InitedFactories)
	{
		ms_InitedFactories = false;
		// grcEffect::UnloadAll();
		delete &grmShaderFactory::GetInstance();
		delete &grmModelFactory::GetInstance();
		delete &grcTextureFactory::GetInstance();
	}

	if (ms_pExternalParser)
	{
		ms_pExternalParser->Uninitialize();
		delete ms_pExternalParser;
		ms_pExternalParser = NULL;
	}

	if (ms_pReferenceParser)
	{
		ms_pReferenceParser->Uninitialize();
		delete ms_pReferenceParser;
		ms_pReferenceParser = NULL;
	}

	//	phOctreeCell::ClassReset();

#ifdef INIT_GFX
	if(gs_pSetup)
	{
		// gpSetup->DestroyFactories();
		gs_pSetup->EndGfx();
		gs_pSetup->Shutdown();
		delete gs_pSetup;
		gs_pSetup = NULL;
	}
#endif
}


void RagePlatform::FreezeRefCounting()
{
	// setup physics reference count
	// after freezing the refcounting, you're not allowed to touch it again
	// however, there is no way to inquire from rage whether that has been done...
	// hence... the bHaveBeenHereBefore thing.
	static bool bHaveBeenHereBefore = false;
	if( !bHaveBeenHereBefore )
	{
		rage::phConfig::EnableRefCounting();
		rage::phConfig::FreezeRefCounting();
		bHaveBeenHereBefore = true;
	}
}

bool RagePlatform::SetRootDirectory(const char* path)
{
	ASSET.SetPath(path);
	return true;
}

void RagePlatform::SetEffectPath(const char* effectpath)
{
	strncpy(ms_effectpath, effectpath, RAGE_MAX_PATH - 1);
	PARAM_rmptfxpath.Set(ms_effectpath);
}

void RagePlatform::SetShaderPath(const char* shaderpath)
{
	strncpy(ms_shaderpath, shaderpath, RAGE_MAX_PATH - 1);

#ifdef INIT_GFX
	grcEffect::SetDefaultPath(ms_shaderpath);
#endif

	// grmShaderGroup::SetDeferredLoad(true, ms_shaderpath, ms_shaderpath);
}

void RagePlatform::SetShaderDbPath(const char* shaderdbpath)
{
	strncpy(ms_shaderdbpath, shaderdbpath, RAGE_MAX_PATH - 1);

	// grmShaderGroup::SetDeferredLoad(true, ms_shaderpath, ms_shaderdbpath);
}

void RagePlatform::SetBuildPath(const char* buildpath)
{
	strncpy(ms_BuildPath, buildpath, RAGE_MAX_PATH - 1);
}

void RagePlatform::SetAssetsPath(const char* assetspath)
{
	strncpy(ms_AssetsPath, assetspath, RAGE_MAX_PATH - 1);
}

void RagePlatform::SetCoreAssetsPath(const char* assetspath)
{
	strncpy(ms_CoreAssetsPath, assetspath, RAGE_MAX_PATH - 1);
}

void RagePlatform::SetMetadataDefinitionsPath(const char* definitionspath)
{
	strncpy(ms_MetadataDefinitionsPath, definitionspath, RAGE_MAX_PATH - 1);
}

void RagePlatform::SetCompressResources(bool enable)
{
	ms_CompressResources = enable;
	PARAM_uncompressed.Set(enable?0:"true"); // yeah I know...
}

void RagePlatform::SetTriStripping(bool enable)
{
	//	PARAM_vertexopt.Set(enable?0:"true"); // yeah I know...
}

s32 RagePlatform::GetMinTextureSize()
{
	return grcImage::GetMinMipSize();
}

s32 RagePlatform::GetMaxTextureSize()
{
	return grcImage::GetMaxMipSize();
}

float RagePlatform::GetTextureRatio()
{
	return grcImage::GetSizeRatio();
}

s32 RagePlatform::GetMaxMipMapLevels()
{
	return grcImage::GetMaxMipLevels();
}

void RagePlatform::SetMaxMipMapLevels(s32 MaxMipMapCount)
{
	grcImage::SetMaxMipLevels(MaxMipMapCount);
}

void RagePlatform::SetMinTextureSize(s32 MinTextureSize)
{
	grcImage::SetMinMipSize(MinTextureSize);
}

void RagePlatform::SetMaxTextureSize(s32 MaxTextureSize)
{
	grcImage::SetMaxMipSize(MaxTextureSize);
}

void RagePlatform::SetTextureRatio(float TextureRatio)
{
	grcImage::SetSizeRatio(TextureRatio);
}

void RagePlatform::ResetBuildVars()
{
	ms_CompressResources = false;
	SetMinTextureSize(1);
	SetMaxTextureSize(-1);
	SetTextureRatio(1.0f);
	SetMaxMipMapLevels(-1);
}

void RagePlatform::PushBuildVars()
{
	if(m_LimitStackPos < MAX_STACK)
	{
		m_LimitStack[m_LimitStackPos].CompressResources = ms_CompressResources;
		m_LimitStack[m_LimitStackPos].MinTextureSize = GetMinTextureSize();
		m_LimitStack[m_LimitStackPos].MaxTextureSize = GetMaxTextureSize();
		m_LimitStack[m_LimitStackPos].TextureScale = GetTextureRatio();
		m_LimitStack[m_LimitStackPos].MaxMipMapCount = GetMaxMipMapLevels();
		m_LimitStackPos++;
	}
}

void RagePlatform::PopBuildVars()
{
	if(m_LimitStackPos > 0)
	{
		m_LimitStackPos--;
		ms_CompressResources = m_LimitStack[m_LimitStackPos].CompressResources;
		SetMinTextureSize(m_LimitStack[m_LimitStackPos].MinTextureSize);
		SetMaxTextureSize(m_LimitStack[m_LimitStackPos].MaxTextureSize);
		SetTextureRatio(m_LimitStack[m_LimitStackPos].TextureScale);
		SetMaxMipMapLevels(m_LimitStack[m_LimitStackPos].MaxMipMapCount);
	}
}

bool RagePlatform::GetActualTexPath(const std::string& r_in,std::string& r_out)
{
	r_out = r_in;

	if(RagePlatform::GetOverloadTextureSrc() == "")
		return false;

	char filenoext[MAX_PATH];
	char realfilename[MAX_PATH];
	ASSET.RemoveExtensionFromPath( filenoext, MAX_PATH, r_in.c_str());
	const char* file = ASSET.FileName( filenoext );

	sprintf(realfilename,"%s%s.dds",RagePlatform::GetOverloadTextureSrc().c_str(),file);

	if(ASSET.Exists(realfilename,""))
	{
		r_out = realfilename;		
		return true;
	}

	return false;
}

bool RagePlatform::StartBuild()
{
	//	InitFactories();
	return EndBuild();
}

bool RagePlatform::EndBuild()
{
	ms_TextureIgnoreList.clear();
	ms_TextureList.clear();
	m_inputsAreTCLFiles = false;
	ms_AnimList.clear();
	ms_ClipList.clear();
	ms_ParamMotionList.clear();
	ms_ExpressionsList.clear();
	ms_ModelList.clear();
	ms_BlendShapeList.clear();
	ms_FragmentList.clear();
	ms_BoundList.clear();
	ms_PathRegionList.clear();
	ms_NavMeshList.clear();
	ms_HeightMeshList.clear();
	ms_HierarchicalNavList.clear();
	ms_WaypointRecordingList.clear();
	ms_EffectList.clear();
	ms_VehicleRecordingList.clear();
	ms_AudMeshList.clear();
	ms_MetaList.clear();
	ms_ScriptList.clear();
	GtaData::reset();
	ms_NMData = "";
	ms_FrameFilterList.clear();

	return true;
}

bool RagePlatform::LoadProceduralDefs(const char* materials)
{
	GtaMaterialMgr* p_GtaMtlMgr = dynamic_cast<GtaMaterialMgr*>(&MATERIALMGR);

	if(!p_GtaMtlMgr)
		return false;

	p_GtaMtlMgr->LoadProcedurals(materials);

	return true;
}

bool RagePlatform::LoadMaterialDefs(const char* materials)
{
	GtaMaterialMgr* p_GtaMtlMgr = dynamic_cast<GtaMaterialMgr*>(&MATERIALMGR);

	if(!p_GtaMtlMgr)
		return false;

	p_GtaMtlMgr->LoadMaterials(materials);

	return true;
}

bool RagePlatform::LoadTextures( const char* filefilter, int& count )
{
	Assertf( strlen( filefilter ) > 0, "Empty filefilter for LoadTextures.  Internal error?" );
	if ( 0 == strlen( filefilter ) )
	{
		Errorf( "Empty filefilter for LoadTextures.  Lua script error?" );
		return ( false );
	}
	if ( 0 != ms_TextureList.size() )
	{
		Warningf( "LoadTextures loading into an already populated texture list.  Bug?" );
	}

	// New uber-load textures method where TCL files are king; and we avoid loading
	// duplicates so we can cope with TCL/DDS/TIF of the same name etc.
	char tclFileFilter[MAX_PATH] = {0};
	char ddsFileFilter[MAX_PATH] = {0};
	char tifFileFilter[MAX_PATH] = {0};
	sprintf( tclFileFilter, "%s*.tcl", filefilter );
	sprintf( ddsFileFilter, "%s*.dds", filefilter );
	sprintf( tifFileFilter, "%s*.tif", filefilter );
	
	// Four scenarios are supported:
	//  1. DDS/TIF only
	//  2. TCL only
	//  3. DDS + TCL
	//  4. TIF + TCL
	
	int prevLoadCount = 0;
	AddFilesToList( ddsFileFilter, ms_TextureList, false, false, true );
	prevLoadCount = ms_TextureList.size();
	rbTracef3( "%s: %d [%d] textures loaded.", ddsFileFilter, 
		ms_TextureList.size(), ms_TextureList.size() );

	AddFilesToList( tifFileFilter, ms_TextureList, false, false, true );
	rbTracef3( "%s: %d [%d] textures loaded.", tifFileFilter, 
		ms_TextureList.size() - prevLoadCount, ms_TextureList.size() );
	prevLoadCount = ms_TextureList.size();
	
	AddFilesToList( tclFileFilter, ms_TextureList, false, false, true );
	if ( prevLoadCount != ms_TextureList.size() )
		m_inputsAreTCLFiles = true;
	rbTracef3( "%s: %d [%d] textures loaded (%d).", tclFileFilter, 
		ms_TextureList.size() - prevLoadCount, ms_TextureList.size(),
		m_inputsAreTCLFiles );

	count = ms_TextureList.size();
	return ( true );
}

bool RagePlatform::LoadAnims(const char* filefilter)
{
	AddFilesToList(filefilter, ms_AnimList, false );
	return true;
}

bool RagePlatform::LoadClips(const char* filefilter)
{
	AddFilesToList(filefilter, ms_ClipList, false );
	return true;
}

bool RagePlatform::LoadParamMotions(const char* filefilter)
{
	AddFilesToList(filefilter, ms_ParamMotionList, false );
	return true;
}

bool RagePlatform::LoadExpressionsList(const char* filefilter)
{
	AddFilesToList(filefilter, ms_ExpressionsList, false );
	return true;
}


bool RagePlatform::LoadModels(const char* filefilter)
{
	AddFilesToList(filefilter, ms_ModelList, false );
	return true;
}


bool RagePlatform::LoadBounds(const char* filefilter)
{
	AddFilesToList(filefilter, ms_BoundList, false );
	return true;
}


bool RagePlatform::LoadFrameFilterList(const char *filefilter)
{
	AddFilesToList(filefilter, ms_FrameFilterList, false );
	return true;
}


//
// name:		LoadObject
// description:	Load a physics bound and archetype
bool LoadObject(const char* pFilename, phArchetype* &pArchetype)
{
	char fileNoExt[MAX_PATH];

	SetRelativePathToFile(pFilename);

	// load bound
	phBound* pBound = phBound::Load(pFilename);
	if(pBound)
	{
		pArchetype = phArchetype::Load(fileNoExt, pBound);
		// if we did not find an archetype then create one for a static object
		if(pArchetype == NULL)
		{
			pArchetype = new phArchetype;
			pArchetype->SetBound(pBound);
			// have to add reference because reference count is initially zero
			// adam reckons we dont need to do this anymore
			//			pArchetype->AddRef();
		}
	}
	else
		pArchetype = NULL;

	//ASSET.PopFolder();

	return (pArchetype != NULL);
}

//
// name:		LoadObject
// description:	Load a physics bound
bool LoadObject(const char* pFilename, phBound* &pBound)
{
	SetRelativePathToFile(pFilename);

	// load drawable

	ASSERT_ONLY(phBoundGeometry::GetBoundNameAssertContextStr() = pFilename);
	pBound = phBound::Load(pFilename);
	ASSERT_ONLY(phBoundGeometry::GetBoundNameAssertContextStr() = NULL);

	// pBound->AddRef();

	if(pBound->GetType() == phBound::COMPOSITE)
	{
		// Confirm that the composite is up-to-date on the bounding boxes of the components.
		Assert(static_cast<phBoundComposite*>(pBound)->CheckCachedMinMaxConsistency());
	}

	//ASSET.PopFolder();

	return (pBound != NULL);
}

//
// name:		LoadObject
// description:	Load a texture
bool LoadObject( const char* pFilename, const char* pPath, int texture_id, TextureList& textures, CTextureConversionSpecification* pSpec )
{
	bool haveBumped = false;
	if(g_sysPlatform == platform::ORBIS)
	{
		//if headblend texture, ped tint texture, script RT or sysmem texture, use virtual memory
		if(strstr(pPath, "mp_headtargets"))
		{
			g_useVirtualMemory++;
			haveBumped = true;
		}
		else if(strstr(pFilename, "script_rt_"))
		{
			g_useVirtualMemory++;
			haveBumped = true;
		}
		else if(stricmp(ASSET.FileName(pFilename), "mp_crewpalette.dds") == 0)
		{
			g_useVirtualMemory++;
			haveBumped = true;
		}
	}


	// if pFilename is in ms_TextureIgnoreList and NOT in ms_TextureList, 
	// then skip it
	{
		const string_list& list = RageBuilder::GetPlatformImpl()->GetTextureIgnoreList();

		for (string_list::const_iterator it = list.begin(); it != list.end(); ++it)
		{
			if (stricmp(ASSET.FileName(pFilename), ASSET.FileName((*it).c_str())) == 0)
			{
				textures.clear( );
				return (true);
			}
		}
	}

	char filenoext[RAGE_MAX_PATH];
	const char* name = NULL;

	sysMemStartTemp();
	std::string* actualpath = new std::string;
	RagePlatform::GetActualTexPath(std::string(pFilename),*actualpath);
	pFilename = (*actualpath).c_str();
	sysMemEndTemp();

	bool isLimitExempt = false;
	string_list& exemptlist = RageBuilder::GetPlatformImpl()->GetLimitExemptTextures();
	string_list::iterator begin = exemptlist.begin();
	string_list::iterator end = exemptlist.end();

	ASSET.RemoveExtensionFromPath( filenoext, MAX_PATH, pFilename );
	name = ASSET.FileName( filenoext );

	while (begin != end)
	{
		if (stricmp(name, begin->c_str()) == 0)
		{
			isLimitExempt = true;
			break;
		}

		begin++;
	}

	if (isLimitExempt)
	{
		RagePlatform::PushBuildVars();
		RagePlatform::ResetBuildVars();
	}

	char pFilenameNormalised[RAGE_MAX_PATH] = "";
	StringNormalize(pFilenameNormalised, pFilename, sizeof(pFilenameNormalised));

	// Pass down the container path; this is to get round the texture caching issues
	// we have seen with same named textures in different dictionaries being converted
	// at the same time.
	CTextureConversionSpecification::Begin( g_sysPlatform, pPath, pSpec );
	TextureFactory::Create( pFilenameNormalised, texture_id, textures );
	CTextureConversionSpecification::End();

	if (isLimitExempt)
	{
		RagePlatform::PopBuildVars();
	}

	sysMemStartTemp();
	delete actualpath;
	sysMemEndTemp();

	if (haveBumped && g_sysPlatform == platform::ORBIS)
	{
		g_useVirtualMemory--;
		if (g_useVirtualMemory < 0)
		{
			Errorf("g_useVirtualMemory underrun");
		}
	}

	return true;
}

//
// name:		LoadObject
// description:	Load an CPathRegion file
bool LoadObject(const char* pFilename, CPathRegion* &pPathRegion)
{
	pPathRegion = new CPathRegion;
	pPathRegion->LoadXML(pFilename);
	return true;
}

//
// name:		LoadObject
// description:	Load an CNavMesh file
bool LoadObject(const char* pFilename, CNavMesh* &pNavMesh)
{
	pNavMesh = CNavMesh::LoadBinary(pFilename);
	return true;
}

//
// name:		LoadObject
// description:	Load an agPolymesh file
bool LoadObject(const char* pFileName, agPolyMesh* &pAudMesh)
{
	pAudMesh = agPolyMesh::Load(pFileName);
	return true;
}

//
// name:		LoadObject
// description:	Load an CHierarchicalNavData file
bool LoadObject(const char* pFilename, CHierarchicalNavData* &pHierNavData)
{
	pHierNavData = CHierarchicalNavData::Load(pFilename);
	return true;
}

//
// name:		LoadObject
// description:	Load an fwWaypointRecordingRoute file
bool LoadObject(const char* pFilename, fwWaypointRecordingRoute* &pRecordingRoute)
{
	pRecordingRoute = fwWaypointRecordingRoute::Load(pFilename);
	return true;
}

#if ENABLE_BLENDSHAPES
//
// name:		LoadObject
// description:	Load a blend shape
bool LoadObject(const char* pFilename, grbTargetManager* &pBlendShape)
{
	SetRelativePathToFile(pFilename);

	// load drawable
	pBlendShape = new grbTargetManager;
	bool rt = pBlendShape->Load(pFilename, NULL);

	//ASSET.PopFolder();

	return rt;
}
#endif // ENABLE_BLENDSHAPES



grmModel::grmMeshPrepareParam DefaultMeshPrepareHookInternal( mshMesh& mesh, const grmShaderGroup *shaderGroup, int i, bool isFrag )
{
	u16 shaderIndex=(u16)-1;
	if (mesh.GetMtl(i).Name[0] == '#')
		shaderIndex = (u16) atoi(mesh.GetMtl(i).Name + 1);
	else
		shaderIndex = (u16) atoi(mesh.GetMtl(i).Name);

	const char* mtlfilename = shaderGroup->GetShader(shaderIndex).GetMtlFileName();

	bool bIsTreeGeometry=false;
	bool bIsTreeLodGeometry=false;
	bool bPedNeedsWetHack=false;
	bool bIsUMovementTex=false;
	bool bIsPed=false;
	bool bIsCameraAligned = false;
	bool bIsWindGeometry = false;
	bool bIsTerrainUberGeometry = false;

	if (mtlfilename)
	{
		if(	!strcmpi(mtlfilename,"trees.sps")								||
			!strcmpi(mtlfilename,"trees_camera_aligned.sps")				||
			!strcmpi(mtlfilename,"trees_camera_facing.sps")					||
			!strcmpi(mtlfilename,"trees_normal.sps")						||
			!strcmpi(mtlfilename,"trees_normal_diffspec.sps")				||
			!strcmpi(mtlfilename,"trees_normal_diffspec_tnt.sps")			||
			!strcmpi(mtlfilename,"trees_normal_spec.sps")					||
			!strcmpi(mtlfilename,"trees_normal_spec_wind.sps")				||
			!strcmpi(mtlfilename,"trees_normal_spec_camera_aligned.sps")	||
			!strcmpi(mtlfilename,"trees_normal_spec_camera_facing.sps")		||
			!strcmpi(mtlfilename,"trees_normal_spec_tnt.sps")				||
			!strcmpi(mtlfilename,"trees_normal_spec_camera_aligned_tnt.sps")||
			!strcmpi(mtlfilename,"trees_normal_spec_camera_facing_tnt.sps")	||
			!strcmpi(mtlfilename,"trees_tnt.sps")							||
			!strcmpi(mtlfilename,"trees_shadow_proxy.sps")					||
			false
			)
		{
			bIsTreeGeometry = true;
		}

		if(	!strcmpi(mtlfilename,"trees_lod.sps")			||
			!strcmpi(mtlfilename,"trees_lod_tnt.sps")		||
			false
			)
		{
			bIsTreeLodGeometry = true;
		}


		if( strstr(mtlfilename, "_wind_") || strstr(mtlfilename, "_wind.sps"))
		{
			bIsWindGeometry = true;
		}
	

		if(strstr(mtlfilename, "terrain_uber_"))
		{
			bIsTerrainUberGeometry = true;
		}

		// AJH :
		// This won't actually get run, since this particular global value will be de-dupped.
		// There's no need to use this anyway anymore, since this was mostly for PS3 where the skining was done outwith
		// the vertex shader .
		//
		//
		// check for peds with wetclothes data. Checking to see if the shader is a ped 
		// and also if the global variable WetClothesData exists in the shader (it should be in every ped shader)
		if(grmShaderGroup::IsPedMaterial(mesh.GetMtl(i).Name , shaderGroup) && !isFrag)
		{
			bIsPed = true;

			if(grcEffect::LookupGlobalVar("WetClothesData",false) != grcegvNONE)				
			{
				bPedNeedsWetHack = shaderGroup->GetShader(shaderIndex).GetDrawBucket()==0;

				if(!strcmpi(mtlfilename,"ped_decal.sps"))
				{
					bPedNeedsWetHack = true;
				}
			}   
		}

		if(	!strcmpi(mtlfilename,"normal_um_tnt.sps")		||
			false
			)
		{
			bIsUMovementTex = true;
		}

		if(grmShaderGroup::IsCameraMaterial(mesh.GetMtl(i).Name, shaderGroup) )
		{
			bIsCameraAligned = true;
		}
	}

	// custom processing for tree CPV's: pack cpv and cpv2 into cpv
	if(bIsTreeGeometry)
	{
		mshMaterial& mat = mesh.GetMtl(i);

		const u32 numVerts = mat.GetVertexCount();
		for(u32 v=0; v<numVerts; v++)
		{
			Vector4 newCPV = mat.GetCpv2(v);	// RGB	= COLOR1.rgb	(um)
			newCPV.w = mat.GetCpv(v).x;			// A	= COLOR0.r		(alpha)
			mat.SetCpv(v, newCPV);

			Vector4 newCPV2(0,0,0,0);
			newCPV2.z = mat.GetCpv2(v).w;		// B	= COLOR1.a		(tint)
			mat.SetCpv2(v, newCPV2);
		}
	}//bIsTreeGeometry...

	if(bIsTreeLodGeometry)
	{
		mshMaterial& mat = mesh.GetMtl(i);

		const u32 numVerts = mat.GetVertexCount();
		for(u32 v=0; v<numVerts; v++)
		{
			Vector4 newCPV = mat.GetCpv(v);		// RGBA	= COLOR0.rgba	(natural+artificial ambient, etc.)
			mat.SetCpv(v, newCPV);

			Vector4 newCPV2(0,0,0,0);
			newCPV2.z = mat.GetCpv2(v).w;		// B	= COLOR1.a		(tint)
			mat.SetCpv2(v, newCPV2);
		}
	}//bIsTreeLodGeometry...

	// shift um control params to texcoord1:
	if(bIsUMovementTex && !isFrag)
	{
		mshMaterial& mat = mesh.GetMtl(i);

		const u32 numVerts = mat.GetVertexCount();
		for(u32 v=0; v<numVerts; v++)
		{
			Vector4 cpv2 = mat.GetCpv2(v);					// cpv2:		um control: Hscale | Vscale | phase
			mat.SetTexCoord(v, 1, Vector2(cpv2.x, cpv2.z));	// texcoord1:	H+Vscale | phase
		}
	}//bIsUMovementTex...

	if (bIsCameraAligned && !isFrag)
	{
		mshMaterial& mat = mesh.GetMtl(i);
		const int iTangentCentreOffset = 1; // Unable to look up this value for shader data unfortunately.
		const int iTexRadius = 1;
		SetBoundCenters(mat, iTangentCentreOffset, iTexRadius);
	}

	if (bIsWindGeometry)
	{
		if (bIsTreeGeometry)
		{
			mshMaterial& mat = mesh.GetMtl(i);

			const u32 numVerts = mat.GetVertexCount();
			for(u32 v=0; v<numVerts; v++)
			{
				Vector4 newCPV = mat.GetCpv2(v);	// B	= COLOR1.b		(tint - done above)
				newCPV.x = mat.GetCpv3(v).x;		// R	= COLOR2.r		(stiffness)
				mat.SetCpv2(v, newCPV);
			}
		}
		else
		{
			mshMaterial& mat = mesh.GetMtl(i);

			const u32 numVerts = mat.GetVertexCount();
			for(u32 v=0; v<numVerts; v++)
			{
				Vector4 newCPV = mat.GetCpv2(v);	// RGB	= COLOR1.rgb	(um)
				newCPV.w = mat.GetCpv3(v).x;		// A	= COLOR2.r		(stiffness)
				mat.SetCpv2(v, newCPV);
			}
		}
	}

	if(bIsTerrainUberGeometry && !isFrag)
	{
		mshMaterial& mat = mesh.GetMtl(i);
		const u32 numVerts = mat.GetVertexCount();
		for(u32 v=0; v<numVerts; v++)
		{
			Vector4 newCPV = mat.GetCpv3(v);	// RGBA = COLOR1.rgba	(blend weight + blend)
			mat.SetTexCoord(v, 6, Vector2(newCPV.x, newCPV.y));
			mat.SetTexCoord(v, 7, Vector2(newCPV.z, newCPV.w));
		}
	}

	grmModel::grmMeshPrepareParam result = grmModel::grcMeshPrepareDefault;
	if( bIsPed && !RagePlatform::GetHeadBlendGeometry() && !isFrag && !RagePlatform::GetMicroMorphMesh())
		result = grmModel::grcMeshPrepareOptimize;

	return result;
}


grmModel::grmMeshPrepareParam DefaultMeshPrepareHook( mshMesh& mesh, const grmShaderGroup *shaderGroup, int i )
{
	return DefaultMeshPrepareHookInternal(mesh, shaderGroup, i, false);
}


grmModel::grmMeshPrepareParam DefaultFragMeshPrepareHook( mshMesh& mesh, const grmShaderGroup *shaderGroup, int i )
{
	return DefaultMeshPrepareHookInternal(mesh, shaderGroup, i, true);
}


// formerly hacked into grModel as CustomMeshPreprocess()
bool DefaultPostProcessMeshLoaderHook( mshMesh& mesh, fiTokenizer &tOrig, const grmShaderGroup *shaderGroup )
{
	fiStream* pStream = tOrig.GetStream();
	if( pStream ) 
	{
		if( !pStream->TellStart() )
		{
			// Reopen the file, so we can scan it again without changing tOrig's state
			fiStream* s = ASSET.Open(tOrig.GetName(), "");
			fiTokenizer t(tOrig.GetName(), s);

			const int MAX_COUNT = 1024 * 8;
			Vector4* weights = Alloca( Vector4, MAX_COUNT );
			int* blendIndices = Alloca( int, MAX_COUNT*4 );


			for(int k = 0; k < mesh.GetMtlCount(); ++k)
			{
				int morphCount = 0;
				if( grmShaderGroup::IsPedClothMaterial( mesh.GetMtl(k).Name, shaderGroup ) )
				{
					BakeClothMorphData( t, weights, blendIndices, morphCount, k, &mesh );

					mshMaterial& mat = mesh.GetMtl(k);
					const int numVerts = (int)mat.GetVertexCount();

					for(int i = 0; i < morphCount; ++i)
					{
						const int offset = i * 4;
						const int idx = blendIndices[offset];
						Assert( idx < numVerts );

						mshBinding newbinding;
						newbinding.Mtx[0] = 255;
						newbinding.Mtx[1] = blendIndices[offset+1];
						newbinding.Mtx[2] = blendIndices[offset+2];
						newbinding.Mtx[3] = blendIndices[offset+3];

						newbinding.Wgt[0] = Min(weights[i].GetX(),1.0f);
						newbinding.Wgt[1] = Min(weights[i].GetY(),1.0f);
						newbinding.Wgt[2] = Min(weights[i].GetZ(),1.0f);
						// Scale it, offset it. pack it.
						newbinding.Wgt[3] = Min((weights[i].GetW() * 10.0f) + 0.5f,1.0f);

						// Special binding, make sure packetization, remap and all ignore those verts
						newbinding.IsPassThrough = true;

						mat.SetBinding( idx, newbinding );
					}
				}
			}

			s->Close();
		}
	}

	return(true);
}

void FragTypeParamHook(fragType::RagebuilderParams* params)
{
	params->lodSkeleton = RagePlatform::GetLodSkeleton();
}

// Drawbucket mask generation/fixup
// See in game Renderer.h for full bucket mask list, once we picked them up.

void FixupDrawBucketMask(rmcDrawable *drawable)
{
	if( !drawable || !drawable->GetLodGroup().ContainsLod(LOD_HIGH) )
	{
		Quitf( "Resource crash, there is no drawable model !" );
	}	
	const grmModel& model = drawable->GetLodGroup().GetLodModel0(LOD_HIGH);
	const grmShaderGroup &shaderGroup = drawable->GetShaderGroup();
	
	int shaderCount = shaderGroup.GetCount();
	for(int i=0;i<shaderCount;i++)
	{
		grmShader &shader = shaderGroup.GetShader(i);
		
		u32 baseMask = shader.GetDrawBucketMask();
		baseMask &= 0xFF;		

		u32 newMask = (0xFF << 8) | baseMask; // Here: do something clever to figure out where we want the drawable.
		
		shader.SetDrawBucketMask(newMask);
	}

	// Regenerate LOD group
	drawable->GetLodGroup().ComputeBucketMask(shaderGroup);
}

//
// name:		LoadObject
// description:	Load a drawable
bool LoadObject(const char* pFilename, gtaDrawable* &pDrawable)
{
	bool haveVirtualBumped = false;
	if(RagePlatform::GetHeadBlendWriteBuffer() && g_sysPlatform == platform::ORBIS)
	{
		haveVirtualBumped = true;
		g_useVirtualMemory++;
	}

	/*
	if((RagePlatform::GetOverloadTextureSrc() != "") && (grmShaderGroup::GetAutoTexDict()))
	{
	char tempPath[MAX_PATH];
	char searchPath[MAX_PATH];
	ASSET.RemoveNameFromPath( tempPath, MAX_PATH, pFilename );
	sprintf(searchPath,"%s/*.dds",tempPath);

	string_list filelist;
	AddFilesToList( searchPath, filelist, true, true );

	for (string_list::iterator it = filelist.begin(); it != filelist.end(); ++it)
	{
	std::string actualpath;
	if(RagePlatform::GetActualTexPath((*it),actualpath))
	{
	rage::grcTextureDefault* t;
	LoadObject(actualpath.c_str(),t);
	}
	}
	}*/

	//ASSET.SetPath(RagePlatform::GetOverloadTextureSrc().c_str());

	grmModel::MeshPrepareHook meshPrepareHook;
	meshPrepareHook.Reset<&DefaultMeshPrepareHook>();
	grmModel::SetMeshPrepareHook(meshPrepareHook);

	grmModel::PreModelHookParams2 postProcessMeshLoaderHook;
	postProcessMeshLoaderHook.Reset<&DefaultPostProcessMeshLoaderHook>();
	grmModel::SetPreModelCreateHook2(postProcessMeshLoaderHook);

	char idefile[MAX_PATH];
	char platformidefile[MAX_PATH];

	strcpy(idefile,RBRemoveNameFromPath(pFilename));
	
	// Build up a path to a platform specific .ide search.
	switch ( RageBuilder::GetPlatform() )
	{
	case RageBuilder::pltWin32:
		formatf(platformidefile, sizeof(platformidefile), "%s/*.win32.ide", idefile);
		break;
	case RageBuilder::pltWin64:
		formatf(platformidefile, sizeof(platformidefile), "%s/*.win64.ide", idefile);
		break;
	case RageBuilder::pltDurango:
		formatf(platformidefile, sizeof(platformidefile), "%s/*.durango.ide", idefile);
		break;
	case RageBuilder::pltOrbis:
		formatf(platformidefile, sizeof(platformidefile), "%s/*.orbis.ide", idefile);
		break;
	case RageBuilder::pltXenon:
		formatf(platformidefile, sizeof(platformidefile), "%s/*.xenon.ide", idefile);
		break;
	case RageBuilder::pltPs3:
		formatf(platformidefile, sizeof(platformidefile), "%s/*.ps3.ide", idefile);
		break;
	case RageBuilder::pltIndependent:
	case RageBuilder::pltNeutral:
	default:
		Warningf( "Unexpected platform during platform ide file resolution." );
	}
	
	strcat(idefile,"/*.ide");

	sysMemStartTemp();
	{
		string_list filelist;
		AddFilesToList( platformidefile, filelist, false, false);

		// First check if we found any platform specific .ide files.
		if (filelist.size() > 0)
		{
			RBClearDefinitions();
			RBLoadDefinitions(filelist.begin()->c_str());
		}
		else
		{
			AddFilesToList( idefile, filelist, false, false);
			RBClearDefinitions();

			if(filelist.size() > 0)
			{
				RBLoadDefinitions(filelist.begin()->c_str());
			}
		}		
	}
	sysMemEndTemp();

	//

	SetRelativePathToFile(pFilename,RagePlatform::GetOverloadTextureSrc().c_str());

	// load drawable
 	char boundfile[MAX_PATH];
 	sprintf(boundfile, "pack:/%s.bnd", RBRemoveExtensionFromPath(s_Buildname));
	bool boundFileExists = ASSET.Exists(boundfile, "");
	bool havePhysicalBumped = false;
	if( boundFileExists == false && g_sysPlatform == platform::WIN64PC &&
		RagePlatform::GetPostProcessVehicles() == false && // We don't do vehicles
		RagePlatform::GetHeadBlendGeometry() == false && // Nor Head Blend Geometry
		RagePlatform::GetHeadBlendWriteBuffer() == false && // Nor Head Blend Write Buffer
		RagePlatform::GetMicroMorphMesh() // And neither micro morph mesh
		)
	{
		havePhysicalBumped = true;
		g_usePhysicalMemory++;
	}

	pDrawable = new gtaDrawable;

	CTextureConversionSpecification::Begin( g_sysPlatform, pFilename );
	bool rt = pDrawable->Load(pFilename);
	CTextureConversionSpecification::End();

	s32 i,Count = GtaData::m_ideSet.getNumEffects();
	CLightAttr set;

	for(i=0;i<Count;i++)
	{
		set = GtaData::m_ideSet.getEffect(i).l;

		pDrawable->m_lights.PushAndGrow(set, (u16) Count);
	}
	int geomCount = 0;
	u32 modelCount = 0;
	if(pDrawable->GetLodGroup().ContainsLod(LOD_HIGH))
	{
		modelCount = pDrawable->GetLodGroup().GetLod(LOD_HIGH).GetCount();
		for(u32 m=0; m<modelCount; m++)
		{
			grmModel *pModel = pDrawable->GetLodGroup().GetLod(LOD_HIGH).GetModel(m);
			Assert(pModel);
			geomCount += pModel->GetGeometryCount();

		}
	}

	// load physics bound
	phBound* pBound = NULL;
	if (boundFileExists)
	{
		LoadObject(boundfile, pBound);
	}

	pDrawable->m_pTintData = PrepackageTint(pDrawable, pFilename);
	pDrawable->m_pPhBound = pBound;
	pDrawable->SetDebugName(s_Buildname);

	if(RageBuilder::GetPlatform()==RageBuilder::pltPs3)
	{
		const int shaderCount = &(pDrawable->GetShaderGroup())? pDrawable->GetShaderGroup().GetCount() : 0;
		if(	pDrawable->GetContainerSize() > DRAWABLE_MAX_CONTAINER_SIZE ||
			modelCount > DRAWABLE_MAX_MODELS ||
			geomCount > DRAWABLE_MAX_GEOMS ||
			shaderCount > DRAWABLE_MAX_SHADERS)
		{
			Displayf("Model complexity of %s high.\n container size (max %d): %d\n models (max %d): %d\n geometries (max %d): %d\n shaders (max %d): %d",
				s_Buildname,
				DRAWABLE_MAX_CONTAINER_SIZE, pDrawable->GetContainerSize(),
				DRAWABLE_MAX_MODELS, modelCount,
				DRAWABLE_MAX_GEOMS, geomCount,
				DRAWABLE_MAX_SHADERS, shaderCount);
			// Let's defer this decision until runtime, except if it's flagged up as a destruction model
			if (RagePlatform::GetPPUOnlyHint())
				pDrawable->MakePpuOnly();
			else
				artErrorf("(model %s is not a destruction model, so this will assert at runtime as well)",s_Buildname);
		}
	}

	FixupDrawBucketMask(pDrawable);
		
	grmModel::RemovePreModelCreateHook();
	grmModel::RemovePreModelCreateHook2();

	if (haveVirtualBumped && g_sysPlatform == platform::ORBIS)
	{
		g_useVirtualMemory--;
		if (g_useVirtualMemory < 0)
		{
			Errorf("g_useVirtualMemory underrun");
		}
	}

	if (havePhysicalBumped && g_sysPlatform == platform::WIN64PC)
	{
		g_usePhysicalMemory--;
		if (g_usePhysicalMemory < 0)
		{
			Errorf("g_usePhysicalMemory underrun");
		}
	}

	return rt;
}

#if ENABLE_BLENDSHAPES
//
// name:		LoadObject
// description:	Load a blend shape
bool LoadObject(const char* pFilename, rmcDrawable* drawable, grbTargetManager* &pBlendShape)
{
	SetRelativePathToFile(pFilename);

	// load drawable
	pBlendShape = new grbTargetManager;
	bool rt = pBlendShape->Load(pFilename, drawable);

	//ASSET.PopFolder();

	return rt;
}
#endif // ENABLE_BLENDSHAPES

//
// name:		LoadObject
// description:	Load an animation

// Handled with a commandline param, because I don't want to introduce differences between the scripting language between 360 & PS3
PARAM(viseme, "Loading visemes is handled differently, no packing or compacting for those bastards"); 
bool LoadObject(const char* pFilename, crAnimation* &pAnim)
{
	if (PARAM_viseme.Get())
	{
		pAnim = crAnimation::AllocateAndLoad(pFilename, NULL, false, false);
	}
	else
	{
		pAnim = crAnimation::AllocateAndLoad(pFilename, NULL, g_sysPlatform == platform::PS3, g_sysPlatform == platform::PS3);
	}

	if(pAnim == NULL)
		return false;
	return true;
}


//
// name:		LoadObject
// description:	Load an animation
bool LoadObject(const char* pFilename, characterCloth* &cCloth)
{
	SetRelativePathToFile(pFilename);

	sysMemStartTemp();
	const char *tuneFilePath = RagePlatform::GetTuningFolder();
	phVerletCloth::sm_tuneFilePath = tuneFilePath;
	sysMemEndTemp();

	bool result = false;
	characterCloth* newCloth = rage_new characterCloth();
	Assert( newCloth );
	newCloth->Load(pFilename);

	if( 1 
#if MESH_LIBRARY
		&& characterCloth::typehascloth
#endif
		)
	{
		cCloth = newCloth;
		result = true;
	}
	else
	{
		delete newCloth;
	}

#if MESH_LIBRARY
	characterCloth::typehascloth = false;
#endif

	return result;
}



//
// name:		LoadObject
// description:	Load an animation
bool LoadObject(const char* pFilename, crExpressions* &pExpressions)
{
	pExpressions = crExpressions::AllocateAndLoad(pFilename, true);

	if(pExpressions == NULL)
		return false;
	return true;
}

//
// name:		LoadObject
// description:	Load a frame filter dictionary
bool LoadObject(const char* pFilename, crFrameFilter* &pFrameFilter)
{
	pFrameFilter = crFrameFilter::AllocateAndLoad(pFilename);

	if(pFrameFilter == NULL)
		return false;
	return true;
}

//
// name:		LoadObject
// description:	Load a text database
bool LoadObject(const char* pFilename, fwTextAsset* &pTextAsset )
{
	INIT_PARSER;

	parSettings s = PARSER.Settings();
	s.SetFlag(parSettings::CULL_OTHER_PLATFORM_DATA, true);
	s.SetFlag(parSettings::READ_SAFE_BUT_SLOW, true);

	sysMemStartTemp();

	//! Read in the database declaration
	rage::fwTextDatabaseSource* pTextDatabase;
	bool parseSuccess = PARSER.LoadObjectPtr( pFilename, "", pTextDatabase, &s );

	sysMemEndTemp();

	if( pTextDatabase )
	{
		if( parseSuccess )
		{
			// Create and populate the actual asset
			pTextAsset = rage_new fwTextAsset();

			Assert( pTextAsset );
			if( pTextAsset )
			{

				bool populatedAsset = fwTextFileConversion::populateTextAsset( *pTextDatabase, *pTextAsset );
				Assertf( populatedAsset, "Failure populating text asset for database %s", pFilename );

				if( !populatedAsset )
				{
					delete pTextAsset;
					pTextAsset = 0;
				}
			}
		}

		sysMemStartTemp();

		delete pTextDatabase;
		pTextDatabase = 0;

		sysMemEndTemp();
	}

	SHUTDOWN_PARSER;

	return pTextAsset != 0;
}

// Name pulled from VehicleFactory.cpp, static ObjectNameIdAssocation carIds[VEH_NUM_NODES + 1]
#define LW_LIGHTCOUNT 20
#define LW_LIGHTDEFAULT 17
static const char * s_BoneNames[LW_LIGHTCOUNT]	=
{
	"headlight_l",			/* 00 */
	"headlight_r",			/* 01 */
	"taillight_l",			/* 02 */
	"taillight_r",			/* 03 */
	"indicator_lf",			/* 04 */
	"indicator_rf",			/* 05 */
	"indicator_lr",			/* 06 */
	"indicator_rr",			/* 07 */
	"brakelight_l",			/* 08 */
	"brakelight_r",			/* 09 */
	"brakelight_m",			/* 10 */
	"reversinglight_l",		/* 11 */
	"reversinglight_r",		/* 12 */
	"extralight_1",			/* 13 */
	"extralight_2",			/* 14 */
	"extralight_3",			/* 15 */
	"extralight_4",			/* 16 */
	"extra_5",				/* 17 */
	"extra_6",				/* 18 */
	"extra_9",				/* 19 */
};

static bool s_NewFragType = false;
static int32 s_boneIDArray[LW_LIGHTCOUNT]; // For default and extras Extras
static fwVehicleGlassConstructorTriangle* s_glassTriangles = NULL;
static unsigned int lodLevel = 0;
static int s_glassTriangleCount = 0;
static bool s_glassTrianglesAreHD = false;
static u32 s_glassTriangleFlags = 0;

//
// name: PostProcessVehiclesMeshLoaderHook
// desc: 
//
static bool PostProcessVehiclesMeshLoaderHook(mshMesh& mesh,const char* meshName, int lod, const grmShaderGroup *)
{
	Displayf( "PostProcessing Vehicle Mesh %s", meshName );

	fragType *currentFragtype = fragType::GetLoadingFragType();
	Assert(currentFragtype);

	const crSkeletonData *pSkeletonData = currentFragtype->GetCommonDrawable()->GetSkeletonData();
	Verifyf(pSkeletonData, "No skeleton data!");

	if( true == s_NewFragType )
	{
		s_NewFragType = false;

		for(int i=0;i<LW_LIGHTCOUNT;i++)
		{
			const crBoneData *pBone = pSkeletonData->FindBoneData(s_BoneNames[i]);
			if( pBone )
			{
				s_boneIDArray[i] = pBone->GetIndex();
			}
			else
			{
				s_boneIDArray[i] = -1;
			}
		}
	}

	fragDrawable* pFragDraw = currentFragtype->GetCommonDrawable( );
	grmShaderGroup& shaderGroup = pFragDraw->GetShaderGroup( );
	int numMaterials = mesh.GetMtlCount( );
	int meshLOD = -1;

	if (strstr(meshName, "wheelmesh") == NULL && strstr(meshName, "_skin.mesh") != NULL) // get mesh LOD from name
	{
		meshLOD = lod;
	}
	
	lodLevel = meshLOD;


#if ADD_PED_SAFE_ZONE_TO_VEHICLES
	//Get the bounding box for the vehicle dimensions
	Vector3 boundingBoxMin = Vector3(+FLT_MAX, +FLT_MAX, +FLT_MAX);
	Vector3 boundingBoxMax = Vector3(-FLT_MAX, -FLT_MAX, -FLT_MAX);

	for( int mtlIdx = 0; mtlIdx < numMaterials; mtlIdx++ )
	{
		mshMaterial& material = mesh.GetMtl( mtlIdx );
		{
			for (int vtxIdx = 0; vtxIdx < material.GetVertexCount(); vtxIdx++)
			{
				Vector3 position = material.GetVertex(vtxIdx).Pos;

				boundingBoxMin.x = Min<float>(position.x, boundingBoxMin.x);
				boundingBoxMin.y = Min<float>(position.y, boundingBoxMin.y);
				boundingBoxMin.z = Min<float>(position.z, boundingBoxMin.z);

				boundingBoxMax.x = Max<float>(position.x, boundingBoxMax.x);
				boundingBoxMax.y = Max<float>(position.y, boundingBoxMax.y);
				boundingBoxMax.z = Max<float>(position.z, boundingBoxMax.z);
			}
		}
	}

	CPedSafeZone pedSafeZone(pSkeletonData, boundingBoxMin, boundingBoxMax);
#endif

	bool bCopyGlassTriangles = false;

	if (meshLOD == 0) // "<vehiclename>_l1_skin.mesh", aka high lod
	{
		if (s_glassTriangles)
		{
			sysMemStartTemp();
			delete[] s_glassTriangles;
			s_glassTriangles = NULL;
			sysMemEndTemp();
		}

		s_glassTriangleCount = 0;
		bCopyGlassTriangles = true;
		s_glassTrianglesAreHD = false;
		s_glassTriangleFlags = 0;
	}

	if (bCopyGlassTriangles)
	{
		Assert(s_glassTriangleCount == 0);

		for (int pass = 0; pass < 2; pass++)
		{
			for( int mtlIdx = 0; mtlIdx < numMaterials; mtlIdx++ )
			{
				mshMaterial& material = mesh.GetMtl( mtlIdx );
				// Materials are named "#([0-9]+)", we just want the index.
				// See ::rage::grmModel::Create as it does it in the same way.
				int shaderIdx = -1;
				if ( '#' == material.Name[0] )
					shaderIdx = atoi( material.Name + 1 );
				else
					shaderIdx = atoi( material.Name );
				grmShader& shader = shaderGroup.GetShader( shaderIdx );

				const char* sShaderName = shader.GetName();

				const bool bIsOuterGlass = (strcmp(sShaderName, "vehicle_vehglass") == 0);
				const bool bIsInnerGlass = (strcmp(sShaderName, "vehicle_vehglass_inner") == 0);

				if (bIsOuterGlass)
				{
					bool bDamageScaleIsNotAllZerosAndOnes = false;

					if (pass == 0)
					{
						float damageScaleMin = +FLT_MAX;
						float damageScaleMax = -FLT_MAX;

						for (int vtxIdx = 0; vtxIdx < material.GetVertexCount(); vtxIdx++)
						{
							const float damageScale = material.GetCpv(vtxIdx).y;

							damageScaleMin = Min<float>(damageScale, damageScaleMin);
							damageScaleMax = Max<float>(damageScale, damageScaleMax);

							if (damageScale != 0.0f &&
								damageScale != 1.0f)
							{
								bDamageScaleIsNotAllZerosAndOnes = true;
							}
						}

						if (bDamageScaleIsNotAllZerosAndOnes)
						{
							// NOTE -- i don't want to stop ragebuilder processing these vehicles, but i do want the string "Error" to occur in the log so i'll see it ..
							Warningf("%s: (Error) material %d uses vehicle_vehglass but damage scale is [%f..%f], expected each vertex be 1 (default) or 0 (exposed)", meshName, mtlIdx, damageScaleMin, damageScaleMax);
							s_glassTriangleFlags |= fwVehicleGlassWindow::VGW_FLAG_ERROR_DAMAGE_SCALE_NOT_01;

							#if 0 // let's report some more information here if needed ..
							for (int vtxIdx = 0; vtxIdx < material.GetVertexCount(); vtxIdx++)
							{
								const float damageScale = material.GetCpv(vtxIdx).y;
								
								if (damageScale != 0.0f &&
									damageScale != 1.0f)
								{
									Warningf("  vtx[%d].cpv.y = %f (%s)", vtxIdx, damageScale, currentFragtype->GetSkeletonData().GetBoneData(material.GetBinding(vtxIdx).Mtx[0])->GetName());
								}
							}
							#endif
						}
					}

					for (mshMaterial::TriangleIterator iter = material.BeginTriangles(); iter != material.EndTriangles(); ++iter)
					{
						int index0;
						int index1;
						int index2;
						iter.GetVertIndices(index0, index1, index2);

						if (index0 == index1 ||
							index1 == index2 ||
							index2 == index0)
						{
							continue;
						}

						if (pass == 0)
						{
							s_glassTriangleCount++;
						}
						else
						{
							const int boneIndex = material.GetBinding(index0).Mtx[0];

							const Vec3V p0 = VECTOR3_TO_VEC3V(material.GetPos(index0));
							const Vec3V p1 = VECTOR3_TO_VEC3V(material.GetPos(index1));
							const Vec3V p2 = VECTOR3_TO_VEC3V(material.GetPos(index2));

							const Vector2 t0a = material.GetTexCoord(index0, 0); // primary texcoord
							const Vector2 t1a = material.GetTexCoord(index1, 0);
							const Vector2 t2a = material.GetTexCoord(index2, 0);

							const Vector2 t0b = material.GetTexCoord(index0, 1); // secondary texcoord (dirt/crack)
							const Vector2 t1b = material.GetTexCoord(index1, 1);
							const Vector2 t2b = material.GetTexCoord(index2, 1);

							const Vec4V t0 = Vec4V(t0a.x, t0a.y, t0b.x, t0b.y);
							const Vec4V t1 = Vec4V(t1a.x, t1a.y, t1b.x, t1b.y);
							const Vec4V t2 = Vec4V(t2a.x, t2a.y, t2b.x, t2b.y);

							const Vec4V c0 = VECTOR4_TO_VEC4V(material.GetCpv(index0));
							const Vec4V c1 = VECTOR4_TO_VEC4V(material.GetCpv(index1));
							const Vec4V c2 = VECTOR4_TO_VEC4V(material.GetCpv(index2));

							s_glassTriangles[s_glassTriangleCount++] = fwVehicleGlassConstructorTriangle(p0, p1, p2, t0, t1, t2, c0, c1, c2, shaderIdx, mtlIdx, boneIndex);
						}
					}
				}

				// Calculate better vehicle damage scale for vehicle verts 
				if (pass == 1 && (bIsOuterGlass || bIsInnerGlass))
				{
					for (int vtxIdx = 0; vtxIdx < material.GetVertexCount(); vtxIdx++)
					{
						Vector4 cpv = material.GetCpv(vtxIdx);
						float damageScaleMult0 = 1.0f;

#if ADD_PED_SAFE_ZONE_TO_VEHICLES
						const Vec3V p0 = VECTOR3_TO_VEC3V(material.GetPos(vtxIdx));
						damageScaleMult0 = pedSafeZone.GetPedSafeAreaMultiplier(p0).Getf();
#endif
						// Move glass open edge to unused channel
						cpv.SetZ(cpv.GetY());
						cpv.SetY(damageScaleMult0);
						
						material.SetCpv(vtxIdx,cpv);
					}
				}
			}

			if (pass == 0)
			{
				if (s_glassTriangleCount > 0)
				{
					sysMemStartTemp();
					Assert(s_glassTriangles == NULL);
					s_glassTriangles = rage_new fwVehicleGlassConstructorTriangle[s_glassTriangleCount];
					s_glassTriangleCount = 0;
					sysMemEndTemp();
				}
				else
				{
					break;
				}
			}
		}
	}

#if ADD_PED_SAFE_ZONE_TO_VEHICLES
	for( int mtlIdx = 0; mtlIdx < numMaterials; mtlIdx++ )
	{
		mshMaterial& material = mesh.GetMtl( mtlIdx );
		u32 damageScaleZeroCount = 0;

		for (int vtxIdx = 0; vtxIdx < material.GetVertexCount(); vtxIdx++)
		{
			Vector3 position = material.GetVertex(vtxIdx).Pos;
			Vector4 cpv = material.GetCpv(vtxIdx);
			if (cpv.y > 0.0f)
			{
				float damageScaleMult = pedSafeZone.GetPedSafeAreaMultiplier(VECTOR3_TO_VEC3V(position)).Getf();
				cpv.SetY(damageScaleMult);
				material.SetCpv(vtxIdx, cpv);
			}
			else
			{
				++damageScaleZeroCount;
			}
		}

		if (damageScaleZeroCount > 0)
		{
			int shaderIdx = -1;
			if ( '#' == material.Name[0] )
				shaderIdx = atoi( material.Name + 1 );
			else
				shaderIdx = atoi( material.Name );
			grmShader& shader = shaderGroup.GetShader( shaderIdx );

			const char* sShaderName = shader.GetName();

			Warningf("%s: (PED SAFE ZONE warning) material %d on shader %s, has %u vertices with 0 damage scale, we expect each vertex to be non-zero other than propellers and wheels", meshName, mtlIdx, sShaderName, damageScaleZeroCount);
		}
	}
#endif

	for( int mtlIdx = 0; mtlIdx < numMaterials; mtlIdx++ )
	{
		mshMaterial& material = mesh.GetMtl( mtlIdx );
		// Materials are named "#([0-9]+)", we just want the index.
		// See ::rage::grmModel::Create as it does it in the same way.
		int shaderIdx = -1;
		if ( '#' == material.Name[0] )
			shaderIdx = atoi( material.Name + 1 );
		else
			shaderIdx = atoi( material.Name );
		grmShader& shader = shaderGroup.GetShader( shaderIdx );

		const char* sShaderName = shader.GetName();
		if(	(stricmp(sShaderName, "vehicle_lightsemissive")!=0)	&&
			(stricmp(sShaderName, "vehicle_dash_emissive") !=0)	)
		{
			Displayf( "\tSkipping material shader %s", sShaderName );
			continue;
		}

		Displayf( "\tPostProcessing material shader %s (%d vertices)", sShaderName, 
			material.GetVertexCount() );
		for ( int vtxIdx = 0; vtxIdx < material.GetVertexCount(); vtxIdx++ )
		{
			const mshBinding& binding = material.GetBinding(vtxIdx);
			uint32 lightID = 0;
			if( binding.IsPassThrough == false )
			{
				for( uint32 i = 0;i < LW_LIGHTCOUNT ;i++)
				{
					if( binding.Mtx[0] == s_boneIDArray[i] )
					{
						const int dimIdx = ( i > LW_LIGHTDEFAULT ) ? LW_LIGHTDEFAULT : i;

						lightID = dimIdx + 1;
						break;
					}
				}
			}

			Vector4 cpv = material.GetCpv(vtxIdx);
			cpv.SetW((float)lightID/255.0f);
			material.SetCpv(vtxIdx,cpv);
		}
	}

	return true;
}

static void VehicleGlassFixupHook(const atArray<int>* skinnedBones)
{
	if (!skinnedBones || !s_glassTriangles || !s_glassTriangleCount)
		return;

	// we have a lodded skeletons and the matrix indices in the verts have been changed, update the bone index in each
	// glass triangle to match the custom skeleton
	for (s32 i = 0; i < s_glassTriangleCount; ++i)
	{
		if (s_glassTriangles[i].m_origBoneIndex == -1)
			continue;

		for (s32 f = 0; f < skinnedBones->GetCount(); ++f)
		{
			if (s_glassTriangles[i].m_origBoneIndex == (*skinnedBones)[f])
			{
				s_glassTriangles[i].m_mappedBoneIndex = f;
				break;
			}
		}
	}
}

//
// name:		LoadObject
// description:	Load a fragment
bool LoadObject(const char* pFilename, gtaFragType* &pFrag)
{
	// It's a new one, this will tell the postprocess callback to setup some data on it's first call...
	s_NewFragType = true;

	sysMemStartTemp();
	const char *tuneFilePath = RagePlatform::GetTuningFolder();
	FRAGTUNE->SetForcedTuningPath(tuneFilePath);
	phVerletCloth::sm_tuneFilePath = tuneFilePath;
	sysMemEndTemp();

	bool oldMatrixSetFlag = fragType::ms_bCreateMatrixSet;
	if (!RagePlatform::IsFragMatrixSetEnabled())
		fragType::ms_bCreateMatrixSet = false;

	if( RagePlatform::GetPostProcessVehicles() )
	{
		grmModel::PreModelHookParams functor;
		functor.Reset<&PostProcessVehiclesMeshLoaderHook>();
		grmModel::SetPreModelCreateHook(functor);

		grmModel::PreModelHookParams3 functor3;
		functor3.Reset<&VehicleGlassFixupHook>();
		grmModel::SetPreModelCreateHook3(functor3);
	}
	else
	{
		grmModel::PreModelHookParams2 functor;
		functor.Reset<&DefaultPostProcessMeshLoaderHook>();
		grmModel::SetPreModelCreateHook2(functor);
	}

	{
		grmModel::MeshPrepareHook meshPrepareHook;
		meshPrepareHook.Reset<&DefaultFragMeshPrepareHook>();
		grmModel::SetMeshPrepareHook(meshPrepareHook);
	}

	{
		fragType::RagebuilderHook functor;
		functor.Reset<&FragTypeParamHook>();
		fragType::SetRagebuilderHook(functor);
	}

	const char* nmdata = RageBuilder::GetPlatformImpl()->GetNMData().c_str();

	if ( stricmp(nmdata, "") != 0 )
	{
		if ( !RBFileExists( nmdata ) )
		{
			Quitf( "Error loading NM XML data, file %s does not exist.", nmdata );
		}
		else
		{
			CTextureConversionSpecification::Begin( g_sysPlatform, pFilename );
			pFrag = gtaFragType::LoadXML<gtaFragType>(pFilename,nmdata);
			CTextureConversionSpecification::End();
		}
	}
	else
	{
		CTextureConversionSpecification::Begin( g_sysPlatform, pFilename );
		pFrag = gtaFragType::Load<gtaFragType>(pFilename);
		CTextureConversionSpecification::End();
	}

	if(pFrag == NULL)
		return false;

	if (fwVehicleGlassConstructorInterface::GetCarGlassMaterialFlagCount() == 0) // set car glass material flags
	{
		const GtaMaterialMgr* p_GtaMtlMgr = dynamic_cast<GtaMaterialMgr*>(&MATERIALMGR);

		for (int i = 0; i < p_GtaMtlMgr->GetNumMaterials(); i++)
		{
			const char* materialName = p_GtaMtlMgr->GetMaterialNameFromIndex(i);

			if (strstr(materialName, "CAR_GLASS") != NULL)
			{
				fwVehicleGlassConstructorInterface::SetCarGlassMaterialFlag(i, materialName);
			}
		}
	}

	const char* _hi = strrchr(pFilename, '_');

	if (_hi == NULL || strcmp(_hi, "_hi/entity.type") != 0)
	{
		pFrag->m_vehicleWindowData = fwVehicleGlassConstructorInterface::ConstructWindowData(pFilename, pFrag, lodLevel, s_glassTriangles, s_glassTriangleCount, s_glassTrianglesAreHD, true, s_glassTriangleFlags);
	}

	pFrag->m_pTintData = PrepackageTint(pFrag->GetCommonDrawable(), pFilename);

	if (s_glassTriangles)
	{
		sysMemStartTemp();
		delete[] s_glassTriangles;
		s_glassTriangles = NULL;
		s_glassTriangleCount = 0;
		sysMemEndTemp();
	}

	fragType::ms_bCreateMatrixSet = oldMatrixSetFlag;

	s32 i,Count = GtaData::m_ideSet.getNumEffects();
	CLightAttr set;

	for(i=0;i<Count;i++)
	{
		set = GtaData::m_ideSet.getEffect(i).l;

		pFrag->m_lights.PushAndGrow(set, (u16) Count);
	}

	sysMemStartTemp();

	//use the first tune file we find...

	char path[MAX_PATH];
	char dirSearch[MAX_PATH];

	// disassemble path
	{
		string_list tuneFiles;
		char basename[MAX_PATH];
		char ext[MAX_PATH];
		SplitFilename(pFilename,path,basename,ext);
		// Try pack file for tune (i.e. entity.type)
		char objname[MAX_PATH];
		strcpy(objname,path);
		objname[strlen(objname) - 1] = '\0';
		char* p_Found = strrchr(objname,'\\');
		if(!p_Found)
			p_Found = strrchr(objname,'/');
		if(p_Found)
			p_Found++;

		// construct new paths
		if(p_Found && tuneFilePath)
		{
			strcpy(path, tuneFilePath);

			strcpy(dirSearch,tuneFilePath);
			strcat(dirSearch, "/");
			strcat(dirSearch, p_Found);
			strcat(dirSearch, ".tune");
		}
		// Disc path
		if(0!=strlen(dirSearch) && ASSET.Exists(dirSearch,"tune"))
		{
			AddFilesToList(dirSearch, tuneFiles);
		}

		if(tuneFiles.empty())
		{
			Displayf("No .tune file found: %s in path %s", p_Found, path);
		}
		else
		{
			char tunePath[RAGE_MAX_PATH];
			strcpy(tunePath, path);
			strcat(tunePath, "/");
			strcat(tunePath, (*tuneFiles.begin()).c_str());

			fiStream* tuningStream = ASSET.Open(tunePath, "tune", true, true);

			fiAsciiTokenizer tuningTok;
			tuningTok.Init(tunePath, tuningStream);
			fragType* tuningType = new fragType;
			tuningType->LoadASCII(tuningTok, tunePath, false);
			sysMemEndTemp();
			pFrag->ApplyTuningData(tuningType, NULL);
			sysMemStartTemp();
			tuningStream->Close();
			delete tuningType;
		}
	}

	fragPhysicsLODGroup* physicsLodGroup = pFrag->GetPhysicsLODGroup();

	if (physicsLodGroup)
	{
		// new code for calculating memory sizes that the fragment will use
		Matrix34 mat;
		mat.Identity();
		fragInst* pFragInst = new fragInst(pFrag,mat);

		sysMemEndTemp();

		pFragInst->Insert();

		if (!pFragInst->GetCached())
			pFragInst->PutIntoCache();

		sysMemStartTemp();

		pFragInst->Remove();
		delete pFragInst;
		// end of new code
	}

	fragDrawable* drawable = pFrag->GetCommonDrawable();
// NOTE: the correct drawable for cloth with LODs is burried inside physics lods - svetli
	if( !drawable->GetLodGroup().ContainsLod(LOD_HIGH) && physicsLodGroup)
	{
		fragPhysicsLOD* physicsLod = physicsLodGroup->GetLODByIndex(0);
		if( physicsLod && physicsLod->GetNumChildren() > 0 )
		{
			fragTypeChild* physicsChild = physicsLod->GetChild(0);
			Assert( physicsChild );
			if( physicsChild )
			{
				drawable = physicsChild->GetUndamagedEntity();
			}			
		}
	}

	fragDrawable *pDrawable = drawable;	//pFrag->GetCommonDrawable();
	int geomCount = 0;
	u32 modelCount = 0;
	if(pDrawable->GetLodGroup().ContainsLod(LOD_HIGH))
	{
		modelCount = pDrawable->GetLodGroup().GetLod(LOD_HIGH).GetCount();
		for(u32 m=0; m<modelCount; m++)
		{
			grmModel *pModel = pDrawable->GetLodGroup().GetLod(LOD_HIGH).GetModel(m);
			Assert(pModel);
			geomCount += pModel->GetGeometryCount();

		}
	}

	if(RageBuilder::GetPlatform()==RageBuilder::pltPs3)
	{
		const int shaderCount = &(pDrawable->GetShaderGroup())? pDrawable->GetShaderGroup().GetCount() : 0;
		if(	pDrawable->GetContainerSize() > DRAWABLE_MAX_CONTAINER_SIZE ||
			modelCount > DRAWABLE_MAX_MODELS ||
			geomCount > DRAWABLE_MAX_GEOMS ||
			shaderCount > DRAWABLE_MAX_SHADERS)
		{
			Displayf("Model complexity of %s high.\n container size (max %d): %d\n models (max %d): %d\n geometries (max %d): %d\n shaders (max %d): %d",
				s_Buildname,
				DRAWABLE_MAX_CONTAINER_SIZE, pDrawable->GetContainerSize(),
				DRAWABLE_MAX_MODELS, modelCount,
				DRAWABLE_MAX_GEOMS, geomCount,
				DRAWABLE_MAX_SHADERS, shaderCount);
			// Let's defer this decision until runtime, except if it's flagged up as a destruction model
			if (RagePlatform::GetPPUOnlyHint())
				pDrawable->MakePpuOnly();
			else
				artErrorf("(model %s is not a destruction model, so this will assert at runtime as well)",s_Buildname);
		}
	}

	FixupDrawBucketMask( drawable );

	if( tuneFilePath )
		delete tuneFilePath;

	sysMemEndTemp();

	grmModel::RemovePreModelCreateHook( );
	grmModel::RemovePreModelCreateHook2( );
	grmModel::RemovePreModelCreateHook3( );

	{
		fragType::RagebuilderHook functor;
		fragType::SetRagebuilderHook(functor);
	}

	return true;
}

bool LoadObject(const char* pFilename, ptxFxList*& pFxList)
{
	const char* p_effectpath = RagePlatform::GetEffectPath();
	char modelpath[RAGE_MAX_PATH];
	char texturepath[RAGE_MAX_PATH];

	sprintf(modelpath, "%smodels", p_effectpath);
	sprintf(texturepath, "%stextures;%smodeltex", p_effectpath, p_effectpath);

	char baseName[RAGE_MAX_PATH];
	strncpy(baseName, pFilename, 128);
	*strrchr(baseName, '.') = '\0';

	pFxList = new ptxFxList();
	CTextureConversionSpecification::Begin( g_sysPlatform, pFilename );
	pFxList->Load(baseName, modelpath, texturepath);
	CTextureConversionSpecification::End( );

	return true;
}

bool LoadObject(const char* pFilename, CVehicleRecording* &pVehicleRecording)
{
	pVehicleRecording = new CVehicleRecording();
	pVehicleRecording->Load(pFilename);
	return true;
}

// Templatised function to save a dictionary of objects.
// @see SaveDictionary
bool SaveDictionaryOfAnims(const char* filename, const char* extension, string_list& list, int32 version, bool bSaveEmpty = false, size_t size = DEFAULT_BUILD_SIZE)
{
	// TODO JA - Consider replacing this with a anim dictionary (to get dictionary compression)
	bool result = false;
	int32 listSize = list.size();
	if(listSize == 0 && !bSaveEmpty)
	{
		Warningf("SaveDictionary: There is nothing to save - %s\n", filename);
		return false;
	}

	for (int pass=0; pass<=pgRscBuilder::LAST_PASS; ++pass)
	{
		RageBuilder::BeginBuild(filename, extension, pass?true:false, size);
		pgDictionary<crAnimation>* pDictionary = new pgDictionary<crAnimation>(list.size());

		//
		// for all the objects in the list
		// for(uint32 i=0; i<list.size(); i++)
		for (string_list::iterator it = list.begin(); it != list.end(); ++it)
		{
			crAnimation* pObject;

			const char* name = (*it).c_str();

			if(!LoadObject(name, pObject))
			{
				RageBuilder::EndBuild(pDictionary);
				return false;
			}

			if(pObject)
			{
				char baseName[64];
				char rootName[64];
				ASSET.BaseName(baseName, 64, ASSET.FileName(name));

				strcpy(rootName,baseName);
				char* p_Found = strstr(rootName,"_uv_");
				u32 code;

				if(p_Found)
				{
					*p_Found = '\0';
					p_Found += 4;
					code = atStringHash(rootName) + atoi(p_Found) + 1;
				}
				else
				{
					code = atStringHash(baseName);
				}

				if ( !pDictionary->AddEntry(code, pObject) )
				{
					Warningf("Could not add entry %s\n", baseName );
					RageBuilder::EndBuild(pDictionary);
					return false;
				}
			}
		}

		if (pass==pgRscBuilder::LAST_PASS)
		{
			pgRscBuilder::ReadyBuild(pDictionary);
			datTypeStruct myStruct;
			pDictionary->DeclareStruct(myStruct);

			result = RageBuilder::SaveBuild(filename, extension, version);

			// Can't delete the type if we byte swapped it...
			if (!RageBuilder::GetPlatformImpl()->IsEndianSwapping())
			{
				//Crashes with WR 217, ref count problem				pDictionary->Release();
			}
		}
		else
		{
			// eventually, but not quite yet: delete whatever
		}

		RageBuilder::EndBuild(pDictionary);
	}
	return result;
}

bool RagePlatform::ValidateClip( const char* clipname )
{
	// Validate the clip and associated animation. If all's jiggy then add to clip list.
	crClip* pClip = crClip::AllocateAndLoad( clipname );
	switch(pClip->GetType())
	{
	case crClip::kClipTypeAnimation:
	case crClip::kClipTypeAnimationExpression:
		{
			crClipAnimation* pClipAnim = static_cast<crClipAnimation*>(pClip);
			const crAnimation* pAnim = pClipAnim->GetAnimation();
			if(!pAnim)
			{
				Errorf("ERROR - Clip has NULL animation reference: '%s'", clipname);
				return false;
			}
		}
		break;

	default:
		Errorf("ERROR - Clip is not of type crClip::kClipTypeAnimation: '%s'", clipname);
		return false;
	}

	return true;
}

class JimmyClipDictionary : public crClipDictionary
{
public:

	virtual ClipKey CalcKey(const char* name) const
	{

		char rootName[RAGE_MAX_PATH] = { 0 };

		strcpy(rootName, name);

		char* p_Found = strrstr(rootName,"_uv_");

		u32 code = 0;

		if(p_Found)
		{
			*p_Found = '\0';

			p_Found += 4;

			code = atStringHash(rootName) + atoi(p_Found) + 1;
		}
		else
		{

			code = atStringHash(name);
		}

		return ClipKey(code);
	}
};



bool SaveDictionaryOfClips(const char* filename, const char* extension, string_list& list, int32 version, bool bSaveEmpty = false, size_t size = DEFAULT_BUILD_SIZE, bool usePackLoader = true)
{
	bool result = false;
	crClipDictionary* pDictionary;

	// Build an array from a string_list
	atArray<atString> clipFilenames;
	for (string_list::iterator it = list.begin(); it != list.end(); ++it)
	{
		const char* name = (*it).c_str();
		//if( !RagePlatform::ValidateClip( name ) )
		//	return false;

		clipFilenames.PushAndGrow(atString(name));
	}

	for (int pass=0; pass<=pgRscBuilder::LAST_PASS; ++pass)
	{
		RageBuilder::BeginBuild(filename, extension, pass?true:false, size);

		pDictionary = rage_new JimmyClipDictionary;
		if(usePackLoader)
		{
			CAnimLoaderPack packloader;
			pDictionary->LoadClips(clipFilenames, 0, &packloader, true);
		}
		else
		{
			pDictionary->LoadClips(clipFilenames, 0, 0, true);
		}

		if (pass==pgRscBuilder::LAST_PASS)
		{
			pgRscBuilder::ReadyBuild(pDictionary);
			datTypeStruct myStruct;
			pDictionary->DeclareStruct(myStruct);

			result = RageBuilder::SaveBuild(filename, extension, version);
		}

		RageBuilder::EndBuild(pDictionary);
	}

	return result;
}

bool SaveDictionaryOfParamMotions(const char* filename, const char* extension, string_list& list, int32 version, bool bSaveEmpty = false, size_t size = DEFAULT_BUILD_SIZE)
{
	bool result = false;

	crpmParameterizedMotionDictionary* pDictionary;

	// Build an array from a string_list
	atArray<atString> pmFilenames;
	for (string_list::iterator it = list.begin(); it != list.end(); ++it)
	{
		const char* name = (*it).c_str();
		pmFilenames.PushAndGrow(atString(name));
	}

	for (int pass=0; pass<=pgRscBuilder::LAST_PASS; ++pass)
	{
		CAnimLoaderPack animPackLoader;
		CClipLoaderPack clipPackLoader;
		RageBuilder::BeginBuild(filename, extension, pass?true:false, size);
		pDictionary = crpmParameterizedMotionDictionary::AllocateAndLoadParameterizedMotions(pmFilenames, 0, &clipPackLoader, &animPackLoader, true);

		if (pass==pgRscBuilder::LAST_PASS)
		{
			pgRscBuilder::ReadyBuild(pDictionary);
			datTypeStruct myStruct;
			pDictionary->DeclareStruct(myStruct);

			result = RageBuilder::SaveBuild(filename, extension, version);
		}

		RageBuilder::EndBuild(pDictionary);
	}

	return result;
}

struct FileEntry
{
	const char *name;
	u64 fileSize;
};


 bool SaveClothDictionaryFolderName(const char* filename, const char* extension, string_list& list, int32 version, bool bSaveEmpty = false, size_t size = DEFAULT_BUILD_SIZE)
{
	bool result = true;
	int32 listSize = list.size();
	if(listSize == 0 && !bSaveEmpty)
	{
		Warningf("SaveDictionary: There is nothing to save - %s\n", filename);
		return false;
	}

	for (int pass=0; pass<=pgRscBuilder::LAST_PASS; ++pass)
	{
		RageBuilder::BeginBuild(filename, extension, pass?true:false,size);

		pgDictionary<characterCloth>* pDictionary = new pgDictionary<characterCloth>(list.size());

		char strCharacterName[MAX_PATH];
		strcpy( strCharacterName, filename );
		char* lastSlash = strrchr( strCharacterName, '/' );
		*lastSlash = '\0';

		lastSlash = strrchr( strCharacterName, '/' );
		*lastSlash = '\0';

		char* nextLastSlash = strrchr( strCharacterName, '/' );
		nextLastSlash++;
		characterClothController::sm_CharacterName = nextLastSlash;
		

		// for all the objects in the list
		for (string_list::iterator it = list.begin(); it != list.end(); ++it)
		{
			characterCloth* pObject;
			const char* name = (*it).c_str();

			if( result )
			{
				result = LoadObject(name, pObject);
			}

			char path[MAX_PATH];
			char modelname[MAX_PATH];

			SplitFilename(name, path, NULL, NULL);

			// now get the last folder name
			path[strlen(path)-1] = '\0';
			SplitFilename(path, NULL, modelname, NULL);

			if (!pDictionary->AddEntry(modelname, pObject))
			{
				Warningf("Could not add entry %s\n", modelname);
				RageBuilder::EndBuild(pDictionary);
				return false;
			}
		}

		if (pass == pgRscBuilder::LAST_PASS && result == true)
		{
			pgRscBuilder::ReadyBuild(pDictionary);
			datTypeStruct myStruct;
			pDictionary->DeclareStruct(myStruct);
			result = RageBuilder::SaveBuild(filename, extension, version);
		}

		RageBuilder::EndBuild(pDictionary);
	}
	return result;
}

#if ENABLE_BLENDSHAPES
bool SaveDictionaryOfBlendShapes(const char* filename, const char* extension, string_list& list, int32 version, bool bSaveEmpty = false, size_t size = DEFAULT_BUILD_SIZE)
{
	bool result = false;
	int32 listSize = list.size();
	if(listSize == 0 && !bSaveEmpty)
	{
		Warningf("SaveDictionary: There is nothing to save - %s\n", filename);
		return false;
	}

	for (int pass=0; pass<=pgRscBuilder::LAST_PASS; ++pass)
	{
		RageBuilder::BeginBuild(filename, extension, pass?true:false,size);

		pgDictionary<grbTargetManager>* pDictionary = new pgDictionary<grbTargetManager>(list.size());
		rmcDrawable* drawable = NULL;

		// for all the objects in the list
		for (string_list::iterator it = list.begin(); it != list.end(); ++it)
		{
			grbTargetManager* pObject;

			const char* name = (*it).c_str();

			// Blendshape specific

			sysMemStartTemp();
			char drawablePath[MAX_PATH];
			ASSET.RemoveNameFromPath(drawablePath,MAX_PATH,name);

			drawable = new gtaDrawable;

			ASSET.PushFolder(drawablePath);

			AssertVerify(drawable->Load("entity.type"));	
			ASSET.PopFolder();
			sysMemEndTemp();

			if(!LoadObject(name, drawable, pObject))
			{
				RageBuilder::EndBuild(pDictionary);
				return false;
			}
			
			// Same as SaveDictionaryFolderName from here onwards

			char path[MAX_PATH];
			char modelname[MAX_PATH];

			SplitFilename(name, path, NULL, NULL);

			// now get the last folder name
			path[strlen(path)-1] = '\0';
			SplitFilename(path, NULL, modelname, NULL);

			if (!pDictionary->AddEntry(modelname, pObject))
			{
				Warningf("Could not add entry %s\n", modelname);
				RageBuilder::EndBuild(pDictionary);
				return false;
			}

			if(drawable)
			{
				sysMemStartTemp();
				delete drawable;
				sysMemEndTemp();
			}
		}

		if (pass == pgRscBuilder::LAST_PASS)
		{
			pgRscBuilder::ReadyBuild(pDictionary);
			datTypeStruct myStruct;
			pDictionary->DeclareStruct(myStruct);
			result = RageBuilder::SaveBuild(filename, extension, version);

			// Can't delete the type if we byte swapped it...
			if (!RageBuilder::GetPlatformImpl()->IsEndianSwapping())
			{
				///hacky fix for crash				pDictionary->Release();
			}
		}
		else
		{
			// eventually, but not quite yet: delete whatever
		}

		RageBuilder::EndBuild(pDictionary);
	}

	return result;
	
}
#endif // ENABLE_BLENDSHAPES

bool
SaveTextureDictionaryAsset( TextureFactory::eTextureID texture_id, 
							const char* filename, 
							const char* ext, 
							TexEntry* list, int listsize, 
							int32 version, 
							bool bSaveEmpty, 
							size_t size,
							bool inputsAreTCLFiles)
{
	bool result = false;
	TextureList textures;

	// Pre-pass to determine size of pgDictionary.  For each texture; loads its
	// meta-data and determine if it has an output for this texture_id.
	// This is required to pre-allocate the texture dictionary size.  The same
	// filter is used in the main loop below to only pack the right textures.
	int count = 0;
	for ( int i = 0; i < listsize; ++i )
	{
		// No longer need to re-load the TCL/TCS/TCP specification as the TexEntry
		// caches the HD split options.
		switch ( texture_id )
		{
		case TextureFactory::TEXTURE_ID_MIP_CHAIN_HIGH:
			if ( list[i].splitHD || list[i].splitHD2 )
				++count;
			break;
		case TextureFactory::TEXTURE_ID_MIP_CHAIN_DEFAULT:
			++count;
			break;
		default:
			break;
		}
	}

	if ( !bSaveEmpty && ( ( 0 == count ) || ( 0 == listsize ) ) )
	{
		// DHM; not sure if we want this as it will warn for every TXD that 
		// isn't saving a +hi component.
		// Bug #132257 to remove this warning.
		// Warningf( "NOT saving empty tex dict - %s", filename );
		return ( true );
	}
	else if ( bSaveEmpty && ( ( 0 == count ) || ( 0 == listsize ) ) )
	{
		Warningf( "saving empty tex dict - %s", filename );
		for ( int pass=0; pass <= pgRscBuilder::LAST_PASS; ++pass )
		{
			RageBuilder::BeginBuild( filename, ext, pass ? true : false, size );

			pgDictionary<grcTextureDefault>* pTexDict = 
				new pgDictionary<grcTextureDefault>( count );
			
			if (pass == pgRscBuilder::LAST_PASS)
			{
				pgRscBuilder::ReadyBuild( pTexDict );
				datTypeStruct myStruct;
				pTexDict->DeclareStruct(myStruct);

				result = RageBuilder::SaveBuild(filename, ext, version);
			}

			RageBuilder::EndBuild(pTexDict);
		}
		return ( result );
	}

	bool encounteredMissingTCSReferencedTextures = false;

	for ( int pass=0; pass <= pgRscBuilder::LAST_PASS; ++pass )
	{
		grcTextureFactory::GetInstance().ClearFreeList();
		RageBuilder::BeginBuild( filename, ext, pass ? true : false, size );

		pgDictionary<grcTextureDefault>* pTexDict = 
			new pgDictionary<grcTextureDefault>( count );

		bool entries_added = false;
		for ( int i = 0; i < listsize; ++i )
		{
			Assertf( list[i].pSpec, "TCS for % not loaded.  Internal error.",
				list[i].name );

			boost::shared_ptr<CTextureConversionSpecification> pSpec = list[i].pSpec;
			const char* name = list[i].name;
			sysMemStartTemp( );
			textures.clear( );
			sysMemEndTemp( );

			if ( list[i].linked )
			{
				if(pSpec->m_resourceTextures.GetCount() == 0)
				{
					Errorf("TCL input %s doesn't specify any resourceTextures", name);
					RageBuilder::EndBuild( pTexDict );
					return false;
				}

				// The TCS has specified the textures to be resources, so act accordingly
				for(int textureIndex = 0 ; textureIndex < pSpec->m_resourceTextures.GetCount() ; ++textureIndex)
				{
					char texturePathname[RAGE_MAX_PATH] = "";
					sysMemStartTemp();
					CTextureConversionParams::ResolvePath(pSpec->m_resourceTextures[textureIndex].m_pathname.c_str(), texturePathname, true);
					sysMemEndTemp();

					// If the file doesn't exist locally, output an appropriate error
					if (!ASSET.Exists(texturePathname, ""))
					{
						Errorf("Texture file '%s', which was referenced from a TCS, is missing locally", texturePathname);
						encounteredMissingTCSReferencedTextures = true;
					}
					
					if ( !LoadObject( texturePathname, filename, texture_id, textures, pSpec.get() ) )
					{
						RageBuilder::EndBuild(pTexDict);
						return false;
					}

					if ( textures.size() > 0 )
					{
						s32 nameLength;
						const char* p_found = strchr(texturePathname,'.');

						if(p_found)
							nameLength = p_found - texturePathname;
						else
							nameLength = strlen(texturePathname);

						sysMemStartTemp();
						char* baseName = new char[nameLength + 1];
						sysMemEndTemp();
						ASSET.BaseName(baseName, nameLength, ASSET.FileName(texturePathname));

						bool retval = true;
						for ( TextureList::iterator it = textures.begin();
							it != textures.end();
							++it )
						{
							if ( it->first == texture_id )
							{
								grcTexture* pTexture = it->second;
								retval &= pTexDict->AddEntry( baseName, (grcTextureDefault*)( pTexture ) );
								entries_added = true;
							}
							else
							{
								grcTexture* pTexture = it->second;
								pTexture->Release( );
							}
						}

						if ( !retval )
						{
							Warningf("Could not add entry %s\n", baseName );
							RageBuilder::EndBuild( pTexDict );
							sysMemStartTemp();
							delete[] baseName;
							sysMemEndTemp();

							return false;
						}
						sysMemStartTemp();
						delete[] baseName;
						sysMemEndTemp();
					}					
				}
			}
			else
			{
				// textures are packed, so fall back to the old way of loading them
				const char* texturePathname = name;
				
				if ( !LoadObject( texturePathname, filename, texture_id, textures, pSpec.get() ) )
				{
					RageBuilder::EndBuild(pTexDict);
					return false;
				}

				if ( textures.size() > 0 )
				{
					s32 nameLength;
					const char* p_found = strchr(texturePathname,'.');

					if(p_found)
						nameLength = p_found - texturePathname;
					else
						nameLength = strlen(texturePathname);

					sysMemStartTemp();
					char* baseName = new char[nameLength + 1];
					sysMemEndTemp();
					ASSET.BaseName(baseName, nameLength, ASSET.FileName(texturePathname));

					bool retval = true;
					for ( TextureList::iterator it = textures.begin();
						it != textures.end();
						++it )
					{
						if ( it->first == texture_id )
						{
							grcTexture* pTexture = it->second;
							retval &= pTexDict->AddEntry( baseName, (grcTextureDefault*)( pTexture ) );
							entries_added = true;
						}
						else
						{
							grcTexture* pTexture = it->second;
							pTexture->Release( );
						}
					}

					if ( !retval )
					{
						Warningf("Could not add entry %s\n", baseName );
						RageBuilder::EndBuild( pTexDict );
						sysMemStartTemp();
						delete[] baseName;
						sysMemEndTemp();

						return false;
					}
					sysMemStartTemp();
					delete[] baseName;
					sysMemEndTemp();
				}
			}
		}

		if(encounteredMissingTCSReferencedTextures)
		{
			RageBuilder::EndBuild(pTexDict);
			return false;
		}

		if (entries_added && pass == pgRscBuilder::LAST_PASS)
		{
			pgRscBuilder::ReadyBuild( pTexDict );
			datTypeStruct myStruct;
			pTexDict->DeclareStruct(myStruct);

			result = RageBuilder::SaveBuild(filename, ext, version);
		}
		else if ( pass == pgRscBuilder::LAST_PASS )
		{
			// pTexDict->Release();
			result = true;
		}

		RageBuilder::EndBuild(pTexDict);
	}
	return ( result );
}

bool 
SaveDictionaryOfTextures( const char* filename, 
						  const char* extension, 
						  const char* hi_suffix, 
						  string_list& list, 
						  int32 version,
						  bool inputsAreTCLFiles,
						  bool bSaveEmpty = false, 
						  size_t size = DEFAULT_BUILD_SIZE,
						  bool sortBySize = false )
{
	bool result = true;

	int32 listSize = list.size();
	if(listSize == 0 && !bSaveEmpty)
	{
		Warningf("SaveDictionaryOfTextures: There is nothing to save - %s\n", filename);
		return false;
	}

	s32 i = 0;
	sysMemStartTemp( );
	TexEntry* pEntries = new TexEntry[listSize];
	sysMemSet( pEntries, 0, listSize * sizeof( TexEntry ) );
	sysMemEndTemp( );

	for (string_list::iterator it = list.begin(); it != list.end(); ++it)
	{
		pEntries[i].name = (*it).c_str();
		pEntries[i].fileSize = fiDevice::GetDevice(pEntries[i].name)->GetFileSize(pEntries[i].name);
		pEntries[i].LoadSpecification( pEntries[i].name );

		i++;
	}

	if(sortBySize)
		std::sort(pEntries, pEntries+i, TexEntrySorter());

	result &= SaveTextureDictionaryAsset( TextureFactory::TEXTURE_ID_MIP_CHAIN_DEFAULT, 
		filename, extension, pEntries, listSize, TEXTURE_DICTIONARY_VERSION, true, size, inputsAreTCLFiles );

	char filename_hi[MAX_PATH];
	memset( filename_hi, 0, sizeof( char ) * MAX_PATH );
	sprintf_s( filename_hi, MAX_PATH, "%s+hi", filename );
	result &= SaveTextureDictionaryAsset( TextureFactory::TEXTURE_ID_MIP_CHAIN_HIGH, 
		filename_hi, extension, pEntries, listSize, TEXTURE_DICTIONARY_VERSION, false, size, inputsAreTCLFiles );

	sysMemStartTemp( );
	delete[] pEntries;
	sysMemEndTemp( );
	return ( result );
}

// Templatised function to save a dictionary of objects.
// @see SaveDictionary
template<class T> bool SaveDictionary(const char* filename, const char* extension, string_list& list, int32 version, bool bSaveEmpty = false, size_t size = DEFAULT_BUILD_SIZE )
{
	bool result = false;

	int32 listSize = list.size();
	if(listSize == 0 && !bSaveEmpty)
	{
		Warningf("SaveDictionary: There is nothing to save - %s\n", filename);
		return false;
	}

	s32 i = 0;
	FileEntry* p_entries = new FileEntry[listSize];

	for (string_list::iterator it = list.begin(); it != list.end(); ++it)
	{
		p_entries[i].name = (*it).c_str();
		p_entries[i].fileSize = fiDevice::GetDevice(p_entries[i].name)->GetFileSize(p_entries[i].name);

		i++;
	}

	for (int pass=0; pass<=pgRscBuilder::LAST_PASS; ++pass)
	{
		// Lol
		grcTextureFactory::GetInstance().ClearFreeList();

		RageBuilder::BeginBuild(filename, extension, pass?true:false,size);
		pgDictionary<T>* pDictionary = new pgDictionary<T>(list.size());

		for(i=0;i<listSize;i++)
			//for (string_list::iterator it = list.begin(); it != list.end(); ++it)
		{
			T* pObject;
			const char* name = p_entries[i].name;

			if(!LoadObject(name, pObject))
			{
				RageBuilder::EndBuild(pDictionary);
				return false;
			}

			if(pObject)
			{
				s32 nameLength;
				const char* p_found = strchr(name,'.');

				if(p_found)
					nameLength = p_found - name;
				else
					nameLength = strlen(name);


				sysMemStartTemp();
				char* baseName = new char[nameLength + 1];
				sysMemEndTemp();
				ASSET.BaseName(baseName, nameLength, ASSET.FileName(name));


				bool retval = pDictionary->AddEntry(baseName, pObject);


				if ( !retval )
				{
					Warningf("Could not add entry %s\n", baseName );
					RageBuilder::EndBuild(pDictionary);
					sysMemStartTemp();
					delete[] baseName;
					sysMemEndTemp();

					return false;
				}
				sysMemStartTemp();
				delete[] baseName;
				sysMemEndTemp();

			}
		}

		if (pass == pgRscBuilder::LAST_PASS)
		{
			pgRscBuilder::ReadyBuild(pDictionary);
			datTypeStruct myStruct;
			pDictionary->DeclareStruct(myStruct);

			result = RageBuilder::SaveBuild(filename, extension, version);

			// Can't delete the type if we byte swapped it...
			if (!RageBuilder::GetPlatformImpl()->IsEndianSwapping())
			{
				//Crashes with WR 217				pDictionary->Release();
			}
		}
		else
		{
			// eventually, but not quite yet: delete whatever
		}

		RageBuilder::EndBuild(pDictionary);
	}

	return result;
}

// Templatised function to save a dictionary of objects.
//
// This one uses the folder name that contains the file to make the objectname,
// x:/data/model/entity.type gets added under 'model'.
// @see SaveDictionary
template<class T> bool SaveDictionaryFolderName(const char* filename, const char* extension, string_list& list, int32 version, bool bSaveEmpty = false, size_t size = DEFAULT_BUILD_SIZE)
{
	bool result = false;
	int32 listSize = list.size();
	if(listSize == 0 && !bSaveEmpty)
	{
		Warningf("SaveDictionary: There is nothing to save - %s\n", filename);
		return false;
	}

	for (int pass=0; pass<=pgRscBuilder::LAST_PASS; ++pass)
	{
		RageBuilder::BeginBuild(filename, extension, pass?true:false,size);

		pgDictionary<T>* pDictionary = new pgDictionary<T>(list.size());

		// for all the objects in the list
		for (string_list::iterator it = list.begin(); it != list.end(); ++it)
		{
			T* pObject;

			const char* name = (*it).c_str();

			if(!LoadObject(name, pObject))
			{
				RageBuilder::EndBuild(pDictionary);
				return false;
			}

			char path[MAX_PATH];
			char modelname[MAX_PATH];

			SplitFilename(name, path, NULL, NULL);

			// now get the last folder name
			path[strlen(path)-1] = '\0';
			SplitFilename(path, NULL, modelname, NULL);

			if (!pDictionary->AddEntry(modelname, pObject))
			{
				Warningf("Could not add entry %s\n", modelname);
				RageBuilder::EndBuild(pDictionary);
				return false;
			}
		}

		if (pass == pgRscBuilder::LAST_PASS)
		{
			pgRscBuilder::ReadyBuild(pDictionary);
			datTypeStruct myStruct;
			pDictionary->DeclareStruct(myStruct);
			result = RageBuilder::SaveBuild(filename, extension, version);

			// Can't delete the type if we byte swapped it...
			if (!RageBuilder::GetPlatformImpl()->IsEndianSwapping())
			{
				///hacky fix for crash				pDictionary->Release();
			}
		}
		else
		{
			// eventually, but not quite yet: delete whatever
		}

		RageBuilder::EndBuild(pDictionary);
	}

	return result;
}


bool RagePlatform::SaveTextureDictionary(const char* filename, const char* hi_suffix)
{
	return SaveDictionaryOfTextures(filename, 
									TEXTURE_DICTIONARY_EXT, 
									hi_suffix, 
									ms_TextureList, 
									TEXTURE_DICTIONARY_VERSION, 
									m_inputsAreTCLFiles, 
									true, 
									DEFAULT_BUILD_SIZE, 
									true);
}

bool RagePlatform::SaveTextureDictionaryHiMip(const char* filename, const char* suffix)
{
	bool result = true;

	int32 listSize = ms_TextureList.size();
	if ( 0 == listSize )
		return (true);

	s32 i = 0;
	sysMemStartTemp( );
	TexEntry* pEntries = new TexEntry[listSize];
	sysMemSet( pEntries, 0, listSize * sizeof( TexEntry ) );
	sysMemEndTemp( );

	for (string_list::iterator it = ms_TextureList.begin(); it != ms_TextureList.end(); ++it)
	{
		pEntries[i].name = (*it).c_str();
		pEntries[i].fileSize = fiDevice::GetDevice(pEntries[i].name)->GetFileSize(pEntries[i].name);
		pEntries[i].LoadSpecification( pEntries[i].name );

		i++;
	}
	std::sort(pEntries, pEntries+i, TexEntrySorter());

	// Recalculate the actual requirements.

	char filename_hi[MAX_PATH];
	memset( filename_hi, 0, sizeof( char ) * MAX_PATH );
	sprintf_s( filename_hi, MAX_PATH, "%s+%s", filename, suffix );
	result &= SaveTextureDictionaryAsset( TextureFactory::TEXTURE_ID_MIP_CHAIN_HIGH, 
		filename_hi, TEXTURE_DICTIONARY_EXT, pEntries, listSize,
		TEXTURE_DICTIONARY_VERSION, false, DEFAULT_BUILD_SIZE, false );

	sysMemStartTemp( );
	delete[] pEntries;
	sysMemEndTemp( );
	return ( result );
}

bool RagePlatform::SaveClipDictionary(const char* filename, bool usePackLoader)
{
	return SaveDictionaryOfClips(filename, RageBuilder::GetPlatformExtension(CLIP_DICTIONARY_EXT), ms_ClipList, CLIP_DICTIONARY_VERSION, false, DEFAULT_BUILD_SIZE, usePackLoader);
}

bool RagePlatform::SaveParamMotionDictionary(const char* filename)
{
	return SaveDictionaryOfParamMotions(filename, RageBuilder::GetPlatformExtension(PARAM_MOTION_DICTIONARY_EXT), ms_ParamMotionList, PARAM_MOTION_DICTIONARY_VERSION);
}

bool RagePlatform::SaveExpressionsDictionary(const char* filename)
{
	return SaveDictionary<crExpressions>(filename, RageBuilder::GetPlatformExtension(EXPRESSIONS_DICTIONARY_EXT), ms_ExpressionsList, EXPRESSIONS_DICTIONARY_VERSION);
}

bool RagePlatform::SaveBlendShapeDictionary(const char* ENABLE_BLENDSHAPES_ONLY(filename))
{
#if ENABLE_BLENDSHAPES
	return SaveDictionaryOfBlendShapes(filename, RageBuilder::GetPlatformExtension(BLENDSHAPE_DICTIONARY_EXT), ms_BlendShapeList, BLENDSHAPE_DICTIONARY_VERSION);
#else // ENABLE_BLENDSHAPES
	return true;
#endif // ENABLE_BLENDSHAPES
}

bool RagePlatform::SaveFrameFilterDictionary(const char* filename)
{
	return SaveDictionary<crFrameFilter>(filename, RageBuilder::GetPlatformExtension(FRAMEFILTER_DICTIONARY_EXT), ms_FrameFilterList, FRAMEFILTER_DICTIONARY_VERSION);
}


bool RagePlatform::SaveModelDictionary(const char* filename)
{
	grcTexture::SetSuppressReferenceWarning();
	if (g_sysPlatform == platform::PS3)
	{
		const char* name = ASSET.FileName(filename);
		RagePlatformPs3::sm_Edgify = (stricmp(name, "plantsmgr") != 0); // these need to be read by the plantsmgr, so they need to stay non-edge		
	}

	// Dictionaries don't have positions in main memory
	bool oldPositionsInMainMem = RagePlatformPs3::sm_EdgePositionsInMainMem;
	RagePlatformPs3::sm_EdgePositionsInMainMem = false;

	bool result = SaveDictionaryFolderName<gtaDrawable>(filename, RageBuilder::GetPlatformExtension(DRAWABLE_DICTIONARY_EXT), ms_ModelList, DRAWABLE_DICTIONARY_VERSION);

	// Restore
	RagePlatformPs3::sm_Edgify = true;
	RagePlatformPs3::sm_EdgePositionsInMainMem = oldPositionsInMainMem;
	RagePlatform::SetDwdType("HD");
	grcTexture::ClearSuppressReferenceWarning();

	return result;
}


bool RagePlatform::SaveClothDictionary(const char* filename)
{
	if (g_sysPlatform == platform::PS3)
	{
		const char* name = ASSET.FileName(filename);
		RagePlatformPs3::sm_Edgify = (stricmp(name, "plantsmgr") != 0); // these need to be read by the plantsmgr, so they need to stay non-edge		
	}

	// Dictionaries don't have positions in main memory
	bool oldPositionsInMainMem = RagePlatformPs3::sm_EdgePositionsInMainMem;
	RagePlatformPs3::sm_EdgePositionsInMainMem = false;

	bool result = SaveClothDictionaryFolderName(filename, RageBuilder::GetPlatformExtension(CLOTH_DICTIONARY_EXT), ms_ModelList, CLOTH_DICTIONARY_VERSION);

	// Restore
	RagePlatformPs3::sm_Edgify = true;
	RagePlatformPs3::sm_EdgePositionsInMainMem = oldPositionsInMainMem;

	return result;
}


bool RagePlatform::SaveBoundDictionary(const char* filename)
{
	return SaveDictionary<phBound>(filename, RageBuilder::GetPlatformExtension(BOUNDS_DICTIONARY_EXT), ms_BoundList, BOUNDS_DICTIONARY_VERSION);
}


bool RagePlatform::SavePhysicsDictionary(const char* filename)
{
	return SaveDictionary<phArchetype>(filename, RageBuilder::GetPlatformExtension(PHYSICS_DICTIONARY_EXT), ms_BoundList, PHYSICS_DICTIONARY_VERSION);
}

bool RagePlatform::LoadTextureTemplates(const char* filefilter)
{
	string_list templateList;
	string_list textureList;

	AddFilesToList(filefilter, templateList, false); // DO_TCP_PRELOAD

	for (string_list::iterator it = templateList.begin(); it != templateList.end(); ++it)
	{
		const char* path = (*it).c_str();

		CTextureConversionSpecification* spec = CTextureConversionSpecification::Load(path, true);

		if (spec) // for all TCP and TCS files ..
		{
			spec->FinaliseConversionParams();

			for (int i = 0; i < spec->m_imageFiles.size(); i++)
			{
				const char* filename = spec->m_imageFiles[i].m_filename.c_str();

				ms_TextureIgnoreList.push_back(filename);
			}

			textureList.push_back(path);
		}
	}

	for (string_list::iterator it = ms_TextureIgnoreList.begin(); it != ms_TextureIgnoreList.end(); ++it)
	{
		for (string_list::iterator it2 = textureList.begin(); it2 != textureList.end(); ++it2)
		{
			if (stricmp((*it).c_str(), (*it2).c_str()) == 0)
			{
				(*it) = ""; // don't ignore
			}
		}
	}

	return true;
}

bool RagePlatform::LoadTexture(const char* filefilter)
{
	AddFilesToList(filefilter, ms_TextureList, false );
	return true;
}


bool RagePlatform::LoadAnim(const char* filefilter)
{
	AddFilesToList(filefilter, ms_AnimList, false );
	return true;
}

bool RagePlatform::LoadClip(const char* filefilter)
{
	AddFilesToList(filefilter, ms_ClipList, false );
	return true;
}

bool RagePlatform::LoadParamMotion(const char* filefilter)
{
	AddFilesToList(filefilter, ms_ParamMotionList, false );
	return true;
}

bool RagePlatform::LoadExpressions(const char* filefilter)
{
	AddFilesToList(filefilter, ms_ExpressionsList, false );
	return true;
}


bool RagePlatform::LoadModel(const char* filefilter)
{
	AddFilesToList(filefilter, ms_ModelList, false );
	return true;
}

bool RagePlatform::LoadBlendShape(const char* ENABLE_BLENDSHAPES_ONLY(filefilter))
{
#if ENABLE_BLENDSHAPES
	AddFilesToList(filefilter, ms_BlendShapeList, false );
#endif // ENABLE_BLENDSHAPES
	return true;
}

bool RagePlatform::LoadFragment(const char* filefilter)
{
	AddFilesToList(filefilter, ms_FragmentList, false );
	return true;
}

bool RagePlatform::LoadBound(const char* filefilter)
{
	AddFilesToList(filefilter, ms_BoundList, false );
	return true;
}

bool RagePlatform::LoadPathRegion(const char* filefilter)
{
	AddFilesToList(filefilter, ms_PathRegionList, false );
	return true;
}

bool RagePlatform::LoadNavMesh(const char* filefilter)
{
	AddFilesToList(filefilter, ms_NavMeshList, false );
	return true;
}

bool RagePlatform::LoadHeightMesh(const char* filefilter)
{
	AddFilesToList(filefilter, ms_HeightMeshList, false );
	return true;
}

bool RagePlatform::LoadAudMesh(const char* filefilter)
{
	AddFilesToList(filefilter, ms_AudMeshList, false);
	return true;
}

bool RagePlatform::LoadHierarchicalNav(const char* filefilter)
{
	AddFilesToList(filefilter, ms_HierarchicalNavList, false );
	return true;
}

bool RagePlatform::LoadWaypointRecording(const char* filefilter)
{
	AddFilesToList(filefilter, ms_WaypointRecordingList, false );
	return true;
}

bool RagePlatform::LoadEffect(const char* filefilter)
{
	AddFilesToList(filefilter, ms_EffectList, false );
	return true;
}

bool RagePlatform::LoadVehicleRecording(const char* filefilter)
{
	AddFilesToList(filefilter, ms_VehicleRecordingList, false );
	return true;
}

bool RagePlatform::LoadMeta(const char* filefilter)
{
	AddFilesToList(filefilter, ms_MetaList, false );
	return true;
}

bool RagePlatform::LoadScript( const char* filefilter )
{
	AddFilesToList( filefilter, ms_ScriptList, false );
	return (true);
}

bool RagePlatform::LoadOccMeshes( const char* filefilter )
{
	AddFilesToList( filefilter, ms_OccMeshList, false );
	return (true);
}

bool RagePlatform::LoadFrameFilter(const char* filename)
{
	AddFilesToList(filename, ms_FrameFilterList, false);
	return true;
}

bool RagePlatform::LoadNMData(const char* file)
{
	ms_NMData = file;
	return true;
}

//
// name:		SaveObject
// description:	template function for saving a single object. save the last on the list
template <class T> bool SaveObject(const char* filename, const char* extension, string_list& list, int32 version, size_t size = DEFAULT_BUILD_SIZE)
{
	bool result = false;

	int32 listSize = list.size();
	if(listSize == 0)
	{
		Warningf("save_object: There is nothing to save - %s", filename);
		return false;
	}

	for (int pass=0; pass<=pgRscBuilder::LAST_PASS; ++pass)
	{
		grcTextureFactory::GetInstance().ClearFreeList();

		// begin resource build
		RageBuilder::BeginBuild(filename, extension, pass==1?true:false, size);

		// load drawable
		T* pObject;
		const char* name = (*list.begin()).c_str();
		if(LoadObject(name, pObject) == false)
		{
			RageBuilder::EndBuild(pObject);
			return false;
		}

		if (pass==pgRscBuilder::LAST_PASS)
		{
			pgRscBuilder::ReadyBuild(pObject);
			size_t filesize = 0;
			datTypeStruct myStruct;
			pObject->DeclareStruct(myStruct);

			result = pgRscBuilder::SaveBuild(filename, extension, version, &filesize);
			CheckFileSize(filename, extension, filesize);

			// Can't delete the type if we byte swapped it...
			if (!RageBuilder::GetPlatformImpl()->IsEndianSwapping())
			{
				//Hack fix				pObject->Release();
			}
		}

		RageBuilder::EndBuild(pObject);
	}

	return result;
}

//
// name:		SaveObject
// description:	template function for saving a single object. save the last on the list
template <class T> bool SavePgBaseWrapperObject(const char* filename, const char* extension, string_list& list, int32 version, size_t size = DEFAULT_BUILD_SIZE)
{
	bool result = false;

	int32 listSize = list.size();
	if(listSize == 0)
	{
		Warningf("save_object: There is nothing to save - %s", filename);
		return false;
	}

	for (int pass=0; pass<=pgRscBuilder::LAST_PASS; ++pass)
	{
		grcTextureFactory::GetInstance().ClearFreeList();

		// begin resource build
		RageBuilder::BeginBuild(filename, extension, pass==1?true:false, size);

		// load drawable
		T* pObject;
		const char* name = (*list.begin()).c_str();

		pgBaseWrapper<datOwner<T>>* pWrapper = new pgBaseWrapper<datOwner<T>>();

		if(LoadObject(name, pObject) == false)
		{
			delete pWrapper;
			RageBuilder::EndBuild(pObject);
			return false;
		}

		pWrapper->Get() = pObject;

		if (pass==1)
		{
			size_t filesize = 0;
			datTypeStruct myStruct;
			pWrapper->DeclareStruct(myStruct);

			result = pgRscBuilder::SaveBuild(filename, extension, version, &filesize);
			CheckFileSize(filename, extension, filesize);

			// Can't delete the type if we byte swapped it...
			if (!RageBuilder::GetPlatformImpl()->IsEndianSwapping())
			{
				//Hack fix				pObject->Release();
			}
		}

		RageBuilder::EndBuild(pWrapper);
	}

	return result;
}

bool RagePlatform::SaveAnim(const char* filename)
{
	bool bResult = SaveObject<crAnimation>(filename, RageBuilder::GetPlatformExtension(ANIM_EXT), ms_AnimList, ANIM_VERSION);
	ms_AnimList.clear();
	return bResult;
}

#if ENABLE_BLENDSHAPES
//
// name:		SaveObject
// description:	template function for saving a single object. save the last on the list
bool SaveBlendshapeObject(const char* filenameIn, const char* filenameOut, const char* extension, string_list& list, int32 version, size_t size = DEFAULT_BUILD_SIZE)
{
	bool result = false;

	int32 listSize = list.size();
	if(listSize == 0)
	{
		Warningf("save_object: There is nothing to save - %s", filenameOut);
		return false;
	}

	// load the drawable to go with the blendshape
	rmcDrawable* drawable = NULL;

	for (int pass=0; pass<=pgRscBuilder::LAST_PASS; ++pass)
	{
		grcTextureFactory::GetInstance().ClearFreeList();
		const char* name = (*list.begin()).c_str();
		// begin resource build
		RageBuilder::BeginBuild(filenameOut, extension, pass==1?true:false, size);

		{
			sysMemStartTemp();
			char drawablePath[MAX_PATH];
			ASSET.RemoveNameFromPath(drawablePath,MAX_PATH,name);
			
			drawable = new gtaDrawable;

			ASSET.PushFolder(drawablePath);
			AssertVerify(drawable->Load("entity.type"));	
			ASSET.PopFolder();
			sysMemEndTemp();
		}



		// load drawable
		grbTargetManager* pObject;
		
		if(!LoadObject(name, drawable, pObject))
		{
			RageBuilder::EndBuild(pObject);
			return false;
		}

		if(drawable)
		{
			sysMemStartTemp();
			delete drawable;
			sysMemEndTemp();
		}

		if (pass==pgRscBuilder::LAST_PASS)
		{
			pgRscBuilder::ReadyBuild(pObject);
			size_t filesize = 0;
			datTypeStruct myStruct;
			pObject->DeclareStruct(myStruct);

			result = pgRscBuilder::SaveBuild(filenameOut, extension, version, &filesize);
			CheckFileSize(filenameOut, extension, filesize);

			// Can't delete the type if we byte swapped it...
			if (!RageBuilder::GetPlatformImpl()->IsEndianSwapping())
			{
				//Hack fix				pObject->Release();
			}
		}

		RageBuilder::EndBuild(pObject);
	}

	return result;
}
#endif // ENABLE_BLENDSHAPES

bool RagePlatform::SaveBlendShape(const char* ENABLE_BLENDSHAPES_ONLY(filenameIn), const char* ENABLE_BLENDSHAPES_ONLY(filenameOut))
{
#if ENABLE_BLENDSHAPES
	return SaveBlendshapeObject(filenameIn, filenameOut, RageBuilder::GetPlatformExtension(BLENDSHAPE_EXT), ms_ModelList, BLENDSHAPE_VERSION);
#else // ENABLE_BLENDSHAPES
	return true;
#endif // ENABLE_BLENDSHAPES
}

bool RagePlatform::SaveModel(const char* filename)
{
	grcTexture::SetSuppressReferenceWarning();
	bool result = SaveObject<gtaDrawable>(filename, RageBuilder::GetPlatformExtension(DRAWABLE_EXT), ms_ModelList, DRAWABLE_VERSION);
	grcTexture::ClearSuppressReferenceWarning();
	return (result);
}

bool RagePlatform::SaveTexture(const char* filename)
{
	return false;
}

bool RagePlatform::SaveFragment(const char* filename)
{
	grcTexture::SetSuppressReferenceWarning();
	bool result = SaveObject<gtaFragType>(filename, RageBuilder::GetPlatformExtension(FRAGMENT_EXT), ms_FragmentList, FRAGMENT_VERSION);
	grcTexture::ClearSuppressReferenceWarning();
	return (result);
}

bool RagePlatform::SaveBound(const char* filename)
{
	return SaveObject<phBound>(filename, RageBuilder::GetPlatformExtension(BOUNDS_EXT), ms_BoundList, BOUNDS_DICTIONARY_VERSION);
}

bool RagePlatform::SavePathRegion(const char* filename)
{
	return SaveObject<CPathRegion>(filename, RageBuilder::GetPlatformExtension(PATHREGION_EXT), ms_PathRegionList, PATHREGION_VERSION);
}

bool RagePlatform::SaveNavMesh(const char* filename)
{
	return SaveObject<CNavMesh>(filename, RageBuilder::GetPlatformExtension(NAVMESH_EXT), ms_NavMeshList, NAVMESH_VERSION);
}

bool RagePlatform::SaveHeightMesh(const char* filename)
{
	return SaveObject<CNavMesh>(filename, RageBuilder::GetPlatformExtension(HEIGHTMESH_EXT), ms_HeightMeshList, NAVMESH_VERSION);
}

bool RagePlatform::SaveAudMesh(const char* filename)
{
	return SaveObject<agPolyMesh>(filename, RageBuilder::GetPlatformExtension(AUDMESH_EXT), ms_AudMeshList, AUDMESH_VERSION);
}

bool RagePlatform::SaveHierarchicalNav(const char* filename)
{
	return SaveObject<CHierarchicalNavData>(filename, RageBuilder::GetPlatformExtension(HIERARCHICAL_NAV_EXT), ms_HierarchicalNavList, HIERARCHICAL_NAV_VERSION);
}

bool RagePlatform::SaveWaypointRecording(const char* filename)
{
	return SaveObject<fwWaypointRecordingRoute>(filename, RageBuilder::GetPlatformExtension(WAYPOINT_RECORDING_EXT), ms_WaypointRecordingList, WAYPOINT_RECORDING_VERSION);
}

bool RagePlatform::SaveEffect(const char* filename)
{
	char oldPlatform = g_sysPlatform;
	g_sysPlatform = platform::WIN32PC;
	ptxManager::InitClass();
	g_sysPlatform = oldPlatform;
	RMPTFXMGR.CreatePointPool(1);
	RMPTFXMGR.CreateEffectInstPool(1);
	RMPTFXMGR.CreateDrawList("DrawList01");

	RMPTFX_REGISTER_BEHAVIOUR(ptxu_Decal);
	RMPTFX_REGISTER_BEHAVIOUR(ptxu_DecalPool);
	//RMPTFX_REGISTER_BEHAVIOUR(ptxu_DecalTrail);
	//RMPTFX_REGISTER_BEHAVIOUR(ptxu_Fire);
	RMPTFX_REGISTER_BEHAVIOUR(ptxu_FogVolume);
	RMPTFX_REGISTER_BEHAVIOUR(ptxu_Light);
	RMPTFX_REGISTER_BEHAVIOUR(ptxu_Liquid);
	RMPTFX_REGISTER_BEHAVIOUR(ptxu_River);
	RMPTFX_REGISTER_BEHAVIOUR(ptxu_ZCull);

	bool retval = SaveObject<ptxFxList>(filename, RageBuilder::GetPlatformExtension(EFFECT_EXT), ms_EffectList, EFFECT_VERSION);

	ptxManager::ShutdownClass();

	return retval;
}

bool RagePlatform::SaveVehicleRecording(const char* filename)
{
	return SaveObject<CVehicleRecording>(filename, RageBuilder::GetPlatformExtension(VEHICLERECORDING_EXT), ms_VehicleRecordingList, VEHICLERECORDING_VERSION);
}

bool RagePlatform::SaveRbf(const char * filename)
{
	int32 listSize = ms_MetaList.size();
	if(listSize == 0)
	{
		Warningf("save_object: There is nothing to save - %s", filename);
		return false;
	}

	const char* inFilename = (*ms_MetaList.begin()).c_str();

	INIT_PARSER;

	parSettings s = PARSER.Settings();
	s.SetFlag(parSettings::CULL_OTHER_PLATFORM_DATA, true);
	s.SetFlag(parSettings::READ_SAFE_BUT_SLOW, true);
	parTree* tree = PARSER.LoadTree(inFilename, "", &s);
	bool result = PARSER.SaveTree(filename, RageBuilder::GetPlatformExtension(META_EXT), tree, parManager::BINARY);
	delete tree;

	SHUTDOWN_PARSER;

	ms_MetaList.clear();

	return result;
}

bool RagePlatform::SavePso(const char * filename, PsoFormat format)
{
	int32 listSize = ms_MetaList.size();
	if ( 0 == listSize )
	{
		Warningf("SavePso: There is nothing to save - %s", filename);
		return false;
	}

	const char* inFilename = (*ms_MetaList.begin()).c_str();

	bool result = ConvertXmlToPso(inFilename, filename, RageBuilder::GetPlatformExtension(META_EXT), format);

	ms_MetaList.clear();

	return result;
}

#define ReRegister(name) PARSER.RegisterStructure(name::parser_GetStaticStructure()->GetNameHash(), name::parser_GetStaticStructure());

void RagePlatform::LoadExternalStructureDefs(bool forceLoadExtstructs /* = false*/)
{
	if (!ms_pExternalParser)
	{
		ms_pExternalParser = rage_new parManager;
		ms_pExternalParser->Initialize(parSettings::sm_StandardSettings, false);

		parAddReflectionClassesToManager(*ms_pExternalParser); 

		parManager* oldMgr = parManager::sm_Instance;
		parManager::sm_Instance = ms_pExternalParser;				// Redirects PARSER to point to the new manager

		ms_pExternalParser->GetExternalStructureManager().LoadStructdefs(GetMetadataDefinitionsPath());

		parManager::sm_Instance = oldMgr;
	}

	if ((forceLoadExtstructs || PARAM_validatepsolayout.Get()) && !ms_pReferenceParser)
	{
		ms_pReferenceParser = rage_new parManager;
		ms_pReferenceParser->Initialize(parSettings::sm_StandardSettings, false);

		parAddReflectionClassesToManager(*ms_pReferenceParser);

		char toolsRoot[MAX_PATH] = {0};
		char realfilename[MAX_PATH];

		RageBuilder::GetConfig().GetToolsRootDir(toolsRoot, _MAX_PATH);
		sprintf_s(realfilename, _MAX_PATH, "%s/etc/content/extstructs", toolsRoot);

		pgRscBuilder::ConstructName(realfilename, MAX_PATH, realfilename, "#esd");

		parManager* oldMgr = parManager::sm_Instance;
		parManager::sm_Instance = ms_pReferenceParser;				// Redirects PARSER to point to the new manager

		ms_pReferenceParser->GetExternalStructureManager().LoadExternalStructureDefns(realfilename);

		parManager::sm_Instance = oldMgr;
	}
}

bool RagePlatform::ConvertXmlToPso(const char* inFilename, const char* outFilename, const char* outExtn, PsoFormat saveFormat)
{
	bool result = false;

	LoadExternalStructureDefs();

	// Redirects PARSER to point to the external manager (i.e. the defns from the PSC files) for this function
	parManager* oldMgr = parManager::sm_Instance;
	parManager::sm_Instance = ms_pExternalParser;

	parSettings s = PARSER.Settings();
	s.SetFlag(parSettings::CULL_OTHER_PLATFORM_DATA, true);
	s.SetFlag(parSettings::READ_SAFE_BUT_SLOW, true);
	parTree* tree = PARSER.LoadTree(inFilename, "", &s);

	if (!tree)
	{
		Errorf("Error loading XML file %s", inFilename);
		parManager::sm_Instance = oldMgr;
		return false;
	}

	// Find the type of whatever was stored in the tree - create an instance of it
	parStructure* toplevelStruct = PARSER.FindStructure(tree->GetRoot()->GetElement().GetName());
	if (toplevelStruct)
	{
		parPtrToStructure instanceStorage = toplevelStruct->Create();
		if (!PARSER.LoadFromStructure(tree->GetRoot(), *toplevelStruct, instanceStorage))
		{
			Errorf("Error converting XML file %s into runtime objects");
			delete tree;
			parManager::sm_Instance = oldMgr;
			return false;
		}

		char buf[RAGE_MAX_PATH];
		formatf(buf, "%s.%s", outFilename, outExtn);

		if (ms_pReferenceParser)
		{
			parCompareMemoryLayouts(instanceStorage, toplevelStruct, ms_pExternalParser, "ParserSchema", ms_pReferenceParser, "runtime", true);
		}

		if (saveFormat == PSO_IFF_FORMAT)
		{
			result = psoSaveFromStructure(buf, *toplevelStruct, instanceStorage, true);
		}
		else if (saveFormat == PSO_RSC_FORMAT)
		{
			result = psoRscSaveFromStructure(buf, *toplevelStruct, instanceStorage, true);
		}

		toplevelStruct->Destroy(instanceStorage);
	}
	else
	{
		Errorf("Couldn't find a definition for type '%s' in the metadata definitions (psc files), can't convert to PSO", tree->GetRoot()->GetElement().GetName());
		result = false;
	}

	delete tree;

	parManager::sm_Instance = oldMgr;

	return result;
}

// RETURNS:
//		-1 - error opening file
//		0 - file is not in-place loadable
//		1 - file is in-place loadable
int RagePlatform::IsPsoInPlaceLoadable(const char* filename, bool printMismatches )
{
	LoadExternalStructureDefs(true);

	// Redirects PARSER to point to the reference manager (i.e. the defns from the ESD files) for this function
	parManager* oldMgr = parManager::sm_Instance;
	parManager::sm_Instance = ms_pReferenceParser;

	psoFile* file = psoLoadFile(filename);

	int returnCode = 0;

	if (!file)
	{
		returnCode = -1;
	}
	else
	{
		if (file->GetFlagsRef().IsSet(psoFile::FLAG_ALL_SCHEMAS_MATCH))
		{
			returnCode = 1;
		}
		else if (printMismatches)
		{
			psoSchemaCatalog& schemaCat = file->GetSchemaCatalog();

			psoSchemaCatalog::StructureSchemaIterator iter = schemaCat.BeginSchemas();
			psoSchemaCatalog::StructureSchemaIterator endIter = schemaCat.EndSchemas();
			for(; iter != endIter; ++iter)
			{
				psoStructureSchema schema = *iter;
				if (!schema.CanUseFastLoading())
				{
					Displayf("File %s, schema %s (0x%x) can not use in-place loading",
						filename, 
						atLiteralHashString::TryGetString(schema.GetNameHash().GetHash()),
						schema.GetNameHash());
				}
			}
		}

		delete file;
	}

	parManager::sm_Instance = oldMgr;

	return returnCode;
}

int RagePlatform::PsoDebugStringSize(const char* filename)
{
	char* fileData = NULL;
	int fileSize = 0;
	bool needToFree = parUtils::MemoryMapFile(filename, fileData, fileSize);
	if (!fileData)
	{
		Displayf("Error opening file %s", filename);
		return 0;
	}

	int stringSize = 0;

	parIffHeader* header = reinterpret_cast<parIffHeader*>(fileData);
	if (header->GetMagic() == psoConstants::PSIN_MAGIC_NUMBER)
	{
		parIffFileReadIterator iter(fileData, fileData + fileSize);
		while(iter.GetChunk())
		{
			if (iter.GetMagicNumber() == psoConstants::STRS_MAGIC_NUMBER)
			{
				stringSize += (int)iter.GetChunkSize();
			}
			iter.NextChunk();
		}
	}
	else 
	{
		psoFile* f = psoLoadResource(filename, "", PSOLOAD_NO_PREP);
		psoResourceData* rsc = f->internal_GetResourceData();
		
		char* strPtr = rsc->m_NonFinalHashStrings.GetPtr();
		if (strPtr)
		{
			while(*strPtr)
			{
				int len = strlen(strPtr);
				stringSize += len + 1;
				strPtr += len + 1;
			}
			stringSize += 1;
		}
	}

	if(needToFree)
	{
		delete [] fileData;
	}

	return stringSize;
}


bool 
RagePlatform::SaveMapData( const char* filename )
{
	int32 listSize = ms_MetaList.size();
	if ( 0 == listSize )
	{
		Warningf("save_mapdata: There is nothing to save - %s", filename);
		return false;
	}

	const char* inFilename = (*ms_MetaList.begin()).c_str();

	bool result = ConvertXmlToPso(inFilename, filename, RageBuilder::GetPlatformExtension(PI_MAPDATA_EXT), PSO_RSC_FORMAT);

	ms_MetaList.clear();

	return result;
}

bool 
RagePlatform::SaveMapTypes( const char* filename )
{
	int32 listSize = ms_MetaList.size();
	if ( 0 == listSize )
	{
		Warningf("save_maptypes: There is nothing to save - %s", filename);
		return false;
	}

	const char* inFilename = (*ms_MetaList.begin()).c_str();

	bool result = ConvertXmlToPso(inFilename, filename, RageBuilder::GetPlatformExtension(PI_MAPTYPES_EXT), PSO_RSC_FORMAT);

	ms_MetaList.clear();

	return result;
}

bool
RagePlatform::SaveCutfile( const char* filename )
{
	int32 listSize = ms_MetaList.size();
	if ( 0 == listSize )
	{
		Warningf("save_cutfile: There is nothing to save - %s", filename);
		return false;
	}

	const char* inFilename = (*ms_MetaList.begin()).c_str();

	bool result = ConvertXmlToPso(inFilename, filename, RageBuilder::GetPlatformExtension(PI_CUTFILE_EXT), PSO_IFF_FORMAT);

	ms_MetaList.clear();

	return result;
}

bool 
RagePlatform::SaveImf( const char* filename )
{
	int32 listSize = ms_MetaList.size();
	if ( 0 == listSize )
	{
		Warningf("save_imf: There is nothing to save - %s", filename);
		return false;
	}

	const char* inFilename = (*ms_MetaList.begin()).c_str();

	bool result = ConvertXmlToPso(inFilename, filename, RageBuilder::GetPlatformExtension(PI_IMF_EXT), PSO_IFF_FORMAT);

	ms_MetaList.clear();

	return result;
}

bool 
RagePlatform::SaveScript( const char* filename )
{
	bool result = false;

	int32 listSize = ms_ScriptList.size();
	if(listSize == 0)
	{
		Warningf("save_script: There is nothing to save - %s", filename);
		return false;
	}

	const char* input = (*ms_ScriptList.begin()).c_str();
	const char* extension = RageBuilder::GetPlatformExtension(SCRIPT_EXT);

	scrProgram::InitClass();
	scrProgramId pid = scrProgram::Load( input );
	if ( pid == srcpidNONE )
	{
		Errorf( "Unable to load script object '%s'.", input );
		ms_ScriptList.clear();
		return ( false );
	}

	scrProgram* prog = scrProgram::GetProgram( pid );
	if ( !prog )
	{
		Errorf( "Unable to fetch loaded script object '%s'.", input );
		ms_ScriptList.clear();
		return ( false );
	}

	for (int pass=0; pass<=pgRscBuilder::LAST_PASS; ++pass)
	{
		// begin resource build
		RageBuilder::BeginBuild( input, extension, pass==1?true:false, SCRIPT_BUILD_SIZE );
		scrProgram* pt = rage_new scrProgram( *scrProgram::GetProgram( pid ) );

		if ( pass==pgRscBuilder::LAST_PASS )
		{
			pgRscBuilder::ReadyBuild(pt);
			size_t filesize = 0;
			datTypeStruct myStruct;
			pt->DeclareStruct(myStruct);

			result = pgRscBuilder::SaveBuild(filename, extension, scrProgram::RORC_VERSION, &filesize, true);
			CheckFileSize(filename, extension, filesize);
		}

		RageBuilder::EndBuild( pt );
	}
	scrProgram::GetProgram( pid )->Release( );

	scrProgram::ShutdownClass();

	ms_ScriptList.clear();

	return result;
}

static const OccludeModel::VertexFormat OCCLUDER_VFMT =  OccludeModel::VFMT_F32;
bool 
RagePlatform::SaveOccImap( const char* filename )
{
	bool result = false;

	int32 listSize = ms_OccMeshList.size();
	if(listSize == 0)
	{
		Warningf("save_occ_imap: There is nothing to save - %s", filename);
		return false;
	}

	const char* extension = PI_MAPDATA_EXT;

	fwMapData mapData;

	OccludeModel::SetCurrentVertexFormat(OCCLUDER_VFMT); 

	OccludeModelList *occList = rage_new OccludeModelList[listSize];
	int occIndex = 0;
	int totalMeshCount = 0;

	spdAABB boundingBox;
	boundingBox.Invalidate();
	
	for (string_list::iterator it = ms_OccMeshList.begin(); it != ms_OccMeshList.end(); ++it)
	{
		const char* input = (*it).c_str();

		const float bitPrecision = OCCLUDE_MODEL_DEFAULT_ERROR;
		LoadOccludeModel(occList[occIndex].m_models, input, ScalarVFromF32(bitPrecision));

		totalMeshCount += occList[occIndex].m_models.GetCount();

		occIndex++;
	}

	if (totalMeshCount == 0)
	{
		Warningf("save_occ_imap: None of the files contained any occluder mesh data - %s", filename);
		return false;
	}

	// Copy all the meshes into the master array.
	mapData.m_occludeModels.Reserve(totalMeshCount);

	for (int x=0; x<listSize; x++)
	{
		int meshCount = occList[x].m_models.GetCount();
		for (int y=0; y<meshCount; y++)
		{
			OccludeModel &model = occList[x].m_models[y];
			mapData.m_occludeModels.Append() = model;
			boundingBox.GrowAABB(spdAABB(model.GetMin(), model.GetMax()));
		}
	}
	
	mapData.SetStreamingExtents(boundingBox);
	mapData.SetPhysicalExtents(boundingBox);
	mapData.SetFlags(0);
	mapData.SetContentFlags(BIT5);
	mapData.SetName(ASSET.FileName(filename));

	{
		parTreeNode *tree = PARSER.BuildTreeNode(&mapData);
		tree->GetElement().SetName("CMapData");

		parTree mainTree;
		mainTree.SetRoot(tree);

		PARSER.SaveTree(filename, extension, &mainTree);
	}

	delete[] occList;

	return true;
}

bool 
RagePlatform::SaveOccXml( const char* filename )
{
	bool result = false;

	int32 listSize = ms_OccMeshList.size();
	if(listSize == 0)
	{
		Warningf("save_occ_xml: There is nothing to save - %s", filename);
		return false;
	}

	const char* extension = "xml";

	fwMapData mapData;

	OccludeModel::SetCurrentVertexFormat(OCCLUDER_VFMT); 

	OccludeModelList *occList = rage_new OccludeModelList[listSize];
	int occIndex = 0;
	int totalMeshCount = 0;

	spdAABB boundingBox;
	boundingBox.Invalidate();

	for (string_list::iterator it = ms_OccMeshList.begin(); it != ms_OccMeshList.end(); ++it)
	{
		const char* input = (*it).c_str();

		const float bitPrecision = OCCLUDE_MODEL_DEFAULT_ERROR;
		LoadOccludeModel(occList[occIndex].m_models, input, ScalarVFromF32(bitPrecision));

		totalMeshCount += occList[occIndex].m_models.GetCount();

		occIndex++;
	}

	if (totalMeshCount == 0)
	{
		Warningf("save_occ_xml: None of the files contained any occluder mesh data - %s", filename);
		return false;
	}

	// Copy all the meshes into the master array.
	mapData.m_occludeModels.Reserve(totalMeshCount);

	for (int x=0; x<listSize; x++)
	{
		int meshCount = occList[x].m_models.GetCount();
		for (int y=0; y<meshCount; y++)
		{
			OccludeModel &model = occList[x].m_models[y];
			mapData.m_occludeModels.Append() = model;
			boundingBox.GrowAABB(spdAABB(model.GetMin(), model.GetMax()));
		}
	}

	mapData.SetStreamingExtents(boundingBox);
	mapData.SetPhysicalExtents(boundingBox);
	mapData.SetFlags(0);
	mapData.SetContentFlags(BIT5);
	mapData.SetName(ASSET.FileName(filename));

	{
		parTreeNode *tree = PARSER.BuildTreeNode(&mapData);
		tree->GetElement().SetName("CMapData");

		parTree mainTree;
		mainTree.SetRoot(tree);

		PARSER.SaveTree(filename, extension, &mainTree);
	}

	delete[] occList;

	return true;
}

bool RagePlatform::SaveTextDatabase( const char* filename )
{
	return SaveObject<fwTextAsset>( filename, RageBuilder::GetPlatformExtension(TEXT_DATABASE_FILE_EXT), ms_MetaList, TEXT_DATABASE_VERSION );
}

bool RagePlatform::CompressAnim( const char* srcfile, const char* skelfile, const char* outfile, int maxblocksize, float compressionrotation, float compressiontranslation, float compressionscale, float compressiondefault, int compressioncost, int decompressioncost, const char* compressionrules, bool rawonly )
{
	// The guts of this is taken from RAGE animcompress tool

	// process animation:
	crAnimation srcAnim;

	if(!srcAnim.Load(srcfile))
	{
		Errorf("ERROR - Failed to load animation '%s'", srcfile);
		return false;
	}

	if(!srcAnim.IsRaw())
	{
		if(rawonly)
		{
			Errorf("ERROR - Source animation '%s' is not marked as raw, and raw only is specified", srcfile);
			return false;
		}
		else
		{
			Warningf("WARNING - Source animation '%s' is not marked as raw, may have already been compressed", srcfile);
		}
	}

	// process skeleton:
	crSkeletonData skelData;
	if( strcmp(skelfile, "") != 0 )
	{
		int version;
		if(!skelData.Load(skelfile, &version))
		{
			Errorf("ERROR - Failed to load skeleton file '%s'", skelfile);
			return false;
		}
	}

	// process tolerance:
	crAnimToleranceComplex tolerance;
	crAnimCompress::ProcessSimpleCompressionSettings(tolerance, compressionrotation, compressiontranslation, compressionscale, compressiondefault, (crAnimTolerance::eDecompressionCost)decompressioncost, (crAnimTolerance::eCompressionCost)compressioncost);
	if( !crAnimCompress::ProcessComplexCompressionRules(tolerance, compressionrules, skelfile?&skelData:NULL) )
		return false;

	// create new animation:
	crAnimation outAnim;
	if(!crAnimCompress::CompressAnimation(srcAnim, outAnim, tolerance, (u32)maxblocksize))
	{
		return false;
	}

	// save new animation file:
	if(!outAnim.Save(outfile))
	{
		Errorf("ERROR - Failed to save animation '%s'", outfile);
		return false;
	}

	return true;
}
static char s_cPropertyBuffer[1024];
const char* RagePlatform::GetClipPropertyString( const char* propertyname, const char* clipfile, bool useLoader )
{
	crClip* pClipAnim = NULL;
	strcpy(s_cPropertyBuffer, "\0");
	atString retval;
	bool propFound = false;
	if( useLoader )
	{
		CAnimLoaderPack packloader;
		pClipAnim = crClip::AllocateAndLoad( clipfile, &packloader );
	}
	else 
	{
		CAnimLoader loader;
		pClipAnim = crClip::AllocateAndLoad( clipfile, &loader );
	}

	if( pClipAnim )
	{
		crProperties* pProperties = pClipAnim->GetProperties();
		if( pProperties )
		{
			crProperty* pProperty = pProperties->FindProperty( propertyname );
			if( pProperty )
			{
				crPropertyAttribute* pAttribute = pProperty->GetAttribute( propertyname );
				if( pAttribute->GetType() == crPropertyAttribute::kTypeString )
				{
					crPropertyAttributeString* pAttributeString = ( crPropertyAttributeString* )pAttribute;
					strcpy(s_cPropertyBuffer, pAttributeString->GetString());
					propFound = true;
				}
			}
		}
		delete pClipAnim;
	}
	if(propFound)
		return s_cPropertyBuffer;
	else
		return NULL;
}

int RagePlatform::GetClipPropertyInt( const char* propertyname, const char* clipfile, bool useLoader )
{
	crClip* pClipAnim = NULL;
	int retval = -1;
	if( useLoader )
	{
		CAnimLoaderPack packloader;
		pClipAnim = crClip::AllocateAndLoad( clipfile, &packloader );
	}
	else 
	{
		CAnimLoader loader;
		pClipAnim = crClip::AllocateAndLoad( clipfile, &loader );
	}

	if( pClipAnim )
	{
		crProperties* pProperties = pClipAnim->GetProperties();
		if( pProperties )
		{
			crProperty* pProperty = pProperties->FindProperty( propertyname );
			if( pProperty )
			{
				crPropertyAttribute* pAttribute = pProperty->GetAttribute( propertyname );
				if( pAttribute->GetType() == crPropertyAttribute::kTypeInt )
				{
					crPropertyAttributeInt* pAttributeInt = ( crPropertyAttributeInt* )pAttribute;
					retval =  pAttributeInt->GetInt();
				}
			}
		}
		delete pClipAnim;
	}

	return retval;
}


float RagePlatform::GetClipPropertyFloat( const char* propertyname, const char* clipfile, bool useLoader )
{
	crClip* pClipAnim = NULL;
	float retval = -1.0f;
	if( useLoader )
	{
		CAnimLoaderPack packloader;
		pClipAnim = crClip::AllocateAndLoad( clipfile, &packloader );
	}
	else 
	{
		CAnimLoader loader;
		pClipAnim = crClip::AllocateAndLoad( clipfile, &loader );
	}


	if( pClipAnim )
	{
		crProperties* pProperties = pClipAnim->GetProperties();
		if( pProperties )
		{
			crProperty* pProperty = pProperties->FindProperty( propertyname );
			if( pProperty )
			{
				crPropertyAttribute* pAttribute = pProperty->GetAttribute( propertyname );
				if( pAttribute->GetType() == crPropertyAttribute::kTypeFloat )
				{
					crPropertyAttributeFloat* pAttributeFloat = ( crPropertyAttributeFloat* )pAttribute;
					retval = pAttributeFloat->GetFloat();
				}
			}
		}
		delete pClipAnim;
	}
	return retval;
}

bool RagePlatform::SetClipPropertyString( const char* propertyname, const char* val, const char* clipfile, bool useLoader )
{
	crClip* pClipAnim = NULL;

	if( useLoader )
	{
		CAnimLoaderPack packloader;
		pClipAnim = crClip::AllocateAndLoad( clipfile, &packloader );
	}
	else
	{
		CAnimLoader loader;
		pClipAnim = crClip::AllocateAndLoad( clipfile, &loader );
	}

	if( pClipAnim )
	{
		crProperties* pProperties = pClipAnim->GetProperties();
		if( pProperties )
		{
			crProperty* pProperty = pProperties->FindProperty( propertyname );
			if( pProperty )
			{
				crPropertyAttribute* pAttribute = pProperty->GetAttribute( propertyname );
				if( pAttribute->GetType() == crPropertyAttribute::kTypeString )
				{
					crPropertyAttributeString* pPropertyAttributeStr = (crPropertyAttributeString*)pAttribute;
					pPropertyAttributeStr->GetString() = atString(val);
				}
			}
			pClipAnim->Save( clipfile );
			return true;
		}
	}
	return false;
}

bool RagePlatform::ConvertAndSaveDDS( const char* ddsFileSrc, const char* ddsFileDest, const char* templateName, const char* platform )
{
	
	return false;
}

bool RagePlatform::SetDwdType( const char* dwdtype )
{
	static const char *	s_dwdStrings[dwdCOUNT] = 
	{
		"HD",			
		"LOD",				
		"SLOD1",			
		"SLOD2",			
		"SLOD3",			
		"SLOD4"				
	};

	for (int i = 0; i < NELEM(s_dwdStrings);++i)
	{
		if (stricmp(dwdtype, s_dwdStrings[i]) == 0)
		{
			ms_eDwdType = static_cast<DWDTYPE>(i);
			return true;
		}
	}
	Errorf("Unknown dwdtype (%s)", dwdtype);
	return false;
}

bool RagePlatform::GenerateDrawableStats(const char* drawablePathname, const char* outputPathname)
{
	// Required to allow vertex data to be properly constructed
	RageBuilder::BeginBuild(drawablePathname, RageBuilder::GetPlatformExtension(DRAWABLE_EXT), false, DEFAULT_BUILD_SIZE);

	// Load the drawable
	gtaDrawable* drawable;
	if(!LoadObject(drawablePathname, drawable))
	{
		Errorf("GenerateDrawableStats: LoadObject('%s') failed.", drawablePathname);
		return false;
	}

	sysMemStartTemp();
	StatsSerializer* statsSerializer = new StatsSerializer(outputPathname);
	statsSerializer->Serialize(*drawable);
	delete statsSerializer;
	sysMemEndTemp();

	// Tidy up
	RageBuilder::EndBuild(drawable);

	return true;
}

bool RagePlatform::GenerateFragmentStats(const char* fragmentPathname, const char* outputPathname)
{
	// We may want to alter this in future, but for now we can just load the fragment as a drawable
	return GenerateDrawableStats(fragmentPathname, outputPathname);
}

bool RagePlatform::SetEdgify( bool value )
{
	RagePlatformPs3::sm_Edgify = value;
	return true;
}

#if TINT_PALETTE_RESOURCES
////
#define VARNAME_TINT_PALETTE_TEX	("TintPaletteTex")
#define VARNAME_TINT_PAL_SELECTOR	("TintPaletteSelector")

// bright pink is end-of-palette marker:
#define END_OF_PALETTE_MARKER		(0xFFFF7FFF)	// A=255,R=255,G=127,B=255

static bool UnswizzleTextureData(void *dstPixels, const void *srcPixels, u32 bpp, u32 width, u32 height);
inline Color32 RawColorToColor32(Color32 colorDC, grcTexture::eTextureSwizzle *remap);

static void ProxyUnlockRect(grcTextureXenonProxy *proxy, const grcTextureLock &lock);
static bool ProxyLockRect(grcTextureXenonProxy* proxy, int layer, int mipLevel,grcTextureLock &lock, u32 uLockFlags);
static int  ProxyGetBitsPerPixel(grcTextureXenonProxy *proxy);

static void RemoveTintPalettesFromRsc(grmShaderGroup *pShaderGroup, bool bResourcingXenon, bool bResourcingPs3, bool bRemoveTintPalettesFromRsc);

//
//
//
//
class tintCache
{
public:
	struct rec
	{
		rec()		: m_numPalettes(-1), m_offset(-1)	{};
		atString	m_name;
		int			m_numPalettes;
		int			m_offset;
	};

public:
	tintCache()
	{
		m_tab.Reserve(16);
	};

	~tintCache() {}

	rec* Get(const char *texName)
	{
		for(int i=0; i<m_tab.GetCount(); i++)
		{
			if(m_tab[i].m_name == texName)
			{
				return( &m_tab[i] );
			}
		}
		return NULL;
	}

	rec* Add(const char *texName)
	{
		rec *found = Get(texName);
		if(found)
			return(found);

		rec newRec;
		newRec.m_name = texName;
		m_tab.Grow() = newRec;
		return &m_tab[ m_tab.GetCount()-1 ];
	}

private:
	atArray<rec>	m_tab;
};

//
//
//
//
static
atArray<u8>* PrepackageTint(rmcDrawable *pDrawable, const char *pFilename)
{
	const bool bResourcingPs3				= (RageBuilder::GetPlatform()==RageBuilder::pltPs3);
	const bool bResourcingXenon				= (RageBuilder::GetPlatform()==RageBuilder::pltXenon);
	const bool bRemoveTintPalettesFromRsc	= bResourcingPs3;		// removes tint palettes from resources (on ps3)

	const u32 TintPaletteSize=256;	// 256 colors in tint palette

	Assert(pDrawable);
	if(!pDrawable)
		return(NULL);

	grmShaderGroup *pShaderGroup = &pDrawable->GetShaderGroup();
	Assert(pShaderGroup);
	if(!pShaderGroup)
		return(NULL);

	bool bDoPrepackageTint=false;

	// Palette tint:
	if(pShaderGroup->CheckForShadersWithSuffix( "_tnt" ))
	{
		bDoPrepackageTint = true;
	}

	if(!bDoPrepackageTint)
		return(NULL);	// nothing to do

	if(!bResourcingPs3 && !bResourcingXenon)
		return(NULL);	// ps3 and 360 only atm

	// BS#988168: skip drawable dictionaries for now:
//	if( strstr(s_Buildname, ".#dd") )
//	{
//		Displayf("\nPrepackageTint: skipping: %s (as it's a drawable dictionary).", s_Buildname);
//		return(NULL);
//	}

	Displayf("\nPrepackageTint: processing: %s.", s_Buildname);

	const s32 shaderCount	= pShaderGroup->GetCount();
	Assert(shaderCount >= 1);
	
	// array of counted palettes for each shader:
	u32* shaderNumPalettes = Alloca(u32, shaderCount);
	sysMemSet(shaderNumPalettes, 0x00, shaderCount*sizeof(u32));


	u32 numAllocTintShaderInfos	= 0;
	u32 numAllocTintPalettes	= 0;	// ps3 only
	tintCache *pTintCache		= NULL;	// ps3 only
	sysMemStartTemp();
		pTintCache = new tintCache;
		Assert(pTintCache);
	sysMemEndTemp();
	grcTexture::eTextureSwizzle texColRemap[4]; // r,g,b,a

	for(s32 j=0; j<shaderCount; j++)
	{
		grmShader		*pShader = &pShaderGroup->GetShader(j);
		Assert(pShader);

		grcEffectVar	effectVarTintPalTex		= pShader->LookupVar(VARNAME_TINT_PALETTE_TEX, FALSE);
		grcEffectVar	effectVarTintPalSelector= pShader->LookupVar(VARNAME_TINT_PAL_SELECTOR, FALSE);

		if(effectVarTintPalTex && effectVarTintPalSelector)
		{
			// find out how many palettes are stored in texture:
			const grcInstanceData& instanceData = pShader->GetInstanceData();
			const int index = (int)effectVarTintPalTex - 1;
			grcInstanceData::Entry *pEntry = &instanceData.Data()[index];
			grcTexture *pTintTexture = pEntry->Texture;
			if (!pTintTexture)
			{
				Errorf("%s (%s): TintPalTexture not found!", s_Buildname, pFilename);
				continue;
			}

			const bool bIsWidthAppropriate	= (pTintTexture->GetWidth()== (bResourcingXenon?64:256));
			const bool bIsHeightAppropriate = (pTintTexture->GetHeight()>=4);
			const bool bIsBPPAppropriate	= bResourcingXenon? (ProxyGetBitsPerPixel(static_cast<grcTextureXenonProxy*>(pTintTexture))==32) : (pTintTexture->GetBitsPerPixel()==32);

			// special case: detect proxy texture (there may be one in some odd *_lod.#dr drawable):
			if( (pTintTexture->GetWidth()	==0)	&&
				(pTintTexture->GetHeight()	==0)	&&
				(bResourcingXenon? (ProxyGetBitsPerPixel(static_cast<grcTextureXenonProxy*>(pTintTexture))==0) : (pTintTexture->GetBitsPerPixel()==0)) )
			{
				Displayf("\nPrepackageTint: skipping: %s (as it contains proxy textures: %s).", s_Buildname, pTintTexture->GetName());
				
				// remove all *removable* tint palettes so ragebuilder has chance to build some (incomplete) resources:
				Displayf("\nProxy tint textures fallback:");
				RemoveTintPalettesFromRsc(pShaderGroup, bResourcingXenon, bResourcingPs3, bRemoveTintPalettesFromRsc);
				return(NULL);
			}

			// check texture dimensions and format (ARGB8888==32bpp):
			if(!bIsWidthAppropriate)
			{
				Errorf("%s (%s): TintPalTexture '%s' must have width=%d (current: %d).", s_Buildname, pFilename, pTintTexture->GetName(), bResourcingXenon?64:256, pTintTexture->GetWidth());
			}

			if(!bIsHeightAppropriate)
			{
				Errorf("%s (%s): TintPalTexture '%s' must have height>=4 (current: %d).", s_Buildname, pFilename, pTintTexture->GetName(), pTintTexture->GetHeight());	// ps3 & 360 min. tex height is 4
			}

			if(!bIsBPPAppropriate)
			{
				Errorf("%s (%s): TintPalTexture '%s' must be uncompressed 32bpp (ARGB8888) (current bpp: %d).", s_Buildname, pFilename, pTintTexture->GetName(), pTintTexture->GetBitsPerPixel());
			}
			//Printf("\n tint palette: w=%d, h=%d, bpp=%d", pTintTexture->GetWidth(), pTintTexture->GetHeight(), pTintTexture->GetBitsPerPixel());
			if(!bIsWidthAppropriate || !bIsHeightAppropriate || !bIsBPPAppropriate)
				continue;

			numAllocTintShaderInfos++;

			if(bResourcingPs3)
			{
				grcTextureGCM *pTintTextureGcm = static_cast<grcTextureGCM*>(pTintTexture);
				Assert(pTintTextureGcm);
				CellGcmTexture *cellTex = (CellGcmTexture*)pTintTextureGcm->GetTexturePtr();
				Assert(cellTex);

				// is texture swizzled?
				const bool bIsSwizzled = !(cellTex->format&CELL_GCM_TEXTURE_LN);
				if(bIsSwizzled)
				{
					Warningf("%s: TintPalTexture '%s' is not linear! Please set its TCP code to linear and re-export your resources.", s_Buildname, pTintTexture->GetName());
				#if 1 //__BANK
					// ...then unswizzle:
					u32 *swizzledTexPtr = (u32*)pTintTextureGcm->GetBits();
					Assert(swizzledTexPtr);
					const u32 width = pTintTextureGcm->GetWidth();
					const u32 height= pTintTextureGcm->GetHeight();
					const u32 bpp	= 32;

					// allocate temp space on the stack:
					const u32 stackAllocSize = width*height*sizeof(u32);
					Assert(stackAllocSize <= 16*1024);	// don't want to alloc more than 16KB on stack
					u32 *unswizzledTexPtr = Alloca(u32, stackAllocSize/sizeof(u32));
					Assert(unswizzledTexPtr);
					if(!UnswizzleTextureData(unswizzledTexPtr, swizzledTexPtr, bpp, width, height))
					{
						Errorf("Error unswizzling texture!");
					}

					// overwrite src tex data with unswizzled data:
					memcpy(swizzledTexPtr, unswizzledTexPtr, stackAllocSize);	// sysMemCpy() fails to copy from stack!

					// set linear flag:
					cellTex->format |= CELL_GCM_TEXTURE_LN;
				#else
					TrapZ(0);	// fatal: can't work with non-linear textures...
				#endif //__BANK...
				}// if(bIsSwizzled)...

				pTintTextureGcm->GetTextureSwizzle(texColRemap[0], texColRemap[1], texColRemap[2], texColRemap[3]);

				// find out # of real palettes (check for end-of-palette markers on col[0] place):
				u32 *tintPalettePtr = (u32*)pTintTextureGcm->GetBits();
				Assert(tintPalettePtr);
				u32 numPalettes = pTintTexture->GetHeight();
				
				const u32 height = pTintTexture->GetHeight();
				for(u32 p=0; p<height; p++)
				{
					Color32 *rawCol32 = (Color32*)(tintPalettePtr + p*TintPaletteSize);
					const Color32 firstColor = RawColorToColor32( rawCol32[0], texColRemap );
					
					if(firstColor.GetColor() == END_OF_PALETTE_MARKER)	// end-of-palette marker on first place?
					{
						numPalettes = p;
						break;
					}
				}
				if(numPalettes==0)
					Errorf("%s: No tint palettes detected in texture '%s'.", s_Buildname, pTintTexture->GetName());
				if(numPalettes > (u32)pTintTexture->GetHeight())
					Errorf("%s: Num tint palettes exceeds height of tint texture '%s'.", s_Buildname, pTintTexture->GetName());

				sysMemStartTemp();
					tintCache::rec *pTintRec = pTintCache->Get(pTintTexture->GetName());
					if(pTintRec)
					{	// palette already in cache:
						Assert(pTintRec->m_numPalettes == numPalettes);					
					}
					else
					{	// tint palette not in cache (yet):
						tintCache::rec *pTintRec	= pTintCache->Add(pTintTexture->GetName());
						pTintRec->m_numPalettes		= numPalettes;
						pTintRec->m_offset			= -1;
						
						numAllocTintPalettes		+= numPalettes;
					}
				sysMemEndTemp();

				shaderNumPalettes[j] = numPalettes;	// store num palettes detected for this shader
			}// if(bResourcingPs3)...

			if(bResourcingXenon)
			{
				grcTextureXenonProxy *pTintTextureProxy360 = static_cast<grcTextureXenonProxy*>(pTintTexture);
				Assert(pTintTextureProxy360);
				D3DBaseTexture *d3dTex = (D3DBaseTexture*)pTintTextureProxy360->GetTexturePtr();
				Assert(d3dTex);

				grcTextureLock lock;
				const bool bLocked = ProxyLockRect(pTintTextureProxy360, 0, 0, lock, grcsRead);
				if(bLocked)
				{
					// only ARGB88888 swizzled or linear allowed:
					if(lock.RawFormat!=D3DFMT_A8R8G8B8 && lock.RawFormat!=D3DFMT_LIN_A8R8G8B8)
						Errorf("%s: Incorrect xenon tint palette format detected in %s.", s_Buildname, pTintTexture->GetName());

					const u32 width		= pTintTexture->GetWidth();
					const u32 height	= pTintTexture->GetHeight();

					u32* tintPalTmp = Alloca(u32, width*height);

					const bool bIsTiled = (lock.RawFormat==D3DFMT_A8R8G8B8);
					if(bIsTiled)
					{	
						D3DFORMAT format = static_cast<D3DFORMAT>(lock.RawFormat);
						Assert(XGIsTiledFormat(format));
						Assert(u32(lock.Pitch)==width*sizeof(u32));
						
						DWORD flags = 0;
						if (!XGIsPackedTexture(d3dTex))
						{
							flags |= XGTILE_NONPACKED;
						}
						if (XGIsBorderTexture(d3dTex))
						{
							flags |= XGTILE_BORDER;
						}
						// untile:
						XGUntileTextureLevel(width, height, /*Level*/0, /*GpuFormat*/XGGetGpuFormat(format), flags,
											/*pDestination*/tintPalTmp, /*RowPitch*/lock.Pitch, /*pPoint*/NULL, /*pSource*/lock.Base, /*pRect*/NULL);
					}
					else
					{
						sysMemCpy(tintPalTmp, lock.Base, width*height*sizeof(u32));
					}
					ProxyUnlockRect(pTintTextureProxy360, lock);

					pTintTexture->GetTextureSwizzle(texColRemap[0], texColRemap[1], texColRemap[2], texColRemap[3]);

					// find out # of real palettes (check for end-of-palette markers on col[0] place):
					u32 *tintPalettePtr = (u32*)tintPalTmp;
					Assert(tintPalettePtr);
					s32 numPalettes = pTintTexture->GetHeight()/4;
					
					for(u32 p=0; p<height; p+=4)	// 64x4H
					{
						Color32 *rawCol32 = (Color32*)(tintPalettePtr + p*(TintPaletteSize/4));
						const Color32 firstColor = RawColorToColor32( rawCol32[0], texColRemap );
						
						if(firstColor.GetColor() == END_OF_PALETTE_MARKER)	// end-of-palette marker on first place?
						{
							numPalettes = p/4;
							break;
						}
					}

					if(numPalettes==0)
						Errorf("%s: No tint palettes detected in texture '%s'.", s_Buildname, pTintTexture->GetName());
					if(numPalettes > pTintTexture->GetHeight())
						Errorf("%s: Num tint palettes exceeds height of tint texture '%s'.", s_Buildname, pTintTexture->GetName());

					shaderNumPalettes[j] = numPalettes;	// store num palettes detected for this shader
				}// if(bLocked)...
				else
				{
					Errorf("%s: Error locking tint palette %s.", s_Buildname, pTintTexture->GetName());
				}
			}//if(bResourcingXenon)...

		}//if(effectVarTintPalTex && effectVarTintPalSelector)...
	}//for(s32 j=0; j<numShaders; j++)...


	if(numAllocTintShaderInfos==0)
	{
		Errorf("\n numAllocTintShaderInfos=0!");
		RemoveTintPalettesFromRsc(pShaderGroup, bResourcingXenon, bResourcingPs3, bRemoveTintPalettesFromRsc);
		return(NULL);
	}

	if(shaderCount > 255)
	{
		Errorf("\n ShaderCount bigger than 255!");	// must fit into u8
		RemoveTintPalettesFromRsc(pShaderGroup, bResourcingXenon, bResourcingPs3, bRemoveTintPalettesFromRsc);
		return(NULL);
	}

	if(numAllocTintShaderInfos > 255)
	{
		Errorf("\n numAllocTintShaderInfos bigger than 255!");	// must fit into u8
		RemoveTintPalettesFromRsc(pShaderGroup, bResourcingXenon, bResourcingPs3, bRemoveTintPalettesFromRsc);
		return(NULL);
	}

	if(bResourcingPs3)
	{
		if(numAllocTintPalettes==0)
		{
			Errorf("\n numAllocTintPalettes=0!");
			RemoveTintPalettesFromRsc(pShaderGroup, bResourcingXenon, bResourcingPs3, bRemoveTintPalettesFromRsc);
			return(NULL);
		}

		if(numAllocTintPalettes > 255)
		{
			Errorf("\n numAllocTintPalettes bigger than 255!");	// must fit into u8
			RemoveTintPalettesFromRsc(pShaderGroup, bResourcingXenon, bResourcingPs3, bRemoveTintPalettesFromRsc);
			return(NULL);
		}
	}

	atArray<u8>* rscTintData	= NULL;
	u32 tintDataSize			= 0;
	u8* tintDataPtr				= NULL;

	if(bResourcingPs3)
	{
		const u32 tintPalSize	=	numAllocTintPalettes * TintPaletteSize*sizeof(u8)*4;	// all tint palette data
		const u32 tintHeaderSize=	sizeof(u8)*7 + sizeof(u8)*1;							// 8 bytes: header ('TNTC000') + shaderCount
		const u32 tintShaderSize=	sizeof(structTintShaderInfo)*numAllocTintShaderInfos;	// 4 bytes per shader data
		
		if((tintHeaderSize + tintShaderSize) > 1024)
		{
			Errorf("\n%s: Too many tint data to write (exceeds 1KB)!", s_Buildname);
			// remove all *removable* tint palettes so ragebuilder has chance to build some (incomplete) resources:
			RemoveTintPalettesFromRsc(pShaderGroup, bResourcingXenon, bResourcingPs3, bRemoveTintPalettesFromRsc);
			return(NULL);
		}

		if(numAllocTintPalettes > 63)
		{
			Errorf("%s: Too many (%d) tint palettes in object (limit is 63). To fix decrease amount of used tint shaders and/or size of their tint palettes!", s_Buildname, numAllocTintPalettes);
			// remove all *removable* tint palettes so ragebuilder has chance to build some (incomplete) resources:
			RemoveTintPalettesFromRsc(pShaderGroup, bResourcingXenon, bResourcingPs3, bRemoveTintPalettesFromRsc);
			return(NULL);
		}

		tintDataSize = tintPalSize + tintHeaderSize + tintShaderSize;
	}

	if(bResourcingXenon)
	{
		const u32 tintHeaderSize=	sizeof(u8)*7 + sizeof(u8)*1;							// 8 bytes: header ('TNTX000') + shaderCount
		const u32 tintShaderSize=	sizeof(structTintShaderInfo)*numAllocTintShaderInfos;	// 4 bytes per shader data
		
		if((tintHeaderSize + tintShaderSize) > 1024)
		{
			Errorf("\n%s: Too many tint data to write (exceeds 1KB)!", s_Buildname);
			// remove all *removable* tint palettes so ragebuilder has chance to build some (incomplete) resources:
			RemoveTintPalettesFromRsc(pShaderGroup, bResourcingXenon, bResourcingPs3, bRemoveTintPalettesFromRsc);
			return(NULL);
		}

		tintDataSize = tintHeaderSize + tintShaderSize;
	}

	rscTintData = rage_new atArray<u8>();
	if(tintDataSize >= atArray<u8>::_CounterMax)
	{
		Errorf("\n%s: Resourced tint data too big (exceeds 64KB)!", s_Buildname);
		// remove all *removable* tint palettes so ragebuilder has chance to build some (incomplete) resources:
		RemoveTintPalettesFromRsc(pShaderGroup, bResourcingXenon, bResourcingPs3, bRemoveTintPalettesFromRsc);
		return(NULL);
	}

	rscTintData->Resize(tintDataSize);
	tintDataPtr = rscTintData->GetElements();
	Assert(tintDataPtr);
	sysMemSet(tintDataPtr, 0x00, tintDataSize);

	if((tintDataSize==0) || (tintDataPtr==NULL))
	{
		Errorf("\n %s: Invalid tint data!", s_Buildname);
		RemoveTintPalettesFromRsc(pShaderGroup, bResourcingXenon, bResourcingPs3, bRemoveTintPalettesFromRsc);
		return(NULL);
	}

	u32 numGlobalTintShaderInfos= 0;
	u32 numGlobalTintPalettes	= 0;
	u8* tintDataPtrCurr	= tintDataPtr;
	u32 numTintShaderInfo = 0;
	structTintShaderInfo* tintInfos = Alloca(structTintShaderInfo, numAllocTintShaderInfos);

	atArray<atString> *tintPalsToRemove=NULL;	// array of tint palettes to be removed from resources
	if(bRemoveTintPalettesFromRsc)
	{
		sysMemStartTemp();
			tintPalsToRemove = new atArray<atString>;
			tintPalsToRemove->Reserve(16);
		sysMemEndTemp();
	}

	for(s32 j=0; j<shaderCount; j++)
	{
		grmShader		*pShader = &pShaderGroup->GetShader(j);
		Assert(pShader);

		grcEffectVar	effectVarTintPalTex		= pShader->LookupVar(VARNAME_TINT_PALETTE_TEX, FALSE);
		grcEffectVar	effectVarTintPalSelector= pShader->LookupVar(VARNAME_TINT_PAL_SELECTOR, FALSE);

		if(effectVarTintPalTex && effectVarTintPalSelector)
		{
			const bool bIsTreeShader = strstr(pShader->GetName(),"trees") != NULL;

			// find out how many palettes are stored in texture:
			const grcInstanceData& instanceData = pShader->GetInstanceData();
			const int index = (int)effectVarTintPalTex - 1;
			grcInstanceData::Entry *pEntry = &instanceData.Data()[index];
			grcTexture *pTintTexture = pEntry->Texture;
			if(!pTintTexture)
			{
				Errorf("%s: TintPalTexture not found!", s_Buildname);
				continue;
			}

			const bool bIsWidthAppropriate	= (pTintTexture->GetWidth()==(bResourcingXenon?64:256));
			const bool bIsHeightAppropriate = (pTintTexture->GetHeight()>=4);
			const bool bIsBPPAppropriate	= bResourcingXenon? (ProxyGetBitsPerPixel(static_cast<grcTextureXenonProxy*>(pTintTexture))==32) : (pTintTexture->GetBitsPerPixel()==32);
			if(!bIsWidthAppropriate)
			{
				Errorf("%s: TintPalTexture '%s' must have width=%d (current: %d).", s_Buildname, pTintTexture->GetName(), bResourcingXenon?64:256, pTintTexture->GetWidth());
			}

			if(!bIsHeightAppropriate)
			{
				Errorf("%s: TintPalTexture '%s' must have height>=4 (current: %d).", s_Buildname, pTintTexture->GetName(), pTintTexture->GetHeight());	// ps3 & 360 min. tex height is 4
			}

			if(!bIsBPPAppropriate)
			{
				Errorf("%s: TintPalTexture '%s' must be uncompressed 32bpp (ARGB8888).", s_Buildname, pTintTexture->GetName());
			}

			if(!bIsWidthAppropriate || !bIsHeightAppropriate || !bIsBPPAppropriate)
				continue;

			const u32 tintPaletteTexHeight = pTintTexture->GetHeight();

			u32 numTintPalettes = tintPaletteTexHeight;	// pc

			if(bResourcingPs3)
			{
				numTintPalettes = shaderNumPalettes[j];	// ps3: real # of useful palettes for this shader
			}
			else if(bResourcingXenon)
			{
				numTintPalettes = shaderNumPalettes[j];	// 360: real # of useful palettes for this shader
			}
			
			if(numTintPalettes==0)
			{
				Errorf("\n %s: numTintPalettes=0!", s_Buildname);
				continue;
			}


			Vector2 vPaletteSelector(0,0);
			pShader->GetVar(effectVarTintPalSelector, vPaletteSelector);	// grab selected palette
			u32 nPaletteSelector = (u32)vPaletteSelector.x; 
			Assertf(nPaletteSelector<numTintPalettes,	"TintPalTexture '%s': Selected palette is too big!", pTintTexture->GetName());
			if(nPaletteSelector >= numTintPalettes)
				nPaletteSelector = numTintPalettes-1;

			structTintShaderInfo* tintInfo = tintInfos + numTintShaderInfo++;
			numGlobalTintShaderInfos++;
	
			if(numTintPalettes > 255)		// must fit into 255
			{
				Errorf("\n %s: numTintPalettes exceeds 255!", s_Buildname);
				return(NULL);
			}
//			Assert(nPaletteSelector < 128);			// must fit into 7 bits

			if(numGlobalTintPalettes > 255)			// must fit into u8
			{
				Errorf("\n %s: numGlobalTintPalettes exceeds 255!", s_Buildname);
				return(NULL);
			}
//			Assert(effectVarTintPalSelector < 256);	// must fit into u8
			
			if(tintPaletteTexHeight > 255)			// must fit into u8
			{
				Errorf("\n %s: tintPaletteTexHeight exceeds 255!", s_Buildname);
				return(NULL);
			}

			tintInfo->m_DefaultPalette			= (u8)nPaletteSelector;		// currently selected palette
			tintInfo->m_PaletteTexHeight		= (u8)numTintPalettes;
			tintInfo->m_bIsTree					= bIsTreeShader;

		
			if(bResourcingPs3)
			{
				sysMemStartTemp();
					tintCache::rec *pTintRec = pTintCache->Get(pTintTexture->GetName());
					Assert(pTintRec);	// must be found
				sysMemEndTemp();

				if(pTintRec->m_offset == -1)
				{
					pTintRec->m_offset			= numGlobalTintPalettes;
					tintInfo->m_PaletteOffset	= (u8)numGlobalTintPalettes;// offset to first palette in big palette buffer
				
					numGlobalTintPalettes		+= numTintPalettes;			// increase global tint palette offset

					// copy texture into local palette storage:
					grcTextureGCM *pTintTextureGcm = static_cast<grcTextureGCM*>(pTintTexture);
					Assert(pTintTextureGcm);
					CellGcmTexture *cellTex = (CellGcmTexture*)pTintTextureGcm->GetTexturePtr();
					Assert(cellTex);
					Assert(cellTex->format&CELL_GCM_TEXTURE_LN);	// is texture unswizzled?

					u32 *tintPalettePtr = (u32*)pTintTextureGcm->GetBits();
					Assert(tintPalettePtr);

					for(u32 p=0; p<numTintPalettes; p++)
					{
						u32 *tintPaletteSrc = tintPalettePtr + TintPaletteSize*p;
						u32 *tintPaletteDst = (u32*)tintDataPtrCurr;
						tintDataPtrCurr += TintPaletteSize*sizeof(u32);
						sysMemCpy(tintPaletteDst, tintPaletteSrc, TintPaletteSize*sizeof(u32));
					
					#if __RESOURCECOMPILER
						// conversion not required - tint palette is already in ARGB format in memory
					#else
						// convert raw DeviceColor palette into Color32 palette
						Color32 *col32 = (Color32*)tintPaletteDst;
						for(u32 c=0; c<TintPaletteSize; c++)
						{
							col32[c] = RawColorToColor32( col32[c], texColRemap );
		//					Color32 c32 = col32[c];
		//					Printf("\n pal=%d: c=%d: %d %d %d %d", p, c, c32.GetRed(), c32.GetGreen(), c32.GetBlue(), c32.GetAlpha());
						}
					#endif
					}// for(u32 p=0; p<numTintPalettes; p++)...

				}
				else
				{
					tintInfo->m_PaletteOffset	= pTintRec->m_offset;
				}
		
				if(bRemoveTintPalettesFromRsc)
				{
					pShader->SetVar(effectVarTintPalTex, (const grcTexture*)NULL);
					sysMemStartTemp();
						tintPalsToRemove->Grow() = pTintTexture->GetName();
					sysMemEndTemp();
				}
			}//if(bResourcingPs3)...
			else if(bResourcingXenon)
			{
				tintInfo->m_varPaletteTintSelector	= (u8)effectVarTintPalSelector;
			}

			tintInfo->m_ShaderIdx	= (u8)j;

		}// if(effectVarTintPalTex && effectVarTintPalSelector)...
	
	}//for(s32 j=0; j<numShaders; j++)...
	
	// check if we haven't overwritten anything?
	if(tintDataPtrCurr > (tintDataPtr+tintDataSize))
	{
		Errorf("\n %s: Memory has been overwritten!", s_Buildname);
		return(NULL);
	}

	// remaining space for tint header must be below 1K:
	if( (tintDataPtr+tintDataSize - tintDataPtrCurr) >= 1024 )
	{
		Errorf("\n %s: Bad memory for tint header!", s_Buildname);
		return(NULL);
	}

	// re-verify what we've just processed:
	if(numGlobalTintShaderInfos	!= numAllocTintShaderInfos)
	{
		Errorf("\n %s: numGlobalTintShaderInfos	!= numAllocTintShaderInfos!", s_Buildname);
		return(NULL);
	}

	if(bResourcingPs3)
	{
		if(numGlobalTintPalettes != numAllocTintPalettes)
		{
			Errorf("\n %s: numGlobalTintPalettes != numAllocTintPalettes!", s_Buildname);
			return(NULL);
		}
	}

	// write header tint data:
	if(bResourcingPs3)
	{
		tintDataPtrCurr[0] = 'T';	// magic header
		tintDataPtrCurr[1] = 'N';
		tintDataPtrCurr[2] = 'T';
		tintDataPtrCurr[3] = 'C';
		tintDataPtrCurr[4] = 0;
		tintDataPtrCurr[5] = 0;
		tintDataPtrCurr[6] = 0;
		tintDataPtrCurr += sizeof(u8)*7;
		
		tintDataPtrCurr[0] = (u8)numAllocTintShaderInfos;	// guaranteed to fit
		tintDataPtrCurr += sizeof(u8);

		sysMemCpy(tintDataPtrCurr, tintInfos, sizeof(structTintShaderInfo)*numAllocTintShaderInfos);
		tintDataPtrCurr += sizeof(structTintShaderInfo)*numAllocTintShaderInfos;

	}

	if(bResourcingXenon)
	{
		tintDataPtrCurr[0] = 'T';	// magic header
		tintDataPtrCurr[1] = 'N';
		tintDataPtrCurr[2] = 'T';
		tintDataPtrCurr[3] = 'X';
		tintDataPtrCurr[4] = 0;
		tintDataPtrCurr[5] = 0;
		tintDataPtrCurr[6] = 0;
		tintDataPtrCurr += sizeof(u8)*7;
		
		tintDataPtrCurr[0] = (u8)numAllocTintShaderInfos;	// guaranteed to fit
		tintDataPtrCurr += sizeof(u8);

		sysMemCpy(tintDataPtrCurr, tintInfos, sizeof(structTintShaderInfo)*numAllocTintShaderInfos);
		tintDataPtrCurr += sizeof(structTintShaderInfo)*numAllocTintShaderInfos;
	}


	if(tintDataPtrCurr != (tintDataPtr+tintDataSize))	// must fit exactly!
	{
		Errorf("\n: %s: Wrong tint memory write!", s_Buildname);
		return(NULL);
	}

	// remove ps3 tint palettes from resources:
	if(bRemoveTintPalettesFromRsc)
	{
		pgDictionary<grcTexture> *pTextDict = pShaderGroup->GetTexDict();

		const int count = tintPalsToRemove->GetCount();
		for(int i=0; i<count; i++)
		{
			const char* texName = (const char*)((*tintPalsToRemove)[i]);
			
			grcTexture *pTintTex = pTextDict->LookupLocal(texName);
			if(pTintTex)
			{
				Displayf("\nRemoving ps3 tint pal from resources (%d/%d): %s", i+1, count, texName);
				pTextDict->DeleteEntry(texName);	// remove from txd
				sysMemStartTemp();
					pTintTex->Release();			// kill texture
				sysMemEndTemp();
			}
#if 0
			// remove palette from ms_TextureList:
			//sysMemStartTemp();
			string_list& textureList = RageBuilder::GetPlatformImpl()->GetTextureList();
			for ( string_list::iterator it = textureList.begin();
				it != textureList.end();
				++it )
			{
				char dir[MAX_PATH];
				char base[MAX_PATH];
				char ext[MAX_PATH]; // extensions are no longer necessarily 3 char long
				SplitFilename( (*it).c_str(), dir, base, ext );

				if(stricmp( base, texName)==0)
				{
					textureList.erase(it);	// remove this entry
				}
			}
			//sysMemEndTemp();
#endif
		}

		if(tintPalsToRemove)
		{
			sysMemStartTemp();
				delete tintPalsToRemove;
				tintPalsToRemove = NULL;
			sysMemEndTemp();
		}
	}// if(bRemoveTintPalettesFromRsc)...

	if(pTintCache)
	{
	sysMemStartTemp();
		delete pTintCache;
		pTintCache = NULL;
	sysMemEndTemp();
	}

	return(rscTintData);
}// end of PrepackageTint()...

//
//
// remove all *removable* tint palettes so ragebuilder has chance to build some (incomplete) resources:
//
static
void RemoveTintPalettesFromRsc(grmShaderGroup *pShaderGroup, bool bResourcingXenon, bool bResourcingPs3, bool bRemoveTintPalettesFromRsc)
{
	if(!pShaderGroup)
		return;

	// remove all *removable* tint palettes so ragebuilder has chance to build some (incomplete) resources:
	if(bRemoveTintPalettesFromRsc)
	{
		sysMemStartTemp();
			atArray<atString> *tintPalsToRemove = new atArray<atString>;	// array of tint palettes to be removed from resources
			tintPalsToRemove->Reserve(16);
		sysMemEndTemp();

		const s32 shaderCount = pShaderGroup->GetCount();
		for(s32 j=0; j<shaderCount; j++)
		{
			grmShader		*pShader = &pShaderGroup->GetShader(j);
			Assert(pShader);

			grcEffectVar	effectVarTintPalTex		= pShader->LookupVar(VARNAME_TINT_PALETTE_TEX, FALSE);
			grcEffectVar	effectVarTintPalSelector= pShader->LookupVar(VARNAME_TINT_PAL_SELECTOR, FALSE);
			if(effectVarTintPalTex && effectVarTintPalSelector)
			{
				// find out how many palettes are stored in texture:
				const grcInstanceData& instanceData = pShader->GetInstanceData();
				const int index = (int)effectVarTintPalTex - 1;
				grcInstanceData::Entry *pEntry = &instanceData.Data()[index];
				grcTexture *pTintTextureToRemove = pEntry->Texture;
				if (pTintTextureToRemove)
				{
					if( (pTintTextureToRemove->GetWidth()	==0)	&&
						(pTintTextureToRemove->GetHeight()	==0)	&&
						(bResourcingXenon? (ProxyGetBitsPerPixel(static_cast<grcTextureXenonProxy*>(pTintTextureToRemove))==0) : (pTintTextureToRemove->GetBitsPerPixel()==0)) )
					{
						// do nothing - it's a proxy
					}
					else
					{
						pShader->SetVar(effectVarTintPalTex, (const grcTexture*)NULL);
						sysMemStartTemp();
							tintPalsToRemove->Grow() = pTintTextureToRemove->GetName();
						sysMemEndTemp();
					}
				}//if (pTintTextureToRemove)...
			}
		}


		pgDictionary<grcTexture> *pTextDict = pShaderGroup->GetTexDict();

		const int count = tintPalsToRemove->GetCount();
		for(int i=0; i<count; i++)
		{
			const char* texName = (const char*)((*tintPalsToRemove)[i]);
			
			grcTexture *pTintTex = pTextDict->LookupLocal(texName);
			if(pTintTex)
			{
				Displayf("\nRemoving ps3 tint pal from resources: %s", texName);
				pTextDict->DeleteEntry(texName);	// remove from txd
				sysMemStartTemp();
					pTintTex->Release();			// kill texture
				sysMemEndTemp();
			}
		}

		if(tintPalsToRemove)
		{
			sysMemStartTemp();
				delete tintPalsToRemove;
				tintPalsToRemove = NULL;
			sysMemEndTemp();
		}
	}//if(bRemoveTintPalettesFromRsc)...
}


//
//
// called from CTextureConversionSpecification::CustomCreateTextureDictionary():
//
// Xenon: resizes tint palette from 256xH to 64x(4*H) for better memory layout utilisation:
//
grcTexture* ResizeTintPalXenon(grcTexture *pTintTexture, grcTextureFactory::TextureCreateParams *params)
{
	if(!pTintTexture)
		return(NULL);

grcTexture *finalTintTexture = NULL;

	Displayf("Resizing 360 tint texture: %s",pTintTexture->GetName());
	grcTextureXenonProxy *pTintTextureProxy360 = static_cast<grcTextureXenonProxy*>(pTintTexture);
	Assert(pTintTextureProxy360);
	D3DBaseTexture *d3dTex = (D3DBaseTexture*)pTintTextureProxy360->GetTexturePtr();
	Assert(d3dTex);

	grcTextureLock lock;
	const bool bLocked = ProxyLockRect(pTintTextureProxy360, 0, 0, lock, grcsRead);
	if(bLocked)
	{
		// only ARGB88888 swizzled or linear allowed:
		if(lock.RawFormat!=D3DFMT_A8R8G8B8 && lock.RawFormat!=D3DFMT_LIN_A8R8G8B8)
		{
			Errorf("%s: Incorrect xenon tint palette format detected in %s.", s_Buildname, pTintTexture->GetName());
			sysMemStartTemp();
				pTintTextureProxy360->Release();
			sysMemEndTemp();
			return(NULL);
		}

		const u32 width		= pTintTexture->GetWidth();
		if(width != 256)
		{
			Errorf("%s: Incorrect xenon tint palette width detected in %s.", s_Buildname, pTintTexture->GetName());
			sysMemStartTemp();
				pTintTextureProxy360->Release();
			sysMemEndTemp();
			return(NULL);
		}
		const u32 height	= pTintTexture->GetHeight();

		u32* tintPal256Tmp	= Alloca(u32, width*height);		// src pixels: 256xH

		const bool bIsTiled = (lock.RawFormat==D3DFMT_A8R8G8B8);
		if(bIsTiled)
		{	
			D3DFORMAT format = static_cast<D3DFORMAT>(lock.RawFormat);
			Assert(XGIsTiledFormat(format));
			Assert(u32(lock.Pitch)==width*sizeof(u32));
			
			DWORD flags = 0;
			if (!XGIsPackedTexture(d3dTex))
			{
				flags |= XGTILE_NONPACKED;
			}
			if (XGIsBorderTexture(d3dTex))
			{
				flags |= XGTILE_BORDER;
			}
			// untile:
			XGUntileTextureLevel(width, height, /*Level*/0, /*GpuFormat*/XGGetGpuFormat(format), flags,
								/*pDestination*/tintPal256Tmp, /*RowPitch*/lock.Pitch, /*pPoint*/NULL, /*pSource*/lock.Base, /*pRect*/NULL);
		}
		else
		{
			sysMemCpy(tintPal256Tmp, lock.Base, width*height*sizeof(u32));
		}


		sysMemStartTemp();
			grcImage *pImage64 = grcImage::Create(64, height*4, /*depth*/1, grcImage::A8R8G8B8, grcImage::STANDARD, 0, 0);
		sysMemEndTemp();
		if(!pImage64)
		{
			Errorf("\n Error creating image for %s!", pTintTextureProxy360->GetName());
			sysMemStartTemp();
				pTintTextureProxy360->Release();
			sysMemEndTemp();
			return(NULL);
		}

		u32* tintPal64Tmp = (u32*)pImage64->GetBits();
		if(!tintPal64Tmp)
		{
			Errorf("\n Error creating image for %s!", pTintTextureProxy360->GetName());
			sysMemStartTemp();
				pImage64->Release();
				pTintTextureProxy360->Release();
			sysMemEndTemp();
			return(NULL);
		}

		grcTexture::eTextureSwizzle texColRemap[4]; // r,g,b,a
		pTintTexture->GetTextureSwizzle(texColRemap[0], texColRemap[1], texColRemap[2], texColRemap[3]);

		// copy to 64x4H texture:
		for(u32 y=0; y<height; y++)
		{
			u32* src = tintPal256Tmp + 256*y;
			u32* dst = tintPal64Tmp  + (64*4)*y;

			for(u32 x=0; x<4; x++)
			{
				//sysMemCpy(dst + x*64, src + x*64, 64*sizeof(u32));
				
				// remap from 360 palette to standarised A8R8G8B8:
				CompileTimeAssert(sizeof(Color32)==sizeof(u32));
				Color32 *srcRawCol	= (Color32*)(src + x*64);
				Color32 *dstColor32 = (Color32*)(dst + x*64);
				for(u32 c=0; c<64; c++)
				{
					dstColor32[c] = RawColorToColor32( srcRawCol[c], texColRemap );
				}
			}
		}

		grcTexture::SetCustomLoadName( pTintTextureProxy360->GetName() );
		finalTintTexture = grcTextureFactory::GetInstance().Create(pImage64, params);
		grcTexture::SetCustomLoadName( NULL );

		sysMemStartTemp();
			pImage64->Release();
			pImage64 = NULL;
		sysMemEndTemp();

		// destroy old tinted texure:
		ProxyUnlockRect(pTintTextureProxy360, lock);
		sysMemStartTemp();
			pTintTextureProxy360->Release();
		sysMemEndTemp();
		pTintTexture = pTintTextureProxy360 = NULL;
	}// if(bLocked)...
	else
	{
		Errorf("%s: Error locking tint palette %s.", s_Buildname, pTintTexture->GetName());
		return(pTintTexture);
	}

	return(finalTintTexture);
}// end of ResizeTintPal360(grcTexture *pTintTexture)...

// Test if a number is a power of two.
static inline bool _isPowerOfTwo(unsigned int value)
{
	return (value & (value-1)) == 0;
}

// Interleave lower 16 bits with 0s.  Ie: 1111 becomes 01010101
static inline unsigned int _spreadBits(unsigned int bits)
{
	bits &= 0xFFFF;

	bits = (bits | (bits<<8)) & 0x00FF00FF;
	bits = (bits | (bits<<4)) & 0x0F0F0F0F;
	bits = (bits | (bits<<2)) & 0x33333333;
	bits = (bits | (bits<<1)) & 0x55555555;

	return bits;
}

// Pack alternate bits into the lower 16 bits of the result.  Ie: x1x1x1x1 becomes 1111.
static inline unsigned int _packBits(unsigned int bits)
{
	bits &= 0x55555555;

	bits = (bits | (bits >> 1)) & 0x33333333;
	bits = (bits | (bits >> 2)) & 0x0F0F0F0F;
	bits = (bits | (bits >> 4)) & 0x00FF00FF;
	bits = (bits | (bits >> 8)) & 0x0000FFFF;

	return bits;
}


// Unswizzle the texture data.
template <typename TEXELTYPE>
void unswizzleImage(TEXELTYPE *dst, const TEXELTYPE *src, unsigned int width, unsigned int height)
{
	Assert(dst);
	Assert(src);
	Assert(_isPowerOfTwo(width));
	Assert(_isPowerOfTwo(height));

	// X coordinate occupies the even bits, Y coordinate occupies the odd bits:
	// yxyxyxyxyxyxyxyxyxyx
	const u32 xAdd = (width > height) ? 1 : 0;
	const u32 yAdd = 1-xAdd;
	const u32 overflowMask = _spreadBits( MIN(width,height) ) << xAdd;

	u32 pixelsLeft = width*height;
	u32 curPos = 0;
	u32 curX=0;
	u32 curY=0;
	while (pixelsLeft--)
	{
		if (overflowMask & curPos)
		{
			// Can't go more in this direction, so move right or down along the Z (non-square textures)
			curX += xAdd;
			curX &= ~(xAdd-1);

			curY += yAdd;
			curY &= ~(yAdd-1);

			curPos = _spreadBits(curX) | (_spreadBits(curY) << 1);
		}

		// Extract the current bit positions for the x and y coordinates
		curX = _packBits(curPos);
		curY = _packBits(curPos >> 1);

		// Write the next pixel in the unswizzled image
		dst[curY*width + curX] = *src++;

		// Go to the next spot in our scanning order
		curPos++;
	}
}

//
//
// Unswizzle texture data back to a linear format.
//
static
bool UnswizzleTextureData(void *dstPixels, const void *srcPixels, u32 bpp, u32 width, u32 height)
{
	Assert(srcPixels);
	Assert(dstPixels);
	// Textures to be unswizzled must be power of 2 in each dimension
	Assert(_isPowerOfTwo(width));
	Assert(_isPowerOfTwo(height));

	switch (bpp)	// bits-per-pixel
	{
	case 8:
		unswizzleImage<u8>((u8*)dstPixels, (const u8*)srcPixels, width, height);
		break;
	case 16:
		unswizzleImage<u16>((u16*)dstPixels, (const u16*)srcPixels, width, height);
		break;
	case 128:
		width *= 2;
		// Fall thru.
	case 64:
		width *= 2;
		// Fall thru.
	case 32:
		unswizzleImage<u32>((u32*)dstPixels, (const u32*)srcPixels, width, height);
		break;
	default:
		Assertf(0, "Invalid bpp");
		return(FALSE);
	}

	return(TRUE);
}

//
// convert raw color from tint palette into 'proper' Color32:
//
inline Color32 RawColorToColor32(Color32 colorDC, grcTexture::eTextureSwizzle * /*remap*/)
{
// remap is broken for tint textures:
// 360/ps3 runtime: tint textures have order ARGB in memory, but remap is BGRA (360) or RGBA (ps3)
// resourcing:      tint textures have order BGRA in memory, but remap is BGRA (360) or RGBA (ps3)
#if 0
u8 dst[4] = {0, 0, 0, 0};

	for(u32 i=0; i<4; i++) // r,g,b,a
	{
		switch(remap[i])
		{
		case grcTexture::TEXTURE_SWIZZLE_R: dst[i] = colorDC.GetRed  ();	break;
		case grcTexture::TEXTURE_SWIZZLE_G: dst[i] = colorDC.GetGreen();	break;
		case grcTexture::TEXTURE_SWIZZLE_B: dst[i] = colorDC.GetBlue ();	break;
		case grcTexture::TEXTURE_SWIZZLE_A: dst[i] = colorDC.GetAlpha();	break;
		case grcTexture::TEXTURE_SWIZZLE_0: dst[i] = 0x00;					break;
		case grcTexture::TEXTURE_SWIZZLE_1: dst[i] = 0xff;					break;
		}
	}

	return Color32(dst[0], dst[1], dst[2], dst[3]);
#else
	#if __RESOURCECOMPILER
		const bool bResourcingPs3				= (RageBuilder::GetPlatform()==RageBuilder::pltPs3);
		const bool bResourcingXenon				= (RageBuilder::GetPlatform()==RageBuilder::pltXenon);

		if(bResourcingPs3 || bResourcingXenon)
		{
			u8 src[4] = {0, 0, 0, 0};
			src[0] = colorDC.GetAlpha();	// byte 0
			src[1] = colorDC.GetRed();		// byte 1
			src[2] = colorDC.GetGreen();	// byte 2
			src[3] = colorDC.GetBlue();		// byte 4
			return Color32(src[2], src[1], src[0], src[3]);	// r,g,b,a
		}
		else
		{
			return colorDC;
		}
	#else //__RESOURCECOMPILER
		return colorDC;	// color field order is already correct
	#endif
#endif //#if 0...
}

// replacements for grcTextureXenonProxy
static int ProxyGetBitsPerPixel(grcTextureXenonProxy *proxy)
{
	if(proxy->GetTexturePtr())
	{
		XGTEXTURE_DESC surfaceDescription;
		XGGetTextureDesc((D3DBaseTexture*)proxy->GetTexturePtr(), 0, &surfaceDescription);
		return surfaceDescription.BitsPerPixel;
	}
	else
	{
		return 0;
	}
}

static bool ProxyLockRect(grcTextureXenonProxy* proxy, int layer, int mipLevel,grcTextureLock &lock, u32 uLockFlags)
{
	IDirect3DTexture9* d3dTexture = static_cast<IDirect3DTexture9*>(proxy->GetTexturePtr());
	if(!d3dTexture)
		return false;

	XGTEXTURE_DESC surfaceDescription;
	XGGetTextureDesc(d3dTexture, mipLevel, &surfaceDescription);
	lock.BitsPerPixel = surfaceDescription.BitsPerPixel;
	lock.Width = surfaceDescription.Width;
	lock.Height = surfaceDescription.Height;
	lock.RawFormat = surfaceDescription.Format;
	lock.MipLevel = mipLevel;
	lock.Layer = layer;

	u32 d3dLockFlags = 0;

	d3dLockFlags |= (uLockFlags & grcsNoOverwrite) ? D3DLOCK_NOOVERWRITE : 0;
	// Add more flags here if any of the rage flags map to the D3D ones. 
	// RMS - One D3D flag that sounds useful, but at the moment I don't have any good test cases for, is D3DLOCK_READONLY.

	switch (d3dTexture->GetType())
	{
	case D3DRTYPE_VOLUMETEXTURE:
		{
			D3DLOCKED_BOX lockedBox;
			if (SUCCEEDED(static_cast<IDirect3DVolumeTexture9*>(proxy->GetTexturePtr())->LockBox(mipLevel, &lockedBox, NULL, d3dLockFlags)))
			{
				Assert(lockedBox.RowPitch * lock.Height == lockedBox.SlicePitch);
				lock.Base = reinterpret_cast<char*>(lockedBox.pBits) + lockedBox.SlicePitch *layer;
				lock.Pitch = lockedBox.RowPitch;
				break;
			}
			else
			{
				return false;
			}
		}
	case D3DRTYPE_CUBETEXTURE:
		{
			D3DLOCKED_RECT lockedRect;
			if (SUCCEEDED(static_cast<IDirect3DCubeTexture9*>(proxy->GetTexturePtr())->LockRect(static_cast<D3DCUBEMAP_FACES>(layer), mipLevel, &lockedRect, NULL, d3dLockFlags)))
			{
				lock.Base = lockedRect.pBits;
				lock.Pitch = lockedRect.Pitch;
				break;
			}
			else
			{
				return false;
			}
		}
	case D3DRTYPE_TEXTURE:
		{
			D3DLOCKED_RECT lockedRect;
			if (SUCCEEDED(d3dTexture->LockRect(mipLevel, &lockedRect, NULL, d3dLockFlags)))
			{
				lock.Base = lockedRect.pBits;
				lock.Pitch = lockedRect.Pitch;
				break;
			}
			else
			{
				return false;
			}
		}

	default:
		Quitf("Invalid D3D resource (texture) type");
		return false;
	}

	return true;
}

static void ProxyUnlockRect(grcTextureXenonProxy *proxy, const grcTextureLock &lock)
{
	IDirect3DTexture9* d3dTexture = static_cast<IDirect3DTexture9*>(proxy->GetTexturePtr());
	if(!d3dTexture)
		return;

	switch (d3dTexture->GetType())
	{
	case D3DRTYPE_VOLUMETEXTURE:
		static_cast<IDirect3DVolumeTexture9*>(proxy->GetTexturePtr())->UnlockBox(lock.Layer);
		break;
	case D3DRTYPE_CUBETEXTURE:
		static_cast<IDirect3DCubeTexture9*>(proxy->GetTexturePtr())->UnlockRect(static_cast<D3DCUBEMAP_FACES>(lock.Layer), lock.MipLevel);
		break;
	case D3DRTYPE_TEXTURE:
		d3dTexture->UnlockRect(lock.MipLevel);
		break;
	default:
		Quitf("Invalid D3D resource (texture) type");
	}
}
#else // TINT_PALETTE_RESOURCES
static atArray<u8>* PrepackageTint(rmcDrawable *pDrawable, const char *pFilename)
{
	return NULL;
}

grcTexture* ResizeTintPalXenon(grcTexture *pTintTexture, grcTextureFactory::TextureCreateParams *params)
{
	return NULL;
}
#endif // TINT_PALETTE_RESOURCES...
