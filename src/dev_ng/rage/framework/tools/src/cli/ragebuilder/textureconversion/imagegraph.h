// ========================================================
// imagegraph.h
// Copyright (c) 2010 Rockstar North.  All Rights Reserved. 
// ========================================================

#ifndef IMAGEGRAPH_H
#define IMAGEGRAPH_H

namespace rage {

float* Graph(float* dst, const float* src, int w, int h);

Vector4* GraphOverlay(
	Vector4*       image,
	int            w,
	int            h,
	const float*   data,
	float          rangeMin,
	float          rangeMax,
	const Vector4& edgeColour,
	const Vector4& blurColour0,
	const Vector4& blurColour1,
	const Vector4& fillColour0,
	const Vector4& fillColour1,
	const Vector4& clearColour,
	bool           bEdgeSharp
);

} // namespace rage

#endif IMAGEGRAPH_H
