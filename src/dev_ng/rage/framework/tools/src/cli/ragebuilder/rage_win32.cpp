//
// rage_win32.cpp
//
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved.
//

#include "rage_win32.h"

#include "grcore/texturedefault.h"
#include "grmodel/model.h"
#include "grmodel/modelfactory.h"
#include "system/platform.h"


static void GeometryParamsHookWin32(const grmModel& UNUSED_PARAM(model), const mshMesh& UNUSED_PARAM(mesh), const grmShaderGroup* UNUSED_PARAM(pShaderGroup), int UNUSED_PARAM(index), GeometryCreateParams* params)
{
	Assert(g_sysPlatform == (__64BIT ? platform::WIN64PC : platform::WIN32PC));

	params->ReadWriteVtxBuf = RagePlatform::GetHeadBlendWriteBuffer();
	params->IsMicroMorph = RagePlatform::GetMicroMorphMesh();
}

void RagePlatformWin32::Init()
{
	g_sysPlatform = sysGetPlatform(__64BIT ? "x64" : "pc");
	g_ByteSwap = sysGetByteSwap(g_sysPlatform);

	grcTextureFactoryDefault::CreatePagedTextureFactory();

	BaseInit();

	grmModelFactory::GetVertexConfigurator()->SetPackNormals(false);

	rage::atFunctor5<void, const grmModel&, const mshMesh&, const grmShaderGroup*, int, GeometryCreateParams*> functor;
	functor.Reset<&GeometryParamsHookWin32>();
	grmModel::SetGeometryParamsHook(functor);
}
