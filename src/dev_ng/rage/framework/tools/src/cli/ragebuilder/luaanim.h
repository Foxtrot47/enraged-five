//
// luagta.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef INC_LUA_ANIM_H_
#define INC_LUA_ANIM_H_

void RegisterCommandsAnim(CLuaCore& lua);

#endif // INC_LUA_ANIM_H_
