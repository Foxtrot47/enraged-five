#ifndef RAGEBUILDER_GTARES_H_
#define RAGEBUILDER_GTARES_H_

#include "atl/array_struct.h"
#include "vector/vector3.h"
#include "fragment/type.h"
#include "rmcore/drawable.h"

#include "vfx/vehicleglass/vehicleglassconstructor.h"

namespace rage {

struct ColourData
{
#if !__FINAL
	void DeclareStruct(datTypeStruct &s);
#endif // !__FINAL

	uint8 red, green, blue;
};

struct VectorData
{
#if !__FINAL
	void DeclareStruct(datTypeStruct &s);
#endif // !__FINAL

	float x,y,z;
};

#define ASSERT(x) (0)

class C2dEffect
{
public:
	virtual ~C2dEffect() {}

#if !__FINAL
	void DeclareStruct(datTypeStruct &s);
#endif // !__FINAL

	VectorData 	m_posn;
};

#define CORONA_DEFAULT_ZBIAS (0.5f)

struct CLightAttr : public C2dEffect
{
	CLightAttr()
	{
		memset(this, 0, sizeof(*this));
		m_coronaZBias = CORONA_DEFAULT_ZBIAS;
	}

	CLightAttr(datResource&)
	{
		memset(this, 0, sizeof(*this));
		m_coronaZBias = CORONA_DEFAULT_ZBIAS;
	}

	IMPLEMENT_PLACE_INLINE(CLightAttr);

#if !__FINAL
	void DeclareStruct(datTypeStruct &s);
#endif // !__FINAL

	// ==================================================================================
	// WARNING!! IF YOU CHANGE THE FIELDS HERE YOU MUST ALSO KEEP IN SYNC WITH:
	// 
	// CLightAttrDef - x:\gta5\src\dev\game\scene\loader\MapData_Extensions.psc
	// CLightAttr    - x:\gta5\src\dev\game\scene\2dEffect.h
	// CLightAttr    - x:\gta5\src\dev\rage\framework\tools\src\cli\ragebuilder\gta_res.h
	// WriteLight    - x:\gta5\src\dev\game\debug\AssetAnalysis\AssetAnalysis.cpp
	// WriteLights   - X:\gta5\src\dev\tools\AssetAnalysisTool\src\AssetAnalysisTool.cpp
	// aaLightRecord - X:\gta5\src\dev\tools\AssetAnalysisTool\src\AssetAnalysisTool.h
	// ==================================================================================

	// General data
	ColourData	m_colour;
	u8			m_flashiness;
	float		m_intensity;
	u32			m_flags;
	s16			m_boneTag;
	u8			m_lightType;
	u8			m_groupId;
	u32			m_timeFlags;
	float		m_falloff;
	float		m_falloffExponent;
	float		m_cullingPlane[4];
	u8			m_shadowBlur;
	u8	  		m_padding1;
	s16	  		m_padding2;
	u32	  		m_padding3;

	// Volume data
	float		m_volIntensity;
	float		m_volSizeScale;
	ColourData	m_volOuterColour;
	u8			m_lightHash;
	float		m_volOuterIntensity;

	// Corona data
	float		m_coronaSize;
	float		m_volOuterExponent;
	u8			m_lightFadeDistance;
	u8			m_shadowFadeDistance;
	u8			m_specularFadeDistance;
	u8			m_volumetricFadeDistance;
	float		m_shadowNearClip;
	float		m_coronaIntensity;
	float		m_coronaZBias;

	// Spot data
	VectorData	m_direction;
	VectorData	m_tangent;
	float		m_coneInnerAngle;
	float		m_coneOuterAngle;

	// Line data
	VectorData	m_extents;

	// Texture data
	u32			m_projectedTextureKey;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class gtaFragType : public fragType
{
public:
	gtaFragType()  : m_pTintData(NULL) {}
	gtaFragType(datResource& rsc);

	~gtaFragType() {}

	IMPLEMENT_PLACE_INLINE(gtaFragType);

	void SetModelIndex(int nModelId) {SetUserData((void*)(size_t)nModelId);}
	int32 GetModelIndex() {return (int32)(size_t)GetUserData();}

	template <class T>
	static T* Load(const char* typeFile)
	{
		T *pFragType = (T*)fragType::Load(typeFile,NULL,new T);

		return pFragType;
	}

#if !__FINAL
	void DeclareStruct(datTypeStruct &s);
#endif // !__FINAL

//protected:
	atArray< CLightAttr > m_lights;
	void* m_vehicleWindowData;
	datOwner< atArray<u8> > m_pTintData;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class gtaDrawable : public rmcDrawable
{
public:
	gtaDrawable() : m_pTintData(NULL), m_pPhBound(NULL) {}
	gtaDrawable(datResource& rsc);

	~gtaDrawable() {}

	IMPLEMENT_PLACE_INLINE(gtaDrawable);

#if !__FINAL
	void DeclareStruct(datTypeStruct &s);
#endif // !__FINAL

	//protected:
	atArray< CLightAttr > m_lights;
	datOwner< atArray<u8> > m_pTintData;
	datOwner< phBound > m_pPhBound;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// keep in sync with: x:\gta5\src\dev\game\shaders\CustomShaderEffectTint.h
struct structTintShaderInfo
{
	u8				DECLARE_BITFIELD_2(	m_DefaultPalette,	7,		// currently selected palette
										m_bIsTree,			1);		// "IsTree" flag (ps3: special unpack path in EDGE)
	u8				m_PaletteTexHeight;			// ps3/360: # of available palettes; rest of the world: texture height
	union
	{
		u8			m_PaletteOffset;			// ps3: offset to first palette in big palette buffer
		u8			m_varPaletteTintSelector;	// 360: 0,1,2,...
	};
	u8				m_ShaderIdx;				// ps3: shader idx in shaderGroup and tintDesc[]
};
CompileTimeAssert(sizeof(structTintShaderInfo)==4);

}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Definitions for the bit flags which signify special map bounds. Must match with
// resourced data!
namespace BoundTypes
{
	enum
	{
		WEAPON_COLLISION_BOUND		= BIT(0),
		MOVER_COLLISION_BOUND		= BIT(1),
		RIVER_BOUND					= BIT(2),
		DEEP_SURFACE_BOUND			= BIT(3),
		FOLIAGE_COLLISION_BOUND		= BIT(4),
		HORSE_COLLISION_BOUND		= BIT(5),
		COVER_COLLISION_BOUND		= BIT(6),
		VEHICLE_COLLISION_BOUND		= BIT(7),
		STAIR_SLOPE_BOUND			= BIT(8),
		MATERIAL_COLLISION_BOUND	= BIT(9),

		LAST_BOUND_TYPE				= BIT(10)
	};
}

// namespace for all type and include flags
// Please keep this in sync with the gtaArchetype.h!!
namespace ArchetypeFlags
{
	// The default type is on by default for all archetypes so that any two objects will be collidable by default.
	// It can be used as some other type by projects that set type flags and include flags for all objects.
	enum
	{
		//GTA_TYPE					= DEFAULT_TYPE,
		GTA_MAP_TYPE_WEAPON			= BIT(1),
		GTA_MAP_TYPE_MOVER			= BIT(2),
		GTA_MAP_TYPE_HORSE			= BIT(3),
		GTA_MAP_TYPE_COVER			= BIT(4),
		GTA_MAP_TYPE_VEHICLE		= BIT(5),

		GTA_VEHICLE_NON_BVH_TYPE	= BIT(6),
		GTA_VEHICLE_BVH_TYPE		= BIT(7),
		GTA_BOX_VEHICLE_TYPE		= BIT(8),
		GTA_PED_TYPE				= BIT(9),
		GTA_RAGDOLL_TYPE			= BIT(10),
		GTA_HORSE_TYPE				= BIT(11),
		GTA_HORSE_RAGDOLL_TYPE		= BIT(12),
		GTA_OBJECT_TYPE				= BIT(13),
		GTA_ENVCLOTH_OBJECT_TYPE	= BIT(14),
		GTA_PLANT_TYPE				= BIT(15),

		GTA_PROJECTILE_TYPE			= BIT(16),
		GTA_EXPLOSION_TYPE			= BIT(17),
		GTA_PICKUP_TYPE				= BIT(18),
		GTA_FOLIAGE_TYPE			= BIT(19),
		GTA_FORKLIFT_FORKS_TYPE		= BIT(20),

		GTA_WEAPON_TEST				= BIT(21),
		GTA_CAMERA_TEST				= BIT(22),
		GTA_AI_TEST					= BIT(23),
		GTA_SCRIPT_TEST				= BIT(24),
		GTA_WHEEL_TEST				= BIT(25),

		GTA_GLASS_TYPE				= BIT(26),
		GTA_RIVER_TYPE				= BIT(27),
		GTA_SMOKE_TYPE				= BIT(28),
		GTA_UNSMASHED_TYPE          = BIT(29),
		GTA_STAIR_SLOPE_TYPE		= BIT(30),
		GTA_DEEP_SURFACE_TYPE		= BIT(31)
	};

#define RAGDOLL_TO_CLOTH_COLLISION 1

	// Compilations of the above type flags into include type sets.
	enum
	{
		// useful base combinations
		GTA_VEHICLE_TYPE					= GTA_VEHICLE_NON_BVH_TYPE | GTA_VEHICLE_BVH_TYPE | GTA_WHEEL_TEST,

		GTA_ALL_MAP_TYPES					= GTA_MAP_TYPE_MOVER,
		GTA_ALL_TYPES_MOVER					= GTA_MAP_TYPE_MOVER | GTA_VEHICLE_TYPE | GTA_PED_TYPE | GTA_OBJECT_TYPE | GTA_PICKUP_TYPE | GTA_GLASS_TYPE | GTA_RIVER_TYPE,
		GTA_ALL_TYPES_WEAPON				= GTA_MAP_TYPE_WEAPON | GTA_VEHICLE_NON_BVH_TYPE | GTA_PED_TYPE | GTA_OBJECT_TYPE | GTA_PICKUP_TYPE | GTA_GLASS_TYPE | GTA_RIVER_TYPE,
		GTA_ALL_SHAPETEST_TYPES				= GTA_WEAPON_TEST | GTA_CAMERA_TEST | GTA_AI_TEST | GTA_SCRIPT_TEST | GTA_WHEEL_TEST,
		GTA_CORE_SHAPETEST_TYPES			= GTA_WEAPON_TEST | GTA_AI_TEST | GTA_SCRIPT_TEST | GTA_WHEEL_TEST,

		// include flag sets for basic game object types
		GTA_BUILDING_INCLUDE_TYPES			=					  GTA_VEHICLE_NON_BVH_TYPE | GTA_FORKLIFT_FORKS_TYPE | GTA_PED_TYPE | GTA_RAGDOLL_TYPE | GTA_OBJECT_TYPE | 						   GTA_PLANT_TYPE | GTA_EXPLOSION_TYPE | GTA_PROJECTILE_TYPE |					 GTA_ALL_SHAPETEST_TYPES							| GTA_GLASS_TYPE | GTA_HORSE_TYPE | GTA_HORSE_RAGDOLL_TYPE,
		GTA_VEHICLE_INCLUDE_TYPES			= GTA_MAP_TYPE_VEHICLE | GTA_VEHICLE_TYPE	   | GTA_FORKLIFT_FORKS_TYPE | GTA_BOX_VEHICLE_TYPE | GTA_PED_TYPE | GTA_RAGDOLL_TYPE | GTA_OBJECT_TYPE | GTA_ENVCLOTH_OBJECT_TYPE | GTA_PLANT_TYPE | GTA_EXPLOSION_TYPE | GTA_PROJECTILE_TYPE | GTA_PICKUP_TYPE | GTA_ALL_SHAPETEST_TYPES							| GTA_GLASS_TYPE | GTA_FOLIAGE_TYPE | GTA_HORSE_TYPE | GTA_HORSE_RAGDOLL_TYPE,
		GTA_BOX_VEHICLE_INCLUDE_TYPES		=					  GTA_VEHICLE_NON_BVH_TYPE | GTA_FORKLIFT_FORKS_TYPE | GTA_BOX_VEHICLE_TYPE | GTA_PED_TYPE | GTA_RAGDOLL_TYPE | GTA_OBJECT_TYPE | 												GTA_EXPLOSION_TYPE | GTA_PROJECTILE_TYPE |												GTA_WEAPON_TEST | GTA_AI_TEST | GTA_SCRIPT_TEST	| GTA_GLASS_TYPE | GTA_HORSE_TYPE | GTA_HORSE_RAGDOLL_TYPE,

#if RAGDOLL_TO_CLOTH_COLLISION
		GTA_PED_INCLUDE_TYPES				= GTA_ALL_MAP_TYPES | GTA_VEHICLE_TYPE		   | GTA_FORKLIFT_FORKS_TYPE | GTA_BOX_VEHICLE_TYPE | GTA_PED_TYPE | GTA_RAGDOLL_TYPE | GTA_OBJECT_TYPE | GTA_ENVCLOTH_OBJECT_TYPE | GTA_PLANT_TYPE |						 GTA_PROJECTILE_TYPE | GTA_PICKUP_TYPE | GTA_ALL_SHAPETEST_TYPES							| GTA_GLASS_TYPE | GTA_FOLIAGE_TYPE | GTA_HORSE_TYPE | GTA_HORSE_RAGDOLL_TYPE,
		GTA_RAGDOLL_INCLUDE_TYPES			= GTA_ALL_MAP_TYPES | GTA_VEHICLE_TYPE		   | GTA_FORKLIFT_FORKS_TYPE | GTA_BOX_VEHICLE_TYPE | GTA_PED_TYPE | GTA_RAGDOLL_TYPE | GTA_OBJECT_TYPE | GTA_ENVCLOTH_OBJECT_TYPE | GTA_PLANT_TYPE | GTA_EXPLOSION_TYPE | GTA_PROJECTILE_TYPE |					 GTA_ALL_SHAPETEST_TYPES							| GTA_GLASS_TYPE | GTA_FOLIAGE_TYPE | GTA_HORSE_TYPE | GTA_HORSE_RAGDOLL_TYPE | GTA_SMOKE_TYPE,
		GTA_ENVCLOTH_OBJECT_INCLUDE_TYPES	= GTA_ALL_MAP_TYPES	| GTA_VEHICLE_TYPE	       | GTA_FORKLIFT_FORKS_TYPE | GTA_PED_TYPE | GTA_RAGDOLL_TYPE | GTA_OBJECT_TYPE |																																							  GTA_GLASS_TYPE | GTA_WEAPON_TEST | GTA_HORSE_RAGDOLL_TYPE,
#else
		GTA_PED_INCLUDE_TYPES				= GTA_ALL_MAP_TYPES | GTA_VEHICLE_TYPE		   | GTA_FORKLIFT_FORKS_TYPE | GTA_BOX_VEHICLE_TYPE | GTA_PED_TYPE | GTA_RAGDOLL_TYPE | GTA_OBJECT_TYPE | GTA_ENVCLOTH_OBJECT_TYPE | GTA_PLANT_TYPE |						 GTA_PROJECTILE_TYPE | GTA_PICKUP_TYPE | GTA_ALL_SHAPETEST_TYPES							| GTA_GLASS_TYPE | GTA_MAP_TYPE_MOVER | GTA_FOLIAGE_TYPE | GTA_HORSE_TYPE | GTA_HORSE_RAGDOLL_TYPE,
		GTA_RAGDOLL_INCLUDE_TYPES			= GTA_ALL_MAP_TYPES | GTA_VEHICLE_TYPE		   | GTA_FORKLIFT_FORKS_TYPE | GTA_BOX_VEHICLE_TYPE | GTA_PED_TYPE | GTA_RAGDOLL_TYPE | GTA_OBJECT_TYPE |							   GTA_PLANT_TYPE | GTA_EXPLOSION_TYPE | GTA_PROJECTILE_TYPE |					 GTA_ALL_SHAPETEST_TYPES							| GTA_GLASS_TYPE | GTA_MAP_TYPE_MOVER | GTA_FOLIAGE_TYPE | GTA_HORSE_TYPE | GTA_HORSE_RAGDOLL_TYPE | GTA_SMOKE_TYPE,
		GTA_ENVCLOTH_OBJECT_INCLUDE_TYPES	= GTA_ALL_MAP_TYPES	| GTA_VEHICLE_TYPE		   | GTA_FORKLIFT_FORKS_TYPE | GTA_PED_TYPE |											GTA_OBJECT_TYPE | GTA_HORSE_TYPE,
#endif

		GTA_PARACHUTING_PED_INCLUDE_TYPES	= GTA_ALL_MAP_TYPES | GTA_VEHICLE_TYPE		   | GTA_BOX_VEHICLE_TYPE |									  GTA_OBJECT_TYPE |																																								  GTA_GLASS_TYPE,
		GTA_OBJECT_INCLUDE_TYPES			= GTA_ALL_MAP_TYPES | GTA_VEHICLE_TYPE		   | GTA_FORKLIFT_FORKS_TYPE | GTA_PED_TYPE | GTA_RAGDOLL_TYPE | GTA_OBJECT_TYPE | 											GTA_EXPLOSION_TYPE | GTA_PROJECTILE_TYPE |					 GTA_ALL_SHAPETEST_TYPES							| GTA_GLASS_TYPE | GTA_HORSE_TYPE | GTA_HORSE_RAGDOLL_TYPE,
		GTA_PLANT_INCLUDE_TYPES				= GTA_ALL_MAP_TYPES	| GTA_VEHICLE_NON_BVH_TYPE | GTA_FORKLIFT_FORKS_TYPE | GTA_PED_TYPE | GTA_RAGDOLL_TYPE | GTA_HORSE_TYPE | GTA_HORSE_RAGDOLL_TYPE,


		// include flag sets for special game object types
		GTA_DOOR_OBJECT_INCLUDE_TYPES		=					  GTA_VEHICLE_NON_BVH_TYPE | GTA_FORKLIFT_FORKS_TYPE | GTA_PED_TYPE | GTA_RAGDOLL_TYPE | GTA_OBJECT_TYPE |												GTA_EXPLOSION_TYPE | GTA_PROJECTILE_TYPE |					 GTA_ALL_SHAPETEST_TYPES |							  GTA_GLASS_TYPE | GTA_HORSE_TYPE | GTA_HORSE_RAGDOLL_TYPE,
		GTA_PROJECTILE_INCLUDE_TYPES		= GTA_ALL_MAP_TYPES | GTA_VEHICLE_TYPE		   | GTA_FORKLIFT_FORKS_TYPE | GTA_BOX_VEHICLE_TYPE |				   GTA_RAGDOLL_TYPE	| GTA_OBJECT_TYPE |												GTA_EXPLOSION_TYPE |					   GTA_PICKUP_TYPE | GTA_ALL_SHAPETEST_TYPES							| GTA_GLASS_TYPE | GTA_HORSE_RAGDOLL_TYPE,
		GTA_PROJECTILE_NEAR_INCLUDE_TYPES	= GTA_MAP_TYPE_WEAPON | GTA_VEHICLE_TYPE	   | GTA_FORKLIFT_FORKS_TYPE | GTA_BOX_VEHICLE_TYPE |				   GTA_RAGDOLL_TYPE	| GTA_OBJECT_TYPE |												GTA_EXPLOSION_TYPE |					   GTA_PICKUP_TYPE | GTA_ALL_SHAPETEST_TYPES							| GTA_GLASS_TYPE | GTA_HORSE_RAGDOLL_TYPE,
		GTA_EXPLOSION_INCLUDE_TYPES			= GTA_ALL_MAP_TYPES | GTA_VEHICLE_NON_BVH_TYPE | GTA_FORKLIFT_FORKS_TYPE | GTA_BOX_VEHICLE_TYPE |				   GTA_RAGDOLL_TYPE	| GTA_OBJECT_TYPE |																	 GTA_PROJECTILE_TYPE | GTA_PICKUP_TYPE |													  GTA_GLASS_TYPE | GTA_HORSE_RAGDOLL_TYPE,
		GTA_SMOKE_INCLUDE_TYPES				= GTA_ALL_MAP_TYPES	|											GTA_PED_TYPE | GTA_RAGDOLL_TYPE	| GTA_OBJECT_TYPE | GTA_HORSE_TYPE | GTA_HORSE_RAGDOLL_TYPE,
		GTA_VEH_SEAT_INCLUDE_FLAGS			= (GTA_VEHICLE_INCLUDE_TYPES & ~GTA_ALL_SHAPETEST_TYPES),
		GTA_WHEEL_INCLUDE_TYPES				= (GTA_VEHICLE_INCLUDE_TYPES & ~(GTA_WHEEL_TEST | GTA_PED_TYPE | GTA_FORKLIFT_FORKS_TYPE)),	// Wheels collide with everything vehicles do
		GTA_VEH_BVH_CHASSIS_INCLUDE_TYPES	=					  GTA_VEHICLE_NON_BVH_TYPE |						GTA_PED_TYPE | GTA_RAGDOLL_TYPE | GTA_OBJECT_TYPE |																	 GTA_PROJECTILE_TYPE |					 GTA_ALL_SHAPETEST_TYPES | GTA_HORSE_TYPE | GTA_HORSE_RAGDOLL_TYPE,

		GTA_CAR_DOOR_UNLATCHED_INCLUDE_TYPES= GTA_VEHICLE_INCLUDE_TYPES,
		GTA_CAR_DOOR_LATCHED_INCLUDE_TYPES	= GTA_CORE_SHAPETEST_TYPES | GTA_PROJECTILE_TYPE,

		// include sets for world probe type tests
		GTA_MELEE_VISIBILTY_TYPES			= GTA_ALL_MAP_TYPES | GTA_VEHICLE_NON_BVH_TYPE | GTA_FORKLIFT_FORKS_TYPE |								  GTA_OBJECT_TYPE |																																								  GTA_GLASS_TYPE,
		GTA_WEAPON_TYPES					=					  GTA_VEHICLE_TYPE		   | GTA_FORKLIFT_FORKS_TYPE |			   GTA_RAGDOLL_TYPE | GTA_OBJECT_TYPE |  GTA_PROJECTILE_TYPE |																		  GTA_GLASS_TYPE | GTA_MAP_TYPE_WEAPON | GTA_RIVER_TYPE | GTA_HORSE_RAGDOLL_TYPE,
		GTA_MOBILE_ACTORS_TYPES				=					  GTA_VEHICLE_NON_BVH_TYPE | GTA_FORKLIFT_FORKS_TYPE | GTA_PED_TYPE | GTA_RAGDOLL_TYPE | GTA_OBJECT_TYPE |																																								  GTA_GLASS_TYPE | GTA_HORSE_TYPE | GTA_HORSE_RAGDOLL_TYPE,
		GTA_CAMERA_CLIPPING_TYPES			=					  GTA_VEHICLE_NON_BVH_TYPE |									   GTA_RAGDOLL_TYPE | GTA_OBJECT_TYPE |																							GTA_PICKUP_TYPE |																	   GTA_MAP_TYPE_WEAPON | GTA_RIVER_TYPE | GTA_UNSMASHED_TYPE | GTA_HORSE_RAGDOLL_TYPE,

		// include flag sets for high detail map collision
		GTA_HIGH_DETAIL_MAP_INCLUDE_TYPES	= GTA_WEAPON_TEST | GTA_CAMERA_TEST | GTA_PROJECTILE_TYPE,

		// bullets and projectiles
		GTA_WEAPON_AND_PROJECTILE_INCLUDE_TYPES = GTA_WEAPON_TEST | GTA_PROJECTILE_TYPE,
	};
}

#endif //RAGEBUILDER_GTARES_H_
