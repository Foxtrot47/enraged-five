/**
 * ragebuilder_dll
 *
 * RageBuilder DLL Interface.
 *
 * This file can be included to provide access to the DLL exported functions
 * into another application / library.  That application would then require
 * to link to the RageBuilder DLL's LIB file.
 *
 * Applications that require to import the RageBuilder DLL functions must
 * define RAGEBUILDER_DLL_IMPORTS before including this file.
 *
 * The RageBuilder DLL project defines RAGEBUILDER_DLL_EXPORTS so that the
 * functions are correctly exported in the associated DLL's LIB file.
 *
 */

#ifndef RAGEBUILDER_DLL_H
#define RAGEBUILDER_DLL_H

#define __MASTER 0

#ifdef RAGEBUILDER_DLL_EXPORTS
#define RAGEBUILDER_DLL	__declspec( dllexport )
#else
#ifdef RAGEBUILDER_DLL_IMPORTS
#define RAGEBUILDER_DLL __declspec( dllimport )
#endif
#endif

#ifndef RAGEBUILDER_DLL
#define RAGEBUILDER_DLL
#endif

#ifdef RAGEBUILDER_DLL_EXPORTS
#define SYSTEM_TLS_H
#undef __THREAD
#define __THREAD  
#endif

//
// ANSI C Compatibility
//
// Rage has some files that are compiled as ANSI C code, namely DevIL although
// there may be more.  Ideally MS-VC would have stdbool.h but it doesn't so
// we have to fake it here.  This might cause problems if called from
// ANSI C code (default under MS-VC for .c extension files).
//
// When MS-VC compiler supports C99 this compatability layer can be changed.
//
// Sample Reference:
//	http://david.tribble.com/text/cdiffs.htm
//
#ifndef __cplusplus
typedef int		bool;		// short, but must be compatible with ati_compress
#define false	('\0')
#define true	(!false)
#endif // __cplusplus

// DLL Export Function Prototypes
#include "librb.h"
#include "scriptgeneral.h"
#include "scriptgta.h"
#include "scriptpack.h"
#include "scriptrage.h"

#endif // RAGEBUILDER_DLL_H

// End of ragebuiler_dll.h
