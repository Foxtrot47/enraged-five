/**
 * RageBuilder Pack Functions
 *
 * This header automatically configures the correct function linkage for
 * the following functions depending on the RageBuilder_DLL.h header file
 * defined RAGEBUILDER_DLL.
 */

#ifdef __cplusplus
extern "C"
{
#endif

RAGEBUILDER_DLL bool RBStartPack( bool includeMetaFiles );
RAGEBUILDER_DLL bool RBStartPackXCompress( bool includeMetaFiles );
RAGEBUILDER_DLL bool RBStartPackZlib( bool includeMetaFiles );
RAGEBUILDER_DLL bool RBStartUncompressedPack( bool includeMetaFiles );
RAGEBUILDER_DLL bool RBAddToPack(const char* source,const char* destination);
RAGEBUILDER_DLL bool RBAddToPackForceCompression(const char* source,const char* destination,bool compress);
RAGEBUILDER_DLL bool RBSavePack(const char* packname);
RAGEBUILDER_DLL bool RBClosePack(void);
RAGEBUILDER_DLL bool RBMountPack(const char* packname, const char* mountpoint);
RAGEBUILDER_DLL bool RBUnmountPack(const char* packname);
RAGEBUILDER_DLL bool RBIsCompressed(const char* filename);
RAGEBUILDER_DLL bool RBExtractFileToMemory(const char* filename, char* outMemoryFileName);
RAGEBUILDER_DLL bool RBReleaseFileFromMemory(const char* filename);

#ifdef __cplusplus
}
#endif
