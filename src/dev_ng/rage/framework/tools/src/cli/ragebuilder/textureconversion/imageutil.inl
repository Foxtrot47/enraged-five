// ========================================================
// imageutil.inl
// Copyright (c) 2010 Rockstar North.  All Rights Reserved. 
// ========================================================

//#define resize Resize

namespace rage {

template <typename T> __forceinline CImage<T>::CImage() : mW(0), mH(0), mPixels(NULL)
{
}

template <typename T> __forceinline CImage<T>::~CImage()
{
	delete[] (char*) mPixels;
}


template <typename T> __forceinline CImage<T>::CImage(int w, int h) : mW(0), mH(0)
{
	if (IsValid(w, h))
	{
		mW = w;
		mH = h;
		mPixels = (T*) rage_new char[w*h*sizeof(T)]; // don't invoke Vector4 ctor on huge array!
	}
	else
		mPixels = NULL;
}

/* template <typename T> __forceinline CImage<T>::CImage(int w, int h, const T& fill) : mW(0), mH(0)
{
	if (IsValid(w, h))
	{
		mW = w;
		mH = h;
		mPixels.resize(w*h, fill);
	}
} */

template <typename T> __forceinline CImage<T>::CImage(const void* data, int w, int h, int bytesPerRow) : mW(0), mH(0)
{
	if (bytesPerRow == 0)
	{
		bytesPerRow = w*sizeof(T);
	}

	if (IsValid(w, h) && bytesPerRow >= w*sizeof(T))
	{
		mW = w;
		mH = h;
		mPixels = (T*) rage_new char[w*h*sizeof(T)]; // don't invoke Vector4 ctor on huge array!

		for (int y = 0; y < h; y++)
		{
			memcpy(GetPixelRowAddr(y), (const u8*)data + y*bytesPerRow, w*sizeof(T));
		}
	}
	else
		mPixels = NULL;
}

template <typename T> __forceinline CImage<T>::CImage(FILE* f, int off, int w, int h, int bytesPerRow) : mW(0), mH(0)
{
	if (bytesPerRow == 0)
	{
		bytesPerRow = w*sizeof(T);
	}

	if (IsValid(w, h) && bytesPerRow >= w*sizeof(T))
	{
		mW = w;
		mH = h;
		mPixels = (T*) rage_new char[w*h*sizeof(T)]; // don't invoke Vector4 ctor on huge array!

		for (int y = 0; y < h; y++)
		{
			fseek(f, off + y*bytesPerRow, SEEK_SET);
			fread(GetPixelRowAddr(y), sizeof(T), w, f);
		}
	}
	else
		mPixels = NULL;
}

template <typename T> template <typename Q> __COMMENT(explicit) __forceinline CImage<T>::CImage(const CImage<Q>& src) : mW(0), mH(0)
{
	if (src.IsValid())
	{
		const int w = src.mW;
		const int h = src.mH;

		mW = w;
		mH = h;
		mPixels = (T*) rage_new char[w*h*sizeof(T)]; // don't invoke Vector4 ctor on huge array!

		ConvertFrom(&src);
	}
	else
		mPixels = NULL;
}

template <typename T> __COMMENT(static) __forceinline bool CImage<T>::IsValid(int w, int h)
{
	const int kMaxImageSize = 16384; // arbitrary limits ..
	const int kMaxImageArea = 8192*8192;

	return (Min<int>(w,h) > 0 && Max<int>(w,h) <= kMaxImageSize && w*h <= kMaxImageArea);
}

template <typename T> __forceinline bool CImage<T>::IsValid() const
{
	return this && IsValid(mW, mH);
}

template <typename T> __forceinline T* CImage<T>::GetPixelRowAddr(int y) const
{
	Assert(y >= 0 && y < mH);

	return const_cast<T*>(&mPixels[y*mW]);
}

template <typename T> __forceinline T AddrClamped(T x, T maxVal)
{
	return x <= T(0) ? T(0) : (x >= maxVal ? maxVal : x);
}

template <typename T> __forceinline T AddrWrapped(T x, T maxVal)
{
	return x >= T(0) ? (x % (maxVal + 1)) : (maxVal - ((-(x + 1)) % (maxVal + 1)));
}

template <typename T> __forceinline T* CImage<T>::GetPixelRowAddrClamped(int y) const
{
	return const_cast<T*>(&mPixels[AddrClamped<int>(y, mH - 1)*mW]);
}

template <typename T> __forceinline T* CImage<T>::GetPixelRowAddrWrapped(int y) const
{
	return const_cast<T*>(&mPixels[AddrWrapped<int>(y, mH - 1)*mW]);
}

template <typename T> __forceinline T& CImage<T>::GetPixel(int x, int y) const
{
	Assert(x >= 0 && x < mW);
	Assert(y >= 0 && y < mH);

	return GetPixelRowAddr(y)[x];
}

template <typename T> __forceinline T& CImage<T>::GetPixelClamped(int x, int y) const
{
	return GetPixelRowAddrClamped(y)[AddrClamped<int>(x, mW - 1)];
}

template <typename T> __forceinline T& CImage<T>::GetPixelWrapped(int x, int y) const
{
	return GetPixelRowAddrWrapped(y)[AddrWrapped<int>(x, mW - 1)];
}

template <typename T> __forceinline T CImage<T>::GetPixelBorder(int x, int y, const T& border) const
{
	if (x >= 0 && x < mW && y >= 0 && y < mH)
	{
		return GetPixelRowAddr(y)[x];
	}

	return border;
}

template <typename T> __forceinline T& CImage<T>::GetPixelAtLinearAddr(int addr) const
{
	Assert(addr >= 0 && addr < mW*mH);

	int x = addr % mW;
	int y = addr / mW;

	return GetPixel(x, y);
}

template <typename T> __forceinline void CImage<T>::SetPixel(int x, int y, const T& p) const
{
	Assert(x >= 0 && x < mW);
	Assert(y >= 0 && y < mH);

	GetPixelRowAddr(y)[x] = p;
}

template <typename T> __forceinline bool CImage<T>::SetPixelDiscard(int x, int y, const T& p) const
{
	if (x >= 0 && x < mW && y >= 0 && y < mH)
	{
		GetPixelRowAddr(y)[x] = p;
		return true;
	}

	return false;
}

} // namespace rage
