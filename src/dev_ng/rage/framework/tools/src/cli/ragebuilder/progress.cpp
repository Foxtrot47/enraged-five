
// Local headers
#include "progress.h"
#include "log.h"
#include "utils.h"

// Platform includes
#include <Windows.h>

// STL headers
#include <fstream>
#include <cstdio>
#include <cstdlib>
#include <cstring>

static CONSOLE_SCREEN_BUFFER_INFO csbiInitial;
::rage::u32 Progress::ms_uNumFiles = 0;

bool 
Progress::ProgressSizeCallback( ::rage::u32 size )
{
	ms_uNumFiles = size;

#if !defined(RAGEBUILDER_LIB) && !defined(RUBYRAGE)
	GetConsoleScreenBufferInfo( GetStdHandle(STD_OUTPUT_HANDLE), &csbiInitial );
#endif //#if !defined(RAGEBUILDER_LIB) && !defined(RUBYRAGE)

	return (true);
}

bool 
Progress::ProgressPosCallback( ::rage::u32 pos )
{
#if !defined(RAGEBUILDER_LIB) && !defined(RUBYRAGE)
	static bool bSwitch = false;

	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE),csbiInitial.dwCursorPosition);

	if(bSwitch)
	{
		printf("\\ ");
		bSwitch = false;
	}
	else
	{
		printf("/ ");
		bSwitch = true;
	}

	int iPercent = (int)(((float)pos / (float)(ms_uNumFiles)) * 100.0f);
	printf("%d%% done",iPercent);
#endif //#if !defined(RAGEBUILDER_LIB) && !defined(RUBYRAGE)
	return true;
}
