//
// Logging support.
//
// Copyright (C) 1999-2005 Rockstar North.  All Rights Reserved.
//

#ifndef INC_LOG_H_
#define INC_LOG_H_

#include "system/xtl.h"
#include "diag/channel.h"
#include "diag/output.h"
#include "file/stream.h"
#include "RsULog.h"

class LogListener
{
public:
	static void Init( const char* log, const char* errorLog );
	static void Close();

	static void SetStopOnError(bool bEnable);
	static void	SetLogLevel(int level);
	static int GetLogLevel(){ return ms_logLevel; }
	static int GetErrorCount() {return ms_errorCount;}
	static int GetErrorLogProduced() {return ms_bErrorLogProduced;}
	static char* GetBuildLogFilename() { return ms_buildlog; }
	static char* GetErrorLogFilename() { return ms_buildlog; }

protected:
	static bool LogFn(const rage::diagChannel &channel,rage::diagSeverity severity,const char *file,int line,const char* fmt,va_list args);
	static void GetTimeStamp(char* timestamp, int maxLen);
	static rage::fiStream* WaitToCreateStream(const char* name, bool bWait, bool bAppend = false);
	static rage::fiStream* ClampStreamSize(const char* name, rage::fiStream* pStream, int size, bool bFindNewLine);

	static bool ms_bBlockWarningsAndErrors;
	static bool	ms_bStopOnError;
	static bool ms_bFileLogging;
	static int	ms_logLevel;
	static char	ms_buildlog[MAX_PATH];
	static char	ms_errorlog[MAX_PATH];
	static rage::fiStream* ms_buildlogStream;
	static rage::fiStream* ms_errorlogStream;
	static int	ms_errorCount;
	static bool	ms_bErrorLogProduced;
	static UniversalLogFile* ms_pUniLog;
};

// only use these to report feedback to the user
#define rbDisplayf	Displayf
#define rbTracef1	(LogListener::GetLogLevel() < 1)?(void)(0):Displayf
#define rbTracef2	(LogListener::GetLogLevel() < 2)?(void)(0):Displayf
#define rbTracef3	(LogListener::GetLogLevel() < 3)?(void)(0):Displayf

#if defined(RUBYRAGE)
#define Errorf		Displayf
#define Warningf	Displayf
#endif // RUBYRAGE

#endif // INC_LOG_H_
