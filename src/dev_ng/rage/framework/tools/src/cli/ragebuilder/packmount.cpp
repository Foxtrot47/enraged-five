//
// packmount.cpp
//
// Copyright (C) 1999-2005 Rockstar North.  All Rights Reserved.
//

#include "packmount.h"

#include "system/xtl.h"
#include "packfile_old.h"
#include "file/asset.h"
#include "file/device.h"
#include "file/packfile.h"

#include "file/zipfile.h"


using namespace rage;

rage::atFixedArray<CPackMountManager::SPackMount, CPackMountManager::MAX_MOUNTED>	CPackMountManager::ms_mountedPacks;
rage::u16 CPackMountManager::ms_RageVersion;

//initialise the system
void CPackMountManager::Init()
{
	fiDevice::SetMaxMounts(CPackMountManager::MAX_MOUNTED);
}

// Mount a pack file.
bool CPackMountManager::Mount( const char* mountName, const char* mountAs, ArchiveType type )
{
	SPackMount packmount;

	Assert( ms_mountedPacks.GetCount() < MAX_MOUNTED );

	char dir[MAX_PATH];
	ASSET.FullPath( dir, sizeof(dir), mountAs, "" );
	ASSET.RemoveNameFromPath(dir, sizeof(dir), dir );
	strcat(dir,"/");

	ms_RageVersion = 0;

	// no shared parent so some code duplication
	switch ( type )
	{
	case RPF:
		// RAGE fiPackfile (all versions)
		{
			fiPackfileOld* pack = rage_new fiPackfileOld;

			if (pack->Init(mountName))
			{
				if (pack->MountAs(dir))
				{
					packmount.m_pack = pack;
				}
				else
				{
					delete pack;
					Errorf("cannot mount archive '%s' at '%s'",mountName,dir);
					return false;
				}
			}
			else
			{
				delete pack;

				fiPackfile* packNew = rage_new fiPackfile;
				if (packNew->Init(mountName, true, fiPackfile::CACHE_NONE))
				{
					if (packNew->MountAs(dir))
					{
						packmount.m_pack = packNew;
					}
					else
					{
						delete packNew;
						Errorf("cannot mount archive '%s' at '%s'",mountName,dir);
						return false;
					}
				}
				else
				{
					delete packNew;
					Errorf("cannot load archive '%s'",mountName);
					return false;
				}
			}
		}
		break;
	case ZIP:
		// PKWARE Zip file support
		{
			fiZipfile * zip = rage_new fiZipfile;

			if (!zip->Init(mountName))
			{
				delete zip;

				Errorf("cannot load zip '%s'",mountName);
				return false;
			}

			if (!zip->MountAs(dir))
			{
				delete zip;

				Errorf("cannot mount zip '%s' at '%s'",mountName,dir);
				return false;
			}

			packmount.m_pack = zip;
		}
		break;
	default:
		Errorf( "Attempting to mount invalid pack file (RPF, IMG or ZIP supported).");
		return (false);
	}

	// Finish initialising our SPathMount structure.
	packmount.m_ArchiveType = type;
	strncpy( packmount.m_packname, mountName, MAX_PATH );
	strncpy( packmount.m_mountAs, dir, MAX_PATH );
	ms_mountedPacks.Push( packmount );

	return true;
}

// Unmount a pack file.
bool CPackMountManager::Unmount( const char* mountName )
{
	char dir[MAX_PATH];

	int iPackIndex = -1;
	for(int i = 0; i < ms_mountedPacks.GetCount(); ++i )
	{
		if( stricmp( mountName, ms_mountedPacks[i].m_packname ) == 0 )
		{
			iPackIndex = i;
			break;
		}
	}

	if (iPackIndex == -1)
		return false;

	SPackMount packmount = ms_mountedPacks[iPackIndex];

	ASSET.FullPath( dir, sizeof(dir), packmount.m_mountAs, "" );
	ASSET.RemoveNameFromPath(dir, sizeof(dir), dir );
	strcat(dir,"/");

	if (!fiDevice::Unmount(dir))
	{
		Errorf("cannot unmount archive '%s' at '%s'", packmount.m_packname, dir);
		return true;
	}

	delete packmount.m_pack;
	packmount.m_pack = NULL;
	ms_mountedPacks.Delete( iPackIndex );

	return true;
}

bool CPackMountManager::IsCompressed(const char* filename)
{
	const fiDevice* device = fiDevice::GetDevice(filename, true);
	const fiPackfile* packfile = dynamic_cast<const fiPackfile*>(device);
	if (!packfile)
	{
		Errorf("File %s is not inside a packfile");
		return false;
	}

	const fiPackEntry* entry = packfile->FindEntry(filename + packfile->GetRelativeOffset());
	if (!entry)
	{
		Errorf("Couldn't find entry for file %s", filename);
		return false;
	}
	return entry->IsCompressed();
}

// Load up a file, memory map it
bool CPackMountManager::ExtractFileToMemory(const char* filename, char* outMemoryName)
{
	const fiDevice* device = fiDevice::GetDevice(filename, true);

	outMemoryName[0] = '\0';

	if (!device)
	{
		return false;
	}

	char* storage = NULL;
	u32 length = 0;

	if (device->IsRpf())
	{
		const fiPackfile* packfile = smart_cast<const fiPackfile*>(device);
		int entryIndex = packfile->GetEntryIndex(filename);
		Assertf(entryIndex >= 0, "Bad packfile entry index %d for %s", entryIndex, filename);
		u32 handle = fiCollection::MakeHandle(packfile->GetPackfileIndex(), entryIndex);
		storage = reinterpret_cast<char*>(packfile->ExtractFileToMemory(handle, length));
	}
	else
	{
		fiStream* s = ASSET.Open(filename, "");
		if (!s)
		{
			return false;
		}

		length = s->Size();
		storage = rage_new char[length];
		s->Read(storage, length);
		s->Close();
	}

	if (!storage)
	{
		return false;
	}

	fiDevice::MakeMemoryFileName(outMemoryName, RAGE_MAX_PATH, storage, length, false, filename);

	return true;
}

bool CPackMountManager::ReleaseFileFromMemory(const char* filename)
{
	char* storage = NULL;
	size_t length = 0;

	sscanf(filename, "memory:$%p,%d,", &storage, &length);

	if (storage != NULL && length > 0)
	{
		delete [] storage;
		return true;
	}

	return false;
}
