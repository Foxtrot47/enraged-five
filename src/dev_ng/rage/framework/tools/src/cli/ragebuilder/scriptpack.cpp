// Local headers
#include "luapack.h"
#include "ragebuilder.h"
#include "scriptpack.h"
#include "scriptrage.h"
#include "pack.h"
#include "packmount.h"
#include "log.h"

// RAGE headers
#include "system/xtl.h"
#include "string/string.h"
#include "file/stream.h"
#include "file/asset.h"
#include "file/packfile.h"

using namespace rage;

bool RBStartPack( bool includeMetaData )
{
	rbTracef2( "starting pack");

	if( CRagePackFileBuilder::Open( 
		CRagePackFileBuilder::RPF7,
		true, includeMetaData ) == false )
	{
		Warningf("failed to start pack");
		return false;
	}

	return true;
}

#if !defined(RUBYRAGE)
bool RBStartPackXCompress( bool includeMetaData )
{
	rbTracef2( "starting xcompress pack");

	RageBuilder::SetCompressionType(RageBuilder::ctXCompress);
	return RBStartPack(includeMetaData);
}

bool RBStartPackZlib( bool includeMetaData )
{
	rbTracef2( "starting zlib pack");

	RageBuilder::SetCompressionType(RageBuilder::ctZlib);
	return RBStartPack(includeMetaData);
}
#endif

bool RBStartUncompressedPack( bool includeMetaData )
{
	rbTracef2( "starting uncompressed pack");

	if( CRagePackFileBuilder::Open( 
		CRagePackFileBuilder::RPF7,
		false, includeMetaData ) == false )
	{
		Warningf("failed to start uncompressed pack");
		return false;
	}

	return true;
}

bool RBAddToPack(const char* source,const char* destin)
{
// DW : 15/02/11 : AddFilesToList is very slow, since it calls fiAssetManager::EnumFiles which searches filesystem for files.
// it seems unnecessary here 
// - unless it verifies that the file exists etc? - but we just created it right? 
// - or we pass in wildcards - do we ever? 
// - we pass in full paths so it seems safe also.
// - speeds up ragebuilder pack/unpack unit-test by x2.  Disabled for now.
#define FAST_ADD_TO_PACK 0 // prevent unrequired enumeration of files?
#define FAST_ADD_TO_PACK_VERIFY 0 // compares the two code branches (original, optimised) produce same result. ( so still slow! )
#if FAST_ADD_TO_PACK

	Displayf("RBAddToPack: %s", source);

	bool isPath = true;
	char destination[MAX_PATH];

	if(destin)
		strcpy(destination,destin);
	else
		destination[0] = '\0';

	if( destination[0] != '\0' )
	{
		const char* endOfDestination = destination + StringLength(destination) - 1;
		isPath = ( *endOfDestination == '\\' || *endOfDestination == '/' );
	}

	char fileDestination[MAX_PATH] = {0};
	strncpy( fileDestination, destination, MAX_PATH );

	if(isPath)
		strcat( fileDestination, ASSET.FileName( source ) );

		if( !CRagePackFileBuilder::AddFile( source, fileDestination ) )
			Warningf("failed to add file '%s' to image as '%s'", source, fileDestination );
		else
			rbTracef2("adding %s to pack as %s", source, fileDestination);

	#if FAST_ADD_TO_PACK_VERIFY
	// verification!
	{		
		char fastpath[MAX_PATH];
		strcpy(fastpath,source);
		
		string_list fileList;
		bool isPath = true;
		char destination[MAX_PATH];

		if(destin)
			strcpy(destination,destin);
		else
			destination[0] = '\0';

		if( destination[0] != '\0' )
		{
			const char* endOfDestination = destination + StringLength(destination) - 1;
			isPath = ( *endOfDestination == '\\' || *endOfDestination == '/' );
		}

		AddFilesToList( source, fileList, false );

		if( fileList.empty() )
			Warningf( "no files found for add_to_pack_path(%s, %s)", source, destination);

		for( string_list::iterator it = fileList.begin(); it != fileList.end(); ++it )
		{
			char fileDestination[MAX_PATH] = {0};
			strncpy( fileDestination, destination, MAX_PATH );

			if(isPath)
				strcat( fileDestination, ASSET.FileName( (*it).c_str() ) );

			int equal = strcmp(fastpath,(*it).c_str());
			if (equal==0)
			{
				Errorf("FAST_ADD_TO_PACK is unsafe! FAST=%s SLOW=%s",fastpath,(*it).c_str());
			}
		}
	}	
	#endif // FAST_ADD_TO_PACK_VERIFY

	return true;

#else

	string_list fileList;
	bool isPath = true;
	char destination[MAX_PATH];

	if(destin)
		strcpy(destination,destin);
	else
		destination[0] = '\0';

	if( destination[0] != '\0' )
	{
		const char* endOfDestination = destination + StringLength(destination) - 1;
		isPath = ( *endOfDestination == '\\' || *endOfDestination == '/' );
	}

	AddFilesToList( source, fileList, false );

	if( fileList.empty() )
		Warningf( "no files found for add_to_pack_path(%s, %s)", source, destination);

	for( string_list::iterator it = fileList.begin(); it != fileList.end(); ++it )
	{
		char fileDestination[MAX_PATH] = {0};
		strncpy( fileDestination, destination, MAX_PATH );

		if(isPath)
			strcat( fileDestination, ASSET.FileName( (*it).c_str() ) );

		if( !CRagePackFileBuilder::AddFile( (*it).c_str(), fileDestination ) )
			Warningf("failed to add file '%s' to image as '%s'", (*it).c_str(), fileDestination );
		else
			rbTracef2("adding %s to pack as %s", (*it).c_str(), fileDestination);
	}

	return true;
#endif // USE_FILE_LIST
}

bool RBAddToPackForceCompression(const char* source,const char* destin,bool compress)
{
	CRagePackFileBuilder::PushCompression(compress);
	RBAddToPack(source,destin);
	CRagePackFileBuilder::PopCompression();

	return true;
}

bool RBSavePack(const char* packname)
{
	if(!CRagePackFileBuilder::Save(packname))
	{
		Errorf( "failed to save_pack(%s)", packname);
		return false;
	}

	Displayf( "saved pack as %s", packname);
	return true;
}

bool RBClosePack()
{
#if !defined(RUBYRAGE)
	RageBuilder::SetCompressionType(RageBuilder::ctDefault);
#endif
	if( CRagePackFileBuilder::Close() == false )
	{
		Warningf("failed to close pack file");
		return false;
	}

	rbTracef2( "closed pack");
	return true;
}

bool RBMountPack(const char* packname, const char* mountpoint)
{
	const char* mountAs = packname;
	if( mountpoint != NULL )
		mountAs = mountpoint;

	const char *extension = ASSET.FindExtensionInPath(packname);
	CPackMountManager::ArchiveType archiveType = (extension && !stricmp(extension, ".zip")) ? CPackMountManager::ZIP : CPackMountManager::RPF;

	if(	CPackMountManager::Mount( packname, mountAs, archiveType ) == false )
	{
		Errorf( "failed to mount pack %s", packname );
		return false;
	}

	rbTracef3("mounted pack - %s", packname);
	return true;
}

bool RBUnmountPack(const char* packname)
{
	if(	CPackMountManager::Unmount( packname ) == false )
	{
		Errorf( "failed to unmount pack %s", packname );
		return false;
	}

	rbTracef3("unmounted pack - %s", packname);
	return true;
}


bool RBIsCompressed(const char* filename)
{
	return CPackMountManager::IsCompressed(filename);
}

bool RBExtractFileToMemory(const char* filename, char* outMemoryFileName)
{
	if (CPackMountManager::ExtractFileToMemory(filename, outMemoryFileName) == false)
	{
		Errorf("failed to load file %s to memory", filename);
		return false;
	}
	return true;
}

bool RBReleaseFileFromMemory(const char* filename)
{
	if (CPackMountManager::ReleaseFileFromMemory(filename) == false)
	{
		Errorf("failed to release memory file %s", filename);
	}
	return true;
}
