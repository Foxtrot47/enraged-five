//
// luageneral.h
//
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#ifndef INC_LUA_GENERAL_H_
#define INC_LUA_GENERAL_H_

#include "luacore.h"

int GetEnv(lua_State *L);
int SetEnv(lua_State *L);
int GetParam(lua_State *L);
int GetParamCount(lua_State *L);
int GetParamByIndex(lua_State *L);
int Wait(lua_State* L);
int Sleep(lua_State* L);

int SetLogLevel(lua_State* L);
int SetStopOnError(lua_State* L);
int ReportError(lua_State* L);
int ReportWarning(lua_State* L);
int ReportQuit(lua_State* L);
int ReportDisplay(lua_State* L);
int ReportTrace1(lua_State* L);
int ReportTrace2(lua_State* L);
int ReportTrace3(lua_State* L);

int GetBuildLogFilename(lua_State* L);
int GetErrorLogFilename(lua_State* L);

int DelFile(lua_State* L);
int DelDir(lua_State* L);
int FindFiles(lua_State* L);
int FindPlatformFiles(lua_State* L);
int FindDirs(lua_State* L);
int RemoveExtensionFromPath(lua_State* L);
int RemoveAllExtensionsFromPath(lua_State* L);
int GetExtensionFromPath(lua_State* L);
int GetFilenameFromPath(lua_State* L);
int RemoveNameFromPath(lua_State* L);
int CreateLeadingPath(lua_State* L);
int Copy_File(lua_State* L);
int Move_File(lua_State* L);
int FileExists(lua_State* L);
int DirectoryExists(lua_State* L);
int Get_FileTime(lua_State* L);
int FileTimeCompare(lua_State* L);
int FileSize(lua_State* L);
int ResourceSize(lua_State* L);
int ResourcePhysicalSize(lua_State* L);
int ResourceVirtualSize(lua_State* L);
int SetFileAttrib(lua_State* L);
int GetFileAttrib(lua_State* L);
int CloneTimestamp(lua_State* L);

int PushTimer(lua_State* L);
int PopTimer(lua_State* L);

int GetVersion(lua_State* L);

int Require(lua_State* L);

int	FindXMLMatch(lua_State* L);

#if __XENON
int Print(lua_State* L);
#endif // __XENON

#endif // INC_LUA_GENERAL_H_
