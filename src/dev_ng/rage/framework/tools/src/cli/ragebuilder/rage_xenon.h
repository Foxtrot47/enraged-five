//
// rage_xenon.h
// 
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved. 
//

#ifndef INC_RAGE_XENON_H_
#define INC_RAGE_XENON_H_

#include "ragebuilder.h"

class RagePlatformXenon : public RagePlatform
{
public:
	virtual void Init();

	virtual bool IsEndianSwapping() {return true;}
};

#endif // INC_RAGE_XENON_H_