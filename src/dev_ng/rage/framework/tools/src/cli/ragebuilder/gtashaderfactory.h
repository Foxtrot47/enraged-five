#ifdef NOT_USED_AT_THE_MOMENT_AND_WONT_COMPILE_IN_NEW_RAGE
#ifndef RAGEBUILDER_GTASHADERFACTORY_H_
#define RAGEBUILDER_GTASHADERFACTORY_H_

#include "grmodel/shader.h"

namespace rage {

/*	PURPOSE
		This only differs from the standard shader factory in that it allows
		for loading of shaders whilst building resources without having to do
		a preload of the shaders and presets in advance
*/
class grmShaderFactoryRagebuilder: public grmShaderFactory {
public:
	// DOM-IGNORE-BEGIN
	// overrides
	static grmShaderFactory* SetActiveInstance();

	grmShader* Create(const char *filename, int argCount = 0, const char** args = NULL );
	grmShaderGroup*   CreateGroup();
	grmShaderGroup*	  LoadGroup(fiTokenizer & T);
	grmShaderGroup*   LoadGroup(const char *filename);
	void PlaceShader(class datResource&,grmShader** shaders,int count);
	void PlaceShaderGroup(datResource &rsc,grmShaderGroup* &group);
	void PreloadShaders( const char *path, bool resetindex=true );
	// DOM-IGNORE-END
};

}

#endif //#define RAGEBUILDER_GTASHADERFACTORY_H_
#endif // NOT_USED_AT_THE_MOMENT_AND_WONT_COMPILE_IN_NEW_RAGE
