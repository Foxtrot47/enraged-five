//
// Cpp wrapper class for Lua, with some support functions for writing
// Lua commands.
//
// Copyright (C) 1999-2005 Rockstar North.  All Rights Reserved.
//

#include "luacore.h"

// windows includes
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>
#include <direct.h>

// lua includes
#define lua_c
extern "C"
{
# include "lua.h"
# include "lauxlib.h"
# include "lualib.h"
}

// rage includes
#include "system/param.h"
#include "file/stream.h"
#include "file/asset.h"

#include "log.h"
#include "utils.h"

using namespace rage;

// pragma based library includes.
#if __XENON
# pragma comment(lib,"luad_xenon.lib")
# pragma comment(lib,"lualibd_xenon.lib")
#else // __XENON
# if __64BIT
#  pragma comment(lib,"luad64.lib")		// hmm should be MT?
#  pragma comment(lib,"lualibd64.lib")
# elif !__OPTIMIZED
#  pragma comment(lib,"luad_mt.lib")
#  pragma comment(lib,"lualibd_mt.lib")
# else // !__OPTIMIZED
#  pragma comment(lib,"lua_mt.lib")
#  pragma comment(lib,"lualib_mt.lib")
# endif
#endif // __XENON

// standard libraries
static const luaL_reg gs_lualibs[] = {
	{"base",		luaopen_base},
	{"table",		luaopen_table},
	{"io",			luaopen_io},
	{"string",		luaopen_string},
	{"math",		luaopen_math},
	{"debug",		luaopen_debug},
	{"loadlib",		luaopen_loadlib},
	{NULL, NULL}
};

// Constructor.
CLuaCore::CLuaCore()
{
	m_L = lua_open();
	m_bOwns = true;

	if (m_L == NULL)
	{
		Errorf("Cannot create lua state: not enough memory");
	}
}

CLuaCore::CLuaCore(lua_State* state, bool bOwns) :
	m_L(state), m_bOwns(bOwns)
{
}

// Destructor.
CLuaCore::~CLuaCore()
{
	if(m_L != NULL && m_bOwns )
	{
		lua_close(m_L);
		m_L = NULL;
	}
}

// Register a Lua command.
void CLuaCore::RegisterCommand(const char* name, LuaCommandFn* function)
{
	lua_register(m_L, name, function);
}

// Execute the Lua script file.
int CLuaCore::ExecuteFile(const char* filename)
{
	if(strstr(filename,"pack:") || strstr(filename,"image:"))
	{
		int iReturn = -1;

		rage::fiStream* pStream = rage::ASSET.Open(filename, "", false, true );

		if( pStream )
		{
			char* pTextBuffer;	// temporary buffer for the text file.

			int iSize = pStream->Size();

			pTextBuffer = new char[iSize+1];

			if( pTextBuffer )
			{
				pStream->Read( pTextBuffer, iSize );
				pStream->Close();
				pTextBuffer[iSize] = '\0';

				iReturn = ExecuteText( pTextBuffer, filename );
				Report(iReturn);
				delete [] pTextBuffer;
			}
		}

		return iReturn;
	}
	else
	{
		char workingdir[256];
		char filedir[256];
		const char* file;
		char path[256];

		SplitFilename(filename, path, NULL, NULL);
		bool bPathed = (path[0] != '\0');

		if (bPathed)
		{
			if(_getcwd(workingdir, MAX_PATH) == NULL )
			{
				Errorf("Could not get current working dir");
				return -1;
			}

			ASSET.RemoveNameFromPath(filedir, 256, filename);
			file = ASSET.FileName(filename);

			if (_chdir(filedir) != 0)
			{
				Errorf("Could not set current working dir to file dir %s", filedir);
				return -1;
			}
		}
		else
		{
			file = filename;
		}

		int iResult = DoCall(luaL_loadfile(m_L, file));

		if (bPathed)
		{
			if (_chdir(workingdir) != 0)
			{
				Errorf("Could not set current working dir to previous dir %s", workingdir);
				return -1;
			}
		}

		return iResult;
	}
}

// Execute a buffer of Lua script text.
int CLuaCore::ExecuteText(const char* text, const char* name)
{
	int status = DoCall(luaL_loadbuffer(m_L, text, strlen(text), name));

	return Report( status );
}

// Report the result of an operation.
// Extracts a message from the LuaState.
int CLuaCore::Report(int status)
{
	const char *msg;
	if (status)
	{
		msg = lua_tostring(m_L, -1);
		if(msg == NULL)
		{
			msg = "(error with no message)";
		}
		Errorf("%s\n", msg);
		lua_pop(m_L, 1);
	}
	return status;
}


// Perform a Lua call and report the status.
int CLuaCore::DoCall(int status)
{
	if (status == 0)
	{
		status = LCall(0, 1);
	}

	return Report(status);
}


// Perform a lua call with support for a callstack trace from the Lua script.
int CLuaCore::LCall(int narg, int clear)
{
	int status;
	int base = lua_gettop(m_L) - narg;  /* function index */
	lua_pushliteral(m_L, "_TRACEBACK");
	lua_rawget(m_L, LUA_GLOBALSINDEX);  /* get traceback function */
	lua_insert(m_L, base);  /* put it under chunk and args */
	// signal(SIGINT, LAction);
#if __XENON
	status = lua_pcall(m_L, narg, (clear ? 0 : LUA_MULTRET), 0 );
#else
	status = lua_pcall(m_L, narg, (clear ? 0 : LUA_MULTRET), base);
#endif

	// signal(SIGINT, SIG_DFL);
	lua_remove(m_L, base);  /* remove traceback function */
	return status;
}


//void CLuaCore::LAction(int i)
//{
//	signal(i, SIG_DFL); /* if another SIGINT happens before lstop,
//						terminate process (default action) */
//	lua_sethook(m_L, LStop, LUA_MASKCALL | LUA_MASKRET | LUA_MASKCOUNT, 1);
//}


//void CLuaCore::LStop (lua_State *l, lua_Debug *ar)
//{
//	(void)ar;  /* unused arg. */
//	lua_sethook(l, NULL, 0, 0);
//	luaL_error(l, "interrupted!");
//}


//void CLuaCore::LMessage(const char* pname, const char* msg)
//{
//	if (pname)
//	{
//		Printf( "%s: ", pname);
//	}
//	Printf( "%s\n", msg);
//}


// Open the Lua standard libraries (@see gs_lualibs)
void CLuaCore::OpenStdLibs()
{
	const luaL_reg *lib = gs_lualibs;
	while(lib->func)
	{
		lib->func(m_L);  /* open library */
		lua_settop(m_L, 0);  /* discard any results */
		++lib;
	}
}

// Push an error into the Lua state.
void PushError(lua_State *L, const char* pCommand, const char* pError)
{
	lua_pushfstring(L, "%s: %s", pCommand, pError);
	lua_error(L);
}

// Get an argument from the lua state.
void GetArguments(lua_State *L, int number, const char* pCommand)
{
	int n = lua_gettop(L);
	if(n != number)
	{
		PushError(L, pCommand, "incorrect number of arguments");
	}
}

// Get the arguments from the lua state but checks against a minimum and maximum.
// Specify -1 for minimum or maximum to not care.
// Returns the number of arguments.
int GetArgumentsRange(lua_State *L, int minimum, int maximum, const char* pCommand)
{
	int n = lua_gettop(L);

	if( n < minimum && minimum != -1 )
	{
		PushError(L, pCommand, "incorrect number of arguments");
	}
	else if( n > maximum && maximum != -1 )
	{
		PushError(L, pCommand, "incorrect number of arguments");
	}

	return n;
}

// Check the type of argument at a particular position.
void CheckArgument(lua_State *L, unsigned int index, int (*lua_istype) (lua_State *L, int idx), const char* pCommand)
{
	if(!lua_istype(L, index))
	{
		PushError(L, pCommand, "incorrect argument type");
	}
}
