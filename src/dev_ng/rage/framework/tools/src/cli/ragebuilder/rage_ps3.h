//
// rage_ps3.h
//
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved.
//

#ifndef INC_RAGE_PS3_H_
#define INC_RAGE_PS3_H_

#include "ragebuilder.h"

#include "grcore/texturedefault.h"

class RagePlatformPs3 : public RagePlatform
{
public:
	virtual void Init();
	virtual bool IsEndianSwapping() {return true;}

	static bool sm_Edgify;
	static bool sm_EdgePositionsInMainMem;
};

#endif // INC_RAGE_PS3_H_
