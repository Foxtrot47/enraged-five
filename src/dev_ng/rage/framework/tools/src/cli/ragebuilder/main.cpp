//
// Main entry point for RageBuilder.
//
// Copyright (C) 1999-2005 Rockstar Games.  All Rights Reserved.
//

#define USE_STOCK_ALLOCATOR	1
//#define SIMPLE_HEAP_SIZE	(248*1024)

#include "system/param.h"

#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>

// RAGE headers
#include "system/xtl.h"
#include "system/main.h"
#include "paging/dictionary.h"
#include "parser/manager.h"
#include "grmodel/model.h"
#include "phbound/bound.h"

#include "packmount.h"
#include "librb.h"
#include "log.h"
#include "ragebuilder.h"

using namespace rage;

extern __THREAD int RAGE_LOG_DISABLE;

PARAM(lowpriority, "Run the app at low priority to not kill the system" );
PARAM(version, "print the ragebuilder version and quit" );
PARAM(usage, "print usage information and quit" );

PARAM(file, "script parameter - here just to stop rage complaining" );
PARAM(project, "script parameter - here just to stop rage complaining" );

PARAM(oneinstance, "script parameter - only allow one instance of ragebuilder to be run" );


PARAM(debuggerattach, "Waits for debugger attachment.");
volatile bool g_DebugAttachBool = true;


extern char aExtensions[RageBuilder::pltIndependent + 1][RageBuilder::fiUnknown][5];

// Main entry point
int Main()
{
	int iResult = EXIT_SUCCESS;

	try
	{
		RAGE_LOG_DISABLE = 1;
		RagePlatform::SetShaderPath("X:/gta/build/common/shaders/");
		RagePlatform::SetShaderDbPath("X:/gta/build/common/shaders/db/");

		HANDLE mut = INVALID_HANDLE_VALUE;

		if (PARAM_oneinstance.Get())
		{
			TCHAR tempPath[MAX_PATH];
			TCHAR tempFilename[MAX_PATH];
			GetTempPath(MAX_PATH,tempPath); 
			GetTempFileName(tempPath,"",8000,tempFilename);

			HANDLE mut = CreateFile(	tempFilename,
										GENERIC_WRITE,
										0,
										NULL,
										CREATE_ALWAYS,
										FILE_ATTRIBUTE_HIDDEN,
										NULL);

			if ( mut == INVALID_HANDLE_VALUE ) 
			{
				printf("instance of ragebuilder already running and -oneinstance specified on command line");
				return -1;
			}
		}

		

		INIT_PARSER;
		PARSER.Settings().SetFlag(parSettings::USE_TEMPORARY_HEAP, true);

		CPackMountManager::Init();

		pgDictionaryBase::SetListOwnerThread(g_CurrentThreadId);

		phBound::SetOctreeAsBVH(true);
		phBound::DisableMessages();

		if (PARAM_version.Get())
		{
			rbDisplayf("%d.%d\n",RAGE_RELEASE,BUILD_VERSION);
			return (EXIT_SUCCESS);
		}
		if ( PARAM_usage.Get() )
		{
			rbDisplayf( "Usage:\n" );
			rbDisplayf( "\t%s [filename]\n", sysParam::GetArg(0) );
			return (EXIT_SUCCESS);
		}

		if ( PARAM_debuggerattach.Get() )
		{
			while( sysBootManager::IsDebuggerPresent() == false ) 
			{
				rbDisplayf( "Waiting for debugger to be attached:\n" );
				Sleep(1);
			};
			Sleep(1);
			__debugbreak();
		}

		// to prevent starving the system.
		if (PARAM_lowpriority.Get())
		{
			HANDLE thread = GetCurrentThread();
			SetThreadPriority( thread, THREAD_PRIORITY_LOWEST );
		}

		rbLuaInit();
		if ( ::rage::sysParam::GetArgCount() > 1 )
			iResult = rbLuaRunScriptFile(rage::sysParam::GetArg(1));
		else
			iResult = rbLuaRunInteractiveConsole( );
		rbLuaClose();

		SHUTDOWN_PARSER;

		if(mut != INVALID_HANDLE_VALUE)
			CloseHandle(mut);
	}
	catch ( ... )
	{
		if ( ::rage::sysParam::GetArgCount() > 1 )
			Errorf( "An uncaught exception occurred during Ragebuilder script %s.", rage::sysParam::GetArg(1) );
		else
			Errorf( "An uncuaght exception occurred during interactive Ragebuilder session." );
		iResult = 5;
	}

#if __DEV
	if (sysBootManager::IsDebuggerPresent())
	{
		system("pause"); // don't exit yet, we might want to see the console output
	}
#endif

	return iResult;
}

void 
boost::throw_exception( std::exception const & e )
{
	Errorf( "BOOST exception: %s.", e.what( ) );
}
