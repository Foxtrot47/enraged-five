
#include "asset_link.h"
#include "asset_link_parser.h"
#include "configParser/configGameView.h"

namespace rage
{

void 
CAssetLink::ResolvePath( )
{
	static configParser::ConfigGameView gv;
	static configUtil::Environment* pEnv = gv.GetEnvironment( );

	char resolvedPath[RAGE_MAX_PATH] = {0};
	pEnv->subst( resolvedPath, RAGE_MAX_PATH, m_filename );

	m_filename = resolvedPath;
	m_filename.Trim( );
}

} // rage namespace
