//
// File:: IRpfBuilder.h
// Description:: Pure-virtual base class for specific RPF version builder 
//               classes.
//
// Author:: David Muir <david.muir@rockstarnorth.com>
// Date:: 23 September 2010
//

#ifndef IRPFBUILDER_H
#define IRPFBUILDER_H

// RAGE headers
#include "atl/array.h"
#include "file/stream.h"

//
class IRpfBuilder
{
public:
	IRpfBuilder( bool compress = true, bool keepNameHeap = true )
		: m_bKeepNameHeap( keepNameHeap )
		, m_Compress( 0, 10 )
	{
		m_Compress.Push( compress );
	}	

	virtual ~IRpfBuilder( ) { };

	virtual bool Add( const char* source, const char* destination ) = 0;
	virtual bool Save( ::rage::fiStream* fp ) = 0;

	void PushCompression( bool compress )
	{
		m_Compress.Push( compress );
	}

	void PopCompression( )
	{
		m_Compress.Pop( );
	}

protected:
	::rage::atArray<bool>	m_Compress;	// Entry compression flags.
	bool					m_bKeepNameHeap; // Maintain name heap in RPF file.

private:
	// Hide copy constructor and assignment op. 
	IRpfBuilder( const IRpfBuilder& copy );
	IRpfBuilder& operator=( IRpfBuilder const& copy );
};

#endif // IRPFBUILDER_H
