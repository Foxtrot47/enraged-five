//
// Ragebuilder library header.
//
// Copyright (C) 1999-2005 Rockstar North.  All Rights Reserved.
//

#ifndef __INC_RBLIB_
#define __INC_RBLIB_

// Please increment this number if you have made a fix or a significant change
// to Ragebuilder.
#define BUILD_VERSION (39)

#ifdef __cplusplus
extern "C"
{
#endif

RAGEBUILDER_DLL bool RBInit(void);
RAGEBUILDER_DLL bool RBShutdown(void);

#if defined(RAGEBUILDER_LIB)

typedef void (LogOutFn) (const char *fmt);
RAGEBUILDER_DLL void RBSetStdOut(LogOutFn fn);
RAGEBUILDER_DLL void RBSetStdErr(LogOutFn fn);

#endif //#if defined(RAGEBUILDER_LIB)

#ifdef __cplusplus
}
#endif

bool rbCoreInit();
bool rbCoreShutdown();

bool rbLuaInit();
int rbLuaRunScriptFile(const char* filename);
int rbLuaRunInteractiveConsole();
void rbLuaClose();

#endif // __INC_RBLIB_
