//
// Lua functions for creating packed files.
//
// Copyright (C) 1999-2005 Rockstar North.  All Rights Reserved. 
//

#if !defined(LUA_PACK_H_)
#define LUA_PACK_H_

#include "luacore.h"

int StartPack( lua_State* L );
int StartPackXCompress( lua_State* L );
int StartPackZlib( lua_State* L );
int StartUncompressedPack( lua_State* L );
int AddToPack( lua_State* L );
int SavePack( lua_State* L );
int ClosePack( lua_State* L );

int MountPack( lua_State* L );
int UnmountPack( lua_State* L );

int IsCompressed( lua_State* L );

int ExtractFileToMemory( lua_State* L );
int ReleaseFileFromMemory( lua_State* L	);

#endif LUA_PACK_H_
