// 
// file/packfile.cpp 
// 
// Copyright (C) 1999-2006 Rockstar Games.  All Rights Reserved. 
// 
#include "packfile_old.h"

#include "atl/staticpool.h"
#include "data/aes.h"
#include "file/asset.h"
#include "string/stringhash.h"
#include "system/criticalsection.h"
#include "system/endian.h"
#include "system/magicnumber.h"
#include "system/memory.h"
#include "zlib/zlib.h"

#include <stdlib.h>

namespace rage {

#if __BE
inline void Convert_LE(u32*d,u32 c) { for (;c;c--,d++) *d = sysEndian::Swap(*d); }
inline void Convert_BE(u32*,u32) { }
#else
inline void Convert_BE(u32*d,u32 c) { for (;c;c--,d++) *d = sysEndian::Swap(*d); }
inline void Convert_LE(u32*,u32) { }
#endif


/* RPF6 format:
	HEADER:
		16-byte header (fiPackFileHeader).  Big endian, unencrypted.
		Contains magic number, entry count, metadata offset, and encryption flags.
		Note that all previous verions of RPF had little-endian data.
	MAIN DIRECTORY:
		hdr.m_EntryCount * sizeof(fiPackEntry6) in size, rounded up to 16 byte boundary.  Big endian, encrypted.
		All files store a hash code and the exact amount of space they consume as "loose" files independently of
		any space introduced by offset alignment of the next file.  Offsets are stored with the lower 8 (11 for resources)
		bits implicitly being zero, giving us an effective maximum size of 16G.
	BODY:
		The body of the archive is next, starting on the next 2k boundary after the header and main directory.
		Files that are 128k or larger, or any resource, will start on a 2k boundary.  Small files that are not resources 
		will start on an 8-byte boundary.  The body of the archive will always end on a 2k boundary.
	METADATA:
		If header metadata offset is nonzero, it defines the start of this data (stored divided by 8 like other offsets).
		The metadata goes all the way to end of file (which is padded to be a multiple of 16 in size).  It is all
		encrypted, and the fiPackMetaData part is stored in big-endian order.  That is immediately followed by 
		the name heap.  The final version of the game can strip off the metadata by simply truncating the archive
		to the size indicated in the header metadata offset, and setting the metadata offset to zero.
*/		

/*
#include <windows.h>
#include <stdlib.h>
#include <stdio.h>

void main()
{
	SYSTEMTIME _2000 = { 2000, 1, 0, 1, 0, 0, 0, 0 };
	unsigned __int64 ft;
	SystemTimeToFileTime(&_2000, (FILETIME*)&ft);
	char buffer[100];
	_ui64toa(ft,buffer,10);
	printf("filetime of 2000 %s\n",buffer);
}
*/
// Seconds since January 1, 2000 gives us a reach of 136 years.
static const u64 FILETIME_2000 = 125911584000000000ULL;
static const u64 CENTINANOSECONDS_PER_SECOND = 10000000ULL;

static u32 ConvertFileTimeToRpfOldTime(u64 filetime) {
	return u32((filetime - FILETIME_2000) / CENTINANOSECONDS_PER_SECOND);
}

static u64 ConvertRpfOldTimeToFileTime(u32 filetime) {
	// system filetimes are in 100ns units
	return (u64) (filetime * CENTINANOSECONDS_PER_SECOND) + FILETIME_2000;
}



struct fiPackHeaderOld {
	u32 m_Magic;			// 'RPF0'
	u32 m_TotalSize;		// sizeof(fiPackEntry6)*m_EntryCount+m_NameHeapSize, rounded up to 2k boundary.
	u32 m_EntryCount;		// number of fiPackEntry6 structures
	u32 m_WastedSpace;		// when a packfile is patched, this tracks how much space is getting wasted.
	u32 m_Encrypted;		// nonzero if it's AES-encrypted.  Key is project-specific.
	char padding[2048-20];	// align to sector boundary
};

struct fiPackEntry2345 {					// RPF2,3,4,5 use this version.
	u32 m_NameOffsetOrHashCode;			// name offset or hashcode of file or directory
	union {
		struct {
			u32 DECLARE_BITFIELD_2(
				m_UncompSize,31,					// Uncompressed size of the file or resource (zero for a directory)
				m_IsNewResource,1);
		} any;
	} s;
	union {
		u32 words[2];
		struct Any {
			u32 DECLARE_BITFIELD_2(
				opaque_0,31,
				m_IsDir,1);
			u32 DECLARE_BITFIELD_3(
				opaque_1,30,
				m_IsCompressed,1,
				m_IsResource,1);
		} any;
		struct Directory {
			u32 DECLARE_BITFIELD_2(
				m_DirectoryIndex,31,	// index of the first entry in the directory
				m_IsDir,1);				// always one
			u32 DECLARE_BITFIELD_3(
				m_DirectoryCount,30,	// count of items in directory
				m_IsCompressed,1,		// always zero
				m_IsResource,1);		// always zero
		} directory;
		struct File {
			u32 DECLARE_BITFIELD_2(
				m_FileOffset,31,		// offset within the archive of the file data
				m_IsDir,1);				// always zero
			u32 DECLARE_BITFIELD_3(
				m_CompSize,30,
				m_IsCompressed,1,		// if one, m_CompSize is valid, else m_CompSize is zero
				m_IsResource,1);		// always zero
		} file;
		struct Resource {
			u32 DECLARE_BITFIELD_3(
				m_Version,8,			// Least sigificant bits of file version information
				m_ResourceOffset,23,	// offset within the archive of the file data (in 256byte blocks)
				m_IsDir,1);				// always zero
		} resourceold;
	} u;
};

CompileTimeAssert(sizeof(fiPackEntry2345) == 16);

static int cmpHashedEntry(const void *a,const void *b) 
{
	const fiPackEntry6 *ea = (fiPackEntry6*) a;
	const fiPackEntry6 *eb = (fiPackEntry6*) b;
	if (ea->m_NameHash > eb->m_NameHash)
		return 1;
	else if (ea->m_NameHash < eb->m_NameHash)
		return -1;
	else
		return 0;
}

void RecurseSort(fiPackEntry6 *base,unsigned offset,unsigned count) {
	// Sort this directory.
	qsort(base+offset,count,sizeof(fiPackEntry6),cmpHashedEntry);
	// Locate subdirectories and process them.
	while (count--) {
		if (base[offset].IsDir())
			RecurseSort(base,base[offset].u.directory.m_DirectoryIndex,base[offset].u.directory.m_DirectoryCount);
		offset++;
	}
}

fiPackfileOld::fiPackfileOld() 
	: m_NameHeap(NULL)
	, m_Entries(NULL) 
	, m_MetaData(NULL)
	, m_Handle(fiHandleInvalid)
	, m_FileTime(0)
	, m_Device(NULL)
	, m_OffsetShift(0)
	, m_EntryCount(0)
{
	m_Name[0] = 0;
}


fiPackfileOld::~fiPackfileOld() {
	if (m_Device && fiIsValidHandle(m_Handle))
		m_Device->CloseBulk(m_Handle);
	m_Device = NULL;
	m_Handle = fiHandleInvalid;

	delete [] m_Entries;

	delete [] m_MetaData;
	m_MetaData = NULL;
	m_Entries = NULL;
	// m_NameHeap is always tied to m_MetaData and points into the same allocation
	m_NameHeap = NULL;
}

bool fiPackfileOld::Init(const char *filename) {
	char fullname[RAGE_MAX_PATH];
	ASSET.FullReadPath(fullname,sizeof(fullname),filename,""); // To allow other extensions than .rpf
	safecpy(m_Name,ASSET.FileName(fullname));

	m_Device = fiDevice::GetDevice(fullname,true);
	if(!Verifyf(m_Device,"Cannot find device for filename '%s'",fullname))
	{
		return false;
	}
	m_Handle = m_Device->OpenBulk(fullname,m_ArchiveBias);
	if (!fiIsValidHandle(m_Handle))
		return false;
	m_FileTime = m_Device->GetFileTime(fullname);
	m_ArchiveSize = m_Device->GetFileSize(fullname);

	if (m_ArchiveSize < 16 /* 2048 - if you ENABLE_NO_BUFFERING in file/device_win32.cpp */) {
		Displayf("'%s' is not a valid rpf or zip file (way too small)",filename);
		m_Device->CloseBulk(m_Handle);
		m_Handle = fiHandleInvalid;
		return false;
	}
	m_RelativeOffset = int(ASSET.FileName(fullname) - fullname);

	// Only read the first five words, that's enough to identify the file type and avoids
	// lockups on very small (less than 2k) .zip (ie not rpf) files.
	fiPackHeader6 hdr;

	m_Device->ReadBulk(m_Handle,m_ArchiveBias,&hdr,16);

	Convert_LE(&hdr.m_Magic,1);
	Convert_BE(&hdr.m_EntryCount,3);

	const u32 RPF2 = MAKE_MAGIC_NUMBER('R','P','F','2');	// no shift, nameheap, 16 byte dir entry
	const u32 RPF3 = MAKE_MAGIC_NUMBER('R','P','F','3');	// no shift, no nameheap, 16 byte dir entry
	const u32 RPF4 = MAKE_MAGIC_NUMBER('R','P','F','4');	// shift 3, nameheap, 16 byte dir entry
	const u32 RPF5 = MAKE_MAGIC_NUMBER('R','P','F','5');	// shift 3, no nameheap, 16 byte dir entry
	const u32 RPF6 = MAKE_MAGIC_NUMBER('R','P','F','6');	// shift 3, extra metadata, 20 byte dir entry
	const u32 RPF7 = MAKE_MAGIC_NUMBER('R','P','F','7');    // we can't open this type, but fail gracefully
	if (hdr.m_Magic !=  RPF2 && hdr.m_Magic != RPF3 && hdr.m_Magic != RPF4 && hdr.m_Magic != RPF5 && 
		hdr.m_Magic != RPF6) {
		if (hdr.m_Magic != RPF7) // silent fail for this one
		{
			Displayf("'%s' is not a valid rpf file (magic=%x)",filename,hdr.m_Magic);
		}
		m_Device->CloseBulk(m_Handle);
		m_Handle = fiHandleInvalid;
		return false;
	}

	RAGE_TRACK(Packfile);

	m_EntryCount = hdr.m_EntryCount;

	if (hdr.m_Magic != RPF6) {
		// Read and byte-swap the old-style header.
		fiPackHeaderOld hdrOld;

		m_Device->ReadBulk(m_Handle,m_ArchiveBias,&hdrOld.m_Magic,20);
		Convert_LE(&hdrOld.m_Magic,5);

		// Read and byte-swap the old-style directory.
		fiPackEntry2345 *oldEntries;

		oldEntries = (fiPackEntry2345*) rage_new char [hdrOld.m_TotalSize];
		m_Device->ReadBulk(m_Handle,m_ArchiveBias + 2048,oldEntries,hdrOld.m_TotalSize);
		char *oldNameHeap = (char*)(oldEntries + hdrOld.m_EntryCount);

		AES aes(hdrOld.m_Encrypted);
		if (hdrOld.m_Encrypted)
			aes.Decrypt(oldEntries,hdrOld.m_TotalSize);

		Convert_LE(&oldEntries[0].m_NameOffsetOrHashCode,4*hdrOld.m_EntryCount);

		m_Entries = rage_new fiPackEntry6[hdrOld.m_EntryCount];
		m_EntryCount = hdrOld.m_EntryCount;

		// If we have a name heap in the file, we'll need to respect it since the sort method changes.
		bool hasNameHeap = hdrOld.m_Magic == RPF2 || hdrOld.m_Magic == RPF4;
		if (hdrOld.m_Magic == RPF4 || hdrOld.m_Magic == RPF5)
			m_OffsetShift = fiBulkOffsetShift;

		u32 nameHeapSize = hdrOld.m_TotalSize - hdrOld.m_EntryCount * sizeof(fiPackEntry2345);

		// Convert entries to wider field.  Note that we don't support new-style resources
		// since we'd lose the correct value of m_ConsumedSize.
		for (u32 i=0; i<m_EntryCount; i++) {
			//Assert(!oldEntries[i].s.any.m_IsNewResource);
			m_Entries[i].m_NameHash = hasNameHeap? atStringHash(oldNameHeap + oldEntries[i].m_NameOffsetOrHashCode) : oldEntries[i].m_NameOffsetOrHashCode;
			if (oldEntries[i].u.any.m_IsDir) {
				m_Entries[i].m_ConsumedSize = 0;
				m_Entries[i].u.words[0] = oldEntries[i].u.words[0];
				m_Entries[i].u.words[1] = oldEntries[i].u.words[1];
			}
			else if (oldEntries[i].u.any.m_IsResource) {
				m_Entries[i].m_ConsumedSize = oldEntries[i].s.any.m_UncompSize;
				m_Entries[i].u.words[0] = oldEntries[i].u.words[0];
				m_Entries[i].u.words[1] = oldEntries[i].u.words[1];
			}
			else { // file
				m_Entries[i].m_ConsumedSize = oldEntries[i].u.file.m_CompSize;
				m_Entries[i].u.words[0] = oldEntries[i].u.words[0];
				m_Entries[i].u.file.m_UncompressedSize = oldEntries[i].s.any.m_UncompSize;
				m_Entries[i].u.file.m_IsCompressed = oldEntries[i].u.file.m_IsCompressed;
				m_Entries[i].u.file.m_IsResource = oldEntries[i].u.file.m_IsResource;
			}
			// Temporarily remember the name offset here since old assets don't support new resources anyway.
			m_Entries[i].u.words[2] = hasNameHeap? oldEntries[i].m_NameOffsetOrHashCode : 0;
		}
		// Re-sort entire directory by hash code
		if (hasNameHeap)
			RecurseSort(m_Entries,0,1);

		if (hasNameHeap) {
			u32 ft = ConvertFileTimeToRpfOldTime(m_FileTime);
			m_MetaData = (fiPackMetaData*) rage_new char[sizeof(fiPackMetaData)*hdrOld.m_EntryCount + nameHeapSize];
			for (u32 i=0; i<m_EntryCount; i++) {
				m_MetaData[i].m_NameOffset = m_Entries[i].u.words[2];
				m_MetaData[i].m_FileTime = ft;
			}
			m_NameHeap = (char*)(m_MetaData + hdrOld.m_EntryCount);
			memcpy(m_NameHeap, oldNameHeap, nameHeapSize);
		}
		else {
			m_MetaData = 0;
			m_NameHeap = 0;
		}
		// Reset the temporary name offset field if we had used it to avoid looking like a new resource.
		if (hasNameHeap)
			for (u32 i=0; i<m_EntryCount; i++)
				m_Entries[i].u.words[2] = 0;
	}
	else 
	{
		u32 entrySize = (hdr.m_EntryCount * sizeof(fiPackEntry6) + 15) & ~15U;

		m_Entries = (fiPackEntry6*) rage_new char[entrySize];
		m_Device->ReadBulk(m_Handle,m_ArchiveBias + sizeof(hdr),m_Entries,entrySize);
		AES aes(hdr.m_Encrypted);
		if (hdr.m_Encrypted)
			aes.Decrypt(m_Entries,entrySize);
		Convert_BE((u32*)&m_Entries[0],(sizeof(fiPackEntry6)/sizeof(u32))*hdr.m_EntryCount);

		// If we want to keep the name heap, and the archive has one, read it.
		// TODO: We need to either find a way to make metadata work again, or rip
		// out the headerDataPtr code below.
		if (hdr.m_MetaDataOffset) {
			u32 size = u32(m_ArchiveSize - ((u64)hdr.m_MetaDataOffset << 3));
			char *buffer;

			buffer = rage_new char[size];
			m_MetaData = (fiPackMetaData*) buffer;
			m_Device->ReadBulk(m_Handle,m_ArchiveBias + ((u64)hdr.m_MetaDataOffset << 3),buffer,size);

			if (hdr.m_Encrypted)
				aes.Decrypt(buffer,size);
			Convert_BE(&m_MetaData[0].m_NameOffset,2*hdr.m_EntryCount);
			m_NameHeap = (char*)(m_MetaData + m_EntryCount);
		}
		else {
			m_MetaData = 0;
			m_NameHeap = 0;
		}

		m_OffsetShift = fiBulkOffsetShift;
	}

	return true;
}

bool fiPackfileOld::MountAs(const char *path) {
	if( fiDevice::Mount( path, *this, true ) )
	{
		m_RelativeOffset = StringLength(path); 
		return true;
	}
	
	return false;
}

fiHandle fiPackfileOld::Create(const char * /*filename*/) const {
	// Never legal to create a file inside a zipfile
	return fiHandleInvalid;
}


fiHandle fiPackfileOld::Open(const char * filename,bool readOnly) const {
	return OpenInternal(filename + m_RelativeOffset,readOnly);
}


static sysCriticalSectionToken s_PackfileOldOpen;

#define STATIC_STATE_BUFFERS	(!__DEV && !__BANK && !__TOOL)

struct fiPackfileOldState {
	static const u32 BufferSize = 8192;
	bool IsCompressed() const;
#if STATIC_STATE_BUFFERS
	char						m_Buffer[BufferSize];
#else
	char						*m_Buffer;
#endif
	char						m_DebugName[32];
	const fiPackEntry6 *			m_Entry;			// Pointer to the file directory entry
	u32							m_Offset;			// Current offset within the (virtual, uncompressed) file
	u32							m_CompOffset;		// Current offset within the compressed file
	z_stream					m_zStream;			// Stream object used by Zlib

	fiPackfileOldState() : m_Entry(NULL) {}

	void Reset() {
		m_Entry = 0;
		m_Offset = 0;
		m_CompOffset = 0;
		memset(&m_zStream, 0, sizeof(z_stream)); // should we be nulling this?
	}
};

bool fiPackfileOldState::IsCompressed() const { 
	return m_Entry->IsCompressed();
}

atStaticPool<fiPackfileOldState,STATIC_STATE_BUFFERS? 4 : 16> s_States;

#if __WIN32
#pragma warning(push)
#pragma warning(disable:4702)	// Unreachable code
#endif

fiHandle fiPackfileOld::OpenInternal(const char *lookupName,bool readOnly) const {
	// Never legal to modify a file inside a zipfile
	if (!readOnly)
		return fiHandleInvalid;

	const fiPackEntry6 *e = FindEntry(lookupName);
	if (e && e->IsFile()) {	// Cannot open a directory this way
		SYS_CS_SYNC(s_PackfileOldOpen);

		if (s_States.IsFull()) {
			for (int i=0; i<s_States.GetSize(); i++)
				Displayf("%d. %s",i,s_States.GetElement(i).m_DebugName);
			Quitf("Out of zipfile descriptors opening %s, too many files in zips open at once (see tty for open files)",lookupName);
			return fiHandleInvalid;
		}

		fiPackfileOldState *s = s_States.New();

		s->Reset();
		if (e->IsCompressed())
		{
			RAGE_TRACK(PackfileState);
			sysMemStartTemp();
#if !STATIC_STATE_BUFFERS
			s->m_Buffer = rage_new char[fiPackfileOldState::BufferSize];
#endif
			if (inflateInit2(&s->m_zStream,-MAX_WBITS) != Z_OK) {
				Errorf("fiPackfileOld::Open(%s) - inflateInit failed.",lookupName);
				sysMemEndTemp();
				return fiHandleInvalid;
			}
			sysMemEndTemp();
		}
		s->m_Entry = e;
		s->m_Offset = 0;
		safecpy(s->m_DebugName,ASSET.FileName(lookupName));

		return (fiHandle)s;
	}
	else {
		// Displayf("Open(%s) failed",filename + m_RelativeOffset);
		return fiHandleInvalid;
	}
}
#if __WIN32
#pragma warning(pop)
#endif


void fiPackfileOld::FillData(fiFindData &outData,u32 entryIndex) const {
	Assert(m_NameHeap && m_MetaData);
	safecpy(outData.m_Name,m_NameHeap + m_MetaData[entryIndex].m_NameOffset,sizeof(outData.m_Name));
	bool isDir = m_Entries[entryIndex].IsDir();
	outData.m_Size = isDir?0:m_Entries[entryIndex].GetUncompressedSize();
	outData.m_LastWriteTime		= ConvertRpfOldTimeToFileTime(m_MetaData[entryIndex].m_FileTime);
	outData.m_Attributes		= FILE_ATTRIBUTE_READONLY | (isDir? FILE_ATTRIBUTE_DIRECTORY : 0);
}


fiHandle fiPackfileOld::FindFileBegin(const char *directoryName,fiFindData &outData) const {
	if (!m_NameHeap || !m_MetaData) {
		AssertMsg(false,"Archive doesn't have name heap, cannot search it");
		return fiHandleInvalid;
	}
	const fiPackEntry6 *e = FindEntry(directoryName + m_RelativeOffset);
	if (e && e->IsDir() && e->u.directory.m_DirectoryCount) {	// must be a non-empty directory
		SYS_CS_SYNC(s_PackfileOldOpen);

		if (s_States.IsFull())
			return fiHandleInvalid;

		fiPackfileOldState *s = s_States.New();
		s->m_Entry = e;
		s->m_Offset = 0;
		FillData(outData,e->u.directory.m_DirectoryIndex);
		return (fiHandle)(size_t)(s);
	}
	else
		return fiHandleInvalid;
}


bool fiPackfileOld::FindFileNext(fiHandle _handle,fiFindData &outData) const {
	fiPackfileOldState &s = *(fiPackfileOldState*)_handle;
	if (++s.m_Offset < s.m_Entry->u.directory.m_DirectoryCount) {
		FillData(outData,s.m_Entry->u.directory.m_DirectoryIndex + s.m_Offset);
		return true;
	}
	else
		return false;
}


int fiPackfileOld::FindFileEnd(fiHandle _handle) const {
	SYS_CS_SYNC(s_PackfileOldOpen);
	s_States.Delete((fiPackfileOldState*)_handle);
	return 0;
}

// can't do a simple subtraction any longer, the hash is a full 32 bits.
inline int hashcmp(u32 a,u32 b) {
	if (a > b)
		return 1;
	else if (b > a)
		return -1;
	else
		return 0;
}

const fiPackEntry6* fiPackfileOld::FindEntry(const char *name) const {
	fiPackEntry6 *e = m_Entries;	// root directory
	if (!e)
		return NULL;

	// remove any leading slash since the root dir is the starting point
	// but it may not be present in the name.
	if (*name == '/' || *name == '\\')
		++name;
	// return the root dir, really only for FindFirst and siblings
	if (!*name)
		return e;

	do {
		// Get min and max entries to search
		Assertf(e->IsDir(), "Found a file where a directory was assumed. Verify the path name: %s", name);
		u32 low = e->u.directory.m_DirectoryIndex;
		u32 high = low + e->u.directory.m_DirectoryCount - 1;
		e = NULL;
		char currentPart[RAGE_MAX_PATH], *cp = currentPart;
		while (*name && *name!='/' && *name!='\\') {
			*cp++ = (*name>='A'&&*name<='Z')?*name+('a'-'A'):*name;
			++name;
		}
		while (*name == '/' || *name == '\\')
			++name;
		*cp = 0;
		u32 currentHash = atStringHash(currentPart);
		while (low <= high) {
			u32 mid = (low + high) >> 1;
			u32 nameOffset = m_Entries[mid].m_NameHash;
			int result = hashcmp(currentHash,nameOffset);
			if (result < 0)
				high = mid-1;
			else if (result > 0)
				low = mid+1;
			else {
				e = &m_Entries[mid];
				break;
			}
		}
	} while (e && e->IsDir() && *name);
	return e;
}


int fiPackfileOld::Close(fiHandle _handle) const {
	fiPackfileOldState *s = (fiPackfileOldState*)_handle;
	SYS_CS_SYNC(s_PackfileOldOpen);
	if (s->IsCompressed()) {
		sysMemStartTemp();
		inflateEnd(&s->m_zStream);
#if !STATIC_STATE_BUFFERS
		delete[] s->m_Buffer;
		s->m_Buffer = NULL;
#endif
		sysMemEndTemp();
	}
	s_States.Delete(s);
	return 0;
}


int fiPackfileOld::Read(fiHandle _handle,void *outBuffer,int bufferSize) const {
	fiPackfileOldState &s = *(fiPackfileOldState*)_handle;
	return ReadInternal(s,outBuffer,bufferSize);
}

int fiPackfileOld::ReadInternalBulk(const fiPackfileOldState& s,u32 relativeOffset,void *outBuffer,int toRead) const
{
	// Eliminate zero-byte requests before getting any further.
	if (toRead == 0)
		return 0;
	// If we got here, assume it's an optical read and therefore worth warning the cache thread about.
	int result = m_Device->ReadBulk(m_Handle, m_ArchiveBias + ((u64)s.m_Entry->GetFileOffset() << m_OffsetShift) + relativeOffset,outBuffer,toRead);
	return result;
}

int	fiPackfileOld::ReadInternal(fiPackfileOldState& s,void *outBuffer,int bufferSize) const
{
	if (s.IsCompressed()) {
		s.m_zStream.next_out = (Bytef*) outBuffer;
		s.m_zStream.avail_out = bufferSize;

		while (s.m_zStream.avail_out) {
			// queue up some source data if we need some
			if (s.m_zStream.avail_in == 0) {
				u32 toRead = fiPackfileOldState::BufferSize;
				if (toRead > u32(s.m_Entry->m_ConsumedSize - s.m_CompOffset))
					toRead = u32(s.m_Entry->m_ConsumedSize - s.m_CompOffset);
				int thisRead = ReadInternalBulk(s,s.m_CompOffset,s.m_Buffer,toRead);
				s.m_CompOffset += (s.m_zStream.avail_in = thisRead);
				s.m_zStream.next_in = (Bytef*) s.m_Buffer;
			}

			// set destination for inflation.
			sysMemStartTemp();
			int err = inflate(&s.m_zStream,Z_SYNC_FLUSH);
			sysMemEndTemp();

			if (err == Z_STREAM_END) 
			{	
				// this is fine, but won't necessarily happen because we don't have a zlib header.
				// Displayf("zipHandle::Read - Normal EOF reached.");
				//inflateDeallocWindow(&s.m_zStream);
				break;
			}

			if (err < 0)
			{
				switch (err)
				{
					case Z_ERRNO:
						Displayf("Disc error: Error in the file system");
						break;

					case Z_STREAM_ERROR:
						Displayf("Disc error: Streaming error");
						break;

					case Z_DATA_ERROR:
						Displayf("Disc error: Data error: %s", s.m_zStream.msg);
						break;

					case Z_MEM_ERROR:
						Displayf("Disc error: Memory error");
						break;

					case Z_BUF_ERROR:
						Displayf("Disc error: Buffer error");
						break;

					case Z_VERSION_ERROR:
						Displayf("Disc error: Version error");
						break;

					default:
						Displayf("Disc error: Unknown error!");
						break;
				}
				Quitf("Fatal disc error");
			}
		}

		// Figure out how much we actually read by subtracting out what was left unfilled.
		bufferSize -= s.m_zStream.avail_out;
		s.m_Offset += bufferSize;
		return bufferSize;
	}
	else {
		u32 amtToRead = bufferSize;
		if (amtToRead > s.m_Entry->GetUncompressedSize() - s.m_Offset)
			amtToRead = s.m_Entry->GetUncompressedSize() - s.m_Offset;
		amtToRead = ReadInternalBulk(s,s.m_Offset,outBuffer,amtToRead);
		s.m_Offset += amtToRead;
		return amtToRead;
	}
}

int fiPackfileOld::Size(fiHandle _handle) const {
	fiPackfileOldState &s = *(fiPackfileOldState*)_handle;
	return s.m_Entry->GetUncompressedSize();
}


u64 fiPackfileOld::Size64(fiHandle _handle) const {
	fiPackfileOldState &s = *(fiPackfileOldState*)_handle;
	return s.m_Entry->GetUncompressedSize();
}


int fiPackfileOld::Write(fiHandle /*handle*/,const void * /*buffer*/,int /*bufferSize*/) const {
	return -1;
}


int fiPackfileOld::Seek(fiHandle _handle,int offset,fiSeekWhence whence) const {
	return (int)Seek64(_handle,offset,whence);
}

u64 fiPackfileOld::Seek64(fiHandle _handle,s64 offset,fiSeekWhence whence) const {
	fiPackfileOldState &s = *(fiPackfileOldState*)_handle;

	if (s.IsCompressed()) {
		u32 destPos;
		switch (whence) {
		case seekSet: destPos = (u32) offset; break;
		case seekEnd: destPos = (u32)(s.m_Entry->GetUncompressedSize() + offset); break;
		case seekCur: destPos = (u32)(s.m_Offset + offset); break;
		default:
			return (u64)(s64)-1;
		}
		if (destPos > s.m_Entry->GetUncompressedSize())
			destPos  = s.m_Entry->GetUncompressedSize();

		if (destPos < s.m_Offset) {
			s.m_Offset = s.m_CompOffset = 0;
			sysMemStartTemp();
			inflateReset(&s.m_zStream);
			sysMemEndTemp();
		}

		while (destPos > s.m_Offset) {
			char buffer[4096];
			unsigned toRead = destPos - s.m_Offset;
			if (toRead > sizeof(buffer))
				toRead = sizeof(buffer);
			if (ReadInternal(s,buffer,toRead) == -1)
				return (u64)(s64)-1;
		}

		return destPos;
	}
	else {
		u32 newPos =
			(whence == seekEnd)? u32(s.m_Entry->GetUncompressedSize() + offset) :
			(whence == seekCur)? u32(s.m_Offset + offset) :
			u32(offset);
		if (newPos > s.m_Entry->GetUncompressedSize())
			newPos = s.m_Entry->GetUncompressedSize();
		s.m_Offset = (u32) newPos;		// TODO: Support >4G files within archives?
		return newPos;
	}
}


u64 fiPackfileOld::GetFileTime(const char * filename) const {
	const fiPackEntry6 *e = FindEntry(filename + m_RelativeOffset);
	if (e) {
		if (m_MetaData)
			return ConvertRpfOldTimeToFileTime(m_MetaData[e - m_Entries].m_FileTime);
		else
			return m_FileTime;
	}
	else
		return 0;
}

bool fiPackfileOld::SetFileTime(const char * /*filename*/,u64 /*timestamp*/) const {
	return false;
}


u32 fiPackfileOld::GetAttributes(const char * filename) const {
	const fiPackEntry6 *e = FindEntry(filename + m_RelativeOffset);
	if (e)
		return FILE_ATTRIBUTE_READONLY;
	else
		return FILE_ATTRIBUTE_INVALID;
}


u64 fiPackfileOld::GetFileSize(const char * filename) const {
	const fiPackEntry6 *e = FindEntry(filename + m_RelativeOffset);
	if (e)
		return e->GetUncompressedSize();
	else
		return 0;
}


fiHandle fiPackfileOld::OpenBulk(const char *filename,u64 &outBias) const {
	const fiPackEntry6 *e = FindEntry(filename + m_RelativeOffset);
	if (e) {
		if (!e->IsCompressed()) {
			outBias = (u64) e->GetFileOffset() << m_OffsetShift;
			return m_Handle;
		}
		else
			Warningf("fiPackfileOld::OpenBulk - '%s' is externally compressed, cannot open with OpenBulk.",filename);
	}
	return fiHandleInvalid;
}


int fiPackfileOld::ReadBulk(fiHandle handle,u64 offset,void *outBuffer,int bufferSize) const {
	AssertMsg(handle == m_Handle,"You are probably trying to load a zip with STREAM_FROM_ARCHIVES_ONLY enabled.");
	offset += m_ArchiveBias;

	u64 totalSize = m_ArchiveSize;
	if (offset + bufferSize > totalSize)
		bufferSize = (int)(totalSize - offset);
	return m_Device->ReadBulk(handle,offset,outBuffer,bufferSize);
}

int fiPackfileOld::CloseBulk(fiHandle /*handle*/) const {
	// Nothing to do here, no real handle allocated.
	return 0;
}

const char *fiPackfileOld::GetDebugName() const {
	return m_Name;
}

}	// namespace rage
