//
// rage_ps3.cpp
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
//

#include "rage_ps3.h"

// rage includes
#include "grcore/texturegcm.h"
#include "grmodel/modelfactory.h"
#include "grmodel/shader.h"
#include "grmodel/shadergroup.h"
#include "phcore/materialmgrimpl.h"
#include "system/platform.h"
#include "system/param.h"
#include "mesh/mesh.h"
#include "grmodel/geometry.h"
#include "paging/rscbuilder.h"

bool RagePlatformPs3::sm_Edgify = true;
bool RagePlatformPs3::sm_EdgePositionsInMainMem = false;	// NS: false = default mode for compatibility - turn to true to have everything but CCDs with positions in main memory

static char* s_nonEdgeShaderNames[] = 
{
	// breakable
	"gta_glass", 
	"glass", 
	"gta_glass_spec", 
	"glass_spec", 
	"gta_glass_reflect", 
	"glass_reflect", 
	"gta_glass_normal_spec_reflect",
	"glass_normal_spec_reflect",
	"gta_glass_emissive", 
	"glass_emissive", 
	"gta_glass_emissivenight", 
	"glass_emissivenight", 
	"vehicle_vehglass",
	// tires
	"vehicle_tire",
	// cloth
	"cloth_default",
	"cloth_normal_spec",
	"cloth_normal_spec_alpha",
	"cloth_normal_spec_cutout",
	"cloth_spec_alpha",
	"cloth_spec_cutout",
	"cloth_sheet_dynamic",
	"vehicle_cloth",
	// trees LOD:
	"trees_lod2",
	"trees_lod2d",
	// cable
	"cable",
	"wire2", // TODO -- remove this soon
	//grass instance batch
	"grass_batch",
};
static const int s_nonEdgeShaderNamesSize = sizeof(s_nonEdgeShaderNames)/sizeof(s_nonEdgeShaderNames[0]);

static char* s_nonEdgeSpsNames[] = 
{
	// cloth
	"vehicle_cloth.sps",
	// used for the distant car model
	"default_noedge.sps",
};
static const int s_nonEdgeSpsNamesSize = sizeof(s_nonEdgeSpsNames)/sizeof(s_nonEdgeSpsNames[0]);

static char* s_EdgeVehicleDamageShaderNames[] = 
{
	"vehicle_badges",
	// vehicle_basic
	// vehicle_blurredrotor
	"vehicle_decal",
	"vehicle_detail",
	"vehicle_generic",
	"vehicle_interior",
	"vehicle_interior2",
	"vehicle_licenseplate",
	"vehicle_lightsemissive",
	"vehicle_mesh",
	"vehicle_mesh_enveff"
	"vehicle_paint1",
	"vehicle_paint1_enveff",
	"vehicle_paint2",
	"vehicle_paint2_enveff",
	"vehicle_paint3",
	"vehicle_paint3_enveff",
	"vehicle_paint4",
	"vehicle_paint4_enveff",
	"vehicle_paint5_enveff",
	"vehicle_paint6",
	"vehicle_paint6_enveff",
	"vehicle_shuts",
	// vehicle_tire
	// vehicle_track
	"vehicle_vehglass",
	"vehicle_vehglass_crack",
	"vehicle_vehglass_inner"
};
static const int s_EdgeVehicleDamageShaderNamesSize = sizeof(s_EdgeVehicleDamageShaderNames)/sizeof(s_EdgeVehicleDamageShaderNames[0]);

static char* s_EdgePedShaderNames[] = 
{
	"ped",
	"ped_cloth",
	"ped_decal",
	"ped_decal_exp",
	"ped_decal_medals",
	"ped_decal_nodiff",
	"ped_default",
	"ped_default_cloth",
	"ped_default_enveff",
	"ped_default_palette",
	"ped_enveff",
	"ped_hair_cutout_alpha",
	"ped_hair_cutout_alpha_cloth",
	"ped_hair_spiked",
	"ped_hair_spiked_enveff",
	"ped_palette",
	"ped_stubble",
	"ped_wrinkle",
	"ped_wrinkle_cloth",
	"ped_wrinkle_cs"
};
static const int s_EdgePedShaderNamesSize = sizeof(s_EdgePedShaderNames)/sizeof(s_EdgePedShaderNames[0]);

#define FORCE_EDGE_OFF 0
#define PARTIAL_EDGE_OFF 0	// GTAIV kind of setup.
#define FORCE_IDXBUFFER_IN_MAIN 0

static void GeometryParamsHookPs3(const grmModel& model, const mshMesh &mesh, const grmShaderGroup *pShaderGroup, int index, GeometryCreateParams* params)
{
	Assert(g_sysPlatform == platform::PS3);

	const int shaderIndex = model.GetShaderIndex(index);
	const char* pShaderName = pShaderGroup->GetShader(shaderIndex).GetName();

	const bool isInstanced = pShaderGroup->GetShader(shaderIndex).IsInstanced();
	const bool isGrassBatch = !stricmp("grass_batch", pShaderName);

	if(isGrassBatch)
		params->IsGpuInstanced = true;	//Need to duplicate index buffer on PS3 for grass instancing.

	params->Edge				= !isInstanced && !isGrassBatch;
	params->EdgeTintColors		= false;
	params->EdgeTintColorsTrees = false;
	params->EdgeSlodGeom		= false;

	if (strstr(pShaderName, "_tnt"))
	{
		params->EdgeTintColors = true;

		if(strstr(pShaderName, "trees"))
		{
			params->EdgeTintColorsTrees = true;
		}
	}
	// BS#629341: Experiment#2: HD&SLOD=EDGE, SLOD1-2=QB, SLOD3-4=EDGE:
	else if (	(RagePlatform::GetDwdType() == RagePlatform::dwdSLOD1)	||
				(RagePlatform::GetDwdType() == RagePlatform::dwdSLOD2)	)  
	{
		params->Edge = false;
	}

	// BS#538203: slods using edge using minimal main memory
	if (RagePlatform::GetDwdType() >= RagePlatform::dwdSLOD1)
	{
		params->EdgeSlodGeom = true;
	}

	params->SkipCentroidGeneration = RagePlatform::GetHeadBlendGeometry() || RagePlatform::GetMicroMorphMesh();
	params->ReadWriteVtxBuf = RagePlatform::GetHeadBlendWriteBuffer();
	params->IsMicroMorph = RagePlatform::GetMicroMorphMesh();

#if FORCE_EDGE_OFF
	params->Edge = false;
#endif

	params->Edge = params->Edge && RagePlatformPs3::sm_Edgify && !isInstanced && !isGrassBatch;

	if(!params->Edge)
	{
		return;
	}

	params->EdgeVehicleDamage	= false;
	// vehicle shaders supporting damage require COLOR0 streams in Main:
	for (int j=0; j<s_EdgeVehicleDamageShaderNamesSize; j++)
	{
		if(!stricmp(pShaderName, s_EdgeVehicleDamageShaderNames[j]))
		{
			params->EdgeVehicleDamage = true;
			break;
		}
	}

	params->EdgePedGeom = false;
	// ped geometries use higher quality fixed pos compression:
	for (int j=0; j<s_EdgePedShaderNamesSize; j++)
	{
		if(!stricmp(pShaderName, s_EdgePedShaderNames[j]))
		{
			params->EdgePedGeom = true;
			break;
		}
	}

	if (!mesh.IsSkinned())
	{
		bool bThisMeshHasNonEdgeShader = false;

#if PARTIAL_EDGE_OFF
		bool bForceEdge = false;
#else // PARTIAL_EDGE_OFF
		bool bForceEdge = true;
#endif // PARTIAL_EDGE_OFF		
		
#if PARTIAL_EDGE_OFF
		if (model.GetAABBs())
		{
			// convert small objects (less than 7.0m distance from local zero pos) to edge
			float distance = 1000.0f;

			const spdAABB &aabb = model.GetGeometryAABB(index);
			Vector3 boxMin = aabb.GetMin();
			Vector3 boxMax = aabb.GetMax();

			distance = rage::Max(boxMin.Mag(), boxMax.Mag());				

			if (distance < 1.0f)
			{
				bForceEdge = true;
			}
		}

		if (!strncmp(pShaderName, "gta_wire", 8))
			bForceEdge = true;
#endif // PARTIAL_EDGE_OFF

		// If any of the shaders are tagged as smashable or tire, only allow up to GeometryCreateParams::INDICESONLY 
		// This is enough to guarantee that the correct edge type is chosen for the entire model.
		for (int i = 0; i < pShaderGroup->GetCount(); ++i)
		{
			for (int j = 0; j < s_nonEdgeShaderNamesSize;++j)
			{
				if (!stricmp(pShaderGroup->GetShader(i).GetName(), s_nonEdgeShaderNames[j]))
				{
					if (i == shaderIndex)
					{
						bThisMeshHasNonEdgeShader |= true;
					}
					break;
				}
			}

			if(pShaderGroup->GetShader(i).GetMtlFileName())
			{
				for (int j = 0; j < s_nonEdgeSpsNamesSize;++j)
				{
					if (!stricmp(pShaderGroup->GetShader(i).GetMtlFileName(), s_nonEdgeSpsNames[j]))
					{
						if (i == shaderIndex)
						{
							bThisMeshHasNonEdgeShader |= true;
						}
						break;
					}
				}
			}

			if (i > shaderIndex && bThisMeshHasNonEdgeShader)
				break;
		}

		if (bThisMeshHasNonEdgeShader)
		{
			params->Edge = false; 
		}
		else if ((bForceEdge || RagePlatformPs3::sm_EdgePositionsInMainMem) ) 
		{
			params->Edge = !isInstanced && !isGrassBatch;
		}
		else
		{
			params->Edge = false;
		}
	}
	else // mesh is skinned
	{
		// gta_vehicle_tire for skinned:
		// 1. cutscene tyres (geomCount=1) are skinned, but still should not be edge'ified
		// 2. in-game vehicles have some skinned parts which use gta_vehicle_tire, but they are to be
		//		edge'ified (vehicle models have more than 1 geometry)
		if (!strcmp(pShaderName, "vehicle_tire") || !strcmp(pShaderName, "gta_vehicle_tire"))
		{
			// Always avoid edgification on anything that has this shader applied. 
			// This makes it not taking the skinned render path and prevents instancing.
			params->Edge = false;
			return;
		}

		// Character cloth can be skinned too
		else if (!strcmp(pShaderName, "cloth_default"))
		{
			params->Edge = false;
			return;
		}
	}
}

#if !__64BIT

void RagePlatformPs3::Init()
{
	g_sysPlatform = sysGetPlatform("ps3");
	g_ByteSwap = sysGetByteSwap(g_sysPlatform);

	grcTextureFactoryGCM::CreatePagedTextureFactory();

	BaseInit();

	grmModelFactory::GetVertexConfigurator()->SetPackNormals(true);
	grmModelFactory::GetVertexConfigurator()->SetPackTexCoords(true);

	rage::atFunctor5<void, const grmModel&, const mshMesh&, const grmShaderGroup*, int, GeometryCreateParams*> functor;
	functor.Reset<&GeometryParamsHookPs3>();
	grmModel::SetGeometryParamsHook(functor);

//	rage::phOctreeCell::InitClass(32768, 32768);
	// configure edge
	grmModel::sm_EdgeGeomSkinned = false; // MIGRATE_FIXME
	grmModel::sm_EdgeGeomUnskinned = false;
}

#endif
