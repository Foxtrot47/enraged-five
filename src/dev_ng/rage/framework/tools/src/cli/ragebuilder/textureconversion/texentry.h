#ifndef TEXENTRY_H
#define TEXENTRY_H

// Local headers
#include "../textureconversion/textureconversion.h"

// RAGE headers
#include "system/memops.h"

// Boost headers
#include "boost/shared_ptr.hpp"

#ifndef MAX_PATH
#define MAX_PATH 260
#endif // !MAX_PATH

namespace rage
{

	// Higher-level abstraction for a texture entry.  This could be expanded upon to
	// correctly cache and re-use loaded TCL/TCS/TCP chains of data.
	// At some point this could actually load the TCL/TCS/TCP spec once and we
	// reuse that everywhere but thats quite an involved Ragebuilder change.
	struct TexEntry 
	{
		const char* name;			// Texture name.
		char basename[MAX_PATH];	// Texture basename; no extension.
		char filename[MAX_PATH];	// Texture filename; TIFF/DDS (if linked).
		u64 fileSize;				// Texture filesize.
		u32 index;
		bool splitHD;
		bool splitHD2;	
		bool linked;
		char specificationFilename[MAX_PATH]; // Specification filename.
		boost::shared_ptr<CTextureConversionSpecification> pSpec; // Texture specification.

		TexEntry( );
		TexEntry( const TexEntry& other );

		bool LoadSpecification( const char* specFilename );
	};

	struct TexEntrySorter {
		bool operator()( TexEntry const & attr1, TexEntry const & attr2 ) const 
		{
			return attr1.fileSize > attr2.fileSize;
		}
	};

} // rage namespace

#endif // TEXENTRY_H
