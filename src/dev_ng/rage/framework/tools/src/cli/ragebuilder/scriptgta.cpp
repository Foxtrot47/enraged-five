#include "scriptgta.h"

#include "ragebuilder.h"
#include "log.h"
#include "gta.h"
#include "resource.h"

#include "grmodel/geometry.h"
#include "grcore/vertexbuffer.h"
#include "grcore/vertexbuffereditor.h"
#include "rmcore/drawable.h"
#include "fragment/drawable.h"
#include "atl/string.h"
#include "paging/rscbuilder.h"

// Static Cache Data
static char* gp_Transform = NULL;


bool RBLoadDefinitions(const char* filename)
{
	GtaData::m_ideSet.load(filename);
	rbTracef3("loaded ide data - %s", filename);

	return true;
}

bool RBLoadPlacements(const char* filename)
{
	GtaData::m_iplSet.load(filename);

	return true;
}

bool RBSaveDefinitions(const char* filename)
{
	if(RageBuilder::GetPlatform() == RageBuilder::pltIndependent)
	{
		GtaData::m_ideSet.save(filename);
	}
	else
	{
		GtaData::m_ideSet.saveBinary(filename,RageBuilder::GetPlatformImpl()->IsEndianSwapping());
	}

	return true;
}

bool RBSavePlacements(const char* filename)
{
	if(RageBuilder::GetPlatform() == RageBuilder::pltIndependent)
	{
		GtaData::m_iplSet.save(filename);
	}
	else
	{
		GtaData::m_iplSet.saveBinary(filename,RageBuilder::GetPlatformImpl()->IsEndianSwapping());
	}

	return true;
}

void RBClearDefinitions()
{
	GtaData::m_ideSet.reset();
}

void RBClearPlacements()
{
	GtaData::m_iplSet.reset();
}

int RBGetNumMilos()
{
	return GtaData::m_ideSet.getNumMilos();
}

const char* RBGetMiloName(int miloidx)
{
	return GtaData::m_ideSet.getMiloName(miloidx);
}

int RBGetMiloNumInstances(int miloidx)
{
	return GtaData::m_ideSet.getMiloNumInstances(miloidx);
}

const char* RBGetMiloInstanceName(int miloidx,int instidx)
{
	return GtaData::m_ideSet.getMiloInstanceName(miloidx,instidx);
}

int RBGetMiloNumRooms(int miloidx)
{
	return GtaData::m_ideSet.getMiloNumRooms(miloidx);
}

const char* RBGetMiloRoomName(int miloidx,int roomidx)
{
	return GtaData::m_ideSet.getMiloRoomName(miloidx,roomidx);
}

int RBGetMiloRoomNumInstances(int miloidx,int roomidx)
{
	return GtaData::m_ideSet.getMiloRoomNumInstances(miloidx,roomidx);
}

int RBGetMiloRoomInstance(int miloidx,int roomidx,int instidx)
{
	return GtaData::m_ideSet.getMiloRoomInstance(miloidx,roomidx,instidx);
}

int RBGetNumDefinitions()
{
	return GtaData::m_ideSet.getNumObjects();
}

const char* RBGetObjectName(int objectidx)
{
	return GtaData::m_ideSet.getObjectName(objectidx);
}

const char* RBGetObjectTxdName(int objectidx)
{
	return GtaData::m_ideSet.getTxdName(objectidx);
}

float RBGetObjectLodDistance(int objectidx)
{
	return GtaData::m_ideSet.getLodDistance(objectidx);
}

int RBGetObjectFlags(int objectidx)
{
	return GtaData::m_ideSet.getObjectFlags(objectidx);
}

const char* RBGetDDName(int objectidx)
{
	return GtaData::m_ideSet.getDdName(objectidx);
}

int RBGetNumTimeDefinitions()
{
	return GtaData::m_ideSet.getNumTimeObjects();
}

const char* RBGetTimeObjectName(int objectidx)
{
	return GtaData::m_ideSet.getTimeObjectName(objectidx);
}

int RBGetNumInstances()
{
	return GtaData::m_iplSet.getNumInstances();
}

const char* RBGetInstanceName(int instidx)
{
	return GtaData::m_iplSet.getInstanceName(instidx);
}

const char* RBGetInstanceTransform(int instidx)
{
	if ( gp_Transform )
		delete gp_Transform;

	atString sTransform = GtaData::m_iplSet.getInstanceTransform(instidx);
	const char* pBuffer = sTransform.c_str();
	gp_Transform = new char[ strlen( pBuffer ) ];
	memset( gp_Transform, '\0', strlen( pBuffer ) );
	strcpy( gp_Transform, pBuffer );

	return ( gp_Transform );
}

int RBGetInstanceIsLod(int instidx)
{
	return GtaData::m_iplSet.getInstanceIsLod(instidx) ? 1 : 0;
}

int RBGetInstanceBlocks(int instidx)
{
	return GtaData::m_iplSet.getInstanceBlock(instidx);
}

int RBGetNumBlocks()
{
	return GtaData::m_iplSet.getNumBlocks();
}

const char* RBGetBlockName(int blockidx)
{
	static atString name;
	GtaData::m_iplSet.getBlockName(blockidx,name);
	return name;
}

const char* RBGetBlockPoints(int blockidx)
{
	static atString points;
	GtaData::m_iplSet.getBlockPoints(blockidx,points);
	return points;
}

float RBGetBlockArea(int blockidx)
{
	return GtaData::m_iplSet.getBlockArea(blockidx);
}

int RBGetNumPeds()
{
	return GtaData::m_ideSet.m_listPedDefs.GetCount();
}

const char* RBGetPedName(int pedidx)
{
	return GtaData::m_ideSet.m_listPedDefs[pedidx].name;
}

static rmcDrawable* g_InfoDrawable = NULL;
static fragType* g_InfoFragment = NULL;
static int g_NumPolys = 0;
static int g_NumShaders = 0;
static int g_NumTextures = 0;
static int g_NumGeoms = 0;
static atStringArray g_ShaderNames;
static atArray<int>	g_ShaderBuckets;
static atStringArray g_TextureNames;
static float g_Volume = 0;

void SetModelInfo(const grmModel& r_model)
{
	g_NumGeoms = r_model.GetGeometryCount();
	g_NumShaders = g_InfoDrawable->GetShaderGroup().GetCount();
	g_NumPolys = 0;
	g_NumTextures = 0;

	sysMemStartTemp();
	g_ShaderNames.Reset();
	g_ShaderBuckets.Reset();
	g_TextureNames.Reset();
	if ( g_NumShaders > 0 )
	{
		const grmShaderGroup& shaderGroup = g_InfoDrawable->GetShaderGroup( );
		for ( int i = 0; i < g_NumShaders; ++i )
		{
			grmShader& shader = shaderGroup.GetShader( i );
			const char* pShaderName = shader.GetName();
			Assertf( pShaderName, "Fetched ShaderName invalid." );

			g_ShaderNames.PushAndGrow( atString( pShaderName ) );
			g_ShaderBuckets.PushAndGrow( shader.GetDrawBucket() );

			const grcInstanceData& instanceData = shader.GetInstanceData( );
			for ( int j = 0; j < instanceData.Count; ++j )
			{
				if ( instanceData.IsTexture( j ) )
				{
					grcTexture* pTexture = instanceData.Entries[ j ].Texture;
					Assertf( pTexture, "Invalid texture read while loading Model Info." );
					if ( !pTexture )
						continue;

					const char* pTextureName = pTexture->GetName();
					Assertf( pTextureName, "Invalid texture name read while loading Model Info." );
					if ( !pTextureName )
						continue;

					++g_NumTextures;
					g_TextureNames.PushAndGrow( atString( pTextureName ) );
				}
			}
		}
	}
	sysMemEndTemp();

	bool FirstVert = true;
	Vector3 BoundMin;
	Vector3 BoundMax;

	for ( int i=0; i<g_NumGeoms; i++ )
	{
		grmGeometry& r_geom = r_model.GetGeometry(i);
		grcVertexBuffer* p_vertBuffer = r_geom.GetVertexBuffer();
		grcVertexBufferEditor editor(p_vertBuffer);
		int vertCount = p_vertBuffer->GetVertexCount();

		for(int j=0;j<vertCount;j++)
		{
			if(FirstVert)
			{
				FirstVert = false;
				BoundMin = BoundMax = editor.GetPosition(j);
			}
			else
			{
				Vector3 BoundCheck = editor.GetPosition(j);

				for(int k=0;k<3;k++)
				{
					if(BoundMin[k] > BoundCheck[k])
						BoundMin[k] = BoundCheck[k];

					if(BoundMax[k] < BoundCheck[k])
						BoundMax[k] = BoundCheck[k];
				}
			}
		}

		int primCount = r_geom.GetPrimitiveCount();
		int triCount = 0;

		switch(r_geom.GetPrimitiveType())
		{
		case drawTris:
			triCount = primCount;
			break;
		case drawTriStrip:
			triCount = primCount;
			break;
		case drawTriFan:
			break;
		case drawQuads:
			break;
		case drawRects:
			break;
		case drawPoints:
		case drawLines:
		case drawLineStrip:
		default:
			primCount = 0;
			break;
		}

		g_NumPolys += triCount;
	}

	g_Volume = (BoundMax[0] - BoundMin[0]) * (BoundMax[1] - BoundMin[1]) * (BoundMax[2] - BoundMin[2]);
}

bool RBInfoModelLoad(const char* filename)
{
	bool retval = false;

	for (int pass=0; pass<=pgRscBuilder::LAST_PASS; ++pass)
	{
		RageBuilder::BeginBuild(filename, "", pass==1?true:false,( 64 * 1024 * 1024 ));

		g_InfoDrawable = new rmcDrawable;

		SetRelativePathToFile(filename);

		bool rt = g_InfoDrawable->Load(filename);

		if((pass==0) && (rt))
		{
			const grmModel& r_model = g_InfoDrawable->GetLodGroup().GetLodModel0(LOD_HIGH);

			SetModelInfo(r_model);

			retval = true;
		}
		else
		{
			g_InfoDrawable = NULL;
		}

		//ASSET.PopFolder();

		RageBuilder::EndBuild(g_InfoDrawable);
	}

	return retval;
}

bool RBInfoModelClose()
{
	g_InfoDrawable = NULL;
	return true;
}

bool RBInfoFragmentLoad(const char* filename)
{
	bool retval = false;

	for (int pass=0; pass<=pgRscBuilder::LAST_PASS; ++pass)
	{
		RageBuilder::BeginBuild(filename, "", pass==1?true:false,( 64 * 1024 * 1024 ));

		const char* pFilename = filename;

		SetRelativePathToFile(pFilename);

		g_InfoFragment = fragType::Load(pFilename);

		bool rt = g_InfoFragment == NULL ? false : true;

		if((pass==0) && (rt))
		{
			const grmModel& r_model = g_InfoFragment->GetCommonDrawable()->GetLodGroup().GetLodModel0(LOD_HIGH);

			SetModelInfo(r_model);

			retval = true;
		}
		else
		{
			g_InfoFragment = NULL;
		}

		//ASSET.PopFolder();

		RageBuilder::EndBuild(g_InfoDrawable);
	}

	return retval;
}

bool RBInfoFragmentClose()
{
	g_InfoDrawable = NULL;
	return true;
}

int RBInfoModelNumPolys()
{
	return g_NumPolys;
}

int RBInfoModelNumShaders()
{
	return g_NumShaders;
}

int RBInfoModelNumTextures()
{
	return g_NumTextures;
}

int RBInfoModelNumGeoms()
{
	return g_NumGeoms;
}

const char* RBInfoModelShaderName(int nIndex)
{
	Assertf( nIndex >= 0, "Shader index starts at 0." );
	Assertf( nIndex < (int)g_ShaderNames.size(), "Shader index (0-based) exceeds count." );
	if ( ( nIndex >= (int)g_ShaderNames.size() ) || ( nIndex < 0 ) )
	{
		return NULL;
	}

	return g_ShaderNames[nIndex].c_str();
}

int RBInfoModelShaderDrawBucket(int nIndex)
{
	Assertf( nIndex >= 0, "Shader index starts at 0." );
	Assertf( nIndex < (int)g_ShaderNames.size(), "Shader index (0-based) exceeds count." );
	if ( ( nIndex >= (int)g_ShaderNames.size() ) || ( nIndex < 0 ) )
	{
		return -1;
	}

	return g_ShaderBuckets[nIndex];
}

const char* RBInfoModelTextureName(int nIndex)
{
	Assertf( nIndex >= 0, "Texture index starts at 0." );
	Assertf( nIndex < (int)g_TextureNames.size(), "Texture index (0-based) exceeds count." );
	if ( ( nIndex >= (int)g_TextureNames.size() ) || ( nIndex < 0 ) )
	{
		return NULL;
	}

	return g_TextureNames[nIndex].c_str();
}

float RBInfoModelVolume()
{
	return g_Volume;
}
