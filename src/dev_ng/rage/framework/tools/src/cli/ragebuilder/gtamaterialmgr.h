#ifndef RAGEBUILDER_GTAMATMGR_H_
#define RAGEBUILDER_GTAMATMGR_H_

#include "phcore/materialmgr.h"
#include "atl/string.h"

namespace rage {

// poly flags
// NOTE: This must line up with \game\physics\gtaMaterial.h polyflags
enum
{
	POLYFLAG_STAIRS					= 0,
	POLYFLAG_NOT_CLIMBABLE,
	POLYFLAG_SEE_THROUGH,
	POLYFLAG_SHOOT_THROUGH,
	POLYFLAG_NOT_COVER,
	POLYFLAG_WALKABLE_PATH,
	POLYFLAG_NO_CAM_COLLISION,
	POLYFLAG_SHOOT_THROUGH_FX,
	POLYFLAG_NO_DECAL,
	POLYFLAG_NO_NAVMESH,
	POLYFLAG_NO_RAGDOLL,
	POLYFLAG_RESERVED_FOR_TOOLS,							// This flag is used by tools only, and is should always be cleared on resources. 
	POLYFLAG_VEHICLE_WHEEL = POLYFLAG_RESERVED_FOR_TOOLS,	// This flag is only used at runtime so it takes the same slot as the tools only flag POLYFLAG_RESERVED_FOR_TOOLS
	POLYFLAG_NO_PTFX,
	POLYFLAG_TOO_STEEP_FOR_PLAYER,
	POLYFLAG_NO_NETWORK_SPAWN,
	POLYFLAG_NO_CAM_COLLISION_ALLOW_CLIPPING,

	POLYFLAG_NUM_FLAGS															// only 16 bits available
};

class GtaMaterial : public phMaterial
{
public:
	GtaMaterial(){}
	GtaMaterial(const atString& name) : m_Name(name), m_IsSeeThrough(false), m_IsShootThrough(false), m_IsShootThroughFx(false) {}
	void LoadGtaMtlData(char* pLine, float version);

	atString m_Name;
	bool m_IsSeeThrough;
	bool m_IsShootThrough;
	bool m_IsShootThroughFx;
};

//a custom gta material manager for creating packed id's from the material name string.
//
//a material name of the form "material name|procedural name|room id|population multiplier|flags" will be converted to an id
//of the form:	flags (8 bits) - highest bits
//				population multiplier (3 bits)
//				room id (5 bits)
//				procedural id (8 bits)
//				material id	(8 bits) - lowest bits
//
//so as an example we can have a material names that looks like this "MTL_GRASS|PROC_BOTTLES|0|7|0"

class GtaMaterialMgr : public phMaterialMgr
{
public:
	~GtaMaterialMgr();

	static void Create();

	void Load(int reservedSlots = 0) {}
	void Destroy();
	int GetNumMaterials() const;
	const phMaterial& FindMaterial(const char* name) const;
	phMaterialMgr::Id GetMaterialId(const phMaterial& material) const;
	const phMaterial& GetDefaultMaterial() const;

	const char* GetMaterialNameFromIndex(int index) const;

	phMaterial& AllocateMaterial(const char* name);
	void AllocateProcedural(const char* name, int index);

	void LoadMaterials(const char* p_filename);
	void LoadProcedurals(const char* p_filename);

	Id GetPackedPolyFlagValue(u32 flag) const { return (static_cast<Id>(BIT(flag)) << 24); }

protected:
	const phMaterial& GetMaterialImpl(Id id) const;
	phMaterialMgr::Id FindMaterialIdImpl(const char* name) const;
	void BeginLoad(int UNUSED_PARAM(reservedSlots)) {}
	void LoadMaterial(phMaterial& material, fiAsciiTokenizer& token) {}
	void SaveMaterial(const phMaterial& material, fiAsciiTokenizer& token) const {}

private:
	atArray<GtaMaterial> m_MaterialList;
	atArray<atString> m_ProceduralList;
	atMap<atString,int32> m_Material2ID;
	atMap<atString,int32> m_Procedural2ID;
};

} //namespace rage {

#endif //RAGEBUILDER_GTAMATMGR_H_