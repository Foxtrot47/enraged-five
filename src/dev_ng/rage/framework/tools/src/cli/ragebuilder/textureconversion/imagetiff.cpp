
// headers
#include "imagetiff.h"

// RAGE headers
#include "diag/channel.h"
#include "file/asset.h"
#include "file/device.h"
#include "system/memops.h"

// libtiff headers
#include "libtiff/tiff.h"
#include "libtiff/tiffio.h"

RAGE_DECLARE_CHANNEL(texconv)
#define texconvDisplay(fmt, ...) RAGE_DISPLAYF(texconv, fmt, ##__VA_ARGS__)
#define texconvWarning(fmt, ...) RAGE_WARNINGF(texconv, fmt, ##__VA_ARGS__)
#define texconvDebug(  fmt, ...) RAGE_DEBUGF1 (texconv, fmt, ##__VA_ARGS__)
#define texconvError(  fmt, ...) RAGE_ERRORF  (texconv, fmt, ##__VA_ARGS__)
#define texconvLog(    fmt, ...) gTexConvLog.Write     (fmt, ##__VA_ARGS__)

// Static functions (providing libtiff with RAGE fiStream access.
static tsize_t tiff_read( thandle_t st, tdata_t buffer, tsize_t size );
static tsize_t tiff_write( thandle_t st, tdata_t buffer, tsize_t size );
static toff_t tiff_seek( thandle_t st, toff_t pos, int whence );
static int tiff_close( thandle_t st );
static toff_t tiff_size( thandle_t st );
static int tiff_map( thandle_t, tdata_t*, toff_t* );
static void tiff_unmap( thandle_t, tdata_t, toff_t );
static void tiff_error( const char* module, const char* fmt, va_list ap );
static void tiff_warn( const char* module, const char* fmt, va_list ap );

bool TIFFImage::ms_bInitialised = false;

void
TIFFImage::Init( )
{
	if ( ms_bInitialised )
		return;

	TIFFSetErrorHandler( tiff_error );
	TIFFSetWarningHandler( tiff_warn );

	ms_bInitialised = true;
}

::rage::grcImage* 
TIFFImage::LoadTIFF( const char* filename )
{
	TIFFImage::Init( );
	::rage::fiSafeStream S = ASSET.Open(filename, "tif", true, true);
	if (!S) {
		ASSET.FindPath(filename, "tif", true);
		return (NULL);
	}

	TIFF* pTif = TIFFClientOpen( filename, "r", (thandle_t)S, 
		tiff_read, tiff_write, tiff_seek, tiff_close, tiff_size, tiff_map, tiff_unmap );
	if ( !pTif )
		return ( NULL );

	u32 width = 0, height = 0, samplesPerPixel = 0;
	u32 orientation = 0;
	TIFFGetField( pTif, TIFFTAG_IMAGEWIDTH, &width );
	TIFFGetField( pTif, TIFFTAG_IMAGELENGTH, &height );
	TIFFGetField( pTif, TIFFTAG_SAMPLESPERPIXEL, &samplesPerPixel );
	TIFFGetField( pTif, TIFFTAG_ORIENTATION, &orientation );
	if ( 4 != samplesPerPixel )
	{
		texconvError( "Failed to read TIFF RGBA data from %s.  Not RGBA format.",
			filename );
		TIFFClose( pTif );
		return ( NULL );
	}

	// Verify libtiff can read the image.
	char msg[1024] = {0};
	if ( 0 == TIFFRGBAImageOK( pTif, msg ) )
	{
		texconvError( "Failed to read TIFF RGBA data from %s.  libtiff: %s.",
			filename, msg );
		TIFFClose( pTif );
		return ( NULL );
	}

	// Create the grcImage; copying the TIFF data into the grcImage buffer.
	grcImage* pABGRImage = grcImage::Create( width, height, 1,
		grcImage::A8B8G8R8, grcImage::STANDARD, 0, 0 );
	int readResult = TIFFReadRGBAImageOriented( pTif, width, height, 
		(u32*)pABGRImage->GetBits(), ORIENTATION_TOPLEFT, 0 );
	if ( !readResult )
	{
		texconvError( "Failed to read TIFF RGBA data from %s.  Load failed.", filename );
		TIFFClose( pTif );
		return (NULL);
	}
	TIFFClose( pTif );

	// The TIFFReadRGBAImageOrientated is named somewhat funny as it expects the
	// raster buffer to be ABGR-format.  Go figure; so anyway lets reformat the
	// grcImage (unfortunately grcImage::Reformat doesn't handle this format change.
	grcImage* pARGBImage = grcImage::Create( width, height, 1,
		grcImage::A8R8G8B8, grcImage::STANDARD, 0, 0 );
	u32* pABGRImageData = (u32*)pABGRImage->GetBits( );
	u32* pARGBImageData = (u32*)pARGBImage->GetBits( );
	for ( u32 n = 0; n < width * height; ++n )
	{
		u32 abgr = pABGRImageData[n];
		u32 argb =  ( abgr & 0xFF000000 ) | // alpha
					( abgr & 0x00FF0000 ) >> 16 | // blue
					( abgr & 0x0000FF00 ) | // green
					( abgr & 0x000000FF ) << 16; // red
		*(pARGBImageData+n) = argb;
	}

	pABGRImage->Release( );
	return ( pARGBImage );
}

// Static functions
tsize_t 
tiff_read( thandle_t st, tdata_t buffer, tsize_t size )
{
	::rage::fiStream* S = (rage::fiStream*)st; 
	return ( S->Read( buffer, size ) );
}

tsize_t 
tiff_write( thandle_t st, tdata_t buffer, tsize_t size )
{	
	// Not required.
	return ( 0 );
}

toff_t 
tiff_seek( thandle_t st, toff_t pos, int whence )
{	
	::rage::fiStream* S = (rage::fiStream*)st; 
	return ( S->Seek( pos ) );
}

int 
tiff_close( thandle_t st )
{
	return ( 0 ); //fiSafeStream handles this.
}

toff_t 
tiff_size( thandle_t st )
{
	::rage::fiStream* S = (rage::fiStream*)st; 
	return ( S->Size( ) );
};

int 
tiff_map(thandle_t, tdata_t*, toff_t*)
{
	return ( 0 );
};

void 
tiff_unmap(thandle_t, tdata_t, toff_t)
{
	return;
};

void 
tiff_error( const char* module, const char* fmt, va_list ap )
{
	texconvError( fmt, ap );
}

void 
tiff_warn( const char* module, const char* fmt, va_list ap )
{
	texconvWarning( fmt, ap );
}

