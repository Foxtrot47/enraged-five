//
// ragebuilder.h
// 
// Copyright (C) 1999-2012 Rockstar Games.  All Rights Reserved. 
//

#ifndef INC_RAGE_BUILDER_H_
#define INC_RAGE_BUILDER_H_

#include "system/xtl.h"		// so we make sure winsock2 is always included before windows to avoid conflicts
#include "luacore.h"
#include "utils.h"
#include "gta.h"

#include "grcore/texture.h"
#include "fragment/manager.h"
#include "system/externalallocator.h"
#include "physics/levelnew.h"
#include "physics/simulator.h"
#include "pheffects/cloth_verlet.h"
#include "cranimation/animation.h"
#include "cranimation/animdictionary.h"
#include "crclip/clipdictionary.h"
#include "crparameterizedmotion/parameterizedmotiondictionary.h"
#include "crextra/expressions.h"
#include "configParser/configParser.h"
#include "configParser/configGameView.h"
#include "fwscene/stores/textAsset.h"

//#define FIXED_VIRTUAL_ADDRESS	0x50000000
//#define FIXED_PHYSICAL_ADDRESS	0x60000000
//#define FIXED_VIRTUAL_SIZE		0x08000000
//#define FIXED_PHYSICAL_SIZE		0x029c0000
//#define FIXED_VIRTUAL_ALIGN		(128)
//#define FIXED_PHYSICAL_ALIGN	(4096)

#define VIRTUAL_CHUNKSIZE		4096
#define PHYSICAL_CHUNKSIZE		4096


#ifndef RAGE_MAX_PATH
#define RAGE_MAX_PATH 4096
#endif //RAGE_MAX_PATH

extern int g_useVirtualMemory;
extern int g_usePhysicalMemory;

#if !defined RUBYRAGE

class RagePlatform;

// a facade for all the different implementations of rage functionality
// 1. different target platforms are implemented as a quiet switch of implementing class
// 2. to be able to execute properly the rage builder will need to dynamically load 
//    a version of rage compiled as a dll - for example an rscbuild can only be enabled in code.
class RageBuilder
{
public:
	enum PLATFORM
	{
		pltNeutral,
		pltWin32,
		pltWin64,
		pltXenon,
		pltPs3,
		pltDurango,
		pltOrbis,
		pltIndependent,
	};

	enum COMPRESSION_TYPE
	{
		ctDefault,
		ctZlib,
		ctXCompress,
	};

	enum FILETYPES
	{		
		fiClipDictionary,
		fiBoundsDictionary,		
		fiDrawableDictionary,
		fiPhysicsDictionary,
		fiTextureDictionary,
		fiBlendShapeDictionary,
		fiClothDictionary,
		fiDrawable,		
		fiFragment,		
		fiBounds,
		fiPathRegion,
		fiNavMesh,
		fiHierarchicalNavData,
		fiWaypointRecording,
		fiBlendShape,
		fiDefinitions,
		fiPlacements,
		fiEffect,
		fiVehicleRecording,
		fiParamMotionDictionary,	
		fiExpressionsDictionary,
		fiAudMesh,
		fiXmlToRbf,
		fiMapData,
		fiMapTypes,
		fiManifest,
		fiScript,
		fiHeightMesh,
		fiFrameFilterDictionary,
		fiTextDatabase,
		fiCutfile,
		fiUnknown
	};



	static void					Init();
	static void					Close();
	static bool					SetPlatform(PLATFORM ePlatform);
	static PLATFORM				GetPlatform();
	static RagePlatform*		GetPlatformImpl();
	static void					SetCompressionType(COMPRESSION_TYPE eType);
	static COMPRESSION_TYPE		GetCompressionType();

	static const char*			GetPlatformExtension(const char* extension);
	static const char*			ConvertExtensionToPlatform(const char* extension, PLATFORM ePlatform);
	static PLATFORM				GetPlatformForExtension(const char* extension);
	static bool					DoesExtensionMatchPlatform(const char* extension);
//	static bool					AllocateFixedMemoryArea(uint32 address, uint32 size);
//	static void					FreeFixedMemoryArea(uint32 address, uint32 size);

// 	static sysMemExternalAllocator* CreateFixedAllocator(uint32 address, uint32 size, uint32 chunkSize, uint32 align, uint32 nodeCount = 0);
// 	static void ReleaseFixedAllocator(sysMemExternalAllocator*& allocator);
// 
// 	static sysMemExternalAllocator& GetFixedVirtualAllocator() { return *ms_virtualFixedAllocator;}
// 	static sysMemExternalAllocator& GetFixedPhysicalAllocator() { return *ms_physicalFixedAllocator;}

	static void BeginBuild(const char* name, const char* ext, bool secondPass, int heapSize,size_t virtualChunkSize = 0,size_t physicalChunkSize = 0,int nodeCount = 0);
	static bool SaveBuild(const char* filename, const char* extension, int version);
	static void EndBuild(pgBase *toplevelPointer);

	static void LoadTextureList(const char* filename);
	
	static Vector3 GetMoverTrackDisplacement(const float fPhase, crAnimation* pAnim);
	static Quaternion GetMoverTrackRotation(const float fPhase, crAnimation* pAnim);

	static void SetCompact(CallbackData anim);

	static const configParser::ConfigGameView& GetConfig();

private:

	static const configParser::ConfigGameView* gv;
	static phLevelNew*				ms_pLevelNew;
	static phSimulator*				ms_pPhysSim;
	static fragManager*				ms_pFragManager;	
	static PLATFORM					ms_ePlatform;
	static RagePlatform*			ms_pPlatform;
	static COMPRESSION_TYPE			ms_compressionType;

	static sysMemExternalAllocator*	ms_virtualFixedAllocator;
	static sysMemExternalAllocator*	ms_physicalFixedAllocator;
};

#endif

// file extensions
#define CLIP_DICTIONARY_EXT				"#cd"
#define PARAM_MOTION_DICTIONARY_EXT		"#pm"
#define EXPRESSIONS_DICTIONARY_EXT		"#ed"
#define BOUNDS_DICTIONARY_EXT			"#bd"
#define DRAWABLE_DICTIONARY_EXT			"#dd"
#define PHYSICS_DICTIONARY_EXT			"#pd"
#define TEXTURE_DICTIONARY_EXT			"#td"
#define BLENDSHAPE_DICTIONARY_EXT		"#sd"
#define CLOTH_DICTIONARY_EXT			"#ld"
#define DRAWABLE_EXT					"#dr"
#define FRAGMENT_EXT					"#ft"
#define BOUNDS_EXT						"#bn"
#define BLENDSHAPE_EXT					"#bs"
#define DEFINITIONS_EXT					"ide"
#define PLACEMENTS_EXT					"#pl"
#define PATHREGION_EXT					"#nd"
#define NAVMESH_EXT						"#nv"
#define HIERARCHICAL_NAV_EXT			"#hn"
#define EFFECT_EXT						"#pt"
#define ANIM_EXT						"anim"
#define VEHICLERECORDING_EXT			"#vr"
#define WAYPOINT_RECORDING_EXT			"#wr"
#define AUDMESH_EXT						"#am"
#define META_EXT						"meta"
#define MAPDATA_EXT						"#map"
#define MAPTYPES_EXT					"#typ"
#define IMF_EXT							"#mf"
#define SCRIPT_EXT						"#sc"
#define HEIGHTMESH_EXT					"#nh"
#define FRAMEFILTER_DICTIONARY_EXT		"#fd"
#define TEXT_DATABASE_FILE_EXT			"#ldb"
#define CUTFILE_EXT						"cutxml"

#define PI_CLIP_DICTIONARY_EXT			"icd"
#define PI_PARAM_MOTION_DICTIONARY_EXT	"ipm"
#define PI_EXPRESSIONS_DICTIONARY_EXT	"ied"
#define PI_BOUNDS_DICTIONARY_EXT		"ibd"
#define PI_DRAWABLE_DICTIONARY_EXT		"idd"
#define PI_PHYSICS_DICTIONARY_EXT		"ipd"
#define PI_TEXTURE_DICTIONARY_EXT		"itd"
#define PI_BLENDSHAPE_DICTIONARY_EXT	"isd"
#define PI_CLOTH_EXT					"ild"
#define PI_DRAWABLE_EXT					"idr"
#define PI_FRAGMENT_EXT					"ift"
#define PI_BOUNDS_EXT					"ibn"
#define PI_BLENDSHAPE_EXT				"ibs"
#define PI_DEFINITIONS_EXT				"ide"
#define PI_PLACEMENTS_EXT				"ipl"
#define PI_PATHREGION_EXT				"ind"
#define PI_NAVMESH_EXT					"inv"
#define PI_HIERARCHICAL_NAV_EXT			"ihn"
#define PI_WAYPOINT_RECORDING_EXT		"iwr"
#define PI_EFFECT_EXT					"ipt"
#define PI_ANIM_EXT						"anim"
#define PI_AUDMESH_EXT					"iam"
#define PI_META_EXT						"meta"
#define PI_MAPDATA_EXT					"imap"
#define PI_MAPTYPES_EXT					"ityp"
#define PI_IMF_EXT						"imf"
#define PI_SCRIPT_EXT					"sco"
#define PI_HEIGHTMESH_EXT				"inh"
#define PI_FRAMEFILTER_DICTIONARY_EXT	"ifd"
#define PI_TEXT_DATABASE_FILE_EXT		"ildb"
#define PI_CUTFILE_EXT					"cutxml"


// resource file version numbers
// TODO JA - NExt time we do a batch export for something move to use RORC_VERSION
#define CLIP_DICTIONARY_VERSION			crClipDictionary::RORC_VERSION
#define PARAM_MOTION_DICTIONARY_VERSION	crpmParameterizedMotionDictionary::RORC_VERSION
#define EXPRESSIONS_DICTIONARY_VERSION	crExpressions::RORC_VERSION
#define BOUNDS_DICTIONARY_VERSION		phBound::RORC_VERSION
#define FRAGMENT_VERSION				(fragType::RORC_VERSION + 18)
#define DRAWABLE_DICTIONARY_VERSION		(rmcDrawable::RORC_VERSION + 18)
#define CLOTH_DICTIONARY_VERSION		phVerletCloth::RORC_VERSION
#define BLENDSHAPE_DICTIONARY_VERSION	grbTargetManager::RORC_VERSION
#define PHYSICS_DICTIONARY_VERSION		phBound::RORC_VERSION
#define TEXTURE_DICTIONARY_VERSION		grcTextureDefault::RORC_VERSION
#define DRAWABLE_VERSION				(rmcDrawable::RORC_VERSION + 18)
#define BLENDSHAPE_VERSION				grbTargetManager::RORC_VERSION
#define PATHREGION_VERSION				1
#define NAVMESH_VERSION					CNavMesh::RORC_VERSION
#define HIERARCHICAL_NAV_VERSION		CHierarchicalNavData::RORC_VERSION
#define WAYPOINT_RECORDING_VERSION		fwWaypointRecordingRoute::RORC_VERSION
#define ANIM_VERSION					crAnimation::RORC_VERSION
#define EFFECT_VERSION					ptxFxList::RORC_VERSION
#define VEHICLERECORDING_VERSION		CVehicleRecording::RORC_VERSION
#define AUDMESH_VERSION					agPolyMesh::RORC_VERSION
#define SCRIPT_VERSION					scrProgram::RORC_VERSION
#define FRAMEFILTER_DICTIONARY_VERSION	crFrameFilter::RORC_VERSION
#define TEXT_DATABASE_VERSION			fwTextAsset::RORC_VERSION

#if !defined RUBYRAGE

// interface
class RagePlatform
{
public:
	enum DWDTYPE
	{
		dwdHD = 0,
		dwdLOD,
		dwdSLOD1,
		dwdSLOD2,
		dwdSLOD3,
		dwdSLOD4,
		dwdCOUNT
	};

	enum PsoFormat
	{
		PSO_IFF_FORMAT,
		PSO_RSC_FORMAT,
		// others?
	};


	RagePlatform();
	virtual ~RagePlatform();

	virtual void Init() = 0;
	virtual void Shutdown();

	virtual bool IsEndianSwapping() {return false;}
	

	virtual bool InitRage();
	virtual void ShutdownRage();
	virtual bool SetRootDirectory(const char* path);
	static void	 SetEffectPath(const char* effectpath);
	static void	 SetShaderPath(const char* shaderpath);
	static void	 SetShaderDbPath(const char* shaderdbpath);
	static void  SetBuildPath(const char* buildpath);
	static void	 SetAssetsPath(const char* assetspath);
	static void  SetCoreAssetsPath(const char* assetspath);
	static void	 SetMetadataDefinitionsPath(const char* definitionspath);
	static const char* GetEffectPath() {return ms_effectpath;}
	static const char* GetShaderPath() {return ms_shaderpath;}
	static const char* GetShaderDbPath() {return ms_shaderdbpath;}
	static const char* GetBuildPath() { return ms_BuildPath; }
	static const char* GetAssetsPath() { return ms_AssetsPath; }
	static const char* GetCoreAssetsPath() { return ms_CoreAssetsPath; }
	static const char* GetMetadataDefinitionsPath() { return ms_MetadataDefinitionsPath; }
	virtual bool StartBuild();
	virtual bool EndBuild();

	virtual bool LoadMaterialDefs(const char* materials);
	virtual bool LoadProceduralDefs(const char* procedurals);
	virtual bool LoadTextureTemplates(const char* filefilter);

	// dictionaries
	virtual bool LoadTextures(const char* filefilter, int& count);
	virtual bool LoadAnims(const char* filefilter);
	virtual bool LoadClips(const char* filefilter);
	virtual bool LoadParamMotions(const char* filefilter);
	virtual bool LoadExpressionsList(const char* filefilter);
	virtual bool LoadModels(const char* filefilter);
	virtual bool LoadBounds(const char* filefilter);
	virtual bool LoadFrameFilterList(const char *filefilter);

	virtual bool SaveTextureDictionary(const char* filename, const char* hi_suffix);
	virtual bool SaveTextureDictionaryHiMip(const char* filename, const char* suffix);
	virtual bool SaveClipDictionary(const char* filename, bool usePackLoader);
	virtual bool SaveParamMotionDictionary(const char* filename);
	virtual bool SaveExpressionsDictionary(const char* filename);
	virtual bool SaveModelDictionary(const char* filename);
	virtual bool SaveClothDictionary(const char* filename);
	virtual bool SaveBoundDictionary(const char* filename);
	virtual bool SavePhysicsDictionary(const char* filename);
	virtual bool SaveBlendShapeDictionary(const char* filename);
	virtual bool SaveFrameFilterDictionary(const char* filename);

	// objects
	virtual bool LoadTexture(const char* filefilter);
	virtual bool LoadAnim(const char* filefilter);
	virtual bool LoadClip(const char* filefilter);
	virtual bool LoadParamMotion(const char* filefilter);
	virtual bool LoadExpressions(const char* filefilter);
	virtual bool LoadModel(const char* filefilter);
	virtual bool LoadBlendShape(const char* filefilter);
	virtual bool LoadFragment(const char* filefilter);
	virtual bool LoadNMData(const char* filefilter);
	virtual bool LoadBound(const char* filefilter);
	virtual bool LoadPathRegion(const char* filename);
	virtual bool LoadNavMesh(const char* filename);
	virtual bool LoadHeightMesh(const char* filename);
	virtual bool LoadHierarchicalNav(const char* filename);
	virtual bool LoadWaypointRecording(const char* filename);
	virtual bool LoadEffect(const char* filename);
	virtual bool LoadVehicleRecording(const char* filename);
	virtual bool LoadAudMesh(const char * filename);
	virtual bool LoadMeta(const char* filename);
	virtual bool LoadScript( const char* filename );
	virtual bool LoadOccMeshes( const char* filename );
	virtual bool LoadFrameFilter(const char* filename);

	virtual bool SaveTexture(const char* filename);
	virtual bool SaveModel(const char* filename);
	virtual bool SaveFragment(const char* filename);
	virtual bool SaveBound(const char* filename);
	virtual bool SavePathRegion(const char* filename);
	virtual bool SaveNavMesh(const char* filename);
	virtual bool SaveHeightMesh(const char* filename);
	virtual bool SaveHierarchicalNav(const char* filename);
	virtual bool SaveWaypointRecording(const char* filename);
	virtual bool SaveBlendShape(const char* filenameIn, const char* filenameOut);
	virtual bool SaveAnim(const char* filename);
	virtual bool SaveEffect(const char* filename);
	virtual bool SaveVehicleRecording(const char* filename);
	virtual bool SaveAudMesh(const char * filename);
	virtual bool SaveRbf(const char * filename);
	virtual bool SavePso(const char * filename, PsoFormat format);
	virtual bool SaveMapData( const char* filename );
	virtual bool SaveMapTypes( const char* filename );
	virtual bool SaveCutfile( const char* filename );
	virtual bool SaveImf( const char* filename );
	virtual bool SaveScript( const char* filename );
	virtual bool SaveOccImap( const char* filename );
	virtual bool SaveOccXml( const char* filename );
	virtual bool SaveTextDatabase( const char* filename );

	string_list& GetTextureIgnoreList() { return ms_TextureIgnoreList; }
	string_list& GetTextureList() { return ms_TextureList; }

	void SetLimitExemptTextures(string_list& r_LimitExemptTexList) { ms_LimitExemptTexList = r_LimitExemptTexList; }
	string_list& GetLimitExemptTextures() { return ms_LimitExemptTexList; }
	std::string& GetNMData() { return ms_NMData; }

	virtual void SetAutoTexDict(bool autoTexDict);
	virtual void RegisterTextureName(const char* pTexname);

	static s32 GetMinTextureSize();
	static s32 GetMaxTextureSize();
	static float GetTextureRatio();
	static s32 GetMaxMipMapLevels();
	static void SetMaxMipMapLevels(s32 MaxMipMapLevels);
	static void SetMinTextureSize(s32 MinTextureSize);
	static void SetMaxTextureSize(s32 MaxTextureSize);
	static void SetTextureRatio(float TextureRatio);
	static void SetCompressResources(bool enable);
	static void SetTriStripping(bool enable);
	static void SetPostProcessVehicles(bool enable) { ms_PostProcessVehicles = enable; }
	static bool GetPostProcessVehicles() { return ms_PostProcessVehicles; }
	static void SetHeadBlendGeometry(bool enable) { ms_HeadBlendGeometry = enable; }
	static bool GetHeadBlendGeometry() { return ms_HeadBlendGeometry; }
	static void SetHeadBlendWriteBuffer(bool enable) { ms_HeadBlendWriteBuffer = enable; }
	static bool GetHeadBlendWriteBuffer() { return ms_HeadBlendWriteBuffer; }
	static void SetMicroMorphMesh(bool enable) { ms_MicroMorphMesh = enable; }
	static bool GetMicroMorphMesh() { return ms_MicroMorphMesh; }
	static void EnableFragMatrixSet(bool enable) { ms_EnableFragMatrixSet = enable; }
	static bool IsFragMatrixSetEnabled() { return ms_EnableFragMatrixSet; }
	static void SetPPUOnlyHint(bool onOff) { ms_PPUOnlyHint = onOff; }
	static bool GetPPUOnlyHint() { return ms_PPUOnlyHint; }
	static bool SetDwdType(const char* dwdtype);
	static DWDTYPE GetDwdType() {return ms_eDwdType;}
	static void SetLodSkeleton(bool enable) { ms_LodSkeleton = enable; }
	static bool GetLodSkeleton() { return ms_LodSkeleton; }

	static bool GenerateDrawableStats(const char* drawablePathname, const char* outputPathname);
	static bool GenerateFragmentStats(const char* fragmentPathname, const char* outputPathname);

	static void SetOverloadTextureSrc(const std::string folder) { ms_OverloadTexSrc = folder; }
	static const std::string& GetOverloadTextureSrc() { return ms_OverloadTexSrc; }

	static bool GetActualTexPath(const std::string& r_in,std::string& r_out);

	static bool CompressAnim( const char* srcfile, const char* skelfile, const char* outfile, int maxblocksize, float compressionrotation, float compressiontranslation, float compressionscale, float compressiondefault, int compressioncost, int decompressioncost, const char* compressionrules, bool rawonly );
	static const char* GetClipPropertyString( const char* propertyname, const char* clipfile, bool useLoader );
	static int GetClipPropertyInt( const char* propertyname, const char* clipfile, bool useLoader );
	static float GetClipPropertyFloat( const char* propertyname, const char* clipfile, bool useLoader );
	static bool SetClipPropertyString( const char* propertyname, const char* val, const char* clipfile, bool useLoader );
	static bool ConvertAndSaveDDS( const char* ddsFileSrc, const char* ddsFileDest, const char* templateName, const char* platform );
	static bool SetEdgify( bool value );

	static void PushBuildVars();
	static void PopBuildVars();
	static void ResetBuildVars();

	static const char* GetTuningFolder();
	static bool ClipPropertyRemovalCB(crClip* clip, crClipDictionary::ClipKey key, void* data);
	static bool ValidateClip( const char* clipname );

	static bool ConvertXmlToPso(const char* inFilename, const char* outFilename, const char* outExtn, PsoFormat saveFormat);
	static void LoadExternalStructureDefs(bool forceLoadExtstruct = false);
	static int IsPsoInPlaceLoadable(const char* filename, bool printMismatchInfo);
	static int PsoDebugStringSize(const char* filename);
protected:
	static void BaseInit();
	static void InitFactories();
	static void FreezeRefCounting();

	static bool		ms_PostProcessVehicles;
	static bool		ms_HeadBlendGeometry;
	static bool		ms_HeadBlendWriteBuffer;
	static bool		ms_MicroMorphMesh;
	static bool		ms_EnableFragMatrixSet;
	static bool		ms_PPUOnlyHint;
	static DWDTYPE	ms_eDwdType;
	static bool		ms_LodSkeleton;

	string_list		ms_TextureIgnoreList;
	string_list		ms_TextureList;
	bool			m_inputsAreTCLFiles;
	string_list		ms_AnimList;
	string_list		ms_ClipList;
	string_list		ms_ParamMotionList;
	string_list		ms_ExpressionsList;
	string_list		ms_ModelList;
	string_list		ms_BlendShapeList;
	string_list		ms_BoundList;
	string_list		ms_FragmentList;
	string_list		ms_PathRegionList;
	string_list		ms_NavMeshList;
	string_list		ms_HeightMeshList;
	string_list		ms_HierarchicalNavList;
	string_list		ms_WaypointRecordingList;
	string_list		ms_EffectList;
	string_list		ms_VehicleRecordingList;
	string_list		ms_AudMeshList;
	string_list		ms_MetaList;
	string_list		ms_ScriptList;
	string_list		ms_OccMeshList;
	std::string		ms_NMData;
	string_list		ms_FrameFilterList;

	static bool		ms_InitedFactories;
	static bool		ms_CompressResources;

	string_list		ms_LimitExemptTexList;

	struct LimitStackSave 
	{
		bool CompressResources;
		s32	MaxMipMapCount;
		s32	MinTextureSize;
		s32	MaxTextureSize;
		f32 TextureScale;
	};

	enum
	{
		MAX_STACK = 10
	};

	static s32 m_LimitStackPos;
	static LimitStackSave m_LimitStack[MAX_STACK];
	static std::string ms_OverloadTexSrc;

	static rage::fragManager* m_FragManager;
	static char		ms_effectpath[RAGE_MAX_PATH];
	static char		ms_shaderpath[RAGE_MAX_PATH];
	static char		ms_shaderdbpath[RAGE_MAX_PATH];
	static char		ms_TuningFolder[RAGE_MAX_PATH];
	static char		ms_BuildPath[RAGE_MAX_PATH];
	static char		ms_AssetsPath[RAGE_MAX_PATH];
	static char		ms_CoreAssetsPath[RAGE_MAX_PATH];
	static char		ms_MetadataDefinitionsPath[RAGE_MAX_PATH];

	static parManager*				ms_pExternalParser;
	static parManager*				ms_pReferenceParser; // For validation, make sure that externalparser objects match referenceparser ones

	rage::grcTextureFactory* m_textureFactory; 
};

#endif

#endif // INC_RAGE_BUILDER_H_
