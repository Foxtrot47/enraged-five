/**
 * RageBuilder General Functions
 *
 * This header automatically configures the correct function linkage for
 * the following functions depending on the RageBuilder_DLL.h header file
 * defined RAGEBUILDER_DLL.
 */

#ifdef __cplusplus
extern "C"
{
#endif

RAGEBUILDER_DLL bool RBInit(void);
RAGEBUILDER_DLL bool RBShutdown(void);

RAGEBUILDER_DLL const char* RBGetParam(const char* param);
RAGEBUILDER_DLL bool RBWait(void);
RAGEBUILDER_DLL void RBSleep(int ms);
RAGEBUILDER_DLL bool RBSetLogLevel(int loglevel);
RAGEBUILDER_DLL bool RBSetStopOnError(bool stoponerror);
RAGEBUILDER_DLL bool RBReportError(const char* msg);
RAGEBUILDER_DLL bool RBReportWarning(const char* msg);
RAGEBUILDER_DLL bool RBReportQuit(const char* msg);
RAGEBUILDER_DLL bool RBReportDisplay(const char* msg);
RAGEBUILDER_DLL bool RBReportTrace1(const char* msg);
RAGEBUILDER_DLL bool RBReportTrace2(const char* msg);
RAGEBUILDER_DLL bool RBReportTrace3(const char* msg);
RAGEBUILDER_DLL const char* RBGetBuildLogFilename(void);
RAGEBUILDER_DLL const char* RBGetErrorLogFilename(void);
RAGEBUILDER_DLL bool RBDelFile(const char* filename);
RAGEBUILDER_DLL bool RBDelDir(const char* dir, bool contents);
RAGEBUILDER_DLL const char* RBFindFiles(const char* path, bool relative, bool sorted);
RAGEBUILDER_DLL const char* RBFindPlatformFiles(const char* path, bool relative, bool sorted);
RAGEBUILDER_DLL const char* RBFindDirs(const char* path);
RAGEBUILDER_DLL const char* RBRemoveExtensionFromPath(const char* path);
RAGEBUILDER_DLL const char* RBGetExtensionFromPath(const char* path);
RAGEBUILDER_DLL const char* RBGetFilenameFromPath(const char* path);
RAGEBUILDER_DLL const char* RBRemoveNameFromPath(const char* path);
RAGEBUILDER_DLL bool RBCreateLeadingPath(const char* path);
RAGEBUILDER_DLL bool RBCopy_File(const char* src,const char* dest);
RAGEBUILDER_DLL bool RBMove_File(const char* src,const char* dest);
RAGEBUILDER_DLL bool RBFileExists(const char* filename);
RAGEBUILDER_DLL bool RBDirectoryExists(const char* dir);
RAGEBUILDER_DLL int RBFileTimeCompare(const char* filea,const char* fileb);
RAGEBUILDER_DLL unsigned int RBFileSize(const char* filename);
RAGEBUILDER_DLL unsigned int RBResourceSize(const char* filename);
RAGEBUILDER_DLL unsigned int RBResourcePhysicalSize(const char* filename);
RAGEBUILDER_DLL unsigned int RBResourceVirtualSize(const char* filename);
RAGEBUILDER_DLL bool RBSetFileAttrib(const char* filename,const char* attribute,bool val);
RAGEBUILDER_DLL bool RBGetFileAttrib(const char* filename,const char* attribute);
RAGEBUILDER_DLL bool RBCloneTimestamp(const char* src,const char* dest);
RAGEBUILDER_DLL bool RBPushTimer(void);
RAGEBUILDER_DLL bool RBPopTimer(const char* msg);
RAGEBUILDER_DLL const char* RBGetVersion(void);
RAGEBUILDER_DLL void RBFindXMLMatch( const char* src, const char* xmlfilename, char* dst, size_t size );
RAGEBUILDER_DLL void LoadNMXMLData( const char* rawxmlfilename );

#ifdef __cplusplus
}
#endif
